package com.pca.rcp.hbcp2.OrderSummary;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.pca.rcp.hbcp2.util.HBCPCAUtil;
import com.yantra.yfc.rcp.IYRCComposite;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCWizardExtensionBehavior;
import com.yantra.yfc.rcp.YRCXmlUtils;
import com.yantra.yfc.rcp.internal.YRCApiCaller;
/**
 * 
 * @author suresh
 *this class call the service to get shipping methods and add them to output of
 *getOrderFulfillmentDetails API
 */
public class HBCAddShippingChargesExtnBehavior extends
		YRCWizardExtensionBehavior {
	@Override
	public IYRCComposite createPage(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void pageBeingDisposed(String arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void postCommand(YRCApiContext api) {
		
		if (YRCPlatformUI
				.equals(api.getApiName(), "getOrderFulfillmentDetails")) {

			Document OrderDoc = api.getOutputXml();
			Element eleOrder = OrderDoc.getDocumentElement();
			// get Item ids into a map
			Map mapItemIDs = getAllItemIds(eleOrder);
			try {

				// call HBCGetShippingEligibilityService to get service types...
				YRCApiContext yrcApiContext = new YRCApiContext();
				yrcApiContext.setApiName("HBCGetShippingEligibilityService");
				yrcApiContext.setInputXml(OrderDoc);
				yrcApiContext.setFormId(this.getFormId());
				new YRCApiCaller(yrcApiContext, true).invokeApi();
				handleApiCompletion(yrcApiContext);
				// get eligibility basket...
				Element eleEligibilityBasket = getExtentionModel("ExtnEligibilityBasket");
				
				
				if (eleEligibilityBasket != null
						&& YRCPlatformUI.equals(eleEligibilityBasket
								.getTagName(), "basket")) {

					NodeList nResponseLineItem = eleEligibilityBasket
							.getElementsByTagName("ResponseLineItem");
					// get Order lines....
					Element eOrderLines = YRCXmlUtils.getChildElement(eleOrder,
							"OrderLines");
					NodeList nOrderLine = eOrderLines
							.getElementsByTagName("OrderLine");
					String sRestrictions = "";
					for (int i = 0; i < nOrderLine.getLength(); i++) {

						Element eOrderLine = (Element) nOrderLine.item(i);

						String sOrderLineKey = YRCXmlUtils.getAttribute(
								eOrderLine, "OrderLineKey");
						Element eCarrierServiceList = YRCXmlUtils
								.getChildElement(eOrderLine,
										"CarrierServiceList");

						// get matching responseline item from basket...
						Element matchingResLineItem = getMatchingResLineItem(
								sOrderLineKey, nResponseLineItem);
						if (matchingResLineItem != null) {
							Element eligibleShipModes = YRCXmlUtils
									.getChildElement(matchingResLineItem,
											"eligibleShipModes");
							if(eligibleShipModes!=null){
							NodeList nShippingMode = eligibleShipModes
									.getElementsByTagName("ShippingMode");
							for (int k = 0; k < nShippingMode.getLength(); k++) {
								Element eShippingMode = (Element) nShippingMode
										.item(k);
								Element eServiceType = YRCXmlUtils
										.getChildElement(eShippingMode,
												"serviceType");
								if (eServiceType != null) {
									// ........add CarrierService...

									String strServiceType = ((Node) eServiceType)
											.getTextContent();

									String localeServiceType = YRCPlatformUI.getString(strServiceType);
									
									if(YRCPlatformUI.isVoid(localeServiceType)){
										localeServiceType = strServiceType;
									}
									
									
									Element eCarrierService = YRCXmlUtils
											.createChild(eCarrierServiceList,
													"CarrierService");
									YRCXmlUtils.setAttribute(eCarrierService,
											"CarrierServiceCode",
											strServiceType);
									YRCXmlUtils.setAttribute(eCarrierService,
											"CarrierServiceDesc",
											localeServiceType);
								}
							}
							}
							// get restrictions..............
							
							Element eshippingRestrictions = YRCXmlUtils
									.getChildElement(matchingResLineItem,
											"shippingRestrictions");
							if (eshippingRestrictions != null) {
								NodeList nShippingRestriction = eshippingRestrictions
										.getElementsByTagName("ShippingRestriction");
								String strrestrictionCode = "";
								String strrestrictionText = "";
								// create one element for shippingRestrictions
								// uder
								// order line.........
								Element eleShippingRestrictionsOL = YRCXmlUtils
										.createChild(eOrderLine,
												"ShippingRestrictions");
								if (nShippingRestriction != null) {
									for (int j = 0; j < nShippingRestriction
											.getLength(); j++) {
										//from restriction popup message
										sRestrictions = sRestrictions+mapItemIDs.get(sOrderLineKey);
										Element eShippingRestriction = (Element) nShippingRestriction
												.item(j);
										Element erestrictionCode = YRCXmlUtils
												.getChildElement(
														eShippingRestriction,
														"restrictionCode");
										if (erestrictionCode != null) {
											strrestrictionCode = ((Node) erestrictionCode)
													.getTextContent();
											sRestrictions = sRestrictions+":"+strrestrictionCode;
										}
										Element erestrictionText = YRCXmlUtils
												.getChildElement(
														eShippingRestriction,
														"restrictionText");
										if (erestrictionText != null) {
											strrestrictionText = ((Node) erestrictionText)
													.getTextContent();
											sRestrictions = sRestrictions+":"+strrestrictionText+"\n";
										}
										// add restriction to OL restrictions...
										Element eleShippingRestrictionOL = YRCXmlUtils
												.createChild(
														eleShippingRestrictionsOL,
														"ShippingRestriction");
										// set attributes for restrictions...
										YRCXmlUtils.setAttribute(
												eleShippingRestrictionOL,
												"restrictionCode",
												strrestrictionCode);
										YRCXmlUtils.setAttribute(
												eleShippingRestrictionOL,
												"restrictionText",
												strrestrictionText);

									}

								}
							}

						}
					
					}
					if(!YRCPlatformUI.equals(sRestrictions,"")){
						HBCPCAUtil.showOptionDialogBoxWithOK("Restrictions", sRestrictions);
					}
				}else if(eleEligibilityBasket != null && YRCPlatformUI.equals(eleEligibilityBasket.getTagName(), "YfsException")){
					String sException = YRCXmlUtils.getAttribute(eleEligibilityBasket, "Exception");
					String strItemId = YRCXmlUtils.getAttribute(eleEligibilityBasket, "ItemID");
					if(sException.equalsIgnoreCase("State"))
						HBCPCAUtil.showOptionDialogBoxWithOK("Error", YRCPlatformUI.getFormattedString("WRONG_STATE_CODE_MSG",strItemId));
					if(sException.equalsIgnoreCase("Postal"))
						HBCPCAUtil.showOptionDialogBoxWithOK("Error", YRCPlatformUI.getFormattedString("WRONG_POSTAL_CODE_MSG",strItemId));	
//					HBCPCAUtil.showOptionDialogBoxWithOK("Error", sException);
					
				}else if (eleEligibilityBasket == null || !YRCPlatformUI.equals(eleEligibilityBasket
						.getTagName(), "NoRequestLine")) {
					HBCPCAUtil.showOptionDialogBoxWithOK(YRCPlatformUI.getFormattedString("ERROR", ""),
							YRCPlatformUI.getFormattedString("WEBSERVICE_DOWN", ""));
				}	
			} catch (Exception e) {
				YRCPlatformUI.trace(e);
				//e.printStackTrace();

			}
		}
		
		super.postCommand(api);
	}

	public Map getAllItemIds(Element eleOrder) {
		// TODO Auto-generated method stub
		Map<String,String> itemsMap = new HashMap<String, String>();
		if(eleOrder!=null){
			Element eOrderLines = YRCXmlUtils.getChildElement(eleOrder, "OrderLines");
			NodeList nOrderLine = eOrderLines.getElementsByTagName("OrderLine");
			if(nOrderLine!=null){
				for(int i=0;i<nOrderLine.getLength();i++){
					Element eOrderLine = (Element)nOrderLine.item(i);
					Element eItem = YRCXmlUtils.getChildElement(eOrderLine, "Item");
					itemsMap.put(YRCXmlUtils.getAttribute(eOrderLine, "OrderLineKey"), YRCXmlUtils.getAttribute(eItem, "ItemID"));
				}
			}
		}
		return itemsMap;
	}

	boolean flag = false;

	@Override
	public void postSetModel(String model) {
		// TODO Auto-generated method stub

		super.postSetModel(model);
	}

	/**
	 * @param itemID
	 * @param responseLineItem
	 * @return matching request line using order line key
	 * 
	 */
	private Element getMatchingResLineItem(String orderLineKey,
			NodeList responseLineItem) {
		// TODO Auto-generated method stub
		Element matchingElement = null;
		if (responseLineItem != null) {
			for (int i = 0; i < responseLineItem.getLength(); i++) {
				Element eleResponseLineItem = (Element) responseLineItem
						.item(i);
				Element elineItemID = YRCXmlUtils.getChildElement(
						eleResponseLineItem, "lineItemID");
				if (elineItemID != null) {
					String tempItemID = ((Node) elineItemID).getTextContent();
					if (tempItemID != null
							&& YRCPlatformUI.equals(tempItemID, orderLineKey)) {
						matchingElement = eleResponseLineItem;
					}
				}
			}
		}
		return matchingElement;

	}

	@Override
	public void handleApiCompletion(YRCApiContext ctx) {
		
		if (YRCPlatformUI.equals(ctx.getApiName(),
				"HBCGetShippingEligibilityService")) {
			Document docBasket = ctx.getOutputXml();

			if (docBasket != null) {
				setExtentionModel("ExtnEligibilityBasket", docBasket
						.getDocumentElement());

			}
		}
		super.handleApiCompletion(ctx);
	}

	@Override
	public boolean preCommand(YRCApiContext ctx) {
		// TODO Auto-generated method stub
		
		return super.preCommand(ctx);
	}

	public Element getOrderModel(String modelname) {
		// TODO Auto-generated method stub
		return this.getModel(modelname);
	}

}
