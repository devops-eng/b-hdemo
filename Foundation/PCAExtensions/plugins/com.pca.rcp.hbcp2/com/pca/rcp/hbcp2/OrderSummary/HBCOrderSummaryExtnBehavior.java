package com.pca.rcp.hbcp2.OrderSummary;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.yantra.yfc.rcp.IYRCComposite;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCWizardExtensionBehavior;
import com.yantra.yfc.rcp.YRCXmlUtils;

public class HBCOrderSummaryExtnBehavior extends
		YRCWizardExtensionBehavior {

	public IYRCComposite createPage(String str1) {
		// TODO Auto-generated method stub
		return null;
	}

	public void pageBeingDisposed(String str2) {
		// TODO Auto-generated method stub

	}

	public void postSetModel(String inp) {
		// TODO Auto-generated method stub
		if (YRCPlatformUI.equals("OrderDetails",inp)) {
			Document orderDetailsDocument = this.getModel(inp)
					.getOwnerDocument();

			Element orderElement = orderDetailsDocument.getDocumentElement();
			String strEnterpriseCode =  orderElement.getAttribute("EnterpriseCode");
			if(!"LT".equals(strEnterpriseCode)){
				setControlVisible("extn_IsShopRnrOrd_Lbl", false);
				setControlVisible("extn_IsShopRnrOrd_Value", false);
			}else{
				setFieldValue("extn_IsShopRnrOrd_Value", YRCPlatformUI.getString("Shoprunner_N"));
			Element extnTag = YRCXmlUtils.getChildElement(orderElement, "Extn");
			if (extnTag != null){ 
			String srAuthToken = YRCXmlUtils.getAttribute(extnTag,"ExtnShopRnrAuthTok");
			if (!YRCPlatformUI.isVoid(srAuthToken)) {
				setFieldValue("extn_IsShopRnrOrd_Value", YRCPlatformUI.getString("Shoprunner_Y"));
			}
			}
			}
			//to display Border Free Order Numbers for international orders
			Element eleExtn = YRCXmlUtils.getXPathElement(orderElement, "Order/Extn");
			if (!YRCPlatformUI.isVoid(eleExtn)) {
				String strExtnIsFiftyOne=eleExtn.getAttribute("ExtnIsFiftyOne");
				if(!"Y".equalsIgnoreCase(strExtnIsFiftyOne)){
					setControlVisible("extn_IsIntlOrder_Lbl", false);
					setControlVisible("extn_IsIntlOrderNo_Value", false);
				}
				
			}
			//Start	Defect #3452 - Reward Number
			if("BAY".equals(strEnterpriseCode)){

				Element extnTag = YRCXmlUtils.getChildElement(orderElement, "Extn");
				setControlVisible("extn_RewardNumber_Lbl", true);
				setControlVisible("extn_txtRewardNumber", true);
				if(extnTag!=null && extnTag.getAttribute("ExtnLoyaltyNo")!=null && extnTag.getAttribute("ExtnLoyaltyNo")!=""){
					setFieldValue("extn_txtRewardNumber", extnTag.getAttribute("ExtnLoyaltyNo"));					
				}
				else{
					setFieldValue("extn_txtRewardPoints", "0");
				}
				
			}else{
				setControlVisible("extn_RewardNumber_Lbl", false);
				setControlVisible("extn_txtRewardNumber", false);					
			}
			//End Defect #3452 - Reward Number
			super.postSetModel(inp);
		}

	}

	/**
		 * R3.3 - This method is used to capture the pick up date to an ExtnPickupDate instead of ReqShipDate
		 * which enables the order to get released irrespective of the ReqShipDate
		**/
		@Override
		public void postCommand(YRCApiContext apiContext) {
			String[] getAllAPIsCalled=apiContext.getApiNames();

		 	for(int i=0;i<getAllAPIsCalled.length;i++)
		 	{
		 		String apiName=getAllAPIsCalled[i];
		 		if(apiName.equalsIgnoreCase("getSalesOrderDetails"))
		 		{
		 			Document[] getAllOutputXMLs=apiContext.getOutputXmls();
		 			Document docGetSalesOrderDetails = getAllOutputXMLs[1];
		 			Element eorderElement = docGetSalesOrderDetails.getDocumentElement();
		 			if (!YRCPlatformUI.isVoid(eorderElement)){
						Element eOrderLines = YRCXmlUtils.getChildElement(eorderElement, "OrderLines");
						if (!YRCPlatformUI.isVoid(eOrderLines)){
							NodeList nOrderLines = eOrderLines.getElementsByTagName("OrderLine");
							for(int t=0;t<nOrderLines.getLength();t++)
							{
							Element eOrderLine = (Element)nOrderLines.item(t);
							if(!YRCPlatformUI.isVoid(eOrderLine))
							{
								Element eExtn = YRCXmlUtils.getXPathElement(eOrderLine, "OrderLine/Extn");
								if(!YRCPlatformUI.isVoid(eExtn))
								{
									String sExtnPickupDate = eExtn.getAttribute("ExtnPickupDate");
									if(!YRCPlatformUI.isVoid(sExtnPickupDate)){
										eOrderLine.setAttribute("ReqShipDate", sExtnPickupDate);
										}
									}
								}
							}
						}
					}
		 		}
		 	}
			super.postCommand(apiContext);
	}

}
//TODO Validation required for a Text control: extn_IsIntlOrderNo_Value
