package com.pca.rcp.hbcp2.OrderSummary.wizardPages;

/**
 * Created on Sep 01,2013
 *
 */
 
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;

import com.pca.rcp.hbcp2.util.XMLUtil;
import com.yantra.yfc.rcp.YRCConstants;
import com.yantra.yfc.rcp.YRCExtendedCellModifier;
import com.yantra.yfc.rcp.YRCExtendedTableImageProvider;
import com.yantra.yfc.rcp.YRCExtentionBehavior;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCTblClmBindingData;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCExtendedTableBindingData;
/**
 * @author Rajath_Babu
 * Copyright IBM Corp. All Rights Reserved.
 */
 public class HBCPickupPanelExtnBehaviour extends YRCExtentionBehavior{
	 Element eleOrderModel = null;
	/**
	 * This method initializes the behavior class.
	 */
	public void init() {
		//TODO: Write behavior init here.
	} 	
 	
	/**
	 * Method for validating the text box.
     */
    public YRCValidationResponse validateTextField(String fieldName, String fieldValue) {
    	// TODO Validation required for the following controls.
		
		// TODO Create and return a response.
		return super.validateTextField(fieldName, fieldValue);
	}
    
    /**
     * Method for validating the combo box entry.
     */
    public void validateComboField(String fieldName, String fieldValue) {
    	// TODO Validation required for the following controls.
		
		// TODO Create and return a response.
		super.validateComboField(fieldName, fieldValue);
    }
    
    /**
     * Method called when a button is clicked.
     */
    public YRCValidationResponse validateButtonClick(String fieldName) {
    	// TODO Validation required for the following controls.
		
		// TODO Create and return a response.
		return super.validateButtonClick(fieldName);
    }
    
    /**
     * Method called when a link is clicked.
     */
	public YRCValidationResponse validateLinkClick(String fieldName) {
    	// TODO Validation required for the following controls.
		
		// TODO Create and return a response.
		return super.validateLinkClick(fieldName);
	}
	
	/**
	 * Create and return the binding data for advanced table columns added to the tables.
	 * 
	 * SOM changes Req#19 May'14 Release 
	 */
	 public YRCExtendedTableBindingData getExtendedTableBindingData(String tableName, ArrayList tableColumnNames) {
	 	// Create and return the binding data definition for the table.
		
		HashMap cellTypesMap = new HashMap();
		/*Create a Map of Advanced Column Binding Data*/
	    HashMap clmBindingData = new HashMap();
		/* Creating extended table binding object*/
      	YRCExtendedTableBindingData tblBindingData = new YRCExtendedTableBindingData();   
      	/* Creating extended table column binding object*/
      	YRCTblClmBindingData advclmBindingData = new YRCTblClmBindingData();
      	/*Setting the attribute binding to the advanced table column*/
      	advclmBindingData.setAttributeBinding("@Calendar");      	
      	/* Add the advanced column binding data object to the data map object.*/
      	clmBindingData.put("extn_CalBtnColumn", advclmBindingData);
      	/* Making the advanced column editable*/
      	cellTypesMap.put("extn_CalBtnColumn", YRCConstants.YRC_BUTTON_BINDING_DEFINATION);
      	
        //--Calendar pop up and capturing date        
        YRCExtendedCellModifier extntblCellModifier = new YRCExtendedCellModifier() {
              public boolean allowModify(String property, String value,Element element) {                    
                    String sCal = null;                   
                    String sMaxLineStatus = element.getAttribute("MaxLineStatus");                    
                    int iMaxLineStatus = 0;
                    //--Start- get pointer location for displaying calendar pop up
                    PointerInfo pointerInfo  = MouseInfo.getPointerInfo();
                    Point pointerLocation  = pointerInfo.getLocation();
                    int x = (int)pointerLocation.getX();
                    int y = (int)pointerLocation.getY();
                    //--End- get pointer location for displaying calendar pop up
                    if(!XMLUtil.isVoid(sMaxLineStatus)){
                    	iMaxLineStatus = Integer.parseInt(sMaxLineStatus);   
                    }
                    if(property.equalsIgnoreCase("@Calendar") && iMaxLineStatus <= 3350){
                    	//-- show calendar pop up
                    	Date disableBeforeDate = new Date();
                    	sCal = YRCPlatformUI.showCalendar(x, y, disableBeforeDate);
                    	//--Calendar pop up captures date in format "MM/dd/yyyy", 
                    	//--convert it to "yyyy-MM-dd" which resembles with OOB display
                    	try {
	                        SimpleDateFormat sdfAsCaptured = new SimpleDateFormat("MM/dd/yyyy");
	                        SimpleDateFormat sdfAsDisplayed = new SimpleDateFormat("yyyy-MM-dd");
	                        Date dPickUpDate;					
						    dPickUpDate = sdfAsCaptured.parse(sCal);
						    sCal = sdfAsDisplayed.format(dPickUpDate);
                    	}catch (ParseException e) {
    						// TODO Auto-generated catch block
    						e.printStackTrace();
    					}                      
                      if(!XMLUtil.isVoid(eleOrderModel) && !XMLUtil.isVoid(sCal)){
                    	  NodeList nOrderLineList = eleOrderModel.getElementsByTagName("OrderLine");
                    	  Element eleOrderLine = null;
                    	  Element eleOrderLineExtn = null;
                    	  int iLength = nOrderLineList.getLength();
                    	  for(int iCount=0; iCount < iLength; iCount++){
                    		  eleOrderLine = (Element) nOrderLineList.item(iCount);
                    		  if(!XMLUtil.isVoid(eleOrderLine)){
                    			  String sMaxOrderLineStatus = eleOrderLine.getAttribute("MaxLineStatus");
                    			  int iMaxOrderLineStatus = 0;
                    			  if(!XMLUtil.isVoid(sMaxLineStatus)){
                    				  iMaxOrderLineStatus = Integer.parseInt(sMaxOrderLineStatus);   
                                  }
                    			  if(iMaxOrderLineStatus <= 3350){
	                    			  eleOrderLineExtn = XMLUtil.getChildElement(eleOrderLine, "Extn");
	                    			  if(XMLUtil.isVoid(eleOrderLineExtn)){
	                                      eleOrderLineExtn = XMLUtil.createChild(eleOrderLine, "Extn");
	                                      }
	                    			  eleOrderLineExtn.setAttribute("ExtnPickupDate", sCal);
                    			  }
                    		  }
                    	  }
                    	  postSetModel("UpdateOrderModel");
                      }
                    }
                    return true;
              }
              public String getModifiedValue(String property, String value,Element element) {
                    return value;
              }            
              public YRCValidationResponse validateModifiedValue(String property,String value, Element element) {
                    return new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_OK, "Valid");
              }
        };
        //--Includes calendar image in cell.
        YRCExtendedTableImageProvider imageProvider = new YRCExtendedTableImageProvider(){
              @Override
              public String getImageThemeForColumn(Object arg0, String arg1) {
                    // TODO Auto-generated method stub                    
                    if(arg1.equalsIgnoreCase("@Calendar")){
                          return "ButtonLargeCalendar";
                    }
                    return null;
              } 
        };        
        tblBindingData.setImageProvider(imageProvider);
        tblBindingData.setCellTypes(cellTypesMap);
        tblBindingData.setTableColumnBindingsMap(clmBindingData);
        tblBindingData.setCellModifier(extntblCellModifier);
        return tblBindingData;	 
	 	// The default super implementation does nothing.
	 	//return super.getExtendedTableBindingData(tableName, tableColumnNames);
	 }
	 
	 /**
	 * Method call to set model content
	 * 
	 * SOM changes Req#19 May'14 Release
	 */
	 
	 @Override
	public void postSetModel(String namespace) {
		// TODO Auto-generated method stub
		 if(namespace.equalsIgnoreCase("Order")){
			 eleOrderModel = this.getModel(namespace);
		 }
		 if(namespace.equalsIgnoreCase("UpdateOrderModel")){
			 this.repopulateModel("Order");
		 }
		super.postSetModel(namespace);
	}
	 
	 /**
	   * R3 - This method is implemented to return a model on this form to another form
	   **/
	 public Element getOrderModel(String model) {
			// TODO Auto-generated method stub
			return this.getModel(model);
		}
}
