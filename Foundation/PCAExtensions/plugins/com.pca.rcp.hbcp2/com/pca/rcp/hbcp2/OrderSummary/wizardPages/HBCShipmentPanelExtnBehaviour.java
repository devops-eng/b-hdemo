package com.pca.rcp.hbcp2.OrderSummary.wizardPages;

import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.pca.rcp.hbcp2.util.HBCPCAConstants;
import com.yantra.yfc.rcp.IYRCComposite;
import com.yantra.yfc.rcp.IYRCTableColumnTextProvider;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCExtendedCellModifier;
import com.yantra.yfc.rcp.YRCExtendedTableBindingData;
import com.yantra.yfc.rcp.YRCExtentionBehavior;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCTblClmBindingData;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCWizardExtensionBehavior;
import com.yantra.yfc.rcp.YRCXmlUtils;
import com.yantra.yfc.rcp.YRCWizardBehavior;
import com.yantra.yfc.rcp.internal.YRCApiCaller;
import java.util.HashMap;

	/**
	 * R3 - Extending YRCExtentionBehavior instead of YRCWizardExtensionBehavior
	 * to enable modification to advanced table columns on this form ID
	**/
//public class HBCShipmentPanelExtnBehaviour extends YRCWizardExtensionBehavior{
public class HBCShipmentPanelExtnBehaviour extends YRCExtentionBehavior{

	String sOrgCode = null;
	NodeList nCommonCodeList = null;
	//R3 - DSV changes - 11-11-2013 - Start
	String sHasDsvItem = "N";
	//R3 - DSV changes - 11-11-2013 - End
	/*@Override
	public void pageBeingDisposed(String wizardPageFormId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public IYRCComposite createPage(String pageIdToBeShown) {
		// TODO Auto-generated method stub
		return null;
	}*/

	public Element getOrderModel(String model) {
		// TODO Auto-generated method stub
		return this.getModel(model);
	}

   /**
	 * R3 - This method creates and returns the binding data for advanced table columns
	 * added to the tables. Check boxes will be disabled for order lines that have
	 * DSV items
	**/
	public YRCExtendedTableBindingData getExtendedTableBindingData(
			String tableName, ArrayList tableColumnNames) {
		YRCExtendedTableBindingData tblBindingData = new YRCExtendedTableBindingData();
		if ("tblOrderLines".equalsIgnoreCase(tableName)) {
			// Create and return the binding data definition for the table.
			YRCExtendedCellModifier extntblCellModifier = new YRCExtendedCellModifier() {
				// This method allows enabling and disabling editing of columns.
				public boolean allowModify(String strAttribute,
						String strValue, Element eleOrderLine) {
					if ("@Checked".equalsIgnoreCase(strAttribute)) {
						Element eleItemExtn = YRCXmlUtils.getXPathElement(
								eleOrderLine, "OrderLine/ItemDetails/Extn");
						String strIsDSVEnabled = eleItemExtn
								.getAttribute("ExtnIsDSVEnabled");
						//R3 - DSV changes - 11-11-2013 - Start
						if ("Y".equalsIgnoreCase(sHasDsvItem)){
							//R3 - DSV changes - 11-11-2013 - End
							return false;
						} else {
							return true;
						}
					}
					else {
						return true;
					}
				}

				public String getModifiedValue(String strAttribute,
						String strValue, Element eleCharge) {
					return strValue;
				}

				public YRCValidationResponse validateModifiedValue(
						String strAttribute, String strValue, Element eleCharge) {
					return null;
				}
			};

			tblBindingData.setCellModifier(extntblCellModifier);
		}
		return tblBindingData;
	}

   /**
	 * R3 - This method is implemented to disable the Check All option for adding
	 * gift options when any order line has a DSV item
	**/
	 public void postSetModel(String model) {
		if (YRCPlatformUI.equals("Order", model)){
			HashMap mapCommonCodeValue = new HashMap();
			String sDisplayControlFlag = "Y";
			Document DocOrder = this.getModel(model).getOwnerDocument();
			Element eleOrder = getModel("Order");
			
			sOrgCode = eleOrder.getAttribute("EnterpriseCode");
			getCommonCodeList(sOrgCode);
			//R3 - DSV changes - 11-11-2013 - Start
			//String sHasDsvItem = "N";
			String sOrderHeaderKey = eleOrder.getAttribute("OrderHeaderKey");
			
			getOrderListToCheckDSVItems(sOrderHeaderKey);
			Element eleOrderList = this.getModel("Extn_HBCGetOrderListToCheckDSVItems_Output");
			Element eleDsvOrder = YRCXmlUtils.getXPathElement(eleOrderList, "OrderList/Order");
			NodeList nDsvOrderLines = eleDsvOrder
					.getElementsByTagName("OrderLine");
			for (int i = 0; i < nDsvOrderLines.getLength(); i++) {
				Element eleDsvOrderLine = (Element) nDsvOrderLines.item(i);
				Element eleDsvItemExtn = YRCXmlUtils.getXPathElement(eleDsvOrderLine, "OrderLine/ItemDetails/Extn");
				String strIsDSVEnabled = YRCXmlUtils.getAttribute(eleDsvItemExtn, "ExtnIsDSVEnabled");
				if (!YRCPlatformUI.isVoid(strIsDSVEnabled)&& strIsDSVEnabled.equalsIgnoreCase("Y"))
				{
					sHasDsvItem = "Y";
					break;
				}
			}
			
			//R3 - DSV changes - 11-11-2013 - End
			// storing Common code value in HashMap
			if (nCommonCodeList!= null)
			 {
				//for each common code value, validating the input for payment Type
				 for(int k=0; k<nCommonCodeList.getLength(); k++)
				 {
					 Element eleCommonCode = (Element) nCommonCodeList.item(k);
					 String sCodeValue = eleCommonCode.getAttribute("CodeValue");
					 mapCommonCodeValue.put(sCodeValue,sCodeValue);
				 }
			}
			if (eleOrder != null) {
				NodeList nOrderLines = eleOrder
						.getElementsByTagName("OrderLine");
				for (int i = 0; i < nOrderLines.getLength(); i++) {
					Element eleOrderLine = (Element) nOrderLines.item(i);
					Element eCarrierCode = YRCXmlUtils.getXPathElement(eleOrderLine, "OrderLine/CarrierServiceList");
					NodeList nCarrierService = eCarrierCode.getElementsByTagName("CarrierService");
			
				// comparing common code value from carrier service code
					if(sHasDsvItem.equalsIgnoreCase("Y")){
					for (int j = 0; j < nCarrierService.getLength(); j++){
						Element eCarrierService = (Element) nCarrierService.item(j);
						String sCarrierServiceCode = YRCXmlUtils.getAttribute(eCarrierService, "CarrierServiceCode");
						
						if(!mapCommonCodeValue.isEmpty()){
						   if (!(mapCommonCodeValue.containsValue(sCarrierServiceCode))){
							   eCarrierCode.removeChild(eCarrierService);
						    }
						}
					}
					}
					//R3 - DSV changes - 11-11-2013 - Start
					if ("Y".equalsIgnoreCase(sHasDsvItem)) {
						//R3 - DSV changes - 11-11-2013 - End
						sDisplayControlFlag = "N";
					}
				}
				if ("N".equalsIgnoreCase(sDisplayControlFlag)) {
					super.setControlVisible("chkSelectAll", false);
				}
			}
		}
		super.repopulateModel(model);
	}
	 //R3 - DSV changes - 11-11-2013 - Start
	 private void getOrderListToCheckDSVItems(String sOrderHeaderKey) {
	// TODO Auto-generated method stub
		 org.w3c.dom.Document docGetOrderList = YRCXmlUtils.createDocument("Order");
		 Element eleGetOrderListInput = docGetOrderList.getDocumentElement();
		 eleGetOrderListInput.setAttribute("OrderHeaderKey", sOrderHeaderKey);
		 YRCApiContext context = new YRCApiContext();
			context.setFormId(super.getFormId());
			context.setApiName("HBCGetOrderListToCheckDSVItems");
			context.setInputXml(docGetOrderList);
			new YRCApiCaller(context, true).invokeApi();
			handleApiCompletion(context);
	 }
	//R3 - DSV changes - 11-11-2013 - End
	 
	//@Override
		public  void getCommonCodeList(String sEnterpriseCode)	//setting the input for  getCommonCodeList API
	 {
		
			org.w3c.dom.Document docHBCGetCommonCodeList = YRCXmlUtils.createDocument("CommonCode");
			Element eleCommonCodeInput = docHBCGetCommonCodeList.getDocumentElement();
			eleCommonCodeInput.setAttribute("CodeType", HBCPCAConstants.DSV_CARRIER_SERVICE);
			eleCommonCodeInput.setAttribute("OrganizationCode",sEnterpriseCode);
			YRCApiContext context = new YRCApiContext();
			context.setFormId(super.getFormId());
			context.setApiName("HBCGetCommonCodeList");
			context.setInputXml(docHBCGetCommonCodeList);
			new YRCApiCaller(context, true).invokeApi();
			handleApiCompletion(context);
			//callApi(context);
		}
	 
	 public void handleApiCompletion(YRCApiContext context) 
		{
			if ("HBCGetCommonCodeList".equals(context.getApiName()))
			{		
				this.setExtentionModel("Extn_HBCGetCommonCode_Output", context.getOutputXml().getDocumentElement());	
				Element eleCommonCodeList = this.getModel("Extn_HBCGetCommonCode_Output");
				if (!YRCPlatformUI.isVoid(eleCommonCodeList)) 
				{
					nCommonCodeList = eleCommonCodeList.getElementsByTagName("CommonCode");

				}
				
				super.handleApiCompletion(context);
			}
			//R3 - DSV changes - 11-11-2013 - Start
			if ("HBCGetOrderListToCheckDSVItems".equals(context.getApiName()))
			{		
				this.setExtentionModel("Extn_HBCGetOrderListToCheckDSVItems_Output", context.getOutputXml().getDocumentElement());					
				super.handleApiCompletion(context);
			}
			//R3 - DSV changes - 11-11-2013 - End
		}
}
