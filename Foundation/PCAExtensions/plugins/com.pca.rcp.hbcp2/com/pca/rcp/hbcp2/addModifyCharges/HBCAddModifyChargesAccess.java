package com.pca.rcp.hbcp2.addModifyCharges;

import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Link;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.pca.rcp.hbcp2.util.HBCPCAConstants;
import com.pca.rcp.hbcp2.util.XMLUtil;
import com.yantra.yfc.rcp.IYRCComposite;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCExtentionBehavior;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCWizardExtensionBehavior;
import com.yantra.yfc.rcp.YRCXmlUtils;
import com.yantra.yfc.rcp.internal.YRCApiCaller;

import com.yantra.pca.ycd.rcp.tasks.addModifyCharges.actions.YCDAddModifyChargesHeaderPopupAction;
import com.yantra.pca.ycd.rcp.tasks.addModifyCharges.wizardpages.YCDAddModifyChargesPage;
import com.yantra.pca.ycd.rcp.tasks.addModifyCharges.wizardpages.YCDAddModifyChargesPageHelper;


/**
 * @author vidya
 *
 */
public class HBCAddModifyChargesAccess extends YRCWizardExtensionBehavior{

List userGroupKeyList = new ArrayList();
	
	@Override
	public void init() { 
};
	public void postCommand(YRCApiContext yrcapicontext) {
			super.postCommand(yrcapicontext);
	}

	@Override
	public boolean preCommand(YRCApiContext yrcapicontext) {
		
		return super.preCommand(yrcapicontext);
	}
		
	
		public static Control getControl(Composite composite, String name) {
			Control[] ctrl = composite.getChildren();
			for(Control object:ctrl){
			if (object instanceof Composite) {
			Composite cmp = (Composite) object;
			Control c = getControl(cmp, name);
			if(!YRCPlatformUI.isVoid(c))
			return c;
			}
			else if(name.equals(object.getData("name")))
			return object;
			}
			return null;
			}
		
	@Override
	public void validateComboField(String arg0, String arg1) {
		
		super.validateComboField(arg0, arg1);
	}
	@Override
	public void handleApiCompletion(YRCApiContext ctx) {
		// TODO Auto-generated method stub
		
		

		super.handleApiCompletion(ctx);
	}
	
	@Override
	public void postSetModel(String model) {
		//Check if Order is confirmed order then disable add/odify charges 
			if(model.equals("OldOrderDetails"))
			{
				Element OrderDetails = this.getModel("OldOrderDetails");
				if (OrderDetails != null) {
					
					String strOrderHdrKey= OrderDetails.getAttribute(HBCPCAConstants.A_ORDERHEADERKEY);
					Document inputDocOrderDeatails;

				
					try {
						inputDocOrderDeatails = XMLUtil
						.createDocument("Order");
						Element inputCCEle = inputDocOrderDeatails.getDocumentElement();
						inputCCEle.setAttribute(HBCPCAConstants.A_ORDERHEADERKEY,strOrderHdrKey);
						YRCApiContext yrcApiContext = new YRCApiContext();
						yrcApiContext.setApiName("getOrderDetails");
						yrcApiContext.setInputXml(inputDocOrderDeatails);
						yrcApiContext.setFormId(this.getFormId());
						new YRCApiCaller(yrcApiContext, true).invokeApi();
						handleApiCompletion(yrcApiContext);
						if (yrcApiContext.getOutputXml() != null) {
							Document outputCCDoc = yrcApiContext.getOutputXml();
							if (!XMLUtil.isVoid(outputCCDoc)) 
							{
								Element outputCCDocEle = outputCCDoc.getDocumentElement();
								String StrStatus=outputCCDocEle.getAttribute("Status");
								if(StrStatus.equals("Created"))
								{								
								getControl(this.getOwnerForm(), "lineModifyChargesLnk").setVisible(false);
								getControl(this.getOwnerForm(), "headerModifyChargesLnk").setVisible(false);
																
								YRCPlatformUI.enableAction(YCDAddModifyChargesHeaderPopupAction.ID,false);
								}							
							}
						}
					} catch (ParserConfigurationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			
				}
			}
		
	super.postSetModel(model);
	}
	

	
	public Element getFromModel(String model){
		return this.getModel(model);
	}
	@Override
	public IYRCComposite createPage(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void pageBeingDisposed(String arg0) {
		// TODO Auto-generated method stub
		
	}
}
