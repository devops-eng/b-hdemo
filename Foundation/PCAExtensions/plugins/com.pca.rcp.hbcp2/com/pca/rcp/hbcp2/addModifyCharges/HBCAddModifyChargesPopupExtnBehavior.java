package com.pca.rcp.hbcp2.addModifyCharges;

import java.util.ArrayList;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.pca.rcp.hbcp2.util.HBCPCAConstants;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCExtentionBehavior;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCXmlUtils;

public class HBCAddModifyChargesPopupExtnBehavior extends YRCExtentionBehavior {
	@Override
	public boolean preCommand(YRCApiContext arg0) {
		if(arg0.getApiName().equals("changeOrder")){
			Document changeOrder = arg0.getInputXml();
			Element eleOrder = changeOrder.getDocumentElement();
			if(eleOrder != null){
				Element eleHeaderCharges = YRCXmlUtils.getChildElement(eleOrder, "HeaderCharges");
				if(eleHeaderCharges != null){
					ArrayList eleHeaderChargeList = YRCXmlUtils.getChildren(eleHeaderCharges, "HeaderCharge");
					if(eleHeaderChargeList != null && eleHeaderChargeList.size() > 0){
						for (int i = 0; i < eleHeaderChargeList.size(); i++) {
							Element eleHeaderCharge = (Element)eleHeaderChargeList.get(i);
							if(eleHeaderCharge != null){
								String chargeCategory = eleHeaderCharge.getAttribute("ChargeCategory");
								String chargeName = eleHeaderCharge.getAttribute("ChargeName");
								String chargeAmt = eleHeaderCharge.getAttribute("ChargeAmount");
								if(chargeCategory != null && chargeCategory.equalsIgnoreCase(HBCPCAConstants.SHIPPING_CHARGE_CATEGORY) 
										&& chargeName != null && chargeName.equalsIgnoreCase(HBCPCAConstants.SHIPPING_CHARGE_NAME) && chargeAmt != null){
									Element eleExtn = YRCXmlUtils.getChildElement(eleHeaderCharge, "Extn");
									if(eleExtn == null){
										//eleExtn = YRCXmlUtils.createChild(eleHeaderCharges, "Extn");
										eleExtn = YRCXmlUtils.createChild(eleHeaderCharge, "Extn");
									} 
									eleExtn.setAttribute("ExtnOriginalAmount", chargeAmt);
									break;
								}
							}
						}
					}
				}
			}
			Element eleOrderLines =YRCXmlUtils.getChildElement(eleOrder, "OrderLines");
			ArrayList eleOrderLinelist = YRCXmlUtils.getChildren(eleOrderLines, "OrderLine");
			for(int i=0;i<eleOrderLinelist.size();i++){
				Element eleOrderLine = (Element)eleOrderLinelist.get(i);
				Element eleLineCharges =YRCXmlUtils.getChildElement(eleOrderLine, "LineCharges");
				ArrayList eleLineChargelist = YRCXmlUtils.getChildren(eleLineCharges, "LineCharge");
				for (int j = 0; j < eleLineChargelist.size(); j++) {
					Element eleLineCharge = (Element)eleLineChargelist.get(j);
					String strChgargePerLine = eleLineCharge.getAttribute("ChargePerLine");
					eleLineCharge.setAttribute("ChargePerUnit",strChgargePerLine );
					eleLineCharge.setAttribute("ChargePerLine","0" );
				}
			}
		}
		
		return super.preCommand(arg0);
	}
	@Override
	public void postCommand(YRCApiContext arg0) {
		// TODO Auto-generated method stub
		super.postCommand(arg0);
	}
	@Override
	public void handleApiCompletion(YRCApiContext arg0) {
		// TODO Auto-generated method stub
		super.handleApiCompletion(arg0);
	}
	@Override
	public void postSetModel(String arg0) {
		// TODO Auto-generatElement 
		super.postSetModel(arg0);
	}
	
	//To get the control value 
	public static Control getControl(Composite composite, String name) {
		Control[] ctrl = composite.getChildren();
		for (Control object : ctrl) {
			if (object instanceof Composite) {
				Composite cmp = (Composite) object;
				Control c = getControl(cmp, name);
				if (!YRCPlatformUI.isVoid(c))
					return c;
			} else if (name.equals(object.getData("name")))
				return object;
		}
		return null;
	}
	//Start defect #272
	@Override
	public YRCValidationResponse validateButtonClick(String arg0) { 
		YRCValidationResponse response = null;
		double disc_amt=0.00;
		double shipping_chargeamt=0.00;
	if(arg0 != null && arg0.equals("applyBttn"))
	{
		Element eleChargeDetails=this.getModel("ChargeDetails");
		
		if(eleChargeDetails != null){
			Element eleHeaderCharges = YRCXmlUtils.getChildElement(eleChargeDetails, "HeaderCharges");
			if(eleHeaderCharges != null){
				ArrayList eleHeaderChargeList = YRCXmlUtils.getChildren(eleHeaderCharges, "HeaderCharge");
				if(eleHeaderChargeList != null && eleHeaderChargeList.size() > 0){
					for (int i = 0; i < eleHeaderChargeList.size(); i++) {
						Element eleHeaderCharge = (Element)eleHeaderChargeList.get(i);
						if(eleHeaderCharge != null){
							String chargeCategory = eleHeaderCharge.getAttribute("ChargeCategory");
							String chargeName = eleHeaderCharge.getAttribute("ChargeName");
							String chargeAmt = eleHeaderCharge.getAttribute("ChargeAmount");
							if(chargeCategory != null && chargeCategory.equalsIgnoreCase(HBCPCAConstants.SHIPPING_CHARGE_CATEGORY) 
									&& chargeName != null && chargeName.equalsIgnoreCase(HBCPCAConstants.SHIPPING_CHARGE_NAME) && chargeAmt != null){
								shipping_chargeamt=new Double(chargeAmt);
								
							}
							if(chargeCategory != null && chargeCategory.equalsIgnoreCase(HBCPCAConstants.DISCOUNT_CHARGE_CATEGORY) 
									&& chargeAmt != null)
							{
								 disc_amt= new Double(chargeAmt);
							}
							
						}
					}
				
				
				}
				if(disc_amt>shipping_chargeamt){
					YRCPlatformUI.showError(YRCPlatformUI.getFormattedString("CHARGE ERROR",""), YRCPlatformUI.getFormattedString("SHIPPING_CHARGE&DISCOUNT_ERROR",""));
					response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"");
					return response;
			
				
			}
		}
		
	}
	
		
	}
	return super.validateButtonClick(arg0);
	
	}
	
	//End defect #272
}
