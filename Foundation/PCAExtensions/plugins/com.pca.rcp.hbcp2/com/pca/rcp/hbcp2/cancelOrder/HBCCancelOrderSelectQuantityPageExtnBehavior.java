package com.pca.rcp.hbcp2.cancelOrder;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;

import com.pca.rcp.hbcp2.util.HBCPCAConstants;
import com.pca.rcp.hbcp2.util.HBCPCAUtil;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCExtentionBehavior;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCXmlUtils;
/*******************************************************************************
 * FileName : HBCCancelOrderSelectQuantityPageExtnBehavior.java
 * 
 * Description : This custom code is linked to a Cancel Order Screen of COM, 
 * all cancellation related customization is done through this class. 
 * 
 * Modification Log:
 * ------------------------------------------------------------------------------------
 * Version Date Author Change Log
 * ------------------------------------------------------------------------------------
 * 1.0 07-04-2014 Mohit Sharma Defect 570 - Update Reason Code for Order Cancellation
 *
 ******************************************************************************************/
public class HBCCancelOrderSelectQuantityPageExtnBehavior extends
		YRCExtentionBehavior {
	
	@Override
	public boolean preCommand(YRCApiContext arg0) {
		if("changeOrder".equals(arg0.getApiName())){
			Document docChangeOrder = arg0.getInputXml();
			if(docChangeOrder!=null){
				docChangeOrder.getDocumentElement().setAttribute("Override","N");
				//Defect 570: start :- Adding Note Reason Codes for order cancellation
				Element eleChangeOrder = docChangeOrder.getDocumentElement();
				Element eleNotes = YRCXmlUtils.getChildElement(eleChangeOrder, "Notes");
				
				if(eleNotes != null)
				{
					Element eleNote = YRCXmlUtils.getChildElement(eleNotes, "Note");
					String strNoteText = eleNote.getAttribute("NoteText");

					if (strNoteText.contains(HBCPCAConstants.NOTE_REASON_CUSTOMER_HAS_DIFFERENT_COUPON))
					{
						eleNote.setAttribute("ReasonCode", HBCPCAConstants.REASON_CODE_CUSTOMER_HAS_DIFFERENT_COUPON);
					}
					
					if (strNoteText.contains(HBCPCAConstants.NOTE_REASON_CUSTOMER_PREFERENCE))
					{
						eleNote.setAttribute("ReasonCode", HBCPCAConstants.REASON_CODE_CUSTOMER_PREFERENCE) ;
					}
					
					if (strNoteText.contains(HBCPCAConstants.NOTE_REASON_DIFFERENT_METHOD_OF_PAYMENT))
					{
						eleNote.setAttribute("ReasonCode", HBCPCAConstants.REASON_CODE_DIFFERENT_METHOD_OF_PAYMENT);
					}
					
					if (strNoteText.contains(HBCPCAConstants.NOTE_REASON_DIFFERENT_PRICE))
					{
						eleNote.setAttribute("ReasonCode", HBCPCAConstants.REASON_CODE_DIFFERENT_PRICE);
					}
					
					if (strNoteText.contains(HBCPCAConstants.NOTE_REASON_OTHER))
					{
						eleNote.setAttribute("ReasonCode", HBCPCAConstants.REASON_CODE_OTHER);		
					}
					//Defect 570: End :- Adding Note Reason Codes for order cancellation
					
					//Start:826
					NodeList nOrderLines = null ;
					nOrderLines=eleChangeOrder.getElementsByTagName("OrderLine");
				for (int j=0; j<nOrderLines.getLength();j++ )
				{
					Node nOrderLine = nOrderLines.item(j) ;
					Element eleOrderLine = 	(Element) nOrderLine ;
					Element eNotes= YRCXmlUtils.createChild(eleOrderLine,"Notes");
					eNotes.appendChild(eleNote);
				}
				//End:826
				}
				
				YRCXmlUtils.getString(arg0.getInputXml());
			}
		}
		// TODO Auto-generated method stub
		return super.preCommand(arg0);
	}
	@Override
	public void postCommand(YRCApiContext arg0) {
		// TODO Auto-generated method stub
		super.postCommand(arg0);
	}
	@Override
	public void handleApiCompletion(YRCApiContext arg0) {
		// TODO Auto-generated method stub
		super.handleApiCompletion(arg0);
	}
	@Override
	public void postSetModel(String arg0) {
		// TODO Auto-generatElement 
		super.postSetModel(arg0);
	}
	
	//To get the control value 
	public static Control getControl(Composite composite, String name) {
		Control[] ctrl = composite.getChildren();
		for (Control object : ctrl) {
			if (object instanceof Composite) {
				Composite cmp = (Composite) object;
				Control c = getControl(cmp, name);
				if (!YRCPlatformUI.isVoid(c))
					return c;
			} else if (name.equals(object.getData("name")))
				return object;
		}
		return null;
	}
	 /**
     * Method called when a Confirm button is clicked On Cancel Screen.
     */
    public YRCValidationResponse validateButtonClick(String fieldName) {
		YRCValidationResponse response = null;

		//Get the control value of radBtnPartial
		Button radBtnPartial = (Button)getControl(this.getOwnerForm(),"radBtnPartial");
		boolean flag = radBtnPartial.getSelection();
		//If user selects the Partial Cancel
		if (radBtnPartial != null && flag) {
			Element bChangeOrdDetails = getModel("BeforeChangeOrderDetails");
			if (bChangeOrdDetails != null) {
				String giftWrap = "";
				String checked="";
				double gwUnselectedOrdQty=0.00;
				double gbUnselectedOrdQty=0.00;
				double gwSelCancelQty=0.00;
				double gbSelCancelQty=0.00;
				double totalGBQty=0.00;
				double totalGWQty=0.00;

				double qty=0.00;
				String sProductLine = "";
					
				// Get the Order lines
				Element eOrderLines = YRCXmlUtils.getChildElement(bChangeOrdDetails,"OrderLines");
				NodeList nOrderLine = eOrderLines.getElementsByTagName("OrderLine");
				if (nOrderLine != null & nOrderLine.getLength()>0) {
					for (int oline = 0; oline < nOrderLine.getLength(); oline++) {//oline for loop start
						Element eOrderLine = (Element) nOrderLine.item(oline);
						if(eOrderLine!=null){
							giftWrap=eOrderLine.getAttribute("GiftWrap");
							//Get the total OrderedQty of GiftWrap items  
							if(!YRCPlatformUI.equals(giftWrap, "")&& giftWrap.equalsIgnoreCase("Y")){
								totalGWQty+=Double.parseDouble(YRCXmlUtils.getAttribute(eOrderLine,"OrderedQty"));
								checked=eOrderLine.getAttribute("Checked");
								if(checked!=null){
									//Getting the Quantity of cancel GiftWraps
									if(YRCXmlUtils.getAttribute(eOrderLine, "QtyToCancel")!=""){
										gwSelCancelQty+=Double.parseDouble(YRCXmlUtils.getAttribute(eOrderLine, "QtyToCancel"));
									}
								}
							}
							
							//Get the total OrderedQty of GiftBox line items.
							Element eItem = YRCXmlUtils.getChildElement(eOrderLine,"Item");
							if(eItem!=null){
							sProductLine=eItem.getAttribute("ProductLine");
							if (!YRCPlatformUI.equals(sProductLine, "") && YRCPlatformUI.equals(sProductLine,HBCPCAConstants.GIFT_BOX_ITEM)) {
								totalGBQty+=Double.parseDouble(YRCXmlUtils.getAttribute(eOrderLine,"OrderedQty"));
								checked=eOrderLine.getAttribute("Checked");
								if(checked!=null){
									//Getting the Quantity of cancel GiftBoxes
									if(YRCXmlUtils.getAttribute(eOrderLine, "QtyToCancel")!=""){
										gbSelCancelQty=Double.parseDouble(YRCXmlUtils.getAttribute(eOrderLine, "QtyToCancel"));
									}
								}
							}
						 }
						}
					}//oline for loop end
					
					//Getting the quantity of Unselected GiftBoxes and GiftWraps
					gbUnselectedOrdQty=totalGBQty-gbSelCancelQty;
					gwUnselectedOrdQty=totalGWQty-gwSelCancelQty;

					if(gbUnselectedOrdQty != gwUnselectedOrdQty){
						if (gbUnselectedOrdQty >= 1) {
							if (gwUnselectedOrdQty > gbUnselectedOrdQty) {
								
								 qty = gwUnselectedOrdQty - gbUnselectedOrdQty;
								if (gbUnselectedOrdQty == 0) {
									 qty = gwUnselectedOrdQty - gbUnselectedOrdQty;
										HBCPCAUtil.showOptionDialogBoxWithOK("CANCEL_GIFT_WRAP_TITLE",  YRCPlatformUI
												.getFormattedString("CANCEL_NO_GW_FOR_GB_MSG",String.valueOf(qty)));
										response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"");
										return response;
										} else if(gbUnselectedOrdQty > 1) {
									HBCPCAUtil.showOptionDialogBoxWithOK("CANCEL_GIFT_WRAP_TITLE", YRCPlatformUI
													.getFormattedString("CANCEL_EQ_QTY_GB_FOR_GW_MSG2",new String[]{String.valueOf(qty)}));//3,2
									response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"");
									return response;
								}
							} else {
								 qty = gbUnselectedOrdQty - gwUnselectedOrdQty;
								if (gwUnselectedOrdQty == 0) {
									HBCPCAUtil.showOptionDialogBoxWithOK(
											"CANCEL_GIFT_WRAP_TITLE", YRCPlatformUI.getFormattedString("CANCEL_NO_GB_FOR_GW_MSG",
															new String[]{String.valueOf(qty)}));
									response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"");
									return response;
								} else {
									HBCPCAUtil.showOptionDialogBoxWithOK("CANCEL_GIFT_WRAP_TITLE", YRCPlatformUI
													.getFormattedString("CANCEL_EQ_QTY_GB_FOR_GW_MSG1",new String[]{String.valueOf(qty)}));//1,2
									response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"");
									return response;
								}
							}
					} else {
						 qty = gwUnselectedOrdQty - gbUnselectedOrdQty;
						HBCPCAUtil.showOptionDialogBoxWithOK("CANCEL_GIFT_WRAP_TITLE",  YRCPlatformUI
								.getFormattedString("CANCEL_EQ_QTY_GB_FOR_GW_MSG2",String.valueOf(qty)));
						response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"");
						return response;
						}
					}
				}
			}
		}
		return super.validateButtonClick(fieldName);
    }
}
