	
package com.pca.rcp.hbcp2.changeDeliveryOptions;

/**
 * Created on Feb 10,2014
 *
 */
 
import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCDesktopUI;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCWizard;
import com.yantra.yfc.rcp.YRCWizardExtensionBehavior;
import com.yantra.yfc.rcp.IYRCComposite;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCExtendedTableBindingData;
import com.yantra.yfc.rcp.YRCXmlUtils;
/**
 * This class is extended to manage modifications in delivery options 
 * @author Rajath_Babu
 * Copyright IBM Corp. All Rights Reserved.
 */
 public class HBCChangeDeliveryOptionsWizard extends YRCWizardExtensionBehavior {

	/**
	 * This method initializes the behavior class.
	 */
	public void init() {
		//TODO: Write behavior init here.
	}
 
 	
    public String getExtnNextPage(String currentPageId) {
		//TODO
		return null;
    }
    
    public IYRCComposite createPage(String pageIdToBeShown) {
		//TODO
		return null;
	}
    
    public void pageBeingDisposed(String pageToBeDisposed) {
		//TODO
    }

    /**
     * Called when a wizard page is about to be shown for the first time.
     *
     */
    public void initPage(String pageBeingShown) {
		//TODO
    }
 	
 	
	/**
	 * Method for validating the text box.
     */
    public YRCValidationResponse validateTextField(String fieldName, String fieldValue) {
    	// TODO Validation required for the following controls.
		
		// TODO Create and return a response.
		return super.validateTextField(fieldName, fieldValue);
	}
    
    /**
     * Method for validating the combo box entry.
     */
    public void validateComboField(String fieldName, String fieldValue) {
    	// TODO Validation required for the following controls.
		
		// TODO Create and return a response.
		super.validateComboField(fieldName, fieldValue);
    }
    
    /**
     * Method called when a button is clicked.
     */
    public YRCValidationResponse validateButtonClick(String fieldName) {
    	// TODO Validation required for the following controls.
		
		// TODO Create and return a response.
		return super.validateButtonClick(fieldName);
    }
    
    /**
     * Method called when a link is clicked.
     */
	public YRCValidationResponse validateLinkClick(String fieldName) {
    	// TODO Validation required for the following controls.
		
		// TODO Create and return a response.
		return super.validateLinkClick(fieldName);
	}
	
	/**
	 * Create and return the binding data for advanced table columns added to the tables.
	 */
	 public YRCExtendedTableBindingData getExtendedTableBindingData(String tableName, ArrayList tableColumnNames) {
	 	// Create and return the binding data definition for the table.
		
	 	// The defualt super implementation does nothing.
	 	return super.getExtendedTableBindingData(tableName, tableColumnNames);
	 }
	 
	 /**
		 * R3/2 - This method is used to capture the pick up date to an ExtnPickupDate instead of ReqShipDate
		 * which enables the order to get released irrespective of the ReqShipDate
	**/
	 public boolean preCommand(YRCApiContext apiContext) {
			// TODO Auto-generated method stub
		 YRCWizard wizard=(YRCWizard)YRCDesktopUI.getCurrentPage();
		 String strPageID=wizard.getCurrentPageID();
			if(strPageID.equals("com.yantra.pca.ycd.rcp.tasks.changeDeliveryOptions.wizardpages.YCDChangeDeliveryOptionsFulfillmentSummaryPage"))
			{
				//Start SOM changes Req#19 May'16 Release 
				//Setting attribute identifier to prevent unSchedule status for orderline. 
				int iTotalNoOfAPIs = apiContext.getApiNames().length ;
				for(int iCount=0; iCount < iTotalNoOfAPIs; iCount++){
					if("UnScheduleOrder".equalsIgnoreCase(apiContext.getApiNames()[iCount]) && iCount != 0){
						apiContext.getInputXmls()[iCount].getDocumentElement().setAttribute("ByPass", "True");
					}
				}
				//As ExtnPickupDate is directly updated as a part of date capture 
				//and ReqShipDate remains blank therefore below workaround is not needed
				/*
				if ("changeOrder".equalsIgnoreCase(apiContext.getApiName()))
				{					
					Document docGetSalesOrderDetails = apiContext.getInputXml();
					Element eorderElement = docGetSalesOrderDetails.getDocumentElement();
					if (!YRCPlatformUI.isVoid(eorderElement)){
						Element eOrderLines = YRCXmlUtils.getChildElement(eorderElement, "OrderLines");
						if (!YRCPlatformUI.isVoid(eOrderLines)){
							NodeList nOrderLines = eOrderLines.getElementsByTagName("OrderLine");
							for(int t=0;t<nOrderLines.getLength();t++)
							{
							Element eOrderLine = (Element)nOrderLines.item(t);
							if(!YRCPlatformUI.isVoid(eOrderLine))
							{
								Element eExtnOrderLine = docGetSalesOrderDetails.createElement("Extn");
									String sReqShipDate = eOrderLine.getAttribute("ReqShipDate");
									if(!YRCPlatformUI.isVoid(sReqShipDate)){
										eExtnOrderLine.setAttribute("ExtnPickupDate", sReqShipDate);
										eOrderLine.appendChild(eExtnOrderLine);
										eOrderLine.removeAttribute("ReqShipDate");
										}									
									}								
								}
							}
						}
					}
				*/
				//End SOM changes Req#19 May'16 Release 
				}			
			return super.preCommand(apiContext);
		}
}
