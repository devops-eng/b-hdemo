	
package com.pca.rcp.hbcp2.common.screens;

/**
 * Created on Aug 05,2014
 *
 */
 
import java.util.ArrayList;

import org.w3c.dom.Document;

import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCExtentionBehavior;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCExtendedTableBindingData;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.pca.rcp.hbcp2.util.HBCPCAConstants;
import com.pca.rcp.hbcp2.util.HBCPCAUtil;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCExtentionBehavior;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCXmlUtils;
/**
 * @author mohit_sharma15
 * � Copyright IBM Corp. All Rights Reserved.
 */
 public class HBCYCDSendOrderEmail extends YRCExtentionBehavior{

	/**
	 * This method initializes the behavior class.
	 */
	public void init() {
		//TODO: Write behavior init here.
	}
 
 	
 	
	/**
	 * Method for validating the text box.
     */
    public YRCValidationResponse validateTextField(String fieldName, String fieldValue) {
    	// TODO Validation required for the following controls.
		
		// TODO Create and return a response.
		return super.validateTextField(fieldName, fieldValue);
	}
    
    /**
     * Method for validating the combo box entry.
     */
    public void validateComboField(String fieldName, String fieldValue) {
    	// TODO Validation required for the following controls.
		
		// TODO Create and return a response.
		super.validateComboField(fieldName, fieldValue);
    }
    
    /**
     * Method called when a button is clicked.
     */
    public YRCValidationResponse validateButtonClick(String fieldName) {
    	// TODO Validation required for the following controls.
		
		// TODO Create and return a response.
		return super.validateButtonClick(fieldName);
    }
    
    /**
     * Method called when a link is clicked.
     */
	public YRCValidationResponse validateLinkClick(String fieldName) {
    	// TODO Validation required for the following controls.
		
		// TODO Create and return a response.
		return super.validateLinkClick(fieldName);
	}
	
	/**
	 * Create and return the binding data for advanced table columns added to the tables.
	 */
	 public YRCExtendedTableBindingData getExtendedTableBindingData(String tableName, ArrayList tableColumnNames) {
	 	// Create and return the binding data definition for the table.
		
	 	// The defualt super implementation does nothing.
	 	return super.getExtendedTableBindingData(tableName, tableColumnNames);
	 }
	 
	 
	 public boolean preCommand(YRCApiContext arg0){
		 
		 
		 if("invokeUE".equals(arg0.getApiName())){			 
			 
			 Document docInvokeUE = arg0.getInputXml();
			 // Call Email service 
			 if(docInvokeUE!=null){				 
				 
				 Element eleInvokeUE = docInvokeUE.getDocumentElement();
				 								 
				 Element eleXMLData = YRCXmlUtils.getChildElement(eleInvokeUE, "XMLData");
				 
				 Element eleOrder = YRCXmlUtils.getChildElement(eleXMLData, "Order");
				 
				 String strOrderHeaderKey = eleOrder.getAttribute("OrderHeaderKey");
				 String strMailId = eleOrder.getAttribute("To");
				 
				 eleInvokeUE.removeChild(eleXMLData);
				 
				 docInvokeUE.renameNode(eleInvokeUE, "InvokeUE", "Order");
				 
				 Element eleNewOrderElement = docInvokeUE.getDocumentElement();
				 
				 eleNewOrderElement.setAttribute("To", strMailId);
				 eleNewOrderElement.setAttribute("OrderHeaderKey", strOrderHeaderKey);
					
				 eleNewOrderElement.removeAttribute("DisplayLocalizedFieldInLocale");
				 eleNewOrderElement.removeAttribute("TransactionId");
				 eleNewOrderElement.removeAttribute("UserExit");
				 				 				 
		     }
		}
	return true;
	}
	 
	 public void postCommand(YRCApiContext yrcapicontext)
	 {
		 // Give a pop up when for Email success/Failure
if ("invokeUE".equals(yrcapicontext.getApiName())){
			 
			 Document docEmailServiceOut = yrcapicontext.getOutputXml();
			 Element eleEmailServiceOut = docEmailServiceOut.getDocumentElement();
			 
			 if(eleEmailServiceOut != null){				 
				 String strMessage = eleEmailServiceOut.getAttribute("Message");
				 HBCPCAUtil.showOptionDialogBoxWithOK("Message", strMessage);
				 
			 }			 
		 } 
	 }
}
