
package com.pca.rcp.hbcp2.customerPick;

/**
 * Created on Jun 21,2013
 *
 */

import java.util.ArrayList;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCExtentionBehavior;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCExtendedTableBindingData;
import com.yantra.yfc.rcp.YRCXmlUtils;
import com.yantra.yfc.rcp.internal.YRCApiCaller;
import com.pca.rcp.hbcp2.util.XMLUtil;
/**
 * @author rajath_babu
 * 
 */
public class HBCYCDStorePanel extends YRCExtentionBehavior{

	//R3 - 11-11 Release. Apply Sourcing Rule
	String sApplySourcingRule = "Y";
	String sSourcingRule = "Y";
	/**
	 * This method initializes the behavior class.
	 */
	public void init() {
		//TODO: Write behavior init here.
	}

	/**
	 * Method for validating the text box.
	 */
	public YRCValidationResponse validateTextField(String fieldName, String fieldValue) {
		// TODO Validation required for the following controls.

		// TODO Create and return a response.
		return super.validateTextField(fieldName, fieldValue);
	}

	/**
	 * Method for validating the combo box entry.
	 */
	public void validateComboField(String fieldName, String fieldValue) {
		// TODO Validation required for the following controls.

		// TODO Create and return a response.
		super.validateComboField(fieldName, fieldValue);
	}

	/**
	 * Method called when a button is clicked.
	 */
	public YRCValidationResponse validateButtonClick(String fieldName) {
		// TODO Validation required for the following controls.

		// TODO Create and return a response.
		return super.validateButtonClick(fieldName);
	}

	/**
	 * Method called when a link is clicked.
	 */
	public YRCValidationResponse validateLinkClick(String fieldName) {
		// TODO Validation required for the following controls.

		// TODO Create and return a response.
		return super.validateLinkClick(fieldName);
	}

	/**
	 * Create and return the binding data for advanced table columns added to the tables.
	 */
	public YRCExtendedTableBindingData getExtendedTableBindingData(String tableName, ArrayList tableColumnNames) {
		// Create and return the binding data definition for the table.

		// The defualt super implementation does nothing.
		return super.getExtendedTableBindingData(tableName, tableColumnNames);
	}

	String strLocaleCode="";

	public void postSetModel(String model) 
	{
    	if(model.equals("UserNameSpace"))
    	{
        	strLocaleCode = getModel(model).getAttribute("Localecode");
        	strLocaleCode= (String) strLocaleCode.subSequence(0, 2);
    	}
    	if(model.equals("Node"))
    	{
        	System.out.println("Inside Node model ");
//        	String currentPageID = fetchCurrentPageID();

       		Element eleNode =getModel(model);
        	Element eleOrganization = XMLUtil.getChildElement(eleNode, "Organization");
        	Element eleExtn = XMLUtil.getChildElement(eleOrganization, "Extn"); 
        	Element eleHBCStoreLocationDetailsList = XMLUtil.getChildElement(eleExtn, "HBCStoreLocationDetailsList"); 
        	
				
				NodeList nHBCStoreLocationDetails = eleNode.getElementsByTagName("HBCStoreLocationDetails");
				
				for(int iOLx=0;iOLx<nHBCStoreLocationDetails.getLength();iOLx++ )
				{

				Element eleHBCPickupLocationDetails = (Element) nHBCStoreLocationDetails.item(iOLx);
				String strExtnLocaleCode = eleHBCPickupLocationDetails.getAttribute("ExtnLocaleCode");
				if(strExtnLocaleCode!=null && !strExtnLocaleCode.equals(""))
				{
				strExtnLocaleCode= (String) strExtnLocaleCode.subSequence(0, 2);
				}
					if(!strExtnLocaleCode.equals(strLocaleCode))
						{
							System.out.println("The corresponding line is "+XMLUtil.getElementXMLString(eleHBCPickupLocationDetails));
							XMLUtil.removeChild(eleHBCStoreLocationDetailsList, eleHBCPickupLocationDetails);
						}
				}
			this.repopulateModel("Node");
			this.setControlEditable("extn_PickupLocation", false);
        }

        super.postSetModel(model);

	}
}