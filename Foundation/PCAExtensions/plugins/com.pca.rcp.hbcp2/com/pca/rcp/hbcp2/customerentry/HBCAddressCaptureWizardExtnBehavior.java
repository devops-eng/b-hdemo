/**
 * 
 */
package com.pca.rcp.hbcp2.customerentry;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.xpath.XPathConstants;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.pca.rcp.hbcp2.customerentry.screens.HBCAddressValidationPage;
import com.pca.rcp.hbcp2.customerentry.screens.HBCAddressValidationsPageExtnBehavior;
import com.pca.rcp.hbcp2.orderentry.HBCOrderEntryWizardExtnBehavior;
import com.pca.rcp.hbcp2.payment.HBCCreditCardDetailsExtnBehavior;
import com.pca.rcp.hbcp2.payment.HBCPaymentMethodPopupExtnBehavior;
import com.pca.rcp.hbcp2.payment.HBCPaymentTypeEntryPanelExtnBehavior;
import com.pca.rcp.hbcp2.util.HBCPCAConstants;
import com.pca.rcp.hbcp2.util.HBCPCAUtil;
import com.pca.rcp.hbcp2.util.XMLUtil;
import com.yantra.yfc.rcp.IYRCComposite;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCDesktopUI;
import com.yantra.yfc.rcp.YRCDialog;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCSharedTaskOutput;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCWizard;
import com.yantra.yfc.rcp.YRCWizardExtensionBehavior;
import com.yantra.yfc.rcp.YRCXPathUtils;
import com.yantra.yfc.rcp.YRCXmlUtils;
import com.yantra.yfc.rcp.internal.YRCApiCaller;

/**
 * @author BalaMuraliKrishna
 * 
 *
 */
public class HBCAddressCaptureWizardExtnBehavior extends YRCWizardExtensionBehavior {
	
	public String strPrevAddress="";
	Element eleQASearchResult = null;
	List<String> FakeEmailIdDomains = new ArrayList<String>();
		
	@Override
	public YRCValidationResponse validateButtonClick(String fieldName) {
		YRCValidationResponse response = null;
		if ("extn_Check_Address".equals(fieldName)) {
			validateAddress(response, false);
		}
		if("chkDefaultToShippingAddress".equals(fieldName)){
			String strLineAdd1ShipTo = "",strLineAdd1BillTo="";		
			String strCityShipTo = "",strCityBillTo = "";		
			String strPostalCodeShipTo = "",strPostalCodeBillTo = "";	
			String strStateShipToCombo="",strStateBillToCombo="";

			HBCCustomerEntryWizardExtnBehavior temp = (HBCCustomerEntryWizardExtnBehavior) this.getParentExtnBehavior();
			ArrayList<HBCAddressCaptureWizardExtnBehavior> hbcAddressCaptureExtnBhvrList = temp.getChildExtnBehaviors();
			Button btnCheckAddress = (Button)getControl(this.getParentExtnBehavior().getOwnerForm(), "extn_Chk_Address");
			HBCAddressCaptureWizardExtnBehavior hbcAddressCaptureExtnBhvrShipTo = (HBCAddressCaptureWizardExtnBehavior) hbcAddressCaptureExtnBhvrList.get(0);
				strLineAdd1ShipTo = hbcAddressCaptureExtnBhvrShipTo.getFieldValue("txtAddressLine1");		 
				strCityShipTo = hbcAddressCaptureExtnBhvrShipTo.getFieldValue("txtCity");
				strPostalCodeShipTo = hbcAddressCaptureExtnBhvrShipTo.getFieldValue("txtPostalCode");
				strStateShipToCombo=hbcAddressCaptureExtnBhvrShipTo.getFieldValue("cmbState");

			 HBCAddressCaptureWizardExtnBehavior hbcAddressCaptureExtnBhvrBillTo = (HBCAddressCaptureWizardExtnBehavior) hbcAddressCaptureExtnBhvrList.get(1);
			 	if(hbcAddressCaptureExtnBhvrBillTo.getFieldValue("chkDefaultToShippingAddress").equals("N")){
			 		
			 		strLineAdd1BillTo = hbcAddressCaptureExtnBhvrBillTo.getFieldValue("txtAddressLine1");    	 
			 		strCityBillTo = hbcAddressCaptureExtnBhvrBillTo.getFieldValue("txtCity");
			 		strStateBillToCombo=hbcAddressCaptureExtnBhvrBillTo.getFieldValue("cmbState");
			 		strPostalCodeBillTo = hbcAddressCaptureExtnBhvrBillTo.getFieldValue("txtPostalCode");
			 	}
			 	
			 	
			 if(hbcAddressCaptureExtnBhvrBillTo.getFieldValue("chkDefaultToShippingAddress").equals("N")){
			 	if(!strLineAdd1ShipTo.equals("") && !strLineAdd1BillTo.equals("")){
				    if(!strPostalCodeShipTo.equals("")&&!strPostalCodeBillTo.equals("")){
			 			btnCheckAddress.setEnabled(true);
					}else if(!strStateShipToCombo.equals("")&& !strCityShipTo.equals("")&& 
							!strStateBillToCombo.equals("")&& !strCityBillTo.equals("")){
						btnCheckAddress.setEnabled(true);
					}else if(!strStateShipToCombo.equals("")&& !strCityShipTo.equals("")&& 
							!strPostalCodeBillTo.equals("")){
						btnCheckAddress.setEnabled(true);
					}
					else if(!strPostalCodeShipTo.equals("")&& !strStateBillToCombo.equals("")&& 
							!strCityBillTo.equals("")){
						btnCheckAddress.setEnabled(true);
					}
					else if(!strStateShipToCombo.equals("")&& !strCityShipTo.equals("")&& !strLineAdd1ShipTo.equals("")&&
							!strStateBillToCombo.equals("")&&!strCityBillTo.equals("")&&!strLineAdd1BillTo.equals("")){
						btnCheckAddress.setEnabled(true);
					}else{
						btnCheckAddress.setEnabled(false);
					}
			 	}else{
					btnCheckAddress.setEnabled(false);
				}
			 }else{
			 		if(!strLineAdd1ShipTo.equals("")){
			 			if(!strPostalCodeShipTo.equals("")){
				 			btnCheckAddress.setEnabled(true);
						}else if(!strStateShipToCombo.equals("")&& !strCityShipTo.equals("")){
							btnCheckAddress.setEnabled(true);
						}
						else if(!strStateShipToCombo.equals("")&& !strCityShipTo.equals("")&& !strLineAdd1ShipTo.equals("")){
							btnCheckAddress.setEnabled(true);
						}
						else{
							btnCheckAddress.setEnabled(false);
						}
			 		}else{
						btnCheckAddress.setEnabled(false);
					}
			 }
			 	
		}

    	if ("btnConfirm".equals(fieldName) || "btnNext".equals(fieldName)) {
    		String strFirstName = this.getFieldValue("txtFirstName"); 
    		String strLastName = this.getFieldValue("txtLastName");
    		String strLineAdd1 = this.getFieldValue("txtAddressLine1");
    		String strCity = this.getFieldValue("txtCity");
    		String strState = this.getFieldValue("cmbState"); 
    		String strPostalCode = this.getFieldValue("txtPostalCode");
    		String strCmbCountry = this.getFieldValue("cmbCountry");
    		String strDayTimePhone = this.getFieldValue("txtDayTimePhone");
    		String strtxtEmail = this.getFieldValue("txtEmail");
    		//Start:Defect 746
    		if(("").equals(strState))
    		{
    			strState =this.getFieldValue("txtState");
    		}
    		//End :Defect 746
    		if ((YRCPlatformUI.isVoid(strFirstName))){
				response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"CUSTOMER_FIRST_NAME_NULL_ERR_MSG");
				YRCPlatformUI.showError(YRCPlatformUI.getString("Error"), "CUSTOMER_FIRST_NAME_NULL_ERR_MSG");
				setFocus("txtFirstName");
				return response;
			}
    		if ((YRCPlatformUI.isVoid(strLastName))){
				response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"CUSTOMER_LAST_NAME_NULL_ERR_MSG");
				YRCPlatformUI.showError(YRCPlatformUI.getString("Error"), "CUSTOMER_LAST_NAME_NULL_ERR_MSG");
				setFocus("txtLastName");
				return response;
			}
    		if ((YRCPlatformUI.isVoid(strLineAdd1))){
				response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"SHIPTO_ADD1_NULL_ERR_MSG");
				YRCPlatformUI.showError(YRCPlatformUI.getString("Error"), "SHIPTO_ADD1_NULL_ERR_MSG");
				setFocus("txtAddressLine1");
				return response;
			}
    		
			if ("".equals(strCity)){
					response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"SHIPTO_CITY_NULL_ERR_MSG");
					YRCPlatformUI.showError("Error", "SHIPTO_CITY_NULL_ERR_MSG");
					setFocus("txtCity");
					return response;
			}
    		
    		if ((YRCPlatformUI.isVoid(strPostalCode))){					
				//if(!HBCPCAUtil.validateZipCode(strPostalCode)){
					response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"SHIPTO_ZIPCODE_NULL_ERR_MSG");
					YRCPlatformUI.showError("Error", "SHIPTO_ZIPCODE_NULL_ERR_MSG");
					setFocus("txtPostalCode");
					return response;
			//	}
    		}
    		if ("".equals(strCmbCountry)){
				response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"SHIPTO_COUNTY_NULL_ERR_MSG");
				YRCPlatformUI.showError("Error", "SHIPTO_COUNTY_NULL_ERR_MSG");
				setFocus("cmbCountry");
				return response;
    		}
    		if ("".equals(strState)){
				response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"SHIPTO_STATE_NULL_ERR_MSG");
				YRCPlatformUI.showError("Error", "SHIPTO_STATE_NULL_ERR_MSG");
				setFocus("cmbState");
				return response;
			}
			if ((YRCPlatformUI.isVoid(strDayTimePhone))){
    			response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"CUSTOMER_DAY_PHONE");
				YRCPlatformUI.showError("Error", "CUSTOMER_DAY_PHONE");
				setFocus("txtDayTimePhone");
				return response;
			}
    		if ((YRCPlatformUI.isVoid(strtxtEmail))){
    			response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"CUSTOMER_EMAI_ID_ERR_MSG");
				YRCPlatformUI.showError("Error", "CUSTOMER_EMAI_ID_ERR_MSG");
				setFocus("txtEmail");
				return response;
			}
    		boolean fakeIDExists = false;
    		String[] emailParts = strtxtEmail.split("@");
    		if(emailParts.length >2){
    		String domain = emailParts[1];
    		getFakeEmailIDDomains();
    		for(String strFakeIDDomain : FakeEmailIdDomains){
    			if (strFakeIDDomain.equals(domain)){
    				fakeIDExists = true;
    				break;
    			}	
    		}
    		}
    		if(fakeIDExists){
    			YRCSharedTaskOutput sharedTaskOutput = HBCPCAUtil.showOptionDialogBox(YRCPlatformUI.getString("GWP_MESSAGE"),YRCPlatformUI.getString("DUMMY_EMAIL_ID_VGC"));
				Element ele =sharedTaskOutput.getOutput();
				String strOutput = ele.getAttribute("SelectedOptionKey");
				if(!strOutput.equals("Ok_Button")){
					response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"DUMMY_EMAIL_ID_VGC");
					return response;
				}
    		}
    		response = validateAddress(response, true);	
    		if(response!=null)
    			return response;
    	}
    	return super.validateButtonClick(fieldName);
	}
	public void getFakeEmailIDDomains(){
		Document commonCodeInput = YRCXmlUtils.createDocument("CommonCode");
		commonCodeInput.getDocumentElement().setAttribute("CodeType", "EMAIL_ID");
		
		YRCApiContext yrcApiContext = new YRCApiContext();
		yrcApiContext.setApiName("getCommonCodeList");
		yrcApiContext.setInputXml(commonCodeInput);
		yrcApiContext.setFormId(this.getFormId());
		new YRCApiCaller(yrcApiContext, true).invokeApi();
		handleApiCompletion(yrcApiContext);
	}
	public YRCValidationResponse validateAddress(YRCValidationResponse response, boolean isApply) {
		try {
			String strLineAdd1 = null;
			String strLineAdd2 = null;
			String strCity = null;
			String strState = null;
			String strPostalCode = null;
			String strCmbCountry = null;
			String strCurrentAddressShipTo = "";
			String strUserSelectedAddrShipTo = "";

			strLineAdd1 = this.getFieldValue("txtAddressLine1");
			strLineAdd2 = this.getFieldValue("txtAddressLine2");
			strCity = this.getFieldValue("txtCity");
			strState = this.getFieldValue("cmbState");
			strPostalCode = this.getFieldValue("txtPostalCode");
			strCmbCountry = this.getFieldValue("cmbCountry");
			
			//Start Defect#511 
			//For CA
			Pattern pc = Pattern.compile("[A-Z][0-9][A-Z] [0-9][A-Z][0-9]"); 
			Matcher mPostalCode = pc.matcher(strPostalCode);
		    if (strCmbCountry.equals("CA") && !mPostalCode.matches())
		    {
		    	 YRCPlatformUI.showError(YRCPlatformUI.getFormattedString("ADDRESS ERROR",""), YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_INVALID_ZIPCODE",""));
					response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"");
					return response;
		    }
		    // For LT
			Pattern pc_US = Pattern.compile("\\d{5}((-)?\\d{4})?"); 
			Matcher mPostalCode_US = pc_US.matcher(strPostalCode);
		    if (strCmbCountry.equals("US") && !mPostalCode_US.matches())
		    {
		    	 YRCPlatformUI.showError(YRCPlatformUI.getFormattedString("ADDRESS ERROR",""), YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_INVALID_US_ZIPCODE",""));
					response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"");
					return response;
		    }	    
		    		    
			// End: Defect#511 
			
			strCurrentAddressShipTo = formatAddress(strLineAdd1,
					strLineAdd2, strCity, strState, strPostalCode,strCmbCountry);
			strUserSelectedAddrShipTo = formatUserSelectedAddr(strLineAdd1,
					strLineAdd2, strCity, strState, strPostalCode);
			if (!strCurrentAddressShipTo.equals(strPrevAddress)) {
				Document docQASearchInputShipTo = YRCXmlUtils
						.createFromString(createQASearchSoapRequest(
								strCmbCountry, strCurrentAddressShipTo));
				callQASWebService(docQASearchInputShipTo, strUserSelectedAddrShipTo, strPostalCode);
			}
			else{
				 response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_OK,YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_VALIDATED_ADDRESS",""));
				  if(!isApply){
					  response.setStatusCode(YRCValidationResponse.YRC_VALIDATION_WARNING);
				  YRCPlatformUI.showWarning("Warning", YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_CHECK_ADDRESS_VERIFIED",""));
				  }
				  return response;
			 }

			HBCAddressValidationPage searchResults = new HBCAddressValidationPage(
					new Shell(Display.getDefault().getActiveShell()),
					SWT.NONE, eleQASearchResult, null,true);
			
			int height=0;
			int newHeight=0;
			height = calculateHeightOfPopup(eleQASearchResult);
			if(height>0){
				if(height ==1)
					height=200;
				else
					height=40+200;
			}
			newHeight=height;
			
			YRCDialog dialog = new YRCDialog(searchResults, 500, newHeight,
					YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_ADDRESS_VALIDATION_RESULTS",""), "ApplicationTitleImage");
			dialog.open();
			this.eleQASearchResult = null;
			HBCAddressValidationsPageExtnBehavior hbcAddValPageExtnBhvr=(HBCAddressValidationsPageExtnBehavior)searchResults.getBehavior();			
			if(hbcAddValPageExtnBhvr.getShipToSelected().equals("")){
				  response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_VALIDATED_ADDRESS",""));
				  return response;
			}
			addressParsing(hbcAddValPageExtnBhvr.getShipPostalCode(),hbcAddValPageExtnBhvr.getShipToSelected(),this,"");
			
		} catch (Exception e) {
			YRCPlatformUI.trace(e);
			response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_VALIDATED_ADDRESS",""));
			return response;
		}
		return response;
	}
	private int calculateHeightOfPopup(Element eSearchResult) {
		int height = 0;
		if(eSearchResult!=null){
			Element elPickList=YRCXmlUtils.getChildElement(eSearchResult, "QAPicklist");
			if(elPickList!=null){
				NodeList nlPickListEntry=eSearchResult.getElementsByTagName("PicklistEntry");
				if(nlPickListEntry!=null && nlPickListEntry.getLength()>0){
					height=nlPickListEntry.getLength();
				}
			}
		}
		return height;
	}
	 public void addressParsing(String postalCode, String address,
			HBCAddressCaptureWizardExtnBehavior hbcAddressCaptureExtnBhvr,
			String callfor) throws Exception {

		 String strAddrWithPostalCode="";
	     String strWithAdd1AndAdd2="";	     
	     String strState="";	    
	 	 String strCity="";
	 	 String strAdd1="";
	 	 String strAdd2="";
	 	 
	 	address=address.trim();
	 	strAddrWithPostalCode=address;
	     //separate add1+add2 and city,state,postal code into different strings
	     if(address.contains(",")){
	    	 strAddrWithPostalCode=address.substring(address.lastIndexOf(",")+1,address.length()).trim();
	    	 strWithAdd1AndAdd2=address.substring(0,address.lastIndexOf(",")).trim();
	     }
	     //strike off postal code
	     if(strAddrWithPostalCode.endsWith(postalCode)){
	    	 strAddrWithPostalCode=strAddrWithPostalCode.substring(0,strAddrWithPostalCode.length()-postalCode.length()).trim();			
		  }		    
	     if(strAddrWithPostalCode.length()==2)
	    	 strState=strAddrWithPostalCode.trim();
	     else if(strAddrWithPostalCode.length()>2){
	    	 if(strAddrWithPostalCode.substring(strAddrWithPostalCode.length()-3,strAddrWithPostalCode.length()-2).equals(" ")){
	    		 strState=strAddrWithPostalCode.substring(strAddrWithPostalCode.length()-2,strAddrWithPostalCode.length()).trim();
	    		 strCity=strAddrWithPostalCode.substring(0,strAddrWithPostalCode.length()-2).trim();
	    	 }
	    	 else{
	    		 strCity=strAddrWithPostalCode.trim();
	    	 }
	     }
	    
	      
	     String strAdd1And2[]=strWithAdd1AndAdd2.split(",");
	     if(strAdd1And2.length>0){
	    	 if(strAdd1And2.length==1)
	    	 strAdd1=strAdd1And2[0].trim();
	    	 else
	    	 {
	    		 strAdd1=strAdd1And2[0].trim();
	    		 strAdd2=strAdd1And2[1].trim();
	    	 }
	     }		 
		 		
		 hbcAddressCaptureExtnBhvr.setFieldValue("txtAddressLine1",strAdd1);
		 hbcAddressCaptureExtnBhvr.setFieldValue("txtAddressLine2",strAdd2);
		 hbcAddressCaptureExtnBhvr.setFieldValue("txtCity",strCity);
		 hbcAddressCaptureExtnBhvr.setFieldValue("cmbState",strState);	     
		 hbcAddressCaptureExtnBhvr.setFieldValue("txtPostalCode",postalCode);

		this.strPrevAddress = formatAddress(strAdd1, strAdd2, strCity,
				strState, postalCode,hbcAddressCaptureExtnBhvr.getFieldValue("cmbCountry"));

	}
	/**
	 * This method formats input address in the form of add1,add2,city,state,postalcode
	 * @param strLineAdd1,strLineAdd2,strCity,strState,strPostalCode
	 * @return String
	 * @throws Exception
	 */
	
	public String formatAddress(String strLineAdd1,String strLineAdd2,String strCity,String strState,String strPostalCode,String strCmbCountry)throws Exception{
		String formatedAddress="";
		if(strLineAdd1!=null && !strLineAdd1.equals(""))
			formatedAddress=strLineAdd1+",";
			if(strLineAdd2!=null && !strLineAdd2.equals(""))
				formatedAddress=formatedAddress+strLineAdd2+",";
			if(strCity!=null && !strCity.equals(""))
				formatedAddress=formatedAddress+strCity+",";
			if(strState!=null && !strState.equals(""))
				formatedAddress=formatedAddress+strState+",";							
			if(strPostalCode!=null && !strPostalCode.equals(""))
				formatedAddress=formatedAddress+strPostalCode+",";
			if(strCmbCountry!=null && !strCmbCountry.equals(""))
				formatedAddress=formatedAddress+strCmbCountry;
			
			if(formatedAddress.endsWith(",")){
				formatedAddress=formatedAddress.substring(0,formatedAddress.length()-1);
			 }
		 return formatedAddress;
	 }
	/**
	 * This method formats input address in the form of add1,add2,city state postalcode
	 * @param strLineAdd1,strLineAdd2,strCity,strState,strPostalCode
	 * @return String
	 * @throws Exception
	 */
	
	public String formatUserSelectedAddr(String strLineAdd1,String strLineAdd2,String strCity,String strState,String strPostalCode)throws Exception{
		String formatedAddress="";
		if(strLineAdd1!=null && !strLineAdd1.equals(""))
			formatedAddress=strLineAdd1+", ";
			if(strLineAdd2!=null && !strLineAdd2.equals(""))
				formatedAddress=formatedAddress+strLineAdd2+",";
			if(strCity!=null && !strCity.equals(""))
				formatedAddress=formatedAddress+strCity+" ";
			if(strState!=null && !strState.equals(""))
				formatedAddress=formatedAddress+strState+" ";							
			if(strPostalCode!=null && !strPostalCode.equals(""))
				formatedAddress=formatedAddress+strPostalCode;
						
		 return formatedAddress.trim();
	 }
	/**
	 * This method calls HBCQASDoSearchWebService
	 * @param strLineAdd1,strLineAdd2,strCity,strState,strPostalCode
	 * @return String
	 * @throws Exception
	 */
	
   public void callQASWebService(Document docQASearchInput, String strUserSelectedAddrShipTo,String strPostalCode) throws Exception{
		
	     YRCApiContext yrcApiContext = new YRCApiContext();
	     yrcApiContext.setApiName("HBCQASDoSearchWebService");
	     yrcApiContext.setInputXml(docQASearchInput);
	     yrcApiContext.setFormId(this.getFormId());
	     new YRCApiCaller(yrcApiContext, true).invokeApi();
	     handleApiCompletion(yrcApiContext,strUserSelectedAddrShipTo,strPostalCode);
   }
   public void handleApiCompletion(YRCApiContext ctx) {
		if (ctx.getApiName().equals("getCommonCodeList")) {
			 Element eleFakeEmailIds = ctx.getOutputXml().getDocumentElement();			 
				NodeList FakeEmailIdsList = eleFakeEmailIds.getElementsByTagName("CommonCode");
				for (int i = 0; i < FakeEmailIdsList.getLength(); i++) {
					Element commonCode = (Element) FakeEmailIdsList.item(i);
					FakeEmailIdDomains.add(commonCode.getAttribute("CodeShortDescription"));
			}
		}
	}
   public void handleApiCompletion(YRCApiContext ctx, String strUserSelectedAddrShipTo,String strPCode) throws Exception {
   		if (YRCPlatformUI.equals(ctx.getApiName(), "HBCQASDoSearchWebService")) {
			if(ctx.getInvokeAPIStatus() == -1){


				Document QASearchDoc =  YRCXmlUtils.createDocument("QASearchDoc");
				Element eQASearch =  QASearchDoc.getDocumentElement();
				eQASearch.setAttribute("ProcessingFailed", "Y");
				Element eQAPicklist = YRCXmlUtils.createChild(eQASearch,"QAPicklist");
				  Element ePicklistEntry=YRCXmlUtils.createChild(eQAPicklist,"PicklistEntry");
				  Element ePartialAddress=YRCXmlUtils.createChild(ePicklistEntry,"PartialAddress");				  
				  ePartialAddress.setTextContent(strUserSelectedAddrShipTo);
				  Element ePostalCode=YRCXmlUtils.createChild(ePicklistEntry,"Postcode");
				  ePostalCode.setTextContent(strPCode);
				  Element eScore=YRCXmlUtils.createChild(ePicklistEntry,"Score");
				  eScore.setTextContent("User");
				  this.eleQASearchResult=QASearchDoc.getDocumentElement();
			}else {
			String strPartialAddress="";
			String strPostalCode="";
			String strCity="";
			String strState="";
			
				Document soapRespDoc = ctx.getOutputXml();
						  
			  Element eQASearchDoc=soapRespDoc.getDocumentElement();
			  
			  String strVerifyLevel= eQASearchDoc.getAttribute("VerifyLevel");
			  
			  if(strVerifyLevel.equalsIgnoreCase("InteractionRequired")||strVerifyLevel.equalsIgnoreCase("Verified")){		
				  Element eleQAAddress= YRCXmlUtils.getChildElement(eQASearchDoc,"QAAddress");
				 NodeList nAddressLine= eleQAAddress.getElementsByTagName("AddressLine");
				 if(nAddressLine!=null){
					   for(int i=0; i<nAddressLine.getLength();i++){
						   	Element eAddressLine=(Element)nAddressLine.item(i);
							if(eAddressLine!=null){								
									Element eLine=YRCXmlUtils.getChildElement(eAddressLine, "Line");
									if(eLine!=null){
										String strLabel=YRCXmlUtils.getChildElement(eAddressLine, "Label").getTextContent();
										String strLine=eLine.getTextContent();
										if(strLabel.equalsIgnoreCase("City name")||strLabel.equalsIgnoreCase("Municipality name")){
											strCity=strLine;
										}
										else if(strLabel.equalsIgnoreCase("State code")||strLabel.equalsIgnoreCase("Province code")){
											strState=strLine;											
										}
										else if(i==nAddressLine.getLength()-2){
											strPostalCode=strLine;
										}
										else if(strLine!=null&&!strLine.equals("")&&!strLabel.equalsIgnoreCase("Country")){
											strPartialAddress=strPartialAddress+strLine+",";
											}
							     }								
							  }
					        }					   
					    }
				 strPartialAddress=strPartialAddress+strCity+" "+strState+" "+strPostalCode;				
				
				  Element eQAPicklist=YRCXmlUtils.createChild(eQASearchDoc,"QAPicklist");
				  Element ePicklistEntry=YRCXmlUtils.createChild(eQAPicklist,"PicklistEntry");
				  Element ePartialAddress=YRCXmlUtils.createChild(ePicklistEntry,"PartialAddress");				  
				  ePartialAddress.setTextContent(strPartialAddress);
				  Element ePostalCode=YRCXmlUtils.createChild(ePicklistEntry,"Postcode");
				  ePostalCode.setTextContent(strPostalCode);
			     }
			  
			  NodeList lstPicklistEntry = (NodeList) YRCXPathUtils.evaluate(eQASearchDoc,"QAPicklist/PicklistEntry[@UnresolvableRange='"+ "true" + "']",XPathConstants.NODESET);
			  Element eleQAPicklist=YRCXmlUtils.getChildElement(eQASearchDoc,"QAPicklist");
			  if(lstPicklistEntry!=null){
				  for(int i=0;i<lstPicklistEntry.getLength();i++){				  
					  eleQAPicklist.removeChild((Element)lstPicklistEntry.item(i));				 
				  }
			   }
			  
			  //  Removing "No Matches" picklist entry from response
			  Element elemQAPicklist=YRCXmlUtils.getChildElement(eQASearchDoc,"QAPicklist");
			  if(elemQAPicklist!=null){
			  Element elePicklistEntry=YRCXmlUtils.getChildElement(elemQAPicklist,"PicklistEntry");
			  if(elePicklistEntry!=null){
				  Element ePicklist=YRCXmlUtils.getChildElement(elePicklistEntry,"Picklist");
				  if(ePicklist!=null && ePicklist.getTextContent().equalsIgnoreCase("No matches")){
					  elemQAPicklist.removeChild(elePicklistEntry);
				  }
			  	}
			  }
			  
			  Element eQAPicklist=YRCXmlUtils.getChildElement(eQASearchDoc,"QAPicklist");
			  Element ePicklistEntry=YRCXmlUtils.createChild(eQAPicklist,"PicklistEntry");
			  Element ePartialAddress=YRCXmlUtils.createChild(ePicklistEntry,"PartialAddress");				  
			  ePartialAddress.setTextContent(strUserSelectedAddrShipTo);
			  Element ePostalCode=YRCXmlUtils.createChild(ePicklistEntry,"Postcode");
			  ePostalCode.setTextContent(strPCode);
			  Element eScore=YRCXmlUtils.createChild(ePicklistEntry,"Score");
			  eScore.setTextContent("User");
			  this.eleQASearchResult=eQASearchDoc;
   			}
		    }
		
		  super.handleApiCompletion(ctx);
	  }
   public String createQASearchSoapRequest(String strCountry,String strCurrentAddress)throws Exception{
		
		StringBuffer strBufferSoapReq = new StringBuffer();
		String strLocalisation="";
		String strCntry="";
		String databaseLayout = "Database layout";

		if(strCountry!=null && !strCountry.equals("")){
			
			if(strCountry.equals("US")){
				 strLocalisation="en_US";
				 strCntry="USA";
			}
			else if(strCountry.equals("CA")){
				strLocalisation="en_CA";
				 strCntry="CAN";
				 databaseLayout += " CAN Flattened";  

			}
		}
		strBufferSoapReq.append("<QASearch Localisation=\""+strLocalisation+"\" RequestTag=\"Address Search\"><Country>"+strCntry+"</Country>");
		strBufferSoapReq.append("<Engine Flatten=\"true\" Intensity=\"Exact\" PromptSet=\"Default\" Threshold=\"5000\" Timeout=\"5000\">Verification</Engine>");
		strBufferSoapReq.append("<Layout>"+databaseLayout+"</Layout>");
		strBufferSoapReq.append("<Search>"+strCurrentAddress+"</Search> ");
		strBufferSoapReq.append("<FormattedAddressInPicklist>true</FormattedAddressInPicklist></QASearch>");
		String returnString = strBufferSoapReq.toString();
		returnString = returnString.replaceAll(">null<", "><");
		return returnString;
	}
   

	@Override
	public void postSetModel(String model) {
		Button btnCheckAddress =null;	
		String strLineAdd1 = "";		
		String strCity = "";		
		String strState = ""; 
		String strPostalCode = "";	
		
		if("PersonInfo".equals(model)){
			String strCurrentPageID = "";
			Element elePersonInfo = this.getModel(model);
			String strPersonInfoKey= elePersonInfo.getAttribute("PersonInfoKey");
			if(strPersonInfoKey == null || strPersonInfoKey.length() <=0){
			Composite currentPageComp = YRCDesktopUI.getCurrentPage(); 
			if (currentPageComp instanceof YRCWizard) {
				YRCWizard yrcWizard = (YRCWizard) currentPageComp;
				if(yrcWizard.getExtensionBehavior()!=null && yrcWizard.getExtensionBehavior() instanceof HBCOrderEntryWizardExtnBehavior){
					HBCOrderEntryWizardExtnBehavior behavior = (HBCOrderEntryWizardExtnBehavior)yrcWizard.getExtensionBehavior();
					Element eleWizardInputForOrderEntry = behavior.getModel("WizardInputForOrderEntry");
					if(eleWizardInputForOrderEntry!=null){
						String strEntripriseCode = eleWizardInputForOrderEntry.getAttribute("EnterpriseCode");
						if(HBCPCAConstants.HBC_ENTERPRISE_LT.equals(strEntripriseCode))
							elePersonInfo.setAttribute("Country", "US");
						else if(HBCPCAConstants.HBC_ENTERPRISE_BAY.equals(strEntripriseCode))
							elePersonInfo.setAttribute("Country", "CA");
						Combo cmbCountry = (Combo)HBCPCAUtil.getControl(getOwnerForm(), "", "cmbCountry", false);
						if(cmbCountry != null)
							((Combo)cmbCountry).notifyListeners(SWT.FocusOut, new Event());
					}
					repopulateModel("PersonInfo");
				}
				
			}
			}
			btnCheckAddress = (Button)getControl(this.getOwnerForm(), "extn_Check_Address");
			Text txtAddressLine1 = (Text)getControl(this.getOwnerForm(), "txtAddressLine1");
			Text txtPostalCode = (Text)getControl(this.getOwnerForm(), "txtPostalCode");
			Text txtCity = (Text)getControl(this.getOwnerForm(), "txtCity");
			Combo cmbState = (Combo)HBCPCAUtil.getControl(this.getOwnerForm(),"","cmbState",false);
			
			//String strCmbState = cmbState.getText();
			if(this.getParentExtnBehavior()!=null && this.getParentExtnBehavior() instanceof HBCCreditCardDetailsExtnBehavior){
				HBCCreditCardDetailsExtnBehavior cardDetailsExtnBehavior = (HBCCreditCardDetailsExtnBehavior)this.getParentExtnBehavior();
				if(cardDetailsExtnBehavior.getParentExtnBehavior()!=null && cardDetailsExtnBehavior.getParentExtnBehavior() instanceof HBCPaymentTypeEntryPanelExtnBehavior){
					HBCPaymentTypeEntryPanelExtnBehavior paymentTypeEntryPanelExtnBehavior = (HBCPaymentTypeEntryPanelExtnBehavior) cardDetailsExtnBehavior.getParentExtnBehavior();
					if(paymentTypeEntryPanelExtnBehavior.getParentExtnBehavior()!=null && paymentTypeEntryPanelExtnBehavior.getParentExtnBehavior() instanceof HBCPaymentMethodPopupExtnBehavior){
						HBCPaymentMethodPopupExtnBehavior paymentMethodPopupExtnBehavior = (HBCPaymentMethodPopupExtnBehavior) paymentTypeEntryPanelExtnBehavior.getParentExtnBehavior();
						String tempStrLineAdd1 = null;
						String tempStrLineAdd2 = null;
						String tempStrCity = null;
						String tempStrState = null;
						String tempStrPostalCode = null;
						String tempStrCmbCountry = null;

						tempStrLineAdd1 = elePersonInfo.getAttribute("AddressLine1");
						tempStrLineAdd2 = elePersonInfo.getAttribute("AddressLine2");
						tempStrCity = elePersonInfo.getAttribute("City");
						tempStrState = elePersonInfo.getAttribute("State");
						tempStrPostalCode = elePersonInfo.getAttribute("ZipCode");
						tempStrCmbCountry = elePersonInfo.getAttribute("Country");
						try {
							this.strPrevAddress = this.formatAddress(tempStrLineAdd1, tempStrLineAdd2, tempStrCity, tempStrState, tempStrPostalCode, tempStrCmbCountry);
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						Button radEnterNewAddress = (Button)HBCPCAUtil.getControl(paymentMethodPopupExtnBehavior.getOwnerForm(), "", "radEnterNewAddress", false);
						if(radEnterNewAddress!=null && radEnterNewAddress.getSelection()){
						btnCheckAddress = (Button)HBCPCAUtil.getControl(paymentMethodPopupExtnBehavior.getOwnerForm(), "", "extn_checkaddress", false);

						
						}
					}
				}
			}
			
			cmbState.addFocusListener(new FocusListener(){
				public void focusGained(FocusEvent e) {
				}
				public void focusLost(FocusEvent e) {
					validateData();
				}
			});
			
			txtAddressLine1.addFocusListener(new FocusListener(){
				public void focusGained(FocusEvent e) {
				}
				public void focusLost(FocusEvent e) {
					validateData();
				}
			});
			
			txtPostalCode.addFocusListener(new FocusListener(){

				public void focusGained(FocusEvent e) {
					
				}

				public void focusLost(FocusEvent e) {
					validateData();
				}
				
			});
			
			txtCity.addFocusListener(new FocusListener(){

				public void focusGained(FocusEvent e) {
					
				}

				public void focusLost(FocusEvent e) {
					validateData();
				}
				
			});

			Element ePersonInfo=getModel("PersonInfo");
			if(ePersonInfo!=null){
				strLineAdd1=ePersonInfo.getAttribute("AddressLine1");
				strCity=ePersonInfo.getAttribute("City");
				strState=ePersonInfo.getAttribute("State");
				strPostalCode=ePersonInfo.getAttribute("ZipCode");
		 		if(!strLineAdd1.equals("") ){
					if(!strPostalCode.equals("")){
			 			btnCheckAddress.setEnabled(true);
					}else if(!strState.equals("")&& !strCity.equals("")){
						btnCheckAddress.setEnabled(true);
					}
					else{
						btnCheckAddress.setEnabled(false);
					}
		 		}
			}
		}
		super.postSetModel(model);

	}
	
	public void validateData(){
		String strLineAdd1ShipTo = "",strLineAdd1BillTo="";		
		String strCityShipTo = "",strCityBillTo = "";	
		String strStateShipToCombo="",strStateBillToCombo="";
		String strPostalCodeShipTo = "",strPostalCodeBillTo = "";	
		ArrayList<HBCAddressCaptureWizardExtnBehavior> hbcAddressCaptureExtnBhvrList =null;
		//Button btnChkAddress = (Button)getControl(this.getOwnerForm(), "extn_Check_Address");
		
		if(this.getParentExtnBehavior()!=null && this.getParentExtnBehavior() instanceof HBCCreditCardDetailsExtnBehavior  ){
			hbcAddressCaptureExtnBhvrList = new ArrayList<HBCAddressCaptureWizardExtnBehavior>();
			hbcAddressCaptureExtnBhvrList.add(this);
			hbcAddressCaptureExtnBhvrList.add(null);
		}else if(this.getParentExtnBehavior()!=null && this.getParentExtnBehavior() instanceof HBCCustomerEntryWizardExtnBehavior  ){
			HBCCustomerEntryWizardExtnBehavior temp = (HBCCustomerEntryWizardExtnBehavior) this.getParentExtnBehavior();
			 hbcAddressCaptureExtnBhvrList = temp.getChildExtnBehaviors();
		}else if(this.getParentExtnBehavior()==null){
			hbcAddressCaptureExtnBhvrList = new ArrayList<HBCAddressCaptureWizardExtnBehavior>();
			hbcAddressCaptureExtnBhvrList.add(this);
			hbcAddressCaptureExtnBhvrList.add(null);
		}
		Button btnCheckAddress =null;	
		if(this.getParentExtnBehavior() !=null && this.getParentExtnBehavior() instanceof HBCCustomerEntryWizardExtnBehavior)
			btnCheckAddress = (Button)getControl(this.getParentExtnBehavior().getOwnerForm(), "extn_Chk_Address");
		else if(this.getParentExtnBehavior() !=null && this.getParentExtnBehavior() instanceof HBCCreditCardDetailsExtnBehavior){
			HBCCreditCardDetailsExtnBehavior cardDetailsExtnBehavior = (HBCCreditCardDetailsExtnBehavior) this.getParentExtnBehavior();
			if(cardDetailsExtnBehavior!=null){
			HBCPaymentTypeEntryPanelExtnBehavior paymentTypeEntryPanelExtnBehavior = (HBCPaymentTypeEntryPanelExtnBehavior) cardDetailsExtnBehavior.getParentExtnBehavior();
			if(paymentTypeEntryPanelExtnBehavior!=null){
			HBCPaymentMethodPopupExtnBehavior paymentMethodPopupExtnBehavior = (HBCPaymentMethodPopupExtnBehavior) paymentTypeEntryPanelExtnBehavior.getParentExtnBehavior();
			if(paymentMethodPopupExtnBehavior!=null)
			btnCheckAddress = (Button)HBCPCAUtil.getControl(paymentMethodPopupExtnBehavior.getOwnerForm(), "", "extn_checkaddress", false);
			}
			}
		}
		else
			btnCheckAddress = (Button)getControl(this.getOwnerForm(), "extn_Check_Address");
		HBCAddressCaptureWizardExtnBehavior hbcAddressCaptureExtnBhvrShipTo = (HBCAddressCaptureWizardExtnBehavior) hbcAddressCaptureExtnBhvrList.get(0);
			strLineAdd1ShipTo = hbcAddressCaptureExtnBhvrShipTo.getFieldValue("txtAddressLine1");		 
			strCityShipTo = hbcAddressCaptureExtnBhvrShipTo.getFieldValue("txtCity");
			strStateShipToCombo=hbcAddressCaptureExtnBhvrShipTo.getFieldValue("cmbState");
			strPostalCodeShipTo = hbcAddressCaptureExtnBhvrShipTo.getFieldValue("txtPostalCode");
			
			 HBCAddressCaptureWizardExtnBehavior hbcAddressCaptureExtnBhvrBillTo  = null;
			 
			 if(hbcAddressCaptureExtnBhvrList.get(1)!=null){
				 hbcAddressCaptureExtnBhvrBillTo = (HBCAddressCaptureWizardExtnBehavior) hbcAddressCaptureExtnBhvrList.get(1); 
			 }
		 
		 	
		 if(hbcAddressCaptureExtnBhvrBillTo!=null && hbcAddressCaptureExtnBhvrBillTo.getFieldValue("chkDefaultToShippingAddress").equals("N")){
			 strLineAdd1BillTo = hbcAddressCaptureExtnBhvrBillTo.getFieldValue("txtAddressLine1");    	 
		 		strCityBillTo = hbcAddressCaptureExtnBhvrBillTo.getFieldValue("txtCity");
		 		strStateBillToCombo=hbcAddressCaptureExtnBhvrBillTo.getFieldValue("cmbState");
		 		strPostalCodeBillTo = hbcAddressCaptureExtnBhvrBillTo.getFieldValue("txtPostalCode");
		 		if(!strLineAdd1ShipTo.equals("") && !strLineAdd1BillTo.equals("")){
			 		if(!strPostalCodeShipTo.equals("")&&!strPostalCodeBillTo.equals("")){
			 			btnCheckAddress.setEnabled(true);
					}else if(!strStateShipToCombo.equals("")&& !strCityShipTo.equals("")&& 
							!strStateBillToCombo.equals("")&& !strCityBillTo.equals("")){
						btnCheckAddress.setEnabled(true);
					}else if(!strStateShipToCombo.equals("")&& !strCityShipTo.equals("")&& 
							!strPostalCodeBillTo.equals("")){
						btnCheckAddress.setEnabled(true);
					}
					else if(!strPostalCodeShipTo.equals("")&& !strStateBillToCombo.equals("")&& 
							!strCityBillTo.equals("")){
						btnCheckAddress.setEnabled(true);
					}
					else if(!strStateShipToCombo.equals("")&& !strCityShipTo.equals("")&& !strLineAdd1ShipTo.equals("")&&
							!strStateBillToCombo.equals("")&&!strCityBillTo.equals("")&&!strLineAdd1BillTo.equals("")){
						btnCheckAddress.setEnabled(true);
					}
					else{
						btnCheckAddress.setEnabled(false);
					}
		 		}else{
						btnCheckAddress.setEnabled(false);
		 		}
		 }else{
			 btnCheckAddress.setVisible(true);
		 		if(!strLineAdd1ShipTo.equals("")){
		 			if(!strPostalCodeShipTo.equals("")){
			 			btnCheckAddress.setEnabled(true);
					}else if(!strStateShipToCombo.equals("")&& !strCityShipTo.equals("")){
						btnCheckAddress.setEnabled(true);
					}
					else if(!strStateShipToCombo.equals("")&& !strCityShipTo.equals("")&& !strLineAdd1ShipTo.equals("")){
						btnCheckAddress.setEnabled(true);
					}
					else{
						btnCheckAddress.setEnabled(false);
					}
		 		}else{
						btnCheckAddress.setEnabled(false);
		 		}
		 }
					 	
	
	}
	
	public static Control getControl(Composite composite, String name) {
		Control[] ctrl = composite.getChildren();
		for(Control object:ctrl){
		if (object instanceof Composite) {
		Composite cmp = (Composite) object;
		Control c = getControl(cmp, name);
		if(!YRCPlatformUI.isVoid(c))
		return c;
		}
		else if(name.equals(object.getData("name")))
		return object;
		}
		return null;
		}
	

	public void setFocusOnControl(String controlName) {
		this.setFocus(controlName);
	}
	 public Element getPersonInfoModel(String strPersonInfo) {
			return this.getTargetModel(strPersonInfo);
		}
	 public Element getFromModel(String strPersonInfo) {
			return this.getModel(strPersonInfo);
		}
	@Override
	public IYRCComposite createPage(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void pageBeingDisposed(String arg0) {
		// TODO Auto-generated method stub
		
	}

}
