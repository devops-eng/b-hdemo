/**
 * 
 */
package com.pca.rcp.hbcp2.customerentry;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.xpath.XPathConstants;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.pca.rcp.hbcp2.customerentry.screens.HBCAddressValidationPage;
import com.pca.rcp.hbcp2.customerentry.screens.HBCAddressValidationsPageExtnBehavior;
import com.pca.rcp.hbcp2.util.HBCPCAConstants;
import com.pca.rcp.hbcp2.util.HBCPCAUtil;
import com.pca.rcp.hbcp2.util.XMLUtil;
import com.yantra.yfc.rcp.IYRCComposite;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCDialog;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCSharedTaskOutput;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCWizardExtensionBehavior;
import com.yantra.yfc.rcp.YRCXPathUtils;
import com.yantra.yfc.rcp.YRCXmlUtils;
import com.yantra.yfc.rcp.internal.YRCApiCaller;

/**
 * @author BalaMuraliKrishna
 *
 */
public class HBCCustomerEntryWizardExtnBehavior extends YRCWizardExtensionBehavior {
	List<String> FakeEmailIdDomains = new ArrayList<String>();

	String strPrevAddressShipTo="";
	String strPrevAddressBillTo="";	
	
	Element eleShipToQASearchResult = null;
	Element eleBillToQASearchResult = null;
	HBCAddressValidationPage searchResults=null;
	public void init() {
		this.callGetPreferredLanguageList();
	}

	private void callGetPreferredLanguageList() {

		Element eCommonCode = YRCXmlUtils.createDocument("CommonCode").getDocumentElement();
		eCommonCode.setAttribute("CodeType", "LANGUAGE");

		YRCApiContext ctxApi = new YRCApiContext();
		ctxApi.setApiName("getCommonCodeList");
		ctxApi.setFormId(getFormId());
		ctxApi.setInputXml(eCommonCode.getOwnerDocument());
		this.callApi(ctxApi);
	}
	public void handleApiCompletion(YRCApiContext ctx) {
		if (ctx.getApiName().equals("getCommonCodeList")) {
			 Element eleLangListDetails = ctx.getOutputXml().getDocumentElement();
			 this.setExtentionModel("Extn_PreferredLanguageList", eleLangListDetails);
		}
	}
	
	
	@Override
	public YRCValidationResponse validateButtonClick(String fieldName) {
		YRCValidationResponse response = null;
		if("extn_Chk_Address".equals(fieldName)){
			validateShipToBillToAddress(response, false); 
		}
		if ("btnConfirm".equals(fieldName)|| "btnNext".equals(fieldName)) {
			this.removeFieldInError("btnConfirm");
			
			String strFirstName = this.getFieldValue("txtFirstName"); 
    		String strLastName = this.getFieldValue("txtLastName");
    		String strPreferedLang=this.getFieldValue("extn_PreferredLanguageOrderValues");
    		String strEmailID=this.getFieldValue("txtEmailID");   	
    		String strDayPhone = this.getFieldValue("txtDayPhone");	
      		if ((YRCPlatformUI.isVoid(strFirstName))){
				response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"CUSTOMER_FIRST_NAME_NULL_ERR_MSG");
				YRCPlatformUI.showError("Error", "CUSTOMER_FIRST_NAME_NULL_ERR_MSG");
				setFocus("txtFirstName");
				return response;
			}
    		if ((YRCPlatformUI.isVoid(strLastName))){
				response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"CUSTOMER_LAST_NAME_NULL_ERR_MSG");
				YRCPlatformUI.showError("Error", "CUSTOMER_LAST_NAME_NULL_ERR_MSG");
				setFocus("txtLastName");
				return response;
			}
    		if ((YRCPlatformUI.isVoid(strEmailID))){
				response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"CUSTOMER_EMAI_ID_ERR_MSG");
				YRCPlatformUI.showError("Error", "CUSTOMER_EMAI_ID_ERR_MSG");
				setFocus("txtEmailID");
				return response;
			}
			if ((YRCPlatformUI.isVoid(strDayPhone))){
				response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"CUSTOMER_DAY_PHONE");
				YRCPlatformUI.showError("Error", "CUSTOMER_DAY_PHONE");
				setFocus("txtDayPhone");
				return response;
			}
    		if ((YRCPlatformUI.isVoid(strPreferedLang))){
				response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"PREFERRED_LANGUAGE_ERR_MSG");
				YRCPlatformUI.showError(YRCPlatformUI.getString("Error"), "PREFERRED_LANGUAGE_ERR_MSG");
				setFocus("extn_PreferredLanguageOrderValues");
				return response;
			}
    		ArrayList<HBCAddressCaptureWizardExtnBehavior> hbcAddressCaptureExtnBhvrList = getChildExtnBehaviors();
    		
    		for( HBCAddressCaptureWizardExtnBehavior hbcAddressCaptureExtnBhvr : hbcAddressCaptureExtnBhvrList ){  			
    			if(! hbcAddressCaptureExtnBhvr.getFieldValue("chkDefaultToShippingAddress").equals("Y")) {
    				Element personInfoElement = hbcAddressCaptureExtnBhvr.getPersonInfoModel("PersonInfoTarget");
    				if(! personInfoElement.getAttribute("State").equals(hbcAddressCaptureExtnBhvr.getFieldValue("txtState"))){
    					personInfoElement.setAttribute("State", hbcAddressCaptureExtnBhvr.getFieldValue("txtState"));
    				}
    				if("".equals(personInfoElement.getAttribute("AddressLine1")))
    				{
    					response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"SHIPTO_ADD1_NULL_ERR_MSG");
    					YRCPlatformUI.showError("Error", "SHIPTO_ADD1_NULL_ERR_MSG");
    					hbcAddressCaptureExtnBhvr.setFocusOnControl("txtAddressLine1");
    					return response;
    				}
    				if ("".equals(personInfoElement.getAttribute("City"))){
						response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"SHIPTO_CITY_NULL_ERR_MSG");
						YRCPlatformUI.showError("Error", "SHIPTO_CITY_NULL_ERR_MSG");
						hbcAddressCaptureExtnBhvr.setFocusOnControl("txtCity");
						return response;
					}
    				
    				if ("".equals(personInfoElement.getAttribute("ZipCode"))){					
    						response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"SHIPTO_ZIPCODE_NULL_ERR_MSG");
    						YRCPlatformUI.showError("Error", "SHIPTO_ZIPCODE_NULL_ERR_MSG");
    						hbcAddressCaptureExtnBhvr.setFocusOnControl("txtPostalCode");
    						return response;
    				}
    				if ("".equals(personInfoElement.getAttribute("Country"))){
    					response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"SHIPTO_COUNTY_NULL_ERR_MSG");
    					YRCPlatformUI.showError("Error", "SHIPTO_COUNTY_NULL_ERR_MSG");
    					hbcAddressCaptureExtnBhvr.setFocusOnControl("cmbCountry");
    					return response;
    	    		}
    				if ("".equals(personInfoElement.getAttribute("State"))){
						response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"SHIPTO_STATE_NULL_ERR_MSG");
						YRCPlatformUI.showError("Error", "SHIPTO_STATE_NULL_ERR_MSG");
						hbcAddressCaptureExtnBhvr.setFocusOnControl("txtState");
						return response;
    				}
    				
    			}
    		}
    		boolean fakeIDExists = false;
    		String[] emailParts = strEmailID.split("@");
    		if(emailParts.length >2){
    		String domain = emailParts[1];
    		getFakeEmailIDDomains();
    		for(String strFakeIDDomain : FakeEmailIdDomains){
    			if (strFakeIDDomain.equals(domain)){
    				fakeIDExists = true;
    				break;
    			}	
    		}
    		}
    		if(fakeIDExists){
    			YRCSharedTaskOutput sharedTaskOutput = HBCPCAUtil.showOptionDialogBox(YRCPlatformUI.getString("GWP_MESSAGE"),YRCPlatformUI.getString("DUMMY_EMAIL_ID_VGC"));
				Element ele =sharedTaskOutput.getOutput();
				String strOutput = ele.getAttribute("SelectedOptionKey");
				if(!strOutput.equals("Ok_Button")){
					response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"DUMMY_EMAIL_ID_VGC");
					return response;
				}
    		}
    		response = validateShipToBillToAddress(response, true);	
    		if(response!=null)
    			return response;
		}

		return super.validateButtonClick(fieldName);
	}
	
	public void getFakeEmailIDDomains(){
		Document commonCodeInput = YRCXmlUtils.createDocument("CommonCode");
		commonCodeInput.getDocumentElement().setAttribute("CodeType", "EMAIL_ID");
		
		YRCApiContext yrcApiContext = new YRCApiContext();
		yrcApiContext.setApiName("getCommonCodeList");
		yrcApiContext.setInputXml(commonCodeInput);
		yrcApiContext.setFormId(this.getFormId());
		new YRCApiCaller(yrcApiContext, true).invokeApi();
		handleApiCompletion(yrcApiContext,"EMAIL_ID");
	}
	
	public void handleApiCompletion(YRCApiContext ctx,String callfor) {
		if (ctx.getApiName().equals("getCommonCodeList")&&callfor.equals("EMAIL_ID")) {
			 Element eleFakeEmailIds = ctx.getOutputXml().getDocumentElement();			 
				NodeList FakeEmailIdsList = eleFakeEmailIds.getElementsByTagName("CommonCode");
				for (int i = 0; i < FakeEmailIdsList.getLength(); i++) {
					Element commonCode = (Element) FakeEmailIdsList.item(i);
					FakeEmailIdDomains.add(commonCode.getAttribute("CodeShortDescription"));
			}
		}
	}
	@Override
	public IYRCComposite createPage(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void pageBeingDisposed(String arg0) {
		// TODO Auto-generated method stub
		
	}
	private YRCValidationResponse validateShipToBillToAddress(YRCValidationResponse response, boolean isNextButton) {
		try{
			
			String strLineAdd1 = null;
			String strLineAdd2 = null;
			String strCity = null;
			String strState = null;
			String strPostalCode = null;
			String strCmbCountry =null;
			String strCurrentAddressShipTo="";
			String strCurrentAddressBillTo="";
			String strUserSelectedAddrShipTo="";
    		String strUserSelectedAddrBillTo="";
			 ArrayList<HBCAddressCaptureWizardExtnBehavior> addressCaptureExtnBhvrList = getChildExtnBehaviors();
			 HBCAddressCaptureWizardExtnBehavior hbcAddressCaptureExtnBhvrShipTo = (HBCAddressCaptureWizardExtnBehavior) addressCaptureExtnBhvrList.get(0);
			 strLineAdd1 = hbcAddressCaptureExtnBhvrShipTo.getFieldValue("txtAddressLine1");
			 strLineAdd2 = hbcAddressCaptureExtnBhvrShipTo.getFieldValue("txtAddressLine2");
			 strCity = hbcAddressCaptureExtnBhvrShipTo.getFieldValue("txtCity");
			 strState = hbcAddressCaptureExtnBhvrShipTo.getFieldValue("cmbState"); 
			 strPostalCode = hbcAddressCaptureExtnBhvrShipTo.getFieldValue("txtPostalCode");
			 strCmbCountry = hbcAddressCaptureExtnBhvrShipTo.getFieldValue("cmbCountry"); 
					
			//Start: Defect#511 
				Pattern pc = Pattern.compile("[A-Z][0-9][A-Z] [0-9][A-Z][0-9]"); 
				Matcher mPostalCode = pc.matcher(strPostalCode);
			    if (strCmbCountry.equals("CA") && !mPostalCode.matches())
			    {
			    	 YRCPlatformUI.showError(YRCPlatformUI.getFormattedString("ADDRESS ERROR",""), YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_SHIPTO_INVALID_ZIPCODE",""));
						response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"");
						return response;
			    }
			    //  For LT
				Pattern pc_US = Pattern.compile("\\d{5}((-)?\\d{4})?"); 
				Matcher mPostalCode_US = pc_US.matcher(strPostalCode);
			    if (strCmbCountry.equals("US") && !mPostalCode_US.matches())
			    {
			    	 YRCPlatformUI.showError(YRCPlatformUI.getFormattedString("ADDRESS ERROR",""), YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_SHIPTO_INVALID_US_ZIPCODE",""));
						response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"");
						return response;
			    }	  
				// End: Defect#511 
			 
			 
			 strCurrentAddressShipTo=formatAddress(strLineAdd1,strLineAdd2,strCity,strState,strPostalCode,strCmbCountry);
			 strUserSelectedAddrShipTo=formatUserSelectedAddr(strLineAdd1,strLineAdd2,strCity,strState,strPostalCode);
			   if(!strCurrentAddressShipTo.equals(strPrevAddressShipTo)){
			     Document docQASearchInputShipTo = YRCXmlUtils.createFromString(createQASearchSoapRequest(strCmbCountry,strCurrentAddressShipTo));
			     callQASWebService(docQASearchInputShipTo,"shipTo",strUserSelectedAddrShipTo,strPostalCode);
			   }
			 
			 HBCAddressCaptureWizardExtnBehavior hbcAddressCaptureExtnBhvrBillTo = (HBCAddressCaptureWizardExtnBehavior) addressCaptureExtnBhvrList.get(1);
			 if(hbcAddressCaptureExtnBhvrBillTo.getFieldValue("chkDefaultToShippingAddress").equals("N")){
			 strLineAdd1 = hbcAddressCaptureExtnBhvrBillTo.getFieldValue("txtAddressLine1");
			 strLineAdd2 = hbcAddressCaptureExtnBhvrBillTo.getFieldValue("txtAddressLine2");
			 strCity = hbcAddressCaptureExtnBhvrBillTo.getFieldValue("txtCity");
			 strState = hbcAddressCaptureExtnBhvrBillTo.getFieldValue("cmbState"); 
			 strPostalCode = hbcAddressCaptureExtnBhvrBillTo.getFieldValue("txtPostalCode");
			 strCmbCountry= hbcAddressCaptureExtnBhvrBillTo.getFieldValue("cmbCountry");
			 
			//Start Defect#511 
				Pattern pcDefault = Pattern.compile("[A-Z][0-9][A-Z] [0-9][A-Z][0-9]"); 
				Matcher mPostalCodeDefault = pcDefault.matcher(strPostalCode);
			    if (strCmbCountry.equals("CA") && !mPostalCodeDefault.matches())
			    {
			    	 YRCPlatformUI.showError(YRCPlatformUI.getFormattedString("ADDRESS ERROR",""), YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_BILLTO_INVALID_ZIPCODE",""));
						response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"");
						return response;
			    }
			    // For LT
			    Pattern pc_Default_US = Pattern.compile("\\d{5}((-)?\\d{4})?"); 
				Matcher mPostalCode_Default_US = pc_Default_US.matcher(strPostalCode);
			    if (strCmbCountry.equals("US") && !mPostalCode_Default_US.matches())
			    {
			    	 YRCPlatformUI.showError(YRCPlatformUI.getFormattedString("ADDRESS ERROR",""), YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_BILLTO_INVALID_US_ZIPCODE",""));
						response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"");
						return response;
			    }	  
				// End: Defect#511 
			
			 strCurrentAddressBillTo=formatAddress(strLineAdd1,strLineAdd2,strCity,strState,strPostalCode,strCmbCountry);
			 strUserSelectedAddrBillTo=formatUserSelectedAddr(strLineAdd1,strLineAdd2,strCity,strState,strPostalCode);
			  if(!strCurrentAddressBillTo.equals(strPrevAddressBillTo)){
			    Document docQASearchInputBillTo = YRCXmlUtils.createFromString(createQASearchSoapRequest(strCmbCountry,strCurrentAddressBillTo));
			    callQASWebService(docQASearchInputBillTo,"billTo",strUserSelectedAddrBillTo,strPostalCode);
			  }else{
				  if(strCurrentAddressShipTo.equals(strPrevAddressShipTo)){
				  response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_OK,YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_VALIDATED_ADDRESS",""));
				  if(!isNextButton){
					  response.setStatusCode(YRCValidationResponse.YRC_VALIDATION_WARNING);
				  YRCPlatformUI.showError("Warning", YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_CHECK_ADDRESS_VERIFIED",""));
				  }
				  return response;
				  
			  }
			 }
			 }
			 else if(strCurrentAddressShipTo.equals(strPrevAddressShipTo)){
				 response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_OK,YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_VALIDATED_ADDRESS",""));
				  if(!isNextButton){
					  response.setStatusCode(YRCValidationResponse.YRC_VALIDATION_WARNING);
				  YRCPlatformUI.showWarning("Warning", YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_CHECK_ADDRESS_VERIFIED",""));
				  }
				  return response;
			 }
			
			searchResults = new HBCAddressValidationPage(new Shell(Display.getDefault().getActiveShell()),SWT.NONE,eleShipToQASearchResult,eleBillToQASearchResult,false);
			int shipToHeight=0;
			int billToHeight=0;
			int newHeight=0;
			
			shipToHeight = calculateHeightOfPopup(eleShipToQASearchResult);
			billToHeight = calculateHeightOfPopup(eleBillToQASearchResult);
			
			if(shipToHeight>0){
				if(shipToHeight ==1)
					shipToHeight=200;
				else
					shipToHeight=40+200;
			}
			if(billToHeight>0){
				if(billToHeight ==1)
					billToHeight=200;
				else
					billToHeight=40+200;
			}
			newHeight=shipToHeight+billToHeight;
			
			YRCDialog dialog = new YRCDialog(searchResults, 500, newHeight, YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_ADDRESS_VALIDATION_RESULTS",""), "ApplicationTitleImage");
			dialog.open();
			
			HBCAddressValidationsPageExtnBehavior hbcAddValPageExtnBhvr=(HBCAddressValidationsPageExtnBehavior)searchResults.getBehavior();			
			
			
			if((hbcAddValPageExtnBhvr.getShipToSelected().equals("") && 
					(hbcAddressCaptureExtnBhvrBillTo.getFieldValue("chkDefaultToShippingAddress").equals("N") && hbcAddValPageExtnBhvr.getBillToSelected().equals("")))
					||(hbcAddValPageExtnBhvr.getShipToSelected().equals("") && 
							(hbcAddressCaptureExtnBhvrBillTo.getFieldValue("chkDefaultToShippingAddress").equals("Y") && hbcAddValPageExtnBhvr.getBillToSelected().equals("")))){
				this.eleShipToQASearchResult = null;
				this.eleBillToQASearchResult = null;
				  response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_VALIDATED_ADDRESS",""));
				  return response;
			} 
			if(this.eleShipToQASearchResult != null)
				addressParsing(hbcAddValPageExtnBhvr.getShipPostalCode(),hbcAddValPageExtnBhvr.getShipToSelected(),hbcAddressCaptureExtnBhvrShipTo,"shipTo");
			
			if(this.eleBillToQASearchResult !=null){
				addressParsing(hbcAddValPageExtnBhvr.getBillPostalCode(),hbcAddValPageExtnBhvr.getBillToSelected(),hbcAddressCaptureExtnBhvrBillTo,"billTo");
			}
			this.eleShipToQASearchResult = null;
			this.eleBillToQASearchResult = null;
			}catch(Exception e){
				YRCPlatformUI.trace(e);
				response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_VALIDATED_ADDRESS",""));
				return response;
			}
		return response;
	}

	private int calculateHeightOfPopup(Element eSearchResult) {
		int height = 0;
		Element elPickList=YRCXmlUtils.getChildElement(eSearchResult, "QAPicklist");
		if(elPickList!=null){
			NodeList nlPickListEntry=eSearchResult.getElementsByTagName("PicklistEntry");
			if(nlPickListEntry!=null && nlPickListEntry.getLength()>0){
				height=nlPickListEntry.getLength();
			}
		}
		return height;
	}
	public void handleApiCompletion(YRCApiContext ctx, String callfor,String strUserSelectedAddrShipTo,String strPCode) throws Exception {
		if (YRCPlatformUI.equals(ctx.getApiName(), "HBCQASDoSearchWebService")) {
			if(ctx.getInvokeAPIStatus() == -1){

				Document QASearchDoc =  YRCXmlUtils.createDocument("QASearchDoc");
				Element eQASearch =  QASearchDoc.getDocumentElement();
				eQASearch.setAttribute("ProcessingFailed", "Y");
				Element eQAPicklist = YRCXmlUtils.createChild(eQASearch,"QAPicklist");
				  Element ePicklistEntry=YRCXmlUtils.createChild(eQAPicklist,"PicklistEntry");
				  Element ePartialAddress=YRCXmlUtils.createChild(ePicklistEntry,"PartialAddress");				  
				  ePartialAddress.setTextContent(strUserSelectedAddrShipTo);
				  Element ePostalCode=YRCXmlUtils.createChild(ePicklistEntry,"Postcode");
				  ePostalCode.setTextContent(strPCode);
				  Element eScore=YRCXmlUtils.createChild(ePicklistEntry,"Score");
				  eScore.setTextContent("User");
				  if(callfor.equals("shipTo")){
					  this.eleShipToQASearchResult=QASearchDoc.getDocumentElement();
				  }else{
					  this.eleBillToQASearchResult=QASearchDoc.getDocumentElement();
				  }
			
			}else {
			String strPartialAddress="";
			String strPostalCode="";
			String strCity="";
			String strState="";
			
			  Document soapRespDoc = ctx.getOutputXml();
						  
			  Element eQASearchDoc=soapRespDoc.getDocumentElement();
			  
			  String strVerifyLevel= eQASearchDoc.getAttribute("VerifyLevel");
			  
			  if(strVerifyLevel.equalsIgnoreCase("InteractionRequired")||strVerifyLevel.equalsIgnoreCase("Verified")){		
				  Element eleQAAddress= YRCXmlUtils.getChildElement(eQASearchDoc,"QAAddress");
				 NodeList nAddressLine= eleQAAddress.getElementsByTagName("AddressLine");
				 if(nAddressLine!=null){
					   for(int i=0; i<nAddressLine.getLength();i++){
						   	Element eAddressLine=(Element)nAddressLine.item(i);
							if(eAddressLine!=null){								
									Element eLine=YRCXmlUtils.getChildElement(eAddressLine, "Line");
									if(eLine!=null){
										String strLabel=YRCXmlUtils.getChildElement(eAddressLine, "Label").getTextContent();
										String strLine=eLine.getTextContent();
										if(strLabel.equalsIgnoreCase("City name")||strLabel.equalsIgnoreCase("Municipality name")){
											strCity=strLine;
										}
										else if(strLabel.equalsIgnoreCase("State code")||strLabel.equalsIgnoreCase("Province code")){
											strState=strLine;											
										}
										else if(i==nAddressLine.getLength()-2){
											strPostalCode=strLine;
										}
										else if(strLine!=null&&!strLine.equals("")&&!strLabel.equalsIgnoreCase("Country")){
											strPartialAddress=strPartialAddress+strLine+",";
											}
							     }								
							  }
					        }					   
					    }
				 strPartialAddress=strPartialAddress+strCity+" "+strState+" "+strPostalCode;				
				
				  Element eQAPicklist=YRCXmlUtils.createChild(eQASearchDoc,"QAPicklist");
				  Element ePicklistEntry=YRCXmlUtils.createChild(eQAPicklist,"PicklistEntry");
				  Element ePartialAddress=YRCXmlUtils.createChild(ePicklistEntry,"PartialAddress");				  
				  ePartialAddress.setTextContent(strPartialAddress);
				  Element ePostalCode=YRCXmlUtils.createChild(ePicklistEntry,"Postcode");
				  ePostalCode.setTextContent(strPostalCode);
			     }
			  
			  NodeList lstPicklistEntry = (NodeList) YRCXPathUtils.evaluate(eQASearchDoc,"QAPicklist/PicklistEntry[@UnresolvableRange='"+ "true" + "']",XPathConstants.NODESET);
			  Element eleQAPicklist=YRCXmlUtils.getChildElement(eQASearchDoc,"QAPicklist");
			  if(lstPicklistEntry!=null){
				  for(int i=0;i<lstPicklistEntry.getLength();i++){				  
					  eleQAPicklist.removeChild((Element)lstPicklistEntry.item(i));				 
				  }
			   }
			  
			  //  Removing "No Matches" picklist entry from response
			  Element eQAPicklist=YRCXmlUtils.getChildElement(eQASearchDoc,"QAPicklist");
			  if(eQAPicklist!=null){
			  Element elePicklistEntry=YRCXmlUtils.getChildElement(eQAPicklist,"PicklistEntry");
			  if(elePicklistEntry!=null){
				  Element ePicklist=YRCXmlUtils.getChildElement(elePicklistEntry,"Picklist");
				  if(ePicklist!=null && ePicklist.getTextContent().equalsIgnoreCase("No matches")){
				  eQAPicklist.removeChild(elePicklistEntry);
				  }
			  	}
			  }
			  
			  Element ePicklistEntry=YRCXmlUtils.createChild(eQAPicklist,"PicklistEntry");
			  Element ePartialAddress=YRCXmlUtils.createChild(ePicklistEntry,"PartialAddress");				  
			  ePartialAddress.setTextContent(strUserSelectedAddrShipTo);
			  Element ePostalCode=YRCXmlUtils.createChild(ePicklistEntry,"Postcode");
			  ePostalCode.setTextContent(strPCode);
			  Element eScore=YRCXmlUtils.createChild(ePicklistEntry,"Score");
			  eScore.setTextContent("User");
			  if(callfor.equals("shipTo")){
				  this.eleShipToQASearchResult=eQASearchDoc;
			  }else{
				  this.eleBillToQASearchResult=eQASearchDoc;
			  }
		    }
		}
		
		  super.handleApiCompletion(ctx);
	  }
	
	/**
	 * This method constructs and returns the QAS SOAP request 
	 * @param strCurrentAddress
	 * @return String
	 * @throws Exception
	 */
	
	public String createQASearchSoapRequest(String strCountry,String strCurrentAddress)throws Exception{
	
		StringBuffer strBufferSoapReq = new StringBuffer();
		String strLocalisation="";
		String strCntry="";
		String databaseLayout = "Database layout";
		if(strCountry!=null && !strCountry.equals("")){
			
			if(strCountry.equals("US")){
				 strLocalisation="en_US";
				 strCntry="USA";
			}
			else if(strCountry.equals("CA")){
				strLocalisation="en_CA";
				 strCntry="CAN";
				 // Modified the database layout if country is CA
				 // Fix for 2441
				 databaseLayout += " CAN Flattened";  
			}
		}
		strBufferSoapReq.append("<QASearch Localisation=\""+strLocalisation+"\" RequestTag=\"Address Search\"><Country>"+strCntry+"</Country>");
		strBufferSoapReq.append("<Engine Flatten=\"true\" Intensity=\"Exact\" PromptSet=\"Default\" Threshold=\"5000\" Timeout=\"5000\">Verification</Engine>");
		//strBufferSoapReq.append("<Layout>Database layout</Layout>");
		strBufferSoapReq.append("<Layout>"+databaseLayout+"</Layout>");
		strBufferSoapReq.append("<Search>"+strCurrentAddress+"</Search> ");
		strBufferSoapReq.append("<FormattedAddressInPicklist>true</FormattedAddressInPicklist></QASearch>");
		String returnString = strBufferSoapReq.toString();
		returnString = returnString.replaceAll(">null<", "><");
		return returnString;
	}
	/**
	 * This method formats input address in the form of add1,add2,city,state,postalcode
	 * @param strLineAdd1,strLineAdd2,strCity,strState,strPostalCode
	 * @return String
	 * @throws Exception
	 */
	
	public String formatAddress(String strLineAdd1,String strLineAdd2,String strCity,String strState,String strPostalCode,String strCmbCountry)throws Exception{
		String formatedAddress="";
		if(strLineAdd1!=null && !strLineAdd1.equals(""))
			formatedAddress=strLineAdd1+",";
			if(strLineAdd2!=null && !strLineAdd2.equals(""))
				formatedAddress=formatedAddress+strLineAdd2+",";
			if(strCity!=null && !strCity.equals(""))
				formatedAddress=formatedAddress+strCity+",";
			if(strState!=null && !strState.equals(""))
				formatedAddress=formatedAddress+strState+",";							
			if(strPostalCode!=null && !strPostalCode.equals(""))
				formatedAddress=formatedAddress+strPostalCode+",";
			if(strCmbCountry!=null && !strCmbCountry.equals(""))
				formatedAddress=formatedAddress+strCmbCountry;
			
			if(formatedAddress.endsWith(",")){
				formatedAddress=formatedAddress.substring(0,formatedAddress.length()-1);
			 }
		 return formatedAddress;
	 }
	/**
	 * This method formats input address in the form of add1,add2,city state postalcode
	 * @param strLineAdd1,strLineAdd2,strCity,strState,strPostalCode
	 * @return String
	 * @throws Exception
	 */
	
	public String formatUserSelectedAddr(String strLineAdd1,String strLineAdd2,String strCity,String strState,String strPostalCode)throws Exception{
		String formatedAddress="";
		if(strLineAdd1!=null && !strLineAdd1.equals(""))
			formatedAddress=strLineAdd1+", ";
			if(strLineAdd2!=null && !strLineAdd2.equals(""))
				formatedAddress=formatedAddress+strLineAdd2+",";
			if(strCity!=null && !strCity.equals(""))
				formatedAddress=formatedAddress+strCity+" ";
			if(strState!=null && !strState.equals(""))
				formatedAddress=formatedAddress+strState+" ";							
			if(strPostalCode!=null && !strPostalCode.equals(""))
				formatedAddress=formatedAddress+strPostalCode;
						
		 return formatedAddress.trim();
	 }
	/**
	 * This method calls HBCQASDoSearchWebService
	 * @param strLineAdd1,strLineAdd2,strCity,strState,strPostalCode
	 * @return String
	 * @throws Exception
	 */
	
   public void callQASWebService(Document docQASearchInput, String callfor,String strUserSelectedAddrShipTo,String strPostalCode) throws Exception{
		
	     YRCApiContext yrcApiContext = new YRCApiContext();
	     yrcApiContext.setApiName("HBCQASDoSearchWebService");
	     yrcApiContext.setInputXml(docQASearchInput);
	     yrcApiContext.setFormId(this.getFormId());
	     new YRCApiCaller(yrcApiContext, true).invokeApi();
	     handleApiCompletion(yrcApiContext,callfor,strUserSelectedAddrShipTo,strPostalCode);
   }
   
   /**
	 * This method parses address
	 * @param strLineAdd1,strLineAdd2
	 * @return String
	 * @throws Exception
	 */
	
  public void addressParsing(String postalCode, String address,HBCAddressCaptureWizardExtnBehavior hbcAddressCaptureExtnBhvr,String callfor) throws Exception{
	    
	  	 String strAddrWithPostalCode="";
	     String strWithAdd1AndAdd2="";	     
	     String strState="";	    
	 	 String strCity="";
	 	 String strAdd1="";
	 	 String strAdd2="";
	 	 
	 	address=address.trim();
	 	strAddrWithPostalCode=address;
	     //separate add1+add2 and city,state,postal code into different strings
	     if(address.contains(",")){
	    	 strAddrWithPostalCode=address.substring(address.lastIndexOf(",")+1,address.length()).trim();
	    	 strWithAdd1AndAdd2=address.substring(0,address.lastIndexOf(",")).trim();
	     }
	     //strike off postal code
	     if(strAddrWithPostalCode.endsWith(postalCode)){
	    	 strAddrWithPostalCode=strAddrWithPostalCode.substring(0,strAddrWithPostalCode.length()-postalCode.length()).trim();			
		  }		    
	     if(strAddrWithPostalCode.length()==2)
	    	 strState=strAddrWithPostalCode.trim();
	     else if(strAddrWithPostalCode.length()>2){
	    	 if(strAddrWithPostalCode.substring(strAddrWithPostalCode.length()-3,strAddrWithPostalCode.length()-2).equals(" ")){
	    		 strState=strAddrWithPostalCode.substring(strAddrWithPostalCode.length()-2,strAddrWithPostalCode.length()).trim();
	    		 strCity=strAddrWithPostalCode.substring(0,strAddrWithPostalCode.length()-2).trim();
	    	 }
	    	 else{
	    		 strCity=strAddrWithPostalCode.trim();
	    	 }
	     }
	    
	      
	     String strAdd1And2[]=strWithAdd1AndAdd2.split(",");
	     if(strAdd1And2.length>0){
	    	 if(strAdd1And2.length==1)
	    	 strAdd1=strAdd1And2[0].trim();
	    	 else
	    	 {
	    		 strAdd1=strAdd1And2[0].trim();
	    		 strAdd2=strAdd1And2[1].trim();
	    	 }
	     }		 
		 		
		 hbcAddressCaptureExtnBhvr.setFieldValue("txtAddressLine1",strAdd1);
		 hbcAddressCaptureExtnBhvr.setFieldValue("txtAddressLine2",strAdd2);
		 hbcAddressCaptureExtnBhvr.setFieldValue("txtCity",strCity);
		 hbcAddressCaptureExtnBhvr.setFieldValue("cmbState",strState);	     
		 hbcAddressCaptureExtnBhvr.setFieldValue("txtPostalCode",postalCode);
		 
		 if(callfor.equals("shipTo")){
			  this.strPrevAddressShipTo=formatAddress(strAdd1,strAdd2,strCity,strState,postalCode,hbcAddressCaptureExtnBhvr.getFieldValue("cmbCountry"));
		  }else{
			  this.strPrevAddressBillTo=formatAddress(strAdd1,strAdd2,strCity,strState,postalCode,hbcAddressCaptureExtnBhvr.getFieldValue("cmbCountry"));
		  }				 		  	 
}
}
