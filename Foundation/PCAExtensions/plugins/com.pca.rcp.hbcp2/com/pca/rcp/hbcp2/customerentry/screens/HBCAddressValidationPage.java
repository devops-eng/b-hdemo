package com.pca.rcp.hbcp2.customerentry.screens;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.yantra.yfc.rcp.IYRCComposite;
import com.yantra.yfc.rcp.IYRCPanelHolder;
import com.yantra.yfc.rcp.YRCConstants;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCScrolledCompositeListener;
import com.yantra.yfc.rcp.YRCXmlUtils;

public class HBCAddressValidationPage extends Composite implements
		IYRCComposite {

	private static final String FORM_ID = "com.pca.rcp.hbcp2.customerentry.screens.HBCAddressValidationPage";
	private Composite pnlRoot = null;

	private Composite pnlShipToAddrValidationResults = null;
	private Composite pnlBillToAddrValidationResults = null;
	private Label lblAddrValidationResults = null;
	
	private Composite pnlChoose = null;
	
	private Composite pnlSuggestions=null;
	private Label lblSuggestions=null;
	
	private Label lblChooseOption = null;
	private HBCAddressValidationsPageExtnBehavior myBehavior = null;
	private ScrolledComposite shipToscrollComposite = null;
	private ScrolledComposite billToscrollComposite = null;

	private Composite compositeForButtons = null;

	private Button btnConfirm = null;
	private Button btnClose = null;
	private Button btnRadio = null;
	protected Element eleBillToSearchResult;
	protected Element eleShipToSearchResult;
	String strShipToSelected="";
	String strBillToSelected="";
	boolean commonAddress = false;
	
	private int noOfChildrenShip = 0;
	private int noOfChildrenBill = 0;
	
	public HBCAddressValidationPage(Composite parent, int style,
			Element eleShipToSearchResult,Element eleBillToSearchResult,boolean commonAddress) {
		super(parent, style);
		
		this.eleShipToSearchResult = eleShipToSearchResult;
		this.eleBillToSearchResult = eleBillToSearchResult;
		this.commonAddress = commonAddress;
		initialize();
		
		myBehavior = new HBCAddressValidationsPageExtnBehavior(this, FORM_ID,
				eleShipToSearchResult,eleBillToSearchResult);
		
		//START: fix for 3399
		if(pnlShipToAddrValidationResults != null)
		{
		Control childIteratorShip[] = pnlShipToAddrValidationResults.getChildren();
		noOfChildrenShip = childIteratorShip.length;
		}
		if(pnlBillToAddrValidationResults != null)
		{
		Control childIteratorBill[] = pnlBillToAddrValidationResults.getChildren();
		noOfChildrenBill = childIteratorBill.length; 
		}
		//END: fix for 3399
		
		//This is for adjusting shipToscrollComposite
		if(eleShipToSearchResult!=null){
			adjustScrollPnl(shipToscrollComposite, pnlShipToAddrValidationResults,
				getRootPanel(), true, true);
		}
		
		//This is for adjusting shipToscrollComposite
		if(eleBillToSearchResult!=null){
			adjustScrollPnl(billToscrollComposite, pnlBillToAddrValidationResults,
					getRootPanel(), true, true);
		}
	}

	public void adjustScrollPnl(ScrolledComposite scrPnl, Composite scrChild,
			Composite scrParent, boolean isHScrollReqd, boolean isVScrollReqd) {
		Control childIterator[] = scrChild.getChildren();
		int noOfChildren = childIterator.length;
		int HEIGHT = 0;
		int WIDTH = 5;
		int selectedHeight = 0;

		int selectedPanelHeight = 0;
		/*for (int k = 0; k < noOfChildren; k++) {
			int boundHeight = childIterator[k].getBounds().height;
			int boundWidth = childIterator[k].getBounds().width;
			if (isVScrollReqd) {

				HEIGHT += boundHeight + 1;
				if (WIDTH < boundWidth)
					WIDTH = boundWidth;
			}
			if (!isHScrollReqd)
				continue;
			WIDTH += boundWidth + 5;
			if (HEIGHT < boundHeight)
				HEIGHT = boundHeight;
		}*/
		
		// START : fix for 3399
		if (noOfChildrenShip <= noOfChildrenBill )
		{
			HEIGHT = noOfChildrenBill * 17;
		}
		if (noOfChildrenShip >= noOfChildrenBill)
		{
			HEIGHT = noOfChildrenShip * 17;
		}
		 //END : fix for 3399
		 
		scrPnl.setMinSize(WIDTH, HEIGHT);
		if (isVScrollReqd
				&& (selectedHeight < scrPnl.getOrigin().y || selectedHeight
						+ selectedPanelHeight > scrPnl.getSize().y
						+ scrPnl.getOrigin().y))
			scrPnl.setOrigin(0, selectedHeight);
		scrParent.layout(true, true);
	}

	/**
	 * This method initializes this
	 * 
	 */
	private void initialize() {
		this.setSize(new Point(353, 161));
		GridLayout thislayout = new GridLayout();
		thislayout.horizontalSpacing = 0;
		thislayout.marginWidth = 0;
		thislayout.verticalSpacing = 0;
		thislayout.marginHeight = 0;
		this.setLayout(thislayout);
		createComposite();
	}

	/**
	 * This method initializes composite
	 * 
	 */
	private void createComposite() {
		String strShipToSuggestions=YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_SHIPTO_SUGGESTIONS","");
		String strBillToSuggestions=YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_BILLTO_SUGGESTIONS","");
		String strMatch=YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_MORETHAN_ONE_MATCH","");
		String strChoose=YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_TO_PROCEED_CHOOSE_OPTION","");
		String strShipTo="ShipTo";
		String strBillTo="BillTo";
		int noofShipToPickList=0;
		int noofBillToPickList=0;
		
        if(this.commonAddress){
        	strShipToSuggestions = YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_ADDRESS_SUGGESTIONS","");
        }
		createRootPanel();
		
		noofShipToPickList = getLengthOfPickListEntries(eleShipToSearchResult);
		noofBillToPickList = getLengthOfPickListEntries(eleBillToSearchResult);
		
		if(eleShipToSearchResult!=null){
			String verifyLevel = eleShipToSearchResult.getAttribute("VerifyLevel");
			
			if(noofShipToPickList == 1){
				strMatch = YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_NO_MATCH","");
				if(YRCPlatformUI.equals(eleShipToSearchResult.getAttribute("ProcessingFailed"),"Y")){
					strMatch = YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_PROCESSING_FAILED","");	
				}
				strChoose = YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_TO_PROCEED_CHOOSE_USER_ENTERED","");
			}
			if("StreetPartial".equals(verifyLevel)){
				strChoose = strChoose +" "+ YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_PLEASE_CONFIRM_BUILDINGNUMBER","");
			}else if("PremisesPartial".equals(verifyLevel)){
				strChoose = strChoose + " "+ YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_PLEASE_CONFIRM_APTNUMBER","");
			}
			createSubRootPanel(strShipToSuggestions,strMatch,strChoose,eleShipToSearchResult);
			createShipToScrolledComposite();
			createCompositeForRadioButtons(pnlShipToAddrValidationResults,eleShipToSearchResult,strShipTo);
		}
		if(eleBillToSearchResult!=null){
			String verifyLevel = eleBillToSearchResult.getAttribute("VerifyLevel");
			if(noofBillToPickList ==1){
				strMatch = YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_NO_MATCH","");
				if(YRCPlatformUI.equals(eleBillToSearchResult.getAttribute("ProcessingFailed"),"Y")){
					strMatch = YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_PROCESSING_FAILED","");	
				}
				strChoose = YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_TO_PROCEED_CHOOSE_USER_ENTERED","");
			}
			if("StreetPartial".equals(verifyLevel)){
				strChoose = strChoose + " "+ YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_PLEASE_CONFIRM_BUILDINGNUMBER","");
			}else if("PremisesPartial".equals(verifyLevel)){
				strChoose = strChoose + " "+ YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_PLEASE_CONFIRM_APTNUMBER","");
			}
			createSubRootPanel(strBillToSuggestions,strMatch,strChoose,eleBillToSearchResult);
			createBillToScrolledComposite();
			createCompositeForRadioButtons(pnlBillToAddrValidationResults,eleBillToSearchResult,strBillTo);
		}
		
		createCompositeForButtons();

	}
	private int getLengthOfPickListEntries(Element eSearchResult) {
		int height = 0;
		Element elPickList=YRCXmlUtils.getChildElement(eSearchResult, "QAPicklist");
		if(elPickList!=null){
			NodeList nlPickListEntry=eSearchResult.getElementsByTagName("PicklistEntry");
			if(nlPickListEntry!=null && nlPickListEntry.getLength()>0){
				height=nlPickListEntry.getLength();
			}
		}
		return height;
	}
	private void createSubRootPanel(String strSuggestions,String strMatch, String strChoose,Element eleSearchResult) {
		if(eleSearchResult!=null){
			createSuggestionsPnlAndLbl(strSuggestions);
			addControlsToDataPnl(strMatch,strChoose);
		}
	}

	private void createRootPanel() {
		// GridLayout for main pnlRoot
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginHeight = 5;
		gridLayout.marginWidth = 5;
		gridLayout.verticalSpacing = 0;
		gridLayout.horizontalSpacing = 0;

		// GridData for pnlRoot
		GridData gridDataForpnlRoot = new GridData();
		gridDataForpnlRoot.grabExcessHorizontalSpace = true;
		gridDataForpnlRoot.horizontalAlignment = GridData.FILL;
		gridDataForpnlRoot.verticalAlignment = GridData.FILL;
		gridDataForpnlRoot.grabExcessVerticalSpace = true;

		// Composite for pnlRoot
		pnlRoot = new Composite(this, SWT.INHERIT_NONE);
		pnlRoot.setLayoutData(gridDataForpnlRoot);
		pnlRoot.setLayout(gridLayout);
		pnlRoot.setData(YRCConstants.YRC_CONTROL_NAME, "pnlRoot");
		pnlRoot.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE, "TaskComposite");
	}

	private void createSuggestionsPnlAndLbl(String strSuggestions) {
		// GridData for pnlSuggestions
		GridData gridDataForSuggestions = new GridData();
		gridDataForSuggestions.grabExcessHorizontalSpace = true;
		gridDataForSuggestions.horizontalAlignment = GridData.FILL;
		gridDataForSuggestions.verticalAlignment = GridData.FILL;

		// GridLayout for pnlSuggestions
		GridLayout glForPnlSuggestions = new GridLayout(1, false);
		glForPnlSuggestions.marginHeight = 5;
		glForPnlSuggestions.marginWidth = 10;
		glForPnlSuggestions.verticalSpacing = 1;
		glForPnlSuggestions.horizontalSpacing = 1;
		
		// Composition for pnlSuggestions
		pnlSuggestions = new Composite(pnlRoot, SWT.NONE);
		pnlSuggestions.setLayoutData(gridDataForSuggestions);
		pnlSuggestions.setLayout(glForPnlSuggestions);
		pnlSuggestions.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE,
				"PanelHeader");	
		
		// GridData for lblSuggestions
		GridData gdForlblSuggestions = new GridData();
		gdForlblSuggestions.horizontalAlignment = GridData.FILL;
		gdForlblSuggestions.grabExcessHorizontalSpace = true;
		gdForlblSuggestions.grabExcessVerticalSpace = true;
		gdForlblSuggestions.verticalAlignment = GridData.FILL;

		// lblSuggestions
		lblSuggestions = new Label(pnlSuggestions, SWT.INHERIT_NONE);
		lblSuggestions.setText(strSuggestions);
		lblSuggestions.setLayoutData(gdForlblSuggestions);
		lblSuggestions.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE,
				"PanelHeader");
	}

	private void createShipToScrolledComposite() {
		//GridData for shipToscrollComposite
		GridData gdForShipToScrollComposite = new GridData();
		gdForShipToScrollComposite.horizontalAlignment = GridData.FILL;
		gdForShipToScrollComposite.grabExcessHorizontalSpace = true;
		gdForShipToScrollComposite.grabExcessVerticalSpace = true;
		gdForShipToScrollComposite.verticalAlignment = GridData.FILL;
		gdForShipToScrollComposite.horizontalIndent=0;

		//shipToscrollComposite
		shipToscrollComposite = new ScrolledComposite(pnlRoot, SWT.V_SCROLL);
		shipToscrollComposite.setLayoutData(gdForShipToScrollComposite);
		shipToscrollComposite.setExpandHorizontal(true);
		shipToscrollComposite.setExpandVertical(true);
		shipToscrollComposite.setAlwaysShowScrollBars(true);
		
		createShipToDataPnlComposite();
		
		shipToscrollComposite.addListener(SWT.CURSOR_SIZEALL,
				new YRCScrolledCompositeListener(shipToscrollComposite));
		
		shipToscrollComposite.setLayout(new GridLayout());
		shipToscrollComposite.setContent(pnlShipToAddrValidationResults);
		shipToscrollComposite.setData("name", "scrolledComposite");
	}
	
	private void createBillToScrolledComposite() {
		//GridData for gdForBillToSc
		GridData gdForBillToSc = new GridData();
		gdForBillToSc.horizontalAlignment = GridData.FILL;
		gdForBillToSc.grabExcessHorizontalSpace = true;
		gdForBillToSc.grabExcessVerticalSpace = true;
		gdForBillToSc.verticalAlignment = GridData.FILL;

		//billToscrollComposite
		billToscrollComposite = new ScrolledComposite(pnlRoot, SWT.V_SCROLL);
		billToscrollComposite.setLayoutData(gdForBillToSc);
		billToscrollComposite.setExpandHorizontal(true);
		billToscrollComposite.setExpandVertical(true);
		billToscrollComposite.setAlwaysShowScrollBars(true);
		
		createBillToDataPnlComposite();
		
		billToscrollComposite.addListener(SWT.CURSOR_SIZEALL,
				new YRCScrolledCompositeListener(billToscrollComposite));
		billToscrollComposite.setLayout(new GridLayout());
		billToscrollComposite.setContent(pnlBillToAddrValidationResults);
		billToscrollComposite.setData("name", "billToscrollComposite");
		
	}
	private void createShipToDataPnlComposite(){
		//GridLayout for pnlShipToAddrValidationResults
		GridLayout glPnlShipToAddrValidationResults = new GridLayout();
		glPnlShipToAddrValidationResults.numColumns = 1;
		glPnlShipToAddrValidationResults.verticalSpacing = 1;
		glPnlShipToAddrValidationResults.marginWidth = 0;
		glPnlShipToAddrValidationResults.marginHeight = 0;
		glPnlShipToAddrValidationResults.horizontalSpacing = 5;
		
		//GridData for pnlShipToAddrValidationResults
		GridData gdPnlShipToAddrValidationResults = new GridData();
		gdPnlShipToAddrValidationResults.horizontalAlignment = GridData.FILL;
		gdPnlShipToAddrValidationResults.grabExcessVerticalSpace = true;
		gdPnlShipToAddrValidationResults.grabExcessHorizontalSpace = true;
		gdPnlShipToAddrValidationResults.verticalAlignment = GridData.FILL;

		// Composition for pnlShipToAddrValidationResults
		pnlShipToAddrValidationResults = new Composite(shipToscrollComposite,
				SWT.INHERIT_NONE);
		pnlShipToAddrValidationResults.setLayoutData(gdPnlShipToAddrValidationResults);
		pnlShipToAddrValidationResults.setLayout(glPnlShipToAddrValidationResults);
	}
	
	private void createBillToDataPnlComposite(){
		//GridLayout for pnlBillToAddrValidationResults
		GridLayout glForBillToAddrRes = new GridLayout();
		glForBillToAddrRes.numColumns = 1;
		glForBillToAddrRes.verticalSpacing = 1;
		glForBillToAddrRes.marginWidth = 0;
		glForBillToAddrRes.marginHeight = 0;
		glForBillToAddrRes.horizontalSpacing = 5;
		
		//GridData for pnlBillToAddrValidationResults
		GridData gdPnlBillToAddrResults = new GridData();
		gdPnlBillToAddrResults.horizontalAlignment = GridData.FILL;
		gdPnlBillToAddrResults.grabExcessVerticalSpace = true;
		gdPnlBillToAddrResults.grabExcessHorizontalSpace = true;
		gdPnlBillToAddrResults.verticalAlignment = GridData.FILL;

		// Composition for pnlBillToAddrValidationResults
		pnlBillToAddrValidationResults = new Composite(billToscrollComposite,
				SWT.INHERIT_NONE);
		pnlBillToAddrValidationResults.setLayoutData(gdPnlBillToAddrResults);
		pnlBillToAddrValidationResults.setLayout(glForBillToAddrRes);
	}

	private void addControlsToDataPnl(String strMatch, String strChoose) {
		
		// GridData for pnlChoose
		GridData gridDataForPnlChoose = new GridData();
		gridDataForPnlChoose.grabExcessHorizontalSpace = true;
		gridDataForPnlChoose.horizontalAlignment = GridData.FILL;
		gridDataForPnlChoose.verticalAlignment = GridData.FILL;

		// GridLayout for pnlChoose
		GridLayout glForPnlChoose = new GridLayout(1, false);
		glForPnlChoose.marginHeight = 5;
		glForPnlChoose.marginWidth = 10;
		glForPnlChoose.verticalSpacing = 3;
		glForPnlChoose.horizontalSpacing = 1;
		
		pnlChoose = new Composite(pnlRoot, SWT.NONE);
		pnlChoose.setLayoutData(gridDataForPnlChoose);
		pnlChoose.setLayout(glForPnlChoose);
		pnlChoose.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE,
				"Composite");
		
		// GridData for lblAddrValidationResults
		lblAddrValidationResults = new Label(pnlChoose,
				SWT.INHERIT_NONE);
		lblAddrValidationResults.setText(strMatch);

		// lblChooseOption
		lblChooseOption = new Label(pnlChoose, SWT.WRAP);
		lblChooseOption.setText(strChoose);
		lblChooseOption.setLayoutData(gridDataForPnlChoose);

	}
	
	private void createCompositeForRadioButtons(Composite pnlAddrValidationResults,Element eleSearchResult , String strShipToOrBillTo) {

		// GridLayout for glForRadioBtn
		GridLayout glForRadioBtn = new GridLayout();
		glForRadioBtn.numColumns = 1;

		// GridData for gdForRadioBtn
		GridData gdForRadioBtn = new GridData();
		gdForRadioBtn.horizontalAlignment = GridData.FILL;
		//gdForRadioBtn.grabExcessVerticalSpace = true;
		gdForRadioBtn.grabExcessHorizontalSpace = true;

		if (eleSearchResult != null) {
				Element eleQAPicklist = YRCXmlUtils.getChildElement(
						eleSearchResult, "QAPicklist");
				if (eleQAPicklist != null) {
					displayQASAddressResponse( gdForRadioBtn,pnlAddrValidationResults,
						eleQAPicklist,strShipToOrBillTo);
					
				}
		}
		
	}

	private void displayQASAddressResponse(GridData gdForRadioBtn, Composite compositeForRdBtn,Element eleQAPicklist,String strShipToOrBillTo) {
		String strPostCode="";
		String strPrompt="";
		String strPartialAddress="";
		String strScore="";
			
		Element ePrompt = YRCXmlUtils.getChildElement(
				eleQAPicklist, "Prompt");
				NodeList nPicklistEntry = eleQAPicklist
				.getElementsByTagName("PicklistEntry");
		if (nPicklistEntry != null) {
			
			for (int j = 0; j < nPicklistEntry.getLength(); j++) {
				Element ePicklistEntry = (Element) nPicklistEntry
						.item(j);
				if (ePicklistEntry != null) {
					Element eleScore=YRCXmlUtils.getChildElement(ePicklistEntry, "Score");
					Element elePartialAddress = YRCXmlUtils
							.getChildElement(ePicklistEntry,
									"PartialAddress");
					if (elePartialAddress != null) {
						strPartialAddress = elePartialAddress.getTextContent();
					}
					
					if(strPartialAddress!=null && strPartialAddress.length() >0){
						Element elePostcode = YRCXmlUtils
							.getChildElement(ePicklistEntry,
									"Postcode");
					if (elePostcode != null) {
						strPostCode = elePostcode.getTextContent();
					}
					btnRadio = new Button(
							compositeForRdBtn, SWT.RADIO);
					btnRadio.setLayoutData(gdForRadioBtn);
					if (elePartialAddress != null) {
						btnRadio.setData("yrc:customType", "Label");
						btnRadio.setData("AddressType", strShipToOrBillTo);
						btnRadio.setData("PostalCode", strPostCode);
						btnRadio.setText(strPartialAddress);
						if(eleScore!=null){
							strScore=eleScore.getTextContent();
							if(strScore.equals("User")){
								btnRadio.setText(strPartialAddress+YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_USER_ENTERED",""));
							}else{
								btnRadio.setText(strPartialAddress);
							}
						}
					}
					
					btnRadio.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
						public void widgetSelected(
								org.eclipse.swt.events.SelectionEvent e) {
							Button ctrl = (Button) e.widget;
	                        Boolean isSelected = ((Button) e.widget).getSelection();

	                         if(isSelected){
	                        	 String strAddressType = (String)ctrl.getData("AddressType");
	                        	 String strPostCode=(String)ctrl.getData("PostalCode");
	                        	 if("ShipTo".equals(strAddressType)){
	                        		 strShipToSelected=ctrl.getText();
	                        		 strShipToSelected = strShipToSelected.replace(YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_USER_ENTERED",""), "");
	                        		 myBehavior.setShipPostalCode(strPostCode);
	                        	 }
	                        	 else if("BillTo".equals(strAddressType)){
	                        		 strBillToSelected=ctrl.getText();
	                        		 strBillToSelected = strBillToSelected.replace(YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_USER_ENTERED",""), "");
	                        		 myBehavior.setBillPostalCode(strPostCode);
	                        	 }
	                         }
	                         
	                         if(eleShipToSearchResult!=null & eleBillToSearchResult!=null){
	                        	 if(!strShipToSelected.equals("") && !strBillToSelected.equals(""))
	                        		 btnConfirm.setEnabled(true);
	                        	 else{
	                        		 btnConfirm.setEnabled(false);
	                        	 }
	                         }else{
	                        	if( eleShipToSearchResult !=null ){
	                        		if( !strShipToSelected.equals("")){
	                        			btnConfirm.setEnabled(true);	
	                        		}else{
	                        			btnConfirm.setEnabled(false);
	                        		}
	                        	}
	                        	if( eleBillToSearchResult !=null) {
	                        		if( !strBillToSelected.equals("")){
	                        			btnConfirm.setEnabled(true);	
	                        		}else{
	                        			btnConfirm.setEnabled(false);
	                        		}
	                        	}
	                        		
	                         }
						}
					});
				 }
				
				}
			}
		}
		if (ePrompt != null) {
			strPrompt = ePrompt.getTextContent();

		}
	}

	/**
	 * This method initializes compositeForButtons
	 * 
	 */
	private void createCompositeForButtons() {

		// GridData for gdForbtnConfirm
		GridData gdForBtnConfirm = new GridData();
		gdForBtnConfirm.horizontalAlignment = GridData.END;
		gdForBtnConfirm.grabExcessHorizontalSpace = true;
		gdForBtnConfirm.verticalAlignment = GridData.CENTER;

		// GridLayout for glForCompositeButtons
		GridLayout glForCompositeButtons = new GridLayout();
		glForCompositeButtons.numColumns = 2;

		// GridData for gdForCompositeButtons
		GridData gdForCompositeButtons = new GridData();
		gdForCompositeButtons.verticalSpan = 3;
		gdForCompositeButtons.verticalAlignment = GridData.FILL;
		gdForCompositeButtons.grabExcessHorizontalSpace = true;
		gdForCompositeButtons.grabExcessVerticalSpace = false;
		gdForCompositeButtons.horizontalAlignment = GridData.FILL;

		// Composite for compositeForButtons
		compositeForButtons = new Composite(getRootPanel(), SWT.INHERIT_NONE);
		compositeForButtons.setLayoutData(gdForCompositeButtons);
		compositeForButtons.setLayout(glForCompositeButtons);
		compositeForButtons.setFocus();
		compositeForButtons.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE,
				"TaskComposite");

		// btnConfirm
		btnConfirm = new Button(compositeForButtons, SWT.INHERIT_NONE);
		btnConfirm.setText(YRCPlatformUI.getFormattedString("CONFIRM_BTN_LABEL", ""));
		btnConfirm.setLayoutData(gdForBtnConfirm);
		btnConfirm.setEnabled(false);
		
		btnConfirm
		.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			public void widgetSelected(
					org.eclipse.swt.events.SelectionEvent e) {
				myBehavior.setShipToSelected(strShipToSelected);
				myBehavior.setBillToSelected(strBillToSelected);
				pnlRoot.getShell().close();
			}
		});

		// btnClose
		btnClose = new Button(compositeForButtons, SWT.INHERIT_NONE);
		btnClose.setText(YRCPlatformUI.getFormattedString("CLOSE_BTN_LABEL", ""));
		btnClose
				.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
					public void widgetSelected(
							org.eclipse.swt.events.SelectionEvent e) {
						pnlRoot.getShell().close();
					}
				});
	}
	public String getFormId() {
		return HBCAddressValidationPage.FORM_ID;
	}

	public Composite getRootPanel() {
		return this.pnlRoot;
	}
	public Element getOutputElement() {
		return myBehavior.getOutputElementMethod();
	}

	public Object getBehavior() {
		return this.myBehavior;
	}

	@Override
	public String getHelpId() {
		return null;
	}

	@Override
	public IYRCPanelHolder getPanelHolder() {
		return null;
	}

}
