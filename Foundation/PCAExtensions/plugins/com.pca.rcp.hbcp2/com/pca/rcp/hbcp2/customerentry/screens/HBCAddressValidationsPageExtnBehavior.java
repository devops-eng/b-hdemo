package com.pca.rcp.hbcp2.customerentry.screens;

import java.util.ArrayList;

import org.eclipse.swt.widgets.Composite;
import org.w3c.dom.Element;

import com.yantra.yfc.rcp.YRCBehavior;

public class HBCAddressValidationsPageExtnBehavior extends YRCBehavior {
	ArrayList<String> arrAddrSearchResults ;
	String shipToSelected="";
	String billToSelected="";
	String shipPostalCode="";
	String billPostalCode="";

	public HBCAddressValidationsPageExtnBehavior(Composite arg0) {
		super(arg0);
	}

	public HBCAddressValidationsPageExtnBehavior(Composite arg0, String arg1) {
		super(arg0, arg1);

	}

	public HBCAddressValidationsPageExtnBehavior(Composite arg0, String arg1,
			Element eleShipToQASearchResult,Element eleBillToSearchResult) {
		super(arg0, arg1, eleBillToSearchResult);
		
	}
	
	public Element getOutputElementMethod() {
		return this.getModel("SelectedQASearchResult");
	}

	public String getShipToSelected() {
		return shipToSelected;
	}

	public void setShipToSelected(String shipToSelected) {
		this.shipToSelected = shipToSelected;
	}

	public String getBillToSelected() {
		return billToSelected;
	}

	public void setBillToSelected(String billToSelected) {
		this.billToSelected = billToSelected;
	}

	public String getShipPostalCode() {
		return shipPostalCode;
	}

	public void setShipPostalCode(String shipPostalCode) {
		this.shipPostalCode = shipPostalCode;
	}

	public String getBillPostalCode() {
		return billPostalCode;
	}

	public void setBillPostalCode(String billPostalCode) {
		this.billPostalCode = billPostalCode;
	}

	
	
}
