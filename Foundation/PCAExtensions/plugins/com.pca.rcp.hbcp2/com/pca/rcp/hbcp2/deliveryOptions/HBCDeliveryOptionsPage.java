
package com.pca.rcp.hbcp2.deliveryOptions;

/**
 * Created on Jun 21,2013
 *
 */

import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.pca.rcp.hbcp2.util.HBCPCAConstants;
import com.pca.rcp.hbcp2.util.HBCPCAUtil;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCExtentionBehavior;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCExtendedTableBindingData;
import com.yantra.yfc.rcp.YRCXmlUtils;
import com.yantra.yfc.rcp.internal.YRCApiCaller;
import com.pca.rcp.hbcp2.util.XMLUtil;
/**
 * @author rajath_babu
 * R3
 * This class is created to update the Sourcing Rules
 * as per the Fulfillment type of the Order
 *
 * Feb Release:
 * This class was modified to check if the ShipNode is same
 * for all OrderLines for a Pickup Order
 *
 */
public class HBCDeliveryOptionsPage extends YRCExtentionBehavior{

	//R3 - 11-11 Release. Apply Sourcing Rule
	String sApplySourcingRule = "Y";
	String sSourcingRule = "Y";
	/**
	 * This method initializes the behavior class.
	 */
	public void init() {
		//TODO: Write behavior init here.
	}

	/**
	 * Method for validating the text box.
	 */
	public YRCValidationResponse validateTextField(String fieldName, String fieldValue) {
		// TODO Validation required for the following controls.

		// TODO Create and return a response.
		return super.validateTextField(fieldName, fieldValue);
	}

	/**
	 * Method for validating the combo box entry.
	 */
	public void validateComboField(String fieldName, String fieldValue) {
		// TODO Validation required for the following controls.

		// TODO Create and return a response.
		super.validateComboField(fieldName, fieldValue);
	}

	/**
	 * Method called when a button is clicked.
	 */
	public YRCValidationResponse validateButtonClick(String fieldName) {
		// TODO Validation required for the following controls.
	//Start: Defect#480
		YRCValidationResponse response = null;
		if(fieldName != null && fieldName.equals("btnApplyDelivery"))
		{
			YRCPlatformUI.showError(YRCPlatformUI.getFormattedString("ADDRESS CHANGE ERROR",""), YRCPlatformUI.getFormattedString("LINE_LEVEL_ADDRESS_CHANGE_ERROR",""));
			response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"");
			return response;
		}
		//End: Defect#480
		// TODO Create and return a response.
		return super.validateButtonClick(fieldName);
	}

	/**
	 * Method called when a link is clicked.
	 */
	public YRCValidationResponse validateLinkClick(String fieldName) {
		// TODO Validation required for the following controls.

		// TODO Create and return a response.
		return super.validateLinkClick(fieldName);
	}

	/**
	 * Create and return the binding data for advanced table columns added to the tables.
	 */
	public YRCExtendedTableBindingData getExtendedTableBindingData(String tableName, ArrayList tableColumnNames) {
		// Create and return the binding data definition for the table.

		// The defualt super implementation does nothing.
		return super.getExtendedTableBindingData(tableName, tableColumnNames);
	}

	/**
	 * R3 - PreCommand is implemented to stamp AllocationRuleID and FulfillmentType
	 * in the input of getShipToFulfillment API
	 **/
	public boolean preCommand(YRCApiContext yrcapicontext) {
		if ("getShipToFulfillment".equalsIgnoreCase(yrcapicontext.getApiName()))
		{
			Document docShipToFulfillmentInput = yrcapicontext.getInputXml();
			Element eleShipToFulfillment = docShipToFulfillmentInput.getDocumentElement();
			String sOrganizationCode = eleShipToFulfillment.getAttribute("OrganizationCode");
			if ("LT".equalsIgnoreCase(sOrganizationCode))
			{
				eleShipToFulfillment.setAttribute("AllocationRuleID", HBCPCAConstants.LT_ALLOCATION_RULE_ID);

			}
			if ("BAY".equalsIgnoreCase(sOrganizationCode))
			{
				eleShipToFulfillment.setAttribute("AllocationRuleID", HBCPCAConstants.BAY_ALLOCATION_RULE_ID);
			}
			Element elePromiseLines = YRCXmlUtils.getChildElement(eleShipToFulfillment, "PromiseLines");
			if(elePromiseLines!=null)
			{
				//R3 - 11-11 Release. Apply Sourcing Rule
				org.w3c.dom.Document docHBCGetCommonCodeList = YRCXmlUtils.createDocument("CommonCode");
				Element eleCommonCodeInput = docHBCGetCommonCodeList.getDocumentElement();
				//eleCommonCodeInput.setAttribute("OrganizationCode", HBCSOMConstants.DEFAULT_ORGANIZATION_CODE);
				eleCommonCodeInput.setAttribute("CodeType", HBCPCAConstants.SOURCING_RULE);
				eleCommonCodeInput.setAttribute("CodeValue", HBCPCAConstants.SOURCING_CHANNEL);
				YRCApiContext context = new YRCApiContext();
				context.setFormId(super.getFormId());
				context.setApiName("HBCGetSourcingRule");
				context.setInputXml(docHBCGetCommonCodeList);
				new YRCApiCaller(context, true).invokeApi();
				handleApiCompletion(context);

				NodeList nPromiseLine = elePromiseLines.getElementsByTagName("PromiseLine");
				for(int p=0;p<nPromiseLine.getLength();p++)
				{
					Element ePromiseLine = (Element)nPromiseLine.item(p);
					if(ePromiseLine!=null)
					{
						if ("LT".equalsIgnoreCase(sOrganizationCode))
						{
          					  //R3 - 11-11 Release. Apply Sourcing Rule
          					  if (HBCPCAConstants.APPLY_SOURCING_RULE.equalsIgnoreCase(sApplySourcingRule))
          					  {
          						  ePromiseLine.setAttribute("FulfillmentType", sSourcingRule);
          					  }
          					  else
          					  {
          						  ePromiseLine.setAttribute("FulfillmentType", HBCPCAConstants.FT_TYPE_STANDARD);
          					  }

						}
						if ("BAY".equalsIgnoreCase(sOrganizationCode))
						{
          					  //R3 - 11-11 Release. Apply Sourcing Rule
          					  if (HBCPCAConstants.APPLY_SOURCING_RULE.equalsIgnoreCase(sApplySourcingRule))
          					  {
          						  ePromiseLine.setAttribute("FulfillmentType", sSourcingRule);
          					  }
          					  else
          					  {
          						  ePromiseLine.setAttribute("FulfillmentType", HBCPCAConstants.FT_TYPE_STANDARD);
          					  }
						}
					}
				}

			}
		}
		return super.preCommand(yrcapicontext);
	}


	/**
	 * R3 - This method is implemented to handle the completion of custom API calls
	 *
	**/
	//R3 - 11-11 Release. Apply Sourcing Rule
	public void handleApiCompletion(YRCApiContext ctx) {
		if (YRCPlatformUI.equals(ctx.getApiName(), "HBCGetSourcingRule")) {
			Document docGetSourcingRule = ctx.getOutputXml();
			Element eleGetSourcingRule = docGetSourcingRule.getDocumentElement();
			NodeList nGetSourcingRuleList = eleGetSourcingRule.getElementsByTagName("CommonCode");
			for (int p = 0; p < nGetSourcingRuleList.getLength(); p++) {
				Element eleCommonCode = (Element) nGetSourcingRuleList.item(p);
				sApplySourcingRule = eleCommonCode.getAttribute("CodeShortDescription");
				if (HBCPCAConstants.APPLY_SOURCING_RULE.equalsIgnoreCase(sApplySourcingRule))
				{
					sSourcingRule = eleCommonCode.getAttribute("CodeLongDescription");
				}
			}
		}
	}

	//@Override
	//-- Kumar :: start :: Check (1/1) :: Check for same Pickup Store
	/*
	public void postSetModel(String namespace) {
		// TODO Auto-generated method stub
		//-- Method call to verify same ship node check.
		if(namespace.equalsIgnoreCase("PICKUP_NODE_CHECK")){
			pickUpNodeCheck();
		}
		super.postSetModel(namespace);
	}
	*/
	public boolean validatePickNode(String sPickUpNodeCheck){
		if(sPickUpNodeCheck.equalsIgnoreCase("PICKUP_NODE_CHECK")){
			return pickUpNodeCheck();
		}
		return true;
	}


	/**
	 * This method checks whether Pick Store value is same for All Order Lines
	 *
	 * @return boolean
	 */

	public boolean pickUpNodeCheck() {
		// TODO Auto-generated method stub
		String sRefShipNode = null;
		String sRefDeliverytMethod = null;
		String sShipNode = null;
		Element eleOrder = this.getModel("getSalesOrderDetails");
		Element eleOrderLine = null;
		NodeList nOrderLineList = eleOrder.getElementsByTagName("OrderLine");
		for(int l=0; l<nOrderLineList.getLength(); l++){
			eleOrderLine = (Element)nOrderLineList.item(l);
			if(l == 0){
				sRefShipNode = eleOrderLine.getAttribute("ShipNode");
				sRefDeliverytMethod = eleOrderLine.getAttribute("DeliveryMethod");
			}
			if(!XMLUtil.isVoid(sRefDeliverytMethod) && sRefDeliverytMethod.equalsIgnoreCase("PICK")){
				sShipNode = eleOrderLine.getAttribute("ShipNode");
				if(!XMLUtil.isVoid(sShipNode) && !sRefShipNode.equalsIgnoreCase(sShipNode)){
					return false;
				}
			}
		}
		return true;
	}
	//-- Kumar :: end :: Check (1/1) :: Check for same Pickup Store

}
