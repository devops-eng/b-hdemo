package com.pca.rcp.hbcp2.itemdetails;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.pca.rcp.hbcp2.util.HBCPCAUtil;
import com.yantra.yfc.rcp.IYRCComposite;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCFormatUtils;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCSharedTaskOutput;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCWizardExtensionBehavior;
import com.yantra.yfc.rcp.YRCXmlUtils;

public class HBCItemDetailsWizardExtnBehavior extends
		YRCWizardExtensionBehavior {

	// public static String FormId =
	// "com.hbc.rcp.pca.itemdetails.HBCItemDetailsWizardExtnBehavior";
	@Override
	protected Element getExtentionModel(String arg0) {
		// TODO Auto-generated method stub
		return super.getExtentionModel(arg0);
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
	}

	@Override
	public void postCommand(YRCApiContext arg0) {
		super.postCommand(arg0);
	}

	@Override
	public void postSetModel(String modelName) {
		Date currentDate = new Date();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String sCurrentDate = formatter.format(currentDate);
		String strItemId = "";
		String strOrganizationCode = "";
//Start: defect 284
		String sEndDateActiveQryType = "GE";
		String sStartDateActiveQryType = "LE";
//End: defect 284
		if (YRCPlatformUI.equals(modelName, "Results")) {
			Element docUser = getModel("UserNameSpace");

			Element docResults = getModel(modelName);
			if (HBCPCAUtil.isStoreApplication()) {
				strOrganizationCode = YRCXmlUtils.getAttribute(docUser,
						"BusinessKey");
			} else {
				strOrganizationCode = YRCXmlUtils.getAttribute(docResults,
						"OrganizationCode");
			}
			strItemId = YRCXmlUtils.getAttribute(docResults, "ItemID");
			/*
			 * strOrganizationCode = YRCXmlUtils.getAttribute(docResults,
			 * "OrganizationCode");
			 */

			Document inputxml = YRCXmlUtils.createDocument("PricelistLine");
			Element elePricelistLine = inputxml.getDocumentElement();
			YRCXmlUtils.setAttribute(elePricelistLine, "ItemID", strItemId);
			YRCXmlUtils.setAttribute(elePricelistLine,
					"SellerOrganizationCode", strOrganizationCode);
			YRCXmlUtils.setAttribute(elePricelistLine, "EndDateActiveQryType",
					sEndDateActiveQryType);
			YRCXmlUtils.setAttribute(elePricelistLine,
					"StartDateActiveQryType", sStartDateActiveQryType);
			YRCXmlUtils.setAttribute(elePricelistLine, "EndDateActive",
					sCurrentDate);
			YRCXmlUtils.setAttribute(elePricelistLine, "StartDateActive",
					sCurrentDate);
			YRCXmlUtils.setAttribute(elePricelistLine, "PricingStatus",
					"ACTIVE");

			YRCApiContext yrcApiContext = new YRCApiContext();
			yrcApiContext.setApiName("getPricelistLineList");
			yrcApiContext.setInputXml(inputxml);
			yrcApiContext.setFormId(this.getFormId());
			this.callApi(yrcApiContext);

		}

		super.postSetModel(modelName);
	}

	@Override
	public boolean preCommand(YRCApiContext arg0) {
		// TODO Auto-generated method stub
		return super.preCommand(arg0);
	}

	@Override
	protected void setExtentionModel(String arg0, Element arg1) {
		// TODO Auto-generated method stub
		super.setExtentionModel(arg0, arg1);
	}

	public void handleApiCompletion(YRCApiContext Ctx) {
		String sExtnMarkDownFlag = "";
		String sListPrice = YRCPlatformUI.getFormattedCurrency((YRCFormatUtils
				.getFormattedValue("UnitPrice", "0")), "USD");
		String sUnitPrice = YRCPlatformUI.getFormattedCurrency((YRCFormatUtils
				.getFormattedValue("UnitPrice", "0")), "USD");
		String sExtnMarkDownFlagDesc = "";
		if (YRCPlatformUI.equals(Ctx.getApiName(), "getPricelistLineList")) {
			Document docPriceList = Ctx.getOutputXml();
			Element ePricelistLineList = docPriceList.getDocumentElement();
			if (ePricelistLineList != null) {
				ArrayList nPricelistLine = YRCXmlUtils.getChildren(
						ePricelistLineList, "PricelistLine");
				// call createLeastPriceListLine method...
				if (nPricelistLine.size() > 0) {
					Element eLeastPriceListLine = createLeastPriceListLine(nPricelistLine);

					sListPrice = YRCPlatformUI
							.getFormattedCurrency((YRCFormatUtils
									.getFormattedValue("UnitPrice", YRCXmlUtils
											.getAttribute(eLeastPriceListLine,
													"ListPrice"))), "USD");
					sUnitPrice = YRCPlatformUI
							.getFormattedCurrency((YRCFormatUtils
									.getFormattedValue("UnitPrice", YRCXmlUtils
											.getAttribute(eLeastPriceListLine,
													"UnitPrice"))), "USD");

					Element eExtn = YRCXmlUtils.getChildElement(
							eLeastPriceListLine, "Extn");
					sExtnMarkDownFlag = YRCXmlUtils.getAttribute(eExtn,
							"ExtnMarkDownFlag");

					if (sExtnMarkDownFlag == null || YRCPlatformUI.equals(sExtnMarkDownFlag, "R") || YRCPlatformUI.equals(sExtnMarkDownFlag, "")) {
						sExtnMarkDownFlagDesc = YRCPlatformUI.getFormattedString("Regular","");
						
					} else if (YRCPlatformUI.equals(sExtnMarkDownFlag, "S")) {
						sExtnMarkDownFlagDesc = YRCPlatformUI.getFormattedString("Sale","");
					} else if (YRCPlatformUI.equals(sExtnMarkDownFlag, "L")) {
						sExtnMarkDownFlagDesc = YRCPlatformUI.getFormattedString("Clearance","");
					} else if (YRCPlatformUI.equals(sExtnMarkDownFlag, "V")) {
						sExtnMarkDownFlagDesc = YRCPlatformUI.getFormattedString("Smart_Value","");
					}

				}
			}

			Document outputPriceList = YRCXmlUtils
					.createDocument("PricelistLineList");
			Element elePricelistLineList = outputPriceList.getDocumentElement();
			Element elePricelistLine = YRCXmlUtils.createChild(
					elePricelistLineList, "PricelistLine");
		
				YRCXmlUtils.setAttribute(elePricelistLine, "ListPrice",
						sListPrice);
				YRCXmlUtils.setAttribute(elePricelistLine, "UnitPrice",
						sUnitPrice);
				YRCXmlUtils.setAttribute(elePricelistLine, "ExtnMarkDownFlag",
						sExtnMarkDownFlag);
				// TODO add brackets using Formatter.....
				YRCXmlUtils.setAttribute(elePricelistLine,
						"ExtnMarkDownFlagDesc", YRCPlatformUI.getFormattedString("Markdown", new String[]{sExtnMarkDownFlagDesc}));

				setExtentionModel("extnPriceList", elePricelistLineList);
			
		}
		super.handleApiCompletion(Ctx);
	}

	/**
	 * @param pricelistLine
	 * @return
	 */
	private Element createLeastPriceListLine(ArrayList npricelistLine) {
		// TODO Auto-generated method stub

		Element ePricelistLineLeast = (Element) npricelistLine.get(0);
		Double dunitPrice = Double.parseDouble(YRCXmlUtils.getAttribute(
				ePricelistLineLeast, "UnitPrice"));
		Double dtemp = 0.0;

		for (int i = 0; i < npricelistLine.size(); i++) {
			Element ePricelistLine = (Element) npricelistLine.get(i);

			dtemp = Double.parseDouble(YRCXmlUtils.getAttribute(ePricelistLine,
					"UnitPrice"));
			if (dtemp <= dunitPrice) {
				dunitPrice = dtemp;
				ePricelistLineLeast = (Element) npricelistLine.get(i);
			}

		}

		return ePricelistLineLeast;
	}

	@Override
	public IYRCComposite createPage(String arg0) {
		// TODO Auto-generated method stub

		return null;
	}

	@Override
	public void pageBeingDisposed(String arg0) {
		// TODO Auto-generated method stub

	}
//	 Start - Zero Dollar CR Changes - Defect # 1856
	public YRCValidationResponse validateButtonClick(String fieldName) {
		YRCValidationResponse response = new YRCValidationResponse();	
		if(fieldName.equals("buttonAddToOrder")|| fieldName.equals("buttonCreateOrder")){
		Element ele=this.getModel("extnPriceList")	;
		if(ele!=null){
			Element elePricelistLineList=YRCXmlUtils.getChildElement(ele, "PricelistLine");
			if(elePricelistLineList!=null){
			String strUnitPrice=elePricelistLineList.getAttribute("UnitPrice");
			if((strUnitPrice==null || strUnitPrice.equals("")|| strUnitPrice.equals("0.00")|| strUnitPrice.equals("0")|| strUnitPrice.equals("$0.00") || strUnitPrice.equals("$0,00"))) 
			{
				YRCSharedTaskOutput out = HBCPCAUtil.showOptionDialogBoxWithYesNo("ITEM_ERROR",
				"ZERO_DOLLAR_PRICE_ITEM");				
				Element eSelectedOption = out.getOutput();
				String sSelectedOptionKey = "";
				sSelectedOptionKey = YRCXmlUtils.getAttribute(
				eSelectedOption, "SelectedOptionKey");				
				//HBCPCAUtil.showOptionDialogBoxWithOK("Item Error", "Item with $0 price can not be added!");
				if (sSelectedOptionKey != null
						&& sSelectedOptionKey.equals("OPTION_NO")) {
					response = new YRCValidationResponse(
							YRCValidationResponse.YRC_VALIDATION_ERROR, "");
				}
				else{
					response = new YRCValidationResponse(
							YRCValidationResponse.YRC_VALIDATION_OK, "");
				}
							
			}
			}
		}
		}
		return response;
		}
//	 End - Zero Dollar CR Changes - Defect # 1856
}
