package com.pca.rcp.hbcp2.itemsearch.wizard;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.pca.rcp.hbcp2.util.HBCPCAUtil;
import com.yantra.yfc.rcp.IYRCComposite;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCSharedTaskOutput;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCWizardExtensionBehavior;
import com.yantra.yfc.rcp.YRCXmlUtils;
import com.yantra.yfc.rcp.internal.YRCApiCaller;

public class HBCItemSearchWizardExtnBehavior extends YRCWizardExtensionBehavior {
	Document docMultiApi = null;
	Element eleMultiApi = null;

	@Override
	public void postCommand(YRCApiContext yrcapicontext) {

		Date currentDate = new Date();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String sCurrentDate = formatter.format(currentDate);
		String strItemId = "";
		String strOrganizationCode = "";

		String sEndDateActiveQryType = "GE";
		String sStartDateActiveQryType = "LE";

		// TODO Auto-generated method stub
		if (YRCPlatformUI.equals(yrcapicontext.getApiName(),
				"GetItemListForOrderingNoAvail")|| YRCPlatformUI.equals(yrcapicontext.getApiName(),
				"searchCatalogIndexNoAvail")) {

			Document docGetItemListForOrder = yrcapicontext.getOutputXml();
			Element eleGetItemListForOrder = docGetItemListForOrder
					.getDocumentElement();

			strOrganizationCode = YRCXmlUtils.getAttribute(
					eleGetItemListForOrder, "CallingOrganizationCode");
			if (strOrganizationCode != null && !strOrganizationCode.equals("")) {
				Document inputxmlForMultiApi = YRCXmlUtils
						.createDocument("MultiApi");
				Element eMultiApi = inputxmlForMultiApi.getDocumentElement();

				/*ArrayList<Element> nItems = YRCXmlUtils.getChildren(
						eleGetItemListForOrder, "Item");*/
//				Defect#2420 -- Commenting the above lines and adding the below one
				NodeList nItems = eleGetItemListForOrder.getElementsByTagName("Item");

				for (int i = 0; i < nItems.getLength(); i++) {

					//Element eItem = (Element) nItems.get(i);
//					Defect#2420 -- Commenting the above line and adding the below one
					Element eItem = (Element)nItems.item(i);
					strItemId = YRCXmlUtils.getAttribute(eItem, "ItemID");

					Element eApi = YRCXmlUtils.createChild(eMultiApi, "API");
					YRCXmlUtils.setAttribute(eApi, "Name",
							"getPricelistLineList");

					Element eInput = YRCXmlUtils.createChild(eApi, "Input");
					Element elePricelistLine = YRCXmlUtils.createChild(eInput,
							"PricelistLine");

					YRCXmlUtils.setAttribute(elePricelistLine, "ItemID",
							strItemId);
					YRCXmlUtils.setAttribute(elePricelistLine,
							"SellerOrganizationCode", strOrganizationCode);
					YRCXmlUtils.setAttribute(elePricelistLine,
							"EndDateActiveQryType", sEndDateActiveQryType);
					YRCXmlUtils.setAttribute(elePricelistLine,
							"StartDateActiveQryType", sStartDateActiveQryType);
					YRCXmlUtils.setAttribute(elePricelistLine, "EndDateActive",
							sCurrentDate);
					YRCXmlUtils.setAttribute(elePricelistLine,
							"StartDateActive", sCurrentDate);
					YRCXmlUtils.setAttribute(elePricelistLine, "PricingStatus",
							"ACTIVE");
				}

				YRCApiContext yrcApiContext = new YRCApiContext();
				yrcApiContext.setApiName("multiApi");
				yrcApiContext.setInputXml(inputxmlForMultiApi);
				yrcApiContext.setFormId(this.getFormId());

				new YRCApiCaller(yrcApiContext, true).invokeApi();
				handleApiCompletion(yrcApiContext);

				ArrayList<Element> multiApis = YRCXmlUtils.getChildren(
						eleMultiApi, "API");

				for (int k = 0; k < multiApis.size(); k++) {

					Element eleApi = (Element) multiApis.get(k);

					Element eleOutput = YRCXmlUtils.getChildElement(eleApi,
							"Output");
					Element elePricelistLineList = YRCXmlUtils.getChildElement(
							eleOutput, "PricelistLineList");

					ArrayList nPricelistLine = YRCXmlUtils.getChildren(
							elePricelistLineList, "PricelistLine");
					if (nPricelistLine.size() > 0) {
						Element eLeastPriceListLine = createLeastPriceListLine(nPricelistLine);
						String strLeastItemId = YRCXmlUtils.getAttribute(
								eLeastPriceListLine, "ItemID");
						String strLeastUnitPrice = YRCXmlUtils.getAttribute(
								eLeastPriceListLine, "ListPrice");

						for (int j = 0; j < nItems.getLength(); j++) {
							//Element eleItem = (Element) nItems.get(j);
							Element eleItem = (Element)nItems.item(j);
							String resultsItemID = YRCXmlUtils.getAttribute(
									eleItem, "ItemID");
							if (resultsItemID.equals(strLeastItemId)) {

								Element eleComputedPrice = YRCXmlUtils
										.getChildElement(eleItem,
												"ComputedPrice");
								YRCXmlUtils.setAttribute(eleComputedPrice,
										"UnitPrice", strLeastUnitPrice);
							}
						}
					}

				}

			}
		}

		super.postCommand(yrcapicontext);
	}

	/**
	 * This method initializes the behavior class.
	 */
	public void init() {
		// TODO: Write behavior init here.
	}

	@Override
	public void postSetModel(String modelName) {

		super.postSetModel(modelName);
	}

	@Override
	public boolean preCommand(YRCApiContext arg0) {
		arg0.getApiName();
		// TODO Auto-generated method stub
		return super.preCommand(arg0);
	}

	@Override
	protected void setExtentionModel(String arg0, Element arg1) {
		// TODO Auto-generated method stub
		super.setExtentionModel(arg0, arg1);
	}

	public void handleApiCompletion(YRCApiContext Ctx) {

		if (YRCPlatformUI.equals(Ctx.getApiName(), "multiApi")) {
			docMultiApi = Ctx.getOutputXml();
			eleMultiApi = docMultiApi.getDocumentElement();

		}

		super.handleApiCompletion(Ctx);

	}

	/**
	 * @param eleResults
	 * @param strItemId
	 * @param strUnitPrice
	 * @return
	 */
	private Element addComputedPriceForResults(Element eleGetItemListForOrder,
			String strLeastItemId, String strLeastUnitPrice) {
		ArrayList nItems = YRCXmlUtils.getChildren(eleGetItemListForOrder,
				"Item");
		for (int j = 0; j < nItems.size(); j++) {
			Element eleItem = (Element) nItems.get(j);
			String resultsItemID = YRCXmlUtils.getAttribute(eleItem, "ItemID");
			if (resultsItemID.equals(strLeastItemId)) {

				Element eleComputedPrice = YRCXmlUtils.getChildElement(eleItem,
						"ComputedPrice");
				YRCXmlUtils.setAttribute(eleComputedPrice, "UnitPrice",
						strLeastUnitPrice);
				YRCXmlUtils.setAttribute(eleComputedPrice, "ListPrice",
						strLeastUnitPrice);
			}
		}
		return eleGetItemListForOrder;
	}

	/**
	 * @param npricelistLine
	 * @return
	 */
	private Element createLeastPriceListLine(ArrayList npricelistLine) {
		// TODO Auto-generated method stub

		Element ePricelistLineLeast = (Element) npricelistLine.get(0);
		Double dunitPrice = Double.parseDouble(YRCXmlUtils.getAttribute(
				ePricelistLineLeast, "UnitPrice"));
		Double dtemp = 0.0;

		for (int i = 0; i < npricelistLine.size(); i++) {
			Element ePricelistLine = (Element) npricelistLine.get(i);

			dtemp = Double.parseDouble(YRCXmlUtils.getAttribute(ePricelistLine,
					"UnitPrice"));
			if (dtemp <= dunitPrice) {
				dunitPrice = dtemp;
				ePricelistLineLeast = (Element) npricelistLine.get(i);
			}

		}

		return ePricelistLineLeast;
	}

	@Override
	public IYRCComposite createPage(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void pageBeingDisposed(String arg0) {
		// TODO Auto-generated method stub

	}
//	 Start - Zero Dollar CR Changes - Defect # 1856
	public YRCValidationResponse validateButtonClick(String fieldName) {
		YRCValidationResponse response = new YRCValidationResponse();	
		if(fieldName.equals("buttonAddToOrder")){
			Element ele=this.getModel("PreviewPanelModel");
		if(ele!=null){
			Element eleComputedPrice=YRCXmlUtils.getChildElement(ele, "ComputedPrice");
			if(eleComputedPrice!=null){
			String strUnitPrice=eleComputedPrice.getAttribute("UnitPrice");
			if((strUnitPrice==null || strUnitPrice.equals("")|| strUnitPrice.equals("0.00")|| strUnitPrice.equals("0")||strUnitPrice.equals("$0.00"))) 
			{
				YRCSharedTaskOutput out = HBCPCAUtil.showOptionDialogBoxWithYesNo("ITEM_ERROR",
				"ZERO_DOLLAR_PRICE_ITEM");				
				Element eSelectedOption = out.getOutput();
				String sSelectedOptionKey = "";
				sSelectedOptionKey = YRCXmlUtils.getAttribute(
				eSelectedOption, "SelectedOptionKey");				
				//HBCPCAUtil.showOptionDialogBoxWithOK("Item Error", "Item with $0 price can not be added!");
				if (sSelectedOptionKey != null
						&& sSelectedOptionKey.equals("OPTION_NO")) {
					response = new YRCValidationResponse(
							YRCValidationResponse.YRC_VALIDATION_ERROR, "");
				}
				else{
					response = new YRCValidationResponse(
							YRCValidationResponse.YRC_VALIDATION_OK, "");
					
				}
			}
			}
		}
		}
		return response;
		}
//	 End - Zero Dollar CR Changes - Defect # 1856
}