package com.pca.rcp.hbcp2.orderNotes;

import java.util.ArrayList;
import org.eclipse.swt.widgets.Composite;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCEditorInput;
import com.yantra.yfc.rcp.IYRCComposite;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCDesktopUI;
import com.yantra.yfc.rcp.YRCExtentionBehavior;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCExtendedTableBindingData;
import com.yantra.yfc.rcp.YRCWizard;
import com.yantra.yfc.rcp.YRCXmlUtils;
import com.pca.rcp.hbcp2.util.XMLUtil;
import com.pca.rcp.hbcp2.orderNotes.HBCNoteComposite;
/**
 *
 * @author Manthena Karthik
 * This class updates the Pickup Location in the COM Screen
 */
public class HBCOrderNotes extends YRCExtentionBehavior {

	/**
	 * This method initializes the behavior class.
	 */
	public void init() {
		//TODO: Write behavior init here.
	}

	/**
	 * Method for validating the text box.
	 */
	public YRCValidationResponse validateTextField(String fieldName, String fieldValue) {
		// TODO Validation required for the following controls.

		// TODO Create and return a response.
		return super.validateTextField(fieldName, fieldValue);
	}

	/**
	 * Method for validating the combo box entry.
	 */
	public void validateComboField(String fieldName, String fieldValue) {
		// TODO Validation required for the following controls.

		// TODO Create and return a response.
		super.validateComboField(fieldName, fieldValue);
	}

	/**
	 * Method called when a button is clicked.
	 */
	public YRCValidationResponse validateButtonClick(String fieldName) {
		// TODO Validation required for the following controls.

		// TODO Create and return a response.
		return super.validateButtonClick(fieldName);
	}

	/**
	 * Method called when a link is clicked.
	 */
	public YRCValidationResponse validateLinkClick(String fieldName) {
		// TODO Validation required for the following controls.

		// TODO Create and return a response.
		return super.validateLinkClick(fieldName);
	}

	/**
	 * Create and return the binding data for advanced table columns added to the tables.
	 */
	public YRCExtendedTableBindingData getExtendedTableBindingData(String tableName, ArrayList tableColumnNames) {
		// Create and return the binding data definition for the table.

		// The default super implementation does nothing.
		return super.getExtendedTableBindingData(tableName, tableColumnNames);
	}	
	
	public void postSetModel(String model) {
		// TODO Auto-generated method stub
		if(model.equals("ContactTypes"))
		{
			Element eleContactTypes = getModel(model);
			String strShopperEmail = YRCPlatformUI.getFormattedString("SECONDARY_SHOPPER_EMAIL","");
			String strShopperFirstName = YRCPlatformUI.getFormattedString("SECONDARY_SHOPPER_FIRST_NAME","");
			String strShopperLastName = YRCPlatformUI.getFormattedString("SECONDARY_SHOPPER_LAST_NAME","");
			String strShopperPhone = YRCPlatformUI.getFormattedString("SECONDARY_SHOPPER_PHONE","");		 
			NodeList nCommonCode = 	eleContactTypes.getElementsByTagName("CommonCode");			
			for(int i=0; i<nCommonCode.getLength(); i++)
			{
				Element eleCommonCode = (Element) nCommonCode.item(i);
				String strCodeValue = eleCommonCode.getAttribute("CodeValue");
				if(strCodeValue.equals("EMAILID"))
				{
					XMLUtil.setAttribute(eleCommonCode, "CodeShortDescription", strShopperEmail);
					XMLUtil.setAttribute(eleCommonCode, "CodeLongDescription", strShopperEmail);
				}
				else if(strCodeValue.equals("FIRST_NAME"))
				{
					XMLUtil.setAttribute(eleCommonCode, "CodeShortDescription", strShopperFirstName);
					XMLUtil.setAttribute(eleCommonCode, "CodeLongDescription", strShopperFirstName);					
				}
				else if(strCodeValue.equals("LAST_NAME"))
				{
					XMLUtil.setAttribute(eleCommonCode, "CodeShortDescription", strShopperLastName);
					XMLUtil.setAttribute(eleCommonCode, "CodeLongDescription", strShopperLastName);				
				}
				else if(strCodeValue.equals("PHONE"))
				{
					XMLUtil.setAttribute(eleCommonCode, "CodeShortDescription", strShopperPhone);
					XMLUtil.setAttribute(eleCommonCode, "CodeLongDescription", strShopperPhone);					
				}
			}
			this.repopulateModel("ContactTypes");						
		}
		super.postSetModel(model);
	}	
}