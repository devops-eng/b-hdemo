package com.pca.rcp.hbcp2.orderentry;

import java.util.ArrayList;

import javax.xml.xpath.XPathConstants;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.yantra.yfc.rcp.YRCExtentionBehavior;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCXPathUtils;

public class HBCAddressPanelExtnBehavior extends YRCExtentionBehavior {
	@Override
	public void postSetModel(String arg0) {
		if("address".equals(arg0)){
			String strIsHistiry ="";
			Element elePersonalInfo = getModel(arg0);
			if(elePersonalInfo!=null)
				strIsHistiry = elePersonalInfo.getAttribute("isHistory");
			if(strIsHistiry!=null && strIsHistiry.equals("N")){
				String actualPanelname = (String)this.getOwnerForm().getData();
			if(this.getParentExtnBehavior()!=null && this.getParentExtnBehavior() instanceof HBCCustomerSearchPageExtnBehavior){
				HBCCustomerSearchPageExtnBehavior customerSearchPageExtnBehavior = (HBCCustomerSearchPageExtnBehavior)this.getParentExtnBehavior();
				Element selectedCustomer = customerSearchPageExtnBehavior.getFromModel("selectedCustomer");
				if(selectedCustomer!=null){
					boolean goingToModify = false;
					String strPhone = elePersonalInfo.getAttribute("DayPhone");
					NodeList nodelist = (NodeList) YRCXPathUtils.evaluate(selectedCustomer, "CustomerContactList/CustomerContact/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo[@DayPhone='']", XPathConstants.NODESET);
					for (int i = 0; i < nodelist.getLength(); i++) {
						Element PersonInfo = (Element)nodelist.item(i);
						PersonInfo.setAttribute("DayPhone", strPhone);
						PersonInfo.setAttribute("AddressChanged", "Y");
						goingToModify = true;
					}
					 nodelist = (NodeList) YRCXPathUtils.evaluate(selectedCustomer, "CustomerContactList/CustomerContact/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo[@EMailID='']", XPathConstants.NODESET);
					 String strEmail =  elePersonalInfo.getAttribute("EMailID");
					for (int i = 0; i < nodelist.getLength(); i++) {
						Element PersonInfo = (Element)nodelist.item(i);
						PersonInfo.setAttribute("EMailID", strEmail);
						PersonInfo.setAttribute("AddressChanged", "Y");
						goingToModify = true;
					}
					 nodelist = (NodeList) YRCXPathUtils.evaluate(selectedCustomer, "CustomerContactList/CustomerContact[@DayPhone='']", XPathConstants.NODESET);
					 for (int i = 0; i < nodelist.getLength(); i++) {
							Element PersonInfo = (Element)nodelist.item(i);
							PersonInfo.setAttribute("DayPhone", strPhone);
							PersonInfo.setAttribute("AddressChanged", "Y");
							goingToModify = true;
						}
					 nodelist = (NodeList) YRCXPathUtils.evaluate(selectedCustomer, "CustomerContactList/CustomerContact[@EmailID='']", XPathConstants.NODESET);
					 for (int i = 0; i < nodelist.getLength(); i++) {
							Element PersonInfo = (Element)nodelist.item(i);
							PersonInfo.setAttribute("EmailID", strEmail);
							PersonInfo.setAttribute("AddressChanged", "Y");
							goingToModify = true;
						}
					if(goingToModify == true){
					customerSearchPageExtnBehavior.repopulateModel("selectedCustomer");
					customerSearchPageExtnBehavior.repopulateModel("CurrentContact");
					ArrayList chiArrayList = customerSearchPageExtnBehavior.getChildExtnBehaviors();
					if(chiArrayList!=null){
					for (int i = 0; i < chiArrayList.size(); i++) {
						if(chiArrayList.get(i) instanceof HBCAddressPanelExtnBehavior ){
							HBCAddressPanelExtnBehavior hbcAddressPanelExtnBehavior=(HBCAddressPanelExtnBehavior)chiArrayList.get(i);
							String panelname = (String)hbcAddressPanelExtnBehavior.getOwnerForm().getData();
							//Getting the address model if it is only BillToAddress Panel
							if(   !actualPanelname.equals(panelname)   && ("pnlBillToAddress".equals(panelname) || "pnlShipToAddress".equals(panelname) )){
								Element eleAddress=hbcAddressPanelExtnBehavior.getFromModel("address");
								
								if(eleAddress!=null){
									String phone = eleAddress.getAttribute("DayPhone");
									String email = eleAddress.getAttribute("EMailID");
									if(YRCPlatformUI.isVoid(phone)){
										eleAddress.setAttribute("DayPhone", strPhone);
										eleAddress.setAttribute("AddressChanged", "N");
									} if(YRCPlatformUI.isVoid(email)){
										eleAddress.setAttribute("EMailID", strEmail);
										eleAddress.setAttribute("AddressChanged", "N");
									}
									//PersonInfo/@AddressLine1;address:/PersonInfo/@AddressLine2;address:/PersonInfo/@AddressLine2;address:/PersonInfo/@City;address:/PersonInfo/@State;address:/PersonInfo/@ZipCode;address:/PersonInfo/@Country;address:/PersonInfo/@AddressID
									hbcAddressPanelExtnBehavior.repopulateModel("address");
								}
							}
						}
					}
					}
				}
				}
			}
			}
		}
		super.postSetModel(arg0);
	}

	public Element getFromModel(String model){
		return this.getModel(model);
	}
}
