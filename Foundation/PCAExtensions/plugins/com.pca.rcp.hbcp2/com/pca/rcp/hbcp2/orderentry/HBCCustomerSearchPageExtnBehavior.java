package com.pca.rcp.hbcp2.orderentry;

import java.util.ArrayList;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.pca.rcp.hbcp2.util.XMLUtil;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCExtentionBehavior;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCXmlUtils;

public class HBCCustomerSearchPageExtnBehavior extends YRCExtentionBehavior {
	String strPrefLang="";
	String billToId="";
	String strCountry = "";
	boolean existingCustomerWithoutPreflanf = false;
	boolean isBack = false;

	@Override
	public boolean preCommand(YRCApiContext ctx) {
		if (YRCPlatformUI.equals(ctx.getApiName(), "getSalesOrderDetailsWithAddresses")) {
			
			if(this.existingCustomerWithoutPreflanf){
				this.existingCustomerWithoutPreflanf = true;
				isBack = true;
			}
		}
		else if (YRCPlatformUI.equals(ctx.getApiName(), "changeOrder")) {

			Document changeOrderInput = ctx.getInputXml();
			Element eChangeOrder = changeOrderInput.getDocumentElement();
			billToId=eChangeOrder.getAttribute("BillToID");
			if(billToId.equals("")){
				if(!YRCPlatformUI.isVoid(this.strPrefLang)){
					appendCountryToPreferedLanguage(eChangeOrder);
					Element eExtn=YRCXmlUtils.getChildElement(eChangeOrder, "Extn");
					if(eExtn == null)
						eExtn =	YRCXmlUtils.createChild(eChangeOrder, "Extn");
						eExtn.setAttribute("ExtnCommLang", this.strPrefLang);
					}
			}
			String[] apiNames = ctx.getApiNames();
			Document[] inputXML = ctx.getInputXmls();
			if(apiNames.length == 1){
				if(this.existingCustomerWithoutPreflanf && !isBack){
					Document docManageCustomer = YRCXmlUtils.createDocument("Customer");
					Element eleCustomer = docManageCustomer.getDocumentElement();
					Element eleCustomerContactList = YRCXmlUtils.createChild(eleCustomer, "CustomerContactList");
					Element eleCustomerContact = YRCXmlUtils.createChild(eleCustomerContactList, "CustomerContact");
					Element eleExtn = YRCXmlUtils.createChild(eleCustomerContact, "Extn");
					Element eleSelectedCustomer = getModel("selectedCustomer");
					eleCustomer.setAttribute("CustomerID", eleSelectedCustomer.getAttribute("CustomerID"));
					eleCustomer.setAttribute("OrganizationCode", eleSelectedCustomer.getAttribute("OrganizationCode"));
					Element eleTempCustomerContactList = XMLUtil.getChildElement(eleSelectedCustomer, "CustomerContactList");
					if(eleTempCustomerContactList!=null){
						ArrayList customerContactList = YRCXmlUtils.getChildren(eleTempCustomerContactList, "CustomerContact");
						for (int k = 0; k < customerContactList.size(); k++) {
							Element eleTempCustomerContact = (Element)customerContactList.get(k);
							eleCustomerContact.setAttribute("CustomerContactID", eleTempCustomerContact.getAttribute("CustomerContactID"));
							eleExtn.setAttribute("ExtnPreferredLanguage", strPrefLang);
						}
					}
					apiNames = new String[2];
					apiNames[0] = "changeOrder";
					apiNames[1] = "manageCustomer";
					Document createOrderDocu = ctx.getInputXmls()[0];
					inputXML = new Document[2];
					inputXML[0] = createOrderDocu;
					inputXML[1] = docManageCustomer;
					ctx.setApiNames(apiNames);
					ctx.setInputXmls(inputXML);
				}
			}else{
			for (int i = 0; i < apiNames.length; i++) {
				String apiName = apiNames[i];
				if("manageCustomer".equals(apiName)){
					Document docInput = ctx.getInputXmls()[i];
					Element eleSelectedCustomer = getModel("selectedCustomer");
					if(eleSelectedCustomer!=null){
						if(this.existingCustomerWithoutPreflanf && !this.isBack){
							NodeList nodeTempCustomerContactList = docInput.getDocumentElement().getElementsByTagName("CustomerContact"); 
							for (int j = 0; j < nodeTempCustomerContactList.getLength(); j++) {
								Element inputCustomerContact = (Element)nodeTempCustomerContactList.item(j);
								Element eleExtn = YRCXmlUtils.createChild(inputCustomerContact, "Extn");
								eleExtn.setAttribute("ExtnPreferredLanguage", strPrefLang);
							}
						}
						NodeList referenceNodeList =eleSelectedCustomer.getElementsByTagName("CustomerAdditionalAddress");
						//Document docInput = ctx.getInputXmls()[i];
						NodeList nodeList =  docInput.getDocumentElement().getElementsByTagName("CustomerAdditionalAddressList");
						for (int j = 0; j < nodeList.getLength(); j++) {
							Element eleCustomerAdditionalAddressList = (Element)nodeList.item(j);
							for (int k = 0; k < referenceNodeList.getLength(); k++) {
								Element eleCustomerAdditionalAddress = (Element)referenceNodeList.item(k);
								Element PersonInfo = YRCXmlUtils.getChildElement(eleCustomerAdditionalAddress, "PersonInfo");
								String strAddressChanged = PersonInfo.getAttribute("AddressChanged");
								if(strAddressChanged!=null && strAddressChanged.equals("Y")){
									PersonInfo.removeAttribute("AddressChanged");
									YRCXmlUtils.importElement(eleCustomerAdditionalAddressList, eleCustomerAdditionalAddress);
								}
							}
						}
						 referenceNodeList =eleSelectedCustomer.getElementsByTagName("CustomerContact");
						 for (int k = 0; k < referenceNodeList.getLength(); k++) {
								Element CustomerContact = (Element)referenceNodeList.item(k);
								String strAddressChanged = CustomerContact.getAttribute("AddressChanged");
								if(strAddressChanged!=null && strAddressChanged.equals("Y")){
									NodeList nodeCustomerContactList = docInput.getDocumentElement().getElementsByTagName("CustomerContact"); 
									for (int j = 0; j < nodeCustomerContactList.getLength(); j++) {
										Element inputCustomerContact = (Element)nodeCustomerContactList.item(j);
										inputCustomerContact.setAttribute("DayPhone", CustomerContact.getAttribute("DayPhone"));
										inputCustomerContact.setAttribute("EmailID", CustomerContact.getAttribute("EmailID"));
									}
								}
								CustomerContact.removeAttribute("AddressChanged");
						 }
					NodeList nodeCustomerAdditionalAddressList =  docInput.getDocumentElement().getElementsByTagName("CustomerAdditionalAddressList");
					NodeList nodeCustomerAdditionalAddress =  docInput.getDocumentElement().getElementsByTagName("CustomerAdditionalAddress");
					for (int j = 0; j < nodeCustomerAdditionalAddress.getLength(); j++) {
						Element eleCustomerAdditionalAddress = (Element)nodeCustomerAdditionalAddress.item(j);
						Element elePersonInfo = YRCXmlUtils.getChildElement(eleCustomerAdditionalAddress, "PersonInfo");
						String strAddressChanged = elePersonInfo.getAttribute("AddressChanged");
						if(strAddressChanged!=null && strAddressChanged.equals("N")){
							nodeCustomerAdditionalAddressList.item(0).removeChild(eleCustomerAdditionalAddress);
						}
					}
					this.repopulateModel("selectedCustomer");	
				}
			}
		}
			}
			
		}
		if (YRCPlatformUI.equals(ctx.getApiName(), "createOrder")) {

			Document changeOrderInput = ctx.getInputXml();
			Element eChangeOrder = changeOrderInput.getDocumentElement();
			billToId=eChangeOrder.getAttribute("BillToID");
			if(billToId.equals("")){
				if(!YRCPlatformUI.isVoid(this.strPrefLang)){
					appendCountryToPreferedLanguage(eChangeOrder);
					Element eExtn=YRCXmlUtils.getChildElement(eChangeOrder, "Extn");
					if(eExtn == null)
						eExtn =	YRCXmlUtils.createChild(eChangeOrder, "Extn");
						eExtn.setAttribute("ExtnCommLang", this.strPrefLang);
					}
				}
				String[] apiNames = ctx.getApiNames();
				Document[] inputXML = ctx.getInputXmls();
			if(apiNames.length == 1){
				if(this.existingCustomerWithoutPreflanf){
					Document docManageCustomer = YRCXmlUtils.createDocument("Customer");
					Element eleCustomer = docManageCustomer.getDocumentElement();
					Element eleCustomerContactList = YRCXmlUtils.createChild(eleCustomer, "CustomerContactList");
					Element eleCustomerContact = YRCXmlUtils.createChild(eleCustomerContactList, "CustomerContact");
					Element eleExtn = YRCXmlUtils.createChild(eleCustomerContact, "Extn");
					Element eleSelectedCustomer = getModel("selectedCustomer");
					eleCustomer.setAttribute("CustomerID", eleSelectedCustomer.getAttribute("CustomerID"));
					eleCustomer.setAttribute("OrganizationCode", eleSelectedCustomer.getAttribute("OrganizationCode"));
					Element eleTempCustomerContactList = XMLUtil.getChildElement(eleSelectedCustomer, "CustomerContactList");
					if(eleTempCustomerContactList!=null){
						ArrayList customerContactList = YRCXmlUtils.getChildren(eleTempCustomerContactList, "CustomerContact");
						for (int k = 0; k < customerContactList.size(); k++) {
							Element eleTempCustomerContact = (Element)customerContactList.get(k);
							eleCustomerContact.setAttribute("CustomerContactID", eleTempCustomerContact.getAttribute("CustomerContactID"));
							eleExtn.setAttribute("ExtnPreferredLanguage", strPrefLang);
						}
					}
					apiNames = new String[2];
					apiNames[0] = "createOrder";
					apiNames[1] = "manageCustomer";
					Document createOrderDocu = ctx.getInputXmls()[0];
					inputXML = new Document[2];
					inputXML[0] = createOrderDocu;
					inputXML[1] = docManageCustomer;
					ctx.setApiNames(apiNames);
					ctx.setInputXmls(inputXML);
				}
			}else{
			for (int i = 0; i < apiNames.length; i++) {
				String apiName = apiNames[i];
				if("manageCustomer".equals(apiName)){
					Element eleSelectedCustomer = getModel("selectedCustomer");
					Document docInput = ctx.getInputXmls()[i];
					if(eleSelectedCustomer!=null){
						if(this.existingCustomerWithoutPreflanf && !this.isBack){
							NodeList nodeTempCustomerContactList = docInput.getDocumentElement().getElementsByTagName("CustomerContact"); 
							for (int j = 0; j < nodeTempCustomerContactList.getLength(); j++) {
								Element inputCustomerContact = (Element)nodeTempCustomerContactList.item(j);
								Element eleExtn = YRCXmlUtils.createChild(inputCustomerContact, "Extn");
								eleExtn.setAttribute("ExtnPreferredLanguage", strPrefLang);
							}
						}
						NodeList referenceNodeList =eleSelectedCustomer.getElementsByTagName("CustomerAdditionalAddress");
						//Document docInput = ctx.getInputXmls()[i];
						NodeList nodeList =  docInput.getDocumentElement().getElementsByTagName("CustomerAdditionalAddressList");
						for (int j = 0; j < nodeList.getLength(); j++) {
							Element eleCustomerAdditionalAddressList = (Element)nodeList.item(j);
							for (int k = 0; k < referenceNodeList.getLength(); k++) {
								Element eleCustomerAdditionalAddress = (Element)referenceNodeList.item(k);
								Element PersonInfo = YRCXmlUtils.getChildElement(eleCustomerAdditionalAddress, "PersonInfo");
								String strAddressChanged = PersonInfo.getAttribute("AddressChanged");
								if(strAddressChanged!=null && strAddressChanged.equals("Y")){
									PersonInfo.removeAttribute("AddressChanged");
									YRCXmlUtils.importElement(eleCustomerAdditionalAddressList, eleCustomerAdditionalAddress);
								}
							}
						}
						 referenceNodeList =eleSelectedCustomer.getElementsByTagName("CustomerContact");
						 for (int k = 0; k < referenceNodeList.getLength(); k++) {
								Element CustomerContact = (Element)referenceNodeList.item(k);
								String strAddressChanged = CustomerContact.getAttribute("AddressChanged");
								if(strAddressChanged!=null && strAddressChanged.equals("Y")){
									NodeList nodeCustomerContactList = docInput.getDocumentElement().getElementsByTagName("CustomerContact"); 
									for (int j = 0; j < nodeCustomerContactList.getLength(); j++) {
										Element inputCustomerContact = (Element)nodeCustomerContactList.item(j);
										inputCustomerContact.setAttribute("DayPhone", CustomerContact.getAttribute("DayPhone"));
										inputCustomerContact.setAttribute("EmailID", CustomerContact.getAttribute("EmailID"));
									}
								}
								CustomerContact.removeAttribute("AddressChanged");
						 }
					NodeList nodeCustomerAdditionalAddressList =  docInput.getDocumentElement().getElementsByTagName("CustomerAdditionalAddressList");
					NodeList nodeCustomerAdditionalAddress =  docInput.getDocumentElement().getElementsByTagName("CustomerAdditionalAddress");
					for (int j = 0; j < nodeCustomerAdditionalAddress.getLength(); j++) {
						Element eleCustomerAdditionalAddress = (Element)nodeCustomerAdditionalAddress.item(j);
						Element elePersonInfo = YRCXmlUtils.getChildElement(eleCustomerAdditionalAddress, "PersonInfo");
						String strAddressChanged = elePersonInfo.getAttribute("AddressChanged");
						if(strAddressChanged!=null && strAddressChanged.equals("N")){
							nodeCustomerAdditionalAddressList.item(0).removeChild(eleCustomerAdditionalAddress);
						}
					}
					this.repopulateModel("selectedCustomer");	
				}
			}
		}
			}
		}
		return super.preCommand(ctx);
	}
	private void appendCountryToPreferedLanguage(Element eChangeOrder) {
		Element ePersonInfoShipTo=YRCXmlUtils.getChildElement(eChangeOrder, "PersonInfoShipTo");
		if(ePersonInfoShipTo!=null){
			strCountry = ePersonInfoShipTo.getAttribute("Country");
		} else {
			Element ePersonInfoBillTo = YRCXmlUtils.getChildElement(
					eChangeOrder, "PersonInfoBillTo");
			if (ePersonInfoBillTo != null)
				strCountry = ePersonInfoBillTo.getAttribute("Country");
		}
		strPrefLang = strPrefLang + "_" + strCountry;
	}
	@Override
	public void postSetModel(String namespace) {
		// TODO Auto-generated method stub
		if("selectedCustomer".equals(namespace)){
			if(this.isBack){
				this.existingCustomerWithoutPreflanf = false;
				this.isBack = false;
			}
		}
		super.postSetModel(namespace);
	}
	public Element getFromModel(String model){
		return this.getModel(model);
	}
	@Override
	public void postCommand(YRCApiContext apiContext) {
		// TODO Auto-generated method stub
		super.postCommand(apiContext);
	}
}
