package com.pca.rcp.hbcp2.orderentry;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.xpath.XPathConstants;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.pca.rcp.hbcp2.orderentry.screens.HBCAddGiftItem;
import com.pca.rcp.hbcp2.orderentry.screens.HBCAddGiftItemBehaviour;
import com.pca.rcp.hbcp2.util.HBCPCAConstants;
import com.pca.rcp.hbcp2.util.HBCPCAUtil;
import com.yantra.pca.ycd.rcp.exposed.YCDExtensionUtils;
//import com.yantra.yfc.rcp.IYRCComposite;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCDialog;
import com.yantra.yfc.rcp.YRCExtentionBehavior;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCSharedTaskOutput;
import com.yantra.yfc.rcp.YRCValidationResponse;
//import com.yantra.yfc.rcp.YRCWizardExtensionBehavior;
import com.yantra.yfc.rcp.YRCXPathUtils;
import com.yantra.yfc.rcp.YRCXmlUtils;
import com.yantra.yfc.rcp.internal.YRCApiCaller;
//R3 imports
import com.yantra.yfc.rcp.YRCDesktopUI;
import com.yantra.yfc.rcp.YRCWizard;

/**
 * 
 * @author Bala MuraliKrishna Cherukuri
 * 
 */

public class HBCOrderEntryLinePanelExtnBehavior extends YRCExtentionBehavior {
	Document docGetPricelistLineList = null;

	Element eleGetPricelistLineList = null;

	private Element giftItems;

	private ArrayList<String> addItems = new ArrayList<String>();

	private boolean isRecentlyTriggered = false;

	Document docGetItemList = null;

	Element eleGetItemList = null;

	public static String unitPrice = "";
	
	//R3 - 11-11 Release Apply Sourcing Rule
	String sApplySourcingRule = "Y";
	String sSourcingRule = "Y";

	@Override
	public void postCommand(YRCApiContext yrcapicontext) {

		String strOrganizationCode = "";
		String strItemId = "";
		Date currentDate = new Date();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String sCurrentDate = formatter.format(currentDate);
		//Start: defect 284
		String sEndDateActiveQryType = "GE";
		String sStartDateActiveQryType = "LE";
		//End: defect 284

		if (YRCPlatformUI.equals("getItemListForOrderingWithoutPrice",
				yrcapicontext.getApiName())) {
			Document docGetItemListForOrderWithOutPrice = yrcapicontext
					.getOutputXml();
			if (docGetItemListForOrderWithOutPrice != null) {
				Element eleGetItemListForOrderWithOutPrice = docGetItemListForOrderWithOutPrice
						.getDocumentElement();

				ArrayList<Element> nItems = YRCXmlUtils.getChildren(
						eleGetItemListForOrderWithOutPrice, "Item");
				// NodeList nItems =
				// eleGetItemListForOrderWithOutPrice.getElementsByTagName("Item");

				Element docUser = getModel("UserNameSpace");
				/*R3 - DEF 574 - As there can be multiple ECOM and MerchLocator items with similar ItemID's, the existing 
				code has been modified to fetch the price of the first retrieved ECOM item*/
				//if (nItems.size() == 1) {
				//R3 Modified to ensure that price is fetched only if at least one item is retrieved.
				if (nItems.size() > 0){
				for (int t=0; t<1; t++){
					Element eItem = (Element) nItems.get(0);
					// Element eItem = (Element) nItems.item(0);

					if (HBCPCAUtil.isStoreApplication()) {
						strOrganizationCode = YRCXmlUtils.getAttribute(docUser,
								"BusinessKey");
					} else {
						strItemId = YRCXmlUtils.getAttribute(eItem, "ItemID");
						strOrganizationCode = YRCXmlUtils.getAttribute(eItem,
								"OrganizationCode");
					}

					Document inputxml = YRCXmlUtils
							.createDocument("PricelistLine");
					Element elePricelistLine = inputxml.getDocumentElement();
					YRCXmlUtils.setAttribute(elePricelistLine, "ItemID",
							strItemId);
					YRCXmlUtils.setAttribute(elePricelistLine,
							"SellerOrganizationCode", strOrganizationCode);
					YRCXmlUtils.setAttribute(elePricelistLine,
							"EndDateActiveQryType", sEndDateActiveQryType);
					YRCXmlUtils.setAttribute(elePricelistLine,
							"StartDateActiveQryType", sStartDateActiveQryType);
					YRCXmlUtils.setAttribute(elePricelistLine, "EndDateActive",
							sCurrentDate);
					YRCXmlUtils.setAttribute(elePricelistLine,
							"StartDateActive", sCurrentDate);
					YRCXmlUtils.setAttribute(elePricelistLine, "PricingStatus",
							"ACTIVE");

					YRCApiContext yrcApiContext = new YRCApiContext();
					yrcApiContext.setApiName("getPricelistLineList");
					yrcApiContext.setInputXml(inputxml);
					yrcApiContext.setFormId(this.getFormId());

					new YRCApiCaller(yrcApiContext, true).invokeApi();
					handleApiCompletion(yrcApiContext);

					if (eleGetPricelistLineList != null) {
						ArrayList nPricelistLine = YRCXmlUtils.getChildren(
								eleGetPricelistLineList, "PricelistLine");
						// call createLeastPriceListLine method...
						if (nPricelistLine.size() > 0) {
							Element eLeastPriceListLine = createLeastPriceListLine(nPricelistLine);
							String strLeastUnitPrice = eLeastPriceListLine.getAttribute("ListPrice");

							Element eleExtnPriceListLine = 
								YRCXmlUtils.getChildElement(eLeastPriceListLine, "Extn") ;
							String strExtnClearanceLevel = eleExtnPriceListLine.getAttribute("ExtnClearanceLevel") ;
	
							Element eleComputedPrice = YRCXmlUtils.getChildElement(eItem, "ComputedPrice");
							YRCXmlUtils.setAttribute(eleComputedPrice, "UnitPrice", strLeastUnitPrice) ;
							YRCXmlUtils.setAttribute(eleComputedPrice, "ListPrice", strLeastUnitPrice) ;
							
							YRCXmlUtils.setAttribute(eleComputedPrice, "ExtnClearanceLevel", strExtnClearanceLevel) ;
						}
					}
					// Start Zero Dollar CR - Defect#1856
					eItem.setAttribute("isGWP", "N");
					YRCXmlUtils.setAttribute(eItem, "isGWP", "N");
					// End Zero Dollar CR - Defect#1856
					}
				}
			}
		}
		super.postCommand(yrcapicontext);
	}
	
	/**
	 * R3 - PreCommand is implemented to stamp AllocationRuleID and FulfillmentType
	 * in the input of getFulfillmentOptionsForLines API
	**/
	@Override
	public boolean preCommand(YRCApiContext yrcapicontext) {

		YRCWizard wizard=(YRCWizard)YRCDesktopUI.getCurrentPage();
		String strPageID=wizard.getCurrentPageID();

		String strFormId= wizard.getFormId();

		if((strPageID.equals("com.yantra.pca.ycd.rcp.tasks.orderEntry.wizardpages.YCDAdvancedAddItemPage"))
				|| (strPageID.equals("com.yantra.pca.ycd.rcp.tasks.orderEntry.wizardpages.YCDOrderEntryAddItemPage"))){

			String strApis[]=yrcapicontext.getApiNames();
			Document docInputXMLS[]=yrcapicontext.getInputXmls();
			 Element elePromise = null;
	            String strApi;
	            for(int i=0 ; i < strApis.length ; i++){
	                  strApi=strApis[i];
	                  if(strApi.equals("getFulfillmentOptionsForLines"))
	                  {
	                	  //R3 - 11-11 Release. Apply Sourcing Rule
	                	  org.w3c.dom.Document docHBCGetCommonCodeList = YRCXmlUtils.createDocument("CommonCode");
	                	  Element eleCommonCodeInput = docHBCGetCommonCodeList.getDocumentElement();
	                	  //eleCommonCodeInput.setAttribute("OrganizationCode", HBCSOMConstants.DEFAULT_ORGANIZATION_CODE);
	                	  eleCommonCodeInput.setAttribute("CodeType", HBCPCAConstants.SOURCING_RULE);
	                	  eleCommonCodeInput.setAttribute("CodeValue", HBCPCAConstants.SOURCING_CHANNEL);
	                	  YRCApiContext context = new YRCApiContext();
	                	  context.setFormId(super.getFormId());
	                	  context.setApiName("HBCGetSourcingRule");
	                	  context.setInputXml(docHBCGetCommonCodeList);

	                	  new YRCApiCaller(context, true).invokeApi();
	                	  handleApiCompletion(context);

	                	  elePromise = docInputXMLS[i].getDocumentElement();

	                        String sOrganizationCode = elePromise.getAttribute("OrganizationCode");
	                        if ("LT".equalsIgnoreCase(sOrganizationCode))
	                        {
	                        	elePromise.setAttribute("AllocationRuleID", HBCPCAConstants.LT_ALLOCATION_RULE_ID);
	                        }
	                        if ("BAY".equalsIgnoreCase(sOrganizationCode))
	                        {
	                        	elePromise.setAttribute("AllocationRuleID", HBCPCAConstants.BAY_ALLOCATION_RULE_ID);
	                        }
	                        Element elePromiseLines = YRCXmlUtils.getChildElement(elePromise, "PromiseLines");
	           	         	if(elePromiseLines!=null)
	           	         	{
	           	        	  NodeList nPromiseLine = elePromiseLines.getElementsByTagName("PromiseLine");
	           	        	  for(int p=0;p<nPromiseLine.getLength();p++)
	           	        	  {
	           	        		  Element ePromiseLine = (Element)nPromiseLine.item(p);
	           	        		  if(ePromiseLine!=null)
	           	        		  {
	           	        			  if ("LT".equalsIgnoreCase(sOrganizationCode))
	           	        			  {
	                					  //R3 - 11-11 Release. Apply Sourcing Rule
	                					  if (HBCPCAConstants.APPLY_SOURCING_RULE.equalsIgnoreCase(sApplySourcingRule))
	                					  {
	                						  ePromiseLine.setAttribute("FulfillmentType", sSourcingRule);
	                					  }
	                					  else
	                					  {
	                						  ePromiseLine.setAttribute("FulfillmentType", HBCPCAConstants.FT_TYPE_STANDARD);
	                					  }

	           	        			  }
	           	        			  if ("BAY".equalsIgnoreCase(sOrganizationCode))
	           	        			  {
	                					  //R3 - 11-11 Release. Apply Sourcing Rule
	                					  if (HBCPCAConstants.APPLY_SOURCING_RULE.equalsIgnoreCase(sApplySourcingRule))
	                					  {
	                						  ePromiseLine.setAttribute("FulfillmentType", sSourcingRule);
	                					  }
	                					  else
	                					  {
	           	        				  ePromiseLine.setAttribute("FulfillmentType", HBCPCAConstants.FT_TYPE_STANDARD);
	           	        			  }
	           	        		  }
	           	        	  }
	                		  }

	           	          }

	                  }
	                  /**
	                   	* R3 - PreCommand is implemented to fetch only ecom items by stamping Status="3000"
	                   	* in the input of getItemListForOrderingWithoutPrice API
	                   	**/
	                  if(strApi.equals("getItemListForOrderingWithoutPrice")){
	                	  Element eleItem = docInputXMLS[i].getDocumentElement();
	                	  Element elePrimaryInformation = docInputXMLS[i].createElement("PrimaryInformation");
	                	  elePrimaryInformation.setAttribute("Status", HBCPCAConstants.ITEM_STATUS);
	                	  eleItem.appendChild(elePrimaryInformation);
	                  }
	            }
		}

		return super.preCommand(yrcapicontext);
	}

	@Override
	public void validateComboField(String arg0, String arg1) {
		super.validateComboField(arg0, arg1);
	}

	@Override
	public void handleApiCompletion(YRCApiContext ctx) {
		// TODO Auto-generated method stub
		if (YRCPlatformUI.equals(ctx.getApiName(), "getPricelistLineList")) {
			docGetPricelistLineList = ctx.getOutputXml();
			eleGetPricelistLineList = docGetPricelistLineList
					.getDocumentElement();

		}
		if (YRCPlatformUI.equals(ctx.getApiName(), "getItemListForOrdering")) {
			docGetItemList = ctx.getOutputXml();
			eleGetItemList = docGetItemList.getDocumentElement();
		}
		
		//R3 - 11-11 Release. Apply Sourcing Rule
		if (YRCPlatformUI.equals(ctx.getApiName(), "HBCGetSourcingRule")) {
			Document docGetSourcingRule = ctx.getOutputXml();
			Element eleGetSourcingRule = docGetSourcingRule.getDocumentElement();
			NodeList nGetSourcingRuleList = eleGetSourcingRule.getElementsByTagName("CommonCode");
			for (int p = 0; p < nGetSourcingRuleList.getLength(); p++) {
				Element eleCommonCode = (Element) nGetSourcingRuleList.item(p);
				sApplySourcingRule = eleCommonCode.getAttribute("CodeShortDescription");
				if (HBCPCAConstants.APPLY_SOURCING_RULE.equalsIgnoreCase(sApplySourcingRule))
				{
					sSourcingRule = eleCommonCode.getAttribute("CodeLongDescription");
				}
			}
		}
		super.handleApiCompletion(ctx);
	}

	/**
	 * @param pricelistLine
	 * @return price list line having least unit price
	 */
	private Element createLeastPriceListLine(ArrayList npricelistLine) {

		Element ePricelistLineLeast = (Element) npricelistLine.get(0);
		Double dunitPrice = Double.parseDouble(YRCXmlUtils.getAttribute(
				ePricelistLineLeast, "UnitPrice"));
		Double dtemp = 0.0;

		for (int i = 0; i < npricelistLine.size(); i++) {
			Element ePricelistLine = (Element) npricelistLine.get(i);

			dtemp = Double.parseDouble(YRCXmlUtils.getAttribute(ePricelistLine,
					"UnitPrice"));
			if (dtemp <= dunitPrice) {
				dunitPrice = dtemp;
				ePricelistLineLeast = (Element) npricelistLine.get(i);
			}

		}

		return ePricelistLineLeast;
	}

	@Override
	public void postSetModel(String model) {

		if (model.equals("OrderLines")) {
			Element OrderLine = this.getModel(model);
			if (OrderLine != null) {
				String strShipTogetherNo = OrderLine
						.getAttribute("ShipTogetherNo");
				if (strShipTogetherNo != null
						&& (strShipTogetherNo.equals("GWP_Order") || strShipTogetherNo
								.equals("GWP_OrderLine"))) {
					Link link = (Link) getControl(this.getOwnerForm(),
							"lnkOverride");
					link.setVisible(false);
					link.setEnabled(false);
					Label label = (Label) getControl(this.getOwnerForm(),
							"extn_Freegift");
					label.setVisible(true);
					label.setToolTipText(YRCPlatformUI.getFormattedString(
							"GWP_FREE_GIFT", ""));
				}
				String strPrimeLineNo = OrderLine.getAttribute("PrimeLineNo");
				if (this.getParentExtnBehavior() != null) {
					HBCOrderEntryWizardExtnBehavior temp = (HBCOrderEntryWizardExtnBehavior) this
							.getParentExtnBehavior();
					if (temp.responseFromAMS != null) {
						Element eleOrderLine = (Element) YRCXPathUtils
								.evaluate(temp.responseFromAMS,
										"OrderLines/OrderLine[@PrimeLineNo='"
												+ strPrimeLineNo + "']",
										XPathConstants.NODE);
						Button buttonGWP = (Button) getControl(this
								.getOwnerForm(), "extn_GWP");
						buttonGWP.setImage(YRCPlatformUI
								.getImage("/icons/ui_unchecked.gif"));
						buttonGWP.setToolTipText(YRCPlatformUI
								.getString("GIFT_ITEMS"));
						buttonGWP.setCursor(new Cursor(buttonGWP.getDisplay(),
								21));
						buttonGWP.setData("name", "extn_GWP");
						if (eleOrderLine != null && hasFreegifts(eleOrderLine))
							buttonGWP.setVisible(true);
						else
							buttonGWP.setVisible(false);
					}
				}
			}
		}
		if (model.equals("ItemDetails")) {
			// Element OrderLine = this.getModel("OrderLines");
			Element eleItemDetails = this.getModel("ItemDetails");
			String itemID = eleItemDetails.getAttribute("ItemID");
			String orgCode = eleItemDetails.getAttribute("OrganizationCode");
			String manfitem = "";
			String prodLine = "";
//			 Start: CR zero Dollar - Defect#1856
			Element eleComputedPrice = YRCXmlUtils.getChildElement(
					eleItemDetails, "ComputedPrice");
			String strUnitPrice = eleComputedPrice.getAttribute("UnitPrice");
			
			//AMS Upgrade Change:: setting of ExtnClearanceLevel flag
			String strClearanceLevel = eleComputedPrice.getAttribute("ExtnClearanceLevel") ;
			
			Element eleItemDesc = YRCXmlUtils.getChildElement(eleItemDetails,
					"PrimaryInformation");
			String strGift = eleItemDesc.getAttribute("ShortDescription");
			String isGWP = eleItemDetails.getAttribute("isGWP");

			if ((strUnitPrice == null || strUnitPrice.equals("0.00")
					|| strUnitPrice.equals("0") || strUnitPrice.equals("$0.00"))
					&& (isGWP != null && isGWP.equals("N"))) {
				YRCSharedTaskOutput out = HBCPCAUtil.showOptionDialogBoxWithYesNo("ITEM_ERROR",
						"ZERO_DOLLAR_PRICE_ITEM");				
				Element eSelectedOption = out.getOutput();
				String sSelectedOptionKey = "";
				sSelectedOptionKey = YRCXmlUtils.getAttribute(
				eSelectedOption, "SelectedOptionKey");
			if (sSelectedOptionKey != null && sSelectedOptionKey.equals("OPTION_NO")) {
				
				Element ele = getFromModel("TransactionalLineId");
				Document doc = YRCXmlUtils.createDocument("OrderLines");
				Element eleOrdLine = doc.getDocumentElement();
				Element element = YRCXmlUtils.createChild(eleOrdLine,
						"OrderLine");
				element.setAttribute("TransactionalLineId", ele
						.getAttribute("TransactionalLineId"));
				YCDExtensionUtils
						.deleteLinesFromOrder(doc.getDocumentElement());
			}
			}

			// End CR Zero Dollar
			// Constructing Input for getItemListForOrdering API
			Document itemInput = YRCXmlUtils.createDocument("Item");
			Element eleItem = itemInput.getDocumentElement();
			YRCXmlUtils.setAttribute(eleItem, "ItemID", itemID);
			YRCXmlUtils.setAttribute(eleItem, "UnitOfMeasure", "EACH");
			YRCXmlUtils.setAttribute(eleItem, "CallingOrganizationCode",
					orgCode);

			// Invoke API getItemListForOrdering
			YRCApiContext ctx = new YRCApiContext();
			ctx.setApiName("getItemListForOrdering");
			ctx.setInputXml(itemInput);
			ctx.setFormId(this.getFormId());
			new YRCApiCaller(ctx, true).invokeApi();
			handleApiCompletion(ctx);
			if (eleGetItemList != null) {
				Element eItem = YRCXmlUtils.getChildElement(eleGetItemList,
						"Item");
				if (eItem != null) {
					Element ePrimaryInfo = YRCXmlUtils.getChildElement(eItem,
							"PrimaryInformation");
					if (ePrimaryInfo != null) {
						manfitem = ePrimaryInfo
								.getAttribute("ManufacturerItem");
						prodLine = ePrimaryInfo.getAttribute("ProductLine");
					}
					Element pPrimaryInfo = YRCXmlUtils.getChildElement(
							eleItemDetails, "PrimaryInformation");
					if (ePrimaryInfo != null) {
						pPrimaryInfo.setAttribute("ManufacturerItem", manfitem);
						pPrimaryInfo.setAttribute("ProductLine", prodLine);
					}
				}
			}
			// Fix for Defect #516
			if (prodLine.equals(HBCPCAConstants.VGC_ITEM)) {
				Text text = (Text) getControl(this.getOwnerForm(),
						"txtQuantity");
				text.setEnabled(false);

			}
		}
//Commented the Below code as part of Zero Dollar CR
		/*if (model.equals("OrderLinePrice")) {
			Element OrderLinePrice = this.getModel("OrderLinePrice");
			if (OrderLinePrice.getAttribute("ListPrice") != null)
				this.unitPrice = OrderLinePrice.getAttribute("ListPrice");
			else
				this.unitPrice = OrderLinePrice.getAttribute("UnitPrice");
		}
*/
		super.postSetModel(model);
	}

	public Element getOrderLineModel(String model) {
		return this.getTargetModel(model);
	}

	public Element getFromModel(String model) {
		return this.getModel(model);
	}

	@Override
	public YRCValidationResponse validateButtonClick(String fieldName) {
		if (fieldName.equals("extn_GWP")) {
			String strPrimeLineNO;
			Element eleOrderLines = this.getFromModel("OrderLines");
			// Element tempeleOrderLine = this.getFromModel("OrderLine");
			Element eleItemDetails = this.getFromModel("ItemDetails");
			if (eleOrderLines != null) {
				strPrimeLineNO = eleOrderLines.getAttribute("PrimeLineNo");
			} else {
				Element eleParentTransactionalLineId = this
						.getFromModel("TransactionalLineId");
				strPrimeLineNO = eleParentTransactionalLineId
						.getAttribute("TransactionalLineId");
			}
			HBCOrderEntryWizardExtnBehavior behavior = (HBCOrderEntryWizardExtnBehavior) this
					.getParentExtnBehavior();
			Element eleOrderLine = (Element) YRCXPathUtils.evaluate(behavior
					.getResponseFromAMS(),
					"OrderLines/OrderLine[@PrimeLineNo='" + strPrimeLineNO
							+ "']", XPathConstants.NODE);
			String strItemId = eleItemDetails.getAttribute("ItemID");
			HBCAddGiftItem addGiftItem = new HBCAddGiftItem(new Shell(Display
					.getDefault().getActiveShell()), SWT.NONE, eleOrderLine);
			YRCDialog dialog = new YRCDialog(addGiftItem, 500, 500,
					YRCPlatformUI
							.getFormattedString("HBC_GWP_TITLE", strItemId),
					"ApplicationTitleImage");
			dialog.open();
			Element eleOutput = ((HBCAddGiftItemBehaviour) addGiftItem
					.getBehavior()).getOutputElementMethod();
			if (eleOutput != null) {
				NodeList eleSelectedItems = (NodeList) YRCXPathUtils.evaluate(
						eleOutput, "Item[@Checked='" + "YES" + "']",
						XPathConstants.NODESET);
				for (int i = 0; i < eleSelectedItems.getLength(); i++) {
					Element elementItem = (Element) eleSelectedItems.item(i);
					if (elementItem != null
							&& elementItem.getAttribute("ItemID") != null
							&& elementItem.getAttribute("ItemID").length() > 0) {
						this.setRecentlyTriggered(true);
						this.addItems.add(elementItem.getAttribute("ItemID")
								.trim());
					}
				}
				if (this.addItems.size() > 0) {
					YRCPlatformUI
							.fireAction("com.yantra.pca.ycd.rcp.tasks.orderEntry.actions.YCDOrderEntryUpdateOrderAction");
				}
			}
		}
		return null;
	}

	public Element getGiftItems() {
		return giftItems;
	}

	public void setGiftItems(Element giftItems) {
		this.giftItems = giftItems;
	}

	public ArrayList<String> getAddItems() {
		return addItems;
	}

	public void setAddItems(ArrayList<String> addItems) {
		this.addItems = addItems;
	}

	public static Control getControl(Composite composite, String name) {
		Control[] ctrl = composite.getChildren();
		for (Control object : ctrl) {
			if (object instanceof Composite) {
				Composite cmp = (Composite) object;
				Control c = getControl(cmp, name);
				if (!YRCPlatformUI.isVoid(c))
					return c;
			} else if (name.equals(object.getData("name")))
				return object;
		}
		return null;
	}

	public boolean isRecentlyTriggered() {
		return isRecentlyTriggered;
	}

	public void setRecentlyTriggered(boolean isRecentlyTriggered) {
		this.isRecentlyTriggered = isRecentlyTriggered;
	}

	private boolean hasFreegifts(Element eleOrderLine) {
		ArrayList extnPassthroughRewardsList = YRCXmlUtils.getChildren(
				eleOrderLine, "ExtnPassthroughRewardsList");
		if (extnPassthroughRewardsList != null
				&& extnPassthroughRewardsList.size() > 0) {
			for (int i = 0; i < extnPassthroughRewardsList.size(); i++) {
				Element eleExtnPassthroughRewardsList = (Element) extnPassthroughRewardsList
						.get(i);
				if (extnPassthroughRewardsList != null) {
					ArrayList list = YRCXmlUtils
							.getChildren(eleExtnPassthroughRewardsList,
									"PassthroughRewards");
					if (list != null && list.size() > 0) {
						return true;
					}
				}
			}
		}
		return false;
	}
	//Start Zero Dollar CR - Defect#1856
	public YRCValidationResponse validateLinkClick(String fieldName) {
		if("lnkOverride".equals(fieldName)){
			Element eleItemDetails=this.getFromModel("ItemDetails");
			if(eleItemDetails!=null){
				Element eleComputePrice=YRCXmlUtils.getChildElement(eleItemDetails, "ComputedPrice");
			if(eleComputePrice!=null){
				if(!YRCPlatformUI.isVoid(eleComputePrice.getAttribute("ListPrice"))){
					this.unitPrice=eleComputePrice.getAttribute("ListPrice");
				}
				else if(!YRCPlatformUI.isVoid(eleComputePrice.getAttribute("UnitPrice"))){
					this.unitPrice=eleComputePrice.getAttribute("UnitPrice");
				}
				else{
					this.unitPrice="0.0";
				}				
			}
			}
						
		}
		return super.validateLinkClick(fieldName);
	}
//End Zero Dollar CR
	/*@Override
	 public IYRCComposite createPage(String arg0) {
	 // TODO Auto-generated method stub
	 return null;
	 }

	 @Override
	 public void pageBeingDisposed(String arg0) {
	 // TODO Auto-generated method stub

	 }*/
}
