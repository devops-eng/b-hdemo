package com.pca.rcp.hbcp2.orderentry;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.xpath.XPathConstants;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.pca.rcp.hbcp2.deliveryOptions.HBCDeliveryOptionsPage;
import com.pca.rcp.hbcp2.OrderSummary.HBCAddShippingChargesExtnBehavior;
import com.pca.rcp.hbcp2.OrderSummary.wizardPages.HBCPickupPanelExtnBehaviour;
import com.pca.rcp.hbcp2.OrderSummary.wizardPages.HBCShipmentPanelExtnBehaviour;
import com.pca.rcp.hbcp2.orderentry.screens.HBCAddGiftItem;
import com.pca.rcp.hbcp2.orderentry.screens.HBCAddGiftItemBehaviour;
import com.pca.rcp.hbcp2.orderentry.screens.HBCPreferedLanguagesPage;
import com.pca.rcp.hbcp2.orderentry.screens.HBCRoyaltyCardPage;
import com.pca.rcp.hbcp2.orderentry.screens.HBCRoyaltyCardPageExtnBehavior;
import com.pca.rcp.hbcp2.payment.HBCPaymentConfirmationPanelExtnBehavior;
import com.pca.rcp.hbcp2.util.HBCPCAConstants;
import com.pca.rcp.hbcp2.util.HBCPCAUtil;
import com.pca.rcp.hbcp2.util.XMLUtil;
import com.yantra.yfc.rcp.IYRCComposite;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCDesktopUI;
import com.yantra.yfc.rcp.YRCDialog;
import com.yantra.yfc.rcp.YRCFormatUtils;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCSharedTaskOutput;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCWizard;
import com.yantra.yfc.rcp.YRCWizardExtensionBehavior;
import com.yantra.yfc.rcp.YRCXPathUtils;
import com.yantra.yfc.rcp.YRCXmlUtils;
import com.yantra.yfc.rcp.internal.YRCApiCaller;
/**
 *
 * @author suresh , suneetha and Bala MuraliKrishna Cherukuri
 * validateButton  method will call service to get shipping charges and HBCAMSPromotionSyncWebService
 * and in changeOrder precommand we apply those charges to input xml and add new free gift lines
 */
public class HBCOrderEntryWizardExtnBehavior extends YRCWizardExtensionBehavior {
	Document docGetPricelistLineList = null;
	Element eleGetPricelistLineList = null;
	private ArrayList<String> addItems = new ArrayList<String>();
	Element responseFromAMS = null;
	String strIsPriceLocked = null;
	boolean isFreeGiftOrder = false;
	private Map<String,String> systemAvailableStatus;
	private String giftBoxItemId = "";
	String tempItemIds ="";
	public  Element docReturnOrderDetails = null;
	ArrayList gborderLineKeyList=new ArrayList();
	Document docDomain = null;
	Element eleDomain = null;
	List<String> domainName = new ArrayList<String>();
	String strRoyaltyNumber = null;
	HBCRoyaltyCardPage	royaltyCardPage =null;

	//R3 - 11-11 Release. Apply Sourcing Rule
	String sApplySourcingRule = "Y";
	String sSourcingRule = "Y";

	@Override
	public boolean preCommand(YRCApiContext ctx) {

		/**
		 * R3 - PreCommand is implemented to stamp ship node on the order line
		 * in the input of changeOrder API
 		**/
		YRCWizard wizard=(YRCWizard)YRCDesktopUI.getCurrentPage();
		String strPageID=wizard.getCurrentPageID();
		if(strPageID.equals("com.yantra.pca.ycd.rcp.tasks.orderEntry.wizardpages.YCDOrderEntryFulfillmentSummaryPage"))
		{
			if (YRCPlatformUI.equals(ctx.getApiName(), "changeOrder"))
			{
				String sShipNode = null;
				Document changeOrderInput = ctx.getInputXml();
				//R3 - modified for DSV changes to stamp extended attributes on order line - Start
				Element eShipmentOrder = null;
				Element eShipmentOrderLines = null;
				NodeList nShipmentOrderLines = null;
				//R3/3 - Start of modification to stamp ReqCancelDate for Pickup orders
				Element ePickOrder = null;
				Element ePickOrderLines = null;
				NodeList nPickOrderLines = null;
				//R3/3 - End of modification to stamp ReqCancelDate for Pickup orders
				ArrayList childExtnBehaviors = this.getChildExtnBehaviors();
				Iterator HBCAddShippingChargesExtnBehaviorList = childExtnBehaviors.iterator();
				while (HBCAddShippingChargesExtnBehaviorList.hasNext()) {
					Object ObjhbcAddShippingChargesExtnBehavior = HBCAddShippingChargesExtnBehaviorList
					.next();
					if (ObjhbcAddShippingChargesExtnBehavior instanceof HBCAddShippingChargesExtnBehavior) {
						HBCAddShippingChargesExtnBehavior hbcAddShippingChargesExtnBehavior = (HBCAddShippingChargesExtnBehavior) ObjhbcAddShippingChargesExtnBehavior;
						ArrayList Secondchilds = hbcAddShippingChargesExtnBehavior
						.getChildExtnBehaviors();
						Iterator HBCShipmentPanelExtnBehaviourList = Secondchilds
						.iterator();

						while (HBCShipmentPanelExtnBehaviourList.hasNext()) {
							Object ObjhbcShipmentPanelExtnBehaviour = HBCShipmentPanelExtnBehaviourList
							.next();
							if (ObjhbcShipmentPanelExtnBehaviour instanceof HBCShipmentPanelExtnBehaviour) {
								HBCShipmentPanelExtnBehaviour hbcShipmentPanelExtnBehaviour = (HBCShipmentPanelExtnBehaviour) ObjhbcShipmentPanelExtnBehaviour;
								eShipmentOrder = hbcShipmentPanelExtnBehaviour.getOrderModel("Order");
								if (eShipmentOrder!= null){
									eShipmentOrderLines = YRCXmlUtils.getChildElement(eShipmentOrder, "OrderLines");
									if (eShipmentOrderLines!= null){
										nShipmentOrderLines = eShipmentOrderLines.getElementsByTagName("OrderLine");
									}
								}
							}
						}
					}
				}
				//R3/3 - Start of modification to stamp ReqCancelDate for Pickup orders
				ArrayList pickChildExtnBehaviors = this.getChildExtnBehaviors();
				Iterator HBCAddShippingChargesExtnBehaviorListForPick = childExtnBehaviors.iterator();
				while (HBCAddShippingChargesExtnBehaviorListForPick.hasNext()) {
					Object ObjhbcAddShippingChargesExtnBehavior = HBCAddShippingChargesExtnBehaviorListForPick
					.next();
					if (ObjhbcAddShippingChargesExtnBehavior instanceof HBCAddShippingChargesExtnBehavior) {
						HBCAddShippingChargesExtnBehavior hbcAddShippingChargesExtnBehavior = (HBCAddShippingChargesExtnBehavior) ObjhbcAddShippingChargesExtnBehavior;
						ArrayList Secondchilds = hbcAddShippingChargesExtnBehavior
						.getChildExtnBehaviors();
						Iterator HBCPickupPanelExtnBehaviourList = Secondchilds
						.iterator();
						while (HBCPickupPanelExtnBehaviourList.hasNext()) {
							Object ObjhbcPickupPanelExtnBehaviour = HBCPickupPanelExtnBehaviourList
							.next();
							if (ObjhbcPickupPanelExtnBehaviour instanceof HBCPickupPanelExtnBehaviour) {
								HBCPickupPanelExtnBehaviour hbcPickupPanelExtnBehaviour = (HBCPickupPanelExtnBehaviour) ObjhbcPickupPanelExtnBehaviour;
								ePickOrder = hbcPickupPanelExtnBehaviour.getOrderModel("Order");
								if (ePickOrder!= null){
									ePickOrderLines = YRCXmlUtils.getChildElement(ePickOrder, "OrderLines");
									if (ePickOrderLines!= null){
										nPickOrderLines = ePickOrderLines.getElementsByTagName("OrderLine");
									}
								}

							}
						}
					}
				}
				//R3/3 - End of modification to stamp ReqCancelDate for Pickup orders

				//R3 - modified for DSV changes to stamp extended attributes on order line - End
				Element eChangeOrder = changeOrderInput.getDocumentElement();
				Element eOrderLines = YRCXmlUtils.getChildElement(eChangeOrder, "OrderLines");
				if(eOrderLines!=null)
				{
					NodeList nOrderLine = eOrderLines.getElementsByTagName("OrderLine");
					for(int t=0;t<nOrderLine.getLength();t++)
					{
						Element eOrderLine = (Element)nOrderLine.item(t);
						if(eOrderLine!=null)
						{
							Element eOrderLineReservations = YRCXmlUtils.getChildElement(eOrderLine,"OrderLineReservations");
							if(eOrderLineReservations!=null)
							{
								Element eOrderLineReservation = YRCXmlUtils.getChildElement(eOrderLineReservations,"OrderLineReservation");
								sShipNode = eOrderLineReservation.getAttribute("Node");
								//Defect Fix 1027 : Start
								String sDeliverMethod = eOrderLine.getAttribute("DeliveryMethod");
								if ("PICK".equalsIgnoreCase(sDeliverMethod))
								{
								eOrderLine.setAttribute("ShipNode", sShipNode);
								}
								//Defect Fix 1027 :End
							}
						}
						//R3 - modified for DSV changes to stamp extended attributes on order line - Start
						String strOrderLineKey = eOrderLine.getAttribute("OrderLineKey");

					//R3/3 - Start of modification to stamp ReqCancelDate for Pickup orders
					if(!YRCPlatformUI.isVoid(nPickOrderLines)){
						for(int j=0; j< nPickOrderLines.getLength(); j++ ){
							Element elePickOrderLine = (Element) nPickOrderLines.item(j);
							if(strOrderLineKey.equals(elePickOrderLine.getAttribute("OrderLineKey"))){
								String sDeliverMethod = elePickOrderLine.getAttribute("DeliveryMethod");
								if ("PICK".equalsIgnoreCase(sDeliverMethod))
								{
										eOrderLine.setAttribute("ExtnPickupDate", elePickOrderLine.getAttribute("ReqShipDate"));
										eOrderLine.removeAttribute("ReqShipDate");
								}
							}
						}
					}
					//R3/3 - End of modification to stamp ReqCancelDate for Pickup orders

						if(nShipmentOrderLines!=null && nShipmentOrderLines.getLength()!=0)
						{
						for(int iOLIdx=0; iOLIdx< nShipmentOrderLines.getLength(); iOLIdx++ ){
							Element eleShipmentOrdLine = (Element) nShipmentOrderLines.item(iOLIdx);
							if(strOrderLineKey.equals(eleShipmentOrdLine.getAttribute("OrderLineKey"))){
								// Setting Unit Cost
								Element eleShipmentOrdLineItemPrimaryInfo = YRCXmlUtils.getXPathElement(eleShipmentOrdLine,
										"/OrderLine/ItemDetails/PrimaryInformation");
								Element eleOrderLineItem = YRCXmlUtils.createChild(eOrderLine, "Item");
								eleOrderLineItem.setAttribute("UnitCost", eleShipmentOrdLineItemPrimaryInfo.getAttribute("UnitCost"));
								// Setting Size & Color
								Element eleShipmentLineAddAttr = YRCXmlUtils.getXPathElement(eleShipmentOrdLine,
										"/OrderLine/ItemDetails/AdditionalAttributeList");
								NodeList nlShipmentAddAttr = eleShipmentLineAddAttr.getElementsByTagName("AdditionalAttribute");
								Element eleOrderLineExtn = YRCXmlUtils.createChild(eOrderLine, "Extn");
								for(int iAddAttrIdx=0;iAddAttrIdx<nlShipmentAddAttr.getLength();iAddAttrIdx++){
									Element eleShipmentAddAttr = (Element) nlShipmentAddAttr.item(iAddAttrIdx);
									String strAttrName = eleShipmentAddAttr.getAttribute("Name");
									String strAttrValue = eleShipmentAddAttr.getAttribute("Value");
									if(strAttrName.equals("Color")){
										eleOrderLineExtn.setAttribute("ExtnColor", strAttrValue);
									}else if(strAttrName.equals("Size")){
										eleOrderLineExtn.setAttribute("ExtnSize", strAttrValue);
									}

								}
								}
							}
						}
						//R3 - modified for DSV changes to stamp extended attributes on order line - End
					}
				}
			}
		}

		/**
		 * R3 - PreCommand is implemented to stamp AllocationRuleID and FulfillmentType
		 * in the input of changeOrder API
		 **/
		if((strPageID.equals("com.yantra.pca.ycd.rcp.tasks.orderEntry.wizardpages.YCDAdvancedAddItemPage"))
				|| (strPageID.equals("com.yantra.pca.ycd.rcp.tasks.orderEntry.wizardpages.YCDOrderEntryAddItemPage")))
		{
			if (YRCPlatformUI.equals(ctx.getApiName(), "changeOrder"))
			{
				//--Kumar : Start (1/3) : : Check for same Pickup Store
				if(strPageID.equals("com.yantra.pca.ycd.rcp.tasks.orderEntry.wizardpages.YCDOrderEntryAddItemPage")){

					Element eleChangeOrder = ctx.getInputXml().getDocumentElement();
					XMLUtil.getElementXMLString(eleChangeOrder);
					String sPage = "AdvanceAddItemPage";
					if(!validatePickUpShipNodes(eleChangeOrder, sPage)){
						HBCPCAUtil.showOptionDialogBoxWithOK("ATTENTION !! ", YRCPlatformUI.getFormattedString("PICK_MULTI_STORE_ERROR",""));
						return false;
					}
				}
				//--Kumar : End  (1/3): : Check for same Pickup Store
				//R3 - 11-11 Release. Apply Sourcing Rule
				org.w3c.dom.Document docHBCGetCommonCodeList = YRCXmlUtils.createDocument("CommonCode");
				Element eleCommonCodeInput = docHBCGetCommonCodeList.getDocumentElement();
				//eleCommonCodeInput.setAttribute("OrganizationCode", HBCSOMConstants.DEFAULT_ORGANIZATION_CODE);
				eleCommonCodeInput.setAttribute("CodeType", HBCPCAConstants.SOURCING_RULE);
				eleCommonCodeInput.setAttribute("CodeValue", HBCPCAConstants.SOURCING_CHANNEL);
				YRCApiContext context = new YRCApiContext();
				context.setFormId(super.getFormId());
				context.setApiName("HBCGetSourcingRule");
				context.setInputXml(docHBCGetCommonCodeList);
				new YRCApiCaller(context, true).invokeApi();
				handleApiCompletion(context);
				Document changeOrderInput = ctx.getInputXml();
				Element eChangeOrder = changeOrderInput.getDocumentElement();
				String sOrganizationCode = eChangeOrder.getAttribute("EnterpriseCode");
				if(eChangeOrder!=null)
				{
					if ("LT".equalsIgnoreCase(sOrganizationCode))
					{
						eChangeOrder.setAttribute("AllocationRuleID", HBCPCAConstants.LT_ALLOCATION_RULE_ID);
					}
					if ("BAY".equalsIgnoreCase(sOrganizationCode))
					{
						eChangeOrder.setAttribute("AllocationRuleID", HBCPCAConstants.BAY_ALLOCATION_RULE_ID);
					}
				}
				Element eOrderLinesAID = YRCXmlUtils.getChildElement(eChangeOrder, "OrderLines");
				if(eOrderLinesAID!=null)
				{
					NodeList nOrderLineAID = eOrderLinesAID.getElementsByTagName("OrderLine");
					for(int t=0;t<nOrderLineAID.getLength();t++)
					{
						Element eOrderLineAID = (Element)nOrderLineAID.item(t);
						
						//AMS Upgrade Change:: setting of ExtnClearanceLevel flag
						String strAction = eOrderLineAID.getAttribute("Action") ;
						
						if(eOrderLineAID!=null)
						{
							if ("SHP".equalsIgnoreCase(eOrderLineAID.getAttribute("DeliveryMethod")))
							{
								if ("LT".equalsIgnoreCase(sOrganizationCode))
								{
									//R3 - 11-11 Release. Apply Sourcing Rule
									if (HBCPCAConstants.APPLY_SOURCING_RULE.equalsIgnoreCase(sApplySourcingRule))
									{
										eOrderLineAID.setAttribute("FulfillmentType", sSourcingRule);
									}
									else
									{
										eOrderLineAID.setAttribute("FulfillmentType", HBCPCAConstants.FT_TYPE_STANDARD);
									}
								}
								if ("BAY".equalsIgnoreCase(sOrganizationCode))
								{
									//R3 - 11-11 Release. Apply Sourcing Rule
									if (HBCPCAConstants.APPLY_SOURCING_RULE.equalsIgnoreCase(sApplySourcingRule))
									{
										eOrderLineAID.setAttribute("FulfillmentType", sSourcingRule);
									}
									else
									{
										eOrderLineAID.setAttribute("FulfillmentType", HBCPCAConstants.FT_TYPE_STANDARD);
									}
								}
							}
							//AMS Upgrade Change:: setting of ExtnClearanceLevel flag
							if(HBCPCAConstants.CREATE_NEW_LINE.equalsIgnoreCase(strAction))
							{
								Element eleItem = YRCXmlUtils.getChildElement(eOrderLineAID, "Item") ;
								String strItemID = eleItem.getAttribute("ItemID") ;
								
								String strExtnTransactionID = getItemFlags(strItemID) ;
								System.out.println("Transaction ID Value::" + strExtnTransactionID) ;
								Element eleExtn = YRCXmlUtils.createChild(eOrderLineAID, "Extn") ;
								eleExtn.setAttribute("ExtnTransactionID", strExtnTransactionID) ;
							}
						}
					}
				}
			}
		}

		if (YRCPlatformUI.equals(ctx.getApiName(), "changeOrder")) {
			setPriceLock(ctx);

			Document changeOrderInput = ctx.getInputXml();
			Element eChangeOrder = changeOrderInput.getDocumentElement();
			/*
			 * input xml change for free gift items
			 */

			if(ctx.getUserData("isRefreshReqd") != null && ctx.getUserData("isRefreshReqd").equals("true")){
				addFreeGiftItems(eChangeOrder);
			}
			/*
			 * change shipToAddress...suresh
			 */
			Element ePersonInfoShipTo = YRCXmlUtils.getChildElement(eChangeOrder, "PersonInfoShipTo");
			if(ePersonInfoShipTo!=null){
				Element eOrderLines = YRCXmlUtils.getChildElement(eChangeOrder, "OrderLines");
				if(eOrderLines!=null){
					NodeList nOrderLine = eOrderLines.getElementsByTagName("OrderLine");
					for(int p=0;p<nOrderLine.getLength();p++){
						Element eOrderLine = (Element)nOrderLine.item(p);
						if(eOrderLine!=null){
							Element eOrderLinePersonInfoShipTo = YRCXmlUtils.getChildElement(eOrderLine, "PersonInfoShipTo");
							if(eOrderLinePersonInfoShipTo!=null){
								eOrderLine.removeChild(eOrderLinePersonInfoShipTo);
							}

							YRCXmlUtils.importElement(eOrderLine, ePersonInfoShipTo);
						}
					}
				}
			}

			stampDeptCodeAtOrderLine(ctx);

			String sOverride = YRCXmlUtils.getAttribute(eChangeOrder,
					"Override");
			// check for override condition....
			if (sOverride != null && sOverride.equals("Y")) {
				// get orderlines with charges.....
				Element eNewOrder = getExtentionModel("ExtnOrder");
				if (eNewOrder != null) {
					YRCXmlUtils.importElement(eChangeOrder, YRCXmlUtils
							.getChildElement(eNewOrder, "HeaderCharges"));
					Element NewOrderLines = YRCXmlUtils.getChildElement(
							eNewOrder, "OrderLines");

					if (NewOrderLines != null) {
						NodeList nNewOrderLine = NewOrderLines
								.getElementsByTagName("OrderLine");
						// new changeorder order lines....
						Element eOrderLines = YRCXmlUtils.getChildElement(
								eChangeOrder, "OrderLines");
						NodeList nOrderLine = eOrderLines
								.getElementsByTagName("OrderLine");
						for (int i = 0; i < nOrderLine.getLength(); i++) {
							Element eOrderLine = (Element) nOrderLine.item(i);
							String sOrderLineKey = YRCXmlUtils.getAttribute(
									eOrderLine, "OrderLineKey");
							//Add GiftFlag Y if its GiftBox Line
							if(gborderLineKeyList.contains(sOrderLineKey))
								eOrderLine.setAttribute("GiftFlag", "Y");

							Element eNewOrderLine = getMatchingOrderLine(
									sOrderLineKey, nNewOrderLine);
							if (eNewOrderLine != null) {
								Element eNewLineCharges = YRCXmlUtils
										.getChildElement(eNewOrderLine,
												"LineCharges");
								if (eNewLineCharges != null) {
									// import new line chargess...
									YRCXmlUtils.importElement(eOrderLine,
											eNewLineCharges);
									Element DiscountModel = getExtentionModel("ExtnDiscountModel");
									if (DiscountModel != null) {
										NodeList nDiscountOrderLine = DiscountModel
												.getElementsByTagName("OrderLine");
										Element eDiscountOrderLine = getMatchingOrderLine(
												sOrderLineKey,
												nDiscountOrderLine);
										if (eDiscountOrderLine != null) {
											Element eLineCharges = YRCXmlUtils
													.getChildElement(
															eOrderLine,
															"LineCharges");
											if (eLineCharges != null) {
												Element eDiscountLineCharges = YRCXmlUtils
														.getChildElement(
																eDiscountOrderLine,
																"LineCharges");
												NodeList nDiscountLineCharge = eDiscountLineCharges
														.getElementsByTagName("LineCharge");
												if (nDiscountLineCharge != null
														&& nDiscountLineCharge
																.getLength() > 0) {
													for (int k = 0; k < nDiscountLineCharge
															.getLength(); k++) {
														Element eDiscountLineCharge = (Element) nDiscountLineCharge
																.item(k);
														// import discount and
														// ecofee
														if (eDiscountLineCharge != null) {
															/*
															 * fix for FIT issue
															 * 91 fixed by
															 * suresh
															 */
															if (!YRCPlatformUI
																	.equals(
																			YRCXmlUtils
																					.getAttribute(
																							eDiscountLineCharge,
																							"ChargeCategory"),
																			HBCPCAConstants.HBC_SHIPPINGSURCHARGE)
																	&& !YRCPlatformUI
																			.equals(
																					YRCXmlUtils
																							.getAttribute(
																									eDiscountLineCharge,
																									"ChargeCategory"),
																					HBCPCAConstants.HBC_SHIPPINGCHARGE)) {
																YRCXmlUtils
																		.importElement(
																				eLineCharges,
																				eDiscountLineCharge);
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				ctx.setInputXml(changeOrderInput);
				}
			} else if ("createOrder".equals(ctx.getApiName())) {
				setPriceLock(ctx);
				//This if condition is fix for GWP
				if(ctx.getUserData("isRefreshReqd") != null && ctx.getUserData("isRefreshReqd").equals("true")){
					Document changeOrderInput = ctx.getInputXml();
					Element eChangeOrder = changeOrderInput.getDocumentElement();
					addFreeGiftItems(eChangeOrder);
				}
				stampDeptCodeAtOrderLine(ctx);
			} else if ("changeOrderForPaymentMethod".equals(ctx.getApiName())) {
				String strApiNames[] = ctx.getApiNames();
				if(strApiNames.length >1 && "confirmDraftOrder".equals(strApiNames[1])){
				Document docInput = ctx.getInputXml();
				if(strRoyaltyNumber!=null && strRoyaltyNumber.length() >0){
					Element docExtn = YRCXmlUtils.createChild(docInput.getDocumentElement(), "Extn");
					docExtn.setAttribute("ExtnLoyaltyNo", strRoyaltyNumber);
					}
				ArrayList childlist = this.getChildExtnBehaviors();
				if (childlist != null) {
					for (int k = 0; k <= childlist.size(); k++) {
						if (childlist.get(k) instanceof HBCPaymentConfirmationPanelExtnBehavior) {
							HBCPaymentConfirmationPanelExtnBehavior paymentConfirmationPanelExtnBehavior = (HBCPaymentConfirmationPanelExtnBehavior) childlist
									.get(k);
							Element eleInput = docInput.getDocumentElement();
							Element elePaymentMethods = YRCXmlUtils
									.getChildElement(eleInput, "PaymentMethods");
							ArrayList paymentMethodList = YRCXmlUtils.getChildren(
									elePaymentMethods, "PaymentMethod");
							if (paymentMethodList != null
									&& paymentMethodList.size() > 0) {
								for (int i = 0; i < paymentMethodList.size(); i++) {
									Element paymentMethod = (Element) paymentMethodList
											.get(i);
									String PaymentType = paymentMethod
											.getAttribute("PaymentType");
									if (PaymentType != null
											&& PaymentType.contains("CREDIT_CARD")) {
										Element elePaymentDetailsList = YRCXmlUtils
												.getChildElement(paymentMethod,
														"PaymentDetailsList");
										if (elePaymentDetailsList != null) {
											ArrayList paymentDetailsList = YRCXmlUtils
													.getChildren(
															elePaymentDetailsList,
															"PaymentDetails");
											for (int j = 0; j < paymentDetailsList
													.size(); j++) {
												Element elePaymentDetails = (Element) paymentDetailsList
														.get(j);
												String strChargeType = elePaymentDetails
														.getAttribute("ChargeType");
												String strAuthReturnCode = elePaymentDetails
														.getAttribute("AuthReturnCode");
												if (strAuthReturnCode != null
														&& strAuthReturnCode
																.equals("APPROVED")
														&& strChargeType != null
														&& strChargeType
																.equals("AUTHORIZATION")) {
													elePaymentDetails.setAttribute("TranReturnCode",paymentConfirmationPanelExtnBehavior.getStrTranReturnCode());
													elePaymentDetails.setAttribute("TranRequestTime",elePaymentDetails.getAttribute("AuthorizationExpirationDate"));
												}
											}
										}
									}else{
										elePaymentMethods.removeChild(paymentMethod);
									}

								}
							}
							break;
						}
					}

					for(int i=0;i<childlist.size(); i++){
						if(childlist.get(i) instanceof HBCPaymentConfirmationPanelExtnBehavior){
					HBCPaymentConfirmationPanelExtnBehavior paymentConfirmationPanelExtnBehavior=(HBCPaymentConfirmationPanelExtnBehavior)childlist.get(i);
					Element elePaymentList = paymentConfirmationPanelExtnBehavior.getPaymentMethodList();
					Document inDoc = YRCXmlUtils.createDocument("PaymentMethods");
					if(elePaymentList!= null){
						ArrayList paymentMethodLsit = YRCXmlUtils.getChildren(elePaymentList, "PaymentMethod");
						boolean giftCardToCharge = false;
						String strOrderHeaderKey = "";
						String strOrderNo = "";
						for (int j = 0; j < paymentMethodLsit.size(); j++) {
									Element elePaymentMethod = (Element) paymentMethodLsit
											.get(j);
									String strPaymentType = elePaymentMethod
											.getAttribute("PaymentType");
									String strTransactionMaxChargeLimit = elePaymentMethod
									.getAttribute("TransactionMaxChargeLimit");
							strOrderHeaderKey = elePaymentMethod
							.getAttribute("OrderHeaderKey");
							strOrderNo = elePaymentMethod
							.getAttribute("OrderNo");
							double doubleTransactionMaxChargeLimit = Double
									.valueOf(
											strTransactionMaxChargeLimit)
									.doubleValue();
									if (strPaymentType.contains("GC")) {
										if (doubleTransactionMaxChargeLimit > 0) {
											giftCardToCharge = true;
											NodeList childs = elePaymentMethod
													.getChildNodes();
											for (int k = 0; k < childs.getLength(); k++) {
												Node node = childs.item(k);
												elePaymentMethod.removeChild(node);
												k--;
											}
											String strEnterpriseCode = elePaymentMethod
													.getAttribute("EnterpriseCode");
											if (strEnterpriseCode != null)
												inDoc.getDocumentElement()
														.setAttribute(
																"EnterpriseCode",
																strEnterpriseCode);
											inDoc.getDocumentElement()
											.setAttribute(
													"OrderHeaderKey",
													strOrderHeaderKey);
											inDoc.getDocumentElement()
											.setAttribute(
													"OrderNo",
													strOrderNo);
											elePaymentMethod.setAttribute(
													"ChargeType", "CHARGE");
											elePaymentMethod
													.setAttribute(
															"RequestAmount",
															elePaymentMethod
																	.getAttribute("MaxChargeLimit"));
											YRCXmlUtils.importElement(inDoc
													.getDocumentElement(),
													elePaymentMethod);
										}
									}
								}
						//API call
						if(giftCardToCharge){
						YRCApiContext yrcApiContext = new YRCApiContext();
						yrcApiContext.setApiName("executePaymentTransactions");
						yrcApiContext.setInputXml(inDoc);
						yrcApiContext.setFormId(this.getFormId());
						new YRCApiCaller(yrcApiContext, true).invokeApi();
						handleApiCompletion(yrcApiContext);
						Element extn_GiftexecutePaymentTransactions = getExtentionModel("extn_GiftexecutePaymentTransactions");
						boolean giftCardfailed = false;
						Element eleInput = docInput.getDocumentElement();
						Element elePaymentMethods = YRCXmlUtils
								.getChildElement(eleInput, "PaymentMethods");
						if(extn_GiftexecutePaymentTransactions!=null){
							ArrayList list = YRCXmlUtils.getChildren(extn_GiftexecutePaymentTransactions, "PaymentMethod");
							String reason = "Failed";
							String strSVCNo ="";
							for(int j=0;j<list.size();j++){
								Element giftCardPayment =(Element) list.get(j);
								String strPaymentTransactionFailed = giftCardPayment.getAttribute("PaymentTransactionFailed");
								if("Y".equals(strPaymentTransactionFailed)){
									 strSVCNo = giftCardPayment.getAttribute("SvcNo");
									Element elePaymentDeatils = YRCXmlUtils.getChildElement(giftCardPayment, "PaymentDetails");
									if(elePaymentDeatils!=null)
									reason = elePaymentDeatils.getAttribute("InternalReturnMessage");
									giftCardfailed = true;
									break;
								}else{
									giftCardPayment.setAttribute("UnlimitedCharges", "N");
									giftCardPayment.setAttribute("MaxChargeLimit", giftCardPayment.getAttribute("RequestAmount"));
									YRCXmlUtils.importElement(elePaymentMethods, giftCardPayment);
								}
								//YRCXmlUtils.importElement(elePaymentMethods, giftCardPayment);
							}
							if(giftCardfailed){
								//HBCPCAUtil.showOptionDialogBoxWithOK("GiftCard Charging Failed","GiftCard("+strSVCNo+") charging failed.\n Reason :-" +reason);
								String sGiftCardFailedMsg=YRCPlatformUI.getFormattedString("REALTIME_GIFTCARDCHARGE_FAILED_REASON",new String[] { strSVCNo,reason });
								YRCPlatformUI.showError(YRCPlatformUI.getFormattedString("REALTIME_GIFTCARDCHARGE_FAILED",""), sGiftCardFailedMsg);

								String apiNames[] = ctx.getApiNames();
								for (int j = 0; j < apiNames.length; j++) {
									String temp = apiNames[j];
									if(temp!=null && temp.equals("confirmDraftOrder")){
										apiNames[j] =null;
										Document document[] =ctx.getInputXmls();
										document[j] =null;
									}
								}

							}
						}else{
							YRCPlatformUI.showError(YRCPlatformUI.getFormattedString("REALTIME_GIFTCARDCHARGE_FAILED",""), YRCPlatformUI.getFormattedString("REALTIME_GIFTCARDCHARGE_ISSUES",""));
							String apiNames[] = ctx.getApiNames();
							for (int j = 0; j < apiNames.length; j++) {
								String temp = apiNames[j];
								if(temp!=null && temp.equals("confirmDraftOrder")){
									apiNames[j] =null;
									Document document[] =ctx.getInputXmls();
									document[j] =null;
								}
							}

						}
						}

					}
						}
					}


				}
				ctx.setInputXmls(ctx.getInputXmls());
			}
			}
		return super.preCommand(ctx);
	}

	/**
	 *
	 * Added free gift items to changeorder xml
	 * @param eChangeOrder
	 */

	public void addFreeGiftItems(Element eChangeOrder) {
		ArrayList childExtnBehaviors = this.getChildExtnBehaviors();
		int totalLines = childExtnBehaviors.size() + 100;
		for (int i = 0; i < childExtnBehaviors.size(); i++) {
			if (childExtnBehaviors != null
					&& childExtnBehaviors.get(i) instanceof HBCOrderEntryLinePanelExtnBehavior) {
				HBCOrderEntryLinePanelExtnBehavior entryLinePanelExtnBehavior = (HBCOrderEntryLinePanelExtnBehavior) childExtnBehaviors
						.get(i);
				if (entryLinePanelExtnBehavior.isRecentlyTriggered())
					getChangeOrderWithNewGiftItems(eChangeOrder,
							entryLinePanelExtnBehavior, totalLines);
			}
		}
		if (this.getAddItems() != null && this.getAddItems().size() > 0) {
			Element eleOrderDetails =  this.getModel("OrderDetails");
			if(eleOrderDetails!=null){

				NodeList lstChildLineOrderLines = (NodeList) YRCXPathUtils
						.evaluate(eleOrderDetails,
								"OrderLines/OrderLine[@ShipTogetherNo='"
										+ "GWP_Order" + "']",
								XPathConstants.NODESET);
				if (lstChildLineOrderLines != null) {
					for (int i = 0; i < lstChildLineOrderLines.getLength(); i++) {
						Element eleOrderLine = (Element) lstChildLineOrderLines
								.item(i);
						String strOderLineKey = eleOrderLine
								.getAttribute("OrderLineKey");
						Element eleRemoverOrderLine = (Element) YRCXPathUtils
								.evaluate(eChangeOrder,
										"OrderLines/OrderLine[@OrderLineKey='"
												+ strOderLineKey + "']",
										XPathConstants.NODE);
						eleRemoverOrderLine.setAttribute("Action", "REMOVE");
					}
				}

			}

			Element eOrderLines = YRCXmlUtils.getChildElement(eChangeOrder,
					"OrderLines");
			for (int j = 0; j < this.getAddItems().size(); j++) {
				createOrder(eChangeOrder, eOrderLines, this.getAddItems()
						.get(j), totalLines, "GWP_Order", null, null);
				totalLines++;
			}
			this.addItems.clear();

		}

	}
	/**
	 *
	 * @param eChangeOrder
	 * @param entryLinePanelExtnBehavior
	 * @param totalLines
	 * @return
	 */
	private Element getChangeOrderWithNewGiftItems(Element eChangeOrder,HBCOrderEntryLinePanelExtnBehavior entryLinePanelExtnBehavior, int totalLines) {
		Element eOrderLines = YRCXmlUtils.getChildElement(
				eChangeOrder, "OrderLines");

		Element eleParentTransactionalLineId = entryLinePanelExtnBehavior.getFromModel("TransactionalLineId");
		String parentId = eleParentTransactionalLineId.getAttribute("TransactionalLineId");
		Element eleOrderLine = (Element)YRCXPathUtils.evaluate(eChangeOrder, "OrderLines/OrderLine[@TransactionalLineId='"+parentId+"']", XPathConstants.NODE);
		String strOrderLineKey =null;
		if(eleOrderLine!=null){
			 strOrderLineKey = eleOrderLine.getAttribute("OrderLineKey");
		}
		Element eOrderLineRelationships = YRCXmlUtils.getChildElement(
				eChangeOrder, "OrderLineRelationships");
		if(eOrderLineRelationships == null){
			eOrderLineRelationships = YRCXmlUtils.createChild(
					eChangeOrder, "OrderLineRelationships");
		}else{
				if(strOrderLineKey!=null && strOrderLineKey.length() >0 ){
				ArrayList existingOrderLines = YRCXmlUtils.getChildren(eOrderLineRelationships, "OrderLineRelationship");
				if(existingOrderLines != null && existingOrderLines.size() >0){
					for (int i = 0; i < existingOrderLines.size(); i++) {
						Element tempOrderLineRelationship = (Element) existingOrderLines.get(i);
						if(tempOrderLineRelationship != null && tempOrderLineRelationship.getAttribute("RelationshipType")!=null && tempOrderLineRelationship.getAttribute("RelationshipType").equals("GWP")){

							Element eParentLine = YRCXmlUtils.getChildElement(tempOrderLineRelationship, "ParentLine");
							if(eParentLine!=null){
							String strParentOrderLineKey =  eParentLine.getAttribute("OrderLineKey");
							if(strParentOrderLineKey.equals(strOrderLineKey)){
							Element eChildLine = YRCXmlUtils.getChildElement(tempOrderLineRelationship, "ChildLine");
							if(eChildLine !=null){
								String strChildLineOrderLineKey = eChildLine.getAttribute("OrderLineKey");

								Element eleChildLineOrderLine = null;
								if(strChildLineOrderLineKey != null && strChildLineOrderLineKey.length() > 0){
									eleChildLineOrderLine = (Element)YRCXPathUtils.evaluate(eOrderLines, "OrderLine[@OrderLineKey='"+strChildLineOrderLineKey+"']", XPathConstants.NODE);
								} else{
									String strChildLineTranLineId = eChildLine.getAttribute("TransactionalLineId");
									eleChildLineOrderLine = (Element)YRCXPathUtils.evaluate(eOrderLines, "OrderLine[@TransactionalLineId='"+strChildLineTranLineId+"']", XPathConstants.NODE);
								}
								if(eleChildLineOrderLine!= null ){
									eleChildLineOrderLine.setAttribute("Action", "REMOVE");
									tempOrderLineRelationship.setAttribute("Operation", "Delete");
									entryLinePanelExtnBehavior.setRecentlyTriggered(false);
								}
							}
							}
							}
						}

					}
				}
			}
		}
		for(int k=0;k<entryLinePanelExtnBehavior.getAddItems().size();k++){
			createOrder(eChangeOrder,eOrderLines,entryLinePanelExtnBehavior.getAddItems().get(k),totalLines,"GWP_OrderLine",eOrderLineRelationships,parentId);
			totalLines++;
		}
		return eChangeOrder;
	}

	/**
	 *
	 * @param changeOrder
	 * @param eOrderLines
	 * @param ItemID
	 * @param totalLines
	 * @param giftLevel
	 * @param eOrderLineRelationships
	 * @param parentId
	 */

	private void createOrder(Element changeOrder, Element eOrderLines,String ItemID, int totalLines, String giftLevel, Element eOrderLineRelationships, String parentId) {
		Element eOrderLine = YRCXmlUtils.createChild(eOrderLines, "OrderLine");
		eOrderLine.setAttribute("Action", "CREATE");
		//eOrderLine.setAttribute("DeliveryMethod", "DEL");
		eOrderLine.setAttribute("ItemGroupCode", "PROD");
		eOrderLine.setAttribute("ShipNode", "");
		eOrderLine.setAttribute("TransactionalLineId", String.valueOf(totalLines));
		eOrderLine.setAttribute("ValidateItem", "N");
		//eOrderLine.setAttribute("LineType", "Gift Item"); ShipTogetherNo
		eOrderLine.setAttribute("ShipTogetherNo", giftLevel);
		Element eItem = YRCXmlUtils.createChild(eOrderLine, "Item");
		eItem.setAttribute("ItemID", ItemID);
		eItem.setAttribute("UnitOfMeasure", "EACH");
		eItem.setAttribute("ProductClass", "");
		Element eOrderLineTranQuantity = YRCXmlUtils.createChild(eOrderLine, "OrderLineTranQuantity");
		eOrderLineTranQuantity.setAttribute("OrderedQty", "1.0");
		eOrderLineTranQuantity.setAttribute("TransactionalUOM", "EACH");
		Element eLinePriceInfo = YRCXmlUtils.createChild(eOrderLine, "LinePriceInfo");
		eLinePriceInfo.setAttribute("IsPriceLocked", "Y");
		eLinePriceInfo.setAttribute("ListPrice", "0.00");
		eLinePriceInfo.setAttribute("UnitPrice", "0.00");
		if(eOrderLineRelationships!=null){
		Element eOrderLineRelationship = YRCXmlUtils.createChild(eOrderLineRelationships, "OrderLineRelationship");
		eOrderLineRelationship.setAttribute("RelationshipType", "GWP");
		Element eParentLine = YRCXmlUtils.createChild(eOrderLineRelationship, "ParentLine");
		eParentLine.setAttribute("TransactionalLineId",parentId );
		Element eChildLine = YRCXmlUtils.createChild(eOrderLineRelationship, "ChildLine");
		eChildLine.setAttribute("TransactionalLineId", String.valueOf(totalLines));
		}
	}
	/**
	 * @param orderLineKey
	 * @param newOrderLines
	 * @return matching requested line using order line key
	 */
	private Element getMatchingOrderLine(String orderLineKey,
			NodeList newOrderLines) {

		Element MatchingElement = null;
		String lineKey = "";
		if (newOrderLines != null) {

			for (int j = 0; j < newOrderLines.getLength(); j++) {
				Element eNewOrderLine = (Element) newOrderLines.item(j);
				lineKey = YRCXmlUtils.getAttribute(eNewOrderLine,
						"OrderLineKey");
				if (lineKey != null && lineKey.equals(orderLineKey)) {
					MatchingElement = eNewOrderLine;
				}
			}
		}

		return MatchingElement;
	}

	/**
	 * @param apiContext
	 */
	private void setPriceLock(YRCApiContext apiContext) {
		Document docCreateOrder = apiContext.getInputXml();
		Element eleCreateOrder = docCreateOrder.getDocumentElement();
		Element orderLines = YRCXmlUtils.getChildElement(eleCreateOrder,
				"OrderLines");
		ArrayList<Element> orderLineList = YRCXmlUtils.getChildren(orderLines,
				"OrderLine");
		for (int ordline = 0; ordline < orderLineList.size(); ordline++) {
			Element eOrderLineInput = (Element) orderLineList.get(ordline);
			if (eOrderLineInput != null) {
				Element eleLinePriceInfoInput = YRCXmlUtils.getChildElement(
						eOrderLineInput, "LinePriceInfo");
				if (eleLinePriceInfoInput != null) {
					strIsPriceLocked = eleLinePriceInfoInput
							.getAttribute("IsPriceLocked");
					if (strIsPriceLocked.equals("N")) {
						eleLinePriceInfoInput
								.setAttribute("IsPriceLocked", "Y");
					}
				}
			}
		}
	}

	@Override
	public void postCommand(YRCApiContext yrcapicontext) {

		String strOrganizationCode = "";
		String strItemId = "";
		Date currentDate = new Date();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String sCurrentDate = formatter.format(currentDate);
		//Start: defect 284
		String sEndDateActiveQryType = "GE";
		String sStartDateActiveQryType = "LE";
		//End: defect 284

		if (YRCPlatformUI.equals("getItemListForOrderingWithoutPrice",
				yrcapicontext.getApiName())) {
			Document docGetItemListForOrderWithOutPrice = yrcapicontext
					.getOutputXml();
			Element eleGetItemListForOrderWithOutPrice = docGetItemListForOrderWithOutPrice
					.getDocumentElement();

			ArrayList<Element> nItems = YRCXmlUtils.getChildren(
					eleGetItemListForOrderWithOutPrice, "Item");
			// NodeList nItems =
			// eleGetItemListForOrderWithOutPrice.getElementsByTagName("Item");

			Element docUser = getModel("UserNameSpace");
			/*R3 - DEF 574 - As there can be multiple ECOM and MerchLocator items with similar ItemID's, the existing
			code has been modified to fetch the price of the first retrieved ECOM item*/
			//if (nItems.size() == 1) {
			for (int t=0; t<1; t++){
				Element eItem = (Element) nItems.get(0);
				// Element eItem = (Element) nItems.item(0);

				if (HBCPCAUtil.isStoreApplication()) {
					strOrganizationCode = YRCXmlUtils.getAttribute(docUser,
							"BusinessKey");
				} else {
					strItemId = YRCXmlUtils.getAttribute(eItem, "ItemID");
					strOrganizationCode = YRCXmlUtils.getAttribute(eItem,
							"OrganizationCode");
				}

				Document inputxml = YRCXmlUtils.createDocument("PricelistLine");
				Element elePricelistLine = inputxml.getDocumentElement();
				YRCXmlUtils.setAttribute(elePricelistLine, "ItemID", strItemId);
				YRCXmlUtils.setAttribute(elePricelistLine,
						"SellerOrganizationCode", strOrganizationCode);
				YRCXmlUtils.setAttribute(elePricelistLine,
						"EndDateActiveQryType", sEndDateActiveQryType);
				YRCXmlUtils.setAttribute(elePricelistLine,
						"StartDateActiveQryType", sStartDateActiveQryType);
				YRCXmlUtils.setAttribute(elePricelistLine, "EndDateActive",
						sCurrentDate);
				YRCXmlUtils.setAttribute(elePricelistLine, "StartDateActive",
						sCurrentDate);
				YRCXmlUtils.setAttribute(elePricelistLine, "PricingStatus",
						"ACTIVE");

				YRCApiContext yrcApiContext = new YRCApiContext();
				yrcApiContext.setApiName("getPricelistLineList");
				yrcApiContext.setInputXml(inputxml);
				yrcApiContext.setFormId(this.getFormId());

				new YRCApiCaller(yrcApiContext, true).invokeApi();
				handleApiCompletion(yrcApiContext);

				if (eleGetPricelistLineList != null) {
					ArrayList nPricelistLine = YRCXmlUtils.getChildren(
							eleGetPricelistLineList, "PricelistLine");
					// call createLeastPriceListLine method...
					if (nPricelistLine.size() > 0) {
						Element eLeastPriceListLine = createLeastPriceListLine(nPricelistLine);
						String strLeastUnitPrice = eLeastPriceListLine
								.getAttribute("ListPrice");

						Element eleComputedPrice = YRCXmlUtils.getChildElement(
								eItem, "ComputedPrice");
						YRCXmlUtils.setAttribute(eleComputedPrice, "UnitPrice",
								strLeastUnitPrice);
						YRCXmlUtils.setAttribute(eleComputedPrice, "ListPrice",
								strLeastUnitPrice);

					}
				}
			}
		}else if (YRCPlatformUI.equals("getSalesOrderDetailsWithAddressesAndOrderLines",yrcapicontext.getApiName())){

			Document docOrderDetails = yrcapicontext.getOutputXml();
			Element eleorderDetials = docOrderDetails.getDocumentElement();
			if(this.responseFromAMS ==null){
			// AMS call
			//Document docOrderDetails = yrcapicontext.getOutputXml();
			//Element eleorderDetials = docOrderDetails.getDocumentElement();
			Element eleOrderLines = YRCXmlUtils.getChildElement(eleorderDetials, "OrderLines");
			Document docOrder = YRCXmlUtils.createDocument("Order");
			Element eleOrder = docOrder.getDocumentElement();
			Element OrderLines = YRCXmlUtils.createChild(eleOrder, "OrderLines");
			eleOrder.setAttribute("SellerOrganizationCode", eleorderDetials.getAttribute("SellerOrganizationCode"));
			eleOrder.setAttribute("EnterpriseCode", eleorderDetials.getAttribute("EnterpriseCode"));
			String strOrderNo =  eleorderDetials.getAttribute("OrderNo");
			if(strOrderNo.length() > 9)
				eleOrder.setAttribute("OrderNo",strOrderNo.substring(strOrderNo.length()-9, strOrderNo.length()) );
			else
				eleOrder.setAttribute("OrderNo",strOrderNo);
			eleOrder.setAttribute("BillToID", eleorderDetials.getAttribute("BillToID"));
			eleOrder.setAttribute("PendingChangesUserID",eleorderDetials.getAttribute("PendingChangesUserID"));
			String sMaxOrderStatus = eleorderDetials.getAttribute("MaxOrderStatus");
			if(sMaxOrderStatus!=null && !sMaxOrderStatus.equals("")){
				eleOrder.setAttribute("Status", sMaxOrderStatus);
			}else{
				eleOrder.setAttribute("Status", "1000");
			}
			boolean paymentDone =false;

			ArrayList orderLinelist = YRCXmlUtils.getChildren(eleOrderLines, "OrderLine");

			for (int i = 0; i < orderLinelist.size(); i++) {
				Element eleOrderLineInput = (Element) orderLinelist.get(i);
				Element eleItemInput = YRCXmlUtils.getChildElement(eleOrderLineInput, "Item");
				Element eleExtnInput = YRCXmlUtils.getChildElement(eleOrderLineInput, "Extn");
				Element eleLinePriceInfoInput = YRCXmlUtils.getChildElement(eleOrderLineInput, "LinePriceInfo");
				Element eletempOrderLineTranQuantity = YRCXmlUtils.getChildElement(eleOrderLineInput, "OrderLineTranQuantity");
				String strShipTogetherNo =  eleOrderLineInput.getAttribute("ShipTogetherNo");
				if(strShipTogetherNo != null && (strShipTogetherNo.equals("GWP_Order") || strShipTogetherNo.equals("GWP_OrderLine")) )
					continue;
				Element eleOrderLine = YRCXmlUtils.createChild(OrderLines, "OrderLine");
				eleOrderLine.setAttribute("OrderedQty", eletempOrderLineTranQuantity.getAttribute("OrderedQty"));
				eleOrderLine.setAttribute("PrimeLineNo", eleOrderLineInput.getAttribute("PrimeLineNo"));
				eleOrderLine.setAttribute("GiftFlag", eleOrderLineInput.getAttribute("GiftFlag"));

				Element eleItem = YRCXmlUtils.createChild(eleOrderLine, "Item");
				eleItem.setAttribute("ItemID", eleItemInput.getAttribute("ItemID"));
				Element eleExtn = YRCXmlUtils.createChild(eleOrderLine,"Extn");
				eleExtn.setAttribute("ExtnMinOrderFlag","1");
				
				//AMS Upgrade Change:: setting of ExtnClearanceLevel flag
				eleExtn.setAttribute("ExtnClearanceLevel", eleExtnInput.getAttribute("ExtnTransactionID")) ;
				
				Element eleLinePriceInfo = YRCXmlUtils.createChild(eleOrderLine, "LinePriceInfo");
				eleLinePriceInfo.setAttribute("IsPriceLocked", eleLinePriceInfoInput.getAttribute("IsPriceLocked"));
				eleLinePriceInfo.setAttribute("UnitPrice", eleLinePriceInfoInput.getAttribute("UnitPrice"));

				Element elePaymentMethods = YRCXmlUtils.getChildElement(
						eleorderDetials, "PaymentMethods");
					if (elePaymentMethods != null && !paymentDone) {
						Document paymentDoc = YRCXmlUtils.createDocument("PaymentMethods");
						NodeList tempPaymentList =elePaymentMethods.getElementsByTagName("PaymentMethod");
						if(tempPaymentList!=null && tempPaymentList.getLength()>0){
							for (int j = 0; j < tempPaymentList.getLength(); j++) {
								Element tempPaymentmethod = (Element) tempPaymentList.item(j);
								Element  paymentMethod = YRCXmlUtils.createChild(paymentDoc.getDocumentElement(), "PaymentMethod");
								paymentMethod.setAttribute("CreditCardType", tempPaymentmethod.getAttribute("CreditCardType"));
								paymentMethod.setAttribute("RequestedChargeAmount", tempPaymentmethod.getAttribute("MaxChargeLimit"));
								paymentMethod.setAttribute("CreditCardNo", "");
							}
						}
						XMLUtil.addDocument(docOrder, paymentDoc, false);
						paymentDone = true;
					}


			}

			ArrayList tempOrderLines = YRCXmlUtils.getChildren(eleOrderLines, "OrderLine");
			if(tempOrderLines!=null && tempOrderLines.size() >0){
				String systemStatusAMS = "Y";
				/*Check whether systemAvailableStatus map is empty or not */
				if(XMLUtil.isVoid(systemAvailableStatus) || systemAvailableStatus.size() == 0){
					getSystemAvalilableStatus();
				}
				if(!XMLUtil.isVoid(systemAvailableStatus) && systemAvailableStatus.size() > 0){
					systemStatusAMS = (String)systemAvailableStatus.get(HBCPCAConstants.SYSTEM_NAME_AMS);
				}
				if(systemStatusAMS.toUpperCase().equals("Y")){
					YRCApiContext yrcApiContext = new YRCApiContext();
					yrcApiContext.setApiName("HBCAMSPromotionSyncWebService");
					//Start defect# 286 -Doubling GWP
					Element eleOrderout=docOrder.getDocumentElement();
					Element eleOrderLine=YRCXmlUtils.getChildElement(eleOrderout, "OrderLines");
					String [] attrNames=new String []{"PrimeLineNo"};
					YRCXmlUtils.sortNumericChildren(eleOrderLine, attrNames, true);		
					//End defect# 286 -Doubling GWP
					yrcApiContext.setInputXml(docOrder);
					yrcApiContext.setFormId(this.getFormId());
					new YRCApiCaller(yrcApiContext, true).invokeApi();
					handleApiCompletion(yrcApiContext);
				}
			}
			}else{
				Element eleOrderLine = (Element)YRCXPathUtils.evaluate(this.responseFromAMS, "OrderLines/OrderLine[@PrimeLineNo='"+"0"+"']", XPathConstants.NODE);
				Button buttonGWP = (Button)getControl(this.getOwnerForm(), "extn_OrderGWP");
				buttonGWP.setImage(YRCPlatformUI
						.getImage("/icons/ui_unchecked.gif"));
				buttonGWP.setToolTipText(YRCPlatformUI
						.getString("ORDER_GIFT_ITEMS"));
				buttonGWP.setCursor(new Cursor(buttonGWP.getDisplay(), 21));
				buttonGWP.setData("name", "extn_OrderGWP");
				if(eleOrderLine!=null && hasFreegifts(eleOrderLine)){
					buttonGWP.setVisible(true);
					isFreeGiftOrder=true;
				}
				else
					buttonGWP.setVisible(false);
			}



		}else if (YRCPlatformUI.equals("changeOrderForPaymentMethod",
				yrcapicontext.getApiName())){
			if(yrcapicontext.getInvokeAPIStatus() > 0){
				yrcapicontext.setInvokeAPIStatus(1);
			}
		}

		String currentPageID = fetchCurrentPageID();
        if("com.yantra.pca.ycd.rcp.tasks.orderEntry.wizardpages.YCDOrderEntryFulfillmentSummaryPage".equalsIgnoreCase(currentPageID))
        {
		 if (yrcapicontext.getApiName().equals("changeOrder"))
		 {
			//--Kumar : Start (4/4) : : Check for same Pickup Store

			Element eleChangeOrder = yrcapicontext.getOutputXml().getDocumentElement();
			XMLUtil.getElementXMLString(eleChangeOrder);
			String sPage = "FullfillmentSummaryPage";
			boolean isDeliveryMethodPick=false;
			boolean isPersonInfoShipToDiff=false;
			if(!validatePickUpShipNodes(eleChangeOrder, sPage)){
				HBCPCAUtil.showOptionDialogBoxWithOK("ATTENTION !! ", YRCPlatformUI.getFormattedString("PICK_MULTI_STORE_ERROR",""));
			}
			//--Kumar : End  (4/4): : Check for same Pickup Store
			 Document indoc = yrcapicontext.getOutputXml();
			 Element eleOrderLineforShipNode = null;


			 Element eleOrder = indoc.getDocumentElement();
			 Document docChangeOrder = YRCXmlUtils.createDocument("Order");
			 Element eleChangeOrderforPick = docChangeOrder.getDocumentElement();
			 eleChangeOrderforPick.setAttribute("OrderHeaderKey",YRCXmlUtils.getAttribute(eleOrder, "OrderHeaderKey") );
			 Element eleOrderLines = YRCXmlUtils.getChildElement(eleOrder, "OrderLines");
			 Element elePersonInfoShipTo=YRCXmlUtils.getChildElement(eleOrder, "PersonInfoShipTo");
			 String strPersonID="";

			 if(elePersonInfoShipTo!= null)
			 {
				 strPersonID=YRCXmlUtils.getAttribute(elePersonInfoShipTo, "PersonInfoKey");
			 }

			 //Updating the PersonInfoShipTo at the OrderLine Level.
			 if(eleOrderLines != null)
			 {
				 Element eleChangeOrderlines = YRCXmlUtils.createChild(eleChangeOrderforPick, "OrderLines");
				 NodeList nOrderLine = eleOrderLines.getElementsByTagName("OrderLine");
				 Element elePersonInfoShipToLine = YRCXmlUtils.getChildElement((Element) nOrderLine.item(0), "PersonInfoShipTo");
				 String strPersonIDLine="";
				 if(elePersonInfoShipToLine!= null)
				 {
					 strPersonIDLine=YRCXmlUtils.getAttribute(elePersonInfoShipToLine, "PersonInfoKey");
				 }
				for(int iOLx=0;iOLx<nOrderLine.getLength();iOLx++ )
				{

				Element eleChangeOrderline = YRCXmlUtils.createChild(eleChangeOrderlines, "OrderLine");
				Element eleOrderLine = (Element) nOrderLine.item(iOLx);
				String strDeliveryMethod= YRCXmlUtils.getAttribute(eleOrderLine, "DeliveryMethod");

				 if(strDeliveryMethod.equals("PICK"))
				 {
					 isDeliveryMethodPick=true;
					 Element eleHeaderCharges = YRCXmlUtils.getChildElement(eleChangeOrderforPick, "HeaderCharges");
					 if(eleHeaderCharges == null)
					 {
						 eleHeaderCharges = YRCXmlUtils.createChild(eleChangeOrderforPick, "HeaderCharges");
					 Element eleHeaderCharge = YRCXmlUtils.createChild(eleHeaderCharges, "HeaderCharge");
					 Element eleExtn = YRCXmlUtils.createChild(eleHeaderCharge, "Extn");
					 eleHeaderCharge.setAttribute("ChargeCategory", "SHIPPINGCHARGE");
					 eleHeaderCharge.setAttribute("ChargeName", "SHIPPINGCHARGE");
					 eleExtn.setAttribute("ExtnOriginalAmount", "0.0");
					 }
					 eleChangeOrderline.setAttribute("OrderLineKey", YRCXmlUtils.getAttribute(eleOrderLine, "OrderLineKey") );
					 updatePersonInfoShipTo(docChangeOrder, eleOrderLine, eleChangeOrderline);
					 eleOrderLineforShipNode=eleOrderLine;
				 }
				 }
				 if(eleOrderLineforShipNode!=null)
				 {
					 updatePersonInfoShipTo(docChangeOrder, eleOrderLineforShipNode, eleChangeOrderforPick);
				 }
				 else if (!strPersonIDLine.equals(strPersonID))
				 {
			//Updating the PersonInfoShipTo at the Order Level.
					isPersonInfoShipToDiff=true;
					 Element eleTmpPersonInfoShipTo = XMLUtil.appendChild(docChangeOrder, eleChangeOrderforPick, "PersonInfoShipTo",null);
						XMLUtil.copyElement(docChangeOrder, elePersonInfoShipToLine, eleTmpPersonInfoShipTo);
						XMLUtil.removeChild(eleChangeOrderforPick, eleChangeOrderlines);
				 }
				 }


			 if(isDeliveryMethodPick || isPersonInfoShipToDiff)
			 {
				YRCApiContext context = new YRCApiContext();
	            context.setApiName("changeOrderforPICK");
	            context.setFormId(this.getFormId());
	            context.setInputXml(docChangeOrder);
	            YRCApiCaller apiCaller1 = new YRCApiCaller(context, true);
	            apiCaller1.invokeApi();
			 }
		 }
		 }


		super.postCommand(yrcapicontext);

	}

	public void handleApiCompletion(YRCApiContext ctx) {
		if (YRCPlatformUI.equals(ctx.getApiName(), "getPricelistLineList")) {
			docGetPricelistLineList = ctx.getOutputXml();
			eleGetPricelistLineList = docGetPricelistLineList
					.getDocumentElement();

		} else if (YRCPlatformUI.equals(ctx.getApiName(),
				"HBCGetShippingEligibilityService")) {
			Document docBasket = ctx.getOutputXml();

			if (docBasket != null) {
				setExtentionModel("ExtnEligibilityBasket", docBasket
						.getDocumentElement());

			}
		} else if (YRCPlatformUI.equals(ctx.getApiName(),
				"HBCGetShippingQuotationService")) {
			Document docBasket = ctx.getOutputXml();

			if (docBasket != null) {
				setExtentionModel("ExtnQuotationBasket", docBasket
						.getDocumentElement());

			}
		}else if (YRCPlatformUI.equals(ctx.getApiName(),
		"executePaymentTransactions")) {
			Document doc = ctx.getOutputXml();
			if(doc!=null && doc.getDocumentElement() !=null)
			setExtentionModel("extn_GiftexecutePaymentTransactions", doc.getDocumentElement());
			/*YRCApiContext yrcApiContext = new YRCApiContext();
			yrcApiContext.setApiName("changeOrder");
			yrcApiContext.setInputXml(docBasket);
			yrcApiContext.setFormId(this.getFormId());
			new YRCApiCaller(yrcApiContext, true).invokeApi();
			handleApiCompletion(yrcApiContext);*/

		}else if (YRCPlatformUI.equals(ctx.getApiName(),
		"changeOrderForPaymentMethod")) {
			Document doc = ctx.getOutputXml();
			if(doc!=null && doc.getDocumentElement() !=doc)
			setExtentionModel("extn_GiftchangeOrderForPaymentMethod", doc.getDocumentElement());
			/*YRCApiContext yrcApiContext = new YRCApiContext();
			yrcApiContext.setApiName("changeOrder");
			yrcApiContext.setInputXml(docBasket);
			yrcApiContext.setFormId(this.getFormId());
			new YRCApiCaller(yrcApiContext, true).invokeApi();
			handleApiCompletion(yrcApiContext);*/


		}else if(YRCPlatformUI.equals(ctx.getApiName(),
		"HBCAMSPromotionSyncWebService")){
			Document docPromotions = ctx.getOutputXml();
			if (docPromotions != null) {
				setExtentionModel("ExtnAMSPromostions", docPromotions
						.getDocumentElement());
				this.responseFromAMS = docPromotions.getDocumentElement();
				Element eleOrderLine = (Element)YRCXPathUtils.evaluate(docPromotions.getDocumentElement(), "OrderLines/OrderLine[@PrimeLineNo='"+"0"+"']", XPathConstants.NODE);
				Button buttonGWP = (Button)getControl(this.getOwnerForm(), "extn_OrderGWP");
				buttonGWP.setImage(YRCPlatformUI
						.getImage("/icons/ui_unchecked.gif"));
				buttonGWP.setToolTipText(YRCPlatformUI
						.getString("ORDER_GIFT_ITEMS"));
				buttonGWP.setCursor(new Cursor(buttonGWP.getDisplay(), 21));
				buttonGWP.setData("name", "extn_OrderGWP");
				if(eleOrderLine!=null && hasFreegifts(eleOrderLine)){
					buttonGWP.setVisible(true);
					isFreeGiftOrder=true;
				}
				else
					buttonGWP.setVisible(false);
				/*
				 * Depending on Icon Click have to decide where we have to put the reponse
				 */
				ArrayList childExtnBehaviors = this.getChildExtnBehaviors();
				for (int i = 0; i < childExtnBehaviors.size(); i++) {
					if(childExtnBehaviors.get(i) instanceof HBCOrderEntryLinePanelExtnBehavior){
					HBCOrderEntryLinePanelExtnBehavior entryLinePanelExtnBehavior = (HBCOrderEntryLinePanelExtnBehavior) childExtnBehaviors.get(i);
					String strPrimLineNo;
					Element eleOrderLines = entryLinePanelExtnBehavior.getFromModel("OrderLines");
					if(eleOrderLines != null)
					{
						 strPrimLineNo = eleOrderLines.getAttribute("PrimeLineNo");
					}else{
						Element eleTransactionalLineId = entryLinePanelExtnBehavior.getFromModel("TransactionalLineId");
						strPrimLineNo = eleTransactionalLineId.getAttribute("TransactionalLineId");
					}
					eleOrderLine = (Element)YRCXPathUtils.evaluate(docPromotions.getDocumentElement(), "OrderLines/OrderLine[@PrimeLineNo='"+strPrimLineNo+"']", XPathConstants.NODE);
					buttonGWP = (Button)getControl(entryLinePanelExtnBehavior.getOwnerForm(), "extn_GWP");
					buttonGWP.setImage(YRCPlatformUI
							.getImage("/icons/ui_unchecked.gif"));
					buttonGWP.setToolTipText(YRCPlatformUI
							.getString("GIFT_ITEMS"));
					buttonGWP.setCursor(new Cursor(buttonGWP.getDisplay(), 21));
					buttonGWP.setData("name", "extn_GWP");
					if(eleOrderLine!=null && hasFreegifts(eleOrderLine)){
						buttonGWP.setVisible(true);
						isFreeGiftOrder=true;
					}
					else
						buttonGWP.setVisible(false);
					}
				}
			}
		}

		else if (YRCPlatformUI.equals(ctx.getApiName(), "getCommonCodeList")) {
				docDomain = ctx.getOutputXml();
				eleDomain = docDomain.getDocumentElement();
				NodeList domainNameList = eleDomain.getElementsByTagName("CommonCode");
				for (int p = 0; p < domainNameList.getLength(); p++) {
					Element commonCode = (Element) domainNameList.item(p);
					domainName.add(commonCode.getAttribute("CodeShortDescription"));
			}
		   }

		//R3 - 11-11 Release. Apply Sourcing Rule
		else if (YRCPlatformUI.equals(ctx.getApiName(), "HBCGetSourcingRule")) {
			Document docGetSourcingRule = ctx.getOutputXml();
			Element eleGetSourcingRule = docGetSourcingRule.getDocumentElement();
			NodeList nGetSourcingRuleList = eleGetSourcingRule.getElementsByTagName("CommonCode");
			for (int p = 0; p < nGetSourcingRuleList.getLength(); p++) {
				Element eleCommonCode = (Element) nGetSourcingRuleList.item(p);
				sApplySourcingRule = eleCommonCode.getAttribute("CodeShortDescription");
				if (HBCPCAConstants.APPLY_SOURCING_RULE.equalsIgnoreCase(sApplySourcingRule))
				{
					sSourcingRule = eleCommonCode.getAttribute("CodeLongDescription");
				}
			}
		}


		super.handleApiCompletion(ctx);

	}

	@Override
	public void postSetModel(String model) {
		if("ReturnOrderDetails".equals(model)){
			if(this.docReturnOrderDetails!=null){
				Element eleOriginalReturnOrderDetails = getModel(model);

				String total = "";
				Element elePriceInfo = YRCXmlUtils.getChildElement(this.docReturnOrderDetails, "PriceInfo");
				total = elePriceInfo.getAttribute("TotalAmount");
				Element eleTempPriceInfo = YRCXmlUtils.getChildElement(eleOriginalReturnOrderDetails, "PriceInfo");
				if (eleTempPriceInfo != null)eleTempPriceInfo.setAttribute("TotalAmount", total);

				Element OverallTotals = YRCXmlUtils.getChildElement(this.docReturnOrderDetails, "OverallTotals");
				if (OverallTotals != null) {
					Element eleTempOverallTotals = YRCXmlUtils.getChildElement(eleOriginalReturnOrderDetails,"OverallTotals");
					if (eleTempOverallTotals != null) {
						eleTempOverallTotals.setAttribute("GrandCharges",OverallTotals.getAttribute("GrandCharges"));
						eleTempOverallTotals.setAttribute("GrandDiscount",OverallTotals.getAttribute("GrandDiscount"));
						eleTempOverallTotals.setAttribute("GrandTax",OverallTotals.getAttribute("GrandTax"));
						eleTempOverallTotals.setAttribute("GrandTotal",OverallTotals.getAttribute("GrandTotal"));
						eleTempOverallTotals.setAttribute("LineSubTotal",OverallTotals.getAttribute("LineSubTotal"));
						eleTempOverallTotals.setAttribute("HdrCharges",OverallTotals.getAttribute("HdrCharges"));
						eleTempOverallTotals.setAttribute("HdrDiscount",OverallTotals.getAttribute("HdrDiscount"));
						eleTempOverallTotals.setAttribute("HdrTax",OverallTotals.getAttribute("HdrTax"));

					}
				}
				this.docReturnOrderDetails = null;
				repopulateModel(model);
			}
		}

		super.postSetModel(model);

	}
	/**
	 *
	 * @param composite
	 * @param name
	 * @return
	 */
	public static Control getControl(Composite composite, String name) {
		Control[] ctrl = composite.getChildren();
		for(Control object:ctrl){
		if (object instanceof Composite) {
		Composite cmp = (Composite) object;
		Control c = getControl(cmp, name);
		if(!YRCPlatformUI.isVoid(c))
		return c;
		}
		else if(name.equals(object.getData("name")))
		return object;
		}
		return null;
		}
	/**
	 * @param pricelistLine
	 * @return price list line having least unit price
	 */
	private Element createLeastPriceListLine(ArrayList npricelistLine) {

		Element ePricelistLineLeast = (Element) npricelistLine.get(0);
		Double dunitPrice = Double.parseDouble(YRCXmlUtils.getAttribute(
				ePricelistLineLeast, "UnitPrice"));
		Double dtemp = 0.0;

		for (int i = 0; i < npricelistLine.size(); i++) {
			Element ePricelistLine = (Element) npricelistLine.get(i);

			dtemp = Double.parseDouble(YRCXmlUtils.getAttribute(ePricelistLine,
					"UnitPrice"));
			if (dtemp <= dunitPrice) {
				dunitPrice = dtemp;
				ePricelistLineLeast = (Element) npricelistLine.get(i);
			}

		}

		return ePricelistLineLeast;
	}

	/**
	 * @param itemID
	 * @param responseLineItem
	 * @return matching response line using order line key
	 */
	private Element getMatchingResLineItem(String orderLineKey,
			NodeList responseLineItem) {

		Element matchingElement = null;
		if (responseLineItem != null) {
			for (int i = 0; i < responseLineItem.getLength(); i++) {
				Element eleResponseLineItem = (Element) responseLineItem
						.item(i);
				Element elineItemID = YRCXmlUtils.getChildElement(
						eleResponseLineItem, "lineItemID");
				if (elineItemID != null) {
					String tempLineItemID = ((Node) elineItemID)
							.getTextContent();
					if (tempLineItemID != null
							&& YRCPlatformUI.equals(tempLineItemID,
									orderLineKey)) {
						matchingElement = eleResponseLineItem;
					}
				}
			}
		}
		return matchingElement;

	}

	@Override
	public YRCValidationResponse validateButtonClick(String fieldName) {
		YRCValidationResponse response = null;
		boolean email_VGCLineFlag = true;
		if(fieldName != null && fieldName.equals("extn_OrderGWP")){
			Element eleOrderLine = (Element)YRCXPathUtils.evaluate(this.getResponseFromAMS(), "OrderLines/OrderLine[@PrimeLineNo='"+"0"+"']", XPathConstants.NODE);
			HBCAddGiftItem addGiftItem = new HBCAddGiftItem(new Shell(Display.getDefault().getActiveShell()),SWT.NONE,eleOrderLine);
			YRCDialog dialog = new YRCDialog(addGiftItem, 500, 500, YRCPlatformUI.getFormattedString("GWP_FREEGIFTS_SELECT",""), "ApplicationTitleImage");
			dialog.open();
			Element eleOutput = ((HBCAddGiftItemBehaviour)addGiftItem.getBehavior()).getOutputElementMethod();
			if(eleOutput!=null){
				NodeList eleSelectedItems = (NodeList)YRCXPathUtils.evaluate(eleOutput, "Item[@Checked='"+"YES"+"']", XPathConstants.NODESET);
			for (int i = 0; i < eleSelectedItems.getLength(); i++) {
				Element elementItem = (Element) eleSelectedItems.item(i);
				if(elementItem!=null && elementItem.getAttribute("ItemID")!=null && elementItem.getAttribute("ItemID").length()>0){
					this.addItems.add(elementItem.getAttribute("ItemID").trim());
				}
			}
			if(this.addItems.size() >0){
				YRCPlatformUI.fireAction("com.yantra.pca.ycd.rcp.tasks.orderEntry.actions.YCDOrderEntryUpdateOrderAction");
			}
			}
			return null;
		}else if(fieldName != null && fieldName.equals("buttonNext")){

			//Defect# 976 start
			String formID = fetchCurrentPageID();
			if(YRCPlatformUI.equals(formID, "com.yantra.pca.ycd.rcp.tasks.orderEntry.wizardpages.YCDOrderEntryAddItemPage")){

				if(validateVGCLine() && validateFakeEmailIDForVGCLine()){
					email_VGCLineFlag = false;
				}
			}
			//Start:defect #511
			Element eleOrderDtls = this.getModel("OrderDetails");
			Element eleShipTo = YRCXmlUtils.getChildElement(eleOrderDtls,"PersonInfoShipTo");
			Element eleBillTo = YRCXmlUtils.getChildElement(eleOrderDtls,"PersonInfoBillTo");
			String strShipToCountry= eleShipTo.getAttribute("Country");
			String strBilltoCountry=eleBillTo.getAttribute("Country");
		    String strShipToZipCode=eleShipTo.getAttribute("ZipCode");
		    String strBillToZipCode=eleBillTo.getAttribute("ZipCode");
		    Pattern pc = Pattern.compile("[A-Z][0-9][A-Z] [0-9][A-Z][0-9]"); 
			Matcher mShipTo = pc.matcher(strShipToZipCode);
			Matcher mBillTo = pc.matcher(strBillToZipCode);
		    if (strShipToCountry.equals("CA") && !mShipTo.matches() || strBilltoCountry.equals("CA") && !mBillTo.matches())
		    {
		    	 YRCPlatformUI.showError(YRCPlatformUI.getFormattedString("ADDRESS ERROR",""), YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_INVALID_ADDRESS",""));
					response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"");
					return response;
		    }
		    Pattern pc_US = Pattern.compile("\\d{5}((-)?\\d{4})?"); 
			Matcher mPostalCode_shipTo_US = pc_US.matcher(strShipToZipCode);
			//Start:Defect# 738 
			Matcher mPostalCode_billTo_US = pc_US.matcher(strBillToZipCode);
			//End:Defect# 738
		    if (strShipToCountry.equals("US") && !mPostalCode_shipTo_US.matches() ||  strBilltoCountry.equals("US") && !mPostalCode_billTo_US.matches())
		    {
		    	 YRCPlatformUI.showError(YRCPlatformUI.getFormattedString("ADDRESS ERROR",""), YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_INVALID_US_ADDRESS",""));
					response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"");
					return response;
		    }	  
		    
		      	//End:Defect# 511 
				if(email_VGCLineFlag){

			//Defect# 976 end
		ArrayList childExtnBehaviors = this.getChildExtnBehaviors();
		if(YRCPlatformUI.equals(formID, "com.yantra.pca.ycd.rcp.tasks.orderEntry.wizardpages.YCDOrderEntryAddItemPage")){
//			Start Zero Dollar CR
			boolean zeroPriceItem=findZeroDollarPriceItem(childExtnBehaviors);
			if(zeroPriceItem){
				YRCPlatformUI.showError(YRCPlatformUI.getFormattedString("ITEM_ERROR",""), YRCPlatformUI.getFormattedString("ZERO_DOLLAR_PRICE_ITEM_NEXT",""));
				response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"");
				return response;
			}
//			End Zero Dollar CR
			for (int i = 0; i < childExtnBehaviors.size(); i++) {
				if(childExtnBehaviors.get(i) instanceof HBCCustomerSearchPageExtnBehavior ){
				HBCCustomerSearchPageExtnBehavior customerSearchPageExtnBehavior= (HBCCustomerSearchPageExtnBehavior)this.getChildExtnBehaviors().get(i);
				Element eleSelectedCustomer =customerSearchPageExtnBehavior.getFromModel("selectedCustomer");
				if(eleSelectedCustomer != null)
				{
					Element eleCustomerContactList = XMLUtil.getChildElement(eleSelectedCustomer, "CustomerContactList");
					if(eleCustomerContactList!=null){
						ArrayList customerContactList = YRCXmlUtils.getChildren(eleCustomerContactList, "CustomerContact");
						for (int k = 0; k < customerContactList.size(); k++) {
							Element eleCustomerContact = (Element)customerContactList.get(k);
							if(eleCustomerContact != null){
								//Start 538 - Verifying the preferredlanguage is present or not for the customer
								String CustomerContactID=eleCustomerContact.getAttribute("CustomerContactID");
								//End 538
								boolean isCustomerPrefLang = false;
								Element eleExtn = XMLUtil.getChildElement(eleCustomerContact, "Extn");
								if(eleExtn != null){
									String strExtnPreferredLanguage = eleExtn.getAttribute("ExtnPreferredLanguage");
									if(strExtnPreferredLanguage!=null && strExtnPreferredLanguage.length()>0){
										isCustomerPrefLang = true;
									}
								}
								if(!isCustomerPrefLang && !customerSearchPageExtnBehavior.existingCustomerWithoutPreflanf){
									//Start 538 - Call getCustomerContactListService1 service to check the customer extn preferred language
									String strPrefLang=null;
									if(CustomerContactID!=null && !CustomerContactID.isEmpty()){
										Document customecontactInput = YRCXmlUtils.createDocument("CustomerContact");
										customecontactInput.getDocumentElement().setAttribute("CustomerContactID", CustomerContactID);
										YRCApiContext yrcApiContext = new YRCApiContext();
										yrcApiContext.setApiName("getCustomerContactListService");
										yrcApiContext.setInputXml(customecontactInput);
										yrcApiContext.setFormId(this.getFormId());
										yrcApiContext.setUserData("apiCalledFromRule", "Y");
										callApi(yrcApiContext);
										Element elementCustomerContactList=yrcApiContext.getOutputXml().getDocumentElement();
										if(elementCustomerContactList!=null){
										Element elementContact=YRCXmlUtils.getChildElement(elementCustomerContactList,"CustomerContact");
										if (elementContact != null) {
											Element elementExtn = YRCXmlUtils.getChildElement(elementContact,"Extn");										
												if (elementExtn != null){
												strPrefLang = elementExtn.getAttribute("ExtnPreferredLanguage");
												}
											}
										}
									}
									if(CustomerContactID==null || strPrefLang==null || strPrefLang.isEmpty()){
									YRCPlatformUI.showError(YRCPlatformUI.getString("CONSUMER_MISSING_INFORMATION"), YRCPlatformUI.getString("COPY_ORDER_ADD_ITEM_PREF_LANG_MESSAGE"));
									response = new YRCValidationResponse(
											YRCValidationResponse.YRC_VALIDATION_ERROR,
											"");
									return response;
									}
									// End 538
								}
							}
						}
					}
					break;
					}
				}
			}
			/****
			 *    Call to Get AMS Response
			 */
			processAMSRequestCall(childExtnBehaviors);
			Element eleAMSPromotion = this.getExtentionModel("ExtnAMSPromostions");

			//Fix for 536 issue start
			Element eleOrderDetails = this.getModel("OrderDetails");
				ArrayList<String> itemListfromOrderDetails = new ArrayList<String>();

				Element eleOrderLines = YRCXmlUtils.getChildElement(
						eleOrderDetails, "OrderLines");
				if (eleOrderLines != null) {
					ArrayList orderLineArray = YRCXmlUtils.getChildren(
							eleOrderLines, "OrderLine");
					if (orderLineArray != null && orderLineArray.size() > 0) {
						for (int i = 0; i < orderLineArray.size(); i++) {
							Element eleOrderLine = (Element) orderLineArray
									.get(i);
							String strShipTogetherNo = eleOrderLine
									.getAttribute("ShipTogetherNo");
							Element eleItem = YRCXmlUtils.getChildElement(
									eleOrderLine, "Item");
							String ItemID = eleItem.getAttribute("ItemID");
							if (strShipTogetherNo.equals("GWP_OrderLine") || strShipTogetherNo.equals("GWP_Order")) {
								itemListfromOrderDetails.add(ItemID);
							}

						}
					}
				}
				Element eleOrderLinesfromAMS = YRCXmlUtils.getChildElement(
						eleAMSPromotion, "OrderLines");
				NodeList nodePassthroughRewardsList=null;
				if (eleOrderLinesfromAMS != null) {
					ArrayList orderLineArraylst = YRCXmlUtils.getChildren(
							eleOrderLinesfromAMS, "OrderLine");
					if (orderLineArraylst != null
							&& orderLineArraylst.size() > 0) {
						boolean atLeastOneFreeGiftChosen = false;
						for (int i = 0; i < orderLineArraylst.size(); i++) {
							Element eleOrderLinefromAMS = (Element) orderLineArraylst
									.get(i);
							NodeList nodeExtnPassthroughRewardsList = eleOrderLinefromAMS
									.getElementsByTagName("ExtnPassthroughRewardsList");
							if (nodeExtnPassthroughRewardsList != null) {
								for (int j = 0; j < nodeExtnPassthroughRewardsList
										.getLength(); j++) {
									Element eleExtnPassthroughRewardsList = (Element) nodeExtnPassthroughRewardsList
											.item(j);
									String eleextnMaxSelections = eleExtnPassthroughRewardsList
											.getAttribute("ExtnMaxSelections");
									if (eleextnMaxSelections != null
											&& eleextnMaxSelections != "") {
										 nodePassthroughRewardsList = eleExtnPassthroughRewardsList
												.getElementsByTagName("PassthroughRewards");
										if (nodePassthroughRewardsList != null) {
											if(nodePassthroughRewardsList.getLength() > 0){
												atLeastOneFreeGiftChosen = false;
											}
											for (int k = 0; k < nodePassthroughRewardsList
													.getLength(); k++) {
												Element elePassthroughRewards = (Element) nodePassthroughRewardsList
														.item(k);
												String GiftItemId = elePassthroughRewards
														.getAttribute("GiftItemId");
												if (itemListfromOrderDetails
														.contains(GiftItemId)) {
													atLeastOneFreeGiftChosen = true;
													break;
												}
											}
											if(atLeastOneFreeGiftChosen){
												break;
											}
										}
									}
								}
							}
							if (atLeastOneFreeGiftChosen) {
								isFreeGiftOrder = false;
							} else {
								isFreeGiftOrder = true;
								break;
							}
						}
					}
				}////Fix for 536 issue End

			if(eleAMSPromotion != null && nodePassthroughRewardsList != null && nodePassthroughRewardsList.getLength()>0 && isFreeGiftOrder){
				YRCSharedTaskOutput sharedTaskOutput = showOptionDialogBoxWithSkip("Message",YRCPlatformUI.getFormattedString("MESSAGE_WHEN_GIFTITEM", ""));
				//*
				Element ele =sharedTaskOutput.getOutput();
				String strOutput = ele.getAttribute("SelectedOptionKey");
				if(strOutput.equals("Skip")){
					/*
					 * Subset Checking
					 */

				ArrayList<String> extraGiftItems = new ArrayList<String>();
			//	Element eleOrderDetails =	this.getModel("OrderDetails");
			//	Element eleOrderLines = YRCXmlUtils.getChildElement(eleOrderDetails, "OrderLines");
				if(eleOrderLines!=null){
					ArrayList orderLineList =  YRCXmlUtils.getChildren(eleOrderLines, "OrderLine");
					if(orderLineList != null && orderLineList.size() >0){
						ArrayList<String> orderGiftItemsList = new ArrayList<String>();
						for (int i = 0; i < orderLineList.size(); i++) {
							Element eleOrderLine = (Element)orderLineList.get(i);
							String strShipTogetherNo = eleOrderLine.getAttribute("ShipTogetherNo");
							if((!strShipTogetherNo.equals("GWP_Order") && !strShipTogetherNo.equals("GWP_OrderLine"))){

								String strOrderLinekey = eleOrderLine.getAttribute("OrderLineKey");

								NodeList orderLineRelationshiplist = (NodeList)YRCXPathUtils.evaluate(eleOrderDetails, "OrderLineRelationships/OrderLineRelationship[@ParentOrderLineKey='"+strOrderLinekey+"']", XPathConstants.NODESET);
								if(orderLineRelationshiplist!=null && orderLineRelationshiplist.getLength() >0){
									ArrayList<String> lineGiftItemsList = new ArrayList<String>();
									ArrayList<String> giftItemsList = new ArrayList<String>();
									for (int j = 0; j < orderLineRelationshiplist.getLength(); j++) {
										Element eleOrderLineRelationship = (Element)orderLineRelationshiplist.item(j);
										if(eleOrderLineRelationship!=null && eleOrderLineRelationship.getAttribute("RelationshipType")!=null &&  eleOrderLineRelationship.getAttribute("RelationshipType").equals("GWP") ){
											String strChildLineKey = eleOrderLineRelationship.getAttribute("ChildOrderLineKey");
											Element eleChildOrderLine = (Element)YRCXPathUtils.evaluate(eleOrderDetails, "OrderLines/OrderLine[@OrderLineKey='"+strChildLineKey+"']", XPathConstants.NODE);
											Element eleItem =  YRCXmlUtils.getChildElement(eleChildOrderLine, "Item");
											if(eleItem!=null){
												lineGiftItemsList.add(eleItem.getAttribute("ItemID"));
											}
										}
									}
									if(lineGiftItemsList.size() >0){
									String strPrimeLineNo = eleOrderLine.getAttribute("PrimeLineNo");
									Element eletempOrderLine = (Element)YRCXPathUtils.evaluate(this.responseFromAMS, "OrderLines/OrderLine[@PrimeLineNo='"+strPrimeLineNo+"']", XPathConstants.NODE);
									if(eletempOrderLine!=null){
										//Element eExtnPassthroughRewardsList = YRCXmlUtils.getChildElement(eletempOrderLine, "ExtnPassthroughRewardsList");
										ArrayList extnPassthroughRewardsList = YRCXmlUtils.getChildren(eletempOrderLine, "ExtnPassthroughRewardsList");

										if(extnPassthroughRewardsList!=null && extnPassthroughRewardsList.size()>0){
											for (int l = 0; l < extnPassthroughRewardsList.size(); l++) {
												Element eleExtnPassthroughRewardsList = (Element) extnPassthroughRewardsList.get(l);
												if(eleExtnPassthroughRewardsList!=null){
													NodeList nPassthroughRewards = eleExtnPassthroughRewardsList.getElementsByTagName("PassthroughRewards");
													if(nPassthroughRewards!=null && nPassthroughRewards.getLength() >0){
														String sGiftItemId = "";
														for(int j=0;j<nPassthroughRewards.getLength();j++){
															Element ePassthroughRewards = (Element)nPassthroughRewards.item(j);
															if(ePassthroughRewards!=null){
																sGiftItemId = YRCXmlUtils.getAttribute(ePassthroughRewards, "GiftItemId").trim();
																//giftItemsList.add(sGiftItemId);
																lineGiftItemsList.remove(sGiftItemId);
															}
														}
													}
													}
												}
											}
										}
									}
									extraGiftItems.addAll(lineGiftItemsList);
								}
							}else{
								if(strShipTogetherNo.equals("GWP_Order")){
									Element eleItem =  YRCXmlUtils.getChildElement(eleOrderLine, "Item");
									orderGiftItemsList.add(eleItem.getAttribute("ItemID"));
								}
							}
						}
						if(orderGiftItemsList.size() >0){
							Element eletempOrderLine = (Element)YRCXPathUtils.evaluate(this.responseFromAMS, "OrderLines/OrderLine[@PrimeLineNo='"+"0"+"']", XPathConstants.NODE);
							if(eletempOrderLine!=null){
								//Element eExtnPassthroughRewardsList = YRCXmlUtils.getChildElement(eletempOrderLine, "ExtnPassthroughRewardsList");
								ArrayList extnPassthroughRewardsList = YRCXmlUtils.getChildren(eletempOrderLine, "ExtnPassthroughRewardsList");
								ArrayList<String> giftItemsList = new ArrayList<String>();
								if(extnPassthroughRewardsList!=null && extnPassthroughRewardsList.size()>0){
									for (int j = 0; j < extnPassthroughRewardsList.size(); j++) {
										Element eleExtnPassthroughRewardsList = (Element) extnPassthroughRewardsList.get(j);
										if(eleExtnPassthroughRewardsList!=null){
										NodeList nPassthroughRewards = eleExtnPassthroughRewardsList.getElementsByTagName("PassthroughRewards");
										if(nPassthroughRewards!=null && nPassthroughRewards.getLength() >0){
											String sGiftItemId = "";
											for(int k=0;k<nPassthroughRewards.getLength();k++){
												Element ePassthroughRewards = (Element)nPassthroughRewards.item(k);
												if(ePassthroughRewards!=null){
													sGiftItemId = YRCXmlUtils.getAttribute(ePassthroughRewards, "GiftItemId").trim();
													//giftItemsList.add(sGiftItemId);
													orderGiftItemsList.remove(sGiftItemId);
												}
											}
										}
										}
									}
								}
							}
						}
						extraGiftItems.addAll(orderGiftItemsList);
					}
				}
				if(extraGiftItems.size()>0){
					String temp ="";
					for (int i = 0; i < extraGiftItems.size(); i++) {
						temp = temp + extraGiftItems.get(i) + ",";
					}
					HBCPCAUtil.showOptionDialogBoxWithOK("Message",YRCPlatformUI.getFormattedString("MESSAGE_WHEN_WRONG_GIFTITEMS", temp.substring(0, temp.length()-1)));
					response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"");
					return response;

				}
					response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_OK,"");
					return response;
				}else{
					response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"");
					return response;
				}
			}else{
				response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_OK,"");
				return response;
			}
		}else if(YRCPlatformUI.equals(formID, "com.yantra.pca.ycd.rcp.tasks.orderEntry.wizardpages.YCDOrderEntryChangeDeliveryOptionsPage")){
			//-- Kumar : : Start (2/3) : : Check for same Pickup Store
			String sPage = "PICKUP_NODE_CHECK";
			Element eleChangeOrder = null;
			if(!validatePickUpShipNodes(eleChangeOrder, sPage)){
				HBCPCAUtil.showOptionDialogBoxWithOK("ATTENTION !! ", YRCPlatformUI.getFormattedString("PICK_MULTI_STORE_ERROR",""));
				response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"");
				return response;
			}
			//-- Kumar : : End (2/3) : : Check for same Pickup Store
			Iterator HBCChangeDeliveryOptionsPageCompositeExtnBehaviorList = childExtnBehaviors.iterator();
			while (HBCChangeDeliveryOptionsPageCompositeExtnBehaviorList.hasNext()) {
				Object ObjhBCChangeDeliveryOptionsPageCompositeExtnBehaviorList = HBCChangeDeliveryOptionsPageCompositeExtnBehaviorList.next();
				if (ObjhBCChangeDeliveryOptionsPageCompositeExtnBehaviorList instanceof HBCChangeDeliveryOptionsPageCompositeExtnBehavior) {
					HBCChangeDeliveryOptionsPageCompositeExtnBehavior hbcChangeDeliveryOptionsPageCompositeExtnBehavior = (HBCChangeDeliveryOptionsPageCompositeExtnBehavior) ObjhBCChangeDeliveryOptionsPageCompositeExtnBehaviorList;
					Element eleSalesOrder = hbcChangeDeliveryOptionsPageCompositeExtnBehavior.getFromModel("getSalesOrderDetails");
					if(eleSalesOrder != null){
						String strEnterpriseCode = eleSalesOrder.getAttribute("EnterpriseCode");
						String strCountryCode = "US";
						String strCountry = "USA";
						String strPlace = "Canadian";
						if(HBCPCAConstants.HBC_LT_BANNER.equals(strEnterpriseCode)){
							strCountryCode = "CA";
							strCountry = "Canada";
							strPlace = "US";

						}else if(HBCPCAConstants.HBC_BAY_BANNER.equals(strEnterpriseCode)){
							 strCountryCode = "US";
							 strCountry = "USA";
							 strPlace = "Canadian";
						}

							NodeList lstPersonInfoShipTo = (NodeList) YRCXPathUtils.evaluate(eleSalesOrder,"OrderLines/OrderLine/PersonInfoShipTo[@Country='"+ strCountryCode + "']",XPathConstants.NODESET);
							if(lstPersonInfoShipTo!=null && lstPersonInfoShipTo.getLength() >0){
								YRCPlatformUI.showError(YRCPlatformUI.getString("CONSUMER_MISSING_INFORMATION"), YRCPlatformUI.getFormattedString("SHIP_TO_VALIDATION_MESSAGE",new String[] { strCountry,strEnterpriseCode,strPlace }));
								response = new YRCValidationResponse(
										YRCValidationResponse.YRC_VALIDATION_ERROR,
										"");
								return response;
							}

					}

				}
			}
		}
		else if(YRCPlatformUI.equals(formID, "com.yantra.pca.ycd.rcp.tasks.orderEntry.wizardpages.YCDOrderEntryFulfillmentSummaryPage")){
		Document orders = YRCXmlUtils.createDocument("Orders");
		Element eOrders = orders.getDocumentElement();
		Element eOrderModel = null;
		double dTotalCharges = 0.0;
		double dHeaderChargeAmt = 0.0;
		String sTotalCharges = "";
		//R3 - This code has been added to enable gift card validation for pick up orders
		Element eModelOrder = this.getModel("Order");
		NodeList nModelOrderLine = eModelOrder.getElementsByTagName("OrderLine");
		int iPickupOrder = 0;
//		for (int l=0; l<nModelOrderLine.getLength();l++)
//		{
//			Element eModelOrderLine = (Element) nModelOrderLine.item(l);
//			if (eModelOrderLine.getAttribute("DeliveryMethod").equalsIgnoreCase("PICK")){
//				iPickupOrder++;
//			}
//		}

		ArrayList SecondchildsAvailable = this.getChildExtnBehaviors();
		Iterator HBCUpdateModelList = SecondchildsAvailable.iterator();
		while (HBCUpdateModelList.hasNext()) {
			Object ObjhbcUpdartModelExtnBehaviour = HBCUpdateModelList.next();
			if (ObjhbcUpdartModelExtnBehaviour instanceof HBCAddShippingChargesExtnBehavior) {
				HBCAddShippingChargesExtnBehavior hbcUpdateModelExtnBehaviour =
					(HBCAddShippingChargesExtnBehavior) ObjhbcUpdartModelExtnBehaviour;
				Element eOriginalOrderModel = hbcUpdateModelExtnBehaviour.getOrderModel("OriginalOrder");
				if(eOriginalOrderModel!=null)
				{
					nModelOrderLine = eOriginalOrderModel.getElementsByTagName("OrderLine");
					for (int l=0; l<nModelOrderLine.getLength();l++)
					{
						Element eModelOrderLine = (Element) nModelOrderLine.item(l);
						if (eModelOrderLine.getAttribute("DeliveryMethod").equalsIgnoreCase("PICK"))
						{
							iPickupOrder++;
						}
					}
				}
			}
			}
//		getOrderModel


		if (iPickupOrder>0){
			Iterator HBCAddShippingChargesExtnBehaviorList = childExtnBehaviors.iterator();
			while (HBCAddShippingChargesExtnBehaviorList.hasNext()) {
				Object ObjhbcAddShippingChargesExtnBehavior = HBCAddShippingChargesExtnBehaviorList
						.next();
				if (ObjhbcAddShippingChargesExtnBehavior instanceof HBCAddShippingChargesExtnBehavior) {
					HBCAddShippingChargesExtnBehavior hbcAddShippingChargesExtnBehavior = (HBCAddShippingChargesExtnBehavior) ObjhbcAddShippingChargesExtnBehavior;
					ArrayList Secondchilds = hbcAddShippingChargesExtnBehavior
							.getChildExtnBehaviors();
					Iterator HBCPickupPanelExtnBehaviourList = Secondchilds
							.iterator();
					while (HBCPickupPanelExtnBehaviourList.hasNext()) {
						Object ObjhbcPickupPanelExtnBehaviour = HBCPickupPanelExtnBehaviourList
								.next();
						if (ObjhbcPickupPanelExtnBehaviour instanceof HBCPickupPanelExtnBehaviour) {
							HBCPickupPanelExtnBehaviour hbcPickupPanelExtnBehaviour = (HBCPickupPanelExtnBehaviour) ObjhbcPickupPanelExtnBehaviour;
							if (!YRCPlatformUI.isVoid(eOrderModel)) {
								eOrderModel = hbcPickupPanelExtnBehaviour
										.getOrderModel("Order");
								Element eNextOrderLines = YRCXmlUtils
										.getChildElement(eOrderModel, "OrderLines");
								Element eNextOrderLine = YRCXmlUtils
										.getChildElement(eNextOrderLines,
												"OrderLine");

								if (eNextOrderLine != null) {
									Element eOrder = YRCXmlUtils.getChildElement(
											eOrders, "Order");
									Element eOrderLines = YRCXmlUtils
											.getChildElement(eOrder, "OrderLines");
									YRCXmlUtils.importElement(eOrderLines,
											eNextOrderLine);
								}
							} else {
								eOrderModel = hbcPickupPanelExtnBehaviour
										.getOrderModel("Order");
								YRCXmlUtils.importElement(eOrders, eOrderModel);
							}

						}
					}
				}
			}

			if(validateGiftWrapAndGiftBox(eOrderModel)){
				//return response;
			}
			else{
				response = new YRCValidationResponse(
				YRCValidationResponse.YRC_VALIDATION_ERROR, "");
				return response;
			}
		}
		else
		{
		//ArrayList firstchilds = this.getChildExtnBehaviors();
		Iterator HBCAddShippingChargesExtnBehaviorList = childExtnBehaviors.iterator();
		while (HBCAddShippingChargesExtnBehaviorList.hasNext()) {
			Object ObjhbcAddShippingChargesExtnBehavior = HBCAddShippingChargesExtnBehaviorList
					.next();
			if (ObjhbcAddShippingChargesExtnBehavior instanceof HBCAddShippingChargesExtnBehavior) {
				HBCAddShippingChargesExtnBehavior hbcAddShippingChargesExtnBehavior = (HBCAddShippingChargesExtnBehavior) ObjhbcAddShippingChargesExtnBehavior;
				ArrayList Secondchilds = hbcAddShippingChargesExtnBehavior
						.getChildExtnBehaviors();
				Iterator HBCShipmentPanelExtnBehaviourList = Secondchilds
						.iterator();

				while (HBCShipmentPanelExtnBehaviourList.hasNext()) {
					Object ObjhbcShipmentPanelExtnBehaviour = HBCShipmentPanelExtnBehaviourList
							.next();
					if (ObjhbcShipmentPanelExtnBehaviour instanceof HBCShipmentPanelExtnBehaviour) {
						HBCShipmentPanelExtnBehaviour hbcShipmentPanelExtnBehaviour = (HBCShipmentPanelExtnBehaviour) ObjhbcShipmentPanelExtnBehaviour;
						if (!YRCPlatformUI.isVoid(eOrderModel)) {
							eOrderModel = hbcShipmentPanelExtnBehaviour
									.getOrderModel("Order");
							Element eNextOrderLines = YRCXmlUtils
									.getChildElement(eOrderModel, "OrderLines");
							Element eNextOrderLine = YRCXmlUtils
									.getChildElement(eNextOrderLines,
											"OrderLine");

							if (eNextOrderLine != null) {
								Element eOrder = YRCXmlUtils.getChildElement(
										eOrders, "Order");
								Element eOrderLines = YRCXmlUtils
										.getChildElement(eOrder, "OrderLines");
								YRCXmlUtils.importElement(eOrderLines,
										eNextOrderLine);
							}
						} else {
							eOrderModel = hbcShipmentPanelExtnBehaviour
									.getOrderModel("Order");
							YRCXmlUtils.importElement(eOrders, eOrderModel);
						}

					}

				}
			}

		}

		Element eOrder = YRCXmlUtils.getChildElement(eOrders, "Order");
		if (eOrder != null) {

			// get Order lines....
			Element eOrderLines = YRCXmlUtils.getChildElement(eOrder,
					"OrderLines");

			NodeList nOrderLine = eOrderLines.getElementsByTagName("OrderLine");
			// check for carriesservice selection.............
			if(validateGiftWrapAndGiftBox(eOrder)){
				//if(isGiftBoxLine(nOrderLine)){
					if (isShippingMethodSelected(nOrderLine)) {
					if (isSameShippingMethodSelected(nOrderLine)) {

					try {
						Document inputxml = XMLUtil
								.getDocumentForElement(eOrder);

						// call HBCGetShippingQuotationService to get service
						// types...
						YRCApiContext yrcApiContext = new YRCApiContext();
						yrcApiContext
								.setApiName("HBCGetShippingQuotationService");
						yrcApiContext.setInputXml(inputxml);
						yrcApiContext.setFormId(this.getFormId());
						new YRCApiCaller(yrcApiContext, true).invokeApi();
						handleApiCompletion(yrcApiContext);

						// get extension model
						Element eleQuotationBasket = getExtentionModel("ExtnQuotationBasket");
						if (eleQuotationBasket != null
								&& YRCPlatformUI.equals(eleQuotationBasket
										.getTagName(), "basket")) {
							NodeList nResponseLineItem = eleQuotationBasket
									.getElementsByTagName("ResponseLineItem");

							double dqty = 0.0;
							double dShippingChargePerUnit = 0.0;
							double dEcoFeePerUnit = 0.0;
							double dSurChargePerUnit = 0.0;

							String strShippingMethod = "";
							String tempvar = "";
							// create document for discount and ecofee...
							Element DiscountModel = getExtentionModel("ExtnDiscountModel");
							Element eDiscountOrderLines = null;
							if (DiscountModel == null) {
								Document DiscountOrderLines = YRCXmlUtils
										.createDocument("DiscountOrderLines");
								eDiscountOrderLines = DiscountOrderLines
										.getDocumentElement();
							}
							// create Document Linecharges...
							Document DOrder = YRCXmlUtils
									.createDocument("Order");
							Element eNewOrder = DOrder.getDocumentElement();
							Element eHeaderCharges = YRCXmlUtils.createChild(
									eNewOrder, "HeaderCharges");
							YRCXmlUtils.setAttribute(eHeaderCharges, "Reset",
									"Y");
							Element eNewOrderLines = YRCXmlUtils.createChild(
									eNewOrder, "OrderLines");

							for (int i = 0; i < nOrderLine.getLength(); i++) {

								Element eOrderLine = (Element) nOrderLine
										.item(i);
								String sOrderLineKey = YRCXmlUtils
										.getAttribute(eOrderLine,
												"OrderLineKey");
								Element eLineCharges = YRCXmlUtils
										.getChildElement(eOrderLine,
												"LineCharges");

								// create orderline in new discount extn model..
								if (eDiscountOrderLines != null) {
									Element eDiscountOrderLine = YRCXmlUtils
											.createChild(eDiscountOrderLines,
													"OrderLine");
									YRCXmlUtils.setAttribute(
											eDiscountOrderLine, "OrderLineKey",
											sOrderLineKey);
									YRCXmlUtils.importElement(
											eDiscountOrderLine, eLineCharges);
								}

								// create orderline in new extn model..
								Element eNewOrderLine = YRCXmlUtils
										.createChild(eNewOrderLines,
												"OrderLine");
								YRCXmlUtils.setAttribute(eNewOrderLine,
										"OrderLineKey", sOrderLineKey);

								// create line charges....
								Element eNewLineCharges = YRCXmlUtils
										.createChild(eNewOrderLine,
												"LineCharges");
								YRCXmlUtils.setAttribute(eNewLineCharges,
										"Reset", "Y");

								String sCarrierServiceCode = YRCXmlUtils
										.getAttribute(eOrderLine,
												"CarrierServiceCode");

								if (!tempvar.equals(sCarrierServiceCode)) {
									strShippingMethod = strShippingMethod
											+ sCarrierServiceCode;
								}
								tempvar = sCarrierServiceCode;

								String OrdQty = YRCXmlUtils.getAttribute(
										eOrderLine, "OrderedQty");
								dqty = Double.parseDouble(OrdQty);

								// get matching responseline item from basket...
								Element matchingResLineItem = getMatchingResLineItem(
										sOrderLineKey, nResponseLineItem);
								if (matchingResLineItem != null) {

									/*
									 * // get eco fee......... Element
									 * eEcoFeeAmount = YRCXmlUtils
									 * .getChildElement(matchingResLineItem,
									 * "ecoFeeAmount"); if (eEcoFeeAmount !=
									 * null) { String sEcoFee = ((Node)
									 * eEcoFeeAmount) .getTextContent(); if
									 * (sEcoFee != null && !sEcoFee.equals("")) {
									 * double tempEcoFee = Double
									 * .parseDouble(sEcoFee); if (dqty != 0.0) {
									 * dEcoFeePerUnit = tempEcoFee / dqty;
									 * dTotalCharges = dTotalCharges +
									 * tempEcoFee; Element eLineCharge =
									 * YRCXmlUtils .createChild(eLineCharges,
									 * "LineCharge"); YRCXmlUtils.setAttribute(
									 * eLineCharge, "ChargeCategory",
									 * "SURCHARGE"); YRCXmlUtils.setAttribute(
									 * eLineCharge, "ChargeName", "Ecofee
									 * Surcharge"); YRCXmlUtils.setAttribute(
									 * eLineCharge, "ChargePerUnit",
									 * dEcoFeePerUnit); } } }
									 */
									// get shippingSurchargeAmount...
									Element eshippingSurchargeAmount = YRCXmlUtils
											.getChildElement(
													matchingResLineItem,
													"shippingSurchargeAmount");
									if (eshippingSurchargeAmount != null) {
										String sSurCharge = ((Node) eshippingSurchargeAmount)
												.getTextContent();
										if (sSurCharge != null
												&& !sSurCharge.equals("")) {
											double tempSurCharge = Double
													.parseDouble(sSurCharge);
											if (dqty != 0.0) {
												dSurChargePerUnit = tempSurCharge
														/ dqty;
												dTotalCharges = dTotalCharges
														+ tempSurCharge;
												Element eLineCharge = YRCXmlUtils
														.createChild(
																eNewLineCharges,
																"LineCharge");
												YRCXmlUtils
														.setAttribute(
																eLineCharge,
																"ChargeCategory",
																HBCPCAConstants.HBC_SHIPPINGSURCHARGE);
												YRCXmlUtils
														.setAttribute(
																eLineCharge,
																"ChargeName",
																HBCPCAConstants.HBC_SHIPPINGSURCHARGE);
												YRCXmlUtils.setAttribute(
														eLineCharge,
														"ChargePerUnit",
														dSurChargePerUnit);
											}
										}
									}

									Element eligibleShipModes = YRCXmlUtils
											.getChildElement(
													matchingResLineItem,
													"eligibleShipModes");
									if(eligibleShipModes!=null){
									NodeList nShippingMode = eligibleShipModes
											.getElementsByTagName("ShippingMode");
									for (int k = 0; k < nShippingMode
											.getLength(); k++) {
										Element eShippingMode = (Element) nShippingMode
												.item(k);
										Element eServiceType = YRCXmlUtils
												.getChildElement(eShippingMode,
														"serviceType");
										if (eServiceType != null) {
											// ........get CarrierService...
											String strServiceType = ((Node) eServiceType)
													.getTextContent();
											if (YRCPlatformUI.equals(
													strServiceType,
													sCarrierServiceCode)) {
												Element eShippingCharge = YRCXmlUtils
														.getChildElement(
																eShippingMode,
																"ShippingCharge");
												if (eShippingCharge != null) {
													String sShippingCharge = ((Node) eShippingCharge)
															.getTextContent();
													if (sShippingCharge != null
															&& !sShippingCharge
																	.equals("")) {
														double tempShippingCharge = Double
																.parseDouble(sShippingCharge);
														if (dqty != 0.0) {
															dShippingChargePerUnit = tempShippingCharge
																	/ dqty;
															dTotalCharges = dTotalCharges
																	+ tempShippingCharge;
															/*
															 * fix for FIT 105
															 * and 171
															 */
															dHeaderChargeAmt = dHeaderChargeAmt
																	+ tempShippingCharge;
															/*
															 * Element
															 * eLineCharge =
															 * YRCXmlUtils
															 * .createChild(
															 * eNewLineCharges,
															 * "LineCharge");
															 * YRCXmlUtils
															 * .setAttribute(
															 * eLineCharge,
															 * "ChargeCategory",
															 * HBCPCAConstants.HBC_SHIPPINGCHARGE);
															 * YRCXmlUtils
															 * .setAttribute(
															 * eLineCharge,
															 * "ChargeName",
															 * HBCPCAConstants.HBC_SHIPPINGCHARGE);
															 * YRCXmlUtils
															 * .setAttribute(
															 * eLineCharge,
															 * "ChargePerUnit",
															 * dShippingChargePerUnit);
															 */
														}
													}
												}
											}
										}
									}
									}
								}

							}

							/*
							 * add header charges for shippingcharge fix for FIT
							 * 105 and 171
							 */
							Element eHeaderCharge = YRCXmlUtils.createChild(
									eHeaderCharges, "HeaderCharge");
							YRCXmlUtils.setAttribute(eHeaderCharge,
									"ChargeCategory",
									HBCPCAConstants.HBC_SHIPPINGCHARGE);
							YRCXmlUtils.setAttribute(eHeaderCharge,
									"ChargeName",
									HBCPCAConstants.HBC_SHIPPINGCHARGE);
							YRCXmlUtils.setAttribute(eHeaderCharge,
									"ChargeAmount", dHeaderChargeAmt);
							/*
							 * copy total header charge to ExtnOriginalAmount
							 */
							Element eleExtn = YRCXmlUtils.createChild(eHeaderCharge, "Extn");
							YRCXmlUtils.setAttribute(eleExtn, "ExtnOriginalAmount", dHeaderChargeAmt);

							sTotalCharges = YRCPlatformUI.getFormattedCurrency(
									YRCFormatUtils.getFormattedValue(
											"UnitPrice", Double
													.toString(dTotalCharges)),
									"USD");
							String sTitle = YRCPlatformUI.getFormattedString(
									"CHARGES_TITLE", "");
							String sTotalChargesMessage = YRCPlatformUI
									.getFormattedString("TOTAL_CHARGES",
											new String[] { strShippingMethod,
													sTotalCharges });
							YRCSharedTaskOutput out = HBCPCAUtil
									.showOptionDialogBoxWithApply(sTitle,
											sTotalChargesMessage);

							Element eSelectedOption = out.getOutput();
							String sSelectedOptionKey = "";
							sSelectedOptionKey = YRCXmlUtils.getAttribute(
									eSelectedOption, "SelectedOptionKey");
							if (sSelectedOptionKey != null
									&& sSelectedOptionKey.equals("Apply")) {
								setExtentionModel("ExtnOrder", eNewOrder);
								if (eDiscountOrderLines != null) {
									setExtentionModel("ExtnDiscountModel",
											eDiscountOrderLines);
								}
							} else {
								response = new YRCValidationResponse(
										YRCValidationResponse.YRC_VALIDATION_ERROR,
										"");
								return response;

							}

							stampCarrierServiceCodeForGWP(eOrderModel);

						}else if(YRCPlatformUI.equals(eleQuotationBasket.getTagName(), "YfsException")){
							String strException = YRCXmlUtils.getAttribute(eleQuotationBasket, "Exception");
							String strItemId = YRCXmlUtils.getAttribute(eleQuotationBasket, "ItemID");
							if(strException.equalsIgnoreCase("State"))
								HBCPCAUtil.showOptionDialogBoxWithOK("Error", YRCPlatformUI.getFormattedString("WRONG_STATE_CODE_MSG",strItemId));
							if(strException.equalsIgnoreCase("Postal"))
								HBCPCAUtil.showOptionDialogBoxWithOK("Error", YRCPlatformUI.getFormattedString("WRONG_POSTAL_CODE_MSG",strItemId));

						}else if (eleQuotationBasket == null
								|| !YRCPlatformUI.equals(eleQuotationBasket
										.getTagName(), "NoRequestLine")) {
							HBCPCAUtil.showOptionDialogBoxWithOK(YRCPlatformUI
									.getFormattedString("ERROR", ""),
									YRCPlatformUI.getFormattedString(
											"WEBSERVICE_DOWN", ""));
							response = new YRCValidationResponse(
									YRCValidationResponse.YRC_VALIDATION_OK,
									"");
							return response;
						}//fix for defect 1875
						else if(isCompleteVGCOrder(nOrderLine)){
							Document DOrder = YRCXmlUtils.createDocument("Order");
							Element eNewOrder = DOrder.getDocumentElement();
							Element eHeaderCharges = YRCXmlUtils.createChild(eNewOrder, "HeaderCharges");
							YRCXmlUtils.setAttribute(eHeaderCharges, "Reset","Y");
							Element eHeaderCharge = YRCXmlUtils.createChild(eHeaderCharges, "HeaderCharge");
							YRCXmlUtils.setAttribute(eHeaderCharge,"ChargeCategory",HBCPCAConstants.HBC_SHIPPINGCHARGE);
							YRCXmlUtils.setAttribute(eHeaderCharge,"ChargeName",HBCPCAConstants.HBC_SHIPPINGCHARGE);
							YRCXmlUtils.setAttribute(eHeaderCharge,"ChargeAmount", "0.00");

							Element eleExtn = YRCXmlUtils.createChild(eHeaderCharge, "Extn");
							YRCXmlUtils.setAttribute(eleExtn, "ExtnOriginalAmount", "0.00");
							setExtentionModel("ExtnOrder", eNewOrder);

						}
					} catch (Exception e) {

						// e.printStackTrace();
						YRCPlatformUI.trace(e);

					}

				} else {
					HBCPCAUtil.showOptionDialogBoxWithOK(YRCPlatformUI
							.getFormattedString("DIFF_METHOD_TITLE", ""),
							YRCPlatformUI.getFormattedString("DIFF_METHOD_MSG",
									""));
					response = new YRCValidationResponse(
							YRCValidationResponse.YRC_VALIDATION_ERROR, "");
					return response;
				}
			}

			else {
				HBCPCAUtil
						.showOptionDialogBoxWithOK(YRCPlatformUI
								.getFormattedString("METHOD_MISS_TITLE", ""),
								YRCPlatformUI.getFormattedString(
										"METHOD_MISS_MSG",  ""));
				response = new YRCValidationResponse(
						YRCValidationResponse.YRC_VALIDATION_ERROR, "");
				return response;

			}
		}else{
			response = new YRCValidationResponse(
					YRCValidationResponse.YRC_VALIDATION_ERROR, "");
			return response;
			}
		} else {
			response = new YRCValidationResponse(
					YRCValidationResponse.YRC_VALIDATION_ERROR, "");
			return response;
		}
		}
		//R3 FOR PICKUP FLOW
		}
		}
//		defect 976 start
		else {
			HBCPCAUtil
					.showOptionDialogBoxWithOK(YRCPlatformUI.getFormattedString("FAKE_EMAIL_FOR_VGC_TITLE", ""),
							YRCPlatformUI.getFormattedString("FAKE_EMAIL_ERROR_MESSAGE_FOR_VGC",  ""));
			response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR, "");
			return response;
		}

		}	//defect 976 end

		//Showing  Preference Language popup while clicking on Next button
		else if(fieldName != null && fieldName.equals("btnNext")){
			boolean checkFlag =false;
			ArrayList childExtensionList = this.getChildExtnBehaviors();
			if(childExtensionList !=null && childExtensionList.size() >0){
				/*
				 * Country Check
				 */
				for (int i = 0; i < childExtensionList.size(); i++) {
					if(this.getChildExtnBehaviors().get(i) instanceof HBCCustomerSearchPageExtnBehavior ){
						HBCCustomerSearchPageExtnBehavior customerSearchPageExtnBehavior= (HBCCustomerSearchPageExtnBehavior)this.getChildExtnBehaviors().get(i);
						ArrayList list=customerSearchPageExtnBehavior.getChildExtnBehaviors();
						if(list !=null && list.size() >0){
							for (int j = 0; j < list.size(); j++) {
								if(list.get(j) instanceof HBCAddressPanelExtnBehavior ){
									HBCAddressPanelExtnBehavior hbcAddressPanelExtnBehavior=(HBCAddressPanelExtnBehavior)list.get(j);
									//Get panel name
									String panelname = (String)hbcAddressPanelExtnBehavior.getOwnerForm().getData();
									//Getting the address model if it is only ShipToAddress Panel
									if("pnlShipToAddress".equals(panelname)){
										Element eleAddress=hbcAddressPanelExtnBehavior.getFromModel("address");
										String strtempCountry = eleAddress.getAttribute("Country");
										if(!YRCPlatformUI.isVoid(strtempCountry)){
										Element wizardInputForOrderEntry = this.getModel("WizardInputForOrderEntry");
										String strEnterpriseCode = wizardInputForOrderEntry.getAttribute("EnterpriseCode");

										String strCountryCode = "US";
										String strCountry = "USA";
										String strPlace = "Canadian";
										if(HBCPCAConstants.HBC_LT_BANNER.equals(strEnterpriseCode)){
											 strCountryCode = "US";
											 strCountry = "Canada";
											 strPlace = "US";

										}else if(HBCPCAConstants.HBC_BAY_BANNER.equals(strEnterpriseCode)){

											 	strCountryCode = "CA";
												strCountry = "USA";
												strPlace = "Canadian";
										}


										if(HBCPCAConstants.HBC_LT_BANNER.equals(strEnterpriseCode)){
											if(!HBCPCAConstants.HBC_LT_BANNER_COUNTRY.equals(strtempCountry))
											{
												YRCPlatformUI.showError(YRCPlatformUI.getString("CONSUMER_MISSING_INFORMATION"), YRCPlatformUI.getFormattedString("SHIP_TO_VALIDATION_MESSAGE",new String[] { strCountry,strEnterpriseCode,strPlace}));
												response = new YRCValidationResponse(
														YRCValidationResponse.YRC_VALIDATION_ERROR,
														"");
												return response;
											}
										}
										if(HBCPCAConstants.HBC_BAY_BANNER.equals(strEnterpriseCode)){
											if(!HBCPCAConstants.HBC_BAY_BANNER_COUNTRY.equals(strtempCountry))
											{
												YRCPlatformUI.showError(YRCPlatformUI.getString("CONSUMER_MISSING_INFORMATION"), YRCPlatformUI.getFormattedString("SHIP_TO_VALIDATION_MESSAGE",new String[] { strCountry,strEnterpriseCode,strPlace}));
												response = new YRCValidationResponse(
														YRCValidationResponse.YRC_VALIDATION_ERROR,
														"");
												return response;
											}
										}
										}
									}
								}
							}
						}
					}
				}
				for (int i = 0; i < childExtensionList.size(); i++) {
					if(childExtensionList.get(i) instanceof HBCCustomerSearchPageExtnBehavior ){
					HBCCustomerSearchPageExtnBehavior customerSearchPageExtnBehavior= (HBCCustomerSearchPageExtnBehavior)this.getChildExtnBehaviors().get(i);
					Element eleSelectedCustomer =customerSearchPageExtnBehavior.getFromModel("selectedCustomer");
					if(eleSelectedCustomer == null)
						checkFlag=true;
					else{
						checkFlag=false;
						Element eleCustomerContactList = XMLUtil.getChildElement(eleSelectedCustomer, "CustomerContactList");
						if(eleCustomerContactList!=null){
							ArrayList customerContactList = YRCXmlUtils.getChildren(eleCustomerContactList, "CustomerContact");
							for (int k = 0; k < customerContactList.size(); k++) {
								Element eleCustomerContact = (Element)customerContactList.get(k);
								if(eleCustomerContact != null){
									boolean isCustomerPrefLang = false;
									Element eleExtn = XMLUtil.getChildElement(eleCustomerContact, "Extn");
									if(eleExtn != null){
										String strExtnPreferredLanguage = eleExtn.getAttribute("ExtnPreferredLanguage");
										if(strExtnPreferredLanguage!=null && strExtnPreferredLanguage.length()>0){
											isCustomerPrefLang = true;
										}
									}
									if(!isCustomerPrefLang && !customerSearchPageExtnBehavior.existingCustomerWithoutPreflanf){
										HBCPreferedLanguagesPage  prefLanguages=new HBCPreferedLanguagesPage(new Shell(Display.getDefault().getActiveShell()),SWT.NONE);
										YRCDialog dialog = new YRCDialog(prefLanguages, 450, 110, YRCPlatformUI.getFormattedString("PREFERNCE_LANGUAGE_SELECT",""), "ApplicationTitleImage");
										dialog.open();
										//Select Prefered Language from the popup --and apply --and then stamp to ExtnPreferredLanguage of ExtnPreferredLanguage Model
										HBCPreferedLanguagesPageExtnBehavior hbcPrefLangPageExtnBhvr=(HBCPreferedLanguagesPageExtnBehavior)prefLanguages.getBehavior();
										if(hbcPrefLangPageExtnBhvr.preferedLangSelected.equals("")){
											response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_VALIDATED_ADDRESS",""));
											return response;
										}
										customerSearchPageExtnBehavior.strPrefLang = hbcPrefLangPageExtnBhvr.preferedLangSelected;
										customerSearchPageExtnBehavior.existingCustomerWithoutPreflanf =  true;
									}
								}
							}
						}
						break;
					}
				}
			}

			if(checkFlag){
				Element eleAddress=null;
				if(childExtensionList !=null && childExtensionList.size() >0){
					for (int i = 0; i < childExtensionList.size(); i++) {
						if(this.getChildExtnBehaviors().get(i) instanceof HBCCustomerSearchPageExtnBehavior ){
							HBCCustomerSearchPageExtnBehavior customerSearchPageExtnBehavior= (HBCCustomerSearchPageExtnBehavior)this.getChildExtnBehaviors().get(i);
							ArrayList list=customerSearchPageExtnBehavior.getChildExtnBehaviors();
							if(list !=null && list.size() >0){
								String firstName = "";
								for (int j = 0; j < list.size(); j++) {
									if(list.get(j) instanceof HBCAddressPanelExtnBehavior ){
										HBCAddressPanelExtnBehavior hbcAddressPanelExtnBehavior=(HBCAddressPanelExtnBehavior)list.get(j);
										//Get panel name
										String panelname = (String)hbcAddressPanelExtnBehavior.getOwnerForm().getData();
										//Getting the address model if it is only BillToAddress Panel
										if("pnlBillToAddress".equals(panelname)){
											eleAddress=hbcAddressPanelExtnBehavior.getFromModel("address");
											if(eleAddress!=null){
												firstName=eleAddress.getAttribute("FirstName");
											}
										}
									}
								}
								//If firstName of the BillToAddress is not empty then show the Prefered Language popup
								if( !firstName.equals("")){
									HBCPreferedLanguagesPage  prefLanguages=new HBCPreferedLanguagesPage(new Shell(Display.getDefault().getActiveShell()),SWT.NONE);
									YRCDialog dialog = new YRCDialog(prefLanguages, 450, 110, YRCPlatformUI.getFormattedString("PREFERNCE_LANGUAGE_SELECT",""), "ApplicationTitleImage");
									dialog.open();
									//Select Prefered Language from the popup --and apply --and then stamp to ExtnPreferredLanguage of ExtnPreferredLanguage Model
									HBCPreferedLanguagesPageExtnBehavior hbcPrefLangPageExtnBhvr=(HBCPreferedLanguagesPageExtnBehavior)prefLanguages.getBehavior();
									if(hbcPrefLangPageExtnBhvr.preferedLangSelected.equals("")){
										response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,YRCPlatformUI.getFormattedString("ADDRESS_CAPTURE_VALIDATED_ADDRESS",""));
										return response;
									}
									customerSearchPageExtnBehavior.strPrefLang = hbcPrefLangPageExtnBhvr.preferedLangSelected;
								}
							}
						}
					}
				}
			}


		}

			String formID = fetchCurrentPageID();
			if("com.yantra.pca.ycd.rcp.tasks.orderEntry.wizardpages.YCDOrderEntryCustomerPage".equals(formID)){
				boolean invalidPhone = false;
				boolean invalidEmail = false;
				boolean done = false;
				for (int i = 0; i < childExtensionList.size() && !done; i++) {
					if(childExtensionList.get(i)!=null && childExtensionList.get(i) instanceof HBCCustomerSearchPageExtnBehavior){
						HBCCustomerSearchPageExtnBehavior behavior = (HBCCustomerSearchPageExtnBehavior)childExtensionList.get(i) ;
						done = true;
						Element selectedCustomer = behavior.getFromModel("selectedCustomer");
						if(selectedCustomer!=null){
						ArrayList behaviorList =  behavior.getChildExtnBehaviors();
						for (int j = 0; j < behaviorList.size(); j++) {
							if(behaviorList.get(j) instanceof HBCAddressPanelExtnBehavior ){
								HBCAddressPanelExtnBehavior hbcAddressPanelExtnBehavior=(HBCAddressPanelExtnBehavior)behaviorList.get(j);
								//Get panel name
								String panelname = (String)hbcAddressPanelExtnBehavior.getOwnerForm().getData();
								//Getting the address model if it is only BillToAddress Panel
								if("pnlBillToAddress".equals(panelname) || "pnlShipToAddress".equals(panelname) ){
									Element eleAddress=hbcAddressPanelExtnBehavior.getFromModel("address");
									if(eleAddress!=null){
										String phone = eleAddress.getAttribute("DayPhone");
										String email = eleAddress.getAttribute("EMailID");
										if(YRCPlatformUI.isVoid(phone))
											invalidPhone= true;
										if(YRCPlatformUI.isVoid(email))
											invalidEmail= true;
									}
									String error = "";
									if (invalidEmail || invalidPhone) {
											if (invalidPhone && invalidEmail) {
												error = "CONSUMER_PHONENUMBER_EMAIL_MISSING";
											} else if (invalidEmail) {
												error = "CONSUMER_EMAIL_MISSING";
											} else if (invalidPhone) {
												error = "CONSUMER_PHONENUMBER_MISSING";
											}
											YRCPlatformUI.showError(YRCPlatformUI.getString("CONSUMER_MISSING_INFORMATION"), YRCPlatformUI.getString(error));

											response = new YRCValidationResponse(
													YRCValidationResponse.YRC_VALIDATION_ERROR,
													"");
											return response;
										}
								}
							}
						}
					}
					}
				}
			}else if("com.yantra.pca.ycd.rcp.tasks.orderEntry.wizardpages.YCDOrderEntryCustomerSkipPage".equals(formID)){

				boolean invalidPhone = false;
				boolean invalidEmail = false;
				boolean done = false;
				for (int i = childExtensionList.size()-1; i >=0 && !done; i++) {
					if(childExtensionList.get(i)!=null && childExtensionList.get(i) instanceof HBCCustomerSearchPageExtnBehavior){
						HBCCustomerSearchPageExtnBehavior behavior = (HBCCustomerSearchPageExtnBehavior)childExtensionList.get(i) ;
						done = true;
						Element selectedCustomer = behavior.getFromModel("selectedCustomer");
						if(selectedCustomer!=null){
						ArrayList behaviorList =  behavior.getChildExtnBehaviors();
						for (int j = 0; j < behaviorList.size(); j++) {
							if(behaviorList.get(j) instanceof HBCAddressPanelExtnBehavior ){
								HBCAddressPanelExtnBehavior hbcAddressPanelExtnBehavior=(HBCAddressPanelExtnBehavior)behaviorList.get(j);
								//Get panel name
								String panelname = (String)hbcAddressPanelExtnBehavior.getOwnerForm().getData();
								//Getting the address model if it is only BillToAddress Panel
								if("pnlBillToAddress".equals(panelname) || "pnlShipToAddress".equals(panelname) ){
									Element eleAddress=hbcAddressPanelExtnBehavior.getFromModel("address");
									if(eleAddress!=null){
										String phone = eleAddress.getAttribute("DayPhone");
										String email = eleAddress.getAttribute("EMailID");
										if(YRCPlatformUI.isVoid(phone))
											invalidPhone= true;
										if(YRCPlatformUI.isVoid(email))
											invalidEmail= true;
									}
									String error = "";
									if (invalidEmail || invalidPhone) {
											if (invalidPhone && invalidEmail) {
												error = "CONSUMER_PHONENUMBER_EMAIL_MISSING";
											} else if (invalidEmail) {
												error = "CONSUMER_EMAIL_MISSING";
											} else if (invalidPhone) {
												error = "CONSUMER_PHONENUMBER_MISSING";
											}
											YRCPlatformUI.showError(YRCPlatformUI.getString("CONSUMER_MISSING_INFORMATION"), YRCPlatformUI.getString(error));

											response = new YRCValidationResponse(
													YRCValidationResponse.YRC_VALIDATION_ERROR,
													"");
											return response;
										}
								}
							}
						}
					}
					}
				}

			}

	}else if(fieldName != null && fieldName.equals("btnConfirm")){
		Element eleOrderDetails = this.getModel("OrderDetails");
		strRoyaltyNumber = null;
		if(eleOrderDetails!=null){
			String strEnterpriseCode = eleOrderDetails.getAttribute("EnterpriseCode");
			if(HBCPCAConstants.HBC_ENTERPRISE_BAY.equals(strEnterpriseCode)){
				ArrayList tempChildExtn = this.getChildExtnBehaviors();
				boolean mandatoryCheckPassed = true;
				for (int j = 0; j < tempChildExtn.size(); j++) {
					if(tempChildExtn.get(j) instanceof HBCPaymentConfirmationPanelExtnBehavior){
						HBCPaymentConfirmationPanelExtnBehavior confirmationPanelExtnBehavior  =(HBCPaymentConfirmationPanelExtnBehavior) this.getChildExtnBehaviors().get(j);
						Element eleOrder = confirmationPanelExtnBehavior.getFromModel("Order");
						if(eleOrder!=null){
							Element elePriceInfo = YRCXmlUtils.getChildElement(eleOrder, "PriceInfo");
							if(elePriceInfo!=null){
								String strAmountLeft = elePriceInfo.getAttribute("AmountLeft");
								double intAmountLeft = Double.parseDouble(strAmountLeft);
								if(intAmountLeft > 0){
									mandatoryCheckPassed = false;
									break;
								}
							}else{
								mandatoryCheckPassed = false;
								break;
							}
						}else{
							mandatoryCheckPassed = false;
							break;
						}

						Element elePaymentMethodList = confirmationPanelExtnBehavior.getFromModel("PaymentMethodList");

						if(elePaymentMethodList!=null){
							NodeList PaymentMethodList = (NodeList) YRCXPathUtils
							.evaluate(elePaymentMethodList,"PaymentMethod[@UsePaymentMethod='Y']",XPathConstants.NODESET);
							for (int i = 0; i < PaymentMethodList.getLength(); i++) {
								Element PaymentMethod = (Element)PaymentMethodList.item(i);
								String paymentGroup = PaymentMethod.getAttribute("PaymentTypeGroup");
								if(HBCPCAConstants.HBC_CREDIT_CARD_PAYMENT_GROUP.equals(paymentGroup)){
									String strSecureAuthenticationCode = PaymentMethod.getAttribute("SecureAuthenticationCode");
									if(YRCPlatformUI.isVoid(strSecureAuthenticationCode)){
										mandatoryCheckPassed = false;
										break;
									}
								}

							}

						}
					}
				}
				if(mandatoryCheckPassed){
					Element EleReturnOrderDetails = this.getModel("ReturnOrderDetails");
					double preTaxTotal = 0;
					if(EleReturnOrderDetails!=null){
						Element eleOverallTotals = YRCXmlUtils.getChildElement(EleReturnOrderDetails, "OverallTotals");
						if(eleOverallTotals!=null){
							String strLineSubTotal = eleOverallTotals.getAttribute("LineSubTotal");
							String strGrandDiscount = eleOverallTotals.getAttribute("GrandDiscount");
							double doubleLineSubTotal = Double.parseDouble(strLineSubTotal);
							double doubleGrandDiscount = Double.parseDouble(strGrandDiscount);
							preTaxTotal = doubleLineSubTotal - doubleGrandDiscount;
						}
					}
					YRCSharedTaskOutput sharedTaskOutput = showOptionDialogBoxWithYesNo(YRCPlatformUI.getFormattedString("REWARDS_MESSAGE", ""),YRCPlatformUI.getString("REWARDS_REQUIRED_MESSAGE"));
					Element ele =sharedTaskOutput.getOutput();
					String strOutput = ele.getAttribute("SelectedOptionKey");
					if(strOutput.equals("Yes")){
							royaltyCardPage  = new HBCRoyaltyCardPage(new Shell(Display.getDefault().getActiveShell()),SWT.NONE,preTaxTotal);
						YRCDialog dialog = new YRCDialog(royaltyCardPage, 550, 130,YRCPlatformUI.getString("REWARDS_ENTER_MESSAGE"),"ApplicationTitleImage");
						dialog.open();
						HBCRoyaltyCardPageExtnBehavior royaltyCardPageExtnBehavior=(HBCRoyaltyCardPageExtnBehavior)royaltyCardPage.getBehavior();
						if(royaltyCardPageExtnBehavior!=null){
							ArrayList arrayList = royaltyCardPageExtnBehavior.getChildBehaviors();

							strRoyaltyNumber = royaltyCardPageExtnBehavior.getStrRoyaltyNumber();
						}
					}
				}
			}
		}
		//Start 3566 - To validate the AMS response before confirming the Order, if any error then throw error message and do not allow to confirm the order
		ArrayList childExtnBehaviors = this.getChildExtnBehaviors();
		Iterator HBCPaymentConfirmation = childExtnBehaviors.iterator();
		while (HBCPaymentConfirmation.hasNext()) {
			Object ObjhbcPaymentConfirmation = HBCPaymentConfirmation
			.next();
			if (ObjhbcPaymentConfirmation instanceof HBCPaymentConfirmationPanelExtnBehavior) {
				HBCPaymentConfirmationPanelExtnBehavior hbcpaymentConfirmBehaviour = (HBCPaymentConfirmationPanelExtnBehavior) ObjhbcPaymentConfirmation;
				boolean isError=false;
				Element elseExtnAMSWebServicePromotion = hbcpaymentConfirmBehaviour.getFromModel("ExtnAMSPromotionSyncWebService");
				Element eleError = YRCXmlUtils.getChildElement(elseExtnAMSWebServicePromotion, "Error");
				if(eleError!=null && !eleError.equals("")){
					String errorCode=eleError.getAttribute("ErrorCode");
						if(errorCode!=null && !errorCode.equalsIgnoreCase(""))
								isError=true;
				}
				if(elseExtnAMSWebServicePromotion!=null && isError){
				YRCPlatformUI.showError(YRCPlatformUI.getString("AMS_ERROR"), YRCPlatformUI.getString("AMS_ERROR_DOWN"));
				response = new YRCValidationResponse(
						YRCValidationResponse.YRC_VALIDATION_ERROR,
						"");
				return response;
				}
			}
		}
		//End 3566
	}
		return super.validateButtonClick(fieldName);
	}
	public  YRCSharedTaskOutput showOptionDialogBoxWithYesNo(String strTitleKey, String strDescriptionKey){
		String xmlStr =
			"<OptionDialog DefaultOption='Yes' "+
			"DialogImage='1'>" +
			"<OptionTextList>"+
			"           <OptionText OptionTextKey= 'Yes' />"+
			"           <OptionText OptionTextKey= 'No' />"+
			"</OptionTextList>"+
			"</OptionDialog>";

		Element optionDialogInput = YRCXmlUtils.createFromString(xmlStr).getDocumentElement();
		optionDialogInput.setAttribute("TitleKey", strTitleKey);
		optionDialogInput.setAttribute("DescriptionKey", strDescriptionKey);

		YRCSharedTaskOutput optionDialogOutput = YRCPlatformUI.launchSharedTask("YCDOptionDialogSharedTask", optionDialogInput);
		return optionDialogOutput;
	}

	/**This method is used to stamp the DepartmentCode at OrderLine level.
	 * @param apiContext
	 *
	 * this code is fix for 1790
	 */
	private void stampDeptCodeAtOrderLine(YRCApiContext apiContext) {
		Document docOrder = apiContext.getInputXml();
		Element eleOrder = docOrder.getDocumentElement();

		ArrayList childExtnBehaviors = this.getChildExtnBehaviors();
		for (int i = 0; i < childExtnBehaviors.size(); i++) {
			if (childExtnBehaviors != null
					&& childExtnBehaviors.get(i) instanceof HBCOrderEntryLinePanelExtnBehavior) {
				String sDeptCode = "";
				String citemId = "";
				String sProdLine="";
				HBCOrderEntryLinePanelExtnBehavior entryLinePanelExtnBehavior = (HBCOrderEntryLinePanelExtnBehavior) childExtnBehaviors
						.get(i);

				Element elemExtnItemList = entryLinePanelExtnBehavior
						.getFromModel("ItemDetails");
				if (elemExtnItemList != null) {
					citemId = elemExtnItemList.getAttribute("ItemID");
					Element elePrimaryInfo = YRCXmlUtils.getChildElement(
							elemExtnItemList, "PrimaryInformation");
					if (elePrimaryInfo != null) {
						sDeptCode = elePrimaryInfo
								.getAttribute("ManufacturerItem");
						sProdLine=elePrimaryInfo.getAttribute("ProductLine");
					}


				Element eleOrdLines = YRCXmlUtils.getChildElement(eleOrder,
							"OrderLines");
					if (eleOrdLines != null) {
						ArrayList orderLineList = YRCXmlUtils.getChildren(
								eleOrdLines, "OrderLine");
						if (orderLineList != null && orderLineList.size() > 0) {
							for (int j = 0; j < orderLineList.size(); j++) {
								Element eleOrderLine = (Element) orderLineList
										.get(j);
								Element elepItem = YRCXmlUtils.getChildElement(
										eleOrderLine, "Item");
								if (elepItem != null) {
									String pItemId = elepItem
											.getAttribute("ItemID");
									if (pItemId.equals(citemId)) {
										eleOrderLine.setAttribute(
												"DepartmentCode", sDeptCode);
									//This is the fix for GiftLine Image displaying once you click on Next button after entering Item Details
										//1530 code reverting
										/*if (sProdLine
												.equals(HBCPCAConstants.GIFT_BOX_ITEM)) {
											eleOrderLine
													.setAttribute(
															"GiftFlag",
															"Y");
										}*/
									}
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	 * @param eOrderModel
	 */
	private void stampCarrierServiceCodeForGWP(Element eOrderModel) {
		//Stamping Parent OrderLine CarrierServiceCode for ChildLines based on OrderLineKey
		String carrierServiceCode="";
		if(eOrderModel!=null){
			Element eOrderLines = YRCXmlUtils.getChildElement(eOrderModel, "OrderLines");
			if(eOrderLines!=null){
				NodeList nOrderLine = eOrderLines.getElementsByTagName("OrderLine");
					for (int i = 0; i < nOrderLine.getLength(); i++) {
						Element eOrderLine = (Element) nOrderLine.item(i);
						if(eOrderLine!=null){
							Element eChildOrderLineRelationships=YRCXmlUtils.getChildElement(eOrderLine, "ChildOrderLineRelationships");
							if(eChildOrderLineRelationships!=null){
								NodeList nOrderLineRelationship=eChildOrderLineRelationships.getElementsByTagName("OrderLineRelationship");
								if(nOrderLineRelationship!=null & nOrderLineRelationship.getLength()>0){
									for(int j=0; j<nOrderLineRelationship.getLength();j++){
										Element eOrderLineRelationship = (Element) nOrderLineRelationship.item(j);
										if(eOrderLineRelationship!=null){
											Element eChildLine=YRCXmlUtils.getChildElement(eOrderLineRelationship, "ChildLine");
											if(eChildLine!=null){
												String orderLineKey=eChildLine.getAttribute("OrderLineKey");

												Element eleStampCarrierServiceCode = (Element) YRCXPathUtils
													.evaluate(eOrderModel,"OrderLines/OrderLine[@OrderLineKey='"
														+ orderLineKey + "']",XPathConstants.NODE);
												if(eleStampCarrierServiceCode!=null){
													carrierServiceCode=eOrderLine.getAttribute("CarrierServiceCode");
													eleStampCarrierServiceCode.setAttribute("CarrierServiceCode", carrierServiceCode);
												}
											}
										}
									}
								}
							}
						}
					}

					//Stamping CarrierServiceCode on Order if ShipTogetherNo="GWP_Order"
					NodeList nShipTogetherNo = (NodeList) YRCXPathUtils
					.evaluate(eOrderModel,"OrderLines/OrderLine[@ShipTogetherNo='"
							+ "GWP_Order" + "']",XPathConstants.NODESET);
					if(nShipTogetherNo!=null & nShipTogetherNo.getLength()>0){
						for(int k=0; k<nShipTogetherNo.getLength();k++){
							Element eleShipTogetherNo = (Element) nShipTogetherNo.item(k);
							if(eleShipTogetherNo!=null){
								eleShipTogetherNo.setAttribute("CarrierServiceCode", carrierServiceCode);
							}
						}
					}
			}
		}
	}

	/**
	 *
	 * @param childExtnBehaviors
	 */
		private void processAMSRequestCall(ArrayList childExtnBehaviors) {
			Document docOrder = YRCXmlUtils.createDocument("Order");
			Element eleOrder = docOrder.getDocumentElement();
			Element eleOrderLines = YRCXmlUtils.createChild(eleOrder, "OrderLines");
			Element inputElement =this.getModel("OrderDetails");
			if(inputElement!= null){
			eleOrder.setAttribute("SellerOrganizationCode", inputElement.getAttribute("SellerOrganizationCode"));
			eleOrder.setAttribute("EnterpriseCode", inputElement.getAttribute("EnterpriseCode"));
			String strOrderNo =  inputElement.getAttribute("OrderNo");
			if(strOrderNo.length() > 9)
				eleOrder.setAttribute("OrderNo",strOrderNo.substring(strOrderNo.length()-9, strOrderNo.length()) );
			else
				eleOrder.setAttribute("OrderNo",strOrderNo);
			eleOrder.setAttribute("BillToID", inputElement.getAttribute("BillToID"));
			eleOrder.setAttribute("PendingChangesUserID",inputElement.getAttribute("PendingChangesUserID"));
			String sMaxOrderStatus = inputElement.getAttribute("MaxOrderStatus");
			if(sMaxOrderStatus!=null && !sMaxOrderStatus.equals("")){
				eleOrder.setAttribute("Status", sMaxOrderStatus);
			}else{
				eleOrder.setAttribute("Status", "1000");
			}
			}
			boolean paymentDone =false;
			for (int i = 0; i < childExtnBehaviors.size(); i++) {
				if(childExtnBehaviors.get(i) instanceof HBCOrderEntryLinePanelExtnBehavior){
				HBCOrderEntryLinePanelExtnBehavior entryLinePanelExtnBehavior = (HBCOrderEntryLinePanelExtnBehavior) childExtnBehaviors.get(i);
				Element eleOrderLineInput = entryLinePanelExtnBehavior.getOrderLineModel("OrderLine");
				Element eleItem = YRCXmlUtils.getChildElement(eleOrderLineInput, "Item");
				Element eletempOrderLineTranQuantity = YRCXmlUtils.getChildElement(eleOrderLineInput, "OrderLineTranQuantity");
				Element eleModelOrderLine = entryLinePanelExtnBehavior.getFromModel("OrderLines");
				if(eleModelOrderLine != null){
					String strShipTogetherNo =  eleModelOrderLine.getAttribute("ShipTogetherNo");
					if(strShipTogetherNo != null && (strShipTogetherNo.equals("GWP_Order") || strShipTogetherNo.equals("GWP_OrderLine")) )
					continue;
				}
				if(eleOrderLineInput != null && eleItem.getAttribute("ItemID")!=null && eleItem.getAttribute("ItemID").length() >0){
					Element eleOrderLine = YRCXmlUtils.createChild(eleOrderLines, "OrderLine");
					Element eleTransactionalLineId = entryLinePanelExtnBehavior.getFromModel("TransactionalLineId");
					eleOrderLine.setAttribute("OrderedQty", eletempOrderLineTranQuantity.getAttribute("OrderedQty"));
					if(eleModelOrderLine !=null)
						eleOrderLine.setAttribute("PrimeLineNo", eleModelOrderLine.getAttribute("PrimeLineNo"));
					else
						eleOrderLine.setAttribute("PrimeLineNo", eleTransactionalLineId.getAttribute("TransactionalLineId"));
					YRCXmlUtils.mergeElement(eleOrderLineInput, eleOrderLine, true);
					Element eleOrderLineTranQuantity = YRCXmlUtils.getChildElement(eleOrderLine, "OrderLineTranQuantity");
					if(eleOrderLineTranQuantity !=null)
						eleOrderLine.removeChild(eleOrderLineTranQuantity);
					eleItem = YRCXmlUtils.getChildElement(eleOrderLine, "Item");
					eleItem.removeAttribute("ItemShortDesc");
					Element eleLinePriceInfo = YRCXmlUtils.getChildElement(eleOrderLine, "LinePriceInfo");
					if(eleModelOrderLine!=null){
						eleOrderLine.setAttribute("GiftFlag", eleModelOrderLine.getAttribute("eleModelOrderLine"));
						Element eleTempLinePriceInfo = YRCXmlUtils.getChildElement(eleModelOrderLine, "LinePriceInfo");
						if(eleLinePriceInfo!=null)
						eleLinePriceInfo.setAttribute("IsPriceLocked", eleLinePriceInfo.getAttribute("IsPriceLocked"));
					}else{
					eleOrderLine.setAttribute("GiftFlag", "N");
					eleLinePriceInfo.setAttribute("IsPriceLocked","Y");
					}
					//
					Element eleExtnInput = YRCXmlUtils.createChild(eleOrderLine,"Extn");
					eleExtnInput.setAttribute("ExtnMinOrderFlag","1");

					//AMS Upgrade Change:: setting of ExtnClearanceLevel flag
					if(eleModelOrderLine !=null)
					{
						Element eleExtnModelOrderLine = YRCXmlUtils.getChildElement(eleModelOrderLine, "Extn") ;
						eleExtnInput.setAttribute("ExtnClearanceLevel", 
								eleExtnModelOrderLine.getAttribute("ExtnTransactionID")) ;
					}
					else
					{
						Element eleItemDetails = entryLinePanelExtnBehavior.getFromModel("ItemDetails") ;
						eleExtnInput.setAttribute("ExtnClearanceLevel",
							getItemFlags(eleItem.getAttribute("ItemID"), eleItemDetails )) ;
					}
					
					Element elePaymentMethods = YRCXmlUtils.getChildElement(
							inputElement, "PaymentMethods");
					if (elePaymentMethods != null && !paymentDone) {
						Document paymentDoc = YRCXmlUtils.createDocument("PaymentMethods");
						NodeList tempPaymentList =elePaymentMethods.getElementsByTagName("PaymentMethod");
						if(tempPaymentList!=null && tempPaymentList.getLength()>0){
							for (int j = 0; j < tempPaymentList.getLength(); j++) {
								Element tempPaymentmethod = (Element) tempPaymentList.item(j);
								Element  paymentMethod = YRCXmlUtils.createChild(paymentDoc.getDocumentElement(), "PaymentMethod");
								paymentMethod.setAttribute("CreditCardType", tempPaymentmethod.getAttribute("CreditCardType"));
								paymentMethod.setAttribute("RequestedChargeAmount", tempPaymentmethod.getAttribute("MaxChargeLimit"));
								paymentMethod.setAttribute("CreditCardNo", "");
							}
						}
						XMLUtil.addDocument(docOrder, paymentDoc, false);
						paymentDone = true;
					}
				}
				}
			}
			String systemStatusAMS = "Y";
			/*Check whether systemAvailableStatus map is empty or not */
			if(XMLUtil.isVoid(systemAvailableStatus) || systemAvailableStatus.size() == 0){
				getSystemAvalilableStatus();
			}
			if(!XMLUtil.isVoid(systemAvailableStatus) && systemAvailableStatus.size() > 0){
				systemStatusAMS = (String)systemAvailableStatus.get(HBCPCAConstants.SYSTEM_NAME_AMS);
			}
			if(systemStatusAMS.toUpperCase().equals("Y")){
				ArrayList tempOrderLines = YRCXmlUtils.getChildren(eleOrderLines, "OrderLine");
				if(tempOrderLines!=null && tempOrderLines.size() >0){
					YRCApiContext yrcApiContext = new YRCApiContext();
					//Start defect# 286 -Doubling GWP
					Element eleOrderout=docOrder.getDocumentElement();
					Element eleOrderLine=YRCXmlUtils.getChildElement(eleOrderout, "OrderLines");
					String [] attrNames=new String []{"PrimeLineNo"};
					YRCXmlUtils.sortNumericChildren(eleOrderLine, attrNames, true);		
					//End defect# 286 -Doubling GWP
					yrcApiContext.setApiName("HBCAMSPromotionSyncWebService");
					yrcApiContext.setInputXml(docOrder);
					yrcApiContext.setFormId(this.getFormId());
					new YRCApiCaller(yrcApiContext, true).invokeApi();
					handleApiCompletion(yrcApiContext);
				}

			}
		}

	//AMS Upgrade Change:: setting of ExtnClearanceLevel flag
	private String getItemFlags(String strItemID) 
	{
		String strClearanceLevel = "" ;
		ArrayList childExtnBehaviors = this.getChildExtnBehaviors() ;
		
		for (int i = 0; i < childExtnBehaviors.size(); i++) 
		{
			if(childExtnBehaviors.get(i) instanceof HBCOrderEntryLinePanelExtnBehavior)
			{
				HBCOrderEntryLinePanelExtnBehavior entryLinePanelExtnBehavior = 
					(HBCOrderEntryLinePanelExtnBehavior) childExtnBehaviors.get(i) ;
				
				Element eleItemDetails = entryLinePanelExtnBehavior.getFromModel("ItemDetails") ;
				if (eleItemDetails != null )
				{
					if (strItemID.equalsIgnoreCase(eleItemDetails.getAttribute("ItemID")))
					{
						Element eleComputedPrice = YRCXmlUtils.getChildElement(eleItemDetails, "ComputedPrice") ;
						strClearanceLevel = eleComputedPrice.getAttribute("ExtnClearanceLevel") ;
					}
				}
			}
		}
		return strClearanceLevel ;
	}
	
	//AMS Upgrade Change:: setting of ExtnClearanceLevel flag
	private String getItemFlags(String strItemID, Element eleItemDetails) 
	{
		String strClearanceLevel = "" ;
		
		if (eleItemDetails != null )
		{
			if (strItemID.equalsIgnoreCase(eleItemDetails.getAttribute("ItemID")))
			{
				Element eleComputedPrice = YRCXmlUtils.getChildElement(eleItemDetails, "ComputedPrice") ;
				strClearanceLevel = eleComputedPrice.getAttribute("ExtnClearanceLevel") ;
			}
		}
		return strClearanceLevel ;
	}
	
	
	
	/**
	 *
	 * @param strTitleKey
	 * @param strDescriptionKey
	 * @return
	 */

	public  YRCSharedTaskOutput showOptionDialogBoxWithSkip(String strTitleKey, String strDescriptionKey){
			String xmlStr =
				"<OptionDialog DefaultOption='Ok' "+
				"DialogImage='1'>" +
				"<OptionTextList>"+
				"           <OptionText OptionTextKey= 'Ok' />"+
				"           <OptionText OptionTextKey= 'Skip' />"+
				"</OptionTextList>"+
				"</OptionDialog>";

			Element optionDialogInput = YRCXmlUtils.createFromString(xmlStr).getDocumentElement();
			optionDialogInput.setAttribute("TitleKey", strTitleKey);
			optionDialogInput.setAttribute("DescriptionKey", strDescriptionKey);

			YRCSharedTaskOutput optionDialogOutput = YRCPlatformUI.launchSharedTask("YCDOptionDialogSharedTask", optionDialogInput);
			return optionDialogOutput;
		}

	/**
	 * @param orderLines
	 * @return true if same shipping method is selected
	 */
	private boolean isSameShippingMethodSelected(NodeList orderLines) {

		String sCarrierServiceCode = "";
		String temp = "";
		String sProductLine = "";
		String sShipTogetherNo = "";
		if (orderLines != null) {
			for (int k = 0; k < orderLines.getLength(); k++) {
				Element eOrderLine = (Element) orderLines.item(k);
				if (eOrderLine != null) {
					sShipTogetherNo = YRCXmlUtils.getAttribute(eOrderLine,
							"ShipTogetherNo");
					Element eItem = YRCXmlUtils.getChildElement(eOrderLine,
							"Item");
					if (eItem != null) {
						sProductLine = YRCXmlUtils.getAttribute(eItem,
								"ProductLine");
						if (YRCPlatformUI.equals(sProductLine, "")
								|| !YRCPlatformUI.equals(sProductLine,
										HBCPCAConstants.VGC_ITEM)) {
							if (YRCPlatformUI.equals(sShipTogetherNo, "")
									|| !(YRCPlatformUI.equals(sShipTogetherNo,
											HBCPCAConstants.HBC_GWP_Order) || YRCPlatformUI
											.equals(
													sShipTogetherNo,
													HBCPCAConstants.HBC_GWP_OrderLine))) {
								sCarrierServiceCode = YRCXmlUtils.getAttribute(
										eOrderLine, "CarrierServiceCode");

								if (sCarrierServiceCode != null
										|| !sCarrierServiceCode.equals("")) {
									if (temp.equals("") || temp == null) {
										temp = sCarrierServiceCode;
									} else if (!temp
											.equals(sCarrierServiceCode)) {
										return false;
									}

								}
							}
						}
					}
				}

			}
		}
		return true;
	}

	/**
	 * @param orderLine
	 * @return true if all shipping methods(CarrierServiceCodes) are selected.
	 */
	private boolean isShippingMethodSelected(NodeList orderLines) {

		String sCarrierServiceCode = "";
		String sProductLine = "";
		String sShipTogetherNo = "";
		String orderLineActionSelected = "";
		if (orderLines != null) {
			for (int k = 0; k < orderLines.getLength(); k++) {
				Element eOrderLine = (Element) orderLines.item(k);
				if (eOrderLine != null) {
					sShipTogetherNo = YRCXmlUtils.getAttribute(eOrderLine,
							"ShipTogetherNo");
					Element eItem = YRCXmlUtils.getChildElement(eOrderLine,
							"Item");
					if (eItem != null) {
						sProductLine = YRCXmlUtils.getAttribute(eItem,
								"ProductLine");
						if (YRCPlatformUI.equals(sProductLine, "")
								|| !YRCPlatformUI.equals(sProductLine,
										HBCPCAConstants.VGC_ITEM)) {
							if (YRCPlatformUI.equals(sShipTogetherNo, "")
									|| !(YRCPlatformUI.equals(sShipTogetherNo,
											HBCPCAConstants.HBC_GWP_Order) || YRCPlatformUI
											.equals(
													sShipTogetherNo,
													HBCPCAConstants.HBC_GWP_OrderLine))) {
								sCarrierServiceCode = YRCXmlUtils.getAttribute(
										eOrderLine, "CarrierServiceCode");
								/**
								 * Modified the below piece of code for SP issue 600
								 * If order line Action is selested as CANCEL
								 * then it should not through the alert for
								 * selecting shipping method for that orderline
								 */
								orderLineActionSelected = YRCXmlUtils.getAttribute(
										eOrderLine, "Action");
								if (sCarrierServiceCode == null
										|| sCarrierServiceCode.equals("") && orderLineActionSelected != null && !orderLineActionSelected.equals("CANCEL")) {
									return false;
								}
							}
						}
					}
				}
			}
		}

		return true;
	}
	/**
	 * @param orderLine
	 * @return true if order contains only VGC items.
	 */
	private boolean isCompleteVGCOrder(NodeList orderLines) {
		String sProductLine = "";
		if (orderLines != null) {
			for (int k = 0; k < orderLines.getLength(); k++) {
				Element eOrderLine = (Element) orderLines.item(k);
				if (eOrderLine != null) {
					Element eItem = YRCXmlUtils.getChildElement(eOrderLine,"Item");
					if (eItem != null) {
						sProductLine = YRCXmlUtils.getAttribute(eItem,"ProductLine");
						if (YRCPlatformUI.equals(sProductLine, "")
								|| !YRCPlatformUI.equals(sProductLine,
										HBCPCAConstants.VGC_ITEM)) {
							return false;
						}
					}
				}
			}
		}

		return true;
	}

	/**
	 * Get the total quantity of GiftWraps if GiftWrap=Y and get the total quantity of GiftBoxes if ProductLine=5 then
	 * i)Check the total quantity of GiftWraps ==   total quantity of GiftBoxes then true
	 * ii)No.of GiftWraps Quantity >= 1 Quantity of GiftBox then true
	 * Otherwise Prompting the message User (Show the popup)
	 * @param eOrder
	 ** @return true
	 */
	private boolean validateGiftWrapAndGiftBox(Element eOrder) {
		double totalGWQty = 0.00;
		double totalGBQty = 0.00;
		if (eOrder != null) {
			// get Order lines....
			Element eOrderLines = YRCXmlUtils.getChildElement(eOrder,"OrderLines");
			if (eOrderLines != null) {
				// To get the GiftWrap Lines
				NodeList nGWOrderLines = (NodeList) YRCXPathUtils.evaluate(
						eOrderLines, "OrderLine[@GiftWrap='" + "Y" + "']",
						XPathConstants.NODESET);

				if (nGWOrderLines != null & nGWOrderLines.getLength() > 0) {
					// Here, getting the total GiftWrap lines OrderedQty
					for (int i = 0; i < nGWOrderLines.getLength(); i++) {
						Element eGWOrderLine = (Element) nGWOrderLines.item(i);
						if (eGWOrderLine != null) {
							// Check if we have at least one line withGiftWrap=Y
							totalGWQty+=Double.parseDouble(YRCXmlUtils.getAttribute(eGWOrderLine, "OrderedQty"));
						}
					}
				}
			}

			//To get GiftBox Lines
			String sProductLine = "";
			//if (totalGWQty > 0) {
				NodeList nOrderLine = eOrderLines
						.getElementsByTagName("OrderLine");
				if (nOrderLine != null & nOrderLine.getLength() > 0) {
					for (int oline = 0; oline < nOrderLine.getLength(); oline++) {
						Element eOrderLine = (Element) nOrderLine.item(oline);
						if (eOrderLine != null) {
							// Get the total OrderedQty of unselected GiftBox
							// line items.
							Element eItem = YRCXmlUtils.getChildElement(
									eOrderLine, "Item");
							if (eItem != null) {
								sProductLine = eItem
										.getAttribute("ProductLine");
								if (!YRCPlatformUI.equals(sProductLine, "")
										&& YRCPlatformUI.equals(sProductLine,
												HBCPCAConstants.GIFT_BOX_ITEM)) {
									gborderLineKeyList.add(eOrderLine.getAttribute("OrderLineKey"));
									giftBoxItemId = eItem
											.getAttribute("ItemID");
									if (!tempItemIds.contains(giftBoxItemId)) {
										tempItemIds = tempItemIds + ","
												+ giftBoxItemId;
									}
									totalGBQty += Double
											.parseDouble(YRCXmlUtils
													.getAttribute(eOrderLine,
															"OrderedQty"));
								}
							}
						}
					}
				}
				if(totalGBQty != totalGWQty){
					if (totalGBQty >= 1) {
						if (totalGWQty > totalGBQty) {
							if(totalGBQty == 1){
								return true;
							}
							double qty = totalGWQty - totalGBQty;
							if (totalGBQty == 0) {
								HBCPCAUtil.showOptionDialogBoxWithOK("ADD_GIFT_BOX_LINE_TITLE",
										YRCPlatformUI.getFormattedString("ADD_GIFT_BOX_MSG",""));
								return false;
							} else {
								HBCPCAUtil.showOptionDialogBoxWithOK("ADD_GIFT_BOX_LINE_TITLE", YRCPlatformUI
												.getFormattedString("ADD_GB_OR_GW_LINE_MSG",""));
								return false;
							}
						} else {
							double qty = totalGBQty - totalGWQty;
							if (totalGWQty == 0) {
								HBCPCAUtil.showOptionDialogBoxWithOK(
										"ADD_GIFT_WRAP_TITLE", YRCPlatformUI.getFormattedString("ADD_GIFT_WRAP_MSG",
														""));
								return false;
							} else {
								HBCPCAUtil.showOptionDialogBoxWithOK("ADD_GIFT_WRAP_TITLE", YRCPlatformUI
												.getFormattedString("ADD_GB_OR_GW_LINE_MSG",""));
								return false;
							}
						}
				} else {
					double qty = totalGWQty - totalGBQty;
					HBCPCAUtil.showOptionDialogBoxWithOK("ADD_GIFT_BOX_LINE_TITLE",  YRCPlatformUI
							.getFormattedString("ADD_GIFT_BOX_MSG",""));
					return false;
					}
				}


		}
		return true;
	}

	/**
	 * i)Check if we have one quantity of one Order line with ProductLine=5 (GiftBox) and no other lines with GiftWrap=Y
	 * ii)Check if No.of GiftWraps Quantity  == NO.of GiftBoxes Quantity then go ahead with Adding GiftBox Line
	 * Otherwise Prompting the message User to add the GiftBox line OR remove GiftWrap option on the existing order lines
	 * iii)Check if we have one GiftBox with More than one GiftWraps then also go ahead with Adding GiftBox Line
	 * @param orderLines
	 ** @return true
	 */

	public ArrayList<String> getAddItems() {
		return addItems;
	}

	public void setAddItems(ArrayList<String> addItems) {
		this.addItems = addItems;
	}

	public Element getResponseFromAMS() {
		return responseFromAMS;
	}

	public void setResponseFromAMS(Element responseFromAMS) {
		this.responseFromAMS = responseFromAMS;
	}
	private boolean hasFreegifts(Element eleOrderLine) {
		//Element eleExtnPassthroughRewardsList  = YRCXmlUtils.getChildElement(eleOrderLine, "ExtnPassthroughRewardsList");
		ArrayList extnPassthroughRewardsList = YRCXmlUtils.getChildren(eleOrderLine, "ExtnPassthroughRewardsList");
		if (extnPassthroughRewardsList != null && extnPassthroughRewardsList.size() > 0) {
			for (int i = 0; i < extnPassthroughRewardsList.size(); i++) {
				Element eleExtnPassthroughRewardsList = (Element) extnPassthroughRewardsList.get(i);
				if (extnPassthroughRewardsList != null) {
					ArrayList passthroughRewardsList = YRCXmlUtils
							.getChildren(eleExtnPassthroughRewardsList,"PassthroughRewards");
					if (passthroughRewardsList != null && passthroughRewardsList.size() > 0) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private void getSystemAvalilableStatus(){
		systemAvailableStatus = new HashMap<String,String>();
		Document inputDocCommonCode;
		try {
			inputDocCommonCode = XMLUtil.createDocument(HBCPCAConstants.E_COMMONCODE);
			Element inputCCEle = inputDocCommonCode.getDocumentElement();
			inputCCEle.setAttribute(HBCPCAConstants.A_CODETYPE, HBCPCAConstants.SYSTEM_AVAILABLE);
			YRCApiContext yrcApiContext = new YRCApiContext();
			yrcApiContext.setApiName(HBCPCAConstants.API_GET_COMMON_CODE_LIST);
			yrcApiContext.setInputXml(inputDocCommonCode);
			yrcApiContext.setFormId(this.getFormId());
			new YRCApiCaller(yrcApiContext, true).invokeApi();
			handleApiCompletion(yrcApiContext);
			if(yrcApiContext.getOutputXml()!= null){
				Document outputCCDoc = yrcApiContext.getOutputXml();
				if(!XMLUtil.isVoid(outputCCDoc)){
					Element outputCCDocEle = outputCCDoc.getDocumentElement();
					if(!XMLUtil.isVoid(outputCCDocEle)){
						NodeList outputCCNodeList = outputCCDocEle.getElementsByTagName(HBCPCAConstants.E_COMMONCODE);
						if(!XMLUtil.isVoid(outputCCNodeList)){
							for(int i=0;i < outputCCNodeList.getLength(); i++){
								Element outputCCEle = (Element) outputCCNodeList.item(i);
								if(!XMLUtil.isVoid(outputCCEle)){
									String systemName = outputCCEle.getAttribute(HBCPCAConstants.A_CODEVALUE);
									String availableStatus = outputCCEle.getAttribute(HBCPCAConstants.A_CODESHORTDESC);
									if(!XMLUtil.isVoid(systemName) && !systemName.trim().equals("") &&
											!XMLUtil.isVoid(availableStatus) && !availableStatus.trim().equals("")){
										systemAvailableStatus.put(systemName, availableStatus);
									}
								}
							}
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	@Override
	public Element getModel(String arg0) {
		// TODO Auto-generated method stub
		return super.getModel(arg0);
	}

	boolean validateFakeEmailIDForVGCLine(){
		boolean emailIDFlag = false;
		Element eleOrderDetails =  this.getModel("OrderDetails");
		if(!XMLUtil.isVoid(eleOrderDetails)){
			Element elePersonInfoShipTo = YRCXmlUtils.getChildElement(eleOrderDetails, "PersonInfoShipTo");
			if(!XMLUtil.isVoid(elePersonInfoShipTo)){
				String strEmailID = elePersonInfoShipTo.getAttribute("EMailID");
				if(strEmailID != null && !strEmailID.isEmpty()){
					getFakeDomainName();
					String[] emailParts = strEmailID.split("@");
		    		String domain = emailParts[1];
		    		for(String strDomainName : domainName){
		    			if (strDomainName.equals(domain)){
		    				emailIDFlag = true;
		    			}
		    		}

				}
			}
		}
		return emailIDFlag;
	}

	boolean validateVGCLine(){
		boolean VGCLineFlag = false;
		ArrayList childExtnBehaviors = this.getChildExtnBehaviors();
		for (int i = 0; i < childExtnBehaviors.size(); i++) {
			if (childExtnBehaviors != null && childExtnBehaviors.get(i) instanceof HBCOrderEntryLinePanelExtnBehavior) {
				String sProdLine="";
				HBCOrderEntryLinePanelExtnBehavior entryLinePanelExtnBehavior = (HBCOrderEntryLinePanelExtnBehavior) childExtnBehaviors.get(i);
				Element eleExtnItemList = entryLinePanelExtnBehavior.getFromModel("ItemDetails");
				if (eleExtnItemList != null) {
					Element elePrimaryInfo = YRCXmlUtils.getChildElement(eleExtnItemList, "PrimaryInformation");
					if (elePrimaryInfo != null) {
						sProdLine=elePrimaryInfo.getAttribute("ProductLine");
						if(YRCPlatformUI.equals(sProdLine, HBCPCAConstants.VGC_ITEM)){
							VGCLineFlag = true;
							return VGCLineFlag;
						}

					}
				}
			}
		}
		return VGCLineFlag;
	}

	private String fetchCurrentPageID() {
		String strCurrentPageID = "";
		Composite currentPageComp = YRCDesktopUI.getCurrentPage();
		if (currentPageComp instanceof YRCWizard) {
			YRCWizard yrcWizard = (YRCWizard) currentPageComp;
			strCurrentPageID = yrcWizard.getCurrentPageID();
		}else if (currentPageComp instanceof IYRCComposite) {
			IYRCComposite iyrComp = (IYRCComposite)currentPageComp;
			strCurrentPageID = iyrComp.getClass().getName();
		}
		return strCurrentPageID;
	}
	public void getFakeDomainName(){
		Document commonCodeInput = YRCXmlUtils.createDocument("CommonCode");
		commonCodeInput.getDocumentElement().setAttribute("CodeType", "EMAIL_ID");

		YRCApiContext yrcApiContext = new YRCApiContext();
		yrcApiContext.setApiName("getCommonCodeList");
		yrcApiContext.setInputXml(commonCodeInput);
		yrcApiContext.setFormId(this.getFormId());
		new YRCApiCaller(yrcApiContext, true).invokeApi();
		handleApiCompletion(yrcApiContext);
	}
//	Start Zero Dollar CR
	private boolean findZeroDollarPriceItem(ArrayList childExtnBehaviors) {
		YRCValidationResponse response = null;
		boolean zeroPriceItem=false;
		for (int i = 0; i < childExtnBehaviors.size(); i++) {
			if(childExtnBehaviors.get(i) instanceof HBCOrderEntryLinePanelExtnBehavior){
			HBCOrderEntryLinePanelExtnBehavior entryLinePanelExtnBehavior = (HBCOrderEntryLinePanelExtnBehavior) childExtnBehaviors.get(i);
			Element eleOrderLineInput = entryLinePanelExtnBehavior.getOrderLineModel("OrderLine");
			Element eleItem = YRCXmlUtils.getChildElement(eleOrderLineInput, "Item");
			Element eleLinePrice = YRCXmlUtils.getChildElement(eleOrderLineInput, "LinePriceInfo");
			Element eleModelOrderLine = entryLinePanelExtnBehavior.getFromModel("OrderLines");
			if(eleModelOrderLine != null){
				String strShipTogetherNo =  eleModelOrderLine.getAttribute("ShipTogetherNo");
				if(strShipTogetherNo != null && (strShipTogetherNo.equals("GWP_Order") || strShipTogetherNo.equals("GWP_OrderLine")) )
				continue;
			}
			if(eleOrderLineInput != null && eleItem.getAttribute("ItemID")!=null && eleItem.getAttribute("ItemID").length() > 0){
				if(eleLinePrice.getAttribute("UnitPrice")!=null && eleLinePrice.getAttribute("UnitPrice").length() >0){
					String strUnitPrice=eleLinePrice.getAttribute("UnitPrice");
					if (strUnitPrice == null || strUnitPrice.equals("0.00")
							|| strUnitPrice.equals("0")||strUnitPrice.equals("0.0")){
						zeroPriceItem=true;
					}
				}
			}
			}
		}
		return zeroPriceItem;
	}

	//END Zero dollar CR
	//-- Kumar : Method : Start (3/3) : Check for same Pickup Store
	private boolean validatePickUpShipNodes(Element eleChangeOrder, String sPage){
		String sRefShipNode = null;
		String sRefDeliverytMethod = null;
		Element eleOrderLines = null;

		Element eleOrder = null;
		Element eleOrderLine = null;
		NodeList nOrderLineList = null;
		String sShipNode = null;

		boolean bFirstTimeEntry = true;

		if(sPage.equalsIgnoreCase("AdvanceAddItemPage") || sPage.equalsIgnoreCase("FullfillmentSummaryPage")){
			//-- get Orderline details from changeOrder element passed in argument.
			nOrderLineList = eleChangeOrder.getElementsByTagName("OrderLine");
			//--get instance of HBCOrderEntryLinePanelExtnBehavior : check if any line info is present.
			ArrayList childExtnBehaviorsList = this.getChildExtnBehaviors();
			for (int i = 0; i < childExtnBehaviorsList.size(); i++) {
				if(childExtnBehaviorsList.get(i) instanceof HBCOrderEntryLinePanelExtnBehavior ){
					HBCOrderEntryLinePanelExtnBehavior orderEntryLinePanelExtnBehavior= (HBCOrderEntryLinePanelExtnBehavior)this.getChildExtnBehaviors().get(i);
					eleOrderLines = orderEntryLinePanelExtnBehavior.getFromModel("OrderLines");
					if(!XMLUtil.isVoid(eleOrderLines) && eleOrderLines != null){
						sRefShipNode = eleOrderLines.getAttribute("ShipNode");
						sRefDeliverytMethod = eleOrderLines.getAttribute("DeliveryMethod");
						//-- if line info present and is pick up order : take one line as reference
						if(!XMLUtil.isVoid(sRefShipNode) && !XMLUtil.isVoid(sRefDeliverytMethod) && sRefDeliverytMethod.equalsIgnoreCase("PICK")){
							bFirstTimeEntry = false ;
						}
					}
					break;
				}
			}
			//-- If its first time entry all details will be in changeOrder element passed as argument
			if(bFirstTimeEntry){
				Element eleOrderLineCO = (Element)nOrderLineList.item(0);
				//-- set reference shipnode for validating with other shipnodes.
				sRefShipNode = eleOrderLineCO.getAttribute("ShipNode");
				sRefDeliverytMethod = eleOrderLineCO.getAttribute("DeliveryMethod");
				for(int l=0; nOrderLineList.getLength()>l; l++){
					eleOrderLineCO = (Element)nOrderLineList.item(l);
					if(!XMLUtil.isVoid(sRefDeliverytMethod) && sRefDeliverytMethod.equalsIgnoreCase("PICK")){
						if(!sRefShipNode.equalsIgnoreCase(eleOrderLineCO.getAttribute("ShipNode"))){
							return false;
						}
					}
				}
			}
			else{
				Element eleOrderLineCO = null;
				String sShipNodeCO = null;
				for(int l=0; nOrderLineList.getLength()>l; l++){
					eleOrderLineCO = (Element)nOrderLineList.item(l);
					if(!XMLUtil.isVoid(sRefDeliverytMethod) && sRefDeliverytMethod.equalsIgnoreCase("PICK")){
						sShipNodeCO = eleOrderLineCO.getAttribute("ShipNode");
						if(!XMLUtil.isVoid(sShipNodeCO) && !sRefShipNode.equalsIgnoreCase(sShipNodeCO)){
							return false;
						}
					}
				}
			}
		}
		else if(sPage.equalsIgnoreCase("PICKUP_NODE_CHECK")){
			ArrayList lstChildExtnBehaviour=this.getChildExtnBehaviors();
            for(int iExtnCounter=0;iExtnCounter<lstChildExtnBehaviour.size();iExtnCounter++){
            	if(lstChildExtnBehaviour.get(iExtnCounter) instanceof HBCDeliveryOptionsPage ){
            	HBCDeliveryOptionsPage extnBehaviour=(HBCDeliveryOptionsPage)lstChildExtnBehaviour.get(iExtnCounter);
            	if(!YRCPlatformUI.isVoid(extnBehaviour)){
                	return extnBehaviour.validatePickNode(sPage);
                    }
            	}
            }
		}
		return true;
	}
	//--Kumar : Stop (3/3) : : Check for same Pickup Store


	@Override
	public IYRCComposite createPage(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void pageBeingDisposed(String arg0) {
		// TODO Auto-generated method stub

	}
	//updating PersonInfoShipTo with store address for pick up orders during creation of orders in COM
	 public void updatePersonInfoShipTo(Document docChangeOrder, Element eleOrderLine, Element eleChangeOrderline) {

		 Element eleShipNode = YRCXmlUtils.getChildElement(eleOrderLine, "Shipnode");
		 Element eleShipNodePersonInfo = YRCXmlUtils.getChildElement(eleShipNode, "ShipNodePersonInfo");
		 Element elePersonInfo = YRCXmlUtils.getChildElement(eleOrderLine, "PersonInfoShipTo");

		 Element elePersonInfoShipTo = YRCXmlUtils.createChild(eleChangeOrderline, "PersonInfoShipTo");

         // verifying whether eleShipNodePersonInfo is null or not
		 if(eleShipNodePersonInfo != null)
		 {
             //fetching the attributes
			 elePersonInfoShipTo.setAttribute("FirstName", YRCXmlUtils.getAttribute(elePersonInfo, "FirstName") );
			 elePersonInfoShipTo.setAttribute("LastName", YRCXmlUtils.getAttribute(elePersonInfo, "LastName"));
			 elePersonInfoShipTo.setAttribute("EMailID", YRCXmlUtils.getAttribute(elePersonInfo, "EMailID"));

			 elePersonInfoShipTo.setAttribute("MiddleName", YRCXmlUtils.getAttribute(eleShipNodePersonInfo, "MiddleName"));
			 elePersonInfoShipTo.setAttribute("AddressLine1", YRCXmlUtils.getAttribute(eleShipNodePersonInfo, "AddressLine1"));
			 elePersonInfoShipTo.setAttribute("AddressLine2", YRCXmlUtils.getAttribute(eleShipNodePersonInfo, "AddressLine2"));
			 elePersonInfoShipTo.setAttribute("AddressLine3", YRCXmlUtils.getAttribute(eleShipNodePersonInfo, "AddressLine3"));
			 elePersonInfoShipTo.setAttribute("AddressLine4", YRCXmlUtils.getAttribute(eleShipNodePersonInfo, "AddressLine4"));
			 elePersonInfoShipTo.setAttribute("AddressLine5", YRCXmlUtils.getAttribute(eleShipNodePersonInfo, "AddressLine5"));
			 elePersonInfoShipTo.setAttribute("AddressLine6", YRCXmlUtils.getAttribute(eleShipNodePersonInfo, "AddressLine6"));
			 elePersonInfoShipTo.setAttribute("AlternateEmailID", YRCXmlUtils.getAttribute(eleShipNodePersonInfo, "AlternateEmailID"));
			 elePersonInfoShipTo.setAttribute("DayPhone", YRCXmlUtils.getAttribute(eleShipNodePersonInfo, "DayPhone"));
			 elePersonInfoShipTo.setAttribute("Company", YRCXmlUtils.getAttribute(eleShipNodePersonInfo, "Company"));
			 elePersonInfoShipTo.setAttribute("City", YRCXmlUtils.getAttribute(eleShipNodePersonInfo, "City"));
			 elePersonInfoShipTo.setAttribute("Country", YRCXmlUtils.getAttribute(eleShipNodePersonInfo, "Country"));
			 elePersonInfoShipTo.setAttribute("State", YRCXmlUtils.getAttribute(eleShipNodePersonInfo, "State"));
			 elePersonInfoShipTo.setAttribute("ZipCode", YRCXmlUtils.getAttribute(eleShipNodePersonInfo, "ZipCode"));
			 elePersonInfoShipTo.setAttribute("MobilePhone", YRCXmlUtils.getAttribute(eleShipNodePersonInfo, "MobilePhone"));
			 elePersonInfoShipTo.setAttribute("EveningPhone", YRCXmlUtils.getAttribute(eleShipNodePersonInfo, "EveningPhone"));
			 elePersonInfoShipTo.setAttribute("DayFaxNo", YRCXmlUtils.getAttribute(eleShipNodePersonInfo, "DayFaxNo"));
			 elePersonInfoShipTo.setAttribute("Department", YRCXmlUtils.getAttribute(eleShipNodePersonInfo, "FirstName")+" "+
					 YRCXmlUtils.getAttribute(eleShipNodePersonInfo, "LastName"));
			 elePersonInfoShipTo.setAttribute("EveningFaxNo", YRCXmlUtils.getAttribute(eleShipNodePersonInfo, "EveningFaxNo"));
			 elePersonInfoShipTo.setAttribute("OtherPhone", YRCXmlUtils.getAttribute(eleShipNodePersonInfo, "OtherPhone"));
			 elePersonInfoShipTo.setAttribute("PersonID",  YRCXmlUtils.getAttribute(eleShipNodePersonInfo, "PersonID"));
			 elePersonInfoShipTo.setAttribute("PreferredShipAddress",  YRCXmlUtils.getAttribute(eleShipNodePersonInfo, "PreferredShipAddress"));
		 }
	 }
}
//TODO Validation required for a Button control: extn_OrderGWP
//TODO Validation required for a Button control: buttonNext
//TODO Validation required for a Button control: btnNext
//TODO Validation required for a Button control: btnConfirm