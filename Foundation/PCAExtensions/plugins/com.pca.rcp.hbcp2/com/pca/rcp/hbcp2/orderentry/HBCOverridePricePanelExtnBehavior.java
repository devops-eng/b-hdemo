package com.pca.rcp.hbcp2.orderentry;


import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.eclipse.swt.widgets.Text;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import com.pca.rcp.hbcp2.util.HBCPCAConstants;
import com.pca.rcp.hbcp2.util.HBCPCAUtil;
import com.pca.rcp.hbcp2.util.XMLUtil;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCExtentionBehavior;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCXmlUtils;
import com.yantra.yfc.rcp.internal.YRCApiCaller;
/**
 * 
 * @author Navya Nagaraju
 * 
 */

public class HBCOverridePricePanelExtnBehavior extends YRCExtentionBehavior{
		
	List userGroupKeyList = new ArrayList();
	String strEnterprisecode="DEFAULT";
	@Override
	public void postCommand(YRCApiContext yrcapicontext) {
		super.postCommand(yrcapicontext);
	}

	@Override
	public boolean preCommand(YRCApiContext yrcapicontext) {
		return super.preCommand(yrcapicontext);
	}

	@Override
	public void validateComboField(String arg0, String arg1) {
		
		super.validateComboField(arg0, arg1);
	}
	@Override
	public void handleApiCompletion(YRCApiContext ctx) {
		// TODO Auto-generated method stub		
		super.handleApiCompletion(ctx);
	}
	

	@Override
	public void postSetModel(String model) {
		
		if(model.equals("UserNameSpace")){
		Element eleUser = this.getModel("UserNameSpace");
		if(eleUser != null && !YRCPlatformUI.isVoid(eleUser.getAttribute("EnterpriseCode"))){
			strEnterprisecode=eleUser.getAttribute("EnterpriseCode");
		}
		Element eleUserGroupLists = YRCXmlUtils.getChildElement(eleUser, "UserGroupLists"); 
		List nlUserGroupList = XMLUtil.getElementsByTagName(eleUserGroupLists, "UserGroupList");
		for (int i = 0;i< nlUserGroupList.size(); i++){
			Element eleUserGroupList = (Element)nlUserGroupList.get(i);
			Element eleUserGroup = YRCXmlUtils.getChildElement(eleUserGroupList,"UserGroup");
			String strUsergroupId = eleUserGroup.getAttribute("UsergroupId");
			userGroupKeyList.add(strUsergroupId);
		}
		}
		super.postSetModel(model);
	}
	

	
	public Element getFromModel(String model){
		return this.getModel(model);
	}

	@Override
	public YRCValidationResponse validateButtonClick(String fieldName) {
		YRCValidationResponse response = null;	
		Double zero = new Double("0.0");
		HBCOrderEntryLinePanelExtnBehavior temp = new HBCOrderEntryLinePanelExtnBehavior();		
		String lineprice=temp.unitPrice;
		Double originalPrice = Double.parseDouble(lineprice);
		String strOverridenPrice=this.getFieldValue("textOverriddenPrice");
		if (fieldName.equals("buttonClose")) {
			if(YRCPlatformUI.isVoid(strOverridenPrice)){
				YRCPlatformUI.showError(YRCPlatformUI.getFormattedString("ERROR",""), YRCPlatformUI.getFormattedString("MANDATORY_FIELD",""));
				return null;
			}
			strOverridenPrice=HBCPCAUtil.getFormattedCurrencyNumber(strOverridenPrice); 
			Double overridenPrice = Double.parseDouble(strOverridenPrice);
			if (overridenPrice.equals(zero)) {
				HBCPCAUtil.showOptionDialogBoxWithOK(YRCPlatformUI
						.getFormattedString("OVERRIDE_PRICE_TITLE", ""),
						YRCPlatformUI.getFormattedString(
								"PRICE_CANNOT_BE_ZERO", ""));
				response = new YRCValidationResponse(
						YRCValidationResponse.YRC_VALIDATION_ERROR, "");
				Text text = (Text) HBCOrderEntryLinePanelExtnBehavior.getControl(this.getOwnerForm(),
				"textOverriddenPrice");
				text.setFocus();
			} else if (!overridenPrice.equals("0.00")) {
				Double max_disc_amt = getMaxPercentageDicount(overridenPrice,
						originalPrice);				
				if (max_disc_amt != 0.00) {
					DecimalFormat df=new DecimalFormat("0.00");
					String formate = df.format(max_disc_amt); 
					HBCPCAUtil.showOptionDialogBoxWithOK(YRCPlatformUI
							.getFormattedString("OVERRIDE_PRICE_TITLE", ""),
							YRCPlatformUI
									.getFormattedString("MAX_DISC_PRICE",
											new String[] { String
													.valueOf(formate) }));
					Text text = (Text) HBCOrderEntryLinePanelExtnBehavior.getControl(this.getOwnerForm(),
					"textOverriddenPrice");
					text.setFocus();
				} else {
					response = new YRCValidationResponse(
							YRCValidationResponse.YRC_VALIDATION_OK, "");
				}
			}
		}	
		
		return response;
	}
	
	private Double getMaxPercentageDicount(Double overridenPrice,
			Double originalPrice) {

		Document inputDocCommonCode;
		Double maxDisc = 0.00;
		HashMap userGroupMap=new HashMap();

		try {
			inputDocCommonCode = XMLUtil
					.createDocument(HBCPCAConstants.E_COMMONCODE);
			Element inputCCEle = inputDocCommonCode.getDocumentElement();
			inputCCEle.setAttribute(HBCPCAConstants.A_CODETYPE,
					HBCPCAConstants.PERC_ITEM_DISCOUNT);
			inputCCEle.setAttribute("OrganizationCode",strEnterprisecode);
			YRCApiContext yrcApiContext = new YRCApiContext();
			yrcApiContext.setApiName(HBCPCAConstants.API_GET_COMMON_CODE_LIST);
			yrcApiContext.setInputXml(inputDocCommonCode);
			yrcApiContext.setFormId(this.getFormId());
			new YRCApiCaller(yrcApiContext, true).invokeApi();
			handleApiCompletion(yrcApiContext);
			if (yrcApiContext.getOutputXml() != null) {
				Document outputCCDoc = yrcApiContext.getOutputXml();
				if (!XMLUtil.isVoid(outputCCDoc)) {
					Element outputCCDocEle = outputCCDoc.getDocumentElement();
					if (!XMLUtil.isVoid(outputCCDocEle)) {
						NodeList outputCCNodeList = outputCCDocEle
								.getElementsByTagName(HBCPCAConstants.E_COMMONCODE);
						if (!XMLUtil.isVoid(outputCCNodeList)) {
							for (int i = 0; i < outputCCNodeList.getLength(); i++) {
								Element outputCCEle = (Element) outputCCNodeList
										.item(i);
								if (!XMLUtil.isVoid(outputCCEle)) {
									String strUserGroup=outputCCEle
									.getAttribute(HBCPCAConstants.A_CODEVALUE);
									String percentage = outputCCEle
											.getAttribute("CodeShortDescription");
									if(!userGroupMap.containsKey(strUserGroup)){
										userGroupMap.put(strUserGroup, percentage);
									}
								
								}
							}
							Double config_percItemDisc = 0.0;
							for(int k = 0 ; k< userGroupKeyList.size() ; k++){
								String strUserGroup=(String)(userGroupKeyList.get(k));
							if(userGroupMap.containsKey(strUserGroup)){	
								Double percItemDisc = Double
								.parseDouble(userGroupMap.get(strUserGroup).toString());
								if(config_percItemDisc < percItemDisc){
									config_percItemDisc=percItemDisc;
								}						
								
							}
						}								 
						Double exp = (config_percItemDisc / 100)
								* originalPrice;
						Double maxOverridenPrice=originalPrice-exp;//To get the Maximum overridden Price
						DecimalFormat df=new DecimalFormat("0.00");
						maxOverridenPrice = Double.parseDouble(HBCPCAUtil.getFormattedCurrencyNumber(df.format(maxOverridenPrice))); 
						if (maxOverridenPrice > overridenPrice) {							
								maxDisc = maxOverridenPrice;							
						}
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return maxDisc;
	}
	
	
}
