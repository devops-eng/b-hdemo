package com.pca.rcp.hbcp2.orderentry;

import org.eclipse.swt.widgets.Composite;
import org.w3c.dom.Element;

import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCBehavior;
import com.yantra.yfc.rcp.YRCXmlUtils;

public class HBCPreferedLanguagesPageExtnBehavior extends YRCBehavior {
	String preferedLangSelected = "";
	Element ePrerferedLanguage = null;

	public void init() {
		this.callGetPreferredLanguageList();
	}

	private void callGetPreferredLanguageList() {

		Element eCommonCode = YRCXmlUtils.createDocument("CommonCode")
				.getDocumentElement();
		eCommonCode.setAttribute("CodeType", "LANGUAGE");
		YRCApiContext ctxApi = new YRCApiContext();
		ctxApi.setApiName("getCommonCodeList");
		ctxApi.setFormId(getFormId());
		ctxApi.setInputXml(eCommonCode.getOwnerDocument());
		this.callApi(ctxApi);
	}

	public void handleApiCompletion(YRCApiContext ctx) {
		if (ctx.getApiName().equals("getCommonCodeList")) {
			Element ePrerferedLanguage = ctx.getOutputXml()
					.getDocumentElement();
			this.setModel("Extn_PreferredLanguageList", ePrerferedLanguage);
		}
		super.handleApiCompletion(ctx);
	}

	public HBCPreferedLanguagesPageExtnBehavior(Composite arg0, String formID) {

		super(arg0, formID);
	}

	public String getPreferedLangSelected() {
		return preferedLangSelected;
	}

	public void setPreferedLangSelected(String preferedLangSelected) {
		this.preferedLangSelected = preferedLangSelected;
	}

	public Element getFromModel(String model) {
		return this.getModel(model);
	}

	@Override
	public Element getTargetModel(String namespace) {
		return super.getTargetModel(namespace);
	}
}
