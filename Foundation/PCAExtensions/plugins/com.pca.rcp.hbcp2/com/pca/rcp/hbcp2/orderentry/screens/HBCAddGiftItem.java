package com.pca.rcp.hbcp2.orderentry.screens;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.pca.rcp.hbcp2.util.HBCPCAConstants;
import com.pca.rcp.hbcp2.util.HBCTableImageProvider;
import com.yantra.yfc.rcp.IYRCCellModifier;
import com.yantra.yfc.rcp.IYRCComposite;
import com.yantra.yfc.rcp.IYRCDefaultServerImageProvider;
import com.yantra.yfc.rcp.IYRCPanelHolder;
import com.yantra.yfc.rcp.IYRCTableColumnTextProvider;
import com.yantra.yfc.rcp.IYRCTableImageProvider;
import com.yantra.yfc.rcp.YRCButtonBindingData;
import com.yantra.yfc.rcp.YRCConstants;
import com.yantra.yfc.rcp.YRCLabelBindingData;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCTableBindingData;
import com.yantra.yfc.rcp.YRCTblClmBindingData;

public class HBCAddGiftItem extends Composite implements IYRCComposite {

	
	private static final String FORM_ID = "com.pca.rcp.hbcp2.orderentry.screens.HBCAddGiftItem";
	private Composite pnlRoot = null;
	private Table tableGiftItems = null;
	//private Label labelHeading = null;
	private Label labelSelection = null;
	private HBCAddGiftItemBehaviour myBehavior = null;
	private Composite compositeForButtons = null;
	private  Button buttonApply = null;
	private Button buttonCancel = null;
	GWPCellModifier GwpCellModifier = null;
	protected Element myElement;
	private int PassthroughRewardsCount = 0;
	
	public HBCAddGiftItem(Composite parent, int style, Element OrderLine) {
		super(parent, style);
		generateRewardsCount(OrderLine);
		initialize(PassthroughRewardsCount);
		
		
		myBehavior = new HBCAddGiftItemBehaviour(this,FORM_ID,OrderLine);
		
	}

	
	private void generateRewardsCount(Element orderLine) {
		if(orderLine!=null){
			NodeList nExtnPassthroughRewardsList = orderLine.getElementsByTagName("ExtnPassthroughRewardsList");
			if(nExtnPassthroughRewardsList!=null){
				PassthroughRewardsCount = nExtnPassthroughRewardsList.getLength();
			}
		}
		
	}




	private void setBindingForButtons() {
		
		YRCButtonBindingData buttonBindingData = new YRCButtonBindingData();
    	buttonBindingData.setActionId(HBCPCAConstants.HBC_ADD_GIFT_APPLY_ACTION);
    	buttonBindingData.setActionHandlerEnabled(true);
    	buttonBindingData.setName("buttonApply");
    	buttonApply.setData(YRCConstants.YRC_BUTTON_BINDING_DEFINATION, buttonBindingData);
    	
    	
	}

	private void setBindingForTable(int RewardListNo) {
		
		YRCTableBindingData tableBindingData = new YRCTableBindingData();
		YRCTblClmBindingData[] clmBindingData = new YRCTblClmBindingData[tableGiftItems.getColumnCount()];

		
		clmBindingData[0] = new YRCTblClmBindingData();
		
		clmBindingData[0].setAttributeBinding("@Checked");
		clmBindingData[0].setColumnBinding("");
		clmBindingData[0].setMovable(false);
		//clmBindingData[0].setTargetAttributeBinding("@Checked");
		clmBindingData[0].setCheckedBinding("YES");
		clmBindingData[0].setUnCheckedBinding("NO");
		clmBindingData[0].setName("clmCheckSelection");		
		clmBindingData[0].setAdvanced(true);
		
		clmBindingData[1] = new YRCTblClmBindingData();     
		clmBindingData[1].setAttributeBinding("@ItemID");
		clmBindingData[1].setName("clmItemImage");
		clmBindingData[1].setColumnBinding(YRCPlatformUI.getString("CFO_Image_Title"));
		clmBindingData[1].setSortReqd(false); 
		clmBindingData[1].setIgnoreText(true);
		clmBindingData[1].setFilterReqd(false);
		clmBindingData[1].setServerImageConfiguration(YRCConstants.IMAGE_SMALL);
		clmBindingData[1].setDefaultServerImageProvider(new IYRCDefaultServerImageProvider(){
			public String getImageTheme(Object arg0) {
				if (arg0 instanceof Element)
				{
					Element elemOrderLine = (Element)arg0;
					if (YRCPlatformUI.equals("PS", elemOrderLine.getAttribute("ItemGroupCode")))
					{
						return "ProvidedServiceSmall";
					}					
				}
				return null;
			}
       	});
		
        
		clmBindingData[2] = new YRCTblClmBindingData();
		clmBindingData[2].setAttributeBinding("@ItemID");
		clmBindingData[2].setColumnBinding("Item");
		clmBindingData[2].setName("clmGiftItem");
		
		clmBindingData[2].setLabelProvider(new IYRCTableColumnTextProvider(){
			public String getColumnText(Element element) {
				return myBehavior.getColumnText(element, "clmGiftItem") ;
			}
       	});
		
		tableBindingData.setTblClmBindings(clmBindingData);
		tableBindingData.setSourceBinding("Items_"+RewardListNo+":Items/Item");
		tableBindingData.setName("tableGiftItems");
		
		tableBindingData.getTblClmBindings()[1].setImageUrlProvider(new HBCTableImageProvider(1, tableGiftItems));
		tableGiftItems.setData(YRCConstants.YRC_TABLE_BINDING_DEFINATION,tableBindingData);	
		
		tableBindingData.setImageProvider(new IYRCTableImageProvider() {
			public String getImageThemeForColumn(Object element, int columnIndex) {
				Element eleAccountSelected = (Element) element;
				String sAlreadyChecked = eleAccountSelected.getAttribute("Checked");
				if (columnIndex == 0) {
					if (! (YRCPlatformUI.isVoid(sAlreadyChecked)) && sAlreadyChecked.equals("YES")) {
						return "CheckBoxCheckedIcon";
					} else {
						return "CheckBoxUncheckedIcon";
					}
				}
				return null;
			}
		});

		String[] editors = new String[tableGiftItems.getColumnCount()];
//		 making the first column (check box) as editable.
		editors[0] = YRCConstants.YRC_CHECK_BOX_CELL_EDITOR;
		tableBindingData.setCellTypes(editors);
		tableBindingData.setCellModifierRequired(true);
		GwpCellModifier = new GWPCellModifier();
		tableBindingData.setCellModifier(GwpCellModifier);
		tableBindingData.setKeyNavigationRequired(true);
		//tableBindingData.setName("tableGiftItems");
		//tableGiftItems.setData(YRCConstants.YRC_TABLE_BINDING_DEFINATION,tableBindingData);
		
	}
	
	
	public class GWPCellModifier extends IYRCCellModifier {

		protected boolean allowModify(String property, String value,
				Element element) {
						
			return true;
		}

		protected int allowModifiedValue(String property, String value,
				Element element) {
			
			return IYRCCellModifier.MODIFY_OK;
		}

		protected String getModifiedValue(String property, String value,
				Element element) {			
			return value;
		}
	}

	private void setBindingForLabels(int RewardListNo) {
		
		
		YRCLabelBindingData  labelBindingData = new YRCLabelBindingData();
		/*labelBindingData.setSourceBinding("OrderLines:OrderLines/OrderLine/Item/@ItemID");
		labelBindingData.setName("labelHeading");
		labelHeading.setData(YRCConstants.YRC_LABEL_BINDING_DEFINITION, labelBindingData);*/
		
		labelBindingData = new YRCLabelBindingData();
		labelBindingData.setSourceBinding("PassthroughRewardsList_"+RewardListNo+":ExtnPassthroughRewardsList/@ExtnMaxSelections");
		
		labelBindingData.setDynamic(true);
		labelBindingData.setName("labelSelection");
		labelSelection.setData(YRCConstants.YRC_LABEL_BINDING_DEFINITION, labelBindingData);
		labelSelection.setData(YRCConstants.YRC_CONTROL_NAME, "labelSelection");
		
	}

	/**
	 * This method initializes this
	 * 
	 */
	private void initialize(int PassthroughRewardsCount) {
		this.setSize(new Point(353, 161));
		GridLayout thislayout = new GridLayout();
		thislayout.horizontalSpacing = 0;
		thislayout.marginWidth = 0;
		thislayout.verticalSpacing = 0;
		thislayout.marginHeight = 0;
		this.setLayout(thislayout);
		
        createComposite(PassthroughRewardsCount);
       

	}

	public String getFormId() {
		// TODO Auto-generated method stub
		return HBCAddGiftItem.FORM_ID;
	}

	public String getHelpId() {
		// TODO Auto-generated method stub
		return null;
	}

	public IYRCPanelHolder getPanelHolder() {
		// TODO Auto-generated method stub
		return null;
	}

	public Composite getRootPanel() {
		// TODO Auto-generated method stub
		return this.pnlRoot;
	}

	/**
	 * This method initializes composite	
	 *
	 */
	private void createComposite(int PassthroughRewardsCount) {
//		for pnlroot
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.numColumns = 1;
		
		// grid data for pnlroot
		gridLayout.marginHeight = 5;
		gridLayout.marginWidth = 5;
		gridLayout.verticalSpacing = 5;
		gridLayout.horizontalSpacing = 5;
		GridData gridDataForRoot = new GridData();
		gridDataForRoot.grabExcessHorizontalSpace = true;
		
		gridDataForRoot.horizontalAlignment = GridData.FILL;
		gridDataForRoot.verticalAlignment = GridData.FILL;
		gridDataForRoot.grabExcessVerticalSpace = true;
		pnlRoot = new Composite(this, SWT.INHERIT_NONE);
		
		pnlRoot.setLayoutData(gridDataForRoot);
		pnlRoot.setLayout(gridLayout);
		pnlRoot.setBounds(new Rectangle(1, 7, 313, 221));
		pnlRoot.setData(YRCConstants.YRC_CONTROL_NAME,"pnlRoot");
		pnlRoot.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE, "PanelHeader");
		
		pnlRoot.setVisible(true);
		
		if(PassthroughRewardsCount!=0){
			
			
				for(int i=0;i<PassthroughRewardsCount;i++){
					
					//for label selection
					GridData gridData2 = new GridData();
					gridData2.horizontalAlignment = GridData.FILL;
					gridData2.grabExcessHorizontalSpace = true;
					gridData2.verticalAlignment = GridData.CENTER;
					//for label heading
					/*GridData gridData1 = new GridData();
					gridData1.grabExcessHorizontalSpace = true;
					gridData1.verticalAlignment = GridData.CENTER;
					gridData1.horizontalAlignment = GridData.FILL;*/
					
					//for table
					GridData gridData = new GridData();
					gridData.grabExcessHorizontalSpace = true;
					gridData.verticalAlignment = GridData.FILL;
					gridData.grabExcessVerticalSpace = true;
					gridData.horizontalAlignment = GridData.FILL;
					
					
				/*	labelHeading = new Label(pnlRoot, SWT.BOLD|SWT.BORDER);
					labelHeading.setText("Gift Items For Selected Item");
					labelHeading.setLayoutData(gridData1);*/
					
					labelSelection = new Label(getRootPanel(), SWT.BACKGROUND);
					labelSelection.setText(YRCPlatformUI.getFormattedString("GWP_SELECTITEMS_FROM_LIST",""));
					labelSelection.setLayoutData(gridData2);
					labelSelection.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE, "BigBoldTextBlackOnLightBlue");
					
					tableGiftItems = new Table(pnlRoot, SWT.BORDER|SWT.INHERIT_NONE);
					tableGiftItems.setHeaderVisible(true);
					tableGiftItems.setLayoutData(gridData);
					tableGiftItems.setLinesVisible(true);
					
					
					TableColumn clmCheckSelection = new TableColumn(tableGiftItems, SWT.INHERIT_NONE);
					clmCheckSelection.setWidth(20);
					clmCheckSelection.setResizable(false);
					clmCheckSelection.setData(YRCConstants.YRC_CONTROL_NAME, "clmCheckSelection");
					
					TableColumn clmItemImage = new TableColumn(tableGiftItems, SWT.INHERIT_NONE);
					clmItemImage.setWidth(32);
					clmItemImage.setResizable(false);
					clmItemImage.setData(YRCConstants.YRC_CONTROL_NAME, "clmItemImage");
					
					TableColumn clmGiftItem = new TableColumn(tableGiftItems, SWT.INHERIT_NONE);
					clmGiftItem.setWidth(250);
					clmGiftItem.setResizable(true);
					clmGiftItem.setText(YRCPlatformUI.getFormattedString("Gift_Item", ""));
					clmGiftItem.setData(YRCConstants.YRC_CONTROL_NAME, "clmGiftItem");
					
					setBindingForLabels(i);
					setBindingForTable(i);
				}
		}
		
	
		
		createCompositeForButtons();
		setBindingForButtons();
	}

	/**
	 * This method initializes compositeForButtons	
	 *
	 */
	private void createCompositeForButtons() {
		GridData gridData4 = new GridData();
		gridData4.horizontalAlignment = GridData.END;
		gridData4.grabExcessHorizontalSpace = true;
		gridData4.verticalAlignment = GridData.CENTER;
		GridLayout gridLayout1 = new GridLayout();
		gridLayout1.numColumns = 2;
		GridData gridData3 = new GridData();
		gridData3.verticalSpan = 3;
		gridData3.verticalAlignment = GridData.FILL;
		gridData3.grabExcessHorizontalSpace = true;
		gridData3.grabExcessVerticalSpace = false;
		gridData3.horizontalAlignment = GridData.FILL;
		compositeForButtons = new Composite(getRootPanel(), SWT.INHERIT_NONE);
		compositeForButtons.setLayoutData(gridData3);
		compositeForButtons.setLayout(gridLayout1);
		compositeForButtons.setBounds(new Rectangle(300, 310, 220, 20));
		compositeForButtons.setFocus();
		compositeForButtons.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE, "PanelHeader");
		buttonApply = new Button(compositeForButtons, SWT.INHERIT_NONE);
		buttonApply.setText(YRCPlatformUI.getFormattedString("APPLY", ""));
		buttonApply.setLayoutData(gridData4);
		
	/*buttonApply.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
			 // TODO Auto-generated Event stub widgetSelected()
				YRCPlatformUI.fireAction(HBCPCAConstants.HBC_ADD_GIFT_APPLY_ACTION);
			}

			
		});*/
		buttonCancel = new Button(compositeForButtons, SWT.INHERIT_NONE);
		buttonCancel.setText(YRCPlatformUI.getFormattedString("CANCEL", ""));
		buttonCancel.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
					public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
						 // TODO Auto-generated Event stub widgetSelected()
						pnlRoot.getShell().close();
					}
				});
	}

	public Element getOutputElement() {
		// TODO Auto-generated method stub
		return myBehavior.getOutputElementMethod();
	}

	public Object getBehavior() {
		// TODO Auto-generated method stub
		return this.myBehavior;
	}
	
	

}  //  @jve:decl-index=0:visual-constraint="47,15"
