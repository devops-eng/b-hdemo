package com.pca.rcp.hbcp2.orderentry.screens;

import java.util.ArrayList;

import javax.xml.xpath.XPathConstants;

import org.eclipse.swt.widgets.Composite;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.pca.rcp.hbcp2.util.HBCPCAUtil;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCBehavior;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCXPathUtils;
import com.yantra.yfc.rcp.YRCXmlUtils;

public class HBCAddGiftItemBehaviour extends YRCBehavior {

	private Element selectedGiftItems = null;
	ArrayList<String> arrGiftItems ;
	private int totalGroupCount = 0;
	private int currentRef = 0;
	public HBCAddGiftItemBehaviour(Composite arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public HBCAddGiftItemBehaviour(Composite arg0, String arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
		
	}

	public HBCAddGiftItemBehaviour(Composite arg0, String arg1, Element eOrderLine) {
		super(arg0, arg1, eOrderLine);
		// TODO Auto-generated constructor stub
		//Element eOrderLine = YRCXmlUtils.getChildElement(orderLines, "OrderLine");
		
		String sGiftItemId = "";
		if(eOrderLine!=null){
			setModel("OrderLine",  eOrderLine);
			
			Document inputxmlForItemList = YRCXmlUtils.createDocument("Item");
			Element eItem = inputxmlForItemList.getDocumentElement();
			Element eComplexQuery = YRCXmlUtils.createChild(eItem, "ComplexQuery");
			Element eOr = YRCXmlUtils.createChild(eComplexQuery, "Or");
			
			NodeList nExtnPassthroughRewardsList = eOrderLine.getElementsByTagName("ExtnPassthroughRewardsList");
			if(nExtnPassthroughRewardsList!=null){
				for(int i=0;i<nExtnPassthroughRewardsList.getLength();i++){
					Element eExtnPassthroughRewardsList = (Element)nExtnPassthroughRewardsList.item(i);
					
					setTotalGroupCount(i);
					setModel("PassthroughRewardsList_"+i, eExtnPassthroughRewardsList);
					//Element eExtnPassthroughRewardsList = YRCXmlUtils.getChildElement(eOrderLine, "ExtnPassthroughRewardsList");
					if(eExtnPassthroughRewardsList!=null){
							//Element ePassthroughRewards = YRCXmlUtils.getChildElement(eExtnPassthroughRewardsList, "PassthroughRewards");
							
							
							NodeList nPassthroughRewards = eExtnPassthroughRewardsList.getElementsByTagName("PassthroughRewards");
							if(nPassthroughRewards!=null){
								arrGiftItems = new ArrayList<String>();
								for(int j=0;j<nPassthroughRewards.getLength();j++){
									Element ePassthroughRewards = (Element)nPassthroughRewards.item(j);
									if(ePassthroughRewards!=null){
										sGiftItemId = (YRCXmlUtils.getAttribute(ePassthroughRewards, "GiftItemId")).trim();
										if(sGiftItemId!=null && !sGiftItemId.equals("")){
											
											arrGiftItems.add(sGiftItemId);
											Element eExp = YRCXmlUtils.createChild(eOr, "Exp");
											YRCXmlUtils.setAttribute(eExp, "Name", "ItemID");
											YRCXmlUtils.setAttribute(eExp, "Value", sGiftItemId);
										}
									}
								}
								
									
									
							}
						}
					}
				YRCApiContext yrcApiContext = new YRCApiContext();
				yrcApiContext.setApiName("getItemList");
				yrcApiContext.setInputXml(inputxmlForItemList);
				yrcApiContext.setFormId(this.getFormId());
				this.callApi(yrcApiContext);
			
				//new YRCApiCaller(yrcApiContext, true).invokeApi();
				handleApiCompletion(yrcApiContext);
				}
			
			}
		
		
		
		
	
		
	}
	// set totalCount
	public void setTotalGroupCount(int i) {
		
		totalGroupCount = i;
	}
	// get TotalCount
	public int getTotalGroupCount(){
		return totalGroupCount;
	}
	

	
	
	public Element getMatchingItem(String itemId,NodeList nItem) {
		
		Element matchingElement = null;
		if (nItem != null) {
			for (int i = 0; i < nItem.getLength(); i++) {
				Element eleItem = (Element) nItem
						.item(i);
				
				if (eleItem != null) {
					String tempItemID = YRCXmlUtils.getAttribute(eleItem, "ItemID");
					if (tempItemID != null
							&& YRCPlatformUI.equals(tempItemID,
									itemId)) {
						matchingElement = eleItem;
					}
				}
			}
		}
		return matchingElement;

	}
	
	@Override
	public void handleApiCompletion(YRCApiContext ctx) {
		// TODO Auto-generated method stub
		if(YRCPlatformUI.equals(ctx.getApiName(),"getItemList")){
			
			Document ItemListDoc = ctx.getOutputXml();
			if(ItemListDoc!=null){
				Element eItemList = ItemListDoc.getDocumentElement();
				
				setModel("ItemList",eItemList);
				
				if(eItemList!=null){
				for(int k=0;k<=getTotalGroupCount();k++){
					Document DocItemList = YRCXmlUtils.createDocument("Items_"+k);
					Element eleItemList = DocItemList.getDocumentElement();
					Element ePassthroughRewardsList = getModel("PassthroughRewardsList_"+k);
					if(ePassthroughRewardsList!=null){
						NodeList nPassthroughRewards = ePassthroughRewardsList.getElementsByTagName("PassthroughRewards");
						for(int p=0;p<nPassthroughRewards.getLength();p++){
							Element ePassthroughRewards = (Element) nPassthroughRewards.item(p);
							String strGiftItemId = YRCXmlUtils.getAttribute(ePassthroughRewards, "GiftItemId");
							Element eMatchingItem = (Element) YRCXPathUtils.evaluate(eItemList, "Item[@ItemID='"+strGiftItemId+"']", XPathConstants.NODE);
							if(eMatchingItem!=null){
								YRCXmlUtils.importElement(eleItemList, eMatchingItem);
							}
						}
						
					}
					setModel("Items_"+k, eleItemList);
				}
				
				
				/*Iterator<String> GiftIterator = arrGiftItems.listIterator();
				while (GiftIterator.hasNext()) {
					String sItemID = (String) GiftIterator.next();
					Element eMatchingItem = (Element) YRCXPathUtils.evaluate(eItemList, "Item[@ItemID='"+sItemID+"']", XPathConstants.NODE);
					
				}*/
				
				
				}			
				
				}
			}
		
		super.handleApiCompletion(ctx);
	}

	
	@Override
	public String evaluateDynamicBinding(String control, Element ExtnPassthroughRewardsList) {
		// TODO Auto-generated method stub
		
		if(YRCPlatformUI.equals(control, "labelSelection")){
			//Element eOrderLine = YRCXmlUtils.getChildElement(OrderLines, "OrderLine");
			if(ExtnPassthroughRewardsList!=null){
				
					
					String sExtnMaxSelections = YRCXmlUtils.getAttribute(ExtnPassthroughRewardsList, "ExtnMaxSelections");
					++currentRef;
					String ref = Integer.toString(currentRef);
					if(sExtnMaxSelections!=null){
						
					String ModifiedString =YRCPlatformUI.getFormattedString("SELECT_MESSAGE", new String[]{sExtnMaxSelections,ref});
					
					return ModifiedString;
					}
				}
			}
			
		
		return super.evaluateDynamicBinding(control, ExtnPassthroughRewardsList);
	}
	

	/**this method is to check for no of selections
	 * @return
	 */
	public boolean checkSelections() {
		
		boolean isCorrectSelection = false;
		for(int p=0;p<=getTotalGroupCount();p++){
			Element ePassthroughRewardsList = getModel("PassthroughRewardsList_"+p);
			Element eItems = getModel("Items_"+p);
			
			String sExtnMaxSelections = "";
			String sChecked = "";
			int maxNoOfSelections = 0;
			int checkedCount = 0;
			
			
				if(ePassthroughRewardsList!=null){
					
					sExtnMaxSelections = YRCXmlUtils.getAttribute(ePassthroughRewardsList, "ExtnMaxSelections");
					if(sExtnMaxSelections!=null && !YRCPlatformUI.equals("", sExtnMaxSelections)){
						maxNoOfSelections = Integer.parseInt(sExtnMaxSelections);
					}
				
					NodeList nGiftItem = eItems.getElementsByTagName("Item");
					for(int i=0;i<nGiftItem.getLength();i++){
					Element eGiftItem = (Element) nGiftItem.item(i);
					if(eGiftItem!=null){
						sChecked = YRCXmlUtils.getAttribute(eGiftItem, "Checked");
						if(sChecked!=null && YRCPlatformUI.equals(sChecked, "YES")){
							++checkedCount;
						}
					}
					
					}
					if(maxNoOfSelections==0){
						HBCPCAUtil.showOptionDialogBoxWithOK(YRCPlatformUI.getFormattedString("GWP_MESSAGE",""),YRCPlatformUI.getFormattedString("SELECTION_MESSAGE2", ""));
						isCorrectSelection =  true;
					}else if(checkedCount!=0 && checkedCount<=maxNoOfSelections){
						
						
						isCorrectSelection =  true;
										
					}else if(checkedCount>maxNoOfSelections){
						
						isCorrectSelection = false;
						HBCPCAUtil.showOptionDialogBoxWithOK(YRCPlatformUI.getFormattedString("GWP_MESSAGE",""),YRCPlatformUI.getFormattedString("SELECTION_MESSAGE1", new Object[]{maxNoOfSelections,p+1}));
						break;
						
					}
					
				}
			}
		return isCorrectSelection;
	}

	public Element getOutputElementMethod() {
		// TODO Auto-generated method stub
		return this.getModel("SelectedGiftItems");
	}

	public boolean applySelected() {
		// TODO Auto-generated method stub
		boolean flag = this.checkSelections();
		if(flag){
			Document docselectedGiftItems = YRCXmlUtils.createDocument("Items");
			selectedGiftItems = docselectedGiftItems.getDocumentElement();
			for(int m=0;m<=getTotalGroupCount();m++){
				Element eItemsSelected = getModel("Items_"+m);
				if(eItemsSelected!=null){
					NodeList nItemSelected = eItemsSelected.getElementsByTagName("Item");
					for(int f=0;f<nItemSelected.getLength();f++){
						Element eItemSelected = (Element)nItemSelected.item(f);
						YRCXmlUtils.importElement(selectedGiftItems, eItemSelected);
					}
				}
			}
			setModel("SelectedGiftItems", selectedGiftItems);
			
		}
		return flag;
		
	}

	public String getColumnText(Element Item, String control) {
		// TODO Auto-generated method stub
		String sFormattedItemID = "";
		if(YRCPlatformUI.equals(control, "clmGiftItem")){
			String sItemID = "";
			String sShortDesc = "";
			
			sItemID = YRCXmlUtils.getAttribute(Item, "ItemID");
			Element ePrimaryInformation = YRCXmlUtils.getChildElement(Item, "PrimaryInformation");
			if(ePrimaryInformation!=null){
				sShortDesc = YRCXmlUtils.getAttribute(ePrimaryInformation, "ShortDescription");
				sFormattedItemID = YRCPlatformUI.getFormattedString("Gift_Item_ID", new String[]{sItemID,sShortDesc});
				return sFormattedItemID;
			}
			
		}
		return sFormattedItemID;
	}

}
