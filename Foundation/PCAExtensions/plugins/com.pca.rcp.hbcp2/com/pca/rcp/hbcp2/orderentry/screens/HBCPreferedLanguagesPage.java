package com.pca.rcp.hbcp2.orderentry.screens;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.w3c.dom.Element;

import com.pca.rcp.hbcp2.orderentry.HBCPreferedLanguagesPageExtnBehavior;
import com.yantra.yfc.rcp.IYRCComposite;
import com.yantra.yfc.rcp.IYRCPanelHolder;
import com.yantra.yfc.rcp.YRCComboBindingData;
import com.yantra.yfc.rcp.YRCConstants;
import com.yantra.yfc.rcp.YRCPlatformUI;

public class HBCPreferedLanguagesPage extends Composite implements
		IYRCComposite {
	private static final String FORM_ID = "com.pca.rcp.hbcp2.orderentry.screens.HBCPreferedLanguagesPage";
	private Composite compositeForButtons = null;
	private Composite pnlRoot = null;
	private Composite pnlLanguage = null;
	private Button btnApply = null;
	private Button btnClose = null;
	private Combo cmbPrefLanguages = null;
	protected Element ePrefLanguageList;
	String strPreferedLangSelected = "";

	private Label lblLanguage = null;
	private HBCPreferedLanguagesPageExtnBehavior myBehavior = null;

	public HBCPreferedLanguagesPage(Composite parent, int style) {
		super(parent, style);
		initialize();
		setBindingForComponents();
		myBehavior = new HBCPreferedLanguagesPageExtnBehavior(this, getFormId());

	}

	private void initialize() {
		this.setSize(new Point(353, 161));
		GridLayout thislayout = new GridLayout();
		thislayout.horizontalSpacing = 0;
		thislayout.marginWidth = 0;
		thislayout.verticalSpacing = 0;
		thislayout.marginHeight = 0;
		this.setLayout(thislayout);
		createComposite();
	}

	private void createComposite() {

		createRootPanel();
		// GridData for pnlLanguage
		GridData gridDataForLanguage = new GridData();
		gridDataForLanguage.grabExcessHorizontalSpace = true;
		gridDataForLanguage.horizontalAlignment = GridData.FILL;
		gridDataForLanguage.verticalAlignment = GridData.FILL;

		// GridLayout for pnlLanguage
		GridLayout glForPnlLanguage = new GridLayout();
		glForPnlLanguage.numColumns = 2;

		glForPnlLanguage.marginHeight = 5;
		glForPnlLanguage.marginWidth = 10;
		glForPnlLanguage.verticalSpacing = 1;
		glForPnlLanguage.horizontalSpacing = 1;

		// Composition for pnlLanguage
		pnlLanguage = new Composite(pnlRoot, SWT.NONE);
		pnlLanguage.setLayoutData(gridDataForLanguage);
		pnlLanguage.setLayout(glForPnlLanguage);
		pnlLanguage.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE, "TitleBarLabel");

		// GridData for lblLanguage
		GridData gdForlblLanguage = new GridData();
		gdForlblLanguage.horizontalAlignment = GridData.FILL;
		gdForlblLanguage.grabExcessHorizontalSpace = true;
		gdForlblLanguage.grabExcessVerticalSpace = true;
		gdForlblLanguage.verticalAlignment = GridData.FILL;

		// lblLanguage
		lblLanguage = new Label(pnlLanguage, SWT.INHERIT_NONE);
		lblLanguage.setText(YRCPlatformUI.getFormattedString("PREFERRED_LANGUAGE",""));
		lblLanguage.setLayoutData(gdForlblLanguage);
		lblLanguage.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE, "TitleBarLabel");

		cmbPrefLanguages = new Combo(pnlLanguage, SWT.READ_ONLY | SWT.DROP_DOWN );
		cmbPrefLanguages.setLayoutData(gdForlblLanguage);
		cmbPrefLanguages.setTextLimit(50);
		cmbPrefLanguages.setData("name", "cmbPrefLanguages");

		cmbPrefLanguages.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				if (!cmbPrefLanguages.getText().equals("")) {
					btnApply.setEnabled(true);
				} else {
					btnApply.setEnabled(false);
				}
			}

		});
		createCompositeForButtons();
	}

	private void setBindingForComponents() {
		YRCComboBindingData cbd = null;
		cbd = new YRCComboBindingData();
		cbd.setCodeBinding("CodeValue");
		cbd.setDescriptionBinding("CodeShortDescription");
		cbd.setListBinding("Extn_PreferredLanguageList:CommonCodeList/CommonCode");
		cbd.setTargetBinding("ExtnPreferredLanguage:/ExtnPreferredLanguage/@ExtnPreferredLanguage");
		cbd.setName("cmbPrefLanguages");
		cmbPrefLanguages.setData(YRCConstants.YRC_COMBO_BINDING_DEFINATION, cbd);
	}

	private void createRootPanel() {
		// GridLayout for main pnlRoot
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginHeight = 5;
		gridLayout.marginWidth = 5;
		gridLayout.verticalSpacing = 0;
		gridLayout.horizontalSpacing = 0;

		// GridData for pnlRoot
		GridData gridDataForpnlRoot = new GridData();
		gridDataForpnlRoot.grabExcessHorizontalSpace = true;
		gridDataForpnlRoot.horizontalAlignment = GridData.FILL;
		gridDataForpnlRoot.verticalAlignment = GridData.FILL;
		gridDataForpnlRoot.grabExcessVerticalSpace = true;

		// Composite for pnlRoot
		pnlRoot = new Composite(this, SWT.INHERIT_NONE);
		pnlRoot.setLayoutData(gridDataForpnlRoot);
		pnlRoot.setLayout(gridLayout);
		pnlRoot.setData(YRCConstants.YRC_CONTROL_NAME, "pnlRoot");
		pnlRoot.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE, "TaskComposite");
	}

	/**
	 * This method initializes compositeForButtons
	 * 
	 */
	private void createCompositeForButtons() {

		// GridData for gdForbtnApply
		GridData gdForBtnApply = new GridData();
		gdForBtnApply.horizontalAlignment = GridData.END;
		gdForBtnApply.grabExcessHorizontalSpace = true;
		gdForBtnApply.verticalAlignment = GridData.CENTER;

		// GridLayout for glForCompositeButtons
		GridLayout glForCompositeButtons = new GridLayout();
		glForCompositeButtons.numColumns = 2;

		// GridData for gdForCompositeButtons
		GridData gdForCompositeButtons = new GridData();
		gdForCompositeButtons.verticalSpan = 3;
		gdForCompositeButtons.verticalAlignment = GridData.FILL;
		gdForCompositeButtons.grabExcessHorizontalSpace = true;
		gdForCompositeButtons.grabExcessVerticalSpace = false;
		gdForCompositeButtons.horizontalAlignment = GridData.FILL;

		// Composite for compositeForButtons
		compositeForButtons = new Composite(pnlRoot, SWT.INHERIT_NONE);
		compositeForButtons.setLayoutData(gdForCompositeButtons);
		compositeForButtons.setLayout(glForCompositeButtons);
		compositeForButtons.setFocus();
		compositeForButtons.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE,
				"TaskComposite");

		// btnApply
		btnApply = new Button(compositeForButtons, SWT.INHERIT_NONE);
		btnApply.setText(YRCPlatformUI.getFormattedString("APPLY", ""));
		btnApply.setLayoutData(gdForBtnApply);
		btnApply.setEnabled(false);
		btnApply
				.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
					public void widgetSelected(
							org.eclipse.swt.events.SelectionEvent e) {
						Element eleExtnPreferredLanguage = myBehavior
								.getTargetModel("ExtnPreferredLanguage");
						if (eleExtnPreferredLanguage != null)
							myBehavior
									.setPreferedLangSelected(eleExtnPreferredLanguage
											.getAttribute("ExtnPreferredLanguage"));
						pnlRoot.getShell().close();
					}
				});

		// btnClose
		btnClose = new Button(compositeForButtons, SWT.INHERIT_NONE);
		btnClose.setText(YRCPlatformUI.getFormattedString("CLOSE_BTN_LABEL", ""));
		btnClose
				.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
					public void widgetSelected(
							org.eclipse.swt.events.SelectionEvent e) {
						pnlRoot.getShell().close();
					}
				});
	}

	@Override
	public String getFormId() {
		return HBCPreferedLanguagesPage.FORM_ID;
	}

	public Object getBehavior() {
		return this.myBehavior;
	}

	@Override
	public String getHelpId() {
		return null;
	}

	@Override
	public IYRCPanelHolder getPanelHolder() {
		return null;
	}

	@Override
	public Composite getRootPanel() {
		return null;
	}

}
