package com.pca.rcp.hbcp2.orderentry.screens;

import java.text.NumberFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.pca.rcp.hbcp2.util.HBCPCAUtil;
import com.yantra.yfc.rcp.IYRCComposite;
import com.yantra.yfc.rcp.IYRCPanelHolder;
import com.yantra.yfc.rcp.YRCConstants;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCTextBindingData;
import com.yantra.yfc.rcp.YRCXmlUtils;

public class HBCRoyaltyCardPage extends Composite implements
		IYRCComposite {

	private static final String FORM_ID = "com.pca.rcp.hbcp2.orderentry.screens.HBCRoyaltyCardPage";
	private Composite pnlRoot = null;

	
	
	private Composite pnlSuggestions=null;
	private Label lblSuggestions=null;
	
	private Composite pnlRoyaltyNumber=null;
	private Label lblRoyaltyNumber=null;
	private Text txtRoyaltyNumber = null;
	
	private double preTaxTotal = 0;

	private Composite compositeForButtons = null;

	private Button btnConfirm = null;
	private Button btnClose = null;
	
	private HBCRoyaltyCardPageExtnBehavior myBehavior = null;
	
	public HBCRoyaltyCardPage(Composite parent, int style,  double preTaxTotal) {
		super(parent, style);
		this.preTaxTotal = preTaxTotal;
		initialize();
		myBehavior = new HBCRoyaltyCardPageExtnBehavior(this, FORM_ID);
		
	}

	
	/**
	 * This method initializes this
	 * 
	 */
	private void initialize() {
		this.setSize(new Point(353, 161));
		GridLayout thislayout = new GridLayout();
		thislayout.horizontalSpacing = 0;
		thislayout.marginWidth = 0;
		thislayout.verticalSpacing = 0;
		thislayout.marginHeight = 0;
		this.setLayout(thislayout);
		createComposite();
	}

	/**
	 * This method initializes composite
	 * 
	 */
	private void createComposite() {
		createRootPanel();
		createSuggestionsPnlAndLbl("REWARDS_ENTER_INFO_MESSAGE");
		createRoyaltyNumberPnl();
		createCompositeForButtons();
	}
	

	private void createRootPanel() {
		// GridLayout for main pnlRoot
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginHeight = 5;
		gridLayout.marginWidth = 5;
		gridLayout.verticalSpacing = 0;
		gridLayout.horizontalSpacing = 0;

		// GridData for pnlRoot
		GridData gridDataForpnlRoot = new GridData();
		gridDataForpnlRoot.grabExcessHorizontalSpace = true;
		gridDataForpnlRoot.horizontalAlignment = GridData.FILL;
		gridDataForpnlRoot.verticalAlignment = GridData.FILL;
		gridDataForpnlRoot.grabExcessVerticalSpace = true;

		// Composite for pnlRoot
		pnlRoot = new Composite(this, SWT.INHERIT_NONE);
		pnlRoot.setLayoutData(gridDataForpnlRoot);
		pnlRoot.setLayout(gridLayout);
		pnlRoot.setData(YRCConstants.YRC_CONTROL_NAME, "pnlRoot");
		pnlRoot.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE, "TaskComposite");
	}

	private void createSuggestionsPnlAndLbl(String strSuggestions) {
		// GridData for pnlSuggestions
		GridData gridDataForSuggestions = new GridData();
		gridDataForSuggestions.grabExcessHorizontalSpace = true;
		gridDataForSuggestions.horizontalAlignment = GridData.FILL;
		gridDataForSuggestions.verticalAlignment = GridData.FILL;

		// GridLayout for pnlSuggestions
		GridLayout glForPnlSuggestions = new GridLayout(1, false);
		glForPnlSuggestions.marginHeight = 5;
		glForPnlSuggestions.marginWidth = 10;
		glForPnlSuggestions.verticalSpacing = 1;
		glForPnlSuggestions.horizontalSpacing = 1;
		
		// Composition for pnlSuggestions
		pnlSuggestions = new Composite(pnlRoot, SWT.NONE);
		pnlSuggestions.setLayoutData(gridDataForSuggestions);
		pnlSuggestions.setLayout(glForPnlSuggestions);
		pnlSuggestions.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE,
				"PanelHeader");	
		
		// GridData for lblSuggestions
		GridData gdForlblSuggestions = new GridData();
		gdForlblSuggestions.horizontalAlignment = GridData.FILL;
		gdForlblSuggestions.grabExcessHorizontalSpace = true;
		gdForlblSuggestions.grabExcessVerticalSpace = true;
		gdForlblSuggestions.verticalAlignment = GridData.FILL;

		// lblSuggestions
		lblSuggestions = new Label(pnlSuggestions, SWT.INHERIT_NONE);
		lblSuggestions.setText(strSuggestions);
		lblSuggestions.setLayoutData(gdForlblSuggestions);
		lblSuggestions.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE,
				"PanelHeader");
	}
	
	private void createRoyaltyNumberPnl() {
		// GridData for pnlSuggestions
		GridData gridDataForSuggestions = new GridData();
		gridDataForSuggestions.grabExcessHorizontalSpace = true;
		gridDataForSuggestions.horizontalAlignment = GridData.FILL;
		gridDataForSuggestions.verticalAlignment = GridData.FILL;

		// GridLayout for pnlSuggestions
		GridLayout glForPnlSuggestions = new GridLayout(2, false);
		glForPnlSuggestions.marginHeight = 5;
		glForPnlSuggestions.marginWidth = 10;
		glForPnlSuggestions.verticalSpacing = 1;
		glForPnlSuggestions.horizontalSpacing = 1;
		
		// Composition for pnlSuggestions
		pnlRoyaltyNumber = new Composite(pnlRoot, SWT.NONE);
		pnlRoyaltyNumber.setLayoutData(gridDataForSuggestions);
		pnlRoyaltyNumber.setLayout(glForPnlSuggestions);
		pnlRoyaltyNumber.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE,
				"Composite");	
		
		// GridData for lblSuggestions
		GridData gdForlblSuggestions = new GridData();
		gdForlblSuggestions.horizontalAlignment = GridData.FILL;
		gdForlblSuggestions.grabExcessHorizontalSpace = true;
		gdForlblSuggestions.grabExcessVerticalSpace = true;
		gdForlblSuggestions.verticalAlignment = GridData.FILL;

		// lblSuggestions
		lblRoyaltyNumber = new Label(pnlRoyaltyNumber, SWT.INHERIT_NONE);
		lblRoyaltyNumber.setText("REWARDS_ENTER_LABEL_MESSAGE");
		lblRoyaltyNumber.setLayoutData(gdForlblSuggestions);
		//lblRoyaltyNumber.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE,"PanelHeader");
		
		txtRoyaltyNumber = new Text(pnlRoyaltyNumber, SWT.BORDER);
		txtRoyaltyNumber.setLayoutData(gdForlblSuggestions);
		//txtRoyaltyNumber.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE,"MandatoryTheme");
		YRCTextBindingData txtRoyaltyNumberBindingData = new YRCTextBindingData();
		txtRoyaltyNumberBindingData.setMandatory(true);
		txtRoyaltyNumber.setData(YRCConstants.YRC_TEXT_BINDING_DEFINATION, txtRoyaltyNumberBindingData);
	}
	/**
	 * This method initializes compositeForButtons
	 * 
	 */
	private void createCompositeForButtons() {

		// GridData for gdForbtnConfirm
		GridData gdForBtnConfirm = new GridData();
		gdForBtnConfirm.horizontalAlignment = GridData.END;
		gdForBtnConfirm.grabExcessHorizontalSpace = true;
		gdForBtnConfirm.verticalAlignment = GridData.CENTER;

		// GridLayout for glForCompositeButtons
		GridLayout glForCompositeButtons = new GridLayout();
		glForCompositeButtons.numColumns = 2;

		// GridData for gdForCompositeButtons
		GridData gdForCompositeButtons = new GridData();
		gdForCompositeButtons.verticalSpan = 3;
		gdForCompositeButtons.verticalAlignment = GridData.FILL;
		gdForCompositeButtons.grabExcessHorizontalSpace = true;
		gdForCompositeButtons.grabExcessVerticalSpace = false;
		gdForCompositeButtons.horizontalAlignment = GridData.FILL;

		// Composite for compositeForButtons
		compositeForButtons = new Composite(getRootPanel(), SWT.INHERIT_NONE);
		compositeForButtons.setLayoutData(gdForCompositeButtons);
		compositeForButtons.setLayout(glForCompositeButtons);
		compositeForButtons.setFocus();
		compositeForButtons.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE,
				"TaskComposite");

		// btnConfirm
		btnConfirm = new Button(compositeForButtons, SWT.INHERIT_NONE);
		btnConfirm.setText(YRCPlatformUI.getFormattedString("CONFIRM_BTN_LABEL", ""));
		btnConfirm.setLayoutData(gdForBtnConfirm);
		
		btnConfirm
		.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			public void widgetSelected(
					org.eclipse.swt.events.SelectionEvent e) {
				try{
				if(txtRoyaltyNumber.getText() == null || txtRoyaltyNumber.getText().length() <=0)
					YRCPlatformUI.showError("Error", "REWARDS_ENTER_INFO_MESSAGE");
				else{
					btnConfirm.setEnabled(false);
					Element eleGetRewardsInfoResponse = myBehavior.callQASWebService(txtRoyaltyNumber.getText());
					btnConfirm.setEnabled(true);
					if(eleGetRewardsInfoResponse == null){
						YRCPlatformUI.showError("Error", "REWARDS_PROCESSING_FAILURE_MESSAGE");
						pnlRoot.getShell().close();
					}else{
					 Element eleGetRewardsInfoResult = YRCXmlUtils.getChildElement(eleGetRewardsInfoResponse, "GetRewardsInfoResult");
					 if(eleGetRewardsInfoResult != null){
						String responseXML =  eleGetRewardsInfoResult.getTextContent();
						Document docResponse  =null;
							docResponse = YRCXmlUtils.createFromString(responseXML);
							if(docResponse!=null){
								Element eleResponse = docResponse.getDocumentElement();
								String strCode = eleResponse.getAttribute("Code");
								if(strCode != null && "0".equals(strCode)){
									//lblSuggestions.setText("Valid Royalty Number.");
									String rewards = Double.toString(preTaxTotal);
									NumberFormat nf = NumberFormat.getIntegerInstance();
									Number price = nf.parse(rewards);
									rewards=String.valueOf(price);
									YRCPlatformUI.showInformation("Rewards", YRCPlatformUI.getFormattedString("REWARDS_SUCCESS_MESSAGE",rewards));
									myBehavior.setStrRoyaltyNumber(txtRoyaltyNumber.getText());
									pnlRoot.getShell().close();
								}else{
									YRCPlatformUI.showError("Error", "REWARDS_INVALID_LABEL_MESSAGE");
									txtRoyaltyNumber.setText("");
								}
							}else{
								YRCPlatformUI.showError("Error", "REWARDS_PROCESSING_FAILURE_MESSAGE");
								pnlRoot.getShell().close();
							}
					 }
					 else{
						 YRCPlatformUI.showError("Error", "REWARDS_PROCESSING_FAILURE_MESSAGE");
						 pnlRoot.getShell().close();
					 }
					}
				}
				//pnlRoot.getShell().close();
			}catch(Exception e1){
				YRCPlatformUI.showError("Error", "REWARDS_PROCESSING_FAILURE_MESSAGE");
				pnlRoot.getShell().close();
			}
			}
		});

		// btnClose
		btnClose = new Button(compositeForButtons, SWT.INHERIT_NONE);
		btnClose.setText(YRCPlatformUI.getFormattedString("CLOSE_BTN_LABEL", ""));
		btnClose
				.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
					public void widgetSelected(
							org.eclipse.swt.events.SelectionEvent e) {
						pnlRoot.getShell().close();
					}
				});
	}
	public String getFormId() {
		return HBCRoyaltyCardPage.FORM_ID;
	}

	public Composite getRootPanel() {
		return this.pnlRoot;
	}

	public Object getBehavior() {
		return this.myBehavior;
	}

	@Override
	public String getHelpId() {
		return null;
	}

	@Override
	public IYRCPanelHolder getPanelHolder() {
		return null;
	}
	
}
