package com.pca.rcp.hbcp2.orderentry.screens;

import org.eclipse.swt.widgets.Composite;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCBehavior;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCXmlUtils;
import com.yantra.yfc.rcp.internal.YRCApiCaller;

public class HBCRoyaltyCardPageExtnBehavior extends YRCBehavior {
	
	String strRoyaltyNumber = null;
	
	public HBCRoyaltyCardPageExtnBehavior(Composite arg0) {
		super(arg0);
	}

	public HBCRoyaltyCardPageExtnBehavior(Composite arg0, String arg1) {
		super(arg0, arg1);

	}

	public Element callQASWebService(String strRoyaltyNumber) {
		 Document docRewardsInfoRequest = createRewardsInfoRequest(strRoyaltyNumber);
	     YRCApiContext yrcApiContext = new YRCApiContext();
	     yrcApiContext.setApiName("HBCLoyaltyPointsWebService");
	     yrcApiContext.setInputXml(docRewardsInfoRequest);
	     yrcApiContext.setFormId(this.getFormId());
	     setModel("RewardsInfo_output", null);
	     this.setStrRoyaltyNumber(null);
	     new YRCApiCaller(yrcApiContext, true).invokeApi();
	     handleApiCompletion(yrcApiContext);
	     return getModel("RewardsInfo_output");
	}

	private Document createRewardsInfoRequest(String strRoyaltyNumber) {
		//<RewardsInfo RewardsNumber="580476246" CCType="0" Or-derSubtotal="0" EstimatedPoints="0" TotalPointsToDate="0" />
		Document docRewardsInfoInput = YRCXmlUtils.createDocument("GetRewardsInfo");
		Element deleRewardsInfoInput  = docRewardsInfoInput.getDocumentElement();
		Element elesRewardsInfoIn = YRCXmlUtils.createChild(deleRewardsInfoInput, "sRewardsInfoIn");
		String RewardsInfo = "<RewardsInfo RewardsNumber=\""+strRoyaltyNumber+"\" CCType=\"0\" OrderSubtotal=\"0\" EstimatedPoints=\"0\" TotalPointsToDate=\"0\" xmlns=\"eretailwebservices.hbc.com\" />";
		elesRewardsInfoIn.setTextContent(RewardsInfo);
		return docRewardsInfoInput;
	}
	@Override
	public void handleApiCompletion(YRCApiContext ctx) {
		if (YRCPlatformUI.equals(ctx.getApiName(),
		"HBCLoyaltyPointsWebService")) {
			if(ctx.getInvokeAPIStatus() == -1){
				setModel("RewardsInfo_output", null);
			}else if(ctx.getInvokeAPIStatus() == 1){
			Document docGetRewardsInfoResponse = ctx.getOutputXml();
			setModel("RewardsInfo_output", docGetRewardsInfoResponse.getDocumentElement());
			}
		}
	}

	public String getStrRoyaltyNumber() {
		return strRoyaltyNumber;
	}

	public void setStrRoyaltyNumber(String strRoyaltyNumber) {
		this.strRoyaltyNumber = strRoyaltyNumber;
	}
	
}
