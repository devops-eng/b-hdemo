package com.pca.rcp.hbcp2.orderentry.screens;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.xpath.XPathConstants;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.pca.rcp.hbcp2.util.HBCPCAConstants;
import com.pca.rcp.hbcp2.util.HBCPCAUtil;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCExtentionBehavior;
import com.yantra.yfc.rcp.YRCFormatUtils;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCSharedTaskOutput;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCXPathUtils;
import com.yantra.yfc.rcp.YRCXmlUtils;
import com.yantra.yfc.rcp.internal.YRCApiCaller;

public class HBCStyleSizeColorPopupExtnBehavior extends YRCExtentionBehavior {

	Document docGetPricelistLineList = null;
	Element eleGetPricelistLineList = null;

	@Override
	public void postCommand(YRCApiContext yrcapicontext) {



		String strOrganizationCode = "";
		String strItemId = "";
		Date currentDate = new Date();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String sCurrentDate = formatter.format(currentDate);
		String sEndDateActiveQryType = "GT";
		String sStartDateActiveQryType = "LT";

		if (YRCPlatformUI.equals("getItemListForOrderingForPrice",
				yrcapicontext.getApiName())) {
			
			
			
			Document docGetItemListForOrderWithOutPrice = yrcapicontext
					.getOutputXml();
			if(docGetItemListForOrderWithOutPrice!=null){
			Element eleGetItemListForOrderWithOutPrice = docGetItemListForOrderWithOutPrice
					.getDocumentElement();
			String strCurrency = docGetItemListForOrderWithOutPrice.getDocumentElement().getAttribute("Currency");
			ArrayList<Element> nItems = YRCXmlUtils.getChildren(
					eleGetItemListForOrderWithOutPrice, "Item");
			// NodeList nItems =
			// eleGetItemListForOrderWithOutPrice.getElementsByTagName("Item");

			Element docUser = getModel("UserNameSpace");
			if (nItems.size() == 1) {
				Element eItem = (Element) nItems.get(0);
				// Element eItem = (Element) nItems.item(0);

				if (HBCPCAUtil.isStoreApplication()) {
					strOrganizationCode = YRCXmlUtils.getAttribute(docUser,
							"BusinessKey");
				} else {
					strItemId = YRCXmlUtils.getAttribute(eItem, "ItemID");
					Element eleSelectedItem  = getModel("SelectedItem");
					if(eleSelectedItem!=null)
						strOrganizationCode = eleSelectedItem.getAttribute("OrganizationCode");
				}

				Document inputxml = YRCXmlUtils.createDocument("PricelistLine");
				Element elePricelistLine = inputxml.getDocumentElement();
				YRCXmlUtils.setAttribute(elePricelistLine, "ItemID", strItemId);
				YRCXmlUtils.setAttribute(elePricelistLine,
						"SellerOrganizationCode", strOrganizationCode);
				YRCXmlUtils.setAttribute(elePricelistLine,
						"EndDateActiveQryType", sEndDateActiveQryType);
				YRCXmlUtils.setAttribute(elePricelistLine,
						"StartDateActiveQryType", sStartDateActiveQryType);
				YRCXmlUtils.setAttribute(elePricelistLine, "EndDateActive",
						sCurrentDate);
				YRCXmlUtils.setAttribute(elePricelistLine, "StartDateActive",
						sCurrentDate);
				YRCXmlUtils.setAttribute(elePricelistLine, "PricingStatus",
						"ACTIVE");

				YRCApiContext yrcApiContext = new YRCApiContext();
				yrcApiContext.setApiName("getPricelistLineList");
				yrcApiContext.setInputXml(inputxml);
				yrcApiContext.setFormId(this.getFormId());

				new YRCApiCaller(yrcApiContext, true).invokeApi();
				handleApiCompletion(yrcApiContext);

				if (eleGetPricelistLineList != null) {
					ArrayList nPricelistLine = YRCXmlUtils.getChildren(
							eleGetPricelistLineList, "PricelistLine");
					// call createLeastPriceListLine method...
					if (nPricelistLine.size() > 0) {
						Element eLeastPriceListLine = createLeastPriceListLine(nPricelistLine);
						String strLeastUnitPrice = eLeastPriceListLine
								.getAttribute("ListPrice");
						String sUnitPrice = YRCPlatformUI
						.getFormattedCurrency((YRCFormatUtils
								.getFormattedValue("UnitPrice", YRCXmlUtils
										.getAttribute(eLeastPriceListLine,
												"UnitPrice"))), strCurrency);

						Element eleComputedPrice = YRCXmlUtils.getChildElement(
								eItem, "ComputedPrice");
						YRCXmlUtils.setAttribute(eleComputedPrice, "UnitPrice",
								strLeastUnitPrice);
						
						Element eExtn = YRCXmlUtils.getChildElement(
								eLeastPriceListLine, "Extn");
						String sExtnMarkDownFlag = YRCXmlUtils.getAttribute(eExtn,
								"ExtnMarkDownFlag");
						String sExtnMarkDownFlagDesc = "";
						if (sExtnMarkDownFlag == null || YRCPlatformUI.equals(sExtnMarkDownFlag, "R") || YRCPlatformUI.equals(sExtnMarkDownFlag, "")) {
							sExtnMarkDownFlagDesc = YRCPlatformUI.getFormattedString("Regular","");
							
						}  else if (YRCPlatformUI.equals(sExtnMarkDownFlag, "S")) {
							sExtnMarkDownFlagDesc = YRCPlatformUI.getFormattedString("Sale","");
						} else if (YRCPlatformUI.equals(sExtnMarkDownFlag, "L")) {
							sExtnMarkDownFlagDesc = YRCPlatformUI.getFormattedString("Clearance","");
						}
						
						YRCXmlUtils.setAttribute(eleComputedPrice, "ListPrice",
								YRCXmlUtils
								.getAttribute(eLeastPriceListLine,
										"UnitPrice"));
						
						Text extn_txtPromoPrice = (Text)getControl(this.getOwnerForm(),"extn_txtPromoPrice");
						if(extn_txtPromoPrice != null){
							extn_txtPromoPrice.setText(sUnitPrice+" "+"("+sExtnMarkDownFlagDesc+")");
						}
					}
				}
			}
		}
		}else if(YRCPlatformUI.equals("getItemListForOrdering",
				yrcapicontext.getApiName())){
			Document docgetCompleteItemList = yrcapicontext.getOutputXml();
				if(docgetCompleteItemList!=null){
						Element elegetCompleteItemList = docgetCompleteItemList.getDocumentElement();
						if(elegetCompleteItemList!=null){
							NodeList nItem = elegetCompleteItemList.getElementsByTagName("Item");
							for(int i=0;i<nItem.getLength();i++){
								Element eleItem = (Element)nItem.item(i);
								if(eleItem!=null){
									Element eleChildItemList  =YRCXmlUtils.getChildElement(eleItem, "ChildItemList");
									if(eleChildItemList!=null){
										NodeList nChildItem = eleChildItemList.getElementsByTagName("ChildItem");
										for(int p=0;p<nChildItem.getLength();p++){
											Element eleChildItem = (Element)nChildItem.item(p);
											Element ePrimaryInformation=YRCXmlUtils.getChildElement(eleChildItem, "PrimaryInformation");
											if(ePrimaryInformation!=null){
												String status=ePrimaryInformation.getAttribute("Status");
												if(status!=null && status.equals(HBCPCAConstants.ITEM_HELD_STATUS)){
													eleChildItemList.removeChild(eleChildItem);
													--p;
													continue;
												}
											}
											Element eleItemAttribute = (Element)YRCXPathUtils.evaluate(eleChildItem, "ItemAttributeGroupTypeList/ItemAttributeGroupType/ItemAttributeGroupList/ItemAttributeGroup/ItemAttributeList/ItemAttribute[@ItemAttributeName='Size']", XPathConstants.NODE);
											
											if(eleItemAttribute!=null){
												Element eleAssignedValueList = YRCXmlUtils.getChildElement(eleItemAttribute, "AssignedValueList");
												if(eleAssignedValueList !=null){
												Element eleAssignedValue = YRCXmlUtils.getChildElement(eleAssignedValueList, "AssignedValue");
												if(eleAssignedValue!=null){
													String sAttributeSequenceNo = eleAssignedValue.getAttribute("SequenceNo");
													YRCXmlUtils.setAttribute(eleItemAttribute, "AttributeValueSeqNo", sAttributeSequenceNo);
												}
												}
											}
										}
									}
								}
								
							}
							
						}

				}
		}
		super.postCommand(yrcapicontext);
	
	}
	private Element createLeastPriceListLine(ArrayList npricelistLine) {

		Element ePricelistLineLeast = (Element) npricelistLine.get(0);
		Double dunitPrice = Double.parseDouble(YRCXmlUtils.getAttribute(
				ePricelistLineLeast, "UnitPrice"));
		Double dtemp = 0.0;

		for (int i = 0; i < npricelistLine.size(); i++) {
			Element ePricelistLine = (Element) npricelistLine.get(i);

			dtemp = Double.parseDouble(YRCXmlUtils.getAttribute(ePricelistLine,
					"UnitPrice"));
			if (dtemp <= dunitPrice) {
				dunitPrice = dtemp;
				ePricelistLineLeast = (Element) npricelistLine.get(i);
			}

		}

		return ePricelistLineLeast;
	}
	public void handleApiCompletion(YRCApiContext ctx) {
		// TODO Auto-generated method stub
		if (YRCPlatformUI.equals(ctx.getApiName(), "getPricelistLineList")) {
			docGetPricelistLineList = ctx.getOutputXml();
			eleGetPricelistLineList = docGetPricelistLineList
					.getDocumentElement();

		}
		super.handleApiCompletion(ctx);
	}
	
	@Override
	public void postSetModel(String arg0) {
		
		super.postSetModel(arg0);
	}

	@Override
	public boolean preCommand(YRCApiContext arg0) {
		// TODO Auto-generated method stub
		return super.preCommand(arg0);
	}
	public static Control getControl(Composite composite, String name) {
		Control[] ctrl = composite.getChildren();
		for(Control object:ctrl){
		if (object instanceof Composite) {
		Composite cmp = (Composite) object;
		Control c = getControl(cmp, name);
		if(!YRCPlatformUI.isVoid(c))
		return c;
		}
		else if(name.equals(object.getData("name")))
		return object;
		}
		return null;
		}
	
//	 Start - Zero Dollar CR Changes - Defect # 1856
	public YRCValidationResponse validateButtonClick(String fieldName) {
		YRCValidationResponse response = new YRCValidationResponse();	
		if(fieldName.equals("btnConfirm")){
			Element eleItemPrice=this.getModel("SelectedItemPrice");
		if(eleItemPrice!=null){
			Element eleComputedPrice=YRCXmlUtils.getChildElement(eleItemPrice, "ComputedPrice");
			if(eleComputedPrice!=null){
				String strUnitPrice=eleComputedPrice.getAttribute("UnitPrice");
			if(!YRCPlatformUI.isVoid(strUnitPrice)&&(strUnitPrice.equals("0.00")|| strUnitPrice.equals("0")||strUnitPrice.equals("$0.00")||strUnitPrice.equals("0.0"))) 
			{
				YRCSharedTaskOutput out = HBCPCAUtil.showOptionDialogBoxWithYesNo("ITEM_ERROR",
				"ZERO_DOLLAR_PRICE_ITEM");				
				Element eSelectedOption = out.getOutput();
				String sSelectedOptionKey = "";
				sSelectedOptionKey = YRCXmlUtils.getAttribute(
				eSelectedOption, "SelectedOptionKey");				
				if (sSelectedOptionKey != null
						&& sSelectedOptionKey.equals("OPTION_NO")) {
					response = new YRCValidationResponse(
							YRCValidationResponse.YRC_VALIDATION_ERROR, "");
				}
				else{
					response = new YRCValidationResponse(
							YRCValidationResponse.YRC_VALIDATION_OK, "");
					
				}
			}
			}
		}
		}
		return response;
		}
//	 End - Zero Dollar CR Changes - Defect # 1856


}
