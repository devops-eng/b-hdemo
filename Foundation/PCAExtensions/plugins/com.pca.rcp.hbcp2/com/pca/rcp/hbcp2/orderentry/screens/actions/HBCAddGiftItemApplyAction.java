package com.pca.rcp.hbcp2.orderentry.screens.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;


import com.pca.rcp.hbcp2.orderentry.screens.HBCAddGiftItem;
import com.pca.rcp.hbcp2.orderentry.screens.HBCAddGiftItemBehaviour;
import com.yantra.yfc.rcp.YRCAction;
import com.yantra.yfc.rcp.YRCDesktopUI;

public class HBCAddGiftItemApplyAction extends YRCAction implements IWorkbenchWindowActionDelegate{

	

	@Override
	public void execute(IAction arg0) {
		// TODO Auto-generated method stub
		Composite currentPage = YRCDesktopUI.getCurrentPage();
		boolean selected = false;
		if (currentPage instanceof HBCAddGiftItem) {
			HBCAddGiftItem current = ((HBCAddGiftItem) currentPage);
			HBCAddGiftItemBehaviour hbcAddGiftItemBehaviour = (HBCAddGiftItemBehaviour)current.getBehavior();  
			selected = hbcAddGiftItemBehaviour.applySelected();
			if(selected){
				current.getShell().close();
				
			}
			
			
        }
	}

}
