package com.pca.rcp.hbcp2.payment;

import java.util.ArrayList;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.pca.rcp.hbcp2.customerentry.HBCAddressCaptureWizardExtnBehavior;
import com.pca.rcp.hbcp2.util.HBCPCAUtil;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCExtentionBehavior;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCXmlUtils;
import com.yantra.yfc.rcp.internal.YRCApiCaller;


public class HBCCreditCardDetailsExtnBehavior extends YRCExtentionBehavior {
	
	Document docCreditCardTypes = null;
	Element eleCreditCardTypes = null;
	HBCCreditCardDetailsExtnBehavior cardDetailsExtnBehavior ;
	
	
	
	@Override
	public boolean preCommand(YRCApiContext arg0) {
		// TODO Auto-generated method stub
		return super.preCommand(arg0);
	}
	@Override
	public void postCommand(YRCApiContext arg0) {
		// TODO Auto-generated method stub
		super.postCommand(arg0);
	}
	public void postSetModel(String arg0) {
		
		if("input".equals(arg0)){
			Button radEnterNewAddress = (Button)HBCPCAUtil.getControl(this.getOwnerForm(), "", "radEnterNewAddress", false);
			Button radUseBillToAddress = (Button)HBCPCAUtil.getControl(this.getOwnerForm(), "", "radUseBillToAddress", false);
			if(this.getParentExtnBehavior()!=null){
				HBCPaymentTypeEntryPanelExtnBehavior paymentTypeEntryPanelExtnBehavior = (HBCPaymentTypeEntryPanelExtnBehavior) this.getParentExtnBehavior();
				if(paymentTypeEntryPanelExtnBehavior.getParentExtnBehavior()!=null){
				HBCPaymentMethodPopupExtnBehavior paymentMethodPopupExtnBehavior = (HBCPaymentMethodPopupExtnBehavior) paymentTypeEntryPanelExtnBehavior.getParentExtnBehavior();
				Button extn_checkaddress = (Button)HBCPCAUtil.getControl(paymentMethodPopupExtnBehavior.getOwnerForm(), "", "extn_checkaddress", false);
				if(extn_checkaddress!=null){
					if(radEnterNewAddress.getSelection()){
            		extn_checkaddress.setVisible(true);
            		extn_checkaddress.setEnabled(true);
					}
				}
				}
			}
			cardDetailsExtnBehavior = (HBCCreditCardDetailsExtnBehavior) this;
			if(radEnterNewAddress!=null){
				radEnterNewAddress.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter(){
					@Override
					public void widgetDefaultSelected(SelectionEvent e) {}
					@Override
					public void widgetSelected(SelectionEvent e) {
						Boolean isSelected = ((Button) e.widget).getSelection();
                        if(isSelected){
						HBCPaymentTypeEntryPanelExtnBehavior paymentTypeEntryPanelExtnBehavior = (HBCPaymentTypeEntryPanelExtnBehavior) cardDetailsExtnBehavior.getParentExtnBehavior();
						HBCPaymentMethodPopupExtnBehavior paymentMethodPopupExtnBehavior = (HBCPaymentMethodPopupExtnBehavior) paymentTypeEntryPanelExtnBehavior.getParentExtnBehavior();
						Button extn_checkaddress = (Button)HBCPCAUtil.getControl(paymentMethodPopupExtnBehavior.getOwnerForm(), "", "extn_checkaddress", false);
						ArrayList arrayList = cardDetailsExtnBehavior.getChildExtnBehaviors();
						for (int i = 0; i < arrayList.size(); i++) {
							if(arrayList.get(i) instanceof HBCAddressCaptureWizardExtnBehavior){
								HBCAddressCaptureWizardExtnBehavior addressCaptureWizardExtnBehavior = (HBCAddressCaptureWizardExtnBehavior)arrayList.get(i);
								Element elePersonInfo = addressCaptureWizardExtnBehavior.getFromModel("PersonInfo");
								String tempStrLineAdd1 = null;
								String tempStrLineAdd2 = null;
								String tempStrCity = null;
								String tempStrState = null;
								String tempStrPostalCode = null;
								String tempStrCmbCountry = null;

								tempStrLineAdd1 = elePersonInfo.getAttribute("AddressLine1");
								tempStrLineAdd2 = elePersonInfo.getAttribute("AddressLine2");
								tempStrCity = elePersonInfo.getAttribute("City");
								tempStrState = elePersonInfo.getAttribute("State");
								tempStrPostalCode = elePersonInfo.getAttribute("ZipCode");
								tempStrCmbCountry = elePersonInfo.getAttribute("Country");
								try {
									addressCaptureWizardExtnBehavior.strPrevAddress = addressCaptureWizardExtnBehavior.formatAddress(tempStrLineAdd1, tempStrLineAdd2, tempStrCity, tempStrState, tempStrPostalCode, tempStrCmbCountry);
								} catch (Exception e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							}
						}
                    	if(extn_checkaddress!=null){
                    		extn_checkaddress.setVisible(true);
                    		extn_checkaddress.setEnabled(true);

                    		/*extn_checkaddress.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter(){
            					@Override
            					public void widgetDefaultSelected(SelectionEvent e) {}
            					@Override
            					public void widgetSelected(SelectionEvent e) {
            						ArrayList  arrayList = cardDetailsExtnBehavior.getChildExtnBehaviors();
            						for (int i = 0; i < arrayList.size(); i++) {
										if(arrayList.get(i)!=null && arrayList.get(i) instanceof HBCAddressCaptureWizardExtnBehavior){
											HBCAddressCaptureWizardExtnBehavior addressCaptureWizardExtnBehavior = (HBCAddressCaptureWizardExtnBehavior)arrayList.get(i);
											YRCValidationResponse response = new YRCValidationResponse();
											response = addressCaptureWizardExtnBehavior.validateAddress(response, false);
											
										}
									}
            					}
            				});*/
            			
                    	}
                    
                         }
					}
				});
			}
			if(radUseBillToAddress!=null){
				radUseBillToAddress.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter(){
					@Override
					public void widgetDefaultSelected(SelectionEvent e) {}
					@Override
					public void widgetSelected(SelectionEvent e) {
						Boolean isSelected = ((Button) e.widget).getSelection();
                        if(isSelected){
						HBCPaymentTypeEntryPanelExtnBehavior paymentTypeEntryPanelExtnBehavior = (HBCPaymentTypeEntryPanelExtnBehavior) cardDetailsExtnBehavior.getParentExtnBehavior();
						HBCPaymentMethodPopupExtnBehavior paymentMethodPopupExtnBehavior = (HBCPaymentMethodPopupExtnBehavior) paymentTypeEntryPanelExtnBehavior.getParentExtnBehavior();
						Button extn_checkaddress = (Button)HBCPCAUtil.getControl(paymentMethodPopupExtnBehavior.getOwnerForm(), "", "extn_checkaddress", false);
                    	if(extn_checkaddress!=null){
                    		extn_checkaddress.setEnabled(false);
                    	}
                         }
					}
				});
			}
			
		}
		else if("getCreditCardTypes_output".equals(arg0)){
			Composite pnlPaymentMethodBody = (Composite) getChildComposit(this.getOwnerForm().getParent().getParent().getParent().getParent(), "pnlPaymentMethodBody");
			if(pnlPaymentMethodBody!=null){
				Combo cmbPaymentTypes =  (Combo)getChildCombo(pnlPaymentMethodBody, "cmbPaymentTypes");
				String strPaymentType = (String)cmbPaymentTypes.getData("YRCComboOptionIndex"+cmbPaymentTypes.getSelectionIndex());
				String strOrganization = "LT";
				Element eleCreditCardTypes_output=getModel(arg0);
				ArrayList<Element> commonCodelist = YRCXmlUtils.getChildren(eleCreditCardTypes_output, "CommonCode");
				if(commonCodelist!=null && commonCodelist.size()>0){
					Element eleCommonCode=  commonCodelist.get(0);
					strOrganization = eleCommonCode.getAttribute("OrganizationCode");
				}
				Document commonCodeInput = YRCXmlUtils.createDocument("CommonCode");
				commonCodeInput.getDocumentElement().setAttribute("CallingOrganizationCode", strOrganization);
				commonCodeInput.getDocumentElement().setAttribute("CodeType", "YCD_CREDIT_CARD_TYPE");
				
				YRCApiContext yrcApiContext = new YRCApiContext();
				yrcApiContext.setApiName("getCommonCodeList");
				yrcApiContext.setInputXml(commonCodeInput);
				yrcApiContext.setFormId(this.getFormId());

				new YRCApiCaller(yrcApiContext, true).invokeApi();
				handleApiCompletion(yrcApiContext);
				boolean interCardsthere = false;
				if("CREDIT_CARD_LT".equals(strPaymentType) ||"CREDIT_CARD_HBC".equals(strPaymentType)){
					//cmbPaymentTypes.remove("L&T PLCC");
					ArrayList list=YRCXmlUtils.getChildren(eleCreditCardTypes_output, "CommonCode");
					for(int k=0;k<list.size();k++){
						Element eleCommonCode = (Element)list.get(k);
						if(eleCommonCode.getAttribute("CodeValue").equals("LT")|| eleCommonCode.getAttribute("CodeValue").contains("HBC")){
							eleCreditCardTypes_output.removeChild(eleCommonCode);
						}else{
							interCardsthere = true;
						}
						
					}
					if(!interCardsthere){

						ArrayList tempList=YRCXmlUtils.getChildren(eleCreditCardTypes, "CommonCode");
						for(int j=0;j<tempList.size();j++){
							Element tempeleCommonCode = (Element)tempList.get(j);
							if(!(tempeleCommonCode.getAttribute("CodeValue").equals("LT")||tempeleCommonCode.getAttribute("CodeValue").contains("HBC"))){
								YRCXmlUtils.importElement(eleCreditCardTypes_output, tempeleCommonCode);
							}
						}
						
					}
				}else{
					boolean pvtCardAlreadyThere = false;
					ArrayList list=YRCXmlUtils.getChildren(eleCreditCardTypes_output, "CommonCode");
					for(int k=0;k<list.size();k++){
						Element eleCommonCode = (Element)list.get(k);
						if(eleCommonCode.getAttribute("CodeValue").equals("LT")|| eleCommonCode.getAttribute("CodeValue").contains("HBC")){
							pvtCardAlreadyThere =true;
						}else{
							eleCreditCardTypes_output.removeChild(eleCommonCode);
						}
					}
					if(!pvtCardAlreadyThere){
					ArrayList tempList=YRCXmlUtils.getChildren(eleCreditCardTypes, "CommonCode");
					for(int j=0;j<tempList.size();j++){
						Element tempeleCommonCode = (Element)tempList.get(j);
						if(tempeleCommonCode.getAttribute("CodeValue").equals("LT")||tempeleCommonCode.getAttribute("CodeValue").contains("HBC")){
							YRCXmlUtils.importElement(eleCreditCardTypes_output, tempeleCommonCode);
						}
								
					}
					}
				}
				repopulateModel(arg0);
			}
		}
		super.postSetModel(arg0);	
		
	}
	@Override
	public void handleApiCompletion(YRCApiContext ctx) {
		// TODO Auto-generated method stub
		if (YRCPlatformUI.equals(ctx.getApiName(), "getCommonCodeList")) {
			docCreditCardTypes = ctx.getOutputXml();
			eleCreditCardTypes = docCreditCardTypes.getDocumentElement();

		}
		super.handleApiCompletion(ctx);
	}
	public static Control getChildCombo(Composite composite, String name) {
		Control[] ctrl = composite.getChildren();
		for(Control object:ctrl){
			if (object instanceof Combo) {	
				if(name.equals(object.getData("name")))
					return object;
			}
			else if(name.equals(object.getData("name")))
			return object;
		}
		return null;
		}
	public static Control getChildComposit(Composite composite, String name) {
		Control[] ctrl = composite.getChildren();
		for (Control object : ctrl) {
			if (object instanceof Composite) {
				Composite cmp = (Composite) object;
				if (name.equals(cmp.getData("name")))
					return object;
				
			} else if (name.equals(object.getData("name")))
				return object;
		}
		return null;
	}
	
}
