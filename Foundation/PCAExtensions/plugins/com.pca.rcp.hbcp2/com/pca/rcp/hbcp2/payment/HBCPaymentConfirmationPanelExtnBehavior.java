package com.pca.rcp.hbcp2.payment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.xpath.XPathConstants;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.pca.rcp.hbcp2.orderentry.HBCOrderEntryWizardExtnBehavior;
import com.pca.rcp.hbcp2.util.HBCPCAConstants;
import com.pca.rcp.hbcp2.util.XMLUtil;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCExtentionBehavior;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCXPathUtils;
import com.yantra.yfc.rcp.YRCXmlUtils;
import com.yantra.yfc.rcp.internal.YRCApiCaller;

public class HBCPaymentConfirmationPanelExtnBehavior extends
		YRCExtentionBehavior  {
	String strTranReturnCode = "";
	Element PaymentMethodList ;
	private Map<String,String> systemAvailableStatus;
	boolean callFromCoupon = false;

	
	@Override
	public void postCommand(YRCApiContext arg0) {
		if(arg0.getApiName() != null && arg0.getApiName().equals("executePaymentTransaction")){
			Document docOutput = arg0.getOutputXml();
			if(docOutput!=null){
				Element eleOutput= docOutput.getDocumentElement();
				Element elePaymentMethod = YRCXmlUtils.getChildElement(eleOutput, "PaymentMethod");
				if(elePaymentMethod!=null){
					
					Element elePaymentDetails = YRCXmlUtils.getChildElement(elePaymentMethod, "PaymentDetails");
					if(elePaymentDetails!=null){
						String strChargeType = elePaymentDetails
						.getAttribute("ChargeType");
				String strAuthReturnCode = elePaymentDetails
						.getAttribute("AuthReturnCode");
				if (strAuthReturnCode != null
						&& strAuthReturnCode
								.equals("APPROVED")
						&& strChargeType != null
						&& strChargeType
								.equals("AUTHORIZATION")) 
					this.setStrTranReturnCode(elePaymentDetails.getAttribute("TranReturnCode"));
					}
				}
			}
		}
		super.postCommand(arg0);
	}
	@Override
	public boolean preCommand(YRCApiContext arg0) {
		// TODO Auto-generated method stub
		return super.preCommand(arg0);
	}
	@Override
	public void handleApiCompletion(YRCApiContext arg0) {
		if (YRCPlatformUI.equals(arg0.getApiName(),"getOrderDetails")) {
			Document docGetOrderDetails = arg0.getOutputXml();

			if (docGetOrderDetails != null) {
				setExtentionModel("ExtnGetOrderDetails", docGetOrderDetails
						.getDocumentElement());
			}
		}
		else if(YRCPlatformUI.equals(arg0.getApiName(),"HBCAMSPromotionSyncWebService")){
			Document docAMSPromotionSyncWebService = arg0.getOutputXml();

			if (docAMSPromotionSyncWebService != null) {
				setExtentionModel("ExtnAMSPromotionSyncWebService", docAMSPromotionSyncWebService
						.getDocumentElement());
			}
		}else if(YRCPlatformUI.equals(arg0.getApiName(),"HBCGetFundsAvailableSyncWebService")){
			Document docGetFundsAvailableSyncWebService = arg0.getOutputXml();

			if (docGetFundsAvailableSyncWebService != null) {
				setExtentionModel("ExtnGetFundsAvailableSyncWebService", docGetFundsAvailableSyncWebService
						.getDocumentElement());
			}
		}
		super.handleApiCompletion(arg0);
	}
	public String getStrTranReturnCode() {
		return strTranReturnCode;
	}
	@Override
	public void postSetModel(String arg0) {
		if("PaymentMethodList".equalsIgnoreCase(arg0)){
			PaymentMethodList = getModel(arg0);
		}
		else if ("Order".equalsIgnoreCase(arg0)) {
			Element eleOrder = getModel("Order");
			Element elePaymentMethodList = getModel("PaymentMethodList");
			Document getOrderDetailsInputDoc = YRCXmlUtils.createDocument("Order");
			String strOrderHeaderKey = eleOrder.getAttribute("OrderHeaderKey");
			Element eleExtnGetOrderDetails = null;
			if (elePaymentMethodList != null || this.callFromCoupon) {
				this.callFromCoupon = false;
				String systemStatusAMS = "Y";
				if (XMLUtil.isVoid(systemAvailableStatus)|| systemAvailableStatus.size() == 0) {
					getSystemAvalilableStatus();
				}
				if (!XMLUtil.isVoid(systemAvailableStatus)&& systemAvailableStatus.size() > 0) {
					systemStatusAMS = (String) systemAvailableStatus
							.get(HBCPCAConstants.SYSTEM_NAME_AMS);
				}
				if (systemStatusAMS.toUpperCase().equals("Y")) {
					Element eleGetOrderDetails = getOrderDetailsInputDoc.getDocumentElement();
					YRCXmlUtils.setAttribute(eleGetOrderDetails,"OrderHeaderKey", strOrderHeaderKey);
					YRCApiContext yrcApiContext = new YRCApiContext();
					yrcApiContext.setApiName("getOrderDetails");
					yrcApiContext.setInputXml(getOrderDetailsInputDoc);
					yrcApiContext.setFormId(this.getFormId());
					new YRCApiCaller(yrcApiContext, true).invokeApi();
					handleApiCompletion(yrcApiContext);
				}
			}

			eleExtnGetOrderDetails = getExtentionModel("ExtnGetOrderDetails");
			if (eleExtnGetOrderDetails != null) {
				Element eleExtnGetOrderDetailsOrderLines = YRCXmlUtils.getChildElement(eleExtnGetOrderDetails, "OrderLines");
				Document amsRequestOrder = YRCXmlUtils.createDocument("Order");
				Element eleAMSOrderInput = amsRequestOrder.getDocumentElement();
				eleAMSOrderInput.setAttribute("SellerOrganizationCode",eleExtnGetOrderDetails.getAttribute("SellerOrganizationCode"));
				eleAMSOrderInput.setAttribute("EnterpriseCode",eleExtnGetOrderDetails.getAttribute("EnterpriseCode"));
				String strOrderNo = eleExtnGetOrderDetails.getAttribute("OrderNo");

				if (strOrderNo.length() > 9) {
					eleAMSOrderInput.setAttribute("OrderNo", strOrderNo
							.substring(strOrderNo.length() - 9, strOrderNo
									.length()));
				} else {
					eleAMSOrderInput.setAttribute("OrderNo", strOrderNo);
				}

				eleAMSOrderInput.setAttribute("BillToID",eleExtnGetOrderDetails.getAttribute("BillToID"));
				eleAMSOrderInput.setAttribute("PendingChangesUserID",eleExtnGetOrderDetails.getAttribute("PendingChangesUserID"));
				String sMaxOrderStatus = eleExtnGetOrderDetails.getAttribute("MaxOrderStatus");
				//Modified to fix defect 822 - Discount is refunded to the customer for the entire order(OMS) after partial cancellation in COM 
				//Invoke HBCAMSPromotionSyncWebService only for draft orders
				String sDraftOrder = eleExtnGetOrderDetails.getAttribute("DraftOrderFlag");
				if (sMaxOrderStatus != null && !sMaxOrderStatus.equals("")) {
					eleAMSOrderInput.setAttribute("Status", sMaxOrderStatus);
				} else {
					eleAMSOrderInput.setAttribute("Status", "1000");
				}
				Element eleAMSOrderLinesInput = YRCXmlUtils.createChild(eleAMSOrderInput, "OrderLines");
				ArrayList extnnGetOrderDetailsOrderLinelist = YRCXmlUtils.getChildren(eleExtnGetOrderDetailsOrderLines,"OrderLine");
				if (extnnGetOrderDetailsOrderLinelist != null) {
					for (int i = 0; i < extnnGetOrderDetailsOrderLinelist.size(); i++) {
						Element eleGetOrderDetailsOrderLineInput = (Element) extnnGetOrderDetailsOrderLinelist.get(i);
						String strShipTogetherNo = eleGetOrderDetailsOrderLineInput.getAttribute("ShipTogetherNo");
						if (strShipTogetherNo != null&& (strShipTogetherNo.equals("GWP_Order") || strShipTogetherNo.equals("GWP_OrderLine")))
							continue;
						Element eleItemInput = YRCXmlUtils.getChildElement(eleGetOrderDetailsOrderLineInput, "Item");
						Element eleLinePriceInfoInput = YRCXmlUtils.getChildElement(eleGetOrderDetailsOrderLineInput,"LinePriceInfo");
						Element eletempOrderLineTranQuantity = YRCXmlUtils.getChildElement(eleGetOrderDetailsOrderLineInput,"OrderLineTranQuantity");
						
						//AMS Upgrade Change:: setting of ExtnClearanceLevel flag
						Element eleExtnOrderLine = 
							YRCXmlUtils.getChildElement(eleGetOrderDetailsOrderLineInput, "Extn") ;
						
						Element eleAMSOrderLineInput = YRCXmlUtils.createChild(eleAMSOrderLinesInput, "OrderLine");
						eleAMSOrderLineInput.setAttribute("OrderedQty",eletempOrderLineTranQuantity.getAttribute("OrderedQty"));
						eleAMSOrderLineInput.setAttribute("PrimeLineNo",eleGetOrderDetailsOrderLineInput.getAttribute("PrimeLineNo"));
						//eleAMSOrderLineInput.setAttribute("GiftFlag",eleGetOrderDetailsOrderLineInput.getAttribute("GiftFlag"));

						Element eleAMSItemInput = YRCXmlUtils.createChild(eleAMSOrderLineInput, "Item");
						eleAMSItemInput.setAttribute("ItemID", eleItemInput.getAttribute("ItemID"));
						Element eleAMSExtnInput = YRCXmlUtils.createChild(eleAMSOrderLineInput, "Extn");
						eleAMSExtnInput.setAttribute("ExtnMinOrderFlag", "1");
						
						//AMS Upgrade Change:: setting of ExtnClearanceLevel flag
						eleAMSExtnInput.setAttribute("ExtnClearanceLevel", 
							eleExtnOrderLine.getAttribute("ExtnTransactionID")) ;

						Element eleAMSLinePriceInfo = YRCXmlUtils.createChild(eleAMSOrderLineInput, "LinePriceInfo");
						eleAMSLinePriceInfo.setAttribute("IsPriceLocked","N");
						eleAMSLinePriceInfo.setAttribute("UnitPrice",eleLinePriceInfoInput.getAttribute("UnitPrice"));
					}
					Element eleExtnGetOrderDetailsHeaderCharges = XMLUtil.getChildElement(eleExtnGetOrderDetails,"HeaderCharges");
					if (eleExtnGetOrderDetailsHeaderCharges != null) {
						NodeList elemsHdrChargeNodeList = eleExtnGetOrderDetailsHeaderCharges.getElementsByTagName("HeaderCharge");
						if(elemsHdrChargeNodeList != null && elemsHdrChargeNodeList.getLength() > 0)
						{
							for(int i=0; i<elemsHdrChargeNodeList.getLength(); i++){
								Element eleExtnGetOrderDetailsHeaderCharge = (Element)elemsHdrChargeNodeList.item(i);
								if (eleExtnGetOrderDetailsHeaderCharge != null) {
									String strChargeCategory = eleExtnGetOrderDetailsHeaderCharge.getAttribute("ChargeCategory");
									String strChargeName = eleExtnGetOrderDetailsHeaderCharge.getAttribute("ChargeName");
		
									if (strChargeCategory.equals(HBCPCAConstants.SHIPPING_CHARGE_CATEGORY)&& strChargeName.equals(HBCPCAConstants.SHIPPING_CHARGE_NAME)) {
										Element eleLineShipCharge = XMLUtil.createChild(eleAMSOrderLinesInput,"OrderLine");
										eleLineShipCharge.setAttribute("GiftFlag", "Y");
										eleLineShipCharge.setAttribute("OrderedQty",
												"1");
		
										int noOfOrderLines = extnnGetOrderDetailsOrderLinelist.size();
										int primeLineNo = noOfOrderLines + 1000;
										String strprimeLineNoShipCharge = String.valueOf(primeLineNo);
										eleLineShipCharge.setAttribute("PrimeLineNo",strprimeLineNoShipCharge);
		
										Element eleItemShipCharge = XMLUtil.createChild(eleLineShipCharge, "Item");
										eleItemShipCharge.setAttribute("ItemID",HBCPCAConstants.ITEM_ID_SH1PPINGCHARGE_AMS_PROMOTIONS);
		
										Element eleExtnShipCharge = YRCXmlUtils.createChild(eleLineShipCharge, "Extn");
										eleExtnShipCharge.setAttribute("ExtnMinOrderFlag", "1");
		
										Element eleLinePriceInfoShipCharge = XMLUtil.createChild(eleLineShipCharge,"LinePriceInfo");
		
										eleLinePriceInfoShipCharge.setAttribute("IsPriceLocked", "Y");
										String strOriginalAmount = "";
										Element eleExtnGetOrderDetailsHeaderChargExtn = YRCXmlUtils.getChildElement(eleExtnGetOrderDetailsHeaderCharge,"Extn");
										if(eleExtnGetOrderDetailsHeaderChargExtn!=null){
											strOriginalAmount=eleExtnGetOrderDetailsHeaderChargExtn.getAttribute("ExtnOriginalAmount");
										}	
										if(strOriginalAmount!=null && !strOriginalAmount.equals("")){
											eleLinePriceInfoShipCharge.setAttribute("UnitPrice",strOriginalAmount);
										} else{
											eleLinePriceInfoShipCharge.setAttribute("UnitPrice",
													eleExtnGetOrderDetailsHeaderCharge.getAttribute("ChargeAmount"));
										}
										break;

									}
								}
							}
						}
					}
				}

				Element eleAMSPaymentMethodsInput = YRCXmlUtils.createChild(eleAMSOrderInput, "PaymentMethods");

				ArrayList paymentMethodList = YRCXmlUtils.getChildren(elePaymentMethodList, "PaymentMethod");
				double amountToCliamed = 0.0;
				if (paymentMethodList != null & paymentMethodList.size() > 0) {
					NodeList paymentMethodNodeList = (NodeList) YRCXPathUtils.evaluate(elePaymentMethodList,"PaymentMethod[@UsePaymentMethod='" + "Y"+ "']", XPathConstants.NODESET);

					for (int i = 0; i < paymentMethodNodeList.getLength(); i++) {
						Element elePaymentMethodInput = (Element) paymentMethodNodeList.item(i);
						if (elePaymentMethodInput != null) {
							Element eleAMSPayementMethodInput = YRCXmlUtils.createChild(eleAMSPaymentMethodsInput,"PaymentMethod");
							
							String strFundsAvailable = elePaymentMethodInput.getAttribute("FundsAvailable");
							String strGetFundsAvailableUserExitInvoked = elePaymentMethodInput.getAttribute("GetFundsAvailableUserExitInvoked");
							String strPaymentType = elePaymentMethodInput.getAttribute("PaymentType");
							String strResponseCode = "";
							String strResponseDescription= "";
							String strFndsAvailable= "";
							if(strFundsAvailable.equals("0.00")&& strPaymentType.equals("LTGC")&& strGetFundsAvailableUserExitInvoked.equals("N")  ){
								
								Document tempDoc = YRCXmlUtils.createDocument("PaymentMethod");
								Element eletempDocPaymentMethod = tempDoc.getDocumentElement();
								eletempDocPaymentMethod.setAttribute("EnterpriseCode", eleExtnGetOrderDetails.getAttribute("EnterpriseCode"));
								eletempDocPaymentMethod.setAttribute("DocumentType", eleExtnGetOrderDetails.getAttribute("DocumentType"));
								eletempDocPaymentMethod.setAttribute("OrderHeaderKey", eleExtnGetOrderDetails.getAttribute("OrderHeaderKey"));
								eletempDocPaymentMethod.setAttribute("OrderNo", eleExtnGetOrderDetails.getAttribute("OrderNo"));								
								
								eletempDocPaymentMethod.setAttribute("PaymentReference1", elePaymentMethodInput.getAttribute("PaymentReference1"));
								eletempDocPaymentMethod.setAttribute("PaymentReference2", elePaymentMethodInput.getAttribute("PaymentReference2"));
								eletempDocPaymentMethod.setAttribute("PaymentReference3", elePaymentMethodInput.getAttribute("PaymentReference3"));
								
								eletempDocPaymentMethod.setAttribute("PaymentType", elePaymentMethodInput.getAttribute("PaymentType"));
								eletempDocPaymentMethod.setAttribute("SvcNo", elePaymentMethodInput.getAttribute("SvcNo"));
															
									Document docGiftCardFundsAvailabale = null;
									YRCApiContext yrcApiContext = new YRCApiContext();
									yrcApiContext.setApiName("HBCGetFundsAvailableSyncWebService");
									yrcApiContext.setInputXml(tempDoc);
									yrcApiContext.setFormId(this.getFormId());
									new YRCApiCaller(yrcApiContext, true).invokeApi();
									handleApiCompletion(yrcApiContext);
									
									Element eleExtnGetFundsAvailableSyncWebService = getExtentionModel("ExtnGetFundsAvailableSyncWebService");
									if (eleExtnGetFundsAvailableSyncWebService != null) {
										strResponseCode = eleExtnGetFundsAvailableSyncWebService.getAttribute("ResponseCode");
										strResponseDescription = eleExtnGetFundsAvailableSyncWebService.getAttribute("ResponseDescription");
										strFndsAvailable = eleExtnGetFundsAvailableSyncWebService.getAttribute("FundsAvailable");
										elePaymentMethodInput.setAttribute("FundsAvailable", strFndsAvailable);									
										repopulateModel("PaymentMethodList");
									}
								}
							//eleAMSPayementMethodInput.setAttribute("CreditCardNo", elePaymentMethodInput.getAttribute("CreditCardNo"));
							// Updating CC No as blank
							// Fix for defect# 1860
							eleAMSPayementMethodInput.setAttribute("CreditCardNo", "");
							eleAMSPayementMethodInput.setAttribute("CreditCardType", elePaymentMethodInput.getAttribute("CreditCardType"));
							eleAMSPayementMethodInput.setAttribute("RequestedChargeAmount",elePaymentMethodInput.getAttribute("TransactionMaxChargeLimit"));
							amountToCliamed += Double.valueOf(elePaymentMethodInput.getAttribute("TransactionMaxChargeLimit")).doubleValue();
						}
					}
				}

				Element eleExtnGetOrderDetailsPromotions = YRCXmlUtils.getChildElement(eleExtnGetOrderDetails, "Promotions");
				if (eleExtnGetOrderDetailsPromotions != null) {
					Element eleAMSPromotionsInput = YRCXmlUtils.createChild(eleAMSOrderInput, "Promotions");
					ArrayList amsExtnGetOrderDetailsPromotionList = YRCXmlUtils.getChildren(eleExtnGetOrderDetailsPromotions,"Promotion");
					for (int i = 0; i < amsExtnGetOrderDetailsPromotionList.size(); i++) {
						Element eleExtnGetOrderDetailsPromotion = (Element) amsExtnGetOrderDetailsPromotionList.get(i);
						Element eleAMSPromotionInput = YRCXmlUtils.createChild(eleAMSPromotionsInput, "Promotion");
						eleAMSPromotionInput.setAttribute("PromotionId",eleExtnGetOrderDetailsPromotion.getAttribute("PromotionId"));
						eleAMSPromotionInput.setAttribute("PromotionApplied","N");

					}
				}

				ArrayList tempOrderLines = YRCXmlUtils.getChildren(eleAMSOrderLinesInput, "OrderLine");
				if (tempOrderLines != null && tempOrderLines.size() > 0) {
					//Modified to fix defect 822 - Discount is refunded to the customer for the entire order(OMS) after partial cancellation in COM 
					//Invoke HBCAMSPromotionSyncWebService only for draft orders
					if (!("N".equalsIgnoreCase(sDraftOrder)))
					{
					YRCApiContext apiContext = new YRCApiContext();
					apiContext.setApiName("HBCAMSPromotionSyncWebService");
					apiContext.setInputXml(amsRequestOrder);
					apiContext.setFormId(this.getFormId());
					new YRCApiCaller(apiContext, true).invokeApi();
					handleApiCompletion(apiContext);
					}
				}

				boolean changeOrderRequired = false;
				Document docChangeOrderInput = YRCXmlUtils.createDocument("Order");
				Element eleChangeOrderInput = docChangeOrderInput.getDocumentElement();

				eleChangeOrderInput.setAttribute("Action", "MODIFY");
				eleChangeOrderInput.setAttribute("OrderHeaderKey",strOrderHeaderKey);

				Element eleExtnAMSPromotionSyncWebService = getExtentionModel("ExtnAMSPromotionSyncWebService");
				//Start 3566 to validate the AMS webservice response- In response Error code is exists then Will not process
				boolean isError=false;
				Element eleError = YRCXmlUtils.getChildElement(eleExtnAMSPromotionSyncWebService, "Error");
				if(eleError!=null && !eleError.equals("")){
					String errorCode=eleError.getAttribute("ErrorCode");
						if(errorCode!=null && !errorCode.equalsIgnoreCase(""))
								isError=true;					
				}
				//End 3566
				if (eleExtnAMSPromotionSyncWebService != null && !isError) {//Added is Error condtion as part of defect fix 3566
					Element eleChangeOrderLinesInput = YRCXmlUtils.createChild(eleChangeOrderInput, "OrderLines");
					for (int m = 0; m < tempOrderLines.size(); m++) {
						Element eleAMSOrderlineInput = (Element) tempOrderLines.get(m);
						String strPrimeLineNo = eleAMSOrderlineInput.getAttribute("PrimeLineNo");
						Element eleAMSOrderlineOutput = (Element) YRCXPathUtils.evaluate(eleExtnAMSPromotionSyncWebService,"//OrderLine[@PrimeLineNo='"+ strPrimeLineNo + "']",XPathConstants.NODE);
						Element eleTempOrderLine = (Element) YRCXPathUtils.evaluate(eleExtnGetOrderDetails,"//OrderLine[@PrimeLineNo='"+ strPrimeLineNo + "']",XPathConstants.NODE);
						double discount = 0;
						if (eleAMSOrderlineOutput != null) {
							Element eleAMSLineChargesOutput = XMLUtil.getChildElement(eleAMSOrderlineOutput,"LineCharges");
							if (eleAMSLineChargesOutput != null) {
								NodeList amsNodeLineChargesList = eleAMSLineChargesOutput.getElementsByTagName("LineCharge");
								if (amsNodeLineChargesList != null&& amsNodeLineChargesList.getLength() > 0) {
									for (int j = 0; j < amsNodeLineChargesList.getLength(); j++) {
										Element eleAMSLineChargeOutput = (Element) amsNodeLineChargesList.item(j);
										// Calculating Discount
										if (eleAMSLineChargeOutput.getAttribute("ChargeCategory").equals("DISCOUNT")&& eleAMSLineChargeOutput.getAttribute("ChargeName").equals("DISCOUNT")) {
											String value = eleAMSLineChargeOutput.getAttribute("ChargePerLine");
											if (value != null&& value.length() > 0)
											discount = discount+ Double.parseDouble(eleAMSLineChargeOutput.getAttribute("ChargePerLine"));
										}
									}
								}
							}

						}
						if (eleTempOrderLine != null) {
								double doubleChargePerUnit = 0;
								Element eleTempLineCharges = YRCXmlUtils.getChildElement(eleTempOrderLine, "LineCharges");
								if(eleTempLineCharges!=null){
									Element eleTempDiscountLineCharge = (Element)YRCXPathUtils.evaluate(eleTempLineCharges, "LineCharge[@ChargeCategory='" + "DISCOUNT" + "']", XPathConstants.NODE);
									if(eleTempDiscountLineCharge != null){
										String strChargePerUnit = eleTempDiscountLineCharge.getAttribute("ChargePerUnit");
										if(strChargePerUnit!= null && !"".equals(strChargePerUnit))
											{
											 doubleChargePerUnit = Double.valueOf(strChargePerUnit).doubleValue();
											}
									}
								}
								if(doubleChargePerUnit != discount){
									 changeOrderRequired = true;
										Element eleChangeOrderLineInput = YRCXmlUtils.createChild(eleChangeOrderLinesInput,"OrderLine");
										eleChangeOrderLineInput.setAttribute("OrderLineKey", eleTempOrderLine.getAttribute("OrderLineKey"));
										eleChangeOrderLineInput.setAttribute("PrimeLineNo", strPrimeLineNo);
										Element eleChangeOrderLineChargesInput = YRCXmlUtils.createChild(eleChangeOrderLineInput,"LineCharges");
										Element eleChangeOrderLineChargeInput = YRCXmlUtils.createChild(eleChangeOrderLineChargesInput,"LineCharge");

										eleChangeOrderLineChargeInput.setAttribute("ChargeCategory", "DISCOUNT");
										eleChangeOrderLineChargeInput.setAttribute("ChargeName", "DISCOUNT");
										eleChangeOrderLineChargeInput.setAttribute("Reference", "ChangeOrderWhilePaymentMethodSelection");
										Element eleOrderLineTranQuantity = YRCXmlUtils.getChildElement(eleTempOrderLine,"OrderLineTranQuantity");
										String strOrderQuantity = eleOrderLineTranQuantity.getAttribute("OrderedQty");
										double intOrderQuantity = Double.parseDouble(strOrderQuantity);
										double chargePerUnit =0;
										if(discount >0){
										chargePerUnit = discount/ intOrderQuantity;
										// Removed the Math.round function on the chargePerUnit calculation
										// Fix for 1841
										chargePerUnit = chargePerUnit * 100.0 / 100.0;
										}
										eleChangeOrderLineChargeInput.setAttribute("ChargePerUnit", String.valueOf(chargePerUnit)); 
								 }
						} else {
								Element eleExtnGetOrderDetailsHeaderCharges = XMLUtil.getChildElement(eleExtnGetOrderDetails,"HeaderCharges");
								if (eleExtnGetOrderDetailsHeaderCharges != null) {
									NodeList elemsHdrChargeInputNodeList = eleExtnGetOrderDetailsHeaderCharges.getElementsByTagName("HeaderCharge");
									if(elemsHdrChargeInputNodeList != null && elemsHdrChargeInputNodeList.getLength() > 0)
									{
										for(int i=0; i<elemsHdrChargeInputNodeList.getLength(); i++){
											Element eleExtnGetOrderDetailsHeaderChargeInput = (Element)elemsHdrChargeInputNodeList.item(i);									
											if (eleExtnGetOrderDetailsHeaderChargeInput != null) {
												String strChargeCategory = eleExtnGetOrderDetailsHeaderChargeInput.getAttribute("ChargeCategory");
												String strChargeName = eleExtnGetOrderDetailsHeaderChargeInput.getAttribute("ChargeName");
												String strChargeAmount = eleExtnGetOrderDetailsHeaderChargeInput.getAttribute("ChargeAmount");
												double chargeAmount = Double.parseDouble(strChargeAmount);
												if(strChargeCategory != null && strChargeName != null){
													if (strChargeCategory.equals(HBCPCAConstants.SHIPPING_CHARGE_CATEGORY) && strChargeName.equals(HBCPCAConstants.SHIPPING_CHARGE_NAME)) {
														Element eleExtnGetOrderDetailsHeaderChargExtn = YRCXmlUtils.getChildElement(eleExtnGetOrderDetailsHeaderChargeInput,"Extn");
														String strOriginalAmount = "";
														double OriginalAmount = 0.0;
														if (eleExtnGetOrderDetailsHeaderChargExtn != null) {
															strOriginalAmount = eleExtnGetOrderDetailsHeaderChargExtn.getAttribute("ExtnOriginalAmount");
														}
				
														if (strOriginalAmount != null&& strOriginalAmount.length() > 0) {
															OriginalAmount = Double.parseDouble(strOriginalAmount);
														} else {
															OriginalAmount = chargeAmount;
														}
														
														double newShpCharge = OriginalAmount-discount;
														if(chargeAmount != newShpCharge){
															changeOrderRequired = true;
															Element eleChangeOrderHeaderCharges = YRCXmlUtils.createChild(eleChangeOrderInput,"HeaderCharges");
															Element eleChangeOrderHeaderCharge = YRCXmlUtils.createChild(eleChangeOrderHeaderCharges,"HeaderCharge");
															eleChangeOrderHeaderCharge.setAttribute("ChargeCategory","SHIPPINGCHARGE");
															eleChangeOrderHeaderCharge.setAttribute("ChargeName","SHIPPINGCHARGE");
															eleChangeOrderHeaderCharge.setAttribute("ChargeAmount",String.valueOf(newShpCharge));
															eleChangeOrderHeaderCharge.setAttribute("Reference", "ChangeOrderWhilePaymentMethodSelection");
															Element eleChangeOrderHeaderChargeExtn = YRCXmlUtils.createChild(eleChangeOrderHeaderCharge,"Extn");
															eleChangeOrderHeaderChargeExtn.setAttribute("ExtnOriginalAmount",String.valueOf(OriginalAmount));
														}
													}
												}
											}
										}
									}
								}
						}
					}
				}
				if(changeOrderRequired){
				YRCApiContext ctx = new YRCApiContext();
				ctx.setApiName("changeOrder");
				ctx.setInputXml(docChangeOrderInput);
				ctx.setFormId(this.getFormId());
				new YRCApiCaller(ctx, true).invokeApi();
				handleApiCompletion(ctx);

				YRCApiContext yrcApiContext = new YRCApiContext();
				yrcApiContext.setApiName("getOrderDetails");
				yrcApiContext.setInputXml(getOrderDetailsInputDoc);
				yrcApiContext.setFormId(this.getFormId());
				new YRCApiCaller(yrcApiContext, true).invokeApi();
				handleApiCompletion(yrcApiContext);
				eleExtnGetOrderDetails = getExtentionModel("ExtnGetOrderDetails");

				double amountLeft = 0.0;
				if (eleExtnGetOrderDetails != null) {
					Element elePriceInfo = YRCXmlUtils.getChildElement(eleExtnGetOrderDetails, "PriceInfo");
					if (elePriceInfo != null) {String total = elePriceInfo.getAttribute("TotalAmount");
						Element eleTempPriceInfo = YRCXmlUtils.getChildElement(eleOrder, "PriceInfo");
						if (eleTempPriceInfo != null)
							eleTempPriceInfo.setAttribute("TotalAmount", total);
						double totalAmount = Double.valueOf(total).doubleValue();
						if (totalAmount >= amountToCliamed) {
							amountLeft = totalAmount - amountToCliamed;
							String strAmountLeft = String.valueOf(amountLeft);
							eleTempPriceInfo.setAttribute("AmountLeft",strAmountLeft);
						} else {
							amountLeft = totalAmount - amountToCliamed;
						}
					}
					repopulateModel("Order");
				}
				Element eleGetOriginalOrderDetails_output = getModel("getOriginalOrderDetails_output");
				if (eleGetOriginalOrderDetails_output != null) {
					Element elePriceInfo = YRCXmlUtils.getChildElement(eleExtnGetOrderDetails, "PriceInfo");
					if (elePriceInfo != null) {
						String total = elePriceInfo.getAttribute("TotalAmount");
						Element eleGetOriginalOrderDetails_outputPriceInfo = YRCXmlUtils.getChildElement(eleGetOriginalOrderDetails_output,"PriceInfo");
						if (eleGetOriginalOrderDetails_outputPriceInfo != null)eleGetOriginalOrderDetails_outputPriceInfo.setAttribute("TotalAmount", total);
						repopulateModel("getOriginalOrderDetails_output");
					}

				}
				Element elePriceInfo = YRCXmlUtils.getChildElement(eleExtnGetOrderDetails, "PriceInfo");
				String total = "";
				if (elePriceInfo != null) {
					total = elePriceInfo.getAttribute("TotalAmount");
				}
				HBCOrderEntryWizardExtnBehavior orderEntryWizardExtnBehavior = (HBCOrderEntryWizardExtnBehavior) this.getParentExtnBehavior();
				Element eleReturnOrderDetails = orderEntryWizardExtnBehavior.getModel("ReturnOrderDetails");
				if (eleReturnOrderDetails != null) {
					total = elePriceInfo.getAttribute("TotalAmount");
					Element eleTempPriceInfo = YRCXmlUtils.getChildElement(eleReturnOrderDetails, "PriceInfo");
					if (eleTempPriceInfo != null)eleTempPriceInfo.setAttribute("TotalAmount", total);

					Element OverallTotals = YRCXmlUtils.getChildElement(eleExtnGetOrderDetails, "OverallTotals");
					if (OverallTotals != null) {
						Element eleTempOverallTotals = YRCXmlUtils.getChildElement(eleReturnOrderDetails,"OverallTotals");
						if (eleTempOverallTotals != null) {
							eleTempOverallTotals.setAttribute("GrandCharges",OverallTotals.getAttribute("GrandCharges"));
							eleTempOverallTotals.setAttribute("GrandDiscount",OverallTotals.getAttribute("GrandDiscount"));
							eleTempOverallTotals.setAttribute("GrandTax",OverallTotals.getAttribute("GrandTax"));
							eleTempOverallTotals.setAttribute("GrandTotal",OverallTotals.getAttribute("GrandTotal"));
							eleTempOverallTotals.setAttribute("LineSubTotal",OverallTotals.getAttribute("LineSubTotal"));
							eleTempOverallTotals.setAttribute("HdrCharges",OverallTotals.getAttribute("HdrCharges"));
							eleTempOverallTotals.setAttribute("HdrDiscount",OverallTotals.getAttribute("HdrDiscount"));
							eleTempOverallTotals.setAttribute("HdrTax",OverallTotals.getAttribute("HdrTax"));

						}
					}
					orderEntryWizardExtnBehavior.docReturnOrderDetails = eleReturnOrderDetails;
					orderEntryWizardExtnBehavior.repopulateModel("ReturnOrderDetails");
				}
				Element eleTemp1PaymentMethodList = this.getModel("PaymentMethodList");
				if (eleTemp1PaymentMethodList != null) {
					NodeList nPaymentMethodList = (NodeList) YRCXPathUtils.evaluate(eleTemp1PaymentMethodList,"PaymentMethod[@UsePaymentMethod='" + "Y"+ "']", XPathConstants.NODESET);
					for (int pm = 0; pm < nPaymentMethodList.getLength(); pm++) {
						Element elePaymentMethodInput = (Element) nPaymentMethodList.item(pm);
						if (elePaymentMethodInput != null) {
							if (elePaymentMethodInput.getAttribute("CreditCardType") != null&& elePaymentMethodInput.getAttribute("CreditCardType").length() > 0) {
								elePaymentMethodInput.setAttribute("MaxChargeLimit", total);
								elePaymentMethodInput.setAttribute("TransactionMaxChargeLimit", total);
								total = "0.0";
							} else {
								String strFundsAvailable = elePaymentMethodInput.getAttribute("FundsAvailable");
								double doubleFundsAvailable = Double.valueOf(strFundsAvailable).doubleValue();
								double doubletotal = Double.valueOf(total).doubleValue();
								if (doubleFundsAvailable > doubletotal) {elePaymentMethodInput.setAttribute("MaxChargeLimit", String.valueOf(doubletotal));
									elePaymentMethodInput.setAttribute("TransactionMaxChargeLimit", String.valueOf(doubletotal));
									total = "0.0";
								} else {
									elePaymentMethodInput.setAttribute("MaxChargeLimit",String.valueOf(doubleFundsAvailable));
									elePaymentMethodInput.setAttribute("TransactionMaxChargeLimit",String.valueOf(doubleFundsAvailable));
									doubletotal = doubletotal- doubleFundsAvailable;
									total = String.valueOf(doubletotal);
								}
							}
						}
					}
				}
			}
			}
		}
		super.postSetModel(arg0);
	}
	
	public boolean isLineChargeUpdateRequired (Element eleTempOrderLine){
		Element eleTempLineCharges = YRCXmlUtils.getChildElement(eleTempOrderLine, "LineCharges");
		if(eleTempLineCharges!=null){
			Element eleTempDiscountLineCharge = (Element)YRCXPathUtils.evaluate(eleTempLineCharges, "LineCharge[@ChargeCategory='" + "DISCOUNT" + "']", XPathConstants.NODE);
			if(eleTempDiscountLineCharge != null){
				String strChargePerUnit = eleTempDiscountLineCharge.getAttribute("ChargePerUnit");
				if(strChargePerUnit!= null && !"".equals(strChargePerUnit))
					{
					double doubleChargePerUnit = Double.valueOf(strChargePerUnit).doubleValue();
					 if(doubleChargePerUnit > 0){
						 return true;
					 }
					}
			}
		}
		return false;
	}
	

	public void setStrTranReturnCode(String strTranReturnCode) {
		this.strTranReturnCode = strTranReturnCode;
	}
	public Element getPaymentMethodList() {
		return PaymentMethodList;
	}
	public void setPaymentMethodList(Element paymentMethodList) {
		PaymentMethodList = paymentMethodList;
	}
	private void getSystemAvalilableStatus(){
		systemAvailableStatus = new HashMap<String,String>();
		Document inputDocCommonCode;
		try {
			inputDocCommonCode = XMLUtil.createDocument(HBCPCAConstants.E_COMMONCODE);
			Element inputCCEle = inputDocCommonCode.getDocumentElement();
			inputCCEle.setAttribute(HBCPCAConstants.A_CODETYPE, HBCPCAConstants.SYSTEM_AVAILABLE);
			YRCApiContext yrcApiContext = new YRCApiContext();
			yrcApiContext.setApiName(HBCPCAConstants.API_GET_COMMON_CODE_LIST);
			yrcApiContext.setInputXml(inputDocCommonCode);
			yrcApiContext.setFormId(this.getFormId());
			new YRCApiCaller(yrcApiContext, true).invokeApi();
			handleApiCompletion(yrcApiContext);
			if(yrcApiContext.getOutputXml()!= null){
				Document outputCCDoc = yrcApiContext.getOutputXml();
				if(!XMLUtil.isVoid(outputCCDoc)){
					Element outputCCDocEle = outputCCDoc.getDocumentElement();
					if(!XMLUtil.isVoid(outputCCDocEle)){
						NodeList outputCCNodeList = outputCCDocEle.getElementsByTagName(HBCPCAConstants.E_COMMONCODE);
						if(!XMLUtil.isVoid(outputCCNodeList)){
							for(int i=0;i < outputCCNodeList.getLength(); i++){
								Element outputCCEle = (Element) outputCCNodeList.item(i);
								if(!XMLUtil.isVoid(outputCCEle)){
									String systemName = outputCCEle.getAttribute(HBCPCAConstants.A_CODEVALUE);
									String availableStatus = outputCCEle.getAttribute(HBCPCAConstants.A_CODESHORTDESC);
									if(!XMLUtil.isVoid(systemName) && !systemName.trim().equals("") && 
											!XMLUtil.isVoid(availableStatus) && !availableStatus.trim().equals("")){
										systemAvailableStatus.put(systemName, availableStatus);
									}
								}
							}
						}
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	@Override
	public YRCValidationResponse validateButtonClick(String arg0) {
		// TODO Auto-generated method stub
		if("btnCoupon".equalsIgnoreCase(arg0)){
			this.callFromCoupon = true;
			if(this.PaymentMethodList !=null){
			Element eleOrder =this.getModel("Order");
			Element eleOriginalOrderDetials = this.getModel("getOriginalOrderDetails_output");
			Element eleExtnGetOrderDetails = this.getExtentionModel("ExtnGetOrderDetails");
			if(eleOriginalOrderDetials !=null){
			Document docChangeOrder = YRCXmlUtils.createDocument("Order");
			Element eleChangeOrder = docChangeOrder.getDocumentElement();
			eleChangeOrder.setAttribute("OrderHeaderKey", eleOrder.getAttribute("OrderHeaderKey"));
			Element elePaymentMethods = YRCXmlUtils.createChild(eleChangeOrder, "PaymentMethods");
			NodeList paymentMethodNodeList = this.PaymentMethodList.getChildNodes();
			//NodeList paymentMethodNodeList = (NodeList) YRCXPathUtils.evaluate(this.PaymentMethodList,"PaymentMethod[@UsePaymentMethod='" + "Y"+ "']", XPathConstants.NODESET);
			if(paymentMethodNodeList !=null && paymentMethodNodeList.getLength() >0){
				for(int i=0;i<paymentMethodNodeList.getLength();i++){
					Element elePaymentMethod = (Element)paymentMethodNodeList.item(i);
					if(elePaymentMethod!=null){
						String strUsePaymentMethod = elePaymentMethod.getAttribute("UsePaymentMethod");
						if(strUsePaymentMethod!=null & strUsePaymentMethod.equals("N")){
							String paymentGroup = elePaymentMethod.getAttribute("PaymentTypeGroup");
							if("STORED_VALUE_CARD".equals(paymentGroup)){
								Boolean flag = (Boolean)YRCXPathUtils.evaluate(eleOriginalOrderDetials, "PaymentMethods/PaymentMethod[@SvcNo='"+elePaymentMethod.getAttribute("SvcNo")+"']", XPathConstants.BOOLEAN);
								if(flag != null && flag.booleanValue()){
									Element eleChangeOrderPaymentMethod = YRCXmlUtils.importElement(elePaymentMethods, elePaymentMethod);
									eleChangeOrderPaymentMethod.setAttribute("Action", "REMOVE");
								}
							}else if("CREDIT_CARD".equals(paymentGroup)){
								NodeList samePaymentMethodList = (NodeList)YRCXPathUtils.evaluate(eleOriginalOrderDetials, "PaymentMethods/PaymentMethod[@CreditCardNo='"+elePaymentMethod.getAttribute("CreditCardNo")+"']", XPathConstants.NODESET);
								if(samePaymentMethodList!=null && samePaymentMethodList.getLength()>0){
									for (int j = 0; j < samePaymentMethodList.getLength(); j++) {
										Element samePaymentMethod = (Element)samePaymentMethodList.item(j);
										String originalCreditCardNo = elePaymentMethod.getAttribute("CreditCardNo");
										String originalCreditCardType = elePaymentMethod.getAttribute("CreditCardType");
										String originalPaymentType = elePaymentMethod.getAttribute("PaymentType");
										
										String tempCreditCardNo = samePaymentMethod.getAttribute("CreditCardNo");
										String tempCreditCardType = samePaymentMethod.getAttribute("CreditCardType");
										String tempPaymentType = samePaymentMethod.getAttribute("PaymentType");
										if(originalCreditCardNo.equals(tempCreditCardNo) && originalCreditCardType.equals(tempCreditCardType) && originalPaymentType.equals(tempPaymentType)){
											Element eleChangeOrderPaymentMethod = YRCXmlUtils.importElement(elePaymentMethods, elePaymentMethod);
											eleChangeOrderPaymentMethod.setAttribute("Action", "REMOVE");
										}
									}
								}
							}
							
						}else{
							Element eleChangeOrderPaymentMethod = YRCXmlUtils.importElement(elePaymentMethods, elePaymentMethod);
							eleChangeOrderPaymentMethod.setAttribute("MaxChargeLimit", elePaymentMethod.getAttribute("TransactionMaxChargeLimit"));
						}
					}
				}
				if(elePaymentMethods.getChildNodes()!=null && elePaymentMethods.getChildNodes().getLength() >0){
				YRCApiContext ctx = new YRCApiContext();
				ctx.setApiName("changeOrder");
				ctx.setInputXml(docChangeOrder);
				ctx.setFormId(this.getFormId());
				new YRCApiCaller(ctx, true).invokeApi();
				handleApiCompletion(ctx);
				}
			
			}
			}
			}
		}
		return super.validateButtonClick(arg0);
	}
	public Element getFromModel(String namespace) {
		// TODO Auto-generated method stub
		return this.getModel(namespace);
	}
}
