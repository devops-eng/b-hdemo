package com.pca.rcp.hbcp2.payment;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.pca.rcp.hbcp2.util.XMLUtil;
import com.yantra.yfc.rcp.YRCExtentionBehavior;
import com.yantra.yfc.rcp.YRCPlatformUI;

public class HBCPaymentMethodLineDisplay extends YRCExtentionBehavior {
	Element elePaymentMethod;
	@Override
	public void postSetModel(String model) {
		String sInternalReturnMsg = "", localeInternalReturnMsg = "";
		if ("paymentMethod".equalsIgnoreCase(model)) {
			elePaymentMethod = getModel(model);
			if (elePaymentMethod != null) {
				Element elePaymentDetailsList = XMLUtil.getChildElement(elePaymentMethod, "PaymentDetailsList");
				if (elePaymentDetailsList != null) {
					NodeList nPaymentDetails = elePaymentDetailsList.getElementsByTagName("PaymentDetails");
					if (nPaymentDetails != null && nPaymentDetails.getLength() > 0) {
						for (int i = 0; i < nPaymentDetails.getLength(); i++) {
							Element elePaymentDetails = (Element) nPaymentDetails.item(i);
							if (elePaymentDetails != null) {
								sInternalReturnMsg = elePaymentDetails.getAttribute("InternalReturnMessage");
								localeInternalReturnMsg = YRCPlatformUI.getFormattedString("PAYMENT_"+sInternalReturnMsg, "");
								if (!YRCPlatformUI.isVoid(localeInternalReturnMsg)) {
									elePaymentDetails.setAttribute("InternalReturnMessage",localeInternalReturnMsg);
								}
							}
						}
						repopulateModel(model);
					}
				}
			}
		}
		super.postSetModel(model);
	}
}
