package com.pca.rcp.hbcp2.payment;

import java.util.ArrayList;

import org.eclipse.swt.widgets.Button;

import com.pca.rcp.hbcp2.customerentry.HBCAddressCaptureWizardExtnBehavior;
import com.pca.rcp.hbcp2.util.HBCPCAUtil;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCExtentionBehavior;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCValidationResponse;

public class HBCPaymentMethodPopupExtnBehavior extends YRCExtentionBehavior {

	
	@Override
	public void postSetModel(String arg0) {
		// TODO Auto-generated method stub
		super.postSetModel(arg0);
	}
	@Override
	public boolean preCommand(YRCApiContext apiContext) {
		// TODO Auto-generated method stub
		return super.preCommand(apiContext);
	}
	
	@Override
	public YRCValidationResponse validateButtonClick(String fieldName) {
		// TODO Auto-generated method stub
		if("btnConfirm".equals(fieldName)){
			ArrayList hbcPaymentTypeEntryPanelExtnBehaviorList = this.getChildExtnBehaviors();
			for (int i = 0; i < hbcPaymentTypeEntryPanelExtnBehaviorList.size(); i++) {
				if(hbcPaymentTypeEntryPanelExtnBehaviorList.get(i)!=null && hbcPaymentTypeEntryPanelExtnBehaviorList.get(i) instanceof HBCPaymentTypeEntryPanelExtnBehavior){
					HBCPaymentTypeEntryPanelExtnBehavior hbcPaymentTypeEntryPanelExtnBehavior = (HBCPaymentTypeEntryPanelExtnBehavior)hbcPaymentTypeEntryPanelExtnBehaviorList.get(i);
					 ArrayList hbcCreditCardDetailsExtnBehaviorList = hbcPaymentTypeEntryPanelExtnBehavior.getChildExtnBehaviors();
						for (int j = 0; j < hbcCreditCardDetailsExtnBehaviorList.size(); j++) {
							if(hbcCreditCardDetailsExtnBehaviorList.get(j)!=null && hbcCreditCardDetailsExtnBehaviorList.get(j) instanceof HBCCreditCardDetailsExtnBehavior){
								HBCCreditCardDetailsExtnBehavior hbCreditCardDetailsExtnBehavior = (HBCCreditCardDetailsExtnBehavior)hbcCreditCardDetailsExtnBehaviorList.get(j);
								Button radEnterNewAddress = (Button)HBCPCAUtil.getControl(hbCreditCardDetailsExtnBehavior.getOwnerForm(), "", "radEnterNewAddress", false);
								if(radEnterNewAddress!=null && radEnterNewAddress.getSelection()){
								 ArrayList hbcAddressCaptureWizardExtnBehaviorList = hbCreditCardDetailsExtnBehavior.getChildExtnBehaviors();
								 for (int k = 0; k < hbcAddressCaptureWizardExtnBehaviorList.size(); k++) {
										if(hbcAddressCaptureWizardExtnBehaviorList.get(k)!=null && hbcAddressCaptureWizardExtnBehaviorList.get(k) instanceof HBCAddressCaptureWizardExtnBehavior){
											HBCAddressCaptureWizardExtnBehavior hbcAddressCaptureWizardExtnBehavior = (HBCAddressCaptureWizardExtnBehavior)hbcAddressCaptureWizardExtnBehaviorList.get(k);
											YRCValidationResponse response = new YRCValidationResponse();
											

								    		String strFirstName = hbcAddressCaptureWizardExtnBehavior.getFieldValue("txtFirstName"); 
								    		String strLastName = hbcAddressCaptureWizardExtnBehavior.getFieldValue("txtLastName");
								    		String strLineAdd1 = hbcAddressCaptureWizardExtnBehavior.getFieldValue("txtAddressLine1");
								    		String strCity = hbcAddressCaptureWizardExtnBehavior.getFieldValue("txtCity");
								    		String strState = hbcAddressCaptureWizardExtnBehavior.getFieldValue("cmbState"); 
								    		String strPostalCode = hbcAddressCaptureWizardExtnBehavior.getFieldValue("txtPostalCode");
								    		String strCmbCountry = hbcAddressCaptureWizardExtnBehavior.getFieldValue("cmbCountry");
								    		String strDayTimePhone = hbcAddressCaptureWizardExtnBehavior.getFieldValue("txtDayTimePhone");
								    		String strtxtEmail = hbcAddressCaptureWizardExtnBehavior.getFieldValue("txtEmail");
								    		//Start:Defect 746
								    		if(("").equals(strState))
								    		{
								    			strState =this.getFieldValue("txtState");
								    		}
								    		//End :Defect 746
								    		if ((YRCPlatformUI.isVoid(strFirstName))){
												response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"CUSTOMER_FIRST_NAME_NULL_ERR_MSG");
												YRCPlatformUI.showError(YRCPlatformUI.getString("Error"), "CUSTOMER_FIRST_NAME_NULL_ERR_MSG");
												setFocus("txtFirstName");
												return response;
											}
								    		if ((YRCPlatformUI.isVoid(strLastName))){
												response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"CUSTOMER_LAST_NAME_NULL_ERR_MSG");
												YRCPlatformUI.showError(YRCPlatformUI.getString("Error"), "CUSTOMER_LAST_NAME_NULL_ERR_MSG");
												setFocus("txtLastName");
												return response;
											}
								    		if ((YRCPlatformUI.isVoid(strLineAdd1))){
												response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"SHIPTO_ADD1_NULL_ERR_MSG");
												YRCPlatformUI.showError(YRCPlatformUI.getString("Error"), "SHIPTO_ADD1_NULL_ERR_MSG");
												setFocus("txtAddressLine1");
												return response;
											}
								    		
											if ("".equals(strCity)){
													response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"SHIPTO_CITY_NULL_ERR_MSG");
													YRCPlatformUI.showError("Error", "SHIPTO_CITY_NULL_ERR_MSG");
													setFocus("txtCity");
													return response;
											}
								    		
								    		if ((YRCPlatformUI.isVoid(strPostalCode))){					
												//if(!HBCPCAUtil.validateZipCode(strPostalCode)){
													response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"SHIPTO_ZIPCODE_NULL_ERR_MSG");
													YRCPlatformUI.showError("Error", "SHIPTO_ZIPCODE_NULL_ERR_MSG");
													setFocus("txtPostalCode");
													return response;
											//	}
								    		}
								    		if ("".equals(strCmbCountry)){
												response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"SHIPTO_COUNTY_NULL_ERR_MSG");
												YRCPlatformUI.showError("Error", "SHIPTO_COUNTY_NULL_ERR_MSG");
												setFocus("cmbCountry");
												return response;
								    		}
								    		if ("".equals(strState)){
												response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"SHIPTO_STATE_NULL_ERR_MSG");
												YRCPlatformUI.showError("Error", "SHIPTO_STATE_NULL_ERR_MSG");
												setFocus("cmbState");
												return response;
											}
											
											response = hbcAddressCaptureWizardExtnBehavior.validateAddress(response, true);
											if(response !=null){
												if(response.getStatusCode() == 3){
													return response;
												}
											}
											if ((YRCPlatformUI.isVoid(strDayTimePhone))){
								    			response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"CUSTOMER_DAY_PHONE");
												YRCPlatformUI.showError("Error", "CUSTOMER_DAY_PHONE");
												setFocus("txtDayTimePhone");
												return response;
											}
								    		if ((YRCPlatformUI.isVoid(strtxtEmail))){
								    			response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"CUSTOMER_EMAI_ID_ERR_MSG");
												YRCPlatformUI.showError("Error", "CUSTOMER_EMAI_ID_ERR_MSG");
												setFocus("txtEmail");
												return response;
											}

								    		/*boolean fakeIDExists = false;
								    		String[] emailParts = strtxtEmail.split("@");
								    		if(emailParts.length >2){
								    		String domain = emailParts[1];
								    		getFakeEmailIDDomains();
								    		for(String strFakeIDDomain : FakeEmailIdDomains){
								    			if (strFakeIDDomain.equals(domain)){
								    				fakeIDExists = true;
								    				break;
								    			}	
								    		}
								    		}
								    		if(fakeIDExists){
								    			YRCSharedTaskOutput sharedTaskOutput = HBCPCAUtil.showOptionDialogBox(YRCPlatformUI.getString("GWP_MESSAGE"),YRCPlatformUI.getString("DUMMY_EMAIL_ID_VGC"));
												Element ele =sharedTaskOutput.getOutput();
												String strOutput = ele.getAttribute("SelectedOptionKey");
												if(!strOutput.equals("Ok_Button")){
													response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"DUMMY_EMAIL_ID_VGC");
													return response;
												}
								    		}*/
								    		
								    	
											
										}
									}
							}
							}
						}
				}
			}
			
			
		}else if("extn_checkaddress".equals(fieldName)){
			
			ArrayList hbcPaymentTypeEntryPanelExtnBehaviorList = this.getChildExtnBehaviors();
			for (int i = 0; i < hbcPaymentTypeEntryPanelExtnBehaviorList.size(); i++) {
				if(hbcPaymentTypeEntryPanelExtnBehaviorList.get(i)!=null && hbcPaymentTypeEntryPanelExtnBehaviorList.get(i) instanceof HBCPaymentTypeEntryPanelExtnBehavior){
					HBCPaymentTypeEntryPanelExtnBehavior hbcPaymentTypeEntryPanelExtnBehavior = (HBCPaymentTypeEntryPanelExtnBehavior)hbcPaymentTypeEntryPanelExtnBehaviorList.get(i);
					 ArrayList hbcCreditCardDetailsExtnBehaviorList = hbcPaymentTypeEntryPanelExtnBehavior.getChildExtnBehaviors();
						for (int j = 0; j < hbcCreditCardDetailsExtnBehaviorList.size(); j++) {
							if(hbcCreditCardDetailsExtnBehaviorList.get(j)!=null && hbcCreditCardDetailsExtnBehaviorList.get(j) instanceof HBCCreditCardDetailsExtnBehavior){
								HBCCreditCardDetailsExtnBehavior hbCreditCardDetailsExtnBehavior = (HBCCreditCardDetailsExtnBehavior)hbcCreditCardDetailsExtnBehaviorList.get(j);
								 ArrayList hbcAddressCaptureWizardExtnBehaviorList = hbCreditCardDetailsExtnBehavior.getChildExtnBehaviors();
								 for (int k = 0; k < hbcAddressCaptureWizardExtnBehaviorList.size(); k++) {
										if(hbcAddressCaptureWizardExtnBehaviorList.get(k)!=null && hbcAddressCaptureWizardExtnBehaviorList.get(k) instanceof HBCAddressCaptureWizardExtnBehavior){
											HBCAddressCaptureWizardExtnBehavior hbcAddressCaptureWizardExtnBehavior = (HBCAddressCaptureWizardExtnBehavior)hbcAddressCaptureWizardExtnBehaviorList.get(k);
											YRCValidationResponse response = new YRCValidationResponse();
											Button extn_checkaddress = (Button)HBCPCAUtil.getControl(this.getOwnerForm(), "", "extn_checkaddress", false);
											extn_checkaddress.setEnabled(false);
											response = hbcAddressCaptureWizardExtnBehavior.validateAddress(response, false);
											extn_checkaddress.setEnabled(true);
										}
									}
							}
						}
				}
			}
		
		}
		return super.validateButtonClick(fieldName);
	}
}
