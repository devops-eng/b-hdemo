package com.pca.rcp.hbcp2.payment;

import org.w3c.dom.Element;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;


import com.pca.rcp.hbcp2.util.HBCPCAConstants;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCExtentionBehavior;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCXmlUtils;

public class HBCPaymentTypeEntryPanelExtnBehavior  extends YRCExtentionBehavior{
	String sOrgCode = null;
	NodeList nCommonCodeList = null;
	
	/**
	 * R3 - This method is implemented to modify the model to disable PAYPAL
	 * and FIFTY-ONE tenders.
	**/
	 public void postSetModel(String model) {
		 if (YRCPlatformUI.equals("input",model)) {
			 Element elePaymentList = getModel("input");
			 sOrgCode = elePaymentList.getAttribute("OrganizationCode");
		 }
		 
		 if (YRCPlatformUI.equals("getPaymentTypeList_output",model)) {
			 Element elePaymentList = getModel("getPaymentTypeList_output");
			 if(elePaymentList!=null){
				 NodeList nPaymentList = elePaymentList.getElementsByTagName("PaymentType");
				 for(int i=0;i<nPaymentList.getLength();i++){
					 Element elePaymentType = (Element) nPaymentList.item(i);
					 String strPaymentType = YRCXmlUtils.getAttribute(elePaymentType, "PaymentType");

					 if (nCommonCodeList!= null)
					 {
						 //for each common code value, validating the input for payment Type
						 for(int j=0; j<nCommonCodeList.getLength(); j++)
						 {
							 Element eleCommonCode = (Element) nCommonCodeList.item(j);
							 String sCodeValue = eleCommonCode.getAttribute("CodeValue");
							 if (sCodeValue.equalsIgnoreCase(strPaymentType))
							 {
								 elePaymentList.removeChild(elePaymentType);
							 }
						 }
					 }
				 }
			 }
			 super.repopulateModel(model);
		 }
	 }
	
	@Override
	public boolean preCommand(YRCApiContext apiContext) {
		// TODO Auto-generated method stub
		return super.preCommand(apiContext);
	}
	@Override
	public void validateComboField(String fieldName, String fieldValue) {
		// TODO Auto-generated method stub
		if("cmbPaymentTypes".equals(fieldName)){
			if(this.getParentExtnBehavior()!=null){
				this.getParentExtnBehavior().setControlVisible("extn_checkaddress", false);
				}
		}
		super.validateComboField(fieldName, fieldValue);
	}
	@Override
	public void init() {
		//setting the input for  getCommonCodeList API
		org.w3c.dom.Document docHBCGetCommonCodeList = YRCXmlUtils.createDocument("CommonCode");
		Element eleCommonCodeInput = docHBCGetCommonCodeList.getDocumentElement();
		//eleCommonCodeInput.setAttribute("OrganizationCode", HBCSOMConstants.DEFAULT_ORGANIZATION_CODE);
		eleCommonCodeInput.setAttribute("CodeType", HBCPCAConstants.DISABLE_PAYMENT);
		YRCApiContext context = new YRCApiContext();
		context.setFormId(super.getFormId());
		context.setApiName("HBCGetCommonCodeList");
		context.setInputXml(docHBCGetCommonCodeList);
		callApi(context);
	}
	/**
	 * This method is implemented to handle the completion of custom API call
	 */
	public void handleApiCompletion(YRCApiContext context) 
	{
		if ("HBCGetCommonCodeList".equals(context.getApiName()))
		{		
			this.setExtentionModel("Extn_output_HBCGetCommonCodeList", context.getOutputXml().getDocumentElement());	
			Element eleCommonCodeList = this.getModel("Extn_output_HBCGetCommonCodeList");
			if (!YRCPlatformUI.isVoid(eleCommonCodeList)) 
			{
				nCommonCodeList = eleCommonCodeList.getElementsByTagName("CommonCode");

			}
			postSetModel("getPaymentTypeList_output");
		}
	}
}
