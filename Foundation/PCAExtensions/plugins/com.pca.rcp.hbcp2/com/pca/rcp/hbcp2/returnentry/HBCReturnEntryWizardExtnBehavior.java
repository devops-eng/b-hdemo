package com.pca.rcp.hbcp2.returnentry;

/**
 * Created on Dec 15,2009
 *
 */

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.pca.rcp.hbcp2.util.HBCPCAConstants;
import com.yantra.yfc.rcp.IYRCComposite;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCDesktopUI;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCWizard;
import com.yantra.yfc.rcp.YRCWizardExtensionBehavior;
import com.yantra.yfc.rcp.YRCXmlUtils;
import com.yantra.yfc.rcp.internal.YRCApiCaller;

/**
 * @author preddy-tw Copyright  2005-2009 Sterling Commerce, Inc. All Rights
 *         Reserved.
 */
public class HBCReturnEntryWizardExtnBehavior extends
		YRCWizardExtensionBehavior {

	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
	}

	@Override
	public void postSetModel(String arg0) {
		// TODO Auto-generated method stub
		super.postSetModel(arg0);
		if (arg0.equalsIgnoreCase("OriginalSalesOrders")) {
			Element eleOrders = getModel("OriginalSalesOrders");
			if (eleOrders != null) {
				YRCXmlUtils.getString(eleOrders);
				Element eOrder = YRCXmlUtils.getFirstChildElement(eleOrders);
				if (eOrder != null) {
					String sSalesOrderHeaderKey = YRCXmlUtils.getAttribute(
							eOrder, "OrderHeaderKey");

					Document inputForOrderDetails = YRCXmlUtils
							.createDocument("Order");
					Element eleOrder = inputForOrderDetails
							.getDocumentElement();
					YRCXmlUtils.setAttribute(eleOrder, "OrderHeaderKey",
							sSalesOrderHeaderKey);

					YRCApiContext yrcApiContext = new YRCApiContext();
					yrcApiContext.setApiName("getOrderDetails");
					yrcApiContext.setInputXml(inputForOrderDetails);
					yrcApiContext.setFormId(this.getFormId());
					this.callApi(yrcApiContext);

				}
			}
		}

	}

	@Override
	public boolean preCommand(YRCApiContext ApiContext) {
		// TODO Auto-generated method stub
		if (ApiContext.getApiName().equals("createOrderForReturn")) {
			Document indoc = ApiContext.getInputXml();
			Element eleOrder = indoc.getDocumentElement();

			
			Element eOrderLines = YRCXmlUtils.getChildElement(eleOrder,
					"OrderLines");
			NodeList nOrderLine = eOrderLines.getElementsByTagName("OrderLine");
			Element elesalesOrder = getExtentionModel("ExtnSalesOrderDetails");
			for (int i = 0; i < nOrderLine.getLength(); i++) {

				Element eOrderLine = (Element) nOrderLine.item(i);
				Element eLineCharges = YRCXmlUtils.createChild(eOrderLine,
						"LineCharges");
				Element eLineTaxes = YRCXmlUtils.createChild(eOrderLine,
						"LineTaxes");
				Element ederivedFrom = YRCXmlUtils.getChildElement(eOrderLine,
						"DerivedFrom");
				String sDerivedOrderLineKey = YRCXmlUtils.getAttribute(
						ederivedFrom, "OrderLineKey");

				
				if (elesalesOrder != null) {
					Element salesOrderLine = getSalesOrderLine(
							sDerivedOrderLineKey, elesalesOrder);
					if (salesOrderLine != null) {

						String salesOrderLineQty = salesOrderLine
								.getAttribute("OrderedQty");

						Element eOrderLineTranQuantity = YRCXmlUtils
								.getChildElement(eOrderLine,
										"OrderLineTranQuantity");
						String returnOrderLineQty = eOrderLineTranQuantity
								.getAttribute("OrderedQty");

						Element eSalesLineTaxes = YRCXmlUtils.getChildElement(
								salesOrderLine, "LineTaxes");

						if (eSalesLineTaxes != null) {
							Double TotalQty = Double
									.parseDouble(salesOrderLineQty);
							Double ReturnQty = Double
									.parseDouble(returnOrderLineQty);

							NodeList nSalesLineTax = eSalesLineTaxes
									.getElementsByTagName("LineTax");
							for (int k = 0; k < nSalesLineTax.getLength(); k++) {
								Element eSalesLineTax = (Element) nSalesLineTax
										.item(k);
								if (eSalesLineTax != null) {

									String sChargeCategory = YRCXmlUtils
											.getAttribute(eSalesLineTax,
													"ChargeCategory");
									if(!YRCPlatformUI.equals(HBCPCAConstants.HBC_SHIPPINGSURCHARGE,sChargeCategory)){
									Double sTax = Double
											.parseDouble(eSalesLineTax
													.getAttribute("Tax"));
									String sTaxName = YRCXmlUtils.getAttribute(
											eSalesLineTax, "TaxName");

									Double newTax = (ReturnQty / TotalQty)
											* sTax;
									String sNewTax = String.valueOf(newTax);

									Element eLineTax = YRCXmlUtils.createChild(
											eLineTaxes, "LineTax");
									YRCXmlUtils.setAttribute(eLineTax,
											"ChargeCategory", sChargeCategory);
									YRCXmlUtils.setAttribute(eLineTax, "Tax",
											sNewTax);
									YRCXmlUtils.setAttribute(eLineTax,
											"TaxName", sTaxName);
									YRCXmlUtils.setAttribute(eLineTax,
											"TaxPercentage", YRCXmlUtils.getAttribute(
													eSalesLineTax, "TaxPercentage"));
									}
								}

							}
						}

						Element eSalesLineCharges = YRCXmlUtils
								.getChildElement(salesOrderLine, "LineCharges");
						if (eSalesLineCharges != null) {
							NodeList nSalesLineCharge = eSalesLineCharges
									.getElementsByTagName("LineCharge");

							for (int j = 0; j < nSalesLineCharge.getLength(); j++) {
								Element eSalesLineCharge = (Element) nSalesLineCharge
										.item(j);
								if (!YRCPlatformUI.equals(HBCPCAConstants.HBC_SHIPPINGSURCHARGE,
										YRCXmlUtils.getAttribute(
												eSalesLineCharge,
												"ChargeCategory"))
										&& !YRCPlatformUI.equals(HBCPCAConstants.HBC_SHIPPINGCHARGE,
												YRCXmlUtils.getAttribute(
														eSalesLineCharge,
														"ChargeCategory"))) {
									YRCXmlUtils.importElement(eLineCharges,
											eSalesLineCharge);
								}

							}
						}
						
					}
				}

			}
			return super.preCommand(ApiContext);
		}
		if (ApiContext.getApiName().equals("changeOrderForReturn")) {
			Document indoc = ApiContext.getInputXml();
			Element eleOrder = indoc.getDocumentElement();

			Element eOrderLines = YRCXmlUtils.getChildElement(eleOrder,
					"OrderLines");
			NodeList nOrderLine = eOrderLines.getElementsByTagName("OrderLine");

			for (int i = 0; i < nOrderLine.getLength(); i++) {

				Element eOrderLine = (Element) nOrderLine.item(i);
				Element eLineCharges = YRCXmlUtils.createChild(eOrderLine,
						"LineCharges");
				Element eLineTaxes = YRCXmlUtils.createChild(eOrderLine,
						"LineTaxes");

				String sReturnOrderLineKey = YRCXmlUtils.getAttribute(
						eOrderLine, "OrderLineKey");
				// input for GetOrderDetails....
				Document inputForOrderLineDetails = YRCXmlUtils
						.createDocument("OrderLineDetail");
				Element eOrderLineElement = inputForOrderLineDetails
						.getDocumentElement();
				YRCXmlUtils.setAttribute(eOrderLineElement, "OrderLineKey",
						sReturnOrderLineKey);

				YRCApiContext yrcApiContext = new YRCApiContext();
				yrcApiContext.setApiName("getOrderLineDetails");
				yrcApiContext.setInputXml(inputForOrderLineDetails);
				yrcApiContext.setFormId(this.getFormId());
				// this.callApi(yrcApiContext);
				new YRCApiCaller(yrcApiContext, true).invokeApi();
				handleApiCompletion(yrcApiContext);

				Element eleOrderLinedetails = getExtentionModel("ExtnOrderLineDetails");

				if (eleOrderLinedetails != null) {

					String sOldOrderLineQty = eleOrderLinedetails
							.getAttribute("OrderedQty");

					Element eOrderLineTranQuantity = YRCXmlUtils
							.getChildElement(eOrderLine,
									"OrderLineTranQuantity");
					String returnOrderLineQty = eOrderLineTranQuantity
							.getAttribute("OrderedQty");

					Element eOldOrderLineTaxes = YRCXmlUtils.getChildElement(
							eleOrderLinedetails, "LineTaxes");
					if (eOldOrderLineTaxes != null) {
						Double OldQty = Double.parseDouble(sOldOrderLineQty);
						Double newReturnQty = Double
								.parseDouble(returnOrderLineQty);

						NodeList nOldOrderLineTax = eOldOrderLineTaxes
								.getElementsByTagName("LineTax");

						for (int k = 0; k < nOldOrderLineTax.getLength(); k++) {
							Element eOldOrderLineTax = (Element) nOldOrderLineTax
									.item(k);
							if (eOldOrderLineTax != null) {

								String sChargeCategory = YRCXmlUtils
										.getAttribute(eOldOrderLineTax,
												"ChargeCategory");
								if(!YRCPlatformUI.equals(HBCPCAConstants.HBC_SHIPPINGSURCHARGE,sChargeCategory)){
									
								Double sTax = Double
										.parseDouble(eOldOrderLineTax
												.getAttribute("Tax"));
								String sTaxName = YRCXmlUtils.getAttribute(
										eOldOrderLineTax, "TaxName");

								Double newTax = (newReturnQty / OldQty) * sTax;
								String sNewTax = String.valueOf(newTax);

								Element eLineTax = YRCXmlUtils.createChild(
										eLineTaxes, "LineTax");
								YRCXmlUtils.setAttribute(eLineTax,
										"ChargeCategory", sChargeCategory);
								YRCXmlUtils.setAttribute(eLineTax, "Tax",
										sNewTax);
								YRCXmlUtils.setAttribute(eLineTax, "TaxName",
										sTaxName);
								YRCXmlUtils.setAttribute(eLineTax,
										"TaxPercentage", YRCXmlUtils.getAttribute(
												eOldOrderLineTax, "TaxPercentage"));
								}

							}

						}
					}

					Element eOldOrderLineCharges = YRCXmlUtils.getChildElement(
							eleOrderLinedetails, "LineCharges");
					if (eOldOrderLineCharges != null) {
						NodeList nOldOrderLineCharges = eOldOrderLineCharges
								.getElementsByTagName("LineCharge");

						for (int j = 0; j < nOldOrderLineCharges.getLength(); j++) {
							Element eOldOrderLineCharge = (Element) nOldOrderLineCharges
									.item(j);
							if (!YRCPlatformUI.equals(HBCPCAConstants.HBC_SHIPPINGSURCHARGE,
									YRCXmlUtils.getAttribute(
											eOldOrderLineCharge,
											"ChargeCategory"))
									&& !YRCPlatformUI.equals(HBCPCAConstants.HBC_SHIPPINGCHARGE,
											YRCXmlUtils.getAttribute(
													eOldOrderLineCharge,
													"ChargeCategory"))) {
								YRCXmlUtils.importElement(eLineCharges,
										eOldOrderLineCharge);
							}

						}
					}
					
				}
			}
			return super.preCommand(ApiContext);
		}
		
		return super.preCommand(ApiContext);
	}

	public void postCommand(YRCApiContext ApiContext)
	{
//		 SOM Changes - Override Return Creation Flow - Start
		String currentPageID = "";
		if(YRCDesktopUI.getCurrentPage() instanceof YRCWizard)
		{
			YRCWizard returnEntryWizard = (YRCWizard) YRCDesktopUI.getCurrentPage();
			currentPageID = returnEntryWizard.getCurrentPageID();
		}
		if("com.yantra.pca.ycd.rcp.tasks.createStoreReturn.wizardpages.YCDCreateStoreReturnAndExchangePaymentDetailsPage".equalsIgnoreCase(currentPageID))
		{
			// Invoke "OVERRIDE_RETURN_FLOW" common code to determine whether to implement OOB/Custom return creation flow.
			   if (ApiContext.getApiName().equals("processReturnCompletionUE")) 
			    {
					Element eleReturnOrderDetails = this.getModel("ReturnOrderDetails");
					String strEnterpriseCode = eleReturnOrderDetails.getAttribute("EnterpriseCode");
					Document docHBCGetCommonCodeList = YRCXmlUtils.createDocument("CommonCode");
					Element eleHBCGetCommonCode = docHBCGetCommonCodeList.getDocumentElement();
					eleHBCGetCommonCode.setAttribute("CodeType", "OVERRIDE_RETURN_FLOW");
					eleHBCGetCommonCode.setAttribute("CodeValue", strEnterpriseCode);
					YRCApiContext apiContext = new YRCApiContext();
					apiContext.setApiName("getCommonCodeList");
					apiContext.setInputXml(docHBCGetCommonCodeList);
					apiContext.setFormId(this.getFormId());
					new YRCApiCaller(apiContext, true).invokeApi();
					handleApiCompletion(apiContext);
					
					
				}
		
		}
      //  SOM Changes - Override Return Creation Flow - End
	}

	private Element getSalesOrderLine(String OrderLineKey, Element elesalesOrder) {
		// TODO Auto-generated method stub
		Element outOrderLine = null;

		Element eOrderLines = YRCXmlUtils.getChildElement(elesalesOrder,
				"OrderLines");
		NodeList nOrderLines = eOrderLines.getElementsByTagName("OrderLine");
		for (int i = 0; i < nOrderLines.getLength(); i++) {
			Element eOrderLine = (Element) nOrderLines.item(i);
			if (YRCPlatformUI.equals(OrderLineKey, YRCXmlUtils.getAttribute(
					eOrderLine, "OrderLineKey"))) {
				outOrderLine = eOrderLine;
			}
		}

		return outOrderLine;
	}

	@Override
	public void handleApiCompletion(YRCApiContext ctx) {
		// TODO Auto-generated method stub
		if (YRCPlatformUI.equals(ctx.getApiName(), "getOrderDetails")) {
			Document docOrderDetails = ctx.getOutputXml();
			Element esalesOrder = getExtentionModel("ExtnSalesOrderDetails");
			if (esalesOrder == null) {
				setExtentionModel("ExtnSalesOrderDetails", docOrderDetails
						.getDocumentElement());

			}
		} else if (YRCPlatformUI
				.equals(ctx.getApiName(), "getOrderLineDetails")) {
			Document docOrderLineDetails = ctx.getOutputXml();
			Element eOrderLinedetail = getExtentionModel("ExtnOrderLineDetails");
			if (eOrderLinedetail == null) {
				setExtentionModel("ExtnOrderLineDetails", docOrderLineDetails
						.getDocumentElement());

			}
		}
    //	SOM Changes - Override Return Creation Flow - Start
		String currentPageID = "";
		if(YRCDesktopUI.getCurrentPage() instanceof YRCWizard)
		{
			YRCWizard returnEntryWizard = (YRCWizard) YRCDesktopUI.getCurrentPage();
			currentPageID = returnEntryWizard.getCurrentPageID();
		}
		if("com.yantra.pca.ycd.rcp.tasks.createStoreReturn.wizardpages.YCDCreateStoreReturnAndExchangePaymentDetailsPage".equalsIgnoreCase(currentPageID))
		{
			// "OVERRIDE_RETURN_FLOW" common code value is used to determine whether to implement OOB/Custom return creation flow.
			if (ctx.getApiName().equals("getCommonCodeList")) {
				Document docCommonCodeListOutput = ctx.getOutputXml();
				Element eleCommonCodeListOutput = docCommonCodeListOutput.getDocumentElement();
				NodeList nlCommonCode = eleCommonCodeListOutput.getElementsByTagName("CommonCode");
				for(int i=0; i<nlCommonCode.getLength(); i++)
				{
					Element eleCommonCode = (Element) nlCommonCode.item(i);
					String strCodeShortDesc = eleCommonCode.getAttribute("CodeShortDescription");
					if((!YRCPlatformUI.isVoid(strCodeShortDesc)) && (strCodeShortDesc.equals("Y")))
					{
						Element returnOrderDetails = this.getModel("ReturnOrderDetails");
						String strOrderHeaderKey = returnOrderDetails.getAttribute("OrderHeaderKey");
						if(!YRCPlatformUI.isVoid(strOrderHeaderKey))
						{
							Document docHBCConfirmDraftReturnOrderInput = YRCXmlUtils.createDocument("ConfirmDraftOrder");
							Element eleHBCConfirmDraftReturnOrderInput = docHBCConfirmDraftReturnOrderInput.getDocumentElement();
							eleHBCConfirmDraftReturnOrderInput.setAttribute("OrderHeaderKey", strOrderHeaderKey);
							YRCApiContext apiContext = new YRCApiContext();
							apiContext.setApiName("HBCConfirmDraftReturnOrder");
							apiContext.setInputXml(docHBCConfirmDraftReturnOrderInput);
							apiContext.setFormId(this.getFormId());
							this.callApi(apiContext);
						}
					}
				}
			}		    	
		}
   //  SOM Changes - Override Return Creation Flow - End
		
		super.handleApiCompletion(ctx);

	}

	@Override
	public IYRCComposite createPage(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void pageBeingDisposed(String arg0) {
		// TODO Auto-generated method stub

	}

}