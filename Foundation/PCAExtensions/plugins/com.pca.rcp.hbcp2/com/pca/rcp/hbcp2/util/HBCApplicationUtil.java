package com.pca.rcp.hbcp2.util;

import com.yantra.yfc.rcp.YRCPlatformUI;

public class HBCApplicationUtil {
	
	private static int maximumSearchRecords;
	private static int initialPageSize;
	
	/**
	 * @return This api returns maximum number of search records to be displayed on the List Screen
	 */
	public static int getMaximumRecordsForSearch() {
		String maxRecords = YRCPlatformUI.getYfsSystemProperty("yfs.ui.MaxRecords");
		if (YRCPlatformUI.isVoid(maxRecords)) {
			maximumSearchRecords = 200;
		} else {
			maximumSearchRecords = Integer.parseInt(maxRecords);
		}
		
		return maximumSearchRecords;
	}
	
	/**
	 * @return This api returns number of records to be shown initially on the screen. If DB has more records
	 * then <b>getMaximumRecordsForSearch</b> method should be called.
	 */
	public static int getNumberOfRecordsForLazyLoading() {
		String maxRecordsperPage = YRCPlatformUI.getYfsSystemProperty("yfs.rcp.ui.pagesize");
		if (YRCPlatformUI.isVoid(maxRecordsperPage)) {
			initialPageSize = 30;
		} else {
			initialPageSize = Integer
					.parseInt(maxRecordsperPage);
		}
		
		return initialPageSize;
	}
}