package com.pca.rcp.hbcp2.util;

import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;

/*
 * This is a listener class which will cause the widget to scroll into view when it gets focus.
 * To use this listner, pass the scrolled composite containing the widget in the constructor of the listener.
 */
public class HBCCompositeListener implements org.eclipse.swt.widgets.Listener
{
	//the scrolled composite containing the widget	
	private ScrolledComposite sc;
	
	public HBCCompositeListener (ScrolledComposite sc)
	{
		this.sc = sc;
	}
	
	public void handleEvent(Event e) {
		Control child = (Control)e.widget;
		Rectangle bounds = child.getBounds();
		Rectangle area = sc.getClientArea();
		Point origin = sc.getOrigin();
		if (origin.x > bounds.x) origin.x = Math.max(0, bounds.x);
		if (origin.y > bounds.y) origin.y = Math.max(0, bounds.y);
		if (origin.x + area.width < bounds.x + bounds.width) origin.x = Math.max(0, bounds.x + bounds.width - area.width);
		if (origin.y + area.height < bounds.y + bounds.height) origin.y = Math.max(0, bounds.y + bounds.height - area.height);
		sc.setOrigin(origin);
	}

}