package com.pca.rcp.hbcp2.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import com.yantra.yfc.rcp.YRCDateTimeUtils;
import com.yantra.yfc.rcp.YRCDateTimeUtils2;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCUserLocale;
import com.yantra.yfc.rcp.YRCXmlUtils;

/**
 *
 * @author nsethi-tw, Modified: Kiran Potnuru
 * Added Date Util methods from COM
 *
 */
public class HBCDateUtil {

	/***************************User Related Variables************************/
	private static YRCUserLocale _yrcUserLocale = (YRCUserLocale)YRCPlatformUI.getYRCUserLocale();
	private static String USER_DATE_FORMAT = _yrcUserLocale.getDateFormat();
	private static Locale _userLocale = _yrcUserLocale.getJLocale();
	private static String _userTimeZone = _yrcUserLocale.getTimezone();
	private static String USER_TIME_FORMAT = _yrcUserLocale.getTimeFormat();
	private static String APPT_TIME_FORMAT = "HH:mm";
	private static String ISO_ENDOFDAY_FORMATTER = "yyyy-MM-dd'T'23:59:59";
	private static String OLD_ISO_ENDOFDAY_FORMATTER = "yyyyMMdd'T'23:59:59";
	/*************************************************************************/

	/**S
	 * Get the Date, after removing the Time component in the given timezone.
	 * @param date
	 * @param timeZone TimeZone Object
	 * @return Returns the date after removing the Time component in the given timezone.
	 */
	public static Date getDateWithoutTime(Date date,TimeZone timeZone){
		Calendar calendar = getCalendarInstance(timeZone);
		calendar.setTime(date);
		calendar.set(Calendar.MILLISECOND,0);
		calendar.set(Calendar.SECOND,0);
		calendar.set(Calendar.MINUTE,0);
		calendar.set(Calendar.HOUR_OF_DAY,0);
		return calendar.getTime();
	}

	private static TimeZone getUserTimeZone(){
		return TimeZone.getTimeZone(_userTimeZone);
	}

	/**
	 * Get the Date, in Users Date Format
	 * @param date
	 * @return Returns the date in Users Date Format.
	 */
	public static String getFormattedDate(Date date){
		return getFormattedDate(date,getUserTimeZone());
	}

	/**
	 * Get the Date String in Users Date format from dateStr in the given TimeZone
	 * @param dateTimeStr Date as a String
	 * @param timeZoneStr TimeZone as a String
	 * @return Returns the Date String in Users Date format from dateStr in the given TimeZone
	 */
	public static String getFormattedDate(String dateTimeStr,String timeZoneStr){
		TimeZone tz = getTimeZoneFromString(timeZoneStr);
		Date date = getDateFromXml(dateTimeStr,getTimeZoneFromString(timeZoneStr));
		return getFormattedDate(date,tz);
	}

	/**
	 * Get the Date String in Users Date format from dateStr in Users TimeZone
	 * @param dateTimeStr Date as a String
	 * @return Returns the Date String in Users Date format from dateStr in Users TimeZone
	 */
	public static String getFormattedDate(String dateTimeStr){
		return getFormattedDate(dateTimeStr,_userTimeZone);
	}

	/**
	 * Get the Date String, in Users Date Format in the given TimeZone
	 * @param date
	 * @param timeZone TimeZone Object
	 * @return Returns the date String in Users Date Format in the given TimeZone.
	 */
	public static String getFormattedDate(Date date,TimeZone timeZone){
		return getFormattedDate(date,timeZone,USER_DATE_FORMAT);
	}

	/**
	 * Get the Date String in given format in the given TimeZone
	 * @param dateStr
	 * @param timeZone TimeZone Object
	 * @param format
	 * @return Returns the Date String in given format in the given TimeZone
	 */
	public static String getFormattedDate(Date date,TimeZone tz,String format){
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		formatter.setTimeZone(tz);
		return formatter.format(date);
	}

	/**
	 * Get the formatted Time String in Users Time Format
	 * @param date
	 * @param timeZone TimeZone Object
	 * @return Returns the formatted Time String in Users Time Format
	 */
	public static String getFormattedTime(String timeStr){
		Date date = getDateFromTimeStr(timeStr);
		SimpleDateFormat timeFormatter = new SimpleDateFormat(USER_TIME_FORMAT);
		return timeFormatter.format(date);
	}

	public static Date getDateFromTimeStr(String timeStr) {
		try {
			return getDateFromTimeStr(timeStr,getInstallTimeZone());
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static Date getDateFromTimeStr(String timeStr, TimeZone tz) throws java.text.ParseException {
		SimpleDateFormat parser = new SimpleDateFormat(getIsoTimeFormat());
		parser.setTimeZone(tz);
		Date date = null;
		try {
			date = parser.parse(timeStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * Get the Time String, in Users Time Format in the given TimeZone
	 * @param date
	 * @param timeZone TimeZone Object
	 * @return Returns the Time String in Users Time Format in the given TimeZone.
	 */
	public static String getFormattedTime(Date date,TimeZone timeZone){
		SimpleDateFormat formatter = new SimpleDateFormat(USER_TIME_FORMAT);
		formatter.setTimeZone(timeZone);
		return formatter.format(date);
	}

	public static String getFormattedTimeForAppt(Date date,TimeZone timeZone){
		SimpleDateFormat formatter = new SimpleDateFormat(APPT_TIME_FORMAT);
		formatter.setTimeZone(timeZone);
		String time = formatter.format(date);
		String hourStr = time.substring(0,2);
		String minStr = time.substring(3);
		if(!YRCPlatformUI.isVoid(hourStr)){
			int hour = Integer.valueOf(hourStr).intValue();
			if(hour >= 12){
				if(hour == 12)
					return YRCPlatformUI.getFormattedString("PM",new String[]{time});
				else{
					hour = hour - 12;
					hourStr = String.valueOf(hour);
					if(hourStr.length() < 2)
						hourStr = "0"+hourStr;
					return YRCPlatformUI.getFormattedString("PM",new String[]{hourStr+":"+minStr});
				}
			}else
				return YRCPlatformUI.getFormattedString("AM", new String[]{time});
		}else
			return time;
	}

	/**
	 * Get the Time String, in Users Time Format in Users TimeZone
	 * @param date
	 * @return Returns the Time String in Users Time Format in the Users TimeZone.
	 */
	public static String getFormattedTime(Date date){
		return getFormattedTime(date,getUserTimeZone());
	}

	/**
	 * Get the DateTime String, in ISO DateTime Format in the install TimeZone
	 * @param date
	 * @return Returns the DateTime String in ISO DateTime Format in the install TimeZone.
	 */
	public static String getDeformattedDateTime(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat(getIsoDateTimeFormat());
		formatter.setTimeZone(getInstallTimeZone());
		return formatter.format(date);
	}

	/**
	 * Get the DateTime String, in ISO DateTime Format in the install TimeZone
	 * @param date
	 * @return Returns the DateTime String in ISO DateTime Format in the install TimeZone.
	 */
	public static String getDeformattedDateTime(Date date,String timeZoneStr){
		SimpleDateFormat formatter = new SimpleDateFormat(getIsoDateTimeFormat());
		formatter.setTimeZone(getTimeZoneFromString(timeZoneStr));
		return formatter.format(date);
	}

	/**
	 * Get the DateTime String, in ISO DateTimeZone Format in the install TimeZone
	 * @param date
	 * @return Returns the DateTime String in ISO DateTime Format in the install TimeZone.
	 */
	public static String getDeformattedDateTimeZone(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat(getIsoDateTimeZoneFormat());
		formatter.setTimeZone(getInstallTimeZone());
		return formatter.format(date);
	}

	/**
	 * Get the DateTime String, in ISO DateTimeZone Format in the given TimeZone
	 * @param date
	 * @param timeZoneStr TimeZone as a String
	 * @return Returns the DateTime String in ISO DateTime Format in the given TimeZone.
	 */
	public static String getDeformattedDateTimeZone(Date date,String timeZoneStr){
		SimpleDateFormat formatter = new SimpleDateFormat(getIsoDateTimeZoneFormat());
		formatter.setTimeZone(getTimeZoneFromString(timeZoneStr));
		return formatter.format(date);
	}



	public static TimeZone getInstallTimeZone() {
		//9.3 Upgrade :: Changing IBM ICU timezone to JAVA timezone
		String timeZone = YRCPlatformUI.getSystemLocale().getTimeZone().toString() ;
		return TimeZone.getTimeZone(timeZone) ;
	}

	private static String getInstallTimeZoneString() {
		return YRCPlatformUI.getSystemLocale().getTimezone();
	}

	/**
	 * Get the DateTime String from dateStr in ISO Date Format in the Users TimeZone
	 * @param dateStr
	 * @return Returns the DateTime String in ISO Date Format in the given TimeZone.
	 */
	public static String getDeformattedDate(String dateStr){
		try {
			return getDeformattedDate(dateStr,getInstallTimeZoneString());
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Get the DateTime String from dateStr in ISO Date Format in the given TimeZone
	 * @param dateStr
	 * @param timeZoneStr TimeZone as a String
	 * @return Returns the DateTime String in ISO Date Format in the given TimeZone.
	 * @throws java.text.ParseException
	 */
	public static String getDeformattedDate(String dateStr,String timeZoneStr) throws java.text.ParseException{
		Date date = getDateFromUI(dateStr,timeZoneStr);
		return getDeformattedDateTimeZone(date,timeZoneStr);
	}

	/**
	 * Get the DateTime String, in ISO Date Format in the install TimeZone
	 * @param date
	 * @return Returns the DateTime String in ISO Date Format in the install TimeZone.
	 */
	public static String getDeformattedDate(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat(getIsoDateFormat());
		formatter.setTimeZone(getInstallTimeZone());
		return formatter.format(date);
	}

	/**
	 * Get the DateTime String, in ISO Date Format in the given TimeZone
	 * @param date
	 * @param timeZoneStr TimeZone as a String
	 * @return Returns the DateTime String in ISO Date Format in the given TimeZone.
	 */
	public static String getDeformattedDate(Date date,String timeZoneStr){
		SimpleDateFormat formatter = new SimpleDateFormat(getIsoDateFormat());
		formatter.setTimeZone(getTimeZoneFromString(timeZoneStr));
		return formatter.format(date);
	}

	/**
	 * Get the Date from DateTime String taken from the xmls. This method is modified in COMv4
	 *  to check if Time needs to be ignored.
	 * @param dateTimeStr Date as a String
	 * @param ignoreTime ignore Time part while parsing the date string
	 * @return Returns the Date
	 */

	public static Date getDateFromXml(String dateTimeStr){
		return getDateFromXml(dateTimeStr,null);
	}

	public static Date getDateFromXml(String dateTimeStr,TimeZone tz){
		if(dateTimeStr.indexOf('T') > 0)
			return getDateFromXml(dateTimeStr,false,tz);
		else
			return getDateFromXml(dateTimeStr,true,tz);
	}

	public static Date getDateFromXml(String dateTimeStr,boolean ignoreTime){
		return getDateFromXml(dateTimeStr,ignoreTime,null);
	}

	public static long getDateDifference(Date biggerDate, Date smallerDate){
		long MILLIS_PER_DAY = 1000 * 60 * 60 * 24;
		long deltaMillis = biggerDate.getTime() - smallerDate.getTime();
		return deltaMillis / MILLIS_PER_DAY;
	}

	public static Date getDateFromXml(String dateTimeStr, boolean ignoreTime, TimeZone tz){
		Date date = null;
		try {
			if(ignoreTime){
				SimpleDateFormat formatter = new SimpleDateFormat(getIsoDateFormat());
				if(!YRCPlatformUI.isVoid(tz))
					formatter.setTimeZone(tz);
				else
					formatter.setTimeZone(getInstallTimeZone());
				date = formatter.parse(dateTimeStr);
			}else{
				if(YRCXmlUtils.getBoolean(YRCPlatformUI.getYfsSystemProperty("yfs.install.date.bcmode"))){
					SimpleDateFormat formatter = new SimpleDateFormat(getIsoDateTimeFormat());
					formatter.setTimeZone(getInstallTimeZone());
//					if(!YRCPlatformUI.isVoid(tz))
//						formatter.setTimeZone(tz);
//					else
//						formatter.setTimeZone(getInstallTimeZone());
					date = formatter.parse(dateTimeStr);
				}else
					date = YRCDateTimeUtils.getDate(dateTimeStr);
			}
		} catch (Exception e) {
			YRCPlatformUI.trace(e.getStackTrace());
		}
		return date;
	}

	/**
	 * Get the Date from Date String, in Users Date format
	 * @param dateStr
	 * @param timeZoneStr TimeZone as a String
	 * @return Returns the Date.
	 * @throws java.text.ParseException
	 */
	public static Date getDateFromUI(String dateStr,String timeZoneStr) throws java.text.ParseException{
		SimpleDateFormat formatter = new SimpleDateFormat(USER_DATE_FORMAT);
		TimeZone tz = getTimeZoneFromString(timeZoneStr);
		formatter.setTimeZone(tz);

		Date date = null;
		try{
			date = formatter.parse(dateStr);
		}catch(ParseException pe){
			YRCPlatformUI.trace(pe.getStackTrace());
		}
		return date;
	}

	/**
	 * Get the Date from Date String, in Users Date format
	 * @param dateStr
	 * @return Returns the Date.
	 * @throws java.text.ParseException
	 */
	public static Date getDateFromUI(String dateStr){
		try {
			return getDateFromUI(dateStr,_userTimeZone);
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Get the Date from Time String, in Users Time format
	 * @param timeStr
	 * @param timeZoneStr TimeZone as a String
	 * @return Returns the Date.
	 * @throws java.text.ParseException
	 */
	public static Date getTimeFromUI(String timeStr,String timeZoneStr) throws java.text.ParseException{
		SimpleDateFormat formatter = new SimpleDateFormat(USER_TIME_FORMAT);
		TimeZone tz = getTimeZoneFromString(timeZoneStr);
		formatter.setTimeZone(tz);
		Date date = null;
		try{
			date = formatter.parse(timeStr);
		}catch(ParseException pe){
			YRCPlatformUI.trace(pe.getStackTrace());
		}
		return date;
	}

	/**
	 * Get the Date from Time String, in Users Time format
	 * @param timeStr
	 * @return Returns the Date.
	 * @throws java.text.ParseException
	 */
	public static Date getTimeFromUI(String timeStr) throws java.text.ParseException{
		return getTimeFromUI(timeStr,_userTimeZone);
	}

	/**
	 * Get the TimeZone from String
	 * @param timeZoneStr TimeZone as a String
	 * @return Returns the TimeZone.
	 */
	public static TimeZone getTimeZoneFromString(String timeZoneStr){
		if(YRCPlatformUI.isVoid(timeZoneStr)){
			timeZoneStr = _userTimeZone;
		}
		TimeZone tz = TimeZone.getTimeZone(timeZoneStr);
		return tz;
	}

	/**
	 * Add two date objects
	 * @param date1 Date Object 1
	 * @param date2 Date Object 2
	 * @param timeZoneStr TimeZone as a String
	 * @return A Date.
	 */
	public static Date addDates(Date date1,Date date2,String timeZoneStr){
		TimeZone tz = getTimeZoneFromString(timeZoneStr);
		return addDates(date1,date2,tz);
	}

	/**
	 * Add two date objects
	 * @param date1 Date Object 1
	 * @param date2 Date Object 2
	 * @param timeZone TimeZone Object
	 * @return A Date.
	 */
	public static Date addDates(Date date1,Date date2,TimeZone timeZone){
		Date newDate = new Date(date1.getTime()+timeZone.getRawOffset()
				+date2.getTime());//adding the TimeZone offset to get 00:00:00 on the required Date in GMT
		return newDate;
	}

	/**
	 * Get the MonthDay String in MMM d format
	 * @param date
	 * @param timeZone TimeZone Object
	 * @return Returns the MonthDay String in MMM d format in the given TimeZone.
	 */
	public static String getMonthAndDay(Date date,TimeZone timeZone){
		String monthDayFormat = "MMM d";
		return getFormattedDate(date,timeZone,monthDayFormat);
	}

	/**
	 * Get the MonthDisplayDate String in the given TimeZone
	 * @param date
	 * @param timeZone TimeZone Object
	 * @return Returns the MonthDisplayDate String in the given TimeZone.
	 */
	public static String getMonthDisplay(Date date,TimeZone timeZone) {
		String monthDisplay = _yrcUserLocale.getMonthDisplayDateFormat();
		return getFormattedDate(date,timeZone,monthDisplay);
	}

	/**
	 * Get the Calendar Instance for a timeZone
	 * @param timeZone TimeZone Object
	 * @return Returns the Calendar in the given TimeZone.
	 */
	public static Calendar getCalendarInstance(TimeZone timeZone){
		return Calendar.getInstance(timeZone);
	}

	/**
	 * Get the Calendar Instance for users timeZone
	 * @return Returns the Calendar in the users TimeZone.
	 */
	public static Calendar getCalendarInstance(){
		return getCalendarInstance(_userLocale);
	}

	/**
	 * Get the Calendar Instance for given locale
	 * @param locale Locale
	 * @return Returns the Calendar in the given locale
	 */
	public static Calendar getCalendarInstance(Locale locale){
		return Calendar.getInstance(locale);
	}

	/**
	 * Get the a Unique key based on timestamp
	 * @param locale Locale
	 * @return Returns a Unique
	 */
	public static String getUniqueKey(){
		Date date = new Date();
		return getUniqueKey(date);
	}

	private static String getUniqueKey(Date date){
		String uniqueKeyFormat = "yyyyMMddHHmmss";
		SimpleDateFormat formatter = new SimpleDateFormat(uniqueKeyFormat);
		return formatter.format(date);
	}

	/**
	 * Get the first day of week
	 * @param timeZone TimeZone Object
	 * @return Returns an integer for the first day of week.
	 */
	public static int getFirstDayOfWeek(TimeZone timeZone){
		return getCalendarInstance(timeZone).getFirstDayOfWeek();
	}

	/**
	 * Check if given timezone is users timezone
	 * @param timeZone TimeZone Object
	 * @return Returns a boolean
	 */
	public static boolean isUserTimeZone(String timeZone) {
		if(timeZone.equals(_userTimeZone))
			return true;
		else
			return false;
	}

	public static Date getDateWithoutTime(Date date) {
		return getDateWithoutTime(date,getInstallTimeZone());
	}

	public static String getIsoDateTimeZoneFormat() {
		if (YRCXmlUtils.getBoolean(YRCPlatformUI.getYfsSystemProperty("yfs.install.date.bcmode")))
			return YRCDateTimeUtils.OLD_ISO_DATETIME_FORMAT;
		else
			return YRCDateTimeUtils.ISO_DATETIMEZONE_FORMAT;
	}

	public static String getIsoTimeFormat() {
		return YRCDateTimeUtils.ISO_TIME_FORMAT;
	}

	public static String getIsoDateFormat() {
		if (YRCXmlUtils.getBoolean(YRCPlatformUI.getYfsSystemProperty("yfs.install.date.bcmode")))
			return YRCDateTimeUtils.OLD_ISO_DATE_FORMAT;
		else
			return YRCDateTimeUtils.ISO_DATE_FORMAT;
	}

	public static String getIsoDateTimeFormat() {
		if (YRCXmlUtils.getBoolean(YRCPlatformUI.getYfsSystemProperty("yfs.install.date.bcmode")))
			return YRCDateTimeUtils.OLD_ISO_DATETIME_FORMAT;
		else
			return YRCDateTimeUtils.ISO_DATETIME_FORMAT;
	}

	public static Date addDays(Date date, TimeZone tz, int noOfDays){
		Calendar cal = getCalendarInstance(tz);
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_YEAR, noOfDays);
		return cal.getTime();
	}

	/**
	 * We will fetch the clienttime stamp format to show time and if it is not configured,
	 * we will show it from the user locales time stamp format.
	 * @param date
	 * @return
	 */
	public static String getClientFormattedDateTime(Date date){
		String clientTimestampFormat = YRCPlatformUI.getYRCUserLocale().getClientTimestampFormat();
		if(!YRCPlatformUI.isVoid(clientTimestampFormat)) {
			return YRCDateTimeUtils.getString(date, false, clientTimestampFormat,YRCPlatformUI.getYRCUserLocale().getJLocale());
		}else{
			return YRCDateTimeUtils.getString(date, false, YRCPlatformUI.getYRCUserLocale().getTimestampFormat(),YRCPlatformUI.getYRCUserLocale().getJLocale());
		}
	}

	/**
	 * Pass the date attribute to be converted, from DB tz to UserLoacle Time zone
	 * in the client time stamp format. If client time stamp format is not available, we will convert into
	 * user loacle time stamp format.
	 * @param attribute
	 * @return
	 */
	public static String convertString(String attribute) {
		// TODO Auto-generated method stub
		try {
			String clientTimestampFormat = YRCPlatformUI.getYRCUserLocale().getClientTimestampFormat();
			if(!YRCPlatformUI.isVoid(clientTimestampFormat))
				return YRCDateTimeUtils2.convertString(attribute,YRCDateTimeUtils.ISO_DATETIME_FORMAT,clientTimestampFormat,YRCPlatformUI.getSystemLocale().getTimeZone(),YRCPlatformUI.getYRCUserLocale().getTimeZone());
			else
				return YRCDateTimeUtils2.convertString(attribute,YRCDateTimeUtils.ISO_DATETIME_FORMAT,YRCPlatformUI.getYRCUserLocale().getTimestampFormat(),YRCPlatformUI.getSystemLocale().getTimeZone(),YRCPlatformUI.getYRCUserLocale().getTimeZone());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return attribute;
		}
	}
	public static String getConvertedEndOfDay(String sDate){
		Date dDate = new Date();
		try {
			dDate = YRCDateTimeUtils.getDate(sDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return getDeformattedDateTimeForEndOfDay(dDate);
	}

	public static String getDeformattedDateTimeForEndOfDay(Date date){
		String sEndOfDay = null;
		if(YRCXmlUtils.getBoolean(YRCPlatformUI.getYfsSystemProperty("yfs.install.date.bcmode"))){
			sEndOfDay = OLD_ISO_ENDOFDAY_FORMATTER;
		}
		else{
			sEndOfDay = ISO_ENDOFDAY_FORMATTER;
		}
		SimpleDateFormat formatter = new SimpleDateFormat(sEndOfDay);
		formatter.setTimeZone(getInstallTimeZone());
		return formatter.format(date);
	}
	/**
	 * This method will result date into string format based on the input.
	 * @param strFormat
	 * @return
	 * @throws Exception
	 */
	public static final String getCurrentDateInStringFormat(String strFormat) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		if (null != strFormat
				&& !"".equals(strFormat))
			dateFormat = new SimpleDateFormat(strFormat);
		Calendar calendar = Calendar.getInstance();
		Date warrantyEndDate = calendar.getTime();
		return dateFormat.format(warrantyEndDate);
	}
	/**
	 *
	 * @return
	 * @throws Exception
	 */
	public static final String getCurrentDateInStringFormat() throws Exception{
		return getCurrentDateInStringFormat("");
	}
	/**
	 * This method will result date into Date format.
	 * @return
	 * @throws Exception
	 */
	public static final Date getCurrentDate() throws Exception{
		Calendar calendar = Calendar.getInstance();
		return calendar.getTime();
	}
	/**
	 * This method will compare two dates and will return
	 * 1 if toDate > fromDate and 0 if toDate == fromDate and -1 if toDate < fromDate
	 * @param fromDate
	 * @param toDate
	 * @return int 1 if toDate > fromDate and 0 if toDate == fromDate and -1 if toDate < fromDate
	 * @throws Exception
	 */
	public static final int compareDates(Date fromDate, Date toDate) throws Exception{
		if (null == fromDate
				|| null == toDate)
			throw new NullPointerException();
		return toDate.compareTo(fromDate);
	}
	/**
	 * This method will return date in string format based on the input date and format.
	 * @param inputDate
	 * @param strFormat
	 * @return
	 * @throws Exception
	 */
	public static final String getDateInStringFormat(Date inputDate, String strFormat) throws Exception{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		if (null == inputDate)
			throw new NullPointerException();
		if (null != strFormat
				&& !"".equals(strFormat))
			dateFormat = new SimpleDateFormat(strFormat);
		return dateFormat.format(inputDate);
	}
	/**
	 * This method will return date in string format based on the input date.
	 * @param inputDate
	 * @return
	 * @throws Exception
	 */
	public static final String getDateInStringFormat(Date inputDate) throws Exception{
		return getDateInStringFormat(inputDate, "");
	}

	/**
	 *
	 * @param strDate1
	 * @param strDate2
	 * @return
	 * @throws Exception
	 */
	public static final long getDiffernceOfTwoDatesInMilliSecond(String strDate1, String strDate2) throws Exception{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date1 = dateFormat.parse(strDate1);
		Date date2 = dateFormat.parse(strDate2);
		int compareDate=compareDates(date2, date1);
		if (compareDate == 1 ){

		long ldate1 = date1.getTime();
		long ldate2 = date2.getTime();
		long diffDate = ldate1 - ldate2;
		return diffDate;
		}else{
		return compareDate;
		}
	}

	/**
	 *
	 * @param apptStart
	 * @param apptEnd
	 * @param timeZone
	 * @return
	 */

	public static String getAppontmentServiceSlot(String apptStart,String apptEnd, String timeZone) {
		TimeZone tz = getTimeZoneFromString(timeZone);

		Date sDate = getDateFromXml(apptStart, tz);
		Date eDate = getDateFromXml(apptEnd, tz);

		String sTimeStr = getFormattedTimeForAppt(sDate, tz);
		String eTimeStr = getFormattedTimeForAppt(eDate, tz);

		return YRCPlatformUI.getFormattedString("{0}-{1}", (Object[]) new String[]{sTimeStr, eTimeStr});
	}

	public static String addDaysToSyDate(Date date, int noOfDays){
		Calendar cal = getCalendarInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_YEAR, noOfDays);

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
		return dateFormat.format(cal.getTime());
	}



	/**
	 * This method will return the days difference between
	 * the given date and the system date.
	 *
	 */

	public static String getDiffernceOfDateInDays(String sOrderDate){
		String strNoOfDays="0";
		try
		{
		String strCurrentDate = HBCDateUtil.getCurrentDateInStringFormat("yyyy-MM-dd hh-mm-ss");
		long timeDiffMilliSec =getDiffernceOfTwoDatesInMilliSecond(strCurrentDate,sOrderDate);
		int iNoOfDays=(int) Math.round(timeDiffMilliSec/(1000*60*60*24));
		strNoOfDays = Integer.toString(iNoOfDays);

		}
		catch(Exception e)
		{
			e.printStackTrace() ;
		}
		return strNoOfDays;
	}


}
