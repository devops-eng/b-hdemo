/**
 * 
 */
package com.pca.rcp.hbcp2.util;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Text;

import com.yantra.yfc.rcp.IYRCComposite;
import com.yantra.yfc.rcp.IYRCPanelHolder;
import com.yantra.yfc.rcp.YRCConstants;
import com.yantra.yfc.rcp.YRCLabelBindingData;
import com.yantra.yfc.rcp.YRCLinkBindingData;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCStyledTextBindingData;
import com.yantra.yfc.rcp.YRCTextBindingData;

/**
 * 
 * @author nsethi-tw
 *
 */

public class HBCNotesDisplay extends Composite implements IYRCComposite {

	private Composite pnlRoot = null;
    private HBCNotesDisplayBehavior myBehavior;
	private StyledText stxNoteText;
	private Composite pnlNoteHeader;
	Label lblSmiley;
	private Text txtNoteHeader;
	private Composite pnlContactDetails;
	private Label lblContactType;
	protected Label lblInternalOnlyNote;
	private Link lnkContactReference;
	private Label lblContactReference;
	private Label lblPriority;
    public static final String FORM_ID = "com.hbc.rcp.pca.util.HBCNotesDisplay";
	private Composite pnlReturnHeader = null;
	private Label lblFiller = null;
	private Label lblReeturnHeader = null;
	private Link lnkReturn = null;
	private Label lblBraces = null;
	private StyledText stxNoteTextMore = null;
	private Link moreLess = null;
	private Composite pnlNoteTxt;

	public HBCNotesDisplay(Composite parent, int style,Object inputObject) {
		super(parent, style);
		initialize();
        setBindingForComponents();
        myBehavior = new HBCNotesDisplayBehavior(this, FORM_ID,inputObject);
	}
	
	private void initialize() {
		createRootPanel();
		excludeReturnHeader();
		this.setLayout(new FillLayout());
		setSize(new Point(300, 68));
	}
	
    private void setBindingForComponents() {
    	YRCLabelBindingData labelBindingData = new YRCLabelBindingData();
    	labelBindingData.setSourceBinding("Note:InboxNotes/@ContactType");
    	labelBindingData.setImageBinding(true);
    	labelBindingData.setName("lblContactType");
    	lblContactType.setData(YRCConstants.YRC_LABEL_BINDING_DEFINITION,labelBindingData);
    	
    	labelBindingData = new YRCLabelBindingData();
    	labelBindingData.setName("lblPriority");
    	lblPriority.setData(YRCConstants.YRC_LABEL_BINDING_DEFINITION,labelBindingData);
    	
    	labelBindingData = new YRCLabelBindingData();
    	labelBindingData.setSourceBinding("Note:InboxNotes/@CustomerSatIndicator");
    	labelBindingData.setImageBinding(true);
    	labelBindingData.setName("lblSmiley");
    	lblSmiley.setData(YRCConstants.YRC_LABEL_BINDING_DEFINITION,labelBindingData);
    	
    	labelBindingData = new YRCLabelBindingData();
    	labelBindingData.setSourceBinding("Note:InboxNotes/@ContactReference");
    	labelBindingData.setName("lblContactReference");
    	lblContactReference.setData(YRCConstants.YRC_LABEL_BINDING_DEFINITION,labelBindingData);
    	
    	YRCLinkBindingData linkBindingData = new YRCLinkBindingData();
    	linkBindingData.setSourceBinding("Note:InboxNotes/@ContactReference");
    	linkBindingData.setName("lnkContactReference");
    	lnkContactReference.setData(YRCConstants.YRC_LINK_BINDING_DEFINATION,linkBindingData);
    	
    	YRCStyledTextBindingData styledTextBindingData = new YRCStyledTextBindingData();
    	styledTextBindingData.setSourceBinding("Note:InboxNotes/@NoteText");
    	styledTextBindingData.setName("stxNoteText");
    	stxNoteText.setData(YRCConstants.YRC_STYLED_TEXT_BINDING_DEFINATION,styledTextBindingData);
    	
    	YRCStyledTextBindingData newStyledTextBindingData = new YRCStyledTextBindingData();
    	newStyledTextBindingData.setSourceBinding("Note:InboxNotes/@NoteText");
    	newStyledTextBindingData.setName("stxNoteTextMore");
    	stxNoteTextMore.setData(YRCConstants.YRC_STYLED_TEXT_BINDING_DEFINATION,newStyledTextBindingData);
    	
    	YRCTextBindingData textBindingData = new YRCTextBindingData();
    	textBindingData.setDynamic(true);
    	textBindingData.setSourceBinding("Note:InboxNotes/@NoteText");
    	textBindingData.setName("txtNoteHeader");
    	textBindingData.setDataType("NoteText");
    	txtNoteHeader.setData(YRCConstants.YRC_TEXT_BINDING_DEFINATION,textBindingData);
    	txtNoteHeader.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE,"NewNotesHeader1");
    	setNameForCompositesAndLabels();
    }
    
    private void setNameForCompositesAndLabels() {
    	
    	pnlRoot.setData(YRCConstants.YRC_CONTROL_NAME,"pnlRoot");
    	pnlNoteHeader.setData(YRCConstants.YRC_CONTROL_NAME,"pnlNoteHeader");
    	pnlContactDetails.setData(YRCConstants.YRC_CONTROL_NAME,"pnlContactDetails");
    	
    	lblSmiley.setData(YRCConstants.YRC_CONTROL_NAME,"lblSmiley");
    	lblContactType.setData(YRCConstants.YRC_CONTROL_NAME,"lblContactType");
    	lblContactReference.setData(YRCConstants.YRC_CONTROL_NAME,"lblContactReference");
    	lblPriority.setData(YRCConstants.YRC_CONTROL_NAME,"lblPriority");
	}

	public String getFormId() {
        return FORM_ID;
    }
    
    public Composite getRootPanel() {
        return pnlRoot;
    }

	private void createRootPanel() {
		GridLayout gridLayout3 = new GridLayout();
		gridLayout3.numColumns = 2;
		gridLayout3.marginWidth = 0;
		gridLayout3.marginHeight = 3;
		gridLayout3.horizontalSpacing = 0;
		gridLayout3.verticalSpacing = 0;

		pnlRoot = new Composite(this, SWT.NONE);
        pnlRoot.setLayout(gridLayout3);
		pnlRoot.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE,"OrderNotesBackGroundTheme");
        createPnlReturnHeader();
        lblFiller = new Label(getRootPanel(), SWT.NONE);
        createNoteHeader();
		createPanelForContactDetails();
		createPanelForNoteText();
		
	}
	
	/**
	 * @returns the width of the panel root
	 */
	public int getPnlRootWidth()
	{
		Point size = pnlRoot.computeSize(SWT.DEFAULT, SWT.DEFAULT, true);
		return size.x;
	}
	private void createPanelForNoteText() {
		
		GridData gridData5 = new GridData();
		gridData5.horizontalAlignment = GridData.BEGINNING;
		gridData5.grabExcessHorizontalSpace = false;
		gridData5.verticalAlignment = GridData.CENTER;
		GridData gridData4 = new GridData();
		gridData4.grabExcessHorizontalSpace = true;
		gridData4.verticalAlignment = GridData.CENTER;
		gridData4.horizontalSpan = 2;
		gridData4.horizontalAlignment = GridData.FILL;
		GridData gridData3 = new GridData();
		gridData3.horizontalAlignment = GridData.END;
		gridData3.grabExcessHorizontalSpace = false;
		gridData3.grabExcessVerticalSpace = true;
		gridData3.verticalAlignment = GridData.CENTER;

		
		GridLayout gridLayout5 = new GridLayout();
		gridLayout5.numColumns = 3;
		gridLayout5.marginWidth = 0;
		gridLayout5.marginHeight = 0;
		gridLayout5.horizontalSpacing = 3;
		gridLayout5.verticalSpacing = 0;
		
		pnlNoteTxt=new Composite(getRootPanel(),SWT.NONE);
		pnlNoteTxt.setData(YRCConstants.YRC_CONTROL_NAME,"pnlNoteTxt");
		pnlNoteTxt.setLayout(gridLayout5);
		pnlNoteTxt.setLayoutData(gridData4);
		pnlNoteTxt.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE,"OrderNotesBackGroundTheme");
		GridData gridData14 = new org.eclipse.swt.layout.GridData();
		gridData14.grabExcessHorizontalSpace = true;
		gridData14.horizontalIndent = 6;
		int pnlRootWidth = getPnlRootWidth();
		gridData14.widthHint = pnlRootWidth-80;

		gridData14.horizontalAlignment = GridData.FILL;
		gridData14.verticalAlignment = GridData.CENTER;
		stxNoteText = new StyledText(pnlNoteTxt, SWT.READ_ONLY); 
		stxNoteText.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE,"OrderNotesBackGroundTheme");
		stxNoteText.setData(YRCConstants.YRC_CONTROL_NAME,"stxNoteText");
		stxNoteText.setLayoutData(gridData14);
		
		
		GridData gridData15 = new org.eclipse.swt.layout.GridData();
//		gridData15.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
		gridData15.verticalAlignment = GridData.CENTER;
		gridData15.grabExcessHorizontalSpace = true;
		gridData15.horizontalIndent = 6;
		gridData15.horizontalAlignment = GridData.FILL;
		gridData15.widthHint = pnlRootWidth-80;
//		Label filler = new Label(pnlNoteTxt, SWT.NONE);
//		Label filler = new Label(pnlNoteTxt, SWT.NONE);
//		Label filler1 = new Label(pnlNoteTxt, SWT.NONE);
//		Label filler2 = new Label(pnlNoteTxt, SWT.NONE);
		gridData15.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
		gridData15.exclude=true;
		stxNoteTextMore = new StyledText(pnlNoteTxt, SWT.WRAP|SWT.READ_ONLY); 
		stxNoteTextMore.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE,"OrderNotesBackGroundTheme");
		stxNoteTextMore.setLayoutData(gridData15);
		Label lblEllipses = new Label(pnlNoteTxt, SWT.NONE);
		lblEllipses.setText(YRCPlatformUI.getString("ellipses"));
		lblEllipses.setLayoutData(gridData5);
		lblEllipses.setData(YRCConstants.YRC_CONTROL_NAME,"lblEllipses");
		lblEllipses.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE,"OrderNotesBackGroundTheme");
		stxNoteTextMore.setData(YRCConstants.YRC_CONTROL_NAME,"stxNoteTextMore");
		moreLess=new Link(pnlNoteTxt,SWT.NONE);
		moreLess.setText("<a>"+YRCPlatformUI.getString("order_notes_more_link")+"</a>"); 
		moreLess.setLayoutData(gridData3);
		moreLess
		.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
				myBehavior.toggleMoreLess();
			}
		});
		moreLess.setData(YRCConstants.YRC_CONTROL_NAME,"moreLess");
		moreLess.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE,"OrderNotesLinkBackGroundTheme");

		
	}

	public IYRCPanelHolder getPanelHolder() {
        return null;
    }

	public String getHelpId() {
		return null;
	}
	
	private void createNoteHeader() {
		GridLayout gridLayout4 = new GridLayout();
		gridLayout4.numColumns = 3;
		gridLayout4.horizontalSpacing = 5;
		gridLayout4.marginWidth = 1;
		gridLayout4.marginHeight = 1;
		GridData gridData17 = new org.eclipse.swt.layout.GridData();
		gridData17.horizontalAlignment = GridData.FILL;
		gridData17.grabExcessHorizontalSpace = true;
		gridData17.verticalAlignment = GridData.CENTER;
		GridData gridData18 = new org.eclipse.swt.layout.GridData();
		gridData18.horizontalAlignment = GridData.BEGINNING;
		gridData18.verticalAlignment = GridData.CENTER;
		gridData18.grabExcessHorizontalSpace = false;
		pnlNoteHeader = new Composite(getRootPanel(), SWT.NONE);
		pnlNoteHeader.setLayoutData(gridData17);
		pnlNoteHeader.setLayout(gridLayout4);
		GridData lblInternalOnlyNoteGridData = new org.eclipse.swt.layout.GridData();
		lblInternalOnlyNoteGridData.horizontalAlignment = GridData.BEGINNING;
		lblInternalOnlyNoteGridData.verticalAlignment = GridData.CENTER;
		lblInternalOnlyNoteGridData.grabExcessHorizontalSpace = true;
		lblInternalOnlyNoteGridData.exclude = true;
		lblInternalOnlyNoteGridData.horizontalIndent=30;
		lblInternalOnlyNoteGridData.horizontalSpan=3;
		lblInternalOnlyNote = new Label(pnlNoteHeader, SWT.NONE);
		lblInternalOnlyNote.setVisible(false);
		lblInternalOnlyNote.setText("Internal_Note");
		lblInternalOnlyNote.setLayoutData(lblInternalOnlyNoteGridData);
		lblInternalOnlyNote.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE, "InternalNoteHeader");
		lblInternalOnlyNote.setData(YRCConstants.YRC_CONTROL_NAME, "lblInternalOnlyNote");

		GridData gridDataPriority = new org.eclipse.swt.layout.GridData();
//		gridDataPriority.horizontalAlignment = GridData.FILL;
//		gridDataPriority.grabExcessHorizontalSpace = true;
		gridDataPriority.verticalAlignment = GridData.CENTER;
		lblPriority = new Label(pnlNoteHeader, SWT.NONE);
		lblPriority.setImage(YRCPlatformUI.getImage("ImportanceHigh"));
		lblPriority.setVisible(false);
		lblPriority.setLayoutData(gridDataPriority);		
		GridData gridDataSmiley = new org.eclipse.swt.layout.GridData();
//		gridDataSmiley.horizontalAlignment = GridData.FILL;
//		gridDataSmiley.grabExcessHorizontalSpace = true;
		gridDataSmiley.verticalAlignment = GridData.CENTER;
		lblSmiley = new Label(pnlNoteHeader, SWT.NONE);
		lblPriority.setLayoutData(gridDataSmiley);		
		txtNoteHeader = new Text(pnlNoteHeader, SWT.READ_ONLY);
		txtNoteHeader.setLayoutData(gridData18);
		pnlNoteHeader.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE,"OrderNotesBackGroundTheme");		
	}


	private void createPanelForContactDetails() {
		GridData gridData1 = new GridData();
		gridData1.horizontalAlignment = GridData.CENTER;
		gridData1.verticalAlignment = GridData.CENTER;
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.CENTER;
		gridData.verticalAlignment = GridData.CENTER;
		gridData.exclude = true;
		gridData1.exclude = true;
		GridLayout gridLayout8 = new GridLayout();
		gridLayout8.numColumns = 3;
		gridLayout8.marginHeight = 0;
		gridLayout8.marginWidth = 0;
		gridLayout8.horizontalSpacing  = 5;
		GridData gridData22 = new org.eclipse.swt.layout.GridData();
		gridData22.grabExcessHorizontalSpace = false;
		gridData22.grabExcessVerticalSpace = false;
		gridData22.verticalAlignment = GridData.CENTER;
		gridData22.heightHint = -1;
		gridData22.horizontalAlignment = GridData.BEGINNING;
		pnlContactDetails = new Composite(getRootPanel(), SWT.NONE);
		pnlContactDetails.setSize(new org.eclipse.swt.graphics.Point(80,20));
		pnlContactDetails.setLayoutData(gridData22);
		pnlContactDetails.setLayout(gridLayout8);
		pnlContactDetails.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE, "OrderNotesLinkBackGroundTheme");
		
		lblContactType = new Label(pnlContactDetails, SWT.NONE);
		lblContactType.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE, "OrderNotesLinkBackGroundTheme");
		lnkContactReference = new Link(pnlContactDetails, SWT.READ_ONLY);
		lnkContactReference.setLayoutData(gridData);
		lnkContactReference.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE, "OrderNotesLinkBackGroundTheme");
		lnkContactReference.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
				myBehavior.openContactLink();
			}
		});
		lblContactReference = new Label(pnlContactDetails, SWT.NONE);
		lblContactReference.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE, "NewNotesHeader1");
		lblContactReference.setLayoutData(gridData1);
	}

	/**
	 * This method initializes pnlReturnHeader	
	 *
	 */
	private void createPnlReturnHeader() {
		GridData gridData2 = new GridData();
		gridData2.grabExcessHorizontalSpace = true;
		gridData2.verticalAlignment = GridData.CENTER;
		gridData2.horizontalAlignment = GridData.FILL;
		GridLayout gridLayout = new GridLayout();
		gridLayout.horizontalSpacing = 0;
		gridLayout.marginWidth = 1;
		gridLayout.numColumns = 3;
		gridLayout.marginHeight = 1;
		gridLayout.verticalSpacing = 2;
		pnlReturnHeader = new Composite(getRootPanel(), SWT.NONE);
		pnlReturnHeader.setLayout(gridLayout);
		pnlReturnHeader.setLayoutData(gridData2);
		pnlReturnHeader.setData(YRCConstants.YRC_CONTROL_NAME,"pnlReturnHeader");
		pnlReturnHeader.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE,"OrderNotesBackGroundTheme");
		lblReeturnHeader = new Label(pnlReturnHeader, SWT.NONE);
		lblReeturnHeader.setText("order_notes_return_header");
		lblReeturnHeader.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE,
		"NewNotesHeader1");
		lblReeturnHeader.setData(YRCConstants.YRC_CONTROL_NAME,
		"lblReeturnHeader");
		lnkReturn = new Link(pnlReturnHeader, SWT.NONE);
		lnkReturn.setData(YRCConstants.YRC_CONTROL_NAME,
		"lnkReturn");
		lnkReturn.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE, "OrderNotesLinkBackGroundTheme");
		lnkReturn.setText("<a>Link</a>");
		lblBraces = new Label(pnlReturnHeader, SWT.NONE);
		lblBraces.setText(YRCPlatformUI.getString("order_notes_braces"));
		lblBraces.setData(YRCConstants.YRC_CONTROL_NAME,
		"lblBraces");
		lblBraces.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE, "NewNotesHeader1");
		lnkReturn
		.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
			}
		});
		lnkReturn.setData(YRCConstants.YRC_CONTROL_NAME,"lnkReturn");
		pnlReturnHeader.setVisible(false);
	}

	public void setLinkText(String displayString) {
		lnkReturn.setText("<a>"+displayString+"</a>");
		pnlReturnHeader.setVisible(true);
		
	}

	public void excludeReturnHeader() {
		((GridData)pnlReturnHeader.getLayoutData()).exclude=true;
		pnlReturnHeader.setVisible(false);
		
		((GridData)lblFiller.getLayoutData()).exclude=true;
		layout(true,true);
		
	}
	public void includeReturnHeader() {
		((GridData)pnlReturnHeader.getLayoutData()).exclude=false;
		pnlReturnHeader.setVisible(true);
		((GridData)lblFiller.getLayoutData()).exclude=false;
		layout(true,true);
		
	}

	public StyledText getStxNoteText() {
		return stxNoteText;
	}

	public StyledText getStxNoteTextMore() {
		return stxNoteTextMore;
	}
	
	public HBCNotesDisplayBehavior getBehavior(){
		return myBehavior;
	}
}
