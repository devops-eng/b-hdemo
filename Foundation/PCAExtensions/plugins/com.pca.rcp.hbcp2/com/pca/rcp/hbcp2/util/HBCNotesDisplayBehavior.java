/**
 * 
 */
package com.pca.rcp.hbcp2.util;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.yantra.yfc.rcp.YRCBehavior;
import com.yantra.yfc.rcp.YRCDateTimeUtils;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCXmlUtils;

/**
 * 
 * @author nsethi-tw
 *
 */
 
public class HBCNotesDisplayBehavior extends YRCBehavior  {
	/**
	 * Constructor for the behavior class. 
	 */
	HBCNotesDisplay owner;
	Element orderElement=null;
	private boolean bIsExpanded = false;
	private String displayString=null;
    public HBCNotesDisplayBehavior(Composite ownerComposite, String formId,Object inputObject) {
        super(ownerComposite, formId,inputObject);
        this.owner=(HBCNotesDisplay)ownerComposite;
        setDefaults();
    }
 
    public String evaluateDynamicBinding(String controlName, Element modelElem) {
    	if(controlName.equals("txtNoteHeader")){
    		owner.excludeReturnHeader();
    		String visibleToAll = modelElem.getAttribute("VisibleToAll");
    		if(YRCPlatformUI.equals(visibleToAll, "N")){
    			owner.lblInternalOnlyNote.setVisible(true);
        		((GridData)owner.lblInternalOnlyNote.getLayoutData()).exclude = false;
    		}    	
    		String smiley = modelElem.getAttribute("CustomerSatIndicator");
    		if(YRCPlatformUI.isVoid(smiley)){
        		((GridData)owner.lblSmiley.getLayoutData()).exclude = false;
    		}    
			return  getNoteHeader(modelElem);
		}
		return null;
	}

	public void init(){}
    
	public void setDefaults() {
		Element elemNote = (Element)getInputObject();
		setModel("Note",elemNote);
		showContactReference(elemNote);
		showPriority(elemNote);
		showHideExpandCollapseLink(elemNote);
	}

	private void showPriority(Element elemNote) {
		double priority = YRCXmlUtils.getDoubleAttribute(elemNote,"Priority");
		
		if(priority != 0){
			Label lblImg= (Label)getControl("lblPriority");
			lblImg.setVisible(true);
		}
	}

	private void showContactReference(Element elemNote) {
		if(elemNote.getAttribute("ContactType").equals("EMAIL")){
			GridData gridData = (GridData) getControl("lnkContactReference").getLayoutData();
			gridData.exclude = false;
		}else{
			GridData gridData = (GridData) getControl("lblContactReference").getLayoutData();
			gridData.exclude = false;
		}
	}

	private String getNoteHeader(Element elemNote) {
		String sNoteType = elemNote.getAttribute("NoteType");
		String expandAll="";
		if(YRCPlatformUI.isVoid(sNoteType)){
			sNoteType = elemNote.getAttribute("ReasonCode");
		}else{
			sNoteType = YRCPlatformUI.getDBString(sNoteType);
		}
		Element elemUser = YRCXmlUtils.getChildElement(elemNote,"User");
		String sContactUser = "";
		String noteContactUser = elemNote.getAttribute("ContactUser");
		if("SYSTEM".equalsIgnoreCase(noteContactUser))
			sContactUser = YRCPlatformUI.getString("SYSTEM");
		else if(!YRCPlatformUI.isVoid(elemUser))
			sContactUser = elemUser.getAttribute("Username");
		else
			sContactUser = elemNote.getAttribute("ContactUser");
		String sContactTime = null;
		if(!YRCPlatformUI.isVoid(YRCXmlUtils.getAttribute(elemNote,"ContactTime"))){
			sContactTime = HBCDateUtil.getClientFormattedDateTime(YRCXmlUtils.getDateTimeAttribute(elemNote,"ContactTime",false));
		}else{
            sContactTime = HBCDateUtil.getClientFormattedDateTime(YRCDateTimeUtils.newTimestamp());
		}
		String strIbNoteText = YRCXmlUtils.getAttribute(elemNote, "InstallBaseNoteText");
		String strNoteText = YRCXmlUtils.getAttribute(elemNote, "NoteText");
		if (!YRCPlatformUI.isVoid(strIbNoteText)
				&& YRCPlatformUI.equals(strIbNoteText, strNoteText)) {
			sContactTime = HBCDateUtil.getClientFormattedDateTime(YRCXmlUtils.getDateTimeAttribute(elemNote, "Createts",false));
		}
		String noteHeader = "";
		Node notesParent= elemNote.getParentNode().getParentNode();
		if (notesParent instanceof Element) {
			orderElement = (Element) notesParent;
			expandAll=orderElement.getAttribute("isExpandAll");
		    displayString=orderElement.getAttribute("OrderNo");
		    String documentType=orderElement.getAttribute("DocumentType");
		    if(YRCPlatformUI.equals(documentType,"0003")){
		    	 if(!YRCPlatformUI.isVoid(displayString)){
		    		 owner.setLinkText(displayString);	
		    		 owner.includeReturnHeader();
		    	 }
		    	 if(orderElement.getParentNode() instanceof Element) {
					Element orderParnetElement = (Element) orderElement.getParentNode();
					expandAll=orderParnetElement.getAttribute("isExpandAll");
		    	 }
		    }else{
		    	owner.excludeReturnHeader(); 
		    }
		    if(YRCPlatformUI.equals(notesParent.getNodeName(), "Customer")){
		    	Node noteList = elemNote.getParentNode();
		    	expandAll = ((Element)noteList).getAttribute("isExpandAll");
		    }
		}
		
		if(YRCPlatformUI.isVoid(elemNote.getAttribute("ContactUser")))
			sContactUser = elemNote.getAttribute("Createuserid");
		else
			sContactUser = elemNote.getAttribute("ContactUser");
		
		sContactUser = YRCPlatformUI.getDBString(sContactUser);
		
		if(!YRCPlatformUI.isVoid(sContactUser))
			noteHeader = YRCPlatformUI.getFormattedString("Notes_Header1",new String[]{sNoteType,sContactUser,sContactTime});
		else
			noteHeader = YRCPlatformUI.getFormattedString("Notes_Header2",new String[]{sNoteType,sContactTime});
		return noteHeader;
	}

	private void showHideExpandCollapseLink(Element elemNote) {
		if(elemNote.getAttribute("NoteText").length()<100){
			getControl("moreLess").setVisible(false);
			getControl("lblEllipses").setVisible(false);
			bIsExpanded = true;
		}else{
			showHideExpandCollapseLinkBasedOnExpandAll(elemNote);
		}
		
	}

	private void showHideExpandCollapseLinkBasedOnExpandAll(Element elemNote) {
		Node notesParent= elemNote.getParentNode().getParentNode();
		if (notesParent instanceof Element) {
			Element notesParentElement = (Element) notesParent;
			if(!YRCPlatformUI.isVoid(notesParentElement)){
				String expandAll="";
				if(notesParentElement.hasAttribute("isExpandAll"))
					expandAll = notesParentElement.getAttribute("isExpandAll");
				else{
					if(YRCPlatformUI.equals(notesParentElement.getNodeName(),"OrderLine")){
						notesParent = notesParentElement.getParentNode().getParentNode();
						if (notesParent instanceof Element) {
							notesParentElement = (Element)notesParent;
							if(!YRCPlatformUI.isVoid(notesParentElement))
								expandAll = notesParentElement.getAttribute("isExpandAll");
						}
					} else if(YRCPlatformUI.equals(notesParentElement.getNodeName(),"Customer")){
						notesParent = elemNote.getParentNode();
						if (notesParent instanceof Element) {
							notesParentElement = (Element)notesParent;
							if(!YRCPlatformUI.isVoid(notesParentElement))
								expandAll = notesParentElement.getAttribute("isExpandAll");
						}
					}
				}
				displayString=notesParentElement.getAttribute("OrderNo");
				String documentType=notesParentElement.getAttribute("DocumentType");
				if(!YRCPlatformUI.isVoid(displayString) && YRCPlatformUI.equals(documentType,"0003")){
				    	 if(notesParentElement.getParentNode() instanceof Element) {
							Element orderParnetElement = (Element) notesParentElement.getParentNode();
							expandAll=orderParnetElement.getAttribute("isExpandAll");
				    	 }
				}
				if(YRCPlatformUI.equals("Y",expandAll))
					toggleMoreLess();
				
				if(YRCPlatformUI.equals(expandAll,"Y")){
					bIsExpanded = true;
				}
			}
		}
	}

	/*
	 * This method always opens the link corresponding to Contact reference
	 * */
	public void openContactLink() {
		Element eNote = getModel("Note");
		if(eNote.getAttribute("ContactType").equals("EMAIL")){
			Program.launch("mailto:"+eNote.getAttribute("ContactReference"));
		}
	}

	public String getTextForLink() {
		return displayString;
	}
	
	private boolean isDraftOrder(Element selectedTableElement) {
		return YRCXmlUtils.getBooleanAttribute(selectedTableElement, "DraftOrderFlag");
	}

	public void toggleMoreLess() {
		StyledText stxNoteTextMore = owner.getStxNoteTextMore();
		StyledText stxNoteText = owner.getStxNoteText();
		GridData stxNoteTextMoredata = (GridData) stxNoteTextMore.getLayoutData();
		GridData stxNoteTextdata = (GridData) stxNoteText.getLayoutData();

		Link lnkMoreLess = (Link) getControl("moreLess");
		String lnkTxt = ((Link) getControl("moreLess")).getText();
		if (lnkTxt.equalsIgnoreCase("<a>"+YRCPlatformUI.getString("order_notes_more_link")+"</a>")) {
			lnkMoreLess.setText("<a>"+YRCPlatformUI.getString("order_notes_less_link")+"</a>");
			stxNoteTextdata.exclude = true;
			stxNoteTextMoredata.exclude = false;
			stxNoteText.setVisible(false);
			stxNoteTextMore.setVisible(true);
			getControl("lblEllipses").setVisible(false);
			bIsExpanded = true;
		} else {
			lnkMoreLess.setText("<a>"+YRCPlatformUI.getString("order_notes_more_link")+"</a>");
			stxNoteTextdata.exclude = false;
			stxNoteTextMoredata.exclude = true;
			stxNoteTextMore.setVisible(false);
			stxNoteText.setVisible(true);
			getControl("lblEllipses").setVisible(true);
			bIsExpanded = false;
		}
		stxNoteTextMore.setLayoutData(stxNoteTextMoredata);
		stxNoteText.setLayoutData(stxNoteTextdata);
		if (owner.getParent().getParent().getParent() instanceof HBCShowNotes)
		{
			HBCShowNotes showNotesPanel = (HBCShowNotes) owner.getParent().getParent().getParent();
			if (showNotesPanel.getParent() instanceof ScrolledComposite) {
				ScrolledComposite scrolledComposite = (ScrolledComposite) showNotesPanel.getParent();
				scrolledComposite.setContent(showNotesPanel);
				scrolledComposite.setMinSize(showNotesPanel.computeSize(SWT.DEFAULT,SWT.DEFAULT,true));
				adjustScrollbarForNotes(owner, scrolledComposite);
				scrolledComposite.getParent().layout(true, true);
			} 
		}
	}
	private void adjustScrollbarForNotes(Composite panel, ScrolledComposite scrolledComposite){
        Point pLocation = panel.getLocation();
        int selectedHeight = scrolledComposite.getOrigin().y;
        int selectedWidth = scrolledComposite.getOrigin().x;
        if(0 < scrolledComposite.getOrigin().y && (pLocation.y + panel.getSize().y) < scrolledComposite.getSize().y){
            selectedHeight = 0;
        } else if(pLocation.y < scrolledComposite.getOrigin().y ||(pLocation.y + panel.getSize().y) > (scrolledComposite.getSize().y + scrolledComposite.getOrigin().y)){
            selectedHeight = pLocation.y;     
        }
        if(0 < scrolledComposite.getOrigin().x && (pLocation.x + panel.getSize().x) < scrolledComposite.getSize().x){
            selectedWidth = 0;
        } else if(pLocation.x < scrolledComposite.getOrigin().x || (pLocation.x + panel.getSize().x) > (scrolledComposite.getSize().x + scrolledComposite.getOrigin().x)){
            selectedWidth = pLocation.x;  
        }
        
        scrolledComposite.setOrigin(selectedWidth, selectedHeight);
    }

	
	public boolean isPanelExpanded(){
		return bIsExpanded;
	}

}
