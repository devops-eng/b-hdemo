package com.pca.rcp.hbcp2.util;

/**
 * 
 * Constant Name should be in Uppercase. Attribute should start with A_
 * (example: A_ACTION) Element should start with E_ (example: E_ORDER) Value
 * should start with V_ (example: V_THREE) No constraint on Misc constants
 * (example: INBOUND_SERVICE)
 */
public interface HBCPCAConstants {

	/**
	 * Link and Buttons Resource Permissions
	 *  
	 */


	String HBC_COM_ASSOCIATE_WARRANTY = "ycdRCPHBCAW5000";
	String HBC_SOP_ASSOCIATE_WARRANTY = "SOPHBCAW5000";

	String HBC_COM_VIEW_ADD_MODIFY_CHARGES = "ycdRCPHBCVA5020";
	String HBC_SOP_VIEW_ADD_MODIFY_CHARGES = "SOPHBCVA5020";

	String HBC_COM_ADD_OPEN_BOX_ITEM = "ycdRCPHBCAO5040";
	String HBC_SOP_ADD_OPEN_BOX_ITEM = "SOPHBCAO5040";

	String HBC_COM_GIFT_REGISTRY_NUMBER = "ycdRCPHBCGR5050";
	String HBC_SOP_GIFT_REGISTRY_NUMBER = "SOPHBCGR5050";

	String HBC_COM_APPLY_DISCOUNT = "ycdRCPHBCAD5010";
	String HBC_SOP_APPLY_DISCOUNT  = "SOPHBCAD5010";

	String HBC_COM_OVERRIDE_SALES_PERSON_ID = "ycdRCPHBCOS5060";
	String HBC_SOP_OVERRIDE_SALES_PERSON_ID = "SOPHBCOS5060";

	String HBC_SOP_REVERSE_SWING = "SOPHBCOS5070";
	String HBC_COM_REVERSE_SWING = "ycdRCPHBCOS5070";

	/**
	 * 
	 */

	String FULFILLMENT_TYPE_CASH_AND_CARRY = "CASH_AND_CARRY";
	String LINE_TYPE_CASH_AND_CARRY = "CashNCarry";
	String LINE_TYPE_PICKUP_DELIVERY = "PickupDelivery";
	String LINE_TYPE_WARRANTY = "Warranty";
	String COMMON_CODE_TYPE = "HBC_ORD_DISCNT";
	String ORGANIZATION_CODE = "DEFAULT";
	String COMMON_CODE_TYPE_FOR_PRODUCT_CLASS = "PRODUCT_CLASS";
	String COMMON_CODE_TYPE_FOR_DELIVERY_METHOD = "DELIVERY_METHOD";
	String HBC_DUMMY_PS_NODE_KEY = "DC_PS_DUMMY";
	/**
	 * Constant for add modify charges action id.
	 */
	String ADD_MODIFY_CHARGES_SHARED_TASK_ACTION_ID = "com.hbc.rcp.pca.orderentry.sharedtasks.actions.HBCAddOrModifyChargesAction";

	String HBC_ITEMS_SPECIFICATION_APPLY_ACTION = "com.hbc.rcp.pca.orderentry.actions.HBCItemsSpecificationAction";
	String HBC_ITEMS_SPECIFICATION_CLOSE_ACTION = "com.hbc.rcp.pca.orderentry.actions.HBCItemsSpecificationCloseAction";
	String HBC_DISCOUNT_APPLY_ACTION = "com.hbc.rcp.pca.orderentry.actions.HBCDiscountApplyAction";
	String HBC_DISCOUNT_CLOSE_ACTION = "com.hbc.rcp.pca.orderentry.actions.HBCDiscountCloseAction";
	String HBC_GIFT_REGISTRY_APPLY_ACTION = "com.hbc.rcp.pca.orderentry.actions.HBCGiftRegistryApplyAction";
	String HBC_GIFT_REGISTRY_CLOSE_ACTION = "com.hbc.rcp.pca.orderentry.actions.HBCGiftRegistryCloseAction";

	String HBC_OPEN_BOX_ITEM_APPLY_ACTION = "com.hbc.rcp.pca.orderentry.actions.HBCOpenBoxItemApplyAction";
	String HBC_OPEN_BOX_ITEM_CLOSE_ACTION = "com.hbc.rcp.pca.orderentry.actions.HBCOpenBoxItemCloseAction";

	String VENDOR_MODEL_ATTRIBUTES = "Vendor_Model_Attributes";
	String VENDOR_MODEL_XMLATTRIBUTES = "ExtnIsVendorModel";
	String HBC_FURNITURE_WARRANTI_APPLY_ACTION = "com.hbc.rcp.pca.orderentry.actions.HBCFurnitureWarrantiApplyAction";
	String HBC_FURNITURE_WARRANTI_CLOSE_ACTION = "com.hbc.rcp.pca.orderentry.actions.HBCFurnitureWarrantiCloseAction";

	String HBC_DELIVERY_LOCATION_POPUP_ACTION ="com.hbc.rcp.pca.orderentry.actions.HBCDeliveryLocationPopupAction";
	String HBC_LOCATION_POPUP_ACTION ="com.hbc.rcp.pca.orderentry.actions.HBCLocationPopupAction";
	String HBC_DELIVERY_LOCATION_POPUP_SEARCH_ACTION ="com.hbc.rcp.pca.orderentry.actions.HBCDeliveryLocationPopupSearchAction";
	String HBC_DELIVERY_LOCATION_APPLY_ACTION ="com.hbc.rcp.pca.orderentry.actions.HBCDeliveryLocationPopupApplyAction";
	String HBC_DELIVERY_LOCATION_CLOSE_ACTION ="com.hbc.rcp.pca.orderentry.actions.HBCDeliveryLocationPopupCloseAction";

	String HBC_DELIVERY_STATUS_UPDATE_PROD_APPLY_ACTION = "com.hbc.rcp.pca.ordersummary.action.HBCDeliveryStatusUpdateProdApplyAction";
	String HBC_DELIVERY_STATUS_UPDATE_PROD_CLOSE_ACTION = "com.hbc.rcp.pca.ordersummary.action.HBCDeliveryStatusUpdateProdCloseAction";

	String HBC_DELIVERY_STATUS_UPDATE_PS_APPLY_ACTION = "com.hbc.rcp.pca.ordersummary.action.HBCDeliveryStatusUpdatePSApplyAction";
	String HBC_DELIVERY_STATUS_UPDATE_PS_CLOSE_ACTION = "com.hbc.rcp.pca.ordersummary.action.HBCDeliveryStatusUpdatePSCloseAction";
	//Cross sell
	String HBC_ALTERNATE_STORE_APPLY_ACTION = "com.hbc.rcp.pca.orderentry.actions.HBCAlternateStoreApplyAction";
	String HBC_ALTERNATE_STORE_CLOSE_ACTION = "com.hbc.rcp.pca.orderentry.actions.HBCAlternateStoreCloseAction";
	
	String HBC_SALES_PERSON_ID_APPLY_ACTION= "com.hbc.rcp.pca.returnentry.actions.HBCSalesPersonIdApplyAction";
	
	String COMMON_CODE_TYPE_Safety_Factor  ="HBC_INV_FACTOR";
	/**	 */
	String E_ORDER = "Order";
	/**	 */
	String E_ORDERLINES = "OrderLines";
	/**	 */
	String E_ORDERLINE = "OrderLine";
	/**	 */
	String E_COMMONCODE = "CommonCode";//
	/**	 */
	String A_ORDERHEADERKEY = "OrderHeaderKey";
	/**	 */
	String A_ORDERLINEKEY = "OrderLineKey";
	/**	 */
	String A_CODEVALUE = "CodeValue";

	/**	 */
	String API_GET_COMMON_CODE_LIST = "getCommonCodeList";

	String ORDER_LINE_STATUS_READY_TO_PICK="3200.200";
	//Item Search
	String ATTR_DELIVERY_METHOD="DeliveryMethod";
	String ATTR_CALLING_ORG_CODE ="CallingOrganizationCode";

	String BBY_ITEM_SEARCH_ADD_TO_ORDER_ACTION ="com.hbc.rcp.pca.itemsearch.actions.HBCItemSearchAddToOrderAction";
	String ITEM_SEARCH_ACTION_WIZARD_OPEN ="com.hbc.rcp.pca.itemsearch.actions.HBCItemSearchPanelAction";
	String CHANGE_ORDER_ITEMGROUPCODE_ATTR = "ItemGroupCode"; //Used to check if an orderline is PROD line or not.
	String ITEM_GROUP_CODE_PROVIDED_SVC = "PS";
	String ATTR_ITEM_PRIMARY_INFO ="PrimaryInformation";
	String ATTR_IS_STANDALONE_SERVICE = "IsStandaloneService";
	String ATTR_VALUE_IS_STANDALONE_SERVICE = "Y";
	String ATTR_MAX_RECORDS = "MaximumRecords";
	String ATTR_EXTN="Extn";
	String ATTR_QUICKORDERORDERABLE = "QuickOrderOrderable";
	String ATTR_VALUE_QUICKORDERORDERABLE_Y = "Y";
	String ATTR_LAST_RECORDSET = "LastRecordSet";
	String CHANGE_ORDER_ITEMGROUPCODE_PROD_ATTR = "PROD";
	String PRICE_REG_EXP="\\d+([-+.]\\d{0,2})?";

	String HOLDTYPE_POS_REQUEST ="AWAITING_POS_REQUEST";
	String HOLDTYPE_POS_TENDER ="AWAITING_POS_TENDER";
	String HOLD_STATUS_CREATED ="1100";
	double CONFIRM_WARRANTY_STATUS_ID = 1100.10;
	double CUSTOMER_KEPT_STATUS = 1100.200;
	String SERVICE_FOR_WARRANTY_DATES_CALCULATION = "HBCUpdateIBService";
	/** Added by Navin*/
	String HBC_SR_PRIORITY_COMMON_CODE = "HBC_SR_PRIORITY";
	String HBC_SR_CHANNEL_COMMON_CODE = "HBC_SR_CHANNEL";
	String HBC_SR_NOTE_TYPE = "SR_NOTE_TYPE";
	String HBC_SR_REASON_CODE_FOR_QUEUE = "Queue Change";
	String HBC_SR_REASON_CODE_FOR_USER = "User Change";
	String HBC_SR_REASON_CODE_FOR_STATUS = "Status Change";

	String ORDER_IN_TRANSIT_PICKED_STATUS = "3200.200";
	String ORDER_IN_TRANSIT_DROP_STATUS = "3200.300";

	/**Added by Kiran for ItemPick screen*/
	String HBC_IP_ORDER_LINE_STATUS_READY_TO_PICK="3200.100";
	String HBC_IP_ORDER_LINE_STATUS_PICKED="3200.200";
	String HBC_IP_TRANSACTION_ID_FOR_PICK_ORDER="PICK_ORDER.0001.ex";
	String HBC_IP_ORDER_LINE_EXTN_PICK="Pick";
	//**Added by Kiran for Delivery Update Screen */

	String HBC_DSU_PROD_ORDER_LINE_STATUS_IN_TRANSIT="3200.300";
	String HBC_DSU_PS_ORDER_LINE_STATUS_IN_CREATED="1100";

	//Added by Prabhakar for Reverse Door Swing

	String HBC_REVERSE_DOOR_SWING_APPLY_ACTION = "com.hbc.rcp.pca.orderentry.actions.HBCReverseDoorSwingApplyAction";
	String HBC_REVERSE_DOOR_SWING_CLOSE_ACTION = "com.hbc.rcp.pca.orderentry.actions.HBCReverseDoorSwingCloseAction";
	String HBC_FRENCH_LANG_CODE = "fr";
	String HBC_PROBLEM_CODE="SR_PROBLEM_CODE";
	String HBC_RESOLUTION_CODE = "SR_RESOL_CODES";

	String SHP_DELIVERY_METHOD="SHP" ;
	String ATTR_CUSTOMER_CAN_KEEP="ExtnCanCustomerKeep" ;
	String ATTR_SHIP_NODE="ShipNode" ;
	String ATTR_LINE_TYPE="LineType";
	String YCDOrderEntryAddItemPage="com.yantra.pca.ycd.rcp.tasks.orderEntry.wizardpages.YCDOrderEntryAddItemPage" ;
	String YCDReturnEntryFulfillmentSummary = "com.yantra.pca.ycd.rcp.tasks.returnEntry.wizardPages.YCDReturnEntryFulfillmentSummary" ;

	//Added by Navin for CR 296.
	String ATTR_CUST_PHONE_NUMBER = "CustomerPhoneNo";
	String ATTR_EXTN_CUST_EVENING_PHONE = "ExtnCustEveningPhone";
	String ATTR_EXTN_CUST_MOBILE_PHONE = "ExtnCustMobilePhone";
    String ATTR_FLIKE_QRY_TYPE = "FLIKE";
    
	// Added by Puja for Discounts
	String HBC_SEL_DISCOUNT_NAME = "SelectedDiscountName";
	String HBC_SEL_DISCOUNT_VALUE = "SelectedDiscountValue";
	String HBC_SEL_DISCOUNT_DENO = "SelectedDiscountDenomination";
	
	String HBC_WARRANTY_STORE_DROP_OFF_PIPELINE_ID = "HBCWarrantyStoreDropOffPipeline" ;
	String A_PIPELINE_ID = "PipelineId";
	
	//	Added by Navin for Refund of DS or PS lines
	String DUMMY_DELIVERY_RETURN_ITEM = "84854824";
	String DUMMY_SERVICE_RETURN_ITEM = "84854819";
	String DUMMY_DELIVERY_OPTION_RETURN_ITEM = "84862995";
	String LINE_TYPE_STORE_DROPOFF = "StoreDropOff";
	
	// Eco-fees Charge Name added to update ChargePerUnit in Add/Modify Charges popup
	String LINE_CHARGE_ECO_FEES = "Eco Fee";
	
	// POND Hold Type
	String POND_HOLD_TYPE = "POND_HOLD";
	
	String HBC_RETURN_FINAL_STATUS_PICKUP = "Receive";
	String HBC_RETURN_FINAL_STATUS_CANCEL = "Cancel";
	
	// Added for UAT # 87
	String HBC_APPT_LEAD_DAYS_COMMON_CODE = "APPT_LEAD_DAYS";
	
	String ORDER_RELEASED_STATUS = "3200";
	String WARRANTY_CONFIRMED_STATUS = "1100.10";
	String ORDER_SCHEDULED_STATUS = "1500";
	// charge types in HBCP2
	String HBC_SHIPPINGCHARGE = "SHIPPINGCHARGE";
	String HBC_SHIPPINGSURCHARGE = "SHIPPINGSURCHARGE";
	String HBC_DISCOUNT = "DISCOUNT";
	String VGC_ITEM = "3";
	String GIFT_BOX_ITEM = "5";
	
	String HBC_ADD_GIFT_APPLY_ACTION = "com.pca.rcp.hbcp2.orderentry.screens.actions.HBCAddGiftItemApplyAction";
	String HBC_GWP_OrderLine = "GWP_OrderLine";
	String HBC_GWP_Order = "GWP_Order";
	
	String SYSTEM_AVAILABLE = "SYSTEM_AVAILABLE";
	String SYSTEM_NAME_AMS = "AMS";
	String SYSTEM_NAME_VERTEX = "VERTEX";
	String A_CODETYPE = "CodeType";
	String A_CODESHORTDESC = "CodeShortDescription";
	
	String SHIPPING_CHARGE_NAME = "SHIPPINGCHARGE";
	String SHIPPING_CHARGE_CATEGORY = "SHIPPINGCHARGE";
	String DISCOUNT_CHARGE_NAME = "DISCOUNT";
	String DISCOUNT_CHARGE_CATEGORY = "DISCOUNT";	
	String ITEM_ID_SH1PPINGCHARGE_AMS_PROMOTIONS = "SH1PPINGCHARGE";
	
	String ITEM_HELD_STATUS="2000";
	
	String HBC_ENTERPRISE_LT = "LT";
	String HBC_ENTERPRISE_BAY = "BAY";
	
	//Defect#1856 Zero Dollar
	String PERC_ITEM_DISCOUNT = "PRCNT_ITEM_DISCOUNT";
	
	//Start of Rewards Constants
	String HBC_CREDIT_CARD_PAYMENT_GROUP = "CREDIT_CARD";
	//End of Rewards Constants
	
	//check ship to adderss
	String HBC_LT_BANNER = "LT";
	String HBC_LT_BANNER_COUNTRY = "US";
	String HBC_BAY_BANNER = "BAY";
	String HBC_BAY_BANNER_COUNTRY = "CA";

	// R3 - AllocationRuleID and FulfillmentType for Bay and LT
	String LT_ALLOCATION_RULE_ID = "LT_SCH"; 
	String BAY_ALLOCATION_RULE_ID = "BAY_SCH"; 
	String FT_TYPE_STANDARD = "FT_STANDARD";
	
	//R3 - DEF 574 - Modified to filter item search only for ECOM items
	String ITEM_STATUS = "3000"; 
	
	//R3 - Disable payment methods from COM
	String DISABLE_PAYMENT = "DISABLE_PAYMENT";
	
	//R3 - Select sourcing rule
	String SOURCING_RULE = "SOURCING_RULE";
	String APPLY_SOURCING_RULE = "N";
	String SOURCING_CHANNEL = "Call Center";
	
	String DSV_CARRIER_SERVICE = "DSV_CARRIER_SERVICE";
	
	//Defect 570 
	String NOTE_REASON_CUSTOMER_HAS_DIFFERENT_COUPON = "Customer has different coupon";
	String NOTE_REASON_CUSTOMER_PREFERENCE = "Customer Preference";
	String NOTE_REASON_DIFFERENT_METHOD_OF_PAYMENT = "Different Method of Payment";
	String NOTE_REASON_DIFFERENT_PRICE = "Different Price";
	String NOTE_REASON_OTHER = "Other";
	
	String REASON_CODE_CUSTOMER_HAS_DIFFERENT_COUPON = "HBC_MC_CC_001";
	String REASON_CODE_CUSTOMER_PREFERENCE = "HBC_MC_CC_002";
	String REASON_CODE_DIFFERENT_METHOD_OF_PAYMENT = "HBC_MC_CC_003";
	String REASON_CODE_DIFFERENT_PRICE = "HBC_MC_CC_004";
	String REASON_CODE_OTHER = "HBC_MC_CC_005";
	
	//AMS Upgrade Change:: setting of ExtnClearanceLevel flag
	String CREATE_NEW_LINE = "CREATE" ;
}
