package com.pca.rcp.hbcp2.util;

import java.text.NumberFormat;
import java.util.Locale;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.yantra.yfc.rcp.YRCApplicationContainerUtils;
import com.yantra.yfc.rcp.YRCConstants;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCSharedTaskOutput;
import com.yantra.yfc.rcp.YRCXmlUtils;

public class HBCPCAUtil {

    private static final String CALLCENTER_MODULE_ID = "ycd";
    private static final String STORE_MODULE_ID = "sop";
    private static String loginId=null;
    private static String storeId=null;
	private static String enterpriseid=null;
    
	public static boolean isStoreApplication()
	{
		String moduleId = YRCApplicationContainerUtils.getContainerModuleCode();
		if (moduleId.equals(STORE_MODULE_ID))
			return true;
		else
			return false;
	}
	
	public static boolean isCallCenterApplication()
	{
		String moduleId = YRCApplicationContainerUtils.getContainerModuleCode();
		if (moduleId.equals(CALLCENTER_MODULE_ID))
			return true;
		else
			return false;
	}
	
	public static String getCurrentStore()
	{
		Element userElement = YRCPlatformUI.getUserElement();
		if (!YRCPlatformUI.isVoid(userElement)) {
			storeId=userElement.getAttribute("ShipNode");
		}
		
		return storeId;
	}
	
	public static String getEnterpriseCode()
	{
		Element userElement = YRCPlatformUI.getUserElement();
		if (!YRCPlatformUI.isVoid(userElement)) {
			enterpriseid=userElement.getAttribute("EnterpriseCode");
		}
		
		return enterpriseid;
	}
	public static void addPanelHeader(final Composite composite, String sPanelHeader, String sPanelImageTheme,int colSpan) {

        GridData gridData = new org.eclipse.swt.layout.GridData();
        gridData.horizontalIndent = 5;
        gridData.heightHint = 17;
        gridData.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        gridData.horizontalSpan = colSpan;
        gridData.grabExcessHorizontalSpace = true;

        

        final Label lblPanelTitle = new Label(composite,SWT.NONE);
        lblPanelTitle.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE,"PanelHeader");
        lblPanelTitle.setText(YRCPlatformUI.getString(sPanelHeader));
        lblPanelTitle.setLayoutData(gridData);



        composite.addPaintListener(new PaintListener(){

            public void paintControl(PaintEvent e) {
                GC gc = new GC(composite);
                Rectangle r = composite.getClientArea();
                Rectangle r1 = lblPanelTitle.getBounds();
                gc.setBackground(YRCPlatformUI.getBackGroundColor("PanelHeader"));
                gc.fillRectangle(r.x,r.y,r.width,r1.height+1);
                gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_GRAY));      
                gc.drawLine(r.x,r1.y+r1.height,r.x+r.width-2,r1.y+r1.height);
                gc.setForeground(YRCPlatformUI.getBackGroundColor("TaskComposite"));        
                gc.drawLine(r.x+r.width-1,r.y,r.x+r.width-1,r.y+r.height-1);
                gc.drawLine(r.x,r.y+r.height-1,r.x+r.width-1,r.y+r.height-1);
                gc.drawLine(r.x,r.y,r.x+r.width,r.y);
                gc.drawLine(r.x,r.y,r.x,r.y+r.height-1);
                gc.dispose();

            }

        });

    }
	
	public static boolean isNull(String value) {

		if (null == value || 0 == value.trim().length()
				|| "null".equalsIgnoreCase(value.trim())) {
			return true;
		}
		return false;
	}
	
	/**
	 * This utillity method will show a dialog box with a OK and a CANCEL button.
	 * 
	 * @param strTitleKey - The title shown for the dialog box.
	 * @param strDescriptionKey - The description to be shown in the dialog box.
	 * @return
	 */
	public static YRCSharedTaskOutput showOptionDialogBox(String strTitleKey, String strDescriptionKey){
		String xmlStr =
			"<OptionDialog DefaultOption='Ok_Button' "+
			"DialogImage='1'>" +
			"<OptionTextList>"+
			"           <OptionText OptionTextKey= 'Ok_Button' />"+
			"           <OptionText OptionTextKey= 'Cancel_Button' />"+
			"</OptionTextList>"+
			"</OptionDialog>";		
		
		Element optionDialogInput = YRCXmlUtils.createFromString(xmlStr).getDocumentElement();
		optionDialogInput.setAttribute("TitleKey", strTitleKey);
		optionDialogInput.setAttribute("DescriptionKey", strDescriptionKey);
		
		YRCSharedTaskOutput optionDialogOutput = YRCPlatformUI.launchSharedTask("YCDOptionDialogSharedTask", optionDialogInput);
		return optionDialogOutput;
	}
	/**
	 * @param strTitleKey
	 * @param strDescriptionKey
	 * @return
	 */
	public static YRCSharedTaskOutput showOptionDialogBoxWithApply(String strTitleKey, String strDescriptionKey){
		String xmlStr =
			"<OptionDialog DefaultOption='Apply' "+
			"DialogImage='1'>" +
			"<OptionTextList>"+
			"           <OptionText OptionTextKey= 'Apply' />"+
			"           <OptionText OptionTextKey= 'Cancel/Retry' />"+
			"</OptionTextList>"+
			"</OptionDialog>";		
		
		Element optionDialogInput = YRCXmlUtils.createFromString(xmlStr).getDocumentElement();
		optionDialogInput.setAttribute("TitleKey", strTitleKey);
		optionDialogInput.setAttribute("DescriptionKey", strDescriptionKey);
		
		YRCSharedTaskOutput optionDialogOutput = YRCPlatformUI.launchSharedTask("YCDOptionDialogSharedTask", optionDialogInput);
		return optionDialogOutput;
	}
	
	public static YRCSharedTaskOutput showOptionDialogBoxWithOK(String strTitleKey, String strDescriptionKey){
		String xmlStr =
			"<OptionDialog DefaultOption='Ok_Button' "+
			"DialogImage='1'>" +
			"<OptionTextList>"+
			"           <OptionText OptionTextKey= 'Ok_Button' />"+
			"</OptionTextList>"+
			"</OptionDialog>";		
		
		Element optionDialogInput = YRCXmlUtils.createFromString(xmlStr).getDocumentElement();
		optionDialogInput.setAttribute("TitleKey", strTitleKey);
		optionDialogInput.setAttribute("DescriptionKey", strDescriptionKey);
		
		YRCSharedTaskOutput optionDialogOutput = YRCPlatformUI.launchSharedTask("YCDOptionDialogSharedTask", optionDialogInput);
		return optionDialogOutput;
	}
	public static String validateExemptId(String strTaxExemptType, String strTaxExemptionCertificate, Element extnTaxExemptionEle) {
		String response="SUCCESS";
		if(!"".equals(strTaxExemptType)){
			if(null!=extnTaxExemptionEle){
				NodeList ndlExtnTaxExemption = extnTaxExemptionEle.getElementsByTagName("CommonCode");
				for(int count=0;count<ndlExtnTaxExemption.getLength();count++){
					Element eleCommonCode = (Element)ndlExtnTaxExemption.item(count);
					if(strTaxExemptType.equals(eleCommonCode.getAttribute("CodeValue"))){
						if("Y".equals(eleCommonCode.getAttribute("CodeLongDescription"))){
							if ("".equals(strTaxExemptionCertificate.trim())) {
								response="FAIL";
							}
						}
					}
				}
			}	
		}
		return response;
	}
	
	/**
	 * This method will be invoked from the Backroom Pick and Customer Pick wizards. 
	 * The input will be a shipment details element which contains the hold information for the lines. 
	 * 
	 * @param eleShipmentDetails
	 * @return
	 */
	public static Document getProcessSplitLineInput(Element eleShipmentDetails) {
		double dblPickQty = 0.00;
		double dblShippableQty = 0.00;
		boolean boolHasAwaitingPOSHold = false;
		Document docSplitLineInput = YRCXmlUtils.createDocument("Order");
		Element eleSplitLineInput = docSplitLineInput.getDocumentElement();
		Element eleSplitLines = YRCXmlUtils.createChild(eleSplitLineInput, "OrderLines");
		NodeList ndlShipmentLine = eleShipmentDetails.getElementsByTagName("ShipmentLine");
		for(int lineCount = 0 ; lineCount < ndlShipmentLine.getLength() ; lineCount++) {
			Element eleShipmentLine = (Element) ndlShipmentLine.item(lineCount);
			dblPickQty = YRCXmlUtils.getDoubleAttribute(eleShipmentLine, "PickQuantity");
			if(dblPickQty == 0.00) {
				dblPickQty = YRCXmlUtils.getDoubleAttribute(eleShipmentLine, "PickedQty");
			}
			dblShippableQty = YRCXmlUtils.getDoubleAttribute(eleShipmentLine, "Quantity");
			if(dblShippableQty - dblPickQty > 0.00) {
				Element eleOrderLine = YRCXmlUtils.getChildElement(eleShipmentLine, "OrderLine");
				NodeList ndlHoldTypes = eleOrderLine.getElementsByTagName("OrderHoldType");
				for(int holdTypeCount = 0 ; holdTypeCount < ndlHoldTypes.getLength() ; holdTypeCount++) {
					Element eleHoldType = (Element) ndlHoldTypes.item(holdTypeCount);
					if(!YRCPlatformUI.isVoid(eleHoldType)) {
						if(HBCPCAConstants.HOLDTYPE_POS_REQUEST.equals(eleHoldType.getAttribute("HoldType")) && 
								HBCPCAConstants.HOLD_STATUS_CREATED.equals(eleHoldType.getAttribute("Status"))) {
							boolHasAwaitingPOSHold = true;
						}
					}
				}
				Element eleSplitLine = YRCXmlUtils.createChild(eleSplitLines, "OrderLine");
				eleSplitLine.setAttribute("OrderLineKey", eleShipmentLine.getAttribute("OrderLineKey"));
				if(boolHasAwaitingPOSHold) {
					eleSplitLine.setAttribute("AwaitingPOSRequestHold", "Y");
				} else {
					eleSplitLine.setAttribute("AwaitingPOSRequestHold", "N");
				}
			}
			eleSplitLineInput.setAttribute("OrderHeaderKey", eleShipmentLine.getAttribute("OrderHeaderKey"));
		}
		return docSplitLineInput;
	}
	
	// method will return number with logged in user locale
	public static String getFormattedNumber(String strPrice){
		if(!YRCPlatformUI.isVoid(strPrice)){
			try {
				NumberFormat nf = null;
				if(HBCPCAConstants.HBC_FRENCH_LANG_CODE.equalsIgnoreCase(YRCPlatformUI.getYRCUserLocale().getLanguage())) {
					nf = NumberFormat.getInstance(Locale.FRENCH);
				} else {
					nf = NumberFormat.getInstance(Locale.CANADA);
				}
				strPrice = nf.format(Double.parseDouble(strPrice));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	return strPrice;
	}
	/**
	 * while creating a customer validating and showing the alert for zipcode 
	 * after clicking the confirm button
	 * UAT Issue : 61.
	 */  
	 public static boolean validateZipCode(String zipCodeValue){
		 if( zipCodeValue.trim().length() > 7 ){
			 return false;
		 }
		 if( zipCodeValue.trim().length() == 7 ){
			 int spaceCount = 0;
			 for(int i=0 ; i< zipCodeValue.trim().length(); i++){
				 String strEachChar= Character.toString(zipCodeValue.charAt(i));
				 if(" ".equals(strEachChar)){
					 spaceCount++;
				 }
				 if(spaceCount == 2){
					 return false;
				 }
			 }			
		}
		if(zipCodeValue.indexOf(" ") != 3){
			return 	false;
		}
		return true;
}
	 
	 public static Control getControl(Composite composite, String parent, String child, boolean isFirstlevelSearch) {
			Control[] ctrl = composite.getChildren();
			if(isFirstlevelSearch){
				for (Control object : ctrl) {
					if (child.equals(object.getData("name")))
						return object;
				}
			}else{
				for(Control object:ctrl){
					if (child.equals(object.getData("name"))){
						if(parent!=null && parent.length() >0 ){
							if(parent.equals(object.getParent().getData("name"))){
								return object;
							}
						}else{
							return object;
						}
					}
					else if (object instanceof Composite) {
						Composite cmp = (Composite) object;
						Control c = getControl(cmp,parent, child,false);
						if(!YRCPlatformUI.isVoid(c))
							return c;
					}
				}		
			}
			return null;
		}
//	 Start Zero Dollar CR - To show the dialog box with value Yes / No
	 /**
		 * @param strTitleKey
		 * @param strDescriptionKey
		 * @return
		 */
	 public static YRCSharedTaskOutput showOptionDialogBoxWithYesNo(String strTitleKey, String strDescriptionKey){
			String xmlStr =
				"<OptionDialog DefaultOption='OPTION_YES' "+
				"DialogImage='1'>" +
				"<OptionTextList>"+
				"           <OptionText OptionTextKey= 'OPTION_YES' />"+
				"           <OptionText OptionTextKey= 'OPTION_NO' />"+
				"</OptionTextList>"+
				"</OptionDialog>";		
			
			Element optionDialogInput = YRCXmlUtils.createFromString(xmlStr).getDocumentElement();
			optionDialogInput.setAttribute("TitleKey", strTitleKey);
			optionDialogInput.setAttribute("DescriptionKey", strDescriptionKey);
			
			YRCSharedTaskOutput optionDialogOutput = YRCPlatformUI.launchSharedTask("YCDOptionDialogSharedTask", optionDialogInput);
			return optionDialogOutput;
		}
	 
//	 Method used to Convert the Locale Number format to Number
		public static String getFormattedCurrencyNumber(String strPrice){
			if(!YRCPlatformUI.isVoid(strPrice)){
				try {
					NumberFormat nf = null;
					if(HBCPCAConstants.HBC_FRENCH_LANG_CODE.equalsIgnoreCase(YRCPlatformUI.getYRCUserLocale().getLanguage())) {
						nf = NumberFormat.getNumberInstance(Locale.FRENCH);
					} else {
						nf = NumberFormat.getNumberInstance(Locale.CANADA);
					}
					Number price = nf.parse(strPrice);
					strPrice=String.valueOf(price);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		return strPrice;
		}
	 // End Zero Dollar
		
		/**
		 * Method call to display POP UP message with Confirm and Cancel button
		 * 
		 * @param strTitleKey
		 * @param strDescriptionKey
		 * @return optionDialogOutput
		 * 
		 * SOM changes Req#4 May'14 Release 
		 */
		public static YRCSharedTaskOutput showOptionDialogBoxWithConfirmCancel(String strTitleKey, String strDescriptionKey){
			String xmlStr =
				"<OptionDialog DefaultOption='Confirm' "+
				"DialogImage='1'>" +
				"<OptionTextList>"+
				"           <OptionText OptionTextKey= 'Confirm' />"+
				"           <OptionText OptionTextKey= 'Cancel' />"+
				"</OptionTextList>"+
				"</OptionDialog>";

			Element optionDialogInput = YRCXmlUtils.createFromString(xmlStr).getDocumentElement();
			optionDialogInput.setAttribute("TitleKey", strTitleKey);
			optionDialogInput.setAttribute("DescriptionKey", strDescriptionKey);

			YRCSharedTaskOutput optionDialogOutput = YRCPlatformUI.launchSharedTask("YCDOptionDialogSharedTask", optionDialogInput);
			return optionDialogOutput;
		}
}
