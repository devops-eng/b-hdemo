package com.pca.rcp.hbcp2.util;

import com.yantra.yfc.rcp.YRCPlatformUI;

public class HBCResourcePermissionUtil {
	
	public static boolean hasPermissionForAssociateWarranties(){
		String sApp = "";
		if(HBCPCAUtil.isStoreApplication()){
			sApp = HBCPCAConstants.HBC_SOP_ASSOCIATE_WARRANTY;
		}else{
			sApp = HBCPCAConstants.HBC_COM_ASSOCIATE_WARRANTY;
		}
		
		return YRCPlatformUI.hasPermission(sApp);
	}
	
	
	public static boolean hasPermissionForViewAddModifyCharges(){
		String sApp = "";
		if(HBCPCAUtil.isStoreApplication()){
			sApp = HBCPCAConstants.HBC_SOP_VIEW_ADD_MODIFY_CHARGES;
		}else{
			sApp = HBCPCAConstants.HBC_COM_VIEW_ADD_MODIFY_CHARGES;
		}
		
		return YRCPlatformUI.hasPermission(sApp);
	}

	
	public static boolean hasPermissionForAddOpenBoxItem(){
		String sApp = "";
		if(HBCPCAUtil.isStoreApplication()){
			sApp = HBCPCAConstants.HBC_SOP_ADD_OPEN_BOX_ITEM;
		}else{
			sApp = HBCPCAConstants.HBC_COM_ADD_OPEN_BOX_ITEM;
		}
		
		return YRCPlatformUI.hasPermission(sApp);
	}
	

	public static boolean hasPermissionForGiftRegistryNumber(){
		String sApp = "";
		if(HBCPCAUtil.isStoreApplication()){
			sApp = HBCPCAConstants.HBC_SOP_GIFT_REGISTRY_NUMBER;
		}else{
			sApp = HBCPCAConstants.HBC_COM_GIFT_REGISTRY_NUMBER;
		}
		
		return YRCPlatformUI.hasPermission(sApp);
	}

	
	public static boolean hasPermissionForApplyDiscount(){
		String sApp = "";
		if(HBCPCAUtil.isStoreApplication()){
			sApp = HBCPCAConstants.HBC_SOP_APPLY_DISCOUNT;
		}else{
			sApp = HBCPCAConstants.HBC_COM_APPLY_DISCOUNT;
		}
		
		return YRCPlatformUI.hasPermission(sApp);
	}
	
	public static boolean hasPermissionForOverrideSalesPersonID(){
		String sApp = "";
		if(HBCPCAUtil.isStoreApplication()){
			sApp = HBCPCAConstants.HBC_SOP_OVERRIDE_SALES_PERSON_ID;
		}else{
			sApp = HBCPCAConstants.HBC_COM_OVERRIDE_SALES_PERSON_ID;
		}
		
		return YRCPlatformUI.hasPermission(sApp);
	}
	
	public static boolean hasPermissionForReverseSwing(){
		String sApp = "";
		if(HBCPCAUtil.isStoreApplication()){
			sApp = HBCPCAConstants.HBC_SOP_REVERSE_SWING;
		}else{
			sApp = HBCPCAConstants.HBC_COM_REVERSE_SWING;
		}
		
		return YRCPlatformUI.hasPermission(sApp);
	}
	



	
	
	
	
	
	
	
}
