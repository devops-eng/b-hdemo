
package com.pca.rcp.hbcp2.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.yantra.yfc.rcp.IYRCComposite;
import com.yantra.yfc.rcp.IYRCPanelHolder;
import com.yantra.yfc.rcp.YRCConstants;
import com.yantra.yfc.rcp.YRCPlatformUI;

/**
 * 
 * @author nsethi-tw
 *
 */

public class HBCShowNotes extends Composite implements IYRCComposite {

	private Composite pnlRoot = null;
    private HBCShowNotesBehavior myBehavior;
	private HBCNotesDisplay noteComposite;
	private HBCCompositeListener listenerObj;
	public boolean expandAll;
	private Composite newPnlRoot;
	private Label lblNoNotes;
    public static final String FORM_ID = "com.hbc.rcp.pca.util.HBCShowNotes";

	public HBCShowNotes(Composite parent, int style,Object notesNodeList,String enterpriseCode, HBCCompositeListener listener) {
		this(parent, style, null, notesNodeList, enterpriseCode, listener);
	}
	
	public HBCShowNotes(Composite parent, int style,Object inputObject, Object notesNodeList,String enterpriseCode, HBCCompositeListener listener) {
		super(parent, style);
		listenerObj = listener;
		initialize();
        setBindingForComponents();
        myBehavior = new HBCShowNotesBehavior(this, FORM_ID,inputObject, notesNodeList,enterpriseCode);
        pnlRoot.layout(true,true);
	}
	
	private void initialize() {
		createRootPanel();
		this.setLayout(new FillLayout());
		setSize(new org.eclipse.swt.graphics.Point(300,200));
	}
	
    private void setBindingForComponents() {
    	
    }
    
    public String getFormId() {
        return FORM_ID;
    }
    
    public Composite getRootPanel() {
        return pnlRoot;
    }

	private void createRootPanel() {
		GridLayout gridLayout = new GridLayout();
		gridLayout.horizontalSpacing = 5;
		gridLayout.marginWidth = 0;
		gridLayout.marginHeight = 0;
		gridLayout.verticalSpacing = 1;
		pnlRoot = new Composite(this, SWT.NONE);
		pnlRoot.setVisible(true);
		pnlRoot.setLayout(gridLayout);
		pnlRoot.setData(YRCConstants.YRC_CONTROL_NAME,"pnlRoot");
		pnlRoot.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE, "OrderNotesBackGroundTheme");
		createNewPanelForNotes();
	}
	
	private void createNewPanelForNotes() {
		GridLayout gridLayout1 = new GridLayout();
		gridLayout1.horizontalSpacing = 5;
		gridLayout1.marginWidth = 0;
		gridLayout1.marginHeight = 0;
		gridLayout1.verticalSpacing = 1;
		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.horizontalAlignment = SWT.FILL;
		
		newPnlRoot= new Composite(pnlRoot, SWT.NONE);
		newPnlRoot.setVisible(true);
		newPnlRoot.setLayout(gridLayout1);
		newPnlRoot.setLayoutData(gridData);
		newPnlRoot.setData(YRCConstants.YRC_CONTROL_NAME,"newPnlRoot");
		newPnlRoot.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE,"TaskComposite");
		
		GridData gridData2 = new GridData();
		gridData2.grabExcessHorizontalSpace = true;
		gridData2.horizontalAlignment = SWT.FILL;
		gridData2.exclude = true;
		
		lblNoNotes = new Label(newPnlRoot,SWT.NONE);
		lblNoNotes.setData(YRCConstants.YRC_CONTROL_NAME, "lblNoNotes");
		lblNoNotes.setVisible(false);
		lblNoNotes.setAlignment(SWT.CENTER);
		lblNoNotes.setLayoutData(gridData2);
		lblNoNotes.setData(YRCConstants.YRC_CONTROL_CUSTOMTYPE,"OrderNotesWarningMessage");
		lblNoNotes.setText(YRCPlatformUI.getString("No_notes_to_Display"));
	}

	public IYRCPanelHolder getPanelHolder() {
        return null;
    }

	public String getHelpId() {
		return null;
	}

	public void showNotes(NodeList listNotes) {
		if(!YRCPlatformUI.isVoid(listNotes) && !newPnlRoot.isDisposed()){
			if(!YRCPlatformUI.isVoid(newPnlRoot)) {
				disposeNotePanels(newPnlRoot);
			}
			ArrayList sortedNotes = sortNotes(listNotes);
			//listNotes = myBehavior.updateNotesList(listNotes);
			Element elemNote;
			GridData gridData;
			int noOfNotes = sortedNotes.size();
			boolean noNotes = (noOfNotes == 0);
			lblNoNotes.setVisible(noNotes);
			((GridData)lblNoNotes.getLayoutData()).exclude = !noNotes;
			
			for (int i = 0; i < noOfNotes; i++) {
				elemNote = (Element)sortedNotes.get(i);
				if(YRCPlatformUI.isVoid(elemNote.getAttribute("NoteType"))){
					if(!YRCPlatformUI.isVoid(myBehavior)){
						elemNote=myBehavior.updateNoteTypeForNotes(elemNote);
					}
					
				}

				gridData = new GridData();
				gridData.grabExcessHorizontalSpace = true;
				gridData.horizontalAlignment = SWT.FILL;
				noteComposite = new HBCNotesDisplay(newPnlRoot,SWT.NONE,elemNote);
				noteComposite.setLayoutData(gridData);
				noteComposite.setData(YRCConstants.YRC_CONTROL_NAME,"noteComposite"+i);
				if(!YRCPlatformUI.isVoid(listenerObj)){
					noteComposite.addListener(SWT.Activate, listenerObj);
				}
			}

		}
		if(!getParent().isDisposed() && this.getParent() instanceof ScrolledComposite) {
			ScrolledComposite sc = (ScrolledComposite) this.getParent();
			sc.setContent(this);
			sc.setMinSize(this.computeSize(SWT.DEFAULT,SWT.DEFAULT,true));
			sc.getParent().layout(true, true);
		}
		newPnlRoot.layout(true, true);
		newPnlRoot.setVisible(true);
	}
	
	private ArrayList sortNotes(NodeList listNotes) {
		ArrayList children = new ArrayList();
		boolean isForIb=false;
		for (int i = 0; i < listNotes.getLength(); i++) {
            children.add(listNotes.item(i));
		}
		if (children.size() > 0) {
			Element eleInboxNote = (Element) children.get(0);
			NamedNodeMap map = eleInboxNote.getAttributes();
			for (int count = 0; count < map.getLength(); count++) {
				Node nd = map.item(count);
				if ("InstallBaseNoteText".equals(nd.getNodeName())) {
					isForIb = true;
					break;
				}
			}
		}
		if (isForIb) {
			Collections.sort(children, new YCDXMLNodeComparer(new String[] { "Createts" }));
		}else {
			Collections.sort(children, new YCDXMLNodeComparer(new String[] { "ContactTime" }));
		}
		return children;			 
	}

	private void disposeNotePanels(Composite panel) {
		if(!panel.isDisposed()){
			Control[] controls = panel.getChildren();
			for(int i=0;i<controls.length;i++){
				if(controls[i] instanceof HBCNotesDisplay)
					controls[i].dispose();
			}
		}
	}
	
    /**
     * Sorting Code.
     */
    static class YCDXMLNodeComparer implements Comparator {
        String[] attributes;
        
        YCDXMLNodeComparer(String[] attrNames) {
            attributes = attrNames;
        }
        
        public int compare(Object o1, Object o2) {
            Element ele1 = (Element)o1;
            Element ele2 = (Element)o2;
            int val;
            Comparable attr1, attr2;
            for (int i=0; i<attributes.length; i++) {
                attr1 = ele1.getAttribute(attributes[i]);
                attr2 = ele2.getAttribute(attributes[i]);
                val = (-1 )* ( attr1 == null ? ( attr2 == null ? 0 : 1 ) : ((attr2 == null) ? -1 : attr1.compareTo(attr2)));
                if (val!=0)
                    return val;
            }
            return 0;
        }
        
        public boolean equals(Object obj) {
            return super.equals(obj);
        }
        
    }
    
   public static String getAttribute(Element ele, String attrName) {
		return (ele == null) ? null : ele.getAttribute(attrName); // no need to check is attrName is void
	}


}
