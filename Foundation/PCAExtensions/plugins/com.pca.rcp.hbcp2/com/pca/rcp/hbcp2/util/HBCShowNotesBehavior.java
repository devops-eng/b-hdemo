package com.pca.rcp.hbcp2.util;

import java.util.HashMap;

import org.eclipse.swt.widgets.Composite;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.yantra.yfc.rcp.YRCBehavior;
import com.yantra.yfc.rcp.YRCPlatformUI;

/**
 * 
 * @author nsethi-tw
 *
 */
 
public class HBCShowNotesBehavior extends YRCBehavior {
	//TODO NodeList passed here from YCDOrderNotes. Is that OK?
	private HashMap mpNoteReasons = new HashMap();
	private HBCShowNotes owner = null;
	private NodeList notesList = null;//TODO try to remove this
	private String sEnterpriseCode = null;

	/**
	 * Constructor for the behavior class. 
	 */
	 
    public HBCShowNotesBehavior(Composite ownerComposite, String formId, Object notesNodeList, String enterpriseCode) {
        this(ownerComposite, formId, null, notesNodeList, enterpriseCode);
    }
    
    public HBCShowNotesBehavior(Composite ownerComposite, String formId, Object inputObject, Object notesNodeList, String enterpriseCode) {
        super(ownerComposite, formId);
        this.sEnterpriseCode = enterpriseCode;
        owner = (HBCShowNotes)ownerComposite;
        if(notesNodeList instanceof NodeList){
        	notesList = (NodeList)notesNodeList;
        }
        showNotes();
    }
    
	private void showNotes() {
		updateNotesList(notesList);
		owner.showNotes(notesList);
	}
	
	public String getContextOrganizationCode(){
		return sEnterpriseCode;
	}
	
	public NodeList updateNotesList(NodeList notesList) {
		Element e = null;
		String reasonCode = null;
		if(YRCPlatformUI.isVoid(notesList))
			return null;
		this.notesList = notesList;
		for(int i=0;i<notesList.getLength();i++){
			e = (Element)notesList.item(i);
			reasonCode = e.getAttribute("ReasonCode");
			if(mpNoteReasons.containsKey(reasonCode))
				e.setAttribute("NoteType",(String)mpNoteReasons.get(reasonCode));
		}
		return notesList;
	}
	
	public Element updateNoteTypeForNotes(Element noteElem){
		String reasonCode=noteElem.getAttribute("ReasonCode");
		if(!YRCPlatformUI.isVoid(reasonCode)){
			if(mpNoteReasons.containsKey(reasonCode)){
				noteElem.setAttribute("NoteType",(String)mpNoteReasons.get(reasonCode));
			}
		}
		return noteElem;
	}
	
	 protected void handleCommonCodeList(Element eCommonCodeList, String sCodeType){
		 if(YRCPlatformUI.equals(sCodeType, "NOTES_REASON")){
			 createMapForNoteReasons(eCommonCodeList);
			 showNotes();
		 }
	 }



	private void createMapForNoteReasons(Element elemNoteReasons) {
		NodeList nlCommonCode = elemNoteReasons.getElementsByTagName("CommonCode");
		Element elemCommonCode = null;
		for(int i=0;i<nlCommonCode.getLength();i++){
			elemCommonCode = (Element)nlCommonCode.item(i);
			mpNoteReasons .put(elemCommonCode.getAttribute("CodeValue"),elemCommonCode.getAttribute("CodeDescription"));
		}
	}
	
	/**
	 * This method initializes the behavior class.
	 */    
	public void init() {
//		getCommonCodeListForReasonCodes();
	}

}

