/** 
 * IBM and Sterling Commerce Confidential
 * OCO Source Materials
 * IBM Sterling Call Center and Store
 * (C) Copyright Sterling Commerce, an IBM Company 2010, 2011
 * The source code for this program is not published or otherwise divested of its trade secrets, irrespective of what has been deposited with the U.S. Copyright Office.
 */
package com.pca.rcp.hbcp2.util;

import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.w3c.dom.Element;

import com.yantra.yfc.rcp.IYRCTableImageURLProvider;
import com.yantra.yfc.rcp.YRCConstants;
import com.yantra.yfc.rcp.YRCLabelBindingData;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCXmlUtils;

public class HBCTableImageProvider implements IYRCTableImageURLProvider {

	private final static String sDisableImages = System.getProperty("disableimages");
	
	public HBCTableImageProvider(int itemColumnIndex, Table table){
		if(!showImage()){
			table.getColumn(itemColumnIndex).setWidth(0);
			table.getColumn(itemColumnIndex).setResizable(false);
		} else {
			if(itemColumnIndex == 0){
				table.getColumn(itemColumnIndex).setWidth(getImageWidth() + 4);
			} else {
				table.getColumn(itemColumnIndex).setWidth(getImageWidth());
			}
			
			table.getColumn(itemColumnIndex).setResizable(false);
		}
	}
	
	public static boolean showImage(){
		if(!YRCPlatformUI.isVoid(sDisableImages) && 
				(YRCPlatformUI.equals(sDisableImages.toLowerCase(), "y") || YRCPlatformUI.equals(sDisableImages.toLowerCase(), "true"))
				){
			return false;
		}
		return true;
	}
	
	public int getImageHeight() {
		
		return 32;
	}

	public String getImageUrlForColumn(Object element) {
		if(showImage())
			return getImageUrlForItem((Element)element);
		
		return null;
	}

	 public static String getImageUrlForItem(Element eItem) {
	    	if(HBCTableImageProvider.showImage()){
	    		if (!YRCPlatformUI.isVoid(eItem)) {
	    			
	    			if (!YRCPlatformUI.isVoid(eItem) && !YRCPlatformUI.isVoid(YRCXmlUtils.getChildElement(eItem, "PrimaryInformation"))) {
	    				eItem = YRCXmlUtils.getChildElement(eItem, "PrimaryInformation");
	    				if (!YRCPlatformUI.isVoid(eItem.getAttribute("ImageID")) && !YRCPlatformUI.isVoid(eItem.getAttribute("ImageLocation"))) {
	    					String sUrl = eItem.getAttribute("ImageLocation") + "/" + eItem.getAttribute("ImageID");
	    					return sUrl;
	    				}
	    			}
	    		}
	    	}
			return null;
		}
	 
	public int getImageWidth() {
		
		return 32;
	}
	
	public static void handleLabelBindings(Label label, String size, int pixels){
		YRCLabelBindingData labelBindingData = (YRCLabelBindingData) label.getData(YRCConstants.YRC_LABEL_BINDING_DEFINITION);
		labelBindingData.setDynamic(true);
		if(showImage()){
			labelBindingData.setServerImageConfiguration(size);
			labelBindingData.setServerImageBinding(true);
			labelBindingData.setServerImageHeight(pixels);
			labelBindingData.setServerImageWidth(pixels);
		} else {
			if(!YRCPlatformUI.isVoid(label)){
				label.setVisible(false);
				//((GridData)label.getLayoutData()).exclude = true;
			}
		}		
	}
	
}
