package com.pca.rcp.hbcp2.util;

import org.eclipse.swt.widgets.Table;

import com.yantra.yfc.rcp.YRCConstants;
import com.yantra.yfc.rcp.YRCTableBindingData;
import com.yantra.yfc.rcp.YRCTblClmBindingData;

public class HBCTableUtils {
	
	/**
	 * @param table - TableName in which the column is to be hidden
	 * @param clmIndexToHide - Column Index to be hidden
	 * @param clmIndexToAdd - Column Index to which the width of the hidden column to be added to
	 */
	public static void hideTableClm(Table table, int clmIndexToHide,int clmIndexToAdd ){
	
			int width = table.getColumn(clmIndexToHide).getWidth();
			table.getColumn(clmIndexToAdd).setWidth(width + table.getColumn(clmIndexToAdd).getWidth());
			table.getColumn(clmIndexToHide).setWidth(0);
			table.getColumn(clmIndexToHide).setResizable(false);
		 	
			table.getParent().redraw();
	}

	/**
	 * This method is checks for isVisible 
	 * @param table - TableName in which the column is to be shown
	 * @param clmIndexToShow - Column Index which is to be shown	
	 * @param donatingclmIndex - Column Index from which the width is to be taken
	 * @param clmWidth - Column width to be set for the column to be shown
	 */
	public static void showTableClm(Table table, int clmIndexToShow,int donatingclmIndex, int clmWidth ){
		YRCTableBindingData bindingData = (YRCTableBindingData)table.getData(YRCConstants.YRC_TABLE_BINDING_DEFINATION);
		YRCTblClmBindingData tblClmBinding[] = bindingData.getTblClmBindings();
		
		if(tblClmBinding[clmIndexToShow].isVisible()){
			if (clmWidth < table.getColumn(donatingclmIndex).getWidth()){
				table.getColumn(donatingclmIndex).setWidth(table.getColumn(donatingclmIndex).getWidth()- clmWidth);
				table.getColumn(clmIndexToShow).setWidth(clmWidth);
			} 
		}
		table.getColumn(clmIndexToShow).setResizable(true);
		table.getParent().redraw();
	}
}
