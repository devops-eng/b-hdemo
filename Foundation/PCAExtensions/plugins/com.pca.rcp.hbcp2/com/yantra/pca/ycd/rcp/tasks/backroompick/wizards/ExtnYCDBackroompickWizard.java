package com.yantra.pca.ycd.rcp.tasks.backroompick.wizards;

/**
 * Created on Jun 18,2013
 *
 */

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.pca.rcp.hbcp2.util.HBCDateUtil;
import com.pca.rcp.hbcp2.util.HBCPCAConstants;
import com.pca.rcp.hbcp2.util.HBCPCAUtil;
import com.pca.rcp.hbcp2.util.XMLUtil;
import com.yantra.pca.ycd.rcp.util.HBCSOMConstants;
import com.yantra.yfc.rcp.IYRCComposite;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCDesktopUI;
import com.yantra.yfc.rcp.YRCExtendedTableBindingData;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCSharedTaskOutput;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCWizard;
import com.yantra.yfc.rcp.YRCWizardExtensionBehavior;
import com.yantra.yfc.rcp.YRCXmlUtils;
import com.yantra.yfc.rcp.internal.YRCApiCaller;
import com.yantra.yfc.rcp.YRCSharedTask;
import com.yantra.yfc.rcp.internal.YRCUIMessages;

 /************************************************************************************
	 *  FileName : ExtnYCDCustomerPickWizard.java
	 *
	 * This Class is Linked to Customer pickup screen of SOM, All customer Pickup operation
	 * customization is done through this code.
	 *
	 * Modification Log:
	 * ------------------------------------------------------------------------------------
	 * Version Date Author Change Log
	 * ------------------------------------------------------------------------------------
	 * 0.1 07-07-2014 Rajath_Babu  Initial Draft - Initial Draft
	 * 1.0 07-07-2014 Mohit Sharma Defect 570    - Update Reason Code for Order Cancellation
	 *
	 *Copyright IBM Corp. All Rights Reserved.
	 *
	 ************************************************************************************/
public class ExtnYCDBackroompickWizard extends YRCWizardExtensionBehavior {

	String sDeliveryMethod = null;
	String sFixedNoteText = "Shortage occurred while picking from backroom. The shortage resolution was recorded as: ";
	String sShortPickReason = "N";
	Element eleSettingModel = null;
	String sbttnShrtgRsn2 = "N";
	String sShipNode = null;
	String sEnterpriseCode = null;
	//String sNoteText = null ;
	List <String>itemList = new ArrayList<String>() ;
	HashMap<String, String> hmpItemOrderLine = new HashMap<String, String>();
	HashMap<Integer, String> hmNoteTexts = new HashMap<Integer, String>();
	boolean isOrderPickup = false;
	boolean blNoShortageReasonSelected = false;
	boolean blShortageReasonByLine = false; // For defect Short pick reason code displaying null
	//Start:Defect 712
	String strDate = null ;
	String sCurrentDate = null ;
	int sDiffDays = 0;
	Document inputDocCommonCode ;
	String CarrierServiceCode = null ;
	String NoOfDays = "0" ;
	Document dCommoncodeList= null;
	//End:Defect 712
	//Modified to fix defect 812: YFS_Notes table has French Description, not English
	String sOOBReasonSelected = "1";
	//--Start : defect# 22
	Element geleShipment = null;
	//end : defect# 22
//	HashMap<String, String> hmpOrderLineShipmentLine = new HashMap<String, String>();
//	HashMap<String, String> hmpPartialCancelLines = new HashMap<String, String>();
//	Document docuMultiApi;
//	ArrayList<String> strOrderLineKeys = new ArrayList<String>();
	String sOrderHeaderKey = null;
	Element eleNewOrderLines = null;
	
	/**
	 * This method initializes the behavior class.
	 */
	public void init() {
		//TODO: Write behavior init here.
		//Modified to fix defect 812: YFS_Notes table has French Description, not English
		sOOBReasonSelected = "1";
	}

	public String getExtnNextPage(String currentPageId) {
		//TODO
		return null;
	}

	public IYRCComposite createPage(String pageIdToBeShown) {
		//TODO
		return null;
	}

	/***
	 * This method edit the message for shortage pick of item during backroom pick.
	 * defect# 22
	 */

	public void pageBeingDisposed(String pageToBeDisposed) {
		//TODO
		//--check if page is YCDBackroomPickShortageReasons and willPickLater radio button is not selected.
		if(pageToBeDisposed.equalsIgnoreCase
			("com.yantra.pca.ycd.rcp.tasks.backroompick.wizardpages.YCDBackroomPickShortageReasons")
				&& !sOOBReasonSelected.equalsIgnoreCase("2"))
		{
			//--get the model content of getShipmentDetails_output
			Element eleShipmentStatusCheck = this.getModel("getShipmentDetails_output");
			if(!XMLUtil.isVoid(eleShipmentStatusCheck)){
				String sDeliveryMethod = eleShipmentStatusCheck.getAttribute("DeliveryMethod");
				//--check if delivery method is pick
				if(!XMLUtil.isVoid(sDeliveryMethod) && sDeliveryMethod.equalsIgnoreCase("PICK")){
					String sStatus = eleShipmentStatusCheck.getAttribute("Status");
					if(!XMLUtil.isVoid(sStatus) && sStatus.equalsIgnoreCase("1100.70.06.10")){
						//--fetch global element variable which is updated in precommand method
						if(!XMLUtil.isVoid(geleShipment)){
							NodeList nShipmentDetailsList = geleShipment.getElementsByTagName("ShipmentLine");
							int iLength = nShipmentDetailsList.getLength();
							int iCounter = 0;
							Element eleShipmentLine = null;
							String sShortageQty = null;
							String sBackroomPickedQuantity = null;
							String strShipmentLineKey="";
							String strOrderLineKey="";
							for(int i=0; i<iLength; i++){
								eleShipmentLine = (Element)nShipmentDetailsList.item(i);
								sShortageQty = eleShipmentLine.getAttribute("ShortageQty");
								sBackroomPickedQuantity = eleShipmentLine.getAttribute("BackroomPickedQuantity");
								//--check if any shortage qty is present
								if(!XMLUtil.isVoid(sShortageQty) && !XMLUtil.isVoid(sBackroomPickedQuantity) 
									&& !sBackroomPickedQuantity.equalsIgnoreCase("0.0"))
								{
									//--update the message on page as "Items with inventory Shortage have been 
									//cancelled and shipment is ready "
									YRCPlatformUI.setMessage(YRCPlatformUI.getString("BACKROOM_SHORT_PICK_MESSAGE") ) ;
									break;
								}
								if(!XMLUtil.isVoid(sShortageQty) && !XMLUtil.isVoid(sBackroomPickedQuantity) 
									&& sBackroomPickedQuantity.equalsIgnoreCase("0.0"))
								{
									//--update the counter to check if all lines have zero picked qty
									iCounter ++ ;
								}	

							}
							//--if all lines have zero backroom picked quantity set message 
							//as "The Shipment is canceled because of inventory shortage"
							if(iCounter == iLength){
								YRCPlatformUI.setMessage(YRCPlatformUI.getString("BACKROOM_NO_PICK_MESSAGE")) ;
							}
						}
					}
				}
			}
		}
	}



	/**
	 * Called when a wizard page is about to be shown for the first time.
	 *
	 */
	public void initPage(String pageBeingShown) 
	{
		//TODO
		
		if(pageBeingShown.equalsIgnoreCase
			("com.yantra.pca.ycd.rcp.tasks.backroompick.wizardpages.YCDBackroomPickShortageReasons"))
		{
			blNoShortageReasonSelected = true ;
		}
	}


	/**
	 * Method for validating the text box.
	 */
	public YRCValidationResponse validateTextField(String fieldName, String fieldValue) 
	{
		// TODO Validation required for the following controls.

		// TODO Create and return a response.
		return super.validateTextField(fieldName, fieldValue) ;
	}

	/**
	 * Method for validating the combo box entry.
	 */
	public void validateComboField(String fieldName, String fieldValue) 
	{
		// TODO Validation required for the following controls.
		// TODO Create and return a response.
		super.validateComboField(fieldName, fieldValue) ;
	}

	/**
	 * Method called when a button is clicked.
	 */
	public YRCValidationResponse validateButtonClick(String fieldName) 
	{

		//Start SOM changes Req#4 May'14 Release */
		//Response type declared to block screen navigation in case of cancel button selection.
		YRCValidationResponse response = null ;
		
		//End SOM changes Req#4 May'14 Release */

		//R3.2: Defect 409- Start of modification to add reason codes during COM/SOM order/shipment cancellation//
		//Modified to fix defect 812: YFS_Notes table has French Description, not English
		/*if(fieldName.equalsIgnoreCase("bttn1ShortageResolutionReason")){
			sShortPickReason = HBCSOMConstants.BP_INVENTORY_SHORTAGE_REASON;
			sOOBReasonSelected = "1";
		}
		if(fieldName.equalsIgnoreCase("bttn2ShortageResolution")){
			sShortPickReason = HBCSOMConstants.BP_WILL_PICK_LATER_REASON;
			sOOBReasonSelected = "2";
		}
		if(fieldName.equalsIgnoreCase("extn_ItemNotFound")){
			sShortPickReason = HBCSOMConstants.ITEM_NOT_FOUND;
			sOOBReasonSelected = "3";
		}*/
		//R3.2: Defect 409- End of modification to add reason codes during COM/SOM order/shipment cancellation
		/*Defect 521 : boolean condition blNoShortageReasonSelected is used to check that
		some reason code is selected by radio button option, If not selected an pop up will be displayed 
		and the user cannot proceed without selecting proper reason code*/
		if(fieldName.equalsIgnoreCase("bttnShrtgRsn1"))
		{
			blNoShortageReasonSelected = true ;
			blShortageReasonByLine = false ;// For defect Short pick reason code displaying null
		}
		
		if(fieldName.equalsIgnoreCase("bttnShrtgRsn2"))
		{
			blNoShortageReasonSelected = false ;
			blShortageReasonByLine = true ;// For defect Short pick reason code displaying null
		}
			
		if(fieldName.equalsIgnoreCase("extn_Blank"))
		{
			sShortPickReason = HBCSOMConstants.NO_SELECTION ;
			sOOBReasonSelected = "16" ;
		}
		
		if(fieldName.equalsIgnoreCase("extn_ItemDamaged"))
		{
			sShortPickReason = HBCSOMConstants.ITEM_DAMAGED ;
			sOOBReasonSelected = "4" ;
			blNoShortageReasonSelected = false ;
		}
		/*if(fieldName.equalsIgnoreCase("extn_InsufficientQuantity")){
			sShortPickReason = HBCSOMConstants.INSUFFICIENT_QUANTITY;
		}*/
		//R3.2: Defect 409- Start of modification to add reason codes during COM/SOM order/shipment cancellation
		//R3.2: Defect 508- Start of modification to add a new reason code: Customer Cancel
		/*if(fieldName.equalsIgnoreCase("extn_CustomerCancel")){
		sShortPickReason = HBCSOMConstants.CUSTOMER_CANCEL;
		sOOBReasonSelected = "5";
		}*/
	    //R3.2: Defect 409- End of modification to add reason codes during COM/SOM order/shipment cancellation
		//R3.2: Defect 508- Start of modification to add a new reason code: Customer Cancel
		//R3.2: Defect 409- Start of modification to add reason codes during COM/SOM order/shipment cancellation
		if(fieldName.equalsIgnoreCase("extn_RTVs"))
		{
			sShortPickReason = HBCSOMConstants.RTVS;
			sOOBReasonSelected = "6";
			blNoShortageReasonSelected = false;
			}
		
		if(fieldName.equalsIgnoreCase("extn_ZeroDollar"))
		{
			sShortPickReason = HBCSOMConstants.ZERO_DOLLAR;
			sOOBReasonSelected = "7";
			blNoShortageReasonSelected = false;
			}
		
		if(fieldName.equalsIgnoreCase("extn_OnHand"))
		{
			sShortPickReason = HBCSOMConstants.ON_HAND ;
			sOOBReasonSelected = "8" ;
			blNoShortageReasonSelected = false ;
			}
		
		if(fieldName.equalsIgnoreCase("extn_Clearance"))
		{
			sShortPickReason = HBCSOMConstants.CLEARANCE ;
			sOOBReasonSelected = "9" ;
			blNoShortageReasonSelected = false ;
			}
		
		if(fieldName.equalsIgnoreCase("extn_GiftWithPurchase"))
		{
			sShortPickReason = HBCSOMConstants.GIFT_WITH_PURCHASE;
			sOOBReasonSelected = "10";
			blNoShortageReasonSelected = false;
			}
		
		if(fieldName.equalsIgnoreCase("extn_InTransit"))
		{
			sShortPickReason = HBCSOMConstants.IN_TRANSIT;
			sOOBReasonSelected = "11";
			blNoShortageReasonSelected = false;
			}
		
		/*if(fieldName.equalsIgnoreCase("extn_DuplicateOrder"))
		 * {
			sShortPickReason = HBCSOMConstants.DUPLICATE_ORDER;
			sOOBReasonSelected = "12";
			blNoShortageReasonSelected = false;
			}*/
		
		if(fieldName.equalsIgnoreCase("extn_UPCDoesNotMatch"))
		{
			sShortPickReason = HBCSOMConstants.UPC_DOES_NOT_MATCH;
			sOOBReasonSelected = "13";
			blNoShortageReasonSelected = false;
			}
		
		if(fieldName.equalsIgnoreCase("extn_Misdirect"))
		{
			sShortPickReason = HBCSOMConstants.MISDIRECT;
			sOOBReasonSelected = "14";
			blNoShortageReasonSelected = false;
			}
		
		if(fieldName.equalsIgnoreCase("extn_Other"))
		{
			sShortPickReason = HBCSOMConstants.OTHER;
			sOOBReasonSelected = "15";
			blNoShortageReasonSelected = false;
			}
		
		/*if(fieldName.equalsIgnoreCase("bttnConfirm")){
			if(sOOBReasonSelected.equalsIgnoreCase("16")){
				YRCSharedTaskOutput sharedTaskOutput = 
					HBCPCAUtil.showOptionDialogBoxWithOK
						("Info", "Please select one of the short pick reason codes from drop down") ;
				response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"") ;
				return response;
			}
		}*/

		//R3.2: Defect 409- End of modification to add reason codes during COM/SOM order/shipment cancellation

		//Start SOM changes Req#4 May'14 Release */
		//Start : Display POP UP confirming the quantity entered while doing Backroom pick on next button click.
		if(fieldName.equalsIgnoreCase("bttn"))
		{
			YRCWizard wizard=(YRCWizard)YRCDesktopUI.getCurrentPage() ;
			String strPageID=wizard.getCurrentPageID() ;
			
			if(strPageID.equals("com.yantra.pca.ycd.rcp.tasks.backroompick.wizardpages.YCDBackroomPick"))
			{
				Element eleShipmentDetails = this.getModel("getShipmentDetails_output");
				
				System.out.println("Element SHipment Details" + XMLUtil.getElementXMLString(eleShipmentDetails)) ;
				//start:Defect 712
				org.w3c.dom.Element eleShipmentOutput =
					 YRCXmlUtils.getXPathElement(eleShipmentDetails, "Shipment/ShipmentLines/ShipmentLine/Order") ;
				strDate = YRCXmlUtils.getAttribute(eleShipmentOutput,"OrderDate");			   		   
				sDiffDays = Integer.parseInt(HBCDateUtil.getDiffernceOfDateInDays(strDate));
				//org.w3c.dom.Element eleShipmentLineOutput =
					 //YRCXmlUtils.getXPathElement(eleShipmentDetials, "Shipment/ShipmentLines/ShipmentLine") ;
				CarrierServiceCode = YRCXmlUtils.getAttribute(eleShipmentDetails, "CarrierServiceCode") ;
				
				//End:Defect712
				//Call updateMessage method to update the quantity of items picked
				double dPickQuantity = updateMessage(eleShipmentDetails, "PickQuantity");
				int pickQuantity = (int)dPickQuantity;
				//Call updateMessage method to update total number of items
				double dTotalQuantity = updateMessage(eleShipmentDetails, "Quantity");
				//Call validateLineQty method to check if entered qty is less than available qty.
				boolean bCheck = validateLineQty(eleShipmentDetails);
				if(bCheck)
				{
					YRCSharedTaskOutput sharedTaskOutput = 
						HBCPCAUtil.showOptionDialogBoxWithConfirmCancel
							(YRCPlatformUI.getFormattedString("PICK_QTY_CONFIRM_TITLE", ""),
							YRCPlatformUI.getFormattedString("PICK_QTY_CONFIRM_MESSAGE","") + " (" +
									YRCPlatformUI.getFormattedString("PICK_QUANTITY","") + ": " + pickQuantity + ")") ;
					
					Element ele = sharedTaskOutput.getOutput() ;
					String strOutput = ele.getAttribute("SelectedOptionKey") ;
					
					//if cancel of cross button selected, block the screen navigation by returning error response
					if(strOutput.equals("Cancel") || XMLUtil.isVoid(strOutput))
					{
						response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"") ;
						return response ;
					}
				}
			}
		}
		//--End : Display POP UP on button click.
		//End SOM changes Req#4 May'14 Release */

		//Start 521: Short pick done even if no reason code is selected.
		//Display POP UP on button click if no reason code is selected.
		if(fieldName.equalsIgnoreCase("bttnConfirm"))
		{
			if(blNoShortageReasonSelected)
			{
			YRCSharedTaskOutput sharedTaskOutput = 
				HBCPCAUtil.showOptionDialogBoxWithOK("Error", 
					YRCPlatformUI.getString("NO_SHORTAGE_REASON_SELECTED_MESSAGE")) ;
				
			response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"") ;
			return response;
		}
			//End 521: Short pick done even if no reason code is selected. 
			//Display POP UP on button click on button click if no reason code is selected.
		}
		return super.validateButtonClick(fieldName);
	}

	/**
	 * Method call to calculate total picked quantity
	 *
	 * @param 	eleShipmentDetials - Element Shipment Details Model.
	 * @param 	sCategory - Attribute name for which quantity has to be calculated.
	 * @return	dQuantity - Total quantity of items.
	 *
	 * SOM changes Req#4 May'14 Release
	 */
	public double updateMessage(Element eleShipmentDetials, String sCategory)
	{
		double dQuantity = 0;
		double dTotalQuantity = 0;
		Element eleShipmentLine = null;
		NodeList nOrderLineList = eleShipmentDetials.getElementsByTagName("ShipmentLine");
		for(int iSL=0; iSL<nOrderLineList.getLength(); iSL++)
		{
			eleShipmentLine = (Element)nOrderLineList.item(iSL);
			dQuantity = dQuantity + Double.parseDouble(eleShipmentLine.getAttribute(sCategory)) ;
		}
		return dQuantity ;
	}

	/**
	 * Method call to validate total picked quantity is not more than available quantity
	 *
	 * @param	eleShipmentDetials - Element Shipment details Model.
	 * @return  Boolean - False (if PickQuantity is greater than Entered qty)
	 * 					  True (else wise)
	 *
	 * SOM changes Req#4 May'14 Release
	 */
	public boolean validateLineQty(Element eleShipmentDetials)
	{
		boolean bCheck = true ;
		double dQuantity = 0 ;
		double dTotalQuantity = 0 ;
		Element eleShipmentLine = null ;
		NodeList nOrderLineList = eleShipmentDetials.getElementsByTagName("ShipmentLine") ;
		
		for(int iSL=0; iSL<nOrderLineList.getLength(); iSL++)
		{
			eleShipmentLine = (Element)nOrderLineList.item(iSL) ;
			dQuantity = Double.parseDouble(eleShipmentLine.getAttribute("PickQuantity")) ;
			dTotalQuantity = Double.parseDouble(eleShipmentLine.getAttribute("Quantity")) ;
			if(dTotalQuantity < dQuantity)
			{
				bCheck = false ;
				break ;
			}
		}
		return bCheck ;
	}

	/**
	 * Method called when a link is clicked.
	 */
	public YRCValidationResponse validateLinkClick(String fieldName) 
	{
		// TODO Validation required for the following controls.

		// TODO Create and return a response.
		return super.validateLinkClick(fieldName) ;
	}

	/**
	 * Create and return the binding data for advanced table columns added to the tables.
	 */
	public YRCExtendedTableBindingData getExtendedTableBindingData(String tableName, ArrayList tableColumnNames) 
	{
		// Create and return the binding data definition for the table.
		// The default super implementation does nothing.
		return super.getExtendedTableBindingData(tableName, tableColumnNames) ;
	}

	/**
	 * This method is implemented to achieve the following in a short pick scenarios.
	 * 1. For Pick Up orders - The unavailable quantity on the shipment should be canceled.
	 * 2. For Ship order - The unavailable quantity on the shipment should be backordered.
	 * 3. For backorder - Order shouldn't be rescheduled to same node where order was backordered.
	 */
	public boolean preCommand(YRCApiContext apiContext)
	{
		YRCWizard wizard=(YRCWizard)YRCDesktopUI.getCurrentPage() ;
		String strPageID=wizard.getCurrentPageID() ;
		
		//--start : defect# 22
		//--updating global element variable with shipment details
		if(strPageID.equalsIgnoreCase
			("com.yantra.pca.ycd.rcp.tasks.backroompick.wizardpages.YCDBackroomPickShortageReasons"))
		{
			if(apiContext.getApiName().equalsIgnoreCase("changeShipment")) ;
			geleShipment = apiContext.getInputXml().getDocumentElement() ;
		}		
		//--end : defect# 22
		//Defect 1045:Start - To disallow backroom pick when BackRoom pick quantity is null.
		//An alert message will be pop up if the short keys are clicked before the SOM screen is loaded fully.
		if(strPageID.equals("com.yantra.pca.ycd.rcp.tasks.backroompick.wizardpages.YCDBackroomPick"))
		{
			apiContext.getApiName() ;
			if ("changeShipment".equals(apiContext.getApiName()))
			{
				Element eChangeShipment = apiContext.getInputXml().getDocumentElement() ;

				//Defect fix 21, 49 : For Pickup expiry Emails
				if("PICK".equalsIgnoreCase(sDeliveryMethod))
					{
					Element eleExtn = XMLUtil.getChildElement(eChangeShipment, "Extn") ;
							if(eleExtn==null)
								{
						eleExtn=XMLUtil.createChild(eChangeShipment, "Extn") ;
								}
					eleExtn.setAttribute("ExtnPickupExpiryStatus", "00") ;
					}

				NodeList nShipmentLines = eChangeShipment.getElementsByTagName("ShipmentLine") ;
				int iShipmentLines = nShipmentLines.getLength() ;
				if(iShipmentLines == 0)
				{
					HBCPCAUtil.showOptionDialogBoxWithOK
						("ATTENTION !! ", "Please wait for the screen to load and retry") ;
					return false ;
				}
			}
		}
		//Defect 1045:End - To disallow backroom pick when BackRoom pick quantity is null.

		if(strPageID.equals("com.yantra.pca.ycd.rcp.tasks.backroompick.wizardpages.YCDBackroomPickShortageReasons"))
		{
			if ("changeShipment".equals(apiContext.getApiName()))
			{
			Document dChangeShipment = apiContext.getInputXml();
			Element eChangeShipment = dChangeShipment.getDocumentElement();

			/*Start Defect586 - CancelRemovedQuantity need not be set for BOPUS Orders*/
			/*if ("PICK".equalsIgnoreCase(sDeliveryMethod)){
				eChangeShipment.removeAttribute("BackOrderRemovedQuantity");
				eChangeShipment.setAttribute("CancelRemovedQuantity", "Y");
			}*/
			/*End Defect586 - CancelRemovedQuantity need not be set for BOPUS Orders*/

				//eChangeShipment.removeAttribute("Action");
			//Defect fix 21, 49 : For Pickup expiry Emails
			if("PICK".equalsIgnoreCase(sDeliveryMethod))
			{
				Element eleExtn = XMLUtil.getChildElement(eChangeShipment, "Extn");
				if(eleExtn==null)
					{
						eleExtn=XMLUtil.createChild(eChangeShipment, "Extn");
					}
				eleExtn.setAttribute("ExtnPickupExpiryStatus", "00");
				isOrderPickup=true;
			}
			NodeList nShipmentLines = eChangeShipment.getElementsByTagName("ShipmentLine");
				int iShipmentLines = nShipmentLines.getLength();
				int iCancelledLines = 0;
			for (int j=0; j<nShipmentLines.getLength();j++ )
			{
				Node nShipmentLine = nShipmentLines.item(j);
				Element eleShipmentLine = 	(Element) nShipmentLine;

				String sShipmentLineKey = eleShipmentLine.getAttribute("ShipmentLineKey");
				String sNoteTextFromModel = getNoteTextForChangeShipment(sShipmentLineKey);
				if (!(HBCSOMConstants.BP_WILL_PICK_LATER_REASON.equalsIgnoreCase(sNoteTextFromModel)))
				{
					if("PICK".equalsIgnoreCase(sDeliveryMethod)
								&& (eleShipmentLine.getAttribute("BackroomPickedQuantity").equals("0.0")))
						{
	//							eChangeShipment.removeAttribute("BackOrderRemovedQuantity");
	//							eChangeShipment.setAttribute("CancelRemovedQuantity", "Y");
						eChangeShipment.setAttribute("BackOrderRemovedQuantity", "Y");
					}
					else
					{
					String sBackroomPickedQuantity = eleShipmentLine.getAttribute("BackroomPickedQuantity");
					eleShipmentLine.setAttribute("Quantity", sBackroomPickedQuantity);
					/*Start Defect586 - CancelRemovedQuantity need not be set for BOPUS Orders*/
					/*if ("SHP".equalsIgnoreCase(sDeliveryMethod)){
						eChangeShipment.setAttribute("BackOrderRemovedQuantity", "Y");
					}*/
						eChangeShipment.setAttribute("BackOrderRemovedQuantity", "Y");
					/*End Defect586 - CancelRemovedQuantity need not be set for BOPUS Orders*/
							
						double dBackroomPickedQuantity = Double.parseDouble(sBackroomPickedQuantity);
							
							if (dBackroomPickedQuantity == 0.0)
							{
							iCancelledLines++;
						}
	//						hmpPartialCancelLines.put(sShipmentLineKey, 
								//eleShipmentLine.getAttribute("BackroomPickedQuantity")) ;
					}
				}
				}
				if(iShipmentLines == iCancelledLines)
				{
					eChangeShipment.setAttribute("Action", "Cancel");
					removeChangeShipmentStatus(apiContext);
				}
			}
		}
		/**
		 * This method is implemented to stamp appropriate Note Text on the Order Lines
		 * based on the short pick reason code selected
		 */
		final String[] strApis = apiContext.getApiNames() ;
		Document[] docAPIInp = apiContext.getInputXmls() ;
		Element eleInp = null ;

		NodeList nOrderLines = null ;
		String strApi ;
		List <String>itemListing = new ArrayList<String>() ;
		for(int i=0 ; i < strApis.length ; i++)
		{
			strApi=strApis[i] ;
			if(strApi.equals("changeOrder"))
			{
				eleInp = docAPIInp[i].getDocumentElement() ;
				eleInp.setAttribute("Override", "Y") ;
				sOrderHeaderKey = eleInp.getAttribute("OrderHeaderKey") ;
				nOrderLines = eleInp.getElementsByTagName("OrderLine") ;
				//docuMultiApi= YRCXmlUtils.createDocument("MultiApi") ;
				//Element eMultiApi = docuMultiApi.getDocumentElement() ;
				
			for (int j=0; j<nOrderLines.getLength();j++ )
				{
					Node nOrderLine = nOrderLines.item(j) ;
					Element eleOrderLine = 	(Element) nOrderLine ;
					String sOrderLineKey = eleOrderLine.getAttribute("OrderLineKey") ;
					String sNoteToBeUsed = getNoteReason (sOrderLineKey) ;
					String sNoteText = sNoteToBeUsed ;
					hmNoteTexts.put(j, sNoteToBeUsed) ;
					
					/*Defect 570 :-start - Reason Codes for order Cancellation */
					try 
					{
						Document docOrderLinesWithReasonCode = XMLUtil.createDocument("OrderLines");
						eleNewOrderLines = docOrderLinesWithReasonCode.getDocumentElement();
						String strOrderLineKey = eleOrderLine.getAttribute("OrderLineKey");
						//String strReasonCode = eleOrderLine.getAttribute("ReasonCode");
						
						Element eleNewOrderLine = XMLUtil.createChild(eleNewOrderLines, "OrderLine");
						eleNewOrderLine.setAttribute("OrderLineKey", strOrderLineKey) ;
						eleNewOrderLine.setAttribute("ReasonCode", sNoteToBeUsed) ;	
					}
					catch (ParserConfigurationException e) 
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					/*Defect 570 :-End - Reason Codes for order Cancellation */
					
					/*Defect 521-Start: Short pick done even When no shortage reason selected 
					   Show message to select appropriate Shortage reason */
					if(sNoteToBeUsed.equalsIgnoreCase("") || sNoteToBeUsed == null)
					{
						HBCPCAUtil.showOptionDialogBoxWithOK("Error", 
							YRCPlatformUI.getString("NO_SHORTAGE_REASON_SELECTED_MESSAGE")) ;
						return false ;
					}
					//Defect 521-End: Short pick done even When no shortage reason selected 
				//code for resourcing rules , not to source to same node if backordered
					//Modified to fix defect 812: YFS_Notes table has French Description, not English
					//R3.2: Defect 508- Modified to enable OrderLineSourcingControls for PICK UP cancellations
					//R3.2: Defect 508- Modified to disable OrderLineSourcingControls for PICK UP Customer Cancellations
					if(strPageID.equals
						("com.yantra.pca.ycd.rcp.tasks.backroompick.wizardpages.YCDBackroomPickShortageReasons") &&
							!sNoteToBeUsed.equalsIgnoreCase(HBCSOMConstants.BP_WILL_PICK_LATER_REASON)
							/* && sDeliveryMethod.equalsIgnoreCase("SHP")*/
							&&!sNoteToBeUsed.equalsIgnoreCase(HBCSOMConstants.CUSTOMER_CANCEL)
							&&!sNoteToBeUsed.equalsIgnoreCase(HBCSOMConstants.GIFT_WITH_PURCHASE)
							&&!sNoteToBeUsed.equalsIgnoreCase(HBCSOMConstants.UPC_DOES_NOT_MATCH))
					{
						if(!isOrderPickup)
						{
							Element eOrderLineSrcCntrls = 
								YRCXmlUtils.createChild(eleOrderLine, "OrderLineSourcingControls") ;
							Element eOrderLineSrcCntrl = 
								YRCXmlUtils.createChild(eOrderLineSrcCntrls, "OrderLineSourcingCntrl") ;

						eOrderLineSrcCntrl.setAttribute("ACTION", "CREATE") ;
						eOrderLineSrcCntrl.setAttribute("InventoryCheckCode", "NOINV") ;
						eOrderLineSrcCntrl.setAttribute("Node", sShipNode) ;
						eOrderLineSrcCntrl.setAttribute("ReasonText", "Line is backordered from Node " + sShipNode +
							", hence ignoring that node for fulfillment for those lines") ;
						eOrderLineSrcCntrl.setAttribute("SuppressSourcing", "Y") ;
						}
						
						if (hmpItemOrderLine.containsKey(sOrderLineKey))
						{
							if(!itemListing.contains(hmpItemOrderLine.get(sOrderLineKey)))
							{
								itemListing.add(hmpItemOrderLine.get(sOrderLineKey));
							}
						}
					}
					
					NodeList nNotes = eleOrderLine.getElementsByTagName("Note") ;
					for (int k=0; k<nNotes.getLength();k++ )
					{
						Node nNote = nNotes.item(k) ;
						Element eleNote = 	(Element) nNote ;
						if (HBCSOMConstants.YCD_SHORTAGE_NOTE_REASON
							.equalsIgnoreCase(eleNote.getAttribute("ReasonCode")))
						{
							if (sNoteToBeUsed != "N")
							{
								eleNote.setAttribute("NoteText", sFixedNoteText+sNoteToBeUsed);
							}
						}
/*					}
					if(isOrderPickup)
					{
						String sShipmentLineKey=hmpOrderLineShipmentLine.get(sOrderLineKey);
						if(hmpPartialCancelLines.containsKey(sShipmentLineKey))
						{
							Element elegetOrderLineListAPI = YRCXmlUtils.createChild(eMultiApi, "API");
							elegetOrderLineListAPI.setAttribute("Name", "getOrderLineList");
							Element	eInput = YRCXmlUtils.createChild(elegetOrderLineListAPI, "Input");
							Element eOrderLineInp= YRCXmlUtils.createChild(eInput, "OrderLine");
							eOrderLineInp.setAttribute("OrigOrderLineKey", sOrderLineKey);
						}
						strOrderLineKeys.add(sOrderLineKey);
*/					}
				}

				 String strNextDate = HBCDateUtil.addDaysToSyDate(new Date(), 3) ;
				Document docMultiApi = YRCXmlUtils.createDocument("MultiApi") ;
				Element eleMultiApi = docMultiApi.getDocumentElement() ;
				Element eleInventoryNodeControlAPI = null ;
				Element eleInput = null ;
				 Element eleInventoryNodeControl = null;
				
				if(itemListing.size()>0)
				{
					for (int j=0; j<itemListing.size();j++ )
					{
						eleInventoryNodeControlAPI = YRCXmlUtils.createChild(eleMultiApi, "API");
						eleInventoryNodeControlAPI.setAttribute("Name", "manageInventoryNodeControl");
						eleInput = YRCXmlUtils.createChild(eleInventoryNodeControlAPI, "Input");
						eleInventoryNodeControl = YRCXmlUtils.createChild(eleInput, "InventoryNodeControl");
						eleInventoryNodeControl.setAttribute("InvPictureIncorrectTillDate", strNextDate);
						eleInventoryNodeControl.setAttribute("InventoryPictureCorrect", "N");
						eleInventoryNodeControl.setAttribute("ItemID",itemListing.get(j).toString());
						eleInventoryNodeControl.setAttribute("Node", sShipNode);
						eleInventoryNodeControl.setAttribute("OrganizationCode", sEnterpriseCode);
						eleInventoryNodeControl.setAttribute("UnitOfMeasure", "EACH");
				 }
			  }
				YRCApiContext context = new YRCApiContext() ;
	            context.setApiName("itemNodeControl") ;
	            context.setFormId(this.getFormId()) ;
	            context.setInputXml(docMultiApi) ;
	            YRCApiCaller apiCaller1 = new YRCApiCaller(context, true) ;
	            apiCaller1.invokeApi() ;
			}
		}
		return true ;
	}

	/*private String getDeformattedDateTimeForEndOfDay(Date date) {
		// TODO Auto-generated method stub
		return null;
	}*/


	public void postSetModel(String model) 
	{
		/**
		 * This model is used to check the DeliveryMethod
		 */
		if(model.equals("getShipmentDetails_output"))
		{
			Element eleShipmentDetails = this.getModel("getShipmentDetails_output");
			if (eleShipmentDetails != null) {
				sDeliveryMethod = eleShipmentDetails.getAttribute("DeliveryMethod");
				sShipNode = eleShipmentDetails.getAttribute("ShipNode");
				sEnterpriseCode = eleShipmentDetails.getAttribute("EnterpriseCode");
			//	Element eleShipmentLines = YRCXmlUtils.createChild(eleShipmentDetails, "ShipmentLines");
				NodeList nShipmentLines =  eleShipmentDetails.getElementsByTagName("ShipmentLine");
				String strItemId=null;
				Element eleShipmentLine = null;
				String strOrderLineKey="";
				String strShipmentLineKey="";
				for (int j=0; j<nShipmentLines.getLength();j++ )
				{
					eleShipmentLine=(Element)nShipmentLines.item(j);
					strOrderLineKey= eleShipmentLine.getAttribute("OrderLineKey");
					strItemId= eleShipmentLine.getAttribute("ItemID");

					hmpItemOrderLine.put(strOrderLineKey, strItemId);

/*					//Fix for Cancellation of Pickup Orders on ShortPick

					strShipmentLineKey=eleShipmentLine.getAttribute("ShipmentLineKey");
					strOrderLineKey=eleShipmentLine.getAttribute("OrderLineKey");
					hmpOrderLineShipmentLine.put(strOrderLineKey, strShipmentLineKey);
*/				}
			}
		}

		/**
		 * This model is modified to display custom short pick reasons on SOM UI
		 */
		if(model.equals("getResolutionList_output"))
		{
			Element eleCommonCodeList = this.getModel("getResolutionList_output");

			if (eleCommonCodeList != null) 
			{
				//R3.2: Defect 409- Start of modification to add reason codes during COM/SOM order/shipment cancellation
				/*Element eleItemNotFound = YRCXmlUtils.createChild(eleCommonCodeList, "CommonCode");
				eleItemNotFound.setAttribute("CodeShortDescription", HBCSOMConstants.ITEM_NOT_FOUND);
				eleItemNotFound.setAttribute("CodeType", HBCSOMConstants.YCD_SHORTAGE_REASON_CODE_TYPE);
				eleItemNotFound.setAttribute("CodeValue", HBCSOMConstants.ITEM_NOT_FOUND_CODE_VALUE);*/
				//R3.2: Defect 409- End of modification to add reason codes during COM/SOM order/shipment cancellation

				Element eleNOSelection = YRCXmlUtils.createChild(eleCommonCodeList, "CommonCode");
				eleNOSelection.setAttribute("CodeShortDescription", HBCSOMConstants.NO_SELECTION);
				eleNOSelection.setAttribute("CodeType", HBCSOMConstants.YCD_SHORTAGE_REASON_CODE_TYPE);
				eleNOSelection.setAttribute("CodeValue", HBCSOMConstants.NO_SELECTION_CODE_VALUE);

				Element eleItemDamaged = YRCXmlUtils.createChild(eleCommonCodeList, "CommonCode");
				eleItemDamaged.setAttribute("CodeShortDescription", HBCSOMConstants.ITEM_DAMAGED);
				eleItemDamaged.setAttribute("CodeType", HBCSOMConstants.YCD_SHORTAGE_REASON_CODE_TYPE);
				eleItemDamaged.setAttribute("CodeValue", HBCSOMConstants.ITEM_DAMAGED_CODE_VALUE);

				/*Element eleInsufficientQuantity = YRCXmlUtils.createChild(eleCommonCodeList, "CommonCode");
				eleInsufficientQuantity.setAttribute("CodeShortDescription", HBCSOMConstants.INSUFFICIENT_QUANTITY);
				eleInsufficientQuantity.setAttribute("CodeType", HBCSOMConstants.YCD_SHORTAGE_REASON_CODE_TYPE);
				eleInsufficientQuantity.setAttribute("CodeValue", HBCSOMConstants.INSUFFICIENT_QUANTITY_CODE_VALUE);*/

				//R3.2: Defect 409- Start of modification to add reason codes during COM/SOM order/shipment cancellation
				//R3.2: Defect 508- Start of modification to add a new reason code: Customer Cancel
				/*Element eleCustomerCancel = YRCXmlUtils.createChild(eleCommonCodeList, "CommonCode");
				eleCustomerCancel.setAttribute("CodeShortDescription", HBCSOMConstants.CUSTOMER_CANCEL);
				eleCustomerCancel.setAttribute("CodeType", HBCSOMConstants.YCD_SHORTAGE_REASON_CODE_TYPE);
				eleCustomerCancel.setAttribute("CodeValue", HBCSOMConstants.CUSTOMER_CANCEL_CODE_VALUE);*/
				//R3.2: Defect 508- End of modification to add a new reason code: Customer Cancel
				//R3.2: Defect 409- Start of modification to add reason codes during COM/SOM order/shipment cancellation

				//R3.2: Defect 409- Start of modification to add reason codes during COM/SOM order/shipment cancellation
				Element eleRTVs = YRCXmlUtils.createChild(eleCommonCodeList, "CommonCode");
				eleRTVs.setAttribute("CodeShortDescription", HBCSOMConstants.RTVS);
				eleRTVs.setAttribute("CodeType", HBCSOMConstants.YCD_SHORTAGE_REASON_CODE_TYPE);
				eleRTVs.setAttribute("CodeValue", HBCSOMConstants.RTVS_CODE_VALUE);

				Element eleZeroDollar = YRCXmlUtils.createChild(eleCommonCodeList, "CommonCode");
				eleZeroDollar.setAttribute("CodeShortDescription", HBCSOMConstants.ZERO_DOLLAR);
				eleZeroDollar.setAttribute("CodeType", HBCSOMConstants.YCD_SHORTAGE_REASON_CODE_TYPE);
				eleZeroDollar.setAttribute("CodeValue", HBCSOMConstants.ZERO_DOLLAR_CODE_VALUE);

				Element eleOnHand = YRCXmlUtils.createChild(eleCommonCodeList, "CommonCode");
				eleOnHand.setAttribute("CodeShortDescription", HBCSOMConstants.ON_HAND);
				eleOnHand.setAttribute("CodeType", HBCSOMConstants.YCD_SHORTAGE_REASON_CODE_TYPE);
				eleOnHand.setAttribute("CodeValue", HBCSOMConstants.ON_HAND_CODE_VALUE);

				Element eleClearance = YRCXmlUtils.createChild(eleCommonCodeList, "CommonCode");
				eleClearance.setAttribute("CodeShortDescription", HBCSOMConstants.CLEARANCE);
				eleClearance.setAttribute("CodeType", HBCSOMConstants.YCD_SHORTAGE_REASON_CODE_TYPE);
				eleClearance.setAttribute("CodeValue", HBCSOMConstants.CLEARANCE_CODE_VALUE);

				Element eleGWP = YRCXmlUtils.createChild(eleCommonCodeList, "CommonCode");
				eleGWP.setAttribute("CodeShortDescription", HBCSOMConstants.GIFT_WITH_PURCHASE);
				eleGWP.setAttribute("CodeType", HBCSOMConstants.YCD_SHORTAGE_REASON_CODE_TYPE);
				eleGWP.setAttribute("CodeValue", HBCSOMConstants.GIFT_WITH_PURCHASE_CODE_VALUE);

				Element eleInTransit = YRCXmlUtils.createChild(eleCommonCodeList, "CommonCode");
				eleInTransit.setAttribute("CodeShortDescription", HBCSOMConstants.IN_TRANSIT);
				eleInTransit.setAttribute("CodeType", HBCSOMConstants.YCD_SHORTAGE_REASON_CODE_TYPE);
				eleInTransit.setAttribute("CodeValue", HBCSOMConstants.IN_TRANSIT_CODE_VALUE);

				/*Element eleDupOrder = YRCXmlUtils.createChild(eleCommonCodeList, "CommonCode");
				eleDupOrder.setAttribute("CodeShortDescription", HBCSOMConstants.DUPLICATE_ORDER);
				eleDupOrder.setAttribute("CodeType", HBCSOMConstants.YCD_SHORTAGE_REASON_CODE_TYPE);
				eleDupOrder.setAttribute("CodeValue", HBCSOMConstants.DUPLICATE_ORDER_CODE_VALUE);*/

				Element eleUPCDoesNotMatch = YRCXmlUtils.createChild(eleCommonCodeList, "CommonCode");
				eleUPCDoesNotMatch.setAttribute("CodeShortDescription", HBCSOMConstants.UPC_DOES_NOT_MATCH);
				eleUPCDoesNotMatch.setAttribute("CodeType", HBCSOMConstants.YCD_SHORTAGE_REASON_CODE_TYPE);
				eleUPCDoesNotMatch.setAttribute("CodeValue", HBCSOMConstants.UPC_DOES_NOT_MATCH_CODE_VALUE);

				Element eleMisdirect = YRCXmlUtils.createChild(eleCommonCodeList, "CommonCode");
				eleMisdirect.setAttribute("CodeShortDescription", HBCSOMConstants.MISDIRECT);
				eleMisdirect.setAttribute("CodeType", HBCSOMConstants.YCD_SHORTAGE_REASON_CODE_TYPE);
				eleMisdirect.setAttribute("CodeValue", HBCSOMConstants.MISDIRECT_CODE_VALUE);

				Element eleOther = YRCXmlUtils.createChild(eleCommonCodeList, "CommonCode");
				eleOther.setAttribute("CodeShortDescription", HBCSOMConstants.OTHER);
				eleOther.setAttribute("CodeType", HBCSOMConstants.YCD_SHORTAGE_REASON_CODE_TYPE);
				eleOther.setAttribute("CodeValue", HBCSOMConstants.OTHER_CODE_VALUE);

				//R3.2: Defect 409- End of modification to add reason codes during COM/SOM order/shipment cancellation

				//System.out.println("In "+YRCXmlUtils.getString(eleCommonCodeList));
				//R3.2: Defect 409- Start of modification to add reason codes during COM/SOM order/shipment cancellation
				//R3.2: Defect 409- End of modification to add reason codes during COM/SOM order/shipment cancellation

				this.repopulateModel("getResolutionList_output");
			}

			NodeList nCommonCodeList =  eleCommonCodeList.getElementsByTagName("CommonCode");
			for (int l=0; l<=nCommonCodeList.getLength();l++ )
			{
				Element eleCommonCode=(Element)nCommonCodeList.item(0);
				if ("BP_Will_Pick_Later".equals(eleCommonCode.getAttribute("CodeValue"))||
						"BP_Inventory_Shortage".equals(eleCommonCode.getAttribute("CodeValue"))){
						eleCommonCodeList.removeChild(eleCommonCode);
					}
			}
				this.repopulateModel("getResolutionList_output");
		}
		if(model.equals("ShortageResolution_input"))
		{
			Element eleShipment = this.getModel("ShortageResolution_input");
			Element eleShipmentLines = YRCXmlUtils.getXPathElement(eleShipment, "Shipment/ShipmentLines");
			 NodeList nShipmentLines = eleShipmentLines.getElementsByTagName("ShipmentLine");
			for (int l=0; l<nShipmentLines.getLength();l++ )
			{
				Element eleShipmentLine=(Element)nShipmentLines.item(l);
				if ("BP_Inventory_Shortage".equalsIgnoreCase(eleShipmentLine.getAttribute("NoteText")))
				{
							eleShipmentLine.removeAttribute("NoteText");
				}
			}
			this.repopulateModel("ShortageResolution_input");
		}
	}

	/**
	 * This method is implemented to add appropriate Note Text on the Order Lines
	 * based on the short pick reasons selected
	 */
	private String getNoteReason (String sOrderLineKey) 
	{
		String sNoteReasonFromModel = null ;
		Element eleSelectedAllLinesOption = this.getModel("ShortageResolution_input") ;
		NodeList nShipmentLines = eleSelectedAllLinesOption.getElementsByTagName("ShipmentLine") ;
		for (int i=0; i<nShipmentLines.getLength();i++ )
		{
			Node nShipmentLine = nShipmentLines.item(i);
			Element eleShipmentLine = (Element) nShipmentLine;
			if (sOrderLineKey.equalsIgnoreCase(eleShipmentLine.getAttribute("OrderLineKey")))
			{
				String sNoteTextInModel = "N" ;
				sNoteTextInModel = eleShipmentLine.getAttribute("NoteText") ;
				
				String test = XMLUtil.getElementXMLString(eleShipmentLine) ;

				//if(!(sNoteTextInModel.isEmpty()))
				//Modified to fix defect 812: YFS_Notes table has French Description, not English
				if(sNoteTextInModel!= null)
				{
					//R3.2: Defect 409- Start of modification to add reason codes during COM/SOM 
					//order/shipment cancellation
					/*if(sOOBReasonSelected.equalsIgnoreCase("1")){
						sNoteReasonFromModel = HBCSOMConstants.BP_INVENTORY_SHORTAGE_REASON;
					}
					if(sOOBReasonSelected.equalsIgnoreCase("2")){
						sNoteReasonFromModel = HBCSOMConstants.BP_WILL_PICK_LATER_REASON;
					}
					if(sOOBReasonSelected.equalsIgnoreCase("3")){
						sNoteReasonFromModel = HBCSOMConstants.ITEM_NOT_FOUND;
					}*/
					//R3.2: Defect 409- End of modification to add reason codes during COM/SOM 
					//order/shipment cancellation
					
					if(sOOBReasonSelected.equalsIgnoreCase("16"))
					{
						sNoteReasonFromModel = HBCSOMConstants.NO_SELECTION ;
					}
					
					if(sOOBReasonSelected.equalsIgnoreCase("4"))
					{
						sNoteReasonFromModel = HBCSOMConstants.ITEM_DAMAGED ;
					}
					//R3.2: Defect 409- Start of modification to add reason codes during COM/SOM 
					//order/shipment cancellation
					
					//R3.2: Defect 508- Start of modification to add a new reason code: Customer Cancel
					/*if(sOOBReasonSelected.equalsIgnoreCase("5")){
						sNoteReasonFromModel = HBCSOMConstants.CUSTOMER_CANCEL;
					}*/
					//R3.2: Defect 508- End of modification to add a new reason code: Customer Cancel
					//R3.2: Defect 409- End of modification to add reason codes during COM/SOM 
					//order/shipment cancellation

					//R3.2: Defect 409- Start of modification to add reason codes during COM/SOM 
					//order/shipment cancellation

					if(sOOBReasonSelected.equalsIgnoreCase("6"))
					{
						sNoteReasonFromModel = HBCSOMConstants.RTVS ;
					}
					
					if(sOOBReasonSelected.equalsIgnoreCase("7"))
					{
						sNoteReasonFromModel = HBCSOMConstants.ZERO_DOLLAR ;
					}
					
					if(sOOBReasonSelected.equalsIgnoreCase("8"))
					{
						sNoteReasonFromModel = HBCSOMConstants.ON_HAND ;
					}
					
					if(sOOBReasonSelected.equalsIgnoreCase("9"))
					{
						sNoteReasonFromModel = HBCSOMConstants.CLEARANCE ;
					}
					
					if(sOOBReasonSelected.equalsIgnoreCase("10"))
					{
						sNoteReasonFromModel = HBCSOMConstants.GIFT_WITH_PURCHASE ;
					}
					
					if(sOOBReasonSelected.equalsIgnoreCase("11"))
					{
						sNoteReasonFromModel = HBCSOMConstants.IN_TRANSIT ;
					}
					
					/*if(sOOBReasonSelected.equalsIgnoreCase("12")){
						sNoteReasonFromModel = HBCSOMConstants.DUPLICATE_ORDER;
					}*/
					
					if(sOOBReasonSelected.equalsIgnoreCase("13"))
					{
						sNoteReasonFromModel = HBCSOMConstants.UPC_DOES_NOT_MATCH ;
					}
					
					if(sOOBReasonSelected.equalsIgnoreCase("14"))
					{
						sNoteReasonFromModel = HBCSOMConstants.MISDIRECT ;
					}
					
					if(sOOBReasonSelected.equalsIgnoreCase("15"))
					{
						sNoteReasonFromModel = HBCSOMConstants.OTHER ;
					}
					//R3.2: Defect 409- End of modification to add reason codes during COM/SOM 
					//order/shipment cancellation

					//R3.2: Defect 409- Start of modification to add reason codes during COM/SOM 
					//order/shipment cancellation
				   /*if(sNoteTextInModel.equalsIgnoreCase(HBCSOMConstants.ITEM_NOT_FOUND_CODE_VALUE)){
						sNoteReasonFromModel = HBCSOMConstants.ITEM_NOT_FOUND;
					}*/
					//R3.2: Defect 409- End of modification to add reason codes during COM/SOM 
					//order/shipment cancellation
					/* For defect Short pick reason code displaying null: check if shortage reason 
					 *differs by line is selected,
					then set value from drop down*/
					if(blShortageReasonByLine)
					{
						if(sNoteTextInModel.equalsIgnoreCase(HBCSOMConstants.NO_SELECTION_CODE_VALUE))
						{
							sNoteReasonFromModel = HBCSOMConstants.NO_SELECTION ;
					}
						
						if(sNoteTextInModel.equalsIgnoreCase(HBCSOMConstants.ITEM_DAMAGED_CODE_VALUE))
						{
							sNoteReasonFromModel = HBCSOMConstants.ITEM_DAMAGED ;
					}
					/*if(sNoteTextInModel.equalsIgnoreCase(HBCSOMConstants.INSUFFICIENT_QUANTITY_CODE_VALUE)){
						sNoteReasonFromModel = HBCSOMConstants.INSUFFICIENT_QUANTITY;
					}*/
						//R3.2: Defect 409- Start of modification to add reason codes during COM/SOM 
						//order/shipment cancellation
					//R3.2: Defect 508- Start of modification to add a new reason code: Customer Cancel
					/*if(sNoteTextInModel.equalsIgnoreCase(HBCSOMConstants.CUSTOMER_CANCEL_CODE_VALUE)){
					sNoteReasonFromModel = HBCSOMConstants.CUSTOMER_CANCEL;
					}*/
					//R3.2: Defect 508- End of modification to add a new reason code: Customer Cancel

					/*if(sNoteTextInModel.equalsIgnoreCase("BP_Will_Pick_Later")){
						sNoteReasonFromModel = HBCSOMConstants.BP_WILL_PICK_LATER_REASON;
					}
					if(sNoteTextInModel.equalsIgnoreCase("BP_Inventory_Shortage")){
						sNoteReasonFromModel = HBCSOMConstants.BP_INVENTORY_SHORTAGE_REASON;
					}*/
						//R3.2: Defect 409- End of modification to add reason codes during COM/SOM 
						//order/shipment cancellation
						//R3.2: Defect 409- Start of modification to add reason codes during COM/SOM 
						//order/shipment cancellation
						
						if(sNoteTextInModel.equalsIgnoreCase(HBCSOMConstants.RTVS_CODE_VALUE))
						{
							sNoteReasonFromModel = HBCSOMConstants.RTVS ;
					}
						
						if(sNoteTextInModel.equalsIgnoreCase(HBCSOMConstants.ZERO_DOLLAR_CODE_VALUE))
						{
							sNoteReasonFromModel = HBCSOMConstants.ZERO_DOLLAR ;
					}
						
						if(sNoteTextInModel.equalsIgnoreCase(HBCSOMConstants.ON_HAND_CODE_VALUE))
						{
							sNoteReasonFromModel = HBCSOMConstants.ON_HAND ;
					}
						
						if(sNoteTextInModel.equalsIgnoreCase(HBCSOMConstants.CLEARANCE_CODE_VALUE))
						{
							sNoteReasonFromModel = HBCSOMConstants.CLEARANCE ;
					}
						
						if(sNoteTextInModel.equalsIgnoreCase(HBCSOMConstants.GIFT_WITH_PURCHASE_CODE_VALUE))
						{
							sNoteReasonFromModel = HBCSOMConstants.GIFT_WITH_PURCHASE ;
					}
						
						if(sNoteTextInModel.equalsIgnoreCase(HBCSOMConstants.IN_TRANSIT_CODE_VALUE))
						{
							sNoteReasonFromModel = HBCSOMConstants.IN_TRANSIT ;
					}
						
					/*if(sNoteTextInModel.equalsIgnoreCase(HBCSOMConstants.DUPLICATE_ORDER_CODE_VALUE)){
						sNoteReasonFromModel = HBCSOMConstants.DUPLICATE_ORDER;
					}*/
						
						if(sNoteTextInModel.equalsIgnoreCase(HBCSOMConstants.UPC_DOES_NOT_MATCH_CODE_VALUE))
						{
							sNoteReasonFromModel = HBCSOMConstants.UPC_DOES_NOT_MATCH ;
					}
						
						if(sNoteTextInModel.equalsIgnoreCase(HBCSOMConstants.MISDIRECT_CODE_VALUE))
						{
							sNoteReasonFromModel = HBCSOMConstants.MISDIRECT ;
					}
						
						if(sNoteTextInModel.equalsIgnoreCase(HBCSOMConstants.OTHER_CODE_VALUE))
						{
							sNoteReasonFromModel = HBCSOMConstants.OTHER ;
					}
					}
					//R3.2: Defect 409- End of modification to add reason codes during COM/SOM 
					//order/shipment cancellation
				}
				else
				{
					sNoteReasonFromModel = sShortPickReason ;
				}
			}
		}
		return sNoteReasonFromModel ;
	}

	/**
	 * This method is implemented to return appropriate Note Text based on the
	 * short pick reasons selected so that quantity on the shipment can be set
	 * accordingly
	 */
	private String getNoteTextForChangeShipment (String sShipmentLineKey) 
	{
		String sNoteReasonFromModel = null;
		Element eleSelectedAllLinesOption = this.getModel("ShortageResolution_input");
		NodeList nShipmentLines = eleSelectedAllLinesOption.getElementsByTagName("ShipmentLine");
		for (int i=0; i<nShipmentLines.getLength();i++ )
		{
			Node nShipmentLine = nShipmentLines.item(i);
			Element eleShipmentLine = (Element) nShipmentLine;
			if (sShipmentLineKey.equalsIgnoreCase(eleShipmentLine.getAttribute("ShipmentLineKey")))
			{
				String sNoteTextInModel = "N";
				sNoteTextInModel = eleShipmentLine.getAttribute("NoteText");
				if(!(sNoteTextInModel.isEmpty()))
				{
					if(sNoteTextInModel.equalsIgnoreCase("BP_Will_Pick_Later"))
					{
						sNoteReasonFromModel = HBCSOMConstants.BP_WILL_PICK_LATER_REASON ;
					}
				}
				else
				{
					sNoteReasonFromModel = sShortPickReason ;
				}
			}
		}
		return sNoteReasonFromModel;
	}

	/**
	 * This method is implemented to remove changeShipmentStatus API call
	 * @param apiContext
	 * @return
	 */
	private  void removeChangeShipmentStatus (YRCApiContext apiContext) 
	{
		final String[] strApis = apiContext.getApiNames();
		Document[] docAPIInp = apiContext.getInputXmls();
		Element eleInp = null;
		String strApi;
		for(int i=0 ; i < strApis.length ; i++)
		{
			strApi=strApis[i];
			if(strApi.equals("changeShipmentStatus"))
			{
				eleInp = docAPIInp[i].getDocumentElement() ;
				eleInp.setAttribute("RemoveAPICall", "Y") ;
			}
		}
	}


	public void postCommand(YRCApiContext yrcapicontext) 
	{
    	HashMap<String, String> hmpSplitLines = new HashMap<String, String>() ;

		if(yrcapicontext.getApiName().equals("changeShipment"))
		{
	/*		if(docuMultiApi!=null)
			{
			Element eMultiApi = docuMultiApi.getDocumentElement();
			NodeList nMultiAPi = eMultiApi.getElementsByTagName("API");
			if(nMultiAPi.getLength()>0)
			{
				YRCApiContext context = new YRCApiContext();
	            context.setApiName("HBCGetOrderLineList");
	            context.setFormId(this.getFormId());
	            context.setInputXml(docuMultiApi);
	            YRCApiCaller apiCaller1 = new YRCApiCaller(context, true);
	            apiCaller1.invokeApi();

				Document docMultiAPIOut = context.getOutputXml();
				if(!XMLUtil.isVoid(docMultiAPIOut))
				{
					Element eleMultiAPIOut =docMultiAPIOut.getDocumentElement();
					NodeList nAPIOut = eleMultiAPIOut.getElementsByTagName("API");

					for (int iAPI=0; iAPI<nAPIOut.getLength();iAPI++ )
					{
						Element eleAPI = 	(Element) nAPIOut.item(iAPI);
						Element eleOrderline = (Element) eleAPI.getElementsByTagName("OrderLine").item(0);
						if(eleOrderline!=null)
						{
							hmpSplitLines.put(eleOrderline.getAttribute("OrigOrderLineKey"),
									eleOrderline.getAttribute("OrderLineKey")) ;
						}
					}
				}
			}
*/
			if(isOrderPickup)
			{
/*				int iOrderLineKeys = strOrderLineKeys.size();		
				Document docChangeOrder = YRCXmlUtils.createDocument("Order");
				Element eleChangeOrder = docChangeOrder.getDocumentElement();
				eleChangeOrder.setAttribute("OrderHeaderKey", sOrderHeaderKey);
				eleChangeOrder.setAttribute("Action", "MODIFY");
				eleChangeOrder.setAttribute("Override", "Y");
				Element eleOrderLines = XMLUtil.createChild(eleChangeOrder, "OrderLines");
				int iOL=0;
				do
				{
					if(!strOrderLineKeys.isEmpty())
					{
					String sOrderLineKey =strOrderLineKeys.get(iOL);
					String sShipmentLineKey=hmpOrderLineShipmentLine.get(sOrderLineKey);

					if(hmpPartialCancelLines.containsKey(sShipmentLineKey))
					{
						Element	eleChangeOrderLine= YRCXmlUtils.createChild(eleOrderLines, "OrderLine");		
						eleChangeOrderLine.setAttribute("OrderLineKey", hmpSplitLines.get(sOrderLineKey));
//						eleChangeOrderLine.setAttribute("OrderedQty","0.0");
						eleChangeOrderLine.setAttribute("Action", "Cancel");
					}
						else
						{
							Element	eleChangeOrderLine= YRCXmlUtils.createChild(eleOrderLines, "OrderLine");
							eleChangeOrderLine.setAttribute("OrderLineKey",sOrderLineKey);
//							eleChangeOrderLine.setAttribute("OrderedQty","0.0");
							eleChangeOrderLine.setAttribute("Action", "Cancel");
						}
					}
					iOL++;
				}
				while(iOrderLineKeys>iOL);
				
*/				
				
				System.out.println("Inside prepare input for cancel pick Order");
				
				Document docChangeOrder = YRCXmlUtils.createDocument("Order") ;
				Element eleChangeOrder = docChangeOrder.getDocumentElement() ;
				eleChangeOrder.setAttribute("OrderHeaderKey", sOrderHeaderKey) ;

				/*570 Start:- appending shortage lines, DeliverMethod, CancellationFrom to the input of 
				         HBCCancelShipmentFromSOM service*/
				eleChangeOrder.setAttribute("DeliveryMethod", "PICK") ;
				eleChangeOrder.setAttribute("CancelFrom", "BackRoomPick") ; 
				if(eleNewOrderLines != null)
				{	
					Element eleOrderLines = XMLUtil.createChild(eleChangeOrder, "OrderLines") ;
					Element eleCloneOrderLines = (Element)eleNewOrderLines.cloneNode(true) ;
					XMLUtil.copyElement(docChangeOrder, eleCloneOrderLines, eleOrderLines) ;
				}
				/*570 End:- appending shortage lines to the input of HBCCancelShipmentFromSOM service */
				
				YRCApiContext context = new YRCApiContext() ;
	            context.setApiName("HBCCancelShipmentFromSOM") ;
	            context.setFormId(this.getFormId()) ;
	            context.setInputXml(docChangeOrder) ;
	            YRCApiCaller apiCaller1 = new YRCApiCaller(context, true) ;
	            apiCaller1.invokeApi() ;
				
			}
			/*Defect 570:Start - Cancel the order lines if Shortage reason is 
                   either Zero dollar or Gift with purchase*/
			else
			{
				//if(sNoteText.equalsIgnoreCase(HBCSOMConstants.ZERO_DOLLAR) 
					//|| sNoteText.equalsIgnoreCase(sNoteText))
				
				//if(HBCSOMConstants.ZERO_DOLLAR.equalsIgnoreCase(sNoteText) 
						//|| HBCSOMConstants.GIFT_WITH_PURCHASE.equalsIgnoreCase(sNoteText))
						//Start:Defect 712
				
				try 
				{
					inputDocCommonCode = XMLUtil.createDocument(HBCPCAConstants.E_COMMONCODE);
					Element inputCCEle = inputDocCommonCode.getDocumentElement();
					inputCCEle.setAttribute(HBCPCAConstants.A_CODETYPE, "CARRIER_SERVICE_CODE") ;
					
					if(CarrierServiceCode != null && !CarrierServiceCode.trim().isEmpty())
					{
						inputCCEle.setAttribute(HBCPCAConstants.A_CODEVALUE, CarrierServiceCode) ;
					}
					else
					{
						inputCCEle.setAttribute(HBCPCAConstants.A_CODEVALUE, HBCSOMConstants.DEFAULT_CARRIERSERVICE_CODE) ;
					}
					
					YRCApiContext yrcApiContext = new YRCApiContext() ;
					yrcApiContext.setApiName(HBCPCAConstants.API_GET_COMMON_CODE_LIST) ;
					yrcApiContext.setInputXml(inputDocCommonCode) ;
					yrcApiContext.setFormId(this.getFormId()) ;
					new YRCApiCaller(yrcApiContext, true).invokeApi() ;
					handleApiCompletion(yrcApiContext);
					
					if (yrcApiContext.getOutputXml() != null) 
					{
						Document outputCCDoc = yrcApiContext.getOutputXml() ;
						if (!XMLUtil.isVoid(outputCCDoc)) 
						{
							Element outputCCDocEle = outputCCDoc.getDocumentElement();
												
							if (outputCCDocEle.hasChildNodes()) 
							{
								Element outputCCEle = 
									XMLUtil.getFirstElementByName(outputCCDocEle, HBCPCAConstants.E_COMMONCODE) ;
																
								NoOfDays = outputCCEle.getAttribute("CodeShortDescription") ;
							}
						}
					}
				} 
				catch (ParserConfigurationException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace() ;
				}
				//Add condition at the end i.e sDiffDays > Integer.parseInt(NoOfDays)&& !NoOfDays.equals(null)
				//End : defect 712
				//Start 885: add below condition for RTV
				if((hmNoteTexts.containsValue(HBCSOMConstants.ZERO_DOLLAR) 
					|| hmNoteTexts.containsValue(HBCSOMConstants.GIFT_WITH_PURCHASE)|| hmNoteTexts.containsValue(HBCSOMConstants.RTVS))
						||(!hmNoteTexts.isEmpty() && (sDiffDays > Integer.parseInt(NoOfDays))))
				{
					System.out.println("Inside prepare input for cancel ship Order");
					Document docChangeOrder = YRCXmlUtils.createDocument("Order") ;
					Element eleChangeOrder = docChangeOrder.getDocumentElement() ;
					eleChangeOrder.setAttribute("OrderHeaderKey", sOrderHeaderKey) ;
					eleChangeOrder.setAttribute("DeliveryMethod", "SHP") ;
					eleChangeOrder.setAttribute("CancelFrom", "BackRoomPick") ;
					
					/*570 Start:- appending shortage lines to the input of HBCCancelShipmentFromSOM service*/
					if(eleNewOrderLines != null)
					{	
						Element eleOrderLines = XMLUtil.createChild(eleChangeOrder, "OrderLines") ;
						Element eleCloneOrderLines = (Element)eleNewOrderLines.cloneNode(true) ;
						XMLUtil.copyElement(docChangeOrder, eleCloneOrderLines, eleOrderLines) ;
					}
					/*570 End:- appending shortage lines to the input of HBCCancelShipmentFromSOM service*/
	
					YRCApiContext context = new YRCApiContext() ;
		            context.setApiName("HBCCancelShipmentFromSOM") ;
		            context.setFormId(this.getFormId()) ;
		            context.setInputXml(docChangeOrder) ;
		            YRCApiCaller apiCaller1 = new YRCApiCaller(context, true) ;
		            apiCaller1.invokeApi() ;
				}
				/*Defect 570:End - Cancel the order lines if Shortage reason is 
				   either Zero dollar or Gift with purchase */
			}
		}
		super.postCommand(yrcapicontext) ; 
	}
}
//TODO Validation required for a Radio control: extn_Clearance
//TODO Validation required for a Radio control: extn_Other
//TODO Validation required for a Radio control: extn_ZeroDollar
//TODO Validation required for a Radio control: bttnShrtgRsn1
//TODO Validation required for a Radio control: extn_GiftWithPurchase
//TODO Validation required for a Radio control: bttnShrtgRsn2
//TODO Validation required for a Radio control: extn_ItemDamaged
//TODO Validation required for a Radio control: extn_InTransit
//TODO Validation required for a Radio control: extn_RTVs
//TODO Validation required for a Radio control: extn_UPCDoesNotMatch
//TODO Validation required for a Radio control: extn_DuplicateOrder
//TODO Validation required for a Button control: bttnConfirm
//TODO Validation required for a Radio control: extn_OnHand
//TODO Validation required for a Button control: bttn
//TODO Validation required for a Radio control: extn_Misdirect