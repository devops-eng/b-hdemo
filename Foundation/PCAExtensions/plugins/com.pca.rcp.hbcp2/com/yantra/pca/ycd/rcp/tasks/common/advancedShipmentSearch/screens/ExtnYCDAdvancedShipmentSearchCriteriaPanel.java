
package com.yantra.pca.ycd.rcp.tasks.common.advancedShipmentSearch.screens;

/**
 * Created on Jun 18,2013
 *
 */

import java.awt.Button;
import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCExtentionBehavior;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCExtendedTableBindingData;
import com.yantra.yfc.rcp.YRCXmlUtils;
/**
 * @author mohit_sharma15
 * Copyright IBM Corp. All Rights Reserved.
 */
public class ExtnYCDAdvancedShipmentSearchCriteriaPanel extends YRCExtentionBehavior{

	Element eleCommonCodeListOutput = null;
	NodeList nCommonCodeListOutput = null;
	String sUserLocale = null;
		//R3 - 905 :- selected status for status search and reprinting flag for Radio buttons
	String sStatusSelected=null;
	String sReprintFlag = "N";
	/**
	 * This method initializes the behavior class.
	 * This method is implemented to hide the combo box present at 
	 * advanced shipment search screen, as a new combo box is created for extended funtionality .
	 */
	public void init() {
		this.setControlVisible("extn_comboCarrier", false);
		//R3: Modified for Defect 905: Setting the input for getStatusList API
		Document docGetCommonCodeList = YRCXmlUtils.createDocument("CommonCode");
		Element eleCommonCodeInput = docGetCommonCodeList.getDocumentElement();
		eleCommonCodeInput.setAttribute("CodeType", "SHIPMENT_STATUS");
		YRCApiContext context = new YRCApiContext();
		context.setFormId(super.getFormId());
		context.setApiName("HBCGetCommonCodeListService");
		context.setInputXml(docGetCommonCodeList);
		callApi(context);
		//R3: End of modification for Defect 905: Setting the input for getStatusList API
		//TODO: Write behavior init here.
	}

	/**
	 * Method for validating the text box.
	 */
	public YRCValidationResponse validateTextField(String fieldName, String fieldValue) {
		// TODO Validation required for the following controls.

		// TODO Create and return a response.
		return super.validateTextField(fieldName, fieldValue);
	}

	/**
	 * Method for validating the combo box entry.
	 */
	public void validateComboField(String fieldName, String fieldValue) {
		// TODO Validation required for the following controls.
		//905:- To fetch the selected status value
		 if ("extn_ComboStatus".equalsIgnoreCase(fieldName)) {
		      this.sStatusSelected = fieldValue;
		    }
		// TODO Create and return a response.
		super.validateComboField(fieldName, fieldValue);
	}

	/**
	 * Method called when a button is clicked.
	 * This method is implemented to achieve following tasks
	 * 
	 * 1. On advanced shipment search when pick up radio button is clicked 
	 * combo box for carrier selection is hidden.
	 * 
	 * 2. On Advanced shipment search when ship option is selected, 
	 * the combo box for carrier selection is made visible.
	 */
	public YRCValidationResponse validateButtonClick(String fieldName) {
		// TODO Validation required for the following controls.
		if ("bttnShip".equalsIgnoreCase(fieldName))
		{
			this.setControlVisible("extn_comboCarrier", true);
		}
		//Defect 905 : To Hide Carrier search combo Box for Delivery method PICK
		else if("bttnPick".equalsIgnoreCase(fieldName))
		{
			this.setControlVisible("extn_comboCarrier", false);
		}
		// Modified for defect 905:Start :- code to search shipments to print pick ticket on the basis of radio button selected
		if("extn_N".equalsIgnoreCase(fieldName)){
			sReprintFlag = "N";			
		}
		if("extn_Y".equalsIgnoreCase(fieldName)){
			sReprintFlag = "Y";		
		}	
		// Modified for defect 905:End :- code to search shipments to print pick ticket on the basis of radio button selected
		// TODO Create and return a response.**/
		return super.validateButtonClick(fieldName);
	}

	/**
	 * Method called when a link is clicked.
	 */
	public YRCValidationResponse validateLinkClick(String fieldName) {
		// TODO Validation required for the following controls.

		// TODO Create and return a response.
		return super.validateLinkClick(fieldName);
	}

	/**
	 * Create and return the binding data for advanced table columns added to the tables.
	 */
	public YRCExtendedTableBindingData getExtendedTableBindingData(String tableName, ArrayList tableColumnNames) {
		// Create and return the binding data definition for the table.

		// The defualt super implementation does nothing.
		return super.getExtendedTableBindingData(tableName, tableColumnNames);
	}

	@Override
	public boolean preCommand(YRCApiContext apiContext) {

		return super.preCommand(apiContext);
	}

	@Override
	public void postCommand(YRCApiContext apiContext) {
		if ("getScacList".equalsIgnoreCase(apiContext.getApiName()))
		{
			Document dgetScacListOut = apiContext.getOutputXml();
		}
		//R3: Modified for Defect 905: Populate extension model with output for getStatusList
		if ("HBCGetCommonCodeListService".equalsIgnoreCase(apiContext.getApiName()))
		{
			this.setExtentionModel("Extn_HBCGetCommonCodeList_output", apiContext.getOutputXml().getDocumentElement());	
			eleCommonCodeListOutput = this.getModel("Extn_HBCGetCommonCodeList_output");
			if (!YRCPlatformUI.isVoid(eleCommonCodeListOutput)) {
				nCommonCodeListOutput = eleCommonCodeListOutput.getElementsByTagName("CommonCode");
			}
			String sLocalecode = getUserLocale();   
			Document docStatusList = YRCXmlUtils.createDocument("StatusList");
			Element eleStatusList = docStatusList.getDocumentElement();
			if (sLocalecode.contains("fr"))
			{
				for (int g = 0; g < nCommonCodeListOutput.getLength(); g++) 
				{
					Element eleCommonCode = (Element) nCommonCodeListOutput.item(g);
					Element eleStatus = docStatusList.createElement("Status");
					eleStatus.setAttribute("Description", eleCommonCode.getAttribute("CodeLongDescription"));
					eleStatus.setAttribute("Status", eleCommonCode.getAttribute("CodeValue"));
					eleStatusList.appendChild(eleStatus);
				}
			}
			else
			{
				for (int g = 0; g < nCommonCodeListOutput.getLength(); g++) 
				{
					Element eleCommonCode = (Element) nCommonCodeListOutput.item(g);
					Element eleStatus = docStatusList.createElement("Status");
					eleStatus.setAttribute("Description", eleCommonCode.getAttribute("CodeShortDescription"));
					eleStatus.setAttribute("Status", eleCommonCode.getAttribute("CodeValue"));
					eleStatusList.appendChild(eleStatus);
				}
			}
			this.setExtentionModel("Extn_getStatusList_output", docStatusList.getDocumentElement());
			
		}
		
		//R3: End of modification for Defect 905: Populate extension model with output for getStatusList
		super.postCommand(apiContext);
	}
	
	@Override
	/**
	 * Method called to set attribute PickTicketPrinted Based on radio button input
	 */
	public void postSetModel(String namespace){
		if (namespace.equalsIgnoreCase("getShipmentList_input")){
			Element eleShipments = this.getModel("getShipmentList_input");

			if (!sReprintFlag.equalsIgnoreCase("") && sReprintFlag!= null){
				eleShipments.setAttribute("PickTicketPrinted",sReprintFlag);
				this.repopulateModel(namespace);
			}
		}
		super.postSetModel(namespace);
	}
	/**
	 * Method called to fetch user locale.
	 */
	private String getUserLocale() {
		org.w3c.dom.Element eleUserNameSpace = this.getModel("UserNameSpace");
		String sLocalecode = null;
		if (!YRCPlatformUI.isVoid(eleUserNameSpace)) {			
			sLocalecode = eleUserNameSpace.getAttribute("Localecode");
		}
		return sLocalecode;
	}
	//Modified for 905 :Start:- to pass value of selected radio button and status
	  public String getSelectedStatus()
	  {
	    return this.sStatusSelected;
	  }	
		public String getReprintFlag(){		
			return this.sReprintFlag;
		}
	//Modified for 905:End:- to pass value of selected radio button and status
}
//TODO Validation required for a Radio control: bttnPick
//TODO Validation required for a Radio control: bttnShip