	
package com.yantra.pca.ycd.rcp.tasks.confirmShipments.wizards;

/**
 * Created on Aug 16,2013
 *
 */
 
import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.yantra.pca.ycd.rcp.util.HBCSOMConstants;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCWizardExtensionBehavior;
import com.yantra.yfc.rcp.IYRCComposite;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCExtendedTableBindingData;
/**
 * @author mohit_sharma15
 * Copyright IBM Corp. All Rights Reserved.
 */
 public class ExtnYCDConfirmShipmentsWizard extends YRCWizardExtensionBehavior {

	/**
	 * This method initializes the behavior class.
	 */
	public void init() {
		//TODO: Write behavior init here.
	}
 
 	
    public String getExtnNextPage(String currentPageId) {
		//TODO
		return null;
    }
    
    public IYRCComposite createPage(String pageIdToBeShown) {
		//TODO
		return null;
	}
    
    public void pageBeingDisposed(String pageToBeDisposed) {
		//TODO
    }

    /**
     * Called when a wizard page is about to be shown for the first time.
     *
     */
    public void initPage(String pageBeingShown) {
		//TODO
    }
 	
 	
	/**
	 * Method for validating the text box.
     */
    public YRCValidationResponse validateTextField(String fieldName, String fieldValue) {
    	// TODO Validation required for the following controls.
		
		// TODO Create and return a response.
		return super.validateTextField(fieldName, fieldValue);
	}
    
    /**
     * Method for validating the combo box entry.
     */
    public void validateComboField(String fieldName, String fieldValue) {
    	// TODO Validation required for the following controls.
		
		// TODO Create and return a response.
		super.validateComboField(fieldName, fieldValue);
    }
    
    /**
     * Method called when a button is clicked.
     */
    public YRCValidationResponse validateButtonClick(String fieldName) {
    	// TODO Validation required for the following controls.
		
		// TODO Create and return a response.
		return super.validateButtonClick(fieldName);
    }
    
    /**
     * Method called when a link is clicked.
     */
	public YRCValidationResponse validateLinkClick(String fieldName) {
    	// TODO Validation required for the following controls.
		
		// TODO Create and return a response.
		return super.validateLinkClick(fieldName);
	}
	
	public void postCommand(YRCApiContext apiContext) {
		if ("confirmShipment".equalsIgnoreCase(apiContext.getApiName()))
		{
			Document dconfirmShipment = apiContext.getOutputXml();
		}
		super.postCommand(apiContext);
	}
	
	
	/**
	 * Create and return the binding data for advanced table columns added to the tables.
	 */
	 public YRCExtendedTableBindingData getExtendedTableBindingData(String tableName, ArrayList tableColumnNames) {
	 	// Create and return the binding data definition for the table.
		
	 	// The defualt super implementation does nothing.
	 	return super.getExtendedTableBindingData(tableName, tableColumnNames);
	 }
	 
	 @Override
	/** This method has been implemented to modify confirm shipment search based on Status.
	  * ShipmentClosedFlag attribute has been removed from the input and Status="1300" 
	  * has been added
	  */
	public boolean preCommand(YRCApiContext apiContext) {
	if ("getShipmentList".equalsIgnoreCase(apiContext.getApiName()))
	{
		Document docConfirmShipment = apiContext.getInputXml();
		Element eleConfirmShipment = docConfirmShipment.getDocumentElement();
		eleConfirmShipment.removeAttribute("ShipmentClosedFlag");
		eleConfirmShipment.setAttribute("Status",HBCSOMConstants.SHIPMENT_PACKED_STATUS);
	}
		return super.preCommand(apiContext);
	}
}
