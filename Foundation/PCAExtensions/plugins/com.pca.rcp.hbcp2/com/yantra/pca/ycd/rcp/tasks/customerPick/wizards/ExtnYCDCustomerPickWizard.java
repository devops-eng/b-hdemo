package com.yantra.pca.ycd.rcp.tasks.customerPick.wizards;

/**
 * Created on Jan 19,2014
 *
 */
 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.yantra.pca.ycd.rcp.tasks.screens.common.ExtnYCDCommonAddress;
import com.pca.rcp.hbcp2.OrderSummary.HBCAddShippingChargesExtnBehavior;

import com.pca.rcp.hbcp2.util.HBCPCAUtil;
import com.pca.rcp.hbcp2.util.XMLUtil;
import com.yantra.pca.ycd.rcp.util.HBCSOMConstants;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCDesktopUI;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCSharedTaskOutput;
import com.yantra.yfc.rcp.YRCWizard;
import com.yantra.yfc.rcp.YRCWizardExtensionBehavior;
import com.yantra.yfc.rcp.IYRCComposite;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCExtendedTableBindingData;
import com.yantra.yfc.rcp.YRCXmlUtils;
import com.yantra.yfc.rcp.internal.YRCApiCaller;


 	/************************************************************************************
	 *  FileName : ExtnYCDCustomerPickWizard.java
	 *
	 * This Class is Linked to Customer pickup screen of SOM, All customer Pickup operation
	 * customization is done through this code.
	 * 
	 * Modification Log:
	 * ------------------------------------------------------------------------------------
	 * Version Date Author Change Log
	 * ------------------------------------------------------------------------------------
	 * 1.0 07-07-2014 Rajath_Babu  Initial Draft - Initial Draft
	 * 1.0 07-07-2014 Mohit Sharma Defect 570    - Update Reason Code for Order Cancellation
 *
	 *Copyright IBM Corp. All Rights Reserved
	 ************************************************************************************/
 public class ExtnYCDCustomerPickWizard extends YRCWizardExtensionBehavior 
 {

	  
	 //Defect 570 :- to store the OrderLineKey and its respective reason code for orderLines wtih shortage.
	 Element eleNewOrderLines = null ;

	/**
	 * This method initializes the behavior class.
	 */
	public void init() {
		//TODO: Write behavior init here.
	}
 
	 public String getExtnNextPage(String currentPageId) 
	 {
		//TODO
		return null;
    }
    
	 public IYRCComposite createPage(String pageIdToBeShown) 
	 {
		//TODO
		return null;
	}
    
	public void pageBeingDisposed(String pageToBeDisposed) 
	{
		//TODO
    }

    /**
     * Called when a wizard page is about to be shown for the first time.
     *
     */
	public void initPage(String pageBeingShown) 
	{
		//TODO
    }
 	
 	
	/**
	 * Method for validating the text box.
     */
    public YRCValidationResponse validateTextField(String fieldName, String fieldValue) 
    {
    	// TODO Validation required for the following controls.
		
		// TODO Create and return a response.
		return super.validateTextField(fieldName, fieldValue);
	}
    
    /**
     * Method for validating the combo box entry.
     */
    public void validateComboField(String fieldName, String fieldValue) 
    {
    	// TODO Validation required for the following controls.
		// TODO Create and return a response.
		super.validateComboField(fieldName, fieldValue);
    }
    
    /**
     * Method called when a button is clicked.
     * To display a pop up to confirm the Picked Qty.
     * SOM changes Req#8 May'14 Release 
     */
    public YRCValidationResponse validateButtonClick(String fieldName) 
    {
    	// TODO Validation required for the following controls.
    	
    	//Response type declared to block screen navigation in case of cancel button selection. 
    	YRCValidationResponse response = null;
    	//start :  Display POP UP confirming the quantity entered while doing Customer pick on next button click.
		if(fieldName.equalsIgnoreCase("bttnNext"))
		{
			YRCWizard wizard=(YRCWizard)YRCDesktopUI.getCurrentPage();
			String strPageID=wizard.getCurrentPageID();
			
			if(strPageID.equals("com.yantra.pca.ycd.rcp.tasks.customerPick.wizardpages.YCDCustomerPickChooseItems"))
			{			
				Element eleShipmentDetials = this.getModel("shipmentDetails_itemsPicked");				
				//Call updateMessage method to update the quantity of items picked
				double dPickQuantity = updateMessage(eleShipmentDetials, "PickedQty");
				int pickQuantity = (int)dPickQuantity;
				//Call updateMessage method to update total number of items
				double dTotalQuantity = updateMessage(eleShipmentDetials, "Quantity");
				if(dTotalQuantity >= dPickQuantity)
				{
					YRCSharedTaskOutput sharedTaskOutput = 
						HBCPCAUtil.showOptionDialogBoxWithConfirmCancel
							(YRCPlatformUI.getFormattedString("PICK_QTY_CONFIRM_TITLE", ""),
							YRCPlatformUI.getFormattedString("PICK_QTY_CONFIRM_MESSAGE","") + " (" +
									YRCPlatformUI.getFormattedString("PICK_QUANTITY","") + ": " + pickQuantity + ")") ;
					
					Element ele =sharedTaskOutput.getOutput();
					String strOutput = ele.getAttribute("SelectedOptionKey");
					
					//if cancel of cross button selected, block the screen navigation by returning error response
					if(strOutput.equals("Cancel") || XMLUtil.isVoid(strOutput))
					{
						response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"");
						return response;
					}
				}
			}
		}
		//end : Display POP UP on button click.
		
		// TODO Create and return a response.
		return super.validateButtonClick(fieldName);
    }
    
    /**
	 * Method call to calculate total picked quantity
	 * @param 	eleShipmentDetials - Element Shipment Details Model.
	 * @param 	sCategory - Attribute name for which quantity has to be calculated.
	 * @return	dQuantity - Total quantity of items.
	 * 
	 * SOM changes Req#8 May'14 Release 
	 */
    
	public double updateMessage(Element eleShipmentDetials, String sCategory)
	{
		double dQuantity = 0;
		Element eleShipmentLine = null;
		NodeList nOrderLineList = eleShipmentDetials.getElementsByTagName("ShipmentLine");
		for(int iSL=0; iSL<nOrderLineList.getLength(); iSL++)
		{
			eleShipmentLine = (Element)nOrderLineList.item(iSL);
			dQuantity = dQuantity + Double.parseDouble(eleShipmentLine.getAttribute(sCategory));
		}
		return dQuantity;
    }
    
    /**
     * Method called when a link is clicked.
     */
	public YRCValidationResponse validateLinkClick(String fieldName) 
	{
    	// TODO Validation required for the following controls.
		
		// TODO Create and return a response.
		return super.validateLinkClick(fieldName);
	}
	
	/**
	 * Create and return the binding data for advanced table columns added to the tables.
	 */
	 public YRCExtendedTableBindingData getExtendedTableBindingData(String tableName, ArrayList tableColumnNames) 
	 {
	 	// Create and return the binding data definition for the table.
		
	 	// The default super implementation does nothing.
	 	return super.getExtendedTableBindingData(tableName, tableColumnNames) ;
	 }
	 @Override
	/**
	 * This method is used to modify the output XML of a command.
	 */
	 public void postCommand(YRCApiContext apiContext) 
	 {
		// TODO Auto-generated method stub
		//--Commenting below piece of code. Update of BillToAddress Tag is not needed anymore as
			//customer details are being updated as a part separate code change
			/*
			 String[] getAllAPIsCalled=apiContext.getApiNames();
			 	for(int i=0;i<getAllAPIsCalled.length;i++)
			 	{
			 		String apiName=getAllAPIsCalled[i];*/
			 		/*Modified for BOPUS: In the output XML of getShipmentDetails command,
			 		update the BillToAddress as To Address so that it gets displayed on CustomerPickScreen*/
			 	/*	if ("getShipmentDetails".equalsIgnoreCase(apiName))
			 		{
				 		Document docGetShipmentDetails= apiContext.getOutputXml();
				 		Element eleShipment = docGetShipmentDetails.getDocumentElement();
			 			Element eleBillToAddress = null;
			 			Element eleToAddress = null;
			 			NodeList nBillToAddress =
			 				eleShipment.getElementsByTagName("BillToAddress");
			 			for(int j=0; j<nBillToAddress.getLength(); j++)
			 			{
			 				eleBillToAddress = (Element) nBillToAddress.item(j);
			 				eleShipment.removeChild(eleBillToAddress);
			 			}
			 			NodeList nToAddress =
			 				eleShipment.getElementsByTagName("ToAddress");
			 			for(int j=0; j<nToAddress.getLength(); j++)
			 			{
			 				eleToAddress = (Element) nToAddress.item(j);
			 				Element eleNewShipToAddress = docGetShipmentDetails.createElement("BillToAddress");
			 				YRCXmlUtils.setAttributes(eleToAddress, eleNewShipToAddress);
			 				eleShipment.removeChild(eleToAddress);
			 				eleShipment.appendChild(eleNewShipToAddress);
			 			}
			 		}
			 	}
			 */
		    
		 YRCWizard wizard=(YRCWizard)YRCDesktopUI.getCurrentPage();
			String strPageID=wizard.getCurrentPageID();
			
			if(strPageID.equals("com.yantra.pca.ycd.rcp.tasks.customerPick.wizardpages.YCDCustomerPickSummary"))
			{
				String[] getAllAPIsCalled=apiContext.getApiNames();
				for(int i=0;i<getAllAPIsCalled.length;i++)
				{
					String apiName=getAllAPIsCalled[i];					
					if(apiName.equalsIgnoreCase("recordCustomerPick") 
						&& strOrderHeaderKey!="" && strOrderHeaderKey!=null)
					{
						Document docChangeOrder = YRCXmlUtils.createDocument("Order");
						Element eleChangeOrder = docChangeOrder.getDocumentElement();
						eleChangeOrder.setAttribute("OrderHeaderKey", strOrderHeaderKey);
						eleChangeOrder.setAttribute("SelectMethod", "WAIT") ;
						/*570 Start:- appending shortage lines to the input of HBCCancelShipmentFromSOM service */
						eleChangeOrder.setAttribute("DeliveryMethod", "PICK") ;
						eleChangeOrder.setAttribute("CancelFrom", "CustomerPick") ;
						
						if(eleNewOrderLines != null)
						{	
						Element eleOrderLines = XMLUtil.createChild(eleChangeOrder, "OrderLines");
						Element eleCloneOrderLines = (Element)eleNewOrderLines.cloneNode(true);
						XMLUtil.copyElement(docChangeOrder, eleCloneOrderLines, eleOrderLines);
						}
						/*570 End:- appending shortage lines to the input of HBCCancelShipmentFromSOM service */
						YRCApiContext context = new YRCApiContext() ;
					    context.setApiName("HBCCancelShipmentFromSOM") ;
					    context.setFormId(this.getFormId());
					    context.setInputXml(docChangeOrder);
					    YRCApiCaller apiCaller1 = new YRCApiCaller(context, true);
					    apiCaller1.invokeApi();
						
					}
				}
			}
		super.postCommand(apiContext);
	}
	 @Override
	 /**
	  * This method is used to modify the input XML of a command.
	  */
	//R3.2:  Defect 796- Start of modification to backorder remaining quantity on a partially canceled shipment
	/*When there is a partial cancel on customer pickup, the remaining quantity is backordered
	so that the tax gets refunded*/
	public boolean preCommand(YRCApiContext apiContext) 
	{
		 YRCWizard wizard=(YRCWizard)YRCDesktopUI.getCurrentPage();
			String strPageID=wizard.getCurrentPageID();
		
		//Defect 570:Start :-  Updating Reason Code for customer pick order cancellation
		if(strPageID.equals("com.yantra.pca.ycd.rcp.tasks.customerPick.wizardpages.YCDCustomerPickSummary"))	
		{  
			Element eleShipment = this.getModel("shipmentDetails_shortageResolution") ;
			
			if (eleShipment.hasChildNodes())
			{
				Element eShipmentLines = YRCXmlUtils.getChildElement(eleShipment, "ShipmentLines") ;
				
				if (eShipmentLines.hasChildNodes())
				{
					NodeList nShipmentLine = eShipmentLines.getElementsByTagName("ShipmentLine") ;
  
			// Creating Element with OrderLineKey with corresponding reason code
			try 
			{
				Document docOrderLinesWithReasonCode = XMLUtil.createDocument("OrderLines") ;
				eleNewOrderLines = docOrderLinesWithReasonCode.getDocumentElement() ;
				for(int i=0; i<nShipmentLine.getLength(); i++ )
				{
			    	Element eleShipmentLine = (Element)nShipmentLine.item(i) ;
					if(!YRCPlatformUI.isVoid(eleShipmentLine))
					{
						String strOrderLineKey = eleShipmentLine.getAttribute("OrderLineKey");
						String strReasonCode = eleShipmentLine.getAttribute("ReasonCode");
	
						Element eleNewOrderLine = XMLUtil.createChild(eleNewOrderLines, "OrderLine");
						eleNewOrderLine.setAttribute("OrderLineKey", strOrderLineKey);
						eleNewOrderLine.setAttribute("ReasonCode", strReasonCode);	
					} 
				}	
			}
			catch (ParserConfigurationException e) 
			{
				  // TODO Auto-generated catch block
				  e.printStackTrace();
			}
		}
			}
		}
		//Defect 570:End :-  Updating Reason Code for customer pick order cancellation
		
			if(strPageID.equals("com.yantra.pca.ycd.rcp.tasks.customerPick.wizardpages.YCDCustomerPickSummary"))
			{
				String[] getAllAPIsCalled=apiContext.getApiNames();
				for(int i=0;i<getAllAPIsCalled.length;i++)
				{
					String apiName=getAllAPIsCalled[i];					
					if(apiName.equalsIgnoreCase("recordCustomerPick"))
					{
						Document[] getAllInputXMLs=apiContext.getInputXmls();
						Document docRecordCustomerPick = getAllInputXMLs[1];				
						Element eleShipment = docRecordCustomerPick.getDocumentElement();
						if (!YRCPlatformUI.isVoid(eleShipment)){
							Element eShipmentLines = YRCXmlUtils.getChildElement(eleShipment, "ShipmentLines");
							if (!YRCPlatformUI.isVoid(eShipmentLines)){
								NodeList nShipmentLines = eShipmentLines.getElementsByTagName("ShipmentLine");
								for(int t=0;t<nShipmentLines.getLength();t++)
								{
									Element eShipmentLine = (Element)nShipmentLines.item(t);
									if(!YRCPlatformUI.isVoid(eShipmentLine))
									{
										String sPickedQuantity = eShipmentLine.getAttribute("PickedQty");
										String sShortageReason = eShipmentLine.getAttribute("ShortageReason");
										if (HBCSOMConstants.CUSTOMER_PICK_UP_CANCEL.equalsIgnoreCase(sShortageReason))
										{
											if(!YRCPlatformUI.isVoid(sPickedQuantity))
											{
//												double dPickedQuantity = Double.parseDouble(sPickedQuantity); 
//												if (dPickedQuantity != 0.0)
//												{
												eShipmentLine.setAttribute("ShortageReason", 
													HBCSOMConstants.ALL_INVENTORY_SHORTAGE) ;
//												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			//R3.2:  Defect 796- End of modification to backorder remaining quantity on a partially canceled shipment
		return super.preCommand(apiContext) ;
	}
	 
	 String strOrderHeaderKey="";
	 
	 public void postSetModel(String namespace) 
	 {
		// TODO Auto-generated method stub
		 YRCWizard wizard=(YRCWizard)YRCDesktopUI.getCurrentPage();
		 String strPageID=wizard.getCurrentPageID();
		 if(strPageID.equalsIgnoreCase
			("com.yantra.pca.ycd.rcp.tasks.customerPick.wizardpages.YCDCustomerPickShortageResolution"))
		 {
		   this.setFieldValue("radioCancelRemaining", true);
     	   this.setFieldValue("radioInventoryShrtg", false);
     	   this.setFieldValue("radioPickLater", true);
     	   this.setControlVisible("cmbLineCnclRsn", false);
		 }
		 
		 
	 	 //--updates ShipToAddress in Address Panel of Customer Verification screen
		 if(strPageID.equalsIgnoreCase
			("com.yantra.pca.ycd.rcp.tasks.customerPick.wizardpages.YCDCustomerPickVerification"))
		 {
			  setAddressFields("ShipToAddress") ;
		 }
		//--updates BillToAddress in Address Panel of Customer PickSummary & ChooseItems screen
		 else if(strPageID.equalsIgnoreCase
				 ("com.yantra.pca.ycd.rcp.tasks.customerPick.wizardpages.YCDCustomerPickChooseItems") ||
				 strPageID.equalsIgnoreCase
				 	("com.yantra.pca.ycd.rcp.tasks.customerPick.wizardpages.YCDCustomerPickSummary"))
		 {
			//--Defect# 223 fix: Comment out setAddressFields("BillToAddress") method call.
			 // setAddressFields("BillToAddress");
		 }
		 
		 if(namespace.equals("getShipmentDetails_output"))
		 {
			 Element eleShipment = this.getModel(namespace) ;
			 Element eShipmentLines = YRCXmlUtils.getChildElement(eleShipment, "ShipmentLines") ;
			 Element eleShipmentLine = (Element) eShipmentLines.getElementsByTagName("ShipmentLine").item(0) ;
			 if(eleShipmentLine!=null)
			 {
				 strOrderHeaderKey=eleShipmentLine.getAttribute("OrderHeaderKey") ;
			 }
		 }
		super.postSetModel(namespace) ;
	}
	 
	 /**
	  * Method to set field values in Address Panel
	  * 
	  * @param strAddressType : String indicator for PageId
	  * @return void
	  */
	 public void setAddressFields(String strAddressType)
	 {
		 Element eleShipmentDetails = this.getModel("getShipmentDetails_output");
		 NodeList nBillToAddress = null;
		 NodeList nToAddress = null;
		 Element eleBillToAddress = null;
		 Element eleToAddress = null;
		 String stxtAddress1="";
		 String stxtAddress2="";
		 String stxtAddress3="";
		 String stxtAddress8="";
		 String stxtAddress9="";
		 String sEMailID="";
		 String sDayPhone="";
		 
		 //-- Get BillToAddress and ToAddress from getShipmentDetails_output model
		 if(!XMLUtil.isVoid(eleShipmentDetails))
		 {
			 nBillToAddress = eleShipmentDetails.getElementsByTagName("BillToAddress") ;
			 nToAddress = eleShipmentDetails.getElementsByTagName("ToAddress") ;
			 
			 //--If call is made for Customer PickSummary & ChooseItems screen
			 if(!XMLUtil.isVoid(nBillToAddress) && strAddressType.equalsIgnoreCase("BillToAddress"))
			 {
				 eleBillToAddress = (Element)nBillToAddress.item(0) ;
				 if(!XMLUtil.isVoid(eleBillToAddress))
				 {	
					 //-- method call to set field values
					 stxtAddress1 = fetchAttributes(eleBillToAddress, "FirstName", "MiddleName", "LastName") ;
					 stxtAddress2 = fetchAttributes(eleBillToAddress, "AddressLine1", "AddressLine2", "AddressLine3") ;
					 stxtAddress3 = fetchAttributes(eleBillToAddress, "AddressLine4", "AddressLine5", "AddressLine6") ;					 
					 stxtAddress8 = fetchAttributes(eleBillToAddress, "City", "State", "ZipCode") ;
					 stxtAddress9 = fetchAttributes(eleBillToAddress, "Country", "", "") ;
					 sEMailID = fetchAttributes(eleBillToAddress, "EMailID", "", "") ;
					 //sDayPhone = fetchAttributes(eleBillToAddress, "DayPhone", "", "") ;					
				 }
			 }
			//--If call is made for Customer Verification screen
			 if(!XMLUtil.isVoid(nToAddress) && strAddressType.equalsIgnoreCase("ShipToAddress"))
			 {
				 eleToAddress = (Element)nToAddress.item(0);
				 if(!XMLUtil.isVoid(eleToAddress))
				 {
					//-- method call to set field values
					 stxtAddress1 = fetchAttributes(eleToAddress, "FirstName", "MiddleName", "LastName") ;
					 stxtAddress2 = fetchAttributes(eleToAddress, "AddressLine1", "AddressLine2", "AddressLine3") ;
					 stxtAddress3 = fetchAttributes(eleToAddress, "AddressLine4", "AddressLine5", "AddressLine6") ;					 
					 stxtAddress8 = fetchAttributes(eleToAddress, "City", "State", "ZipCode") ;
					 stxtAddress9 = fetchAttributes(eleToAddress, "Country", "", "") ;
					 sEMailID = fetchAttributes(eleToAddress, "EMailID", "", "") ;
					 sDayPhone = fetchAttributes(eleToAddress, "DayPhone", "", "") ;
				 }
			 } 
		 }
		 //--Defect# 223 :: Update the field content with Ship To Address.
		 
		this.setFieldValue("extn_AddressTxt1", stxtAddress1);
		this.setFieldValue("extn_AddressTxt2", stxtAddress2);
		this.setFieldValue("extn_AddressTxt3", stxtAddress3);
		this.setFieldValue("extn_AddressTxt4", stxtAddress8);
		this.setFieldValue("extn_AddressTxt5", stxtAddress9);
		this.setFieldValue("extn_AddressTxt6", sEMailID);
		this.setFieldValue("extn_AddressTxt7", sDayPhone);
		 
		 //--Get the child behavior of Address Panel
	 	/*ArrayList childExtnBehaviors = this.getChildExtnBehaviors();
		Iterator ExtnYCDCommonAddressList = childExtnBehaviors.iterator();
		while (ExtnYCDCommonAddressList.hasNext()) {
			Object ObjExtnYCDCommonAddress = ExtnYCDCommonAddressList
			.next();
			if (ObjExtnYCDCommonAddress instanceof ExtnYCDCommonAddress) {
				ExtnYCDCommonAddress objExtnYCDCommonAddress = (ExtnYCDCommonAddress) ObjExtnYCDCommonAddress;
				//--Set the field values for Address Panel
 				objExtnYCDCommonAddress.setFieldValue("txtAddress1", stxtAddress1);
 				objExtnYCDCommonAddress.setFieldValue("txtAddress2", stxtAddress2);
 				objExtnYCDCommonAddress.setFieldValue("txtAddress3", stxtAddress3);
 				objExtnYCDCommonAddress.setFieldValue("txtAddress8", stxtAddress8);
 				objExtnYCDCommonAddress.setFieldValue("txtAddress9", stxtAddress9);
 				objExtnYCDCommonAddress.setFieldValue("extn_EmailId", sEMailID);
 				//--start : defect# 66 fix
 				//--Set Phone number field value only for ShipToAddress
 				if(!sDayPhone.equalsIgnoreCase("")){
 					objExtnYCDCommonAddress.setFieldValue("extn_DayPhone", sDayPhone);
 					//--get the field content of Phone number.
 					String sPhoneNoDisplayed = objExtnYCDCommonAddress.getFieldValue("extn_DayPhone");
 					//--If extension number is also present with phone number, remove it 
 					 * and reset the field with only Phone number.
 					int iExtnMark = 0;
 					iExtnMark = sPhoneNoDisplayed.indexOf("X");			
 					if(iExtnMark > 0){
 						sDayPhone = sPhoneNoDisplayed.substring(0, iExtnMark);
 						objExtnYCDCommonAddress.setFieldValue("extn_DayPhone", sDayPhone);
 					}
 				}
 				//--end : defect# 66 fix
	 		}
		 } */
	 }
	 
	 /**
	  * Method to fetch Attributes values from Address Element
	  * 
	  * @param eleAddress 	: Element Address Tag
	  * @param strVal1		: String Attribute
	  * @param strVal2		: String Attribute 
	  * @param strVal3		: String Attribute
	  * 
	  * @return String		: String Field Value
	  */
	 public String fetchAttributes(Element eleAddress, String strVal1, String strVal2, String strVal3)
	 {		 
		 String strFieldValue = null;
		 //--Fetch attributes from the element parameter
		 strVal1 = eleAddress.getAttribute(strVal1);
		 strVal2 = eleAddress.getAttribute(strVal2);
		 strVal3 = eleAddress.getAttribute(strVal3);
		 if(XMLUtil.isVoid(strVal1))
		 {
			 strVal1="" ;
		 }
		 
		 if(XMLUtil.isVoid(strVal2))
		 {
			 strVal2="" ;
		 }
		 
		 if(XMLUtil.isVoid(strVal3))
		 {
			 strVal3="" ;
		 }
		 
		 //--Concatenate attribute values for text field entry
		 strFieldValue = strVal1 + " " +strVal2 + " " + strVal3 ;
		 return strFieldValue ;
	 }
}	
