package com.yantra.pca.ycd.rcp.tasks.printPickTicket.wizards;

/**
 * Created on Jul 03,2013
 *
 */

import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.net.URL;

import org.eclipse.swt.SWT;
import org.eclipse.swt.program.Program;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.lowagie.text.Image;
import com.lowagie.text.Chunk;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.Barcode128;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.yantra.pca.ycd.rcp.util.HBCSOMConstants;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCDesktopUI;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCSharedTaskOutput;
import com.yantra.yfc.rcp.YRCWizard;
import com.yantra.yfc.rcp.YRCWizardExtensionBehavior;
import com.yantra.yfc.rcp.IYRCComposite;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCExtendedTableBindingData;
import com.yantra.yfc.rcp.YRCXmlUtils;
import com.yantra.pca.ycd.rcp.tasks.common.advancedShipmentSearch.screens.ExtnYCDAdvancedShipmentSearchCriteriaPanel;
import com.yantra.pca.ycd.rcp.tasks.common.pagination.ExtnYCDPaginationPanel;
import com.lowagie.text.PageSize;
import com.pca.rcp.hbcp2.util.HBCDateUtil;
import com.pca.rcp.hbcp2.util.HBCPCAUtil;
import com.pca.rcp.hbcp2.util.XMLUtil;
import com.yantra.yfc.rcp.YRCPaginationData;
import com.yantra.yfc.rcp.internal.YRCAboutPanel;
import com.yantra.yfc.rcp.internal.YRCApiCaller;

//Printing

/**
 * @author rajath_babu
 * Copyright IBM Corp. All Rights Reserved.
 */
public class ExtnYCDPrintPickTicketWizard extends YRCWizardExtensionBehavior {

	/* Start of SOM changes Req#1 May'14 Release */
	//declared global variable for Delivery Method initiated with value "SHP"
	String gsDeliveryMethod = "SHP";
	//Declared global variable to identify button initiated with first radio button
	String gsCallByButton = "bttnPrintForAll";
	//Declared global boolean variable for updating Delivery Method
	boolean gbUpdateDelMethod = true;
	/* End of SOM changes Req#1 May'14 Release */

	String sShipmentChecked = "N";
	String pltform = SWT.getPlatform();
	Element eleXpathForItemAttributes = null;
	NodeList nXpathForItemAttributes = null;
	//R3: Modified for defect 855- SOM batch printing and re-printing- Start
	String sCountOfShipments = "0";
	String sStatusSelected = "N";
	//R3: Modified for defect 855- SOM batch printing and re-printing- End


	/**
	 * This method initializes the behavior class.
	 * This method has been implemented to get the XPath for additional item attributes to be displayed on the pick ticket
	 * based on whether it an ECom (status = 2000) or a MerchLocatorItem (status = 3000)
	 */
	public void init()
	{

		org.w3c.dom.Document docHBCGetCommonCodeForItemAttributesXML = YRCXmlUtils.createDocument("CommonCode");
		Element eleCommonCodeInput = docHBCGetCommonCodeForItemAttributesXML.getDocumentElement();
		eleCommonCodeInput.setAttribute("OrganizationCode", HBCSOMConstants.DEFAULT_ORGANIZATION_CODE);
		eleCommonCodeInput.setAttribute("CodeType", HBCSOMConstants.PRINT_ITEM_ATTR);
		YRCApiContext context = new YRCApiContext();
		//R3: Modified for defect 855- SOM batch printing and re-printing- Start
		//Calling service to get the XPath for additional item attributes to be displayed on the pick ticket		YRCApiContext contextGetXpathForItemAttributes = new YRCApiContext();
		contextGetXpathForItemAttributes.setFormId(super.getFormId());
		contextGetXpathForItemAttributes.setApiName("HBCGetXpathForItemAttributes");
		contextGetXpathForItemAttributes.setInputXml(docHBCGetCommonCodeForItemAttributesXML);
		callApi(contextGetXpathForItemAttributes);
		org.w3c.dom.Document docHBCGetCountOfShipments = YRCXmlUtils.createDocument("Shipment");
		Element eleHBCGetShipmentListInput = docHBCGetCountOfShipments.getDocumentElement();
		Element eleUseNameSpace = this.getModel("UserNameSpace");
		String sShipNode = eleUseNameSpace.getAttribute("ShipNode");
		eleHBCGetShipmentListInput.setAttribute("PickTicketPrinted", "N");
		eleHBCGetShipmentListInput.setAttribute("QueryTimeout", "300");
		eleHBCGetShipmentListInput.setAttribute("ShipNode", sShipNode);
		eleHBCGetShipmentListInput.setAttribute("Status", "1400");
		eleHBCGetShipmentListInput.setAttribute("StatusQryType", "LT");
		Element eleShipments = docHBCGetCountOfShipments.createElement("ShipmentLines");
		Element eleShipment = docHBCGetCountOfShipments.createElement("ShipmentLine");
		Element eleOrderLine = docHBCGetCountOfShipments.createElement("OrderLine");
		Element eleOrder = docHBCGetCountOfShipments.createElement("Order");

		/* Start of SOM changes Req#1 May'14 Release */
		//Start: Update Delivery method based on radio button (Ship/Pick)selection
		if(gbUpdateDelMethod){
			YRCApiCaller apiCaller = null;
			eleOrderLine.setAttribute("DeliveryMethod", gsDeliveryMethod);
			gbUpdateDelMethod = false;

			eleHBCGetShipmentListInput.appendChild(eleShipments);
			eleShipments.appendChild(eleShipment);
			eleShipment.appendChild(eleOrderLine);
			eleShipment.appendChild(eleOrder);

			context.setFormId(super.getFormId());
			context.setApiName("HBCGetCountOfShipments");
			context.setInputXml(docHBCGetCountOfShipments);

			apiCaller = new YRCApiCaller(context,true);
			apiCaller.invokeApi();
			//Output Document contains Shipment details
			Document dHBCGetCountOfShipmentsOutput = context.getOutputXml();
			Element eleHBCGetCountOfShipmentsOutput = dHBCGetCountOfShipmentsOutput.getDocumentElement();
			eleHBCGetCountOfShipmentsOutput.setAttribute("TotalNumberOfRecords", "0");
			//Update the model content with the output document
			this.setExtentionModel("Extn_output_HBCGetCountOfShipments", eleHBCGetCountOfShipmentsOutput);
		}
		//End: Update Delivery method based on radio button (Ship/Pick)selection
		/* End of SOM changes Req#1 May'14 Release */

	}


	public String getExtnNextPage(String currentPageId) {
		//TODO
		return null;
	}

	public IYRCComposite createPage(String pageIdToBeShown) {
		//TODO
		return null;
	}

	public void pageBeingDisposed(String pageToBeDisposed) {
		//TODO
	}

	/**
	 * Called when a wizard page is about to be shown for the first time.
	 *
	 */
	public void initPage(String pageBeingShown) {
		//R3: Modified for defect 855- SOM batch printing and re-printing- Start
		if("com.yantra.pca.ycd.rcp.tasks.printPickTicket.wizardpages.YCDPrintPickTicketScreen".equalsIgnoreCase(pageBeingShown))
		{
			YRCPaginationData page = null;
			page = this.getPaginationData();
			page.setPageSize(100);
		}
		//R3: Modified for defect 855- SOM batch printing and re-printing- End
	}


	/**
	 * Method for validating the text box.
	 */
	public YRCValidationResponse validateTextField(String fieldName, String fieldValue) {
		// TODO Validation required for the following controls.

		// TODO Create and return a response.
		return super.validateTextField(fieldName, fieldValue);
	}

	/**
	 * Method for validating the combo box entry.
	 */
	public void validateComboField(String fieldName, String fieldValue) {
		// TODO Validation required for the following controls.

		// TODO Create and return a response.
		super.validateComboField(fieldName, fieldValue);
	}

	/**
	 * This method is implemented to achieve the following.
	 * 1. Select All Option - Enable the option to select all eligible shipments while printing pick tickets.
	 * 2. Customize the "Print" button so that barcode can be printed in the pick ticket.
	 */
	public YRCValidationResponse validateButtonClick(String fieldName) {

		/* Start of SOM changes Req#1 May'14 Release */
		//update global variable (gsDeliveryMethod & gsCallByButton) based on the radio button selection
		YRCValidationResponse response = null;
		//--if radio button "All shipments pending pick" is selected
		if(fieldName.equalsIgnoreCase("bttnPrintForAll")){
			//--Enable sub radio buttons
			this.enableField("extn_Ship");
			this.enableField("extn_Pick");
			gsCallByButton = "bttnPrintForAll";
		}
		//--if radio button "PICK UP Shipments" is selected
		if(fieldName.equalsIgnoreCase("extn_Pick")){
			gsCallByButton = "extn_Pick";
			gsDeliveryMethod = "PICK";//--update DeliveryMethod for Pick Up Shipments
		}
		//--if radio button "SHIPPING Shipments" is selected
		if(fieldName.equalsIgnoreCase("extn_Ship")){
			gsCallByButton = "extn_Ship";
			gsDeliveryMethod = "SHP";//--update DeliveryMethod for Shipping Shipments
		}
		//--if radio button "All shipments for order#" is selected
		if(fieldName.equalsIgnoreCase("bttnPrintForOneOrder")){
			gsCallByButton = "bttnPrintForOneOrder";
			//--Disable Sub radio buttons
			this.disableField("extn_Ship");
			this.disableField("extn_Pick");
		}
		//--if radio button "Search shipments to print pick tickets" is selected
		if(fieldName.equalsIgnoreCase("bttnSearchShipment")){
			gsCallByButton = "bttnSearchShipment";
			this.disableField("extn_Ship");
			this.disableField("extn_Pick");
			init();//--Call init method to update Shipment Lists for all Shipments
		}
		/* End of SOM changes Req#1 May'14 Release */

		if(fieldName.equals("extn_ChkSelectAll")){
			String sIsChecked= getFieldValue(fieldName);
			org.w3c.dom.Element eleShipmentList = this.getModel("getShipmentList_output");
			if (!YRCPlatformUI.isVoid(eleShipmentList)) {
				org.w3c.dom.Element eleShipments = YRCXmlUtils.getXPathElement(eleShipmentList, "Shipments");
				if (!YRCPlatformUI.isVoid(eleShipments)) {
					NodeList nShipmentList = eleShipmentList.getElementsByTagName("Shipment");
					for (int i=0; i<nShipmentList.getLength(); i++) {
						org.w3c.dom.Element eleShipment = (org.w3c.dom.Element) nShipmentList.item(i);
						if ("Y".equalsIgnoreCase(sIsChecked)) {
							eleShipment.setAttribute("Checked", "Y");
						}
						else {
							eleShipment.setAttribute("Checked", "N");
						}
						this.repopulateModel("getShipmentList_output");
					}
				}
			}
		}
		/* Start of SOM changes Req#1 May'14 Release */
		//Inserted addition check for selected button <gsCallByButton.equalsIgnoreCase("bttnSearchShipment")>
		if(fieldName.equals("extn_Print") && gsCallByButton.equalsIgnoreCase("bttnSearchShipment")){
		/* End of SOM changes Req#1 May'14 Release */
			sShipmentChecked = "N";

			//Check if the platform is Windows
			if ("win32".equals(pltform))
			{
				String sUserID = getUserID();
				org.w3c.dom.Element eleShipmentList = this.getModel("getShipmentList_output");
				if (!YRCPlatformUI.isVoid(eleShipmentList)) {
					org.w3c.dom.Element eleShipments = YRCXmlUtils.getXPathElement(eleShipmentList, "Shipments");
					if (!YRCPlatformUI.isVoid(eleShipments)) {
						NodeList nShipmentList = eleShipmentList.getElementsByTagName("Shipment");
						for (int h=0; h<nShipmentList.getLength(); h++) {
							org.w3c.dom.Element eleShipmentChecked = (org.w3c.dom.Element) nShipmentList.item(h);
							if ("Y".equalsIgnoreCase(eleShipmentChecked.getAttribute("Checked"))) {
								sShipmentChecked = "Y";
							}
						}
						if (sShipmentChecked.equalsIgnoreCase("Y"))
						{
							org.w3c.dom.Document docHBCPrintPickListInXML = YRCXmlUtils.createDocument("Shipments");
							for (int i=0; i<nShipmentList.getLength(); i++) {
								org.w3c.dom.Element eleShipment = (org.w3c.dom.Element) nShipmentList.item(i);
								if ("Y".equalsIgnoreCase(eleShipment.getAttribute("Checked"))) {
									org.w3c.dom.Element eleShipmentIn = docHBCPrintPickListInXML.createElement("Shipment");
									eleShipmentIn.setAttribute("ShipmentKey", eleShipment.getAttribute("ShipmentKey"));
									eleShipmentIn.setAttribute("EnterpriseCode", eleShipment.getAttribute("EnterpriseCode"));
									eleShipmentIn.setAttribute("Loginid", sUserID);
									docHBCPrintPickListInXML.getDocumentElement().appendChild(eleShipmentIn);
								}
							}
							//Invoke "HBCPrinrPickList" to fetch the details for generating the pick ticket
							YRCApiContext context = new YRCApiContext();
							context.setFormId(super.getFormId());
							context.setApiName("HBCPrintPickList");
							context.setInputXml(docHBCPrintPickListInXML);
							callApi(context);
						}
					}
				}
			}
		}

		/* Start of SOM changes Req#1 May'14 Release */
		//Start : Print the Shipments based on the radio button (Ship/Pick) selection
		if(fieldName.equals("extn_Print") && (gsCallByButton.equalsIgnoreCase("extn_Ship") || gsCallByButton.equalsIgnoreCase("extn_Pick"))){
			gbUpdateDelMethod = true;
			init();//--Call init method to update Shipment Lists for all Shipments

			//Check if the platform is Windows
			if ("win32".equals(pltform))
			{
				int iTotalNumberOfRecords = 0;
				int iPrintableShipment = 0;
				int iBaseShipmentTotal = 0;
				//get total number of shipment could be printed in one go from bundle file.
				String sBaseShipmentTotal = YRCPlatformUI.getString("BASE_QTY_OF_SHIPMENTS");
				//update the value of integer iBaseShipmentTotal based on fetched value of sBaseShipmentTotal
				if(!XMLUtil.isVoid(sBaseShipmentTotal)){
					iBaseShipmentTotal = Integer.parseInt(sBaseShipmentTotal);
				}
				YRCSharedTaskOutput sharedTaskOutput = null;
				String sUserID = getUserID();
				org.w3c.dom.Element eleShipmentList = this.getModel("Extn_output_HBCGetCountOfShipments");
				if (!YRCPlatformUI.isVoid(eleShipmentList)) {
					org.w3c.dom.Element eleShipments = YRCXmlUtils.getXPathElement(eleShipmentList, "Shipments");
					if (!YRCPlatformUI.isVoid(eleShipments)) {
						NodeList nShipmentList = eleShipmentList.getElementsByTagName("Shipment");
							if(!XMLUtil.isVoid(nShipmentList.item(0))){
								org.w3c.dom.Document docHBCPrintPickListInXML = YRCXmlUtils.createDocument("Shipments");
								//--Populating only 100 shipments at a time.
								iTotalNumberOfRecords = nShipmentList.getLength();
								iPrintableShipment = iTotalNumberOfRecords;
								if(iTotalNumberOfRecords > iBaseShipmentTotal){
									iPrintableShipment = iBaseShipmentTotal;
								}
								Element eleShipment = null;
								Element eleShipmentIn = null;
								for (int i=0; i<iPrintableShipment; i++) {
									eleShipment = (Element) nShipmentList.item(i);
									eleShipmentIn = docHBCPrintPickListInXML.createElement("Shipment");
									eleShipmentIn.setAttribute("ShipmentKey", eleShipment.getAttribute("ShipmentKey"));
									eleShipmentIn.setAttribute("EnterpriseCode", eleShipment.getAttribute("EnterpriseCode"));
									eleShipmentIn.setAttribute("Loginid", sUserID);
									docHBCPrintPickListInXML.getDocumentElement().appendChild(eleShipmentIn);
								}
							//Start : Insert Pop up message for confirmation on number of shipments getting printed
							if(iPrintableShipment == iBaseShipmentTotal){
							sharedTaskOutput = HBCPCAUtil.showOptionDialogBoxWithOK("Info",YRCPlatformUI.getFormattedString("PRINT_MESSAGE","")
									+ " : " + iTotalNumberOfRecords + "\n" + YRCPlatformUI.getFormattedString("PRINT_TOTAL","") + " "
									+ iPrintableShipment + " " + YRCPlatformUI.getFormattedString("PRINT_SHIPMENT","") );
							}
							else{
							sharedTaskOutput = HBCPCAUtil.showOptionDialogBoxWithOK("Info",YRCPlatformUI.getFormattedString("PRINT_MESSAGE","")
									+ " : " + iTotalNumberOfRecords + "\n" + YRCPlatformUI.getFormattedString("PRINT_ALL","") + " "
									+ iPrintableShipment + " " + YRCPlatformUI.getFormattedString("PRINT_SHIPMENT","") );
							}

							Element ele =sharedTaskOutput.getOutput();
							String strOutput = ele.getAttribute("SelectedOptionKey");
							if(XMLUtil.isVoid(strOutput)){
								response = new YRCValidationResponse(YRCValidationResponse.YRC_VALIDATION_ERROR,"");
								return response;
							}

							//End : Insert Pop up message for confirmation on number of shipments getting printed
							//Invoke "HBCPrintPickList" to fetch the details for generating the pick ticket
							YRCApiContext context = new YRCApiContext();
							context.setFormId(super.getFormId());
							context.setApiName("HBCPrintPickList");
							context.setInputXml(docHBCPrintPickListInXML);
							callApi(context);
						}
					}
				}
				//If there is no shipments available for printing, pop up will be displayed with message "No shipments available for printing"
				if(iTotalNumberOfRecords==0){
					sharedTaskOutput = HBCPCAUtil.showOptionDialogBoxWithOK("Info", YRCPlatformUI.getFormattedString("PRINT_ERROR","") );
				}
			}
		}
		//If the sub radio button (ship/pick) is not selected, pop up will be displayed with message "To proceed, please choose a sub-option: Shipping or Pick Up shipments."
		if(fieldName.equals("extn_Print") && gsCallByButton.equalsIgnoreCase("bttnPrintForAll")){
			YRCSharedTaskOutput sharedTaskOutput = HBCPCAUtil.showOptionDialogBoxWithOK("Info",YRCPlatformUI.getFormattedString("MESSAGE_SELECT_BUTTON",""));
		}
		//End : Print the Shipments based on the Button selection
		/* End of SOM changes Req#1 May'14 Release */

		return super.validateButtonClick(fieldName);
	}

	/**
	 * This method is implemented to handle the completion of custom API call
	 */
	public void handleApiCompletion(YRCApiContext context)
	{
		if ("HBCPrintPickList".equals(context.getApiName()))
		{
			this.setExtentionModel("Extn_output_HBCPrintPickList", context.getOutputXml().getDocumentElement());
			try {
				//Invoke generateBarCode() to generate the pick ticket
				generateBarCode();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//This method has been implemented handle the completion of HBCGetXpathForItemAttributes custom API call
		if ("HBCGetXpathForItemAttributes".equals(context.getApiName()))
		{
			this.setExtentionModel("Extn_output_HBCGetXpathForItemAttributes", context.getOutputXml().getDocumentElement());
			eleXpathForItemAttributes = this.getModel("Extn_output_HBCGetXpathForItemAttributes");
			if (!YRCPlatformUI.isVoid(eleXpathForItemAttributes)) {
				nXpathForItemAttributes = eleXpathForItemAttributes.getElementsByTagName("CommonCode");

			}
		}
		//R3: Modified for defect 855- SOM batch printing and re-printing- Start
		if ("HBCGetCountOfShipments".equals(context.getApiName()))
		{
			this.setExtentionModel("Extn_output_HBCGetCountOfShipments", context.getOutputXml().getDocumentElement());
			Element eleShipmentListOut = this.getModel("Extn_output_HBCGetCountOfShipments");
			if (!YRCPlatformUI.isVoid(eleShipmentListOut)) {
				sCountOfShipments = eleShipmentListOut.getAttribute("TotalNumberOfRecords");
			}
		}
		//R3: Modified for defect 855- SOM batch printing and re-printing- End
	}

	private void setModel(String string, Element documentElement) {

		// TODO Auto-generated method stub

	}

	/**
	 * Method called when a link is clicked.
	 */
	public YRCValidationResponse validateLinkClick(String fieldName) {
		// TODO Validation required for the following controls.

		// TODO Create and return a response.
		return super.validateLinkClick(fieldName);
	}

	/**
	 * Create and return the binding data for advanced table columns added to the tables.
	 */
	public YRCExtendedTableBindingData getExtendedTableBindingData(String tableName, ArrayList tableColumnNames) {
		// Create and return the binding data definition for the table.

		// The default super implementation does nothing.
		return super.getExtendedTableBindingData(tableName, tableColumnNames);
	}
	//@Override
	public boolean preCommand(YRCApiContext apiContext) {
		YRCWizard wizard=(YRCWizard)YRCDesktopUI.getCurrentPage();
		String strPageID=wizard.getCurrentPageID();
		String strFormId= wizard.getFormId();
		String strDeliveryMethod = "PICK";
		//R3:Start:- Modified for defect 905- Shipment Search based on pick ticket printed and Shipment Status
		if("getShipmentList".equalsIgnoreCase(apiContext.getApiName())){
			sStatusSelected = "N";
			Document docGetShipmentListInput = apiContext.getInputXml();
			Element eleGetShipmentListIn = docGetShipmentListInput.getDocumentElement();
			//defect 855 : - This attribute added for restricting the records to 100 per page
			eleGetShipmentListIn.setAttribute("MaximumRecords", "100");
			String sShipmentStatus = null;
			String sReprintFlag = null;
		    ArrayList childlist = super.getChildExtnBehaviors();
			if (childlist != null) {
				for (int k = 0; k < childlist.size(); k++) {
					if (childlist.get(k) instanceof ExtnYCDAdvancedShipmentSearchCriteriaPanel) {
						ExtnYCDAdvancedShipmentSearchCriteriaPanel advancedShipmentSearchCriteriaPanel = (ExtnYCDAdvancedShipmentSearchCriteriaPanel) childlist
								.get(k);

						sReprintFlag = advancedShipmentSearchCriteriaPanel.getReprintFlag();
						sShipmentStatus = advancedShipmentSearchCriteriaPanel.getSelectedStatus();
						if ( sShipmentStatus != null && !sShipmentStatus.equalsIgnoreCase(""))
						{
							eleGetShipmentListIn.setAttribute("Status",sShipmentStatus);
							eleGetShipmentListIn.removeAttribute("StatusQryType");
							sStatusSelected = "Y";
						}
						if (!sReprintFlag.equalsIgnoreCase("") && sReprintFlag!= null){
							eleGetShipmentListIn.setAttribute("PickTicketPrinted",sReprintFlag);

						}
						//Start of modification to fix Defect 1054: Pick Ticket is not getting printed for gift box lines
						//Fetch shipments irrespective of the value of GiftFlag
						Element eleOrderLine = YRCXmlUtils.getXPathElement(eleGetShipmentListIn, "Shipment/ShipmentLines/ShipmentLine/OrderLine");
						eleOrderLine.removeAttribute("GiftFlag");
						strDeliveryMethod = eleOrderLine.getAttribute("DeliveryMethod");
						//End of modification to fix Defect 1054: Pick Ticket is not getting printed for gift box lines
					}
				}
			}

			YRCApiContext context = new YRCApiContext();
			updateTotalNumberOfShipments (context, strDeliveryMethod);

		}
		//R3: End:- Modified for defect 905- Shipment Search based on pick ticket printed and Shipment Status
		return super.preCommand(apiContext);
	}

	/**
	 * This method is implemented to generate the pick list in the required format with barcode using iText
	 */
	public void generateBarCode () throws Exception 
	{
		//Format of the barcode to be used
		Barcode128 code128 = new Barcode128();
		code128.setGenerateChecksum(true);

		//Fetch the temp directory
		String sDocumentLocation = System.getProperty ("java.io.tmpdir");
		String sDocumentType = ".pdf";
//		com.lowagie.text.Document document = new com.lowagie.text.Document(new Rectangle(PageSize.A4.rotate()));
		 com.lowagie.text.Document document = new com.lowagie.text.Document(new Rectangle (842, 595)) ;
		 document.setMargins(30f, 30f, 30f, 30f);
		//Adding data to the table

		org.w3c.dom.Element eleSortedShipmentOutput = this.getModel("Extn_output_HBCPrintPickList");
		if (!YRCPlatformUI.isVoid(eleSortedShipmentOutput))
		{
			org.w3c.dom.Element eleDocumentName  =
				YRCXmlUtils.getXPathElement(eleSortedShipmentOutput,
					"Shipments/Shipment/ShipmentDetails/MultiApi/API/Output/Shipment") ;
			if (!YRCPlatformUI.isVoid(eleDocumentName))
			{
				String sDocumentName = eleDocumentName.getAttribute("ShipmentNo") ;

				String sDocumentPathAndName = sDocumentLocation+sDocumentName+sDocumentType ;
				PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(sDocumentPathAndName)) ;
				document.open();

				NodeList nSortedShipmentList =  eleSortedShipmentOutput.getElementsByTagName("ShipmentDetails") ;
				for (int g = 0; g < nSortedShipmentList.getLength(); g++)
				{
					document.newPage();
					org.w3c.dom.Element eleShipmentDetails = (org.w3c.dom.Element) nSortedShipmentList.item(g) ;
					org.w3c.dom.Element eleShipment =
						YRCXmlUtils.getXPathElement(eleShipmentDetails, "ShipmentDetails/MultiApi/API/Output/Shipment") ;
					if (!YRCPlatformUI.isVoid(eleShipment))
					{
						org.w3c.dom.Element eleToAddress =
							YRCXmlUtils.getXPathElement(eleShipment, "Shipment/ToAddress") ;

						//Start SOM changes Req#16 May'14 Release
						String sBillToDayPhone="";//Declared DayPhoneNumber variable for BillToAddress tag
						currentPageNo=1; //Setting the PageNo to 0 for each shipment
						//End SOM changes Req#16 May'14 Release

						//Start SOM changes Req#2 May'14 Release */
						String sPickUpDate = "";//Declared Pickup date variable
						//Start:Defect#1094
						org.w3c.dom.Element eleLineExtn =
							YRCXmlUtils.getXPathElement(eleShipment, "Shipment/ShipmentLines/ShipmentLine/OrderLine/Extn");
						if (!YRCPlatformUI.isVoid(eleLineExtn)){
							sPickUpDate = eleLineExtn.getAttribute("ExtnPickupDate");
						}
						//End:Defect#1094
						//End SOM changes Req#2 May'14 Release */

						//Start SOM changes Req#16 May'14 Release */
						//Start: Defect# 1089 : Fix Phone Number from BillToAddress
						org.w3c.dom.Element eleBillToAddress = YRCXmlUtils.getXPathElement(eleShipment, "Shipment/BillToAddress");
						if (!YRCPlatformUI.isVoid(eleBillToAddress)){
							sBillToDayPhone = eleBillToAddress.getAttribute("DayPhone");
						}
						//End : Defect# 1089 : Fix Phone Number from BillToAddress
						//End SOM changes Req#16 May'14 Release */

						//Checking if the Order is a BorderFree Order as part of CR
						String strIsOrderFiftyOne="";
						org.w3c.dom.Element eleOrderExtn =
							YRCXmlUtils.getXPathElement(eleShipment, "Shipment/ShipmentLines/ShipmentLine/Order/Extn");
						if (!YRCPlatformUI.isVoid(eleLineExtn)){
							strIsOrderFiftyOne = eleOrderExtn.getAttribute("ExtnIsFiftyOne");
						}

						if (!YRCPlatformUI.isVoid(eleToAddress))
						{
							String sCity = eleToAddress.getAttribute("City");
							String sState = eleToAddress.getAttribute("State");
							String sShortZipCode = eleToAddress.getAttribute("ZipCode");
							String sFormattedCityStateZipCodeStore = sCity + ' ' + sState + ' ' + sShortZipCode ;
							String sFirstName = eleToAddress.getAttribute("FirstName");
							String sLastName = eleToAddress.getAttribute("LastName");
							String sCompany = eleToAddress.getAttribute("Company");
							String sAddressLine1 = eleToAddress.getAttribute("AddressLine1");
							String sAddressLine2 = eleToAddress.getAttribute("AddressLine2");
							String sCountry = eleToAddress.getAttribute("Country");
							String sDayPhone = eleToAddress.getAttribute("DayPhone");
							String sEmailID = eleToAddress.getAttribute("EmailID");
							String sCustomerName = sFirstName+" "+sLastName;
							String sOrderDate ="";
							String sOrderNo ="";
							String sDeliveryMethod = eleShipment.getAttribute("DeliveryMethod");
							//Start: Req#1 Pick ticket Enhancement
							//Retrieving the Shipping Method for Shipping Order to display in PickTicket
							String sShippingMethod = eleShipment.getAttribute("CarrierServiceCode");
							//End: Req#1 Pick ticket Enhancement
							String sShipmentNo = eleShipment.getAttribute("ShipmentNo");
							String sShipDate = eleShipment.getAttribute("ShipDate");
							String sReqShipDate = eleShipment.getAttribute("RequestedShipmentDate");
							String sExpectedDeliveryDate = eleShipment.getAttribute("ExpectedDeliveryDate");

							//Start SOM changes Req#2 May'14 Release */
							int iNoOfDaysToAdd = 0;
							//get numbers of days to be added in pickup date display from bundle file
							String sNoOfDaysToAdd = YRCPlatformUI.getString("NO_OF_DAYS_TO_ADD");
							//update integer value (iNoOfDaysToAdd)of days from the fetched value (sNoOfDaysToAdd)
							if(!XMLUtil.isVoid(sNoOfDaysToAdd)){
								iNoOfDaysToAdd = Integer.parseInt(sNoOfDaysToAdd);
							}
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
							Calendar cCalendar = Calendar.getInstance();
							//If ExtnPickUp Date is blank or null then use RequestedShipmentDate
							if(XMLUtil.isVoid(sPickUpDate)){
								cCalendar.setTime(sdf.parse(sReqShipDate));
								cCalendar.add(Calendar.DATE, iNoOfDaysToAdd); // Adding iNoOfDaysToAdd days to RequestedShipmentDate
								sPickUpDate = sdf.format(cCalendar.getTime());
							}else{
								cCalendar.setTime(sdf.parse(sPickUpDate));
								cCalendar.add(Calendar.DATE, iNoOfDaysToAdd); // Adding iNoOfDaysToAdd days to ExtnPickUpDate
								sPickUpDate = sdf.format(cCalendar.getTime());
							}
							//End SOM changes Req#2 May'14 Release */

							String strOrderType="";
							org.w3c.dom.Element eleOrder =
								YRCXmlUtils.getXPathElement(eleShipment, "Shipment/ShipmentLines/ShipmentLine/Order") ;
							if (!YRCPlatformUI.isVoid(eleOrder))
							{
								sOrderDate = eleOrder.getAttribute("OrderDate") ;
								sOrderNo = eleOrder.getAttribute("OrderNo") ;
								strOrderType=eleOrder.getAttribute("OrderType");
								org.w3c.dom.Element eleShipNodePersonInfo =
									YRCXmlUtils.getXPathElement(eleShipment, "Shipment/ShipNode/ShipNodePersonInfo") ;
								if (!YRCPlatformUI.isVoid(eleShipNodePersonInfo))
								{
									String sStoreName = eleShipNodePersonInfo.getAttribute("Description") ;
									String sStoreCompany = eleShipNodePersonInfo.getAttribute("Company") ;
									String sStoreAddress1 = eleShipNodePersonInfo.getAttribute("AddressLine1") ;
									String sStoreAddress2 = eleShipNodePersonInfo.getAttribute("AddressLine2") ;
									String sStoreCity = eleShipNodePersonInfo.getAttribute("City") ;
									String sStoreState = eleShipNodePersonInfo.getAttribute("State") ;
									String sStoreZipCode = eleShipNodePersonInfo.getAttribute("ZipCode") ;
									String sStoreDayPhone = eleShipNodePersonInfo.getAttribute("DayPhone") ;
									String sStoreEmailID = eleShipNodePersonInfo.getAttribute("EmailID") ;
									String sStoreFormattedCityStateZipCode = sStoreCity + ' '
										+ sStoreState + ' ' + sStoreZipCode;

									//Fonts
									BaseFont bFontHeaderDetails =
										BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, false) ;
									Font fontHeaderDetails = new Font(bFontHeaderDetails, 9, Font.NORMAL, Color.BLACK) ;

									BaseFont bFontHeaderDetailsBold =
										BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, false) ;
									Font fontHeaderDetailsBold =
										new Font(bFontHeaderDetailsBold, 9, Font.BOLD, Color.BLACK) ;
									
									//Defect fix 232: Start
									Font fontHeaderDetailsBoldRed =
										new Font(bFontHeaderDetailsBold, 9, Font.BOLD, Color.RED) ;
									//Defect fix 232: Start
									
									BaseFont bfontCell =
										BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, false) ;
									Font fontCell = new Font(bfontCell, 7, Font.NORMAL, Color.BLACK) ;

									//Store Details Table
									PdfPTable storeDetailsTable = new PdfPTable(1) ;
									storeDetailsTable.setWidthPercentage(30) ;
									storeDetailsTable.getDefaultCell().setBorder(0) ;
									storeDetailsTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

									//Store Name Details
									Phrase pStoreName = new Phrase(String.format(sStoreName), fontHeaderDetailsBold) ;
									PdfPCell StoreNameDataCell = new PdfPCell() ;
									StoreNameDataCell.setPhrase(pStoreName) ;
									StoreNameDataCell.setBorder(Rectangle.NO_BORDER) ;

									//Company Details
									Phrase pStoreCompany =
										new Phrase(String.format(sStoreCompany), fontHeaderDetails) ;
									PdfPCell StoreCompanyDataCell = new PdfPCell() ;
									StoreCompanyDataCell.setPhrase(pStoreCompany) ;
									StoreCompanyDataCell.setBorder(Rectangle.NO_BORDER) ;

									//StoreAddress1 Details
									Phrase pStoreAddress1 =
										new Phrase(String.format(sStoreAddress1), fontHeaderDetails) ;
									PdfPCell StoreAddress1DataCell = new PdfPCell();
									StoreAddress1DataCell.setPhrase(pStoreAddress1);
									StoreAddress1DataCell.setBorder(Rectangle.NO_BORDER);

									//StoreAddress2 Details
									Phrase pStoreAddress2 =
										new Phrase(String.format(sStoreAddress2), fontHeaderDetails) ;
									PdfPCell StoreAddress2DataCell = new PdfPCell();
									StoreAddress2DataCell.setPhrase(pStoreAddress2);
									StoreAddress2DataCell.setBorder(Rectangle.NO_BORDER);

									//Company Details
									Phrase pStoreFormattedCityStateZipCode =
										new Phrase(String.format(sStoreFormattedCityStateZipCode), fontHeaderDetails) ;
									PdfPCell StoreFormattedCityStateZipCodeDataCell = new PdfPCell() ;
									StoreFormattedCityStateZipCodeDataCell.setPhrase(pStoreFormattedCityStateZipCode) ;
									StoreFormattedCityStateZipCodeDataCell.setBorder(Rectangle.NO_BORDER) ;

									//Store  Day Phone Details
									// Start : 05/15/2014 : Defect# 188 converting phone# to phone# format
									sStoreDayPhone = sStoreDayPhone.replaceFirst("^(\\d{3})(\\d{3})(\\d{4})$", "$1-$2-$3");  
									// End : 05/15/2014 : Defect# 188 converting phone# to phone# format
									Phrase pStoreDayPhone =
										new Phrase(String.format(sStoreDayPhone), fontHeaderDetails) ;
									PdfPCell StoreDayPhoneDataCell = new PdfPCell() ;
									StoreDayPhoneDataCell.setPhrase(pStoreDayPhone) ;
									StoreDayPhoneDataCell.setBorder(Rectangle.NO_BORDER) ;

									//Company Email Details
									Phrase pStoreEmailID =
										new Phrase(String.format(sStoreEmailID), fontHeaderDetails) ;
									PdfPCell StoreEmailIDDataCell = new PdfPCell() ;
									StoreEmailIDDataCell.setPhrase(pStoreEmailID) ;
									StoreEmailIDDataCell.setBorder(Rectangle.NO_BORDER) ;

									storeDetailsTable.addCell(StoreNameDataCell) ;
									storeDetailsTable.addCell(StoreCompanyDataCell) ;
									storeDetailsTable.addCell(StoreAddress1DataCell) ;
									//Defect fix 145: Start
									if(!sStoreAddress2.equals("") && sStoreAddress2!=null)
									{
										storeDetailsTable.addCell(StoreAddress2DataCell) ;
									}
									//Defect fix 145: End
									storeDetailsTable.addCell(StoreFormattedCityStateZipCodeDataCell) ;
									storeDetailsTable.addCell(StoreDayPhoneDataCell) ;
									storeDetailsTable.addCell(StoreEmailIDDataCell) ;

//									document.add(storeDetailsTable) ;

									//Add document Title
									PdfPCell documentName = new PdfPCell();
									Paragraph pName = addTitle(document);
									documentName.setPhrase(pName);
									documentName.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
									documentName.setBorder(Rectangle.NO_BORDER) ;

									//Start : Method call to add Customer Name at Header Title
									Paragraph pCustName = addTitleCustName(document, sCustomerName);
									PdfPCell CustomerName = new PdfPCell();
									CustomerName.setPhrase(pCustName);
									CustomerName.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
									CustomerName.setBorder(Rectangle.NO_BORDER);

									PdfPTable documentDetailsTable = new PdfPTable(1) ;
									documentDetailsTable.setWidthPercentage(30) ;
									documentDetailsTable.getDefaultCell().setBorder(0);
									documentDetailsTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

									//Empty Cell Details
									PdfPCell BlankDataCell= new PdfPCell(new Phrase(" ", fontHeaderDetails)) ;
									BlankDataCell.setBorder(Rectangle.NO_BORDER);

									//Start SOM changes Req#3 May'14 Release */
									//Start : Delivery Method Details Title Table
									PdfPTable titleDetailsTable = new PdfPTable(2) ;
//									titleDetailsTable.setSpacingBefore(10f) ;
									titleDetailsTable.setWidthPercentage(30) ;
									titleDetailsTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

									// Pick ticket Enhancement Start : Delivery Method Details
									String sDelMethod = sDeliveryMethod ;
									if(sDelMethod.equalsIgnoreCase("PICK"))
									{
										sDelMethod = YRCPlatformUI.getString("PICK");
									}
									else
									{
										//Start: Req#2 Pick ticket Enhancement
										//Displaying the Delivery Method as International shipping for Borderfree Orders.
										if(strIsOrderFiftyOne.equals("Y"))
										{
											sDelMethod = YRCPlatformUI.getString("INTERNATIONAL_SHIPPING");;
										}
										//End: Req#2 Pick ticket Enhancement
										else
										{
											sDelMethod = YRCPlatformUI.getString("SHIP");
										}
									}
									// Pick ticket Enhancement End : Delivery Method Details

									PdfPCell DeliveryMethodNameCell =
										new PdfPCell(new Phrase(YRCPlatformUI.getString("DELIVERY_METHOD"), fontHeaderDetailsBold)) ;
									DeliveryMethodNameCell.setBorder(Rectangle.NO_BORDER);
									DeliveryMethodNameCell
										.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;
									Phrase pDeliveryMethodName = new Phrase(sDelMethod, fontHeaderDetailsBold);
									PdfPCell DeliveryMethodDataCell = new PdfPCell();
									DeliveryMethodDataCell.setPhrase(pDeliveryMethodName);
									DeliveryMethodDataCell.setBorder(Rectangle.NO_BORDER);
									DeliveryMethodDataCell
										.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;
									titleDetailsTable.addCell(DeliveryMethodNameCell) ;
									titleDetailsTable.addCell(DeliveryMethodDataCell) ;

									//Start: Req#1 Pick ticket Enhancement
									//Creating and displaying a Shipping method Cell only if the Delivery Method is Shipping
									if(sDeliveryMethod.equalsIgnoreCase("SHP") && !strIsOrderFiftyOne.equals("Y"))
									{
										PdfPCell ShippingMethodNameCell =
											new PdfPCell(new Phrase(YRCPlatformUI.getString("SHIPPING_METHOD"), fontHeaderDetailsBold)) ;
										ShippingMethodNameCell.setBorder(Rectangle.NO_BORDER);
										ShippingMethodNameCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT);
										Phrase pShippingMethodName = new Phrase(sShippingMethod.toUpperCase(), fontHeaderDetailsBoldRed);
										
										//Defect fix 232: Start
										
										Element eleCommonCodeList=(Element) eleSortedShipmentOutput.
																	getElementsByTagName("CommonCodeList").item(0);
										
										if (!YRCPlatformUI.isVoid(eleCommonCodeList))
										{
											NodeList nCommonCode = eleCommonCodeList.getElementsByTagName("CommonCode");
											if(nCommonCode.getLength()>0)
											{
												String strCarrierServiceBlack ="";
												for (int iCC=0; iCC<nCommonCode.getLength(); iCC++)
												{
													Element eleCommonCode =	(Element) nCommonCode.item(iCC) ;
													strCarrierServiceBlack=eleCommonCode.getAttribute("CodeValue");
													if(sShippingMethod.equals(strCarrierServiceBlack))
													{
														pShippingMethodName = new Phrase(sShippingMethod.toUpperCase(), fontHeaderDetailsBold);
													}
												}
											}
																				
										}
										//Defect fix 232: End

										PdfPCell ShippingMethodDataCell = new PdfPCell();
										ShippingMethodDataCell.setPhrase(pShippingMethodName);
										ShippingMethodDataCell.setBorder(Rectangle.NO_BORDER);
										ShippingMethodDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;
										titleDetailsTable.addCell(ShippingMethodNameCell) ;
										titleDetailsTable.addCell(ShippingMethodDataCell) ;
									}
									//End: Req#1 Pick ticket Enhancement


									documentDetailsTable.addCell(documentName);
									documentDetailsTable.addCell(BlankDataCell);
									documentDetailsTable.addCell(CustomerName);
									documentDetailsTable.addCell(titleDetailsTable);

									PdfPTable documentHeader = new PdfPTable(new float[]{1,1,1,1,1});
									documentHeader.setWidthPercentage(100);
									documentHeader.getDefaultCell().setBorder(0);
									documentHeader.addCell(BlankDataCell);
									documentHeader.addCell(BlankDataCell);
									documentHeader.addCell(documentDetailsTable);
									documentHeader.addCell(BlankDataCell);
									documentHeader.addCell(storeDetailsTable);

									document.add(documentHeader);


//									document.add(titleDetailsTable) ;
									document.add(Chunk.NEWLINE) ;

									//--End : Delivery Method Details Title Table
									//End SOM changes Req#3 May'14 Release */

									//Anchor Table
									PdfPTable anchorTable = new PdfPTable(2) ;
									anchorTable.setWidthPercentage(100) ;
									anchorTable.getDefaultCell().setBorder(0) ;

									//Add Current Date
									PdfPTable currentDateTable = new PdfPTable(2) ;
									currentDateTable.setWidthPercentage(50) ;
									currentDateTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;

									//Current Date Details
									PdfPCell CurrentDateCell =
										new PdfPCell(new Phrase(YRCPlatformUI.getString("CURRENT_DATE"),
											fontHeaderDetailsBold)) ;
									CurrentDateCell.setBorder(Rectangle.NO_BORDER) ;
									CurrentDateCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT);
									Date date = new Date() ;
									String sDate = date.toString() ;
									Phrase pCurrentDate = new Phrase(String.format(sDate), fontHeaderDetails) ;
									PdfPCell CurrentDateDataCell = new PdfPCell() ;
									CurrentDateDataCell.setPhrase(pCurrentDate) ;
									CurrentDateDataCell.setBorder(Rectangle.NO_BORDER) ;

									//Barcode for Order No
									code128.setCode(sOrderNo);
									Image imageBarCode =
										code128.createImageWithBarcode(writer.getDirectContent(), null, null) ;

									PdfPCell OrderNoDetailsCell =
										new PdfPCell(new Phrase(YRCPlatformUI.getString("ORDER_NO"),
											fontHeaderDetailsBold));
									OrderNoDetailsCell.setBorder(Rectangle.NO_BORDER);
									OrderNoDetailsCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT);
									PdfPCell OrderNoBarcodeCell = new PdfPCell(imageBarCode,false);
									OrderNoBarcodeCell.setBorder(Rectangle.NO_BORDER);

									//Barcode for Shipment No
									code128.setCode(sShipmentNo);
									Image imageShipmentNoBarCode =
										code128.createImageWithBarcode(writer.getDirectContent(), null, null) ;

									PdfPCell ShipmentNoDetailsCell =
										new PdfPCell(new Phrase(YRCPlatformUI.getString("SHIPMENT_NO"),
											fontHeaderDetailsBold)) ;
									ShipmentNoDetailsCell.setBorder(Rectangle.NO_BORDER);
									ShipmentNoDetailsCell
										.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;
									PdfPCell ShipmentNoBarcodeCell = new PdfPCell(imageShipmentNoBarCode,false) ;
									ShipmentNoBarcodeCell.setBorder(Rectangle.NO_BORDER) ;

									//Start: Req#8 Pick ticket Enhancement
									//Displaying the No of Days Since Order Placed Cell
									PdfPCell DaysSinceOrderDateDetailsCell =
										new PdfPCell(new Phrase(String.format(YRCPlatformUI.getString("DAYS_SINCE"),
											bFontHeaderDetailsBold))) ;
									DaysSinceOrderDateDetailsCell.setBorder(Rectangle.NO_BORDER);
									DaysSinceOrderDateDetailsCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;

									//Start: Defect fix 104
									String strNoOfDays = HBCDateUtil.getDiffernceOfDateInDays(sOrderDate);
									strNoOfDays = YRCPlatformUI.getString("ORDER_PLACED") + " " + strNoOfDays+" "+YRCPlatformUI.getString("DAYS");
									//End: Defect fix 104

									Phrase pDaysSinceOrderDate = new Phrase(String.format(strNoOfDays, bFontHeaderDetailsBold));
									PdfPCell DaysSinceOrderDateDataCell = new PdfPCell() ;
									DaysSinceOrderDateDataCell.setPhrase(pDaysSinceOrderDate);
									DaysSinceOrderDateDataCell.setBorder(Rectangle.NO_BORDER) ;
									//End: Req#8 Pick ticket Enhancement

									//Empty Cell Details
									PdfPCell EmptyDataCell= new PdfPCell(new Phrase(" ", fontHeaderDetails)) ;
									EmptyDataCell.setBorder(Rectangle.NO_BORDER);
									Phrase pEmptyData = new Phrase();
									PdfPCell EmptyDateCellValue = new PdfPCell();
									EmptyDateCellValue.setPhrase(pEmptyData);
									EmptyDateCellValue.setBorder(Rectangle.NO_BORDER);

									//Add the cells to currentDateTable
									currentDateTable.addCell(CurrentDateCell);
									currentDateTable.addCell(CurrentDateDataCell);
									currentDateTable.addCell(EmptyDataCell);
									currentDateTable.addCell(EmptyDateCellValue);
									//Pick ticket Enhancement Start: Adding to display the No of Days Since OrderDate
									currentDateTable.addCell(DaysSinceOrderDateDetailsCell);
									currentDateTable.addCell(DaysSinceOrderDateDataCell);
									//Pick ticket Enhancement End: Adding to display the No of Days Since OrderDate
									currentDateTable.addCell(EmptyDataCell);
									currentDateTable.addCell(EmptyDateCellValue);
									currentDateTable.addCell(OrderNoDetailsCell);
									currentDateTable.addCell(OrderNoBarcodeCell);
									//Added to display Shipment No
									currentDateTable.addCell(EmptyDataCell);
									currentDateTable.addCell(EmptyDateCellValue);
									currentDateTable.addCell(ShipmentNoDetailsCell);
									currentDateTable.addCell(ShipmentNoBarcodeCell);


									//Check if Delivery Method is PICK
									if (sDeliveryMethod.equalsIgnoreCase("PICK"))
									{
										PdfPTable headerTable = new PdfPTable(2);
										headerTable.setWidthPercentage(50);
										headerTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);
										//Order No Details
										PdfPCell OrderNoCell =
											new PdfPCell(new Phrase(YRCPlatformUI.getString("ORDER_NO"),
												fontHeaderDetailsBold)) ;
										OrderNoCell.setBorder(Rectangle.NO_BORDER);
										Phrase pOrderNo = new Phrase(String.format(sOrderNo), fontHeaderDetails);
										PdfPCell OrderNoDataCell = new PdfPCell();
										OrderNoDataCell.setPhrase(pOrderNo);
										OrderNoDataCell.setBorder(Rectangle.NO_BORDER);

										//Shipment No Details
										PdfPCell ShipmentNoCell =
											new PdfPCell(new Phrase(YRCPlatformUI.getString("SHIPMENT_NO"),
												fontHeaderDetailsBold)) ;
										ShipmentNoCell.setBorder(Rectangle.NO_BORDER) ;
										Phrase pShipmentNo = new Phrase(String.format(sShipmentNo), fontHeaderDetails) ;
										PdfPCell ShipmentNoDataCell = new PdfPCell();
										ShipmentNoDataCell.setPhrase(pShipmentNo);
										ShipmentNoDataCell.setBorder(Rectangle.NO_BORDER);

										//Customer Name Details
										PdfPCell CustomerNameCell =
											new PdfPCell(new Phrase(YRCPlatformUI.getString("CUSTOMER_NAME"),
												fontHeaderDetailsBold)) ;
										CustomerNameCell.setBorder(Rectangle.NO_BORDER);
										sCustomerName = eleBillToAddress.getAttribute("FirstName") +" "+eleBillToAddress.getAttribute("LastName");
										Phrase pCustomerName =
											new Phrase(String.format(sCustomerName), fontHeaderDetails) ;
										PdfPCell CustomerNameDataCell = new PdfPCell() ;
										CustomerNameDataCell.setPhrase(pCustomerName) ;
										CustomerNameDataCell.setBorder(Rectangle.NO_BORDER) ;

										//Ship Date Details
										PdfPCell ShipDateCell=
											new PdfPCell(new Phrase(YRCPlatformUI.getString("STORE_NOTIFIED_ON"),
												fontHeaderDetailsBold)) ;
										ShipDateCell.setBorder(Rectangle.NO_BORDER);
										Phrase pShipDate =
											new Phrase(String.format(sShipDate.substring(0, 10)), fontHeaderDetails) ;
										PdfPCell ShipDateDataCell = new PdfPCell();
										ShipDateDataCell.setPhrase(pShipDate);
										ShipDateDataCell.setBorder(Rectangle.NO_BORDER);

										//Pickup Date Details
										PdfPCell PickUpDateCell =
											new PdfPCell(new Phrase(YRCPlatformUI.getString("PICK_UP_DATE"),
												fontHeaderDetailsBold)) ;
										PickUpDateCell.setBorder(Rectangle.NO_BORDER);

										//Start SOM changes Req#2 May'14 Release */
										//Earlier order date was getting displayed as pickup date,
										//The same is replaced with date which is calculated above  as part of this requirement
										Phrase pPickUpDate =
											new Phrase(String.format(sPickUpDate.substring(0, 10)), fontHeaderDetails) ;
										//End SOM changes Req#2 May'14 Release */

										PdfPCell PickupDateDataCell = new PdfPCell();
										PickupDateDataCell.setPhrase(pPickUpDate);
										PickupDateDataCell.setBorder(Rectangle.NO_BORDER);

										//Start SOM changes Req#16 May'14 Release */
										//Start: Added cell for "Phone No"
										// Start : 05/15/2014 : Defect# 188 converting phone# to phone# format
										sBillToDayPhone = sBillToDayPhone.replaceFirst("^(\\d{3})(\\d{3})(\\d{4})$", "$1-$2-$3");  
										// End : 05/15/2014 : Defect# 188 converting phone# to phone# format
										//Day Phone Details
										PdfPCell DayPhoneCell= new PdfPCell(new Phrase(YRCPlatformUI.getString("PHONE_NUMBER"), fontHeaderDetailsBold));
										DayPhoneCell.setBorder(Rectangle.NO_BORDER);
										Phrase pDayPhone = new Phrase(String.format(sBillToDayPhone), fontHeaderDetails);
										PdfPCell DayPhoneDataCell = new PdfPCell();
										DayPhoneDataCell.setPhrase(pDayPhone);
										DayPhoneDataCell.setBorder(Rectangle.NO_BORDER);
										//End: Added cell for "Phone No"
										//End SOM changes Req#16 May'14 Release */

										//Order Date Details
										PdfPCell OrderDateCell=
											new PdfPCell(new Phrase(YRCPlatformUI.getString("ORDER_DATE"),
												fontHeaderDetailsBold)) ;
										OrderDateCell.setBorder(Rectangle.NO_BORDER);
										Phrase pOrderDate =
											new Phrase(String.format(sOrderDate.substring(0, 10)), fontHeaderDetails) ;
										PdfPCell OrderDateDataCell = new PdfPCell();
										OrderDateDataCell.setPhrase(pOrderDate);
										OrderDateDataCell.setBorder(Rectangle.NO_BORDER);

										headerTable.addCell(OrderNoCell);
										headerTable.addCell(OrderNoDataCell);
										headerTable.addCell(ShipmentNoCell);
										headerTable.addCell(ShipmentNoDataCell);
										//Defect fix 172: start
//										headerTable.addCell(CustomerNameCell);
//										headerTable.addCell(CustomerNameDataCell);

										//Start SOM changes Req#16 May'14 Release */
//										headerTable.addCell(DayPhoneCell);//Inserted Cell for Phone No
//										headerTable.addCell(DayPhoneDataCell);// Inserted Cell for Phone No
										//End SOM changes Req#16 May'14 Release */

//										headerTable.addCell(ShipDateCell);
//										headerTable.addCell(ShipDateDataCell);
										//Defect fix 172: End

										headerTable.addCell(PickUpDateCell);
										headerTable.addCell(PickupDateDataCell);
										headerTable.addCell(OrderDateCell);
										headerTable.addCell(OrderDateDataCell);

										//Add tables to Anchor Table
										anchorTable.addCell(headerTable);
										anchorTable.addCell(currentDateTable);

										//Add Anchor Table to the document
										document.add(anchorTable);
										document.add(Chunk.NEWLINE);
									}

									//Check if Delivery Method is SHP
									if (sDeliveryMethod.equalsIgnoreCase("SHP"))
									{
										//Ship To Details And Instructions Anchor Table
										PdfPTable shipInstrAnchorTable = new PdfPTable(2);
										shipInstrAnchorTable.setWidthPercentage(100);
										shipInstrAnchorTable.getDefaultCell().setBorder(0);

										PdfPTable headerTable = new PdfPTable(2);
										headerTable.setWidthPercentage(50);
										headerTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;

										//Order No Details
										PdfPCell OrderNoCell =
											new PdfPCell(new Phrase(YRCPlatformUI.getString("ORDER_NO"),
												fontHeaderDetailsBold)) ;
										OrderNoCell.setBorder(Rectangle.NO_BORDER);
										Phrase pOrderNo = new Phrase(String.format(sOrderNo), fontHeaderDetails) ;
										PdfPCell OrderNoDataCell = new PdfPCell() ;
										OrderNoDataCell.setPhrase(pOrderNo) ;
										OrderNoDataCell.setBorder(Rectangle.NO_BORDER) ;

										//Shipment No Details
										PdfPCell ShipmentNoCell =
											new PdfPCell(new Phrase(YRCPlatformUI.getString("SHIPMENT_NO"),
												fontHeaderDetailsBold)) ;
										ShipmentNoCell.setBorder(Rectangle.NO_BORDER);
										Phrase pShipmentNo = new Phrase(String.format(sShipmentNo), fontHeaderDetails);
										PdfPCell ShipmentNoDataCell = new PdfPCell();
										ShipmentNoDataCell.setPhrase(pShipmentNo);
										ShipmentNoDataCell.setBorder(Rectangle.NO_BORDER);

										//Expected Ship Date Details
										PdfPCell ExpDeliveryDateCell =
											new PdfPCell(new Phrase(YRCPlatformUI.getString("EXPECTED_SHIP_DATE"),
												fontHeaderDetailsBold)) ;
										ExpDeliveryDateCell.setBorder(Rectangle.NO_BORDER);
										Phrase pExpDelDate =
											new Phrase(String.format(sExpectedDeliveryDate.substring(0, 10)),
												fontHeaderDetails) ;
										PdfPCell ExpDeliveryDateDataCell = new PdfPCell();
										ExpDeliveryDateDataCell.setPhrase(pExpDelDate);
										ExpDeliveryDateDataCell.setBorder(Rectangle.NO_BORDER);

										//Order Date Details
										PdfPCell OrderDateCell =
											new PdfPCell(new Phrase(YRCPlatformUI.getString("ORDER_DATE"),
												fontHeaderDetailsBold)) ;
										OrderDateCell.setBorder(Rectangle.NO_BORDER);
										Phrase pOrderDate =
											new Phrase(String.format(sOrderDate.substring(0, 10)), fontHeaderDetails) ;
										PdfPCell OrderDateDataCell = new PdfPCell();
										OrderDateDataCell.setPhrase(pOrderDate);
										OrderDateDataCell.setBorder(Rectangle.NO_BORDER);

										headerTable.addCell(OrderNoCell) ;
										headerTable.addCell(OrderNoDataCell) ;
										headerTable.addCell(ShipmentNoCell) ;
										headerTable.addCell(ShipmentNoDataCell) ;
										headerTable.addCell(ExpDeliveryDateCell) ;
										headerTable.addCell(ExpDeliveryDateDataCell) ;
										headerTable.addCell(OrderDateCell) ;
										headerTable.addCell(OrderDateDataCell) ;
//										headerTable.addCell(EmptyDataCell) ;
	//									headerTable.addCell(EmptyDateCellValue) ;

										//Table for Instructions
										PdfPTable instructionsTable = new PdfPTable(1) ;
										instructionsTable.setWidthPercentage(50) ;
										instructionsTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;
										org.w3c.dom.Element eleInstructions =
											YRCXmlUtils.getXPathElement(eleShipment, "Shipment/Instructions") ;
										if (eleInstructions != null && !(eleInstructions.equals("")))
										{
											NodeList nInstruction = eleInstructions.getElementsByTagName("Instruction");
											if (1 <= nInstruction.getLength())
											{
												PdfPCell InstructionsCell =
													new PdfPCell(new Phrase("Instructions", fontHeaderDetailsBold)) ;
												InstructionsCell.setBackgroundColor(Color.LIGHT_GRAY) ;
												instructionsTable.addCell(InstructionsCell) ;
												for (int r=0; r<nInstruction.getLength(); r++)
												{
													org.w3c.dom.Element eleInstruction =
														(org.w3c.dom.Element) nInstruction.item(r) ;
													if (eleInstruction != null && !(eleInstruction.equals("")))
													{
														String sInstructionText =
															eleInstruction.getAttribute("InstructionText") ;
														Phrase pInstructionText =
															new Phrase(String.format(sInstructionText),
																fontHeaderDetails) ;
														PdfPCell InstructionCell= new PdfPCell() ;
														InstructionCell.setPhrase(pInstructionText) ;
														InstructionCell.setBorder(Rectangle.NO_BORDER) ;
														instructionsTable.addCell(InstructionCell) ;
													}
												}
											}
										}

										//Ship To Details Table
										PdfPTable shipToTable = new PdfPTable(1);
										shipToTable.setWidthPercentage(50);
										shipToTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);

										PdfPCell ShipToCell=
											new PdfPCell(new Phrase(YRCPlatformUI.getString("SHIP_TO"),
												fontHeaderDetailsBold)) ;
										//ShipToCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
										ShipToCell.setBackgroundColor(Color.LIGHT_GRAY);

										//Customer Name Details
										Phrase pShipTo = new Phrase(String.format(sCustomerName), fontHeaderDetails) ;
										PdfPCell ShipToDataCell = new PdfPCell() ;
										ShipToDataCell.setPhrase(pShipTo) ;
										ShipToDataCell.setBorder(Rectangle.NO_BORDER) ;

										//Company Details
										Phrase pCompany = new Phrase(String.format(sCompany), fontHeaderDetails) ;
										PdfPCell CompanyDataCell = new PdfPCell();
										CompanyDataCell.setPhrase(pCompany);
										CompanyDataCell.setBorder(Rectangle.NO_BORDER);

										//AddressLine1 Details
										Phrase pAddressLine1 =
											new Phrase(String.format(sAddressLine1), fontHeaderDetails) ;
										PdfPCell AddressLine1DataCell = new PdfPCell();
										AddressLine1DataCell.setPhrase(pAddressLine1);
										AddressLine1DataCell.setBorder(Rectangle.NO_BORDER);

										//AddressLine2 Details
										Phrase pAddressLine2 =
											new Phrase(String.format(sAddressLine2), fontHeaderDetails) ;
										PdfPCell AddressLine2DataCell = new PdfPCell();
										AddressLine2DataCell.setPhrase(pAddressLine2);
										AddressLine2DataCell.setBorder(Rectangle.NO_BORDER);

										//FormattedCityStateZipCodeStore Details
										Phrase pCityStateZipCodeStore =
											new Phrase(String.format(sFormattedCityStateZipCodeStore),
												fontHeaderDetails) ;
										PdfPCell CityStateZipCodeStoreDataCell = new PdfPCell() ;
										CityStateZipCodeStoreDataCell.setPhrase(pCityStateZipCodeStore) ;
										CityStateZipCodeStoreDataCell.setBorder(Rectangle.NO_BORDER) ;

										//CountryDetails
										Phrase pCountry = new Phrase(String.format(sCountry), fontHeaderDetails);
										PdfPCell CountryDataCell = new PdfPCell();
										CountryDataCell.setPhrase(pCountry);
										CountryDataCell.setBorder(Rectangle.NO_BORDER);

										//Day Phone Details
										// Start : 05/15/2014 : Defect# 188 converting phone# to phone# format
										sDayPhone = sDayPhone.replaceFirst("^(\\d{3})(\\d{3})(\\d{4})$", "$1-$2-$3");  
										// End : 05/15/2014 : Defect# 188 converting phone# to phone# format
										Phrase pDayPhone = new Phrase(String.format(sDayPhone), fontHeaderDetails);
										PdfPCell DayPhoneDataCell = new PdfPCell();
										DayPhoneDataCell.setPhrase(pDayPhone);
										DayPhoneDataCell.setBorder(Rectangle.NO_BORDER);

										//EmailID Details
										Phrase pEmailID = new Phrase(String.format(sEmailID), fontHeaderDetails);
										PdfPCell EmailIDDataCell = new PdfPCell();
										EmailIDDataCell.setPhrase(pEmailID);
										EmailIDDataCell.setBorder(Rectangle.NO_BORDER);

										shipToTable.addCell(ShipToCell);
										shipToTable.addCell(ShipToDataCell);
										shipToTable.addCell(CompanyDataCell);
										shipToTable.addCell(AddressLine1DataCell);
										//Defect fix 145: Start
										if(!sAddressLine2.equals("") && sAddressLine2!=null)
										{
											shipToTable.addCell(AddressLine2DataCell);
										}
										//Defect fix 145: End
										shipToTable.addCell(CityStateZipCodeStoreDataCell);
										shipToTable.addCell(CountryDataCell);
										shipToTable.addCell(DayPhoneDataCell);
										shipToTable.addCell(EmailIDDataCell);
										//shipToTable.addCell(EmptyDataCell);

										//Add Tables to Anchor Table
										anchorTable.addCell(headerTable);
										anchorTable.addCell(currentDateTable);

										//Add Anchor Table to the document
										document.add(anchorTable);

										//Add Tables to Ship To Details And Instructions Anchor Table Table
										shipInstrAnchorTable.addCell(shipToTable);
										shipInstrAnchorTable.addCell(instructionsTable);

										//Add Ship To Details And Instructions Anchor Table to the document
										document.add(shipInstrAnchorTable);
										document.add(Chunk.NEWLINE);
									}

									//Start: Req#5 Pick ticket Enhancement
									//Displaying the BillTo and Secondary Shopper Details for Pickup Orders
									else
									{
										PdfPTable billInstrAnchorTable = new PdfPTable(2);
										billInstrAnchorTable.setWidthPercentage(50);
										billInstrAnchorTable.getDefaultCell().setBorder(0);
										billInstrAnchorTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);

										//Bill To Details Table
										PdfPTable billToTable = new PdfPTable(1);
										billToTable.setWidthPercentage(25);
										billToTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);

										PdfPCell BillToCell=
											new PdfPCell(new Phrase(YRCPlatformUI.getString("BILL_TO"),
												fontHeaderDetailsBold)) ;
										BillToCell.setBackgroundColor(Color.LIGHT_GRAY);
										PdfPCell ShipToCell=
											new PdfPCell(new Phrase(YRCPlatformUI.getString("ALTER_SHOPPER"),
													fontHeaderDetailsBold)) ;
										ShipToCell.setBackgroundColor(Color.LIGHT_GRAY);

										//Customer Name Details
										String strBillToName = eleBillToAddress.getAttribute("FirstName") +" "+eleBillToAddress.getAttribute("LastName");
										Phrase pShipTo = new Phrase(String.format(strBillToName), fontHeaderDetails) ;
										PdfPCell BillToDataCell = new PdfPCell() ;
										BillToDataCell.setPhrase(pShipTo) ;
										BillToDataCell.setBorder(Rectangle.NO_BORDER) ;

										//AddressLine1 Details
										Phrase pAddressLine1 =
											new Phrase(String.format(eleBillToAddress.getAttribute("AddressLine1")), fontHeaderDetails) ;
										PdfPCell AddressLine1DataCell = new PdfPCell();
										AddressLine1DataCell.setPhrase(pAddressLine1);
										AddressLine1DataCell.setBorder(Rectangle.NO_BORDER);

										//AddressLine2 Details
										Phrase pAddressLine2 =
											new Phrase(String.format(eleBillToAddress.getAttribute("AddressLine2")), fontHeaderDetails) ;
										PdfPCell AddressLine2DataCell = new PdfPCell();
										AddressLine2DataCell.setPhrase(pAddressLine2);
										AddressLine2DataCell.setBorder(Rectangle.NO_BORDER);

										//FormattedCityStateZipCodeStore Details
										String strBillToCityStateZipCode = eleBillToAddress.getAttribute("City") +" "
																		+eleBillToAddress.getAttribute("State")
																		+" "+eleBillToAddress.getAttribute("ZipCode");
										Phrase pCityStateZipCodeStore =
											new Phrase(String.format(strBillToCityStateZipCode),
												fontHeaderDetails) ;
										PdfPCell CityStateZipCodeStoreDataCell = new PdfPCell() ;
										CityStateZipCodeStoreDataCell.setPhrase(pCityStateZipCodeStore) ;
										CityStateZipCodeStoreDataCell.setBorder(Rectangle.NO_BORDER) ;

										//CountryDetails
										String strBillToCountrZipCode = eleBillToAddress.getAttribute("Country");
										Phrase pCountry = new Phrase(String.format(strBillToCountrZipCode), fontHeaderDetails);
										PdfPCell CountryDataCell = new PdfPCell();
										CountryDataCell.setPhrase(pCountry);
										CountryDataCell.setBorder(Rectangle.NO_BORDER);

										String strBillToDayPhone = eleBillToAddress.getAttribute("DayPhone");
										// Start : 05/15/2014 : Defect# 188 converting phone# to phone# format
										strBillToDayPhone = strBillToDayPhone.replaceFirst("^(\\d{3})(\\d{3})(\\d{4})$", "$1-$2-$3");  
										// End : 05/15/2014 : Defect# 188 converting phone# to phone# format
										//Day Phone Details
										Phrase pDayPhone = new Phrase(String.format(strBillToDayPhone), fontHeaderDetails);
										PdfPCell DayPhoneDataCell = new PdfPCell();
										DayPhoneDataCell.setPhrase(pDayPhone);
										DayPhoneDataCell.setBorder(Rectangle.NO_BORDER);

										billToTable.addCell(BillToCell);
										billToTable.addCell(BillToDataCell);
										billToTable.addCell(AddressLine1DataCell);
										//Defect fix 145: Start
										if(!eleBillToAddress.getAttribute("AddressLine2").equals("") &&
												eleBillToAddress.getAttribute("AddressLine2")!=null)
										{
											billToTable.addCell(AddressLine2DataCell);
										}
										//Defect fix 145: End
										billToTable.addCell(CityStateZipCodeStoreDataCell);
										billToTable.addCell(CountryDataCell);
										billToTable.addCell(DayPhoneDataCell);
										//shipToTable.addCell(EmptyDataCell);

										billInstrAnchorTable.addCell(billToTable);
										//Adding the Alternate shopper details only if the secondary shopper present
										PdfPTable AlterShopperTable = new PdfPTable(1);
										AlterShopperTable.setWidthPercentage(25);
										AlterShopperTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);

										PdfPCell AlterShopperCell=
											new PdfPCell(new Phrase(YRCPlatformUI.getString("ALTER_SHOPPER"),
												fontHeaderDetailsBold)) ;
										AlterShopperCell.setBackgroundColor(Color.LIGHT_GRAY);
										AlterShopperTable.addCell(AlterShopperCell);
										Element eleNotes= XMLUtil.getChildElement(eleOrder, "Notes");
										if(eleNotes!=null)
										{
											NodeList nNote = eleNotes.getElementsByTagName("Note");
											String strFirstName="";
											String strLastName="";
											String strDayPhoneNo="";
											String strEMailID="";
											for(int iNts=0; iNts<nNote.getLength(); iNts++)
											{
												org.w3c.dom.Element eleNote = (org.w3c.dom.Element) nNote.item(iNts) ;
												String strContactType = eleNote.getAttribute("ContactType");
												if(strContactType!=null && strContactType.equals("FIRST_NAME"))
												{
													strFirstName = eleNote.getAttribute("NoteText");
												}
												else if(strContactType!=null && strContactType.equals("LAST_NAME"))
												{
													strLastName = eleNote.getAttribute("NoteText");
												}
												else if(strContactType!=null && strContactType.equals("PHONE"))
												{
													strDayPhoneNo=eleNote.getAttribute("NoteText");
												}
												else if (strContactType!=null && strContactType.equals("PHONE"))
												{
													strEMailID=eleNote.getAttribute("NoteText");
												}
											}
											//Customer Name Details
											String strSecondaryShopperName = strFirstName +" "+strLastName;
											Phrase pSecondaryShopperName = new Phrase(String.format(strSecondaryShopperName), fontHeaderDetails) ;
											PdfPCell SecondaryShopperNameCell = new PdfPCell() ;
											SecondaryShopperNameCell.setPhrase(pSecondaryShopperName) ;
											SecondaryShopperNameCell.setBorder(Rectangle.NO_BORDER) ;

											//Day Phone Details
											// Start : 05/15/2014 : Defect# 188 converting phone# to phone# format
											strDayPhoneNo = strDayPhoneNo.replaceFirst("^(\\d{3})(\\d{3})(\\d{4})$", "$1-$2-$3");  
											// End : 05/15/2014 : Defect# 188 converting phone# to phone# format
											Phrase pSecondaryShopperPhone = new Phrase(String.format(strDayPhoneNo), fontHeaderDetails);
											PdfPCell SecondaryShopperPhoneDataCell = new PdfPCell();
											SecondaryShopperPhoneDataCell.setPhrase(pSecondaryShopperPhone);
											SecondaryShopperPhoneDataCell.setBorder(Rectangle.NO_BORDER);

											//Shopper Email Details
											Phrase pSecondaryShopperEMailID = new Phrase(String.format(strEMailID), fontHeaderDetails);
											PdfPCell SecondaryShopperEMailIDDataCell = new PdfPCell();
											SecondaryShopperEMailIDDataCell.setPhrase(pSecondaryShopperEMailID);
											SecondaryShopperEMailIDDataCell.setBorder(Rectangle.NO_BORDER);

											//Displaying the secondary shopper details if present
											if(!strFirstName.equals("")&& !strFirstName.equals(eleBillToAddress.getAttribute("FirstName")))
											{
//												AlterShopperTable.addCell(AlterShopperCell);
												AlterShopperTable.addCell(SecondaryShopperNameCell);
												AlterShopperTable.addCell(SecondaryShopperPhoneDataCell);
												if(!strEMailID.equals("")&& !strEMailID.equals(eleBillToAddress.getAttribute("EMailID")))
												{
													AlterShopperTable.addCell(SecondaryShopperEMailIDDataCell);
												}
												AlterShopperTable.addCell(EmptyDataCell);
											}
											else
											{
												AlterShopperTable.addCell(EmptyDataCell);

											}

										}
										billInstrAnchorTable.addCell(AlterShopperTable);
										document.add(billInstrAnchorTable);
										document.add(Chunk.NEWLINE);
									}
									//End: Req#5 Pick ticket Enhancement

/*									PdfPTable table =
										new PdfPTable(new float[]{ 2, 5, 4, 8, 3, 3, 3, 3, 4, 3, 2, 2, 2, 2, 3}) ;
*/
									//Pick ticket Enhancement Start: Update the table configuration as per the new CR.
									PdfPTable tableheader =
										new PdfPTable(new float[]{ 2, 5, 4, 8, 4, 3, 2, 2, 3, 3, 2,2}) ;
									tableheader.setWidthPercentage(100f) ;
									//Pick ticket Enhancement End: Update the table configuration as per the new CR.

									//Sl.No Cell
									PdfPCell SLNoCell =
										new PdfPCell(new Phrase(YRCPlatformUI.getString("SL_NO"), fontCell)) ;
									SLNoCell.setBackgroundColor(Color.LIGHT_GRAY) ;
									SLNoCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

									//UPC Code Cell
									PdfPCell UPCCodeCell =
										new PdfPCell(new Phrase(YRCPlatformUI.getString("UPC_CODE"), fontCell)) ;
									UPCCodeCell.setBackgroundColor(Color.LIGHT_GRAY);
									UPCCodeCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

									//Item ID Cell
									//Defect fix 172: Start
//									PdfPCell ItemIDCell =
//										new PdfPCell(new Phrase(YRCPlatformUI.getString("ITEM_ID"), fontCell)) ;
									PdfPCell ItemIDCell =
										new PdfPCell(new Phrase(YRCPlatformUI.getString("SKN"), fontCell)) ;
									//Defect fix 172: End
									
									ItemIDCell.setBackgroundColor(Color.LIGHT_GRAY);
									ItemIDCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

									//Item Description Cell
									PdfPCell ItemDescCell =
										new PdfPCell(new Phrase(YRCPlatformUI.getString("ITEM_DESCRIPTION"), fontCell)) ;
									ItemDescCell.setBackgroundColor(Color.LIGHT_GRAY);
									ItemDescCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

									//Original Quantity Cell
									PdfPCell OrgQtyCell =
										new PdfPCell(new Phrase(YRCPlatformUI.getString("ORIGINAL_QUANTITY"), fontCell)) ;
									OrgQtyCell.setBackgroundColor(Color.LIGHT_GRAY);
									OrgQtyCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
									//UOM Cell
									PdfPCell UOMCell= new PdfPCell(new Phrase(YRCPlatformUI.getString("UOM"), fontCell));
									UOMCell.setBackgroundColor(Color.LIGHT_GRAY);
									UOMCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
									//Quantity To Pick Cell
									PdfPCell PickQtyCell =
										new PdfPCell(new Phrase(YRCPlatformUI.getString("QUANTITY_TO_PICK"), fontCell)) ;
									PickQtyCell.setBackgroundColor(Color.LIGHT_GRAY);
									PickQtyCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

									//Picked Quantity Cell
									PdfPCell PickedQtyCell =
										new PdfPCell(new Phrase(YRCPlatformUI.getString("PICKED_QUANTITY"), fontCell)) ;
									PickedQtyCell.setBackgroundColor(Color.LIGHT_GRAY);
									PickedQtyCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

									//Department Cell
									PdfPCell DeptCell =
										new PdfPCell(new Phrase(YRCPlatformUI.getString("DEPARTMENT"), fontCell));
									DeptCell.setBackgroundColor(Color.LIGHT_GRAY);
									DeptCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

									//Division Cell
									PdfPCell DivisionCell=
										new PdfPCell(new Phrase(YRCPlatformUI.getString("DIVISION"), fontCell)) ;
									DivisionCell.setBackgroundColor(Color.LIGHT_GRAY);
									DivisionCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

									//Gift Box Cell
									PdfPCell GiftBoxCell=
										new PdfPCell(new Phrase(YRCPlatformUI.getString("GIFT_BOX"), fontCell)) ;
									GiftBoxCell.setBackgroundColor(Color.LIGHT_GRAY);
									GiftBoxCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

									//Gift Wrap Cell
									PdfPCell GiftWrapCell=
										new PdfPCell(new Phrase(YRCPlatformUI.getString("GIFT_WRAP"), fontCell)) ;
									GiftWrapCell.setBackgroundColor(Color.LIGHT_GRAY);
									GiftWrapCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

									//Color Cell
									PdfPCell ColorCell=
										new PdfPCell(new Phrase(YRCPlatformUI.getString("COLOR"), fontCell)) ;
									ColorCell.setBackgroundColor(Color.LIGHT_GRAY);
									ColorCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

									//Size Cell
									PdfPCell SizeCell=
										new PdfPCell(new Phrase(YRCPlatformUI.getString("SIZE"), fontCell)) ;
									SizeCell.setBackgroundColor(Color.LIGHT_GRAY);
									SizeCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

									//Price Cell
									PdfPCell PriceCell=
										new PdfPCell(new Phrase(YRCPlatformUI.getString("PRICE"), fontCell)) ;
									PriceCell.setBackgroundColor(Color.LIGHT_GRAY);
									PriceCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

									//Start: Req#10 Pick ticket Enhancement
									//Onhand Inventory Cell
									PdfPCell OnHandInventoryCell=
										new PdfPCell(new Phrase(YRCPlatformUI.getString("ON_HAND"), fontCell)) ;
									OnHandInventoryCell.setBackgroundColor(Color.LIGHT_GRAY);
									OnHandInventoryCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
									//Start: Req#10 Pick ticket Enhancement

									//Pick ticket Enhancement Start: Changing the Order Of the Columns.
/*									//Add the cells to the table
									table.addCell(SLNoCell);
									table.addCell(UPCCodeCell);
									table.addCell(ItemIDCell);
									table.addCell(ItemDescCell);
									table.addCell(OrgQtyCell);
									table.addCell(UOMCell);
									table.addCell(PickQtyCell);
									table.addCell(PickedQtyCell);
									table.addCell(DeptCell);
									table.addCell(DivisionCell);
									table.addCell(GiftBoxCell);
									table.addCell(GiftWrapCell);
									table.addCell(ColorCell);
									table.addCell(SizeCell);
									table.addCell(PriceCell);
*/
									tableheader.addCell(SLNoCell);
									tableheader.addCell(UPCCodeCell);
									tableheader.addCell(ItemIDCell);
									tableheader.addCell(ItemDescCell);
									tableheader.addCell(DivisionCell);
									tableheader.addCell(DeptCell);
									tableheader.addCell(ColorCell);
									tableheader.addCell(SizeCell);
									tableheader.addCell(PriceCell);
									tableheader.addCell(PickQtyCell);
									tableheader.addCell(GiftBoxCell);
									tableheader.addCell(OnHandInventoryCell);
									//Pick ticket Enhancement End: Changing the Order Of the Columns.

									document.add(tableheader);
									org.w3c.dom.Element eleShipmentLines =
										YRCXmlUtils.getXPathElement(eleShipment, "Shipment/ShipmentLines") ;
									if (!YRCPlatformUI.isVoid(eleShipmentLines))
									{
										int noOfShipmentLinesperPage=3;
										NodeList nShipmentLines =
											eleShipmentLines.getElementsByTagName("ShipmentLine");

										//Start: Req#10 Pick ticket Enhancement
										//Making a getAvailableInventory call for the displaying the Onhand Inventory
//										prepareInputForGetAvailableInv(eleShipment);
/*										 Document docGetAvailableInventory = YRCXmlUtils.createDocument("Promise");
										 Element elePromise = docGetAvailableInventory.getDocumentElement();
										 elePromise.setAttribute("OrganizationCode",YRCXmlUtils.getAttribute(eleShipment, "EnterpriseCode"));
										 Element elePromiseLines = XMLUtil.createChild(elePromise, "PromiseLines");

											for (int k=0; k<nShipmentLines.getLength(); k++)
											{
												org.w3c.dom.Element eleShipmentLine =
													(org.w3c.dom.Element) nShipmentLines.item(k) ;
												Element elePromiseLine = XMLUtil.createChild(elePromiseLines, "PromiseLine");
												elePromiseLine.setAttribute("ItemID",YRCXmlUtils.getAttribute(eleShipmentLine, "ItemID"));
												elePromiseLine.setAttribute("UnitOfMeasure",YRCXmlUtils.getAttribute(eleShipmentLine, "UnitOfMeasure"));
												elePromiseLine.setAttribute("ShipNode",YRCXmlUtils.getAttribute(eleShipment, "ShipNode"));
												elePromiseLine.setAttribute("LineId",Integer.toString(k));

											}

										YRCApiContext context = new YRCApiContext();
							            context.setApiName("HBCGetAvailableInventoryForPrinting");
							            context.setFormId(this.getFormId());
							            context.setInputXml(docGetAvailableInventory);
							            YRCApiCaller apiCaller1 = new YRCApiCaller(context, true);
							            apiCaller1.invokeApi();

							            Document docGetAvailableInvOut = context.getOutputXml();
										Element elePromiseOut = docGetAvailableInvOut.getDocumentElement();
										NodeList nPromiseLine= elePromiseOut.getElementsByTagName("PromiseLine");
										//End: Req#10 Pick ticket Enhancement

*/										
										//Defect fix 239: Start(Displaying Onhand Inventory instead of Available Inventory
										
										 Document docMultiApi = YRCXmlUtils.createDocument("MultiApi");
										 Element eleMultiApi = docMultiApi.getDocumentElement();
											for (int k=0; k<nShipmentLines.getLength(); k++)
											{
												org.w3c.dom.Element eleShipmentLine =
													(org.w3c.dom.Element) nShipmentLines.item(k) ;
												Element eleAPI = XMLUtil.createChild(eleMultiApi, "API");
												eleAPI.setAttribute("Name","getInventorySupply");
												Element eleInput = XMLUtil.createChild(eleAPI, "Input");
												Element eleInventorySupply = XMLUtil.createChild(eleInput, "InventorySupply");
												eleInventorySupply.setAttribute("UnitOfMeasure",
														YRCXmlUtils.getAttribute(eleShipmentLine, "UnitOfMeasure"));
												eleInventorySupply.setAttribute("ItemID",
																YRCXmlUtils.getAttribute(eleShipmentLine, "ItemID"));
												eleInventorySupply.setAttribute("ShipNode",YRCXmlUtils.getAttribute(eleShipment, "ShipNode"));
												eleInventorySupply.setAttribute("OrganizationCode",
														YRCXmlUtils.getAttribute(eleShipment, "EnterpriseCode"));
												eleInventorySupply.setAttribute("ValidateShipNode","Y");
												eleInventorySupply.setAttribute("SupplyType","ONHAND");
											}

										YRCApiContext context = new YRCApiContext();
							            context.setApiName("HBCGetInventorySupply");
							            context.setFormId(this.getFormId());
							            context.setInputXml(docMultiApi);
							            YRCApiCaller apiCaller1 = new YRCApiCaller(context, true);
							            apiCaller1.invokeApi();
							            
							            Document docGetInventorySupplyOut = context.getOutputXml();
										Element eleMultiAPI = docGetInventorySupplyOut.getDocumentElement();
										NodeList nItems= eleMultiAPI.getElementsByTagName("Item");
										//Defect fix 239: End (Displaying Onhand Inventory instead of Available Inventory
										
										for (int k=0; k<nShipmentLines.getLength(); k++)
										{
											//new PdfPTable(new float[]{ 2, 5, 4, 8, 4, 3, 2, 2, 3, 3, 2,2}) ;
											//Pick ticket Enhancement Start: Update the table configuration as per the new CR.
											PdfPTable table =
												new PdfPTable(new float[]{ 11, 8, 4, 3, 2, 2, 3, 3, 2,2}) ;
											table.setWidthPercentage(100f) ;
											//Pick ticket Enhancement End: Update the table configuration as per the new CR.
											
											org.w3c.dom.Element eleShipmentLine =
												(org.w3c.dom.Element) nShipmentLines.item(k) ;

											//SlNo Data Cell
											String sSlNo = String.valueOf(k+1) ;
											Phrase pSlNo = new Phrase(String.format(sSlNo), fontCell) ;
											PdfPCell SLNoDataCell = new PdfPCell() ;
											SLNoDataCell.setPhrase(pSlNo) ;

											//UPCCode Data Cell
											NodeList nOrderLines = eleShipmentLine.getElementsByTagName("OrderLine") ;
											String sUPCCode = "";
											String sExtnDepartment = "";
											String sExtnDivision = "";
											String sGiftFlag = "";
											String sGiftWrap = "";
											String sSize = "";
											String sColor = "";
										    String sPrice = "0.0";
											String sItemDescription = "";
											String sStatus = "";
											String strItemImageID= "";
											String strItemImageLocation= "";
											String sOnHand="";

											for (int l=0; l<nOrderLines.getLength(); l++)
											{
												org.w3c.dom.Element eleOrderLine =
													(org.w3c.dom.Element) nOrderLines.item(l) ;
												NodeList nItemAlias = eleOrderLine.getElementsByTagName("ItemAlias") ;
												for (int m=0; m<nItemAlias.getLength(); m++)
												{
													org.w3c.dom.Element eleItemAlias =
														(org.w3c.dom.Element) nItemAlias.item(m) ;
													if ("UPC".equalsIgnoreCase(eleItemAlias.getAttribute("AliasName")))
													{
														sUPCCode = eleItemAlias.getAttribute("AliasValue");
													}
												}
												org.w3c.dom.Element eleItemExtn =
													YRCXmlUtils.getXPathElement(eleOrderLine,
														"OrderLine/ItemDetails/Extn") ;

												if (!YRCPlatformUI.isVoid(eleItemExtn))
												{
													sExtnDepartment = eleItemExtn.getAttribute("ExtnDepartment") ;
													sExtnDivision = eleItemExtn.getAttribute("ExtnDivision") ;
												}
												//Modified to fetch item status
												org.w3c.dom.Element eleItemPrimaryInfo =
													YRCXmlUtils.getXPathElement(eleOrderLine,
														"OrderLine/ItemDetails/PrimaryInformation") ;
												if (!YRCPlatformUI.isVoid(eleItemPrimaryInfo))
												{
													sItemDescription = eleItemPrimaryInfo.getAttribute("Description") ;
													sStatus = eleItemPrimaryInfo.getAttribute("Status") ;
													//Pick ticket Enhancement Start: Changes to get the Item Image
													strItemImageLocation = eleItemPrimaryInfo.getAttribute("ImageLocation");
													strItemImageID=eleItemPrimaryInfo.getAttribute("ImageID");
													strItemImageLocation = strItemImageLocation+"/"+strItemImageID;
													//Pick ticket Enhancement End: Changes to get the Item Image

												}

												//Start: Req#9 Pick ticket Enhancement For GR Orders
												//Updating the Value of Gift Flag as Gift Registry if it is a GR Order and has Gift Box
												//Start: Defect R3.4 160,176 --If Order is GR and GiftWrap = "Y", set Gift column to Y
												sGiftWrap = eleOrderLine.getAttribute("GiftWrap") ;
												if(sGiftWrap.equalsIgnoreCase("Y")) {
													if(strOrderType.equals("GR")){
														sGiftWrap = "Gift Regst";
													}
												} else {
													sGiftWrap="No";
												}
												//End: Defect R3.4 160.176 --If Order is GR and GiftWrap = "Y", set Gift column to Y
												//End: Req#9 Pick ticket Enhancement
									
												NodeList nAdditionalAttribute =
													eleOrderLine.getElementsByTagName("AdditionalAttribute") ;
												for (int n=0; n<nAdditionalAttribute.getLength(); n++)
												{
													org.w3c.dom.Element eleAdditionalAttribute =
														(org.w3c.dom.Element) nAdditionalAttribute.item(n) ;
													//Modified to display color and size values on the pick ticket
													//based on the item status
													for (int t=0; t<nXpathForItemAttributes.getLength(); t++)
													{
														org.w3c.dom.Element eleXpathForItemAttributes =
															(org.w3c.dom.Element) nXpathForItemAttributes.item(t) ;
														String sCommonCodeValue =
															eleXpathForItemAttributes.getAttribute("CodeValue") ;
														if (sStatus.equalsIgnoreCase(sCommonCodeValue)){
															if (eleXpathForItemAttributes
																.getAttribute("CodeShortDescription")
																	.equalsIgnoreCase(eleAdditionalAttribute
																		.getAttribute("Name")))
															{
																sColor = eleAdditionalAttribute.getAttribute("Value");
															}
															if (eleXpathForItemAttributes
																.getAttribute("CodeLongDescription")
																	.equalsIgnoreCase(eleAdditionalAttribute
																		.getAttribute("Name")))
															{
																sSize = eleAdditionalAttribute.getAttribute("Value");
															}
														}
													}
												}
												org.w3c.dom.Element eleLinePriceInfo =
													YRCXmlUtils.getXPathElement(eleOrderLine,
														"OrderLine/LinePriceInfo") ;
												if (!YRCPlatformUI.isVoid(eleLinePriceInfo))
												{
													sPrice = eleLinePriceInfo.getAttribute("UnitPrice") ;
												}
											}
											PdfPTable pTbSlUPCSKN = new PdfPTable(new float[]{ 2, 5, 4});
											
											pTbSlUPCSKN.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
											
											PdfPCell pclSlNo = new PdfPCell() ;
											pclSlNo.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
											pclSlNo.setBorder(Rectangle.NO_BORDER);
											//SlNo Data Cell
											pclSlNo.setPhrase(pSlNo);
											pTbSlUPCSKN.addCell(pclSlNo);

											PdfPTable pTbUPCBarCode = new PdfPTable(1);
											pTbUPCBarCode.getDefaultCell().setBorder(Rectangle.NO_BORDER);
											pTbUPCBarCode.getDefaultCell().setBorderColor(Color.WHITE);
											PdfPCell pclUPCCode = new PdfPCell() ;
											pclUPCCode.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
											pclUPCCode.setBorder(Rectangle.NO_BORDER);
											PdfPCell pclUPCBarCode = new PdfPCell() ;
											pclUPCBarCode.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
											pclUPCBarCode.setBorder(Rectangle.NO_BORDER);
											if(!XMLUtil.isVoid(sUPCCode)){
												Phrase pUPCCode = new Phrase(String.format(sUPCCode), fontCell);
												pclUPCCode.setPhrase(pUPCCode);
												
												code128.setCode(sUPCCode);
												Image imageUPCCodeBarCode =
													code128.createImageWithBarcode(writer.getDirectContent(), null, null) ;
												pclUPCBarCode.setImage(imageUPCCodeBarCode);
											}
											pTbUPCBarCode.addCell(pclUPCCode);
											pTbUPCBarCode.addCell(pclUPCBarCode);
											
											PdfPCell pclTmpUPCBarCode = new PdfPCell() ;
											pclTmpUPCBarCode.addElement(pTbUPCBarCode);
											pclTmpUPCBarCode.setBorder(Rectangle.NO_BORDER);
											pTbSlUPCSKN.addCell(pclTmpUPCBarCode);
											
											PdfPTable pTbSKNIMG = new PdfPTable(1);
											pTbSKNIMG.getDefaultCell().setBorder(Rectangle.NO_BORDER);
											PdfPCell pclSKN = new PdfPCell() ;
											pclSKN.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
											pclSKN.setBorder(Rectangle.NO_BORDER);
											String sItemID = null;
											sItemID = eleShipmentLine.getAttribute("ItemID");
											Phrase pItemID = new Phrase(String.format(sItemID), fontCell);
											pclSKN.setPhrase(pItemID);
											
											pTbSKNIMG.addCell(pclSKN);
											
											PdfPCell pclSKNImage = new PdfPCell() ;
											pclSKNImage.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
											pclSKNImage.setBorder(Rectangle.NO_BORDER);
											try
											{
												URL urltesting =new URL(strItemImageLocation);
												Image imageItem=null;
												imageItem = Image.getInstance(urltesting);
												pclSKNImage.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
												pclSKNImage.setImage(imageItem);
												pclSKNImage.setFixedHeight(80.0f);


											}
											catch(Exception e){
												e.toString();
											}
											
											pTbSKNIMG.addCell(pclSKNImage);
											PdfPCell pclTmpSKNIMG = new PdfPCell() ;
											pclTmpSKNIMG.addElement(pTbSKNIMG);
											pclTmpSKNIMG.setBorder(Rectangle.NO_BORDER);
											
											pTbSlUPCSKN.addCell(pclTmpSKNIMG);

											//Item Desc Data Cell
											//Modified for Defect 991:Start
											Phrase pItemDesc = new Phrase(String.format(sItemDescription), fontCell);
											//Phrase pItemDesc = new Phrase(String.format(sItemDescription), fontCell);
											//Modified for Defect 991:End


											PdfPCell ItemDescDataCell = new PdfPCell();
											ItemDescDataCell.setPhrase(pItemDesc);

											//Original Quantity Data Cell
											String sQuantity = null;
											sQuantity = eleShipmentLine.getAttribute("Quantity");
											if (sQuantity.contains(".")){
												sQuantity = sQuantity.replaceAll("\\..*", "");
											}
											//Start of Fix for Defect 357, R 3.2 Feb Release
											if("0.0".equals(sQuantity) || "0".equals(sQuantity))
											{
												 continue;
											}
											//End of Fix for Defect 357, R 3.2 Feb Release

											Phrase pOrgQty = new Phrase(String.format(sQuantity), fontCell);
											PdfPCell OrgQtyDataCell = new PdfPCell();
											OrgQtyDataCell.setPhrase(pOrgQty);

											//UOM Data Cell
											String sUnitOfMeasure = null;
											sUnitOfMeasure = eleShipmentLine.getAttribute("UnitOfMeasure");
											Phrase pUOM = new Phrase(String.format(sUnitOfMeasure), fontCell);
											PdfPCell UOMDataCell = new PdfPCell();
											UOMDataCell.setPhrase(pUOM);

											//Quantity To Pick Data Cell
											String sBackroomPickedQuantity =
												eleShipmentLine.getAttribute("BackroomPickedQuantity") ;
											String sOriginalQty = eleShipmentLine.getAttribute("Quantity") ;
											double dBackRoomPickedQty = 0.00 ;
											double dOriginalQty = 0.00 ;
											double dToPickQty = 0.00 ;
											String spickedQty = "NULL" ;
											if ((!sBackroomPickedQuantity.equals(""))
												&& (!sBackroomPickedQuantity.equals(null)))
											{
												dBackRoomPickedQty = Double.parseDouble(sBackroomPickedQuantity);
												dOriginalQty = Double.parseDouble(sOriginalQty);
												dToPickQty = dOriginalQty - dBackRoomPickedQty;
												spickedQty = Double.toString(dToPickQty);
												if (spickedQty.contains("."))
												{
													spickedQty = spickedQty.replaceAll("\\..*", "") ;
												}
											}
											else
											{
												spickedQty = eleShipmentLine.getAttribute("Quantity") ;
												spickedQty = spickedQty.replaceAll("\\..*", "") ;
											}

											Phrase pPickQty = new Phrase(String.format(spickedQty), fontCell);
											PdfPCell PickQtyDataCell = new PdfPCell() ;
											PickQtyDataCell.setPhrase(pPickQty) ;

											//Picked Quantity Data Cell
											String sBackroomPickQty = "0" ;
											if ((!sBackroomPickedQuantity.equals(""))
												&& (!sBackroomPickedQuantity.equals(null)))
											{
												sBackroomPickQty = sBackroomPickedQuantity;
												sBackroomPickQty = sBackroomPickQty.replaceAll("\\..*", "") ;
											}

											//Start: Req#10 Pick ticket Enhancement
											//Retrieveing the Onhand Availability for each Item
/*											for(int iPL=0;iPL<nPromiseLine.getLength(); iPL++)
											{
												org.w3c.dom.Element elePromiseLineOut =
													(org.w3c.dom.Element) nPromiseLine.item(iPL);
												if(elePromiseLineOut.getAttribute("ItemID").equals(sItemID))
												{
													Element eleAvailability = XMLUtil.getChildElement(elePromiseLineOut, "Availability");
													Element eleAvailableInv = XMLUtil.getChildElement(eleAvailability, "AvailableInventory");
													sOnHand=eleAvailableInv.getAttribute("AvailableOnhandQuantity");
												}
											}
*/											//End: Req#10 Pick ticket Enhancement
											
											//Defect fix 239: Start(Displaying Onhand Inventory instead of Available Inventory
											for(int iPL=0;iPL<nItems.getLength(); iPL++)
											{
												org.w3c.dom.Element eleItemOut =
													(org.w3c.dom.Element) nItems.item(iPL);
												if(eleItemOut.getAttribute("ItemID").equals(sItemID))
												{
													Element eleSupplies = XMLUtil.getChildElement(eleItemOut, "Supplies");
													Element eleInventorySupply = XMLUtil.getChildElement(eleSupplies, "InventorySupply");
													if(eleInventorySupply != null)
													{
														sOnHand=eleInventorySupply.getAttribute("Quantity");
													}
												}
											}
											//Defect fix 239: Start(Displaying Onhand Inventory instead of Available Inventory

											Phrase pPickedQty = new Phrase(String.format(sBackroomPickQty), fontCell) ;
											PdfPCell PickedQtyDataCell = new PdfPCell() ;
											PickedQtyDataCell.setPhrase(pPickedQty) ;

											//Department Data Cell
											Phrase pDepartment = new Phrase(String.format(sExtnDepartment), fontCell) ;
											PdfPCell DeptDataCell = new PdfPCell() ;
											DeptDataCell.setPhrase(pDepartment) ;

											//Division Data Cell
											Phrase pDivision  = new Phrase(String.format(sExtnDivision), fontCell) ;
											PdfPCell DivDataCell = new PdfPCell() ;
											DivDataCell.setPhrase(pDivision) ;

											//GiftBox Data Cell
											Phrase pGiftBox = new Phrase(String.format(sGiftWrap), fontCell) ;
//											Phrase pGiftBox = new Phrase(String.format(sGiftFlag), fontCell) ;
											PdfPCell GiftBoxDataCell = new PdfPCell() ;
											GiftBoxDataCell.setPhrase(pGiftBox) ;

											//GiftWrap Data Cell
											Phrase pGiftWrap = new Phrase(String.format(sGiftWrap), fontCell) ;
											PdfPCell GiftWrapDataCell = new PdfPCell() ;
											GiftWrapDataCell.setPhrase(pGiftWrap) ;

											//Color Data Cell
											Phrase pColor = new Phrase(String.format(sColor), fontCell) ;
											PdfPCell ColorDataCell = new PdfPCell() ;
											ColorDataCell.setPhrase(pColor);

											//Size Data Cell
											Phrase pSize = new Phrase(String.format(sSize), fontCell);
											PdfPCell SizeDataCell = new PdfPCell();
											SizeDataCell.setPhrase(pSize);

											//Price Data Cell
											Phrase pPrice = new Phrase(String.format(sPrice), fontCell);
											PdfPCell PriceDataCell = new PdfPCell();
											PriceDataCell.setPhrase(pPrice);

											//Start: Req#10 Pick ticket Enhancement
											//On Hand Inventory Data Cell
											Phrase pOnHandInventory = new Phrase(String.format(sOnHand), fontCell);
											PdfPCell OnHandInventoryDataCell = new PdfPCell();
											OnHandInventoryDataCell.setPhrase(pOnHandInventory);
											//End: Req#10 Pick ticket Enhancement

											//Add Data cells to the table
											//Pick ticket Enhancement Start: Changing the Order of Columns in the table as per the new CR
/*											table.addCell(SLNoDataCell);
											table.addCell(itemUPCCode);
											table.addCell(itemDetails);
											table.addCell(ItemDescDataCell);
											table.addCell(OrgQtyDataCell);
											table.addCell(UOMDataCell);
											table.addCell(PickQtyDataCell);
											table.addCell(PickedQtyDataCell);
											table.addCell(DeptDataCell);
											table.addCell(DivDataCell);
											table.addCell(GiftBoxDataCell);
											table.addCell(GiftWrapDataCell);
											table.addCell(ColorDataCell);
											table.addCell(SizeDataCell);
											table.addCell(PriceDataCell);
*/
											//table.addCell(SLNoDataCell);
											//table.addCell(itemUPCCode); //UPCCodeDataCell);//
											table.addCell(pTbSlUPCSKN);
											table.addCell(ItemDescDataCell);
											table.addCell(DivDataCell);
											table.addCell(DeptDataCell);
											table.addCell(ColorDataCell);
											table.addCell(SizeDataCell);
											table.addCell(PriceDataCell);
											table.addCell(PickQtyDataCell);
											table.addCell(GiftBoxDataCell);
											table.addCell(OnHandInventoryDataCell);
											//Pick ticket Enhancement End: Changing the Order of Columns in the table as per the new CR

											if(noOfShipmentLinesperPage == Integer.valueOf(sSlNo))
											{
												document.setMargins(30f, 30f, 5f, 5f);
												document.newPage();
												noOfShipmentLinesperPage=noOfShipmentLinesperPage+5;
												currentPageNo++;
												addHeaderDetails(sOrderNo, sShipmentNo, document);
											}
											document.add(table);

										}
									}
								}
							}
						}
					}
				}

				document.close();
				if ("win32".equals(pltform))
				{
					Program.launch(sDocumentPathAndName);
				}
			}
		}
	}

	/*
	 *This method was added to include the header details
	 *for multiple pages incase the pick ticket is
	 *for more than 1 page
	 *
	 */

	int currentPageNo=0;
	public void addHeaderDetails( String sOrderNo, String sShipmentNo, com.lowagie.text.Document document1)
	{
		try
		 {
			BaseFont bfontCell =	BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, false) ;
			  Font fontCell = new Font(bfontCell, 7, Font.NORMAL, Color.BLACK) ;

			 BaseFont bFontHeaderDetails =
				 BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, false) ;
			 Font fontHeaderDetails =
				 new Font(bFontHeaderDetails, 9, Font.NORMAL, Color.BLACK) ;

			BaseFont bFontHeaderDetailsBold =
				 BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, false) ;
			 Font fontHeaderDetailsBold =
				 new Font(bFontHeaderDetailsBold, 9, Font.BOLD, Color.BLACK) ;

			//Order No Details
			 String strOrderNo = YRCPlatformUI.getString("ORDER_NO")+": ";
			 String strShipmentNo = YRCPlatformUI.getString("SHIPMENT_NO")+": ";
			 String strPageNo=YRCPlatformUI.getString("PAGE_NO")+": ";

			 String strHeaderDetails = strOrderNo+sOrderNo+" /"+strShipmentNo+
			 sShipmentNo+" /"+strPageNo+Integer.toString(currentPageNo);

			Phrase pHeaderDetails = new Phrase(String.format(strHeaderDetails), fontHeaderDetails);
			PdfPCell HeaderDetails = new PdfPCell();
			HeaderDetails.setPhrase(pHeaderDetails);
			HeaderDetails.setBorder(Rectangle.NO_BORDER);

			PdfPTable headerTable = new PdfPTable(1);
			headerTable.setWidthPercentage(50);
			headerTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);

			headerTable.addCell(HeaderDetails);

			document1.add(headerTable);

			//Empty Data Cell
			 PdfPCell EmptyDataCell =
				 new PdfPCell(new Phrase(" ", fontHeaderDetailsBold)) ;
			 EmptyDataCell.setBorder(Rectangle.NO_BORDER) ;

			PdfPTable emptytable = new PdfPTable(1) ;
			emptytable.addCell(EmptyDataCell);

			document1.add(emptytable);

			PdfPTable tableheader =
				new PdfPTable(new float[]{ 2, 5, 4, 8, 4, 3, 2, 2, 3, 3, 2,2}) ;
			tableheader.setWidthPercentage(100f) ;
			//Pick ticket Enhancement End: Update the table configuration as per the new CR.

			//Sl.No Cell
			PdfPCell SLNoCell =
				new PdfPCell(new Phrase(YRCPlatformUI.getString("SL_NO"), fontCell)) ;
			SLNoCell.setBackgroundColor(Color.LIGHT_GRAY) ;
			SLNoCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

			//UPC Code Cell
			PdfPCell UPCCodeCell =
				new PdfPCell(new Phrase(YRCPlatformUI.getString("UPC_CODE"), fontCell)) ;
			UPCCodeCell.setBackgroundColor(Color.LIGHT_GRAY);
			UPCCodeCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

			//Item ID Cell
			PdfPCell ItemIDCell =
				new PdfPCell(new Phrase(YRCPlatformUI.getString("ITEM_ID"), fontCell)) ;
			ItemIDCell.setBackgroundColor(Color.LIGHT_GRAY);
			ItemIDCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

			//Item Description Cell
			PdfPCell ItemDescCell =
				new PdfPCell(new Phrase(YRCPlatformUI.getString("ITEM_DESCRIPTION"), fontCell)) ;
			ItemDescCell.setBackgroundColor(Color.LIGHT_GRAY);
			ItemDescCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

			//Original Quantity Cell
			PdfPCell OrgQtyCell =
				new PdfPCell(new Phrase(YRCPlatformUI.getString("ORIGINAL_QUANTITY"), fontCell)) ;
			OrgQtyCell.setBackgroundColor(Color.LIGHT_GRAY);
			OrgQtyCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
			//UOM Cell
			PdfPCell UOMCell= new PdfPCell(new Phrase(YRCPlatformUI.getString("UOM"), fontCell));
			UOMCell.setBackgroundColor(Color.LIGHT_GRAY);
			UOMCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
			//Quantity To Pick Cell
			PdfPCell PickQtyCell =
				new PdfPCell(new Phrase(YRCPlatformUI.getString("QUANTITY_TO_PICK"), fontCell)) ;
			PickQtyCell.setBackgroundColor(Color.LIGHT_GRAY);
			PickQtyCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

			//Picked Quantity Cell
			PdfPCell PickedQtyCell =
				new PdfPCell(new Phrase(YRCPlatformUI.getString("PICKED_QUANTITY"), fontCell)) ;
			PickedQtyCell.setBackgroundColor(Color.LIGHT_GRAY);
			PickedQtyCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

			//Department Cell
			PdfPCell DeptCell =
				new PdfPCell(new Phrase(YRCPlatformUI.getString("DEPARTMENT"), fontCell));
			DeptCell.setBackgroundColor(Color.LIGHT_GRAY);
			DeptCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

			//Division Cell
			PdfPCell DivisionCell=
				new PdfPCell(new Phrase(YRCPlatformUI.getString("DIVISION"), fontCell)) ;
			DivisionCell.setBackgroundColor(Color.LIGHT_GRAY);
			DivisionCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

			//Gift Box Cell
			PdfPCell GiftBoxCell=
				new PdfPCell(new Phrase(YRCPlatformUI.getString("GIFT_BOX"), fontCell)) ;
			GiftBoxCell.setBackgroundColor(Color.LIGHT_GRAY);
			GiftBoxCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

			//Gift Wrap Cell
			PdfPCell GiftWrapCell=
				new PdfPCell(new Phrase(YRCPlatformUI.getString("GIFT_WRAP"), fontCell)) ;
			GiftWrapCell.setBackgroundColor(Color.LIGHT_GRAY);
			GiftWrapCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

			//Color Cell
			PdfPCell ColorCell=
				new PdfPCell(new Phrase(YRCPlatformUI.getString("COLOR"), fontCell)) ;
			ColorCell.setBackgroundColor(Color.LIGHT_GRAY);
			ColorCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

			//Size Cell
			PdfPCell SizeCell=
				new PdfPCell(new Phrase(YRCPlatformUI.getString("SIZE"), fontCell)) ;
			SizeCell.setBackgroundColor(Color.LIGHT_GRAY);
			SizeCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

			//Price Cell
			PdfPCell PriceCell=
				new PdfPCell(new Phrase(YRCPlatformUI.getString("PRICE"), fontCell)) ;
			PriceCell.setBackgroundColor(Color.LIGHT_GRAY);
			PriceCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

			//Start: Req#10 Pick ticket Enhancement
			//Onhand Inventory Cell
			PdfPCell OnHandInventoryCell=
				new PdfPCell(new Phrase(YRCPlatformUI.getString("ON_HAND"), fontCell)) ;
			OnHandInventoryCell.setBackgroundColor(Color.LIGHT_GRAY);
			OnHandInventoryCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
			tableheader.addCell(SLNoCell);
			tableheader.addCell(UPCCodeCell);
			tableheader.addCell(ItemIDCell);
			tableheader.addCell(ItemDescCell);
			tableheader.addCell(DivisionCell);
			tableheader.addCell(DeptCell);
			tableheader.addCell(ColorCell);
			tableheader.addCell(SizeCell);
			tableheader.addCell(PriceCell);
			tableheader.addCell(PickQtyCell);
			tableheader.addCell(GiftBoxCell);
			tableheader.addCell(OnHandInventoryCell);

			document1.add(tableheader);
		 }
		 catch(Exception e)
		 {
			e.printStackTrace();
		 }
	  }





	/**
	 * This method is implemented to fetch the store associate id
	 */
	public String getUserID (){
		org.w3c.dom.Element eleUserNameSpace = this.getModel("UserNameSpace");
		String sUserID = null;
		if (!YRCPlatformUI.isVoid(eleUserNameSpace)) {
			sUserID = eleUserNameSpace.getAttribute("Loginid");
		}
		return sUserID;
	}


	 /**
	 * This method is implemented to add title to the pick list
	 */
	private static Paragraph addTitle(com.lowagie.text.Document document) throws Exception {
		BaseFont bFontParagraph = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, false);
		Font fontParagraph = new Font(bFontParagraph, 15, Font.BOLD, Color.BLACK);
		Paragraph pName = new Paragraph(YRCPlatformUI.getString("PICK_TICKET"), fontParagraph);
		pName.setAlignment(com.lowagie.text.Element.ALIGN_CENTER);
//		document.add(pName);
//		document.add(Chunk.NEWLINE);
		return pName;
	}


	/**
	 * This method is implemented to add Customer Name to the pick list.
	 * Start SOM changes Req#3 May'14 Release
	 */
	private static Paragraph addTitleCustName(com.lowagie.text.Document document, String sCustName) throws Exception {
		BaseFont bFontParagraph = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, false);
		Font fontParagraph = new Font(bFontParagraph, 15, Font.BOLD, Color.BLACK);
		Paragraph pName = new Paragraph(sCustName, fontParagraph);
		pName.setAlignment(com.lowagie.text.Element.ALIGN_CENTER);
//		document.add(pName);

		return pName;
	}

	@Override
	public void postSetModel(String namespace) {
		/**
		 * This method is implemented to display Order Date on SOM UI
		 */
		if (namespace.equalsIgnoreCase("getShipmentList_output"))
		{
			Element eleShipments = this.getModel(namespace);
			NodeList nShipmentLines = eleShipments.getElementsByTagName("ShipmentLines");
			for (int n=0; n<nShipmentLines.getLength(); n++) {
				Element eleShipmentLines = this.getModel(namespace);
				NodeList nShipmentLine =  eleShipmentLines.getElementsByTagName("ShipmentLine");
				for (int p=0; p<nShipmentLine.getLength(); p++) {
					org.w3c.dom.Element eleShipmentLine = (org.w3c.dom.Element) nShipmentLine.item(p);
					Element eleOrder = YRCXmlUtils.getXPathElement(eleShipmentLine, "ShipmentLine/Order");
					String sOrderDate = eleOrder.getAttribute("OrderDate");
					if (sOrderDate!= null)
		//Modification for Date format Exception
					{
						try{
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
					SimpleDateFormat output = new SimpleDateFormat("MM/dd/yyyy");
					Date d = sdf.parse(sOrderDate);
					String sFormattedOrderDate = output.format(d);
					eleOrder.setAttribute("DateOfOrder", sFormattedOrderDate);
						}catch(Exception E){
							E.printStackTrace();
						}
					}
				}
			}
			//R3: Modified for defect 855- SOM batch printing and re-printing- Start
			ArrayList childlist = this.getChildExtnBehaviors();
			for (int i=0;i<childlist.size();i++)
			{
				if (childlist.get(i) instanceof ExtnYCDPaginationPanel) {
					ExtnYCDPaginationPanel YCDPaginationPanel = (ExtnYCDPaginationPanel) childlist.get(i);
						YCDPaginationPanel.setControlVisible("lnkGetMoreRecords", false);
				}
			}
			//R3: Modified for defect 855- SOM batch printing and re-printing- End
			this.repopulateModel(namespace);
		}
		super.postSetModel(namespace);
	}


		/**
		 * This method is implemented to get and display the total number of Pick/Ship Shipments as part of defect fix 816
		 */
	public void updateTotalNumberOfShipments (YRCApiContext context, String strDeliveryMethod)
	{
			org.w3c.dom.Document docHBCGetCountOfShipments = YRCXmlUtils.createDocument("Shipment");
			Element eleHBCGetShipmentListInput = docHBCGetCountOfShipments.getDocumentElement();
			Element eleUseNameSpace = this.getModel("UserNameSpace");
			String sShipNode = eleUseNameSpace.getAttribute("ShipNode");
			eleHBCGetShipmentListInput.setAttribute("PickTicketPrinted", "N");
			eleHBCGetShipmentListInput.setAttribute("QueryTimeout", "300");
			eleHBCGetShipmentListInput.setAttribute("ShipNode", sShipNode);
			eleHBCGetShipmentListInput.setAttribute("Status", "1400");
			eleHBCGetShipmentListInput.setAttribute("StatusQryType", "LT");

			eleHBCGetShipmentListInput.setAttribute("DeliveryMethod",strDeliveryMethod);

			context.setFormId(super.getFormId());
			context.setApiName("HBCGetCountOfShipments");
			context.setInputXml(docHBCGetCountOfShipments);
			callApi(context);

			//this.repopulateModel("Extn_output_HBCGetCountOfShipments");
	}

}