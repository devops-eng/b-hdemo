package com.yantra.pca.ycd.rcp.tasks.shipmentTracking.screens;

/**
 * Created on Jul 24,2013
 *
 */

import java.awt.Color ;
import java.io.FileOutputStream ;
import java.io.FileInputStream ;
import java.io.File ;
import java.math.BigDecimal ;
import java.math.RoundingMode ;
import java.net.URL ;
import java.util.ArrayList ;
import java.util.Date ;
import java.util.HashMap ;
import java.text.SimpleDateFormat ;
import java.util.Calendar ;
import java.text.ParseException ;
import com.pca.rcp.hbcp2.util.HBCDateUtil ;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.program.Program;
import org.w3c.dom.Document ;
import org.w3c.dom.Element ;
import org.w3c.dom.NodeList ;
import com.lowagie.text.Chunk ;
import com.lowagie.text.Font ;
import com.lowagie.text.Image ;
import com.lowagie.text.PageSize ;
import com.lowagie.text.Paragraph ;
import com.lowagie.text.Phrase ;
import com.lowagie.text.Rectangle ;
import com.lowagie.text.pdf.Barcode128 ;
import com.lowagie.text.pdf.BaseFont ;
import com.lowagie.text.pdf.PdfPCell ;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import com.pca.rcp.hbcp2.util.XMLUtil;
import com.yantra.pca.ycd.rcp.util.HBCSOMConstants;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCExtentionBehavior;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCExtendedTableBindingData;
import com.yantra.yfc.rcp.YRCXmlUtils;
import com.yantra.yfc.rcp.internal.YRCApiCaller;
import com.yantra.yfc.rcp.YRCWizard;
import com.yantra.yfc.rcp.YRCDesktopUI;
import javax.imageio.ImageIO;

/************************************************************************************
 *  FileName : ExtnYCDShipmentDetails.java
 *
 * This Class is Shipment Details screen
 *
 * Modification Log:
 * ------------------------------------------------------------------------------------
 * Version Date Author Change Log
 * ------------------------------------------------------------------------------------
 * 0.1 07-07-2014 Rajath_Babu  Initial Draft - Initial Draft
 * 1.0 07-07-2014 Paban Chetry Alignment Issue Resolved
 *
 *Copyright IBM Corp. All Rights Reserved.
 *
 ************************************************************************************/
 public class ExtnYCDShipmentDetails extends YRCExtentionBehavior {

	 String sPackSlipValidation = "";
	 String pltform = SWT.getPlatform();
	 Element eleXpathForItemAttributes = null;
	 NodeList nXpathForItemAttributes = null;

	/**
	 * This method initializes the behavior class.
	 */
	public void init()
	{
		//TODO: Write behavior init here.
		/**This method has been implemented to get the XPath for additional item attributes to be displayed on the pick ticket
		 * based on whether it an ECom (status = 2000) or a MerchLocatorItem (status = 3000)
		 */
		// Disabling the Print pack slip button initially
		this.setControlVisible("extn_print_packslip", false);
		this.setControlVisible("extn_RePrint", false);
		YRCApiContext context = new YRCApiContext();
		org.w3c.dom.Document docHBCGetCommonCodeForItemAttributesXML = YRCXmlUtils.createDocument("CommonCode");
		Element eleCommonCodeInput = docHBCGetCommonCodeForItemAttributesXML.getDocumentElement();
		eleCommonCodeInput.setAttribute("OrganizationCode", HBCSOMConstants.DEFAULT_ORGANIZATION_CODE);
		eleCommonCodeInput.setAttribute("CodeType", HBCSOMConstants.PRINT_ITEM_ATTR);
		context.setFormId(super.getFormId());
		context.setApiName("HBCGetXpathForItemAttributes") ;
		context.setInputXml(docHBCGetCommonCodeForItemAttributesXML) ;
		callApi(context) ;
	}

 	/**
	 * Method for validating the text box.
     */
    public YRCValidationResponse validateTextField(String fieldName, String fieldValue)
    {
    	// TODO Validation required for the following controls.

		// TODO Create and return a response.
		return super.validateTextField(fieldName, fieldValue) ;
	}

    /**
     * Method for validating the combo box entry.
     */
    public void validateComboField(String fieldName, String fieldValue)
    {
    	// TODO Validation required for the following controls.
		// TODO Create and return a response.
		super.validateComboField(fieldName, fieldValue) ;
    }

    /**
	 * This method is implemented to customize the "Print" button so that barcode
	 * can be printed in the pick ticket.
	 */
    public YRCValidationResponse validateButtonClick(String fieldName)
    {
    	// TODO Validation required for the following controls.

    	if(fieldName.equals("extn_print_packslip"))
    	{
    		sPackSlipValidation = "y" ;
    		//Print pack slip code for delivery method Pick.

    		if ("win32".equals(pltform))
			{
				String sUserID = getUserID();
				String sShipNode = getShipNode();
				org.w3c.dom.Document docHBCPrintPickListInXML = YRCXmlUtils.createDocument("Shipments");
				org.w3c.dom.Element eleShipment = this.getModel("Shipment");
				if (!YRCPlatformUI.isVoid(eleShipment))
				{
					org.w3c.dom.Element eleShipmentIn = docHBCPrintPickListInXML.createElement("Shipment");
					eleShipmentIn.setAttribute("Loginid", sUserID);
					eleShipmentIn.setAttribute("ShipNode", sShipNode);
					eleShipmentIn.setAttribute("SellerOrganizationCode",
						eleShipment.getAttribute("SellerOrganizationCode")) ;
					eleShipmentIn.setAttribute("ShipmentNo", eleShipment.getAttribute("ShipmentNo")) ;
					eleShipmentIn.setAttribute("EnterpriseCode", eleShipment.getAttribute("EnterpriseCode")) ;
					docHBCPrintPickListInXML.getDocumentElement().appendChild(eleShipmentIn) ;
				}

				//Invoke "HBCPrinrPickList" to fetch the details for generating the pick ticket
				YRCApiContext context = new YRCApiContext() ;
				context.setFormId(super.getFormId()) ;
				context.setApiName("HBCPrintPickList") ;
				context.setInputXml(docHBCPrintPickListInXML) ;
				callApi(context) ;
			}
    	}


    	if(fieldName.equals("extn_RePrint"))
    	{
    		//Check if the platform is Windows
			if ("win32".equals(pltform))
			{
				String sUserID = getUserID();
				String sShipNode = getShipNode();
				org.w3c.dom.Document docHBCPrintPickListInXML = YRCXmlUtils.createDocument("Shipments");
				org.w3c.dom.Element eleShipment = this.getModel("Shipment");
				if (!YRCPlatformUI.isVoid(eleShipment))
				{
					org.w3c.dom.Element eleShipmentIn = docHBCPrintPickListInXML.createElement("Shipment");
					eleShipmentIn.setAttribute("Loginid", sUserID);
					eleShipmentIn.setAttribute("ShipNode", sShipNode);
					eleShipmentIn.setAttribute("SellerOrganizationCode",
						eleShipment.getAttribute("SellerOrganizationCode")) ;
					eleShipmentIn.setAttribute("ShipmentNo", eleShipment.getAttribute("ShipmentNo"));
					eleShipmentIn.setAttribute("EnterpriseCode", eleShipment.getAttribute("EnterpriseCode"));
					docHBCPrintPickListInXML.getDocumentElement().appendChild(eleShipmentIn);
				}
				//Invoke "HBCPrinrPickList" to fetch the details for generating the pick ticket
				YRCApiContext context = new YRCApiContext();
				context.setFormId(super.getFormId());
				context.setApiName("HBCPrintPickList");
				context.setInputXml(docHBCPrintPickListInXML);
				callApi(context);
			}
    	}

		// TODO Create and return a response.
		return super.validateButtonClick(fieldName);
    }

    /**
	 * This method is implemented to handle the completion of custom API call
	 */
	public void handleApiCompletion(YRCApiContext context)
	{
		if ("HBCPrintPickList".equals(context.getApiName()))
		{
			this.setExtentionModel("Extn_output_HBCPrintPickList", context.getOutputXml().getDocumentElement());
			try {

				if(!sPackSlipValidation.equalsIgnoreCase("y"))
				{
					//Invoke generateBarCode() to generate the pick ticket
					generateBarCode() ;
				}
				else
				{
					//Invoke generateBarCodeForPackSlip() to generate the pack slip
					generateBarCodeForPackSlip() ;
				}

			} catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace() ;
			}
		}

		//This method has been implemented handle the completion of HBCGetXpathForItemAttributes custom API call
		if ("HBCGetXpathForItemAttributes".equals(context.getApiName()))
		{
			this.setExtentionModel("Extn_output_HBCGetXpathForItemAttributes", context.getOutputXml().getDocumentElement());
			eleXpathForItemAttributes = this.getModel("Extn_output_HBCGetXpathForItemAttributes");
			if (!YRCPlatformUI.isVoid(eleXpathForItemAttributes))
			{
				nXpathForItemAttributes = eleXpathForItemAttributes.getElementsByTagName("CommonCode");

			}
		}
	}

    /**
     * Method called when a link is clicked.
     */
	public YRCValidationResponse validateLinkClick(String fieldName) {
    	// TODO Validation required for the following controls.

		// TODO Create and return a response.
		return super.validateLinkClick(fieldName);
	}

	/**
	 * Create and return the binding data for advanced table columns added to the tables.
	 */
	 public YRCExtendedTableBindingData getExtendedTableBindingData(String tableName, ArrayList tableColumnNames)
	 {
	 	// Create and return the binding data definition for the table.

	 	// The defualt super implementation does nothing.
	 	return super.getExtendedTableBindingData(tableName, tableColumnNames);
	 }

	 /**
		 * This method is implemented to generate the pack slip in the required format with barcode using iText
		 */

	 PdfWriter writer ;
	 com.lowagie.text.Document document = new com.lowagie.text.Document(new Rectangle (550, 900)) ;
	 //Barcode128 code128 = new Barcode128();

	 public void generateBarCodeForPackSlip() throws Exception
	 {
		 //Format of the bar code to be used
		 Barcode128 code128 = new Barcode128() ;
		 code128.setGenerateChecksum(true);
		 code128.setBarHeight(30) ;

		 //Fetch the temp directory
		 String sDocumentLocation = System.getProperty ("java.io.tmpdir") ;
		 String sDocumentType = ".pdf" ;
		 //set size of LT Enterprise pack slip
		 //set margin for the LT Enterprise pack slip page
		 document.setMargins(5, 0, 0, 0) ;

		 //Adding data to the table
		 org.w3c.dom.Element eleSortedShipmentOutput = this.getModel("Extn_output_HBCPrintPickList") ;
		 if (!YRCPlatformUI.isVoid(eleSortedShipmentOutput))
		 {
			 org.w3c.dom.Element eleDocumentName  =
				 YRCXmlUtils.getXPathElement(eleSortedShipmentOutput,
					"Shipments/Shipment/ShipmentDetails/MultiApi/API/Output/Shipment") ;
			 if (!YRCPlatformUI.isVoid(eleDocumentName))
			 {
				 String sDocumentName = eleDocumentName.getAttribute("ShipmentNo");

				 //to set page size based on enterprise code
				 String strEnterpriseCode = eleDocumentName.getAttribute("EnterpriseCode");
				 if("BAY".equalsIgnoreCase(strEnterpriseCode))
				 {
					  document = new com.lowagie.text.Document(new Rectangle (PageSize.LETTER)) ;
					  document.setMargins(12, 18, 0, 0) ;
				 }

				 String sDocumentPathAndName = sDocumentLocation + sDocumentName + sDocumentType ;
				 writer = PdfWriter.getInstance(document, new FileOutputStream(sDocumentPathAndName)) ;
				 document.open() ;

				 NodeList nSortedShipmentList =  eleSortedShipmentOutput.getElementsByTagName("ShipmentDetails");
				 for (int g = 0; g < nSortedShipmentList.getLength(); g++)
				 {
					 document.newPage() ;
					 org.w3c.dom.Element eleShipmentDetails = (org.w3c.dom.Element) nSortedShipmentList.item(g) ;
					 org.w3c.dom.Element eleShipment = YRCXmlUtils.getXPathElement(eleShipmentDetails,
						"ShipmentDetails/MultiApi/API/Output/Shipment") ;
					 if (!YRCPlatformUI.isVoid(eleShipment))
					 {
//							HashMap<String, Double> hmShipmentLine = new HashMap<String, Double>();
							Element eleShipmentLines = XMLUtil.getChildElement(eleShipment, "ShipmentLines");
							NodeList nShipmentLine =  eleShipment.getElementsByTagName("ShipmentLine");
							updateTaxes(eleShipment);
/*							 for (int in = 0; in < nShipmentLine.getLength(); in++)
							 {
								 org.w3c.dom.Element eleShipmentLine = (org.w3c.dom.Element) nShipmentLine.item(in);
								 String strActualQuantity = eleShipmentLine.getAttribute("ActualQuantity");

								 if (!strActualQuantity.equals("0.00"))
								 {
									 hmShipmentLine.put(Integer.toString(in), Double.parseDouble("strActualQuantity")) ;
								 }
							 }
*/							 /*  */

							 Element eleShipmentTemp = YRCXmlUtils.getCopy(eleShipment);
							 int lineCount = 0 ;
							 do
							 {
								 eleShipmentLines = XMLUtil.getChildElement(eleShipmentTemp, "ShipmentLines") ;
								 //eleShipmentTemp.removeChild(eleShipmentLines);
								 XMLUtil.removeChild(eleShipmentTemp, eleShipmentLines);
								 Element eleShipmentLinesTmp = XMLUtil.createChild(eleShipmentTemp, "ShipmentLines") ;

								 for (int in = 0; in < 5; in++)
								 {
									 //Creating a temporary Shipment with only 5 lines and update with the next 5 lines.
									 org.w3c.dom.Element eleShipmentLine =
										 (org.w3c.dom.Element) nShipmentLine.item(lineCount) ;
									 //if(hmShipmentLine.containsKey(in)){
									 if(eleShipmentLine!=null)
									 {
										 String strActualQuantity = eleShipmentLine.getAttribute("ActualQuantity") ;

										 if (!strActualQuantity.equals("0.00"))
										 {
										 	//eleShipmentLinesTmp.appendChild(eleShipmentLine);
											 YRCXmlUtils.importElement(eleShipmentLinesTmp, eleShipmentLine);
											 lineCount++;
										 }
										 else
										 {
											 in = in-1 ;
											 lineCount++ ;
										 }
									 }
								 }
							 	 generateInvoice(eleShipmentTemp , code128);
							 }
							 while (lineCount < nShipmentLine.getLength());
					 	}
				 		//document.close();
				 		if ("win32".equals(pltform))
						{
							 Program.launch(sDocumentPathAndName) ;
						}
					}
					document.close() ;
		 		}
		 	}
	 	}

	HashMap<String, String> hmShipmentLine = new HashMap<String, String>();

	public void updateTaxes(Element eleShipment)
	 {

		 String sTax = "0.00";
		 String sTaxBAY="0.00";
		 String sPSTTaxBAY="0.00";
		 String sRSTTaxBAY="0.00";
		 String sQSTTaxBAY="0.00";
		 String sTotalPrice="0.00";
		 double dTotalPrice= 0.00;
		 double dLineTaxTotal = 0.00;
		 double dTotalHeaderLineTax =0.00;
		 String sGSTHSTTax = "0.00";
		 String sPSTLineTax="0.00";
		 String sRSTLineTax="0.00";
		 String sQSTLineTax="0.00";
		 double dTotalGSTHSTTax=0.0;
		 double dTotalPSTLineTax=0.00;
		 double dTotalRSTLineTax=0.00;
		 double dTotalQSTLineTax=0.00;
		 double dPSTRSTQSTHeaderTax=0.00;
		 double dPSTRSTQSTLineTax=0.00;
		 double dTotalPSTRSTQSTTax=0.00;
		 String sTotalPSTRSTQSTTax="0.00";
		 String strTotalGSTHSTTax="0.00";
		 String sOrderTotal="0.00";

		 NodeList nShipmentLine =  eleShipment.getElementsByTagName("ShipmentLine");
		 org.w3c.dom.Element eleOrder = YRCXmlUtils.getXPathElement(eleShipment,
			"Shipment/ShipmentLines/ShipmentLine/Order") ;
		 String sEnterpriseCode = eleShipment.getAttribute("EnterpriseCode");

		 org.w3c.dom.Element eleHeaderTax = YRCXmlUtils.getXPathElement(eleOrder, "Order/HeaderTaxes/HeaderTax") ;
		 if(!YRCPlatformUI.isVoid(eleHeaderTax))
		 {
			 if("SHIPPINGCHARGE".equalsIgnoreCase(eleHeaderTax.getAttribute("ChargeCategory"))
				&& "GST/HST".equalsIgnoreCase(eleHeaderTax.getAttribute("TaxName")))
			 {
				 sTaxBAY = eleHeaderTax.getAttribute("Tax");
			 }
			 if("SHIPPINGCHARGE".equalsIgnoreCase(eleHeaderTax.getAttribute("ChargeCategory"))
				&& "Provincial Sales Tax (PST)".equalsIgnoreCase(eleHeaderTax.getAttribute("TaxName")))
			 {
				 sPSTTaxBAY = eleHeaderTax.getAttribute("Tax");
			 }
			 if("SHIPPINGCHARGE".equalsIgnoreCase(eleHeaderTax.getAttribute("ChargeCategory"))
				&& "Retail Sales Tax (RST)".equalsIgnoreCase(eleHeaderTax.getAttribute("TaxName")))
			 {
				 sRSTTaxBAY = eleHeaderTax.getAttribute("Tax");
			 }
			 if("SHIPPINGCHARGE".equalsIgnoreCase(eleHeaderTax.getAttribute("ChargeCategory"))
				&& "Quebec Sales Tax (QST)".equalsIgnoreCase(eleHeaderTax.getAttribute("TaxName")))
			 {
				 sQSTTaxBAY = eleHeaderTax.getAttribute("Tax");
			 }
			 if("SHIPPINGCHARGE".equalsIgnoreCase(eleHeaderTax.getAttribute("ChargeCategory")))
			 {
				 sTax = eleHeaderTax.getAttribute("Tax");
			 }
		 }

		 for (int in = 0; in < nShipmentLine.getLength(); in++)
		 {
			 org.w3c.dom.Element eleShipmentLine = (org.w3c.dom.Element) nShipmentLine.item(in);
			 String strActualQuantity = eleShipmentLine.getAttribute("ActualQuantity");

			 if (!strActualQuantity.equals("0.00"))
			 {
//				 hmShipmentLine.put(Integer.toString(in), strActualQuantity);

				 Element eleOrderLine = XMLUtil.getChildElement(eleShipmentLine, "OrderLine") ;
				 String sGiftFlag = eleOrderLine.getAttribute("GiftFlag") ;
				 String sPackSlpPrcSuppress = eleOrderLine.getAttribute("ExtnPackSlpPrcSuppress") ;
				 String sBackroomPickQty = strActualQuantity ;
				 String sPrice = "0.00" ;
				 org.w3c.dom.Element eleLinePriceInfo =
					 YRCXmlUtils.getXPathElement(eleOrderLine, "OrderLine/LinePriceInfo") ;
				 if (!YRCPlatformUI.isVoid(eleLinePriceInfo))
				 {
					 sPrice = eleLinePriceInfo.getAttribute("UnitPrice") ;
				 }

				//Start change for defect 841 to subtract discount from unit price
				 org.w3c.dom.Element eleLineCharges =
					 YRCXmlUtils.getXPathElement(eleOrderLine, "OrderLine/LineCharges") ;
				 if (!YRCPlatformUI.isVoid(eleLineCharges))
				 {
					 NodeList nLineCharge = eleLineCharges.getElementsByTagName("LineCharge") ;
					 for (int m=0; m<nLineCharge.getLength(); m++)
					 {
						 org.w3c.dom.Element eleLineCharge = (org.w3c.dom.Element) nLineCharge.item(m) ;
						 String strChargeCategory="";
						 String strChargeName="";
						 strChargeCategory = eleLineCharge.getAttribute("ChargeCategory") ;
						 strChargeName = eleLineCharge.getAttribute("ChargeName") ;
						 if (strChargeCategory.equalsIgnoreCase("DISCOUNT")
							&& strChargeName.equalsIgnoreCase("DISCOUNT"))
						 {
							 String sDiscount = eleLineCharge.getAttribute("ChargePerUnit") ;
							 sPrice = String.valueOf((Double.valueOf(sPrice)) - (Double.valueOf(sDiscount))) ;
						 }
					 }
				 }
				 //End change for defect 841 to subtract discount from unit price
				 //check if order has GiftFlag as Y then  Total Price will display blank
				 if(("Y".equalsIgnoreCase(sGiftFlag)) || ("Y".equalsIgnoreCase(sPackSlpPrcSuppress)))
				 {
					 sPrice="0.00";
				 }

				 if(!sPrice.equals("") && sPrice!=null)
				 {
					 dTotalPrice= Double.parseDouble(sBackroomPickQty)*Double.parseDouble(sPrice);
					 sOrderTotal = String.valueOf(Double.parseDouble(sOrderTotal) + dTotalPrice);
				 }

				 if(sEnterpriseCode.equals("LT"))
				 {
					 org.w3c.dom.Element eleLineOverallTotals =
						 YRCXmlUtils.getXPathElement(eleOrderLine, "OrderLine/LineOverallTotals") ;
					 String sLineOverallTotals = eleLineOverallTotals.getAttribute("Tax");
					 dLineTaxTotal = dLineTaxTotal + Double.parseDouble(sLineOverallTotals);
					 Double dTax = 0.00;
					 dTax = Double.parseDouble(sTax);
					 dTotalHeaderLineTax = dTax + dLineTaxTotal ;
				 }
				 else if(sEnterpriseCode.equals("BAY"))
				 {
					 //to retrieve the GST/HST taxes
					 org.w3c.dom.Element eleLineTaxes =
						 YRCXmlUtils.getXPathElement(eleOrderLine, "OrderLine/LineTaxes") ;
					 NodeList nLineTax = eleLineTaxes.getElementsByTagName("LineTax");

					 for (int m=0; m<nLineTax.getLength(); m++)
					 {
						 org.w3c.dom.Element eleLineTax = (org.w3c.dom.Element) nLineTax.item(m);
						 if (!YRCPlatformUI.isVoid(eleLineTax))
						 {

							 if("GST/HST".equalsIgnoreCase(eleLineTax.getAttribute("TaxName")))
							 {
								 sGSTHSTTax = eleLineTax.getAttribute("Tax");
								 if(!XMLUtil.isVoid(sGSTHSTTax))
								 {
									dTotalGSTHSTTax+=Double.parseDouble(sGSTHSTTax);
								 }
							 }
							 if("Provincial Sales Tax (PST)".equalsIgnoreCase(eleLineTax.getAttribute("TaxName")))
							 {
								 sPSTLineTax = eleLineTax.getAttribute("Tax");
								 if(!XMLUtil.isVoid(sPSTLineTax))
								 {
									 dTotalPSTLineTax+=Double.parseDouble(sPSTLineTax) ;
								 }
							 }
							 if("Retail Sales Tax (RST)".equalsIgnoreCase(eleLineTax.getAttribute("TaxName")))
							 {
								 sRSTLineTax = eleLineTax.getAttribute("Tax");
								 if(!XMLUtil.isVoid(sRSTLineTax))
								 {
									 dTotalRSTLineTax+=Double.parseDouble(sRSTLineTax);
								 }
							 }
							 if("Quebec Sales Tax (QST)".equalsIgnoreCase(eleLineTax.getAttribute("TaxName")))
							 {
								 sQSTLineTax = eleLineTax.getAttribute("Tax");
								 if(!XMLUtil.isVoid(sQSTLineTax))
								 {
									 dTotalQSTLineTax+=Double.parseDouble(sQSTLineTax);
								 }
							 }
						 }
					 }
					 //total GST/HST taxes summing header and line level tax
					 strTotalGSTHSTTax = String.valueOf(dTotalGSTHSTTax + Double.parseDouble(sTaxBAY));
					 dPSTRSTQSTHeaderTax= Double.parseDouble(sPSTTaxBAY) + Double.parseDouble(sRSTTaxBAY)
					 	+ Double.parseDouble(sQSTTaxBAY) ;
					 dPSTRSTQSTLineTax = dTotalPSTLineTax + dTotalRSTLineTax + dTotalQSTLineTax ;
					 dTotalPSTRSTQSTTax = dPSTRSTQSTHeaderTax + dPSTRSTQSTLineTax ;
					 sTotalPSTRSTQSTTax = String.valueOf(dTotalPSTRSTQSTTax) ;
					 //Updating the taxes for Multi OrderLines
				}
			}

		 }
		 hmShipmentLine.put("OrderTotal", sOrderTotal) ;
		 if(sEnterpriseCode.equals("LT"))
		 {
			 hmShipmentLine.put("LTtax", Double.toString(dTotalHeaderLineTax)) ;
		 }
		 else if(sEnterpriseCode.equals("BAY"))
		 {
			 hmShipmentLine.put("TotalGSTHSTTax", strTotalGSTHSTTax) ;
			 hmShipmentLine.put("TotalPSTRSTQSTTax", sTotalPSTRSTQSTTax) ;
		 }
	 }


	 public void generateInvoice( Element eleShipment, Barcode128 code128)
	 {
		 try
		 {
			 org.w3c.dom.Element eleBillToAddress =
				 YRCXmlUtils.getXPathElement(eleShipment, "Shipment/BillToAddress") ;
			 if (!YRCPlatformUI.isVoid(eleBillToAddress))
			 {
				 String sCity = eleBillToAddress.getAttribute("City");
				 String sState = eleBillToAddress.getAttribute("State");
				 String sShortZipCode = eleBillToAddress.getAttribute("ZipCode");
				 String sFormattedCityStateZipCode = sCity + ' ' + sState + ' ' + sShortZipCode ; 
				 String sFirstName = eleBillToAddress.getAttribute("FirstName") ;
				 String sLastName = eleBillToAddress.getAttribute("LastName") ;
				 String sCompany = eleBillToAddress.getAttribute("Company") ;
				 String sAddressLine1 = eleBillToAddress.getAttribute("AddressLine1");
				 String sAddressLine2 = eleBillToAddress.getAttribute("AddressLine2");
				 String sCountry = eleBillToAddress.getAttribute("Country");
				 String sDayPhone = eleBillToAddress.getAttribute("DayPhone");
				 String sEmailID = eleBillToAddress.getAttribute("EmailID");
				 String sCustomerName = sFirstName+" "+sLastName;
				 String sOrderDate ="";
				 String sOrderNo ="";
				 String sExtnRegistrantName="";
				 String sExtnRegistryId="";
				 String sBillToID="";
				 String sExtnTransactionID="";
				 String sTax = "0.00";
				 String sTaxBAY="0.00";
				 String sPSTTaxBAY="0.00";
				 String sRSTTaxBAY="0.00";
				 String sQSTTaxBAY="0.00";
				 String sSCAC= eleShipment.getAttribute("SCAC");
				 String sCarrierServiceCode = eleShipment.getAttribute("CarrierServiceCode");
				 String sShippingMethod = sSCAC+"-"+sCarrierServiceCode;
				 String sDeliveryMethod = eleShipment.getAttribute("DeliveryMethod");
				 String sEnterpriseCode = eleShipment.getAttribute("EnterpriseCode");
				 String strStatus = eleShipment.getAttribute("Status");
				 String sShipmentNo = eleShipment.getAttribute("ShipmentNo");
				 String sShipDate = eleShipment.getAttribute("ShipDate");
				 String sReqShipDate = eleShipment.getAttribute("RequestedShipmentDate");
				 String sExpectedDeliveryDate = eleShipment.getAttribute("ExpectedDeliveryDate");
				 String sPaymentType = "";
				 float fCountCanceledLines=0;

				 //to verify if Carrier Service Code is blank then Shipping Method is Store Pick Up
				 if("PICK".equalsIgnoreCase(sDeliveryMethod))
				 {
					 sShippingMethod = YRCPlatformUI.getString("STORE_PICK_UP") ;
				 }

				 org.w3c.dom.Element eleToAddress =
					 YRCXmlUtils.getXPathElement(eleShipment, "Shipment/ToAddress") ;
				 String sToCity = "";
				 String sToState = "";
				 String sToShortZipCode = "";
				 String sToFormattedCityStateZipCode = "";
				 String sToFirstName = "";
				 String sToLastName = "";
				 String sToAddressLine1 = "";
				 String sToAddressLine2 = "";
				 String sToCountry = "";
				 String sToCustomerName = "" ;

				 if (!YRCPlatformUI.isVoid(eleToAddress))
				 {
					 sToCity = eleToAddress.getAttribute("City") ;
					 sToState = eleToAddress.getAttribute("State") ;
					 sToShortZipCode = eleToAddress.getAttribute("ZipCode") ;
					 sToFormattedCityStateZipCode = sToCity + ' ' + sToState + ' ' + sToShortZipCode ;
					 sToFirstName = eleToAddress.getAttribute("FirstName") ;
					 sToLastName = eleToAddress.getAttribute("LastName") ;
					 sToAddressLine1 = eleToAddress.getAttribute("AddressLine1") ;
					 sToAddressLine2 = eleToAddress.getAttribute("AddressLine2") ;
					 sToCountry = eleToAddress.getAttribute("Country") ;
					 sToCustomerName = sToFirstName + " " + sToLastName ;
				 }

				 org.w3c.dom.Element eleOrder =
					 YRCXmlUtils.getXPathElement(eleShipment, "Shipment/ShipmentLines/ShipmentLine/Order") ;
				 if (!YRCPlatformUI.isVoid(eleOrder))
				 {
					 sOrderDate = eleOrder.getAttribute("OrderDate") ;
					 sOrderNo = eleOrder.getAttribute("OrderNo") ;
					 sBillToID = eleOrder.getAttribute("BillToID") ;

					 //adding GiftRegistry Details
					org.w3c.dom.Element eleExtn = YRCXmlUtils.getXPathElement(eleOrder, "Order/Extn") ;
					if (!YRCPlatformUI.isVoid(eleExtn))
					{
						if(eleExtn.hasAttribute("ExtnRegistryID"))
						{
							sExtnRegistryId = eleExtn.getAttribute("ExtnRegistryID");
							sExtnRegistrantName = eleExtn.getAttribute("ExtnRegistrantName");
						}
					}
					 org.w3c.dom.Element eleChargeTransactionDetail =
						 YRCXmlUtils.getXPathElement(eleOrder,
							"Order/ChargeTransactionDetails/ChargeTransactionDetail");
					 String sAuthorizationID = " ";
					 if (!YRCPlatformUI.isVoid(eleChargeTransactionDetail)){
						 sAuthorizationID = eleChargeTransactionDetail.getAttribute("AuthorizationID");
					 }

					 org.w3c.dom.Element eleHeaderCharge =
						 YRCXmlUtils.getXPathElement(eleOrder, "Order/HeaderCharges/HeaderCharge") ;
					 String sChargeName="" ;
					 String sChargeAmount = "0.0" ;
					 if(!YRCPlatformUI.isVoid(eleHeaderCharge))
					 {
						 sChargeName = eleHeaderCharge.getAttribute("ChargeName");
						 if(sChargeName.equalsIgnoreCase("SHIPPINGCHARGE"))
						 {
							 sChargeAmount = eleHeaderCharge.getAttribute("ChargeAmount");
						 }
					 }

					 org.w3c.dom.Element eleHeaderTax =
						 YRCXmlUtils.getXPathElement(eleOrder, "Order/HeaderTaxes/HeaderTax") ;
					 if(!YRCPlatformUI.isVoid(eleHeaderTax))
					 {

						 if("SHIPPINGCHARGE".equalsIgnoreCase(eleHeaderTax.getAttribute("ChargeCategory"))
								 && "GST/HST".equalsIgnoreCase(eleHeaderTax.getAttribute("TaxName")))
						 {
							 sTaxBAY = eleHeaderTax.getAttribute("Tax");

						 }
						 if("SHIPPINGCHARGE".equalsIgnoreCase(eleHeaderTax.getAttribute("ChargeCategory"))
								 && "Provincial Sales Tax (PST)".equalsIgnoreCase(eleHeaderTax.getAttribute("TaxName")))
						 {
							 sPSTTaxBAY = eleHeaderTax.getAttribute("Tax");
						 }
						 if("SHIPPINGCHARGE".equalsIgnoreCase(eleHeaderTax.getAttribute("ChargeCategory"))
								 && "Retail Sales Tax (RST)".equalsIgnoreCase(eleHeaderTax.getAttribute("TaxName")))
						 {
							 sRSTTaxBAY = eleHeaderTax.getAttribute("Tax");
						 }
						 if("SHIPPINGCHARGE".equalsIgnoreCase(eleHeaderTax.getAttribute("ChargeCategory"))
								 && "Quebec Sales Tax (QST)".equalsIgnoreCase(eleHeaderTax.getAttribute("TaxName")))
						 {
							 sQSTTaxBAY = eleHeaderTax.getAttribute("Tax");
						 }
						 if("SHIPPINGCHARGE".equalsIgnoreCase(eleHeaderTax.getAttribute("ChargeCategory")))
						 {
							 sTax = eleHeaderTax.getAttribute("Tax") ;
						 }
					 }

					 //retrieve Payment Type to populate if PAYPAL is the Payment Type
					 org.w3c.dom.Element elePaymentMethod =
						 YRCXmlUtils.getXPathElement(eleOrder, "Order/PaymentMethods/PaymentMethod") ;

					 if(!YRCPlatformUI.isVoid(elePaymentMethod))
					 {
						 sPaymentType = elePaymentMethod.getAttribute("PaymentType") ;
						 if(sPaymentType.equalsIgnoreCase("PAYPAL_LT")
							|| sPaymentType.equalsIgnoreCase("PAYPAL_HBC"))
						 {
							 sPaymentType = "PAYPAL" ;
						 }
					 else
					 {
						 sPaymentType = "" ;
					 }
				 }

				 //retrieve ExtnTransactionID for each orderLine
				 org.w3c.dom.Element eleExtnOrderLine =
					 YRCXmlUtils.getXPathElement(eleShipment,
						"Shipment/ShipmentLines/ShipmentLine/OrderLine/Extn") ;
				 if(!YRCPlatformUI.isVoid(eleExtnOrderLine))
				 {
					 sExtnTransactionID = eleExtnOrderLine.getAttribute("ExtnTransactionID");
				 }

				 //fetching ShipTo address
				 org.w3c.dom.Element eleShipNodePersonInfo =
					 YRCXmlUtils.getXPathElement(eleShipment, "Shipment/ShipNode/ShipNodePersonInfo") ;
				 if (!YRCPlatformUI.isVoid(eleShipNodePersonInfo))
				 {
					 String sStoreName = eleShipNodePersonInfo.getAttribute("Description") ;
					 String sStoreCompany = eleShipNodePersonInfo.getAttribute("Company") ;
					 String sStoreAddress1 = eleShipNodePersonInfo.getAttribute("AddressLine1") ;
					 String sStoreAddress2 = eleShipNodePersonInfo.getAttribute("AddressLine2") ;
					 String sStoreCity = eleShipNodePersonInfo.getAttribute("City") ;
					 String sStoreState = eleShipNodePersonInfo.getAttribute("State") ;
					 String sStoreZipCode = eleShipNodePersonInfo.getAttribute("ZipCode") ;
					 String sStoreDayPhone = eleShipNodePersonInfo.getAttribute("DayPhone") ;
					 String sStoreEmailID = eleShipNodePersonInfo.getAttribute("EmailID") ;
					 String sStoreFormattedCityStateZipCode = sStoreCity + ' '
					 	+ sStoreState + ' ' + sStoreZipCode ;
					 String sStoreFirstName = eleShipNodePersonInfo.getAttribute("FirstName") ;
					 String sStoreLastName = eleShipNodePersonInfo.getAttribute("LastName") ;
					 String sFormatCustomerName = sStoreFirstName + " " + sStoreLastName ;
					 String sStoreCountry = eleShipNodePersonInfo.getAttribute("Country") ;

					 //Fonts
					 BaseFont bFontHeaderDetails =
						 BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1250, false) ;
					 Font fontHeaderDetails =
						 new Font(bFontHeaderDetails, 7, Font.NORMAL, Color.BLACK) ;

					 BaseFont bFontHeaderDetailsBold =
						 BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1250, false) ;
					 Font fontHeaderDetailsBold =
						 new Font(bFontHeaderDetailsBold, 8, Font.BOLD, Color.BLACK) ;

					 BaseFont bfontCell =
						 BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1250, false);
					 Font fontCell = new Font(bfontCell, 7, Font.NORMAL, Color.BLACK) ;
					 Font boldFontCell = new Font(bfontCell, 7, Font.BOLD, Color.BLACK) ;

					 BaseFont bfontAddressCell =
						 BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1250, false) ;
					 Font fontAddressCell = new Font(bfontAddressCell, 7, Font.NORMAL, Color.BLACK) ;

					 //creation of pack slip for LT enterprise
					 if (sEnterpriseCode.equalsIgnoreCase("LT"))
					 {
						 //main table to which all the tables are added
						 PdfPTable mainTable = new PdfPTable(1) ;
						 mainTable.setWidthPercentage(100) ;
						 mainTable.getDefaultCell().setBorder(0) ;
						 mainTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

						 //Header Table having enterprise logo and other details
						 PdfPTable headerTable = new PdfPTable(1);
						 headerTable.setWidthPercentage(100);
						 headerTable.getDefaultCell().setBorder(1);
						 headerTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

						 //Header Table Details
						 Phrase pHeaderDetails = new Phrase("", fontHeaderDetailsBold);
						 PdfPCell HeaderDetailsDataCell = new PdfPCell();
						 HeaderDetailsDataCell.setPhrase(pHeaderDetails);
						 //HeaderDetailsDataCell.setFixedHeight(5f);
						 HeaderDetailsDataCell.setBorder(Rectangle.NO_BORDER);

						 //adding header table details to header table
						 headerTable.addCell(HeaderDetailsDataCell);
						 //adding header table to the main table
						 mainTable.addCell(headerTable);

						 //Shipping Billing address and order details Table
						 PdfPTable addressTable = new PdfPTable(3);
						 addressTable.setWidthPercentage(100) ;
						 addressTable.setWidths(new int[]{3, 3, 4}) ;
						 addressTable.getDefaultCell().setBorder(Rectangle.NO_BORDER) ;
						 addressTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);

						 //Billing Address
						 PdfPTable billingTable = new PdfPTable(1);
						 billingTable.setWidthPercentage(25);
						 billingTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);

						 //Billing Address Details
						 //PdfPCell BillingMethodCell =
						 	//new PdfPCell(new Phrase("Billing Address:", fontHeaderDetailsBold));
						 PdfPCell BillingMethodCell= new PdfPCell(new Phrase("", fontHeaderDetailsBold)) ;
						 BillingMethodCell.setBorder(Rectangle.NO_BORDER) ;

						 //Add Customer Name
						 Phrase pBillingCustomerName = new Phrase(String.format(sCustomerName), fontAddressCell) ;
						 PdfPCell BillingMethodDataCell = new PdfPCell() ;
						 BillingMethodDataCell.setPhrase(pBillingCustomerName) ;
						 BillingMethodDataCell.setBorder(Rectangle.NO_BORDER) ;
						 BillingMethodDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;

					 //Add AddressLine1
					 Phrase pBillingAddress1 = new Phrase(String.format(sAddressLine1), fontAddressCell) ;
					 PdfPCell BillingAddress1DataCell = new PdfPCell() ;
					 BillingAddress1DataCell.setPhrase(pBillingAddress1) ;
					 BillingAddress1DataCell.setBorder(Rectangle.NO_BORDER) ;
					 BillingAddress1DataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;

					 //Add AddressLine2
					 Phrase pBillingeAddress2 = new Phrase(String.format(sAddressLine2), fontAddressCell) ;
					 PdfPCell BillingAddress2DataCell = new PdfPCell() ;
					 BillingAddress2DataCell.setPhrase(pBillingeAddress2) ;
					 BillingAddress2DataCell.setBorder(Rectangle.NO_BORDER) ;
					 BillingAddress2DataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;

					 //Add FormattedCityStateZipCode
					 Phrase pBillingFormattedCityStateZipCode = new Phrase(String.format(sFormattedCityStateZipCode), fontAddressCell);
					 PdfPCell BillingFormattedCityStateZipCodeDataCell = new PdfPCell();
					 BillingFormattedCityStateZipCodeDataCell.setPhrase(pBillingFormattedCityStateZipCode);
					 BillingFormattedCityStateZipCodeDataCell.setBorder(Rectangle.NO_BORDER);
					 BillingFormattedCityStateZipCodeDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);

					 //Add Country
					 Phrase pBillingCountry = new Phrase(String.format(sCountry), fontAddressCell);
					 PdfPCell BillingCountryDataCell = new PdfPCell();
					 BillingCountryDataCell.setPhrase(pBillingCountry);
					 BillingCountryDataCell.setBorder(Rectangle.NO_BORDER);
					 BillingCountryDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);

					 //Add Dummy Cell
					 Phrase pBillingDummyCell = new Phrase(String.format(" "), fontAddressCell) ;
					 PdfPCell BillingMethodDummyCell = new PdfPCell() ;
					 BillingMethodDummyCell.setPhrase(pBillingDummyCell) ;
					 BillingMethodDummyCell.setBorder(Rectangle.NO_BORDER) ;
					 BillingMethodDummyCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;
					 
					 //Adding Cells
					 billingTable.addCell(BillingMethodCell);
					 billingTable.addCell(BillingMethodCell);
					 billingTable.addCell(BillingMethodCell);
					 billingTable.addCell(BillingMethodDataCell);
					 billingTable.addCell(BillingAddress1DataCell) ;
					 if (sAddressLine2 != null && sAddressLine2 != "")
					 {
					   billingTable.addCell(BillingAddress2DataCell) ;
					 }
					 billingTable.addCell(BillingFormattedCityStateZipCodeDataCell);
					 billingTable.addCell(BillingCountryDataCell);

					 if (!(sAddressLine2 != null && sAddressLine2 != ""))
					 {
					   billingTable.addCell(BillingMethodDummyCell) ;
					 }

					 addressTable.addCell(billingTable);

					 //Shipping Address Table details
					 PdfPTable shippingTable = new PdfPTable(1);
					 shippingTable.setWidthPercentage(25);
					 shippingTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);

					 //Shipping Details
					 //PdfPCell ShippingAddressCell= new PdfPCell(new Phrase("Shipping Address:", fontHeaderDetailsBold));
					 PdfPCell ShippingAddressCell= new PdfPCell(new Phrase("", fontAddressCell));
					 ShippingAddressCell.setBorder(Rectangle.NO_BORDER);

					 //Add Customer Name
					 Phrase pFormatCustomerName = new Phrase(String.format(sFormatCustomerName), fontAddressCell);
					 PdfPCell ShippingAddressDataCell = new PdfPCell();
					 ShippingAddressDataCell.setPhrase(pFormatCustomerName);
					 ShippingAddressDataCell.setBorder(Rectangle.NO_BORDER);
					 ShippingAddressDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);

					 //Add AddressLine1
					 Phrase pStoreAddress1 = new Phrase(String.format(sStoreAddress1), fontAddressCell);
					 PdfPCell StoreAddress1DataCell = new PdfPCell();
					 StoreAddress1DataCell.setPhrase(pStoreAddress1);
					 StoreAddress1DataCell.setBorder(Rectangle.NO_BORDER);
					 StoreAddress1DataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);

					 //Add AddressLine2
					 Phrase pStoreAddress2 = new Phrase(String.format(sStoreAddress2), fontAddressCell);
					 PdfPCell StoreAddress2DataCell = new PdfPCell();
					 StoreAddress2DataCell.setPhrase(pStoreAddress2);
					 StoreAddress2DataCell.setBorder(Rectangle.NO_BORDER);
					 StoreAddress2DataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);

					 //Add FormattedCityStateZipCode
					 Phrase pStoreFormattedCityStateZipCode =
						 new Phrase(String.format(sStoreFormattedCityStateZipCode), fontAddressCell) ;
					 PdfPCell StoreFormattedCityStateZipCodeDataCell = new PdfPCell() ;
					 StoreFormattedCityStateZipCodeDataCell.setPhrase(pStoreFormattedCityStateZipCode) ;
					 StoreFormattedCityStateZipCodeDataCell.setBorder(Rectangle.NO_BORDER) ;
					 StoreFormattedCityStateZipCodeDataCell
					 	.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;

					 //Add Country
					 Phrase pStoreCountry = new Phrase(String.format(sStoreCountry), fontAddressCell) ;
					 PdfPCell StoreCountryDataCell = new PdfPCell() ;
					 StoreCountryDataCell.setPhrase(pStoreCountry) ;
					 StoreCountryDataCell.setBorder(Rectangle.NO_BORDER) ;
					 StoreCountryDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;

					//Add Dummy Cell
					 Phrase pShippingDummyCell = new Phrase(String.format(" "), fontAddressCell) ;
					 PdfPCell ShippingMethodDummyCell = new PdfPCell() ;
					 ShippingMethodDummyCell.setPhrase(pShippingDummyCell) ;
					 ShippingMethodDummyCell.setBorder(Rectangle.NO_BORDER) ;
					 ShippingMethodDummyCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;
					 
					 //Adding Cells
					 shippingTable.addCell(ShippingAddressCell);
					 shippingTable.addCell(ShippingAddressCell);
					 shippingTable.addCell(ShippingAddressCell);
					 shippingTable.addCell(ShippingAddressDataCell);
					 shippingTable.addCell(StoreAddress1DataCell);
					 
					 if (sStoreAddress2 != null && sStoreAddress2 != "")
					 {
						 shippingTable.addCell(StoreAddress2DataCell) ;
					 }
					 shippingTable.addCell(StoreFormattedCityStateZipCodeDataCell);
					 shippingTable.addCell(StoreCountryDataCell);

					 if (!(sStoreAddress2 != null && sStoreAddress2 != ""))
					 {
						 shippingTable.addCell(ShippingMethodDummyCell) ;
					 }
					 
					 addressTable.addCell(shippingTable) ;

					 PdfPTable orderNumberTable = new PdfPTable(2) ;
					 orderNumberTable.setWidthPercentage(100) ;
					 orderNumberTable.setWidths(new int[]{2, 3}) ;
					 orderNumberTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;

					 BaseFont bfontOrderDtls = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1250, false);
					 Font fontOrderDtls = new Font(bfontOrderDtls, 7, Font.NORMAL, Color.BLACK);
					 Font fontOrderNoDtls = new Font(bfontOrderDtls, 8, Font.NORMAL, Color.BLACK);

					 //Order No Details
					 //PdfPCell OrderNoCell =
					 	//new PdfPCell(new Phrase(YRCPlatformUI.getString("ORDER_NO"), fontHeaderDetailsBold)) ;
					 PdfPCell OrderNoCell= new PdfPCell(new Phrase("", fontOrderNoDtls)) ;
					 OrderNoCell.setBorder(Rectangle.NO_BORDER) ;
					 
					 Phrase pOrderNo = new Phrase(String.format(sOrderNo), fontOrderNoDtls) ;
					 PdfPCell OrderNoDataCell = new PdfPCell() ;
					 OrderNoDataCell.setPhrase(pOrderNo) ;
					 OrderNoDataCell.setBorder(Rectangle.NO_BORDER) ;

					 //Customer Id Details
					 //PdfPCell CustomerIdCell= new PdfPCell(new Phrase("Customer#", fontHeaderDetailsBold));
					 PdfPCell CustomerIdCell= new PdfPCell(new Phrase("", fontOrderDtls));
					 CustomerIdCell.setBorder(Rectangle.NO_BORDER);
					 Phrase pCustomerId = new Phrase(String.format(sBillToID), fontOrderDtls);
					 PdfPCell CustomerIdDataCell = new PdfPCell();
					 CustomerIdDataCell.setPhrase(pCustomerId);
					 CustomerIdDataCell.setBorder(Rectangle.NO_BORDER);

					 //Order Date Details
					 //R3/3 Begin : Defect# 730 To print date in dd-MMM-yyyy format for LT
					 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					 Calendar cCalendar = Calendar.getInstance();
					 //Start:833 OrderDate to be printed on Invoice 
					 cCalendar.setTime(sdf.parse(sOrderDate));
					 //End 833: OrderDate to be printed on Invoice
					 sOrderDate = sdf.format(cCalendar.getTime());
					 //R3/3 Begin : Defect# 730 To print date in dd-MMM-yyyy format for LT

					 PdfPCell OrderDateCell= new PdfPCell(new Phrase("", fontOrderDtls));
					 OrderDateCell.setBorder(Rectangle.NO_BORDER);
					 //CurrentDateCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
					 //sOrderDate= sOrderDate.substring(0, 10);
					 //Date date = new Date();
					 //sOrderDate = date.toString();
					 Phrase pOrderDate = new Phrase(String.format(sOrderDate), fontOrderDtls);
					 PdfPCell OrderDateDataCell = new PdfPCell();
					 OrderDateDataCell.setPhrase(pOrderDate);
					 OrderDateDataCell.setBorder(Rectangle.NO_BORDER);

					 //Transaction ID Details
					 PdfPCell TransactionIdCell= new PdfPCell(new Phrase("", fontOrderDtls));
					 TransactionIdCell.setBorder(Rectangle.NO_BORDER);
					 Phrase pTransactionId = new Phrase(String.format("      " + sExtnTransactionID), fontOrderDtls) ;
					 PdfPCell TransactionIdDataCell = new PdfPCell();
					 TransactionIdDataCell.setPhrase(pTransactionId);
					 TransactionIdDataCell.setBorder(Rectangle.NO_BORDER);

					 //Shipping Method Details
					 PdfPCell ShippingMethodCell= new PdfPCell(new Phrase("", fontOrderDtls));
					 ShippingMethodCell.setBorder(Rectangle.NO_BORDER);
					 Phrase pShippingMethod = new Phrase(String.format("          " + sShippingMethod), fontOrderDtls) ;
					 PdfPCell ShippingMethodDataCell = new PdfPCell();
					 ShippingMethodDataCell.setPhrase(pShippingMethod);
					 ShippingMethodDataCell.setBorder(Rectangle.NO_BORDER);

					 //Add the cells to orderNumberTable
					 orderNumberTable.addCell(OrderNoCell) ;
					 orderNumberTable.addCell(OrderNoDataCell) ;
					 orderNumberTable.addCell(CustomerIdCell) ;
					 orderNumberTable.addCell(CustomerIdDataCell) ;
					 orderNumberTable.addCell(OrderDateCell) ;
					 orderNumberTable.addCell(OrderDateDataCell) ;
					 orderNumberTable.addCell(TransactionIdCell) ;
					 orderNumberTable.addCell(TransactionIdDataCell) ;
					 orderNumberTable.addCell(ShippingMethodCell) ;
					 orderNumberTable.addCell(ShippingMethodDataCell) ;
					 addressTable.setSpacingAfter(10f) ;


					 addressTable.addCell(orderNumberTable);
					 mainTable.addCell(addressTable);

					 String sOrderTotal = "0.00" ;

					//Shipment Details Table
					 PdfPTable shipmentTable = new PdfPTable(1);
					 shipmentTable.setWidthPercentage(40);
					 shipmentTable.getDefaultCell().setBorder(0);


					 PdfPCell ShipmentCell= new PdfPCell(new Phrase("", fontCell));
					 //UPCCodeCell.setBackgroundColor(Color.LIGHT_GRAY);
					 ShipmentCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
					 ShipmentCell.setFixedHeight(60);

					 // Fixed Size as per L&T Pack Slip { 6, 10, 6, 4, 3, 4, 4, 2, 4}
					 //each order line detail will be captured in this table
					 PdfPTable orderLineTable = new PdfPTable(new float[]{ 6, 10, 6, 4, 3, 4, 4, 2, 4});
					 orderLineTable.setWidthPercentage(100);

					 //UPC Code Cell
					 //PdfPCell UPCCodeCell= new PdfPCell(new Phrase(YRCPlatformUI.getString("UPC_CODE"), fontCell));
					 PdfPCell UPCCodeCell= new PdfPCell(new Phrase(YRCPlatformUI.getString(""), fontCell));
					 //UPCCodeCell.setBackgroundColor(Color.LIGHT_GRAY);
					 UPCCodeCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);
					 UPCCodeCell.setBorder(0);
					 UPCCodeCell.setFixedHeight(25f);
					 //Item Description Cell
					 //PdfPCell ItemDescCell = 
					 	//new PdfPCell(new Phrase(YRCPlatformUI.getString("ITEM_DESCRIPTION"), fontCell));
					 PdfPCell ItemDescCell= new PdfPCell(new Phrase(YRCPlatformUI.getString(""), fontCell));
					 //ItemDescCell.setBackgroundColor(Color.LIGHT_GRAY);
					 ItemDescCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);
					 ItemDescCell.setBorder(0);
					 ItemDescCell.setFixedHeight(25f);
					 //Color Cell
					 //PdfPCell ColorCell= new PdfPCell(new Phrase(YRCPlatformUI.getString("COLOR"), fontCell));
					 PdfPCell ColorCell= new PdfPCell(new Phrase(YRCPlatformUI.getString(""), fontCell));
					 //ColorCell.setBackgroundColor(Color.LIGHT_GRAY);
					 ColorCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);
					 ColorCell.setBorder(0);
					 ColorCell.setFixedHeight(25f);
					 //Size Cell
					 //PdfPCell SizeCell= new PdfPCell(new Phrase(YRCPlatformUI.getString("SIZE"), fontCell));
					 PdfPCell SizeCell= new PdfPCell(new Phrase(YRCPlatformUI.getString(""), fontCell));
					 //SizeCell.setBackgroundColor(Color.LIGHT_GRAY);
					 SizeCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);
					 SizeCell.setBorder(0);
					 SizeCell.setFixedHeight(25f);
					 //Picked Quantity Cell
					 //PdfPCell PickedQtyCell= new PdfPCell(new Phrase(YRCPlatformUI.getString("QTY"), fontCell));
					 PdfPCell PickedQtyCell= new PdfPCell(new Phrase(YRCPlatformUI.getString(""), fontCell));
					 //PickedQtyCell.setBackgroundColor(Color.LIGHT_GRAY);
					 PickedQtyCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);
					 PickedQtyCell.setBorder(0);
					 PickedQtyCell.setFixedHeight(25f);
					 //Price Cell
					 //PdfPCell PriceCell= new PdfPCell(new Phrase(YRCPlatformUI.getString("PRICE"), fontCell));
					 PdfPCell PriceCell= new PdfPCell(new Phrase(YRCPlatformUI.getString(""), fontCell));
					 //PriceCell.setBackgroundColor(Color.LIGHT_GRAY);
					 PriceCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);
					 PriceCell.setBorder(0);
					 PriceCell.setFixedHeight(25f);
					 //Gift Box Cell
					 //PdfPCell GiftBoxCell= new PdfPCell(new Phrase(YRCPlatformUI.getString("GIFT_BOX"), fontCell));
					 PdfPCell GiftBoxCell= new PdfPCell(new Phrase(YRCPlatformUI.getString(""), fontCell));
					 //GiftBoxCell.setBackgroundColor(Color.LIGHT_GRAY);
					 GiftBoxCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);
					 GiftBoxCell.setBorder(0);
					 GiftBoxCell.setFixedHeight(25f);
					 //Gift Wrap Cell
					 //PdfPCell GiftWrapCell= new PdfPCell(new Phrase(YRCPlatformUI.getString("Gift Box $"), fontCell));
					 PdfPCell GiftWrapCell= new PdfPCell(new Phrase(YRCPlatformUI.getString(""), fontCell));
					 //GiftWrapCell.setBackgroundColor(Color.LIGHT_GRAY);
					 GiftWrapCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);
					 GiftWrapCell.setBorder(0);
					 GiftWrapCell.setFixedHeight(25f);
					 //Total Cell
					 //PdfPCell TotalCell= new PdfPCell(new Phrase(YRCPlatformUI.getString("Total"), fontCell));
					 PdfPCell TotalCell= new PdfPCell(new Phrase(YRCPlatformUI.getString(""), fontCell));
					 //TotalCell.setBackgroundColor(Color.LIGHT_GRAY);
					 TotalCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);
					 TotalCell.setBorder(0);
					 TotalCell.setFixedHeight(25f);

					 //Add the cells to the table
					 orderLineTable.addCell(UPCCodeCell);
					 orderLineTable.addCell(ItemDescCell);
					 orderLineTable.addCell(ColorCell);
					 orderLineTable.addCell(SizeCell);
					 orderLineTable.addCell(PickedQtyCell);
					 orderLineTable.addCell(PriceCell);
					 orderLineTable.addCell(GiftBoxCell);
					 orderLineTable.addCell(GiftWrapCell);
					 orderLineTable.addCell(TotalCell);
					 String sGiftFlag = "";
					 org.w3c.dom.Element eleShipmentLines =
						 YRCXmlUtils.getXPathElement(eleShipment, "Shipment/ShipmentLines") ;
					 if (!YRCPlatformUI.isVoid(eleShipmentLines))
					 {
						 NodeList nShipmentLines = eleShipmentLines.getElementsByTagName("ShipmentLine") ;
						 for (int k=0; k<nShipmentLines.getLength(); k++)
						 {
							 org.w3c.dom.Element eleShipmentLine = (org.w3c.dom.Element) nShipmentLines.item(k) ;

							 //UPCCode Data Cell
							 NodeList nOrderLines = eleShipmentLine.getElementsByTagName("OrderLine");
							 String sUPCCode = "";
							 String sExtnDepartment = "";
							 String sExtnDivision = "";
							 String sGiftWrap = "";
							 String sSize = "";
							 String sColor = "";
							 String sPrice = "0.0";
							 String sItemDescription = "";
							 String sStatus = "";
							 String sPackSlpPrcSuppress="";
							 String sRegistrantAddrsIndictr="";
							 String sDiscount="0.0";

							 for (int l=0; l<nOrderLines.getLength(); l++)
							 {
								 org.w3c.dom.Element eleOrderLine = (org.w3c.dom.Element) nOrderLines.item(l);
								 NodeList nItemAlias = eleOrderLine.getElementsByTagName("ItemAlias");
								 for (int m=0; m<nItemAlias.getLength(); m++) {
									 org.w3c.dom.Element eleItemAlias = (org.w3c.dom.Element) nItemAlias.item(m);
									 if ("UPC".equalsIgnoreCase(eleItemAlias.getAttribute("AliasName")))
									 {
										 sUPCCode = eleItemAlias.getAttribute("AliasValue");
									 }
								 }
								 org.w3c.dom.Element eleItemExtn =
									 YRCXmlUtils.getXPathElement(eleOrderLine, "OrderLine/ItemDetails/Extn") ;
								 if (!YRCPlatformUI.isVoid(eleItemExtn))
								 {
									 sExtnDepartment = eleItemExtn.getAttribute("ExtnDepartment");
									 sExtnDivision = eleItemExtn.getAttribute("ExtnDivision");
								 }

								 //Modified to fetch item status
								 org.w3c.dom.Element eleItemPrimaryInfo =
									 YRCXmlUtils.getXPathElement(eleOrderLine,
											 "OrderLine/ItemDetails/PrimaryInformation") ;
								 if (!YRCPlatformUI.isVoid(eleItemPrimaryInfo))
								 {
									 sItemDescription = eleItemPrimaryInfo.getAttribute("ShortDescription") ;
									 sStatus = eleItemPrimaryInfo.getAttribute("Status");
								 }
								 sGiftFlag = eleOrderLine.getAttribute("GiftFlag");
								 sGiftWrap = eleOrderLine.getAttribute("GiftWrap");
								 NodeList nAdditionalAttribute =
									 eleOrderLine.getElementsByTagName("AdditionalAttribute") ;
								 for (int n=0; n<nAdditionalAttribute.getLength(); n++)
								 {
									 org.w3c.dom.Element eleAdditionalAttribute =
										 (org.w3c.dom.Element) nAdditionalAttribute.item(n) ;
									 //Modified to display color and size values on the pick ticket
									 //based on the item status
									 for (int t=0; t<nXpathForItemAttributes.getLength(); t++)
									 {
										 org.w3c.dom.Element eleXpathForItemAttributes =
											 (org.w3c.dom.Element) nXpathForItemAttributes.item(t) ;
										 String sCommonCodeValue = eleXpathForItemAttributes.getAttribute("CodeValue") ;
										 if (sStatus.equalsIgnoreCase(sCommonCodeValue))
										 {
											 if (eleXpathForItemAttributes.getAttribute("CodeShortDescription")
												.equalsIgnoreCase(eleAdditionalAttribute.getAttribute("Name")))
											 {
												 sColor = eleAdditionalAttribute.getAttribute("Value") ;
											 }
											 if (eleXpathForItemAttributes.getAttribute("CodeLongDescription")
												.equalsIgnoreCase(eleAdditionalAttribute.getAttribute("Name")))
											 {
												 sSize = eleAdditionalAttribute.getAttribute("Value") ;
											 }
										 }
									 }
								 }

								 org.w3c.dom.Element eleLinePriceInfo =
									 YRCXmlUtils.getXPathElement(eleOrderLine, "OrderLine/LinePriceInfo") ;
								 if (!YRCPlatformUI.isVoid(eleLinePriceInfo))
								 {
									 sPrice = eleLinePriceInfo.getAttribute("UnitPrice") ;
								 }

								 //Gift Registry
								 org.w3c.dom.Element eleOrderLineExtn =
									 YRCXmlUtils.getXPathElement(eleOrderLine, "OrderLine/Extn") ;
								 if (!YRCPlatformUI.isVoid(eleOrderLineExtn))
								 {
									 sPackSlpPrcSuppress = eleItemExtn.getAttribute("ExtnPackSlpPrcSuppress") ;
									 sRegistrantAddrsIndictr = eleItemExtn.getAttribute("ExtnRegistrantAddrsIndictr") ;
								 }

								 //Start change for defect 841 to subtract discount from unit price
								 org.w3c.dom.Element eleLineCharges =
									 YRCXmlUtils.getXPathElement(eleOrderLine, "OrderLine/LineCharges") ;
								 if (!YRCPlatformUI.isVoid(eleLineCharges)){
									 NodeList nLineCharge = eleLineCharges.getElementsByTagName("LineCharge") ;
									 for (int m=0; m<nLineCharge.getLength(); m++) 
									 {
										 org.w3c.dom.Element eleLineCharge = (org.w3c.dom.Element) nLineCharge.item(m) ;
										 String strChargeCategory = "" ;
										 String strChargeName = "" ;
										 strChargeCategory = eleLineCharge.getAttribute("ChargeCategory") ;
										 strChargeName = eleLineCharge.getAttribute("ChargeName") ;
										 if (strChargeCategory.equalsIgnoreCase("DISCOUNT")
												&& strChargeName.equalsIgnoreCase("DISCOUNT"))
										 {
											 sDiscount = eleLineCharge.getAttribute("ChargePerUnit") ;
											 sPrice = String.valueOf((Double.valueOf(sPrice))
													- (Double.valueOf(sDiscount))) ;
										 }
									 }
								 }
							 //End change for defect 841 to subtract discount from unit price
							 }

							 Phrase pUPCCode = new Phrase(String.format(sUPCCode), fontCell);
							 PdfPCell UPCCodeDataCell = new PdfPCell();
							 UPCCodeDataCell.setPhrase(pUPCCode);
							 UPCCodeDataCell.setBorder(0);
							 UPCCodeDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;

							 //Item Desc Data Cell
							 String strItemDescDisplyed = "" ;
							 if(sItemDescription.length()>40)
							 {
								 strItemDescDisplyed = sItemDescription.substring(0,40);
							 }
							 else
							 {
								 strItemDescDisplyed = sItemDescription;
							 }
							 Phrase pItemDesc = new Phrase(String.format(strItemDescDisplyed), fontCell);
							 PdfPCell ItemDescDataCell = new PdfPCell();
							 ItemDescDataCell.setPhrase(pItemDesc);
							 ItemDescDataCell.setBorder(0);
							 ItemDescDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);

							 //Color Data Cell
							 Phrase pColor = new Phrase(String.format(sColor), fontCell);
							 PdfPCell ColorDataCell = new PdfPCell();
							 ColorDataCell.setPhrase(pColor);
							 ColorDataCell.setBorder(0);
							 ColorDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

							 //Size Data Cell
							 Phrase pSize = new Phrase(String.format(sSize), fontCell);
							 PdfPCell SizeDataCell = new PdfPCell();
							 SizeDataCell.setPhrase(pSize);
							 SizeDataCell.setBorder(0);
							 SizeDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);

							 //Original Quantity Data Cell
							 /*String sQuantity = null;
							sQuantity = eleShipmentLine.getAttribute("Quantity");
							if (sQuantity.contains(".")){
								sQuantity = sQuantity.replaceAll("\\..*", "");
							}
							Phrase pOrgQty = new Phrase(String.format(sQuantity), fontCell);
							PdfPCell OrgQtyDataCell = new PdfPCell();
							OrgQtyDataCell.setPhrase(pOrgQty);*/

							 //Quantity To Pick Data Cell
							 String sBackroomPickedQuantity = eleShipmentLine.getAttribute("BackroomPickedQuantity") ;
							 String sActualQuantity = eleShipmentLine.getAttribute("ActualQuantity");
							 String sOriginalQty = eleShipmentLine.getAttribute("Quantity");
							 //to refrain canceled line from being added to pack slip
							 if("0.0".equals(sOriginalQty) || "0".equals(sOriginalQty))
							 {
								 // Defect 850
								 fCountCanceledLines++ ;
								 continue ;
							 }
							 double dBackRoomPickedQty = 0.00 ;
							 double dOriginalQty = 0.00 ;
							 double dToPickQty = 0.00 ;
							 String spickedQty = "NULL" ;
							 if ((!sBackroomPickedQuantity.equals("")) && (!sBackroomPickedQuantity.equals(null)))
							 {
								 dBackRoomPickedQty = Double.parseDouble(sBackroomPickedQuantity);
								 dOriginalQty = Double.parseDouble(sOriginalQty);
								 dToPickQty = dOriginalQty - dBackRoomPickedQty;
								 spickedQty = Double.toString(dToPickQty);
								 if (spickedQty.contains("."))
								 {
									 spickedQty = spickedQty.replaceAll("\\..*", "") ;
								 }
							 }
							 else
							 {
								 spickedQty = eleShipmentLine.getAttribute("Quantity") ;
								 spickedQty = spickedQty.replaceAll("\\..*", "") ;
							 }

							/*Phrase pPickQty = new Phrase(String.format(spickedQty), fontCell);
							PdfPCell PickQtyDataCell = new PdfPCell();
							PickQtyDataCell.setPhrase(pPickQty);*/

							 //Picked Quantity Data Cell
							 String sBackroomPickQty = "0.00" ;
							 if ((!sBackroomPickedQuantity.equals("")) && (!sBackroomPickedQuantity.equals(null)))
							 {
								 sBackroomPickQty = sBackroomPickedQuantity ;
								 sBackroomPickQty = sBackroomPickQty.replaceAll("\\..*", "") ;

								 //Start SOM changes Req#7 May'14 Release
								 //[Begin: Feb 3, 2014], To print actual quantity picked by the customer
								 if(strStatus.equals("1400.10") && ((!sActualQuantity.equals(""))
										&& (!sActualQuantity.equals(null))))
								 {
									 sBackroomPickQty= sActualQuantity ;
								 }
								//[End: Feb 3, 2014]
								//End SOM changes Req#7 May'14 Release
							 }

							 //Begin:Removing decimals from the quantity
							 Double dBackroomPick = Double.parseDouble(sBackroomPickQty) ;
							 BigDecimal bdBackroomPick = new BigDecimal(dBackroomPick) ;
							 bdBackroomPick= bdBackroomPick.setScale(0, RoundingMode.HALF_DOWN) ;
							 sBackroomPickQty= String.valueOf(bdBackroomPick) ;
							 //End:Removing decimals from the quantity
							 Phrase pPickedQty = new Phrase(String.format(sBackroomPickQty), fontCell) ;
							 PdfPCell PickedQtyDataCell = new PdfPCell() ;
							 PickedQtyDataCell.setPhrase(pPickedQty) ;
							 PickedQtyDataCell.setFixedHeight(25f) ;
							 PickedQtyDataCell.setBorder(0) ;
							 PickedQtyDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;

							 //Price Data Cell
							 //Begin:Removing decimals from the quantity
							 Double dPrice = Double.parseDouble(sPrice);
							 BigDecimal bdPrice = new BigDecimal(dPrice);
							 bdPrice= bdPrice.setScale(2, RoundingMode.HALF_DOWN);
							 sPrice= String.valueOf(bdPrice);
							// Start Defect 993 march release.
							 if(("Y".equalsIgnoreCase(sGiftFlag)))
							 {
								 sPrice="";
							 }
							 if(("N".equalsIgnoreCase(sGiftFlag)) && ("Y".equalsIgnoreCase(sPackSlpPrcSuppress)))
							 {
								 sPrice="0.0";
							 }

							 // End Defect 993 march release.
							 Phrase pPrice = new Phrase(String.format(sPrice), fontCell);
							 PdfPCell PriceDataCell = new PdfPCell();
							 PriceDataCell.setPhrase(pPrice);
							 PriceDataCell.setFixedHeight(25f);
							 PriceDataCell.setBorder(0);
							 PriceDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);

							 //GiftBox Data Cell
							 Phrase pGiftBox = new Phrase(String.format("N"), fontCell);
							 PdfPCell GiftBoxDataCell = new PdfPCell();
							 GiftBoxDataCell.setPhrase(pGiftBox);
							 GiftBoxDataCell.setFixedHeight(25f);
							 GiftBoxDataCell.setBorder(0);
							 GiftBoxDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

							 //GiftWrap Data Cell
							 Phrase pGiftWrap = new Phrase(String.format("   "), fontCell);
							 PdfPCell GiftWrapDataCell = new PdfPCell();
							 GiftWrapDataCell.setPhrase(pGiftWrap);
							 GiftWrapDataCell.setFixedHeight(25f);
							 GiftWrapDataCell.setBorder(0);
							 GiftWrapDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);
							// Start Defect 993 march release.
							 double dTotalPrice= 0.00;
							 if(!XMLUtil.isVoid(sPrice))
							 {
								 dTotalPrice= (Double.parseDouble(sBackroomPickQty) * Double.parseDouble(sPrice));
							 }
							 // End Defect 993 march release.

							 // double dTotalPrice= 0.00;
							 // dTotalPrice= (Double.parseDouble(sBackroomPickQty) * Double.parseDouble(sPrice));
							 //Begin: display price up to 2 decimal places
							 BigDecimal totalDecimal = new BigDecimal(dTotalPrice);
							 totalDecimal= totalDecimal.setScale(2, RoundingMode.HALF_UP);
							 //End: display price up to 2 decimal places
							 //Start: Fix for multiple Orderline (greater than 5)
							 String sTotalPrice = String.valueOf(totalDecimal);
							 //String sTotalPrice = hmShipmentLine.get("OrderTotal");
							 //End: Fix for multiple Orderline (greater than 5)


							 sOrderTotal = String.valueOf(Double.parseDouble(sOrderTotal) + dTotalPrice);
							 //Total Data Cell

							 //check if order has GiftFlag as Y then  Total Price will display blank
							 if(("Y".equalsIgnoreCase(sGiftFlag)) || ("Y".equalsIgnoreCase(sPackSlpPrcSuppress)))
							 {
								 sTotalPrice="" ;
							 }
							 /*if(sTotalPrice.contains(".")){

							 }*/
							 Phrase pTotal = new Phrase(String.format(sTotalPrice), fontCell);
							 PdfPCell TotalDataCell = new PdfPCell();
							 TotalDataCell.setPhrase(pTotal);
							 TotalDataCell.setFixedHeight(25f);
							 TotalDataCell.setBorder(0);
							 TotalDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT);

							 //Add Data cells to the table
							 orderLineTable.addCell(UPCCodeDataCell);
							 orderLineTable.addCell(ItemDescDataCell);
							 orderLineTable.addCell(ColorDataCell);
							 orderLineTable.addCell(SizeDataCell);
							 orderLineTable.addCell(PickedQtyDataCell);
							 orderLineTable.addCell(PriceDataCell);
							 orderLineTable.addCell(GiftBoxDataCell);
							 orderLineTable.addCell(GiftWrapDataCell);
							 orderLineTable.addCell(TotalDataCell);
						}

						float iNoOfAddedCells = nShipmentLines.getLength() * 25;
						float iTableLength = 170 ;
						float fAdjustLength = 15;

						if (iNoOfAddedCells < iTableLength)
						{
							 float iTableLengthDiff = iTableLength - iNoOfAddedCells - fAdjustLength ;
							 orderLineTable.setSpacingAfter(iTableLengthDiff);
						}

						//Begin: Defect 850: adding height when shipment has canceled lines
						if(fCountCanceledLines>0)
						{
							 fCountCanceledLines = fCountCanceledLines * 25;
							 orderLineTable.setSpacingAfter(fCountCanceledLines);
						}
						//End: Defect 850: adding height when shipment has canceled lines

						shipmentTable.addCell(orderLineTable);
						mainTable.addCell(shipmentTable);
					}
					//document.add(table);
					String sInstructionText="";
					double dLineTaxTotal = 0.00;

					 //Instruction and Order Total Details Table
					 PdfPTable shipmentTotalTable = new PdfPTable(2);
					 shipmentTotalTable.setWidthPercentage(100);
					 shipmentTotalTable.getDefaultCell().setBorder(0);
					 shipmentTotalTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT);
					 shipmentTotalTable.setSpacingBefore(2f) ;

					 PdfPTable instructionTextTable = new PdfPTable(1) ;
					 instructionTextTable.setWidthPercentage(60) ;
					 instructionTextTable.getDefaultCell().setBorder(0) ;
					 instructionTextTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;

					 NodeList nShipmentLines = eleShipmentLines.getElementsByTagName("ShipmentLine") ;
					 for (int k=0; k<nShipmentLines.getLength(); k++)
					 {
						 String sInstructionType="";

						 org.w3c.dom.Element eleShipmentLine = (org.w3c.dom.Element) nShipmentLines.item(k) ;

						 //UPCCode Data Cell
						 NodeList nOrderLines = eleShipmentLine.getElementsByTagName("OrderLine") ;
						 for (int l=0; l<nOrderLines.getLength(); l++)
						 {
							 org.w3c.dom.Element eleOrderLine = (org.w3c.dom.Element) nOrderLines.item(l) ;

							 org.w3c.dom.Element eleLineOverallTotals =
								 YRCXmlUtils.getXPathElement(eleOrderLine, "OrderLine/LineOverallTotals") ;

							 String sLineOverallTotals = eleLineOverallTotals.getAttribute("Tax");
							 dLineTaxTotal = dLineTaxTotal + Double.parseDouble(sLineOverallTotals);

							 org.w3c.dom.Element eleInstructions =
								 YRCXmlUtils.getXPathElement(eleOrderLine, "OrderLine/Instructions") ;
							 NodeList nInstruction = eleInstructions.getElementsByTagName("Instruction") ;
							 for (int m=0; m<nInstruction.getLength(); m++)
							 {
								 org.w3c.dom.Element eleInstruction = (org.w3c.dom.Element) nInstruction.item(m) ;
								 if (!YRCPlatformUI.isVoid(eleInstruction))
								 {
									 sInstructionType = eleInstruction.getAttribute("InstructionType") ;
									 if(sInstructionType.equalsIgnoreCase("GIFT"))
									 {
										 sInstructionText = eleInstruction.getAttribute("InstructionText") ;
									 }
								 }
							 }
						 }
					 }
					 // to display tax up to 2 decimal digits
					 Double dTax = 0.00;
					 dTax = Double.parseDouble(sTax);
					 Double dTotalHeaderLineTax = dTax + dLineTaxTotal;
					 //Updating the LT taxes for multi Orderlines
					 dTotalHeaderLineTax = Double.parseDouble(hmShipmentLine.get("LTtax"));
					 BigDecimal bdDecimal = new BigDecimal(dTotalHeaderLineTax);
					 bdDecimal= bdDecimal.setScale(2, RoundingMode.HALF_UP);
					 //sTax = String.valueOf(Double.parseDouble(sTax) + dLineTaxTotal);
					 sTax = String.valueOf(bdDecimal);

					//check for this validation
					if( sExtnRegistryId!="")
					{
						//Gift Registry ID Data Cell
						Phrase pGiftRegistryID = new Phrase(String.format(sExtnRegistryId), fontCell);
						 PdfPCell GiftRegistryIDDataCell = new PdfPCell();
						 GiftRegistryIDDataCell.setBorder(Rectangle.NO_BORDER);
						 GiftRegistryIDDataCell.setPhrase(pGiftRegistryID);
						 //Gift Registrant Data Cell
						 Phrase pGiftRegistrantName = new Phrase(String.format(sExtnRegistrantName), fontCell);
						 PdfPCell RegistrantNameDataCell = new PdfPCell();
						 RegistrantNameDataCell.setBorder(Rectangle.NO_BORDER);
						 RegistrantNameDataCell.setPhrase(pGiftRegistrantName);

						 instructionTextTable.addCell(GiftRegistryIDDataCell);
						 instructionTextTable.addCell(RegistrantNameDataCell);
					}

					 //Instruction Text Data Cell
					 Phrase pInstructionText = new Phrase(String.format(sInstructionText), fontCell);
					 PdfPCell InstructionTextDataCell = new PdfPCell();
					 InstructionTextDataCell.setBorder(Rectangle.NO_BORDER);
					 InstructionTextDataCell.setPhrase(pInstructionText);

					 instructionTextTable.addCell(InstructionTextDataCell);

					 PdfPTable orderTotalTable = new PdfPTable(2);
					 orderTotalTable.setWidthPercentage(40);
					 orderTotalTable.getDefaultCell().setBorder(0);
					 orderTotalTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;

					 BaseFont bfontMerchandiseDtls = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1250, false) ;
					 Font fontMerchandiseDtls = new Font(bfontMerchandiseDtls, 7, Font.NORMAL, Color.BLACK) ;


					 //Total Merchandise Data Cell
					 //Begin: converting Total Merchandise to two decimal places
					 Double dTotalMerchandiseDecimalUpdate = Double.parseDouble(sOrderTotal) ;
					 BigDecimal bdTotalMerchandise = new BigDecimal(dTotalMerchandiseDecimalUpdate) ;
					 bdTotalMerchandise= bdTotalMerchandise.setScale(2, RoundingMode.HALF_UP) ;
					 sOrderTotal= String.valueOf(bdTotalMerchandise) ;

					 //Start: Fix for multiple Orderline (greater than 5)
					 sOrderTotal = hmShipmentLine.get("OrderTotal") ;
					 //End: converting Total Price to two decimal places
					 //Start defect993 March release
					 if(("Y".equalsIgnoreCase(sGiftFlag)))
					 {
						 sOrderTotal="";
					 }
					 //END defect993 March release
					 //PdfPCell TotalMerchandiseCell =
					 	//new PdfPCell(new Phrase("Total Merchandise in this Package:", fontHeaderDetailsBold)) ;
					 PdfPCell TotalMerchandiseCell= new PdfPCell(new Phrase("", fontMerchandiseDtls));
					 TotalMerchandiseCell.setBorder(Rectangle.NO_BORDER) ;

					 //TotalMerchandiseCell.setFixedHeight(10f);
					 Phrase pTotalMerchandise = new Phrase(String.format(sOrderTotal), fontMerchandiseDtls) ;
					 PdfPCell TotalMerchandiseDataCell = new PdfPCell() ;
					 TotalMerchandiseDataCell.setPhrase(pTotalMerchandise) ;
					 TotalMerchandiseDataCell.setBorder(Rectangle.NO_BORDER) ;
					 //TotalMerchandiseDataCell.setFixedHeight(10f);
					 TotalMerchandiseDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;


					 //Total Shipping Charge Data Cell
					 //Begin: converting Shipping Charge Amount to two decimal places
					 Double dChargeAmountDecimalUpdate = Double.parseDouble(sChargeAmount);
					 BigDecimal bdChargeAmount = new BigDecimal(dChargeAmountDecimalUpdate);
					 bdChargeAmount= bdChargeAmount.setScale(2, RoundingMode.HALF_UP);
					 sChargeAmount= String.valueOf(bdChargeAmount);
					 //End: converting Shipping Charge Amount to two decimal places
					 //Start defect993 March release
					 if(("Y".equalsIgnoreCase(sGiftFlag)))
					 {
						 sChargeAmount="" ;
					 }
					 //END defect993 March release
					 //PdfPCell ShippingCell= new PdfPCell(new Phrase("Shipping:", fontHeaderDetailsBold));
					 PdfPCell ShippingCell= new PdfPCell(new Phrase("", fontHeaderDetailsBold)) ;
					 ShippingCell.setBorder(Rectangle.NO_BORDER) ;
					 //ShippingCell.setFixedHeight(10f);
					 Phrase pShippingCell = new Phrase(String.format(sChargeAmount), fontMerchandiseDtls) ;
					 PdfPCell ShippingDataCell = new PdfPCell() ;
					 ShippingDataCell.setPhrase(pShippingCell) ;
					 ShippingDataCell.setBorder(Rectangle.NO_BORDER) ;
					 //ShippingDataCell.setFixedHeight(10f);
					 ShippingDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;

					 //Total Tax Data Cell
					 //PdfPCell TaxCell= new PdfPCell(new Phrase("Tax:", fontHeaderDetailsBold));
					 PdfPCell TaxCell= new PdfPCell(new Phrase("", fontHeaderDetailsBold));
					 TaxCell.setBorder(Rectangle.NO_BORDER);
					 //TaxCell.setFixedHeight(10f);
					 //Start defect993 March release
					 if(("Y".equalsIgnoreCase(sGiftFlag)))
					 {
						 sTax="";
					 }
					 //END defect993 March release
					 Phrase pTaxCell = new Phrase(String.format(sTax), fontMerchandiseDtls);
					 PdfPCell TaxDataCell = new PdfPCell();
					 TaxDataCell.setPhrase(pTaxCell);
					 TaxDataCell.setBorder(Rectangle.NO_BORDER);
					 //TaxDataCell.setFixedHeight(10f);
					 TaxDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT);

					 //Authorization ID Data Cell
					 //PdfPCell AuthorizationIDCell= new PdfPCell(new Phrase("Authorization#", fontHeaderDetailsBold));
					 PdfPCell AuthorizationIDCell= new PdfPCell(new Phrase("", fontHeaderDetailsBold));
					 AuthorizationIDCell.setBorder(Rectangle.NO_BORDER);
					 //AuthorizationIDCell.setFixedHeight(10f);
					 Phrase pAuthorizationIDCell = new Phrase(String.format(sAuthorizationID), fontMerchandiseDtls) ;
					 PdfPCell AuthorizationIDDataCell = new PdfPCell() ;
					 AuthorizationIDDataCell.setPhrase(pAuthorizationIDCell) ;
					 AuthorizationIDDataCell.setBorder(Rectangle.NO_BORDER) ;
					 //AuthorizationIDDataCell.setFixedHeight(10f);
					 AuthorizationIDDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;

					 orderTotalTable.addCell(TotalMerchandiseCell) ;
					 orderTotalTable.addCell(TotalMerchandiseDataCell) ;
					 orderTotalTable.addCell(ShippingCell) ;
					 orderTotalTable.addCell(ShippingDataCell) ;
					 orderTotalTable.addCell(TaxCell) ;
					 orderTotalTable.addCell(TaxDataCell) ;
					 orderTotalTable.addCell(AuthorizationIDCell) ;
					 orderTotalTable.addCell(AuthorizationIDDataCell) ;

					 shipmentTotalTable.addCell(instructionTextTable) ;
					 shipmentTotalTable.addCell(orderTotalTable) ;
					 mainTable.addCell(shipmentTotalTable) ;
					 //document.add(shipmentTotalTable);

					 //PayPal and Bar Code Cell
					 PdfPTable PayPalTransactionIDTable = new PdfPTable(2);
					 PayPalTransactionIDTable.setWidthPercentage(100);
					 PayPalTransactionIDTable.getDefaultCell().setBorder(0);
					 PayPalTransactionIDTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT);
					 PayPalTransactionIDTable.setSpacingBefore(30f);

					 //Payment Type Data Cell
					 Phrase pBlankCellPaymentType = new Phrase(String.format(""), fontCell) ;
					 PdfPCell BlankCellPaymentTypeDataCell = new PdfPCell() ;
					 BlankCellPaymentTypeDataCell.setBorder(Rectangle.NO_BORDER) ;
					 BlankCellPaymentTypeDataCell.setFixedHeight(10f) ;
					 BlankCellPaymentTypeDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;
					 BlankCellPaymentTypeDataCell.setPhrase(pBlankCellPaymentType) ;

					 //Payment Type Data Cell
					 if(sPaymentType!="")
					 {
						 sPaymentType = YRCPlatformUI.getString("PAYMENT_TYPE") + sPaymentType ;
					 }
					 Phrase pPaymentType = new Phrase(String.format(sPaymentType), fontCell);
					 PdfPCell PaymentTypeDataCell = new PdfPCell();
					 PaymentTypeDataCell.setBorder(Rectangle.NO_BORDER);
					 //PaymentTypeDataCell.setFixedHeight(25f);
					 PaymentTypeDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;
					 PaymentTypeDataCell.setPhrase(pPaymentType) ;
					 PaymentTypeDataCell.setFixedHeight(25f) ;

					 //Payment Type Blank Data Cell
					 Phrase pBlankCellBarCodeTransactionID = new Phrase(String.format(""), fontCell) ;
					 PdfPCell BlankCellBarCodeTransactionIDDataCell = new PdfPCell() ;
					 BlankCellBarCodeTransactionIDDataCell.setBorder(Rectangle.NO_BORDER) ;
					 //BlankCellBarCodeTransactionIDDataCell.setFixedHeight(25f);
					 BlankCellBarCodeTransactionIDDataCell
					 	.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;
					 BlankCellBarCodeTransactionIDDataCell.setPhrase(pBlankCellBarCodeTransactionID) ;
					 BlankCellBarCodeTransactionIDDataCell.setFixedHeight(25f) ;

					 //Barcode for 678+TransactionID
					 String sBarCodeTransactionID="678" ;
					 sBarCodeTransactionID = sBarCodeTransactionID + sExtnTransactionID ;
					 code128.setCode(sBarCodeTransactionID);
					 Image imageBarCode = code128.createImageWithBarcode(writer.getDirectContent(), null, null) ;
					 PdfPCell BarCodeTransactionIDCell = new PdfPCell(imageBarCode,false) ;
					 BarCodeTransactionIDCell.setBorder(Rectangle.NO_BORDER) ;
					 //BarCodeTransactionIDCell.setFixedHeight(25f);
					 BarCodeTransactionIDCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

					 PayPalTransactionIDTable.addCell(BlankCellPaymentTypeDataCell) ;
					 PayPalTransactionIDTable.addCell(PaymentTypeDataCell) ;
					 PayPalTransactionIDTable.addCell(BlankCellBarCodeTransactionIDDataCell) ;
					 PayPalTransactionIDTable.addCell(BarCodeTransactionIDCell) ;

					 mainTable.addCell(PayPalTransactionIDTable) ;

					 /*//Tracking # details
					 PdfPTable TrackingNoTable = new PdfPTable(1);
					 TrackingNoTable.getDefaultCell().setBorder(0);
					 TrackingNoTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

					 PdfPCell TrackingNoDataHeightCell = new PdfPCell();
					 TrackingNoDataHeightCell.setBorder(Rectangle.NO_BORDER);
					 TrackingNoDataHeightCell.setFixedHeight(125f);

					 //Payment Type Data Cell
					 String strOrderTrackingDetail=YRCPlatformUI.getString("ORDER_TRACKING");
					 Phrase pTrackingNo = new Phrase(String.format(strOrderTrackingDetail.concat(sOrderNo)), fontCell);
					 PdfPCell TrackingNoDataCell = new PdfPCell();
					 TrackingNoDataCell.setBorder(Rectangle.NO_BORDER);
					 TrackingNoDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
					 TrackingNoDataCell.setPhrase(pTrackingNo);

					 TrackingNoTable.addCell(TrackingNoDataHeightCell);
					 TrackingNoTable.addCell(TrackingNoDataCell);
					 mainTable.addCell(TrackingNoTable);*/

					 document.add(mainTable) ;
				 }

				 if (sEnterpriseCode.equalsIgnoreCase("BAY"))
				 {
					 //main table holding all the detail table
					 PdfPTable mainTable = new PdfPTable(1) ;
					 mainTable.setWidthPercentage(100) ;
					 mainTable.getDefaultCell().setBorder(0) ;
					 mainTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

					 //Header Table to contain enterprise related details
					 PdfPTable headerTable = new PdfPTable(1) ;
					 headerTable.setWidthPercentage(100) ;
					 headerTable.getDefaultCell().setBorder(Rectangle.NO_BORDER) ;
					 headerTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

					 //Enterprise Logo  Details
					 Phrase pHeaderName = new Phrase("", fontHeaderDetailsBold) ;
					 PdfPCell HeaderNameDataCell = new PdfPCell() ;
					 // HeaderNameDataCell.setFixedHeight(60f) ;
					 HeaderNameDataCell.setFixedHeight(68f) ;
					 HeaderNameDataCell.setPhrase(pHeaderName);
					 HeaderNameDataCell.setBorder(Rectangle.NO_BORDER) ;

					 headerTable.addCell(HeaderNameDataCell) ;
					 mainTable.addCell(headerTable);

					 //Shipping Billing and Order Details Table
					 PdfPTable addressTable = new PdfPTable(3) ;
					 addressTable.setWidthPercentage(100) ;
					 addressTable.getDefaultCell().setBorder(0) ;
					 addressTable.getDefaultCell().setFixedHeight(76f) ;


					 //Billing Address Table
					 PdfPTable billingTable = new PdfPTable(1) ;
					 billingTable.setWidthPercentage(20) ;
					 billingTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;

					 //Billing Header Details
					 //PdfPCell BillingMethodCell =
					 	//new PdfPCell(new Phrase("Billing Address:", fontHeaderDetailsBold)) ;
					 PdfPCell BillingMethodCell =
						 new PdfPCell(new Phrase("", fontHeaderDetailsBold)) ;
					 BillingMethodCell.setBorder(Rectangle.NO_BORDER) ;
					 BillingMethodCell.setFixedHeight(12f) ;

					 //Add Customer Name
					 Phrase pBillingCustomerName = new Phrase(String.format(sCustomerName), fontAddressCell);
					 PdfPCell BillingMethodDataCell = new PdfPCell();
					 BillingMethodDataCell.setPhrase(pBillingCustomerName);
					 BillingMethodDataCell.setBorder(Rectangle.NO_BORDER);

					 //Add AddressLine1
					 Phrase pBillingAddress1 =
						 new Phrase(String.format(sAddressLine1), fontAddressCell) ;
					 PdfPCell BillingAddress1DataCell = new PdfPCell() ;
					 BillingAddress1DataCell.setPhrase(pBillingAddress1) ;
					 BillingAddress1DataCell.setBorder(Rectangle.NO_BORDER) ;

					 //Add AddressLine2
					 Phrase pBillingeAddress2 =
						 new Phrase(String.format(sAddressLine2), fontAddressCell) ;
					 PdfPCell BillingAddress2DataCell = new PdfPCell();
					 BillingAddress2DataCell.setPhrase(pBillingeAddress2);
					 BillingAddress2DataCell.setBorder(Rectangle.NO_BORDER);

					 //Add FormattedCityStateZipCode
					 Phrase pBillingFormattedCityStateZipCode =
						 new Phrase(String.format(sFormattedCityStateZipCode), fontAddressCell) ;
					 PdfPCell BillingFormattedCityStateZipCodeDataCell = new PdfPCell();
					 BillingFormattedCityStateZipCodeDataCell
					 	.setPhrase(pBillingFormattedCityStateZipCode);
					 BillingFormattedCityStateZipCodeDataCell.setBorder(Rectangle.NO_BORDER) ;

					 //Add Country
					 Phrase pBillingCountry = new Phrase(String.format(sCountry), fontAddressCell) ;
					 PdfPCell BillingCountryDataCell = new PdfPCell() ;
					 BillingCountryDataCell.setPhrase(pBillingCountry) ;
					 BillingCountryDataCell.setBorder(Rectangle.NO_BORDER) ;

					 //Add Dummy Cell
					 Phrase pBillingDummyCell = new Phrase(String.format(" "), fontAddressCell) ;
					 PdfPCell BillingMethodDummyCell = new PdfPCell() ;
					 BillingMethodDummyCell.setPhrase(pBillingDummyCell) ;
					 BillingMethodDummyCell.setBorder(Rectangle.NO_BORDER) ;
					 BillingMethodDummyCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;
					 
					 //Adding Cells
					 billingTable.addCell(BillingMethodCell);
					 //billingTable.addCell(BillingMethodCell);
					 billingTable.addCell(BillingMethodDataCell);
					 billingTable.addCell(BillingAddress1DataCell);
					 
					 if (sAddressLine2 != null && sAddressLine2 != "")
					 {
					 billingTable.addCell(BillingAddress2DataCell);
					 }
					 billingTable.addCell(BillingFormattedCityStateZipCodeDataCell) ;
					 billingTable.addCell(BillingCountryDataCell);

					 if (!(sAddressLine2 != null && sAddressLine2 != ""))
					 {
						 billingTable.addCell(BillingMethodDummyCell) ;
					 }
					 addressTable.addCell(billingTable) ;

					 //Shipping Address
					 PdfPTable shippingTable = new PdfPTable(1);
					 shippingTable.setWidthPercentage(20);
					 shippingTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

					 //Shipping Header Details
					 //PdfPCell ShippingAddressCell = 
					 	//new PdfPCell(new Phrase("Shipping Address:", fontHeaderDetailsBold));
					 PdfPCell ShippingAddressCell= new PdfPCell(new Phrase("", fontHeaderDetailsBold)) ;
					 ShippingAddressCell.setBorder(Rectangle.NO_BORDER);
					 ShippingAddressCell.setFixedHeight(12f);

					 //Add Customer Name
					 Phrase pFormatCustomerName =
						 new Phrase(String.format("                    " + sFormatCustomerName), fontAddressCell) ;
					 PdfPCell ShippingAddressDataCell = new PdfPCell();
					 ShippingAddressDataCell.setPhrase(pFormatCustomerName);
					 ShippingAddressDataCell.setBorder(Rectangle.NO_BORDER);

					 //Add AddressLine1
					 Phrase pStoreAddress1 =
						 new Phrase(String.format("                    " + sStoreAddress1), fontAddressCell) ;
					 PdfPCell StoreAddress1DataCell = new PdfPCell();
					 StoreAddress1DataCell.setPhrase(pStoreAddress1);
					 StoreAddress1DataCell.setBorder(Rectangle.NO_BORDER);

					 //Add AddressLine2
					 Phrase pStoreAddress2 =
						 new Phrase(String.format("                    " + sStoreAddress2), fontAddressCell) ;
					 PdfPCell StoreAddress2DataCell = new PdfPCell();
					 StoreAddress2DataCell.setPhrase(pStoreAddress2);
					 StoreAddress2DataCell.setBorder(Rectangle.NO_BORDER);

					 //Add FormattedCityStateZipCode
					 Phrase pStoreFormattedCityStateZipCode =
						 new Phrase(String.format("                    " + sStoreFormattedCityStateZipCode), fontHeaderDetails) ;
					 PdfPCell StoreFormattedCityStateZipCodeDataCell = new PdfPCell() ;
					 StoreFormattedCityStateZipCodeDataCell
					 	.setPhrase(pStoreFormattedCityStateZipCode) ;
					 StoreFormattedCityStateZipCodeDataCell.setBorder(Rectangle.NO_BORDER) ;

					 //Add Country
					 Phrase pStoreCountry =
						 new Phrase(String.format("                    " + sStoreCountry), fontAddressCell) ;
					 PdfPCell StoreCountryDataCell = new PdfPCell() ;
					 StoreCountryDataCell.setPhrase(pStoreCountry) ;
					 StoreCountryDataCell.setBorder(Rectangle.NO_BORDER) ;

					 //Add Dummy Cell
					 Phrase pStoreDummyCell = new Phrase(String.format(" "), fontAddressCell) ;
					 PdfPCell StoreAddressDummyCell = new PdfPCell() ;
					 StoreAddressDummyCell.setPhrase(pStoreDummyCell) ;
					 StoreAddressDummyCell.setBorder(Rectangle.NO_BORDER) ;
					 StoreAddressDummyCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;
					 
					 //Adding Cells
					 shippingTable.addCell(ShippingAddressCell) ;
					 //shippingTable.addCell(ShippingAddressCell);
					 shippingTable.addCell(ShippingAddressDataCell) ;
					 shippingTable.addCell(StoreAddress1DataCell) ;
					 
					 if (sStoreAddress2 != null && sStoreAddress2 != "")
					 {
					 shippingTable.addCell(StoreAddress2DataCell) ;
					 }
					 shippingTable.addCell(StoreFormattedCityStateZipCodeDataCell) ;
					 shippingTable.addCell(StoreCountryDataCell) ;

					 if (!(sStoreAddress2 != null && sStoreAddress2 != ""))
					 {
						 shippingTable.addCell(StoreAddressDummyCell) ;
					 }
					
					 addressTable.addCell(shippingTable);

					 PdfPTable orderNumberTable = new PdfPTable(new float[]{20}) ;
					 orderNumberTable.setWidthPercentage(50) ;
					 orderNumberTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;

					 //Order No Details
					 //PdfPCell OrderNoCell =
					 //new PdfPCell(new Phrase(YRCPlatformUI.getString("ORDER_NO_BAY"),
					 //fontHeaderDetailsBold));

					 PdfPCell OrderNoCell= new PdfPCell(new Phrase("", fontHeaderDetailsBold)) ;
					 OrderNoCell.setBorder(Rectangle.NO_BORDER) ;

					 String strSpace="                                                    "  ;
					 Phrase pOrderNo =
						 new Phrase(String.format(strSpace.concat(sOrderNo)), boldFontCell) ;
					 PdfPCell OrderNoDataCell = new PdfPCell();
					 OrderNoDataCell.setPhrase(pOrderNo);
					 OrderNoDataCell.setBorder(Rectangle.NO_BORDER) ;
					 OrderNoDataCell.setVerticalAlignment(pOrderNo.ALIGN_TOP) ;

					 //OrderNoDataCell.setFixedHeight(12f);
					 /*PdfPCell OrderNoCell= new PdfPCell(new Phrase("", fontHeaderDetailsBold));
					 OrderNoCell.setBorder(Rectangle.NO_BORDER);*/

					 //Customer Id Details
					 /*PdfPCell CustomerIdCell= new PdfPCell(new Phrase(YRCPlatformUI
					  * .getString("CUSTOMER_ID"), fontHeaderDetailsBold));
					 CustomerIdCell.setBorder(Rectangle.NO_BORDER);	*/

					 Phrase pCustomerId =
						 new Phrase(String.format(YRCPlatformUI.getString("CUSTOMER_ID")
							+ "  " + sBillToID), fontCell) ;
					 PdfPCell CustomerIdDataCell = new PdfPCell() ;
					 CustomerIdDataCell.setPhrase(pCustomerId) ;
					 CustomerIdDataCell.setBorder(Rectangle.NO_BORDER) ;

					/* PdfPCell CustomerIdCell= new PdfPCell(new Phrase("", fontCell));
					 CustomerIdCell.setBorder(Rectangle.NO_BORDER);*/

					 //Order Date Details
					 /*PdfPCell OrderDateCell= new PdfPCell(new Phrase(YRCPlatformUI
					  * .getString("ORDER_DATE_PACKSLIP"), fontHeaderDetailsBold));
					 OrderDateCell.setBorder(Rectangle.NO_BORDER);	*/
					 //CurrentDateCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
					 //Date date = new Date();
					 //String sDate = date.toString();
					 //sOrderDate = sOrderDate.substring(0, 10);
					 //sOrderDate="   "+YRCPlatformUI.getString("ORDER_DATE_PACKSLIP")+sOrderDate;
					 //R3/3 Begin : Defect# 730 To print date in yyyy-MM-dd format for BAY

					 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd") ;
					 Calendar cCalendar = Calendar.getInstance() ;
					 //Start:833 OrderDate to be printed on Invoice 
					 cCalendar.setTime(sdf.parse(sOrderDate));
					 //End 833: OrderDate to be printed on Invoice
					 sOrderDate = sdf.format(cCalendar.getTime()) ;

					 //R3/3 Begin : Defect# 730 To print date in yyyy-MM-dd format for BAY
					 Phrase pOrderDate =
						 new Phrase(YRCPlatformUI.getString("ORDER_DATE_PACKSLIP")
							+ sOrderDate, fontCell) ;
					 PdfPCell OrderDateDataCell = new PdfPCell() ;
					 OrderDateDataCell.setPhrase(pOrderDate) ;
					 OrderDateDataCell.setBorder(Rectangle.NO_BORDER) ;

					 /*PdfPCell OrderDateCell= new PdfPCell(new Phrase("", fontCell));
					 OrderDateCell.setBorder(Rectangle.NO_BORDER);*/

					 //Transaction ID Details
					 /*PdfPCell TransactionIdCell=
					  * new PdfPCell(new Phrase(YRCPlatformUI.getString("TRANSACTION_ID"),
					  * fontHeaderDetailsBold));
					 TransactionIdCell.setBorder(Rectangle.NO_BORDER);	*/

					 Phrase pTransactionId =
						 new Phrase(String.format(YRCPlatformUI.getString("TRANSACTION_ID")
							+ sExtnTransactionID), fontCell) ;
					 PdfPCell TransactionIdDataCell = new PdfPCell() ;
					 TransactionIdDataCell.setPhrase(pTransactionId) ;
					 TransactionIdDataCell.setBorder(Rectangle.NO_BORDER) ;
					 /*PdfPCell TransactionIdCell =
					  * new PdfPCell(new Phrase("", fontHeaderDetailsBold));
					 TransactionIdCell.setBorder(Rectangle.NO_BORDER);*/

					 //Shipping Method Details
					 Phrase pShippingMethod = 
						 new Phrase(String.format(sShippingMethod), fontCell) ;
					 PdfPCell ShippingMethodDataCell = new PdfPCell() ;
					 ShippingMethodDataCell.setPhrase(pShippingMethod) ;
					 ShippingMethodDataCell.setBorder(Rectangle.NO_BORDER) ;
					 /*PdfPCell ShippingMethodCell=
					  * new PdfPCell(new Phrase("", fontHeaderDetailsBold));
					 ShippingMethodCell.setBorder(Rectangle.NO_BORDER);*/


					 //Add the cells to orderNumberTable
					 //orderNumberTable.addCell(OrderNoCell);
					 //orderNumberTable.addCell(OrderNoCell);
					 //orderNumberTable.addCell(OrderNoCell);
					 orderNumberTable.addCell(OrderNoDataCell);
					 //orderNumberTable.addCell(OrderNoCell);
					 //orderNumberTable.addCell(CustomerIdCell);
					 orderNumberTable.addCell(CustomerIdDataCell);
					 //orderNumberTable.addCell(CustomerIdCell);
					 //orderNumberTable.addCell(OrderDateCell);
					 orderNumberTable.addCell(OrderDateDataCell);
					 //orderNumberTable.addCell(OrderDateCell);
					 //orderNumberTable.addCell(TransactionIdCell);
					 orderNumberTable.addCell(TransactionIdDataCell);
					 //orderNumberTable.addCell(TransactionIdCell);
					 orderNumberTable.addCell(ShippingMethodDataCell);
					 //orderNumberTable.addCell(ShippingMethodCell);

					 addressTable.addCell(orderNumberTable);
					 addressTable.setSpacingAfter(11f) ;
					 mainTable.addCell(addressTable) ;

					 String sOrderTotal = "0" ;

					 //Shipment Lines detail table
					 PdfPTable shipmentTable = new PdfPTable(1);
					 shipmentTable.setWidthPercentage(40);
					 shipmentTable.getDefaultCell().setBorder(0);

					 //adding font for the orderLineTable
					 BaseFont bfontOrderLineTable =
						 BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1250, false) ;
					 Font fontOrderLineTable =
						 new Font(bfontOrderLineTable, 8, Font.NORMAL, Color.BLACK) ;
					 PdfPTable orderLineTable =
						 new PdfPTable(new float[]{ 7, 14, 7, 6, 3, 5, 3, 5, 3, 5}) ;
					 //PdfPTable orderLineTable =
					 	//new PdfPTable(new float[]{ 7, 14, 7, 7, 3, 4, 3, 5, 3, 5});
					 orderLineTable.setWidthPercentage(100f) ;

					 //UPC Code Cell
					 //PdfPCell UPCCodeCell= new PdfPCell(new Phrase(YRCPlatformUI.getString("UPC_CODE"), fontCell));
					 PdfPCell UPCCodeCell= new PdfPCell(new Phrase("", fontCell));
					 //UPCCodeCell.setBackgroundColor(Color.LIGHT_GRAY);
					 UPCCodeCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);
					 UPCCodeCell.setFixedHeight(25f);
					 UPCCodeCell.setBorder(0);
					 //Item Description Cell
					 //PdfPCell ItemDescCell = 
					 	//new PdfPCell(new Phrase(YRCPlatformUI.getString("ITEM_DESCRIPTION"), fontCell));
					 PdfPCell ItemDescCell= new PdfPCell(new Phrase("", fontCell));
					 //ItemDescCell.setBackgroundColor(Color.LIGHT_GRAY);
					 ItemDescCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT);
					 ItemDescCell.setFixedHeight(25f);
					 ItemDescCell.setBorder(0);
					 //Color Cell
					 //PdfPCell ColorCell= new PdfPCell(new Phrase(YRCPlatformUI.getString("COLOR"), fontCell));
					 PdfPCell ColorCell= new PdfPCell(new Phrase("", fontCell)) ;
					 //ColorCell.setBackgroundColor(Color.LIGHT_GRAY);
					 ColorCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;
					 ColorCell.setFixedHeight(25f) ;
					 ColorCell.setBorder(0) ;
					 //Size Cell
					 //PdfPCell SizeCell= new PdfPCell(new Phrase(YRCPlatformUI.getString("SIZE"), fontCell));
					 PdfPCell SizeCell= new PdfPCell(new Phrase("", fontCell)) ;
					 //SizeCell.setBackgroundColor(Color.LIGHT_GRAY);
					 SizeCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;
					 SizeCell.setFixedHeight(25f) ;
					 SizeCell.setBorder(0) ;
					 //Picked Quantity Cell
					 //PdfPCell PickedQtyCell= new PdfPCell(new Phrase(YRCPlatformUI.getString("QTY"), fontCell));
					 PdfPCell PickedQtyCell= new PdfPCell(new Phrase("", fontCell));
					 //PickedQtyCell.setBackgroundColor(Color.LIGHT_GRAY);
					 PickedQtyCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT);
					 PickedQtyCell.setFixedHeight(25f);
					 PickedQtyCell.setBorder(0);
					 //Price Cell
					 //PdfPCell PriceCell= new PdfPCell(new Phrase(YRCPlatformUI.getString("PRICE"), fontCell));
					 PdfPCell PriceCell= new PdfPCell(new Phrase("", fontCell));
					 //PriceCell.setBackgroundColor(Color.LIGHT_GRAY);
					 PriceCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT);
					 PriceCell.setFixedHeight(25f);
					 PriceCell.setBorder(0);
					 //Gift Box Cell
					 //PdfPCell GiftBoxCell= new PdfPCell(new Phrase(YRCPlatformUI.getString("GIFT_BOX"), fontCell));
					 PdfPCell GiftBoxCell= new PdfPCell(new Phrase("", fontCell));
					 //GiftBoxCell.setBackgroundColor(Color.LIGHT_GRAY);
					 GiftBoxCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT);
					 GiftBoxCell.setFixedHeight(25f);
					 GiftBoxCell.setBorder(0);
					 //Gift Wrap Cell
					 //PdfPCell GiftWrapCell= new PdfPCell(new Phrase(YRCPlatformUI.getString("Gift Box $"), fontCell));
					 PdfPCell GiftWrapCell= new PdfPCell(new Phrase("", fontCell));
					 //GiftWrapCell.setBackgroundColor(Color.LIGHT_GRAY);
					 GiftWrapCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT);
					 GiftWrapCell.setFixedHeight(25f);
					 GiftWrapCell.setBorder(0);
					 //ECOFEE Cell
					 //PdfPCell EcoFeeCell= new PdfPCell(new Phrase(YRCPlatformUI.getString("EHF"), fontCell));
					 PdfPCell EcoFeeCell= new PdfPCell(new Phrase("", fontCell));
					 //EcoFeeCell.setBackgroundColor(Color.LIGHT_GRAY);
					 EcoFeeCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT);
					 EcoFeeCell.setFixedHeight(25f);
					 EcoFeeCell.setBorder(0);
					 //Total Cell
					 PdfPCell TotalCell= new PdfPCell(new Phrase("", fontCell)) ;
					 //PdfPCell TotalCell= new PdfPCell(new Phrase(YRCPlatformUI.getString("Total"), fontCell)) ;
					 //TotalCell.setBackgroundColor(Color.LIGHT_GRAY) ;
					 TotalCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;
					 TotalCell.setFixedHeight(25f) ;
					 TotalCell.setBorder(0) ;

					 //Add the cells to the table
					 orderLineTable.addCell(UPCCodeCell) ;
					 orderLineTable.addCell(ItemDescCell) ;
					 orderLineTable.addCell(ColorCell) ;
					 orderLineTable.addCell(SizeCell) ;
					 orderLineTable.addCell(PickedQtyCell) ;
					 orderLineTable.addCell(PriceCell) ;
					 orderLineTable.addCell(GiftBoxCell) ;
					 orderLineTable.addCell(GiftWrapCell) ;
					 orderLineTable.addCell(EcoFeeCell) ;
					 orderLineTable.addCell(TotalCell) ;

					 String sGiftFlag = "" ;
					 org.w3c.dom.Element eleShipmentLines = 
						 YRCXmlUtils.getXPathElement(eleShipment, "Shipment/ShipmentLines") ;
					 if (!YRCPlatformUI.isVoid(eleShipmentLines)) 
					 {
						 NodeList nShipmentLines = eleShipmentLines.getElementsByTagName("ShipmentLine") ;
						 for (int k=0; k<nShipmentLines.getLength(); k++) 
						 {
						 	 org.w3c.dom.Element eleShipmentLine = (org.w3c.dom.Element) nShipmentLines.item(k) ;

							 //UPCCode Data Cell
							 NodeList nOrderLines = eleShipmentLine.getElementsByTagName("OrderLine") ;
							 String sUPCCode = "" ;
							 String sExtnDepartment = "" ;
							 String sExtnDivision = "" ;

							 String sGiftWrap = "" ;
							 String sSize = "" ;
							 String sColor = "" ;
							 String sPrice = "0.0" ;
							 String sItemDescription = "" ;
							 String sStatus = "" ;
							 String sPackSlpPrcSuppress="" ;
							 String sRegistrantAddrsIndictr = "" ;
							 String strEcoFeeChargeAmount = "0.0" ;
							 String sShippingSurchargeAmount = "0.0" ;
							 String sDiscount = "0.0" ;
							 String sQuantity = eleShipmentLine.getAttribute("Quantity") ;

							 for (int l=0; l<nOrderLines.getLength(); l++)
							 {
								 org.w3c.dom.Element eleOrderLine = (org.w3c.dom.Element) nOrderLines.item(l) ;
								 org.w3c.dom.Element eleLineCharges = 
									 YRCXmlUtils.getXPathElement(eleOrderLine, "OrderLine/LineCharges") ;
								 if (!YRCPlatformUI.isVoid(eleLineCharges))
								 {
								 NodeList nLineCharge = eleLineCharges.getElementsByTagName("LineCharge");
									 for (int m=0; m<nLineCharge.getLength(); m++) 
									 {
									 	 org.w3c.dom.Element eleLineCharge = (org.w3c.dom.Element) nLineCharge.item(m) ;
										 String strChargeCategory = "" ;
										 String strChargeName = "" ;

										 strChargeCategory = eleLineCharge.getAttribute("ChargeCategory") ;
										 strChargeName = eleLineCharge.getAttribute("ChargeName") ;
										 if (strChargeCategory.equalsIgnoreCase("ECOFEE") 
											&& strChargeName.equalsIgnoreCase("ECOFEE"))
									 {
											 strEcoFeeChargeAmount = eleLineCharge.getAttribute("ChargeAmount") ;
										 }

									 if (strChargeCategory.equalsIgnoreCase("SHIPPINGSURCHARGE")
										&& strChargeName.equalsIgnoreCase("SHIPPINGSURCHARGE"))
									 {
											 sShippingSurchargeAmount = eleLineCharge.getAttribute("ChargeAmount") ;
									 }
										 
									 //Start change for defect 841 to subtract discount from unit price
									 if (strChargeCategory.equalsIgnoreCase("DISCOUNT")
										&& strChargeName.equalsIgnoreCase("DISCOUNT"))
									 {
											 sDiscount = eleLineCharge.getAttribute("ChargePerUnit") ;
									 }
									 //End change for defect 841 to subtract discount from unit price
								 }
								 }

								 //calculate shipping surcharge for each line
								 Double dShippingSurchargeAmount =
									 (Double.valueOf(sShippingSurchargeAmount) * Double.valueOf(sQuantity)) ;
								 sChargeAmount =
									 String.valueOf(Double.valueOf(sChargeAmount) + dShippingSurchargeAmount) ;

								 NodeList nItemAlias = eleOrderLine.getElementsByTagName("ItemAlias");
								 for (int m=0; m<nItemAlias.getLength(); m++) 
								 {
									 org.w3c.dom.Element eleItemAlias = (org.w3c.dom.Element) nItemAlias.item(m) ;
									 if ("UPC".equalsIgnoreCase(eleItemAlias.getAttribute("AliasName")))
									 {
										 sUPCCode = eleItemAlias.getAttribute("AliasValue") ;
									 }
								 }
								 org.w3c.dom.Element eleItemExtn =
									 YRCXmlUtils.getXPathElement(eleOrderLine, "OrderLine/ItemDetails/Extn");
								 if (!YRCPlatformUI.isVoid(eleItemExtn)) 
								 {
									 sExtnDepartment = eleItemExtn.getAttribute("ExtnDepartment");
									 sExtnDivision = eleItemExtn.getAttribute("ExtnDivision");
								 }
								 //Modified to fetch item status
								 org.w3c.dom.Element eleItemPrimaryInfo =
									 YRCXmlUtils.getXPathElement(eleOrderLine,
										"OrderLine/ItemDetails/PrimaryInformation") ;
								 if (!YRCPlatformUI.isVoid(eleItemPrimaryInfo)) 
								 {
									 sItemDescription = eleItemPrimaryInfo.getAttribute("ShortDescription") ;
									 sStatus = eleItemPrimaryInfo.getAttribute("Status") ;
								 }
								 
								 sGiftFlag = eleOrderLine.getAttribute("GiftFlag") ;
								 sGiftWrap = eleOrderLine.getAttribute("GiftWrap") ;
								 NodeList nAdditionalAttribute =
									 eleOrderLine.getElementsByTagName("AdditionalAttribute") ;
								 for (int n=0; n<nAdditionalAttribute.getLength(); n++)
								 {
									 org.w3c.dom.Element eleAdditionalAttribute =
										 (org.w3c.dom.Element) nAdditionalAttribute.item(n) ;
									 //Modified to display color and size values on the pick ticket
									 //based on the item status
									 for (int t=0; t<nXpathForItemAttributes.getLength(); t++)
									 {
										 org.w3c.dom.Element eleXpathForItemAttributes =
											 (org.w3c.dom.Element) nXpathForItemAttributes.item(t) ;
										 String sCommonCodeValue = eleXpathForItemAttributes.getAttribute("CodeValue") ;
										 if (sStatus.equalsIgnoreCase(sCommonCodeValue))
										 {
											 if (eleXpathForItemAttributes.getAttribute("CodeShortDescription")
												.equalsIgnoreCase(eleAdditionalAttribute.getAttribute("Name")))
											 {
												 sColor = eleAdditionalAttribute.getAttribute("Value") ;
											 }
											 if (eleXpathForItemAttributes.getAttribute("CodeLongDescription")
												.equalsIgnoreCase(eleAdditionalAttribute.getAttribute("Name")))
											 {
												 sSize = eleAdditionalAttribute.getAttribute("Value") ;
											 }
										 }
									 }
								 }

								 org.w3c.dom.Element eleLinePriceInfo =
									 YRCXmlUtils.getXPathElement(eleOrderLine, "OrderLine/LinePriceInfo") ;
								 if (!YRCPlatformUI.isVoid(eleLinePriceInfo))
								 {
									 sPrice = eleLinePriceInfo.getAttribute("UnitPrice") ;
									 //Start change for defect 841 to subtract discount from unit price
									 sPrice = String.valueOf((Double.valueOf(sPrice)) - (Double.valueOf(sDiscount))) ;
									 //End change for defect 841 to subtract discount from unit price
								}

								 //Gift Registry
								 org.w3c.dom.Element eleOrderLineExtn =
									 YRCXmlUtils.getXPathElement(eleOrderLine, "OrderLine/Extn") ;
								 if (!YRCPlatformUI.isVoid(eleOrderLineExtn))
								 {
									 sPackSlpPrcSuppress =
										 eleItemExtn.getAttribute("ExtnPackSlpPrcSuppress") ;
									 //sRegistrantAddrsIndictr =
									 	//eleItemExtn.getAttribute("ExtnRegistrantAddrsIndictr");
								 }
							 }

							 Phrase pUPCCode =  new Phrase(String.format(" " + sUPCCode), fontOrderLineTable) ;
							 PdfPCell UPCCodeDataCell = new PdfPCell() ;
							 UPCCodeDataCell.setBorder(0) ;
							 UPCCodeDataCell.setPhrase(pUPCCode) ;
							 UPCCodeDataCell.setFixedHeight(27f) ;
							 UPCCodeDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;

							 //Item Desc Data Cell
							 //condition to limit the max string size of item description to 40
							 String strItemDescDisplyed = "";
							 if(sItemDescription.length()>40)
							 {
								 strItemDescDisplyed = sItemDescription.substring(0, 40) ;
							 }
							 else
							 {
								 strItemDescDisplyed = sItemDescription;
							 }
							 Phrase pItemDesc =
								 new Phrase(String.format(strItemDescDisplyed), fontOrderLineTable) ;
							 //Phrase pItemDesc =
							 	//new Phrase(String.format(sItemDescription), fontCell);
							 PdfPCell ItemDescDataCell = new PdfPCell() ;
							 ItemDescDataCell.setBorder(0) ;
							 ItemDescDataCell.setPhrase(pItemDesc) ;
							 ItemDescDataCell.setFixedHeight(27f) ;
							 ItemDescDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

							 //Color Data Cell
							 Phrase pColor = new Phrase(String.format(sColor), fontOrderLineTable) ;
							 PdfPCell ColorDataCell = new PdfPCell() ;
							 ColorDataCell.setBorder(0) ;
							 ColorDataCell.setPhrase(pColor) ;
							 ColorDataCell.setFixedHeight(27f) ;
							 ColorDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

							 //Size Data Cell
							 Phrase pSize = new Phrase(String.format(sSize), fontOrderLineTable) ;
							 PdfPCell SizeDataCell = new PdfPCell();
							 SizeDataCell.setBorder(0) ;
							 SizeDataCell.setPhrase(pSize) ;
							 SizeDataCell.setFixedHeight(27f) ;
							 SizeDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;

							 //Original Quantity Data Cell
							 /*String sQuantity = null;
							sQuantity = eleShipmentLine.getAttribute("Quantity");
							if (sQuantity.contains(".")){
								sQuantity = sQuantity.replaceAll("\\..*", "");
							}
							Phrase pOrgQty = new Phrase(String.format(sQuantity), fontCell);
							PdfPCell OrgQtyDataCell = new PdfPCell();
							OrgQtyDataCell.setPhrase(pOrgQty);*/

							 //Quantity To Pick Data Cell
							 String sBackroomPickedQuantity = eleShipmentLine.getAttribute("BackroomPickedQuantity") ;
							 String sActualQuantity = eleShipmentLine.getAttribute("ActualQuantity") ;
							 String sOriginalQty = eleShipmentLine.getAttribute("Quantity") ;
							 //to refrain canceled line from being added to pack slip
							 //to have  a count of number of cancelled lines
							 if(sOriginalQty!=null)
							 {
								 if("0.0".equals(sOriginalQty) || "0".equals(sOriginalQty))
								 {
									 fCountCanceledLines++;
									 continue;
								 }
							 }
							 double dBackRoomPickedQty = 0.00;
							 double dOriginalQty = 0.00;
							 double dToPickQty = 0.00;
							 String spickedQty = "NULL";
							 if ((!sBackroomPickedQuantity.equals(""))
								&& (!sBackroomPickedQuantity.equals(null)))
							 {
								 dBackRoomPickedQty = Double.parseDouble(sBackroomPickedQuantity) ;
								 dOriginalQty = Double.parseDouble(sOriginalQty) ;
								 dToPickQty = dOriginalQty - dBackRoomPickedQty ;
								 spickedQty = Double.toString(dToPickQty) ;
								 if (spickedQty.contains("."))
								 {
									 spickedQty = spickedQty.replaceAll("\\..*", "") ;
								 }
							 }
							 else
							 {
								 spickedQty = eleShipmentLine.getAttribute("Quantity") ;
								 spickedQty = spickedQty.replaceAll("\\..*", "") ;
							 }

							 /*Phrase pPickQty = new Phrase(String.format(spickedQty), fontCell);
							PdfPCell PickQtyDataCell = new PdfPCell();
							PickQtyDataCell.setPhrase(pPickQty);*/

							 //Picked Quantity Data Cell
							 String sBackroomPickQty = "0.0" ;
							 if ((!sBackroomPickedQuantity.equals(""))
								&& (!sBackroomPickedQuantity.equals(null)))
							 {
								 sBackroomPickQty = sBackroomPickedQuantity;

								 //Start SOM changes Req#7 May'14 Release
								 //To print actual quantity picked by the customer
								 if(strStatus.equals("1400.10") && ((!sActualQuantity.equals(""))
									&& (!sActualQuantity.equals(null))))
								 {
									 sBackroomPickQty = sActualQuantity ;
								 }
								 //sBackroomPickQty = sBackroomPickQty.replaceAll("\\..*", "");
								//End SOM changes Req#7 May'14 Release
							 }
							 //Begin:Removing decimals from the quantity
							 Double dBackroomPick = Double.parseDouble(sBackroomPickQty) ;
							 BigDecimal bdBackroomPick = new BigDecimal(dBackroomPick) ;
							 bdBackroomPick= bdBackroomPick.setScale(0, RoundingMode.HALF_DOWN) ;
							 sBackroomPickQty= String.valueOf(bdBackroomPick) ;
							 //End:Removing decimals from the quantity

							 Phrase pPickedQty =
								 new Phrase(String.format(sBackroomPickQty), fontOrderLineTable) ;
							 PdfPCell PickedQtyDataCell = new PdfPCell() ;
							 PickedQtyDataCell.setBorder(0) ;
							 PickedQtyDataCell.setPhrase(pPickedQty) ;
							 PickedQtyDataCell.setFixedHeight(27f) ;
							 PickedQtyDataCell
							 	.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;

							 //Price Data Cell
							 //Begin:Removing decimals from the quantity
							 Double dPrice = Double.parseDouble(sPrice);
							 BigDecimal bdPrice = new BigDecimal(dPrice);
							 bdPrice= bdPrice.setScale(2, RoundingMode.HALF_DOWN) ;
							 sPrice= String.valueOf(bdPrice);
							 //End:Removing decimals from the quantity

							 // Start Defect 993 march release.
							 if(("Y".equalsIgnoreCase(sGiftFlag)))
							 {
								 sPrice="" ;
							 }
							 if(("N".equalsIgnoreCase(sGiftFlag))
								&& ("Y".equalsIgnoreCase(sPackSlpPrcSuppress)))
							 {
								 sPrice="0.0" ;
							 }
					         // End Defect 993 march release.

							 Phrase pPrice = new Phrase(String.format(sPrice), fontOrderLineTable) ;
							 PdfPCell PriceDataCell = new PdfPCell() ;
							 PriceDataCell.setBorder(0) ;
							 PriceDataCell.setPhrase(pPrice) ;
							 PriceDataCell.setFixedHeight(27f) ;
							 PriceDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;

							 //GiftBox Data Cell
							 Phrase pGiftBox = new Phrase(String.format("N"), fontOrderLineTable);
							 PdfPCell GiftBoxDataCell = new PdfPCell() ;
							 GiftBoxDataCell.setBorder(0) ;
							 GiftBoxDataCell.setPhrase(pGiftBox) ;
							 GiftBoxDataCell.setFixedHeight(27f) ;
							 GiftBoxDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;

							 //GiftWrap Data Cell
							 Phrase pGiftWrap = new Phrase(String.format(" "), fontOrderLineTable) ;
							 PdfPCell GiftWrapDataCell = new PdfPCell() ;
							 GiftWrapDataCell.setBorder(0) ;
							 GiftWrapDataCell.setPhrase(pGiftWrap) ;
							 GiftWrapDataCell.setFixedHeight(27f) ;
							 GiftWrapDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;

							 // Start Defect 993 march release.
							 double dTotalPrice = 0.0;
							 if(!XMLUtil.isVoid(sPrice))
							 {
								 dTotalPrice = (Double.parseDouble(sBackroomPickQty) *
										 ( Double.parseDouble(sPrice)
												 + Double.parseDouble(strEcoFeeChargeAmount))) ;
							 }
							 // End Defect 993 march release.

							 String sTotalPrice = String.valueOf(dTotalPrice) ;
							 sOrderTotal = String.valueOf(Double.parseDouble(sOrderTotal) + dTotalPrice) ;

							 //Eco fee Cell
							 //Begin: converting Eco fee to two decimal places
							 Double dEcoFeeChargeAmount = Double.parseDouble(strEcoFeeChargeAmount);
							 BigDecimal bdDecimal = new BigDecimal(dEcoFeeChargeAmount);
							 bdDecimal= bdDecimal.setScale(2, RoundingMode.HALF_UP);
							 strEcoFeeChargeAmount= String.valueOf(bdDecimal);
							 //End: converting Eco fee to two decimal places

							 if(("Y".equalsIgnoreCase(sGiftFlag)))
							 {
								 strEcoFeeChargeAmount="";
							 }
							 
							 Phrase pEcoFee =
								 new Phrase(String.format(strEcoFeeChargeAmount + " "),  fontOrderLineTable) ;
							 PdfPCell EcoFeeDataCell = new PdfPCell() ;
							 EcoFeeDataCell.setBorder(0) ;
							 EcoFeeDataCell.setPhrase(pEcoFee) ;
							 EcoFeeDataCell.setFixedHeight(27f) ;
							 EcoFeeDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

							 //Total Price Data Cell
							 //Begin: converting Total Price to two decimal places
							 Double dTotalPriceDecimalUpdate = Double.parseDouble(sTotalPrice);
							 BigDecimal bdTotalPrice = new BigDecimal(dTotalPriceDecimalUpdate);
							 bdTotalPrice= bdTotalPrice.setScale(2, RoundingMode.HALF_UP);
							 sTotalPrice= String.valueOf(bdTotalPrice);
							 //End: converting Total Price to two decimal places

							 //to display total order line price as blank if GiftFlag="Y"
							 if(("Y".equalsIgnoreCase(sGiftFlag))
								|| ("Y".equalsIgnoreCase(sPackSlpPrcSuppress)))
							 {
								 sTotalPrice="" ;
							 }

							 Phrase pTotal =
								 new Phrase(String.format(sTotalPrice), fontOrderLineTable) ;
							 PdfPCell TotalDataCell = new PdfPCell() ;
							 TotalDataCell.setBorder(0) ;
							 TotalDataCell.setPhrase(pTotal) ;
							 TotalDataCell.setFixedHeight(27f) ;
							 TotalDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

							 //Add Data cells to the table
							 orderLineTable.addCell(UPCCodeDataCell) ;
							 orderLineTable.addCell(ItemDescDataCell) ;
							 orderLineTable.addCell(ColorDataCell) ;
							 orderLineTable.addCell(SizeDataCell) ;
							 orderLineTable.addCell(PickedQtyDataCell) ;
							 orderLineTable.addCell(PriceDataCell) ;
							 orderLineTable.addCell(GiftBoxDataCell) ;
							 orderLineTable.addCell(GiftWrapDataCell) ;
							 orderLineTable.addCell(EcoFeeDataCell) ;
							 orderLineTable.addCell(TotalDataCell) ;
						 }

						 float iNoOfAddedCells = nShipmentLines.getLength() * 27 ;
						 float iTableLength = 150 ;
						 if (iNoOfAddedCells < iTableLength) 
						 {
							 float iTableLengthDiff = iTableLength - iNoOfAddedCells ;
							 orderLineTable.setSpacingAfter(iTableLengthDiff);
						 }
						 shipmentTable.addCell(orderLineTable);
						 shipmentTable.setSpacingAfter(24f);
						 
						 //Begin: Defect 850: adding height when shipment has canceled lines
						 if(fCountCanceledLines>0)
						 {
							 fCountCanceledLines = fCountCanceledLines * 27 ;
							 fCountCanceledLines = fCountCanceledLines+30 ;
							 shipmentTable.setSpacingAfter(fCountCanceledLines) ;
						 }
						//End: Defect 850: adding height when shipment has canceled lines
						 //shipmentTable.addCell(ShipTableFixHeightCell);
						 mainTable.addCell(shipmentTable) ;
					 }

					 String sInstructionText="" ;
					 double dLineTaxTotal = 0.00 ;

					 String sGSTHSTTax = "0.00" ;
					 String sPSTLineTax = "0.00" ;
					 String sRSTLineTax = "0.00" ;
					 String sQSTLineTax = "0.00" ;
					 double dTotalGSTHSTTax = 0.0 ;
					 double dTotalPSTLineTax = 0.00 ;
					 double dTotalRSTLineTax = 0.00 ;
					 double dTotalQSTLineTax = 0.00 ;

					 //Instruction and Order Total Details Table
					 PdfPTable shipmentTotalTable = new PdfPTable(2) ;
					 shipmentTotalTable.setWidthPercentage(100) ;
					 shipmentTotalTable.getDefaultCell().setBorder(0) ;
					 shipmentTotalTable
					 	.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;

					 PdfPTable instructionTextTable = new PdfPTable(1) ;
					 instructionTextTable.setWidthPercentage(100) ;
					 instructionTextTable.getDefaultCell().setBorder(0) ;
					 instructionTextTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;

					 NodeList nShipmentLines =
						 eleShipmentLines.getElementsByTagName("ShipmentLine") ;
					 for (int k=0; k<nShipmentLines.getLength(); k++)
					 {
						 String sInstructionType="" ;

						 org.w3c.dom.Element eleShipmentLine =
							 (org.w3c.dom.Element) nShipmentLines.item(k) ;

						 //UPCCode Data Cell
						 NodeList nOrderLines = eleShipmentLine.getElementsByTagName("OrderLine") ;
						 for (int l=0; l<nOrderLines.getLength(); l++)
						 {
							 org.w3c.dom.Element eleOrderLine =
								 (org.w3c.dom.Element) nOrderLines.item(l) ;

							 org.w3c.dom.Element eleLineOverallTotals =
								 YRCXmlUtils.getXPathElement(eleOrderLine,
									"OrderLine/LineOverallTotals") ;
							 String sLineOverallTotals = eleLineOverallTotals.getAttribute("Tax") ;
							 dLineTaxTotal =
								 dLineTaxTotal + Double.parseDouble(sLineOverallTotals) ;

							 //to retrieve the GST/HST taxes
							 org.w3c.dom.Element eleLineTaxes =
								 YRCXmlUtils.getXPathElement(eleOrderLine, "OrderLine/LineTaxes") ;
							 NodeList nLineTax = eleLineTaxes.getElementsByTagName("LineTax") ;
							 for (int m=0; m<nLineTax.getLength(); m++)
							 {
								 org.w3c.dom.Element eleLineTax =
									 (org.w3c.dom.Element) nLineTax.item(m) ;
								 if (!YRCPlatformUI.isVoid(eleLineTax))
								 {
									 if("GST/HST"
										.equalsIgnoreCase(eleLineTax.getAttribute("TaxName")))
									 {
										 sGSTHSTTax = eleLineTax.getAttribute("Tax");
										 if(!XMLUtil.isVoid(sGSTHSTTax))
										 {
											dTotalGSTHSTTax += Double.parseDouble(sGSTHSTTax);
										 }
									 }
									 if("Provincial Sales Tax (PST)"
										.equalsIgnoreCase(eleLineTax.getAttribute("TaxName")))
									 {
										 sPSTLineTax = eleLineTax.getAttribute("Tax") ;
										 if(!XMLUtil.isVoid(sPSTLineTax))
										 {
											 dTotalPSTLineTax+=Double.parseDouble(sPSTLineTax);
									     }
									 }
									 if("Retail Sales Tax (RST)"
										.equalsIgnoreCase(eleLineTax.getAttribute("TaxName")))
									 {
										 sRSTLineTax = eleLineTax.getAttribute("Tax");
										 if(!XMLUtil.isVoid(sRSTLineTax))
										 {
											 dTotalRSTLineTax+=Double.parseDouble(sRSTLineTax);
									 }
								 }
								 if("Quebec Sales Tax (QST)"
									.equalsIgnoreCase(eleLineTax.getAttribute("TaxName")))
								 {
									sQSTLineTax = eleLineTax.getAttribute("Tax");
									if(!XMLUtil.isVoid(sQSTLineTax))
									{
										dTotalQSTLineTax += Double.parseDouble(sQSTLineTax) ;
									}
								 }
							}
						}
						org.w3c.dom.Element eleInstructions =
							 YRCXmlUtils.getXPathElement(eleOrderLine, "OrderLine/Instructions") ;
						NodeList nInstruction =
							 eleInstructions.getElementsByTagName("Instruction") ;
						for (int m=0; m<nInstruction.getLength(); m++)
						{
							 org.w3c.dom.Element eleInstruction =
								 (org.w3c.dom.Element) nInstruction.item(m) ;
							 if (!YRCPlatformUI.isVoid(eleInstruction))
							 {
								 sInstructionType = eleInstruction.getAttribute("InstructionType") ;
								 if(sInstructionType.equalsIgnoreCase("GIFT"))
								 {
									 sInstructionText =
										 eleInstruction.getAttribute("InstructionText") ;
								 }
							 }
						 }
					 }
				 }
				 sTax = String.valueOf(Double.parseDouble(sTax) + dLineTaxTotal) ;

				 //check for this validation
				 if( sExtnRegistryId!="")
				 {
					//Gift Registry ID Data Cell
					Phrase pGiftRegistryID =
						new Phrase(String.format(sExtnRegistryId), fontCell) ;
					PdfPCell GiftRegistryIDDataCell = new PdfPCell() ;
					GiftRegistryIDDataCell.setBorder(Rectangle.NO_BORDER) ;
					GiftRegistryIDDataCell.setPhrase(pGiftRegistryID) ;

					//Gift Registrant Data Cell
					Phrase pGiftRegistrantName =
						new Phrase(String.format(sExtnRegistrantName), fontCell) ;
					PdfPCell RegistrantNameDataCell = new PdfPCell() ;
					RegistrantNameDataCell.setBorder(Rectangle.NO_BORDER) ;
					RegistrantNameDataCell.setPhrase(pGiftRegistrantName) ;

					instructionTextTable.addCell(GiftRegistryIDDataCell) ;
					instructionTextTable.addCell(RegistrantNameDataCell) ;
				 }
				 //Instruction Text Data Cell
				 Phrase pInstructionText = new Phrase(String.format(sInstructionText), fontCell) ;
				 PdfPCell InstructionTextDataCell = new PdfPCell() ;
				 InstructionTextDataCell.setBorder(Rectangle.NO_BORDER) ;
				 InstructionTextDataCell.setPhrase(pInstructionText) ;

				 instructionTextTable.addCell(InstructionTextDataCell);
				 shipmentTotalTable.addCell(instructionTextTable);

				 PdfPTable orderTotalTable = new PdfPTable(new float[]{15,10}) ;
				 orderTotalTable.setWidthPercentage(40) ;
				 orderTotalTable.getDefaultCell().setBorder(0) ;
				 orderTotalTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;

				 //Total Merchandise Data Cell
				 //Begin: converting Order Total Price to two decimal places
				 //Start: Fix for multi Orderline (greater than 5)
				 sOrderTotal = hmShipmentLine.get("OrderTotal") ;

				 Double dOrderTotalDecimalUpdate = Double.parseDouble(sOrderTotal);
				 BigDecimal bdOrderTotal = new BigDecimal(dOrderTotalDecimalUpdate);
				 bdOrderTotal= bdOrderTotal.setScale(2, RoundingMode.HALF_UP);
				 sOrderTotal = String.valueOf(bdOrderTotal) ;
				 //End: converting Total Price to two decimal places

				 //End: converting Order Total Price to two decimal places
				 //Start defect993 March release
				 if(("Y".equalsIgnoreCase(sGiftFlag)))
				 {
					 sOrderTotal = "" ;
				 }
				 //END defect993 March release

				 PdfPCell TotalMerchandiseCell =
					 new PdfPCell(new Phrase(YRCPlatformUI.getString("TOTAL_MERCHANDISE_VALUE"),
						fontHeaderDetailsBold)) ;
				 TotalMerchandiseCell.setBorder(Rectangle.NO_BORDER) ;

				 Phrase pTotalMerchandise =
					 new Phrase(String.format(sOrderTotal), fontHeaderDetails) ;
				 PdfPCell TotalMerchandiseDataCell = new PdfPCell() ;
				 TotalMerchandiseDataCell.setPhrase(pTotalMerchandise) ;
				 TotalMerchandiseDataCell.setBorder(Rectangle.NO_BORDER) ;
				 TotalMerchandiseDataCell
				 	.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;

				 //Total Shipping Charge Data Cell
				 //Begin: converting Shipping Charge to two decimal places
				 Double dShippingChargeDecimalUpdate = Double.parseDouble(sChargeAmount);
				 BigDecimal bdShippingCharge = new BigDecimal(dShippingChargeDecimalUpdate);
				 bdShippingCharge= bdShippingCharge.setScale(2, RoundingMode.HALF_UP);
				 sChargeAmount= String.valueOf(bdShippingCharge);
				 //End: converting Shipping Charge to two decimal places

				 //Start defect993 March release
				 if(("Y".equalsIgnoreCase(sGiftFlag)))
				 {
					 sChargeAmount="" ;
				 }
				 //END defect993 March release

				 PdfPCell ShippingCell =
					 new PdfPCell(new Phrase(YRCPlatformUI.getString("SHIPPING_CHARGE"),
						fontHeaderDetailsBold)) ;
				 ShippingCell.setBorder(Rectangle.NO_BORDER);
				 Phrase pShippingCell =
					 new Phrase(String.format(sChargeAmount), fontHeaderDetails) ;
				 PdfPCell ShippingDataCell = new PdfPCell();
				 ShippingDataCell.setPhrase(pShippingCell);
				 ShippingDataCell.setBorder(Rectangle.NO_BORDER);
				 ShippingDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;

				 //To print Tax amount based on Ship To States
				 String sTaxHeader = "" ;

				 //Start fix for defect851 (r3.2)
				 if(sState.equalsIgnoreCase("NB") || sState.equalsIgnoreCase("NL")
					|| sState.equalsIgnoreCase("NS") || sState.equalsIgnoreCase("ON")
						|| sState.equalsIgnoreCase("PE"))
				 {
					 sTaxHeader = YRCPlatformUI.getString("HST(10242 0296 RT0001)");
				 }
				 else if(sState.equalsIgnoreCase("QC"))
				 {
					 sTaxHeader = YRCPlatformUI.getString("TPS/GST(10242 0296 RT0001)");
				 }
				 else
				 {
					 sTaxHeader = YRCPlatformUI.getString("GST(10242 0296 RT0001)") ;
				 }
				 //End fix for defect851 (r3.2)

				 //total GST/HST taxes summing header and line level tax
				 String strTotalGSTHSTTax = String.valueOf(dTotalGSTHSTTax + Double.parseDouble(sTaxBAY)) ;
				 //Begin: converting Shipping Charge to two decimal places

				 //Updating taxes for multi-Orderlines
				 strTotalGSTHSTTax= hmShipmentLine.get("TotalGSTHSTTax") ;
				 Double dTotalGSTHSTTaxRoundOff = Double.parseDouble(strTotalGSTHSTTax) ;
				 BigDecimal bdTotalGSTHSTTax = new BigDecimal(dTotalGSTHSTTaxRoundOff) ;
				 bdTotalGSTHSTTax = bdTotalGSTHSTTax.setScale(2, RoundingMode.HALF_UP) ;
				 strTotalGSTHSTTax = String.valueOf(bdTotalGSTHSTTax) ;

				 //End: converting Shipping Charge to two decimal places
				 //Start defect993 March release
				 if(("Y".equalsIgnoreCase(sGiftFlag)))
				 {
					 strTotalGSTHSTTax = "" ;
				 }
				 //END defect993 March release

				 PdfPCell TaxGSTHSTCell =
					 new PdfPCell(new Phrase(YRCPlatformUI.getString(sTaxHeader),
						fontHeaderDetailsBold)) ;
				 TaxGSTHSTCell.setBorder(Rectangle.NO_BORDER) ;
				 Phrase pTaxGSTHSTCell =
					 new Phrase(String.format(strTotalGSTHSTTax), fontHeaderDetails) ;
				 PdfPCell TaxGSTHSTDataCell = new PdfPCell() ;
				 TaxGSTHSTDataCell.setPhrase(pTaxGSTHSTCell) ;
				 TaxGSTHSTDataCell.setBorder(Rectangle.NO_BORDER) ;
				 TaxGSTHSTDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;

				 //To print  PST/RST/QST Tax amount cell based on Ship To States
				 //Start:defect 551 : replace sState with sStoreState
				 String sPSTRSTQSTHeader = "" ;
				 if(sStoreState.equalsIgnoreCase("BC") || sStoreState.equalsIgnoreCase("SK"))
				 {
					 sPSTRSTQSTHeader = "PST" ;
				 }
				 else if(sStoreState.equalsIgnoreCase("MB"))
				 {
					 sPSTRSTQSTHeader = "RST" ;
				 }
				 else if(sStoreState.equalsIgnoreCase("QC"))
				 {
					 sPSTRSTQSTHeader = "TVQ/QST (100801 1563 TQ0501)" ;
				 }
				//End:defect551	
				 //summing header level and line level PST RST and QST taxes
				 double dPSTRSTQSTHeaderTax = 0.00 ;
				 double dPSTRSTQSTLineTax = 0.00 ;
				 double dTotalPSTRSTQSTTax = 0.00 ;
				 String sTotalPSTRSTQSTTax = "0.00" ;
				 dPSTRSTQSTHeaderTax = Double.parseDouble(sPSTTaxBAY)
				 	+ Double.parseDouble(sRSTTaxBAY) + Double.parseDouble(sQSTTaxBAY) ;
				 dPSTRSTQSTLineTax = dTotalPSTLineTax + dTotalRSTLineTax + dTotalQSTLineTax ;
				 dTotalPSTRSTQSTTax = dPSTRSTQSTHeaderTax + dPSTRSTQSTLineTax ;
				 sTotalPSTRSTQSTTax = String.valueOf(dTotalPSTRSTQSTTax) ;

				 //Updating the taxes for Multi OrderLines
				 sTotalPSTRSTQSTTax = hmShipmentLine.get("TotalPSTRSTQSTTax") ;
				 //creating cell to add PST/RST/QST taxes

				 //Begin: converting Total Tax to two decimal places
				 Double dTotalPSTRSTQSTTaxDecimalUpdate = Double.parseDouble(sTotalPSTRSTQSTTax) ;
				 BigDecimal bddTotalPSTRSTQSTTax = new BigDecimal(dTotalPSTRSTQSTTaxDecimalUpdate) ;
				 bddTotalPSTRSTQSTTax= bddTotalPSTRSTQSTTax.setScale(2, RoundingMode.HALF_UP) ;
				 sTotalPSTRSTQSTTax= String.valueOf(bddTotalPSTRSTQSTTax);
				 //End: converting Total Tax to two decimal places

				 //Start defect993 March release
				 if(("Y".equalsIgnoreCase(sGiftFlag)))
				 {
					 sTotalPSTRSTQSTTax = "" ;
				 }
				 //END defect993 March release

				 PdfPCell PSTRSTQSTTaxCell =
					 new PdfPCell(new Phrase(YRCPlatformUI.getString(sPSTRSTQSTHeader),
						fontHeaderDetailsBold)) ;
				 PSTRSTQSTTaxCell.setBorder(Rectangle.NO_BORDER) ;
				 Phrase pPSTRSTQSTTaxCellCell =
					 new Phrase(String.format(sTotalPSTRSTQSTTax), fontHeaderDetails) ;
				 PdfPCell PSTRSTQSTTaxCellDataCell = new PdfPCell() ;
				 PSTRSTQSTTaxCellDataCell.setPhrase(pPSTRSTQSTTaxCellCell) ;
				 PSTRSTQSTTaxCellDataCell.setBorder(Rectangle.NO_BORDER) ;
				 PSTRSTQSTTaxCellDataCell
				 	.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;

				 //Authorization ID Data Cell
				 PdfPCell AuthorizationIDCell =
					 new PdfPCell(new Phrase(YRCPlatformUI.getString("AUTHORIZATION_NUMBER"),
						fontHeaderDetailsBold)) ;
				 AuthorizationIDCell.setBorder(Rectangle.NO_BORDER) ;

				 Phrase pAuthorizationIDCell =
					 new Phrase(String.format(sAuthorizationID), fontHeaderDetails) ;
				 PdfPCell AuthorizationIDDataCell = new PdfPCell() ;
				 AuthorizationIDDataCell.setPhrase(pAuthorizationIDCell) ;
				 AuthorizationIDDataCell.setBorder(Rectangle.NO_BORDER) ;
				 AuthorizationIDDataCell
				 	.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;

				 //Empty Header Data Cell
				 PdfPCell EmptyHeaderCell =
					 new PdfPCell(new Phrase(" ", fontHeaderDetailsBold)) ;
				 EmptyHeaderCell.setBorder(Rectangle.NO_BORDER) ;

				//Empty Data Cell
				 PdfPCell EmptyDataCell =
					 new PdfPCell(new Phrase(" ", fontHeaderDetailsBold)) ;
				 EmptyDataCell.setBorder(Rectangle.NO_BORDER) ;

				 orderTotalTable.addCell(TotalMerchandiseCell) ;
				 orderTotalTable.addCell(TotalMerchandiseDataCell) ;
				 orderTotalTable.addCell(ShippingCell) ;
				 orderTotalTable.addCell(ShippingDataCell) ;
				 orderTotalTable.addCell(TaxGSTHSTCell) ;
				 orderTotalTable.addCell(TaxGSTHSTDataCell) ;

				 //adding cell only if ShipTo states are in 'BC', 'SK','MB','QC'
				 if(!(sPSTRSTQSTHeader==""))
				 {
					 orderTotalTable.addCell(PSTRSTQSTTaxCell) ;
					 orderTotalTable.addCell(PSTRSTQSTTaxCellDataCell) ;
				 }

				 orderTotalTable.addCell(AuthorizationIDCell) ;
				 orderTotalTable.addCell(AuthorizationIDDataCell) ;

				//adding cell only if ShipTo states are in 'BC', 'SK','MB','QC'
				 if(sPSTRSTQSTHeader=="")
				 {
					orderTotalTable.addCell(EmptyHeaderCell);
					orderTotalTable.addCell(EmptyDataCell);
				 }

				 shipmentTotalTable.addCell(orderTotalTable) ;
				 //shipmentTotalTable.setSpacingAfter(10f);
				 mainTable.addCell(shipmentTotalTable) ;

				//PayPal and Bar Code Cell
				 PdfPTable PayPalTransactionIDTable = new PdfPTable(2) ;
				 PayPalTransactionIDTable.setWidthPercentage(100) ;
				 PayPalTransactionIDTable.getDefaultCell().setBorder(0) ;
				 PayPalTransactionIDTable
				 	.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;
				 PayPalTransactionIDTable.setSpacingBefore(20f) ;

				 //Payment Type Data Cell
				 Phrase pBlankCellPaymentType = new Phrase(String.format(" "), fontCell) ;
				 PdfPCell BlankCellPaymentTypeDataCell = new PdfPCell() ;
				 BlankCellPaymentTypeDataCell.setBorder(Rectangle.NO_BORDER) ;
				 BlankCellPaymentTypeDataCell.setFixedHeight(25f) ;
				 BlankCellPaymentTypeDataCell
				 	.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;
				 BlankCellPaymentTypeDataCell.setPhrase(pBlankCellPaymentType) ;

				 //Payment Type Data Cell
				 if(sPaymentType!="")
				 {
					 sPaymentType = YRCPlatformUI.getString("PAYMENT_TYPE") + "::" + sPaymentType ;
				 }
				 
				 Phrase pPaymentType = new Phrase(String.format(sPaymentType), fontCell) ;
				 PdfPCell PaymentTypeDataCell = new PdfPCell() ;
				 PaymentTypeDataCell.setBorder(Rectangle.NO_BORDER) ;
				 PaymentTypeDataCell.setFixedHeight(32f) ;
				 PaymentTypeDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;
				 PaymentTypeDataCell.setPhrase(pPaymentType) ;
				 PaymentTypeDataCell.setVerticalAlignment(com.lowagie.text.Element.ALIGN_MIDDLE) ;

				 //Payment Type Blank Data Cell
				 Phrase pBlankCellBarCodeTransactionID = new Phrase(String.format(""), fontCell) ;
				 PdfPCell BlankCellBarCodeTransactionIDDataCell = new PdfPCell() ;
				 BlankCellBarCodeTransactionIDDataCell.setBorder(Rectangle.NO_BORDER) ;
				 BlankCellBarCodeTransactionIDDataCell.setFixedHeight(25f) ;
				 BlankCellBarCodeTransactionIDDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;
				 BlankCellBarCodeTransactionIDDataCell.setPhrase(pBlankCellBarCodeTransactionID) ;

				 //Barcode for 678+TransactionID
				 String sBarCodeTransactionID = "678" ;
				 sBarCodeTransactionID = sBarCodeTransactionID.concat(sExtnTransactionID) ;
				 code128.setCode(sBarCodeTransactionID) ;
				 Image imageBarCode = code128.createImageWithBarcode(writer.getDirectContent(), null, null) ;
				 PdfPCell BarCodeTransactionIDCell = new PdfPCell(imageBarCode,false) ;
				 BarCodeTransactionIDCell.setBorder(Rectangle.NO_BORDER) ;
				 //BarCodeTransactionIDCell.setFixedHeight(25f);
				 BarCodeTransactionIDCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

				 PayPalTransactionIDTable.addCell(BlankCellPaymentTypeDataCell) ;
				 PayPalTransactionIDTable.addCell(PaymentTypeDataCell) ;
				 PayPalTransactionIDTable.addCell(BlankCellBarCodeTransactionIDDataCell) ;
				 PayPalTransactionIDTable.addCell(BarCodeTransactionIDCell) ;
				 PayPalTransactionIDTable.setSpacingAfter(20f) ;

				 mainTable.addCell(PayPalTransactionIDTable) ;
				 document.add(mainTable) ;

				 //Ship From Address details
				 PdfPTable ShipFromAddressTable = new PdfPTable(1) ;
				 ShipFromAddressTable.setWidthPercentage(100) ;
				 ShipFromAddressTable.getDefaultCell().setBorder(0) ;
				 ShipFromAddressTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;
				 ShipFromAddressTable.setSpacingBefore(15f) ;

				 //adding data for Ship From address cell
				 Phrase pShipFromAddress = new Phrase("    "
					+ YRCPlatformUI.getString("SHIP_FROM_ADDRESS"), boldFontCell) ;
				 PdfPCell ShipFromAddressDataCell = new PdfPCell() ;
				 ShipFromAddressDataCell.setBorder(Rectangle.NO_BORDER) ;
				 ShipFromAddressDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;
				 ShipFromAddressDataCell.setPhrase(pShipFromAddress) ;
				 ShipFromAddressDataCell.setFixedHeight(20f) ;

				 //adding Customer to ship from address
				 Phrase pShipFromCustomerName = new Phrase("    "
					+ String.format(sCustomerName), fontCell) ;
				 PdfPCell ShipFromCustomerNameDataCell = new PdfPCell() ;
				 ShipFromCustomerNameDataCell.setBorder(Rectangle.NO_BORDER) ;
				 ShipFromCustomerNameDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;
				 ShipFromCustomerNameDataCell.setPhrase(pShipFromCustomerName) ;

				 //adding AddressLine1 to ship from address
				 Phrase pShipFromAddressLine1 = new Phrase("    " + String.format(sAddressLine1), fontCell) ;
				 PdfPCell ShipFromAddressLine1DataCell = new PdfPCell() ;
				 ShipFromAddressLine1DataCell.setBorder(Rectangle.NO_BORDER) ;
				 ShipFromAddressLine1DataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;
				 ShipFromAddressLine1DataCell.setPhrase(pShipFromAddressLine1) ;

				 //adding AddressLine2 to ship from address
				 Phrase pShipFromAddressLine2 = new Phrase("    "
					+ String.format(sAddressLine2), fontCell) ;
				 PdfPCell ShipFromAddressLine2DataCell = new PdfPCell() ;
				 ShipFromAddressLine2DataCell.setBorder(Rectangle.NO_BORDER) ;
				 ShipFromAddressLine2DataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;
				 ShipFromAddressLine2DataCell.setPhrase(pShipFromAddressLine2) ;

				 //Add FormattedCityStateZipCode
				 Phrase pShipFromToFormattedCityStateZipCode = new Phrase("    "
					+ String.format(sFormattedCityStateZipCode), fontCell) ;
				 PdfPCell ShipFromFormattedCityStateZipCodeStoreDataCell = new PdfPCell() ;
				 ShipFromFormattedCityStateZipCodeStoreDataCell.setPhrase(pShipFromToFormattedCityStateZipCode) ;
				 ShipFromFormattedCityStateZipCodeStoreDataCell.setBorder(Rectangle.NO_BORDER) ;

				 //Add Country
				 Phrase pShipFromCountry = new Phrase("    "	+ String.format(sCountry), fontCell) ;
				 PdfPCell ShipFromCountryDataCell = new PdfPCell() ;
				 ShipFromCountryDataCell.setPhrase(pShipFromCountry) ;
				 ShipFromCountryDataCell.setBorder(Rectangle.NO_BORDER) ;

				//Add Dummy Cell
				 Phrase pShipFromDummyCell = new Phrase(String.format(" "), fontAddressCell) ;
				 PdfPCell ShipFromDummyCell = new PdfPCell() ;
				 ShipFromDummyCell.setPhrase(pShipFromDummyCell) ;
				 ShipFromDummyCell.setBorder(Rectangle.NO_BORDER) ;
				 ShipFromDummyCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;

				 ShipFromAddressTable.addCell(ShipFromAddressDataCell) ;
				 ShipFromAddressTable.addCell(ShipFromCustomerNameDataCell) ;
				 ShipFromAddressTable.addCell(ShipFromAddressLine1DataCell) ;
				 
				 if (sToAddressLine2 != null && sToAddressLine2 != "")
				 {
				 ShipFromAddressTable.addCell(ShipFromAddressLine2DataCell) ;
				 }
				 
				 ShipFromAddressTable.addCell(ShipFromFormattedCityStateZipCodeStoreDataCell) ;
				 ShipFromAddressTable.addCell(ShipFromCountryDataCell) ;
				 
				 if (!(sToAddressLine2 != null && sToAddressLine2 != ""))
				 {
					 ShipFromAddressTable.addCell(ShipFromDummyCell) ;
				 }
				 
				 ShipFromAddressTable.setSpacingAfter(10f) ;
				 document.add(ShipFromAddressTable) ;

				 //Ship To Address details
				 PdfPTable ShipToAddressTable = new PdfPTable(1) ;
				 ShipToAddressTable.setWidthPercentage(50) ;
				 ShipToAddressTable.getDefaultCell().setBorder(0) ;
				 ShipToAddressTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;

				 //adding data for Ship To address cell
				 Phrase pShipToAddress = new Phrase("    "
					+ YRCPlatformUI.getString("SHIP_TO_ADDRESS_PACKSLIP"), boldFontCell) ;
				 PdfPCell ShipToAddressDataCell = new PdfPCell() ;
				 ShipToAddressDataCell.setBorder(Rectangle.NO_BORDER) ;
				 ShipToAddressDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;
				 ShipToAddressDataCell.setPhrase(pShipToAddress) ;
				 ShipToAddressDataCell.setFixedHeight(20f) ;

				 //adding AddressLine1 to ship to address
				 Phrase pShipToAddressLine1 = new Phrase("    "
					+ YRCPlatformUI.getString("AddressLine1"), fontCell) ;
				 PdfPCell ShipToAddressLine1DataCell = new PdfPCell() ;
				 ShipToAddressLine1DataCell.setBorder(Rectangle.NO_BORDER) ;
				 ShipToAddressLine1DataCell
				 	.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;
				 ShipToAddressLine1DataCell.setPhrase(pShipToAddressLine1) ;

				 //adding AddressLine2 to ship to address
				 Phrase pShipToAddressLine2 = new Phrase("    "
					+ YRCPlatformUI.getString("AddressLine2"), fontCell) ;
				 PdfPCell ShipToAddressLine2DataCell = new PdfPCell() ;
				 ShipToAddressLine2DataCell.setBorder(Rectangle.NO_BORDER) ;
				 ShipToAddressLine2DataCell
				 	.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);
				 ShipToAddressLine2DataCell.setPhrase(pShipToAddressLine2) ;

				 //adding AddressLine3 to ship to address
				 Phrase pShipToAddressLine3 = new Phrase("    "
					+ YRCPlatformUI.getString("AddressLine3"), fontCell) ;
				 PdfPCell ShipToAddressLine3DataCell = new PdfPCell() ;
				 ShipToAddressLine3DataCell.setBorder(Rectangle.NO_BORDER) ;
				 ShipToAddressLine3DataCell
				 	.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;
				 ShipToAddressLine3DataCell.setPhrase(pShipToAddressLine3) ;

				 //adding cells to ShiToAddressTable
				 ShipToAddressTable.addCell(ShipToAddressDataCell) ;
				 ShipToAddressTable.addCell(ShipToAddressLine1DataCell) ;
				 ShipToAddressTable.addCell(ShipToAddressLine2DataCell) ;
				 ShipToAddressTable.addCell(ShipToAddressLine3DataCell) ;
				 //ShipToAddressTable.setSpacingAfter(25f);

				 document.add(ShipToAddressTable) ;

				//Order # details
				 PdfPTable OrderNoTable = new PdfPTable(2) ;
				 OrderNoTable.setWidthPercentage(50) ;
				 OrderNoTable.getDefaultCell().setBorder(0) ;
				 OrderNoTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;

				 //adding blank cell
				 Phrase pOrderNoBlank = new Phrase(String.format(""), fontCell) ;
				 PdfPCell OrderNoBlankCell = new PdfPCell() ;
				 OrderNoBlankCell.setBorder(Rectangle.NO_BORDER) ;
				 OrderNoBlankCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;
				 OrderNoBlankCell.setPhrase(pOrderNoBlank) ;

				 //Payment Type Data Cell
					 code128.setCode(sOrderNo) ;
					 Image imageOrderBarCode =
						 code128.createImageWithBarcode(writer.getDirectContent(), null, null) ;
					 PdfPCell OrderBarCodeCell = new PdfPCell(imageOrderBarCode,false) ;
					 OrderBarCodeCell.setBorder(Rectangle.NO_BORDER) ;
					 OrderBarCodeCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

					 OrderNoTable.addCell(OrderNoBlankCell) ;
					 OrderNoTable.addCell(OrderBarCodeCell) ;
					 document.add(OrderNoTable) ;
				 }
			 }
		 }
	 }
 }
		 catch (Exception e)
		 {
			e.printStackTrace() ;
		 }
		 document.newPage() ;
	}


	/**
	* This method is implemented to generate the pick list in the required format with barcode using iText
	*/


	public void generateBarCode () throws Exception
	{
		//Format of the barcode to be used
		Barcode128 code128 = new Barcode128() ;
		code128.setGenerateChecksum(true) ;

		//Fetch the temp directory
		String sDocumentLocation = System.getProperty ("java.io.tmpdir") ;
		String sDocumentType = ".pdf" ;
//		com.lowagie.text.Document document = new com.lowagie.text.Document(new Rectangle(PageSize.A4.rotate()));
		 com.lowagie.text.Document document = new com.lowagie.text.Document(new Rectangle (842, 595)) ;
		 document.setMargins(30f, 30f, 30f, 30f);
		//Adding data to the table

		org.w3c.dom.Element eleSortedShipmentOutput = this.getModel("Extn_output_HBCPrintPickList");
		if (!YRCPlatformUI.isVoid(eleSortedShipmentOutput))
		{
			org.w3c.dom.Element eleDocumentName  =
				YRCXmlUtils.getXPathElement(eleSortedShipmentOutput,
					"Shipments/Shipment/ShipmentDetails/MultiApi/API/Output/Shipment") ;
			if (!YRCPlatformUI.isVoid(eleDocumentName))
			{
				String sDocumentName = eleDocumentName.getAttribute("ShipmentNo") ;

				String sDocumentPathAndName = sDocumentLocation+sDocumentName+sDocumentType ;
				PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(sDocumentPathAndName)) ;
				document.open();

				NodeList nSortedShipmentList =  eleSortedShipmentOutput.getElementsByTagName("ShipmentDetails") ;
				for (int g = 0; g < nSortedShipmentList.getLength(); g++)
				{
					document.newPage();
					org.w3c.dom.Element eleShipmentDetails = (org.w3c.dom.Element) nSortedShipmentList.item(g) ;
					org.w3c.dom.Element eleShipment =
						YRCXmlUtils.getXPathElement(eleShipmentDetails, "ShipmentDetails/MultiApi/API/Output/Shipment") ;
					if (!YRCPlatformUI.isVoid(eleShipment))
					{
						org.w3c.dom.Element eleToAddress =
							YRCXmlUtils.getXPathElement(eleShipment, "Shipment/ToAddress") ;

						//Start SOM changes Req#16 May'14 Release
						String sBillToDayPhone="";//Declared DayPhoneNumber variable for BillToAddress tag
						currentPageNo=1; //Setting the PageNo to 0 for each shipment
						//End SOM changes Req#16 May'14 Release

						//Start SOM changes Req#2 May'14 Release */
						String sPickUpDate = "";//Declared Pickup date variable
						//Start:Defect#1094
						org.w3c.dom.Element eleLineExtn =
							YRCXmlUtils.getXPathElement(eleShipment, "Shipment/ShipmentLines/ShipmentLine/OrderLine/Extn");
						if (!YRCPlatformUI.isVoid(eleLineExtn)){
							sPickUpDate = eleLineExtn.getAttribute("ExtnPickupDate");
						}
						//End:Defect#1094
						//End SOM changes Req#2 May'14 Release */

						//Start SOM changes Req#16 May'14 Release */
						//Start: Defect# 1089 : Fix Phone Number from BillToAddress
						org.w3c.dom.Element eleBillToAddress = YRCXmlUtils.getXPathElement(eleShipment, "Shipment/BillToAddress");
						if (!YRCPlatformUI.isVoid(eleBillToAddress)){
							sBillToDayPhone = eleBillToAddress.getAttribute("DayPhone");
						}
						//End : Defect# 1089 : Fix Phone Number from BillToAddress
						//End SOM changes Req#16 May'14 Release */

						//Checking if the Order is a BorderFree Order as part of CR
						String strIsOrderFiftyOne="";
						org.w3c.dom.Element eleOrderExtn =
							YRCXmlUtils.getXPathElement(eleShipment, "Shipment/ShipmentLines/ShipmentLine/Order/Extn");
						if (!YRCPlatformUI.isVoid(eleLineExtn)){
							strIsOrderFiftyOne = eleOrderExtn.getAttribute("ExtnIsFiftyOne");
						}

						if (!YRCPlatformUI.isVoid(eleToAddress))
						{
							String sCity = eleToAddress.getAttribute("City");
							String sState = eleToAddress.getAttribute("State");
							String sShortZipCode = eleToAddress.getAttribute("ZipCode");
							String sFormattedCityStateZipCodeStore = sCity + ' ' + sState + ' ' + sShortZipCode ;
							String sFirstName = eleToAddress.getAttribute("FirstName");
							String sLastName = eleToAddress.getAttribute("LastName");
							String sCompany = eleToAddress.getAttribute("Company");
							String sAddressLine1 = eleToAddress.getAttribute("AddressLine1");
							String sAddressLine2 = eleToAddress.getAttribute("AddressLine2");
							String sCountry = eleToAddress.getAttribute("Country");
							String sDayPhone = eleToAddress.getAttribute("DayPhone");
							String sEmailID = eleToAddress.getAttribute("EmailID");
							String sCustomerName = sFirstName+" "+sLastName;
							String sOrderDate ="";
							String sOrderNo ="";
							String sDeliveryMethod = eleShipment.getAttribute("DeliveryMethod");
							//Start: Req#1 Pick ticket Enhancement
							//Retrieving the Shipping Method for Shipping Order to display in PickTicket
							String sShippingMethod = eleShipment.getAttribute("CarrierServiceCode");
							//End: Req#1 Pick ticket Enhancement
							String sShipmentNo = eleShipment.getAttribute("ShipmentNo");
							String sShipDate = eleShipment.getAttribute("ShipDate");
							String sReqShipDate = eleShipment.getAttribute("RequestedShipmentDate");
							String sExpectedDeliveryDate = eleShipment.getAttribute("ExpectedDeliveryDate");

							//Start SOM changes Req#2 May'14 Release */
							int iNoOfDaysToAdd = 0;
							//get numbers of days to be added in pickup date display from bundle file
							String sNoOfDaysToAdd = YRCPlatformUI.getString("NO_OF_DAYS_TO_ADD");
							//update integer value (iNoOfDaysToAdd)of days from the fetched value (sNoOfDaysToAdd)
							if(!XMLUtil.isVoid(sNoOfDaysToAdd)){
								iNoOfDaysToAdd = Integer.parseInt(sNoOfDaysToAdd);
							}
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
							Calendar cCalendar = Calendar.getInstance();
							//If ExtnPickUp Date is blank or null then use RequestedShipmentDate
							if(XMLUtil.isVoid(sPickUpDate)){
								cCalendar.setTime(sdf.parse(sReqShipDate));
								cCalendar.add(Calendar.DATE, iNoOfDaysToAdd); // Adding iNoOfDaysToAdd days to RequestedShipmentDate
								sPickUpDate = sdf.format(cCalendar.getTime());
							}else{
								cCalendar.setTime(sdf.parse(sPickUpDate));
								cCalendar.add(Calendar.DATE, iNoOfDaysToAdd); // Adding iNoOfDaysToAdd days to ExtnPickUpDate
								sPickUpDate = sdf.format(cCalendar.getTime());
							}
							//End SOM changes Req#2 May'14 Release */

							String strOrderType="";
							org.w3c.dom.Element eleOrder =
								YRCXmlUtils.getXPathElement(eleShipment, "Shipment/ShipmentLines/ShipmentLine/Order") ;
							if (!YRCPlatformUI.isVoid(eleOrder))
							{
								sOrderDate = eleOrder.getAttribute("OrderDate") ;
								sOrderNo = eleOrder.getAttribute("OrderNo") ;
								strOrderType=eleOrder.getAttribute("OrderType");
								org.w3c.dom.Element eleShipNodePersonInfo =
									YRCXmlUtils.getXPathElement(eleShipment, "Shipment/ShipNode/ShipNodePersonInfo") ;
								if (!YRCPlatformUI.isVoid(eleShipNodePersonInfo))
								{
									String sStoreName = eleShipNodePersonInfo.getAttribute("Description") ;
									String sStoreCompany = eleShipNodePersonInfo.getAttribute("Company") ;
									String sStoreAddress1 = eleShipNodePersonInfo.getAttribute("AddressLine1") ;
									String sStoreAddress2 = eleShipNodePersonInfo.getAttribute("AddressLine2") ;
									String sStoreCity = eleShipNodePersonInfo.getAttribute("City") ;
									String sStoreState = eleShipNodePersonInfo.getAttribute("State") ;
									String sStoreZipCode = eleShipNodePersonInfo.getAttribute("ZipCode") ;
									String sStoreDayPhone = eleShipNodePersonInfo.getAttribute("DayPhone") ;
									String sStoreEmailID = eleShipNodePersonInfo.getAttribute("EmailID") ;
									String sStoreFormattedCityStateZipCode = sStoreCity + ' '
										+ sStoreState + ' ' + sStoreZipCode ;

									//Fonts
									BaseFont bFontHeaderDetails =
										BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, false) ;
									Font fontHeaderDetails = new Font(bFontHeaderDetails, 9, Font.NORMAL, Color.BLACK) ;

									BaseFont bFontHeaderDetailsBold =
										BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, false) ;
									Font fontHeaderDetailsBold =
										new Font(bFontHeaderDetailsBold, 9, Font.BOLD, Color.BLACK) ;
									
									//Defect fix 232: Start
									Font fontHeaderDetailsBoldRed =
										new Font(bFontHeaderDetailsBold, 9, Font.BOLD, Color.RED) ;
									//Defect fix 232: Start
									
									BaseFont bfontCell =
										BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, false) ;
									Font fontCell = new Font(bfontCell, 7, Font.NORMAL, Color.BLACK) ;

									//Store Details Table
									PdfPTable storeDetailsTable = new PdfPTable(1) ;
									storeDetailsTable.setWidthPercentage(30) ;
									storeDetailsTable.getDefaultCell().setBorder(0) ;
									storeDetailsTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

									//Store Name Details
									Phrase pStoreName = new Phrase(String.format(sStoreName), fontHeaderDetailsBold) ;
									PdfPCell StoreNameDataCell = new PdfPCell() ;
									StoreNameDataCell.setPhrase(pStoreName) ;
									StoreNameDataCell.setBorder(Rectangle.NO_BORDER) ;

									//Company Details
									Phrase pStoreCompany =
										new Phrase(String.format(sStoreCompany), fontHeaderDetails) ;
									PdfPCell StoreCompanyDataCell = new PdfPCell() ;
									StoreCompanyDataCell.setPhrase(pStoreCompany) ;
									StoreCompanyDataCell.setBorder(Rectangle.NO_BORDER) ;

									//StoreAddress1 Details
									Phrase pStoreAddress1 =
										new Phrase(String.format(sStoreAddress1), fontHeaderDetails) ;
									PdfPCell StoreAddress1DataCell = new PdfPCell();
									StoreAddress1DataCell.setPhrase(pStoreAddress1);
									StoreAddress1DataCell.setBorder(Rectangle.NO_BORDER);

									//StoreAddress2 Details
									Phrase pStoreAddress2 =
										new Phrase(String.format(sStoreAddress2), fontHeaderDetails) ;
									PdfPCell StoreAddress2DataCell = new PdfPCell();
									StoreAddress2DataCell.setPhrase(pStoreAddress2);
									StoreAddress2DataCell.setBorder(Rectangle.NO_BORDER);

									//Company Details
									Phrase pStoreFormattedCityStateZipCode =
										new Phrase(String.format(sStoreFormattedCityStateZipCode), fontHeaderDetails) ;
									PdfPCell StoreFormattedCityStateZipCodeDataCell = new PdfPCell() ;
									StoreFormattedCityStateZipCodeDataCell.setPhrase(pStoreFormattedCityStateZipCode) ;
									StoreFormattedCityStateZipCodeDataCell.setBorder(Rectangle.NO_BORDER) ;

									//Store  Day Phone Details
									// Start : 05/15/2014 : Defect# 188 converting phone# to phone# format
									sStoreDayPhone = sStoreDayPhone.replaceFirst("^(\\d{3})(\\d{3})(\\d{4})$", "$1-$2-$3");  
									// End : 05/15/2014 : Defect# 188 converting phone# to phone# format
									Phrase pStoreDayPhone =
										new Phrase(String.format(sStoreDayPhone), fontHeaderDetails) ;
									PdfPCell StoreDayPhoneDataCell = new PdfPCell() ;
									StoreDayPhoneDataCell.setPhrase(pStoreDayPhone) ;
									StoreDayPhoneDataCell.setBorder(Rectangle.NO_BORDER) ;

									//Company Email Details
									Phrase pStoreEmailID =
										new Phrase(String.format(sStoreEmailID), fontHeaderDetails) ;
									PdfPCell StoreEmailIDDataCell = new PdfPCell() ;
									StoreEmailIDDataCell.setPhrase(pStoreEmailID) ;
									StoreEmailIDDataCell.setBorder(Rectangle.NO_BORDER) ;

									storeDetailsTable.addCell(StoreNameDataCell) ;
									storeDetailsTable.addCell(StoreCompanyDataCell) ;
									storeDetailsTable.addCell(StoreAddress1DataCell) ;
									//Defect fix 145: Start
									if(!sStoreAddress2.equals("") && sStoreAddress2!=null)
									{
										storeDetailsTable.addCell(StoreAddress2DataCell) ;
									}
									//Defect fix 145: End
									storeDetailsTable.addCell(StoreFormattedCityStateZipCodeDataCell) ;
									storeDetailsTable.addCell(StoreDayPhoneDataCell) ;
									storeDetailsTable.addCell(StoreEmailIDDataCell) ;

//									document.add(storeDetailsTable) ;

									//Add document Title
									PdfPCell documentName = new PdfPCell();
									Paragraph pName = addTitle(document);
									documentName.setPhrase(pName);
									documentName.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
									documentName.setBorder(Rectangle.NO_BORDER) ;

									//Start : Method call to add Customer Name at Header Title
									Paragraph pCustName = addTitleCustName(document, sCustomerName);
									PdfPCell CustomerName = new PdfPCell();
									CustomerName.setPhrase(pCustName);
									CustomerName.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
									CustomerName.setBorder(Rectangle.NO_BORDER);

									PdfPTable documentDetailsTable = new PdfPTable(1) ;
									documentDetailsTable.setWidthPercentage(30) ;
									documentDetailsTable.getDefaultCell().setBorder(0);
									documentDetailsTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

									//Empty Cell Details
									PdfPCell BlankDataCell= new PdfPCell(new Phrase(" ", fontHeaderDetails)) ;
									BlankDataCell.setBorder(Rectangle.NO_BORDER);

									//Start SOM changes Req#3 May'14 Release */
									//Start : Delivery Method Details Title Table
									PdfPTable titleDetailsTable = new PdfPTable(2) ;
//									titleDetailsTable.setSpacingBefore(10f) ;
									titleDetailsTable.setWidthPercentage(30) ;
									titleDetailsTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

									// Pick ticket Enhancement Start : Delivery Method Details
									String sDelMethod = sDeliveryMethod ;
									if(sDelMethod.equalsIgnoreCase("PICK"))
									{
										sDelMethod = YRCPlatformUI.getString("PICK");
									}
									else
									{
										//Start: Req#2 Pick ticket Enhancement
										//Displaying the Delivery Method as International shipping for Borderfree Orders.
										if(strIsOrderFiftyOne.equals("Y"))
										{
											sDelMethod = YRCPlatformUI.getString("INTERNATIONAL_SHIPPING");;
										}
										//End: Req#2 Pick ticket Enhancement
										else
										{
											sDelMethod = YRCPlatformUI.getString("SHIP");
										}
									}
									// Pick ticket Enhancement End : Delivery Method Details

									PdfPCell DeliveryMethodNameCell =
										new PdfPCell(new Phrase(YRCPlatformUI.getString("DELIVERY_METHOD"), fontHeaderDetailsBold)) ;
									DeliveryMethodNameCell.setBorder(Rectangle.NO_BORDER);
									DeliveryMethodNameCell
										.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;
									Phrase pDeliveryMethodName = new Phrase(sDelMethod, fontHeaderDetailsBold);
									PdfPCell DeliveryMethodDataCell = new PdfPCell();
									DeliveryMethodDataCell.setPhrase(pDeliveryMethodName);
									DeliveryMethodDataCell.setBorder(Rectangle.NO_BORDER);
									DeliveryMethodDataCell
										.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;
									titleDetailsTable.addCell(DeliveryMethodNameCell) ;
									titleDetailsTable.addCell(DeliveryMethodDataCell) ;

									//Start: Req#1 Pick ticket Enhancement
									//Creating and displaying a Shipping method Cell only if the Delivery Method is Shipping
									if(sDeliveryMethod.equalsIgnoreCase("SHP") && !strIsOrderFiftyOne.equals("Y"))
									{
										PdfPCell ShippingMethodNameCell =
											new PdfPCell(new Phrase(YRCPlatformUI.getString("SHIPPING_METHOD"), fontHeaderDetailsBold)) ;
										ShippingMethodNameCell.setBorder(Rectangle.NO_BORDER);
										ShippingMethodNameCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT);
										Phrase pShippingMethodName = new Phrase(sShippingMethod.toUpperCase(), fontHeaderDetailsBoldRed);
										
										//Defect fix 232: Start
										
										Element eleCommonCodeList=(Element) eleSortedShipmentOutput.
																	getElementsByTagName("CommonCodeList").item(0);
										
										if (!YRCPlatformUI.isVoid(eleCommonCodeList))
										{
											NodeList nCommonCode = eleCommonCodeList.getElementsByTagName("CommonCode");
											if(nCommonCode.getLength()>0)
											{
												String strCarrierServiceBlack ="";
												for (int iCC=0; iCC<nCommonCode.getLength(); iCC++)
												{
													Element eleCommonCode =	(Element) nCommonCode.item(iCC) ;
													strCarrierServiceBlack=eleCommonCode.getAttribute("CodeValue");
													if(sShippingMethod.equals(strCarrierServiceBlack))
													{
														pShippingMethodName = new Phrase(sShippingMethod.toUpperCase(), fontHeaderDetailsBold);
													}
												}
											}
																				
										}
										//Defect fix 232: End

										PdfPCell ShippingMethodDataCell = new PdfPCell();
										ShippingMethodDataCell.setPhrase(pShippingMethodName);
										ShippingMethodDataCell.setBorder(Rectangle.NO_BORDER);
										ShippingMethodDataCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;
										titleDetailsTable.addCell(ShippingMethodNameCell) ;
										titleDetailsTable.addCell(ShippingMethodDataCell) ;
									}
									//End: Req#1 Pick ticket Enhancement


									documentDetailsTable.addCell(documentName);
									documentDetailsTable.addCell(BlankDataCell);
									documentDetailsTable.addCell(CustomerName);
									documentDetailsTable.addCell(titleDetailsTable);

									PdfPTable documentHeader = new PdfPTable(new float[]{1,1,1,1,1});
									documentHeader.setWidthPercentage(100);
									documentHeader.getDefaultCell().setBorder(0);
									documentHeader.addCell(BlankDataCell);
									documentHeader.addCell(BlankDataCell);
									documentHeader.addCell(documentDetailsTable);
									documentHeader.addCell(BlankDataCell);
									documentHeader.addCell(storeDetailsTable);

									document.add(documentHeader);


//									document.add(titleDetailsTable) ;
									document.add(Chunk.NEWLINE) ;

									//--End : Delivery Method Details Title Table
									//End SOM changes Req#3 May'14 Release */

									//Anchor Table
									PdfPTable anchorTable = new PdfPTable(2) ;
									anchorTable.setWidthPercentage(100) ;
									anchorTable.getDefaultCell().setBorder(0) ;

									//Add Current Date
									PdfPTable currentDateTable = new PdfPTable(2) ;
									currentDateTable.setWidthPercentage(50) ;
									currentDateTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;

									//Current Date Details
									PdfPCell CurrentDateCell =
										new PdfPCell(new Phrase(YRCPlatformUI.getString("CURRENT_DATE"),
											fontHeaderDetailsBold)) ;
									CurrentDateCell.setBorder(Rectangle.NO_BORDER) ;
									CurrentDateCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT);
									Date date = new Date() ;
									String sDate = date.toString() ;
									Phrase pCurrentDate = new Phrase(String.format(sDate), fontHeaderDetails) ;
									PdfPCell CurrentDateDataCell = new PdfPCell() ;
									CurrentDateDataCell.setPhrase(pCurrentDate) ;
									CurrentDateDataCell.setBorder(Rectangle.NO_BORDER) ;

									//Barcode for Order No
									code128.setCode(sOrderNo);
									Image imageBarCode =
										code128.createImageWithBarcode(writer.getDirectContent(), null, null) ;

									PdfPCell OrderNoDetailsCell =
										new PdfPCell(new Phrase(YRCPlatformUI.getString("ORDER_NO"),
											fontHeaderDetailsBold));
									OrderNoDetailsCell.setBorder(Rectangle.NO_BORDER);
									OrderNoDetailsCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT);
									PdfPCell OrderNoBarcodeCell = new PdfPCell(imageBarCode,false);
									OrderNoBarcodeCell.setBorder(Rectangle.NO_BORDER);

									//Barcode for Shipment No
									code128.setCode(sShipmentNo);
									Image imageShipmentNoBarCode =
										code128.createImageWithBarcode(writer.getDirectContent(), null, null) ;

									PdfPCell ShipmentNoDetailsCell =
										new PdfPCell(new Phrase(YRCPlatformUI.getString("SHIPMENT_NO"),
											fontHeaderDetailsBold)) ;
									ShipmentNoDetailsCell.setBorder(Rectangle.NO_BORDER);
									ShipmentNoDetailsCell
										.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;
									PdfPCell ShipmentNoBarcodeCell = new PdfPCell(imageShipmentNoBarCode,false) ;
									ShipmentNoBarcodeCell.setBorder(Rectangle.NO_BORDER) ;

									//Start: Req#8 Pick ticket Enhancement
									//Displaying the No of Days Since Order Placed Cell
									PdfPCell DaysSinceOrderDateDetailsCell =
										new PdfPCell(new Phrase(String.format(YRCPlatformUI.getString("DAYS_SINCE"),
											bFontHeaderDetailsBold))) ;
									DaysSinceOrderDateDetailsCell.setBorder(Rectangle.NO_BORDER);
									DaysSinceOrderDateDetailsCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT) ;

									//Start: Defect fix 104
									String strNoOfDays = HBCDateUtil.getDiffernceOfDateInDays(sOrderDate);
									strNoOfDays = YRCPlatformUI.getString("ORDER_PLACED") + " " + strNoOfDays+" "+YRCPlatformUI.getString("DAYS");
									//End: Defect fix 104

									Phrase pDaysSinceOrderDate = new Phrase(String.format(strNoOfDays, bFontHeaderDetailsBold));
									PdfPCell DaysSinceOrderDateDataCell = new PdfPCell() ;
									DaysSinceOrderDateDataCell.setPhrase(pDaysSinceOrderDate);
									DaysSinceOrderDateDataCell.setBorder(Rectangle.NO_BORDER) ;
									//End: Req#8 Pick ticket Enhancement

									//Empty Cell Details
									PdfPCell EmptyDataCell= new PdfPCell(new Phrase(" ", fontHeaderDetails)) ;
									EmptyDataCell.setBorder(Rectangle.NO_BORDER);
									Phrase pEmptyData = new Phrase();
									PdfPCell EmptyDateCellValue = new PdfPCell();
									EmptyDateCellValue.setPhrase(pEmptyData);
									EmptyDateCellValue.setBorder(Rectangle.NO_BORDER);

									//Add the cells to currentDateTable
									currentDateTable.addCell(CurrentDateCell);
									currentDateTable.addCell(CurrentDateDataCell);
									currentDateTable.addCell(EmptyDataCell);
									currentDateTable.addCell(EmptyDateCellValue);
									//Pick ticket Enhancement Start: Adding to display the No of Days Since OrderDate
									currentDateTable.addCell(DaysSinceOrderDateDetailsCell);
									currentDateTable.addCell(DaysSinceOrderDateDataCell);
									//Pick ticket Enhancement End: Adding to display the No of Days Since OrderDate
									currentDateTable.addCell(EmptyDataCell);
									currentDateTable.addCell(EmptyDateCellValue);
									currentDateTable.addCell(OrderNoDetailsCell);
									currentDateTable.addCell(OrderNoBarcodeCell);
									//Added to display Shipment No
									currentDateTable.addCell(EmptyDataCell);
									currentDateTable.addCell(EmptyDateCellValue);
									currentDateTable.addCell(ShipmentNoDetailsCell);
									currentDateTable.addCell(ShipmentNoBarcodeCell);


									//Check if Delivery Method is PICK
									if (sDeliveryMethod.equalsIgnoreCase("PICK"))
									{
										PdfPTable headerTable = new PdfPTable(2);
										headerTable.setWidthPercentage(50);
										headerTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);
										//Order No Details
										PdfPCell OrderNoCell =
											new PdfPCell(new Phrase(YRCPlatformUI.getString("ORDER_NO"),
												fontHeaderDetailsBold)) ;
										OrderNoCell.setBorder(Rectangle.NO_BORDER);
										Phrase pOrderNo = new Phrase(String.format(sOrderNo), fontHeaderDetails);
										PdfPCell OrderNoDataCell = new PdfPCell();
										OrderNoDataCell.setPhrase(pOrderNo);
										OrderNoDataCell.setBorder(Rectangle.NO_BORDER);

										//Shipment No Details
										PdfPCell ShipmentNoCell =
											new PdfPCell(new Phrase(YRCPlatformUI.getString("SHIPMENT_NO"),
												fontHeaderDetailsBold)) ;
										ShipmentNoCell.setBorder(Rectangle.NO_BORDER) ;
										Phrase pShipmentNo = new Phrase(String.format(sShipmentNo), fontHeaderDetails) ;
										PdfPCell ShipmentNoDataCell = new PdfPCell();
										ShipmentNoDataCell.setPhrase(pShipmentNo);
										ShipmentNoDataCell.setBorder(Rectangle.NO_BORDER);

										//Customer Name Details
										PdfPCell CustomerNameCell =
											new PdfPCell(new Phrase(YRCPlatformUI.getString("CUSTOMER_NAME"),
												fontHeaderDetailsBold)) ;
										CustomerNameCell.setBorder(Rectangle.NO_BORDER);
										sCustomerName = eleBillToAddress.getAttribute("FirstName") +" "+eleBillToAddress.getAttribute("LastName");
										Phrase pCustomerName =
											new Phrase(String.format(sCustomerName), fontHeaderDetails) ;
										PdfPCell CustomerNameDataCell = new PdfPCell() ;
										CustomerNameDataCell.setPhrase(pCustomerName) ;
										CustomerNameDataCell.setBorder(Rectangle.NO_BORDER) ;

										//Ship Date Details
										PdfPCell ShipDateCell=
											new PdfPCell(new Phrase(YRCPlatformUI.getString("STORE_NOTIFIED_ON"),
												fontHeaderDetailsBold)) ;
										ShipDateCell.setBorder(Rectangle.NO_BORDER);
										Phrase pShipDate =
											new Phrase(String.format(sShipDate.substring(0, 10)), fontHeaderDetails) ;
										PdfPCell ShipDateDataCell = new PdfPCell();
										ShipDateDataCell.setPhrase(pShipDate);
										ShipDateDataCell.setBorder(Rectangle.NO_BORDER);

										//Pickup Date Details
										PdfPCell PickUpDateCell =
											new PdfPCell(new Phrase(YRCPlatformUI.getString("PICK_UP_DATE"),
												fontHeaderDetailsBold)) ;
										PickUpDateCell.setBorder(Rectangle.NO_BORDER);

										//Start SOM changes Req#2 May'14 Release */
										//Earlier order date was getting displayed as pickup date,
										//The same is replaced with date which is calculated above  as part of this requirement
										Phrase pPickUpDate =
											new Phrase(String.format(sPickUpDate.substring(0, 10)), fontHeaderDetails) ;
										//End SOM changes Req#2 May'14 Release */

										PdfPCell PickupDateDataCell = new PdfPCell();
										PickupDateDataCell.setPhrase(pPickUpDate);
										PickupDateDataCell.setBorder(Rectangle.NO_BORDER);

										//Start SOM changes Req#16 May'14 Release */
										//Start: Added cell for "Phone No"
										// Start : 05/15/2014 : Defect# 188 converting phone# to phone# format
										sBillToDayPhone = sBillToDayPhone.replaceFirst("^(\\d{3})(\\d{3})(\\d{4})$", "$1-$2-$3");  
										// End : 05/15/2014 : Defect# 188 converting phone# to phone# format
										//Day Phone Details
										PdfPCell DayPhoneCell= new PdfPCell(new Phrase(YRCPlatformUI.getString("PHONE_NUMBER"), fontHeaderDetailsBold));
										DayPhoneCell.setBorder(Rectangle.NO_BORDER);
										Phrase pDayPhone = new Phrase(String.format(sBillToDayPhone), fontHeaderDetails);
										PdfPCell DayPhoneDataCell = new PdfPCell();
										DayPhoneDataCell.setPhrase(pDayPhone);
										DayPhoneDataCell.setBorder(Rectangle.NO_BORDER);
										//End: Added cell for "Phone No"
										//End SOM changes Req#16 May'14 Release */

										//Order Date Details
										PdfPCell OrderDateCell=
											new PdfPCell(new Phrase(YRCPlatformUI.getString("ORDER_DATE"),
												fontHeaderDetailsBold)) ;
										OrderDateCell.setBorder(Rectangle.NO_BORDER);
										Phrase pOrderDate =
											new Phrase(String.format(sOrderDate.substring(0, 10)), fontHeaderDetails) ;
										PdfPCell OrderDateDataCell = new PdfPCell();
										OrderDateDataCell.setPhrase(pOrderDate);
										OrderDateDataCell.setBorder(Rectangle.NO_BORDER);

										headerTable.addCell(OrderNoCell);
										headerTable.addCell(OrderNoDataCell);
										headerTable.addCell(ShipmentNoCell);
										headerTable.addCell(ShipmentNoDataCell);
										//Defect fix 172: start
//										headerTable.addCell(CustomerNameCell);
//										headerTable.addCell(CustomerNameDataCell);

										//Start SOM changes Req#16 May'14 Release */
//										headerTable.addCell(DayPhoneCell);//Inserted Cell for Phone No
//										headerTable.addCell(DayPhoneDataCell);// Inserted Cell for Phone No
										//End SOM changes Req#16 May'14 Release */

//										headerTable.addCell(ShipDateCell);
//										headerTable.addCell(ShipDateDataCell);
										//Defect fix 172: End

										headerTable.addCell(PickUpDateCell);
										headerTable.addCell(PickupDateDataCell);
										headerTable.addCell(OrderDateCell);
										headerTable.addCell(OrderDateDataCell);

										//Add tables to Anchor Table
										anchorTable.addCell(headerTable);
										anchorTable.addCell(currentDateTable);

										//Add Anchor Table to the document
										document.add(anchorTable);
										document.add(Chunk.NEWLINE);
									}

									//Check if Delivery Method is SHP
									if (sDeliveryMethod.equalsIgnoreCase("SHP"))
									{
										//Ship To Details And Instructions Anchor Table
										PdfPTable shipInstrAnchorTable = new PdfPTable(2);
										shipInstrAnchorTable.setWidthPercentage(100);
										shipInstrAnchorTable.getDefaultCell().setBorder(0);

										PdfPTable headerTable = new PdfPTable(2);
										headerTable.setWidthPercentage(50);
										headerTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;

										//Order No Details
										PdfPCell OrderNoCell =
											new PdfPCell(new Phrase(YRCPlatformUI.getString("ORDER_NO"),
												fontHeaderDetailsBold)) ;
										OrderNoCell.setBorder(Rectangle.NO_BORDER);
										Phrase pOrderNo = new Phrase(String.format(sOrderNo), fontHeaderDetails) ;
										PdfPCell OrderNoDataCell = new PdfPCell() ;
										OrderNoDataCell.setPhrase(pOrderNo) ;
										OrderNoDataCell.setBorder(Rectangle.NO_BORDER) ;

										//Shipment No Details
										PdfPCell ShipmentNoCell =
											new PdfPCell(new Phrase(YRCPlatformUI.getString("SHIPMENT_NO"),
												fontHeaderDetailsBold)) ;
										ShipmentNoCell.setBorder(Rectangle.NO_BORDER);
										Phrase pShipmentNo = new Phrase(String.format(sShipmentNo), fontHeaderDetails);
										PdfPCell ShipmentNoDataCell = new PdfPCell();
										ShipmentNoDataCell.setPhrase(pShipmentNo);
										ShipmentNoDataCell.setBorder(Rectangle.NO_BORDER);

										//Expected Ship Date Details
										PdfPCell ExpDeliveryDateCell =
											new PdfPCell(new Phrase(YRCPlatformUI.getString("EXPECTED_SHIP_DATE"),
												fontHeaderDetailsBold)) ;
										ExpDeliveryDateCell.setBorder(Rectangle.NO_BORDER);
										Phrase pExpDelDate =
											new Phrase(String.format(sExpectedDeliveryDate.substring(0, 10)),
												fontHeaderDetails) ;
										PdfPCell ExpDeliveryDateDataCell = new PdfPCell();
										ExpDeliveryDateDataCell.setPhrase(pExpDelDate);
										ExpDeliveryDateDataCell.setBorder(Rectangle.NO_BORDER);

										//Order Date Details
										PdfPCell OrderDateCell =
											new PdfPCell(new Phrase(YRCPlatformUI.getString("ORDER_DATE"),
												fontHeaderDetailsBold)) ;
										OrderDateCell.setBorder(Rectangle.NO_BORDER);
										Phrase pOrderDate =
											new Phrase(String.format(sOrderDate.substring(0, 10)), fontHeaderDetails) ;
										PdfPCell OrderDateDataCell = new PdfPCell();
										OrderDateDataCell.setPhrase(pOrderDate);
										OrderDateDataCell.setBorder(Rectangle.NO_BORDER);

										headerTable.addCell(OrderNoCell) ;
										headerTable.addCell(OrderNoDataCell) ;
										headerTable.addCell(ShipmentNoCell) ;
										headerTable.addCell(ShipmentNoDataCell) ;
										headerTable.addCell(ExpDeliveryDateCell) ;
										headerTable.addCell(ExpDeliveryDateDataCell) ;
										headerTable.addCell(OrderDateCell) ;
										headerTable.addCell(OrderDateDataCell) ;
//										headerTable.addCell(EmptyDataCell) ;
	//									headerTable.addCell(EmptyDateCellValue) ;

										//Table for Instructions
										PdfPTable instructionsTable = new PdfPTable(1) ;
										instructionsTable.setWidthPercentage(50) ;
										instructionsTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT) ;
										org.w3c.dom.Element eleInstructions =
											YRCXmlUtils.getXPathElement(eleShipment, "Shipment/Instructions") ;
										if (eleInstructions != null && !(eleInstructions.equals("")))
										{
											NodeList nInstruction = eleInstructions.getElementsByTagName("Instruction");
											if (1 <= nInstruction.getLength())
											{
												PdfPCell InstructionsCell =
													new PdfPCell(new Phrase("Instructions", fontHeaderDetailsBold)) ;
												InstructionsCell.setBackgroundColor(Color.LIGHT_GRAY) ;
												instructionsTable.addCell(InstructionsCell) ;
												for (int r=0; r<nInstruction.getLength(); r++)
												{
													org.w3c.dom.Element eleInstruction =
														(org.w3c.dom.Element) nInstruction.item(r) ;
													if (eleInstruction != null && !(eleInstruction.equals("")))
													{
														String sInstructionText =
															eleInstruction.getAttribute("InstructionText") ;
														Phrase pInstructionText =
															new Phrase(String.format(sInstructionText),
																fontHeaderDetails) ;
														PdfPCell InstructionCell= new PdfPCell() ;
														InstructionCell.setPhrase(pInstructionText) ;
														InstructionCell.setBorder(Rectangle.NO_BORDER) ;
														instructionsTable.addCell(InstructionCell) ;
													}
												}
											}
										}

										//Ship To Details Table
										PdfPTable shipToTable = new PdfPTable(1);
										shipToTable.setWidthPercentage(50);
										shipToTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);

										PdfPCell ShipToCell=
											new PdfPCell(new Phrase(YRCPlatformUI.getString("SHIP_TO"),
												fontHeaderDetailsBold)) ;
										//ShipToCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
										ShipToCell.setBackgroundColor(Color.LIGHT_GRAY);

										//Customer Name Details
										Phrase pShipTo = new Phrase(String.format(sCustomerName), fontHeaderDetails) ;
										PdfPCell ShipToDataCell = new PdfPCell() ;
										ShipToDataCell.setPhrase(pShipTo) ;
										ShipToDataCell.setBorder(Rectangle.NO_BORDER) ;

										//Company Details
										Phrase pCompany = new Phrase(String.format(sCompany), fontHeaderDetails) ;
										PdfPCell CompanyDataCell = new PdfPCell();
										CompanyDataCell.setPhrase(pCompany);
										CompanyDataCell.setBorder(Rectangle.NO_BORDER);

										//AddressLine1 Details
										Phrase pAddressLine1 =
											new Phrase(String.format(sAddressLine1), fontHeaderDetails) ;
										PdfPCell AddressLine1DataCell = new PdfPCell();
										AddressLine1DataCell.setPhrase(pAddressLine1);
										AddressLine1DataCell.setBorder(Rectangle.NO_BORDER);

										//AddressLine2 Details
										Phrase pAddressLine2 =
											new Phrase(String.format(sAddressLine2), fontHeaderDetails) ;
										PdfPCell AddressLine2DataCell = new PdfPCell();
										AddressLine2DataCell.setPhrase(pAddressLine2);
										AddressLine2DataCell.setBorder(Rectangle.NO_BORDER);

										//FormattedCityStateZipCodeStore Details
										Phrase pCityStateZipCodeStore =
											new Phrase(String.format(sFormattedCityStateZipCodeStore),
												fontHeaderDetails) ;
										PdfPCell CityStateZipCodeStoreDataCell = new PdfPCell() ;
										CityStateZipCodeStoreDataCell.setPhrase(pCityStateZipCodeStore) ;
										CityStateZipCodeStoreDataCell.setBorder(Rectangle.NO_BORDER) ;

										//CountryDetails
										Phrase pCountry = new Phrase(String.format(sCountry), fontHeaderDetails);
										PdfPCell CountryDataCell = new PdfPCell();
										CountryDataCell.setPhrase(pCountry);
										CountryDataCell.setBorder(Rectangle.NO_BORDER);

										//Day Phone Details
										// Start : 05/15/2014 : Defect# 188 converting phone# to phone# format
										sDayPhone = sDayPhone.replaceFirst("^(\\d{3})(\\d{3})(\\d{4})$", "$1-$2-$3");  
										// End : 05/15/2014 : Defect# 188 converting phone# to phone# format
										Phrase pDayPhone = new Phrase(String.format(sDayPhone), fontHeaderDetails);
										PdfPCell DayPhoneDataCell = new PdfPCell();
										DayPhoneDataCell.setPhrase(pDayPhone);
										DayPhoneDataCell.setBorder(Rectangle.NO_BORDER);

										//EmailID Details
										Phrase pEmailID = new Phrase(String.format(sEmailID), fontHeaderDetails);
										PdfPCell EmailIDDataCell = new PdfPCell();
										EmailIDDataCell.setPhrase(pEmailID);
										EmailIDDataCell.setBorder(Rectangle.NO_BORDER);

										shipToTable.addCell(ShipToCell);
										shipToTable.addCell(ShipToDataCell);
										shipToTable.addCell(CompanyDataCell);
										shipToTable.addCell(AddressLine1DataCell);
										//Defect fix 145: Start
										if(!sAddressLine2.equals("") && sAddressLine2!=null)
										{
											shipToTable.addCell(AddressLine2DataCell);
										}
										//Defect fix 145: End
										shipToTable.addCell(CityStateZipCodeStoreDataCell);
										shipToTable.addCell(CountryDataCell);
										shipToTable.addCell(DayPhoneDataCell);
										shipToTable.addCell(EmailIDDataCell);
										//shipToTable.addCell(EmptyDataCell);

										//Add Tables to Anchor Table
										anchorTable.addCell(headerTable);
										anchorTable.addCell(currentDateTable);

										//Add Anchor Table to the document
										document.add(anchorTable);

										//Add Tables to Ship To Details And Instructions Anchor Table Table
										shipInstrAnchorTable.addCell(shipToTable);
										shipInstrAnchorTable.addCell(instructionsTable);

										//Add Ship To Details And Instructions Anchor Table to the document
										document.add(shipInstrAnchorTable);
										document.add(Chunk.NEWLINE);
									}

									//Start: Req#5 Pick ticket Enhancement
									//Displaying the BillTo and Secondary Shopper Details for Pickup Orders
									else
									{
										PdfPTable billInstrAnchorTable = new PdfPTable(2);
										billInstrAnchorTable.setWidthPercentage(50);
										billInstrAnchorTable.getDefaultCell().setBorder(0);
										billInstrAnchorTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);

										//Bill To Details Table
										PdfPTable billToTable = new PdfPTable(1);
										billToTable.setWidthPercentage(25);
										billToTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);

										PdfPCell BillToCell=
											new PdfPCell(new Phrase(YRCPlatformUI.getString("BILL_TO"),
												fontHeaderDetailsBold)) ;
										BillToCell.setBackgroundColor(Color.LIGHT_GRAY);
										PdfPCell ShipToCell=
											new PdfPCell(new Phrase(YRCPlatformUI.getString("ALTER_SHOPPER"),
													fontHeaderDetailsBold)) ;
										ShipToCell.setBackgroundColor(Color.LIGHT_GRAY);

										//Customer Name Details
										String strBillToName = eleBillToAddress.getAttribute("FirstName") +" "+eleBillToAddress.getAttribute("LastName");
										Phrase pShipTo = new Phrase(String.format(strBillToName), fontHeaderDetails) ;
										PdfPCell BillToDataCell = new PdfPCell() ;
										BillToDataCell.setPhrase(pShipTo) ;
										BillToDataCell.setBorder(Rectangle.NO_BORDER) ;

										//AddressLine1 Details
										Phrase pAddressLine1 =
											new Phrase(String.format(eleBillToAddress.getAttribute("AddressLine1")), fontHeaderDetails) ;
										PdfPCell AddressLine1DataCell = new PdfPCell();
										AddressLine1DataCell.setPhrase(pAddressLine1);
										AddressLine1DataCell.setBorder(Rectangle.NO_BORDER);

										//AddressLine2 Details
										Phrase pAddressLine2 =
											new Phrase(String.format(eleBillToAddress.getAttribute("AddressLine2")), fontHeaderDetails) ;
										PdfPCell AddressLine2DataCell = new PdfPCell();
										AddressLine2DataCell.setPhrase(pAddressLine2);
										AddressLine2DataCell.setBorder(Rectangle.NO_BORDER);

										//FormattedCityStateZipCodeStore Details
										String strBillToCityStateZipCode = eleBillToAddress.getAttribute("City") +" "
																		+eleBillToAddress.getAttribute("State")
																		+" "+eleBillToAddress.getAttribute("ZipCode");
										Phrase pCityStateZipCodeStore =
											new Phrase(String.format(strBillToCityStateZipCode),
												fontHeaderDetails) ;
										PdfPCell CityStateZipCodeStoreDataCell = new PdfPCell() ;
										CityStateZipCodeStoreDataCell.setPhrase(pCityStateZipCodeStore) ;
										CityStateZipCodeStoreDataCell.setBorder(Rectangle.NO_BORDER) ;

										//CountryDetails
										String strBillToCountrZipCode = eleBillToAddress.getAttribute("Country");
										Phrase pCountry = new Phrase(String.format(strBillToCountrZipCode), fontHeaderDetails);
										PdfPCell CountryDataCell = new PdfPCell();
										CountryDataCell.setPhrase(pCountry);
										CountryDataCell.setBorder(Rectangle.NO_BORDER);

										String strBillToDayPhone = eleBillToAddress.getAttribute("DayPhone");
										// Start : 05/15/2014 : Defect# 188 converting phone# to phone# format
										strBillToDayPhone = strBillToDayPhone.replaceFirst("^(\\d{3})(\\d{3})(\\d{4})$", "$1-$2-$3");  
										// End : 05/15/2014 : Defect# 188 converting phone# to phone# format
										//Day Phone Details
										Phrase pDayPhone = new Phrase(String.format(strBillToDayPhone), fontHeaderDetails);
										PdfPCell DayPhoneDataCell = new PdfPCell();
										DayPhoneDataCell.setPhrase(pDayPhone);
										DayPhoneDataCell.setBorder(Rectangle.NO_BORDER);

										billToTable.addCell(BillToCell);
										billToTable.addCell(BillToDataCell);
										billToTable.addCell(AddressLine1DataCell);
										//Defect fix 145: Start
										if(!eleBillToAddress.getAttribute("AddressLine2").equals("") &&
												eleBillToAddress.getAttribute("AddressLine2")!=null)
										{
											billToTable.addCell(AddressLine2DataCell);
										}
										//Defect fix 145: End
										billToTable.addCell(CityStateZipCodeStoreDataCell);
										billToTable.addCell(CountryDataCell);
										billToTable.addCell(DayPhoneDataCell);
										//shipToTable.addCell(EmptyDataCell);

										billInstrAnchorTable.addCell(billToTable);
										//Adding the Alternate shopper details only if the secondary shopper present
										PdfPTable AlterShopperTable = new PdfPTable(1);
										AlterShopperTable.setWidthPercentage(25);
										AlterShopperTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);

										PdfPCell AlterShopperCell=
											new PdfPCell(new Phrase(YRCPlatformUI.getString("ALTER_SHOPPER"),
												fontHeaderDetailsBold)) ;
										AlterShopperCell.setBackgroundColor(Color.LIGHT_GRAY);
										AlterShopperTable.addCell(AlterShopperCell);
										Element eleNotes= XMLUtil.getChildElement(eleOrder, "Notes");
										if(eleNotes!=null)
										{
											NodeList nNote = eleNotes.getElementsByTagName("Note");
											String strFirstName="";
											String strLastName="";
											String strDayPhoneNo="";
											String strEMailID="";
											for(int iNts=0; iNts<nNote.getLength(); iNts++)
											{
												org.w3c.dom.Element eleNote = (org.w3c.dom.Element) nNote.item(iNts) ;
												String strContactType = eleNote.getAttribute("ContactType");
												if(strContactType!=null && strContactType.equals("FIRST_NAME"))
												{
													strFirstName = eleNote.getAttribute("NoteText");
												}
												else if(strContactType!=null && strContactType.equals("LAST_NAME"))
												{
													strLastName = eleNote.getAttribute("NoteText");
												}
												else if(strContactType!=null && strContactType.equals("PHONE"))
												{
													strDayPhoneNo=eleNote.getAttribute("NoteText");
												}
												else if (strContactType!=null && strContactType.equals("PHONE"))
												{
													strEMailID=eleNote.getAttribute("NoteText");
												}
											}
											//Customer Name Details
											String strSecondaryShopperName = strFirstName +" "+strLastName;
											Phrase pSecondaryShopperName = new Phrase(String.format(strSecondaryShopperName), fontHeaderDetails) ;
											PdfPCell SecondaryShopperNameCell = new PdfPCell() ;
											SecondaryShopperNameCell.setPhrase(pSecondaryShopperName) ;
											SecondaryShopperNameCell.setBorder(Rectangle.NO_BORDER) ;

											//Day Phone Details
											// Start : 05/15/2014 : Defect# 188 converting phone# to phone# format
											strDayPhoneNo = strDayPhoneNo.replaceFirst("^(\\d{3})(\\d{3})(\\d{4})$", "$1-$2-$3");  
											// End : 05/15/2014 : Defect# 188 converting phone# to phone# format
											Phrase pSecondaryShopperPhone = new Phrase(String.format(strDayPhoneNo), fontHeaderDetails);
											PdfPCell SecondaryShopperPhoneDataCell = new PdfPCell();
											SecondaryShopperPhoneDataCell.setPhrase(pSecondaryShopperPhone);
											SecondaryShopperPhoneDataCell.setBorder(Rectangle.NO_BORDER);

											//Shopper Email Details
											Phrase pSecondaryShopperEMailID = new Phrase(String.format(strEMailID), fontHeaderDetails);
											PdfPCell SecondaryShopperEMailIDDataCell = new PdfPCell();
											SecondaryShopperEMailIDDataCell.setPhrase(pSecondaryShopperEMailID);
											SecondaryShopperEMailIDDataCell.setBorder(Rectangle.NO_BORDER);

											//Displaying the secondary shopper details if present
											if(!strFirstName.equals("")&& !strFirstName.equals(eleBillToAddress.getAttribute("FirstName")))
											{
//												AlterShopperTable.addCell(AlterShopperCell);
												AlterShopperTable.addCell(SecondaryShopperNameCell);
												AlterShopperTable.addCell(SecondaryShopperPhoneDataCell);
												if(!strEMailID.equals("")&& !strEMailID.equals(eleBillToAddress.getAttribute("EMailID")))
												{
													AlterShopperTable.addCell(SecondaryShopperEMailIDDataCell);
												}
												AlterShopperTable.addCell(EmptyDataCell);
											}
											else
											{
												AlterShopperTable.addCell(EmptyDataCell);

											}

										}
										billInstrAnchorTable.addCell(AlterShopperTable);
										document.add(billInstrAnchorTable);
										document.add(Chunk.NEWLINE);
									}
									//End: Req#5 Pick ticket Enhancement

/*									PdfPTable table =
										new PdfPTable(new float[]{ 2, 5, 4, 8, 3, 3, 3, 3, 4, 3, 2, 2, 2, 2, 3}) ;
*/
									//Pick ticket Enhancement Start: Update the table configuration as per the new CR.
									PdfPTable tableheader =
										new PdfPTable(new float[]{ 2, 5, 4, 8, 4, 3, 2, 2, 3, 3, 2,2}) ;
									tableheader.setWidthPercentage(100f) ;
									//Pick ticket Enhancement End: Update the table configuration as per the new CR.

									//Sl.No Cell
									PdfPCell SLNoCell =
										new PdfPCell(new Phrase(YRCPlatformUI.getString("SL_NO"), fontCell)) ;
									SLNoCell.setBackgroundColor(Color.LIGHT_GRAY) ;
									SLNoCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

									//UPC Code Cell
									PdfPCell UPCCodeCell =
										new PdfPCell(new Phrase(YRCPlatformUI.getString("UPC_CODE"), fontCell)) ;
									UPCCodeCell.setBackgroundColor(Color.LIGHT_GRAY);
									UPCCodeCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

									//Item ID Cell
									//Defect fix 172: Start
//									PdfPCell ItemIDCell =
//										new PdfPCell(new Phrase(YRCPlatformUI.getString("ITEM_ID"), fontCell)) ;
									PdfPCell ItemIDCell =
										new PdfPCell(new Phrase(YRCPlatformUI.getString("SKN"), fontCell)) ;
									//Defect fix 172: End
									
									ItemIDCell.setBackgroundColor(Color.LIGHT_GRAY);
									ItemIDCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

									//Item Description Cell
									PdfPCell ItemDescCell =
										new PdfPCell(new Phrase(YRCPlatformUI.getString("ITEM_DESCRIPTION"), fontCell)) ;
									ItemDescCell.setBackgroundColor(Color.LIGHT_GRAY);
									ItemDescCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

									//Original Quantity Cell
									PdfPCell OrgQtyCell =
										new PdfPCell(new Phrase(YRCPlatformUI.getString("ORIGINAL_QUANTITY"), fontCell)) ;
									OrgQtyCell.setBackgroundColor(Color.LIGHT_GRAY);
									OrgQtyCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
									//UOM Cell
									PdfPCell UOMCell= new PdfPCell(new Phrase(YRCPlatformUI.getString("UOM"), fontCell));
									UOMCell.setBackgroundColor(Color.LIGHT_GRAY);
									UOMCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
									//Quantity To Pick Cell
									PdfPCell PickQtyCell =
										new PdfPCell(new Phrase(YRCPlatformUI.getString("QUANTITY_TO_PICK"), fontCell)) ;
									PickQtyCell.setBackgroundColor(Color.LIGHT_GRAY);
									PickQtyCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

									//Picked Quantity Cell
									PdfPCell PickedQtyCell =
										new PdfPCell(new Phrase(YRCPlatformUI.getString("PICKED_QUANTITY"), fontCell)) ;
									PickedQtyCell.setBackgroundColor(Color.LIGHT_GRAY);
									PickedQtyCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

									//Department Cell
									PdfPCell DeptCell =
										new PdfPCell(new Phrase(YRCPlatformUI.getString("DEPARTMENT"), fontCell));
									DeptCell.setBackgroundColor(Color.LIGHT_GRAY);
									DeptCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

									//Division Cell
									PdfPCell DivisionCell=
										new PdfPCell(new Phrase(YRCPlatformUI.getString("DIVISION"), fontCell)) ;
									DivisionCell.setBackgroundColor(Color.LIGHT_GRAY);
									DivisionCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

									//Gift Box Cell
									PdfPCell GiftBoxCell=
										new PdfPCell(new Phrase(YRCPlatformUI.getString("GIFT_BOX"), fontCell)) ;
									GiftBoxCell.setBackgroundColor(Color.LIGHT_GRAY);
									GiftBoxCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

									//Gift Wrap Cell
									PdfPCell GiftWrapCell=
										new PdfPCell(new Phrase(YRCPlatformUI.getString("GIFT_WRAP"), fontCell)) ;
									GiftWrapCell.setBackgroundColor(Color.LIGHT_GRAY);
									GiftWrapCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

									//Color Cell
									PdfPCell ColorCell=
										new PdfPCell(new Phrase(YRCPlatformUI.getString("COLOR"), fontCell)) ;
									ColorCell.setBackgroundColor(Color.LIGHT_GRAY);
									ColorCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

									//Size Cell
									PdfPCell SizeCell=
										new PdfPCell(new Phrase(YRCPlatformUI.getString("SIZE"), fontCell)) ;
									SizeCell.setBackgroundColor(Color.LIGHT_GRAY);
									SizeCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

									//Price Cell
									PdfPCell PriceCell=
										new PdfPCell(new Phrase(YRCPlatformUI.getString("PRICE"), fontCell)) ;
									PriceCell.setBackgroundColor(Color.LIGHT_GRAY);
									PriceCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

									//Start: Req#10 Pick ticket Enhancement
									//Onhand Inventory Cell
									PdfPCell OnHandInventoryCell=
										new PdfPCell(new Phrase(YRCPlatformUI.getString("ON_HAND"), fontCell)) ;
									OnHandInventoryCell.setBackgroundColor(Color.LIGHT_GRAY);
									OnHandInventoryCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
									//Start: Req#10 Pick ticket Enhancement

									//Pick ticket Enhancement Start: Changing the Order Of the Columns.
/*									//Add the cells to the table
									table.addCell(SLNoCell);
									table.addCell(UPCCodeCell);
									table.addCell(ItemIDCell);
									table.addCell(ItemDescCell);
									table.addCell(OrgQtyCell);
									table.addCell(UOMCell);
									table.addCell(PickQtyCell);
									table.addCell(PickedQtyCell);
									table.addCell(DeptCell);
									table.addCell(DivisionCell);
									table.addCell(GiftBoxCell);
									table.addCell(GiftWrapCell);
									table.addCell(ColorCell);
									table.addCell(SizeCell);
									table.addCell(PriceCell);
*/
									tableheader.addCell(SLNoCell);
									tableheader.addCell(UPCCodeCell);
									tableheader.addCell(ItemIDCell);
									tableheader.addCell(ItemDescCell);
									tableheader.addCell(DivisionCell);
									tableheader.addCell(DeptCell);
									tableheader.addCell(ColorCell);
									tableheader.addCell(SizeCell);
									tableheader.addCell(PriceCell);
									tableheader.addCell(PickQtyCell);
									tableheader.addCell(GiftBoxCell);
									tableheader.addCell(OnHandInventoryCell);
									//Pick ticket Enhancement End: Changing the Order Of the Columns.

									document.add(tableheader);
									org.w3c.dom.Element eleShipmentLines =
										YRCXmlUtils.getXPathElement(eleShipment, "Shipment/ShipmentLines") ;
									if (!YRCPlatformUI.isVoid(eleShipmentLines))
									{
										int noOfShipmentLinesperPage=3;
										NodeList nShipmentLines =
											eleShipmentLines.getElementsByTagName("ShipmentLine");

										//Start: Req#10 Pick ticket Enhancement
										//Making a getAvailableInventory call for the displaying the Onhand Inventory
//										prepareInputForGetAvailableInv(eleShipment);
/*										 Document docGetAvailableInventory = YRCXmlUtils.createDocument("Promise");
										 Element elePromise = docGetAvailableInventory.getDocumentElement();
										 elePromise.setAttribute("OrganizationCode",YRCXmlUtils.getAttribute(eleShipment, "EnterpriseCode"));
										 Element elePromiseLines = XMLUtil.createChild(elePromise, "PromiseLines");

											for (int k=0; k<nShipmentLines.getLength(); k++)
											{
												org.w3c.dom.Element eleShipmentLine =
													(org.w3c.dom.Element) nShipmentLines.item(k) ;
												Element elePromiseLine = XMLUtil.createChild(elePromiseLines, "PromiseLine");
												elePromiseLine.setAttribute("ItemID",YRCXmlUtils.getAttribute(eleShipmentLine, "ItemID"));
												elePromiseLine.setAttribute("UnitOfMeasure",YRCXmlUtils.getAttribute(eleShipmentLine, "UnitOfMeasure"));
												elePromiseLine.setAttribute("ShipNode",YRCXmlUtils.getAttribute(eleShipment, "ShipNode"));
												elePromiseLine.setAttribute("LineId",Integer.toString(k));

											}

										YRCApiContext context = new YRCApiContext();
							            context.setApiName("HBCGetAvailableInventoryForPrinting");
							            context.setFormId(this.getFormId());
							            context.setInputXml(docGetAvailableInventory);
							            YRCApiCaller apiCaller1 = new YRCApiCaller(context, true);
							            apiCaller1.invokeApi();

							            Document docGetAvailableInvOut = context.getOutputXml();
										Element elePromiseOut = docGetAvailableInvOut.getDocumentElement();
										NodeList nPromiseLine= elePromiseOut.getElementsByTagName("PromiseLine");
										//End: Req#10 Pick ticket Enhancement

*/										
										//Defect fix 239: Start(Displaying Onhand Inventory instead of Available Inventory
										
										 Document docMultiApi = YRCXmlUtils.createDocument("MultiApi");
										 Element eleMultiApi = docMultiApi.getDocumentElement();
											for (int k=0; k<nShipmentLines.getLength(); k++)
											{
												org.w3c.dom.Element eleShipmentLine =
													(org.w3c.dom.Element) nShipmentLines.item(k) ;
												Element eleAPI = XMLUtil.createChild(eleMultiApi, "API");
												eleAPI.setAttribute("Name","getInventorySupply");
												Element eleInput = XMLUtil.createChild(eleAPI, "Input");
												Element eleInventorySupply = XMLUtil.createChild(eleInput, "InventorySupply");
												eleInventorySupply.setAttribute("UnitOfMeasure",
														YRCXmlUtils.getAttribute(eleShipmentLine, "UnitOfMeasure"));
												eleInventorySupply.setAttribute("ItemID",
																YRCXmlUtils.getAttribute(eleShipmentLine, "ItemID"));
												eleInventorySupply.setAttribute("ShipNode",YRCXmlUtils.getAttribute(eleShipment, "ShipNode"));
												eleInventorySupply.setAttribute("OrganizationCode",
														YRCXmlUtils.getAttribute(eleShipment, "EnterpriseCode"));
												eleInventorySupply.setAttribute("ValidateShipNode","Y");
												eleInventorySupply.setAttribute("SupplyType","ONHAND");
											}

										YRCApiContext context = new YRCApiContext() ;
							            context.setApiName("HBCGetInventorySupply") ;
							            context.setFormId(this.getFormId()) ;
							            context.setInputXml(docMultiApi) ;
							            YRCApiCaller apiCaller1 = new YRCApiCaller(context, true) ;
							            apiCaller1.invokeApi() ;
							            
							            Document docGetInventorySupplyOut = context.getOutputXml() ;
										Element eleMultiAPI = docGetInventorySupplyOut.getDocumentElement() ;
										NodeList nItems= eleMultiAPI.getElementsByTagName("Item") ;
										//Defect fix 239: End (Displaying Onhand Inventory 
										//instead of Available Inventory
										
										for (int k=0; k<nShipmentLines.getLength(); k++)
										{
											//new PdfPTable(new float[]{ 2, 5, 4, 8, 4, 3, 2, 2, 3, 3, 2,2}) ;
											//Pick ticket Enhancement Start: Update the table configuration 
											//as per the new CR.
											PdfPTable table =
												new PdfPTable(new float[]{ 11, 8, 4, 3, 2, 2, 3, 3, 2,2}) ;
											table.setWidthPercentage(100f) ;
											//Pick ticket Enhancement End: Update the table configuration 
											//as per the new CR.
											
											org.w3c.dom.Element eleShipmentLine =
												(org.w3c.dom.Element) nShipmentLines.item(k) ;

											//SlNo Data Cell
											String sSlNo = String.valueOf(k+1) ;
											Phrase pSlNo = new Phrase(String.format(sSlNo), fontCell) ;
											PdfPCell SLNoDataCell = new PdfPCell() ;
											SLNoDataCell.setPhrase(pSlNo) ;

											//UPCCode Data Cell
											NodeList nOrderLines = eleShipmentLine.getElementsByTagName("OrderLine") ;
											String sUPCCode = "" ;
											String sExtnDepartment = "" ;
											String sExtnDivision = "" ;
											String sGiftFlag = "" ;
											String sGiftWrap = "" ;
											String sSize = "" ;
											String sColor = "" ;
										    String sPrice = "0.0" ;
											String sItemDescription = "" ;
											String sStatus = "" ;
											String strItemImageID= "" ;
											String strItemImageLocation= "" ;
											String sOnHand="" ;

											for (int l=0; l<nOrderLines.getLength(); l++)
											{
												org.w3c.dom.Element eleOrderLine =
													(org.w3c.dom.Element) nOrderLines.item(l) ;
												NodeList nItemAlias = eleOrderLine.getElementsByTagName("ItemAlias") ;
												for (int m=0; m<nItemAlias.getLength(); m++)
												{
													org.w3c.dom.Element eleItemAlias =
														(org.w3c.dom.Element) nItemAlias.item(m) ;
													if ("UPC".equalsIgnoreCase(eleItemAlias.getAttribute("AliasName")))
													{
														sUPCCode = eleItemAlias.getAttribute("AliasValue");
													}
												}
												org.w3c.dom.Element eleItemExtn =
													YRCXmlUtils.getXPathElement(eleOrderLine,
														"OrderLine/ItemDetails/Extn") ;

												if (!YRCPlatformUI.isVoid(eleItemExtn))
												{
													sExtnDepartment = eleItemExtn.getAttribute("ExtnDepartment") ;
													sExtnDivision = eleItemExtn.getAttribute("ExtnDivision") ;
												}
												
												//Modified to fetch item status
												org.w3c.dom.Element eleItemPrimaryInfo =
													YRCXmlUtils.getXPathElement(eleOrderLine,
														"OrderLine/ItemDetails/PrimaryInformation") ;
												if (!YRCPlatformUI.isVoid(eleItemPrimaryInfo))
												{
													sItemDescription = eleItemPrimaryInfo.getAttribute("Description") ;
													sStatus = eleItemPrimaryInfo.getAttribute("Status") ;
													//Pick ticket Enhancement Start: Changes to get the Item Image
													strItemImageLocation = eleItemPrimaryInfo.getAttribute("ImageLocation");
													strItemImageID=eleItemPrimaryInfo.getAttribute("ImageID");
													strItemImageLocation = strItemImageLocation+"/"+strItemImageID;
													//Pick ticket Enhancement End: Changes to get the Item Image
												}

												//Start: Req#9 Pick ticket Enhancement For GR Orders
												//Updating the Value of Gift Flag as Gift Registry if it is a GR Order 
												//and has Gift Box
												//Start: Defect R3.4 160,176 --If Order is GR and GiftWrap = "Y", 
												//set Gift column to Y
												sGiftWrap = eleOrderLine.getAttribute("GiftWrap") ;
												if(sGiftWrap.equalsIgnoreCase("Y")) 
												{
													if(strOrderType.equals("GR"))
													{
														sGiftWrap = "Gift Regst" ;
													}
												}
												else 
												{
													sGiftWrap="No" ;
												}
												//End: Defect R3.4 160.176 --If Order is GR and GiftWrap = "Y", 
												//set Gift column to Y
												//End: Req#9 Pick ticket Enhancement
									
												NodeList nAdditionalAttribute =
													eleOrderLine.getElementsByTagName("AdditionalAttribute") ;
												for (int n=0; n<nAdditionalAttribute.getLength(); n++)
												{
													org.w3c.dom.Element eleAdditionalAttribute =
														(org.w3c.dom.Element) nAdditionalAttribute.item(n) ;
													//Modified to display color and size values on the pick ticket
													//based on the item status
													for (int t=0; t<nXpathForItemAttributes.getLength(); t++)
													{
														org.w3c.dom.Element eleXpathForItemAttributes =
															(org.w3c.dom.Element) nXpathForItemAttributes.item(t) ;
														String sCommonCodeValue =
															eleXpathForItemAttributes.getAttribute("CodeValue") ;
														if (sStatus.equalsIgnoreCase(sCommonCodeValue))
														{
															if (eleXpathForItemAttributes
																.getAttribute("CodeShortDescription")
																	.equalsIgnoreCase(eleAdditionalAttribute
																		.getAttribute("Name")))
															{
																sColor = eleAdditionalAttribute.getAttribute("Value") ;
															}
															if (eleXpathForItemAttributes
																.getAttribute("CodeLongDescription")
																	.equalsIgnoreCase(eleAdditionalAttribute
																		.getAttribute("Name")))
															{
																sSize = eleAdditionalAttribute.getAttribute("Value") ;
															}
														}
													}
												}
												org.w3c.dom.Element eleLinePriceInfo =
													YRCXmlUtils.getXPathElement(eleOrderLine,
														"OrderLine/LinePriceInfo") ;
												if (!YRCPlatformUI.isVoid(eleLinePriceInfo))
												{
													sPrice = eleLinePriceInfo.getAttribute("UnitPrice") ;
												}
											}
											PdfPTable pTbSlUPCSKN = new PdfPTable(new float[]{ 2, 5, 4}) ;
											
											pTbSlUPCSKN.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
											
											PdfPCell pclSlNo = new PdfPCell() ;
											pclSlNo.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
											pclSlNo.setBorder(Rectangle.NO_BORDER);
											//SlNo Data Cell
											pclSlNo.setPhrase(pSlNo);
											pTbSlUPCSKN.addCell(pclSlNo);

											PdfPTable pTbUPCBarCode = new PdfPTable(1);
											pTbUPCBarCode.getDefaultCell().setBorder(Rectangle.NO_BORDER);
											pTbUPCBarCode.getDefaultCell().setBorderColor(Color.WHITE);
											PdfPCell pclUPCCode = new PdfPCell() ;
											pclUPCCode.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
											pclUPCCode.setBorder(Rectangle.NO_BORDER);
											PdfPCell pclUPCBarCode = new PdfPCell() ;
											pclUPCBarCode.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
											pclUPCBarCode.setBorder(Rectangle.NO_BORDER);
											if(!XMLUtil.isVoid(sUPCCode))
											{
												Phrase pUPCCode = new Phrase(String.format(sUPCCode), fontCell) ;
												pclUPCCode.setPhrase(pUPCCode) ;
												
												code128.setCode(sUPCCode) ;
												Image imageUPCCodeBarCode = code128.createImageWithBarcode
													(writer.getDirectContent(), null, null) ;
												pclUPCBarCode.setImage(imageUPCCodeBarCode);
											}
											pTbUPCBarCode.addCell(pclUPCCode);
											pTbUPCBarCode.addCell(pclUPCBarCode);
											
											PdfPCell pclTmpUPCBarCode = new PdfPCell() ;
											pclTmpUPCBarCode.addElement(pTbUPCBarCode);
											pclTmpUPCBarCode.setBorder(Rectangle.NO_BORDER);
											pTbSlUPCSKN.addCell(pclTmpUPCBarCode);
											
											PdfPTable pTbSKNIMG = new PdfPTable(1);
											pTbSKNIMG.getDefaultCell().setBorder(Rectangle.NO_BORDER);
											PdfPCell pclSKN = new PdfPCell() ;
											pclSKN.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
											pclSKN.setBorder(Rectangle.NO_BORDER);
											String sItemID = null;
											sItemID = eleShipmentLine.getAttribute("ItemID");
											Phrase pItemID = new Phrase(String.format(sItemID), fontCell);
											pclSKN.setPhrase(pItemID);
											
											pTbSKNIMG.addCell(pclSKN);
											
											PdfPCell pclSKNImage = new PdfPCell() ;
											pclSKNImage.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
											pclSKNImage.setBorder(Rectangle.NO_BORDER);
											try
											{
												URL urltesting =new URL(strItemImageLocation);
												Image imageItem=null;
												imageItem = Image.getInstance(urltesting);
												pclSKNImage
													.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;
												pclSKNImage.setImage(imageItem) ;
												pclSKNImage.setFixedHeight(80.0f) ;


											}
											catch(Exception e)
											{
												e.toString() ;
											}
											
											pTbSKNIMG.addCell(pclSKNImage);
											PdfPCell pclTmpSKNIMG = new PdfPCell() ;
											pclTmpSKNIMG.addElement(pTbSKNIMG);
											pclTmpSKNIMG.setBorder(Rectangle.NO_BORDER);
											
											pTbSlUPCSKN.addCell(pclTmpSKNIMG);

											//Item Desc Data Cell
											//Modified for Defect 991:Start
											Phrase pItemDesc = new Phrase(String.format(sItemDescription), fontCell);
											//Phrase pItemDesc = new Phrase(String.format(sItemDescription), fontCell);
											//Modified for Defect 991:End


											PdfPCell ItemDescDataCell = new PdfPCell();
											ItemDescDataCell.setPhrase(pItemDesc);

											//Original Quantity Data Cell
											String sQuantity = null;
											sQuantity = eleShipmentLine.getAttribute("Quantity");
											if (sQuantity.contains(".")){
												sQuantity = sQuantity.replaceAll("\\..*", "");
											}
											//Start of Fix for Defect 357, R 3.2 Feb Release
											if("0.0".equals(sQuantity) || "0".equals(sQuantity))
											{
												 continue;
											}
											//End of Fix for Defect 357, R 3.2 Feb Release

											Phrase pOrgQty = new Phrase(String.format(sQuantity), fontCell);
											PdfPCell OrgQtyDataCell = new PdfPCell();
											OrgQtyDataCell.setPhrase(pOrgQty);

											//UOM Data Cell
											String sUnitOfMeasure = null;
											sUnitOfMeasure = eleShipmentLine.getAttribute("UnitOfMeasure");
											Phrase pUOM = new Phrase(String.format(sUnitOfMeasure), fontCell);
											PdfPCell UOMDataCell = new PdfPCell();
											UOMDataCell.setPhrase(pUOM);

											//Quantity To Pick Data Cell
											String sBackroomPickedQuantity =
												eleShipmentLine.getAttribute("BackroomPickedQuantity") ;
											String sOriginalQty = eleShipmentLine.getAttribute("Quantity") ;
											double dBackRoomPickedQty = 0.00 ;
											double dOriginalQty = 0.00 ;
											double dToPickQty = 0.00 ;
											String spickedQty = "NULL" ;
											if ((!sBackroomPickedQuantity.equals(""))
												&& (!sBackroomPickedQuantity.equals(null)))
											{
												dBackRoomPickedQty = Double.parseDouble(sBackroomPickedQuantity);
												dOriginalQty = Double.parseDouble(sOriginalQty);
												dToPickQty = dOriginalQty - dBackRoomPickedQty;
												spickedQty = Double.toString(dToPickQty);
												if (spickedQty.contains("."))
												{
													spickedQty = spickedQty.replaceAll("\\..*", "") ;
												}
											}
											else
											{
												spickedQty = eleShipmentLine.getAttribute("Quantity") ;
												spickedQty = spickedQty.replaceAll("\\..*", "") ;
											}

											Phrase pPickQty = new Phrase(String.format(spickedQty), fontCell);
											PdfPCell PickQtyDataCell = new PdfPCell() ;
											PickQtyDataCell.setPhrase(pPickQty) ;

											//Picked Quantity Data Cell
											String sBackroomPickQty = "0" ;
											if ((!sBackroomPickedQuantity.equals(""))
												&& (!sBackroomPickedQuantity.equals(null)))
											{
												sBackroomPickQty = sBackroomPickedQuantity;
												sBackroomPickQty = sBackroomPickQty.replaceAll("\\..*", "") ;
											}

											//Start: Req#10 Pick ticket Enhancement
											//Retrieveing the Onhand Availability for each Item
/*											for(int iPL=0;iPL<nPromiseLine.getLength(); iPL++)
											{
												org.w3c.dom.Element elePromiseLineOut =
													(org.w3c.dom.Element) nPromiseLine.item(iPL);
												if(elePromiseLineOut.getAttribute("ItemID").equals(sItemID))
												{
													Element eleAvailability = XMLUtil.getChildElement(elePromiseLineOut, "Availability");
													Element eleAvailableInv = XMLUtil.getChildElement(eleAvailability, "AvailableInventory");
													sOnHand=eleAvailableInv.getAttribute("AvailableOnhandQuantity");
												}
											}
*/											//End: Req#10 Pick ticket Enhancement
											
											//Defect fix 239: Start(Displaying Onhand Inventory instead of Available Inventory
											for(int iPL=0;iPL<nItems.getLength(); iPL++)
											{
												org.w3c.dom.Element eleItemOut =
													(org.w3c.dom.Element) nItems.item(iPL);
												if(eleItemOut.getAttribute("ItemID").equals(sItemID))
												{
													Element eleSupplies = XMLUtil.getChildElement(eleItemOut, "Supplies");
													Element eleInventorySupply = XMLUtil.getChildElement(eleSupplies, "InventorySupply");
													if(eleInventorySupply != null)
													{
														sOnHand=eleInventorySupply.getAttribute("Quantity");
													}
												}
											}
											//Defect fix 239: Start(Displaying Onhand Inventory instead of Available Inventory

											Phrase pPickedQty = new Phrase(String.format(sBackroomPickQty), fontCell) ;
											PdfPCell PickedQtyDataCell = new PdfPCell() ;
											PickedQtyDataCell.setPhrase(pPickedQty) ;

											//Department Data Cell
											Phrase pDepartment = new Phrase(String.format(sExtnDepartment), fontCell) ;
											PdfPCell DeptDataCell = new PdfPCell() ;
											DeptDataCell.setPhrase(pDepartment) ;

											//Division Data Cell
											Phrase pDivision  = new Phrase(String.format(sExtnDivision), fontCell) ;
											PdfPCell DivDataCell = new PdfPCell() ;
											DivDataCell.setPhrase(pDivision) ;

											//GiftBox Data Cell
											Phrase pGiftBox = new Phrase(String.format(sGiftWrap), fontCell) ;
//											Phrase pGiftBox = new Phrase(String.format(sGiftFlag), fontCell) ;
											PdfPCell GiftBoxDataCell = new PdfPCell() ;
											GiftBoxDataCell.setPhrase(pGiftBox) ;

											//GiftWrap Data Cell
											Phrase pGiftWrap = new Phrase(String.format(sGiftWrap), fontCell) ;
											PdfPCell GiftWrapDataCell = new PdfPCell() ;
											GiftWrapDataCell.setPhrase(pGiftWrap) ;

											//Color Data Cell
											Phrase pColor = new Phrase(String.format(sColor), fontCell) ;
											PdfPCell ColorDataCell = new PdfPCell() ;
											ColorDataCell.setPhrase(pColor) ;

											//Size Data Cell
											Phrase pSize = new Phrase(String.format(sSize), fontCell) ;
											PdfPCell SizeDataCell = new PdfPCell() ;
											SizeDataCell.setPhrase(pSize) ;

											//Price Data Cell
											Phrase pPrice = new Phrase(String.format(sPrice), fontCell) ;
											PdfPCell PriceDataCell = new PdfPCell() ;
											PriceDataCell.setPhrase(pPrice) ;

											//Start: Req#10 Pick ticket Enhancement
											//On Hand Inventory Data Cell
											Phrase pOnHandInventory = new Phrase(String.format(sOnHand), fontCell) ;
											PdfPCell OnHandInventoryDataCell = new PdfPCell() ;
											OnHandInventoryDataCell.setPhrase(pOnHandInventory) ;
											//End: Req#10 Pick ticket Enhancement

											//Add Data cells to the table
											//Pick ticket Enhancement Start: Changing the Order of Columns in the table 
											//as per the new CR
/*											table.addCell(SLNoDataCell);
											table.addCell(itemUPCCode);
											table.addCell(itemDetails);
											table.addCell(ItemDescDataCell);
											table.addCell(OrgQtyDataCell);
											table.addCell(UOMDataCell);
											table.addCell(PickQtyDataCell);
											table.addCell(PickedQtyDataCell);
											table.addCell(DeptDataCell);
											table.addCell(DivDataCell);
											table.addCell(GiftBoxDataCell);
											table.addCell(GiftWrapDataCell);
											table.addCell(ColorDataCell);
											table.addCell(SizeDataCell);
											table.addCell(PriceDataCell);
*/
											//table.addCell(SLNoDataCell);
											//table.addCell(itemUPCCode); //UPCCodeDataCell);//
											table.addCell(pTbSlUPCSKN);
											table.addCell(ItemDescDataCell);
											table.addCell(DivDataCell);
											table.addCell(DeptDataCell);
											table.addCell(ColorDataCell);
											table.addCell(SizeDataCell);
											table.addCell(PriceDataCell);
											table.addCell(PickQtyDataCell);
											table.addCell(GiftBoxDataCell);
											table.addCell(OnHandInventoryDataCell);
											//Pick ticket Enhancement End: Changing the Order of Columns in the table 
											//as per the new CR

											if(noOfShipmentLinesperPage == Integer.valueOf(sSlNo))
											{
												document.setMargins(30f, 30f, 5f, 5f);
												document.newPage();
												noOfShipmentLinesperPage=noOfShipmentLinesperPage+5 ;
												currentPageNo++;
												addHeaderDetails(sOrderNo, sShipmentNo, document) ;
											}
											document.add(table);

										}
									}
								}
							}
						}
					}
				}

				document.close();
				if ("win32".equals(pltform))
				{
					Program.launch(sDocumentPathAndName);
				}
			}
		}
	}


	/*
	 *This method was added to include the header details
	 *for multiple pages incase the pick ticket is
	 *for more than 1 page
	 *
	 */
	int currentPageNo=1;
	public void addHeaderDetails( String sOrderNo, String sShipmentNo, com.lowagie.text.Document document1)
	{
	try
	 {
		BaseFont bfontCell =	BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, false) ;
		  Font fontCell = new Font(bfontCell, 7, Font.NORMAL, Color.BLACK) ;

		 BaseFont bFontHeaderDetails =
			 BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, false) ;
		 Font fontHeaderDetails =
			 new Font(bFontHeaderDetails, 9, Font.NORMAL, Color.BLACK) ;

		BaseFont bFontHeaderDetailsBold =
			 BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, false) ;
		 Font fontHeaderDetailsBold =
			 new Font(bFontHeaderDetailsBold, 9, Font.BOLD, Color.BLACK) ;

		//Order No Details
		 String strOrderNo = YRCPlatformUI.getString("ORDER_NO")+": ";
		 String strShipmentNo = YRCPlatformUI.getString("SHIPMENT_NO")+": ";
		 String strPageNo=YRCPlatformUI.getString("PAGE_NO")+": ";

		 String strHeaderDetails = strOrderNo+sOrderNo+" /"+strShipmentNo+
		 sShipmentNo+" /"+strPageNo+Integer.toString(currentPageNo);

		Phrase pHeaderDetails = new Phrase(String.format(strHeaderDetails), fontHeaderDetails);
		PdfPCell HeaderDetails = new PdfPCell();
		HeaderDetails.setPhrase(pHeaderDetails);
		HeaderDetails.setBorder(Rectangle.NO_BORDER);

		PdfPTable headerTable = new PdfPTable(1);
		headerTable.setWidthPercentage(50);
		headerTable.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);

		headerTable.addCell(HeaderDetails);

		document1.add(headerTable);

		//Empty Data Cell
		 PdfPCell EmptyDataCell =
			 new PdfPCell(new Phrase(" ", fontHeaderDetailsBold)) ;
		 EmptyDataCell.setBorder(Rectangle.NO_BORDER) ;

		PdfPTable emptytable = new PdfPTable(1) ;
		emptytable.addCell(EmptyDataCell);

		document1.add(emptytable);

		PdfPTable tableheader =
			new PdfPTable(new float[]{ 2, 5, 4, 8, 4, 3, 2, 2, 3, 3, 2,2}) ;
		tableheader.setWidthPercentage(100f) ;
		//Pick ticket Enhancement End: Update the table configuration as per the new CR.

		//Sl.No Cell
		PdfPCell SLNoCell =
			new PdfPCell(new Phrase(YRCPlatformUI.getString("SL_NO"), fontCell)) ;
		SLNoCell.setBackgroundColor(Color.LIGHT_GRAY) ;
		SLNoCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

		//UPC Code Cell
		PdfPCell UPCCodeCell =
			new PdfPCell(new Phrase(YRCPlatformUI.getString("UPC_CODE"), fontCell)) ;
		UPCCodeCell.setBackgroundColor(Color.LIGHT_GRAY);
		UPCCodeCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

		//Item ID Cell
		PdfPCell ItemIDCell =
			new PdfPCell(new Phrase(YRCPlatformUI.getString("ITEM_ID"), fontCell)) ;
		ItemIDCell.setBackgroundColor(Color.LIGHT_GRAY);
		ItemIDCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

		//Item Description Cell
		PdfPCell ItemDescCell =
			new PdfPCell(new Phrase(YRCPlatformUI.getString("ITEM_DESCRIPTION"), fontCell)) ;
		ItemDescCell.setBackgroundColor(Color.LIGHT_GRAY);
		ItemDescCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

		//Original Quantity Cell
		PdfPCell OrgQtyCell =
			new PdfPCell(new Phrase(YRCPlatformUI.getString("ORIGINAL_QUANTITY"), fontCell)) ;
		OrgQtyCell.setBackgroundColor(Color.LIGHT_GRAY);
		OrgQtyCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
		//UOM Cell
		PdfPCell UOMCell= new PdfPCell(new Phrase(YRCPlatformUI.getString("UOM"), fontCell));
		UOMCell.setBackgroundColor(Color.LIGHT_GRAY);
		UOMCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
		//Quantity To Pick Cell
		PdfPCell PickQtyCell =
			new PdfPCell(new Phrase(YRCPlatformUI.getString("QUANTITY_TO_PICK"), fontCell)) ;
		PickQtyCell.setBackgroundColor(Color.LIGHT_GRAY);
		PickQtyCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

		//Picked Quantity Cell
		PdfPCell PickedQtyCell =
			new PdfPCell(new Phrase(YRCPlatformUI.getString("PICKED_QUANTITY"), fontCell)) ;
		PickedQtyCell.setBackgroundColor(Color.LIGHT_GRAY);
		PickedQtyCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

		//Department Cell
		PdfPCell DeptCell =
			new PdfPCell(new Phrase(YRCPlatformUI.getString("DEPARTMENT"), fontCell));
		DeptCell.setBackgroundColor(Color.LIGHT_GRAY);
		DeptCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

		//Division Cell
		PdfPCell DivisionCell=
			new PdfPCell(new Phrase(YRCPlatformUI.getString("DIVISION"), fontCell)) ;
		DivisionCell.setBackgroundColor(Color.LIGHT_GRAY);
		DivisionCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

		//Gift Box Cell
		PdfPCell GiftBoxCell=
			new PdfPCell(new Phrase(YRCPlatformUI.getString("GIFT_BOX"), fontCell)) ;
		GiftBoxCell.setBackgroundColor(Color.LIGHT_GRAY);
		GiftBoxCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

		//Gift Wrap Cell
		PdfPCell GiftWrapCell=
			new PdfPCell(new Phrase(YRCPlatformUI.getString("GIFT_WRAP"), fontCell)) ;
		GiftWrapCell.setBackgroundColor(Color.LIGHT_GRAY);
		GiftWrapCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER) ;

		//Color Cell
		PdfPCell ColorCell=
			new PdfPCell(new Phrase(YRCPlatformUI.getString("COLOR"), fontCell)) ;
		ColorCell.setBackgroundColor(Color.LIGHT_GRAY);
		ColorCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

		//Size Cell
		PdfPCell SizeCell=
			new PdfPCell(new Phrase(YRCPlatformUI.getString("SIZE"), fontCell)) ;
		SizeCell.setBackgroundColor(Color.LIGHT_GRAY);
		SizeCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

		//Price Cell
		PdfPCell PriceCell=
			new PdfPCell(new Phrase(YRCPlatformUI.getString("PRICE"), fontCell)) ;
		PriceCell.setBackgroundColor(Color.LIGHT_GRAY);
		PriceCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);

		//Start: Req#10 Pick ticket Enhancement
		//Onhand Inventory Cell
		PdfPCell OnHandInventoryCell=
			new PdfPCell(new Phrase(YRCPlatformUI.getString("ON_HAND"), fontCell)) ;
		OnHandInventoryCell.setBackgroundColor(Color.LIGHT_GRAY);
		OnHandInventoryCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
		tableheader.addCell(SLNoCell);
		tableheader.addCell(UPCCodeCell);
		tableheader.addCell(ItemIDCell);
		tableheader.addCell(ItemDescCell);
		tableheader.addCell(DivisionCell);
		tableheader.addCell(DeptCell);
		tableheader.addCell(ColorCell);
		tableheader.addCell(SizeCell);
		tableheader.addCell(PriceCell);
		tableheader.addCell(PickQtyCell);
		tableheader.addCell(GiftBoxCell);
		tableheader.addCell(OnHandInventoryCell);

		document1.add(tableheader);
	 }
	 catch(Exception e)
	 {
		e.printStackTrace();
	 }
  }



 	/**
	 * This method is implemented to fetch the store associate id
	 */
	public String getUserID ()
	{
		org.w3c.dom.Element eleUserNameSpace = this.getModel("UserNameSpace");
		String sUserID = null;
		if (!YRCPlatformUI.isVoid(eleUserNameSpace)) {
			sUserID = eleUserNameSpace.getAttribute("Loginid");
		}
		return sUserID;
	 }

	 /**
	 * This method is implemented to fetch the ship node
	 */
	 public String getShipNode ()
	 {
			org.w3c.dom.Element eleUserNameSpace = this.getModel("UserNameSpace") ;
			String sShipNode = null ;
			if (!YRCPlatformUI.isVoid(eleUserNameSpace))
			{
				sShipNode = eleUserNameSpace.getAttribute("ShipNode") ;
			}
			return sShipNode;
	 }

	 /**
	 * This method is implemented to add title to the pick list
	 */
	private static Paragraph addTitle(com.lowagie.text.Document document) throws Exception {
		BaseFont bFontParagraph = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, false);
		Font fontParagraph = new Font(bFontParagraph, 15, Font.BOLD, Color.BLACK);
		Paragraph pName = new Paragraph(YRCPlatformUI.getString("PICK_TICKET"), fontParagraph);
		pName.setAlignment(com.lowagie.text.Element.ALIGN_CENTER);
//		document.add(pName);
//		document.add(Chunk.NEWLINE);
		return pName;
	}


	/**
	 * This method is implemented to add Customer Name to the pick list.
	 * Start SOM changes Req#3 May'14 Release
	 */
	private static Paragraph addTitleCustName(com.lowagie.text.Document document, String sCustName) throws Exception {
		BaseFont bFontParagraph = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, false);
		Font fontParagraph = new Font(bFontParagraph, 15, Font.BOLD, Color.BLACK);
		Paragraph pName = new Paragraph(sCustName, fontParagraph);
		pName.setAlignment(com.lowagie.text.Element.ALIGN_CENTER);
//		document.add(pName);

		return pName;
	}

	//@SuppressWarnings("static-access")
	public void postSetModel(String model)
	{

        //-- Kumar : start : GridData
		GridData objGridData = new GridData();
		objGridData.exclude = true;
		//-- Kumar : end : GridData

        if(model.equalsIgnoreCase("Shipment"))
        {
        	Element eleShipment = this.getModel("Shipment") ;
        	if (!YRCPlatformUI.isVoid(eleShipment))
        	{
        		/**Defect#23 :  Requested Ship Date Start***/
        		Element eleShipmentLines = XMLUtil.getChildElement(eleShipment, "ShipmentLines");
        		NodeList nShipmentLine = eleShipmentLines.getElementsByTagName("ShipmentLine");
        		Element eleOrderLine = null;
        		Element eleOrderLineExtn = null;
        		String sPickUpDate = null;
        		int iTotalLines = nShipmentLine.getLength();
        		for(int iLine=0; iLine < iTotalLines; iLine++){
        			eleOrderLine = XMLUtil.getChildElement((Element)nShipmentLine.item(iLine), "OrderLine");
        			if(!XMLUtil.isVoid(eleOrderLine)){
        				eleOrderLineExtn = XMLUtil.getChildElement(eleOrderLine, "Extn");
        				if(!XMLUtil.isVoid(eleOrderLineExtn)){
        					eleOrderLineExtn.getAttribute("ExtnPickupDate");
        					if(!XMLUtil.isVoid(eleOrderLineExtn.getAttribute("ExtnPickupDate"))){
        						sPickUpDate = eleOrderLineExtn.getAttribute("ExtnPickupDate");
        						break;
        					}
        				}
        			}
        		}
        		if(!XMLUtil.isVoid(sPickUpDate)){
        			try {
        			SimpleDateFormat sdfPickUpDateIn = new SimpleDateFormat("yyyy-MM-dd");
        			SimpleDateFormat sdfPickUpDateOut = new SimpleDateFormat("MM/dd/yyyy");
        			Date dPickUpDate = sdfPickUpDateIn.parse(sPickUpDate);
					sPickUpDate = sdfPickUpDateOut.format(dPickUpDate);
        			this.setFieldValue("txtRequestedShipDate", sPickUpDate);
        			} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
        		}

        		/**Defect#23 :  Requested Ship Date End****/


				String strDeliveryMethod =  eleShipment.getAttribute("DeliveryMethod");
				String strStatus = eleShipment.getAttribute("Status");
                if (strStatus.equals("1100.70.06.30")) {
                    Element eleOrder = this.getModel("Order");

                  if (!XMLUtil.isVoid(eleOrder)) {
                    NodeList nOrderStatusList = eleOrder.getElementsByTagName("OrderStatus");
                    Element eleOrderStatus = null;
                    String sOrderStatus = null;
                    boolean bDisplayCustomMessage = false;
                    int iLength = nOrderStatusList.getLength();

                          for(int i=0; i<iLength; i++){
                      eleOrderStatus = (Element)nOrderStatusList.item(i);
                      if (!XMLUtil.isVoid(eleOrderStatus)) {
                        sOrderStatus = eleOrderStatus.getAttribute("Status");
                                      if(!XMLUtil.isVoid(sOrderStatus) && sOrderStatus.equalsIgnoreCase("1400")){
                          bDisplayCustomMessage = true;
                          break;
                        }
                      }
                    }
                    if (bDisplayCustomMessage) {


                         YRCPlatformUI.setMessage(YRCPlatformUI.getString("BACKROOM_SHORT_PICK_MESSAGE"));
                    }
                  }
                }
				//Print Pack Ticket button available only if Delivery Method is PICK and Status is Ready for Customer
				//[Beging:Feb 3, 2014]Updated condition check to validate button on "Shipment Picked Up" status too.
					if(strDeliveryMethod.equals("PICK") && (strStatus.equals("1400.10") || strStatus.equals("1100.70.06.30")||strStatus.equals("1600.002"))){
					//[End:Feb 3, 2014]
					//--Kumar: excluding button from Grid : Defect# 625.
					this.setControlLayoutData("extn_print_packslip", objGridData);

				//Start SOM changes Req#6 May'14 Release
				this.setControlVisible("extn_print_packslip", true);
				//End SOM changes Req#6 May'14 Release
				}
    			else if(strStatus.equals("1100.70.06.10"))
				{
					this.setControlVisible("extn_RePrint", true);
				}

				//Start SOM changes Req#5 May'14 Release
				//R3/3 Begin: Defect# 723 Updating Delivery Method as PICKUP for PICK and SHIPPING for SHP
				if(!strDeliveryMethod.isEmpty())
				{
					if(strDeliveryMethod.equalsIgnoreCase("SHP"))
					{
						setFieldValue("extn_DeliveryMethod_txt", YRCPlatformUI.getString("SHIP")) ;
					}
					else
					{
						setFieldValue("extn_DeliveryMethod_txt", YRCPlatformUI.getString("PICK")) ;
					}
				}
				//R3/3 End: Defect# 723 Updating Delivery Method as PICKUP for PICK and SHIPPING for SHP
				//End SOM changes Req#5 May'14 Release
			}
		}
	}
}
//TODO Validation required for a Button control: extn_print_packslip
//TODO Validation required for a Text control: extn_DeliveryMethod_txt
//TODO Validation required for a Button control: extn_RePrint
