package com.yantra.pca.ycd.rcp.util;

/**
 * @author rajath_babu
 * Copyright IBM Corp. All Rights Reserved.
 */

/**
 *
 * Constant Name should be in Uppercase. Attribute should start with A_
 * (example: A_ACTION) Element should start with E_ (example: E_ORDER) Value
 * should start with V_ (example: V_THREE) No constraint on Misc constants
 * (example: INBOUND_SERVICE)
 */

public interface HBCSOMConstants {

	String ITEM_NOT_FOUND = "Item Not Found";
	String ITEM_DAMAGED = "Item Damaged";
	String INSUFFICIENT_QUANTITY = "Insufficient Quantity";
	//String BP_WILL_PICK_LATER_REASON = "Insufficient_Quantity";
	String BP_WILL_PICK_LATER_REASON = "Will Pick Later";
	String BP_INVENTORY_SHORTAGE_REASON = "Inventory shortage";
	String YCD_SHORTAGE_REASON_CODE_TYPE = "YCD_BP_SHRTG_RSLTN";
	String YCD_SHORTAGE_NOTE_REASON = "YCD_BACKROOM_PICK_SHORTAGE";
	String ITEM_NOT_FOUND_CODE_VALUE = "Item_Not_Found";
	String ITEM_DAMAGED_CODE_VALUE = "Item_Damaged";
	String INSUFFICIENT_QUANTITY_CODE_VALUE = "Insufficient_Quantity";
	//Added to fetch common code details to fetch the XPath to be used item attributes based on item status (ECom or MerchLocater)
	String DEFAULT_ORGANIZATION_CODE = "DEFAULT";
	String PRINT_ITEM_ATTR = "PRINT_ITEM_ATTR";
	//Added to modify confirm shipment search based on status
	String SHIPMENT_PACKED_STATUS = "1300";

	//R3.2: Defect 508- Start of modification to add a new reason code: Customer Cancel
	String CUSTOMER_CANCEL = "Customer Cancel";
	String CUSTOMER_CANCEL_CODE_VALUE = "Customer_Cancel";
	//R3.2: Defect 508- End of modification to add a new reason code: Customer Cancel

	//R3.2:  Defect 796- Start of modification to backorder remaining quantity on a partially canceled shipment
	String CUSTOMER_PICK_UP_CANCEL = "Cancel";
	String ALL_INVENTORY_SHORTAGE = "AllInventoryShortage";
	//R3.2  Defect 796- Start of modification to backorder remaining quantity on a partially canceled shipment

	//R3.2: Defect 409- Start of modification to add reason codes during COM/SOM order/shipment cancellation
	String RTVS = "RTV's";
	String RTVS_CODE_VALUE = "RTVs";
	String ZERO_DOLLAR = "$0.00";
	String ZERO_DOLLAR_CODE_VALUE = "Zero_Dollar";
	String ON_HAND = "On Hand Inaccuracy";
	String ON_HAND_CODE_VALUE = "On_Hand";
	String CLEARANCE = "Clearance";
	String CLEARANCE_CODE_VALUE = "Clearance";
	String GIFT_WITH_PURCHASE = "Gift W/ Purchase";
	String GIFT_WITH_PURCHASE_CODE_VALUE = "Gift_With_Purchase";
	String IN_TRANSIT = "In Transit";
	String IN_TRANSIT_CODE_VALUE = "In_Transit";
	String DUPLICATE_ORDER = "Duplicate Order";
	String DUPLICATE_ORDER_CODE_VALUE = "Duplicate_Order";
	String UPC_DOES_NOT_MATCH = "UPC does not match";
	String UPC_DOES_NOT_MATCH_CODE_VALUE = "UPC_Does_Not_Match";
	String MISDIRECT = "Misdirect";
	String MISDIRECT_CODE_VALUE = "Misdirect";
	String OTHER = "Other";
	String OTHER_CODE_VALUE = "Other";
	//R3.2: Defect 409- End of modification to add reason codes during COM/SOM order/shipment cancellation
	String NO_SELECTION ="";
	String NO_SELECTION_CODE_VALUE = "";

	//Defect 570 : Start - Adding reason codes for order cancellation
	String ZERO_DOLLAR_SHIP_REASON_CODE = "HBC_MC_SS_001" ;
	String ZERO_DOLLAR_PICKUP_REASON_CODE = "HBC_MC_PS_001" ;
	String GIFT_WITH_PURCHASE_REASON_CODE = "HBC_MC_SS_002" ;
	String RTV_PICKUP_REASON_CODE = "HBC_MC_PS_002" ;
	String ON_HAND_PICKUP_REASON_CODE = "HBC_MC_PS_003" ;
	String CLEARANCE_PICKUP_REASON_CODE = "HBC_MC_PS_004" ;
	String IN_TRANSIT_PICKUP_REASON_CODE = "HBC_MC_PS_005" ;
	String ITEM_DAMAGED_PICKUP_REASON_CODE = "HBC_MC_PS_006" ;
	String UPC_DOES_NOT_MATCH_PICKUP_REASON_CODE = "HBC_MC_PS_007" ;
	String MISDIRECT_PICKUP_REASON_CODE = "HBC_MC_PS_008" ;
	String OTHER_PICKUP_REASON_CODE = "HBC_MC_PS_009" ;
	//Defect 570 : End - Adding reason codes for order cancellation
	
	String DEFAULT_CARRIERSERVICE_CODE = "Default" ;
}
