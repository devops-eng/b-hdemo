
package com.yantra.pca.ycd.rcp.tasks.backroompick.wizards;

/**
 * Created on Jun 18,2013
 *
 */

import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCWizardExtensionBehavior;
import com.yantra.yfc.rcp.IYRCComposite;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCExtendedTableBindingData;
import com.yantra.yfc.rcp.YRCXmlUtils;
/**
 * @author rajath_babu
 * Copyright IBM Corp. All Rights Reserved.
 */
public class ExtnYCDBackroompickWizard extends YRCWizardExtensionBehavior {

	String sDeliveryMethod = null;
	/**
	 * This method initializes the behavior class.
	 */
	public void init() {
		//TODO: Write behavior init here.
	}


	public String getExtnNextPage(String currentPageId) {
		//TODO
		return null;
	}

	public IYRCComposite createPage(String pageIdToBeShown) {
		//TODO
		return null;
	}

	public void pageBeingDisposed(String pageToBeDisposed) {
		//TODO
	}

	/**
	 * Called when a wizard page is about to be shown for the first time.
	 *
	 */
	public void initPage(String pageBeingShown) {
		//TODO
	}


	/**
	 * Method for validating the text box.
	 */
	public YRCValidationResponse validateTextField(String fieldName, String fieldValue) {
		// TODO Validation required for the following controls.

		// TODO Create and return a response.
		return super.validateTextField(fieldName, fieldValue);
	}

	/**
	 * Method for validating the combo box entry.
	 */
	public void validateComboField(String fieldName, String fieldValue) {
		// TODO Validation required for the following controls.

		// TODO Create and return a response.
		super.validateComboField(fieldName, fieldValue);
	}

	/**
	 * Method called when a button is clicked.
	 */
	public YRCValidationResponse validateButtonClick(String fieldName) {
		// TODO Validation required for the following controls.

		// TODO Create and return a response.
		return super.validateButtonClick(fieldName);
	}

	/**
	 * Method called when a link is clicked.
	 */
	public YRCValidationResponse validateLinkClick(String fieldName) {
		// TODO Validation required for the following controls.

		// TODO Create and return a response.
		return super.validateLinkClick(fieldName);
	}

	/**
	 * Create and return the binding data for advanced table columns added to the tables.
	 */
	public YRCExtendedTableBindingData getExtendedTableBindingData(String tableName, ArrayList tableColumnNames) {
		// Create and return the binding data definition for the table.

		// The default super implementation does nothing.
		return super.getExtendedTableBindingData(tableName, tableColumnNames);
	}

	/**
	 * This method is implemented to achieve the following in a short pick scenarios.
	 * 1. For Pick Up orders - The unavailable quantity on the shipment should be canceled.
	 * 2. For Ship order - The unavailable quantity on the shipment should be backordered.
	 */
	public boolean preCommand(YRCApiContext apiContext){
		if ("changeShipment".equals(apiContext.getApiName())){
			Document dChangeShipment = apiContext.getInputXml();
			Element eChangeShipment = dChangeShipment.getDocumentElement();

			if ("PICK".equalsIgnoreCase(sDeliveryMethod)){											
				eChangeShipment.removeAttribute("BackOrderRemovedQuantity");
				eChangeShipment.setAttribute("CancelRemovedQuantity", "Y");
			}
			eChangeShipment.removeAttribute("Action");
		}
		return true;
	}

	public void postSetModel(String model) {
		//Check if Order is confirmed order then disable add/modify charges 
		if(model.equals("getShipmentDetails_output"))
		{
			Element ShipmentDetails = this.getModel("getShipmentDetails_output");
			if (ShipmentDetails != null) {
				sDeliveryMethod = ShipmentDetails.getAttribute("DeliveryMethod");
			}
		}
	}
}