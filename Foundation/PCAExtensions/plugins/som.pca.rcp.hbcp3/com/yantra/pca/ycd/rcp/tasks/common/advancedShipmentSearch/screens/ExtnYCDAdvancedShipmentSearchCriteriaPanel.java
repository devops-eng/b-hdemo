
package com.yantra.pca.ycd.rcp.tasks.common.advancedShipmentSearch.screens;

/**
 * Created on Jun 18,2013
 *
 */

import java.awt.Button;
import java.util.ArrayList;

import org.w3c.dom.Document;

import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCExtentionBehavior;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCExtendedTableBindingData;
import com.yantra.yfc.rcp.YRCXmlUtils;
/**
 * @author mohit_sharma15
 * Copyright IBM Corp. All Rights Reserved.
 */
public class ExtnYCDAdvancedShipmentSearchCriteriaPanel extends YRCExtentionBehavior{

	/**
	 * This method initializes the behavior class.
	 * This method is implemented to hide the combo box present at 
	 * advanced shipment search screen, as a new combo box is created for extended funtionality .
	 */
	public void init() {
		this.setControlVisible("extn_comboCarrier", false);
		//TODO: Write behavior init here.
	}

	/**
	 * Method for validating the text box.
	 */
	public YRCValidationResponse validateTextField(String fieldName, String fieldValue) {
		// TODO Validation required for the following controls.

		// TODO Create and return a response.
		return super.validateTextField(fieldName, fieldValue);
	}

	/**
	 * Method for validating the combo box entry.
	 */
	public void validateComboField(String fieldName, String fieldValue) {
		// TODO Validation required for the following controls.

		// TODO Create and return a response.
		super.validateComboField(fieldName, fieldValue);
	}

	/**
	 * Method called when a button is clicked.
	 * This method is implemented to achieve following tasks
	 * 
	 * 1. On advanced shipment search when pick up radio button is clicked 
	 * combo box for carrier selection is hidden.
	 * 
	 * 2. On Advanced shipment search when ship option is selected, 
	 * the combo box for carrier selection is made visible.
	 */
	public YRCValidationResponse validateButtonClick(String fieldName) {
		// TODO Validation required for the following controls.
		if ("bttnShip".equalsIgnoreCase(fieldName))
		{
			this.setControlVisible("extn_comboCarrier", true);
		}

		else
		{
			this.setControlVisible("extn_comboCarrier", false);
		}
		// TODO Create and return a response.**/
		return super.validateButtonClick(fieldName);
	}

	/**
	 * Method called when a link is clicked.
	 */
	public YRCValidationResponse validateLinkClick(String fieldName) {
		// TODO Validation required for the following controls.

		// TODO Create and return a response.
		return super.validateLinkClick(fieldName);
	}

	/**
	 * Create and return the binding data for advanced table columns added to the tables.
	 */
	public YRCExtendedTableBindingData getExtendedTableBindingData(String tableName, ArrayList tableColumnNames) {
		// Create and return the binding data definition for the table.

		// The defualt super implementation does nothing.
		return super.getExtendedTableBindingData(tableName, tableColumnNames);
	}

	@Override
	public boolean preCommand(YRCApiContext apiContext) {

		return super.preCommand(apiContext);
	}

	@Override
	public void postCommand(YRCApiContext apiContext) {
		if ("getScacList".equalsIgnoreCase(apiContext.getApiName()))
		{
			Document dgetScacListOut = apiContext.getOutputXml();
		}
		super.postCommand(apiContext);
	}
}
//TODO Validation required for a Radio control: bttnPick
//TODO Validation required for a Radio control: bttnShip