	
package com.yantra.pca.ycd.rcp.tasks.shipmentTracking.screens;

/**
 * Created on Jul 24,2013
 *
 */
 
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.program.Program;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.Barcode128;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.yantra.yfc.rcp.YRCApiContext;
import com.yantra.yfc.rcp.YRCExtentionBehavior;
import com.yantra.yfc.rcp.YRCPlatformUI;
import com.yantra.yfc.rcp.YRCValidationResponse;
import com.yantra.yfc.rcp.YRCExtendedTableBindingData;
import com.yantra.yfc.rcp.YRCXmlUtils;
/**
 * @author rajath_babu
 * Copyright IBM Corp. All Rights Reserved.
 */
 public class ExtnYCDShipmentDetails extends YRCExtentionBehavior{
	 
	 String pltform = SWT.getPlatform();
	 
	/**
	 * This method initializes the behavior class.
	 */
	public void init() {
		//TODO: Write behavior init here.
	}
 
 	
 	
	/**
	 * Method for validating the text box.
     */
    public YRCValidationResponse validateTextField(String fieldName, String fieldValue) {
    	// TODO Validation required for the following controls.
		
		// TODO Create and return a response.
		return super.validateTextField(fieldName, fieldValue);
	}
    
    /**
     * Method for validating the combo box entry.
     */
    public void validateComboField(String fieldName, String fieldValue) {
    	// TODO Validation required for the following controls.
		
		// TODO Create and return a response.
		super.validateComboField(fieldName, fieldValue);
    }
    
    /**
	 * This method is implemented to customize the "Print" button so that barcode 
	 * can be printed in the pick ticket.
	 */
    public YRCValidationResponse validateButtonClick(String fieldName) {
    	// TODO Validation required for the following controls.
		
    	if(fieldName.equals("extn_RePrint")){
    		
    		//Check if the platform is Windows 
			if ("win32".equals(pltform)) 
			{  
				String sUserID = getUserID();
				String sShipNode = getShipNode();
				org.w3c.dom.Document docHBCPrintPickListInXML = YRCXmlUtils.createDocument("Shipments");  
				org.w3c.dom.Element eleShipment = this.getModel("Shipment");
				if (!YRCPlatformUI.isVoid(eleShipment)) 
				{
					org.w3c.dom.Element eleShipmentIn = docHBCPrintPickListInXML.createElement("Shipment");
					eleShipmentIn.setAttribute("Loginid", sUserID);
					eleShipmentIn.setAttribute("ShipNode", sShipNode);
					eleShipmentIn.setAttribute("SellerOrganizationCode", eleShipment.getAttribute("SellerOrganizationCode"));
					eleShipmentIn.setAttribute("ShipmentNo", eleShipment.getAttribute("ShipmentNo"));
					eleShipmentIn.setAttribute("EnterpriseCode", eleShipment.getAttribute("EnterpriseCode"));
					docHBCPrintPickListInXML.getDocumentElement().appendChild(eleShipmentIn);	
				}
				//Invoke "HBCPrinrPickList" to fetch the details for generating the pick ticket
				YRCApiContext context = new YRCApiContext();
				context.setFormId(super.getFormId());				
				context.setApiName("HBCPrintPickList");
				context.setInputXml(docHBCPrintPickListInXML);
				callApi(context); 
			}
    	}
		
		// TODO Create and return a response.
		return super.validateButtonClick(fieldName);
    }
    
    /**
	 * This method is implemented to handle the completion of custom API call
	 */
	public void handleApiCompletion(YRCApiContext context) 
	{
		if ("HBCPrintPickList".equals(context.getApiName()))
		{						
			this.setExtentionModel("Extn_output_HBCPrintPickList", context.getOutputXml().getDocumentElement());					
			try { 
				//Invoke generateBarCode() to generate the pick ticket
				generateBarCode();				                         					
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
	}
    
    /**
     * Method called when a link is clicked.
     */
	public YRCValidationResponse validateLinkClick(String fieldName) {
    	// TODO Validation required for the following controls.
		
		// TODO Create and return a response.
		return super.validateLinkClick(fieldName);
	}
	
	/**
	 * Create and return the binding data for advanced table columns added to the tables.
	 */
	 public YRCExtendedTableBindingData getExtendedTableBindingData(String tableName, ArrayList tableColumnNames) {
	 	// Create and return the binding data definition for the table.
		
	 	// The defualt super implementation does nothing.
	 	return super.getExtendedTableBindingData(tableName, tableColumnNames);
	 }
	 	 	
	 /**
		 * This method is implemented to generate the pick list in the required format with barcode using iText
		 */
		public void generateBarCode () throws Exception {

			//Format of the barcode to be used
			Barcode128 code128 = new Barcode128();
			code128.setGenerateChecksum(true);		

			//Fetch the temp directory
			String sDocumentLocation = System.getProperty ("java.io.tmpdir");
			String sDocumentType = ".pdf"; 
			com.itextpdf.text.Document document = new com.itextpdf.text.Document(new Rectangle(PageSize.A4));

			//Adding data to the table     
			org.w3c.dom.Element eleSortedShipmentOutput = this.getModel("Extn_output_HBCPrintPickList"); 
			if (!YRCPlatformUI.isVoid(eleSortedShipmentOutput)) {				
				org.w3c.dom.Element eleDocumentName  = YRCXmlUtils.getXPathElement(eleSortedShipmentOutput, "Shipments/Shipment/ShipmentDetails/MultiApi/API/Output/Shipment");
				if (!YRCPlatformUI.isVoid(eleDocumentName)) {
					String sDocumentName = eleDocumentName.getAttribute("ShipmentNo"); 

					String sDocumentPathAndName = sDocumentLocation+sDocumentName+sDocumentType;
					PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(sDocumentPathAndName));
					document.open();

					NodeList nSortedShipmentList =  eleSortedShipmentOutput.getElementsByTagName("ShipmentDetails");			
					for (int g = 0; g < nSortedShipmentList.getLength(); g++) {
						document.newPage();
						org.w3c.dom.Element eleShipmentDetails = (org.w3c.dom.Element) nSortedShipmentList.item(g);
						org.w3c.dom.Element eleShipment = YRCXmlUtils.getXPathElement(eleShipmentDetails, "ShipmentDetails/MultiApi/API/Output/Shipment");
						if (!YRCPlatformUI.isVoid(eleShipment)) {
							org.w3c.dom.Element eleToAddress = YRCXmlUtils.getXPathElement(eleShipment, "Shipment/ToAddress");
							if (!YRCPlatformUI.isVoid(eleToAddress)) {
								String sCity = eleToAddress.getAttribute("City");
								String sState = eleToAddress.getAttribute("State");
								String sShortZipCode = eleToAddress.getAttribute("ShortZipCode");
								String sFormattedCityStateZipCodeStore = sCity+sState+sShortZipCode;
								String sFirstName = eleToAddress.getAttribute("FirstName");
								String sLastName = eleToAddress.getAttribute("LastName");
								String sCompany = eleToAddress.getAttribute("Company");
								String sAddressLine1 = eleToAddress.getAttribute("AddressLine1");
								String sAddressLine2 = eleToAddress.getAttribute("AddressLine2");
								String sCountry = eleToAddress.getAttribute("Country");
								String sDayPhone = eleToAddress.getAttribute("DayPhone");
								String sEmailID = eleToAddress.getAttribute("EmailID");				
								String sCustomerName = sFirstName+" "+sLastName;
								String sOrderDate ="";
								String sOrderNo ="";
								String sDeliveryMethod = eleShipment.getAttribute("DeliveryMethod");
								String sShipmentNo = eleShipment.getAttribute("ShipmentNo");
								String sShipDate = eleShipment.getAttribute("ShipDate");
								String sReqShipDate = eleShipment.getAttribute("RequestedShipmentDate");
								String sExpectedDeliveryDate = eleShipment.getAttribute("ExpectedDeliveryDate");

								org.w3c.dom.Element eleOrder = YRCXmlUtils.getXPathElement(eleShipment, "Shipment/ShipmentLines/ShipmentLine/Order");
								if (!YRCPlatformUI.isVoid(eleOrder)) {
									sOrderDate = eleOrder.getAttribute("OrderDate");
									sOrderNo = eleOrder.getAttribute("OrderNo");

									org.w3c.dom.Element eleShipNodePersonInfo = YRCXmlUtils.getXPathElement(eleShipment, "Shipment/ShipNode/ShipNodePersonInfo");
									if (!YRCPlatformUI.isVoid(eleShipNodePersonInfo)) {
										String sStoreName = eleShipNodePersonInfo.getAttribute("Description");
										String sStoreCompany = eleShipNodePersonInfo.getAttribute("Company");
										String sStoreAddress1 = eleShipNodePersonInfo.getAttribute("AddressLine1");
										String sStoreAddress2 = eleShipNodePersonInfo.getAttribute("AddressLine2");
										String sStoreCity = eleShipNodePersonInfo.getAttribute("City");
										String sStoreState = eleShipNodePersonInfo.getAttribute("State");
										String sStoreZipCode = eleToAddress.getAttribute("ZipCode");
										String sStoreDayPhone = eleToAddress.getAttribute("DayPhone");
										String sStoreEmailID = eleToAddress.getAttribute("EmailID");
										String sStoreFormattedCityStateZipCode = sStoreCity+sStoreState+sStoreZipCode;

										//Fonts
										Font fontHeaderDetails = new Font(FontFamily.TIMES_ROMAN, 9, Font.NORMAL, BaseColor.BLACK);
										Font fontHeaderDetailsBold = new Font(FontFamily.TIMES_ROMAN, 9, Font.BOLD, BaseColor.BLACK);
										Font fontCell = new Font(FontFamily.TIMES_ROMAN, 7, Font.NORMAL, BaseColor.BLACK);

										//Store Details Table
										PdfPTable storeDetailsTable = new PdfPTable(1);
										storeDetailsTable.setWidthPercentage(30);
										storeDetailsTable.getDefaultCell().setBorder(0);
										storeDetailsTable.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);

										//Store Name Details									
										Phrase pStoreName = new Phrase(String.format(sStoreName), fontHeaderDetailsBold);
										PdfPCell StoreNameDataCell = new PdfPCell(); 
										StoreNameDataCell.setPhrase(pStoreName);
										StoreNameDataCell.setBorder(Rectangle.NO_BORDER);

										//Company Details									
										Phrase pStoreCompany = new Phrase(String.format(sStoreCompany), fontHeaderDetails);
										PdfPCell StoreCompanyDataCell = new PdfPCell(); 
										StoreCompanyDataCell.setPhrase(pStoreCompany);
										StoreCompanyDataCell.setBorder(Rectangle.NO_BORDER);

										//StoreAddress1 Details									
										Phrase pStoreAddress1 = new Phrase(String.format(sStoreAddress1), fontHeaderDetails);
										PdfPCell StoreAddress1DataCell = new PdfPCell(); 
										StoreAddress1DataCell.setPhrase(pStoreAddress1);
										StoreAddress1DataCell.setBorder(Rectangle.NO_BORDER);

										//StoreAddress2 Details									
										Phrase pStoreAddress2 = new Phrase(String.format(sStoreAddress2), fontHeaderDetails);
										PdfPCell StoreAddress2DataCell = new PdfPCell(); 
										StoreAddress2DataCell.setPhrase(pStoreAddress2);
										StoreAddress2DataCell.setBorder(Rectangle.NO_BORDER);

										//Company Details									
										Phrase pStoreFormattedCityStateZipCode = new Phrase(String.format(sStoreFormattedCityStateZipCode), fontHeaderDetails);
										PdfPCell StoreFormattedCityStateZipCodeDataCell = new PdfPCell(); 
										StoreFormattedCityStateZipCodeDataCell.setPhrase(pStoreFormattedCityStateZipCode);
										StoreFormattedCityStateZipCodeDataCell.setBorder(Rectangle.NO_BORDER);

										//Store  Day Phone Details									
										Phrase pStoreDayPhone = new Phrase(String.format(sStoreDayPhone), fontHeaderDetails);
										PdfPCell StoreDayPhoneDataCell = new PdfPCell(); 
										StoreDayPhoneDataCell.setPhrase(pStoreDayPhone);
										StoreDayPhoneDataCell.setBorder(Rectangle.NO_BORDER);

										//Company Details									
										Phrase pStoreEmailID = new Phrase(String.format(sStoreEmailID), fontHeaderDetails);
										PdfPCell StoreEmailIDDataCell = new PdfPCell(); 
										StoreEmailIDDataCell.setPhrase(pStoreEmailID);
										StoreEmailIDDataCell.setBorder(Rectangle.NO_BORDER);

										storeDetailsTable.addCell(StoreNameDataCell);
										storeDetailsTable.addCell(StoreCompanyDataCell);
										storeDetailsTable.addCell(StoreAddress1DataCell);
										storeDetailsTable.addCell(StoreAddress2DataCell);
										storeDetailsTable.addCell(pStoreAddress2);
										storeDetailsTable.addCell(StoreFormattedCityStateZipCodeDataCell);
										storeDetailsTable.addCell(StoreDayPhoneDataCell);
										storeDetailsTable.addCell(StoreEmailIDDataCell);													

										document.add(storeDetailsTable);

										//Add document Title

										addTitle(document);

										//Anchor Table
										PdfPTable anchorTable = new PdfPTable(2);
										anchorTable.setWidthPercentage(100);
										anchorTable.getDefaultCell().setBorder(0);

										//Add Current Date
										PdfPTable currentDateTable = new PdfPTable(2);
										currentDateTable.setWidthPercentage(50);
										currentDateTable.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);

										//Current Date Details
										PdfPCell CurrentDateCell= new PdfPCell(new Phrase("Current Date", fontHeaderDetailsBold));
										CurrentDateCell.setBorder(Rectangle.NO_BORDER);	
										CurrentDateCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
										Date date = new Date(); 
										String sDate = date.toString();
										Phrase pCurrentDate = new Phrase(String.format(sDate), fontHeaderDetails);
										PdfPCell CurrentDateDataCell = new PdfPCell(); 
										CurrentDateDataCell.setPhrase(pCurrentDate);
										CurrentDateDataCell.setBorder(Rectangle.NO_BORDER);

										//Barcode for Order No
										code128.setCode(sOrderNo);				
										Image imageBarCode = code128.createImageWithBarcode(writer.getDirectContent(), null, null);

										PdfPCell OrderNoDetailsCell= new PdfPCell(new Phrase(" ", fontHeaderDetailsBold));
										OrderNoDetailsCell.setBorder(Rectangle.NO_BORDER);	
										OrderNoDetailsCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);				
										PdfPCell OrderNoBarcodeCell = new PdfPCell(imageBarCode,false);												
										OrderNoBarcodeCell.setBorder(Rectangle.NO_BORDER);

										//Empty Cell Details
										PdfPCell EmptyDataCell= new PdfPCell(new Phrase(" ", fontHeaderDetails));
										EmptyDataCell.setBorder(Rectangle.NO_BORDER);					
										Phrase pEmptyData = new Phrase();
										PdfPCell EmptyDateCellValue = new PdfPCell(); 
										EmptyDateCellValue.setPhrase(pEmptyData);
										EmptyDateCellValue.setBorder(Rectangle.NO_BORDER);

										//Add the cells to currentDateTable
										currentDateTable.addCell(CurrentDateCell);
										currentDateTable.addCell(CurrentDateDataCell);
										currentDateTable.addCell(EmptyDataCell);
										currentDateTable.addCell(EmptyDateCellValue);
										currentDateTable.addCell(OrderNoDetailsCell);
										currentDateTable.addCell(OrderNoBarcodeCell);								

										//Check if Delivery Method is PICK
										if (sDeliveryMethod.equalsIgnoreCase("PICK"))
										{
											PdfPTable headerTable = new PdfPTable(2);
											headerTable.setWidthPercentage(50);
											headerTable.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);						
											//Order No Details
											PdfPCell OrderNoCell= new PdfPCell(new Phrase("Order #", fontHeaderDetailsBold));
											OrderNoCell.setBorder(Rectangle.NO_BORDER);					
											Phrase pOrderNo = new Phrase(String.format(sOrderNo), fontHeaderDetails);
											PdfPCell OrderNoDataCell = new PdfPCell(); 				        
											OrderNoDataCell.setPhrase(pOrderNo);
											OrderNoDataCell.setBorder(Rectangle.NO_BORDER);				

											//Shipment No Details
											PdfPCell ShipmentNoCell= new PdfPCell(new Phrase("Shipment #", fontHeaderDetailsBold));
											ShipmentNoCell.setBorder(Rectangle.NO_BORDER);					
											Phrase pShipmentNo = new Phrase(String.format(sShipmentNo), fontHeaderDetails);
											PdfPCell ShipmentNoDataCell = new PdfPCell(); 
											ShipmentNoDataCell.setPhrase(pShipmentNo);
											ShipmentNoDataCell.setBorder(Rectangle.NO_BORDER);

											//Customer Name Details
											PdfPCell CustomerNameCell= new PdfPCell(new Phrase("Customer Name", fontHeaderDetailsBold));
											CustomerNameCell.setBorder(Rectangle.NO_BORDER);					
											Phrase pCustomerName = new Phrase(String.format(sCustomerName), fontHeaderDetails);
											PdfPCell CustomerNameDataCell = new PdfPCell(); 
											CustomerNameDataCell.setPhrase(pCustomerName);
											CustomerNameDataCell.setBorder(Rectangle.NO_BORDER);

											//Ship Date Details
											PdfPCell ShipDateCell= new PdfPCell(new Phrase("Store Notified On", fontHeaderDetailsBold));
											ShipDateCell.setBorder(Rectangle.NO_BORDER);					
											Phrase pShipDate = new Phrase(String.format(sShipDate.substring(0, 10)), fontHeaderDetails);
											PdfPCell ShipDateDataCell = new PdfPCell(); 
											ShipDateDataCell.setPhrase(pShipDate);
											ShipDateDataCell.setBorder(Rectangle.NO_BORDER);

											//Pickup Date Details
											PdfPCell PickUpDateCell= new PdfPCell(new Phrase("Pick Up Date", fontHeaderDetailsBold));
											PickUpDateCell.setBorder(Rectangle.NO_BORDER);					
											Phrase pPickUpDate = new Phrase(String.format(sReqShipDate.substring(0, 10)), fontHeaderDetails);
											PdfPCell PickupDateDataCell = new PdfPCell(); 
											PickupDateDataCell.setPhrase(pPickUpDate);
											PickupDateDataCell.setBorder(Rectangle.NO_BORDER);

											//Order Date Details
											PdfPCell OrderDateCell= new PdfPCell(new Phrase("Order Date", fontHeaderDetailsBold));
											OrderDateCell.setBorder(Rectangle.NO_BORDER);					
											Phrase pOrderDate = new Phrase(String.format(sOrderDate.substring(0, 10)), fontHeaderDetails);
											PdfPCell OrderDateDataCell = new PdfPCell(); 
											OrderDateDataCell.setPhrase(pOrderDate);
											OrderDateDataCell.setBorder(Rectangle.NO_BORDER);										

											headerTable.addCell(OrderNoCell);
											headerTable.addCell(OrderNoDataCell);
											headerTable.addCell(ShipmentNoCell);
											headerTable.addCell(ShipmentNoDataCell);
											headerTable.addCell(CustomerNameCell);
											headerTable.addCell(CustomerNameDataCell);
											headerTable.addCell(ShipDateCell);
											headerTable.addCell(ShipDateDataCell);
											headerTable.addCell(PickUpDateCell);
											headerTable.addCell(PickupDateDataCell);
											headerTable.addCell(OrderDateCell);
											headerTable.addCell(OrderDateDataCell);

											//Add tables to Anchor Table
											anchorTable.addCell(headerTable);
											anchorTable.addCell(currentDateTable);					

											//Add Anchor Table to the document
											document.add(anchorTable);
											document.add(Chunk.NEWLINE);									
										}

										//Check if Delivery Method is SHP
										if (sDeliveryMethod.equalsIgnoreCase("SHP"))
										{
											//Ship To Details And Instructions Anchor Table
											PdfPTable shipInstrAnchorTable = new PdfPTable(2);
											shipInstrAnchorTable.setWidthPercentage(100);
											shipInstrAnchorTable.getDefaultCell().setBorder(0);	

											PdfPTable headerTable = new PdfPTable(2);
											headerTable.setWidthPercentage(50);
											headerTable.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);												

											//Order No Details
											PdfPCell OrderNoCell= new PdfPCell(new Phrase("Order #", fontHeaderDetailsBold));
											OrderNoCell.setBorder(Rectangle.NO_BORDER);					
											Phrase pOrderNo = new Phrase(String.format(sOrderNo), fontHeaderDetails);
											PdfPCell OrderNoDataCell = new PdfPCell(); 				        
											OrderNoDataCell.setPhrase(pOrderNo);
											OrderNoDataCell.setBorder(Rectangle.NO_BORDER);				

											//Shipment No Details
											PdfPCell ShipmentNoCell= new PdfPCell(new Phrase("Shipment #", fontHeaderDetailsBold));
											ShipmentNoCell.setBorder(Rectangle.NO_BORDER);					
											Phrase pShipmentNo = new Phrase(String.format(sShipmentNo), fontHeaderDetails);
											PdfPCell ShipmentNoDataCell = new PdfPCell(); 
											ShipmentNoDataCell.setPhrase(pShipmentNo);
											ShipmentNoDataCell.setBorder(Rectangle.NO_BORDER);

											//Expected Ship Date Details
											PdfPCell ExpDeliveryDateCell= new PdfPCell(new Phrase("Expected Ship Date", fontHeaderDetailsBold));
											ExpDeliveryDateCell.setBorder(Rectangle.NO_BORDER);					
											Phrase pExpDelDate = new Phrase(String.format(sExpectedDeliveryDate.substring(0, 10)), fontHeaderDetails);
											PdfPCell ExpDeliveryDateDataCell = new PdfPCell(); 
											ExpDeliveryDateDataCell.setPhrase(pExpDelDate);
											ExpDeliveryDateDataCell.setBorder(Rectangle.NO_BORDER);									

											//Order Date Details
											PdfPCell OrderDateCell= new PdfPCell(new Phrase("Order Date", fontHeaderDetailsBold));
											OrderDateCell.setBorder(Rectangle.NO_BORDER);					
											Phrase pOrderDate = new Phrase(String.format(sOrderDate.substring(0, 10)), fontHeaderDetails);
											PdfPCell OrderDateDataCell = new PdfPCell(); 
											OrderDateDataCell.setPhrase(pOrderDate);
											OrderDateDataCell.setBorder(Rectangle.NO_BORDER);															

											headerTable.addCell(OrderNoCell);
											headerTable.addCell(OrderNoDataCell);
											headerTable.addCell(ShipmentNoCell);
											headerTable.addCell(ShipmentNoDataCell);
											headerTable.addCell(ExpDeliveryDateCell);
											headerTable.addCell(ExpDeliveryDateDataCell);					
											headerTable.addCell(OrderDateCell);
											headerTable.addCell(OrderDateDataCell);
											headerTable.addCell(EmptyDataCell);
											headerTable.addCell(EmptyDateCellValue);				    					

											//Table for Instructions
											PdfPTable instructionsTable = new PdfPTable(1);
											instructionsTable.setWidthPercentage(50);
											instructionsTable.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);	
											org.w3c.dom.Element eleInstructions = YRCXmlUtils.getXPathElement(eleShipment, "Shipment/Instructions");
											if (eleInstructions != null && !(eleInstructions.equals(""))) {						
												NodeList nInstruction = eleInstructions.getElementsByTagName("Instruction");
												if (1 <= nInstruction.getLength()){
													PdfPCell InstructionsCell= new PdfPCell(new Phrase("Instructions", fontHeaderDetailsBold));
													//InstructionsCell.setBorder(Rectangle.NO_BORDER);	
													InstructionsCell.setBackgroundColor(BaseColor.LIGHT_GRAY);		
													instructionsTable.addCell(InstructionsCell);						
													for (int r=0; r<nInstruction.getLength(); r++) {
														org.w3c.dom.Element eleInstruction = (org.w3c.dom.Element) nInstruction.item(r);
														if (eleInstruction != null && !(eleInstruction.equals("")))
														{
															String sInstructionText = eleInstruction.getAttribute("InstructionText");
															Phrase pInstructionText = new Phrase(String.format(sInstructionText), fontHeaderDetails);
															PdfPCell InstructionCell= new PdfPCell();
															InstructionCell.setPhrase(pInstructionText);
															InstructionCell.setBorder(Rectangle.NO_BORDER);
															instructionsTable.addCell(InstructionCell);								
														}						
													}							
												}
											}									

											//Ship To Details Table
											PdfPTable shipToTable = new PdfPTable(1);
											shipToTable.setWidthPercentage(50);
											shipToTable.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);					

											PdfPCell ShipToCell= new PdfPCell(new Phrase("Ship To", fontHeaderDetailsBold));	
											ShipToCell.setBackgroundColor(BaseColor.LIGHT_GRAY);

											//Customer Name Details
											Phrase pShipTo = new Phrase(String.format(sCustomerName), fontHeaderDetails);
											PdfPCell ShipToDataCell = new PdfPCell(); 
											ShipToDataCell.setPhrase(pShipTo);
											ShipToDataCell.setBorder(Rectangle.NO_BORDER);					

											//Company Details									
											Phrase pCompany = new Phrase(String.format(sCompany), fontHeaderDetails);
											PdfPCell CompanyDataCell = new PdfPCell(); 
											CompanyDataCell.setPhrase(pCompany);
											CompanyDataCell.setBorder(Rectangle.NO_BORDER);

											//AddressLine1 Details									
											Phrase pAddressLine1 = new Phrase(String.format(sAddressLine1), fontHeaderDetails);
											PdfPCell AddressLine1DataCell = new PdfPCell(); 
											AddressLine1DataCell.setPhrase(pAddressLine1);
											AddressLine1DataCell.setBorder(Rectangle.NO_BORDER);

											//AddressLine2 Details									
											Phrase pAddressLine2 = new Phrase(String.format(sAddressLine2), fontHeaderDetails);
											PdfPCell AddressLine2DataCell = new PdfPCell(); 
											AddressLine2DataCell.setPhrase(pAddressLine2);
											AddressLine2DataCell.setBorder(Rectangle.NO_BORDER);

											//FormattedCityStateZipCodeStore Details									
											Phrase pCityStateZipCodeStore = new Phrase(String.format(sFormattedCityStateZipCodeStore), fontHeaderDetails);
											PdfPCell CityStateZipCodeStoreDataCell = new PdfPCell(); 
											CityStateZipCodeStoreDataCell.setPhrase(pCityStateZipCodeStore);
											CityStateZipCodeStoreDataCell.setBorder(Rectangle.NO_BORDER);

											//CountryDetails									
											Phrase pCountry = new Phrase(String.format(sCountry), fontHeaderDetails);
											PdfPCell CountryDataCell = new PdfPCell(); 
											CountryDataCell.setPhrase(pCountry);
											CountryDataCell.setBorder(Rectangle.NO_BORDER);

											//Day Phone Details									
											Phrase pDayPhone = new Phrase(String.format(sDayPhone), fontHeaderDetails);
											PdfPCell DayPhoneDataCell = new PdfPCell(); 
											DayPhoneDataCell.setPhrase(pDayPhone);
											DayPhoneDataCell.setBorder(Rectangle.NO_BORDER);

											//EmailID Details									
											Phrase pEmailID = new Phrase(String.format(sEmailID), fontHeaderDetails);
											PdfPCell EmailIDDataCell = new PdfPCell(); 
											EmailIDDataCell.setPhrase(pEmailID);
											EmailIDDataCell.setBorder(Rectangle.NO_BORDER);

											shipToTable.addCell(ShipToCell);
											shipToTable.addCell(ShipToDataCell);
											shipToTable.addCell(CompanyDataCell);
											shipToTable.addCell(AddressLine1DataCell);
											shipToTable.addCell(AddressLine2DataCell);
											shipToTable.addCell(CityStateZipCodeStoreDataCell);
											shipToTable.addCell(CountryDataCell);
											shipToTable.addCell(DayPhoneDataCell);
											shipToTable.addCell(EmailIDDataCell);
											//shipToTable.addCell(EmptyDataCell);

											//Add Tables to Anchor Table
											anchorTable.addCell(headerTable);
											anchorTable.addCell(currentDateTable);					

											//Add Anchor Table to the document
											document.add(anchorTable);

											//Add Tables to Ship To Details And Instructions Anchor Table Table
											shipInstrAnchorTable.addCell(shipToTable);
											shipInstrAnchorTable.addCell(instructionsTable);				  

											//Add Ship To Details And Instructions Anchor Table to the document
											document.add(shipInstrAnchorTable);
											document.add(Chunk.NEWLINE);					
										}

										PdfPTable table = new PdfPTable(new float[]{ 2, 5, 4, 8, 3, 3, 3, 3, 4,
												3, 2, 2, 2, 2, 3}); 
										table.setWidthPercentage(100f);

										//Sl.No Cell
										PdfPCell SLNoCell= new PdfPCell(new Phrase("Sl.No", fontCell)); 
										SLNoCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
										SLNoCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
										//UPC Code Cell
										PdfPCell UPCCodeCell= new PdfPCell(new Phrase("UPC Code", fontCell)); 
										UPCCodeCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
										UPCCodeCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
										//Item ID Cell
										PdfPCell ItemIDCell= new PdfPCell(new Phrase("Item ID", fontCell)); 
										ItemIDCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
										ItemIDCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
										//Item Description Cell
										PdfPCell ItemDescCell= new PdfPCell(new Phrase("Item Description", fontCell)); 
										ItemDescCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
										ItemDescCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
										//Original Quantity Cell
										PdfPCell OrgQtyCell= new PdfPCell(new Phrase("Original Quantity", fontCell)); 
										OrgQtyCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
										OrgQtyCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
										//UOM Cell
										PdfPCell UOMCell= new PdfPCell(new Phrase("UOM", fontCell)); 
										UOMCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
										UOMCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
										//Quantity To Pick Cell
										PdfPCell PickQtyCell= new PdfPCell(new Phrase("Quantity To Pick", fontCell)); 
										PickQtyCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
										PickQtyCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
										//Picked Quantity Cell
										PdfPCell PickedQtyCell= new PdfPCell(new Phrase("Picked Quantity", fontCell)); 
										PickedQtyCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
										PickedQtyCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
										//Department Cell
										PdfPCell DeptCell= new PdfPCell(new Phrase("Department", fontCell)); 
										DeptCell.setBackgroundColor(BaseColor.LIGHT_GRAY);	 
										DeptCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
										//Division Cell
										PdfPCell DivisionCell= new PdfPCell(new Phrase("Division", fontCell)); 
										DivisionCell.setBackgroundColor(BaseColor.LIGHT_GRAY);	
										DivisionCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
										//Gift Box Cell
										PdfPCell GiftBoxCell= new PdfPCell(new Phrase("Gift Box", fontCell)); 
										GiftBoxCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
										GiftBoxCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
										//Gift Wrap Cell
										PdfPCell GiftWrapCell= new PdfPCell(new Phrase("Gift Wrap", fontCell)); 
										GiftWrapCell.setBackgroundColor(BaseColor.LIGHT_GRAY);	 
										GiftWrapCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
										//Color Cell
										PdfPCell ColorCell= new PdfPCell(new Phrase("Color", fontCell)); 
										ColorCell.setBackgroundColor(BaseColor.LIGHT_GRAY);	
										ColorCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
										//Size Cell
										PdfPCell SizeCell= new PdfPCell(new Phrase("Size", fontCell)); 
										SizeCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
										SizeCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
										//Price Cell
										PdfPCell PriceCell= new PdfPCell(new Phrase("Price", fontCell)); 
										PriceCell.setBackgroundColor(BaseColor.LIGHT_GRAY);	 
										PriceCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);

										//Add the cells to the table        
										table.addCell(SLNoCell);
										table.addCell(UPCCodeCell);
										table.addCell(ItemIDCell);
										table.addCell(ItemDescCell);
										table.addCell(OrgQtyCell);
										table.addCell(UOMCell);
										table.addCell(PickQtyCell);
										table.addCell(PickedQtyCell);
										table.addCell(DeptCell);
										table.addCell(DivisionCell);
										table.addCell(GiftBoxCell);
										table.addCell(GiftWrapCell);
										table.addCell(ColorCell);
										table.addCell(SizeCell);
										table.addCell(PriceCell);					

										org.w3c.dom.Element eleShipmentLines = YRCXmlUtils.getXPathElement(eleShipment, "Shipment/ShipmentLines");
										if (!YRCPlatformUI.isVoid(eleShipmentLines)) {

											NodeList nShipmentLines = eleShipmentLines.getElementsByTagName("ShipmentLine");
											for (int k=0; k<nShipmentLines.getLength(); k++) {

												org.w3c.dom.Element eleShipmentLine = (org.w3c.dom.Element) nShipmentLines.item(k);												

												//SlNo Data Cell	
												String sSlNo = String.valueOf(k+1);
												Phrase pSlNo = new Phrase(String.format(sSlNo), fontCell);
												PdfPCell SLNoDataCell = new PdfPCell(); 				        
												SLNoDataCell.setPhrase(pSlNo);	

												//UPCCode Data Cell							
												NodeList nOrderLines = eleShipmentLine.getElementsByTagName("OrderLine");
												String sUPCCode = "";
												String sExtnDepartment = "";
												String sExtnDivision = "";
												String sGiftFlag = "";
												String sGiftWrap = "";
												String sSize = "";
												String sColor = "";
												String sPrice = "";
												String sItemDescription = "";

												for (int l=0; l<nOrderLines.getLength(); l++) {
													org.w3c.dom.Element eleOrderLine = (org.w3c.dom.Element) nOrderLines.item(l);
													NodeList nItemAlias = eleOrderLine.getElementsByTagName("ItemAlias");
													for (int m=0; m<nItemAlias.getLength(); m++) {
														org.w3c.dom.Element eleItemAlias = (org.w3c.dom.Element) nItemAlias.item(m);
														if ("UPCCode".equalsIgnoreCase(eleItemAlias.getAttribute("AliasName")))
														{
															sUPCCode = eleItemAlias.getAttribute("AliasValue");
														}
													}
													org.w3c.dom.Element eleItemExtn = YRCXmlUtils.getXPathElement(eleOrderLine, "OrderLine/ItemDetails/Extn");
													if (!YRCPlatformUI.isVoid(eleItemExtn)) {	
														sExtnDepartment = eleItemExtn.getAttribute("ExtnDepartment");
														sExtnDivision = eleItemExtn.getAttribute("ExtnDepartment");
													}
													sGiftFlag = eleOrderLine.getAttribute("GiftFlag"); 
													sGiftWrap = eleOrderLine.getAttribute("GiftWrap");
													NodeList nAdditionalAttribute = eleOrderLine.getElementsByTagName("AdditionalAttribute");
													for (int n=0; n<nAdditionalAttribute.getLength(); n++) {
														org.w3c.dom.Element eleAdditionalAttribute = (org.w3c.dom.Element) nAdditionalAttribute.item(n);
														if ("Color".equalsIgnoreCase(eleAdditionalAttribute.getAttribute("Name")))
														{
															sColor = eleAdditionalAttribute.getAttribute("Value");
														}
														if ("Size".equalsIgnoreCase(eleAdditionalAttribute.getAttribute("Name")))
														{
															sSize = eleAdditionalAttribute.getAttribute("Value");
														}
													}

													org.w3c.dom.Element eleLinePriceInfo = YRCXmlUtils.getXPathElement(eleOrderLine, "OrderLine/LinePriceInfo");
													if (!YRCPlatformUI.isVoid(eleLinePriceInfo)) {	
														sPrice = eleLinePriceInfo.getAttribute("UnitPrice");
													}
													org.w3c.dom.Element eleItemPrimaryInfo = YRCXmlUtils.getXPathElement(eleOrderLine, "OrderLine/ItemDetails/PrimaryInformation");
													if (!YRCPlatformUI.isVoid(eleItemPrimaryInfo)) {	
														sItemDescription = eleItemPrimaryInfo.getAttribute("Description");
													}
												}

												Phrase pUPCCode = new Phrase(String.format(sUPCCode), fontCell);
												PdfPCell UPCCodeDataCell = new PdfPCell(); 				        
												UPCCodeDataCell.setPhrase(pUPCCode);

												//ItemID Data Cell
												String sItemID = null;
												sItemID = eleShipmentLine.getAttribute("ItemID");
												Phrase pItemID = new Phrase(String.format(sItemID), fontCell);
												PdfPCell ItemIDDataCell = new PdfPCell(); 				        
												ItemIDDataCell.setPhrase(pItemID);

												//Item Desc Data Cell
												Phrase pItemDesc = new Phrase(String.format(sItemDescription), fontCell);
												PdfPCell ItemDescDataCell = new PdfPCell(); 				        
												ItemDescDataCell.setPhrase(pItemDesc);

												//Original Quantity Data Cell
												String sQuantity = null;
												sQuantity = eleShipmentLine.getAttribute("Quantity");
												Phrase pOrgQty = new Phrase(String.format(sQuantity), fontCell);
												PdfPCell OrgQtyDataCell = new PdfPCell(); 				        
												OrgQtyDataCell.setPhrase(pOrgQty);

												//UOM Data Cell
												String sUnitOfMeasure = null;
												sUnitOfMeasure = eleShipmentLine.getAttribute("UnitOfMeasure");
												Phrase pUOM = new Phrase(String.format(sUnitOfMeasure), fontCell);
												PdfPCell UOMDataCell = new PdfPCell(); 				        
												UOMDataCell.setPhrase(pUOM);

												//Quantity To Pick Data Cell
												String sBackroomPickedQuantity = eleShipmentLine.getAttribute("BackroomPickedQuantity");
												String sOriginalQty = eleShipmentLine.getAttribute("Quantity");
												double dBackRoomPickedQty = 0.00;
												double dOriginalQty = 0.00;
												double dToPickQty = 0.00;
												String spickedQty = "NULL";
												if ((!sBackroomPickedQuantity.equals("")) && (!sBackroomPickedQuantity.equals(null))) 
												{
													dBackRoomPickedQty = Double.parseDouble(sBackroomPickedQuantity);
													dOriginalQty = Double.parseDouble(sOriginalQty);
													dToPickQty = dOriginalQty - dBackRoomPickedQty;					   							
													DecimalFormat df = new DecimalFormat("###.00");
													spickedQty = df.format(dToPickQty);											
												}
												else 
												{
													spickedQty = eleShipmentLine.getAttribute("Quantity");
												}

												Phrase pPickQty = new Phrase(String.format(spickedQty), fontCell);
												PdfPCell PickQtyDataCell = new PdfPCell(); 				        
												PickQtyDataCell.setPhrase(pPickQty);

												//Picked Quantity Data Cell
												Phrase pPickedQty = new Phrase(String.format("0.00"), fontCell);
												PdfPCell PickedQtyDataCell = new PdfPCell(); 				        
												PickedQtyDataCell.setPhrase(pPickedQty);

												//Department Data Cell
												Phrase pDepartment = new Phrase(String.format(sExtnDepartment), fontCell);
												PdfPCell DeptDataCell = new PdfPCell(); 				        
												DeptDataCell.setPhrase(pDepartment);

												//Division Data Cell
												Phrase pDivision  = new Phrase(String.format(sExtnDivision), fontCell);
												PdfPCell DivDataCell = new PdfPCell(); 				        
												DivDataCell.setPhrase(pDivision);

												//GiftBox Data Cell
												Phrase pGiftBox = new Phrase(String.format(sGiftFlag), fontCell);
												PdfPCell GiftBoxDataCell = new PdfPCell(); 				        
												GiftBoxDataCell.setPhrase(pGiftBox);

												//GiftWrap Data Cell
												Phrase pGiftWrap = new Phrase(String.format(sGiftWrap), fontCell);
												PdfPCell GiftWrapDataCell = new PdfPCell(); 				        
												GiftWrapDataCell.setPhrase(pGiftWrap);

												//Color Data Cell
												Phrase pColor = new Phrase(String.format(sColor), fontCell);
												PdfPCell ColorDataCell = new PdfPCell(); 				        
												ColorDataCell.setPhrase(pColor);

												//Size Data Cell
												Phrase pSize = new Phrase(String.format(sSize), fontCell);
												PdfPCell SizeDataCell = new PdfPCell(); 				        
												SizeDataCell.setPhrase(pSize);

												//Price Data Cell
												Phrase pPrice = new Phrase(String.format(sPrice), fontCell);
												PdfPCell PriceDataCell = new PdfPCell(); 				        
												PriceDataCell.setPhrase(pPrice);

												//Add Data cells to the table						     
												table.addCell(SLNoDataCell);
												table.addCell(UPCCodeDataCell);
												table.addCell(ItemIDDataCell);
												table.addCell(ItemDescDataCell);
												table.addCell(OrgQtyDataCell);
												table.addCell(UOMDataCell);
												table.addCell(PickQtyDataCell);
												table.addCell(PickedQtyDataCell);
												table.addCell(DeptDataCell);
												table.addCell(DivDataCell);
												table.addCell(GiftBoxDataCell);
												table.addCell(GiftWrapDataCell);
												table.addCell(ColorDataCell);
												table.addCell(SizeDataCell);
												table.addCell(PriceDataCell);		        				        
											}					
										}
										document.add(table);
									}
								}
							}
						}
					}		

					document.close();

					if ("win32".equals(pltform))
					{
						Program.launch(sDocumentPathAndName);
					}
				}
			}
		}
	 
	 	/**
		 * This method is implemented to fetch the store associate id 
		 */
		public String getUserID (){
			org.w3c.dom.Element eleUserNameSpace = this.getModel("UserNameSpace");
			String sUserID = null;
			if (!YRCPlatformUI.isVoid(eleUserNameSpace)) {			
				sUserID = eleUserNameSpace.getAttribute("Loginid");
			}
			return sUserID;
		}
	 
		/**
		 * This method is implemented to fetch the ship node 
		 */
	 public String getShipNode (){
			org.w3c.dom.Element eleUserNameSpace = this.getModel("UserNameSpace");
			String sShipNode = null;
			if (!YRCPlatformUI.isVoid(eleUserNameSpace)) {			
				sShipNode = eleUserNameSpace.getAttribute("ShipNode");
			}
			return sShipNode;
		}	 
	 
	 	/**
		 * This method is implemented to add title to the pick list
		 */
		private static void addTitle(com.itextpdf.text.Document document) throws Exception {		    
			Font fontParagraph = new Font(FontFamily.TIMES_ROMAN, 15, Font.BOLD, BaseColor.BLACK);
			Paragraph pName = new Paragraph("PICK TICKET", fontParagraph);
			pName.setAlignment(com.itextpdf.text.Element.ALIGN_CENTER);								
			document.add(pName);
			document.add(Chunk.NEWLINE);							
		}
}
