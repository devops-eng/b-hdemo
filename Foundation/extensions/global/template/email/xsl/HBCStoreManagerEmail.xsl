<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:lxslt="http://xml.apache.org/xslt" version="1.0">

<xsl:output method="xml" indent="yes" version="1.0"/>
   <xsl:template match="/">
      <HTML>
	     <xsl:comment>CONTENT_TYPE=text/html</xsl:comment>
         <HEAD>
		 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" style="font-family:Calibri;"/>
            <title>Store manager escalation email </title>
         </HEAD>

         <BODY topmargin="0" leftmargin="0">
            <xsl:apply-templates select="Store" />
         </BODY>
      </HTML>
   </xsl:template>

   <xsl:template match="Store">
   
	<xsl:choose>
         <xsl:when test="@ReportID='MissedSLA'" >
			<div style="margin-left:1em; font-family:Calibri;" >
				<p><font size="3" >Please be informed that your store <xsl:value-of select="@ShipNode" /> has <xsl:value-of select="@NoOfShipments" /> BOPUS shipment(s) that are past SLA. Please process these shipments immediately. </font></p>
			</div>
		</xsl:when>
		<xsl:otherwise>
			<div style="margin-left:1em; font-family:Calibri;">
				<p><font size="3" >Please be informed that your store <xsl:value-of select="@ShipNode" /> has <xsl:value-of select="@NoOfShipments" /> BOPUS shipments nearing SLA. Please process as soon as possible. </font></p>
			</div>
         </xsl:otherwise>
    </xsl:choose>


      <TABLE style="margin-left:1em; border: 1px solid #e0e0e0; font-family:Calibri;" border="1" WIDTH="90%" CELLSPACING="0" CELLPADDING="3" >
         <TR>
            <TD align="center" BGCOLOR="#cccccc" >
				<b>
                  <font size="3">Order Number</font>
				</b>
            </TD>
            <TD align="center" BGCOLOR="#cccccc" >
				<b>
                  <font size="3">Shipment Number</font>
				</b>
            </TD>
            <TD align="center" BGCOLOR="#cccccc" >
				<b>
                  <font size="3">Shipment status</font>
				</b>  
            </TD>
            <TD align="center" BGCOLOR="#cccccc" >
				<b>
                  <font size="3">Shipment due date and time</font> 
				</b>
            </TD>
            <TD align="center" BGCOLOR="#cccccc" >  
				<b>
                  <font size="3">Currently actioned by</font> 
				</b>
            </TD>
         </TR>

         <xsl:for-each select="ShipmentList/Shipment">

            <TR class="Items">
               <TD align="center">
                     <font size="3">
                        <xsl:value-of select="@OrderNo" />
                     </font>
               </TD>

               <TD align="center">
                     <font size="3">
                        <xsl:value-of select="@ShipmentNo" />
                     </font>
               </TD>
			   
			   <TD align="center">
                     <font size="3">
                        <xsl:value-of select="@StatusName" />
                     </font>
               </TD>
			   
			   <TD align="center">
                     <font size="3">
                        <xsl:value-of select="@ExpectedShipmentDate" />
                     </font>
               </TD>
			   
			   <TD align="center">
                     <font size="3">
                        <xsl:value-of select="@CurrentlyActionedBy" />
                     </font>
               </TD>

            </TR>
            </xsl:for-each>
  </TABLE>
  <div style="margin-left:1em; font-family:Calibri;">
				<p><font size="3" >This is an automated message sent by the Store Order Management (SOM) system.  Please do not reply to this message. For questions or issues regarding shipments in SOM please contact the Help Desk. </font></p>
			</div>
  <BR/>
  </xsl:template>
  </xsl:stylesheet>