<?xml version="1.0" encoding="utf-8" ?>
<!--
Licensed Materials - Property of IBM
IBM Sterling Selling and Fulfillment Suite - Foundation
(C) Copyright IBM Corp. 2011, 2012 All Rights Reserved.
US Government Users Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:java="http://xml.apache.org/xslt/java" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:fopUtil="com.yantra.pca.ycd.fop.YCDFOPUtils" exclude-result-prefixes="java" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:import href="/global/template/prints/ycd/xsl/HBCPackSlipCommonTemplates_FOP.xsl" />
	<xsl:output method="xml" indent="yes" />


	<xsl:variable name="locale">
		<xsl:value-of select="/Containers/@Locale" /> 
	</xsl:variable>

	<xsl:template match="/">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" >
			<fo:layout-master-set>
				<!--	<xsl:call-template name="A4-portrait_layout-master-set" />  -->
				<fo:simple-page-master master-name="Letter-portrait"  
				page-height="8.5in" page-width="11in" margin-top=".8cm"
					margin-bottom="1cm" margin-left=".2cm" margin-right=".8cm">
					<fo:region-body />
					<fo:region-before region-name="header-blank" extent="297mm"/>					
					<fo:region-after />
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="Letter-portrait">
				<!--	<xsl:call-template name="orderFooter" /> -->
				<fo:flow flow-name="xsl-region-body" >                    
					<!--	<fo:block>
						<xsl:apply-templates select="/Containers" >
							<xsl:with-param name="islast" select="position()=last()" />
						</xsl:apply-templates> 						
					</fo:block> -->
					<xsl:apply-templates select="/Containers" /> 
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>

	<xsl:variable name="OrderNo" select="/Containers/@OrderNo" />
	<xsl:variable name="FirstName" select="/Containers/PersonInfoBillTo/@FirstName" />
	<xsl:variable name="MiddleName" select="/Containers/PersonInfoBillTo/@MiddleName" />
	<xsl:variable name="LastName" select="/Containers/PersonInfoBillTo/@LastName" />
	<xsl:variable name="FullName">
		<xsl:value-of select="concat($FirstName,' ' ,$MiddleName,' ' ,$LastName)" />
	</xsl:variable>
	<xsl:variable name="AddressLine1" select="/Containers/PersonInfoBillTo/@AddressLine1" />
	<xsl:variable name="AddressLine2" select="/Containers/PersonInfoBillTo/@AddressLine2" />
	<xsl:variable name="AddressLine3" select="/Containers/PersonInfoBillTo/@AddressLine3" />
	<xsl:variable name="FullAddress">
		<xsl:value-of select="concat($AddressLine1,' ' ,$AddressLine2,' ',$AddressLine3)" />
	</xsl:variable>
	<xsl:variable name="City" select="/Containers/PersonInfoBillTo/@City" />
	<xsl:variable name="State" select="/Containers/PersonInfoBillTo/@State" />
	<xsl:variable name="ZipCode" select="/Containers/PersonInfoBillTo/@ZipCode" />
	<xsl:variable name="AddressLocation">
		<xsl:value-of select="concat($City,' ' ,$State,' ',$ZipCode)" />
	</xsl:variable>
	<xsl:variable name="FirstNameOne" select="/Containers/ToAddress/@FirstName" />
	<xsl:variable name="MiddleNameOne" select="/Containers/ToAddress/@MiddleName" />
	<xsl:variable name="LastNameOne" select="/Containers/ToAddress/@LastName" />
	<xsl:variable name="FullNameOne">
		<xsl:value-of select="concat($FirstNameOne,' ' ,$MiddleNameOne,' ' ,$LastNameOne)" />
	</xsl:variable>
	<xsl:variable name="AddressLine1One" select="/Containers/ToAddress/@AddressLine1" />
	<xsl:variable name="AddressLine2One" select="/Containers/ToAddress/@AddressLine2" />
	<xsl:variable name="AddressLine3One" select="/Containers/ToAddress/@AddressLine3" />
	<xsl:variable name="FullAddressOne">
		<xsl:value-of select="concat($AddressLine1One,' ' ,$AddressLine2One,' ',$AddressLine3One)" />
	</xsl:variable>
	<xsl:variable name="CityOne" select="/Containers/ToAddress/@City" />
	<xsl:variable name="StateOne" select="/Containers/ToAddress/@State" />
	<xsl:variable name="ZipCodeOne" select="/Containers/ToAddress/@ZipCode" />
	<xsl:variable name="AddressLocationOne">
		<xsl:value-of select="concat($CityOne,' ' ,$StateOne,' ',$ZipCodeOne)" />
	</xsl:variable>
	<xsl:variable name="Logo">
		<xsl:value-of select="/Containers/@Logo" />
	</xsl:variable>


	<xsl:template match="Containers">
		
		<xsl:for-each select="Container">
			<xsl:variable name="islast" select="position()=last()" />
			<xsl:call-template name="addEmptyBlock" />
			<xsl:call-template name="addEmptyBlock" />
			<xsl:call-template name="addEmptyBlock" />
			<xsl:call-template name="addEmptyBlock" />
			<!-- ShipmentLine Header Start -->
			<fo:table background-color="White" table-layout="fixed" width="100%" >
				<fo:table-column column-width="2%" />
				<fo:table-column column-width="23%" />
				<fo:table-column column-width="9%" />
				<fo:table-column column-width="16%" />
				<fo:table-column column-width="18%" />
				<fo:table-column column-width="32%" />
				

				<fo:table-body>

					<!-- Cell 1 -->
					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>

					<!-- Cell 2 -->
					<fo:table-cell padding="1.50mm">
						<fo:block>
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="100%" />
								<fo:table-body>
									<fo:table-row >
										<fo:table-cell padding="0.30mm">
											<fo:block />
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell padding="0.30mm">
											<fo:block />
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell padding="0.30mm">
											<fo:block />
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell padding="0.30mm">
											<fo:block />
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell padding="0.30mm">
											<fo:block />
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell padding="0.30mm">
											<fo:block text-align="left"  font-family="Arial" font-size="9pt" padding=".050mm">
												<xsl:value-of select="$FullName" />																						
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell padding=".30mm">
											<fo:block text-align="left"  font-family="Arial" font-size="9pt" padding=".050mm">
												<xsl:value-of select="$FullAddress" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell padding=".30mm">
											<fo:block text-align="left"  font-family="Arial" font-size="9pt" padding=".050mm">
												<xsl:value-of select="$AddressLocation" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>

					<!-- Cell 3 -->
					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>

					<!-- Cell 4 -->
					<fo:table-cell padding="1.5mm">
						<fo:block>
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="100%" />
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="0.30mm">
											<fo:block />
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell padding="0.30mm">
											<fo:block />
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell padding="0.30mm">
											<fo:block />
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell padding="0.30mm">
											<fo:block />
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell padding="0.30mm">
											<fo:block />
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell>
											<fo:block text-align="left"  font-family="Arial" font-size="9pt" padding=".050mm">
												<xsl:value-of select="$FullNameOne" />																						
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell>
											<fo:block text-align="left" font-family="Arial" font-size="9pt" padding=".050mm">
												<xsl:value-of select="$FullAddressOne" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell>
											<fo:block text-align="left"  font-family="Arial" font-size="9pt" padding=".10mm">
												<xsl:value-of select="$AddressLocationOne" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>

								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
					
					<!-- Cell 5 -->
					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>
					
					
					<!-- Cell 6 -->
					<fo:table-cell padding="1.5mm">
						<fo:block>
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="50%" />
								<fo:table-column column-width="50%" />
								<fo:table-body>
									<fo:table-row >
										<fo:table-cell >
											<fo:block />
										</fo:table-cell>
										<fo:table-cell >
											<fo:block />
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell >
											<fo:block />
										</fo:table-cell>
										<fo:table-cell >
											<fo:block />
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell >
											<fo:block />
										</fo:table-cell>
										<fo:table-cell>
											<fo:block text-align="right"  font-family="Arial" font-size="9pt" padding=".050mm">
												<xsl:value-of select="$OrderNo" />																						
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell>
											<fo:block text-align="Left" font-family="Arial" font-size="9pt" padding=".050mm">
												<xsl:value-of select="fopUtil:getLocalizedString('Customer #/No de client:',$locale)" />
											</fo:block>
										</fo:table-cell>
										<fo:table-cell>
											<fo:block text-align="right" font-family="Arial" font-size="9pt" padding=".050mm">
												<xsl:value-of select="/Containers/PersonInfoBillTo/@EMailID" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell>
											<fo:block text-align="Left" font-family="Arial" font-size="9pt" padding=".050mm">
												<xsl:value-of select="fopUtil:getLocalizedString('Order Date/Date de la commande',$locale)" />
											</fo:block>
										</fo:table-cell>
										<fo:table-cell>
											<fo:block text-align="right"  font-family="Arial" font-size="9pt" padding=".10mm">
												<xsl:value-of select="/Containers/@Date" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell>
											<fo:block text-align="Left" font-family="Arial" font-size="9pt" padding=".050mm">
												<xsl:value-of select="fopUtil:getLocalizedString('T/N: ',$locale)" />
											</fo:block>
										</fo:table-cell>
										<fo:table-cell>
											<fo:block text-align="right"  font-family="Arial" font-size="9pt" padding=".10mm">
												<xsl:value-of select="/Containers/@TransactionId" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell>
											<fo:block text-align="left"  font-family="Arial" font-size="9pt" padding=".10mm">
												<xsl:value-of select="/Containers/@CarrierServiceCode" />
											</fo:block>
										</fo:table-cell>
										<fo:table-cell >
											<fo:block />
										</fo:table-cell>
									</fo:table-row>
									

								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
					
				</fo:table-body>
			</fo:table>			

			<xsl:call-template name="addEmptyBlock" />
			<xsl:call-template name="addEmptyBlock" />
			<xsl:call-template name="addEmptyBlock" />
			<xsl:call-template name="addEmptyBlock" />
			
			<xsl:apply-templates select="OrderLines" />
			

			<xsl:call-template name="addEmptyBlock" />
			<xsl:call-template name="addEmptyBlock" />
			<xsl:call-template name="addEmptyBlock" />
			<xsl:call-template name="addEmptyBlock" />
			<xsl:call-template name="addEmptyBlock" />
			<xsl:call-template name="addEmptyBlock" />
			<xsl:call-template name="addEmptyBlock" />
			<xsl:call-template name="PriceBlock" >
				<xsl:with-param name="ContainerSubTotal" select="@SubTotal"/>
				<xsl:with-param name="ContainerShippingTotal" select="@ContainerShippingTotal"/>
				<xsl:with-param name="ContainerTaxTotal" select="@ContainerTaxTotal"/>
				<xsl:with-param name="ContainerTotal" select="@ContainerTotal"/>
			</xsl:call-template>
			<xsl:call-template name="addEmptyBlock" />
			<xsl:call-template name="addEmptyBlock" />
			<xsl:call-template name="addEmptyBlock" />
			<xsl:call-template name="ReturnBarCode" />	
		
			<xsl:call-template name="addEmptyBlock" />
			<xsl:call-template name="addEmptyBlock" />
			<xsl:call-template name="addEmptyBlock" />
			<xsl:call-template name="addEmptyBlock" />
			<xsl:call-template name="ShipFromAdress" />	
		
			<xsl:call-template name="addEmptyBlock" />
			<xsl:call-template name="ShipToAdress" />	
			<xsl:call-template name="addOrderNoBarCode" />
		
		
	
			<!--<xsl:if test="@IsLastContainer!='Y'">
				<fo:block break-before="page"/>
			 </xsl:if>-->

			<xsl:if test="$islast!='true'" >
				<fo:block break-before="page"/>
			</xsl:if>
		</xsl:for-each>

		<fo:block id="last-page" /> 
	</xsl:template>





	<!-- ############# OrderLines: Start ############### -->

	<xsl:template match="OrderLines">		

		<fo:table width="100%" height="1.7in"  table-layout="fixed" >		

			<!-- UPC  -->
			<fo:table-column column-width="15%" />
			<!-- Item Desc -->
			<fo:table-column column-width="20%" />
			<!-- Color -->
			<fo:table-column column-width="10%" />
			<!-- Size-->
			<fo:table-column column-width="8%" />
			<!-- Qty -->
			<fo:table-column column-width="7%" />
			<!-- Price -->
			<fo:table-column column-width="9%" />
			<!-- Gift Box -->
			<fo:table-column column-width="9%" />
			<!-- Gift Box$   -->
			<fo:table-column column-width="9%" />
			<!-- Unknown   -->
			<fo:table-column column-width="4%" />
			<!-- Total   -->
			<fo:table-column column-width="9%" />

			<fo:table-body>
		
				<xsl:for-each select="OrderLine" >               
								

					<fo:table-row  visibility="hidden"  border-color="white" font-size="7pt" >
						<!-- UPC# -->
						<fo:table-cell padding=".20mm" >
							<fo:block text-align="center" font-family="Lucida">
								<fo:block>
									<xsl:value-of select="ItemDetails/ItemAliasList/ItemAlias [@AliasName='ACTIVE_UPC']/@AliasValue" />
								</fo:block>
							</fo:block>
						</fo:table-cell>						

						<!-- Item Desc # -->
						<fo:table-cell padding=".20mm" >
							<fo:block text-align="center" font-family="Lucida">
								<xsl:value-of select="ItemDetails/PrimaryInformation/@ShortDescription" />
								/<xsl:value-of select="ItemDetails/ItemLocaleList/ItemLocale [@Country='CA' and @Language='fr']/PrimaryInformation/@ShortDescription" />				
							</fo:block>
						</fo:table-cell>
						
						<!-- Color # -->
						<fo:table-cell padding=".20mm"  >
							<fo:block text-align="center" font-family="Lucida">
								<xsl:value-of
									select="ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Color']/@Value" />
								/<xsl:value-of
									select="ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Color']/@FrenchShortDesc" />						
							</fo:block>
						</fo:table-cell>
						
						<!-- Size # -->
						<fo:table-cell padding=".20mm"  >
							<fo:block text-align="center" font-family="Lucida">
								<xsl:value-of
									select="ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Size']/@Value" />
								/<xsl:value-of
									select="ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Size']/@FrenchShortDesc" />							
							</fo:block>
						</fo:table-cell>

						

						<!--QTY  -->
						<fo:table-cell padding=".20mm"  >
							<fo:block text-align="center" font-family="Lucida" >
								<xsl:value-of select="@OrderLinePackQty" />
							</fo:block>
						</fo:table-cell>

						<!--Unit Price-->
						<fo:table-cell padding=".20mm"  >
							<fo:block text-align="center" font-family="Lucida" >
								<xsl:choose>
									<xsl:when test="LinePriceInfo/@UnitPrice!=''">
										<xsl:value-of select="fopUtil:getLocalizedString('&#36;&#x20;',$locale)" />
										<xsl:value-of select="LinePriceInfo/@UnitPrice" />							
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="LinePriceInfo/@UnitPrice" />
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>
						</fo:table-cell>

						<!--Gift Box  -->
						<fo:table-cell padding=".20mm"  >
							<fo:block text-align="center" font-family="Lucida" >
								<xsl:value-of select="@GiftFlag" />
							</fo:block>
						</fo:table-cell>

						<!--Gift Box $  -->
						<fo:table-cell padding=".20mm"  >
							<fo:block text-align="center" font-family="Lucida" >
								<xsl:value-of select="fopUtil:getLocalizedString('',$locale)" />
							</fo:block>
						</fo:table-cell>
						
						<!--Unknown  -->
						<fo:table-cell padding=".20mm"  >
							<fo:block text-align="center" font-family="Lucida" >
								<xsl:value-of select="fopUtil:getLocalizedString('',$locale)" />
							</fo:block>
						</fo:table-cell>

						<!-- OrderLine Total  -->
						<fo:table-cell padding=".20mm"  >
							<fo:block text-align="center" font-family="Lucida" >									
								<xsl:choose>
									<xsl:when test="@OrderLineTotalPrice!=''">
										<xsl:value-of select="fopUtil:getLocalizedString('&#36;&#x20;',$locale)" />
										<xsl:value-of select="@OrderLineTotalPrice" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="@OrderLineTotalPrice" />
										<xsl:call-template name="addEmptyBlock" />
									</xsl:otherwise>
								</xsl:choose>								
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

				</xsl:for-each>
			</fo:table-body>
		</fo:table>	


	</xsl:template>
	

	<xsl:template name="PriceBlock">
		<xsl:param name="ContainerSubTotal" />  
		<xsl:param name="ContainerShippingTotal" />
		<xsl:param name="ContainerTaxTotal" />
		<xsl:param name="ContainerTotal" />
		<!-- OrderDetails -->	
		<fo:table width="100%">
			<fo:table-column column-width="10%" />
			<fo:table-column column-width="35%" />
			<fo:table-column column-width="5%" />
			<fo:table-column column-width="5%" />
			<fo:table-column column-width="45%" />

			<fo:table-body>			
				<fo:table-row >
					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>
					
					<fo:table-cell padding="1.50mm">
						<fo:block>
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="100%" />
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell>
											<fo:block text-align="left"  font-family="Arial" font-size="9pt" padding=".050mm">
												<xsl:value-of select="fopUtil:getLocalizedString('',$locale)" />																						
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell>
											<fo:block text-align="left"  font-family="Arial" font-size="9pt" padding=".050mm">
												<xsl:value-of select="fopUtil:getLocalizedString('',$locale)" />																						
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell>
											<fo:block text-align="left"  font-family="Arial" font-size="9pt" padding=".050mm">
												<xsl:value-of select="/Containers/Container/GiftMessages/@ContainerGiftMessageOne" />																						
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>

					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>
					
					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>
					
					<fo:table-cell padding=".50mm">
						<fo:block>
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="52%" />
								<fo:table-column column-width="3%" />
								<fo:table-column column-width="45%" />
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding=".30mm" >
											<fo:block text-align="left" font-size="7pt" font-family="Lucida">
												<xsl:value-of select="fopUtil:getLocalizedString('Total Merchandise in this Package/Total Marchandise pour cet envoie :',$locale)" />
											</fo:block>
										</fo:table-cell>
										<fo:table-cell empty-cells="hide">
											<fo:block />
										</fo:table-cell>
										<fo:table-cell padding=".30mm" >
											<fo:block text-align="right" font-family="Lucida" font-size="7pt" >												
												<xsl:choose>
													<xsl:when test="$ContainerTotal!=''">
														<xsl:value-of select="fopUtil:getLocalizedString('&#36;&#x20;',$locale)" />
														<xsl:value-of select="$ContainerTotal" />							
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="$ContainerTotal" />
													</xsl:otherwise>
												</xsl:choose>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>

									<fo:table-row>
										<fo:table-cell padding=".30mm" >
											<fo:block text-align="left" font-size="7pt"  font-family="Lucida">
												<xsl:value-of select="fopUtil:getLocalizedString('Shipping/Expedition:',$locale)" />
											</fo:block>

										</fo:table-cell>
										<fo:table-cell empty-cells="hide">
											<fo:block />
										</fo:table-cell>
										<fo:table-cell padding=".30mm" >
											<fo:block text-align="right" font-size="7pt" font-family="Lucida" >								
												<xsl:choose>
													<xsl:when test="$ContainerShippingTotal!=''">
														<xsl:value-of select="fopUtil:getLocalizedString('&#36;&#x20;',$locale)" />
														<xsl:value-of select="$ContainerShippingTotal" />							
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="$ContainerShippingTotal" />
													</xsl:otherwise>
												</xsl:choose>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>

									<fo:table-row>
										<fo:table-cell padding=".30mm" >
											<fo:block text-align="left" font-size="7pt" font-family="Lucida">
												<xsl:value-of select="fopUtil:getLocalizedString('TPS/GST(UnknowString):',$locale)" />
											</fo:block>
										</fo:table-cell>
										<fo:table-cell empty-cells="hide">
											<fo:block />
										</fo:table-cell>
										<fo:table-cell padding=".30mm">
											<fo:block text-align="right" font-family="Lucida" font-size="7pt">												
												<xsl:choose>
													<xsl:when test="$ContainerTaxTotal!=''">
														<xsl:value-of select="fopUtil:getLocalizedString('&#36;&#x20;',$locale)" />
														<xsl:value-of select="$ContainerTaxTotal" />							
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="$ContainerTaxTotal" />
													</xsl:otherwise>
												</xsl:choose>												
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell padding=".30mm" >
											<fo:block text-align="left" font-size="7pt" font-family="Lucida">
												<xsl:value-of select="fopUtil:getLocalizedString('TVQ/QST(UnknowString):',$locale)" />
											</fo:block>
										</fo:table-cell>
										<fo:table-cell empty-cells="hide">
											<fo:block />
										</fo:table-cell>
										<fo:table-cell padding=".30mm" >
											<fo:block text-align="right" font-family="Lucida" font-size="7pt">												
												<xsl:value-of select="fopUtil:getLocalizedString('TBD ',$locale)" />								
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell padding=".30mm" >
											<fo:block text-align="left" font-size="7pt" font-family="Lucida">
												<xsl:value-of select="fopUtil:getLocalizedString('Authorization #/No Autorisation:',$locale)" />
											</fo:block>
										</fo:table-cell>
										<fo:table-cell empty-cells="hide">
											<fo:block />
										</fo:table-cell>
										<fo:table-cell padding=".30mm" >
											<fo:block text-align="right" font-family="Lucida" font-size="7pt">												
												<xsl:value-of select="/Containers/@AuthorizationID" />									
											</fo:block>
										</fo:table-cell>
									</fo:table-row>

									
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
			</fo:table-body>
		</fo:table>
		
	</xsl:template>

	<xsl:template name="ReturnBarCode">
		<xsl:variable name="ReturnBarCodeOrderNumber" select="/Containers/@ReturnTrackingNo" />

		<fo:table width="100%">
			<fo:table-column column-width="18%" />
			<fo:table-column column-width="7%" />
			<fo:table-column column-width="40%" />
			<fo:table-column column-width="35%" />

			<fo:table-body>
				<fo:table-row >
					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>
					
					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>
					

					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>

					<fo:table-cell padding="1.5mm" border-width=".15pt">
						<fo:block text-align-last="left">
							<fo:instream-foreign-object>
								<barcode:barcode xmlns:barcode="http://barcode4j.krysalis.org/ns" message="{$ReturnBarCodeOrderNumber}">
									<barcode:code128>
										<barcode:height>12mm</barcode:height>
										<barcode:width>55mm</barcode:width>
										<module-width>.35mm</module-width>
										<human-readable>
											<placement>none</placement>
											<pattern></pattern>
										</human-readable>
									</barcode:code128>
								</barcode:barcode>
							</fo:instream-foreign-object>
						</fo:block>
					</fo:table-cell>

				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	
	
	<xsl:template name="ShipFromAdress">

		<fo:table width="100%">
			<fo:table-column column-width="10%" />
			<fo:table-column column-width="50%" />
			<fo:table-column column-width="40%" />

			<fo:table-body>
				<fo:table-row padding=".10mm">

					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>
					<fo:table-cell padding="1.5mm">
						<fo:block>
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="100%" />
								<fo:table-body>
									<fo:table-row >
										<fo:table-cell>
											<fo:block text-align="left"  font-family="Arial" font-size="9pt" padding=".050mm">
												<xsl:value-of select="$FullNameOne" />																						
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell>
											<fo:block text-align="left" font-family="Arial" font-size="9pt" padding=".050mm">
												<xsl:value-of select="$FullAddressOne" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell>
											<fo:block text-align="left"  font-family="Arial" font-size="9pt" padding=".10mm">
												<xsl:value-of select="$AddressLocationOne" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>

								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>


				</fo:table-row>
			</fo:table-body>
		</fo:table>
		

	</xsl:template>

	
	<xsl:template name="ShipToAdress">

		<fo:table width="100%">
			<fo:table-column column-width="10%" />
			<fo:table-column column-width="50%" />
			<fo:table-column column-width="40%" />

			<fo:table-body>
				<fo:table-row padding=".10mm">

					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>
					<fo:table-cell padding="1.5mm">
						<fo:block>
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="100%" />
								<fo:table-body>
									<fo:table-row >
										<fo:table-cell>
											<fo:block text-align="left"  font-family="Arial" font-size="9pt" padding=".050mm">
												<xsl:value-of select="/Containers/ReturnStoreAddress/@ReturnAddressLine1" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell>
											<fo:block text-align="left"  font-family="Arial" font-size="9pt" padding=".050mm">
												<xsl:value-of select="/Containers/ReturnStoreAddress/@ReturnAddressLine2" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell>
											<fo:block text-align="left"  font-family="Arial" font-size="9pt" padding=".050mm">
												<xsl:value-of select="/Containers/ReturnStoreAddress/@ReturnAddressLine3" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>

								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>


				</fo:table-row>
			</fo:table-body>
		</fo:table>
		

	</xsl:template>


	<xsl:template name="addOrderNoBarCode">
		<xsl:variable name="OrderNoBarCode" select="/Containers/@OrderNo" />

		<fo:table width="100%">
			<fo:table-column column-width="30%" />
			<fo:table-column column-width="40%" />
			<fo:table-column column-width="30%" />

			<fo:table-body>

				<fo:table-row font-size="7pt" padding=".050mm">
					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>
					<fo:table-cell padding=".10mm" border-width=".15pt">
						<fo:block text-align-last="left" padding="1mm">
							<fo:instream-foreign-object>
								<barcode:barcode xmlns:barcode="http://barcode4j.krysalis.org/ns" message="{$OrderNoBarCode}">
									<barcode:code128>
										<barcode:height>12mm</barcode:height>
										<barcode:width>58mm</barcode:width>
										<module-width>.38mm</module-width>										
									</barcode:code128>
								</barcode:barcode>
							</fo:instream-foreign-object>
						</fo:block>
					</fo:table-cell>

					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>					
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	

	<xsl:template name="addEmptyBlock">
		<xsl:param name="hightinpt" />
		<fo:table width="100%">
			<fo:table-column column-width="100%" />
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell>
						<fo:block white-space-treatment="preserve" linefeed-treatment="preserve">
							<xsl:text> &#x00A0;</xsl:text>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>



	<!-- PackSlip -->


</xsl:stylesheet>