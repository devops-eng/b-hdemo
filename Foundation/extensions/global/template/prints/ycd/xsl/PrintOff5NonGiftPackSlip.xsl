<?xml version="1.0" encoding="utf-8" ?>
<!--
Licensed Materials - Property of IBM
IBM Sterling Selling and Fulfillment Suite - Foundation
(C) Copyright IBM Corp. 2011, 2012 All Rights Reserved.
US Government Users Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:java="http://xml.apache.org/xslt/java" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:fopUtil="com.yantra.pca.ycd.fop.YCDFOPUtils" exclude-result-prefixes="java" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:import href="/global/template/prints/ycd/xsl/HBCPackSlipCommonTemplates_FOP.xsl"/>
	<xsl:output method="xml" indent="yes" />


	<xsl:variable name="locale">
		<xsl:value-of select="/Containers/@Locale" /> 
	</xsl:variable>

	<xsl:template match="/">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
			<fo:layout-master-set>
				<!--	<xsl:call-template name="A4-portrait_layout-master-set" />  -->
				<fo:simple-page-master master-name="Legal-portrait"  
				page-height="14in" page-width="8.5in" margin-top=".3cm"
					margin-bottom="1cm" margin-left="1cm" margin-right=".8cm">
					<fo:region-body />
					<fo:region-before region-name="header-blank" extent="378mm"/>					
					<fo:region-after />
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="Legal-portrait">
				<!--	<xsl:call-template name="orderFooter" /> -->
				<fo:flow flow-name="xsl-region-body" >                    
					<!--	<fo:block>
						<xsl:apply-templates select="/Containers" >
							<xsl:with-param name="islast" select="position()=last()" />
						</xsl:apply-templates> 						
					</fo:block> -->
					<xsl:apply-templates select="/Containers" /> 
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>

	<xsl:variable name="OrderNo" select="/Containers/@OrderNo" />
	<xsl:variable name="FirstName" select="/Containers/PersonInfoBillTo/@FirstName" />
	<xsl:variable name="MiddleName" select="/Containers/PersonInfoBillTo/@MiddleName" />
	<xsl:variable name="LastName" select="/Containers/PersonInfoBillTo/@LastName" />
	<xsl:variable name="FullName">
		<xsl:value-of select="concat($FirstName,' ' ,$LastName)" />
	</xsl:variable>
	<xsl:variable name="AddressLine1" select="/Containers/PersonInfoBillTo/@AddressLine1" />
	<xsl:variable name="AddressLine2" select="/Containers/PersonInfoBillTo/@AddressLine2" />
	<xsl:variable name="AddressLine3" select="/Containers/PersonInfoBillTo/@AddressLine3" />
	<xsl:variable name="FullAddress">
		<xsl:value-of select="concat($AddressLine1,' ' ,$AddressLine2,' ',$AddressLine3)" />
	</xsl:variable>
	<xsl:variable name="City" select="/Containers/PersonInfoBillTo/@City" />
	<xsl:variable name="State" select="/Containers/PersonInfoBillTo/@State" />
	<xsl:variable name="ZipCode" select="/Containers/PersonInfoBillTo/@ZipCode" />
	<xsl:variable name="AddressLocation">
		<xsl:value-of select="concat($City,' ' ,$State,' ',$ZipCode)" />
	</xsl:variable>
	<xsl:variable name="FirstNameOne" select="/Containers/ToAddress/@FirstName" />
	<xsl:variable name="MiddleNameOne" select="/Containers/ToAddress/@MiddleName" />
	<xsl:variable name="LastNameOne" select="/Containers/ToAddress/@LastName" />
	<xsl:variable name="FullNameOne">
		<xsl:value-of select="concat($FirstNameOne,' ' ,$LastNameOne)" />
	</xsl:variable>
	<xsl:variable name="AddressLine1One" select="/Containers/ToAddress/@AddressLine1" />
	<xsl:variable name="AddressLine2One" select="/Containers/ToAddress/@AddressLine2" />
	<xsl:variable name="AddressLine3One" select="/Containers/ToAddress/@AddressLine3" />
	<xsl:variable name="FullAddressOne">
		<xsl:value-of select="concat($AddressLine1One,' ' ,$AddressLine2One,' ',$AddressLine3One)" />
	</xsl:variable>
	<xsl:variable name="CityOne" select="/Containers/ToAddress/@City" />
	<xsl:variable name="StateOne" select="/Containers/ToAddress/@State" />
	<xsl:variable name="ZipCodeOne" select="/Containers/ToAddress/@ZipCode" />
	<xsl:variable name="AddressLocationOne">
		<xsl:value-of select="concat($CityOne,' ' ,$StateOne,' ',$ZipCodeOne)" />
	</xsl:variable>
	<xsl:variable name="Logo">
		<xsl:value-of select="/Containers/@Logo" />
	</xsl:variable>


	<xsl:template match="Containers">
		
		<xsl:for-each select="Container">
			<xsl:variable name="islast" select="position()=last()" />
			<!-- ShipmentLine Header Start -->
			<fo:table background-color="White" table-layout="fixed" width="100%" >
				<fo:table-column column-width="2%" />
				<fo:table-column column-width="15%" />
				<fo:table-column column-width="2%" />
				<fo:table-column column-width="19%" />
				<fo:table-column column-width="8%" />
				<fo:table-column column-width="31%" />
				<fo:table-column column-width="5%" />
				<fo:table-column column-width="15%" />
				<fo:table-column column-width="3%" />

				<fo:table-body>

					<!-- Cell 1 -->
					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>

					<!-- Cell 2 -->
					<fo:table-cell>
						<fo:block>
							<fo:external-graphic src="{$Logo}" content-height="scale-to-fit" content-width="1.1in"/>
						</fo:block>
					</fo:table-cell>

					<!-- Cell 3 -->
					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>

					<!-- Cell 4  :Start-->
					<fo:table-cell font-family="Lucida" font-size="10pt" text-align="left">
						<fo:block text-align="justify">

							<fo:table table-layout="fixed" width="100%">
								<fo:table-body>				

									<fo:table-row>
										<fo:table-cell padding="3mm">
											<fo:block text-align="justify">
												<fo:table background-color="White" table-layout="fixed" width="100%">
													<fo:table-column column-width="35%" />
													<fo:table-column column-width="65%" />
													<fo:table-body>

														<fo:table-row>
															<fo:table-cell>
																<fo:block text-align="left">
																	<xsl:value-of select="fopUtil:getLocalizedString('Order #',$locale)" />
																</fo:block>
															</fo:table-cell>
															<fo:table-cell>
																<fo:block text-align="left">
																	<xsl:value-of select="$OrderNo" />
																</fo:block>
															</fo:table-cell>
														</fo:table-row>
													</fo:table-body>
												</fo:table>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
					<!-- Cell 4  :End -->
					<!-- Cell 5 -->
					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>
					<!-- Cell 6 -->
					<fo:table-cell>
						<fo:block text-align="left">
							<fo:table background-color="White" table-layout="fixed" width="100%" font-size="7pt">
								<fo:table-body>
									<fo:table-row >
										<fo:table-cell>
											<fo:block text-align="left" padding="1mm">
												<xsl:value-of select="fopUtil:getLocalizedString('Packed with Pride for You by:',$locale)" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>

									<fo:table-row >
										<fo:table-cell>
											<fo:block text-align="left" padding="2.50mm">
												<xsl:value-of select="fopUtil:getLocalizedString('_______________________________',$locale)" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
					<!-- Cell 7 -->
					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>
					<!-- Cell 8 -->
					<fo:table-cell>
						<fo:block text-align="left">
							<fo:table background-color="White" table-layout="fixed" width="100%" font-size="7pt">
								<fo:table-body>
									<fo:table-row >
										<fo:table-cell padding="1mm">
											<fo:block text-align="left" font-size="7pt" font-family="sans-serif"
														background-color="White" white-space-collapse="false">				
												<xsl:value-of select="fopUtil:getLocalizedString('Page ',$locale)" />
												<fo:page-number />
												<xsl:value-of select="fopUtil:getLocalizedString(' of ',$locale)" /> 
												<fo:page-number-citation ref-id="last-page" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell padding=".70mm">
											<fo:block  font-size="7pt">
												<fo:table table-layout="fixed" width="100%">
													<fo:table-column column-width="30%" />
													<fo:table-column column-width="70%" />
													<fo:table-body>
														<fo:table-row>
															<fo:table-cell padding=".30mm">
																<fo:block text-align="left">
																	<xsl:value-of select="fopUtil:getLocalizedString('Date :',$locale)" />
																</fo:block>
															</fo:table-cell >
															<fo:table-cell padding=".30mm">
																<fo:block text-align="left">
																	<xsl:value-of select="/Containers/@Date"/>
																</fo:block>
															</fo:table-cell>
														</fo:table-row>
													</fo:table-body>
												</fo:table>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
					<!-- Cell 9 -->
					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>
				</fo:table-body>
			</fo:table>			

			<xsl:apply-templates select="OrderLines" >
				<xsl:with-param name="ContainerSubTotal" select="@SubTotal"/>
				<xsl:with-param name="ContainerShippingTotal" select="@ContainerShippingTotal"/>
				<xsl:with-param name="ContainerTaxTotal" select="@ContainerTaxTotal"/>
				<xsl:with-param name="ContainerTotal" select="@ContainerTotal"/>
			</xsl:apply-templates>

			<!--<xsl:if test="@IsLastContainer!='Y'">
				<fo:block break-before="page"/>
			 </xsl:if>-->

			<xsl:if test="$islast!='true'" >
				<fo:block break-before="page"/>
			</xsl:if>
		</xsl:for-each>

		<fo:block id="last-page" /> 
	</xsl:template>





	<!-- ############# OrderLines: Start ############### -->

	<xsl:template match="OrderLines">		

		<xsl:param name="ContainerSubTotal" />  
		<xsl:param name="ContainerShippingTotal" />
		<xsl:param name="ContainerTaxTotal" />
		<xsl:param name="ContainerTotal" />
		<fo:table border-width=".40mm" border-style="solid" border-color="black" width="100%" height="1.7in"  border-collapse="collapse" visibility="collapse"   border="solid 0mm" table-layout="fixed" >		

			<!-- Line  -->
			<fo:table-column column-width="4%" />
			<!-- Item Desc -->
			<fo:table-column column-width="26%" />
			<!-- Color and Size -->
			<fo:table-column column-width="17%" />
			<!-- UPC-->
			<fo:table-column column-width="12%" />
			<!-- Unit Price -->
			<fo:table-column column-width="7%" />
			<!-- Discount -->
			<fo:table-column column-width="19%" />
			<!-- Qty -->
			<fo:table-column column-width="5%" />
			<!-- Total Price   -->
			<fo:table-column column-width="10%" />

			<fo:table-body>
				<fo:table-row background-color="#CCCCFF" font-size="8pt" border="solid 0.1mm ">				

					<fo:table-cell border-width=".10mm" border-style="solid" border-color="black" padding=".5mm">
						<fo:block text-align="center" font-family="Lucida">
							<xsl:value-of select="fopUtil:getLocalizedString('Line',$locale)" />
						</fo:block>
					</fo:table-cell>

					<fo:table-cell border-width=".10mm" border-style="solid" border-color="black" padding=".5mm">
						<fo:block text-align="center" font-family="Lucida">
							<xsl:value-of select="fopUtil:getLocalizedString('Item Desc',$locale)" />
						</fo:block>
					</fo:table-cell>

					<fo:table-cell border-width=".10mm" border-style="solid" border-color="black" padding=".5mm">
						<fo:block text-align="center" font-family="Lucida">
							<fo:table table-layout="fixed">
								<fo:table-column column-width="60%" />
								<fo:table-column column-width="40%" />
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding=".025mm" border-width=".10mm">
											<fo:block text-align="left" font-family="Lucida">
												<xsl:value-of select="fopUtil:getLocalizedString('Color',$locale)" />
											</fo:block>
										</fo:table-cell>

										<fo:table-cell padding=".025mm" border-width=".10mm">
											<fo:block text-align="left" font-family="Lucida">
												<xsl:value-of select="fopUtil:getLocalizedString('Size',$locale)" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>

					<fo:table-cell border-width=".2mm" border-style="solid" border-color="black" padding=".5mm">
						<fo:block text-align="center" font-family="Lucida" >
							<xsl:value-of select="fopUtil:getLocalizedString('UPC',$locale)" />
						</fo:block>
					</fo:table-cell>

					<fo:table-cell border-width=".2mm" border-style="solid" border-color="black" padding=".5mm">
						<fo:block text-align="center" font-family="Lucida" >
							<xsl:value-of select="fopUtil:getLocalizedString('Unit Price',$locale)" />
						</fo:block>
					</fo:table-cell>

					<fo:table-cell border-width=".2mm" border-style="solid" border-color="black" padding=".5mm">
						<fo:block text-align="center" font-family="Lucida" >
							<xsl:value-of select="fopUtil:getLocalizedString('Discount',$locale)" />
						</fo:block>
					</fo:table-cell>

					<fo:table-cell border-width=".2mm" border-style="solid" border-color="black" padding=".5mm">
						<fo:block text-align="center" font-family="Lucida" >
							<xsl:value-of select="fopUtil:getLocalizedString('QTY',$locale)" />
						</fo:block>
					</fo:table-cell>

					<fo:table-cell border-width=".2mm" border-style="solid" border-color="black" padding=".5mm">
						<fo:block text-align="center" font-family="Lucida" >
							<xsl:value-of select="fopUtil:getLocalizedString('Total Price',$locale)" />
						</fo:block>
					</fo:table-cell>

				</fo:table-row>                



				<xsl:for-each select="OrderLine" >               
					<!--Row 1 -->				

					<fo:table-row  visibility="hidden"  border-color="white" font-size="7pt" border-style="dotted">
						<!-- 1LINE# -->
						<fo:table-cell padding=".20mm" border-width=".20mm" border-style="solid" border-color="black">
							<fo:block text-align="center" font-family="Lucida">
								<fo:block>
									<!-- <xsl:value-of select="@PrimeLineNo" /> -->
									<xsl:value-of select="@SerialNo" />
								</fo:block>
							</fo:block>
						</fo:table-cell>						

						<!-- Item Desc # -->
						<fo:table-cell padding=".20mm" border-width=".10mm" border-style="solid" border-color="black">
							<fo:block text-align="left" font-family="Lucida">
								<xsl:value-of select="Item/@ItemShortDesc" />							
							</fo:block>
						</fo:table-cell>

						<!-- Color and Size -->
						<fo:table-cell padding=".20mm" border-width=".10mm" border-style="solid" border-color="black">
							<fo:block font-family="Lucida">
								<fo:table background-color="White" table-layout="fixed">
									<fo:table-column column-width="60%" />
									<fo:table-column column-width="40%" />
									<fo:table-body>
										<fo:table-row>
											<fo:table-cell>
												<fo:block text-align="left" font-family="Lucida">
													<xsl:value-of select="ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Color']/@Value" />
												</fo:block>
											</fo:table-cell>
											<fo:table-cell>
												<fo:block text-align="left" font-family="Lucida">
													<xsl:value-of select="ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Size']/@Value" />
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
							</fo:block>
						</fo:table-cell>

						<!--4 UPC # -->
						<fo:table-cell padding=".20mm" border-width=".10mm" border-style="solid" border-color="black">
							<fo:block text-align="left" font-family="Lucida" >
								<xsl:value-of select="ItemDetails/ItemAliasList/ItemAlias [@AliasName='ACTIVE_UPC']/@AliasValue" />
							</fo:block>
						</fo:table-cell>

						<!-- 5 Unit Price-->
						<fo:table-cell padding=".20mm" border-width=".10mm" border-style="solid" border-color="black">
							<fo:block text-align="right" font-family="Lucida" >
								<xsl:choose>
									<xsl:when test="LinePriceInfo/@UnitPrice!=''">
										<xsl:value-of select="fopUtil:getLocalizedString('&#36;&#x20;',$locale)" />
										<xsl:value-of select="LinePriceInfo/@UnitPrice" />							
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="LinePriceInfo/@UnitPrice" />
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>
						</fo:table-cell>

						<!--6 Discount  -->
						<fo:table-cell padding=".20mm" border-width=".10mm" border-style="solid" border-color="black">
							<fo:block text-align="center" font-family="Lucida" >
								<fo:table background-color="White" table-layout="fixed">
									<fo:table-column column-width="68%" />									
									<fo:table-column column-width="30%" />
									<fo:table-column column-width="2%" />
									<fo:table-body>
										<fo:table-row>
											<fo:table-cell  border-width=".10pt">
												<fo:block text-align="left" font-family="Lucida" font-size="6pt">
													<xsl:value-of select="@DiscountName" />
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  border-width=".10pt">
												<fo:block text-align="right" font-family="Lucida">
													<xsl:value-of select="@DiscountChargeAmount" />
												</fo:block>
											</fo:table-cell>
											<fo:table-cell empty-cells="hide">
												<fo:block />
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
							</fo:block>
						</fo:table-cell>

						<!--7  -->
						<fo:table-cell padding=".20mm" border-width=".10mm" border-style="solid" border-color="black">
							<fo:block text-align="center" font-family="Lucida" >
								<xsl:value-of select="@OrderLinePackQty" />
							</fo:block>
						</fo:table-cell>

						<!-- 8  -->
						<fo:table-cell padding=".20mm" border-width=".2mm" border-style="solid" border-color="black">
							<fo:block text-align="right" font-family="Lucida" >									
								<xsl:choose>
									<xsl:when test="@OrderLineTotalPrice!=''">
										<xsl:value-of select="fopUtil:getLocalizedString('&#36;&#x20;',$locale)" />
										<xsl:value-of select="@OrderLineTotalPrice" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="@OrderLineTotalPrice" />
										<xsl:call-template name="addEmptyBlock" />
									</xsl:otherwise>
								</xsl:choose>								
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

				</xsl:for-each>
			</fo:table-body>
		</fo:table>	

		<!-- OrderDetails -->	
		<fo:table width="100%">
			<fo:table-column column-width="10%" />
			<fo:table-column column-width="35%" />
			<fo:table-column column-width="15%" />
			<fo:table-column column-width="15%" />
			<fo:table-column column-width="25%" />

			<fo:table-body>			
				<fo:table-row >
					<fo:table-cell padding="1mm">
						<fo:block text-align="left"  font-family="Lucida" font-size="7pt" >
							<xsl:value-of select="fopUtil:getLocalizedString('Order By:',$locale)" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1.50mm">
						<fo:block>
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="100%" />
								<fo:table-body>
									<fo:table-row >
										<fo:table-cell padding="0.30mm">
											<fo:block text-align="left"  font-family="Lucida" font-size="7pt" >
												<xsl:value-of select="$FullName" />																						
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell padding=".30mm">
											<fo:block text-align="left" font-family="Lucida" font-size="7pt" >
												<xsl:value-of select="$FullAddress" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell padding=".30mm">
											<fo:block text-align="left"  font-family="Lucida" font-size="7pt" >
												<xsl:value-of select="$AddressLocation" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>

					<fo:table-cell padding=".50mm">
						<fo:block>
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="100%" />								
								<fo:table-body>

									<fo:table-row>
										<fo:table-cell padding=".30mm">
											<fo:block text-align="left" font-size="7pt" font-family="Lucida"  font-weight="bold">
												<xsl:value-of select="fopUtil:getLocalizedString('Return TRK#',$locale)" />
											</fo:block>
										</fo:table-cell>										
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding=".30mm">
											<fo:block text-align="left" font-size="7pt">
												<xsl:value-of select="/Containers/@ReturnTrackingNo" />
											</fo:block>
										</fo:table-cell>										
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>
					<fo:table-cell padding=".50mm">
						<fo:block>
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="45%" />
								<fo:table-column column-width="10%" />
								<fo:table-column column-width="45%" />
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding=".30mm">
											<fo:block text-align="left" font-family="Lucida" font-size="7pt" font-weight="bold">
												<xsl:value-of select="fopUtil:getLocalizedString('Subtotal',$locale)" />
											</fo:block>
										</fo:table-cell>

										<fo:table-cell empty-cells="hide">
											<fo:block />
										</fo:table-cell>

										<fo:table-cell padding=".30mm" font-weight="bold">
											<fo:block text-align="right" font-family="Lucida" font-size="7pt" >
												<xsl:choose>
													<xsl:when test="$ContainerSubTotal!=''">
														<xsl:value-of select="fopUtil:getLocalizedString('&#36;&#x20;',$locale)" />
														<xsl:value-of select="$ContainerSubTotal" />							
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="$ContainerSubTotal" />
													</xsl:otherwise>
												</xsl:choose>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>

									<fo:table-row>
										<fo:table-cell padding=".30mm" font-weight="bold">
											<fo:block text-align="left" font-size="7pt"  font-family="Lucida">
												<xsl:value-of select="/Containers/@CarrierServiceCode" />
											</fo:block>

										</fo:table-cell>
										<fo:table-cell empty-cells="hide">
											<fo:block />
										</fo:table-cell>
										<fo:table-cell padding=".30mm" font-weight="bold">
											<fo:block text-align="right" font-size="7pt" font-family="Lucida" >								
												<xsl:choose>
													<xsl:when test="$ContainerShippingTotal!=''">
														<xsl:value-of select="fopUtil:getLocalizedString('&#36;&#x20;',$locale)" />
														<xsl:value-of select="$ContainerShippingTotal" />							
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="$ContainerShippingTotal" />
													</xsl:otherwise>
												</xsl:choose>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>

									<fo:table-row>
										<fo:table-cell padding=".30mm" font-weight="bold">
											<fo:block text-align="left" font-size="7pt" font-family="Lucida">
												<xsl:value-of select="fopUtil:getLocalizedString('Tax',$locale)" />
											</fo:block>
										</fo:table-cell>
										<fo:table-cell empty-cells="hide">
											<fo:block />
										</fo:table-cell>
										<fo:table-cell padding=".30mm" font-weight="bold">
											<fo:block text-align="right" font-family="Lucida" font-size="7pt">												
												<xsl:choose>
													<xsl:when test="$ContainerTaxTotal!=''">
														<xsl:value-of select="fopUtil:getLocalizedString('&#36;&#x20;',$locale)" />
														<xsl:value-of select="$ContainerTaxTotal" />							
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="$ContainerTaxTotal" />
													</xsl:otherwise>
												</xsl:choose>												
											</fo:block>
										</fo:table-cell>
									</fo:table-row>

									<fo:table-row>
										<fo:table-cell padding=".30mm" font-weight="bold">
											<fo:block text-align="left" font-size="7pt" font-family="Lucida">
												<xsl:value-of select="fopUtil:getLocalizedString('Total',$locale)" />
											</fo:block>
										</fo:table-cell>
										<fo:table-cell empty-cells="hide">
											<fo:block />
										</fo:table-cell>
										<fo:table-cell padding=".30mm" font-weight="bold">
											<fo:block text-align="right" font-family="Lucida" font-size="7pt" >												
												<xsl:choose>
													<xsl:when test="$ContainerTotal!=''">
														<xsl:value-of select="fopUtil:getLocalizedString('&#36;&#x20;',$locale)" />
														<xsl:value-of select="$ContainerTotal" />							
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="$ContainerTotal" />
													</xsl:otherwise>
												</xsl:choose>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row >
					<fo:table-cell padding=".005mm">
						<fo:block text-align="left" font-size="4pt">
							<xsl:value-of select="fopUtil:getLocalizedString('____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________',$locale)" />
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>

		<xsl:call-template name="addBarCodeAndOrderMessage" />		
		<xsl:call-template name="addEmptyBlock" />				
		<xsl:call-template name="addLocatorReturnForm" />		



		<!--	!!!!!!!!!!!!!!!!###Table 3!!!!!!!!!!!!!!!! Return Label Print :Start!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!	-->

		<fo:table border-width=".40mm" border-style="solid" border-color="black" width="100%" height="20cm"  border-collapse="collapse" visibility="collapse"   border="solid 0mm" >
			<!-- Line  -->
			<fo:table-column column-width="4%" />
			<!-- Item Desc -->
			<fo:table-column column-width="26%" />
			<!-- Color and Size -->
			<fo:table-column column-width="17%" />
			<!-- UPC-->
			<fo:table-column column-width="12%" />
			<!-- Net Price -->
			<fo:table-column column-width="7%" />
			<!-- Qty Return Code -->
			<fo:table-column column-width="16%" />
			<!-- Return Codes   -->
			<fo:table-column column-width="18%" />

			<fo:table-body>
				<fo:table-row background-color="#CCCCFF" font-size="8pt" border="solid 0.1mm ">				

					<fo:table-cell border-width=".10mm" border-style="solid" border-color="black" padding=".50mm">
						<fo:block text-align="center" font-family="Lucida">
							<xsl:value-of select="fopUtil:getLocalizedString('Line',$locale)" />
						</fo:block>
					</fo:table-cell>

					<fo:table-cell border-width=".10mm" border-style="solid" border-color="black" padding=".50mm">
						<fo:block text-align="center" font-family="Lucida">
							<xsl:value-of select="fopUtil:getLocalizedString('Item Desc',$locale)" />
						</fo:block>
					</fo:table-cell>

					<fo:table-cell border-width=".10mm" border-style="solid" border-color="black" padding=".50mm">
						<fo:block text-align="center" font-family="Lucida">
							<fo:table table-layout="fixed">
								<fo:table-column column-width="60%" />
								<fo:table-column column-width="40%" />
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding=".025mm" border-width=".10mm">
											<fo:block text-align="left" font-family="Lucida">
												<xsl:value-of select="fopUtil:getLocalizedString('Color',$locale)" />
											</fo:block>
										</fo:table-cell>

										<fo:table-cell padding=".025mm" border-width=".10mm">
											<fo:block text-align="left" font-family="Lucida">
												<xsl:value-of select="fopUtil:getLocalizedString('Size',$locale)" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>

					<fo:table-cell border-width=".2mm" border-style="solid" border-color="black" padding=".50mm">
						<fo:block text-align="center" font-family="Lucida" >
							<xsl:value-of select="fopUtil:getLocalizedString('UPC',$locale)" />
						</fo:block>
					</fo:table-cell>

					<fo:table-cell border-width=".2mm" border-style="solid" border-color="black" padding=".50mm">
						<fo:block text-align="center" font-family="Lucida">
							<xsl:value-of select="fopUtil:getLocalizedString('Net Price',$locale)" />
						</fo:block>
					</fo:table-cell>

					<fo:table-cell border-width=".10mm" border-style="solid" border-color="black" padding=".50mm">
						<fo:block text-align="center" font-family="Lucida">
							<fo:table table-layout="fixed" border-style="solid" border-width="0mm" width="100%">
								<fo:table-column column-width="40%" />
								<fo:table-column column-width="60%" />
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding=".025mm" border-width=".10mm">
											<fo:block text-align="left" font-family="Lucida" >
												<xsl:value-of select="fopUtil:getLocalizedString('QTY',$locale)" />
											</fo:block>
										</fo:table-cell>

										<fo:table-cell padding=".025mm" border-width=".10mm">
											<fo:block text-align="left" font-family="Lucida">
												<xsl:value-of select="fopUtil:getLocalizedString('Return Code',$locale)" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>

					<fo:table-cell border-width=".10mm" border-style="solid" border-color="black" padding=".50mm">
						<fo:block text-align="center" font-family="Lucida">
							<xsl:value-of select="fopUtil:getLocalizedString('Return Codes',$locale)" />
						</fo:block>
					</fo:table-cell>
				</fo:table-row>


				<xsl:for-each select="OrderLine" >             
					<!--Row 1 -->
					<fo:table-row  visibility="hidden"  border-color="white" font-size="7pt" border-style="dotted">
						<!-- 1LINE# -->
						<fo:table-cell padding=".20mm" border-width=".10mm" border-style="solid" border-color="black">
							<fo:block text-align="center" font-family="Lucida">
								<fo:block>
									<!-- <xsl:value-of select="@PrimeLineNo" /> -->
									<xsl:value-of select="@SerialNo" />
								</fo:block>
							</fo:block>
						</fo:table-cell>
						<!-- Item Desc # -->
						<fo:table-cell padding=".20mm" border-width=".10mm" border-style="solid" border-color="black">
							<fo:block text-align="left" font-family="Lucida">
								<xsl:value-of select="Item/@ItemShortDesc" />							
							</fo:block>
						</fo:table-cell>
						<!-- Color and Size -->
						<fo:table-cell padding=".20mm" border-width=".10mm" border-style="solid" border-color="black">
							<fo:block font-family="Lucida">
								<fo:table background-color="White" table-layout="fixed">
									<fo:table-column column-width="60%" />
									<fo:table-column column-width="40%" />
									<fo:table-body>
										<fo:table-row>
											<fo:table-cell>
												<fo:block text-align="left" font-family="Lucida">
													<xsl:value-of select="ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Color']/@Value" />
												</fo:block>
											</fo:table-cell>
											<fo:table-cell>
												<fo:block text-align="left" font-family="Lucida">
													<xsl:value-of select="ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Size']/@Value" />
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
							</fo:block>
						</fo:table-cell>
						<!--4 UPC # -->
						<fo:table-cell padding=".20mm" border-width=".10mm" border-style="solid" border-color="black">
							<fo:block text-align="left" font-family="Lucida" >
								<xsl:value-of select="ItemDetails/ItemAliasList/ItemAlias [@AliasName='ACTIVE_UPC']/@AliasValue" />
							</fo:block>
						</fo:table-cell>
						<!-- 5 Net Price-->
						<fo:table-cell padding=".20mm" border-width=".10mm" border-style="solid" border-color="black">
							<fo:block text-align="right" font-family="Lucida">
								<xsl:choose>
									<xsl:when test="LinePriceInfo/@UnitPrice!=''">
										<xsl:value-of select="fopUtil:getLocalizedString('&#36;&#x20;',$locale)" />
										<xsl:value-of select="LinePriceInfo/@UnitPrice" />							
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="LinePriceInfo/@UnitPrice" />
										<xsl:call-template name="addEmptyBlock" />
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>
						</fo:table-cell>
						<!--6 Qty Return Code  -->
						<fo:table-cell padding=".20mm" border-width=".10mm" border-style="solid" border-color="black">
							<fo:block>
								<fo:table table-layout="fixed" width="100%">
									<fo:table-column column-width="35%" />
									<fo:table-column column-width="65%" />
									<fo:table-body>
										<fo:table-row>
											<fo:table-cell padding=".50mm">
												<fo:block text-align="left"  font-size="4pt" padding=".40mm">
													<xsl:value-of select="@LocatorReturnFormQtySymbol" />
												</fo:block>
											</fo:table-cell>

											<fo:table-cell padding=".50mm">
												<fo:block text-align="center" font-size="4pt" padding=".40mm">
													<xsl:value-of select="@LocatorReturnFormReturnCodeSymbol" />
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
							</fo:block>
						</fo:table-cell>
						<!--7  Return Codes-->
						<fo:table-cell padding=".20mm" border-width=".10mm" border-style="solid" border-color="black">

							<fo:block>
								<fo:table table-layout="fixed" width="100%">
									<fo:table-column column-width="68%" />
									<fo:table-column column-width="32%" />
									<fo:table-body>
										<fo:table-row>
											<fo:table-cell>
												<fo:block font-size="7pt">
													<xsl:value-of select="@ReturnCode" />
												</fo:block>
											</fo:table-cell>

											<fo:table-cell>
												<fo:block font-size="7pt" text-align="left">
													<xsl:value-of select="@ReturnCodeNo" />
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:for-each>		
			</fo:table-body>
		</fo:table>


		<!-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Return Label, Address and gift detail methods!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  -->		
		<xsl:call-template name="addLocatorReturnFormMessage" />
		<xsl:call-template name="addEmptyBlock" />
		<xsl:call-template name="addEmptyBlock" />
		<xsl:call-template name="addEmptyBlock" />
		<xsl:call-template name="addEmptyBlock" />
		<xsl:call-template name="addEmptyBlock" />
		<xsl:call-template name="addEmptyBlock" />
		<xsl:call-template name="addBillToAdd" />	
		<xsl:call-template name="addShipToAsStoreAddress" />
		<xsl:call-template name="addEmptyBlock" />
		<xsl:call-template name="addEmptyBlock" />
		<xsl:call-template name="addEmptyBlock" />
		<xsl:call-template name="addEmptyBlock" />
		<xsl:call-template name="addEmptyBlock" />		
		<xsl:call-template name="InStoreLabel" />
		<xsl:call-template name="CustomerAddress" />
		<xsl:call-template name="LevelOfService" /> 
		<xsl:call-template name="addOrderNoBarCode" /> 
	
	</xsl:template>




	<xsl:template name="addBarCodeAndOrderMessage">
		<xsl:variable name="Barcode" select="/Containers/BarCodes/@ShippingSummaryBarCodeNo" />
		<fo:table width="100%" table-layout="fixed">
			<fo:table-column column-width="2%" />
			<fo:table-column column-width="55%" />
			<fo:table-column column-width="2%" />
			<fo:table-column column-width="41%" />
			<fo:table-body>
				<fo:table-cell empty-cells="hide">
					<fo:block />
				</fo:table-cell>
				<fo:table-cell padding="1mm">
					<fo:block>
						<fo:table width="100%" table-layout="fixed">
							<fo:table-column column-width="100%" />
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell border-width=".15pt" font-family="Lucida" font-size="6pt">
										<fo:block text-align-last="left">
											<fo:instream-foreign-object>
												<barcode:barcode xmlns:barcode="http://barcode4j.krysalis.org/ns" message="{$Barcode}">					
													<barcode:code128>
														<barcode:height>12mm</barcode:height>
														<barcode:width>55mm</barcode:width>
														<module-width>.35mm</module-width>
														<human-readable>
															<placement>none</placement>
															<pattern></pattern>
														</human-readable>
													</barcode:code128>
												</barcode:barcode>
											</fo:instream-foreign-object>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>							
								<fo:table-row>
									<fo:table-cell padding=".20mm" border-width=".15pt" font-family="Lucida" font-size="7pt">
										<fo:block text-align-last="left" font-family="Lucida" white-space-collapse="false">
											<xsl:value-of select="/Containers/BarCodes/@ShippingSummaryBarCodeForDisplay" />
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell empty-cells="hide">
					<fo:block />
				</fo:table-cell>
				<fo:table-cell>
					<fo:block padding="2mm">
						<fo:table table-layout="fixed" width="100%">
							<fo:table-column column-width="100%" />
							<fo:table-body>

								<fo:table-row padding=".050mm">
									<fo:table-cell>
										<fo:block text-align="left" font-size="6pt">
											<xsl:value-of select="fopUtil:getLocalizedString('PaymentMethod : ',$locale)" />
											<xsl:value-of select="/Containers/@PaymentType" />
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row padding=".050mm">
									<fo:table-cell>
										<fo:block text-align="left" font-size="7pt">
											<xsl:value-of select="/Containers/@CustomerCareMessage" />
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row padding=".050mm">
									<fo:table-cell>
										<fo:block text-align="justify">
											<fo:table table-layout="fixed" width="100%">
												<fo:table-column column-width="40%" />
												<fo:table-column column-width="60%" />
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding=".050mm" border-width=".15pt">
															<fo:block text-align="left" font-size="7pt">
																<xsl:value-of select="fopUtil:getLocalizedString('Customer Service',$locale)" />
															</fo:block>
														</fo:table-cell>
														<fo:table-cell >
															<fo:block text-align="left" font-size="7pt">													
																<xsl:value-of select="/Containers/@CustomerCareNo" />
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:table-cell>
			</fo:table-body>
		</fo:table>
	</xsl:template>



	<!--Return Receipt  -->
	<xsl:template name="addLocatorReturnForm">
		<!-- Fetch Values-->
		<fo:table background-color="White" table-layout="fixed" width="100%">
			<fo:table-column column-width="2%" />
			<fo:table-column column-width="15%" />
			<fo:table-column column-width="2%" />
			<fo:table-column column-width="19%" />
			<fo:table-column column-width="43%" />
			<fo:table-column column-width="16%" />
			<fo:table-column column-width="3%" />

			<fo:table-body>
				<!-- Cell 1 -->
				<fo:table-cell empty-cells="hide">
					<fo:block />
				</fo:table-cell>
				<!-- Cell 2 -->
				<fo:table-cell>
					<fo:block>
						<fo:external-graphic src="{$Logo}" content-height="scale-to-fit" content-width="1.1in"/>
					</fo:block>
				</fo:table-cell>
				<!-- Cell 3 -->
				<fo:table-cell empty-cells="hide">
					<fo:block />
				</fo:table-cell>
				<!-- Cell 4  :Start-->
				<fo:table-cell font-family="Lucida" font-size="10pt" text-align="left">
					<fo:block text-align="justify">

						<fo:table table-layout="fixed" width="100%">
							<fo:table-body>
								<!--1st row -->
								<fo:table-row>
									<fo:table-cell padding="3mm">
										<fo:block text-align="justify">
											<fo:table background-color="White" table-layout="fixed" width="100%">
												<fo:table-column column-width="35%" />
												<fo:table-column column-width="65%" />
												<fo:table-body>

													<fo:table-row>
														<fo:table-cell>
															<fo:block text-align="left">
																<xsl:value-of select="fopUtil:getLocalizedString('Order #',$locale)" />
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block text-align="left">
																<xsl:value-of select="$OrderNo" />
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:table-cell>
				<!-- Cell 4  :End -->
				<!-- Cell 5 -->
				<fo:table-cell empty-cells="hide">
					<fo:block />
				</fo:table-cell>
				<!-- Cell 6 -->
				<fo:table-cell>
					<fo:block text-align="left">
						<fo:table background-color="White" table-layout="fixed" width="100%" font-size="7pt">
							<fo:table-body>
								<fo:table-row >
									<fo:table-cell padding="1mm" border-width=".15pt">
										<fo:block text-align-last="left">
											<xsl:call-template name="addEmptyBlock" />
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding="1mm" border-width=".15pt">
										<fo:block text-align-last="left">
											<xsl:call-template name="addEmptyBlock" />
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding="0.50mm">
										<fo:block text-align="justify" vertical-align="bottom">
											<fo:table table-layout="fixed" width="100%">
												<fo:table-column column-width="25%" />
												<fo:table-column column-width="75%" />
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell>
															<fo:block text-align="left">
																<xsl:value-of select="fopUtil:getLocalizedString('Date : ',$locale)" />
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block text-align="left">
																<xsl:value-of select="/Containers/@Date"/>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:table-cell>
				<!-- Cell 9 -->
				<fo:table-cell empty-cells="hide">
					<fo:block />
				</fo:table-cell>
			</fo:table-body>
		</fo:table>
	</xsl:template>



	<xsl:template name="addLocatorReturnFormMessage">		
		<xsl:variable name="ShippingSummaryBarCodeNo" select="/Containers/BarCodes/@ShippingSummaryBarCodeNo" />
		<fo:table width="100%" table-layout="fixed">
			<fo:table-column column-width="50%" />
			<fo:table-column column-width="8%" />
			<fo:table-column column-width="42%" />
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell>
						<fo:block padding="2mm">
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="100%" />
								<fo:table-body>
									<fo:table-row font-size="5pt" >
										<fo:table-cell padding=".10mm">
											<fo:block text-align="left">
												<xsl:value-of select="/Containers/ReturnMessages/@ReturnMessage1" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell font-size="5pt" padding=".10mm">
											<fo:block text-align="left">
												<xsl:value-of select="/Containers/ReturnMessages/@ReturnMessage2" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell font-size="5pt" padding=".10mm">
											<fo:block text-align="left">
												<xsl:value-of select="/Containers/ReturnMessages/@ReturnMessage3" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell font-size="5pt" padding=".10mm">
											<fo:block text-align="left">
												<xsl:value-of select="/Containers/ReturnMessages/@ReturnMessage4" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell font-size="5pt" padding=".10mm">
											<fo:block text-align="left">
												<xsl:value-of select="/Containers/ReturnMessages/@ReturnMessage5" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>

					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>

					<fo:table-cell padding="1.5mm" border-width=".15pt">
						<fo:block text-align-last="left" >
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="100%" />
								<fo:table-body>
									<fo:table-row >
										<fo:table-cell >
											<fo:block text-align-last="left" >
												<fo:instream-foreign-object>
													<barcode:barcode xmlns:barcode="http://barcode4j.krysalis.org/ns" message="{$ShippingSummaryBarCodeNo}">
														<barcode:code128>
															<barcode:height>12mm</barcode:height>
															<barcode:width>55mm</barcode:width>
															<module-width>.35mm</module-width>
															<human-readable>
																<placement>none</placement>
																<pattern></pattern>
															</human-readable>
														</barcode:code128>
													</barcode:barcode>
												</fo:instream-foreign-object>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding=".10mm" border-width=".15pt" font-family="Lucida" font-size="7pt">
											<fo:block text-align-last="left" font-family="Lucida" white-space-collapse="false">
												<xsl:value-of select="/Containers/BarCodes/@ShippingSummaryBarCodeForDisplay" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>		
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>

			</fo:table-body>
		</fo:table>
	</xsl:template>




	<xsl:template name="addBillToAdd">
		<xsl:variable name="BillToAddBarcode" select="/Containers/BarCodes/@BillToAddressBarCode" />

		<fo:table width="100%">
			<fo:table-column column-width="3%" />
			<fo:table-column column-width="9%" />
			<fo:table-column column-width="26%" />
			<fo:table-column column-width="20%" />
			<fo:table-column column-width="42%" />

			<fo:table-body>
				<fo:table-row >
					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>
					<fo:table-cell padding="1.5mm">
						<fo:block text-align="left" font-size="7pt" padding=".050mm" font-family="Lucida">
							<xsl:value-of select="fopUtil:getLocalizedString('From :',$locale)" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1.5mm">
						<fo:block>
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="100%" />
								<fo:table-body>
									<fo:table-row >
										<fo:table-cell>
											<fo:block text-align="left"  font-family="Arial" font-size="7pt" padding=".050mm">
												<xsl:value-of select="$FullNameOne" />																						
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell>
											<fo:block text-align="left" font-family="Arial" font-size="7pt" padding=".050mm">
												<xsl:value-of select="$FullAddressOne" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell>
											<fo:block text-align="left"  font-family="Arial" font-size="7pt" padding=".050mm">
												<xsl:value-of select="$AddressLocationOne" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>

								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>

					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>

					<fo:table-cell padding="1.5mm" border-width=".15pt">
						<fo:block text-align-last="left">
							<fo:instream-foreign-object>
								<barcode:barcode xmlns:barcode="http://barcode4j.krysalis.org/ns" message="{$BillToAddBarcode}">
									<barcode:ean-128>
										<barcode:height>15mm</barcode:height>
										<barcode:width>70mm</barcode:width>
										<module-width>.43mm</module-width>										
									</barcode:ean-128>
								</barcode:barcode>
							</fo:instream-foreign-object>
						</fo:block>
					</fo:table-cell>

				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>




	<xsl:template name="addShipToAsStoreAddress">
		<xsl:variable name="ReturnLabelBarCode" select="/Containers/BarCodes/@ReturnLabelTrackBarCodeNo" />

		<fo:table width="100%">
			<fo:table-column column-width="3%" />
			<fo:table-column column-width="9%" />
			<fo:table-column column-width="26%" />
			<fo:table-column column-width="20%" />
			<fo:table-column column-width="42%" />

			<fo:table-body>
				<fo:table-row >
					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>
					<fo:table-cell padding="1.5mm">
						<fo:block text-align="left" font-size="7pt" padding=".050mm">
							<xsl:value-of select="fopUtil:getLocalizedString('Ship To :',$locale)" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1.5mm">
						<fo:block>
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="100%" />
								<fo:table-body>
									<fo:table-row >
										<fo:table-cell>
											<fo:block text-align="left" font-size="6pt" padding=".050mm">
												<xsl:value-of select="/Containers/ReturnStoreAddress/@ReturnAddressLine1" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell>
											<fo:block text-align="left" font-size="6pt" padding=".050mm">
												<xsl:value-of select="/Containers/ReturnStoreAddress/@ReturnAddressLine2" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell>
											<fo:block text-align="left" font-size="6pt" padding=".050mm">
												<xsl:value-of select="/Containers/ReturnStoreAddress/@ReturnAddressLine3" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>

					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>

					<fo:table-cell padding="1mm">
						<fo:block>
							<fo:table width="100%" table-layout="fixed">
								<fo:table-column column-width="100%" />
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding=".5mm" border-width=".15pt">
											<fo:block text-align-last="left">
												<fo:instream-foreign-object>
													<barcode:barcode xmlns:barcode="http://barcode4j.krysalis.org/ns" message="{$ReturnLabelBarCode}">
														<barcode:ean-128>
															<barcode:height>35mm</barcode:height>
															<barcode:width>70mm</barcode:width>
															<module-width>.43mm</module-width>
															<human-readable>
																<placement>none</placement>
																<pattern></pattern>
															</human-readable>
														</barcode:ean-128>
													</barcode:barcode>
												</fo:instream-foreign-object>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>							
									<fo:table-row>
										<fo:table-cell padding=".20mm" >
											<fo:block>
												<fo:table width="100%" table-layout="fixed">
													<fo:table-column column-width="15%" />
													<fo:table-column column-width="15%" />
													<fo:table-column column-width="15%" />
													<fo:table-column column-width="15%" />
													<fo:table-column column-width="15%" />
													<fo:table-column column-width="25%" />
													<fo:table-body>

														<fo:table-row>
															<fo:table-cell padding=".20mm" border-width=".15pt" font-family="Lucida" font-size="7pt"> 
																<fo:block text-align-last="left" font-family="Lucida">
																	<xsl:value-of select="fopUtil:getLocalizedString('GND ',$locale)" />
																</fo:block>
															</fo:table-cell>
															<fo:table-cell padding=".20mm" border-width=".15pt" font-family="Lucida" font-size="7pt"> 
																<fo:block text-align-last="left" font-family="Lucida">
																	<xsl:value-of select="/Containers/BarCodes/@ReturnTrackingBlocksForDisplay" />
																</fo:block>
															</fo:table-cell>
															<fo:table-cell padding=".20mm" border-width=".15pt" font-family="Lucida" font-size="7pt"> 
																<fo:block text-align-last="left" font-family="Lucida">
																	<xsl:value-of select="/Containers/BarCodes/@ShipperIdForDisplay" />
																</fo:block>
															</fo:table-cell>
															<fo:table-cell padding=".20mm" border-width=".15pt" font-family="Lucida" font-size="7pt"> 
																<fo:block text-align-last="left" font-family="Lucida">
																	<xsl:value-of select="/Containers/BarCodes/@ReturnTrackingPkgNoForDisplay" />
																</fo:block>
															</fo:table-cell>
															<fo:table-cell padding=".20mm" border-width=".15pt" font-family="Lucida" font-size="7pt"> 
																<fo:block text-align-last="left" font-family="Lucida">
																	<xsl:value-of select="fopUtil:getLocalizedString('FedEx ',$locale)" />
																</fo:block>
															</fo:table-cell>
															<fo:table-cell empty-cells="hide">
																<fo:block />
															</fo:table-cell>
														</fo:table-row>

														<fo:table-row>
															<fo:table-cell padding=".20mm" border-width=".15pt" font-family="Lucida" font-size="7pt"> 
																<fo:block text-align-last="left" font-family="Lucida">
																	<xsl:value-of select="fopUtil:getLocalizedString('Prepaid ',$locale)" />
																</fo:block>
															</fo:table-cell>
															<fo:table-cell padding=".20mm" border-width=".15pt" font-family="Lucida" font-size="7pt"> 
																<fo:block text-align-last="right" font-family="Lucida">
																	<xsl:value-of select="fopUtil:getLocalizedString('Return ',$locale)" />
																</fo:block>
															</fo:table-cell>
															<fo:table-cell padding=".20mm" border-width=".15pt" font-family="Lucida" font-size="7pt"> 
																<fo:block text-align-last="left" font-family="Lucida">
																	<xsl:value-of select="fopUtil:getLocalizedString('Mgr ',$locale)" />
																</fo:block>
															</fo:table-cell>
															<fo:table-cell empty-cells="hide">
																<fo:block />
															</fo:table-cell>
															<fo:table-cell padding=".20mm" border-width=".15pt" font-family="Lucida" font-size="7pt"> 
																<fo:block text-align-last="left" font-family="Lucida">
																	<xsl:value-of select="fopUtil:getLocalizedString('Ground ',$locale)" />
																</fo:block>
															</fo:table-cell>
															<fo:table-cell empty-cells="hide">
																<fo:block />
															</fo:table-cell>
														</fo:table-row>
													</fo:table-body>
												</fo:table>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>

				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>


	<xsl:template name="addOrderNoBarCode">
		<xsl:variable name="OrderNoBarCode" select="/Containers/@OrderNo" />
		<fo:table width="100%">
			<fo:table-column column-width="10%" />
			<fo:table-column column-width="40%" />
			<fo:table-column column-width="50%" />
			<fo:table-body>
				<fo:table-row >
					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>
					<fo:table-cell padding=".20mm" border-width=".15pt" font-size="7pt">
						<fo:block text-align-last="left" padding=".50mm">
							<fo:instream-foreign-object>
								<barcode:barcode xmlns:barcode="http://barcode4j.krysalis.org/ns" message="{$OrderNoBarCode}">
									<barcode:code128>
										<barcode:height>12mm</barcode:height>
										<barcode:width>58mm</barcode:width>
										<module-width>.38mm</module-width>										
									</barcode:code128>
								</barcode:barcode>
							</fo:instream-foreign-object>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>										
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>

	<xsl:template name="CustomerAddress">

		<fo:table width="100%">
			<fo:table-column column-width="10%" />
			<fo:table-column column-width="40%" />
			<fo:table-column column-width="50%" />

			<fo:table-body>
				<fo:table-row padding=".10mm">

					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>
					<fo:table-cell padding="1.5mm">
						<fo:block>
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="100%" />
								<fo:table-body>
									<fo:table-row >
										<fo:table-cell>
											<fo:block text-align="left"  font-family="Arial" font-size="10pt" padding=".050mm">
												<xsl:value-of select="$FullNameOne" />																						
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell>
											<fo:block text-align="left" font-family="Arial" font-size="10pt" padding=".050mm">
												<xsl:value-of select="$FullAddressOne" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row >
										<fo:table-cell>
											<fo:block text-align="left"  font-family="Arial" font-size="10pt" padding=".10mm">
												<xsl:value-of select="$AddressLocationOne" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>

								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>

					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>

				</fo:table-row>
			</fo:table-body>
		</fo:table>

	</xsl:template>

	
	<xsl:template name="LevelOfService">		
		<fo:table width="100%">
			<fo:table-column column-width="10%" />
			<fo:table-column column-width="40%" />
			<fo:table-column column-width="50%" />
			<fo:table-body>
				<fo:table-row >
					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>
					<fo:table-cell padding="1.5mm">
						<fo:block text-align="left" font-family="Arial" font-size="10pt" padding=".050mm">
							<xsl:value-of select="/Containers/@CarrierServiceCode" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>	
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	
	<xsl:template name="InStoreLabel">		
		<fo:table width="100%">
			<fo:table-column column-width="10%" />
			<fo:table-column column-width="40%" />
			<fo:table-column column-width="50%" />
			<fo:table-body>
				<fo:table-row >
					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>
					<fo:table-cell padding="1.5mm">
						<fo:block text-align="left" font-family="Arial" font-size="10pt" padding=".050mm">
							<xsl:value-of select="fopUtil:getLocalizedString('Temporary In-Store Label',$locale)" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>	
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>


	<xsl:template name="addEmptyBlock">
		<xsl:param name="hightinpt" />
		<fo:table width="100%">
			<fo:table-column column-width="100%" />
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell>
						<fo:block white-space-treatment="preserve" linefeed-treatment="preserve">
							<xsl:text> &#x00A0;</xsl:text>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>



	<!-- PackSlip -->


</xsl:stylesheet>