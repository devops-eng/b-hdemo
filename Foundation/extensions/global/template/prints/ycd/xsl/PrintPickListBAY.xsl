<?xml version="1.0" encoding="utf-8"?>
<!--
Licensed Materials - Property of IBM
IBM Sterling Selling and Fulfillment Suite - Foundation
(C) Copyright IBM Corp. 2011, 2012 All Rights Reserved.
US Government Users Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:java="http://xml.apache.org/xslt/java"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:fopUtil="com.yantra.pca.ycd.fop.YCDFOPUtils" 
	exclude-result-prefixes="java"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:import href="/template/prints/ycd/xsl/CommonTemplates_FOP.xsl"/>
	<xsl:output method="xml" indent="yes" />


	<!-- This template accepts multi api with getSortedShipmentDetails	 api out put  -->
	<xsl:variable name="locale">
		<xsl:value-of select="/StoreBatch/@LocaleCode" />
	</xsl:variable>



	<xsl:template match="/">
		<fo:root>
			<fo:layout-master-set>
				<xsl:call-template name="A4-landscape_layout-master-set" />
			</fo:layout-master-set>
			<fo:page-sequence master-reference="A4-landscape">

				<!--	<xsl:call-template name="orderFooter"/>  -->

				<fo:flow flow-name="xsl-region-body"
					font-family="Arial">
					<xsl:apply-templates select="/StoreBatch" />
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>

	<xsl:template match="StoreBatch">
	<!--	<xsl:variable name="Date" select="java:format(java:java.text.SimpleDateFormat.new('MM/dd/yyyy'), java:java.util.Date.new())" /> -->
		<xsl:call-template name="addEmptyBlock" />
		<!-- ShipmentLine Header Start -->
		<fo:table background-color="White" table-layout="fixed" width="100%" >				
			<fo:table-column column-width="15%" />
			<fo:table-column column-width="2%" />
			<fo:table-column column-width="15%" />
			<fo:table-column column-width="48%" />
			<fo:table-column column-width="20%" />		

			<fo:table-body>				
				<fo:table-row padding="1mm" font-size="8pt">
					<fo:table-cell>
						<fo:block text-align="justify" font-size="8pt">
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="30%" />
								<fo:table-column column-width="70%" />
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding=".30mm">
											<fo:block text-align="left" font-size="8pt">
												<xsl:value-of select="fopUtil:getLocalizedString('Date :',$locale)" />
											</fo:block>
										</fo:table-cell >
										<fo:table-cell padding=".30mm">
											<fo:block text-align="left" font-size="8pt">
												<xsl:value-of select="@Date"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>

					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>

					<fo:table-cell>
						<fo:block text-align="justify">
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="20%" />
								<fo:table-column column-width="80%" />
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding=".30mm">
											<fo:block text-align="left" font-size="8pt">
												<xsl:value-of select="fopUtil:getLocalizedString('Batch# / Lot n°',$locale)" />
											</fo:block>
										</fo:table-cell >
										<fo:table-cell padding=".30mm">
											<fo:block text-align="left" font-size="8pt">
												<xsl:value-of select="@BatchNo"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>

					<fo:table-cell empty-cells="hide">
						<fo:block />
					</fo:table-cell>

					<fo:table-cell>
						<fo:block text-align="justify">
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="30%" />
								<fo:table-column column-width="70%" />
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding=".30mm">
											<fo:block text-align="left" font-size="8pt">
												<xsl:value-of select="fopUtil:getLocalizedString('Printed By / Imprimé par : ',$locale)" />
											</fo:block>
										</fo:table-cell >
										<fo:table-cell padding=".30mm">
											<fo:block text-align="left" font-size="8pt">
												<xsl:value-of select="@UserName"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>

				</fo:table-row>
			</fo:table-body>
		</fo:table>		

		<!--#############################################################-->
		<xsl:call-template name="addEmptyBlock" />
		<xsl:call-template name="addEmptyBlock" />
		<xsl:apply-templates select="StoreBatchLines" />
		<xsl:call-template name="addEmptyBlock" />
		<xsl:call-template name="addEmptyBlock" />
		<xsl:call-template name="addPickListBottomDetails" />


		<fo:block id="last-page"/>
	</xsl:template>



	<xsl:template match="StoreBatchLines">

		<fo:table  border-width=".5mm" border-style="solid"
			border-color="black" width="100%" font-size="10pt">		

			<!-- SR# -->
			<fo:table-column column-width="2%" />			
			<!-- Product Image -->
			<fo:table-column column-width="18%" />
			<!-- DMM # -->
			<fo:table-column column-width="4%" />
			<!-- Department  -->			
			<fo:table-column column-width="6%" />
			<!-- Department # -->
			<fo:table-column column-width="7%" />
			<!-- Label -->
			<fo:table-column column-width="8%" />
			<!-- UPC -->
			<fo:table-column column-width="7%" />			
			<!-- Item Description -->
			<fo:table-column column-width="16%" />			
			<!-- Style -->
			<fo:table-column column-width="7%" />
			<!-- Color   -->
			<fo:table-column column-width="5%" />			
			<!-- Size -->
			<fo:table-column column-width="5%" />						
			<!-- Price-->
			<fo:table-column column-width="5%" />
			<!-- Onhand Qty -->
			<fo:table-column column-width="5%" />			
			<!-- Pick Quantity -->
			<fo:table-column column-width="5%" />

			<fo:table-body>

				<fo:table-row background-color="White" font-size="9pt">
					<!-- SR# -->
					<fo:table-cell border-width=".2mm"
						border-style="solid" border-color="black" text-align="center">
						<fo:block wrap-option="wrap" font-weight="bold" font-size="9pt" padding=".5mm">
							<xsl:value-of
								select="fopUtil:getLocalizedString('SR# / N° SR',$locale)" />
						</fo:block>
					</fo:table-cell>
					<!-- Product Image -->
					<fo:table-cell border-width=".2mm"
						border-style="solid" border-color="black" text-align="center">
						<fo:block wrap-option="wrap" font-weight="bold" font-size="9pt" padding=".5mm">
							<xsl:value-of
								select="fopUtil:getLocalizedString('Product Image /Photo du produit ',$locale)" />
						</fo:block>
					</fo:table-cell>
					<!-- DMM #  -->
					<fo:table-cell border-width=".2mm"
						border-style="solid" border-color="black" text-align="center">
						<fo:block wrap-option="wrap" font-weight="bold" font-size="9pt" padding=".5mm">
							<xsl:value-of
								select="fopUtil:getLocalizedString('DMM# / N° DDA ',$locale)" />
						</fo:block>
					</fo:table-cell>
					<!-- Department  -->
					<fo:table-cell border-width=".2mm"
						border-style="solid" border-color="black" text-align="center">
						<fo:block wrap-option="wrap" font-weight="bold" font-size="9pt" padding=".5mm">
							<xsl:value-of
								select="fopUtil:getLocalizedString('Department / Rayon ',$locale)" />
						</fo:block>
					</fo:table-cell>
					<!--5 Department # -->
					<fo:table-cell border-width=".2mm"
						border-style="solid" border-color="black" text-align="center" padding=".5mm">
						<fo:block wrap-option="wrap" font-weight="bold" font-size="9pt">
							<xsl:value-of
								select="fopUtil:getLocalizedString('Department # / N° rayon ',$locale)" />
						</fo:block>
					</fo:table-cell>
					<!-- Label -->
					<fo:table-cell border-width=".2mm"
						border-style="solid" border-color="black" text-align="center" padding=".5mm">
						<fo:block wrap-option="wrap" font-weight="bold" font-size="9pt">
							<xsl:value-of
								select="fopUtil:getLocalizedString('Label / Marque',$locale)" />
						</fo:block>
					</fo:table-cell>
					<!-- UPC -->
					<fo:table-cell border-width=".2mm"
						border-style="solid" border-color="black" text-align="center">
						<fo:block wrap-option="wrap" font-weight="bold" font-size="9pt" padding=".5mm">
							<xsl:value-of
								select="fopUtil:getLocalizedString('UPC / CUP ',$locale)" />
						</fo:block>
					</fo:table-cell>				
					<!-- Item Description -->
					<fo:table-cell border-width=".2mm"
						border-style="solid" border-color="black" text-align="center">
						<fo:block wrap-option="wrap" font-weight="bold" font-size="9pt" padding=".5mm">
							<xsl:value-of
								select="fopUtil:getLocalizedString('Item Description / Description de l’article ',$locale)" />
						</fo:block>
					</fo:table-cell>
					<!-- Style -->
					<fo:table-cell border-width=".2mm"
						border-style="solid" border-color="black" text-align="center">
						<fo:block wrap-option="wrap" font-weight="bold" font-size="9pt" padding=".5mm">
							<xsl:value-of
								select="fopUtil:getLocalizedString('Style / Modèle',$locale)" />
						</fo:block>
					</fo:table-cell>				
					<!-- Color   -->
					<fo:table-cell border-width=".2mm"
						border-style="solid" border-color="black" text-align="center">
						<fo:block wrap-option="wrap" font-weight="bold" font-size="9pt" padding=".5mm">
							<xsl:value-of
								select="fopUtil:getLocalizedString('Color / Couleur ',$locale)" />
						</fo:block>
					</fo:table-cell>
					<!-- Size   -->
					<fo:table-cell border-width=".2mm"
						border-style="solid" border-color="black" text-align="center">
						<fo:block wrap-option="wrap" font-weight="bold" font-size="9pt" padding=".5mm">
							<xsl:value-of
								select="fopUtil:getLocalizedString('Size / Taille',$locale)" />
						</fo:block>
					</fo:table-cell>
					<!-- Price-->
					<!--<fo:table-cell border-width=".2mm"
						border-style="solid" border-color="black" text-align="center">
						<fo:block font-weight="bold" font-size="9pt" padding=".5mm">
							<xsl:value-of
								select="fopUtil:getLocalizedString('Price',$locale)" />
						</fo:block>
					</fo:table-cell> -->
					<!-- Price Attribute  -->
					<fo:table-cell border-width=".2mm"
						border-style="solid" border-color="black" text-align="center">
						<fo:block wrap-option="wrap" font-weight="bold" font-size="9pt" padding=".5mm">
							<xsl:value-of
								select="fopUtil:getLocalizedString('Price Attribute / Caract. de prix',$locale)" />
						</fo:block>
					</fo:table-cell>
					<!-- Onhand Qty -->
					<fo:table-cell border-width=".2mm"
						border-style="solid" border-color="black" text-align="center">
						<fo:block wrap-option="wrap" font-weight="bold" font-size="9pt" padding=".5mm">
							<xsl:value-of
								select="fopUtil:getLocalizedString('Onhand Qty / Qté en stock',$locale)" />
						</fo:block>
					</fo:table-cell>
					<!-- Pick Quantity -->
					<fo:table-cell border-width=".2mm"
						border-style="solid" border-color="black" text-align="center">
						<fo:block wrap-option="wrap" font-weight="bold" font-size="9pt" padding=".5mm">
							<xsl:value-of
								select="fopUtil:getLocalizedString('Pick Qty / Qté pour cueillette',$locale)" />
						</fo:block>
					</fo:table-cell>		

				</fo:table-row>


				<xsl:for-each select="StoreBatchLine">
					<fo:table-row border-style="solid"
						border-width=".5pt" border-color="black" font-size="8pt">
						<xsl:variable name="i" select="position()" /> 

						<!-- SR# -->
						<fo:table-cell padding="1mm" border-width=".2mm"
							border-style="solid" border-color="black" text-align="center">
							<fo:block font-size="8pt" padding=".5mm">
								<xsl:value-of select="$i" />								
							</fo:block>
						</fo:table-cell>

						<!-- 2Product Image -->	
						<fo:table-cell padding="1mm" border-width=".2mm"
							border-style="solid" border-color="black" text-align="center">
							<fo:block font-size="8pt" padding=".5mm">								
								<xsl:variable name="ImageLocation" select="ItemDetails/PrimaryInformation/@ImageLocation" />
								<xsl:variable name="ImageID" select="ItemDetails/PrimaryInformation/@ImageID" />
								<fo:external-graphic src="url({$ImageLocation}/{$ImageID})" content-height="scale-to-fit" height="2.00in"  content-width="2.00in" scaling="non-uniform"/> 
							</fo:block>
						</fo:table-cell>

						<!-- 3 DMM # -->
						<fo:table-cell padding="1mm" border-width=".2mm"
							border-style="solid" border-color="black" text-align="center">
							<fo:block font-size="8pt" padding=".5mm">
								<xsl:value-of select="@ExtnDivision" />
							</fo:block>
						</fo:table-cell>

						<!-- 4 Department -->
						<fo:table-cell padding="1mm" border-width=".2mm"
							border-style="solid" border-color="black" text-align="center">
							<fo:block font-size="8pt" padding=".5mm">
								<xsl:value-of select="@ExtnDepartmentName" />
							</fo:block>
						</fo:table-cell>
						<!--5 Department # -->
						<fo:table-cell padding="1mm" border-width=".2mm"
							border-style="solid" border-color="black" text-align="center">
							<fo:block font-size="8pt" padding=".5mm">
								<xsl:value-of select="@ExtnDepartment" />
							</fo:block>
						</fo:table-cell>

						<!-- 6 Label-->
						<fo:table-cell padding="1mm" border-width=".2mm"
							border-style="solid" border-color="black" text-align="center">
							<fo:block font-size="8pt" padding=".5mm">
								<xsl:value-of
									select="ItemDetails/PrimaryInformation/@ManufacturerName" />
							</fo:block>
						</fo:table-cell>
						<!-- 7 UPC-->
						<fo:table-cell padding="1mm" border-width=".2mm"
							border-style="solid" border-color="black" text-align="center">
							<fo:block font-size="8pt" padding=".5mm">
								<xsl:value-of
									select="ItemDetails/ItemAliasList/ItemAlias [@AliasName='ACTIVE_UPC']/@AliasValue" />
							</fo:block>
						</fo:table-cell>
						<!-- 8 ItemDescription -->			
						<fo:table-cell padding="1mm" border-width=".2mm"
							border-style="solid" border-color="black" text-align="left">
							<fo:block font-size="8pt" padding=".5mm">
								<xsl:value-of select="ItemDetails/PrimaryInformation/@ShortDescription" />
								/<xsl:value-of select="ItemDetails/ItemLocaleList/ItemLocale [@Country='CA' and @Language='fr']/PrimaryInformation/@ShortDescription" />
					
							</fo:block>
						</fo:table-cell>
						<!-- 9 Style -->	
						<fo:table-cell padding="1mm" border-width=".2mm"
							border-style="solid" border-color="black" text-align="center">
							<fo:block font-size="8pt" padding=".5mm">
								<xsl:value-of
									select="ItemDetails/Extn/@ExtnVendorStyle" />
							</fo:block>
						</fo:table-cell>

						<!--10 Color  -->					
						<fo:table-cell padding="1mm" border-width=".2mm"
							border-style="solid" border-color="black" text-align="center">
							<fo:block font-size="8pt" padding=".5mm">
								<xsl:value-of
									select="ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Color']/@Value" />
								/<xsl:value-of
									select="ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Color']/@FrenchShortDesc" />
							</fo:block>
						</fo:table-cell>

						<!-- 11 Size -->
						<fo:table-cell padding="1mm" border-width=".2mm"
							border-style="solid" border-color="black" text-align="center">
							<fo:block font-size="8pt" padding=".5mm">
								<xsl:value-of
									select="ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Size']/@Value" />
								/<xsl:value-of
									select="ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Size']/@FrenchShortDesc" />
							</fo:block>
						</fo:table-cell>
						<!-- 12  Price  -->	
						<!--<fo:table-cell padding="1mm" border-width=".2mm"
							border-style="solid" border-color="black" text-align="center">
							<fo:block font-size="8pt" padding=".5mm">
								<xsl:value-of
									select="ShipmentLines/ShipmentLine/OrderLine/ComputedPrice/@UnitPrice" />
							</fo:block>
						</fo:table-cell> -->
						<!-- 13 PriceAttribute-->
						<fo:table-cell padding="1mm" border-width=".2mm"
							border-style="solid" border-color="black" text-align="center">
							<fo:block font-size="8pt" padding=".5mm">
								<xsl:value-of
									select="ShipmentLines/ShipmentLine/Extn/@ExtnPriceAttribute" />
							</fo:block>
						</fo:table-cell>

						<!--14 Onhand Quantity-->	
						<fo:table-cell padding="1mm" border-width=".2mm"
							border-style="solid" border-color="black" text-align="center">
							<fo:block font-size="8pt" padding=".5mm">
								<xsl:value-of select="@OnhandQty" />
							</fo:block>
						</fo:table-cell>
						<!--15 Pick Qty -->		
						<fo:table-cell padding="1mm" border-width=".2mm"
							border-style="solid" border-color="black" text-align="center">
							<fo:block font-size="8pt" padding=".5mm">
								<xsl:value-of
									select="@PickingQty" />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>		
				</xsl:for-each>				
			</fo:table-body>
		</fo:table>
	</xsl:template>	


	<xsl:template name="addPickListBottomDetails">

		<fo:table background-color="White" table-layout="fixed" width="100%" >				
			<fo:table-column column-width="100%" />				

			<fo:table-body>				
				<fo:table-row padding="1mm" font-size="8pt">
					<fo:table-cell>
						<fo:block text-align="justify" padding="2.5mm">
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="6%" />
								<fo:table-column column-width="94%" />
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding=".30mm">
											<fo:block text-align="left" font-size="8pt">
												<xsl:value-of select="fopUtil:getLocalizedString('Picked By / Cueillette effectuée par : ',$locale)" />
											</fo:block>
										</fo:table-cell >
										<fo:table-cell padding=".30mm">
											<fo:block text-align="left" font-size="8pt">
												<xsl:value-of select="fopUtil:getLocalizedString('____________',$locale)"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>


	</xsl:template>


	<xsl:template name="addEmptyBlock">
		<xsl:param name="hightinpt" />
		<fo:table width="100%">
			<fo:table-column column-width="100%" />
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell>
						<fo:block white-space-treatment="preserve"
							linefeed-treatment="preserve">
							<xsl:text> &#x00A0;</xsl:text>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
</xsl:stylesheet>
