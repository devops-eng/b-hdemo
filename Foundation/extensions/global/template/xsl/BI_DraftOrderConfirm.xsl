<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<Order>
			<xsl:attribute name="DocumentType">
				<xsl:value-of select="Order/@DocumentType"/>
			</xsl:attribute>
			<xsl:attribute name="EnterpriseCode">
				<xsl:value-of select="Order/@EnterpriseCode"/>
			</xsl:attribute>
			<xsl:attribute name="EntryType">
				<xsl:value-of select="Order/@EntryType"/>
			</xsl:attribute>
			<xsl:attribute name="OrderDate">
				<xsl:value-of select="Order/@OrderDate"/>
			</xsl:attribute>
			<xsl:attribute name="OrderNo">
				<xsl:value-of select="Order/@OrderNo"/>
			</xsl:attribute>
			<xsl:attribute name="OriginalTotalAmount">
				<xsl:value-of select="Order/@OriginalTotalAmount"/>
			</xsl:attribute>
			<PriceInfo>
				<xsl:attribute name="Currency">
					<xsl:value-of select="Order/PriceInfo/@Currency"/>
				</xsl:attribute>
				<xsl:attribute name="EnterpriseCurrency">
					<xsl:value-of select="Order/PriceInfo/@EnterpriseCurrency"/>
				</xsl:attribute>
				<xsl:attribute name="ReportingConversionDate">
					<xsl:value-of select="Order/PriceInfo/@ReportingConversionDate"/>
				</xsl:attribute>
				<xsl:attribute name="ReportingConversionRate">
					<xsl:value-of select="Order/PriceInfo/@ReportingConversionRate"/>
				</xsl:attribute>
				<xsl:attribute name="TotalAmount">
					<xsl:value-of select="Order/PriceInfo/@TotalAmount"/>
				</xsl:attribute>
			</PriceInfo>
			<OrderLines>
			 <xsl:for-each select="Order/OrderLines/OrderLine">
   <xsl:value-of select="."/>
				<OrderLine>
					<xsl:attribute name="CarrierServiceCode">
						<xsl:value-of select="Order/OrderLines/OrderLine/@CarrierServiceCode"/>
					</xsl:attribute>
					<xsl:attribute name="DeliveryMethod">
						<xsl:value-of select="Order/OrderLines/OrderLine/@DeliveryMethod"/>
					</xsl:attribute>
					<xsl:attribute name="ItemGroupCode">
						<xsl:value-of select="Order/OrderLines/OrderLine/@ItemGroupCode"/>
					</xsl:attribute>
					<xsl:attribute name="OrderedQty">
						<xsl:value-of select="Order/OrderLines/OrderLine/@OrderedQty"/>
					</xsl:attribute>
					<xsl:attribute name="PrimeLineNo">
						<xsl:value-of select="Order/OrderLines/OrderLine/@PrimeLineNo"/>
					</xsl:attribute>
					<Item>
						<xsl:attribute name="ItemID">
							<xsl:value-of select="Order/OrderLines/OrderLine/Item/@ItemID"/>
						</xsl:attribute>
						<xsl:attribute name="ItemShortDesc">
							<xsl:value-of select="Order/OrderLines/OrderLine/Item/@ItemShortDesc"/>
						</xsl:attribute>
						<xsl:attribute name="UnitOfMeasure">
							<xsl:value-of select="Order/OrderLines/OrderLine/Item/@UnitOfMeasure"/>
						</xsl:attribute>
					</Item>
					<LineOverallTotals>
						<xsl:attribute name="Charges">
							<xsl:value-of select="Order/OrderLines/OrderLine/LineOverallTotals/@Charges"/>
						</xsl:attribute>
						<xsl:attribute name="Discount">
							<xsl:value-of select="Order/OrderLines/OrderLine/LineOverallTotals/@Discount"/>
						</xsl:attribute>
						<xsl:attribute name="ExtendedPrice">
							<xsl:value-of select="Order/OrderLines/OrderLine/LineOverallTotals/@ExtendedPrice"/>
						</xsl:attribute>
						<xsl:attribute name="LineTotal">
							<xsl:value-of select="Order/OrderLines/OrderLine/LineOverallTotals/@LineTotal"/>
						</xsl:attribute>
						<xsl:attribute name="OptionPrice">
							<xsl:value-of select="Order/OrderLines/OrderLine/LineOverallTotals/@OptionPrice"/>
						</xsl:attribute>
						<xsl:attribute name="PricingQty">
							<xsl:value-of select="Order/OrderLines/OrderLine/LineOverallTotals/@PricingQty"/>
						</xsl:attribute>
						<xsl:attribute name="Tax">
							<xsl:value-of select="Order/OrderLines/OrderLine/LineOverallTotals/@Tax"/>
						</xsl:attribute>
						<xsl:attribute name="UnitPrice">
							<xsl:value-of select="Order/OrderLines/OrderLine/LineOverallTotals/@UnitPrice"/>
						</xsl:attribute>
					</LineOverallTotals>
				</OrderLine>
				</xsl:for-each>
			</OrderLines>
		</Order>
	</xsl:template>
</xsl:stylesheet>