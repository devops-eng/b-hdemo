<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">
	<xsl:template match="/">
		<Order>
			<xsl:attribute name="DocumentType">
				<xsl:value-of select="Order/@DocumentType"/>
			</xsl:attribute>
			<xsl:attribute name="EnterpriseCode">
				<xsl:value-of select="Order/@EnterpriseCode"/>
			</xsl:attribute>
			<xsl:attribute name="EntryType">
				<xsl:value-of select="Order/@EntryType"/>
			</xsl:attribute>
			
			<xsl:attribute name="OrderNo">
				<xsl:value-of select="Order/@OrderNo"/>
			</xsl:attribute>
			
			<xsl:attribute name="OrderHeaderKey">
				<xsl:value-of select="Order/@OrderHeaderKey"/>
			</xsl:attribute>
			<Extn>
			</Extn>
		</Order>
	</xsl:template>
</xsl:stylesheet>