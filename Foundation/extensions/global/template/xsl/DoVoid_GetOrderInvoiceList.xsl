<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:template match="/Order">
		<OrderInvoice>
			<xsl:attribute name="OrderHeaderKey">
				<xsl:value-of select="@OrderHeaderKey"/>
			</xsl:attribute>
		</OrderInvoice>
	</xsl:template>    
</xsl:stylesheet>