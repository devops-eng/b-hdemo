<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<Order>
			<xsl:attribute name="AllocationRuleID">
				<xsl:value-of select="Order/@AllocationRuleID"/>
			</xsl:attribute>
			<xsl:if test="(Order/ChargeTransactionDetails/ChargeTransactionDetail/PaymentMethod/@PaymentType = 'CREDIT_CARD_LT' 
				or Order/ChargeTransactionDetails/ChargeTransactionDetail/PaymentMethod/@PaymentType = 'CREDIT_CARD_PVT_LT' 
				or Order/ChargeTransactionDetails/ChargeTransactionDetail/PaymentMethod/@PaymentType = 'CREDIT_CARD_HBC' 
				or Order/ChargeTransactionDetails/ChargeTransactionDetail/PaymentMethod/@PaymentType = 'CREDIT_CARD_PVT_HBC')" >
			<xsl:attribute name="AuthorizationExpirationDate">
				<xsl:value-of select="Order/@AuthorizationExpirationDate"/>
			</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="BillToID">
				<xsl:value-of select="Order/@BillToID"/>
			</xsl:attribute>
			<xsl:attribute name="ChargeActualFreightFlag">
				<xsl:value-of select="Order/@ChargeActualFreightFlag"/>
			</xsl:attribute>
			<xsl:attribute name="CustomerEMailID">
				<xsl:value-of select="Order/@CustomerEMailID"/>
			</xsl:attribute>
			<xsl:attribute name="DocumentType">
				<xsl:value-of select="Order/@DocumentType"/>
			</xsl:attribute>
			<xsl:attribute name="EnteredBy">
				<xsl:value-of select="Order/@EnteredBy"/>
			</xsl:attribute>
			<xsl:attribute name="EnterpriseCode">
				<xsl:value-of select="Order/@EnterpriseCode"/>
			</xsl:attribute>
			<xsl:attribute name="EntryType">
				<xsl:value-of select="Order/@EntryType"/>
			</xsl:attribute>
			<xsl:attribute name="OrderDate">
				<xsl:value-of select="Order/@OrderDate"/>
			</xsl:attribute>
			<xsl:attribute name="OrderHeaderKey">
				<xsl:value-of select="Order/@OrderHeaderKey"/>
			</xsl:attribute>
			<xsl:attribute name="OrderNo">
				<xsl:value-of select="Order/@OrderNo"/>
			</xsl:attribute>
			<xsl:attribute name="PaymentStatus">
				<xsl:value-of select="Order/@PaymentStatus"/>
			</xsl:attribute>
			<xsl:attribute name="SellerOrganizationCode">
				<xsl:value-of select="Order/@SellerOrganizationCode"/>
			</xsl:attribute>
			<xsl:attribute name="ShipToID">
				<xsl:value-of select="Order/@ShipToID"/>
			</xsl:attribute>
			<OrderLines>
				<xsl:attribute name="NumberOfOrderLines">
					<xsl:value-of select="Order/OrderLines/@TotalNumberOfRecords"/>
				</xsl:attribute>
				<xsl:for-each select="Order/OrderLines/OrderLine">
					<OrderLine>
						<xsl:attribute name="CarrierServiceCode">
							<xsl:value-of select="@CarrierServiceCode"/>
						</xsl:attribute>
						<xsl:attribute name="DepartmentCode">
							<xsl:value-of select="@DepartmentCode"/>
						</xsl:attribute>
						<xsl:attribute name="OrderedQty">
							<xsl:value-of select="@OrderedQty"/>
						</xsl:attribute>
						<xsl:attribute name="PrimeLineNo">
							<xsl:value-of select="@PrimeLineNo"/>
						</xsl:attribute>						
						<Item>
							<xsl:attribute name="ItemDesc">
								<xsl:value-of select="Item/@ItemDesc"/>
							</xsl:attribute>
							<xsl:attribute name="ItemID">
								<xsl:value-of select="Item/@ItemID"/>
							</xsl:attribute>
							<xsl:attribute name="ItemShortDesc">
								<xsl:value-of select="Item/@ItemShortDesc"/>
							</xsl:attribute>
							<xsl:attribute name="UnitPrice">
								<xsl:value-of select="Extn/@ExtnDiscountedUnitPrice"/>
							</xsl:attribute>							
						</Item>					
					</OrderLine>
				</xsl:for-each>
			</OrderLines>
			<PersonInfoShipTo>
				<xsl:attribute name="AddressLine1">
					<xsl:value-of select="Order/PersonInfoShipTo/@AddressLine1"/>
				</xsl:attribute>
				<xsl:attribute name="AddressLine2">
					<xsl:value-of select="Order/PersonInfoShipTo/@AddressLine2"/>
				</xsl:attribute>
				<xsl:attribute name="AddressLine3">
					<xsl:value-of select="Order/PersonInfoShipTo/@AddressLine3"/>
				</xsl:attribute>
				<xsl:attribute name="AddressLine4">
					<xsl:value-of select="Order/PersonInfoShipTo/@AddressLine4"/>
				</xsl:attribute>
				<xsl:attribute name="City">
					<xsl:value-of select="Order/PersonInfoShipTo/@City"/>
				</xsl:attribute>
				<xsl:attribute name="Company">
					<xsl:value-of select="Order/PersonInfoShipTo/@Company"/>
				</xsl:attribute>
				<xsl:attribute name="Country">
					<xsl:value-of select="Order/PersonInfoShipTo/@Country"/>
				</xsl:attribute>
				<xsl:attribute name="DayPhone">
					<xsl:value-of select="Order/PersonInfoShipTo/@DayPhone"/>
				</xsl:attribute>
				<xsl:attribute name="EMailID">
					<xsl:value-of select="Order/PersonInfoShipTo/@EMailID"/>
				</xsl:attribute>
				<xsl:attribute name="EveningPhone">
					<xsl:value-of select="Order/PersonInfoShipTo/@EveningPhone"/>
				</xsl:attribute>
				<xsl:attribute name="FirstName">
					<xsl:value-of select="Order/PersonInfoShipTo/@FirstName"/>
				</xsl:attribute>
				<xsl:attribute name="LastName">
					<xsl:value-of select="Order/PersonInfoShipTo/@LastName"/>
				</xsl:attribute>
				<xsl:attribute name="MiddleName">
					<xsl:value-of select="Order/PersonInfoShipTo/@MiddleName"/>
				</xsl:attribute>
				<xsl:attribute name="MobilePhone">
					<xsl:value-of select="Order/PersonInfoShipTo/@MobilePhone"/>
				</xsl:attribute>
				<xsl:attribute name="OtherPhone">
					<xsl:value-of select="Order/PersonInfoShipTo/@OtherPhone"/>
				</xsl:attribute>
				<xsl:attribute name="State">
					<xsl:value-of select="Order/PersonInfoShipTo/@State"/>
				</xsl:attribute>
				<xsl:attribute name="Suffix">
					<xsl:value-of select="Order/PersonInfoShipTo/@Suffix"/>
				</xsl:attribute>
				<xsl:attribute name="Title">
					<xsl:value-of select="Order/PersonInfoShipTo/@Title"/>
				</xsl:attribute>
				<xsl:attribute name="ZipCode">
					<xsl:value-of select="Order/PersonInfoShipTo/@ZipCode"/>
				</xsl:attribute>
			</PersonInfoShipTo>
			<xsl:if test="(Order/ChargeTransactionDetails/ChargeTransactionDetail/PaymentMethod/@PaymentType = 'CREDIT_CARD_LT' 
				or Order/ChargeTransactionDetails/ChargeTransactionDetail/PaymentMethod/@PaymentType = 'CREDIT_CARD_PVT_LT' 
				or Order/ChargeTransactionDetails/ChargeTransactionDetail/PaymentMethod/@PaymentType = 'CREDIT_CARD_HBC' 
				or Order/ChargeTransactionDetails/ChargeTransactionDetail/PaymentMethod/@PaymentType = 'CREDIT_CARD_PVT_HBC')" >
			<PersonInfoBillTo>
				<xsl:attribute name="AddressLine1">
					<xsl:value-of select="Order/PersonInfoBillTo/@AddressLine1"/>
				</xsl:attribute>
				<xsl:attribute name="AddressLine2">
					<xsl:value-of select="Order/PersonInfoBillTo/@AddressLine2"/>
				</xsl:attribute>
				<xsl:attribute name="AddressLine3">
					<xsl:value-of select="Order/PersonInfoBillTo/@AddressLine3"/>
				</xsl:attribute>
				<xsl:attribute name="AddressLine4">
					<xsl:value-of select="Order/PersonInfoBillTo/@AddressLine4"/>
				</xsl:attribute>
				<xsl:attribute name="City">
					<xsl:value-of select="Order/PersonInfoBillTo/@City"/>
				</xsl:attribute>
				<xsl:attribute name="Company">
					<xsl:value-of select="Order/PersonInfoBillTo/@Company"/>
				</xsl:attribute>
				<xsl:attribute name="Country">
					<xsl:value-of select="Order/PersonInfoBillTo/@Country"/>
				</xsl:attribute>
				<xsl:attribute name="DayPhone">
					<xsl:value-of select="Order/PersonInfoBillTo/@DayPhone"/>
				</xsl:attribute>
				<xsl:attribute name="EMailID">
					<xsl:value-of select="Order/PersonInfoBillTo/@EMailID"/>
				</xsl:attribute>
				<xsl:attribute name="EveningFaxNo">
					<xsl:value-of select="Order/PersonInfoBillTo/@EveningFaxNo"/>
				</xsl:attribute>
				<xsl:attribute name="EveningPhone">
					<xsl:value-of select="Order/PersonInfoBillTo/@EveningPhone"/>
				</xsl:attribute>
				<xsl:attribute name="FirstName">
					<xsl:value-of select="Order/PersonInfoBillTo/@FirstName"/>
				</xsl:attribute>
				<xsl:attribute name="LastName">
					<xsl:value-of select="Order/PersonInfoBillTo/@LastName"/>
				</xsl:attribute>
				<xsl:attribute name="MiddleName">
					<xsl:value-of select="Order/PersonInfoBillTo/@MiddleName"/>
				</xsl:attribute>
				<xsl:attribute name="MobilePhone">
					<xsl:value-of select="Order/PersonInfoBillTo/@MobilePhone"/>
				</xsl:attribute>
				<xsl:attribute name="OtherPhone">
					<xsl:value-of select="Order/PersonInfoBillTo/@OtherPhone"/>
				</xsl:attribute>
				<xsl:attribute name="PersonID">
					<xsl:value-of select="Order/PersonInfoBillTo/@PersonID"/>
				</xsl:attribute>
				<xsl:attribute name="State">
					<xsl:value-of select="Order/PersonInfoBillTo/@State"/>
				</xsl:attribute>
				<xsl:attribute name="Suffix">
					<xsl:value-of select="Order/PersonInfoBillTo/@Suffix"/>
				</xsl:attribute>
				<xsl:attribute name="Title">
					<xsl:value-of select="Order/PersonInfoBillTo/@Title"/>
				</xsl:attribute>
				<xsl:attribute name="ZipCode">
					<xsl:value-of select="Order/PersonInfoBillTo/@ZipCode"/>
				</xsl:attribute>
			</PersonInfoBillTo>
			</xsl:if>
			<ChargeTransactionDetails>
				<xsl:for-each select="Order/ChargeTransactionDetails/ChargeTransactionDetail">
					<ChargeTransactionDetail>
						<xsl:attribute name="AuditTransactionID">
							<xsl:value-of select="@AuditTransactionID"/>
						</xsl:attribute>
						<xsl:if test="(.//PaymentMethod/@PaymentType = 'CREDIT_CARD_LT' 
						or .//PaymentMethod/@PaymentType = 'CREDIT_CARD_PVT_LT' 
				or .//PaymentMethod/@PaymentType = 'CREDIT_CARD_HBC' 
				or .//PaymentMethod/@PaymentType = 'CREDIT_CARD_PVT_HBC')" >
						<xsl:attribute name="AuthorizationExpirationDate">
							<xsl:value-of select="@AuthorizationExpirationDate"/>
						</xsl:attribute>
						</xsl:if>
						<xsl:attribute name="AuthorizationID">
							<xsl:value-of select="@AuthorizationID"/>
						</xsl:attribute>
						<CreditCardTransactions>
							<xsl:for-each select="CreditCardTransactions/CreditCardTransaction">
								<CreditCardTransaction>
									<xsl:attribute name="AuthAmount">
										<xsl:value-of select="@AuthAmount"/>
									</xsl:attribute>
									<xsl:attribute name="AuthAvs">
										<xsl:value-of select="@AuthAvs"/>
									</xsl:attribute>
									<xsl:attribute name="AuthCode">
										<xsl:value-of select="@AuthCode"/>
									</xsl:attribute>
									<xsl:attribute name="sCVVAuthCode">
										<xsl:value-of select="@CVVAuthCode"/>
									</xsl:attribute>
									<xsl:attribute name="AuthReturnCode">
										<xsl:value-of select="@AuthReturnCode"/>
									</xsl:attribute>
									<xsl:attribute name="AuthReturnMessage">
										<xsl:value-of select="@AuthReturnMessage"/>
									</xsl:attribute>
									<xsl:attribute name="AuthTime">
										<xsl:value-of select="@AuthTime"/>
									</xsl:attribute>
									<xsl:attribute name="Reference1">
										<xsl:value-of select="@Reference1"/>
									</xsl:attribute>
									<xsl:attribute name="Reference2">
										<xsl:value-of select="@Reference2"/>
									</xsl:attribute>
									<xsl:attribute name="RequestId">
										<xsl:value-of select="@RequestId"/>
									</xsl:attribute>
									<xsl:attribute name="TranAmount">
										<xsl:value-of select="@TranAmount"/>
									</xsl:attribute>
									<xsl:attribute name="TranRequestTime">
										<xsl:value-of select="@TranRequestTime"/>
									</xsl:attribute>
									<xsl:attribute name="TranType">
										<xsl:value-of select="@TranType"/>
									</xsl:attribute>
								</CreditCardTransaction>
							</xsl:for-each>
						</CreditCardTransactions>
						<PaymentMethod>
							
							<xsl:attribute name="CreditCardNo">
								<xsl:value-of select="PaymentMethod/@SvcNo"/>
							</xsl:attribute>
							<xsl:attribute name="CreditCardType">
								<xsl:value-of select="PaymentMethod/@CreditCardType"/>
							</xsl:attribute>
							<xsl:if test="(PaymentMethod/@PaymentType = 'CREDIT_CARD_LT' 
						or PaymentMethod/@PaymentType = 'CREDIT_CARD_PVT_LT' 
				or PaymentMethod/@PaymentType = 'CREDIT_CARD_HBC' 
				or PaymentMethod/@PaymentType = 'CREDIT_CARD_PVT_HBC')" >
				<xsl:attribute name="CreditCardNo">
								<xsl:value-of select="PaymentMethod/@CreditCardNo"/>
							</xsl:attribute>
							<xsl:attribute name="CreditCardExpDate">
								<xsl:value-of select="PaymentMethod/@CreditCardExpDate"/>
							</xsl:attribute>
							<xsl:attribute name="CreditCardName">
								<xsl:value-of select="PaymentMethod/@CreditCardName"/>
							</xsl:attribute>
							<xsl:attribute name="CustomerAccountNo">
								<xsl:value-of select="PaymentMethod/@CustomerAccountNo"/>
							</xsl:attribute>
				</xsl:if>
							
							<xsl:attribute name="DisplayCreditCardNo">
								<xsl:value-of select="PaymentMethod/@DisplayCreditCardNo"/>
							</xsl:attribute>
							<xsl:attribute name="MaxChargeLimit">
								<xsl:value-of select="PaymentMethod/@MaxChargeLimit"/>
							</xsl:attribute>
							<xsl:attribute name="PaymentKey">
								<xsl:value-of select="PaymentMethod/@PaymentKey"/>
							</xsl:attribute>
							<xsl:attribute name="PaymentReference1">
								<xsl:value-of select="PaymentMethod/@PaymentReference1"/>
							</xsl:attribute>
							<xsl:attribute name="PaymentReference2">
								<xsl:value-of select="PaymentMethod/@PaymentReference2"/>
							</xsl:attribute>
							<xsl:attribute name="PaymentReference3">
								<xsl:value-of select="PaymentMethod/@PaymentReference3"/>
							</xsl:attribute>
							<xsl:attribute name="PaymentType">
								<xsl:value-of select="PaymentMethod/@PaymentType"/>
							</xsl:attribute>
							<xsl:attribute name="TotalAuthorized">
								<xsl:value-of select="PaymentMethod/@TotalAuthorized"/>
							</xsl:attribute>							
						</PaymentMethod>	
					</ChargeTransactionDetail>
				</xsl:for-each>
			</ChargeTransactionDetails>
			<OverallTotals>
				<xsl:attribute name="GrandDiscount">
					<xsl:value-of select="Order/OverallTotals/@GrandDiscount"/>
				</xsl:attribute>
				<xsl:attribute name="GrandShippingTotal">
					<xsl:value-of select="Order/OverallTotals/@GrandShippingTotal"/>
				</xsl:attribute>
				<xsl:attribute name="GrandTax">
					<xsl:value-of select="Order/OverallTotals/@GrandTax"/>
				</xsl:attribute>
				<xsl:attribute name="GrandTotal">
					<xsl:value-of select="Order/OverallTotals/@GrandTotal"/>
				</xsl:attribute>
				<xsl:attribute name="HdrShippingCharges">
					<xsl:value-of select="Order/OverallTotals/@HdrShippingCharges"/>
				</xsl:attribute>
			</OverallTotals>
		</Order>
	</xsl:template>
</xsl:stylesheet>