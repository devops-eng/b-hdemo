<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
<Payment>
 
 		 		  <xsl:attribute name="authReturnCode">
			<xsl:value-of select="'1'"/></xsl:attribute>
			<xsl:attribute name="authCode">
			<xsl:value-of select="'1'"/></xsl:attribute>
  			  <xsl:attribute name="authorizationId">
			<xsl:value-of select="Payment/@authorizationId"/></xsl:attribute>
	<xsl:attribute name="authAVS">
			<xsl:value-of select="'111'"/></xsl:attribute>
			<xsl:attribute name="sCVVAuthCode">
			<xsl:value-of select="'1'"/></xsl:attribute>
<xsl:attribute name="authReturnMessage">
			<xsl:value-of select="'Approved'"/></xsl:attribute>
		<xsl:attribute name="tranReturnMessage">
			<xsl:value-of select="'Approved'"/></xsl:attribute>
<xsl:attribute name="authorizationExpirationDate">
			<xsl:value-of select="'25000101'"/></xsl:attribute>
<xsl:attribute name="requestAmount">
			<xsl:value-of select="Payment/@RequestAmount"/></xsl:attribute>
			<xsl:attribute name="tranReturnFlag">
			<xsl:value-of select="'Y'"/></xsl:attribute>
				<xsl:attribute name="authReturnFlag">
			<xsl:value-of select="'Y'"/></xsl:attribute>
					<xsl:attribute name="retryFlag">
			<xsl:value-of select="'N'"/></xsl:attribute>
			<xsl:attribute name="PaymentReference1">
			<xsl:value-of select="Payment/@PaymentReference1"/></xsl:attribute>
<xsl:attribute name="PaymentReference2">
			<xsl:value-of select="Payment/@PaymentReference2"/></xsl:attribute>
<xsl:attribute name="PaymentReference3">
			<xsl:value-of select="Payment/@PaymentReference3"/></xsl:attribute>


</Payment>


</xsl:template>


</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2004. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="GiftCardIP.xml" htmlbaseurl="" outputurl="GiftCardOP.xml" processortype="internal" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->