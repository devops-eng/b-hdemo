<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
		<OrderStatusChange>
			<xsl:attribute name="OrderHeaderKey">
				<xsl:value-of select="OrderRelease/@OrderHeaderKey"/>
			</xsl:attribute>
			<xsl:attribute name="BaseDropStatus">
				<xsl:value-of select="'3900'"/>
			</xsl:attribute>
                        <xsl:attribute name="TransactionId">
				<xsl:value-of select="'RECEIVE_ORDER.0003'"/>
			</xsl:attribute>
			 <OrderLines>
			 <xsl:for-each select="/OrderRelease/OrderLine">
					<OrderLine>
			 <xsl:attribute name="PrimeLineNo">
							<xsl:value-of select="./@PrimeLineNo"/>
						</xsl:attribute>
						<xsl:attribute name="Quantity">
							<xsl:value-of select="./@OrderedQty"/>
						</xsl:attribute>
						<xsl:attribute name="SubLineNo">
							<xsl:value-of select="./@SubLineNo"/>
						</xsl:attribute>
						<xsl:attribute name="OrderLineKey">
							<xsl:value-of select="./@OrderLineKey"/>
						</xsl:attribute>
						<xsl:attribute name="OrderReleaseKey">
							<xsl:value-of select="/OrderRelease/@OrderReleaseKey"/>
						</xsl:attribute>
						<xsl:attribute name="BaseDropStatus">
							<xsl:value-of select="'3900'"/>
						</xsl:attribute>
					</OrderLine>
					</xsl:for-each>
			</OrderLines>
		</OrderStatusChange>	
	</xsl:template>
</xsl:stylesheet>
