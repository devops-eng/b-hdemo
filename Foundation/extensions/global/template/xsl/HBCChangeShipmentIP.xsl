<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
						<Shipment>
			<xsl:attribute name="Action">
				<xsl:value-of select="'Modify'"/>
			</xsl:attribute>
			<xsl:attribute name="SellerOrganizationCode">
				<xsl:value-of select="/MonitorConsolidation/Shipment/@SellerOrganizationCode"/>
			</xsl:attribute>
			<xsl:attribute name="CancelShipmentOnZeroTotalQuantity">
				<xsl:value-of select="'Y'"/>
			</xsl:attribute>
			<xsl:attribute name="ShipmentKey">
				<xsl:value-of select="/MonitorConsolidation/Shipment/@ShipmentKey"/>
			</xsl:attribute>
			<xsl:attribute name="BackOrderRemovedQuantity">
				<xsl:value-of select="'Y'"/>
			</xsl:attribute>
		
					 <ShipmentLines>
			 <xsl:for-each select="/MonitorConsolidation/Shipment/ShipmentLines/ShipmentLine">
					<ShipmentLine>
			 <xsl:attribute name="OrderNo">
							<xsl:value-of select="/MonitorConsolidation/Shipment/@OrderNo"/>
						</xsl:attribute>
						<xsl:attribute name="Quantity">
							<xsl:value-of select="'0'"/>
						</xsl:attribute>
						<xsl:attribute name="PrimeLineNo">
							<xsl:value-of select="./OrderLine/@PrimeLineNo"/>
						</xsl:attribute>
						<xsl:attribute name="SubLineNo">
							<xsl:value-of select="./OrderLine/@SubLineNo"/>
						</xsl:attribute>
						<xsl:attribute name="EnterpriseCode">
							<xsl:value-of select="/MonitorConsolidation/Shipment/@EnterpriseCode"/>
						</xsl:attribute>
						<xsl:attribute name="ShipmentLineKey">
							<xsl:value-of select="./@ShipmentLineKey"/>
						</xsl:attribute>
							<xsl:attribute name="DocumentType">
							<xsl:value-of select="/MonitorConsolidation/Shipment/@DocumentType"/>
						</xsl:attribute>
										</ShipmentLine>
					</xsl:for-each>
			</ShipmentLines>
		</Shipment>	
	</xsl:template>
</xsl:stylesheet>
