<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
	<xsl:template match="/">
		<ItemList>
			<AttributeList>
				<Attribute>
					<xsl:attribute name="AttributeID">Color</xsl:attribute>
					<xsl:attribute name="SequenceNo">100</xsl:attribute>
					<xsl:attribute name="ShortDescription">
						<xsl:value-of select="ItemList/Item[1]/ItemAttributeGroupTypeList[1]/ItemAttributeGroupType[1]/ItemAttributeGroupList[1]/ItemAttributeGroup[1]/ItemAttributeList[1]/ItemAttribute[@ItemAttributeName='Color']/Attribute/@ShortDescription"/>
					</xsl:attribute>
					<AssignedValueList>
						<xsl:for-each select="ItemList/Item">
							<xsl:for-each select="ItemAttributeGroupTypeList/ItemAttributeGroupType/ItemAttributeGroupList/ItemAttributeGroup/ItemAttributeList/ItemAttribute">
								<xsl:if test="Attribute/@AttributeID='Color'">
									<xsl:for-each select="AssignedValueList/AssignedValue">
									<xsl:if test ="not(preceding::AssignedValue[@Value = current()/@Value])">
										<AssignedValue>
											<xsl:attribute name="DisplayAttributeValue">
												<xsl:value-of select="@ShortDescription"/>
											</xsl:attribute>
											<xsl:copy-of select="@*" />
										</AssignedValue>
										</xsl:if>
									</xsl:for-each>
								</xsl:if>
							</xsl:for-each>
						</xsl:for-each>
					</AssignedValueList>
				</Attribute>
				<Attribute>
					<xsl:attribute name="AttributeID">Size</xsl:attribute>
					<xsl:attribute name="SequenceNo">200</xsl:attribute>
					<xsl:attribute name="ShortDescription">
						<xsl:value-of select="ItemList/Item[1]/ItemAttributeGroupTypeList[1]/ItemAttributeGroupType[1]/ItemAttributeGroupList[1]/ItemAttributeGroup[1]/ItemAttributeList[1]/ItemAttribute[@ItemAttributeName='Size']/Attribute/@ShortDescription"/>
					</xsl:attribute>
					<AssignedValueList>
						<xsl:for-each select="ItemList/Item">
							<xsl:for-each select="ItemAttributeGroupTypeList/ItemAttributeGroupType/ItemAttributeGroupList/ItemAttributeGroup/ItemAttributeList/ItemAttribute">
								<xsl:if test="Attribute/@AttributeID='Size'">
									<xsl:for-each select="AssignedValueList/AssignedValue">
										<xsl:if test ="not(preceding::AssignedValue[@Value = current()/@Value])">
										<AssignedValue>
											<xsl:attribute name="DisplayAttributeValue">
												<xsl:value-of select="@ShortDescription"/>
											</xsl:attribute>
											<xsl:copy-of select="@*" />
										</AssignedValue>
										</xsl:if>
									</xsl:for-each>
								</xsl:if>
							</xsl:for-each>
						</xsl:for-each>
					</AssignedValueList>
				</Attribute>
			</AttributeList>
			<AllowedCombinationList>
				<xsl:for-each select="ItemList/Item">
					<AllowedCombination>
					<xsl:attribute name="ItemID">
						<xsl:value-of select="@ItemID"/>
					</xsl:attribute>
					<ItemAttributeList>
						<xsl:for-each select="ItemAttributeGroupTypeList/ItemAttributeGroupType/ItemAttributeGroupList/ItemAttributeGroup/ItemAttributeList/ItemAttribute">
							<ItemAttribute>
								<xsl:copy-of select="@*" />
								<xsl:attribute name="DisplayAttributeValue">
									<xsl:value-of select="AssignedValueList/AssignedValue/@ShortDescription"/>
								</xsl:attribute>
							</ItemAttribute>
						</xsl:for-each>
					</ItemAttributeList>
					</AllowedCombination>
				</xsl:for-each>
			</AllowedCombinationList>
		</ItemList>
	</xsl:template>
</xsl:stylesheet>