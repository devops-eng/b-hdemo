<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<Order>
			<xsl:attribute name="DocumentType">
				<xsl:value-of select="//Order/@DocumentType"/>
			</xsl:attribute>
			<xsl:attribute name="EnterpriseCode">
				<xsl:value-of select="//Order/@EnterpriseCode"/>
			</xsl:attribute>
			<xsl:attribute name="OrderHeaderKey">
				<xsl:value-of select="//Order/@OrderHeaderKey"/>
			</xsl:attribute>
			<xsl:attribute name="RefundPaymentMethods">
				<xsl:value-of select="//Order/@OrderNo"/>
			</xsl:attribute>
			<RefundPaymentMethods>
				<xsl:for-each select="Order/RefundPaymentMethods/RefundPaymentMethod">
					<xsl:choose>
						<xsl:when test="./@PaymentType = 'CREDIT_CARD_HBC' or ./@PaymentType = 'CREDIT_CARD_PVT_HBC' or ./@PaymentType = 'PAYPAL_HBC' or ./@PaymentType = 'CREDIT_CARD_LT' or ./@PaymentType = 'CREDIT_CARD_PVT_LT' or ./@PaymentType = 'PAYPAL_LT'">
							<RefundPaymentMethod>
								<xsl:copy-of select="./@*"/>
								<RefundForPaymentMethods>
									<RefundForPaymentMethod>
										<xsl:attribute name="PaymentKey">
											<xsl:value-of select="./@PaymentKey"/>
										</xsl:attribute>
										<xsl:attribute name="RefundAmount">
											<xsl:value-of select="./@RefundAmount"/>
										</xsl:attribute>
									</RefundForPaymentMethod>
								</RefundForPaymentMethods>
								<RefundFulfillmentDetails>
									<xsl:attribute name="ItemID">
										<xsl:value-of select="/Order/@ValueCodeLongDesc"/>
									</xsl:attribute>
									<xsl:attribute name="UnitOfMeasure">
										<xsl:value-of select="/Order/@ValueCodeShortDesc"/>
									</xsl:attribute>
								</RefundFulfillmentDetails>
								<PersonInfoBillTo>
									<xsl:copy-of select="/Order/PersonInfoShipTo/@*"/>									
								</PersonInfoBillTo>
								<PersonInfoShipTo>
									<xsl:copy-of select="/Order/PersonInfoShipTo/@*"/>									
								</PersonInfoShipTo>
							</RefundPaymentMethod>
						</xsl:when>
						<xsl:otherwise>
							<RefundPaymentMethod>
								<xsl:copy-of select="./@*"/>
							</RefundPaymentMethod>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</RefundPaymentMethods>
		</Order>
	</xsl:template>
</xsl:stylesheet>