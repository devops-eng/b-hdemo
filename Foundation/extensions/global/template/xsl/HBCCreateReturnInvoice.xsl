<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<Order>
						<xsl:attribute name="OrderHeaderKey">
				<xsl:value-of select="/OrderStatusChange/@OrderHeaderKey"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionId">
				<xsl:value-of select="'CREATE_ORDER_INVOICE.0003'"/>
			</xsl:attribute>
			<OrderLines>
				<xsl:for-each select="/OrderStatusChange/OrderLines/OrderLine">
					<OrderLine>
						<xsl:attribute name="PrimeLineNo">
							<xsl:value-of select="./@PrimeLineNo"/>
						</xsl:attribute>
						<xsl:attribute name="Quantity">
							<xsl:value-of select="./@Quantity"/>
						</xsl:attribute>
						<xsl:attribute name="SubLineNo">
							<xsl:value-of select="./@SubLineNo"/>
						</xsl:attribute>
					</OrderLine>
				</xsl:for-each>
			</OrderLines>
		</Order>
	</xsl:template>
</xsl:stylesheet>
