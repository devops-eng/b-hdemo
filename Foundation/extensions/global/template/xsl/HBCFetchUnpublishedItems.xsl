<?xml version="1.0"?>
<!-- Defect428 : XSL to fetch unpublished items based on the UPC scanned -->
<!-- This XSL is used to form the input to getItemList API to get the unpublished items -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<Item>
			<xsl:attribute name="GetUnpublishedItems">
				<xsl:value-of select="'Y'"/>
			</xsl:attribute>
			<xsl:attribute name="OrganizationCode">
				<xsl:value-of select="//BarCode/ContextualInfo/@OrganizationCode"/>
			</xsl:attribute>
			<ItemAliasList>
				<ItemAlias>
					<xsl:attribute name="AliasName">
						<xsl:value-of select="'UPC'"/>
					</xsl:attribute>
					<xsl:attribute name="AliasValue">
						<xsl:value-of select="//BarCode/@BarCodeData"/>
					</xsl:attribute>
				</ItemAlias>
			</ItemAliasList>
		</Item>
	</xsl:template>
</xsl:stylesheet>
