<?xml version="1.0"?>
<!-- Defect428 : XSL to fetch unpublished items based on the UPC scanned -->
<!-- This XSL is used to translate the getItemList Output to translateBarcode Output -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<BarCode>
			<xsl:attribute name="BarCodeData">
				<xsl:value-of select="//Item/ItemAliasList/ItemAlias[./@AliasName='UPC']/@AliasValue"/>
			</xsl:attribute>
			<xsl:attribute name="BarCodeType">
				<xsl:value-of select="'Item'"/>
			</xsl:attribute>
			<Translations>
				<xsl:attribute name="BarCodeTranslationSource">
					<xsl:value-of select="'ItemAlias'"/>
				</xsl:attribute>
        <xsl:choose>
				<xsl:when test="(//ItemID != ' ' or //ItemID != null)" >
				<xsl:attribute name="TotalNumberOfRecords">
					<xsl:value-of select="'1'"/>
				</xsl:attribute>
				<Translation>
					<ContextualInfo>
						<xsl:attribute name="EnterpriseCode">
							<xsl:value-of select="//@OrganizationCode"/>
						</xsl:attribute>
						<xsl:attribute name="InventoryOrganizationCode">
							<xsl:value-of select="//@OrganizationCode"/>
						</xsl:attribute>
						<xsl:attribute name="OrganizationCode">
							<xsl:value-of select="//@OrganizationCode"/>
						</xsl:attribute>
					</ContextualInfo>
					<ItemContextualInfo>
						<xsl:attribute name="InventoryUOM">
							<xsl:value-of select="//@UnitOfMeasure"/>
						</xsl:attribute>
						<xsl:attribute name="ItemID">
							<xsl:value-of select="//@ItemID"/>
						</xsl:attribute>
						<xsl:attribute name="KitCode">
							<xsl:value-of select="//Item/PrimaryInformation/@KitCode"/>
						</xsl:attribute>
					</ItemContextualInfo>
				</Translation>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="TotalNumberOfRecords">
						<xsl:value-of select="'0'"/>
					</xsl:attribute>
				</xsl:otherwise>
        </xsl:choose>
			</Translations>
		</BarCode>
	</xsl:template>
</xsl:stylesheet>