<?xml version="1.0" encoding="UTF-8"?>
<!-- Input XMLs

When Shipping method: NextDayAir
<basket>
    <ResponseLineItem>
        <lineItemID>20150413091105437942454</lineItemID>
        <shippingSurchargeAmount>0.00</shippingSurchargeAmount>
        <ecoFeeAmount>0.0</ecoFeeAmount>
        <eligibleShipModes>
            <ShippingMode>
                <serviceType>NextDayAir</serviceType>
                <ShippingCharge currency="USD">16.53</ShippingCharge>
            </ShippingMode>
        </eligibleShipModes>
    </ResponseLineItem>
    <ResponseLineItem>
        <lineItemID>20150413091105437942455</lineItemID>
        <shippingSurchargeAmount>0.00</shippingSurchargeAmount>
        <ecoFeeAmount>0.0</ecoFeeAmount>
        <eligibleShipModes>
            <ShippingMode>
                <serviceType>NextDayAir</serviceType>
                <ShippingCharge currency="USD">7.42</ShippingCharge>
            </ShippingMode>
        </eligibleShipModes>
    </ResponseLineItem>
</basket>

When Shipping method: Standard
<basket>
    <ResponseLineItem>
        <lineItemID>20150413091105437942454</lineItemID>
        <shippingSurchargeAmount>0.00</shippingSurchargeAmount>
        <ecoFeeAmount>0.0</ecoFeeAmount>
        <eligibleShipModes>
            <ShippingMode>
                <serviceType>Standard</serviceType>
                <ShippingCharge currency="USD">7.56</ShippingCharge>
            </ShippingMode>
        </eligibleShipModes>
    </ResponseLineItem>
    <ResponseLineItem>
        <lineItemID>20150413091105437942455</lineItemID>
        <shippingSurchargeAmount>0.00</shippingSurchargeAmount>
        <ecoFeeAmount>0.0</ecoFeeAmount>
        <eligibleShipModes>
            <ShippingMode>
                <serviceType>Standard</serviceType>
                <ShippingCharge currency="USD">3.39</ShippingCharge>
            </ShippingMode>
        </eligibleShipModes>
    </ResponseLineItem>
</basket>

When Shipping method: Saturday
<basket>
    <ResponseLineItem>
        <lineItemID>20150413091105437942454</lineItemID>
        <shippingSurchargeAmount>0.00</shippingSurchargeAmount>
        <ecoFeeAmount>0.0</ecoFeeAmount>
        <eligibleShipModes>
            <ShippingMode>
                <serviceType>Saturday</serviceType>
                <ShippingCharge currency="USD">31.71</ShippingCharge>
            </ShippingMode>
        </eligibleShipModes>
    </ResponseLineItem>
    <ResponseLineItem>
        <lineItemID>20150413091105437942455</lineItemID>
        <shippingSurchargeAmount>0.00</shippingSurchargeAmount>
        <ecoFeeAmount>0.0</ecoFeeAmount>
        <eligibleShipModes>
            <ShippingMode>
                <serviceType>Saturday</serviceType>
                <ShippingCharge currency="USD">14.24</ShippingCharge>
            </ShippingMode>
        </eligibleShipModes>
    </ResponseLineItem>
</basket>

When Shipping method: SecondDayAir
<basket>
    <ResponseLineItem>
        <lineItemID>20150413091105437942454</lineItemID>
        <shippingSurchargeAmount>0.00</shippingSurchargeAmount>
        <ecoFeeAmount>0.0</ecoFeeAmount>
        <eligibleShipModes>
            <ShippingMode>
                <serviceType>SecondDayAir</serviceType>
                <ShippingCharge currency="USD">12.39</ShippingCharge>
            </ShippingMode>
        </eligibleShipModes>
    </ResponseLineItem>
    <ResponseLineItem>
        <lineItemID>20150413091105437942455</lineItemID>
        <shippingSurchargeAmount>0.00</shippingSurchargeAmount>
        <ecoFeeAmount>0.0</ecoFeeAmount>
        <eligibleShipModes>
            <ShippingMode>
                <serviceType>SecondDayAir</serviceType>
                <ShippingCharge currency="USD">5.56</ShippingCharge>
            </ShippingMode>
        </eligibleShipModes>
    </ResponseLineItem>
</basket>
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<basket>
			<xsl:for-each select="/basket/ResponseLineItem">
			<ResponseLineItem>
				<xsl:copy-of select="./*[name()!='eligibleShipModes']" />
				 <eligibleShipModes>
					<ShippingMode>
						<serviceType>
							<xsl:value-of select="./eligibleShipModes/ShippingMode/serviceType"/>
						</serviceType>
						<ShippingCharge>
							<xsl:attribute name="currency">
								<xsl:value-of select="./eligibleShipModes/ShippingMode/ShippingCharge/@currency"/>
							</xsl:attribute>
							<xsl:attribute name="shippingAmount">
								<xsl:value-of select="./eligibleShipModes/ShippingMode/ShippingCharge"/>
							</xsl:attribute>
						</ShippingCharge>
					</ShippingMode>
				</eligibleShipModes>
			</ResponseLineItem>
			</xsl:for-each>
		</basket>
	</xsl:template>
</xsl:stylesheet>

<!-- Output XML 

When Shipping method: SecondDayAir
<basket>
<ResponseLineItem>
<lineItemID>20150413091105437942454</lineItemID>
<shippingSurchargeAmount>0.00</shippingSurchargeAmount>
<ecoFeeAmount>0.0</ecoFeeAmount>
<eligibleShipModes>
<ShippingMode>
<serviceType>SecondDayAir</serviceType>
<ShippingCharge currency="USD" shippingAmount="12.39"/>
</ShippingMode>
</eligibleShipModes>
</ResponseLineItem>
<ResponseLineItem>
<lineItemID>20150413091105437942455</lineItemID>
<shippingSurchargeAmount>0.00</shippingSurchargeAmount>
<ecoFeeAmount>0.0</ecoFeeAmount>
<eligibleShipModes>
<ShippingMode>
<serviceType>SecondDayAir</serviceType>
<ShippingCharge currency="USD" shippingAmount="5.56"/>
</ShippingMode>
</eligibleShipModes>
</ResponseLineItem>
</basket>
-->