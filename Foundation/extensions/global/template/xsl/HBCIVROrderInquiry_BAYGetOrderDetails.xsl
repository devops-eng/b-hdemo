<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
          <xsl:element name="OrderList" >
			<xsl:choose>
              <xsl:when test="Order/OrderList">
             <xsl:attribute name="TotalOrderList">
                <xsl:value-of select="Order/OrderList/@TotalOrderList"/>
            </xsl:attribute>
            <xsl:for-each select="Order/OrderList/Order">
        <xsl:element name="Order" >
          <xsl:attribute name="DocumentType">
                <xsl:value-of select="@DocumentType" />
            </xsl:attribute>
              <xsl:attribute name="OrderNo">
                <xsl:value-of select="@OrderNo" />
            </xsl:attribute>
              <xsl:attribute name="EnterpriseCode">
                <xsl:value-of select="@EnterpriseCode" />
            </xsl:attribute>
               <xsl:attribute name="Language">
                <xsl:value-of select="/Order/@Language" />
            </xsl:attribute>
        </xsl:element>
        </xsl:for-each>
        </xsl:when>
        <xsl:otherwise>
            <xsl:attribute name="TotalOrderList">1</xsl:attribute>
           <xsl:element name="Order" >
          <xsl:attribute name="DocumentType">
                <xsl:value-of select="Order/@DocumentType" />
            </xsl:attribute>
              <xsl:attribute name="OrderNo">
                <xsl:value-of select="Order/@OrderNo" />
            </xsl:attribute>
              <xsl:attribute name="EnterpriseCode">
                <xsl:value-of select="Order/@EnterpriseCode" />
            </xsl:attribute>
               <xsl:attribute name="Language">
                <xsl:value-of select="Order/@Language" />
            </xsl:attribute>
        </xsl:element>       
        </xsl:otherwise>
        </xsl:choose>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>
