<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
 <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
        <OrderList>
            <xsl:attribute name="TotalOrderList">
                <xsl:value-of select="OrderList/@TotalOrderList"/>
            </xsl:attribute>
            <xsl:for-each select="/OrderList/Order">
                <xsl:variable name="Language">
                    <xsl:value-of select="@Language"/>
                </xsl:variable>
                <xsl:variable name="Org">
                    <xsl:value-of select="@EnterpriseCode"/>
                </xsl:variable>
                <xsl:element name="Order">
                    <xsl:attribute name="OrderNo">
                            <xsl:value-of select ="Order/@OrderNo"/>
                    </xsl:attribute>
                    <xsl:attribute name="DocumentType">
                            <xsl:value-of select ="Order/@DocumentType"/>
                    </xsl:attribute>
                    <xsl:attribute name="EnterpriseCode">
                        <xsl:value-of select ="Order/@EnterpriseCode"/>
                    </xsl:attribute>
                    <xsl:attribute name="OrderDate">
                        <xsl:value-of select ="Order/@OrderDate"/>
                    </xsl:attribute>
                    <xsl:attribute name="Status">
                        <xsl:value-of select ="Order/@Status"/>
                    </xsl:attribute>
                     <xsl:variable name="PostCode">
                         <xsl:value-of select="substring(translate(Order/PersonInfoBillTo/@ZipCode,' ',''),1,5)"/>
                         </xsl:variable>

                                <xsl:variable name="IsNumeric">
                                    <xsl:choose>
                                        <xsl:when test="number($PostCode)">
                                            <xsl:value-of select="'true'"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                                <xsl:value-of select="'false'"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                 </xsl:variable>

                    <xsl:element name="PersonInfoBillTo">
                         <xsl:attribute name="ZipCode">
                            <xsl:choose>
                                <xsl:when test="$IsNumeric='true'">
                                    <xsl:value-of select="$PostCode"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="substring(translate(Order/PersonInfoBillTo/@ZipCode,' ',''),1,6)"/>
                                </xsl:otherwise>
                                </xsl:choose>
                         </xsl:attribute>
                        </xsl:element>
                    <OrderLines>
                        <xsl:for-each select="Order/OrderLines/OrderLine">
                            <OrderLine>
                                <xsl:copy-of select="@*"/>
                                <Item>
                                    <xsl:attribute name="ItemID">
                                        <xsl:value-of select="ItemDetails/@ItemID" />
                                        </xsl:attribute>
                                        <xsl:if test="$Org='LT'">
                                        <xsl:attribute name="ShortDescription" >
                                        <xsl:value-of select="ItemDetails/PrimaryInformation/@ShortDescription" />
                                        </xsl:attribute>
                                        </xsl:if>
                                        <xsl:if test="$Org='BAY'">
                                     <xsl:attribute name="ShortDescription">
                                        <xsl:for-each select="ItemDetails/ItemLocaleList/ItemLocale">
                                            <xsl:if test="@Language=$Language">
                                                <xsl:value-of select="PrimaryInformation/@ShortDescription"/>
                                            </xsl:if>
                                        </xsl:for-each>
                                       </xsl:attribute>
                                         </xsl:if>
                                </Item>
                            </OrderLine>
                        </xsl:for-each>
                    </OrderLines>
                    <xsl:choose>
                    <xsl:when test="Order/@HasChainedLines='Y'">
                        <xsl:if test="Order/Shipments/Shipment">
                            <xsl:element name="Containers">
                                <xsl:for-each select="Order/Shipments/Shipment">
                                    <xsl:variable name="CarrierService">
                                        <xsl:value-of select="@CarrierServiceCode"/>
                                    </xsl:variable>
										<xsl:variable name="SCAC">
											<xsl:value-of select="@SCAC"/>
										</xsl:variable>
                                    <xsl:for-each select="Containers/Container">
                                        <xsl:element name="Container">
                                            <xsl:attribute name="TrackingNo">
                                                <xsl:value-of select="@TrackingNo"/>
                                            </xsl:attribute>
                                
                                             <xsl:element name="Shipment">
                                                <xsl:attribute name="CarrierServiceCode">
                                                    <xsl:value-of select="$CarrierService"/>
                                                 </xsl:attribute>
													<xsl:attribute name="SCAC">
														<xsl:value-of select="$SCAC"/>
													</xsl:attribute>
                                            </xsl:element>
                                     <xsl:choose>
                                         <xsl:when test="ContainerDetails/ContainerDetail">
                                            <xsl:copy-of select="ContainerDetails"/>
                                         </xsl:when>
                                          <xsl:otherwise>
                                              <ContainerDetails>
                                                     <xsl:for-each select="../../ShipmentLines/ShipmentLine">
                                                            <ContainerDetail>
                                                                    <xsl:attribute name="OrderLineKey">
                                                                            <xsl:value-of select="@ChainedFromOrderLineKey"/>
                                                                    </xsl:attribute>
                                                            </ContainerDetail>
                                                    </xsl:for-each>
                                              </ContainerDetails>
                                          </xsl:otherwise>
                                     </xsl:choose>
                            </xsl:element>
                        </xsl:for-each>
                         </xsl:for-each>
                  </xsl:element>
                  </xsl:if>
                  </xsl:when>
                  <xsl:otherwise>
                         <xsl:if test="Order/Containers/Container">
                                <xsl:element name="Containers">
                                   <xsl:for-each select="Order/Containers/Container">
                                    <xsl:element name="Container">
                                         <xsl:attribute name="TrackingNo">
                                            <xsl:value-of select="@TrackingNo"/>
                                            </xsl:attribute>
                                        <xsl:element name="Shipment">
											<xsl:attribute name="SCAC">
												<xsl:value-of select="Shipment/@SCAC"/>
											</xsl:attribute>
                                         <xsl:attribute name="CarrierServiceCode">
                                                <xsl:value-of select="Shipment/@CarrierServiceCode"/>
                                         </xsl:attribute>
                                        </xsl:element>
                                         <xsl:copy-of select="ContainerDetails"/>
                                </xsl:element>
                            </xsl:for-each>
                        </xsl:element>
                  </xsl:if>
                  </xsl:otherwise>
                  </xsl:choose>
                  </xsl:element>
                </xsl:for-each>
                </OrderList>
    </xsl:template>
</xsl:stylesheet>
