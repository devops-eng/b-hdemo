<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/"> 
        <xsl:element name="OrderList" >
            <xsl:attribute name="TotalOrderList">
                <xsl:value-of select="Order/OrderList/@TotalOrderList"/>
            </xsl:attribute>
            <xsl:variable name="Language">
                    <xsl:value-of select="Order/@Language"/>
                </xsl:variable>
                <xsl:variable name="Org">
                    <xsl:value-of select="Order/@EnterpriseCode"/>
            </xsl:variable>
            <xsl:for-each select="Order/OrderList/Order">
                <xsl:element name="Order" >
                    <xsl:copy-of  select="@*"/>
					
					     <xsl:variable name="PostCode">
                         <xsl:value-of select="substring(translate(PersonInfoBillTo/@ZipCode,' ',''),1,5)"/>
                         </xsl:variable>

                                <xsl:variable name="IsNumber">
                                    <xsl:choose>
                                        <xsl:when test="number($PostCode)">
                                            <xsl:value-of select="'true'"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                                <xsl:value-of select="'false'"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                 </xsl:variable>
					
                    <xsl:element name="PersonInfoBillTo">
                         <xsl:attribute name="ZipCode">
                            <xsl:choose>
                                <xsl:when test="$IsNumber='true'">
                                    <xsl:value-of select="$PostCode"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="substring(translate(PersonInfoBillTo/@ZipCode,' ',''),1,6)"/>
                                </xsl:otherwise>
                                </xsl:choose>
                         </xsl:attribute>
                        </xsl:element>
						
                    <OrderLines>
                         <xsl:for-each select="OrderLines/OrderLine">
                             <OrderLine>
                                <xsl:attribute name="OrderLineKey">
                                    <xsl:value-of select="@OrderLineKey" />
                                 </xsl:attribute>
                                 <Item>
                                     <xsl:attribute name="ItemID">
                                         <xsl:value-of select="ItemDetails/@ItemID" />
                                     </xsl:attribute>
                                     <xsl:attribute name="ShortDescription" >
                                     <xsl:choose>
                                     <xsl:when test="$Org='LT'">
                                        <xsl:value-of select="ItemDetails/PrimaryInformation/@ShortDescription" />                                       
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:for-each select="ItemDetails/ItemLocaleList/ItemLocale">
                                                <xsl:if test="@Language=$Language">
                                                    <xsl:value-of select="PrimaryInformation/@ShortDescription"/>
                                                </xsl:if>
                                           </xsl:for-each>
                                       </xsl:otherwise>
                                        </xsl:choose>
                                     </xsl:attribute>
                                </Item>
                            </OrderLine>
                         </xsl:for-each>
                    </OrderLines>
                 </xsl:element>
           </xsl:for-each>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>
