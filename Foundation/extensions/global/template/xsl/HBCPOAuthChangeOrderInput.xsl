<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">
	<xsl:template match="/">
		<Order>
			<xsl:copy-of select="Order/@*[not(name()='BillToID')]" />
			<xsl:attribute name="BillToID">
				<xsl:value-of select="Order/@CustomerContactID" />
			</xsl:attribute>
			<OrderLines>
				<xsl:for-each select="Order/OrderLines/OrderLine">
					<OrderLine>
						<xsl:copy-of select="@*" />
						<Item> 
							<xsl:copy-of select="Item/@*"/>
						</Item>
						<LinePriceInfo> 
							<xsl:copy-of select="LinePriceInfo/@*"/> 
						</LinePriceInfo>
						<Extn>
							<xsl:copy-of select="Extn/@*" />
						</Extn>
						<OrderHoldTypes>
							<xsl:copy-of select="OrderHoldTypes/*" />
						</OrderHoldTypes>

					</OrderLine>
				</xsl:for-each>
			</OrderLines>
			<PaymentMethods>
				<xsl:for-each
					select="Order/PaymentMethods/PaymentMethod">
					<PaymentMethod>
						<xsl:copy-of select="@*|*" />
					</PaymentMethod>
				</xsl:for-each>
			</PaymentMethods>
		</Order>
	</xsl:template>
</xsl:stylesheet>