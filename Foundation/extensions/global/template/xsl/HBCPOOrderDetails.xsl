<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <Order>
		<xsl:attribute name="EntryType">
			<xsl:value-of select="Order/@EntryType"/>
		</xsl:attribute>
		<xsl:attribute name="BillToID">
			<xsl:value-of select="Order/@BillToID"/>
		</xsl:attribute>
		<xsl:attribute name="DocumentType">
			<xsl:value-of select="Order/@DocumentType"/>
		</xsl:attribute>
		<xsl:attribute name="EnterpriseCode">
			<xsl:value-of select="Order/@EnterpriseCode"/>
		</xsl:attribute>
		<xsl:attribute name="OrderDate">
			<xsl:value-of select="Order/@OrderDate"/>
		</xsl:attribute>
		<xsl:attribute name="OrderNo">
			<xsl:value-of select="Order/@OrderNo"/>
		</xsl:attribute>
		<xsl:attribute name="Status">
			<xsl:value-of select="Order/@Status"/>
		</xsl:attribute>
		<PersonInfoBillTo>
			<xsl:copy-of select="Order/PersonInfoBillTo/@*" />
		</PersonInfoBillTo>
		<OrderLines>
			<xsl:for-each select="Order/OrderLines/OrderLine">
				<OrderLine>
					<xsl:copy-of select="@*[not(.='')]|node()"/>
					<xsl:apply-templates select="*"/>
				</OrderLine>
			</xsl:for-each>
		</OrderLines>		
		<PaymentMethods>
			<xsl:for-each select="Order/PaymentMethods/PaymentMethod">
				<PaymentMethod>
					<xsl:copy-of select="@*[not(.='')]|node()"/>
					<xsl:apply-templates select="*"/>
				</PaymentMethod>
			</xsl:for-each>
		</PaymentMethods>
		<HeaderCharges>
			<xsl:for-each select="Order/HeaderCharges/HeaderCharge">
				<HeaderCharge>
					<xsl:copy-of select="@*[not(.='')]|node()"/>
					<xsl:apply-templates select="*"/>
				</HeaderCharge>
			</xsl:for-each>
		</HeaderCharges>
 		<HeaderTaxes>
			<xsl:for-each select="Order/HeaderTaxes/HeaderTax">
				<HeaderTax>
					<xsl:copy-of select="@*[not(.='')]|node()"/>
					<xsl:apply-templates select="*"/>
				</HeaderTax>
			</xsl:for-each>
		</HeaderTaxes>
		<PriceInfo>
			<xsl:copy-of select="Order/PriceInfo/@*" />
		</PriceInfo>
		<OverallTotals>
			<xsl:copy-of select="Order/OverallTotals/@*" />
		</OverallTotals>

<xsl:if test="Order/@HasChainedLines='N'">
	<Containers>
		<xsl:for-each select="Order/Containers/Container">
			<Container>
				<xsl:copy-of select="@*[not(.='')]|node()"/>
				<xsl:apply-templates select="*"/>
			</Container>
		</xsl:for-each>
	</Containers>
</xsl:if>
<xsl:if test="Order/@HasChainedLines=''">
	<Containers>
		<xsl:for-each select="Order/Containers/Container">
			<Container>
				<xsl:copy-of select="@*[not(.='')]|node()"/>
				<xsl:apply-templates select="*"/>
			</Container>
		</xsl:for-each>
	</Containers>
</xsl:if>
<!-- This will be used for PO (VDS) Orders -->
<xsl:if test="Order/@HasChainedLines='Y'">
	<Containers>
		<xsl:for-each select="Order/Shipments/Shipment/Containers/Container">
			<Container>
				<xsl:attribute name="TrackingNo">
					<xsl:value-of select="@TrackingNo"/>
				</xsl:attribute>				
				<Shipment>
					<xsl:attribute name="ActualDeliveryDate">
						<xsl:value-of select="../../@ActualDeliveryDate"/>
					</xsl:attribute>				
					<xsl:attribute name="ActualShipmentDate">
						<xsl:value-of select="../../@ActualShipmentDate"/>
					</xsl:attribute>	
					<xsl:attribute name="CarrierServiceCode">
						<xsl:value-of select="../../@CarrierServiceCode"/>
					</xsl:attribute>	
					<xsl:attribute name="DeliveryMethod">
						<xsl:value-of select="../../@DeliveryMethod"/>
					</xsl:attribute>	
					<xsl:attribute name="ExpectedDeliveryDate">
						<xsl:value-of select="../../@ExpectedDeliveryDate"/>
					</xsl:attribute>	
					<xsl:attribute name="ExpectedShipmentDate">
						<xsl:value-of select="../../@ExpectedShipmentDate"/>
					</xsl:attribute>	
					<xsl:attribute name="SCAC">
						<xsl:value-of select="../../@SCAC"/>
					</xsl:attribute>					
				</Shipment> 
				<ContainerDetails>
					<ContainerDetail>
						<xsl:for-each select="../../ShipmentLines/ShipmentLine">
							<ShipmentLine>
								<xsl:attribute name="OrderLineKey">
									<xsl:value-of select="@ChainedFromOrderLineKey"/>
								</xsl:attribute>
							</ShipmentLine>
						</xsl:for-each> 
					</ContainerDetail>
				</ContainerDetails> 
			</Container>
		</xsl:for-each>
		<xsl:for-each select="Order/Containers/Container">
			<Container>
				<xsl:copy-of select="@*[not(.='')]|node()"/>
				<xsl:apply-templates select="*"/>
			</Container>
		</xsl:for-each>		
	</Containers>
</xsl:if> 

    </Order>
  </xsl:template>
</xsl:stylesheet>