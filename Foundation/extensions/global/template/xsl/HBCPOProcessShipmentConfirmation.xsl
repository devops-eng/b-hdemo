<?xml version="1.0" encoding="utf-8"?>
<!-- 
Name : HBCPOProcessShipmentConfirmation
Use: This XSL is used to create containerdetails to makes similar to WMOS/SendSuite Shipments
so that TrackingNo will be visible in COM screen similar to Non drop ship orders..
This XSL is added as part of Defect # 409
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <Shipment>
      <xsl:copy-of select="Shipment/@*"/>
      <!--<ToAddress>
        <xsl:copy-of select="Order/ToAddress/@*" />
      </ToAddress>-->
      <Containers>
        <xsl:attribute name="Replace">
          <xsl:value-of select="'N'"/>
        </xsl:attribute>
        <Container>
          <xsl:attribute name="TrackingNo">
            <xsl:value-of select="Shipment/Containers/Container/@TrackingNo"/>
          </xsl:attribute>

          <ContainerDetails>
            <xsl:attribute name="Replace">
              <xsl:value-of select="'N'"/>
            </xsl:attribute>
            <xsl:for-each select="Shipment/ShipmentLines/ShipmentLine">
              <ContainerDetail>
               
                  <ShipmentLine>
                    
                <xsl:copy-of select="@*[not(.='')]|node()"/>
                  <xsl:apply-templates select="*"/>

                  </ShipmentLine>
              
              </ContainerDetail>
            </xsl:for-each>
          </ContainerDetails>
          
        </Container>
        
      </Containers>
      <ShipmentLines>
        <xsl:for-each select="Shipment/ShipmentLines/ShipmentLine">
          <ShipmentLine>
            <xsl:copy-of select="@*[not(.='')]|node()"/>
            <xsl:apply-templates select="*"/>
          </ShipmentLine>
        </xsl:for-each>
      </ShipmentLines>
 
    </Shipment>
  </xsl:template>
</xsl:stylesheet>