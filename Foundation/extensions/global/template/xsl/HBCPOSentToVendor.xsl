<!--DSG Merge Changes: Updated-->
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<OrderRelease>
			<xsl:attribute name="OrderType">
				<xsl:value-of select="'DROP_SHIP'"/>
			</xsl:attribute>
			<xsl:attribute name="SCAC">
				<xsl:value-of select="OrderRelease/@SCAC"/>
			</xsl:attribute>
			<xsl:attribute name="ShipmentMethod">
				<xsl:value-of select="OrderRelease/@ShipmentMethod"/>
			</xsl:attribute>
			<xsl:attribute name="SalesOrderNo">
				<xsl:value-of select="OrderRelease/OrderLine/ChainedFromOrderLine/Order/@OrderNo"/>
			</xsl:attribute>
			<xsl:attribute name="EnterpriseCode">
				<xsl:value-of select="OrderRelease/@EnterpriseCode"/>
			
			</xsl:attribute>
			<xsl:attribute name="ShipNode">
				<xsl:value-of select="OrderRelease/@ShipNode"/>
			</xsl:attribute>
			<!--Type of PO-->
			<xsl:attribute name="ShipNodeClass">
				<xsl:value-of select="OrderRelease/@ShipNodeClass"/>
			</xsl:attribute>
			<xsl:attribute name="ReqShipDate">
				<xsl:value-of select="OrderRelease/@ReqShipDate"/>
			</xsl:attribute>
			<xsl:attribute name="ReqDeliveryDate">
				<xsl:value-of select="OrderRelease/@ReqDeliveryDate"/>
			</xsl:attribute>
			<xsl:attribute name="ReleaseNo">
				<xsl:value-of select="OrderRelease/@ReleaseNo"/>
			</xsl:attribute>
			<xsl:attribute name="OrderHeaderKey">
				<xsl:value-of select="OrderRelease/@OrderHeaderKey"/>
			</xsl:attribute>
			<xsl:attribute name="Currency">
				<xsl:value-of select="OrderRelease/@Currency"/>
			</xsl:attribute>
			<xsl:attribute name="Createts">
				<xsl:value-of select="OrderRelease/@Createts"/>
			</xsl:attribute>
			<xsl:attribute name="CarrierServiceCode">
				<xsl:value-of select="OrderRelease/@CarrierServiceCode"/>
			</xsl:attribute>
			<xsl:attribute name="DocumentType">
				<xsl:value-of select="OrderRelease/@DocumentType"/>
			</xsl:attribute>
			<ShipNode>
				<xsl:attribute name="ShipnodeType">
					<xsl:value-of select="OrderRelease/ShipNode/@ShipnodeType"/>
				</xsl:attribute>
			</ShipNode>
			<Order>
				<xsl:attribute name="SellerOrganizationCode">
					<xsl:value-of select="OrderRelease/Order/@SellerOrganizationCode"/>
				</xsl:attribute>
				<xsl:attribute name="OrderNo">
					<xsl:value-of select="OrderRelease/Order/@OrderNo"/>
				</xsl:attribute>
				<xsl:attribute name="EnterpriseCode">
					<xsl:value-of select="OrderRelease/Order/@EnterpriseCode"/>
				</xsl:attribute>
				<xsl:attribute name="DocumentType">
					<xsl:value-of select="OrderRelease/Order/@DocumentType"/>
				</xsl:attribute>
        		<xsl:attribute name="BillToID">
          			<xsl:value-of select="OrderRelease/Order/@BillToID"/>
				</xsl:attribute>
				<!-- Start : 1/20 : Reporting Related Change (CR) -->
        		<xsl:attribute name="EntryType">
          			<xsl:value-of select="OrderRelease/Order/@EntryType"/>
				</xsl:attribute>
				<!-- End : 1/20 : Reporting Related Change (CR) -->
				<Extn>
					<xsl:attribute name="ExtnLoyaltyNo">
						<xsl:value-of select="OrderRelease/Order/Extn/@ExtnLoyaltyNo"/>
					</xsl:attribute>
					<xsl:attribute name="ExtnCommLang">
						<xsl:value-of select="OrderRelease/Order/Extn/@ExtnCommLang"/>
					</xsl:attribute>
					<xsl:attribute name="ExtnIsFiftyOne">
						<xsl:value-of select="OrderRelease/OrderLine/ChainedFromOrderLine/Order/Extn/@ExtnIsFiftyOne"/>
					</xsl:attribute>
				</Extn>
				<Promotions>
					<Promotion>
					<xsl:attribute name="PromotionId">
						<xsl:value-of select="OrderRelease/OrderLine/ChainedFromOrderLine/Order/Promotions/Promotion/@PromotionId"/>
					</xsl:attribute>
					</Promotion>
				</Promotions>
			</Order>
			<PackListPriceInfo>
				<xsl:attribute name="TotalTax">
					<xsl:value-of select="OrderRelease/PackListPriceInfo/@TotalTax"/>
				</xsl:attribute>
				<xsl:attribute name="TotalDiscountAmount">
					<xsl:value-of select="OrderRelease/PackListPriceInfo/@TotalDiscountAmount"/>
				</xsl:attribute>
				<xsl:attribute name="TotalCharges">
					<xsl:value-of select="OrderRelease/PackListPriceInfo/@TotalCharges"/>
				</xsl:attribute>
				<xsl:attribute name="TotalAmount">
					<xsl:value-of select="OrderRelease/PackListPriceInfo/@TotalAmount"/>
				</xsl:attribute>
				<xsl:attribute name="LineSubTotal">
					<xsl:value-of select="OrderRelease/PackListPriceInfo/@LineSubTotal"/>
				</xsl:attribute>
				<xsl:attribute name="HeaderTax">
					<xsl:value-of select="OrderRelease/PackListPriceInfo/@HeaderTax"/>
				</xsl:attribute>
				<xsl:attribute name="HeaderDiscount">
					<xsl:value-of select="OrderRelease/PackListPriceInfo/@HeaderDiscount"/>
				</xsl:attribute>
				<xsl:attribute name="HeaderCharges">
					<xsl:value-of select="OrderRelease/PackListPriceInfo/@HeaderCharges"/>
				</xsl:attribute>
				<xsl:attribute name="Currency">
					<xsl:value-of select="OrderRelease/PackListPriceInfo/@Currency"/>
				</xsl:attribute>
			</PackListPriceInfo>
			<PersonInfoShipTo>
				<xsl:attribute name="FirstName">
					<xsl:value-of select="OrderRelease/PersonInfoShipTo/@FirstName"/>
				</xsl:attribute>
				<xsl:attribute name="LastName">
					<xsl:value-of select="OrderRelease/PersonInfoShipTo/@LastName"/>
				</xsl:attribute>
				<xsl:attribute name="MiddleName">
					<xsl:value-of select="OrderRelease/PersonInfoShipTo/@MiddleName"/>
				</xsl:attribute>
				<xsl:attribute name="State">
					<xsl:value-of select="OrderRelease/PersonInfoShipTo/@State"/>
				</xsl:attribute>
				<xsl:attribute name="Title">
					<xsl:value-of select="OrderRelease/PersonInfoShipTo/@Title"/>
				</xsl:attribute>
				<xsl:attribute name="ZipCode">
					<xsl:value-of select="OrderRelease/PersonInfoShipTo/@ZipCode"/>
				</xsl:attribute>
				<xsl:attribute name="PersonInfoKey">
					<xsl:value-of select="OrderRelease/PersonInfoShipTo/@PersonInfoKey"/>
				</xsl:attribute>
				<xsl:attribute name="MobilePhone">
					<xsl:value-of select="OrderRelease/PersonInfoShipTo/@MobilePhone"/>
				</xsl:attribute>
				<xsl:attribute name="EveningPhone">
					<xsl:value-of select="OrderRelease/PersonInfoShipTo/@EveningPhone"/>
				</xsl:attribute>
				<xsl:attribute name="EMailID">
					<xsl:value-of select="OrderRelease/PersonInfoShipTo/@EMailID"/>
				</xsl:attribute>
				<xsl:attribute name="DayPhone">
					<xsl:value-of select="OrderRelease/PersonInfoShipTo/@DayPhone"/>
				</xsl:attribute>
				<xsl:attribute name="Country">
					<xsl:value-of select="OrderRelease/PersonInfoShipTo/@Country"/>
				</xsl:attribute>
				<xsl:attribute name="City">
					<xsl:value-of select="OrderRelease/PersonInfoShipTo/@City"/>
				</xsl:attribute>
				<xsl:attribute name="AddressLine3">
					<xsl:value-of select="OrderRelease/PersonInfoShipTo/@AddressLine3"/>
				</xsl:attribute>
				<xsl:attribute name="AddressLine2">
					<xsl:value-of select="OrderRelease/PersonInfoShipTo/@AddressLine2"/>
				</xsl:attribute>
				<xsl:attribute name="AddressLine1">
					<xsl:value-of select="OrderRelease/PersonInfoShipTo/@AddressLine1"/>
				</xsl:attribute>
			</PersonInfoShipTo>
			<PersonInfoBillTo>
				<xsl:attribute name="EveningPhone">
					<xsl:value-of select="OrderRelease/OrderLine/ChainedFromOrderLine/Order/PersonInfoBillTo/@EveningPhone"/>
				</xsl:attribute>
				<xsl:attribute name="State">
					<xsl:value-of select="OrderRelease/OrderLine/ChainedFromOrderLine/Order/PersonInfoBillTo/@State"/>
				</xsl:attribute>
				<xsl:attribute name="Title">
					<xsl:value-of select="OrderRelease/OrderLine/ChainedFromOrderLine/Order/PersonInfoBillTo/@Title"/>
				</xsl:attribute>
				<xsl:attribute name="ZipCode">
					<xsl:value-of select="OrderRelease/OrderLine/ChainedFromOrderLine/Order/PersonInfoBillTo/@ZipCode"/>
				</xsl:attribute>
				<xsl:attribute name="PersonInfoKey">
					<xsl:value-of select="OrderRelease/OrderLine/ChainedFromOrderLine/Order/PersonInfoBillTo/@PersonInfoKey"/>
				</xsl:attribute>
				<xsl:attribute name="MobilePhone">
					<xsl:value-of select="OrderRelease/OrderLine/ChainedFromOrderLine/Order/PersonInfoBillTo/@MobilePhone"/>
				</xsl:attribute>
				<xsl:attribute name="MiddleName">
					<xsl:value-of select="OrderRelease/OrderLine/ChainedFromOrderLine/Order/PersonInfoBillTo/@MiddleName"/>
				</xsl:attribute>
				<xsl:attribute name="LastName">
					<xsl:value-of select="OrderRelease/OrderLine/ChainedFromOrderLine/Order/PersonInfoBillTo/@LastName"/>
				</xsl:attribute>
				<xsl:attribute name="FirstName">
					<xsl:value-of select="OrderRelease/OrderLine/ChainedFromOrderLine/Order/PersonInfoBillTo/@FirstName"/>
				</xsl:attribute>
				<xsl:attribute name="EMailID">
					<xsl:value-of select="OrderRelease/OrderLine/ChainedFromOrderLine/Order/PersonInfoBillTo/@EMailID"/>
				</xsl:attribute>
				<xsl:attribute name="DayPhone">
					<xsl:value-of select="OrderRelease/OrderLine/ChainedFromOrderLine/Order/PersonInfoBillTo/@DayPhone"/>
				</xsl:attribute>
				<xsl:attribute name="Country">
					<xsl:value-of select="OrderRelease/OrderLine/ChainedFromOrderLine/Order/PersonInfoBillTo/@Country"/>
				</xsl:attribute>
				<xsl:attribute name="City">
					<xsl:value-of select="OrderRelease/OrderLine/ChainedFromOrderLine/Order/PersonInfoBillTo/@City"/>
				</xsl:attribute>
				<xsl:attribute name="AddressLine3">
					<xsl:value-of select="OrderRelease/OrderLine/ChainedFromOrderLine/Order/PersonInfoBillTo/@AddressLine3"/>
				</xsl:attribute>
				<xsl:attribute name="AddressLine2">
					<xsl:value-of select="OrderRelease/OrderLine/ChainedFromOrderLine/Order/PersonInfoBillTo/@AddressLine2"/>
				</xsl:attribute>
				<xsl:attribute name="AddressLine1">
					<xsl:value-of select="OrderRelease/OrderLine/ChainedFromOrderLine/Order/PersonInfoBillTo/@AddressLine1"/>
				</xsl:attribute>
			</PersonInfoBillTo>
			<xsl:variable name="EntCode" select="//OrderRelease/@EnterpriseCode" >
				</xsl:variable>
           <xsl:for-each select="OrderRelease/OrderLine">
             <OrderLine>
               
<!--           <xsl:copy-of select="@*[not(.='')]|node()"/>
               <xsl:apply-templates select="*"/>
 -->
               <xsl:attribute name="CarrierServiceCode">
                 <xsl:value-of select="@CarrierServiceCode"/>
               </xsl:attribute>
               <xsl:attribute name="SCAC">
                 <xsl:value-of select="@SCAC"/>
               </xsl:attribute>
               <xsl:attribute name="DepartmentCode">
                 <xsl:value-of select="@DepartmentCode"/>
               </xsl:attribute>
               <xsl:attribute name="FulfillmentType">
                 <xsl:value-of select="@FulfillmentType"/>
               </xsl:attribute>
			   
				
			   <xsl:choose>
			   <xsl:when test="($EntCode='BAY') or ($EntCode='LT') or ($EntCode='HO')">
					<xsl:attribute name="GiftFlag">
					   <xsl:value-of select="@GiftFlag"/>
					</xsl:attribute>
			   </xsl:when>
			   <xsl:otherwise>
					<xsl:attribute name="GiftFlag">
					 <xsl:value-of select="ChainedFromOrderLine/@GiftFlag"/>
					</xsl:attribute>
				</xsl:otherwise>
				</xsl:choose>
               
               <xsl:attribute name="LineType">
                 <xsl:value-of select="@LineType"/>
               </xsl:attribute>
               <xsl:attribute name="OrderedQty">
                 <xsl:value-of select="@OrderedQty"/>
               </xsl:attribute>
               <xsl:attribute name="PackListType">
                 <xsl:value-of select="@PackListType"/>
               </xsl:attribute>
               <xsl:attribute name="PrimeLineNo">
                 <xsl:value-of select="@PrimeLineNo"/>
               </xsl:attribute>
               <xsl:attribute name="ReqDeliveryDate">
                 <xsl:value-of select="@ReqDeliveryDate"/>
               </xsl:attribute>
               <xsl:attribute name="ReqShipDate">
                 <xsl:value-of select="@ReqShipDate"/>
               </xsl:attribute>
               <xsl:attribute name="ShipNode">
                 <xsl:value-of select="@ShipNode"/>
               </xsl:attribute>
               <xsl:attribute name="StatusQuantity">
                 <xsl:value-of select="@StatusQuantity"/>
               </xsl:attribute>
               <xsl:attribute name="SubLineNo">
                 <xsl:value-of select="@SubLineNo"/>
               </xsl:attribute>
			    <Item>
                 <xsl:attribute name="ItemDesc">
                   <xsl:value-of select="Item/@ItemDesc"/>
                 </xsl:attribute>
                  <xsl:attribute name="ItemShortDesc">
                   <xsl:value-of select="Item/@ItemShortDesc"/>
                 </xsl:attribute>
                 <xsl:attribute name="ItemID">
                   <xsl:value-of select="Item/@ItemID"/>
                 </xsl:attribute>
                 <xsl:attribute name="ItemWeight">
                   <xsl:value-of select="Item/@ItemWeight"/>
                 </xsl:attribute>
                 <xsl:attribute name="ItemWeightUOM">
                   <xsl:value-of select="Item/@ItemWeightUOM"/>
                 </xsl:attribute>
                 <xsl:attribute name="ProductClass">
                   <xsl:value-of select="Item/@ProductClass"/>
                 </xsl:attribute>
                 <xsl:attribute name="UnitOfMeasure">
                   <xsl:value-of select="Item/@UnitOfMeasure"/>
                 </xsl:attribute>
                 <!-- Supplier Item Same as Item ID for DSV -->
                 <xsl:attribute name="SupplierItem">
                   <xsl:value-of select="Item/@ItemID"/>
                 </xsl:attribute>
                 <xsl:attribute name="UnitCost">
                   <xsl:value-of select="Item/@UnitCost"/>
                 </xsl:attribute>
                 <xsl:attribute name="UPCCode">
                   <xsl:value-of select="Item/@UPCCode"/>
                 </xsl:attribute>
				 <!-- Start : 1/20 : Reporting Related Change (CR) -->
				 <xsl:attribute name="ManufacturerItem">
                   <xsl:value-of select="Item/@ManufacturerItem"/>
                 </xsl:attribute>
				 <!-- End : 1/20 : Reporting Related Change (CR) -->
				 <xsl:attribute name="LeadTime">
                   <xsl:value-of select="ChainedFromOrderLine/ItemDetails/InventoryParameters/@LeadTime"/>
                 </xsl:attribute>
				 <Extn>
					<xsl:attribute name="ExtnVendorStyle">
						<xsl:value-of select="ChainedFromOrderLine/ItemDetails/Extn/@ExtnVendorStyle"/>
					</xsl:attribute>
				</Extn>
               </Item>
			   <ItemDetails>
					<ItemAliasList>
					<xsl:for-each select="ItemDetails/ItemAliasList/ItemAlias">
						<ItemAlias>
						<xsl:attribute name="AliasName">
						<xsl:value-of select="@AliasName"/>
						</xsl:attribute>
						<xsl:attribute name="AliasValue">
						<xsl:value-of select="@AliasValue"/>
						</xsl:attribute>
						</ItemAlias>
						</xsl:for-each>
					</ItemAliasList>
				</ItemDetails>
               <LinePackListPriceInfo>
                 <xsl:attribute name="ExtendedPrice">
				    <xsl:text></xsl:text>
                   <!-- <xsl:value-of select="LinePackListPriceInfo/@ExtendedPrice"/> -->
                 </xsl:attribute>
                 <xsl:attribute name="UnitPrice">
                   <xsl:value-of select="LinePackListPriceInfo/@UnitPrice"/>
                 </xsl:attribute>
                 <xsl:attribute name="Charges">
                   <xsl:value-of select="/LinePackListPriceInfo/@Charges"/>
                 </xsl:attribute>
                 <xsl:attribute name="LineTotal">
                   <xsl:value-of select="LinePackListPriceInfo/@LineTotal"/>
                 </xsl:attribute>
                 <xsl:attribute name="Tax">
                   <xsl:value-of select="LinePackListPriceInfo/@Tax"/>
                 </xsl:attribute>
               </LinePackListPriceInfo>
               <Instructions>
                 <xsl:for-each select="Instructions/Instruction">
                 <Instruction>
                   <xsl:attribute name="InstructionText">
                     <xsl:value-of select="@InstructionText"/>
                   </xsl:attribute>
                   <xsl:attribute name="InstructionType">
                     <xsl:value-of select="@InstructionType"/>
                   </xsl:attribute>
                 </Instruction>
                 </xsl:for-each>
               </Instructions>
               <LinePriceInfo>
                 <xsl:attribute name="UnitPrice">
                   <xsl:value-of select="LinePriceInfo/@UnitPrice"/>
                 </xsl:attribute>
                 <xsl:attribute name="OrderedPricingQty">
                   <xsl:value-of select="LinePriceInfo/@OrderedPricingQty"/>
                 </xsl:attribute>
                 <xsl:attribute name="LineTotal">
                   <xsl:value-of select="LinePriceInfo/@LineTotal"/>
                 </xsl:attribute>
               </LinePriceInfo>
               <ChainedFromOrderLine>
                 <xsl:attribute name="FulfillmentType">
                   <xsl:value-of select="ChainedFromOrderLine/@FulfillmentType"/>
                 </xsl:attribute>
                 <xsl:attribute name="ReqDeliveryDate">
                   <xsl:value-of select="ChainedFromOrderLine/@ReqDeliveryDate"/>
                 </xsl:attribute>
                 <xsl:attribute name="ReqShipDate">
                   <xsl:value-of select="ChainedFromOrderLine/@ReqShipDate"/>
                 </xsl:attribute>
                 <xsl:attribute name="PrimeLineNo">
                   <xsl:value-of select="ChainedFromOrderLine/@PrimeLineNo"/>
                 </xsl:attribute>
                 <xsl:attribute name="SubLineNo">
                   <xsl:value-of select="ChainedFromOrderLine/@SubLineNo"/>
                 </xsl:attribute>
                 <xsl:attribute name="OrderedQty">      
				 <xsl:value-of select="ChainedFromOrderLine/@OrderedQty"/>
                 </xsl:attribute>
                    <xsl:attribute name="Status">
                   <xsl:value-of select="ChainedFromOrderLine/@Status"/>
                 </xsl:attribute>
                  <xsl:attribute name="Modifyts">
                   <xsl:value-of select="ChainedFromOrderLine/@Modifyts"/>
                 </xsl:attribute>				 
                 
				   
                 <Order>
                   <xsl:attribute name="CustomerEMailID">
                     <xsl:value-of select="ChainedFromOrderLine/Order/@CustomerEMailID"/>
                   </xsl:attribute>
                   <xsl:attribute name="OrderDate">
                     <xsl:value-of select="ChainedFromOrderLine/Order/@OrderDate"/>
                   </xsl:attribute>
                   <xsl:attribute name="OrderNo">
                     <xsl:value-of select="ChainedFromOrderLine/Order/@OrderNo"/>
                   </xsl:attribute>
                   <xsl:attribute name="DocumentType">
                     <xsl:value-of select="ChainedFromOrderLine/Order/@DocumentType"/>
                   </xsl:attribute>
                   <xsl:attribute name="BillToID">
                     <xsl:value-of select="ChainedFromOrderLine/Order/@BillToID"/>
                   </xsl:attribute>
				   
				   
				   
				   <xsl:attribute name="OrderPurpose">  
                     <xsl:value-of select="ChainedFromOrderLine/Order/@OrderPurpose"/>
                   </xsl:attribute>
                    <xsl:attribute name="EntryType">  
                     <xsl:value-of select="ChainedFromOrderLine/Order/@EntryType"/>
                   </xsl:attribute>
                   <xsl:attribute name="EnterpriseCode">  
                     <xsl:value-of select="ChainedFromOrderLine/Order/@EnterpriseCode"/>
                   </xsl:attribute>
                   <xsl:attribute name="Status">  
                     <xsl:value-of select="ChainedFromOrderLine/Order/@Status"/>
					 
                   </xsl:attribute>
				   
				   <Extn>
                 <xsl:attribute name="ExtnReference1">
                   <xsl:value-of select="ChainedFromOrderLine/Order/Extn/@ExtnReference1"/>
                 </xsl:attribute>
                 </Extn>

                   <HeaderTaxes>
                     <xsl:for-each select="ChainedFromOrderLine/Order/HeaderTaxes/HeaderTax">
                     <HeaderTax>
                       <xsl:attribute name="ChargeName">
                         <xsl:value-of select="@ChargeName"/>
                       </xsl:attribute>
                       <xsl:attribute name="ChargeCategory">
                         <xsl:value-of select="@ChargeCategory"/>
                       </xsl:attribute>
                       <xsl:attribute name="TaxPercentage">
                         <xsl:value-of select="@TaxPercentage"/>
                       </xsl:attribute>
                       <xsl:attribute name="TaxName">
                         <xsl:value-of select="@TaxName"/>
                       </xsl:attribute>
                       <xsl:attribute name="Tax">
                         <xsl:value-of select="@Tax"/>
                       </xsl:attribute>

                     </HeaderTax>
                     </xsl:for-each>
                   </HeaderTaxes>
                   <HeaderCharges>
                     <xsl:for-each select="ChainedFromOrderLine/Order/HeaderCharges/HeaderCharge">
                     <HeaderCharge>
                       <xsl:attribute name="ChargeName">
                         <xsl:value-of select="@ChargeName"/>
                       </xsl:attribute>
                       <xsl:attribute name="ChargeAmount">
                         <xsl:value-of select="@ChargeAmount"/>
                       </xsl:attribute>
                       <xsl:attribute name="ChargeCategory">
                         <xsl:value-of select="@ChargeCategory"/>
                       </xsl:attribute>
                       <xsl:attribute name="IsShippingCharge">
                         <xsl:value-of select="@IsShippingCharge"/>
                       </xsl:attribute>
                       <xsl:attribute name="IsDiscount">
                         <xsl:value-of select="@IsDiscount"/>
                       </xsl:attribute>
                     </HeaderCharge>
                     </xsl:for-each>
                   </HeaderCharges>
				   <PaymentMethods>
				   <xsl:for-each select="ChainedFromOrderLine/Order/PaymentMethods/PaymentMethod">
				   <PaymentMethod>
						<xsl:attribute name="PaymentType">
							<xsl:value-of select="@PaymentType"/>
						</xsl:attribute>
						<xsl:attribute name="CreditCardType">
							<xsl:value-of select="@CreditCardType"/>
						</xsl:attribute>
				   </PaymentMethod>
				   </xsl:for-each>
				   </PaymentMethods>
                 </Order>
				 
				 <ItemDetails>  
                    <ItemAliasList>
                      <xsl:for-each select="ChainedFromOrderLine/ItemDetails/ItemAliasList/ItemAlias">
                      <ItemAlias>
							<xsl:attribute name="AliasName">
							<xsl:value-of select="@AliasName"/>
							</xsl:attribute>
							<xsl:attribute name="AliasValue">
							<xsl:value-of select="@AliasValue"/>
							</xsl:attribute>
							</ItemAlias>
							</xsl:for-each>
					</ItemAliasList>
				</ItemDetails>
				 
                 <LineCharges>
                   <xsl:for-each select="ChainedFromOrderLine/LineCharges/LineCharge">
                   <LineCharge>
                     <xsl:attribute name="ChargeName">
                       <xsl:value-of select="@ChargeName"/>
                     </xsl:attribute>
                     <xsl:attribute name="ChargeCategory">
                       <xsl:value-of select="@ChargeCategory"/>
                     </xsl:attribute>
                     <xsl:attribute name="ChargePerUnit">
                       <xsl:value-of select="@ChargePerUnit"/>
                     </xsl:attribute>
                     <xsl:attribute name="ChargeAmount">
                       <xsl:value-of select="@ChargeAmount"/>
                     </xsl:attribute>
                     <xsl:attribute name="IsDiscount">
                       <xsl:value-of select="@IsDiscount"/>
                     </xsl:attribute>
                   </LineCharge>
                   </xsl:for-each>
                 </LineCharges>

                 <LinePriceInfo>
                <xsl:attribute name="UnitPrice">
                 <xsl:value-of select="ChainedFromOrderLine/LinePriceInfo/@UnitPrice"/>
                </xsl:attribute>
                   <xsl:attribute name="OrderedPricingQty">
                     <xsl:value-of select="ChainedFromOrderLine/LinePriceInfo/@OrderedPricingQty"/>

                   </xsl:attribute>
                   <xsl:attribute name="LineTotal">
                     <xsl:value-of select="ChainedFromOrderLine/LinePriceInfo/@LineTotal"/>
                   </xsl:attribute>
                 </LinePriceInfo>
                 
                 <LineTaxes>
                   <xsl:for-each select="ChainedFromOrderLine/LineTaxes/LineTax">
                   <LineTax>
                     <xsl:attribute name="ChargeName">
                       <xsl:value-of select="@ChargeName"/>
                     </xsl:attribute>
                     <xsl:attribute name="ChargeCategory">
                       <xsl:value-of select="@ChargeCategory"/>
                     </xsl:attribute>
                     <xsl:attribute name="Tax">
                       <xsl:value-of select="@Tax"/>
                     </xsl:attribute>
                     <xsl:attribute name="TaxName">
                       <xsl:value-of select="@TaxName"/>
                     </xsl:attribute>
                     <xsl:attribute name="TaxPercentage">
                       <xsl:value-of select="@TaxPercentage"/>
                     </xsl:attribute>
                   </LineTax>
                                                                  
                 </xsl:for-each>
                                                                <TaxSummary>
                                                                <xsl:for-each select="ChainedFromOrderLine/LineTaxes/TaxSummary/TaxSummaryDetail">
                                                                <TaxSummaryDetail>
                                                                <xsl:attribute name="OverallTax">
                       <xsl:value-of select="@OverallTax"/>
                     </xsl:attribute>
                                                                </TaxSummaryDetail>
                                                                </xsl:for-each>
                                                               
                                                                 </TaxSummary>
                                                               
                 </LineTaxes>

                 <LineOverallTotals>
                   <xsl:attribute name="Tax">
                     <xsl:value-of select="ChainedFromOrderLine/LineOverallTotals/@Tax"/>
                   </xsl:attribute>
                   <xsl:attribute name="UnitPrice">
                     <xsl:value-of select="ChainedFromOrderLine/LineOverallTotals/@UnitPrice"/>
                   </xsl:attribute>
                   <xsl:attribute name="LineTotal">
                     <xsl:value-of select="ChainedFromOrderLine/LineOverallTotals/@LineTotal"/>
                   </xsl:attribute>
                   <xsl:attribute name="ExtendedPrice">
                     <xsl:value-of select="ChainedFromOrderLine/LineOverallTotals/@ExtendedPrice"/>
                   </xsl:attribute>
                   <xsl:attribute name="Charges">
                     <xsl:value-of select="ChainedFromOrderLine/LineOverallTotals/@Charges"/>
                   </xsl:attribute>
                   <xsl:attribute name="Discount">
                     <xsl:value-of select="ChainedFromOrderLine/LineOverallTotals/@Discount"/>
                   </xsl:attribute>
                   <xsl:attribute name="ShippingTotal">
                     <xsl:value-of select="ChainedFromOrderLine/LineOverallTotals/@ShippingTotal"/>
                   </xsl:attribute>
                   <xsl:attribute name="ShippingCharges">
                     <xsl:value-of select="ChainedFromOrderLine/LineOverallTotals/@ShippingCharges"/>
                   </xsl:attribute>
                 </LineOverallTotals>
				 
				 <Extn>
                 <xsl:attribute name="ExtnWebLineNumber">
                   <xsl:value-of select="ChainedFromOrderLine/Extn/@ExtnWebLineNumber"/>
                 </xsl:attribute>
                 </Extn>
				 <Item>
                 <xsl:attribute name="ItemID">
                   <xsl:value-of select="ChainedFromOrderLine/Item/@ItemID"/>
                 </xsl:attribute>
                 </Item>
				 
               </ChainedFromOrderLine>
               <Extn>
                 <xsl:attribute name="ExtnColor">
                 </xsl:attribute>
                 <xsl:attribute name="ExtnSize">
                   <xsl:value-of select="Extn/@ExtnSize"/>
                 </xsl:attribute>
				 <xsl:attribute name="ExtnEstimatedShipDate">
                     <xsl:value-of select="ChainedFromOrderLine/Extn/@ExtnEstimatedShipDate"/>
                 </xsl:attribute>
				
               </Extn>
               
             </OrderLine> 
      </xsl:for-each>      
      <Extn>
        <xsl:attribute name="ExtnTransactionID">
          <xsl:value-of select="OrderRelease/Extn/@ExtnTransactionID"/>
        </xsl:attribute>
        <xsl:attribute name="ExtnTransactionIDBarCode">
          <xsl:value-of select="OrderRelease/Extn/@ExtnTransactionIDBarCode"/>
        </xsl:attribute>
		<xsl:attribute name="ExtnReleaseNo">
					<xsl:value-of select="OrderRelease/Extn/@ExtnReleaseNo"/>
        </xsl:attribute>
     </Extn>
     <PaymentMethod>
     	<xsl:attribute name="PaymentType">
               <xsl:value-of select="OrderRelease/PaymentMethod/@PaymentType"/>
        </xsl:attribute>
     </PaymentMethod>
     
	</OrderRelease>
	</xsl:template>
</xsl:stylesheet>