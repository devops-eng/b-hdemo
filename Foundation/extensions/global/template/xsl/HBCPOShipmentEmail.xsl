<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:element name="Shipment" >
			<xsl:copy-of select="Shipment/@*"/>
			<xsl:attribute name="DocumentType">
				<xsl:text>0001</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="OrderHeaderKey">
				<xsl:value-of select="Shipment/ShipmentLines/ShipmentLine/@ChainedFromOrderHeaderKey"/>
			</xsl:attribute>	
			<ToAddress>
				<xsl:copy-of select="Shipment/ToAddress/@*"/>
			</ToAddress>
			<BillToAddress>
				<xsl:copy-of select="Shipment/ShipmentLines/ShipmentLine/OrderLine/ChainedFromOrderLine/Order/PersonInfoBillTo/@*"/>
			</BillToAddress>
			<Containers>
				<xsl:for-each select="Shipment/Containers/Container">
					<Container>
						<xsl:copy-of select="@*"/>
						<Instructions>
							<xsl:for-each select="Instructions/Instruction">
								<Instruction>
									<xsl:copy-of select="@*[not(.='')]|node()"/>
									<xsl:apply-templates select="*"/>
								</Instruction>
							</xsl:for-each>
						</Instructions>
						<xsl:element name="ContainerDetails" >
							<xsl:for-each select="/Shipment/ShipmentLines/ShipmentLine">
								<xsl:element name="ContainerDetail" >
									<xsl:attribute name="ItemID">
										<xsl:value-of select="OrderLine/ChainedFromOrderLine/Item/@ItemID"/>
									</xsl:attribute>
									<xsl:attribute name="Quantity">
										<xsl:value-of select="@Quantity"/>
									</xsl:attribute>
									<xsl:attribute name="ShipmentKey">
										<xsl:value-of select="@ShipmentKey"/>
									</xsl:attribute>
									<xsl:attribute name="ShipmentLineKey">
										<xsl:value-of select="@ShipmentLineKey"/>
									</xsl:attribute>
									<xsl:attribute name="UnitOfMeasure">
										<xsl:value-of select="OrderLine/ChainedFromOrderLine/Item/@UnitOfMeasure"/>
									</xsl:attribute>
									<xsl:element name="ShipmentLine" >
									<xsl:attribute name="ItemID">
										<xsl:value-of select="OrderLine/ChainedFromOrderLine/Item/@ItemID"/>
									</xsl:attribute>
									<xsl:attribute name="Quantity">
										<xsl:value-of select="@Quantity"/>
									</xsl:attribute>
									<xsl:attribute name="ShipmentKey">
										<xsl:value-of select="@ShipmentKey"/>
									</xsl:attribute>
									<xsl:attribute name="ShipmentLineKey">
										<xsl:value-of select="@ShipmentLineKey"/>
									</xsl:attribute>
									<xsl:attribute name="UnitOfMeasure">
										<xsl:value-of select="OrderLine/ChainedFromOrderLine/Item/@UnitOfMeasure"/>
									</xsl:attribute> 
								</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</xsl:element> 
					</Container>
				</xsl:for-each>
			</Containers>
			<Instructions>
				<xsl:for-each select="Shipment/Instructions/Instruction">
					<Instruction>
						<xsl:copy-of select="@*[not(.='')]|node()"/>
						<xsl:apply-templates select="*"/>
					</Instruction>
				</xsl:for-each>
			</Instructions>
			<ShipmentCharges>
				<xsl:for-each select="Shipment/ShipmentCharges/ShipmentCharge">
					<ShipmentCharge>
						<xsl:copy-of select="@*[not(.='')]|node()"/>
						<xsl:apply-templates select="*"/>
					</ShipmentCharge>
				</xsl:for-each>
			</ShipmentCharges>
			<ShipmentLines>
				<xsl:for-each select="Shipment/ShipmentLines/ShipmentLine">
					<xsl:element name="ShipmentLine">
						<xsl:copy-of select="@*"/>
						<xsl:attribute name="OrderHeaderKey">
							<xsl:value-of select="@ChainedFromOrderHeaderKey"/>
						</xsl:attribute>
						<xsl:attribute name="OrderLineKey">
							<xsl:value-of select="@ChainedFromOrderLineKey"/>
						</xsl:attribute>
						<xsl:attribute name="OrderNo">
							<xsl:value-of select="OrderLine/ChainedFromOrderLine/Order/@OrderNo"/>
						</xsl:attribute>
						<xsl:for-each select="OrderLine/ChainedFromOrderLine/Order">
							<xsl:element name="Order">
								<xsl:copy-of select="@*[not(.='')]|node()"/>
								<xsl:apply-templates select="*"/>
							</xsl:element>
						</xsl:for-each> 
						<xsl:element name="OrderLine">
						<xsl:attribute name="Modifyts">
							<xsl:value-of select="OrderLine/@Modifyts"/>
						</xsl:attribute>
						<xsl:attribute name="Status">
							<xsl:value-of select="OrderLine/@Status"/>
						</xsl:attribute>
							<xsl:copy-of select="OrderLine/ChainedFromOrderLine/@*"/>
							<xsl:element name="Item">
								<xsl:copy-of select="OrderLine/ChainedFromOrderLine/Item/@*"/>
							</xsl:element>
							<xsl:element name="LinePriceInfo">
								<xsl:copy-of select="OrderLine/ChainedFromOrderLine/LinePriceInfo/@*"/>
							</xsl:element>
							<xsl:for-each select="OrderLine/ChainedFromOrderLine/ItemDetails">
								<xsl:element name="ItemDetails">
									<xsl:copy-of select="@*[not(.='')]|node()"/>
									<xsl:apply-templates select="*"/>
								</xsl:element>
							</xsl:for-each> 
							<xsl:element name="LineOverallTotals">
								<xsl:copy-of select="OrderLine/ChainedFromOrderLine/LineOverallTotals/@*"/>
							</xsl:element>
							<xsl:element name="LineCharges">
								<xsl:for-each select="OrderLine/ChainedFromOrderLine/LineCharges/LineCharge">
									<xsl:element name="LineCharge">
										<xsl:copy-of select="@*[not(.='')]|node()"/>
										<xsl:apply-templates select="*"/>
									</xsl:element>
								</xsl:for-each> 
							</xsl:element>
							<xsl:element name="LineTaxes">
								<xsl:for-each select="OrderLine/ChainedFromOrderLine/LineTaxes/LineTax">
									<xsl:element name="LineTax">
										<xsl:copy-of select="@*[not(.='')]|node()"/>
										<xsl:apply-templates select="*"/>
									</xsl:element>
								</xsl:for-each> 
							</xsl:element>
							<xsl:element name="OrderLineTranQuantity">
								<xsl:copy-of select="OrderLine/ChainedFromOrderLine/OrderLineTranQuantity/@*"/>
							</xsl:element>
							<Extn>
							<xsl:attribute name="ExtnWebLineNumber">
							<xsl:value-of select="OrderLine/ChainedFromOrderLine/Extn/@ExtnWebLineNumber"/>	
							</xsl:attribute>							
							</Extn>
						</xsl:element>

						<xsl:for-each select="ShipmentTagSerials">
							<xsl:element name="ShipmentTagSerials">
								<xsl:copy-of select="@*[not(.='')]|node()"/>
								<xsl:apply-templates select="*"/>
							</xsl:element>
						</xsl:for-each> 
						<xsl:for-each select="Instructions">
							<xsl:element name="Instructions">
								<xsl:copy-of select="@*[not(.='')]|node()"/>
								<xsl:apply-templates select="*"/>
							</xsl:element>
						</xsl:for-each> 
						<xsl:for-each select="ShipmentLineInvAttRequest">
							<xsl:element name="ShipmentLineInvAttRequest">
								<xsl:copy-of select="@*[not(.='')]|node()"/>
								<xsl:apply-templates select="*"/>
							</xsl:element>
						</xsl:for-each> 
					</xsl:element>
				</xsl:for-each>
			</ShipmentLines>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>

