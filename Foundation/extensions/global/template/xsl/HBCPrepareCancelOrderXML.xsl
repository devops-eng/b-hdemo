<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">
	<xsl:template match="/">
		<Order ModificationReasonText="Systematic Backorder Cancellation">
			<xsl:copy-of select="MonitorConsolidation/Order/@*" />
			<OrderLines>
				<xsl:for-each select="MonitorConsolidation/Order/OrderLines/OrderLine">
				<xsl:if test="@LineType!='PREORDER'">
					<OrderLine>
						<xsl:copy-of select="./@*" />
						<Item>
							<xsl:copy-of select="./Item/@*" />
						</Item>
					</OrderLine>
				</xsl:if>
				</xsl:for-each>
			</OrderLines>
			<OrderStatuses>
				<xsl:for-each select="MonitorConsolidation/Order/OrderStatuses/OrderStatus">
					<OrderStatus>
						<xsl:copy-of select="./@*" />
					</OrderStatus>
				</xsl:for-each>
			</OrderStatuses>
		</Order>
	</xsl:template>
</xsl:stylesheet>