<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
		<CancelReservationList>
			<xsl:for-each select="InventoryReservations/InventoryReservation">
			<xsl:value-of select="."/>
			<CancelReservation>
			<xsl:attribute name="ItemID">
			<xsl:value-of select="Item/@ItemID"/>
			</xsl:attribute>
			<xsl:attribute name="OrganizationCode">
			<xsl:value-of select="@OrganizationCode"/>
			</xsl:attribute>
			<xsl:attribute name="ReservationID">
			<xsl:value-of select="@ReservationID"/>
			</xsl:attribute>
			<xsl:attribute name="QtyToBeCancelled">
			<xsl:value-of select="@Quantity"/>
			</xsl:attribute>
			<xsl:attribute name="ShipNode">
			<xsl:value-of select="@ShipNode"/>
			</xsl:attribute>
			<xsl:attribute name="UnitOfMeasure">
			<xsl:value-of select="Item/@UnitOfMeasure"/>
			</xsl:attribute>
			<xsl:attribute name="ShipDate">
			<xsl:value-of select="@ShipDate"/>
			</xsl:attribute>
			</CancelReservation>
			</xsl:for-each>
		</CancelReservationList>
	</xsl:template>
</xsl:stylesheet>


						
					