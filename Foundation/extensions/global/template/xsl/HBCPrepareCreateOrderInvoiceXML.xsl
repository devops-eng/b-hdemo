<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<Order>
			<xsl:attribute name="DocumentType">
				<xsl:value-of select="/Receipt/@DocumentType"/>
			</xsl:attribute>
			<xsl:attribute name="EnterpriseCode">
				<xsl:value-of select="/Receipt/Shipment/@EnterpriseCode"/>
			</xsl:attribute>
			<!--Defect 403 Apr 15 Release : RO lines stuck in Receipt Closed Status
			<xsl:attribute name="IgnoreStatusCheck">
				<xsl:value-of select="'Y'"/>
			</xsl:attribute> -->
			 <!--<xsl:attribute name="IgnoreTransactionDependencies">
				<xsl:value-of select="'Y'"/>
			</xsl:attribute> -->
			<xsl:attribute name="OrderNo">
				<xsl:value-of select="/Receipt/Shipment/@OrderNo"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionId">
				<xsl:value-of select="'CREATE_ORDER_INVOICE.0003'"/>
			</xsl:attribute>
			<OrderLines>
				<xsl:for-each select="/Receipt/ReceiptLines/ReceiptLine">
					<OrderLine>
						<xsl:attribute name="PrimeLineNo">
							<xsl:value-of select="./@PrimeLineNo"/>
						</xsl:attribute>
						<xsl:attribute name="Quantity">
							<xsl:value-of select="./@Quantity"/>
						</xsl:attribute>
						<xsl:attribute name="SubLineNo">
							<xsl:value-of select="./@SubLineNo"/>
						</xsl:attribute>
					</OrderLine>
				</xsl:for-each>
			</OrderLines>
		</Order>
	</xsl:template>
</xsl:stylesheet>
