<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
	        <InventoryReservation>
			<xsl:attribute name="OrganizationCode">
				<xsl:value-of select="Promise/@OrganizationCode"/>
			</xsl:attribute>			
			<xsl:attribute name="ReservationID">
				<xsl:value-of select="Promise/ReservationParameters/@ReservationID"/>
			</xsl:attribute>
		</InventoryReservation>
	</xsl:template>
</xsl:stylesheet>




