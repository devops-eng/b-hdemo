<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:lxslt="http://xml.apache.org/xslt" version="1.0">
	<xsl:template match="/">
		<HTML>
			<xsl:comment>FROM=omsdaily.reports@hbc.com</xsl:comment>
			<xsl:comment>SUBJECT=Order cancellation request</xsl:comment>
			<xsl:comment>CONTENT_TYPE=text/html</xsl:comment>
			<HEAD>
				<title>Request for cancellation of <xsl:value-of select="//@EnterpriseCode"/> Order # <xsl:value-of select="//Order/@OrderNo"/>.</title>
			</HEAD>
				<BODY topmargin="0" leftmargin="6">
				<BR/>
				<font>Hi,</font>
				<BR/>
				<BR/>
				<font>Request you to cancel the below order lines of <B> <xsl:value-of select="//@EnterpriseCode"/> Order # <xsl:value-of select="//@OrderNo"/></B>.</font>
				<BR/>
				<BR/>
				<TABLE BORDER="1" BORDERCOLOR="#000000" style="border-collapse: collapse;">
					<TR>
						<TH BGCOLOR="#cdc8b1">
							<b>
								<font size="2">Line #</font>
							</b>
						</TH>
						<TH BGCOLOR="#cdc8b1">
							<b>
								<font size="2">Item ID</font>
							</b>
						</TH>
						<TH BGCOLOR="#cdc8b1">
							<b>
								<font size="2">
									<xsl:text>Item Description</xsl:text>
								</font>
							</b>
						</TH>
						<TH BGCOLOR="#cdc8b1">
							<b>
								<font size="2">
									<xsl:text>Model #</xsl:text>
								</font>
							</b>
						</TH>
						<TH BGCOLOR="#cdc8b1">
							<b>
								<font size="2">Ship Node</font>
							</b>
						</TH>
						<TH BGCOLOR="#cdc8b1">
							<b>
								<font size="2">Quantity</font>
							</b>
						</TH>
						<TH BGCOLOR="#cdc8b1">
							<b>
								<font size="2">Color</font>
							</b>
						</TH>
						<TH BGCOLOR="#cdc8b1">
							<b>
								<font size="2">Size</font>
							</b>
						</TH>
						<TH BGCOLOR="#cdc8b1">
							<b>
								<font size="2">UPC</font>
							</b>
						</TH>
						<TH BGCOLOR="#cdc8b1">
							<b>
								<font size="2">Release #</font>
							</b>
						</TH>
						<TH BGCOLOR="#cdc8b1">
							<b>
								<font size="2">Ship Advice #</font>
							</b>
						</TH>
					</TR>
					<xsl:for-each select="//OrderLines/OrderLine">
						<TR>
							<TD align="center">
								<font size="2">
									<xsl:value-of select="./@PrimeLineNo"/>
								</font>
							</TD>
							<TD align="center">
								<font size="2">
									<xsl:value-of select="./ItemDetails/@ItemID"/>
								</font>
							</TD>
							<TD align="center">
								<font size="2">
									<xsl:value-of select="./ItemDetails/PrimaryInformation/@ExtendedDisplayDescription"/>
								</font>
							</TD>
							<TD align="center" BORDER="2">
								<font size="2">
									<xsl:value-of select="./ItemDetails/ClassificationCodes/@Model"/>
								</font>
							</TD>
							<TD align="center" BORDER="2">
								<font size="2">
									<xsl:value-of select="./@ShipNode"/>
								</font>
							</TD>
							<TD align="center">
								<font size="2">
									<xsl:value-of select="./@OrderedQty"/>
								</font>
							</TD>
							<TD align="center">
								<font size="2">
									<xsl:value-of select="./ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Color']/@Value"/>
								</font>
							</TD>
							<TD align="center">
								<font size="2">
									<xsl:value-of select="./ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Size']/@Value"/>
								</font>
							</TD>
							<TD align="center">
								<font size="2">
									<xsl:choose>
										<xsl:when test="//@EnterpriseCode = 'LT' or //@EnterpriseCode = 'BAY'">
											<xsl:value-of select="./ItemDetails/ItemAliasList/ItemAlias[@AliasName='UPC']/@AliasValue"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="./ItemDetails/ItemAliasList/ItemAlias[@AliasName='ACTIVE_UPC']/@AliasValue"/>
										</xsl:otherwise>
									</xsl:choose>
								</font>
							</TD>
							<TD align="center">
								<font size="2">
									<xsl:value-of select="./@ReleaseNo"/>
								</font>
							</TD>
							<TD align="center">
								<font size="2">
									<xsl:value-of select="./@ShipAdviceNo"/>
								</font>
							</TD>
						</TR>
					</xsl:for-each>
				</TABLE>
				<BR/>
				<font>Sincerely,</font>
				<BR/>
				<font>HBC Digital OMS Support</font>
				<BR/>
			</BODY>
		</HTML>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2004. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="DC_Release_email_input.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->