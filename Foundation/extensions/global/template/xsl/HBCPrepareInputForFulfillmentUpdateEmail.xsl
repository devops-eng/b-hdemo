<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:lxslt="http://xml.apache.org/xslt" version="1.0">
	<xsl:template match="/">
		<HTML>
			<xsl:comment>FROM=omsdaily.reports@hbc.com</xsl:comment>
			<xsl:comment>SUBJECT=Update on order cancellation request</xsl:comment>
			<xsl:comment>CONTENT_TYPE=text/html</xsl:comment>
			<HEAD>
				<title>Fulfillment update on <xsl:value-of select="//@EnterpriseCode"/> Order # <xsl:value-of select="//Order/@OrderNo"/>.</title>
			</HEAD>
			<BODY topmargin="0" leftmargin="6">
				<BR/>
				<font>Hi,</font>
				<BR/>
				<BR/>
				<font>Please find the update on cancellation request of <B> <xsl:value-of select="//@EnterpriseCode"/> Order # <xsl:value-of select="//Order/@OrderNo"/></B>.</font>
				<BR/>
				<BR/>
				<xsl:choose>
					<xsl:when test="//Order/@FulfillmentStatus = 'Shipped'">
						<font><b>Following order lines have been shipped by DC:</b></font>
					</xsl:when>
					<xsl:otherwise>
						<font><b>Following order lines have been cancelled by DC:</b></font>
					</xsl:otherwise>
				</xsl:choose>
				<BR/>
				<BR/>
				<TABLE BORDER="1" BORDERCOLOR="#000000" style="border-collapse: collapse;">
					<TR>
						<TH BGCOLOR="#cdc8b1">
							<b>
								<font size="2">Line #</font>
							</b>
						</TH>
						<TH BGCOLOR="#cdc8b1">
							<b>
								<font size="2">Item ID</font>
							</b>
						</TH>
						<TH BGCOLOR="#cdc8b1">
							<b>
								<font size="2">
									<xsl:text>Item Description</xsl:text>
								</font>
							</b>
						</TH>
						<TH BGCOLOR="#cdc8b1">
							<b>
								<font size="2">
									<xsl:text>Model #</xsl:text>
								</font>
							</b>
						</TH>
						<TH BGCOLOR="#cdc8b1">
							<b>
								<font size="2">Ship Node</font>
							</b>
						</TH>
						<TH BGCOLOR="#cdc8b1">
							<b>
								<xsl:choose>
									<xsl:when test="//Order/@FulfillmentStatus = 'Shipped'">
										<font size="2">Shipped Quantity</font>
									</xsl:when>
									<xsl:otherwise>
										<font size="2">Cancelled Quantity</font>
									</xsl:otherwise>
								</xsl:choose>
							</b>
						</TH>
						<TH BGCOLOR="#cdc8b1">
							<b>
								<font size="2">Color</font>
							</b>
						</TH>
						<TH BGCOLOR="#cdc8b1">
							<b>
								<font size="2">Size</font>
							</b>
						</TH>
						<TH BGCOLOR="#cdc8b1">
							<b>
								<font size="2">ShipTo Address</font>
							</b>
						</TH>
					</TR>
					<xsl:for-each select="//Order/OrderLines/OrderLine">
						<TR>
							<TD align="center">
								<font size="2">
									<xsl:value-of select="./@PrimeLineNo"/>
								</font>
							</TD>
							<TD align="center">
								<font size="2">
									<xsl:value-of select="./ItemDetails/@ItemID"/>
								</font>
							</TD>
							<TD align="center">
								<font size="2">
									<xsl:value-of select="./ItemDetails/PrimaryInformation/@ExtendedDisplayDescription"/>
								</font>
							</TD>
							<TD align="center" BORDER="2">
								<font size="2">
									<xsl:value-of select="./ItemDetails/ClassificationCodes/@Model"/>
								</font>
							</TD>
							<TD align="center" BORDER="2">
								<font size="2">
									<xsl:value-of select="./@ShipNode"/>
								</font>
							</TD>
							<TD align="center">
								<xsl:choose>
									<xsl:when test="//Order/@FulfillmentStatus = 'Shipped'">
										<font size="2">
											<xsl:value-of select="./@ShippedQty"/>
										</font>
									</xsl:when>
									<xsl:otherwise>
										<font size="2">
											<xsl:value-of select="./@OrderedQty"/>
										</font>
									</xsl:otherwise>
								</xsl:choose>
							</TD>
							<TD align="center">
								<font size="2">
									<xsl:value-of select="./ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Color']/@Value"/>
								</font>
							</TD>
							<TD align="center">
								<font size="2">
									<xsl:value-of select="./ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Size']/@Value"/>
								</font>
							</TD>
							<TD align="center">
								<font size="2">
									<xsl:value-of select="./PersonInfoShipTo/@FirstName"/>
									<xsl:text> </xsl:text>
									<xsl:value-of select="./PersonInfoBillTo/@LastName"/>
									<br/>
									<xsl:value-of select="./PersonInfoShipTo/@AddressLine1"/>
									<xsl:text> </xsl:text>
									<xsl:value-of select="./PersonInfoShipTo/@AddressLine2"/>
									<br/>
									<xsl:value-of select="./PersonInfoShipTo/@City"/>
									<xsl:text> </xsl:text>
									<xsl:value-of select="./PersonInfoShipTo/@State"/>
									<xsl:text> </xsl:text>
									<xsl:value-of select="./PersonInfoShipTo/@ZipCode"/>
									<br/>
									<xsl:value-of select="./PersonInfoShipTo/@Country"/>
								</font>
							</TD>
						</TR>
					</xsl:for-each>
				</TABLE>
				<BR/>
				<xsl:apply-templates select="//Order/PersonInfoBillTo"/>
				<BR/>
				<xsl:choose>
					<xsl:when test="//Order/@FulfillmentStatus = 'Shipped'">
						<font>Kindly update the customer with fulfillment details and suggest return creation.</font>
					</xsl:when>
					<xsl:otherwise>
						<font>Customer has been notified by Order Cancellation E-mail.</font>
					</xsl:otherwise>
				</xsl:choose>
				<BR/>
				<BR/>
				<font>Sincerely,</font>
				<BR/>
				<font>HBC Digital OMS Support</font>
				<BR/>
			</BODY>
		</HTML>
	</xsl:template>
	<xsl:template match="PersonInfoBillTo">
		<font>
			<b>Customer contact details:</b>
		</font>
		<BR/>
		<BR/>
		<TABLE BORDER="1" BORDERCOLOR="#000000" style="border-collapse: collapse;">
			<TR>
				<TD align="left" BGCOLOR="#cdc8b1">
					<b>
						<font size="2">
							<xsl:text>Name</xsl:text>
						</font>
					</b>
				</TD>
				<TD align="left">
					<font size="2">
						<xsl:value-of select="//Order/PersonInfoBillTo/@FirstName"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="//Order/PersonInfoBillTo/@LastName"/>
					</font>
				</TD>
			</TR>
			<TR>
				<TD align="left" BGCOLOR="#cdc8b1">
					<b>
						<font size="2">
							<xsl:text>Address</xsl:text>
						</font>
					</b>
				</TD>
				<TD align="left">
					<font size="2">
						<xsl:value-of select="//Order/PersonInfoBillTo/@AddressLine1"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="//Order/PersonInfoBillTo/@AddressLine2"/>
						<br/>
						<xsl:value-of select="//Order/PersonInfoBillTo/@City"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="//Order/PersonInfoBillTo/@State"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="//Order/PersonInfoBillTo/@ZipCode"/>
						<br/>
						<xsl:value-of select="//Order/PersonInfoBillTo/@Country"/>
					</font>
				</TD>
			</TR>
			<TR>
				<TD align="left" BGCOLOR="#cdc8b1">
					<b>
						<font size="2">
							<xsl:text>Phone #</xsl:text>
						</font>
					</b>
				</TD>
				<TD align="left">
					<font size="2">
						<xsl:value-of select="//Order/PersonInfoBillTo/@DayPhone"/>
					</font>
				</TD>
			</TR>
			<TR>
				<TD align="left" BGCOLOR="#cdc8b1">
					<b>
						<font size="2">
							<xsl:text>Email</xsl:text>
						</font>
					</b>
				</TD>
				<TD align="left">
					<font size="2">
						<xsl:value-of select="//Order/PersonInfoBillTo/@EMailID"/>
					</font>
				</TD>
			</TR>
		</TABLE>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2004. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="Shipped_input.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->