<?xml version="1.0" encoding="UTF-8"?>
<!-- Input XML 
<GetRewardsInfo DisplayLocalizedFieldInLocale="en_US_EST" IgnoreOrdering="Y">
    <sRewardsInfoIn>
        <RewardsInfo CCType="0" EstimatedPoints="0" OrderSubtotal="0" RewardsNumber="921041175" TotalPointsToDate="0"/>
    </sRewardsInfoIn>
</GetRewardsInfo>
-->
<!-- This XSLT prepares input for HBCLoyaltyPointsWebService in format required for web-service call. -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output indent="yes"/>
	<xsl:template match="/">
	<GetRewardsInfo>
			<xsl:variable name="varCCType" select="/GetRewardsInfo/sRewardsInfoIn/RewardsInfo/@CCType"/>
			<xsl:variable name="varEstimatedPoints" select="/GetRewardsInfo/sRewardsInfoIn/RewardsInfo/@EstimatedPoints"/>
			<xsl:variable name="varOrderSubtotal" select="/GetRewardsInfo/sRewardsInfoIn/RewardsInfo/@OrderSubtotal"/>
			<xsl:variable name="varRewardsNumber" select="/GetRewardsInfo/sRewardsInfoIn/RewardsInfo/@RewardsNumber"/>
			<xsl:variable name="varTotalPointsToDate" select="/GetRewardsInfo/sRewardsInfoIn/RewardsInfo/@TotalPointsToDate"/>
			<xsl:copy-of select="/GetRewardsInfo/@*"/>
			<sRewardsInfoIn>
			<xsl:text>&lt;RewardsInfo CCType="</xsl:text>
				<xsl:value-of select="$varCCType"/>
			<xsl:text>" EstimatedPoints="</xsl:text>
				<xsl:value-of select="$varEstimatedPoints"/>
			<xsl:text>" OrderSubtotal="</xsl:text>
				<xsl:value-of select="$varOrderSubtotal"/>
			<xsl:text>" RewardsNumber="</xsl:text>
				<xsl:value-of select="$varRewardsNumber"/>
			<xsl:text>" TotalPointsToDate="</xsl:text>
				<xsl:value-of select="$varTotalPointsToDate"/>
			<xsl:text>" xmlns="eretailwebservices.hbc.com" /&gt;</xsl:text>
			</sRewardsInfoIn>
	</GetRewardsInfo>
	</xsl:template>
</xsl:stylesheet>

<!-- Output XML
<GetRewardsInfo DisplayLocalizedFieldInLocale="en_US_EST" IgnoreOrdering="Y">
    <sRewardsInfoIn>
        &lt;RewardsInfo CCType="0" EstimatedPoints="0" OrderSubtotal="0"
            RewardsNumber="921041175" TotalPointsToDate="0" xmlns="eretailwebservices.hbc.com"/&gt;
    </sRewardsInfoIn>
</GetRewardsInfo>
-->