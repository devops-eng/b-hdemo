<!--Code Merge Changes -->
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.1"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                exclude-result-prefixes="fo" xmlns:exsl="http://exslt.org/common" extension-element-prefixes="exsl">
	
	<xsl:template match="/">
		<xsl:variable name="EntCode" select="//MultiApi/API[@Name='getSortedShipmentDetails']/Output/Shipment/@EnterpriseCode">
		</xsl:variable>
		<xsl:choose>
		<xsl:when test="($EntCode='BAY') or ($EntCode='LT') or ($EntCode='HO')">
		
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
		
            <fo:layout-master-set>
                <fo:simple-page-master master-name="Bay-page-master" page-height="27.94cm" page-width="21.59cm" >
					<fo:region-body margin="6mm"/>
				</fo:simple-page-master>
				
				 <fo:simple-page-master master-name="LT-page-master" page-height="356mm" page-width="216mm" >				
					<fo:region-body margin="4mm"/>						
				</fo:simple-page-master>
				
				<fo:page-sequence-master master-name="Bay-page">
					<fo:repeatable-page-master-reference master-reference="Bay-page-master"/> 
				</fo:page-sequence-master>
				
				<fo:page-sequence-master master-name="LT-page">
					<fo:repeatable-page-master-reference master-reference="LT-page-master"/>
				</fo:page-sequence-master>
            </fo:layout-master-set>	
            <xsl:if test="/MultiApi/API[@Name='getSortedShipmentDetails']/Output/Shipment/@EnterpriseCode[.='BAY']">
            <fo:page-sequence master-reference="Bay-page">
			
                <fo:flow flow-name="xsl-region-body">
			
				<fo:block-container absolute-position="absolute"
				left="6mm" top="30mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/BillToAddress/@FirstName"/>
						<fo:inline space-before="2mm" margin-left="2mm">	
                   <xsl:value-of select="concat(' ',//Shipment/BillToAddress/@LastName)"/>
						</fo:inline>
				</fo:block>
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="6mm" top="34mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/BillToAddress/@AddressLine1"/>
				</fo:block>
						
				</fo:block-container>
			
				<fo:block-container absolute-position="absolute"
				left="6mm" top="38mm" width="50mm" height="4mm">
				
				<fo:block  
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/BillToAddress/@City"/>
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="concat(', ',//Shipment/BillToAddress/@State)"/>
						</fo:inline>
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="concat(' ',//Shipment/BillToAddress/@ZipCode)"/>	
						</fo:inline>
						</fo:block>
						
				</fo:block-container>
				
				// country
				<fo:block-container absolute-position="absolute"
				left="6mm" top="42mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/BillToAddress/@Country"/>
						</fo:block>		
				</fo:block-container>
				
				
				//Store Address blocks				
				
				//First name , last name
				<fo:block-container absolute-position="absolute"
				left="72mm" top="30mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
				
						<xsl:value-of select="//Shipment/ShipNode/ShipNodePersonInfo/@FirstName"/>
						<fo:inline space-before="2mm" margin-left="2mm">
                         <xsl:value-of select="concat(' ',//Shipment/ShipNode/ShipNodePersonInfo/@LastName)"/>
						</fo:inline>
						</fo:block>
						
				</fo:block-container>
					
				<fo:block-container absolute-position="absolute"
				left="72mm" top="34mm" width="50mm" height="4mm">
				
				<fo:block  
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ShipNode/ShipNodePersonInfo/@AddressLine1"/>
						</fo:block>
						
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="72mm" top="38mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ShipNode/ShipNodePersonInfo/@City"/>
						
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="concat(', ',//Shipment/ShipNode/ShipNodePersonInfo/@State)"/>
                           </fo:inline>
						
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="concat(' ',//Shipment/ShipNode/ShipNodePersonInfo/@ZipCode)"/>
						</fo:inline>
						</fo:block>		
				</fo:block-container>
				
				//country
				<fo:block-container absolute-position="absolute"
				left="72mm" top="42mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ToAddress/@Country"/>
						</fo:block>		
				</fo:block-container>
								
				<fo:block-container absolute-position="absolute"
				left="177mm" top="22mm" width="60mm" height="4mm">
				<fo:block
				font-family="Helvetica" font-size="8pt" font-weight="normal"> 
						<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/@OrderNo"/>
				</fo:block>			
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="145mm" top="30mm" width="60mm" height="4mm">
				<fo:block  	font-family="Helvetica" font-size="7pt" font-weight="normal">
				Customer #/N de client:	<xsl:value-of select="//Shipment/ToAddress/@EMailID"/>
				</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="145mm" top="36mm" width="60mm" height="4mm">
				
				<fo:block 	font-family="Helvetica" font-size="7pt" font-weight="normal">
					Order Date/Date da le commande: <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/Order/@OrderDate"/>
				</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="145mm" top="42mm" width="60mm" height="4mm">
				 
				<fo:block
				font-family="Helvetica" font-size="7pt" font-weight="normal">
				T/N:
				  <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/Extn/@ExtnTransactionID"/>
						
						</fo:block>		
				</fo:block-container>
                      <fo:block-container absolute-position="absolute"
				left="145mm" top="46mm" width="60mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
				
					<xsl:value-of select="//Shipment/@SCAC"/>
						</fo:block>		
				</fo:block-container>
				
				//Item details table				
				
				//1 UPC
				<fo:block-container absolute-position="absolute"
				left="2mm" top="65mm" width="25mm" height="4mm">
				
				<fo:block
				font-family="Helvetica" font-size="7pt" font-weight="normal">					
						<fo:table>
                       <fo:table-column		column-width="2.5cm" />
									<fo:table-column column-width="5cm" />
									<fo:table-column
										column-width="2.5cm" />
									<fo:table-column
										column-width="2.3cm" />
									<fo:table-column
										column-width="0.9cm" />
									<fo:table-column
										column-width="1.9cm" />
									<fo:table-column column-width="1cm" />
									<fo:table-column
										column-width="1.5cm" />
									<fo:table-column
										column-width="0.8cm" />
									<fo:table-column
										column-width="1.8cm" />
                  <fo:table-body>
					<xsl:for-each select="//Shipment/ShipmentLines/ShipmentLine">
					<xsl:if test="(@ActualQuantity!='null' and @ActualQuantity > 0 )">
				<fo:table-row>
				<fo:table-cell>	
					<fo:block	font-family="Helvetica" font-size="7pt" font-weight="normal" text-align="center">
						<xsl:value-of select="OrderLine/ItemDetails/ItemAliasList/ItemAlias/@AliasValue"/>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell>
					<fo:block   font-family="Helvetica" font-size="7pt" font-weight="normal" text-align="center">
						<xsl:value-of select="OrderLine/ItemDetails/PrimaryInformation/@ShortDescription"/>
					</fo:block>
						
				 </fo:table-cell>
				<fo:table-cell>
					<fo:block	font-family="Helvetica" font-size="7pt" font-weight="normal"  text-align="center">
						<xsl:value-of select="OrderLine/ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Color']/@Value"/>
					</fo:block>		
				</fo:table-cell>
				<fo:table-cell>
				<fo:block font-family="Helvetica" font-size="7pt" font-weight="normal" text-align="center">
					  <xsl:value-of select="OrderLine/ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Size']/@Value"/>
						</fo:block>		
				</fo:table-cell>
				<fo:table-cell>			
					<fo:block font-family="Helvetica" font-size="7pt" font-weight="normal" text-align="left">
						<xsl:choose>	
						 <xsl:when test="@ActualQuantity!='null' and  @ActualQuantity > 0">
						 <xsl:value-of select="@ActualQuantity" />
						 </xsl:when>
						 <xsl:otherwise>
						<xsl:value-of select="@BackroomPickedQuantity" />
						</xsl:otherwise>
						</xsl:choose>
						</fo:block>		
				 </fo:table-cell>
				<fo:table-cell>			
									
				<fo:block 	font-family="Helvetica" font-size="7pt" font-weight="normal" text-align="center">
					 <xsl:variable name="UnitPrice">
				    <xsl:value-of select="OrderLine/LinePriceInfo/@UnitPrice" />
			       </xsl:variable>
			   
				    <xsl:variable name="ChargePerUnit">
				     <xsl:value-of select="OrderLine/LineCharges/LineCharge[@ChargeCategory='DISCOUNT' and @ChargeName='DISCOUNT']/@ChargePerUnit" />
				     </xsl:variable>
				      <xsl:if test="$ChargePerUnit != 'null' and $ChargePerUnit!=''">
				       <xsl:value-of select="(($UnitPrice) -($ChargePerUnit))" />
				       </xsl:if>
					 <xsl:if test="$ChargePerUnit = 'null' or $ChargePerUnit =''">
				      <xsl:value-of select="($UnitPrice) " />
				      </xsl:if>
						</fo:block>		
				</fo:table-cell>
			    <fo:table-cell>
					<fo:block	font-family="Helvetica" font-size="7pt" font-weight="normal" text-align="left">
						<xsl:value-of select="OrderLine/@GiftFlag"/>
					</fo:block>		
				</fo:table-cell>
				 <fo:table-cell>
				
					<fo:block 	font-family="Helvetica" font-size="7pt" font-weight="normal" text-align="left">
						<xsl:value-of select="OrderLine/@GiftWrap"/>
					</fo:block>		
			      </fo:table-cell>
				<fo:table-cell>
				<fo:block	font-family="Helvetica" font-size="7pt" font-weight="normal" text-align="left">
				 <xsl:value-of select="OrderLine/LineCharges/LineCharge[@ChargeCategory='ECOFEE' and @ChargeName='ECOFEE']/@ChargeAmount"/>
				</fo:block>		
				</fo:table-cell>
			    <fo:table-cell>
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal" text-align="left">
						<xsl:variable name="BackroomPickedQuantity">
						   <xsl:value-of select="@BackroomPickedQuantity" />
						  </xsl:variable>
						  <xsl:variable name="ActualQuantity">
						   <xsl:value-of select="@ActualQuantity" />
						  </xsl:variable>
						  	 <xsl:variable name="UnitPrice">
				   			 <xsl:value-of select="OrderLine/LinePriceInfo/@UnitPrice" />
			     			  </xsl:variable>
			     			 <xsl:variable name="Tax">
				   			 <xsl:value-of select="OrderLine/LineOverallTotals/@Tax" />
			     			  </xsl:variable>
						  
							    <xsl:variable name="ChargeAmount">
							      <xsl:value-of select="OrderLine/LineCharges/LineCharge[@ChargeCategory='ECOFEE' and @ChargeName='ECOFEE']/@ChargeAmount"/>
							    </xsl:variable>
								  <xsl:if test="(($ChargeAmount != 'null' and $ChargeAmount!='') and ($Tax!='null'))">
								  <xsl:variable name="TotalPrice">
								   <xsl:if test="$ActualQuantity!='null' and $ActualQuantity > 0">
								   <xsl:value-of select="((($ActualQuantity) * ($UnitPrice)) + ($ChargeAmount)+ $Tax )" />
								   </xsl:if>
								   
								     </xsl:variable>
								      <xsl:value-of select="(round($TotalPrice*100) div 100)"/>
								        </xsl:if>
										<xsl:if test="(($ChargeAmount = 'null' or $ChargeAmount ='') and ($Tax!='null' ))">
										<xsl:variable name="TotalPrice">
										  <xsl:if test="$ActualQuantity!='null' and $ActualQuantity > 0">
										 <xsl:value-of select="((($ActualQuantity) * ($UnitPrice))+ $Tax)" />
										 </xsl:if>
										
									      </xsl:variable>
									       <xsl:value-of select="format-number((round($TotalPrice*100) div 100),'#.00')"/>
										</xsl:if>
														  
									               
						</fo:block>		
				</fo:table-cell>
				</fo:table-row>
				</xsl:if>
				</xsl:for-each>
				</fo:table-body>
				</fo:table>
				</fo:block>
				</fo:block-container>
				
				
				// total amount blocks
				
				
			<fo:block-container absolute-position="absolute"
				left="100mm" top="134mm" width="58mm" height="3mm">
				
				<fo:block text-align="right"
				font-family="Helvetica" font-size="8pt" font-weight="bold">
						Total Merchandise for this package/Total
						Marchandise pour cet envoie:
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="130mm" top="138mm" width="58mm" height="3mm">
				
				<fo:block text-align="right"
				font-family="Helvetica" font-size="7pt" font-weight="normal">
	              <xsl:variable name="GrandTotal">
								<xsl:for-each	select="//Shipment/ShipmentLines/ShipmentLine">
								<xsl:if test="(@ActualQuantity!='null' and @ActualQuantity > 0 )">
									<xsl:variable name="UnitPrice">
										<xsl:value-of select="OrderLine/LinePriceInfo/@UnitPrice" />
									</xsl:variable>
                                    <xsl:variable name="Tax">
				   					 <xsl:value-of select="OrderLine/LineOverallTotals/@Tax" />
			     			  		</xsl:variable>
									<xsl:variable name="BackroomPickedQuantity">
								   <xsl:value-of select="@BackroomPickedQuantity" />
								  </xsl:variable>
								  <xsl:variable name="ActualQuantity">
								   <xsl:value-of select="@ActualQuantity" />
								  </xsl:variable>


									<xsl:variable name="ChargeAmount">
										<xsl:value-of	select="OrderLine/LineCharges/LineCharge[@ChargeCategory='ECOFEE' and @ChargeName='ECOFEE']/@ChargeAmount" />
									</xsl:variable>
									<xsl:if
										test="(($ChargeAmount != 'null' and $ChargeAmount!='') and ($Tax!='null'))">
										<total>
										 <xsl:if test="$ActualQuantity!='null' and $ActualQuantity > 0">
								  		 <xsl:value-of select="((($ActualQuantity) * ($UnitPrice)) + ($ChargeAmount) + $Tax)" />
								  		 </xsl:if>
								  		 
										</total>
										
									</xsl:if>
									<xsl:if	test="(($ChargeAmount = 'null' or $ChargeAmount ='') and ($Tax!='null'))">
										<total>
										 <xsl:if test="$ActualQuantity!='null' and $ActualQuantity > 0 ">
								  		 <xsl:value-of select="(($ActualQuantity) * ($UnitPrice) + $Tax)" />
								  		 </xsl:if>
								  		
										</total>

									</xsl:if>
									</xsl:if>
								</xsl:for-each>
                                 </xsl:variable>
                                 <xsl:variable name="TotalPrice">
								<xsl:value-of   select="sum(exsl:node-set($GrandTotal)/total)"/>	
								</xsl:variable>	
							  <xsl:value-of
									select="format-number((round($TotalPrice*100) div 100),'#.00')" />			
				</fo:block>		
				</fo:block-container>
				
				
				<fo:block-container absolute-position="absolute"
				left="100mm" top="142mm" width="58mm" height="3mm" font-weight="bold">
				
				<fo:block text-align="right"
				font-family="Helvetica" font-size="8pt" font-weight="bold">
						Shipping/ExpÃ©dition:
                         </fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="130mm" top="142mm" width="58mm" height="3mm">
				
				<fo:block text-align="right"
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						 <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/Order/HeaderCharges/HeaderCharge[@ChargeCategory='SHIPPINGCHARGE' and @ChargeName='SHIPPINGCHARGE']/@ChargeAmount"/>
						</fo:block>		
				</fo:block-container>
				
				
				<fo:block-container absolute-position="absolute"
				left="100mm" top="146mm" width="58mm" height="3mm" font-weight="bold">
				
				<fo:block text-align="right"
				font-family="Helvetica" font-size="8pt" font-weight="bold">
						GST(10242 0296 RT0001)
                       </fo:block>		
				</fo:block-container>
						<fo:block-container absolute-position="absolute"
				left="130mm" top="146mm" width="58mm" height="3mm">
				
				<fo:block text-align="right"
				font-family="Helvetica" font-size="7pt" font-weight="normal">
				
						<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/LineTaxes/LineTax[@TaxName='General Sales and Use Tax']/@Tax"/>
						 
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="100mm" top="150mm" width="58mm" height="3mm" font-weight="bold">
				
				<fo:block text-align="right"
				font-family="Helvetica" font-size="8pt" font-weight="bold">
						Authorization #/NÂ° Autorisation:
                           </fo:block>		
				</fo:block-container>
			<fo:block-container absolute-position="absolute"
				left="130mm" top="150mm" width="58mm" height="3mm">
				
				<fo:block text-align="right"
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/Order/ChargeTransactionDetails/ChargeTransactionDetail/@AuthorizationID"/>
						</fo:block>		
				</fo:block-container>
				
				
				//678+TransactionId Barcode
				<fo:block-container absolute-position="absolute"
				left="130mm" top="160mm" width="60mm" height="30mm">
				<xsl:variable name="TxnId" select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/Extn/@ExtnTransactionID"/>
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<fo:instream-foreign-object>	
                                                    <barcode:barcode
                                                        xmlns:barcode="http://barcode4j.krysalis.org/ns"
                                                        message="678{$TxnId}">
                                                        <barcode:code128>
                                                            <barcode:height>14mm</barcode:height>                                                            
                                                        </barcode:code128>
                                                    </barcode:barcode>
                                                </fo:instream-foreign-object>
						</fo:block>		
				</fo:block-container>	

				// Ship from address
				<fo:block-container absolute-position="absolute"
				left="6mm" top="205mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="bold">
						SHIP FROM ADDRESS / EXPEDITEUR
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="6mm" top="210mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ToAddress/@FirstName"/>
						<fo:inline space-before="2mm" margin-left="2mm">	
						<xsl:value-of select="concat(' ',//Shipment/ToAddress/@LastName)"/>
						</fo:inline>
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="6mm" top="214mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ToAddress/@AddressLine1"/>
						</fo:block>		
				</fo:block-container>		

				<fo:block-container absolute-position="absolute"
				left="6mm" top="217mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ToAddress/@City"/>
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="concat(', ',//Shipment/ToAddress/@State)"/>
						</fo:inline>
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="concat(' ',//Shipment/ToAddress/@ZipCode)"/>	
						</fo:inline>
						</fo:block>		
				</fo:block-container>
				
				// Country
				<fo:block-container absolute-position="absolute"
				left="6mm" top="221mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
				<xsl:value-of select="//Shipment/ToAddress/@Country"/>
						</fo:block>		
				</fo:block-container>	
				
				// Ship to address
				
				<fo:block-container absolute-position="absolute"
				left="6mm" top="232mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="bold">
						SHIP TO ADDRESS / DESTINATAIRE
						</fo:block>		
				</fo:block-container>
				<fo:block-container absolute-position="absolute"
				left="6mm" top="237mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ShipNode/ShipNodePersonInfo/@FirstName"/>
						<fo:inline space-before="2mm" margin-left="2mm">	
						<xsl:value-of select="concat(' ',//Shipment/ShipNode/ShipNodePersonInfo/@LastName)"/>
						</fo:inline>
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="6mm" top="241mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						 <xsl:value-of select="//Shipment/ShipNode/ShipNodePersonInfo/@AddressLine1"/>
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="6mm" top="245mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ShipNode/ShipNodePersonInfo/@City"/>
						 <fo:inline space-before="2mm" margin-left="2mm">
						                 <xsl:value-of select="concat(', ',//Shipment/ShipNode/ShipNodePersonInfo/@State)"/>
						                    </fo:inline>
											 <fo:inline space-before="2mm" margin-left="2mm">
						                  <xsl:value-of select="concat(' ',//Shipment/ShipNode/ShipNodePersonInfo/@ZipCode)"/>
						                       </fo:inline>
						</fo:block>		
				</fo:block-container>		

				<fo:block-container absolute-position="absolute"
				left="6mm" top="249mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ToAddress/@Country"/>
						</fo:block>		
				</fo:block-container>
				
				// Barcode for order no
				
				<fo:block-container absolute-position="absolute"
				left="60mm" top="247mm" width="50mm" height="20mm">
				<xsl:variable name="OrderNo" select="//Shipment/ShipmentLines/ShipmentLine/@OrderNo"/>
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<fo:instream-foreign-object>	
                                                    <barcode:barcode
                                                        xmlns:barcode="http://barcode4j.krysalis.org/ns"
                                                        message="{$OrderNo}">
                                                        <barcode:code128>
                                                            <barcode:height>14mm</barcode:height>
                                                        </barcode:code128>
                                                    </barcode:barcode>
                                                </fo:instream-foreign-object>
						</fo:block>		
				</fo:block-container>	
				
				</fo:flow>
			
			</fo:page-sequence>
		</xsl:if>
		<xsl:if test="/MultiApi/API[@Name='getSortedShipmentDetails']/Output/Shipment/@EnterpriseCode[.='LT']">
			<fo:page-sequence master-reference="LT-page">
			
			
                <fo:flow flow-name="xsl-region-body">
			
				<fo:block-container absolute-position="absolute"
				left="16mm" top="28mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/BillToAddress/@FirstName"/>
						<fo:inline space-before="2mm" margin-left="2mm">	
						<xsl:value-of select="concat(' ',//Shipment/BillToAddress/@LastName)"/>
						</fo:inline>
				</fo:block>
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="16mm" top="32mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/BillToAddress/@AddressLine1"/>
						</fo:block>
						
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="16mm" top="36mm" width="50mm" height="4mm">
				
				<fo:block  
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/BillToAddress/@City"/>
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="concat(', ',//Shipment/BillToAddress/@State)"/>
						</fo:inline>
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="concat(' ',//Shipment/BillToAddress/@ZipCode)"/>	
						</fo:inline>
						</fo:block>
						
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="16mm" top="40mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/BillToAddress/@Country"/>
						</fo:block>		
				</fo:block-container>
				
              <fo:block-container absolute-position="absolute"
				left="72mm" top="28mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ShipNode/ShipNodePersonInfo/@FirstName"/>
						<fo:inline space-before="2mm" margin-left="2mm">	
						<xsl:value-of select="concat(' ',//Shipment/ShipNode/ShipNodePersonInfo/@LastName)"/>
						</fo:inline>
						</fo:block>
						
				</fo:block-container>				
				
				<fo:block-container absolute-position="absolute"
				left="72mm" top="32mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ShipNode/ShipNodePersonInfo/@AddressLine1"/>
                                  </fo:block>
						
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="72mm" top="36mm" width="50mm" height="4mm">
				
				<fo:block  
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ShipNode/ShipNodePersonInfo/@City"/>
						
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="concat(', ',//Shipment/ShipNode/ShipNodePersonInfo/@State)"/>
						</fo:inline>
						
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="concat(' ',//Shipment/ShipNode/ShipNodePersonInfo/@ZipCode)"/>
						</fo:inline>
						</fo:block>
						
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="72mm" top="40mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ToAddress/@Country"/>
						</fo:block>		
				</fo:block-container>
				<fo:block-container absolute-position="absolute"
				left="157mm" top="21mm" width="40mm" height="4mm">
				
				<fo:block
				font-family="Helvetica" font-size="8pt" font-weight="normal" >
						<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/@OrderNo"/>
						</fo:block>
						
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="160mm" top="25mm" width="40mm" height="4mm">
				
				<fo:block  
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						 <xsl:value-of select="//Shipment/ToAddress/@EMailID"/>
						</fo:block>
						
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="160mm" top="29mm" width="40mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						 <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/Order/@OrderDate"/>
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="160mm" top="33mm" width="40mm" height="4mm">
				
				<fo:block
				font-family="Helvetica" font-size="7pt" font-weight="normal">
				
				  <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/Extn/@ExtnTransactionID"/>
						
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="160mm" top="37mm" width="40mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/@SCAC"/>
						</fo:block>		
				</fo:block-container>
								
				<fo:block-container absolute-position="absolute"
				left="4mm" top="58mm" width="25mm" height="4mm">
				
				<fo:block
				font-family="Helvetica" font-size="7pt" font-weight="normal">					
						<fo:table >
                        <fo:table-column column-width="3.4cm" />
                        <fo:table-column column-width="4.6cm" />
						<fo:table-column column-width="2.5cm" />
                        <fo:table-column column-width="1.7cm" />
						<fo:table-column column-width="1.2cm" />
						<fo:table-column column-width="1.7cm" />
                        <fo:table-column column-width="1.8cm" />
						<fo:table-column column-width="1.6cm" />
						<fo:table-column column-width="2.1cm"/>
                        <fo:table-body>
						<xsl:for-each select="//Shipment/ShipmentLines/ShipmentLine">
						<xsl:if test="(@ActualQuantity!='null' and @ActualQuantity > 0 )">
						
                            <fo:table-row>
							 <fo:table-cell >
                                    <fo:block  text-align="center" font-family="Helvetica" font-size="7pt" font-weight="normal">
									<xsl:value-of select="OrderLine/ItemDetails/ItemAliasList/ItemAlias/@AliasValue"/>
									</fo:block>
                             </fo:table-cell>
								
                                <fo:table-cell>
                                    <fo:block  text-align="center" font-family="Helvetica" font-size="7pt" font-weight="normal">
									<xsl:value-of select="OrderLine/ItemDetails/PrimaryInformation/@ShortDescription"/>
									</fo:block>
                                </fo:table-cell>

                                <fo:table-cell>
                                    <fo:block  text-align="center" font-family="Helvetica" font-size="7pt" font-weight="normal"> 
							<xsl:value-of select="OrderLine/ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Color']/@Value" />

									</fo:block>
                                </fo:table-cell>
								
								<fo:table-cell >
                                    <fo:block text-align="left" font-family="Helvetica" font-size="7pt" font-weight="normal">
									
							<xsl:value-of select="OrderLine/ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Size']/@Value" />

									</fo:block>
                                </fo:table-cell>

                                <fo:table-cell>
                                    <fo:block  text-align="left" font-family="Helvetica" font-size="7pt" font-weight="normal">
									<xsl:choose>	
									 <xsl:when test="@ActualQuantity!='null' and @ActualQuantity > 0">
									 <xsl:value-of select="@ActualQuantity" />
									 </xsl:when>
									<xsl:otherwise>
									<xsl:value-of select="@BackroomPickedQuantity" />
									</xsl:otherwise>
									</xsl:choose>
									</fo:block>
                                </fo:table-cell>
								
								 <fo:table-cell >
                                    <fo:block  text-align="center" font-family="Helvetica" font-size="7pt" font-weight="normal">
										  
									                  <xsl:variable name="UnitPrice">
										              <xsl:value-of select="OrderLine/LinePriceInfo/@UnitPrice" />
									                  </xsl:variable>
													     <xsl:variable name="ChargePerUnit">
										              <xsl:value-of select="OrderLine/LineCharges/LineCharge[@ChargeCategory='DISCOUNT' and @ChargeName='DISCOUNT']/@ChargePerUnit" />
									                  </xsl:variable>
									                  <xsl:if test="$ChargePerUnit != 'null' and $ChargePerUnit!=''">
													    <xsl:variable name="Price">
													       <xsl:value-of select="(($UnitPrice) -($ChargePerUnit))" />
									                       </xsl:variable>
									                          <xsl:value-of select="$Price"/>
														   </xsl:if>
													   	 <xsl:if test="$ChargePerUnit = 'null' or $ChargePerUnit =''">
															 <xsl:variable name="Price">
															  <xsl:value-of select="OrderLine/LinePriceInfo/@UnitPrice" />
									                          </xsl:variable>
									                       <xsl:value-of select="$Price"/>
													 </xsl:if>
									</fo:block>
                                </fo:table-cell>

                                <fo:table-cell >
                                    <fo:block  text-align="center" font-family="Helvetica" font-size="7pt" font-weight="normal">
									<xsl:value-of select="OrderLine/@GiftFlag"/>
									</fo:block>
                                </fo:table-cell>
								
								<fo:table-cell >
                                    <fo:block  text-align="center" font-family="Helvetica" font-size="7pt" font-weight="normal">
									<xsl:value-of select="OrderLine/@GiftWrap"/>
									</fo:block>
                                </fo:table-cell>
                                  <fo:table-cell >
                                    <fo:block text-align="center" font-family="Helvetica" font-size="7pt" font-weight="normal">
									<xsl:variable name="ActualQuantity">
									 <xsl:value-of select="@ActualQuantity" />
									 </xsl:variable>
									  <xsl:variable name="UnitPrice">
									    <xsl:value-of select="OrderLine/LinePriceInfo/@UnitPrice" />
									    </xsl:variable>
									     <xsl:variable name="tax">
					 					 <xsl:value-of select="OrderLine/LineOverallTotals/@Tax"/>
					 					 </xsl:variable>
												
									     <xsl:variable name="ChargePerUnit">
										  <xsl:value-of select="OrderLine/LineCharges/LineCharge[@ChargeCategory='DISCOUNT' and @ChargeName='DISCOUNT']/@ChargePerUnit" />
									       </xsl:variable>
										  <xsl:variable name="TotalPrice">
										    <xsl:if test="$ChargePerUnit != 'null' and $ChargePerUnit!='' and  $tax !='null' ">
										  <xsl:value-of select="(($ActualQuantity) * (($UnitPrice) - $ChargePerUnit)) + $tax +0.00" />
										  </xsl:if>
										    <xsl:if test="($ChargePerUnit = 'null' or $ChargePerUnit='' and ($tax !='null'))">
										  <xsl:value-of select="(($ActualQuantity) * ($UnitPrice) + $tax +0.00)" />
										  </xsl:if>
									      </xsl:variable>
										  <xsl:value-of select="format-number((round($TotalPrice*100) div 100),'#.00')"/>
													  
									                   
									
									
									</fo:block>
                                </fo:table-cell>
								
                            </fo:table-row>
                            </xsl:if>
							</xsl:for-each>
                        </fo:table-body>
                    </fo:table>
			    </fo:block>		
			</fo:block-container>
					
				// total amount blocks
				
				
				<fo:block-container absolute-position="absolute"
				left="186mm" top="117mm" width="58mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
				<xsl:variable name="GrandTotal">

				<xsl:for-each	select="//Shipment/ShipmentLines/ShipmentLine">
				<xsl:if test="(@ActualQuantity!='null' and @ActualQuantity > 0 )">
				<xsl:variable 	name="BackroomPickedQuantity">
				<xsl:value-of 	select="@BackroomPickedQuantity" />
				</xsl:variable>
				<xsl:variable 	name="ActualQuantity">
				<xsl:value-of 	select="@ActualQuantity" />
				</xsl:variable>
				<xsl:variable	name="UnitPrice">
				<xsl:value-of	select="OrderLine/LinePriceInfo/@UnitPrice" />
				</xsl:variable>
				 <xsl:variable name="tax">
				 <xsl:value-of select="OrderLine/LineOverallTotals/@Tax"/>
				</xsl:variable>
				<xsl:variable name="ChargePerUnit">
				 <xsl:value-of select="OrderLine/LineCharges/LineCharge[@ChargeCategory='DISCOUNT' and @ChargeName='DISCOUNT']/@ChargePerUnit" />
				 </xsl:variable>
				<total>
				 <xsl:if test="$ActualQuantity!='null' and $ActualQuantity > 0">
				 
				   <xsl:if test="$ChargePerUnit != 'null' and $ChargePerUnit!='' and $tax!='null' ">
				  <xsl:value-of select="(($ActualQuantity) * (($UnitPrice) - $ChargePerUnit) + $tax)" />
			     </xsl:if>
			     
			      <xsl:if test="(($ChargePerUnit = 'null' or $ChargePerUnit='') and ($tax!='null'))">
				  <xsl:value-of select="(($ActualQuantity) * ($UnitPrice) + $tax)" />
			     </xsl:if>
			      
				 </xsl:if>
				
				
				</total>
				</xsl:if>
				</xsl:for-each>
				</xsl:variable>
				<xsl:variable name="TotalPrice">
				<xsl:value-of select="sum(exsl:node-set($GrandTotal)/total)" />
				</xsl:variable>
				<xsl:value-of select="format-number((round($TotalPrice*100) div 100),'#.00')" />
				</fo:block>		
				</fo:block-container>
	             <fo:block-container absolute-position="absolute"
				left="186mm" top="121mm" width="58mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						 <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/Order/HeaderCharges/HeaderCharge[@ChargeCategory='SHIPPINGCHARGE' and @ChargeName='SHIPPINGCHARGE']/@ChargeAmount"/>
						</fo:block>		
				</fo:block-container>
				
				
				
				<fo:block-container absolute-position="absolute"
				left="186mm" top="125mm" width="58mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
				
				<xsl:variable name="TotalTax">

				<xsl:for-each	select="//Shipment/ShipmentLines/ShipmentLine">
				<xsl:if test="(@ActualQuantity!='null' and @ActualQuantity > 0 )">
			    <xsl:variable 	name="ActualQuantity">
				<xsl:value-of 	select="@ActualQuantity" />
				</xsl:variable>
				
				 <xsl:variable name="tax">
				 <xsl:value-of select="OrderLine/LineOverallTotals/@Tax"/>
				</xsl:variable>
				
				<tax>
				 <xsl:if test="$ActualQuantity!='null' and $ActualQuantity > 0">
				 
				   <xsl:if test="$tax!='null' ">
				  <xsl:value-of select="format-number(($tax),'#.00')" />
			     </xsl:if>
			     			      
				 </xsl:if>
						
				</tax>
				</xsl:if>
				</xsl:for-each>
				</xsl:variable>
				<xsl:variable name="TotaltaxPrice">
				<xsl:value-of select="sum(exsl:node-set($TotalTax)/tax)" />
				</xsl:variable>
				<xsl:value-of select="format-number((round($TotaltaxPrice*100) div 100),'#.00')" />
				</fo:block>		
				</fo:block-container>
					
				<fo:block-container absolute-position="absolute"
				left="186mm" top="129mm" width="58mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
							<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/Order/ChargeTransactionDetails/ChargeTransactionDetail/@AuthorizationID"/>
						</fo:block>		
				</fo:block-container>
				
				//Barcode
				<fo:block-container absolute-position="absolute"
				left="160mm" top="143mm" width="40mm" height="4mm" >
				<xsl:variable name="TransID" select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/Extn/@ExtnTransactionID"/>
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<fo:instream-foreign-object>	
                        	<barcode:barcode xmlns:barcode="http://barcode4j.krysalis.org/ns" message="678{$TransID}">
                            	<barcode:code128>
                                	<barcode:height>12mm</barcode:height>
                                </barcode:code128>
                            </barcode:barcode>
                         </fo:instream-foreign-object>
				</fo:block>		
				</fo:block-container>			
				
				</fo:flow>
			</fo:page-sequence>
		</xsl:if>
					
		</fo:root>
	
	</xsl:when>
	<xsl:otherwise>
	
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
		
            <fo:layout-master-set>
                <fo:simple-page-master master-name="Bay-page-master" page-height="29.7cm" page-width="21.0cm" >
					<fo:region-body margin="6mm"/>
				</fo:simple-page-master>
				
				 <fo:simple-page-master master-name="LT-page-master" page-height="35.6cm" page-width="21.6cm" >				
					<fo:region-body margin="4mm"/>						
				</fo:simple-page-master>
				
				<fo:page-sequence-master master-name="Bay-page">
					<fo:repeatable-page-master-reference master-reference="Bay-page-master"/> 
				</fo:page-sequence-master>
				
				<fo:page-sequence-master master-name="LT-page">
					<fo:repeatable-page-master-reference master-reference="LT-page-master"/>
				</fo:page-sequence-master>
            </fo:layout-master-set>	
            <xsl:if test="/MultiApi/API[@Name='getSortedShipmentDetails']/Output/Shipment/@EnterpriseCode[.='BAY']">
            <fo:page-sequence master-reference="Bay-page">
			
                <fo:flow flow-name="xsl-region-body">
			
				
				// customer Address blocks
				
				// first name , last name
				<fo:block-container absolute-position="absolute"
				left="6mm" top="36mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ToAddress/@FirstName"/>
						<fo:inline space-before="2mm" margin-left="2mm">	
                   <xsl:value-of select="concat(' ',//Shipment/ToAddress/@LastName)"/>
						</fo:inline>
				</fo:block>
				</fo:block-container>
				
				// address line 1
				
				<fo:block-container absolute-position="absolute"
				left="6mm" top="40mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ToAddress/@AddressLine1"/>
				</fo:block>
						
				</fo:block-container>
			
				// city, state, zipcode
				<fo:block-container absolute-position="absolute"
				left="6mm" top="44mm" width="50mm" height="4mm">
				
				<fo:block  
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ToAddress/@City"/>
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="concat(', ',//Shipment/ToAddress/@State)"/>
						</fo:inline>
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="concat(' ',//Shipment/ToAddress/@ZipCode)"/>	
						</fo:inline>
						</fo:block>
						
				</fo:block-container>
				
				// country
				<fo:block-container absolute-position="absolute"
				left="6mm" top="48mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ToAddress/@Country"/>
						</fo:block>		
				</fo:block-container>
				
				
				//Store Address blocks				
				
				//First name , last name
				<fo:block-container absolute-position="absolute"
				left="72mm" top="36mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
				
						<xsl:value-of select="//Shipment/ShipNode/ShipNodePersonInfo/@FirstName"/>
						<fo:inline space-before="2mm" margin-left="2mm">
                         <xsl:value-of select="concat(' ',//Shipment/ShipNode/ShipNodePersonInfo/@LastName)"/>
						</fo:inline>
						</fo:block>
						
				</fo:block-container>
					
				//address line 1
				<fo:block-container absolute-position="absolute"
				left="72mm" top="40mm" width="50mm" height="4mm">
				
				<fo:block  
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ShipNode/ShipNodePersonInfo/@AddressLine1"/>
						</fo:block>
						
				</fo:block-container>
				
				
				//city, state, zipcode
				<fo:block-container absolute-position="absolute"
				left="72mm" top="44mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ShipNode/ShipNodePersonInfo/@City"/>
						
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="concat(', ',//Shipment/ShipNode/ShipNodePersonInfo/@State)"/>
                           </fo:inline>
						
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="concat(' ',//Shipment/ShipNode/ShipNodePersonInfo/@ZipCode)"/>
						</fo:inline>
						</fo:block>		
				</fo:block-container>
				
				//country
				<fo:block-container absolute-position="absolute"
				left="72mm" top="48mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ToAddress/@Country"/>
						</fo:block>		
				</fo:block-container>
								
				
				
				//Order and shipment info

								
				
				<fo:block-container absolute-position="absolute"
				left="172mm" top="30mm" width="60mm" height="4mm">
				<fo:block
				font-family="Helvetica" font-size="8pt" font-weight="normal"> 
						<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/@OrderNo"/>
				</fo:block>			
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="145mm" top="36mm" width="60mm" height="4mm">
				<fo:block  
				font-family="Helvetica" font-size="7pt" font-weight="normal">
				Customer #/N de client:
						<xsl:value-of select="//Shipment/ToAddress/@EMailID"/>
				</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="145mm" top="42mm" width="60mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
					Order Date/Date da le commande: 
						 <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/Order/@OrderDate"/>
				</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="145mm" top="48mm" width="60mm" height="4mm">
				 
				<fo:block
				font-family="Helvetica" font-size="7pt" font-weight="normal">
				T/N:
				  <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/Extn/@ExtnTransactionID"/>
						
						</fo:block>		
				</fo:block-container>
                      <fo:block-container absolute-position="absolute"
				left="145mm" top="52mm" width="60mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
				
					<xsl:value-of select="//Shipment/@SCAC"/>
						</fo:block>		
				</fo:block-container>
				
				//Item details table				
				
				//1 UPC
				<fo:block-container absolute-position="absolute"
				left="2mm" top="70mm" width="25mm" height="4mm">
				
				<fo:block
				font-family="Helvetica" font-size="7pt" font-weight="normal">					
					<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/ItemDetails/ItemAliasList/ItemAlias/@AliasValue"/>
					</fo:block>
						
				</fo:block-container>
				
				//2 Desc
				<fo:block-container absolute-position="absolute"
				left="29mm" top="70mm" width="40mm" height="4mm">
				
				<fo:block   
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/ItemDetails/PrimaryInformation/@ShortDescription"/>
					</fo:block>
						
				</fo:block-container>
				
				//3 color
				<fo:block-container absolute-position="absolute"
				left="79mm" top="70mm" width="24mm" height="4mm">
				
				<fo:block
				font-family="Helvetica" font-size="7pt" font-weight="normal">
							<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Color']/@Value"/>
					</fo:block>		
				</fo:block-container>
				
				//4 size
				<fo:block-container absolute-position="absolute"
				left="103mm" top="70mm" width="22mm" height="4mm">
				
				<fo:block
				font-family="Helvetica" font-size="7pt" font-weight="normal">
					  <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Size']/@Value"/>
						</fo:block>		
				</fo:block-container>
				
				//5 qty
				<fo:block-container absolute-position="absolute"
				left="127mm" top="70mm" width="9mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:choose>	
 <xsl:when test="//Shipment/@Status = '1400.10' and  //Shipment/ShipmentLines/ShipmentLine/@ActualQuantity!='' and //Shipment/ShipmentLines/ShipmentLine/@ActualQuantity!='null'">
 <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/@ActualQuantity" />
						 </xsl:when>
						 <xsl:otherwise>
<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/@BackroomPickedQuantity" />
</xsl:otherwise>
</xsl:choose>
			
						</fo:block>		
				</fo:block-container>
									
				//6 Price
					 <xsl:variable name="UnitPrice">
										              <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/LinePriceInfo/@UnitPrice" />
			       </xsl:variable>
				<fo:block-container absolute-position="absolute"
				left="138mm" top="70mm" width="18mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
			   
				    <xsl:variable name="ChargePerUnit">
										              <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/LineCharges/LineCharge[@ChargeCategory='DISCOUNT' and @ChargeName='DISCOUNT']/@ChargePerUnit" />
				     </xsl:variable>
				      <xsl:if test="$ChargePerUnit != 'null' and $ChargePerUnit!=''">
														             <xsl:value-of select="(round(($UnitPrice) -($ChargePerUnit))*100 div 100)" />
				       </xsl:if>
					 <xsl:if test="$ChargePerUnit = 'null' or $ChargePerUnit =''">
													                   <xsl:value-of select="(round($UnitPrice)*100 div 100) " />
				      </xsl:if>
						</fo:block>		
				</fo:block-container>
				
				//7 Gift Box
				<fo:block-container absolute-position="absolute"
				left="154mm" top="70mm" width="10mm" height="4mm">
				
				<fo:block
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/@GiftFlag"/>
					</fo:block>		
				</fo:block-container>
				
				//8 Gift box price
				<fo:block-container absolute-position="absolute"
				left="166mm" top="70mm" width="15mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/@GiftWrap"/>
					</fo:block>		
				</fo:block-container>
				
				//9 EHF
				<fo:block-container absolute-position="absolute"
				left="175mm" top="70mm" width="8mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						 <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/LineCharges/LineCharge[@ChargeCategory='ECOFEE' and @ChargeName='ECOFEE']/@ChargeAmount"/>
				</fo:block>		
				</fo:block-container>	
				
				//10 total price
				<fo:block-container absolute-position="absolute"
				left="190mm" top="70mm" width="20mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:variable name="BackroomPickedQuantity">
										          <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/@BackroomPickedQuantity" />
												  
			     			  </xsl:variable>
														    <xsl:variable name="ChargeAmount">
										        <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/LineCharges/LineCharge[@ChargeCategory='ECOFEE' and @ChargeName='ECOFEE']/@ChargeAmount"/>
						  
							    </xsl:variable>
														  <xsl:if test="$ChargeAmount != 'null' and $ChargeAmount!=''">
								  <xsl:variable name="TotalPrice">
								   
										                                 <xsl:value-of select="((($BackroomPickedQuantity) * ($UnitPrice)) + ($ChargeAmount))" />
								     </xsl:variable>
								      <xsl:value-of select="(round($TotalPrice*100) div 100)"/>
								        </xsl:if>
																	  	 <xsl:if test="$ChargeAmount = 'null' or $ChargeAmount =''">
										<xsl:variable name="TotalPrice">
																	   <xsl:value-of select="(($BackroomPickedQuantity) * ($UnitPrice))" />
									      </xsl:variable>
									                                  <xsl:value-of select="(round($TotalPrice*100) div 100)"/>
										</xsl:if>
														  
									               
						</fo:block>		
				</fo:block-container>
				
				
				// total amount blocks
				
				
			<fo:block-container absolute-position="absolute"
				left="100mm" top="134mm" width="58mm" height="3mm">
				
				<fo:block text-align="right"
				font-family="Helvetica" font-size="8pt" font-weight="bold">
						Total Merchandise for this package/Total
						Marchandise pour cet envoie:
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="130mm" top="138mm" width="58mm" height="3mm">
				
				<fo:block text-align="right"
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						
									<xsl:variable name="BackroomPickedQuantity">
										          <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/@BackroomPickedQuantity" />
												  
								  </xsl:variable>


									<xsl:variable name="ChargeAmount">
										              <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/LineCharges/LineCharge[@ChargeCategory='ECOFEE' and @ChargeName='ECOFEE']/@ChargeAmount" />
									</xsl:variable>
									                    <xsl:if test="$ChargeAmount != 'null' and $ChargeAmount!=''">
														         <xsl:variable name="TotalPrice">
								  		 
										                                 <xsl:value-of select="((($UnitPrice) * ($BackroomPickedQuantity)) + ($ChargeAmount))" />
									                                 </xsl:variable>
									                                  <xsl:value-of select="(round($TotalPrice*100) div 100)"/>
									</xsl:if>
																	 <xsl:if test="$ChargeAmount = 'null' or $ChargeAmount =''">
                                 <xsl:variable name="TotalPrice">
																	   <xsl:value-of select="(($UnitPrice) * ($BackroomPickedQuantity))" />
								</xsl:variable>	
									                                  <xsl:value-of select="(round($TotalPrice*100) div 100)"/>
																	 </xsl:if>
				</fo:block>		
				</fo:block-container>
				
				
				<fo:block-container absolute-position="absolute"
				left="100mm" top="142mm" width="58mm" height="3mm" font-weight="bold">
				
				<fo:block text-align="right"
				font-family="Helvetica" font-size="8pt" font-weight="bold">
						Shipping/ExpÃ©dition:
                         </fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="130mm" top="142mm" width="58mm" height="3mm">
				
				<fo:block text-align="right"
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						 <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/Order/HeaderCharges/HeaderCharge[@ChargeCategory='SHIPPINGCHARGE' and @ChargeName='SHIPPINGCHARGE']/@ChargeAmount"/>
						</fo:block>		
				</fo:block-container>
				
				
				<fo:block-container absolute-position="absolute"
				left="100mm" top="146mm" width="58mm" height="3mm" font-weight="bold">
				
				<fo:block text-align="right"
				font-family="Helvetica" font-size="8pt" font-weight="bold">
						GST(10242 0296 RT0001)
                       </fo:block>		
				</fo:block-container>
						<fo:block-container absolute-position="absolute"
				left="130mm" top="146mm" width="58mm" height="3mm">
				
				<fo:block text-align="right"
				font-family="Helvetica" font-size="7pt" font-weight="normal">
				
						<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/LineTaxes/LineTax[@TaxName='General Sales and Use Tax']/@Tax"/>
						 
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="100mm" top="150mm" width="58mm" height="3mm" font-weight="bold">
				
				<fo:block text-align="right"
				font-family="Helvetica" font-size="8pt" font-weight="bold">
						Authorization #/NÂ° Autorisation:
                           </fo:block>		
				</fo:block-container>
			<fo:block-container absolute-position="absolute"
				left="130mm" top="150mm" width="58mm" height="3mm">
				
				<fo:block text-align="right"
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/Order/ChargeTransactionDetails/ChargeTransactionDetail/@AuthorizationID"/>
						</fo:block>		
				</fo:block-container>
				
				
				//678+TransactionId Barcode
				<fo:block-container absolute-position="absolute"
				left="130mm" top="175mm" width="60mm" height="30mm">
				<xsl:variable name="TxnId" select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/Extn/@ExtnTransactionID"/>
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<fo:instream-foreign-object>	
                                                    <barcode:barcode
                                                        xmlns:barcode="http://barcode4j.krysalis.org/ns"
                                                        message="678{$TxnId}">
                                                        <barcode:code128>
                                                            <barcode:height>14mm</barcode:height>                                                            
                                                        </barcode:code128>
                                                    </barcode:barcode>
                                                </fo:instream-foreign-object>
						</fo:block>		
				</fo:block-container>	

				// Ship from address
				<fo:block-container absolute-position="absolute"
				left="6mm" top="205mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="bold">
						SHIP FROM ADDRESS / EXPEDITEUR
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="6mm" top="210mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ToAddress/@FirstName"/>
						<fo:inline space-before="2mm" margin-left="2mm">	
						<xsl:value-of select="concat(' ',//Shipment/ToAddress/@LastName)"/>
						</fo:inline>
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="6mm" top="214mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ToAddress/@AddressLine1"/>
						</fo:block>		
				</fo:block-container>		

				<fo:block-container absolute-position="absolute"
				left="6mm" top="217mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ToAddress/@City"/>
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="concat(', ',//Shipment/ToAddress/@State)"/>
						</fo:inline>
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="concat(' ',//Shipment/ToAddress/@ZipCode)"/>	
						</fo:inline>
						</fo:block>		
				</fo:block-container>
				
				// Country
				<fo:block-container absolute-position="absolute"
				left="6mm" top="221mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
				<xsl:value-of select="//Shipment/ToAddress/@Country"/>
						</fo:block>		
				</fo:block-container>	
				
				// Ship to address
				
				<fo:block-container absolute-position="absolute"
				left="6mm" top="232mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="bold">
						SHIP TO ADDRESS / DESTINATAIRE
						</fo:block>		
				</fo:block-container>
				<fo:block-container absolute-position="absolute"
				left="6mm" top="237mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ShipNode/ShipNodePersonInfo/@FirstName"/>
						<fo:inline space-before="2mm" margin-left="2mm">	
						<xsl:value-of select="concat(' ',//Shipment/ShipNode/ShipNodePersonInfo/@LastName)"/>
						</fo:inline>
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="6mm" top="241mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						 <xsl:value-of select="//Shipment/ShipNode/ShipNodePersonInfo/@AddressLine1"/>
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="6mm" top="245mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ShipNode/ShipNodePersonInfo/@City"/>
						 <fo:inline space-before="2mm" margin-left="2mm">
						                 <xsl:value-of select="concat(', ',//Shipment/ShipNode/ShipNodePersonInfo/@State)"/>
						                    </fo:inline>
											 <fo:inline space-before="2mm" margin-left="2mm">
						                  <xsl:value-of select="concat(' ',//Shipment/ShipNode/ShipNodePersonInfo/@ZipCode)"/>
						                       </fo:inline>
						</fo:block>		
				</fo:block-container>		

				<fo:block-container absolute-position="absolute"
				left="6mm" top="249mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ToAddress/@Country"/>
						</fo:block>		
				</fo:block-container>
				
				// Barcode for order no
				
				<fo:block-container absolute-position="absolute"
				left="60mm" top="247mm" width="50mm" height="20mm">
				<xsl:variable name="OrderNo" select="//Shipment/ShipmentLines/ShipmentLine/@OrderNo"/>
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<fo:instream-foreign-object>	
                                                    <barcode:barcode
                                                        xmlns:barcode="http://barcode4j.krysalis.org/ns"
                                                        message="{$OrderNo}">
                                                        <barcode:code128>
                                                            <barcode:height>14mm</barcode:height>
                                                        </barcode:code128>
                                                    </barcode:barcode>
                                                </fo:instream-foreign-object>
						</fo:block>		
				</fo:block-container>	
				
				</fo:flow>
			
			</fo:page-sequence>
		</xsl:if>
		<xsl:if test="/MultiApi/API[@Name='getSortedShipmentDetails']/Output/Shipment/@EnterpriseCode[.='LT']">
			<fo:page-sequence master-reference="LT-page">
			
			
                <fo:flow flow-name="xsl-region-body">
			
				
				// customer Address blocks
				<fo:block-container absolute-position="absolute"
				left="14mm" top="26mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ToAddress/@FirstName"/>
						<fo:inline space-before="2mm" margin-left="2mm">	
						<xsl:value-of select="concat(' ',//Shipment/ToAddress/@LastName)"/>
						</fo:inline>
				</fo:block>
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="14mm" top="30mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ToAddress/@AddressLine1"/>
						</fo:block>
						
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="14mm" top="34mm" width="50mm" height="4mm">
				
				<fo:block  
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ToAddress/@City"/>
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="concat(', ',//Shipment/ToAddress/@State)"/>
						</fo:inline>
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="concat(' ',//Shipment/ToAddress/@ZipCode)"/>	
						</fo:inline>
						</fo:block>
						
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="14mm" top="38mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ToAddress/@Country"/>
						</fo:block>		
				</fo:block-container>
				
				
				//Store Address blocks	
              <fo:block-container absolute-position="absolute"
				left="72mm" top="26mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ShipNode/ShipNodePersonInfo/@FirstName"/>
						<fo:inline space-before="2mm" margin-left="2mm">	
						<xsl:value-of select="concat(' ',//Shipment/ShipNode/ShipNodePersonInfo/@LastName)"/>
						</fo:inline>
						</fo:block>
						
				</fo:block-container>				
				
				<fo:block-container absolute-position="absolute"
				left="72mm" top="30mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ShipNode/ShipNodePersonInfo/@AddressLine1"/>
                                  </fo:block>
						
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="72mm" top="34mm" width="50mm" height="4mm">
				
				<fo:block  
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ShipNode/ShipNodePersonInfo/@City"/>
						
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="concat(', ',//Shipment/ShipNode/ShipNodePersonInfo/@State)"/>
						</fo:inline>
						
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="concat(' ',//Shipment/ShipNode/ShipNodePersonInfo/@ZipCode)"/>
						</fo:inline>
						</fo:block>
						
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="72mm" top="38mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/ToAddress/@Country"/>
						</fo:block>		
				</fo:block-container>
				
				
				
				//Order and shipment info				
				
				<fo:block-container absolute-position="absolute"
				left="160mm" top="22mm" width="40mm" height="4mm">
				
				<fo:block
				font-family="Helvetica" font-size="8pt" font-weight="normal" >
						<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/@OrderNo"/>
						</fo:block>
						
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="160mm" top="26mm" width="40mm" height="4mm">
				
				<fo:block  
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						 <xsl:value-of select="//Shipment/ToAddress/@EMailID"/>
						</fo:block>
						
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="160mm" top="30mm" width="40mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						 <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/Order/@OrderDate"/>
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="160mm" top="34mm" width="40mm" height="4mm">
				
				<fo:block
				font-family="Helvetica" font-size="7pt" font-weight="normal">
				
				  <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/Extn/@ExtnTransactionID"/>
						
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="160mm" top="38mm" width="40mm" height="4mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<xsl:value-of select="//Shipment/@SCAC"/>
						</fo:block>		
				</fo:block-container>
								
				/Item details table				
				
				<fo:block-container absolute-position="absolute"
				left="4mm" top="58mm" width="25mm" height="4mm">
				
				<fo:block
				font-family="Helvetica" font-size="7pt" font-weight="normal">					
						<fo:table >
                        <fo:table-column column-width="3.4cm" />
                        <fo:table-column column-width="4.6cm" />
						<fo:table-column column-width="2.5cm" />
                        <fo:table-column column-width="1.7cm" />
						<fo:table-column column-width="1.2cm" />
						<fo:table-column column-width="1.7cm" />
                        <fo:table-column column-width="1.8cm" />
						<fo:table-column column-width="1.6cm" />
						<fo:table-column column-width="2.1cm"/>
                        <fo:table-body>
						<xsl:for-each select="//Shipment/ShipmentLines/ShipmentLine">
                            <fo:table-row>
							 <fo:table-cell >
                                    <fo:block  text-align="center" font-family="Helvetica" font-size="7pt" font-weight="normal">
									<xsl:value-of select="OrderLine/ItemDetails/ItemAliasList/ItemAlias/@AliasValue"/>
									</fo:block>
                             </fo:table-cell>
								
                                <fo:table-cell>
                                    <fo:block  text-align="center" font-family="Helvetica" font-size="7pt" font-weight="normal">
									<xsl:value-of select="OrderLine/ItemDetails/PrimaryInformation/@ShortDescription"/>
									</fo:block>
                                </fo:table-cell>

                                <fo:table-cell>
                                    <fo:block  text-align="center" font-family="Helvetica" font-size="7pt" font-weight="normal"> 
							<xsl:value-of select="OrderLine/ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Color']/@Value" />

									</fo:block>
                                </fo:table-cell>
								
								<fo:table-cell >
                                    <fo:block text-align="center" font-family="Helvetica" font-size="7pt" font-weight="normal">
									
							<xsl:value-of select="OrderLine/ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Size']/@Value" />

									</fo:block>
                                </fo:table-cell>

                                <fo:table-cell>
                                    <fo:block  text-align="center" font-family="Helvetica" font-size="7pt" font-weight="normal">
									<xsl:choose>	
									 <xsl:when test="//Shipment/ShipmentLines/ShipmentLine/OrderLine/ItemDetails/PrimaryInformation/@Status = '1400.10' and  //Shipment/ShipmentLines/ShipmentLine/@ActualQuantity!='' and //Shipment/ShipmentLines/ShipmentLine/@ActualQuantity!='null'">
									 <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/@ActualQuantity" />
									 </xsl:when>
									<xsl:otherwise>
									<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/@BackroomPickedQuantity" />
									</xsl:otherwise>
									</xsl:choose>
									</fo:block>
                                </fo:table-cell>
								
								 <fo:table-cell >
                                    <fo:block  text-align="center" font-family="Helvetica" font-size="7pt" font-weight="normal">
										  
									                  <xsl:variable name="UnitPrice">
										              <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/LinePriceInfo/@UnitPrice" />
									                  </xsl:variable>
													     <xsl:variable name="ChargePerUnit">
										              <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/LineCharges/LineCharge[@ChargeCategory='DISCOUNT' and @ChargeName='DISCOUNT']/@ChargePerUnit" />
									                  </xsl:variable>
									                  <xsl:if test="$ChargePerUnit != 'null' and $ChargePerUnit!=''">
													    <xsl:variable name="Price">
														 
													       <xsl:value-of select="(($UnitPrice) -($ChargePerUnit))" />
									                       </xsl:variable>
									                                   <xsl:value-of select="round($Price*100) div 100"/>
																	   
														   </xsl:if>
																	   
													   	 <xsl:if test="$ChargePerUnit = 'null' or $ChargePerUnit =''">
															 <xsl:variable name="Price">
																	   <xsl:value-of select="($UnitPrice) " />
									                          </xsl:variable>
									                                  <xsl:value-of select="round($Price*100) div 100"/>
													 </xsl:if>
									</fo:block>
                                </fo:table-cell>

                                <fo:table-cell >
                                    <fo:block  text-align="center" font-family="Helvetica" font-size="7pt" font-weight="normal">
									<xsl:value-of select="OrderLine/@GiftFlag"/>
									</fo:block>
                                </fo:table-cell>
								
								<fo:table-cell >
                                    <fo:block  text-align="center" font-family="Helvetica" font-size="7pt" font-weight="normal">
									<xsl:value-of select="OrderLine/@GiftWrap"/>
									</fo:block>
                                </fo:table-cell>
                                  <fo:table-cell >
                                    <fo:block text-align="center" font-family="Helvetica" font-size="7pt" font-weight="normal">
									<xsl:variable name="BackroomPickedQuantity">
										          <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/@BackroomPickedQuantity" />
												  
									 </xsl:variable>
														  
									  <xsl:variable name="UnitPrice">
										              <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/LinePriceInfo/@UnitPrice" />
					 					 </xsl:variable>
									     <xsl:variable name="ChargePerUnit">
										              <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/LineCharges/LineCharge[@ChargeCategory='DISCOUNT' and @ChargeName='DISCOUNT']/@ChargePerUnit" />
									       </xsl:variable>
										  <xsl:variable name="TotalPrice">
																	   <xsl:value-of select="(($BackroomPickedQuantity) * ($UnitPrice))" />
									      </xsl:variable>
																	   <xsl:value-of select="(round($TotalPrice*100) div 100)"/>
													  
									                   
									
									
									</fo:block>
                                </fo:table-cell>
								
                            </fo:table-row>
							</xsl:for-each>
                        </fo:table-body>
                    </fo:table>
			    </fo:block>		
			</fo:block-container>
					
				// total amount blocks
				
				
				<fo:block-container absolute-position="absolute"
				left="186mm" top="118mm" width="58mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
				 <xsl:variable name="TotalPrice">
																	   <xsl:value-of select="0" />
									                                 </xsl:variable>
				
   	                                <xsl:for-each select="//Shipment/ShipmentLines/ShipmentLine">


				<xsl:variable 	name="BackroomPickedQuantity">
										          <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/@BackroomPickedQuantity" />
												  
				</xsl:variable>
														  
				<xsl:variable	name="UnitPrice">
										              <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/LinePriceInfo/@UnitPrice" />
				</xsl:variable>
				 
			     
			      
																	 
																	  <xsl:value-of select="((($UnitPrice) * ($BackroomPickedQuantity)) + ($TotalPrice))" />
				
				
				</xsl:for-each>
																	  <xsl:value-of select="(round($TotalPrice*100) div 100)"/>
																	 
																	
				</fo:block>		
				</fo:block-container>
	             <fo:block-container absolute-position="absolute"
				left="186mm" top="122mm" width="58mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						 <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/Order/HeaderCharges/HeaderCharge[@ChargeCategory='SHIPPINGCHARGE' and @ChargeName='SHIPPINGCHARGE']/@ChargeAmount"/>
						</fo:block>		
				</fo:block-container>
				
				
				
				<fo:block-container absolute-position="absolute"
				left="186mm" top="126mm" width="58mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
					  <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/LineOverallTotals/@Tax"/>
				</fo:block>		
				</fo:block-container>
					
				<fo:block-container absolute-position="absolute"
				left="186mm" top="130mm" width="58mm" height="3mm">
				
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
							<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/Order/ChargeTransactionDetails/ChargeTransactionDetail/@AuthorizationID"/>
						</fo:block>		
				</fo:block-container>
				
				//Barcode
				<fo:block-container absolute-position="absolute"
				left="160mm" top="143mm" width="40mm" height="4mm" >
				<xsl:variable name="TransID" select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/Extn/@ExtnTransactionID"/>
				<fo:block 
				font-family="Helvetica" font-size="7pt" font-weight="normal">
						<fo:instream-foreign-object>	
                        	<barcode:barcode xmlns:barcode="http://barcode4j.krysalis.org/ns" message="678{$TransID}">
                            	<barcode:code128>
                                	<barcode:height>12mm</barcode:height>
                                </barcode:code128>
                            </barcode:barcode>
                         </fo:instream-foreign-object>
				</fo:block>		
				</fo:block-container>			
				
				</fo:flow>
			</fo:page-sequence>
		</xsl:if>
					
		</fo:root>
	
	</xsl:otherwise>
	</xsl:choose>
	</xsl:template>
	</xsl:stylesheet>
