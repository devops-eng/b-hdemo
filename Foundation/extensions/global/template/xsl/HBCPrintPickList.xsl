<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<MultiApi>
			<API Name="getSortedShipmentDetails">
				<Input>
					<Shipment>
						<xsl:choose>
							<xsl:when test="//@ShipmentKey">
								<xsl:attribute name="ShipmentKey">
									<xsl:value-of select="//@ShipmentKey"/>
								</xsl:attribute>
							</xsl:when>
							<xsl:otherwise>
								<xsl:attribute name="ShipmentNo">
									<xsl:value-of select="//@ShipmentNo"/>
								</xsl:attribute>
								<xsl:attribute name="SellerOrganizationCode">
									<xsl:value-of select="//@SellerOrganizationCode"/>
								</xsl:attribute>
								<xsl:attribute name="ShipNode">
									<xsl:value-of select="//@ShipNode"/>
								</xsl:attribute>
							</xsl:otherwise>
						</xsl:choose>
						<!-- R3 - Modified to fix item description issue -->
						<xsl:attribute name="DisplayLocalizedFieldInLocale">
							<xsl:value-of select="//@DisplayLocalizedFieldInLocale"/>
						</xsl:attribute>
					</Shipment>
				</Input>
			</API>
			<API Name="changeShipment">
				<Input>
					<Shipment>
						<xsl:attribute name="OverrideModificationRules">
							<xsl:value-of select="'Y'"/>
						</xsl:attribute>
						<xsl:attribute name="PickTicketPrinted">
							<xsl:value-of select="'Y'"/>
						</xsl:attribute>
						<xsl:choose>
							<xsl:when test="//@ShipmentKey">
								<xsl:attribute name="ShipmentKey">
									<xsl:value-of select="//@ShipmentKey"/>
								</xsl:attribute>
							</xsl:when>
							<xsl:otherwise>
								<xsl:attribute name="ShipmentNo">
									<xsl:value-of select="//@ShipmentNo"/>
								</xsl:attribute>
								<xsl:attribute name="SellerOrganizationCode">
									<xsl:value-of select="//@SellerOrganizationCode"/>
								</xsl:attribute>
								<xsl:attribute name="ShipNode">
									<xsl:value-of select="//@ShipNode"/>
								</xsl:attribute>
								<xsl:attribute name="EnterpriseCode">
									<xsl:value-of select="//@EnterpriseCode"/>
								</xsl:attribute>
							</xsl:otherwise>
						</xsl:choose>
						<Extn>
							<xsl:attribute name="ExtnFulfillingAssociate">
								<xsl:value-of select="//@Loginid"/>
							</xsl:attribute>
						</Extn>
					</Shipment>
				</Input>
			</API>
			<API Name="getItemUOMMasterList">
				<Input>
					<ItemUOMMaster>
						<xsl:attribute name="EnterpriseCode">
							<xsl:value-of select="//@EnterpriseCode"/>
						</xsl:attribute>
					</ItemUOMMaster>
				</Input>
			</API>
			<API Name="getUserHierarchy">
				<Input>
					<User>
						<xsl:attribute name="Loginid">
							<xsl:value-of select="//@Loginid"/>
						</xsl:attribute>
					</User>
				</Input>
			</API>
		<!-- Defect Fix 232: Start -->
			<API Name="getCommonCodeList">
				<Input>
					<CommonCode>
						<xsl:attribute name="CodeType">
							<xsl:value-of select="'PICK_TICKET_CARRIER'"/>
						</xsl:attribute>
						<xsl:attribute name="OrganizationCode">
							<xsl:value-of select="//@EnterpriseCode"/>
						</xsl:attribute>
					</CommonCode>
				</Input>
			</API>
			<!-- Defect Fix 232: End -->
		</MultiApi>
	</xsl:template>
</xsl:stylesheet>
