<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<Order>
			<xsl:copy-of select="Order/@*"/>
			<OrderLines>
				<xsl:for-each select="Order/OrderLines/OrderLine">
					<OrderLine>
						<xsl:copy-of select="@*"/>
						<xsl:copy-of select="LinePriceInfo"/>
						<xsl:copy-of select="Extn"/>
					</OrderLine>
				</xsl:for-each>
			</OrderLines>
			<PaymentMethods>
				<xsl:for-each select="Order/PaymentMethods/PaymentMethod">
					<PaymentMethod>
						<xsl:copy-of select="@*"/>
						<PaymentDetailsList>
							<PaymentDetails>
								<xsl:attribute name="ChargeType">
									<xsl:value-of select="'AUTHORIZATION'"/>
								</xsl:attribute>
								<xsl:attribute name="ProcessedAmount">
									<xsl:value-of select="'-100'"/>
								</xsl:attribute>
								<xsl:attribute name="RequestAmount">
									<xsl:value-of select="'-100'"/>
								</xsl:attribute>
							</PaymentDetails>
						</PaymentDetailsList>
					</PaymentMethod>
				</xsl:for-each>
			</PaymentMethods>
		</Order>
	</xsl:template>
</xsl:stylesheet>