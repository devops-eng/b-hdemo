<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
		<InvoiceDetail>
			<InvoiceHeader>
				<xsl:copy-of select="InvoiceDetail/InvoiceHeader/@*"/>
				<xsl:variable name="DocType" select="InvoiceDetail/InvoiceHeader/Order/@DocumentType"/>
				<xsl:variable name="EntryType" select="InvoiceDetail/InvoiceHeader/Order/@EntryType"/>
				<xsl:variable name="PaymentType" select="InvoiceDetail/InvoiceHeader/CollectionDetails/CollectionDetail/PaymentMethod/@PaymentType"/>
				<xsl:variable name="CreditCardType" select="InvoiceDetail/InvoiceHeader/CollectionDetails/CollectionDetail/PaymentMethod/@CreditCardType"/>

				<Order>

					<xsl:copy-of select="InvoiceDetail/InvoiceHeader/Order/@*"/>
					<xsl:choose>
						<xsl:when test="$DocType = '0003'">
							<xsl:attribute name="EnteredBy">
								<xsl:value-of select="InvoiceDetail/InvoiceHeader/LineDetails/LineDetail/OrderLine/DerivedFromOrderLine/Order/@EnteredBy"/>
							</xsl:attribute>
							<xsl:attribute name="EntryType">
								<xsl:value-of select="InvoiceDetail/InvoiceHeader/LineDetails/LineDetail/OrderLine/DerivedFromOrderLine/Order/@EntryType"/>
							</xsl:attribute>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="EnteredBy">
								<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/@EnteredBy"/>
							</xsl:attribute>
							<xsl:attribute name="EntryType">
								<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/@EntryType"/>
							</xsl:attribute>
						</xsl:otherwise>
					</xsl:choose>
					<Extn>
						<xsl:copy-of select="InvoiceDetail/InvoiceHeader/Order/Extn/@*"/>
					</Extn>
					<PersonInfoBillTo>
						<xsl:copy-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@*"/>
					</PersonInfoBillTo>
					<PersonInfoShipTo>
						<xsl:copy-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoShipTo/@*"/>
					</PersonInfoShipTo>
					<Promotions>
								<xsl:for-each select="InvoiceDetail/InvoiceHeader/Order/Promotions/Promotion">
									<Promotion>
										<xsl:copy-of select="@*"/>
									</Promotion>
								</xsl:for-each>
							</Promotions>
					<xsl:copy-of select="InvoiceDetail/InvoiceHeader/Order/PaymentMethods"/>
					<xsl:copy-of select="InvoiceDetail/InvoiceHeader/Order/ChargeTransactionDetails"/>
					<xsl:copy-of select="InvoiceDetail/InvoiceHeader/Order/OrderLines"/>
					<xsl:copy-of select="InvoiceDetail/InvoiceHeader/Order/SalesOrderLines"/>
				</Order>
				<Shipment>
					<xsl:copy-of select="InvoiceDetail/InvoiceHeader/Shipment/@*"/>
					<Extn>
						<xsl:copy-of select="InvoiceDetail/InvoiceHeader/Shipment/Extn/@*"/>
						<HbcJccItemCaseList>
							<xsl:for-each select="InvoiceDetail/InvoiceHeader/Shipment/Extn/HbcJccItemCaseList/HbcJccItemCase">
								<HbcJccItemCase>
									<xsl:copy-of select="@*" /> 
								</HbcJccItemCase>
							</xsl:for-each>
						</HbcJccItemCaseList>	
					</Extn>
					<ToAddress>
						<xsl:choose>
							<xsl:when test="$DocType = '0003'">
								<xsl:copy-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@*"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:copy-of select="InvoiceDetail/InvoiceHeader/Shipment/ToAddress/@*"/>
							</xsl:otherwise>
						</xsl:choose>
					</ToAddress>
					<ShipNode>
						<xsl:copy-of select="InvoiceDetail/InvoiceHeader/Shipment/ShipNode/@*"/>
					</ShipNode>
					<ShipmentLines>
						<xsl:for-each select="InvoiceDetail/InvoiceHeader/Shipment/ShipmentLines/ShipmentLine">
							<ShipmentLine>
								<xsl:copy-of select="@*"/>
								<xsl:copy-of select="OrderRelease"/>
								<xsl:variable name="OrderLineKey" select="@OrderLineKey"/>
								<ShipmentTagSerials>
									<xsl:for-each select="ShipmentTagSerials/ShipmentTagSerial">
										<ShipmentTagSerial>
											<xsl:copy-of select="@SerialNo"/>
											<xsl:variable name="CheckSerialNo" select="@SerialNo"/>
											
											<ActivationResponse>
												<xsl:for-each select="/InvoiceDetail/InvoiceHeader/LineDetails/LineDetail/OrderLine[@OrderLineKey=$OrderLineKey]">

													<xsl:attribute name="ApprovalCode">
														<xsl:for-each select="Notes/Note[@ReasonCode='APPROVAL_CODE']">
															<xsl:variable name="NSerialNo" select="substring-before(@NoteText,'_')"/>

															<xsl:if test="$NSerialNo=$CheckSerialNo">

																<xsl:value-of select="substring-after(@NoteText,'_')"/>
															</xsl:if>
														</xsl:for-each>
													</xsl:attribute>

													<xsl:attribute name="ResponseDate">
														<xsl:for-each select="Notes/Note[@ReasonCode='RESPONSE_DATE']">
															<xsl:variable name="NSerialNo" select="substring-before(@NoteText,'_')"/>

															<xsl:if test="$NSerialNo=$CheckSerialNo">
																<xsl:value-of select="substring-after(@NoteText,'_')"/>
															</xsl:if>
														</xsl:for-each>
													</xsl:attribute>
													<xsl:attribute name="RegisterNo">
														<xsl:for-each select="Notes/Note[@ReasonCode='REGISTER_NO']">
															<xsl:variable name="NSerialNo" select="substring-before(@NoteText,'_')"/>

															<xsl:if test="$NSerialNo=$CheckSerialNo">
																<xsl:value-of select="substring-after(@NoteText,'_')"/>
															</xsl:if>
														</xsl:for-each>
													</xsl:attribute>
													<xsl:attribute name="TransactionNo">
														<xsl:for-each select="Notes/Note[@ReasonCode='TRANSACTION_NO']">
															<xsl:variable name="NSerialNo" select="substring-before(@NoteText,'_')"/>

															<xsl:if test="$NSerialNo=$CheckSerialNo">
																<xsl:value-of select="substring-after(@NoteText,'_')"/>
															</xsl:if>
														</xsl:for-each>
													</xsl:attribute>
												</xsl:for-each>
											</ActivationResponse>
										</ShipmentTagSerial>
									</xsl:for-each>
								</ShipmentTagSerials>
							</ShipmentLine>
						</xsl:for-each>
					</ShipmentLines>
				</Shipment>
				<LineDetails>
					<xsl:for-each select="InvoiceDetail/InvoiceHeader/LineDetails/LineDetail">
						<LineDetail>
							<xsl:copy-of select="@*"/>
							<LineDetailTranQuantity>
								<xsl:copy-of select="LineDetailTranQuantity/@ShippedQty"/>
							</LineDetailTranQuantity>
							<OrderLine>

								<xsl:choose>
									<xsl:when test="(../../Shipment/ShipNode/@NodeType = 'VENDOR')">
										<xsl:attribute name="FulfillmentType">DROPSHIP</xsl:attribute>
									</xsl:when>
									<xsl:when test="(OrderLine/@FulfillmentType = 'FT_PICKUP')">
										<xsl:attribute name="FulfillmentType">PICKUP</xsl:attribute>
									</xsl:when>
									<xsl:when test="(OrderLine/@DeliveryMethod = 'PICK')">
										<xsl:attribute name="FulfillmentType">PICKUP</xsl:attribute>
									</xsl:when>
									<xsl:otherwise>
										<xsl:attribute name="FulfillmentType">SHIP</xsl:attribute>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:copy-of select="OrderLine/@*[name()!='FulfillmentType']"/>
								<Item>
									<xsl:copy-of select="OrderLine/Item/@*"/>
								</Item>
								<ItemDetails>
									<xsl:copy-of select="OrderLine/ItemDetails/Extn"/>
									<xsl:copy-of select="OrderLine/ItemDetails/PrimaryInformation"/>
									<xsl:copy-of select="OrderLine/ItemDetails/AdditionalAttributeList"/>
								</ItemDetails>
								<Extn>

									<xsl:choose>
										<xsl:when test="OrderLine/Extn/@ExtnRegistrantAddrsIndictr = 'Y'">
											<xsl:attribute name="ExtnRegistrantAddrsIndictr">Y</xsl:attribute>
										</xsl:when>
										<xsl:otherwise>
											<xsl:attribute name="ExtnRegistrantAddrsIndictr">N</xsl:attribute>
										</xsl:otherwise>
									</xsl:choose>

									<xsl:choose>
										<xsl:when test="OrderLine/Extn/@ExtnPackSlpPrcSuppress = 'Y'">
											<xsl:attribute name="ExtnPackSlpPrcSuppress">Y</xsl:attribute>
										</xsl:when>
										<xsl:otherwise>
											<xsl:attribute name="ExtnPackSlpPrcSuppress">N</xsl:attribute>
										</xsl:otherwise>
									</xsl:choose>

									<xsl:choose>
										<xsl:when test="$DocType = '0003'">
											<xsl:attribute name="ExtnTransactionID">
												<xsl:value-of select="OrderLine/Extn/@ExtnTransactionID"/>
											</xsl:attribute>
										</xsl:when>
									</xsl:choose>

									<xsl:attribute name="ExtnPOOrderNo">
										<xsl:value-of select="OrderLine/Extn/@ExtnPOOrderNo"/>
									</xsl:attribute>

									<xsl:copy-of select="OrderLine/Extn/@*"/>
								</Extn>
								<Instructions>
									<xsl:for-each select="OrderLine/Instructions/Instruction">
										<Instruction>
											<xsl:copy-of select="@*"/>
										</Instruction>
									</xsl:for-each>
								</Instructions>
								<Notes>
									<xsl:for-each select="OrderLine/Notes/Note">
										<Note>
											<xsl:copy-of select="@*"/>
										</Note>
									</xsl:for-each>
								</Notes>
								<LinePriceInfo>
									<xsl:copy-of select="OrderLine/LinePriceInfo/@*"/>
								</LinePriceInfo>
								<LineOverallTotals>
									<xsl:copy-of select="OrderLine/LineOverallTotals/@*"/>
								</LineOverallTotals>
								<Promotions>
								<xsl:for-each select="OrderLine/Promotions/Promotion">
									<Promotion>
										<xsl:copy-of select="@*"/>
									</Promotion>
								</xsl:for-each>
							</Promotions>
							</OrderLine>
							<LineCharges>
								<xsl:for-each select="LineCharges/LineCharge">
									<LineCharge>
										<xsl:copy-of select="@*"/>
									</LineCharge>
								</xsl:for-each>
							</LineCharges>
							<LineTaxes>
								<xsl:for-each select="LineTaxes/LineTax">
									<LineTax>
										<xsl:copy-of select="@*"/>
									</LineTax>
								</xsl:for-each>
							</LineTaxes>
							<Promotions>
								<xsl:for-each select="Promotions/Promotion">
									<Promotion>
										<xsl:copy-of select="@*"/>
									</Promotion>
								</xsl:for-each>
							</Promotions>
						</LineDetail>
					</xsl:for-each>
				</LineDetails>
				<HeaderCharges>
					<xsl:for-each select="InvoiceDetail/InvoiceHeader/HeaderCharges/HeaderCharge">
						<HeaderCharge>
							<xsl:copy-of select="@*"/>
						</HeaderCharge>
					</xsl:for-each>
				</HeaderCharges>
				<HeaderTaxes>
					<xsl:for-each select="InvoiceDetail/InvoiceHeader/HeaderTaxes/HeaderTax">
						<HeaderTax>
							<xsl:copy-of select="@*"/>
						</HeaderTax>
					</xsl:for-each>
				</HeaderTaxes>

				<CollectionDetails>
					<xsl:choose>
						<xsl:when test="/InvoiceDetail/InvoiceHeader/@InvoiceType = 'RETURN' ">
							<xsl:choose>					
								<xsl:when test="/InvoiceDetail/InvoiceHeader/Order[@EnterpriseCode='OFF5' or @EnterpriseCode='SAKS']">
									<xsl:copy-of select="/InvoiceDetail/InvoiceHeader/CollectionDetails/*"/>								
								</xsl:when>
								<xsl:otherwise>
							<xsl:for-each select="InvoiceDetail/InvoiceHeader/CollectionDetails/CollectionDetail[@RetainElement='Y']">
								<CollectionDetail>
									<xsl:copy-of select="@*"/>
									<CreditCardTransactions>
										<CreditCardTransaction>
											<xsl:attribute name="AuthCode">
												<xsl:choose>
													<xsl:when test="$CreditCardType = 'PAYPAL'">
														<xsl:text></xsl:text>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="@AuthorizationID"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:attribute>
											<xsl:copy-of select="CreditCardTransactions/CreditCardTransaction/@*[name()!='AuthorizationID']"/>
										</CreditCardTransaction>
									</CreditCardTransactions>
									<PaymentMethod>
										<xsl:attribute name="CreditCardNo">
											<xsl:choose>
												<xsl:when test="$CreditCardType = 'PAYPAL'">
													<xsl:value-of select="@AuthorizationID"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="PaymentMethod/@CreditCardNo"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:attribute>
										<xsl:attribute name="PaymentReference1">
											<xsl:choose>
												<xsl:when test="$CreditCardType = 'PAYPAL'">
													<xsl:value-of select="PaymentMethod/@PaymentReference1"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="PaymentMethod/@DisplayCreditCardNo"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:attribute>
										<xsl:attribute name="PaymentReference3">
											<xsl:choose>
												<xsl:when test="$EntryType = 'POS'">
													<xsl:value-of select="PaymentMethod/@PaymentReference3"/>
												</xsl:when>

												<xsl:when test="$CreditCardType = 'PAYPAL'">
													<xsl:text></xsl:text>
												</xsl:when>

												<xsl:otherwise>
													<xsl:value-of select="CreditCardTransactions/CreditCardTransaction/@TranReturnMessage"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:attribute>
										<xsl:copy-of select="PaymentMethod/@*[name()!='CreditCardNo' and name()!='PaymentReference3' and name()!='PaymentReference1']"/>

										<PersonInfoBillTo>
											<xsl:copy-of select="PaymentMethod/PersonInfoBillTo/@*"/>
										</PersonInfoBillTo>
									</PaymentMethod>
								</CollectionDetail>
							</xsl:for-each>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise>
							<xsl:for-each select="InvoiceDetail/InvoiceHeader/CollectionDetails/CollectionDetail">
								<CollectionDetail>
									<xsl:copy-of select="@*"/>
									<xsl:choose>
										<xsl:when test="/InvoiceDetail/InvoiceHeader/Order[@EnterpriseCode='OFF5' or @EnterpriseCode='SAKS']">
											<xsl:copy-of select="*"/>
										</xsl:when>
										<xsl:otherwise>
									<CreditCardTransactions>
										<CreditCardTransaction>
											<xsl:attribute name="AuthCode">
												<xsl:choose>
													<xsl:when test="$CreditCardType = 'PAYPAL'">
														<xsl:text></xsl:text>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="@AuthorizationID"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:attribute>
											<xsl:copy-of select="CreditCardTransactions/CreditCardTransaction/@*[name()!='AuthorizationID']"/>
										</CreditCardTransaction>
									</CreditCardTransactions>
									<PaymentMethod>
										<xsl:attribute name="CreditCardNo">
											<xsl:choose>
												<xsl:when test="$CreditCardType = 'PAYPAL'">
													<xsl:value-of select="@AuthorizationID"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="PaymentMethod/@CreditCardNo"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:attribute>
										<xsl:attribute name="PaymentReference1">
											<xsl:choose>
												<xsl:when test="$CreditCardType = 'PAYPAL'">
													<xsl:value-of select="PaymentMethod/@PaymentReference1"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="PaymentMethod/@DisplayCreditCardNo"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:attribute>
										<xsl:attribute name="PaymentReference3">

											<xsl:choose>
												<xsl:when test="$EntryType = 'POS'">

													<xsl:value-of select="PaymentMethod/@PaymentReference3"/>
												</xsl:when>

												<xsl:when test="$CreditCardType = 'PAYPAL'">
													<xsl:text></xsl:text>
												</xsl:when>

												<xsl:otherwise>
													<xsl:value-of select="CreditCardTransactions/CreditCardTransaction/@TranReturnMessage"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:attribute>
										<xsl:copy-of select="PaymentMethod/@*[name()!='CreditCardNo' and name()!='PaymentReference3' and name()!='PaymentReference1']"/>
										<PersonInfoBillTo>

											<xsl:copy-of select="PaymentMethod/PersonInfoBillTo/@*"/>
										</PersonInfoBillTo>
									</PaymentMethod>
										</xsl:otherwise>
									</xsl:choose>
								</CollectionDetail>
							</xsl:for-each>
						</xsl:otherwise>
					</xsl:choose>
				</CollectionDetails>
				<Extn>
					<TransferFromList>
						<xsl:for-each select="InvoiceDetail/InvoiceHeader/Extn/TransferFromList/TransferFrom">
							<xsl:value-of select="."/>
							<TransferFrom>
								<xsl:copy-of select="@*"/>
							</TransferFrom>
						</xsl:for-each>
					</TransferFromList>
				</Extn>
			</InvoiceHeader>
		</InvoiceDetail>
	</xsl:template>
</xsl:stylesheet>