<?xml version="1.0" encoding="UTF-8"?>
<!-- Input XML
<GetRewardsInfo DisplayLocalizedFieldInLocale="en_US_EST" IgnoreOrdering="Y">
    <sRewardsInfoIn>
        <RewardsInfo CCType="0" EstimatedPoints="0" OrderSubtotal="0"
            RewardsNumber="921041175" TotalPointsToDate="0" xmlns="eretailwebservices.hbc.com" />
    </sRewardsInfoIn>
</GetRewardsInfo>
-->
<!-- This XSLT removes xmlns namespace attribute from given input xml which is mandatory to include logic required in next/following XSLT -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="xsl">
	<xsl:output indent="yes"/>
	<xsl:template match="*">   
		<xsl:element name="{local-name()}">   
			<xsl:apply-templates select="@* | node()"/>   
		</xsl:element>   
	</xsl:template>   
	<xsl:template match="@* | text()">   
		<xsl:copy/>   
	</xsl:template>   
</xsl:stylesheet>

<!--Output XML
<GetRewardsInfo DisplayLocalizedFieldInLocale="en_US_EST" IgnoreOrdering="Y">
    <sRewardsInfoIn>
        <RewardsInfo CCType="0" EstimatedPoints="0" OrderSubtotal="0" RewardsNumber="921041175" TotalPointsToDate="0"/>
    </sRewardsInfoIn>
</GetRewardsInfo>
-->