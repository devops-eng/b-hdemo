<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
		<OrderInvoice>
		    <xsl:copy-of select="InvoiceDetail/InvoiceHeader/LineDetails/LineDetail/OrderLine/DerivedFromOrder/@*"/>
			<xsl:copy-of select="/InvoiceDetail/InvoiceHeader/@*"/>
			<xsl:copy-of select="/InvoiceDetail/InvoiceHeader/*[name()!='HeaderCharges' and name()!='LineDetails' and name()!='HeaderTaxes']"/>
			<DerivedFromOrder>
			<xsl:copy-of select="InvoiceDetail/InvoiceHeader/LineDetails/LineDetail/OrderLine/DerivedFromOrder/@*"/>
			<xsl:copy-of select="InvoiceDetail/InvoiceHeader/LineDetails/LineDetail/OrderLine/DerivedFromOrder/*"/>
			</DerivedFromOrder>
			<HeaderChargeList>
				<xsl:copy-of select="/InvoiceDetail/InvoiceHeader/HeaderCharges/*"/>
			</HeaderChargeList>
			<TaxBreakupList>
				<xsl:for-each select="/InvoiceDetail/InvoiceHeader/HeaderTaxes/HeaderTax">
					<TaxBreakup>
						<xsl:copy-of select="./@*"/>
					</TaxBreakup>
				</xsl:for-each>
			</TaxBreakupList>
			<LineDetails>
				<xsl:for-each select="/InvoiceDetail/InvoiceHeader/LineDetails/LineDetail">
					<LineDetail>
						<xsl:copy-of select="./@*"/>
						<xsl:copy-of select="./*[name()!='LineCharges' and name()!='LineTaxes' and name()!='OrderLine'] "/>
						<OrderLine>
						<xsl:copy-of select="./OrderLine/@*"/>
						<xsl:copy-of select="./OrderLine/*[name()!='DerivedFromOrder']"/>
						</OrderLine>
						<LineChargeList>
							<xsl:copy-of select="./LineCharges/*"/>
						</LineChargeList>
						<LineTaxList>
							<xsl:copy-of select="./LineTaxes/*"/>
						</LineTaxList>
					</LineDetail>
				</xsl:for-each>
			</LineDetails>
		</OrderInvoice>
	</xsl:template>
</xsl:stylesheet>