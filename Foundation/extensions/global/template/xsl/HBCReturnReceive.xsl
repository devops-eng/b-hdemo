<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
		<OrderStatusChange>
			<xsl:attribute name="OrderHeaderKey">
				<xsl:value-of select="Order/@OrderHeaderKey"/>
			</xsl:attribute>
			<xsl:attribute name="BaseDropStatus">
				<xsl:value-of select="'3900'"/>
			</xsl:attribute>
                        <xsl:attribute name="TransactionId">
				<xsl:value-of select="'RECEIVE_ORDER.0003'"/>
			</xsl:attribute>
			
		</OrderStatusChange>	
	</xsl:template>
</xsl:stylesheet>
