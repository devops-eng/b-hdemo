<?xml version="1.0" encoding="UTF-8"?>
  <!-- Modified the xsl for defect 591. Added Orderline level Notes -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<Order>
			<xsl:attribute name="Action">
				<xsl:value-of select="'MODIFY'"/>
			</xsl:attribute>
			<xsl:attribute name="OrderHeaderKey">
				<xsl:value-of select="Shipment/ShipmentLines/ShipmentLine/@OrderHeaderKey"/>
			</xsl:attribute>
			<OrderLines>
			<xsl:for-each select="Shipment/ShipmentLines/ShipmentLine">
			  <!-- For handling the shipments with a line short picked and other purged, in such cases update the note only for the line cancelled by Shipment Monitor-->
				<xsl:if test="@Modifyprogid ='SHIPMENT_MONITOR'">
				<OrderLine>
					<xsl:attribute name="Action">
						<xsl:value-of select="'MODIFY'"/>
					</xsl:attribute>
					<xsl:attribute name="OrderLineKey">
						<xsl:value-of select="@OrderLineKey"/>
					</xsl:attribute>
	<Notes>
         <Note>
		    <xsl:attribute name="ReasonCode">
				<xsl:value-of select="'HBC_SC_SM_001'"/>
			</xsl:attribute>
			 <xsl:attribute name="NoteText">
				<xsl:value-of select="'Cancelling the order as no action taken by Store with in targeted duration'"/>
			</xsl:attribute>			
		</Note>
     </Notes>		
				</OrderLine>
				</xsl:if>
				</xsl:for-each>
			</OrderLines>
	</Order>
	</xsl:template>
</xsl:stylesheet>