<?xml version="1.0" encoding="UTF-8"?>
<!--Defect #668 To stamp notes when order is cancelled by VDSPOCancel-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<Order>
	        <xsl:attribute name="Action">
				<xsl:value-of select="'MODIFY'"/>
			</xsl:attribute>
			<xsl:attribute name="OrderHeaderKey">
				<xsl:value-of select="Order/OrderLines/OrderLine/@ChainedFromOrderHeaderKey"/>
			</xsl:attribute>
	<Notes>
         <Note>
		    <xsl:attribute name="ReasonCode">
				<xsl:value-of select="'HBC_SC_VD_001'"/>
			</xsl:attribute>
			 <xsl:attribute name="NoteText">
				<xsl:value-of select="'Cancelling the order as unable to fulfill by Vendor'"/>
			</xsl:attribute>			
		</Note>
     </Notes>					
	</Order>
	</xsl:template>
</xsl:stylesheet>
