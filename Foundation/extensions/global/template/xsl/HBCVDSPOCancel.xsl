<?xml version="1.0" encoding="UTF-8"?>
<!--Defect #668 To stamp notes when order is cancelled by VDSPOCancel-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<Order>
			<xsl:attribute name="Action">
				<xsl:value-of select="OrderUpdates/OrderRelease/@Action"/>
			</xsl:attribute>
			<xsl:attribute name="EnterpriseCode">
				<xsl:value-of select="OrderUpdates/OrderRelease/@EnterpriseCode"/>
			</xsl:attribute>
			<xsl:attribute name="OrderNo">
				<xsl:value-of select="OrderUpdates/OrderRelease/@OrderNo"/>
			</xsl:attribute>
			<xsl:attribute name="DocumentType">
				<xsl:value-of select="OrderUpdates/OrderRelease/@DocumentType"/>
			</xsl:attribute>
	 <Notes>
         <Note>
		    <xsl:attribute name="ReasonCode">
				<xsl:value-of select="'HBC_SC_VD_001'"/>
			</xsl:attribute>
			 <xsl:attribute name="NoteText">
				<xsl:value-of select="'Cancelling the order as unable to fulfill by Vendor'"/>
			</xsl:attribute>			
		</Note>
     </Notes>								
	</Order>
	</xsl:template>
</xsl:stylesheet>
