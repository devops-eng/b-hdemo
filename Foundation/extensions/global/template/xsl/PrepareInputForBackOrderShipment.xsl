<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
 <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
		<Shipment>
			<xsl:attribute name="Action">
				<xsl:value-of select="'MODIFY'"/>
			</xsl:attribute >
			<xsl:attribute name="BackOrderRemovedQuantity">
				<xsl:value-of select="'Y'"/>
			</xsl:attribute >
			<xsl:attribute name="OrderNo">
				<xsl:value-of select="/MonitorConsolidation/Shipment/ShipmentLines/ShipmentLine/Order/@OrderNo"/>
			</xsl:attribute >
			<xsl:attribute name="DocumentType">
				<xsl:value-of select="/MonitorConsolidation/Shipment/@DocumentType"/>
			</xsl:attribute >
			<xsl:attribute name="EnterpriseCode">
				<xsl:value-of select="/MonitorConsolidation/Shipment/ShipmentLines/ShipmentLine/Order/@EnterpriseCode"/>
			</xsl:attribute >
			<xsl:attribute name="ShipNode">
				<xsl:value-of select="/MonitorConsolidation/Shipment/@ShipNode"/>
			</xsl:attribute >
			<xsl:attribute name="ShipmentKey">
				<xsl:value-of select="/MonitorConsolidation/Shipment/@ShipmentKey"/>
			</xsl:attribute >
		
	
	<ShipmentLines>
		<xsl:for-each select="MonitorConsolidation/Shipment/ShipmentLines/ShipmentLine">
			<ShipmentLine>
			<xsl:attribute name="Action">
				<xsl:value-of select="''"/>
			</xsl:attribute >
			<xsl:attribute name="Quantity">
				<xsl:value-of select="0"/>
			</xsl:attribute >
			<xsl:attribute name="OrderNo">
				<xsl:value-of select="/MonitorConsolidation/Shipment/ShipmentLines/ShipmentLine/Order/@OrderNo"/>
			</xsl:attribute >
			<xsl:attribute name="DocumentType">
				<xsl:value-of select="/MonitorConsolidation/Shipment/@DocumentType"/>
			</xsl:attribute >
			<xsl:attribute name="EnterpriseCode">
				<xsl:value-of select="/MonitorConsolidation/Shipment/ShipmentLines/ShipmentLine/Order/@EnterpriseCode"/>
			</xsl:attribute >
			<xsl:attribute name="PrimeLineNo">
				<xsl:value-of select="/MonitorConsolidation/Shipment/ShipmentLines/ShipmentLine/OrderLine/@PrimeLineNo"/>
			</xsl:attribute >
			<xsl:attribute name="SubLineNo">
				<xsl:value-of select="/MonitorConsolidation/Shipment/ShipmentLines/ShipmentLine/OrderLine/@PrimeLineNo"/>
			</xsl:attribute >
			<xsl:attribute name="OrderLineKey">
				<xsl:value-of select="/MonitorConsolidation/Shipment/ShipmentLines/ShipmentLine/OrderLine/@OrderLineKey"/>
			</xsl:attribute >
			<xsl:attribute name="ShipmentLineKey">
				<xsl:value-of select="/MonitorConsolidation/Shipment/ShipmentLines/ShipmentLine/@ShipmentLineKey"/>
			</xsl:attribute >
			</ShipmentLine>
		</xsl:for-each>
	</ShipmentLines>
		</Shipment>
	</xsl:template>
</xsl:stylesheet>