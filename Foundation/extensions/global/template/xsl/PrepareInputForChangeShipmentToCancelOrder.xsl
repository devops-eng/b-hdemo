<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<Shipment>
			<xsl:attribute name="Action">
				<xsl:value-of select="'Cancel'"/>
			</xsl:attribute>
			<xsl:attribute name="SellerOrganizationCode">
				<xsl:value-of select="MonitorConsolidation/Shipment/@SellerOrganizationCode"/>
			</xsl:attribute>
			<xsl:attribute name="ShipmentKey">
				<xsl:value-of select="MonitorConsolidation/Shipment/@ShipmentKey"/>
			</xsl:attribute>
			<xsl:attribute name="CancelRemovedQuantity">
				<xsl:value-of select="'Y'"/>
			</xsl:attribute>
			<xsl:attribute name="SelectMethod">
				<xsl:value-of select="'WAIT'"/>
			</xsl:attribute>			
	</Shipment>
	</xsl:template>
</xsl:stylesheet>