<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<OrderRelease>
			<xsl:copy-of select="OrderRelease/@*"/>
		<xsl:attribute name="OrderType">
				<xsl:value-of select="DROP_SHIP"/>
				</xsl:attribute>
			<xsl:attribute name="DocumentType">
				<xsl:value-of select="OrderRelease/OrderLine/ChainedFromOrderLine/Order/@DocumentType"/>
			</xsl:attribute>
			<xsl:attribute name="SalesOrderNo">
				<xsl:value-of select="OrderRelease/OrderLine/ChainedFromOrderLine/Order/@OrderNo"/>
			</xsl:attribute>
			<xsl:attribute name="OrderHeaderKey">
				<xsl:value-of select="OrderRelease/OrderLine/ChainedFromOrderLine/Order/@OrderHeaderKey"/>
			</xsl:attribute>

			<PackListPriceInfo>
			<xsl:copy-of select="OrderRelease/PackListPriceInfo/@*"/>
			<xsl:attribute name="TotalAmount">
				<xsl:value-of select="format-number(sum(OrderRelease/OrderLine/ChainedFromOrderLine/LinePriceInfo/@LineTotal), '0.00')"/>
			</xsl:attribute>
			</PackListPriceInfo>
			<xsl:copy-of select="OrderRelease/PersonInfoShipTo/."/>
		<xsl:copy-of select="OrderRelease/OrderLine[1]/ChainedFromOrderLine/Order/PersonInfoBillTo/."/>

			<xsl:copy-of select="OrderRelease/OrderLine[1]/ChainedFromOrderLine/Order/."/>
               
			   <Extn>
				<xsl:attribute name="ExtnTransactionID">
					<xsl:value-of select="OrderRelease/Extn/@ExtnTransactionID"/>
                 </xsl:attribute>
				<xsl:attribute name="ExtnTransactionIDBarCode">
					<xsl:value-of select="OrderRelease/Extn/@ExtnTransactionIDBarCode"/>
                 </xsl:attribute>
				 <xsl:attribute name="ExtnReleaseNo">
					<xsl:value-of select="OrderRelease/Extn/@ExtnReleaseNo"/>
                 </xsl:attribute>
			   </Extn>
			   
			<xsl:for-each select="OrderRelease/OrderLine">
            <OrderLine>
				<xsl:copy-of select="ChainedFromOrderLine/@*"/>	
				<xsl:copy-of select="Shipnode/."/>						
				<xsl:copy-of select="Item/."/>
					<xsl:copy-of select="ItemDetails/."/>
				<xsl:copy-of select="Instructions/."/>
				<xsl:copy-of select="ChainedFromOrderLine/LinePriceInfo/."/>               
				<xsl:copy-of select="ChainedFromOrderLine/LineCharges/."/>
				<xsl:copy-of select="ChainedFromOrderLine/LineTaxes/."/>			
				   <Extn>
				    <xsl:attribute name="ExtnWebOrderSuffix">
					   <xsl:value-of select="ChainedFromOrderLine/Extn/@ExtnWebOrderSuffix"/>
										 </xsl:attribute>
                 <xsl:attribute name="ExtnWebLineNumber">
                   <xsl:value-of select="ChainedFromOrderLine/Extn/@ExtnWebLineNumber"/>
                 </xsl:attribute>
					<xsl:attribute name="ExtnIsReturnable">
					   <xsl:value-of select="Extn/@ExtnIsReturnable"/>
					 </xsl:attribute>
					 <xsl:attribute name="ExtnTransactionID">
					   <xsl:value-of select="Extn/@ExtnTransactionID"/>
                 </xsl:attribute>
                 <xsl:attribute name="ExtnColor">
                   <xsl:value-of select="Extn/@ExtnColor"/>
                 </xsl:attribute>
                 <xsl:attribute name="ExtnSize">
                   <xsl:value-of select="Extn/@ExtnSize"/>
                 </xsl:attribute>
				 <xsl:attribute name="ExtnEstimatedShipDate">
                     <xsl:value-of select="ChainedFromOrderLine/Extn/@ExtnEstimatedShipDate"/>
                 </xsl:attribute>
               </Extn>
            <LinePackListPriceInfo>
						 <xsl:copy-of select="ChainedFromOrderLine/LinePriceInfo/@*"/>
             </LinePackListPriceInfo>			
			
             </OrderLine>  
      </xsl:for-each> 	  
	</OrderRelease>
	</xsl:template>
</xsl:stylesheet>