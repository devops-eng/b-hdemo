<?xml version="1.0" encoding="UTF-8"?>
<!-- 
Name : PrepareInputForPickUpMonitor
Use: This XSL is used to remove the Cancelled OrderLines from the 
XML before posting it to the email queue.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <MonitorConsolidation>
      <xsl:copy-of select="MonitorConsolidation/@*"/>
      <Shipment>
        <xsl:copy-of select="MonitorConsolidation/Shipment/@*"/>
        <ShipmentLines>
          <xsl:for-each select="MonitorConsolidation/Shipment/ShipmentLines/ShipmentLine">
          <!-- OrderedQty retrived from Shipmentline/@Quantity rather than OrderLine/@OrderedQty as a fix for Defect 988 -->
            <xsl:variable name="OrderedQuantity" select="@Quantity" />
            <xsl:if test="$OrderedQuantity &gt; 0">
              <ShipmentLine>
                <xsl:copy-of select="@*[not(.='')]|node()"/>
              </ShipmentLine>
            </xsl:if>
          </xsl:for-each>
        </ShipmentLines>
        <MonitorRule>
          <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates/>
          </xsl:copy>
        </MonitorRule>
      </Shipment>
    </MonitorConsolidation>
  </xsl:template>
</xsl:stylesheet>