<?xml version="1.0" encoding="UTF-8"?>

<!-- This file has been added newly as a solution for 
  Defect 242: Orders not shipped per Send Suite are showing Shipment Packed in OMS. 
  This is used to prepare input for getShipmentList API-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <Shipment>
      <xsl:attribute name="ShipmentKey"><xsl:value-of select="Shipment/@ShipmentKey"/></xsl:attribute>
      </Shipment>
  </xsl:template>
</xsl:stylesheet>
