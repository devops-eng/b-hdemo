<?xml version="1.0" encoding="UTF-8"?>
<!-- This file has been added newly as a solution for 
  Defect 242: Orders not shipped per Send Suite are showing Shipment Packed in OMS.
This is used to prepare input to undoBackroomPickAPI  -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <xsl:template match="/">
  <UndoPick>
    <Shipment>
    	<xsl:attribute name="SellerOrganizationCode"><xsl:value-of select="/Shipment/Output/Shipments/Shipment/@EnterpriseCode"/></xsl:attribute>
    	<xsl:attribute name="ShipNode"><xsl:value-of select="Shipment/ShipNode/@ShipNode"/></xsl:attribute>
    	<xsl:attribute name="ShipmentKey"><xsl:value-of select="Shipment/@ShipmentKey"/></xsl:attribute>
    	<xsl:attribute name="ShipmentNo"><xsl:value-of select="Shipment/@ShipmentNo"/></xsl:attribute>
   	<xsl:attribute name="TransactionId"><xsl:value-of select="'YCD_UNDO_BACKROOM_PICK'"/></xsl:attribute>
    	<ShipmentLines>
	 	<xsl:for-each select="/Shipment/Output/Shipments/Shipment/ShipmentLines/ShipmentLine">
		<!--Defect 664 adding a check - sBackroomPickedQuantity should be greater than 0 for performing an undo backroom pick -->
		<xsl:variable name="BackroomPickedQuantity" select="@BackroomPickedQuantity" />
            <xsl:if test="$BackroomPickedQuantity &gt; 0">
			<ShipmentLine>
				<xsl:copy-of select="@*[not(.='')]|node()"/>
				<xsl:apply-templates select="*"/>
			</ShipmentLine>
			</xsl:if>
		</xsl:for-each>
	</ShipmentLines>				
   </Shipment>
  </UndoPick>
 </xsl:template>
</xsl:stylesheet>
