<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">
	<xsl:template match="/">
		<Order>
			<xsl:attribute name="DocumentType">
				<xsl:value-of select="Order/@DocumentType"/>
			</xsl:attribute>
			<xsl:attribute name="EnterpriseCode">
				<xsl:value-of select="Order/@EnterpriseCode"/>
			</xsl:attribute>
			<xsl:attribute name="EntryType">
				<xsl:value-of select="Order/@EntryType"/>
			</xsl:attribute>
			<xsl:attribute name="OrderDate">
				<xsl:value-of select="Order/@OrderDate"/>
			</xsl:attribute>
			<xsl:attribute name="OrderNo">
				<xsl:value-of select="Order/@OrderNo"/>
			</xsl:attribute>
			<xsl:attribute name="OriginalTotalAmount">
				<xsl:value-of select="Order/@OriginalTotalAmount"/>
			</xsl:attribute>
			<PriceInfo>
				<xsl:copy-of select="Order/PriceInfo/@*" />
			</PriceInfo>
			<OrderLines>
				<xsl:for-each select="Order/OrderLines/OrderLine">					
					<OrderLine>
						<xsl:attribute name="CarrierServiceCode">
							<xsl:value-of select="./@CarrierServiceCode"/>
						</xsl:attribute>
						<xsl:attribute name="DeliveryMethod">
							<xsl:value-of select="./@DeliveryMethod"/>
						</xsl:attribute>
						<xsl:attribute name="ItemGroupCode">
							<xsl:value-of select="./@ItemGroupCode"/>
						</xsl:attribute>
						<xsl:attribute name="OrderedQty">
							<xsl:value-of select="./@OrderedQty"/>
						</xsl:attribute>
						<xsl:attribute name="PrimeLineNo">
							<xsl:value-of select="./@PrimeLineNo"/>
						</xsl:attribute>
						<Item>
							<xsl:attribute name="ItemID">
								<xsl:value-of select="./Item/@ItemID"/>
							</xsl:attribute>
							<xsl:attribute name="ItemShortDesc">
								<xsl:value-of select="./Item/@ItemShortDesc"/>
							</xsl:attribute>
							<xsl:attribute name="UnitOfMeasure">
								<xsl:value-of select="./Item/@UnitOfMeasure"/>
							</xsl:attribute>
						</Item>
						<LineOverallTotals>
							<xsl:attribute name="Charges">
								<xsl:value-of select="./LineOverallTotals/@Charges"/>
							</xsl:attribute>
							<xsl:attribute name="Discount">
								<xsl:value-of select="./LineOverallTotals/@Discount"/>
							</xsl:attribute>	
							<xsl:attribute name="ExtendedPrice">
								<xsl:value-of select="./LineOverallTotals/@ExtendedPrice"/>
							</xsl:attribute>
							<xsl:attribute name="LineTotal">
								<xsl:value-of select="./LineOverallTotals/@LineTotal"/>
							</xsl:attribute>
							<xsl:attribute name="OptionPrice">
								<xsl:value-of select="./LineOverallTotals/@OptionPrice"/>
							</xsl:attribute>
							<xsl:attribute name="PricingQty">
								<xsl:value-of select="./LineOverallTotals/@PricingQty"/>
							</xsl:attribute>
							<xsl:attribute name="Tax">
								<xsl:value-of select="./LineOverallTotals/@Tax"/>
							</xsl:attribute>
							<xsl:attribute name="UnitPrice">
								<xsl:value-of select="./LineOverallTotals/@UnitPrice"/>
							</xsl:attribute>
						</LineOverallTotals>
					</OrderLine>					
				</xsl:for-each>
			</OrderLines>
		</Order>
	</xsl:template>
</xsl:stylesheet>