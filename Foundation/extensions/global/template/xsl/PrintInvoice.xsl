<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.1"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fopUtil="com.yantra.pca.ycd.fop.YCDFOPUtils"
			   xmlns:fo="http://www.w3.org/1999/XSL/Format"
                exclude-result-prefixes="fo">
				
				
				<xsl:variable name="locale">
		<xsl:value-of select="/MultiApi/API[@Name='getUserHierarchy']/Output/User/@Localecode" />
	</xsl:variable>
			
    <xsl:template match="/">
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
		
            <fo:layout-master-set>
                <fo:simple-page-master master-name="my-page" page-height="29.7cm" page-width="21cm" >
				
						<fo:region-body />
						
				</fo:simple-page-master>
            </fo:layout-master-set>

	
            <fo:page-sequence master-reference="my-page">
			
                <fo:flow flow-name="xsl-region-body">
				
				
				// customer Address blocks
				
				// first name , last name
				<fo:block-container absolute-position="absolute"
				left="6mm" top="33mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Calibri" font-size="8pt" >
						<xsl:value-of select="//Shipment/ToAddress/@FirstName"/>
						<fo:inline space-before="2mm" margin-left="2mm">	
						<xsl:value-of select="//Shipment/ToAddress/@LastName"/>
						</fo:inline>
				</fo:block>
				</fo:block-container>
				
				// address line 1
				
				<fo:block-container absolute-position="absolute"
				left="6mm" top="37mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Calibri" font-size="8pt" >
						<xsl:value-of select="//Shipment/ToAddress/@AddressLine1"/>
				</fo:block>
						
				</fo:block-container>
				
				// city, state, zipcode
				<fo:block-container absolute-position="absolute"
				left="6mm" top="41mm" width="50mm" height="4mm">
				
				<fo:block  
				font-family="Calibri" font-size="8pt" >
						<xsl:value-of select="//Shipment/ToAddress/@City"/>
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="//Shipment/ToAddress/@State"/>
						</fo:inline>
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="//Shipment/ToAddress/@ZipCode"/>	
						</fo:inline>
						</fo:block>
						
				</fo:block-container>
				
				// country
				<fo:block-container absolute-position="absolute"
				left="6mm" top="45mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Calibri" font-size="9pt">
						<xsl:value-of select="//Shipment/ToAddress/@Country"/>
						</fo:block>		
				</fo:block-container>
				
				
				//Store Address blocks				
				
				//First name , last name
				<fo:block-container absolute-position="absolute"
				left="72mm" top="33mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Calibri" font-size="8pt" >
						<xsl:value-of select="//Shipment/ToAddress/@FirstName"/>
						<fo:inline space-before="2mm" margin-left="2mm">	
						<xsl:value-of select="//Shipment/ToAddress/@LastName"/>
						</fo:inline>
						</fo:block>
						
				</fo:block-container>
				
				//address line 1
				<fo:block-container absolute-position="absolute"
				left="72mm" top="37mm" width="50mm" height="4mm">
				
				<fo:block  
				font-family="Calibri" font-size="8pt" >
						<xsl:value-of select="//Shipment/ToAddress/@AddressLine1"/>
							</fo:block>
				</fo:block-container>
				
				
				//city, state, zipcode
				<fo:block-container absolute-position="absolute"
				left="72mm" top="41mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Calibri" font-size="8pt">
						<xsl:value-of select="//Shipment/ToAddress/@City"/>
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="//Shipment/ToAddress/@State"/>
						</fo:inline>
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="//Shipment/ToAddress/@ZipCode"/>	
						</fo:inline>
						</fo:block>		
				</fo:block-container>
				
				//country
				<fo:block-container absolute-position="absolute"
				left="72mm" top="45mm" width="50mm" height="4mm">
				
				<fo:block 
				font-family="Calibri" font-size="8pt">
						<xsl:value-of select="//Shipment/ToAddress/@Country"/>
						</fo:block>		
				</fo:block-container>
				
				
				
				//Order and shipment info

								
				
				<fo:block-container absolute-position="absolute"
				left="160mm" top="32mm" width="60mm" height="4mm">
				<fo:block
				font-family="Calibri" font-size="8pt" font-weight="bold"> 
						<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/@OrderNo"/>
				</fo:block>			
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="147mm" top="36mm" width="60mm" height="4mm">
				<fo:block  
				font-family="Calibri" font-size="8pt"  >
				Customer #/N de client:
					 <xsl:value-of select="//Shipment/ToAddress/@EMailID"/>
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="147mm" top="40mm" width="60mm" height="4mm">
				
				<fo:block 
				font-family="Calibri" font-size="8pt">
				Order Date/Date da le commande: 
						 <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/Order/@OrderDate"/>
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="147mm" top="48mm" width="60mm" height="4mm">
				 
				<fo:block
				font-family="Calibri" font-size="8pt">
				T/N:
                             <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/Extn/@ExtnTransactionID"/>
						
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="147mm" top="52mm" width="60mm" height="4mm">
				
				<fo:block 
				font-family="Calibri" font-size="8pt">
						<xsl:value-of select="//Shipment/@SCAC"/>
						</fo:block>		
				</fo:block-container>
				
				//Item details table				
				
				//1 UPC
				<fo:block-container absolute-position="absolute"
				left="8mm" top="70mm" width="25mm" height="4mm">
				
				<fo:block
				font-family="Calibri" font-size="8pt" >
						
						<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/ItemDetails/ItemAliasList/ItemAlias/@AliasValue"/>
						</fo:block>
						
				</fo:block-container>
				
				//2 Desc
				<fo:block-container absolute-position="absolute"
				left="31mm" top="70mm" width="40mm" height="4mm">
				
				<fo:block   
				font-family="Calibri" font-size="8pt" >
						<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/ItemDetails/PrimaryInformation/@ShortDescription"/>
						</fo:block>
						
				</fo:block-container>
				
				//3 color
				<fo:block-container absolute-position="absolute"
				left="81mm" top="70mm" width="24mm" height="4mm">
				
				<fo:block
				font-family="Calibri" font-size="8pt">
						<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Color']/@Value"/>
						</fo:block>		
				</fo:block-container>
				
				//4 size
				<fo:block-container absolute-position="absolute"
				left="99mm" top="70mm" width="22mm" height="4mm">
				
				<fo:block
				font-family="Calibri" font-size="8pt">
						  <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Size']/@Value"/>
						</fo:block>		
				</fo:block-container>
				
				//5 qty
				
				<fo:block-container absolute-position="absolute"
				left="115mm" top="70mm" width="22mm" height="4mm">
				
				<fo:block
				font-family="Calibri" font-size="8pt">
		          <xsl:if test="//Shipment/ShipmentLines/ShipmentLine/@BackroomPickedQuantity > '0.0'">
				  
				
<xsl:variable name="originalQty">
 <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/@Quantity" />
 </xsl:variable>
 
 <xsl:variable name="backroomPickedQty">
                    <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/@BackroomPickedQuantity" />
					</xsl:variable>	
			  
			    <xsl:variable name="pickedQty">
	 <xsl:value-of select="$originalQty - $backroomPickedQty" />
	</xsl:variable>
		<fo:block
				font-family="Calibri" font-size="8pt">
		<xsl:value-of select="$pickedQty" />
		
						</fo:block>	
				  
				  
				    </xsl:if>
					  <xsl:else test="//Shipment/@Status = '1400.10'  and  //Shipment/ShipmentLines/ShipmentLine/@Quantity > '0.0' ">
					
 <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/@Quantity" />
 
					
	</xsl:else>
					
						</fo:block>		
				</fo:block-container>
				
			
				
				//6 Price
				<fo:block-container absolute-position="absolute"
				left="125mm" top="70mm" width="18mm" height="4mm">
				
				<fo:block 
				font-family="Calibri" font-size="8pt">
						<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/LinePriceInfo/@UnitPrice"/>
						</fo:block>		
				</fo:block-container>
				
				//7 Gift Box
				<fo:block-container absolute-position="absolute"
				left="140mm" top="70mm" width="10mm" height="4mm">
				
				<fo:block
				font-family="Calibri" font-size="8pt">
						<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/@GiftFlag"/>
						</fo:block>		
				</fo:block-container>
				
				//8 Gift box price
				<fo:block-container absolute-position="absolute"
				left="150mm" top="70mm" width="15mm" height="4mm">
				
				<fo:block 
				font-family="Calibri" font-size="8pt">
						<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/@GiftWrap"/>
						</fo:block>		
				</fo:block-container>
				
				//9 EHF
				<fo:block-container absolute-position="absolute"
				left="160mm" top="70mm" width="8mm" height="3mm">
				
				<fo:block 
				font-family="Calibri" font-size="8pt">
						 <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/LineCharges/LineCharge[@ChargeCategory='ECOFEE' and @ChargeName='ECOFEE']/@ChargeAmount"/>
						</fo:block>		
				</fo:block-container>
				
				//10 total price
				<fo:block-container absolute-position="absolute"
				left="177mm" top="70mm" width="20mm" height="3mm">
				
				<fo:block 
				font-family="Calibri" font-size="8pt">
						<xsl:variable name="BackroomPickedQuantity">
										          <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/@BackroomPickedQuantity" />
												  
									                    </xsl:variable>
														  
									                  <xsl:variable name="UnitPrice">
										              <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/LinePriceInfo/@UnitPrice" />
									                  </xsl:variable>
													     <xsl:variable name="ChargeAmount">
										              <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/LineCharges/LineCharge[@ChargeCategory='ECOFEE' and @ChargeName='ECOFEE']/@ChargeAmount" />
									                  </xsl:variable>
									                    <xsl:variable name="TotalPrice">
										                 <xsl:value-of select="((($UnitPrice) * ($BackroomPickedQuantity)) + ($ChargeAmount))" />
									                                 </xsl:variable>
									                                  <xsl:value-of select="round($TotalPrice*100) div 100"/>
						</fo:block>		
				</fo:block-container>
				
				
				// total amount blocks
				
				
				<fo:block-container absolute-position="absolute"
				left="106mm" top="140mm" width="58mm" height="3mm">
				
				<fo:block text-align="right"
				font-family="Calibri" font-size="9pt" font-weight="bold">
						Total Merchandise for this package/Total
						Marchandise pour cet envoie:
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="150mm" top="144mm" width="58mm" height="3mm">
				
				<fo:block text-align="right"
				font-family="Calibri" font-size="9pt">
						<xsl:variable name="BackroomPickedQuantity">
										          <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/@BackroomPickedQuantity" />
												  
									                    </xsl:variable>
														  
									                  <xsl:variable name="UnitPrice">
										              <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/LinePriceInfo/@UnitPrice" />
									                  </xsl:variable>
													     <xsl:variable name="ChargeAmount">
										              <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/LineCharges/LineCharge[@ChargeCategory='ECOFEE' and @ChargeName='ECOFEE']/@ChargeAmount" />
									                  </xsl:variable>
									                    <xsl:variable name="TotalPrice">
										                 <xsl:value-of select="((($UnitPrice) * ($BackroomPickedQuantity)) + ($ChargeAmount))" />
									                                 </xsl:variable>
									                                  <xsl:value-of select="round($TotalPrice*100) div 100"/>
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="106mm" top="148mm" width="58mm" height="3mm" font-weight="bold">
				
				<fo:block text-align="right"
				font-family="Calibri" font-size="9pt">
						Shipping/Expédition:
						</fo:block>		
				</fo:block-container>
					<fo:block-container absolute-position="absolute"
				left="150mm" top="148mm" width="58mm" height="3mm">
				
				<fo:block text-align="right"
				font-family="Calibri" font-size="9pt">
						 <xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/Order/HeaderCharges/HeaderCharge[@ChargeCategory='SHIPPINGCHARGE' and @ChargeName='SHIPPINGCHARGE']/@ChargeAmount"/>
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="106mm" top="152mm" width="58mm" height="3mm" font-weight="bold">
				
				<fo:block text-align="right"
				font-family="Calibri" font-size="9pt">
						GST(10242 0296 RT0001)
						</fo:block>		
				</fo:block-container>
				<fo:block-container absolute-position="absolute"
				left="150mm" top="152mm" width="58mm" height="3mm">
				
				<fo:block text-align="right"
				font-family="Calibri" font-size="9pt">
						<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/LineTaxes/LineTax[@TaxName='General Sales and Use Tax']/@Tax"/>
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="106mm" top="156mm" width="58mm" height="3mm" font-weight="bold">
				
				<fo:block text-align="right"
				font-family="Calibri" font-size="9pt">
						Authorization #/N° Autorisation:
						</fo:block>		
				</fo:block-container>
				<fo:block-container absolute-position="absolute"
				left="150mm" top="156mm" width="58mm" height="3mm">
				
				<fo:block text-align="right"
				font-family="Calibri" font-size="9pt">
						<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/Order/ChargeTransactionDetails/ChargeTransactionDetail/@AuthorizationID"/>
						</fo:block>		
				</fo:block-container>
				
				
				//678+TransactionId Barcode
				<fo:block-container absolute-position="absolute"
				left="130mm" top="167mm" width="50mm" height="20mm">
				<xsl:variable name="TxnId" select="//Shipment/ShipmentLines/ShipmentLine/OrderLine/Extn/@ExtnTransactionID"/>
				<fo:block 
				font-family="Calibri" font-size="8pt">
						<fo:instream-foreign-object>	
                                                    <barcode:barcode
                                                        xmlns:barcode="http://barcode4j.krysalis.org/ns"
                                                        message="678{$TxnId}">
                                                        <barcode:code128>
                                                            <barcode:height>14mm</barcode:height>
                                                        </barcode:code128>
                                                    </barcode:barcode>
                                                </fo:instream-foreign-object>
						</fo:block>		
				</fo:block-container>	

				// Ship from address
				<fo:block-container absolute-position="absolute"
				left="6mm" top="232mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Calibri" font-size="8pt">
						SHIP FROM ADDRESS/EXPEDITEUR
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="6mm" top="237mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Calibri" font-size="8pt">
						<xsl:value-of select="//Shipment/ToAddress/@FirstName"/>
						<fo:inline space-before="2mm" margin-left="2mm">	
						<xsl:value-of select="//Shipment/ToAddress/@LastName"/>
						</fo:inline>
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="6mm" top="241mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Calibri" font-size="8pt">
						<xsl:value-of select="//Shipment/ToAddress/@AddressLine1"/>
						</fo:block>		
				</fo:block-container>		

				<fo:block-container absolute-position="absolute"
				left="6mm" top="245mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Calibri" font-size="8pt">
						<xsl:value-of select="//Shipment/ToAddress/@City"/>
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="//Shipment/ToAddress/@State"/>
						</fo:inline>
						<fo:inline space-before="2mm" margin-left="2mm">
						<xsl:value-of select="//Shipment/ToAddress/@ZipCode"/>	
						</fo:inline>
						</fo:block>		
				</fo:block-container>
				
				// Country
				<fo:block-container absolute-position="absolute"
				left="6mm" top="249mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Calibri" font-size="8pt">
				<xsl:value-of select="//Shipment/ToAddress/@Country"/>
						</fo:block>		
				</fo:block-container>	
				
				// Ship to address
				
				<fo:block-container absolute-position="absolute"
				left="6mm" top="260mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Calibri" font-size="8pt">
						SHIP TO ADDRESS/DESTINATAIRE
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="6mm" top="265mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Calibri" font-size="8pt">
						 <xsl:value-of select="//Shipment/ShipNode/ShipNodePersonInfo/@AddressLine1"/>
						</fo:block>		
				</fo:block-container>
				
				<fo:block-container absolute-position="absolute"
				left="6mm" top="269mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Calibri" font-size="8pt">
						<xsl:value-of select="//Shipment/ShipNode/ShipNodePersonInfo/@City"/>
						 <fo:inline space-before="2mm" margin-left="2mm">
						                 <xsl:value-of select="//Shipment/ShipNode/ShipNodePersonInfo/@State"/>
						                    </fo:inline>
											 <fo:inline space-before="2mm" margin-left="2mm">
						                  <xsl:value-of select="//Shipment/ShipNode/ShipNodePersonInfo/@ZipCode"/>
						                       </fo:inline>
						</fo:block>		
				</fo:block-container>		

				<fo:block-container absolute-position="absolute"
				left="6mm" top="273mm" width="70mm" height="3mm">
				
				<fo:block 
				font-family="Calibri" font-size="8pt">
						<xsl:value-of select="//Shipment/ToAddress/@Country"/>
						</fo:block>		
				</fo:block-container>
				
				// Barcode for order no
				
				<fo:block-container absolute-position="absolute"
				left="60mm" top="275mm" width="50mm" height="20mm">
				<xsl:variable name="OrderNo" select="//Shipment/ShipmentLines/ShipmentLine/@OrderNo"/>
				<fo:block 
				font-family="Calibri" font-size="8pt">
						<fo:instream-foreign-object>	
                                                    <barcode:barcode
                                                        xmlns:barcode="http://barcode4j.krysalis.org/ns"
                                                        message="{$OrderNo}">
                                                        <barcode:code128>
                                                            <barcode:height>14mm</barcode:height>
                                                        </barcode:code128>
                                                    </barcode:barcode>
                                                </fo:instream-foreign-object>
						</fo:block>		
				</fo:block-container>	
				
				</fo:flow>
			</fo:page-sequence>
		
		</fo:root>
	</xsl:template>
	</xsl:stylesheet>