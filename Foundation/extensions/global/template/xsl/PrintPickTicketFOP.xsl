<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.1"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
               
				xmlns:java="http://xml.apache.org/xslt/java"
				 xmlns:fopUtil="com.yantra.pca.ycd.fop.YCDFOPUtils"
				xmlns:fo="http://www.w3.org/1999/XSL/Format"
                exclude-result-prefixes="fo">
				<xsl:variable name="locale">
		<xsl:value-of select="/MultiApi/API[@Name='getUserHierarchy']/Output/User/@Localecode" />
	</xsl:variable>
                <xsl:template match="/">
               <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
		
            <fo:layout-master-set>
                <fo:simple-page-master master-name="my-page" page-height="310mm" page-width="220mm" margin-top="-0.5in" margin-bottom="-0.5in" margin-left="-0.75in" 
				margin-right="-0.75in" border-width="2px">
				
						<fo:region-body margin="1in"/>
						
				</fo:simple-page-master>
            </fo:layout-master-set>
			
            <fo:page-sequence master-reference="my-page">
			
                <fo:flow flow-name="xsl-region-body">
				
					<fo:table table-layout="fixed">
						<fo:table-body>
							<fo:table-row block-progression-dimension="20mm">
								<fo:table-cell border-bottom-width="3px" border-bottom-style="hidden" border-bottom-color="black">
									<fo:block font-family="Calibri" font-size="12pt"  text-align="left" font-weight="bold" >
									
									</fo:block>
									
								</fo:table-cell>
								//customer name , delivery method and shipment method
								<fo:table-cell text-align="center" padding-top="3pt" border-bottom-width="1px" border-bottom-style="hidden" border-bottom-color="black">
							         <fo:block font-family="Calibri" font-size="12pt"  text-align="center" font-weight="bold" >
									PICK TICKET
									</fo:block>
								    <fo:block text-align="center" font-size="10pt" padding-top="8pt">
									<xsl:value-of select="//Shipment/ToAddress/@FirstName"/>
									</fo:block>
									<fo:block  left="4cm" top="1cm" font-size="8pt" padding-top="3pt">
									Delivery Method 
									<fo:inline space-before="3mm" margin-left="3mm" font-size="6pt" >
									<xsl:value-of select="//Shipment/@DeliveryMethod"/>
									</fo:inline>
									</fo:block>
									
									<fo:block text-align="center" font-size="8pt" padding-top="3pt">
									Shipping Method  
									<fo:inline space-before="3mm" margin-left="3mm" font-size="6pt" >									
									<xsl:value-of select="//Shipment/@SCAC"/>
									</fo:inline>
									</fo:block>
									
						             </fo:table-cell>
							
								<fo:table-cell border-bottom-width="3px" border-bottom-style="hidden" border-bottom-color="black">
								
									<fo:block text-align="center" font-size="8pt">
									<xsl:value-of select="//Shipment/ShipNode/@ShipNode"/>
									</fo:block>
									<fo:block text-align="center" font-size="8pt">
									<xsl:value-of select="//Shipment/ShipNode/ShipNodePersonInfo/@AddressLine1"/>
									</fo:block>
									<fo:block text-align="center" font-size="8pt">
									<xsl:value-of select="//Shipment/ShipNode/ShipNodePersonInfo/@City"/>
									</fo:block>
									<fo:block text-align="center" font-size="8pt"  >
									<xsl:value-of select="//Shipment/ShipNode/ShipNodePersonInfo/@Country"/>
									</fo:block>
								</fo:table-cell>
								
								
							</fo:table-row>
							
							
							
							
							/*Order no, shipment no, Date*/
							<fo:table-row>
							
							<fo:table-cell height="15mm" width="40mm" padding-top="3pt">
									<fo:block font-weight="bold" text-align="left" font-size="6pt" padding="3pt" font-family="Calibri">
									ORDER #: 																	
									

									</fo:block>
									<fo:block font-weight="bold" text-align="left" font-size="6pt" padding="3pt" font-family="Calibri">
									Shipment #            
									
									</fo:block>
									
									<fo:block font-weight="bold" text-align="left" font-size="6pt" padding="3pt"  font-family="Calibri">
									Expected Ship Date 
									</fo:block>
									
									<fo:block font-weight="bold" text-align="left" font-size="6pt" padding="3pt" font-family="Calibri">
									Order Date  
									</fo:block>
									
								</fo:table-cell>
								/*Values of Order no, shipment no, Date*/
								<fo:table-cell height="15mm" width="40mm" padding-top="3pt">
									<fo:block text-align="left" font-size="6pt" padding="3pt" font-family="Calibri">
																		
									<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/@OrderNo"/>
									

									</fo:block>
									
									<fo:block  text-align="left" font-size="6pt" padding="3pt" font-family="Calibri">
									
									<xsl:value-of select="//Shipment/@ShipmentNo"/>
									
									</fo:block>
									
									<fo:block text-align="left" font-size="6pt" padding="3pt"  font-family="Calibri">
																		
									<xsl:value-of select="//Shipment/@ShipDate"/>
									
									</fo:block>
									
									<fo:block text-align="left" font-size="6pt" padding="3pt" font-family="Calibri">
									
									<xsl:value-of select="//Shipment/@ShipDate"/>
									
									</fo:block>
									
								</fo:table-cell>
								
								/*Barcodes, current date, number of days order placed*/
								 <fo:table-cell text-align="left" width="8mm" height="10mm"  padding-top="5pt">
								
									<fo:block font-weight="bold" text-align="left" font-size="6pt" padding="5pt">
									Current Date 
									<xsl:value-of select="java:format(java:java.text.SimpleDateFormat.new('yyyy-MM-dd HH:mm:ss.SSS'), java:java.util.Date.new())" />
									</fo:block>	
									
									<fo:block font-weight="bold" text-align="left" font-size="8pt"  padding="5pt">
									Days Since Order Placed 
									<xsl:value-of select="//Shipment/ShipmentLines/ShipmentLine/@DaysSinceOrderPlaced"/>
									DAYS
									</fo:block>
									<fo:block font-weight="bold" text-align="left" font-size="6pt">
									Order # 
									<xsl:variable name="OrderNo" select="//Shipment/ShipmentLines/ShipmentLine/@OrderNo"/>
									<fo:instream-foreign-object>  	
                                                    <barcode:barcode
                                                        xmlns:barcode="http://barcode4j.krysalis.org/ns"
                                                        message="{$OrderNo}">
		   
                                                        <barcode:code128>
                                                            <barcode:height>8mm</barcode:height>
                                                        </barcode:code128>
                                                    </barcode:barcode>
                                                </fo:instream-foreign-object>
									
									</fo:block>
									
									<fo:block font-weight="bold" text-align="left" font-size="6pt"  padding="5pt">
									Shipment # 
									<xsl:variable name="ShipmentNo" select="//Shipment/@ShipmentNo"/>
									<fo:instream-foreign-object>
                                                    <barcode:barcode
                                                        xmlns:barcode="http://barcode4j.krysalis.org/ns"
                                                        message="{$ShipmentNo}">
                                                        <barcode:code128>
                                                            <barcode:height>8mm</barcode:height>
                                                        </barcode:code128>
                                                    </barcode:barcode>
                                                </fo:instream-foreign-object>
									
									</fo:block>	
																			
								</fo:table-cell>
											

								
								/* Values of Barcodes, current date, number of days order placed*/
								
								
								
						</fo:table-row>
							
						<fo:table-row>							
								/*Shipping Address field*/
								<fo:table-cell  border="solid 0.25px black" 
                                                 text-align="left" height="2mm" font-weight="bold" width="20mm" font-size="8pt" background-color="gray" padding-top="2pt">
									<fo:block   padding="2pt" >
									Ship To
									</fo:block>
								</fo:table-cell>
								
						</fo:table-row>
						
						<fo:table-row>
								<fo:table-cell padding-top="3pt">								
									<fo:block text-align="left" font-size="5pt">
									    <xsl:value-of select="//Shipment/ToAddress/@AddressLine1"/>
									</fo:block>
									<fo:block text-align="left" font-size="5pt" padding-top="3pt">
										<xsl:value-of select="//Shipment/ToAddress/@City"/>
									</fo:block>
									<fo:block text-align="left" font-size="5pt" padding-top="3pt">
										<xsl:value-of select="//Shipment/ToAddress/@Country"/>
									</fo:block>
									<fo:block text-align="left" font-size="5pt" padding-top="3pt">
										<xsl:value-of select="//Shipment/ToAddress/@DayPhone"/>
									</fo:block>
													
								</fo:table-cell>
						</fo:table-row>	
						
				</fo:table-body>	
						
		</fo:table>		
								<fo:block padding-top="8pt">
									<fo:table table-layout="fixed" height="30cm" >
										<fo:table-body>
											<fo:table-row>
											
											<fo:table-cell border="solid 0.25px black" 
                                                   text-align="center" font-weight="bold" width="7mm" font-size="8pt" background-color="gray">
												<fo:block padding-top="8pt">
													SL#
												</fo:block>
												</fo:table-cell>
												
												<fo:table-cell border="solid 0.25px black" 
                                                   text-align="center" font-weight="bold" width="30mm" font-size="8pt" background-color="gray">
												<fo:block padding-top="8pt">
													UPC Code
												</fo:block>
												</fo:table-cell>
												
												<fo:table-cell border="solid 0.25px black" 
                                                    text-align="center" font-weight="bold" width="12mm" font-size="8pt" background-color="gray">
												<fo:block padding-top="8pt">
												SKN
												</fo:block>
												</fo:table-cell>
												
												<fo:table-cell border="solid 0.25px black" 
                                                   text-align="center" font-weight="bold" width="40mm" font-size="8pt" background-color="gray">
												<fo:block padding-top="8pt">
												Item Description
												</fo:block>
												</fo:table-cell>
												
												<fo:table-cell border="solid 0.25px black" 
                                                   text-align="center" font-weight="bold" width="15mm" font-size="8pt" background-color="gray">
												<fo:block padding-top="8pt">
												Division
												</fo:block>
												</fo:table-cell>
												
												<fo:table-cell border="solid 0.25px black" 
                                                   text-align="center" font-weight="bold" width="20mm" font-size="8pt" background-color="gray">
												<fo:block padding-top="8pt">
												Department
												</fo:block>
												</fo:table-cell>
												
												<fo:table-cell border="solid 0.25px black" 
                                                    text-align="center" font-weight="bold" width="12mm" font-size="8pt" background-color="gray">
												<fo:block padding-top="8pt">
												Color
												</fo:block>
												</fo:table-cell>
												
												<fo:table-cell border="solid 0.25px black" 
                                                    text-align="center" font-weight="bold" width="10mm" font-size="8pt" background-color="gray">
												<fo:block padding-top="8pt">
												Size
												</fo:block>
												</fo:table-cell>
												
												<fo:table-cell border="solid 0.25px black" 
                                                    text-align="center" font-weight="bold" width="10mm" font-size="8pt" background-color="gray">
												<fo:block padding-top="8pt">
												Price
												</fo:block>
												</fo:table-cell>
												
												<fo:table-cell border="solid 0.25px black" 
                                                    text-align="center" font-weight="bold" width="18mm" font-size="8pt" background-color="gray">
												<fo:block padding-top="8pt">
												Quantity to Pick
												</fo:block>
												</fo:table-cell>
												
												<fo:table-cell border="solid 0.25px black" 
                                                    text-align="center" font-weight="bold" width="12mm" font-size="8pt" background-color="gray">
												<fo:block padding-top="8pt">
												Gift Box
												</fo:block>
												</fo:table-cell>
												
												<fo:table-cell border="solid 0.25px black" 
                                                    text-align="center" font-weight="bold" width="12mm" font-size="8pt" background-color="gray">
												<fo:block padding-top="8pt">
												On Hand
												</fo:block>
												</fo:table-cell>
												
											</fo:table-row>
											
											<xsl:for-each select="//Shipment/ShipmentLines/ShipmentLine">
												<fo:table-row>
												
												<fo:table-cell text-align="center"  border-left-width="0.25px" border-left-style="solid"   font-size="5pt"  border-bottom-width="0.25px" border-bottom-style="solid">
														<fo:block padding-top="8pt">
														1
														</fo:block>
												</fo:table-cell>
													
													<fo:table-cell text-align="center"    font-size="5pt" border-bottom-width="0.25px" border-bottom-style="solid">
														<fo:block padding-top="8pt">
														<xsl:value-of select="./OrderLine/ItemDetails/ItemAliasList/ItemAlias/@AliasValue" />
														</fo:block>
														<fo:block padding-top="4pt">
														<fo:instream-foreign-object>	
                                                    <barcode:barcode
                                                        xmlns:barcode="http://barcode4j.krysalis.org/ns"
                                                        message="{./OrderLine/ItemDetails/ItemAliasList/ItemAlias/@AliasValue}">
		   
                                                        <barcode:code128>
                                                            <barcode:height>8mm</barcode:height>
                                                        </barcode:code128>
                                                    </barcode:barcode>
                                                </fo:instream-foreign-object>
													
														</fo:block>
													</fo:table-cell>
													
													<fo:table-cell  text-align="center" border-right-width="0.25px" border-right-style="solid"
														   font-size="5pt" border-bottom-width="0.25px" border-bottom-style="solid">
														<fo:block padding-top="8pt">
														<xsl:value-of select="OrderLine/ItemDetails/@ItemID" />
														
								                              
																</fo:block>


								                      
													</fo:table-cell>
													
													<fo:table-cell  text-align="center" border-right-width="0.25px" border-right-style="solid"
														border-left-width="0.25px" border-left-style="solid"   font-size="5pt"
														border-bottom-width="0.25px" border-bottom-style="solid">
														<fo:block padding-top="8pt">
														<xsl:value-of select="OrderLine/ItemDetails/PrimaryInformation/@Description" />
														</fo:block>
													</fo:table-cell>
													
													<fo:table-cell  text-align="center" border-right-width="0.25px" border-right-style="solid"
														border-left-width="0.25px" border-left-style="solid"  font-size="5pt"
														border-bottom-width="0.25px" border-bottom-style="solid">
														<fo:block padding-top="8pt">
														<xsl:value-of select="OrderLine/ItemDetails/Extn/@ExtnDivision" />
														</fo:block>
													</fo:table-cell>
													
													<fo:table-cell  text-align="center" border-right-width="0.25px" border-right-style="solid"
														border-left-width="0.25px" border-left-style="solid"  font-size="5pt"
														border-bottom-width="0.25px" border-bottom-style="solid">
														<fo:block padding-top="8pt">
														<xsl:value-of select="OrderLine/ItemDetails/Extn/@ExtnDivision" />
														</fo:block>
													</fo:table-cell>
													
													Color
													<fo:table-cell  text-align="center" border-right-width="0.25px" border-right-style="solid"
														border-left-width="0.25px" border-left-style="solid"  font-size="5pt"
														border-bottom-width="0.25px" border-bottom-style="solid">
														<fo:block padding-top="8pt">
														<xsl:value-of select="OrderLine/ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Color']/@Value" />
														</fo:block>
													</fo:table-cell>
													
														Size
													<fo:table-cell  text-align="center" border-right-width="0.25px" border-right-style="solid"
														border-left-width="0.25px" border-left-style="solid"  font-size="5pt"
														border-bottom-width="0.25px" border-bottom-style="solid">
														<fo:block padding-top="8pt">
														<xsl:value-of select="OrderLine/ItemDetails/AdditionalAttributeList/AdditionalAttribute[@Name='Size']/@Value" />
														</fo:block>
													</fo:table-cell>
													
													<fo:table-cell  text-align="center" border-right-width="0.25px" border-right-style="solid"
														border-left-width="0.25px" border-left-style="solid"  font-size="5pt"
														border-bottom-width="0.25px" border-bottom-style="solid">
														<fo:block padding-top="8pt">
														<xsl:value-of select="OrderLine/LinePriceInfo/@UnitPrice" />
														</fo:block>
													</fo:table-cell>
													
												<fo:table-cell  text-align="center" border-right-width="0.25px" border-right-style="solid"
														border-left-width="0.25px" border-left-style="solid"  font-size="5pt"
														border-bottom-width="0.25px" border-bottom-style="solid">
							                          <xsl:choose>
								                  <xsl:when test="@BackroomPickedQuantity">
									                 <xsl:variable name="backroomPickedQty">
										          <xsl:value-of select="number(@BackroomPickedQuantity)" />
									                    </xsl:variable>
									                  <xsl:variable name="originalQty">
										              <xsl:value-of select="number(@Quantity)" />
									                  </xsl:variable>
									                    <xsl:variable name="pickedQty">
										                 <xsl:value-of select="$originalQty - $backroomPickedQty" />
									                                 </xsl:variable>
									                                   <fo:block text-align="center" padding-top="8pt">
										                         <xsl:value-of select="fopUtil:getFormattedDouble($pickedQty,$locale)" />
									                           </fo:block>
								                                    </xsl:when>
								                                  <xsl:otherwise>
									                                  <xsl:variable name="lineQty" select="@Quantity" />
									                                 <fo:block text-align="center" padding-top="8pt">
										                       <xsl:value-of select="fopUtil:getFormattedDouble($lineQty,$locale)" />
									                           </fo:block>
								                         </xsl:otherwise>
							                                </xsl:choose>
						                                          </fo:table-cell>
													
													<fo:table-cell  text-align="center" border-right-width="0.25px" border-right-style="solid"
														border-left-width="0.25px" border-left-style="solid"  font-size="5pt"
														border-bottom-width="0.25px" border-bottom-style="solid">
														<fo:block padding-top="8pt">
														<xsl:value-of select="OrderLine/@GiftFlag" />
														</fo:block>
													</fo:table-cell>
													
													<fo:table-cell text-align="center" border-right-width="0.25px" border-right-style="solid" 
													border-left-width="0.25px" border-left-style="solid"  font-size="5pt"
													border-bottom-width="0.25px" border-bottom-style="solid">
														<fo:block padding-top="8pt">
														<xsl:value-of select="@OnHand" />
														
														</fo:block>
													</fo:table-cell>
													
													
												</fo:table-row>
											</xsl:for-each>
										</fo:table-body>
									</fo:table>
								</fo:block>
				</fo:flow>
			</fo:page-sequence>
		
		</fo:root>
	</xsl:template>
	</xsl:stylesheet>