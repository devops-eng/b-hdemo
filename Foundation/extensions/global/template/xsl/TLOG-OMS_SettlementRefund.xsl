<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
		<InvoiceDetail>
			<InvoiceHeader>
				<xsl:attribute name="InvoiceNo">
					<xsl:value-of select="InvoiceDetail/InvoiceHeader/@InvoiceNo"/>
				</xsl:attribute>
				<xsl:variable name="DocType" select="InvoiceDetail/InvoiceHeader/Order/@DocumentType"/>
				<xsl:variable name="EntryType" select="InvoiceDetail/InvoiceHeader/Order/@EntryType"/>
				<xsl:variable name="PaymentType" select="InvoiceDetail/InvoiceHeader/CollectionDetails/CollectionDetail/PaymentMethod/@PaymentType"/>
				<xsl:variable name="CreditCardType" select="InvoiceDetail/InvoiceHeader/CollectionDetails/CollectionDetail/PaymentMethod/@CreditCardType"/>
				<Order>
					<xsl:attribute name="BillToID">
						<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/@BillToID"/>
					</xsl:attribute>
					<xsl:attribute name="DocumentType">
						<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/@DocumentType"/>
					</xsl:attribute>
					<xsl:attribute name="EnterpriseCode">
						<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/@EnterpriseCode"/>
					</xsl:attribute>
					<xsl:attribute name="OrderHeaderKey">
						<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/@OrderHeaderKey"/>
					</xsl:attribute>
					<xsl:attribute name="OrderNo">
						<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/@OrderNo"/>
					</xsl:attribute>
					<xsl:attribute name="SellerOrganizationCode">
						<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/@SellerOrganizationCode"/>
					</xsl:attribute>
					<xsl:attribute name="OrderType">
						<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/@OrderType"/>
					</xsl:attribute>
					<!-- R3: Modified to fix defect 899 -->
					<xsl:choose>
						<xsl:when test="$DocType = '0003'">
							<xsl:attribute name="EnteredBy">
								<xsl:value-of select="InvoiceDetail/InvoiceHeader/LineDetails/LineDetail/OrderLine/DerivedFromOrderLine/Order/@EnteredBy"/>
							</xsl:attribute>
							<xsl:attribute name="EntryType">
								<xsl:value-of select="InvoiceDetail/InvoiceHeader/LineDetails/LineDetail/OrderLine/DerivedFromOrderLine/Order/@EntryType"/>
							</xsl:attribute>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="EnteredBy">
								<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/@EnteredBy"/>
							</xsl:attribute>		
							<xsl:attribute name="EntryType">
								<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/@EntryType"/>
							</xsl:attribute>							
						</xsl:otherwise>
					</xsl:choose>
					<!-- R3: End modifications for defect 899 -->
					<Extn>
						<xsl:attribute name="ExtnLoyaltyNo">
							<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/Extn/@ExtnLoyaltyNo"/>
						</xsl:attribute>
						<xsl:attribute name="ExtnCommLang">
							<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/Extn/@ExtnCommLang"/>
						</xsl:attribute>
						<!-- ******* Kumar: Modification for Gift Registry ******* -->
						<xsl:attribute name="ExtnRegistrantName">
							<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/Extn/@ExtnRegistrantName"/>
						</xsl:attribute>
						<xsl:attribute name="ExtnRegistryID">
							<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/Extn/@ExtnRegistryID"/>
						</xsl:attribute>	
						<!-- End of GR Modification -->
					</Extn>
					<PersonInfoBillTo>
						<xsl:attribute name="AddressLine1">
							<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@AddressLine1"/>
						</xsl:attribute>
						<xsl:attribute name="AddressLine2">
							<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@AddressLine2"/>
						</xsl:attribute>
						<xsl:attribute name="City">
							<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@City"/>
						</xsl:attribute>
						<xsl:attribute name="Country">
							<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@Country"/>
						</xsl:attribute>
						<xsl:attribute name="DayPhone">
							<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@DayPhone"/>
						</xsl:attribute>
						<xsl:attribute name="EMailID">
							<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@EMailID"/>
						</xsl:attribute>
						<xsl:attribute name="EveningPhone">
							<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@EveningPhone"/>
						</xsl:attribute>
						<xsl:attribute name="FirstName">
							<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@FirstName"/>
						</xsl:attribute>
						<xsl:attribute name="LastName">
							<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@LastName"/>
						</xsl:attribute>
						<xsl:attribute name="MiddleName">
							<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@MiddleName"/>
						</xsl:attribute>
						<xsl:attribute name="MobilePhone">
							<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@MobilePhone"/>
						</xsl:attribute>
						<xsl:attribute name="State">
							<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@State"/>
						</xsl:attribute>
						<xsl:attribute name="ZipCode">
							<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@ZipCode"/>
						</xsl:attribute>
					</PersonInfoBillTo>
				</Order>
				<Shipment>
					<xsl:attribute name="ShipDate">
						<xsl:value-of select="InvoiceDetail/InvoiceHeader/Shipment/@ShipDate"/>
					</xsl:attribute>
					<xsl:attribute name="ShipNode">
						<xsl:value-of select="InvoiceDetail/InvoiceHeader/Shipment/@ShipNode"/>
					</xsl:attribute>
					<xsl:attribute name="ShipmentKey">
						<xsl:value-of select="InvoiceDetail/InvoiceHeader/Shipment/@ShipmentKey"/>
					</xsl:attribute>
					<xsl:attribute name="CarrierServiceCode">
						<xsl:value-of select="InvoiceDetail/InvoiceHeader/Shipment/@CarrierServiceCode"/>
					</xsl:attribute>
					<!--********Modification done for DSV********** -->
					<xsl:attribute name="ShipmentNo">
					  <xsl:value-of select="InvoiceDetail/InvoiceHeader/Shipment/@ShipmentNo"/>
					</xsl:attribute>
					<!--********End Of Modification********** -->
					<!--********Modification done for TLog********** -->
					<Extn>
						<xsl:attribute name="FulfillingAssociate">
							<xsl:value-of select="InvoiceDetail/InvoiceHeader/Shipment/Extn/@ExtnFulfillingAssociate"/>
						</xsl:attribute>
					</Extn>
					<!--********End Of Modification********** -->
					<ToAddress>
						<xsl:choose>
							<xsl:when test="$DocType = '0003'">
								<xsl:attribute name="AddressLine1">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@AddressLine1"/>
								</xsl:attribute>
								<xsl:attribute name="AddressLine2">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@AddressLine2"/>
								</xsl:attribute>
								<xsl:attribute name="City">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@City"/>
								</xsl:attribute>
								<xsl:attribute name="Country">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@Country"/>
								</xsl:attribute>
								<xsl:attribute name="DayPhone">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@DayPhone"/>
								</xsl:attribute>
								<xsl:attribute name="EMailID">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@EMailID"/>
								</xsl:attribute>
								<xsl:attribute name="EveningPhone">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@EveningPhone"/>
								</xsl:attribute>
								<xsl:attribute name="FirstName">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@FirstName"/>
								</xsl:attribute>
								<xsl:attribute name="LastName">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@LastName"/>
								</xsl:attribute>
								<xsl:attribute name="MiddleName">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@MiddleName"/>
								</xsl:attribute>
								<xsl:attribute name="MobilePhone">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@MobilePhone"/>
								</xsl:attribute>
								<xsl:attribute name="State">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@State"/>
								</xsl:attribute>
								<xsl:attribute name="ZipCode">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Order/PersonInfoBillTo/@ZipCode"/>
								</xsl:attribute>
							</xsl:when>
							<xsl:otherwise>
								<xsl:attribute name="AddressLine1">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Shipment/ToAddress/@AddressLine1"/>
								</xsl:attribute>
								<xsl:attribute name="AddressLine2">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Shipment/ToAddress/@AddressLine2"/>
								</xsl:attribute>
								<xsl:attribute name="City">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Shipment/ToAddress/@City"/>
								</xsl:attribute>
								<xsl:attribute name="Country">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Shipment/ToAddress/@Country"/>
								</xsl:attribute>
								<xsl:attribute name="DayPhone">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Shipment/ToAddress/@DayPhone"/>
								</xsl:attribute>
								<xsl:attribute name="EMailID">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Shipment/ToAddress/@EMailID"/>
								</xsl:attribute>
								<xsl:attribute name="EveningPhone">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Shipment/ToAddress/@EveningPhone"/>
								</xsl:attribute>
								<xsl:attribute name="FirstName">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Shipment/ToAddress/@FirstName"/>
								</xsl:attribute>
								<xsl:attribute name="LastName">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Shipment/ToAddress/@LastName"/>
								</xsl:attribute>
								<xsl:attribute name="MiddleName">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Shipment/ToAddress/@MiddleName"/>
								</xsl:attribute>
								<xsl:attribute name="MobilePhone">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Shipment/ToAddress/@MobilePhone"/>
								</xsl:attribute>
								<xsl:attribute name="State">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Shipment/ToAddress/@State"/>
								</xsl:attribute>
								<xsl:attribute name="ZipCode">
									<xsl:value-of select="InvoiceDetail/InvoiceHeader/Shipment/ToAddress/@ZipCode"/>
								</xsl:attribute>
							</xsl:otherwise>
						</xsl:choose>
					</ToAddress>
					<!-- R3.1.21 Added for DSV Process -->
					<ShipNode>
						<xsl:attribute name="ShipnodeKey">
							<xsl:value-of select="InvoiceDetail/InvoiceHeader/Shipment/ShipNode/@ShipnodeKey"/>
						</xsl:attribute>
						<xsl:attribute name="NodeType">
							<xsl:value-of select="InvoiceDetail/InvoiceHeader/Shipment/ShipNode/@NodeType"/>
						</xsl:attribute>
					</ShipNode>
					<!-- R3.1.21 Added for DSV Process -->
				</Shipment>
				<LineDetails>
					<xsl:for-each select="InvoiceDetail/InvoiceHeader/LineDetails/LineDetail">
						<xsl:value-of select="."/>
						<LineDetail>
							<xsl:attribute name="OrderLineKey">
								<xsl:value-of select="@OrderLineKey"/>
							</xsl:attribute>
							<xsl:attribute name="PrimeLineNo">
								<xsl:value-of select="@PrimeLineNo"/>
							</xsl:attribute>
							<xsl:attribute name="LineTotal">
								<xsl:value-of select="@LineTotal"/>
							</xsl:attribute>
							<LineDetailTranQuantity>
								<xsl:attribute name="ShippedQty">
									<xsl:value-of select="@ShippedQty"/>
								</xsl:attribute>
							</LineDetailTranQuantity>
							<OrderLine>
							
								<!--***********Modification done for TLog***********-->
								<xsl:choose>
									<xsl:when test="(../../Shipment/ShipNode/@NodeType = 'VENDOR')">
										<xsl:attribute name="FulfillmentType">DROPSHIP</xsl:attribute>
									</xsl:when>
									<xsl:when test="(OrderLine/@FulfillmentType = 'FT_PICKUP')">
										<xsl:attribute name="FulfillmentType">PICKUP</xsl:attribute>
									</xsl:when>
									<xsl:when test="(OrderLine/@DeliveryMethod = 'PICK')" >
										<xsl:attribute name="FulfillmentType">PICKUP</xsl:attribute>								
									</xsl:when>
									<xsl:otherwise>
										<xsl:attribute name="FulfillmentType">SHIP</xsl:attribute>						
									</xsl:otherwise>
								</xsl:choose>
							        
								<!--***********End Of Modification***********-->
								<Item>
									<xsl:attribute name="ItemID">
										<xsl:value-of select="OrderLine/Item/@ItemID"/>
									</xsl:attribute>
									<xsl:attribute name="ProductLine">
										<xsl:value-of select="OrderLine/Item/@ProductLine"/>
									</xsl:attribute>
									<!--********Modification done for DSV********** -->
									<xsl:attribute name="UnitCost">
										<xsl:value-of select="OrderLine/Item/@UnitCost"/>
									</xsl:attribute>
									<!--********End Of Modification********** -->
								</Item>
								<!--CMS-Retek Project change start-->
								<xsl:for-each select="OrderLine/ItemDetails">
									<ItemDetails>
										<xsl:copy-of select="@*|node()" />
									</ItemDetails>
								</xsl:for-each>
								<!--CMS-Retek Project change end-->
								<Extn>
									<!-- ******* Kumar: Modification for Gift Registry ******* -->
									<!-- Start : 1/27 Defect # R3.2 : 405 Adding default values for ExtnRegistrantAddrsIndictr -->
									<xsl:variable name="GRAddressInd" select="OrderLine/Extn/@ExtnRegistrantAddrsIndictr" />
									<xsl:choose>
										<xsl:when test="$GRAddressInd = 'Y'" >
											<xsl:attribute name="ExtnRegistrantAddrsIndictr">Y</xsl:attribute>
										</xsl:when>
										<xsl:otherwise>
											<xsl:attribute name="ExtnRegistrantAddrsIndictr">N</xsl:attribute>						
										</xsl:otherwise>
									</xsl:choose>
									<!-- End : 1/27 Defect # R3.2 : 405 Adding default values for ExtnRegistrantAddrsIndictr -->
									<!-- Start : 2/1 Defect # R3.2 :405  Adding default values for ExtnPackSlpPrcSuppress-->
									<xsl:variable name="COMPackSlp" select="OrderLine/Extn/@ExtnPackSlpPrcSuppress" />
									<xsl:choose>
										<xsl:when test="$COMPackSlp = 'Y'" >
											<xsl:attribute name="ExtnPackSlpPrcSuppress">Y</xsl:attribute>
										</xsl:when>
										<xsl:otherwise>
											<xsl:attribute name="ExtnPackSlpPrcSuppress">N</xsl:attribute>
										</xsl:otherwise>
									</xsl:choose>
										<!--		<xsl:attribute name="ExtnPackSlpPrcSuppress">
										<xsl:value-of select="OrderLine/Extn/@ExtnPackSlpPrcSuppress"/>
									</xsl:attribute> -->
									<!-- End : 2/1 Defect # R3.2 :405  Adding default values for ExtnPackSlpPrcSuppress-->
									<!--***********End Of GR Modification***********-->
									<!-- Start:  ExtnTransactionId for Returns as per VDS -->
									<xsl:choose>
										<xsl:when test="$DocType = '0003'">
											<xsl:attribute name="ExtnTransactionID">
												<xsl:value-of select="OrderLine/Extn/@ExtnTransactionID"/>
											</xsl:attribute>
										</xsl:when>
									</xsl:choose>
									<!-- End: ExtnTransactionId for Returns as per VDS -->
                  					<!--********Modification done for DSV********** -->
                  					<xsl:attribute name="ExtnPOOrderNo">
                    					<xsl:value-of select="OrderLine/Extn/@ExtnPOOrderNo"/>
                  					</xsl:attribute>
                  					<!--********End Of Modification********** -->
								</Extn>
								<Instructions>
									<xsl:for-each select="OrderLine/Instructions/Instruction">
										<xsl:value-of select="."/>
										<Instruction>
											<xsl:attribute name="InstructionText">
												<xsl:value-of select="@InstructionText"/>
											</xsl:attribute>
											<xsl:attribute name="InstructionType">
												<xsl:value-of select="@InstructionType"/>
											</xsl:attribute>
										</Instruction>
									</xsl:for-each>
								</Instructions>
								<Notes>
									<xsl:for-each select="OrderLine/Notes/Note">
										<xsl:value-of select="."/>
										<Note>
											<xsl:attribute name="NoteText">
												<xsl:value-of select="@NoteText"/>
											</xsl:attribute>
											<xsl:attribute name="ReasonCode">
												<xsl:value-of select="@ReasonCode"/>
											</xsl:attribute>
										</Note>
									</xsl:for-each>
								</Notes>
								<LinePriceInfo>
									<xsl:attribute name="UnitPrice">
										<xsl:value-of select="OrderLine/LinePriceInfo/@UnitPrice"/>
									</xsl:attribute>
								</LinePriceInfo>								
							</OrderLine>
							<LineCharges>
								<xsl:for-each select="LineCharges/LineCharge">
									<xsl:value-of select="."/>
									<LineCharge>
										<xsl:attribute name="ChargeAmount">
											<xsl:value-of select="@ChargeAmount"/>
										</xsl:attribute>
										<xsl:attribute name="ChargeCategory">
											<xsl:value-of select="@ChargeCategory"/>
										</xsl:attribute>
										<xsl:attribute name="ChargeName">
											<xsl:value-of select="@ChargeName"/>
										</xsl:attribute>
										<xsl:attribute name="ChargePerUnit">
											<xsl:value-of select="@ChargePerUnit"/>
										</xsl:attribute>
										<xsl:attribute name="IsDiscount">
											<xsl:value-of select="@IsDiscount"/>
										</xsl:attribute>
										<xsl:attribute name="Reference">
											<xsl:value-of select="@Reference"/>
										</xsl:attribute>
									</LineCharge>
								</xsl:for-each>
							</LineCharges>
							<LineTaxes>
								<xsl:for-each select="LineTaxes/LineTax">
									<xsl:value-of select="."/>
									<LineTax>
										<xsl:attribute name="ChargeCategory">
											<xsl:value-of select="@ChargeCategory"/>
										</xsl:attribute>
										<xsl:attribute name="ChargeName">
											<xsl:value-of select="@ChargeName"/>
										</xsl:attribute>
										<xsl:attribute name="Tax">
											<xsl:value-of select="@Tax"/>
										</xsl:attribute>
										<xsl:attribute name="TaxName">
											<xsl:value-of select="@TaxName"/>
										</xsl:attribute>
										<xsl:attribute name="TaxPercentage">
											<xsl:value-of select="@TaxPercentage"/>
										</xsl:attribute>
									</LineTax>
								</xsl:for-each>
							</LineTaxes>
							<!--********Modification done for TLog**********-->
							<Promotions>
								<xsl:for-each select="OrderLine/Promotions/Promotion">
									<xsl:value-of select="."/>
									<Promotion>
										<xsl:attribute name="PromotionId">
											<xsl:value-of select="@PromotionId"/>
										</xsl:attribute>
										<xsl:attribute name="PromotionType">
											<xsl:value-of select="@PromotionType"/>
										</xsl:attribute>
										<xsl:attribute name="OverrideAdjustmentValue">
											<xsl:value-of select="@OverrideAdjustmentValue"/>
										</xsl:attribute>
										<xsl:attribute name="PromotionGroup">
											<xsl:value-of select="@PromotionGroup"/>
										</xsl:attribute>
									</Promotion>
								</xsl:for-each>
							</Promotions>
							<!--********End Of Modification**********-->
						</LineDetail>
					</xsl:for-each>
				</LineDetails>
				<HeaderCharges>
					<xsl:for-each select="InvoiceDetail/InvoiceHeader/HeaderCharges/HeaderCharge">
						<xsl:value-of select="."/>
						<HeaderCharge>
							<xsl:attribute name="ChargeAmount">
								<xsl:value-of select="@ChargeAmount"/>
							</xsl:attribute>
							<xsl:attribute name="ChargeCategory">
								<xsl:value-of select="@ChargeCategory"/>
							</xsl:attribute>
							<xsl:attribute name="ChargeName">
								<xsl:value-of select="@ChargeName"/>
							</xsl:attribute>
						</HeaderCharge>
					</xsl:for-each>
				</HeaderCharges>
				<HeaderTaxes>
					<xsl:for-each select="InvoiceDetail/InvoiceHeader/HeaderTaxes/HeaderTax">
						<xsl:value-of select="."/>
						<HeaderTax>
							<xsl:attribute name="ChargeCategory">
								<xsl:value-of select="@ChargeCategory"/>
							</xsl:attribute>
							<xsl:attribute name="ChargeName">
								<xsl:value-of select="@ChargeName"/>
							</xsl:attribute>
							<xsl:attribute name="Tax">
								<xsl:value-of select="@Tax"/>
							</xsl:attribute>
							<xsl:attribute name="TaxName">
								<xsl:value-of select="@TaxName"/>
							</xsl:attribute>
							<xsl:attribute name="TaxPercentage">
								<xsl:value-of select="@TaxPercentage"/>
							</xsl:attribute>
						</HeaderTax>
					</xsl:for-each>
				</HeaderTaxes>
				<CollectionDetails>
					<!-- Modified to fix defect 1047 - Multiple collection details in PayPal RO invoices. -->
					<!-- Populate only CollectionDetails with RetainElement=Y. -->
					<xsl:choose>
						<xsl:when test="/InvoiceDetail/InvoiceHeader/@InvoiceType = 'RETURN'">
					<xsl:for-each select="InvoiceDetail/InvoiceHeader/CollectionDetails/CollectionDetail[@RetainElement='Y']">
						<xsl:value-of select="."/>
						<CollectionDetail>
							<xsl:attribute name="AmountCollected">
								<xsl:value-of select="@AmountCollected"/>
							</xsl:attribute>
							<xsl:attribute name="AuthorizationExpirationDate">
								<xsl:value-of select="@AuthorizationExpirationDate"/>
							</xsl:attribute>
							<CreditCardTransactions>
									<CreditCardTransaction>
										<xsl:attribute name="AuthCode">
											<xsl:choose>
											<xsl:when test="$CreditCardType = 'PAYPAL'">
														<xsl:text></xsl:text>
												</xsl:when>
												<xsl:otherwise>
														<xsl:value-of select="@AuthorizationID"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:attribute>
									</CreditCardTransaction>
							</CreditCardTransactions>
							<PaymentMethod>
							    <!-- R3.01 : For PAYPAL, TLOG expects AuthID in Credit Card No -->
								<xsl:attribute name="CreditCardNo">
									<xsl:choose>
										<xsl:when test="$CreditCardType = 'PAYPAL'">
												<xsl:value-of select="@AuthorizationID"/>
										</xsl:when>
										<xsl:otherwise>
												<xsl:value-of select="PaymentMethod/@CreditCardNo"/>
										</xsl:otherwise>
									</xsl:choose>
							    <!-- R3.01 : For PAYPAL, TLOG expects AuthID in Credit Card No -->
								</xsl:attribute>
								<xsl:attribute name="CreditCardType">
									<xsl:value-of select="PaymentMethod/@CreditCardType"/>
								</xsl:attribute>
								<xsl:attribute name="CreditCardExpDate">
									<xsl:value-of select="PaymentMethod/@CreditCardExpDate"/>
								</xsl:attribute>
								<!-- Start : Kumar: update PaymentReference1 based on PaymentType -->
								<xsl:attribute name="PaymentReference1">
									<xsl:choose>
										<xsl:when test="$CreditCardType = 'PAYPAL'">
												<xsl:value-of select="PaymentMethod/@PaymentReference1"/>
										</xsl:when>
										<xsl:otherwise>
												<xsl:value-of select="PaymentMethod/@DisplayCreditCardNo"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:attribute>
								
								<!-- End -->
								<xsl:attribute name="PaymentReference3">
								<!-- R3: POS is not passing @TranReturnMessage, instead @AuthReturnMessage  -->
								    <xsl:choose>
										<xsl:when test="$EntryType = 'POS'">
											<!-- <xsl:value-of select="CreditCardTransactions/CreditCardTransaction/@PaymentReference3"/> -->	
											<xsl:value-of select="PaymentMethod/@PaymentReference3"/>
										</xsl:when>									
										<!-- Start : Kumar: update PaymentReference3 based on PaymentType only for Online Order (non paypal) -->	
										<xsl:when test="$CreditCardType = 'PAYPAL'" >
											<xsl:text></xsl:text>
										</xsl:when>
										<!-- End -->									
										<xsl:otherwise>
											<xsl:value-of select="CreditCardTransactions/CreditCardTransaction/@TranReturnMessage"/>
										</xsl:otherwise>
								    </xsl:choose>
								<!-- R3: POS is not passing @TranReturnMessage, instead @PaymentReference3 -->
								</xsl:attribute>
								<xsl:attribute name="PaymentType">
									<xsl:value-of select="PaymentMethod/@PaymentType"/>
								</xsl:attribute>
								<xsl:attribute name="SvcNo">
									<xsl:value-of select="PaymentMethod/@SvcNo"/>
								</xsl:attribute>
								<PersonInfoBillTo>
									<xsl:attribute name="AddressLine1">
										<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@AddressLine1"/>
									</xsl:attribute>
									<xsl:attribute name="AddressLine2">
										<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@AddressLine2"/>
									</xsl:attribute>
									<xsl:attribute name="City">
										<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@City"/>
									</xsl:attribute>
									<xsl:attribute name="Country">
										<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@Country"/>
									</xsl:attribute>
									<xsl:attribute name="DayPhone">
										<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@DayPhone"/>
									</xsl:attribute>
									<xsl:attribute name="EMailID">
										<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@EMailID"/>
									</xsl:attribute>
									<xsl:attribute name="EveningPhone">
										<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@EveningPhone"/>
									</xsl:attribute>
									<xsl:attribute name="FirstName">
										<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@FirstName"/>
									</xsl:attribute>
									<xsl:attribute name="LastName">
										<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@LastName"/>
									</xsl:attribute>
									<xsl:attribute name="MiddleName">
										<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@MiddleName"/>
									</xsl:attribute>
									<xsl:attribute name="MobilePhone">
										<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@MobilePhone"/>
									</xsl:attribute>
									<xsl:attribute name="State">
										<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@State"/>
									</xsl:attribute>
									<xsl:attribute name="ZipCode">
										<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@ZipCode"/>
									</xsl:attribute>
								</PersonInfoBillTo>
							</PaymentMethod>
						</CollectionDetail>
					</xsl:for-each>
						</xsl:when>
						<xsl:otherwise>
							<xsl:for-each select="InvoiceDetail/InvoiceHeader/CollectionDetails/CollectionDetail">
								<xsl:value-of select="."/>
								<CollectionDetail>
									<xsl:attribute name="AmountCollected">
										<xsl:value-of select="@AmountCollected"/>
									</xsl:attribute>
									<xsl:attribute name="AuthorizationExpirationDate">
										<xsl:value-of select="@AuthorizationExpirationDate"/>
									</xsl:attribute>
									<CreditCardTransactions>
										<CreditCardTransaction>
											<xsl:attribute name="AuthCode">
												<xsl:choose>
													<xsl:when test="$CreditCardType = 'PAYPAL'">
														<xsl:text></xsl:text>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="@AuthorizationID"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:attribute>
										</CreditCardTransaction>
									</CreditCardTransactions>
									<PaymentMethod>
										<!-- R3.01 : For PAYPAL, TLOG expects AuthID in Credit Card No -->
										<xsl:attribute name="CreditCardNo">
											<xsl:choose>
												<xsl:when test="$CreditCardType = 'PAYPAL'">
													<xsl:value-of select="@AuthorizationID"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="PaymentMethod/@CreditCardNo"/>
												</xsl:otherwise>
											</xsl:choose>
											<!-- R3.01 : For PAYPAL, TLOG expects AuthID in Credit Card No -->
										</xsl:attribute>
										<xsl:attribute name="CreditCardType">
											<xsl:value-of select="PaymentMethod/@CreditCardType"/>
										</xsl:attribute>
										<xsl:attribute name="CreditCardExpDate">
											<xsl:value-of select="PaymentMethod/@CreditCardExpDate"/>
										</xsl:attribute>
										<!-- Start : Kumar: update PaymentReference1 based on PaymentType -->
										<xsl:attribute name="PaymentReference1">
											<xsl:choose>
												<xsl:when test="$CreditCardType = 'PAYPAL'">
													<xsl:value-of select="PaymentMethod/@PaymentReference1"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="PaymentMethod/@DisplayCreditCardNo"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:attribute>

										<!-- End -->
										<xsl:attribute name="PaymentReference3">
											<!-- R3: POS is not passing @TranReturnMessage, instead @AuthReturnMessage  -->
											<xsl:choose>
												<xsl:when test="$EntryType = 'POS'">
													<!-- <xsl:value-of select="CreditCardTransactions/CreditCardTransaction/@PaymentReference3"/> -->
													<xsl:value-of select="PaymentMethod/@PaymentReference3"/>
												</xsl:when>
												<!-- Start : Kumar: update PaymentReference3 based on PaymentType only for Online Order (non paypal) -->
												<xsl:when test="$CreditCardType = 'PAYPAL'">
													<xsl:text></xsl:text>
												</xsl:when>
												<!-- End -->
												<xsl:otherwise>
													<xsl:value-of select="CreditCardTransactions/CreditCardTransaction/@TranReturnMessage"/>
												</xsl:otherwise>
											</xsl:choose>
											<!-- R3: POS is not passing @TranReturnMessage, instead @PaymentReference3 -->
										</xsl:attribute>
										<xsl:attribute name="PaymentType">
											<xsl:value-of select="PaymentMethod/@PaymentType"/>
										</xsl:attribute>
										<xsl:attribute name="SvcNo">
											<xsl:value-of select="PaymentMethod/@SvcNo"/>
										</xsl:attribute>
										<PersonInfoBillTo>
											<xsl:attribute name="AddressLine1">
												<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@AddressLine1"/>
											</xsl:attribute>
											<xsl:attribute name="AddressLine2">
												<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@AddressLine2"/>
											</xsl:attribute>
											<xsl:attribute name="City">
												<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@City"/>
											</xsl:attribute>
											<xsl:attribute name="Country">
												<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@Country"/>
											</xsl:attribute>
											<xsl:attribute name="DayPhone">
												<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@DayPhone"/>
											</xsl:attribute>
											<xsl:attribute name="EMailID">
												<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@EMailID"/>
											</xsl:attribute>
											<xsl:attribute name="EveningPhone">
												<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@EveningPhone"/>
											</xsl:attribute>
											<xsl:attribute name="FirstName">
												<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@FirstName"/>
											</xsl:attribute>
											<xsl:attribute name="LastName">
												<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@LastName"/>
											</xsl:attribute>
											<xsl:attribute name="MiddleName">
												<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@MiddleName"/>
											</xsl:attribute>
											<xsl:attribute name="MobilePhone">
												<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@MobilePhone"/>
											</xsl:attribute>
											<xsl:attribute name="State">
												<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@State"/>
											</xsl:attribute>
											<xsl:attribute name="ZipCode">
												<xsl:value-of select="PaymentMethod/PersonInfoBillTo/@ZipCode"/>
											</xsl:attribute>
										</PersonInfoBillTo>
									</PaymentMethod>
								</CollectionDetail>
							</xsl:for-each>
						</xsl:otherwise>
					</xsl:choose>
				</CollectionDetails>
				<Extn>
					<TransferFromList>
						<xsl:for-each select="InvoiceDetail/InvoiceHeader/Extn/TransferFromList/TransferFrom">
							<xsl:value-of select="."/>
							<TransferFrom>
								<xsl:attribute name="DerivedOrder">
									<xsl:value-of select="@DerivedOrder"/>
								</xsl:attribute>
								<xsl:attribute name="DerivedDocumentType">
									<xsl:value-of select="@DerivedDocumentType"/>
								</xsl:attribute>
								<xsl:attribute name="OrigGiftCardNo">
									<xsl:value-of select="@OrigGiftCardNo"/>
								</xsl:attribute>
								<xsl:attribute name="TransferAmount">
									<xsl:value-of select="@TransferAmount"/>
								</xsl:attribute>
								<xsl:attribute name="AuthCode">
									<xsl:value-of select="@AuthCode"/>
								</xsl:attribute>
							</TransferFrom>
						</xsl:for-each>
					</TransferFromList>
				</Extn>
			</InvoiceHeader>
		</InvoiceDetail>
	</xsl:template>
</xsl:stylesheet>
