<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
		<Order>
			<xsl:attribute name="OrderHeaderKey">
				<xsl:value-of select="Order/@OrderHeaderKey"/>
			</xsl:attribute>
			<xsl:attribute name="Override">
				<xsl:value-of select="'Y'"/>
			</xsl:attribute>
			<PersonInfoBillTo>
				<xsl:copy-of select="Order/PersonInfoShipTo/@*"/>
			</PersonInfoBillTo>
		</Order>	
	</xsl:template>
</xsl:stylesheet>
