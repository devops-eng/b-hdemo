<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0" >
	<xsl:template match="/">
		<Shipment>
			<xsl:copy-of select="Shipment/@*" />
			<xsl:attribute name="SellerOrganizationCode">
				<xsl:value-of select="Shipment/OrderList/Order/@SellerOrganizationCode"/>
			</xsl:attribute>
			<Extn>
			<xsl:copy-of select="Shipment/Extn/@*" />
			</Extn>
			<xsl:copy-of select="Shipment/Containers" />
			<xsl:copy-of select="Shipment/ShipmentLines" />
		</Shipment>
	</xsl:template>
</xsl:stylesheet>