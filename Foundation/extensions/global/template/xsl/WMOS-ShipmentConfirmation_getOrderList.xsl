<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
	<Order>
		<xsl:attribute name="DocumentType">
			<xsl:value-of select="/Shipment/@DocumentType"/>
		</xsl:attribute>	
		<xsl:attribute name="EnterpriseCode">
			<xsl:value-of select="/Shipment/@EnterpriseCode"/>
		</xsl:attribute>	
		<xsl:attribute name="OrderNo">
			<xsl:value-of select="/Shipment/ShipmentLines/ShipmentLine/@OrderNo"/>
		</xsl:attribute>			
	</Order>
	</xsl:template>
</xsl:stylesheet>