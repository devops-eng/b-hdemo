<?xml version="1.0" encoding="US-ASCII"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  version="1.0">
    <xsl:template match="/*">
        <xsl:copy-of select="*[1]"/>
    </xsl:template>
</xsl:stylesheet>