<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<PersonInfo>
			<xsl:attribute name="AddressLine1">
				<xsl:value-of select="PersonInfo/@AddressLine1"/>
			</xsl:attribute>
			<xsl:attribute name="AddressLine2">
				<xsl:value-of select="PersonInfo/@AddressLine2"/>
			</xsl:attribute>
			<xsl:attribute name="AddressLine3">
				<xsl:value-of select="PersonInfo/@AddressLine3"/>
			</xsl:attribute>
			<xsl:attribute name="AddressLine4">
				<xsl:value-of select="PersonInfo/@AddressLine4"/>
			</xsl:attribute>
			<xsl:attribute name="AddressLine5">
				<xsl:value-of select="PersonInfo/@AddressLine5"/>
			</xsl:attribute>
			<xsl:attribute name="AddressLine6">
				<xsl:value-of select="PersonInfo/@AddressLine6"/>
			</xsl:attribute>
			<xsl:attribute name="City">
				<xsl:value-of select="PersonInfo/@City"/>
			</xsl:attribute>
			<xsl:attribute name="Country">
				<xsl:value-of select="PersonInfo/@Country"/>
			</xsl:attribute>
			<xsl:attribute name="State">
				<xsl:value-of select="PersonInfo/@State"/>
			</xsl:attribute>
			<xsl:attribute name="ZipCode">
				<xsl:value-of select="PersonInfo/@ZipCode"/>
			</xsl:attribute>
			<xsl:attribute name="LocaleCode">
				<xsl:text>E</xsl:text>
			</xsl:attribute>
	</PersonInfo>
	</xsl:template>
</xsl:stylesheet>