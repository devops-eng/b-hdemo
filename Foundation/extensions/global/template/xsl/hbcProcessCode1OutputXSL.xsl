<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="avs soapenv" 
xmlns:avs="http://www.HBCMBServices.com/Code1AddValService" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
<xsl:output method="xml" indent="no"/>
	<xsl:template match="/">
	<PersonInfoList>
		<PersonInfo>
				<xsl:attribute name="AVSReturnCode">
					<xsl:value-of select="PersonInfoList/PersonInfo/AVSReturnCode"/>
				</xsl:attribute>
				<xsl:attribute name="AddressLine1">
					<xsl:value-of select="PersonInfoList/PersonInfo/AddressLine1"/>
				</xsl:attribute>
				<xsl:attribute name="AddressLine2">
					<xsl:value-of select="PersonInfoList/PersonInfo/AddressLine2"/>
				</xsl:attribute>
				<xsl:attribute name="AddressLine3">
					<xsl:value-of select="PersonInfoList/PersonInfo/AddressLine3"/>
				</xsl:attribute>
				<xsl:attribute name="AddressLine4">
					<xsl:value-of select="PersonInfoList/PersonInfo/AddressLine4"/>
				</xsl:attribute>
				<xsl:attribute name="AddressLine5">
					<xsl:value-of select="PersonInfoList/PersonInfo/AddressLine5"/>
				</xsl:attribute>
				<xsl:attribute name="AddressLine6">
					<xsl:value-of select="PersonInfoList/PersonInfo/AddressLine6"/>
				</xsl:attribute>
				<xsl:attribute name="City">
					<xsl:value-of select="PersonInfoList/PersonInfo/City"/>
				</xsl:attribute>
				<xsl:attribute name="Country">
					<xsl:value-of select="PersonInfoList/PersonInfo/Country"/>
				</xsl:attribute>
				<xsl:attribute name="IsAddressVerified">
					<xsl:value-of select="PersonInfoList/PersonInfo/IsAddressVerified"/>
				</xsl:attribute>
				<xsl:attribute name="State">
					<xsl:value-of select="PersonInfoList/PersonInfo/State"/>
				</xsl:attribute>
				<xsl:attribute name="ZipCode">
					<xsl:value-of select="PersonInfoList/PersonInfo/ZipCode"/>
				</xsl:attribute>
		</PersonInfo>
		<AddressVerificationResponseMessages>
			<AddressVerificationResponseMessage>
				<xsl:attribute name="MessageCode">
					<xsl:value-of select="PersonInfoList/AddressVerificationResponseMessages/AddressVerificationResponseMessage/MessageCode"/>
				</xsl:attribute>
				<xsl:attribute name="MessageText">
					<xsl:value-of select="PersonInfoList/AddressVerificationResponseMessages/AddressVerificationResponseMessage/MessageText"/>
				</xsl:attribute>
			</AddressVerificationResponseMessage>
		</AddressVerificationResponseMessages>
	</PersonInfoList>
	</xsl:template>
</xsl:stylesheet>
