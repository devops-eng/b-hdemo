scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/common/address/capture/AddressCapturePageExtnUI",
        "scbase/loader!sc/plat/dojo/utils/EditorUtils",
        "scbase/loader!sc/plat/dojo/utils/ScreenUtils",
        "scbase/loader!sc/plat/dojo/utils/ModelUtils",
        "scbase/loader!sc/plat/dojo/utils/BaseUtils",
        "scbase/loader!isccs/utils/BaseTemplateUtils",
        "scbase/loader!isccs/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils","scbase/loader!isccs/utils/ModelUtils"
    ],
    function (
        _dojodeclare,
        _extnAddressCapturePageExtnUI,
        _scEditorUtils,
        _scScreenUtils,
        _scModelUtils,
        _scBaseUtils,
        _isccsBaseTemplateUtils,
        _isccsUIUtils,
        _scEventUtils,
        _scWidgetUtils,
        _isccsModelUtils
    ) {
        return _dojodeclare("extn.common.address.capture.AddressCapturePageExtn", [_extnAddressCapturePageExtnUI], {
            // custom code here

            extn_AddressValidation: function (event, bEvent, ctrl, args) {
                var currentEditor = null;
                var screen = _scEditorUtils.getCurrentEditor();
                var screenInput = _scScreenUtils.getInitialInputData(this);
                var value = _scWidgetUtils.getValue(this,"radSelectOrAdd");
                if(value == "N")
                {
                    return true;
                }
                var targetModel = _scBaseUtils.getTargetModel(this, "verifyAddress_input", null);
                var firstName = _scModelUtils.getStringValueFromPath("PersonInfo.FirstName", targetModel);
                var lastName = _scModelUtils.getStringValueFromPath("PersonInfo.LastName", targetModel);
                var memberEmail = _scModelUtils.getStringValueFromPath("PersonInfo.EMailID", targetModel);
                var memberDayPhone = _scModelUtils.getStringValueFromPath("PersonInfo.DayPhone", targetModel);
                var memberEveningPhone = _scModelUtils.getStringValueFromPath("PersonInfo.EveningPhone", targetModel);
                var memberAddressLine1 = _scModelUtils.getStringValueFromPath("PersonInfo.AddressLine1", targetModel);
                var memberCity = _scModelUtils.getStringValueFromPath("PersonInfo.City", targetModel);
                var memberState = _scModelUtils.getStringValueFromPath("PersonInfo.State", targetModel);
                var memberZipCode = _scModelUtils.getStringValueFromPath("PersonInfo.ZipCode", targetModel);
                var countryCode = _scModelUtils.getStringValueFromPath("PersonInfo.Country", targetModel);
                var aphlbtsPattern = /^[a-zA-Z0-9]+$/;
                var numbPattern = /^[0-9]+$/;
                var emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z0-9]+\.)+[a-zA-Z]{2,}))$/;
                var zipLength = 5;
                var postalCodeRegex_CAN = /^[A-Z][0-9][A-Z] [0-9][A-Z][0-9]$/;
                var postalCodeRegex_US = /^\d{5}(-\d{4})?$/;
                var phoneLength = 10;
                //First Name Validation
                if ((_scBaseUtils.isVoid(firstName))) {
                     var message = null;
                     message = _scScreenUtils.getString(this,"extn_FirstName_Mandatory");
                     _isccsBaseTemplateUtils.showMessage(this,message,"error",null);
                     
                  //  _isccsBaseTemplateUtils.showMessage(this, "Enter First Name", "error", null);
                    sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
                    return true;

                } else {
                    if (!(aphlbtsPattern.test(firstName))) {
                      var message = null;
                      message = _scScreenUtils.getString(this,"extn_FirstName_Validation");
                     _isccsBaseTemplateUtils.showMessage(this,message,"error",null);

                       // _isccsBaseTemplateUtils.showMessage(this, "The First Name entered is invalid", "error", null);
                        sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
			   return true;
                    }
                }

                //Last Name Validation
                if ((_scBaseUtils.isVoid(lastName))) {
                     var message = null;
                      message = _scScreenUtils.getString(this,"extn_LastName_Mandatory");
                     _isccsBaseTemplateUtils.showMessage(this,message,"error",null);


                   // _isccsBaseTemplateUtils.showMessage(this, "Enter Last Name", "error", null);
                    sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
                    return true;


                } else {
                    if (!(aphlbtsPattern.test(lastName))) {
                      var message = null;
                      message = _scScreenUtils.getString(this,"extn_LastName_Validation");
                     _isccsBaseTemplateUtils.showMessage(this,message,"error",null);

                       // _isccsBaseTemplateUtils.showMessage(this, "The Last Name entered is invalid", "error", null);
                        sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
                         return true;

                    }
                }



                //Email validation
                if ((_scBaseUtils.isVoid(memberEmail))) {
//                     var message = null;
//                      message = _scScreenUtils.getString(this,"extn_Email_Mandatory");
//                     _isccsBaseTemplateUtils.showMessage(this,message,"error",null);

                   // _isccsBaseTemplateUtils.showMessage(this, "Enter Email address", "error", null);
//                    sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
//                    return true;



                } else {

                    /*--Regular Expression for validating the Login Email Id--*/
                    var pattern = emailPattern;


                    if (memberEmail && !(pattern.test(memberEmail))) {

                          var message = null;
                          message = _scScreenUtils.getString(this,"extn_Email_Validation");
                         _isccsBaseTemplateUtils.showMessage(this,message,"error",null);

                        //_isccsBaseTemplateUtils.showMessage(this, "The email address entered is invalid", "error", null);
                        sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
                         return true;


                    } else {

                        //_scWidgetUtils.clearFieldinError(this, "memberEmail");

                    }
                }


                //Telephone validation
                if (!(_scBaseUtils.isVoid(memberDayPhone))) {
                    if (memberDayPhone.length != phoneLength) {
                         var message = null;
                         message = _scScreenUtils.getString(this,"extn_Telephone_Validation");
                         _isccsBaseTemplateUtils.showMessage(this,message,"error",null);

                       // _isccsBaseTemplateUtils.showMessage(this, "The phone number entered is invalid", "error", null);
                        sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
                         return true;


                    } else if (memberDayPhone.length == phoneLength) {

                        if (!(numbPattern.test(memberDayPhone))) {
                            var message = null;
                            message = _scScreenUtils.getString(this,"extn_Telephone_Validation");
                             _isccsBaseTemplateUtils.showMessage(this,message,"error",null);

                           // _isccsBaseTemplateUtils.showMessage(this, "The phone number entered is invalid", "error", null);
                            sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
                            return true;

                        }

                    }
                } else {
                         var message = null;
                         message = _scScreenUtils.getString(this,"extn_Telephone_Mandatory");
                         _isccsBaseTemplateUtils.showMessage(this,message,"error",null);
                    //_isccsBaseTemplateUtils.showMessage(this, "Enter Telephone number", "error", null);
                    sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
                     return true;


                }


                //EveningPhone validation
                if (!(_scBaseUtils.isVoid(memberEveningPhone))) {
                    if (memberEveningPhone.length != phoneLength) {
				var message = null;
                            message = _scScreenUtils.getString(this,"extn_Telephone_Validation");
                            _isccsBaseTemplateUtils.showMessage(this,message,"error",null);

                       // _isccsBaseTemplateUtils.showMessage(this, "The phone number entered is invalid", "error", null);
                        sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
                         return true;


                    } else if (memberEveningPhone.length == phoneLength) {

                        if (!(numbPattern.test(memberEveningPhone))) {
                            var message = null;
                            message = _scScreenUtils.getString(this,"extn_Telephone_Validation");
                             _isccsBaseTemplateUtils.showMessage(this,message,"error",null);

                           // _isccsBaseTemplateUtils.showMessage(this, "The phone number entered is invalid", "error", null);
                            sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
                             return true;

                        }
                    }

                }
                //mandatory fields check
                //Address Line 1 validation
                if ((_scBaseUtils.isVoid(memberAddressLine1))) {
			var message = null;
                     message = _scScreenUtils.getString(this,"extn_AddressLine1_Manadatory");
                     _isccsBaseTemplateUtils.showMessage(this,message,"error",null);

                   // _isccsBaseTemplateUtils.showMessage(this, "Enter Address Line1", "error", null);
                    sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
                     return true;


                }

                //Postal Code validation
                if ((_scBaseUtils.isVoid(memberZipCode))) {
                      var message = null;
                      message = _scScreenUtils.getString(this,"extn_ZipCode_Manadatory");
                     _isccsBaseTemplateUtils.showMessage(this,message,"error",null);

                   // _isccsBaseTemplateUtils.showMessage(this, "Enter Zip code", "error", null);
                    sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
                     return true;


                } else if (!(_scBaseUtils.isVoid(memberZipCode))) {
                    switch (countryCode) {
                    case "US":
                        if (!(postalCodeRegex_US.test(memberZipCode))) {
                            var message = null;
                            message = _scScreenUtils.getString(this,"extn_ZipCode_Validation_US");
                            _isccsBaseTemplateUtils.showMessage(this,message,"error",null);

                           // _isccsBaseTemplateUtils.showMessage(this, "Invalid U.S. ZipCode", "error", null);
                            sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
                             return true;

                        }
                        break;
                    case "CA":
                        if (!(postalCodeRegex_CAN.test(memberZipCode))) {
                            var message = null;
                            message = _scScreenUtils.getString(this,"extn_ZipCode_Validation_CA");
                            _isccsBaseTemplateUtils.showMessage(this,message,"error",null);

                          //  _isccsBaseTemplateUtils.showMessage(this, "Invalid Canada ZipCode", "error", null);
                            sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
                             return true;

                        }
                        break;
                    }
                }
            },

            extn_init: function (event, bEvent, ctrl, args) {
                var inputModel = _scScreenUtils.getModel(this, "screenInitialInput");
                var personInfoModel = _scScreenUtils.getModel(this, "PersonInfo");
                var addressLine1 = _scModelUtils.getStringValueFromPath("PersonInfo.AddressLine1", personInfoModel);
		        var enterpriseCode = _scModelUtils.getStringValueFromPath("PersonInfo.EnterpriseCode", personInfoModel);
                var title = args.screen.popupInstance.params.title;
                var shipToTitle = _scScreenUtils.getString(this,"ShipTo");
                var billToTitle = _scScreenUtils.getString(this,"BillTo");
		  if (!(_scBaseUtils.isVoid(inputModel))) {
		  //	if (enterpriseCode== "DEFAULT") {
                		if (title == billToTitle) {
                    			if (_scBaseUtils.isVoid(addressLine1)) {
                        			var orderHeaderKey = _scModelUtils.getStringValueFromPath("Order.OrderHeaderKey", inputModel);
                        			var orderModel = _scBaseUtils.getNewBeanInstance();
                        			_scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", orderHeaderKey, orderModel);
                        			_isccsUIUtils.callApi(this, orderModel, "extn_OrderDetails");
                    				}
                			}

               		if (title == shipToTitle) {
                    			var orderHeaderKey = _scModelUtils.getStringValueFromPath("Order.OrderHeaderKey", inputModel);
                    			var orderModel = _scBaseUtils.getNewBeanInstance();
                    			_scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", orderHeaderKey, orderModel);
                    			_isccsUIUtils.callApi(this, orderModel, "extn_OrderDetails");
                			}
		 	    //	}
			}
            },

            extn_MashupCompletion: function (event, bEvent, ctrl, args) {
                var output = args.mashupArray;
                var model = null;
                var enterpriseCode = null;
                for (var index in output) {
                    if (output[index].mashupRefId == "extn_OrderDetails") {
                        model = output[index].mashupRefOutput;
                        var commonCodeList = null;
                        enterpriseCode = _scModelUtils.getStringValueFromPath("Order.EnterpriseCode", model);
                        var screenInput = _scScreenUtils.getModel(this, "PersonInfo");
                        _scScreenUtils.setModel(this, "extn_enterpriseCode_ns", model);
                        var enterpriseModel = _scScreenUtils.getModel(this, "extn_enterpriseCode_ns");
                        if (enterpriseCode == "LT") {
                            var countryCode = "LTCountryCode";
                            var apiInput = {
                                CommonCode: {
                                    CodeType: countryCode
                                }
                            }
                            _isccsUIUtils.callApi(this, apiInput, "extn_countryCode");
                        } else if (enterpriseCode == "BAY") {
                            var countryCode = "BayCountryCode";
                            var apiInput = {
                                CommonCode: {
                                    CodeType: countryCode
                                }
                            }
                            _isccsUIUtils.callApi(this, apiInput, "extn_countryCode");
                        }
                    } else if (output[index].mashupRefId == "extn_countryCode") {
                        commonCodeList = output[index].mashupRefOutput;
                        var commonCodes = _scModelUtils.getStringValueFromPath("CommonCodeList.CommonCode", commonCodeList);
                        for (var i in commonCodes) {
                            var country = commonCodes[i].CodeShortDescription;
                            var screenInput = _scScreenUtils.getModel(this, "PersonInfo");
                            _scWidgetUtils.setValue(this, "cmbCountry", country, false);
                            this.handleCountrySelected();
                        }

                    }
                }
            },

            extn_countryValidation: function (event, bEvent, ctrl, args) {
                var title = args.screen.popupInstance.popupParams.title;
                 var shipToTitle = _scScreenUtils.getString(this,"ShipTo");
                if (title == shipToTitle) {
                    var inputModel = _scScreenUtils.getModel(this, "extn_enterpriseCode_ns");
                    if(!(_scBaseUtils.isVoid(inputModel))){
                        enterpriseCode = _scModelUtils.getStringValueFromPath("Order.EnterpriseCode", inputModel);
                        var countryModel = _scBaseUtils.getTargetModel(this, "getCountry_input");
                        var country = _scModelUtils.getStringValueFromPath("PersonInfo.Country", countryModel);
                        var message = null;
                        if (enterpriseCode == "LT") {
                            if (!(country == "US")) {
    
                                message = _scScreenUtils.getString(this,"extn_ShipTO_LT_Validation");
                                _scScreenUtils.showErrorMessageBox(this, message, null, null, args);
    
                              //  _scScreenUtils.showErrorMessageBox(this, "Order cannot be shipped to CANADA for LT. Please enter US shipping Address", null, null, args);
                                _scEventUtils.stopEvent(bEvent);
                            }
                        } else if (enterpriseCode == "BAY") {
                            if (!(country == "CA")) {
                                
                              
                                message = _scScreenUtils.getString(this,"extn_ShipTO_BAY_Validation");
                                _scScreenUtils.showErrorMessageBox(this, message, null, null, args);
    
                              //_scScreenUtils.showErrorMessageBox(this, "Order cannot be shipped to USA for BAY. Please enter Canadian shipping address", null, null, args);
                                _scEventUtils.stopEvent(bEvent);
                            }
                        }
                    }else{
                        var personInfoModel = _scScreenUtils.getModel(this, "PersonInfo");
		                var enterpriseCode = _scModelUtils.getStringValueFromPath("PersonInfo.EnterpriseCode", personInfoModel);
                              var countryModel = _scBaseUtils.getTargetModel(this, "getCountry_input");
                              var country = _scModelUtils.getStringValueFromPath("PersonInfo.Country", countryModel);
		                //var country = _scModelUtils.getStringValueFromPath("PersonInfo.Country", personInfoModel);
		                if (enterpriseCode == "LT") {
                            if (!(country == "US")) {
    
                                message = _scScreenUtils.getString(this,"extn_ShipTO_LT_Validation");
                                _scScreenUtils.showErrorMessageBox(this, message, null, null, args);
    
                              //  _scScreenUtils.showErrorMessageBox(this, "Order cannot be shipped to CANADA for LT. Please enter US shipping Address", null, null, args);
                                _scEventUtils.stopEvent(bEvent);
                            }
                        } else if (enterpriseCode == "BAY") {
                            if (!(country == "CA")) {
                                
                              
                                message = _scScreenUtils.getString(this,"extn_ShipTO_BAY_Validation");
                                _scScreenUtils.showErrorMessageBox(this, message, null, null, args);
    
                              //_scScreenUtils.showErrorMessageBox(this, "Order cannot be shipped to USA for BAY. Please enter Canadian shipping address", null, null, args);
                                _scEventUtils.stopEvent(bEvent);
                            }
                        }
                    }
                }

            },

            handleVerifyAddress: function (
                verifyAddress) {
                var sState = null;
                var ePersonInfo = null;
                sState = _scModelUtils.getStringValueFromPath("PersonInfoList.Status", verifyAddress);
                var closePopup = "false";
                var isMultipleRequired = false;
                var message = null;
                var verifyLevel = _scModelUtils.getStringValueFromPath("PersonInfoList.VerifyLevel", verifyAddress);
                var personInfoList =_scModelUtils.getNumberValueFromPath("PersonInfoList.TotalNumberOfRecords", verifyAddress);
                var personInfoRecord = verifyAddress.PersonInfoList.PersonInfo;
                if (!(
                        _scModelUtils.getBooleanValueFromPath("PersonInfoList.ProceedWithSingleAVSResult", verifyAddress, true))) {
                    isMultipleRequired = true;
                }
                if (
                    _scBaseUtils.equals(
                        sState, "FAILED")) {
                    var lAddressVerificationResponseMessages = null;
                    lAddressVerificationResponseMessages = _scModelUtils.getModelListFromPath("PersonInfoList.AddressVerificationResponseMessages.AddressVerificationResponseMessage", verifyAddress);
                    if (!(
                            _scBaseUtils.isVoid(
                                lAddressVerificationResponseMessages))) {
                        var eMessageList = null;
                        eMessageList = _scBaseUtils.getNewBeanInstance();
                        _scBaseUtils.addArrayValueToBean("messageList", lAddressVerificationResponseMessages, eMessageList);
                        _isccsBaseTemplateUtils.showMessage(this, "InvalidAddressProvided", "error", eMessageList);



                        if (
                            _scResourcePermissionUtils.hasPermission("ISCADDOVR001")) {
                            _scWidgetUtils.showWidget(
                                this, "pnlAddressOverride", true, null);
                            _scScreenUtils.clearScreen(
                                this, null);
                        }
                    }
                } else if (
                    _scBaseUtils.or(
                        _scBaseUtils.greaterThan(
                            _scModelUtils.getNumberValueFromPath("PersonInfoList.TotalNumberOfRecords", verifyAddress), 1), isMultipleRequired)) {
                    _scWidgetUtils.hideWidget(
                        this, "pnlAddressCapture", true);
                    _scWidgetUtils.showWidget(
                        this, "pnlAddressVerification", true, null);
                    _scWidgetUtils.showWidget(
                        this, "lnkEnterAddress", true, null);
                        // custom Code for AVS Call
                    //_isccsBaseTemplateUtils.showMessage(this, "ExactAddressNotFound", "error", null);
                    
                    
                    if (verifyLevel == "PremisesPartial") {

                         message = _scScreenUtils.getString(this,"extn_AVS_ApartmentNumber");
                         _isccsBaseTemplateUtils.showMessage(this,message, "warning",null);

                        //_isccsBaseTemplateUtils.showMessage(this, "To proceed, please choose one of the options below. (or)Please confirm your apartment number.", "warning", null);
                    } else if (verifyLevel == "StreetPartial") {
                         
                          message = _scScreenUtils.getString(this,"extn_AVS_BuildingNumber");
                         _isccsBaseTemplateUtils.showMessage(this,message, "warning",null);

                       // _isccsBaseTemplateUtils.showMessage(this, "To proceed, please choose one of the options below. (or)Please confirm your building number.", "warning", null);
                    }
                    else if (verifyLevel == "InteractionRequired") {
                          
                          message = _scScreenUtils.getString(this,"extn_AVS_InteractionRequired");
                         _isccsBaseTemplateUtils.showMessage(this,message, "warning",null);


                        //_isccsBaseTemplateUtils.showMessage(this, "We found more than one match for your address. To proceed, please choose one of the options below.", "warning", null);
                    }
                    // Custom Code end
                } else if (
                    _scBaseUtils.equals(
                        sState, "AVS_DOWN")) {
                    ePersonInfo = _scBaseUtils.getTargetModel(
                        this, "verifyAddress_input", null);
                    _scModelUtils.setBooleanValueAtModelPath("PersonInfo.IsAddressVerified", false, ePersonInfo);
                    _isccsBaseTemplateUtils.hideMessage(
                        this);
                    _scModelUtils.setStringValueAtModelPath("PersonInfo.PersonInfoKey", " ", ePersonInfo);
                    _scScreenUtils.setPopupOutput(
                        this, ePersonInfo);
                    _scWidgetUtils.closePopup(
                        this, "APPLY", false);
                    closePopup = "true";
                } 
                // Custom Code for AVS Call for No matches found
               else if(verifyLevel=="" && personInfoList== 1)
                {
                        message = _scScreenUtils.getString(this,"extn_AVS_NoMatches");
                        _isccsBaseTemplateUtils.showMessage(this,message, "warning",null);

                   //  _isccsBaseTemplateUtils.showMessage(this, "No Matches Found. To Proceed Please Choose User Entered Address", "warning",null);
                    _scWidgetUtils.hideWidget(
                        this, "pnlAddressCapture", true);
                    _scWidgetUtils.showWidget(
                        this, "pnlAddressVerification", true, null);
                    _scWidgetUtils.showWidget(
                        this, "lnkEnterAddress", true, null);
                } 
                else if(personInfoRecord.length ==1)
                {
                    var personInfo = personInfoRecord[0];
                    var processingFailed =  _scModelUtils.getStringValueFromPath("ProcessingFailed", personInfo);
                    if(processingFailed== "Y")
                    {
                        message = _scScreenUtils.getString(this,"extn_AVS_WebServiceDown");
                        _isccsBaseTemplateUtils.showMessage(this,message,"warning",null);

                     //_isccsBaseTemplateUtils.showMessage(this, "AVS down. Please try After Sometime. To Proceed Please Choose User Entered Address ", "warning",null);   
                    }
                    
                }
                // custom code end
                else {
                    ePersonInfo = this.getFirstPersonInfo(
                        verifyAddress);
                    _isccsBaseTemplateUtils.hideMessage(
                        this);
                    _scModelUtils.setStringValueAtModelPath("PersonInfo.PersonInfoKey", " ", ePersonInfo);
                    _scScreenUtils.setPopupOutput(
                        this, ePersonInfo);
                    _scWidgetUtils.closePopup(
                        this, "APPLY", false);
                    closePopup = "true";
                }
                this.fireAddressVerified(
                    ePersonInfo, closePopup);
            }

        });

    });
