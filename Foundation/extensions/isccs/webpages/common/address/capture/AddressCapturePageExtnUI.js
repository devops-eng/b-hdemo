
scDefine(["dojo/text!./templates/AddressCapturePageExtn.html","scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/_base/lang","scbase/loader!dojo/text","scbase/loader!sc/plat","scbase/loader!sc/plat/dojo/utils/BaseUtils"]
 , function(			 
			    templateText
			 ,
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojolang
			 ,
			    _dojotext
			 ,
			    _scplat
			 ,
			    _scBaseUtils
){
return _dojodeclare("extn.common.address.capture.AddressCapturePageExtnUI",
				[], {
			templateString: templateText
	
	
	
	
	
	
					,	
	namespaces : {
		targetBindingNamespaces :
		[
		],
		sourceBindingNamespaces :
		[
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_9'
						,
	  description: 'extn_enterpriseCode_ns'
						,
	  value: 'extn_enterpriseCode_ns'
						
			}
			
		]
	}

	

	,
	hotKeys: [ 
	]

,events : [
	]

,subscribers : {
local : [

{
	  eventId: 'afterScreenInit'

,	  sequence: '51'

,	  description: 'extn_init'



,handler : {
methodName : "extn_init"

, description :  "extn_init"  
}
}
,
{
	  eventId: 'Popup_btnNext_onClick'

,	  sequence: '19'




,handler : {
methodName : "extn_AddressValidation"

, description :  "Address Validation"  
}
}
,
{
	  eventId: 'Popup_btnNext_onClick'

,	  sequence: '18'

,	  description: 'extn_countryValidation'



,handler : {
methodName : "extn_countryValidation"

 
}
}
,
{
	  eventId: 'onExtnMashupCompletion'

,	  sequence: '51'

,	  description: 'extn_MashupCompletion '



,handler : {
methodName : "extn_MashupCompletion"

 
}
}

]
					 	 ,
}

});
});


