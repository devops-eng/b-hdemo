
scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/common/address/display/AddressDisplayExtnUI","scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnAddressDisplayExtnUI,
			    _scWidgetUtils, _scBaseUtils, _scModelUtils, _scScreenUtils
			    

){ 
	return _dojodeclare("extn.common.address.display.AddressDisplayExtn", [_extnAddressDisplayExtnUI],{
	// custom code here
	extn_afterScreenInIt:function()
	{
		
		
            
		if (_scBaseUtils.equals(this.ownerScreen.className, "OrderLineSummary")) {
                var orderDetails = _scScreenUtils.getModel(this.ownerScreen, "getCompleteOrderLineDetails_output");
                var deliveryMethod = _scModelUtils.getStringValueFromPath("OrderLine.DeliveryMethod", orderDetails);
                if (_scBaseUtils.equals(deliveryMethod, "PICK")) {
                    
                        _scWidgetUtils.hideWidget(this, "lblAddressLine1", true);
                        _scWidgetUtils.hideWidget(this, "lblCityStateZip", true);
                        _scWidgetUtils.hideWidget(this, "lblName", true);
                        _scWidgetUtils.hideWidget(this, "lblCountry", true);
                        _scWidgetUtils.hideWidget(this, "lblAddressLine2", true);
                        var billToFirstName = _scModelUtils.getStringValueFromPath("OrderLine.BillToFirstName", orderDetails);
                        var billToLastName = _scModelUtils.getStringValueFromPath("OrderLine.BillToLastName", orderDetails);
                        var markForFirstName = _scModelUtils.getStringValueFromPath("OrderLine.PersonInfoFirstName", orderDetails);
                        var markForLastName = _scModelUtils.getStringValueFromPath("OrderLine.PersonInfoLastName", orderDetails);
                        _scWidgetUtils.setValue(this, "extn_shipToName", billToFirstName + " " + billToLastName);
                        _scWidgetUtils.showWidget(this, "extn_shipToName", true);
                        
                        if(!_scBaseUtils.isVoid(markForFirstName) && !_scBaseUtils.isVoid(markForLastName)){
                        _scWidgetUtils.setValue(this, "extn_MarkForName", markForFirstName + " " + markForLastName);
                        _scWidgetUtils.showWidget(this, "extn_MarkForName", true);
                    }

                     else if(!_scBaseUtils.isVoid(markForFirstName) && _scBaseUtils.isVoid(markForLastName)){
                        _scWidgetUtils.setValue(this, "extn_MarkForName", markForFirstName);
                        _scWidgetUtils.showWidget(this, "extn_MarkForName", true);
                    }
                    else if(_scBaseUtils.isVoid(markForFirstName) && _scBaseUtils.isVoid(markForLastName)){
                       // _scWidgetUtils.setValue(this, "extn_MarkForName", markForFirstName);
                        _scWidgetUtils.hideWidget(this, "extn_MarkForName");
                    }

                      if(!_scBaseUtils.isVoid(billToFirstName) && !_scBaseUtils.isVoid(billToLastName)){
                        _scWidgetUtils.setValue(this, "extn_shipToName", billToFirstName + " " + billToLastName);
                        _scWidgetUtils.showWidget(this, "extn_shipToName", true);
                    }

                    else if(!_scBaseUtils.isVoid(billToFirstName) && _scBaseUtils.isVoid(billToLastName)){
                        _scWidgetUtils.setValue(this, "extn_shipToName", billToFirstName);
                        _scWidgetUtils.showWidget(this, "extn_shipToName", true);
                    } 
                    else if(_scBaseUtils.isVoid(billToFirstName) && _scBaseUtils.isVoid(billToLastName)){
                      //  _scWidgetUtils.setValue(this, "extn_shipToName", billToFirstName);
                        _scWidgetUtils.hideWidget(this, "extn_shipToName");
                    }
                        
                        // var billToFirstName1 = _scModelUtils.getStringValueFromPath("OrderLine.PersonInfoShipTo.FirstName", orderDetails);
                    
                    // }
                    //OrderList.LastOrderHeaderKey
                }
            }
	}
});
});

