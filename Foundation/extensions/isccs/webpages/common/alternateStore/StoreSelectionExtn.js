scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/common/alternateStore/StoreSelectionExtnUI", "scbase/loader!sc/plat/dojo/utils/WidgetUtils",
"scbase/loader!sc/plat/dojo/utils/ScreenUtils","scbase/loader!sc/plat/dojo/utils/GridxUtils","scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!sc/plat/dojo/utils/ModelUtils",
"scbase/loader!sc/plat/dojo/utils/ControllerUtils","scbase/loader!isccs/utils/UIUtils",
	"scbase/loader!isccs/utils/BaseTemplateUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnStoreSelectionExtnUI
			 ,
			 	_scWidgetUtils
			,
				_scScreenUtils
			,
				_scGridxUtils
			,
				_scBaseUtils
			,
				_scModelUtils
			,
				_scControllerUtils
			,
				_isccsUIUtils
			,
				_isccsBaseTemplateUtils

		     
){ 
	return _dojodeclare("extn.common.alternateStore.StoreSelectionExtn", [_extnStoreSelectionExtnUI],{
	// custom code here
		extn_Refresh:false,

	extn_gwpScreenInitialize:  function(event, bEvent, ctrl, args){
		
		var screenParams = null;
		screenParams =  _scScreenUtils.getInitialInputData(
        this);
		//_scScreenUtils.getScreenEditorInput(this);
		if(screenParams.gwpScreen == "Y"){
			sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
			var gwpGridList = screenParams.gwpList.ExtnPassthroughRewardsList;
			var screenInput = _scBaseUtils.getNewBeanInstance();

			
			var targetModel = null;
			targetModel = _scBaseUtils.getNewBeanInstance();
			var gwpItemList = null;

            gwpItemList = _scBaseUtils.getNewArrayInstance();
            var gwpArrayCheck = null;
            gwpArrayCheck = gwpGridList.length;

            if(!_scBaseUtils.isVoid(gwpArrayCheck)){
            	screenInput= {GwpGridList:{count:gwpGridList.length}};
            	for(var gwplist=0; gwplist<gwpGridList.length; gwplist++){
				_scWidgetUtils.showWidget(this, "extn_GWPGroup_contentpane"+gwplist, false,null);
					var gwpListWithOneItem =  _scBaseUtils.getNewArrayInstance();
					_scBaseUtils.appendToArray(gwpListWithOneItem, gwpGridList[gwplist].PassthroughRewards);
				if(_scBaseUtils.isVoid(gwpGridList[gwplist].PassthroughRewards.length)){
					_scModelUtils.addModelToModelPath( "GwpGridList.GwpList"+gwplist+".PassthroughRewards",gwpListWithOneItem,screenInput);
					_scModelUtils.setStringValueAtModelPath( "GwpGridList.GwpList"+gwplist+".ExtnMaxSelections",gwpGridList[gwplist].ExtnMaxSelections,screenInput);
					var gwpItem = _scBaseUtils.getNewBeanInstance();
				 	gwpItem.Name = "ItemID";
					gwpItem.Value = gwpGridList[gwplist].PassthroughRewards.GiftItemId;
					_scBaseUtils.appendToArray(gwpItemList, gwpItem);

				}else{
					_scModelUtils.addModelToModelPath( "GwpGridList.GwpList"+gwplist, gwpGridList[gwplist],screenInput);
				 	for(var gwpItemListCount=0; gwpItemListCount<gwpGridList[gwplist].PassthroughRewards.length; gwpItemListCount++){
						var gwpItem = _scBaseUtils.getNewBeanInstance();
				 		gwpItem.Name = "ItemID";
						gwpItem.Value = gwpGridList[gwplist].PassthroughRewards[gwpItemListCount].GiftItemId;
						_scBaseUtils.appendToArray(gwpItemList, gwpItem);
					}
				 
				 	
				 }
				 	
				}
            }else{
            	screenInput= {GwpGridList:{count:1}};
				_scWidgetUtils.showWidget(this, "extn_GWPGroup_contentpane"+0, false,null);
					var gwpListWithOneItem =  _scBaseUtils.getNewArrayInstance();
					_scBaseUtils.appendToArray(gwpListWithOneItem, gwpGridList.PassthroughRewards);
				if(_scBaseUtils.isVoid(gwpGridList.PassthroughRewards.length)){
					_scModelUtils.addModelToModelPath( "GwpGridList.GwpList"+0+".PassthroughRewards",gwpListWithOneItem,screenInput);
					_scModelUtils.setStringValueAtModelPath( "GwpGridList.GwpList"+0+".ExtnMaxSelections",gwpGridList.ExtnMaxSelections,screenInput);
					var gwpItem = _scBaseUtils.getNewBeanInstance();
				 	gwpItem.Name = "ItemID";
					gwpItem.Value = gwpGridList.PassthroughRewards.GiftItemId;
					_scBaseUtils.appendToArray(gwpItemList, gwpItem);

				}else{
					_scModelUtils.addModelToModelPath( "GwpGridList.GwpList"+0, gwpGridList,screenInput);
				 	for(var gwpItemListCount=0; gwpItemListCount<gwpGridList.PassthroughRewards.length; gwpItemListCount++){
						var gwpItem = _scBaseUtils.getNewBeanInstance();
				 		gwpItem.Name = "ItemID";
						gwpItem.Value = gwpGridList.PassthroughRewards[gwpItemListCount].GiftItemId;
						_scBaseUtils.appendToArray(gwpItemList, gwpItem);
					}
				 
				 	
				 }

            }
			
//            targetModel.Item.ComplexQuery.Or.Exp.Name = "ItemID";
//             targetModel.Item.ComplexQuery.Or.Exp.Value = "103";
			 _scModelUtils.addListToModelPath("Item.ComplexQuery.Or.Exp",gwpItemList,targetModel);
			var Locale=sc.plat.dojo.utils.PlatformUIFmkImplUtils.getLocaleCode(); 
			_scModelUtils.setStringValueAtModelPath("Item.DisplayLocalizedFieldInLocale",Locale,targetModel);
//			_scModelUtils.setStringValueAtModelPath("Item.ComplexQuery.Operator","AND",targetModel);
//			_scModelUtils.setStringValueAtModelPath("Item.ComplexQuery.Or.Exp.Name","ItemID",targetModel);
			_isccsUIUtils.callApi(this, targetModel, "extn_getGWPItems", null);
			_scScreenUtils.setModel(this, "screenInput", screenInput, null);
			_scWidgetUtils.hideWidget(
            	this, "pnlAddressSelect");
			_scWidgetUtils.hideWidget(
            	this, "pnlResults");	
			var screenInput = _scScreenUtils.getModel(this, "screenInput");
		}
	},
	
	extn_checkForGWP: function(event, bEvent, ctrl, args){
	
			var selectedSourceRecordsList = _scBaseUtils.getNewArrayInstance();
			var screenInput = _scScreenUtils.getModel(this, "screenInput");
			screenParams =  _scScreenUtils.getInitialInputData(this);
		
		if(screenParams.gwpScreen == "Y"){
			
			var noOfGrids = screenInput.GwpGridList.count;
			var checkOfSelection = true;
			for(var count=0; count<noOfGrids;count++){
				var selectedList = _scGridxUtils.getSelectedSourceRecordsWithRowIndex(this, "extn_gwp_gridx"+count);
				if(selectedList.length <= _scModelUtils.getStringValueFromPath("GwpGridList.GwpList"+count+".ExtnMaxSelections",screenInput)){
					var selectedGWPItems = _scGridxUtils.getSelectedSourceRecordsWithRowIndex(this, "extn_gwp_gridx"+count);
					for(var selectedCount=0; selectedCount<selectedGWPItems.length;selectedCount++){
						_scBaseUtils.appendToArray(selectedSourceRecordsList,selectedGWPItems[selectedCount].rowData);
					}
					

				}else{
					checkOfSelection = false;
				}
			}
           	if(checkOfSelection){
           		var selectedGWPList = _scBaseUtils.getNewArrayInstance();
           		var InvalidGWPList = _scBaseUtils.getNewArrayInstance();
           		var selectedGWPListArray = _scBaseUtils.getNewArrayInstance();
           		var allGWPlist = _scScreenUtils.getModel(this, "extn_getGWPItemList_Output");
           		for(var allGWPCount= 0;allGWPCount< allGWPlist.ItemList.Item.length;allGWPCount++){
           			for(var selectedGWPCount= 0;selectedGWPCount< selectedSourceRecordsList.length;selectedGWPCount++){
           				if(allGWPlist.ItemList.Item[allGWPCount].ItemID == selectedSourceRecordsList[selectedGWPCount].GiftItemId){
           					_scBaseUtils.appendToArray(selectedGWPList,allGWPlist.ItemList.Item[allGWPCount]);
           					_scBaseUtils.appendToArray(selectedGWPListArray,selectedSourceRecordsList[selectedGWPCount].GiftItemId);
           				}
           			}
           		}
           		for(var selectedGWPCount= 0;selectedGWPCount< selectedSourceRecordsList.length;selectedGWPCount++)
           				{
           				    if(!(_scBaseUtils.containsInArray(selectedGWPListArray,selectedSourceRecordsList[selectedGWPCount].GiftItemId)))
           				    {
           				    _scBaseUtils.appendToArray(InvalidGWPList,selectedSourceRecordsList[selectedGWPCount].GiftItemId);
           				    }
           				}
           				
           		
           		 if(_scBaseUtils.isVoid(InvalidGWPList))
                   {
				_scScreenUtils.setPopupOutput(this, selectedGWPList);
                    _scWidgetUtils.closePopup(
                    this, "APPLY", false);
                   }else
                   {
                    var Message = null;
                		Message = _scScreenUtils.getString(this, "extn_Product");
                    var GWPMsg = null;
                		GWPMsg = _scScreenUtils.getString(this, "extn_GWP_doesn't_exist");
   
                      for(var i in InvalidGWPList)  
                      {
                           Message=_scBaseUtils.stringConcat(Message,InvalidGWPList[i]); 
                           Message=_scBaseUtils.stringConcat(Message," "); 
                      }
                      
                              Message=_scBaseUtils.stringConcat(Message,GWPMsg);
                           
                       _isccsBaseTemplateUtils.showMessage(this,Message, "error", null);   
                   }
				
           	} else {
					var sMessage = null;
                		sMessage = _scScreenUtils.getString(
               		 this, "extn_Invalid_Selection");
				_isccsBaseTemplateUtils.showMessage(this,sMessage,"error");			}
		}
			
	},
	extn_getTextForGrid: function(event, bEvent, ctrl, args){
	    
	        var sMessage1 = null;
                sMessage1 = _scScreenUtils.getString(
                this, "extn_Select_Any");
             var sMessage2 = null;
                sMessage2 = _scScreenUtils.getString(
                this, "extn_Below_Items");
		return sMessage1 +event+sMessage2;
	},
	
	extn_handleGWPItemList: function(event, bEvent, ctrl, args){
		var mashupArrayObj = null;
		mashupArrayObj = args.mashupArray;
		var mashupOutput = null;
		for(var obj in mashupArrayObj){
			if((mashupArrayObj[obj].mashupRefId == "extn_getGWPItems")){
				mashupOutput = mashupArrayObj[obj].mashupRefOutput;
                _scScreenUtils.setModel(
                this, "extn_getGWPItemList_Output", mashupOutput, null);
				var screenInput = _scScreenUtils.getModel(this, "screenInput");
				for(var count=0; count<screenInput.GwpGridList.count;count++){
					_scGridxUtils.refreshGridUsingUId(this, "extn_gwp_gridx"+count);
				}
			}
		}
	},
	extn_getGWPItemDescription: function(event, bEvent, ctrl, args){
       	var gwpItemListObj = _scScreenUtils.getModel(this, "extn_getGWPItemList_Output");
		if(_scBaseUtils.isVoid(gwpItemListObj)){
			return;
		}
		for(var count in gwpItemListObj.ItemList.Item){
       		if(gwpItemListObj.ItemList.Item[count].ItemID == args.GiftItemId){
       			return gwpItemListObj.ItemList.Item[count].PrimaryInformation.ShortDescription + " (" + args.GiftItemId+")";
       		}
              }

	}

	
});
});
