
scDefine(["dojo/text!./templates/StoreSelectionExtn.html","scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/_base/lang","scbase/loader!dojo/text","scbase/loader!extn/utils/CustomUtils","scbase/loader!gridx/Grid","scbase/loader!gridx/modules/ColumnResizer","scbase/loader!gridx/modules/ColumnWidth","scbase/loader!gridx/modules/HLayout","scbase/loader!gridx/modules/HScroller","scbase/loader!gridx/modules/IndirectSelect","scbase/loader!gridx/modules/RowHeader","scbase/loader!gridx/modules/select/Row","scbase/loader!idx/layout/ContentPane","scbase/loader!sc/plat","scbase/loader!sc/plat/dojo/binding/CurrencyDataBinder","scbase/loader!sc/plat/dojo/binding/GridxDataBinder","scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!sc/plat/dojo/widgets/Label"]
 , function(			 
			    templateText
			 ,
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojolang
			 ,
			    _dojotext
			 ,
			    _extnCustomUtils
			 ,
			    _gridxGrid
			 ,
			    _gridxColumnResizer
			 ,
			    _gridxColumnWidth
			 ,
			    _gridxHLayout
			 ,
			    _gridxHScroller
			 ,
			    _gridxIndirectSelect
			 ,
			    _gridxRowHeader
			 ,
			    _gridxRow
			 ,
			    _idxContentPane
			 ,
			    _scplat
			 ,
			    _scCurrencyDataBinder
			 ,
			    _scGridxDataBinder
			 ,
			    _scBaseUtils
			 ,
			    _scLabel
){
return _dojodeclare("extn.common.alternateStore.StoreSelectionExtnUI",
				[], {
			templateString: templateText
	
	
	
	
	
	
					,	
	namespaces : {
		targetBindingNamespaces :
		[
		],
		sourceBindingNamespaces :
		[
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_7'
						,
	  value: 'extn_getGWPItemList_Output'
						
			}
			
		]
	}

	

	,
	hotKeys: [ 
	]

,events : [
	]

,subscribers : {
local : [

{
	  eventId: 'afterScreenInit'

,	  sequence: '19'




,handler : {
methodName : "extn_gwpScreenInitialize"

 
}
}
,
{
	  eventId: 'Popup_btnNext_onClick'

,	  sequence: '19'




,handler : {
methodName : "extn_checkForGWP"

 
}
}
,
{
	  eventId: 'onExtnMashupCompletion'

,	  sequence: '51'




,handler : {
methodName : "extn_handleGWPItemList"

 
}
}

]
					 	 ,
}

});
});


