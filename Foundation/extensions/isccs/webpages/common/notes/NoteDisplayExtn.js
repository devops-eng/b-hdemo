scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/common/notes/NoteDisplayExtnUI",
"scbase/loader!sc/plat/dojo/utils/ModelUtils",
"scbase/loader!sc/plat/dojo/utils/BaseUtils"
]
,
function(			 
			    _dojodeclare
			 ,
			    _extnNoteDisplayExtnUI
			,
				_scModelUtils
			,
				_scBaseUtils
){ 
	return _dojodeclare("extn.common.notes.NoteDisplayExtn", [_extnNoteDisplayExtnUI],{
	// custom code here
        defaultInput: function(
        inputModel) {
            var newInputModel = null;
            newInputModel = _scModelUtils.createNewModelObjectWithRootKey("Note");
            if (!(
            _scBaseUtils.isVoid(
            _scModelUtils.getStringValueFromPath("Note.OrderLineKey", inputModel)))) {
                _scModelUtils.setStringValueAtModelPath("Note.OrderLineKey", _scModelUtils.getStringValueFromPath("Note.OrderLineKey", inputModel), newInputModel);
                _scModelUtils.setBooleanValueAtModelPath("Note.OrderLineNote", true, newInputModel);
                if (!(
                _scBaseUtils.isVoid(
                _scModelUtils.getStringValueFromPath("Note.OrderHeaderKey", inputModel)))) {
                    _scModelUtils.setStringValueAtModelPath("Note.OrderHeaderKey", _scModelUtils.getStringValueFromPath("Note.OrderHeaderKey", inputModel), newInputModel);
                }
            } else if (!(
            _scBaseUtils.isVoid(
            _scModelUtils.getStringValueFromPath("Note.OrderHeaderKey", inputModel)))) {
                _scModelUtils.setStringValueAtModelPath("Note.OrderHeaderKey", _scModelUtils.getStringValueFromPath("Note.OrderHeaderKey", inputModel), newInputModel);
                _scModelUtils.setBooleanValueAtModelPath("Note.OrderNote", true, newInputModel);
            }
            var ExcludeList = null;
            ExcludeList = _scBaseUtils.getNewArrayInstance();
/*   Custom Commenting
         _scBaseUtils.appendToArray(
            ExcludeList, "SystemNote");
            _scBaseUtils.appendToArray(
            ExcludeList, "Priority");
*/
            _scBaseUtils.setAttributeValue("Exclude", ExcludeList, _scModelUtils.getModelObjectFromPath("Note", newInputModel));
            return newInputModel;
        }
});
});

