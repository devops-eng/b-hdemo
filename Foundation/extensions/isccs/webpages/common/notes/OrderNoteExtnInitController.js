


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/common/notes/OrderNoteExtn","scbase/loader!sc/plat/dojo/controller/ExtnScreenController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnOrderNoteExtn
			 ,
			    _scExtnScreenController
){

return _dojodeclare("extn.common.notes.OrderNoteExtnInitController", 
				[_scExtnScreenController], {

			
			 screenId : 			'extn.common.notes.OrderNoteExtn'

			
			
			
}
);
});

