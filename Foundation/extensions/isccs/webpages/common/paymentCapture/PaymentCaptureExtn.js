scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/common/paymentCapture/PaymentCaptureExtnUI",
"scbase/loader!sc/plat/dojo/utils/WidgetUtils","scbase/loader!sc/plat/dojo/utils/ScreenUtils",
"scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!sc/plat/dojo/utils/ModelUtils","scbase/loader!sc/plat/dojo/utils/EditorUtils",
"scbase/loader!isccs/utils/UIUtils","scbase/loader!isccs/utils/BaseTemplateUtils","scbase/loader!isccs/utils/ModelUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnPaymentCaptureExtnUI
			 ,  
			    _scWidgetUtils
             ,
                _scScreenUtils
             ,  
                _scBaseUtils
             ,
                _scModelUtils
             ,
                _scEditorUtils
			 ,
			    isccsUIUtils
			 ,
			    _isccsBaseTemplateUtils
		   ,_isccsModelUtils
){ 
	return _dojodeclare("extn.common.paymentCapture.PaymentCaptureExtn", [_extnPaymentCaptureExtnUI],{
       

	disableAgainstCustomer : function(event,bEvent,ctrl,args){
		_scWidgetUtils.disableWidget(this,"chkSaveAgainstCustomer",false);
        _scWidgetUtils.hideWidget(this,"chkSaveAgainstCustomer",false);
	},
    
	
	extn_validateGiftCard:function(event, bEvent, ctrl, args) {
	var paymentListModel = _scScreenUtils.getModel(this,"getPaymentTypeList_Output");
	var paymentCaptureModel = _scBaseUtils.getTargetModel(this, "PaymentCapture_input", null);
    var editorInputdata =  _scScreenUtils.getInitialInputData(_scEditorUtils.getCurrentEditor());
	var paymentType = paymentCaptureModel.PaymentMethod.PaymentType;
		if(paymentType == "LTGC" || paymentType == "HBGC")
		{
			sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
			var InvokeUEListModel = _scBaseUtils.getNewModelInstance();
			_scModelUtils.setStringValueAtModelPath("InvokeUE.EnterpriseCode",editorInputdata.Order.EnterpriseCode,InvokeUEListModel);
			_scModelUtils.setStringValueAtModelPath("InvokeUE.XMLData.PaymentMethod.DocumentType",editorInputdata.Order.DocumentType,InvokeUEListModel);
			_scModelUtils.setStringValueAtModelPath("InvokeUE.XMLData.PaymentMethod.EnterpriseCode",editorInputdata.Order.EnterpriseCode,InvokeUEListModel);
			_scModelUtils.setStringValueAtModelPath("InvokeUE.XMLData.PaymentMethod.MaxChargeLimit",paymentCaptureModel.PaymentMethod.RequestedAmount,InvokeUEListModel);
			_scModelUtils.setStringValueAtModelPath("InvokeUE.XMLData.PaymentMethod.OrderHeaderKey",editorInputdata.Order.OrderHeaderKey,InvokeUEListModel);
			_scModelUtils.setStringValueAtModelPath("InvokeUE.XMLData.PaymentMethod.OrderNo",editorInputdata.Order.OrderNo,InvokeUEListModel);
			_scModelUtils.setStringValueAtModelPath("InvokeUE.XMLData.PaymentMethod.PaymentType",paymentType,InvokeUEListModel);
			_scModelUtils.setStringValueAtModelPath("InvokeUE.XMLData.PaymentMethod.PaymentReference1",paymentCaptureModel.PaymentMethod.PaymentReference1,InvokeUEListModel);
			_scModelUtils.setStringValueAtModelPath("InvokeUE.XMLData.PaymentMethod.PaymentReference2",paymentCaptureModel.PaymentMethod.PaymentReference2,InvokeUEListModel);
			_scModelUtils.setStringValueAtModelPath("InvokeUE.XMLData.PaymentMethod.PaymentReference3",paymentCaptureModel.PaymentMethod.PaymentReference3,InvokeUEListModel);
			_scModelUtils.setStringValueAtModelPath("InvokeUE.XMLData.PaymentMethod.SvcNo",paymentCaptureModel.PaymentMethod.SvcNo,InvokeUEListModel);
			_scModelUtils.setStringValueAtModelPath("InvokeUE.XMLData.PaymentMethod.TokenizedSvcNo","",InvokeUEListModel);
			_scModelUtils.setStringValueAtModelPath("InvokeUE.XMLData.PaymentMethod.TempPaymentReference1","",InvokeUEListModel);
			isccsUIUtils.callApi(this,InvokeUEListModel,"extn_GiftCardMashup",args);
		}
	},
	

    // eventId:  'afterScreenInit'
   // To remove the 'Paypal' Payment Type from the Payment Type drop down list on click of AddPaymentMethod button

     extn_initDisablePaypal:function(event, bEvent, ctrl, args) {
	var paymentListModel = _scScreenUtils.getModel(this,"getPaymentTypeList_Output");
    var commonCodeModel = _scScreenUtils.getModel(this,"extn_disablePayment_ns");
    var paymentModel = null;
    var paymentTypeList= _scModelUtils.getModelListFromPath("PaymentTypeList.PaymentType", paymentListModel);
    paymentType = _scBaseUtils.getNewArrayInstance();
    var paymentListArray= _scBaseUtils.getNewArrayInstance();
	var paymentTypeModel = _scBaseUtils.getNewBeanInstance();
	var paymentTypeListModel = _scBaseUtils.getNewModelInstance();
	var paymentIndex = null;
    var commonCodes = _scModelUtils.getModelListFromPath("CommonCodeList.CommonCode",commonCodeModel);
    for(var i in commonCodes)
    {
        var paymentType = commonCodes[i].CodeValue;
		for(var index in paymentTypeList)
		{
			if(paymentTypeList[index].PaymentType == paymentType)
			{
				paymentIndex = index;
				break;
			} 
		}
    }
	for(var index in paymentTypeList)
	{
		if(index==paymentIndex)
		{
			continue;
		} 
		paymentType=  _scBaseUtils.getArrayItemByIndex(paymentTypeList,index);
       _scBaseUtils.appendToArray(paymentListArray,paymentType);
	}
		//_scBaseUtils.setAttributeValue("PaymentTypeList.PaymentType",paymentListArray,model1);
        _scBaseUtils.addArrayValueToBean("PaymentType",paymentListArray ,paymentTypeModel);
        _scBaseUtils.setAttributeValue("PaymentTypeList",paymentTypeModel,paymentTypeListModel);
        _scBaseUtils.setModel(this,"getPaymentTypeList_Output",paymentTypeListModel,null);
	}, 

	
       
	onApply: function(event, bEvent, ctrl, args){
            var targetModel = null;
            var paymentTypeGroup = null;
            var errorMessage = null;
			
            if (_scWidgetUtils.isWidgetVisible(this, "pnlSavedPaymentMethods")) 
			{
                var selectedCustomerPaymentMethodList = null;
                selectedCustomerPaymentMethodList = _scGridxUtils.getSelectedSourceRecordsUsingUId(this, "tblSavedPaymentMethods");
                var customerPaymentMethods = null;
                customerPaymentMethods = _scModelUtils.getModelListFromPath("CustomerPaymentMethodList.CustomerPaymentMethod",selectedCustomerPaymentMethodList);
                targetModel = _scModelUtils.createNewModelObjectWithRootKey("PaymentMethod");
                _scModelUtils.addModelToModelPath("PaymentMethod", _scModelUtils.getModelFromList(customerPaymentMethods, 0), targetModel);
                paymentTypeGroup = _scModelUtils.getStringValueFromPath("PaymentMethod.PaymentTypeGroup", targetModel);
                if (_scBaseUtils.equals(paymentTypeGroup, "OTHER")) {
                    var paymentReference2 = null;
                    paymentReference2 = _scModelUtils.getStringValueFromPath("PaymentMethod.PaymentReference2", targetModel);
                    if (_scBaseUtils.isVoid(paymentReference2)) {
                        _scModelUtils.setStringValueAtModelPath("PaymentMethod.RequestedAmount", "0", targetModel);
                    } else {
                        _scModelUtils.setStringValueAtModelPath("PaymentMethod.RequestedAmount", paymentReference2, targetModel);
                    }
                }
                _isccsModelUtils.removeAttributeFromModel("PaymentMethod.IsDefaultMethod", targetModel);
                _isccsModelUtils.removeAttributeFromModel("PaymentMethod.PaymentTypeDesc", targetModel);
                _isccsModelUtils.removeAttributeFromModel("PaymentMethod.CreditCardTypeDesc", targetModel);
                _isccsModelUtils.removeAttributeFromModel("PaymentMethod.PaymentTypeGroup", targetModel);
            } 
			else {
                targetModel = _scBaseUtils.getTargetModel(
                this, "PaymentCapture_input", null);
                paymentTypeGroup = _scModelUtils.getStringValueFromPath("PaymentMethod.PaymentTypeGroup", targetModel);
                if (_scBaseUtils.equals(paymentTypeGroup, "CREDIT_CARD")) 
				{
                    var creditCardExpMonth = null;
                    creditCardExpMonth = _scModelUtils.getStringValueFromPath("PaymentMethod.CreditCardExpMonth", targetModel);
                    var creditCardExpYear = null;
                    creditCardExpYear = _scModelUtils.getStringValueFromPath("PaymentMethod.CreditCardExpYear", targetModel);
                    if ( _scBaseUtils.equals(creditCardExpMonth, "**")) {
                        if (!(_scBaseUtils.equals(creditCardExpYear, "****"))) {
                            errorMessage = _scScreenUtils.getString(this, "MonthYearErrorMessage");
                            _isccsBaseTemplateUtils.showMessage(this, errorMessage, "error", null);
                            return;
                        }
                    }
                    if (_scBaseUtils.equals(creditCardExpYear, "****")) {
                        if (!(_scBaseUtils.equals(creditCardExpMonth, "**"))) {
                            errorMessage = _scScreenUtils.getString(this, "MonthYearErrorMessage");
                            _isccsBaseTemplateUtils.showMessage(this, errorMessage, "error", null);
                            return;
                        }
                    }
                    var creditCardToken = null;
                    creditCardToken = _scModelUtils.getStringValueFromPath("PaymentMethod.CreditCardNo", targetModel);
                    var creditCardDisplay = null;
                    creditCardDisplay = _scModelUtils.getStringValueFromPath("PaymentMethod.DisplayCreditCardNo", targetModel);
                    if (_scBaseUtils.and(_scBaseUtils.isVoid(creditCardToken), _scBaseUtils.isVoid(creditCardDisplay))) {
                        errorMessage = _scScreenUtils.getString(this, "CreditCardVoidMessage");
                        _isccsBaseTemplateUtils.showMessage(this, errorMessage, "error", null);
                        return;
                    }
                }
                if (_scBaseUtils.equals(paymentTypeGroup, "STORED_VALUE_CARD")) {
                    var svcCardToken = null;
                    svcCardToken = _scModelUtils.getStringValueFromPath("PaymentMethod.SvcNo", targetModel);
                    var svcCardDisplay = null;
                    svcCardDisplay = _scModelUtils.getStringValueFromPath("PaymentMethod.DisplaySvcNo", targetModel);
                    if (_scBaseUtils.and(_scBaseUtils.isVoid(svcCardToken), _scBaseUtils.isVoid(svcCardDisplay))) {
                        errorMessage = _scScreenUtils.getString(this, "SVCCardVoidMessage");
                        _isccsBaseTemplateUtils.showMessage(this, errorMessage, "error", null);
                        return;
                    }
		      var pin =  _scModelUtils.getStringValueFromPath("PaymentMethod.PaymentReference1", targetModel);
                    if (_scBaseUtils.isVoid(pin)) {
                        errorMessage = _scScreenUtils.getString(this, "extn_Pin_GC_Mandatory");
                        _isccsBaseTemplateUtils.showMessage(this, errorMessage, "error", null);
                        return;
                    }
                    var output= args.mashupArray;
			        for(var i in output)
			        {
				          if(output[i].mashupRefId == "extn_GiftCardMashup")
				          {
					        var mashupRefOutputObj = output[i].mashupRefOutput;
					        var strFundsAvailable = mashupRefOutputObj.InvokeUE.XMLData.PaymentMethod.FundsAvailable;
					        if(strFundsAvailable == "0.00"){
					            _isccsBaseTemplateUtils.showMessage(this, "No Funds Available", "error", null);
					           return;
					        }else if(_scBaseUtils.isVoid(strFundsAvailable)){
					            _isccsBaseTemplateUtils.showMessage(this, "Funds Available Service is Down", "error", null);
					            return;
					        }
					            
				          }
			        }	
					_scModelUtils.setStringValueAtModelPath("PaymentMethod.FundsAvailable", strFundsAvailable, targetModel);
                }
                if (_scBaseUtils.equals(this.updatedAddress, true)) {
                    var updatedAddressModel = null;
                    updatedAddressModel = _scScreenUtils.getModel(
                    this, "updatedAddress_Output");
                    updatedAddressModel = _scModelUtils.getModelObjectFromPath("PersonInfo", updatedAddressModel);
                    _isccsModelUtils.removeAttributeFromModel("isHistory", updatedAddressModel);
                    _isccsModelUtils.removeAttributeFromModel("Lockid", updatedAddressModel);
                    _isccsModelUtils.removeAttributeFromModel("UseCount", updatedAddressModel);
                    _isccsModelUtils.removeAttributeFromModel("CountryDesc", updatedAddressModel);
                    _isccsModelUtils.removeAttributeFromModel("TitleDesc", updatedAddressModel);
                    _scModelUtils.addModelToModelPath("PaymentMethod.PersonInfoBillTo", updatedAddressModel, targetModel);
                }
            }
            _scScreenUtils.setPopupOutput(this, targetModel);
            _scWidgetUtils.closePopup(this, "APPLY", false);
    },
	
      
	extn_FundsAvailableMashupCompletion:function(event, bEvent, ctrl, args) {
	this.onApply(null,null,null,args);
	},

  handleMashupCompletion: function(
        mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data) {
            _isccsBaseTemplateUtils.handleMashupCompletion(
            mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data, this);
        }

});
});
