
scDefine(["dojo/text!./templates/PaymentCaptureExtn.html","scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/_base/lang","scbase/loader!dojo/text","scbase/loader!idx/form/CheckBox","scbase/loader!idx/form/FilteringSelect","scbase/loader!sc/plat","scbase/loader!sc/plat/dojo/binding/CheckBoxDataBinder","scbase/loader!sc/plat/dojo/binding/ComboDataBinder","scbase/loader!sc/plat/dojo/utils/BaseUtils"]
 , function(			 
			    templateText
			 ,
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojolang
			 ,
			    _dojotext
			 ,
			    _idxCheckBox
			 ,
			    _idxFilteringSelect
			 ,
			    _scplat
			 ,
			    _scCheckBoxDataBinder
			 ,
			    _scComboDataBinder
			 ,
			    _scBaseUtils
){
return _dojodeclare("extn.common.paymentCapture.PaymentCaptureExtnUI",
				[], {
			templateString: templateText
	
	
	
	
	
	
					,	
	namespaces : {
		targetBindingNamespaces :
		[
		],
		sourceBindingNamespaces :
		[
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_6'
						,
	  description: 'extn_disablePayment_ns'
						,
	  value: 'extn_disablePayment_ns'
						
			}
			
		]
	}

	

	,
	hotKeys: [ 
	]

,events : [
	]

,subscribers : {
local : [

{
	  eventId: 'cmbPaymentType_onChange'

,	  sequence: '51'




,handler : {
methodName : "disableAgainstCustomer"

 
}
}
,
{
	  eventId: 'afterScreenInit'

,	  sequence: '51'




,handler : {
methodName : "disableAgainstCustomer"

 
}
}
,
{
	  eventId: 'afterScreenInit'

,	  sequence: '52'

,	  description: 'extn_initDisablePaypal'



,handler : {
methodName : "extn_initDisablePaypal"

 
}
}
,
{
	  eventId: 'Popup_btnNext_onClick'

,	  sequence: '19'

,	  description: 'Validate Gift Card'



,handler : {
methodName : "extn_validateGiftCard"

 
}
}
,
{
	  eventId: 'afterBehaviorMashupCall'

,	  sequence: '51'




,handler : {
methodName : "extn_FundsAvailableMashupCompletion"

 
}
}

]
					 	 ,
}

});
});


