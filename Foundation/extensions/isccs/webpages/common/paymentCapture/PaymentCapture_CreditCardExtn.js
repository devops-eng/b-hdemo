scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/common/paymentCapture/PaymentCapture_CreditCardExtnUI",
    "scbase/loader!isccs/utils/UIUtils","scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!sc/plat/dojo/utils/ModelUtils",
    "scbase/loader!sc/plat/dojo/utils/ScreenUtils","scbase/loader!isccs/utils/ModelUtils","scbase/loader!sc/plat/dojo/utils/EditorUtils"]
,
function(    
       _dojodeclare
    ,
    _extnPaymentCapture_CreditCardExtnUI
    ,
    _isccsUIUtils
    ,
    _scBaseUtils
    ,
    _scModelUtils
    ,
    _scScreenUtils
    ,
    _isccsModelUtils
    ,_scEditorUtils
   
   
){ 
 return _dojodeclare("extn.common.paymentCapture.PaymentCapture_CreditCardExtn", [_extnPaymentCapture_CreditCardExtnUI],{
// custom code here

  // event id : 'afterScreenInit
 //  Description : To validate the payment type entered and display the respective card types
  extn_paymentType:function(event, bEvent, ctrl, args)
    {
        var parentScreenData=null;
        parentScreenData = _isccsUIUtils.getParentScreen(this, true);
        var paymentCaptureModel=null;
        PaymentCaptureModel = _scBaseUtils.getTargetModel(parentScreenData, "PaymentCapture_input", null);
        var paymentType = null;
        paymentType= _scModelUtils.getStringValueFromPath("PaymentMethod.PaymentType", PaymentCaptureModel);
         if(_scBaseUtils.equals("CREDIT_CARD_PVT_LT",paymentType))
        {
             sc.plat.dojo.utils.WidgetUtils.setWidgetNonMandatory(this,"cmbMonth");
             sc.plat.dojo.utils.WidgetUtils.setWidgetNonMandatory(this,"cmbYear");
        }
                var CardTypes= _scScreenUtils.getModel(this, "extn_CardTypes");
                if(_scBaseUtils.isVoid(CardTypes))
                {
                    var editorInputdata =  _scScreenUtils.getInitialInputData(_scEditorUtils.getCurrentEditor());

                   var apiInput=_scBaseUtils.getNewModelInstance();
                   			_scModelUtils.setStringValueAtModelPath("CommonCode.OrganizationCode",editorInputdata.Order.EnterpriseCode,apiInput);

                     _isccsUIUtils.callApi(this, apiInput, "extn_getCardTypes_CreditCard");
                }
                else
                {

      //  var commonCodeOutput = _scScreenUtils.getModel(this, "getPaymentCardTypeList_Output");
        //var modelList = _scModelUtils.getModelListFromPath("CommonCodeList.CommonCode", commonCodeOutput);
                                var modelList = _scModelUtils.getModelListFromPath("PaymentCardTypeList.PaymentCardType", CardTypes);
        if(_scBaseUtils.equals("CREDIT_CARD_LT",paymentType) || _scBaseUtils.equals("CREDIT_CARD_HBC",paymentType))
        {
            for(var i=0; i<modelList.length; i++)
            {
                var value = modelList[i];
                //var codeValue1 = _scModelUtils.getStringValueFromPath("CodeValue", value);
               var codeValue1 = _scModelUtils.getStringValueFromPath("PaymentCardType", value);
                //var orgCode = _scModelUtils.getStringValueFromPath("OrganizationCode", value);
                if(_scBaseUtils.equals("LT", codeValue1) || _scBaseUtils.contains(codeValue1,"HBC"))
                {
                  _isccsModelUtils.removeItemFromArray(modelList, value);
                    i=-1;
                }
            }
        }
        else
        {
            for(var i=0; i<modelList.length; i++)
            {
                var value = modelList[i];
                //var codeValue1 = _scModelUtils.getStringValueFromPath("CodeValue", value);
                 var codeValue1 = _scModelUtils.getStringValueFromPath("PaymentCardType", value);
                //var orgCode = _scModelUtils.getStringValueFromPath("OrganizationCode", value);
                if(!(_scBaseUtils.equals("LT", codeValue1) || _scBaseUtils.contains(codeValue1,"HBC")))
                {
                    _isccsModelUtils.removeItemFromArray(modelList, value);
                    i=-1;
                }
            }
        }                                                                                                                              
        _scScreenUtils.setModel(this, "getPaymentCardTypeList_Output", CardTypes, false); 
        }
    },


  // event id : afterBehaviorMashupCall
  // Description : To get the values of different card types

   extn_afterBehaviorMashupCall:function(event, bEvent, ctrl, args)
	{
		//alert("extn_afterBehaviorMashupCall");
		var MashupList=args.mashupArray;
		for(var i in MashupList)
		{
            var OrderLineElement = MashupList[i];
            var mashupRefId=_scModelUtils.getStringValueFromPath("mashupRefId", OrderLineElement);
			if (_scBaseUtils.equals(mashupRefId, "extn_getCardTypes_CreditCard")) 
               {
                   var CommonCode=OrderLineElement.mashupRefOutput.CommonCodeList.CommonCode;
                   var NameSpace=_scBaseUtils.getNewArrayInstance();
                   for(var i=0; i<CommonCode.length; i++)
                    {
                        var value = CommonCode[i];
                        var apiInput=_scBaseUtils.getNewModelInstance();
                        _scModelUtils.setStringValueAtModelPath("PaymentCardType",value.CodeValue,apiInput);
                            _scModelUtils.setStringValueAtModelPath("ShortDescription",value.CodeShortDescription,apiInput);
                   _scBaseUtils.appendToArray(NameSpace,apiInput);

                          
                    }
                    var NameSpaceModel=_scBaseUtils.getNewModelInstance();
                   _scModelUtils.setStringValueAtModelPath("PaymentCardTypeList.PaymentCardType",NameSpace,NameSpaceModel);

                    _scScreenUtils.setModel(this, "extn_CardTypes",NameSpaceModel, null); 
                          this.extn_paymentType();
               }
	}
	}
 
});
});
