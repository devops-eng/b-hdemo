
scDefine(["dojo/text!./templates/PaymentCapture_CreditCardExtn.html","scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/_base/lang","scbase/loader!dojo/text","scbase/loader!sc/plat","scbase/loader!sc/plat/dojo/utils/BaseUtils"]
 , function(			 
			    templateText
			 ,
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojolang
			 ,
			    _dojotext
			 ,
			    _scplat
			 ,
			    _scBaseUtils
){
return _dojodeclare("extn.common.paymentCapture.PaymentCapture_CreditCardExtnUI",
				[], {
			templateString: templateText
	
	
	
	
	
	
					,	
	namespaces : {
		targetBindingNamespaces :
		[
		],
		sourceBindingNamespaces :
		[
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_8'
						,
	  description: 'CardTypes'
						,
	  value: 'extn_CardTypes'
						
			}
			
		]
	}

	

	,
	hotKeys: [ 
	]

,events : [
	]

,subscribers : {
local : [

{
	  eventId: 'afterScreenInit'

,	  sequence: '51'




,handler : {
methodName : "extn_paymentType"

 
}
}
,
{
	  eventId: 'afterBehaviorMashupCall'

,	  sequence: '51'

,	  description: 'afterBehaviorMashupCall'



,handler : {
methodName : "extn_afterBehaviorMashupCall"

 
}
}

]
					 	 ,
}

});
});


