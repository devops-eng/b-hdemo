scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/common/paymentCapture/PaymentCapture_StoredValueExtnUI",
"scbase/loader!sc/plat/dojo/utils/WidgetUtils","scbase/loader!sc/plat/dojo/utils/ScreenUtils","scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!sc/plat/dojo/utils/ModelUtils"]
,
function(			 
			     _dojodeclare
			 ,
			     _extnPaymentCapture_StoredValueExtnUI
			 ,
			     _scWidgetUtils
			 ,
			     _scScreenUtils
			,
			     _scBaseUtils
			,
			     _scModelUtils
			
){ 
	return _dojodeclare("extn.common.paymentCapture.PaymentCapture_StoredValueExtn", [_extnPaymentCapture_StoredValueExtnUI],{

	extn_DisableCardNumber:function(event, bEvent, ctrl, args) {
		_scWidgetUtils.hideWidget(this,"padssHolder",false);
	},
	
	extn_hideCardNumber:function(event, bEvent, ctrl, args) {
	var paymentCaptureModel = _scScreenUtils.getModel(this,"PaymentCapture_output");
	if(paymentCaptureModel != null)
	{
	    var giftCardNumber = _scWidgetUtils.getValue(this,"extn_GiftCardNo");
	    var newPaymentMethodModel = _scBaseUtils.getNewModelInstance();
	    _scModelUtils.setStringValueAtModelPath("Order.PaymentMethod.SvcNo",giftCardNumber,newPaymentMethodModel);		
	    _scScreenUtils.setModel(this,"PaymentCapture_output",newPaymentMethodModel,null);
	}
    }
});
});

