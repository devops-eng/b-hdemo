/*
 * Licensed Materials - Property of IBM
 * IBM Call Center for Commerce (5725-P82)
 * (C) Copyright IBM Corp. 2013 All Rights Reserved.
 * US Government Users Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */
scDefine(["dojo/text!./templates/CancelPickupEditor.html", "scbase/loader!dojo/_base/declare", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat", "scbase/loader!sc/plat/dojo/binding/CurrencyDataBinder", "scbase/loader!sc/plat/dojo/binding/ImageDataBinder", "scbase/loader!sc/plat/dojo/layout/AdvancedTableLayout", "scbase/loader!sc/plat/dojo/widgets/Editor", "scbase/loader!sc/plat/dojo/widgets/Image", "scbase/loader!sc/plat/dojo/widgets/Label", "scbase/loader!sc/plat/dojo/widgets/Link", "scbase/loader!isccs/utils/BaseTemplateUtils", "scbase/loader!isccs/utils/EditorScreenUtils", "scbase/loader!isccs/utils/OrderUtils", "scbase/loader!isccs/utils/UIUtils", "scbase/loader!isccs/common/customer/BusinessCardInitController", "scbase/loader!isccs/editors/OrderEditorRTInitController", "scbase/loader!isccs/utils/EditorRelatedTaskUtils", "scbase/loader!isccs/utils/ItemUtils", "scbase/loader!isccs/utils/OrderUtils", "scbase/loader!isccs/utils/RelatedTaskUtils", "scbase/loader!isccs/utils/SharedComponentUtils", "scbase/loader!isccs/utils/WidgetUtils", "scbase/loader!sc/plat", "scbase/loader!sc/plat/dojo/binding/CurrencyDataBinder", "scbase/loader!sc/plat/dojo/binding/ImageDataBinder", "scbase/loader!sc/plat/dojo/layout/AdvancedTableLayout", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/ControllerUtils", "scbase/loader!sc/plat/dojo/utils/EditorUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/WizardUtils", "scbase/loader!sc/plat/dojo/widgets/ControllerWidget", "scbase/loader!sc/plat/dojo/widgets/IdentifierControllerWidget"], function(templateText, _dojodeclare, _idxContentPane, _scplat, _scCurrencyDataBinder, _scImageDataBinder, _scAdvancedTableLayout, _scEditor, _scImage, _scLabel, _scLink, _isccsBaseTemplateUtils, _isccsEditorScreenUtils, _isccsOrderUtils, _isccsUIUtils, _isccsBusinessCardInitController, _isccsOrderEditorRTInitController, _isccsEditorRelatedTaskUtils, _isccsItemUtils, _isccsOrderUtils, _isccsRelatedTaskUtils, _isccsSharedComponentUtils, _isccsWidgetUtils, _scplat, _scCurrencyDataBinder, _scImageDataBinder, _scAdvancedTableLayout, _scBaseUtils, _scControllerUtils, _scEditorUtils, _scEventUtils, _scModelUtils, _scScreenUtils, _scWidgetUtils, _scWizardUtils, _scControllerWidget, _scIdentifierControllerWidget) {
    return _dojodeclare("extn.customScreen.CancelPickupEditor", [_scEditor], {
        templateString: templateText,
        uId: "CancelPickupEditor",
        packageName: "extn.customScreen",
        className: "CancelPickupEditor",
        namespaces: {
            targetBindingNamespaces: [],
            sourceBindingNamespaces: [{
                value: 'InitialEditorInput',
                description: "Holds the initial editor input passed into the editor on open."
            }, {
                value: 'getOrganizationList_output',
                description: "Holds a list of organizations used to create a new order."
            }, {
                value: 'getEnterpriseList',
                description: "Holds a list of enterprises the user can access."
            }, {
                value: 'enterpriseContext',
                description: "Used to display the enterprise context information."
            }]
        },
        // comparisonAttributes are used to create a model object (JSON Object), which is used for 
        // comparing two different instances for the editor. If the model object that is created by using 
        // comparisonAttributes for two instances of the editor are similar, they are identified as the 
        // matching editor. This is mainly used to determine whether the new instance of the editor 
        // must be opened in a new tab or focused on the matching editor. 
        // This is used when using the sc.plat.dojo.utils.ControllerUtils.openScreenInEditor utility 
        isRTscreenLoaded: 'null',
        newScreenData: null,
        isRTscreeninitialized: 'null',
        isWizardinitialized: 'null',
        comparisonAttributes: ["Order.OrderHeaderKey"],
        hotKeys: [],
        events: [{
            name: 'setSystemMessage'
        }, {
            name: 'resizeEditor'
        }, {
            name: 'setEditorInput'
        }, {
            name: 'setScreenTitle'
        }, {
            name: 'createNewOrder'
        }, {
            name: 'createNewConsumer'
        }, {
            name: 'createNewBusiness'
        }, {
            name: 'beforeEditorClosed'
        }, {
            name: 'showOrHideRelatedTask'
        }, {
            name: 'afterRTScreenStartup'
        }, {
            name: 'openNote'
        }, {
            name: 'showNotesIcon'
        }, {
            name: 'openSummary'
        }, {
            name: 'updateScreenInEditor'
        }, {
            name: 'addToOrder'
        }, {
            name: 'updateRequiredInputValues'
        }],
        subscribers: {
            local: [{
                eventId: 'closeSystemMessage_onClick',
                sequence: '25',
                handler: {
                    methodName: "closeMessagePanel",
                    className: "BaseTemplateUtils",
                    packageName: "isccs.utils"
                }
            }, {
                eventId: 'setEditorInput',
                sequence: '25',
                handler: {
                    methodName: "setEditorInput"
                }
            }, {
                eventId: 'setScreenTitle',
                sequence: '25',
                handler: {
                    methodName: "setScreenTitle",
                    description: ""
                }
            }, {
                eventId: 'showOrHideRelatedTask',
                sequence: '25',
                handler: {
                    methodName: "showOrHideRelTask",
                    description: ""
                }
            }, {
                eventId: 'afterRTScreenStartup',
                sequence: '25',
                handler: {
                    methodName: "showRelatedTaskScreenHolder",
                    description: ""
                }
            }, {
                eventId: 'createNewOrder',
                sequence: '25',
                handler: {
                    methodName: "handleCreateNewOrder",
                    className: "EditorScreenUtils",
                    packageName: "isccs.utils",
                    description: ""
                }
            }, {
                eventId: 'createNewConsumer',
                sequence: '25',
                handler: {
                    methodName: "handleCreateNewCustomer",
                    className: "EditorScreenUtils",
                    packageName: "isccs.utils",
                    description: ""
                }
            }, {
                eventId: 'createNewBusiness',
                sequence: '25',
                handler: {
                    methodName: "handleCreateNewCustomer",
                    className: "EditorScreenUtils",
                    packageName: "isccs.utils",
                    description: ""
                }
            }, {
                eventId: 'beforeEditorClosed',
                sequence: '25',
                handler: {
                    methodName: "handleEditorClose",
                    className: "UIUtils",
                    packageName: "isccs.utils",
                    description: ""
                }
            }, {
                eventId: 'afterScreenInit',
                sequence: '25',
                handler: {
                    methodName: "onScreenInit"
                }
            }, {
                eventId: 'openNote',
                sequence: '30',
                handler: {
                    methodName: "openNotePanel"
                }
            }, {
                eventId: 'showNotesIcon',
                sequence: '30',
                handler: {
                    methodName: "updateScreenNotes"
                }
            }, {
                eventId: 'afterScreenInit',
                sequence: '30',
                handler: {
                    methodName: "initOrderEditor"
                }
            }, {
                eventId: 'updateScreenInEditor',
                sequence: '30',
                handler: {
                    methodName: "updateScreenInEditor"
                }
            }, {
                eventId: 'openSummary',
                sequence: '30',
                handler: {
                    methodName: "openOrderSummary",
                    className: "OrderUtils",
                    packageName: "isccs.utils"
                }
            }, {
                eventId: 'addToOrder',
                sequence: '30',
                handler: {
                    methodName: "handleAddToOrder",
                    className: "OrderUtils",
                    packageName: "isccs.utils"
                }
            }, {
                eventId: 'updateRequiredInputValues',
                sequence: '30',
                handler: {
                    methodName: "updateRequiredInputValues"
                }
            }, {
                eventId: 'linkclose_onClick',
                sequence: '30',
                description: '',
                listeningControlUId: 'linkclose',
                handler: {
                    methodName: "closeCustomerMessagePanel",
                    className: "BaseTemplateUtils",
                    packageName: "isccs.utils"
                }
            }, {
                eventId: 'siteMapLink_onClick',
                sequence: '30',
                description: '',
                listeningControlUId: 'siteMapLink',
                handler: {
                    methodName: "openSiteMap",
                    className: "EditorScreenUtils",
                    packageName: "isccs.utils",
                    description: ""
                }
            }],
        },
        updateRequiredInputValues: function(event, bEvent, ctrl, args) {
            var screenInput = null;
            screenInput = _scScreenUtils.getInitialInputData(this);
            var apiInput = null;
            apiInput = _scModelUtils.createNewModelObjectWithRootKey("Order");
            _scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", _scModelUtils.getStringValueFromPath("Order.OrderHeaderKey", screenInput), apiInput);
            var mashupContext = null;
            if (!(_scBaseUtils.isVoid(_scModelUtils.getStringValueFromPath("Order.OrderLineKey", screenInput)))) {
                mashupContext = _scControllerUtils.getMashupContext(this);
                _scBaseUtils.setAttributeValue("OrderLineKey", _scModelUtils.getStringValueFromPath("Order.OrderLineKey", screenInput), mashupContext);
            }
            _isccsUIUtils.callApi(this, apiInput, "getRequiredEditorAttributes", mashupContext);
        },
        updateScreenInEditor: function(event, bEvent, ctrl, args) {
            var screenInput = null;
            screenInput = _scScreenUtils.getInitialInputData(this);
            if (!(_scBaseUtils.isVoid(_scModelUtils.getStringValueFromPath("Order.OrderHeaderKey", screenInput)))) {
                if (!(_isccsOrderUtils.hasRequiredOrderAttributes(screenInput))) {
                    this.updateRequiredInputValues(event, bEvent, ctrl, args);
                }
            }
        },
        initOrderEditor: function(event, bEvent, ctrl, args) {
            if (_scBaseUtils.isVoid(this.SkipRequiredAttrsCheck)) {
                this.updateScreenInEditor(event, bEvent, ctrl, args);
            }
        },
        beforeOpenScreenInEditor: function(data) {
            _isccsEditorScreenUtils.beforeOpenScreenInOrderCustomerEditor(data);
        },
        updateScreenNotes: function(event, bEvent, ctrl, args) {
            var eventDefn = null;
            eventDefn = _scBaseUtils.getNewBeanInstance();
            _scBaseUtils.setAttributeValue("argumentList", _scBaseUtils.getNewBeanInstance(), eventDefn);
            _scEventUtils.fireEventInsideScreen(_scWizardUtils.getCurrentPage(_isccsUIUtils.getCurrentWizardInstance(this)), "refreshNotes", eventDefn, args);
        },
        openNotePanel: function(event, bEvent, ctrl, args) {
            var currentScreen = null;
            var screenInput = null;
            currentScreen = _scEditorUtils.getScreenInstance(this);
            screenInput = _scScreenUtils.getInitialInputData(currentScreen);
            if (_scBaseUtils.isVoid(_scModelUtils.getStringValueFromPath("Order.OrderLineKey", screenInput))) {
                _isccsSharedComponentUtils.openNotesForOrderObject(this, screenInput, "AddViewOrderNotes");
            } else {
                _isccsSharedComponentUtils.openNotesForOrderObject(this, screenInput, "AddViewOrderLineNotes");
            }
        },
        massageFulfillmentInput: function(targetModel, targetBean) {
            _scModelUtils.setStringValueAtModelPath("Order.IsOpenedFrom", "CHANGE_FULFILLMENT_OPTIONS", targetModel);
            return targetModel;
        },
        massageOrderReturn: function(targetModel, targetBean) {
            var returnOrderInput = null;
            returnOrderInput = _scModelUtils.createNewModelObjectWithRootKey("Order");
            _scModelUtils.setStringValueAtModelPath("Order.EnterpriseCode", _scModelUtils.getStringValueFromPath("Order.EnterpriseCode", targetModel), returnOrderInput);
            _scModelUtils.setStringValueAtModelPath("Order.SellerOrganizationCode", _scModelUtils.getStringValueFromPath("Order.SellerOrganizationCode", targetModel), returnOrderInput);
            var sBillToID = "";
            sBillToID = _scModelUtils.getStringValueFromPath("Order.BillToID", targetModel);
            if (!(_scBaseUtils.isVoid(sBillToID))) {
                _scModelUtils.setStringValueAtModelPath("Order.BillToID", sBillToID, returnOrderInput);
                _scModelUtils.setStringValueAtModelPath("Order.SearchCriteria.BillToID", sBillToID, returnOrderInput);
                _scModelUtils.setStringValueAtModelPath("Order.CustomerContactID", _scModelUtils.getStringValueFromPath("Order.CustomerContactID", targetModel), returnOrderInput);
            } else if (!(_scBaseUtils.isVoid(_scModelUtils.getStringValueFromPath("Order.CustomerEMailID", targetModel)))) {
                _scModelUtils.setStringValueAtModelPath("Order.SearchCriteria.CustomerEMailID", _scModelUtils.getStringValueFromPath("Order.CustomerEMailID", targetModel), returnOrderInput);
            } else if (!(_scBaseUtils.isVoid(_scModelUtils.getStringValueFromPath("Order.CustomerPhoneNo", targetModel)))) {
                _scModelUtils.setStringValueAtModelPath("Order.SearchCriteria.CustomerPhoneNo", _scModelUtils.getStringValueFromPath("Order.CustomerPhoneNo", targetModel), returnOrderInput);
            }
            _scModelUtils.setStringValueAtModelPath("Order.SearchCriteria.OrderHeaderKey", _scModelUtils.getStringValueFromPath("Order.OrderHeaderKey", targetModel), returnOrderInput);
            return returnOrderInput;
        },
        getScreenTitle: function() {
            var currentScreen = null;
            var screenInput = null;
            var returnVal = null;
            screenInput = _scScreenUtils.getInitialInputData(this);
            returnVal = _isccsOrderUtils.getTitleForOrderEditor(screenInput);
            return returnVal;
        },
        updateScreenWithEditorInput: function(model) {
            var eventDefn = null;
            var blankModel = null;
            eventDefn = _scBaseUtils.getNewBeanInstance();
            blankModel = _scBaseUtils.getNewModelInstance();
            _scBaseUtils.setAttributeValue("argumentList", blankModel, eventDefn);
            _scBaseUtils.setAttributeValue("argumentList.inputData", model, eventDefn);
            _scEventUtils.fireEventInsideScreen(_scWizardUtils.getCurrentPage(_isccsUIUtils.getCurrentWizardInstance(this)), "updateScreenInitData", null, eventDefn);
        },
        showRelatedTaskScreenHolder: function(event, bEvent, ctrl, args) {
            _isccsEditorRelatedTaskUtils.showRelatedTaskScreenHolder(this);
            this.initRelatedTasks(event, bEvent, ctrl, args);
        },
        showOrHideRelTask: function(event, bEvent, ctrl, args) {
            _isccsEditorRelatedTaskUtils.showOrHideRelatedTaskScreen(this);
        },
        onScreenInit: function(event, bEvent, ctrl, args) {
            var screenInput = null;
            screenInput = _scScreenUtils.getInitialInputData(this);
            this.setInitialEditorInput(screenInput);
            _isccsUIUtils.getEnterpriseList(this);
        },
        setInitialEditorInput: function(screenInput) {
            if (!(_scBaseUtils.isVoid(_scModelUtils.getModelObjectFromPath("Order", screenInput)))) {
                _scWidgetUtils.showWidget(this, "pnlCustomerContext", false, null);
            }
            _scScreenUtils.setModel(this, "InitialEditorInput", screenInput, null);
        },
        setScreenTitle: function(event, bEvent, ctrl, args) {
            _isccsBaseTemplateUtils.updateScreenTitleOnEditor(this, bEvent);
        },
        handleResponseForReplaceScreen: function(res, args) {
            if (_scBaseUtils.equals(res, "Ok")) {
                _scScreenUtils.clearScreen(_isccsUIUtils.getCurrentWizardInstance(this), null);
                if (_scBaseUtils.isVoid(args)) {
                    _scControllerUtils.continueOpeningInEditor(this.newScreenData);
                } else {
                    _scControllerUtils.continueOpeningInEditor(args);
                }
            }
        },
        setEditorInput: function(event, bEvent, ctrl, args) {
            var model = null;
            model = _scBaseUtils.getAttributeValue("model", false, args);
            if (!(_scBaseUtils.isVoid(model))) {
                var wiz = null;
                _scScreenUtils.setInitialInputData(this, model);
                this.setInitialEditorInput(model);
                var eventDefn = null;
                eventDefn = _scBaseUtils.getNewBeanInstance();
                _scBaseUtils.setAttributeValue("argumentList", _scBaseUtils.getNewBeanInstance(), eventDefn);
                _isccsEditorScreenUtils.updateEditorTab(this);
                wiz = _isccsUIUtils.getCurrentWizardInstance(this);
                if (!(_scBaseUtils.isVoid(wiz))) {
                    _scEventUtils.fireEventInsideScreen(wiz, "setWizardInput", eventDefn, args);
                }
                this.handleEnterpriseContextDisplay();
            }
        },
        handleMashupOutput: function(mashupRefId, modelOutput, mashupInput, mashupContext) {
            if (_scBaseUtils.equals(mashupRefId, "createOrder_getOrganizationList")) {
                _isccsEditorScreenUtils.handleGetOrganizationList(this, modelOutput, mashupContext);
            } else if (_scBaseUtils.equals(mashupRefId, "createCustomer_getOrganizationList")) {
                _isccsEditorScreenUtils.handleCustomerGetOrganizationList(this, modelOutput, mashupContext);
            } else if (_scBaseUtils.equals(mashupRefId, "createOrder")) {
                _isccsEditorScreenUtils.handleCreateOrderAPICall(this, modelOutput, mashupContext);
            } else if (_scBaseUtils.equals(mashupRefId, "productBrowsing_getOrganizationList")) {
                _isccsItemUtils.handleProductGetOrganizationList(this, modelOutput, mashupContext);
            } else if (_scBaseUtils.equals(mashupRefId, "searchCatalogIndex_portlet")) {
                _isccsItemUtils.handleItemSearchResults(this, modelOutput, mashupInput);
            } else if (_scBaseUtils.equals(mashupRefId, "productBrowsing_storefrontGetRuleDetails")) {
                _isccsItemUtils.handleStorefrontRuleForProductBrowsing(this, modelOutput, mashupInput, mashupContext);
            } else if (_scBaseUtils.equals(mashupRefId, "productBrowsing_storefrontCreateOrder")) {
                _isccsEditorScreenUtils.handleCreateOrderAPICallForStorefrontProductBrowse(this, modelOutput, mashupContext);
            } else if (_scBaseUtils.equals(mashupRefId, "flushCache")) {
                if (_scModelUtils.getBooleanValueFromPath("Cache.IsSuccessful", modelOutput, false)) {
                    _isccsBaseTemplateUtils.showMessage(this, "System_Cache_Flushed", "success", null);
                } else {
                    _isccsBaseTemplateUtils.showMessage(this, "System_Cache_Failed", "error", null);
                }
            }
            if (_scBaseUtils.equals(mashupRefId, "getRequiredEditorAttributes")) {
                if (!(_scBaseUtils.isVoid(_scBaseUtils.getAttributeValue("OrderLineKey", false, mashupContext)))) {
                    _scModelUtils.setStringValueAtModelPath("Order.OrderLineKey", _scBaseUtils.getAttributeValue("OrderLineKey", false, mashupContext), modelOutput);
                }
                var args = null;
                args = _scBaseUtils.getNewBeanInstance();
                _scBaseUtils.setAttributeValue("model", modelOutput, args);
                _scEventUtils.fireEventInsideScreen(this, "setEditorInput", null, args);
            }
            if (_scBaseUtils.equals(mashupRefId, "getEnterpriseList")) {
                _scScreenUtils.setModel(this, "getEnterpriseList", modelOutput, null);
                this.handleEnterpriseContextDisplay();
            }
        },
        handleMashupCompletion: function(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data) {
            _isccsBaseTemplateUtils.handleMashupCompletion(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data, this);
        },
        onEnterpriseSelectionForCustomer: function(actionPerformed, model, popupParams) {
            if (!(_scBaseUtils.equals(actionPerformed, "CLOSE"))) {
                _scModelUtils.setStringValueAtModelPath("Customer.CustomerType", "02", model);
                _isccsUIUtils.openWizardInEditor("isccs.customer.wizards.createConsumer.CreateConsumerWizard", model, "isccs.editors.CustomerEditor", this);
            }
        },
        onEnterpriseSelectionForBusiness: function(actionPerformed, model, popupParams) {
            if (!(_scBaseUtils.equals(actionPerformed, "CLOSE"))) {
                _scModelUtils.setStringValueAtModelPath("Customer.CustomerType", "01", model);
                _isccsUIUtils.openWizardInEditor("isccs.customer.wizards.createBusiness.CreateBusinessWizard", model, "isccs.editors.CustomerEditor", this);
            }
        },
        onEnterpriseSelectionForOrder: function(actionPerformed, model, popupParams) {
            if (!(_scBaseUtils.equals(actionPerformed, "CLOSE"))) {
                var mashupContext = null;
                mashupContext = _scBaseUtils.getAttributeValue("mashupContext", false, popupParams);
                _isccsEditorScreenUtils.callCreateOrderAndOpenCreateOrderWiz(this, model, mashupContext);
            }
        },
        onEnterpriseSelectionForItemBrowsing: function(actionPerformed, model, popupParams) {
            if (!(_scBaseUtils.equals(actionPerformed, "CLOSE"))) {
                var searchText = null;
                searchText = _scModelUtils.getStringValueFromPath("CatalogSearch.Terms.Term.Value", model);
                if (!(_scBaseUtils.isVoid(searchText))) {
                    _scModelUtils.setStringValueAtModelPath("CatalogSearch.Terms.Term.Condition", "MUST", model);
                }
                _isccsItemUtils.openItemSearch(this, model, "searchCatalogIndex_portlet");
            }
        },
        onReturnEnterpriseSelection: function(actionPerformed, model, popupParams) {
            if (!(_scBaseUtils.equals(actionPerformed, "CLOSE"))) {
                _isccsUIUtils.openWizardInEditor("isccs.return.wizards.createReturn.CreateReturnWizard", model, "isccs.editors.ReturnEditor", this);
            }
        },
        initRelatedTasks: function(event, bEvent, ctrl, args) {
            if (!(_isccsEditorScreenUtils.showOrHidecustomerOptions(event, bEvent, ctrl, args))) {
                _isccsRelatedTaskUtils.hideTask(this, "customerOptions");
            }
        },
        handleEnterpriseContextDisplay: function() {
            var screenInput = null;
            screenInput = _scScreenUtils.getModel(this, "InitialEditorInput");
            if (!(_isccsWidgetUtils.hideOrganizationWithOneResult(this, "pnlEnterpriseHolder", _scScreenUtils.getModel(this, "getEnterpriseList")))) {
                if (!(_isccsUIUtils.initContextEnterprise(this, "Order", screenInput))) {
                    _scWidgetUtils.hideWidget(this, "pnlEnterpriseHolder");
                } else {
                    _scWidgetUtils.showWidget(this, "pnlEnterpriseHolder", false, null);
                    _scScreenUtils.setModel(this, "enterpriseContext", screenInput, null);
                }
            }
        }
    });
});
