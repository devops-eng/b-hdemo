/*
 * Licensed Materials - Property of IBM
 * IBM Call Center for Commerce (5725-P82)
 * (C) Copyright IBM Corp. 2013 All Rights Reserved.
 * US Government Users Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */
scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/text", "scbase/loader!extn/customScreen/CancelPickupEditor", "scbase/loader!sc/plat/dojo/controller/ServerDataController"], function(
_dojodeclare, _dojokernel, _dojotext, _isccsCancelEditor, _scServerDataController) {
    return _dojodeclare("extn.customScreen.CancelPickupEditorBehaviorController", [_scServerDataController], {
        screenId: 'extn.customScreen.CancelPickupEditor',
        mashupRefs: [{
            mashupId: 'createOrder_getOrganizationList',
            mashupRefId: 'createOrder_getOrganizationList'
        }, {
            mashupId: 'customerSearch_getOrganizationList',
            mashupRefId: 'createCustomer_getOrganizationList'
        }, {
            mashupId: 'searchCatalogIndex_portlet',
            mashupRefId: 'searchCatalogIndex_portlet'
        }, {
            mashupId: 'storefront_getRuleDetails',
            mashupRefId: 'productBrowsing_storefrontGetRuleDetails'
        }, {
            mashupId: 'productBrowsing_getOrganizationList',
            mashupRefId: 'productBrowsing_getOrganizationList'
        }, {
            mashupId: 'testScreen_flushCache',
            mashupRefId: 'flushCache'
        }, {
            mashupId: 'getEnterpriseList',
            mashupRefId: 'getEnterpriseList'
        }, {
            mashupId: 'createOrder_createOrder',
            mashupRefId: 'createOrder'
        }, {
            mashupId: 'createOrder_createOrder',
            mashupRefId: 'productBrowsing_storefrontCreateOrder'
        }, {
            mashupId: 'orderEditor_getCompleteOrderDetails',
            mashupRefId: 'getRequiredEditorAttributes'
        }]
    });
});