scDefine(["dojo/text!./templates/cancelPickup.html", "scbase/loader!dojo/_base/declare", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!isccs/utils/UIUtils", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!isccs/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/EditorUtils", "scbase/loader!sc/plat/dojo/utils/GridxUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils"], function(templateText, _dojodeclare, _scScreen, _scBaseUtils, _isccsUIUtils, _scUserprefs, _scScreenUtils, _scModelUtils, _isccsBaseTemplateUtils, _scEditorUtils, _scGridxUtils, _scWidgetUtils, _scEventUtils) {
    return _dojodeclare("extn.customScreen.cancelPickup", [_scScreen], {
        templateString: templateText,
        uId: "cancelPickup",
        packageName: "extn.customScreen",
        className: "cancelPickup",
        namespaces: {
            sourceBindingNamespaces: [{
                value: 'parentModel',
                description: "This model has everything"
            }, {
                value: 'gridNamespace',
                description: "This model has ShipmentLines details"
            }]
        },
        subscribers: {
            local: [{
                eventId: 'afterScreenInit',
                sequence: '30',
                description: 'This event is raised after screen has been initialized',
                handler: {
                    methodName: "initializeScreen"
                }
            }, {
                eventId: 'nextBttn2_onClick',
                sequence: '25',
                description: 'Handler for next button onClick event',
                handler: {
                    methodName: "handleConfirm",
                    description: "Handler for next event"
                }
            }, {
                eventId: 'prevBttn2_onClick',
                sequence: '25',
                description: 'Handler for next button onClick event',
                handler: {
                    methodName: "handleClose",
                    description: "Handler for next event"
                }
            }, {
                eventId: 'nextBttn_onClick',
                sequence: '25',
                description: 'Handler for next button onClick event',
                handler: {
                    methodName: "handleConfirm",
                    description: "Handler for next event"
                }
            }, {
                eventId: 'prevBttn_onClick',
                sequence: '25',
                description: 'Handler for next button onClick event',
                handler: {
                    methodName: "handleClose",
                    description: "Handler for next event"
                }
            }, {
                eventId: 'gridCancelPickup_ScRowSelect',
                sequence: '30',
                handler: {
                    methodName: "onRowSelect"
                }
            }, {
                eventId: 'gridCancelPickup_ScRowDeselect',
                sequence: '30',
                handler: {
                    methodName: "onRowDeSelect"
                }
            }, {
                eventId: 'gridCancelPickup_ScHeaderSelect',
                sequence: '30',
                handler: {
                    methodName: "onRowSelect"
                }
            }, {
                eventId: 'gridCancelPickup_ScHeaderDeselect',
                sequence: '30',
                handler: {
                    methodName: "onRowDeSelect"
                }
            }]
        },
        initializeScreen: function(event, bEvent, ctrl, args) {
            var orderHeaderKey = _scModelUtils.getStringValueFromPath("screen.params.scEditorInput.Order.OrderHeaderKey", args);
            var inputModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
            _scModelUtils.setStringValueAtModelPath("Shipment.OrderHeaderKey", orderHeaderKey, inputModel);
            _scWidgetUtils.setLabel(this, "nextBttn2", "Cancel Pick up");
            _scWidgetUtils.setLabel(this, "nextBttn", "Cancel Pick up");
            _isccsUIUtils.callApi(this, inputModel, "cancelPickup_getShipmentList_ref", null);
        },
        handleMashupCompletion: function(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data) {
            _isccsBaseTemplateUtils.handleMashupCompletion(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data, this);
        },
        handleMashupOutput: function(mashupRefId, modelOutput, mashupInput, mashupContext, applySetModel) {
            if (_scBaseUtils.equals(mashupRefId, "cancelPickup_getShipmentList_ref")) {
                var editorInstance = _scEditorUtils.getCurrentEditor();
                var editorTitle = "Cancel Pickup";
                editorInstance.setInitialTitle(editorTitle);
                editorInstance.set('title', editorTitle);
                //_scScreenUtils.setScreenTitle(this, "Cancel Pickup");
                var inputParams = _scBaseUtils.getNewBeanInstance();
                _scBaseUtils.setAttributeValue("ScreenTitle", "Cancel Pickup", inputParams);
                var eventDefn = _scBaseUtils.getNewBeanInstance();
                var args = _scBaseUtils.getNewBeanInstance();
                _scBaseUtils.setAttributeValue("inputData", inputParams, args);
                _scBaseUtils.setAttributeValue("argumentList", args, eventDefn);
                _isccsBaseTemplateUtils.updateScreenTitleOnEditor(_isccsUIUtils.getEditorFromScreen(this), eventDefn);
                var outputModel = modelOutput;
                _scScreenUtils.setModel(this, "parentModel", outputModel, null);
                var shipment = _scModelUtils.getStringValueFromPath("Shipments.Shipment", outputModel);
                var shipmentLinesArr = _scBaseUtils.getNewArrayInstance();
                if (!_scBaseUtils.isVoid(shipment)) {
                    for (i = 0; i < shipment.length; i++) {
                        var shipmentNo = _scModelUtils.getStringValueFromPath("ShipmentNo", shipment[i]);
                        var shipmentLine = _scModelUtils.getStringValueFromPath("ShipmentLines.ShipmentLine", shipment[i]);
                        var status = _scModelUtils.getStringValueFromPath("Status", shipment[i]);
                        //  if(_scBaseUtils.equals(status, "1100.70.06.30.5")){
                        if (_scBaseUtils.equals(status, "1600.002.10")) {
                            for (j = 0; j < shipmentLine.length; j++) {
                                var shipLinesTemp = shipmentLine[j];
                                _scModelUtils.setStringValueAtModelPath("ShipmentNo", shipmentNo, shipmentLine[j]);
                                shipmentLinesArr.push(shipmentLine[j]);
                            }
                        }
                    }
                    if (!_scBaseUtils.isVoid(shipmentLinesArr)) {
                        var GridNamespace = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
                        _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLine", shipmentLinesArr, GridNamespace);
                        _scScreenUtils.setModel(this, "gridNamespace", GridNamespace, null);
                        var isRowSelected = false;
                        for (i = 0; i < shipmentLinesArr.length; i++) {
                            isRowSelected = _scGridxUtils.isRowSelectedUsingUId(this, "gridCancelPickup", i);
                            if (isRowSelected) {
                                _scWidgetUtils.enableWidget(this, "nextBttn2", false);
                                _scWidgetUtils.enableWidget(this, "nextBttn", false);
                                break;
                            } else {
                                _scWidgetUtils.disableWidget(this, "nextBttn", false);
                                _scWidgetUtils.disableWidget(this, "nextBttn2", false);
                            }
                        }
                    } else {
                        _scWidgetUtils.disableWidget(this, "nextBttn", false);
                        _scWidgetUtils.disableWidget(this, "nextBttn2", false);
                    }
                }
            }
            if (_scBaseUtils.equals(mashupRefId, "cancelPickup_createReturn_ref")) {
                var outputModel = modelOutput;
                var grandTotaltemp = _scModelUtils.getStringValueFromPath("Order.OverallTotals.GrandTotal", outputModel);
                grandTotal = '$' + grandTotaltemp;
                var returnOrderNo = _scModelUtils.getStringValueFromPath("Order.OrderNo", outputModel);
                if (_scBaseUtils.isVoid(returnOrderNo)) {
                    var editor = _scEditorUtils.getCurrentEditor();
                    _scEventUtils.fireEventGlobally(this, "extn_displayPaypalReturnErrorMessage", null, outputModel);
                    _scEditorUtils.closeEditor(editor);
                } else {
                    _scUserprefs.setProperty("grandTotal", grandTotal);
                    _scUserprefs.setProperty("OrderNoReturn", returnOrderNo);
                    var grandTotalModel = _scModelUtils.createNewModelObjectWithRootKey("ReturnOrder");
                    _scModelUtils.setStringValueAtModelPath("ReturnOrder.GrandTotal", grandTotal, grandTotalModel);
                    _scModelUtils.setStringValueAtModelPath("ReturnOrder.OrderNo", returnOrderNo, grandTotalModel);
                    var editor = _scEditorUtils.getCurrentEditor();
                    _scEventUtils.fireEventGlobally(this, "extn_displayReturnMessage", null, grandTotalModel);
                    _scEditorUtils.closeEditor(editor);
                }
            }
        },
        handleConfirm: function(event, bEvent, ctrl, args) {
            var gridModel = _scScreenUtils.getModel(this, "gridNamespace");
            var shipmentLinesArr = _scModelUtils.getStringValueFromPath("Shipment.ShipmentLine", gridModel);
            var shiplineModel = _scBaseUtils.getNewArrayInstance();
            for (i = 0; i < shipmentLinesArr.length; i++) {
                var isRowSelected = _scGridxUtils.isRowSelectedUsingUId(this, "gridCancelPickup", i);
                if (isRowSelected) {
                    var rowData = _scGridxUtils.getRowJsonFromRowIndex(this, "gridCancelPickup", i);
                    var orderHeaderKey = _scModelUtils.getStringValueFromPath("OrderHeaderKey", rowData);
                    var orderLineKey = _scModelUtils.getStringValueFromPath("OrderLineKey", rowData);
                    var shipment = {
                        "OrderLine": {
                            "OrderLineKey": orderLineKey
                        }
                    };
                    shiplineModel.push(shipment);
                }
            }
            var inputModel = _scModelUtils.createNewModelObjectWithRootKey("MonitorConsolidation");
            _scModelUtils.setStringValueAtModelPath("MonitorConsolidation.Shipment.OrderHeaderKey", orderHeaderKey, inputModel);
            _scModelUtils.setStringValueAtModelPath("MonitorConsolidation.Shipment.ShipmentLines.ShipmentLine", shiplineModel, inputModel);
            _isccsUIUtils.callApi(this, inputModel, "cancelPickup_createReturn_ref", null);
        },
        handleClose: function(event, bEvent, ctrl, args) {
            var editor = _scEditorUtils.getCurrentEditor();
            _scEventUtils.fireEventGlobally(this, "extn_refreshOrderSummary", null, {});
            _scEditorUtils.closeEditor(editor);
        },
        onRowSelect: function(event, bEvent, ctrl, args) {
            _scWidgetUtils.enableWidget(this, "nextBttn2", false);
            _scWidgetUtils.enableWidget(this, "nextBttn", false);
            if (_scBaseUtils.equals(bEvent._id, "gridCancelPickup_ScHeaderSelect")) {
                return;
            }
            var selectedRowIndex = null;
            selectedRowIndex = _scBaseUtils.getNumberValueFromBean("rowIndex", args);
            var selectedRowObj = null;
            selectedRowObj = _scBaseUtils.getModelValueFromBean("selectedRow", args);
            var shipmentNoGrid = _scModelUtils.getStringValueFromPath("ShipmentNo", selectedRowObj);
            var inputModel = _scScreenUtils.getModel(this, "gridNamespace");
            var shipmentLinesArr = _scModelUtils.getStringValueFromPath("Shipment.ShipmentLine", inputModel);
            var message = "Please un-check shipment lines of other shipments before selecting new ones.";
            var messagefr = "Veuillez décocher les lignes d’expédition des autres expéditions avant d’en sélectionner de nouvelles.";
            var localeCode = _scUserprefs.localeCode;
            //  var isRowSelectedArr= _scBaseUtils.getNewArrayInstance();
            //  var shipment= null;
            for (i = 0; i < shipmentLinesArr.length; i++) {
                var shipmentNo = _scModelUtils.getStringValueFromPath("ShipmentNo", shipmentLinesArr[i]);
                if (_scBaseUtils.equals(shipmentNoGrid, shipmentNo)) {
                    var isRowSelected = _scGridxUtils.isRowSelectedUsingUId(this, "gridCancelPickup", i);
                    if (!isRowSelected) {
                        _scGridxUtils.selectRowUsingUId(this, "gridCancelPickup", i);
                        //    isRowSelectedArr.push("N");
                    }
                    /*else{
                                           shipment={
                                            
                                            i :"N"
                                        };
                                        isRowSelectedArr.push("N"); 
                                        }*/
                } else {
                    var isRowSelected = _scGridxUtils.isRowSelectedUsingUId(this, "gridCancelPickup", i);
                    if (isRowSelected) {
                        // isRowSelectedArr.push("Y");
                        if (_scBaseUtils.equals(localeCode, "fr_CA_EST")) {
                            _isccsBaseTemplateUtils.showMessage(this, message, "error", null);
                        } else {
                            _isccsBaseTemplateUtils.showMessage(this, messagefr, "error", null);
                        }
                        
                        _scGridxUtils.deselectRowUsingRowindex(this, "gridCancelPickup", selectedRowIndex);
                        break;
                    }
                    /*else{
                                            isRowSelectedArr.push("N");
                                      }*/
                }
            }
            console.log("All rows of same shipment selected");
        },
        onRowDeSelect: function(event, bEvent, ctrl, args) {
            var deSelectedRowObj = null;
            deSelectedRowObj = _scBaseUtils.getModelValueFromBean("deselectedRow", args);
            var shipmentNoGrid = _scModelUtils.getStringValueFromPath("ShipmentNo", deSelectedRowObj);
            //  var shipmentNoGrid= null;
            var inputModel = _scScreenUtils.getModel(this, "gridNamespace");
            var shipmentLinesArr = _scModelUtils.getStringValueFromPath("Shipment.ShipmentLine", inputModel);
            var isRowSelectedArr = _scBaseUtils.getNewArrayInstance();
            for (i = 0; i < shipmentLinesArr.length; i++) {
                var isRowSelected = _scGridxUtils.isRowSelectedUsingUId(this, "gridCancelPickup", i);
                var shipmentNo = _scModelUtils.getStringValueFromPath("ShipmentNo", shipmentLinesArr[i]);
                if (_scBaseUtils.equals(shipmentNoGrid, shipmentNo)) {
                    if (isRowSelected) {
                        _scGridxUtils.deselectRowUsingRowindex(this, "gridCancelPickup", i);
                    }
                }
                if (isRowSelected) {
                    isRowSelectedArr.push("Y");
                } else {
                    isRowSelectedArr.push("N");
                }
            }
            this.enableDisableButton(shipmentLinesArr);
            //                for(i=0; i<isRowSelectedArr.length;i++){
            //                 if(_scBaseUtils.equals(isRowSelectedArr[i], "Y")){
            //                     _scWidgetUtils.enableWidget(this, "nextBttn2", false);
            //                     _scWidgetUtils.enableWidget(this, "nextBttn", false);
            //                 }else{
            //                     _scWidgetUtils.disableWidget(this, "nextBttn", false);
            //                     _scWidgetUtils.disableWidget(this, "nextBttn2", false);
            //                     }
            //                }
        },
        enableDisableButton: function(shipmentLinesArr) {
            var flagSelected = false;
            for (var j = 0; j < shipmentLinesArr.length; j++) {
                var isRowSelectedFinal = _scGridxUtils.isRowSelectedUsingUId(this, "gridCancelPickup", i);
                if (isRowSelectedFinal) {
                    flagSelected = true;
                    break;
                } else {
                    flagSelected = false;
                }
            }
            if (flagSelected) {
                _scWidgetUtils.enableWidget(this, "nextBttn2", false);
                _scWidgetUtils.enableWidget(this, "nextBttn", false);
            } else {
                _scWidgetUtils.disableWidget(this, "nextBttn", false);
                _scWidgetUtils.disableWidget(this, "nextBttn2", false);
            }
        }
    });
});
