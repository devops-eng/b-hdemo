scDefine([
	"scbase/loader!dojo/_base/declare",
	"scbase/loader!dojo/_base/kernel",
	"scbase/loader!dojo/text",
	"scbase/loader!extn/customScreen/cancelPickup",
	"scbase/loader!sc/plat/dojo/controller/ServerDataController"
], function(
	_dojodeclare, _dojokernel, _dojotext, _extncancelPickup, _scServerDataController
) {
return _dojodeclare("extn.customScreen.cancelPickupBehaviorController", [_scServerDataController], {
	screenId: 'extn.customScreen.cancelPickup',
	mashupRefs: [{
            extnType: 'ADD',
            mashupRefId: 'cancelPickup_getShipmentList_ref',
            mashupId: 'cancelPickup_getShipmentList'
        },
        {
            extnType: 'ADD',
            mashupRefId: 'cancelPickup_createReturn_ref',
            mashupId: 'cancelPickup_createReturn'
        }]
});
});
