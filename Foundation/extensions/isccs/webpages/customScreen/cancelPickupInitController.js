scDefine([
	"scbase/loader!dojo/_base/declare",
	"scbase/loader!dojo/_base/kernel",
	"scbase/loader!dojo/text",
	"scbase/loader!extn/customScreen/cancelPickup",
	"scbase/loader!sc/plat/dojo/controller/ScreenController"
], function(
	_dojodeclare, _dojokernel, _dojotext, _extncancelPickup, _scScreenController
) {
return _dojodeclare("extn.customScreen.cancelPickupInitController", [_scScreenController], {
	screenId: 'extn.customScreen.cancelPickup',
	mashupRefs: []

});
});
