scDefine(["scbase/loader!dojo/_base/declare",
"scbase/loader!extn/customer/create/CreateContactExtnUI",
"scbase/loader!sc/plat/dojo/utils/BaseUtils",
"scbase/loader!sc/plat/dojo/utils/ModelUtils",
"scbase/loader!isccs/utils/BaseTemplateUtils",
"scbase/loader!sc/plat/dojo/utils/ScreenUtils",
"scbase/loader!isccs/utils/SharedComponentUtils",
"scbase/loader!isccs/utils/UIUtils",
"scbase/loader!isccs/utils/ModelUtils",
"scbase/loader!sc/plat/dojo/utils/WidgetUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnCreateContactExtnUI
                      ,
			   _scBaseUtils
                      ,
                       _scModelUtils
                     ,
                       _isccsBaseTemplateUtils
                    ,
                      _scScreenUtils
			,
			_isccsSharedComponentUtils
			,
			_isccsUIUtils
			,
			_isccsModelUtils
			,
			_scWidgetUtils
	

){ 
	return _dojodeclare("extn.customer.create.CreateContactExtn", [_extnCreateContactExtnUI],{
	// custom code here

	
extn_organization:"DEFAULT",
extn_shipToAddress:{},
extn_billToAddress:{},
extn_TelephoneValidation: function(event, bEvent, ctrl, args)
	{
	
	var customerModel = _scBaseUtils.getTargetModel(this, "manageCustomer_input");
    var TelephoneNumber =  _scModelUtils.getStringValueFromPath("Customer.CustomerContactList.CustomerContact.DayPhone",customerModel);
	var phoneLength = 10;
	var numbPattern = /^[0-9]+$/;

    if(_scBaseUtils.isVoid(TelephoneNumber))
    {
        var message = null;
         message = _scScreenUtils.getString(this,"extn_Telephone_Mandatory");
        _isccsBaseTemplateUtils.showMessage(this,message,"error",null);
    }
	else
	{
	if (TelephoneNumber.length != phoneLength) {
                         var message = null;
                         message = _scScreenUtils.getString(this,"extn_Telephone_Validation");
                        _isccsBaseTemplateUtils.showMessage(this,message,"error",null);

                       // _isccsBaseTemplateUtils.showMessage(this, "The phone number entered is invalid", "error", null);
                      //  sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
                      //   return true;


                    }
			else if (TelephoneNumber.length == phoneLength) {

                        if (!(numbPattern.test(TelephoneNumber))) {
                            var message = null;
                            message = _scScreenUtils.getString(this,"extn_Telephone_Validation");
                            _isccsBaseTemplateUtils.showMessage(this,message,"error",null);

                           // _isccsBaseTemplateUtils.showMessage(this, "The phone number entered is invalid", "error", null);
                          //  sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
                          //  return true;

                        }
                          else
                        {
                             _isccsBaseTemplateUtils.closeMessagePanel(event, bEvent, ctrl, args);
                        }
						}
                      

	}
	},

extn_OpenAddressPopup: function(event, bEvent, ctrl, args){
	    
	var personInfo= _scBaseUtils.getNewBeanInstance();
	var title = "";
	
	var eCustomer = _scScreenUtils.getTargetModel(this, "manageCustomer_input", null);
	var personInfoDefaultFirstName = _scModelUtils.getStringValueFromPath("Customer.CustomerContactList.CustomerContact.FirstName",eCustomer);
	var personInfoDefaultLastName = _scModelUtils.getStringValueFromPath("Customer.CustomerContactList.CustomerContact.LastName",eCustomer);
	var personInfoDefaultEmailID = _scModelUtils.getStringValueFromPath("Customer.CustomerContactList.CustomerContact.EmailID",eCustomer);
	var personInfoDefaultDayPhone = _scModelUtils.getStringValueFromPath("Customer.CustomerContactList.CustomerContact.DayPhone",eCustomer);
	var personInfoDefaultEveningPhone = _scModelUtils.getStringValueFromPath("Customer.CustomerContactList.CustomerContact.EveningPhone",eCustomer);
	var personInfoDefaultTitle = _scModelUtils.getStringValueFromPath("Customer.CustomerContactList.CustomerContact.Title",eCustomer);
	if(_scBaseUtils.isVoid(personInfoDefaultFirstName ) || _scBaseUtils.isVoid(personInfoDefaultLastName) || _scBaseUtils.isVoid(personInfoDefaultDayPhone)){
	    var message = _scScreenUtils.getString(this,"extn_AddressManadatoryCheck");
           _isccsBaseTemplateUtils.showMessage(this,message,"error",null);
      //  _isccsBaseTemplateUtils.showMessage(this,"Enter all mandatory fields before adding Addresses","error",null);
        return;
	}
	_scModelUtils.setStringValueAtModelPath("PersonInfo.FirstName", personInfoDefaultFirstName , personInfo);
	_scModelUtils.setStringValueAtModelPath("PersonInfo.LastName", personInfoDefaultLastName , personInfo);
	_scModelUtils.setStringValueAtModelPath("PersonInfo.EMailID", personInfoDefaultEmailID , personInfo);
	_scModelUtils.setStringValueAtModelPath("PersonInfo.DayPhone", personInfoDefaultDayPhone , personInfo);
	_scModelUtils.setStringValueAtModelPath("PersonInfo.EveningPhone", personInfoDefaultEveningPhone , personInfo);
	_scModelUtils.setStringValueAtModelPath("PersonInfo.Title", personInfoDefaultTitle , personInfo);
	
//	_isccsModelUtils.removeAttributeFromModel("Extn",personInfoDefault);
//	_isccsModelUtils.removeAttributeFromModel("Status",personInfoDefault);
	var testCustomer = _scScreenUtils.getTargetModel(this, "manageCustomer_input", null);
	//_scModelUtils.setStringValueAtModelPath("PersonInfo", personInfoDefault, personInfo);
   
	if(bEvent._originatingControlUId == "extn_add_shipto_link"){
	    title = "ShipTo";
	   
	    var shiptoPersonInfo=  _scScreenUtils.getModel(this, "extn_shipto_address");
	     if(!(_scBaseUtils.isVoid(shiptoPersonInfo))){
	         personInfo = shiptoPersonInfo;
	     }
	}else if(bEvent._originatingControlUId == "extn_add_billto_link"){
	     title = "BillTo";
	     var billtoPersonInfo=  _scScreenUtils.getModel(this, "extn_billto_address");
	     if(!(_scBaseUtils.isVoid(billtoPersonInfo))){
	         personInfo = billtoPersonInfo;
	     }
	}
	if(_scBaseUtils.isVoid(personInfo.PersonInfo.Country)){
    		if (this.extn_organization == "BAY") {
    			_scModelUtils.setStringValueAtModelPath("PersonInfo.Country", "CA", personInfo);
    		}else{
    			_scModelUtils.setStringValueAtModelPath("PersonInfo.Country", "US", personInfo);
			if(_scBaseUtils.isVoid(this.extn_organization)){
    			   this.extn_organization ="DEFAULT";
    			}
    		}	
    	}
	_isccsBaseTemplateUtils.closeMessagePanel(event, bEvent, ctrl, args);

 	_isccsSharedComponentUtils.openAddressCapture(this, null, personInfo, this.extn_organization, title, "extn_saveNewAddress");

},
extn_saveNewAddress: function(actionPerformed, model, popupParams) {
	var output = model;
	
	if(actionPerformed == "APPLY"){
	    model.IsBillTo = "Y";
    	    model.IsShipTo = "Y";
    	    model.IsSoldTo = "Y";
	    var shipToTitle = _scScreenUtils.getString(this,"ShipTo");
           var billToTitle = _scScreenUtils.getString(this,"BillTo");
    	    if(popupParams.title == shipToTitle){
	    	        model.IsDefaultShipTo = "Y";
    	      	        this.extn_shipToAddress = model;
        		_scScreenUtils.setModel(this,"extn_shipto_address",model,null);
        		_scWidgetUtils.hideWidget(this, "extn_shipto_empty_address_message_contentpane");
        		_scWidgetUtils.showWidget(this, "extn_shipto_address_contentpane");
                      var action = _scScreenUtils.getString(this,"Action_Edit");
        		 _scWidgetUtils.setValue(this, "extn_add_shipto_link", action , false); 
                   //  _scWidgetUtils.setValue(this, "extn_add_shipto_link", "Edit", false);   
    	    }else if(popupParams.title == billToTitle){
		        model.IsDefaultBillTo = "Y";
    	               this.extn_billToAddress = model;
	               _scScreenUtils.setModel(this,"extn_billto_address",model,null);
                      _scWidgetUtils.hideWidget(this, "extn_billto_empty_address_message_contentpane");
        		 _scWidgetUtils.showWidget(this, "extn_billto_address_contentpane");
                       var action = _scScreenUtils.getString(this,"Action_Edit");
        		  _scWidgetUtils.setValue(this, "extn_add_billto_link", action, false);
                     //_scWidgetUtils.setValue(this, "extn_add_billto_link", "Edit", false);
    	    }
      }
},

extn_enable_billto_panel: function(event, bEvent, ctrl, args) {

	if(event){
	    _scWidgetUtils.hideWidget(this, "extn_create_billto_contentpane",false);
	}else{

	    _scWidgetUtils.showWidget(this, "extn_create_billto_contentpane",false);
   
	}

},
extn_checkForCustomAddress: function(event, bEvent, ctrl, args) {
	var parentScreen = null;
	parentScreen = _isccsUIUtils.getParentScreen(this, false);
 	if(!(_scBaseUtils.isVoid(parentScreen))){
		this.extn_organization = _scModelUtils.getStringValueFromPath("Order.EnterpriseCode",_scScreenUtils.getModel(parentScreen, "screenInput"));
		if(_scBaseUtils.isVoid(this.extn_organization)){
			var currentScreenInput = _scScreenUtils.getInitialInputData(this);
		    	var customerKey = _scModelUtils.getStringValueFromPath("Customer.CustomerKey",_scScreenUtils.getInitialInputData(this));
            	    	if(!(_scBaseUtils.isVoid(customerKey))){	
          			_scWidgetUtils.hideWidget(this, "extn_createAddress_contentpane");
				_scWidgetUtils.hideWidget(this, "extn_use_shipto_address_for_billto_checkbox");
          	  	}
		}
	}
},
manageCustomer: function() {
            var eCustomer = null;
            var isPopup = false;
            eCustomer = _scScreenUtils.getTargetModel(
            this, "manageCustomer_input", null);
            isPopup = _scScreenUtils.isPopup(this);
            var message = null;
       var extn_shiptoAddress = _scScreenUtils.getModel(this, "extn_shipto_address");
       var extn_billtoAddress = _scScreenUtils.getModel(this, "extn_billto_address");
	if(_scWidgetUtils.isWidgetVisible(this,"extn_createAddress_contentpane")){
	    var checkBoxValue = _scWidgetUtils.getValue(this, "extn_use_shipto_address_for_billto_checkbox");
	            
                if(_scBaseUtils.isVoid(extn_shiptoAddress)){
                   
                     message = _scScreenUtils.getString(this,"ProvideShipToAddress");
                     _isccsBaseTemplateUtils.showMessage(this,message,"error",null);
                  
                }else if((checkBoxValue != "on") && _scBaseUtils.isVoid(extn_billtoAddress)){
                         
                     message = _scScreenUtils.getString(this,"ProvideBillToaddress");
                     _isccsBaseTemplateUtils.showMessage(this,message,"error",null);
                    
                }else{
                     var additionalAddressesList = _scBaseUtils.getNewArrayInstance(); 
			if(checkBoxValue == "on"){
				extn_shiptoAddress.IsDefaultBillTo = "Y";
			}else{
				_scBaseUtils.appendToArray(additionalAddressesList,extn_billtoAddress);
			}
                     _scBaseUtils.appendToArray(additionalAddressesList,extn_shiptoAddress);                                         
                     _scModelUtils.setStringValueAtModelPath("Customer.CustomerContactList.CustomerContact.CustomerAdditionalAddressList.CustomerAdditionalAddress", additionalAddressesList, eCustomer);
                
                     if(_scBaseUtils.equals(isPopup, true)) {
                            
                            _isccsUIUtils.callApi(this, eCustomer, "orderEntryManageCustomer", null);
                            
                        } else {
                            _isccsUIUtils.callApi(
                            this, eCustomer, "manageCustomer", null);
                        }
                }
         
 
                
                
	}else{
		 _isccsUIUtils.callApi(
                        this, eCustomer, "manageCustomer", null);
	}
}


});
});

