


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/customer/create/CreateContactExtn","scbase/loader!sc/plat/dojo/controller/ExtnScreenController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnCreateContactExtn
			 ,
			    _scExtnScreenController
){

return _dojodeclare("extn.customer.create.CreateContactExtnInitController", 
				[_scExtnScreenController], {

			
			 screenId : 			'extn.customer.create.CreateContactExtn'

			
			
			
			
			
						,

			
			
			 mashupRefs : 	[
	 		{
		 sourceNamespace : 			'extn_Language_NS'
,
		 mashupRefId : 			'extn_LanguageCommonCodeList'
,
		 sequence : 			''
,
		 mashupId : 			'extn_LanguageCommonCodeList'
,
		 callSequence : 			''
,
		 extnType : 			'ADD'
,
		 sourceBindingOptions : 			''

	}

	]

}
);
});

