
scDefine(["dojo/text!./templates/CreateContactExtn.html","scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/_base/lang","scbase/loader!dojo/text","scbase/loader!idx/form/CheckBox","scbase/loader!idx/form/FilteringSelect","scbase/loader!idx/form/TextBox","scbase/loader!idx/layout/ContentPane","scbase/loader!sc/plat","scbase/loader!sc/plat/dojo/binding/CheckBoxDataBinder","scbase/loader!sc/plat/dojo/binding/ComboDataBinder","scbase/loader!sc/plat/dojo/binding/CurrencyDataBinder","scbase/loader!sc/plat/dojo/binding/SimpleDataBinder","scbase/loader!sc/plat/dojo/layout/AdvancedTableLayout","scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!sc/plat/dojo/widgets/Label","scbase/loader!sc/plat/dojo/widgets/Link"]
 , function(			 
			    templateText
			 ,
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojolang
			 ,
			    _dojotext
			 ,
			    _idxCheckBox
			 ,
			    _idxFilteringSelect
			 ,
			    _idxTextBox
			 ,
			    _idxContentPane
			 ,
			    _scplat
			 ,
			    _scCheckBoxDataBinder
			 ,
			    _scComboDataBinder
			 ,
			    _scCurrencyDataBinder
			 ,
			    _scSimpleDataBinder
			 ,
			    _scAdvancedTableLayout
			 ,
			    _scBaseUtils
			 ,
			    _scLabel
			 ,
			    _scLink
){
return _dojodeclare("extn.customer.create.CreateContactExtnUI",
				[], {
			templateString: templateText
	
	
	
	
	
	
					,	
	namespaces : {
		targetBindingNamespaces :
		[
		],
		sourceBindingNamespaces :
		[
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_9'
						,
	  description: 'extn_Language_NS'
						,
	  value: 'extn_Language_NS'
						
			}
			,
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_10'
						,
	  description: 'extn_shipto_address'
						,
	  value: 'extn_shipto_address'
						
			}
			,
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_11'
						,
	  description: 'extn_billto_address'
						,
	  value: 'extn_billto_address'
						
			}
			
		]
	}

	

	,
	hotKeys: [ 
	]

,events : [
	]

,subscribers : {
local : [

{
	  eventId: 'afterScreenInit'

,	  sequence: '51'

,	  description: 'Check for custom address'



,handler : {
methodName : "extn_checkForCustomAddress"

 
}
}
,
{
	  eventId: 'txtDayPhone_onBlur'

,	  sequence: '51'

,	  description: 'extn_TelephoneValidation'



,handler : {
methodName : "extn_TelephoneValidation"

 
}
}
,
{
	  eventId: 'extn_Add_address_link_onClick'

,	  sequence: '51'

,	  description: 'Add address to new customer'



,handler : {
methodName : "extn_OpenAddressPopup"

 
}
}
,
{
	  eventId: 'extn_add_shipto_link_onClick'

,	  sequence: '51'

,	  description: 'Open add address popup'



,handler : {
methodName : "extn_OpenAddressPopup"

 
}
}
,
{
	  eventId: 'extn_add_billto_link_onClick'

,	  sequence: '51'




,handler : {
methodName : "extn_OpenAddressPopup"

 
}
}
,
{
	  eventId: 'extn_use_shipto_address_for_billto_checkbox_onChange'

,	  sequence: '51'




,handler : {
methodName : "extn_enable_billto_panel"

 
}
}

]
					 	 ,
}

});
});


