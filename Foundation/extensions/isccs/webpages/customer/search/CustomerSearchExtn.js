scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/customer/search/CustomerSearchExtnUI","scbase/loader!sc/plat/dojo/utils/ScreenUtils","scbase/loader!sc/plat/dojo/utils/ModelUtils","scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!isccs/utils/UIUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnCustomerSearchExtnUI
             ,
               _scScreenUtils
             ,
               _scModelUtils
			 ,
			   _scBaseUtils
			 ,
			 
			 _isccsUIUtils
){ 
	return _dojodeclare("extn.customer.search.CustomerSearchExtn", [_extnCustomerSearchExtnUI],{
	// custom code here
	
	extn_createConsumerOnClick:function(event, bEvent, ctrl, args){
			  var model = _scBaseUtils.getTargetModel(this,"getCustomerList_input",null);
              var customerType = _scModelUtils.getStringValueFromPath("Customer.CustomerType",model);
              var organizationCode = _scModelUtils.getStringValueFromPath("Customer.CallingOrganizationCode",model);
              var customerModel= _scBaseUtils.getNewModelInstance();	 
	          _scModelUtils.setStringValueAtModelPath("Customer.CustomerType",customerType, customerModel);
              _scModelUtils.setStringValueAtModelPath("Customer.OrganizationCode", organizationCode, customerModel);
              _isccsUIUtils.openWizardInEditor("isccs.customer.wizards.createConsumer.CreateConsumerWizard", customerModel, "isccs.editors.CustomerEditor", this); 
	}
	
});
});

