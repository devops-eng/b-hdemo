scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/editors/HomeEditorRTExtnUI", "scbase/loader!isccs/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!isccs/utils/RelatedTaskUtils", "scbase/loader!isccs/utils/OrderUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!isccs/utils/UIUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!sc/plat/dojo/utils/EventUtils"], function(_dojodeclare, _extnHomeEditorRTExtnUI, _isccsUIUtils, _scBaseUtils, _isccsRelatedTaskUtils, _isccsOrderUtils, _scModelUtils, _isccsUIUtils, _extnExpicientUtils, _scEventUtils) {
    return _dojodeclare("extn.editors.HomeEditorRTExtn", [_extnHomeEditorRTExtnUI], {
        // custom code here
        extn_afterScreenInit: function(event, bEvent, ctrl, args) {
            var hashValue = window.location.hash.split('#').splice(1).toString();
            var that = this;
            var argument = args;
            if (_scBaseUtils.equals(hashValue, "OS")) {
                var args = {};
                //setTimeout(myFunction, 1500);
                setTimeout(function() {
                    myFunc();
                }, 1000);

                function myFunc() {
                    _scEventUtils.fireEventInsideScreen(that, "lnk_RT_OrderSearch_onClick", null, argument);
                }
            }
        }
    });
});
