scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/editors/OrderEditorRTExtnUI","scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!isccs/utils/UIUtils","scbase/loader!sc/plat/dojo/utils/ScreenUtils","scbase/loader!sc/plat/dojo/utils/ModelUtils","scbase/loader!sc/plat/dojo/utils/PlatformUIFmkImplUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnOrderEditorRTExtnUI
                      ,
                          _scBaseUtils
                      ,
                         _isccsUIUtils
                      ,
                         _scScreenUtils
                      ,
                         _scModelUtils
                      ,  _scPlatformUIFmkUtils
){ 
	return _dojodeclare("extn.editors.OrderEditorRTExtn", [_extnOrderEditorRTExtnUI],{
	// custom code here

    extn_sendOrderEmail:function(event, bEvent, ctrl, args) {
    var OrderDetails =_scScreenUtils.getInitialInputData(this);
    var emailId=OrderDetails.Order.CustomerEMailID;
    var email = _scModelUtils.getStringValueFromPath("Order.CustomerEMailID",OrderDetails);
	var popupParams=null;
	var dialogParams= null;
	 popupParams = _scBaseUtils.getNewBeanInstance();
	 dialogParams = _scBaseUtils.getNewBeanInstance();
     _scBaseUtils.setAttributeValue("orderDetails",OrderDetails,popupParams);
     _scBaseUtils.setAttributeValue("emailId",emailId,popupParams);
	_scBaseUtils.setAttributeValue("closeCallBackHandler", "extn_handleSendEmailMsg", dialogParams);
	_isccsUIUtils.openSimplePopup("isccs.order.create.orderEntryDialog.OnPreviousDialog", "Send Order EMail", this, popupParams,dialogParams);
	},
     extn_callCopyOrder:function(event, bEvent, ctrl, args) {
		var OrderDetails =_scScreenUtils.getInitialInputData(this);
        var inputModel = _scBaseUtils.getNewModelInstance();
		     var OrderNo=_scModelUtils.getStringValueFromPath("Order.OrderNo",OrderDetails);
        var OrderName=" Copy of " + OrderNo;
 		     _scModelUtils.setStringValueAtModelPath("Order.DocumentType",_scModelUtils.getStringValueFromPath("Order.DocumentType",OrderDetails),inputModel);
     		     _scModelUtils.setStringValueAtModelPath("Order.EntryType",_scModelUtils.getStringValueFromPath("Order.EntryType",OrderDetails),inputModel);

     _scModelUtils.setStringValueAtModelPath("Order.DraftOrderFlag","Y",inputModel);
        _scModelUtils.setStringValueAtModelPath("Order.AuthorizedClient",_scModelUtils.getStringValueFromPath("Order.EntryType",OrderDetails),inputModel);
_scModelUtils.setStringValueAtModelPath("Order.OrderName",OrderName,inputModel);
     _scModelUtils.setStringValueAtModelPath("Order.ValidateItem","N",inputModel);
var EnteredBy=_scPlatformUIFmkUtils.getUserId();
     _scModelUtils.setStringValueAtModelPath("Order.EnteredBy",EnteredBy,inputModel);
     
        _scModelUtils.setStringValueAtModelPath("Order.CopyFromOrderHeaderKey",_scModelUtils.getStringValueFromPath("Order.OrderHeaderKey",OrderDetails),inputModel);

		_isccsUIUtils.callApi(this, inputModel, "extn_copyOrder_mashup", null);
		
	},

	extn_handleCopyOrder:function(event, bEvent, ctrl, args) {
		var mashupArrayObj = args.mashupArray;
		var mashupOutput = null;
		
		for(var obj in mashupArrayObj){
			if((mashupArrayObj[obj].mashupRefId == "extn_copyOrder_mashup")){
				mashupOutput = mashupArrayObj[obj].mashupRefOutput;
				_isccsUIUtils.openWizardInEditor("isccs.order.wizards.createOrder.CreateOrderWizard", mashupOutput, "isccs.editors.OrderEditor", this);

			}
			else if((mashupArrayObj[obj].mashupRefId == "extn_getUserGrpForPopUpMashup")){
				mashupOutput = mashupArrayObj[obj].mashupRefOutput;			
				var userGroupList  = _scModelUtils.getStringValueFromPath("User.UserGroupLists.UserGroupList",mashupOutput);
				_scScreenUtils.setModel(this, "extn_commonCodeForUserGroup", userGroupList, null);
                             var commonCodeModel= _scBaseUtils.getNewModelInstance();
                             var inputData = _scScreenUtils.getInitialInputData(this);
                             var sOrgCode = _scModelUtils.getStringValueFromPath("Order.EnterpriseCode",inputData);
                             _scModelUtils.setStringValueAtModelPath("CommonCode.OrganizationCode",sOrgCode,commonCodeModel);
							 _isccsUIUtils.callApi(this,commonCodeModel,"extn_getCommonCodeListUserGroup",null);
				 				
				 
			}	
			else if((mashupArrayObj[obj].mashupRefId == "extn_getCommonCodeListUserGroup")){
				mashupOutput = mashupArrayObj[obj].mashupRefOutput;
				var commonCodeUserList=_scModelUtils.getStringValueFromPath("CommonCodeList.CommonCode",mashupOutput);
		              this.extn_showPopUpForHoldHandler(commonCodeUserList);
			}
	
		}
	},

extn_showPopUpForHold:function(event, bEvent, ctrl, args){
		var inputData = _scScreenUtils.getInitialInputData(this);
		var holdList = _scModelUtils.getStringValueFromPath("Order.OrderHoldTypes.OrderHoldType",inputData);
		var sActiveHoldStatus = "1100";		
		var sHoldType = "REVIEW_NOTES";
		 for(var i in holdList)
                            {
                                var status  = _scModelUtils.getStringValueFromPath("Status", holdList[i]);
								var holdType = _scModelUtils.getStringValueFromPath("HoldType", holdList[i]);								
								if(status==sActiveHoldStatus && holdType==sHoldType){
								 var userModel= _scBaseUtils.getNewModelInstance();							
							     _isccsUIUtils.callApi(this,userModel,"extn_getUserGrpForPopUpMashup",null);
								 sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
								}
                            }
	},

extn_showPopUpForHoldHandler:function(commonCodeUserList){
		var flag = false;
	    var textObj = _scBaseUtils.getNewBeanInstance();
		var sProceedBtn = null;
		sProceedBtn = _scScreenUtils.getString(this, "extn_Proceed_btn");
	    textObj.OK = sProceedBtn;	    
	    var dialogParams = _scBaseUtils.getNewBeanInstance();
	    var userGroupList= _scScreenUtils.getModel(this, "extn_commonCodeForUserGroup");	
	    var sPopUpMessage = null;
		sPopUpMessage = _scScreenUtils.getString(this, "extn_ReviewHold_PoUp");
	    for(var index in commonCodeUserList)
		{
            for(var i in userGroupList)
                            {
                            	
                                var userGroupId  = _scModelUtils.getStringValueFromPath("UserGroup.UsergroupId", userGroupList[i]);    
                                if(_scBaseUtils.isVoid(userGroupId))
                                {
                                	var userGroup = _scModelUtils.getStringValueFromPath("UserGroup", userGroupList[i]);
                                	for(var j in userGroup)
                                	{
                                		userGroupId  = _scModelUtils.getStringValueFromPath("UsergroupId", userGroup[j]);
                                	}
                                }
			           var commonCodeUserGrpId=_scModelUtils.getStringValueFromPath("CodeValue", commonCodeUserList[index]);
                            	if(userGroupId==commonCodeUserGrpId){
								
                            		_scScreenUtils.showConfirmMessageBox(this,sPopUpMessage,"extn_createReturnDraft",textObj,dialogParams);
						flag = true;
						break;
                            	}
                            }
              }
	    	if(!flag){
	    		_scScreenUtils.showInfoMessageBox(this,sPopUpMessage,null,null);
	    	}

	    
	},

	extn_createReturnDraft:function(event, bEvent, ctrl, args){
		var inputData = _scScreenUtils.getInitialInputData(this);
		var orderHeaderKey = _scModelUtils.getStringValueFromPath("Order.OrderHeaderKey", inputData);
		var test = null;
		if(event=="Ok"){
			 var InputModel=_scBaseUtils.getNewModelInstance();
             _scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", orderHeaderKey, InputModel);
			_isccsUIUtils.callApi(this,InputModel,"extn_changeOrderToResolveHold",null);
			isccs.utils.EditorRelatedTaskUtils.createReturn_onClickHandler();
		}	
		
	},

	extn_checkForMigartedAndMultiCC:function(event, bEvent, ctrl, args){
		var inputData = _scScreenUtils.getInitialInputData(this);
		var isReturnElgible = true;
		var messageDetails = "";
		var orderType = _scModelUtils.getStringValueFromPath("Order.OrderType", inputData);
		if(_scBaseUtils.stringEquals(orderType,"MIGRATED")){
			isReturnElgible = false;
			messageDetails = _scScreenUtils.getString(this, "extn_migrated_order_error"); //"This is a Migrated order. Please return through POS"
		}else{
			var paymentMethodsList = _scModelUtils.getModelListFromPath("Order.PaymentMethods.PaymentMethod", inputData); 
			var ccCount = 0;
			for(var paymentCount in paymentMethodsList){
				var paymentType =  _scModelUtils.getStringValueFromPath("PaymentType", paymentMethodsList[paymentCount]);
				if(_scBaseUtils.contains(paymentType, "CREDIT_CARD")){
					ccCount++;
				}
			}
			if(ccCount > 1){
				isReturnElgible = false;
				messageDetails = _scScreenUtils.getString(this, "extn_multiple_cc_error");//"Order has multiple CC. Please return through POS"
			}

		}
		if(!isReturnElgible){
			sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
			_scScreenUtils.showErrorMessageBox(this, messageDetails, null,null,null);
			//_scScreenUtils.showInfoMessageBox(this, messageDetails, null,null,null);
			//_scScreenUtils.showWarningMessageBoxWithOk(this, messageDetails, null,null,null);

		}
	
	}
});
});

