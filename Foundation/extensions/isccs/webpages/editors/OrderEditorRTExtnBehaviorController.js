


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/editors/OrderEditorRTExtn","scbase/loader!sc/plat/dojo/controller/ExtnServerDataController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnOrderEditorRTExtn
			 ,
			    _scExtnServerDataController
){

return _dojodeclare("extn.editors.OrderEditorRTExtnBehaviorController", 
				[_scExtnServerDataController], {

			
			 screenId : 			'extn.editors.OrderEditorRTExtn'

			
			
			
			
			
						,

			
			
			 mashupRefs : 	[
	 		{
		 extnType : 			'ADD'
,
		 mashupId : 			'extn_sendEMailMashUp'
,
		 mashupRefId : 			'extn_sendEMailMashUp'

	}
,
	 		{
		 extnType : 			'ADD'
,
		 mashupId : 			'extn_copyOrder_mashup'
,
		 mashupRefId : 			'extn_copyOrder_mashup'

	}
,
	 		{
		 extnType : 			'ADD'
,
		 mashupId : 			'extn_getUserGrpForPopUpMashup'
,
		 mashupRefId : 			'extn_getUserGrpForPopUpMashup'

	}
,
	 		{
		 extnType : 			'ADD'
,
		 mashupId : 			'extn_getCommonCodeListUserGroup'
,
		 mashupRefId : 			'extn_getCommonCodeListUserGroup'

	}
,
	 		{
		 extnType : 			'ADD'
,
		 mashupId : 			'extn_changeOrderToResolveHold'
,
		 mashupRefId : 			'extn_changeOrderToResolveHold'

	}

	]

}
);
});

