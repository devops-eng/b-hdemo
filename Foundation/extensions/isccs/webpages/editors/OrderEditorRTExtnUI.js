
scDefine(["dojo/text!./templates/OrderEditorRTExtn.html","scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/_base/lang","scbase/loader!dojo/text","scbase/loader!idx/layout/TitlePane","scbase/loader!sc/plat","scbase/loader!sc/plat/dojo/binding/CurrencyDataBinder","scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!sc/plat/dojo/widgets/Link"]
 , function(			 
			    templateText
			 ,
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojolang
			 ,
			    _dojotext
			 ,
			    _idxTitlePane
			 ,
			    _scplat
			 ,
			    _scCurrencyDataBinder
			 ,
			    _scBaseUtils
			 ,
			    _scLink
){
return _dojodeclare("extn.editors.OrderEditorRTExtnUI",
				[], {
			templateString: templateText
	
	
	
	
	
	
					,	
	namespaces : {
		targetBindingNamespaces :
		[
		],
		sourceBindingNamespaces :
		[
			{
	  value: 'extn_commonCodeForUserGroup'
						,
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_0'
						,
	  description: "extn_commonCodeForUserGroup"
						
			}
			
		]
	}

	
	,
	hotKeys: [ 
	]

,events : [
	]

,subscribers : {

local : [

{
	  eventId: 'extn_sendEMail_onClick'

,	  sequence: '51'

,	  description: 'extn_sendOrderEmail'



,handler : {
methodName : "extn_sendOrderEmail"

 
}
}
,
{
	  eventId: 'onExtnMashupCompletion'

,	  sequence: '51'

,	  description: 'extn_handleCopyOrder'



,handler : {
methodName : "extn_handleCopyOrder"

 
}
}
,
{
	  eventId: 'extn_copyOrder_onClick'

,	  sequence: '51'




,handler : {
methodName : "extn_callCopyOrder"

 
}
}
,
{
	  eventId: 'createReturn_onClick'

,	  sequence: '19'

,	  description: 'showPopUpForHold'



,handler : {
methodName : "extn_showPopUpForHold"

 
}
}
,
{
	  eventId: 'createReturn_onClick'

,	  sequence: '18'

,	  description: 'check for Migrated order type and multiple credit cards'



,handler : {
methodName : "extn_checkForMigartedAndMultiCC"

 
}
}

]
}

});
});


