scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/lang", "scbase/loader!sc/plat", "scbase/loader!sc/plat/dojo/utils/WidgetUtils",
    // To instantiate all widgets
    "scbase/loader!dijit/form/Button", "scbase/loader!idx/form/CheckBox", "scbase/loader!idx/form/CurrencyTextBox", "scbase/loader!sc/plat/dojo/widgets/DataLabel", "scbase/loader!dijit/form/TextBox", "scbase/loader!sc/plat/dojo/widgets/Label", "scbase/loader!idx/form/NumberTextBox", "scbase/loader!idx/form/Textarea", "scbase/loader!idx/form/TextBox", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!dojo/currency", "scbase/loader!sc/plat/dojo/utils/BundleUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!isccs/utils/OrderLineUtils"
], function(_dojodeclare, dLang, scPlat, scWidgetUtils, // To instantiate widgets
    dijit_form_Button, idx_form_CheckBox, idx_form_CurrencyTextBox, sc_plat_dojo_widgets_DataLabel, dijit_form_TextBox, sc_plat_dojo_widgets_Label, idx_form_NumberTextBox, idx_form_Textarea, idx_form_TextBox, _scBaseUtils, _scModelUtils, dCurrency, scBundleUtils, scScreenUtils, orderLineUtils) {
    var expicientUtil = dLang.getObject("dojo.utils.ExpicientUtil", true, scPlat);
    expicientUtil.extn_getExpectedDate = function(gridReference, rowIndex, columnIndex, gridRowJSON, unformattedValue, value, gridRowRecord, colConfig) {
        var str = '<div class="fulfillmentAddress">';
        str += '<div class="fulfillmentAddress">';
        var delMethod = _scModelUtils.getStringValueFromPath("DeliveryMethod", gridRowJSON);
        if (_scBaseUtils.equals(delMethod, "SHP")) {
            var expOnAchorTag = orderLineUtils.getAnchorTag(colConfig.bindingData, gridRowJSON, gridReference, gridRowRecord, 'ExpectedOn', 'ExpectedOn');
            str += expOnAchorTag;
            str += '</div>';
            str += '<div class="fulfillmentAddress">';
            var bundleUnitPriceTag = orderLineUtils.getAnchorTag(colConfig.bindingData, gridRowJSON, gridReference, gridRowRecord, 'BundleUnitPtice', 'BundleUnitPtice');
            str += bundleUnitPriceTag;
            str += '</div>';
            str += '</div>';
            str += '<div class="fulfillmentAddress">';
            var trackAnchorTag = orderLineUtils.getAnchorTag(colConfig.bindingData, gridRowJSON, gridReference, gridRowRecord, 'TrackingNo', 'TrackingNo');
            str += trackAnchorTag;
            str += '</div>';
        } else if (_scBaseUtils.equals(delMethod, "PICK")) {
            var extnOrderDate = null;
            extnOrderDate = _scModelUtils.getModelObjectFromPath("Extn.ExtnExpectedShipmentDate", gridRowJSON);
            var formattedDate = null;
            formattedDate = _scBaseUtils.formatDateToUserFormat(extnOrderDate);
            str += formattedDate;
            str += '</div></div>';
        }
        return str;
    };
    expicientUtil.extn_getDeliveryMethod = function(gridReference, rowIndex, columnIndex, gridRowJSON, unformattedValue, value, gridRowRecord, colConfig) {
        if (gridRowJSON && gridRowJSON.DeliveryMethod) {
            if (_scBaseUtils.equals(gridRowJSON.DeliveryMethod, "SHP")) {
                if (orderLineUtils.isShipTogetherBundle(gridRowJSON)) {
                    return scBundleUtils.getString("ShipIndBundleFulfillment");
                } else {
                    unformattedValue = orderLineUtils.handleDeliveryMethod(gridReference, rowIndex, columnIndex, gridRowJSON, unformattedValue);
                    return unformattedValue;
                }
            } else if (_scBaseUtils.equals(gridRowJSON.DeliveryMethod, "PICK")) {
                unformattedValue = _scModelUtils.getStringValueFromPath("DeliveryMethod", gridRowJSON);
                return unformattedValue;
            } else {
                return unformattedValue;
            }
        }
        return unformattedValue;
    };
    expicientUtil.extn_GetColor = function(gridReference, rowIndex, columnIndex, gridRowJSON, unformattedValue, value, gridRowRecord, colConfig) {
        var attributeList = gridRowJSON.ItemDetails.AdditionalAttributeList.AdditionalAttribute;
        var unformattedValue="";
        for (var index in attributeList) {
            if (attributeList[index].Name == "Color") {
                unformattedValue = attributeList[index].Value;
            }
        }
        return unformattedValue;
    };
    expicientUtil.extn_GetSize = function(gridReference, rowIndex, columnIndex, gridRowJSON, unformattedValue, value, gridRowRecord, colConfig) {
        var attributeList = gridRowJSON.ItemDetails.AdditionalAttributeList.AdditionalAttribute;
        var unformattedValue="";
        for (var index in attributeList) {
            if (attributeList[index].Name == "Size") {
                unformattedValue = attributeList[index].Value;
            }
        }
        return unformattedValue;
    };
    expicientUtil.getLineTotal = function(gridReference, rowIndex, columnIndex, gridRowJSON, unformattedValue, value, gridRowRecord, colConfig) {
            var unitprice = null;
            unitprice = _scModelUtils.getStringValueFromPath("LineOverallTotals.LineTotal", gridRowJSON);
            if (unitprice != scBundleUtils.getString("Included")) {
                //Format currency since not supported on multiline cells.  Only do if not "Included".
                var screenInstance = gridReference.ownerScreen;
                var orderNamespace = "getCompleteOrderLineList_output";
                if (!(_scBaseUtils.isVoid(screenInstance.lstOutputNamespace))) {
                    orderNamespace = screenInstance.lstOutputNamespace;
                }
                var currencyDtlsFromOrder = scScreenUtils.getModel(screenInstance, orderNamespace);
                var countryCode = _scBaseUtils.getAttributeValue("Order.PriceInfo.Currency", false, gridRowJSON);
                if (!countryCode) {
                    countryCode = _scBaseUtils.getAttributeValue("OrderLineList.Order.PriceInfo.Currency", false, currencyDtlsFromOrder);
                }
                if (!countryCode) {
                    countryCode = _scBaseUtils.getAttributeValue("Page.Output.OrderLineList.Order.PriceInfo.Currency", false, currencyDtlsFromOrder);
                }
                if (!currencyDtlsFromOrder || !countryCode) {
                    orderNamespace = "getCompleteOrderDetails_output";
                    if (!(_scBaseUtils.isVoid(screenInstance.lstOrderNamespace))) {
                        orderNamespace = screenInstance.lstOrderNamespace;
                    }
                    currencyDtlsFromOrder = scScreenUtils.getModel(screenInstance, orderNamespace);
                    if (currencyDtlsFromOrder) countryCode = _scBaseUtils.getAttributeValue("Order.PriceInfo.Currency", false, currencyDtlsFromOrder);
                }
                unitprice = dCurrency.format(unitprice, {
                    currency: countryCode
                });
            }
            //  return _extnExpicientUtils.getLineTotal(unitprice);
            var title = "Click to view detailed line price information.";
            str = '<div class="fulfillmentAddress"><a href="javascript:void(0)" title="' + title + '" linkUId="' + "LineTotal" + '">' + unitprice + '</a></div>';
            return str;
        },
        /**
         * this is for afterBehaviorMashupCall
         */
        expicientUtil.getMashupOutputForRefID = function(mashrefID, output) {
            var mashupArray = output.mashupArray;
            var length = _scBaseUtils.getAttributeCount(mashupArray);
            var isRefIdAvailable = false;
            for (var index = 0; _scBaseUtils.lessThan(index, length); index = _scBaseUtils.incrementNumber(index, 1)) {
                var mashupDetail = null;
                mashupDetail = _scBaseUtils.getArrayBeanItemByIndex(mashupArray, index);
                var mashupRefId = mashupDetail.mashupRefId;
                if (_scBaseUtils.equals(mashrefID, mashupRefId)) {
                    var mashupOutput = mashupDetail.mashupRefOutput;
                    return mashupOutput;
                }
            }
        },
        /**
         * this is for afterBehaviorMashupCall
         */
        expicientUtil.isMashupAvailable = function(mashrefID, output) {
            var mashupArray = output.mashupArray;
            var length = _scBaseUtils.getAttributeCount(mashupArray);
            var isRefIdAvailable = false;
            for (var index = 0; _scBaseUtils.lessThan(index, length); index = _scBaseUtils.incrementNumber(index, 1)) {
                var mashupDetail = null;
                mashupDetail = _scBaseUtils.getArrayBeanItemByIndex(mashupArray, index);
                var mashupRefId = mashupDetail.mashupRefId;
                if (_scBaseUtils.equals(mashrefID, mashupRefId)) {
                    isRefIdAvailable = true;
                }
            }
            return isRefIdAvailable;
        },
        /**
         * this is for beforeBehaviourMashupCall
         */
        expicientUtil.isBeforeBehaviorMashupAvailable = function(mashrefID, output) {
            var mashupRefs = output.mashupRefs;
            var length = _scBaseUtils.getAttributeCount(mashupRefs);
            var isRefIdAvailable = false;
            for (var index = 0; _scBaseUtils.lessThan(index, length); index = _scBaseUtils.incrementNumber(index, 1)) {
                var mashupDetail = null;
                mashupDetail = _scBaseUtils.getArrayBeanItemByIndex(mashupRefs, index);
                var mashupRefId = mashupDetail.mashupRefId;
                if (_scBaseUtils.equals(mashrefID, mashupRefId)) {
                    isRefIdAvailable = true;
                }
            }
            return isRefIdAvailable;
        },
        /**
         * * this is for beforeBehaviourMashupCall input
         */
        expicientUtil.getMashupInputForRefID = function(mashrefID, output) {
            var mashupRefs = output.mashupRefs;
            var length = _scBaseUtils.getAttributeCount(mashupRefs);
            var isRefIdAvailable = false;
            for (var index = 0; _scBaseUtils.lessThan(index, length); index = _scBaseUtils.incrementNumber(index, 1)) {
                var mashupDetail = null;
                mashupDetail = _scBaseUtils.getArrayBeanItemByIndex(mashupRefs, index);
                var mashupRefId = mashupDetail.mashupRefId;
                if (_scBaseUtils.equals(mashrefID, mashupRefId)) {
                    var mashupOutput = mashupDetail.mashupInputObject;
                    return mashupOutput;
                }
            }
        },
        expicientUtil.testHello = function() {
            alert("jai ho");
        }
        /*
         Scope and information about this util:
         1. The util allows creation, placement and automatic association of events for simple widgets listed under a json
         _widgetTypeToVariableMap. Any widgets required in future may be imported, added to this map for use in instantiation.
         NOTE: complex widgets like: Grid, Combobox, Containers are not supported currently. They will require some more custom
         logic(as per the requirements of the application) to instantiate and place.
         2. This util allows users to bind controls to the existing namespaces only. It does not support new namespace creation on screen.
         3. As Address panels are going to be customized, there may be some widgets like AddressLine2 that may appear only for a few
         locales. Thus, when it is used as referenceWidgetUId, it will only lead to correct insertion when it is present. When not
         found, insertion will not occur.
         Application will need to add more custom if() blocks in AddressPanelExtn to use the correct referenceWidgetUId to achieve
         the desired result on UI.
         The method screen.getIdentifierId() will give the identifier id (typically locale) for an Address panel. Application may use
         this to choose the correct referenceWidgetUId for identifiers.
         4. The solution provided here is a custom solution. Extensibility support will be attempted for this approach, but it may or
         may not be available.
         */
        // _widgetTypeToVariableMap: [private] object
        //      Maintain a json map of the "widgetType" to imported variable name. Used to instantiate widget and this is better
        //      than using switch(). Update this as and when more widgets are imported.
        //      NOTE: The keys act as the list of allowed "widgetType".
    var _widgetTypeToVariableMap = {
        "button": dijit_form_Button,
        "checkbox": idx_form_CheckBox,
        "currencytextbox": idx_form_CurrencyTextBox,
        "datalabel": sc_plat_dojo_widgets_DataLabel,
        "hiddenfield": dijit_form_TextBox, // NOTE: you will need to pass renderHidden as true or use css in widgetProperties.
        "label": sc_plat_dojo_widgets_Label,
        "numbertextbox": idx_form_NumberTextBox,
        "textarea": idx_form_Textarea,
        "textfield": idx_form_TextBox
    };
    expicientUtil.createAndPlaceWidget = function(widgetType, widgetProperties, referenceWidgetUId, position, screenInstance) {
        // summary:
        //      This method will create a new widget instance and then place the newly created widget on the screen at the
        //      specified position on the screen wrt the reference widget.
        //      This method returns the widget instance created or null if widget could not be created.
        //
        //  widgetType: string
        //      Mandatory. It must be a string. It signifies the type of widget required. "_widgetTypeToVariableMap"
        //      contains all supported widgetTypes as keys. Any additions/modifications must be correctly updated.
        //  widgetProperties: object
        //      Mandatory. It is a json containing all the required widget properties. Additionally, this json must contain
        //      a "uId" attribute.
        //  referenceWidgetUId: string
        //      Mandatory. The uId of the existing widget wrt which the new widget should be placed. If the widget requested
        //      is not found, then neither is a new widget instance create nor is it inserted on screen.
        //      E.g.:
        //      If the requirement is to add an address line after 2nd address line widget, then "referenceWidgetUId"
        //      becomes the uId of the 2nd address line widget.
        //  position: string
        //      Optional. Specify the position where the new widget is required to be added wrt the "referenceWidgetUId".
        //      Allowed values: "before", "after", "first" and "last". Defaults to "after" if not passed.
        //      E.g.:
        //      In the above example position is "after".
        //  screenInstance: Screen
        //      Mandatory. The instance of the screen in which the widget is required to be inserted.
        var canInsertWidget = true,
            createdWidgetInstance = null;
        if (widgetType == null || typeof widgetType != "string") {
            canInsertWidget = false;
            console.error("widgetType must be a string. Passed value: ", widgetType);
        } else if (widgetProperties == null || dLang.getObject("uId", false, widgetProperties) == null) {
            canInsertWidget = false;
            console.error("widgetProperties was either null or missing mandatory uId. Passed value: ", widgetProperties);
        } else if (referenceWidgetUId == null || typeof referenceWidgetUId != "string") {
            canInsertWidget = false;
            console.error("referenceWidgetUId must be a string. Passed value: ", referenceWidgetUId);
        } else if (screenInstance == null) {
            canInsertWidget = false;
            console.error("screenInstance was passed as null.");
        }
        if (canInsertWidget == true) {
            var processedWidgetProps = {};
            widgetType = widgetType.toLowerCase();
            processedWidgetProps[widgetType] = widgetProperties;
            processedWidgetProps = scWidgetUtils._createParamsFromBean(widgetType, screenInstance, widgetProperties.uId, processedWidgetProps);
            if (_widgetTypeToVariableMap[widgetType] != null) {
                if (screenInstance["uIdMap"][referenceWidgetUId] != null) {
                    createdWidgetInstance = new _widgetTypeToVariableMap[widgetType](processedWidgetProps);
                    if (position == null) {
                        position = "after";
                    }
                    screenInstance["uIdMap"][processedWidgetProps.uId] = createdWidgetInstance;
                    scWidgetUtils._addSubscriberEventsFromConfig(screenInstance, processedWidgetProps.uId, processedWidgetProps);
                    scWidgetUtils.placeAt(screenInstance, referenceWidgetUId, processedWidgetProps.uId, position);
                    _associateWidgetHandlersToScreen(screenInstance, createdWidgetInstance);
                } else {
                    // referenceWidgetUId was not found. No need to announce it as some calls may be made from prototypes for AddressPanel.
                }
            } else {
                canInsertWidget = false;
                console.error("Unsupported widget. Import widget, update _widgetTypeToVariableMap map and then continue.");
            }
        }
        return createdWidgetInstance;
    };
    var _associateWidgetHandlersToScreen = function(screenInstance, createdWidgetInstance) {
        // summary: [private]
        //      This method associates change/validation related handlers for a newly created widget on its parent screen.
        //      It is invoked once a widget is added to screen. If this method is not called, the newly created widgets
        //      (esp. editable widgets) will need to be handled separately to track dirty/validation.
        //      NOTE:
        //      If this method is not called, then the screen will not be marked dirty(*) automatically whenever a value
        //      changes. However, if the isDirty(null, true) method is called on screen, correct response will be returned.
        //
        //  screenInstance: Screen
        //      The screen on which the widget has been added. All handlers for the widget events will be associated to this
        //      screen.
        //  createdWidgetInstance: Widget
        //      The widget that has been added to screen.
        var conns = screenInstance._changeConnections;
        if (conns == null) {
            conns = screenInstance._changeConnections = [];
        }
        conns.push(screenInstance.connect(createdWidgetInstance, "onChange", dLang.hitch(screenInstance, "_checkDirtyOnWidgetChange", createdWidgetInstance)));
        conns.push(screenInstance.connect(createdWidgetInstance, "onChange", dLang.hitch(screenInstance, "_checkInvalidOnWidgetChange", createdWidgetInstance)));
        conns.push(screenInstance.connect(createdWidgetInstance, "validate", dLang.hitch(screenInstance, "_checkInvalidOnWidgetChange", createdWidgetInstance)));
        conns.push(screenInstance.connect(createdWidgetInstance, "_onClean", dLang.hitch(screenInstance, "_checkScreenForCleanOnWidgetClean", createdWidgetInstance)));
        conns.push(screenInstance.connect(createdWidgetInstance, "_afterDestroyRecursive", dLang.hitch(screenInstance, "_removeWidgetForNamespaceMaps", createdWidgetInstance)));
    };
    return expicientUtil;
});
