scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/home/portlets/OrderPortletExtnUI", "scbase/loader!isccs/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!isccs/utils/RelatedTaskUtils", "scbase/loader!isccs/utils/OrderUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!isccs/utils/UIUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil"], function(_dojodeclare, _extnOrderPortletExtnUI, _isccsUIUtils, scBaseUtils, _isccsRelatedTaskUtils, _isccsOrderUtils, _scModelUtils, _isccsUIUtils, _extnExpicientUtils) {
    return _dojodeclare("extn.home.portlets.OrderPortletExtn", [_extnOrderPortletExtnUI], {
        portletScreen:null,
        extn_afterInitialize: function() {
            //var wizardID = location.search.split('ScreenID=').splice(1).join('').split('&')[0];
            //var orderHeaderKey = location.search.split('OrderHeaderKey=').splice(1).join('').split('&')[0];
            var orderHeaderKey = location.hash.split('OrderHeaderKey=').splice(1).join('');
            this.portletScreen = this;
            var hashValue = window.location.hash.split('#').splice(1).toString();
            /*window.onhashchange = function() {
                var hash = window.location.hash.split('#').splice(1).toString();
                orderHeaderKey = location.hash.split('OrderHeaderKey=').splice(1).join('');
                if (!scBaseUtils.isVoid(orderHeaderKey)) {
                    _isccsUIUtils.openWizardInEditor("isccs.order.wizards.orderSummary.OrderSummaryWizard", {
                        Order: {
                            OrderHeaderKey: orderHeaderKey
                        }
                    }, "isccs.editors.OrderEditor", portletScreen);
                } else if (scBaseUtils.equals(hash, "OS")) {
                    var taskInput = null;
                    var taskConfig = null;
                    taskInput = scBaseUtils.getNewModelInstance();
                    taskInput = {
                        "Order": {
                            "AdvancedSearch": "true",
                            "FireSearch": "false"
                        }
                    };
                    _isccsUIUtils.openWizardInEditor("isccs.order.wizards.orderSearch.OrderSearchWizard", taskInput, "isccs.editors.OrderSearchEditor", portletScreen);
                    _isccsOrderUtils.openOrderSearch(portletScreen, taskInput, "getOrderList");
                }
            }*/
            if (!scBaseUtils.isVoid(orderHeaderKey)) {
                var orderModel = _scModelUtils.createNewModelObjectWithRootKey("Order");
                _scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", orderHeaderKey, orderModel);
                _isccsUIUtils.callApi(this.portletScreen, orderModel, "extn_getOrderDetailsSSO_ref");
                /* _isccsUIUtils.openWizardInEditor("isccs.order.wizards.orderSummary.OrderSummaryWizard", {
                     Order: {
                         OrderHeaderKey: orderHeaderKey
                     }
                 }, "isccs.editors.OrderEditor", this);*/
            } /*else if (scBaseUtils.equals(hashValue, "OS")) {
                var taskInput = null;
                var taskConfig = null;
                taskInput = scBaseUtils.getNewModelInstance();
                   _isccsOrderUtils.openOrderSearch(this, taskInput, "getOrderList");
            }*/
        },
        extn_afterBehaviorMashupCall: function(event, bEvent, ctrl, args) {
            var isMashupAvailable = _extnExpicientUtils.isMashupAvailable("extn_getOrderDetailsSSO_ref", args);
            if (isMashupAvailable) {
                var availableQuantity = null;
                var modelOutput = null;
                modelOutput = _extnExpicientUtils.getMashupOutputForRefID("extn_getOrderDetailsSSO_ref", args);
                var order = _isccsOrderUtils.getEditorInputForOrder(modelOutput);
                _isccsOrderUtils.openOrder(this.portletScreen, order);
            }
        },
        extn_hashChanged: function() {
                alert("hashChanged");
            }
            // custom code here
    });
});
