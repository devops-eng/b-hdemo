scDefine(["scbase/loader!dojo/_base/declare", 
    "scbase/loader!extn/item/details/ItemDetailsExtnUI", 
    "scbase/loader!isccs/utils/ContextUtils",
        "scbase/loader!isccs/utils/UIUtils", 
        "scbase/loader!sc/plat/dojo/utils/BaseUtils", 
        "scbase/loader!sc/plat/dojo/utils/ModelUtils", 
        "scbase/loader!sc/plat/dojo/utils/WidgetUtils",
        "scbase/loader!sc/plat/dojo/utils/ScreenUtils",
        "scbase/loader!dojo/currency"
    ],
    function(
        _dojodeclare,
        _extnItemDetailsExtnUI,
        _isccsContextUtils,
        _isccsUIUtils,
        _scBaseUtils,
        _scModelUtils,
        _scWidgetUtils,
        _scScreenUtils,
        dCurrency
    ) {
        return _dojodeclare("extn.item.details.ItemDetailsExtn", [_extnItemDetailsExtnUI], {
            // custom code here
            // Overriden for Image
            getItemImageUrl: function(screen, widget, nameSpace, itemModel, options, dataValue) {
                var displayImage = "";
                displayImage = _isccsContextUtils.getDisplayItemImageRule();
                if (
                    _scBaseUtils.equals(
                        displayImage, "N")) {
                    var retVal = "";
                    retVal = _isccsUIUtils.fetchBlankImage(
                        this);
                    _scWidgetUtils.hideWidget(
                        this, "moreImagesLink", false);
                    return retVal;
                }
                var assetList = null;
                var imageUrl = "";
                var length = 0;
                var hasMoreImages = false;
                assetList = _scModelUtils.getModelListFromPath("Item.AssetList.Asset", itemModel);
                length = _scBaseUtils.getAttributeCount(
                    assetList);
                for (
                    var index = 0; _scBaseUtils.lessThan(
                        index, length); index = _scBaseUtils.incrementNumber(
                        index, 1)) {
                    var assetElem = null;
                    var assetType = null;
                    if (!(
                            _scBaseUtils.isVoid(
                                assetList))) {
                        assetElem = _scBaseUtils.getArrayBeanItemByIndex(
                            assetList, index);
                        assetType = _scBaseUtils.getAttributeValue("Type", false, assetElem);
                        if (
                            _scBaseUtils.equals(
                                assetType, "ITEM_IMAGE_1")) {
                            imageUrl = _scBaseUtils.getAttributeValue("ContentLocation", false, assetElem);
                        } else if (
                            _scBaseUtils.equals(
                                assetType, "ITEM_IMAGE_LRG_1")) {
                            _scWidgetUtils.showWidget(
                                this, "moreImagesLink", false, null);
                            hasMoreImages = true;
                        }
                    }
                }
                if (
                    _scBaseUtils.equals(
                        hasMoreImages, false)) {
                    _scWidgetUtils.hideWidget(
                        this, "moreImagesLink", false);
                }
                imageUrl = _scModelUtils.getStringValueFromPath("Item.PrimaryInformation.ImageLocation", itemModel);
                return imageUrl;
            },

            // Overriden for Image
            getImageIcon: function(screen, widget, nameSpace, itemModel, options, dataValue) {
                var displayImage = "";
                displayImage = _isccsContextUtils.getDisplayItemImageRule();
                if (
                    _scBaseUtils.equals(
                        displayImage, "N")) {
                    var retValue = "";
                    return retValue;
                }
                var assetList = null;
                var imageIcon = "";
                var length = 0;
                assetList = _scModelUtils.getModelListFromPath("Item.AssetList.Asset", itemModel);
                length = _scBaseUtils.getAttributeCount(
                    assetList);
                for (
                    var index = 0; _scBaseUtils.lessThan(
                        index, length); index = _scBaseUtils.incrementNumber(
                        index, 1)) {
                    var assetElem = null;
                    var assetType = null;
                    if (!(
                            _scBaseUtils.isVoid(
                                assetList))) {
                        assetElem = _scBaseUtils.getArrayBeanItemByIndex(
                            assetList, index);
                        assetType = _scBaseUtils.getAttributeValue("Type", false, assetElem);
                        if (
                            _scBaseUtils.equals(
                                assetType, "ITEM_IMAGE_1")) {
                            imageIcon = _scBaseUtils.getAttributeValue("ContentID", false, assetElem);
                        }
                    }
                }
                imageIcon = _scModelUtils.getStringValueFromPath("Item.PrimaryInformation.ImageID", itemModel);
                return imageIcon;
            },
            extn_getItemAlias: function(dataValue, screen, widget, nameSpace, itemDetailsModel, options) {
                var aliasValue = null;
                if (!_scBaseUtils.isVoid(itemDetailsModel)) {
                    var aliasModel = _scModelUtils.getModelObjectFromPath("Item.ItemAliasList.ItemAlias", itemDetailsModel);
                    if (!_scBaseUtils.isVoid(aliasModel)) {
                        for (var i = 0; i < aliasModel.length; i++) {
                            var refrenceObj = _scBaseUtils.getArrayItemByIndex(aliasModel, i);
                            var aliasName = _scModelUtils.getStringValueFromPath("AliasName", refrenceObj);
                            if (_scBaseUtils.equals(aliasName, "ACTIVE_UPC")) {

                                aliasValue = _scModelUtils.getStringValueFromPath("AliasValue", refrenceObj);

                            }

                        }

                    }

                }

                return aliasValue;
            },
            extn_getColor: function(dataValue, screen, widget, nameSpace, itemDetailsModel, options) {
                var color = null;
                if (!_scBaseUtils.isVoid(itemDetailsModel)) {
                    var attributeModel = _scModelUtils.getModelObjectFromPath("Item.AdditionalAttributeList.AdditionalAttribute", itemDetailsModel);
                    if (!_scBaseUtils.isVoid(attributeModel)) {
                        for (var i = 0; i < attributeModel.length; i++) {
                            var refrenceObj = _scBaseUtils.getArrayItemByIndex(attributeModel, i);
                            var name = _scModelUtils.getStringValueFromPath("Name", refrenceObj);
                            if (_scBaseUtils.equals(name, "Color")) {

                                color = _scModelUtils.getStringValueFromPath("Value", refrenceObj);

                            }


                        }
                    }
                }
                return color;
            },
            extn_getSize: function(dataValue, screen, widget, nameSpace, itemDetailsModel, options) {
                var size = null;
                if (!_scBaseUtils.isVoid(itemDetailsModel)) {
                    var attributeModel = _scModelUtils.getModelObjectFromPath("Item.AdditionalAttributeList.AdditionalAttribute", itemDetailsModel);
                    if (!_scBaseUtils.isVoid(attributeModel)) {
                        for (var i = 0; i < attributeModel.length; i++) {
                            var refrenceObj = _scBaseUtils.getArrayItemByIndex(attributeModel, i);
                            var name = _scModelUtils.getStringValueFromPath("Name", refrenceObj);
                            if (_scBaseUtils.equals(name, "Size")) {

                                size = _scModelUtils.getStringValueFromPath("Value", refrenceObj);

                            }


                        }
                    }
                }
                return size;
            },
            extn_beforeInitialize:function(event, bEvent, ctrl, args){
                 var initialInputData = null;
           
            initialInputData = _scScreenUtils.getInitialInputData(this);
             var availableQuantity=null;
             availableQuantity= _scModelUtils.getStringValueFromPath("Item.QtyOnHand", initialInputData);
             if(!_scBaseUtils.isVoid(availableQuantity)){
                _scWidgetUtils.setValue(this, "extn_qtyOnHand", parseInt(availableQuantity));
             }

            },
            extn_getPrice: function(dataValue, screen, widget, nameSpace, itemDetailsModel, options) {
            var value=itemDetailsModel.Item.ComputedPrice.ListPrice;
            var price = null;
            if (value) {
                if (!_scBaseUtils.isVoid(itemDetailsModel)) {
                    var currencyModel = _scModelUtils.getModelObjectFromPath("Item.Currency", itemDetailsModel);
                    if (!_scBaseUtils.isVoid(currencyModel)) {
                        country = currencyModel;
                        if (!_scBaseUtils.isVoid(country)) {
                            price = dCurrency.format(parseFloat(value), {
                                currency: country
                            });
                        }
                    }
                }
                return price;
            } else {
                return "";
            }
        }
        });
    });
