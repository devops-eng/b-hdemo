


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/item/details/ItemDetailsExtn","scbase/loader!sc/plat/dojo/controller/ExtnScreenController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnItemDetailsExtn
			 ,
			    _scExtnScreenController
){

return _dojodeclare("extn.item.details.ItemDetailsExtnInitController", 
				[_scExtnScreenController], {

			
			 screenId : 			'extn.item.details.ItemDetailsExtn'

			
			
			
			
			
						,

			
			
			 mashupRefs : 	[
	 		{
		 sourceNamespace : 			'itemDetails'
,
		 mashupRefId : 			'itemDetails_getCompleteItemDetails'
,
		 sequence : 			'2'
,
		 mashupId : 			'itemDetails_getCompleteItemDetails'
,
		 callSequence : 			'2'
,
		 extnType : 			''
,
		 sourceBindingOptions : 			''

	}
,
	 		{
		 sourceNamespace : 			'itemDetails'
,
		 mashupRefId : 			'itemDetails_getCompleteItemDetails'
,
		 sequence : 			'2'
,
		 mashupId : 			'itemDetails_getCompleteItemDetails'
,
		 callSequence : 			'2'
,
		 extnType : 			'ADD'
,
		 sourceBindingOptions : 			''

	}

	]

}
);
});

