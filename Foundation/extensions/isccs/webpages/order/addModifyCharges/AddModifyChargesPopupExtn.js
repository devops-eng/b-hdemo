scDefine(["scbase/loader!dojo/_base/declare",
"scbase/loader!extn/order/addModifyCharges/AddModifyChargesPopupExtnUI",
"scbase/loader!sc/plat/dojo/utils/BaseUtils",
"scbase/loader!isccs/utils/UIUtils",
"scbase/loader!isccs/utils/BaseTemplateUtils",
"scbase/loader!sc/plat/dojo/utils/ModelUtils",
"scbase/loader!isccs/utils/ModelUtils",
"scbase/loader!sc/plat/dojo/utils/ScreenUtils","scbase/loader!sc/plat/dojo/utils/WidgetUtils" ]
,
function(			 
			    _dojodeclare
			 ,
			    _extnAddModifyChargesPopupExtnUI
                      ,
                        _scBaseUtils
                      ,
                        _isccsUIUtils
                      ,
                        _isccsBaseTemplateUtils
                      ,
                         _scModelUtils
                     ,
                        _isccsModelUtils
                     ,
                       _scScreenUtils
					 ,
						_scWidgetUtils

){ 
	return _dojodeclare("extn.order.addModifyCharges.AddModifyChargesPopupExtn", [_extnAddModifyChargesPopupExtnUI],{
	// custom code here

 onApply: function(
        event, bEvent, ctrl, args) {
            var targetModel = null;
            var LineCharges = null;
            var OrderHeaderKey = null;
            var options = null;
            var headerCharge = null;
            var count = 0;
            options = _scBaseUtils.getNewBeanInstance();
            _isccsUIUtils.addBooleanValueToBean("dynamicRepeatingPanel", true, options);
            targetModel = _scBaseUtils.getTargetModel(
            this, "getCompleteOrderDetails_input", options);
            headerCharge = _scModelUtils.getModelListFromPath("Order.HeaderCharges.HeaderCharge", targetModel);
            count = _scBaseUtils.getAttributeCount(
            headerCharge);
            if (
            _scBaseUtils.equals(
            count, 0)) {
                _isccsBaseTemplateUtils.showMessage(
                this, "NoChargeError", "error", null);
            } else {
                if (
                _scBaseUtils.equals(
                this.screenMode, "LineMode")) {
                    var manageChargeDetails = null;
                    manageChargeDetails = _scBaseUtils.getAttributeValue("scEditorInput", false, this);
                    var OrderLineKey = null;
                    OrderHeaderKey = _scModelUtils.getStringValueFromPath("Order.OrderHeaderKey", manageChargeDetails);
                    _scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", OrderHeaderKey, targetModel);
                    OrderLineKey = _scModelUtils.getStringValueFromPath("Order.OrderLines.OrderLine.OrderLineKey", manageChargeDetails);
                    _scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.OrderLineKey", OrderLineKey, targetModel);
                    _isccsUIUtils.callApi(
                    this, targetModel, "lineCharges_changeOrder", null);
                } else {
                    OrderHeaderKey = _scBaseUtils.getAttributeValue("params.scEditorInput.Order.OrderHeaderKey", false, this);
                    _scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", OrderHeaderKey, targetModel);

                   
                    for(var index in headerCharge)
                    {
                        var chargeCategory = _scModelUtils.getStringValueFromPath("ChargeCategory", headerCharge[index]);
                        if(chargeCategory =="SHIPPINGCHARGE")
                        {
                            var chargeamount = _scModelUtils.getStringValueFromPath("ChargeAmount", headerCharge[index]);
                             _scModelUtils.setStringValueAtModelPath("Extn.ExtnOriginalAmount", chargeamount, headerCharge[index]);
                            
                        }
                    }
                     _isccsModelUtils.removeAttributeFromModel("Order.HeaderCharges.HeaderCharge",targetModel);
                      _scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge", headerCharge, targetModel);
                    _isccsUIUtils.callApi(
                    this, targetModel, "headerCharges_changeOrder", null);
                
            }
            }
        },

extn_discountValidation:function(event, bEvent, ctrl, args)
{
   var targetModel = null;
   var options = null;
   options = _scBaseUtils.getNewBeanInstance();
   var shippingCharge = null;
   var discount = null;
    _isccsUIUtils.addBooleanValueToBean("dynamicRepeatingPanel", true, options);
    targetModel = _scBaseUtils.getTargetModel(this, "getCompleteOrderDetails_input", options);
    var orderDetails = _scScreenUtils.getModel(this, "getCompleteOrderDetails_output");
    var documentType = _scModelUtils.getModelListFromPath("Order.DocumentType", orderDetails );
    var enterpriseCode = _scModelUtils.getModelListFromPath("Order.EnterpriseCode", orderDetails );

    if(!(_scBaseUtils.equals("0003",documentType) && (_scBaseUtils.equals("OFF5",enterpriseCode) || _scBaseUtils.equals("SAKS",enterpriseCode)))){
    var headerCharge = _scModelUtils.getModelListFromPath("Order.HeaderCharges.HeaderCharge", targetModel);
    for(var i in headerCharge)
    {
        var chargeCategory = _scModelUtils.getStringValueFromPath("ChargeCategory", headerCharge[i]);
        if(chargeCategory =="SHIPPINGCHARGE")
          {
              shippingCharge =_scModelUtils.getNumberValueFromPath ("ChargeAmount", headerCharge[i]);
          }
          else if(chargeCategory =="DISCOUNT")
          {
              discount =_scModelUtils.getNumberValueFromPath ("ChargeAmount", headerCharge[i]);
              if(discount > shippingCharge)
              {
                    var message = null;
                     message = _scScreenUtils.getString(this,"extn_DiscountValidation");
                    _isccsBaseTemplateUtils.showMessage(this, message , "error", null);
                  // _isccsBaseTemplateUtils.showMessage(this, "Please enter discount less than Shipping Charge.", "error", null);
                   sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
                   return true;

              }
          }
          
    }
   }
},
extn_hideAddCharges:function(event, bEvent, ctrl, args)
{
			var manageChargeDetails = null;
            manageChargeDetails = _scScreenUtils.getInitialInputData(
            this);
			var LineMode = null;
            LineMode = _scModelUtils.getStringValueFromPath("LineMode", manageChargeDetails);
            if (!(
            _scBaseUtils.equals(
            LineMode, "Y"))) {
			_scWidgetUtils.hideWidget(this, "bttnAddCharges", false);
			
			}

}

});
});
