


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/order/addModifyCharges/AddModifyChargesPopupExtn","scbase/loader!sc/plat/dojo/controller/ExtnServerDataController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnAddModifyChargesPopupExtn
			 ,
			    _scExtnServerDataController
){

return _dojodeclare("extn.order.addModifyCharges.AddModifyChargesPopupExtnBehaviorController", 
				[_scExtnServerDataController], {

			
			 screenId : 			'extn.order.addModifyCharges.AddModifyChargesPopupExtn'

			
			
			
}
);
});

