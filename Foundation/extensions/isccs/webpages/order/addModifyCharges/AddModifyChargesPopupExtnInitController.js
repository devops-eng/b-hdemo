


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/order/addModifyCharges/AddModifyChargesPopupExtn","scbase/loader!sc/plat/dojo/controller/ExtnScreenController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnAddModifyChargesPopupExtn
			 ,
			    _scExtnScreenController
){

return _dojodeclare("extn.order.addModifyCharges.AddModifyChargesPopupExtnInitController", 
				[_scExtnScreenController], {

			
			 screenId : 			'extn.order.addModifyCharges.AddModifyChargesPopupExtn'

			
			
			
}
);
});

