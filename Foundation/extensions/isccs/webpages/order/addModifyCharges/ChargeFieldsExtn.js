
scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/order/addModifyCharges/ChargeFieldsExtnUI", "scbase/loader!sc/plat/dojo/utils/ScreenUtils",
"scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils","scbase/loader!sc/plat/dojo/utils/WidgetUtils","scbase/loader!isccs/utils/UIUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnChargeFieldsExtnUI
			 ,
			    _scScreenUtils
			 ,
			    _scBaseUtils
			 ,
			    _scModelUtils
			 ,
			    _scWidgetUtils
			 ,
			    _isccsUIUtils
){ 
	return _dojodeclare("extn.order.addModifyCharges.ChargeFieldsExtn", [_extnChargeFieldsExtnUI],{
	// custom code here
	extn_afterInitializeScreen: function(
        event, bEvent, ctrl, args) {
          	var inputModel = _scScreenUtils.getModel(this, "getCompleteOrderDetails_output");
          	var chargecategoryValue = _scModelUtils.getStringValueFromPath("HeaderCharge.ChargeCategory",inputModel);
          	var inputOrderModel = _scScreenUtils.getModel(this, "currencySource_output");
          	var enterpriseCode = _scModelUtils.getStringValueFromPath("Order.EnterpriseCode", inputOrderModel );
          	var documentType = _scModelUtils.getStringValueFromPath("Order.DocumentType", inputOrderModel );
          	var parentScreen = null;
            parentScreen = _isccsUIUtils.getParentScreen(
            this, false);
            var manageChargeDetails = null;
            manageChargeDetails = _scScreenUtils.getInitialInputData(
            parentScreen);
		    var checkForLineMode = null;
		    checkForLineMode = _scModelUtils.getStringValueFromPath("LineMode", manageChargeDetails);
          	if((enterpriseCode == "OFF5") && (documentType == "0003") && (chargecategoryValue != "DISCOUNT") && (checkForLineMode == "Y")){
			    if(this.IsNewCharge == "Y"){
			        
                  	 _scWidgetUtils.setValue(this, "cmbChargeCategory", "DISCOUNT",false);
                  	 this.handleCategoryChange();
                  	 //_scWidgetUtils.disableWidget(this, "cmbChargeCategory", false);
                  	 _scWidgetUtils.enableReadOnlyWidget(this, "cmbChargeCategory");

           
          	     } else{
			        _scWidgetUtils.disableWidget(this, "txt_chargeAmount", false);
                	_scWidgetUtils.disableWidget(this, "cmbApplyTo", false);
                 }
          	 }
          	 
          	    
          	    
        }
	
});
});

