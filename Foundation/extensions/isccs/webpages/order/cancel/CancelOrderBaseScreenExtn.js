scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/order/cancel/CancelOrderBaseScreenExtnUI",
"scbase/loader!isccs/utils/BaseTemplateUtils", "scbase/loader!isccs/utils/OrderUtils", "scbase/loader!isccs/utils/UIUtils",
 "scbase/loader!sc/plat", "scbase/loader!sc/plat/dojo/binding/ButtonDataBinder", "scbase/loader!sc/plat/dojo/binding/ComboDataBinder",
 "scbase/loader!sc/plat/dojo/binding/CurrencyDataBinder", "scbase/loader!sc/plat/dojo/binding/RadioSetDataBinder", 
 "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/ControllerUtils",
 "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/GridxUtils",
 "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils",
 "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/widgets/ControllerWidget", 
 "scbase/loader!sc/plat/dojo/widgets/IdentifierControllerWidget", "scbase/loader!sc/plat/dojo/widgets/Label",
 "scbase/loader!sc/plat/dojo/widgets/Screen","scbase/loader!sc/plat/dojo/utils/PlatformUIFmkImplUtils","scbase/loader!isccs/utils/ModelUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnCancelOrderBaseScreenExtnUI
, _isccsBaseTemplateUtils, _isccsOrderUtils, 
				_isccsUIUtils, _scplat, _scButtonDataBinder, _scComboDataBinder,
				_scCurrencyDataBinder, _scRadioSetDataBinder, _scBaseUtils, _scControllerUtils, 
				_scEventUtils, _scGridxUtils, _scModelUtils, _scScreenUtils, _scWidgetUtils, _scControllerWidget,
				_scIdentifierControllerWidget, _scLabel, _scScreen, _scPlatformUIFmkUtils,_isccsModelUtils
){ 
	return _dojodeclare("extn.order.cancel.CancelOrderBaseScreenExtn", [_extnCancelOrderBaseScreenExtnUI],{
	// custom code here
//Used to form a model with GiftBox OrderLines which are to be deleted.

extn_CancelGiftBoxMethod: function(selectedOrderLinesList)
        {
            var GiftBoxOrderModel = _scScreenUtils.getModel(this, "extn_GiftBoxCommonList");
            var CommonCodeList=_scModelUtils.getModelListFromPath("CommonCodeList.CommonCode", GiftBoxOrderModel);
			var GiftBoxID=null;
            	for(var i in CommonCodeList)
		{
            var CommonCodeObj = _scModelUtils.getModelFromList(CommonCodeList, i); 
            if(CommonCodeObj.CodeType=="LT_CustomCode_Gift")
            {
                 GiftBoxID=CommonCodeObj.CodeValue;
            }
            
		}
			var cancelOrderListScreenObj = _scScreenUtils.getChildScreen(this, "cancelOrderListScreen");
			var getCompleteOrderLineList_output = _scScreenUtils.getModel(cancelOrderListScreenObj, "getCompleteOrderLineList_output");
			var orderLinesList = null;
			orderLinesList = _scModelUtils.getModelListFromPath("Page.Output.OrderLineList.OrderLine", getCompleteOrderLineList_output);
				var store=_scGridxUtils.getGridStoreUsingUId(cancelOrderListScreenObj,"OLST_listGrid");
                var OrderLineList1=_scModelUtils.getModelListFromPath("_arrayOfAllItems", store); 
                	var TargetModel = _scScreenUtils.getTargetModel(cancelOrderListScreenObj, "getCompleteOrderLineList_input");
			var TargetOrderLinesList = _scModelUtils.getModelListFromPath("OrderLineList.OrderLine", TargetModel);
          //  var OrderLineList=_scModelUtils.getModelListFromPath("Order.OrderLines.OrderLine", tmodel);
			var DeleteCount=0;
			var GiftWrappedItems=0;
			var GiftBoxesCount=0;
			var GiftBoxtobeDeleted=0;
			var OrderLineKeyArray=_scBaseUtils.getNewArrayInstance();
			for(var m in OrderLineList1)
				{
					var TargetLine = OrderLineList1[m];
					var k=0;
					if(!(_scBaseUtils.isVoid(TargetLine)))
					{
                        var CancelQuantityModel=_scModelUtils.getModelListFromPath("CancelQuantity", TargetLine);
                        var CancelQuantity = _scModelUtils.getModelFromList(CancelQuantityModel, k);

						var ItemData = _scModelUtils.getModelListFromPath("_dataItem", TargetLine);
						//var GiftWrap=_scModelUtils.getStringValueFromPath("GiftWrap", ItemData);
						//var ItemID=_scModelUtils.getStringValueFromPath("ItemDetails.ItemID", ItemData);
                        if(!(_scBaseUtils.stringEquals(ItemData.MaxLineStatusDesc,"Cancelled")))
                        {
						if(_scBaseUtils.stringEquals(ItemData.GiftWrap,"Y") && !(_scBaseUtils.stringEquals(ItemData.ItemDetails.ItemID,GiftBoxID)))
						{
							GiftWrappedItems=parseInt(GiftWrappedItems)+1;
						}
// 						/if(_scBaseUtils.stringEquals(Action,"REMOVE") && _scBaseUtils.stringEquals(GiftWrap,"Y"))
                        if(parseInt(CancelQuantity)==parseInt(ItemData.OrderedQty))
						{
							DeleteCount=parseInt(DeleteCount)+1;
						}
						if(_scBaseUtils.stringEquals(ItemData.ItemDetails.ItemID,GiftBoxID))
						{
							GiftBoxesCount=parseInt(GiftBoxesCount)+1;
							//_scBaseUtils.appendToArray(GiftBoxObjects,m);
                            _scBaseUtils.appendToArray(OrderLineKeyArray,ItemData.OrderLineKey);

						}
                        }
					}
				}
            if(GiftWrappedItems==DeleteCount && GiftBoxesCount=="1")
			{
				GiftBoxtobeDeleted=1;
         	}
			else if(GiftWrappedItems==GiftBoxesCount && DeleteCount>0)
			{
				GiftBoxtobeDeleted=DeleteCount;
			}
        if(GiftBoxtobeDeleted>0) 
        {
            var GiftboxTobeDeletedArray=[];
               var count=0;
               for(var m in TargetOrderLinesList)
                {
                    indexObj = _scModelUtils.getModelFromList(TargetOrderLinesList, m);
                    if(indexObj.OrderedQty>0)
                    {
                        if(_scBaseUtils.containsInArray(OrderLineKeyArray,indexObj.OrderLineKey) && count<GiftBoxtobeDeleted)
                        {
                            _scBaseUtils.appendToArray(GiftboxTobeDeletedArray,indexObj);
                            count=count+1;
                        }
                    }
                } 
                return GiftboxTobeDeletedArray;
			}
		/*	var TargetModel = _scScreenUtils.getTargetModel(cancelOrderListScreenObj, "getCompleteOrderLineList_input");
			var TargetOrderLinesList = _scModelUtils.getModelListFromPath("OrderLineList.OrderLine", TargetModel);
			var ItemstobeDeleted=0;
			var OrderLineKeyArray=_scBaseUtils.getNewArrayInstance();
		    var rowsCount = 0;
            rowsCount = _scBaseUtils.getAttributeCount(orderLinesList);
            for(var m in selectedOrderLinesList)
            {
                var OrderLineElement=selectedOrderLinesList[m];
                if(parseInt(OrderLineElement.CancelQuantity)==parseInt(OrderLineElement.OrderedQty))
                {
                    ItemstobeDeleted=_scBaseUtils.incrementNumber(ItemstobeDeleted,1);
                }
            }
			if(ItemstobeDeleted>0) 
			{
               for (var i = 0;_scBaseUtils.lessThan(i, rowsCount); i = _scBaseUtils.incrementNumber(i, 1)) 
               {
               indexObj = _scModelUtils.getModelFromList(orderLinesList, i); 
			    var	ItemID=indexObj.ItemDetails.ItemID;
                if(ItemID=="GiftBox")
                    {
                     _scBaseUtils.appendToArray(OrderLineKeyArray,indexObj.OrderLineKey);
                    }
               }
               var GiftboxTobeDeleted=[];
               var count=0;
               for(var m in TargetOrderLinesList)
                {
                    indexObj = _scModelUtils.getModelFromList(TargetOrderLinesList, m);
                    if(indexObj.OrderedQty>0)
                    {
                        if(_scBaseUtils.containsInArray(OrderLineKeyArray,indexObj.OrderLineKey) && count<ItemstobeDeleted)
                        {
                            _scBaseUtils.appendToArray(GiftboxTobeDeleted,indexObj);
                            count=count+1;
                        }
                    }
                } 
                return GiftboxTobeDeleted;
			}*/

	},

//OOB Overriden method

	   cancelSelectedLines: function(
        cancelOrderItems, actionOnPage, paginatedContext) {
            var selectedOrderLinesList = null;
            selectedOrderLinesList = _scModelUtils.getModelListFromPath("OrderLineList.OrderLine", cancelOrderItems);

//To get GiftBox Items to be deleted-Begin.
        var screenInputData = _scScreenUtils.getModel(this, "screenInput");
        var enterpriseCode = screenInputData.Order.EnterpriseCode;
        if(_scBaseUtils.equals(enterpriseCode,"LT") || _scBaseUtils.equals(enterpriseCode,"BAY"))
            var GiftboxTobeDeleted=this.extn_CancelGiftBoxMethod(selectedOrderLinesList);
        else
            var GiftboxTobeDeleted = this.extn_GiftBoxCancellation(selectedOrderLinesList);
        
            if(!(_scBaseUtils.isVoid(GiftboxTobeDeleted))) 
            {
		  var OrderLineList=_scModelUtils.getModelListFromPath("", GiftboxTobeDeleted);
                for(var m in OrderLineList)
			{
				var OrderLineElement=_scBaseUtils.getNewModelInstance();
				OrderLineElement =_scModelUtils.getModelFromList(OrderLineList,m);
                    		_scModelUtils.setStringValueAtModelPath("CancelQuantity","1.0000", OrderLineElement);
				_scBaseUtils.appendToArray(selectedOrderLinesList,OrderLineElement);
			}
	    }
//end of code
            var cancelOrderLineList = null;
            cancelOrderLineList = this.updateQtyToCancel(
            selectedOrderLinesList);
            var cancelOrder = null;
            cancelOrder = this.createSelectedCancelObj(
            cancelOrderLineList);
            _scScreenUtils.setModel(
            this, "cancelOrderReasonCode_input", cancelOrder, null);
            var mashupContext = null;
            mashupContext = _scControllerUtils.getMashupContext(
            this);
            _scBaseUtils.setAttributeValue("appPaginatedContext", paginatedContext, mashupContext);
            _scControllerUtils.setMashupContextIdenitier(
            mashupContext, actionOnPage);
            var confirm_msg = null;
            confirm_msg = _scScreenUtils.getString(
            this, "ConfirmCalcelOrderMsg");
            _scScreenUtils.showConfirmMessageBox(
            this, confirm_msg, "handleConfirm", null, mashupContext);
        },

/*  extn_cancelNotes:function(event, bEvent, ctrl, args)
  {
    var cancelPropModel = null;
    cancelPropModel = _scScreenUtils.getTargetModel(this, "getCancelProp_input", null);
  }
*/

extn_ValidateCancel:function(event, bEvent, ctrl, args){
var cancelOrder = null;         
cancelOrder = _scScreenUtils.getModel(this, "cancelOrderReasonCode_input");

},

handleConfirm: function(res, mashupContext) {
            if (_scBaseUtils.equals(res, "Ok"))
            {
               var cancelOrder = null;         
		 cancelOrder = _scScreenUtils.getModel(this, "cancelOrderReasonCode_input");
               var userId = _scPlatformUIFmkUtils.getUserId();      
		 var screenData = _scScreenUtils.getModel(this, "screenInput");  
		 var firstName = _scModelUtils.getStringValueFromPath("Order.CustomerFirstName",screenData);
               var lastName = _scModelUtils.getStringValueFromPath("Order.CustomerLastName",screenData); 
		 var modifiedReasonCode= _scModelUtils.getStringValueFromPath("Order.ModificationReasonCode", cancelOrder);                
		 var noteText = null; 
	         var reasonCode = null;
	         var reasonText = null;
		 noteText = firstName+" " +lastName +" requested to cancel because of " ;       
               //_scModelUtils.setStringValueAtModelPath("Order.Notes.Note.NoteText",noteText,cancelOrder); 
               _scModelUtils.setStringValueAtModelPath("Order.Notes.Note.ContactUser",userId,cancelOrder);
               _scModelUtils.setStringValueAtModelPath("Order.Notes.Note.VisibleToAll","Y",cancelOrder);
               var actionPerformed = _scModelUtils.getStringValueFromPath("Order.Action", cancelOrder);

               if(actionPerformed == "CANCEL")
               {
                 if(modifiedReasonCode== "Customer has Different Coupon") 
		     {
                     noteText =noteText +modifiedReasonCode;
					 _scModelUtils.setStringValueAtModelPath("Order.Notes.Note.NoteText",noteText,cancelOrder);
		             _scModelUtils.setStringValueAtModelPath("Order.Notes.Note.ReasonCode","HBC_MC_CC_001",cancelOrder);   
		             reasonCode = "HBC_MC_CC_001";
		       }
		      else if(modifiedReasonCode== "Customer Preference")
		       {                      
		            noteText =noteText +modifiedReasonCode; 
                    _scModelUtils.setStringValueAtModelPath("Order.Notes.Note.NoteText",noteText,cancelOrder);
                    _scModelUtils.setStringValueAtModelPath("Order.Notes.Note.ReasonCode","HBC_MC_CC_002",cancelOrder);
                    reasonCode = "HBC_MC_CC_002";
                }
              else if(modifiedReasonCode== "Different Method of Payment") 
		       {                      
		             noteText =noteText +modifiedReasonCode; 
                     _scModelUtils.setStringValueAtModelPath("Order.Notes.Note.NoteText",noteText,cancelOrder);
                     _scModelUtils.setStringValueAtModelPath("Order.Notes.Note.ReasonCode","HBC_MC_CC_003",cancelOrder);
                     reasonCode = "HBC_MC_CC_003";
                }
               else if(modifiedReasonCode== "Different Price")   
		       { 
                     noteText =noteText +modifiedReasonCode; 
                    _scModelUtils.setStringValueAtModelPath("Order.Notes.Note.NoteText",noteText,cancelOrder); 
                     _scModelUtils.setStringValueAtModelPath("Order.Notes.Note.ReasonCode","HBC_MC_CC_004",cancelOrder);
                     reasonCode = "HBC_MC_CC_004";
               }
               else if(modifiedReasonCode== "Other")  
		       {
		             noteText =noteText +modifiedReasonCode;
                     _scModelUtils.setStringValueAtModelPath("Order.Notes.Note.NoteText",noteText,cancelOrder);
                      _scModelUtils.setStringValueAtModelPath("Order.Notes.Note.ReasonCode","HBC_MC_CC_005",cancelOrder);
                     reasonCode = "HBC_MC_CC_005";
               }
              reasonText = noteText;
            }
           else
            {  
                var orderLines = _scModelUtils.getStringValueFromPath("Order.OrderLines.OrderLine",cancelOrder); 
		        for(var index in orderLines)  
		         {                
		          noteText = firstName+" " +lastName +" requested to cancel because of " ;  
		          if(modifiedReasonCode== "Customer has Different Coupon") 
		           {                                
		             noteText =noteText +modifiedReasonCode; 
                    _scModelUtils.setStringValueAtModelPath("Order.Notes.Note.NoteText",noteText,cancelOrder);
                    _scModelUtils.setStringValueAtModelPath("Notes.Note.NoteText",noteText,orderLines[index]);
                    _scModelUtils.setStringValueAtModelPath("Notes.Note.ReasonCode","HBC_MC_CC_001",orderLines[index]);
                    _scModelUtils.setStringValueAtModelPath("Order.Notes.Note.ReasonCode","HBC_MC_CC_001",cancelOrder); 
                     _scModelUtils.setStringValueAtModelPath("Notes.Note.ContactUser",userId,orderLines[index]);
                     _scModelUtils.setStringValueAtModelPath("Notes.Note.VisibleToAll","Y",orderLines[index]);
                     reasonCode = "HBC_MC_CC_001";
                   }     
		         else if(modifiedReasonCode== "Customer Preference")
		         {                      
		              noteText =noteText +modifiedReasonCode; 
                    _scModelUtils.setStringValueAtModelPath("Order.Notes.Note.NoteText",noteText,cancelOrder);
                    _scModelUtils.setStringValueAtModelPath("Notes.Note.NoteText",noteText,orderLines[index]); 
                    _scModelUtils.setStringValueAtModelPath("Notes.Note.ReasonCode","HBC_MC_CC_002",orderLines[index]);
                    _scModelUtils.setStringValueAtModelPath("Order.Notes.Note.ReasonCode","HBC_MC_CC_002",cancelOrder);
                     _scModelUtils.setStringValueAtModelPath("Notes.Note.ContactUser",userId,orderLines[index]);  
                      _scModelUtils.setStringValueAtModelPath("Notes.Note.VisibleToAll","Y",orderLines[index]);     
                      reasonCode = "HBC_MC_CC_002";  
		           }  
		        else if(modifiedReasonCode== "Different Method of Payment") 
		        {                      
		             noteText =noteText +modifiedReasonCode; 
                    _scModelUtils.setStringValueAtModelPath("Order.Notes.Note.NoteText",noteText,cancelOrder);
                    _scModelUtils.setStringValueAtModelPath("Notes.Note.NoteText",noteText,orderLines[index]);
                    _scModelUtils.setStringValueAtModelPath("Notes.Note.ReasonCode","HBC_MC_CC_003",orderLines[index]);
                    _scModelUtils.setStringValueAtModelPath("Order.Notes.Note.ReasonCode","HBC_MC_CC_003",cancelOrder);
                     _scModelUtils.setStringValueAtModelPath("Notes.Note.ContactUser",userId,orderLines[index]);
                     _scModelUtils.setStringValueAtModelPath("Notes.Note.VisibleToAll","Y",orderLines[index]);
                     reasonCode = "HBC_MC_CC_003"; 
		      }       
		      else if(modifiedReasonCode== "Different Price")   
		       { 
                     noteText =noteText +modifiedReasonCode; 
                      _scModelUtils.setStringValueAtModelPath("Order.Notes.Note.NoteText",noteText,cancelOrder); 
                      _scModelUtils.setStringValueAtModelPath("Notes.Note.NoteText",noteText,orderLines[index]); 
                      _scModelUtils.setStringValueAtModelPath("Notes.Note.ReasonCode","HBC_MC_CC_004",orderLines[index]); 
                      _scModelUtils.setStringValueAtModelPath("Order.Notes.Note.ReasonCode","HBC_MC_CC_004",cancelOrder);
                      _scModelUtils.setStringValueAtModelPath("Notes.Note.ContactUser",userId,orderLines[index]);
                      _scModelUtils.setStringValueAtModelPath("Notes.Note.VisibleToAll","Y",orderLines[index]);	  
                     reasonCode = "HBC_MC_CC_004"; 	  
		           }    
		        else if(modifiedReasonCode== "Other")  
		           {
		              noteText =noteText +modifiedReasonCode;
                     _scModelUtils.setStringValueAtModelPath("Order.Notes.Note.NoteText",noteText,cancelOrder);
                     _scModelUtils.setStringValueAtModelPath("Notes.Note.NoteText",noteText,orderLines[index]);
                     _scModelUtils.setStringValueAtModelPath("Notes.Note.ReasonCode","HBC_MC_CC_005",orderLines[index]);
                     _scModelUtils.setStringValueAtModelPath("Order.Notes.Note.ReasonCode","HBC_MC_CC_005",cancelOrder);
                     _scModelUtils.setStringValueAtModelPath("Notes.Note.ContactUser",userId,orderLines[index]);
                       _scModelUtils.setStringValueAtModelPath("Notes.Note.VisibleToAll","Y",orderLines[index]);
                      reasonCode = "HBC_MC_CC_005"; 
	           }         
	           reasonText = noteText;
	           noteText = null;       
		       }  
		 }
        var screenInputData = _scScreenUtils.getModel(this, "screenInput");
        var enterpriseCode = screenInputData.Order.EnterpriseCode;
        if(_scBaseUtils.equals(enterpriseCode,"LT") || _scBaseUtils.equals(enterpriseCode,"BAY"))
        {
          
            var GiftBoxOrderModel = _scScreenUtils.getModel(this, "extn_GiftBoxCommonList");
            var CommonCodeList = _scModelUtils.getModelListFromPath("CommonCodeList.CommonCode", GiftBoxOrderModel);
            var GiftBoxID = null;
            for (var i in CommonCodeList) {
                var CommonCodeObj = _scModelUtils.getModelFromList(CommonCodeList, i);
                if (CommonCodeObj.CodeType == "LT_CustomCode_Gift") {
                    GiftBoxID = CommonCodeObj.CodeValue;
                }
            }
            var orderModel = _scScreenUtils.getModel(this, "getCompleteOrderDetails_output");
            var orderLinesModel = _scModelUtils.getModelListFromPath("Order.OrderLines.OrderLine", orderModel);
            var modelList = _scBaseUtils.getNewArrayInstance();
            var namespaceList = _scBaseUtils.getNewArrayInstance();
            var changeOrderInputArray = _scBaseUtils.getNewArrayInstance();
            var cancelOrderInputArray = _scBaseUtils.getNewArrayInstance();
            var holdReasonText = reasonCode + "," + reasonText;
            var releasedStatusCheck = false;
            for (var index in orderLinesModel) {
                var strMinLineStatus = orderLinesModel[index].MinLineStatus;
                if (strMinLineStatus == "3200") {
                    releasedStatusCheck = true;
                    break;
                }
            }
            if (actionPerformed == "CANCEL" && releasedStatusCheck) {
                var nonReleasedStatusOrderLinesArray = _scBaseUtils.getNewArrayInstance();
                for (var index in orderLinesModel) {
                    var strMinLineStatus = _scModelUtils.getNumberValueFromPath("MinLineStatus", orderLinesModel[index]);
                    var strOrderLineKey = _scModelUtils.getStringValueFromPath("OrderLineKey", orderLinesModel[index]);
                    if (strMinLineStatus == 3200) {
                        var changeOrderLine = _scBaseUtils.getNewModelInstance();
                        _scModelUtils.setStringValueAtModelPath("OrderLineKey", strOrderLineKey, changeOrderLine);
                        _scModelUtils.setStringValueAtModelPath("OrderHoldTypes.OrderHoldType.HoldType", "DC_CANCEL_HOLD", changeOrderLine);
                        _scModelUtils.setStringValueAtModelPath("OrderHoldTypes.OrderHoldType.Status", "1100", changeOrderLine);
                        _scModelUtils.setStringValueAtModelPath("OrderHoldTypes.OrderHoldType.ReasonText", holdReasonText, changeOrderLine);
                        _scBaseUtils.appendToArray(changeOrderInputArray, changeOrderLine);
                    } else if ((strMinLineStatus <= 2100)) {
                        var ItemId = _scModelUtils.getStringValueFromPath("ItemDetails.ItemID", orderLinesModel[index]);
                        if (!(_scBaseUtils.equals(ItemId, GiftBoxID))) {
                            _scModelUtils.setStringValueAtModelPath("Action", "NEXT", orderLinesModel[index]);
                            _scBaseUtils.appendToArray(nonReleasedStatusOrderLinesArray, orderLinesModel[index]);
                            var cancelOrderLineModel = _scBaseUtils.getNewModelInstance();
                            _scModelUtils.setStringValueAtModelPath("OrderLineKey", strOrderLineKey, cancelOrderLineModel);
                            var strPrimeLineNo = _scModelUtils.getStringValueFromPath("PrimeLineNo", orderLinesModel[index]);
                            var strAvailableQuantityToCancel = _scModelUtils.getStringValueFromPath("OrderLineTranQuantity.OrderedQty", orderLinesModel[index]);
                            _scModelUtils.setStringValueAtModelPath("PrimeLineNo", strPrimeLineNo, cancelOrderLineModel);
                            _scModelUtils.setStringValueAtModelPath("QuantityToCancel", strAvailableQuantityToCancel, cancelOrderLineModel);
                            _scBaseUtils.appendToArray(cancelOrderInputArray, cancelOrderLineModel);
                        }
                    }
                }
                if (!_scBaseUtils.isVoid(nonReleasedStatusOrderLinesArray)) {
                    var GiftBoxArray = this.extn_GiftBoxCancellation(nonReleasedStatusOrderLinesArray);
                    if (!_scBaseUtils.isVoid(GiftBoxArray)) {
                        for (var m in GiftBoxArray) {
                            var cancelOrderLineModel = _scBaseUtils.getNewModelInstance();
                            var giftBoxOrderLine = _scModelUtils.getStringValueFromPath("OrderLineKey", GiftBoxArray[m]);
                            _scModelUtils.setStringValueAtModelPath("OrderLineKey", giftBoxOrderLine, cancelOrderLineModel);
                            var strPrimeLineNo = _scModelUtils.getStringValueFromPath("PrimeLineNo", GiftBoxArray[m]);
                            var strAvailableQuantityToCancel = _scModelUtils.getStringValueFromPath("OrderLineTranQuantity.OrderedQty", GiftBoxArray[m]);
                            _scModelUtils.setStringValueAtModelPath("PrimeLineNo", strPrimeLineNo, cancelOrderLineModel);
                            _scModelUtils.setStringValueAtModelPath("QuantityToCancel", strAvailableQuantityToCancel, cancelOrderLineModel);
                            _scBaseUtils.appendToArray(cancelOrderInputArray, cancelOrderLineModel);
                        }
                    }
                }
                var releaseLineCount = _scBaseUtils.getAttributeCount(changeOrderInputArray);
                var count = _scBaseUtils.getAttributeCount(cancelOrderInputArray);
                var changeOrderInputModel = _scBaseUtils.getNewModelInstance();
                var strOrderHeaderKey = _scModelUtils.getStringValueFromPath("Order.OrderHeaderKey", screenData);
                _scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", strOrderHeaderKey, changeOrderInputModel);
                _scModelUtils.setStringValueAtModelPath("Order.OrderLines.OrderLine", changeOrderInputArray, changeOrderInputModel);
                if (count != 0 && releaseLineCount != 0) {
                    _isccsModelUtils.removeAttributeFromModel("Order.Action", cancelOrder);
                    _scModelUtils.setStringValueAtModelPath("Order.OrderLines.OrderLine", cancelOrderInputArray, cancelOrder);
                    _scBaseUtils.appendToArray(modelList, changeOrderInputModel);
                    _scBaseUtils.appendToArray(namespaceList, "extn_applyHoldOnRequestedLines");
                    _scBaseUtils.appendToArray(modelList, cancelOrder);
                    _scBaseUtils.appendToArray(namespaceList, "cancelOrderMashupRef");
                    _isccsUIUtils.callApis(this, modelList, namespaceList, mashupContext);
                } else if (releaseLineCount != 0) {
                    _isccsUIUtils.callApi(this, changeOrderInputModel, "extn_applyHoldOnRequestedLines", mashupContext);
                    _scModelUtils.setStringValueAtModelPath("CompleteOrder", true, mashupContext);
                } else if (count != 0) {
                    _isccsUIUtils.callApi(this, cancelOrder, "cancelOrderMashupRef", mashupContext);
                }
            } else if (releasedStatusCheck) {
                for (var index in orderLinesModel) {
                    var strMinLineStatus = _scModelUtils.getStringValueFromPath("MinLineStatus", orderLinesModel[index]);
                    var strOrderLineKey = _scModelUtils.getStringValueFromPath("OrderLineKey", orderLinesModel[index]);
                    if (strMinLineStatus == "3200") {
                        var cancelLines = _scModelUtils.getModelListFromPath("Order.OrderLines.OrderLine", cancelOrder);
                        for (var i in cancelLines) {
                            var cancelOrderLineKey = cancelLines[i].OrderLineKey;
                            if (cancelOrderLineKey == strOrderLineKey) {
                                _isccsModelUtils.removeItemFromArray(cancelLines, cancelLines[i]);
                                i = -1;
                                var changeOrderLine = _scBaseUtils.getNewModelInstance();
                                _scModelUtils.setStringValueAtModelPath("OrderLineKey", strOrderLineKey, changeOrderLine);
                                _scModelUtils.setStringValueAtModelPath("OrderHoldTypes.OrderHoldType.HoldType", "DC_CANCEL_HOLD", changeOrderLine);
                                _scModelUtils.setStringValueAtModelPath("OrderHoldTypes.OrderHoldType.Status", "1100", changeOrderLine);
                                _scModelUtils.setStringValueAtModelPath("OrderHoldTypes.OrderHoldType.ReasonText", holdReasonText, changeOrderLine);
                                _scBaseUtils.appendToArray(changeOrderInputArray, changeOrderLine);
                            }
                        }
                    }
                }
                var changeOrderInputModel = _scBaseUtils.getNewModelInstance();
                var strOrderHeaderKey = _scModelUtils.getStringValueFromPath("Order.OrderHeaderKey", screenData);
                _scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", strOrderHeaderKey, changeOrderInputModel);
                _scModelUtils.setStringValueAtModelPath("Order.OrderLines.OrderLine", changeOrderInputArray, changeOrderInputModel);
                var releaseLinesCount = _scBaseUtils.getAttributeCount(changeOrderInputArray);
                var cancelOrderLines = _scModelUtils.getStringValueFromPath("Order.OrderLines.OrderLine", cancelOrder);
                var count = _scBaseUtils.getAttributeCount(cancelOrderLines);
                if (count != 0 && releaseLinesCount != 0) {
                    _scBaseUtils.appendToArray(modelList, changeOrderInputModel);
                    _scBaseUtils.appendToArray(namespaceList, "extn_applyHoldOnRequestedLines");
                    _scBaseUtils.appendToArray(modelList, cancelOrder);
                    _scBaseUtils.appendToArray(namespaceList, "cancelOrderMashupRef");
                    _isccsUIUtils.callApis(this, modelList, namespaceList, mashupContext);
                } else if (releaseLinesCount != 0) {
                    _isccsUIUtils.callApi(this, changeOrderInputModel, "extn_applyHoldOnRequestedLines", mashupContext);
                    _scModelUtils.setStringValueAtModelPath("CompleteOrder", true, mashupContext);
                } else if (count != 0) {
                    _isccsUIUtils.callApi(this, cancelOrder, "cancelOrderMashupRef", mashupContext);
                }
            } else {
                _isccsUIUtils.callApi(this, cancelOrder, "cancelOrderMashupRef", mashupContext);
            }
        }
        else
        {
            var status = _scModelUtils.getStringValueFromPath("Order.Status",screenData);
            _scModelUtils.setStringValueAtModelPath("Order.Status",status,cancelOrder);
            _isccsUIUtils.callApi(this, cancelOrder, "cancelOrderMashupRef", mashupContext);
        }
	  }  
},
    extn_RestrictCancel: function(event, bEvent, ctrl, args){
		var getCompleteOrderDetails_output = _scScreenUtils.getModel(this, "getCompleteOrderDetails_output");
	       var orderLineList = _scModelUtils.getModelListFromPath("Order.OrderLines.OrderLine", getCompleteOrderDetails_output);
	       var statusCount = 0;
	       var index = 0;
		for(index in orderLineList)
		{
		  var strMinLineStatus= _scModelUtils.getStringValueFromPath("MinLineStatus",orderLineList[index]);
		  var releasedStatus = _scScreenUtils.getString(this,"extn_Released_Status");
                var POSentToVendorStatus = _scScreenUtils.getString(this,"extn_PO_Send_to_Vendor_Status");
		  var cancelledStatus=_scScreenUtils.getString(this,"extn_Cancelled_Status");
		  if(strMinLineStatus==releasedStatus || strMinLineStatus==cancelledStatus || strMinLineStatus==POSentToVendorStatus)
		  {
                  statusCount++;
		  }
		  else 
		  {
		  	continue;
		  }
		}

		if(statusCount == orderLineList.length)
		{
			 _scWidgetUtils.showWidget(this, "lblnoCancellation", false, null);
         		 _scWidgetUtils.hideWidget(this, "cmbReasoncode", false);
        		 _scWidgetUtils.hideWidget( this, "cancelType", false);
        	        _scWidgetUtils.setWidgetNonMandatory(this,"cmbReasoncode");
			 _scEventUtils.fireEventToParent(this, "disableNextButton", null);
		}
		else if(statusCount > 0)
		{
			_scWidgetUtils.disableOptionsInListWidget(this, "cancelType", "01");
			//_scWidgetUtils.setValue("cancelType", "02", false);
			var cancelRadioModel = _scBaseUtils.getNewModelInstance();
			var defaultValue = "02";
			_scModelUtils.setStringValueAtModelPath("Order.CancelType", defaultValue, cancelRadioModel);
			 var setModelOptns = _scBaseUtils.getNewBeanInstance();
             _scBaseUtils.setAttributeValue("clearOldVals", false, setModelOptns);
             _scScreenUtils.setModel(this, "getCancelProp_output", cancelRadioModel, setModelOptns);
             this.cancelType_changed(event, bEvent,"cancelType" , args);
		}


      },
	extn_AllowCombinationCancellation: function(event , bEvent, ctrl, args)
	{
   		var value =_scWidgetUtils.isWidgetVisible(this,"lblnoCancellation");
		if(!value)
		{
			this.canOrderBeModified= true;
		}

    },
    extn_afterBehaviorMashupCall: function(event,bEvent,ctrl,args)
    {
        var mashupArrayObj = args.mashupArray;
        var mashupOutput = null;
        for(var obj in mashupArrayObj){
            if((mashupArrayObj[obj].mashupRefId == "extn_applyHoldOnRequestedLines"))
            {
                var orderModel = _scScreenUtils.getModel(this, "getCompleteOrderDetails_output");
                var orderLinesModel = _scModelUtils.getModelListFromPath("Order.OrderLines.OrderLine", orderModel);
                var linesReleasedToStoresOrVendors = false;
                var strReleasedLinesMessage = _scScreenUtils.getString(this, "extn_ReleasedLinesMessage");
                var strIncludedInShipmentLines = _scScreenUtils.getString(this, "extn_IncludedInShipmentLinesMessage");
                for (var i in orderLinesModel) {
                    var strMinLineStatus = _scModelUtils.getNumberValueFromPath("MinLineStatus", orderLinesModel[i]);
                    if ((strMinLineStatus > 3200) && (strMinLineStatus < 3700)) {
                        linesReleasedToStoresOrVendors = true;
                    }
                }
                if (linesReleasedToStoresOrVendors) {
                    _scScreenUtils.showInfoMessageBox(
                        this, "<div>" + strReleasedLinesMessage + "</br></br>" + strIncludedInShipmentLines + "</div>", "extn_displayCancellationInfo", null, mashupContext);
                } else {
                    _scScreenUtils.showInfoMessageBox(
                        this, "<div>" + strReleasedLinesMessage + "</div>", "extn_displayCancellationInfo", null, mashupContext);
                }
            }
        }
    },
    extn_displayCancellationInfo: function(res, mashupContext) 
    {
        if (_scBaseUtils.equals(res, "Ok")) {
            var output = mashupContext;
            var modelOutput = _scBaseUtils.getNewModelInstance();
            var mashupRefId = null;
            var mashupArrayModel = _scModelUtils.getStringValueFromPath("mashupArray", output);
            for (var index in mashupArrayModel) {
                mashupRefId = mashupArrayModel[index].mashupRefId;
                if (_scBaseUtils.equals(mashupRefId, "cancelOrderMashupRef")) {
                    modelOutput = mashupArrayModel[index].mashupRefOutput;
                    if (modelOutput != null) {
                        this.handleCancelOrder(modelOutput, mashupContext);
                        modelOutput = _scBaseUtils.getNewModelInstance();
                    }
                }
                if (_scBaseUtils.equals(mashupRefId, "extn_applyHoldOnRequestedLines")) {
                    var orderFlag = _scModelUtils.getStringValueFromPath("CompleteOrder", mashupContext);
                    if (orderFlag) {
                        var orderModel = _scScreenUtils.getModel(this, "getCompleteOrderDetails_output");
                        var editordetails = sc.plat.dojo.utils.EditorUtils.getCurrentEditor(this);
                        var screeenInput = _scScreenUtils.getInitialInputData(this);
                        _isccsUIUtils.openWizardInEditor("isccs.order.wizards.orderSummary.OrderSummaryWizard", orderModel, "isccs.editors.OrderEditor", this);
                    }
                }
            }
        }
    },
    extn_ValidatingQtyForReleasedLines:function(event, bEvent, ctrl, args)
    {  
       var cancelOrderOptions = _scBaseUtils.getNewModelInstance();
       cancelOrderOptions = _scScreenUtils.getTargetModel(this, "getCancelProp_input", null);
       var cancelType = null;
       cancelType = _scModelUtils.getStringValueFromPath("Order.CancelType", cancelOrderOptions);
       if(cancelType == "02")
       {
           var cancelOrderListScreenObj = null;
           cancelOrderListScreenObj = _scScreenUtils.getChildScreen(this, "cancelOrderListScreen");
           var cancelOrderItems = null;
           cancelOrderItems = _scGridxUtils.getSelectedTargetRecordsUsingUId(cancelOrderListScreenObj, "OLST_listGrid");
           var selectedOrderLineModel = _scModelUtils.getStringValueFromPath("OrderLineList.OrderLine", cancelOrderItems); 
           var orderModel = _scScreenUtils.getModel(this,"getCompleteOrderDetails_output");
           var orderLinesModel =_scModelUtils.getStringValueFromPath("Order.OrderLines.OrderLine",orderModel);
           var QuantityCheck = false;
           for(var index in selectedOrderLineModel)
           {
             var strOrderLineKey = _scModelUtils.getStringValueFromPath("OrderLineKey",selectedOrderLineModel[index]);
             for(var i in orderLinesModel)
              {
                  var strMinLineStatus = _scModelUtils.getStringValueFromPath("MinLineStatus",orderLinesModel[i]);
                  var strSelectedOrderLineKey = _scModelUtils.getStringValueFromPath("OrderLineKey",orderLinesModel[i]);
                  if((strMinLineStatus == "3200") && (strOrderLineKey == strSelectedOrderLineKey ))
                  {
                          var availableQty =_scModelUtils.getNumberValueFromPath("AvailableQtyForCancelAndStopDelivery",selectedOrderLineModel[index]); 
                          var enteredQty = _scModelUtils.getNumberValueFromPath("CancelQuantity",selectedOrderLineModel[index]);
                          if(enteredQty < availableQty)
                          {
                             QuantityCheck = true;
                          }           
                  }
                }
            }
            if(QuantityCheck)
            {
                _isccsBaseTemplateUtils.showMessage(this,"Cancellation of partial quantity for order lines released to DC is not allowed.", "Error" ,null);
                _scEventUtils.stopEvent(bEvent);
                        
            }
            else
            {
                 _isccsBaseTemplateUtils.closeMessagePanel(event, bEvent, ctrl, args);
            }
        }   
    },
      extn_GiftBoxCancellation: function(selectedOrderLinesList) {
                var GiftBoxOrderModel = _scScreenUtils.getModel(this, "extn_GiftBoxCommonList");
                var CommonCodeList = _scModelUtils.getModelListFromPath("CommonCodeList.CommonCode", GiftBoxOrderModel);
                var GiftBoxID = null;
                for (var i in CommonCodeList) {
                    var CommonCodeObj = _scModelUtils.getModelFromList(CommonCodeList, i);
                    if (CommonCodeObj.CodeType == "LT_CustomCode_Gift") {
                        GiftBoxID = CommonCodeObj.CodeValue;
                    }
                }
                var cancelOrderListScreenObj = _scScreenUtils.getChildScreen(this, "cancelOrderListScreen");
                var getCompleteOrderLineList_output = _scScreenUtils.getModel(cancelOrderListScreenObj, "getCompleteOrderLineList_output");
                var orderLinesList = null;
                orderLinesList = _scModelUtils.getModelListFromPath("Page.Output.OrderLineList.OrderLine", getCompleteOrderLineList_output);
                var store = _scGridxUtils.getGridStoreUsingUId(cancelOrderListScreenObj, "OLST_listGrid");
                var OrderLineList1 = _scModelUtils.getModelListFromPath("_arrayOfAllItems", store);
                var TargetModel = _scScreenUtils.getTargetModel(cancelOrderListScreenObj, "getCompleteOrderLineList_input");
                var TargetOrderLinesList = _scModelUtils.getModelListFromPath("OrderLineList.OrderLine", TargetModel);
                var giftBoxCount = 0;
                var totalGiftWrapCount = 0;
                var selectedReleasedGiftWrapCount = 0;
                var selectNoReleasedGiftWrapCount = 0;
                var gitBoxesToBeDeletedCount = 0;
                var k = 0;
                var OrderLineKeyArray = _scBaseUtils.getNewArrayInstance();
                for (var m in OrderLineList1) {
                    var TargetLine = OrderLineList1[m];
                    if (!(_scBaseUtils.isVoid(TargetLine))) {
                        var ItemData = _scModelUtils.getModelListFromPath("_dataItem", TargetLine);
                        if ((!_scBaseUtils.isVoid(ItemData.ItemDetails)) && (_scBaseUtils.stringEquals(ItemData.ItemDetails.ItemID, GiftBoxID))) {
                            giftBoxCount++;
                            _scBaseUtils.appendToArray(OrderLineKeyArray, ItemData.OrderLineKey);
                        } else if (!_scBaseUtils.stringEquals(ItemData.MaxLineStatusDesc, "Cancelled") && _scBaseUtils.stringEquals(ItemData.GiftWrap, "Y")) {
                            totalGiftWrapCount++;
                        }
                    }
                }

                for (var m in selectedOrderLinesList) {
                    var selectedOrderLineModel = selectedOrderLinesList[m];
                    if (!(_scBaseUtils.isVoid(selectedOrderLineModel))) {
                        for (var index in OrderLineList1) {
                            var orderLineModel = OrderLineList1[index];
                            var ItemData = _scModelUtils.getModelListFromPath("_dataItem", orderLineModel);
                            [m]
                            var strOrderLineKey = _scModelUtils.getStringValueFromPath("OrderLineKey", ItemData);
                            if (_scBaseUtils.stringEquals(strOrderLineKey, _scModelUtils.getStringValueFromPath("OrderLineKey", selectedOrderLineModel))) {
                                var status = _scModelUtils.getNumberValueFromPath("MinLineStatus", ItemData);
                                var giftWrap = _scModelUtils.getStringValueFromPath("GiftWrap", ItemData);
                                if (status < 3200 && _scBaseUtils.stringEquals(giftWrap, "Y")) {
                                    var action = _scModelUtils.getStringValueFromPath("Action", selectedOrderLineModel);
                                    var CancelQuantityModel = _scModelUtils.getModelListFromPath("CancelQuantity", OrderLineList1[index]);
                                    var CancelQuantity = _scModelUtils.getModelFromList(CancelQuantityModel, k);
                                    if (parseInt(CancelQuantity) == parseInt(ItemData.OrderedQty)) {
                                        selectNoReleasedGiftWrapCount++;
                                    }
                                    if (action == "NEXT") {
                                        selectNoReleasedGiftWrapCount++;
                                    }
                                } else if (status == 3200 && _scBaseUtils.stringEquals(giftWrap, "Y")) {
                                    selectedReleasedGiftWrapCount++;
                                }
                            }
                        }
                    }
                }

                if (selectNoReleasedGiftWrapCount < totalGiftWrapCount && giftBoxCount == 1) {
                    gitBoxesToBeDeletedCount = 0;
                } else if (giftBoxCount == 1 && selectedReleasedGiftWrapCount > 0) {
                    gitBoxesToBeDeletedCount = 0;
                } else {
                    gitBoxesToBeDeletedCount = selectNoReleasedGiftWrapCount;
                }
                if (gitBoxesToBeDeletedCount > 0) {
                    var GiftboxTobeDeletedArray = [];
                    var count = 0;
                    for (var m in TargetOrderLinesList) {
                        indexObj = _scModelUtils.getModelFromList(TargetOrderLinesList, m);
                        if (indexObj.OrderedQty > 0) {
                            if (_scBaseUtils.containsInArray(OrderLineKeyArray, indexObj.OrderLineKey)) {
                                _scBaseUtils.appendToArray(GiftboxTobeDeletedArray, indexObj);
                                gitBoxesToBeDeletedCount--;
                            }
                        }
                        if (gitBoxesToBeDeletedCount == 0) {
                            break;
                        }
                    }
                }
                return GiftboxTobeDeletedArray;
            }
});
});

