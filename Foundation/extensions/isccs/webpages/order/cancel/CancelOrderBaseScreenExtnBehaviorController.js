


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/order/cancel/CancelOrderBaseScreenExtn","scbase/loader!sc/plat/dojo/controller/ExtnServerDataController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnCancelOrderBaseScreenExtn
			 ,
			    _scExtnServerDataController
){

return _dojodeclare("extn.order.cancel.CancelOrderBaseScreenExtnBehaviorController", 
				[_scExtnServerDataController], {

			
			 screenId : 			'extn.order.cancel.CancelOrderBaseScreenExtn'

			
			
			
			
			
						,

			
			
			 mashupRefs : 	[
	 		{
		 extnType : 			'MODIFY'
,
		 mashupId : 			'extn_cancelOrderService'
,
		 mashupRefId : 			'cancelOrderMashupRef'

	},
		{
		 extnType : 			'ADD'
,
		 mashupId : 			'extn_applyHoldOnRequestedLines'
,
		 mashupRefId : 			'extn_applyHoldOnRequestedLines'

	}

	]

}
);
});

