
scDefine(["dojo/text!./templates/CancelOrderBaseScreenExtn.html","scbase/loader!dijit/form/Button","scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/_base/lang","scbase/loader!dojo/text","scbase/loader!sc/plat","scbase/loader!sc/plat/dojo/binding/ButtonDataBinder","scbase/loader!sc/plat/dojo/utils/BaseUtils"]
 , function(			 
			    templateText
			 ,
			    _dijitButton
			 ,
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojolang
			 ,
			    _dojotext
			 ,
			    _scplat
			 ,
			    _scButtonDataBinder
			 ,
			    _scBaseUtils
){
return _dojodeclare("extn.order.cancel.CancelOrderBaseScreenExtnUI",
				[], {
			templateString: templateText
	
	
	
	
	
	
					,	
	namespaces : {
		targetBindingNamespaces :
		[
		],
		sourceBindingNamespaces :
		[
			{
	  value: 'extn_GiftBoxCommonList'
						,
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_7'
						,
	  description: "extn_GiftBoxCommonList"
						
			}
			
		]
	}

	
	,
	hotKeys: [ 
	]

,events : [
	]

,subscribers : {

local : [

{
	  eventId: 'initializeLayout'

,	  sequence: '51'

,	  description: 'extn_RestrictCancel'



,handler : {
methodName : "extn_RestrictCancel"

 
}
}
,
{
	  eventId: 'initializeLayout'

,	  sequence: '52'

,	  description: 'To allow partial cancellation '



,handler : {
methodName : "extn_AllowCombinationCancellation"

 
}
}
,
{
	  eventId: 'afterBehaviorMashupCall'

,	  sequence: '51'




,handler : {
methodName : "extn_afterBehaviorMashupCall"

 
}
}

]
}

});
});


