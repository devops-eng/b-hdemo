scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/order/cancel/CancelOrderListScreenExtnUI",
"scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!sc/plat/dojo/utils/GridxUtils",
"scbase/loader!sc/plat/dojo/utils/ModelUtils","scbase/loader!sc/plat/dojo/utils/ScreenUtils",
"scbase/loader!isccs/utils/OrderUtils","scbase/loader!isccs/utils/BaseTemplateUtils","scbase/loader!isccs/utils/UIUtils","scbase/loader!sc/plat/dojo/utils/EventUtils","scbase/loader!sc/plat/dojo/utils/WidgetUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnCancelOrderListScreenExtnUI,
				_scBaseUtils,
				_scGridxUtils,
				_scModelUtils,
				_scScreenUtils,
				_isccsOrderUtils,
				_isccsBaseTemplateUtils,
				_isccsUIUtils,
				_scEventUtils,
                            _scWidgetUtils
){ 
	return _dojodeclare("extn.order.cancel.CancelOrderListScreenExtn", [_extnCancelOrderListScreenExtnUI],{
	// custom code here

// Used to disable the "GiftBox" OrderLine.

extn_ValidateCancel: function(event,bEvent,ctrl,args){
/*var getCompleteOrderLineList_output = null;
        getCompleteOrderLineList_output = _scScreenUtils.getModel(this, "getCompleteOrderLineList_output");
        var orderLinesList = null;
        orderLinesList = _scModelUtils.getModelListFromPath("Page.Output.OrderLineList.OrderLine", getCompleteOrderLineList_output);
		var rowsCount = 0;
        rowsCount = _scBaseUtils.getAttributeCount(orderLinesList);
var vCountReleased=0;
        for (var i = 0;_scBaseUtils.lessThan(i, rowsCount);i = _scBaseUtils.incrementNumber(i,1))
		{
            var indexObj = null;
	     indexObj = _scModelUtils.getModelFromList(orderLinesList, i);
		var vMaxLineStatus=indexObj.MaxLineStatus;
		var vReleaseStatus="3200";

            if(_scBaseUtils.equals(vMaxLineStatus,vReleaseStatus))
			{
		vCountReleased=vCountReleased+1;
                var vOrderLineKey= indexObj.OrderLineKey;
                var _gridRowIndex = false;
                _gridRowIndex = _scGridxUtils.returnUniqueRowIndexUsingHiddenColumn(this, "OLST_listGrid", vOrderLineKey);
                _scGridxUtils.setGridRowDisabled (this, _gridRowIndex, "OLST_listGrid",true);
			}

		}
		_scGridxUtils.refreshGridxHeader(this, "OLST_listGrid");

		if(rowsCount==vCountReleased){
	    		 _scWidgetUtils.showWidget(this, "lblnoCancellation", false, null);
         		 _scWidgetUtils.hideWidget(this, "cmbReasoncode", false);
        		 _scWidgetUtils.hideWidget( this, "cancelType", false);
        	        _scWidgetUtils.setWidgetNonMandatory(this,"cmbReasoncode");
			 _scEventUtils.fireEventToParent(this, "disableNextButton", null);

		} */
},
extn_GetColor:function(gridReference,rowIndex,columnIndex,gridRowJSON,unformattedValue)
	{
	var attributeList = gridRowJSON.ItemDetails.AdditionalAttributeList.AdditionalAttribute;
	for(var index in attributeList)
	{
	    if(attributeList[index].Name =="Color") 
	    {
	        unformattedValue=attributeList[index].Value;
	    }
	}
	 return unformattedValue;
	},

   extn_GetSize:function(gridReference,rowIndex,columnIndex,gridRowJSON,unformattedValue)
	{
	var attributeList = gridRowJSON.ItemDetails.AdditionalAttributeList.AdditionalAttribute;
	for(var index in attributeList)
	{
	    if(attributeList[index].Name =="Size") 
	    {
	        unformattedValue=attributeList[index].Value;
	    }
	}
	 return unformattedValue;
	},
extn_DisableGiftBox: function(event, bEvent, ctrl, args) 
	{
  var parentscreen=args.screen.ownerScreen;
	    	//var parentscreen1=_isccsUIUtils.getParentScreen(this,false);
	if(!(_scBaseUtils.isVoid(parentscreen)))
	{
		var GiftBoxOrderModel = _scScreenUtils.getModel(parentscreen, "extn_GiftBoxCommonList");
        var CommonCodeList=_scModelUtils.getModelListFromPath("CommonCodeList.CommonCode", GiftBoxOrderModel);
			var GiftBoxID=null;
            	for(var i in CommonCodeList)
		{
            var CommonCodeObj = _scModelUtils.getModelFromList(CommonCodeList, i); 
            if(CommonCodeObj.CodeType=="LT_CustomCode_Gift")
            {
                 GiftBoxID=CommonCodeObj.CodeValue;
            }
            
		}
		var getCompleteOrderLineList_output = null;
        getCompleteOrderLineList_output = _scScreenUtils.getModel(this, "getCompleteOrderLineList_output");
        var orderLinesList = null;
        orderLinesList = _scModelUtils.getModelListFromPath("Page.Output.OrderLineList.OrderLine", getCompleteOrderLineList_output);
		var rowsCount = 0;
        rowsCount = _scBaseUtils.getAttributeCount(orderLinesList);
        for (var i = 0;_scBaseUtils.lessThan(i, rowsCount);i = _scBaseUtils.incrementNumber(i,1))
		{
            var indexObj = null;
            indexObj = _scModelUtils.getModelFromList(orderLinesList, i); 
	/*		var ItemId=indexObj.ItemDetails.ItemID;
            if(_scBaseUtils.equals(ItemId,GiftBoxID))
			{
                var vOrderLineKey= indexObj.OrderLineKey;
                var _gridRowIndex = false;
                _gridRowIndex = _scGridxUtils.returnUniqueRowIndexUsingHiddenColumn(this, "OLST_listGrid", vOrderLineKey);
                _scGridxUtils.setGridRowDisabled (this, _gridRowIndex, "OLST_listGrid",true);
			}
	*/
		}
         _scGridxUtils.refreshGridxHeader(this, "OLST_listGrid");
	}
	},

  isGridRowDisabled: function(
        rowData, screen) {
            var availableCancelQty = 0;
            var strMinLineStatus = _scBaseUtils.getValueFromPath("MinLineStatus", rowData);
            var releasedStatus = _scScreenUtils.getString(this,"extn_Released_Status");
            var POSentToVendorStatus = _scScreenUtils.getString(this,"extn_PO_Send_to_Vendor_Status");
	     if(_scBaseUtils.equals(strMinLineStatus,releasedStatus) || _scBaseUtils.equals(strMinLineStatus,"2100.100"))
	     {
                     return true;
            }
            else
	    {
            availableCancelQty = _isccsOrderUtils.calculateOrder_CancelQuantityWOFormat(
            this.txQtyRuleSetValue, rowData, false);
            if (
            availableCancelQty <= 0) {
                return true;
            } else {
                return false;
            }
          }
	}
});
});

