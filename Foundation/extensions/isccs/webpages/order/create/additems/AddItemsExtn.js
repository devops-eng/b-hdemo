scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/order/create/additems/AddItemsExtnUI",
	"scbase/loader!sc/plat/dojo/utils/ModelUtils",
	"scbase/loader!sc/plat/dojo/utils/BaseUtils",
	"scbase/loader!isccs/utils/UIUtils",
	"scbase/loader!sc/plat/dojo/utils/EventUtils",
	"scbase/loader!sc/plat/dojo/utils/WidgetUtils",
	"scbase/loader!sc/plat/dojo/utils/ScreenUtils",
	"scbase/loader!isccs/utils/ModelUtils",
	"scbase/loader!sc/plat/dojo/utils/ControllerUtils",
	"scbase/loader!isccs/utils/WidgetUtils",
	"scbase/loader!isccs/utils/BaseTemplateUtils",
	"scbase/loader!sc/plat/dojo/utils/EditorUtils",
	"scbase/loader!sc/plat/dojo/utils/GridxUtils"
]
,
function(			 
			    _dojodeclare
			 ,
			    _extnAddItemsExtnUI
			 ,
				_scModelUtils
			 ,
				_scBaseUtils
			 ,
				_isccsUIUtils
			 ,
				_scEventUtils
			 ,
				_scWidgetUtils
			 ,
				_scScreenUtils
			,
				_isccsModelUtils
			,
				_scControllerUtils
			,
				_isccsWidgetUtils
			,
				_isccsBaseTemplateUtils
			,
				_scEditorUtils
			,	_scGridxUtils
){ 
	return _dojodeclare("extn.order.create.additems.AddItemsExtn", [_extnAddItemsExtnUI],{
	// custom code here
	extnMashupCall: false,
	extn_GWPListObj: {},
	extn_giftProductsCheck: function(
        event, bEvent, ctrl, args){
		//this.openStoreSelectionPopup()
			var mashupArrayObj = args.mashupArray;
			var mashupOutput = null;
           		this.extn_RefreshButton();
			for(var obj in mashupArrayObj){
				if((mashupArrayObj[obj].mashupRefId == "modifyFulfillmentOptionsWithOvp")||(mashupArrayObj[obj].mashupRefId == "modifyFulfillmentOptions")){
					//_scEventUtils.fireEventInsideScreen(this, "reloadScreen", null, null);
					if(!this.extnMashupCall){
						mashupOutput = mashupArrayObj[obj].mashupRefOutput;
						_isccsUIUtils.callApi(this, this.extn_createModelForGWPCall(mashupOutput), "extn_HBCPromotionCall", args.mashupContext);
					
					}else{
						this.extnMashupCall = false;
					}
								
				}
//Code for Refresh
				if (_scBaseUtils.equals(mashupArrayObj[obj].mashupRefId, "getCompleteItemList")) {
                			_scWidgetUtils.disableWidget(this, "extn_Refresh_Button", false);
            }
// Code end for Refersh
			}				
		},
		extn_createModelForGWPCall:function(mashupOutput) {
			var orderNumberObj = _scModelUtils.getStringValueFromPath("Order.OrderNo", mashupOutput);
			var orderNumArray = orderNumberObj.split("");
			var newOrderNumber = "";
			for(var num = orderNumArray.length-9;num<orderNumArray.length; num++){
				newOrderNumber +=orderNumArray[num];
			}
			var targetModelObj = _scBaseUtils.getNewBeanInstance();
			var targetOrderLineModelObj =   _scBaseUtils.getNewArrayInstance();
			_scModelUtils.setStringValueAtModelPath("Order.BillToID", _scModelUtils.getStringValueFromPath("Order.BillToID", mashupOutput), targetModelObj);
			_scModelUtils.setStringValueAtModelPath("Order.EnterpriseCode", _scModelUtils.getStringValueFromPath("Order.EnterpriseCode", mashupOutput), targetModelObj);
			_scModelUtils.setStringValueAtModelPath("Order.OrderNo", newOrderNumber, targetModelObj);
			_scModelUtils.setStringValueAtModelPath("Order.PendingChangesUserID", _scModelUtils.getStringValueFromPath("Order.PendingChangesUserID", mashupOutput), targetModelObj);
			_scModelUtils.setStringValueAtModelPath("Order.SellerOrganizationCode", _scModelUtils.getStringValueFromPath("Order.SellerOrganizationCode", mashupOutput), targetModelObj);
			_scModelUtils.setStringValueAtModelPath("Order.Status", "1000", targetModelObj);
			var getOrderLines = _scModelUtils.getModelObjectFromPath("Order.OrderLines", mashupOutput);
					
			var OrderLine=_scModelUtils.getModelListFromPath("OrderLine",getOrderLines );
			var length=0;
			for(var orderLineNo in getOrderLines.OrderLine){
			    	var OrderLineObj = OrderLine[orderLineNo];
			    	var PrimeLineNo=_scModelUtils.getStringValueFromPath("PrimeLineNo", OrderLineObj);
			    	if(PrimeLineNo>length)
			    	{
			    	 length= PrimeLineNo;  
			    	}
			    	
			}
			for(var i =1;i<=length;i++)
			{
				for(var orderLineNo in getOrderLines.OrderLine){
			    	var OrderLineObj = OrderLine[orderLineNo];
			    	var PrimeLineNo=_scModelUtils.getStringValueFromPath("PrimeLineNo", OrderLineObj);
				if(PrimeLineNo==i)
				{
				    var ParentOrderLineRelationships=_scModelUtils.getStringValueFromPath("ParentOrderLineRelationships", OrderLineObj);
					var extnDeptCode = _scModelUtils.getStringValueFromPath("ItemDetails.Extn.ExtnDepartment", OrderLineObj);
					var extnClearanceLevel = _scModelUtils.getStringValueFromPath("Extn.ExtnClearanceLevel", OrderLineObj);
					var extnOriginalListPrice = _scModelUtils.getStringValueFromPath("Extn.ExtnOriginalListPrice", OrderLineObj);
					
                    if(_scBaseUtils.isVoid(ParentOrderLineRelationships)){		
						_scModelUtils.setStringValueAtModelPath("Item.Extn.ExtnDepartment", extnDeptCode, OrderLineObj);
        				_scModelUtils.setStringValueAtModelPath("Extn.ExtnClearanceLevel", extnClearanceLevel, OrderLineObj);
        				_scModelUtils.setStringValueAtModelPath("Extn.ExtnOriginalListPrice", extnOriginalListPrice, OrderLineObj);
        				_scModelUtils.setStringValueAtModelPath("Extn.ExtnMinOrderFlag", "1", OrderLineObj);
        				_isccsModelUtils.removeAttributeFromModel("ChildOrderLineRelationships",OrderLineObj);
        				_isccsModelUtils.removeAttributeFromModel("ParentOrderLineRelationships",OrderLineObj);
						_isccsModelUtils.removeAttributeFromModel("ItemDetails",OrderLineObj);
        				_isccsModelUtils.removeAttributeFromModel("ShipTogetherNo",OrderLineObj);
        				_scBaseUtils.appendToArray(targetOrderLineModelObj, OrderLineObj);
				    }
				}	
			}		
			}
			var paymentMethodsObj = _scBaseUtils.getNewBeanInstance();
			var promotionsObj = _scBaseUtils.getNewBeanInstance();
			_scModelUtils.addModelToModelPath("Order.PaymentMethods", paymentMethodsObj,targetModelObj);
			_scModelUtils.addModelToModelPath("Order.Promotions", promotionsObj,targetModelObj);
			_scModelUtils.addModelToModelPath("Order.OrderLines.OrderLine", targetOrderLineModelObj,targetModelObj);
			return targetModelObj;
		},
		
		extn_afterPromotionCall:function(
        event, bEvent, ctrl, args) {
			var output = args;
			var mashupIdentifier = null; 
			mashupIdentifier = _scControllerUtils.getMashupContextIdenitier(args.mashupContext);
			var mashupArray = _scModelUtils.getModelObjectFromPath("mashupArray", args);
			for(var count in mashupArray){
				if(mashupArray[count].mashupRefId == "extn_HBCPromotionCall"){
					var hbcPromotionCallOutput = mashupArray[count].mashupRefOutput;
 _scScreenUtils.setModel(this, "extn_GWPNS", hbcPromotionCallOutput , null);
 var childScreen = null;
            childScreen = _scScreenUtils.getChildScreen(this, "orderLineList");
            _scGridxUtils.refreshGridUsingUId(childScreen,"OLST_listGrid");
					var gwpPopupFlag = "N";
					for(var eachOrderLine in hbcPromotionCallOutput.Order.OrderLines.OrderLine){
						var orderLineObj = hbcPromotionCallOutput.Order.OrderLines.OrderLine[eachOrderLine];
						if(!_scBaseUtils.isVoid(orderLineObj.ExtnPassthroughRewardsList)){

								gwpPopupFlag = "Y";
                           }

					}
					if(_scBaseUtils.equals(gwpPopupFlag, "Y")){

						if(_scBaseUtils.equals(mashupIdentifier, "SAVEONPREVIOUS") || _scBaseUtils.equals(
            mashupIdentifier, "onNext")){
            				
            				var dialogParams = _scBaseUtils.getNewBeanInstance();
            				_scBaseUtils.setAttributeValue("applyButton", "Y", dialogParams);
							_scBaseUtils.setAttributeValue("Ok", "callbackForNextPreviousClick", dialogParams);
							_scBaseUtils.setAttributeValue("Cancel", "callbackForNextPreviousClick", dialogParams);
							var ConfirmationMessage = null;
                		ConfirmationMessage = _scScreenUtils.getString(this, "extn_GWP_Confirmation");
            				_scScreenUtils.showConfirmMessageBox(this,ConfirmationMessage,"callbackForNextPreviousClick", null,args);

						}else if(_scBaseUtils.equals(mashupIdentifier, "updateButtonClick")){
						//	this.extn_showGWPPopup();
						} else if(_scBaseUtils.isVoid(mashupIdentifier)){}else{
							
						}
						
					}else{
						if(_scBaseUtils.equals(mashupIdentifier, "SAVEONPREVIOUS") || _scBaseUtils.equals(mashupIdentifier, "onNext")){
							_scEventUtils.fireEventToParent(this, "onSaveSuccess", args);
						}
					}
				}
			}
		},
		callbackForNextPreviousClick:function(actionPerformed, model, popupParams) {
			var output = actionPerformed;
			if(output == "Ok"){
				//this.extn_showGWPPopup();
			}else
			{
				_scEventUtils.fireEventToParent(this, "onSaveSuccess", model);
			}
			
        },
        
		callbackSelectGWPPopup: function(
        actionPerformed, model, popupParams) {
			if(actionPerformed == "APPLY"){
				this.extnMashupCall = true;
				var itemsSelected = model;
				
				var orderDetailsModel = _scScreenUtils.getModel(this, "getCompleteOrderDetails_output"); 
				var ModelList=_scModelUtils.getModelListFromPath("Order.OrderLines.OrderLine",orderDetailsModel);
			var inputToChangeOrder = _scBaseUtils.getNewBeanInstance();
				_scModelUtils.setStringValueAtModelPath("Order.DocumentType", _scModelUtils.getStringValueFromPath("Order.DocumentType", orderDetailsModel), inputToChangeOrder);
				_scModelUtils.setStringValueAtModelPath("Order.EnterpriseCode", _scModelUtils.getStringValueFromPath("Order.EnterpriseCode", orderDetailsModel), inputToChangeOrder);
				_scModelUtils.setStringValueAtModelPath("Order.SellerOrganizationCode", _scModelUtils.getStringValueFromPath("Order.SellerOrganizationCode", orderDetailsModel), inputToChangeOrder);
				_scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", _scModelUtils.getStringValueFromPath("Order.OrderHeaderKey", orderDetailsModel), inputToChangeOrder);
				_scModelUtils.addModelToModelPath("Order.PersonInfoShipTo", _scModelUtils.getModelObjectFromPath("Order.PersonInfoShipTo", orderDetailsModel), inputToChangeOrder);
                  	var PopUpPrimeLineNo=_scModelUtils.getStringValueFromPath("screenInput.gwpList.PrimeLineNo",popupParams);

				var OrderLine = _scBaseUtils.getNewArrayInstance();
				var OrderLinesForChangeOrder = _scBaseUtils.getNewArrayInstance();
				var OrderLinesRelationsForChangeOrder = _scBaseUtils.getNewArrayInstance();
				var parentTransactionLineId = null;
				var orderlineTransNo = 0;
				_scModelUtils.addListToModelPath("OrderLine",_scModelUtils.getModelListFromPath("Order.OrderLines.OrderLine",orderDetailsModel),OrderLine);
				for(var orderLineCount in OrderLine.OrderLine){
					var orderLineForChangeOrder = _scBaseUtils.getNewBeanInstance();
					orderlineTransNo += 1;
					if(OrderLine.OrderLine[orderLineCount].PrimeLineNo == PopUpPrimeLineNo)
					{
						parentTransactionLineId = orderlineTransNo;
					}
					var ParentOrderLineRelationships=_scModelUtils.getStringValueFromPath("ParentOrderLineRelationships", OrderLine.OrderLine[orderLineCount]);
                    if(!_scBaseUtils.isVoid(ParentOrderLineRelationships)){		
                  	var PrimeLineNo=_scModelUtils.getStringValueFromPath("OrderLineRelationship.ParentLine.PrimeLineNo",ParentOrderLineRelationships);
                        if(PrimeLineNo==PopUpPrimeLineNo)
                        {
                          _scModelUtils.setStringValueAtModelPath("Action", "REMOVE", orderLineForChangeOrder);

                        }
                    }
    			    _scModelUtils.setStringValueAtModelPath("OrderLineKey", _scModelUtils.getStringValueFromPath("OrderLineKey", OrderLine.OrderLine[orderLineCount]), orderLineForChangeOrder);
//					_scModelUtils.setStringValueAtModelPath("PrimeLineNo", _scModelUtils.getStringValueFromPath("PrimeLineNo", OrderLine.OrderLine[orderLineCount]), orderLineForChangeOrder);
					_scModelUtils.setStringValueAtModelPath("ValidateItem", "N", orderLineForChangeOrder);
					_scModelUtils.setStringValueAtModelPath("TransactionalLineId", orderlineTransNo, orderLineForChangeOrder);
					_scModelUtils.addModelToModelPath("PersonInfoShipTo", _scModelUtils.getModelObjectFromPath("Order.PersonInfoShipTo", orderDetailsModel), orderLineForChangeOrder);
					_scBaseUtils.appendToArray(OrderLinesForChangeOrder, orderLineForChangeOrder);
				}
				orderlineTransNo = 100;
				for(var orderGWPLineCount in model){
					var orderLineForChangeOrder = _scBaseUtils.getNewBeanInstance();
					orderlineTransNo += 1;
					var childTransactionalLineId = orderlineTransNo;
					_scModelUtils.setStringValueAtModelPath("TransactionalLineId", childTransactionalLineId, orderLineForChangeOrder);
					_scModelUtils.setStringValueAtModelPath("ItemGroupCode", _scModelUtils.getModelObjectFromPath("ItemGroupCode", model[orderGWPLineCount]), orderLineForChangeOrder);
					_scModelUtils.setStringValueAtModelPath("ShipTogetherNo", "GWP_OrderLine", orderLineForChangeOrder);
					_scModelUtils.setStringValueAtModelPath("Action", "CREATE", orderLineForChangeOrder);
					_scModelUtils.setStringValueAtModelPath("ValidateItem", "N", orderLineForChangeOrder);
					_scModelUtils.addModelToModelPath("PersonInfoShipTo", _scModelUtils.getModelObjectFromPath("OrderLine.PersonInfoShipTo", orderDetailsModel), orderLineForChangeOrder);
					_scModelUtils.setStringValueAtModelPath("Item.ItemID", _scModelUtils.getModelObjectFromPath("ItemID",model[orderGWPLineCount]), orderLineForChangeOrder);
					_scModelUtils.setStringValueAtModelPath("Item.UnitOfMeasure", _scModelUtils.getModelObjectFromPath("UnitOfMeasure",model[orderGWPLineCount]), orderLineForChangeOrder);
					_scModelUtils.setStringValueAtModelPath("OrderLineTranQuantity.OrderedQty", "1", orderLineForChangeOrder);
					_scModelUtils.setStringValueAtModelPath("OrderLineTranQuantity.TransactionalUOM", _scModelUtils.getModelObjectFromPath("UnitOfMeasure",model[orderGWPLineCount]), orderLineForChangeOrder);
					_scModelUtils.setStringValueAtModelPath("LinePriceInfo.IsPriceLocked", "Y", orderLineForChangeOrder);
					_scModelUtils.setStringValueAtModelPath("LinePriceInfo.ListPrice","0.00", orderLineForChangeOrder);
					_scModelUtils.setStringValueAtModelPath("LinePriceInfo.UnitPrice","0.00", orderLineForChangeOrder);
					_scBaseUtils.appendToArray(OrderLinesForChangeOrder, orderLineForChangeOrder);

					var OrderLinesRelationForChangeOrder = _scBaseUtils.getNewBeanInstance();
					_scModelUtils.setStringValueAtModelPath("RelationshipType","GWP", OrderLinesRelationForChangeOrder);
					_scModelUtils.setStringValueAtModelPath("ParentLine.TransactionalLineId",parentTransactionLineId, OrderLinesRelationForChangeOrder);
					_scModelUtils.setStringValueAtModelPath("ChildLine.TransactionalLineId",childTransactionalLineId, OrderLinesRelationForChangeOrder);
					_scBaseUtils.appendToArray(OrderLinesRelationsForChangeOrder, OrderLinesRelationForChangeOrder);
				}
				_scModelUtils.addListToModelPath("Order.OrderLines.OrderLine",OrderLinesForChangeOrder,inputToChangeOrder);
				_scModelUtils.addListToModelPath("Order.OrderLineRelationships.OrderLineRelationship",OrderLinesRelationsForChangeOrder,inputToChangeOrder);
				var mashupContextBean = null;
				mashupContextBean = _scControllerUtils.getMashupContext(this);
				this.extnMashupCall = true;
				this.callMultiApi( inputToChangeOrder, mashupContextBean, "N", this);
			//	_isccsUIUtils.callApi(this, inputToChangeOrder, "modifyFulfillmentOptions", null);
			
			}
		},
		/*
		<OrderLine Action="CREATE" ItemGroupCode="PROD" ShipNode=""
            ShipTogetherNo="GWP_OrderLine" TransactionalLineId="104" ValidateItem="N">
            <Item ItemID="63722375" ProductClass="" UnitOfMeasure="EACH"/>
            <OrderLineTranQuantity OrderedQty="1.0" TransactionalUOM="EACH"/>
            <LinePriceInfo IsPriceLocked="Y" ListPrice="0.00" UnitPrice="0.00"/>
<OrderLineRelationships>
        <OrderLineRelationship RelationshipType="GWP">
            <ParentLine TransactionalLineId="1"/>
            <ChildLine TransactionalLineId="104"/>
        </OrderLineRelationship>
    </OrderLineRelationships>
            */
		extn_showGWPButton: function(event, bEvent, ctrl, args){
		//	var editorInputdata =  _scScreenUtils.getInitialInputData(_scEditorUtils.getCurrentEditor());
		//	var extn_GWPListObj = _scModelUtils.getStringValueFromPath("GWPItemDetails", editorInputdata);
		var tmodel=null;
		
              
                     tmodel=this.extn_apiInputMethod(tmodel);
                     			if(!_scBaseUtils.isVoid(tmodel)){
                    
                     var model = _scBaseUtils.getTargetModel(this, "getCompleteOrderLineList_input");
                     var InputModel=_scBaseUtils.getNewModelInstance();
                     _scModelUtils.setStringValueAtModelPath("Order.BillToID", _scModelUtils.getStringValueFromPath("Order.BillToID", model ), InputModel);
                _scModelUtils.setStringValueAtModelPath("Order.AllocationRuleID", _scModelUtils.getStringValueFromPath("Order.AllocationRuleID", model ), InputModel);
                     _scModelUtils.setStringValueAtModelPath("Order.DocumentType", _scModelUtils.getStringValueFromPath("Order.DocumentType", model ), InputModel);
                     _scModelUtils.setStringValueAtModelPath("Order.EnterpriseCode", _scModelUtils.getStringValueFromPath("Order.EnterpriseCode", model ), InputModel);
                     _scModelUtils.setStringValueAtModelPath("Order.EntryType", _scModelUtils.getStringValueFromPath("Order.EntryType", model ), InputModel);
                     _scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", _scModelUtils.getStringValueFromPath("Order.OrderHeaderKey", model ), InputModel);

                     _scModelUtils.setStringValueAtModelPath("Order.OrderLines.OrderLine", tmodel, InputModel);

                    var mashupContextBean = null;
				mashupContextBean = _scControllerUtils.getMashupContext(this);
				this.callMultiApi( InputModel, mashupContextBean, "N", this);
    }
    else
    {
 		var newOrderLinesObj = _scBaseUtils.getNewArrayInstance();
		var OrderDetailsObj =  _scScreenUtils.getModel(this, "getCompleteOrderDetails_output");
		var orderLinesObj = _scModelUtils.getModelObjectFromPath("Order.OrderLines.OrderLine", OrderDetailsObj);

		_isccsModelUtils.removeAttributeFromModel("Order.OrderLines.OrderLine",OrderDetailsObj);
			if(!_scBaseUtils.isVoid(orderLinesObj)){
				for(var lineNo in orderLinesObj){
					var orderLineObj = _scBaseUtils.getNewBeanInstance();
					var OldOrderLineObj = orderLinesObj[lineNo];
					 _scModelUtils.setStringValueAtModelPath("Item",_scModelUtils.getStringValueFromPath("Item", OldOrderLineObj), orderLineObj);
					_scModelUtils.setStringValueAtModelPath("LinePriceInfo",_scModelUtils.getStringValueFromPath("LinePriceInfo", OldOrderLineObj), orderLineObj);
					_scModelUtils.setStringValueAtModelPath("OrderLineKey",_scModelUtils.getStringValueFromPath("OrderLineKey", OldOrderLineObj), orderLineObj);
					_scModelUtils.setStringValueAtModelPath("PrimeLineNo",_scModelUtils.getStringValueFromPath("PrimeLineNo", OldOrderLineObj), orderLineObj);
					_scModelUtils.setStringValueAtModelPath("OrderedQty",_scModelUtils.getStringValueFromPath("OrderedQty", OldOrderLineObj), orderLineObj);
					_scModelUtils.setStringValueAtModelPath("ParentOrderLineRelationships",_scModelUtils.getStringValueFromPath("ParentOrderLineRelationships", OldOrderLineObj), orderLineObj);
					_scBaseUtils.appendToArray(newOrderLinesObj, orderLineObj);
				}
				_scModelUtils.addListToModelPath("Order.OrderLines.OrderLine",newOrderLinesObj,OrderDetailsObj);
				_isccsUIUtils.callApi(this, this.extn_createModelForGWPCall(OrderDetailsObj), "extn_HBCPromotionCall", null);
			}
		}	
	},
	
		
		extn_showGWPPopup: function(event, bEvent, ctrl, args){
			
			var output = args;
			var popupParams = _scBaseUtils.getNewBeanInstance();
				var screenParams = _scBaseUtils.getNewBeanInstance();
				//var editorInputdata =  _scScreenUtils.getInitialInputData(_scEditorUtils.getCurrentEditor());
				//var extn_GWPListObj = _scModelUtils.getStringValueFromPath("GWPItemDetails", editorInputdata);
				screenParams.gwpList = this.extn_GWPListObj;
				screenParams.gwpScreen = "Y";
				_scBaseUtils.addModelValueToBean("screenInput", screenParams, popupParams);
				//_scBaseUtils.addModelValueToBean("gwpScreenSetup", gwpScreen, popupParams);
				var dialogParams = _scBaseUtils.getNewBeanInstance();
				_scBaseUtils.setAttributeValue("closeCallBackHandler", "callbackSelectGWPPopup", dialogParams);
				_scBaseUtils.setAttributeValue("applyButton", "Y", dialogParams);
				var sMessage = null;
                		sMessage = _scScreenUtils.getString(
                		this, "extn_Select_Gift");
				_isccsUIUtils.openSimplePopup("isccs.common.alternateStore.StoreSelection", sMessage , this, popupParams, dialogParams);

				//_isccsUIUtils.openSimplePopup("isccs.common.alternateStore.StoreSelection", "Select Gift", this, popupParams, dialogParams);
		},

		handleUpdateOrder: function(screen, mashupIndentifier, tmodel, args){
            var sOrderHeaderKey = null;
            var length = 0;  
            sOrderHeaderKey = _scBaseUtils.getValueFromPath("Order.OrderHeaderKey", tmodel);
            var mashupContextBean = null;
            mashupContextBean = _scControllerUtils.getMashupContext(
            this);
            _scControllerUtils.setMashupContextIdenitier(
            mashupContextBean, mashupIndentifier);
            var orderLine = null;
            var isPriceOverridenForOrder = "N";
	     //Custom Code for GiftBox
	     var apiInput=this.extn_apiInputMethod(tmodel);
            if(!(_scBaseUtils.isVoid(apiInput))) 
            {
		  var OrderLineList=_scModelUtils.getModelListFromPath("", apiInput);
                for(var m in OrderLineList)
		    {
			var OrderLineElement=_scBaseUtils.getNewModelInstance();
			OrderLineElement =_scModelUtils.getModelFromList(OrderLineList,m);
			_scBaseUtils.appendToArray(tmodel.Order.OrderLines.OrderLine,OrderLineElement);
		    }
	    }
	   //Customcode end
            orderLine = _scBaseUtils.getValueFromPath("Order.OrderLines.OrderLine", tmodel);
            if (!(
            _scBaseUtils.isVoid("orderLine"))) {
                length = _scBaseUtils.getAttributeCount(
                orderLine);
			//Custom Code for Stapping PersonInfoShipTo
				for(var orderLineNum in tmodel.Order.OrderLines.OrderLine){
					var orderDetailsModel = _scScreenUtils.getModel(this, "getCompleteOrderDetails_output"); 
					_scModelUtils.addModelToModelPath("PersonInfoShipTo", _scModelUtils.getModelObjectFromPath("Order.PersonInfoShipTo", orderDetailsModel), tmodel.Order.OrderLines.OrderLine[orderLineNum]);

				}
			//custom Code End
                var orderHasValidationError = false;
                for (
                var index = 0;
                _scBaseUtils.lessThan(
                index, length);
                index = _scBaseUtils.incrementNumber(
                index, 1)) {
                    var currOrderLineBean = null;
                    currOrderLineBean = _scBaseUtils.getArrayBeanItemByIndex(
                    orderLine, index);
                    var currOrderLine = currOrderLineBean;
                    if (
                    _scBaseUtils.and(
                    _scBaseUtils.equals(
                    isPriceOverridenForOrder, "N"), _scBaseUtils.equals(
                    _scModelUtils.getStringValueFromPath("LinePriceInfo.IsPriceOverridenInPreview", currOrderLine), "Y"))) {
                        isPriceOverridenForOrder = "Y";
                    }
                    if (
                    _scBaseUtils.isVoid(
                    _scModelUtils.getStringValueFromPath("OrderLineTranQuantity.OrderedQty", currOrderLine))) {
                        _isccsBaseTemplateUtils.showMessage(
                        this, "screenHasErrors", "error", null);
                        return -1;
                    }
                    var isModelItem = "";
                    isModelItem = _scModelUtils.getStringValueFromPath("ItemDetails.PrimaryInformation.IsModelItem", currOrderLine);
                    if (
                    _scBaseUtils.equals(
                    isModelItem, "Y")) {
                        var modelItemErrorString = null;
                        _isccsBaseTemplateUtils.showMessage(
                        this, "ModelItemErrorMessage", "error", null);
                        return -1;
                    }
                }
            }
            var orderName = null;
            orderName = _scBaseUtils.getValueFromPath("OrderName", this);
            _scModelUtils.setStringValueAtModelPath("Order.OrderName", orderName, tmodel);
            var relatedItemRelationData = null;
            relatedItemRelationData = _scScreenUtils.getModel(
            this, "relatedItemRelationData");
            if (!(
            _scBaseUtils.isVoid(
            relatedItemRelationData))) {
                var relationshipModel = null;
                relationshipModel = _scModelUtils.getModelObjectFromPath("OrderLineRelationships", relatedItemRelationData);
                _scModelUtils.addModelToModelPath("Order.OrderLineRelationships", relationshipModel, tmodel);
            }
            if (
            _scBaseUtils.equals("updateButtonClick", mashupIndentifier)) {
                _scModelUtils.setStringValueAtModelPath("Order.UIOperation", mashupIndentifier, tmodel);
            }
            var mashupRefId = null;
            if (
            _scBaseUtils.isVoid(
            sOrderHeaderKey)) {
                mashupRefId = "createOrder";
                _isccsUIUtils.callApi(
                this, tmodel, mashupRefId, mashupContextBean);
            } else if (
            _scBaseUtils.equals("updateButtonClick", mashupIndentifier)) {
                this.callMultiApi(
                tmodel, mashupContextBean, isPriceOverridenForOrder, args);
            } else if (
            _scBaseUtils.equals(
            isPriceOverridenForOrder, "Y")) {
                _isccsUIUtils.callApi(
                this, tmodel, "modifyFulfillmentOptionsWithOvp", mashupContextBean);
            } else if (
            _scBaseUtils.equals("onNext", mashupIndentifier)) {
            	this.callMultiApi(
                tmodel, mashupContextBean, isPriceOverridenForOrder, args);
            /*    var mashupRefs = null;
                var screenId = null;
                var args = null;
                mashupRefs = _scBaseUtils.getNewArrayInstance();
                newModelData = _scBaseUtils.getNewModelInstance();
                _scModelUtils.addStringValueToModelObject("mashupRefId", "modifyFulfillmentOptions", newModelData);
                _scModelUtils.addStringValueToModelObject("mashupInput", tmodel, newModelData);
                _scBaseUtils.appendToArray(
                mashupRefs, newModelData);
                screenId = _scBaseUtils.getValueFromPath("declaredClass", this);
                args = _isccsUIUtils.formatArgForUpdateAndNextCall(
                screenId, mashupRefs);
                _scEventUtils.fireEventToParent(
                this, "combinedAPICallOnNext", args);*/
            } else {
            	this.callMultiApi(
                tmodel, mashupContextBean, isPriceOverridenForOrder, args);
//                _isccsUIUtils.callApi(this, tmodel, "modifyFulfillmentOptions", mashupContextBean);
            }
        },
		
		handleModifyFulfillmentOptions: function(
        mashupContext, modelOutput) {
            var orderHasValidationError = true;
            var mashupIdentifier = null;
            var refreshGrid = false;
            var addItemsLinesScr = false;
            mashupIdentifier = _scControllerUtils.getMashupContextIdenitier(
            mashupContext);
            addItemsLinesScr = _scScreenUtils.getChildScreen(
            this, "orderLineList");
            _isccsWidgetUtils.commitUncommittedGridRecords(
            addItemsLinesScr, "OLST_listGrid");
            if (
            _scBaseUtils.equals(
            mashupIdentifier, "updateButtonClick")) {
                this.handleCreateUpdateOrder(
                mashupContext, modelOutput, _scControllerUtils.getMashupContextIdenitier(
                mashupContext));
            } else if (
            _scBaseUtils.equals(
            mashupIdentifier, "SAVEONPREVIOUS")) {
                var args = null;
                args = _scBaseUtils.getNewBeanInstance();
 //               _scEventUtils.fireEventToParent(
 //               this, "onSaveSuccess", args);
            } else {
                if (
                _scBaseUtils.equals(
                mashupIdentifier, "reloadScreen")) {
                    refreshGrid = true;
                }
                orderHasValidationError = _scModelUtils.getBooleanValueFromPath("Order.OrderValidationErrorPresent", modelOutput, false);
                this.showSuccessMessage(
                mashupIdentifier);
                if (
                _scBaseUtils.or(
                _scBaseUtils.isBooleanTrue(
                orderHasValidationError), _scBaseUtils.isBooleanTrue(
                refreshGrid))) {
                    this.showOrderLines(
                    this);
                } else if (
                _scBaseUtils.equals(
                mashupIdentifier, "onNext")) {
                    if (
                    _scBaseUtils.greaterThan(
                    _scModelUtils.getNumberValueFromPath("Order.OrderLines.TotalNumberOfRecords", modelOutput), 0)) {
                        var args = null;
                        args = _scBaseUtils.getNewBeanInstance();
                        _scBaseUtils.setAttributeValue("argumentList", "", args);
                        var modelToAdd = null;
                        modelToAdd = _scBaseUtils.getNewBeanInstance();
                        _scBaseUtils.addBeanValueToBean("argumentList", modelToAdd, args);
                      //  _scEventUtils.fireEventToParent(
                      //  this, "onSaveSuccess", args);
                    } else {
                        _isccsBaseTemplateUtils.showMessage(
                        this, "AddProductToOrder", "error", null);
                    }
                }
            }
        },

extn_getAllocationRuleID:function(event, bEvent, ctrl, args){

	var commonCodeList= event.CommonCode;
	for(var count in commonCodeList){
	    if(commonCodeList[count].CodeValue == "AllocationRuleID"){
			return commonCodeList[count].CodeShortDescription;
		}
	}
},

handleCreateUpdateOrder: function(
        mashupContext, modelOutput, mashupIdentifier) {
            this.resetVariablesAndWidgets();
                _scScreenUtils.setModel(this, "getCompleteOrderDetails_output", modelOutput, null);
        },
extn_apiInputMethod: function(tmodel)
	{
			tmodel = _scBaseUtils.getTargetModel(this, "getCompleteOrderLineList_input");
			var ChildScreen=_scScreenUtils.getChildScreen(this, "orderLineList");
            var store=_scGridxUtils.getGridStoreUsingUId(ChildScreen,"OLST_listGrid");
            var OrderLineList1=_scModelUtils.getModelListFromPath("_arrayOfAllItems", store); 
            var OrderLineList=_scModelUtils.getModelListFromPath("Order.OrderLines.OrderLine", tmodel);
            var GiftBoxOrderModel = _scScreenUtils.getModel(this, "extn_getGiftBoxCommonCodeList_AddItems");
            var CommonCodeList=_scModelUtils.getModelListFromPath("CommonCodeList.CommonCode", GiftBoxOrderModel);
			var GiftBoxID=null;
            	for(var i in CommonCodeList)
		{
            var CommonCodeObj = _scModelUtils.getModelFromList(CommonCodeList, i); 
            if(CommonCodeObj.CodeType=="LT_CustomCode_Gift")
            {
                 GiftBoxID=CommonCodeObj.CodeValue;
            }
            
		}
			var DeleteCount=0;
			var GiftWrappedItems=0;
			var GiftBoxesCount=0;
			var GiftBoxtobeDeleted=0;
			var GiftBoxObjects=_scBaseUtils.getNewArrayInstance();
			for(var m in OrderLineList1)
				{
					var TargetLine = OrderLineList1[m];
					var k=0;
					if(!(_scBaseUtils.isVoid(TargetLine)))
					{
						var orderLineKeyColData = _scModelUtils.getModelListFromPath("Action", TargetLine);
						var Action = _scModelUtils.getModelFromList(orderLineKeyColData, k);
						var ItemData = _scModelUtils.getModelListFromPath("_dataItem", TargetLine);
                        var QuantityObj = _scModelUtils.getModelListFromPath("Quantity", TargetLine);
                        var Quantity = _scModelUtils.getModelFromList(QuantityObj, k);
						var GiftWrap=_scModelUtils.getStringValueFromPath("GiftWrap", ItemData);
						var ItemID=_scModelUtils.getStringValueFromPath("ItemDetails.ItemID", ItemData);
						if(_scBaseUtils.stringEquals(GiftWrap,"Y") && !(_scBaseUtils.stringEquals(ItemID,GiftBoxID)))
						{
							GiftWrappedItems=parseInt(GiftWrappedItems)+1;
						}
						if(_scBaseUtils.stringEquals(Action,"REMOVE") && _scBaseUtils.stringEquals(GiftWrap,"Y"))
						{
							DeleteCount=parseInt(DeleteCount)+1;
						}
                        else if(Quantity=="0"  && _scBaseUtils.stringEquals(GiftWrap,"Y"))
                        {
                            DeleteCount=parseInt(DeleteCount)+1;
                        }
						if(_scBaseUtils.stringEquals(ItemID,GiftBoxID))
						{
							GiftBoxesCount=parseInt(GiftBoxesCount)+1;
							_scBaseUtils.appendToArray(GiftBoxObjects,m);
						}
                        
					}
				}
			if(GiftWrappedItems==DeleteCount && GiftBoxesCount=="1" && DeleteCount>"0")
			{
				GiftBoxtobeDeleted=1;
         	}
			else if(GiftWrappedItems==GiftBoxesCount && DeleteCount>"0")
			{
				GiftBoxtobeDeleted=DeleteCount;
			}else if(GiftWrappedItems=="0"&& GiftBoxesCount>"0")
                         {
                             GiftBoxtobeDeleted=GiftBoxesCount;
			}
		if(GiftBoxtobeDeleted>0) 
        {
        	var apiInput=_scBaseUtils.getNewArrayInstance();	
               var count=0;
               for(var i in OrderLineList)
                {
                     if(_scBaseUtils.containsInArray(GiftBoxObjects,i) && count<GiftBoxtobeDeleted)
                    {
                      //  indexObj = _scModelUtils.getModelFromList(OrderLineList, i);
                        var GiftBoxes=OrderLineList[i];
						_scModelUtils.setStringValueAtModelPath("Action","REMOVE", GiftBoxes);
						_scBaseUtils.appendToArray(apiInput,GiftBoxes);
                         count=count+1;
                        }
                } 
			return apiInput;
		}
	},
extn_OpenGWPPopUp:function(event, bEvent, ctrl, args){
    var gwpOutNSValue = _scScreenUtils.getModel(this, "extn_GWPNS");
    var orderLines = _scModelUtils.getModelListFromPath("Order.OrderLines.OrderLine", gwpOutNSValue);
    var primeLineNumber = _scModelUtils.getStringValueFromPath("PrimeLineNo", args);
    for(var eachOrderLine in orderLines){
		var orderLineObj = orderLines[eachOrderLine];
		if(_scBaseUtils.equals(primeLineNumber, _scModelUtils.getStringValueFromPath("PrimeLineNo", orderLineObj))){

			this.extn_GWPListObj = orderLineObj;
			this.extn_showGWPPopup();
		}
}
	},
extn_GiftBoxValidationMethod:function(event, bEvent, ctrl, args){
	var GiftBoxOrderModel = _scScreenUtils.getModel(this, "extn_getGiftBoxCommonCodeList_AddItems");
       var CommonCodeList=_scModelUtils.getModelListFromPath("CommonCodeList.CommonCode", GiftBoxOrderModel);
	var GiftBoxID=null;
       for(var i in CommonCodeList)
	{
            var CommonCodeObj = _scModelUtils.getModelFromList(CommonCodeList, i); 
            if(CommonCodeObj.CodeType=="LT_CustomCode_Gift")
            {
                GiftBoxID=CommonCodeObj.CodeValue;
            }
            
	}
	var targetModel = _scBaseUtils.getTargetModel(this, "getCompleteItemList_input", null);
       var CurrentItemID = _scModelUtils.getStringValueFromPath("Item.BarCode.BarCodeData", targetModel);
	if(!(_scBaseUtils.isVoid(CurrentItemID)))
	{
       	if (_scBaseUtils.equals(GiftBoxID,CurrentItemID))
            	{
			_isccsBaseTemplateUtils.showMessage(this, "extn_GiftBox_Item_Validation", "error", null);
                	_scEventUtils.stopEvent(bEvent);
                	return true;
            	}
	}
},

extn_PriceValidation:function(event, bEvent, ctrl, args) 
	{
	var tmodel = _scBaseUtils.getNewModelInstance();
	var sourceModel =  _scScreenUtils.getModel(this, "getCompleteOrderDetails_output");
       var options = null;
       var action = args.Action;
            options = _scBaseUtils.getNewBeanInstance();
            _scBaseUtils.addStringValueToBean("added", "true", options);
            _scBaseUtils.addStringValueToBean("modified", "true", options);
            _scBaseUtils.addStringValueToBean("deleted", "true", options);
            _scBaseUtils.setAttributeValue("allowEmpty", true, options);
	    tmodel = _scBaseUtils.getTargetModel(
            this, "getCompleteOrderLineList_input", options);
            var orderLinesList= tmodel.Order.OrderLines.OrderLine;
            var sOrderLinesList = sourceModel.Order.OrderLines.OrderLine;
           
        /*    for(var i in sOrderLinesList)
            {
                var parentLine =_scModelUtils.getStringValueFromPath("ParentOrderLineRelationships.OrderLineRelationship", sOrderLinesList[i]);
                if(!(_scBaseUtils.isVoid(parentLine)))
                {
                var sOrderLinekey = _scModelUtils.getStringValueFromPath("OrderLineKey",sOrderLinesList[i]); 
                for(var t in orderLinesList)
                {
                   var tOrderLinekey = _scModelUtils.getStringValueFromPath("OrderLineKey",orderLinesList[t]); 
                   if(sOrderLinekey == tOrderLinekey )
                   {
                       var quantity = _scModelUtils.getStringValueFromPath("OrderLineTranQuantity.OrderedQty",orderLinesList[t]); 
                       if(quantity >1)
                       {
                           _isccsBaseTemplateUtils.showMessage(this,"Ordered Quantity for the Free Gift Items cannot be greater than 1.","ERROR",null);
                           sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
                            return true;
                       }
                         else
                       {
                        _isccsBaseTemplateUtils.closeMessagePanel(event, bEvent, ctrl, args);
                       }
                   } 
                }
                }
                
            }*/
            
            for(var index in orderLinesList)
            {
                var listPrice =_scModelUtils.getNumberValueFromPath("LineOverallTotals.DisplayUnitPrice",orderLinesList[index]);
                var action = orderLinesList[index].Action;
                var displayUnitPrice = _scModelUtils.getStringValueFromPath("LinePriceInfo.DisplayUnitPrice",orderLinesList[index]);
                if(listPrice==0 && action!="REMOVE" && _scBaseUtils.isVoid(displayUnitPrice))
                {
				var sMessage = null;
                		sMessage = _scScreenUtils.getString(
                		this, "extn_Zero_Item");

                    _isccsBaseTemplateUtils.showMessage(this,sMessage, "error", null);
                    sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
                    return true;
                }
                else
                {
                     _isccsBaseTemplateUtils.closeMessagePanel(event, bEvent, ctrl, args);
                }
            }
       // }
	},
//Code for Refresh
extn_RefreshButton:function(event, bEvent, ctrl, args) 
{
	if(_scWidgetUtils.isWidgetDisabled(this,"update_order"))
		{
		  _scWidgetUtils.enableWidget(this, "extn_Refresh_Button", false);
		}else
		{
		  _scWidgetUtils.disableWidget(this, "extn_Refresh_Button", false);   
		} 
}

});
});
