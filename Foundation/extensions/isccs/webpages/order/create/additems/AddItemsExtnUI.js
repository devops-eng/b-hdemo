
scDefine(["dojo/text!./templates/AddItemsExtn.html","scbase/loader!dijit/form/Button","scbase/loader!dijit/form/TextBox","scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/_base/lang","scbase/loader!dojo/text","scbase/loader!sc/plat","scbase/loader!sc/plat/dojo/binding/ButtonDataBinder","scbase/loader!sc/plat/dojo/binding/SimpleDataBinder","scbase/loader!sc/plat/dojo/utils/BaseUtils"]
 , function(			 
			    templateText
			 ,
			    _dijitButton
			 ,
			    _dijitTextBox
			 ,
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojolang
			 ,
			    _dojotext
			 ,
			    _scplat
			 ,
			    _scButtonDataBinder
			 ,
			    _scSimpleDataBinder
			 ,
			    _scBaseUtils
){
return _dojodeclare("extn.order.create.additems.AddItemsExtnUI",
				[], {
			templateString: templateText
	
	
	
	
	
	
					,	
	namespaces : {
		targetBindingNamespaces :
		[
		],
		sourceBindingNamespaces :
		[
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_8'
						,
	  description: 'PriceList_NS'
						,
	  value: 'extn_PriceList_NS'
						
			}
			,
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_9'
						,
	  description: 'extn_AllocationRuleID_Output'
						,
	  value: 'extn_AllocationRuleID_Output'
						
			}
			,
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_10'
						,
	  description: 'extn_getGiftBoxCommonCodeList_AddItems'
						,
	  value: 'extn_getGiftBoxCommonCodeList_AddItems'
						
			}
			,
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_11'
						,
	  description: 'GWp Item List'
						,
	  value: 'extn_GWPNS'
						
			}
			
		]
	}

	

	,
	hotKeys: [ 
	]

,events : [
	]

,subscribers : {
local : [

{
	  eventId: 'update_screen_evnt'

,	  sequence: '51'

,	  description: 'Enable/Disable refresh button'



,handler : {
methodName : "extn_RefreshButton"

 
}
}
,
{
	  eventId: 'txt_ItemID_onKeyUp'

,	  sequence: '19'

,	  description: 'GiftBox Item Validation'



,handler : {
methodName : "extn_GiftBoxValidationMethod"

 
}
}
,
{
	  eventId: 'afterScreenLoad'

,	  sequence: '51'




,handler : {
methodName : "extn_showGWPButton"

 
}
}
,
{
	  eventId: 'saveCurrentPage'

,	  sequence: '19'

,	  description: 'extn_PriceValidation'



,handler : {
methodName : "extn_PriceValidation"

 
}
}
,
{
	  eventId: 'add_item_onClick'

,	  sequence: '19'

,	  description: 'GiftBox Item Validation'



,handler : {
methodName : "extn_GiftBoxValidationMethod"

 
}
}
,
{
	  eventId: 'update_order_onClick'

,	  sequence: '19'

,	  description: 'extn_PriceValidation'



,handler : {
methodName : "extn_PriceValidation"

 
}
}
,
{
	  eventId: 'afterBehaviorMashupCall'

,	  sequence: '51'




,handler : {
methodName : "extn_giftProductsCheck"

 
}
}
,
{
	  eventId: 'onExtnMashupCompletion'

,	  sequence: '51'




,handler : {
methodName : "extn_afterPromotionCall"

 
}
}
,
{
	  eventId: 'extn_GWPbutton_onClick'

,	  sequence: '51'




,handler : {
methodName : "extn_showGWPPopup"

 
}
}
,
{
	  eventId: 'extn_OpenGWPPopUp'

,	  sequence: '51'

,	  description: 'To open GWP PopUp'



,handler : {
methodName : "extn_OpenGWPPopUp"

 
}
}
,
{
	  eventId: 'extn_Refresh_Button_onClick'

,	  sequence: '51'

,	  description: 'handleReloadScreen'



,handler : {
methodName : "handleReloadScreen"

 
}
}

]
					 	 ,
}

});
});


