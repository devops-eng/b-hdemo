scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/order/create/additems/AddItemsLinesDetailsExtnUI","scbase/loader!sc/plat/dojo/utils/BaseUtils",
"scbase/loader!sc/plat/dojo/utils/WidgetUtils","scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!isccs/utils/UIUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnAddItemsLinesDetailsExtnUI
                      ,
                          _scBaseUtils
                      ,
                         _scWidgetUtils
                      ,
                         _scModelUtils
			 ,  
			    _isccsUIUtils
){ 
	return _dojodeclare("extn.order.create.additems.AddItemsLinesDetailsExtn", [_extnAddItemsLinesDetailsExtnUI],{
	// custom code here

   getItemQuantity: function(
        dataValue, screen, widget, namespace, modelObj, options) {
            var quantity = dataValue;
            var parentLine = _scModelUtils.getStringValueFromPath("ParentOrderLineRelationships.OrderLineRelationship.ParentOrderLineKey",modelObj);
         /*  var ParentScreen = _isccsUIUtils.getParentScreen(this, true);
           var GiftBoxCommonCodeList= _scScreenUtils.getModel(ParentScreen , "extn_getGiftBoxCommonCodeList_AddItemsLines");
            var CommonCodeList=_scModelUtils.getModelListFromPath("CommonCodeList.CommonCode", GiftBoxCommonCodeList);
			var GiftBoxID=null;
            	for(var i in CommonCodeList)
		{
            var CommonCodeObj = _scModelUtils.getModelFromList(CommonCodeList, i); 
            if(CommonCodeObj.CodeType=="LT_CustomCode_Gift")
            {
                 GiftBoxID=CommonCodeObj.CodeValue;
            }
            
		}
            var ItemID= _scModelUtils.getStringValueFromPath("Item.ItemID",modelObj);*/

            if (
            _scBaseUtils.isVoid(
            dataValue)) {
                _scWidgetUtils.setFocusOnWidgetUsingUid(
                this, "quantity");
                return dataValue;
            }else if((!( _scBaseUtils.isVoid(parentLine))))
            {
                 _scWidgetUtils.disableWidget(
                this, "quantity",false);
                return dataValue;
            }
            else
            {
                _scWidgetUtils.enableWidget(
                this, "quantity",false);
            }
            return dataValue;       
 }
});
});

