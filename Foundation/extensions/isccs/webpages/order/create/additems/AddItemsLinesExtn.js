scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/order/create/additems/AddItemsLinesExtnUI",
"scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!sc/plat/dojo/utils/ScreenUtils","scbase/loader!sc/plat/dojo/utils/GridxUtils",
"scbase/loader!sc/plat/dojo/utils/ModelUtils","scbase/loader!sc/plat/dojo/utils/EventUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnAddItemsLinesExtnUI
			 ,
			 	_scBaseUtils
			 	,_scScreenUtils
			 	,scGridxUtils
                      ,
                          _scModelUtils
                      ,
                      _scEventUtils

                          
){  
	return _dojodeclare("extn.order.create.additems.AddItemsLinesExtn", [_extnAddItemsLinesExtnUI],{
	// custom code here
	extn_PopulateUnitPrice: function(gridReference, rowIndex, columnIndex, gridRowJSON, unformattedValue,value,gridRowRecord,colConfig)
	{
		var unitPrice=0;
		/*var OrderModel = _scScreenUtils.getModel(this, "extn_UnitPrice_NS");

		if(!_scBaseUtils.isVoid(OrderModel))
		{
			if(OrderModel.ItemID==gridRowJSON.ItemDetails.ItemID)
			{
			if(!(_scBaseUtils.isVoid(OrderModel.ListPrice)))
				unitPrice = OrderModel.ListPrice;
			else if(!(_scBaseUtils.isVoid(OrderModel.UnitPrice)))
				unitPrice = OrderModel.UnitPrice;
			else
				unitPrice = 0.00;
			}
			else
		{
			unitPrice = gridRowJSON.LinePriceInfo.UnitPrice;
		}
		}else
		{
			unitPrice = gridRowJSON.LinePriceInfo.UnitPrice;
		}*/
	
		if(_scBaseUtils.isVoid(gridRowJSON.PrimeLineNo))
		{
			if(!(_scBaseUtils.isVoid(gridRowJSON.ItemDetails.ComputedPrice.ListPrice)))
				unitPrice = gridRowJSON.ItemDetails.ComputedPrice.ListPrice;
			else if(!(_scBaseUtils.isVoid(gridRowJSON.ItemDetails.ComputedPrice.UnitPrice)))
				unitPrice = gridRowJSON.ItemDetails.ComputedPrice.UnitPrice;
			else
				unitPrice = 0.00;
		}
		else
		{
			unitPrice = gridRowJSON.LinePriceInfo.UnitPrice;
		}

	return unitPrice;
	},

	extn_PriceValueMethod:function(event, bEvent, ctrl, args){
		var output= args.PricelistLine;
		_scScreenUtils.setModel(this, "extn_UnitPrice_NS", args.PricelistLine, null);
		scGridxUtils.refreshGridUsingUId(this,"OLST_listGrid");
		
	},

  checkOverridenPriceAndUpdate: function() {
            var newModelData = null;
            var overridenPrice = null;
            var isPriceLessThanZero = false;
            var isPriceValZero = false;
            var tmodel = null;
            var updatedListPrice = "0";
            newModelData = _scScreenUtils.getModel(
            this, "getOverridenPrice");
            tmodel = _scScreenUtils.getTargetModel(
            this, "getCompleteOrderLineList_input", null);
            updatedListPrice = _scModelUtils.getStringValueFromPath("LinePriceInfo.ListPrice", tmodel);
            if (!(
            _scBaseUtils.isVoid(
            updatedListPrice))) {
                _scModelUtils.setStringValueAtModelPath("LinePriceInfo.ListPrice", updatedListPrice, newModelData);
            }
            _scModelUtils.setStringValueAtModelPath("LinePriceInfo.IsPriceOverridenInPreview", "Y", newModelData);
			 _scModelUtils.setStringValueAtModelPath("LinePriceInfo.IsPriceLocked", "Y", newModelData);
            this.fireEventToUpdateGridColumn(
            this, newModelData);
        },
extn_DisableGiftBox: function(event, bEvent, ctrl, args)
	{
	
	   var orderModel =args.result;
	   var OrderLineList=_scModelUtils.getModelListFromPath("Page.Output.OrderLineList.OrderLine", orderModel);
	   var GiftBoxOrderModel = _scScreenUtils.getModel(this, "extn_getGiftBoxCommonCodeList_AddItemsLines");
	   var CommonCodeList=_scModelUtils.getModelListFromPath("CommonCodeList.CommonCode", GiftBoxOrderModel);
	   var GiftBoxID=null;
	   if(!(_scBaseUtils.isVoid(CommonCodeList)))
		{	
         		 for(var i in CommonCodeList)
	 			{
	            			var CommonCodeObj = _scModelUtils.getModelFromList(CommonCodeList, i); 
		    			var CodeType=_scModelUtils.getStringValueFromPath("CodeType", CommonCodeObj );
           	    			if(CodeType=="LT_CustomCode_Gift")
            				{
                 				GiftBoxID=_scModelUtils.getStringValueFromPath("CodeValue", CommonCodeObj );
                    			}
          			}
      			for(var i in OrderLineList)
				{
	  	   			var OrderLineElement = _scModelUtils.getModelFromList(OrderLineList, i); 
	           			var ItemID = _scModelUtils.getStringValueFromPath("ItemDetails.ItemID", OrderLineElement);
		   			if(ItemID==GiftBoxID)
					{
                			       var vOrderLineKey=OrderLineElement.OrderLineKey;
                  				var _gridRowIndex = null;
                 				_gridRowIndex = scGridxUtils.returnUniqueRowIndex(this, "OLST_listGrid", vOrderLineKey);
                  				scGridxUtils.setGridRowDisabled (this, _gridRowIndex, "OLST_listGrid",true);
					}
				}
		}
	},
extn_GWPLink: function(event, bEvent, ctrl, args)
{
  	     var cellJson = _scBaseUtils.getAttributeValue("cellJson",false,args);
		if (_scBaseUtils.equals("extn_GWP", _scBaseUtils.getAttributeValue("colField", false, cellJson)))
		 { 
		     _scEventUtils.stopEvent(bEvent);
	            var eventDefn = _scBaseUtils.getNewBeanInstance();
       	     var blankModel = _scBaseUtils.getNewModelInstance();
	            _scModelUtils.setStringValueAtModelPath("PrimeLineNo",_scModelUtils.getStringValueFromPath("item.PrimeLineNo", args),blankModel);
       	     _scBaseUtils.setAttributeValue("argumentList", blankModel, eventDefn);
	            _scEventUtils.fireEventToParent(this, "extn_OpenGWPPopUp", eventDefn);
               }
},

extn_Quantity: function(gridReference, rowIndex, columnIndex, gridRowJSON, unformattedValue,value,gridRowRecord,colConfig)
{
var parentKey = _scBaseUtils.getValueFromPath("ParentOrderLineRelationships.OrderLineRelationship.ParentOrderLineKey", gridRowJSON.item);
           /* if(!(_scBaseUtils.isVoid(parentKey)))
            {
                 //_scModelUtils.setNumberValueAtModelPath("cellJson.cellValue",1, args);
                 editedQuantity = _scBaseUtils.getValueFromPath("cellJson.cellValue", args);
                 if(editedQuantity>1)
                 {
                  _scEventUtils.stopEvent(bEvent);
                  return true;
                 } 
           }*/
}
});
});
