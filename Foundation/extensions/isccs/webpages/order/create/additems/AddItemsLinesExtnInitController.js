


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/order/create/additems/AddItemsLinesExtn","scbase/loader!sc/plat/dojo/controller/ExtnScreenController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnAddItemsLinesExtn
			 ,
			    _scExtnScreenController
){

return _dojodeclare("extn.order.create.additems.AddItemsLinesExtnInitController", 
				[_scExtnScreenController], {

			
			 screenId : 			'extn.order.create.additems.AddItemsLinesExtn'

			
			
			
			
			
						,

			
			
			 mashupRefs : 	[
	 		{
		 sourceNamespace : 			'extn_getGiftBoxCommonCodeList_AddItemsLines'
,
		 mashupRefId : 			'extn_getGiftBoxCommonCode_AddItemsLines'
,
		 sequence : 			''
,
		 mashupId : 			'extn_getGiftBoxCommonCode_AddItemsLines'
,
		 callSequence : 			''
,
		 extnType : 			'ADD'
,
		 sourceBindingOptions : 			''

	}

	]

}
);
});

