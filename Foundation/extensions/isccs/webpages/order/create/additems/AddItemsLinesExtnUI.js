
scDefine(["dojo/text!./templates/AddItemsLinesExtn.html","scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/_base/lang","scbase/loader!dojo/text","scbase/loader!extn/utils/CustomUtils","scbase/loader!gridx/Grid","scbase/loader!sc/plat","scbase/loader!sc/plat/dojo/binding/GridxDataBinder","scbase/loader!sc/plat/dojo/utils/BaseUtils"]
 , function(			 
			    templateText
			 ,
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojolang
			 ,
			    _dojotext
			 ,
			    _extnCustomUtils
			 ,
			    _gridxGrid
			 ,
			    _scplat
			 ,
			    _scGridxDataBinder
			 ,
			    _scBaseUtils
){
return _dojodeclare("extn.order.create.additems.AddItemsLinesExtnUI",
				[], {
			templateString: templateText
	
	
	
	
	
	
					,	
	namespaces : {
		targetBindingNamespaces :
		[
		],
		sourceBindingNamespaces :
		[
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_9'
						,
	  description: 'UnitPrice_NS'
						,
	  value: 'extn_UnitPrice_NS'
						
			}
			,
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_10'
						,
	  description: 'extn_getGiftBoxCommonCodeList_AddItemsLines'
						,
	  value: 'extn_getGiftBoxCommonCodeList_AddItemsLines'
						
			}
			
		]
	}

	

	,
	hotKeys: [ 
	]

,events : [
	]

,subscribers : {
local : [

{
	  eventId: 'OLST_listGrid_Link_ScHandleLinkClicked'

,	  sequence: '19'

,	  description: 'GWP click on OrderLine'



,handler : {
methodName : "extn_GWPLink"

 
}
}
,
{
	  eventId: 'OLST_listGrid_afterPagingload'

,	  sequence: '19'

,	  description: 'extn_DisableGiftBox'



,handler : {
methodName : "extn_DisableGiftBox"

 
}
}
,
{
	  eventId: 'OLST_listGrid_ScOnApplyCellEdit'

,	  sequence: '19'

,	  description: 'extn_Quantity'



,handler : {
methodName : "extn_Quantity"

 
}
}
,
{
	  eventId: 'extn_PriceValue'

,	  sequence: '51'

,	  description: 'PriceValueMethod'



,handler : {
methodName : "extn_PriceValueMethod"

 
}
}
,
{
	  eventId: 'OLST_listGrid_ScDoubleClick'

,	  sequence: '51'




,handler : {
methodName : "extn_Quantity"

 
}
}

]
					 	 ,
}

});
});


