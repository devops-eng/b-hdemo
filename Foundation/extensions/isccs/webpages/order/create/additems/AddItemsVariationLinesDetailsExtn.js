scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/order/create/additems/AddItemsVariationLinesDetailsExtnUI", "scbase/loader!isccs/utils/ContextUtils",
 "scbase/loader!isccs/utils/UIUtils","scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils","scbase/loader!sc/plat/dojo/utils/WidgetUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnAddItemsVariationLinesDetailsExtnUI,
 			_isccsContextUtils,  
			_isccsUIUtils,    
			  _scBaseUtils,  _scModelUtils,  _scWidgetUtils
    
){ 
	return _dojodeclare("extn.order.create.additems.AddItemsVariationLinesDetailsExtn", [_extnAddItemsVariationLinesDetailsExtnUI],{
	// custom code here

getItemLocation: function(
        screen, widget, nameSpace, orderlineModel, options, dataValue) {
            var isModel = "";
            var retVal = "";
            var displayImage = "";
            displayImage = _isccsContextUtils.getDisplayItemImageRule();
            if (
            _scBaseUtils.equals(
            displayImage, "N")) {
                var retValue = "";
                retValue = _isccsUIUtils.fetchBlankImage(
                this);
                _scWidgetUtils.hideWidget(
                this, "moreImagesLink", true);
                return retValue;
            }
            isModel = _scModelUtils.getStringValueFromPath("ItemDetails.PrimaryInformation.IsModelItem", orderlineModel);
            if (
            _scBaseUtils.equals(
            isModel, "Y")) {
                retVal = _scModelUtils.getStringValueFromPath("ItemDetails.PrimaryInformation.ImageLocation", orderlineModel);
                return retVal;
            }
            var model = null;
            var length = 0;
            var hasMoreImages = false;
            model = _scModelUtils.getModelListFromPath("ItemDetails.AssetList.Asset", orderlineModel);
            length = _scBaseUtils.getAttributeCount(
            model);
            for (
            var index = 0;
            _scBaseUtils.lessThan(
            index, length);
            index = _scBaseUtils.incrementNumber(
            index, 1)) {
                var assetElem = null;
                var assetType = null;
                if (!(
                _scBaseUtils.isVoid(
                model))) {
                    assetElem = _scModelUtils.getModelFromList(
                    model, index);
                    assetType = _scModelUtils.getStringValueFromPath("Type", assetElem);
                    if (
                    _scBaseUtils.equals(
                    assetType, "ITEM_IMAGE_1")) {
                        retVal = _scModelUtils.getStringValueFromPath("ContentLocation", assetElem);
                    } else if (
                    _scBaseUtils.equals(
                    assetType, "ITEM_IMAGE_LRG_1")) {
                        _scWidgetUtils.showWidget(
                        this, "moreImagesLink", true, null);
                        hasMoreImages = true;
                    }
                }
            }
            if (
            _scBaseUtils.equals(
            hasMoreImages, false)) {
                _scWidgetUtils.hideWidget(
                this, "moreImagesLink", true);
            }
          retVal = _scModelUtils.getStringValueFromPath("ItemDetails.PrimaryInformation.ImageLocation", orderlineModel);
            return retVal;
        },
        getImageID: function(
        screen, widget, nameSpace, orderlineModel, options, dataValue) {
            var displayImage = "";
            displayImage = _isccsContextUtils.getDisplayItemImageRule();
            if (
            _scBaseUtils.equals(
            displayImage, "N")) {
                var retValue = "";
                return retValue;
            }
            var isModel = "";
            var retVal = "";
            isModel = _scModelUtils.getStringValueFromPath("ItemDetails.PrimaryInformation.IsModelItem", orderlineModel);
            if (
            _scBaseUtils.equals(
            isModel, "Y")) {
                retVal = _scModelUtils.getStringValueFromPath("ItemDetails.PrimaryInformation.ImageID", orderlineModel);
                return retVal;
            }
            var model = null;
            var length = 0;
            model = _scModelUtils.getModelListFromPath("ItemDetails.AssetList.Asset", orderlineModel);
            length = _scBaseUtils.getAttributeCount(
            model);
            for (
            var index = 0;
            _scBaseUtils.lessThan(
            index, length);
            index = _scBaseUtils.incrementNumber(
            index, 1)) {
                var assetElem = null;
                var assetType = null;
                if (!(
                _scBaseUtils.isVoid(
                model))) {
                    assetElem = _scModelUtils.getModelFromList(
                    model, index);
                    assetType = _scModelUtils.getStringValueFromPath("Type", assetElem);
                    if (
                    _scBaseUtils.equals(
                    assetType, "ITEM_IMAGE_1")) {
                        retVal = _scModelUtils.getStringValueFromPath("ContentID", assetElem);
                    }
                }
            }
                retVal = _scModelUtils.getStringValueFromPath("ItemDetails.PrimaryInformation.ImageID", orderlineModel);

            return retVal;
        }
});
});

