


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/order/create/additems/AddItemsVariationLinesDetailsExtn","scbase/loader!sc/plat/dojo/controller/ExtnScreenController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnAddItemsVariationLinesDetailsExtn
			 ,
			    _scExtnScreenController
){

return _dojodeclare("extn.order.create.additems.AddItemsVariationLinesDetailsExtnInitController", 
				[_scExtnScreenController], {

			
			 screenId : 			'extn.order.create.additems.AddItemsVariationLinesDetailsExtn'

			
			
			
}
);
});

