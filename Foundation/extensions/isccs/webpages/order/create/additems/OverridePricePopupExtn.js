scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/order/create/additems/OverridePricePopupExtnUI","scbase/loader!sc/plat/dojo/utils/PlatformUIFmkImplUtils","scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!sc/plat/dojo/utils/ModelUtils","scbase/loader!isccs/utils/BaseTemplateUtils","scbase/loader!sc/plat/dojo/utils/EventUtils","scbase/loader!isccs/utils/UIUtils","scbase/loader!sc/plat/dojo/utils/ScreenUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnOverridePricePopupExtnUI
                      ,
                         _scPlatformUIFmkUtils
                      ,
                         _scBaseUtils
                      ,
                         _scModelUtils
		 	 ,
			   _isccsBaseTemplateUtils
			 ,
			   _scEventUtils
                      ,
                        _isccsUIUtils
                      ,
                       _scScreenUtils
){ 
	return _dojodeclare("extn.order.create.additems.OverridePricePopupExtn", [_extnOverridePricePopupExtnUI],{
	// custom code here

     extn_OverridePriceValidation:function(event, bEvent, ctrl, args)
	{
    var userName = _scPlatformUIFmkUtils.getUserName();
     var userId = _scPlatformUIFmkUtils.getUserId();
	var overridePriceModel = _scBaseUtils.getTargetModel(this,"NewPriceModel");
    var price= _scModelUtils.getStringValueFromPath("LinePriceInfo.DisplayUnitPrice",overridePriceModel);
	if(price==0)
    {
                var message = null;
                message = _scScreenUtils.getString(this,"extn_OverridePrice_Validation");
                _isccsBaseTemplateUtils.showMessage(this,message,"error",null);
       	//_isccsBaseTemplateUtils.showMessage(this,"Override Price cannot be zero","error",null);
		_scEventUtils.stopEvent(bEvent);
    }
	else
	{
	_isccsBaseTemplateUtils.closeMessagePanel(event, bEvent, ctrl, args);   
    var userModel= _scBaseUtils.getNewModelInstance();
    _scModelUtils.setStringValueAtModelPath("User.Loginid",userId,userModel);
    _isccsUIUtils.callApi(this,userModel,"extn_getUserGrpMashUp",null);
    _scEventUtils.stopEvent(bEvent);
	}
	},
 extn_overrideCalculation:function(event, bEvent, ctrl, args)
     {
     var output = args.mashupArray;
     var discount = 0.0;
     var maxDiscount = 0.0;
     var maxOverridenPrice=0.0;
     var originalPriceModel = _scScreenUtils.getModel(this,"currentPriceModel");
     var originalPrice= _scModelUtils.getStringValueFromPath("LinePriceInfo.DisplayUnitPrice",originalPriceModel);
     var overridePriceModel = _scBaseUtils.getTargetModel(this,"NewPriceModel");
     var overriddenPrice= _scModelUtils.getStringValueFromPath("LinePriceInfo.DisplayUnitPrice",overridePriceModel);
     var percentages = _scScreenUtils.getModel(this,"extn_getPrcnt_ns");
     var orderModel =  _scScreenUtils.getModel(this,"getCompleteOrderDetails_output");
     var orderOrganizationCode =_scModelUtils.getStringValueFromPath("Order.EnterpriseCode",orderModel);
     var commonCodes = percentages.CommonCodeList.CommonCode;
     for(var index in output)
     {
        if(output[index].mashupRefId =="extn_getUserGrpMashUp")
        {
            var user = output[index].mashupRefOutput;
            
            var userOrganizationCode = user.User.OrganizationKey;
            var usergrpList = user.User.UserGroupLists.UserGroupList;
            for(var index1 in usergrpList)
            {
                var userLists = usergrpList[index1].UserGroup;
                var userGroupId = userLists[0].UsergroupId;
                for(var i in commonCodes)
                {
                   
                  if((commonCodes[i].CodeValue == userGroupId) && (commonCodes[i].OrganizationCode == userOrganizationCode))
                  {
                     var itemDisc= commonCodes[i].CodeShortDescription;
                     discount= (itemDisc/100)*originalPrice;
			maxOverridenPrice = originalPrice-discount;
                     if(maxOverridenPrice<=maxDiscount)
                     {
                         maxDiscount=maxOverridenPrice;
                     }
                     
                  }
                }
            }
        }
     }
     if(maxDiscount> overriddenPrice)
     {
	      var maxdisc = maxDiscount.toString();
              var message = null;
              message = _scScreenUtils.getString(this,"extn_MaxOverridePrice");
              var msg = message +maxdisc;

	      //var msg = "Maximum Discounted Price is " +maxdisc;
	      _scScreenUtils.showInfoMessageBox(this,msg,null,null);
     }
      else
	 {
	 this.onPopupConfirm();
	 }
	 }

});
});

