


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/order/create/additems/OverridePricePopupExtn","scbase/loader!sc/plat/dojo/controller/ExtnScreenController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnOverridePricePopupExtn
			 ,
			    _scExtnScreenController
){

return _dojodeclare("extn.order.create.additems.OverridePricePopupExtnInitController", 
				[_scExtnScreenController], {

			
			 screenId : 			'extn.order.create.additems.OverridePricePopupExtn'

			
			
			
			
			
						,

			
			
			 mashupRefs : 	[
	 		{
		 sourceNamespace : 			'extn_getPrcnt_ns'
,
		 mashupRefId : 			'extn_getPrctnMashup'
,
		 sequence : 			''
,
		 mashupId : 			'extn_getPrctnMashup'
,
		 callSequence : 			''
,
		 extnType : 			'ADD'
,
		 sourceBindingOptions : 			''

	}

	]

}
);
});

