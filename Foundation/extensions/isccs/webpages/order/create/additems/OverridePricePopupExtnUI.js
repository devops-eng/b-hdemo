
scDefine(["dojo/text!./templates/OverridePricePopupExtn.html","scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/_base/lang","scbase/loader!dojo/text","scbase/loader!sc/plat","scbase/loader!sc/plat/dojo/utils/BaseUtils"]
 , function(			 
			    templateText
			 ,
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojolang
			 ,
			    _dojotext
			 ,
			    _scplat
			 ,
			    _scBaseUtils
){
return _dojodeclare("extn.order.create.additems.OverridePricePopupExtnUI",
				[], {
			templateString: templateText
	
	
	
	
	
	
					,	
	namespaces : {
		targetBindingNamespaces :
		[
		],
		sourceBindingNamespaces :
		[
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_6'
						,
	  description: 'extn_getPrcnt_ns'
						,
	  value: 'extn_getPrcnt_ns'
						
			}
			
		]
	}

	

	,
	hotKeys: [ 
	]

,events : [
	]

,subscribers : {
local : [

{
	  eventId: 'Popup_btnNext_onClick'

,	  sequence: '19'

,	  description: 'extn_OverridePriceValidation'



,handler : {
methodName : "extn_OverridePriceValidation"

 
}
}
,
{
	  eventId: 'onExtnMashupCompletion'

,	  sequence: '51'

,	  description: 'extn_overrideCalculation'



,handler : {
methodName : "extn_overrideCalculation"

 
}
}

]
					 	 ,
}

});
});


