scDefine(["scbase/loader!dojo/_base/declare",
"scbase/loader!extn/order/create/address/AddressEntryExtnUI",
"scbase/loader!sc/plat/dojo/utils/EventUtils",
"scbase/loader!sc/plat/dojo/utils/ScreenUtils",
"scbase/loader!sc/plat/dojo/utils/WizardUtils",
"scbase/loader!sc/plat/dojo/utils/BaseUtils",
"scbase/loader!isccs/utils/UIUtils",
"scbase/loader!sc/plat/dojo/utils/ControllerUtils",
"scbase/loader!sc/plat/dojo/utils/EditorUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnAddressEntryExtnUI
				,
				_scEventUtils,
				_scScreenUtils,
				_scWizardUtils,
				_scBaseUtils,
				_isccsUIUtils,
				_scControllerUtils,
				_scEditorUtils

){ 
	return _dojodeclare("extn.order.create.address.AddressEntryExtn", [_extnAddressEntryExtnUI],{
	// custom code here
	afterScreenLoadExtn : function(event, bEvent, ctrl, args){

	bEventGlobal=bEvent;
	eventGlobal=event;
	ctrlGlobal=ctrl;
	argsGlobal=args;

	},
	
	 handleMashupOutput: function(
        mashupRefId, modelOutput, mashupInput, mashupContext, applySetModel) {
            if (
            _scBaseUtils.equals(
            mashupRefId, "modifyFulfillmentOptions")) {
                this.handleModifyFulfillmentOptions(
                modelOutput);
            }
            if (
            _scBaseUtils.equals(
            mashupRefId, "getCompleteOrderDetails")) {
                _scScreenUtils.setModel(
                this, "getCompleteOrderDetails_output", modelOutput, null);
				 var  ownerScr = _isccsUIUtils.getWizardForScreen(this);
				 if(ownerScr.SkipAddressEntryExtn=="YES"){
					ownerScr.SkipAddressEntryExtn="NO";
					ownerScr.ActionPerformed="NEXT";
				  }
				  else{
					ownerScr.ActionPerformed="SAVEONPREVIOUS";
				  }
				this.save(eventGlobal, bEventGlobal, ctrlGlobal, argsGlobal);
				
                
            }
        }

});
});

