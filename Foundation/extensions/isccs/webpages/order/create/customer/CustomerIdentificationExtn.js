scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/order/create/customer/CustomerIdentificationExtnUI","scbase/loader!sc/plat/dojo/utils/ModelUtils","scbase/loader!isccs/utils/UIUtils","scbase/loader!sc/plat/dojo/utils/EventUtils","scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!isccs/utils/BaseTemplateUtils","scbase/loader!sc/plat/dojo/utils/ScreenUtils","scbase/loader!sc/plat/dojo/utils/EditorUtils","scbase/loader!sc/plat/dojo/utils/WizardUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnCustomerIdentificationExtnUI
             ,
                _scModelUtils
             ,
             
                _isccsUIUtils
             ,
                _scEventUtils
             ,
              _scBaseUtils
             ,
               _isccsBaseTemplateUtils
             ,
              _scScreenUtils
			,
			 _scEditorUtils
			 ,
			  _scWizardUtils
              
){ 
	return _dojodeclare("extn.order.create.customer.CustomerIdentificationExtn", [_extnCustomerIdentificationExtnUI],{
	// custom code here
	
     extn_checkCountry:function( event, bEvent, ctrl, args) {
     var orderModel = _scScreenUtils.getModel(this, "PreviewData");
     var personShipToModel = _scModelUtils.getModelObjectFromPath("Order.PersonInfoShipTo", orderModel);
	 var country= _scModelUtils.getStringValueFromPath("Country",personShipToModel);
    if(!(_scBaseUtils.isVoid(personShipToModel)))
    {
    var inputData = _scScreenUtils.getInitialInputData(this);
    var enterpriseCode= _scModelUtils.getStringValueFromPath("Order.EnterpriseCode",inputData);
    if(enterpriseCode== "LT")
     {
	  var countryCode = "LTCountryCode";
      var apiInput = {
				CommonCode: {
						CodeType: countryCode,
						OrganizationCode: enterpriseCode
					}
			}
			_isccsUIUtils.callApi(this, apiInput, "extn_getCountryMashup"); 
           
     }
     else if(enterpriseCode== "BAY")
     {
	  var countryCode = "BayCountryCode";
      var apiInput = {
				CommonCode: {
						CodeType: countryCode,
						OrganizationCode: enterpriseCode
					}
			}
			_isccsUIUtils.callApi(this, apiInput, "extn_getCountryMashup");  
     }
      _scEventUtils.stopEvent(bEvent);
    }
	    else
    {
          var message = null;
          message = _scScreenUtils.getString(this,"ProvideShipToAddress");
          _isccsBaseTemplateUtils.showMessage(this,message,"error",null);

       // var message = "Please enter ship to address";
      //  _isccsBaseTemplateUtils.showMessage(this,message,"error",null);
        _scEventUtils.stopEvent(bEvent);

    }

	},
extn_shipToCountryValidation:function( event, bEvent, ctrl, args) {
       
    var orderModel =  _scScreenUtils.getModel(this, "PreviewData");
    var personShipToModel = _scModelUtils.getModelObjectFromPath("Order.PersonInfoShipTo", orderModel);  
    var country= _scModelUtils.getStringValueFromPath("Country",personShipToModel);
    output = args.mashupArray;
    for(var index in output)
    {
        if(output[index].mashupRefId == "extn_getCountryMashup")
        {
            var commonCodeOutput = output[index].mashupRefOutput.CommonCodeList.CommonCode;
            for(var i in commonCodeOutput)
            {
            var defaultCountry = commonCodeOutput[i].CodeShortDescription;
            if(defaultCountry!=country)
            {
                  var message = null;
                  var message1 = _scScreenUtils.getString(this,"extn_Order_ShipTO");
                  var message2 = _scScreenUtils.getString(this,"extn_for");
                  var message3 = _scScreenUtils.getString(this,"extn_EnterShipTO");
                  var message4 = _scScreenUtils.getString(this,"extn_ShippingAddress");
                  message = message1 +country +message2 +commonCodeOutput[i].OrganizationCode +message3 +defaultCountry +message4;
                  _isccsBaseTemplateUtils.showMessage(this,message,"error",null);
               // var message = "Order cannot be shipped to "+country+" for " +commonCodeOutput[i].OrganizationCode+". Please enter "+defaultCountry+" shipping Address";
		 //_isccsBaseTemplateUtils.showMessage(this,message,"error",null);
            }
			else
			{
			_isccsBaseTemplateUtils.closeMessagePanel(event, bEvent, ctrl, args);
			this.save();
			}
            }
        }
    }
	},
 handleTopPanel: function() {
            var skipCustomerModel = null;
            var sScreenType = null;
            skipCustomerModel = _isccsUIUtils.getWizardModel(
            this, "SkipCustomer_output");
            sScreenType = _scWizardUtils.getCurrentPageScreenType(
            _isccsUIUtils.getCurrentWizardInstance(
            _scEditorUtils.getCurrentEditor()));
            if (
            _scBaseUtils.equals(
            sScreenType, "giftRecepient")) {
                _scWidgetUtils.showWidget(
                this, "topContentPanel", false, null);
            } else if (
            _scBaseUtils.equals(
            this.checkIfCustomerPresentInInput(), true)) {
           //     _scWidgetUtils.hideWidget(this, "topContentPanel", true);
            } else if (
            _scBaseUtils.and(
            _scBaseUtils.equals(
            sScreenType, "customerIdentificationBeforePayment"), _scBaseUtils.equals(
            _scModelUtils.getStringValueFromPath("Order.Action", skipCustomerModel), "SkipCustomer"))) {
                _scWidgetUtils.hideWidget(
                this, "lnkNoConsumer", true);
	}
        }
});
});

