


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/order/create/customer/CustomerPreviewExtn","scbase/loader!sc/plat/dojo/controller/ExtnScreenController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnCustomerPreviewExtn
			 ,
			    _scExtnScreenController
){

return _dojodeclare("extn.order.create.customer.CustomerPreviewExtnInitController", 
				[_scExtnScreenController], {

			
			 screenId : 			'extn.order.create.customer.CustomerPreviewExtn'

			
			
			
}
);
});

