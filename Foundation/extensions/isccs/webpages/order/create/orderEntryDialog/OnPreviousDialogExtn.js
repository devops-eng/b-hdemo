scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/order/create/orderEntryDialog/OnPreviousDialogExtnUI","scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!sc/plat/dojo/utils/WidgetUtils","scbase/loader!sc/plat/dojo/utils/ModelUtils","scbase/loader!sc/plat/dojo/utils/ScreenUtils","scbase/loader!isccs/utils/UIUtils","scbase/loader!sc/plat/dojo/utils/EventUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnOnPreviousDialogExtnUI
                      ,
                         _scBaseUtils
                      ,
                         _scWidgetUtils
                      ,
                         _scModelUtils
                      ,
                          _scScreenUtils 
                      ,
                         _isccsUIUtils
                      ,
                         _scEventUtils
){         
	return _dojodeclare("extn.order.create.orderEntryDialog.OnPreviousDialogExtn", [_extnOnPreviousDialogExtnUI],{
	// custom code here
      extn_getScreenTitle:function(event, bEvent, ctrl, args)
	{
	var title= _scBaseUtils.getScreenTitle(this);
       var output=args.screen.popupInstance.popupParams.title;
      
         if(output=="Send Order EMail")
         {
           var emailId = args.screen.popupInstance.popupParams.emailId;
	   _scWidgetUtils.hideWidget(this,"confirmationMessage",null);
	   _scWidgetUtils.hideWidget(this,"saveChangesBeforePreviousMsg",null);
	   _scWidgetUtils.hideWidget(this,"Popup_btnNext",null);
	   _scWidgetUtils.hideWidget(this,"Popup_btnCancel",null);
	   _scWidgetUtils.showWidget(this,"extn_email",null);
	   _scWidgetUtils.hideWidget(this,"extn_cancelButton",null);
          _scWidgetUtils.showWidget(this,"extn_confirm",null);	
          var Model=  _scScreenUtils.getModel(this,"extn_getEmailID_ns");
          _scModelUtils.setStringValueAtModelPath("EMailId",emailId,Model) ;
          _scScreenUtils.setModel(this,"extn_getEmailID_ns",Model,null);
         }
 
	},

      extn_onConfirm:function(event, bEvent, ctrl, args)
	{	
         var inputModel = _scBaseUtils.getNewModelInstance();
	  var OrderDetails =args.screen.popupInstance.popupParams.orderDetails;
	  var emailId = _scBaseUtils.getTargetModel(this,"extn_email_ns",null);
         var OrderHeaderKey = _scModelUtils.getStringValueFromPath("Order.OrderHeaderKey",OrderDetails);
         var email =_scModelUtils.getStringValueFromPath("EmailID",emailId);
         var enterprise =_scModelUtils.getStringValueFromPath("Order.EnterpriseCode",OrderDetails);
        _scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey",OrderHeaderKey,inputModel);
        _scModelUtils.setStringValueAtModelPath("Order.To",email,inputModel);
        _scModelUtils.setStringValueAtModelPath("Order.EnterpriseCode",enterprise,inputModel);
        _isccsUIUtils.callApi(this, inputModel, "extn_sendEMail", null);
        _scEventUtils.stopEvent(bEvent);
	},

      extn_displayConfirmationMsg:function(event, bEvent, ctrl, args)
	{

       var output = args.mashupArray;
       var outputModel = null;
       for(var index in output)
        {
         if(output[index].mashupRefId=="extn_sendEMail")  
          {
            outputModel= output[index].mashupRefOutput;
          }
        }
       var msg = _scModelUtils.getStringValueFromPath("Order.Message",outputModel);
       _scScreenUtils.showInfoMessageBox(this, msg, "extn_response", null, null);
      },

    extn_response:function(actionPerformed, model, popupParams)
      {
	this.onPopupConfirm();
	}
});
});

