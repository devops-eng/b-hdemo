


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/order/create/orderEntryDialog/OnPreviousDialogExtn","scbase/loader!sc/plat/dojo/controller/ExtnServerDataController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnOnPreviousDialogExtn
			 ,
			    _scExtnServerDataController
){

return _dojodeclare("extn.order.create.orderEntryDialog.OnPreviousDialogExtnBehaviorController", 
				[_scExtnServerDataController], {

			
			 screenId : 			'extn.order.create.orderEntryDialog.OnPreviousDialogExtn'

			
			
			
			
			
						,

			
			
			 mashupRefs : 	[
	 		{
		 mashupRefId : 			'extn_sendEMail'
,
		 mashupId : 			'extn_sendEMail'
,
		 extnType : 			'ADD'

	}

	]

}
);
});

