
scDefine(["dojo/text!./templates/OnPreviousDialogExtn.html","scbase/loader!dijit/form/Button","scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/_base/lang","scbase/loader!dojo/text","scbase/loader!idx/form/TextBox","scbase/loader!sc/plat","scbase/loader!sc/plat/dojo/binding/ButtonDataBinder","scbase/loader!sc/plat/dojo/binding/CurrencyDataBinder","scbase/loader!sc/plat/dojo/binding/SimpleDataBinder","scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!sc/plat/dojo/widgets/Label"]
 , function(			 
			    templateText
			 ,
			    _dijitButton
			 ,
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojolang
			 ,
			    _dojotext
			 ,
			    _idxTextBox
			 ,
			    _scplat
			 ,
			    _scButtonDataBinder
			 ,
			    _scCurrencyDataBinder
			 ,
			    _scSimpleDataBinder
			 ,
			    _scBaseUtils
			 ,
			    _scLabel
){
return _dojodeclare("extn.order.create.orderEntryDialog.OnPreviousDialogExtnUI",
				[], {
			templateString: templateText
	
	
	
	
	
	
					,	
	namespaces : {
		targetBindingNamespaces :
		[
			{
	  scExtensibilityArrayItemId: 'extn_TargetNamespaces_0'
						,
	  description: 'extn_email_ns'
						,
	  value: 'extn_email_ns'
						
			}
			
		],
		sourceBindingNamespaces :
		[
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_1'
						,
	  description: 'extn_getEmailID_ns'
						,
	  value: 'extn_getEmailID_ns'
						
			}
			,
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_2'
						,
	  description: 'extn_getConfirmationMsg_ns'
						,
	  value: 'extn_getConfirmationMsg_ns'
						
			}
			
		]
	}

	

	,
	hotKeys: [ 
	]

,events : [
	]

,subscribers : {
local : [

{
	  eventId: 'afterScreenInit'

,	  sequence: '51'

,	  description: 'extn_getScreenTitle'



,handler : {
methodName : "extn_getScreenTitle"

 
 
}
}
,
{
	  eventId: 'extn_confirm_onClick'

,	  sequence: '51'

,	  description: 'extn_onConfirm'



,handler : {
methodName : "extn_onConfirm"

 
 
}
}
,
{
	  eventId: 'onExtnMashupCompletion'

,	  sequence: '51'

,	  description: 'extn_displayConfirmationMsg'



,handler : {
methodName : "extn_displayConfirmationMsg"

 
 
}
}

]
					 	 ,
}

});
});


