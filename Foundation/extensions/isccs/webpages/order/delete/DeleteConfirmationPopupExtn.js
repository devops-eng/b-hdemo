scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/order/delete/DeleteConfirmationPopupExtnUI","scbase/loader!isccs/utils/UIUtils", 
 "scbase/loader!sc/plat/dojo/utils/ScreenUtils","scbase/loader!sc/plat/dojo/utils/ModelUtils","scbase/loader!sc/plat/dojo/utils/BaseUtils",
 "scbase/loader!sc/plat/dojo/utils/WidgetUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnDeleteConfirmationPopupExtnUI
			    ,_isccsUIUtils
			    ,_scScreenUtils
			    ,_scModelUtils
			    ,_scBaseUtils
			    ,_scWidgetUtils
){ 
	return _dojodeclare("extn.order.delete.DeleteConfirmationPopupExtn", [_extnDeleteConfirmationPopupExtnUI],{
	// custom code here
extn_afterScreenInit:function(event,bevent,ctrl,args)
{
    var ownerScreen = null;
            ownerScreen = _isccsUIUtils.getParentScreen(
            this, false);
            var hideRelatedItemChkModel = null;
            hideRelatedItemChkModel = _scScreenUtils.getModel(
            ownerScreen, "HideRelatedChkBox");
            var relatedItemFlag = null;
            relatedItemFlag = _scModelUtils.getBooleanValueFromKey("HideRelatedChkBox", hideRelatedItemChkModel);
            if (
            _scBaseUtils.isBooleanTrue(
            relatedItemFlag)) {
                 
		_scWidgetUtils.hideWidget(
                this, "extn_deleteConfirmationMsgwithRelatedItems", false);

            } else {
                _scWidgetUtils.hideWidget(
                this, "deleteConfirmationMsgWithRelatedItem", false, null);
                _scWidgetUtils.hideWidget(
                this, "radDeleteType", false, null);
            }

},
getPopupOutput: function(
        event, bEvent, ctrl, args) {
            var deleteRelatedItemModel = null;
            deleteRelatedItemModel = _scBaseUtils.getTargetModel(
            this, "DeleteRelatedItems", null);
            if ((
            _scWidgetUtils.isWidgetVisible(
            this, "extn_deleteConfirmationMsgwithRelatedItems"))) {
                _scModelUtils.setStringValueAtModelPath("DeleteFlag", "Y", deleteRelatedItemModel);
            }
            
            return deleteRelatedItemModel;
        },
});
});

