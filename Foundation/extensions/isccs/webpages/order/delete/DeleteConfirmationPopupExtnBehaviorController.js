


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/order/delete/DeleteConfirmationPopupExtn","scbase/loader!sc/plat/dojo/controller/ExtnServerDataController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnDeleteConfirmationPopupExtn
			 ,
			    _scExtnServerDataController
){

return _dojodeclare("extn.order.delete.DeleteConfirmationPopupExtnBehaviorController", 
				[_scExtnServerDataController], {

			
			 screenId : 			'extn.order.delete.DeleteConfirmationPopupExtn'

			
			
			
}
);
});

