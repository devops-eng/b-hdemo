


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/order/delete/DeleteConfirmationPopupExtn","scbase/loader!sc/plat/dojo/controller/ExtnScreenController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnDeleteConfirmationPopupExtn
			 ,
			    _scExtnScreenController
){

return _dojodeclare("extn.order.delete.DeleteConfirmationPopupExtnInitController", 
				[_scExtnScreenController], {

			
			 screenId : 			'extn.order.delete.DeleteConfirmationPopupExtn'

			
			
			
}
);
});

