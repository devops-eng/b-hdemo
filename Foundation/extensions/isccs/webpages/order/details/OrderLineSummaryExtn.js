scDefine(["scbase/loader!dojo/_base/declare",
        "scbase/loader!extn/order/details/OrderLineSummaryExtnUI",
        "scbase/loader!sc/plat/dojo/utils/ScreenUtils",
        "scbase/loader!sc/plat/dojo/utils/ModelUtils",
        "scbase/loader!sc/plat/dojo/utils/BaseUtils",
        "scbase/loader!isccs/utils/UIUtils",
        "scbase/loader!sc/plat/dojo/utils/WidgetUtils",
        "scbase/loader!extn/expicient/utils/ExpicientUtil",
        "scbase/loader!sc/plat/dojo/utils/EventUtils",
        "scbase/loader!dojo/currency"
    ],
    function(
        _dojodeclare,
        _extnOrderLineSummaryExtnUI,
        _scScreenUtils,
        _scModelUtils,
        _scBaseUtils,
        _isccsUIUtils,
        _scWidgetUtils,
        _extnExpicientUtils,
        _scEventUtils,
        _dCurrency
    ) {
        return _dojodeclare("extn.order.details.OrderLineSummaryExtn", [_extnOrderLineSummaryExtnUI], {
            // custom code here
            extn_getPickDateTime: function(dataValue, screen, widget, nameSpace, orderLineModel, options) {
            	if(orderLineModel)
            	{
            	var pickDate=null;
            	pickDate=_scModelUtils.getStringValueFromPath("OrderLine.NewStatusDate", orderLineModel);
            	if(pickDate)

            	{
            		var newPickDate=new Date(pickDate);
            		var finalDate=newPickDate.toLocaleDateString()+" "+ newPickDate.toLocaleTimeString();
            		return finalDate; 
            	}
            	else{
            		return "";
            	} 
				
            	}
            	else{
            		return "";
            	} 
            },
            
            extn_getQty: function(dataValue, screen, widget, nameSpace, orderLineModel, options) {
                var itemID = null;
                var unitOfMeasure = null;
                var itemModel = null;
                if (!_scBaseUtils.isVoid(orderLineModel)) {
                    itemModel = _scModelUtils.getModelObjectFromPath("OrderLine.ItemDetails", orderLineModel);
                    if (!_scBaseUtils.isVoid(itemModel)) {
                        itemID = _scModelUtils.getStringValueFromPath("ItemID", itemModel);
                        unitOfMeasure = _scModelUtils.getStringValueFromPath("UnitOfMeasure", itemModel);
                        productClass = _scModelUtils.getStringValueFromPath("ProductClass", itemModel);
                    }



                    var shipNode = null;
                    shipNode = _scModelUtils.getStringValueFromPath("OrderLine.ShipNode", orderLineModel);
                    var organization = null;
                    organization = _scModelUtils.getStringValueFromPath("OrderLine.ItemDetails.OrganizationCode", orderLineModel);
                    var model = null;
                    model = _scBaseUtils.getNewModelInstance();
                    _scModelUtils.setStringValueAtModelPath("InventorySupply.OrganizationCode", organization, model);
                    _scModelUtils.setStringValueAtModelPath("InventorySupply.ShipNode", shipNode, model);
                    _scModelUtils.setStringValueAtModelPath("InventorySupply.ItemID", itemID, model);
                    _scModelUtils.setStringValueAtModelPath("InventorySupply.UnitOfMeasure", unitOfMeasure, model);
                    _scModelUtils.setStringValueAtModelPath("InventorySupply.ProductClass", productClass, model);
                    _scModelUtils.setStringValueAtModelPath("InventorySupply.SupplyType", "ONHAND", model);
                    _isccsUIUtils.callApi(this, model, "extn_getQtyOnHand_ref", null);


                }

            },

            extn_afterBehaviorMashupCall: function(event, bEvent, ctrl, args) {

                var isMashupAvailable = _extnExpicientUtils.isMashupAvailable("extn_getQtyOnHand_ref", args);
                if (isMashupAvailable) {
                    var availableQuantity = null;


                    var getItemListModelOutput = null;
                    getItemListModelOutput = _extnExpicientUtils.getMashupOutputForRefID("extn_getQtyOnHand_ref", args);
                    if (!_scBaseUtils.isVoid(getItemListModelOutput)) {
                        var itemModel = null;
                        itemModel = _scModelUtils.getModelObjectFromPath("Item.Supplies.InventorySupply", getItemListModelOutput);
                        var itemID = null;
                        if (!_scBaseUtils.isVoid(itemModel)) {

                            availableQuantity = _scModelUtils.getStringValueFromPath("Quantity", itemModel);
                            if (!_scBaseUtils.isVoid(availableQuantity)) {
                                _scWidgetUtils.setValue(this, "extn_qtyOnHand", parseInt(availableQuantity));
                                var model = null;
                                model = _scBaseUtils.getNewModelInstance();
                                _scModelUtils.setStringValueAtModelPath("OrderLine.QtyOnHand", availableQuantity, model);
                                _scBaseUtils.setModel(this, "qtyOnHandModel", model, null);

                            }



                        }

                    }


                }else{
					var extnMashupArray = _scModelUtils.getModelListFromPath("mashupArray",args);
					for(var mashUpSequence in extnMashupArray){
						var mashupObj = extnMashupArray[mashUpSequence];
						if(_scBaseUtils.stringEquals(mashupObj.mashupRefId,"extn_getOrderAuditList_ref")){
							var orderAuditsListObj = mashupObj.mashupRefOutput;
							var extnMashupInputData = args.inputData;
							var chargesAndDiscountsHTMLStr = "<tr><td align='left'><b>Charges / Discounts</b></td></tr>";
							var sellingPriceHTMLStr = "<td align='left'>Selling price:</td>";
							var taxHTMLStr = "<tr><td align='left'><b>Taxes</b></td></tr>";
							var trStartHTMLStr = "<tr  >";
							var trEndHTMLStr = "</tr>";
							var orderTotalHTMLStr = "<td align='left' class='orderTotalLine'>Total:</td>";
							var orderAuditArray = _scModelUtils.getModelListFromPath("OrderAuditList.OrderAudit",orderAuditsListObj);
							for(var auditSequence in orderAuditArray){
								var orderAuditObj = orderAuditArray[auditSequence];
								var orderAuditLevelArray = _scModelUtils.getModelListFromPath("OrderAuditLevels.OrderAuditLevel",orderAuditObj);
								for(var auditLevelSequence in orderAuditLevelArray){
									var orderAuditLevelObj = orderAuditLevelArray[auditLevelSequence];
									if(_scBaseUtils.stringEquals(orderAuditLevelObj.ModificationLevel,"ORDER_LINE")){
										   var orderLineKeyinput =null;
										for(var index in extnMashupInputData)
											{
												var mashupInputObject =extnMashupInputData[index].mashupInputObject;
												orderLineKeyinput =  _scModelUtils.getStringValueFromPath("OrderAudit.OrderLineKey", mashupInputObject);
											}
											var outputOrderLineKey = _scModelUtils.getStringValueFromPath("OrderLineKey",orderAuditLevelObj);
											if(orderLineKeyinput == outputOrderLineKey)
											{
										var orderAuditDetailsArray = _scModelUtils.getModelListFromPath("OrderAuditDetails.OrderAuditDetail",orderAuditLevelObj);
										for(var auditDetailsSequence in orderAuditDetailsArray){
											var eachAuditDetails = orderAuditDetailsArray[auditDetailsSequence];
											switch(eachAuditDetails.AuditType){
												case "OrderLine":
													var orderTotalArray = _scModelUtils.getModelListFromPath("Attributes.Attribute", eachAuditDetails)
													var orderTotal = this.extn_frameTableRowForInput(orderTotalArray,"LineTotal","OldValue");
													var orderQty = this.extn_frameTableRowForInput(orderTotalArray,"OrderedQty","OldValue");
													var sellingPrice = this.extn_subTotalAndTotal(orderQty);
													orderTotalHTMLStr = "<tr>" + orderTotalHTMLStr +"<td align='right' class='orderTotalLine'>" + this.extn_convertToCurrancyFormat(orderTotal) + " </td>" + trEndHTMLStr 
													sellingPriceHTMLStr = "<tr>" + sellingPriceHTMLStr + "<td align='right'>"+ sellingPrice +"</td></tr>";
													break;
												case "LineCharge":
													chargesAndDiscountsHTMLStr = chargesAndDiscountsHTMLStr + trStartHTMLStr + this.extn_chargesAndDiscounts(eachAuditDetails,"ChargeAmount") + trEndHTMLStr;
													break;
												case "LineTax":
													taxHTMLStr = taxHTMLStr + trStartHTMLStr + this.extn_chargesAndDiscounts(eachAuditDetails,"Tax") + trEndHTMLStr;
											}
										}	
									}
								}
							}
							}
							_scScreenUtils.showInfoMessageBox(this,"<div align='center'><table width='71%' class = 'contentSeparator'><tr><td class = 'dijitTitlePaneTextNode' align='left'>Pricing summary</td><td></td></tr></table><table class='accentPanel' style='margin-top:5px; border-spacing:0 5px;' width='70%'>" + sellingPriceHTMLStr +chargesAndDiscountsHTMLStr+ taxHTMLStr + orderTotalHTMLStr  + "</table></div>","OK",null);
						}
					}
                	
                	}

		

            	},

		extn_subTotalAndTotal: function(orderQty){
			var subTotalStr = "<tr><td><b>";
			var totalStr = "<tr><td><b>";
			var getCompleteOrderLineDetails_output = null;
                	getCompleteOrderLineDetails_output = _scScreenUtils.getModel(this, "getCompleteOrderLineDetails_output");
			var unitPrice = _scModelUtils.getStringValueFromPath("OrderLine.ComputedPrice.UnitPrice", getCompleteOrderLineDetails_output);
			return this.extn_convertToCurrancyFormat(orderQty*unitPrice);
		},
		extn_chargesAndDiscounts: function(auditLineDetails,chargeAmountType){
			var tdStartHTMLStr = "<td align='left'>";
            		var tdEndHTMLStr = "</td>";
            		var isDiscount ="";
			var chargeAndDiscountRowHTMLStr = "";
			var amountStr = "";

			var idsArray = _scModelUtils.getModelListFromPath("IDs.ID", auditLineDetails);
			if (!_scBaseUtils.isVoid(idsArray)) {
				isDiscount = this.extn_frameTableRowForInput(idsArray,"ChargeCategory","Value");
				var chargeTypeName = "";
				if(_scBaseUtils.stringEquals(chargeAmountType,"Tax")){
					chargeTypeName = "TaxName";
					chargeAndDiscountRowHTMLStr = tdStartHTMLStr + this.extn_frameTableRowForInput(idsArray,chargeTypeName,"Value")+":"+ tdEndHTMLStr;
				}else
				{
					chargeTypeName = "ChargeName";
					chargeAndDiscountRowHTMLStr = tdStartHTMLStr + _scScreenUtils.getString(this, "extn_"+this.extn_frameTableRowForInput(idsArray,chargeTypeName,"Value"))+":"+ tdEndHTMLStr;
				}
			}
			var attributeArray = _scModelUtils.getModelListFromPath("Attributes.Attribute", auditLineDetails);
			if (!_scBaseUtils.isVoid(idsArray)) {
				amountStr = this.extn_convertToCurrancyFormat(this.extn_frameTableRowForInput(attributeArray,chargeAmountType,"OldValue"));
				if(_scBaseUtils.stringEquals(isDiscount,"DISCOUNT")){
					amountStr = "(" + amountStr + ")";
				}
				chargeAndDiscountRowHTMLStr = chargeAndDiscountRowHTMLStr + "<td align='right'>" + amountStr + tdEndHTMLStr;
			}
			return chargeAndDiscountRowHTMLStr;
	
		},
		extn_frameTableRowForInput: function(attrArray,attrName,attrValue){
			for(var attrSequence in attrArray){
          		var attrObj = attrArray[attrSequence];
          		if(_scBaseUtils.stringEquals(_scModelUtils.getStringValueFromPath("Name", attrObj),attrName)){
          			return _scModelUtils.getStringValueFromPath(attrValue, attrObj);
          		}
			}
			return;
		},
		extn_convertToCurrancyFormat: function(amountStr){
			var extnInitialInputData = _scScreenUtils.getInitialInputData(this);
			var countryCode = _scModelUtils.getStringValueFromPath("Order.PriceInfo.Currency", extnInitialInputData);
			if(countryCode){
				return _dCurrency.format(amountStr,{currency:countryCode});
			}
			return amountStr;
		},
            extn_openItemDetails: function(event, bEvent, ctrl, args) {
                var sItemId = null;
                var sUom = null;
                var orderLine = null;
                var sOrgCode = null;
                var itemDetailsInput = null;
                var getCompleteOrderLineDetails_output = null;
                getCompleteOrderLineDetails_output = _scScreenUtils.getModel(
                    this, "getCompleteOrderLineDetails_output");
                var qtyOnHandModel = null;
                qtyOnHandModel = _scScreenUtils.getModel(
                    this, "qtyOnHandModel");
                sOrgCode = _scModelUtils.getStringValueFromPath("OrderLine.Order.EnterpriseCode", getCompleteOrderLineDetails_output);
                sItemId = _scModelUtils.getStringValueFromPath("OrderLine.ItemDetails.ItemID", getCompleteOrderLineDetails_output);
                sUom = _scModelUtils.getStringValueFromPath("OrderLine.ItemDetails.UnitOfMeasure", getCompleteOrderLineDetails_output);
                qtyOnHand = _scModelUtils.getStringValueFromPath("OrderLine.QtyOnHand", qtyOnHandModel);
                itemDetailsInput = _scModelUtils.createNewModelObjectWithRootKey("Item");
                _scModelUtils.setStringValueAtModelPath("Item.ItemID", sItemId, itemDetailsInput);
                _scModelUtils.setStringValueAtModelPath("Item.UnitOfMeasure", sUom, itemDetailsInput);
                _scModelUtils.setStringValueAtModelPath("Item.QtyOnHand", qtyOnHand, itemDetailsInput);
                _scModelUtils.setStringValueAtModelPath("Item.CallingOrganizationCode", sOrgCode, itemDetailsInput);
                _scModelUtils.setStringValueAtModelPath("Item.CustomerInformation.BuyerUserId", _scModelUtils.getStringValueFromPath("OrderLine.Order.BuyerUserId", getCompleteOrderLineDetails_output), itemDetailsInput);
                if (
                    _scBaseUtils.equals("Y", _scModelUtils.getStringValueFromPath("Rules.RuleSetValue", _scScreenUtils.getModel(
                        this, "storefrontGetRuleDetails_output")))) {
                    var clonedModel = null;
                    clonedModel = _scBaseUtils.cloneModel(
                        itemDetailsInput);
                    _scModelUtils.setStringValueAtModelPath("Item.Storefront.UrlType", "WCCProductDisplayURL", clonedModel);
                    _scModelUtils.setStringValueAtModelPath("Item.Storefront.IsStatic", "Y", clonedModel);
                    _isccsUIUtils.openWizardInEditor("isccs.storefront.wizards.storefrontItemDetails.StorefrontItemDetailsWizard", clonedModel, "isccs.editors.StorefrontItemEditor", this);
                } else {
                    _isccsUIUtils.openWizardInEditor("isccs.item.wizards.itemDetails.ItemDetailsWizard", itemDetailsInput, "isccs.editors.ItemEditor", this);
                }
                _scEventUtils.stopEvent(bEvent);

            },

		extnOpenCancelledDetails: function(event, bEvent, ctrl, args) {
			var popupParams = null;
			var dialogParams = null;
			
		var extnInitialInputData = _scScreenUtils.getInitialInputData(this);
			var model = null;
                     model = _scBaseUtils.getNewBeanInstance();
     		      	_scModelUtils.setStringValueAtModelPath("OrderAudit.ModificationType", "CANCEL", model);
			_scModelUtils.setStringValueAtModelPath("OrderAudit.OrderLineKey", _scModelUtils.getStringValueFromPath("Order.OrderLineKey", extnInitialInputData), model);
			
			//For Test, hardcoding OrderLineKey
			//_scModelUtils.setStringValueAtModelPath("OrderAudit.OrderLineKey","201512300223344935124" , model);
						
			_scModelUtils.setStringValueAtModelPath("OrderAudit.EnterpriseCode", _scModelUtils.getStringValueFromPath("Order.EnterpriseCode", extnInitialInputData), model);
                    	_isccsUIUtils.callApi(this, model, "extn_getOrderAuditList_ref", null);

			
		},

      		extn_getAssociateDetails:function (gridReference, rowIndex, columnIndex, gridRowJSON, unformattedValue, value, gridRowRecord, colConfig){
			var associateId = _scModelUtils.getStringValueFromPath("AssociateId", gridRowJSON);
			if (_scBaseUtils.equals("NA",associateId)){
				var associateName = _scModelUtils.getStringValueFromPath("AssociateName", gridRowJSON);
				associateId = associateName; 
			}
			return associateId;

		},
        extn_afterScreenInit: function(event, bEvent, ctrl, args){
             var orderLineModel = _scScreenUtils.getModel(this, "getCompleteOrderLineDetails_output");
             var personInfoShipTo= _scModelUtils.getStringValueFromPath("OrderLine.PersonInfoShipTo", orderLineModel);
             if(!_scBaseUtils.isVoid(personInfoShipTo)){
                var name= null;
                var firstName= _scModelUtils.getStringValueFromPath("FirstName", personInfoShipTo);
                var lastName= _scModelUtils.getStringValueFromPath("LastName", personInfoShipTo);
                name= firstName + " "+ lastName;
                _scWidgetUtils.setValue(this, "extn_pickupPerson", name)
             }
             var personInfoMarkFor= _scModelUtils.getStringValueFromPath("OrderLine.PersonInfoMarkFor", orderLineModel);
             if(!_scBaseUtils.isVoid(personInfoMarkFor)){
                var name= null;
                var firstName= _scModelUtils.getStringValueFromPath("FirstName", personInfoShipTo);
                var lastName= _scModelUtils.getStringValueFromPath("LastName", personInfoShipTo);
                name= firstName + " "+ lastName;
                _scWidgetUtils.setValue(this, "extn_alternatePerson", name)
             }

        }
	});
    });
