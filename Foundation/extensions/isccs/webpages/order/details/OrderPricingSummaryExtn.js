scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/order/details/OrderPricingSummaryExtnUI", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!isccs/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!isccs/utils/SharedComponentUtils", "scbase/loader!isccs/utils/UIUtils", "scbase/loader!isccs/utils/ModelUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!extn/expicient/utils/ExpicientUtil"], function(_dojodeclare, _extnOrderPricingSummaryExtnUI, _scBaseUtils, _scModelUtils, _isccsBaseTemplateUtils, _scScreenUtils, _isccsSharedComponentUtils, _isccsUIUtils, _isccsModelUtils, _scWidgetUtils, _scUserprefs, _extnExpicientUtils) {
    return _dojodeclare("extn.order.details.OrderPricingSummaryExtn", [_extnOrderPricingSummaryExtnUI], {
        // custom code here
        extn_afterScreenInit: function(event, bEvent, ctrl, args) {
            var model = null;
            model = _scScreenUtils.getModel(this, "getCompleteOrderDetails_output");
            var promotions = _scModelUtils.getModelListFromPath("Order.Promotions.Promotion", model);
            if (!_scBaseUtils.isVoid(promotions)) {
                for (var i = 0; i < promotions.length; i++) {
                    var referenceModel = null;
                    referenceModel = _scBaseUtils.getArrayItemByIndex(promotions, i);
                    if (!_scBaseUtils.isVoid(referenceModel)) {
                        var promotionID = null;
                        var promotionAmount = null;
                        promotionID = _scModelUtils.getStringValueFromPath("PromotionId", referenceModel);
                        // promotionAmount=_scModelUtils.getStringValueFromPath("PromotionAmount",referenceModel);
                        var dataLabelUID = null;
                        dataLabelUID = "dynamicDataLabel" + i;
                        var datUID = null;
                        datUID = _scWidgetUtils.createDataLabel(null, this, dataLabelUID, null);
                        _scWidgetUtils.placeAt(this, "extn_diffPromotionPanel", datUID, "after");
                        var datLabel = null;
                        datLabel = "Promotion";
                        _scWidgetUtils.setLabel(this, datUID, datLabel);
                        _scWidgetUtils.setValue(this, datUID, promotionID, false);
                    }
                }
            }
            else{
            	_scWidgetUtils.addClass(this,"extn_noPromotions","labelMessage");
            	_scWidgetUtils.setValue(this, "extn_noPromotions", "There are no promotions", false);
            }
        }
    });
});
