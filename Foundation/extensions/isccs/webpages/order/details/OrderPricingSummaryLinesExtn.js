
scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/order/details/OrderPricingSummaryLinesExtnUI","scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/GridxUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!sc/plat/dojo/utils/PaginationUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/widgets/Screen"]
,
function(			 
			    _dojodeclare
			 , _extnOrderPricingSummaryLinesExtnUI
			 ,  _scBaseUtils
			 , _scEventUtils
			 , _scGridxUtils
			 , _scModelUtils
			 , _scPaginationUtils
			 , _scScreenUtils
			 , _scWidgetUtils
			 , _scScreen
){ 
	return _dojodeclare("extn.order.details.OrderPricingSummaryLinesExtn", [_extnOrderPricingSummaryLinesExtnUI],{
	// custom code here

	
	customOrderListHandler: function(modelOutput) {
            var orderModel = null;
            orderModel = _scScreenUtils.getInitialInputData(
            this);
            var orderLineKey = null;
            orderLineKey = _scModelUtils.getStringValueFromPath("Order.OrderLineKey", orderModel);
            if (!(
            _scBaseUtils.isVoid(
            orderLineKey))) {
                _scGridxUtils.deselectAllRowsInGridUsingUId(
                this, "OLST_listGrid");
                _scGridxUtils.selectUniqueRowUsingHiddenColumn(
                this, "OLST_listGrid", orderLineKey);
            }

            var parentParent= this.getOwnerScreen().getOwnerScreen();
            if(!_scBaseUtils.isVoid(parentParent))
            {
            	if(_scBaseUtils.equals(parentParent.className,"OrderSummaryLines")){

            		var i=null;
            	i=_scModelUtils.getStringValueFromPath("currentRowIndex",parentParent);
            	if(!_scBaseUtils.isVoid(i)){
            		 _scGridxUtils.deselectAllRowsInGridUsingUId(
                this, "OLST_listGrid");
            		 _scGridxUtils.selectRowUsingUId(
                    this, "OLST_listGrid", i);
            	}
            	}
            	
            }
			
           
        }
});
});

