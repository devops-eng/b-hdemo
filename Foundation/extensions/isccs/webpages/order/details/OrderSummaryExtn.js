scDefine(["scbase/loader!dojo/_base/declare",
        "scbase/loader!extn/order/details/OrderSummaryExtnUI",
        "scbase/loader!sc/plat/dojo/utils/ScreenUtils",
        "scbase/loader!sc/plat/dojo/utils/ModelUtils",
        "scbase/loader!sc/plat/dojo/utils/BaseUtils",
        "scbase/loader!isccs/utils/UIUtils",
        "scbase/loader!sc/plat/dojo/utils/WidgetUtils",
        "scbase/loader!extn/expicient/utils/ExpicientUtil"
    ],
    function(
        _dojodeclare,
        _extnOrderSummaryExtnUI,
        _scScreenUtils,
        _scModelUtils,
        _scBaseUtils,
        _isccsUIUtils,
        _scWidgetUtils,
        _extnExpicientUtils
    ) {
        return _dojodeclare("extn.order.details.OrderSummaryExtn", [_extnOrderSummaryExtnUI], {
            // custom code here


            extn_beforeInitialize: function(event, bEvent, ctrl, args) {

                var orderModel = null;
                orderModel = _scScreenUtils.getModel(this, "getCompleteOrderDetails_output");
                var entryType = null;
                entryType = _scModelUtils.getStringValueFromPath("Order.EntryType", orderModel);
                if (_scBaseUtils.equals(entryType, "POS")) {
                    _scWidgetUtils.hideWidget(this, "extn_storeNo", false);
                    _scWidgetUtils.hideWidget(this, "extn_storePhone", false);

                } else {
                    _scWidgetUtils.showWidget(this, "extn_storeNo", false);
                    _scWidgetUtils.showWidget(this, "extn_storePhone", false);
                }
            },
            extn_CustomerLanguageDisplay: function(event, bEvent, ctrl, args) {

                var orderModel = _scScreenUtils.getModel(this, "getCompleteOrderDetails_output");
          /*      var value = null;
                var shopRunnerOrder = _scModelUtils.getStringValueFromPath("Order.Extn.ExtnShopRnrAuthTok", orderModel);
                if (!(_scBaseUtils.isVoid(shopRunnerOrder))) {
                    value = _scScreenUtils.getString(this, "Action_textOK");
                    _scWidgetUtils.setValue(this, "extn_shopRunnerOrder", value, false);
                } else {
                    value = _scScreenUtils.getString(this, "Action_textCANCEL");
                    _scWidgetUtils.setValue(this, "extn_shopRunnerOrder", value, false);
                }
            */
                var customerID = _scModelUtils.getStringValueFromPath("Order.BillToID", orderModel);
                var organizationCode = _scModelUtils.getStringValueFromPath("Order.EnterpriseCode", orderModel);
                var inputModel = _scBaseUtils.getNewModelInstance();
                if (!_scBaseUtils.isVoid(customerID)) {
                    _scModelUtils.setStringValueAtModelPath("Customer.CustomerID", customerID, inputModel);
                    _scModelUtils.setStringValueAtModelPath("Customer.OrganizationCode", organizationCode, inputModel);
                    _isccsUIUtils.callApi(this, inputModel, "extn_customerLanguageMashup", null);
                }

            },

            extn_LanguageMashupCompletion: function(event, bEvent, ctrl, args) {
                var output = args.mashupArray;
                for (var index in output) {
                    if (output[index].mashupRefId == "extn_customerLanguageMashup") {
                        var LanguageModel = output[index].mashupRefOutput.Customer.CustomerContactList.CustomerContact;
                        for (var i in LanguageModel) {
                            var language = LanguageModel[i].Extn.ExtnPreferredLanguage;
                            _scWidgetUtils.setValue(this, "extn_Language", language, false);
                        }

                    }
                }

            },
            extn_getStorePhone: function(dataValue, screen, widget, nameSpace, orderSummaryModel, options) {
              if (!_scBaseUtils.isVoid(orderSummaryModel)) {
                    var organization=null;
                    organization = _scModelUtils.getModelObjectFromPath("Order.SellerOrganizationCode", orderSummaryModel);
                     var model = null;
                    model = _scBaseUtils.getNewModelInstance();
                    _scModelUtils.setStringValueAtModelPath("Organization.OrganizationCode", organization, model);
                     _isccsUIUtils.callApi(this, model, "extn_getOrganizationHierarchy_ref", null);
                  }

            },

		extn_checkShopRunner:  function(dataValue, screen, widget, nameSpace, orderSummaryModel, options) {
             if (!_scBaseUtils.isVoid(orderSummaryModel)) {
                var promotionList = _scModelUtils.getModelListFromPath("Order.Promotions.Promotion",orderSummaryModel);
                if (!_scBaseUtils.isVoid(promotionList)) {
                    for(var count in promotionList){
                        var promotionID = _scModelUtils.getStringValueFromPath("PromotionId", promotionList[count]);
                        if(_scBaseUtils.stringEquals(promotionID,"SRUNNER")){
                            return _scScreenUtils.getString(this, "Action_textOK");
                        }
                    }
                }
             }
			 return _scScreenUtils.getString(this, "Action_textCANCEL");
		},

            extn_afterBehaviorMashupCall: function(event, bEvent, ctrl, args) {

                var isMashupAvailable = _extnExpicientUtils.isMashupAvailable("extn_getOrganizationHierarchy_ref", args);
                if (isMashupAvailable) {


                    var getOrderModelOutput = null;
                    getOrderModelOutput = _extnExpicientUtils.getMashupOutputForRefID("extn_getOrganizationHierarchy_ref", args);
                    if (!_scBaseUtils.isVoid(getOrderModelOutput)) {
                        var phone = null;
                        phone = _scModelUtils.getStringValueFromPath("Organization.CorporatePersonInfo.DayPhone", getOrderModelOutput);
                        if(!_scBaseUtils.isVoid(phone)){
                          _scWidgetUtils.setValue(this, "extn_storePhone",phone);
                        }
                        var itemID = null;
                    

                    }

                }

            }

        });
    });
