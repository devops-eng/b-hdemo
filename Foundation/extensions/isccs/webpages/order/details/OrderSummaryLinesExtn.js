scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/order/details/OrderSummaryLinesExtnUI", "scbase/loader!sc/plat/dojo/utils/GridxUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!isccs/utils/OrderLineUtils", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!isccs/utils/UIUtils", "scbase/loader!dojo/currency", "scbase/loader!sc/plat/dojo/utils/BundleUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!isccs/utils/OrderLineUtils"], function(_dojodeclare, _extnOrderSummaryLinesExtnUI, _scGridxUtils, _scEventUtils, _isccsOrderLineUtils, _scBaseUtils, _scModelUtils, _isccsUIUtils, dCurrency, scBundleUtils, _scScreenUtils, _extnExpicientUtils, orderLineUtils) {
    return _dojodeclare("extn.order.details.OrderSummaryLinesExtn", [_extnOrderSummaryLinesExtnUI], {
        // custom code here
      
        extn_getShippingService: function(gridReference, rowIndex, columnIndex, gridRowJSON, unformattedValue) {
            var deliveryMethod = gridRowJSON.DeliveryMethod;
            /* if(deliveryMethod == "PICK"){
               _scGridxUtils.hideTableColumn(
                       this, "OLST_listGrid", "extn_shippingService", null);
             } 
            if(unformattedValue =="")
              {
                  unformattedValue ="No Shipping Service";
              } */
            return unformattedValue;
        },
        extn_trackingNoLink: function(event, bEvent, ctrl, args) {
            var cellJson = _scBaseUtils.getAttributeValue("cellJson", false, args);
            var screen = _scEventUtils.getScreenFromEventArguments(args);
            var cellJsonData = _scBaseUtils.getAttributeValue("cellJsonData", false, args);
            var itemData = _scBaseUtils.getAttributeValue("item", false, args);
            if (!_scBaseUtils.isVoid(itemData) && _scBaseUtils.isVoid(cellJsonData)) _scBaseUtils.setAttributeValue("cellJsonData", itemData, args);
            var uniqueRowId = _scBaseUtils.getAttributeValue("uniqueRowId", false, args);
            var rowIndex = _scBaseUtils.getAttributeValue("rowIndex", false, args);
            if (!_scBaseUtils.isVoid(rowIndex) && _scBaseUtils.isVoid(uniqueRowId)) _scBaseUtils.setAttributeValue("uniqueRowId", rowIndex, args);
            if (!(_scBaseUtils.isVoid(cellJson))) {
                var item = _scBaseUtils.getAttributeValue("item", false, args);
                if (_scBaseUtils.equals("extn_ExpectedOn", _scBaseUtils.getAttributeValue("colField", false, cellJson))) {
                    if (_scBaseUtils.equals("ExpectedOn", args.linkUId)) {
                        var delMethod = null;
                        delMethod = _scModelUtils.getStringValueFromPath("DeliveryMethod", itemData);
                        if (_scBaseUtils.equals("SHP", delMethod)) {
                            _isccsOrderLineUtils.OLST_openExpectedOn(event, bEvent, ctrl, args);
                        }
                    }
                    if (_scBaseUtils.equals("TrackingNo", args.linkUId)) {
                        var trackinginfoList = _scModelUtils.getModelObjectFromPath("TrackingInfoList.TrackingInfo", itemData);
                        if (!_scBaseUtils.isVoid(trackinginfoList)) {
                            var trackingTotal = null;
                            trackingTotal = trackinginfoList.length;
                            if (_scBaseUtils.numberEquals(trackingTotal, 1)) {
                                for (var i in trackinginfoList) {
                                    var trackingUrl = _scModelUtils.getModelObjectFromPath("TrackingUrl", trackinginfoList[i]);
                                }
                                _scEventUtils.stopEvent(bEvent);
                                if (!_scBaseUtils.isVoid(trackingUrl)) {
                                    this.extn_openTrackingUrl(trackingUrl);
                                }
                            } else if (_scBaseUtils.numberGreaterThan(trackingTotal, 1)) {
                                _scEventUtils.stopEvent(bEvent);
                                _isccsOrderLineUtils.OLST_openExpectedOn(event, bEvent, ctrl, args);
                            }
                        }
                    }
                } else if (_scBaseUtils.equals("extn_lineTotal", _scBaseUtils.getAttributeValue("colField", false, cellJson))) {
                    _scEventUtils.stopEvent(bEvent);
                    this.extn_openTotalAmount();
                }
            }
        },
        extn_openTotalAmount: function() {
            var orderModel = null;
            orderModel = _scScreenUtils.getModel(this.getOwnerScreen(), "getCompleteOrderDetails_output");
            var cleanOrderModel = null;
            cleanOrderModel = _scScreenUtils.getModel(this.getOwnerScreen(), "popupInput_output");
            var popupParams = null;
            popupParams = _scBaseUtils.getNewBeanInstance();
            var EnterpriseCode = null;
            EnterpriseCode = _scModelUtils.getStringValueFromPath("Order.EnterpriseCode", cleanOrderModel);
            _scModelUtils.setStringValueAtModelPath("Order.CallingOrganizationCode", EnterpriseCode, cleanOrderModel);
            _scBaseUtils.addModelValueToBean("screenInput", cleanOrderModel, popupParams);
            var options = null;
            options = _scBaseUtils.getNewBeanInstance();
            _scBaseUtils.addStringValueToBean("style", "width:90%;", options);
            _scBaseUtils.addStringValueToBean("closeCallBackHandler", "refreshNamespaces", options);
            var screenConstructorParams = null;
            screenConstructorParams = _scBaseUtils.getNewModelInstance();
            _scModelUtils.addStringValueToModelObject("screenMode", "ReadOnlyMode", screenConstructorParams);
            _scBaseUtils.addModelValueToBean("screenConstructorParams", screenConstructorParams, popupParams);
            _isccsUIUtils.openSimplePopup("isccs.order.details.OrderPricingSummary", "Order_Price_Summary", this, popupParams, options);
        },
        refreshNamespaces: function(actionPerformed, targetModel, popupParams) {
            if (!(_scBaseUtils.isVoid(targetModel))) {
                _scScreenUtils.setModel(this.getOwnerScreen(), "getCompleteOrderDetails_output", targetModel, null);
            }
        },
        extn_openTrackingUrl: function(trackingUrl) {
            var options = null;
            options = _scBaseUtils.getNewBeanInstance();
            _scBaseUtils.addStringValueToBean("destination", "window", options);
            _isccsUIUtils.openURL(trackingUrl, options);
        },
    });
});
