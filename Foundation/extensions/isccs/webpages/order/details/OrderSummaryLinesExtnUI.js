scDefine(["dojo/text!./templates/OrderSummaryLinesExtn.html", "scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/_base/lang", "scbase/loader!dojo/text", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!extn/utils/CustomUtils", "scbase/loader!gridx/Grid", "scbase/loader!sc/plat", "scbase/loader!sc/plat/dojo/binding/GridxDataBinder", "scbase/loader!sc/plat/dojo/utils/BaseUtils"], function(templateText, _dojodeclare, _dojokernel, _dojolang, _dojotext, _extnExpicientUtil, _extnCustomUtils, _gridxGrid, _scplat, _scGridxDataBinder, _scBaseUtils) {
    return _dojodeclare("extn.order.details.OrderSummaryLinesExtnUI", [], {
        templateString: templateText,
        hotKeys: [],
        events: [],
        subscribers: {
            local: [{
                eventId: 'OLST_listGrid_Link_ScHandleLinkClicked',
                sequence: '19',
                handler: {
                    methodName: "extn_trackingNoLink"
                }
            }, {
                eventId: 'OLST_listGrid_extn_lineTotal_onClick',
                sequence: '51',
                handler: {
                    methodName: "extn_opendetails"
                }
            }]
        }
    });
});
