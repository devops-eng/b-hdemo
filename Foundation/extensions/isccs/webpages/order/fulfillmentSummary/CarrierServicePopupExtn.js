scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/order/fulfillmentSummary/CarrierServicePopupExtnUI",
"scbase/loader!isccs/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils",
"scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!sc/plat/dojo/utils/ModelUtils",
"scbase/loader!sc/plat/dojo/utils/WidgetUtils","scbase/loader!isccs/utils/BaseTemplateUtils"]
,
function(			 
			    _dojodeclare
			,
			    _extnCarrierServicePopupExtnUI
			,
			    _isccsUIUtils
			,
			    _scScreenUtils
                     ,
                          _scBaseUtils
                     ,
                          _scModelUtils
			,
			     _scWidgetUtils
                     ,
                         _isccsBaseTemplateUtils

){ 
	return _dojodeclare("extn.order.fulfillmentSummary.CarrierServicePopupExtn", [_extnCarrierServicePopupExtnUI],{
	// custom code here
	 extn_initScreen:function( event, bEvent, ctrl, args) 
        {
             var screenModel = null;
             screenModel = _scScreenUtils.getInitialInputData(this);
             var orderLineArray = _scBaseUtils.getNewArrayInstance();
             var model= _scScreenUtils.getModel(this, "extn_OrderDetails_ns");
             var inputModel = _scBaseUtils.getNewModelInstance();
             var enterpriseCode= _scModelUtils.getStringValueFromPath("Order.EnterpriseCode",model);
             _scModelUtils.setStringValueAtModelPath("Order.EnterpriseCode",enterpriseCode,inputModel);
             var personInfoShipTo=  _scModelUtils.getModelObjectFromPath("Order.PersonInfoShipTo", screenModel);
             var orderLine =_scModelUtils.getModelObjectFromPath("Order.OrderLines.OrderLine", model);
             for(var index in orderLine)
              {
                 var deliveryMethod = _scModelUtils.getStringValueFromPath("DeliveryMethod",orderLine[index]);
                 if(deliveryMethod == "SHP")
                 {
                    var orderLineKey= _scModelUtils.getStringValueFromPath("OrderLineKey",orderLine[index]);
                    var currentOrderLine =_scModelUtils.getModelObjectFromPath("Order.OrderLines.OrderLine", screenModel); 
                    for(var i in currentOrderLine)
                      {
                        var currentOrderLineKey= _scModelUtils.getStringValueFromPath("OrderLineKey",currentOrderLine[i]);
                        if(orderLineKey == currentOrderLineKey)
                          {
                            var orderLineModel = _scBaseUtils.getNewModelInstance();
                            _scModelUtils.setStringValueAtModelPath("OrderLineKey",orderLineKey,orderLineModel);
                            var carrierServiceCode = _scModelUtils.getStringValueFromPath("CarrierServiceCode",orderLine[index]);
                            _scModelUtils.setStringValueAtModelPath("CarrierServiceCode",carrierServiceCode,orderLineModel);
                            var shipTogetherNo = _scModelUtils.getStringValueFromPath("ShipTogetherNo",orderLine[index]);
                            _scModelUtils.setStringValueAtModelPath("ShipTogetherNo",shipTogetherNo,orderLineModel);
                            var qty= _scModelUtils.getStringValueFromPath("OrderedQty",orderLine[index]);
                            _scModelUtils.setStringValueAtModelPath("OrderedQty",qty,orderLineModel);
                            var shipToKey = _scModelUtils.getStringValueFromPath("ShipToKey",orderLine[index]); 
                            if(_scBaseUtils.isVoid(shipToKey))
                              {
                                var shipToKeyOrder= _scModelUtils.getStringValueFromPath("Order.ShipToKey",model);
                               _scModelUtils.setStringValueAtModelPath("ShipToKey",shipToKeyOrder,orderLineModel);
                              }
                            else
                              {
                               _scModelUtils.setStringValueAtModelPath("ShipToKey",shipToKey,orderLineModel);
                              }
                            var item = _scModelUtils.getModelObjectFromPath("Item", orderLine[index]);
                            var itemId = _scModelUtils.getStringValueFromPath("ItemID",item);
                            var productLine = _scModelUtils.getStringValueFromPath("ProductLine",item);
                            _scModelUtils.setStringValueAtModelPath("Item.ItemID",itemId,orderLineModel);
                           _scModelUtils.setStringValueAtModelPath("Item.ProductLine",productLine,orderLineModel);
                            var linePriceModel= _scModelUtils.getModelObjectFromPath("LinePriceInfo", orderLine[index]);
                            var displayUnitPrice = _scModelUtils.getStringValueFromPath("ListPrice",linePriceModel);
                            var unitPrice = _scModelUtils.getStringValueFromPath("UnitPrice",linePriceModel);
                           _scModelUtils.setStringValueAtModelPath("LinePriceInfo.DisplayUnitPrice",displayUnitPrice,orderLineModel);
                           _scModelUtils.setStringValueAtModelPath("LinePriceInfo.UnitPrice",unitPrice,orderLineModel);
                           var primaryInfoModel =  _scModelUtils.getModelObjectFromPath("ItemDetails.PrimaryInformation", orderLine[index]);
                           var isShippingAllowed = _scModelUtils.getStringValueFromPath("IsShippingAllowed",primaryInfoModel);
                           var isAirShippingAllowed= _scModelUtils.getStringValueFromPath("IsAirShippingAllowed",primaryInfoModel);
                           _scModelUtils.setStringValueAtModelPath("ItemDetails.PrimaryInformation.IsShippingAllowed",isShippingAllowed,orderLineModel);
                           _scModelUtils.setStringValueAtModelPath("ItemDetails.PrimaryInformation.IsAirShippingAllowed",isAirShippingAllowed,orderLineModel);
                           var extnModel =  _scModelUtils.getModelObjectFromPath("ItemDetails.Extn", orderLine[index]);
                           var eligible = _scModelUtils.getStringValueFromPath("ExtnEcoFeeEligible",extnModel );
                           var province= _scModelUtils.getStringValueFromPath("ExtnExcludedStateProvinceList",extnModel );
	                    var dsvEnabled= _scModelUtils.getStringValueFromPath("ExtnIsDSVEnabled",extnModel );
                           _scModelUtils.setStringValueAtModelPath("ItemDetails.Extn.ExtnEcoFeeEligible",eligible,orderLineModel);
                           _scModelUtils.setStringValueAtModelPath("ItemDetails.Extn.ExtnExcludedStateProvinceList",province,orderLineModel);
	                    _scModelUtils.setStringValueAtModelPath("ItemDetails.Extn.ExtnIsDSVEnabled",dsvEnabled,orderLineModel);
                           var personInfoShipToOrderLine =  _scModelUtils.getModelObjectFromPath("PersonInfoShipTo", orderLine[index]);
                           var personInfoShipToOrderLine =  _scModelUtils.getModelObjectFromPath("PersonInfoShipTo", orderLine[index]);
                           if(_scBaseUtils.isVoid(personInfoShipToOrderLine))
                            {
                              _scModelUtils.setStringValueAtModelPath("PersonInfoShipTo",personInfoShipTo,orderLineModel);
                            }
                          else
                            {
                              _scModelUtils.setStringValueAtModelPath("PersonInfoShipTo",personInfoShipToOrderLine,orderLineModel);
                            }
                          _scBaseUtils.appendToArray(orderLineArray,orderLineModel);
                       }
                   }
              }
         }
            _scModelUtils.setStringValueAtModelPath("Order.OrderLines.OrderLine",orderLineArray,inputModel);
            _isccsUIUtils.callApi(this,inputModel, "extn_LOSMashup", null);	
       },

    extn_handleMashupOutput:function( event, bEvent, ctrl, args) {

        var ShippingModes = null;
        var  ShippingCharges= null;
        var shippingModeDisplay= null;
        var shippingChargeModel =null;
        var inputData = args.inputData;
        var output= _scModelUtils.getModelObjectFromPath("mashupArray",args);
	 for(var index in output)
          {
            if(output[index].mashupRefId == "extn_LOSMashup")
              {
                 ShippingModes = _scModelUtils.getModelObjectFromPath("mashupRefOutput",output[index]);
		   var shippingModesList = _scModelUtils.getModelListFromPath("basket.ResponseLineItem", ShippingModes );
		   var model=_scScreenUtils.getModel(this, "extn_los_ns");
                  if(!_scBaseUtils.isVoid(shippingModesList))
                   {
		          var checkForShippingList = null;
		          checkForShippingList = shippingModesList.length;
		          if(!_scBaseUtils.isVoid(checkForShippingList ))
                         {
		              for(var shippingCount in shippingModesList){
		              shippingModeDisplay = _scModelUtils.getModelListFromPath("eligibleShipModes",shippingModesList[0]);
		               break;
             	            }
		      }
                  else
		      {
		       shippingModeDisplay = _scModelUtils.getModelObjectFromPath("eligibleShipModes",shippingModesList);
		      }
		      if(!_scBaseUtils.isVoid(shippingModeDisplay))
		      {
		      if(_scBaseUtils.isVoid(shippingModeDisplay.ShippingMode.length))
		      {
		          var serviceModel = _scBaseUtils.getNewModelInstance();
		          var shippingModeModel =  _scBaseUtils.getNewModelInstance();
		          var serviceArray = _scBaseUtils.getNewArrayInstance();
		          var serviceType =  _scModelUtils.getStringValueFromPath("serviceType",shippingModeDisplay.ShippingMode);
		          _scModelUtils.setStringValueAtModelPath("serviceType",serviceType ,serviceModel);
		          _scBaseUtils.appendToArray(serviceArray ,serviceModel);
		          _scModelUtils.addModelToModelPath ("ShippingMode",serviceArray ,shippingModeModel);
		          _scScreenUtils.setModel(this, "extn_los_ns",shippingModeModel,null);
		      }
		      else
		      {
		        _scScreenUtils.setModel(this, "extn_los_ns",shippingModeDisplay ,null);
		      }
		      }
		      else
		      {
		          var shippingRestrictions= _scModelUtils.getStringValueFromPath("shippingRestrictions",shippingModesList);
		           if(!(_scBaseUtils.isVoid(shippingRestrictions)))

                        {
                            var itemId = inputData[0].mashupInputObject.Order.OrderLines.OrderLine[0].Item.ItemID;
                             var errorMsg ="";
                             var restrictionText=_scModelUtils.getStringValueFromPath("ShippingRestriction.restrictionText",shippingRestrictions);
                             var restrictionCode=_scModelUtils.getStringValueFromPath("ShippingRestriction.restrictionCode",shippingRestrictions);
                             errorMsg = itemId +":" +restrictionCode +":" +restrictionText;
                             _isccsBaseTemplateUtils.showMessage(this,errorMsg, "error", null);
                             return true;
                        }
                        else 
                        {
                            shippingRestrictions= _scModelUtils.getStringValueFromPath("shippingRestrictions",shippingModesList[0]);
                            var orderLine= inputData[0].mashupInputObject.Order.OrderLines.OrderLine;
                            var errorMsg ="";
                            for(var index in orderLine)
                            {
                             var itemId = orderLine[index].Item.ItemID;
                             var restrictionText=_scModelUtils.getStringValueFromPath("ShippingRestriction.restrictionText",shippingRestrictions);
                             var restrictionCode=_scModelUtils.getStringValueFromPath("ShippingRestriction.restrictionCode",shippingRestrictions);
                             errorMsg = errorMsg +"\n" +itemId +":" +restrictionCode +":" + restrictionText;
                             
                            _isccsBaseTemplateUtils.showMessage(this,errorMsg, "error", null);
                            }
                         return true;
                        }
		      }
                   var orderModel = _scBaseUtils.getNewModelInstance();
                   var model = _scScreenUtils.getModel(this, "extn_OrderDetails_ns");
                   var orderHeaderKey =  _scModelUtils.getStringValueFromPath("Order.OrderHeaderKey",model);
                    _scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey",orderHeaderKey,orderModel);
		     _isccsUIUtils.callApi(this, orderModel, "extn_getDsvFlagMashUp", null);
                }
                 else 
                {
                   var exception = _scModelUtils.getStringValueFromPath("YfsException.Exception",ShippingModes);  
                   if(exception == "State")
                   {
                         var message = null;
                         message = _scScreenUtils.getString(this,"extn_LOS_StateValidation");
                        _isccsBaseTemplateUtils.showMessage(this,message,"error",null);

                     //  _isccsBaseTemplateUtils.showMessage(this,"Enter the Valid State in the Shipping Address", "error", null);
                   }
                   else if(exception == "Postal")
                   {
                         var message = null;
                          message = _scScreenUtils.getString(this,"extn_LOS_ZipCode_Validation");
                         _isccsBaseTemplateUtils.showMessage(this,message,"error",null);

                       //_isccsBaseTemplateUtils.showMessage(this,"Enter the Valid Postal Code in the Shipping Address", "error", null);
                   }
                   
                   else if(_scBaseUtils.isVoid(ShippingModes.NoRequestLine))
                   {
                         var message = null;
                         message = _scScreenUtils.getString(this,"extn_LOS_NoRequest_Validation");
                         _isccsBaseTemplateUtils.showMessage(this,message,"error",null);

                       // _isccsBaseTemplateUtils.showMessage(this,"No Shipping Service available for the selected Item", "error", null);
                   }
                   else 
                   {
                          var message = null;
                          message = _scScreenUtils.getString(this,"extn_LOSWebServiceDown");
                         _isccsBaseTemplateUtils.showMessage(this,message,"error",null);

                    //    _isccsBaseTemplateUtils.showMessage(this,"Not able to connect to Shipping Service", "error", null);
                   }
                }        
            }
      else if(output[index].mashupRefId == "extn_getDsvFlagMashUp")
	 {
	      var orderModel = _scModelUtils.getModelObjectFromPath("mashupRefOutput",output[index]);
             var orderListModel = _scModelUtils.getModelListFromPath("OrderList.Order",orderModel);
             var losModel = _scScreenUtils.getModel(this,"extn_los_ns");
             var commonCodeModel = _scScreenUtils.getModel(this,"extn_dsvItems_ns");
             var serviceArray=_scBaseUtils.getNewArrayInstance();
             for(var i in orderListModel)
             {
                 var orderLine = _scModelUtils.getModelListFromPath("OrderLines.OrderLine",orderListModel[i]);
                 var enterpriseCode =_scModelUtils.getStringValueFromPath("EnterpriseCode",orderListModel[i]);
                 for(var j in orderLine)
                 {
                     var dsvModel =  _scModelUtils.getModelObjectFromPath("ItemDetails.Extn",orderLine[j]);
                     var dsvFlag = _scModelUtils.getStringValueFromPath("ExtnIsDSVEnabled",dsvModel);
                     //dsvFlag="Y";
                     if(dsvFlag == "Y")
                     {
                        var commonCode = _scModelUtils.getModelObjectFromPath("CommonCodeList.CommonCode",commonCodeModel);
                        for(var c in commonCode)
                        {
                            if(commonCode[c].OrganizationCode == enterpriseCode)
                            {
                                var service = _scModelUtils.getStringValueFromPath("CodeShortDescription",commonCode[c]);
                                 var serviceModel = _scBaseUtils.getNewModelInstance();
                                _scModelUtils.setStringValueAtModelPath("serviceType",service,serviceModel)
                                _scBaseUtils.appendToArray(serviceArray,serviceModel);
                            }
                             
                        }
                        var shippingModeModel= _scBaseUtils.getNewModelInstance();
                        _scModelUtils.setStringValueAtModelPath("ShippingMode",serviceArray,shippingModeModel);
                        _scScreenUtils.setModel(this,"extn_los_ns",shippingModeModel,null);
                         break;  
                     }
                 }
             }
              var oModel=_scScreenUtils.getModel(this, "extn_los_ns");
              if(_scBaseUtils.isVoid(oModel))
              {
                 var losModel = _scScreenUtils.getModel(this,"extn_shippingModes");
                 _scScreenUtils.setModel(this,"extn_los_ns",losModel,null);  
              }
		}	 
        } 
},

	extn_changeLOS: function(event, bEvent, ctrl, args) {
		this.carrierServiceOnChange();  
	}
});
});
