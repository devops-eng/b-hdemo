


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/order/fulfillmentSummary/CarrierServicePopupExtn","scbase/loader!sc/plat/dojo/controller/ExtnServerDataController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnCarrierServicePopupExtn
			 ,
			    _scExtnServerDataController
){

return _dojodeclare("extn.order.fulfillmentSummary.CarrierServicePopupExtnBehaviorController", 
				[_scExtnServerDataController], {

			
			 screenId : 			'extn.order.fulfillmentSummary.CarrierServicePopupExtn'

			
			
			
			
			
						,

			
			
			 mashupRefs : 	[
	 		{
		 mashupRefId : 			'extn_LOSMashup'
,
		 mashupId : 			'extn_LOSMashup'
,
		 extnType : 			'ADD'

	}
,
	 		{
		 mashupRefId : 			'extn_ShippingChargesMashUP'
,
		 mashupId : 			'extn_ShippingChargesMashUP'
,
		 extnType : 			'ADD'

	}
,
	 		{
		 mashupRefId : 			'extn_getDsvFlagMashUp'
,
		 mashupId : 			'extn_getDsvFlagMashUp'
,
		 extnType : 			'ADD'

	}

	]

}
);
});

