
scDefine(["dojo/text!./templates/CarrierServicePopupExtn.html","scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/_base/lang","scbase/loader!dojo/text","scbase/loader!idx/form/FilteringSelect","scbase/loader!sc/plat","scbase/loader!sc/plat/dojo/binding/ComboDataBinder","scbase/loader!sc/plat/dojo/utils/BaseUtils"]
 , function(			 
			    templateText
			 ,
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojolang
			 ,
			    _dojotext
			 ,
			    _idxFilteringSelect
			 ,
			    _scplat
			 ,
			    _scComboDataBinder
			 ,
			    _scBaseUtils
){
return _dojodeclare("extn.order.fulfillmentSummary.CarrierServicePopupExtnUI",
				[], {
			templateString: templateText
	
	
	
	
	
	
					,	
	namespaces : {
		targetBindingNamespaces :
		[
		],
		sourceBindingNamespaces :
		[
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_4'
						,
	  description: 'extn_los_ns'
						,
	  value: 'extn_los_ns'
						
			}
			,
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_5'
						,
	  description: 'extn_OrderDetails_ns'
						,
	  value: 'extn_OrderDetails_ns'
						
			}
			,
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_6'
						,
	  description: 'extn_ShippingCharges_ns'
						,
	  value: 'extn_ShippingCharges_ns'
						
			}
			,
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_7'
						,
	  description: 'extn_dsvItems_ns'
						,
	  value: 'extn_dsvItems_ns'
						
			}
			
		]
	}

	

	,
	hotKeys: [ 
	]

,events : [
	]

,subscribers : {
local : [

{
	  eventId: 'afterScreenInit'

,	  sequence: '19'

,	  description: 'extn_initScreen'



,handler : {
methodName : "extn_initScreen"

 
}
}
,
{
	  eventId: 'onExtnMashupCompletion'

,	  sequence: '51'

,	  description: 'extn_handleMashupOutput'



,handler : {
methodName : "extn_handleMashupOutput"

 
}
}
,
{
	  eventId: 'extn_levelofService_onChange'

,	  sequence: '51'

,	  description: 'extn_changeLOS'



,handler : {
methodName : "extn_changeLOS"

 
}
}

]
					 	 ,
}

});
});


