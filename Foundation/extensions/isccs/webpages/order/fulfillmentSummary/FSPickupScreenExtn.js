scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/order/fulfillmentSummary/FSPickupScreenExtnUI","scbase/loader!sc/plat/dojo/utils/GridxUtils","scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!sc/plat/dojo/utils/ScreenUtils","scbase/loader!sc/plat/dojo/utils/ModelUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnFSPickupScreenExtnUI
             ,
                _scGridxUtils
             ,
                _scBaseUtils
             ,
               _scScreenUtils
             ,
               _scModelUtils
){ 
	return _dojodeclare("extn.order.fulfillmentSummary.FSPickupScreenExtn", [_extnFSPickupScreenExtnUI],{
	// custom code here

     extn_DefaultPickUpDate:function(event, bEvent, ctrl, args) {
       var selectedSourceRecordsList = null;
        selectedSourceRecordsList = _scGridxUtils.getSelectedSourceRecordsWithRowIndex(this, "OLST_listGrid");
        if(!_scBaseUtils.isVoid(selectedSourceRecordsList))
        {
           _scGridxUtils.deselectAllRowsInGridUsingUId(this, "OLST_listGrid");
          _scGridxUtils.selectAllRowsUsingUId(this, "OLST_listGrid");
        }
     },

  extn_init:function(event, bEvent, ctrl, args) {
            var Data = null;
            Data = _scScreenUtils.getModel(
            this, "getFulfillmentSummaryDetails_output");
            var orderLineList = _scModelUtils.getStringValueFromPath("Page.Output.OrderLines.OrderLine",Data);
            for(var index in orderLineList)
            {
                var pickupDate = orderLineList[index].ReqShipDate;
                if(_scBaseUtils.isVoid(pickupDate))
                {
                    var date =_scBaseUtils.getDate();
                    var formattedServerDate = _scBaseUtils.convertToServerFormat(date);
                    _scModelUtils.setStringValueAtModelPath("ReqShipDate",formattedServerDate,orderLineList[index]);
                }
            }
  },

extn_PickupDate:function(event, bEvent, ctrl, args) {
 var output =  _scModelUtils.getStringValueFromPath("mashupArray", args);
   /* for(var index in output)
    {
        var mashupId = _scModelUtils.getStringValueFromPath("mashupRefId", output[index]);
        if(mashupId == "getCompleteOrderLineList")
        {
        var outputModel = _scModelUtils.getStringValueFromPath("mashupRefOutput", output[index]);
        var orderLinesList =_scModelUtils.getStringValueFromPath("Page.Output.OrderLines.OrderLine", outputModel); 
            for(var i in orderLinesList)
            {
                var pickupDate = orderLineList[index].ReqShipDate;
                if(_scBaseUtils.isVoid(pickupDate))
                {
                    var date =_scBaseUtils.getDate();
                    var formattedServerDate = _scBaseUtils.convertToServerFormat(date);
                    _scModelUtils.setStringValueAtModelPath("ReqShipDate",formattedServerDate,orderLineList[index]);
                }
            }
        }
    } */
}
});
});

