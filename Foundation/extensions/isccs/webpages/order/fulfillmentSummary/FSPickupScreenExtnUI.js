
scDefine(["dojo/text!./templates/FSPickupScreenExtn.html","scbase/loader!dijit/form/Button","scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/_base/lang","scbase/loader!dojo/text","scbase/loader!sc/plat","scbase/loader!sc/plat/dojo/binding/ButtonDataBinder","scbase/loader!sc/plat/dojo/utils/BaseUtils"]
 , function(			 
			    templateText
			 ,
			    _dijitButton
			 ,
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojolang
			 ,
			    _dojotext
			 ,
			    _scplat
			 ,
			    _scButtonDataBinder
			 ,
			    _scBaseUtils
){
return _dojodeclare("extn.order.fulfillmentSummary.FSPickupScreenExtnUI",
				[], {
			templateString: templateText
	
	
	
	
	
	
	

	,
	hotKeys: [ 
	]

,events : [
	]

,subscribers : {
local : [

{
	  eventId: 'afterScreenInit'

,	  sequence: '19'

,	  description: 'extn_init'



,handler : {
methodName : "extn_init"

 
}
}
,
{
	  eventId: 'pickupDate_onClick'

,	  sequence: '19'

,	  description: 'extn_DefaultPickUpDate'



,handler : {
methodName : "extn_DefaultPickUpDate"

 
}
}
,
{
	  eventId: 'afterBehaviorMashupCall'

,	  sequence: '51'

,	  description: 'extn_PickupDate'



,handler : {
methodName : "extn_PickupDate"

 
}
}

]
					 	 ,
}

});
});


