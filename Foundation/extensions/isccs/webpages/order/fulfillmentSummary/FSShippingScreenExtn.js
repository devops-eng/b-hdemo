scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/order/fulfillmentSummary/FSShippingScreenExtnUI","scbase/loader!sc/plat/dojo/utils/ScreenUtils","scbase/loader!sc/plat/dojo/utils/EditorUtils","scbase/loader!isccs/utils/ModelUtils",
"scbase/loader!sc/plat/dojo/utils/ModelUtils","scbase/loader!sc/plat/dojo/utils/BaseUtils",
"scbase/loader!isccs/utils/UIUtils","scbase/loader!sc/plat/dojo/utils/EventUtils","scbase/loader!sc/plat/dojo/utils/GridxUtils","scbase/loader!sc/plat/dojo/utils/WidgetUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnFSShippingScreenExtnUI,
 				_scScreenUtils,
				   _scEditorUtils,
				   _iscModelUtils,
				   _scModelUtils,
				   _scBaseUtils,
                   _isccsUIUtils,
			_scEventUtils,
			_scGridxUtils,
                    _scWidgetUtils
){ 
	return _dojodeclare("extn.order.fulfillmentSummary.FSShippingScreenExtn", [_extnFSShippingScreenExtnUI],{
	// custom code here
// Used to disable the "Delete can" of "GiftBox" OrderLine.
// To call getcommoncedelist Api for GiftBoxID.

extn_DisableGiftBox: function(event, bEvent, ctrl, args)
	{
		var orderModel = _scScreenUtils.getModel(this, "getFulfillmentSummaryDetails_output");
		
		var carrierServiceCode = _scModelUtils.getStringValueFromPath("CarrierServiceCode", orderModel);
		if(_scBaseUtils.isVoid(carrierServiceCode))
		{
		    var label = _scScreenUtils.getString(this,"extn_ShippingLabel");
		     _scWidgetUtils.setLabel(this,"changeLOS",label);
		  //  _scWidgetUtils.setLabel(this,"changeLOS","Select Shipping Method");
		}
 		var GiftBoxModel = _scScreenUtils.getModel(this, "extn_GiftBoxCommonCode_NS");
		if(_scBaseUtils.isVoid(GiftBoxModel))
		{
		  apiInput = _scBaseUtils.getNewModelInstance();
		  var orderModel1 = _scScreenUtils.getModel(this, "getCompleteOrderDetails_output");
		  _scModelUtils.setStringValueAtModelPath("CommmonCode.OrganizationCode", orderModel1.Order.EnterpriseCode ,apiInput);
      		_isccsUIUtils.callApi(this, apiInput, "extn_getCommonCode_Behv_FS");
		}

		else{
    			var CommonCodeList=_scModelUtils.getModelListFromPath("CommonCodeList.CommonCode", GiftBoxModel );
			var GiftBoxID=null;
            		for(var i in CommonCodeList)
			{
            			var CommonCodeObj = _scModelUtils.getModelFromList(CommonCodeList, i); 
           			var CodeType=_scModelUtils.getStringValueFromPath("CodeType", CommonCodeObj );
           			if(CodeType=="LT_CustomCode_Gift")
            			{
                			 GiftBoxID=_scModelUtils.getStringValueFromPath("CodeValue", CommonCodeObj );           			 }
            			}
				var OrderLineList=_scModelUtils.getModelListFromPath("Page.Output.OrderLines.OrderLine", orderModel);
				for(var i in OrderLineList)
				{
					var OrderLineElement = OrderLineList[i];
		   			var ItemID = _scModelUtils.getStringValueFromPath("Item.ItemID", OrderLineElement);
					if(_scBaseUtils.equals(ItemID,GiftBoxID))
					{
 						_scModelUtils.setStringValueAtModelPath("CanDeleteOnUI", "00",OrderLineElement);
						var ModificationList=_scModelUtils.getModelListFromPath("Modifications.Modification", OrderLineElement);
						for(var k in ModificationList)
						{
							var ModificationLineElement = ModificationList[k];
							var ModificationType = _scModelUtils.getStringValueFromPath("ModificationType", ModificationLineElement);
							if(_scBaseUtils.equals(ModificationType,"REMOVE"))
							{
								_scModelUtils.setStringValueAtModelPath("ModificationAllowed", "N",ModificationLineElement);
							}
						}
					}
				}
      		_scScreenUtils.setModel(this, "getFulfillmentSummaryDetails_output",orderModel, null); 
 		this.initializeScreen();
}
	},
  /*    onChangeCarrierService: function(
        actionPerformed, model, popupParams) {
            var argumentForStore = null;
            argumentForStore = _scBaseUtils.getNewBeanInstance();
            if (
            _scBaseUtils.negateBoolean(
            _scBaseUtils.or(
            _scBaseUtils.equals(
            actionPerformed, "CLOSE"), _scBaseUtils.equals(
            actionPerformed, "APPLY_NOCHANGE")))) {
                var outputModel = null;
                outputModel = _scScreenUtils.getModel(
                this, "gerCarrierServiceOutput");
                var carrierServiceCode = null;
                carrierServiceCode = _scModelUtils.getStringValueFromPath("CarrierServiceCode", outputModel);
                var temp = null;
                temp = _scBaseUtils.getNewBeanInstance();
                _scEventUtils.fireEventToParent(
                this, "ScreenChanged", temp);
                    _scBaseUtils.setAttributeValue("argumentList.CarrierDetails", outputModel.extn_shippingDetails, temp);
		        _scEventUtils.fireEventToParent(
                this, "extn_setCarrierDetailsToEditor", temp);
                temp = _scBaseUtils.getNewBeanInstance();
                _scEventUtils.fireEventToParent(
                this, "ScreenChanged", temp);
                var newModelData = null;
                newModelData = _scBaseUtils.getNewModelInstance();
                _scModelUtils.addStringValueToModelObject("CarrierServiceCode", carrierServiceCode, newModelData);
                this.updateCarrierService(
                model);
            }
        }*/

// To update GiftBoxID

extn_afterBehaviorMashup: function(event, bEvent, ctrl, args)
{
var MashupList=args.mashupArray;
		for(var i in MashupList)
		{
            var OrderLineElement = MashupList[i];
            var mashupRefId=_scModelUtils.getStringValueFromPath("mashupRefId", OrderLineElement);
			if (_scBaseUtils.equals(mashupRefId, "extn_getCommonCode_Behv_FS")) 
               {
                  _scScreenUtils.setModel(this, "extn_GiftBoxCommonCode_NS",OrderLineElement.mashupRefOutput, null); 
                  this.extn_DisableGiftBox();
               }
               else if(_scBaseUtils.equals(mashupRefId, "getCompleteOrderLineList")) 
               {
                    var outputModel = _scModelUtils.getStringValueFromPath("mashupRefOutput", OrderLineElement);
                    var orderModel = _scScreenUtils.getModel(this, "getCompleteOrderDetails_output");
                    var personInfoShipTo = _scModelUtils.getStringValueFromPath("Order.PersonInfoShipTo", orderModel);
                    _scModelUtils.setStringValueAtModelPath("PersonInfoShipTo", personInfoShipTo,outputModel);
                    var orderLinesList =_scModelUtils.getStringValueFromPath("Page.Output.OrderLines.OrderLine", outputModel); 
                    for(var index in orderLinesList)
                      {
                       _scModelUtils.setStringValueAtModelPath("PersonInfoShipTo", personInfoShipTo,orderLinesList[index]);
                       }
               }
         }


},

extn_updateShipToAddress: function(event, bEvent, ctrl, args)
{
           var Data = null;
            Data = _scScreenUtils.getModel(
            this, "getFulfillmentSummaryDetails_output");
            var orderModel = _scScreenUtils.getModel(this, "getCompleteOrderDetails_output");
            var personInfoShipTo = _scModelUtils.getStringValueFromPath("Order.PersonInfoShipTo", orderModel);
            _scModelUtils.setStringValueAtModelPath("PersonInfoShipTo", personInfoShipTo,Data);
           var orderLinesList =_scModelUtils.getStringValueFromPath("Page.Output.OrderLines.OrderLine", Data); 
            for(var index in orderLinesList)
            {
                _scModelUtils.setStringValueAtModelPath("PersonInfoShipTo", personInfoShipTo,orderLinesList[index]);
            }
},

extn_disablePickupOption: function(event, bEvent, ctrl, args)
{
_scWidgetUtils.disableWidget(this,"changeToPickup",false);

}



});
});
