
scDefine(["dojo/text!./templates/FSShippingScreenExtn.html","scbase/loader!dijit/form/Button","scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/_base/lang","scbase/loader!dojo/text","scbase/loader!gridx/Grid","scbase/loader!sc/plat","scbase/loader!sc/plat/dojo/binding/ButtonDataBinder","scbase/loader!sc/plat/dojo/binding/GridxDataBinder","scbase/loader!sc/plat/dojo/utils/BaseUtils"]
 , function(			 
			    templateText
			 ,
			    _dijitButton
			 ,
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojolang
			 ,
			    _dojotext
			 ,
			    _gridxGrid
			 ,
			    _scplat
			 ,
			    _scButtonDataBinder
			 ,
			    _scGridxDataBinder
			 ,
			    _scBaseUtils
){
return _dojodeclare("extn.order.fulfillmentSummary.FSShippingScreenExtnUI",
				[], {
			templateString: templateText
	
	
	
	
	
	
					,	
	namespaces : {
		targetBindingNamespaces :
		[
		],
		sourceBindingNamespaces :
		[
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_3'
						,
	  value: 'extn_shippingDetails'
						
			}
			,
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_4'
						,
	  description: 'extn_GiftBoxCommonCode_NS'
						,
	  value: 'extn_GiftBoxCommonCode_NS'
						
			}
			
		]
	}

	

	,
	hotKeys: [ 
	]

,events : [
	]

,subscribers : {
local : [

{
	  eventId: 'OLST_listGrid_onClickOfCancel'

,	  sequence: '51'

,	  description: 'extn_disablePickupOption'



,handler : {
methodName : "extn_disablePickupOption"

 
}
}
,
{
	  eventId: 'OLST_listGrid_ScRowSelect'

,	  sequence: '51'

,	  description: 'extn_disablePickupOption'



,handler : {
methodName : "extn_disablePickupOption"

 
}
}
,
{
	  eventId: 'OLST_listGrid_ScRowDeselect'

,	  sequence: '51'

,	  description: 'extn_disablePickupOption'



,handler : {
methodName : "extn_disablePickupOption"

 
}
}
,
{
	  eventId: 'OLST_listGrid_ScHeaderDeselect'

,	  sequence: '51'

,	  description: 'extn_disablePickupOption'



,handler : {
methodName : "extn_disablePickupOption"

 
}
}
,
{
	  eventId: 'afterScreenInit'

,	  sequence: '19'

,	  description: 'extn_updateShipToAddress'



,handler : {
methodName : "extn_updateShipToAddress"

 
}
}
,
{
	  eventId: 'afterScreenInit'

,	  sequence: '51'

,	  description: 'extn_DisableGiftBox'



,handler : {
methodName : "extn_DisableGiftBox"

 
}
}
,
{
	  eventId: 'afterBehaviorMashupCall'

,	  sequence: '51'

,	  description: 'extn_afterBehaviorMashup'



,handler : {
methodName : "extn_afterBehaviorMashup"

 
}
}

]
					 	 ,
}

});
});


