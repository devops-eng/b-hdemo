scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/order/fulfillmentSummary/FulfillmentSummaryDetailsScreenExtnUI",
"scbase/loader!sc/plat/dojo/utils/ScreenUtils","scbase/loader!sc/plat/dojo/utils/GridxUtils","scbase/loader!sc/plat/dojo/utils/EditorUtils",
"scbase/loader!sc/plat/dojo/utils/ModelUtils","scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!isccs/utils/UIUtils",
"scbase/loader!sc/plat/dojo/utils/ControllerUtils","scbase/loader!sc/plat/dojo/utils/WidgetUtils","scbase/loader!sc/plat/dojo/utils/EventUtils",
"scbase/loader!isccs/utils/OrderUtils","scbase/loader!isccs/utils/BaseTemplateUtils","scbase/loader!isccs/utils/ModelUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnFulfillmentSummaryDetailsScreenExtnUI
			, _scScreenUtils
                  , _scGridxUtils
				  ,_scEditorUtils
                  ,_scModelUtils
                  ,_scBaseUtils
                  ,_isccsUIUtils
				  ,_scControllerUtils
                  ,_scWidgetUtils
				  ,_scEventUtils
				  ,_isccsOrderUtils
				  ,_isccsBaseTemplateUtils
                  ,_isccsModelUtils
				 
){ 
	return _dojodeclare("extn.order.fulfillmentSummary.FulfillmentSummaryDetailsScreenExtn", [_extnFulfillmentSummaryDetailsScreenExtnUI],{
	// custom code here

//Used to form a model with GiftBox OrderLines which are to be deleted.

extn_onSave: function(event, bEvent, ctrl, args)
	{
	         var GiftBoxOrderModel = _scScreenUtils.getModel(this, "extn_GiftBoxCommonList");
            var CommonCodeList=_scModelUtils.getModelListFromPath("CommonCodeList.CommonCode", GiftBoxOrderModel);
           var GiftBoxID=null;
            	for(var i in CommonCodeList)
		{
            var CommonCodeObj = _scModelUtils.getModelFromList(CommonCodeList, i); 
            var CodeType=_scModelUtils.getStringValueFromPath("CodeType", CommonCodeObj );
           	if(CodeType=="LT_CustomCode_Gift")
            	{
                 GiftBoxID=_scModelUtils.getStringValueFromPath("CodeValue", CommonCodeObj );
            }
            
		}
		var OrderModel = _scScreenUtils.getTargetModel(this, "getFulfillmentSummaryDetails_input");
		var SourceModel = _scScreenUtils.getModel(this, "getFulfillmentSummaryDetails_output");
           var OrderLineList=_scModelUtils.getModelListFromPath("Order.OrderLines.OrderLine", OrderModel);
	    var ShippingGroupsList=_scModelUtils.getModelListFromPath("Order.ShippingGroups.ShippingGroup", SourceModel);
        var DeleteCount=0;
        var GiftWrappedItems=0;
        var GiftBoxesCount=0;
        var QuantitytobeDeleted=0;
		var GiftBoxtobeDeleted=0;
        var GiftBoxObjects=_scBaseUtils.getNewArrayInstance();
		for(var m in OrderLineList)
		{
	        var OrderLineElement = OrderLineList[m];
			for(var k in OrderLineElement)
			{	
				var TargetLine=OrderLineElement[k];
				var Action=_scModelUtils.getStringValueFromPath("Action", TargetLine);
				var GiftWrap=_scModelUtils.getStringValueFromPath("GiftWrap", TargetLine);
                var ItemID=_scModelUtils.getStringValueFromPath("Item.ItemID", TargetLine);
                if(_scBaseUtils.stringEquals(GiftWrap,"Y") && !(_scBaseUtils.stringEquals(ItemID,GiftBoxID)))
                {
                   GiftWrappedItems=parseInt(GiftWrappedItems)+1;
                }
                if(_scBaseUtils.stringEquals(ItemID,GiftBoxID))
                {
                    GiftBoxesCount=parseInt(GiftBoxesCount)+1;
                    _scBaseUtils.appendToArray(GiftBoxObjects,TargetLine);
                }
                if(_scBaseUtils.stringEquals(Action,"REMOVE") && _scBaseUtils.stringEquals(GiftWrap,"Y"))
                {
                    DeleteCount=parseInt(DeleteCount)+1;
		  }
  		    }
        }    
			if(GiftWrappedItems==DeleteCount && GiftBoxesCount=="1" && DeleteCount>"0")
			{
				GiftBoxtobeDeleted=1;
         	}
			else if(GiftWrappedItems==GiftBoxesCount && DeleteCount>"0")
			{
				GiftBoxtobeDeleted=DeleteCount;
			}
		
				//var apiInput=null;
				//apiInput = _scBaseUtils.getNewModelInstance();
				//_scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", _scModelUtils.getStringValueFromPath("Order.OrderHeaderKey", OrderModel), apiInput);
				//_scModelUtils.setStringValueAtModelPath("Order.EnterpriseCode", _scModelUtils.getStringValueFromPath("Order.EnterpriseCode", OrderModel), apiInput);
				var apiInput=_scBaseUtils.getNewModelInstance();	
				for(var i=0; i<GiftBoxtobeDeleted;i++)
				{
					var GiftBoxes=GiftBoxObjects[i];
					_scModelUtils.setStringValueAtModelPath("Action","REMOVE", GiftBoxes);
					_scModelUtils.addModelObjectAsChildToModelObject(i,GiftBoxes,apiInput);
				}
				
                 return apiInput;
 	},

//OOB Overriden method

    callSaveOrder: function(
        model, mashupContext) {
//To get GiftBox Items to be deleted-Begin.
            var apiInput=this.extn_onSave();
            var OrderLineList=_scModelUtils.getModelListFromPath("", apiInput);
            var shippingModel = _scScreenUtils.getModel(this,"extn_shippingDetails");
			var actionPerformed = _scModelUtils.getStringValueFromPath("Action", mashupContext);
           
            var ModelOrderLineList=_scModelUtils.getModelListFromPath("Order.OrderLines.OrderLine", model);
         
		if(!(_scBaseUtils.isVoid(apiInput))) 
           	 {
                for(var i in ModelOrderLineList)
				{
					ModelOrderLineElement =_scModelUtils.getModelFromList(ModelOrderLineList,i);
					for(var m in OrderLineList)
				    {
				         var OrderLineElement=_scBaseUtils.getNewModelInstance();
					    OrderLineElement =_scModelUtils.getModelFromList(OrderLineList,m);
				        if(ModelOrderLineElement.OrderLineKey==OrderLineElement.OrderLineKey)
					    {
					      	_scModelUtils.setStringValueAtModelPath("Action","REMOVE", model.Order.OrderLines.OrderLine[i]);
  					    
					    }
					  else{ 
					    _scBaseUtils.appendToArray(model.Order.OrderLines.OrderLine,OrderLineElement);
						}
				    }
				}
           	 }
//end of code
            
           var pickupModel = _scScreenUtils.getModel(this, "getFulfillmentSummaryDetails_output");
           var pickpupGroupList = _scModelUtils.getModelListFromPath("Order.PickupGroups.PickupGroup",pickupModel);
            var initialInputData = _scScreenUtils.getInitialInputData(this);
          if(!_scBaseUtils.isVoid(pickpupGroupList))
          {
	     for(var p in pickpupGroupList )
         {
                var shipNodePersonInfo = _scModelUtils.getStringValueFromPath("Shipnode.ShipNodePersonInfo", pickpupGroupList[p]);
                var customerFirstName = _scModelUtils.getStringValueFromPath("Order.CustomerFirstName", initialInputData);
                var customerLastName  = _scModelUtils.getStringValueFromPath("Order.CustomerLastName", initialInputData);
                _scModelUtils.setStringValueAtModelPath("FirstName",customerFirstName, shipNodePersonInfo);
                _scModelUtils.setStringValueAtModelPath("LastName",customerLastName, shipNodePersonInfo);
                _scModelUtils.setStringValueAtModelPath("Order.PersonInfoShipTo",shipNodePersonInfo, model);
                _scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge.ChargeAmount","0.00",model);
               _scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge.Extn.ExtnOriginalAmount","0.00",model);
               _scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge.ChargeCategory","SHIPPINGCHARGE",model);
               _scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge.ChargeName","SHIPPINGCHARGE",model);
            //  _scModelUtils.setStringValueAtModelPath("PersonInfoShipTo",shipNodePersonInfo, model.Order);
              var pickupOrderLineList = _scModelUtils.getModelListFromPath("Page.Output.OrderLines.OrderLine",pickpupGroupList[p]);
              for(var index in pickupOrderLineList)
              {
              var orderLineKey =  _scModelUtils.getStringValueFromPath("OrderLineKey", pickupOrderLineList[index]);
             for(var i in ModelOrderLineList)
				{
				  if(ModelOrderLineList[i].OrderLineKey == orderLineKey)
				  {
				      _scModelUtils.setStringValueAtModelPath("PersonInfoShipTo",shipNodePersonInfo, model.Order.OrderLines.OrderLine[i]);
				  }
                 }
                }
             }   
         } 
         
           var shippingGroupList = _scModelUtils.getModelListFromPath("Order.ShippingGroups.ShippingGroup",pickupModel);
		if(actionPerformed != "NEXT")
	    {
         if(!_scBaseUtils.isVoid(shippingGroupList ))
          {
	     for(var s in shippingGroupList)
         {
             var shippingOrderLineList = _scModelUtils.getModelListFromPath("Page.Output.OrderLines.OrderLine",shippingGroupList[s]); 
             for(var index in shippingOrderLineList)
             {
               var orderLineKey =  _scModelUtils.getStringValueFromPath("OrderLineKey", shippingOrderLineList[index]);  
               var shipTogetherNo =  _scModelUtils.getStringValueFromPath("ShipTogetherNo", shippingOrderLineList[index]); 
               var productLine = _scModelUtils.getStringValueFromPath("Item.ProductLine", shippingOrderLineList[index]);
                for(var i in ModelOrderLineList)
				{
				  if(ModelOrderLineList[i].OrderLineKey == orderLineKey)
				  {
				      if((shipTogetherNo =="GWP_OrderLine") || (shipTogetherNo =="GWP_Order")|| (productLine=="3"))
				      {
				      _scModelUtils.setStringValueAtModelPath("CarrierServiceCode","", model.Order.OrderLines.OrderLine[i]);
				      }
				  }
                 }
             }
         }
         }
	}

            var action = null;
            if (!(
            _scBaseUtils.isVoid(
            mashupContext))) {
                action = _scBaseUtils.getStringValueFromBean("Action", mashupContext);
            } else {
                mashupContext = _scControllerUtils.getMashupContext(
                this);
            }
            var conditionsArray = null;
            conditionsArray = _scBaseUtils.getNewArrayInstance();
            _scBaseUtils.appendToArray(
            conditionsArray, _scBaseUtils.equals(
            action, "ADDTOORDER"));
            _scBaseUtils.appendToArray(
            conditionsArray, _scBaseUtils.equals(
            action, "GOTOGIFTOPTIONS"));
            _scBaseUtils.appendToArray(
            conditionsArray, _scBaseUtils.equals(
            action, "SAVEONPREVIOUS"));
            _scBaseUtils.appendToArray(
            conditionsArray, _scBaseUtils.equals(
            action, "PREVIOUS"));
            _scBaseUtils.appendToArray(
            conditionsArray, _scBaseUtils.equals(
            action, "CLOSE"));
            var modelList = null;
            var mashupRefList = null;
            if (
            _isccsUIUtils.or(
            conditionsArray)) {
                _isccsUIUtils.callApi(
                this, model, "modifyFulfillmentOptions", mashupContext);
            } else if (
            _scBaseUtils.equals(
            action, "NEXT")) {
                if (
                _scBaseUtils.equals(
                this.YCDReservationRule, "Y")) {
                    this.Action = "NEXT";
                    modelList = _scBaseUtils.getNewArrayInstance();
                    mashupRefList = _scBaseUtils.getNewArrayInstance();
                   // custom Code for Node Capacity for Pickup Order
                    var orderLineList = _scModelUtils.getStringValueFromPath("Order.OrderLines.OrderLine", model);
                    for(var index in orderLineList )
                     {
                      var deliveryMethod = _scModelUtils.getStringValueFromPath("DeliveryMethod", orderLineList[index]);
                      if(deliveryMethod  == "PICK")
                       {
                        var currentDate = _scBaseUtils.getDate();
                        var orderDate = _scModelUtils.getStringValueFromPath("OrderDates.OrderDate", orderLineList[index]);
                         for(var i in orderDate)
                         {
                              var actualDate = _scModelUtils.getStringValueFromPath("ActualDate", orderDate[i]);
                              // var pickDate = _scBaseUtils.formatDateToUserFormat(actualDate);
                              _scModelUtils.setStringValueAtModelPath("Extn.ExtnPickupDate", actualDate,orderLineList[index]);
                              var newActualDate = _scBaseUtils.convertToServerFormat(currentDate);
                              _scModelUtils.setStringValueAtModelPath("ActualDate", newActualDate, orderDate[i]);
                              _scModelUtils.setStringValueAtModelPath("ReqShipDate", newActualDate, orderLineList[index]);
                             _scModelUtils.setStringValueAtModelPath("LineCharges.LineCharge.ChargePerLine","0.00",orderLineList[index]);
                             _scModelUtils.setStringValueAtModelPath("LineCharges.LineCharge.ChargeCategory","SHIPPINGSURCHARGE",orderLineList[index]);
                            _scModelUtils.setStringValueAtModelPath("LineCharges.LineCharge.ChargeName","SHIPPINGSURCHARGE",orderLineList[index]);
                         }
                       }
                      } 
                                                
                         // custom code ends here
                    _scBaseUtils.appendToArray(
                    modelList, model);
                    _scBaseUtils.appendToArray(
                    mashupRefList, "modifyFulfillmentOptions");
                    _scBaseUtils.appendToArray(
                    modelList, _scBaseUtils.getTargetModel(
                    this, "reserveOrder_input", null));
                    _scBaseUtils.appendToArray(
                    mashupRefList, "reserveOrderConditionalCall");
                    var options = null;
                    options = _scBaseUtils.getNewBeanInstance();
                    _scBaseUtils.setAttributeValue("aggregate", false, options);
                    _isccsUIUtils.callApis(
                    this, modelList, mashupRefList, mashupContext, options);
                } else {
                    if (
                    _scWidgetUtils.isWidgetDisabled(
                    this, "update_order")) {
                        var isLargeOrder = null;
                        isLargeOrder = _scBaseUtils.getAttributeValue("Order.IsLargeOrder", false, _scScreenUtils.getModel(
                        this, "getFulfillmentSummaryDetails_output"));
                        if (
                        _scBaseUtils.and(
                        _scBaseUtils.negateBoolean(
                        _scBaseUtils.isVoid(
                        isLargeOrder)), _scBaseUtils.negateBoolean(
                        _scBaseUtils.equals(
                        isLargeOrder, "Y")))) {
                            var ftcDateInput = null;
                            ftcDateInput = _scBaseUtils.getTargetModel(
                            this, "getFulfillmentSummaryDetails_input", null);
                            var massageFTCModel = null;
                            massageFTCModel = _isccsOrderUtils.prepareModelForFtcInput(
                            this, ftcDateInput, null);
                            if (!(
                            _scBaseUtils.isVoid(
                            massageFTCModel))) {
                                var updateOrderModelList = null;
                                updateOrderModelList = _scBaseUtils.getNewArrayInstance();
                                var mashupRefObj = null;
                                mashupRefObj = _scBaseUtils.getNewModelInstance();
                                _scModelUtils.addStringValueToModelObject("mashupRefId", "modifyFulfillmentOptions", mashupRefObj);
                                _scModelUtils.addStringValueToModelObject("mashupInput", massageFTCModel, mashupRefObj);
                                _scBaseUtils.appendToArray(
                                updateOrderModelList, mashupRefObj);
                                var screenId = null;
                                screenId = _scBaseUtils.getValueFromPath("declaredClass", this);
                                var args = null;
                                args = _isccsUIUtils.formatArgForUpdateAndNextCall(
                                screenId, updateOrderModelList);
                                _scEventUtils.fireEventToParent(
                                this, "combinedAPICallOnNext", args);
                            } else {
                                _scEventUtils.fireEventToParent(
                                this, "onSaveSuccess", null);
                            }
                        } else {
                            _scEventUtils.fireEventToParent(
                            this, "onSaveSuccess", null);
                        }
                    } else {
                        var updateOrderModelList = null;
                        updateOrderModelList = _scBaseUtils.getNewArrayInstance();
                        var mashupRefObj = null;
                        mashupRefObj = _scBaseUtils.getNewModelInstance();
                        _scModelUtils.addStringValueToModelObject("mashupRefId", "modifyFulfillmentOptions", mashupRefObj);
                        _scModelUtils.addStringValueToModelObject("mashupInput", model, mashupRefObj);
                        _scBaseUtils.appendToArray(
                        updateOrderModelList, mashupRefObj);
                        var screenId = null;
                        screenId = _scBaseUtils.getValueFromPath("declaredClass", this);
                        var args = null;
                        args = _isccsUIUtils.formatArgForUpdateAndNextCall(
                        screenId, updateOrderModelList);
                        _scEventUtils.fireEventToParent(
                        this, "combinedAPICallOnNext", args);
                    }
                }
            } else {
                modelList = _scBaseUtils.getNewArrayInstance();
                mashupRefList = _scBaseUtils.getNewArrayInstance();
                _scBaseUtils.appendToArray(
                modelList, model);
                _scBaseUtils.appendToArray(
                mashupRefList, "modifyFulfillmentOptions");
                _scBaseUtils.appendToArray(
                modelList, _scBaseUtils.getTargetModel(
                this, "behavior_getFulfillmentSummaryDetails_input", null));
                _scBaseUtils.appendToArray(
                mashupRefList, "postCall_getFulfillmentSummaryDetails");
                _scControllerUtils.setMashupContextIdenitier(
                mashupContext, "OrderUpdated");
                var options = null;
                options = _scBaseUtils.getNewBeanInstance();
                _scBaseUtils.setAttributeValue("aggregate", false, options);
                _isccsUIUtils.callApis(
                this, modelList, mashupRefList, mashupContext, options);
            }
        },
	extn_ShippingChargeCall: function(event, bEvent, ctrl, args)
	{
        var orderModel = _scBaseUtils.getTargetModel(this,"getFulfillmentSummaryDetails_input");
        var model = _scScreenUtils.getModel(this, "getFulfillmentSummaryDetails_output");
        orderLines = _scModelUtils.getModelListFromPath("Order.ShippingGroups.ShippingGroup",model);
        var inputModel = _scBaseUtils.getNewModelInstance();
        var orderLineArray = _scBaseUtils.getNewArrayInstance();
        var carrierService = "";
        var check = false;
        var deliveryMethod ="";
        var shipNode = "";
        var pickDate = "";
	    var shippingGroupList =_scModelUtils.getModelListFromPath("Order.ShippingGroups.ShippingGroup",model);
	    var orderLinesList= _scModelUtils.getModelListFromPath("Order.OrderLines.OrderLine",orderModel);
	    var actionPerformed = _scModelUtils.getStringValueFromPath("Action",args);
	    if(actionPerformed == "NEXT")
         {
	     for(var p in orderLinesList)
            {
              var pickupOrderLines =  _scModelUtils.getModelListFromPath("",orderLinesList[p]);
               
              for(var index in pickupOrderLines)
              {
                 var action = _scModelUtils.getStringValueFromPath("Action",pickupOrderLines[index]);
                 var currentDeliveryMethod = _scModelUtils.getStringValueFromPath("DeliveryMethod",pickupOrderLines[index]);
                  if(action != "REMOVE")
                 {
                    if(_scBaseUtils.isVoid(deliveryMethod))
                     {
                          deliveryMethod= _scModelUtils.getStringValueFromPath("DeliveryMethod",pickupOrderLines[index]);
                     }
                    if(_scBaseUtils.isVoid(shipNode))
                    {
                      shipNode= _scModelUtils.getStringValueFromPath("ShipNode",pickupOrderLines[index]);
                    }
                    if(_scBaseUtils.isVoid(pickDate))
                    {
                       pickDate = _scModelUtils.getStringValueFromPath("ReqShipDate",pickupOrderLines[index]);
                    }
                     if(deliveryMethod != currentDeliveryMethod)
                     {
				var sMessage = null;
                		sMessage = _scScreenUtils.getString(
               		 this, "extn_Order_Will_Not_Proceed");
				_isccsBaseTemplateUtils.showMessage(this,sMessage,"error");

                     // _isccsBaseTemplateUtils.showMessage(this,"Order will not proceeded with both Shipping and Pick up OrderLines","error");
		               _scEventUtils.stopEvent(bEvent);
					   return true;
		       }
		    else if(currentDeliveryMethod == "PICK")  
                    {
                      var currentShipNode = _scModelUtils.getStringValueFromPath("ShipNode",pickupOrderLines[index]);
                      var currentPickDate = _scModelUtils.getStringValueFromPath("ReqShipDate",pickupOrderLines[index]);
                     
                     if(shipNode != currentShipNode)
                     {
				var sMessage = null;
                		sMessage = _scScreenUtils.getString(
               		 this, "extn_Please_Choose_Same_Pick_Up_Store");
				_isccsBaseTemplateUtils.showMessage(this,sMessage,"error");
                       // _isccsBaseTemplateUtils.showMessage(this,"Please choose same Pick up Store for each OrderLine","error");
		               _scEventUtils.stopEvent(bEvent);
					    return true;
                     }
                     else if (_scBaseUtils.isVoid(currentPickDate))
                     {
				var sMessage = null;
                		sMessage = _scScreenUtils.getString(
               		 this, "extn_Please_Select_Pick_Up_Date");
				_isccsBaseTemplateUtils.showMessage(this,sMessage,"error");

				
                       // _isccsBaseTemplateUtils.showMessage(this,"Please select Pick up Date for each OrderLine","error");
		               _scEventUtils.stopEvent(bEvent);
					   return true;
                     }
                    else if(pickDate != currentPickDate )
                     {
				var sMessage = null;
                		sMessage = _scScreenUtils.getString(
               		 this, "extn_Please_Choose_Same_Pick_Up_Date");
				_isccsBaseTemplateUtils.showMessage(this,sMessage,"error");

                       // _isccsBaseTemplateUtils.showMessage(this,"Please choose same Pick up Date for each OrderLine","error");
		               _scEventUtils.stopEvent(bEvent);
					    return true;
                     }
                    }
					  
                 }

                 }
             }   
         	
         
         for(var j in shippingGroupList)
         {
          var shippingMethod = _scModelUtils.getStringValueFromPath("DeliveryMethod", shippingGroupList[j]);
          if(shippingMethod  == "SHP")
          {
            for(var index in orderLines)
           {
              
            var orderLineList = _scModelUtils.getModelListFromPath("Page.Output.OrderLines.OrderLine",orderLines[index]);
             for(var i in orderLineList )
           {
            var shipTogetherNo= _scModelUtils.getStringValueFromPath("ShipTogetherNo", orderLineList[i]);
            var productLine =  _scModelUtils.getStringValueFromPath("Item.ProductLine", orderLineList[i]);
            
	     if((productLine != "3") && (!_scBaseUtils.isVoid(productLine)))
	     {
            if((shipTogetherNo=="") && (!_scBaseUtils.equals(shipTogetherNo,"GWP_Order")) && (!_scBaseUtils.equals(shipTogetherNo,"GWP_OrderLine")))
            {
               if(_scBaseUtils.isVoid(carrierService))
                 {
                 carrierService =  _scModelUtils.getStringValueFromPath("CarrierServiceCode", orderLineList[i]);
                 }
			 var scarrierServiceObj = _scModelUtils.getStringValueFromPath("CarrierServiceCode",orderLineList[i]); 
			 for(var j in orderLinesList)
			 {
			     var orderLinesObj=  _scModelUtils.getModelListFromPath("",orderLinesList[j]);
			     for(var k in orderLinesObj)
			     {
			     var orderLineKey =  _scModelUtils.getStringValueFromPath("OrderLineKey",orderLinesObj[k]); 
			     if(orderLineKey == orderLineList[i].OrderLineKey)
			     {
			         var actionSelected = _scModelUtils.getStringValueFromPath("Action",orderLinesObj[k]);
			         if(!_scBaseUtils.equals(actionSelected,"REMOVE"))
			          {
                      if(scarrierServiceObj== "") 
                      {
				var sMessage = null;
                		sMessage = _scScreenUtils.getString(
               		 this, "extn_Please_Select_Shipping_Method");
				_isccsBaseTemplateUtils.showMessage(this,sMessage,"error");
		
                       	// _isccsBaseTemplateUtils.showMessage(this,"Please select Shipping Method for each OrderLine","error");
		                _scEventUtils.stopEvent(bEvent);
                         check = true;
                         return true;
                       }
                       else
                       {
                	 var currentCarrierService = _scModelUtils.getStringValueFromPath("CarrierServiceCode",orderLineList[i]);
                        if(carrierService != currentCarrierService )
                         {
				var sMessage = null;
                		sMessage = _scScreenUtils.getString(
               		 this, "extn_Please_Choose_Same_Shipping_Method");
				_isccsBaseTemplateUtils.showMessage(this,sMessage,"error");	

                             // _isccsBaseTemplateUtils.showMessage(this,"Please choose same Shipping Method for each OrderLine","error");
		               _scEventUtils.stopEvent(bEvent);
                             check = true;
                             return true;  
                          }
                        }
		         }
			     }
			     }
			 }
		}
              }
             }
            }
           }
         }
         }
         var actionPerformed = null;
          actionPerformed = _scModelUtils.getStringValueFromPath("Action", args);
           if (
            _scBaseUtils.equals(
            actionPerformed, "NEXT")) {
        if(!_scBaseUtils.isVoid(shippingGroupList))
         {
       if(check == false)
       {
          //var shippingMethod = _scModelUtils.getStringValueFromPath("DeliveryMethod", shippingGroupList[0]);
         
         _scEventUtils.stopEvent(bEvent);
        for(var i in orderLines)
		{
		    var deliveryMethod =  _scModelUtils.getStringValueFromPath("DeliveryMethod",orderLines[i]);
            if(deliveryMethod  == "SHP")
            {
           var orderLineList = _scModelUtils.getModelListFromPath("Page.Output.OrderLines.OrderLine",orderLines[i]);
           var screenModel = _scScreenUtils.getInitialInputData(this);
           var enterpriseCode= _scModelUtils.getStringValueFromPath("Order.EnterpriseCode",screenModel);
           _scModelUtils.setStringValueAtModelPath("Order.EnterpriseCode",enterpriseCode,inputModel);
           var orderHeaderKey = _scModelUtils.getStringValueFromPath("Order.OrderHeaderKey",screenModel); 
           _scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey",orderHeaderKey,screenModel);
           var personInfoShipTo=  _scModelUtils.getModelObjectFromPath("Order.PersonInfoShipTo", screenModel);
            for(var index in orderLineList)
             {    
               var orderLineKey= _scModelUtils.getStringValueFromPath("OrderLineKey",orderLineList[index]);
               for(var p in orderLinesList)
                {
                  var orderLinesObj=  _scModelUtils.getModelListFromPath("",orderLinesList[p]);
			     for(var k in orderLinesObj)
			     {
			     var tOrderLineKey =  _scModelUtils.getStringValueFromPath("OrderLineKey",orderLinesObj[k]); 
			     if(orderLineKey == tOrderLineKey)
			     {
			        var actionSelected = _scModelUtils.getStringValueFromPath("Action",orderLinesObj[k]);
			     if(!_scBaseUtils.equals(actionSelected,"REMOVE"))
			     {
               var orderLineModel = _scBaseUtils.getNewModelInstance();
               _scModelUtils.setStringValueAtModelPath("OrderLineKey",orderLineKey,orderLineModel);
               var carrierService = _scModelUtils.getStringValueFromPath("CarrierServiceCode",orderLineList[index]);
               _scModelUtils.setStringValueAtModelPath("CarrierServiceCode",carrierService,orderLineModel);
               var shipTogetherNo = _scModelUtils.getStringValueFromPath("ShipTogetherNo",orderLineList[index]);
               if(!_scBaseUtils.isVoid(shipTogetherNo))
                 {
                     continue;
                 }
               _scModelUtils.setStringValueAtModelPath("ShipTogetherNo",shipTogetherNo,orderLineModel);
               
               var qty= _scModelUtils.getStringValueFromPath("OrderedQty",orderLineList[index]);
               _scModelUtils.setStringValueAtModelPath("OrderedQty",qty,orderLineModel);
               var shipToKey = _scModelUtils.getStringValueFromPath("ShipToKey",orderLineList[index]); 
                if(_scBaseUtils.isVoid(shipToKey))
                 {
                    var shipToKeyOrder= _scModelUtils.getStringValueFromPath("Order.ShipToKey",model);
                    _scModelUtils.setStringValueAtModelPath("ShipToKey",shipToKeyOrder,orderLineModel);
                 }
                else
                 {
                    _scModelUtils.setStringValueAtModelPath("ShipToKey",shipToKey,orderLineModel);
                 }
              var item = _scModelUtils.getModelObjectFromPath("Item", orderLineList[index]);
              var itemId = _scModelUtils.getStringValueFromPath("ItemID",item);
              var productLine = _scModelUtils.getStringValueFromPath("ProductLine",item);
              _scModelUtils.setStringValueAtModelPath("Item.ItemID",itemId,orderLineModel);
              _scModelUtils.setStringValueAtModelPath("Item.ProductLine",productLine,orderLineModel);
              var linePriceModel= _scModelUtils.getModelObjectFromPath("LinePriceInfo", orderLineList[index]);
              var displayUnitPrice = _scModelUtils.getStringValueFromPath("DisplayUnitPrice",linePriceModel);
              var unitPrice = _scModelUtils.getStringValueFromPath("UnitPrice",linePriceModel);
              _scModelUtils.setStringValueAtModelPath("LinePriceInfo.DisplayUnitPrice",displayUnitPrice,orderLineModel);
              _scModelUtils.setStringValueAtModelPath("LinePriceInfo.UnitPrice",unitPrice,orderLineModel);
              var extnModel = _scModelUtils.getModelObjectFromPath("ItemDetails.Extn", orderLineList[index]);
               _scModelUtils.setStringValueAtModelPath("ItemDetails.Extn",extnModel,orderLineModel);
              var primaryInfoModel =  _scModelUtils.getModelObjectFromPath("ItemDetails.PrimaryInformation", orderLineList[index]);
              var isShippingAllowed = _scModelUtils.getStringValueFromPath("IsShippingAllowed",primaryInfoModel);
              var isAirShippingAllowed= _scModelUtils.getStringValueFromPath("IsAirShippingAllowed",primaryInfoModel);
              _scModelUtils.setStringValueAtModelPath("ItemDetails.PrimaryInformation.IsShippingAllowed",isShippingAllowed,orderLineModel);
              _scModelUtils.setStringValueAtModelPath("ItemDetails.PrimaryInformation.IsAirShippingAllowed",isAirShippingAllowed,orderLineModel);
              var personInfoShipToOrderLine =  _scModelUtils.getModelObjectFromPath("PersonInfoShipTo", orderLineList[index]);
                if(_scBaseUtils.isVoid(personInfoShipToOrderLine))
                 {
                  _scModelUtils.setStringValueAtModelPath("PersonInfoShipTo",personInfoShipTo,orderLineModel);
                 }
                else
                {
                  _scModelUtils.setStringValueAtModelPath("PersonInfoShipTo",personInfoShipToOrderLine,orderLineModel);
                }
              _scBaseUtils.appendToArray(orderLineArray,orderLineModel);
              if(index ==orderLineList.length-1)
                {
                  break;
                } 
             }
           }
          }
          }
             }
            }
          }
        }
        _scModelUtils.setStringValueAtModelPath("Order.OrderLines.OrderLine",orderLineArray,inputModel);
	      var mashupCurrentContext = _scControllerUtils.getMashupContext(this);
            _isccsUIUtils.callApi(this,inputModel,"extn_ShippingChargesCall",args);
       }
       }
	},

 extn_shippingChargeCalculation: function(event, bEvent, ctrl, args)
	{
          var  ShippingCharges= null;
          var shippingChargeModel =null;
          var shippingAmount = 0.0;
          var output= _scModelUtils.getModelObjectFromPath("mashupArray",args);
	   for(var index in output)
            {
              if(output[index].mashupRefId == "extn_ShippingChargesCall")
                {
                  ShippingCharges = _scModelUtils.getModelObjectFromPath("mashupRefOutput",output[index]);
		     shippingChargeModel = _scModelUtils.getModelListFromPath("basket.ResponseLineItem" ,ShippingCharges);
               // var extn_shippingSelected = _scBaseUtils.getNewModelInstance();
                  var orderLineArray = _scBaseUtils.getNewArrayInstance();
                  var checkForShippingList = null;
		     if(!_scBaseUtils.isVoid(shippingChargeModel))
		         {
		           checkForShippingList = shippingChargeModel.length;
		           if(!_scBaseUtils.isVoid(checkForShippingList )){
				   for(var i in shippingChargeModel)
				   {
				       var amt = _scModelUtils.getNumberValueFromPath("eligibleShipModes.ShippingMode.ShippingCharge.shippingAmount",shippingChargeModel[i]);
				       var eachLineModel = _scBaseUtils.getNewModelInstance();
				       shippingAmount = shippingAmount + amt;
				        var lineId =  _scModelUtils.getStringValueFromPath("lineItemID",shippingChargeModel[i]);
					 var surcharge = _scModelUtils.getStringValueFromPath("shippingSurchargeAmount",shippingChargeModel[i]);
					 _scModelUtils.setStringValueAtModelPath("shippingSurchargeAmount",surcharge,eachLineModel);
					 _scModelUtils.setStringValueAtModelPath("lineId",lineId,eachLineModel);
					 _scBaseUtils.appendToArray(orderLineArray,eachLineModel);
				   }
				  }
				   else
				   {
				       shippingAmount = _scModelUtils.getStringValueFromPath("eligibleShipModes.ShippingMode.ShippingCharge.shippingAmount",shippingChargeModel);
					   var eachLineModel = _scBaseUtils.getNewModelInstance();
					   var lineId = _scModelUtils.getStringValueFromPath("lineItemID",shippingChargeModel);
				          var surcharge =_scModelUtils.getStringValueFromPath("shippingSurchargeAmount",shippingChargeModel);
					   _scModelUtils.setStringValueAtModelPath("shippingSurchargeAmount",surcharge,eachLineModel);
					   _scModelUtils.setStringValueAtModelPath("lineId",lineId,eachLineModel);
					   _scBaseUtils.appendToArray(orderLineArray,eachLineModel);
				    }
                    
                             var shippingDetailsModel = _scBaseUtils.getNewModelInstance();
                            if(shippingAmount == 0)
                            {
                                shippingAmount = "0.00";
                            }
                              shippingAmount = shippingAmount.toString();
                              var shippingCharge = shippingAmount.split(".");
                              if(shippingCharge[1].length>2)
                              {
                                  var charge = shippingCharge[1].split("");
                                  shippingAmount= shippingCharge[0]+"." + charge[0]+charge[1];
                                  
                              }
                             _scModelUtils.setStringValueAtModelPath("shippingLines",orderLineArray,shippingDetailsModel);
                             _scModelUtils.setStringValueAtModelPath("shippingAmount",shippingAmount.toString(),shippingDetailsModel);
                             _scScreenUtils.setModel(this, "extn_shippingDetails",shippingDetailsModel, null);
                             var msg = null;
                             var inputModel = args.inputData[0];
                             var sEnterpriseCode = inputModel .mashupInputObject.Order.EnterpriseCode;
                             var sCarrierServiceCode = inputModel .mashupInputObject.Order.OrderLines.OrderLine[0].CarrierServiceCode;
				
                		 msg = _scScreenUtils.getString(
               		 this, "extn_Total_Shipping_Charge");			 
                             
                             
                             if(sEnterpriseCode == "BAY")
                             {
                                 msg += "CA$" + shippingAmount;
                             }
                             if(sEnterpriseCode == "LT")
                             {
                                 msg += "$" + shippingAmount;
                             }
		               _scScreenUtils.showConfirmMessageBox(this, msg, "extn_ShippingChargesConfirmation", null, args);

			        }
			       else if(_scBaseUtils.isVoid(ShippingCharges.basket))
			        {
			            var actionPerformed = null;
                       actionPerformed = _scModelUtils.getStringValueFromPath("mashupContext.Action", args);
            
                   if (_scBaseUtils.equals(actionPerformed, "NEXT")) {
                if (
                _scBaseUtils.negateBoolean(
                _scBaseUtils.or(
                this.isUnavailbleShpVisible, _scBaseUtils.or(
                this.isUnavailblePickVisible, this.isPickWithoutNodeVisible)))) {
                    if (!(
                    _scWidgetUtils.isWidgetVisible(
                    this, "emptyOlMessage"))) {
                        var mashupContext2 = null;
                        mashupContext2 = _scControllerUtils.getMashupContext(
                        this);
                        _scBaseUtils.addStringValueToBean("Action", "NEXT", mashupContext2);
                        this.Action = "NEXT";
                        var targetModel = null;
                        targetModel = _scBaseUtils.getTargetModel(
                        this, "getFulfillmentSummaryDetails_input", null);
                        var optimizationType = null;
                        var optimizationTypeOutput = null;
                        optimizationTypeOutput = _scScreenUtils.getModel(
                        this, "optimizationTypeOutput");
                        if (!(
                        _scBaseUtils.isVoid(
                        optimizationTypeOutput))) {
                            optimizationType = _scModelUtils.getStringValueFromPath("Order.OptimizationType", optimizationTypeOutput);
                            _scModelUtils.setStringValueAtModelPath("Order.OptimizationType", optimizationType, targetModel);
                        }
                        var targetModel1 = null;
                        targetModel1 = _isccsOrderUtils.prepareModel(
                        this, targetModel, mashupContext2);
                        var options = null;
                        options = _scBaseUtils.getNewBeanInstance();
                        _scBaseUtils.addStringValueToBean("modified", true, options);
                        _scBaseUtils.addStringValueToBean("deleted", true, options);
                        _scBaseUtils.addStringValueToBean("allowEmpty", true, options);
                        var addedModifiedtargetModel = null;
                        addedModifiedtargetModel = _scBaseUtils.getTargetModel(
                        this, "getFulfillmentSummaryDetails_input", options);
                        var modelForMFO = null;
                        modelForMFO = _isccsOrderUtils.prepareModel(
                        this, addedModifiedtargetModel, mashupContext2);
                        var orderlines = null;
                        orderlines = _scModelUtils.getModelListFromPath("Order.OrderLines.OrderLine", modelForMFO);
                        var length = null;
                        length = _scBaseUtils.getAttributeCount(
                        orderlines);
                        var orderLineListArray = null;
                        orderLineListArray = _scModelUtils.getModelListFromPath("Order.OrderLines.OrderLine", targetModel1);
                        if (!(
                        _scBaseUtils.isVoid(
                        orderLineListArray))) {
                            for (
                            var i = 0;
                            _scBaseUtils.lessThan(
                            i, length);
                            i = _scBaseUtils.incrementNumber(
                            i, 1)) {
                                var orderLine = null;
                                orderLine = _scBaseUtils.getArrayBeanItemByIndex(
                                orderlines, i);
                                _scBaseUtils.appendToArray(
                                _scModelUtils.getModelListFromPath("Order.OrderLines.OrderLine", targetModel1), orderLine);
                            }
                        } else {
                            _scModelUtils.addListToModelPath("Order.OrderLines.OrderLine", orderlines, targetModel1);
                        }
                        // Stamping of Header Charges to Order
                       _scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge.ChargeAmount","0.00",targetModel1);
                       _scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge.Extn.ExtnOriginalAmount","0.00",targetModel1);
                       _scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge.ChargeCategory","SHIPPINGCHARGE",targetModel1);
                       _scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge.ChargeName","SHIPPINGCHARGE",targetModel1); 
                       // Custom Code Ends
                        this.callSaveOrder(
                        targetModel1, mashupContext2);
                    } else {
                        _isccsBaseTemplateUtils.showMessage(
                        this, "cancelledLines", "error", null);
                    }
                } else {
                    _isccsBaseTemplateUtils.showMessage(
                    this, "unavailableLines", "error", null);
                }
            }
            if (
            _scBaseUtils.or(
            _scBaseUtils.equals(
            actionPerformed, "SAVEONPREVIOUS"), _scBaseUtils.equals(
            actionPerformed, "ADDTOORDER"))) {
                if (
                isPageDirty = _isccsWidgetUtils.isDirty(
                this)) {
                    var mashupContext3 = null;
                    mashupContext3 = _scControllerUtils.getMashupContext(
                    this);
                    if (
                    _scBaseUtils.equals(
                    actionPerformed, "ADDTOORDER")) {
                        _scBaseUtils.addStringValueToBean("Action", "ADDTOORDER", mashupContext3);
                        this.Action = "ADDTOORDER";
                    } else {
                        _scBaseUtils.addStringValueToBean("Action", "SAVEONPREVIOUS", mashupContext3);
                        this.Action = "SAVEONPREVIOUS";
                    }
                    var args2 = null;
                    args2 = _scBaseUtils.getNewBeanInstance();
                    _scBaseUtils.addBeanValueToBean("mashupContext", mashupContext3, args2);
                    this.updateOrder(
                    null, null, null, args2);
                } else {
                    _scEventUtils.fireEventToParent(
                    this, "onSaveSuccess", null);
                }
                 }
			 }
			  else 
                         {
				var sMessage = null;
                		sMessage = _scScreenUtils.getString(
               		 this, "extn_Not_Able_To_Connect");
				_isccsBaseTemplateUtils.showMessage(this,sMessage,"error");


                          //_isccsBaseTemplateUtils.showMessage(this,"Not able to connect to Shipping Charge Service", "error", null);
                        }
                     }
              }
       } 
 
,

extn_ShippingChargesConfirmation:function(res, args) {
    
	  if (_scBaseUtils.equals(res, "Ok"))
            {
		var lineChargesModel =  _scScreenUtils.getModel(this, "extn_shippingDetails");
		var orderLineArray= _scModelUtils.getModelListFromPath("shippingLines",lineChargesModel);
		var shippingAmount =_scModelUtils.getStringValueFromPath("shippingAmount",lineChargesModel);
		var inputModel = _scBaseUtils.getNewModelInstance();
		var  orderLineChargeArray = _scBaseUtils.getNewArrayInstance();
		var model = _scScreenUtils.getModel(this, "getFulfillmentSummaryDetails_output");
		var carrierServiceCode= null;
		orderLines = _scModelUtils.getModelListFromPath("Order.ShippingGroups.ShippingGroup",model);
		for(var i in orderLines)
		   {
				var serviceCode= _scModelUtils.getStringValueFromPath("CarrierServiceCode",orderLines[i]);
		    	var parentLine = null;
                var orderLineList = _scModelUtils.getModelListFromPath("Page.Output.OrderLines.OrderLine",orderLines[i]);
                  
		     for(var index in orderLineList)
			{
			   var childLineRelationShip = _scModelUtils.getStringValueFromPath("ChildOrderLineRelationships.OrderLineRelationship",orderLineList[index]);
			   if(!(_scBaseUtils.isVoid(childLineRelationShip)))
		       {
		          parentLine =childLineRelationShip;
		          break;
		      }
			}
		    if((!(_scBaseUtils.isVoid(serviceCode))) && (!(_scBaseUtils.isVoid(parentLine))))
		 //  if(!(_scBaseUtils.isVoid(serviceCode)))
		    {
		     carrierServiceCode=serviceCode;
		     break;
		    }
		 
		}
		for(var i in orderLines)
		{
        	var orderLineList = _scModelUtils.getModelListFromPath("Page.Output.OrderLines.OrderLine",orderLines[i]);
                  
		     for(var index in orderLineList)
			{
			  var orderlineKey = _scModelUtils.getStringValueFromPath("OrderLineKey",orderLineList[index]); 
				var shipTogetherNo=_scModelUtils.getStringValueFromPath("ShipTogetherNo",orderLineList[index]);
		        var orderLineModel = _scBaseUtils.getNewModelInstance();
				if(shipTogetherNo=="GWP_OrderLine")
                {
                   _scModelUtils.setStringValueAtModelPath("CarrierServiceCode",carrierServiceCode,orderLineModel);
                    _scModelUtils.setStringValueAtModelPath("OrderLineKey",orderlineKey,orderLineModel);  
                   _scBaseUtils.appendToArray(orderLineChargeArray,orderLineModel);
                }
                for(var k in orderLineArray)
                          {
                             var lineId = _scModelUtils.getStringValueFromPath("lineId",orderLineArray[k]);
                             if(orderlineKey == lineId)
                                {
                                   var surcharge = _scModelUtils.getStringValueFromPath("shippingSurchargeAmount",orderLineArray[k]);
                                   _scModelUtils.setStringValueAtModelPath("LineCharges.LineCharge.ChargePerLine",surcharge,orderLineModel);
                                   _scModelUtils.setStringValueAtModelPath("LineCharges.LineCharge.ChargeCategory","SHIPPINGSURCHARGE",orderLineModel);
                                    _scModelUtils.setStringValueAtModelPath("LineCharges.LineCharge.ChargeName","SHIPPINGSURCHARGE",orderLineModel);
                                   _scModelUtils.setStringValueAtModelPath("OrderLineKey",orderlineKey,orderLineModel);
                                   _scBaseUtils.appendToArray(orderLineChargeArray,orderLineModel);
				     }
				 }
                         if(index == orderLineList.length-1)
                           {
                              break;
                           }
			     }
                      }
		    _scModelUtils.setStringValueAtModelPath("Order.OrderLines.OrderLine",orderLineChargeArray,inputModel);
                 var orderHeaderKey =  _scModelUtils.getStringValueFromPath("Order.OrderHeaderKey",model);
                 _scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey",orderHeaderKey,inputModel);
                 _scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge.ChargeAmount",shippingAmount,inputModel);
                 var actionPerformed = _scModelUtils.getStringValueFromPath("mashupContext.Action", args);
              if (
            _scBaseUtils.equals(
            actionPerformed, "NEXT")) {
                if (
                _scBaseUtils.negateBoolean(
                _scBaseUtils.or(
                this.isUnavailbleShpVisible, _scBaseUtils.or(
                this.isUnavailblePickVisible, this.isPickWithoutNodeVisible)))) {
                    if (!(
                    _scWidgetUtils.isWidgetVisible(
                    this, "emptyOlMessage"))) {
                        var mashupContext2 = null;
                        mashupContext2 = _scControllerUtils.getMashupContext(
                        this);
                        _scBaseUtils.addStringValueToBean("Action", "NEXT", mashupContext2);
                        this.Action = "NEXT";
                        var targetModel = null;
                        targetModel = _scBaseUtils.getTargetModel(
                        this, "getFulfillmentSummaryDetails_input", null);
                        var optimizationType = null;
                        var optimizationTypeOutput = null;
                        optimizationTypeOutput = _scScreenUtils.getModel(
                        this, "optimizationTypeOutput");
                        if (!(
                        _scBaseUtils.isVoid(
                        optimizationTypeOutput))) {
                            optimizationType = _scModelUtils.getStringValueFromPath("Order.OptimizationType", optimizationTypeOutput);
                            _scModelUtils.setStringValueAtModelPath("Order.OptimizationType", optimizationType, targetModel);
                        }
                        var targetModel1 = null;
                        targetModel1 = _isccsOrderUtils.prepareModel(
                        this, targetModel, mashupContext2);
                        var options = null;
                        options = _scBaseUtils.getNewBeanInstance();
                        _scBaseUtils.addStringValueToBean("modified", true, options);
                        _scBaseUtils.addStringValueToBean("deleted", true, options);
                        _scBaseUtils.addStringValueToBean("allowEmpty", true, options);
                        var addedModifiedtargetModel = null;
                        addedModifiedtargetModel = _scBaseUtils.getTargetModel(
                        this, "getFulfillmentSummaryDetails_input", options);
                        var modelForMFO = null;
                        modelForMFO = _isccsOrderUtils.prepareModel(
                        this, addedModifiedtargetModel, mashupContext2);
                        var orderlines = null;
                        orderlines = _scModelUtils.getModelListFromPath("Order.OrderLines.OrderLine", modelForMFO);
                        var length = null;
                        length = _scBaseUtils.getAttributeCount(
                        orderlines);
                        var orderLineListArray = null;
                        orderLineListArray = _scModelUtils.getModelListFromPath("Order.OrderLines.OrderLine", targetModel1);
                        if (!(
                        _scBaseUtils.isVoid(
                        orderLineListArray))) {
                            for (
                            var i = 0;
                            _scBaseUtils.lessThan(
                            i, length);
                            i = _scBaseUtils.incrementNumber(
                            i, 1)) {
                                var orderLine = null;
                                orderLine = _scBaseUtils.getArrayBeanItemByIndex(
                                orderlines, i);
                                _scBaseUtils.appendToArray(
                                _scModelUtils.getModelListFromPath("Order.OrderLines.OrderLine", targetModel1), orderLine);
                            }
                        } else {
                            _scModelUtils.addListToModelPath("Order.OrderLines.OrderLine", orderlines, targetModel1);
                        }
                        /*custom Code for HearderCharges stamping*/
                      _scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge.ChargeAmount",shippingAmount.toString(),targetModel1);
                      _scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge.Extn.ExtnOriginalAmount",shippingAmount.toString(),targetModel1);
                      _scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge.ChargeCategory","SHIPPINGCHARGE",targetModel1);
                      _scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge.ChargeName","SHIPPINGCHARGE",targetModel1);

                         var OrderLineArray = _scBaseUtils.getNewArrayInstance();
                         for(var tNo in targetModel1.Order.OrderLines.OrderLine)
                         {
                             var OrderLine = targetModel1.Order.OrderLines.OrderLine[tNo];
                             var actionSelected = _scModelUtils.getStringValueFromPath("Action", OrderLine);
							// var shipTogetherNo=_scModelUtils.getStringValueFromPath("ShipTogetherNo",targetModel1.Order.OrderLines.OrderLine[tNo]);
                             if(actionSelected != "REMOVE")
                                 {
                             for(var oNo in orderLineChargeArray)
                             {
                                 
                                 if(OrderLine.OrderLineKey == orderLineChargeArray[oNo].OrderLineKey){
									var eachLineCharge =_scModelUtils.getStringValueFromPath("LineCharges",orderLineChargeArray[oNo]); 
                                     if(!(_scBaseUtils.isVoid(eachLineCharge)))
                                     {
                                     _scModelUtils.setStringValueAtModelPath("LineCharges", orderLineChargeArray[oNo].LineCharges, OrderLine);
                                     _scBaseUtils.appendToArray(OrderLineArray,OrderLine);
                                   }
									else{
                                          _scModelUtils.setStringValueAtModelPath("CarrierServiceCode",carrierServiceCode,OrderLine);
                                             _scBaseUtils.appendToArray(OrderLineArray,OrderLine);
                                          }
                                  }
                               }
                               }
                                 else
                                  {
                                      _scBaseUtils.appendToArray(OrderLineArray,OrderLine);
                                  }
                             }
                         
                         _isccsModelUtils.removeAttributeFromModel("Order.OrderLines.OrderLine",targetModel1);
                         _scModelUtils.addListToModelPath("Order.OrderLines.OrderLine", OrderLineArray, targetModel1);
                        this.callSaveOrder(
                        targetModel1, mashupContext2);
                    } else {
                        _isccsBaseTemplateUtils.showMessage(
                        this, "cancelledLines", "error", null);
                    }
                } else {
                    _isccsBaseTemplateUtils.showMessage(
                    this, "unavailableLines", "error", null);
                }
            }
			}
	 },
updateEditorHeader: function(
        event, bEvent, ctrl, args) {
            if (
            _scBaseUtils.equals("ExchangeOrder", this.screenMode)) {
                _isccsBaseTemplateUtils.updateCustomerMessage(
                this, "CUST_FulfillmentSummaryForExchange", true);
                _isccsBaseTemplateUtils.updateTitle(
                this, "Fulfillment_Summary_For_Exchange", null);
            } else {
                _isccsBaseTemplateUtils.updateCustomerMessage(
                this, "CUST_FulfillmentSummaryDetailsScreen", true);
                _isccsBaseTemplateUtils.updateTitle(
                this, "Fulfillment_Summary", null);
            
            }
// Custom code to disable GiftOptions button if Order contains VDS Item
            var model = _scScreenUtils.getModel(this, "getFulfillmentSummaryDetails_output");
		orderLines = _scModelUtils.getModelListFromPath("Order.ShippingGroups.ShippingGroup",model);
		if (!(_scBaseUtils.isVoid(orderLines)))
		{
        var ExtnIsDSVEnabledflag="N";
        for(var i in orderLines)
		   {
		       if(ExtnIsDSVEnabledflag=="Y")
		       {
		           break;
		       } 
                  var orderLineList = _scModelUtils.getModelListFromPath("Page.Output.OrderLines.OrderLine",orderLines[i]);
		     for(var index in orderLineList)
			{
			     var OrderLine=orderLineList[index];
			     var ExtnIsDSVEnabled= _scModelUtils.getStringValueFromPath("ItemDetails.Extn.ExtnIsDSVEnabled",OrderLine); 
                    if (!_scBaseUtils.isVoid(ExtnIsDSVEnabled) && _scBaseUtils.equals(ExtnIsDSVEnabled,"Y"))
				{
				    	ExtnIsDSVEnabledflag="Y";
                        _scWidgetUtils.disableWidget(this, "gift_option", false);
				}

			}
			}
			if(ExtnIsDSVEnabledflag=="N")
				{
				       _scWidgetUtils.enableWidget(this, "gift_option", false);

				}
		}

            return true;
        },
setModelForScreen: function(
        modelOutput) {
            _scWidgetUtils.showWidget(
            this, "mainShippingLinesContainer", false, null);
            _scWidgetUtils.showWidget(
            this, "mainPickupLinesContainer", false, null);
            this.generateUnavailableLinesScreen(
            modelOutput);
            this.generateBackOrderedLinesScreen(
            modelOutput);
            this.generateAvailableLinesScreen(
            modelOutput);
            _scScreenUtils.setModel(
            this, "getFulfillmentSummaryDetails_output", modelOutput, null);
            this.loadOrderModelInFulfillmentPanels();
            var orderTotalModel = null;
            orderTotalModel = _scBaseUtils.getNewModelInstance();
            var orderTotal = null;
            orderTotal = _scModelUtils.getStringValueFromPath("Order.PriceInfo.TotalAmount", modelOutput);
            var currency = null;
            currency = _scModelUtils.getStringValueFromPath("Order.PriceInfo.Currency", modelOutput);
            var orderHeaderKey = null;
            orderHeaderKey = _scModelUtils.getStringValueFromPath("Order.OrderHeaderKey", modelOutput);
            _scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", orderHeaderKey, orderTotalModel);
            _scModelUtils.setStringValueAtModelPath("Order.OverallTotals.GrandTotal", orderTotal, orderTotalModel);
            _scModelUtils.setStringValueAtModelPath("Order.PriceInfo.Currency", currency, orderTotalModel);
            _scBaseUtils.setModel(
            this, "OrderTotalModel", orderTotalModel, null);
            this.defaultOptimizationType();
//Custome code to enable or disable GiftOptions button on click of save
           // var model = _scScreenUtils.getModel(this, "getFulfillmentSummaryDetails_output");
		orderLines = _scModelUtils.getModelListFromPath("Order.ShippingGroups.ShippingGroup",modelOutput);
		if (!(_scBaseUtils.isVoid(orderLines)))
		{
        var ExtnIsDSVEnabledflag="N";
        for(var i in orderLines)
		   {
		       if(ExtnIsDSVEnabledflag=="Y")
		       {
		           break;
		       } 
                  var orderLineList = _scModelUtils.getModelListFromPath("Page.Output.OrderLines.OrderLine",orderLines[i]);
		     for(var index in orderLineList)
			{
			     var OrderLine=orderLineList[index];
			     var ExtnIsDSVEnabled= _scModelUtils.getStringValueFromPath("ItemDetails.Extn.ExtnIsDSVEnabled",OrderLine); 
                    if (!_scBaseUtils.isVoid(ExtnIsDSVEnabled) && _scBaseUtils.equals(ExtnIsDSVEnabled,"Y"))
				{
				    	ExtnIsDSVEnabledflag="Y";
                        _scWidgetUtils.disableWidget(this, "gift_option", false);
				}
				
			}
			}
			if(ExtnIsDSVEnabledflag=="N")
				{
				       _scWidgetUtils.enableWidget(this, "gift_option", false);

				}

		}
        }


});
});
