


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/order/fulfillmentSummary/FulfillmentSummaryDetailsScreenExtn","scbase/loader!sc/plat/dojo/controller/ExtnServerDataController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnFulfillmentSummaryDetailsScreenExtn
			 ,
			    _scExtnServerDataController
){

return _dojodeclare("extn.order.fulfillmentSummary.FulfillmentSummaryDetailsScreenExtnBehaviorController", 
				[_scExtnServerDataController], {

			
			 screenId : 			'extn.order.fulfillmentSummary.FulfillmentSummaryDetailsScreenExtn'

			
			
			
			
			
						,

			
			
			 mashupRefs : 	[
	 		{
		 mashupRefId : 			'extn_ShippingChargesCall'
,
		 mashupId : 			'extn_ShippingChargesMashUP'
,
		 extnType : 			'ADD'

	}

	]

}
);
});

