
scDefine(["dojo/text!./templates/FulfillmentSummaryDetailsScreenExtn.html","scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/_base/lang","scbase/loader!dojo/text","scbase/loader!sc/plat","scbase/loader!sc/plat/dojo/utils/BaseUtils"]
 , function(			 
			    templateText
			 ,
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojolang
			 ,
			    _dojotext
			 ,
			    _scplat
			 ,
			    _scBaseUtils
){
return _dojodeclare("extn.order.fulfillmentSummary.FulfillmentSummaryDetailsScreenExtnUI",
				[], {
			templateString: templateText
	
	
	
	
	
	
					,	
	namespaces : {
		targetBindingNamespaces :
		[
		],
		sourceBindingNamespaces :
		[
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_13'
						,
	  description: 'GiftBoxCommonList'
						,
	  value: 'extn_GiftBoxCommonList'
						
			}
			,
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_14'
						,
	  value: 'extn_shippingDetails'
						
			}
			
		]
	}

	

	,
	hotKeys: [ 
	]

,events : [
	]

,subscribers : {
local : [

{
	  eventId: 'saveCurrentPage'

,	  sequence: '19'




,handler : {
methodName : "extn_ShippingChargeCall"

 
}
}
,
{
	  eventId: 'extn_setCarrierDetailsToEditor'

,	  sequence: '51'




,handler : {
methodName : "extn_setCarrierDetailsToEditor"

 
}
}
,
{
	  eventId: 'afterBehaviorMashupCall'

,	  sequence: '51'




,handler : {
methodName : "extn_shippingChargeCalculation"

 
}
}

]
					 	 ,
}

});
});


