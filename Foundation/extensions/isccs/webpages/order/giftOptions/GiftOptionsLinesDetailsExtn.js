scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/order/giftOptions/GiftOptionsLinesDetailsExtnUI","scbase/loader!sc/plat/dojo/utils/ScreenUtils",
"scbase/loader!sc/plat/dojo/utils/EditorUtils","scbase/loader!isccs/utils/UIUtils","scbase/loader!sc/plat/dojo/utils/ModelUtils",
"scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!sc/plat/dojo/utils/WidgetUtils","scbase/loader!sc/plat/dojo/utils/GridxUtils",
"scbase/loader!sc/plat/dojo/utils/WizardUtils", "scbase/loader!sc/plat/dojo/utils/ControllerUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnGiftOptionsLinesDetailsExtnUI
				,_scScreenUtils
				,_scEditorUtils
				,_isccsUIUtils
				,_scModelUtils
				,_scBaseUtils
				,_scWidgetUtils
				,_scGridxUtils
				,_scWizardUtils
				,_scControllerUtils
				,_scEventUtils
){ 
	return _dojodeclare("extn.order.giftOptions.GiftOptionsLinesDetailsExtn", [_extnGiftOptionsLinesDetailsExtnUI],{
	// custom code here
refreshflag: true,
//Used to Select or deselect all OrderLines based on the radio option selected
// event is fired to Parent Screen to event "extn_SelectAllOrderLines" and method "extn_SelectAllOrderLinesMethod" 

	extn_IncludeAllMethod: function(event, bEvent, ctrl, args)
		{
	
		  if(event=="01"){
              	this.refreshflag= false;
         	 }else{
            		 this.refreshflag= true;
          	 }
            	  var eventDefn = null;
            	  eventDefn = _scBaseUtils.getNewBeanInstance();
		  _scBaseUtils.setAttributeValue("argumentList.eventType", event, eventDefn);
		  _scEventUtils.fireEventToParent(this, "extn_SelectAllOrderLines", eventDefn);   
            
		},

//Used to display or hide the Radio button based on "Gift This product" check box

	extn_GiftProductMethod:function(event, bEvent, ctrl, args) 
		{
            var targetModel = null;
            targetModel = _scBaseUtils.getTargetModel(this, "giftOptions_input", null);
            var GiftWrap = _scModelUtils.getStringValueFromPath("GiftWrap", targetModel);          
			if(this.refreshflag)
           {
			       this.refreshflag = false;
                   if(GiftWrap=="Y")
			        {
				        _scWidgetUtils.showWidget(this, "extn_radiobuttonset", false, null);
				
				         _scWidgetUtils.setValue(this,"extn_radiobuttonset","02",true);
			        }
			       else
			          {
				        _scWidgetUtils.hideWidget(this, "extn_radiobuttonset", false);
				         _scWidgetUtils.setValue(this,"extn_radiobuttonset","02",true);
                       }
             }
            else
            {
               

                   if(GiftWrap=="Y")
			        {
				        _scWidgetUtils.showWidget(this, "extn_radiobuttonset", false, null);
			        }
			       else
			        {
				        _scWidgetUtils.hideWidget(this, "extn_radiobuttonset", false);
				     
                    }
            }
             
		},

//Used to enable the grid on click of "Apply".
// Execution comes to this method when "fireEventToChild" is used in "extn_StopApplymethod" method in Parent screen.
// Event is fired to Parent Screen to eventID "extn_SelectAllOrderLines" and method "extn_SelectAllOrderLinesMethod" 

extn_onApplyMethod:function(event, bEvent, ctrl, args)
	{
	    eventDefn = _scBaseUtils.getNewBeanInstance();
	this.refreshflag= false;
		_scBaseUtils.setAttributeValue("argumentList.eventType", "02", eventDefn);
		_scEventUtils.fireEventToParent(this, "extn_SelectAllOrderLines", eventDefn);
		this.applyChanges();  
	},

//Used to stop "Apply" event when Radio Option is Selected wrongly
// event is fired to Parent Screen to event "extn_StopApply" and method "extn_StopApplymethod" 

extn_OnApplyStop :function(event, bEvent, ctrl, args)
	{ 
	    sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
		eventDefn = _scBaseUtils.getNewBeanInstance();
      var targetModel = _scBaseUtils.getTargetModel(this, "giftOptions_input", null);
            var GiftWrap = _scModelUtils.getStringValueFromPath("GiftWrap", targetModel); 
		var RadioButton=_scWidgetUtils.getValue(this,"extn_radiobuttonset");
		_scBaseUtils.setAttributeValue("argumentList.RadioButton", RadioButton, eventDefn);
        _scBaseUtils.setAttributeValue("argumentList.GiftWrap", GiftWrap, eventDefn);
   
            
		_scEventUtils.fireEventToParent(this, "extn_StopApply", eventDefn);  

			
	}

});
});

