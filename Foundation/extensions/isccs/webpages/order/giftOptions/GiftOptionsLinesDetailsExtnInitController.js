


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/order/giftOptions/GiftOptionsLinesDetailsExtn","scbase/loader!sc/plat/dojo/controller/ExtnScreenController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnGiftOptionsLinesDetailsExtn
			 ,
			    _scExtnScreenController
){

return _dojodeclare("extn.order.giftOptions.GiftOptionsLinesDetailsExtnInitController", 
				[_scExtnScreenController], {

			
			 screenId : 			'extn.order.giftOptions.GiftOptionsLinesDetailsExtn'

			
			
			
}
);
});

