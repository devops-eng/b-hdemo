
scDefine(["dojo/text!./templates/GiftOptionsLinesDetailsExtn.html","scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/_base/lang","scbase/loader!dojo/text","scbase/loader!idx/form/CheckBox","scbase/loader!idx/form/RadioButtonSet","scbase/loader!sc/plat","scbase/loader!sc/plat/dojo/binding/CheckBoxDataBinder","scbase/loader!sc/plat/dojo/binding/RadioSetDataBinder","scbase/loader!sc/plat/dojo/utils/BaseUtils"]
 , function(			 
			    templateText
			 ,
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojolang
			 ,
			    _dojotext
			 ,
			    _idxCheckBox
			 ,
			    _idxRadioButtonSet
			 ,
			    _scplat
			 ,
			    _scCheckBoxDataBinder
			 ,
			    _scRadioSetDataBinder
			 ,
			    _scBaseUtils
){
return _dojodeclare("extn.order.giftOptions.GiftOptionsLinesDetailsExtnUI",
				[], {
			templateString: templateText
	
	
	
	
	
	
	

	,
	hotKeys: [ 
	]

,events : [
	]

,subscribers : {
local : [

{
	  eventId: 'singleLineSelected'

,	  sequence: '51'

,	  description: 'GiftProductMethod'



,handler : {
methodName : "extn_GiftProductMethod"

 
}
}
,
{
	  eventId: 'multipleLineSelected'

,	  sequence: '51'

,	  description: 'GiftProductMethod'



,handler : {
methodName : "extn_GiftProductMethod"

 
}
}
,
{
	  eventId: 'giftThisItemCheck_onClick'

,	  sequence: '51'

,	  description: 'GiftProductMethod'



,handler : {
methodName : "extn_GiftProductMethod"

 
}
}
,
{
	  eventId: 'giftWrapThisItemCheck_onClick'

,	  sequence: '51'

,	  description: 'GiftProductMethod'



,handler : {
methodName : "extn_GiftProductMethod"

 
}
}
,
{
	  eventId: 'apply_onClick'

,	  sequence: '19'

,	  description: 'OnApplyStop'



,handler : {
methodName : "extn_OnApplyStop"

 
}
}
,
{
	  eventId: 'extn_radiobuttonset_onChange'

,	  sequence: '51'

,	  description: 'extn_IncludeAllMethod'



,handler : {
methodName : "extn_IncludeAllMethod"

 
}
}
,
{
	  eventId: 'extn_onapply'

,	  sequence: '51'




,handler : {
methodName : "extn_onApplyMethod"

 
}
}

]
					 	 ,
}

});
});


