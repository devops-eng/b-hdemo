scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/order/giftOptions/GiftOptionsLinesExtnUI","scbase/loader!sc/plat/dojo/utils/ScreenUtils",
"scbase/loader!sc/plat/dojo/utils/EditorUtils","scbase/loader!isccs/utils/UIUtils","scbase/loader!sc/plat/dojo/utils/ModelUtils",
"scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!sc/plat/dojo/utils/WidgetUtils","scbase/loader!sc/plat/dojo/utils/GridxUtils",
"scbase/loader!sc/plat/dojo/utils/WizardUtils", "scbase/loader!sc/plat/dojo/utils/ControllerUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils",
"scbase/loader!isccs/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/PaginationUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnGiftOptionsLinesExtnUI
				,_scScreenUtils
				,_scEditorUtils
				,_isccsUIUtils
				,_scModelUtils
				,_scBaseUtils
				,_scWidgetUtils
				,_scGridxUtils
				,_scWizardUtils
				,_scControllerUtils
				,_scEventUtils
               		 ,_isccsBaseTemplateUtils
				,_scPaginationUtils
){ 
	return _dojodeclare("extn.order.giftOptions.GiftOptionsLinesExtn", [_extnGiftOptionsLinesExtnUI],{
	// custom code here

//Used to disable the "GiftBox" OrderLine

	extn_DisableGiftBoxMethod:function(event, bEvent, ctrl, args) 
		{
			var modelOutput = _scBaseUtils.getValueFromPath("result.Page.Output", args);
            		var GiftBoxOrderModel = _scScreenUtils.getModel(this, "extn_GiftBoxCommonList");
	   		var CommonCodeList=_scModelUtils.getModelListFromPath("CommonCodeList.CommonCode", GiftBoxOrderModel);
            		var OrganizationCode=null;
            		var GiftBoxID=null;
            for(var i in CommonCodeList)
		{
           	 	var CommonCodeObj = _scModelUtils.getModelFromList(CommonCodeList, i); 
			var CodeType=_scModelUtils.getStringValueFromPath("CodeType", CommonCodeObj );
           		if(CodeType=="LT_CustomCode_Gift")
            		{
                 		GiftBoxID=_scModelUtils.getStringValueFromPath("CodeValue", CommonCodeObj );
                 		OrganizationCode=_scModelUtils.getStringValueFromPath("OrganizationCode", CommonCodeObj);           		}
            
		}
			var TargetOrderLineList=_scModelUtils.getModelListFromPath("OrderLineList.OrderLine", modelOutput);
			var ChildScreen=_scScreenUtils.getChildScreen(this, "detailsGiftOptions")
			for(var i in TargetOrderLineList)
                {
                    var OrderLineElement = TargetOrderLineList[i];
                    var ItemID = _scModelUtils.getStringValueFromPath("ItemDetails.ItemID", OrderLineElement);
                    if(ItemID==GiftBoxID)
                        {
				var rowIndex = i;
				_scGridxUtils.setGridRowDisabled(this, rowIndex, "OLST_listGrid", false);
				_scGridxUtils.refreshGridxLayout(this, "OLST_listGrid");
                        }
                }
		var GiftBoxItemNS = _scScreenUtils.getModel(this, "extn_GiftBoxItem");
                if(_scBaseUtils.isVoid(GiftBoxItemNS))
			{
				var Input=null;
				Input = _scBaseUtils.getNewModelInstance();
                    
                    _scModelUtils.setStringValueAtModelPath("Item.ItemID",GiftBoxID, Input);
               _scModelUtils.setStringValueAtModelPath("Item.CallingOrganizationCode",OrganizationCode, Input);
		var Locale=sc.plat.dojo.utils.PlatformUIFmkImplUtils.getLocaleCode(); 
			_scModelUtils.setStringValueAtModelPath("Item.DisplayLocalizedFieldInLocale",Locale,Input);
             _isccsUIUtils.callApi(this, Input, "extn_getItemList_Service_GiftOptions");
			}
		},

// Used to select all the OrderLines and to disable/enable the grid based on Radio Option selected.
// Execution comes to thismethod when "fireEventToParent" is used in "extn_IncludeAllMethod" and "extn_onApplyMethod" methods in child screen.
// If all Items are Gift wrap not allowed, error message is shown and selecting all the OrderLines is stopped.

	extn_SelectAllOrderLinesMethod: function(event, bEvent, ctrl, args)
		{
            var gridObj = _scScreenUtils.getWidgetByUId(this,"OLST_listGrid"); 
            var eventType = _scModelUtils.getStringValueFromPath("eventType", args);
             if(eventType == "01")
			{
				var ChildScreen=_scScreenUtils.getChildScreen(this, "detailsGiftOptions");
				var TargetModel = _scScreenUtils.getTargetModel(this, "getCompleteOrderLineList_input");
				var selectedSourceRecordsList = _scGridxUtils.getSelectedSourceRecordsWithRowIndex(this, "OLST_listGrid");
                var sourcetOrderLineList=_scModelUtils.getModelListFromPath("", selectedSourceRecordsList);
				var sourceLength=_scBaseUtils.getAttributeCount(sourcetOrderLineList);
                var Store=_scGridxUtils.getGridStore(gridObj);
                var ArrayOfAllItems =  _scModelUtils.getStringValueFromPath("_arrayOfAllItems", Store);
                var GiftWrapNotAllowedItemsArray=[];
                for(var i in ArrayOfAllItems)
                    {
						var LineElement = ArrayOfAllItems[i];
                        var disabledRowArray =  _scModelUtils.getStringValueFromPath("disabledRow", LineElement);
                        var disabledRow =  _scBaseUtils.getArrayItemByIndex(disabledRowArray,0);
						if(!(disabledRow))
						{
                          var isGiftWrapAllowedForItemArray =  _scModelUtils.getStringValueFromPath("isGiftWrapAllowedForItem", LineElement);
                          var isGiftWrapAllowedForItem =  _scBaseUtils.getArrayItemByIndex(isGiftWrapAllowedForItemArray,0);
                          if(isGiftWrapAllowedForItem=="N")
                          {
                              var ProductsArray= _scModelUtils.getStringValueFromPath("Product_AddItems", LineElement);
                              var Product=_scBaseUtils.getArrayItemByIndex(ProductsArray,0);
                              if(!(_scBaseUtils.containsInArray(GiftWrapNotAllowedItemsArray,Product)))
                              {
                                  _scBaseUtils.appendToArray(GiftWrapNotAllowedItemsArray,Product);
                              }
                          }
						}             
                    }
                if(_scBaseUtils.isVoid(GiftWrapNotAllowedItemsArray))
                   {
						_isccsBaseTemplateUtils.closeMessagePanel(event, bEvent, ctrl,args);
						RowIndexArray=_scBaseUtils.getNewArrayInstance();
           				for(var j in sourcetOrderLineList)
						{
							var OrderLineElement1 = sourcetOrderLineList[j];
                            var rowIndex =  _scModelUtils.getStringValueFromPath("rowIndex", OrderLineElement1);
							 _scBaseUtils.appendToArray(RowIndexArray,rowIndex);
						}
                        for(var i in ArrayOfAllItems)
                            {
                                if(!(_scBaseUtils.containsInArray(RowIndexArray,i)))
				                 {
						            if(!(_scGridxUtils.isGridRowDisabled(gridObj,i)))
						            {
							            _scGridxUtils.selectRow(gridObj,i);
						            }
					             }
                            }
                    }
                         
                   else
                    {
                       //var GiftWrapNotAllowedLength=_scBaseUtils.getAttributeCount(GiftWrapNotAllowedItemsArray);

				var Message = null;
                		Message = _scScreenUtils.getString(
               		 this, "extn_Product");

				var GiftMsg = null;
                		GiftMsg = _scScreenUtils.getString(
               		 this, "extn_Not_Authorized_Gift_Wrap");

					
                       	

                          
                      for(var i in GiftWrapNotAllowedItemsArray)  
                      {
                           Message=_scBaseUtils.stringConcat(Message,GiftWrapNotAllowedItemsArray[i]); 
                           Message=_scBaseUtils.stringConcat(Message," "); 
                      }
                      
                              Message=_scBaseUtils.stringConcat(Message,GiftMsg);
                           
                       _isccsBaseTemplateUtils.showMessage(this,Message, "error", null);
			  _scWidgetUtils.setValue(ChildScreen,"extn_radiobuttonset","02",true);						
                       sc.plat.dojo.utils.EventUtils.stopEvent(bEvent); 
                    }
               _scGridxUtils.disableGrid(gridObj);
             } else {
                _scGridxUtils.enableGrid(gridObj);
                
            }
		},

//Used to flow the execution when Radio Option is Selected correctly.
//Execution comes to this method when "fireEventToParent" is used in "extn_onApplyStop" method in child screen.
// If Radio Option is Selected correctly event is fired to ChildScreen to eventID "extn_onapply" and method "extn_onApplyMethod".

extn_StopApplymethod: function(event, bEvent, ctrl, args)
	{
		var length=0;
        var gridObj = _scScreenUtils.getWidgetByUId(this,"OLST_listGrid"); 
		var RowCount=_scGridxUtils.getRowCount(this,"OLST_listGrid");
		var selectedSourceRecordsList = _scGridxUtils.getSelectedSourceRecordsWithRowIndex(this, "OLST_listGrid");
		var sourcetOrderLineList=_scModelUtils.getModelListFromPath("", selectedSourceRecordsList);
		var sourceLength=_scBaseUtils.getAttributeCount(sourcetOrderLineList);
		var GiftWrap = _scModelUtils.getStringValueFromPath("GiftWrap", args);
		var RadioButton= _scModelUtils.getStringValueFromPath("RadioButton", args);
        if(GiftWrap=="Y" && RadioButton=="01")
			{
				for(var i=0; i<RowCount;i++)
				{
					if(!(_scGridxUtils.isGridRowDisabled(gridObj,i)))
					{
						length=length+1;
					}
				}
			if(sourceLength<length)
				{

				var sMessage = null;
                		sMessage = _scScreenUtils.getString(
               		 this, "extn_Please_Select_Radio_Button_Correctly");
				_isccsBaseTemplateUtils.showMessage(this,sMessage,"error");

					//_isccsBaseTemplateUtils.showMessage(this, "Please select radio button correctly", "error", null);
					return;
					
				}
			else
			{
				_isccsBaseTemplateUtils.closeMessagePanel(event, bEvent, ctrl,args);
				_scEventUtils.fireEventToChild(this, "detailsGiftOptions", "extn_onapply", null);
			}
		}
         else
         {
            _isccsBaseTemplateUtils.closeMessagePanel(event, bEvent, ctrl,args);
            _scEventUtils.fireEventToChild(this, "detailsGiftOptions", "extn_onapply", null);
         }
	},
//Used to unflag the remaining OrderLines while Individually condition is used.

extn_SelectedLinesInGrid: function( event, bEvent, ctrl, args) 
		{
			var selectedSourceRecordsList = null;
            selectedSourceRecordsList = _scGridxUtils.getSelectedSourceRecordsWithRowIndex(this, "OLST_listGrid");
        	var TargetModel = _scScreenUtils.getTargetModel(this, "getCompleteOrderLineList_input");
			var TargetOrderLineList=_scModelUtils.getModelListFromPath("OrderLineList.OrderLine", TargetModel);
			var sourcetOrderLineList=_scModelUtils.getModelListFromPath("", selectedSourceRecordsList);
			var sourceLength=_scBaseUtils.getAttributeCount(sourcetOrderLineList);
			var RowIndexArray=_scBaseUtils.getNewArrayInstance();
            var gridObj = _scScreenUtils.getWidgetByUId(this,"OLST_listGrid"); 
			for(var j in sourcetOrderLineList)
				{
				var OrderLineElement1 = sourcetOrderLineList[j];
                var rowIndex =  _scModelUtils.getStringValueFromPath("rowIndex", OrderLineElement1);
				_scBaseUtils.appendToArray(RowIndexArray,rowIndex);
				}
			for(var i in TargetOrderLineList)
				{
					var count=0;
				    var OrderLineElement = TargetOrderLineList[i];
					var GiftWrap = _scModelUtils.getStringValueFromPath("GiftWrap", OrderLineElement);
                    if(GiftWrap=="Y")
                    {
				        if(!(_scBaseUtils.containsInArray(RowIndexArray,i)))
				        {
						    if(!(_scGridxUtils.isGridRowDisabled(gridObj,i)))
						    {
							    this.extn_UnFlagMethod(i);	
						    }
					    }
					}
				}
	},
//Used to unflag the OrderLines.
// Execution comes to this method when it is called in "extn_SelectedLinesInGrid".

extn_UnFlagMethod: function(rowIndex)
		{
			var rowIndex = rowIndex;
            var rowItem = null;
            var newModelData = null;
            newModelData = _scBaseUtils.getNewModelInstance();
            var GiftWrap="N";
	     var GiftFlag="N";
            rowItem = _scGridxUtils.getItemFromRowIndexUsingUId(this, "OLST_listGrid", rowIndex);
			_scModelUtils.addStringValueToModelObject("GiftWrap", GiftWrap, newModelData);
			_scModelUtils.addStringValueToModelObject("GiftFlag", GiftFlag, newModelData);
			_scGridxUtils.updateRecordToGridUsingUId(this, "OLST_listGrid", rowItem, newModelData, true, true);
		},
//Used to get the quantity of GiftBox to be added or deleted.

extn_OnSave: function(event, bEvent, ctrl, args) 
		{
		var editorInputdata =  _scScreenUtils.getInitialInputData(_scEditorUtils.getCurrentEditor());
		            var GiftBoxOrderModel = _scScreenUtils.getModel(this, "extn_GiftBoxCommonList");

			var CommonCodeList=_scModelUtils.getModelListFromPath("CommonCodeList.CommonCode", GiftBoxOrderModel);
            var OrganizationCode=null;
            var GiftBoxItemID=null;
        for(var i in CommonCodeList)
		{
            var CommonCodeObj = _scModelUtils.getModelFromList(CommonCodeList, i); 
            var CodeType=_scModelUtils.getStringValueFromPath("CodeType", CommonCodeObj );
            if(CodeType=="LT_CustomCode_Gift")
            {
                 GiftBoxItemID=_scModelUtils.getStringValueFromPath("CodeValue", CommonCodeObj );
            }
            
		}
        
			var TargetModel = _scScreenUtils.getTargetModel(this, "getCompleteOrderLineList_input");
			
			var TargetOrderLineList=_scModelUtils.getModelListFromPath("OrderLineList.OrderLine", TargetModel);
			var TargetLength=_scBaseUtils.getAttributeCount(TargetOrderLineList);
			var initialInputData = _scScreenUtils.getInitialInputData(this);
			var OrderHeaderKey = _scModelUtils.getStringValueFromPath("Order.OrderHeaderKey", initialInputData);
			var apiInput=null;
			apiInput = _scBaseUtils.getNewModelInstance();
			var apiInput1 = null;
			_scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", OrderHeaderKey, apiInput); 
            var gridObj = _scScreenUtils.getWidgetByUId(this,"OLST_listGrid"); 
			var Store=_scGridxUtils.getGridStore(gridObj);
            var GiftWrapCount=0;
            var quantity=0;
			var GiftBoxCount=0;
			var DeliveryMethod=null;
			var Shipnode=null
			var OrderLineKey=null;
			var OrderLineKeyArray=_scBaseUtils.getNewArrayInstance();
			var GridArray=_scModelUtils.getModelListFromPath("_arrayOfAllItems", Store); 
            for(var m in GridArray)
				{
					var GridArrayElement= GridArray[m];
					DeliveryMethod= _scModelUtils.getModelListFromPath("_dataItem.DeliveryMethod", GridArrayElement);
					Shipnode=_scModelUtils.getModelListFromPath("_dataItem.Shipnode.ShipNode", GridArrayElement);

					break;

				}
			   for(var i in TargetOrderLineList)
                               {
                                    var OrderLineElement = TargetOrderLineList[i];
                                   
                                    var GiftWrap = _scModelUtils.getStringValueFromPath("GiftWrap", OrderLineElement);
                                    var ItemID = _scModelUtils.getStringValueFromPath("Item.ItemID", OrderLineElement);
                                     if(ItemID==GiftBoxItemID)
                                        {
                                            GiftBoxCount=GiftBoxCount+1;
											var OrderLineKey = _scModelUtils.getStringValueFromPath("OrderLineKey", OrderLineElement);
											 _scBaseUtils.appendToArray(OrderLineKeyArray,OrderLineKey);
                                        }
									if(GiftWrap=="Y" && ItemID!=GiftBoxItemID)
                                    {
                                        GiftWrapCount=GiftWrapCount+1;
                                    }
                                    
                                         
                                        
                                    
                               }
				var ChildScreen=_scScreenUtils.getChildScreen(this, "detailsGiftOptions")               
				var RadioButton=_scWidgetUtils.getValue(ChildScreen,"extn_radiobuttonset");
			if(RadioButton=="01")
			{
                GiftWrapItemsCount=TargetLength-GiftBoxCount;
				if(GiftWrapCount==GiftWrapItemsCount)
				{
                    quantity=1;
					
					      
				}
			}
			else{
            	quantity=GiftWrapCount;
				}
                            if(GiftBoxCount!=quantity)
                            {
							var GiftBoxItemModel = _scScreenUtils.getModel(this, "extn_GiftBoxItem");
								var ItemList=_scModelUtils.getModelListFromPath("Item", GiftBoxItemModel );
						var GiftItem=null;
						for(var i in ItemList)
						{
							var ItemObj = _scModelUtils.getModelFromList(ItemList, i); 
							var ItemID= _scModelUtils.getStringValueFromPath("ItemID", ItemObj );

							if(ItemID==GiftBoxItemID)
							{
							GiftItem=ItemObj ;
							}
            
						}
             				var input=this.extn_OrderLines(quantity,GiftBoxCount,OrderLineKeyArray,GiftItem,DeliveryMethod,Shipnode);
							_scModelUtils.setStringValueAtModelPath("Order.OrderLines.OrderLine", input, apiInput);
							_isccsUIUtils.callApi(this, apiInput, "extn_ChangeOrder");
                            }
				
		},

//Used to form the Input to the "ChangeOrder" Api.

extn_OrderLines: function(quantity,GiftBoxCount,OrderLineKeyArray,GiftItem,DeliveryMethod,Shipnode)
		{
            if(quantity>GiftBoxCount)
		{
			var UnitPrice=0;
			if(!(_scBaseUtils.isVoid(_scModelUtils.getStringValueFromPath("ComputedPrice.ListPrice", GiftItem))))
			{
				UnitPrice=_scModelUtils.getStringValueFromPath("ComputedPrice.ListPrice", GiftItem);
			}
			else if(!(_scBaseUtils.isVoid(_scModelUtils.getStringValueFromPath("ComputedPrice.UnitPrice", GiftItem))))
			{
				UnitPrice=_scModelUtils.getStringValueFromPath("ComputedPrice.UnitPrice", GiftItem);			
			}
			else
			{
				UnitPrice = 0.00;
			}

			quantity=quantity-GiftBoxCount;
                     var apiInput=[];	
                     var apiInput1 = _scBaseUtils.getNewModelInstance();
			_scModelUtils.setStringValueAtModelPath("GiftFlag", "Y", apiInput1);
                    //_scModelUtils.setStringValueAtModelPath("GiftWrap", "Y", apiInput1);
			_scModelUtils.setStringValueAtModelPath("OrderLineTranQuantity.OrderedQty", "1", apiInput1);
			_scModelUtils.setStringValueAtModelPath("Item.ItemID", _scModelUtils.getStringValueFromPath("ItemID", GiftItem), apiInput1);
			_scModelUtils.setStringValueAtModelPath("Item.UnitOfMeasure", _scModelUtils.getStringValueFromPath("UnitOfMeasure", GiftItem), apiInput1);
			_scModelUtils.setStringValueAtModelPath("LinePriceInfo.UnitPrice", UnitPrice, apiInput1);
			_scModelUtils.setStringValueAtModelPath("DeliveryMethod", DeliveryMethod, apiInput1);
			if(DeliveryMethod=="PICK")
			{
			_scModelUtils.setStringValueAtModelPath("ShipNode", Shipnode, apiInput1);
            }
			for(var i=0; i<quantity;i++)
			{
				_scModelUtils.addModelObjectToModelList(apiInput1, apiInput);
			}
			return(apiInput);
		}
	      else if(quantity<GiftBoxCount)
		{
		      quantity=GiftBoxCount-quantity;
                    var apiInput=[];
		      for(var i=0; i<quantity;i++)
		  	{
				var apiInput1 = _scBaseUtils.getNewModelInstance();
				_scModelUtils.setStringValueAtModelPath("OrderLineKey", OrderLineKeyArray[i], apiInput1);
				_scModelUtils.setStringValueAtModelPath("Action", "REMOVE", apiInput1);
				_scModelUtils.addModelObjectToModelList(apiInput1, apiInput);
			}
			return(apiInput);
		}
		},

//Used to update the Price and OrderTotal 

extn_afterBehaviorMashupCall: function(event, bEvent, ctrl, args)
	{
		var MashupList=_scModelUtils.getStringValueFromPath("mashupArray", args);

		for(var i in MashupList)
		{
            var OrderLineElement = MashupList[i];
            var mashupRefId=_scModelUtils.getStringValueFromPath("mashupRefId", OrderLineElement);
			if (_scBaseUtils.equals(mashupRefId, "modifyFulfillmentOptions")) 
               {
                  _scScreenUtils.setModel(this, "getCompleteOrderDetails_output",OrderLineElement.mashupRefOutput, null); 
               }
           else if (_scBaseUtils.equals(mashupRefId, "extn_getItemList_Service_GiftOptions")) 
            {
                var mashupOutput=_scModelUtils.getModelListFromPath("mashupRefOutput.ItemList", OrderLineElement);
               _scScreenUtils.setModel(this, "extn_GiftBoxItem",mashupOutput, null); 
		           
            }
        }
    }


});
});

