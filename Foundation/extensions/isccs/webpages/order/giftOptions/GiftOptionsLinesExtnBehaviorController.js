


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/order/giftOptions/GiftOptionsLinesExtn","scbase/loader!sc/plat/dojo/controller/ExtnServerDataController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnGiftOptionsLinesExtn
			 ,
			    _scExtnServerDataController
){

return _dojodeclare("extn.order.giftOptions.GiftOptionsLinesExtnBehaviorController", 
				[_scExtnServerDataController], {

			
			 screenId : 			'extn.order.giftOptions.GiftOptionsLinesExtn'

			
			
			
			
			
						,

			
			
			 mashupRefs : 	[
	 		{
		 mashupRefId : 			'extn_ChangeOrder'
,
		 mashupId : 			'extn_giftOptionsLines_ChangeOrder'
,
		 extnType : 			'ADD'

	}
,
	 		{
		 mashupRefId : 			'extn_getItemList_Service_GiftOptions'
,
		 mashupId : 			'extn_getItemList_Service_GiftOptions'
,
		 extnType : 			'ADD'

	}

	]

}
);
});

