


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/order/giftOptions/GiftOptionsLinesExtn","scbase/loader!sc/plat/dojo/controller/ExtnScreenController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnGiftOptionsLinesExtn
			 ,
			    _scExtnScreenController
){

return _dojodeclare("extn.order.giftOptions.GiftOptionsLinesExtnInitController", 
				[_scExtnScreenController], {

			
			 screenId : 			'extn.order.giftOptions.GiftOptionsLinesExtn'

			
			
			
			
			
						,

			
			
			 mashupRefs : 	[
	 		{
		 sourceNamespace : 			''
,
		 mashupRefId : 			'getCompleteOrderDetails'
,
		 sequence : 			''
,
		 mashupId : 			'giftOptions_getCompleteOrderDetails'
,
		 callSequence : 			''
,
		 extnType : 			'MODIFY'
,
		 sourceBindingOptions : 			''

	}
,
	 		{
		 sourceNamespace : 			'extn_GiftBoxCommonList'
,
		 mashupRefId : 			'extn_getGiftBoxCommonCode_giftOptions'
,
		 sequence : 			''
,
		 mashupId : 			'extn_getGiftBoxCommonCode_giftOptions'
,
		 callSequence : 			''
,
		 extnType : 			'ADD'
,
		 sourceBindingOptions : 			''

	}

	]

}
);
});

