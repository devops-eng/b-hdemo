
scDefine(["dojo/text!./templates/GiftOptionsLinesExtn.html","scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/_base/lang","scbase/loader!dojo/text","scbase/loader!gridx/Grid","scbase/loader!sc/plat","scbase/loader!sc/plat/dojo/binding/GridxDataBinder","scbase/loader!sc/plat/dojo/utils/BaseUtils"]
 , function(			 
			    templateText
			 ,
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojolang
			 ,
			    _dojotext
			 ,
			    _gridxGrid
			 ,
			    _scplat
			 ,
			    _scGridxDataBinder
			 ,
			    _scBaseUtils
){
return _dojodeclare("extn.order.giftOptions.GiftOptionsLinesExtnUI",
				[], {
			templateString: templateText
	
	
	
	
	
	
					,	
	namespaces : {
		targetBindingNamespaces :
		[
		],
		sourceBindingNamespaces :
		[
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_9'
						,
	  description: 'GiftBoxCommonList'
						,
	  value: 'extn_GiftBoxCommonList'
						
			}
			,
			{
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_10'
						,
	  description: 'GiftBoxItem'
						,
	  value: 'extn_GiftBoxItem'
						
			}
			
		]
	}

	

	,
	hotKeys: [ 
	]

,events : [
	]

,subscribers : {
local : [

{
	  eventId: 'OLST_listGrid_afterPagingload'

,	  sequence: '51'

,	  description: 'DisableGiftBoxMethod'



,handler : {
methodName : "extn_DisableGiftBoxMethod"

 
}
}
,
{
	  eventId: 'updateAllLines'

,	  sequence: '19'

,	  description: 'SelectedLinesInGrid'



,handler : {
methodName : "extn_SelectedLinesInGrid"

 
}
}
,
{
	  eventId: 'saveCurrentPage'

,	  sequence: '19'

,	  description: 'OnSave'



,handler : {
methodName : "extn_OnSave"

 
}
}
,
{
	  eventId: 'save_onClick'

,	  sequence: '19'

,	  description: 'OnSave'



,handler : {
methodName : "extn_OnSave"

 
}
}
,
{
	  eventId: 'extn_SelectAllOrderLines'

,	  sequence: '51'

,	  description: 'extn_SelectAllOrderLinesMethod'



,handler : {
methodName : "extn_SelectAllOrderLinesMethod"

 
}
}
,
{
	  eventId: 'extn_StopApply'

,	  sequence: '51'

,	  description: 'StopApplymethod'



,handler : {
methodName : "extn_StopApplymethod"

 
}
}
,
{
	  eventId: 'afterBehaviorMashupCall'

,	  sequence: '51'

,	  description: 'afterBehaviorMashupCall'



,handler : {
methodName : "extn_afterBehaviorMashupCall"

 
}
}

]
					 	 ,
}

});
});


