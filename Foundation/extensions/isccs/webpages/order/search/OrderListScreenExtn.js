scDefine(["scbase/loader!dojo/_base/declare",
        "scbase/loader!extn/order/search/OrderListScreenExtnUI",
        "scbase/loader!sc/plat/dojo/utils/BaseUtils",
        "scbase/loader!sc/plat/dojo/utils/ModelUtils",
        "scbase/loader!isccs/utils/BaseTemplateUtils",
        "scbase/loader!sc/plat/dojo/utils/ScreenUtils",
        "scbase/loader!isccs/utils/SharedComponentUtils",
        "scbase/loader!isccs/utils/UIUtils",
        "scbase/loader!isccs/utils/ModelUtils",
        "scbase/loader!sc/plat/dojo/utils/WidgetUtils",
        "scbase/loader!sc/plat/dojo/Userprefs",
        "scbase/loader!extn/expicient/utils/ExpicientUtil"
    ],
    function(
        _dojodeclare,
        _extnOrderListScreenExtnUI,
        _scBaseUtils,
        _scModelUtils,
        _isccsBaseTemplateUtils,
        _scScreenUtils,
        _isccsSharedComponentUtils,
        _isccsUIUtils,
        _isccsModelUtils,
        _scWidgetUtils,
        _scUserprefs,
        _extnExpicientUtils
    ) {
        return _dojodeclare("extn.order.search.OrderListScreenExtn", [_extnOrderListScreenExtnUI], {
            // custom code here
            extn_beforeBehaviorMashupCall: function(event, bEvent, ctrl, args) {
                var isMashupAvailable = _extnExpicientUtils.isBeforeBehaviorMashupAvailable("getOrderList", args);
                if (isMashupAvailable) {
                    var inputData = null;
                    inputData = _extnExpicientUtils.getMashupInputForRefID("getOrderList", args);
                    var statusModel = _scScreenUtils.getTargetModel(this.getOwnerScreen(), "extn_notCancelledStatus");
                    var status = _scModelUtils.getStringValueFromPath("Order.Status", statusModel);
                    if (!_scBaseUtils.isVoid(status)) {
                        if (_scBaseUtils.equals(status,"Y")) {
                        _scModelUtils.setStringValueAtModelPath("Order.Status", 9000, inputData);
                        _scModelUtils.setStringValueAtModelPath("Order.StatusQryType", "NE", inputData);
                        }
                    }
                    var upc=null;
                    var trackingNo=null;
                    var trackingData = _scScreenUtils.getModel(this.getOwnerScreen(), "extn_additionalData_output");
                    if(!_scBaseUtils.isVoid(trackingData))
                    {
                        upc=_scModelUtils.getStringValueFromPath("Order.UPC", trackingData);
                        trackingNo=_scModelUtils.getStringValueFromPath("Order.TrackingNo", trackingData);

                        if (_scBaseUtils.and(!_scBaseUtils.isVoid(upc),!_scBaseUtils.isVoid(trackingNo))) {

                        _scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", trackingNo, inputData);
                         _scModelUtils.setStringValueAtModelPath("Order.OrderLine.Item.ItemID", upc, inputData);

                    }
                    else if(!_scBaseUtils.isVoid(upc)){
                      _scModelUtils.setStringValueAtModelPath("Order.OrderLine.Item.ItemID", upc, inputData);
                    }
                    else if(!_scBaseUtils.isVoid(trackingNo)){
                        _scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", trackingNo, inputData);
                    }
                    

                    }
               
                }
            }
        });
    });
