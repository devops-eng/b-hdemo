scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/order/search/OrderSearchExtnUI", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!isccs/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!isccs/utils/SharedComponentUtils", "scbase/loader!isccs/utils/UIUtils", "scbase/loader!isccs/utils/ModelUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!isccs/utils/EventUtils", "scbase/loader!isccs/utils/WidgetUtils"], function(_dojodeclare, _extnOrderSearchExtnUI, _scBaseUtils, _scModelUtils, _isccsBaseTemplateUtils, _scScreenUtils, _isccsSharedComponentUtils, _isccsUIUtils, _isccsModelUtils, _scWidgetUtils, _scUserprefs, _extnExpicientUtils, _scEventUtils, _isccsEventUtils, _isccsWidgetUtils) {
    return _dojodeclare("extn.order.search.OrderSearchExtn", [_extnOrderSearchExtnUI], {
        // custom code here
        extn_CardValidation: function(event, bEvent, ctrl, args) {
            var cardNo = _scWidgetUtils.getValue(this, "extn_cardNo");
            if (!(_scBaseUtils.isVoid(cardNo))) {
                _scWidgetUtils.setWidgetMandatory(this, "txt_fName");
                _scWidgetUtils.setWidgetMandatory(this, "txt_lName");
            } else {
                _scWidgetUtils.setWidgetNonMandatory(this, "txt_fName");
                _scWidgetUtils.setWidgetNonMandatory(this, "txt_lName");
            }
        },
        extn_upcSearch: function(event, bEvent, ctrl, args) {
            var targetModel = null;
            var dummyModel = {};
            _scScreenUtils.setModel(this, "extn_additionalData_output", dummyModel);
            targetModel = _scScreenUtils.getTargetModel(this, "extn_additionalData_input", null);
            var upc = _scModelUtils.getStringValueFromPath("Order.UPC", targetModel);
            var tracking = _scModelUtils.getStringValueFromPath("Order.Tracking", targetModel);
            if (!_scBaseUtils.isVoid(tracking)) {
                var trackingModel = _scBaseUtils.getNewModelInstance();
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.TrackingNo", tracking, trackingModel);
                _isccsUIUtils.callApi(this, trackingModel, "extn_getTracking_ref", null);
                _scEventUtils.stopEvent(bEvent);
            } else {
                var model = _scScreenUtils.getModel(this, "extn_additionalData_output");
                if (_scBaseUtils.isVoid(model)) {
                    model = _scBaseUtils.getNewModelInstance();
                }
                _scModelUtils.setStringValueAtModelPath("Order.TrackingNo", "", model);
                _scScreenUtils.setModel(this, "extn_additionalData_output", model);
            }
            if (!_scBaseUtils.isVoid(upc)) {
                var organization = _scUserprefs.getUserOrganization();
                var enterpriseModel = _scScreenUtils.getTargetModel(this, "getAdvancedOrderList_input", null);
                var enterprise = _scModelUtils.getStringValueFromPath("Order.EnterpriseCode", enterpriseModel);
                var catalogOrgCode = _scModelUtils.getStringValueFromPath("Organization.CatalogOrganizationCode", organization);
                var itemModel = _scBaseUtils.getNewModelInstance();
                if (_scBaseUtils.equals(catalogOrgCode, "DEFAULT")) {
                    _scModelUtils.setStringValueAtModelPath("Item.OrganizationCode", enterprise, itemModel);
                } else {
                    _scModelUtils.setStringValueAtModelPath("Item.OrganizationCode", catalogOrgCode, itemModel);
                }
                _scModelUtils.setStringValueAtModelPath("Item.ItemAliasList.ItemAlias.AliasName", "ACTIVE_UPC", itemModel);
                _scModelUtils.setStringValueAtModelPath("Item.ItemAliasList.ItemAlias.AliasValue", upc, itemModel);
                _isccsUIUtils.callApi(this, itemModel, "extn_getItem_ref", null);
                _scEventUtils.stopEvent(bEvent);
            } else {
                var model1 = _scScreenUtils.getModel(this, "extn_additionalData_output");
                if (_scBaseUtils.isVoid(model1)) {
                    model1 = _scBaseUtils.getNewModelInstance();
                }
                _scModelUtils.setStringValueAtModelPath("Order.UPC", "", model1);
                _scScreenUtils.setModel(this, "extn_additionalData_output", model1);
            }
           var orderModel = _scScreenUtils.getTargetModel(this, "getAdvancedOrderList_input", null);
            var svcNo= _scModelUtils.getStringValueFromPath("Order.PaymentMethod.SvcNo", orderModel);
            if(!_scBaseUtils.isVoid(svcNo)){
                    this.SST_invokeApi();
            }
            
        },
        extn_invokeApiOnEnter: function(event, bEvent, ctrl, args) {
            if (_isccsEventUtils.isEnterPressed(event)) {
                _isccsWidgetUtils.validateWidget(this, ctrl);
                this.extn_upcSearch(event, bEvent, ctrl, args);
            }
        },
        extn_afterBehaviorMashupCall: function(event, bEvent, ctrl, args) {
            var targetModel = null;
            var flag = false;
            targetModel = _scScreenUtils.getTargetModel(this, "extn_additionalData_input", null);
            var upc = _scModelUtils.getStringValueFromPath("Order.UPC", targetModel);
            var trackingNo = _scModelUtils.getStringValueFromPath("Order.Tracking", targetModel);
            var additionalModel = _scScreenUtils.getModel(this, "extn_additionalData_output");
            var isItemMashupAvailable = _extnExpicientUtils.isMashupAvailable("extn_getItem_ref", args);
            if (isItemMashupAvailable) {
                if (!_scBaseUtils.isVoid(trackingNo)) {
                    if (!_scBaseUtils.isVoid(additionalModel)) {
                        var trackingVal = _scModelUtils.getStringValueFromPath("Order.TrackingNo", additionalModel);
                        if (!_scBaseUtils.isVoid(trackingVal)) {
                            flag = false;
                        } else {
                            flag = true;
                        }
                    } else {
                        flag = true;
                    }
                }
                var getItemListModelOutput = null;
                getItemListModelOutput = _extnExpicientUtils.getMashupOutputForRefID("extn_getItem_ref", args);
                if (!_scBaseUtils.isVoid(getItemListModelOutput)) {
                    var itemModel = null;
                    itemModel = _scModelUtils.getModelObjectFromPath("ItemList.Item", getItemListModelOutput);
                    var itemID = null;
                    if (!_scBaseUtils.isVoid(itemModel)) {
                        for (var i = 0; i < itemModel.length; i++) {
                            var a = null;
                            var referenceModel = null;
                            referenceModel = _scBaseUtils.getArrayItemByIndex(itemModel, i);
                            itemID = _scModelUtils.getStringValueFromPath("ItemID", referenceModel);
                            var refModel = _scScreenUtils.getModel(this, "extn_additionalData_output");
                            if (_scBaseUtils.isVoid(refModel)) {
                                refModel = _scBaseUtils.getNewModelInstance();
                            }
                            _scModelUtils.setStringValueAtModelPath("Order.UPC", itemID, refModel);
                        }
                        _scScreenUtils.setModel(this, "extn_additionalData_output", refModel);
                        if (_scBaseUtils.equals(flag, false)) {
                            this.SST_invokeApi();
                        }
                    } else {
                        var refModelUpc = _scScreenUtils.getModel(this, "extn_additionalData_output");
                        if (_scBaseUtils.isVoid(refModelUpc)) {
                            refModelUpc = _scBaseUtils.getNewModelInstance();
                        }
                        _scModelUtils.setStringValueAtModelPath("Order.UPC", "WrongVal", refModelUpc);
                        _scScreenUtils.setModel(this, "extn_additionalData_output", refModelUpc);
                        if (_scBaseUtils.equals(flag, false)) {
                            this.SST_invokeApi();
                        }
                    }
                }
            }
            var isTrackingMashupAvailable = _extnExpicientUtils.isMashupAvailable("extn_getTracking_ref", args);
            if (isTrackingMashupAvailable) {
                if (!_scBaseUtils.isVoid(upc)) {
                    if (!_scBaseUtils.isVoid(additionalModel)) {
                        var upcVal = _scModelUtils.getStringValueFromPath("Order.UPC", additionalModel);
                        if (!_scBaseUtils.isVoid(upcVal)) {
                            flag = false;
                        } else {
                            flag = true;
                        }
                    } else {
                        flag = true;
                    }
                }
                var getOrderOutput = null;
                getOrderOutput = _extnExpicientUtils.getMashupOutputForRefID("extn_getTracking_ref", args);
                if (!_scBaseUtils.isVoid(getOrderOutput)) {
                    var shipments = null;
                    shipments = _scModelUtils.getModelObjectFromPath("Shipments.Shipment", getOrderOutput);
                    var orderkey = null;
                    if (!_scBaseUtils.isVoid(shipments)) {
                        for (var i = 0; i < shipments.length; i++) {
                            var referenceModel1 = null;
                            referenceModel1 = _scBaseUtils.getArrayItemByIndex(shipments, i);
                            if (!_scBaseUtils.isVoid(referenceModel1)) {
                                orderkey = _scModelUtils.getStringValueFromPath("OrderHeaderKey", referenceModel1);
                                if (_scBaseUtils.isVoid(orderkey)) {
                                    var shipmentLines = null;
                                    shipmentLines = _scModelUtils.getModelObjectFromPath("ShipmentLines.ShipmentLine", referenceModel1);
                                    var shipmentLinesList = null;
                                    shipmentLinesList = _scBaseUtils.getArrayItemByIndex(shipmentLines, 0);
                                    orderkey = _scModelUtils.getStringValueFromPath("OrderLine.OrderHeaderKey", shipmentLinesList);
                                }
                                var refModel = _scScreenUtils.getModel(this, "extn_additionalData_output");
                                if (_scBaseUtils.isVoid(refModel)) {
                                    refModel = _scBaseUtils.getNewModelInstance();
                                }
                                _scModelUtils.setStringValueAtModelPath("Order.TrackingNo", orderkey, refModel);
                            } else {
                                var refModelTracking = _scScreenUtils.getModel(this, "extn_additionalData_output");
                                if (_scBaseUtils.isVoid(refModelTracking)) {
                                    refModelTracking = _scBaseUtils.getNewModelInstance();
                                }
                                _scModelUtils.setStringValueAtModelPath("Order.TrackingNo", "WrongVal", refModelTracking);
                                _scScreenUtils.setModel(this, "extn_additionalData_output", refModelTracking);
                                if (_scBaseUtils.equals(flag, false)) {
                                    this.SST_invokeApi();
                                }
                            }
                        }
                        _scScreenUtils.setModel(this, "extn_additionalData_output", refModel);
                        if (_scBaseUtils.equals(flag, false)) {
                            this.SST_invokeApi();
                        }
                    } else {
                        var refModelTracking = _scScreenUtils.getModel(this, "extn_additionalData_output");
                        if (_scBaseUtils.isVoid(refModelTracking)) {
                            refModelTracking = _scBaseUtils.getNewModelInstance();
                        }
                        _scModelUtils.setStringValueAtModelPath("Order.TrackingNo", "WrongVal", refModelTracking);
                        _scScreenUtils.setModel(this, "extn_additionalData_output", refModelTracking);
                        if (_scBaseUtils.equals(flag, false)) {
                            this.SST_invokeApi();
                        }
                    }
                }
            }
        }
    });
});
