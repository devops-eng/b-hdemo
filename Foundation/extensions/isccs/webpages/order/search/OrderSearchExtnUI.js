
scDefine(["dojo/text!./templates/OrderSearchExtn.html","scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/_base/lang","scbase/loader!dojo/text","scbase/loader!idx/form/CheckBox","scbase/loader!idx/form/DateTextBox","scbase/loader!idx/form/FilteringSelect","scbase/loader!idx/form/TextBox","scbase/loader!idx/layout/ContentPane","scbase/loader!idx/layout/TitlePane","scbase/loader!sc/plat","scbase/loader!sc/plat/dojo/binding/CheckBoxDataBinder","scbase/loader!sc/plat/dojo/binding/ComboDataBinder","scbase/loader!sc/plat/dojo/binding/CurrencyDataBinder","scbase/loader!sc/plat/dojo/binding/DateDataBinder","scbase/loader!sc/plat/dojo/binding/SimpleDataBinder","scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!sc/plat/dojo/widgets/Label"]
 , function(			 
			    templateText
			 ,
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojolang
			 ,
			    _dojotext
			 ,
			    _idxCheckBox
			 ,
			    _idxDateTextBox
			 ,
			    _idxFilteringSelect
			 ,
			    _idxTextBox
			 ,
			    _idxContentPane
			 ,
			    _idxTitlePane
			 ,
			    _scplat
			 ,
			    _scCheckBoxDataBinder
			 ,
			    _scComboDataBinder
			 ,
			    _scCurrencyDataBinder
			 ,
			    _scDateDataBinder
			 ,
			    _scSimpleDataBinder
			 ,
			    _scBaseUtils
			 ,
			    _scLabel
){
return _dojodeclare("extn.order.search.OrderSearchExtnUI",
				[], {
			templateString: templateText
	
	
	
	
	
	
					,	
	namespaces : {
		targetBindingNamespaces :
		[
			{
	  value: 'extn_notCancelledStatus'
						,
	  scExtensibilityArrayItemId: 'extn_TargetNamespaces_2'
						
			}
			,
			{
	  value: 'extn_additionalData_input'
						,
	  scExtensibilityArrayItemId: 'extn_TargetNamespaces_3'
						
			}
			
		],
		sourceBindingNamespaces :
		[
			{
	  value: 'extn_additionalData_output'
						,
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_7'
						
			}
			
		]
	}

	
	,
	hotKeys: [ 
	]

,events : [
	]

,subscribers : {

local : [

{
	  eventId: 'afterBehaviorMashupCall'

,	  sequence: '51'




,handler : {
methodName : "extn_afterBehaviorMashupCall"

 
}
}
,
{
	  eventId: 'SST_SearchButton_onClick'

,	  sequence: '19'




,handler : {
methodName : "extn_upcSearch"

 
}
}
,
{
	  eventId: 'txt_orderNo2_onKeyUp'

,	  sequence: '19'




,handler : {
methodName : "extn_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'txt_custPoNo_onKeyUp'

,	  sequence: '19'




,handler : {
methodName : "extn_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'txt_orderDateFrom_onKeyUp'

,	  sequence: '19'




,handler : {
methodName : "extn_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'txt_orderDateTo_onKeyUp'

,	  sequence: '19'




,handler : {
methodName : "extn_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'cmbHoldType_onKeyUp'

,	  sequence: '19'




,handler : {
methodName : "extn_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'SST_OrderBy_onKeyUp'

,	  sequence: '19'




,handler : {
methodName : "extn_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'extn_UPC_onKeyUp'

,	  sequence: '51'




,handler : {
methodName : "extn_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'txt_postalCode_onKeyUp'

,	  sequence: '19'




,handler : {
methodName : "extn_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'txt_lName_onKeyUp'

,	  sequence: '19'




,handler : {
methodName : "extn_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'txt_fName_onKeyUp'

,	  sequence: '19'




,handler : {
methodName : "extn_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'txt_custPhone_onKeyUp'

,	  sequence: '19'




,handler : {
methodName : "extn_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'txt_custEmailId_onKeyUp'

,	  sequence: '19'




,handler : {
methodName : "extn_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'extn_tracking_onKeyUp'

,	  sequence: '51'




,handler : {
methodName : "extn_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'extn_department_onKeyUp'

,	  sequence: '51'




,handler : {
methodName : "extn_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'extn_cardNo_onKeyUp'

,	  sequence: '52'




,handler : {
methodName : "SST_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'extn_placingAssociate_onKeyUp'

,	  sequence: '51'




,handler : {
methodName : "SST_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'extn_storeOrder_onKeyUp'

,	  sequence: '51'




,handler : {
methodName : "SST_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'extn_placingAssociate_onKeyUp'

,	  sequence: '19'




,handler : {
methodName : "extn_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'extn_storeOrder_onKeyUp'

,	  sequence: '19'




,handler : {
methodName : "extn_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'extn_cardNo_onKeyUp'

,	  sequence: '19'




,handler : {
methodName : "extn_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'extn_TransactionId_onKeyUp'

,	  sequence: '51'




,handler : {
methodName : "SST_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'extn_TransactionId_onKeyUp'

,	  sequence: '19'




,handler : {
methodName : "extn_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'extn_tracking_onKeyDown'

,	  sequence: '51'




,handler : {
methodName : "extn_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'extn_giftcardno_onKeyUp'

,	  sequence: '51'




,handler : {
methodName : "extn_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'extn_UPC_onKeyDown'

,	  sequence: '51'




,handler : {
methodName : "extn_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'extn_TransactionId_onKeyDown'

,	  sequence: '51'




,handler : {
methodName : "extn_invokeApiOnEnter"

 
}
}
,
{
	  eventId: 'extn_TransactionId_onKeyDown'

,	  sequence: '52'




,handler : {
methodName : "SST_invokeApiOnEnter"

 
}
}

]
}

});
});


