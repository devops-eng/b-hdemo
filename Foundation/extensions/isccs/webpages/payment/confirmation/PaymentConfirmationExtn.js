scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/payment/confirmation/PaymentConfirmationExtnUI", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!isccs/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!isccs/utils/UIUtils", "scbase/loader!isccs/utils/ModelUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!isccs/utils/OrderUtils"], function(_dojodeclare, _extnPaymentConfirmationExtnUI, scScreenUtils, scWidgetUtils, scBaseUtils, isccsBaseTemplateUtils, scModelUtils, isccsUIUtils, isccsModelUtils, scEventUtils, isccsOrderUtils) {
    return _dojodeclare("extn.payment.confirmation.PaymentConfirmationExtn", [_extnPaymentConfirmationExtnUI], {
        // custom code here
        extn_fundsAvailable: {},
        extn_fundsAvailableModel: '',
        extn_isChangeOrderPayment: 'N',
        extn_DiscountUpdated: '',
        // event id : 'afterScreenInit'
        //  Description : Checking the Enterprise Code and Rewards information is displayed only for Bay Organization. 
        extn_initializerewards: function(event, bEvent, ctrl, args) {
            var OrderObj = scScreenUtils.getModel(this, "paymentConfirmation_getCompleteOrderDetails_Output");
            var orderLineList = scModelUtils.getStringValueFromPath("Order.OrderLines.OrderLine", OrderObj);
            var documentType = scModelUtils.getStringValueFromPath("Order.DocumentType", OrderObj);
            if (documentType == "0001") {
                if (OrderObj.Order.EnterpriseCode == "BAY") {
                    scWidgetUtils.showWidget(this, "extn_rewardsContentPane", false, null);
                    scWidgetUtils.showWidget(this, "extn_rewardsAccNo", false, null);
                    scWidgetUtils.showWidget(this, "extn_getRewardsButton", false, null);
                    this.extn_CheckRewardsOnOrder(OrderObj);
                }
                var PaymentMethods = scModelUtils.getStringValueFromPath("Order.PaymentMethods", OrderObj);
                var PaymentMethodsList = scModelUtils.getStringValueFromPath("PaymentMethod", PaymentMethods);
                //Custom Code for cancel
                var screenInput = scScreenUtils.getInitialInputData(this);
                var draftOrderFlag = scModelUtils.getStringValueFromPath("Order.DraftOrderFlag", screenInput);
                if (!(scBaseUtils.equals(draftOrderFlag, "Y"))) {
                    var sPreviousOrderTotal = scModelUtils.getStringValueFromPath("Order.PriceInfo.TotalAmount", screenInput);
                    var previousOrderTotal = scModelUtils.getNumberValueFromPath("Order.PriceInfo.TotalAmount", screenInput);
                    var sOrderTotal = scModelUtils.getStringValueFromPath("Order.OverallTotals.GrandTotal", OrderObj);
                    var orderTotal = scModelUtils.getNumberValueFromPath("Order.OverallTotals.GrandTotal", OrderObj);
                    if (!(scBaseUtils.isVoid(sPreviousOrderTotal))) {
                        if (!(scBaseUtils.or(scBaseUtils.equals(previousOrderTotal, orderTotal), scBaseUtils.equals(previousOrderTotal, 0)))) {
                            var difference = 0;
                            difference = isccsOrderUtils.subtract(orderTotal, previousOrderTotal);
                        }
                    }
                    if (!(scBaseUtils.isVoid(difference))) {
                        if (!(scBaseUtils.numberEquals(difference, 0))) {
                            if (!(scBaseUtils.isVoid(PaymentMethods))) {
                                scWidgetUtils.hideWidget(this, "transferPaymentMethodHolder", false);
                                scWidgetUtils.hideWidget(this, "lblSufficientFunds", false);
                                scWidgetUtils.hideWidget(this, "btnAddPaymentMethod", false);
                            }
                        }
                    }
                }
                var Model = scBaseUtils.getNewModelInstance();
                for (var i in PaymentMethodsList) {
                    var PaymentMethod = PaymentMethodsList[i];
                    var paymentType = scModelUtils.getStringValueFromPath("PaymentType", PaymentMethod);
                    if (paymentType == "LTGC" || paymentType == "HBGC") {
                        scModelUtils.setStringValueAtModelPath("PaymentMethod.FundsAvailable", scModelUtils.getStringValueFromPath("FundsAvailable", PaymentMethod), Model);
                        scModelUtils.setStringValueAtModelPath("PaymentMethod.PaymentKey", scModelUtils.getStringValueFromPath("PaymentKey", PaymentMethod), Model);
                        scModelUtils.setStringValueAtModelPath("PaymentMethod.PaymentReference1", scModelUtils.getStringValueFromPath("PaymentReference1", PaymentMethod), Model);
                        scModelUtils.setStringValueAtModelPath("PaymentMethod.PaymentType", scModelUtils.getStringValueFromPath("PaymentType", PaymentMethod), Model);
                        scModelUtils.setStringValueAtModelPath("PaymentMethod.SvcNo", scModelUtils.getStringValueFromPath("SvcNo", PaymentMethod), Model);
                        scModelUtils.setStringValueAtModelPath("PaymentMethod.RequestedAmount", "0", Model);
                        scModelUtils.setStringValueAtModelPath("PaymentMethod.PaymentTypeGroup", scModelUtils.getStringValueFromPath("PaymentTypeGroup", PaymentMethod), Model);
                        this.callCapturePayment(Model);
                    }
                }
            }
        },
        // Description : Checking whether the Loyalty Number is stamped on the Order or not.
        extn_CheckRewardsOnOrder: function(args) {
            var OrderObj = args;
            var LoayaltyNo = scModelUtils.getStringValueFromPath("Order.Extn.ExtnLoyaltyNo", OrderObj);
            if (!scBaseUtils.isVoid(LoayaltyNo)) {
                scWidgetUtils.setValue(this, "extn_rewardsAccNo", LoayaltyNo, false);
                this.extn_CalculateRewads(OrderObj.Order.OverallTotals);
            }
        },
        // Description : Calculation of Reward points
        extn_CalculateRewads: function(args) {
            var overallTotals = args;
            var lineSubTotals = overallTotals.LineSubTotal;
            var grandDiscount = overallTotals.GrandDiscount;
            var rewards = lineSubTotals - grandDiscount;
            rewards = rewards.toString();
            scWidgetUtils.showWidget(this, "extn_rewards", false, null);
            scWidgetUtils.setValue(this, "extn_rewards", rewards, false);
        },
        // event id : 'extn_getRewardsButton_onClick' (on click of Get Rewards button)
        //  Description : To Invoke the web service 'HBCLoyaltyPointsWebServiceWebCom'  
        extn_getRewardPoints: function(event, bEvent, ctrl, args) {
            var inputModel = scBaseUtils.getTargetModel(this, "extn_getRewardPoints_ns", null);
            var accNo = scModelUtils.getStringValueFromPath("LoyaltyNo", inputModel);
            var rewardsModel = scBaseUtils.getNewModelInstance();
            // var rewardsInfoText = '&lt;RewardsInfo RewardsNumber="'+ accNo +'" CCType="0" OrderSubtotal="0" EstimatedPoints="0" TotalPointsToDate="0" xmlns="eretailwebservices.hbc.com" /&gt;';
            //var rewardsInfoText = "&lt;RewardsInfo RewardsNumber=\""+ accNo +"\" CCType=\"0\" OrderSubtotal=\"0\" EstimatedPoints=\"0\" TotalPointsToDate=\"0\" xmlns=\"eretailwebservices.hbc.com\" /&gt;"
            // scModelUtils.setStringValueAtModelPath("GetRewardsInfo.sRewardsInfoIn",rewardsInfoText ,rewardsModel);
            //scModelUtils.setStringValueAtModelPath("GetRewardsInfo.DisplayLocalizedFieldInLocale","en_US_EST",rewardsModel);
            // scModelUtils.setStringValueAtModelPath("GetRewardsInfo.sRewardsInfoIn",rewardsInfoText,rewardsModel);
            scModelUtils.setStringValueAtModelPath("GetRewardsInfo.sRewardsInfoIn.RewardsInfo.RewardsNumber", accNo, rewardsModel);
            scModelUtils.setStringValueAtModelPath("GetRewardsInfo.sRewardsInfoIn.RewardsInfo.CCType", "0", rewardsModel);
            scModelUtils.setStringValueAtModelPath("GetRewardsInfo.sRewardsInfoIn.RewardsInfo.OrderSubtotal", "0", rewardsModel);
            scModelUtils.setStringValueAtModelPath("GetRewardsInfo.sRewardsInfoIn.RewardsInfo.EstimatedPoints", "0", rewardsModel);
            scModelUtils.setStringValueAtModelPath("GetRewardsInfo.sRewardsInfoIn.RewardsInfo.TotalPointsToDate", "0", rewardsModel);
            //scModelUtils.setStringValueAtModelPath("GetRewardsInfo.sRewardsInfoIn.RewardsInfo.xmlns","eretailwebservices.hbc.com",rewardsModel);
            isccsUIUtils.callApi(this, rewardsModel, "extn_GetRewardPointsMashup", null);
        },
        // event id : 'onExtnMashupCompletion' 
        //  Description : Checking for Valid Loyalty Number from web Service, stamping it on the Order and calculating the reward points
        extn_getLoyaltyNumber: function(event, bEvent, ctrl, args) {
            var output = args.mashupArray;
            var orderObj = scScreenUtils.getModel(this, "paymentConfirmation_getCompleteOrderDetails_Output");
            var OrderPaymentMethodsList = scModelUtils.getStringValueFromPath("Order.PaymentMethods.PaymentMethod", orderObj);
            for (var index in output) {
                if (output[index].mashupRefId == "extn_GetRewardPointsMashup") {
                    var rewardsOutput = output[index].mashupRefOutput;
                    var strResponseXML = rewardsOutput.GetRewardsInfoResponse.GetRewardsInfoResult;
                    var strsRewardsInfoInXML = rewardsOutput.GetRewardsInfoResponse.sRewardsInfoIn;
                    var strCode = strResponseXML.split(" ");
                    var codeConst = "Code=\"0\"";
                    var inputModel = scBaseUtils.getNewModelInstance();
                    if (scBaseUtils.containsInArray(strCode, codeConst)) {
                        var accNo = scWidgetUtils.getValue(this, "extn_rewardsAccNo");
                        scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", orderObj.Order.OrderHeaderKey, inputModel);
                        scModelUtils.setStringValueAtModelPath("Order.Extn.ExtnLoyaltyNo", accNo, inputModel);
                        isccsUIUtils.callApi(this, inputModel, "extn_changeOrder", null);
                    } else {
                        var sMessage = null;
                        sMessage = scScreenUtils.getString(this, "extn_Invalid_Reward_Acc");
                        isccsBaseTemplateUtils.showMessage(this, sMessage, "error");
                        scWidgetUtils.hideWidget(this, "extn_rewards", false, null);
                        //isccsBaseTemplateUtils.showMessage(this,"Invalid Hudson's Bay Reward Account Number entered. Please enter a valid Account Number.","error", null);
                        sc.plat.dojo.utils.EventUtils.stopEvent("extn_getRewardsButton_onClick");
                        // return;
                    }
                }
                if (output[index].mashupRefId == "extn_ExecutePaymentTransactions") {
                    var Message = "";
                    var PaymentError = "N";
                    var PaymentMethods = output[index].mashupRefOutput;
                    //if(PaymentMethods.length>0)
                    PaymentMethodsList = scModelUtils.getStringValueFromPath("PaymentMethods.PaymentMethod", PaymentMethods);
                    for (var i in PaymentMethodsList) {
                        var PaymentMethod = PaymentMethodsList[i];
                        var PaymentDetails = scModelUtils.getStringValueFromPath("PaymentDetails", PaymentMethod);
                        var AuthReturnCode = scModelUtils.getStringValueFromPath("AuthReturnCode", PaymentDetails);
                        if (!(AuthReturnCode == "APPROVED")) {
                            var AuthReturnCode = scModelUtils.getStringValueFromPath("AuthReturnCode", PaymentDetails);
                            PaymentError = "Y";
                            if (AuthReturnCode == "HARD_DECLINE") {
                                Message = scScreenUtils.getString(this, "extn_Hard_Decline");
                            } else {
                                Message = AuthReturnCode;
                                //scScreenUtils.getString(this, "extn_Invalid_Payment");     
                            }
                        }
                    }
                    if (PaymentError == "N") {
                        //this.checkProcessOrderPaymentsErrors(PaymentMethods); 
                        var inputModel = scBaseUtils.getNewModelInstance();
                        var PaymentDetails = scModelUtils.getStringValueFromPath("PaymentDetails", PaymentMethods);
                        scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", scModelUtils.getStringValueFromPath("PaymentMethods.OrderHeaderKey", PaymentMethods), inputModel);
                        //PaymentDetailsList
                        for (var i in OrderPaymentMethodsList) {
                            var OrderPaymentMethod = OrderPaymentMethodsList[i];
                            for (var i in PaymentMethodsList) {
                                var PaymentMethod = PaymentMethodsList[i];
                                var paymentType = scModelUtils.getStringValueFromPath("PaymentType", PaymentMethod);
                                if (paymentType == "LTGC" || paymentType == "HBGC") {
                                    scModelUtils.setStringValueAtModelPath("MaxChargeLimit", scModelUtils.getStringValueFromPath("RequestAmount", PaymentMethod), PaymentMethod);
                                    if (OrderPaymentMethod.SvcNo == PaymentMethod.SvcNo) {
                                        scModelUtils.setStringValueAtModelPath("PaymentKey", scModelUtils.getStringValueFromPath("PaymentKey", OrderPaymentMethod), PaymentMethod);
                                    }
                                } else if (OrderPaymentMethod.CreditCardNo == PaymentMethod.CreditCardNo) {
                                    scModelUtils.setStringValueAtModelPath("PaymentKey", scModelUtils.getStringValueFromPath("PaymentKey", OrderPaymentMethod), PaymentMethod);
                                    scModelUtils.setStringValueAtModelPath("DisplayCreditCardNo", scModelUtils.getStringValueFromPath("DisplayCreditCardNo", OrderPaymentMethod), PaymentMethod);
                                }
                                scModelUtils.setStringValueAtModelPath("UnlimitedCharges", "N", PaymentMethod);
                            }
                        }
                        scModelUtils.setStringValueAtModelPath("Order.PaymentMethods.PaymentMethod", scModelUtils.getStringValueFromPath("PaymentMethods.PaymentMethod", PaymentMethods), inputModel);
                        //scModelUtils.setStringValueAtModelPath("Order.Notes.Note.ContactReference","", inputModel);
                        //scModelUtils.setStringValueAtModelPath("Order.Notes.Note.ContactType","", inputModel);
                        //scModelUtils.setStringValueAtModelPath("Order.Notes.Note.ContactUser", scModelUtils.getStringValueFromPath("PaymentMethods.PaymentMethod", PaymentMethods), inputModel);
                        //scModelUtils.setStringValueAtModelPath("Order.Notes.Note.NoteText", scModelUtils.getStringValueFromPath("PaymentMethods.PaymentMethod", PaymentMethods), inputModel);
                        //scModelUtils.setStringValueAtModelPath("Order.Notes.Note.Priority", scModelUtils.getStringValueFromPath("PaymentMethods.PaymentMethod", PaymentMethods), inputModel);
                        //scModelUtils.setStringValueAtModelPath("Order.Notes.Note.ReasonCode","YCD_ORDER_ENTRY", inputModel);
                        //scModelUtils.setStringValueAtModelPath("Order.Notes.Note.VisibleToAll","Y", inputModel);
                        this.extn_isChangeOrderPayment = "Y";
                        isccsUIUtils.callApi(this, inputModel, "extn_changeOrder", null);
                    } else {
                        isccsBaseTemplateUtils.showMessage(this, Message, "error", null);
                    }
                } else if (output[index].mashupRefId == "extn_changeOrder") {
                    if (this.extn_isChangeOrderPayment == "Y") {
                        this.checkProcessOrderPaymentsErrors(output[index].mashupRefOutput);
                        return;
                    }
                    this.extn_CalculateRewads(orderObj.Order.OverallTotals);
                    var Order = output[index].mashupRefOutput.Order;
                    var DocumentType = scModelUtils.getStringValueFromPath("DocumentType", Order);
                    if (DocumentType == "0003") {
                        this.updateEditorHeader();
                        this.refreshChargeUpdates();
                    }
                } else if (output[index].mashupRefId == "extn_ChangeOrder_Return") {
                    this.updateEditorHeader();
                    this.refreshChargeUpdates();
                }
                //Refund Changes
                if (output[index].mashupRefId == "extn_setExtnIsOrgPmtRefund") {
                    //Resume execution of Save() function on completion of "extn_setExtnIsOrgPmtRefund" mashup
                    //  this.save(eventGlobal,bEventGlobal,ctrlGlobal,argsGlobal);
                    var orderObj = scScreenUtils.getModel(this, "paymentConfirmation_getCompleteOrderDetails_Output");
                    var orderLineList = scModelUtils.getStringValueFromPath("Order.OrderLines.OrderLine", orderObj);
                    var paypalCheckFlag = true;
                    if (!(scBaseUtils.isVoid(orderLineList))) {
                        for (var index in orderLineList) {
                            var paymentTypeList = scModelUtils.getStringValueFromPath("DerivedFromOrder.PaymentMethods.PaymentMethod", orderLineList[index]);
                            for (var paymentIndex in paymentTypeList) {
                                var paymentType = scModelUtils.getStringValueFromPath("PaymentType", paymentTypeList[paymentIndex]);
                                if ((scBaseUtils.equals(paymentType, "PAYPAL_OFF5") || scBaseUtils.equals(paymentType, "PAYPAL_SAKS"))) {
                                    var extnRadiobtnValue = scWidgetUtils.getValue(this, "extn_radiobtn_refundOptions");
                                    if (scBaseUtils.equals(extnRadiobtnValue, "Y")) {
                                        paypalCheckFlag = false;
                                        isccsBaseTemplateUtils.showMessage(this, "Order has been paid through PayPal, Perform this DC Returns through POS or proceed by selecting Refund option Gift Card", "error", null);
                                        sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
                                        break;
                                    }
                                }
                            }
                            if (paypalCheckFlag) {
                                this.save(eventGlobal, bEventGlobal, ctrlGlobal, argsGlobal);
                            }
                        }
                    }
                }
                //Refund Changes end
            }
        },
        callCapturePayment: function(paymentMethod) {
            var orderModel = null;
            orderModel = scScreenUtils.getModel(this, "paymentConfirmation_getCompleteOrderDetails_Output");
            var orderHeaderKey = null;
            orderHeaderKey = scModelUtils.getStringValueFromPath("Order.OrderHeaderKey", orderModel);
            var capturePaymentModel = null;
            capturePaymentModel = scBaseUtils.getNewModelInstance();
            scModelUtils.setStringValueAtModelPath("CapturePayment.OrderHeaderKey", orderHeaderKey, capturePaymentModel);
            var orderObj = scScreenUtils.getModel(this, "capturePayment_Output");
            var paymentMethodObj = orderObj.Order.PaymentMethods.PaymentMethod;
            if (!(scBaseUtils.isVoid(paymentMethod))) {
                var sRequestedAmount = "";
                sRequestedAmount = scModelUtils.getStringValueFromPath("PaymentMethod.RequestedAmount", paymentMethod);
                var paymentType = scModelUtils.getStringValueFromPath("PaymentMethod.PaymentType", paymentMethod);
                if (paymentType == "LTGC" || paymentType == "HBGC") {
                    if (scBaseUtils.isVoid(sRequestedAmount)) {
                        sRequestedAmount = scModelUtils.getStringValueFromPath("Order.ChargeTransactionDetails.RemainingAmountToAuth", orderModel)
                    }
                    var total = scModelUtils.getStringValueFromPath("Order.OverallTotals.GrandTotal", orderModel);
                    var fundsAvailableValue = scModelUtils.getStringValueFromPath("PaymentMethod.FundsAvailable", paymentMethod);
                    var svcNoValue = scModelUtils.getStringValueFromPath("PaymentMethod.SvcNo", paymentMethod);
                    scModelUtils.setStringValueAtModelPath("SvcNo", svcNoValue, this.extn_fundsAvailable);
                    scModelUtils.setStringValueAtModelPath("fundsAvailableValue", fundsAvailableValue, this.extn_fundsAvailable);
                    // scModelUtils.setStringValueAtModelPath("PaymentMethod.ChargeSequence", "1", paymentMethod);
                    if (Number(sRequestedAmount) >= Number(total)) {
                        sRequestedAmount = total;
                        if (Number(sRequestedAmount) > Number(fundsAvailableValue)) {
                            sRequestedAmount = fundsAvailableValue;
                        }
                    } else {
                        if (Number(sRequestedAmount) > Number(fundsAvailableValue)) {
                            sRequestedAmount = fundsAvailableValue;
                        }
                    }
                    isccsModelUtils.removeAttributeFromModel("PaymentMethod.FundsAvailable", paymentMethod);
                    scModelUtils.setStringValueAtModelPath("PaymentMethod.RequestedAmount", sRequestedAmount, paymentMethod);
                }
                if (!(scBaseUtils.or(scModelUtils.hasAttributeInModelPath("PaymentMethod.IsCorrection", paymentMethod), scBaseUtils.isVoid(sRequestedAmount)))) {
                    scModelUtils.setStringValueAtModelPath("PaymentMethod.IsCorrection", "Y", paymentMethod);
                }
                scModelUtils.setStringValueAtModelPath("PaymentMethod.Operation", "Manage", paymentMethod);
                isccsModelUtils.removeAttributeFromModel("PaymentMethod.PaymentTypeGroup", paymentMethod);
                isccsModelUtils.removeAttributeFromModel("PaymentMethod.SecureAuthenticationCode", paymentMethod);
                scModelUtils.addModelToModelPath("CapturePayment.PaymentMethods", paymentMethod, capturePaymentModel);
                isccsBaseTemplateUtils.showMessage(this, "PaymentMethodsUpdated", "success", null);
            }
            isccsUIUtils.callApi(this, capturePaymentModel, "capturePayment", null);
        },
        updateRemainingAmountToAuth: function(modelOutput) {
            var PaymentMethodsModel = null;
            PaymentMethodsModel = scModelUtils.createNewModelObjectWithRootKey("PaymentMethods");
            scModelUtils.addModelToModelPath("Order.PaymentMethods", scModelUtils.getModelObjectFromPath("Order.PaymentMethods", modelOutput), PaymentMethodsModel);
            scModelUtils.setStringValueAtModelPath("Order.ChargeTransactionDetails.RemainingAmountToAuth", scModelUtils.getStringValueFromPath("Order.ChargeTransactionDetails.RemainingAmountToAuth", modelOutput), PaymentMethodsModel);
            if (!scBaseUtils.isVoid(this.extn_fundsAvailable)) {
                for (var num in PaymentMethodsModel.Order.PaymentMethods.PaymentMethod) {
                    if (this.extn_fundsAvailable.SvcNo == PaymentMethodsModel.Order.PaymentMethods.PaymentMethod[num].SvcNo) {
                        scModelUtils.setStringValueAtModelPath("FundsAvailable", this.extn_fundsAvailable.fundsAvailableValue, PaymentMethodsModel.Order.PaymentMethods.PaymentMethod[num]);
                    }
                }
            }
            scScreenUtils.setModel(this, "capturePayment_Output", PaymentMethodsModel, null);
            this.updateDisplay(modelOutput);
            scWidgetUtils.hideWidget(this, "btnCreateExchange", false, null);
            //Custom Code for refresh
            var GrandAdjustmentsWithoutTotalShipping = scModelUtils.getStringValueFromPath("Order.OverallTotals.GrandAdjustmentsWithoutTotalShipping", modelOutput);
            var screenInput = scScreenUtils.getInitialInputData(this);
            var modelObject = scScreenUtils.getModel(this, "paymentConfirmation_getCompleteOrderDetails_Output");
            var sPreviousOrderTotal = scModelUtils.getStringValueFromPath("Order.PriceInfo.TotalAmount", screenInput);
            var previousOrderTotal = scModelUtils.getNumberValueFromPath("Order.PriceInfo.TotalAmount", screenInput);
            var sOrderTotal = scModelUtils.getStringValueFromPath("Order.OverallTotals.GrandTotal", modelObject);
            var orderTotal = scModelUtils.getNumberValueFromPath("Order.OverallTotals.GrandTotal", modelObject);
            if (!(scBaseUtils.isVoid(sPreviousOrderTotal))) {
                if (!(scBaseUtils.equals(previousOrderTotal, orderTotal))) {
                    var difference = 0;
                    difference = isccsOrderUtils.subtract(orderTotal, previousOrderTotal);
                }
            }
            if (!(scBaseUtils.isVoid(difference))) {
                if ((!(scBaseUtils.numberEquals(difference, 0)))) {
                    // if(!(GrandAdjustmentsWithoutTotalShipping==0))
                    //  {
                    if (this.extn_DiscountUpdated != difference) {
                        var PaymentMethods = scModelUtils.getStringValueFromPath("Order.PaymentMethods", modelObject);
                        if (!(scBaseUtils.isVoid(PaymentMethods))) {
                            var PaymentMethodsList = scModelUtils.getStringValueFromPath("PaymentMethod", PaymentMethods);
                            this.extn_DiscountUpdated = difference;
                            this.extn_PaymentRefresh(PaymentMethodsList);
                            // }     
                        }
                    }
                }
            }
        },
        //Set ExtnIsOrgPmtRefund attribute based on refund option is selected
        saveExtn: function(event, bEvent, ctrl, args) {
            eventGlobal = event;
            bEventGlobal = bEvent;
            ctrlGlobal = ctrl;
            argsGlobal = args;
            scEventUtils.fireEventToParent(this, "setWizardInput", null);
            var screenInput = scScreenUtils.getInitialInputData(this);
            var retOrderHeaderKey = scModelUtils.getStringValueFromPath("Order.OrderHeaderKey", screenInput);
            var vDocumentType = scModelUtils.getStringValueFromPath("Order.DocumentType", screenInput);
            if (vDocumentType == "0003") {
                var newReturnOrderModel = scBaseUtils.getNewModelInstance();
                scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", retOrderHeaderKey, newReturnOrderModel);
                //check the refund option selected
                var extnIsOrgPmtRefund = scWidgetUtils.getValue(this, "extn_radiobtn_refundOptions");
                scModelUtils.setStringValueAtModelPath("Order.Extn.ExtnIsOrgPmtRefund", extnIsOrgPmtRefund, newReturnOrderModel)
                isccsUIUtils.callApi(this, newReturnOrderModel, "extn_setExtnIsOrgPmtRefund", null);
                //stop saveCurrentPage event(i.e. stop save function execution) until extn_setExtnIsOrgPmtRefund mashup execution completes
                sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
            }
        },
        save: function(event, bEvent, ctrl, args) {
            var confirmOrder = true;
            var screenInput = null;
            var apiInput = null;
            var orderHeaderKey = null;
            var orderName = this.OrderName;
            screenInput = scScreenUtils.getInitialInputData(this);
            apiInput = scBaseUtils.getNewModelInstance();
            orderHeaderKey = scModelUtils.getStringValueFromPath("Order.OrderHeaderKey", screenInput);
            var actionPerformed = null;
            actionPerformed = scBaseUtils.getStringValueFromBean("Action", args);
            if (scBaseUtils.equals(actionPerformed, "CONFIRM")) {
                var capturePayment = null;
                capturePayment = scScreenUtils.getModel(this, "capturePayment_Output");
                var remainingAmountToAuth = 0;
                remainingAmountToAuth = scModelUtils.getNumberValueFromPath("Order.ChargeTransactionDetails.RemainingAmountToAuth", capturePayment);
                if (scBaseUtils.greaterThan(remainingAmountToAuth, 0)) {
                    isccsBaseTemplateUtils.showMessage(this, "AdditionaAmountNotZero", "error", null);
                    confirmOrder = false;
                } else {
                    var RealTimeRulesModel = null;
                    RealTimeRulesModel = scScreenUtils.getModel(this, "getRuleDetails_RealTimeAuthorization_Output");
                    var sRealTimeAuth = null;
                    sRealTimeAuth = scModelUtils.getStringValueFromPath("Rules.RuleSetValue", RealTimeRulesModel);
                    if (scBaseUtils.equals(sRealTimeAuth, "02")) {
                        var orderModel = null;
                        orderModel = scScreenUtils.getModel(this, "capturePayment_Output");
                        var listOfPaymentMethods = null;
                        listOfPaymentMethods = scModelUtils.getModelListFromPath("Order.PaymentMethods.PaymentMethod", orderModel);
                        var length = 0;
                        length = scBaseUtils.getAttributeCount(listOfPaymentMethods);
                        var paymentMethodModel = null;
                        var secureAuthCodesModel = null;
                        secureAuthCodesModel = scBaseUtils.getNewArrayInstance();
                        var secureAuthMapModel = null;
                        secureAuthMapModel = isccsUIUtils.getWizardModel(this, "secureAuthMap");
                        var secureAuthMap = null;
                        secureAuthMap = scModelUtils.getModelObjectFromPath("SecureAuthMap", secureAuthMapModel);
                        for (var counter = 0; scBaseUtils.lessThan(counter, length); counter = scBaseUtils.incrementNumber(counter, 1)) {
                            paymentMethodModel = scModelUtils.getModelFromList(listOfPaymentMethods, counter);
                            isccsModelUtils.removeAttributeFromModel("TotalCharged", paymentMethodModel);
                            isccsModelUtils.removeAttributeFromModel("TotalRefundedAmount", paymentMethodModel);
                            isccsModelUtils.removeAttributeFromModel("TotalAuthorized", paymentMethodModel);
                            isccsModelUtils.removeAttributeFromModel("CreditCardExpMonth", paymentMethodModel);
                            isccsModelUtils.removeAttributeFromModel("CreditCardExpYear", paymentMethodModel);
                            isccsModelUtils.removeAttributeFromModel("CreditCardTypeDesc", paymentMethodModel);
                            isccsModelUtils.removeAttributeFromModel("IsDefaultMethod", paymentMethodModel);
                            isccsModelUtils.removeAttributeFromModel("Currency", paymentMethodModel);
                            var paymentMethodKey = null;
                            paymentMethodKey = scBaseUtils.getAttributeValue("PaymentKey", false, paymentMethodModel);
                            var secureAuthCode = null;
                            secureAuthCode = scBaseUtils.getAttributeValue(paymentMethodKey, false, secureAuthMap);
                            if (!(scBaseUtils.isVoid(secureAuthCode))) {
                                var tmpPaymentMethod = null;
                                tmpPaymentMethod = scBaseUtils.getNewModelInstance();
                                scModelUtils.setStringValueAtModelPath("PaymentKey", paymentMethodKey, tmpPaymentMethod);
                                scModelUtils.setStringValueAtModelPath("SecureAuthenticationCode", secureAuthCode, tmpPaymentMethod);
                                scModelUtils.addModelObjectToModelList(tmpPaymentMethod, secureAuthCodesModel);
                            }
                        }
                        var newOrderModel = null;
                        newOrderModel = scModelUtils.createNewModelObjectWithRootKey("Order");
                        var exchangeOrderModel = null;
                        exchangeOrderModel = scScreenUtils.getModel(this, "exchangeOrderTotal_Output");
                        var ohkForAPI = orderHeaderKey;
                        if (!(scBaseUtils.isVoid(exchangeOrderModel))) {
                            ohkForAPI = scModelUtils.getStringValueFromPath("Order.OrderHeaderKey", exchangeOrderModel);
                            scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", orderHeaderKey, newOrderModel);
                            isccsUIUtils.callApi(this, newOrderModel, "processOrderPayments", null);
                        }
                        scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", ohkForAPI, newOrderModel);
                        if (!(scBaseUtils.isVoid(secureAuthCodesModel))) {
                            scModelUtils.addModelToModelPath("Order.PaymentMethods.PaymentMethod", secureAuthCodesModel, newOrderModel);
                        }
                        //Custom Code
                        var newExecutePaymentOrderModel = null;
                        newExecutePaymentOrderModel = scModelUtils.createNewModelObjectWithRootKey("PaymentMethods");
                        var PaymentMethodsListArray = scBaseUtils.getNewArrayInstance();
                        var CompleteOrderDetails = scScreenUtils.getModel(this, "paymentConfirmation_getCompleteOrderDetails_Output");
                        //Custom Code for Cancel Order
                        screenInput = scScreenUtils.getInitialInputData(this);
                        //var modelObject=scScreenUtils.getModel(this,"paymentConfirmation_getCompleteOrderDetails_Output");
                        var draftOrderFlag = scModelUtils.getStringValueFromPath("Order.DraftOrderFlag", screenInput);
                        if (scBaseUtils.equals(draftOrderFlag, "Y")) {
                            for (var count in listOfPaymentMethods) {
                                if (scModelUtils.getStringValueFromPath("MaxChargeLimit", listOfPaymentMethods[count]) > 0) {
                                    var PaymentMethod = scBaseUtils.getNewModelInstance();
                                    scModelUtils.setStringValueAtModelPath("BillToKey", scModelUtils.getStringValueFromPath("BillToKey", listOfPaymentMethods[count]), PaymentMethod);
                                    // scModelUtils.setStringValueAtModelPath("PaymentKey", scModelUtils.getStringValueFromPath("PaymentKey", listOfPaymentMethods[count]),PaymentMethod);
                                    scModelUtils.setStringValueAtModelPath("ChargeSequence", scModelUtils.getStringValueFromPath("ChargeSequence", listOfPaymentMethods[count]), PaymentMethod);
                                    scModelUtils.setStringValueAtModelPath("CreditCardExpDate", scModelUtils.getStringValueFromPath("CreditCardExpDate", listOfPaymentMethods[count]), PaymentMethod);
                                    scModelUtils.setStringValueAtModelPath("CreditCardName", scModelUtils.getStringValueFromPath("CreditCardName", listOfPaymentMethods[count]), PaymentMethod);
                                    scModelUtils.setStringValueAtModelPath("CreditCardNo", scModelUtils.getStringValueFromPath("CreditCardNo", listOfPaymentMethods[count]), PaymentMethod);
                                    scModelUtils.setStringValueAtModelPath("CreditCardType", scModelUtils.getStringValueFromPath("CreditCardType", listOfPaymentMethods[count]), PaymentMethod);
                                    scModelUtils.setStringValueAtModelPath("DisplayCreditCardNo", scModelUtils.getStringValueFromPath("DisplayCreditCardNo", listOfPaymentMethods[count]), PaymentMethod);
                                    scModelUtils.setStringValueAtModelPath("PaymentType", scModelUtils.getStringValueFromPath("PaymentType", listOfPaymentMethods[count]), PaymentMethod);
                                    scModelUtils.setStringValueAtModelPath("RequestAmount", scModelUtils.getStringValueFromPath("MaxChargeLimit", listOfPaymentMethods[count]), PaymentMethod);
                                    scModelUtils.setStringValueAtModelPath("OrderNo", scModelUtils.getStringValueFromPath("Order.OrderNo", screenInput), PaymentMethod);
                                    scModelUtils.setStringValueAtModelPath("OrganizationCode", scModelUtils.getStringValueFromPath("Order.EnterpriseCode", screenInput), PaymentMethod);
                                    scModelUtils.setStringValueAtModelPath("FirstName", "", PaymentMethod);
                                    scModelUtils.setStringValueAtModelPath("LastName", "", PaymentMethod);
                                    scModelUtils.setStringValueAtModelPath("MiddleName", "", PaymentMethod);
                                    scModelUtils.setStringValueAtModelPath("DocumentType", scModelUtils.getStringValueFromPath("Order.DocumentType", screenInput), PaymentMethod);
                                    scModelUtils.setStringValueAtModelPath("UnlimitedCharges", scModelUtils.getStringValueFromPath("UnlimitedCharges", listOfPaymentMethods[count]), PaymentMethod);
                                    scModelUtils.addModelToModelPath("Currency", scModelUtils.getStringValueFromPath("Order.PriceInfo.Currency", screenInput), PaymentMethod);
                                    scModelUtils.setStringValueAtModelPath("MaxChargeLimit", scModelUtils.getStringValueFromPath("MaxChargeLimit", listOfPaymentMethods[count]), PaymentMethod);
                                    var paymentType = scModelUtils.getStringValueFromPath("PaymentType", listOfPaymentMethods[count]);
                                    if (paymentType == "LTGC" || paymentType == "HBGC") {
                                        scModelUtils.setStringValueAtModelPath("SvcNo", scModelUtils.getStringValueFromPath("SvcNo", listOfPaymentMethods[count]), PaymentMethod);
                                        scModelUtils.setStringValueAtModelPath("PaymentReference1", scModelUtils.getStringValueFromPath("PaymentReference1", listOfPaymentMethods[count]), PaymentMethod);
                                        //scModelUtils.setStringValueAtModelPath("ChargeSequence","1",PaymentMethod);
                                        scModelUtils.setStringValueAtModelPath("ChargeType", "CHARGE", PaymentMethod);
                                    } else {
                                        scModelUtils.setStringValueAtModelPath("ChargeType", "AUTHORIZATION", PaymentMethod);
                                    }
                                    scModelUtils.addModelToModelPath("PersonInfoBillTo", scModelUtils.getStringValueFromPath("Order.PersonInfoBillTo", CompleteOrderDetails), PaymentMethod);
                                    scBaseUtils.appendToArray(PaymentMethodsListArray, PaymentMethod);
                                }
                            }
                            scModelUtils.addModelToModelPath("PaymentMethods.PaymentMethod", PaymentMethodsListArray, newExecutePaymentOrderModel);
                            scModelUtils.setStringValueAtModelPath("PaymentMethods.OrderHeaderKey", orderHeaderKey, newExecutePaymentOrderModel);
                            scModelUtils.setStringValueAtModelPath("PaymentMethods.DocumentType", scModelUtils.getStringValueFromPath("Order.DocumentType", screenInput), newExecutePaymentOrderModel);
                            scModelUtils.setStringValueAtModelPath("PaymentMethods.EnterpriseCode", scModelUtils.getStringValueFromPath("Order.EnterpriseCode", screenInput), newExecutePaymentOrderModel);
                            scModelUtils.setStringValueAtModelPath("PaymentMethods.SellerOrganizationCode", scModelUtils.getStringValueFromPath("Order.EnterpriseCode", screenInput), newExecutePaymentOrderModel);
                            scModelUtils.setStringValueAtModelPath("PaymentMethods.OrderNo", scModelUtils.getStringValueFromPath("Order.OrderNo", screenInput), newExecutePaymentOrderModel);
                            isccsUIUtils.callApi(this, newExecutePaymentOrderModel, "extn_ExecutePaymentTransactions", null);
                            confirmOrder = false;
                        } else {
                            var sPreviousOrderTotal = scModelUtils.getStringValueFromPath("Order.PriceInfo.TotalAmount", screenInput);
                            var previousOrderTotal = scModelUtils.getNumberValueFromPath("Order.PriceInfo.TotalAmount", screenInput);
                            var sOrderTotal = scModelUtils.getStringValueFromPath("Order.OverallTotals.GrandTotal", CompleteOrderDetails);
                            var orderTotal = scModelUtils.getNumberValueFromPath("Order.OverallTotals.GrandTotal", CompleteOrderDetails);
                            if (!(scBaseUtils.isVoid(sPreviousOrderTotal))) {
                                if (!(scBaseUtils.or(scBaseUtils.equals(previousOrderTotal, orderTotal), scBaseUtils.equals(previousOrderTotal, 0)))) {
                                    var difference = 0;
                                    difference = isccsOrderUtils.subtract(orderTotal, previousOrderTotal);
                                }
                            }
                            if (!(scBaseUtils.isVoid(difference)) && (!(scBaseUtils.numberEquals(difference, 0)))) {
                                var InputModel = scBaseUtils.getNewModelInstance();
                                scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", orderHeaderKey, InputModel);
                                this.extn_isChangeOrderPayment = "Y";
                                isccsUIUtils.callApi(this, InputModel, "extn_changeOrder", null);
                                confirmOrder = false;
                            }
                        }
                    }
                }
            } else {
                scEventUtils.fireEventToParent(this, "onSaveSuccess", null);
            }
            if (scBaseUtils.equals(confirmOrder, true)) {
                scEventUtils.fireEventToParent(this, "onSaveSuccess", null);
            }
        },
        handleMashupCompletion: function(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data) {
            for (var index in inputData) {
                var mashupId = inputData[index].mashupRefId;
                if (mashupId == "applyCoupon") {
                    var errorResponse = scModelUtils.getStringValueFromPath("response.Errors.Error", data);
                    for (var i in errorResponse) {
                        if (errorResponse[i].ErrorCode == "Invalid Promotion") {
                            var sMessage = null;
                            sMessage = scScreenUtils.getString(this, "extn_Invalid_Promotion_Code");
                            isccsBaseTemplateUtils.showMessage(this, sMessage, "error");
                            mashupContext.showError = false;
                            return true;
                        }
                    }
                }
            }
            isccsBaseTemplateUtils.handleMashupCompletion(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data, this);
        },
        extn_PaymentRefresh: function(PaymentMethodsList) {
            var CreditCard = "N";
            var GiftCard = "N";
            var LTPCCCard = "N";
            var Refund = "N";
            for (var i in PaymentMethodsList) {
                var PaymentMethod = PaymentMethodsList[i];
                var paymentType = scModelUtils.getStringValueFromPath("PaymentType", PaymentMethod);
                if (paymentType == "CREDIT_CARD_LT" || paymentType == "CREDIT_CARD_HBC") {
                    CreditCard = "Y";
                } else if (paymentType == "CREDIT_CARD_PVT_LT" || paymentType == "CREDIT_CARD_PVT_HBC") {
                    LTPCCCard = "Y";
                } else if (paymentType == "LTGC" || paymentType == "HBGC") {
                    GiftCard = "Y";
                } else {
                    Refund = "Y";
                }
            }
            var Model = scBaseUtils.getNewModelInstance();
            for (var i in PaymentMethodsList) {
                var PaymentMethod = PaymentMethodsList[i];
                var paymentType = scModelUtils.getStringValueFromPath("PaymentType", PaymentMethod);
                if (CreditCard == "Y" || LTPCCCard == "Y") {
                    if (paymentType == "CREDIT_CARD_LT" || paymentType == "CREDIT_CARD_HBC" || paymentType == "CREDIT_CARD_PVT_LT" || paymentType == "CREDIT_CARD_PVT_HBC") {
                        scModelUtils.setStringValueAtModelPath("PaymentMethod.PaymentKey", scModelUtils.getStringValueFromPath("PaymentKey", PaymentMethod), Model);
                        this.callCapturePayment(Model);
                        return;
                    }
                } else if (GiftCard == "Y") {
                    if (paymentType == "LTGC" || paymentType == "HBGC") {
                        var MaxChageLimit = scModelUtils.getStringValueFromPath("MaxChargeLimit", PaymentMethod);
                        scModelUtils.setStringValueAtModelPath("PaymentMethod.PaymentKey", scModelUtils.getStringValueFromPath("PaymentKey", PaymentMethod), Model);
                        scModelUtils.setStringValueAtModelPath("PaymentMethod.RequestedAmount", MaxChageLimit, Model);
                        this.callCapturePayment(Model);
                        return;
                    }
                }
            }
        },
        extn_RefundShippingCharge: function(event, bEvent, ctrl, args) {
            var testLabelCharge = null;
            var orderModel = scScreenUtils.getModel(this, "paymentConfirmation_getCompleteOrderDetails_Output");
            //var organizationCode = scModelUtils.getStringValueFromPath("Order.EnterpriseCode", orderModel);
            var OrderHeaderKey = scModelUtils.getStringValueFromPath("Order.OrderHeaderKey", orderModel);
            var inputModel = scBaseUtils.getNewModelInstance();
            scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", OrderHeaderKey, inputModel);
            //scModelUtils.setStringValueAtModelPath("Order.Action", "MODIFY", inputModel);
            // scModelUtils.setStringValueAtModelPath("Order.EnterpriseCode", organizationCode, inputModel);
            if (bEvent._originatingControlUId == "extn_RefundShippingCharges") {
                var checkBoxValueShipping = scWidgetUtils.getValue(this, "extn_RefundShippingCharges");
                var modelList = scBaseUtils.getNewArrayInstance();
                var OrderLines = scModelUtils.getModelObjectFromPath("Order.OrderLines.OrderLine", orderModel);
                for (var i in OrderLines) {
                    var object = scModelUtils.getModelFromList(OrderLines, i);
                    var OrderLineKey = scModelUtils.getStringValueFromPath("OrderLineKey", object);
                    var LineCharges = scModelUtils.getStringValueFromPath("DerivedFromOrderLine.LineCharges.LineCharge", object);
                    var lineTaxes = scModelUtils.getStringValueFromPath("DerivedFromOrderLine.LineTaxes.LineTax", object);
                    var returnOrderedQty = scModelUtils.getNumberValueFromPath("OrderLineTranQuantity.OrderedQty", object);
                    var returnOrderedQty = scModelUtils.getNumberValueFromPath("OrderLineTranQuantity.OrderedQty", object);
                    var orderedQty = scModelUtils.getNumberValueFromPath("DerivedFromOrderLine.OrderedQty", object);
                    var Model = scBaseUtils.getNewModelInstance();
                    var ChargePerUnit = 0;
                    for (var k in LineCharges) {
                        var LineCharge = scModelUtils.getModelFromList(LineCharges, k);
                        var ChargeCategory = scModelUtils.getStringValueFromPath("ChargeCategory", LineCharge);
                        var ChargeName = scModelUtils.getStringValueFromPath("ChargeName", LineCharge);
                        if (ChargeCategory == "SHIPPINGCHARGE") {
                            ChargePerUnit = scModelUtils.getStringValueFromPath("ChargePerUnit", LineCharge);
                            break;
                        }
                    }
                    scModelUtils.setStringValueAtModelPath("OrderLineKey", OrderLineKey, Model);
                    scModelUtils.setStringValueAtModelPath("Action", "MODIFY", Model);
                    if (checkBoxValueShipping == "on") {
                        if (ChargeCategory == "SHIPPINGCHARGE") {
                            scModelUtils.setStringValueAtModelPath("LineCharges.LineCharge.ChargeCategory", ChargeCategory, Model);
                            scModelUtils.setStringValueAtModelPath("LineCharges.LineCharge.ChargeName", ChargeName, Model);
                            scModelUtils.setStringValueAtModelPath("LineCharges.LineCharge.ChargePerUnit", ChargePerUnit, Model);
                        }
                        var taxArray = scBaseUtils.getNewArrayInstance();
                        for (var i in lineTaxes) {
                            var chargeCategory = scModelUtils.getStringValueFromPath("ChargeCategory", lineTaxes[i]);
                            if (chargeCategory != "SHIPPINGSURCHARGE") {
                                var model = scBaseUtils.getNewModelInstance();
                                scModelUtils.setStringValueAtModelPath("ChargeCategory", scModelUtils.getStringValueFromPath("ChargeCategory", lineTaxes[i]), model);
                                scModelUtils.setStringValueAtModelPath("ChargeName", scModelUtils.getStringValueFromPath("ChargeCategory", lineTaxes[i]), model);
                                scModelUtils.setStringValueAtModelPath("TaxName", scModelUtils.getStringValueFromPath("TaxName", lineTaxes[i]), model);
                                scModelUtils.setStringValueAtModelPath("TaxPercentage", scModelUtils.getStringValueFromPath("TaxPercentage", lineTaxes[i]), model);
                                var tax = scModelUtils.getNumberValueFromPath("Tax", lineTaxes[i]);
                                var newTaxamount = (returnOrderedQty / orderedQty) * tax;
                                scModelUtils.setStringValueAtModelPath("Tax", newTaxamount.toString(), model);
                                scBaseUtils.appendToArray(taxArray, model);
                            }
                        }
                        scModelUtils.setStringValueAtModelPath("LineTaxes.LineTax", taxArray, Model);
                    } else {
                        if (ChargeCategory == "SHIPPINGCHARGE") {
                            scModelUtils.setStringValueAtModelPath("LineCharges.LineCharge.ChargeCategory", ChargeCategory, Model);
                            scModelUtils.setStringValueAtModelPath("LineCharges.LineCharge.ChargeName", ChargeName, Model);
                            scModelUtils.setStringValueAtModelPath("LineCharges.LineCharge.ChargePerUnit", "0", Model);
                        }
                        var taxArray = scBaseUtils.getNewArrayInstance();
                        for (var i in lineTaxes) {
                            var chargeCategory = scModelUtils.getStringValueFromPath("ChargeCategory", lineTaxes[i]);
                            if (chargeCategory != "SHIPPINGSURCHARGE") {
                                var model = scBaseUtils.getNewModelInstance();
                                scModelUtils.setStringValueAtModelPath("ChargeCategory", scModelUtils.getStringValueFromPath("ChargeCategory", lineTaxes[i]), model);
                                scModelUtils.setStringValueAtModelPath("ChargeName", scModelUtils.getStringValueFromPath("ChargeCategory", lineTaxes[i]), model);
                                scModelUtils.setStringValueAtModelPath("TaxName", scModelUtils.getStringValueFromPath("TaxName", lineTaxes[i]), model);
                                scModelUtils.setStringValueAtModelPath("TaxPercentage", scModelUtils.getStringValueFromPath("TaxPercentage", lineTaxes[i]), model);
                                var tax = scModelUtils.getNumberValueFromPath("Tax", lineTaxes[i]);
                                if (chargeCategory == "SHIPPINGCHARGE") {
                                    scModelUtils.setStringValueAtModelPath("Tax", "0.0", model);
                                } else {
                                    var newTaxamount = (returnOrderedQty / orderedQty) * tax;
                                    scModelUtils.setStringValueAtModelPath("Tax", newTaxamount.toString(), model);
                                }
                                scBaseUtils.appendToArray(taxArray, model);
                            }
                        }
                        scModelUtils.setStringValueAtModelPath("LineTaxes.LineTax", taxArray, Model);
                    }
                    scBaseUtils.appendToArray(modelList, Model);
                }
                scModelUtils.setStringValueAtModelPath("Order.OrderLines.OrderLine", modelList, inputModel);
            }
            if (bEvent._originatingControlUId == "extn_chargePrepaidShippingLabel") {
                var checkBoxValuePrepaid = scWidgetUtils.getValue(this, "extn_chargePrepaidShippingLabel");
                scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge.ChargeCategory", "PREPAIDLABELCHARGE", inputModel);
                scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge.ChargeName", "PREPAIDLABELCHARGE", inputModel);
                var CommonCodes = scScreenUtils.getModel(this, "extn_PrepaidChargeCommonCodes");
                var CommonCodesList = scModelUtils.getStringValueFromPath("CommonCodeList.CommonCode", CommonCodes);
                var lengthNew = scBaseUtils.getAttributeCount(CommonCodesList);
                for (var counter = 0; scBaseUtils.lessThan(counter, lengthNew); counter = scBaseUtils.incrementNumber(counter, 1)) {
                    var object = scModelUtils.getModelFromList(CommonCodesList, counter);
                    var chargeAmount = scModelUtils.getStringValueFromPath("CodeValue", object);
                }
                if (checkBoxValuePrepaid == "on") {
                    scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge.ChargeAmount", chargeAmount, inputModel);
                } else {
                    scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge.ChargeAmount", "0.00", inputModel);
                }
            }
            isccsUIUtils.callApi(this, inputModel, "extn_ChangeOrder_Return", null);
        },
        extn_updateOrderTotal: function(event, bEvent, ctrl, args) {
            bEvent.stopEvent();
            var OrderObj = scScreenUtils.getModel(this, "paymentConfirmation_getCompleteOrderDetails_Output");
            var orderLineList = scModelUtils.getStringValueFromPath("Order.OrderLines.OrderLine", OrderObj);
            var documentType = scModelUtils.getStringValueFromPath("Order.DocumentType", OrderObj);
            if (documentType == "0003") {
                var inputModel = scBaseUtils.getNewModelInstance();
                var inputArray = scBaseUtils.getNewArrayInstance();
                var personInfoShipToModel = scBaseUtils.getNewModelInstance();
                var personInfoBillToModel = scBaseUtils.getNewModelInstance();
                if (!(scBaseUtils.isVoid(orderLineList))) {
                    for (var index in orderLineList) {
                        var orderLineModel = scBaseUtils.getNewModelInstance();
                        personInfoShipToModel = scModelUtils.getStringValueFromPath("DerivedFromOrderLine.PersonInfoShipTo", orderLineList[index]);
                        personInfoBillToModel = scModelUtils.getStringValueFromPath("DerivedFromOrder.PersonInfoBillTo", orderLineList[index]);
                        break;
                    }
                }
                if (!(scBaseUtils.isVoid(orderLineList))) {
                    for (var index in orderLineList) {
                        var orderLineModel = scBaseUtils.getNewModelInstance();
                        var lineCharges = scModelUtils.getStringValueFromPath("DerivedFromOrderLine.LineCharges.LineCharge", orderLineList[index]);
                        var lineTaxes = scModelUtils.getStringValueFromPath("DerivedFromOrderLine.LineTaxes.LineTax", orderLineList[index]);
                        var orderLineKey = scModelUtils.getStringValueFromPath("OrderLineKey", orderLineList[index]);
                        var returnOrderedQty = scModelUtils.getNumberValueFromPath("OrderLineTranQuantity.OrderedQty", orderLineList[index]);
                        var orderedQty = scModelUtils.getNumberValueFromPath("DerivedFromOrderLine.OrderedQty", orderLineList[index]);
                        var scacCode = scModelUtils.getStringValueFromPath("DerivedFromOrderLine.SCAC", orderLineList[index]);
                        var carrierServiceCode = scModelUtils.getStringValueFromPath("DerivedFromOrderLine.CarrierServiceCode", orderLineList[index]);
                        var taxArray = scBaseUtils.getNewArrayInstance();
                        var LineChargesModel = scBaseUtils.getNewModelInstance();
                        for (var i in lineCharges) {
                            var chargeCategory = scModelUtils.getStringValueFromPath("ChargeCategory", lineCharges[i]);
                            if (chargeCategory == "SHIPPINGCHARGE") {
                                scModelUtils.setStringValueAtModelPath("LineCharges.LineCharge.ChargeCategory", "SHIPPINGCHARGE", LineChargesModel);
                                scModelUtils.setStringValueAtModelPath("LineCharges.LineCharge.ChargePerUnit", "0.0", LineChargesModel);
                                scModelUtils.setStringValueAtModelPath("LineCharges.LineCharge.ChargeName", "SHIPPINGCHARGE", LineChargesModel);
                            }
                        }
                        for (var i in lineTaxes) {
                            var chargeCategory = scModelUtils.getStringValueFromPath("ChargeCategory", lineTaxes[i]);
                            if (chargeCategory != "SHIPPINGSURCHARGE") {
                                var model = scBaseUtils.getNewModelInstance();
                                scModelUtils.setStringValueAtModelPath("ChargeCategory", scModelUtils.getStringValueFromPath("ChargeCategory", lineTaxes[i]), model);
                                scModelUtils.setStringValueAtModelPath("ChargeName", scModelUtils.getStringValueFromPath("ChargeCategory", lineTaxes[i]), model);
                                scModelUtils.setStringValueAtModelPath("TaxName", scModelUtils.getStringValueFromPath("TaxName", lineTaxes[i]), model);
                                scModelUtils.setStringValueAtModelPath("TaxPercentage", scModelUtils.getStringValueFromPath("TaxPercentage", lineTaxes[i]), model);
                                var tax = scModelUtils.getNumberValueFromPath("Tax", lineTaxes[i]);
                                if (chargeCategory == "SHIPPINGCHARGE") {
                                    scModelUtils.setStringValueAtModelPath("Tax", "0.0", model);
                                } else {
                                    var newTaxamount = (returnOrderedQty / orderedQty) * tax;
                                    scModelUtils.setStringValueAtModelPath("Tax", newTaxamount.toString(), model);
                                }
                                scBaseUtils.appendToArray(taxArray, model);
                            }
                        }
                        scModelUtils.setStringValueAtModelPath("LineTaxes.LineTax", taxArray, orderLineModel);
                        scModelUtils.setStringValueAtModelPath("OrderLineKey", orderLineKey, orderLineModel);
                        scModelUtils.setStringValueAtModelPath("SCAC", scacCode, orderLineModel);
                        scModelUtils.setStringValueAtModelPath("CarrierServiceCode", carrierServiceCode, orderLineModel);
                        scModelUtils.setStringValueAtModelPath("Action", "Modify", orderLineModel);
                        scModelUtils.setStringValueAtModelPath("LineTaxes.LineTax", taxArray, orderLineModel);
                        scModelUtils.setStringValueAtModelPath("LineCharges", scModelUtils.getStringValueFromPath("LineCharges", LineChargesModel), orderLineModel);
                        scBaseUtils.appendToArray(inputArray, orderLineModel);
                    }
                    scModelUtils.setStringValueAtModelPath("Order.OrderLines.OrderLine", inputArray, inputModel);
                }
                scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", scModelUtils.getStringValueFromPath("Order.OrderHeaderKey", OrderObj), inputModel);
                var promoCode = null;
                var CommonCodesFR = scScreenUtils.getModel(this, "extn_FreeReturnsCommonCode");
                var CommonCodesListFR = scModelUtils.getStringValueFromPath("CommonCodeList.CommonCode", CommonCodesFR);
                var lengthNewFR = scBaseUtils.getAttributeCount(CommonCodesListFR);
                for (var counter = 0; scBaseUtils.lessThan(counter, lengthNewFR); counter = scBaseUtils.incrementNumber(counter, 1)) {
                    var objectFR = scModelUtils.getModelFromList(CommonCodesListFR, counter);
                    promoCode = scModelUtils.getStringValueFromPath("CodeValue", objectFR);
                }
                var flag;
                if (!(scBaseUtils.isVoid(orderLineList))) {
                    for (var index in orderLineList) {
                        var promotionList = scModelUtils.getStringValueFromPath("DerivedFromOrder.Promotions.Promotion", orderLineList[index]);
                        var promotionListPos = scModelUtils.getStringValueFromPath("DerivedFromOrderLine.Promotions.Promotion", orderLineList[index]);
                        if (!(scBaseUtils.isVoid(promotionList))) {
                            for (var FrIndex in promotionList) {
                                var promotionID = scModelUtils.getStringValueFromPath("PromotionId", promotionList[FrIndex]);
                                if ((scBaseUtils.equals(promotionID, promoCode))) {
                                    flag = 1;
                                    scWidgetUtils.setValue(this, "extn_chargePrepaidShippingLabel", false, null);
                                    scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge.ChargeAmount", "0.00", inputModel);
                                    break;
                                }
                            }
                        }
                        if (!(scBaseUtils.isVoid(promotionListPos))) {
                            for (var FrIndexPos in promotionListPos) {
                                var promotionID = scModelUtils.getStringValueFromPath("PromotionId", promotionListPos[FrIndexPos]);
                                if ((scBaseUtils.equals(promotionID, promoCode))) {
                                    flag = 1;
                                    scWidgetUtils.setValue(this, "extn_chargePrepaidShippingLabel", false, null);
                                    scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge.ChargeAmount", "0.00", inputModel);
                                    break;
                                }
                            }
                        }
                    }
                    //Logic for only pick return order
                    var shipCount = 0;
                    var pickCount = 0;
                    var tempOrderLines = [];
                    for (var index in orderLineList) {
                        var currentLine = null;
                        currentLine = scBaseUtils.getArrayItemByIndex(orderLineList, index);
                        var deliveryMethod = null;
                        deliveryMethod = scModelUtils.getStringValueFromPath("DerivedFromOrderLine.DeliveryMethod", currentLine);
                        var oLineKey = null;
                        oLineKey = scModelUtils.getStringValueFromPath("OrderLineKey", currentLine);
                        var orderLineMod = {
                            "OrderLineKey": oLineKey,
                            "DeliveryMethod": deliveryMethod
                        };
                        tempOrderLines.push(orderLineMod);
                        if (deliveryMethod) {
                            if (scBaseUtils.equals(deliveryMethod, "SHP")) {
                                shipCount++;
                            } else if (scBaseUtils.equals(deliveryMethod, "PICK")) {
                                pickCount++;
                            }
                        }
                    }
                    scModelUtils.setStringValueAtModelPath("Order.OrderLines.OrderLine", tempOrderLines, inputModel);
                    if (scBaseUtils.and(scBaseUtils.numberEquals(shipCount, 0), scBaseUtils.numberGreaterThan(pickCount, 0))) {
                        scWidgetUtils.setValue(this, "extn_chargePrepaidShippingLabel", false, null);
                        scWidgetUtils.disableWidget(this, "extn_chargePrepaidShippingLabel", true);
                        var OrderHeaderKey = scModelUtils.getStringValueFromPath("Order.OrderHeaderKey", OrderObj);
                        //var inputModel = scBaseUtils.getNewModelInstance();
                        scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", OrderHeaderKey, inputModel);
                        scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge.ChargeCategory", "PREPAIDLABELCHARGE", inputModel);
                        scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge.ChargeName", "PREPAIDLABELCHARGE", inputModel);
                        scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge.ChargeAmount", "0.00", inputModel);
                        // isccsUIUtils.callApi(this, inputModel, "extn_ChangeOrder_Return", null);
                    } else {
                        scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge.ChargeCategory", "PREPAIDLABELCHARGE", inputModel);
                        scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge.ChargeName", "PREPAIDLABELCHARGE", inputModel);
                        if (!(flag == 1)) {
                            var CommonCodes = scScreenUtils.getModel(this, "extn_PrepaidChargeCommonCodes");
                            var CommonCodesList = scModelUtils.getStringValueFromPath("CommonCodeList.CommonCode", CommonCodes);
                            var lengthNew = scBaseUtils.getAttributeCount(CommonCodesList);
                            for (var counter = 0; scBaseUtils.lessThan(counter, lengthNew); counter = scBaseUtils.incrementNumber(counter, 1)) {
                                var object = scModelUtils.getModelFromList(CommonCodesList, counter);
                                var chargeAmount = scModelUtils.getStringValueFromPath("CodeValue", object);
                            }
                            scModelUtils.setStringValueAtModelPath("Order.HeaderCharges.HeaderCharge.ChargeAmount", chargeAmount, inputModel);
                        }
                    }
                    scModelUtils.setStringValueAtModelPath("Order.PersonInfoShipTo", personInfoShipToModel, inputModel);
                    scModelUtils.setStringValueAtModelPath("Order.PersonInfoBillTo", personInfoBillToModel, inputModel);
                    isccsUIUtils.callApi(this, inputModel, "extn_ChangeOrder_Return", null);
                }
            }
        },
        extn_RadioBtnCheck: function(event, bEvent, ctrl, args) {
            var testVar = null;
            var OrderObj = scScreenUtils.getModel(this, "paymentConfirmation_getCompleteOrderDetails_Output");
            var orderLineList = scModelUtils.getStringValueFromPath("Order.OrderLines.OrderLine", OrderObj);
            var documentType = scModelUtils.getStringValueFromPath("Order.DocumentType", OrderObj);
            if (documentType == "0003") {
                var inputModel = scBaseUtils.getNewModelInstance();
                var inputArray = scBaseUtils.getNewArrayInstance();
                if (!(scBaseUtils.isVoid(orderLineList))) {
                    for (var index in orderLineList) {
                        var orderLineModel = scBaseUtils.getNewModelInstance();
                        var giftFlag = scModelUtils.getStringValueFromPath("DerivedFromOrderLine.GiftFlag", orderLineList[index]);
                        if (giftFlag == "Y") {
                            scWidgetUtils.setValue(this, "extn_radiobtn_refundOptions", "N");
                        }
                    }
                }
            }
        }
    });
});
