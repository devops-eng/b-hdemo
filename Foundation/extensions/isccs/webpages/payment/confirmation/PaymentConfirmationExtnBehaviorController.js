


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/payment/confirmation/PaymentConfirmationExtn","scbase/loader!sc/plat/dojo/controller/ExtnServerDataController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnPaymentConfirmationExtn
			 ,
			    _scExtnServerDataController
){

return _dojodeclare("extn.payment.confirmation.PaymentConfirmationExtnBehaviorController", 
				[_scExtnServerDataController], {

			
			 screenId : 			'extn.payment.confirmation.PaymentConfirmationExtn'

			
			
			
			
			
						,

			
			
			 mashupRefs : 	[
	 		{
		 extnType : 			'ADD'
,
		 mashupId : 			'extn_GetRewardPointsMashup'
,
		 mashupRefId : 			'extn_GetRewardPointsMashup'

	}
,
	 		{
		 extnType : 			'ADD'
,
		 mashupId : 			'extn_changeOrder'
,
		 mashupRefId : 			'extn_changeOrder'

	}
,
	 		{
		 extnType : 			'ADD'
,
		 mashupId : 			'extn_ExecutePaymentTransactions'
,
		 mashupRefId : 			'extn_ExecutePaymentTransactions'

	}
,
	 		{
		 extnType : 			'ADD'
,
		 mashupId : 			'extn_setExtnIsOrgPmtRefund'
,
		 mashupRefId : 			'extn_setExtnIsOrgPmtRefund'

	}
,
	 		{
		 extnType : 			'ADD'
,
		 mashupId : 			'extn_ChangeOrder_Return'
,
		 mashupRefId : 			'extn_ChangeOrder_Return'

	}

	]

}
);
});

