


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/payment/confirmation/PaymentConfirmationExtn","scbase/loader!sc/plat/dojo/controller/ExtnScreenController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnPaymentConfirmationExtn
			 ,
			    _scExtnScreenController
){

return _dojodeclare("extn.payment.confirmation.PaymentConfirmationExtnInitController", 
				[_scExtnScreenController], {

			
			 screenId : 			'extn.payment.confirmation.PaymentConfirmationExtn'

			
			
			
			
			
						,

			
			
			 mashupRefs : 	[
	 		{
		 sourceNamespace : 			'extn_PrepaidChargeCommonCodes'
,
		 mashupRefId : 			'extn_PrepaidChargeCommonCodesList'
,
		 sequence : 			''
,
		 mashupId : 			'extn_PrepaidChargeCommonCodesList'
,
		 callSequence : 			''
,
		 extnType : 			'ADD'
,
		 sourceBindingOptions : 			''

	}
,
	 		{
		 sourceNamespace : 			'extn_FreeReturnsCommonCode'
,
		 mashupRefId : 			'extn_FreeReturnsCommonCode'
,
		 sequence : 			''
,
		 mashupId : 			'extn_FreeReturnsCommonCode'
,
		 callSequence : 			''
,
		 extnType : 			'ADD'
,
		 sourceBindingOptions : 			''

	}

	]

}
);
});

