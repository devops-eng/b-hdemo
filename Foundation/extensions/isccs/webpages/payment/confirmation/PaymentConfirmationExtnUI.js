
scDefine(["dojo/text!./templates/PaymentConfirmationExtn.html","scbase/loader!dijit/form/Button","scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/_base/lang","scbase/loader!dojo/text","scbase/loader!idx/form/CheckBox","scbase/loader!idx/form/RadioButtonSet","scbase/loader!idx/form/TextBox","scbase/loader!idx/layout/ContentPane","scbase/loader!sc/plat","scbase/loader!sc/plat/dojo/binding/ButtonDataBinder","scbase/loader!sc/plat/dojo/binding/CheckBoxDataBinder","scbase/loader!sc/plat/dojo/binding/CurrencyDataBinder","scbase/loader!sc/plat/dojo/binding/RadioSetDataBinder","scbase/loader!sc/plat/dojo/binding/SimpleDataBinder","scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!sc/plat/dojo/widgets/DataLabel","scbase/loader!sc/plat/dojo/widgets/Link"]
 , function(			 
			    templateText
			 ,
			    _dijitButton
			 ,
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojolang
			 ,
			    _dojotext
			 ,
			    _idxCheckBox
			 ,
			    _idxRadioButtonSet
			 ,
			    _idxTextBox
			 ,
			    _idxContentPane
			 ,
			    _scplat
			 ,
			    _scButtonDataBinder
			 ,
			    _scCheckBoxDataBinder
			 ,
			    _scCurrencyDataBinder
			 ,
			    _scRadioSetDataBinder
			 ,
			    _scSimpleDataBinder
			 ,
			    _scBaseUtils
			 ,
			    _scDataLabel
			 ,
			    _scLink
){
return _dojodeclare("extn.payment.confirmation.PaymentConfirmationExtnUI",
				[], {
			templateString: templateText
	
	
	
	
	
	
					,	
	namespaces : {
		targetBindingNamespaces :
		[
			{
	  value: 'extn_getRewardPoints_ns'
						,
	  scExtensibilityArrayItemId: 'extn_TargetNamespaces_3'
						,
	  description: "extn_getRewardPoints_ns"
						
			}
			
		],
		sourceBindingNamespaces :
		[
			{
	  value: 'extn_validRewardsAccNo'
						,
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_13'
						,
	  description: "validRewardsAccNo"
						
			}
			,
			{
	  value: 'extn_PrepaidChargeCommonCodes'
						,
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_14'
						,
	  description: "extn_PrepaidChargeCommonCodes"
						
			}
			,
			{
	  value: 'extn_FreeReturnsCommonCode'
						,
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_15'
						,
	  description: "Free Returns Common Code"
						
			}
			
		]
	}

	
	,
	hotKeys: [ 
	]

,events : [
	]

,subscribers : {

local : [

{
	  eventId: 'afterScreenInit'

,	  sequence: '51'

,	  description: 'extn_initializerewards'



,handler : {
methodName : "extn_initializerewards"

 
}
}
,
{
	  eventId: 'afterScreenInit'

,	  sequence: '52'

,	  description: 'RadioBtnCheck'



,handler : {
methodName : "extn_RadioBtnCheck"

 
}
}
,
{
	  eventId: 'afterScreenLoad'

,	  sequence: '19'

,	  description: 'extn_updateOrderTotal'



,handler : {
methodName : "extn_updateOrderTotal"

 
}
}
,
{
	  eventId: 'saveCurrentPage'

,	  sequence: '19'




,handler : {
methodName : "saveExtn"

 
}
}
,
{
	  eventId: 'onExtnMashupCompletion'

,	  sequence: '51'

,	  description: 'extn_getLoyaltyNumber'



,handler : {
methodName : "extn_getLoyaltyNumber"

 
}
}
,
{
	  eventId: 'extn_getRewardsButton_onClick'

,	  sequence: '51'

,	  description: 'extn_getRewardPoints'



,handler : {
methodName : "extn_getRewardPoints"

 
}
}
,
{
	  eventId: 'extn_RefundShippingCharges_onChange'

,	  sequence: '51'

,	  description: 'RefundShippingCharge'



,handler : {
methodName : "extn_RefundShippingCharge"

 
}
}
,
{
	  eventId: 'extn_chargePrepaidShippingLabel_onChange'

,	  sequence: '51'

,	  description: 'RefundShippingCharge'



,handler : {
methodName : "extn_RefundShippingCharge"

 
}
}

]
}

});
});


