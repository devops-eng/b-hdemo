scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/payment/confirmation/PaymentMethodExtnUI","scbase/loader!sc/plat/dojo/utils/WidgetUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnPaymentMethodExtnUI
			 ,
				_scWidgetUtils
){ 
	return _dojodeclare("extn.payment.confirmation.PaymentMethodExtn", [_extnPaymentMethodExtnUI],{
	// custom code here
	extn_CVVOptional: function(event, bEvent, ctrl, args){
	_scWidgetUtils.setWidgetNonMandatory(this,"txtCvv");
	}
});
});