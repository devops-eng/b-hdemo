


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/payment/confirmation/PromotionExtn","scbase/loader!sc/plat/dojo/controller/ExtnScreenController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnPromotionExtn
			 ,
			    _scExtnScreenController
){

return _dojodeclare("extn.payment.confirmation.PromotionExtnInitController", 
				[_scExtnScreenController], {

			
			 screenId : 			'extn.payment.confirmation.PromotionExtn'

			
			
			
}
);
});

