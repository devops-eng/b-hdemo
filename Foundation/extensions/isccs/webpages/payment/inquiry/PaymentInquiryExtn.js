scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/payment/inquiry/PaymentInquiryExtnUI","scbase/loader!sc/plat/dojo/utils/ModelUtils","scbase/loader!sc/plat/dojo/utils/ScreenUtils","scbase/loader!sc/plat/dojo/utils/BaseUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnPaymentInquiryExtnUI
			 ,
			     scModelUtils
			 ,
			     scScreenUtils
			 ,
			     scBaseUtils
){ 
	return _dojodeclare("extn.payment.inquiry.PaymentInquiryExtn", [_extnPaymentInquiryExtnUI],{
	// custom code here

	extn_getResponseMsg : function(gridReference,rowIndex,columnIndex,gridRowJSON,sReturnMessage)
	{
		var sReturnMessageTemp = null;
	                sReturnMessageTemp = scModelUtils.getStringValueFromPath("CreditCardTransactions",gridRowJSON);
	                if(!scBaseUtils.isVoid(sReturnMessageTemp))
	                {
	                    
	                    sReturnMessage=gridRowJSON.CreditCardTransactions.CreditCardTransaction[0].AuthReturnMessage;
	                    return sReturnMessage;
	                    
	                }
	                
				//
            
        
		
	},

	extn_getProcessedDate : function(gridReference,rowIndex,columnIndex,gridRowJSON,sProcessedDate)
	{
		var gwpItemList = scScreenUtils.getModel(this, "paymentInquiry-getPaymentInquiryDetails_Output");
		var sOrganizationCode=null;
		sOrganizationCode = scModelUtils.getStringValueFromPath("Order.EnterpriseCode",gwpItemList);
		if((scBaseUtils.equals(sOrganizationCode, "OFF5")))
		{
		var sProcessedDateTemp = null;
		sProcessedDateTemp = scModelUtils.getStringValueFromPath("InvoiceCollectionDetails",gridRowJSON);
		if(!scBaseUtils.isVoid(sProcessedDateTemp))
		{
		 var sServerDate =  gridRowJSON.InvoiceCollectionDetails.InvoiceCollectionDetail[0].DateInvoiced;
		   sProcessedDate= scBaseUtils.formatDateToUserFormat(sServerDate);
		   return sProcessedDate;
		}
		}
		else{
		    var sExecutionDateTemp = null;
		sExecutionDateTemp = scModelUtils.getStringValueFromPath("ExecutionDate",gridRowJSON);
		return sExecutionDateTemp;

		/*
		 This conversion will be used if the Execution date is in Server format for LT/BAY
		if(!scBaseUtils.isVoid(sExecutionDateTemp ))
		{
		 var sServerDate =  sExecutionDateTemp;
		   
		   sExecutionDate= scBaseUtils.formatDateToUserFormat(sServerDate); 
		   return sServerDate;
		}*/
		}

	}

});
});
