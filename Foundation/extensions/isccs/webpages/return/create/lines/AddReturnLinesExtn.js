scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/return/create/lines/AddReturnLinesExtnUI",
"scbase/loader!sc/plat/dojo/utils/BaseUtils",
"scbase/loader!isccs/utils/UIUtils",
"scbase/loader!sc/plat/dojo/utils/ModelUtils",
"scbase/loader!sc/plat/dojo/utils/ScreenUtils",
"scbase/loader!isccs/utils/BaseTemplateUtils"
]
,
function(			 
			    _dojodeclare
			 ,
			    _extnAddReturnLinesExtnUI
			 ,
			 	_scBaseUtils
			 ,	
			 	_isccsUIUtils
			 ,	
			 	_scModelUtils
             ,
             	_scScreenUtils
             ,	
             	_isccsBaseTemplateUtils
                       

){ 
	return _dojodeclare("extn.return.create.lines.AddReturnLinesExtn", [_extnAddReturnLinesExtnUI],{
	// custom code here

	extn_OrderNoCheckHeader : function(event, bEvent, ctrl, args){
		var OrderObj=args;
		var orderLineList = _scModelUtils.getModelListFromPath("orderLines.OrderLineList.OrderLine",OrderObj);
		var existingOrder = _scScreenUtils.getModel(this, "createdOrder");
		var NoOfRecordsModel = _scScreenUtils.getModel(this, "numberOfLines");
		var NoOfRecord = _scModelUtils.getNumberValueFromPath("OrderLineList.TotalNumberOfRecords",NoOfRecordsModel);
		if(!_scBaseUtils.isVoid(NoOfRecord)){		
			if(_scBaseUtils.greaterThan(NoOfRecord,0)){
				var orderLinesInExistingOrder = _scModelUtils.getModelListFromPath("OrderLineList.OrderLine",NoOfRecordsModel); 
				var existingOrderNo= "";
				var newOrderNo="";
				for(var orderLine in orderLinesInExistingOrder){
					existingOrderNo = _scModelUtils.getStringValueFromPath("DerivedFromOrder.OrderNo",orderLinesInExistingOrder[orderLine]);
					break;
				}
				for (var orderLine in orderLineList){
					newOrderNo = _scModelUtils.getStringValueFromPath("Order.OrderNo", orderLineList[orderLine]);
					break;
				}
				if(!_scBaseUtils.equals(existingOrderNo,newOrderNo)){
					_isccsBaseTemplateUtils.showMessage(this,"Please Select Order Lines Belonging to same Order Number","error");
							sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
				}
			}
		}
	},
	 updateReturn: function(
                event, bEvent, ctrl, args) {

                var mashupRefs = _scBaseUtils.getValueFromPath("mashupRefs", args);
                var length = _scBaseUtils.getAttributeCount(mashupRefs);
                
                for (
                    var index = 0; _scBaseUtils.lessThan(
                    index, length); index = _scBaseUtils.incrementNumber(
                    index, 1)) {
                    var currLine = _scBaseUtils.getArrayBeanItemByIndex(
                        mashupRefs, index);
                    var mashupRefId = _scBaseUtils.getValueFromPath("mashupRefId", currLine);
                    if (_scBaseUtils.equals(mashupRefId, "updateReturnOrder")) {

                        var mashupInput = _scBaseUtils.getValueFromPath("mashupInputObject", currLine);
                    _scModelUtils.setStringValueAtModelPath(
                                "Order.IsNoShowValidationReq", "Y", mashupInput);
        }
        }
		}
});
});
