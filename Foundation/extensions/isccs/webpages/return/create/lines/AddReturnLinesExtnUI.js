
scDefine(["dojo/text!./templates/AddReturnLinesExtn.html","scbase/loader!dijit/form/Button","scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/_base/lang","scbase/loader!dojo/text","scbase/loader!sc/plat","scbase/loader!sc/plat/dojo/binding/ButtonDataBinder","scbase/loader!sc/plat/dojo/utils/BaseUtils"]
 , function(			 
			    templateText
			 ,
			    _dijitButton
			 ,
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojolang
			 ,
			    _dojotext
			 ,
			    _scplat
			 ,
			    _scButtonDataBinder
			 ,
			    _scBaseUtils
){
return _dojodeclare("extn.return.create.lines.AddReturnLinesExtnUI",
				[], {
			templateString: templateText
	
	
	
	
	
	
					,	
	namespaces : {
		targetBindingNamespaces :
		[
		],
		sourceBindingNamespaces :
		[
			{
	  value: 'extn_PersonInfoDetails_ns'
						,
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_6'
						,
	  description: "extn_PersonInfoDetails_ns"
						
			}
			
		]
	}

	
	,
	hotKeys: [ 
	]

,events : [
	]

,subscribers : {

local : [

{
	  eventId: 'onAddReturnLine'

,	  sequence: '19'

,	  description: 'OrderNoCheckHeader'



,handler : {
methodName : "extn_OrderNoCheckHeader"

 
}
},
{
   eventId: 'beforeBehaviorMashupCall'

,   sequence: '51'




,handler : {
methodName : "updateReturn"

 
 
}
}

]
}

});
});


