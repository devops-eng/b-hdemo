scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/return/create/lines/ExistingLineSearchExtnUI",
"scbase/loader!sc/plat/dojo/utils/ScreenUtils","scbase/loader!isccs/utils/BaseTemplateUtils", "scbase/loader!isccs/utils/ReturnUtils",  
 "scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!sc/plat/dojo/utils/EventUtils","scbase/loader!sc/plat/dojo/utils/ModelUtils",
"scbase/loader!isccs/utils/UIUtils","scbase/loader!isccs/utils/ModelUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnExistingLineSearchExtnUI
,
_scScreenUtils,
_isccsBaseTemplateUtils,
_isccsReturnUtils,
_scBaseUtils,
_scEventUtils,
_scModelUtils,
_isccsUIUtils,
_isccsModelUtils

){ 
	return _dojodeclare("extn.return.create.lines.ExistingLineSearchExtn", [_extnExistingLineSearchExtnUI],{
	// custom code here

	extn_handleExtnMashupCompletion: function(
        event, bEvent, ctrl, args){
		var output = null;
		ouput = _scModelUtils.getModelListFromPath("mashupArray",args);
		for(var extnMashupCount in ouput ){
    		var extnMashupRef = ouput[extnMashupCount];
    		if(extnMashupRef.mashupRefId == "extn_getItemListWithUPCCode" ){
    		    extnMashupOutput = extnMashupRef.mashupRefOutput;
    		    var extnUPCItems = null;
    		    extnUPCItems = _scModelUtils.getModelListFromPath("ItemList.Item",extnMashupOutput);
    		
        		if(!_scBaseUtils.isVoid(extnUPCItems)){
            		var targetModel = null;
            		var extnUPCItemID = extnUPCItems[0].ItemID;
            		targetModel = _scBaseUtils.getTargetModel(
                            this, this.SST_getSearchNamespace(), null);
                     _isccsModelUtils.removeAttributeFromModel("OrderLine.UPCCode",targetModel);
                     
                     var eventDefn = null;
                     var args = null;
                     eventDefn = {};
                     args = {};
                     _scBaseUtils.setAttributeValue("OrderLine.Item.ItemID",extnUPCItemID, targetModel);
                     _scBaseUtils.setAttributeValue("inputData", targetModel, args);
                     _scBaseUtils.setAttributeValue("argumentList", args, eventDefn);
                     _scBaseUtils.setAttributeValue("argumentList.Flow", "SST_SearchButton", eventDefn);
                     _scEventUtils.fireEventToChild(
                            this, "previewPanel", "callListApi", eventDefn);
                  }
              }
        }
	},

	 SST_search: function(
        event, bEvent, ctrl, args) {
            var targetModel = null;
            if (!(
            _isccsReturnUtils.validateSearchInput(
            this, this.SST_getSearchNamespace()))) {
                _isccsBaseTemplateUtils.showMessage(
                this, "You_Must_Refine_Your_Search_Criteria", "error", null);
                return;
            }
            if (!(
            _scScreenUtils.isValid(
            this, this.SST_getSearchNamespace()))) {
                _isccsBaseTemplateUtils.showMessage(
                this, "InvalidSearchCriteria", "error", null);
                return;
            } else {
                _isccsBaseTemplateUtils.hideMessage(
                this);
                targetModel = _scBaseUtils.getTargetModel(
                this, this.SST_getSearchNamespace(), null);
            }
            _scBaseUtils.removeBlankAttributes(
            targetModel);
            
            //Custom code for UPC Code
            var extnUPCCode = null;
            extnUPCCode = _scModelUtils.getStringValueFromPath("OrderLine.UPCCode",targetModel);
            
            if(_scBaseUtils.isVoid(extnUPCCode)){
             
                var eventDefn = null;
                var args = null;
                eventDefn = {};
                args = {};
                _scBaseUtils.setAttributeValue("inputData", targetModel, args);
                _scBaseUtils.setAttributeValue("argumentList", args, eventDefn);
                _scBaseUtils.setAttributeValue("argumentList.Flow", ctrl, eventDefn);
                _scEventUtils.fireEventToChild(
                this, "previewPanel", "callListApi", eventDefn);
            }else{
                var inputData = null;
                inputData = {};
                _scBaseUtils.setAttributeValue("Item.ItemAliasList.ItemAlias.AliasValue", extnUPCCode, inputData);
                _isccsUIUtils.callApi(
                this, inputData, "extn_getItemListWithUPCCode", null);
            }
        }
});
});

