


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/return/create/lines/ExistingLineSearchExtn","scbase/loader!sc/plat/dojo/controller/ExtnServerDataController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnExistingLineSearchExtn
			 ,
			    _scExtnServerDataController
){

return _dojodeclare("extn.return.create.lines.ExistingLineSearchExtnBehaviorController", 
				[_scExtnServerDataController], {

			
			 screenId : 			'extn.return.create.lines.ExistingLineSearchExtn'

			
			
			
			
			
						,

			
			
			 mashupRefs : 	[
	 		{
		 extnType : 			'ADD'
,
		 mashupId : 			'extn_getItemList'
,
		 mashupRefId : 			'extn_getItemListWithUPCCode'

	}

	]

}
);
});

