


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/return/create/lines/ExistingLineSearchExtn","scbase/loader!sc/plat/dojo/controller/ExtnScreenController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnExistingLineSearchExtn
			 ,
			    _scExtnScreenController
){

return _dojodeclare("extn.return.create.lines.ExistingLineSearchExtnInitController", 
				[_scExtnScreenController], {

			
			 screenId : 			'extn.return.create.lines.ExistingLineSearchExtn'

			
			
			
}
);
});

