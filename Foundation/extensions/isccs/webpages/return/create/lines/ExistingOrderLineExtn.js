scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/return/create/lines/ExistingOrderLineExtnUI","scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!sc/plat/dojo/utils/ScreenUtils","scbase/loader!sc/plat/dojo/utils/ModelUtils","scbase/loader!isccs/utils/BaseTemplateUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnExistingOrderLineExtnUI
			 ,
			 	_scBaseUtils
			 ,
			 	_scScreenUtils
			 ,
			 	_scModelUtils
			 ,
			 	_isccsBaseTemplateUtils
){ 
	return _dojodeclare("extn.return.create.lines.ExistingOrderLineExtn", [_extnExistingOrderLineExtnUI],{
	extn_OrderNoCheck:function(event,bEvent,ctrl,args)
				{
					
					if(!(this.isSingleLineSelected())){
						var test = null;
						var orderLineObj = _scScreenUtils.getModel(
                        this, "selectedOrderLineList");
                        var orderLineList = _scModelUtils.getModelListFromPath("OrderLineList.OrderLine",orderLineObj);
                        var listLength = orderLineList.length;
                        if(orderLineList.length>1){
							var orderNo = null;
                        	for(var i in orderLineList ){
								if(_scBaseUtils.isVoid(orderNo)){
									orderNo = _scModelUtils.getStringValueFromPath("Order.OrderNo", orderLineList[i]);
								}
                        		else{
                        			var orderNo1 = _scModelUtils.getStringValueFromPath("Order.OrderNo", orderLineList[i]);
                        			if(!(orderNo==orderNo1)){
                        				_isccsBaseTemplateUtils.showMessage(this,"Please Select Order Lines Belonging to same Order Number","error");
                        			sc.plat.dojo.utils.EventUtils.stopEvent(bEvent);
                                 	break;
                        			}

                      		}
                        		
                        	}
                        }
					}
						
				}
});
});
