scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/return/create/lines/ExistingOrderLinesExtnUI",
"scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnExistingOrderLinesExtnUI
                      ,
                          _scBaseUtils
                      ,
                          _scModelUtils
                        
                      
){ 
	return _dojodeclare("extn.return.create.lines.ExistingOrderLinesExtn", [_extnExistingOrderLinesExtnUI],{
	// custom code here

extn_GetColor:function(gridReference,rowIndex,columnIndex,gridRowJSON,unformattedValue)

       {
        var color = "";

       var attributeList = _scModelUtils.getModelListFromPath("ItemDetails.AdditionalAttributeList.AdditionalAttribute",gridRowJSON);

       for(var index in attributeList)

       {
            var name =attributeList[index].Name;
           if(_scBaseUtils.contains(name, "Color")) 

           {

               color =attributeList[index].Value;

           }

       }

       return color;

       },
	extn_GetSize:function(gridReference,rowIndex,columnIndex,gridRowJSON,unformattedValue)

       {
        var size = "";
       var attributeList = _scModelUtils.getModelListFromPath("ItemDetails.AdditionalAttributeList.AdditionalAttribute",gridRowJSON);
       for(var index in attributeList)

       {
            var name =attributeList[index].Name;
           if(_scBaseUtils.contains(name, "Size")) 

           {

               size =attributeList[index].Value;

           }

       }

       return size;

       },
      extn_UPCCode:function(gridReference,rowIndex,columnIndex,gridRowJSON,unformattedValue)

       {
        var upcCode= "";
       var aliasList = _scModelUtils.getModelListFromPath("ItemDetails.ItemAliasList.ItemAlias",gridRowJSON);
       for(var index in aliasList)

       {
            var name =aliasList[index].AliasName;
           if(name=="ACTIVE_UPC") 

           {

               upcCode=aliasList[index].AliasValue;

           }

       }

       return upcCode;

       },


  
 extn_finalScaleIndicator:function(gridReference,rowIndex,columnIndex,gridRowJSON,unformattedValue)

       {
           var finalScaleIndicator = "";
          var value = _scModelUtils.getModelListFromPath("ItemDetails.PrimaryInformation.IsReturnable",gridRowJSON);
          if(value =="Y")
          {
            finalScaleIndicator = "N";
          } 	
          else
          {
            finalScaleIndicator = "Y";
          }
           
           return finalScaleIndicator;

       }



});
});

