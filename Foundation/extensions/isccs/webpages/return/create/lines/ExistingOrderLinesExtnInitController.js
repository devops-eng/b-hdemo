


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/return/create/lines/ExistingOrderLinesExtn","scbase/loader!sc/plat/dojo/controller/ExtnScreenController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnExistingOrderLinesExtn
			 ,
			    _scExtnScreenController
){

return _dojodeclare("extn.return.create.lines.ExistingOrderLinesExtnInitController", 
				[_scExtnScreenController], {

			
			 screenId : 			'extn.return.create.lines.ExistingOrderLinesExtn'

			
			
			
}
);
});

