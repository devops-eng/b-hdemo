scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/return/create/lines/ReturnOrderLinesExtnUI","scbase/loader!sc/plat/dojo/utils/BaseUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnReturnOrderLinesExtnUI
	               ,
                         _scBaseUtils
){ 
	return _dojodeclare("extn.return.create.lines.ReturnOrderLinesExtn", [_extnReturnOrderLinesExtnUI],{
	
	extn_GetColor:function(gridReference,rowIndex,columnIndex,gridRowJSON,unformattedValue)

       {

       var attributeList = gridRowJSON.ItemDetails.AdditionalAttributeList.AdditionalAttribute;

       for(var index in attributeList)

       {
            var name =attributeList[index].Name;
           if(_scBaseUtils.contains(name, "Color")) 

           {

               unformattedValue=attributeList[index].Value;

           }

       }

       return unformattedValue;

       },
	extn_GetSize:function(gridReference,rowIndex,columnIndex,gridRowJSON,unformattedValue)

       {

       var attributeList = gridRowJSON.ItemDetails.AdditionalAttributeList.AdditionalAttribute;

       for(var index in attributeList)

       {
            var name =attributeList[index].Name;
           if(_scBaseUtils.contains(name, "Size")) 

           {

               unformattedValue=attributeList[index].Value;

           }

       }

       return unformattedValue;

       },
      extn_UPCCode:function(gridReference,rowIndex,columnIndex,gridRowJSON,unformattedValue)

       {

       var aliasList = gridRowJSON.ItemDetails.ItemAliasList.ItemAlias;
       for(var index in aliasList)

       {
            var name =aliasList[index].AliasName;
           if(name=="ACTIVE_UPC") 

           {

               unformattedValue=aliasList[index].AliasValue;

           }

       }

       return unformattedValue;

       },

 extn_finalScaleIndicator:function(gridReference,rowIndex,columnIndex,gridRowJSON,unformattedValue)

      {
        //  var value = gridRowJSON.ItemDetails.PrimaryInformation.IsReturnable;
         var value = gridRowJSON.Extn.ExtnIsReturnable;
         if(!(_scBaseUtils.isVoid(value)))
         {
          if(value =="Y")
          {
            unformattedValue = "N";
          } 	
          else
          {
            unformattedValue = "Y";
          }
         }
           return unformattedValue;

       }


});
});

