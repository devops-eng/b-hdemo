


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/return/create/preview/ReturnLinesPreviewExtn","scbase/loader!sc/plat/dojo/controller/ExtnServerDataController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnReturnLinesPreviewExtn
			 ,
			    _scExtnServerDataController
){

return _dojodeclare("extn.return.create.preview.ReturnLinesPreviewExtnBehaviorController", 
				[_scExtnServerDataController], {

			
			 screenId : 			'extn.return.create.preview.ReturnLinesPreviewExtn'

			
			
			
}
);
});

