


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/return/create/preview/ReturnLinesPreviewExtn","scbase/loader!sc/plat/dojo/controller/ExtnScreenController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnReturnLinesPreviewExtn
			 ,
			    _scExtnScreenController
){

return _dojodeclare("extn.return.create.preview.ReturnLinesPreviewExtnInitController", 
				[_scExtnScreenController], {

			
			 screenId : 			'extn.return.create.preview.ReturnLinesPreviewExtn'

			
			
			
}
);
});

