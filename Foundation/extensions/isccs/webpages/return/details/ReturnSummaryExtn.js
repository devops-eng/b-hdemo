
scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/return/details/ReturnSummaryExtnUI", "scbase/loader!sc/plat/dojo/utils/ScreenUtils",]
,
function(			 
			    _dojodeclare
			 ,
			    _extnReturnSummaryExtnUI
			 ,
			    _scScreenUtils
){ 
	return _dojodeclare("extn.return.details.ReturnSummaryExtn", [_extnReturnSummaryExtnUI],{
	// custom code here
extn_getRefundTender: function(dataValue,screen, uIdOfWidget, namespace, modelObject, options){
	var refundTender = "";
	if(dataValue == "N"){
		refundTender  = _scScreenUtils.getString(this,"extn_Gift_Card");
	}else{
		refundTender  = _scScreenUtils.getString(this,"extn_Original_Payment_Tender");
	}
	return refundTender;
}
});
});

