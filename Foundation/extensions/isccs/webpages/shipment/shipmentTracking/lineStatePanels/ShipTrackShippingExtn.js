scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/shipment/shipmentTracking/lineStatePanels/ShipTrackShippingExtnUI","scbase/loader!sc/plat/dojo/utils/ScreenUtils","scbase/loader!isccs/utils/UIUtils","scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!sc/plat/dojo/utils/ModelUtils","scbase/loader!sc/plat/dojo/utils/WidgetUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnShipTrackShippingExtnUI
             ,
                _scScreenUtils
             ,
                _isccsUIUtils
             ,
                _scBaseUtils
             ,
                _scModelUtils
             ,
                _scWidgetUtils
){ 	return _dojodeclare("extn.shipment.shipmentTracking.lineStatePanels.ShipTrackShippingExtn", [_extnShipTrackShippingExtnUI],{
	// custom code here

extn_init:function(event, bEvent, ctrl, args)
	{
	
            var trackingDataModel = _scScreenUtils.getModel(this, "TrackingData");
            var status = _scModelUtils.getStringValueFromPath("OrderStatus.DisplayStatus", trackingDataModel);
              // if(status == "Shipped")
             //  {
            var parent = _isccsUIUtils.getParentScreen(this,true);
            var parent1 =  _isccsUIUtils.getParentScreen(parent,true);
            var sourceData=_scScreenUtils.getModel(parent1, "LST_listAPIInput");
            var orderLineDetails = _scScreenUtils.getModel(this, "getCompleteOrderLineDetails_Output");
            var orderLineKey =_scModelUtils.getStringValueFromPath("OrderLine.OrderLineKey", orderLineDetails); 
            var inputModel = _scBaseUtils.getNewModelInstance();
            var orderHeaderKey=_scModelUtils.getStringValueFromPath("OrderLine.OrderHeaderKey", sourceData);
            _scModelUtils.setStringValueAtModelPath("OrderLine.OrderHeaderKey",orderHeaderKey,inputModel);
            _scModelUtils.setStringValueAtModelPath("OrderLine.OrderLineKey",orderLineKey,inputModel);
            if((!(_scBaseUtils.isVoid(orderLineKey))) && (!(_scBaseUtils.isVoid(orderHeaderKey))))
              {
               _isccsUIUtils.callApi(this,inputModel,"extn_TrackingMashupBeh",null);
              }
          // }         
	},

extn_getTrackingNumber:function(event, bEvent, ctrl, args)
{
	  var output = args.mashupArray;
	  var trackingNo = null;
         for(var index in output)
         {
            var mashupId =output[index].mashupRefId;
            if(mashupId=="extn_TrackingMashupBeh")
           {
              var outputModel = output[index].mashupRefOutput;
              var orderLines = outputModel.OrderLineList.OrderLine[0];
              var trackingInfo = _scModelUtils.getStringValueFromPath("TrackingInfoList.TrackingInfo", orderLines);
              if(!(_scBaseUtils.isVoid(trackingInfo)))
               {
                 for(var i in trackingInfo)
                 {
                   trackingNo =  _scModelUtils.getStringValueFromPath("TrackingNo", trackingInfo[i]); 
                 }
               }
           var trackingDataModel = _scScreenUtils.getModel(this, "TrackingData");
           var containerElem = _scBaseUtils.getValueFromPath("ContainerDetail", trackingDataModel);
           var status = _scModelUtils.getStringValueFromPath("OrderStatus.DisplayStatus", trackingDataModel);
            // if(_scBaseUtils.isVoid(containerElem) && status=="Shipped")
             if(_scBaseUtils.isVoid(containerElem))
              {
                if(!(_scBaseUtils.isVoid(trackingNo)))
                {
                  sc.plat.dojo.utils.WidgetUtils.setValue(this,"lblTrackingNo",trackingNo,false);
                }
              }
          }
      }
 }
});
});

