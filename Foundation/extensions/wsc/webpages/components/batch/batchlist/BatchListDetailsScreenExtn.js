scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/components/batch/batchlist/BatchListDetailsScreenExtnUI", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/widgets/Label", "scbase/loader!sc/plat/dojo/widgets/Link", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/GridxUtils", "scbase/loader!gridx/Grid", "scbase/loader!sc/plat/dojo/binding/GridxDataBinder", "scbase/loader!ias/utils/RepeatingScreenUtils", "scbase/loader!sc/plat/dojo/utils/RepeatingPanelUtils", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!sc/plat/dojo/utils/RequestUtils", "scbase/loader!sc/plat/dojo/utils/PlatformUIFmkImplUtils"], function(_dojodeclare, _extnBatchListDetailsScreenExtnUI, _scWidgetUtils, _scBaseUtils, _scEventUtils, _scLabel, _scLink, _scScreen, _scModelUtils, _iasUIUtils, _scScreenUtils, _iasBaseTemplateUtils, _scWidgetUtils, _scEventUtils, _scGridxUtils, _gridxGrid, _scGridxDataBinder, _iasRepeatingScreenUtils, _scRepeatingPanelUtils, _scUserprefs, _iasContextUtils, _extnExpicientUtils, _scRequestUtils, _scPlatformUIFmkImplUtils) {
    return _dojodeclare("extn.components.batch.batchlist.BatchListDetailsScreenExtn", [_extnBatchListDetailsScreenExtnUI], {
        // custom code here
        extn_afterScreenInit: function(event, bEvent, ctrl, args) {
            var batchListModel = _scScreenUtils.getModel(this, "getBatchList_output");
            var batchArr = _scModelUtils.getStringValueFromPath("ShipmentLines.ShipmentLine", batchListModel);
            var orderDate = batchArr[0].Shipment[0].Createts;
            var oldestDate = orderDate;
            for (var i = 0; i < batchArr.length; i++) {
                orderDate = batchArr[i].Shipment[0].Createts;
                var TorF = _scBaseUtils.dateGreaterThan(oldestDate, orderDate);
                if (TorF) {
                    oldestDate = orderDate;
                }
            }
            var dateVal = new Date(oldestDate);
            var finalDate = dateVal.toLocaleDateString();
            _scWidgetUtils.setValue(this, "extn_earliestDate", finalDate);
            var shipmentLines = _scModelUtils.getStringValueFromPath("ShipmentLines.ShipmentLine", batchListModel);
            var storeBatchConfigList = _scModelUtils.getStringValueFromPath("StoreBatchConfigList.StoreBatchConfig", batchListModel);
            len = storeBatchConfigList.length;
            var deplist = _scUserprefs.getProperty("DEP_GROUP");
            if (!_scBaseUtils.isVoid(deplist)) {
                var keys = Object.keys(deplist);
                var DepartmentDisplayDesc = _scModelUtils.getStringValueFromPath("DepartmentDisplayDesc", batchListModel);
                for (var index = 0; _scBaseUtils.lessThan(index, storeBatchConfigList.length); index = _scBaseUtils.incrementNumber(index, 1)) {
                    currConfigLine = storeBatchConfigList[index];
                    var name = _scModelUtils.getStringValueFromPath("Name", currConfigLine);
                    if (_scBaseUtils.equals("CarrierServiceCode", name)) {
                        var value = _scModelUtils.getStringValueFromPath("Value", currConfigLine);
                        _scWidgetUtils.showWidget(this, "departmentPanel", true, null);
                        _scWidgetUtils.setValue(this, "lbl_Department", value);
                    } else if (_scBaseUtils.equals("Extn_ExtnPriceAttribute", name)) {
                        var value = _scModelUtils.getStringValueFromPath("Value", currConfigLine);
                        _scWidgetUtils.showWidget(this, "departmentPanel", true, null);
                        _scWidgetUtils.setValue(this, "lbl_Department", value);
                    } else if (_scBaseUtils.equals("DepartmentGroup", name)) {
                        var value = _scModelUtils.getStringValueFromPath("Value", currConfigLine);
                        if (_scBaseUtils.equals(value, "NO_DEPT_GRP")) {
                            for (j = 0; j < keys.length; j++) {
                                var key = keys[j];
                                if (DepartmentDisplayDesc.indexOf(key) >= 0) {
                                    var name = deplist[key];
                                    var DepartmentDisplayDesc = DepartmentDisplayDesc.replace(key, name);
                                    _scWidgetUtils.showWidget(this, "departmentPanel", true, null);
                                    _scWidgetUtils.setValue(this, "lbl_Department", DepartmentDisplayDesc);
                                }
                            }
                        } else {
                            var apiInput = _scModelUtils.createNewModelObjectWithRootKey("CommonCode");
                            _scModelUtils.setStringValueAtModelPath("CommonCode.CodeType", "WSC_DEPART_GROUP", apiInput);
                            _scModelUtils.setStringValueAtModelPath("CommonCode.CodeValue", value, apiInput);
                            _scModelUtils.setStringValueAtModelPath("CommonCode.OrganizationCode", _iasContextUtils.getFromContext("CurrentStore"), apiInput);
                            _iasUIUtils.callApi(this, apiInput, "extn_getDepartmentNames_ref", null);
                        }
                    }
                }
            }
        },
        extn_beforeContinueBatchPick: function(event, bEvent, ctrl, args) {
            var currentUserId = _scPlatformUIFmkImplUtils.getUserId();
            var currScreen = _scModelUtils.getModelObjectFromPath("screen", args);
            var assignedUserId = _scModelUtils.getStringValueFromPath("AssignedToUserId", _scScreenUtils.getModel(currScreen, "getBatchList_output"));
            if (!_scBaseUtils.isVoid(assignedUserId) && assignedUserId != currentUserId) {
                _scEventUtils.stopEvent(bEvent);
                this.openBatchPickWizard(true);
            }
        },
        extn_afterBehaviorMashupCall: function(event, bEvent, ctrl, args) {
            var isMashupAvailable1 = _extnExpicientUtils.isMashupAvailable("extn_getDepartmentNames_ref", args);
            if (isMashupAvailable1) {
                var batchListModel = _scScreenUtils.getModel(this, "getBatchList_output");
                var deplist = _scUserprefs.getProperty("DEP_GROUP");
                var keys = Object.keys(deplist);
                var DepartmentDisplayDesc = _scModelUtils.getStringValueFromPath("DepartmentDisplayDesc", batchListModel);
                var outputData = null;
                var map1 = {}
                outputData = _extnExpicientUtils.getMashupOutputForRefID("extn_getDepartmentNames_ref", args);
                if (outputData.CommonCodeList && outputData.CommonCodeList.CommonCode && outputData.CommonCodeList.CommonCode.length == 1) {
                    var commonModel = null;
                    commonModel = outputData.CommonCodeList.CommonCode[0];
                    var shortDescription = null;
                    shortDescription = commonModel.CodeShortDescription;
                    if (!_scBaseUtils.isVoid(shortDescription)) {
                        _scWidgetUtils.showWidget(this, "departmentPanel", true, null);
                        _scWidgetUtils.setValue(this, "lbl_Department", shortDescription);
                    } else {
                        for (j = 0; j < keys.length; j++) {
                            var key = keys[j];
                            if (DepartmentDisplayDesc.indexOf(key) >= 0) {
                                var name = deplist[key];
                                var DepartmentDisplayDesc = DepartmentDisplayDesc.replace(key, name);
                                _scWidgetUtils.showWidget(this, "departmentPanel", true, null);
                                _scWidgetUtils.setValue(this, "lbl_Department", DepartmentDisplayDesc);
                            }
                        }
                    }
                } else {
                    for (j = 0; j < keys.length; j++) {
                        var key = keys[j];
                        if (DepartmentDisplayDesc.indexOf(key) >= 0) {
                            var name = deplist[key];
                            var DepartmentDisplayDesc = DepartmentDisplayDesc.replace(key, name);
                            _scWidgetUtils.showWidget(this, "departmentPanel", true, null);
                            _scWidgetUtils.setValue(this, "lbl_Department", DepartmentDisplayDesc);
                        }
                    }
                }
            }
        }
    });
});
