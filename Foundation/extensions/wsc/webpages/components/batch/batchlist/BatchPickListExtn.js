    scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/components/batch/batchlist/BatchPickListExtnUI", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/widgets/Label", "scbase/loader!sc/plat/dojo/widgets/Link", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/GridxUtils", "scbase/loader!gridx/Grid", "scbase/loader!sc/plat/dojo/binding/GridxDataBinder", "scbase/loader!ias/utils/RepeatingScreenUtils", "scbase/loader!sc/plat/dojo/utils/RepeatingPanelUtils", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!sc/plat/dojo/utils/RequestUtils", "scbase/loader!sc/plat/dojo/utils/PlatformUIFmkImplUtils", "scbase/loader!ias/utils/ScreenUtils"], function(_dojodeclare, _extnBatchPickListExtnUI, _scWidgetUtils, _scBaseUtils, _scEventUtils, _scLabel, _scLink, _scScreen, _scModelUtils, _iasUIUtils, _scScreenUtils, _iasBaseTemplateUtils, _scGridxUtils, _gridxGrid, _scGridxDataBinder, _iasRepeatingScreenUtils, _scRepeatingPanelUtils, _scUserprefs, _iasContextUtils, _extnExpicientUtils, _scRequestUtils, _scPlatformUIFmkImplUtils, _iasScreenUtils) {
        return _dojodeclare("extn.components.batch.batchlist.BatchPickListExtn", [_extnBatchPickListExtnUI], {
            // custom code here
            extn_afterScreenInit: function(event, bEvent, ctrl, args) {
                var flowFlag = _scUserprefs.getProperty("DissolveBatchFlow");
                if (!_scBaseUtils.isVoid(flowFlag) && _scBaseUtils.equals(flowFlag, "Y")) {
                    var batchNo = _scUserprefs.getProperty("DissolveBatchNo");
                    msg = "Batch " + batchNo + " was dissolved successfully.";
                    textObj = {};
                    textObj["OK"] = "Close";
                    _scScreenUtils.showSuccessMessageBox(this, msg, null, textObj, null);
                }
                _scUserprefs.setProperty("DissolveBatchFlow", "");
                _scUserprefs.setProperty("DissolveBatchNo", "");
                var currScreen = _scModelUtils.getModelObjectFromPath("screen", args);
                var batchListModel = _scScreenUtils.getModel(currScreen, "getBatchList_output");
                var filterOptionModel = _scModelUtils.getModelObjectFromPath("Page.Output.StoreBatchList.StoreBatch", batchListModel);
                if(!_scBaseUtils.isVoid(filterOptionModel)){
                var Code = _scModelUtils.getStringValueFromPath("OrganizationCode", filterOptionModel[0]);
                var apiInput = _scModelUtils.createNewModelObjectWithRootKey("Department");
                _scModelUtils.setStringValueAtModelPath("Department.CallingOrganizationCode", Code, apiInput);
                _iasUIUtils.callApi(this, apiInput, "extn_getDepartmentList_ref", null);
            }
        },
            extn_afterBehaviorMashupCall: function(event, bEvent, ctrl, args) {
                var isMashupAvailable1 = _extnExpicientUtils.isMashupAvailable("extn_getDepartmentList_ref", args);
                if (isMashupAvailable1) {
                    var outputData = null;
                    var map1 = {}
                    outputData = _extnExpicientUtils.getMashupOutputForRefID("extn_getDepartmentList_ref", args);
                    depList = _scModelUtils.getStringValueFromPath("DepartmentList.Department", outputData);
                    for (i = 0; i < depList.length; i++) {
                        deptCode = _scModelUtils.getStringValueFromPath("DepartmentCode", depList[i]);
                        deptName = _scModelUtils.getStringValueFromPath("DepartmentName", depList[i]);
                        map1[deptCode] = deptName;
                    }
                    _scUserprefs.setProperty("DEP_GROUP", map1);
                }
            }
    });
    });
