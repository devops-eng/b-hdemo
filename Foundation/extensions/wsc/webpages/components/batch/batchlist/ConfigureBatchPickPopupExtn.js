scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/components/batch/batchlist/ConfigureBatchPickPopupExtnUI", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/widgets/Label", "scbase/loader!sc/plat/dojo/widgets/Link", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/GridxUtils", "scbase/loader!gridx/Grid", "scbase/loader!sc/plat/dojo/binding/GridxDataBinder", "scbase/loader!ias/utils/RepeatingScreenUtils", "scbase/loader!sc/plat/dojo/utils/RepeatingPanelUtils", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!sc/plat/dojo/utils/RequestUtils"], function(_dojodeclare, _extnConfigureBatchPickPopupExtnUI, _scWidgetUtils, _scBaseUtils, _scEventUtils, _scLabel, _scLink, _scScreen, _scModelUtils, _iasUIUtils, _scScreenUtils, _iasBaseTemplateUtils, _scWidgetUtils, _scEventUtils, _scGridxUtils, _gridxGrid, _scGridxDataBinder, _iasRepeatingScreenUtils, _scRepeatingPanelUtils, _scUserprefs, _iasContextUtils, _extnExpicientUtils, _scRequestUtils) {
    return _dojodeclare("extn.components.batch.batchlist.ConfigureBatchPickPopupExtn", [_extnConfigureBatchPickPopupExtnUI], {
        // custom code here
        extn_afterTxtBatchSize_onKeyUp: function(event, bEvent, ctrl, args) {
            _scWidgetUtils.clearFieldinError(this, "txtBatchSize");
            var isValid = true;
            var batchSizeValue = _scWidgetUtils.getValue(this, "txtBatchSize");
            if (_scBaseUtils.isVoid(batchSizeValue)) {
                _scWidgetUtils.markFieldinError(this, "txtBatchSize", _scScreenUtils.getString(this, "Message_NotValidNumber"), false);
                isValid = false;
            } else {
                batchSizeValue = Number(batchSizeValue);
                if (_scBaseUtils.lessThan(batchSizeValue, 1)) {
                    isValid = false;
                    _scWidgetUtils.markFieldinError(this, "txtBatchSize", _scScreenUtils.getString(this, "Message_MinBatchSize"), false);
                } else if (_scBaseUtils.greaterThan(batchSizeValue, 500)) {
                    isValid = false;
                    _scWidgetUtils.markFieldinError(this, "txtBatchSize", _scScreenUtils.getString(this, "extn_MaxBatchSize_500"), false);
                }
            }
            if (isValid) {
                _scWidgetUtils.clearFieldinError(this, "txtBatchSize");
            }
        },
        extn_beforeBehaviorMashupCall: function(event, bEvent, ctrl, args) {
            var configureBy = _scWidgetUtils.getValue(this, "extn_configureByRadBttn");
            if (_extnExpicientUtils.isBeforeBehaviorMashupAvailable("manageUserUiState_batchSortMethod", args)) {
                var inpModel = _extnExpicientUtils.getMashupInputForRefID("manageUserUiState_batchSortMethod", args);
                _scModelUtils.setStringValueAtModelPath("UserUiState.Extn.ExtnConfigureBatch", configureBy, inpModel);
                _scModelUtils.setStringValueAtModelPath("UserUiState.Definition", "SORT_AFTER_PICK", inpModel);
            }
        }
    });
});
