scDefine(["dojo/text!./templates/ConfigureBatchPickPopupExtn.html", "scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/_base/lang", "scbase/loader!dojo/text", "scbase/loader!idx/form/NumberTextBox", "scbase/loader!idx/form/RadioButtonSet", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat", "scbase/loader!sc/plat/dojo/binding/CurrencyDataBinder", "scbase/loader!sc/plat/dojo/binding/RadioSetDataBinder", "scbase/loader!sc/plat/dojo/binding/SimpleDataBinder", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/widgets/Label"], function(templateText, _dojodeclare, _dojokernel, _dojolang, _dojotext, _idxNumberTextBox, _idxRadioButtonSet, _idxContentPane, _scplat, _scCurrencyDataBinder, _scRadioSetDataBinder, _scSimpleDataBinder, _scBaseUtils, _scLabel) {
    return _dojodeclare("extn.components.batch.batchlist.ConfigureBatchPickPopupExtnUI", [], {
        templateString: templateText,
        hotKeys: [],
        events: [],
        subscribers: {
            local: [{
                eventId: 'txtBatchSize_onKeyUp',
                sequence: '51',
                handler: {
                    methodName: "extn_afterTxtBatchSize_onKeyUp"
                }
            }, {
                eventId: 'beforeBehaviorMashupCall',
                sequence: '51',
                handler: {
                    methodName: "extn_beforeBehaviorMashupCall"
                }
            }]
        }
    });
});
