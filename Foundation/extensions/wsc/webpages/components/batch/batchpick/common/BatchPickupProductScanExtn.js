scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/components/batch/batchpick/common/BatchPickupProductScanExtnUI", "scbase/loader!sc/plat/dojo/utils/EditorUtils", "scbase/loader!ias/utils/WizardUtils", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/widgets/Label", "scbase/loader!sc/plat/dojo/widgets/Link", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/GridxUtils", "scbase/loader!gridx/Grid", "scbase/loader!sc/plat/dojo/binding/GridxDataBinder", "scbase/loader!ias/utils/RepeatingScreenUtils", "scbase/loader!sc/plat/dojo/utils/RepeatingPanelUtils", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!sc/plat/dojo/utils/RequestUtils", "scbase/loader!ias/utils/ScreenUtils"], function(_dojodeclare, _extnBatchPickupProductScanExtnUI, _scEditorUtils, _iasWizardUtils, _scBaseUtils, _scEventUtils, _scLabel, _scLink, _scScreen, _scModelUtils, _iasUIUtils, _scScreenUtils, _iasBaseTemplateUtils, _scWidgetUtils, _scEventUtils, _scGridxUtils, _gridxGrid, _scGridxDataBinder, _iasRepeatingScreenUtils, _scRepeatingPanelUtils, _scUserprefs, _iasContextUtils, _extnExpicientUtils, _scRequestUtils, _iasScreenUtils) {
    return _dojodeclare("extn.components.batch.batchpick.common.BatchPickupProductScanExtn", [_extnBatchPickupProductScanExtnUI], {
        // custom code here
        uniqueId: null,
        lastJewelleryItem: false,
        imgID: "up",
        previous: null,
        mashupResult: null,
        // tempBatchModel:null,
        extn_afterScreenInit: function(event, bEvent, ctrl, args) {
            _scWidgetUtils.hideWidget(this, "extn_styleLinkImg");
            _scWidgetUtils.hideWidget(this, "extn_departmentLinkImg");
            _scWidgetUtils.hideWidget(this, "extn_dmmLinkImg");
            _scWidgetUtils.hideWidget(this, "extn_labelLinkImg");
            this.imgID = "down";
            desc = this.showRelevantImage("extn_slaLinkImg");
            var model = _scScreenUtils.getModel(this, "StoreBatchLines");
            var batchNo = _scModelUtils.getStringValueFromPath("Page.Output.StoreBatch.BatchNo", model);
            _scWidgetUtils.setValue(this, "extn_batchNoLbl", "Batch " + batchNo);
        },
        extn_afterScreenLoad: function(event, bEvent, ctrl, args) {
            var wizardScreen = _iasUIUtils.getParentScreen(this, true);
            _iasWizardUtils.hideNavigationalWidget(wizardScreen, "nextBttn", false);
            // CSS changes for Mobile - START
            if (_iasContextUtils.isMobileContainer()) {
                _scWidgetUtils.addClass(this, "extn_sortByPane", "batchSortContainerMobile");
                _scWidgetUtils.addClass(this, "extn_slaContainer", "containerSortMobile");
                _scWidgetUtils.removeClass(this, "extn_slaContainer", "containerSort");
                _scWidgetUtils.addClass(this, "extn_dmmContainer", "containerSortMobile");
                _scWidgetUtils.removeClass(this, "extn_dmmContainer", "containerSort");
                _scWidgetUtils.addClass(this, "extn_departmentContainer", "containerSortMobile");
                _scWidgetUtils.removeClass(this, "extn_departmentContainer", "containerSort");
                _scWidgetUtils.addClass(this, "extn_labelContainer", "containerSortMobile");
                _scWidgetUtils.removeClass(this, "extn_labelContainer", "containerSort");
                _scWidgetUtils.addClass(this, "extn_styleContainer", "containerSortMobile");
                _scWidgetUtils.removeClass(this, "extn_styleContainer", "containerSort");
                _scWidgetUtils.addClass(this, "extn_priceAttributeContainer", "containerSortMobile");
                _scWidgetUtils.removeClass(this, "extn_priceAttributeContainer", "containerSort");
                _scWidgetUtils.addClass(this, "unpickedLineList", "margin-top-0");
                _scWidgetUtils.addClass(this, "extn_dissolveContainer", "margin-top-15");
                _scWidgetUtils.addClass(this, "extn_dissolveContainer", "alignDissolveMobile");
                _scWidgetUtils.removeClass(this, "extn_dissolveContainer", "alignDissolve");
                //_scWidgetUtils.removeClass(this, "extn_dissolveBatch", "left620");
                _scWidgetUtils.addClass(this, "extn_dissolveBatch", "left400");
                _scWidgetUtils.addClass(this, "scanPanel", "margin-top-52");
                _scWidgetUtils.addClass(this, "batchLineActionsPane", "margin-top-200");
                _scWidgetUtils.addClass(this, "topPanel", "marginZeroWidth");
                _scWidgetUtils.addClass(this, "batchLineListContainer", "screenSwipe");
                _scWidgetUtils.addClass(this.getOwnerScreen(), "pnlBatchDetails", "displayNone")
            } else {
                _scWidgetUtils.hideWidget(this, "extn_batchNoLbl");
            }
            // CSS Changes for Mobile - END
        },
        extn_printPickTicket: function(event, bEvent, ctrl, args) {
            var inputArray = _scBaseUtils.getNewArrayInstance();
            var newBatchLine = _scBaseUtils.getNewModelInstance();
            var model = _scScreenUtils.getModel(this, "StoreBatchLines");
            var stringLength = _scModelUtils.getStringValueFromPath("Page.Output.StoreBatch.StoreBatchLines.TotalNumberOfRecords", model);
            var length = parseInt(stringLength);
            batchLine = _scModelUtils.getStringValueFromPath("Page.Output.StoreBatch.StoreBatchLines.StoreBatchLine", model);
            for (var index = 0; _scBaseUtils.lessThan(index, length); index = _scBaseUtils.incrementNumber(index, 1)) {
                currBatchLine = _scBaseUtils.getArrayBeanItemByIndex(batchLine, index);
                var isSelected = _scModelUtils.getStringValueFromPath("IsSelected", currBatchLine);
                if (!_scBaseUtils.isVoid(isSelected) && _scBaseUtils.equals(isSelected, "Y")) {
                    var inpArray = {};
                    inpArray.ItemID = _scModelUtils.getStringValueFromPath("ItemDetails.ItemID", currBatchLine);
                    inpArray.UnitOfMeasure = _scModelUtils.getStringValueFromPath("ItemDetails.UnitOfMeasure", currBatchLine);
                    inpArray.ProductClass = _scModelUtils.getStringValueFromPath("ItemDetails.Item.0.PrimaryInformation.DefaultProductClass", currBatchLine);
                    inputArray.push(inpArray);
                    _scModelUtils.setStringValueAtModelPath("StoreBatchLines.StoreBatchLine", inputArray, newBatchLine);
                }
            }
            var userName = _scUserprefs.getUserName();
            var org = _scUserprefs.getUserOrganization();
            var orgCode = _scModelUtils.getStringValueFromPath("Page.Output.StoreBatch.OrganizationCode", model);
            var storeBatchKey = _scModelUtils.getStringValueFromPath("Page.Output.StoreBatch.StoreBatchKey", model);
            _scModelUtils.setStringValueAtModelPath("StoreBatchLines.StoreBatchKey", storeBatchKey, newBatchLine);
            _scModelUtils.setStringValueAtModelPath("StoreBatchLines.Store", orgCode, newBatchLine);
            _scModelUtils.setStringValueAtModelPath("StoreBatchLines.UserName", userName, newBatchLine);
            var label = _scModelUtils.getStringValueFromPath("originatingControlInstance._explicitLabel", args);
            if (_scBaseUtils.equals(label, "Print All")) {
                _scModelUtils.setStringValueAtModelPath("StoreBatchLines.PrintAll", "Y", newBatchLine);
            } else {
                _scModelUtils.setStringValueAtModelPath("StoreBatchLines.PrintAll", "N", newBatchLine);
            }
            _iasUIUtils.callApi(this, newBatchLine, "extn_printPickSlip_ref", null);
        },
        extn_afterBehaviorMashupCall: function(event, bEvent, ctrl, args) {
            if (_extnExpicientUtils.isMashupAvailable("extn_printPickSlip_ref", args)) {
                outputModel = _extnExpicientUtils.getMashupOutputForRefID("extn_printPickSlip_ref", args);
                this.openPDF(outputModel);
            }
            if (_extnExpicientUtils.isMashupAvailable("extn_dissolveBatch_ref", args)) {
                outputModel = _extnExpicientUtils.getMashupOutputForRefID("extn_dissolveBatch_ref", args);
                if (_scModelUtils.hasAttributeInModelPath("Error.ErrorMessage", outputModel)) {
                    var textObj = {};
                    textObj["OK"] = "Ok";
                    errorMsg = this.getSimpleBundleString("extn_dissolveBatchErrorMsg");
                    _scScreenUtils.showErrorMessageBox(this, errorMsg, null, textObj, null);
                } else {
                    var batchModel = _scScreenUtils.getModel(this, "StoreBatchLines");
                    var storeBatch = _scModelUtils.getStringValueFromPath("Page.Output.StoreBatch", batchModel);
                    var batchNo = _scModelUtils.getStringValueFromPath("BatchNo", storeBatch);
                    _scUserprefs.setProperty("DissolveBatchFlow", "Y");
                    _scUserprefs.setProperty("DissolveBatchNo", batchNo);
                    this.closeWizard();
                }
            }
            if (_extnExpicientUtils.isMashupAvailable("recordShortageForBatchPick", args)) {
                inputModel = _extnExpicientUtils.getMashupOutputForRefID("recordShortageForBatchPick", args);
                var itemID = _scModelUtils.getStringValueFromPath("StoreBatch.StoreBatchLines.StoreBatchLine.0.ItemID", inputModel);
                var uom = _scModelUtils.getStringValueFromPath("StoreBatch.StoreBatchLines.StoreBatchLine.0.UnitOfMeasure", inputModel);
                var uniqueId = itemID + "|" + uom;
                this.uniqueId = uniqueId;
            }
            if (_extnExpicientUtils.isMashupAvailable("extn_sortBatchList_ref", args)) {
                sortStatusModel = _scScreenUtils.getModel(this, "lastSelectedBatchLineStatus");
                sortStatus = _scModelUtils.getStringValueFromPath("BatchLineStatus.StatusCode", sortStatusModel);
                outputModel = _extnExpicientUtils.getMashupOutputForRefID("extn_sortBatchList_ref", args);
                var stringLength = _scModelUtils.getStringValueFromPath("Page.Output.StoreBatch.StoreBatchLines.TotalNumberOfRecords", outputModel);
                var length = parseInt(stringLength);
                batchLine = _scModelUtils.getStringValueFromPath("Page.Output.StoreBatch.StoreBatchLines.StoreBatchLine", outputModel);
                for (var index = length - 1; _scBaseUtils.greaterThanOrEqual(index, 0); index = index - 1) {
                    currBatchLine = _scBaseUtils.getArrayBeanItemByIndex(batchLine, index);
                    var bpComplete = _scModelUtils.getStringValueFromPath("BackroomPickComplete", currBatchLine);
                    if (_scBaseUtils.equals("Y", bpComplete) && _scBaseUtils.equals("toBePickedLines", sortStatus)) {
                        _scBaseUtils.removeItemFromArray(batchLine, index);
                        batchLine.splice(index, 1);
                    }
                }
                _scScreenUtils.setModel(this, "StoreBatchLines", outputModel);
            }
            if (_extnExpicientUtils.isMashupAvailable("registerBatchPickManualModeSAP", args)) {
                mashupRefId = "registerBatchPickManualModeSAP";
                var mode = "MANUALPICK";
                this.ifMultiLine(mashupRefId, args, mode);
                //this.registerBatchAndCasePopup(mashupRefId, args, mode);
            }
            if (_extnExpicientUtils.isMashupAvailable("registerBatchPickScanModeSAP", args)) {
                mashupRefId = "registerBatchPickScanModeSAP";
                var mode = "SCAN";
                this.ifMultiLine(mashupRefId, args, mode);
                //this.registerBatchAndCasePopup(mashupRefId, args, mode);
            }
            if (_extnExpicientUtils.isMashupAvailable("extn_removeCaseNumberService", args)) {
                console.log("asasa");
            }
        },
        ifMultipleShipmentLines: function(outModel) {
            // var outModel = _extnExpicientUtils.getMashupOutputForRefID(mashupRefId, args);
            var shipment = null;
            shipment = _scModelUtils.getModelObjectFromPath("StoreBatch.ShipmentLines.ShipmentLine", outModel);
            if (shipment) {
                var multiLineFlag = _scModelUtils.getStringValueFromPath("Extn.ExtnIsMultiline", shipment);
                return multiLineFlag;
            }
        },
        ifMultiLine: function(mashupRefId, args, mode) {
            var outModel = _extnExpicientUtils.getMashupOutputForRefID(mashupRefId, args);
            var isMultiLine = this.ifMultipleShipmentLines(outModel);
            if (_scBaseUtils.equals(isMultiLine, "Y")) {
                var inputModel = _extnExpicientUtils.getInputMashupForRefID(mashupRefId, args);
                var action = _scModelUtils.getStringValueFromPath("StoreBatch.Item.Action", inputModel);
                if (_scBaseUtils.equals(action, "PICK") || _scBaseUtils.equals(mode, "SCAN")) {
                    var outputObject = {
                        "mashupRefId": mashupRefId,
                        "args": args,
                        "mode": mode,
                        "OK": "OK"
                    };
                    var messageArgs = {};
                    this.mashupResult = null;
                    this.mashupResult = outputObject;
                    var status = _scModelUtils.getStringValueFromPath("StoreBatch.Status", outModel);
                    _scScreenUtils.showInfoMessageBox(this, "This item is part of a multi-line order", "extn_furtherOperation", outputObject, messageArgs);
                    // return;
                } else {
                    this.registerBatchAndCasePopup(mashupRefId, args, mode);
                }
            } else {
                this.registerBatchAndCasePopup(mashupRefId, args, mode);
            }
        },
        extn_furtherOperation: function() {
            if (this.mashupResult) {
                var mashupRefId = this.mashupResult.mashupRefId;
                var args = this.mashupResult.args;
                var mode = this.mashupResult.mode;
                this.registerBatchAndCasePopup(mashupRefId, args, mode);
            }
        },
        registerBatchAndCasePopup: function(mashupRefId, args, mode) {
            var outModel = _extnExpicientUtils.getMashupOutputForRefID(mashupRefId, args);
            var isJewellaryItem = _scModelUtils.getStringValueFromPath("StoreBatch.ShipmentLines.ShipmentLine.Extn.ExtnJewellryItemFlag", outModel);
            if (isJewellaryItem != "Y") {
                this.registerBatch(mashupRefId, args);
            }
            this.checkBatchPickMode(mashupRefId, args, mode);
        },
        checkBatchPickMode: function(mashupRefId, args, mode) {
            var inputModel = _extnExpicientUtils.getInputMashupForRefID(mashupRefId, args);
            var outModel = _extnExpicientUtils.getMashupOutputForRefID(mashupRefId, args);
            _scScreenUtils.setModel(this, "isJewellaryItem_output", outModel, null);
            var isJewellaryItem = _scModelUtils.getStringValueFromPath("StoreBatch.ShipmentLines.ShipmentLine.Extn.ExtnJewellryItemFlag", outModel);
            if (isJewellaryItem != undefined && isJewellaryItem == "Y") {
                var action = _scModelUtils.getStringValueFromPath("StoreBatch.Item.Action", inputModel);
                if (_scBaseUtils.equals(action, "UNDOPICK")) {
                    var outModel = _extnExpicientUtils.getMashupOutputForRefID(mashupRefId, args);
                    var newinputModel = _scBaseUtils.getNewBeanInstance();
                    var shipmentLineKey = _scModelUtils.getStringValueFromPath("StoreBatch.ShipmentLines.ShipmentLine.ShipmentLineKey", outModel);
                    _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLine.ShipmentLineKey", shipmentLineKey, newinputModel);
                    var shipmentKey = _scModelUtils.getStringValueFromPath("StoreBatch.ShipmentLines.ShipmentLine.ShipmentKey", outModel);
                    _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", shipmentKey, newinputModel);
                    _iasUIUtils.callApi(this, newinputModel, "extn_removeCaseNumberService", null);
                } else {
                    this.openCaseNoPopUp(mashupRefId, args, mode);
                }
            } else {
                var status = _scModelUtils.getStringValueFromPath("StoreBatch.Status", outModel);
                if (_scBaseUtils.contains(status, "2000")) {
                    var msg = _scScreenUtils.getString(this, "Message_AllBatchLinesPicked");
                    textObj = {};
                    var messageObj = {};
                    textObj["OK"] = _scScreenUtils.getString(this, "Action_Finish");
                    _scScreenUtils.showSuccessMessageBox(this, msg, "handleFinshBatchPickup", textObj, messageObj);
                }
            }
        },
        openCaseNoPopUp: function(mashupRefId, args, mode) {
            var bindings = _scBaseUtils.getNewBeanInstance();
            var popupParams = _scBaseUtils.getNewModelInstance();
            var dialogParams = _scBaseUtils.getNewModelInstance();
            var newinputModel = _scBaseUtils.getNewModelInstance();
            var outModel = _extnExpicientUtils.getMashupOutputForRefID(mashupRefId, args);
            var inputModel = _extnExpicientUtils.getInputMashupForRefID(mashupRefId, args);
            var completeOrderModel = _scBaseUtils.getNewBeanInstance();
            if (_scBaseUtils.equals(mode, "SCAN")) {
                var upcCode = _scModelUtils.setStringValueAtModelPath("StoreBatch.Item.UPCCode", inputModel.StoreBatch.BarCode.BarCodeData, newinputModel);
            }
            var currentStore = _iasContextUtils.getFromContext("CurrentStore")
            _scModelUtils.setStringValueAtModelPath("StoreBatch.Mode", mode, newinputModel);
            _scModelUtils.setStringValueAtModelPath("StoreBatch.Item.Action", "UNDOPICK", newinputModel);
            var userId = _scUserprefs.getUserId();
            _scModelUtils.setStringValueAtModelPath("StoreBatch.StoreNo", currentStore, newinputModel);
            _scModelUtils.setStringValueAtModelPath("StoreBatch.UserID", userId, newinputModel);
            var storeBatchKey = _scModelUtils.getStringValueFromPath("StoreBatch.StoreBatchKey", outModel);
            _scModelUtils.setStringValueAtModelPath("StoreBatch.StoreBatchKey", storeBatchKey, newinputModel);
            _scModelUtils.setStringValueAtModelPath("StoreBatch.Mode", mode, outModel);
            var shipmentLineKey = _scModelUtils.getStringValueFromPath("StoreBatch.ShipmentLines.ShipmentLine.ShipmentLineKey", outModel);
            _scModelUtils.setStringValueAtModelPath("StoreBatch.ShipmentLineKey", shipmentLineKey, newinputModel);
            _scModelUtils.setStringValueAtModelPath("StoreBatch.Item.ItemID", outModel.StoreBatch.ShipmentLines.ShipmentLine.ItemID, newinputModel);
            var shipmentKey = _scModelUtils.getStringValueFromPath("StoreBatch.ShipmentLines.ShipmentLine.ShipmentKey", outModel);
            _scModelUtils.setStringValueAtModelPath("StoreBatch.ShipmentKey", shipmentKey, newinputModel);
            //_scModelUtils.setStringValueAtModelPath("StoreBatch.Item.ItemID", inputModel.StoreBatch.Item.ItemID, outModel);
            _scModelUtils.setStringValueAtModelPath("IsBatchPick", "Y", newinputModel);
            _scModelUtils.setStringValueAtModelPath("IsBatchPick", "Y", outModel);
            _scBaseUtils.addModelValueToBean("CompleteOrderData", outModel, bindings);
            _scBaseUtils.addModelValueToBean("InputOrderData", newinputModel, bindings);
            _scBaseUtils.addBeanValueToBean("binding", bindings, popupParams);
            _scBaseUtils.setAttributeValue("closeCallBackHandler", "caseNoCallBackHandler", dialogParams);
            _scBaseUtils.addStringValueToBean("outputNamespace", "caseNo_getCompleteOrderDetails_output", popupParams);
            _iasUIUtils.openSimplePopup("extn.customScreen.CaseNoScan", "", this, popupParams, dialogParams);
        },
        caseNoCallBackHandler: function(actionPerformed, model, popupParams) {
            if (actionPerformed == "APPLY") {
                this.registerBatchJewellary(model);
            }
            var bModel = model;
            if (_scBaseUtils.equals(bModel.StatusChanged, "Y")) {
                this.lastJewelleryItem = false;
            }
            //  var flag=model.Order.QuoteSentSuccessfully;
            if (!(_scBaseUtils.isVoid(model))) {
                if (model.RefreshScreen != undefined && model.RefreshScreen == "Y") {
                    var mode = _scModelUtils.getStringValueFromPath("StoreBatch.Mode", model);
                    if (_scBaseUtils.equals(mode, "MANUALPICK")) {
                        // _scWidgetUtils.closePopup(model.StoreBatch.childscreen, "CANCEL", false);
                        _scScreenUtils.setModel(this, "registerBatchPickManualModeSAP_output", model, null);
                        this.registerBatchPickManualModeSAP(model);
                    } else {
                        _scScreenUtils.setModel(this, "registerBatchPickScanModeSAP_output", model, null);
                        this.registerBatchPickManualModeSAP(model);
                    }
                } else {
                    if (!_scBaseUtils.equals(model.StatusChanged, "Y")) {
                        var status = _scModelUtils.getStringValueFromPath("StoreBatch.Status", model);
                        if (_scBaseUtils.contains(status, "2000")) {
                            var msg = _scScreenUtils.getString(this, "Message_AllBatchLinesPicked");
                            textObj = {};
                            var messageObj = {};
                            textObj["OK"] = _scScreenUtils.getString(this, "Action_Finish");
                            _scScreenUtils.showSuccessMessageBox(this, msg, "handleFinshBatchPickup", textObj, messageObj);
                        }
                    }
                }
            }
        },
        registerBatch: function(mashupRefId, args) {
            sortStatusModel = _scScreenUtils.getModel(this, "lastSelectedBatchLineStatus");
            sortStatus = _scModelUtils.getStringValueFromPath("BatchLineStatus.StatusCode", sortStatusModel);
            if (_scBaseUtils.equals(sortStatus, "toBePickedLines")) {
                inputModel = _extnExpicientUtils.getMashupOutputForRefID(mashupRefId, args);
                this.registerBatchModel(inputModel);
            }
        },
        registerBatchModel: function(inputModel) {
            var itemID = _scModelUtils.getStringValueFromPath("StoreBatch.StoreBatchLine.ItemID", inputModel);
            var uom = _scModelUtils.getStringValueFromPath("StoreBatch.StoreBatchLine.UnitOfMeasure", inputModel);
            var bpQty = _scModelUtils.getStringValueFromPath("StoreBatch.StoreBatchLine.BackroomPickedQuantity", inputModel);
            var origQty = _scModelUtils.getStringValueFromPath("StoreBatch.StoreBatchLine.Quantity", inputModel);
            if (_scBaseUtils.equals(bpQty, origQty)) {
                if (!_scBaseUtils.isVoid(this.uniqueId)) {
                    var repPanelUId = _scRepeatingPanelUtils.returnUIdOfIndividualRepeatingPanel(this, "unpickedLineList", this.uniqueId);
                    _scWidgetUtils.hideWidget(this, repPanelUId);
                    this.uniqueId = null;
                }
                var uniqueId = itemID + "|" + uom;
                this.uniqueId = uniqueId;
            } else {
                if (!_scBaseUtils.isVoid(this.uniqueId)) {
                    var newUniqId = itemID + "|" + uom;
                    if (_scBaseUtils.equals(newUniqId, this.uniqueId)) {
                        this.uniqueId = null;
                    } else {
                        var repPanelUId = _scRepeatingPanelUtils.returnUIdOfIndividualRepeatingPanel(this, "unpickedLineList", this.uniqueId);
                        _scWidgetUtils.hideWidget(this, repPanelUId);
                        this.uniqueId = null;
                    }
                }
            }
        },
        registerBatchJewellary: function(model) {
            sortStatusModel = _scScreenUtils.getModel(this, "lastSelectedBatchLineStatus");
            sortStatus = _scModelUtils.getStringValueFromPath("BatchLineStatus.StatusCode", sortStatusModel);
            if (_scBaseUtils.equals(sortStatus, "toBePickedLines")) {
                this.registerBatchModel(model);
            }
        },
        sortBatchLines: function(sortValue, descValue) {
            inputModel = _scBaseUtils.getNewModelInstance();
            var storeBatchOp = _scScreenUtils.getModel(this, "StoreBatchLines");
            var orgCode = _scModelUtils.getStringValueFromPath("Page.Output.StoreBatch.OrganizationCode", storeBatchOp);
            var batchKey = _scModelUtils.getStringValueFromPath("Page.Output.StoreBatch.StoreBatchKey", storeBatchOp);
            _scModelUtils.setStringValueAtModelPath("Page.API.Input.StoreBatch.OrganizationCode", orgCode, inputModel);
            _scModelUtils.setStringValueAtModelPath("Page.API.Input.StoreBatch.StoreBatchKey", batchKey, inputModel);
            _scModelUtils.setStringValueAtModelPath("Page.API.Input.StoreBatch.Name", sortValue, inputModel);
            _scModelUtils.setStringValueAtModelPath("Page.API.Input.StoreBatch.Desc", descValue, inputModel);
            _iasUIUtils.callApi(this, inputModel, "extn_sortBatchList_ref", null);
        },
        showRelevantImage: function(imageLink) {
            _scWidgetUtils.showWidget(this, imageLink);
            _scWidgetUtils.addClass(this, imageLink, "sortByImg");
            var ret = null;
            var imgArrayList = ["extn_slaLinkImg", "extn_departmentLinkImg", "extn_dmmLinkImg", "extn_labelLinkImg", "extn_styleLinkImg", "extn_priceAttributeImg"];
            for (var index = 0; _scBaseUtils.lessThan(index, imgArrayList.length); index = _scBaseUtils.incrementNumber(index, 1)) {
                var currImg = _scBaseUtils.getArrayBeanItemByIndex(imgArrayList, index);
                if (!_scBaseUtils.equals(imageLink, currImg)) {
                    _scWidgetUtils.hideWidget(this, currImg);
                    var currLink = currImg.substring(0, currImg.length - 3);
                    _scWidgetUtils.addClass(this, currLink, "sortByImg");
                } else {
                    var currLink = currImg.substring(0, currImg.length - 3);
                    _scWidgetUtils.removeClass(this, currLink, "sortByImg");
                }
            }
            if (_scBaseUtils.isVoid(this.previous)) {
                if (_scBaseUtils.equals(this.imgID, "up")) {
                    _scWidgetUtils.setLinkImageSrc(this, imageLink, "wsc/resources/css/icons/images/arrowDown.png");
                    this.imgID = "down";
                    ret = "Y";
                } else {
                    _scWidgetUtils.setLinkImageSrc(this, imageLink, "wsc/resources/css/icons/images/arrowUp.png");
                    this.imgID = "up";
                    ret = "N";
                }
            } else {
                if (_scBaseUtils.equals(this.previous, imageLink)) {
                    if (_scBaseUtils.equals(this.imgID, "up")) {
                        _scWidgetUtils.setLinkImageSrc(this, imageLink, "wsc/resources/css/icons/images/arrowDown.png");
                        this.imgID = "down";
                        ret = "Y";
                    } else {
                        _scWidgetUtils.setLinkImageSrc(this, imageLink, "wsc/resources/css/icons/images/arrowUp.png");
                        this.imgID = "up";
                        ret = "N";
                    }
                } else {
                    if (_scBaseUtils.equals(this.imgID, "up")) {
                        _scWidgetUtils.setLinkImageSrc(this, imageLink, "wsc/resources/css/icons/images/arrowUp.png");
                        ret = "N";
                    } else {
                        _scWidgetUtils.setLinkImageSrc(this, imageLink, "wsc/resources/css/icons/images/arrowDown.png");
                        ret = "Y";
                    }
                }
            }
            this.previous = imageLink;
            return ret;
        },
        extn_slaOnClick: function(event, bEvent, ctrl, args) {
            desc = this.showRelevantImage("extn_slaLinkImg");
            this.sortBatchLines("SLA", desc);
        },
        extn_departmentOnClick: function(event, bEvent, ctrl, args) {
            desc = this.showRelevantImage("extn_departmentLinkImg");
            this.sortBatchLines("DepartmentCode", desc);
        },
        extn_dmmOnClick: function(event, bEvent, ctrl, args) {
            desc = this.showRelevantImage("extn_dmmLinkImg");
            this.sortBatchLines("DMM", desc);
        },
        extn_labelOnClick: function(event, bEvent, ctrl, args) {
            desc = this.showRelevantImage("extn_labelLinkImg");
            this.sortBatchLines("Label", desc);
        },
        extn_styleOnClick: function(event, bEvent, ctrl, args) {
            desc = this.showRelevantImage("extn_styleLinkImg");
            this.sortBatchLines("Style", desc);
        },
        extn_priceAttributeOnClick: function(event, bEvent, ctrl, args) {
            desc = this.showRelevantImage("extn_priceAttributeImg");
            this.sortBatchLines("Extn_ExtnPriceAttribute", desc);
        },
        extn_dissolveBatchOnClick: function(event, bEvent, ctrl, args) {
            var textObj = {};
            textObj["OK"] = "Confirm";
            textObj["CANCEL"] = "Cancel";
            var messageObj = {};
            errorMsg = this.getSimpleBundleString("extn_dissolveBatchConfirmationMsg");
            _scScreenUtils.showWarningMessageBox(this, errorMsg, "dissolveBatchHandler", messageObj, textObj);
        },
        dissolveBatchHandler: function(event, bEvent, ctrl, args) {
            if (_scBaseUtils.equals(event, "Ok")) {
                var batchModel = _scScreenUtils.getModel(this, "StoreBatchLines");;
                var batchKey = _scModelUtils.getStringValueFromPath("Page.Output.StoreBatch.StoreBatchKey", batchModel);
                var inputModel = _scBaseUtils.getNewModelInstance();
                _scModelUtils.setStringValueAtModelPath("StoreBatch.StoreBatchKey", batchKey, inputModel);
                _iasUIUtils.callApi(this, inputModel, "extn_dissolveBatch_ref", null);
            }
        },
        openPDF: function(modelOutput) {
            var fileName = _scModelUtils.getStringValueFromPath("PickListPDF.PdfFileName", modelOutput);
            var location = window.location.origin + window.contextPath + "/" + fileName;
            window.open(location);
        },
        // Overriding the OOB showBatchPickingCompletionSuccess so as to remove the Assign Staging Location
        // button in the popup (showing a new successMessageBox)
        showBatchPickingCompletionSuccess: function(event, bEvent, ctrl, args) {
            // var jelwellaryOutput = _scScreenUtils.getModel(this, "isJewellaryItem_output");
            // var isJewellaryItem = _scModelUtils.getStringValueFromPath("StoreBatch.ShipmentLines.ShipmentLine.Extn.ExtnJewellryItemFlag", jelwellaryOutput);
            // if (isJewellaryItem != undefined && isJewellaryItem == "Y") {
            //     this.lastJewelleryItem = true;
            // } else {
            //     var msg = _scScreenUtils.getString(this, "Message_AllBatchLinesPicked");
            //     textObj = {};
            //     textObj["OK"] = _scScreenUtils.getString(this, "Action_Finish");
            //     _scScreenUtils.showSuccessMessageBox(this, msg, "handleFinshBatchPickup", textObj, null);
            // }
        },
        extn_beforeBehaviorMashupCall: function(event, bEvent, ctrl, args) {
            if (_extnExpicientUtils.isBeforeBehaviorMashupAvailable("recordShortageForBatchPick", args)) {
                var inpModel = _extnExpicientUtils.getMashupInputForRefID("recordShortageForBatchPick", args);
                var org = _scUserprefs.getUserOrganization();
                var parentOrg = _scModelUtils.getStringValueFromPath("Organization.ParentOrganizationCode", org);
                _scModelUtils.setStringValueAtModelPath("StoreBatch.OrganizationCode", parentOrg, inpModel);
            }
        }
    });
});
