scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/text", "scbase/loader!extn/components/batch/batchpick/common/BatchPickupProductScanExtn", "scbase/loader!sc/plat/dojo/controller/ExtnServerDataController"], function(_dojodeclare, _dojokernel, _dojotext, _extnBatchPickupProductScanExtn, _scExtnServerDataController) {
    return _dojodeclare("extn.components.batch.batchpick.common.BatchPickupProductScanExtnBehaviorController", [_scExtnServerDataController], {
        screenId: 'extn.components.batch.batchpick.common.BatchPickupProductScanExtn',
        mashupRefs: [{
            extnType: 'MODIFY',
            mashupId: 'batchPickup_getAllStoreBatchLines',
            mashupRefId: 'getAllStoreBatchLines'
        }, {
            extnType: 'MODIFY',
            mashupId: 'batchPickup_recordShortageForBatchPick',
            mashupRefId: 'recordShortageForBatchPick'
        }, {
            extnType: 'MODIFY',
            mashupId: 'batchPickup_registerBatchPickScanModeSAP',
            mashupRefId: 'registerBatchPickScanModeSAP'
        }, {
            extnType: 'ADD',
            mashupId: 'extn_printPickSlip',
            mashupRefId: 'extn_printPickSlip_ref'
        }, {
            extnType: 'ADD',
            mashupId: 'extn_sortBatchList',
            mashupRefId: 'extn_sortBatchList_ref'
        }, {
            extnType: 'ADD',
            mashupId: 'extn_dissolveBatch',
            mashupRefId: 'extn_dissolveBatch_ref'
        }, {
            extnType: 'ADD',
            mashupId: 'removeCaseNo',
            mashupRefId: 'extn_removeCaseNumberService'
        }]
    });
});
