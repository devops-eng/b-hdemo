scDefine(["dojo/text!./templates/BatchPickupProductScanExtn.html", "scbase/loader!dijit/form/Button", "scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/_base/lang", "scbase/loader!dojo/text", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat", "scbase/loader!sc/plat/dojo/binding/ButtonDataBinder", "scbase/loader!sc/plat/dojo/binding/CurrencyDataBinder", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/widgets/Label", "scbase/loader!sc/plat/dojo/widgets/Link"], function(templateText, _dijitButton, _dojodeclare, _dojokernel, _dojolang, _dojotext, _idxContentPane, _scplat, _scButtonDataBinder, _scCurrencyDataBinder, _scBaseUtils, _scLabel, _scLink) {
    return _dojodeclare("extn.components.batch.batchpick.common.BatchPickupProductScanExtnUI", [], {
        templateString: templateText,
        hotKeys: [],
        events: [],
        subscribers: {
            local: [{
                eventId: 'afterScreenInit',
                sequence: '51',
                handler: {
                    methodName: "extn_afterScreenInit"
                }
            }, {
                eventId: 'afterScreenLoad',
                sequence: '51',
                handler: {
                    methodName: "extn_afterScreenLoad"
                }
            }, {
                eventId: 'extn_printPick_onClick',
                sequence: '51',
                handler: {
                    methodName: "extn_printPickTicket"
                }
            }, {
                eventId: 'afterBehaviorMashupCall',
                sequence: '51',
                handler: {
                    methodName: "extn_afterBehaviorMashupCall"
                }
            }, {
                eventId: 'extn_sortBy_onChange',
                sequence: '51',
                handler: {
                    methodName: "extn_sortByOnChange"
                }
            }, {
                eventId: 'extn_slaLink_onClick',
                sequence: '51',
                handler: {
                    methodName: "extn_slaOnClick"
                }
            }, {
                eventId: 'extn_departmentLink_onClick',
                sequence: '51',
                handler: {
                    methodName: "extn_departmentOnClick"
                }
            }, {
                eventId: 'extn_dmmLink_onClick',
                sequence: '51',
                handler: {
                    methodName: "extn_dmmOnClick"
                }
            }, {
                eventId: 'extn_labelLink_onClick',
                sequence: '51',
                handler: {
                    methodName: "extn_labelOnClick"
                }
            }, {
                eventId: 'extn_styleLink_onClick',
                sequence: '51',
                handler: {
                    methodName: "extn_styleOnClick"
                }
            }, {
                eventId: 'checkForLastItem',
                sequence: '30',
                handler: {
                    methodName: "checkForLastItem"
                }
            }, {
                eventId: 'extn_dissolveBatch_onClick',
                sequence: '51',
                handler: {
                    methodName: "extn_dissolveBatchOnClick"
                }
            }, {
                eventId: 'beforeBehaviorMashupCall',
                sequence: '51',
                handler: {
                    methodName: "extn_beforeBehaviorMashupCall"
                }
            }, {
                eventId: 'extn_priceAttribute_onClick',
                sequence: '51',
                handler: {
                    methodName: "extn_priceAttributeOnClick"
                }
            }]
        }
    });
});
