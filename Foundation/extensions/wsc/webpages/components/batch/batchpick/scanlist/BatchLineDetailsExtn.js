scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/components/batch/batchpick/scanlist/BatchLineDetailsExtnUI", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/widgets/Label", "scbase/loader!sc/plat/dojo/widgets/Link", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/GridxUtils", "scbase/loader!gridx/Grid", "scbase/loader!sc/plat/dojo/binding/GridxDataBinder", "scbase/loader!ias/utils/RepeatingScreenUtils", "scbase/loader!sc/plat/dojo/utils/RepeatingPanelUtils", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!sc/plat/dojo/utils/RequestUtils", "scbase/loader!dojo/currency","scbase/loader!wsc/components/product/common/utils/ProductUtils"], function(_dojodeclare, _extnBatchLineDetailsExtnUI, _scWidgetUtils, _scBaseUtils, _scEventUtils, _scLabel, _scLink, _scScreen, _scModelUtils, _iasUIUtils, _scScreenUtils, _iasBaseTemplateUtils, _scWidgetUtils, _scEventUtils, _scGridxUtils, _gridxGrid, _scGridxDataBinder, _iasRepeatingScreenUtils, _scRepeatingPanelUtils, _scUserprefs, _iasContextUtils, _extnExpicientUtils, _scRequestUtils, dCurrency,_wscProductUtils) {
    return _dojodeclare("extn.components.batch.batchpick.scanlist.BatchLineDetailsExtn", [_extnBatchLineDetailsExtnUI], {
        // custom code here
        extn_beforeScreenInit: function(event, bEvent, ctrl, args) {
            _scWidgetUtils.destroyWidget(this, "extn_mfgNumber");
            var opModelv = _scScreenUtils.getModel(this, "BatchLine");
            if (!_scBaseUtils.isVoid(_scModelUtils.getStringValueFromPath("StoreBatchLine.ItemDetails.Item", opModelv))) {
                var itemModelObj = opModelv.StoreBatchLine.ItemDetails.Item[0];
            } else {
                var itemModelObj = opModelv.StoreBatchLine.ItemDetails;
            }
            var itemObj = _scBaseUtils.getNewBeanInstance();
            _scModelUtils.setStringValueAtModelPath("Item", itemModelObj, itemObj);
            _scScreenUtils.setModel(this, "extn_ItemDet", itemObj);
            this.applyBgColorForPickedItem();
            _scWidgetUtils.hideWidget(this, "extn_mfgNumber");
            // CSS Changes for Mobile - START
            if (_iasContextUtils.isMobileContainer()) {
                _scWidgetUtils.addClass(this, "topPanel", "marginZeroWidth");
                _scWidgetUtils.addClass(this, "extn_detailsPane", "itemDetWidthMobile");
            }
            // CSS Changes for Mobile - END
        },
        /*extn_getUnitCost: function(dataValue, screen, widget, nameSpace, itemDetailsModel, options) {
            var b;
            //alert("hi");
            var c;
            var price = null;
            var country = null;
            var priceVal = null;
            if (!_scBaseUtils.isVoid(itemDetailsModel)) {
                var currencyModel = _scModelUtils.getModelObjectFromPath("StoreBatchLine.ItemDetails.PrimaryInformation", itemDetailsModel);
                if (!_scBaseUtils.isVoid(currencyModel)) {
                    priceVal = _scModelUtils.getStringValueFromPath("UnitCost", currencyModel);
                    country = _scModelUtils.getStringValueFromPath("CostCurrency", currencyModel);
                    if (!_scBaseUtils.isVoid(priceVal)) {
                        if (!_scBaseUtils.isVoid(country)) {
                            price = dCurrency.format(priceVal, {
                                currency: country
                            });
                        }
                    } else {
                        var opModelv = _scScreenUtils.getModel(this, "BatchLine");
                        if (!_scBaseUtils.isVoid(_scModelUtils.getStringValueFromPath("StoreBatchLine.ItemDetails.Item", opModelv))) {
                            var itemModelObj = opModelv.StoreBatchLine.ItemDetails.Item[0];
                        } else {
                            var itemModelObj = opModelv.StoreBatchLine.ItemDetails;
                        }
                        var itemObj = _scBaseUtils.getNewBeanInstance();
                        _scModelUtils.setStringValueAtModelPath("Item", itemModelObj, itemObj);
                        var priceItem = null;
                        priceItem = _scModelUtils.getStringValueFromPath("Item.PrimaryInformation.UnitCost", itemObj);
                        var countryItem = null;
                        countryItem = _scModelUtils.getStringValueFromPath("Item.PrimaryInformation.CostCurrency", itemObj);
                        if (!_scBaseUtils.isVoid(priceItem)) {
                            if (!_scBaseUtils.isVoid(countryItem)) {
                                price = dCurrency.format(priceItem, {
                                    currency: countryItem
                                });
                            }
                        } else {
                            price = dCurrency.format(priceItem, {
                                currency: countryItem
                            });
                        }
                    }
                }
            }
            return price;
        },*/
        extn_chkBoxOnClick: function(event, bEvent, ctrl, args) {
            var count = 0;
            var length;
            var parentScreen = _iasUIUtils.getParentScreen(this);
            var targetModel = _scScreenUtils.getTargetModel(this, "extn_printTicket");
            var flag = targetModel.Selected;
            var ipModel = _scScreenUtils.getModel(this, "BatchLine");
            var item = _scModelUtils.getStringValueFromPath("StoreBatchLine.ItemID", ipModel);
            if (_scBaseUtils.equals(flag, "Y")) {
                _scWidgetUtils.setLabel(parentScreen, "extn_printPick", this.getSimpleBundleString('extn_PrintSelected'));
                currBatchLine = this.currentBatchLineReturn(item);
                currBatchLine.IsSelected = "Y";
            } else {
                currBatchLine = this.currentBatchLineReturn(item);
                currBatchLine.IsSelected = "N";
                var model = _scScreenUtils.getModel(parentScreen, "StoreBatchLines");
                var stringLength = _scModelUtils.getStringValueFromPath("Page.Output.StoreBatch.StoreBatchLines.TotalNumberOfRecords", model);
                var length = parseInt(stringLength);
                batchLine = _scModelUtils.getStringValueFromPath("Page.Output.StoreBatch.StoreBatchLines.StoreBatchLine", model);
                for (var index = 0; _scBaseUtils.lessThan(index, length); index = _scBaseUtils.incrementNumber(index, 1)) {
                    currBatchLine = _scBaseUtils.getArrayBeanItemByIndex(batchLine, index);
                    var isSelected = _scModelUtils.getStringValueFromPath("IsSelected", currBatchLine);
                    if (_scBaseUtils.isVoid(isSelected) || _scBaseUtils.equals(isSelected, "N")) {
                        count = count + 1;
                    }
                }
            }
            if (_scBaseUtils.equals(count, length)) {
                _scWidgetUtils.setLabel(parentScreen, "extn_printPick", this.getSimpleBundleString('extn_PrintAll'));
                _scWidgetUtils.addClass(parentScreen, "extn_printPick", "printButtonWidth");
            } else {
                _scWidgetUtils.removeClass(parentScreen, "extn_printPick", "printButtonWidth");
            }
        },
        currentBatchLineReturn: function(itemID) {
            var ps = _iasUIUtils.getParentScreen(this);
            var model = _scScreenUtils.getModel(ps, "StoreBatchLines");
            var stringLength = _scModelUtils.getStringValueFromPath("Page.Output.StoreBatch.StoreBatchLines.TotalNumberOfRecords", model);
            var length = parseInt(stringLength);
            batchLine = _scModelUtils.getStringValueFromPath("Page.Output.StoreBatch.StoreBatchLines.StoreBatchLine", model);
            for (var index = 0; _scBaseUtils.lessThan(index, length); index = _scBaseUtils.incrementNumber(index, 1)) {
                currBatchLine = _scBaseUtils.getArrayBeanItemByIndex(batchLine, index);
                var currItemID = _scModelUtils.getStringValueFromPath("ItemID", currBatchLine);
                if (_scBaseUtils.equals(currItemID, itemID)) {
                    return currBatchLine;
                }
            }
        },
        extn_getItemAlias: function(dataValue, screen, widget, nameSpace, itemDetailsModel, options) {
            var aliasValue = null;
            if (!_scBaseUtils.isVoid(itemDetailsModel)) {
                var aliasModel = _scModelUtils.getModelObjectFromPath("Item.ItemAliasList.ItemAlias", itemDetailsModel);
                if (!_scBaseUtils.isVoid(aliasModel)) {
                    for (var i = 0; i < aliasModel.length; i++) {
                        var refrenceObj = _scBaseUtils.getArrayItemByIndex(aliasModel, i);
                        var aliasName = _scModelUtils.getStringValueFromPath("AliasName", refrenceObj);
                        if (_scBaseUtils.equals(aliasName, "ACTIVE_UPC")) {
                            aliasValue = _scModelUtils.getStringValueFromPath("AliasValue", refrenceObj);
                        }
                    }
                }
            }
            return aliasValue;
        },
        extn_getColor: function(dataValue, screen, widget, nameSpace, itemDetailsModel, options) {
            var color = null;
            if (!_scBaseUtils.isVoid(itemDetailsModel)) {
                var attributeModel = _scModelUtils.getModelObjectFromPath("Item.AdditionalAttributeList.AdditionalAttribute", itemDetailsModel);
                if (!_scBaseUtils.isVoid(attributeModel)) {
                    for (var i = 0; i < attributeModel.length; i++) {
                        var refrenceObj = _scBaseUtils.getArrayItemByIndex(attributeModel, i);
                        var name = _scModelUtils.getStringValueFromPath("Name", refrenceObj);
                        if (_scBaseUtils.equals(name, "Color")) {
                            color = _scModelUtils.getStringValueFromPath("Value", refrenceObj);
                        }
                    }
                }
            }
            return color;
        },
        extn_getPriceAttribute: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
            if (!_scBaseUtils.isVoid(dataValue)) {
                var shipmentLine = null;
                shipmentLine = _scBaseUtils.getArrayItemByIndex(dataValue, 0);
                var priceAttribute = null;
                priceAttribute = _scModelUtils.getStringValueFromPath("Extn.ExtnPriceAttribute", shipmentLine);
                if (!_scBaseUtils.isVoid(priceAttribute)) {
                    return priceAttribute;
                }
            }
        },
        extn_getPrice: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
            var price = null;
            if (!_scBaseUtils.isVoid(dataValue)) {
                var shipmentLine = null;
                shipmentLine = _scBaseUtils.getArrayItemByIndex(dataValue, 0);
                var listPrice = null;
                listPrice = _scModelUtils.getStringValueFromPath("Extn.ExtnItemListPrice", shipmentLine);
                if (!_scBaseUtils.isVoid(listPrice)) {
                    var currencyModel = _scModelUtils.getStringValueFromPath("Shipment", shipmentLine);					
                    if (!_scBaseUtils.isVoid(currencyModel)) {
						currencyModel = _scBaseUtils.getArrayItemByIndex(currencyModel, 0);
                        country = _scModelUtils.getStringValueFromPath("Currency", currencyModel);
                        if (!_scBaseUtils.isVoid(country)) {
                            price = dCurrency.format(parseFloat(listPrice), {
                                currency: country
                            });
                        }
                    }
                } else {
                    price = "";
                    return "";
                }
                var allPrice = {
                    "price": price
                };
                _scScreenUtils.setModel(this, "ListPrice", allPrice);
            }
            return price;
        },
        extn_getSize: function(dataValue, screen, widget, nameSpace, itemDetailsModel, options) {
            var size = null;
            if (!_scBaseUtils.isVoid(itemDetailsModel)) {
                var attributeModel = _scModelUtils.getModelObjectFromPath("Item.AdditionalAttributeList.AdditionalAttribute", itemDetailsModel);
                if (!_scBaseUtils.isVoid(attributeModel)) {
                    for (var i = 0; i < attributeModel.length; i++) {
                        var refrenceObj = _scBaseUtils.getArrayItemByIndex(attributeModel, i);
                        var name = _scModelUtils.getStringValueFromPath("Name", refrenceObj);
                        if (_scBaseUtils.equals(name, "Size")) {
                            size = _scModelUtils.getStringValueFromPath("Value", refrenceObj);
                        }
                    }
                }
            }
            return size;
        },
        extn_afterRefreshBatchLine: function(event, bEvent, ctrl, args) {
            this.applyBgColorForPickedItem();
        },
        applyBgColorForPickedItem: function() {
            var opModelv = _scScreenUtils.getModel(this, "BatchLine");
            var bpQty = _scModelUtils.getStringValueFromPath("StoreBatchLine.BackroomPickedQuantity", opModelv);
            var totQty = _scModelUtils.getStringValueFromPath("StoreBatchLine.Quantity", opModelv);
            if (!_scBaseUtils.equals(bpQty, totQty) && !_scBaseUtils.equals("0", bpQty)) {
                //_scWidgetUtils.addClass(this, "quantityPanel", "bgcolorr");
                _scWidgetUtils.addClass(this, "extn_qtyShipPanel", "bgcolorr");
            } else {
                _scWidgetUtils.removeClass(this, "extn_qtyShipPanel", "bgcolorr");
                //_scWidgetUtils.removeClass(this, "pickPanel", "bgcolorr");
            }
        },
        extn_openItemDetails: function(event, bEvent, ctrl, args) {
              _scEventUtils.stopEvent(bEvent);
            var itemModel = _scScreenUtils.getTargetModel(this, "ItemDetails", null);
            var priceModel = _scScreenUtils.getModel(this, "ListPrice");
            if (priceModel && priceModel.price) {
                _scModelUtils.setStringValueAtModelPath("Price", priceModel.price, itemModel);
            } else {
                _scModelUtils.setStringValueAtModelPath("Price", "", itemModel);
            }
            _wscProductUtils.openItemDetails(this, itemModel);
        }
    });
});
