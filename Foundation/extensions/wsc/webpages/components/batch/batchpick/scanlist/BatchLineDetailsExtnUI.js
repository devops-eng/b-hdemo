
scDefine(["dojo/text!./templates/BatchLineDetailsExtn.html","scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/_base/lang","scbase/loader!dojo/text","scbase/loader!idx/form/CheckBox","scbase/loader!idx/layout/ContentPane","scbase/loader!sc/plat","scbase/loader!sc/plat/dojo/binding/CheckBoxDataBinder","scbase/loader!sc/plat/dojo/binding/CurrencyDataBinder","scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!sc/plat/dojo/widgets/DataLabel","scbase/loader!sc/plat/dojo/widgets/Label"]
 , function(			 
			    templateText
			 ,
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojolang
			 ,
			    _dojotext
			 ,
			    _idxCheckBox
			 ,
			    _idxContentPane
			 ,
			    _scplat
			 ,
			    _scCheckBoxDataBinder
			 ,
			    _scCurrencyDataBinder
			 ,
			    _scBaseUtils
			 ,
			    _scDataLabel
			 ,
			    _scLabel
){
return _dojodeclare("extn.components.batch.batchpick.scanlist.BatchLineDetailsExtnUI",
				[], {
			templateString: templateText
	
	
	
	
	
	
					,	
	namespaces : {
		targetBindingNamespaces :
		[
			{
	  value: 'extn_printTicket'
						,
	  scExtensibilityArrayItemId: 'extn_TargetNamespaces_0'
						,
	  description: "Stores the items that requires print"
						
			}
			
		],
		sourceBindingNamespaces :
		[
			{
	  value: 'extn_ItemDet'
						,
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_1'
						,
	  description: "This contains the item Details"
						
			}
			
		]
	}

	
	,
	hotKeys: [ 
	]

,events : [
	]

,subscribers : {

local : [

{
	  eventId: 'afterScreenInit'

,	  sequence: '19'




,handler : {
methodName : "extn_beforeScreenInit"

 
}
}
,
{
	  eventId: 'refreshBatchLine'

,	  sequence: '51'




,handler : {
methodName : "extn_afterRefreshBatchLine"

 
}
}
,
{
	  eventId: 'extn_sortAfterPick_selectChkBox_onClick'

,	  sequence: '51'




,handler : {
methodName : "extn_chkBoxOnClick"

 
}
}
,
{
	  eventId: 'itemdescriptionLink_onClick'

,	  sequence: '19'




,handler : {
methodName : "extn_openItemDetails"

 
}
}
,
{
	  eventId: 'itemImage_imageClick'

,	  sequence: '19'




,handler : {
methodName : "extn_openItemDetails"

 
}
}

]
}

});
});


