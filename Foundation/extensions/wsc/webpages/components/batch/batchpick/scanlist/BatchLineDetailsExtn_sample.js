<?xml version="1.0" encoding="UTF-8"?>
<ExtensionMetaData>
    <ScreenExtension extnClassName="BatchLineDetailsExtn"
        extnPackage="extn.components.batch.batchpick.scanlist"
        originalScreen="wsc.components.batch.batchpick.scanlist.BatchLineDetails"
        originalScreenPackage="wsc.components.batch.batchpick.scanlist" type="diffrential">
        <Items>
            <DataLabel _sc_extensibility_operation="change"
                _sc_extensibility_referenceUId="departmentLbl" renderHidden="false"/>
            <DataLabel _sc_extensibility_moveOrder="6"
                _sc_extensibility_operation="insert"
                _sc_extensibility_position="after"
                _sc_extensibility_referenceUId="extn_sortAfterPick_selectChkBox"
                class="sizein verticalAlign1"
                label="$(_scSimpleBundle:extn_DMM)" uId="extn_DeptCode">
                <BindingData>
                    <SourceBinding namespace="extn_ItemDet" path="Item.Extn.ExtnDivision"/>
                </BindingData>
            </DataLabel>
            <DataLabel _sc_extensibility_moveOrder="7"
                _sc_extensibility_operation="insert"
                _sc_extensibility_position="after"
                _sc_extensibility_referenceUId="extn_DeptCode"
                class="sizein verticalAlign1 negativeMargin"
                label="$(_scSimpleBundle:extn_Style)" uId="extn_Style">
                <BindingData>
                    <SourceBinding namespace="extn_ItemDet" path="Item.ClassificationCodes.Model"/>
                </BindingData>
            </DataLabel>
            <DataLabel _sc_extensibility_moveOrder="9"
                _sc_extensibility_operation="insert"
                _sc_extensibility_position="before"
                _sc_extensibility_referenceUId="extn_color"
                class="sizein verticalAlign1"
                label="$(_scSimpleBundle:extn_Class)" uId="extn_classDesc">
                <BindingData>
                    <SourceBinding namespace="extn_ItemDet" path="Item.Extn.ExtnClass"/>
                </BindingData>
            </DataLabel>
            <DataLabel _sc_extensibility_moveOrder="11"
                _sc_extensibility_operation="insert"
                _sc_extensibility_position="after"
                _sc_extensibility_referenceUId="extn_classDesc"
                class="sizein verticalAlign1"
                label="$(_scSimpleBundle:extn_PreSell)" uId="extn_preSell"/>
            <DataLabel _sc_extensibility_moveOrder="48"
                _sc_extensibility_operation="insert"
                _sc_extensibility_position="after"
                _sc_extensibility_referenceUId="extn_preSell"
                class="sizein verticalAlign1"
                label="$(_scSimpleBundle:extn_priceAttribute)"
                renderHidden="false" uId="extn_priceAttr" value="$(_scSimpleBundle:extn_empty)">
                <BindingData>
                    <SourceBinding
                        dynamicBindingFunc="extn_getPriceAttribute"
                        namespace="BatchLine" path="StoreBatchLine.ShipmentLines.ShipmentLine"/>
                </BindingData>
            </DataLabel>
            <DataLabel _sc_extensibility_moveOrder="12"
                _sc_extensibility_operation="insert"
                _sc_extensibility_position="after"
                _sc_extensibility_referenceUId="extn_preSell"
                class="sizein verticalAlign1"
                label="$(_scSimpleBundle:extn_Price)" uId="extn_price">
                <BindingData>
                    <SourceBinding dynamicBindingFunc="extn_getUnitCost"
                        namespace="BatchLine" path="ItemDetails.PrimaryInformation.UnitCost"/>
                </BindingData>
            </DataLabel>
            <DataLabel _sc_extensibility_moveOrder="13"
                _sc_extensibility_operation="insert"
                _sc_extensibility_position="before"
                _sc_extensibility_referenceUId="extn_size"
                class="sizein verticalAlign1"
                label="$(_scSimpleBundle:extn_DeptNo)" uId="extn_deptNumber">
                <BindingData>
                    <SourceBinding namespace="extn_ItemDet" path="Item.Extn.ExtnDepartment"/>
                </BindingData>
            </DataLabel>
            <DataLabel _sc_extensibility_moveOrder="14"
                _sc_extensibility_operation="insert"
                _sc_extensibility_position="after"
                _sc_extensibility_referenceUId="extn_deptNumber"
                class="sizein verticalAlign1"
                label="$(_scSimpleBundle:extn_Label)" uId="extn_label">
                <BindingData>
                    <SourceBinding namespace="extn_ItemDet" path="Item.PrimaryInformation.ManufacturerName"/>
                </BindingData>
            </DataLabel>
            <DataLabel _sc_extensibility_moveOrder="16"
                _sc_extensibility_operation="insert"
                _sc_extensibility_position="after"
                _sc_extensibility_referenceUId="extn_label"
                class="sizein verticalAlign1"
                label="$(_scSimpleBundle:extn_SKN)" uId="extn_sknNumber" value="$(_scSimpleBundle:skn_type)">
                <BindingData>
                    <SourceBinding namespace="BatchLine" path="StoreBatchLine.ItemID"/>
                </BindingData>
            </DataLabel>
            <DataLabel _sc_extensibility_moveOrder="15"
                _sc_extensibility_operation="insert"
                _sc_extensibility_position="before"
                _sc_extensibility_referenceUId="extn_size"
                class="sizein verticalAlign1"
                label="$(_scSimpleBundle:extn_UPC)" uId="extn_upcNumber">
                <BindingData>
                    <SourceBinding
                        dynamicBindingFunc="extn_getItemAlias"
                        namespace="extn_ItemDet" path="Item.ItemAliasList.ItemAlias"/>
                </BindingData>
            </DataLabel>
            <DataLabel _sc_extensibility_moveOrder="17"
                _sc_extensibility_operation="insert"
                _sc_extensibility_position="before"
                _sc_extensibility_referenceUId="extn_size"
                class="sizein verticalAlign1"
                label="$(_scSimpleBundle:extn_MFG)" renderHidden="true" uId="extn_mfgNumber">
                <BindingData>
                    <SourceBinding namespace="extn_ItemDet" path="Item.PrimaryInformation.ManufacturerItem"/>
                </BindingData>
            </DataLabel>
            <DataLabel _sc_extensibility_moveOrder="10"
                _sc_extensibility_operation="insert"
                _sc_extensibility_position="after"
                _sc_extensibility_referenceUId="extn_classDesc"
                class="sizein verticalAlign1"
                label="$(_scSimpleBundle:extn_Size)" uId="extn_size">
                <BindingData>
                    <SourceBinding dynamicBindingFunc="extn_getSize"
                        namespace="extn_ItemDet" path="Item"/>
                </BindingData>
            </DataLabel>
            <DataLabel _sc_extensibility_moveOrder="8"
                _sc_extensibility_operation="insert"
                _sc_extensibility_position="after"
                _sc_extensibility_referenceUId="extn_Style"
                class="sizein verticalAlign1"
                label="$(_scSimpleBundle:extn_Color)" uId="extn_color">
                <BindingData>
                    <SourceBinding dynamicBindingFunc="extn_getColor"
                        namespace="extn_ItemDet" path="Item"/>
                </BindingData>
            </DataLabel>
            <CheckBox _sc_extensibility_moveOrder="1"
                _sc_extensibility_operation="insert"
                _sc_extensibility_position="after"
                _sc_extensibility_referenceUId="displaySAPStgLocPanelActionsLink"
                class="chkBoxLeft" scExcludeForDirtyCheck="true" uId="extn_sortAfterPick_selectChkBox">
                <BindingData>
                    <CheckedValue value="Y"/>
                    <TargetBindings>
                        <TargetBinding namespace="extn_printTicket"
                            path="Selected" scExtensibilityArrayItemId="extn_TargetBindings_0"/>
                    </TargetBindings>
                    <UncheckedValue value="N"/>
                </BindingData>
            </CheckBox>
            <Label _sc_extensibility_operation="change"
                _sc_extensibility_referenceUId="totalPickQtyLabel" class="autoLabelZeroWidth pickQuantity idxLabel pipeBorder zeroPaddingLeft"/>
            <Label _sc_extensibility_operation="change"
                _sc_extensibility_referenceUId="pickCompleteLabel" class="inline idxLabel zeroWidthLabelKey completeLabel zeroPaddingLeft"/>
            <Label _sc_extensibility_operation="change"
                _sc_extensibility_referenceUId="ofQtyLbl" class="autoLabelZeroWidth pickQuantity idxLabel zeroPaddingLeft"/>
            <Label _sc_extensibility_operation="change"
                _sc_extensibility_referenceUId="shortageLbl" class="autoLabelZeroWidth shortQuantity idxLabel zeroPaddingLeft"/>
            <Label _sc_extensibility_operation="change"
                _sc_extensibility_referenceUId="totalShipQty" class="inline idxLabel zeroWidthLabelKey"/>
        </Items>
        <Namespaces>
            <SourceNamespaces>
                <Source description="This contains the item Details"
                    isExtn="true"
                    scExtensibilityArrayItemId="extn_SourceNamespaces_1" value="extn_ItemDet"/>
            </SourceNamespaces>
            <TargetNamespaces>
                <Target
                    description="Stores the items that requires print"
                    isExtn="true"
                    scExtensibilityArrayItemId="extn_TargetNamespaces_0" value="extn_printTicket"/>
            </TargetNamespaces>
        </Namespaces>
        <Subscribers>
            <Local>
                <Subscriber eventId="afterScreenInit" isExtn="true" sequence="19">
                    <Handler methodName="extn_beforeScreenInit"/>
                </Subscriber>
                <Subscriber eventId="refreshBatchLine" isExtn="true" sequence="51">
                    <Handler methodName="extn_afterRefreshBatchLine"/>
                </Subscriber>
                <Subscriber
                    eventId="extn_sortAfterPick_selectChkBox_onClick"
                    isExtn="true" sequence="51">
                    <Handler methodName="extn_chkBoxOnClick"/>
                </Subscriber>
            </Local>
        </Subscribers>
    </ScreenExtension>
</ExtensionMetaData>
