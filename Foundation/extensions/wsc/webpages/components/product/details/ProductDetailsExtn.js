scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/components/product/details/ProductDetailsExtnUI", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/widgets/Label", "scbase/loader!sc/plat/dojo/widgets/Link", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/GridxUtils", "scbase/loader!gridx/Grid", "scbase/loader!sc/plat/dojo/binding/GridxDataBinder", "scbase/loader!ias/utils/RepeatingScreenUtils", "scbase/loader!sc/plat/dojo/utils/RepeatingPanelUtils", "scbase/loader!ias/utils/ScreenUtils", "scbase/loader!ias/widgets/IASBaseScreen", "scbase/loader!ias/utils/EventUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!ias/utils/ContextUtils","scbase/loader!dojo/currency"], function(_dojodeclare, _extnProductDetailsExtnUI, _idxContentPane, _scBaseUtils, _scEventUtils, _scLabel, _scLink, _scScreen, _scModelUtils, _iasUIUtils, _scScreenUtils, _iasBaseTemplateUtils, _scWidgetUtils, _scEventUtils, _scGridxUtils, _gridxGrid, _scGridxDataBinder, _iasRepeatingScreenUtils, _scRepeatingPanelUtils, _iasScreenUtils, _iasIASBaseScreen, _iasEventUtils, _extnExpicientUtils, _iasContextUtils,dCurrency) {
    return _dojodeclare("extn.components.product.details.ProductDetailsExtn", [_extnProductDetailsExtnUI], {
        // custom code here
        // Overriden for Image URL
        getItemImageUrl: function(screen, widget, nameSpace, itemImageModel, options, dataValue) {
            var displayImage = "";
            displayImage = _iasContextUtils.getDisplayItemImageRule();
            if (_scBaseUtils.equals(displayImage, "N")) {
                var retVal = "";
                retVal = _iasUIUtils.fetchBlankImage(this);
                _scWidgetUtils.hideWidget(this, "itemImgMoreDtls", false);
                return retVal;
            }
            var imageUrl = "";
            imageUrl = _scBaseUtils.getAttributeValue("ContentLocation", false, itemImageModel);
            imageUrl = _scModelUtils.getStringValueFromPath("Item.PrimaryInformation.ImageLocation", itemImageModel);
            return imageUrl;
        },
        // Overriden for Image ID
        getImageIcon: function(screen, widget, nameSpace, itemImageModel, options, dataValue) {
            var displayImage = "";
            displayImage = _iasContextUtils.getDisplayItemImageRule();
            if (_scBaseUtils.equals(displayImage, "N")) {
                var retValue = "";
                return retValue;
            }
            var imageIcon = "";
            imageIcon = _scBaseUtils.getAttributeValue("ContentID", false, itemImageModel);
            imageIcon = _scModelUtils.getStringValueFromPath("Item.PrimaryInformation.ImageID", itemImageModel);
            return imageIcon;
        },
        extn_getItemAlias: function(dataValue, screen, widget, nameSpace, itemDetailsModel, options) {
            var aliasValue = null;
            if (!_scBaseUtils.isVoid(itemDetailsModel)) {
                var aliasModel = _scModelUtils.getModelObjectFromPath("Item.ItemAliasList.ItemAlias", itemDetailsModel);
                if (!_scBaseUtils.isVoid(aliasModel)) {
                    for (var i = 0; i < aliasModel.length; i++) {
                        var refrenceObj = _scBaseUtils.getArrayItemByIndex(aliasModel, i);
                        var aliasName = _scModelUtils.getStringValueFromPath("AliasName", refrenceObj);
                        if (_scBaseUtils.equals(aliasName, "ACTIVE_UPC")) {
                            aliasValue = _scModelUtils.getStringValueFromPath("AliasValue", refrenceObj);
                        }
                    }
                }
            }
            return aliasValue;
        },
        extn_getColor: function(dataValue, screen, widget, nameSpace, itemDetailsModel, options) {
            var color = null;
            if (!_scBaseUtils.isVoid(itemDetailsModel)) {
                var attributeModel = _scModelUtils.getModelObjectFromPath("Item.AdditionalAttributeList.AdditionalAttribute", itemDetailsModel);
                if (!_scBaseUtils.isVoid(attributeModel)) {
                    for (var i = 0; i < attributeModel.length; i++) {
                        var refrenceObj = _scBaseUtils.getArrayItemByIndex(attributeModel, i);
                        var name = _scModelUtils.getStringValueFromPath("Name", refrenceObj);
                        if (_scBaseUtils.equals(name, "Color")) {
                            color = _scModelUtils.getStringValueFromPath("Value", refrenceObj);
                        }
                    }
                }
            }
            return color;
        },
        extn_getSize: function(dataValue, screen, widget, nameSpace, itemDetailsModel, options) {
            var size = null;
            if (!_scBaseUtils.isVoid(itemDetailsModel)) {
                var attributeModel = _scModelUtils.getModelObjectFromPath("Item.AdditionalAttributeList.AdditionalAttribute", itemDetailsModel);
                if (!_scBaseUtils.isVoid(attributeModel)) {
                    for (var i = 0; i < attributeModel.length; i++) {
                        var refrenceObj = _scBaseUtils.getArrayItemByIndex(attributeModel, i);
                        var name = _scModelUtils.getStringValueFromPath("Name", refrenceObj);
                        if (_scBaseUtils.equals(name, "Size")) {
                            size = _scModelUtils.getStringValueFromPath("Value", refrenceObj);
                        }
                    }
                }
            }
            return size;
        },
        extn_getQty: function(dataValue, screen, widget, nameSpace, itemDetailsModel, options) {
            var itemID = null;
            var unitOfMeasure = null;
            var itemModel = null;
            if (!_scBaseUtils.isVoid(itemDetailsModel)) {
                itemModel = _scModelUtils.getModelObjectFromPath("Item", itemDetailsModel);
                if (!_scBaseUtils.isVoid(itemModel)) {
                    itemID = _scModelUtils.getStringValueFromPath("ItemID", itemModel);
                    unitOfMeasure = _scModelUtils.getStringValueFromPath("UnitOfMeasure", itemModel);
                    productClass = _scModelUtils.getStringValueFromPath("ProductClass", itemModel);
                }
                var shipNode = null;
                shipNode = _iasContextUtils.getFromContext("CurrentStore");
                var organization = null;
                organization = _scModelUtils.getStringValueFromPath("Item.OrganizationCode", itemDetailsModel);
                var model = null;
                model = _scBaseUtils.getNewModelInstance();
                _scModelUtils.setStringValueAtModelPath("InventorySupply.OrganizationCode", organization, model);
                _scModelUtils.setStringValueAtModelPath("InventorySupply.ShipNode", shipNode, model);
                _scModelUtils.setStringValueAtModelPath("InventorySupply.ItemID", itemID, model);
                _scModelUtils.setStringValueAtModelPath("InventorySupply.UnitOfMeasure", unitOfMeasure, model);
                _scModelUtils.setStringValueAtModelPath("InventorySupply.ProductClass", productClass, model);
                _scModelUtils.setStringValueAtModelPath("InventorySupply.SupplyType", "ONHAND", model);
                _iasUIUtils.callApi(this, model, "extn_getQtyOnHand_ref", null);
            }
        },
        extn_afterBehaviorMashupCall: function(event, bEvent, ctrl, args) {
            var isMashupAvailable = _extnExpicientUtils.isMashupAvailable("extn_getQtyOnHand_ref", args);
            if (isMashupAvailable) {
                var getItemListModelOutput = null;
                getItemListModelOutput = _extnExpicientUtils.getMashupOutputForRefID("extn_getQtyOnHand_ref", args);
                if (!_scBaseUtils.isVoid(getItemListModelOutput)) {
                    var itemModel = null;
                    itemModel = _scModelUtils.getModelObjectFromPath("Item.Supplies.InventorySupply", getItemListModelOutput);
                    var itemID = null;
                    if (!_scBaseUtils.isVoid(itemModel)) {
                        var availableQuantity = null;
                        availableQuantity = _scModelUtils.getStringValueFromPath("Quantity", itemModel);
                        if (!_scBaseUtils.isVoid(availableQuantity)) {
                            _scWidgetUtils.setValue(this, "extn_qtyOnHand", parseInt(availableQuantity));
                        }
                    }
                }
            }
        },
        extn_getPrice: function(dataValue, screen, widget, nameSpace, itemDetailsModel, options) {
            var inputModel = _scScreenUtils.getModel(this, "apiInput");
            
            if (inputModel.Price) {
                 return inputModel.Price;
            }
            else{
                return "";
            }
           
        }
    });
});
