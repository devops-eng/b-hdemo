scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/components/shipment/backroompick/BackroomPickProductScanExtnUI", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/widgets/Label", "scbase/loader!sc/plat/dojo/widgets/Link", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/GridxUtils", "scbase/loader!gridx/Grid", "scbase/loader!sc/plat/dojo/binding/GridxDataBinder", "scbase/loader!ias/utils/RepeatingScreenUtils", "scbase/loader!sc/plat/dojo/utils/RepeatingPanelUtils", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!sc/plat/dojo/utils/RequestUtils", "scbase/loader!sc/plat/dojo/utils/PlatformUIFmkImplUtils", "scbase/loader!ias/utils/WizardUtils"], function(_dojodeclare, _extnBackroomPickProductScanExtnUI, _scWidgetUtils, _scBaseUtils, _scEventUtils, _scLabel, _scLink, _scScreen, _scModelUtils, _iasUIUtils, _scScreenUtils, _iasBaseTemplateUtils, _scWidgetUtils, _scEventUtils, _scGridxUtils, _gridxGrid, _scGridxDataBinder, _iasRepeatingScreenUtils, _scRepeatingPanelUtils, _scUserprefs, _iasContextUtils, _extnExpicientUtils, _scRequestUtils, _scPlatformUIFmkImplUtils, _iasWizardUtils) {
    return _dojodeclare("extn.components.shipment.backroompick.BackroomPickProductScanExtn", [_extnBackroomPickProductScanExtnUI], {
        // custom code here
        extn_afterBehaviourMashupCall: function(event, bEvent, ctrl, args) {
            if (_extnExpicientUtils.isMashupAvailable("extn_printPDFOnClick_ref", args)) {
                outputModel = _extnExpicientUtils.getMashupOutputForRefID("extn_printPDFOnClick_ref", args);
                this.openPDF(outputModel);
            }
            if (_extnExpicientUtils.isMashupAvailable("extn_shipmentDetailsForJewelry", args)) {
                var outModel = _extnExpicientUtils.getMashupOutputForRefID("extn_shipmentDetailsForJewelry", args);
                var shipmentLinesList = outModel.ShipmentLines.ShipmentLine;
                var length = _scBaseUtils.getAttributeCount(shipmentLinesList);
                var diamondFlag = false;
                for (var index = 0; _scBaseUtils.lessThan(index, length); index = _scBaseUtils.incrementNumber(index, 1)) {
                    var referenceModel1 = null;
                    referenceModel1 = _scBaseUtils.getArrayItemByIndex(shipmentLinesList, index);
                    if (referenceModel1.OrderLine && referenceModel1.OrderLine.ItemDetails && referenceModel1.OrderLine.ItemDetails.Extn && referenceModel1.OrderLine.ItemDetails.Extn.ExtnJewelryFlag == "Y") {
                        diamondFlag = true;
                        break;
                    }
                    //  _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLine.ShipmentLineKey", shipmentLineKey, newInputModel);
                }
                if (diamondFlag == true) {
                    _scWidgetUtils.showWidget(this, "extn_jelwellaryItem", false, null);
                } else {
                    _scWidgetUtils.hideWidget(this, "extn_jelwellaryItem");
                }
            }
            if (_extnExpicientUtils.isMashupAvailable("updateShipmentQuantityForPickAllLine", args)) {
                mashupRefId = "updateShipmentQuantityForPickAllLine";
                var outModel = _extnExpicientUtils.getMashupOutputForRefID(mashupRefId, args);
                var inputModel = _extnExpicientUtils.getInputMashupForRefID(mashupRefId, args);
                if (_scBaseUtils.equals(inputModel.Shipment.Action, "PickLine")) {
                    this.callForJwelleryItemFlag(outModel);
                }
                //      if (outModel.ShipmentLine.Extn.ExtnJewellryItemFlag!= undefined && outModel.ShipmentLine.Extn.ExtnJewellryItemFlag == "Y") {
                //     this.checkBtnClicked(mashupRefId, args);
                // }
            }
            if (_extnExpicientUtils.isMashupAvailable("translateBarCode", args)) {
                mashupRefId = "translateBarCode";
                var outModel = _extnExpicientUtils.getMashupOutputForRefID(mashupRefId, args);
                if (outModel.BarCode.Shipment.ShipmentLine.Extn.ExtnJewellryItemFlag != undefined && outModel.BarCode.Shipment.ShipmentLine.Extn.ExtnJewellryItemFlag == "Y") {
                    this.checkBtnClicked(mashupRefId, args);
                }
            }
            if (_extnExpicientUtils.isMashupAvailable("extn_getShipmentLineList", args)) {
                mashupRefId = "extn_getShipmentLineList";
                var outModel = _extnExpicientUtils.getMashupOutputForRefID(mashupRefId, args);
                var shipmentLinesList = outModel.ShipmentLines.ShipmentLine;
                var length = _scBaseUtils.getAttributeCount(shipmentLinesList);
                for (var index = 0; _scBaseUtils.lessThan(index, length); index = _scBaseUtils.incrementNumber(index, 1)) {
                    var shipmentLine = _scBaseUtils.getArrayBeanItemByIndex(shipmentLinesList, index);
                    var ExtnJewellryItemFlag = _scModelUtils.getStringValueFromPath("Extn.ExtnJewellryItemFlag", shipmentLine);
                    //  _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLine.ShipmentLineKey", shipmentLineKey, newInputModel);
                }
                if (ExtnJewellryItemFlag != undefined && ExtnJewellryItemFlag == "Y") {
                    this.checkBtnClicked(mashupRefId, args);
                }
            }
        },
        callForJwelleryItemFlag: function(outputModel) {
            var shipmentLineKey = outputModel.ShipmentLine.ShipmentLineKey;
            var newInputModel = _scBaseUtils.getNewModelInstance();
            _scModelUtils.setStringValueAtModelPath("ShipmentLine.ShipmentLineKey", shipmentLineKey, newInputModel);
            _iasUIUtils.callApi(this, newInputModel, "extn_getShipmentLineList", null);
        },
        checkBtnClicked: function(mashupRefId, args) {
            var minusBtnClicked = _scUserprefs.getProperty("MinusBtnClicked");
            if (_scBaseUtils.equals(minusBtnClicked, "Y")) {
                var outModel = _extnExpicientUtils.getMashupOutputForRefID(mashupRefId, args);
                var inputModel = _extnExpicientUtils.getInputMashupForRefID(mashupRefId, args);
                var newInputModel = _scBaseUtils.getNewModelInstance();
                var shipmentLinesList = outModel.ShipmentLines.ShipmentLine;
                var length = _scBaseUtils.getAttributeCount(shipmentLinesList);
                for (var index = 0; _scBaseUtils.lessThan(index, length); index = _scBaseUtils.incrementNumber(index, 1)) {
                    var shipmentLine = _scBaseUtils.getArrayBeanItemByIndex(shipmentLinesList, index);
                    var shipmentKey = _scModelUtils.getStringValueFromPath("ShipmentKey", shipmentLine);
                    var shipmentLineKey = _scModelUtils.getStringValueFromPath("ShipmentLineKey", shipmentLine);
                }
                _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", shipmentKey, newInputModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLine.ShipmentLineKey", shipmentLineKey, newInputModel);
                _scUserprefs.setProperty("MinusBtnClicked", "N");
                _iasUIUtils.callApi(this, newInputModel, "extn_removeCaseNumberService", null);
            } else {
                this.openCaseNoPopUp(mashupRefId, args);
            }
        },
        openCaseNoPopUp: function(mashupRefId, args) {
            var bindings = _scBaseUtils.getNewBeanInstance();
            var popupParams = _scBaseUtils.getNewModelInstance();
            var dialogParams = _scBaseUtils.getNewModelInstance();
            var newInputModel = _scBaseUtils.getNewModelInstance();
            var outModel = _extnExpicientUtils.getMashupOutputForRefID(mashupRefId, args);
            var inputModel = _extnExpicientUtils.getInputMashupForRefID(mashupRefId, args);
            var userId = _scUserprefs.getUserId();
            var currentStore = _iasContextUtils.getFromContext("CurrentStore");
            if (_scBaseUtils.equals(mashupRefId, "extn_getShipmentLineList")) {
                var shipmentLinesList = outModel.ShipmentLines.ShipmentLine;
                var length = _scBaseUtils.getAttributeCount(shipmentLinesList);
                for (var index = 0; _scBaseUtils.lessThan(index, length); index = _scBaseUtils.incrementNumber(index, 1)) {
                    var shipmentLine = _scBaseUtils.getArrayBeanItemByIndex(shipmentLinesList, index);
                    var shipmentKey = _scModelUtils.getStringValueFromPath("ShipmentKey", shipmentLine);
                    var shipmentLineKey = _scModelUtils.getStringValueFromPath("ShipmentLineKey", shipmentLine);
                    var BackroomPickedQuantity = _scModelUtils.getStringValueFromPath("BackroomPickedQuantity", shipmentLine);
                }
                _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLines.ShipmentLine.ShipmentLineKey", shipmentLineKey, newInputModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLines.ShipmentLine.BackroomPickedQuantity", BackroomPickedQuantity, newInputModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", shipmentKey, newInputModel);
                _scModelUtils.setStringValueAtModelPath("IsScanMode", "N", outModel);
                //                 var shipmentLinesList = ;
                //                 var shipmentKey = _scModelUtils.getStringValueFromPath("Shipment.ShipmentKey", inputModel);
                //                 _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", shipmentKey, newInputModel);
                //                 var length = _scBaseUtils.getAttributeCount(shipmentLinesList);
                //                 for (var index = 0; _scBaseUtils.lessThan(index, length); index = _scBaseUtils.incrementNumber(index, 1)) {
                //                     var shipmentLine = _scBaseUtils.getArrayBeanItemByIndex(shipmentLinesList, index);
                //                     var shipmentLineKey = _scModelUtils.getStringValueFromPath("ShipmentLine.ShipmentLineKey", shipmentLine);
                //                     var BackroomPickedQuantity = _scModelUtils.getStringValueFromPath("ShipmentLine.BackroomPickedQuantity", shipmentLine);
                //                 }
            } else {
                var shipmentKey = _scModelUtils.getStringValueFromPath("BarCode.Shipment.ShipmentLine.ShipmentKey", outModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", shipmentKey, newInputModel);
                var shipmentLineKey = _scModelUtils.getStringValueFromPath("BarCode.Shipment.ShipmentLine.ShipmentLineKey", outModel);
                var BackroomPickedQuantity = _scModelUtils.getStringValueFromPath("BarCode.Shipment.ShipmentLine.BackroomPickedQuantity", outModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLines.ShipmentLine.ShipmentLineKey", shipmentLineKey, newInputModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLines.ShipmentLine.BackroomPickedQuantity", BackroomPickedQuantity, newInputModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLines.ShipmentLine.UPCCode", outModel.BarCode.BarCodeData, newInputModel);
                _scModelUtils.setStringValueAtModelPath("IsScanMode", "Y", outModel);
            }
            _scModelUtils.setStringValueAtModelPath("Shipment.UserID", userId, newInputModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.StoreNo", currentStore, newInputModel);
            _scModelUtils.setStringValueAtModelPath("IsBatchPick", "N", newInputModel);
            _scModelUtils.setStringValueAtModelPath("IsBatchPick", "N", outModel);
            _scBaseUtils.addModelValueToBean("InputOrderData", newInputModel, bindings);
            _scBaseUtils.addModelValueToBean("CompleteOrderData", outModel, bindings);
            _scBaseUtils.addBeanValueToBean("binding", bindings, popupParams);
            _scBaseUtils.setAttributeValue("closeCallBackHandler", "caseNoCallBackHandler", dialogParams);
            _scBaseUtils.addStringValueToBean("outputNamespace", "caseNo_getCompleteOrderDetails_output", popupParams);
            _iasUIUtils.openSimplePopup("extn.customScreen.CaseNoScan", "", this, popupParams, dialogParams);
        },
        caseNoCallBackHandler: function(actionPerformed, model, popupParams) {
            //  var flag=model.Order.QuoteSentSuccessfully;
            if (!(_scBaseUtils.isVoid(model))) {
                if (model.RefreshScreen != undefined && model.RefreshScreen == "Y") {
                    this.refreshShipmentLineAfterQuantityUpdate(model);
                }
            }
        },
        extn_beforeBehaviorMashupCall: function(event, bEvent, ctrl, args) {
            if (_extnExpicientUtils.isBeforeBehaviorMashupAvailable("updateShipmentQuantityForPickAllLine", args) || _extnExpicientUtils.isBeforeBehaviorMashupAvailable("updateShipmentQuantityForPickAll", args)) {
                if (_extnExpicientUtils.isBeforeBehaviorMashupAvailable("updateShipmentQuantityForPickAllLine", args) || _extnExpicientUtils.isBeforeBehaviorMashupAvailable("updateShipmentQuantityForPickAll", args)) {
                    var inpModel = _extnExpicientUtils.getMashupInputForRefID("updateShipmentQuantityForPickAllLine", args);
                    if (_scBaseUtils.isVoid(inpModel)) {
                        var inpModel = _extnExpicientUtils.getMashupInputForRefID("updateShipmentQuantityForPickAll", args);
                    }
                    var action = _scModelUtils.getStringValueFromPath("Shipment.Action", inpModel);
                    if (_scBaseUtils.equals(action, "MarkAllLinesShortage") || _scBaseUtils.equals(action, "MarkLineAsShortage")) {
                        var newModel = null;
                        newModel = _scScreenUtils.getModel(this, "backroomPickShipmentDetails_output");
                        var shipmentShipNode = null;
                        var shipmentOrganizationCode = null;
                        shipmentShipNode = _scModelUtils.getStringValueFromPath("Shipment.ShipNode", newModel);
                        shipmentOrganizationCode = _scModelUtils.getStringValueFromPath("Shipment.EnterpriseCode", newModel);
                        var org = _scUserprefs.getUserOrganization();
                        var orgCode = null;
                        var inventoryOrgCode = null;
                        var parentOrg = null;
                        if (_scBaseUtils.equals(_scModelUtils.getStringValueFromPath("Organization.OrganizationCode", org), "DEFAULT")) {
                            orgCode = shipmentShipNode;
                            inventoryOrgCode = shipmentOrganizationCode;
                            parentOrg = shipmentOrganizationCode;
                        } else {
                            orgCode = _scModelUtils.getStringValueFromPath("Organization.OrganizationCode", org);
                            inventoryOrgCode = _scModelUtils.getStringValueFromPath("Organization.InventoryOrganizationCode", org);
                            parentOrg = _scModelUtils.getStringValueFromPath("Organization.ParentOrganizationCode", org);
                        }
                        _scModelUtils.setStringValueAtModelPath("Shipment.InventoryOrganizationCode", inventoryOrgCode, inpModel);
                        _scModelUtils.setStringValueAtModelPath("Shipment.OrganizationCode", shipmentShipNode, inpModel);
                        _scModelUtils.setStringValueAtModelPath("Shipment.ParentOrganizationCode", parentOrg, inpModel);
                        shipmentModel = _scScreenUtils.getModel(this, "getShortageLines");
                        if (_scBaseUtils.isVoid(shipmentModel)) {
                            shipmentModel = _scScreenUtils.getModel(this, "getReadyForPickLines");
                        }
                        shipmentLineKey = _scModelUtils.getStringValueFromPath("Shipment.ShipmentLines.ShipmentLine.0.ShipmentLineKey", inpModel);
                        shipmentLineModel = _scModelUtils.getStringValueFromPath("Page.Output.ShipmentLines.ShipmentLine", shipmentModel);
                        var length = _scBaseUtils.getAttributeCount(shipmentLineModel);
                        for (var index = 0; _scBaseUtils.lessThan(index, length); index = _scBaseUtils.incrementNumber(index, 1)) {
                            currShipmentLine = _scBaseUtils.getArrayBeanItemByIndex(shipmentLineModel, index);
                            currShipmentLineKey = _scModelUtils.getStringValueFromPath("ShipmentLineKey", currShipmentLine);
                            if (_scBaseUtils.equals(currShipmentLineKey, shipmentLineKey)) {
                                _scModelUtils.setStringValueAtModelPath("Shipment.ItemID", _scModelUtils.getStringValueFromPath("ItemID", currShipmentLine), inpModel);
                                _scModelUtils.setStringValueAtModelPath("Shipment.UnitOfMeasure", _scModelUtils.getStringValueFromPath("UnitOfMeasure", currShipmentLine), inpModel);
                                _scModelUtils.setStringValueAtModelPath("Shipment.ProductClass", _scModelUtils.getStringValueFromPath("ProductClass", currShipmentLine), inpModel);
                                _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLines.ShipmentLine.0.OrderHeaderKey", _scModelUtils.getStringValueFromPath("OrderHeaderKey", currShipmentLine), inpModel);
                                _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLines.ShipmentLine.0.OrderLineKey", _scModelUtils.getStringValueFromPath("OrderLineKey", currShipmentLine), inpModel);
                                break;
                            }
                        }
                        pickedShipmentLineModel = currShipmentLine;
                        if (!_scBaseUtils.isVoid(pickedShipmentLineModel)) {
                            var pickedLine = {};
                            var pickedQty = !_scBaseUtils.isVoid(pickedShipmentLineModel.BackroomPickedQuantity) ? Number(pickedShipmentLineModel.BackroomPickedQuantity) : Number('0');
                            var shortageQty = !_scBaseUtils.isVoid(pickedShipmentLineModel.ShortageQty) ? Number(pickedShipmentLineModel.ShortageQty) : Number('0');
                            pickedLine.BackroomPickedQuantity = pickedQty;
                            if (!_scBaseUtils.isVoid(args) && this.isLineMarkedWithInventoryShortage(inpModel)) {
                                pickedLine.Quantity = pickedQty;
                            } else {
                                pickedLine.Quantity = pickedShipmentLineModel.Quantity;
                            }
                            pickedLine.ShortageQty = (Number(pickedShipmentLineModel.Quantity) - pickedQty) + shortageQty;
                            inpModel.Shipment.ShipmentLines.ShipmentLine[0].Quantity = pickedLine.Quantity.toString();
                            inpModel.Shipment.ShipmentLines.ShipmentLine[0].ShortageQty = pickedLine.ShortageQty.toString();
                        }
                    }
                    _scModelUtils.setStringValueAtModelPath("Shipment.DeliveryMethod", _scModelUtils.getStringValueFromPath("Shipment.DeliveryMethod", newModel), inpModel);
                }
            }
        },
        isLineMarkedWithInventoryShortage: function(shortedShipmentModel) {
            if (!_scBaseUtils.isVoid(shortedShipmentModel) && !_scBaseUtils.isVoid(shortedShipmentModel.Shipment) && _scBaseUtils.equals("CA4", shortedShipmentModel.Shipment.ShortageReasonCode)) {
                return true;
            }
            if (!_scBaseUtils.isVoid(shortedShipmentModel) && !_scBaseUtils.isVoid(shortedShipmentModel.Shipment) && _scBaseUtils.equals("CA1", shortedShipmentModel.Shipment.ShortageReasonCode)) {
                return true;
            }
            if (!_scBaseUtils.isVoid(shortedShipmentModel) && !_scBaseUtils.isVoid(shortedShipmentModel.Shipment) && _scBaseUtils.equals("SH2", shortedShipmentModel.Shipment.ShortageReasonCode)) {
                return true;
            }
            if (!_scBaseUtils.isVoid(shortedShipmentModel) && !_scBaseUtils.isVoid(shortedShipmentModel.Shipment) && _scBaseUtils.equals("CA2", shortedShipmentModel.Shipment.ShortageReasonCode)) {
                return true;
            }
            if (!_scBaseUtils.isVoid(shortedShipmentModel) && !_scBaseUtils.isVoid(shortedShipmentModel.Shipment) && _scBaseUtils.equals("CA3", shortedShipmentModel.Shipment.ShortageReasonCode)) {
                return true;
            }
            if (!_scBaseUtils.isVoid(shortedShipmentModel) && !_scBaseUtils.isVoid(shortedShipmentModel.Shipment) && _scBaseUtils.equals("SH1", shortedShipmentModel.Shipment.ShortageReasonCode)) {
                return true;
            }
            return false;
        },
        extn_afterScreenLoad: function(event, bEvent, ctrl, args) {
            var wizardScreen = _iasUIUtils.getParentScreen(this, true);
            _scWidgetUtils.hideWidget(this, "extn_printTciket", false);
            _scWidgetUtils.addClass(this, "shipmentDetails", "widthContentPane");
            _iasWizardUtils.setLabelOnNavigationalWidget(wizardScreen, "nextBttn", "Finish Pick");
            var newModel = null;
            newModel = _scScreenUtils.getModel(this, "backroomPickShipmentDetails_output");
            _scWidgetUtils.hideWidget(this, "dueInTimeTextLabel");
            var imgName = _scModelUtils.getStringValueFromPath("Shipment.ImageUrl", newModel);
            if (newModel.Shipment.DeliveryMethod == "PICK") {
                if (_scBaseUtils.contains(imgName, "Overdue")) {
                    _scWidgetUtils.setValue(this, "extn_slaIndicator", "Missed SLA");
                } else if (_scBaseUtils.contains(imgName, "MediumPriority")) {
                    _scWidgetUtils.setValue(this, "extn_slaIndicator", "Nearing SLA");
                } else if (_scBaseUtils.contains(imgName, "LowPriority")) {
                    _scWidgetUtils.setValue(this, "extn_slaIndicator", "Within SLA");
                }
            }
            //Add css for Mobile - START
             if(_iasContextUtils.isMobileContainer())
             {
                _scWidgetUtils.addClass(this, "shipmentDetails", "showDueInforMobile");
                _scWidgetUtils.addClass(this, "img_TimeRmnClock", "hideSLAindicatorMobile");
                _scWidgetUtils.addClass(this, "extn_expectedDateContainer", "floatPropertyDue1");
                _scWidgetUtils.removeClass(this, "extn_expectedDateContainer", "floatPropertyDue");
             }

            //Add css for Mobile -  END
            var shipmodel = _scModelUtils.getStringValueFromPath("Shipment.ShipmentLines.ShipmentLine", newModel);
            var shipLineKey = _scModelUtils.getStringValueFromPath("ShipmentLineKey", shipmodel[0]);
            var newInputModel = _scBaseUtils.getNewModelInstance();
            _scModelUtils.setStringValueAtModelPath("ShipmentLine.ShipmentLineKey", shipLineKey, newInputModel);
            _iasUIUtils.callApi(this, newInputModel, "extn_shipmentDetailsForJewelry", null);
        },
        // Overriding the OOB gotoNextScreen method and setting the action in parent screen as CONFIRM,
        // so as to hide the assign staging location screen
        gotoNextScreen: function() {
            _scScreenUtils.clearScreen(this, "translateBarCode_input");
            var parentScreen = null;
            parentScreen = _iasUIUtils.getParentScreen(this, true);
            _iasWizardUtils.setActionPerformedOnWizard(parentScreen, "CONFIRM");
            _scEventUtils.fireEventToParent(this, "onSaveSuccess", null);
        },
        extn_printPDFOnClick: function(event, bEvent, ctrl, args) {
            var getShipmentDetails_output = null;
            getShipmentDetails_output = _scScreenUtils.getModel(this, "backroomPickShipmentDetails_output");
            var shipmentKey = null;
            shipmentKey = _scModelUtils.getStringValueFromPath("Shipment.ShipmentKey", getShipmentDetails_output);
            var deliveryMethod = null;
            deliveryMethod = _scModelUtils.getStringValueFromPath("Shipment.DeliveryMethod", getShipmentDetails_output);
            var userId = null;
            userId = _scUserprefs.getUserId();
            var shipModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
            _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", shipmentKey, shipModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.UserID", userId, shipModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.DeliveryMethod", deliveryMethod, shipModel);
            _iasUIUtils.callApi(this, shipModel, "extn_printPDFOnClick_ref", null);
        },
        openPDF: function(modelOutput) {
            var fileName = _scModelUtils.getStringValueFromPath("Pdf.PdfName", modelOutput);
            var location = window.location.origin + window.contextPath + "/" + fileName;
            window.open(location);
        }
    });
});
