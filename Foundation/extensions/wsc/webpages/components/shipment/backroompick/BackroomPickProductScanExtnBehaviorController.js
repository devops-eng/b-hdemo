scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/text", "scbase/loader!extn/components/shipment/backroompick/BackroomPickProductScanExtn", "scbase/loader!sc/plat/dojo/controller/ExtnServerDataController"], function(_dojodeclare, _dojokernel, _dojotext, _extnBackroomPickProductScanExtn, _scExtnServerDataController) {
    return _dojodeclare("extn.components.shipment.backroompick.BackroomPickProductScanExtnBehaviorController", [_scExtnServerDataController], {
        screenId: 'extn.components.shipment.backroompick.BackroomPickProductScanExtn',
        mashupRefs: [{
            extnType: 'MODIFY',
            mashupId: 'backroomPick_updateShipmentQuantity',
            mashupRefId: 'updateShipmentQuantityForPickAll'
        }, {
            extnType: 'MODIFY',
            mashupId: 'backroomPick_updateShipmentQuantity',
            mashupRefId: 'updateShipmentQuantityForPickAllLine'
        }, {
            extnType: 'MODIFY',
            mashupId: 'backroomPickUp_registerBarcodeForBackroomPick',
            mashupRefId: 'translateBarCode'
        }, {
            extnType: 'ADD',
            mashupId: 'removeCaseNo',
            mashupRefId: 'extn_removeCaseNumberService'
        }, {
            extnType: 'ADD',
            mashupId: 'getShipmentLineList',
            mashupRefId: 'extn_getShipmentLineList'
        }, {
            extnType: 'ADD',
            mashupId: 'extn_printPDFOnClick',
            mashupRefId: 'extn_printPDFOnClick_ref'
        }, {
            extnType: 'ADD',
            mashupId: 'getShipmentLineList',
            mashupRefId: 'extn_shipmentDetailsForJewelry'
        }]
    });
});
