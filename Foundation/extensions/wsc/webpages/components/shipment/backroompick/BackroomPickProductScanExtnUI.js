scDefine(["dojo/text!./templates/BackroomPickProductScanExtn.html", "scbase/loader!dijit/form/Button", "scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/_base/lang", "scbase/loader!dojo/text", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat", "scbase/loader!sc/plat/dojo/binding/ButtonDataBinder", "scbase/loader!sc/plat/dojo/binding/CurrencyDataBinder", "scbase/loader!sc/plat/dojo/binding/ImageDataBinder", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/widgets/DataLabel", "scbase/loader!sc/plat/dojo/widgets/Image", "scbase/loader!sc/plat/dojo/widgets/Label"], function(templateText, _dijitButton, _dojodeclare, _dojokernel, _dojolang, _dojotext, _idxContentPane, _scplat, _scButtonDataBinder, _scCurrencyDataBinder, _scImageDataBinder, _scBaseUtils, _scDataLabel, _scImage, _scLabel) {
    return _dojodeclare("extn.components.shipment.backroompick.BackroomPickProductScanExtnUI", [], {
        templateString: templateText,
        hotKeys: [],
        events: [],
        subscribers: {
            local: [{
                eventId: 'afterScreenLoad',
                sequence: '51',
                handler: {
                    methodName: "extn_afterScreenLoad"
                }
            }, {
                eventId: 'beforeBehaviorMashupCall',
                sequence: '51',
                handler: {
                    methodName: "extn_beforeBehaviorMashupCall"
                }
            }, {
                eventId: 'afterBehaviorMashupCall',
                sequence: '51',
                handler: {
                    methodName: "extn_afterBehaviourMashupCall"
                }
            }, {
                eventId: 'extn_printTciket_onClick',
                sequence: '51',
                handler: {
                    methodName: "extn_printPDFOnClick"
                }
            }]
        }
    });
});
