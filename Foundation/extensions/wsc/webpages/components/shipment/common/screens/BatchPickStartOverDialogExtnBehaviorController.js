


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/components/shipment/common/screens/BatchPickStartOverDialogExtn","scbase/loader!sc/plat/dojo/controller/ExtnServerDataController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnBatchPickStartOverDialogExtn
			 ,
			    _scExtnServerDataController
){

return _dojodeclare("extn.components.shipment.common.screens.BatchPickStartOverDialogExtnBehaviorController", 
				[_scExtnServerDataController], {

			
			 screenId : 			'extn.components.shipment.common.screens.BatchPickStartOverDialogExtn',

			
 mashupRefs: [{
            mashupRefId: 'hbcDeleteAllCaseNo_ref',
            mashupId: 'hbcDeleteAllCaseNo'
        }]
    });
});
