
scDefine(["scbase/loader!dojo/_base/declare",
	"scbase/loader!extn/components/shipment/common/screens/LastScannedProductDetailsExtnUI",
	"scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!ias/utils/BaseTemplateUtils",
 "scbase/loader!ias/utils/EventUtils", "scbase/loader!ias/utils/ScreenUtils", "scbase/loader!ias/utils/UIUtils",
  "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils",
   "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils",
    "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!wsc/components/product/common/utils/ProductUtils",
     "scbase/loader!wsc/components/shipment/backroompick/utils/BackroomPickUpUtils",
     ]
,
function(			 
			    _dojodeclare
			 ,
			    _extnLastScannedProductDetailsExtnUI,
			    _scUserprefs, _iasBaseTemplateUtils, _iasEventUtils, _iasScreenUtils, _iasUIUtils, _scBaseUtils, _scEventUtils,
 _scModelUtils, _scScreenUtils, _scWidgetUtils, _wscProductUtils, _wscBackroomPickUpUtils 

){  
	return _dojodeclare("extn.components.shipment.common.screens.LastScannedProductDetailsExtn", [_extnLastScannedProductDetailsExtnUI],{
	// custom code here
extn_removeBackRoomPickQty:function( event, bEvent, ctrl, args){
	_scUserprefs.setProperty("MinusBtnClicked", "Y");

},
validateAndUpdatePickedQuantity: function(
        action) {
            var quantityTxtBoxModel = null;
            var currentQty = null;
            var maxQty = null;
            var isQtyInRange = null;
            var zero = 0;
            var originalShipmentLineQtyModel = null;
            currentQty = _scModelUtils.getNumberValueFromPath("Quantity", _scScreenUtils.getTargetModel(
            this, "PickedQuantity_Output", null));
            maxQty = _scModelUtils.getNumberValueFromPath("ShipmentLine.Quantity", _scScreenUtils.getTargetModel(
            this, "ShipmentLineQty", null));
            var validationConfigBean = null;
            validationConfigBean = {};
            validationConfigBean["currentQty"] = currentQty;
            validationConfigBean["maxQty"] = maxQty;
            validationConfigBean["minQty"] = zero;
            validationConfigBean["oldQty"] = this.pickedQuantity;
            validationConfigBean["minErrorMsg"] = "Message_Pickedup_Quantity_must_be_greater_than_zero";
            validationConfigBean["maxErrorMsg"] = this.qtyOverrageBundleKey;
            validationConfigBean["messagePanelId"] = "errorMsgPnl";
            isQtyInRange = _iasScreenUtils.ifQtyIsInRangeNew(
            this, this, action, "txtScannedQuantity", validationConfigBean, false);
            if (
            isQtyInRange) {
                if (!(
                _scBaseUtils.equals(
                action, "validate"))) {
                    this.fireEventToUpdateShipmentLineQuantity("editQty", _scWidgetUtils.getValue(
                    this, "txtScannedQuantity"));
                }
            }else{
                if(_scBaseUtils.equals(action,"decrease")){
                    _scUserprefs.setProperty("MinusBtnClicked", "N");
                }
            }
            return isQtyInRange;
        }

});
});

