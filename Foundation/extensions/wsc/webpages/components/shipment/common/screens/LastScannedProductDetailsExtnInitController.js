


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/components/shipment/common/screens/LastScannedProductDetailsExtn","scbase/loader!sc/plat/dojo/controller/ExtnScreenController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnLastScannedProductDetailsExtn
			 ,
			    _scExtnScreenController
){

return _dojodeclare("extn.components.shipment.common.screens.LastScannedProductDetailsExtnInitController", 
				[_scExtnScreenController], {

			
			 screenId : 			'extn.components.shipment.common.screens.LastScannedProductDetailsExtn'

			
			
			
}
);
});

