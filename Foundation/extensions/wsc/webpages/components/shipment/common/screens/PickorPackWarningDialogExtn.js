
scDefine(["scbase/loader!dojo/_base/declare",
	"scbase/loader!extn/components/shipment/common/screens/PickorPackWarningDialogExtnUI",
	 "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!ias/utils/BaseTemplateUtils",
     "scbase/loader!ias/utils/UIUtils", "scbase/loader!ias/utils/EventUtils",
      "scbase/loader!ias/utils/ContextUtils",
       "scbase/loader!sc/plat/dojo/utils/ResourcePermissionUtils",
        "scbase/loader!sc/plat/dojo/utils/ScreenUtils",
         "scbase/loader!sc/plat/dojo/utils/ModelUtils",
          "scbase/loader!sc/plat/dojo/utils/WidgetUtils", 
          "scbase/loader!sc/plat/dojo/utils/EditorUtils",
           "scbase/loader!sc/plat/dojo/utils/EventUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnPickorPackWarningDialogExtnUI, _scBaseUtils, _iasBaseTemplateUtils, 
          _iasUIUtils, _iasEventUtils, _iasContextUtils, _scResourcePermissionUtils, _scScreenUtils,
           _scModelUtils, _scWidgetUtils, _scEditorUtils, _scEventUtils){
	return _dojodeclare("extn.components.shipment.common.screens.PickorPackWarningDialogExtn",[_extnPickorPackWarningDialogExtnUI],{
	// custom code here
extn_removeCaseNo:function(event, bEvent, ctrl, args) {
	var popupData = null;
            popupData = _scBaseUtils.getTargetModel(
            this, "popupData", null);
            var modelOutput=_scScreenUtils.getModel(
            this, "Shipment");
            
            var inputdata = _scBaseUtils.getNewBeanInstance();
            var shipmentKey = _scModelUtils.getStringValueFromPath("Shipment.ShipmentKey", modelOutput);
         _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", shipmentKey, inputdata);
       
            _iasUIUtils.callApi(this, inputdata, "hbcDeleteAllCaseNo_ref", null);
            _scEventUtils.stopEvent(bEvent);

},
 handleMashupOutput: function(mashupRefId, modelOutput, mashupInput, mashupContext, applySetModel) {
            if (_scBaseUtils.equals(mashupRefId, "hbcDeleteAllCaseNo_ref")) {
               this.onStartOverSelectAction();
            } 
        },
        handleMashupCompletion: function(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data) {
            _iasBaseTemplateUtils.handleMashupCompletion(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data, this);
        },

});
});

