
scDefine(["scbase/loader!dojo/_base/declare",
	"scbase/loader!extn/components/shipment/common/screens/ShipmentLineDetailsExtnUI",
	"scbase/loader!sc/plat/dojo/Userprefs",
	"scbase/loader!ias/utils/EventUtils",
 "scbase/loader!ias/utils/ScreenUtils", "scbase/loader!ias/utils/UIUtils",
  "scbase/loader!sc/plat/dojo/utils/BaseUtils",
  "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", 
  "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils",
   "scbase/loader!wsc/components/product/common/utils/ProductUtils",
    "scbase/loader!wsc/components/shipment/backroompick/utils/BackroomPickUpUtils",
    "scbase/loader!ias/utils/ContextUtils",
    "scbase/loader!extn/expicient/utils/ExpicientUtil",
    "scbase/loader!dojo/currency"
     ]
,
function(			 
			    _dojodeclare
			 ,
			    _extnShipmentLineDetailsExtnUI,
			    _scUserprefs,
			    _iasEventUtils, _iasScreenUtils, _iasUIUtils, _scBaseUtils, _scEventUtils,
 _scModelUtils, _scScreenUtils, _scWidgetUtils, _wscProductUtils, _wscBackroomPickUpUtils, _iasContextUtils,_extnExpicientUtils,dCurrency
){ 
	return _dojodeclare("extn.components.shipment.common.screens.ShipmentLineDetailsExtn", [_extnShipmentLineDetailsExtnUI],{
	// custom code here
	extn_removeBackRoomPickQty:function( event, bEvent, ctrl, args){
	_scUserprefs.setProperty("MinusBtnClicked", "Y");

},
 extn_afterScreenInit: function(event, bEvent, ctrl, args) {
            var shipmentLineDetails = _scScreenUtils.getModel(this, "ShipmentLine");
            _scWidgetUtils.addClass(this, "extn_label", "labelPaddingGridCss");
            //add css for Mobile - START
            if (_iasContextUtils.isMobileContainer()) {
                _scWidgetUtils.addClass(this, "extn_DeptCode", "verticalAlignMobile");
                _scWidgetUtils.removeClass(this, "extn_DeptCode", "verticalAlign1");
                _scWidgetUtils.addClass(this, "extn_deptNumber", "verticalAlignMobile");
                _scWidgetUtils.removeClass(this, "extn_deptNumber", "verticalAlign1");
                _scWidgetUtils.addClass(this, "extn_deptName", "verticalAlignMobile");
                _scWidgetUtils.removeClass(this, "extn_deptName", "verticalAlign1");
                _scWidgetUtils.addClass(this, "extn_classDesc", "verticalAlignMobile");
                _scWidgetUtils.removeClass(this, "extn_classDesc", "verticalAlign1");
                _scWidgetUtils.addClass(this, "extn_Style", "verticalAlignMobile");
                _scWidgetUtils.removeClass(this, "extn_Style", "verticalAlign1");
                _scWidgetUtils.addClass(this, "extn_color", "verticalAlignMobile");
                _scWidgetUtils.removeClass(this, "extn_color", "verticalAlign1");
                _scWidgetUtils.addClass(this, "extn_size", "verticalAlignMobile");
                _scWidgetUtils.removeClass(this, "extn_size", "verticalAlign1");
                _scWidgetUtils.addClass(this, "extn_upcNumber", "verticalAlignMobile");
                _scWidgetUtils.removeClass(this, "extn_upcNumber", "verticalAlign1");
                _scWidgetUtils.addClass(this, "extn_sknNumber", "verticalAlignMobile");
                _scWidgetUtils.removeClass(this, "extn_sknNumber", "verticalAlign1");
                _scWidgetUtils.addClass(this, "extn_label", "verticalAlignMobile");
                _scWidgetUtils.removeClass(this, "extn_label", "verticalAlign1");
                _scWidgetUtils.addClass(this, "extn_price", "verticalAlignMobile");
                _scWidgetUtils.removeClass(this, "extn_price", "verticalAlign1");
                _scWidgetUtils.addClass(this, "extn_qtyOnHand", "verticalAlignMobile");
                _scWidgetUtils.removeClass(this, "extn_qtyOnHand", "verticalAlign1");
                _scWidgetUtils.addClass(this, "shortageResolutionLink", "shortageResolutionLinkMobile");
                _scWidgetUtils.addClass(this, "extn_contentpane", "containerCss");
            }
            //add css for Mobile -  END
            //   var quantity= _scModelUtils.getStringValueFromPath("ShipmentLine.Quantity", shipmentLineDetails);
            //  _scWidgetUtils.setValue(this, "extn_qtyOnHand", quantity);
            /*  if (shipmentLineDetails.ShipmentLine.OrderLine && shipmentLineDetails.ShipmentLine.OrderLine.ItemDetails && shipmentLineDetails.ShipmentLine.OrderLine.ItemDetails.Extn && shipmentLineDetails.ShipmentLine.OrderLine.ItemDetails.Extn.ExtnJewelryFlag == "Y") {
                _scWidgetUtils.showWidget(this, "extn_jelwellaryItem", false, null);
 } else {
     
 }*/
_scWidgetUtils.hideWidget(this, "extn_jelwellaryItem");
        },
 validateAndUpdatePickedQuantity: function(
        action) {
            var quantityTxtBoxModel = null;
            var currentQty = null;
            var maxQty = null;
            var zero = 0;
            var isQtyInRange = false;
            var originalShipmentLineQtyModel = null;
            currentQty = _scModelUtils.getNumberValueFromPath("Quantity", _scScreenUtils.getTargetModel(
            this, "QuantityTextBoxModel_Output", null));
            maxQty = _scModelUtils.getNumberValueFromPath("ShipmentLine.Quantity", _scScreenUtils.getTargetModel(
            this, "ShipmentLineQty", null));
            var validationConfigBean = null;
            validationConfigBean = {};
            validationConfigBean["currentQty"] = currentQty;
            validationConfigBean["maxQty"] = maxQty;
            validationConfigBean["minQty"] = zero;
            validationConfigBean["oldQty"] = this.pickedQuantity;
            validationConfigBean["minErrorMsg"] = "Message_Picked_Quantity_must_be_greater_than_zero";
            validationConfigBean["maxErrorMsg"] = "ItemOverrage_BackroomPick";
            validationConfigBean["lastUpdatedRepPanelPropertyName"] = "LastScannedShipmentLineScreenUId";
            validationConfigBean["messagePanelId"] = "errorMsgPnl";
            isQtyInRange = _iasScreenUtils.ifQtyIsInRangeNew(
            this, this, action, "txtScannedQuantity", validationConfigBean, true);
            if (
            isQtyInRange) {
                if (!(
                _scBaseUtils.equals(
                action, "validate"))) {
                    this.fireEventToUpdateShipmentLineQuantity("editQty", _scWidgetUtils.getValue(
                    this, "txtScannedQuantity"));
                }
            }else{
            	if(_scBaseUtils.equals(action,"decrease")){
            		_scUserprefs.setProperty("MinusBtnClicked", "N");
            	}
            }
            return isQtyInRange;
        },
         extn_getColor: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
            var color = null;
            if (!_scBaseUtils.isVoid(shipmentLineModel)) {
                var attributeModel = _scModelUtils.getModelObjectFromPath("ShipmentLine.OrderLine.ItemDetails.AdditionalAttributeList.AdditionalAttribute", shipmentLineModel);
                if (!_scBaseUtils.isVoid(attributeModel)) {
                    for (var i = 0; i < attributeModel.length; i++) {
                        var refrenceObj = _scBaseUtils.getArrayItemByIndex(attributeModel, i);
                        var name = _scModelUtils.getStringValueFromPath("Name", refrenceObj);
                        if (_scBaseUtils.equals(name, "Color")) {
                            color = _scModelUtils.getStringValueFromPath("Value", refrenceObj);
                        }
                    }
                }
            }
            return color;
        },
        extn_getPrice: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
            var price = null;
            var country = null;
            if (!_scBaseUtils.isVoid(dataValue)) {
                var listPrice = null;
                listPrice = _scModelUtils.getStringValueFromPath("Extn.ExtnItemListPrice", dataValue);
                if (!_scBaseUtils.isVoid(listPrice)) {
                var currencyModel = _scModelUtils.getModelObjectFromPath("ShipmentLine.Shipment", shipmentLineModel);
                if (!_scBaseUtils.isVoid(currencyModel)) {
                    currencyModel = _scBaseUtils.getArrayItemByIndex(currencyModel, 0);
                        country = _scModelUtils.getStringValueFromPath("Currency", currencyModel);
                        if (!_scBaseUtils.isVoid(country)) {
                            price = dCurrency.format(parseFloat(listPrice), {
                                currency: country
                            });
                        }
                    }
                } else {
                    price = "";
                    return "";
                }
                var allPrice = {
                    "price": price
                };
                _scScreenUtils.setModel(this, "ListPrice", allPrice);
            }
            return price;
        },
        // extn_openProductDetails: function(event, bEvent, ctrl, args) {
        //      _scEventUtils.stopEvent(bEvent);
        //     var itemModel = _scScreenUtils.getTargetModel(this, "ItemDetails", null);
        //     var priceModel = _scScreenUtils.getModel(this, "ListPrice");
        //     if (priceModel && priceModel.price) {
        //         _scModelUtils.setStringValueAtModelPath("Price", priceModel.price, itemModel);
        //     } else {
        //         _scModelUtils.setStringValueAtModelPath("Price", "", itemModel);
        //     }
        //     _wscProductUtils.openItemDetails(this, itemModel);
        // },
        extn_getSize: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
            var size = null;
            if (!_scBaseUtils.isVoid(shipmentLineModel)) {
                var attributeModel = _scModelUtils.getModelObjectFromPath("ShipmentLine.OrderLine.ItemDetails.AdditionalAttributeList.AdditionalAttribute", shipmentLineModel);
                if (!_scBaseUtils.isVoid(attributeModel)) {
                    for (var i = 0; i < attributeModel.length; i++) {
                        var refrenceObj = _scBaseUtils.getArrayItemByIndex(attributeModel, i);
                        var name = _scModelUtils.getStringValueFromPath("Name", refrenceObj);
                        if (_scBaseUtils.equals(name, "Size")) {
                            size = _scModelUtils.getStringValueFromPath("Value", refrenceObj);
                        }
                    }
                }
            }
            return size;
        },
        extn_getUPC: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
            var aliasValue = null;
            if (!_scBaseUtils.isVoid(shipmentLineModel)) {
                var aliasModel = _scModelUtils.getModelObjectFromPath("ShipmentLine.OrderLine.ItemDetails.ItemAliasList.ItemAlias", shipmentLineModel);
                if (!_scBaseUtils.isVoid(aliasModel)) {
                    for (var i = 0; i < aliasModel.length; i++) {
                        var refrenceObj = _scBaseUtils.getArrayItemByIndex(aliasModel, i);
                        var aliasName = _scModelUtils.getStringValueFromPath("AliasName", refrenceObj);
                        if (_scBaseUtils.equals(aliasName, "ACTIVE_UPC")) {
                            aliasValue = _scModelUtils.getStringValueFromPath("AliasValue", refrenceObj);
                        }
                    }
                }
            }
            return aliasValue;
        },
        extn_getQty: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
              var itemID = null;
            var unitOfMeasure = null;
            var itemModel = null;
            if (!_scBaseUtils.isVoid(shipmentLineModel)) {
                itemModel = _scModelUtils.getModelObjectFromPath("ShipmentLine", shipmentLineModel);
                if (!_scBaseUtils.isVoid(itemModel)) {
                    itemID = _scModelUtils.getStringValueFromPath("ItemID", itemModel);
                    unitOfMeasure = _scModelUtils.getStringValueFromPath("UnitOfMeasure", itemModel);
                    productClass = _scModelUtils.getStringValueFromPath("ProductClass", itemModel);
                }
                var shipNode = null;
                shipNode = _iasContextUtils.getFromContext("CurrentStore");
                var organization = null;
                var shipmentModel= _scModelUtils.getStringValueFromPath("ShipmentLine.Shipment", shipmentLineModel); 
                  if(!_scBaseUtils.isVoid(shipmentModel)){
                  	organization = _scModelUtils.getStringValueFromPath("EnterpriseCode", shipmentModel[0]);
                  }               
                var model = null;
                model = _scBaseUtils.getNewModelInstance();
                if(!_scBaseUtils.isVoid(organization)){
                	 _scModelUtils.setStringValueAtModelPath("InventorySupply.OrganizationCode", organization, model);
                }
                _scModelUtils.setStringValueAtModelPath("InventorySupply.ShipNode", shipNode, model);
                _scModelUtils.setStringValueAtModelPath("InventorySupply.ItemID", itemID, model);
                _scModelUtils.setStringValueAtModelPath("InventorySupply.UnitOfMeasure", unitOfMeasure, model);
                _scModelUtils.setStringValueAtModelPath("InventorySupply.ProductClass", productClass, model);
                _scModelUtils.setStringValueAtModelPath("InventorySupply.SupplyType", "ONHAND", model);
                _iasUIUtils.callApi(this, model, "extn_getQtyOnHand_ref", null);
            }
        },
         extn_afterBehaviorMashupCall: function(event, bEvent, ctrl, args) {
            var isMashupAvailable = _extnExpicientUtils.isMashupAvailable("extn_getQtyOnHand_ref", args);
            if (isMashupAvailable) {
                var getItemListModelOutput = null;
                getItemListModelOutput = _extnExpicientUtils.getMashupOutputForRefID("extn_getQtyOnHand_ref", args);
                if (!_scBaseUtils.isVoid(getItemListModelOutput)) {
                    var itemModel = null;
                    itemModel = _scModelUtils.getModelObjectFromPath("Item.Supplies.InventorySupply", getItemListModelOutput);
                    var itemID = null;
                    if (!_scBaseUtils.isVoid(itemModel)) {
                        var availableQuantity = null;
                        availableQuantity = _scModelUtils.getStringValueFromPath("Quantity", itemModel);
                        if (!_scBaseUtils.isVoid(availableQuantity)) {
                            _scWidgetUtils.setValue(this, "extn_qtyOnHand", parseInt(availableQuantity));
                        }
                    }
                }
            }
        }
});
});

