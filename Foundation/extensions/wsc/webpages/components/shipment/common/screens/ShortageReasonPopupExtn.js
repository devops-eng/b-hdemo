scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/components/shipment/common/screens/ShortageReasonPopupExtnUI", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/widgets/Label", "scbase/loader!sc/plat/dojo/widgets/Link", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/GridxUtils", "scbase/loader!gridx/Grid", "scbase/loader!sc/plat/dojo/binding/GridxDataBinder", "scbase/loader!ias/utils/RepeatingScreenUtils", "scbase/loader!sc/plat/dojo/utils/RepeatingPanelUtils", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!sc/plat/dojo/utils/RequestUtils", "scbase/loader!ias/utils/ScreenUtils"], function(_dojodeclare, _extnShortageReasonPopupExtnUI, _scBaseUtils, _scEventUtils, _scLabel, _scLink, _scScreen, _scModelUtils, _iasUIUtils, _scScreenUtils, _iasBaseTemplateUtils, _scWidgetUtils, _scEventUtils, _scGridxUtils, _gridxGrid, _scGridxDataBinder, _iasRepeatingScreenUtils, _scRepeatingPanelUtils, _scUserprefs, _iasContextUtils, _extnExpicientUtils, _scRequestUtils, _iasScreenUtils) {
    return _dojodeclare("extn.components.shipment.common.screens.ShortageReasonPopupExtn", [_extnShortageReasonPopupExtnUI], {
        // custom code here
        extn_beforeInitScreen: function(event, bEvent, ctrl, args) {
            var shortageCodeModel = _scScreenUtils.getModel(this, "getShortageReasonCode_output");
            var numOfShortageCode = _scModelUtils.getNumberValueFromPath("CommonCodeList.TotalNumberOfRecords", shortageCodeModel);
            var reasonCode = _scModelUtils.getStringValueFromPath("CommonCodeList.CommonCode", shortageCodeModel);
            var length = parseInt(numOfShortageCode);
            for (var index = length - 1; _scBaseUtils.greaterThanOrEqual(index, 0); index = index - 1) {
                currReason = _scBaseUtils.getArrayBeanItemByIndex(reasonCode, index);
                var codeValue = _scModelUtils.getStringValueFromPath("CodeValue", currReason);
                if (_scBaseUtils.equals(codeValue, "picklater")) {
                    reasonCode.splice(index, 1);
                }
            }
            _scScreenUtils.setModel(this, "getShortageReasonCode_output", shortageCodeModel);
            if (_scBaseUtils.equals(this.flowName, "Bagging")) {
                _scWidgetUtils.setLabel(this, "lblPickedQty", _scScreenUtils.getString(this, "extn_bagged"));
               // _scWidgetUtils.showWidget(this, "chkMarkAllLines", false, null);
                _scWidgetUtils.hideWidget(this, "chkMarkAllLines", false);  
            }
        }
    });
});
