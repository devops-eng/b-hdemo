scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/components/shipment/container/pack/ContainerPackContainerListExtnUI", "scbase/loader!idx/form/TextBox", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/widgets/Label", "scbase/loader!sc/plat/dojo/widgets/Link", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/GridxUtils", "scbase/loader!gridx/Grid", "scbase/loader!sc/plat/dojo/binding/GridxDataBinder", "scbase/loader!ias/utils/RepeatingScreenUtils", "scbase/loader!sc/plat/dojo/utils/RepeatingPanelUtils", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!sc/plat/dojo/utils/RequestUtils", "scbase/loader!ias/utils/ScreenUtils", "scbase/loader!ias/utils/EventUtils", "scbase/loader!wsc/components/shipment/container/pack/ContainerPackUtils"], function(_dojodeclare, _extnContainerPackContainerListExtnUI, _idxTextBox, _idxContentPane, _scBaseUtils, _scWidgetUtils, _scEventUtils, _scLabel, _scLink, _scScreen, _scModelUtils, _iasUIUtils, _scScreenUtils, _iasBaseTemplateUtils, _scWidgetUtils, _scEventUtils, _scGridxUtils, _gridxGrid, _scGridxDataBinder, _iasRepeatingScreenUtils, _scRepeatingPanelUtils, _scUserprefs, _iasContextUtils, _extnExpicientUtils, _scRequestUtils, _iasScreenUtils, _iasEventUtils, _wscContainerPackUtils) {
    return _dojodeclare("extn.components.shipment.container.pack.ContainerPackContainerListExtn", [_extnContainerPackContainerListExtnUI], {
        // custom code here
        extn_afterScreenInit: function(event, bEvent, ctrl, args) {
            if (_iasContextUtils.isMobileContainer()) {
                _scWidgetUtils.addClass(this, "weightDetails", "weightWidgetMobile");
                _scWidgetUtils.addClass(this, "extn_printLabelContainer", "extn_printLabelContainerMobile");
                _scWidgetUtils.addClass(this, "saveButton", "saveButtonMobile");
            }
            var labelModel = null;
            labelModel = _scScreenUtils.getModel(this, "container_Src");
            var shipmentContainerKey = _scModelUtils.getStringValueFromPath("Container.ShipmentContainerKey", labelModel);
            var shipmentKey = _scModelUtils.getStringValueFromPath("Container.ShipmentKey", labelModel);
            //  var apiInput1 = _scModelUtils.createNewModelObjectWithRootKey("Container");
            // _scModelUtils.setStringValueAtModelPath("Container.ShipmentKey", shipmentKey, apiInput1);
            // _scModelUtils.setStringValueAtModelPath("Container.ShipmentContainerKey", shipmentContainerKey, apiInput1);
            //  _iasUIUtils.callApi(this, apiInput1, "extn_getShipmentContainerDetails_ref", null);
            var isShipLabelGenerated = null;
            var isPackingCompleted = null;
            var containerRef = _scUserprefs.getProperty("Container");
            isShipLabelGenerated = _scModelUtils.getStringValueFromPath("Container.Extn.ExtnIsShipLabelGenerated", labelModel);
            isPackingCompleted = _scModelUtils.getStringValueFromPath("Container.Extn.ExtnIsPackingCompleted", labelModel);
            if (_scBaseUtils.equals(isShipLabelGenerated, "Y")) {
                _scWidgetUtils.setLabel(this, "extn_printShippingLabel", this.getSimpleBundleString('extn_Reprint_Shipping_Label'));
            } else {
                _scWidgetUtils.setLabel(this, "extn_printShippingLabel", this.getSimpleBundleString('extn_Print_Shipping_Label'));
            }
            if (_scBaseUtils.equals(isPackingCompleted, "Y")) {
                _scWidgetUtils.setLabel(this, "extn_printPackSlip", this.getSimpleBundleString('extn_Reprint_Pack_Slip'));
            } else {
                _scWidgetUtils.setLabel(this, "extn_printPackSlip", this.getSimpleBundleString('PrintPackSlip'));
            }
            this.extn_populateGiftWrap();
        },
        extn_populateGiftWrap: function() {
            // var containerList = _scUserprefs.getProperty("ContainerList");
            var shipmentLineKeyArr = _scBaseUtils.getNewArrayInstance();
            var labelModel = null;
            labelModel = _scScreenUtils.getModel(this, "container_Src");
            var shipmentContainerKey = _scModelUtils.getStringValueFromPath("Container.ShipmentContainerKey", labelModel);
            var parent = _iasUIUtils.getParentScreen(this, false);
            getContainerList_Out = _scScreenUtils.getModel(parent, "getContainerList_Out_Src_Pg");
            var containerList= _scModelUtils.getStringValueFromPath("Page.Output.Containers.Container",getContainerList_Out);
            var len = containerList.length;
            if (!_scBaseUtils.isVoid(len)) {
                for (i = 0; i < len; i++) {
                    ContainerKey = _scModelUtils.getStringValueFromPath("ShipmentContainerKey", containerList[i]);
                    if (_scBaseUtils.equals(shipmentContainerKey, ContainerKey)) {
                        containerDetail = _scModelUtils.getStringValueFromPath("ContainerDetails.ContainerDetail", containerList[i]);
                        len1 = containerDetail.length;
                        if (!_scBaseUtils.isVoid(len1)) {
                            for (j = 0; j < len1; j++) {
                                shipmentlinekey = _scModelUtils.getStringValueFromPath("ShipmentLineKey", containerDetail[j]);
                                shipmentLineKeyArr.push(shipmentlinekey);
                            }
                        }
                    }
                }
            }
            var getShipmentDetails_output = _scScreenUtils.getModel(parent, "getShipmentDetails_output");
            var shipment = _scModelUtils.getStringValueFromPath("Shipment.ShipmentLines", getShipmentDetails_output);
            var shipmentLines = _scModelUtils.getStringValueFromPath("ShipmentLine", shipment);
            var count = 0;
            var instructionText = "NONE";
            var InstructionText1= instructionText;
            for (i = 0; i < shipmentLines.length; i++) {
                var shipmentLineKey1 = _scModelUtils.getStringValueFromPath("ShipmentLineKey", shipmentLines[i]);
                if (_scBaseUtils.containsInArray(shipmentLineKeyArr, shipmentLineKey1)) {
                    var shipementModel = shipmentLines[i];
                    var giftFlag = _scModelUtils.getStringValueFromPath("OrderLine.GiftFlag", shipementModel);
                    var instruction = _scModelUtils.getStringValueFromPath("OrderLine.Instructions.Instruction", shipementModel);
                    if (_scBaseUtils.equals(giftFlag, "Y")) {
                        for (j = 0; j < instruction.length; j++) {
                            var instructionType = _scModelUtils.getStringValueFromPath("InstructionType", instruction[j]);
                            if (_scBaseUtils.equals(instructionType, "WRAP_TYPE")) {
                                instructionText = _scModelUtils.getStringValueFromPath("InstructionText", instruction[j]);
                                count++;
                                break;
                            }
                        }
                    }
                }
            }
            if (_scBaseUtils.contains(instructionText, "-")) {
                var index = instructionText.indexOf("-");
                index++;
                InstructionText1 = instructionText.substring(index);
            } else {
                InstructionText1 = instructionText;
            }

            if (count > 1) {
                _scWidgetUtils.setValue(this, "extn_giftWrap", "Multiple", false);
            } else {
                _scWidgetUtils.setValue(this, "extn_giftWrap", InstructionText1, false);
            }
            //    var shipmentDetails= _scScreenUtils.getModel(parent, "getContainerList_Out_Src_Pg");
            // var labelModel = null;
            // labelModel = _scScreenUtils.getModel(this, "container_Src");
            //  var shipmentContainer = _scModelUtils.getStringValueFromPath("Container.ShipmentContainerKey", labelModel);
            // var shipmentLineKeyArr = _scBaseUtils.getNewArrayInstance();
            // var commonCodeList = _scUserprefs.getProperty("WRAP");
            // var outputData = _scUserprefs.getProperty("outputData1");
            /* var containerDetail = _scModelUtils.getStringValueFromPath("Container.ContainerDetails.ContainerDetail", outputData);
             var len = containerDetail.length;
             if (!_scBaseUtils.isVoid(len)) {
                 for (i = 0; i < len; i++) {
                     var shipmentLineKey = _scModelUtils.getStringValueFromPath("ShipmentLineKey", containerDetail[i]);
                     shipmentLineKeyArr.push(shipmentLineKey);
                 }
             } else {
                 var shipmentLineKey = _scModelUtils.getStringValueFromPath("ShipmentLineKey", containerDetail);
                 shipmentLineKeyArr.push(shipmentLineKey);
             }*/
            // var commonCodeList = _scScreenUtils.getModel(parent, "extn_commonCode");
            //   var commonCode = _scModelUtils.getStringValueFromPath("CommonCodeList.CommonCode", commonCodeList);

            /*if (!_scBaseUtils.isVoid(commonCode)) {
                for (i = 0; i < commonCode.length; i++) {
                    var codevalue = _scModelUtils.getStringValueFromPath("CodeValue", commonCode[i]);
                    if (_scBaseUtils.equals(instructionText, codevalue)) {
                        var InstructionText1 = _scModelUtils.getStringValueFromPath("CodeShortDescription", commonCode[i]);
                        break;
                    }
                    else{
                                var InstructionText1= instructionText;
                        
                    }
                }
            }*/
        },
        extn_rePrintShippingLabel: function(event, bEvent, ctrl, args) {
            var scacIntegration = null;
            scacIntegration = _wscContainerPackUtils.getScacIntegrationReqd(this);
            var containerModel = null;
            var packageWeight = null;
            containerModel = _scScreenUtils.getModel(this, "container_Src");
            var containerShipmentKey = null;
            var containerNo = null;
            var isShipLabelGenerated = null;
            var isPackingCompleted = null;
            isShipLabelGenerated = _scModelUtils.getStringValueFromPath("Container.Extn.ExtnIsShipLabelGenerated", containerModel);
            isPackingCompleted = _scModelUtils.getStringValueFromPath("Container.Extn.ExtnIsPackingCompleted", containerModel);
            containerShipmentKey = _scModelUtils.getStringValueFromPath("Container.ShipmentKey", containerModel);
            if (_scBaseUtils.isVoid(containerShipmentKey)) {
                var parentModel = null;
                parentModel = _scScreenUtils.getModel(this.getOwnerScreen(), "getContainerList_Out_Src_Pg");
                containerShipmentKey = _scModelUtils.getStringValueFromPath("Page.LastRecord.Container.ShipmentKey", parentModel);
            }
            containerNo = _scModelUtils.getStringValueFromPath("Container.ContainerNo", containerModel);
            packageWeight = _scModelUtils.getStringValueFromPath("Container.ActualWeight", containerModel);
            var numberPackageWeight = 0;
            numberPackageWeight = _scModelUtils.getNumberValueFromPath("Container.ActualWeight", containerModel);
            if (_scBaseUtils.equals(numberPackageWeight, 0) || _scBaseUtils.isVoid(packageWeight)) {
                _iasBaseTemplateUtils.showMessage(this, "extn_Package_Weight", "information", null);
            } else {
                if (_scBaseUtils.equals(isShipLabelGenerated, "Y")) {
                    _scWidgetUtils.setLabel(this, "extn_printShippingLabel", this.getSimpleBundleString('extn_Reprint_Shipping_Label'));
                }
                var shippingLabel = null;
                shippingLabel = _scWidgetUtils.getLabel(this, "extn_printShippingLabel");
                var printPackSlip = null;
                var printShippingLabel = null;
                var rePrintPackSlip = null;
                var rePrintShippingLabel = null;
                if (_scBaseUtils.equals(shippingLabel, this.getSimpleBundleString("extn_Print_Shipping_Label"))) {
                    var printPackSlip = "N";
                    var printShippingLabel = "Y";
                    var rePrintPackSlip = "N";
                    var rePrintShippingLabel = "N";
                    var shipmentModel = null;
                    shipmentModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
                    _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", containerShipmentKey, shipmentModel);
                    _scModelUtils.setStringValueAtModelPath("Shipment.ScacIntegrationRequired", scacIntegration, shipmentModel);
                    _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.ContainerNo", containerNo, shipmentModel);
                    _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.PrintPackSlip", printPackSlip, shipmentModel);
                    _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.PrintShippingLabel", printShippingLabel, shipmentModel);
                    _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.RePrintPackSlip", rePrintPackSlip, shipmentModel);
                    _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.RePrintShippingLabel", rePrintShippingLabel, shipmentModel);
                    _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.Extn.ExtnIsShipLabelGenerated", isShipLabelGenerated, shipmentModel);
                    _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.Extn.ExtnIsPackingCompleted", isPackingCompleted, shipmentModel);
                    _iasUIUtils.callApi(this, shipmentModel, "extn_hbcPackSlip_ref", null);
                } else if (_scBaseUtils.equals(shippingLabel, this.getSimpleBundleString("extn_Reprint_Shipping_Label"))) {
                    var printPackSlip = "N";
                    var printShippingLabel = "N";
                    var rePrintPackSlip = "N";
                    var rePrintShippingLabel = "Y";
                    var shipmentModel = null;
                    shipmentModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
                    _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", containerShipmentKey, shipmentModel);
                    _scModelUtils.setStringValueAtModelPath("Shipment.ScacIntegrationRequired", scacIntegration, shipmentModel);
                    _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.ContainerNo", containerNo, shipmentModel);
                    _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.PrintPackSlip", printPackSlip, shipmentModel);
                    _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.PrintShippingLabel", printShippingLabel, shipmentModel);
                    _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.RePrintPackSlip", rePrintPackSlip, shipmentModel);
                    _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.RePrintShippingLabel", rePrintShippingLabel, shipmentModel);
                    _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.Extn.ExtnIsShipLabelGenerated", isShipLabelGenerated, shipmentModel);
                    _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.Extn.ExtnIsPackingCompleted", isPackingCompleted, shipmentModel);
                    _iasUIUtils.callApi(this, shipmentModel, "extn_hbcPackSlip_ref", null);
                }
            }
        },
        extn_printPackSlip: function(event, bEvent, ctrl, args) {
            var containerModel = null;
            containerModel = _scScreenUtils.getModel(this, "container_Src");
            var containerShipmentKey = null;
            var containerNo = null;
            var isShipLabelGenerated = null;
            var isPackingCompleted = null;
            isShipLabelGenerated = _scModelUtils.getStringValueFromPath("Container.Extn.ExtnIsShipLabelGenerated", containerModel);
            isPackingCompleted = _scModelUtils.getStringValueFromPath("Container.Extn.ExtnIsPackingCompleted", containerModel);
            containerShipmentKey = _scModelUtils.getStringValueFromPath("Container.ShipmentKey", containerModel);
            if (_scBaseUtils.isVoid(containerShipmentKey)) {
                var parentModel = null;
                parentModel = _scScreenUtils.getModel(this.getOwnerScreen(), "getContainerList_Out_Src_Pg");
                containerShipmentKey = _scModelUtils.getStringValueFromPath("Page.LastRecord.Container.ShipmentKey", parentModel);
            }
            containerNo = _scModelUtils.getStringValueFromPath("Container.ContainerNo", containerModel);
            var shipmentModel = null;
            shipmentModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
            var printPackSlip = null;
            var printShippingLabel = null;
            var rePrintPackSlip = null;
            var rePrintShippingLabel = null;
            if (_scBaseUtils.equals(isPackingCompleted, "Y")) {
                _scWidgetUtils.setLabel(this, "extn_printPackSlip", this.getSimpleBundleString('extn_Reprint_Pack_Slip'));
            }
            var packSlipLabel = null;
            packSlipLabel = _scWidgetUtils.getLabel(this, "extn_printPackSlip");
            if (_scBaseUtils.equals(packSlipLabel, this.getSimpleBundleString("PrintPackSlip"))) {
                printPackSlip = "Y";
                printShippingLabel = "N";
                rePrintPackSlip = "N";
                rePrintShippingLabel = "N";
                _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", containerShipmentKey, shipmentModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.ContainerNo", containerNo, shipmentModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.PrintPackSlip", printPackSlip, shipmentModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.PrintShippingLabel", printShippingLabel, shipmentModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.RePrintPackSlip", rePrintPackSlip, shipmentModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.RePrintShippingLabel", rePrintShippingLabel, shipmentModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.Extn.ExtnIsShipLabelGenerated", isShipLabelGenerated, shipmentModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.Extn.ExtnIsPackingCompleted", isPackingCompleted, shipmentModel);
                _iasUIUtils.callApi(this, shipmentModel, "extn_hbcPackSlip_ref", null);
            } else if (_scBaseUtils.equals(packSlipLabel, this.getSimpleBundleString("extn_Reprint_Pack_Slip"))) {
                printPackSlip = "N";
                printShippingLabel = "N";
                rePrintPackSlip = "Y";
                rePrintShippingLabel = "N";
                _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", containerShipmentKey, shipmentModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.ContainerNo", containerNo, shipmentModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.PrintPackSlip", printPackSlip, shipmentModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.PrintShippingLabel", printShippingLabel, shipmentModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.RePrintPackSlip", rePrintPackSlip, shipmentModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.RePrintShippingLabel", rePrintShippingLabel, shipmentModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.Extn.ExtnIsShipLabelGenerated", isShipLabelGenerated, shipmentModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.Extn.ExtnIsPackingCompleted", isPackingCompleted, shipmentModel);
                _iasUIUtils.callApi(this, shipmentModel, "extn_hbcPackSlip_ref", null);
            }
        },
			   printBufferMsg : function(bufferMsg){
               var iframe = document.createElement('iframe'); 
			   var myDiv=document.getElementById("extensibilityShellHolder");
               myDiv.appendChild(iframe);
               var html = '<HTML><BODY onload="silentPrintPage()"style="margin: 20px;">';
               html+=bufferMsg;
			   html += '<object id="factory" style="display:none" classid="clsid:1663ed61-23eb-11d2-b92f-008048fdd814" codebase="/extnloader/smsx.cab#Version=7.7.0.20"><param name="template" value="MeadCo://IE7" /></object>';
    html += '<script>function silentPrintPage(){factory.printing.Print(false)}</script>';
    html += '</BODY></HTML>';
    iframe.contentWindow.document.open();
    iframe.contentWindow.document.write(html);
    iframe.contentWindow.document.close();
    myDiv.removeChild(iframe);
        },
        extn_afterBehaviorMashupCall: function(event, bEvent, ctrl, args) {
            /*var isMashupAvailable1 = _extnExpicientUtils.isMashupAvailable("extn_getShipmentContainerDetails_ref", args);
            if (isMashupAvailable1) {
                var outputData = null;
                outputData = _extnExpicientUtils.getMashupOutputForRefID("extn_getShipmentContainerDetails_ref", args);
                var shipLineKey = _scModelUtils.getStringValueFromPath("Container.ContainerDetails.ContainerDetail.ShipmentLineKey", outputData);
                _scUserprefs.setProperty("outputData1", outputData);
                this.extn_populateGiftWrap();
            }*/
            var isMashupAvailable = _extnExpicientUtils.isMashupAvailable("extn_hbcPackSlip_ref", args);
            if (isMashupAvailable) {
                _scEventUtils.fireEventToParent(this, "refreshScreenData", null);
                var outputData = null;
                outputData = _extnExpicientUtils.getMashupOutputForRefID("extn_hbcPackSlip_ref", args);
                var shiplabelFlag = null;
                shiplabelFlag = _scModelUtils.getStringValueFromPath("Shipment.ShipLabelPrinted", outputData);
                var packSlipFlag = null;
                packSlipFlag = _scModelUtils.getStringValueFromPath("Shipment.PackSlipPrinted", outputData);
                var errorCode = null;
                errorCode = _scModelUtils.getStringValueFromPath("Shipment.ErrorCode", outputData);
				var usbPrinterFlag = _scModelUtils.getStringValueFromPath("Shipment.USBPrinter", outputData);

                if (!_scBaseUtils.isVoid(errorCode)) {
                    var errorDesc = null;
                    errorDesc = _scModelUtils.getStringValueFromPath("Shipment.ErrorDescription", outputData);
                    _iasBaseTemplateUtils.showMessage(this, errorDesc, "error", null);
                } else {
				if(_scBaseUtils.equals(usbPrinterFlag,"Y")){
                                var printmsg=outputData.Shipment.Printers.Printer[0].PrintBuffer;
                                this.printBufferMsg(printmsg);
                        }
                    var printerName = null;
                    printerName = _scModelUtils.getStringValueFromPath("Shipment.PrinterName", outputData);
                    var bothPrint = null;
                    bothPrint = this.getSimpleBundleString("extn_ShippingLabel_PackSlip") + " " + "<font size='2px'>" + printerName + "</font>";
                    var packPrint = null;
                    packPrint = this.getSimpleBundleString("extn_Pack_Slip_Message") + " " + "<font size='2px'>" + printerName + "</font>";
                    var shipPrint = null;
                    shipPrint = this.getSimpleBundleString("extn_Shipping_Label_Message") + " " + "<font size='2px'>" + printerName + "</font>";
                    if (_scBaseUtils.and(_scBaseUtils.equals(shiplabelFlag, "Y"), _scBaseUtils.equals(packSlipFlag, "Y"))) {
                        _iasBaseTemplateUtils.showMessage(this, bothPrint, "information", null);
                    } else if (_scBaseUtils.equals(packSlipFlag, "Y")) {
                        _iasBaseTemplateUtils.showMessage(this, packPrint, "information", null);
                    } else if (_scBaseUtils.equals(shiplabelFlag, "Y")) {
                        _iasBaseTemplateUtils.showMessage(this, shipPrint, "information", null);
                    }
                }
            }
        },
        extn_WeightChangeEnter: function(event, bEvent, ctrl, args) {
            if (_iasEventUtils.isEnterPressed(event)) {
                this.extn_containerWeightChange();
            }
        },
		extn_GetWeight: function(event, bEvent, ctrl, args) {
		var container_Src = null;
        container_Src = _scScreenUtils.getModel(this, "container_Src");
		var targetModel = null;
		var packageWeight = 0;
        targetModel = _scBaseUtils.getTargetModel(this, "changeShipment_input", null);
        packageWeight = _scModelUtils.getNumberValueFromPath("Shipment.Containers.Container.ActualWeight", targetModel);
		if(_scBaseUtils.equals(
                    packageWeight, _scModelUtils.getNumberValueFromPath("Container.ActualWeight", container_Src)))
			{
				try{
					var xhr = new XMLHttpRequest();						
					xhr.open('GET', "http://localhost:8000", false);			
					xhr.send();
					packageWeight = parseFloat(xhr.responseText);
					if (_scBaseUtils.equals(packageWeight, 0)) {
						_wscContainerPackUtils.highlightWeightError(this, 
						"extn_GetWeightZeroWeight", "errorMsgPnl", "containerWeight");
						_scEventUtils.stopEvent(bEvent);
					}
					_scWidgetUtils.setValue(this, "containerWeight", packageWeight, false);
				}
				catch (error) {
						_wscContainerPackUtils.highlightWeightError(
						this, "extn_GetWeightTimeOut", "errorMsgPnl", "containerWeight");
						_scEventUtils.stopEvent(bEvent);
				}			
			}			
		},
        extn_containerWeightChange: function() {
            var container_Src = null;
            container_Src = _scScreenUtils.getModel(this, "container_Src");
            var targetModel = null;
            var packageWeight = 0;
            targetModel = _scBaseUtils.getTargetModel(this, "changeShipment_input", null);
            packageWeight = _scModelUtils.getNumberValueFromPath("Shipment.Containers.Container.ActualWeight", targetModel);
            if (_scBaseUtils.numberGreaterThan(packageWeight, 0)) {
                if (!_scBaseUtils.equals(packageWeight, _scModelUtils.getNumberValueFromPath("Container.ActualWeight", container_Src))) {
                    _scWidgetUtils.setLabel(this, "extn_printShippingLabel", this.getSimpleBundleString('extn_Print_Shipping_Label'));
                }
            }
        }
    });
});
