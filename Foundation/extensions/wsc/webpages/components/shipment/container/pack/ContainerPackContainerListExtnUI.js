scDefine(["dojo/text!./templates/ContainerPackContainerListExtn.html", "scbase/loader!dijit/form/Button", "scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/_base/lang", "scbase/loader!dojo/text", "scbase/loader!idx/form/NumberTextBox", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat", "scbase/loader!sc/plat/dojo/binding/ButtonDataBinder", "scbase/loader!sc/plat/dojo/binding/CurrencyDataBinder", "scbase/loader!sc/plat/dojo/binding/SimpleDataBinder", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/widgets/DataLabel"], function(templateText, _dijitButton, _dojodeclare, _dojokernel, _dojolang, _dojotext, _idxNumberTextBox, _idxContentPane, _scplat, _scButtonDataBinder, _scCurrencyDataBinder, _scSimpleDataBinder, _scBaseUtils, _scDataLabel) {
    return _dojodeclare("extn.components.shipment.container.pack.ContainerPackContainerListExtnUI", [], {
        templateString: templateText,
        hotKeys: [],
        events: [],
        subscribers: {
            local: [{
                eventId: 'afterScreenInit',
                sequence: '51',
                handler: {
                    methodName: "extn_afterScreenInit"
                }
            }, {
                eventId: 'containerWeight_onKeyUp',
                sequence: '19',
                handler: {
                    methodName: "extn_WeightChangeEnter"
                }
            }, {
                eventId: 'saveButton_onClick',
                sequence: '19',
                handler: {
                    methodName: "extn_containerWeightChange"
                }
            }, {
                eventId: 'extn_printShippingLabel_onClick',
                sequence: '51',
                handler: {
                    methodName: "extn_rePrintShippingLabel"
                }
            }, {
                eventId: 'extn_printPackSlip_onClick',
                sequence: '51',
                handler: {
                    methodName: "extn_printPackSlip"
                }
            }, {
                eventId: 'afterBehaviorMashupCall',
                sequence: '51',
                handler: {
                    methodName: "extn_afterBehaviorMashupCall"
                }
            },
				{
				eventId: 'saveButton_onClick',
				sequence: '18',
				description: 'extn_GetWeight',
				handler : {
					methodName : "extn_GetWeight"
				}
			}
		]
        }
    });
});
