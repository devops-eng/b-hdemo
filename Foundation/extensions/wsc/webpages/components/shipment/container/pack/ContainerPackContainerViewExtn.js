scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/components/shipment/container/pack/ContainerPackContainerViewExtnUI", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/widgets/Label", "scbase/loader!sc/plat/dojo/widgets/Link", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/GridxUtils", "scbase/loader!gridx/Grid", "scbase/loader!sc/plat/dojo/binding/GridxDataBinder", "scbase/loader!ias/utils/RepeatingScreenUtils", "scbase/loader!sc/plat/dojo/utils/RepeatingPanelUtils", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!sc/plat/dojo/utils/RequestUtils", "scbase/loader!ias/utils/ScreenUtils"], function(_dojodeclare, _extnContainerPackContainerViewExtnUI, _idxContentPane, _scBaseUtils, _scUserPrefs, _scWidgetUtils, _scEventUtils, _scLabel, _scLink, _scScreen, _scModelUtils, _iasUIUtils, _scScreenUtils, _iasBaseTemplateUtils, _scWidgetUtils, _scEventUtils, _scGridxUtils, _gridxGrid, _scGridxDataBinder, _iasRepeatingScreenUtils, _scRepeatingPanelUtils, _scUserprefs, _iasContextUtils, _extnExpicientUtils, _scRequestUtils, _iasScreenUtils) {
    return _dojodeclare("extn.components.shipment.container.pack.ContainerPackContainerViewExtn", [_extnContainerPackContainerViewExtnUI], {
        // custom code here
        selectedLevelOfService: null,
        extn_beforeScreenInit: function(event, bEvent, ctrl, args) {
            var getShipmentDetails_output = null;
            getShipmentDetails_output = _scScreenUtils.getModel(this, "getShipmentDetails_output");
            var shipmentContainerFlag = null;
            var enterpriseCode = _scModelUtils.getStringValueFromPath("Shipment.EnterpriseCode", getShipmentDetails_output);
            var apiInput = _scModelUtils.createNewModelObjectWithRootKey("CommonCode");
            _scModelUtils.setStringValueAtModelPath("CommonCode.OrganizationCode", enterpriseCode, apiInput);
            _scModelUtils.setStringValueAtModelPath("CommonCode.CodeType", "GIFTWRAP_INSTRUCTION", apiInput);
            shipmentContainerFlag = _scModelUtils.getStringValueFromPath("Shipment.ShipmentContainerizedFlag", getShipmentDetails_output);
            //    var apiInput = _scModelUtils.createNewModelObjectWithRootKey("Container");
            //  _iasUIUtils.callApi(this, apiInput, "extn_containerList_ref", null);
            if (!_scBaseUtils.equals(shipmentContainerFlag, "03")) {
                this.extn_refreshShipmentDetails();
            }
        },
        extn_afterScreenInit: function(event, bEvent, ctrl, args) {
            var getShipmentDetails_output = null;
            getShipmentDetails_output = _scScreenUtils.getModel(this, "getShipmentDetails_output");
            var shipmentContainerFlag = null;
            shipmentContainerFlag = _scModelUtils.getStringValueFromPath("Shipment.ShipmentContainerizedFlag", getShipmentDetails_output);
            var model = _scScreenUtils.getModel(this, "getContainerList_Out_Src_Pg");
            // _scUserPrefs.setProperty("Model", model);
            //var parent = _iasUIUtils.getParentScreen(this, false);
            /*  if (_scBaseUtils.equals(parent.previous, "Y")) {
                  var model_temp = _scUserPrefs.getProperty("Model");
                  var changeServiceFlag = _scModelUtils.getStringValueFromPath("Page.Output.Containers.DisableChangeServiceFlag", model_temp);
                  }
                  else {*/
            var changeServiceFlag = _scModelUtils.getStringValueFromPath("Page.Output.Containers.DisableChangeServiceFlag", model);
            var checkBoxPreSelected = _scModelUtils.getStringValueFromPath("Page.Output.Containers.CheckBoxPreSelected", model);
            var disableCheckBox = _scModelUtils.getStringValueFromPath("Page.Output.Containers.DisableSignReqdBox", model);            
            var shipmentLineList = _scModelUtils.getStringValueFromPath("Page.Output.Containers.Container", model);
            if (!_scBaseUtils.isVoid(shipmentLineList)) {
                var flag = false;
                for (var i = 0; i < shipmentLineList.length; i++) {
                    var referenceModel = null;
                    referenceModel = _scBaseUtils.getArrayItemByIndex(shipmentLineList, i);
                    var labelGenerated = null;
                    labelGenerated = _scModelUtils.getStringValueFromPath("Extn.ExtnIsShipLabelGenerated", referenceModel);
                    if (_scBaseUtils.equals(labelGenerated, "Y")) {
                        flag = true;
                        break;
                    }
                }
                if (_scBaseUtils.or(_scBaseUtils.equals(flag, false), (_scBaseUtils.equals(shipmentContainerFlag, "01")))) {
                    _scWidgetUtils.enableWidget(this, "extn_changeLos");
					_scWidgetUtils.enableWidget(this, "extn_signatureCheck");
                } else {
                    _scWidgetUtils.disableWidget(this, "extn_changeLos");
                    _scWidgetUtils.disableWidget(this, "extn_signatureCheck");
                }
            }
            if (_scBaseUtils.equals(changeServiceFlag, "Y")) {
                _scWidgetUtils.disableWidget(this, "extn_changeLos", null);
            }
            if (_scBaseUtils.equals(disableCheckBox, "Y")) {
                _scWidgetUtils.disableWidget(this, "extn_signatureCheck", null);
            }
            //    parent.previous = "Y";
            //  var enterpriseCode = _scModelUtils.getStringValueFromPath("Shipment.EnterpriseCode", getShipmentDetails_output);
            //_scModelUtils.setStringValueAtModelPath("CommonCode.OrganizationCode", enterpriseCode, apiInput);
            //_scModelUtils.setStringValueAtModelPath("CommonCode.CodeType", "GIFTWRAP_INSTRUCTION", apiInput);
        },
        extn_beforeBehaviorMashupCall: function(event, bEvent, ctrl, args) {
            var isMashupAvailable = _extnExpicientUtils.isBeforeBehaviorMashupAvailable("containerPack_changeShipmentForWeight", args);
            if (isMashupAvailable) {
                var inputData = null;
                inputData = _extnExpicientUtils.getMashupInputForRefID("containerPack_changeShipmentForWeight", args);
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.Extn.ExtnIsShipLabelGenerated", "N", inputData);
            }
        },
        extn_allItemPackedMessage: function(event, bEvent, ctrl, args) {
            _iasBaseTemplateUtils.showMessage(this.getOwnerScreen(), "Message_PackCompleted", "information", null);
        },
        extn_setLevelOfService: function(event, bEvent, ctrl, args) {
            var model = _scScreenUtils.getModel(this.getOwnerScreen(), "getShipmentDetails_output");
            var levelOfService = _scModelUtils.getStringValueFromPath("Shipment.ScacAndService", model);
            if (!_scBaseUtils.isVoid(levelOfService)) {
                return levelOfService;
            }
        },
        extn_changeLosService: function(event, bEvent, ctrl, args) {
            var popupParams = _scBaseUtils.getNewBeanInstance();
            var dialogParams = _scBaseUtils.getNewBeanInstance();
            dialogParams["closeCallBackHandler"] = "onCloseCallSelection";
            _iasUIUtils.openSimplePopup("extn.customScreen.LevelOfService", "Change Shipping Service Level", this, popupParams, dialogParams);
        },
        onCloseCallSelection: function(actionPerformed, model, popupParams) {
            if (_scBaseUtils.equals(actionPerformed, "SETVAL")) {
                if (!_scBaseUtils.isVoid(this.selectedLevelOfService)) {
                    var model = _scScreenUtils.getModel(this, "getShipmentDetails_output");
                    _scModelUtils.setStringValueAtModelPath("Shipment.ScacAndService", this.selectedLevelOfService, model);
                    _scScreenUtils.setModel(this, "getShipmentDetails_output", model);
                }
            }
        },
        extn_afterBehaviorMashupCall: function(event, bEvent, ctrl, args) {
            /* var isMashupAvailable1 = _extnExpicientUtils.isMashupAvailable("extn_containerList_ref", args);
             if (isMashupAvailable1) {
                 var outputData = null;
                 outputData = _extnExpicientUtils.getMashupOutputForRefID("extn_containerList_ref", args);
                 containerList = _scModelUtils.getStringValueFromPath("Containers.Container", outputData);
                 _scUserPrefs.setProperty("ContainerList", containerList);*/
            //  }
            var isMashupAvailableSignature = _extnExpicientUtils.isMashupAvailable("extn_signatureChangeShipment_ref", args);
            if (isMashupAvailableSignature) {
                var outputData = null;
                outputData = _extnExpicientUtils.getMashupOutputForRefID("extn_signatureChangeShipment_ref", args);
            }
            var isMashupAvailable = _extnExpicientUtils.isMashupAvailable("containerPack_getShipmentContainerList_pg", args);
            if (isMashupAvailable) {
                var outputData = null;
                outputData = _extnExpicientUtils.getMashupOutputForRefID("containerPack_getShipmentContainerList_pg", args);
                _scScreenUtils.setModel(this, "getContainerList_Out_Src_Pg", outputData);
                _scScreenUtils.setModel(this.getOwnerScreen(), "getShipmentContainerList_output", outputData.Page.Output, null);
                _scUserPrefs.setProperty("TempModel", outputData);
                var getShipmentDetails_output = null;
                getShipmentDetails_output = _scScreenUtils.getModel(this, "getShipmentDetails_output");
                var shipmentContainerFlag = null;
                shipmentContainerFlag = _scModelUtils.getStringValueFromPath("Shipment.ShipmentContainerizedFlag", getShipmentDetails_output);
                var shipmentLineList = _scModelUtils.getStringValueFromPath("Page.Output.Containers.Container", outputData);
                if (!_scBaseUtils.isVoid(shipmentLineList)) {
                    var flag = false;
                    for (var i = 0; i < shipmentLineList.length; i++) {
                        var referenceModel = null;
                        referenceModel = _scBaseUtils.getArrayItemByIndex(shipmentLineList, i);
                        var labelGenerated = null;
                        labelGenerated = _scModelUtils.getStringValueFromPath("Extn.ExtnIsShipLabelGenerated", referenceModel);
                        if (_scBaseUtils.equals(labelGenerated, "Y")) {
                            flag = true;
                            break;
                        }
                    }
                    if (_scBaseUtils.or(_scBaseUtils.equals(flag, false), (_scBaseUtils.equals(shipmentContainerFlag, "01")))) {
                        _scWidgetUtils.enableWidget(this, "extn_changeLos");
						_scWidgetUtils.enableWidget(this, "extn_signatureCheck");
                    } else {
                        _scWidgetUtils.disableWidget(this, "extn_changeLos");
						_scWidgetUtils.disableWidget(this, "extn_signatureCheck");
                    }
                } else {
                    _scWidgetUtils.enableWidget(this, "extn_changeLos");
					_scWidgetUtils.enableWidget(this, "extn_signatureCheck");
                }
                var model = _scScreenUtils.getModel(this, "getContainerList_Out_Src_Pg");
                //   var parent = _iasUIUtils.getParentScreen(this, false);
                var model1 = _scUserPrefs.getProperty("TempModel");
                if (_scBaseUtils.isVoid(model1)) {
                    var changeServiceFlag = _scModelUtils.getStringValueFromPath("Page.Output.Containers.DisableChangeServiceFlag", model);
                } else {
                    var changeServiceFlag = _scModelUtils.getStringValueFromPath("Page.Output.Containers.DisableChangeServiceFlag", model1);
                }
                if (_scBaseUtils.equals(changeServiceFlag, "Y")) {
                    _scWidgetUtils.disableWidget(this, "extn_changeLos", null);
                }
                var disableCheckBox = _scModelUtils.getStringValueFromPath("Page.Output.Containers.DisableSignReqdBox", outputData);
                if (_scBaseUtils.equals(disableCheckBox, "Y")) {
                    _scWidgetUtils.disableWidget(this, "extn_signatureCheck", null);
                }
            }
            var weightMashup = _extnExpicientUtils.isMashupAvailable("containerPack_changeShipmentForWeight", args);
            if (weightMashup) {
                var outputData = null;
                outputData = _extnExpicientUtils.getMashupOutputForRefID("containerPack_changeShipmentForWeight", args);
                var model = null;
                model = _scBaseUtils.getNewModelInstance();
                _scEventUtils.fireEventInsideScreen(this, "refreshScreenData", null, model);
            }
            var detailsMashup = _extnExpicientUtils.isMashupAvailable("extn_containerPack_getShipmentDetails", args);
            if (detailsMashup) {
                var outputData = null;
                outputData = _extnExpicientUtils.getMashupOutputForRefID("extn_containerPack_getShipmentDetails", args);
                _scScreenUtils.setModel(this.getOwnerScreen(), "getShipmentContainerList_output", outputData.Shipment, null);
                var totalRecords = null;
                totalRecords = _scModelUtils.getStringValueFromPath("Shipment.Containers.TotalNumberOfRecords", outputData);
                if (!_scBaseUtils.isVoid(totalRecords)) {
                    if (!_scBaseUtils.numberGreaterThan(totalRecords, 0)) {
                        _scWidgetUtils.enableWidget(this, "extn_changeLos");
                    }
                }
                var model = _scScreenUtils.getModel(this, "getContainerList_Out_Src_Pg");
                //  var parent = _iasUIUtils.getParentScreen(this, false);
                var model1 = _scUserPrefs.getProperty("TempModel");
                if (_scBaseUtils.isVoid(model1)) {
                    var changeServiceFlag = _scModelUtils.getStringValueFromPath("Page.Output.Containers.DisableChangeServiceFlag", model);
                } else {
                    var changeServiceFlag = _scModelUtils.getStringValueFromPath("Page.Output.Containers.DisableChangeServiceFlag", model1);
                }
                if (_scBaseUtils.equals(changeServiceFlag, "Y")) {
                    _scWidgetUtils.disableWidget(this, "extn_changeLos", null);
                }
                var disableCheckBox = _scModelUtils.getStringValueFromPath("Page.Output.Containers.DisableSignReqdBox", model);
                if (_scBaseUtils.equals(disableCheckBox, "Y")) {
                    _scWidgetUtils.disableWidget(this, "extn_signatureCheck", null);
                }
                _scScreenUtils.setModel(this, "getShipmentDetails_output", outputData);
            }
        },
        extn_refreshShipmentDetails: function(event, bEvent, ctrl, args) {
            var getShipmentDetails_output = null;
            getShipmentDetails_output = _scScreenUtils.getModel(this, "getShipmentDetails_output");
            var shipmentKey = null;
            shipmentKey = _scModelUtils.getStringValueFromPath("Shipment.ShipmentKey", getShipmentDetails_output);
            if (!_scBaseUtils.isVoid(shipmentKey)) {
                var shipModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
                _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", shipmentKey, shipModel);
                _iasUIUtils.callApi(this, shipModel, "extn_containerPack_getShipmentDetails", null);
            }
        },
        extn_checkBoxClicked: function(value, bEvent, ctrl, args) {
            var getShipmentDetails_output = null;
            getShipmentDetails_output = _scScreenUtils.getModel(this, "getShipmentDetails_output");
            var shipmentKey = null;
            shipmentKey = _scModelUtils.getStringValueFromPath("Shipment.ShipmentKey", getShipmentDetails_output);
            var shipModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
            _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", shipmentKey, shipModel);
            if (value == true) {
                _scModelUtils.setStringValueAtModelPath("Shipment.SpecialServices.SpecialService.Action", "Create", shipModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.SpecialServices.SpecialService.SpecialServicesCode", "INDSIGN", shipModel);
            } else {
                _scModelUtils.setStringValueAtModelPath("Shipment.SpecialServices.SpecialService.Action", "Delete", shipModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.SpecialServices.SpecialService.SpecialServicesCode", "INDSIGN", shipModel);
            }
            _iasUIUtils.callApi(this, shipModel, "extn_signatureChangeShipment_ref", null);
        }
    });
});