


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/components/shipment/container/pack/ContainerPackContainerViewExtn","scbase/loader!sc/plat/dojo/controller/ExtnServerDataController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnContainerPackContainerViewExtn
			 ,
			    _scExtnServerDataController
){

return _dojodeclare("extn.components.shipment.container.pack.ContainerPackContainerViewExtnBehaviorController", 
				[_scExtnServerDataController], {

			
			 screenId : 			'extn.components.shipment.container.pack.ContainerPackContainerViewExtn'

			
			
			
			
			
						,

			
			
			 mashupRefs : 	[
	 		{
		 extnType : 			'MODIFY'
,
		 mashupId : 			'pack_getShipmentContainerDetails'
,
		 mashupRefId : 			'pack_getShipmentContainerDetails'

	}
,
	 		{
		 extnType : 			'ADD'
,
		 mashupId : 			'containerPack_getShipmentDetails'
,
		 mashupRefId : 			'extn_containerPack_getShipmentDetails'

	}
,
	 		{
		 extnType : 			'ADD'
,
		 mashupId : 			'extn_commonCodeList'
,
		 mashupRefId : 			'extn_commonCodeList_refId'
}
,
{
		 extnType : 			'ADD'
,
		 mashupId : 			'extn_signatureChangeShipment_ref'
,
		 mashupRefId : 			'extn_signatureChangeShipment_ref'

	}

	]

}
);
});

