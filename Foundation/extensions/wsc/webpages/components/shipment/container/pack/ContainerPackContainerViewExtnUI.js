scDefine(["dojo/text!./templates/ContainerPackContainerViewExtn.html", "scbase/loader!dijit/form/Button", "scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/_base/lang", "scbase/loader!dojo/text", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat", "scbase/loader!sc/plat/dojo/binding/ButtonDataBinder", "scbase/loader!sc/plat/dojo/binding/CurrencyDataBinder", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/widgets/DataLabel"], function(templateText, _dijitButton, _dojodeclare, _dojokernel, _dojolang, _dojotext, _idxContentPane, _scplat, _scButtonDataBinder, _scCurrencyDataBinder, _scBaseUtils, _scDataLabel) {
    return _dojodeclare("extn.components.shipment.container.pack.ContainerPackContainerViewExtnUI", [], {
        templateString: templateText,	
		namespaces : {
			targetBindingNamespaces :
		[
			{
			value: 'extn_signature'
			,
			scExtensibilityArrayItemId: 'extn_TargetNamespaces_8'			
			}
		],
		sourceBindingNamespaces :
		[
		]
		}	,
        hotKeys: [],
        events: [],
        subscribers: {
            local: [{
                eventId: 'refreshScreenData',
                sequence: '19',
                handler: {
                    methodName: "extn_refreshShipmentDetails"
                }
            }, {
                eventId: 'deleteContainer',
                sequence: '51',
                handler: {
                    methodName: "extn_refreshShipmentDetails"
                }
            }, {
                eventId: 'afterScreenInit',
                sequence: '19',
                handler: {
                    methodName: "extn_beforeScreenInit"
                }
            }, {
                eventId: 'afterScreenInit',
                sequence: '51',
                handler: {
                    methodName: "extn_afterScreenInit"
                }
            }, {
                eventId: 'beforeBehaviorMashupCall',
                sequence: '51',
                handler: {
                    methodName: "extn_beforeBehaviorMashupCall"
                }
            }, {
                eventId: 'extn_changeLos_onClick',
                sequence: '51',
                handler: {
                    methodName: "extn_changeLosService"
                }
            }, {
                eventId: 'afterBehaviorMashupCall',
                sequence: '51',
                handler: {
                    methodName: "extn_afterBehaviorMashupCall"
                }
            }, {
				eventId: 'extn_signatureCheck_onChange',
				sequence: '51',
				handler : {
					methodName : "extn_checkBoxClicked"
				}
			}
			],
            global: [{
                eventId: 'extn_allItemPacked',
                sequence: '51',
                handler: {
                    methodName: "extn_allItemPackedMessage"
                }
            }]
        }
    });
});
