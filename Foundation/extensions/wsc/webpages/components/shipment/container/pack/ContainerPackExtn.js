scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/components/shipment/container/pack/ContainerPackExtnUI", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/EditorUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!ias/utils/ScreenUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!ias/utils/RelatedTaskUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!ias/utils/ModelUtils", "scbase/loader!ias/utils/ContextUtils"], function(_dojodeclare, _extnContainerPackExtnUI, _scBaseUtils, _scScreenUtils, _scModelUtils, _iasUIUtils, _scEventUtils, scEditorUtils, _scWidgetUtils, _iasScreenUtils, _extnExpicientUtils, _iasRelatedTaskUtils, _iasBaseTemplateUtils, _iasModelUtils, _iasContextUtils) {
    return _dojodeclare("extn.components.shipment.container.pack.ContainerPackExtn", [_extnContainerPackExtnUI], {
        // custom code here
        extn_customSave: function(event, bEvent, widget_uId, args) {
            var targetModel = null;
            targetModel = _scBaseUtils.getTargetModel(this, "getShipmentContainerList_input", null);
            _iasUIUtils.callApi(this, targetModel, "extn_containerPack_getShipmentContainerList_NoScac", null);
            _scEventUtils.stopEvent(bEvent);
        },
        printBufferMsg: function(bufferMsg) {
            var iframe = document.createElement('iframe');
            var myDiv = document.getElementById("extensibilityShellHolder");
            myDiv.appendChild(iframe);
            var html = '<HTML><BODY style="margin: 20px;">';
            html += bufferMsg;
            html += '<object id="factory" style="display:none" classid="clsid:1663ed61-23eb-11d2-b92f-008048fdd814" codebase="/extnloader/smsx.cab#Version=7.7.0.20"><param name="template" value="MeadCo://IE7" /></object>';
            html += '<script>function silentPrintPage(){factory.printing.Print(false)}</script>';
            html += '<div id="print" onclick="silentPrintPage();"></div>';
            html += '</BODY></HTML>';
            iframe.contentWindow.document.open();
            iframe.contentWindow.document.write(html);
            var printbtn = iframe.contentWindow.document.getElementById("print");
            printbtn.click();
            iframe.contentWindow.document.close();
            myDiv.removeChild(iframe);
        },
        extn_afterBehaviorMashupCall: function(event, bEvent, widget_uId, args) {
            var nonScacMashupAvailable = _extnExpicientUtils.isMashupAvailable("extn_containerPack_getShipmentContainerList_NoScac", args);
            if (nonScacMashupAvailable) {
                var outputData = null;
                outputData = _extnExpicientUtils.getMashupOutputForRefID("extn_containerPack_getShipmentContainerList_NoScac", args);
                var containerizedFlag = 0;
                containerizedFlag = _scModelUtils.getNumberValueFromPath("Containers.ShipmentContainerizedFlag", outputData);
                var totalRecords = 0;
                totalRecords = _scModelUtils.getNumberValueFromPath("Containers.TotalNumberOfRecords", outputData);
                if (_scBaseUtils.or(!_scBaseUtils.equals(containerizedFlag, 3), !_scBaseUtils.equals(totalRecords, 0))) {
                    _iasBaseTemplateUtils.showMessage(this, "extn_Not_Packed", "information", null);
                } else {
                    var targetModel = null;
                    targetModel = _scBaseUtils.getTargetModel(this, "getShipmentContainerList_input", null);
                    _iasUIUtils.callApi(this, targetModel, "extn_getContainerExtendedFlags_ref", null);
                }
                var fdf = null;
                var fedf = null;
            }
            var printMashupAvailable = _extnExpicientUtils.isMashupAvailable("extn_hbcPackSlip_ref", args);
            if (printMashupAvailable) {
                var targetModel = null;
                targetModel = _scBaseUtils.getTargetModel(this, "changeShipmentStatus_input", null);
                _iasUIUtils.callApi(this, targetModel, "extn_changeShipmentStatus_ref", null);
                var outputData = null;
                outputData = _extnExpicientUtils.getMashupOutputForRefID("extn_hbcPackSlip_ref", args);
                var printMessage = null;
                var usbPrinterFlag = _scModelUtils.getStringValueFromPath("Shipment.USBPrinter", outputData);
                if (_scBaseUtils.equals(usbPrinterFlag, "Y")) {
                    var printers = outputData.Shipment.Printers.Printer;
                    for (var i = 0; i < printers.length; i++) {
                        var printer = _scBaseUtils.getArrayItemByIndex(printers, i);
                        var buffermsg = printer.PrintBuffer;
                        this.printBufferMsg(buffermsg);
                    }
                }
                printMessage = _scModelUtils.getStringValueFromPath("Shipment.PrintMessage", outputData);
                if (!_scBaseUtils.isVoid(printMessage)) {
                    _iasScreenUtils.showInfoMessageBoxWithOk(this, printMessage, null, null);
                }
                /* var shiplabelFlag = null;
                 shiplabelFlag = _scModelUtils.getStringValueFromPath("Shipment.ShipLabelPrinted", outputData);
                 var packSlipFlag = null;
                 packSlipFlag = _scModelUtils.getStringValueFromPath("Shipment.PackSlipPrinted", outputData);
                 var errorCode = null;
                 errorCode = _scModelUtils.getStringValueFromPath("Shipment.ErrorCode", outputData);
                 if (!_scBaseUtils.isVoid(errorCode)) {
                     var errorDesc = null;
                     errorDesc = _scModelUtils.getStringValueFromPath("Shipment.ErrorDescription", outputData);
                     _iasBaseTemplateUtils.showMessage(this, errorDesc, "error", null);
                 } else {
                     var printerName = null;
                     printerName = _scModelUtils.getStringValueFromPath("Shipment.PrinterName", outputData);
                     var bothPrint = null;
                     bothPrint = this.getSimpleBundleString("extn_ShippingLabel_PackSlip") + " " + "<font size='2px'>" + printerName + "</font>";
                     var packPrint = null;
                     packPrint = this.getSimpleBundleString("extn_Pack_Slip_Message") + " " + "<font size='2px'>" + printerName + "</font>";
                     var shipPrint = null;
                     shipPrint = this.getSimpleBundleString("extn_Shipping_Label_Message") + " " + "<font size='2px'>" + printerName + "</font>";
                     if (_scBaseUtils.and(_scBaseUtils.equals(shiplabelFlag, "Y"), _scBaseUtils.equals(packSlipFlag, "Y"))) {
                         _iasScreenUtils.showInfoMessageBoxWithOk(this, bothPrint, null, null);
                     } else if (_scBaseUtils.equals(packSlipFlag, "Y")) {
                         _iasScreenUtils.showInfoMessageBoxWithOk(this, packPrint, null, null);
                     } else if (_scBaseUtils.equals(shiplabelFlag, "Y")) {
                         _iasScreenUtils.showInfoMessageBoxWithOk(this, shipPrint, null, null);
                     }
                 }*/
            }
            var statusMashupAvailable = _extnExpicientUtils.isMashupAvailable("extn_changeShipmentStatus_ref", args);
            if (statusMashupAvailable) {
                var outputData = null;
                outputData = _extnExpicientUtils.getMashupOutputForRefID("extn_changeShipmentStatus_ref", args);
                this.extn_openShipmentSummary("No");
            }
            var isContainerAvailable = _extnExpicientUtils.isMashupAvailable("extn_getContainerExtendedFlags_ref", args);
            if (isContainerAvailable) {
                var outputData = null;
                outputData = _extnExpicientUtils.getMashupOutputForRefID("extn_getContainerExtendedFlags_ref", args);
                var containerModel = null;
                containerModel = _scModelUtils.getModelListFromPath("Containers.Container", outputData);
                var shipmentKeyModel = null;
                shipmentKeyModel = _scBaseUtils.getArrayItemByIndex(containerModel, 0);
                var shipmentKey = _scModelUtils.getStringValueFromPath("ShipmentKey", shipmentKeyModel);
                var shipLabelErrorFlag = false;
                var packSlipErrorFlag = false;
                if (!_scBaseUtils.isVoid(containerModel)) {
                    var referenceArray = [];
                    for (var i = 0; i < containerModel.length; i++) {
                        var referenceModel = null;
                        referenceModel = _scBaseUtils.getArrayItemByIndex(containerModel, i);
                        var isPackingCompleted = null;
                        isPackingCompleted = _scModelUtils.getStringValueFromPath("Extn.ExtnIsPackingCompleted", referenceModel);
                        var isShipLabelGenerated = null;
                        isShipLabelGenerated = _scModelUtils.getStringValueFromPath("Extn.ExtnIsShipLabelGenerated", referenceModel);
                        if (_scBaseUtils.equals(isPackingCompleted, "Y")) {
                            _scModelUtils.setStringValueAtModelPath("PrintPackSlip", "N", referenceModel);
                        } else if (_scBaseUtils.equals(isPackingCompleted, "N")) {
                            _scModelUtils.setStringValueAtModelPath("PrintPackSlip", "Y", referenceModel);
                            packSlipErrorFlag = true;
                        }
                        if (_scBaseUtils.equals(isShipLabelGenerated, "Y")) {
                            _scModelUtils.setStringValueAtModelPath("PrintShippingLabel", "N", referenceModel);
                        } else if (_scBaseUtils.equals(isShipLabelGenerated, "N")) {
                            _scModelUtils.setStringValueAtModelPath("PrintShippingLabel", "Y", referenceModel);
                            shipLabelErrorFlag = true;
                        }
                        _scModelUtils.setStringValueAtModelPath("RePrintPackSlip", "N", referenceModel);
                        _scModelUtils.setStringValueAtModelPath("RePrintShippingLabel", "N", referenceModel);
                        referenceArray.push(referenceModel);
                    }
                    var cloneReferenceModel = null;
                    cloneReferenceModel = _scBaseUtils.cloneModel(referenceArray);
                    if (!_scBaseUtils.isVoid(cloneReferenceModel)) {
                        var shipmentModel = null;
                        shipmentModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
                        _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", shipmentKey, shipmentModel);
                        for (var j = 0; j < cloneReferenceModel.length; j++) {
                            var packModel = null;
                            packModel = _scBaseUtils.getArrayItemByIndex(cloneReferenceModel, j);
                            _iasModelUtils.removeAttributeFromModel("ActualWeight", packModel);
                            _iasModelUtils.removeAttributeFromModel("ShipmentKey", packModel);
                        }
                        _scModelUtils.setStringValueAtModelPath("Shipment.ScacIntegrationRequired", this.scacIntegrationReqd, shipmentModel);
                        _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container", cloneReferenceModel, shipmentModel);
                        if (_scBaseUtils.or(_scBaseUtils.equals(shipLabelErrorFlag, true), _scBaseUtils.equals(packSlipErrorFlag, true))) {
                            _scEventUtils.stopEvent(bEvent);
                            _iasBaseTemplateUtils.showMessage(this, "extn_Not_Print_Ship_Label", "information", null);
                        } else {
                            _iasUIUtils.callApi(this, shipmentModel, "extn_hbcPackSlip_ref", null);
                        }
                    }
                }
            }
        },
        extn_openShipmentSummary: function(printReq) {
            var initialInput = null;
            initialInput = {};
            _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", _scModelUtils.getStringValueFromPath("Shipment.ShipmentKey", _scScreenUtils.getModel(this, "getShipmentDetails_output")), initialInput);
            if (!(_scBaseUtils.isVoid(printReq))) {
                _scModelUtils.setStringValueAtModelPath("Shipment.isPackSlipPrintRequired", printReq, initialInput);
            }
            if (_iasContextUtils.gotoSummaryOnCompleteRule()) {
                _iasUIUtils.openWizardInEditor("wsc.components.shipment.summary.ShipmentSummaryWizard", initialInput, "wsc.desktop.editors.ShipmentEditor", this);
            } else {
                this.gotoHomePage();
            }
        }
    });
});
