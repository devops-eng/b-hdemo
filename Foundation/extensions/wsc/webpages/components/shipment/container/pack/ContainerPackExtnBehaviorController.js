scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/text", "scbase/loader!extn/components/shipment/container/pack/ContainerPackExtn", "scbase/loader!sc/plat/dojo/controller/ExtnServerDataController"], function(_dojodeclare, _dojokernel, _dojotext, _extnContainerPackExtn, _scExtnServerDataController) {
    return _dojodeclare("extn.components.shipment.container.pack.ContainerPackExtnBehaviorController", [_scExtnServerDataController], {
        screenId: 'extn.components.shipment.container.pack.ContainerPackExtn',
        mashupRefs: [{
            extnType: 'ADD',
            mashupId: 'extn_getContainerExtendedFlags',
            mashupRefId: 'extn_getContainerExtendedFlags_ref'
        }, {
            extnType: 'ADD',
            mashupId: 'extn_getShipmentDetails',
            mashupRefId: 'extn_getShipmentDetails_ref'
        }, {
            extnType: 'ADD',
            mashupId: 'extn_hbcPackSlip',
            mashupRefId: 'extn_hbcPackSlip_ref'
        }, {
            extnType: 'ADD',
            mashupId: 'containerPacking_changeShipment',
            mashupRefId: 'extn_changeShipmentStatus_ref'
        }, {
            extnType: 'ADD',
            mashupId: 'containerPack_getShipmentContainerList_NoScac',
            mashupRefId: 'extn_containerPack_getShipmentContainerList_NoScac'
        }]
    });
});
