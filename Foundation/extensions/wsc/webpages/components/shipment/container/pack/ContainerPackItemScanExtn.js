scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/components/shipment/container/pack/ContainerPackItemScanExtnUI", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/widgets/Label", "scbase/loader!sc/plat/dojo/widgets/Link", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/GridxUtils", "scbase/loader!gridx/Grid", "scbase/loader!sc/plat/dojo/binding/GridxDataBinder", "scbase/loader!ias/utils/RepeatingScreenUtils", "scbase/loader!sc/plat/dojo/utils/RepeatingPanelUtils", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!sc/plat/dojo/utils/RequestUtils", "scbase/loader!ias/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/EditorUtils", "scbase/loader!dojo/dom-class"], function(_dojodeclare, _extnContainerPackItemScanExtnUI, _idxContentPane, _scBaseUtils, _scWidgetUtils, _scEventUtils, _scLabel, _scLink, _scScreen, _scModelUtils, _iasUIUtils, _scScreenUtils, _iasBaseTemplateUtils, _scWidgetUtils, _scEventUtils, _scGridxUtils, _gridxGrid, _scGridxDataBinder, _iasRepeatingScreenUtils, _scRepeatingPanelUtils, _scUserprefs, _iasContextUtils, _extnExpicientUtils, _scRequestUtils, _iasScreenUtils, _scEditorUtils, dDomClass) {
    return _dojodeclare("extn.components.shipment.container.pack.ContainerPackItemScanExtn", [_extnContainerPackItemScanExtnUI], {
        // custom code here
        extn_showScanAndProductsTab: function(event, bEvent, ctrl, args) {
            var eventName = null;
            eventName = _scModelUtils.getStringValueFromPath("_id", bEvent);
            if (_scBaseUtils.equals(eventName, "refreshScreenData")) {
                var currEditor = _scEditorUtils.getCurrentEditor();
                var screenOpener = _scModelUtils.getStringValueFromPath("_initialCallbackData.oldScreen.className", currEditor);
                var ProductsScreen = null;
                ProductsScreen = _scScreenUtils.getChildScreen(this, "ContainerPackProductList");
                if (!(_scBaseUtils.isVoid(ProductsScreen))) {
                    _scScreenUtils.destroyDynamicScreen(this, "ContainerPackProductList");
                }
                var options = null;
                options = {};
                options["screen"] = this;
                options["createdUId"] = "ContainerPackProductList";
                _scScreenUtils.showChildScreen(this, "ContainerPackProductList", null, "", options, null);
                this.isProductsPainted = "Y";
                _scWidgetUtils.setLinkImageSrc(this, "expandOrCollapseLink", "wsc/resources/css/icons/images/arrowUp.png");
                _scWidgetUtils.showWidget(this, "scanDataForm", false, "");
                _scWidgetUtils.setLinkImageSrc(this, "expandOrCollapseLnk", "wsc/resources/css/icons/images/arrowUp.png");
                this.isScanPanelPainted = "Y";
                if (this.isHidden == false) {
                    dDomClass.remove(this.domNode.parentNode.parentNode, "scHidden");
                }
            } else if (_scBaseUtils.equals(eventName, "afterScreenInit")) {
                var currEditor = _scEditorUtils.getCurrentEditor();
                var screenOpener = _scUserprefs.getProperty("customPrevWizard");
                if (!_scBaseUtils.or(_scBaseUtils.equals(screenOpener, "ItemPack"), _scBaseUtils.equals(screenOpener, "PackProductScan"))) {
                    var ProductsScreen = null;
                    ProductsScreen = _scScreenUtils.getChildScreen(this, "ContainerPackProductList");
                    if (!(_scBaseUtils.isVoid(ProductsScreen))) {
                        _scScreenUtils.destroyDynamicScreen(this, "ContainerPackProductList");
                    }
                    var options = null;
                    options = {};
                    options["screen"] = this;
                    options["createdUId"] = "ContainerPackProductList";
                    _scScreenUtils.showChildScreen(this, "ContainerPackProductList", null, "", options, null);
                    this.isProductsPainted = "Y";
                    _scWidgetUtils.setLinkImageSrc(this, "expandOrCollapseLink", "wsc/resources/css/icons/images/arrowUp.png");
                    _scWidgetUtils.showWidget(this, "scanDataForm", false, "");
                    _scWidgetUtils.setLinkImageSrc(this, "expandOrCollapseLnk", "wsc/resources/css/icons/images/arrowUp.png");
                    this.isScanPanelPainted = "Y";
                }
            }
        },
        extn_beforeBehaviorMashupCall: function(event, bEvent, ctrl, args) {
            var isMashupvailable = _extnExpicientUtils.isBeforeBehaviorMashupAvailable("containerPack_packAll", args);
            if (isMashupvailable) {
                var inputData = null;
                inputData = _extnExpicientUtils.getMashupInputForRefID("containerPack_packAll", args);
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.Extn.ExtnIsPackingCompleted", "N", inputData);
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.Extn.ExtnIsShipLabelGenerated", "N", inputData);
            }
            var isMashupAvailable = _extnExpicientUtils.isBeforeBehaviorMashupAvailable("containerPack_changeShipmentCall", args);
            if (isMashupAvailable) {
                var inputData = null;
                inputData = _extnExpicientUtils.getMashupInputForRefID("containerPack_changeShipmentCall", args);
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.Extn.ExtnIsPackingCompleted", "N", inputData);
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.Extn.ExtnIsShipLabelGenerated", "N", inputData);
            }
            var isDraftMashupAvailable = _extnExpicientUtils.isBeforeBehaviorMashupAvailable("pack_changeShipmentForDraftContainer", args);
            if (isDraftMashupAvailable) {
                var inputData1 = null;
                inputData1 = _extnExpicientUtils.getMashupInputForRefID("pack_changeShipmentForDraftContainer", args);
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.Extn.ExtnIsPackingCompleted", "N", inputData1);
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.Extn.ExtnIsShipLabelGenerated", "N", inputData1);
            }
        },
        extn_showScan: function(event, bEvent, ctrl, args) {
            _scWidgetUtils.showWidget(this, "scanDataForm", false, "");
            _scWidgetUtils.setLinkImageSrc(this, "expandOrCollapseLnk", "wsc/resources/css/icons/images/arrowUp.png");
            this.isScanPanelPainted = "Y";
        },
        extn_scrollToScan: function(event, bEvent, ctrl, args) {
            _iasScreenUtils.scrollToWidgetTop(null, null, "scanTogglePanel", args);
            _scWidgetUtils.setFocusOnWidgetUsingUid(this, "txtScanField");
        },
        extn_afterBehaviorMashupCall: function(event, bEvent, ctrl, args) {
            var isMashup1Available = _extnExpicientUtils.isMashupAvailable("containerPack_changeShipmentCall", args);
            if (isMashup1Available) {
                var productChild = _scScreenUtils.getChildScreen(this, "ContainerPackProductList");
                var view = productChild.currentView;
                if (_scBaseUtils.equals(view, "All")) {
                    productChild.callGetShipmentLineListForManualPack(null);
                } else if (_scBaseUtils.equals(view, "short")) {
                    productChild.callGetShipmentLineListForManualPack("short");
                }
                var modelOutput = null;
                modelOutput = _extnExpicientUtils.getMashupOutputForRefID("containerPack_changeShipmentCall", args);
                if (_scBaseUtils.equals(_scModelUtils.getNumberValueFromPath("Shipment.ShipmentContainerizedFlag", modelOutput), 3)) {
                    _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentContainerizedFlag", "04", modelOutput);
                    this.handlePackCompletion();
                    this.extn_showPackedMessage();
                }
            }
            var isMashup2Available = _extnExpicientUtils.isMashupAvailable("containerPack_packAll", args);
            if (isMashup2Available) {
                var modelOutput = null;
                modelOutput = _extnExpicientUtils.getMashupOutputForRefID("containerPack_packAll", args);
                if (_scBaseUtils.equals(_scModelUtils.getNumberValueFromPath("Shipment.ShipmentContainerizedFlag", modelOutput), 3)) {
                    _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentContainerizedFlag", "04", modelOutput);
                    this.handlePackCompletion();
                    this.extn_showPackedMessage();
                }
            }
            var isMashup3Available = _extnExpicientUtils.isMashupAvailable("containerPack_registerBarCodeForPacking", args);
            if (isMashup3Available) {
                var productChild = _scScreenUtils.getChildScreen(this, "ContainerPackProductList");
                var view = productChild.currentView;
                if (_scBaseUtils.equals(view, "All")) {
                    productChild.callGetShipmentLineListForManualPack(null);
                } else if (_scBaseUtils.equals(view, "short")) {
                    productChild.callGetShipmentLineListForManualPack("short");
                }
                var modelOutput = null;
                modelOutput = _extnExpicientUtils.getMashupOutputForRefID("containerPack_registerBarCodeForPacking", args);
                if (_scBaseUtils.equals(_scModelUtils.getNumberValueFromPath("BarCode.Shipment.ShipmentContainerizedFlag", modelOutput), 3)) {
                    _scModelUtils.setStringValueAtModelPath("BarCode.Shipment.ShipmentContainerizedFlag", "04", modelOutput);
                    this.handlePackCompletion();
                    this.extn_showPackedMessage();
                }
            }
        },
        extn_showPackedMessage: function() {
            var model = null;
            model = _scBaseUtils.getNewModelInstance();
            _scEventUtils.fireEventGlobally(this, "extn_allItemPacked", null, model);
        }
    });
});
