scDefine(["dojo/text!./templates/ContainerPackItemScanExtn.html", "scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/_base/lang", "scbase/loader!dojo/text", "scbase/loader!sc/plat", "scbase/loader!sc/plat/dojo/utils/BaseUtils"], function(templateText, _dojodeclare, _dojokernel, _dojolang, _dojotext, _scplat, _scBaseUtils) {
    return _dojodeclare("extn.components.shipment.container.pack.ContainerPackItemScanExtnUI", [], {
        templateString: templateText,
        hotKeys: [],
        events: [],
        subscribers: {
            local: [{
                eventId: 'refreshScreenData',
                sequence: '51',
                handler: {
                    methodName: "extn_showScanAndProductsTab"
                }
            }, {
                eventId: 'showProducts',
                sequence: '51',
                handler: {
                    methodName: "extn_showScan"
                }
            }, {
                eventId: 'afterScreenInit',
                sequence: '51',
                handler: {
                    methodName: "extn_showScanAndProductsTab"
                }
            }, {
                eventId: 'beforeBehaviorMashupCall',
                sequence: '51',
                handler: {
                    methodName: "extn_beforeBehaviorMashupCall"
                }
            }, {
                eventId: 'scrollToScanView',
                sequence: '51',
                handler: {
                    methodName: "extn_scrollToScan"
                }
            }, {
                eventId: 'afterBehaviorMashupCall',
                sequence: '51',
                handler: {
                    methodName: "extn_afterBehaviorMashupCall"
                }
            }]
        }
    });
});
