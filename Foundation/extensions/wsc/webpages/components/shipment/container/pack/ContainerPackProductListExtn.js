scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/components/shipment/container/pack/ContainerPackProductListExtnUI", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/widgets/Label", "scbase/loader!sc/plat/dojo/widgets/Link", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/GridxUtils", "scbase/loader!gridx/Grid", "scbase/loader!sc/plat/dojo/binding/GridxDataBinder", "scbase/loader!ias/utils/RepeatingScreenUtils", "scbase/loader!sc/plat/dojo/utils/RepeatingPanelUtils", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!sc/plat/dojo/utils/RequestUtils", "scbase/loader!ias/utils/ScreenUtils"], function(_dojodeclare, _extnContainerPackProductListExtnUI, _idxContentPane, _scBaseUtils, _scWidgetUtils, _scEventUtils, _scLabel, _scLink, _scScreen, _scModelUtils, _iasUIUtils, _scScreenUtils, _iasBaseTemplateUtils, _scWidgetUtils, _scEventUtils, _scGridxUtils, _gridxGrid, _scGridxDataBinder, _iasRepeatingScreenUtils, _scRepeatingPanelUtils, _scUserprefs, _iasContextUtils, _extnExpicientUtils, _scRequestUtils, _iasScreenUtils) {
    return _dojodeclare("extn.components.shipment.container.pack.ContainerPackProductListExtn", [_extnContainerPackProductListExtnUI], {
        // custom code here
		shipmentStatus: null,
        extn_beforeBehaviorMashupCall: function(event, bEvent, ctrl, args) {
            var isMashupAvailable = _extnExpicientUtils.isBeforeBehaviorMashupAvailable("containerPack_changeShipmentCall", args);
            if (isMashupAvailable) {
                var inputData = null;
                inputData = _extnExpicientUtils.getMashupInputForRefID("containerPack_changeShipmentCall", args);
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.Extn.ExtnIsPackingCompleted", "N", inputData);
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.Extn.ExtnIsShipLabelGenerated", "N", inputData);
            }
            var isDraftMashupAvailable = _extnExpicientUtils.isBeforeBehaviorMashupAvailable("pack_changeShipmentForDraftContainer", args);
            if (isDraftMashupAvailable) {
                var inputData1 = null;
                inputData1 = _extnExpicientUtils.getMashupInputForRefID("pack_changeShipmentForDraftContainer", args);
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.Extn.ExtnIsPackingCompleted", "N", inputData1);
                _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.Extn.ExtnIsShipLabelGenerated", "N", inputData1);
            }
            var isDraftMashup2Available = _extnExpicientUtils.isBeforeBehaviorMashupAvailable("containerPack_recordShortageForPack", args);
            if (isDraftMashup2Available) {
                inputData2 = _extnExpicientUtils.getMashupInputForRefID("containerPack_recordShortageForPack", args);
                var shipmentLineKey = _scModelUtils.getStringValueFromPath("Shipment.ShipmentLines.ShipmentLine.ShipmentLineKey", inputData2);
				if(!_scBaseUtils.isVoid(this.shipmentStatus))
				{
					_scModelUtils.setStringValueAtModelPath("Shipment.Status",this.shipmentStatus, inputData2);
				}
                var shipmentLineModel = null;
                var childScreenList = this.childScreenList;
                length = _scBaseUtils.getAttributeCount(childScreenList);
                for (var index = 0; _scBaseUtils.lessThan(index, length); index = index + 1) {
                    var currChildScreen = _scBaseUtils.getArrayBeanItemByIndex(childScreenList, index);
                    var childName = _scModelUtils.getStringValueFromPath("className", currChildScreen);
                    if (_scBaseUtils.equals("ContainerPackShipmentLineList", childName)) {
                        shipmentLineModel = _scScreenUtils.getModel(currChildScreen, "shipmentLine_Src");
                        currShipmentLineKey = _scModelUtils.getStringValueFromPath("ShipmentLine.ShipmentLineKey", shipmentLineModel);
                        if (_scBaseUtils.equals(shipmentLineKey, currShipmentLineKey)) {
                            currShipmentLineModel = shipmentLineModel;
                            break;
                        }
                    }
                }
                var org = _scUserprefs.getUserOrganization();
                var parentOrg = _scModelUtils.getStringValueFromPath("Organization.ParentOrganizationCode", org);
                if (!_scBaseUtils.isVoid(currShipmentLineModel)) {
                    var qty = _scModelUtils.getStringValueFromPath("ShipmentLine.Quantity", currShipmentLineModel);
                    var placedQty = _scModelUtils.getStringValueFromPath("ShipmentLine.PlacedQuantity", currShipmentLineModel);
                    var orgShortageQty = _scModelUtils.getStringValueFromPath("ShipmentLine.ShortageQty", currShipmentLineModel);
                    var orderHeaderKey = _scModelUtils.getStringValueFromPath("ShipmentLine.OrderHeaderKey", currShipmentLineModel);
                    var orderLineKey = _scModelUtils.getStringValueFromPath("ShipmentLine.OrderLineKey", currShipmentLineModel);
                    var intQty = parseInt(qty);
                    var intPlacedQty = parseInt(placedQty);
                    var shortageQty = intQty - intPlacedQty;
                    _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLines.ShipmentLine.OriginalShortageQty", orgShortageQty, inputData2);
                    _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLines.ShipmentLine.ShortageQty", shortageQty.toString(), inputData2);
                    _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLines.ShipmentLine.Quantity", qty, inputData2);
                    _scModelUtils.setStringValueAtModelPath("Shipment.OrderHeaderKey", orderHeaderKey, inputData2);
                    _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLines.ShipmentLine.OrderLineKey", orderLineKey, inputData2);
                    _scModelUtils.setStringValueAtModelPath("Shipment.OrganizationCode", parentOrg, inputData2);
                }
            }
        },
        extn_goToScan: function(event, bEvent, ctrl, args) {
            _scEventUtils.fireEventToParent(this, "scrollToScanView", null);
        },
        extn_afterBehaviorMashupCall: function(event, bEvent, ctrl, args) {
            var that = this.getOwnerScreen();
            var isMashup1Available = _extnExpicientUtils.isMashupAvailable("containerPack_changeShipmentCall", args);
            if (isMashup1Available) {
                var modelOutput = null;
                modelOutput = _extnExpicientUtils.getMashupOutputForRefID("containerPack_changeShipmentCall", args);
                if (_scBaseUtils.equals(_scModelUtils.getNumberValueFromPath("Shipment.ShipmentContainerizedFlag", modelOutput), 3)) {
                    _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentContainerizedFlag", "04", modelOutput);
                    this.handlePackCompletion();
                    that.extn_showPackedMessage();
                }
            }
            var isMashup2Available = _extnExpicientUtils.isMashupAvailable("pack_changeShipmentForDraftContainer", args);
            if (isMashup2Available) {
                var modelOutput = null;
                modelOutput = _extnExpicientUtils.getMashupOutputForRefID("pack_changeShipmentForDraftContainer", args);
                if (_scBaseUtils.equals(_scModelUtils.getNumberValueFromPath("Shipment.ShipmentContainerizedFlag", modelOutput), 3)) {
                    _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentContainerizedFlag", "04", modelOutput);
                    this.handlePackCompletion();
                    that.extn_showPackedMessage();
                }
            }
            var isMashup3Available = _extnExpicientUtils.isMashupAvailable("containerPack_recordShortageForPack", args);
            if (isMashup3Available) {
                var modelOutput = null;
                modelOutput = _extnExpicientUtils.getMashupOutputForRefID("containerPack_recordShortageForPack", args);
                var shipmentStatus = _scModelUtils.getStringValueFromPath("Shipment.Status", modelOutput);
				this.shipmentStatus = shipmentStatus;
            }

        }
     //   extn_afterScreenInit: function(event, bEvent, ctrl, args) {
           /* var shipmentModel= _scScreenUtils.getModel(this, "getShipmentDetails_output");
            var enterpriseCode= _scModelUtils.getStringValueFromPath("Shipment.EnterpriseCode", shipmentModel);
            var apiInput= _scModelUtils.createNewModelObjectWithRootKey("CommonCode");
            _scModelUtils.setStringValueAtModelPath("CommonCode.OrganizationCode", enterpriseCode, apiInput);
            _scModelUtils.setStringValueAtModelPath("CommonCode.CodeType", "GIFTWRAP_INSTRUCTION", apiInput);
            _iasUIUtils.callApi(this, apiInput, "extn_containerPack_getCommonCodeList_refId", null);
*/
       // }

    });
});
