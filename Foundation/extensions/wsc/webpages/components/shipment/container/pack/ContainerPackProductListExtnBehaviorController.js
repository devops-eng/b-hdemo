


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/components/shipment/container/pack/ContainerPackProductListExtn","scbase/loader!sc/plat/dojo/controller/ExtnServerDataController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnContainerPackProductListExtn
			 ,
			    _scExtnServerDataController
){

return _dojodeclare("extn.components.shipment.container.pack.ContainerPackProductListExtnBehaviorController", 
				[_scExtnServerDataController], {

			
			 screenId : 			'extn.components.shipment.container.pack.ContainerPackProductListExtn'

			
			
			
			
			
						,

			
			
			 mashupRefs : 	[
	 		{
		 extnType : 			'MODIFY'
,
		 mashupId : 			'containerPack_getShipmentLineList_pg'
,
		 mashupRefId : 			'containerPack_getShipmentLineList_pg_refid'

	}
,
	 		{
		 extnType : 			'MODIFY'
,
		 mashupId : 			'pack_changeShipmentForDraftContainer'
,
		 mashupRefId : 			'pack_changeShipmentForDraftContainer'

	}
,
	 		{
		 extnType : 			'ADD'
,
		 mashupId : 			'extn_containerpack_getCommonCodeList'
,
		 mashupRefId : 			'extn_containerpack_getCommonCodeList'

	}
,
	 		{
		 extnType : 			'MODIFY'
,
		 mashupId : 			'containerPack_recordShortageForPack'
,
		 mashupRefId : 			'containerPack_recordShortageForPack'

	},
{
		 extnType : 			'ADD'
,
		 mashupId : 			'extn_containerPack_getCommonCodeList'
,
		 mashupRefId : 			'extn_containerPack_getCommonCodeList_refId'

	}

	]

}
);
});

