
scDefine(["dojo/text!./templates/ContainerPackProductListExtn.html","scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/_base/lang","scbase/loader!dojo/text","scbase/loader!sc/plat","scbase/loader!sc/plat/dojo/utils/BaseUtils"]
 , function(			 
			    templateText
			 ,
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojolang
			 ,
			    _dojotext
			 ,
			    _scplat
			 ,
			    _scBaseUtils
){
return _dojodeclare("extn.components.shipment.container.pack.ContainerPackProductListExtnUI",
				[], {
			templateString: templateText
	
	
	
	
	
	
					,	
	namespaces : {
		targetBindingNamespaces :
		[
		],
		sourceBindingNamespaces :
		[
			{
	  value: 'extn_getCommonCodeList_out'
						,
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_4'
						,
	  description: "output of common code list"
						
			}
			
		]
	}

	
	,
	hotKeys: [ 
	]

,events : [
	]

,subscribers : {

local : [

{
	  eventId: 'shipmentLineListRepPanel_afterPagingLoad'

,	  sequence: '51'




,handler : {
methodName : "extn_goToScan"

 
}
}
,
{
	  eventId: 'beforeBehaviorMashupCall'

,	  sequence: '51'




,handler : {
methodName : "extn_beforeBehaviorMashupCall"

 
}
}
,
{
	  eventId: 'afterBehaviorMashupCall'

,	  sequence: '51'




,handler : {
methodName : "extn_afterBehaviorMashupCall"

 
}
}

]
}

});
});


