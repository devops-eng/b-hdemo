scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/components/shipment/container/pack/ContainerPackShipmentLineListExtnUI", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!dojo/currency","scbase/loader!wsc/components/product/common/utils/ProductUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils"], function(_dojodeclare, _extnContainerPackShipmentLineListExtnUI, _scUserprefs, _scScreenUtils, _iasUIUtils, _scBaseUtils, _scModelUtils, _scWidgetUtils, dCurrency,_wscProductUtils,_scEventUtils) {
    return _dojodeclare("extn.components.shipment.container.pack.ContainerPackShipmentLineListExtn", [_extnContainerPackShipmentLineListExtnUI], {
        // custom code here
        extn_afterScreenInit: function(event, bEvent, ctrl, args) {
            var shipmentLine_Src = _scScreenUtils.getModel(this, "shipmentLine_Src");
            var giftFlag = null;
            giftFlag = _scModelUtils.getStringValueFromPath("ShipmentLine.OrderLine.GiftFlag", shipmentLine_Src);
            // organizationCode = _scModelUtils.getStringValueFromPath("ShipmentLine.SellerOrganizationCode", shipmentLine_Src);
            // var parent = _iasUIUtils.getParentScreen(this, false);
            //  var commonCodeList = _scUserprefs.getProperty("WRAP1");
            // var commonCodeList = _scScreenUtils.getModel(parent, "extn_getCommonCodeList_out");
            //  var commonCode= _scModelUtils.getStringValueFromPath("CommonCodeList.CommonCode", commonCodeList);
            var instructionText = "NONE";
            var InstructionText1 = instructionText;
            if (_scBaseUtils.equals(giftFlag, "Y")) {
                _scWidgetUtils.showWidget(this, "extn_giftFlag", false, null);
                var instruction = _scModelUtils.getStringValueFromPath("ShipmentLine.OrderLine.Instructions.Instruction", shipmentLine_Src);
                for (i = 0; i < instruction.length; i++) {
                    var instructionType = _scModelUtils.getStringValueFromPath("InstructionType", instruction[i]);
                    if (_scBaseUtils.equals(instructionType, "WRAP_TYPE")) {
                        instructionText = _scModelUtils.getStringValueFromPath("InstructionText", instruction[i]);
                        break;
                    }
                }
            } else {
                _scWidgetUtils.hideWidget(this, "extn_giftFlag");
            }
            if (_scBaseUtils.contains(instructionText, "-")) {
                var index = instructionText.indexOf("-");
                index++;
                InstructionText1 = instructionText.substring(index);
            } else {
                InstructionText1 = instructionText;
            }
            _scWidgetUtils.setValue(this, "extn_giftWrap", InstructionText1, false);
            /* for(i=0; i<commonCode.length; i++){
                            var codevalue= _scModelUtils.getStringValueFromPath("CodeValue", commonCode[i]);
                            if(_scBaseUtils.equals(instructionText, codevalue)){
                                 var  InstructionText1= _scModelUtils.getStringValueFromPath("CodeLongDescription", commonCode[i]);
                                    break;
                            }
                            else{
                                var InstructionText1= instructionText;
                            }

                     }*/
        },
        extn_getColor: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
            var color = null;
            if (!_scBaseUtils.isVoid(shipmentLineModel)) {
                var attributeModel = _scModelUtils.getModelObjectFromPath("ShipmentLine.OrderLine.ItemDetails.AdditionalAttributeList.AdditionalAttribute", shipmentLineModel);
                if (!_scBaseUtils.isVoid(attributeModel)) {
                    for (var i = 0; i < attributeModel.length; i++) {
                        var refrenceObj = _scBaseUtils.getArrayItemByIndex(attributeModel, i);
                        var name = _scModelUtils.getStringValueFromPath("Name", refrenceObj);
                        if (_scBaseUtils.equals(name, "Color")) {
                            color = _scModelUtils.getStringValueFromPath("Value", refrenceObj);
                        }
                    }
                }
            }
            return color;
        },
        extn_getPrice: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
            var price = null;
            var country = null;
            if (!_scBaseUtils.isVoid(shipmentLineModel)) {
                var currencyModel = _scModelUtils.getModelObjectFromPath("ShipmentLine.Shipment", shipmentLineModel);
                if (!_scBaseUtils.isVoid(currencyModel)) {
                    var refrenceObj = _scBaseUtils.getArrayItemByIndex(currencyModel, 0);
                    country = _scModelUtils.getStringValueFromPath("Currency", refrenceObj);
                    if (!_scBaseUtils.isVoid(country)) {
                        if (!_scBaseUtils.isVoid(dataValue)) {
                            price = dCurrency.format(parseFloat(dataValue), {
                                currency: country
                            });
                        } else {
                            price = "";
                            return "";
                        }
                        var listPrice = {
                            "price": price
                        };
                        _scScreenUtils.setModel(this, "ListPrice", listPrice);
                    }
                }
            }
            return price;
        },
        extn_openProductDetails: function(event, bEvent, ctrl, args) {
             _scEventUtils.stopEvent(bEvent);
            var itemModel = _scScreenUtils.getTargetModel(this, "ItemDetails", null);
            var priceModel = _scScreenUtils.getModel(this, "ListPrice");
            if (priceModel && priceModel.price) {
                _scModelUtils.setStringValueAtModelPath("Price", priceModel.price, itemModel);
            } else {
                _scModelUtils.setStringValueAtModelPath("Price", "", itemModel);
            }
            _wscProductUtils.openItemDetails(this, itemModel);
        },
        extn_getSize: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
            var size = null;
            if (!_scBaseUtils.isVoid(shipmentLineModel)) {
                var attributeModel = _scModelUtils.getModelObjectFromPath("ShipmentLine.OrderLine.ItemDetails.AdditionalAttributeList.AdditionalAttribute", shipmentLineModel);
                if (!_scBaseUtils.isVoid(attributeModel)) {
                    for (var i = 0; i < attributeModel.length; i++) {
                        var refrenceObj = _scBaseUtils.getArrayItemByIndex(attributeModel, i);
                        var name = _scModelUtils.getStringValueFromPath("Name", refrenceObj);
                        if (_scBaseUtils.equals(name, "Size")) {
                            size = _scModelUtils.getStringValueFromPath("Value", refrenceObj);
                        }
                    }
                }
            }
            return size;
        },
        extn_getUPC: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
            var aliasValue = null;
            if (!_scBaseUtils.isVoid(shipmentLineModel)) {
                var aliasModel = _scModelUtils.getModelObjectFromPath("ShipmentLine.OrderLine.ItemDetails.ItemAliasList.ItemAlias", shipmentLineModel);
                if (!_scBaseUtils.isVoid(aliasModel)) {
                    for (var i = 0; i < aliasModel.length; i++) {
                        var refrenceObj = _scBaseUtils.getArrayItemByIndex(aliasModel, i);
                        var aliasName = _scModelUtils.getStringValueFromPath("AliasName", refrenceObj);
                        if (_scBaseUtils.equals(aliasName, "ACTIVE_UPC")) {
                            aliasValue = _scModelUtils.getStringValueFromPath("AliasValue", refrenceObj);
                        }
                    }
                }
            }
            return aliasValue;
        }
    });
});