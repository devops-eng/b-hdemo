scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/components/shipment/container/pack/ItemPackExtnUI", "scbase/loader!idx/form/TextBox", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat", "scbase/loader!sc/plat/dojo/binding/ButtonDataBinder", "scbase/loader!sc/plat/dojo/binding/SimpleDataBinder", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!ias/utils/EventUtils", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!sc/plat/dojo/utils/ResourcePermissionUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EditorUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/info/ApplicationInfo", "scbase/loader!wsc/components/shipment/common/utils/ShipmentUtils", "scbase/loader!sc/plat/dojo/Userprefs"], function(_dojodeclare, _extnItemPackExtnUI, _idxTextBox, _idxContentPane, _scplat, _scButtonDataBinder, _scSimpleDataBinder, _scBaseUtils, _iasBaseTemplateUtils, _iasUIUtils, _iasEventUtils, _iasContextUtils, _scResourcePermissionUtils, _scScreenUtils, _scModelUtils, _scWidgetUtils, _scEditorUtils, _scEventUtils, _scApplicationInfo, _wscShipmentUtils, _scUserprefs) {
    return _dojodeclare("extn.components.shipment.container.pack.ItemPackExtn", [_extnItemPackExtnUI], {
        // custom code here
        extn_handleBarcodeScan: function(event, bEvent, ctrl, args) {
            if (_iasEventUtils.isEnterPressed(event)) {
                if (_scResourcePermissionUtils.hasPermission("WSC000008")) {
                    _scUserprefs.setProperty("customPrevWizard", "ItemPack");
                }
            }
        },
        extn_packItemIfPermitted: function(event, bEvent, ctrl, args) {
            if (_scResourcePermissionUtils.hasPermission("WSC000008")) {
                _scUserprefs.setProperty("customPrevWizard", "ItemPack");
            }
        },
        handle_pack_packItem: function(mashupRefId, modelOutput, mashupInput, mashupContext, applySetModel) {
            var errorCode = _scModelUtils.getStringValueFromPath("BarCode.ErrorCode", modelOutput);
            if (!_scBaseUtils.isVoid(errorCode)) {
                var messageConfig = {};
                messageConfig["showAsDialog"] = "true";
                _iasBaseTemplateUtils.showMessage(this, errorCode, "error", messageConfig);
            } else {
                var batchModelExist = null;
                batchModelExist = _scModelUtils.hasModelObjectForPathInModel("BarCode.ShipItems", modelOutput);
                if (batchModelExist) {
                    var shipmentNo = null;
                    shipmentNo = _scModelUtils.getStringValueFromPath("BarCode.ShipItems.ShipmentNo", modelOutput);
                    var displayMessage = "To start packing Shipment #: " + shipmentNo + ", please complete picking:";
                    var displayMessageShipment = "To start packing Shipment #: " + shipmentNo + ", please complete picking:";
                    var completeBPMessage = "Shipment #: " + shipmentNo;
                    var finalMessage = null;
                    var batchList = null;
                    var headerModel = null;
                    headerModel = _scModelUtils.getModelObjectFromPath("BarCode.ShipItems", modelOutput);
                    batchList = _scModelUtils.getModelObjectFromPath("BarCode.ShipItems.ShipItem", modelOutput);
                    var batchCount = 0;
                    if (!_scBaseUtils.isVoid(batchList)) {
                        if (_scBaseUtils.equals(headerModel.IsComplete, "Y")) {
                            //Backroom pick complete for all items
                            for (var n = 0; n < batchList.length; n++) {
                                var referenceModel = null;
                                referenceModel = _scBaseUtils.getArrayItemByIndex(batchList, n);
                                var batchNo = null;
                                var itemID = null;
                                var departmentCode = null;
                                var displayMessage1 = null;
                                batchNo = _scModelUtils.getStringValueFromPath("BatchNo", referenceModel);
                                if (!_scBaseUtils.isVoid(batchNo)) {
                                    batchCount++;
                                    displayMessage1 = '<br/>- Batch #: ' + batchNo + ' or dissolve the Batch';
                                    displayMessage += displayMessage1;
                                }
                            }
                            if (!_scBaseUtils.numberGreaterThan(batchCount, 0)) {
                                var displayMessage = displayMessageShipment + '<br/>- ' + completeBPMessage;
                            }
                        } else {
                            //Backroom pick not complete for all items
                            for (var n = 0; n < batchList.length; n++) {
                                var referenceModel = null;
                                referenceModel = _scBaseUtils.getArrayItemByIndex(batchList, n);
                                var batchNo = null;
                                var itemID = null;
                                var departmentCode = null;
                                var displayMessage1 = null;
                                batchNo = _scModelUtils.getStringValueFromPath("BatchNo", referenceModel);
                                itemID = _scModelUtils.getStringValueFromPath("ItemDesc", referenceModel);
                                departmentCode = _scModelUtils.getStringValueFromPath("DepartmentCode", referenceModel);
                                if (_scBaseUtils.isVoid(batchNo)) {
                                    displayMessage1 = '<br/>- Item ' + '"' + itemID + '"' + ' in Department ' + '"' + departmentCode + '"';
                                    displayMessage += displayMessage1;
                                } else {
                                    displayMessage1 = '<br/>- Item ' + '"' + itemID + '"' + ' in Department ' + '"' + departmentCode + '"' + ' in Batch #: ' + batchNo;
                                    displayMessage += displayMessage1;
                                }
                            }
                        }
                        var messageConfig1 = {};
                        messageConfig1["showAsDialog"] = "true";
                        _iasBaseTemplateUtils.showMessage(this, displayMessage, "information", messageConfig1);
                    }
                } else {
                    var shipmentModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
                    _scModelUtils.addModelToModelPath("Shipment", _scModelUtils.getModelObjectFromPath("BarCode.Shipment", modelOutput), shipmentModel);
                    var openPackWizard = true;
                    if (!_iasContextUtils.isMobileContainer()) {
                        var screenContainerInstance = _scApplicationInfo.getSCScreenContainerInstance();
                        var editorInput = {};
                        _scBaseUtils.setAttributeValue("inputData", shipmentModel, editorInput);
                        var allOpenEditors = _scEditorUtils.getSpecificOpenEditors("wsc.desktop.editors.ShipmentEditor");
                        for (i = 0; i < allOpenEditors.length; i++) {
                            var wizardId = allOpenEditors[i].editorInstance.getScreenInstance().wizardId;
                            if (_scBaseUtils.equals("wsc.components.shipment.container.pack.ContainerPackWizard", wizardId) || _scBaseUtils.equals("wsc.components.shipment.backroompick.HoldLocationWizard", wizardId)) {
                                var matchingEditor = screenContainerInstance.getMatchingEditor(allOpenEditors[i].editorInstance, editorInput);
                                if (matchingEditor) {
                                    var packingWizardUI = matchingEditor.getScreenInstance();
                                    if (_scScreenUtils.isDirty(matchingEditor)) {
                                        openPackWizard = false;
                                        var args = {};
                                        _scBaseUtils.setAttributeValue("wizardInput", shipmentModel, args);
                                        var packingWizard = packingWizardUI.getCurrentWizardInstance();
                                        _scEditorUtils.focusOnEditor(matchingEditor, editorInput);
                                        _scEventUtils.fireEventInsideScreen(packingWizard, "dirtyCheckOnItemPack", {}, args);
                                    } else {
                                        _scEditorUtils.closeEditor(packingWizardUI);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    if (openPackWizard) {
                        if (modelOutput && modelOutput.BarCode && modelOutput.BarCode.Shipment && (modelOutput.BarCode.Shipment.PackComplete == "Y")) {
                            var emptyShipmentModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
                            _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", _scModelUtils.getStringValueFromPath("BarCode.Shipment.ShipmentKey", modelOutput), emptyShipmentModel);
                            _wscShipmentUtils.openPackingWizard(this, emptyShipmentModel);
                        } else {
                            _wscShipmentUtils.openPackingWizard(this, shipmentModel);
                        }
                    }
                }
            }
        }
    });
});
