scDefine(["dojo/text!./templates/ItemPackExtn.html", "scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/_base/lang", "scbase/loader!dojo/text", "scbase/loader!idx/form/TextBox", "scbase/loader!sc/plat", "scbase/loader!sc/plat/dojo/binding/SimpleDataBinder", "scbase/loader!sc/plat/dojo/utils/BaseUtils"], function(templateText, _dojodeclare, _dojokernel, _dojolang, _dojotext, _idxTextBox, _scplat, _scSimpleDataBinder, _scBaseUtils) {
    return _dojodeclare("extn.components.shipment.container.pack.ItemPackExtnUI", [], {
        templateString: templateText,
        hotKeys: [],
        events: [],
        subscribers: {
            local: [{
                eventId: 'btnPackItem_onClick',
                sequence: '19',
                handler: {
                    methodName: "extn_packItemIfPermitted"
                }
            }, {
                eventId: 'txtItemID_onKeyUp',
                sequence: '19',
                handler: {
                    methodName: "extn_handleBarcodeScan"
                }
            }]
        }
    });
});
