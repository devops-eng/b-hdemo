scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/components/shipment/customerpickup/CustomerIdentificationExtnUI", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!dojo/currency", "scbase/loader!wsc/components/product/common/utils/ProductUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!wsc/components/product/common/utils/ProductUtils", "scbase/loader!wsc/components/shipment/backroompick/utils/BackroomPickUpUtils", "scbase/loader!wsc/components/shipment/common/screens/ShipmentLineDetailsUI", "scbase/loader!wsc/components/shipment/common/utils/ShipmentUtils", "scbase/loader!ias/utils/EventUtils", "scbase/loader!ias/utils/ScreenUtils", "scbase/loader!ias/utils/UOMUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil"], function(_dojodeclare, _extnCustomerIdentificationExtnUI, _iasContextUtils, _scScreen, _scUserprefs, _scScreenUtils, _iasUIUtils, _scBaseUtils, _scModelUtils, _scWidgetUtils, dCurrency, _wscProductUtils, _scEventUtils, _wscProductUtils, _wscBackroomPickUpUtils, _wscShipmentLineDetailsUI, _wscShipmentUtils, _iasEventUtils, _iasScreenUtils, _iasUOMUtils, _extnExpicientUtils) {
    return _dojodeclare("extn.components.shipment.customerpickup.CustomerIdentificationExtn", [_extnCustomerIdentificationExtnUI], {
        // custom code here
        extn_afterScreenInit: function(event, bEvent, ctrl, args) {
            var shipmentModel = _scScreenUtils.getModel(this, "ShipmentDetails");
            var billToAddress = _scModelUtils.getModelObjectFromPath("Shipment.BillToAddress", shipmentModel);
            var infoMarkFor = null;
            var billToPresent = false;
            var infoToPresent = false;
            var personInfoBillToFirstName = null;
            var personInfoBillToLastName = null;
            var personInfoBillToCombinedName = null;
            var personInfoFirstName = null;
            var personInfoLastName = null;
            var personInfoCombinedName = null;
            var shipmentlineListModel = _scModelUtils.getStringValueFromPath("Shipment.ShipmentLines.ShipmentLine", shipmentModel);
            var diamondFlag = false;
            for (var j = 0; j < shipmentlineListModel.length; j++) {
                var referenceModel1 = null;
                referenceModel1 = _scBaseUtils.getArrayItemByIndex(shipmentlineListModel, j);
                if (referenceModel1.OrderLine && referenceModel1.OrderLine.ItemDetails && referenceModel1.OrderLine.ItemDetails.Extn && referenceModel1.OrderLine.ItemDetails.Extn.ExtnJewelryFlag == "Y") {
                    diamondFlag = true;
                    break;
                }
            }
            if (diamondFlag == true) {
                _scWidgetUtils.showWidget(this, "extn_jewellarySign", false, null);
            } else {
                _scWidgetUtils.hideWidget(this, "extn_jewellarySign");
            }
            if (shipmentModel.Shipment.ShipmentLines && shipmentModel.Shipment.ShipmentLines.ShipmentLine && (shipmentModel.Shipment.ShipmentLines.ShipmentLine.length > 0)) {
                var shipmentLine = null;
                shipmentLine = shipmentModel.Shipment.ShipmentLines.ShipmentLine[0];
                infoMarkFor = shipmentLine.OrderLine.PersonInfoMarkFor;
            }
            if (billToAddress) {
                personInfoBillToFirstName = _scModelUtils.getStringValueFromPath("FirstName", billToAddress);
                personInfoBillToLastName = _scModelUtils.getStringValueFromPath("LastName", billToAddress);
                if ((!_scBaseUtils.isVoid(personInfoBillToFirstName)) && (!_scBaseUtils.isVoid(personInfoBillToLastName))) {
                    //both present
                    billToPresent = true;
                    personInfoBillToCombinedName = personInfoBillToFirstName + " " + personInfoBillToLastName;
                } else if ((!_scBaseUtils.isVoid(personInfoBillToFirstName)) && _scBaseUtils.isVoid(personInfoBillToLastName)) {
                    //only firstname present
                    billToPresent = true;
                    personInfoBillToCombinedName = personInfoBillToFirstName;
                } else if (_scBaseUtils.isVoid(personInfoBillToFirstName) && (!_scBaseUtils.isVoid(personInfoBillToLastName))) {
                    //only lastname present
                    billToPresent = true;
                    personInfoBillToCombinedName = personInfoBillToLastName;
                } else {
                    billToPresent = false;
                }
            }
            if (infoMarkFor) {
                personInfoFirstName = _scModelUtils.getStringValueFromPath("FirstName", infoMarkFor);
                personInfoLastName = _scModelUtils.getStringValueFromPath("LastName", infoMarkFor);
                if ((!_scBaseUtils.isVoid(personInfoFirstName)) && (!_scBaseUtils.isVoid(personInfoLastName))) {
                    //both present
                    infoToPresent = true;
                    personInfoCombinedName = personInfoFirstName + " " + personInfoLastName;
                } else if ((!_scBaseUtils.isVoid(personInfoFirstName)) && _scBaseUtils.isVoid(personInfoLastName)) {
                    //only firstname present
                    infoToPresent = true;
                    personInfoCombinedName = personInfoFirstName;
                } else if (_scBaseUtils.isVoid(personInfoFirstName) && (!_scBaseUtils.isVoid(personInfoLastName))) {
                    //only lastname present
                    infoToPresent = true;
                    personInfoCombinedName = personInfoLastName;
                } else {
                    infoToPresent = false;
                }
            }
            var pickupModel = _scModelUtils.createNewModelObjectWithRootKey("PersonInfo");
            pickupModel.PersonInfo.Person = [];
            if (billToPresent) {
                pickupModel.PersonInfo.Person.push({
                    "Name": personInfoBillToCombinedName
                });
            }
            if (infoToPresent) {
                pickupModel.PersonInfo.Person.push({
                    "Name": personInfoCombinedName
                });
            }
            _scScreenUtils.setModel(this, "extn_personInfoModel", pickupModel);
            var currentStore = _iasContextUtils.getFromContext("CurrentStore")
            var organizationInput = _scModelUtils.createNewModelObjectWithRootKey("Organization");
            _scModelUtils.setStringValueAtModelPath("Organization.OrganizationCode", currentStore, organizationInput);
            _iasUIUtils.callApi(this, organizationInput, "extn_getOrganizationForLocation_ref", null);
        },
        extn_customSave: function(event, bEvent, ctrl, args) {
            _scEventUtils.stopEvent(bEvent);
            this.extn_openReminderPopup();
        },
        extn_openReminderPopup: function() {
            var bindings = _scBaseUtils.getNewBeanInstance();
            var popupParams = _scBaseUtils.getNewModelInstance();
            var dialogParams = _scBaseUtils.getNewModelInstance();
            var newInputModel = _scBaseUtils.getNewModelInstance();
            var outModel = {};
            var inputModel = {};
            _scBaseUtils.addModelValueToBean("InputOrderData", newInputModel, bindings);
            _scBaseUtils.addModelValueToBean("CompleteOrderData", outModel, bindings);
            _scBaseUtils.addBeanValueToBean("binding", bindings, popupParams);
            _scBaseUtils.setAttributeValue("closeCallBackHandler", "extn_onCloseCallSelection", dialogParams);
            _iasUIUtils.openSimplePopup("extn.customScreen.reminderpopup.ReminderPopup", "Reminder", this, popupParams, dialogParams);
        },
        extn_onCloseCallSelection: function(actionPerformed, model, popupParams) {
            if (_scBaseUtils.equals(actionPerformed, "CONFIRM")) {
                this.extn_finishCustomerPickup();
            }
        },
        extn_finishCustomerPickup: function(event, bEvent, ctrl, args) {
            var shipmentModel = null;
            shipmentModel = _scScreenUtils.getModel(this, "ShipmentDetails");
            var shipmentKey = null;
            shipmentKey = _scModelUtils.getStringValueFromPath("Shipment.ShipmentKey", shipmentModel);
            var targetModel = null;
            targetModel = _scScreenUtils.getTargetModel(this, "extn_customerVerificationMethod");
            var verificationMethod = null;
            var pickupPerson = null;
            verificationMethod = _scModelUtils.getStringValueFromPath("Shipment.VerificationId", targetModel);
            pickupPerson = _scModelUtils.getStringValueFromPath("Shipment.PickUpPerson", targetModel);
            var finishModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
            _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", shipmentKey, finishModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.VerificationId", verificationMethod, finishModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.PickUpPerson", pickupPerson, finishModel);
            _iasUIUtils.callApi(this, finishModel, "extn_finishCustomerPickup_ref", null);
        },
        extn_afterBehaviorMashupCall: function(event, bEvent, ctrl, args) {
            var isMashupAvailable = _extnExpicientUtils.isMashupAvailable("extn_finishCustomerPickup_ref", args);
            if (isMashupAvailable) {
                var outputData = null;
                outputData = _extnExpicientUtils.getMashupOutputForRefID("extn_finishCustomerPickup_ref", args);
                _scEventUtils.fireEventToParent(this, "onSaveSuccess", null);
            }
            var isMashupAvailable1 = _extnExpicientUtils.isMashupAvailable("extn_getOrganizationForLocation_ref", args);
            if (isMashupAvailable1) {
                var shipmentModel = null;
                shipmentModel = _scScreenUtils.getModel(this, "ShipmentDetails");
                var lineModel = _scModelUtils.getStringValueFromPath("Shipment.ShipmentLines.ShipmentLine", shipmentModel);
                var orderHeaderKey = _scModelUtils.getStringValueFromPath("Order.OrderHeaderKey", lineModel[0]);
                var shipModel = _scModelUtils.createNewModelObjectWithRootKey("Order");
                _scModelUtils.setStringValueAtModelPath("Order.OrderHeaderKey", orderHeaderKey, shipModel);
                _iasUIUtils.callApi(this, shipModel, "extn_mixedOrder_ref", null);
                var locModel = _scModelUtils.createNewModelObjectWithRootKey("Pickup");
                var outputData = null;
                var normalPickLoc = null;
                var jewellaryPickLoc = null;
                outputData = _extnExpicientUtils.getMashupOutputForRefID("extn_getOrganizationForLocation_ref", args);
                if (outputData.OrganizationList.Organization && (outputData.OrganizationList.Organization.length > 0)) {
                    var orgDetails = _scBaseUtils.getArrayItemByIndex(outputData.OrganizationList.Organization, 0);
                    if (orgDetails.Extn && orgDetails.Extn.HBCStoreLocationDetailsList && (orgDetails.Extn.HBCStoreLocationDetailsList.HBCStoreLocationDetails.length > 0)) {
                        var hbcStoreLocationDetails = null;
                        hbcStoreLocationDetails = orgDetails.Extn.HBCStoreLocationDetailsList.HBCStoreLocationDetails[0];
                        if (hbcStoreLocationDetails && hbcStoreLocationDetails.ExtnPickUpLocation) {
                            normalPickLoc = hbcStoreLocationDetails.ExtnPickUpLocation;
                        }
                        if (hbcStoreLocationDetails && hbcStoreLocationDetails.ExtnJewelPickLoc) {
                            jewellaryPickLoc = hbcStoreLocationDetails.ExtnJewelPickLoc;
                        }
                    }
                }
                var shipmentModel = null;
                shipmentModel = _scScreenUtils.getModel(this, "ShipmentDetails");
                var shipmentlineListModel = null;
                shipmentlineListModel = _scModelUtils.getStringValueFromPath("Shipment.ShipmentLines.ShipmentLine", shipmentModel);
                var diamondFlag = false;
                for (var j = 0; j < shipmentlineListModel.length; j++) {
                    var referenceModel1 = null;
                    referenceModel1 = _scBaseUtils.getArrayItemByIndex(shipmentlineListModel, j);
                    if (referenceModel1.OrderLine && referenceModel1.OrderLine.ItemDetails && referenceModel1.OrderLine.ItemDetails.Extn && referenceModel1.OrderLine.ItemDetails.Extn.ExtnJewelryFlag == "Y") {
                        diamondFlag = true;
                        break;
                    }
                }
                if (diamondFlag == true) {
                    _scModelUtils.setStringValueAtModelPath("Pickup.Location", jewellaryPickLoc, locModel);
                } else {
                    _scModelUtils.setStringValueAtModelPath("Pickup.Location", normalPickLoc, locModel);
                }
                _scScreenUtils.setModel(this, "extn_pickLocModel", locModel);
            }
            var isMashupAvailable2 = _extnExpicientUtils.isMashupAvailable("extn_mixedOrder_ref", args);
            if (isMashupAvailable2) {
                var model = null;
                model = _scScreenUtils.getModel(this, "ShipmentDetails");
                var outputData = null;
                outputData = _extnExpicientUtils.getMashupOutputForRefID("extn_mixedOrder_ref", args);
                var isMixedOrderFlag = _scModelUtils.getStringValueFromPath("Order.IsMixedBagFlag", outputData);
                var shipmentStatus = model.Shipment.Status;
                if (_scBaseUtils.equals(shipmentStatus, "1600.002.10")) {
                    if (!_scBaseUtils.equals(isMixedOrderFlag, "Y")) {
                        _scWidgetUtils.hideWidget(this, "extn_mixedOrder", false, null);
                    }
                } else {
                    _scWidgetUtils.hideWidget(this, "extn_mixedOrder", false, null);
                }
            }
        }
    });
});
