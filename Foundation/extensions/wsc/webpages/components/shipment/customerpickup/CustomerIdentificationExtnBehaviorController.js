


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/components/shipment/customerpickup/CustomerIdentificationExtn","scbase/loader!sc/plat/dojo/controller/ExtnServerDataController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnCustomerIdentificationExtn
			 ,
			    _scExtnServerDataController
){

return _dojodeclare("extn.components.shipment.customerpickup.CustomerIdentificationExtnBehaviorController", 
				[_scExtnServerDataController], {

			
			 screenId : 			'extn.components.shipment.customerpickup.CustomerIdentificationExtn'

			
			
			
			
			
						,

			
			
			 mashupRefs : 	[
	 		{
		 extnType : 			'ADD'
,
		 mashupId : 			'extn_getOrganizationForLocation'
,
		 mashupRefId : 			'extn_getOrganizationForLocation_ref'

	}
,
	 		{
		 extnType : 			'ADD'
,
		 mashupId : 			'extn_finishCustomerPickup'
,
		 mashupRefId : 			'extn_finishCustomerPickup_ref'

	}
,
	 		{
		 extnType : 			'ADD'
,
		 mashupId : 			'extn_mixedOrder'
,
		 mashupRefId : 			'extn_mixedOrder_ref'

	}

	]

}
);
});

