scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/components/shipment/search/ShipmentDetailsExtnUI", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!sc/plat/dojo/utils/ResourcePermissionUtils", "scbase/loader!dojo/_base/array", "scbase/loader!sc/plat/dojo/utils/ControllerUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!sc/plat/dojo/info/ApplicationInfo"], function(_dojodeclare, _extnShipmentDetailsExtnUI, _scScreenUtils, _scBaseUtils, _scModelUtils, _scWidgetUtils, iasContextUtils, scResourcePermissionUtils, dArray, _scControllerUtils, _iasUIUtils, _extnExpicientUtils, scApplicationInfo) {
    return _dojodeclare("extn.components.shipment.search.ShipmentDetailsExtn", [_extnShipmentDetailsExtnUI], {
        // custom code here
        extn_afterScreenInit: function(event, bEvent, ctrl, args) {
            var shipmentList = _scScreenUtils.getModel(this, "getShipmentList_output");
            var shipmentlineListModel = null;
            shipmentlineListModel = _scModelUtils.getStringValueFromPath("Shipment.ShipmentLines.ShipmentLine", shipmentList);
            var giftFlagShow = false;
            _scWidgetUtils.hideWidget(this, "extn_slaIndicator");
            if (!_scBaseUtils.isVoid(shipmentlineListModel)) {
                for (var i = 0; i < shipmentlineListModel.length; i++) {
                    var referenceModel = null;
                    referenceModel = _scBaseUtils.getArrayItemByIndex(shipmentlineListModel, i);
                    var giftFlag = null;
                    giftFlag = _scModelUtils.getStringValueFromPath("OrderLine.GiftFlag", referenceModel);
                    if (_scBaseUtils.equals(giftFlag, "Y")) {
                        giftFlagShow = true;
                    }
                }
                if (_scBaseUtils.equals(giftFlagShow, true)) {
                    _scWidgetUtils.showWidget(this, "extn_flagImage", false, null);
                } else {
                    _scWidgetUtils.hideWidget(this, "extn_flagImage");
                }
                var diamondFlag = false;
                for (var j = 0; j < shipmentlineListModel.length; j++) {
                    var referenceModel1 = null;
                    referenceModel1 = _scBaseUtils.getArrayItemByIndex(shipmentlineListModel, j);
                    if (referenceModel1.OrderLine && referenceModel1.OrderLine.ItemDetails && referenceModel1.OrderLine.ItemDetails.Extn && referenceModel1.OrderLine.ItemDetails.Extn.ExtnJewelryFlag == "Y") {
                        diamondFlag = true;
                        break;
                    }
                }
                if (diamondFlag == true) {
                    _scWidgetUtils.showWidget(this, "extn_jewellarySign", false, null);
                } else {
                    _scWidgetUtils.hideWidget(this, "extn_jewellarySign");
                }
            }
            var shipmentStatus = null;
            shipmentStatus = _scModelUtils.getStringValueFromPath("Shipment.Status.Status", shipmentList);
            var imgName = _scModelUtils.getStringValueFromPath("Shipment.ImageUrl", shipmentList);
            if (_scBaseUtils.equals(shipmentStatus, "1100.70.06.10") || _scBaseUtils.equals(shipmentStatus, "1100.70.06.20") || _scBaseUtils.equals(shipmentStatus, "1100.70.06.30") || _scBaseUtils.equals(shipmentStatus, "1100.70.06.30.5")) {
                _scWidgetUtils.setLabel(this, "lblExpectedShipDate", "Due");
            }
            _scWidgetUtils.addClass(this, "lblExpectedShipDate", "paddinglblnew");
            if (_scBaseUtils.equals(shipmentStatus, "1600.002.10")) {
                _scWidgetUtils.setLabel(this, "lblExpectedShipDate", "Shipment Expiry");
            } else if (!_scBaseUtils.equals(shipmentStatus, "1600.002.20")) {
                if (shipmentList.Shipment.DeliveryMethod == "PICK") {
                    _scWidgetUtils.hideWidget(this, "lbl_timeRemaining");
                    _scWidgetUtils.showWidget(this, "extn_slaIndicator", false, null);
                    if (_scBaseUtils.contains(imgName, "Overdue")) {
                        _scWidgetUtils.setValue(this, "extn_slaIndicator", "Missed SLA");
                    } else if (_scBaseUtils.contains(imgName, "MediumPriority")) {
                        _scWidgetUtils.setValue(this, "extn_slaIndicator", "Nearing SLA");
                    } else if (_scBaseUtils.contains(imgName, "LowPriority")) {
                        _scWidgetUtils.setValue(this, "extn_slaIndicator", "Within SLA");
                    }
                }
            }
            this.extn_baggingPermitted(shipmentList);
        },
        extn_baggingPermitted: function(shipmentModel) {
            var deliveryMethod = _scModelUtils.getStringValueFromPath("Shipment.DeliveryMethod", shipmentModel);
            var shipNode = _scModelUtils.getStringValueFromPath("Shipment.ShipNode", shipmentModel);
            var currentStore = iasContextUtils.getFromContext("CurrentStore");
            var status = _scModelUtils.getStringValueFromPath("Shipment.Status", shipmentModel).Status;
            if (_scBaseUtils.equals(deliveryMethod, "PICK")) {
                if (_scBaseUtils.equals(status, "1600.002.10")) {
                    _scWidgetUtils.showWidget(this, "lnkStartCustomerPickup", true, null);
                } else {
                    _scWidgetUtils.hideWidget(this, "lnkStartCustomerPickup");
                }
                var baggingPermission = "WSC000197";
                if (_scBaseUtils.equals(currentStore, shipNode)) {
                    if ((_scBaseUtils.equals(status, "1100.70.06.30")) && scResourcePermissionUtils.hasPermission(baggingPermission)) {
                        _scWidgetUtils.showWidget(this, "extn_startBagging", true, null);
                        _scWidgetUtils.hideWidget(this, "lnkStartCustomerPickup");
                    } else if ((_scBaseUtils.equals(status, "1100.70.06.30.5")) && scResourcePermissionUtils.hasPermission(baggingPermission)) {
                        _scWidgetUtils.showWidget(this, "extn_continueBagging", true, null);
                        _scWidgetUtils.hideWidget(this, "lnkStartCustomerPickup");
                    }
                }
            }
        },
        addComparisonAttributes: function() {
            var scScreenContainerInstance = scApplicationInfo.getSCScreenContainerInstance();
            var allopenEditorsArray = scScreenContainerInstance.getChildren();
            if (!_scBaseUtils.isVoid(allopenEditorsArray)) {
                for (var i = 0; i < allopenEditorsArray.length; i++) {
                    var referenceArray = null;
                    referenceArray = _scBaseUtils.getArrayItemByIndex(allopenEditorsArray, i);
                    var classEditor = null;
                    classEditor = referenceArray.className;
                    if (_scBaseUtils.equals("BlankEditor", classEditor)) {
                        referenceArray.comparisonAttributes.push("Shipment.ShipmentKey");
                        break;
                    }
                }
            }
        },
        extn_continueBagging: function(event, bEvent, ctrl, args) {
            this.extn_baggingProcess();
        },
        extn_startBagging: function(event, bEvent, ctrl, args) {
            this.extn_baggingProcess();
        },
        extn_afterBehaviorMashupCall: function(event, bEvent, ctrl, args) {
            var isMashupAvailable1 = _extnExpicientUtils.isMashupAvailable("extn_baggingStatusChange_ref", args);
            if (isMashupAvailable1) {
                var outputData = null;
                outputData = _extnExpicientUtils.getMashupOutputForRefID("extn_baggingStatusChange_ref", args);
                var model = null;
                model = _scScreenUtils.getModel(this, "getShipmentList_output");
                var inputParams = {
                    "Shipment": {
                        "ShipmentKey": model.Shipment.ShipmentKey,
                        "DisplayOrderNo": model.Shipment.DisplayOrderNo
                    }
                };
                this.addComparisonAttributes();
                _iasUIUtils.openNewScreenInEditor(this, "extn.customScreen.bagging.Bagging", inputParams, null, "wsc.desktop.editors.BlankEditor");
                //_scControllerUtils.openScreenInEditor("extn.customScreen.bagging.Bagging", inputParams, this, null, null, "wsc.desktop.editors.ShipmentEditor");
            }
            var isMashupAvailable2 = _extnExpicientUtils.isMashupAvailable("extn_baggingStartOver_ref", args);
            if (isMashupAvailable2) {
                var outputData = null;
                outputData = _extnExpicientUtils.getMashupOutputForRefID("extn_baggingStartOver_ref", args);
                this.openBagging();
            }
        },
        extn_baggingProcess: function(event, bEvent, ctrl, args) {
            inputParams = {};
            var model = null;
            model = _scScreenUtils.getModel(this, "getShipmentList_output");
            var assignedToUser = null;
            assignedToUser = _scModelUtils.getStringValueFromPath("Shipment.AssignedToUserId", model);
            var storeUserID = null;
            storeUserID = sc.plat.dojo.Userprefs.getUserId();
            if (assignedToUser) {
                if (_scBaseUtils.equals(assignedToUser, storeUserID)) {
                    //proceed;
                    this.openBagging();
                } else {
                    //show popup
                    this.openBaggingWarningDialog();
                }
            }
        },
        openBagging: function(event, bEvent, ctrl, args) {
            var model = null;
            model = _scScreenUtils.getModel(this, "getShipmentList_output");
            var status = _scModelUtils.getStringValueFromPath("Shipment.Status", model).Status;
            if (_scBaseUtils.equals(status, "1100.70.06.30")) {
                var inputModel = {};
                inputModel = {
                    "Shipment": {
                        "ShipNode": model.Shipment.ShipNode,
                        "BaseDropStatus": "1100.70.06.30.5",
                        "ShipmentKey": model.Shipment.ShipmentKey,
                        "TransactionId": "BAGGING_PROGRESS.0001.ex"
                    }
                };
                _iasUIUtils.callApi(this, inputModel, "extn_baggingStatusChange_ref", null);
            } else {
                var inputParams = {
                    "Shipment": {
                        "ShipmentKey": model.Shipment.ShipmentKey,
                        "DisplayOrderNo": model.Shipment.DisplayOrderNo
                    }
                };
                this.addComparisonAttributes();
                _iasUIUtils.openNewScreenInEditor(this, "extn.customScreen.bagging.Bagging", inputParams, null, "wsc.desktop.editors.BlankEditor");
                //_scControllerUtils.openScreenInEditor("extn.customScreen.bagging.Bagging", inputParams, this, null, null, "wsc.desktop.editors.ShipmentEditor");
            }
        },
        openBaggingWarningDialog: function(event, bEvent, ctrl, args) {
            var modelOutput = null;
            modelOutput = _scScreenUtils.getModel(this, "getShipmentList_output");
            var action = "Bagging";
            var popupParams = _scBaseUtils.getNewBeanInstance();
            _scBaseUtils.addStringValueToBean("outputNamespace", "popupData", popupParams);
            var dialogParams = _scBaseUtils.getNewBeanInstance();
            var bindings = _scBaseUtils.getNewBeanInstance();
            _scBaseUtils.addModelValueToBean("Shipment", modelOutput, bindings);
            _scBaseUtils.addStringValueToBean("Action", action, bindings);
            _scBaseUtils.addBeanValueToBean("binding", bindings, popupParams);
            _scBaseUtils.setAttributeValue("closeCallBackHandler", "abandonedCallbackHandler", dialogParams);
            _scBaseUtils.setAttributeValue("class", "idxModalDialog", dialogParams);
            _iasUIUtils.openSimplePopup("extn.customScreen.bagging.BaggingWarningDialog", "Label_warning_message", this, popupParams, dialogParams);
        },
        abandonedCallbackHandler: function(actionPerformed, model, popupParams) {
            if (!(_scBaseUtils.isVoid(popupParams))) {
                var shipmentModel = null;
                shipmentModel = _scModelUtils.getModelObjectFromPath("binding.Shipment", popupParams);
                var storeUserID = null;
                storeUserID = sc.plat.dojo.Userprefs.getUserId();
                var shipmentKey = null;
                shipmentKey = _scModelUtils.getStringValueFromPath("Shipment.ShipmentKey", shipmentModel);
                if (_scBaseUtils.equals("STARTOVER", actionPerformed)) {
                    var startInputData = {
                        "Shipment": {
                            "ShipmentKey": shipmentKey,
                            "AssignedToUserId": storeUserID,
                            "StartOver": "Y"
                        }
                    };
                    if (shipmentModel && shipmentModel.Shipment && shipmentModel.Shipment.ShipmentLines && shipmentModel.Shipment.ShipmentLines.ShipmentLine.length > 0) {
                        var shipmentLinesList = null;
                        shipmentLinesList = _scModelUtils.getModelObjectFromPath("Shipment.ShipmentLines.ShipmentLine", shipmentModel);
                        var lineList = [];
                        for (var i = 0; i < shipmentLinesList.length; i++) {
                            var currLine = _scBaseUtils.getArrayItemByIndex(shipmentLinesList, i);
                            if (currLine) {
                                var shipmentLineKey = null;
                                shipmentLineKey = _scModelUtils.getStringValueFromPath("ShipmentLineKey", currLine);
                                var lineModel = {
                                    "ShipmentLineKey": shipmentLineKey
                                };
                                lineList.push(lineModel);
                            }
                        }
                        _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLines.ShipmentLine", lineList, startInputData);
                    }
                    _iasUIUtils.callApi(this, startInputData, "extn_baggingStartOver_ref", null);
                } else if (_scBaseUtils.equals("CONTINUE", actionPerformed)) {
                    var startInputDataContinue = {
                        "Shipment": {
                            "ShipmentKey": shipmentKey,
                            "AssignedToUserId": storeUserID,
                            "StartOver": "N"
                        }
                    };
                    _iasUIUtils.callApi(this, startInputDataContinue, "extn_baggingStartOver_ref", null);
                } else if (_scBaseUtils.equals("CLOSEPOPUP", actionPerformed)) {
                    var initialInput = {};
                    _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", shipmentKey, initialInput);
                    _iasUIUtils.openWizardInEditor("wsc.components.shipment.summary.ShipmentSummaryWizard", initialInput, "wsc.desktop.editors.ShipmentEditor", this);
                }
            }
        },
        //overriding the OOTB customer pick method
        startCustomerPick: function(event, bEvent, ctrl, args) {
            var shipmentDetailsModel = null;
            shipmentDetailsModel = _scScreenUtils.getModel(this, "getShipmentList_output");
            var deliveryMethod = null;
            deliveryMethod = _scModelUtils.getStringValueFromPath("Shipment.DeliveryMethod", shipmentDetailsModel);
            if (!(_scBaseUtils.equals(deliveryMethod, "PICK"))) {
                _iasBaseTemplateUtils.showMessage(this, _scScreenUtils.getString(this, "Message_OrderCannotBePick"), "error", null);
            } else {
                var shipmentModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");;
                var shipKey = _scModelUtils.getStringValueFromPath("Shipment.ShipmentKey", shipmentDetailsModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", shipKey, shipmentModel);
                // shipmentModel = _scScreenUtils.getTargetModel(
                // this, "recordCustomerPick_Input", null);
                _iasUIUtils.openWizardInEditor("wsc.components.shipment.customerpickup.CustomerPickUpWizard", shipmentModel, "wsc.desktop.editors.ShipmentEditor", this);
            }
        },
        extn_getOrderPlacedDate: function(dataValue, screen, widget, nameSpace, shipmentDetailsModel, options) {
            if (!_scBaseUtils.isVoid(shipmentDetailsModel)) {
                var shipmentLineModel = _scModelUtils.getModelObjectFromPath("Shipment.ShipmentLines.ShipmentLine", shipmentDetailsModel);
                if (!_scBaseUtils.isVoid(shipmentLineModel)) {
                    var model = _scBaseUtils.getArrayItemByIndex(shipmentLineModel, 0);
                    var orderDate = _scModelUtils.getStringValueFromPath("Order.OrderDate", model);
                    if (!_scBaseUtils.isVoid(orderDate)) {
                        return this.extn_getDate(orderDate);
                    } else {
                        return null;
                    }
                }
            }
        },
        extn_getOrderOMSDate: function(dataValue, screen, widget, nameSpace, shipmentDetailsModel, options) {
            if (!_scBaseUtils.isVoid(shipmentDetailsModel)) {
                var shipmentLineModel = _scModelUtils.getModelObjectFromPath("Shipment.ShipmentLines.ShipmentLine", shipmentDetailsModel);
                if (!_scBaseUtils.isVoid(shipmentLineModel)) {
                    var model = _scBaseUtils.getArrayItemByIndex(shipmentLineModel, 0);
                    var orderDate = _scModelUtils.getStringValueFromPath("Order.Createts", model);
                    if (!_scBaseUtils.isVoid(orderDate)) {
                        return this.extn_getDate(orderDate);
                    } else {
                        return null;
                    }
                }
            }
        },
        extn_getShipStoreDate: function(dataValue, screen, widget, nameSpace, shipmentDetailsModel, options) {
            if (!_scBaseUtils.isVoid(shipmentDetailsModel)) {
                var createDate = _scModelUtils.getModelObjectFromPath("Shipment.Createts", shipmentDetailsModel);
                if (!_scBaseUtils.isVoid(createDate)) {
                    return this.extn_getDate(createDate);
                } else {
                    return null;
                }
            }
        },
        extn_getDate: function(date) {
            var dateVal = new Date(date);
            var finalDate = dateVal.toLocaleDateString();
            return finalDate;
        }
    });
});
