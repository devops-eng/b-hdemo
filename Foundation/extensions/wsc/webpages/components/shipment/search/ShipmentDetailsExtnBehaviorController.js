scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/text", "scbase/loader!extn/components/shipment/search/ShipmentDetailsExtn", "scbase/loader!sc/plat/dojo/controller/ExtnServerDataController"], function(_dojodeclare, _dojokernel, _dojotext, _extnShipmentDetailsExtn, _scExtnServerDataController) {
    return _dojodeclare("extn.components.shipment.search.ShipmentDetailsExtnBehaviorController", [_scExtnServerDataController], {
        screenId: 'extn.components.shipment.search.ShipmentDetailsExtn',
        mashupRefs: [{
            extnType: 'ADD',
            mashupId: 'common_changeShipmentStatus',
            mashupRefId: 'extn_baggingStatusChange_ref'
        }, {
            extnType: 'ADD',
            mashupId: 'extn_baggingStartOver',
            mashupRefId: 'extn_baggingStartOver_ref'
        }]
    });
});
