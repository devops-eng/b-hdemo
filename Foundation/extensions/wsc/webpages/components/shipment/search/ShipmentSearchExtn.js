scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/components/shipment/search/ShipmentSearchExtnUI", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/widgets/Label", "scbase/loader!sc/plat/dojo/widgets/Link", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!wsc/components/shipment/search/utils/SearchUtils", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!ias/utils/EventUtils"], function(_dojodeclare, _extnShipmentSearchExtnUI, _idxContentPane, _scBaseUtils, _scEventUtils, _scLabel, _scLink, _scScreen, _scModelUtils, _iasUIUtils, _scScreenUtils, _iasBaseTemplateUtils, _scWidgetUtils, _scEventUtils, _iasContextUtils, _wscSearchUtils, _scUserprefs, _extnExpicientUtils, _iasEventUtils) {
    return _dojodeclare("extn.components.shipment.search.ShipmentSearchExtn", [_extnShipmentSearchExtnUI], {
        // custom code here
        extn_afterScreenInit: function(event, bEvent, ctrl, args) {
            var apiDateInput = _scModelUtils.createNewModelObjectWithRootKey("CommonCode");
            _scModelUtils.setStringValueAtModelPath("CommonCode.CodeType", "DATE_TYPE", apiDateInput);
            _iasUIUtils.callApi(this, apiDateInput, "extn_getDateType", null);
            var organization = _scUserprefs.getUserOrganization();
            var screenInput = _scScreenUtils.getModel(this, "screenInput");
            /*  _scWidgetUtils.hideWidget(this, "txt_dayPhone");
             */
            var defaultStatus = null;
            defaultStatus = _scModelUtils.getStringValueFromPath("Shipment.NoDefaultStatuses", screenInput);
            var defaultStatusArray = null;
            defaultStatusArray = _scModelUtils.getStringValueFromPath("Shipment.Status", screenInput);
            if (_scBaseUtils.and(_scBaseUtils.equals(defaultStatus, "true"), _scBaseUtils.numberEquals(defaultStatusArray.length, 0))) {
                _scWidgetUtils.hideWidget(this, "extn_scnPack");
                _scWidgetUtils.showWidget(this, "SST_SearchCriteria", false, null);
            } else {
                _scWidgetUtils.hideWidget(this, "SST_SearchCriteria");
                _scWidgetUtils.showWidget(this, "extn_scnPack", false, null);
                var widgetScan = _scScreenUtils.getWidgetByUId(this, "extn_scnPack");
                widgetScan.focusNode.focus();
                //_scWidgetUtils.setFocusOnWidgetUsingUid(this, "extn_scnPack");
            }
                var delMethod = null;
                delMethod = _scModelUtils.getStringValueFromPath("Shipment.DeliveryMethod", screenInput);
                var searchInit = null;
                searchInit = _scModelUtils.getStringValueFromPath("Shipment.DoSearchOnInit", screenInput);
                if (_scBaseUtils.equals(delMethod, "PICK") && _scBaseUtils.equals(searchInit, "true") && _scBaseUtils.equals(defaultStatus, "false")) {
                    var bopusSearchFlag = null;
                    bopusSearchFlag = _scModelUtils.getStringValueFromPath("Shipment.BopusSearch", screenInput);
                if(!_scBaseUtils.isVoid(bopusSearchFlag)){
                        _scWidgetUtils.hideWidget(this, "extn_scnPack"); 
                    }
                }


            this.extn_fulfillmentChange();
            _scBaseUtils.setModel(this, "screenInput", screenInput, null);
            this.extn_toggleRecipient();
        },
        extn_toggleRecipient: function() {
            var targetModel = null;
            var giftFlag = null;
            targetModel = _scBaseUtils.getTargetModel(this, "readFromPersonInfoMarkFor", null);
            giftFlag = _scModelUtils.getStringValueFromPath("Shipment.GiftFlag", targetModel);
            if (_scBaseUtils.equals(giftFlag, "Y")) {
                _scWidgetUtils.enableWidget(this, "extn_recipientEmailStart");
            } else {
                _scWidgetUtils.disableWidget(this, "extn_recipientEmailStart", false);
                var screenInput = null;
                screenInput = _scScreenUtils.getModel(this, "screenInput");
                _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLines.ShipmentLine.OrderLine.PersonInfoMarkFor.EMailID", "", screenInput);
                _scScreenUtils.setModel(this, "screenInput", screenInput, null);
            }
        },
        extn_invokeApiOnEnter: function(event, bEvent, ctrl, args) {
            if (_iasEventUtils.isEnterPressed(event)) {
                this.extn_upcSearch(event, bEvent, ctrl, args);
                //_scEventUtils.stopEvent(bEvent);
            }
        },
        extn_upcSearch: function(event, bEvent, ctrl, args) {
            var targetModel = null;
            targetModel = _scScreenUtils.getTargetModel(this, "extn_upcData_input", null);
            var upc = _scModelUtils.getStringValueFromPath("Shipment.UPC", targetModel);
            if (!_scBaseUtils.isVoid(upc)) {
                var currentStore = _iasContextUtils.getFromContext("CurrentStore")
                var itemModel = _scBaseUtils.getNewModelInstance();
                _scModelUtils.setStringValueAtModelPath("BarCode.BarCodeData", upc, itemModel);
                _iasUIUtils.callApi(this, itemModel, "extn_getItemList_ref", null);
                _scEventUtils.stopEvent(bEvent);
            }
        },
        extn_afterBehaviorMashupCall: function(event, bEvent, ctrl, args) {
            var isDateMashupAvailable = _extnExpicientUtils.isMashupAvailable("extn_getDateType", args);
            if (isDateMashupAvailable) {
                var getDateModelOutput = null;
                getDateModelOutput = _extnExpicientUtils.getMashupOutputForRefID("extn_getDateType", args);
                if (getDateModelOutput && getDateModelOutput.CommonCodeList && getDateModelOutput.CommonCodeList.CommonCode.length > 0) {
                    _scScreenUtils.setModel(this, "extn_dateModel", getDateModelOutput);
                    var dateModel = _scBaseUtils.getNewModelInstance();
                    _scModelUtils.setStringValueAtModelPath("Date.DateType", "Order Placed", dateModel);
                    _scScreenUtils.setModel(this, "extn_initialDateType", dateModel);
                }
            }
            var targetModel = null;
            targetModel = _scScreenUtils.getTargetModel(this, "extn_upcData_input", null);
            var upc = _scModelUtils.getStringValueFromPath("Shipment.UPC", targetModel);
            var isMashupAvailable = _extnExpicientUtils.isMashupAvailable("extn_getItemList_ref", args);
            if (isMashupAvailable) {
                var getItemListModelOutput = null;
                getItemListModelOutput = _extnExpicientUtils.getMashupOutputForRefID("extn_getItemList_ref", args);
                if (!_scBaseUtils.isVoid(getItemListModelOutput)) {
                    var itemModel = null;
                    itemModel = _scModelUtils.getModelObjectFromPath("BarCode.Translations.Translation", getItemListModelOutput);
                    var itemID = null;
                    if (!_scBaseUtils.isVoid(itemModel)) {
                        for (var i = 0; i < itemModel.length; i++) {
                            var a = null;
                            var referenceModel = null;
                            referenceModel = _scBaseUtils.getArrayItemByIndex(itemModel, i);
                            itemID = _scModelUtils.getStringValueFromPath("ItemContextualInfo.ItemID", referenceModel);
                        }
                        _scScreenUtils.setModel(this, "extn_upcData", itemID);
                        this.SST_invokeApi();
                    } else {
                        _scScreenUtils.setModel(this, "extn_upcData", "WrongVal");
                        this.SST_invokeApi();
                    }
                }
            }
        },
        extn_fulfillmentChange: function(event, bEvent, ctrl, args) {
            var selectedTargetModel = null;
            var selected = null;
            selectedTargetModel = _scBaseUtils.getTargetModel(this, "pickShipOrBoth", null);
            selected = _scModelUtils.getStringValueFromPath("Option", selectedTargetModel);
            if (_scBaseUtils.equals(selected, "PICK")) {
                var shipmentStatusList = null;
                shipmentStatusList = _wscSearchUtils.getStatusModel(selected);
                for (var i = 0; i < shipmentStatusList.StatusList.Status.length; i++) {
                    currentStatus = shipmentStatusList.StatusList.Status[i];
                    if (_scBaseUtils.equals(currentStatus.StatusCode, "1100.70.06.30")) {
                        currentStatus.StatusKey = "extn_readyBagging";
                    }
                    if (_scBaseUtils.equals(currentStatus.StatusKey, "Option_ShipmentComplete")) {
                        currentStatus.StatusCode = "1600.002.20";
                    }
                }
                var currentStatusArray = _scModelUtils.getStringValueFromPath("StatusList.Status", shipmentStatusList);
                var updatedArray = _scBaseUtils.getNewArrayInstance();
                for (i = 0; i < currentStatusArray.length; i++) {
                    var statusCode = _scModelUtils.getStringValueFromPath("StatusCode", currentStatusArray[i]);
                    if (_scBaseUtils.equals(statusCode, "1600.002.20")) {
                        var shipcompleteItem = _scModelUtils.getStringValueFromPath("StatusKey", currentStatusArray[i]);
                        var shipcomplete = _scBaseUtils.getNewModelInstance();
                        _scModelUtils.setStringValueAtModelPath("StatusCode", statusCode, shipcomplete);
                        _scModelUtils.setStringValueAtModelPath("StatusKey", shipcompleteItem, shipcomplete);
                    } else {
                        var model = _scBaseUtils.getNewModelInstance();
                        var shipcompleteItem = _scModelUtils.getStringValueFromPath("StatusKey", currentStatusArray[i]);
                        _scModelUtils.setStringValueAtModelPath("StatusCode", statusCode, model);
                        _scModelUtils.setStringValueAtModelPath("StatusKey", shipcompleteItem, model);
                        _scBaseUtils.appendToArray(updatedArray, model);
                    }
                }
                _scModelUtils.setStringValueAtModelPath("StatusList.Status", updatedArray, shipmentStatusList);
                var baggingInProg = _scBaseUtils.getNewModelInstance();
                _scModelUtils.setStringValueAtModelPath("StatusCode", "1100.70.06.30.5", baggingInProg);
                _scModelUtils.setStringValueAtModelPath("StatusKey", "extn_baggingProgress", baggingInProg);
                _scBaseUtils.appendToArray(shipmentStatusList.StatusList.Status, baggingInProg);
                var baggingInProg1 = _scBaseUtils.getNewModelInstance();
                _scModelUtils.setStringValueAtModelPath("StatusCode", "1600.002.10", baggingInProg1);
                _scModelUtils.setStringValueAtModelPath("StatusKey", "Option_ReadyForCustomerPickup", baggingInProg1);
                _scBaseUtils.appendToArray(shipmentStatusList.StatusList.Status, baggingInProg1);
                _scBaseUtils.appendToArray(shipmentStatusList.StatusList.Status, shipcomplete);
                var customerNoShow = _scBaseUtils.getNewModelInstance();
                _scModelUtils.setStringValueAtModelPath("StatusCode", "1600.002.15", customerNoShow);
                _scModelUtils.setStringValueAtModelPath("StatusKey", "extn_Customer_No_Show", customerNoShow);
                _scBaseUtils.appendToArray(shipmentStatusList.StatusList.Status, customerNoShow);
                var cancelledStatus = _scBaseUtils.getNewModelInstance();
                _scModelUtils.setStringValueAtModelPath("StatusCode", "9000", cancelledStatus);
                _scModelUtils.setStringValueAtModelPath("StatusKey", "extn_Cancelled", cancelledStatus);
                _scBaseUtils.appendToArray(shipmentStatusList.StatusList.Status, cancelledStatus);
                this.extn_unsetShipmentInfo();
            } else if (_scBaseUtils.equals(selected, "SHP")) {
                var shipmentStatusList = null;
                shipmentStatusList = _wscSearchUtils.getStatusModel(selected);
                var cancelledStatus = _scBaseUtils.getNewModelInstance();
                _scModelUtils.setStringValueAtModelPath("StatusCode", "9000", cancelledStatus);
                _scModelUtils.setStringValueAtModelPath("StatusKey", "extn_Cancelled", cancelledStatus);
                _scBaseUtils.appendToArray(shipmentStatusList.StatusList.Status, cancelledStatus);
                this.extn_toggleRecipient();
            } else {
                var shipmentStatusList = null;
                shipmentStatusList = _wscSearchUtils.getStatusModel(selected);
                for (var i = 0; i < shipmentStatusList.StatusList.Status.length; i++) {
                    currentStatus = shipmentStatusList.StatusList.Status[i];
                    if (_scBaseUtils.equals(currentStatus.StatusCode, "1100.70.06.30")) {
                        currentStatus.StatusKey = "extn_readyBagging";
                    }
                    if (_scBaseUtils.equals(currentStatus.StatusCode, "1400")) {
                        currentStatus.StatusKey = "Option_Shipped";
                    }
                }
                var currentStatusArray = _scModelUtils.getStringValueFromPath("StatusList.Status", shipmentStatusList);
                var shpArray = _scBaseUtils.getNewArrayInstance();
                var pickArray = _scBaseUtils.getNewArrayInstance();
                var totalArray = _scBaseUtils.getNewArrayInstance();
                for (i = 0; i < currentStatusArray.length; i++) {
                    var statusCode = _scModelUtils.getStringValueFromPath("StatusCode", currentStatusArray[i]);
                    var shipcompleteItem = _scModelUtils.getStringValueFromPath("StatusKey", currentStatusArray[i]);
                    if (_scBaseUtils.equals(statusCode, "1100.70.06.10")) {
                        var shipcomplete = _scBaseUtils.getNewModelInstance();
                        _scModelUtils.setStringValueAtModelPath("StatusCode", statusCode, shipcomplete);
                        _scModelUtils.setStringValueAtModelPath("StatusKey", shipcompleteItem, shipcomplete);
                        _scBaseUtils.appendToArray(pickArray, shipcomplete);
                    } else if (_scBaseUtils.equals(statusCode, "1100.70.06.20")) {
                        var shipcomplete = _scBaseUtils.getNewModelInstance();
                        _scModelUtils.setStringValueAtModelPath("StatusCode", statusCode, shipcomplete);
                        _scModelUtils.setStringValueAtModelPath("StatusKey", shipcompleteItem, shipcomplete);
                        _scBaseUtils.appendToArray(pickArray, shipcomplete);
                    } else if (!_scBaseUtils.equals(statusCode, "1100.70.06.30")) {
                        var model = _scBaseUtils.getNewModelInstance();
                        _scModelUtils.setStringValueAtModelPath("StatusCode", statusCode, model);
                        _scModelUtils.setStringValueAtModelPath("StatusKey", shipcompleteItem, model);
                        _scBaseUtils.appendToArray(shpArray, model);
                    } else {
                        var shipcomplete = _scBaseUtils.getNewModelInstance();
                        _scModelUtils.setStringValueAtModelPath("StatusCode", statusCode, shipcomplete);
                        _scModelUtils.setStringValueAtModelPath("StatusKey", shipcompleteItem, shipcomplete);
                        _scBaseUtils.appendToArray(pickArray, shipcomplete);
                    }
                }
                var statusCode = _scModelUtils.getStringValueFromPath("StatusCode", currentStatusArray[i]);
                var baggingInProg = _scBaseUtils.getNewModelInstance();
                _scModelUtils.setStringValueAtModelPath("StatusCode", "1100.70.06.30.5", baggingInProg);
                _scModelUtils.setStringValueAtModelPath("StatusKey", "extn_baggingProgress", baggingInProg);
                _scBaseUtils.appendToArray(pickArray, baggingInProg);
                var readyBagging = _scBaseUtils.getNewModelInstance();
                _scModelUtils.setStringValueAtModelPath("StatusCode", "1600.002.10", readyBagging);
                _scModelUtils.setStringValueAtModelPath("StatusKey", "Option_ReadyForCustomerPickup", readyBagging);
                _scBaseUtils.appendToArray(pickArray, readyBagging);
                var pickedByCustomer = _scBaseUtils.getNewModelInstance();
                _scModelUtils.setStringValueAtModelPath("StatusCode", "1600.002.20", pickedByCustomer);
                _scModelUtils.setStringValueAtModelPath("StatusKey", "Option_ShipmentComplete", pickedByCustomer);
                _scBaseUtils.appendToArray(pickArray, pickedByCustomer);
                var customerNoShow = _scBaseUtils.getNewModelInstance();
                _scModelUtils.setStringValueAtModelPath("StatusCode", "1600.002.15", customerNoShow);
                _scModelUtils.setStringValueAtModelPath("StatusKey", "extn_Customer_No_Show", customerNoShow);
                _scBaseUtils.appendToArray(pickArray, customerNoShow);
                var cancelledStatus = _scBaseUtils.getNewModelInstance();
                _scModelUtils.setStringValueAtModelPath("StatusCode", "9000", cancelledStatus);
                _scModelUtils.setStringValueAtModelPath("StatusKey", "extn_Cancelled", cancelledStatus);
                _scBaseUtils.appendToArray(shpArray, cancelledStatus);
                updatedArray = pickArray.concat(shpArray);
                _scModelUtils.setStringValueAtModelPath("StatusList.Status", updatedArray, shipmentStatusList);
            }
            _scScreenUtils.setModel(this, "getShipmentStatusList_output", shipmentStatusList, null);
        },
        extn_unsetShipmentInfo: function() {
            var screenInput = null;
            screenInput = _scScreenUtils.getModel(this, "screenInput");
            var targetModel = null;
            var recipEmailID = null;
            targetModel = _scScreenUtils.getTargetModel(this, "getAdvancedShipmentListWithGiftRct_input", null);
            recipEmailID = _scModelUtils.getStringValueFromPath("Shipment.ShipmentLines.ShipmentLine.OrderLine.PersonInfoMarkFor.EMailID", targetModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLines.ShipmentLine.OrderLine.PersonInfoMarkFor.EMailID", recipEmailID, screenInput);
            _scScreenUtils.setModel(this, "screenInput", screenInput, null);
        }
    });
});
