scDefine(["dojo/text!./templates/ShipmentSearchExtn.html", "scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/_base/lang", "scbase/loader!dojo/text", "scbase/loader!extn/customScreen/PackProductScanInitController", "scbase/loader!idx/form/CheckBox", "scbase/loader!idx/form/DateTextBox", "scbase/loader!idx/form/FilteringSelect", "scbase/loader!idx/form/RadioButtonSet", "scbase/loader!idx/form/TextBox", "scbase/loader!idx/layout/TitlePane", "scbase/loader!sc/plat", "scbase/loader!sc/plat/dojo/binding/CheckBoxDataBinder", "scbase/loader!sc/plat/dojo/binding/ComboDataBinder", "scbase/loader!sc/plat/dojo/binding/CurrencyDataBinder", "scbase/loader!sc/plat/dojo/binding/DateDataBinder", "scbase/loader!sc/plat/dojo/binding/RadioSetDataBinder", "scbase/loader!sc/plat/dojo/binding/SimpleDataBinder", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/widgets/ControllerWidget", "scbase/loader!sc/plat/dojo/widgets/IdentifierControllerWidget", "scbase/loader!sc/plat/dojo/widgets/Label"], function(templateText, _dojodeclare, _dojokernel, _dojolang, _dojotext, _extnPackProductScanInitController, _idxCheckBox, _idxDateTextBox, _idxFilteringSelect, _idxRadioButtonSet, _idxTextBox, _idxTitlePane, _scplat, _scCheckBoxDataBinder, _scComboDataBinder, _scCurrencyDataBinder, _scDateDataBinder, _scRadioSetDataBinder, _scSimpleDataBinder, _scBaseUtils, _scControllerWidget, _scIdentifierControllerWidget, _scLabel) {
    return _dojodeclare("extn.components.shipment.search.ShipmentSearchExtnUI", [], {
        templateString: templateText,
        namespaces: {
            targetBindingNamespaces: [{
                value: 'extn_upcData_input',
                scExtensibilityArrayItemId: 'extn_TargetNamespaces_5',
                description: "Pass upc value"
            }, {
                value: 'extn_selectedDateType',
                scExtensibilityArrayItemId: 'extn_TargetNamespaces_6'
            }],
            sourceBindingNamespaces: [{
                value: 'extn_upcData',
                scExtensibilityArrayItemId: 'extn_SourceNamespaces_11',
                description: "Pass UPC in search"
            }, {
                value: 'extn_dateModel',
                scExtensibilityArrayItemId: 'extn_SourceNamespaces_12',
                description: "Date model"
            }, {
                value: 'extn_initialDateType',
                scExtensibilityArrayItemId: 'extn_SourceNamespaces_13',
                description: "Initial Date Type"
            }]
        },
        hotKeys: [],
        events: [],
        subscribers: {
            local: [{
                eventId: 'afterScreenInit',
                sequence: '51',
                handler: {
                    methodName: "extn_afterScreenInit"
                }
            }, {
                eventId: 'txt_orderNo_onKeyUp',
                sequence: '19',
                handler: {
                    methodName: "extn_invokeApiOnEnter"
                }
            }, {
                eventId: 'txt_shipmentNo_onKeyUp',
                sequence: '19',
                handler: {
                    methodName: "extn_invokeApiOnEnter"
                }
            }, {
                eventId: 'txt_assignedToUsr_onKeyUp',
                sequence: '19',
                handler: {
                    methodName: "extn_invokeApiOnEnter"
                }
            }, {
                eventId: 'fs_carrier_onKeyUp',
                sequence: '19',
                handler: {
                    methodName: "extn_invokeApiOnEnter"
                }
            }, {
                eventId: 'txt_dayPhone_onKeyUp',
                sequence: '19',
                handler: {
                    methodName: "extn_invokeApiOnEnter"
                }
            }, {
                eventId: 'txt_emailId_onKeyUp',
                sequence: '19',
                handler: {
                    methodName: "extn_invokeApiOnEnter"
                }
            }, {
                eventId: 'txt_firstName_onKeyUp',
                sequence: '19',
                handler: {
                    methodName: "extn_invokeApiOnEnter"
                }
            }, {
                eventId: 'txt_lastName_onKeyUp',
                sequence: '19',
                handler: {
                    methodName: "extn_invokeApiOnEnter"
                }
            }, {
                eventId: 'SST_OrderBy_onKeyUp',
                sequence: '19',
                handler: {
                    methodName: "extn_invokeApiOnEnter"
                }
            }, {
                eventId: 'SST_SearchButton_onClick',
                sequence: '19',
                handler: {
                    methodName: "extn_upcSearch"
                }
            }, {
                eventId: 'afterBehaviorMashupCall',
                sequence: '51',
                handler: {
                    methodName: "extn_afterBehaviorMashupCall"
                }
            }, {
                eventId: 'extn_toDate_onKeyUp',
                sequence: '51',
                handler: {
                    methodName: "extn_invokeApiOnEnter"
                }
            }, {
                eventId: 'extn_fromDate_onKeyUp',
                sequence: '51',
                handler: {
                    methodName: "extn_invokeApiOnEnter"
                }
            }, {
                eventId: 'extn_UPC_onKeyUp',
                sequence: '51',
                handler: {
                    methodName: "extn_invokeApiOnEnter"
                }
            }, {
                eventId: 'extn_UPC_onKeyDown',
                sequence: '51',
                handler: {
                    methodName: "extn_invokeApiOnEnter"
                }
            }, {
                eventId: 'rad_pickShipOrBoth_onChange',
                sequence: '51',
                handler: {
                    methodName: "extn_fulfillmentChange"
                }
            }, {
                eventId: 'isGiftRecpt_onChange',
                sequence: '51',
                handler: {
                    methodName: "extn_toggleRecipient"
                }
            }]
        }
    });
});
