scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/components/shipment/search/ShipmentSearchResultExtnUI", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/widgets/Label", "scbase/loader!sc/plat/dojo/widgets/Link", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!wsc/components/shipment/search/utils/SearchUtils", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!ias/utils/ModelUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil"], function(_dojodeclare, _extnShipmentSearchResultExtnUI, _idxContentPane, _scBaseUtils, _scEventUtils, _scLabel, _scLink, _scScreen, _scModelUtils, _iasUIUtils, _scScreenUtils, _iasBaseTemplateUtils, _scWidgetUtils, _scEventUtils, _iasContextUtils, _wscSearchUtils, _scUserprefs, _iasModelUtils, _extnExpicientUtils) {
    return _dojodeclare("extn.components.shipment.search.ShipmentSearchResultExtn", [_extnShipmentSearchResultExtnUI], {
        // custom code here
        extn_beforeBehaviorMashupCall: function(event, bEvent, ctrl, args) {
            var isMashupAvailable = _extnExpicientUtils.isBeforeBehaviorMashupAvailable("getListBehavior", args);
            if (isMashupAvailable) {
                var selectedDateModel = _scScreenUtils.getTargetModel(this.getOwnerScreen(), "extn_selectedDateType");
                var selectedDate = null;
                if (selectedDateModel && selectedDateModel.Date) {
                    selectedDate = _scModelUtils.getStringValueFromPath("Date.DateType", selectedDateModel);
                } else {
                    selectedDate = "Order Placed";
                }
                var inputData = null;
                inputData = _extnExpicientUtils.getMashupInputForRefID("getListBehavior", args);
                var fromDate = null;
                fromDate = _scModelUtils.getStringValueFromPath("Shipment.ShipmentLines.ShipmentLine.Order.FromOrderDate", inputData);
                var toDate = null;
                toDate = _scModelUtils.getStringValueFromPath("Shipment.ShipmentLines.ShipmentLine.Order.ToOrderDate", inputData);
                if (fromDate || toDate) {
                    if (selectedDate == "Order Created (in OMS)") {
                        _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLines.ShipmentLine.Order.FromCreatets", fromDate, inputData);
                        _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLines.ShipmentLine.Order.ToCreatets", toDate, inputData);
                        _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLines.ShipmentLine.Order.CreatetsQryType", "DATERANGE", inputData);
                        delete inputData.Shipment.ShipmentLines.ShipmentLine.Order.FromOrderDate;
                        delete inputData.Shipment.ShipmentLines.ShipmentLine.Order.ToOrderDate;
                    } else if (selectedDate == "Order Released to store") {
                        _scModelUtils.setStringValueAtModelPath("Shipment.FromCreatets", fromDate, inputData);
                        _scModelUtils.setStringValueAtModelPath("Shipment.ToCreatets", toDate, inputData);
                        _scModelUtils.setStringValueAtModelPath("Shipment.CreatetsQryType", "DATERANGE", inputData);
                        delete inputData.Shipment.ShipmentLines.ShipmentLine.Order.FromOrderDate;
                        delete inputData.Shipment.ShipmentLines.ShipmentLine.Order.ToOrderDate;
                    } else {
                        _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLines.ShipmentLine.Order.OrderDateQryType", "DATERANGE", inputData);
                    }
                }
                var statusArray = _scModelUtils.getModelObjectFromPath("Shipment.StatusList.Status", inputData);
                var orderno = _scModelUtils.getStringValueFromPath("Shipment.ShipmentLines.ShipmentLine.OrderNo", inputData);
                if (orderno) {
                    _scModelUtils.setStringValueAtModelPath("Shipment.OrderNo", orderno, inputData);
                    _iasModelUtils.removeAttributeFromModel("Shipment.ShipmentLines.ShipmentLine.OrderNo", inputData);
                }
                if (!_scBaseUtils.isVoid(statusArray)) {
                    if (_scBaseUtils.containsInArray(statusArray, "1400")) {
                        if ((!_scBaseUtils.containsInArray(statusArray, this.getSimpleBundleString("Extn_Shipment_Invoiced")))) {
                            _scBaseUtils.appendToArray(statusArray, this.getSimpleBundleString("Extn_Shipment_Invoiced"));
                        }
                    }
                }
                var itemData = _scScreenUtils.getModel(this.getOwnerScreen(), "extn_upcData");
                _scScreenUtils.setModel(this.getOwnerScreen(), "extn_upcData", "");
                if (!_scBaseUtils.isVoid(itemData)) {
                    var a = null;
                    var b = null;
                    _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLines.ShipmentLine.ItemID", itemData, inputData);
                }
                var parentInput = null;
                parentInput = _scScreenUtils.getModel(this.getOwnerScreen(), "screenInput");
                var delMethod = null;
                delMethod = _scModelUtils.getStringValueFromPath("Shipment.DeliveryMethod", parentInput);
                var searchInit = null;
                searchInit = _scModelUtils.getStringValueFromPath("Shipment.DoSearchOnInit", parentInput);
                var noDefStatus = null;
                noDefStatus = _scModelUtils.getStringValueFromPath("Shipment.NoDefaultStatuses", parentInput);
                if (_scBaseUtils.equals(delMethod, "PICK") && _scBaseUtils.equals(searchInit, "true") && _scBaseUtils.equals(noDefStatus, "false")) {
                    var bopusSearchFlag = null;
                    bopusSearchFlag = _scModelUtils.getStringValueFromPath("Shipment.BopusSearch", parentInput);
                    if (_scBaseUtils.equals(bopusSearchFlag, "InProgress")) {
                        if (inputData.Shipment && inputData.Shipment.StatusList && inputData.Shipment.StatusList.Status.length > 0) {
                            if (!(_scBaseUtils.containsInArray(statusArray, "1100.70.06.20") && _scBaseUtils.containsInArray(statusArray, "1100.70.06.30") && _scBaseUtils.containsInArray(statusArray, "1100.70.06.30.5"))) {
                                inputData.Shipment.StatusList.Status = [];
                                inputData.Shipment.StatusList.Status.push("1100.70.06.20");
                                inputData.Shipment.StatusList.Status.push("1100.70.06.30");
                                inputData.Shipment.StatusList.Status.push("1100.70.06.30.5");
                            }
                        }
                    }
                    if (_scBaseUtils.equals(bopusSearchFlag, "MissedSLA")) {
                        if (inputData.Shipment && inputData.Shipment.StatusList && inputData.Shipment.StatusList.Status.length > 0) {
                            if (!(_scBaseUtils.containsInArray(statusArray, "1600.002.10") && _scBaseUtils.numberEquals(inputData.Shipment.StatusList.Status.length, 1))) {
                                inputData.Shipment.StatusList.Status = [];
                                inputData.Shipment.StatusList.Status.push("1600.002.10");
                            }
                        }
                         _scModelUtils.setStringValueAtModelPath("Shipment.BopusSearch","MissedSLA", inputData);
                    }
                    if (_scBaseUtils.equals(bopusSearchFlag, "NearingSLA")) {
                        if (inputData.Shipment && inputData.Shipment.StatusList && inputData.Shipment.StatusList.Status.length > 0) {
                            if (!(_scBaseUtils.containsInArray(statusArray, "1600.002.10") && _scBaseUtils.numberEquals(inputData.Shipment.StatusList.Status.length, 1))) {
                                inputData.Shipment.StatusList.Status = [];
                                inputData.Shipment.StatusList.Status.push("1600.002.10");
                            }
                        }
                        _scModelUtils.setStringValueAtModelPath("Shipment.BopusSearch","NearingSLA", inputData);
                    }
                    if (_scBaseUtils.equals(bopusSearchFlag, "NewShipment")) {
                        if (inputData.Shipment && inputData.Shipment.StatusList && inputData.Shipment.StatusList.Status.length > 0) {
                            if (!(_scBaseUtils.containsInArray(statusArray, "1100.70.06.10") && _scBaseUtils.numberEquals(inputData.Shipment.StatusList.Status.length, 1))) {
                                inputData.Shipment.StatusList.Status = [];
                                inputData.Shipment.StatusList.Status.push("1100.70.06.10");
                            }
                        }
                    }
                    if (_scBaseUtils.equals(bopusSearchFlag, "Restocking")) {}
                    if(!_scBaseUtils.isVoid(bopusSearchFlag)){
                        _scWidgetUtils.hideWidget(this.getOwnerScreen(), "extn_scnPack"); 
                    }
                }
            }
        },
        extn_scrollToTop: function(event, bEvent, ctrl, args) {
            var widgetScan = _scScreenUtils.getWidgetByUId(this.getOwnerScreen(), "extn_scnPack");
            if (!_scBaseUtils.isVoid(widgetScan)) {
                if (_scBaseUtils.equals(widgetScan.isHidden, false)) {
                    _scScreenUtils.scrollToTop();
                }
            }
        },
        extn_getTotalRecords: function(dataValue, screen, widget, nameSpace, shipmentListModel, options) {
            if (shipmentListModel) {
                var totalRecords = null;
                totalRecords = _scModelUtils.getStringValueFromPath("Page.Output.Shipments.TotalNumberOfRecords", shipmentListModel);
                if (totalRecords > 0) {
                    return totalRecords;
                } else {
                    return "0";
                }
            } else {
                return "0";
            }
        }
    });
});
