scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/text", "scbase/loader!extn/components/shipment/search/ShipmentSearchResultExtn", "scbase/loader!sc/plat/dojo/controller/ExtnServerDataController"], function(_dojodeclare, _dojokernel, _dojotext, _extnShipmentSearchResultExtn, _scExtnServerDataController) {
    return _dojodeclare("extn.components.shipment.search.ShipmentSearchResultExtnBehaviorController", [_scExtnServerDataController], {
        screenId: 'extn.components.shipment.search.ShipmentSearchResultExtn',
        mashupRefs: [{
            extnType: 'MODIFY',
            mashupId: 'shipmentSearch_fetchShipmentList',
            mashupRefId: 'getListBehavior'
        }]
    });
});
