scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/text", "scbase/loader!extn/components/shipment/search/ShipmentSearchResultExtn", "scbase/loader!sc/plat/dojo/controller/ExtnScreenController"], function(_dojodeclare, _dojokernel, _dojotext, _extnShipmentSearchResultExtn, _scExtnScreenController) {
    return _dojodeclare("extn.components.shipment.search.ShipmentSearchResultExtnInitController", [_scExtnScreenController], {
        screenId: 'extn.components.shipment.search.ShipmentSearchResultExtn'
    });
});
