scDefine(["dojo/text!./templates/ShipmentSearchResultExtn.html", "scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/_base/lang", "scbase/loader!dojo/text", "scbase/loader!sc/plat", "scbase/loader!sc/plat/dojo/binding/CurrencyDataBinder", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/widgets/DataLabel"], function(templateText, _dojodeclare, _dojokernel, _dojolang, _dojotext, _scplat, _scCurrencyDataBinder, _scBaseUtils, _scDataLabel) {
    return _dojodeclare("extn.components.shipment.search.ShipmentSearchResultExtnUI", [], {
        templateString: templateText,
        hotKeys: [],
        events: [],
        subscribers: {
            local: [{
                eventId: 'repeatingPanel_afterPagingLoad',
                sequence: '51',
                handler: {
                    methodName: "extn_scrollToTop"
                }
            }, {
                eventId: 'beforeBehaviorMashupCall',
                sequence: '51',
                handler: {
                    methodName: "extn_beforeBehaviorMashupCall"
                }
            }]
        }
    });
});
