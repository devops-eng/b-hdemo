scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/components/shipment/summary/ShipmentRTExtnUI", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/EditorUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!ias/utils/ScreenUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!ias/utils/RelatedTaskUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!sc/plat/dojo/utils/ResourcePermissionUtils", "scbase/loader!dojo/_base/array", "scbase/loader!sc/plat/dojo/utils/ControllerUtils", "scbase/loader!sc/plat/dojo/info/ApplicationInfo"], function(_dojodeclare, _extnShipmentRTExtnUI, _scBaseUtils, _scScreenUtils, _scModelUtils, _iasUIUtils, _scEventUtils, scEditorUtils, _scWidgetUtils, _iasScreenUtils, _extnExpicientUtils, _iasRelatedTaskUtils, _iasBaseTemplateUtils, _scUserprefs, iasContextUtils, scResourcePermissionUtils, dArray, _scControllerUtils, scApplicationInfo) {
    return _dojodeclare("extn.components.shipment.summary.ShipmentRTExtn", [_extnShipmentRTExtnUI], {
        // custom code here
        extn_baggingClicked: function(event, bEvent, ctrl, args) {
            inputParams = {};
            var model = null;
            model = _scScreenUtils.getModel(this, "getShipmentDetails_output");
            var assignedToUser = null;
            assignedToUser = _scModelUtils.getStringValueFromPath("Shipment.AssignedToUserId", model);
            var storeUserID = null;
            storeUserID = sc.plat.dojo.Userprefs.getUserId();
            if (assignedToUser) {
                if (_scBaseUtils.equals(assignedToUser, storeUserID)) {
                    //proceed;
                    this.openBagging();
                } else {
                    //show popup
                    this.openBaggingWarningDialog();
                }
            }
        },
        openBagging: function(event, bEvent, ctrl, args) {
            var model = null;
            model = _scScreenUtils.getModel(this, "getShipmentDetails_output");
            var status = _scModelUtils.getStringValueFromPath("Shipment.Status", model);
            if (_scBaseUtils.equals(status, "1100.70.06.30")) {
                var inputModel = {};
                inputModel = {
                    "Shipment": {
                        "ShipNode": model.Shipment.ShipNode,
                        "BaseDropStatus": "1100.70.06.30.5",
                        "ShipmentKey": model.Shipment.ShipmentKey,
                        "TransactionId": "BAGGING_PROGRESS.0001.ex"
                    }
                };
                _iasUIUtils.callApi(this, inputModel, "extn_baggingStatusChange_ref", null);
            } else {
                var inputParams = {
                    "Shipment": {
                        "ShipmentKey": model.Shipment.ShipmentKey,
                        "DisplayOrderNo": model.Shipment.DisplayOrderNo
                    }
                };
                //_scControllerUtils.openScreenInEditor("extn.customScreen.bagging.Bagging", inputParams, this, null, null, "wsc.desktop.editors.ShipmentEditor");
                this.addComparisonAttributes();
                _iasUIUtils.openNewScreenInEditor(this, "extn.customScreen.bagging.Bagging", inputParams, null, "wsc.desktop.editors.BlankEditor");
            }
        },
        openBaggingWarningDialog: function(event, bEvent, ctrl, args) {
            var modelOutput = null;
            modelOutput = _scScreenUtils.getModel(this, "getShipmentDetails_output");
            var action = "Bagging";
            var popupParams = _scBaseUtils.getNewBeanInstance();
            _scBaseUtils.addStringValueToBean("outputNamespace", "popupData", popupParams);
            var dialogParams = _scBaseUtils.getNewBeanInstance();
            var bindings = _scBaseUtils.getNewBeanInstance();
            _scBaseUtils.addModelValueToBean("Shipment", modelOutput, bindings);
            _scBaseUtils.addStringValueToBean("Action", action, bindings);
            _scBaseUtils.addBeanValueToBean("binding", bindings, popupParams);
            _scBaseUtils.setAttributeValue("closeCallBackHandler", "abandonedCallbackHandler", dialogParams);
            _scBaseUtils.setAttributeValue("class", "idxModalDialog", dialogParams);
            _iasUIUtils.openSimplePopup("extn.customScreen.bagging.BaggingWarningDialog", "Label_warning_message", this, popupParams, dialogParams);
        },
        abandonedCallbackHandler: function(actionPerformed, model, popupParams) {
            if (!(_scBaseUtils.isVoid(popupParams))) {
                var shipmentModel = null;
                shipmentModel = _scModelUtils.getModelObjectFromPath("binding.Shipment", popupParams);
                var storeUserID = null;
                storeUserID = sc.plat.dojo.Userprefs.getUserId();
                var shipmentKey = null;
                shipmentKey = _scModelUtils.getStringValueFromPath("Shipment.ShipmentKey", shipmentModel);
                if (_scBaseUtils.equals("STARTOVER", actionPerformed)) {
                    var startInputData = {
                        "Shipment": {
                            "ShipmentKey": shipmentKey,
                            "AssignedToUserId": storeUserID,
                            "StartOver": "Y"
                        }
                    };
                    if (shipmentModel && shipmentModel.Shipment && shipmentModel.Shipment.ShipmentLines && shipmentModel.Shipment.ShipmentLines.ShipmentLine.length > 0) {
                        var shipmentLinesList = null;
                        shipmentLinesList = _scModelUtils.getModelObjectFromPath("Shipment.ShipmentLines.ShipmentLine", shipmentModel);
                        var lineList = [];
                        for (var i = 0; i < shipmentLinesList.length; i++) {
                            var currLine = _scBaseUtils.getArrayItemByIndex(shipmentLinesList, i);
                            if (currLine) {
                                var shipmentLineKey = null;
                                shipmentLineKey = _scModelUtils.getStringValueFromPath("ShipmentLineKey", currLine);
                                var lineModel = {
                                    "ShipmentLineKey": shipmentLineKey
                                };
                                lineList.push(lineModel);
                            }
                        }
                        _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLines.ShipmentLine", lineList, startInputData);
                    }
                    _iasUIUtils.callApi(this, startInputData, "extn_baggingStartOver_ref", null);
                } else if (_scBaseUtils.equals("CONTINUE", actionPerformed)) {
                    var startInputDataContinue = {
                        "Shipment": {
                            "ShipmentKey": shipmentKey,
                            "AssignedToUserId": storeUserID,
                            "StartOver": "N"
                        }
                    };
                    _iasUIUtils.callApi(this, startInputDataContinue, "extn_baggingStartOver_ref", null);
                } else if (_scBaseUtils.equals("CLOSEPOPUP", actionPerformed)) {
                    var initialInput = {};
                    _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", shipmentKey, initialInput);
                    _iasUIUtils.openWizardInEditor("wsc.components.shipment.summary.ShipmentSummaryWizard", initialInput, "wsc.desktop.editors.ShipmentEditor", this);
                }
            }
        },
        extn_baggingRefresh: function(argument) {
            var summaryShipmentKey = _scModelUtils.getStringValueFromPath("Shipment.ShipmentKey", _scScreenUtils.getModel(this, "getShipmentDetails_output"));
            if (argument && argument.Binding && argument.Binding.ShipmentKey) {
                if (_scBaseUtils.equals(summaryShipmentKey, argument.Binding.ShipmentKey)) {
                    // alert("same");
                    this.initializeScreen();
                }
            }
        },
        extn_hideForBagging: function(event, bEvent, ctrl, args) {
            var shipmentModel = null;
            shipmentModel = _scScreenUtils.getModel(this, "getShipmentDetails_output");
            var deliveryMethod = _scModelUtils.getStringValueFromPath("Shipment.DeliveryMethod", shipmentModel);
            var shipNode = _scModelUtils.getStringValueFromPath("Shipment.ShipNode", shipmentModel);
            var currentStore = iasContextUtils.getFromContext("CurrentStore");
            var status = _scModelUtils.getStringValueFromPath("Shipment.Status", shipmentModel);
            if (_scBaseUtils.equals(deliveryMethod, "PICK")) {
                var baggingPermission = "WSC000197";
                if (_scBaseUtils.equals(currentStore, shipNode)) {
                    if (((_scBaseUtils.equals(status, "1100.70.06.30")) || (_scBaseUtils.equals(status, "1100.70.06.30.5"))) && scResourcePermissionUtils.hasPermission(baggingPermission)) {
                        _scWidgetUtils.showWidget(this, "extn_startBagging", true, null);
                        _scWidgetUtils.hideWidget(this, "lnk_RT_RecordCustomerPick");
                    } else {
                        if (_scBaseUtils.equals(status, "1600.002.10")) {
                            _scWidgetUtils.showWidget(this, "lnk_RT_RecordCustomerPick", true, null);
                            _scWidgetUtils.enableWidget(this, "lnk_RT_RecordCustomerPick");
                        } else {
                            _scWidgetUtils.hideWidget(this, "lnk_RT_RecordCustomerPick");
                        }
                        _scWidgetUtils.hideWidget(this, "extn_startBagging");
                    }
                }
            }
            if (_scBaseUtils.equals(status, "1100.70.06.10") || _scBaseUtils.equals(status, "1100.70.06.20") || _scBaseUtils.equals(status, "1100.70.06.30") || _scBaseUtils.equals(status, "1100.70.06.30.5")) {
                _scWidgetUtils.showWidget(this, "extn_reprintPickTicket", true, null);
            } else {
                _scWidgetUtils.hideWidget(this, "extn_reprintPickTicket");
            }
            if (_scBaseUtils.equals(status, "1600.002.10")) {
                _scWidgetUtils.showWidget(this, "extn_printReceipt", true, null);
            } else {
                _scWidgetUtils.hideWidget(this, "extn_printReceipt");
            }
            var printMsg = _scUserprefs.getProperty("printMsg");
            if (_scBaseUtils.equals(status, "1600.002.10")) {
                if (!_scBaseUtils.isVoid(printMsg)) {
                    var that = this;
                    setTimeout(function() {
                        console.log("setTimeout");
                        _iasBaseTemplateUtils.showMessage(that, printMsg, "information", null);
                    }, 500);
                }
            }
            _scUserprefs.setProperty("printMsg", null);
        },
        extn_openCallCenter: function(event, bEvent, ctrl, args) {
            var model = null;
            model = _scScreenUtils.getModel(this, "getShipmentDetails_output");
            var model1 = _scModelUtils.getStringValueFromPath("Shipment.ShipmentLines.ShipmentLine", model);
            var orderHeaderKey = _scBaseUtils.getArrayItemByIndex(model1, 0).OrderHeaderKey;
            var comPermission = _extnExpicientUtils.getComPermission();
            if (!_scBaseUtils.equals(comPermission, "true")) {
                var errorMessage = _scScreenUtils.getString(this, "extn_COM_PERMISSION_ERROR");
                _iasBaseTemplateUtils.showMessage(this, errorMessage, "error", null);
            } else {
                _extnExpicientUtils.openCOM(orderHeaderKey);
            }
        },
        extn_afterBehaviorMashupCall: function(event, bEvent, ctrl, args) {
            if (_extnExpicientUtils.isMashupAvailable("extn_printPDFOnClick_ref", args)) {
                outputModel = _extnExpicientUtils.getMashupOutputForRefID("extn_printPDFOnClick_ref", args);
                this.openPDF(outputModel);
            }
            if (_extnExpicientUtils.isMashupAvailable("extn_printReceipt_ref", args)) {
                outputModel = _extnExpicientUtils.getMashupOutputForRefID("extn_printReceipt_ref", args);
                var printMsg = _scModelUtils.getStringValueFromPath("Shipment.PrintMessage", outputModel);
                _iasBaseTemplateUtils.showMessage(this, printMsg, "information", null);
            }
            var printMashupAvailable = _extnExpicientUtils.isMashupAvailable("extn_hbcPackSlip_ref", args);
            if (printMashupAvailable) {
                var outputData = null;
                outputData = _extnExpicientUtils.getMashupOutputForRefID("extn_hbcPackSlip_ref", args);
                var shiplabelFlag = null;
                shiplabelFlag = _scModelUtils.getStringValueFromPath("Shipment.ShipLabelPrinted", outputData);
                var packSlipFlag = null;
                packSlipFlag = _scModelUtils.getStringValueFromPath("Shipment.PackSlipPrinted", outputData);
                var errorCode = null;
                errorCode = _scModelUtils.getStringValueFromPath("Shipment.ErrorCode", outputData);
                if (!_scBaseUtils.isVoid(errorCode)) {
                    var errorDesc = null;
                    errorDesc = _scModelUtils.getStringValueFromPath("Shipment.ErrorDescription", outputData);
                    _iasBaseTemplateUtils.showMessage(this, errorDesc, "error", null);
                } else {
                    var printerName = null;
                    printerName = _scModelUtils.getStringValueFromPath("Shipment.PrinterName", outputData);
                    var bothPrint = null;
                    bothPrint = this.getSimpleBundleString("extn_ShippingLabel_PackSlip") + " " + "<font size='2px'>" + printerName + "</font>";
                    var packPrint = null;
                    packPrint = this.getSimpleBundleString("extn_Pack_Slip_Message") + " " + "<font size='2px'>" + printerName + "</font>";
                    var shipPrint = null;
                    shipPrint = this.getSimpleBundleString("extn_Shipping_Label_Message") + " " + "<font size='2px'>" + printerName + "</font>";
                    if (_scBaseUtils.and(_scBaseUtils.equals(shiplabelFlag, "Y"), _scBaseUtils.equals(packSlipFlag, "Y"))) {
                        _iasBaseTemplateUtils.showMessage(this, bothPrint, "information", null);
                    } else if (_scBaseUtils.equals(packSlipFlag, "Y")) {
                        _iasBaseTemplateUtils.showMessage(this, packPrint, "information", null);
                    } else if (_scBaseUtils.equals(shiplabelFlag, "Y")) {
                        _iasBaseTemplateUtils.showMessage(this, shipPrint, "information", null);
                    }
                }
            }
            var isMashupAvailable1 = _extnExpicientUtils.isMashupAvailable("extn_baggingStatusChange_ref", args);
            if (isMashupAvailable1) {
                var outputData = null;
                outputData = _extnExpicientUtils.getMashupOutputForRefID("extn_baggingStatusChange_ref", args);
                var model = null;
                model = _scScreenUtils.getModel(this, "getShipmentDetails_output");
                var inputParams = {
                    "Shipment": {
                        "ShipmentKey": model.Shipment.ShipmentKey,
                        "DisplayOrderNo": model.Shipment.DisplayOrderNo
                    }
                };
                this.addComparisonAttributes();
                _iasUIUtils.openNewScreenInEditor(this, "extn.customScreen.bagging.Bagging", inputParams, null, "wsc.desktop.editors.BlankEditor");
                // _scControllerUtils.openScreenInEditor("extn.customScreen.bagging.Bagging", inputParams, this, null, null, "wsc.desktop.editors.ShipmentEditor");
            }
            var isDetailsMashupAvailable = _extnExpicientUtils.isMashupAvailable("getShipmentDetails", args);
            if (isDetailsMashupAvailable) {
                var outputData = null;
                outputData = _extnExpicientUtils.getMashupOutputForRefID("getShipmentDetails", args);
                var status = _scModelUtils.getStringValueFromPath("Shipment.Status", outputData);
                if (_scBaseUtils.equals(status, "1600.002.10")) {
                    _scWidgetUtils.showWidget(this, "extn_printReceipt", true, null);
                }
            }
            var isMashupAvailable2 = _extnExpicientUtils.isMashupAvailable("extn_baggingStartOver_ref", args);
            if (isMashupAvailable2) {
                var outputData = null;
                outputData = _extnExpicientUtils.getMashupOutputForRefID("extn_baggingStartOver_ref", args);
                this.openBagging();
            }
        },
        addComparisonAttributes: function() {
            var scScreenContainerInstance = scApplicationInfo.getSCScreenContainerInstance();
            var allopenEditorsArray = scScreenContainerInstance.getChildren();
            if (!_scBaseUtils.isVoid(allopenEditorsArray)) {
                for (var i = 0; i < allopenEditorsArray.length; i++) {
                    var referenceArray = null;
                    referenceArray = _scBaseUtils.getArrayItemByIndex(allopenEditorsArray, i);
                    var classEditor = null;
                    classEditor = referenceArray.className;
                    if (_scBaseUtils.equals("BlankEditor", classEditor)) {
                        referenceArray.comparisonAttributes.push("Shipment.ShipmentKey");
                        break;
                    }
                }
            }
        },
        extn_customPackSlip: function(event, bEvent, ctrl, args) {
            var model = null;
            model = _scScreenUtils.getModel(this, "getShipmentDetails_output");
            var model1 = _scBaseUtils.cloneModel(model);
            var shipmentModel = null;
            shipmentModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
            var containerModel = null;
            containerModel = _scModelUtils.getModelObjectFromPath("Shipment.Containers", model1);
            var shipmentKey = null;
            shipmentKey = _scModelUtils.getStringValueFromPath("Shipment.ShipmentKey", model1);
            _scModelUtils.addModelToModelPath("Shipment.ShipmentKey", shipmentKey, shipmentModel);
            _scModelUtils.addModelToModelPath("Shipment.Containers", containerModel, shipmentModel);
            var containerList = null;
            containerList = _scModelUtils.getStringValueFromPath("Shipment.Containers.Container", shipmentModel);
            for (var i = 0; i < containerList.length; i++) {
                var referenceModel = null;
                referenceModel = _scBaseUtils.getArrayItemByIndex(containerList, i);
                var isPackCompleted = null;
                isPackCompleted = _scModelUtils.getStringValueFromPath("Extn.ExtnIsPackingCompleted", referenceModel);
                if (_scBaseUtils.equals(isPackCompleted, "Y")) {
                    _scModelUtils.setStringValueAtModelPath("PrintPackSlip", "N", referenceModel);
                    _scModelUtils.setStringValueAtModelPath("RePrintPackSlip", "Y", referenceModel);
                } else {
                    _scModelUtils.setStringValueAtModelPath("PrintPackSlip", "Y", referenceModel);
                    _scModelUtils.setStringValueAtModelPath("RePrintPackSlip", "N", referenceModel);
                }
                _scModelUtils.setStringValueAtModelPath("PrintShippingLabel", "N", referenceModel);
                _scModelUtils.setStringValueAtModelPath("RePrintShippingLabel", "N", referenceModel);
            }
            _scEventUtils.stopEvent(bEvent);
            _iasUIUtils.callApi(this, shipmentModel, "extn_hbcPackSlip_ref", null);
        },
        extn_setWizardName: function(event, bEvent, ctrl, args) {
            _scUserprefs.setProperty("customPrevWizard", "ShipmentSummary");
        },
        extn_UpdatePrinter: function(event, bEvent, ctrl, args) {
            var bindings = _scBaseUtils.getNewBeanInstance();
            var popupParams = _scBaseUtils.getNewModelInstance();
            var dialogParams = _scBaseUtils.getNewModelInstance();
            var newInputModel = _scBaseUtils.getNewModelInstance();
            var outModel = {};
            var inputModel = {};
            _scBaseUtils.addModelValueToBean("InputOrderData", newInputModel, bindings);
            _scBaseUtils.addModelValueToBean("CompleteOrderData", outModel, bindings);
            _scBaseUtils.addBeanValueToBean("binding", bindings, popupParams);
            _scBaseUtils.setAttributeValue("closeCallBackHandler", "onCloseCallSelection", dialogParams);
            _scBaseUtils.addStringValueToBean("outputNamespace", "getPackStationDetails_output", popupParams);
            _iasUIUtils.openSimplePopup("extn.customScreen.UpdatePrinter", "Update Printer", this, popupParams, dialogParams);
        },
        onCloseCallSelection: function(actionPerformed, model, popupParams) {
            if (_scBaseUtils.equals(actionPerformed, "APPLY")) {
                var packStation = model.HBCUserPrinter.PackStationID;
                _iasBaseTemplateUtils.showMessage(this, "Updated Successfully", "success", null);
            }
        },
        extn_printPDFOnClick: function(event, bEvent, ctrl, args) {
            var getShipmentDetails_output = null;
            getShipmentDetails_output = _scScreenUtils.getModel(this, "getShipmentDetails_output");
            var shipmentKey = null;
            shipmentKey = _scModelUtils.getStringValueFromPath("Shipment.ShipmentKey", getShipmentDetails_output);
            var deliveryMethod = null;
            deliveryMethod = _scModelUtils.getStringValueFromPath("Shipment.DeliveryMethod", getShipmentDetails_output);
            var userId = null;
            userId = _scUserprefs.getUserId();
            var shipModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
            _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", shipmentKey, shipModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.UserID", userId, shipModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.DeliveryMethod", deliveryMethod, shipModel);
            _iasUIUtils.callApi(this, shipModel, "extn_printPDFOnClick_ref", null);
        },
        openPDF: function(modelOutput) {
            var fileName = _scModelUtils.getStringValueFromPath("Pdf.PdfName", modelOutput);
            var location = window.location.origin + window.contextPath + "/" + fileName;
            window.open(location);
        },
        extn_printReceiptCustomerPick: function(event, bEvent, ctrl, args) {
            var getShipmentDetails_output = null;
            getShipmentDetails_output = _scScreenUtils.getModel(this, "getShipmentDetails_output");
            var shipmentKey = null;
            shipmentKey = _scModelUtils.getStringValueFromPath("Shipment.ShipmentKey", getShipmentDetails_output);
            var shipModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
            _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", shipmentKey, shipModel);
            _iasUIUtils.callApi(this, shipModel, "extn_printReceipt_ref", null);
        }
    });
});
