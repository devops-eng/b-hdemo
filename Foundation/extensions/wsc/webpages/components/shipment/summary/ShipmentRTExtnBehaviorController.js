scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/text", "scbase/loader!extn/components/shipment/summary/ShipmentRTExtn", "scbase/loader!sc/plat/dojo/controller/ExtnServerDataController"], function(_dojodeclare, _dojokernel, _dojotext, _extnShipmentRTExtn, _scExtnServerDataController) {
    return _dojodeclare("extn.components.shipment.summary.ShipmentRTExtnBehaviorController", [_scExtnServerDataController], {
        screenId: 'extn.components.shipment.summary.ShipmentRTExtn',
        mashupRefs: [{
            extnType: 'MODIFY',
            mashupId: 'shipmentRT_getShipmentDetails',
            mashupRefId: 'getShipmentDetails'
        }, {
            extnType: 'ADD',
            mashupId: 'extn_unpickUnpack',
            mashupRefId: 'extn_unpickUnpack_ref'
        }, {
            extnType: 'ADD',
            mashupId: 'extn_hbcPackSlip',
            mashupRefId: 'extn_hbcPackSlip_ref'
        }, {
            extnType: 'ADD',
            mashupId: 'extn_printPDFOnClick',
            mashupRefId: 'extn_printPDFOnClick_ref'
        }, {
            extnType: 'ADD',
            mashupId: 'common_changeShipmentStatus',
            mashupRefId: 'extn_baggingStatusChange_ref'
        }, {
            extnType: 'ADD',
            mashupId: 'extn_printReceipt',
            mashupRefId: 'extn_printReceipt_ref'
        }, {
            extnType: 'ADD',
            mashupId: 'extn_baggingStartOver',
            mashupRefId: 'extn_baggingStartOver_ref'
        }]
    });
});
