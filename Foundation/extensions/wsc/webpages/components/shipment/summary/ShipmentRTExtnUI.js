scDefine(["dojo/text!./templates/ShipmentRTExtn.html", "scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/_base/lang", "scbase/loader!dojo/text", "scbase/loader!sc/plat", "scbase/loader!sc/plat/dojo/binding/CurrencyDataBinder", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/widgets/Link"], function(templateText, _dojodeclare, _dojokernel, _dojolang, _dojotext, _scplat, _scCurrencyDataBinder, _scBaseUtils, _scLink) {
    return _dojodeclare("extn.components.shipment.summary.ShipmentRTExtnUI", [], {
        templateString: templateText,
        hotKeys: [],
        events: [],
        subscribers: {
            local: [{
                eventId: 'hideOrShowRelatedTasks',
                sequence: '51',
                handler: {
                    methodName: "extn_hideForBagging"
                }
            }, {
                eventId: 'reloadRelatedTask',
                sequence: '51',
                handler: {
                    methodName: "extn_hideForBagging"
                }
            }, {
                eventId: 'lnk_RT_PackShipment_onClick',
                sequence: '19',
                handler: {
                    methodName: "extn_setWizardName"
                }
            }, {
                eventId: 'lnk_RT_PrintPackSlip_onClick',
                sequence: '19',
                handler: {
                    methodName: "extn_customPackSlip"
                }
            }, {
                eventId: 'extn_moreDetails_onClick',
                sequence: '51',
                handler: {
                    methodName: "extn_openCallCenter"
                }
            }, {
                eventId: 'afterBehaviorMashupCall',
                sequence: '51',
                handler: {
                    methodName: "extn_afterBehaviorMashupCall"
                }
            }, {
                eventId: 'extn_ChangePrinter_onClick',
                sequence: '51',
                handler: {
                    methodName: "extn_UpdatePrinter"
                }
            }, {
                eventId: 'extn_reprintPickTicket_onClick',
                sequence: '51',
                handler: {
                    methodName: "extn_printPDFOnClick"
                }
            }, {
                eventId: 'extn_startBagging_onClick',
                sequence: '51',
                handler: {
                    methodName: "extn_baggingClicked"
                }
            }, {
                eventId: 'extn_printReceipt_onClick',
                sequence: '51',
                handler: {
                    methodName: "extn_printReceiptCustomerPick"
                }
            }],
            global: [{
                eventId: 'baggingSummaryRefresh',
                sequence: '51',
                handler: {
                    methodName: "extn_baggingRefresh"
                }
            }]
        }
    });
});
