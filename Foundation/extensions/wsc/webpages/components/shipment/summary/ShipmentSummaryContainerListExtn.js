scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/components/shipment/summary/ShipmentSummaryContainerListExtnUI", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/EditorUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!ias/utils/ScreenUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!ias/utils/RelatedTaskUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/Userprefs"], function(_dojodeclare, _extnShipmentSummaryContainerListExtnUI, _scBaseUtils, _scScreenUtils, _scModelUtils, _iasUIUtils, _scEventUtils, scEditorUtils, _scWidgetUtils, _iasScreenUtils, _extnExpicientUtils, _iasRelatedTaskUtils, _iasBaseTemplateUtils, _scUserprefs) {
    return _dojodeclare("extn.components.shipment.summary.ShipmentSummaryContainerListExtn", [_extnShipmentSummaryContainerListExtnUI], {
        // custom code here
        extn_afterBehaviorMashupCall: function(event, bEvent, ctrl, args) {
            var isMashupAvailable = _extnExpicientUtils.isMashupAvailable("extn_hbcPackSlip_ref", args);
            if (isMashupAvailable) {
                var outputData = null;
                outputData = _extnExpicientUtils.getMashupOutputForRefID("extn_hbcPackSlip_ref", args);
                var shiplabelFlag = null;
                shiplabelFlag = _scModelUtils.getStringValueFromPath("Shipment.ShipLabelPrinted", outputData);
                var packSlipFlag = null;
                packSlipFlag = _scModelUtils.getStringValueFromPath("Shipment.PackSlipPrinted", outputData);
                var errorCode = null;
                errorCode = _scModelUtils.getStringValueFromPath("Shipment.ErrorCode", outputData);
                var usbPrinterFlag = _scModelUtils.getStringValueFromPath("Shipment.USBPrinter", outputData);
                if (!_scBaseUtils.isVoid(errorCode)) {
                    var errorDesc = null;
                    errorDesc = _scModelUtils.getStringValueFromPath("Shipment.ErrorDescription", outputData);
                    _iasBaseTemplateUtils.showMessage(this, errorDesc, "error", null);
                } else {
                    if (_scBaseUtils.equals(usbPrinterFlag, "Y")) {
                        var printmsg = outputData.Shipment.Printers.Printer[0].PrintBuffer;
                        this.printBufferMsg(printmsg);
                    }
                    var printerName = null;
                    printerName = _scModelUtils.getStringValueFromPath("Shipment.PrinterName", outputData);
                    var bothPrint = null;
                    bothPrint = this.getSimpleBundleString("extn_ShippingLabel_PackSlip") + " " + "<font size='2px'>" + printerName + "</font>"
                    var packPrint = null;
                    packPrint = this.getSimpleBundleString("extn_Pack_Slip_Message") + " " + "<font size='2px'>" + printerName + "</font>";
                    var shipPrint = null;
                    shipPrint = this.getSimpleBundleString("extn_Shipping_Label_Message") + " " + "<font size='2px'>" + printerName + "</font>";
                    if (_scBaseUtils.and(_scBaseUtils.equals(shiplabelFlag, "Y"), _scBaseUtils.equals(packSlipFlag, "Y"))) {
                        _iasBaseTemplateUtils.showMessage(this, bothPrint, "information", null);
                    } else if (_scBaseUtils.equals(packSlipFlag, "Y")) {
                        _iasBaseTemplateUtils.showMessage(this, packPrint, "information", null);
                    } else if (_scBaseUtils.equals(shiplabelFlag, "Y")) {
                        _iasBaseTemplateUtils.showMessage(this, shipPrint, "information", null);
                    }
                }
                _scScreenUtils.scrollToTop();
            }
        },
        printBufferMsg: function(bufferMsg) {
            var iframe = document.createElement('iframe');
            var myDiv = document.getElementById("extensibilityShellHolder");
            myDiv.appendChild(iframe);
            var html = '<HTML><BODY style="margin: 20px;">';
            html += bufferMsg;
            html += '<object id="factory" style="display:none" classid="clsid:1663ed61-23eb-11d2-b92f-008048fdd814" codebase="/extnloader/smsx.cab#Version=7.7.0.20"><param name="template" value="MeadCo://IE7" /></object>';
            html += '<script>function silentPrintPage(){ console.log("inside silent print mssage");factory.printing.Print(false)}</script>';
            html += '<div id="print" onclick="silentPrintPage();"></div>';
            html += '</BODY></HTML>';
            iframe.contentWindow.document.open();
            iframe.contentWindow.document.write(html);
            setTimeout(function() {
                console.log("Inside set time out method");
                iframe.contentWindow.focus();
                var printbtn = iframe.contentWindow.document.getElementById("print");
                printbtn.click();
                iframe.contentWindow.focus();
                setTimeout(function() {
                    iframe.contentWindow.document.close();
                    myDiv.removeChild(iframe);
                    console.log("empty set timeout");
                }, 3000)
            }, 1000);
        },
        extn_PackSlipMethod: function(event, bEvent, ctrl, args) {
            var container_Src = null;
            container_Src = _scScreenUtils.getModel(this, "container_Src");
            var shipmentSrcModel = null;
            shipmentSrcModel = _scScreenUtils.getModel(this, "parentShipmentModel");
            var containerShipmentKey = null;
            containerShipmentKey = _scModelUtils.getStringValueFromPath("Shipment.ShipmentKey", shipmentSrcModel);
            var containerNo = null;
            containerNo = _scModelUtils.getStringValueFromPath("Container.ContainerNo", container_Src);
            var isShipLabelGenerated = null;
            var isPackingCompleted = null;
            isShipLabelGenerated = _scModelUtils.getStringValueFromPath("Container.Extn.ExtnIsShipLabelGenerated", container_Src);
            isPackingCompleted = _scModelUtils.getStringValueFromPath("Container.Extn.ExtnIsPackingCompleted", container_Src);
            var shipmentModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
            _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", containerShipmentKey, shipmentModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.ContainerNo", containerNo, shipmentModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.PrintPackSlip", "N", shipmentModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.PrintShippingLabel", "N", shipmentModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.RePrintPackSlip", "Y", shipmentModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.RePrintShippingLabel", "N", shipmentModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.Extn.ExtnIsShipLabelGenerated", isShipLabelGenerated, shipmentModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.Extn.ExtnIsPackingCompleted", isPackingCompleted, shipmentModel);
            _iasUIUtils.callApi(this, shipmentModel, "extn_hbcPackSlip_ref", null);
        },
        extn_shippingLabelMethod: function(event, bEvent, ctrl, args) {
            var container_Src = null;
            container_Src = _scScreenUtils.getModel(this, "container_Src");
            var shipmentSrcModel = null;
            shipmentSrcModel = _scScreenUtils.getModel(this, "parentShipmentModel");
            var containerShipmentKey = null;
            containerShipmentKey = _scModelUtils.getStringValueFromPath("Shipment.ShipmentKey", shipmentSrcModel);
            var scacIntegration = null;
            scacIntegration = _scModelUtils.getStringValueFromPath("Shipment.ScacIntegrationRequired", shipmentSrcModel);
            var containerNo = null;
            containerNo = _scModelUtils.getStringValueFromPath("Container.ContainerNo", container_Src);
            var isShipLabelGenerated = null;
            var isPackingCompleted = null;
            isShipLabelGenerated = _scModelUtils.getStringValueFromPath("Container.Extn.ExtnIsShipLabelGenerated", container_Src);
            isPackingCompleted = _scModelUtils.getStringValueFromPath("Container.Extn.ExtnIsPackingCompleted", container_Src);
            var shipmentModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
            _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", containerShipmentKey, shipmentModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.ScacIntegrationRequired", scacIntegration, shipmentModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.ContainerNo", containerNo, shipmentModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.PrintPackSlip", "N", shipmentModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.PrintShippingLabel", "N", shipmentModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.RePrintPackSlip", "N", shipmentModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.RePrintShippingLabel", "Y", shipmentModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.Extn.ExtnIsShipLabelGenerated", isShipLabelGenerated, shipmentModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.Containers.Container.Extn.ExtnIsPackingCompleted", isPackingCompleted, shipmentModel);
            _iasUIUtils.callApi(this, shipmentModel, "extn_hbcPackSlip_ref", null);
        }
    });
});
