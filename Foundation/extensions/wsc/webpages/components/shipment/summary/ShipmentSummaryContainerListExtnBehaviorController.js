scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/text", "scbase/loader!extn/components/shipment/summary/ShipmentSummaryContainerListExtn", "scbase/loader!sc/plat/dojo/controller/ExtnServerDataController"], function(_dojodeclare, _dojokernel, _dojotext, _extnShipmentSummaryContainerListExtn, _scExtnServerDataController) {
    return _dojodeclare("extn.components.shipment.summary.ShipmentSummaryContainerListExtnBehaviorController", [_scExtnServerDataController], {
        screenId: 'extn.components.shipment.summary.ShipmentSummaryContainerListExtn',
        mashupRefs: [{
            extnType: 'ADD',
            mashupId: 'extn_hbcPackSlip',
            mashupRefId: 'extn_hbcPackSlip_ref'
        }]
    });
});
