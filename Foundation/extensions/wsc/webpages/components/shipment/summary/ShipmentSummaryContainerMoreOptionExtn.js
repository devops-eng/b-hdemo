scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/components/shipment/summary/ShipmentSummaryContainerMoreOptionExtnUI", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/EditorUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!ias/utils/ScreenUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!ias/utils/RelatedTaskUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/Userprefs"], function(_dojodeclare, _extnShipmentSummaryContainerMoreOptionExtnUI, _scBaseUtils, _scScreenUtils, _scModelUtils, _iasUIUtils, _scEventUtils, scEditorUtils, _scWidgetUtils, _iasScreenUtils, _extnExpicientUtils, _iasRelatedTaskUtils, _iasBaseTemplateUtils, _scUserprefs) {
    return _dojodeclare("extn.components.shipment.summary.ShipmentSummaryContainerMoreOptionExtn", [_extnShipmentSummaryContainerMoreOptionExtnUI], {
        // custom code here
        extn_afterScreenInit: function(event, bEvent, ctrl, args) {
            this.extn_showRePrintPackSlip();
            this.extn_showRePrintShippingLabel();
        },
        extn_showRePrintPackSlip: function() {
            var containerSrcModel = null;
            containerSrcModel = _scScreenUtils.getModel(this, "container_Src");
            var isPackComplete = null;
            isPackComplete = _scModelUtils.getStringValueFromPath("Container.Extn.ExtnIsPackingCompleted", containerSrcModel);
            if (_scBaseUtils.equals(isPackComplete, "Y")) {
                _scWidgetUtils.showWidget(this, "extn_reprintPackSlip", false, null);
            } else {
                _scWidgetUtils.hideWidget(this, "extn_reprintPackSlip");
            }
        },
        extn_showRePrintShippingLabel: function() {
            var containerSrcModel = null;
            containerSrcModel = _scScreenUtils.getModel(this, "container_Src");
            var isShipGenerated = null;
            isShipGenerated = _scModelUtils.getStringValueFromPath("Container.Extn.ExtnIsShipLabelGenerated", containerSrcModel);
            if (_scBaseUtils.equals(isShipGenerated, "Y")) {
                _scWidgetUtils.showWidget(this, "extn_reprintShippingLabel", false, null);
            } else {
                _scWidgetUtils.hideWidget(this, "extn_reprintShippingLabel");
            }
        },
        extn_printPackSlip: function(event, bEvent, ctrl, args) {
            _scEventUtils.fireEventToParent(this, "extn_rePrintPackSlip", args);
            _scWidgetUtils.closePopup(this, "PRINT PACK", false);
        },
        extn_printShippingLabel: function(event, bEvent, ctrl, args) {
            _scEventUtils.fireEventToParent(this, "extn_rePrintShippingLabel", args);
            _scWidgetUtils.closePopup(this, "PRINT SHIPPING", false);
        }
    });
});
