scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/components/shipment/summary/ShipmentSummaryExtnUI", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/EditorUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!wsc/components/shipment/common/utils/ShipmentUtils", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!sc/plat/dojo/utils/ResourcePermissionUtils", "scbase/loader!dojo/_base/array", "scbase/loader!sc/plat/dojo/utils/ControllerUtils", "scbase/loader!sc/plat/dojo/info/ApplicationInfo"], function(_dojodeclare, _extnShipmentSummaryExtnUI, _scBaseUtils, _scScreenUtils, _scModelUtils, _iasUIUtils, _scEventUtils, scEditorUtils, _scWidgetUtils, _extnExpicientUtils, _iasBaseTemplateUtils, _scUserprefs, _wscShipmentUtils, iasContextUtils, scResourcePermissionUtils, dArray, _scControllerUtils, scApplicationInfo) {
    return _dojodeclare("extn.components.shipment.summary.ShipmentSummaryExtn", [_extnShipmentSummaryExtnUI], {
        // custom code here
        orderStatus: null,
        extn_afterScreenInit: function(event, bEvent, ctrl, args) {
            var model = null;
            model = _scScreenUtils.getModel(this, "getShipmentDetails_output");
            var orderStatusFinal = _scModelUtils.getStringValueFromPath("Shipment.Status.Status", model);
            if (!_scBaseUtils.isVoid(orderStatusFinal)) {
                if (_scBaseUtils.numberLessThan(parseFloat(orderStatusFinal), 1400)) {
                    _scWidgetUtils.showWidget(this, "extn_shipStatus", false, null);
                } else {
                    _scWidgetUtils.hideWidget(this, "extn_shipStatus");
                }
            }
            //Add css for Mobile - START
            if(iasContextUtils.isMobileContainer())
            {
                _scWidgetUtils.addClass(this, "pnlShipmentDtls", "inlineBlock");
                _scWidgetUtils.addClass(this, "pnlPrimaryInfo", "inlineBlock");
            }
            //Add css for Mobile -  END
            var shipmentStatus = model.Shipment.Status.Status;
            var imgName = _scModelUtils.getStringValueFromPath("Shipment.ImageUrl", model);
            if (!_scBaseUtils.equals(shipmentStatus, "1600.002.10")) {
                _scWidgetUtils.hideWidget(this, "extn_noOfBags", false, null);
                //                 if(!_scBaseUtils.equals(shipmentStatus, "1600.002.20")){
                //                 if(_scBaseUtils.contains(imgName, "Overdue")){
                //                    _scWidgetUtils.setValue(this, "extn_shipStatus", "Missed SLA");
                //                  }
                //                  else if(_scBaseUtils.contains(imgName, "HighPriority")){
                //                     _scWidgetUtils.setValue(this, "extn_shipStatus", "Nearing SLA");
                //                  } 
                //                  else if(_scBaseUtils.contains(imgName, "LowPriority")){
                //                     _scWidgetUtils.setValue(this, "extn_shipStatus", "Within SLA");
                //                  }
                //              }
            } else {
                _scWidgetUtils.addClass(this, "pnlShipmentDtls", "shipmentDetailsWidth");
            }
            if (_scBaseUtils.equals(shipmentStatus, "1100.70.06.10") || _scBaseUtils.equals(shipmentStatus, "1100.70.06.20") || _scBaseUtils.equals(shipmentStatus, "1100.70.06.30") || _scBaseUtils.equals(shipmentStatus, "1100.70.06.30.5")) {
                _scWidgetUtils.setLabel(this, "extn_expiryDate", "Due");
            }
            if (_scBaseUtils.equals(shipmentStatus, "1600.002.20")) {
                _scWidgetUtils.hideWidget(this, "extn_expiryDate", false, null);
            } else {
                _scWidgetUtils.showWidget(this, "extn_expiryDate", false, null);
            }
            if (_scBaseUtils.equals(shipmentStatus, "1600.002.15")) {
                _scWidgetUtils.hideWidget(this, "extn_expiryDate", false, null);
            }
            _scWidgetUtils.hideWidget(this, "lblExpectedShipDate", true);
            this.extn_baggingPermitted(model);
            if (model.Shipment.DeliveryMethodName == "Pickup") {
                _scWidgetUtils.hideWidget(this, "extn_shipStatus", true);
                _scWidgetUtils.showWidget(this, "extn_orderStatusSla", false, null);
                if (_scBaseUtils.contains(imgName, "timeOverdue")) {
                    _scWidgetUtils.setValue(this, "extn_orderStatusSla", "Missed SLA");
                } else if (_scBaseUtils.contains(imgName, "timeLeftMediumPriority")) {
                    _scWidgetUtils.setValue(this, "extn_orderStatusSla", "Nearing SLA");
                } else if (_scBaseUtils.contains(imgName, "timeLeftLowPriority")) {
                    _scWidgetUtils.setValue(this, "extn_orderStatusSla", "Within SLA");
                }
            } else {
                _scWidgetUtils.hideWidget(this, "extn_expiryDate", false, null);
                //     _scWidgetUtils.showWidget(this, "lblExpectedShipDate", false, null);
            }
            if (_scBaseUtils.equals(shipmentStatus, "1600.002.20") || _scBaseUtils.equals(shipmentStatus, "1600.002.10")) {
                _scWidgetUtils.hideWidget(this, "extn_shipStatus", true);
                _scWidgetUtils.hideWidget(this, "extn_orderStatusSla", true);
            }
            var shipmentlineListModel = null;
            shipmentlineListModel = _scModelUtils.getStringValueFromPath("Shipment.ShipmentLines.ShipmentLine", model);
            var diamondFlag = false;
            for (var j = 0; j < shipmentlineListModel.length; j++) {
                var referenceModel1 = null;
                referenceModel1 = _scBaseUtils.getArrayItemByIndex(shipmentlineListModel, j);
                if (referenceModel1.OrderLine && referenceModel1.OrderLine.ItemDetails && referenceModel1.OrderLine.ItemDetails.Extn && referenceModel1.OrderLine.ItemDetails.Extn.ExtnJewelryFlag == "Y") {
                    diamondFlag = true;
                    break;
                }
            }
            if (diamondFlag == true) {
                _scWidgetUtils.showWidget(this, "extn_jewellarySign", false, null);
            } else {
                _scWidgetUtils.hideWidget(this, "extn_jewellarySign");
            }
        },
        extn_baggingPermitted: function(shipmentModel) {
            var deliveryMethod = _scModelUtils.getStringValueFromPath("Shipment.DeliveryMethod", shipmentModel);
            var shipNode = _scModelUtils.getStringValueFromPath("Shipment.ShipNode.ShipNode", shipmentModel);
            var currentStore = iasContextUtils.getFromContext("CurrentStore");
            var status = _scModelUtils.getStringValueFromPath("Shipment.Status", shipmentModel).Status;
            if (_scBaseUtils.equals(deliveryMethod, "PICK")) {
                if (_scBaseUtils.equals(status, "1600.002.10")) {
                    _scWidgetUtils.showWidget(this, "lnkStartCustomerPickup", true, null);
                } else {
                    _scWidgetUtils.hideWidget(this, "lnkStartCustomerPickup");
                }
                var baggingPermission = "WSC000197";
                if (_scBaseUtils.equals(currentStore, shipNode)) {
                    if (((_scBaseUtils.equals(status, "1100.70.06.30")) || (_scBaseUtils.equals(status, "1100.70.06.30.5"))) && scResourcePermissionUtils.hasPermission(baggingPermission)) {
                        _scWidgetUtils.showWidget(this, "extn_startBagging", true, null);
                        _scWidgetUtils.hideWidget(this, "lnkStartCustomerPickup");
                    }
                }
            }
        },
        extn_setWizardName: function(event, bEvent, ctrl, args) {
            _scUserprefs.setProperty("customPrevWizard", "ShipmentSummary");
        },
        extn_getOrderDate: function(dataValue, screen, widget, nameSpace, shipmentDetailsModel, options) {
            var finalDate = null;
            if (!_scBaseUtils.isVoid(shipmentDetailsModel)) {
                var shipmentLineModel = _scModelUtils.getModelObjectFromPath("Shipment.ShipmentLines.ShipmentLine", shipmentDetailsModel);
                if (!_scBaseUtils.isVoid(shipmentLineModel)) {
                    var model = _scBaseUtils.getArrayItemByIndex(shipmentLineModel, 0);
                    var orderDate = _scModelUtils.getStringValueFromPath("Order.OrderDate", model);
                    var dateVal = new Date(orderDate);
                    finalDate = dateVal.toLocaleDateString();
                }
                return finalDate;
            }
        },
        extn_openCallCenter: function(event, bEvent, ctrl, args) {
            var model = null;
            model = _scScreenUtils.getModel(this, "getShipmentDetails_output");
            var model1 = _scModelUtils.getStringValueFromPath("Shipment.ShipmentLines.ShipmentLine", model);
            var orderHeaderKey = _scBaseUtils.getArrayItemByIndex(model1, 0).OrderHeaderKey;
            var comPermission = _extnExpicientUtils.getComPermission();
            if (!_scBaseUtils.equals(comPermission, "true")) {
                var errorMessage = _scScreenUtils.getString(this, "extn_COM_PERMISSION_ERROR");
                _iasBaseTemplateUtils.showMessage(this, errorMessage, "error", null);
            } else {
                _extnExpicientUtils.openCOM(orderHeaderKey);
            }
        },
        extn_applyOverdueStyling: function(unformattedValue, screen, widgetId, namespace, shipment) {
            var model = null;
            model = _scScreenUtils.getModel(this, "getShipmentDetails_output");
            if (_scBaseUtils.isVoid(shipment.Shipment.TimeRemaining)) {
                var currentOrderStatus = this.orderStatus;
                _scModelUtils.setStringValueAtModelPath("Shipment.TimeRemaining", currentOrderStatus, model);
                _scScreenUtils.setModel(this, "getShipmentDetails_output", model);
            }
            var dueInDate = "";
            if (!_scBaseUtils.isVoid(shipment) && !_scBaseUtils.isVoid(shipment.Shipment.TimeRemaining) && _wscShipmentUtils.showSLA(shipment.Shipment.Status.Status)) {
                dueInDate = shipment.Shipment.TimeRemaining;
                /**
                 *  var isOverdue = "";
                 *  isOverdue = shipment.Shipment.IsOverdue;
                 *  if(_scBaseUtils.equals(isOverdue,"true")){
                 *      scWidgetUtils.addClass(screen, widgetId, "pastDue");
                 *    }
                 */
                /**
                 * Defect 445014: Due In Label will appear in colored text based on image file name. 
                 * CSS class are added based image file names.
                 */
                var imageUrl = shipment.Shipment.ImageUrl;
                var cssClass = "";
                if (!_scBaseUtils.isVoid(imageUrl) && imageUrl.lastIndexOf("/") != -1) {
                    cssClass = imageUrl.substring(imageUrl.lastIndexOf("/") + 1, imageUrl.indexOf("."));
                    if (!_scBaseUtils.isVoid(cssClass)) {
                        _scWidgetUtils.addClass(screen, widgetId, cssClass);
                        //scWidgetUtils.addClass(screen,"lblShipmentDesc",cssClass);
                    }
                }
            }
            this.orderStatus = dueInDate;
            return dueInDate;
        },
        extn_continueBagging: function(event, bEvent, ctrl, args) {
            this.extn_baggingProcess();
        },
        extn_startBagging: function(event, bEvent, ctrl, args) {
            this.extn_baggingProcess();
        },
        extn_afterBehaviorMashupCall: function(event, bEvent, ctrl, args) {
            var isMashupAvailable1 = _extnExpicientUtils.isMashupAvailable("extn_baggingStatusChange_ref", args);
            if (isMashupAvailable1) {
                var outputData = null;
                outputData = _extnExpicientUtils.getMashupOutputForRefID("extn_baggingStatusChange_ref", args);
                inputParams = {};
                var model = null;
                model = _scScreenUtils.getModel(this, "getShipmentDetails_output");
                inputParams = {
                    "Shipment": {
                        "ShipmentKey": model.Shipment.ShipmentKey,
                        "DisplayOrderNo": model.Shipment.DisplayOrderNo
                    }
                };
                this.addComparisonAttributes();
                _iasUIUtils.openNewScreenInEditor(this, "extn.customScreen.bagging.Bagging", inputParams, null, "wsc.desktop.editors.BlankEditor");
                // _scControllerUtils.openScreenInEditor("extn.customScreen.bagging.Bagging", inputParams, this, null, null, "wsc.desktop.editors.ShipmentEditor");
            }
            var isMashupAvailable2 = _extnExpicientUtils.isMashupAvailable("extn_baggingStartOver_ref", args);
            if (isMashupAvailable2) {
                var outputData = null;
                outputData = _extnExpicientUtils.getMashupOutputForRefID("extn_baggingStartOver_ref", args);
                this.openBagging();
            }
        },
        addComparisonAttributes: function() {
            var scScreenContainerInstance = scApplicationInfo.getSCScreenContainerInstance();
            var allopenEditorsArray = scScreenContainerInstance.getChildren();
            if (!_scBaseUtils.isVoid(allopenEditorsArray)) {
                for (var i = 0; i < allopenEditorsArray.length; i++) {
                    var referenceArray = null;
                    referenceArray = _scBaseUtils.getArrayItemByIndex(allopenEditorsArray, i);
                    var classEditor = null;
                    classEditor = referenceArray.className;
                    if (_scBaseUtils.equals("BlankEditor", classEditor)) {
                        referenceArray.comparisonAttributes.push("Shipment.ShipmentKey");
                        break;
                    }
                }
            }
        },
        extn_baggingRefresh: function(argument) {
            var summaryShipmentKey = _scModelUtils.getStringValueFromPath("Shipment.ShipmentKey", _scScreenUtils.getModel(this, "getShipmentDetails_output"));
            if (argument && argument.Binding && argument.Binding.ShipmentKey) {
                if (_scBaseUtils.equals(summaryShipmentKey, argument.Binding.ShipmentKey)) {
                    // alert("same");
                    sc.plat.dojo.utils.EditorUtils.refresh(sc.plat.dojo.utils.EditorUtils.getCurrentEditor());
                }
            }
        },
        extn_baggingProcess: function(event, bEvent, ctrl, args) {
            var model = null;
            model = _scScreenUtils.getModel(this, "getShipmentDetails_output");
            var assignedToUser = null;
            assignedToUser = _scModelUtils.getStringValueFromPath("Shipment.AssignedToUserId", model);
            var storeUserID = null;
            storeUserID = sc.plat.dojo.Userprefs.getUserId();
            if (assignedToUser) {
                if (_scBaseUtils.equals(assignedToUser, storeUserID)) {
                    //proceed;
                    this.openBagging();
                } else {
                    //show popup
                    this.openBaggingWarningDialog();
                }
            }
        },
        openBagging: function(event, bEvent, ctrl, args) {
            var model = null;
            model = _scScreenUtils.getModel(this, "getShipmentDetails_output");
            var status = null;
            status = _scModelUtils.getStringValueFromPath("Shipment.Status.Status", model);
            if (_scBaseUtils.isVoid(status)) {
                status = _scModelUtils.getStringValueFromPath("Shipment.Status", model);
            }
            if (_scBaseUtils.equals(status, "1100.70.06.30")) {
                var inputModel = {};
                inputModel = {
                    "Shipment": {
                        "ShipNode": model.Shipment.ShipNode.ShipNode,
                        "BaseDropStatus": "1100.70.06.30.5",
                        "ShipmentKey": model.Shipment.ShipmentKey,
                        "TransactionId": "BAGGING_PROGRESS.0001.ex"
                    }
                };
                _iasUIUtils.callApi(this, inputModel, "extn_baggingStatusChange_ref", null);
            } else {
                var inputParams = {
                    "Shipment": {
                        "ShipmentKey": model.Shipment.ShipmentKey,
                        "DisplayOrderNo": model.Shipment.DisplayOrderNo
                    }
                };
                //_scControllerUtils.openScreenInEditor("extn.customScreen.bagging.Bagging", inputParams, this, null, null, "wsc.desktop.editors.ShipmentEditor");
                this.addComparisonAttributes();
                _iasUIUtils.openNewScreenInEditor(this, "extn.customScreen.bagging.Bagging", inputParams, null, "wsc.desktop.editors.BlankEditor");
            }
        },
        openBaggingWarningDialog: function(event, bEvent, ctrl, args) {
            var modelOutput = null;
            modelOutput = _scScreenUtils.getModel(this, "getShipmentDetails_output");
            var action = "Bagging";
            var popupParams = _scBaseUtils.getNewBeanInstance();
            _scBaseUtils.addStringValueToBean("outputNamespace", "popupData", popupParams);
            var dialogParams = _scBaseUtils.getNewBeanInstance();
            var bindings = _scBaseUtils.getNewBeanInstance();
            _scBaseUtils.addModelValueToBean("Shipment", modelOutput, bindings);
            _scBaseUtils.addStringValueToBean("Action", action, bindings);
            _scBaseUtils.addBeanValueToBean("binding", bindings, popupParams);
            _scBaseUtils.setAttributeValue("closeCallBackHandler", "abandonedCallbackHandler", dialogParams);
            _scBaseUtils.setAttributeValue("class", "idxModalDialog", dialogParams);
            _iasUIUtils.openSimplePopup("extn.customScreen.bagging.BaggingWarningDialog", "Label_warning_message", this, popupParams, dialogParams);
        },
        abandonedCallbackHandler: function(actionPerformed, model, popupParams) {
            if (!(_scBaseUtils.isVoid(popupParams))) {
                var shipmentModel = null;
                shipmentModel = _scModelUtils.getModelObjectFromPath("binding.Shipment", popupParams);
                var storeUserID = null;
                storeUserID = sc.plat.dojo.Userprefs.getUserId();
                var shipmentKey = null;
                shipmentKey = _scModelUtils.getStringValueFromPath("Shipment.ShipmentKey", shipmentModel);
                if (_scBaseUtils.equals("STARTOVER", actionPerformed)) {
                    var startInputData = {
                        "Shipment": {
                            "ShipmentKey": shipmentKey,
                            "AssignedToUserId": storeUserID,
                            "StartOver": "Y"
                        }
                    };
                    if (shipmentModel && shipmentModel.Shipment && shipmentModel.Shipment.ShipmentLines && shipmentModel.Shipment.ShipmentLines.ShipmentLine.length > 0) {
                        var shipmentLinesList = null;
                        shipmentLinesList = _scModelUtils.getModelObjectFromPath("Shipment.ShipmentLines.ShipmentLine", shipmentModel);
                        var lineList = [];
                        for (var i = 0; i < shipmentLinesList.length; i++) {
                            var currLine = _scBaseUtils.getArrayItemByIndex(shipmentLinesList, i);
                            if (currLine) {
                                var shipmentLineKey = null;
                                shipmentLineKey = _scModelUtils.getStringValueFromPath("ShipmentLineKey", currLine);
                                var lineModel = {
                                    "ShipmentLineKey": shipmentLineKey
                                };
                                lineList.push(lineModel);
                            }
                        }
                        _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLines.ShipmentLine", lineList, startInputData);
                    }
                    _iasUIUtils.callApi(this, startInputData, "extn_baggingStartOver_ref", null);
                } else if (_scBaseUtils.equals("CONTINUE", actionPerformed)) {
                    var startInputDataContinue = {
                        "Shipment": {
                            "ShipmentKey": shipmentKey,
                            "AssignedToUserId": storeUserID,
                            "StartOver": "N"
                        }
                    };
                    _iasUIUtils.callApi(this, startInputDataContinue, "extn_baggingStartOver_ref", null);
                } else if (_scBaseUtils.equals("CLOSEPOPUP", actionPerformed)) {
                    var initialInput = {};
                    _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", shipmentKey, initialInput);
                    _iasUIUtils.openWizardInEditor("wsc.components.shipment.summary.ShipmentSummaryWizard", initialInput, "wsc.desktop.editors.ShipmentEditor", this);
                }
            }
        },
        //overriding the OOTB customer pick method
        startCustomerPick: function(event, bEvent, ctrl, args) {
            var shipmentDetailsModel = null;
            shipmentDetailsModel = _scScreenUtils.getModel(this, "getShipmentDetails_output");
            var deliveryMethod = null;
            deliveryMethod = _scModelUtils.getStringValueFromPath("Shipment.DeliveryMethod", shipmentDetailsModel);
            if (!(_scBaseUtils.equals(deliveryMethod, "PICK"))) {
                _iasBaseTemplateUtils.showMessage(this, _scScreenUtils.getString(this, "Message_OrderCannotBePick"), "error", null);
            } else {
                var shipmentModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");;
                var shipKey = _scModelUtils.getStringValueFromPath("Shipment.ShipmentKey", shipmentDetailsModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", shipKey, shipmentModel);
                // shipmentModel = _scScreenUtils.getTargetModel(
                // this, "recordCustomerPick_Input", null);
                _iasUIUtils.openWizardInEditor("wsc.components.shipment.customerpickup.CustomerPickUpWizard", shipmentModel, "wsc.desktop.editors.ShipmentEditor", this);
            }
        }
    });
});
