scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/text", "scbase/loader!extn/components/shipment/summary/ShipmentSummaryExtn", "scbase/loader!sc/plat/dojo/controller/ExtnServerDataController"], function(_dojodeclare, _dojokernel, _dojotext, _extnShipmentSummaryExtn, _scExtnServerDataController) {
    return _dojodeclare("extn.components.shipment.summary.ShipmentSummaryExtnBehaviorController", [_scExtnServerDataController], {
        screenId: 'extn.components.shipment.summary.ShipmentSummaryExtn',
        mashupRefs: [{
            extnType: 'ADD',
            mashupId: 'shipSummary_getShipmentLineList',
            mashupRefId: 'getShipmentLineList'
        }, {
            extnType: 'ADD',
            mashupId: 'shipSummary_getShipmentContainerList',
            mashupRefId: 'getShipmentContainerList'
        }, {
            extnType: 'ADD',
            mashupId: 'unpack_deleteContainerForSummary',
            mashupRefId: 'unpack_deleteContainer_refid'
        }, {
            extnType: 'ADD',
            mashupId: 'common_changeShipmentStatus',
            mashupRefId: 'extn_baggingStatusChange_ref'
        }, {
            extnType: 'ADD',
            mashupId: 'extn_baggingStartOver',
            mashupRefId: 'extn_baggingStartOver_ref'
        }]
    });
});
