scDefine(["dojo/text!./templates/ShipmentSummaryExtn.html", "scbase/loader!dijit/form/Button", "scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/_base/lang", "scbase/loader!dojo/text", "scbase/loader!sc/plat", "scbase/loader!sc/plat/dojo/binding/ButtonDataBinder", "scbase/loader!sc/plat/dojo/binding/CurrencyDataBinder", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/widgets/DataLabel", "scbase/loader!sc/plat/dojo/widgets/Link"], function(templateText, _dijitButton, _dojodeclare, _dojokernel, _dojolang, _dojotext, _scplat, _scButtonDataBinder, _scCurrencyDataBinder, _scBaseUtils, _scDataLabel, _scLink) {
    return _dojodeclare("extn.components.shipment.summary.ShipmentSummaryExtnUI", [], {
        templateString: templateText,
        hotKeys: [],
        events: [],
        subscribers: {
            local: [{
                eventId: 'afterScreenInit',
                sequence: '51',
                handler: {
                    methodName: "extn_afterScreenInit"
                }
            }, {
                eventId: 'lnkPack_onClick',
                sequence: '19',
                handler: {
                    methodName: "extn_setWizardName"
                }
            }, {
                eventId: 'extn_orderLink_onClick',
                sequence: '51',
                handler: {
                    methodName: "extn_openCallCenter"
                }
            }, {
                eventId: 'extn_startBagging_onClick',
                sequence: '51',
                handler: {
                    methodName: "extn_startBagging"
                }
            }, {
                eventId: 'extn_continueBagging_onClick',
                sequence: '51',
                handler: {
                    methodName: "extn_continueBagging"
                }
            }, {
                eventId: 'afterBehaviorMashupCall',
                sequence: '51',
                handler: {
                    methodName: "extn_afterBehaviorMashupCall"
                }
            }],
            global: [{
                eventId: 'baggingSummaryRefresh',
                sequence: '51',
                handler: {
                    methodName: "extn_baggingRefresh"
                }
            }]
        }
    });
});
