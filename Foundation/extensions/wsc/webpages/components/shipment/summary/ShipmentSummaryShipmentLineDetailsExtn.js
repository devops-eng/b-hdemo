scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/components/shipment/summary/ShipmentSummaryShipmentLineDetailsExtnUI", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/widgets/Label", "scbase/loader!sc/plat/dojo/widgets/Link", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/GridxUtils", "scbase/loader!gridx/Grid", "scbase/loader!sc/plat/dojo/binding/GridxDataBinder", "scbase/loader!ias/utils/RepeatingScreenUtils", "scbase/loader!sc/plat/dojo/utils/RepeatingPanelUtils", "scbase/loader!ias/utils/ScreenUtils", "scbase/loader!ias/widgets/IASBaseScreen", "scbase/loader!ias/utils/EventUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!dojo/currency", "scbase/loader!wsc/components/shipment/common/utils/ShipmentUtils", "scbase/loader!wsc/components/product/common/utils/ProductUtils"], function(_dojodeclare, _extnShipmentSummaryShipmentLineDetailsExtnUI, _idxContentPane, _scBaseUtils, _scEventUtils, _scLabel, _scLink, _scScreen, _scModelUtils, _iasUIUtils, _scScreenUtils, _iasBaseTemplateUtils, _scWidgetUtils, _scEventUtils, _scGridxUtils, _gridxGrid, _scGridxDataBinder, _iasRepeatingScreenUtils, _scRepeatingPanelUtils, _iasScreenUtils, _iasIASBaseScreen, _iasEventUtils, _extnExpicientUtils, _iasContextUtils, dCurrency, _wscShipmentUtils, _wscProductUtils) {
    return _dojodeclare("extn.components.shipment.summary.ShipmentSummaryShipmentLineDetailsExtn", [_extnShipmentSummaryShipmentLineDetailsExtnUI], {
        // custom code here
        extn_getQuantity: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
            var units = Math.floor(dataValue);
            return units;
        },
        extn_getOrderedQuantity: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
            var retValue = null;
            retValue = _scModelUtils.getStringValueFromPath("ShipmentLine.Quantity", shipmentLineModel);
            retValue = _wscShipmentUtils.getFormattedDisplayQuantity(retValue, this, widget, nameSpace, shipmentLineModel, options);
            return retValue;
        },
        extn_getColor: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
            var color = null;
            if (!_scBaseUtils.isVoid(shipmentLineModel)) {
                var attributeModel = _scModelUtils.getModelObjectFromPath("ShipmentLine.OrderLine.ItemDetails.AdditionalAttributeList.AdditionalAttribute", shipmentLineModel);
                if (!_scBaseUtils.isVoid(attributeModel)) {
                    for (var i = 0; i < attributeModel.length; i++) {
                        var refrenceObj = _scBaseUtils.getArrayItemByIndex(attributeModel, i);
                        var name = _scModelUtils.getStringValueFromPath("Name", refrenceObj);
                        if (_scBaseUtils.equals(name, "Color")) {
                            color = _scModelUtils.getStringValueFromPath("Value", refrenceObj);
                        }
                    }
                }
            }
            return color;
        },
        extn_getPrice: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
            var price = null;
            var country = null;
            var listprice = null;
            dataValue = _scModelUtils.getModelObjectFromPath("ShipmentLine.Extn.ExtnItemListPrice", shipmentLineModel);
            if (!_scBaseUtils.isVoid(shipmentLineModel)) {
                var currencyModel = _scModelUtils.getModelObjectFromPath("ShipmentLine.Shipment", shipmentLineModel);
                if (!_scBaseUtils.isVoid(currencyModel)) {
                    var refrenceObj = _scBaseUtils.getArrayItemByIndex(currencyModel, 0);
                    country = _scModelUtils.getStringValueFromPath("Currency", refrenceObj);
                    if (!_scBaseUtils.isVoid(country)) {
                        if (!_scBaseUtils.isVoid(dataValue)) {
                            listprice = dataValue;
                            price = dCurrency.format(parseFloat(listprice), {
                                currency: country
                            });
                        } else {
                            price = "";
                            return "";
                        }
                        var listPrice = {
                            "price": price
                        };
                        _scScreenUtils.setModel(this, "ListPrice", listPrice);
                    }
                }
            }
            return price;
        },
        extn_getSize: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
            var size = null;
            if (!_scBaseUtils.isVoid(shipmentLineModel)) {
                var attributeModel = _scModelUtils.getModelObjectFromPath("ShipmentLine.OrderLine.ItemDetails.AdditionalAttributeList.AdditionalAttribute", shipmentLineModel);
                if (!_scBaseUtils.isVoid(attributeModel)) {
                    for (var i = 0; i < attributeModel.length; i++) {
                        var refrenceObj = _scBaseUtils.getArrayItemByIndex(attributeModel, i);
                        var name = _scModelUtils.getStringValueFromPath("Name", refrenceObj);
                        if (_scBaseUtils.equals(name, "Size")) {
                            size = _scModelUtils.getStringValueFromPath("Value", refrenceObj);
                        }
                    }
                }
            }
            return size;
        },
        extn_getQty: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
            var itemID = null;
            var unitOfMeasure = null;
            var itemModel = null;
            if (!_scBaseUtils.isVoid(shipmentLineModel)) {
                itemModel = _scModelUtils.getModelObjectFromPath("ShipmentLine.OrderLine.ItemDetails", shipmentLineModel);
                if (!_scBaseUtils.isVoid(itemModel)) {
                    itemID = _scModelUtils.getStringValueFromPath("ItemID", itemModel);
                    unitOfMeasure = _scModelUtils.getStringValueFromPath("UnitOfMeasure", itemModel);
                    productClass = _scModelUtils.getStringValueFromPath("ProductClass", itemModel);
                }
                var shipNode = null;
                shipNode = _scModelUtils.getStringValueFromPath("ShipmentLine.OrderLine.ShipNode", shipmentLineModel);
                var organization = null;
                organization = _scModelUtils.getStringValueFromPath("ShipmentLine.OrderLine.ItemDetails.OrganizationCode", shipmentLineModel);
                var model = null;
                model = _scBaseUtils.getNewModelInstance();
                _scModelUtils.setStringValueAtModelPath("InventorySupply.OrganizationCode", organization, model);
                _scModelUtils.setStringValueAtModelPath("InventorySupply.ShipNode", shipNode, model);
                _scModelUtils.setStringValueAtModelPath("InventorySupply.ItemID", itemID, model);
                _scModelUtils.setStringValueAtModelPath("InventorySupply.UnitOfMeasure", unitOfMeasure, model);
                _scModelUtils.setStringValueAtModelPath("InventorySupply.ProductClass", productClass, model);
                _scModelUtils.setStringValueAtModelPath("InventorySupply.SupplyType", "ONHAND", model);
                _iasUIUtils.callApi(this, model, "extn_getQtyOnHand_ref", null);
            }
        },
        extn_afterBehaviorMashupCall: function(event, bEvent, ctrl, args) {
            var isMashupAvailable = _extnExpicientUtils.isMashupAvailable("extn_getQtyOnHand_ref", args);
            if (isMashupAvailable) {
                var getItemListModelOutput = null;
                getItemListModelOutput = _extnExpicientUtils.getMashupOutputForRefID("extn_getQtyOnHand_ref", args);
                if (!_scBaseUtils.isVoid(getItemListModelOutput)) {
                    var itemModel = null;
                    itemModel = _scModelUtils.getModelObjectFromPath("Item.Supplies.InventorySupply", getItemListModelOutput);
                    var itemID = null;
                    if (!_scBaseUtils.isVoid(itemModel)) {
                        var availableQuantity = null;
                        availableQuantity = _scModelUtils.getStringValueFromPath("Quantity", itemModel);
                        if (!_scBaseUtils.isVoid(availableQuantity)) {
                            _scWidgetUtils.setValue(this, "extn_qtyHand", parseInt(availableQuantity));
                        }
                    }
                }
            }
        },
        extn_getUPC: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
            var aliasValue = null;
            if (!_scBaseUtils.isVoid(shipmentLineModel)) {
                var aliasModel = _scModelUtils.getModelObjectFromPath("ShipmentLine.OrderLine.ItemDetails.ItemAliasList.ItemAlias", shipmentLineModel);
                if (!_scBaseUtils.isVoid(aliasModel)) {
                    for (var i = 0; i < aliasModel.length; i++) {
                        var refrenceObj = _scBaseUtils.getArrayItemByIndex(aliasModel, i);
                        var aliasName = _scModelUtils.getStringValueFromPath("AliasName", refrenceObj);
                        if (_scBaseUtils.equals(aliasName, "ACTIVE_UPC")) {
                            aliasValue = _scModelUtils.getStringValueFromPath("AliasValue", refrenceObj);
                        }
                    }
                }
            }
            return aliasValue;
        },
        extn_openItemDetails: function(event, bEvent, ctrl, args) {
            _scEventUtils.stopEvent(bEvent);
            var itemDetailsInput = null;
            itemDetailsInput = _scScreenUtils.getTargetModel(this, "itemDetailsModel", null);
            var priceModel = _scScreenUtils.getModel(this, "ListPrice");
            if (priceModel && priceModel.price) {
                _scModelUtils.setStringValueAtModelPath("Price", priceModel.price, itemDetailsInput);
            } else {
                _scModelUtils.setStringValueAtModelPath("Price", "", itemDetailsInput);
            }
            _wscProductUtils.openItemDetails(this, itemDetailsInput);
        }
    });
});
