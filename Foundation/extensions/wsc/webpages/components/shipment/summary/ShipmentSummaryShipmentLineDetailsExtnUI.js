scDefine(["dojo/text!./templates/ShipmentSummaryShipmentLineDetailsExtn.html", "scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/_base/lang", "scbase/loader!dojo/text", "scbase/loader!sc/plat", "scbase/loader!sc/plat/dojo/binding/CurrencyDataBinder", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/widgets/DataLabel"], function(templateText, _dojodeclare, _dojokernel, _dojolang, _dojotext, _scplat, _scCurrencyDataBinder, _scBaseUtils, _scDataLabel) {
    return _dojodeclare("extn.components.shipment.summary.ShipmentSummaryShipmentLineDetailsExtnUI", [], {
        templateString: templateText,
        hotKeys: [],
        events: [],
        subscribers: {
            local: [{
                eventId: 'afterScreenInit',
                sequence: '51',
                handler: {
                    methodName: "extn_afterScreenInit"
                }
            }, {
                eventId: 'afterBehaviorMashupCall',
                sequence: '51',
                handler: {
                    methodName: "extn_afterBehaviorMashupCall"
                }
            }, {
                eventId: 'itemdescriptionLink_onClick',
                sequence: '19',
                handler: {
                    methodName: "extn_openItemDetails"
                }
            }, {
                eventId: 'itemImage_imageClick',
                sequence: '19',
                handler: {
                    methodName: "extn_openItemDetails"
                }
            }]
        }
    });
});
