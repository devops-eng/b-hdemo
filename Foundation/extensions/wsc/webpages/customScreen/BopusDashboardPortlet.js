scDefine(["dojo/text!./templates/BopusDashboardPortlet.html", "scbase/loader!dijit/form/Button", "scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/_base/lang", "scbase/loader!dojo/text", "scbase/loader!ias/widgets/IASBasePortlet", "scbase/loader!idx/form/TextBox", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat", "scbase/loader!sc/plat/dojo/binding/ButtonDataBinder", "scbase/loader!sc/plat/dojo/binding/CurrencyDataBinder", "scbase/loader!sc/plat/dojo/binding/ImageDataBinder", "scbase/loader!sc/plat/dojo/binding/SimpleDataBinder", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/widgets/Image", "scbase/loader!sc/plat/dojo/widgets/Label", "scbase/loader!sc/plat/dojo/widgets/Link", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EditorUtils", "scbase/loader!ias/utils/ContextUtils"], function(templateText, _dijitButton, _dojodeclare, _dojokernel, _dojolang, _dojotext, _iasIASBasePortlet, _idxTextBox, _idxContentPane, _scplat, _scButtonDataBinder, _scCurrencyDataBinder, _scImageDataBinder, _scSimpleDataBinder, _scBaseUtils, _scScreenUtils, _scImage, _scLabel, _scLink, _iasUIUtils, _scModelUtils, _scWidgetUtils, _scEditorUtils, _iasContextUtils) {
    return _dojodeclare("extn.customScreen.BopusDashboardPortlet", [_iasIASBasePortlet], {
        templateString: templateText,
        uId: "BopusDashboardPortlet",
        packageName: "extn.customScreen",
        className: "BopusDashboardPortlet",
        title: "BopusDashboardPortlet",
        screen_description: "This screen is used as Bopus Dashboard portlet",
        rootInputElement: '',
        firstClickEvent: 'true',
        isDirtyCheckRequired: true,
        panelApiMashupId: '',
        namespaces: {
            targetBindingNamespaces: [{
                value: 'getShipmentList_input',
                description: "Input for custom mashup - used to validate OrderNo or ShipmentNo"
            }],
            sourceBindingNamespaces: [{
                value: 'portletTitle',
                description: "This namespace is used to store and set the title of the portlet"
            }, {
                value: 'labels',
                description: "labels for buttons"
            }, {
                value: 'pickCount_output',
                description: "output for getShipmentList API - count of pick orders ready for pick"
            }, {
                value: 'shipCount_output',
                description: "output for getShipmentList API - count of ship orders ready for pick"
            }, {
                value: 'getShipmentList_output',
                description: "output for custom mashup -- either getShipmentListForOrder or getShipmentList"
            }, {
                value: 'printPickTicket_output',
                description: "output for print pick ticket service - document to be printed"
            }, {
                value: 'getBopusDashboardCount_output',
                description: "output for getting bopus dashboard count"
            }]
        },
        hotKeys: [],
        events: [{
            name: 'onClick',
            originatingControlUId: 'portletHeaderPanel'
        }],
        subscribers: {
            local: [{
                eventId: 'portletHeaderPanel_onClick',
                sequence: '30',
                description: 'This event is fired when the portlet header is clicked.',
                handler: {
                    methodName: "clickPortletPnlEvent"
                }
            }, {
                eventId: 'afterScreenInit',
                sequence: '55',
                handler: {
                    methodName: "initializeScreenTemplate"
                }
            }, {
                eventId: 'afterScreenInit',
                sequence: '30',
                description: 'upon init',
                handler: {
                    methodName: "initializeScreen"
                }
            }, {
                eventId: 'txtOrderNumber_onKeyDown',
                sequence: '30',
                description: 'for catching enter on order number field',
                handler: {
                    methodName: "recordPickActionOnEnter"
                }
            }, {
                eventId: 'advanceSearch_onClick',
                sequence: '25',
                description: 'user clicks advanced search',
                listeningControlUId: 'advanceSearch',
                handler: {
                    methodName: "advanceSearchAction",
                    description: ""
                }
            }, {
                eventId: 'lnkRefresh_onClick',
                sequence: '25',
                description: 'user clicks refresh',
                listeningControlUId: 'lnkRefresh',
                handler: {
                    methodName: "refreshPrint",
                    description: ""
                }
            }, {
                eventId: 'btnPrintPickup_onClick',
                sequence: '25',
                description: 'user clicks print pickup orders',
                listeningControlUId: 'btnPrintPickup',
                handler: {
                    methodName: "printPickup",
                    description: ""
                }
            }, {
                eventId: 'btnPrintShipping_onClick',
                sequence: '25',
                description: 'user clicks print shipout orders',
                listeningControlUId: 'btnPrintShipping',
                handler: {
                    methodName: "printShipout",
                    description: ""
                }
            }, {
                eventId: 'inProgressButton_onClick',
                sequence: '25',
                description: 'user clicks on progress button',
                listeningControlUId: 'inProgressButton',
                handler: {
                    methodName: "listInProgress",
                    description: ""
                }
            }, {
                eventId: 'missedSLAButton_onClick',
                sequence: '25',
                description: 'user clicks on missed SLA button',
                listeningControlUId: 'missedSLAButton',
                handler: {
                    methodName: "listMissedSLA",
                    description: ""
                }
            }, {
                eventId: 'nearingSLAButton_onClick',
                sequence: '25',
                description: 'user clicks on nearing SLA button',
                listeningControlUId: 'nearingSLAButton',
                handler: {
                    methodName: "listNearingSLA",
                    description: ""
                }
            }, {
                eventId: 'newShipmentButton_onClick',
                sequence: '25',
                description: 'user clicks on new shipment button',
                listeningControlUId: 'newShipmentButton',
                handler: {
                    methodName: "listNewShipment",
                    description: ""
                }
            }, {
                eventId: 'restockingButton_onClick',
                sequence: '25',
                description: 'user clicks on restocking button',
                listeningControlUId: 'restockingButton',
                handler: {
                    methodName: "listRestocking",
                    description: ""
                }
            }, {
                eventId: 'btnRecordPick_onClick',
                sequence: '25',
                description: 'user clicks record pick',
                listeningControlUId: 'btnRecordPick',
                handler: {
                    methodName: "recordPickIfPermitted",
                    description: ""
                }
            }],
            global: [{
                eventId: 'extn_refreshBopusDashboard',
                sequence: '51',
                handler: {
                    methodName: "getDashboardCount"
                }
            }]
        },
        // custom code here
        initializeScreen: function(event, bEvent, ctrl, args) {
            this.getDashboardCount();
            // this.setCountToLabels();
            // if (_scResourcePermissionUtils.hasPermission("WSC000009") || _scResourcePermissionUtils.hasPermission("WSC000021")) {
            //     _scWidgetUtils.showWidget(this, "lnkRefresh", false, null);
            //     _scWidgetUtils.showWidget(this, "pnlPrintButtons", false, null);
            // }
            // if (_scResourcePermissionUtils.hasPermission("WSC000009") && _scResourcePermissionUtils.hasPermission("WSC000021")) {
            //     _scWidgetUtils.showWidget(this, "btnPrintAll", false, null);
            // }
        },
        getDashboardCount: function(event, bEvent, ctrl, args) {
            var shipNode = _iasContextUtils.getFromContext("CurrentStore");
            var inputModel = {
                "Shipment": {
                    "ShipNode": shipNode
                }
            };
            /*            var alertFlag= _scModelUtils.getStringValueFromPath("Shipment.IsDashBoardAlert", event);
                        if(!_scBaseUtils.isVoid(alertFlag)){
                            if(_scBaseUtils.equals(alertFlag, "Y")){
                                _scModelUtils.setStringValueAtModelPath("Shipment.IsDashBoardAlert", "Y", inputModel);
                            }
                        }*/
            _iasUIUtils.callApi(this, inputModel, "getBopusDashboardCount_ref", null);
        },
        setCountToLabels: function() {
            var labels = null;
            labels = _scModelUtils.createNewModelObjectWithRootKey("labels");
            var shipmentCountModel = null;
            shipmentCountModel = _scScreenUtils.getModel(this, "pickCount_output");
            var numOfRecords = "0";
            if (_scModelUtils.getStringValueFromPath("Shipments.TotalNumberOfRecords", shipmentCountModel)) {
                numOfRecords = _scModelUtils.getStringValueFromPath("Shipments.TotalNumberOfRecords", shipmentCountModel);
            }
            var completeTasksTitle = null;
            completeTasksTitle = this.addCountToLabel(numOfRecords, "Action_PrintPickupOrders");
            _scModelUtils.setStringValueAtModelPath("labels.printPickup", completeTasksTitle, labels);
            shipmentCountModel = null;
            shipmentCountModel = _scScreenUtils.getModel(this, "shipCount_output");
            numOfRecords = "0";
            if (_scModelUtils.getStringValueFromPath("Shipments.TotalNumberOfRecords", shipmentCountModel)) {
                numOfRecords = _scModelUtils.getStringValueFromPath("Shipments.TotalNumberOfRecords", shipmentCountModel);
            }
            completeTasksTitle = null;
            completeTasksTitle = this.addCountToLabel(numOfRecords, "Action_PrintShippingOrders");
            _scModelUtils.setStringValueAtModelPath("labels.printShipout", completeTasksTitle, labels);
            _scBaseUtils.setModel(this, "labels", labels, null);
        },
        addCountToLabel: function(num, label) {
            var inputArray = null;
            inputArray = [];
            inputArray.push(num);
            var completePortletTitle = null;
            completePortletTitle = _scScreenUtils.getFormattedString(this, label, inputArray);
            return completePortletTitle;
        },
        advanceSearchAction: function(event, bEvent, ctrl, args) {
            var shipmentModel = null;
            shipmentModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
            _scModelUtils.setStringValueAtModelPath("Shipment.DeliveryMethod", " ", shipmentModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.NoDefaultStatuses", "true", shipmentModel);
            _iasUIUtils.openWizardInEditor("wsc.components.shipment.search.ShipmentSearchWizard", shipmentModel, "wsc.desktop.editors.ShipmentSearchEditor", this, null);
        },
        noShipmentsToPrint: function() {
            var warningString = null;
            warningString = _scScreenUtils.getString(this, "NoShipmentsToPrint");
            var textObj = null;
            textObj = {};
            var textOK = null;
            textOK = _scScreenUtils.getString(this, "Ok");
            textObj["OK"] = textOK;
            _scScreenUtils.showWarningMessageBoxWithOk(this, warningString, "waringCallback", textObj, null);
        },
        printPickup: function(event, bEvent, ctrl, args) {
            var printActionModel = null;
            printActionModel = {};
            _scModelUtils.setStringValueAtModelPath("Print.DeliveryMethod", "PICK", printActionModel);
            _scModelUtils.setStringValueAtModelPath("Print.Action", "PrintPick", printActionModel);
            var shipmentCountModel = null;
            shipmentCountModel = _scScreenUtils.getModel(this, "pickCount_output");
            var numOfRecords = 0;
            if (_scModelUtils.getNumberValueFromPath("Shipments.TotalNumberOfRecords", shipmentCountModel)) {
                numOfRecords = _scModelUtils.getNumberValueFromPath("Shipments.TotalNumberOfRecords", shipmentCountModel);
            }
            if (_scBaseUtils.equals(numOfRecords, 0)) {
                this.noShipmentsToPrint();
            } else if (numOfRecords >= 100) {
                _scScreenUtils.showConfirmMessageBox(this, _scScreenUtils.getString(this, "Message_PrintLargeOrderConfirmation"), "printOrders", null, printActionModel);
            } else {
                this.printOrders("Ok", printActionModel);
            }
        },
        recordPickIfPermitted: function(event, bEvent, ctrl, args) {
            if (_scResourcePermissionUtils.hasPermission("WSC000008")) {
                this.recordPick(event, bEvent, ctrl, args);
            } else {
                _iasBaseTemplateUtils.showMessage(this, _scScreenUtils.getString(this, "Label_NoSearchPermission"), "error", null);
            }
        },
        recordPickActionOnEnter: function(event, bEvent, ctrl, args) {
            if (_iasEventUtils.isEnterPressed(event)) {
                this.recordPickIfPermitted(event, bEvent, ctrl, args);
            }
        },
        recordPick: function(event, bEvent, ctrl, args) {
            var order = null;
            order = _scScreenUtils.getTargetModel(this, "getShipmentList_input", null);
            var shipmentNo = null;
            shipmentNo = _scModelUtils.getStringValueFromPath("Shipment.ShipmentNo", order);
            if (_scBaseUtils.isVoid(shipmentNo)) {
                var modelOutput = null;
                modelOutput = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
                _scModelUtils.setStringValueAtModelPath("Shipment.DeliveryMethod", " ", modelOutput);
                _scModelUtils.setStringValueAtModelPath("Shipment.NoDefaultStatuses", "true", modelOutput);
                _iasUIUtils.openWizardInEditor("wsc.components.shipment.search.ShipmentSearchWizard", modelOutput, "wsc.desktop.editors.ShipmentSearchEditor", this, null);
            } else {
                _scModelUtils.setStringValueAtModelPath("Shipment.Action", "BackroomPick", order);
                _iasUIUtils.callApi(this, order, "getShipmentList", null);
                this.resetFields();
            }
        },
        refreshPrint: function(event, bEvent, ctrl, args) {
            this.getDashboardCount();
        },
        listInProgress: function(event, bEvent, ctrl, args) {
            var modelOutput = null;
            modelOutput = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
            _scModelUtils.setStringValueAtModelPath("Shipment.DeliveryMethod", "PICK", modelOutput);
            _scModelUtils.setStringValueAtModelPath("Shipment.DoSearchOnInit", "true", modelOutput);
            _scModelUtils.setStringValueAtModelPath("Shipment.NoDefaultStatuses", "false", modelOutput);
            _scModelUtils.setStringValueAtModelPath("Shipment.BopusSearch", "InProgress", modelOutput);
            _iasUIUtils.openWizardInEditor("wsc.components.shipment.search.ShipmentSearchWizard", modelOutput, "wsc.desktop.editors.ShipmentSearchEditor", this, null);
        },
        listMissedSLA: function(event, bEvent, ctrl, args) {
            var modelOutput = null;
            modelOutput = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
            _scModelUtils.setStringValueAtModelPath("Shipment.BopusSearch", "MissedSLA", modelOutput);
            _scModelUtils.setStringValueAtModelPath("Shipment.DoSearchOnInit", "true", modelOutput);
            _scModelUtils.setStringValueAtModelPath("Shipment.NoDefaultStatuses", "false", modelOutput);
            _iasUIUtils.openWizardInEditor("wsc.components.shipment.search.ShipmentSearchWizard", modelOutput, "wsc.desktop.editors.ShipmentSearchEditor", this, null);
        },
        listNearingSLA: function(event, bEvent, ctrl, args) {
            var modelOutput = null;
            modelOutput = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
            _scModelUtils.setStringValueAtModelPath("Shipment.BopusSearch", "NearingSLA", modelOutput);
            _scModelUtils.setStringValueAtModelPath("Shipment.DoSearchOnInit", "true", modelOutput);
            _scModelUtils.setStringValueAtModelPath("Shipment.NoDefaultStatuses", "false", modelOutput);
            _iasUIUtils.openWizardInEditor("wsc.components.shipment.search.ShipmentSearchWizard", modelOutput, "wsc.desktop.editors.ShipmentSearchEditor", this, null);
        },
        listNewShipment: function(event, bEvent, ctrl, args) {
            var modelOutput = null;
            modelOutput = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
            _scModelUtils.setStringValueAtModelPath("Shipment.DeliveryMethod", "PICK", modelOutput);
            _scModelUtils.setStringValueAtModelPath("Shipment.DoSearchOnInit", "true", modelOutput);
            _scModelUtils.setStringValueAtModelPath("Shipment.NoDefaultStatuses", "false", modelOutput);
            _scModelUtils.setStringValueAtModelPath("Shipment.BopusSearch", "NewShipment", modelOutput);
            _iasUIUtils.openWizardInEditor("wsc.components.shipment.search.ShipmentSearchWizard", modelOutput, "wsc.desktop.editors.ShipmentSearchEditor", this, null);
        },
        listRestocking: function(event, bEvent, ctrl, args) {
            // var modelOutput = null;
            // modelOutput = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
            // _scModelUtils.setStringValueAtModelPath("Shipment.BopusSearch", "Restocking", modelOutput);
            // _scModelUtils.setStringValueAtModelPath("Shipment.NoDefaultStatuses", "true", modelOutput);
            // _iasUIUtils.openWizardInEditor("wsc.components.shipment.search.ShipmentSearchWizard", modelOutput, "wsc.desktop.editors.ShipmentSearchEditor", this, null);
            var allOpenEditors = _scEditorUtils.getSpecificOpenEditors("extn.customScreen.restocking.editors.restockingEditor");
            for (var i = 0; i < allOpenEditors.length; i++) {
                var currEditor = _scBaseUtils.getArrayItemByIndex(allOpenEditors, i);
                var currEditorInstance = currEditor.editorInstance;
                _scEditorUtils.closeEditor(currEditorInstance);
            }
            _iasUIUtils.openWizardInEditor("extn.customScreen.restocking.wizard.restockingWizard", null, "extn.customScreen.restocking.editors.restockingEditor", this, null);
        },
        handleMashupOutput: function(mashupRefId, modelOutput, mashupInput, mashupContext, applySetModel) {
            if (_scBaseUtils.equals(mashupRefId, "getBopusDashboardCount_ref")) {
                if (modelOutput && modelOutput.Shipment) {
                    var inProgress = null;
                    inProgress = _scModelUtils.getModelObjectFromPath("Shipment.InProgress", modelOutput);
                    var missedSLA = null;
                    missedSLA = _scModelUtils.getModelObjectFromPath("Shipment.MissedSLA", modelOutput);
                    var nearingSLA = null;
                    nearingSLA = _scModelUtils.getModelObjectFromPath("Shipment.NearingSLA", modelOutput);
                    var newShipment = null;
                    newShipment = _scModelUtils.getModelObjectFromPath("Shipment.NewShipment", modelOutput);
                    var restocking = null;
                    restocking = _scModelUtils.getModelObjectFromPath("Shipment.Restocking", modelOutput);
                    if (inProgress) {
                        var inProgressCount = _scModelUtils.getStringValueFromPath("TotalNumberOfRecords", inProgress);
                        var inProgressLabel = this.getSimpleBundleString("inProgressButton");
                        if ((!_scBaseUtils.isVoid(inProgressCount)) && _scBaseUtils.numberGreaterThan(inProgressCount, 0)) {
                            inProgressLabel = inProgressLabel + " " + "(" + inProgressCount + ")";
                            _scWidgetUtils.setLabel(this, "inProgressButton", inProgressLabel);
                        } else {
                            inProgressLabel = inProgressLabel + " " + "(0)";
                            _scWidgetUtils.setLabel(this, "inProgressButton", inProgressLabel);
                        }
                    }
                    if (missedSLA) {
                        var missedSLACount = _scModelUtils.getStringValueFromPath("TotalNumberOfRecords", missedSLA);
                        var missedSLALabel = this.getSimpleBundleString("missedSLAButton");
                        if ((!_scBaseUtils.isVoid(missedSLACount)) && _scBaseUtils.numberGreaterThan(missedSLACount, 0)) {
                            missedSLALabel = missedSLALabel + " " + "(" + missedSLACount + ")";
                            _scWidgetUtils.setLabel(this, "missedSLAButton", missedSLALabel);
                        } else {
                            missedSLALabel = missedSLALabel + " " + "(0)";
                            _scWidgetUtils.setLabel(this, "missedSLAButton", missedSLALabel);
                        }
                    }
                    if (nearingSLA) {
                        var nearingSLACount = _scModelUtils.getStringValueFromPath("TotalNumberOfRecords", nearingSLA);
                        var nearingSLALabel = this.getSimpleBundleString("nearingSLAButton");
                        if ((!_scBaseUtils.isVoid(nearingSLACount)) && _scBaseUtils.numberGreaterThan(nearingSLACount, 0)) {
                            nearingSLALabel = nearingSLALabel + " " + "(" + nearingSLACount + ")";
                            _scWidgetUtils.setLabel(this, "nearingSLAButton", nearingSLALabel);
                        } else {
                            nearingSLALabel = nearingSLALabel + " " + "(0)";
                            _scWidgetUtils.setLabel(this, "nearingSLAButton", nearingSLALabel);
                        }
                    }
                    if (newShipment) {
                        var newShipmentCount = _scModelUtils.getStringValueFromPath("TotalNumberOfRecords", newShipment);
                        var newShipmentLabel = this.getSimpleBundleString("newShipmentButton");
                        if ((!_scBaseUtils.isVoid(newShipmentCount)) && _scBaseUtils.numberGreaterThan(newShipmentCount, 0)) {
                            newShipmentLabel = newShipmentLabel + " " + "(" + newShipmentCount + ")";
                            _scWidgetUtils.setLabel(this, "newShipmentButton", newShipmentLabel);
                        } else {
                            newShipmentLabel = newShipmentLabel + " " + "(0)";
                            _scWidgetUtils.setLabel(this, "newShipmentButton", newShipmentLabel);
                        }
                    }
                    if (restocking) {
                        var restockingCount = _scModelUtils.getStringValueFromPath("TotalNumberOfRecords", restocking);
                        var restockingLabel = this.getSimpleBundleString("restockingButton");
                        if ((!_scBaseUtils.isVoid(restockingCount)) && _scBaseUtils.numberGreaterThan(restockingCount, 0)) {
                            restockingLabel = restockingLabel + " " + "(" + restockingCount + ")";
                            _scWidgetUtils.setLabel(this, "restockingButton", restockingLabel);
                        } else {
                            restockingLabel = restockingLabel + " " + "(0)";
                            _scWidgetUtils.setLabel(this, "restockingButton", restockingLabel);
                        }
                    }
                }
                if (!(_scBaseUtils.equals(false, applySetModel))) {
                    _scScreenUtils.setModel(this, "pickCount_output", modelOutput, null);
                }
            }
            if (_scBaseUtils.equals(mashupRefId, "getShipmentListReadyForShip")) {
                if (!(_scBaseUtils.equals(false, applySetModel))) {
                    _scScreenUtils.setModel(this, "shipCount_output", modelOutput, null);
                }
                this.setCountToLabels();
            }
            if (_scBaseUtils.equals(mashupRefId, "getShipmentList")) {
                if (!(_scBaseUtils.equals(false, applySetModel))) {
                    _scScreenUtils.setModel(this, "getShipmentList_output", modelOutput, null);
                }
                this.processShipmentListByOutput(modelOutput, mashupInput);
            }
            if (_scBaseUtils.equals(mashupRefId, "printPickTicket")) {
                if (!(_scBaseUtils.equals(false, applySetModel))) {
                    _scScreenUtils.setModel(this, "printPickTicket_output", modelOutput, null);
                }
                this.printAndRefresh(modelOutput, mashupContext);
            }
        },
    });
});
