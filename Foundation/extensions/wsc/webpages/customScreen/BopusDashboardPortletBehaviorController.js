scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/text", "scbase/loader!sc/plat/dojo/controller/ServerDataController", "scbase/loader!extn/customScreen/BopusDashboardPortlet"], function(_dojodeclare, _dojokernel, _dojotext, _scServerDataController, _BopusDashboardPortlet) {
    return _dojodeclare("extn.customScreen.BopusDashboardPortletBehaviorController", [_scServerDataController], {
        screenId: 'extn.customScreen.BopusDashboardPortlet',
        mashupRefs: [{
                mashupRefId: 'getBopusDashboardCount_ref',
                mashupId: 'getBopusDashboardCount'
            }
            // {
            //     mashupId: 'backroomPickPortlet_getShipmentListPickCount',
            //     mashupRefId: 'getShipmentListReadyForPick'
            // }, {
            //     mashupId: 'backroomPickPortlet_getShipmentListShipCount',
            //     mashupRefId: 'getShipmentListReadyForShip'
            // }, {
            //     mashupId: 'portlet_getShipmentList',
            //     mashupRefId: 'getShipmentList'
            // }, {
            //     mashupId: 'backroomPickPortlet_printPickTicket',
            //     mashupRefId: 'printPickTicket'
            // }
        ]
    });
});
