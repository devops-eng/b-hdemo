scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/text", "scbase/loader!sc/plat/dojo/controller/ScreenController", "scbase/loader!extn/customScreen/BopusDashboardPortlet"], function(_dojodeclare, _dojokernel, _dojotext, _scScreenController, _BopusDashboardPortlet) {
    return _dojodeclare("extn.customScreen.BopusDashboardPortletInitController", [_scScreenController], {
        screenId: 'extn.customScreen.BopusDashboardPortlet',
        mashupRefs: [
        {
            sourceNamespace: 'getBopusDashboardCount_output',
            callSequence: '',
            mashupRefId: 'getBopusDashboardCount_ref',
            sequence: '',
            sourceBindingOptions: '',
            mashupId: 'getBopusDashboardCount'
        }
        // , {
        //     sourceNamespace: 'shipCount_output',
        //     callSequence: '',
        //     mashupRefId: 'getShipmentListReadyForShipInit',
        //     sequence: '',
        //     sourceBindingOptions: '',
        //     mashupId: 'backroomPickPortlet_getShipmentListShipCount'
        // }
        ]
    });
});
