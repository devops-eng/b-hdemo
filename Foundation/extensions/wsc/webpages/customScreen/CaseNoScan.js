scDefine(["dojo/text!./templates/CaseNoScan.html", "scbase/loader!dijit/form/Button", "scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/_base/lang", "scbase/loader!dojo/text", "scbase/loader!ias/widgets/IASBaseScreen", "scbase/loader!idx/form/TextBox", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat", "scbase/loader!sc/plat/dojo/binding/ButtonDataBinder", "scbase/loader!sc/plat/dojo/binding/SimpleDataBinder", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!ias/utils/EventUtils", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!sc/plat/dojo/utils/ResourcePermissionUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EditorUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/info/ApplicationInfo", "scbase/loader!wsc/components/shipment/common/utils/ShipmentUtils", "scbase/loader!sc/plat/dojo/Userprefs"], function(templateText, _dijitButton, _dojodeclare, _dojokernel, _dojolang, _dojotext, _iasIASBaseScreen, _idxTextBox, _idxContentPane, _scplat, _scButtonDataBinder, _scSimpleDataBinder, _scBaseUtils, _iasBaseTemplateUtils, _iasUIUtils, _iasEventUtils, _iasContextUtils, _scResourcePermissionUtils, _scScreenUtils, _scModelUtils, _scWidgetUtils, _scEditorUtils, _scEventUtils, _scApplicationInfo, _wscShipmentUtils, _scUserprefs) {
    return _dojodeclare("extn.customScreen.CaseNoScan", [_iasIASBaseScreen], {
        templateString: templateText,
        uId: "CaseNoScan",
        packageName: "extn.customScreen",
        className: "CaseNoScan",
        title: "Screen",
        screen_description: "This is screen is used to paint item pack feature",
        isDirtyCheckRequired: true,
        namespaces: {
            targetBindingNamespaces: [{
                value: 'packItem_input',
                description: "Input for custom mashup - to get a shipment from an item scan"
            }],
            sourceBindingNamespaces: [{
                value: 'CompleteOrderData',
                description: "This model is used for item pack rule"
            }, {
                value: 'initializeModel',
                description: "This model is used to reset the search criteria"
            }, {
                description: 'Stores popup output set from setPopupOutput',
                value: 'popupOutput'
            }]
        },
        staticBindings: [],
        hotKeys: [{
            id: "Popup_btnCancel",
            key: "ESCAPE",
            description: "$(_scSimpleBundle:Close)",
            widgetId: "Popup_btnCancel",
            invocationContext: "",
            category: "$(_scSimpleBundle:General)",
            helpContextId: ""
        }],
        events: [],
        subscribers: {
            local: [{
                eventId: 'afterScreenInit',
                sequence: '30',
                description: 'This event is raised after screen has been initialized',
                handler: {
                    methodName: "initializeScreen"
                }
            }, {
                eventId: 'txtItemID_onKeyUp',
                sequence: '30',
                listeningControlUId: 'txtItemID',
                handler: {
                    methodName: "handleBarcodeScan"
                }
            }, {
                eventId: 'addProductButton_onClick',
                sequence: '30',
                description: 'Subscriber for Scan/Add button',
                listeningControlUId: 'addProductButton',
                handler: {
                    methodName: "productSearch",
                    description: "Handled for Scan product"
                }
            }, {
                eventId: 'txtItemID_onKeyUp',
                sequence: '19',
                listeningControlUId: 'txtItemID',
                handler: {
                    methodName: "handleBarcodeScanSettingWizard"
                }
            }, {
                eventId: 'addProductButton_onClick',
                sequence: '30',
                description: 'Subscriber for Scan/Add button',
                listeningControlUId: 'addProductButton',
                handler: {
                    methodName: "scanProduct",
                    description: "Handled for Scan product"
                }
            }, {
                eventId: 'scanProductIdTxt_onKeyDown',
                sequence: '30',
                description: 'This method is used to handle Key Down event of Scan Product text field.',
                handler: {
                    methodName: "scanProductOnEnter"
                }
            }, {
                eventId: 'addProductButton_onClick',
                sequence: '19',
                description: 'Subscriber for Scan/Add button',
                listeningControlUId: 'addProductButton',
                handler: {
                    methodName: "productSearchSettingWizard",
                    description: "Handled for Scan product"
                }
            }, {
                eventId: 'cancelButton_onClick',
                sequence: '25',
                description: '',
                listeningControlUId: 'cancelButton',
                handler: {
                    methodName: "handleCancel",
                    description: ""
                }
            }]
        },
        handleMashupOutput: function(mashupRefId, modelOutput, mashupInput, mashupContext, applySetModel) {
            if (_scBaseUtils.equals(mashupRefId, "validateBatchPickCaseNumber_ref")) {
                this.checkCaseNumberValidation(modelOutput);
            } else if (_scBaseUtils.equals(mashupRefId, "validateBackRoomPickCaseNumber_ref")) {
                this.checkCaseNumberValidation(modelOutput);
            } else if (_scBaseUtils.equals(mashupRefId, "CancelBackRoomPick_ref")) {
                this.refreshUiScreen(modelOutput);
            } else if (_scBaseUtils.equals(mashupRefId, "cancelBatchPick_ref")) {
                this.refreshUiScreen(modelOutput);
            }
        },
        handleMashupCompletion: function(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data) {
            _iasBaseTemplateUtils.handleMashupCompletion(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data, this);
        },
        checkCaseNumberValidation: function(modelOutput) {
            var isCaseValid = _scModelUtils.getStringValueFromPath("CaseNoValidationResult.isSuccessfulFlag", modelOutput);
            if (_scBaseUtils.equals(isCaseValid, "Y")) {
                var completeOutput = _scScreenUtils.getModel(this, "CompleteOrderData");
                _scModelUtils.setStringValueAtModelPath("RefreshScreen", "N", completeOutput);
                _scWidgetUtils.closePopup(this, "APPLY", false);
            } else {
                var textObj = {};
                textObj["OK"] = "Ok";
                textObj["CANCEL"] = "Cancel";
                //errorMsg = this.getSimpleBundleString("extn_dissolveBatchConfirmationMsg");
                _scScreenUtils.showWarningMessageBox(this, "Your entry is invalid. Tap OK to re-enter the case number.  If issue persists please contact the Help Desk", "caseValidateHandler", null, textObj);
            }
        },
        setPopupOutput: function(model) {
            _scScreenUtils.setModel(this, "popupOutput", model, null);
        },
        getPopupOutput: function(event, bEvent, ctrl, args) {
            var options = null;
            options = _scBaseUtils.getNewBeanInstance();
            _scBaseUtils.addStringValueToBean("allowEmpty", true, options);
            var caseoutput = _scScreenUtils.getModel(this, "CompleteOrderData");
            return caseoutput;
        },
        scanProductOnEnter: function(event, bEvent, ctrl, args) {
            if (_iasEventUtils.isEnterPressed(event)) {
                this.scanProduct();
            }
        },
        scanProduct: function() {
            // _scWidgetUtils.closePopup(this, "CANCEL", false);
            var apiInput = _scScreenUtils.getTargetModel(this, "caseNumber_input", null);
            var completeOrderDetails = _scScreenUtils.getModel(this, "CompleteOrderData");
            var inputOrderDetails = _scScreenUtils.getModel(this, "InputOrderData");
            var caseNo = apiInput.StoreBatch.CaseNo;
            if ((caseNo != parseInt(caseNo, 10)) || _scBaseUtils.lessThanOrEqual(parseInt(caseNo), 0) || (isNaN(caseNo))) {
                _iasBaseTemplateUtils.showMessage(this, "Case number must be a number greater than 0", "error", null);
            } else {
                if (caseNo.length > 11) {
                    _iasBaseTemplateUtils.showMessage(this, "Case number entered must not be greater than 11 character length.", "error", null);
                } else {
                    if (_scBaseUtils.equals(inputOrderDetails.IsBatchPick, "Y")) {
                        _scModelUtils.setStringValueAtModelPath("StoreBatch.Item.CaseNo", apiInput.StoreBatch.CaseNo, inputOrderDetails);
                        // _scModelUtils.setStringValueAtModelPath("StoreBatch.Item.CaseNo", "16548", inputOrderDetails);
                        _iasUIUtils.callApi(this, inputOrderDetails, "validateBatchPickCaseNumber_ref", null);
                    } else {
                        _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLines.ShipmentLine.CaseNo", apiInput.StoreBatch.CaseNo, inputOrderDetails);
                        //_scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLines.ShipmentLine.CaseNo","16548", inputOrderDetails);
                        _iasUIUtils.callApi(this, inputOrderDetails, "validateBackRoomPickCaseNumber_ref", null);
                    }
                }
            }
            //_iasBaseTemplateUtils.showMessage(this,"sdsdsdsds","error",null);
            //errorMsg = this.getSimpleBundleString("extn_dissolveBatchConfirmationMsg");
            //_scScreenUtils.showWarningMessageBox(this, "The case number you entered is invalid.Tab on OK to re-enter the case number or CANCEL to cancel picking", "caseValidateHandler", null, textObj);
        },
        handleCancel: function(event, bEvent, ctrl, args) {
            _scWidgetUtils.closePopup(this, "CLOSE", false);
        },
        refreshUiScreen: function(event, bEvent, ctrl, args) {
            var completeOutput = _scScreenUtils.getModel(this, "CompleteOrderData");
            if (_scBaseUtils.equals(completeOutput.IsBatchPick, "Y")) {
                var BackroomPickedQuantity = _scModelUtils.getStringValueFromPath("StoreBatch.StoreBatchLine.BackroomPickedQuantity", completeOutput);
                _scModelUtils.setStringValueAtModelPath("StoreBatch.StoreBatchLine.BackroomPickedQuantity", BackroomPickedQuantity - 1, completeOutput);
                _scModelUtils.setStringValueAtModelPath("StoreBatch.StoreBatchLine.BackroomPickComplete", "N", completeOutput);
                _scModelUtils.setStringValueAtModelPath("RefreshScreen", "Y", completeOutput);
                var statusCalled = null;
                if (event && event.CaseNoValidationResult && event.CaseNoValidationResult.isStatusChanged == "Y") {
                    statusCalled = "Y";
                } else {
                    statusCalled = "N";
                }

                _scModelUtils.setStringValueAtModelPath("StatusChanged", statusCalled, completeOutput);
                _scUserprefs.setProperty("popUpRefrence", this);
                setTimeout(function() {
                    console.log("setTimeout");
                    _scModelUtils.setStringValueAtModelPath("RefreshScreen", "N", completeOutput);
                    var ref = _scUserprefs.getProperty("popUpRefrence");
                    // _scWidgetUtils.closePopup(ref, "CANCEL", false);
                    _scWidgetUtils.closePopup(ref, "CLOSE", false);
                }, 100);
                _scWidgetUtils.closePopup(this, "CANCEL", false);
            } else {
                if (_scBaseUtils.equals(completeOutput.IsScanMode, "Y")) {
                    var BackroomPickedQuantity = _scModelUtils.getStringValueFromPath("BarCode.Shipment.ShipmentLine.BackroomPickedQuantity", completeOutput);
                    _scModelUtils.setStringValueAtModelPath("BarCode.Shipment.ShipmentLine.BackroomPickedQuantity", BackroomPickedQuantity - 1, completeOutput);
                    var shipmentLine = _scModelUtils.getStringValueFromPath("BarCode.Shipment.ShipmentLine", completeOutput);
                    _scModelUtils.setStringValueAtModelPath("ShipmentLine", shipmentLine, completeOutput);
                } else {
                    var shipmenmtLine = completeOutput.ShipmentLines.ShipmentLine[0];
                    var BackroomPickedQuantity = _scModelUtils.getStringValueFromPath("BackroomPickedQuantity", shipmenmtLine);
                    _scModelUtils.setStringValueAtModelPath("BackroomPickedQuantity", BackroomPickedQuantity - 1, shipmenmtLine);
                    _scModelUtils.setStringValueAtModelPath("ShipmentLine", shipmenmtLine, completeOutput);
                }
                _scModelUtils.setStringValueAtModelPath("RefreshScreen", "Y", completeOutput);
                _scWidgetUtils.closePopup(this, "CANCEL", false);
            }
        },
        caseValidateHandler: function(event, bEvent, ctrl, args) {
            if (_scBaseUtils.equals(event, "Cancel")) {
                var completeOutput = _scScreenUtils.getModel(this, "CompleteOrderData");
                var inputOrderDetails = _scScreenUtils.getModel(this, "InputOrderData");
                if (_scBaseUtils.equals(completeOutput.IsBatchPick, "Y")) {
                    if (completeOutput.StoreBatch.StoreBatchLine.BackroomPickComplete == "Y" && completeOutput.StoreBatch.Status != "1100") {
                        _scModelUtils.setStringValueAtModelPath("StoreBatch.IsLast", "Y", inputOrderDetails);
                    }

                    _iasUIUtils.callApi(this, inputOrderDetails, "cancelBatchPick_ref", null);
                } else {
                    _iasUIUtils.callApi(this, inputOrderDetails, "CancelBackRoomPick_ref", null);
                }
            } else if (_scBaseUtils.equals(event, "Ok")) {
                var that = this;
                _scWidgetUtils.setValue(this, "scanProductIdTxt", "", false);
                setTimeout(function() {
                    //  _scWidgetUtils.setFocusOnControl(that, "scanProductIdTxt");
                    document.querySelector(".jccScanTextBox  .dijitTextBox .dijitInputField .dijitInputInner").focus();
                }, 400);
            }
        },
        productSearch: function(event, bEvent, ctrl, args) {},
        productSearchSettingWizard: function(event, bEvent, ctrl, args) {},
        initializeScreen: function(event, bEvent, ctrl, args) {
            var that = this;
            var completeOrderDetails = _scScreenUtils.getModel(this, "CompleteOrderData");
            bEvent.stopEvent();
            setTimeout(function() {
                // _scWidgetUtils.setFocusOnControl(that, "scanProductIdTxt");

                document.querySelector(".jccScanTextBox  .dijitTextBox .dijitInputField .dijitInputInner").focus();
            }, 500);
        },
        resetFields: function() {
            var eShipment = null;
            eShipment = {};
            _scScreenUtils.setModel(this, "initializeModel", eShipment, null);
        },
        handleBarcodeScan: function(event, bEvent, ctrl, args) {
            if (_iasEventUtils.isEnterPressed(event)) {
                if (_scResourcePermissionUtils.hasPermission("WSC000008")) {
                    this.packItem(event, bEvent, ctrl, args);
                } else {
                    _iasBaseTemplateUtils.showMessage(this, _scScreenUtils.getString(this, "Message_UserHasNoPermission"), "error", null);
                }
            }
        },
        handleBarcodeScanSettingWizard: function(event, bEvent, ctrl, args) {
            if (_iasEventUtils.isEnterPressed(event)) {
                if (_scResourcePermissionUtils.hasPermission("WSC000008")) {
                    _scUserprefs.setProperty("customPrevWizard", "PackProductScan");
                }
            }
        }
    });
});