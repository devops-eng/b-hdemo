scDefine(["dojo/text!./templates/CustomerServicePortlet.html", "scbase/loader!dijit/form/Button", "scbase/loader!dojo/_base/declare", "scbase/loader!idx/form/TextBox", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/widgets/Label", "scbase/loader!sc/plat/dojo/widgets/Link", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/GridxUtils", "scbase/loader!gridx/Grid", "scbase/loader!sc/plat/dojo/binding/GridxDataBinder", "scbase/loader!ias/utils/RepeatingScreenUtils", "scbase/loader!sc/plat/dojo/utils/RepeatingPanelUtils", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!sc/plat/dojo/utils/RequestUtils", "scbase/loader!ias/utils/BaseTemplateUtils"], function(templateText, _dijitButton, _dojodeclare, _idxTextBox, _idxContentPane, _scBaseUtils, _scWidgetUtils, _scEventUtils, _scLabel, _scLink, _scScreen, _scModelUtils, _iasUIUtils, _scScreenUtils, _iasBaseTemplateUtils, _scWidgetUtils, _scEventUtils, _scGridxUtils, _gridxGrid, _scGridxDataBinder, _iasRepeatingScreenUtils, _scRepeatingPanelUtils, _scUserprefs, _iasContextUtils, _extnExpicientUtils, _scRequestUtils, _iasBaseTemplateUtils) {
    return _dojodeclare("extn.customScreen.CustomerServicePortlet", [_scScreen], {
        templateString: templateText,
        uId: "CustomerServicePortlet",
        packageName: "extn.customScreen",
        className: "CustomerServicePortlet",
        namespaces: {
            targetBindingNamespaces: [{
                value: 'extn_storeInput',
                description: "shipment "
            }],
            sourceBindingNamespaces: [{
                value: 'getUserData_output',
                description: "User Token"
            }]
        },
        staticBindings: [],
        events: [],
        subscribers: {
            local: [{
                eventId: 'afterScreenInit',
                sequence: '30',
                handler: {
                    methodName: "initializeScreen"
                }
            }, {
                eventId: 'basicSearch_onClick',
                sequence: '25',
                description: '',
                listeningControlUId: 'basicSearch',
                handler: {
                    methodName: "basicSearchAction",
                    description: ""
                }
            }, {
                eventId: 'advancedSearch_onClick',
                sequence: '25',
                description: '',
                listeningControlUId: 'advancedSearch',
                handler: {
                    methodName: "advanceSearchAction",
                    description: ""
                }
            }]
        },
        initializeScreen: function(event, bEvent, ctrl, args) {
            var inputData = _scBaseUtils.getNewModelInstance();
            var user = _scUserprefs.getUserId();
            var context = _extnExpicientUtils.getComContext();
            _scModelUtils.setStringValueAtModelPath("User.UserId", user, inputData);
            _scModelUtils.setStringValueAtModelPath("User.StoreContext", context, inputData)
            _iasUIUtils.callApi(this, inputData, "getUserData_ref", null);
        },
        advanceSearchAction: function(event, bEvent, ctrl, args) {
            var comPermission = _extnExpicientUtils.getComPermission();
            if (!_scBaseUtils.equals(comPermission, "true")) {
                var errorMessage = _scScreenUtils.getString(this, "extn_COM_PERMISSION_ERROR");
                _iasBaseTemplateUtils.showMessage(this, errorMessage, "error", null);
            } else {
                _extnExpicientUtils.openCOM();
            }
        },
        basicSearchAction: function(event, bEvent, ctrl, args) {
            var shipmentModel = null;
            shipmentModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
            _scModelUtils.setStringValueAtModelPath("Shipment.DeliveryMethod", " ", shipmentModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.NoDefaultStatuses", "true", shipmentModel);
            _iasUIUtils.openWizardInEditor("wsc.components.shipment.search.ShipmentSearchWizard", shipmentModel, "wsc.desktop.editors.ShipmentSearchEditor", this, null);
        },
        handleMashupCompletion: function(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data) {
            _iasBaseTemplateUtils.handleMashupCompletion(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data, this);
        },
        handleMashupOutput: function(mashupRefId, modelOutput, mashupInput, mashupContext, applySetModel) {
            if (_scBaseUtils.equals(mashupRefId, "getUserHierarchy_ref")) {
                var currentStore = _iasContextUtils.getFromContext("CurrentStore");
                _scModelUtils.setStringValueAtModelPath("User.CurrentStore", currentStore, modelOutput);
                _scBaseUtils.setModel(this, "extn_getShip", modelOutput, null);
                _scScreenUtils.getModel(this, "extn_getShip");
                var a = null;
            }
            if (_scBaseUtils.equals(mashupRefId, "getUserData_ref")) {
                var comPermission = _scModelUtils.getStringValueFromPath("Token.comPermission", modelOutput);
                var context = _scModelUtils.getStringValueFromPath("Token.context", modelOutput);
                var token = _scModelUtils.getStringValueFromPath("Token.securityToken", modelOutput);
                _extnExpicientUtils.setComPermission(comPermission);
                _extnExpicientUtils.setToken(token);
                _extnExpicientUtils.setContext(context);
            }
        }
    });
});
