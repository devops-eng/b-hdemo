scDefine(["dojo/text!./templates/LevelOfService.html", "scbase/loader!dijit/form/Button", "scbase/loader!dojo/_base/declare", "scbase/loader!idx/form/TextBox", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/widgets/Label", "scbase/loader!sc/plat/dojo/widgets/Link", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/GridxUtils", "scbase/loader!gridx/Grid", "scbase/loader!sc/plat/dojo/binding/GridxDataBinder", "scbase/loader!ias/utils/RepeatingScreenUtils", "scbase/loader!sc/plat/dojo/utils/RepeatingPanelUtils", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!sc/plat/dojo/utils/EditorUtils", "scbase/loader!sc/plat/dojo/info/ApplicationInfo"], function(templateText, _dijitButton, _dojodeclare, _idxTextBox, _idxContentPane, _scBaseUtils, _scWidgetUtils, _scEventUtils, _scLabel, _scLink, _scScreen, _scModelUtils, _iasUIUtils, _scScreenUtils, _iasBaseTemplateUtils, _scWidgetUtils, _scEventUtils, _scGridxUtils, _gridxGrid, _scGridxDataBinder, _iasRepeatingScreenUtils, _scRepeatingPanelUtils, _scUserprefs, _iasContextUtils, _extnExpicientUtils, _scEditorUtils, scApplicationInfo) {
    return _dojodeclare("extn.customScreen.LevelOfService", [_scScreen], {
        templateString: templateText,
        uId: "LevelOfService",
        packageName: "extn.customScreen",
        className: "LevelOfService",
        namespaces: {
            targetBindingNamespaces: [{
                value: 'extn_selectedService',
                description: "Selected Service "
            }],
            sourceBindingNamespaces: [{
                value: 'extn_serviceInput',
                description: "shipment service "
            }, {
                value: 'extn_initialServiceVal',
                description: "Initial carrier service "
            }]
        },
        staticBindings: [],
        events: [],
        subscribers: {
            local: [{
                    eventId: 'afterScreenInit',
                    sequence: '30',
                    handler: {
                        methodName: "initializeScreen"
                    }
                }, {
                    eventId: 'changeService_onClick',
                    sequence: '25',
                    description: '',
                    listeningControlUId: 'changeService',
                    handler: {
                        methodName: "changeLos",
                        description: ""
                    }
                }, {
                    eventId: 'cancelButton_onClick',
                    sequence: '25',
                    description: '',
                    listeningControlUId: 'cancelButton',
                    handler: {
                        methodName: "handleCancel",
                        description: ""
                    }
                },
                {
                    eventId: 'afterBehaviorMashupCall',
                    sequence: '51',
                    handler: {
                        methodName: "handleStoreChange"
                    }
                }
                /*, {
                                eventId: 'filteringSelectCarrier_onChange',
                                sequence: '51',
                                description: '',
                                listeningControlUId: 'filteringSelectCarrier',
                                handler: {
                                    methodName: "onFilteringSelectChange",
                                    description: ""
                                }
                            }*/
            ]
        },
        sameVal: true,
        firstRun: true,
        storeInstance: '',
        /*onFilteringSelectChange: function(event, bEvent, ctrl, args) {
            //var oldValue = null;
            //oldValue = _scBaseUtils.getAttributeValue("initialValue", false, args.originatingControlInstance);
            if (_scBaseUtils.equals(event, this.storeInstance)) {
                this.sameVal = true;
            } else {
                this.sameVal = false;
                this.storeInstance=event;
            }
            this.firstRun = false;
            var a = null;
            var b = null;
        },*/
        initializeScreen: function(event, bEvent, ctrl, args) {
            var model = _scScreenUtils.getModel(this.getOwnerScreen(), "getShipmentDetails_output");
            //var enterpriseCode = _scModelUtils.getStringValueFromPath("Shipment.EnterpriseCode", model);
            var scac = null;
            scac = _scModelUtils.getStringValueFromPath("Shipment.SCAC", model);
            var inputModel = _scModelUtils.createNewModelObjectWithRootKey("ScacAndService");
            _scModelUtils.setStringValueAtModelPath("ScacAndService.ScacKey", scac, inputModel);
            _iasUIUtils.callApi(this, inputModel, "changeLevelOfService_ref", null);
            /*var a = null;
            var item = null;
            var sUserID = _scUserprefs.getUserId();
            var userHierarchyInput = _scModelUtils.createNewModelObjectWithRootKey("User");
            _scModelUtils.setStringValueAtModelPath("User.Loginid", sUserID, userHierarchyInput);
            _iasUIUtils.callApi(this, userHierarchyInput, "getUserHierarchy_ref", null);
            var data = null;*/
        },
        handleCancel: function(event, bEvent, ctrl, args) {
            _scWidgetUtils.closePopup(this, "CLOSE", false);
        },
        changeLos: function(event, bEvent, ctrl, args) {
            var targetModel = null;
            targetModel = _scScreenUtils.getTargetModel(this, "extn_selectedService", null);
            var selectedCarrierService = null;
            selectedCarrierService = _scModelUtils.getStringValueFromPath("CarrierService.SCACAndService", targetModel);
            var inputModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
            var model = _scScreenUtils.getModel(this.getOwnerScreen(), "getShipmentDetails_output");
            var shipmentKey = null;
            shipmentKey = _scModelUtils.getStringValueFromPath("Shipment.ShipmentKey", model);
            _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", shipmentKey, inputModel);
            var carrierServiceListModel = _scScreenUtils.getModel(this, "extn_serviceInput");
            var carrierServiceList = null;
            var carrierServiceKey = null;
            var scacAndServiceKey = null;
            var scacAndService = null;
            carrierServiceList = _scModelUtils.getStringValueFromPath("ScacAndServiceList.ScacAndService", carrierServiceListModel);
            for (var i = 0; i < carrierServiceList.length; i++) {
                var referenceModel = null;
                referenceModel = _scBaseUtils.getArrayItemByIndex(carrierServiceList, i);
                var scacAndServiceDesc = null;
                scacAndServiceDesc = _scModelUtils.getStringValueFromPath("ScacAndService", referenceModel);
                if (_scBaseUtils.equals(selectedCarrierService, scacAndServiceDesc)) {
                    carrierServiceKey = _scModelUtils.getStringValueFromPath("CarrierServiceKey", referenceModel);
                    scacAndServiceKey = _scModelUtils.getStringValueFromPath("ScacAndServiceKey", referenceModel);
                    // scacAndService=_scModelUtils.getStringValueFromPath("ScacAndService", referenceModel);
                }
            }
            _scModelUtils.setStringValueAtModelPath("Shipment.ScacAndService.CarrierServiceKey", carrierServiceKey, inputModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.ScacAndService.ScacAndServiceKey", scacAndServiceKey, inputModel);
            _iasUIUtils.callApi(this, inputModel, "changeShipmentForService_ref", null);
            this.getOwnerScreen().selectedLevelOfService = selectedCarrierService;
            _scWidgetUtils.closePopup(this, "SETVAL", false);
        },
        handleStoreChange: function(event, bEvent, ctrl, args) {
            var isMashupAvailable = _extnExpicientUtils.isMashupAvailable("changeLevelOfService_ref", args);
            if (isMashupAvailable) {
                var modelOutput = null;
                modelOutput = _extnExpicientUtils.getMashupOutputForRefID("changeLevelOfService_ref", args);
                _scScreenUtils.setModel(this, "extn_serviceInput", modelOutput);
                var headerElement = document.getElementsByClassName("idxHeaderUserName")[0].title;
            }
            var isMashup2Available = _extnExpicientUtils.isMashupAvailable("changeShipmentForService_ref", args);
            if (isMashup2Available) {
                var modelOutput = null;
                modelOutput = _extnExpicientUtils.getMashupOutputForRefID("changeShipmentForService_ref", args);
                _scScreenUtils.setModel(this, "extn_serviceInput", modelOutput);
                var headerElement = document.getElementsByClassName("idxHeaderUserName")[0].title;
            }
        },
        handleMashupCompletion: function(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data) {
            _iasBaseTemplateUtils.handleMashupCompletion(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data, this);
        },
        handleMashupOutput: function(mashupRefId, modelOutput, mashupInput, mashupContext, applySetModel) {
            if (_scBaseUtils.equals(mashupRefId, "changeLevelOfService_ref")) {
                var model = _scScreenUtils.getModel(this.getOwnerScreen(), "getShipmentDetails_output");
                var initialServiceValue = null;
                initialServiceValue = _scModelUtils.getStringValueFromPath("Shipment.ScacAndService", model);
                if (!_scBaseUtils.isVoid(initialServiceValue)) {
                    var initialModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
                    _scModelUtils.setStringValueAtModelPath("Shipment.ScacAndService", initialServiceValue, initialModel);
                    _scScreenUtils.setModel(this, "extn_initialServiceVal", initialModel);
                }
            }
        }
    });
});
