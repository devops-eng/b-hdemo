scDefine(["dojo/text!./templates/PackProductScan.html", "scbase/loader!dijit/form/Button", "scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/_base/lang", "scbase/loader!dojo/text", "scbase/loader!ias/widgets/IASBaseScreen", "scbase/loader!idx/form/TextBox", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat", "scbase/loader!sc/plat/dojo/binding/ButtonDataBinder", "scbase/loader!sc/plat/dojo/binding/SimpleDataBinder", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!ias/utils/EventUtils", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!sc/plat/dojo/utils/ResourcePermissionUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EditorUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/info/ApplicationInfo", "scbase/loader!wsc/components/shipment/common/utils/ShipmentUtils", "scbase/loader!sc/plat/dojo/Userprefs"], function(templateText, _dijitButton, _dojodeclare, _dojokernel, _dojolang, _dojotext, _iasIASBaseScreen, _idxTextBox, _idxContentPane, _scplat, _scButtonDataBinder, _scSimpleDataBinder, _scBaseUtils, _iasBaseTemplateUtils, _iasUIUtils, _iasEventUtils, _iasContextUtils, _scResourcePermissionUtils, _scScreenUtils, _scModelUtils, _scWidgetUtils, _scEditorUtils, _scEventUtils, _scApplicationInfo, _wscShipmentUtils, _scUserprefs) {
    return _dojodeclare("extn.customScreen.PackProductScan", [_iasIASBaseScreen], {
        templateString: templateText,
        uId: "PackProductScan",
        packageName: "extn.customScreen",
        className: "PackProductScan",
        title: "Screen",
        screen_description: "This is screen is used to paint item pack feature",
        isDirtyCheckRequired: true,
        namespaces: {
            targetBindingNamespaces: [{
                value: 'packItem_input',
                description: "Input for custom mashup - to get a shipment from an item scan"
            }],
            sourceBindingNamespaces: [{
                value: 'PackProductScanRule',
                description: "This model is used for item pack rule"
            }, {
                value: 'initializeModel',
                description: "This model is used to reset the search criteria"
            }]
        },
        staticBindings: [],
        hotKeys: [],
        events: [],
        subscribers: {
            local: [{
                eventId: 'afterScreenInit',
                sequence: '30',
                description: 'This event is raised after screen has been initialized',
                handler: {
                    methodName: "initializeScreen"
                }
            }, {
                eventId: 'txtItemID_onKeyUp',
                sequence: '30',
                listeningControlUId: 'txtItemID',
                handler: {
                    methodName: "handleBarcodeScan"
                }
            }, {
                eventId: 'txtItemID_onKeyDown',
                sequence: '30',
                listeningControlUId: 'txtItemID',
                handler: {
                    methodName: "handleBarcodeScan"
                }
            }, {
                eventId: 'addProductButton_onClick',
                sequence: '30',
                description: 'Subscriber for Scan/Add button',
                listeningControlUId: 'addProductButton',
                handler: {
                    methodName: "productSearch",
                    description: "Handled for Scan product"
                }
            }, {
                eventId: 'txtItemID_onKeyUp',
                sequence: '19',
                listeningControlUId: 'txtItemID',
                handler: {
                    methodName: "handleBarcodeScanSettingWizard"
                }
            }, {
                eventId: 'addProductButton_onClick',
                sequence: '19',
                description: 'Subscriber for Scan/Add button',
                listeningControlUId: 'addProductButton',
                handler: {
                    methodName: "productSearchSettingWizard",
                    description: "Handled for Scan product"
                }
            }]
        },
        handleMashupOutput: function(mashupRefId, modelOutput, mashupInput, mashupContext, applySetModel) {
            if (_scBaseUtils.equals(mashupRefId, "pack_packItem")) {
                this.handle_pack_packItem(mashupRefId, modelOutput, mashupInput, mashupContext, applySetModel);
            }
        },
        productSearch: function(event, bEvent, ctrl, args) {
            if (_scResourcePermissionUtils.hasPermission("WSC000008")) {
                this.packItem(event, bEvent, ctrl, args);
            } else {
                _iasBaseTemplateUtils.showMessage(this, _scScreenUtils.getString(this, "Message_UserHasNoPermission"), "error", null);
            }
        },
        productSearchSettingWizard: function(event, bEvent, ctrl, args) {
            if (_scResourcePermissionUtils.hasPermission("WSC000008")) {
                _scUserprefs.setProperty("customPrevWizard", "PackProductScan");
            }
        },
        initializeScreen: function(event, bEvent, ctrl, args) {
            if (_iasContextUtils.isMobileContainer()) {
                _scWidgetUtils.setLabel(this, "txtItemID", "");
            }
        },
        resetFields: function() {
            var eShipment = null;
            eShipment = {};
            _scScreenUtils.setModel(this, "initializeModel", eShipment, null);
        },
        packItem: function(event, bEvent, ctrl, args) {
            var input = _scScreenUtils.getTargetModel(this, "packItem_input", null);
            var itemID = _scModelUtils.getStringValueFromPath("Barcode.BarCodeData", input);
            if (_scBaseUtils.isVoid(itemID)) {
                var messageConfig = {};
                messageConfig["showAsDialog"] = "true";
                _iasBaseTemplateUtils.showMessage(this, "NoProductScanned", "error", messageConfig);
            } else {
                _iasUIUtils.callApi(this, input, "pack_packItem", null);
                this.resetFields();
            }
        },
        handle_pack_packItem: function(mashupRefId, modelOutput, mashupInput, mashupContext, applySetModel) {
            var errorCode = _scModelUtils.getStringValueFromPath("BarCode.ErrorCode", modelOutput);
            if (!_scBaseUtils.isVoid(errorCode)) {
                var messageConfig = {};
                messageConfig["showAsDialog"] = "true";
                _iasBaseTemplateUtils.showMessage(this, errorCode, "error", messageConfig);
            } else {
                var batchModelExist = null;
                batchModelExist = _scModelUtils.hasModelObjectForPathInModel("BarCode.ShipItems", modelOutput);
                if (batchModelExist) {
                    var shipmentNo = null;
                    shipmentNo = _scModelUtils.getStringValueFromPath("BarCode.ShipItems.ShipmentNo", modelOutput);
                    var displayMessage = "To start packing Shipment #: " + shipmentNo + ", please complete picking:";
                    var displayMessageShipment = "To start packing Shipment #: " + shipmentNo + ", please complete picking:";
                    var completeBPMessage = "Shipment #: " + shipmentNo;
                    var finalMessage = null;
                    var batchList = null;
                    var headerModel = null;
                    headerModel = _scModelUtils.getModelObjectFromPath("BarCode.ShipItems", modelOutput);
                    batchList = _scModelUtils.getModelObjectFromPath("BarCode.ShipItems.ShipItem", modelOutput);
                    var batchCount = 0;
                    if (!_scBaseUtils.isVoid(batchList)) {
                        if (_scBaseUtils.equals(headerModel.IsComplete, "Y")) {
                            //Backroom pick complete for all items
                            for (var n = 0; n < batchList.length; n++) {
                                var referenceModel = null;
                                referenceModel = _scBaseUtils.getArrayItemByIndex(batchList, n);
                                var batchNo = null;
                                var itemID = null;
                                var departmentCode = null;
                                var displayMessage1 = null;
                                batchNo = _scModelUtils.getStringValueFromPath("BatchNo", referenceModel);
                                if (!_scBaseUtils.isVoid(batchNo)) {
                                    batchCount++;
                                    displayMessage1 = '<br/>- Batch #: ' + batchNo + ' or dissolve the Batch';
                                    displayMessage += displayMessage1;
                                }
                            }
                            if (!_scBaseUtils.numberGreaterThan(batchCount, 0)) {
                                var displayMessage = displayMessageShipment + '<br/>- ' + completeBPMessage;
                            }
                        } else {
                            //Backroom pick not complete for all items
                            for (var n = 0; n < batchList.length; n++) {
                                var referenceModel = null;
                                referenceModel = _scBaseUtils.getArrayItemByIndex(batchList, n);
                                var batchNo = null;
                                var itemID = null;
                                var departmentCode = null;
                                var displayMessage1 = null;
                                batchNo = _scModelUtils.getStringValueFromPath("BatchNo", referenceModel);
                                itemID = _scModelUtils.getStringValueFromPath("ItemDesc", referenceModel);
                                departmentCode = _scModelUtils.getStringValueFromPath("DepartmentCode", referenceModel);
                                if (_scBaseUtils.isVoid(batchNo)) {
                                    displayMessage1 = '<br/>- Item ' + '"' + itemID + '"' + ' in Department ' + '"' + departmentCode + '"';
                                    displayMessage += displayMessage1;
                                } else {
                                    displayMessage1 = '<br/>- Item ' + '"' + itemID + '"' + ' in Department ' + '"' + departmentCode + '"' + ' in Batch #: ' + batchNo;
                                    displayMessage += displayMessage1;
                                }
                            }
                        }
                        var messageConfig1 = {};
                        messageConfig1["showAsDialog"] = "true";
                        _iasBaseTemplateUtils.showMessage(this, displayMessage, "information", messageConfig1);
                    }
                } else {
                    var shipmentModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
                    _scModelUtils.addModelToModelPath("Shipment", _scModelUtils.getModelObjectFromPath("BarCode.Shipment", modelOutput), shipmentModel);
                    var openPackWizard = true;
                    if (!_iasContextUtils.isMobileContainer()) {
                        var screenContainerInstance = _scApplicationInfo.getSCScreenContainerInstance();
                        var editorInput = {};
                        _scBaseUtils.setAttributeValue("inputData", shipmentModel, editorInput);
                        var allOpenEditors = _scEditorUtils.getSpecificOpenEditors("wsc.desktop.editors.ShipmentEditor");
                        for (i = 0; i < allOpenEditors.length; i++) {
                            var wizardId = allOpenEditors[i].editorInstance.getScreenInstance().wizardId;
                            if (_scBaseUtils.equals("wsc.components.shipment.container.pack.ContainerPackWizard", wizardId) || _scBaseUtils.equals("wsc.components.shipment.backroompick.HoldLocationWizard", wizardId)) {
                                var matchingEditor = screenContainerInstance.getMatchingEditor(allOpenEditors[i].editorInstance, editorInput);
                                if (matchingEditor) {
                                    var packingWizardUI = matchingEditor.getScreenInstance();
                                    if (_scScreenUtils.isDirty(matchingEditor)) {
                                        openPackWizard = false;
                                        var args = {};
                                        _scBaseUtils.setAttributeValue("wizardInput", shipmentModel, args);
                                        var packingWizard = packingWizardUI.getCurrentWizardInstance();
                                        _scEditorUtils.focusOnEditor(matchingEditor, editorInput);
                                        _scEventUtils.fireEventInsideScreen(packingWizard, "dirtyCheckOnItemPack", {}, args);
                                    } else {
                                        _scEditorUtils.closeEditor(packingWizardUI);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    if (openPackWizard) {
                        if (modelOutput && modelOutput.BarCode && modelOutput.BarCode.Shipment && (modelOutput.BarCode.Shipment.PackComplete == "Y")) {
                            var emptyShipmentModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
                            _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", _scModelUtils.getStringValueFromPath("BarCode.Shipment.ShipmentKey", modelOutput), emptyShipmentModel);
                            _wscShipmentUtils.openPackingWizard(this, emptyShipmentModel);
                        } else {
                            _wscShipmentUtils.openPackingWizard(this, shipmentModel);
                        }
                    }
                }
            }
        },
        handleBarcodeScan: function(event, bEvent, ctrl, args) {
            if (_iasEventUtils.isEnterPressed(event)) {
                if (_scResourcePermissionUtils.hasPermission("WSC000008")) {
                    this.packItem(event, bEvent, ctrl, args);
                } else {
                    _iasBaseTemplateUtils.showMessage(this, _scScreenUtils.getString(this, "Message_UserHasNoPermission"), "error", null);
                }
            }
        },
        handleBarcodeScanSettingWizard: function(event, bEvent, ctrl, args) {
            if (_iasEventUtils.isEnterPressed(event)) {
                if (_scResourcePermissionUtils.hasPermission("WSC000008")) {
                    _scUserprefs.setProperty("customPrevWizard", "PackProductScan");
                }
            }
        }
    });
});
