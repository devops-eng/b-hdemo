scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/text", "scbase/loader!sc/plat/dojo/controller/ServerDataController", "scbase/loader!extn/customScreen/PackProductScan"], function(_dojodeclare, _dojokernel, _dojotext, _scServerDataController, _wscPackProductScan) {
    return _dojodeclare("extn.customScreen.PackProductScanBehaviorController", [_scServerDataController], {
        screenId: 'extn.customScreen.PackProductScan',
        mashupRefs: [{
            mashupId: 'pack_packItem',
            mashupRefId: 'pack_packItem'
        }]
    });
});
