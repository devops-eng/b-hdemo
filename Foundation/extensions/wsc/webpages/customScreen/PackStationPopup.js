scDefine(["dojo/text!./templates/PackStationPopup.html", "scbase/loader!dijit/form/Button", "scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/_base/lang", "scbase/loader!dojo/text", "scbase/loader!ias/widgets/IASBaseScreen", "scbase/loader!idx/form/TextBox", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat", "scbase/loader!sc/plat/dojo/binding/ButtonDataBinder", "scbase/loader!sc/plat/dojo/binding/SimpleDataBinder", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!ias/utils/EventUtils", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!sc/plat/dojo/utils/ResourcePermissionUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EditorUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/info/ApplicationInfo", "scbase/loader!wsc/components/shipment/common/utils/ShipmentUtils", "scbase/loader!sc/plat/dojo/Userprefs"], function(templateText, _dijitButton, _dojodeclare, _dojokernel, _dojolang, _dojotext, _iasIASBaseScreen, _idxTextBox, _idxContentPane, _scplat, _scButtonDataBinder, _scSimpleDataBinder, _scBaseUtils, _iasBaseTemplateUtils, _iasUIUtils, _iasEventUtils, _iasContextUtils, _scResourcePermissionUtils, _scScreenUtils, _scModelUtils, _scWidgetUtils, _scEditorUtils, _scEventUtils, _scApplicationInfo, _wscShipmentUtils, _scUserprefs) {
    return _dojodeclare("extn.customScreen.PackStationPopup", [_iasIASBaseScreen], {
        templateString: templateText,
        uId: "PackStationPopup",
        packageName: "extn.customScreen",
        className: "PackStationPopup",
        title: "Screen",
        screen_description: "This is screen is used to paint item pack feature",
        isDirtyCheckRequired: true,
        namespaces: {
            targetBindingNamespaces: [{
                value: 'packItem_input',
                description: "Input for custom mashup - to get a shipment from an item scan"
            }],
            sourceBindingNamespaces: [{
                value: 'printerList',
                description: "This model is used for all device list"
            }, {
                value: 'initializeModel',
                description: "This model is used to reset the search criteria"
            }, {
                description: 'Stores popup output set from setPopupOutput',
                value: 'popupOutput'
            }]
        },
        staticBindings: [],
        hotKeys: [{
            id: "Popup_btnCancel",
            key: "ESCAPE",
            description: "$(_scSimpleBundle:Close)",
            widgetId: "Popup_btnCancel",
            invocationContext: "",
            category: "$(_scSimpleBundle:General)",
            helpContextId: ""
        }],
        events: [],
        subscribers: {
            local: [{
                eventId: 'afterScreenInit',
                sequence: '30',
                description: 'This event is raised after screen has been initialized',
                handler: {
                    methodName: "initializeScreen"
                }
            }, {
                eventId: 'txtItemID_onKeyUp',
                sequence: '30',
                listeningControlUId: 'txtItemID',
                handler: {
                    methodName: "handleBarcodeScan"
                }
            }, {
                eventId: 'scanProductIdTxt_onKeyUp',
                sequence: '19',
                listeningControlUId: 'scanProductIdTxt',
                handler: {
                    methodName: "scanProductOnEnter"
                }
            }, {
                eventId: 'updateButton_onClick',
                sequence: '25',
                description: '',
                listeningControlUId: 'updateButton',
                handler: {
                    methodName: "handleUpdate",
                    description: ""
                }
            }, {
                eventId: 'cancelButton_onClick',
                sequence: '25',
                description: '',
                listeningControlUId: 'cancelButton',
                handler: {
                    methodName: "handleCancel",
                    description: ""
                }
            }]
        },
        handleCancel: function(event, bEvent, ctrl, args) {
            _scWidgetUtils.closePopup(this, "CLOSE", false);
        },
        handleMashupOutput: function(mashupRefId, modelOutput, mashupInput, mashupContext, applySetModel) {
            if (_scBaseUtils.equals(mashupRefId, "getDefaultStorePrinterList_ref")) {
                var packStations = [];
                if (modelOutput.Devices && modelOutput.Devices.Device.length > 0) {
                    var deviceArray = modelOutput.Devices.Device;
                    for (var i = 0; i < deviceArray.length; i++) {
                        var currDevice = _scBaseUtils.getArrayItemByIndex(deviceArray, i);
                        currDevice.PackStation = "";
                        if (currDevice && currDevice.DeviceParamsXML && currDevice.DeviceParamsXML.Attributes && currDevice.DeviceParamsXML.Attributes.Attribute.length > 0) {
                            var currDeviceAttributeList = currDevice.DeviceParamsXML.Attributes.Attribute;
                            for (var m = 0; m < currDeviceAttributeList.length; m++) {
                                var currAttribute = _scBaseUtils.getArrayItemByIndex(currDeviceAttributeList, m);
                                if (_scBaseUtils.equals(currAttribute.Name, "PackStation")) {
                                    currDevice.PackStation = currAttribute.Value;
                                    break;
                                }
                            }
                        }
                    }
                }
                _scScreenUtils.setModel(this, "printerList", modelOutput);
                // this.checkCaseNumberValidation(modelOutput);
            }
        },
        handleMashupCompletion: function(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data) {
            _iasBaseTemplateUtils.handleMashupCompletion(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data, this);
        },
        setPopupOutput: function(model) {
            _scScreenUtils.setModel(this, "popupOutput", model, null);
        },
        getPopupOutput: function(event, bEvent, ctrl, args) {
            // var options = null;
            // options = _scBaseUtils.getNewBeanInstance();
            // _scBaseUtils.addStringValueToBean("allowEmpty", true, options);
            var caseoutput = _scScreenUtils.getModel(this, "packStationUpdateModel");
            return caseoutput;
        },
        scanProductOnEnter: function(event, bEvent, ctrl, args) {
            if (_iasEventUtils.isEnterPressed(event)) {
                this.handleUpdate();
            }
        },
        handleUpdate: function(event, bEvent, ctrl, args) {
            // alert("Button Clicked");
            var packStationInput = _scScreenUtils.getTargetModel(this, "packStation_input");
            var id = _scModelUtils.getStringValueFromPath("PackStation.ID", packStationInput);
            var printerList = _scScreenUtils.getModel(this, "printerList");
            var stationModel = _scModelUtils.createNewModelObjectWithRootKey("DeviceList");
            stationModel.DeviceList.HBCUserPrinter = [];
            var valExist = false;
            if (printerList.Devices && (printerList.Devices.Device.length > 0)) {
                var deviceList = _scModelUtils.getStringValueFromPath("Devices.Device", printerList);
                for (var j = 0; j < deviceList.length; j++) {
                    var currDevice = _scBaseUtils.getArrayItemByIndex(deviceList, j);
                    if (_scBaseUtils.equals(currDevice.PackStation, id)) {
                        valExist = true;
                        var newModel = {
                            "PackStationID": currDevice.PackStation,
                            "DocumentType": currDevice.DeviceSubType,
                            "PrinterAddress": currDevice.DeviceId,
                            "PrinterName": "",
                            "StoreID": _iasContextUtils.getFromContext("CurrentStore"),
                            "UserID": _scUserprefs.getUserId()
                        };
                        stationModel.DeviceList.HBCUserPrinter.push(newModel);
                    }
                }
            }
            if (_scBaseUtils.equals(valExist, false)) {
                _iasBaseTemplateUtils.showMessage(this, "Pack Station" + " '" + id + "' " + "does not exist.", "error", null);
                this.resetFields();
            } else {
                _scScreenUtils.setModel(this, "packStationUpdateModel", stationModel);
                _scWidgetUtils.closePopup(this, "APPLY", false);
            }
        },
        initializeScreen: function(event, bEvent, ctrl, args) {
            var inputModel_2 = _scModelUtils.createNewModelObjectWithRootKey("Device");
            _scModelUtils.setStringValueAtModelPath("Device.OrganizationCode", _iasContextUtils.getFromContext("CurrentStore"), inputModel_2);
            var sDeviceType = 'Printer';
            _scModelUtils.setStringValueAtModelPath("Device.DeviceType", sDeviceType, inputModel_2);
            _iasUIUtils.callApi(this, inputModel_2, "getDefaultStorePrinterList_ref", null);
            // bEvent.stopEvent();
            setTimeout(function() {
                // _scWidgetUtils.setFocusOnControl(that, "scanProductIdTxt");
                document.querySelector(".packStationTextBox  .dijitTextBox .dijitInputField .dijitInputInner").focus();
            }, 500);
        },
        resetFields: function() {
            var eShipment = null;
            eShipment = {};
            _scScreenUtils.setModel(this, "packStation_output", eShipment, null);
        }
    });
});
