scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/text", "scbase/loader!sc/plat/dojo/controller/ServerDataController", "scbase/loader!extn/customScreen/PackStationPopup"], function(_dojodeclare, _dojokernel, _dojotext, _scServerDataController, _wscPackProductScan) {
    return _dojodeclare("extn.customScreen.PackStationPopupBehaviorController", [_scServerDataController], {
        screenId: 'extn.customScreen.PackStationPopup',
        mashupRefs: [{
            callSequence: '',
            mashupRefId: 'getDefaultStorePrinterList_ref',
            sequence: '',
            sourceBindingOptions: '',
            mashupId: 'getDefaultStorePrinterList'
        }]
    });
});
