scDefine(["dojo/text!./templates/StoreContextPortlet.html", "scbase/loader!dijit/form/Button", "scbase/loader!dojo/_base/declare", "scbase/loader!idx/form/TextBox", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/widgets/Label", "scbase/loader!sc/plat/dojo/widgets/Link", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/GridxUtils", "scbase/loader!gridx/Grid", "scbase/loader!sc/plat/dojo/binding/GridxDataBinder", "scbase/loader!ias/utils/RepeatingScreenUtils", "scbase/loader!sc/plat/dojo/utils/RepeatingPanelUtils", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!sc/plat/dojo/utils/EditorUtils", "scbase/loader!sc/plat/dojo/info/ApplicationInfo"], function(templateText, _dijitButton, _dojodeclare, _idxTextBox, _idxContentPane, _scBaseUtils, _scWidgetUtils, _scEventUtils, _scLabel, _scLink, _scScreen, _scModelUtils, _iasUIUtils, _scScreenUtils, _iasBaseTemplateUtils, _scWidgetUtils, _scEventUtils, _scGridxUtils, _gridxGrid, _scGridxDataBinder, _iasRepeatingScreenUtils, _scRepeatingPanelUtils, _scUserprefs, _iasContextUtils, _extnExpicientUtils, _scEditorUtils, scApplicationInfo) {
    return _dojodeclare("extn.customScreen.StoreContextPortlet", [_scScreen], {
        templateString: templateText,
        uId: "StoreContextPortlet",
        packageName: "extn.customScreen",
        className: "StoreContextPortlet",
        namespaces: {
            targetBindingNamespaces: [{
                value: 'extn_storeInput',
                description: "shipment "
            }],
            sourceBindingNamespaces: [{
                value: 'extn_getShip',
                description: "shipment "
            }, {
                value: 'extn_storeDescription',
                description: "store description "
            }]
        },
        staticBindings: [],
        events: [],
        subscribers: {
            local: [{
                    eventId: 'afterScreenInit',
                    sequence: '30',
                    handler: {
                        methodName: "initializeScreen"
                    }
                }, {
                    eventId: 'extn_changeStore_onClick',
                    sequence: '25',
                    description: '',
                    listeningControlUId: 'extn_changeStore',
                    handler: {
                        methodName: "changeStore",
                        description: ""
                    }
                }, {
                    eventId: 'afterBehaviorMashupCall',
                    sequence: '51',
                    handler: {
                        methodName: "handleStoreChange"
                    }
                }
                /*, {
                                eventId: 'filteringSelectCarrier_onChange',
                                sequence: '51',
                                description: '',
                                listeningControlUId: 'filteringSelectCarrier',
                                handler: {
                                    methodName: "onFilteringSelectChange",
                                    description: ""
                                }
                            }*/
            ]
        },
        sameVal: true,
        firstRun: true,
        storeInstance: '',
        /*onFilteringSelectChange: function(event, bEvent, ctrl, args) {
            //var oldValue = null;
            //oldValue = _scBaseUtils.getAttributeValue("initialValue", false, args.originatingControlInstance);
            if (_scBaseUtils.equals(event, this.storeInstance)) {
                this.sameVal = true;
            } else {
                this.sameVal = false;
                this.storeInstance=event;
            }
            this.firstRun = false;
            var a = null;
            var b = null;
        },*/
        initializeScreen: function(event, bEvent, ctrl, args) {
            var a = null;
            var item = null;
            var sUserID = _scUserprefs.getUserId();
            var userHierarchyInput = _scModelUtils.createNewModelObjectWithRootKey("User");
            _scModelUtils.setStringValueAtModelPath("User.Loginid", sUserID, userHierarchyInput);
            _iasUIUtils.callApi(this, userHierarchyInput, "getUserHierarchy_ref", null);
            var data = null;
        },
        changeStore: function(event, bEvent, ctrl, args) {
            var selectedStore = _scScreenUtils.getTargetModel(this, "extn_storeInput", null);
            var store = _scModelUtils.getStringValueFromPath("User.CurrentStore", selectedStore);
            var storeValueModel = _scScreenUtils.getModel(this, "extn_storeDescription");
            var initialStoreValue = _scModelUtils.getStringValueFromPath("Store.Description", storeValueModel);
            if (_scBaseUtils.isVoid(this.storeInstance)) {
                if (_scBaseUtils.equals(store, initialStoreValue)) {
                    this.firstRun = true;
                } else {
                    this.firstRun = false;
                }
            } else {
                this.firstRun = false;
            }
            if (_scBaseUtils.equals(store, this.storeInstance)) {
                this.sameVal = true;
            } else {
                this.sameVal = false;
                this.storeInstance = store;
            }
            if (_scBaseUtils.equals(this.firstRun, false)) {
                if (!_scBaseUtils.equals(this.sameVal, true)) {
                    var c = null;
                    var model = null;
                    model = _scModelUtils.createNewModelObjectWithRootKey("Store");
                    var currentEditor = _scEditorUtils.getCurrentEditor();
                    _scModelUtils.setStringValueAtModelPath("Store.StoreID", store, model);
                    _iasUIUtils.callApi(this, model, "changeStoreContext_ref", null);
                    this.sameVal = true;
                }
            }
        },
        handleStoreChange: function(event, bEvent, ctrl, args) {
            var isMashupAvailable = _extnExpicientUtils.isMashupAvailable("changeStoreContext_ref", args);
            if (isMashupAvailable) {
                var modelOutput = null;
                modelOutput = _extnExpicientUtils.getMashupOutputForRefID("changeStoreContext_ref", args);
                var storeID = _scModelUtils.getStringValueFromPath("ShipNode.ShipNode", modelOutput);
                var nodeDescription = _scModelUtils.getStringValueFromPath("ShipNode.Description", modelOutput);
                _iasContextUtils.addToContext("CurrentStore", storeID);
                var currentEditor = _scEditorUtils.getCurrentEditor().className;
                var scScreenContainerInstance = scApplicationInfo.getSCScreenContainerInstance();
                var allopenEditorsArray = scScreenContainerInstance.getChildren();
                if (!_scBaseUtils.isVoid(allopenEditorsArray)) {
                    for (var i = 0; i < allopenEditorsArray.length; i++) {
                        var referenceArray = null;
                        referenceArray = _scBaseUtils.getArrayItemByIndex(allopenEditorsArray, i);
                        var classEditor = null;
                        classEditor = referenceArray.className;
                        if (!_scBaseUtils.equals(currentEditor, classEditor)) {
                            _scEditorUtils.closeEditor(referenceArray);
                        }
                    }
                }
                var headerElement = document.getElementsByClassName("idxHeaderUserName")[0].title;
                var headerElementUserName = headerElement.split("|")[0];
                var finalHeader = headerElementUserName + "| " + nodeDescription;
                document.getElementsByClassName("idxHeaderUserName")[0].title = finalHeader;
                var headerText = document.getElementsByClassName("idxHeaderUserText")[0].innerHTML;
                var headerTextUserName = headerText.split("|")[0];
                var finalText = headerTextUserName + "| " + nodeDescription;
                document.getElementsByClassName("idxHeaderUserText")[0].innerHTML = finalText;
                _scEventUtils.fireEventGlobally(this, "extn_storeContextChanged", null, args);
                var model = null;
                model = _scBaseUtils.getNewModelInstance();
                _scEventUtils.fireEventGlobally(this, "extn_refreshBopusDashboard", null, model);
                _scEventUtils.fireEventGlobally(this, "extn_refreshAlert", null, model);
            }
        },
        handleMashupCompletion: function(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data) {
            _iasBaseTemplateUtils.handleMashupCompletion(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data, this);
        },
        handleMashupOutput: function(mashupRefId, modelOutput, mashupInput, mashupContext, applySetModel) {
            if (_scBaseUtils.equals(mashupRefId, "getUserHierarchy_ref")) {
                var currentStore = _iasContextUtils.getFromContext("CurrentStore");
                _scModelUtils.setStringValueAtModelPath("User.CurrentStore", currentStore, modelOutput);
                _scBaseUtils.setModel(this, "extn_getShip", modelOutput, null);
                var headerText = document.getElementsByClassName("idxHeaderUserText")[0].innerHTML;
                var headerTextStore = headerText.split("|")[1].trim();
                var model = _scBaseUtils.getNewModelInstance();
                _scModelUtils.setStringValueAtModelPath("Store.Description", headerTextStore, model);
                _scScreenUtils.setModel(this, "extn_storeDescription", model, null);
                var accessibleOrg = _scModelUtils.getStringValueFromPath("User.AccessibleOrganizations", modelOutput);
                // _scWidgetUtils.hideWidget(parentScreen, "customerPickPortlet");
                if (_scBaseUtils.isVoid(accessibleOrg)) {
                    var parentScreen = null;
                    parentScreen = this.getOwnerScreen();
                    _scWidgetUtils.hideWidget(parentScreen, "extn_storeContextPortlet");
                }
                _scScreenUtils.getModel(this, "extn_getShip");
            }
        }
    });
});
