scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/text", "scbase/loader!sc/plat/dojo/controller/ServerDataController"], function(_dojodeclare, _dojokernel, _dojotext, _scServerDataController) {
    return _dojodeclare("extn.customScreen.StoreContextPortletBehaviorController", [_scServerDataController], {
        screenId: 'extn.customScreen.StoreContextPortlet',
        childControllers: [],
        mashupRefs: [{
            callSequence: '',
            mashupRefId: 'getUserHierarchy_ref',
            sequence: '',
            sourceBindingOptions: '',
            mashupId: 'getUserHierarchy'
        }, {
            callSequence: '',
            mashupRefId: 'changeStoreContext_ref',
            sequence: '',
            sourceBindingOptions: '',
            mashupId: 'changeStoreContext'
        }]
    });
});
