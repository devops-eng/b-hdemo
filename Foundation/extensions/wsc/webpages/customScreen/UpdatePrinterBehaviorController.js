scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/text", "scbase/loader!sc/plat/dojo/controller/ServerDataController"], function(_dojodeclare, _dojokernel, _dojotext, _scServerDataController) {
    return _dojodeclare("extn.customScreen.UpdatePrinterBehaviorController", [_scServerDataController], {
        screenId: 'extn.customScreen.UpdatePrinter',
        childControllers: [],
         mashupRefs: [{
            callSequence: '',
            mashupRefId: 'getDocumentTypeList_ref',
            sequence: '',
            sourceBindingOptions: '',
            mashupId: 'getDocumentTypeList'
        }, {
            callSequence: '',
            mashupRefId: 'getDefaultStorePrinterList_ref',
            sequence: '',
            sourceBindingOptions: '',
            mashupId: 'getDefaultStorePrinterList'
        }, {
			callSequence: '',
            mashupRefId: 'changeUserPrinter_ref',
            sequence: '',
            sourceBindingOptions: '',
            mashupId: 'changeUserPrinter'
        }]
    });
});
