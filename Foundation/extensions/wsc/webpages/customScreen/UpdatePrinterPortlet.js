scDefine(["dojo/text!./templates/UpdatePrinterPortlet.html", "scbase/loader!dijit/form/Button", "scbase/loader!dojo/_base/declare", "scbase/loader!idx/form/TextBox", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/widgets/Label", "scbase/loader!sc/plat/dojo/widgets/Link", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/GridxUtils", "scbase/loader!gridx/Grid", "scbase/loader!sc/plat/dojo/binding/GridxDataBinder", "scbase/loader!ias/utils/RepeatingScreenUtils", "scbase/loader!sc/plat/dojo/utils/RepeatingPanelUtils", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!sc/plat/dojo/utils/EditorUtils", "scbase/loader!sc/plat/dojo/info/ApplicationInfo"], function(templateText, _dijitButton, _dojodeclare, _idxTextBox, _idxContentPane, _scBaseUtils, _scWidgetUtils, _scEventUtils, _scLabel, _scLink, _scScreen, _scModelUtils, _iasUIUtils, _scScreenUtils, _iasBaseTemplateUtils, _scWidgetUtils, _scEventUtils, _scGridxUtils, _gridxGrid, _scGridxDataBinder, _iasRepeatingScreenUtils, _scRepeatingPanelUtils, _scUserprefs, _iasContextUtils, _extnExpicientUtils, _scEditorUtils, scApplicationInfo) {
    return _dojodeclare("extn.customScreen.UpdatePrinterPortlet", [_scScreen], {
        templateString: templateText,
        uId: "UpdatePrinterPortlet",
        packageName: "extn.customScreen",
        className: "UpdatePrinterPortlet",
        namespaces: {
            targetBindingNamespaces: [{
                value: 'extn_storeInput',
                description: "shipment "
            }],
            sourceBindingNamespaces: [{
                value: 'getPrinterType_output',
                description: "printer list "
            }, {
                value: 'deviceNPackStation_output',
                description: "device and packstation list "
            }]
        },
        staticBindings: [],
        events: [],
        subscribers: {
            local: [{
                eventId: 'afterScreenInit',
                sequence: '30',
                handler: {
                    methodName: "initializeScreen"
                }
            }, {
                eventId: 'changeService_onClick',
                sequence: '25',
                description: '',
                listeningControlUId: 'changeService',
                handler: {
                    methodName: "changeLos",
                    description: ""
                }
            }, {
                eventId: 'filteringSelectDocumentType_onChange',
                sequence: '35',
                handler: {
                    methodName: "getPrinterList"
                }
            }, {
                eventId: 'filteringSelectPrinter_onChange',
                sequence: '35',
                handler: {
                    methodName: "getPrinterIDList"
                }
            }, {
                eventId: 'filteringPackStation_onChange',
                sequence: '35',
                handler: {
                    methodName: "getPackStationList"
                }
            }]
        },
        changeLos: function(event, bEvent, ctrl, args) {
            //Device ID
            var targetModel = null;
            targetModel = _scScreenUtils.getTargetModel(this, "extn_selectedPrinter", null);
            var selectedPrinterAddress = null;
            selectedPrinterAddress = _scModelUtils.getStringValueFromPath("Device.DeviceId", targetModel);
            //Document Type
            var documentTypeModel = null;
            documentTypeModel = _scScreenUtils.getTargetModel(this, "extn_selectedDocType", null);
            var selectedDocumentType = null;
            selectedDocumentType = _scModelUtils.getStringValueFromPath("CommonCode.CodeValue", documentTypeModel);
            //Pack Station
            var packStationModel = null;
            packStationModel = _scScreenUtils.getTargetModel(this, "extn_selectedPackStation", null);
            var packStation = null;
            packstation = _scModelUtils.getStringValueFromPath("Device.PackStation", packStationModel);
            var shipNode = _iasContextUtils.getFromContext("CurrentStore");
            var sUserID = _scUserprefs.getUserId();
            var inputModel = _scModelUtils.createNewModelObjectWithRootKey("HBCUserPrinter");
            if (_scBaseUtils.isVoid(selectedDocumentType)) { //Document Type empty
                _iasBaseTemplateUtils.showMessage(this, "Please select document type", "error", null);
            } else if ((!_scBaseUtils.isVoid(selectedDocumentType)) && ((_scBaseUtils.isVoid(selectedPrinterAddress) || (_scBaseUtils.isVoid(packstation))))) {
                //Device id & packstation id empty
                _iasBaseTemplateUtils.showMessage(this, "Please select both Device ID and Packstation ID", "error", null);
            } else {
                _scModelUtils.setStringValueAtModelPath("HBCUserPrinter.DocumentType", selectedDocumentType, inputModel);
                if (selectedPrinterAddress) {
                    _scModelUtils.setStringValueAtModelPath("HBCUserPrinter.PrinterAddress", selectedPrinterAddress, inputModel);
                }
                _scModelUtils.setStringValueAtModelPath("HBCUserPrinter.UserID", sUserID, inputModel);
                _scModelUtils.setStringValueAtModelPath("HBCUserPrinter.StoreID", shipNode, inputModel);
                if (packStationModel && packStationModel.Device && packStationModel.Device.PackStation) {
                    _scModelUtils.setStringValueAtModelPath("HBCUserPrinter.PackStationID", packStationModel.Device.PackStation, inputModel);
                }
                _iasUIUtils.callApi(this, inputModel, "changeUserPrinter_ref", null);
            }
        },
        initializeScreen: function(event, bEvent, ctrl, args) {
            var cookieValue = this.getCookie('PackStation_ID');
            if (cookieValue == "") {
                //open popup
                var bindings = _scBaseUtils.getNewBeanInstance();
                var popupParams = _scBaseUtils.getNewModelInstance();
                var dialogParams = _scBaseUtils.getNewModelInstance();
                var newInputModel = _scBaseUtils.getNewModelInstance();
                var outModel = {};
                var inputModel = {};
                _scBaseUtils.addModelValueToBean("InputOrderData", newInputModel, bindings);
                _scBaseUtils.addModelValueToBean("CompleteOrderData", outModel, bindings);
                _scBaseUtils.addBeanValueToBean("binding", bindings, popupParams);
                _scBaseUtils.setAttributeValue("closeCallBackHandler", "packStationCallBackHandler", dialogParams);
                _scBaseUtils.addStringValueToBean("outputNamespace", "getPackStationDetails_output", popupParams);
                _iasUIUtils.openSimplePopup("extn.customScreen.PackStationPopup", "Update PackStation", this, popupParams, dialogParams);
            } else {
                // call api
                this.getDefaultDeviceList();
            }
            var inputModel_2 = _scModelUtils.createNewModelObjectWithRootKey("CommonCode");
            // _scModelUtils.setStringValueAtModelPath("CommonCode.CallingOrganizationCode", enterpriseCode, inputModel_2);
            _iasUIUtils.callApi(this, inputModel_2, "getDocumentTypeList_ref", null);
        },
        getDefaultDeviceList: function(event, bEvent, ctrl, args) {
            var inputModel_2 = _scModelUtils.createNewModelObjectWithRootKey("Device");
            _scModelUtils.setStringValueAtModelPath("Device.OrganizationCode", _iasContextUtils.getFromContext("CurrentStore"), inputModel_2);
            var sDeviceType = 'Printer';
            _scModelUtils.setStringValueAtModelPath("Device.DeviceType", sDeviceType, inputModel_2);
            _iasUIUtils.callApi(this, inputModel_2, "getDefaultStorePrinterList_ref", null);
        },
        packStationCallBackHandler: function(actionPerformed, model, popupParams) {
            if (_scBaseUtils.equals(actionPerformed, "APPLY")) {
                var packStation = model.DeviceList.HBCUserPrinter[0].PackStationID;
                this.setCookie(packStation);
                var that = this;
                setTimeout(function() {
                    // _scWidgetUtils.setFocusOnControl(that, "scanProductIdTxt");
                    _iasUIUtils.callApi(that, model, "setPackStation_ref", null);
                }, 500);
            }
        },
        getCookie: function(cookiename) {
            // Get name followed by anything except a semicolon
            var cookiestring = RegExp("" + cookiename + "[^;]+").exec(document.cookie);
            // Return everything after the equal sign, or an empty string if the cookie name not found
            return unescape(!!cookiestring ? cookiestring.toString().replace(/^[^=]+./, "") : "");
        },
        setCookie: function(cookieValue) {
            //document.cookie = "PackStation_ID=" + cookieValue;
            var CookieDate = new Date;
            CookieDate.setFullYear(CookieDate.getFullYear() + 1);
            document.cookie = 'PackStation_ID=' + cookieValue + '; expires=' + CookieDate.toGMTString() + ';';
        },
        handleMashupCompletion: function(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data) {
            _iasBaseTemplateUtils.handleMashupCompletion(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data, this);
        },
        handleMashupOutput: function(mashupRefId, modelOutput, mashupInput, mashupContext, applySetModel) {
            if (_scBaseUtils.equals(mashupRefId, "changeUserPrinter_ref")) {
                _iasBaseTemplateUtils.showMessage(this, "Updated Successfully", "success", null);
                _scWidgetUtils.setValue(this, "filteringSelectPrinter", "", false);
                _scWidgetUtils.setValue(this, "filteringPackStation", "", false);
                _scWidgetUtils.setValue(this, "filteringSelectDocumentType", "", false);
                if (mashupInput && mashupInput.HBCUserPrinter && mashupInput.HBCUserPrinter.PackStationID) {
                    this.setCookie(mashupInput.HBCUserPrinter.PackStationID);
                }
            }
            if (_scBaseUtils.equals(mashupRefId, "getDocumentTypeList_ref")) {
                _scScreenUtils.setModel(this, "getDocType_input", modelOutput);
                var headerElement = document.getElementsByClassName("idxHeaderUserName")[0].title;
            }
            if (_scBaseUtils.equals(mashupRefId, "getDefaultStorePrinterList_ref")) {
                var packStations = [];
                if (modelOutput.Devices && modelOutput.Devices.Device.length > 0) {
                    var deviceArray = modelOutput.Devices.Device;
                    for (var i = 0; i < deviceArray.length; i++) {
                        var currDevice = _scBaseUtils.getArrayItemByIndex(deviceArray, i);
                        currDevice.PackStation = "";
                        if (currDevice && currDevice.DeviceParamsXML && currDevice.DeviceParamsXML.Attributes && currDevice.DeviceParamsXML.Attributes.Attribute.length > 0) {
                            var currDeviceAttributeList = currDevice.DeviceParamsXML.Attributes.Attribute;
                            for (var m = 0; m < currDeviceAttributeList.length; m++) {
                                var currAttribute = _scBaseUtils.getArrayItemByIndex(currDeviceAttributeList, m);
                                if (_scBaseUtils.equals(currAttribute.Name, "PackStation")) {
                                    currDevice.PackStation = currAttribute.Value;
                                    break;
                                }
                            }
                        }
                    }
                }
                if (mashupInput && mashupInput.Device && mashupInput.Device.DeviceSubType) {
                    _scScreenUtils.setModel(this, "getPrinterType_output", modelOutput);
                    var clonedModel = _scBaseUtils.cloneModel(modelOutput);
                    _scScreenUtils.setModel(this, "deviceNPackStation_output", clonedModel);
                } else {
                    var id = this.getCookie('PackStation_ID');
                    var printerList = _scBaseUtils.cloneModel(modelOutput);
                    var stationModel = _scModelUtils.createNewModelObjectWithRootKey("DeviceList");
                    stationModel.DeviceList.HBCUserPrinter = [];
                    if (printerList.Devices && (printerList.Devices.Device.length > 0)) {
                        var deviceList = _scModelUtils.getStringValueFromPath("Devices.Device", printerList);
                        for (var j = 0; j < deviceList.length; j++) {
                            var currDevice = _scBaseUtils.getArrayItemByIndex(deviceList, j);
                            if (_scBaseUtils.equals(currDevice.PackStation, id)) {
                                valExist = true;
                                var newModel = {
                                    "PackStationID": currDevice.PackStation,
                                    "DocumentType": currDevice.DeviceSubType,
                                    "PrinterAddress": currDevice.DeviceId,
                                    "PrinterName": "",
                                    "StoreID": _iasContextUtils.getFromContext("CurrentStore"),
                                    "UserID": _scUserprefs.getUserId()
                                };
                                stationModel.DeviceList.HBCUserPrinter.push(newModel);
                            }
                        }
                    }
                    _iasUIUtils.callApi(this, stationModel, "setPackStation_ref", null);
                }
            }
        },
        getPrinterList: function(event, bEvent, ctrl, args) {
            var dummyModel = {};
            _scScreenUtils.setModel(this, "printerDetailsModel", dummyModel);
            _scWidgetUtils.setValue(this, "filteringSelectPrinter", "", false);
            _scWidgetUtils.setValue(this, "filteringPackStation", "", false);
            var targetModel = null;
            targetModel = _scScreenUtils.getTargetModel(this, "extn_selectedDocType", null);
            var model = _scScreenUtils.getModel(this.getOwnerScreen(), "getShipmentDetails_output");
            var shipNode = _iasContextUtils.getFromContext("CurrentStore");
            var selectedDocumentType = null;
            selectedDocumentType = _scModelUtils.getStringValueFromPath("CommonCode.CodeValue", targetModel);
            var inputModel_2 = _scModelUtils.createNewModelObjectWithRootKey("Device");
            _scModelUtils.setStringValueAtModelPath("Device.DeviceSubType", selectedDocumentType, inputModel_2);
            _scModelUtils.setStringValueAtModelPath("Device.OrganizationCode", shipNode, inputModel_2);
            var sDeviceType = 'Printer';
            _scModelUtils.setStringValueAtModelPath("Device.DeviceType", sDeviceType, inputModel_2);
            _iasUIUtils.callApi(this, inputModel_2, "getDefaultStorePrinterList_ref", null);
        },
        getPrinterIDList: function(event, bEvent, ctrl, args) {
            var targetModel = null;
            targetModel = _scScreenUtils.getTargetModel(this, "extn_selectedPrinter", null);
            var deviceID = _scModelUtils.getStringValueFromPath("Device.DeviceId", targetModel);
            if (deviceID) {
                this.changeModelBasedOnAttribute("DeviceId", deviceID);
            }
        },
        getPackStationList: function(event, bEvent, ctrl, args) {
            var targetModel = null;
            targetModel = _scScreenUtils.getTargetModel(this, "extn_selectedPackStation", null);
            var packStation = _scModelUtils.getStringValueFromPath("Device.PackStation", targetModel);
            if (packStation) {
                this.changeModelBasedOnAttribute("PackStation", packStation);
            }
        },
        changeModelBasedOnAttribute: function(attribute, value) {
            var oldModel = _scScreenUtils.getModel(this, "getPrinterType_output");
            var newModel = _scBaseUtils.cloneModel(oldModel);
            if (_scBaseUtils.equals(attribute, "PackStation")) {
                var packStationModel = _scBaseUtils.cloneModel(oldModel);
                packStationModel.Devices.Device = [];
                if (newModel.Devices.Device.length > 0) {
                    for (var n = 0; n < newModel.Devices.Device.length; n++) {
                        var currDevice = _scBaseUtils.getArrayItemByIndex(newModel.Devices.Device, n);
                        if (_scBaseUtils.equals(currDevice.PackStation, value)) {
                            packStationModel.Devices.Device.push(currDevice);
                        }
                    }
                }
                _scScreenUtils.setModel(this, "deviceNPackStation_output", packStationModel);
            } else if (_scBaseUtils.equals(attribute, "DeviceId")) {
                var deviceModel = _scBaseUtils.cloneModel(oldModel);
                deviceModel.Devices.Device = [];
                if (newModel.Devices.Device.length > 0) {
                    for (var n = 0; n < newModel.Devices.Device.length; n++) {
                        var currDevice = _scBaseUtils.getArrayItemByIndex(newModel.Devices.Device, n);
                        if (_scBaseUtils.equals(currDevice.DeviceId, value)) {
                            deviceModel.Devices.Device.push(currDevice);
                        }
                    }
                }
                _scScreenUtils.setModel(this, "deviceNPackStation_output", deviceModel);
            }
        }
    });
});
