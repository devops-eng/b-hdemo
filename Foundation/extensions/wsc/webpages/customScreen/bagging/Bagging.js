scDefine(["dojo/text!./templates/Bagging.html", "scbase/loader!dojo/_base/declare", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!ias/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/RepeatingPanelUtils", "scbase/loader!sc/plat/dojo/info/ApplicationInfo", "scbase/loader!ias/utils/EventUtils"], function(templateText, _dojodeclare, _scUserprefs, _scScreen, _scScreenUtils, _scModelUtils, _iasUIUtils, _extnExpicientUtils, _scBaseUtils, _scEventUtils, _iasBaseTemplateUtils, _iasScreenUtils, _scWidgetUtils, _scRepeatingPanelUtils, scApplicationInfo, _iasEventUtils) {
    return _dojodeclare("extn.customScreen.bagging.Bagging", [_scScreen], {
        templateString: templateText,
        uId: "Bagging",
        packageName: "extn.customScreen.bagging",
        className: "Bagging",
        namespaces: {
            sourceBindingNamespaces: [{
                value: 'shipmentNoNS',
                description: "This model is used for item pack rule"
            },
            {
                value: 'extn_slaIndicatorNamespace',
                description: "SLA data"

            }
            ]
        },
        staticBindings: [{
            targetBinding: {
                path: 'Shipment.ShipmentKey',
                namespace: 'updateShipmentPickQuantity_input'
            },
            sourceBinding: {
                path: 'Shipment.ShipmentKey',
                namespace: 'baggingModel'
            }
        }, {
            targetBinding: {
                path: 'BarCode.ShipmentContextualInfo.ShipmentKey',
                namespace: 'translateBarCode_input'
            },
            sourceBinding: {
                path: 'Shipment.ShipmentKey',
                namespace: 'baggingModel'
            }
        }, {
            targetBinding: {
                path: 'BarCode.ShipmentContextualInfo.SellerOrganizationCode',
                namespace: 'translateBarCode_input'
            },
            sourceBinding: {
                path: 'Shipment.SellerOrganizationCode',
                namespace: 'baggingModel'
            }
        }, {
            targetBinding: {
                path: 'BarCode.ContextualInfo.EnterpriseCode',
                namespace: 'translateBarCode_input'
            },
            sourceBinding: {
                path: 'Shipment.EnterpriseCode',
                namespace: 'baggingModel'
            }
        }],
        subscribers: {
            local: [{
                    eventId: 'afterScreenInit',
                    sequence: '30',
                    description: 'This event is raised after screen has been initialized',
                    handler: {
                        methodName: "initializeScreen"
                    }
                }, {
                    eventId: 'updateShipmentLineDetails',
                    sequence: '30',
                    description: 'This method is used to handle Key Up event of Scan Product text field.',
                    handler: {
                        methodName: "updateShipmentLineDetails"
                    }
                }, {
                    eventId: 'closeToSummary_onClick',
                    sequence: '30',
                    description: 'This method is used to go to summary',
                    listeningControlUId: 'closeToSummary',
                    handler: {
                        methodName: "goToSummary"
                    }
                }, {
                    eventId: 'scanProductIdTxt_onKeyDown',
                    sequence: '30',
                    description: 'This method is used to handle Key Up event of Scan Product text field.',
                    handler: {
                        methodName: "scanProductOnEnter"
                    }
                }, {
                    eventId: 'addProductButton_onClick',
                    sequence: '30',
                    description: 'Subscriber for Scan/Add button',
                    listeningControlUId: 'addProductButton',
                    handler: {
                        methodName: "scanProduct",
                        description: "Handled for Scan product"
                    }
                }, {
                    eventId: 'pickNBag_onClick',
                    sequence: '30',
                    description: 'Subscriber for pick n bag button',
                    listeningControlUId: 'pickNBag',
                    handler: {
                        methodName: "bagProduct",
                        description: "Handled for bag product"
                    }
                }
                // , {
                //     eventId: 'afterBehaviorMashupCall',
                //     sequence: '51',
                //     handler: {
                //         methodName: "extn_afterBehaviorMashupCall"
                //     }
                // }
            ]
        },
        activeRepeatingPanelUId: 'baggingLineScreenPanel',
        initializeScreen: function() {
            // modelObject = {};
            // _scModelUtils.setStringValueAtModelPath("Portlet.Title", "213444324", modelObject);
            // _scScreenUtils.setModel(this, "shipmentNoNS", modelObject, null);
            // modelObject2 = {};
            // _scModelUtils.setStringValueAtModelPath("StoreBatchLine.DepartmentDisplayDesc", "101010110", modelObject2);
            // _scScreenUtils.setModel(this, "BatchLine", modelObject2, null);
            //var inputArray = null;
            inputArray = [];
            inputArray.push("7");
            var completePortletTitle = null;
            completePortletTitle = _scScreenUtils.getFormattedString(this, "No_of_Products", inputArray);
            sc.plat.dojo.utils.WidgetUtils.setLabel(this, "productNo", completePortletTitle);
            // extn_getBaggingLineDetail_refId
            var toBeShortedBatchLineModel = {};
            _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", this.getInitialInputData(this).Shipment.ShipmentKey, toBeShortedBatchLineModel);
            _iasUIUtils.callApi(this, toBeShortedBatchLineModel, "bagging_getShipmentDetails_ref", null);
        },
        // extn_afterBehaviorMashupCall: function(event, bEvent, ctrl, args) {
        //     if (_extnExpicientUtils.isMashupAvailable("bagging_getShipmentDetails_ref", args)) {
        //         //  _scEventUtils.fireEventGlobally(this, "changeEditorTitle", null, args);
        //     }
        // },
        bagProduct: function(event, bEvent, ctrl, args) {
            var bagModel = null;
            bagModel = _scScreenUtils.getModel(this, "baggingModel");
            var noBagsModel = _scScreenUtils.getTargetModel(this, "Bags_input");
            var bags = _scModelUtils.getStringValueFromPath("Shipment.DestinationZone", noBagsModel);
            if ((_scBaseUtils.numberGreaterThan(Number(bags), 0)) && (_scBaseUtils.numberLessThan(bags, 100))) {
                var shipmentModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
                _scModelUtils.setStringValueAtModelPath("Shipment.DocumentType", "0001", shipmentModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.EnterpriseCode", bagModel.Shipment.EnterpriseCode, shipmentModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.DestinationZone", bags, shipmentModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.ShipNode", bagModel.Shipment.ShipNode.ShipNode, shipmentModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", bagModel.Shipment.ShipmentKey, shipmentModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.SellerOrganizationCode", bagModel.Shipment.SellerOrganizationCode, shipmentModel);
                var newShipmentLineList = [];
                var existingShipmentLines = _scModelUtils.getModelObjectFromPath("Shipment.ShipmentLines.ShipmentLine", bagModel);
                var allLinesBaggedShorted = true;
                for (var m = 0; m < existingShipmentLines.length; m++) {
                    var currLine1 = _scBaseUtils.getArrayItemByIndex(existingShipmentLines, m);
                    if (!_scBaseUtils.equals(Number(currLine1.Extn.ExtnBaggedQty), Number(currLine1.Quantity))) {
                        allLinesBaggedShorted = false;
                        break;
                    }
                }
                if (_scBaseUtils.equals(allLinesBaggedShorted, true)) {
                    for (var n = 0; n < existingShipmentLines.length; n++) {
                        var currLine = _scBaseUtils.getArrayItemByIndex(existingShipmentLines, n);
                        if (!_scBaseUtils.equals(currLine.OrderLine.Status, "Cancelled")) {
                            var shipLineModel = _scModelUtils.createNewModelObjectWithRootKey("ShipmentLine");
                            var shipmentLine = {
                                "DocumentType": "0001",
                                "ItemID": currLine.OrderLine.ItemDetails.ItemID,
                                "OrderHeaderKey": currLine.OrderHeaderKey,
                                "OrderLineKey": currLine.OrderLineKey,
                                "ProductClass": currLine.OrderLine.ItemDetails.ProductClass,
                                "Quantity": currLine.Quantity,
                                "ShipmentKey": currLine.ShipmentKey,
                                "ShipmentLineKey": currLine.ShipmentLineKey,
                                "UnitOfMeasure": currLine.OrderLine.ItemDetails.UnitOfMeasure
                            };
                            newShipmentLineList.push(shipmentLine);
                        }
                    }
                    if (newShipmentLineList.length > 0) {
                        _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentLines.ShipmentLine", newShipmentLineList, shipmentModel);
                        _iasUIUtils.callApi(this, shipmentModel, "confirmBaggingShipment_ref", null);
                    } else {
                        _iasBaseTemplateUtils.showMessage(this, "All lines are cancelled", "error", null);
                        //all lines cancelled
                    }
                } else {
                    _iasBaseTemplateUtils.showMessage(this, "All shipment lines are not picked", "error", null);
                    //All lines not bagged/shorted
                }
                // <Shipment DocumentType="" EnterpriseCode="" ShipNode="" ShipmentKey="" SellerOrganizationCode="">
                //        <ShipmentLines>
                //            <ShipmentLine DocumentType="" ItemID="" OrderHeaderKey="" OrderLineKey="" ProductClass="" Quantity="" ShipmentKey="" ShipmentLineKey="" UnitOfMeasure="" />
                //        </ShipmentLines>
                //    </Shipment>
            } else {
                //no of bags error
                _iasBaseTemplateUtils.showMessage(this, "Number of bags should be between 0 to 100", "error", null);
            }
        },
        updateShipmentLineDetails: function(event, bEvent, ctrl, args) {
            //this.backroomPickSaved = false;
            var shipmentModel = null;
            shipmentModel = _scBaseUtils.getModelValueFromBean("inputData", args);
            var shipmentKey = shipmentModel.Shipment.ShipmentKey;
            var action = shipmentModel.Shipment.Action;
            var shortageReason = shipmentModel.Shipment.ShortageReasonCode;
            var shipmentLine = {};
            if (shipmentModel.Shipment && shipmentModel.Shipment.ShipmentLines.length && shipmentModel.Shipment.ShipmentLines[0].ShipmentLine) {
                shipmentLine = shipmentModel.Shipment.ShipmentLines[0].ShipmentLine;
            } else if (shipmentModel.Shipment && shipmentModel.Shipment.ShipmentLines && shipmentModel.Shipment.ShipmentLines.ShipmentLine) {
                shipmentLine = shipmentModel.Shipment.ShipmentLines.ShipmentLine[0];
            }
            var shipmentLineKey = shipmentLine.ShipmentLineKey;
            var updateqty = "1";
            var shortageQty = shipmentLine.ShortageQty;
            var baggedQty = shipmentLine.Extn.ExtnBaggedQty;
            var newShipmentModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
            _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", shipmentKey, newShipmentModel);
            newShipmentModel.Shipment.ShipmentLines = [];
            var newShipmentLineModel = _scModelUtils.createNewModelObjectWithRootKey("ShipmentLine");
            _scModelUtils.setStringValueAtModelPath("ShipmentLine.ShipmentLineKey", shipmentLineKey, newShipmentLineModel);
            if (_scBaseUtils.equals(action, "ADD")) {
                _scModelUtils.setStringValueAtModelPath("ShipmentLine.UpdatedBaggageQty", updateqty, newShipmentLineModel);
                _scModelUtils.setStringValueAtModelPath("ShipmentLine.Action", action, newShipmentLineModel);
            } else if (_scBaseUtils.equals(action, "SUB")) {
                _scModelUtils.setStringValueAtModelPath("ShipmentLine.UpdatedBaggageQty", updateqty, newShipmentLineModel);
                _scModelUtils.setStringValueAtModelPath("ShipmentLine.Action", action, newShipmentLineModel);
            } else if (_scBaseUtils.equals(action, "MarkLineAsShortage")) {
                _scModelUtils.setStringValueAtModelPath("ShipmentLine.ShortageQty", shortageQty, newShipmentLineModel);
                _scModelUtils.setStringValueAtModelPath("ShipmentLine.ShortageReason", shortageReason, newShipmentLineModel);
            } else {
                _scModelUtils.setStringValueAtModelPath("ShipmentLine.UpdatedBaggageQty", baggedQty, newShipmentLineModel);
                _scModelUtils.setStringValueAtModelPath("ShipmentLine.Action", action, newShipmentLineModel);
            }
            newShipmentModel.Shipment.ShipmentLines.push(newShipmentLineModel);
            if (_scBaseUtils.equals(action, "MarkLineAsShortage")) {
                _iasUIUtils.callApi(this, newShipmentModel, "shortageBaggageQty_ref", null);
            } else {
                _iasUIUtils.callApi(this, newShipmentModel, "increaseDecreaseQty_ref", null);
            }
        },
        handleMashupOutput: function(mashupRefId, modelOutput, mashupInput, mashupContext, applySetModel) {
            if (_scBaseUtils.equals(mashupRefId, "confirmBaggingShipment_ref")) {
                var printerInfo= _scModelUtils.getStringValueFromPath("Shipment.PrinterInfo", modelOutput);
               
                   var printMsg= _scModelUtils.getStringValueFromPath("PrintMessage", printerInfo);
                    _scUserprefs.setProperty("printMsg", printMsg); 

                
                this.goToSummary();
            }
            if (_scBaseUtils.equals(mashupRefId, "bagging_getShipmentDetails_ref")) {
                outputModel = modelOutput;
                _scScreenUtils.setModel(this, "baggingModel", outputModel, null);
                inputArray = [];
                inputArray.push(outputModel.Shipment.ShipmentLines.TotalNumberOfRecords);
                var completePortletTitle = null;
                completePortletTitle = _scScreenUtils.getFormattedString(this, "No_of_Products", inputArray);
                sc.plat.dojo.utils.WidgetUtils.setLabel(this, "productNo", completePortletTitle);
                var returnVal = null;
                var args = null;
                returnVal = "Order" + " " + outputModel.Shipment.DisplayOrderNo;
                args = {};
                _scBaseUtils.setAttributeValue("title", returnVal, args);
                _scEventUtils.fireEventGlobally(this, "changeEditorTitle", null, args);
                var shipmentlineListModel = null;
                shipmentlineListModel = _scModelUtils.getStringValueFromPath("Shipment.ShipmentLines.ShipmentLine", modelOutput);
                var diamondFlag = false;
                for (var j = 0; j < shipmentlineListModel.length; j++) {
                    var referenceModel1 = null;
                    referenceModel1 = _scBaseUtils.getArrayItemByIndex(shipmentlineListModel, j);
                    if (referenceModel1.OrderLine && referenceModel1.OrderLine.ItemDetails && referenceModel1.OrderLine.ItemDetails.Extn && referenceModel1.OrderLine.ItemDetails.Extn.ExtnJewelryFlag == "Y") {
                        diamondFlag = true;
                        break;
                    }
                }
                if (diamondFlag == true) {
                    _scWidgetUtils.showWidget(this, "extn_jewellarySign", false, null);
                } else {
                    _scWidgetUtils.hideWidget(this, "extn_jewellarySign");
                }
                var shipkey= _scModelUtils.getStringValueFromPath("Shipment.ShipmentKey",modelOutput);
                var shipModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
                _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", shipkey, shipModel);
                _iasUIUtils.callApi(this, shipModel, "extn_getSLAImage_ref", null);
            }
            if (_scBaseUtils.equals(mashupRefId, "extn_getSLAImage_ref")) {
                outputModel = modelOutput;
                var imgName= _scModelUtils.getStringValueFromPath("Shipment.ImageUrl", outputModel);
                 var imageUrlModel = null;
                    imageUrlModel = _scModelUtils.createNewModelObjectWithRootKey("CommonCode");
                 _scModelUtils.setStringValueAtModelPath("CommonCode.CodeLongDescription", imgName, imageUrlModel);
                  _scScreenUtils.setModel( this, "extn_slaIndicatorNamespace", imageUrlModel, null);




                _scWidgetUtils.showWidget(this, "extn_slaIndicatorLabel", false, null);
                 if(_scBaseUtils.contains(imgName, "Overdue")){
                   _scWidgetUtils.setValue(this, "extn_slaIndicatorLabel", "Missed SLA");
                 }
                 else if(_scBaseUtils.contains(imgName, "MediumPriority")){
                    _scWidgetUtils.setValue(this, "extn_slaIndicatorLabel", "Nearing SLA");
                 } 
                 else if(_scBaseUtils.contains(imgName, "LowPriority")){
                    _scWidgetUtils.setValue(this, "extn_slaIndicatorLabel", "Within SLA");
                 }

            }
            if (_scBaseUtils.equals(mashupRefId, "increaseDecreaseQty_ref")) {
                var shipmentLineKey = null;
                var shipmentLineModel = _scModelUtils.createNewModelObjectWithRootKey("ShipmentLine");
                if (mashupInput.Shipment && mashupInput.Shipment.ShipmentLines[0] && mashupInput.Shipment.ShipmentLines[0].ShipmentLine) {
                    shipmentLineKey = mashupInput.Shipment.ShipmentLines[0].ShipmentLine.ShipmentLineKey;
                }
                if (modelOutput.Shipment && modelOutput.Shipment.ShipmentLines && (modelOutput.Shipment.ShipmentLines.ShipmentLine.length > 0)) {
                    var shipmentLines = _scModelUtils.getModelObjectFromPath("Shipment.ShipmentLines.ShipmentLine", modelOutput);
                    for (var i = 0; i < shipmentLines.length; i++) {
                        var currLine = _scBaseUtils.getArrayItemByIndex(shipmentLines, i);
                        if (currLine.ShipmentLineKey == shipmentLineKey) {
                            _scModelUtils.setStringValueAtModelPath("ShipmentLine", currLine, shipmentLineModel);
                            break;
                        }
                    }
                }
                // this.refreshShipmentLine(shipmentLineModel);
                _scScreenUtils.setModel(this, "baggingModel", modelOutput, null);
            }
            if (_scBaseUtils.equals(mashupRefId, "shortageBaggageQty_ref")) {
                if (modelOutput.Shipments && modelOutput.Shipments.Shipment[0]) {
                    var newShipmentModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
                    _scModelUtils.setStringValueAtModelPath("Shipment", modelOutput.Shipments.Shipment[0], newShipmentModel);
                    _scScreenUtils.setModel(this, "baggingModel", newShipmentModel, null);
                }
            }
            if (_scBaseUtils.equals(mashupRefId, "registerBarcodeForBagging_ref")) {
                var itemExist = false;
                var barData = _scModelUtils.getStringValueFromPath("BarCode.BarCodeData", modelOutput);
                if (_scBaseUtils.equals(Number(modelOutput.BarCode.Translations.TotalNumberOfRecords), 0)) {
                    var bagModel = null;
                    bagModel = _scScreenUtils.getModel(this, "baggingModel");
                    if (bagModel.Shipment.ShipmentLines.ShipmentLine.length > 0) {
                        var lines = _scModelUtils.getModelObjectFromPath("Shipment.ShipmentLines.ShipmentLine", bagModel);
                        var n = null;
                        for (var m = 0; m < lines.length; m++) {
                            var currLine = _scBaseUtils.getArrayItemByIndex(lines, m);
                            if (currLine.OrderLine && currLine.OrderLine.ItemDetails && currLine.OrderLine.ItemDetails.ItemID) {
                                if (_scBaseUtils.equals(currLine.OrderLine.ItemDetails.ItemID, barData)) {
                                    itemExist = true;
                                    break;
                                }
                            }
                        }
                    }
                    if (_scBaseUtils.equals(itemExist, true)) {
                        //execute logic
                        this.updateLineQty(modelOutput, barData);
                    } else {
                        _iasBaseTemplateUtils.showMessage(this, "Invalid Item", "error", null);
                    }
                } else {
                    if (modelOutput.BarCode && modelOutput.BarCode.Translations && modelOutput.BarCode.Translations.Translation[0] && modelOutput.BarCode.Translations.Translation[0].ItemContextualInfo && modelOutput.BarCode.Translations.Translation[0].ItemContextualInfo.ItemID) {
                        var itemId = modelOutput.BarCode.Translations.Translation[0].ItemContextualInfo.ItemID;
                        this.updateLineQty(modelOutput, itemId);
                    }
                }
            }
        },
        updateLineQty: function(modelOutput, itemId) {
            var shipmentLineModel = {};
            var lineFound = false;
            var bagModel = null;
            bagModel = _scScreenUtils.getModel(this, "baggingModel");
            if (bagModel.Shipment.ShipmentLines.ShipmentLine.length > 0) {
                var lines = _scModelUtils.getModelObjectFromPath("Shipment.ShipmentLines.ShipmentLine", bagModel);
                for (var k = 0; k < lines.length; k++) {
                    var currLine1 = _scBaseUtils.getArrayItemByIndex(lines, k);
                    var itemID = currLine1.OrderLine.ItemDetails.ItemID;
                    var oStatus = currLine1.OrderLine.Status;
                    var remainingQty = _scModelUtils.getNumberValueFromPath("Quantity", currLine1) - _scModelUtils.getNumberValueFromPath("Extn.ExtnBaggedQty", currLine1);
                    if (_scBaseUtils.equals(itemID, itemId) && (!_scBaseUtils.equals(oStatus, "Cancelled")) && (_scBaseUtils.numberGreaterThan(remainingQty, 0))) {
                        shipmentLineModel = currLine1;
                        lineFound = true;
                        break;
                    }
                }
            }
            if (_scBaseUtils.equals(lineFound, true)) {
                var shipmentKey = bagModel.Shipment.ShipmentKey;
                var action = "ADD";
                var shipmentLine = shipmentLineModel;
                var shipmentLineKey = shipmentLine.ShipmentLineKey;
                var updateqty = "1";
                var baggedQty = shipmentLine.Extn.ExtnBaggedQty;
                var newShipmentModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
                _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", shipmentKey, newShipmentModel);
                newShipmentModel.Shipment.ShipmentLines = [];
                var newShipmentLineModel = _scModelUtils.createNewModelObjectWithRootKey("ShipmentLine");
                _scModelUtils.setStringValueAtModelPath("ShipmentLine.ShipmentLineKey", shipmentLineKey, newShipmentLineModel);
                _scModelUtils.setStringValueAtModelPath("ShipmentLine.UpdatedBaggageQty", updateqty, newShipmentLineModel);
                _scModelUtils.setStringValueAtModelPath("ShipmentLine.Action", action, newShipmentLineModel);
                newShipmentModel.Shipment.ShipmentLines.push(newShipmentLineModel);
                _iasUIUtils.callApi(this, newShipmentModel, "increaseDecreaseQty_ref", null);
            } else {
                _iasBaseTemplateUtils.showMessage(this, "Item is either completely picked or shorted", "error", null);
                //Item not Found
            }
        },
        refreshShipmentLine: function(modelOutput) {
            var baggedQtyModel = null;
            baggedQtyModel = {};
            _scModelUtils.setNumberValueAtModelPath("Quantity", _scModelUtils.getNumberValueFromPath("ShipmentLine.BackroomPickedQuantity", modelOutput), baggedQtyModel);
            var repPanelUId = null;
            repPanelUId = _scRepeatingPanelUtils.returnUIdOfIndividualRepeatingPanel(this, this.activeRepeatingPanelUId, _scModelUtils.getStringValueFromPath("ShipmentLine.ShipmentLineKey", modelOutput));
            _scRepeatingPanelUtils.setModelForIndividualRepeatingPanel(this, this.activeRepeatingPanelUId, _scModelUtils.getStringValueFromPath("ShipmentLine.ShipmentLineKey", modelOutput), "baggingLine", modelOutput, null);
        },
        handleMashupCompletion: function(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data) {
            _iasBaseTemplateUtils.handleMashupCompletion(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data, this);
        },
        getUniqueRepeatingPanelUId: function(screen, widget, namespace, dataObject, modelObject, repeatingScreenObject, repeatingScreenIndex) {
            var shipmentLineKey = null;
            shipmentLineKey = _scModelUtils.getStringValueFromPath("ShipmentLineKey", dataObject);
            return shipmentLineKey;
        },
        // extn_getRepatingScreen:function()
        // {
        // },
        noOfProducts: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
            var a = "23";
            b = {};
        },
        goToSummary: function(event, bEvent, ctrl, args) {
            var initialInput = null;
            initialInput = {};
            _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", _scModelUtils.getStringValueFromPath("Shipment.ShipmentKey", _scScreenUtils.getModel(this, "baggingModel")), initialInput);
            _iasUIUtils.openWizardInEditor("wsc.components.shipment.summary.ShipmentSummaryWizard", initialInput, "wsc.desktop.editors.ShipmentEditor", this);
            var binding = {
                "ShipmentKey": _scModelUtils.getStringValueFromPath("Shipment.ShipmentKey", _scScreenUtils.getModel(this, "baggingModel"))
            };
            var args = {
                "Binding": {}
            }
            args.Binding = binding;
            _scEventUtils.fireEventGlobally(this, "baggingSummaryRefresh", null, args);
            sc.plat.dojo.utils.EditorUtils.closeEditor(this);
        },
        scanProductOnEnter: function(event, bEvent, ctrl, args) {
            if (_iasEventUtils.isEnterPressed(event)) {
                this.scanProduct();
            }
        },
        scanProduct: function() {
            var barCodeModel = null;
            var barCodeData = null;
            if (!(_scScreenUtils.isValid(this, "translateBarCode_input"))) {
                _iasScreenUtils.showErrorMessageBoxWithOk(this, "InvalidBarCodeData");
            } else {
                barCodeModel = _scScreenUtils.getTargetModel(this, "translateBarCode_input", null);
                barCodeData = _scModelUtils.getStringValueFromPath("BarCode.BarCodeData", barCodeModel);
                if (_scBaseUtils.isVoid(barCodeData)) {
                    _iasScreenUtils.showErrorMessageBoxWithOk(this, "NoProductScanned");
                } else {
                    _iasUIUtils.callApi(this, barCodeModel, "registerBarcodeForBagging_ref", null);
                    var eventDefn = null;
                    var eventArgs = null;
                    eventArgs = {};
                    eventDefn = {};
                    _scBaseUtils.setAttributeValue("argumentList", _scBaseUtils.getNewBeanInstance(), eventDefn);
                    this.clearItemFilter();
                }
            }
        },
        clearItemFilter: function(event, bEvent, ctrl, args) {
            var tempModel = null;
            tempModel = {};
            _scScreenUtils.setModel(this, "translateBarCode_output", tempModel, null);
            _scWidgetUtils.setFocusOnWidgetUsingUid(this, "scanProductIdTxt");
        }
    });
});
