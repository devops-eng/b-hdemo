scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/text", "scbase/loader!extn/customScreen/bagging/Bagging", "scbase/loader!sc/plat/dojo/controller/ServerDataController"], function(_dojodeclare, _dojokernel, _dojotext, _extntestScreen, _scServerDataController) {
    return _dojodeclare("extn.customScreen.bagging.BaggingBehaviorController", [_scServerDataController], {
        screenId: 'extn.customScreen.bagging.Bagging',
        mashupRefs: [{
            extnType: 'ADD',
            mashupRefId: 'bagging_getShipmentDetails_ref',
            mashupId: 'bagging_getShipmentDetails'
        }, {
            extnType: 'ADD',
            mashupRefId: 'increaseDecreaseQty_ref',
            mashupId: 'increaseDecreaseQty'
        }, {
            extnType: 'ADD',
            mashupRefId: 'shortageBaggageQty_ref',
            mashupId: 'shortageBaggageQty'
        }, {
            extnType: 'ADD',
            mashupRefId: 'registerBarcodeForBagging_ref',
            mashupId: 'registerBarcodeForBagging'
        }, {
            extnType: 'ADD',
            mashupRefId: 'confirmBaggingShipment_ref',
            mashupId: 'confirmBaggingShipment'
        }, {
            extnType: 'ADD',
            mashupRefId: 'extn_getSLAImage_ref',
            mashupId: 'extn_getSLAImage'
        }]
    });
});
