scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/text", "scbase/loader!extn/customScreen/bagging/Bagging", "scbase/loader!sc/plat/dojo/controller/ScreenController"], function(_dojodeclare, _dojokernel, _dojotext, _extntestScreen, _scScreenController) {
    return _dojodeclare("extn.customScreen.bagging.BaggingInitController", [_scScreenController], {
        screenId: 'extn.customScreen.bagging.Bagging',
        mashupRefs: []
    });
});
