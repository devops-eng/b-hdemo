scDefine(["dojo/text!./templates/BaggingLineDetails.html", "scbase/loader!dojo/_base/declare", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/Userprefs", 
        "scbase/loader!ias/utils/ContextUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", 
        "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!dojo/currency", "scbase/loader!wsc/components/product/common/utils/ProductUtils", 
        "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!wsc/components/product/common/utils/ProductUtils", 
        "scbase/loader!wsc/components/shipment/backroompick/utils/BackroomPickUpUtils", "scbase/loader!wsc/components/shipment/common/screens/ShipmentLineDetailsUI", 
        "scbase/loader!wsc/components/shipment/common/utils/ShipmentUtils", "scbase/loader!ias/utils/EventUtils", "scbase/loader!ias/utils/ScreenUtils", 
        "scbase/loader!ias/utils/UOMUtils", "scbase/loader!ias/utils/BaseTemplateUtils"], function(templateText, _dojodeclare, _scScreen, _scUserprefs, _iasContextUtils, _scScreenUtils, _iasUIUtils, _scBaseUtils, _scModelUtils, _scWidgetUtils, dCurrency, _wscProductUtils, _scEventUtils, _wscProductUtils, _wscBackroomPickUpUtils, _wscShipmentLineDetailsUI, _wscShipmentUtils, _iasEventUtils, _iasScreenUtils, _iasUOMUtils, _iasBaseTemplateUtils) {
    return _dojodeclare("extn.customScreen.bagging.BaggingLineDetails", [_scScreen], {
        templateString: templateText,
        uId: "BaggingLineDetails",
        packageName: "extn.customScreen.bagging",
        className: "BaggingLineDetails",
        flowName: 'Bagging',
        baggedQuantity: '0',
        shortageResolved: false,
        namespaces: {
            targetBindingNamespaces: [{
                value: 'Shortage_Output',
                description: "This model is used to pass shipment information to shortage popups/screen "
            }, {
                value: 'ShipmentLine_Output',
                description: "This namespace contains the shipment line details"
            }],
            sourceBindingNamespaces: [{
                value: 'baggingLine',
                description: "This model is used for item pack rule"
            }]
        },
        staticBindings: [{
            targetBinding: {
                path: 'ShipmentLine.ShipmentKey',
                namespace: 'ShipmentLine_Output'
            },
            sourceBinding: {
                path: 'ShipmentLine.ShipmentKey',
                namespace: 'baggingLine'
            }
        }, {
            targetBinding: {
                path: 'ShipmentLine.ShipmentLineKey',
                namespace: 'ShipmentLineKey_Output'
            },
            sourceBinding: {
                path: 'ShipmentLine.ShipmentLineKey',
                namespace: 'baggingLine'
            }
        }, {
            targetBinding: {
                path: 'ShipmentLine.OrderLine.ItemDetails.ItemID',
                namespace: 'ShipmentLine_Output'
            },
            sourceBinding: {
                path: 'ShipmentLine.OrderLine.ItemDetails.ItemID',
                namespace: 'baggingLine'
            }
        }, {
            targetBinding: {
                path: 'Shipment.ShipmentKey',
                namespace: 'ShipmentKey_Output'
            },
            sourceBinding: {
                path: 'ShipmentLine.ShipmentKey',
                namespace: 'baggingLine'
            }
        }, {
            targetBinding: {
                path: 'ShipmentLine.OrderLine.ItemDetails.DisplayUnitOfMeasure',
                namespace: 'ShipmentLine_Output'
            },
            sourceBinding: {
                path: 'ShipmentLine.OrderLine.ItemDetails.DisplayUnitOfMeasure',
                namespace: 'BaggingLine'
            }
        }, {
            targetBinding: {
                path: 'ShipmentLine.OrderLine.ItemDetails.UOMDisplayFormat',
                namespace: 'ShipmentLine_Output'
            },
            sourceBinding: {
                path: 'ShipmentLine.OrderLine.ItemDetails.UOMDisplayFormat',
                namespace: 'BaggingLine'
            }
        }, {
            targetBinding: {
                path: 'ShipmentLine.OrderLine.ItemDetails.UnitOfMeasure',
                namespace: 'ShipmentLine_Output'
            },
            sourceBinding: {
                path: 'ShipmentLine.OrderLine.ItemDetails.UnitOfMeasure',
                namespace: 'baggingLine'
            }
        }, {
            targetBinding: {
                path: 'ShipmentLine.OrderLine.Item.ProductClass',
                namespace: 'ShipmentLine_Output'
            },
            sourceBinding: {
                path: 'ShipmentLine.OrderLine.Item.ProductClass',
                namespace: 'baggingLine'
            }
        }, {
            targetBinding: {
                path: 'ShipmentLine.OrderLine.ItemDetails.PrimaryInformation.ImageID',
                namespace: 'ShipmentLine_Output'
            },
            sourceBinding: {
                path: 'ShipmentLine.OrderLine.ItemDetails.PrimaryInformation.ImageID',
                namespace: 'baggingLine'
            }
        }, {
            targetBinding: {
                path: 'ShipmentLine.OrderLine.ItemDetails.PrimaryInformation.ImageLabel',
                namespace: 'ShipmentLine_Output'
            },
            sourceBinding: {
                path: 'ShipmentLine.OrderLine.ItemDetails.PrimaryInformation.ImageLabel',
                namespace: 'baggingLine'
            }
        }, {
            targetBinding: {
                path: 'ShipmentLine.OrderLine.ItemDetails.PrimaryInformation.ImageLocation',
                namespace: 'ShipmentLine_Output'
            },
            sourceBinding: {
                path: 'ShipmentLine.OrderLine.ItemDetails.PrimaryInformation.ImageLocation',
                namespace: 'baggingLine'
            }
        }, {
            targetBinding: {
                path: 'ShipmentLine.ShipmentLineKey',
                namespace: 'ShipmentLine_Output'
            },
            sourceBinding: {
                path: 'ShipmentLine.ShipmentLineKey',
                namespace: 'baggingLine'
            }
        }, {
            targetBinding: {
                path: 'ShipmentLine.ShipmentLineNo',
                namespace: 'ShipmentLine_Output'
            },
            sourceBinding: {
                path: 'ShipmentLine.ShipmentLineNo',
                namespace: 'baggingLine'
            }
        }, {
            targetBinding: {
                path: 'ShipmentLine.ShipmentSubLineNo',
                namespace: 'ShipmentLine_Output'
            },
            sourceBinding: {
                path: 'ShipmentLine.ShipmentSubLineNo',
                namespace: 'baggingLine'
            }
        }, {
            targetBinding: {
                path: 'ShipmentLine.Extn.ExtnBaggedQty',
                namespace: 'ShipmentLine_Output'
            },
            sourceBinding: {
                path: 'ShipmentLine.Extn.ExtnBaggedQty',
                namespace: 'baggingLine'
            }
        }, {
            targetBinding: {
                path: 'ShipmentLine.OrderHeaderKey',
                namespace: 'ShipmentLine_Output'
            },
            sourceBinding: {
                path: 'ShipmentLine.OrderHeaderKey',
                namespace: 'baggingLine'
            }
        }, {
            targetBinding: {
                path: 'ShipmentLine.OrderLineKey',
                namespace: 'ShipmentLine_Output'
            },
            sourceBinding: {
                path: 'ShipmentLine.OrderLineKey',
                namespace: 'baggingLine'
            }
        }, {
            targetBinding: {
                path: 'ShipmentLine.Quantity',
                namespace: 'ShipmentLineQty'
            },
            sourceBinding: {
                path: 'ShipmentLine.Quantity',
                namespace: 'baggingLine'
            }
        }, {
            targetBinding: {
                path: 'ShipmentLine.Quantity',
                namespace: 'ShipmentLine_Output'
            },
            sourceBinding: {
                path: 'ShipmentLine.Quantity',
                namespace: 'baggingLine'
            }
        }, {
            targetBinding: {
                path: 'ShipmentLine.ShortageQty',
                namespace: 'ShipmentLine_Output'
            },
            sourceBinding: {
                path: 'ShipmentLine.ShortageQty',
                namespace: 'baggingLine'
            }
        }, {
            targetBinding: {
                path: 'ShipmentLine.OrderLine.ItemDetails.AttributeList',
                namespace: 'ShipmentLine_Output'
            },
            sourceBinding: {
                path: 'ShipmentLine.OrderLine.ItemDetails.AttributeList',
                namespace: 'baggingLine'
            }
        }, {
            targetBinding: {
                path: 'ShipmentLine.OrderLine.ItemDetails.PrimaryInformation.ExtendedDisplayDescription',
                namespace: 'ShipmentLine_Output'
            },
            sourceBinding: {
                path: 'ShipmentLine.OrderLine.ItemDetails.PrimaryInformation.ExtendedDisplayDescription',
                namespace: 'baggingLine'
            }
        }, {
            targetBinding: {
                path: 'CommonCode.CallingOrganizationCode',
                namespace: 'Shortage_Output'
            },
            sourceBinding: {
                path: 'Shipment.EnterpriseCode',
                namespace: 'Shipment'
            }
        }],
        subscribers: {
            local: [{
                eventId: 'afterScreenInit',
                sequence: '30',
                description: 'This event is raised after screen has been initialized',
                handler: {
                    methodName: "initializeScreen"
                }
            }, {
                eventId: 'removeQtyLink_onClick',
                sequence: '30',
                description: 'This method reduces the picked quantity by 1',
                listeningControlUId: 'removeQtyLink',
                handler: {
                    methodName: "decreaseQuantity"
                }
            }, {
                eventId: 'addQtyLink_onClick',
                sequence: '30',
                description: 'This method increase the picked quantity by 1',
                listeningControlUId: 'addQtyLink',
                handler: {
                    methodName: "increaseQuantity"
                }
            }, {
                eventId: 'manualQtyInContainerTxtBox_onKeyUp',
                sequence: '30',
                description: 'This method is used to handle Key Up event of Scan Product text field.',
                handler: {
                    methodName: "editQuantityOnEnter"
                }
            }, {
                eventId: 'manualQtyInContainerTxtBox_onClick',
                sequence: '30',
                description: 'This method increase the picked quantity by 1',
                listeningControlUId: 'manualQtyInContainerTxtBox',
                handler: {
                    methodName: "increaseQuantity"
                }
            }, {
                eventId: 'handleQuantityChange',
                sequence: '30',
                description: 'This method is used to update shipment line panel with quantity updates',
                handler: {
                    methodName: "handleQuantityChange"
                }
            }, {
                eventId: 'shortageResolutionLink_onClick',
                sequence: '30',
                description: 'This method is used to open shortage resolution popup',
                listeningControlUId: 'shortageResolutionLink',
                handler: {
                    methodName: "openShortageResolutionPopup",
                    description: ""
                }
            }, {
                eventId: 'afterScreenLoad',
                sequence: '51',
                handler: {
                    methodName: "afterScreenLoad"
                }
            }]
        },
        initializeScreen: function(event, bEvent, ctrl, args) {
            var parentModel = _scScreenUtils.getModel(this.getOwnerScreen(), "baggingModel");
            _scScreenUtils.setModel(this, "Shipment", parentModel, null);
            var shipmentLineModel = null;
            var shipmentLineBaggedQuantity = null;
            var varItemWidget = null;
            shipmentLineModel = _scScreenUtils.getModel(this, "baggingLine");
            var quantityTextBoxModel = null;
            quantityTextBoxModel = {};
            shipmentLineBaggedQuantity = _scModelUtils.getStringValueFromPath("ShipmentLine.Extn.ExtnBaggedQty", shipmentLineModel);
            this.baggedQuantity = shipmentLineBaggedQuantity;
            _scModelUtils.setStringValueAtModelPath("Quantity", shipmentLineBaggedQuantity, quantityTextBoxModel);
            _scScreenUtils.setModel(this, "QuantityTextBoxModel_Input", quantityTextBoxModel, null);
            varItemWidget = _wscProductUtils.createItemVariationPanel(this, "itemVariationPanelUp", shipmentLineModel, "ShipmentLine.OrderLine.ItemDetails.AttributeList.Attribute");
            if (!(_scBaseUtils.isVoid(varItemWidget))) {
                _scWidgetUtils.placeAt(this, "itemVariationPanelHolder", "itemVariationPanelUp", null);
            }
            var shortageQty = 0;
            shortageQty = _scModelUtils.getNumberValueFromPath("ShipmentLine.ShortageQty", shipmentLineModel);
            if (shortageQty >= 1) {
                this.handleShipmentLineShortage(shipmentLineModel);
            } else {
                this.validatePickedQuantity(shipmentLineModel, "productScanImagePanel", "completelyPickedLabel", "productScanShortageImagePanel", "updateShortageResolutionImage", "shortageResolutionLink");
            }
            if (_scBaseUtils.equals(Number(shipmentLineBaggedQuantity), Number(shipmentLineModel.ShipmentLine.Quantity)) && _scBaseUtils.equals(Number(shipmentLineModel.ShipmentLine.Quantity), 0)) {
                _scWidgetUtils.showWidget(this, "shortageCompCont", false, "");
            } else {
                _scWidgetUtils.hideWidget(this, "shortageCompCont", false);
            }
            this.extn_populateGiftWrap();
        },
        extn_populateGiftWrap: function() {
            var shipmentLines = _scScreenUtils.getModel(this, "baggingLine").ShipmentLine;
            var instructionText = "NONE";
            var InstructionText1 = instructionText;
            var shipmentLineKey1 = _scModelUtils.getStringValueFromPath("ShipmentLineKey", shipmentLines);
            var giftFlag = _scModelUtils.getStringValueFromPath("OrderLine.GiftFlag", shipmentLines);
            var instruction = _scModelUtils.getStringValueFromPath("OrderLine.Instructions.Instruction", shipmentLines);
            if (_scBaseUtils.equals(giftFlag, "Y")) {
                for (j = 0; j < instruction.length; j++) {
                    var instructionType = _scModelUtils.getStringValueFromPath("InstructionType", instruction[j]);
                    if (_scBaseUtils.equals(instructionType, "WRAP_TYPE")) {
                        instructionText = _scModelUtils.getStringValueFromPath("InstructionText", instruction[j]);
                        break;
                    }
                }
            }
            if (_scBaseUtils.contains(instructionText, "-")) {
                var index = instructionText.indexOf("-");
                index++;
                InstructionText1 = instructionText.substring(index);
            } else {
                InstructionText1 = instructionText;
            }
            _scWidgetUtils.setValue(this, "extn_giftWrap", InstructionText1, false);
        },
        validatePickedQuantity: function(shipmentLineModel, statusImagePanel, statusLabel, shortageImagePanel, shortageResolutionIcon, shortageReasonFilterSelect) {
            var status = false;
            if (!_scBaseUtils.isVoid(shipmentLineModel.ShipmentLine.Extn.ExtnBaggedQty) && !_scBaseUtils.isVoid(shipmentLineModel.ShipmentLine.Quantity)) {
                // changes for handling line previously marked as shortage
                var shortageQty = _scBaseUtils.isVoid(shipmentLineModel.ShipmentLine.ShortageQty) ? Number("0") : Number(shipmentLineModel.ShipmentLine.ShortageQty);
                if (_scBaseUtils.numberEquals(Number(shipmentLineModel.ShipmentLine.Extn.ExtnBaggedQty), Number(shipmentLineModel.ShipmentLine.Quantity)) && _scBaseUtils.numberEquals(shortageQty, 0)) {
                    status = true;
                }
            }
            this.showHideWidgetsOnValidate(status, statusImagePanel, statusLabel, shortageImagePanel, shortageResolutionIcon, shortageReasonFilterSelect);
        },
        isShipmentLineMarkedShortage: function(shipmentLineModel) {
            var status = false,
                isBagComplete = false,
                isLineCompletelyShorted = false;
            if (!_scBaseUtils.isVoid(shipmentLineModel.ShipmentLine.ShortageQty) && (Number(shipmentLineModel.ShipmentLine.ShortageQty) > 0)) {
                status = true;
            }
            if (!_scBaseUtils.isVoid(shipmentLineModel.ShipmentLine.Extn.ExtnBaggedQty) && _scBaseUtils.equals(Number(shipmentLineModel.ShipmentLine.Extn.ExtnBaggedQty), Number(shipmentLineModel.ShipmentLine.Quantity))) {
                isBagComplete = true;
            }
            if (!_scBaseUtils.isVoid(shipmentLineModel.ShipmentLine.Quantity) && _scBaseUtils.numberEquals(Number(shipmentLineModel.ShipmentLine.Quantity), Number("0"))) {
                isLineCompletelyShorted = true;
            }
            if (status) {
                _scWidgetUtils.showWidget(this, "shortagelbl", false, "");
                _scWidgetUtils.showWidget(this, "shortageResolutionLink", false, "");
                if (isBagComplete) {
                    _scWidgetUtils.hideWidget(this, "shortageResolutionLink", false);
                }
                if (isLineCompletelyShorted) {
                    _scWidgetUtils.disableFields(this, null);
                    _scScreenUtils.addClass(this, "completeShort");
                    _scWidgetUtils.showWidget(this, "productShortedImagePanelCont", false, "");
                    _scWidgetUtils.changeImageAlt(this, "productShortedImagePanelCont", _scScreenUtils.getString(this, "Tooltip_title_completelyShort"));
                    _scWidgetUtils.changeImageTitle(this, "productShortedImagePanelCont", _scScreenUtils.getString(this, "Tooltip_title_completelyShort"));
                }
            }
            return status;
        },
        afterScreenLoad: function(event, bEvent, ctrl, args) {
            _scWidgetUtils.hideWidget(this, "productShortedImagePanelCont", false);
        },
        showHideWidgetsOnValidate: function(status, statusImagePanel, statusLabel, shortageImagePanel, shortageResolutionIcon, shortageReasonFilterSelect) {
            _scWidgetUtils.removeClass(this, "shipmentLineDetailsContainer", "completedLine");
            _scWidgetUtils.removeClass(this, "shipmentLineDetailsContainer", "shortedLine");
            if (status) {
                _scWidgetUtils.showWidget(this, statusImagePanel, false, "");
                _scWidgetUtils.setLabelText(this, statusLabel, _scScreenUtils.getString(this, "extn_bagged"), false);
                _scWidgetUtils.hideWidget(this, shortageImagePanel, false);
                _scWidgetUtils.hideWidget(this, shortageResolutionIcon, false);
                _scWidgetUtils.hideWidget(this, "productShortedImagePanelCont", false);
                _scWidgetUtils.hideWidget(this, shortageReasonFilterSelect, false);
                _scWidgetUtils.addClass(this, "shipmentLineDetailsContainer", "completedLine");
            } else {
                _scWidgetUtils.hideWidget(this, statusImagePanel, false);
                _scWidgetUtils.setLabelText(this, statusLabel, _scScreenUtils.getString(this, "Label_emptyLabel"), false);
                _scWidgetUtils.showWidget(this, shortageReasonFilterSelect, false, "");
                _scWidgetUtils.removeClass(this, "shipmentLineDetailsContainer", "completedLine");
                if (this.screenMode == "ItemScanMobile" && this.shortageResolved) {
                    _scWidgetUtils.showWidget(this, shortageImagePanel, false, "");
                    _scWidgetUtils.hideWidget(this, shortageResolutionIcon, false);
                    _scWidgetUtils.addClass(this, "shipmentLineDetailsContainer", "shortedLine");
                } else if (this.screenMode == "ItemScanMobile" && !this.shortageResolved) {
                    _scWidgetUtils.showWidget(this, shortageResolutionIcon, false, "");
                    _scWidgetUtils.hideWidget(this, shortageImagePanel, false);
                    _scWidgetUtils.removeClass(this, "shipmentLineDetailsContainer", "shortedLine");
                } else if (this.screenMode == "ItemScan") {
                    _scWidgetUtils.showWidget(this, shortageReasonFilterSelect, false, "");
                }
            }
            if (this.screenMode == "ReadOnlyMobile") {
                _scWidgetUtils.hideWidget(this, statusImagePanel, false);
                _scWidgetUtils.hideWidget(this, shortageImagePanel, false);
                _scWidgetUtils.hideWidget(this, shortageResolutionIcon, false);
                _scWidgetUtils.removeClass(this, "shipmentLineDetailsContainer", "completedLine");
                _scWidgetUtils.removeClass(this, "shipmentLineDetailsContainer", "shortedLine");
            } else if (this.screenMode == "ItemScan") {
                _scWidgetUtils.hideWidget(this, shortageImagePanel, false);
                _scWidgetUtils.hideWidget(this, shortageResolutionIcon, false);
                _scWidgetUtils.removeClass(this, "shipmentLineDetailsContainer", "shortedLine");
                if (this.shortageResolved) {
                    _scWidgetUtils.addClass(this, "shipmentLineDetailsContainer", "shortedLine");
                }
            }
        },
        handleShipmentLineShortage: function(shipmentLineModel) {
            if (this.isShipmentLineMarkedShortage(shipmentLineModel)) {
                var shortedShipmentLineModel = null;
                shortedShipmentLineModel = {};
                shortedShipmentLineModel = _scModelUtils.createModelObjectFromKey("ShipmentLine", shortedShipmentLineModel);
                _scModelUtils.setStringValueAtModelPath("ShipmentLine.ShortageQuantity", _scModelUtils.getStringValueFromPath("ShipmentLine.ShortageQty", shipmentLineModel), shortedShipmentLineModel);
                _scScreenUtils.setModel(this, "ShortedShipmentLine", shortedShipmentLineModel, null);
            }
        },
        extn_getColor: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
            var color = null;
            if (!_scBaseUtils.isVoid(shipmentLineModel)) {
                var attributeModel = _scModelUtils.getModelObjectFromPath("ShipmentLine.OrderLine.ItemDetails.AdditionalAttributeList.AdditionalAttribute", shipmentLineModel);
                if (!_scBaseUtils.isVoid(attributeModel)) {
                    for (var i = 0; i < attributeModel.length; i++) {
                        var refrenceObj = _scBaseUtils.getArrayItemByIndex(attributeModel, i);
                        var name = _scModelUtils.getStringValueFromPath("Name", refrenceObj);
                        if (_scBaseUtils.equals(name, "Color")) {
                            color = _scModelUtils.getStringValueFromPath("Value", refrenceObj);
                        }
                    }
                }
            }
            return color;
        },
        extn_getPrice: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
            var price = null;
            var country = null;
            if (!_scBaseUtils.isVoid(dataValue)) {
                var listPrice = null;
                listPrice = _scModelUtils.getStringValueFromPath("Extn.ExtnItemListPrice", dataValue);
                if (!_scBaseUtils.isVoid(listPrice)) {
                var currencyModel = _scModelUtils.getModelObjectFromPath("ShipmentLine.Shipment", shipmentLineModel);
                if (!_scBaseUtils.isVoid(currencyModel)) {
                    currencyModel = _scBaseUtils.getArrayItemByIndex(currencyModel, 0);
                        country = _scModelUtils.getStringValueFromPath("Currency", currencyModel);
                        if (!_scBaseUtils.isVoid(country)) {
                            price = dCurrency.format(parseFloat(listPrice), {
                                currency: country
                            });
                        }
                    }
                } else {
                    price = "";
                    return "";
                }
                var allPrice = {
                    "price": price
                };
                _scScreenUtils.setModel(this, "ListPrice", allPrice);
            }
            return price;
        },

        // extn_openProductDetails: function(event, bEvent, ctrl, args) {
        //      _scEventUtils.stopEvent(bEvent);
        //     var itemModel = _scScreenUtils.getTargetModel(this, "ItemDetails", null);
        //     var priceModel = _scScreenUtils.getModel(this, "ListPrice");
        //     if (priceModel && priceModel.price) {
        //         _scModelUtils.setStringValueAtModelPath("Price", priceModel.price, itemModel);
        //     } else {
        //         _scModelUtils.setStringValueAtModelPath("Price", "", itemModel);
        //     }
        //     _wscProductUtils.openItemDetails(this, itemModel);
        // },
        extn_getSize: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
            var size = null;
            if (!_scBaseUtils.isVoid(shipmentLineModel)) {
                var attributeModel = _scModelUtils.getModelObjectFromPath("ShipmentLine.OrderLine.ItemDetails.AdditionalAttributeList.AdditionalAttribute", shipmentLineModel);
                if (!_scBaseUtils.isVoid(attributeModel)) {
                    for (var i = 0; i < attributeModel.length; i++) {
                        var refrenceObj = _scBaseUtils.getArrayItemByIndex(attributeModel, i);
                        var name = _scModelUtils.getStringValueFromPath("Name", refrenceObj);
                        if (_scBaseUtils.equals(name, "Size")) {
                            size = _scModelUtils.getStringValueFromPath("Value", refrenceObj);
                        }
                    }
                }
            }
            return size;
        },
        extn_getUPC: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
            var aliasValue = null;
            if (!_scBaseUtils.isVoid(shipmentLineModel)) {
                var aliasModel = _scModelUtils.getModelObjectFromPath("ShipmentLine.OrderLine.ItemDetails.ItemAliasList.ItemAlias", shipmentLineModel);
                if (!_scBaseUtils.isVoid(aliasModel)) {
                    for (var i = 0; i < aliasModel.length; i++) {
                        var refrenceObj = _scBaseUtils.getArrayItemByIndex(aliasModel, i);
                        var aliasName = _scModelUtils.getStringValueFromPath("AliasName", refrenceObj);
                        if (_scBaseUtils.equals(aliasName, "ACTIVE_UPC")) {
                            aliasValue = _scModelUtils.getStringValueFromPath("AliasValue", refrenceObj);
                        }
                    }
                }
            }
            return aliasValue;
        },
         extn_getQty: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
            var itemID = null;
            var unitOfMeasure = null;
            var itemModel = null;
            if (!_scBaseUtils.isVoid(shipmentLineModel)) {
                itemModel = _scModelUtils.getModelObjectFromPath("ShipmentLine", shipmentLineModel);
                if (!_scBaseUtils.isVoid(itemModel)) {
                    itemID = _scModelUtils.getStringValueFromPath("ItemID", itemModel);
                    unitOfMeasure = _scModelUtils.getStringValueFromPath("UnitOfMeasure", itemModel);
                    productClass = _scModelUtils.getStringValueFromPath("ProductClass", itemModel);
                }
                var shipNode = null;
                shipNode = _iasContextUtils.getFromContext("CurrentStore");
                var organization = null;
                var shipmentModel= _scModelUtils.getStringValueFromPath("ShipmentLine.Shipment", shipmentLineModel); 
                  if(!_scBaseUtils.isVoid(shipmentModel)){
                    organization = _scModelUtils.getStringValueFromPath("EnterpriseCode", shipmentModel[0]);
                  }               
                var model = null;
                model = _scBaseUtils.getNewModelInstance();
                if(!_scBaseUtils.isVoid(organization)){
                     _scModelUtils.setStringValueAtModelPath("InventorySupply.OrganizationCode", organization, model);
                }
                _scModelUtils.setStringValueAtModelPath("InventorySupply.ShipNode", shipNode, model);
                _scModelUtils.setStringValueAtModelPath("InventorySupply.ItemID", itemID, model);
                _scModelUtils.setStringValueAtModelPath("InventorySupply.UnitOfMeasure", unitOfMeasure, model);
                _scModelUtils.setStringValueAtModelPath("InventorySupply.ProductClass", productClass, model);
                _scModelUtils.setStringValueAtModelPath("InventorySupply.SupplyType", "ONHAND", model);
                _iasUIUtils.callApi(this, model, "extn_getQtyOnHand_ref", null);
            }
        },
        handleMashupOutput: function(mashupRefId, modelOutput, mashupInput, mashupContext, applySetModel) {
                if (_scBaseUtils.equals(mashupRefId, "extn_getQtyOnHand_ref")) {
                if (!_scBaseUtils.isVoid(modelOutput)) {
                    var itemModel = null;
                    itemModel = _scModelUtils.getModelObjectFromPath("Item.Supplies.InventorySupply", modelOutput);
                    var itemID = null;
                    if (!_scBaseUtils.isVoid(itemModel)) {
                        var availableQuantity = null;
                        availableQuantity = _scModelUtils.getStringValueFromPath("Quantity", itemModel);
                        if (!_scBaseUtils.isVoid(availableQuantity)) {
                            _scWidgetUtils.setValue(this, "extn_getQty", parseInt(availableQuantity));
                        }
                    }
                }
            }
        },
        handleMashupCompletion: function(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data) {
            _iasBaseTemplateUtils.handleMashupCompletion(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data, this);
        },
        openShortageResolutionPopup: function(event, bEvent, ctrl, args) {
            var screenInputModel = null;
            var baggedQuantity = null;
            var shortageQuantity = null;
            var zero = 0;
            var codeType = null;
            screenInputModel = _scScreenUtils.getTargetModel(this, "Shortage_Output", null);
            codeType = "YCD_PACK_SHORT_RESOL";
            _scModelUtils.setStringValueAtModelPath("CommonCode.CodeType", codeType, screenInputModel);
            var shipmentLineModel = null;
            shipmentLineModel = _scScreenUtils.getModel(this, "baggingLine");
            baggedQuantity = _scModelUtils.getNumberValueFromPath("ShipmentLine.Extn.ExtnBaggedQty", shipmentLineModel);
            if (!(_iasUIUtils.isValueNumber(baggedQuantity))) {
                baggedQuantity = zero;
            }
            _scModelUtils.setNumberValueAtModelPath("ShipmentLine.DisplayQty", baggedQuantity, shipmentLineModel);
            _scModelUtils.setStringValueAtModelPath("ShipmentLine.DisplayShortQty", _wscShipmentUtils.subtract(_scModelUtils.getNumberValueFromPath("ShipmentLine.Quantity", shipmentLineModel), baggedQuantity), shipmentLineModel);
            var bindings = null;
            bindings = {};
            var screenConstructorParams = null;
            screenConstructorParams = {};
            _scModelUtils.addStringValueToModelObject("flowName", this.flowName, screenConstructorParams);
            bindings["ShipmentLine"] = shipmentLineModel;
            var popupParams = null;
            popupParams = {};
            popupParams["screenInput"] = screenInputModel;
            popupParams["outputNamespace"] = "ShortedShipmentLineModel";
            popupParams["binding"] = bindings;
            popupParams["screenConstructorParams"] = screenConstructorParams;
            var dialogParams = null;
            dialogParams = {};
            dialogParams["closeCallBackHandler"] = "onShortageReasonSelection";
            dialogParams["class"] = "popupTitleBorder fixedActionBarDialog";
            _iasUIUtils.openSimplePopup("wsc.components.shipment.common.screens.ShortageReasonPopup", "Title_ShortageReason", this, popupParams, dialogParams);
        },
        onShortageReasonSelection: function(actionPerformed, model, popupParams) {
            if (!(_scBaseUtils.equals(actionPerformed, "CLOSE"))) {
                var shortedShipmentLineModel = null;
                shortedShipmentLineModel = _scScreenUtils.getModel(this, "ShortedShipmentLineModel");
                this.handleShortageResolution(shortedShipmentLineModel);
            }
        },
        handleShortageResolution: function(shortedShipmentLineModel) {
            var argBean = null;
            argBean = {};
            argBean["ShortedShipmentLine"] = shortedShipmentLineModel;
            this.callMultiApiShortageUpdate(argBean);
        },
        callMultiApiShortageUpdate: function(args) {
            var shortedShipmentLineModel = null;
            var shipmentLinePickedModel = null;
            shipmentLinePickedModel = _scScreenUtils.getTargetModel(this, "ShipmentLine_Output", null);
            shortedShipmentLineModel = _scBaseUtils.getModelValueFromBean("ShortedShipmentLine", args);
            var updateShortageModel = null;
            updateShortageModel = this.getShortageChangeShipmentModel(shipmentLinePickedModel, args);
            if (_scBaseUtils.equals("Y", _scModelUtils.getStringValueFromPath("ShipmentLine.MarkAllShortLineWithShortage", shortedShipmentLineModel))) {
                var shipmentModel = null;
                shipmentModel = {};
                shipmentModel = _scModelUtils.createModelObjectFromKey("Shipment", shipmentModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.Action", "MarkAllLinesShortage", shipmentModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", _scModelUtils.getStringValueFromPath("Shipment.ShipmentKey", updateShortageModel), shipmentModel);
                updateShortageModel = shipmentModel;
            } else {
                _scModelUtils.setStringValueAtModelPath("Shipment.Action", "MarkLineAsShortage", updateShortageModel);
            }
            _scModelUtils.setStringValueAtModelPath("Shipment.ShortageReasonCode", _scModelUtils.getStringValueFromPath("ShipmentLine.ShortageReasonCode", shortedShipmentLineModel), updateShortageModel);
            var eventArgs = null;
            var eventDefn = null;
            eventDefn = {};
            eventArgs = {};
            _scBaseUtils.setAttributeValue("inputData", updateShortageModel, eventArgs);
            _scBaseUtils.setAttributeValue("argumentList", eventArgs, eventDefn);
            _scEventUtils.fireEventToParent(this, "updateShipmentLineDetails", eventDefn);
        },
        getShortageChangeShipmentModel: function(baggedShipmentLineModel, args) {
            var baggedModel = {};
            baggedModel.Shipment = {};
            baggedModel.Shipment.ShipmentKey = baggedShipmentLineModel.ShipmentLine.ShipmentKey;
            baggedModel.Shipment.ShipmentLines = {};
            baggedModel.Shipment.ShipmentLines.ShipmentLine = [];
            var baggedLine = {};
            baggedLine.Extn = {};
            baggedLine.ShipmentLineKey = baggedShipmentLineModel.ShipmentLine.ShipmentLineKey;
            var baggedQty = !_scBaseUtils.isVoid(baggedShipmentLineModel.ShipmentLine.Extn && baggedShipmentLineModel.ShipmentLine.Extn.ExtnBaggedQty) ? Number(baggedShipmentLineModel.ShipmentLine.Extn.ExtnBaggedQty) : Number('0');
            var shortageQty = !_scBaseUtils.isVoid(baggedShipmentLineModel.ShipmentLine.ShortageQty) ? Number(baggedShipmentLineModel.ShipmentLine.ShortageQty) : Number('0');
            baggedLine.Extn.ExtnBaggedQty = baggedQty;
            if (!_scBaseUtils.isVoid(args) && _wscBackroomPickUpUtils.isLineMarkedWithInventoryShortage(args.ShortedShipmentLine)) {
                baggedLine.Quantity = baggedQty;
            } else {
                baggedLine.Quantity = baggedShipmentLineModel.ShipmentLine.Quantity;
            }
            baggedLine.ShortageQty = (Number(baggedShipmentLineModel.ShipmentLine.Quantity) - baggedQty) ;
            baggedModel.Shipment.ShipmentLines.ShipmentLine[0] = baggedLine;
            /*if(args.isLastShortageLineForCancellingShipment=="true" || args.isLastShortageLineForCancellingShipment==true){
                pickedModel.Shipment.Action="Cancel";
            }*/
            return baggedModel;
        },
        increaseQuantity: function(event, bEvent, ctrl, args) {
            this.validateAndUpdateBaggedQuantity("increase", bEvent);
        },
        decreaseQuantity: function(event, bEvent, ctrl, args) {
            this.validateAndUpdateBaggedQuantity("decrease", bEvent);
        },
        editQuantityOnEnter: function(event, bEvent, ctrl, args) {
            if (_iasEventUtils.isEnterPressed(event)) {
                this.validateAndUpdateBaggedQuantity("update", bEvent);
            } else {
                if (_scBaseUtils.isBooleanTrue(this.validateAndUpdateBaggedQuantity("validate", bEvent)) && _wscShipmentUtils.IsQuantityValueChanged(this, "QuantityTextBoxModel_Input", "Quantity", "QuantityTextBoxModel_Output", "Quantity")) {
                    if (!(_scWidgetUtils.isWidgetVisible(this, "updateQtyButton"))) {
                        _scWidgetUtils.showWidget(this, "updateQtyButton", false, "");
                    }
                } else {
                    _scWidgetUtils.hideWidget(this, "updateQtyButton", false);
                }
            }
        },
        validateAndUpdateBaggedQuantity: function(action, bEvent) {
            var quantityTxtBoxModel = null;
            var currentQty = null;
            var maxQty = null;
            var zero = 0;
            var isQtyInRange = false;
            var originalShipmentLineQtyModel = null;
            currentQty = _scModelUtils.getNumberValueFromPath("Quantity", _scScreenUtils.getTargetModel(this, "QuantityTextBoxModel_Output", null));
            maxQty = _scModelUtils.getNumberValueFromPath("ShipmentLine.Quantity", _scScreenUtils.getTargetModel(this, "ShipmentLineQty", null));
            var validationConfigBean = null;
            validationConfigBean = {};
            validationConfigBean["currentQty"] = currentQty;
            validationConfigBean["maxQty"] = maxQty;
            validationConfigBean["minQty"] = zero;
            validationConfigBean["oldQty"] = this.baggedQuantity;
            validationConfigBean["minErrorMsg"] = "Message_Bagged_Quantity_must_be_greater_than_zero";
            validationConfigBean["maxErrorMsg"] = "ItemOverrage_Bagged";
            validationConfigBean["lastUpdatedRepPanelPropertyName"] = "LastScannedShipmentLineScreenUId";
            validationConfigBean["messagePanelId"] = "errorMsgPnl";
            isQtyInRange = _iasScreenUtils.ifQtyIsInRangeNew(this, this, action, "manualQtyInContainerTxtBox", validationConfigBean, true);
            if (isQtyInRange) {
                if (!(_scBaseUtils.equals(action, "validate"))) {
                    this.fireEventToUpdateShipmentLineQuantity("editQty", _scWidgetUtils.getValue(this, "manualQtyInContainerTxtBox"), bEvent);
                }
            }
            return isQtyInRange;
        },
        fireEventToUpdateShipmentLineQuantity: function(lineAction, shipmentLineQuantity, bEvent) {
            var quantityTextBoxModel = null;
            var shipmentLineModel = null;
            var action = null;
            var zero = 0;
            quantityTextBoxModel = _scScreenUtils.getTargetModel(this, "ShipmentLineKey_Output", null);
            shipmentLineModel = _scScreenUtils.getModel(this, "baggingLine");
            if (_scBaseUtils.equals(this.flowName, "Bagging")) {
                if (!(_scBaseUtils.equals(lineAction, "fullQty"))) {
                    if (_scBaseUtils.equals(bEvent._originatingControlUId, "addQtyLink")) {
                        Action = "ADD";
                    } else if (_scBaseUtils.equals(bEvent._originatingControlUId, "removeQtyLink")) {
                        Action = "SUB";
                    } else {
                        Action = "";
                    }
                    _scModelUtils.setNumberValueAtModelPath("ShipmentLine.Extn.ExtnBaggedQty", shipmentLineQuantity, quantityTextBoxModel);
                } else {
                    Action = "BagAllLine";
                }
            }
            var shipmentModel = null;
            shipmentModel = _scScreenUtils.getTargetModel(this, "ShipmentKey_Output", null);
            _scModelUtils.getOrCreateChildModelObject("Shipment.ShipmentLines", shipmentModel);
            shipmentModel = _iasScreenUtils.appendChildModelAsArrayToParentModelPath(shipmentModel, "Shipment.ShipmentLines", quantityTextBoxModel, zero);
            _scModelUtils.setStringValueAtModelPath("Shipment.Action", Action, shipmentModel);
            var eventArgs = null;
            var eventDefn = null;
            eventDefn = {};
            eventArgs = {};
            _scBaseUtils.setAttributeValue("inputData", shipmentModel, eventArgs);
            _scBaseUtils.setAttributeValue("argumentList", eventArgs, eventDefn);
            _scEventUtils.fireEventToParent(this, "updateShipmentLineDetails", eventDefn);
        },
        getRemainingFormattedQuantity: function(dataValue, screen, widget, namespace, modelObj, options) {
            dataValue = this.getFormattedRemainingQuantity(this, modelObj);
            return dataValue;
        },
        getFormattedRemainingQuantity: function(screen, shipmentLineModel) {
            var remainingQtyWithUOM = "";
            var uomDisplayFormat = "";
            var baggedQty = 0;
            if (!_scBaseUtils.isVoid(_scModelUtils.getStringValueFromPath("ShipmentLine.Extn.ExtnBaggedQty", shipmentLineModel))) {
                baggedQty = _scModelUtils.getNumberValueFromPath("ShipmentLine.Extn.ExtnBaggedQty", shipmentLineModel)
            }
            var uom = _scModelUtils.getStringValueFromPath("ShipmentLine.OrderLine.ItemDetails.DisplayUnitOfMeasure", shipmentLineModel);
            uomDisplayFormat = _scModelUtils.getStringValueFromPath("ShipmentLine.OrderLine.ItemDetails.UOMDisplayFormat", shipmentLineModel);
            var remainingQty = _scModelUtils.getNumberValueFromPath("ShipmentLine.Quantity", shipmentLineModel) - baggedQty;
            remainingQtyWithUOM = _iasUOMUtils.getFormatedQuantityWithUom(remainingQty, uom, uomDisplayFormat);
            return remainingQtyWithUOM;
        }
    });
});
