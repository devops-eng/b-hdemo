scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/text", "scbase/loader!extn/customScreen/bagging/BaggingLineDetails", "scbase/loader!sc/plat/dojo/controller/ServerDataController"], function(_dojodeclare, _dojokernel, _dojotext, _extnBaggingLineDetails, _scServerDataController) {
    return _dojodeclare("extn.customScreen.bagging.BaggingLineDetailsBehaviorController", [_scServerDataController], {
        screenId: 'extn.customScreen.bagging.BaggingLineDetails',
        mashupRefs: [
        {
		 extnType : 			'ADD'
,
		 mashupId : 			'extn_getQtyOnHand'
,
		 mashupRefId : 			'extn_getQtyOnHand_ref'

	}]
    });
});
