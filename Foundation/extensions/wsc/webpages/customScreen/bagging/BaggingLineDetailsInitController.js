scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/text", "scbase/loader!extn/customScreen/bagging/BaggingLineDetails", "scbase/loader!sc/plat/dojo/controller/ScreenController"], function(_dojodeclare, _dojokernel, _dojotext, _extnBaggingLineDetails, _scScreenController) {
    return _dojodeclare("extn.customScreen.bagging.BaggingLineDetailsInitController", [_scScreenController], {
        screenId: 'extn.customScreen.bagging.BaggingLineDetails',
        mashupRefs: []
    });
});
