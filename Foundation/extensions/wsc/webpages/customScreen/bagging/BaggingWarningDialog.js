scDefine(["dojo/text!./templates/BaggingWarningDialog.html", "scbase/loader!dijit/form/Button", "scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/_base/lang", "scbase/loader!dojo/text", "scbase/loader!ias/widgets/IASBaseScreen", "scbase/loader!idx/form/TextBox", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat", "scbase/loader!sc/plat/dojo/binding/ButtonDataBinder", "scbase/loader!sc/plat/dojo/binding/SimpleDataBinder", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!ias/utils/EventUtils", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!sc/plat/dojo/utils/ResourcePermissionUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EditorUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/info/ApplicationInfo", "scbase/loader!wsc/components/shipment/common/utils/ShipmentUtils", "scbase/loader!sc/plat/dojo/Userprefs"], function(templateText, _dijitButton, _dojodeclare, _dojokernel, _dojolang, _dojotext, _iasIASBaseScreen, _idxTextBox, _idxContentPane, _scplat, _scButtonDataBinder, _scSimpleDataBinder, _scBaseUtils, _iasBaseTemplateUtils, _iasUIUtils, _iasEventUtils, _iasContextUtils, _scResourcePermissionUtils, _scScreenUtils, _scModelUtils, _scWidgetUtils, _scEditorUtils, _scEventUtils, _scApplicationInfo, _wscShipmentUtils, _scUserprefs) {
    return _dojodeclare("extn.customScreen.bagging.BaggingWarningDialog", [_iasIASBaseScreen], {
        templateString: templateText,
        uId: "BaggingWarningDialog",
        packageName: "extn.customScreen.bagging",
        className: "BaggingWarningDialog",
        title: "Screen",
        screen_description: "This is screen is used to show bagging warning",
        isDirtyCheckRequired: true,
        namespaces: {
            targetBindingNamespaces: [{
                value: 'popupData',
                description: "target namespace which initialize the preview."
            }],
            sourceBindingNamespaces: [{
                value: 'Shipment',
                description: "Source namespace"
            }, {
                value: 'popupOutput',
                description: "Stores popup output set from setPopupOutput"
            }]
        },
        staticBindings: [],
        hotKeys: [{
            id: "Popup_cancelButton",
            key: "ESCAPE",
            description: "$(_scSimpleBundle:Close)",
            widgetId: "Popup_cancelButton",
            invocationContext: "",
            category: "$(_scSimpleBundle:General)",
            helpContextId: ""
        }, {
            id: "Popup_btnCancel",
            key: "ESCAPE",
            description: "$(_scSimpleBundle:Close)",
            widgetId: "Popup_btnCancel",
            invocationContext: "",
            category: "$(_scSimpleBundle:General)",
            helpContextId: ""
        }],
        events: [],
        subscribers: {
            local: [{
                eventId: 'afterScreenInit',
                sequence: '30',
                handler: {
                    methodName: "initializeScreen"
                }
            }, {
                eventId: 'afterScreenInit',
                sequence: '25',
                description: 'Subscriber for after Screen Init event',
                handler: {
                    methodName: "initScreen"
                }
            }, {
                eventId: 'Popup_startOver_onClick',
                sequence: '25',
                description: 'Start Over Button Action',
                listeningControlUId: 'Popup_startOver',
                handler: {
                    methodName: "onStartOverSelectAction",
                    description: ""
                }
            }, {
                eventId: 'Popup_continue_onClick',
                sequence: '25',
                description: 'Continue Pick Button Action',
                listeningControlUId: 'Popup_continue',
                handler: {
                    methodName: "onContinueSelectAction",
                    description: ""
                }
            }, {
                eventId: 'Popup_cancelButton_onClick',
                sequence: '25',
                description: '',
                listeningControlUId: 'Popup_cancelButton',
                handler: {
                    methodName: "onPopUpCancelAction",
                    description: ""
                }
            }]
        },
        initializeScreen: function(event, bEvent, ctrl, args) {
            var warningString = null;
            var shipmentModel = null;
            var context = null;
            context = _scBaseUtils.getAttributeValue("screen.params.binding.Action", false, args);
            shipmentModel = _scScreenUtils.getModel(this, "Shipment");
            var assignedUser = null;
            assignedUser = _scModelUtils.getStringValueFromPath("Shipment.AssignedToUserId", shipmentModel);
            warningString = "The Bagging for this order is already started by " + assignedUser + " <br/><br/>Select Start Over to start bagging afresh. <br/>Select Continue to carry on bagging from where " + assignedUser + " has left.";
            _scWidgetUtils.setValue(this, "confirmationMessage", warningString, false);
        },
        onContinueSelectAction: function(event, bEvent, ctrl, args) {
            var popupData = null;
            popupData = _scBaseUtils.getTargetModel(this, "popupData", null);
            _scModelUtils.setStringValueAtModelPath("actionPerformed", "CONTINUE", popupData);
            this.onPopupConfirm(popupData);
        },
        onStartOverSelectAction: function(event, bEvent, ctrl, args) {
            var popupData = null;
            popupData = _scBaseUtils.getTargetModel(this, "popupData", null);
            _scModelUtils.setStringValueAtModelPath("actionPerformed", "STARTOVER", popupData);
            this.onPopupConfirm(popupData);
        },
        onPopUpCancelAction: function(event, bEvent, ctrl, args) {
            var popupData = null;
            popupData = _scBaseUtils.getTargetModel(this, "popupData", null);
            _scModelUtils.setStringValueAtModelPath("actionPerformed", "CLOSEPOPUP", popupData);
            this.onPopupConfirm(popupData);
        },
        getPopupOutput: function() {
            var popupData = null;
            popupData = _scBaseUtils.getTargetModel(this, "popupData", null);
            return popupData;
        },
        onClickOfYes: function(event, bEvent, ctrl, args) {
            _scWidgetUtils.closePopup(this, "CLOSE", false);
        },
        onPopupConfirm: function(popupData) {
            var actionPerformed = null;
            actionPerformed = _scModelUtils.getStringValueFromPath("actionPerformed", popupData);
            var parentScreen = null;
            if (_scBaseUtils.equals(actionPerformed, "STARTOVER")) {
                _scWidgetUtils.closePopup(this, "STARTOVER", false);
            } else if (_scBaseUtils.equals(actionPerformed, "CONTINUE")) {
                _scWidgetUtils.closePopup(this, "CONTINUE", false);
            } else if (_scBaseUtils.equals(actionPerformed, "CLOSEPOPUP")) {
                _scWidgetUtils.closePopup(this, "CLOSEPOPUP", false);
            }
        }
    });
});
