scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel",
 "scbase/loader!dojo/text", "scbase/loader!sc/plat/dojo/controller/ServerDataController",
  "scbase/loader!extn/customScreen/bagging/BaggingWarningDialog"], function(_dojodeclare, _dojokernel, _dojotext, _scServerDataController, _wscPackProductScan) {
    return _dojodeclare("extn.customScreen.bagging.BaggingWarningDialogBehaviorController", [_scServerDataController], {
        screenId: 'extn.customScreen.bagging.BaggingWarningDialog',
        mashupRefs: [
        // {
        //     mashupRefId: 'validateBatchPickCaseNumber_ref',
        //     mashupId: 'validateBatchPickCaseNumber'
        // },
        // {
        //     mashupRefId: 'validateBackRoomPickCaseNumber_ref',
        //     mashupId: 'validateBackRoomPickCaseNumber'
        // },
        // {
        //     mashupRefId: 'cancelBatchPick_ref',
        //     mashupId: 'cancelBatchPick'
        // },{
        //     mashupRefId: 'CancelBackRoomPick_ref',
        //     mashupId: 'cancelBackRoomPick'
        // }
        ]
    });
});
