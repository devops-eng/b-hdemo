scDefine(["dojo/text!./templates/ReminderPopup.html", "scbase/loader!dijit/form/Button", "scbase/loader!dojo/_base/declare", "scbase/loader!idx/form/TextBox", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/widgets/Label", "scbase/loader!sc/plat/dojo/widgets/Link", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/GridxUtils", "scbase/loader!gridx/Grid", "scbase/loader!sc/plat/dojo/binding/GridxDataBinder", "scbase/loader!ias/utils/RepeatingScreenUtils", "scbase/loader!sc/plat/dojo/utils/RepeatingPanelUtils", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!sc/plat/dojo/utils/EditorUtils", "scbase/loader!sc/plat/dojo/info/ApplicationInfo"], function(templateText, _dijitButton, _dojodeclare, _idxTextBox, _idxContentPane, _scBaseUtils, _scWidgetUtils, _scEventUtils, _scLabel, _scLink, _scScreen, _scModelUtils, _iasUIUtils, _scScreenUtils, _iasBaseTemplateUtils, _scWidgetUtils, _scEventUtils, _scGridxUtils, _gridxGrid, _scGridxDataBinder, _iasRepeatingScreenUtils, _scRepeatingPanelUtils, _scUserprefs, _iasContextUtils, _extnExpicientUtils, _scEditorUtils, scApplicationInfo) {
    return _dojodeclare("extn.customScreen.reminderpopup.ReminderPopup", [_scScreen], {
        templateString: templateText,
        uId: "ReminderPopup",
        packageName: "extn.customScreen.reminderpopup",
        className: "ReminderPopup",
        subscribers: {
            local: [{
                eventId: 'confirmButton_onClick',
                sequence: '25',
                description: '',
                listeningControlUId: 'confirmButton',
                handler: {
                    methodName: "confirmPopup",
                    description: ""
                }
            }, {
                eventId: 'cancelButton_onClick',
                sequence: '25',
                description: '',
                listeningControlUId: 'cancelButton',
                handler: {
                    methodName: "handleCancel",
                    description: ""
                }
            }]
        },
        handleCancel: function(event, bEvent, ctrl, args) {
            _scWidgetUtils.closePopup(this, "CLOSE", false);
        },
        confirmPopup: function(event, bEvent, ctrl, args) {
           _scWidgetUtils.closePopup(this, "CONFIRM", false);
        }
    });
});
