scDefine(["dojo/text!./templates/restockingEditor.html", 
"scbase/loader!dojo/_base/declare", 
"scbase/loader!idx/layout/ContentPane", 
"scbase/loader!sc/plat", 
 "scbase/loader!sc/plat/dojo/binding/CurrencyDataBinder", 
 "scbase/loader!sc/plat/dojo/binding/ImageDataBinder", 
 "scbase/loader!sc/plat/dojo/layout/AdvancedTableLayout", 
 "scbase/loader!sc/plat/dojo/widgets/Editor",
 "scbase/loader!sc/plat/dojo/widgets/Image", 
 "scbase/loader!sc/plat/dojo/widgets/Label", 
 "scbase/loader!sc/plat/dojo/widgets/Link",
 "scbase/loader!ias/utils/BaseTemplateUtils"
 ], function(
templateText, _dojodeclare,  _idxContentPane, _scplat, _scCurrencyDataBinder, _scImageDataBinder, _scAdvancedTableLayout,  
 _scEditor,  _scImage, _scLabel, _scLink,
 _isccsBaseTemplateUtils
 ) {
    return _dojodeclare("extn.customScreen.restocking.editors.restockingEditor", [_scEditor], {
        templateString: templateText,
        uId: "restockingEditor",
        packageName: "extn.customScreen.restocking.editors",
        className: "restockingEditor",
        title: "extn_user_details",
        namespaces: {
            targetBindingNamespaces: [],
            sourceBindingNamespaces: [{
                description: 'Opens Restocking Screen',
                value: 'InitialEditorInput'
            }]
        },

// comparisonAttributes are used to create a model object (JSON Object), which is used for 
// comparing two different instances for the editor. If the model object that is created by using 
// comparisonAttributes for two instances of the editor are similar, they are identified as the 
// matching editor. This is mainly used to determine whether the new instance of the editor 
// must be opened in a new tab or focused on the matching editor. 
// This is used when using the sc.plat.dojo.utils.ControllerUtils.openScreenInEditor utility 
	    comparisonAttributes: ["Order.OrderHeaderKey"],    
        hotKeys: [],
        events: [ {
            name: 'setEditorInput'
        },{
            name: 'setScreenTitle'
        }],
        subscribers: {
            local: [ {
                eventId: 'setEditorInput',
                sequence: '25',
                handler: {
                    methodName: "setEditorInput"
                }
            },{
                eventId: 'setScreenTitle',
                sequence: '25',
                handler: {
                    methodName: "setScreenTitle"
                }
            }],
        },
      
        getScreenTitle: function() {

        },

        showOrHideRelTask: function(
        event, bEvent, ctrl, args) {
            var isWizardinitialized = null;
            var isRTscreeninitialized = null;
            isWizardinitialized = _scBaseUtils.getAttributeValue("isWizardinitialized", false, args);
            isRTscreeninitialized = _scBaseUtils.getAttributeValue("isRTscreeninitialized", false, args);
            if (!(
            _scBaseUtils.isVoid(
            isRTscreeninitialized))) {
                this.isRTscreeninitialized = isRTscreeninitialized;
            }
            if (!(
            _scBaseUtils.isVoid(
            isWizardinitialized))) {
                this.isWizardinitialized = isWizardinitialized;
            }
            if (!(
            _isccsWidgetUtils.isWizardInstance(
            _scEditorUtils.getScreenInstance(
            this)))) {
                _isccsBaseTemplateUtils.hideRelatedTask(
                this);
            } else {
                if (
                _scBaseUtils.and(
                _scBaseUtils.equals(
                this.isWizardinitialized, "true"), _scBaseUtils.equals(
                this.isRTscreeninitialized, "true"))) {
                    _isccsBaseTemplateUtils.hideRelatedTask(
                    this);
                }
            }
        },

        updateScreenWithEditorInput: function(
			model) {

        },
        onScreenInit: function(
			event, bEvent, ctrl, args) {

        },
        setInitialEditorInput: function(
			screenInput) {

        },
        setScreenTitle: function(
			event, bEvent, ctrl, args) {
            _isccsBaseTemplateUtils.updateScreenTitleOnEditor(
            this, bEvent);
        },
        setEditorInput: function(
			event, bEvent, ctrl, args) {
		}
    });
});
