scDefine([
	"scbase/loader!dojo/_base/declare",
	"scbase/loader!dojo/_base/kernel",
	"scbase/loader!dojo/text",
	"scbase/loader!extn/customScreen/restocking/editors/restockingEditor",
	"scbase/loader!sc/plat/dojo/controller/ServerDataController"
], function(
	_dojodeclare, _dojokernel, _dojotext, _extnManageUserEditor, _scServerDataController
) {
return _dojodeclare("extn.customScreen.restocking.editors.restockingEditorBehaviorController", [_scServerDataController], {
	screenId: 'extn.customScreen.restocking.editors.restockingEditor',
	mashupRefs: []

});
});
