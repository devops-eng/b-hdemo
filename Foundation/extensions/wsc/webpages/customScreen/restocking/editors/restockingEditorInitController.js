scDefine([
	"scbase/loader!dojo/_base/declare",
	"scbase/loader!dojo/_base/kernel",
	"scbase/loader!dojo/text",
	"scbase/loader!extn/customScreen/restocking/editors/restockingEditor",
	"scbase/loader!sc/plat/dojo/controller/ScreenController"
], function(
	_dojodeclare, _dojokernel, _dojotext, _extnrestockingEditor, _scScreenController
) {
return _dojodeclare("extn.customScreen.restocking.editors.restockingEditorInitController", [_scScreenController], {
	screenId: 'extn.customScreen.restocking.editors.restockingEditor',
	mashupRefs: []

});
});
