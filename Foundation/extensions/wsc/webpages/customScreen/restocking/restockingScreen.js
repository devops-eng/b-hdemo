scDefine(["dojo/text!./templates/restockingScreen.html", "scbase/loader!dojo/_base/declare", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!ias/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/RepeatingPanelUtils", "scbase/loader!sc/plat/dojo/info/ApplicationInfo", "scbase/loader!ias/utils/EventUtils","scbase/loader!sc/plat/dojo/utils/EditorUtils"], function(templateText, _dojodeclare, _scScreen, _scScreenUtils, _scModelUtils, _iasUIUtils, _extnExpicientUtils, _scBaseUtils, _scEventUtils, _iasBaseTemplateUtils, _iasScreenUtils, _scWidgetUtils, _scRepeatingPanelUtils, scApplicationInfo, _iasEventUtils,_scEditorUtils) {
    return _dojodeclare("extn.customScreen.restocking.restockingScreen", [_scScreen], {
        templateString: templateText,
        uId: "restockingScreen",
        packageName: "extn.customScreen.restocking",
        className: "restockingScreen",
        namespaces: {
            sourceBindingNamespaces: [{
                value: 'parentModel',
                description: "This model has everything"
            }]
        },
        subscribers: {
            local: [{
                eventId: 'afterScreenInit',
                sequence: '30',
                description: 'This event is raised after screen has been initialized',
                handler: {
                    methodName: "initializeScreen"
                }
            }]
        },
        getUniqueRepeatingPanelUId: function() {},
        initializeScreen: function() {
            var editorInstance = _scEditorUtils.getCurrentEditor();
            var editorTitle = "Restocking";
            editorInstance.setInitialTitle(editorTitle);
            editorInstance.set('title', editorTitle);

            var inputModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
/*            _scModelUtils.setStringValueAtModelPath("Shipment.ShipNode", "", inputModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.DeliveryMethod", "", inputModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.DocumentType", "", inputModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.EnterpriseCode", "", inputModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.ShipNode.Extn.ExtnRestockingRequired", "", inputModel);*/
            _iasUIUtils.callApi(this, inputModel, "restocking_getShipmentList_ref", null);
        },
        handleMashupCompletion: function(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data) {
            _iasBaseTemplateUtils.handleMashupCompletion(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data, this);
        },
        handleMashupOutput: function(mashupRefId, modelOutput, mashupInput, mashupContext, applySetModel) {
            if (_scBaseUtils.equals(mashupRefId, "restocking_getShipmentList_ref")) {
                var outputModel = modelOutput;
                _scScreenUtils.setModel(this, "parentModel", outputModel, null);
            }
        }
    });
});
