scDefine([
	"scbase/loader!dojo/_base/declare",
	"scbase/loader!dojo/_base/kernel",
	"scbase/loader!dojo/text",
	"scbase/loader!extn/customScreen/restocking/restockingScreen",
	"scbase/loader!sc/plat/dojo/controller/ServerDataController"
], function(
	_dojodeclare, _dojokernel, _dojotext, _extntestScreen, _scServerDataController
) {
return _dojodeclare("extn.customScreen.restocking.restockingScreenBehaviorController", [_scServerDataController], {
	screenId: 'extn.customScreen.restocking.restockingScreen',
	mashupRefs: [{
            extnType: 'ADD',
            mashupRefId: 'restocking_getShipmentList_ref',
            mashupId: 'restocking_getShipmentList'
        }]

});
});
