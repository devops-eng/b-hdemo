scDefine(["dojo/text!./templates/restockingShipmentDetails.html", "scbase/loader!dojo/_base/declare", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!ias/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/RepeatingPanelUtils", "scbase/loader!sc/plat/dojo/info/ApplicationInfo", "scbase/loader!ias/utils/EventUtils", "scbase/loader!ias/utils/ModelUtils", "scbase/loader!sc/plat/dojo/utils/EditorUtils"], function(templateText, _dojodeclare, _scScreen, _scScreenUtils, _scModelUtils, _iasUIUtils, _extnExpicientUtils, _scBaseUtils, _scEventUtils, _iasBaseTemplateUtils, _iasScreenUtils, _scWidgetUtils, _scRepeatingPanelUtils, scApplicationInfo, _iasEventUtils, _iasModelUtils, _scEditorUtils) {
    return _dojodeclare("extn.customScreen.restocking.restockingShipmentDetails", [_scScreen], {
        shipmentNoArr: [],
        repeatingPanelIdArr: [],
        templateString: templateText,
        uId: "restockingShipmentDetails",
        packageName: "extn.customScreen.restocking",
        className: "restockingShipmentDetails",
        namespaces: {
            sourceBindingNamespaces: [{
                value: 'shipmentLevel',
                description: "This namespace has all info about a single shipment"
            }]
        },
        subscribers: {
            local: [{
                eventId: 'afterScreenInit',
                sequence: '30',
                description: 'This event is raised after screen has been initialized',
                handler: {
                    methodName: "initializeScreen"
                }},
                {
                eventId: 'restockingTitlePane_onShow',
                sequence: '30',
                description: 'This subscriber populates the shipmentlines details screen when the Products titlepane is expanded',
                handler: {
                    methodName: "titlePaneShowEvent"
                }
            }
            ]
        },
        initializeScreen: function() {
            var shipmentModel = _scScreenUtils.getModel(this, "shipmentLevel");
            var shipmentKey = shipmentModel.Shipment.ShipmentKey;
            var noOfItems = shipmentModel.Shipment;
            var shipmentNo = shipmentModel.Shipment.ShipmentNo;
            var orderNo = shipmentModel.Shipment.OrderNo;
            var noOfBags = shipmentModel.Shipment.DestinationZone;
            var arr = shipmentModel.Shipment.ShipmentLines.ShipmentLine;
            var noOfitems = arr.length;
            for (var k = 0; k < this.shipmentNoArr.length; k++) {
                var currShipmentNumber = _scBaseUtils.getArrayItemByIndex(this.shipmentNoArr, k);
                if (_scBaseUtils.equals(Number(currShipmentNumber), Number(shipmentNo))) {
                    this.shipmentNoArr.splice(k, 1);
                    this.repeatingPanelIdArr.splice(k, 1);
                    break;
                }
            }
            this.shipmentNoArr.push(shipmentNo);
            var reptPanelId = this.id;
            this.repeatingPanelIdArr.push(reptPanelId);
            var buttonID = "restockingComplete_" + shipmentNo;
            var mainDivId = "mainDiv_" + shipmentKey;
            var imageClassId = "imageClass_" + shipmentKey;
            var str = "<div class='maintitle' id=" + mainDivId + "><div class='restockingTab'> Shipment# " + shipmentNo + "</div>  <div class='restockingTab'> Order# " + orderNo + "</div>  <div class='restockingTab'> Number of Bags: " + noOfBags + " </div> <div class='noofitems'>Number of items: " + noOfitems + " </div> <div> <button class='restockingButton' id=" + buttonID + ">Restocking Complete</button></div></div>";
            _scWidgetUtils.setTitle(this, "restockingTitlePane", str);
            var divToClickToToggle = document.getElementsByClassName("dijitTitlePaneTitle");
            // var divall = document.getElementById("extn_customScreen_restocking_restockingScreen_0");
            // var that1 = this;
            // dojo.connect(divall, "onclick", function(evt) {
            //     shipmentNoActive = null;
            //     // $(function() {
            //     //     $('div').on('focus', function() {
            //     //         var id = $(this).attr('id') // Got id here
            //     //     })
            //     // })
            //     var presentDiv = document.getElementsByClassName("dijitTitlePaneTitle dijitTitlePaneTitleHover");
            //     if (presentDiv.length != 0) {
            //         var presentDivId = presentDiv[0].id;
            //         var innerText = presentDiv[0].innerText;
            //         innerText = innerText.replace(/(\r\n|\n|\r)/gm, "");
            //         var n = innerText.indexOf("Order");
            //         var m = innerText.indexOf("#");
            //         var m1 = m + 2;
            //         var length = n - m1;
            //         var shipmentNoActive = innerText.substr(m1, length);
            //         that1.setToggelStraight(shipmentNoActive);
            //     }
            // });
            // old code
            var homeTab = null;
            var that = this;
            homeTab = document.getElementById(buttonID);
            dojo.connect(homeTab, "onclick", function(evt) {
                evt.stopPropagation();
                var model = null;
                model = _scBaseUtils.getNewModelInstance();
                //  console.log("in the button");
                var inputModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
                _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", shipmentKey, inputModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.Extn.ExtnRestockingRequired", "N", inputModel);
                that.callApiFunction();
            });
        },
        handleMashupCompletion: function(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data) {
            _iasBaseTemplateUtils.handleMashupCompletion(mashupContext, mashupRefObj, mashupRefList, inputData, hasError, data, this);
        },
        handleMashupOutput: function(mashupRefId, modelOutput, mashupInput, mashupContext, applySetModel) {
            if (_scBaseUtils.equals(mashupRefId, "restockingItem_ref")) {
                var successFlag = _scModelUtils.getModelObjectFromPath("Shipment.isSuccess", modelOutput);
                if (successFlag == 'Y') {
                    var mainShipmentKey = mashupInput.Shipment.ShipmentKey;
                    var parentModel = _scScreenUtils.getModel(this.getOwnerScreen(), "parentModel");
                    var ShipmentsArr = _scModelUtils.getModelObjectFromPath("Page.Output.Shipments.Shipment", parentModel);
                    var totalNoOfRecords = parentModel.Page.Output.Shipments.TotalNumberOfRecords;
                    for (var i = 0; i < ShipmentsArr.length; i++) {
                        var currShipment = _scBaseUtils.getArrayItemByIndex(ShipmentsArr, i);
                        var currShipmentKey = currShipment.ShipmentKey;
                        if (_scBaseUtils.equals(Number(currShipmentKey), Number(mainShipmentKey))) {
                            ShipmentsArr.splice(i, 1);
                            totalNoOfRecords = totalNoOfRecords - 1;
                            break;
                        }
                    }
                    _scModelUtils.addModelToModelPath("Page.Output.Shipments.Shipment", ShipmentsArr, parentModel);
                    _scModelUtils.setStringValueAtModelPath("Page.Output.Shipments.TotalNumberOfRecords", totalNoOfRecords, parentModel);
                    _scScreenUtils.setModel(this.getOwnerScreen(), "parentModel", parentModel, null);
                    if (_scBaseUtils.equals(Number(totalNoOfRecords), Number(0))) {
                        var model = _scBaseUtils.getNewModelInstance();
                        _scEventUtils.fireEventGlobally(this, "extn_refreshBopusDashboard", null, model);
                        var editor = _scEditorUtils.getCurrentEditor();
                        _scEditorUtils.closeEditor(editor);
                    }
                }
            }
        },
        callApiFunction: function() {
            var shipmentModel = _scScreenUtils.getModel(this, "shipmentLevel");
            var shipmentKey = shipmentModel.Shipment.ShipmentKey;
            var inputModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
            _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", shipmentKey, inputModel);
            _scModelUtils.setStringValueAtModelPath("Shipment.Extn.ExtnRestockingRequired", "N", inputModel);
            _iasUIUtils.callApi(this, inputModel, "restockingItem_ref", null);
        },
        setToggelStraight: function(shipmentNoActive) {
            var repeatingPanelId = null;
            var shipmentNo = null;
            var screenID = this.id;
            for (var n = 0; n < this.repeatingPanelIdArr.length; n++) {
                var currScreenId = _scBaseUtils.getArrayItemByIndex(this.repeatingPanelIdArr, n);
                if (_scBaseUtils.equals((screenID), (currScreenId))) {
                    shipmentNo = _scBaseUtils.getArrayItemByIndex(this.shipmentNoArr, n);
                    break;
                }
            }
            if (_scBaseUtils.equals(Number(shipmentNo), Number(shipmentNoActive))) {
                for (var i = 0; i < this.shipmentNoArr.length; i++) {
                    var currShipmentNo = _scBaseUtils.getArrayItemByIndex(this.shipmentNoArr, i);
                    if (_scBaseUtils.equals(Number(currShipmentNo), Number(shipmentNo))) {
                        var repeatingPanelId = _scBaseUtils.getArrayItemByIndex(this.repeatingPanelIdArr, i);
                        break;
                    }
                }
                var parentScreen = this.ownerScreen;
                var childScreenArr = parentScreen._allChildScreens;
                for (var j = 0; j < childScreenArr.length; j++) {
                    var currChildScreen = _scBaseUtils.getArrayItemByIndex(childScreenArr, j);
                    var currRepeatingPanelId = currChildScreen.id;
                    if (_scBaseUtils.equals((currRepeatingPanelId), (repeatingPanelId))) {
                        var repeatingPanelScreenInstance = currChildScreen;
                    } else {
                        _scWidgetUtils.closeTogglePanel(currChildScreen, "restockingTitlePane");
                    }
                }
                var buttonID2 = "restockingComplete_" + shipmentNoActive;
                document.getElementById(buttonID2).focus();
            }
        },
        titlePaneShowEvent:function(){
            var shipmentModel = _scScreenUtils.getModel(this, "shipmentLevel");
            var shipmentNo = shipmentModel.Shipment.ShipmentNo;
            
            this.setToggelStraight(shipmentNo);
        }
    });
});
