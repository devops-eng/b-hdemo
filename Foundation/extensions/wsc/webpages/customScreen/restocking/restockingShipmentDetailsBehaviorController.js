scDefine([
	"scbase/loader!dojo/_base/declare",
	"scbase/loader!dojo/_base/kernel",
	"scbase/loader!dojo/text",
	"scbase/loader!extn/customScreen/restocking/restockingShipmentDetails",
	"scbase/loader!sc/plat/dojo/controller/ServerDataController"
], function(
	_dojodeclare, _dojokernel, _dojotext, _extnrepeatingScreen, _scServerDataController
) {
return _dojodeclare("extn.customScreen.restocking.restockingShipmentDetailsBehaviorController", [_scServerDataController], {
	screenId: 'extn.customScreen.restocking.restockingShipmentDetails',
	mashupRefs: [{
            extnType: 'ADD',
            mashupRefId: 'restockingItem_ref',
            mashupId: 'restockingItem'
        }]

});
});
