scDefine([
    "dojo/text!./templates/restockingShipmentLineDetails.html",
    "scbase/loader!dojo/_base/declare",
    "scbase/loader!sc/plat/dojo/widgets/Screen",
    "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!dojo/currency",
    "scbase/loader!ias/utils/UIUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!ias/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/RepeatingPanelUtils", "scbase/loader!sc/plat/dojo/info/ApplicationInfo", "scbase/loader!ias/utils/EventUtils"
], function(
    templateText, _dojodeclare, _scScreen, _scScreenUtils, _scModelUtils, dCurrency,
    _iasUIUtils, _extnExpicientUtils, _scBaseUtils, _scEventUtils, _iasBaseTemplateUtils, _iasScreenUtils, _scWidgetUtils, _scRepeatingPanelUtils, scApplicationInfo, _iasEventUtils
) {
return _dojodeclare("extn.customScreen.restocking.restockingShipmentLineDetails", [_scScreen], {
    templateString: templateText,
    uId: "restockingShipmentLineDetails",
    packageName: "extn.customScreen.restocking",
    className: "restockingShipmentLineDetails",
    subscribers: {
            local: [{
                eventId: 'afterScreenInit',
                sequence: '30',
                description: 'This event is raised after screen has been initialized',
                handler: {
                    methodName: "initializeScreen"
                }
            }]
        },

    extn_getColor:function(dataValue, screen, widget, nameSpace, shipmentLineModel, options){
            var color = null;
            if (!_scBaseUtils.isVoid(shipmentLineModel)) {
                var attributeModel = _scModelUtils.getModelObjectFromPath("ShipmentLine.OrderLine.ItemDetails.AdditionalAttributeList.AdditionalAttribute", shipmentLineModel);
                if (!_scBaseUtils.isVoid(attributeModel)) {
                    for (var i = 0; i < attributeModel.length; i++) {
                        var refrenceObj = _scBaseUtils.getArrayItemByIndex(attributeModel, i);
                        var name = _scModelUtils.getStringValueFromPath("Name", refrenceObj);
                        if (_scBaseUtils.equals(name, "Color")) {
                            color = _scModelUtils.getStringValueFromPath("Value", refrenceObj);
                        }
                    }
                }
            }
            return color;
    },
    extn_getSize:function(dataValue, screen, widget, nameSpace, shipmentLineModel, options){
         var size = null;
            if (!_scBaseUtils.isVoid(shipmentLineModel)) {
                var attributeModel = _scModelUtils.getModelObjectFromPath("ShipmentLine.OrderLine.ItemDetails.AdditionalAttributeList.AdditionalAttribute", shipmentLineModel);
                if (!_scBaseUtils.isVoid(attributeModel)) {
                    for (var i = 0; i < attributeModel.length; i++) {
                        var refrenceObj = _scBaseUtils.getArrayItemByIndex(attributeModel, i);
                        var name = _scModelUtils.getStringValueFromPath("Name", refrenceObj);
                        if (_scBaseUtils.equals(name, "Size")) {
                            size = _scModelUtils.getStringValueFromPath("Value", refrenceObj);
                        }
                    }
                }
            }
            return size;

    },
    extn_getPrice: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
            var price = null;
            var country = null;
            if (!_scBaseUtils.isVoid(dataValue)) {
                var listPrice = null;
                listPrice = _scModelUtils.getStringValueFromPath("Extn.ExtnItemListPrice", dataValue);
                if (!_scBaseUtils.isVoid(listPrice)) {
                var currencyModel = _scModelUtils.getModelObjectFromPath("ShipmentLine.Shipment", shipmentLineModel);
                if (!_scBaseUtils.isVoid(currencyModel)) {
                    currencyModel = _scBaseUtils.getArrayItemByIndex(currencyModel, 0);
                        country = _scModelUtils.getStringValueFromPath("Currency", currencyModel);
                        if (!_scBaseUtils.isVoid(country)) {
                            price = dCurrency.format(parseFloat(listPrice), {
                                currency: country
                            });
                        }
                    }
                } else {
                    price = "";
                    return "";
                }
                var allPrice = {
                    "price": price
                };
                _scScreenUtils.setModel(this, "ListPrice", allPrice);
            }
            return price;
        },
    extn_getPriceAttribute: function(dataValue, screen, widget, nameSpace, shipmentLineModel, options) {
            if (!_scBaseUtils.isVoid(dataValue)) {               
                var priceAttribute = null;
                priceAttribute = _scModelUtils.getStringValueFromPath("Extn.ExtnPriceAttribute", dataValue);
                if (!_scBaseUtils.isVoid(priceAttribute)) {
                    return priceAttribute;
                }
            }
        },
    
    extn_getUPC:function(dataValue, screen, widget, nameSpace, shipmentLineModel, options){

         var aliasValue = null;
            if (!_scBaseUtils.isVoid(shipmentLineModel)) {
                var aliasModel = _scModelUtils.getModelObjectFromPath("ShipmentLine.OrderLine.ItemDetails.ItemAliasList.ItemAlias", shipmentLineModel);
                if (!_scBaseUtils.isVoid(aliasModel)) {
                    for (var i = 0; i < aliasModel.length; i++) {
                        var refrenceObj = _scBaseUtils.getArrayItemByIndex(aliasModel, i);
                        var aliasName = _scModelUtils.getStringValueFromPath("AliasName", refrenceObj);
                        if (_scBaseUtils.equals(aliasName, "ACTIVE_UPC")) {
                            aliasValue = _scModelUtils.getStringValueFromPath("AliasValue", refrenceObj);
                        }
                    }
                }
            }
            return aliasValue;
    },
    initializeScreen:function(){
        var shipmentModel = _scScreenUtils.getModel(this, "shipmentLineLevel");
        var quantity= shipmentModel.ShipmentLine.Quantity;
        var quantityLabel= "Pick "+ quantity + "Each |";
        _scWidgetUtils.setValue(this, "totalPickQtyLabel", quantityLabel);
        var quantityLabel1= quantity + "Each";
        _scWidgetUtils.setValue(this, "pickBPQtyLabel", quantityLabel1);

        var diamondFlag=shipmentModel.ShipmentLine.OrderLine.ItemDetails.Extn.ExtnJewelryFlag;
        // diamondFlag=true;
         if (diamondFlag == 'Y') {
                    _scWidgetUtils.showWidget(this, "extn_jewellarySign", false, null);
                } else {
                    _scWidgetUtils.hideWidget(this, "extn_jewellarySign");
                }
    }
});
});
