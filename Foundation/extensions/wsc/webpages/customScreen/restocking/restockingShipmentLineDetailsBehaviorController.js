scDefine([
	"scbase/loader!dojo/_base/declare",
	"scbase/loader!dojo/_base/kernel",
	"scbase/loader!dojo/text",
	"scbase/loader!extn/customScreen/restocking/restockingShipmentLineDetails",
	"scbase/loader!sc/plat/dojo/controller/ServerDataController"
], function(
	_dojodeclare, _dojokernel, _dojotext, _extnShipmentLine, _scServerDataController
) {
return _dojodeclare("extn.customScreen.restocking.restockingShipmentLineDetailsBehaviorController", [_scServerDataController], {
	screenId: 'extn.customScreen.restocking.restockingShipmentLineDetails',
	mashupRefs: []

});
});
