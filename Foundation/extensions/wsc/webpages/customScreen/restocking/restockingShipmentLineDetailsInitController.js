scDefine([
	"scbase/loader!dojo/_base/declare",
	"scbase/loader!dojo/_base/kernel",
	"scbase/loader!dojo/text",
	"scbase/loader!extn/customScreen/restocking/restockingShipmentLineDetails",
	"scbase/loader!sc/plat/dojo/controller/ScreenController"
], function(
	_dojodeclare, _dojokernel, _dojotext, _extnShipmentLine, _scScreenController
) {
return _dojodeclare("extn.customScreen.restocking.restockingShipmentLineDetailsInitController", [_scScreenController], {
	screenId: 'extn.customScreen.restocking.restockingShipmentLineDetails',
	mashupRefs: []

});
});
