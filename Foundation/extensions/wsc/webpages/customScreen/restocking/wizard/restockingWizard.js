/*
 * Licensed Materials - Property of IBM
 * IBM Sterling Order Management Store (5725-D10)
 *(C) Copyright IBM Corp. 2015 All Rights Reserved. , 2015 All Rights Reserved.
 * US Government Users Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */
scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils",
	"extn/customScreen/restocking/wizard/restockingWizardUI"], function(
_dojodeclare,_scBaseUtils,_scEventUtils,
_extnrestockingWizardUI) {

    return _dojodeclare("extn.customScreen.restocking.wizard.restockingWizard", [_extnrestockingWizardUI], {
           
           extn_handleClose: function(event, bEvent, ctrl, args){
                var model = null;
                model = _scBaseUtils.getNewModelInstance();
                _scEventUtils.fireEventGlobally(this, "extn_refreshBopusDashboard", null, model);
        }
    });
});
