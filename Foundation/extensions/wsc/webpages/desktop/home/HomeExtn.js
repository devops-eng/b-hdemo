scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/desktop/home/HomeExtnUI", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!sc/plat/dojo/utils/ResourcePermissionUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!sc/plat/dojo/utils/PlatformUIFmkImplUtils", "scbase/loader!wsc/mobile/container/AppHeader", "scbase/loader!dojo/_base/lang", "scbase/loader!dojo/_base/json", "scbase/loader!ias/polling/Poller", "scbase/loader!dojo/dom-class", "scbase/loader!dojo/dom-geometry", "scbase/loader!dojo/dom-attr", "scbase/loader!dojo/aspect", "scbase/loader!dijit/form/Button", "scbase/loader!dijit/Menu", "scbase/loader!dijit/MenuItem", "scbase/loader!dijit/MenuSeparator", "scbase/loader!dijit/DropDownMenu", "scbase/loader!dijit/form/DropDownButton", "scbase/loader!sc/plat/dojo/utils/ResourcePermissionUtils", "scbase/loader!sc/plat/dojo/info/ApplicationInfo", "scbase/loader!sc/plat/dojo/events/GlobalEventManager", "scbase/loader!sc/plat/dojo/widgets/Link", "scbase/loader!sc/plat/dojo/utils/LogUtils", "scbase/loader!wsc/mobile/container/ApplicationMenu", "scbase/loader!wsc", "scbase/loader!wsc/mobile/home/utils/MobileHomeUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/EditorUtils", "scbase/loader!sc/plat/dojo/info/ApplicationInfo"], function(_dojodeclare, _extnHomeExtnUI, _scBaseUtils, _scModelUtils, _scResourcePermissionUtils, _scScreenUtils, _scWidgetUtils, _scEventUtils, _iasContextUtils, _scPlatformUIFmkImplUtils, wscAppHeader, dLang, dJSON, dPoller, dDomClass, domGeometry, dDomAttr, dAspect, dButton, dMenu, dMenuItem, dMenuSeparator, dDropDownMenu, dDDButton, scResourcePermissionUtil, scApplicationInfo, scGlobalEventManager, scLink, scLogUtils, wscApplicationMenu, wsc, _wscMobileHomeUtils, _extnExpicientUtils, _iasUIUtils, _scEditorUtils, scApplicationInfo) {
    return _dojodeclare("extn.desktop.home.HomeExtn", [_extnHomeExtnUI], {
        // custom code here
        extn_refreshAlert: function(event, bEvent, ctrl, args) {
            if (_scResourcePermissionUtils.hasPermission("WSC000197")) {
                this.extn_afterScreenLoad();
            }
        },
        extn_afterScreenLoad: function(event, bEvent, ctrl, args) {
            // _scWidgetUtils.hideWidget(this, "customerPickPortlet", false);
            var portletWidget = _scScreenUtils.getWidgetByUId(this, "backroomPickPortlet");
            if (_scBaseUtils.equals(portletWidget.isHidden, true)) {
                _scWidgetUtils.showWidget(this, "backroomPickPortlet", true, null);
                this.showPortlet("backroomPickPortlet");
            }
            var homeTab = null;
            var that = this;
            homeTab = document.getElementById('borderContainerCenter_tablist_wsc_desktop_editors_HomeEditor_0').parentNode;
            dojo.connect(homeTab, "onclick", function(evt) {
                var model = null;
                model = _scBaseUtils.getNewModelInstance();
                _scEventUtils.fireEventGlobally(that, "extn_refreshBopusDashboard", null, model);
                // Do stuff on click
            });
            //for bopus alert
            if (_scResourcePermissionUtils.hasPermission("WSC000197")) {
                var myButton = document.getElementById("alertText");
                myButton.innerHTML = "";
                dojo.connect(myButton, "onclick", function(evt) {
                    var model = null;
                    model = _scBaseUtils.getNewModelInstance();
                    //_scModelUtils.setStringValueAtModelPath("Shipment.IsDashBoardAlert", "Y", model);
                    if (myButton.innerHTML) {
                        myButton.innerHTML = "";
                        var currEditor = null;
                        currEditor = _scEditorUtils.getCurrentEditor(this);
                        if (!_scBaseUtils.equals(currEditor.className, "HomeEditor")) {
                            var scScreenContainerInstance = scApplicationInfo.getSCScreenContainerInstance();
                            var allopenEditorsArray = scScreenContainerInstance.getChildren();
                            var homeReference = null;
                            if (!_scBaseUtils.isVoid(allopenEditorsArray)) {
                                for (var i = 0; i < allopenEditorsArray.length; i++) {
                                    var referenceArray = null;
                                    referenceArray = _scBaseUtils.getArrayItemByIndex(allopenEditorsArray, i);
                                    var classEditor = null;
                                    classEditor = referenceArray.className;
                                    if (_scBaseUtils.equals("HomeEditor", classEditor)) {
                                        homeReference = referenceArray;
                                        break;
                                    }
                                }
                                if (homeReference) {
                                    _scEditorUtils.focusOnEditor(homeReference, null);
                                }
                            }
                        }
                        _scEventUtils.fireEventGlobally(that, "extn_refreshBopusDashboard", null, model);
                    }
                });
                this.getPollingInterval();
            }
        },
        getPollingInterval: function() {
            var apiInput = _scModelUtils.createNewModelObjectWithRootKey("CommonCode");
            _scModelUtils.setStringValueAtModelPath("CommonCode.CodeType", "BOPUS_POLLING", apiInput);
            _iasUIUtils.callApi(this, apiInput, "extn_getPollingInterval", null);
        },
        alertsPolling: function(appConfig, screenClass, _scScreenUtils, pollingInverval) {
            that = screenClass;
            screenUtils = _scScreenUtils;
            var pollWorker = function(success) {
                if (success && success.output && success.output.StoreAlert && success.output.StoreAlert.AlertFlag) {
                    var myButton = document.getElementById("alertText");
                    if (_scBaseUtils.equals(success.output.StoreAlert.AlertFlag, "Y")) {
                        myButton.innerHTML = "Bopus Alert";
                    } else {
                        myButton.innerHTML = "";
                    }
                }
            }
            var fullUrl = scApplicationInfo.getApplicationContext() + '/store/mobile/orderPollCount.do';
            var shipNode = _iasContextUtils.getFromContext("CurrentStore");
            var pollingContext = {
                url: fullUrl,
                sync: false,
                httpMethod: 'POST',
                inputObj: {
                    'getBopusDashboardCount_input': dJSON.toJson({
                        "Shipment": {
                            "IsDashBoardAlert": "Y",
                            "ShipNode": shipNode
                        }
                    })
                },
                handleError: function(e) {
                    scLogUtils.error(e);
                }
            };
            var pollID = dPoller.startPolling(pollWorker, pollingContext, pollingInverval, false);
        },
        extn_beforeInitialize: function(event, bEvent, ctrl, args) {
            if (_scResourcePermissionUtils.hasPermission("WSC000198")) {
                _scWidgetUtils.showWidget(this, "extn_storeContextPortlet", true, null);
                this.showPortlet("extn_storeContextPortlet");
            }
            if (_scResourcePermissionUtils.hasPermission("WSC000199")) {
                _scWidgetUtils.showWidget(this, "extn_customerServicePortlet", true, null);
                this.showPortlet("extn_customerServicePortlet");
            }
        },
        extn_afterBehaviorMashupCall: function(event, bEvent, ctrl, args) {
            var isMashupAvailable1 = _extnExpicientUtils.isMashupAvailable("extn_getPollingInterval", args);
            if (isMashupAvailable1) {
                var outputData = null;
                outputData = _extnExpicientUtils.getMashupOutputForRefID("extn_getPollingInterval", args);
                if (outputData && outputData.CommonCodeList && outputData.CommonCodeList.CommonCode && outputData.CommonCodeList.CommonCode.length > 0) {
                    var codeList = null;
                    codeList = _scModelUtils.getModelListFromPath("CommonCodeList.CommonCode", outputData);
                    var codeValue = null;
                    for (var i = 0; i < codeList.length; i++) {
                        var currObj = null;
                        currObj = _scBaseUtils.getArrayItemByIndex(codeList, i);
                        var longDesc = null;
                        longDesc = _scModelUtils.getStringValueFromPath("CodeLongDescription", currObj);
                        if (_scBaseUtils.equals(longDesc, "SAKS_BOPUS_DASHBOARD")) {
                            codeValue = _scModelUtils.getStringValueFromPath("CodeValue", currObj);
                            break;
                        }
                    }
                    if (codeValue) {
                        var appConfig = null;
                        appConfig = _scBaseUtils.getNewModelInstance();
                        this.alertsPolling(appConfig, this, _scScreenUtils, codeValue);
                    }
                }
            }
        }
    });
});
