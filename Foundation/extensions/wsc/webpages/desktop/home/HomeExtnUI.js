
scDefine(["dojo/text!./templates/HomeExtn.html","scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/_base/lang","scbase/loader!dojo/text","scbase/loader!extn/customScreen/BopusDashboardPortletInitController","scbase/loader!extn/customScreen/CustomerServicePortletInitController","scbase/loader!extn/customScreen/StoreContextPortletInitController","scbase/loader!extn/customScreen/UpdatePrinterPortletInitController","scbase/loader!idx/layout/ContentPane","scbase/loader!sc/plat","scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!sc/plat/dojo/widgets/ControllerWidget","scbase/loader!sc/plat/dojo/widgets/IdentifierControllerWidget","scbase/loader!wsc/desktop/home/portlets/BackroomPickPortletInitController","scbase/loader!wsc/desktop/home/portlets/ConfirmShipmentPortletInitController","scbase/loader!wsc/desktop/home/portlets/CustomerPickPortletInitController","scbase/loader!wsc/desktop/home/portlets/OrderCapturePortletInitController","scbase/loader!wsc/desktop/home/portlets/PackPortletInitController","scbase/loader!wsc/desktop/home/portlets/PickOrdersPortletInitController"]
 , function(			 
			    templateText
			 ,
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojolang
			 ,
			    _dojotext
			 ,
			    _extnBopusDashboardPortletInitController
			 ,
			    _extnCustomerServicePortletInitController
			 ,
			    _extnStoreContextPortletInitController
			 ,
			    _extnUpdatePrinterPortletInitController
			 ,
			    _idxContentPane
			 ,
			    _scplat
			 ,
			    _scBaseUtils
			 ,
			    _scControllerWidget
			 ,
			    _scIdentifierControllerWidget
			 ,
			    _wscBackroomPickPortletInitController
			 ,
			    _wscConfirmShipmentPortletInitController
			 ,
			    _wscCustomerPickPortletInitController
			 ,
			    _wscOrderCapturePortletInitController
			 ,
			    _wscPackPortletInitController
			 ,
			    _wscPickOrdersPortletInitController
){
return _dojodeclare("extn.desktop.home.HomeExtnUI",
				[], {
			templateString: templateText
	
	
	
	
	
	
	
	,
	hotKeys: [ 
	]

,events : [
	]

,subscribers : {

local : [

{
	  eventId: 'afterScreenInit'

,	  sequence: '19'




,handler : {
methodName : "extn_beforeInitialize"

 
}
}
,
{
	  eventId: 'afterScreenLoad'

,	  sequence: '51'




,handler : {
methodName : "extn_afterScreenLoad"

 
}
}
,
{
	  eventId: 'afterBehaviorMashupCall'

,	  sequence: '51'




,handler : {
methodName : "extn_afterBehaviorMashupCall"

 
}
}

]
,
global : [

{
	  eventId: 'extn_refreshAlert'

,	  sequence: '51'




,handler : {
methodName : "extn_refreshAlert"

 
}
}

]
}

});
});


