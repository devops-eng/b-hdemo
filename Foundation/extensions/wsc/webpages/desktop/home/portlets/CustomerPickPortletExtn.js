scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/desktop/home/portlets/CustomerPickPortletExtnUI", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!ias/utils/ContextUtils", "scbase/loader!ias/utils/EventUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!sc/plat/dojo/utils/ResourcePermissionUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!wsc/components/shipment/common/utils/ShipmentUtils", "scbase/loader!wsc/desktop/home/portlets/CustomerPickPortletUI", "scbase/loader!wsc/desktop/home/utils/HomeUtils"], function(_dojodeclare, _extnCustomerPickPortletExtnUI, _iasBaseTemplateUtils, _iasContextUtils, _iasEventUtils, _iasUIUtils, _scBaseUtils, _scModelUtils, _scResourcePermissionUtils, _scScreenUtils, _wscShipmentUtils, _wscCustomerPickPortletUI, _wscHomeUtils) {
    return _dojodeclare("extn.desktop.home.portlets.CustomerPickPortletExtn", [_extnCustomerPickPortletExtnUI], {
        // custom code here
        processShipmentListByCustomerOutput: function(modelOutput, mashupInput) {
            var numberOfRecords = null;
            numberOfRecords = _scModelUtils.getStringValueFromPath("Shipments.TotalNumberOfRecords", modelOutput);
            if (_scBaseUtils.equals(numberOfRecords, "1")) {
                var status = null;
                status = _scModelUtils.getStringValueFromPath("Shipments.Shipment.0.Status", modelOutput);
                var values = null;
                values = _scBaseUtils.getAttributeValue("Shipments.Shipment.0", false, modelOutput);
                var shipmentModel = null;
                shipmentModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
                _scModelUtils.addModelObjectAsChildToModelObject("Shipment", values, shipmentModel);
                if (_wscShipmentUtils.startsWith(status, "1600.002.10")) {
                    if (!(_scBaseUtils.equals(_iasContextUtils.getFromContext("CurrentStore"), _scModelUtils.getStringValueFromPath("Shipments.Shipment.0.ShipNode", modelOutput)))) {
                        _iasUIUtils.openWizardInEditor("wsc.components.shipment.summary.ShipmentSummaryWizard", shipmentModel, "wsc.desktop.editors.ShipmentEditor", this, null);
                    } else {
                        _iasUIUtils.openWizardInEditor("wsc.components.shipment.customerpickup.CustomerPickUpWizard", shipmentModel, "wsc.desktop.editors.ShipmentEditor", this, null);
                    }
                } else {
                    _iasUIUtils.openWizardInEditor("wsc.components.shipment.summary.ShipmentSummaryWizard", shipmentModel, "wsc.desktop.editors.ShipmentEditor", this, null);
                }
            } else if (_scBaseUtils.equals(numberOfRecords, "0")) {
                var byRecipient = null;
                byRecipient = _scScreenUtils.getTargetModel(this, "getShipmentListByRecipient_input", null);
                _iasUIUtils.callApi(this, byRecipient, "getShipmentListByRecipient", null);
            } else if (!(_scBaseUtils.isVoid(numberOfRecords))) {
                this.multipleReturned(mashupInput, modelOutput, "customer");
            }
            this.resetFields();
        },
        processShipmentListByRecipientOutput: function(modelOutput, mashupInput) {
            var numberOfRecords = null;
            numberOfRecords = _scModelUtils.getStringValueFromPath("Shipments.TotalNumberOfRecords", modelOutput);
            if (_scBaseUtils.equals(numberOfRecords, "1")) {
                var status = null;
                status = _scModelUtils.getStringValueFromPath("Shipments.Shipment.0.Status", modelOutput);
                var values = null;
                values = _scBaseUtils.getAttributeValue("Shipments.Shipment.0", false, modelOutput);
                var shipmentModel = null;
                shipmentModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
                _scModelUtils.addModelObjectAsChildToModelObject("Shipment", values, shipmentModel);
                if (_wscShipmentUtils.startsWith(status, "1600.002.10")) {
                    if (!(_scBaseUtils.equals(_iasContextUtils.getFromContext("CurrentStore"), _scModelUtils.getStringValueFromPath("Shipments.Shipment.0.ShipNode", modelOutput)))) {
                        _iasUIUtils.openWizardInEditor("wsc.components.shipment.summary.ShipmentSummaryWizard", shipmentModel, "wsc.desktop.editors.ShipmentEditor", this, null);
                    } else {
                        _iasUIUtils.openWizardInEditor("wsc.components.shipment.customerpickup.CustomerPickUpWizard", shipmentModel, "wsc.desktop.editors.ShipmentEditor", null);
                    }
                } else {
                    _iasUIUtils.openWizardInEditor("wsc.components.shipment.summary.ShipmentSummaryWizard", shipmentModel, "wsc.desktop.editors.ShipmentEditor", this, null);
                }
            } else if (_scBaseUtils.equals(numberOfRecords, "0")) {
                var searchCriteriaModel = null;
                searchCriteriaModel = this.getSearchCriteriaModel(mashupInput, "recipient");
                _scModelUtils.setStringValueAtModelPath("Shipment.RefreshSearchResult", "true", searchCriteriaModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.DoSearchOnInit", "true", searchCriteriaModel);
                //_scModelUtils.setStringValueAtModelPath("Shipment.ErrorMessage", _scScreenUtils.getString(this, "Label_OrderNotPick"), searchCriteriaModel);
                _scModelUtils.setStringValueAtModelPath("Shipment.NoDefaultStatuses", "true", searchCriteriaModel);
                _iasUIUtils.openWizardInEditor("wsc.components.shipment.search.ShipmentSearchWizard", searchCriteriaModel, "wsc.desktop.editors.ShipmentSearchEditor", this, null);
            } else if (!(_scBaseUtils.isVoid(numberOfRecords))) {
                this.multipleReturned(mashupInput, modelOutput, "recipient");
            }
            this.resetFields();
        }
    });
});
