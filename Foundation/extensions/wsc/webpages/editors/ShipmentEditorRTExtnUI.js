
scDefine(["dojo/text!./templates/ShipmentEditorRTExtn.html","scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/_base/lang","scbase/loader!dojo/text","scbase/loader!sc/plat","scbase/loader!sc/plat/dojo/binding/CurrencyDataBinder","scbase/loader!sc/plat/dojo/utils/BaseUtils","scbase/loader!sc/plat/dojo/widgets/Link"]
 , function(			 
			    templateText
			 ,
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojolang
			 ,
			    _dojotext
			 ,
			    _scplat
			 ,
			    _scCurrencyDataBinder
			 ,
			    _scBaseUtils
			 ,
			    _scLink
){
return _dojodeclare("extn.editors.ShipmentEditorRTExtnUI",
				[], {
			templateString: templateText
	
	
	
	
	
	
	

	,
	hotKeys: [ 
	]

,events : [
	]

,subscribers : {
local : [

{
	  eventId: 'extn_link_RT_PrintPickTicket_onClick'

,	  sequence: '51'

,	  description: 'extnhandlePickTicketCall'



,handler : {
methodName : "extnhandlePickTicketCall"
, className :  "ExtnShipmentSearchUtils" 
, packageName :  "extn.utils"  
 
}
}

]
					 	 ,
}

});
});


