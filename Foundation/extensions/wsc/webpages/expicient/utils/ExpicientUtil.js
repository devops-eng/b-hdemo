scDefine(["scbase/loader!dojo/_base/declare",
    "scbase/loader!dojo/_base/lang",
    "scbase/loader!sc/plat",
    "scbase/loader!sc/plat/dojo/utils/WidgetUtils",
    "scbase/loader!dijit/form/Button",
    "scbase/loader!idx/form/CheckBox",
    "scbase/loader!idx/form/CurrencyTextBox",
    "scbase/loader!sc/plat/dojo/widgets/DataLabel",
    "scbase/loader!dijit/form/TextBox",
    "scbase/loader!sc/plat/dojo/widgets/Label",
    "scbase/loader!idx/form/NumberTextBox",
    "scbase/loader!idx/form/Textarea",
    "scbase/loader!idx/form/TextBox",
    "scbase/loader!sc/plat/dojo/utils/BaseUtils",
    "scbase/loader!sc/plat/dojo/utils/EventUtils",
    "scbase/loader!sc/plat/dojo/utils/ScreenUtils",
    "scbase/loader!sc/plat/dojo/utils/EditorUtils",
    "scbase/loader!sc/plat/dojo/events/BusinessEventObj",
    "scbase/loader!sc/plat/dojo/utils/ModelUtils",
    "scbase/loader!ias/utils/UIUtils",
    "scbase/loader!ias/utils/EditorScreenUtils",
    "scbase/loader!sc/plat/dojo/Userprefs",
    "scbase/loader!sc/plat/dojo/utils/WizardUtils",
    "scbase/loader!sc/plat/dojo/utils/WidgetUtils",
	"scbase/loader!ias/utils/ContextUtils"
], function(
    _dojodeclare,
    dLang,
    scPlat,
    scWidgetUtils,
    dijit_form_Button,
    idx_form_CheckBox,
    idx_form_CurrencyTextBox,
    sc_plat_dojo_widgets_DataLabel,
    dijit_form_TextBox,
    sc_plat_dojo_widgets_Label,
    idx_form_NumberTextBox,
    idx_form_Textarea,
    idx_form_TextBox,
    _scBaseUtils,
    _scEventUtils,
    _scScreenUtils,
    _scEditorUtils,
    scBusinessEventObj,
    _scModelUtils,
    _iasUIUtils,
    _iasEditorScreenUtils,
    _scUserprefs,
    _scWizardUtils,
    _scWidgetUtils,
	_iasContextUtils) {
    var expicientUtil = dLang.getObject("expicient.utils.ExpicientUtil", true, scPlat);
    //This is bug in extensibility workbench. I can't define new event.To make it work, we can use this
    var prod = true;
    var location = "isccs";
    if (prod != true) {
        location = "isccsdev"
    }
    expicientUtil.addEventToManager = function(screen, eventName) {
            var myEvent = {};
            myEvent.name = eventName;
            screen._eMgr.addEventsAndMap([myEvent]);
        },

        expicientUtil.setComPermission= function(Permission){
            document.comPermission=Permission;
        },
        expicientUtil.getComPermission= function(Permission){
            return document.comPermission;
        },

        expicientUtil.setContext= function(context){
            document.cContext=context;
        },
        expicientUtil.getContext= function(context){
            return document.cContext;
        },


        expicientUtil.setToken = function(token) {
            document.cToken = token;
        },

        expicientUtil.getToken = function(token, orderHeaderKey) {

            return document.cToken;
        },

        expicientUtil.getComContext= function() {
            if (!_scBaseUtils.equals(window.location.pathname.indexOf("wscdev"),-1)) {
               return "isccsdev";
            }
            else{
                return "isccs";
            }
        },

        expicientUtil.getComUrl = function(orderHeaderKey) {
             
            var url = window.location.origin + "/" + expicientUtil.getContext() + "/isccs/container/home.do?scFlag=Y&token=" + encodeURIComponent(expicientUtil.getToken());
           if (!_scBaseUtils.isVoid(orderHeaderKey)) {
                url = url + "#OrderHeaderKey=" + orderHeaderKey;
            } else {
                url = url + "#OS";
            }
            return url;
        },


        expicientUtil.openCOM = function(orderHeaderKey) {
            window.name = "store";
            var url = expicientUtil.getComUrl(orderHeaderKey);

            if (document.callWindow != null) {
                document.callWindow.close()
            }
            document.callWindow = window.open(url, 'callcenter');


            document.callWindow.onload = function(e) {
               /* if (document.callWindow.location.pathname.includes("login.do")) {
                    document.callWindow = window.open(url, 'callcenter');
                }*/
                 if (!_scBaseUtils.equals(document.callWindow.location.pathname.indexOf("login.do"),-1)) {
                    document.callWindow = window.open(url, 'callcenter');
                }
            }
            document.callWindow.focus();
        },



        expicientUtil.getCurrentDate = function() {
            return expicientUtil.getDateInFormat(new Date());
        },
        expicientUtil.getDateInFormat = function(date) {
            var today = date;
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd
            }
            if (mm < 10) {
                mm = '0' + mm
            }
            var today = yyyy + '-' + mm + '-' + dd;
            return today;
        },


        //this should be used only where events are not avaliable OOB
        expicientUtil.addEventAndSubscriber = function(screen, eventId, subscriberId) {
            var eventObj = new scBusinessEventObj({
                name: eventId
            }, screen);
            screen._eMgr.addEventObject(eventObj)
            _scEventUtils.subscribeEvent(screen, {
                handler: {
                    methodName: subscriberId
                },
                eventId: eventId
            });
        },
        expicientUtil.getMashupOutputForRefID = function(mashrefID, output) {
            var mashupArray = output.mashupArray;
            var length = _scBaseUtils.getAttributeCount(mashupArray);
            var isRefIdAvailable = false;
            for (var index = 0; _scBaseUtils.lessThan(index, length); index = _scBaseUtils.incrementNumber(index, 1)) {
                var mashupDetail = null;
                mashupDetail = _scBaseUtils.getArrayBeanItemByIndex(mashupArray, index);
                var mashupRefId = mashupDetail.mashupRefId;
                if (_scBaseUtils.equals(mashrefID, mashupRefId)) {
                    var mashupOutput = mashupDetail.mashupRefOutput;
                    return mashupOutput;
                }
            }
        },

        expicientUtil.isNumber = function(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        },
        expicientUtil.getMashupInputForRefID = function(mashrefID, output) {
            var mashupArray = output.mashupRefs;
            var length = _scBaseUtils.getAttributeCount(mashupArray);
            var isRefIdAvailable = false;
            for (var index = 0; _scBaseUtils.lessThan(index, length); index = _scBaseUtils.incrementNumber(index, 1)) {
                var mashupDetail = null;
                mashupDetail = _scBaseUtils.getArrayBeanItemByIndex(mashupArray, index);
                var mashupRefId = mashupDetail.mashupRefId;
                if (_scBaseUtils.equals(mashrefID, mashupRefId)) {
                    var mashupInput = mashupDetail.mashupInputObject;
                    return mashupInput;
                }
            }
        },
		 expicientUtil.getInputMashupForRefID = function(mashrefID, output) {
            var mashupArray = output.inputData;
            var length = _scBaseUtils.getAttributeCount(mashupArray);
            var isRefIdAvailable = false;
            for (var index = 0; _scBaseUtils.lessThan(index, length); index = _scBaseUtils.incrementNumber(index, 1)) {
                var mashupDetail = null;
                mashupDetail = _scBaseUtils.getArrayBeanItemByIndex(mashupArray, index);
                var mashupRefId = mashupDetail.mashupRefId;
                if (_scBaseUtils.equals(mashrefID, mashupRefId)) {
                    var mashupInput = mashupDetail.mashupInputObject;
                    return mashupInput;
                }
            }
        },
        expicientUtil.isBeforeBehaviorMashupAvailable = function(mashrefID, output) {
            var mashupRefs = output.mashupRefs;
            var length = _scBaseUtils.getAttributeCount(mashupRefs);
            var isRefIdAvailable = false;
            for (var index = 0; _scBaseUtils.lessThan(index, length); index = _scBaseUtils.incrementNumber(index, 1)) {
                var mashupDetail = null;
                mashupDetail = _scBaseUtils.getArrayBeanItemByIndex(mashupRefs, index);
                var mashupRefId = mashupDetail.mashupRefId;
                if (_scBaseUtils.equals(mashrefID, mashupRefId)) {
                    isRefIdAvailable = true;
                }
            }
            return isRefIdAvailable;
        },
        expicientUtil.isMashupAvailable = function(mashrefID, output) {
            var mashupArray = output.mashupArray;
            var length = _scBaseUtils.getAttributeCount(mashupArray);
            var isRefIdAvailable = false;
            for (var index = 0; _scBaseUtils.lessThan(index, length); index = _scBaseUtils.incrementNumber(index, 1)) {
                var mashupDetail = null;
                mashupDetail = _scBaseUtils.getArrayBeanItemByIndex(mashupArray, index);
                var mashupRefId = mashupDetail.mashupRefId;
                if (_scBaseUtils.equals(mashrefID, mashupRefId)) {
                    isRefIdAvailable = true;
                }
            }
            return isRefIdAvailable;
        },
		 expicientUtil.getPageSizeForRepeatingScreen_restocking = function(path, screen, repeatingScreenId, model) {
            var rtn = "StoreBatch";
            return _iasContextUtils.getPaginationSize(rtn);
        },
        expicientUtil.getPageSizesForRepeatingScreen_restocking = function(path, screen, repeatingScreenId, model) {
            var rtn = "StoreBatch";
            return _iasContextUtils.getPaginationSizes(rtn);
        }
    return expicientUtil;
});
