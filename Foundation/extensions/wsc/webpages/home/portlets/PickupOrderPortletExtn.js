scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/home/portlets/PickupOrderPortletExtnUI", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!extn/utils/extnutils/CustomerPickUpUtils", "scbase/loader!isccs/utils/UIUtils"], function(_dojodeclare, _extnPickupOrderPortletExtnUI, _scBaseUtils, _scWidgetUtils, _scModelUtils, _scScreenUtils, customerPickupUtils, _isccsUIUtils)
{
    return _dojodeclare("extn.home.portlets.PickupOrderPortletExtn", [_extnPickupOrderPortletExtnUI],
    {
        // overriden the method to valiate  the "shipment no" field in landing page
        pickUpOrderSearchAction: function()
        {
            var pickUpOrderTargetModel = null;
            var warningString = null;
            var textObj = null;
            textObj = _scBaseUtils.getNewBeanInstance();
            var textOK = null;
            textOK = "OK";
            _scBaseUtils.addStringValueToBean("OK", textOK, textObj);
            if (!(_scScreenUtils.isValid(this, "customerpickup_getShipmentListForPickup_input")))
            {
                warningString = _scScreenUtils.getString(this, "InvalidSearchCriteria");
                _scScreenUtils.showErrorMessageBox(this, warningString, "waringCallback", textObj, null);
            }
            else
            {
                pickUpOrderTargetModel = _scBaseUtils.getTargetModel(this, "customerpickup_getShipmentListForPickup_input", null);
                _scModelUtils.setStringValueAtModelPath("Shipment.SearchGiftAddresses", "Y", pickUpOrderTargetModel);
                var validationResult = null;
                validationResult = customerPickupUtils.validateSearchPickUpOrderInput(pickUpOrderTargetModel);
                var hasValidationError = null;
                hasValidationError = _scModelUtils.getBooleanValueFromPath("hasError", validationResult, null);
                if (_scBaseUtils.isBooleanTrue(hasValidationError))
                {
                    var validationMsg = null;
                    validationMsg = _scModelUtils.getStringValueFromPath("errorMsg", validationResult);
                    warningString = _scScreenUtils.getString(this, validationMsg);
                    _scScreenUtils.showErrorMessageBox(this, warningString, "waringCallback", textObj, null);
                }
                else
                {
                    _isccsUIUtils.callApi(this, pickUpOrderTargetModel, "searchPickupOrders", null);
                }
            }
        },
    });
});
