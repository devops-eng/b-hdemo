
scDefine(["scbase/loader!dojo/_base/declare",
	"scbase/loader!extn/mobile/common/screens/shipment/picking/ShipmentPickDetailsExtnUI",
	"scbase/loader!sc/plat/dojo/utils/BaseUtils",
	"scbase/loader!sc/plat/dojo/utils/ModelUtils",
	"scbase/loader!wsc/components/shipment/common/utils/ShipmentUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnShipmentPickDetailsExtnUI,
			    _scBaseUtils,
			    _scModelUtils,
			    _wscShipmentUtils
){ 
	return _dojodeclare("extn.mobile.common.screens.shipment.picking.ShipmentPickDetailsExtn", [_extnShipmentPickDetailsExtnUI],{
	// custom code here
	extn_getOrderPlacedDate: function(dataValue, screen, widget, nameSpace, shipmentDetailsModel, options) {
        if (!_scBaseUtils.isVoid(shipmentDetailsModel)) {
            var shipmentLineModel = _scModelUtils.getModelObjectFromPath("Shipment.ShipmentLines.ShipmentLine", shipmentDetailsModel);
            if (!_scBaseUtils.isVoid(shipmentLineModel)) {
                var model = _scBaseUtils.getArrayItemByIndex(shipmentLineModel, 0);
                var orderDate = _scModelUtils.getStringValueFromPath("Order.OrderDate", model);
                if (!_scBaseUtils.isVoid(orderDate)) {
                    return this.extn_getDate(orderDate);
                } else {
                    return null;
                }
            }
        }
    },
  	extn_getShipStoreDate: function(dataValue, screen, widget, nameSpace, shipmentDetailsModel, options) {
        if (!_scBaseUtils.isVoid(shipmentDetailsModel)) {
            var createDate = _scModelUtils.getModelObjectFromPath("Shipment.Createts", shipmentDetailsModel);
            if (!_scBaseUtils.isVoid(createDate)) {
                return this.extn_getDate(createDate);
            } else {
                return null;
            }
        }
    },
   	extn_getOrderOMSDate: function(dataValue, screen, widget, nameSpace, shipmentDetailsModel, options) {
        if (!_scBaseUtils.isVoid(shipmentDetailsModel)) {
            var shipmentLineModel = _scModelUtils.getModelObjectFromPath("Shipment.ShipmentLines.ShipmentLine", shipmentDetailsModel);
            if (!_scBaseUtils.isVoid(shipmentLineModel)) {
                var model = _scBaseUtils.getArrayItemByIndex(shipmentLineModel, 0);
                var orderDate = _scModelUtils.getStringValueFromPath("Order.Createts", model);
                if (!_scBaseUtils.isVoid(orderDate)) {
                    return this.extn_getDate(orderDate);
                } else {
                    return null;
                }
            }
        }
    },
    extn_getDate: function(date) {
        var dateVal = new Date(date);
        var finalDate = dateVal.toLocaleDateString();
        return finalDate;
    },
    getFormattedNameDisplay: function(dataValue, screen, widget, namespace, modelObj, options) {
            var billToAddressModel = null;
            billToAddressModel = _scModelUtils.getModelObjectFromPath("Shipment.BillToAddress", modelObj);
            if (!(_scBaseUtils.isVoid(billToAddressModel))) {
                dataValue = _wscShipmentUtils.getNameDisplay(this, billToAddressModel);
            }
            return dataValue;
        },
});
});

