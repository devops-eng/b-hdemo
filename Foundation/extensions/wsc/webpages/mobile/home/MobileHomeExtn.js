
scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!extn/mobile/home/MobileHomeExtnUI","scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!sc/plat/dojo/widgets/Screen", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!ias/utils/UIUtils", "scbase/loader!extn/expicient/utils/ExpicientUtil", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!ias/utils/BaseTemplateUtils", "scbase/loader!ias/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/RepeatingPanelUtils", "scbase/loader!sc/plat/dojo/info/ApplicationInfo", "scbase/loader!ias/utils/EventUtils"]
,
function(			 
			    _dojodeclare
			 ,
			    _extnMobileHomeExtnUI,
			    _scUserprefs, _scScreen, _scScreenUtils, _scModelUtils, _iasUIUtils, _extnExpicientUtils, _scBaseUtils, _scEventUtils, _iasBaseTemplateUtils, _iasScreenUtils, _scWidgetUtils, _scRepeatingPanelUtils, scApplicationInfo, _iasEventUtils
){ 
	return _dojodeclare("extn.mobile.home.MobileHomeExtn", [_extnMobileHomeExtnUI],{
	// custom code here
	
	extn_afterBehaviourMashupcall: function(event, bEvent, ctrl, args) {
			// var mashuparr= args.mashupRefs;
			// args.mashupArray=mashuparr;

            if (_extnExpicientUtils.isMashupAvailable("mobileHomeShip_getShipmentListCount_mashup_ref", args)) {
            
            	outputModel = _extnExpicientUtils.getMashupOutputForRefID("mobileHomeShip_getShipmentListCount_mashup_ref", args);
            	var noOfRecords = _scModelUtils.getStringValueFromPath("Shipments", outputModel);
            	_scScreenUtils.setModel(this, "extn_ship_getShipmentListCountoutput", noOfRecords, null);
            }
            if (_extnExpicientUtils.isMashupAvailable("mobileHomeBatch_getStoreBatchList_mashup_ref", args)) {
            
            	outputModel = _extnExpicientUtils.getMashupOutputForRefID("mobileHomeBatch_getStoreBatchList_mashup_ref", args);
            	var noOfRecords = _scModelUtils.getStringValueFromPath("StoreBatchList", outputModel);
            	_scScreenUtils.setModel(this, "extn_batch_getStoreBatchList_Count", noOfRecords, null);
            }
        },
	extn_afterScreenInit:function()
	{
		apiInput={};
		_iasUIUtils.callApi(this, apiInput, "mobileHomeShip_getShipmentListCount_mashup_ref", null);
		_iasUIUtils.callApi(this, apiInput, "mobileHomeBatch_getStoreBatchList_mashup_ref", null);
	},
	batchBindingFunc: function(dataValue, screen, widget, nameSpace, shipmentModel) 
	{
        if (_scBaseUtils.equals("0", dataValue)) {
            _scWidgetUtils.addClass(this, "pnlBatchPick", "zeroCount");
        }
        return dataValue;
   	},
    shipPackBindingFunc: function(dataValue, screen, widget, nameSpace, shipmentModel) 
    {
        if (_scBaseUtils.equals("0", dataValue)) {
            _scWidgetUtils.addClass(this, "pnlConfirmOrders", "zeroCount");
        }
        return dataValue;
    },
});
});

