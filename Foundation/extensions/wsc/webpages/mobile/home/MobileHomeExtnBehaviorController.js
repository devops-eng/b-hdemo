


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/mobile/home/MobileHomeExtn","scbase/loader!sc/plat/dojo/controller/ExtnServerDataController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnMobileHomeExtn
			 ,
			    _scExtnServerDataController
){

return _dojodeclare("extn.mobile.home.MobileHomeExtnBehaviorController", 
				[_scExtnServerDataController], {

			
			 screenId : 			'extn.mobile.home.MobileHomeExtn'

			
			
			
			
			
						,

			
			
			 mashupRefs : 	[
	 		{
		 extnType : 			'ADD'
,
		 mashupId : 			'mobileHomeShip_getShipmentListCount_mashup'
,
		 mashupRefId : 			'mobileHomeShip_getShipmentListCount_mashup_ref'

	}
,
	 		{
		 extnType : 			'ADD'
,
		 mashupId : 			'mobileHomeBatch_getStoreBatchList_mashup'
,
		 mashupRefId : 			'mobileHomeBatch_getStoreBatchList_mashup_ref'

	}

	]

}
);
});

