


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/mobile/home/subscreens/ShipFromStoreExtn","scbase/loader!sc/plat/dojo/controller/ExtnScreenController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnShipFromStoreExtn
			 ,
			    _scExtnScreenController
){

return _dojodeclare("extn.mobile.home.subscreens.ShipFromStoreExtnInitController", 
				[_scExtnScreenController], {

			
			 screenId : 			'extn.mobile.home.subscreens.ShipFromStoreExtn'

			
			
			
			
			
						,

			
			
			 mashupRefs : 	[
	 		{
		 sourceNamespace : 			''
,
		 mashupRefId : 			'getShipmentList'
,
		 sequence : 			''
,
		 mashupId : 			'shipFromStore_getShipmentListInit'
,
		 callSequence : 			''
,
		 extnType : 			'MODIFY'
,
		 sourceBindingOptions : 			''

	}

	]

}
);
});

