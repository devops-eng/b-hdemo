scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/shipment/backroomPick/BPItemScanExtnUI", "scbase/loader!isccs/utils/BaseTemplateUtils", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!wsc/utils/BackroomPickUtils", "scbase/loader!isccs/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", ], function(_dojodeclare, _extnBPItemScanExtnUI, _isccsBaseTemplateUtils, _scBaseUtils, _wscBackroomPickUtils, _isccsUIUtils, _scScreenUtils)
{
    return _dojodeclare("extn.shipment.backroomPick.BPItemScanExtn", [_extnBPItemScanExtnUI],
    {
        extn_confirmQty: function() {},
        // custom code here
        //Overridden the Call handler function 
        callbackPickedQuantityPopup: function(actionPerformed, model, popupParams)
        {
            var action = actionPerformed;
            if (action == "Ok")
            {
                this.callMultiApiSave();
            }
        },
        //Overridden the saveItemScan function form BPItemScanUI
        saveItemScan: function()
        {
            var shipmentLinePickedModel = null;
            shipmentLinePickedModel = _scScreenUtils.getTargetModel(this, "ItemScan_Output", null);
            _isccsUIUtils.setWizardModel(this, "ShipmentLinePickedModel", shipmentLinePickedModel, null);
            var shipmemtLines = _scBaseUtils.getValueFromPath("Shipment.ShipmentLines.ShipmentLine", shipmentLinePickedModel);
            //Calculating the Total quantities picked up
            var i;
            var totalqty = 0;
            for (i = 0; i < shipmemtLines.length; i++)
            {
                var tempTotalQty = shipmemtLines[i].BackroomPickedQuantity;
                var totalqty = totalqty + tempTotalQty;
            }
            var warningString = null;
            if (_wscBackroomPickUtils.hasShortages(shipmentLinePickedModel))
            {
                warningString = _scScreenUtils.getString(this, "WARNING_Has_Shortages_Not_Resolved");
                _scScreenUtils.showErrorMessageBox(this, warningString, null, _wscBackroomPickUtils.getTextOkObjectForMessageBox(this), null);
            }
            else if (_wscBackroomPickUtils.hasOverages(shipmentLinePickedModel))
            {}
            else
            {
                //Calling the confirm pop-up for displaying the total quantities ordered
                var dialogParams = _scBaseUtils.getNewBeanInstance();
                _scBaseUtils.setAttributeValue("callBackHandler", "callbackPickedQuantityPopup", dialogParams);
                _isccsBaseTemplateUtils.showMessage(this, "To Proceed , please confirm if the picked quantity is correct (Qty : " + totalqty + ")", "confirm", dialogParams);
                //Commented the existing functionality
                //this.callMultiApiSave();
            }
        }
    });
});
