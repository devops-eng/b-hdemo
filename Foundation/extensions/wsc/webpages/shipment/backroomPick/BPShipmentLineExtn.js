scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/shipment/backroomPick/BPShipmentLineExtnUI", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!sc/plat/dojo/utils/BundleUtils", "scbase/loader!wsc/utils/BackroomPickUtils", "scbase/loader!wsc/utils/CustomerPickUpUtils", "scbase/loader!extn/utils/extnutils/CustomPickUtils", "scbase/loader!isccs/utils/UIUtils"], function(_dojodeclare, _extnBPShipmentLineExtnUI, _scBaseUtils, _scScreenUtils, _scWidgetUtils, _scModelUtils, scBundleUtils, _wscBackroomPickUtils, _wscCustomerPickUpUtils, _wscCustomPickUtils, _isccsUIUtils)
{
    return _dojodeclare("extn.shipment.backroomPick.BPShipmentLineExtn", [_extnBPShipmentLineExtnUI],
    {
        // Overriden method to handl the backroom pick shortage reasons
        initializeScreen: function(event, bEvent, ctrl, args)
        {
            var ShipmentLineModel = null;
            var shipmentkey = null;
            var shipmentDetails = null;
            ShipmentLineModel = _scScreenUtils.getModel(this, "ShipmentLine");
            var shipmentDetailModel = null
            shipmentDetailModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
            shipmentkey = ShipmentLineModel.ShipmentLine.ShipmentKey;
            _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentKey", shipmentkey, shipmentDetailModel);
            _isccsUIUtils.callApi(this, shipmentDetailModel, "extn_getshipmentDetails", null);
            var pickAll_Input = null;
            pickAll_Input = _scBaseUtils.getNewModelInstance();
            var backroomPickedQty = null;
            backroomPickedQty = _scModelUtils.getStringValueFromPath("ShipmentLine.BackroomPickedQuantity", ShipmentLineModel);
            this.pickedQuantity = backroomPickedQty;
            var shipmentQty = null;
            shipmentQty = _scModelUtils.getStringValueFromPath("ShipmentLine.Quantity", ShipmentLineModel);
            _scModelUtils.setStringValueAtModelPath("Quantity", backroomPickedQty, pickAll_Input);
            _scScreenUtils.setModel(this, "PickAll_Input", pickAll_Input, null);
            var shortageReasonData = null;
            shortageReasonData = _wscCustomPickUtils.getShortageReasonList();
            _scScreenUtils.setModel(this, "ShortageReasonData", shortageReasonData, null);
            _wscBackroomPickUtils.validatePickedQuantity(this, ShipmentLineModel, "productScanImagePanel", "productScanShortageImagePanel", "updateShortageResolutionImage", "shortageReasonFilterSelect");
            if (_wscBackroomPickUtils.isShipmentLinePreviouslyMarkedShortage(this, ShipmentLineModel, "productScanImagePanel", "productScanShortageImagePanel", "updateShortageResolutionImage", "shortageReasonFilterSelect", "txtScannedQuantity", "pickedQtylbl"))
            {
                var shortageReasonModel = null;
                var shipmentQty = null;
                shipmentQty = _scModelUtils.getStringValueFromPath("ShipmentLine.ShipmentLineKey", ShipmentLineModel);
                var NoteList = _scModelUtils.getStringValueFromPath("ShipmentLine.OrderLine.Notes.Note", ShipmentLineModel);
                for (i = 0; i < NoteList.length; i++)
                {
                    var noteText = NoteList[0].NoteText;
                    var shortageReasonCodeFromNotes = NoteList[0].ReasonCode;
                }
               
                shortageReasonModel = _scModelUtils.createNewModelObjectWithRootKey("ShortageReason");
                _scModelUtils.setStringValueAtModelPath("ShortageReason", shortageReasonCodeFromNotes, shortageReasonModel);
                _scScreenUtils.setModel(this, "ShortageReasonSourceData", shortageReasonModel, null);
                _scWidgetUtils.addClass(this, "shipmentLineDetailsContainer", "shortedLine");
            }
            if (_scBaseUtils.equals(this.screenMode, "ReadOnlyMobile"))
            {
                this.initializeReadOnlyMobileScreen();
            }
            if (_scBaseUtils.equals(this.screenMode, "ItemScanMobile"))
            {
                this.initializeItemScanMobileScreen();
            }
            if (_scBaseUtils.equals(this.screenMode, "ItemScan"))
            {
                this.initializeItemScanScreen();
            }
        },
        initializeReadOnlyMobileScreen: function() {},
        initializeItemScanMobileScreen: function() {},
        initializeItemScanScreen: function() {},
        handleFilterSelectShortageResolution: function(event, bEvent, ctrl, args)
        {
            if (_scBaseUtils.equals(event, "HBC_MC_PS_006"))
            {
                this.handleShortageResolution();
            }
            else if (_scBaseUtils.equals(event, "HBC_MC_PS_002"))
            {
                this.handleShortageResolution();
            }
            else if (_scBaseUtils.equals(event, "HBC_MC_PS_001"))
            {
                this.handleShortageResolution();
            }
            else if (_scBaseUtils.equals(event, "HBC_MC_PS_003"))
            {
                this.handleShortageResolution();
            }
            else if (_scBaseUtils.equals(event, "HBC_MC_PS_004"))
            {
                this.handleShortageResolution();
            }
            else if (_scBaseUtils.equals(event, "HBC_MC_PS_005"))
            {
                this.handleShortageResolution();
            }
            else if (_scBaseUtils.equals(event, "HBC_MC_PS_015"))
            {
                this.handleShortageResolution();
            }
            else if (_scBaseUtils.equals(event, "HBC_MC_PS_007"))
            {
                this.handleShortageResolution();
            }
            else if (_scBaseUtils.equals(event, "HBC_MC_PS_008"))
            {
                this.handleShortageResolution();
            }
            else if (_scBaseUtils.equals(event, "HBC_MC_PS_009"))
            {
                this.handleShortageResolution();
            }
            else
            {
                this.shortageResolved = false;
            }
        },
        //Merged code start from here
        updateShortageResolution: function(res, args)
        {
            var shortageReasonModel = null;
            shortageReasonModel = _scModelUtils.createNewModelObjectWithRootKey("ShortageReason");
            var targetModel = null;
            targetModel = _scBaseUtils.getTargetModel(this, "PickAll_Output", null);
            var shortageReason = null;
            shortageReason = _scModelUtils.getStringValueFromPath("ShortageReason", targetModel);
            if (_scBaseUtils.equals(res, "Ok"))
            {
				//Replaced the Hard coded shortage Reason with the User selected Reason.
                _scModelUtils.setStringValueAtModelPath("ShortageReason", shortageReason, shortageReasonModel);
                _scScreenUtils.setModel(this, "ShortageReasonSourceData", shortageReasonModel, null);
                this.callMultiApiShortageUpdate(args);
            }
            else
            {
                this.shortageResolved = false;
                _scModelUtils.setStringValueAtModelPath("ShortageReason", "", shortageReasonModel);
                _scScreenUtils.setModel(this, "ShortageReasonSourceData", shortageReasonModel, null);
            }
        },
		//Overridden Method which will call multi APIS for updating the Shortage Reasons
        callMultiApiShortageUpdate: function(args)
        {
            var mashupRefIdList = null;
            var mashupInputModelList = null;
            mashupRefIdList = _scBaseUtils.getNewArrayInstance();
            mashupInputModelList = _scBaseUtils.getNewArrayInstance();
            var shipmentLinePickedModel = null;
            shipmentLinePickedModel = _scScreenUtils.getTargetModel(this, "ShipmentLine_Output", null);
            var updateShortageModel = null;
            updateShortageModel = _wscBackroomPickUtils.getShortageChangeShipmentModel(shipmentLinePickedModel, args);
            _scBaseUtils.appendToArray(mashupRefIdList, "updateShortageLineToShipment");
            _scBaseUtils.appendToArray(mashupInputModelList, updateShortageModel);
            var orderNotes = null;
			var orderLineCancel=null;
			// method is called for getting the ReasonCode and NoteText.
            orderNotes = this.getNotesForShortageShipmentLine(shipmentLinePickedModel);
			_scBaseUtils.appendToArray(mashupRefIdList, "addShortageReasonNote");
            _scBaseUtils.appendToArray(mashupInputModelList, orderNotes);
			//Method is called for forming the input to cancel the orderline which are backordered
			orderLineCancel = this.getOrderCancelLineInput(shipmentLinePickedModel);
			_scBaseUtils.appendToArray(mashupRefIdList, "extn_HBCCancelShipmentFromSOM");
            _scBaseUtils.appendToArray(mashupInputModelList, orderLineCancel);
            _isccsUIUtils.callApis(this, mashupInputModelList, mashupRefIdList, null);
        },
		//Method which will add the ReasonCode and NoteText in YFS_NOTES table
        getNotesForShortageShipmentLine: function(pickedShipmentLineModel)
        {
            var ShortageReason = _scModelUtils.getStringValueFromPath("ShipmentLine.ShortageReason", pickedShipmentLineModel);
            var ShipmentKey = _scModelUtils.getStringValueFromPath("ShipmentLine.ShipmentLineKey", pickedShipmentLineModel);
            var pickedLine = pickedShipmentLineModel.ShipmentLine;
            var orderNotes = {};
            var order = {};
            order.OrderHeaderKey = pickedLine.OrderHeaderKey;
            order.OrderLines = {};
            order.OrderLines.OrderLine = [];
            order.OrderLines.OrderLine[0] = {};
            order.OrderLines.OrderLine[0].OrderLineKey = pickedLine.OrderLineKey;
            order.OrderLines.OrderLine[0].Notes = {};
            order.OrderLines.OrderLine[0].Notes.Note = [];
            order.OrderLines.OrderLine[0].Notes.Note[0] = {};
            //order.OrderLines.OrderLine[0].Notes.Note[0].NoteText="A shortage was detected while performing a backroom pick. The shortage resolution was recorded as:"+ShortageReason+"For Shipment Line Key :"+ ShipmentKey
            order.OrderLines.OrderLine[0].Notes.Note[0].NoteText = "A shortage was detected while performing a backroom pick . The shortage resolution was recorded as:" + scBundleUtils.getString(ShortageReason);
                //order.OrderLines.OrderLine[0].Notes.Note[0].NoteText=scBundleUtils.getString("Inventory_Shortage_OrderLine_Note");				
            order.OrderLines.OrderLine[0].Notes.Note[0].ReasonCode = ShortageReason;
            orderNotes.Order = order;
            return orderNotes;
        },
		
	  //Custom method to form the input data to cancel the orderline to backorder
        getOrderCancelLineInput: function(pickedShipmentLineModel)
        {
            var ShortageReason = _scModelUtils.getStringValueFromPath("ShipmentLine.ShortageReason", pickedShipmentLineModel);
            var ShipmentKey = _scModelUtils.getStringValueFromPath("ShipmentLine.ShipmentLineKey", pickedShipmentLineModel);
            var pickedLine = pickedShipmentLineModel.ShipmentLine;
            
            var order  = {};
            order.OrderHeaderKey = pickedLine.OrderHeaderKey;
            order.OrderLines = {};
            order.OrderLines.OrderLine = [];
            order.OrderLines.OrderLine[0] = {};
			order.OrderLines.OrderLine[0].ReasonCode={};
            order.OrderLines.OrderLine[0].OrderLineKey = pickedLine.OrderLineKey;
            order.OrderLines.OrderLine[0].ReasonCode = scBundleUtils.getString(ShortageReason);
             var input={};
			input.Input={};
			input.Input.Order=order;
            return input
        },	  
     //custome method to get the details of SCAC,Service
        extn_BpShipmentLine_handleMashup: function(UIEvent, bEvent, control, args)
        {
            var mashupRefId = null;
            var modelOutput = null;
            var mashupRefOutput = null;
            var creditcardLastDigits = null;
            var shipmetLines = null;
            var paymentMethods = null;
            var mashupObject = args.mashupArray;
            if (!_scBaseUtils.isVoid(mashupObject))
            {
                for (var i = 0; i < mashupObject.length; i++)
                {
                    mashupRefOutput = mashupObject[i].mashupRefOutput;
                    mashupRefId = mashupObject[i].mashupRefId
                    if (_scBaseUtils.equals(mashupRefId, "extn_getshipmentDetails"))
                    {
                        _scScreenUtils.setModel(this, "extn_BPLine_ShipmentDetails", mashupRefOutput, null);
                        break;
                    }
                }
            }
        },
        extn_getSCaC: function(event, bEvent, ctrl, args)
        {
            var shipmentDetailModel = null;
            var Scac = null;
            shipmentDetailModel = _scScreenUtils.getModel(this, "extn_BPLine_ShipmentDetails");
			var shipments = shipmentDetailModel.Shipments;
			for (var j = 0; j< shipments.length; i++)
			{
			   Scac = shipments[i].SCAC
			}
          
            return Scac;
        },
        extn_getSCaCAndService: function(event, bEvent, ctrl, args)
        {
            var shipmentDetailModel = null;
            var service = null;
            shipmentDetailModel = _scScreenUtils.getModel(this, "extn_BPLine_ShipmentDetails");
			var shipments = shipmentDetailModel.Shipments;
			for (var k = 0; k< shipments.length; k++)
			{
			   service = shipments[k].ScacAndService
			}
           
            return service;
        },
        extn_TrackingNo: function(event, bEvent, ctrl, args)
        {
            var shipmentDetailModel = null;
            var trackingNo = null;
            shipmentDetailModel = _scScreenUtils.getModel(this, "extn_BPLine_ShipmentDetails");
			var shipments = shipmentDetailModel.Shipments;
			for (var l = 0; l< shipments.length; l++)
			{
			   trackingNo = shipments[l].TrackingNo
			}
          
            return trackingNo;
        }
    });
});
