	scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/shipment/customerpickup/CustomerIdentificationExtnUI", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!isccs/utils/UIUtils", "scbase/loader!isccs/utils/BaseTemplateUtils"], function(_dojodeclare, _extnCustomerIdentificationExtnUI, _scBaseUtils, _scWidgetUtils, _scEventUtils, _scModelUtils, _scScreenUtils, _isccsUIUtils, _isccsBaseTemplateUtils) {
	    return _dojodeclare("extn.shipment.customerpickup.CustomerIdentificationExtn", [
	        _extnCustomerIdentificationExtnUI
	    ], {
	        // custom code to hide/unhide the widget of credit card number text field based on the drop down values from verificatin screen
	        handleSelectVerificationMethod: function(event, bEvent, ctrl, args) {
	            if (_scBaseUtils.equals(event, "Credit Card Last Four Digits")) {
	                _scWidgetUtils.showWidget(this, "extn_credit_card", false, null);
	            } else {
	                _scWidgetUtils.hideWidget(this, "extn_credit_card", false, null);
	            }
	        },
	        //Custom code to validate the credit card last four digits entered in the custmer verificaion screen
	        //validating the credit card last four digts and preparing the input to the getshipemntList API
	        extn_validateInput: function(UIEvent, bEvent, control, args) {
	            var inputModel = null;
	            inputModel = _scScreenUtils.getTargetModel(this, "extn_customer_creditcard_input", null);
	            var inputtxt = null
	            inputtxt = _scModelUtils.getStringValueFromPath("CustomerPickUp.CustomerIdentification.CreditCardDigits", inputModel);
	            var shipmentDetails = null;
	            shipmentDetails = _scScreenUtils.getModel(this, "getCustomerPickupOrder_Source");
	            var shipmentNo = null;
	            var shipNode = null;
	            var sellerOrganizationCode = null;
	            shipmentNo = _scModelUtils.getStringValueFromPath("Shipment.ShipmentNo", shipmentDetails);
	            shipNode = _scModelUtils.getStringValueFromPath("Shipment.ShipNode", shipmentDetails);
	            sellerOrganizationCode = _scModelUtils.getStringValueFromPath("Shipment.SellerOrganizationCode", shipmentDetails);
	            var shipmentDetailModel = null
	            shipmentDetailModel = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
	            _scModelUtils.setStringValueAtModelPath("Shipment.ShipmentNo", shipmentNo, shipmentDetailModel);
	            _scModelUtils.setStringValueAtModelPath("Shipment.ShipNode", shipNode, shipmentDetailModel);
	            _scModelUtils.setStringValueAtModelPath("Shipment.SellerOrganizationCode", sellerOrganizationCode, shipmentDetailModel);
	            _isccsUIUtils.callApi(this, shipmentDetailModel, "extn_creditCardMashup", null);
	        },
	        //custom method to validate the output of the credit card last four digits against the input
	        extn_handleMashupOutput: function(UIEvent, bEvent, control, args) {
	            var mashupRefId = null;
	            var modelOutput = null;
	            var mashupRefOutput = null;
	            var creditcardLastDigits = null;
	            var shipmetLines = null;
	            var paymentMethods = null;
	            var mashupObject = args.mashupArray;
	            var inputModel = null;
	            inputModel = _scScreenUtils.getTargetModel(this, "extn_customer_creditcard_input", null);
	            var inputtxt = null
	            inputtxt = _scModelUtils.getStringValueFromPath("CustomerPickUp.CustomerIdentification.CreditCardDigits", inputModel);
	            if (!_scBaseUtils.isVoid(mashupObject)) {
	                for (var i = 0; i < mashupObject.length; i++) {
	                    mashupRefOutput = mashupObject[i].mashupRefOutput;
	                    mashupRefId = mashupObject[i].mashupRefId
	                }
	                shipmetLines = mashupRefOutput.Shipment.ShipmentLines.ShipmentLine;
	                if (!_scBaseUtils.isVoid(shipmetLines)) {
	                    for (var j = 0; j < shipmetLines.length; j++) {
	                        paymentMethods = shipmetLines[j].Order.PaymentMethods.PaymentMethod
	                        if (!_scBaseUtils.isVoid(paymentMethods)) {
	                            for (var k = 0; k < paymentMethods.length; k++) {
	                                creditcardLastDigits = paymentMethods[k].DisplayCreditCardNo;
	                                if (!_scBaseUtils.isVoid(creditcardLastDigits)) {
	                                    if (!_scBaseUtils.equals(inputtxt, creditcardLastDigits)) {
	                                        _isccsBaseTemplateUtils.showMessage(this, "Invalid Credit Card Number", "error", null);
	                                        break;
	                                    }
	                                }
	                            }
	                        }
	                    }
	                }
	            }
	        }
	    });
	});
