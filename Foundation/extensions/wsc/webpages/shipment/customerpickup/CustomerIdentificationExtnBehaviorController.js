scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/text", "scbase/loader!extn/shipment/customerpickup/CustomerIdentificationExtn", "scbase/loader!sc/plat/dojo/controller/ExtnServerDataController"], function(_dojodeclare, _dojokernel, _dojotext, _extnCustomerIdentificationExtn, _scExtnServerDataController) {
    return _dojodeclare("extn.shipment.customerpickup.CustomerIdentificationExtnBehaviorController", [_scExtnServerDataController], {
        screenId: 'extn.shipment.customerpickup.CustomerIdentificationExtn',
        mashupRefs: [{
            extnType: 'ADD',
            mashupId: 'customerIdentification_getShipmentDetails',
            mashupRefId: 'extn_creditCardMashup'
        }]
    });
});
