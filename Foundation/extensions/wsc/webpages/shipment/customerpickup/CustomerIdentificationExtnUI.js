scDefine(["dojo/text!./templates/CustomerIdentificationExtn.html", "scbase/loader!dojo/_base/declare", "scbase/loader!dojo/_base/kernel", "scbase/loader!dojo/_base/lang", "scbase/loader!dojo/text", "scbase/loader!idx/form/FilteringSelect", "scbase/loader!idx/form/TextBox", "scbase/loader!idx/layout/ContentPane", "scbase/loader!sc/plat", "scbase/loader!sc/plat/dojo/binding/ComboDataBinder", "scbase/loader!sc/plat/dojo/binding/SimpleDataBinder", "scbase/loader!sc/plat/dojo/utils/BaseUtils"], function(templateText, _dojodeclare, _dojokernel, _dojolang, _dojotext, _idxFilteringSelect, _idxTextBox, _idxContentPane, _scplat, _scComboDataBinder, _scSimpleDataBinder, _scBaseUtils) {
    return _dojodeclare("extn.shipment.customerpickup.CustomerIdentificationExtnUI", [], {
        templateString: templateText,
        namespaces: {
            targetBindingNamespaces: [{
                value: 'extn_customer_creditcard_input',
                scExtensibilityArrayItemId: 'extn_TargetNamespaces_2',
                description: 'customer credit card '
            }],
            sourceBindingNamespaces: [{
                value: 'extn_getCreditCardDetails',
                scExtensibilityArrayItemId: 'extn_SourceNamespaces_2',
                description: 'Getting credit card details'
            }, {
                value: 'extn_customerIdent_getOrderDetails_Output',
                scExtensibilityArrayItemId: 'extn_SourceNamespaces_3'
            }]
        },
        hotKeys: [],
        events: [],
        subscribers: {
            local: [{
                eventId: 'cmbCustVerfMethod_onChange',
                sequence: '51',
                handler: {
                    methodName: "handleSelectVerificationMethod"
                }
            }, {
                eventId: 'extn_credit_card_onBlur',
                sequence: '51',
                handler: {
                    methodName: "extn_validateInput"
                }
            }, {
                eventId: 'onExtnMashupCompletion',
                sequence: '51',
                handler: {
                    methodName: "extn_handleMashupOutput"
                }
            }],
        }
    });
});
