scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/shipment/customerpickup/PickupOrderDetailsExtnUI", "scbase/loader!wsc/utils/CustomerPickUpUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!extn/utils/extnutils/CustomerPickUpUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!wsc/utils/CustomerPickUpUtils", "scbase/loader!isccs/utils/UIUtils"], function(_dojodeclare, _extnPickupOrderDetailsExtnUI, _wscCustomerPickUpUtils, _scScreenUtils, customerPickupUtils, _scModelUtils, _scBaseUtils, _scWidgetUtils, _wscCustomerPickUpUtils, _isccsUIUtils) {
    return _dojodeclare("extn.shipment.customerpickup.PickupOrderDetailsExtn", [_extnPickupOrderDetailsExtnUI], {
        initializeScreen: function(event, bEvent, ctrl, args) {
            if (_scBaseUtils.isBooleanTrue(this.isInfoHidden)) {
                _scWidgetUtils.hideWidget(this, "pnlSecondaryInfo", false);
                _scWidgetUtils.hideWidget(this, "orderPickedQtyInfo", false);
            }
            var formattedValue = null;
            var inputArray = null;
            var shipmentDetailModel = null;
            shipmentDetailModel = _scScreenUtils.getModel(this, "Shipment");
            if (_scBaseUtils.equals(this.screenMode, "CustomerPickupOrderSummary")) {
                _scWidgetUtils.hideTabPanel(this, "orderPickedQtyInfo", "pnlReady_for_Pickup");
                _scWidgetUtils.showWidget(this, "pnlSecondaryInfo", false, null);
                _scWidgetUtils.showWidget(this, "orderPickedQtyInfo", false, null);
                if (!(_wscCustomerPickUpUtils.hasRemainingShipmentLines(shipmentDetailModel))) {
                    _scWidgetUtils.hideTabPanel(this, "orderPickedQtyInfo", "pnlRemaining_Products");
                }
                if (!(_wscCustomerPickUpUtils.hasCancelShipmentLines(shipmentDetailModel))) {
                    _scWidgetUtils.hideTabPanel(this, "orderPickedQtyInfo", "pnlcancelProducts");
                }
            } else if (_scBaseUtils.equals(this.screenMode, "CustomerPickupOrderVerification")) {
                _scWidgetUtils.hideTabPanel(this, "orderPickedQtyInfo", "pnlIncluded");
                _scWidgetUtils.hideTabPanel(this, "orderPickedQtyInfo", "pnlRemaining_Products");
                _scWidgetUtils.hideTabPanel(this, "orderPickedQtyInfo", "pnlcancelProducts");
            } else if (_scBaseUtils.equals(this.screenMode, "ShipmentSummary")) {
                _scWidgetUtils.hideTabPanel(this, "orderPickedQtyInfo", "pnlReady_for_Pickup");
            }
            var originalShipmentKey = null;
            originalShipmentKey = _scModelUtils.getStringValueFromPath("Shipment.OriginalShipmentKey", shipmentDetailModel);
            if (_scBaseUtils.isVoid(originalShipmentKey)) {
                _scWidgetUtils.hideTabPanel(this, "orderPickedQtyInfo", "pnlPreviously_PickedUp");
            }
            //customized to prepare the input API and get the orderdetails
            var OrderNo = null;
            var EnterpriseCode = null;
            var documentType = null;
            EnterpriseCode = shipmentDetailModel.Shipment.EnterpriseCode;
            if (!_scBaseUtils.isVoid(shipmentDetailModel.Shipment.ShipmentLines.ShipmentLine[0].OrderNo)) {
                OrderNo = shipmentDetailModel.Shipment.ShipmentLines.ShipmentLine[0].OrderNo;
            }
            var OrderDetailsModel = null;
            documentType = shipmentDetailModel.Shipment.ShipmentLines.ShipmentLine[0].Order.DocumentType;
            OrderDetailsModel = _scModelUtils.createNewModelObjectWithRootKey("Order");
            _scModelUtils.setStringValueAtModelPath("Order.OrderNo", OrderNo, OrderDetailsModel);
            _scModelUtils.setStringValueAtModelPath("Order.EnterpriseCode", EnterpriseCode, OrderDetailsModel);
            _scModelUtils.setStringValueAtModelPath("Order.DocumentType", documentType, OrderDetailsModel);
            _isccsUIUtils.callApi(this, OrderDetailsModel, "extn_pickuporder_getOrderDetails", null);
        },
        extn_hanldeMashupCompletion: function(UIEvent, bEvent, control, args) {
            var mashupRefId = null;
            var modelOutput = null;
            var mashupRefOutput = null;
            var mashupObject = null;
            mashupObject = args.mashupArray;
            if (!_scBaseUtils.isVoid(mashupObject)) {
                for (var i = 0; i < mashupObject.length; i++) {
                    mashupRefOutput = mashupObject[i].mashupRefOutput;
                    mashupRefId = mashupObject[i].mashupRefId
                }
                if (_scBaseUtils.equals(mashupRefId, "extn_pickuporder_getOrderDetails")) {
                    _scScreenUtils.setModel(this, "extn_pickuporder_Output", mashupRefOutput, null);
                }
            }
        },
        //custom code to display the notes section in customer pickup
        extn_NotesDisplay: function(event, bEvent, ctrl, args) {
            var shipmentDetailModel = null;
            shipmentDetailModel = _scScreenUtils.getModel(this, "Shipment");
            var customerVerificationNotesModel = null;
            var OrderDetailsModel = null;
            OrderDetailsModel = _scScreenUtils.getModel(this, "extn_pickuporder_Output");
            customerVerificationNotesModel = _scScreenUtils.getTargetModel(this, "CustomerVerificationNotes_Input", null);
            customerVerificationNotesModel = customerPickupUtils.massageCustVerificationNotesModel(this, customerVerificationNotesModel, shipmentDetailModel, OrderDetailsModel);
            return customerVerificationNotesModel.Notes.Note.NoteText;
        }
    });
});
