
scDefine(["dojo/text!./templates/PickupOrderDetailsExtn.html","scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/_base/lang","scbase/loader!dojo/text","scbase/loader!idx/form/Textarea","scbase/loader!idx/layout/ContentPane","scbase/loader!sc/plat","scbase/loader!sc/plat/dojo/binding/SimpleDataBinder","scbase/loader!sc/plat/dojo/utils/BaseUtils"]
 , function(			 
			    templateText
			 ,
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojolang
			 ,
			    _dojotext
			 ,
			    _idxTextarea
			 ,
			    _idxContentPane
			 ,
			    _scplat
			 ,
			    _scSimpleDataBinder
			 ,
			    _scBaseUtils
){
return _dojodeclare("extn.shipment.customerpickup.PickupOrderDetailsExtnUI",
				[], {
			templateString: templateText
	
	
	
	
	
	
					,	
	namespaces : {
		targetBindingNamespaces :
		[
		],
		sourceBindingNamespaces :
		[
			{
	  value: 'extn_getCustomerVerificaiton_Notes'
						,
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_2'
						
			}
			,
			{
	  value: 'extn_pickuporder_Output'
						,
	  scExtensibilityArrayItemId: 'extn_SourceNamespaces_3'
						
			}
			
		]
	}

	

	,
	hotKeys: [ 
	]

,events : [
	]

,subscribers : {
local : [

{
	  eventId: 'onExtnMashupCompletion'

,	  sequence: '51'




,handler : {
methodName : "extn_hanldeMashupCompletion"

 
 
}
}

]
					 	 ,
}

});
});


