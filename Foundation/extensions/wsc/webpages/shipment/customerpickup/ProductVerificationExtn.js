scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/shipment/customerpickup/ProductVerificationExtnUI", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!isccs/utils/EventUtils", "scbase/loader!isccs/utils/UIUtils", "scbase/loader!wsc/utils/CustomerPickUpUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!sc/plat/dojo/utils/EventUtils", "scbase/loader!sc/plat/dojo/utils/EditorUtils", "scbase/loader!isccs/utils/BaseTemplateUtils"], function(_dojodeclare, _extnProductVerificationExtnUI, _scBaseUtils, _scScreenUtils, _isccsEventUtils, _isccsUIUtils, _wscCustomerPickUpUtils, _scModelUtils, _scEventUtils, _scEditorUtils, _isccsBaseTemplateUtils)
{
    return _dojodeclare("extn.shipment.customerpickup.ProductVerificationExtn", [_extnProductVerificationExtnUI],
    {
        // Overriden CallHandler method
        callbackPickedQuantityPopup: function(actionPerformed, model, popupParams)
        {
            var action = actionPerformed;
            if (action == "Ok")
            {
                this.gotoNextScreen();
            }
        },
        //Overriden save function from ProductVerificationUI
        save: function(event, bEvent, ctrl, args)
        {
            var pickUpOrderLineVerificationModel = null;
            var ShortageResolutionModel = null;
            var args = null;
            var initialInputData = null;
            var test = null;
            args = _scBaseUtils.getNewBeanInstance();
            test = _isccsUIUtils.getEditorFromScreen(this);
            initialInputData = _scScreenUtils.getInitialInputData(_isccsUIUtils.getEditorFromScreen(this));
            pickUpOrderLineVerificationModel = _scScreenUtils.getTargetModel(this, "ItemScan_Output", null);
            _isccsUIUtils.setWizardModel(this, "pickUpOrderLinesResolution_input", pickUpOrderLineVerificationModel, null);
            ShortageResolutionModel = _wscCustomerPickUpUtils.getShortageResolutionModel(pickUpOrderLineVerificationModel);
            //Calculating the total Quantities picked up
            var shipmentLines = _scBaseUtils.getValueFromPath("ShipmentLines.ShipmentLine", pickUpOrderLineVerificationModel);
            var i;
            var totalqty = 0;
            for (i = 0; i < shipmentLines.length; i++)
            {
                var tempTotalQty = parseInt(shipmentLines[i].PickedQty);
                totalqty = totalqty + tempTotalQty;
            }
            var hasShortages = null;
            hasShortages = _scModelUtils.getStringValueFromPath("hasShortages", ShortageResolutionModel);
            var hasConfirmed = null;
            hasConfirmed = _scModelUtils.getStringValueFromPath("hasConfirmed", ShortageResolutionModel);
            var hasShipmentsWithNoPickItems = null;
            hasShipmentsWithNoPickItems = _scModelUtils.getStringValueFromPath("hasShipmentsWithNoPickItems", ShortageResolutionModel);
            var hasCancelledLines = null;
            hasCancelledLines = _scModelUtils.getStringValueFromPath("hasCancelledLines", ShortageResolutionModel);
            if (_scBaseUtils.equals(hasShortages, "Y"))
            {
                _isccsUIUtils.setWizardModel(this, "ShortageResolutionModel", ShortageResolutionModel, null);
                _scEventUtils.fireEventToParent(this, "GoToResolution", args);
                initialInputData = _wscCustomerPickUpUtils.addModelToModelPath("ShortageResolutionModel", ShortageResolutionModel, initialInputData);
                initialInputData = _wscCustomerPickUpUtils.appendModelToInitialInputData(this, initialInputData);
                _scBaseUtils.setAttributeValue("argumentList.model", initialInputData, args);
                var wizardInstance = null;
                wizardInstance = _isccsUIUtils.getCurrentWizardInstance(_scEditorUtils.getCurrentEditor());
                _scScreenUtils.setInitialInputData(wizardInstance, initialInputData);
                //to show confirm popup for displaying the total quantities picked up
                var dialogParams = _scBaseUtils.getNewBeanInstance();
                _scBaseUtils.setAttributeValue("callBackHandler", "callbackPickedQuantityPopup", dialogParams);
                _isccsBaseTemplateUtils.showMessage(this, "To Proceed , please confirm if the picked quantity is correct (Qty : " + totalqty + ")", "confirm", dialogParams);
                //commented the existing functionality starts
                //this.gotoNextScreen();
                //commented the existing functionality stops
            }
            else
            {
                _isccsUIUtils.setWizardModel(this, "ShortageResolutionModel_Resolved", ShortageResolutionModel, null);
                _scEventUtils.fireEventToParent(this, "GoToSummary", args);
                initialInputData = _wscCustomerPickUpUtils.addModelToModelPath("ShortageResolutionModel_Resolved", ShortageResolutionModel, initialInputData);
                initialInputData = _wscCustomerPickUpUtils.appendModelToInitialInputData(this, initialInputData);
                _scBaseUtils.setAttributeValue("argumentList.model", initialInputData, args);
                var wizardInstance = null;
                wizardInstance = _isccsUIUtils.getCurrentWizardInstance(_scEditorUtils.getCurrentEditor());
                _scScreenUtils.setInitialInputData(wizardInstance, initialInputData);
                if (_scBaseUtils.equals(hasCancelledLines, "Y"))
                {
                    this.updateCancelReasonCode();
                }
                else
                {
                    //to show confirm popup for displaying the total quantities picked up
                    var dialogParams = _scBaseUtils.getNewBeanInstance();
                    _scBaseUtils.setAttributeValue("callBackHandler", "callbackPickedQuantityPopup", dialogParams);
                    _isccsBaseTemplateUtils.showMessage(this, "To Proceed , please confirm if the picked quantity is correct (Qty : " + totalqty + ")", "confirm", dialogParams);
                    //commented the existing functionality starts
                    //this.gotoNextScreen();
                    //commented the existing functionality stops
                }
            }
        }
    });
});
