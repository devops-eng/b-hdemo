scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/shipment/details/ShipmentLineDetailsExtnUI", "scbase/loader!sc/plat/dojo/utils/ScreenUtils", "scbase/loader!isccs/utils/BaseTemplateUtils", "scbase/loader!isccs/utils/UIUtils", "scbase/loader!sc/plat/dojo/utils/WidgetUtils", "scbase/loader!sc/plat/dojo/utils/BaseUtils", "scbase/loader!sc/plat/dojo/utils/ModelUtils"], function(_dojodeclare, _extnShipmentLineDetailsExtnUI, _scScreenUtils, _isccsBaseTemplateUtils, _isccsUIUtils, _scWidgetUtils, _scBaseUtils, _scModelUtils)
{
    return _dojodeclare("extn.shipment.details.ShipmentLineDetailsExtn", [_extnShipmentLineDetailsExtnUI],
    {
        // Overriden method to populate the prdouct color, size and UPC code
        initScreen: function(event, bEvent, ctrl, args)
        {
            var parentScreen = null;
            parentScreen = _isccsUIUtils.getParentScreen(this, false);
            var hasParent = false;
            if (!(_scScreenUtils.isPopup(this)))
            {
                _scWidgetUtils.hideWidget(this, "Popup_navigationPanel", false);
            }
            else if (!(_scBaseUtils.isVoid(parentScreen)))
            {
                if (_isccsUIUtils.isScreenRef(this))
                {
                    _scWidgetUtils.hideWidget(this, "Popup_navigationPanel", false);
                }
                else
                {
                    _scWidgetUtils.hideWidget(this, "titleMessagePanel", false);
                    _isccsBaseTemplateUtils.setPopupStyle(this);
                }
            }
            else
            {
                _scWidgetUtils.hideWidget(this, "titleMessagePanel", false);
                _isccsBaseTemplateUtils.setPopupStyle(this);
            }
            var extn_ShipmentLine = null;
            var shipmentLineDetails = null;
            extn_ShipmentLine = _scBaseUtils.getNewModelInstance();
            shipmentLineDetails = _scScreenUtils.getModel(this, "ShipmentLine", null);
            var additionalAttributes = shipmentLineDetails.ShipmentLine.OrderLine.ItemDetails.AdditionalAttributeList.AdditionalAttribute;
            var itemAliass = shipmentLineDetails.ShipmentLine.OrderLine.ItemDetails.ItemAliasList.ItemAlias;
            for (var iIttemAliass in itemAliass)
            {
                if (itemAliass[iIttemAliass].AliasName == "UPC")
                {
                    _scModelUtils.setStringValueAtModelPath("ShipmentLine.Upc", itemAliass[iIttemAliass].AliasValue, extn_ShipmentLine);
                }
            }
            for (var addAttrObj in additionalAttributes)
            {
                if (additionalAttributes[addAttrObj].Name == "Color")
                {
                    _scModelUtils.setStringValueAtModelPath("ShipmentLine.Color", additionalAttributes[addAttrObj].Value, extn_ShipmentLine);
                }
                if (additionalAttributes[addAttrObj].Name == "Size")
                {
                    _scModelUtils.setStringValueAtModelPath("ShipmentLine.Size", additionalAttributes[addAttrObj].Value, extn_ShipmentLine);
                }
            }
            _scScreenUtils.setModel(this, "extn_ShipmentLine", extn_ShipmentLine, null);
            return true;
        }
    });
});
