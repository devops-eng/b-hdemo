scDefine(["scbase/loader!dojo/_base/declare", "scbase/loader!extn/shipment/details/ShipmentSummaryExtnUI", "scbase/loader!wsc/utils/CustomerPickUpUtils"], function(_dojodeclare, _extnShipmentSummaryExtnUI, _wscCustomerPickUpUtils)
{
    return _dojodeclare("extn.shipment.details.ShipmentSummaryExtn", [
        _extnShipmentSummaryExtnUI
    ],
    {
        // Calling the Utils method to populate the order number in shipment summary
        computeSalesOrderNo: function(dataValue, screen, widget, nameSpace, shipmentModel)
        {
            var salesOrderNo = null;
            salesOrderNo = _wscCustomerPickUpUtils.computeSalesOrderNo(this, shipmentModel);
            return  salesOrderNo;
        }
    });
});
