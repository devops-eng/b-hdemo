


scDefine(["scbase/loader!dojo/_base/declare","scbase/loader!dojo/_base/kernel","scbase/loader!dojo/text","scbase/loader!extn/shipment/details/ShipmentSummaryExtn","scbase/loader!sc/plat/dojo/controller/ExtnScreenController"]
 , function(			 
			    _dojodeclare
			 ,
			    _dojokernel
			 ,
			    _dojotext
			 ,
			    _extnShipmentSummaryExtn
			 ,
			    _scExtnScreenController
){

return _dojodeclare("extn.shipment.details.ShipmentSummaryExtnInitController", 
				[_scExtnScreenController], {

			
			 screenId : 			'extn.shipment.details.ShipmentSummaryExtn'

			
			
			
			
			
						,

			
			
			 mashupRefs : 	[
	 		{
		 sourceNamespace : 			'getShipmentDetails_output'
,
		 mashupRefId : 			'getShipmentDetails'
,
		 sequence : 			''
,
		 mashupId : 			'shipmentSummary_getShipmentDetails'
,
		 callSequence : 			''
,
		 extnType : 			''
,
		 sourceBindingOptions : 			''

	}

	]

}
);
});

