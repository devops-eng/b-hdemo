scDefine(["scbase/loader!dojo/_base/declare",
           "scbase/loader!extn/shipment/search/ShipmentSearchExtnUI",
		   "scbase/loader!sc/plat/dojo/utils/ScreenUtils", 
		   "scbase/loader!sc/plat/dojo/utils/BaseUtils", 
		   "scbase/loader!sc/plat/dojo/utils/ModelUtils", 
		   "scbase/loader!isccs/utils/BaseTemplateUtils", 
		   "scbase/loader!sc/plat/dojo/utils/EventUtils"],
		   function(_dojodeclare, 
		   _extnShipmentSearchExtnUI, 
		   scScreenUtils,
		   _scBaseUtils, 
		   scModelUtils,
		   _isccsBaseTemplateUtils,
		   scEventUtils)
{
    return _dojodeclare("extn.shipment.search.ShipmentSearchExtn", [_extnShipmentSearchExtnUI],
    {
        // custom code to find out the results based on the filed "order not picked by no.of days
		//Validating the input and converting the no.of days to Date and setting the model inputs
		//and finally calling the API through event to get the results
        SST_search: function()
        {
            var targetModel = null;
            var tempModel = null;
            if (!(scScreenUtils.isValid(this, this.SST_getSearchNamespace())))
            {
                _isccsBaseTemplateUtils.showMessage(this, "InvalidSearchCriteria", "error", null);
                return;
            }
            else
            {
                _isccsBaseTemplateUtils.hideMessage(this);
                targetModel = _scBaseUtils.getTargetModel(this, this.SST_getSearchNamespace(), null);
                var noOfDays = targetModel.Shipment.ExtnNoOfDays;
                if (noOfDays != null)
                {
                    var today = new Date();
                    today.setDate(today.getDate() - noOfDays);
                    scModelUtils.setStringValueAtModelPath("Shipment.RequestedShipmentDateQryType", "GE", targetModel);
                    scModelUtils.setStringValueAtModelPath("Shipment.RequestedShipmentDate", today, targetModel);
                }
                if (_scBaseUtils.equals(this.SST_getSearchNamespace(), "getAdvancedShipmentListWithGiftRct_input"))
                {
                    tempModel = scModelUtils.getModelObjectFromPath("Shipment.OrderBy", scScreenUtils.getTargetModel(this, "getAdvancedShipmentList_input", null));
                    scModelUtils.addModelToModelPath("Shipment.OrderBy", tempModel, targetModel);
                }
            }
            _scBaseUtils.removeBlankAttributes(targetModel);
            var eventDefn = null;
            var args = null;
            eventDefn = _scBaseUtils.getNewBeanInstance();
            args = _scBaseUtils.getNewBeanInstance();
            _scBaseUtils.setAttributeValue("inputData", targetModel, args);
            _scBaseUtils.setAttributeValue("argumentList", args, eventDefn);
            scEventUtils.fireEventToChild(this, "shipmentSearchResult", "callListApi", eventDefn);
        },
		//Overrident the method to set the default values to be selected in Advance shipment search 
        setShipmentStatusDefaultValues: function(screenInput)
        {
            var shipmentElem = null;
            if (_scBaseUtils.isVoid(screenInput))
            {
                screenInput = _scModelUtils.createNewModelObjectWithRootKey("Shipment");
            }
            shipmentElem = scModelUtils.getModelObjectFromKey("Shipment", screenInput);
            var defaultOptions = null;
            defaultOptions = scModelUtils.createModelListFromKey("Status", shipmentElem);
            _scBaseUtils.appendToArray(defaultOptions, "1100.70.06.10");
            _scBaseUtils.appendToArray(defaultOptions, "1100.70.06.20");
            _scBaseUtils.appendToArray(defaultOptions, "1100.70.06.30");
            _scBaseUtils.appendToArray(defaultOptions, "1400.10");
            return screenInput;
        },
		// Overriden the mthod to populate the values based on shipment complete(picked up by customer) 
        setShipmentStatusList: function()
        {
            var getShipmentStatusList_output = null;
            getShipmentStatusList_output = {
                "StatusList":
                {
                    "Status": [
                    {
                        "StatusCode": "1100.70.06.10",
                        "StatusKey": "Option_ReadyForBackroomPickup"
                    },
                    {
                        "StatusCode": "1100.70.06.20",
                        "StatusKey": "Option_PicksInProgress"
                    },
                    {
                        "StatusCode": "1100.70.06.30",
                        "StatusKey": "Option_ReadyForCustomerPickup"
                    },
                    {
                        "StatusCode": "1400.10",
                        "StatusKey": "Option_ShipmentComplete"
                    }]
                }
            }
            scScreenUtils.setModel(this, "getShipmentStatusList_output", getShipmentStatusList_output, null);
        },
    });
});
