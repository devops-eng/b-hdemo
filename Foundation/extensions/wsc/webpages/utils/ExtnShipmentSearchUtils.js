scDefine([
	"scbase/loader!dojo/_base/lang",	
	"scbase/loader!extn",	
	"scbase/loader!sc/plat/dojo/Userprefs",
	"scbase/loader!sc/plat/dojo/utils/EditorUtils",
	"scbase/loader!sc/plat/dojo/utils/WizardUtils",
	"scbase/loader!wsc/utils/PrintUtils",
	"scbase/loader!sc/plat/dojo/utils/BaseUtils",
	"scbase/loader!sc/plat/dojo/utils/ModelUtils",
	"scbase/loader!sc/plat/dojo/utils/ScreenUtils",
	"scbase/loader!wsc/utils/BackroomPickUtils",
	],
	function(dLang,extn,scUserprefs,scEditorUtils,scWizardUtils,wscPrintUtils,scBaseUtils,scModelUtils,scScreenUtils,wscBackroomPickUtils){
	var shipmentSearchUtils = dLang.getObject("utils.ExtnShipmentSearchUtils", true,extn);
		//var customPickUtils = dLang.getObject("utils.extnutils.customPickUtils", true,extnpkg);
// custom function needs to define here for example start here

shipmentSearchUtils.extnhandlePickTicketCall = function(){		
			
			var editor = scEditorUtils.getCurrentEditor();
			var wizard = scEditorUtils.getScreenInstance(editor);
			var wInstance = wizard.getWizardElemForId(wizard.wizardId).wizardInstance;
			var summaryScreen = scWizardUtils.getCurrentPage(wInstance);
			var isPrintServiceConfigured = false;
            isPrintServiceConfigured = wscPrintUtils.isPrintServiceConfigured(
            summaryScreen, "BackroomPick");
            if (!(
            scBaseUtils.isBooleanTrue(
            isPrintServiceConfigured))) {
                wscPrintUtils.openPrintServiceConfigErrorBox(
                summaryScreen, "BackroomPick");
                return;
            } 
			var shipmentDetails = summaryScreen.getModel("getShipmentDetails_output");
			var status = scModelUtils.getStringValueFromPath("Shipment.Status.Status",shipmentDetails);
			if(!scBaseUtils.isVoid(status) && (status.indexOf("1100.70.06.10" ) != -1 || status.indexOf("1100.70.06.20" ) != -1||status.indexOf("1100.70.06.30" ) != -1 || status.indexOf("1400" ) != -1)) {
				// only these status are valid to move further
				
				var modelOutput = scModelUtils.createNewModelObjectWithRootKey("Shipments");
				modelOutput.Shipments.Shipment=[];
				modelOutput.Shipments.Shipment[0]={};
				modelOutput.Shipments.Shipment[0].ShipmentKey=scModelUtils.getStringValueFromPath("Shipment.ShipmentKey",shipmentDetails);
				var printModel = null;
                printModel = wscBackroomPickUtils.createMultiApiInputForPrintService(
                summaryScreen, modelOutput);
                wscPrintUtils.openPrintPopupByPickType(
                summaryScreen, printModel, "BackroomPick");
				
			}else{
				var warningString = null;
                warningString = scScreenUtils.getString(
                summaryScreen, "NoPickTicketsToPrint");
				var textObj = null;
                textObj = scBaseUtils.getNewBeanInstance();
                var textOK = null;
                textOK = scScreenUtils.getString(
                summaryScreen, "Ok");
                scBaseUtils.addStringValueToBean("OK", textOK, textObj);
				scScreenUtils.showErrorMessageBox(
                summaryScreen, warningString, "waringCallback", textObj, null);
                return;
			}			
			//return resolutionType;
		};
		
//custom code ended here

return shipmentSearchUtils;
	});
