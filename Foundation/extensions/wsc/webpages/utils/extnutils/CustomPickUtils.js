scDefine([
	"scbase/loader!dojo/_base/lang",	
	"scbase/loader!extn",	
	"scbase/loader!sc/plat/dojo/Userprefs",
	"scbase/loader!sc/plat/dojo/utils/BundleUtils"
	],
	function(dLang,extnpkg,scUserprefs,scBundleUtils){
		var customPickUtils = dLang.getObject("utils.extnutils.CustomPickUtils", true,extnpkg);

		
		customPickUtils.getShortageReasonList = function(){		
			var resolutionType={
								"ShortageReason":[
				{
						"ShortageReasonCode":"HBC_MC_PS_006",
						"ShortageReasonDesc":""+scBundleUtils.getString('HBC_MC_PS_006')
					},
					{
						"ShortageReasonCode":"HBC_MC_PS_002",
						"ShortageReasonDesc":""+scBundleUtils.getString('HBC_MC_PS_002')
					},
					{
						"ShortageReasonCode":"HBC_MC_PS_001",
						"ShortageReasonDesc":""+scBundleUtils.getString('HBC_MC_PS_001')
					},
					{
						"ShortageReasonCode":"HBC_MC_PS_003",
						"ShortageReasonDesc":""+scBundleUtils.getString('HBC_MC_PS_003')
					},
					{
						"ShortageReasonCode":"HBC_MC_PS_004",
						"ShortageReasonDesc":""+scBundleUtils.getString('HBC_MC_PS_004')
					},
					{
						"ShortageReasonCode":"HBC_MC_PS_005",
						"ShortageReasonDesc":""+scBundleUtils.getString('HBC_MC_PS_005')
					},
					{
						"ShortageReasonCode":"HBC_MC_PS_015",
						"ShortageReasonDesc":""+scBundleUtils.getString('HBC_MC_PS_015')
					},
					{
						"ShortageReasonCode":"HBC_MC_PS_007",
						"ShortageReasonDesc":""+scBundleUtils.getString('HBC_MC_PS_007')
					},
					{
						"ShortageReasonCode":"HBC_MC_PS_008",
						"ShortageReasonDesc":""+scBundleUtils.getString('HBC_MC_PS_008')
					},
					{
						"ShortageReasonCode":"HBC_MC_PS_009",
						"ShortageReasonDesc":""+scBundleUtils.getString('HBC_MC_PS_009')
					}
				]
			};
			return resolutionType;
		};
		
		
		
		return customPickUtils;
	});
