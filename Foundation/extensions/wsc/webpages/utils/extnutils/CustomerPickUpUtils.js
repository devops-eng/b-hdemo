scDefine(["scbase/loader!dojo/_base/lang", "scbase/loader!extn", "scbase/loader!sc/plat/dojo/Userprefs", "scbase/loader!sc/plat/dojo/utils/BundleUtils","scbase/loader!sc/plat/dojo/utils/ScreenUtils","scbase/loader!sc/plat/dojo/utils/ModelUtils", "scbase/loader!isccs/utils/ContextUtils", "scbase/loader!sc/plat/dojo/utils/BaseUtils"], function(dLang, extnpkg, scUserprefs, scBundleUtils,scScreenUtils,scModelUtils,sccsContextUtils, scBaseUtils)
{
    var customerPickUpUtils = dLang.getObject("utils.extnutils.CustomerPickUpUtils", true, extnpkg);
    //custom method to validate the search of pickup order
    customerPickUpUtils.validateSearchPickUpOrderInput = function(pickUpOrderTargetModel)
    {
        //console.log("pickUpOrderTargetModel : ",pickUpOrderTargetModel);
        var throwError = false,
            isNameComboValid = false,
            isNameEmpty = false;
        var hasOrderNo = false,
            hasEmailID = false,
            hasPhoneNo = false,
            hasShipmentNo = false;
        var errorMsg = '';
        var errorJSON = {};
        if (scBaseUtils.isVoid(pickUpOrderTargetModel.Shipment))
        {
            throwError = true;
            errorMsg = "pickUpOrderSearchCriteriaMsg";
        }
        if (!scBaseUtils.isVoid(pickUpOrderTargetModel.Shipment.ShipmentNo))
        {
            hasShipmentNo = true;
        }
        if (!throwError && !scBaseUtils.isVoid(pickUpOrderTargetModel.Shipment.ShipmentLines) && !scBaseUtils.isVoid(pickUpOrderTargetModel.Shipment.ShipmentLines.ShipmentLine.OrderNo))
        {
            hasOrderNo = true;
        }
        if (!throwError && !scBaseUtils.isVoid(pickUpOrderTargetModel.Shipment.BillToAddress))
        {
            var firstName = pickUpOrderTargetModel.Shipment.BillToAddress.FirstName;
            var lastName = pickUpOrderTargetModel.Shipment.BillToAddress.LastName;
            var emailID = pickUpOrderTargetModel.Shipment.BillToAddress.EMailID;
            var phoneNo = pickUpOrderTargetModel.Shipment.BillToAddress.DayPhone;
            if (!scBaseUtils.isVoid(firstName))
            {
                if (scBaseUtils.isVoid(lastName))
                {
                    throwError = true;
                    errorMsg = "firstAndLastNameMsg";
                }
                else
                {
                    isNameComboValid = true;
                }
            }
            else
            {
                if (!scBaseUtils.isVoid(lastName))
                {
                    throwError = true;
                    errorMsg = "firstAndLastNameMsg";
                }
                else
                {
                    isNameEmpty = true;
                }
            }
            if (!scBaseUtils.isVoid(emailID))
            {
                hasEmailID = true;
            }
            if (!scBaseUtils.isVoid(phoneNo))
            {
                hasPhoneNo = true;
            }
        }
        if (throwError)
        {
            errorJSON.hasError = throwError;
            errorJSON.errorMsg = errorMsg;
        }
        else if (!isNameComboValid && !hasOrderNo && !hasEmailID && !hasPhoneNo && !hasShipmentNo)
        {
            errorJSON.hasError = true;
            errorJSON.errorMsg = "pickUpOrderSearchCriteriaMsg";
        }
        //console.log("errorJSON : ",errorJSON);
        return errorJSON;
    };
	
	//Overriden  the method to have the to customizt concatCustomerName 
	
	
	customerPickUpUtils.massageCustVerificationNotesModel = function(screen,sCustVerificationNotesModel,selectedShipmentsModel,OrderDetailsModel) {

			var customerName = "";
			var shipmentModel = {};
			var custVerificationNotesModel = {};
			custVerificationNotesModel.Notes = {};
			custVerificationNotesModel.Notes.Note = {};
			custVerificationNotesModel.Notes.Note.ReasonCode = "YCD_CUSTOMER_VERIFICATION";
			custVerificationNotesModel.Notes.Note.ContactUser = scUserprefs.getUserId();


			if(!scBaseUtils.isVoid(selectedShipmentsModel) && !scBaseUtils.isVoid(selectedShipmentsModel.Shipment)) {
				customerName = customerPickUpUtils.concatCustomerName(screen,selectedShipmentsModel,OrderDetailsModel);
			}
			
			if(scBaseUtils.isVoid(customerName)) {
				customerName = "Customer";
			}
			
			var verificationMethodListModel = scScreenUtils.getModel(screen, "common_getCustomerVerficationMethodList_output");
			
			if(!scBaseUtils.isVoid(verificationMethodListModel) && !scBaseUtils.isVoid(verificationMethodListModel.CommonCodeList) && !scBaseUtils.isVoid(verificationMethodListModel.CommonCodeList.CommonCode) && !scBaseUtils.isVoid(verificationMethodListModel.CommonCodeList.CommonCode.length)) {
				
				for(var i=0;i<verificationMethodListModel.CommonCodeList.CommonCode.length;i++) {
				
					if(scBaseUtils.equals(scModelUtils.getStringValueFromPath("Notes.Note.NoteText", sCustVerificationNotesModel), verificationMethodListModel.CommonCodeList.CommonCode[i].CodeValue)) {
						scModelUtils.setStringValueAtModelPath("Notes.Note.NoteText", verificationMethodListModel.CommonCodeList.CommonCode[i].CodeShortDescription,sCustVerificationNotesModel);
						break;
					}
				
				}
			
			}
			
			var dataArray = scBaseUtils.getNewArrayInstance();
			scBaseUtils.appendToArray(dataArray, customerName);
			scBaseUtils.appendToArray(dataArray, scModelUtils.getStringValueFromPath("Notes.Note.NoteText", sCustVerificationNotesModel));

			var formattedValue =  scScreenUtils.getFormattedString(screen,"CustVerificationDefaultNoteText",dataArray);

			custVerificationNotesModel.Notes.Note.NoteText = formattedValue;

			//console.log("custVerificationNotesModel: ",custVerificationNotesModel);

			return custVerificationNotesModel;

		};
	
	//Modifed the method to add the following details 
	
	customerPickUpUtils.concatCustomerName = function(screen,shipmentModel,OrderDetailsModel) {
		
			//console.log("screen : ",screen);
			//console.log("shipmentModel : ",shipmentModel);
			
			var customerName = "",isPersonInfoMarkForPresent = false;
			var orderModel = null;
			
			orderModel= OrderDetailsModel;
				
			if(!scBaseUtils.isVoid(orderModel)) {
					
						isPersonInfoMarkForPresent = true;
						if(!scBaseUtils.isVoid(orderModel.Order.PersonInfoShipTo.FirstName)) {
						
							customerName = orderModel.Order.PersonInfoShipTo.FirstName +" "+orderModel.Order.PersonInfoShipTo.LastName;	
							
						} else {
						
							customerName = orderModel.Order.PersonInfoShipTo.LastName;	
						
						}

						
					
					}
				
				
			
			
			
			if(!isPersonInfoMarkForPresent && !scBaseUtils.isVoid(shipmentModel.Shipment.BillToAddress) && !scBaseUtils.isVoid(shipmentModel.Shipment.BillToAddress.FirstName) && !scBaseUtils.isVoid(shipmentModel.Shipment.BillToAddress.LastName)) {
			
				customerName = shipmentModel.Shipment.BillToAddress.FirstName+" "+shipmentModel.Shipment.BillToAddress.LastName;
				
			}
			
			
			
			return customerName;
		
		
		};
    return customerPickUpUtils;
});
