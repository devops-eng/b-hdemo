/*
 * Licensed Materials - Property of IBM
 * IBM Call Center for Commerce (5725-P82)
 * (C) Copyright IBM Corp. 2013 , 2015 All Rights Reserved.
 * US Government Users Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */
define({ root:
{
	"copyright":"IBM Call Center for Commerce<br>Licensed Materials - Property of IBM.<br><br>(C) Copyright IBM Corporation and its licensors 2013, 2015. All Rights Reserved.<br><br>IBM and the IBM logo are trademarks of IBM Corporation in the United States, other countries, or both.",
	"inactivity_message":"Please note, after some time of inactivity, the system will sign you out automatically and ask you to sign in again.",
	"product_name":"HBC Call Center - DSG",
	"login_title":"Welcome to IBM Call Center for Commerce",
	"login_sub_title":"Please enter your information",
	"login_User_ID":"User name",
	"login_Password":"Password",
	"login_Submit":"Login",
	"invalidMessageTitle":"Login Failed",
	"Login_Failed":"The combination of user name and password you entered does not match any existing accounts. Please try again.",
	"System_Timeout":"System Timeout",
	"login_failed_authorization_error":"User is not authorized to access the application."
}, //EOF
// START NON-TRANSLATABLE 
"de" : true,
"es" : true,
"fr" : true,
"it" : true,
"ja" : true,
"ko" : true,
"pl" : true,
"pt" : true,
"ru" : true,
"tr" : true,
"zh" : true,
"zh-tw" : true
});
// END NON-TRANSLATABLE
