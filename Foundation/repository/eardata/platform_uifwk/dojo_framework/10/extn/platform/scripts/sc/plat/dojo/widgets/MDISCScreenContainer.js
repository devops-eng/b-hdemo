/*
Licensed Materials - Property of IBM
IBM Sterling Order Management  (5725-D10)
IBM Call Center for Commerce
(C) Copyright IBM Corp. 2013 All Rights Reserved.
US Government Users Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
*/
scDefine([
         "scbase/loader!dojo/_base/declare",
         "scbase/loader!dojo/_base/lang",
         "scbase/loader!dojo/_base/array",
         "scbase/loader!idx/layout/MoveableTabContainer",
         "scbase/loader!sc/plat/dojo/widgets/_ISCScreenContainer",
         "scbase/loader!sc/plat/dojo/widgets/SCScreenContainerHelper",
         "scbase/loader!sc/plat/dojo/info/ApplicationInfo",
         "scbase/loader!sc/plat/dojo/utils/BaseUtils",
         "scbase/loader!sc/plat/dojo/hotkeys/HotKeysManager",
         "scbase/loader!sc/plat/dojo/utils/Util",
		 "scbase/loader!sc/plat/dojo/utils/ScreenExtnUtils",
         "scbase/loader!sc/plat/dojo/utils/LogUtils",
         "scbase/loader!sc/plat/dojo/events/GlobalEventManager",
         "sc/plat/dojo/widgets/Editor",
         "scbase/loader!dijit/registry",
         "scbase/loader!sc/plat/dojo/events/BusinessEventObj",
		 // Not used in MDI, but imported so that established sequence of imports is maintained in app.
		 "scbase/loader!sc/plat/dojo/widgets/WizardUI",
		 "scbase/loader!sc/plat/dojo/widgets/Wizard"
          ],
          function(
        		   dDeclare,
        		   dLang,
        		   dArray,
        		   idxMoveableTabContainer,
        		   scISCScreenContainer,
        		   scSCScreenContainerHelper,
        		   scApplicationInfo,
        		   scBaseUtils,
        		   scHotKeysManager,
        		   scUtil,
        		   scScreenExtnUtils,
        		   scLogUtils,
        		   scGlobalEventManager,
        		   scEditor,
        		   dRegistry,
        		   scBusinessEventObj,
				   scWizardUI,
				   scWizard
          		   ){
						// Whenever the browser is closed/ or a link is clicked/or refreshed,
						// a check is done if any of the opened screens in MDI are dirty, if they are dirty then an message is shown.
						var checkOnExit = function(){
							// if(!scBaseUtils.and(!scBaseUtils.isVoid(window.opener.name),scBaseUtils.equals(window.opener.name,"store")))
							if(!(window.opener && window.opener.name=="store"))
							 {
								
								var container=scApplicationInfo.getSCScreenContainerInstance();
							var children = container.getChildren();
							var checkRequired = false;
							var isDirty = false;
							var dirtyCheckRequired = true;
							if(scUtil.getReloadConfirmationPopupMode() == 'ALWAYS'){
								checkRequired = true;
							}else if(scUtil.getReloadConfirmationPopupMode() == 'NEVER'){
								dirtyCheckRequired = false;
								checkRequired = false;
							}
							if(dirtyCheckRequired){
								isDirty=dArray.some(children,function(child){
									if(child != null) {
										return child.isDirty();
									}
								},container);
							}

							if(scApplicationInfo.getIsExtensibilityActive() == true) {
								// do nothing. This is to avoid popup when shell is open.
							} else if(isDirty){
								//return sc.plat.dojo.utils.BundleUtils.getFormattedString('CONFIRM_EXIT_DIRTY');
								return scUtil.getReloadConfirmationMessageForDirtyScreen();
							}else if(checkRequired){
								return scUtil.getReloadConfirmationMessageForNonDirtyScreen();
							}
							}
							
						};
						window.onbeforeunload=checkOnExit;
						return dDeclare("sc.plat.dojo.widgets.MDISCScreenContainer",
								[idxMoveableTabContainer,scISCScreenContainer],{								 
							
							// _globalEMgr: [private] object
							//		An instance of sc.plat.dojo.events.GlobalEventManager maintained locally for publishing MDI events.
							_globalEMgr: null,
							
							// _helperInstance: [private] object
							//		Instance of the sc.plat.dojo.widgets.SCScreenContainerHelper class. This class maintains the internal 
							//		workings of the MDI container.
							_helperInstance: null,
							
							// _subscribersGlobal: [private] Array[object]
							//		An array with a list of all the events that the MDI Container subscribes to.
							_subscribersGlobal: [],
							
							// _preventSelectChild: [private] boolean
							//		Simple flag maintained internally to prevent calling the selectChild() method for the MDI container
							//		superclass.
							_preventSelectChild: false,
							
							// Editor class
							_editorClass : "sc.plat.dojo.widgets.Editor",
							
							constructor: function() {
								// Subscribe to global events for popups: modalDialogOpened, PopupClose. Ignoring nonmodalDialogOpened.
								// Adding event subscription for closeAllEditors...
								this._subscribersGlobal = [{
									eventId: "modalDialogOpened",
									handler: {
										methodName: "_raiseScreenContainerEventsForDialog" 
									}
								}, {
									eventId: "PopupClose",
									handler: {
										methodName: "_raiseScreenContainerEventsForDialog" 
									}
								}, {
									eventId: "closeAllEditors",
									handler: {
										methodName: "_handleCloseAllEditors" 
									}
								}, {
									eventId: "changeEditorTitle",
									handler: {
										methodName: "_handleEditorTitleChange" 
									}
								}, {
									eventId: "screenchanged",
									handler: {
										methodName: "_raiseEventEditorContentChanged"
									}
								}];
								this._globalEMgr = scGlobalEventManager;
								this._globalEMgr.addListeners(this, this._subscribersGlobal);
								this._helperInstance = new scSCScreenContainerHelper();
							},
							postCreate: function() {
								this.inherited(arguments);
								this.subscribe(this.id+"-addChild", "_raiseEventEditorOpened");
								this.subscribe(this.id+"-removeChild", "_raiseEventEditorClosed");
								this.subscribe(this.id+"-selectChild", "_raiseEventEditorFocusGained");
							},
							startup: function() {
								this.inherited(arguments);
								scApplicationInfo.setSCScreenContainerInstance(this);
								scApplicationInfo.setHotKeysManagerInstance(new scHotKeysManager());
							},
							_raiseEventEditorOpened: function(/*dijit/_Widget*/ editor, /*Integer?*/ insertIndex) {
								this._raiseScreenContainerEvent("editorOpened", editor, null, null);
							},
							_raiseEventEditorClosed: function(/*dijit/_Widget*/ editor) {
								this._raiseScreenContainerEvent("editorClosed", editor, null, null);
							},
							_raiseEventEditorFocusGained: function(/*dijit/_Widget*/ editor) {
								this._raiseScreenContainerEvent("editorFocusGained", editor, null, null);
							},
							_raiseEventEditorContentChanged: function(args) {
								this._raiseScreenContainerEvent("editorContentChanged", args.screen, null, null);
							},
							
							_handleEditorTitleChange: function(args){
								// summary: 
								//		This method is the handler for event changeEditorTitle.
								//		It changes the title of the editor with the new title which is supplied.
								//		It check if the editor corresponding to the screen from which the event 
								//		was raise has the same uId as that of the current editor. 
								//		If the uId matches, the title is changed. Otherwise not.
								// description:
								//		Handler for event "changeEditorTitle" used for changing the title of the editor.
								// args: Object
								//		This is the input object to the MDI/SDI Container. It has the following attributes:
								//
								// 		title: String
								//			New title for the editor.

								// Check if the title is supplied or not
								// TODO: All this logic about ownerscreen etc does not seem ideal. If any screen within editor is dirty, it can mark editor as dirty. Need to understand reasons and maybe simplify. For now we will retain this. Maybe reason is to avoid changes in popup from making screen dirty?
								if(!args.title){
									scLogUtils.debug("Title is not supplied while trying to change editor title");
								}

								var currentEditor = this.getCurrentEditor();
								var screenEditor = args.screen.editor;

								
								if(screenEditor == null && args.screen.ownerScreen){ // This happens when wizard page raises the event 'changeEditorTitle'
                                    //In that case, WizardUI's editor, of the page which raises the event should be same as the current editor in focus									
									if(args.screen.ownerScreen.wizardUI){
										screenEditor = args.screen.ownerScreen.wizardUI.editor;
									}
								}
								// check if the event is raised by the current screen itself
								if((currentEditor && screenEditor) && (screenEditor.id == currentEditor.id)){   //checking if it the is the same instance
									//We will not force compute dirty but rely on cached values. This is less heavy.
									if(args.clean && args.screen.isDirty(null, false)){
										args.title=currentEditor.getDirtyTitle(args.title);
									}
									currentEditor.setInitialTitle(args.title);
									currentEditor.set('title', currentEditor.getInitialTitle());
								}
							},
							
							_raiseScreenEvent: function(eventName, editor, parentScreen, eventDefn) {
								
									var screenInstance = null;
									var editorInstance = null;
									
									if ( editor.isInstanceOf(scEditor)){
										screenInstance = editor.getScreenInstance();
										editorInstance = editor;
									} else {
										screenInstance = editor;
									}
									editorInstance = editor;
									
									var screenInputData = screenInstance.get("_initialInputData");
									var screenClassName = screenInstance.get("declaredClass");
									var eventArgsObj = {
										"eventName": eventName,
										// screen is always the screen which is of interest to listener or the reference screen.
										"screen": screenInstance,
										"originatingControl": this,
										"screenInstance": screenInstance,
										"screenInputData": screenInputData,
										"screenClassName": screenClassName,
										"parentScreen": parentScreen,										
										"editor" : editorInstance
									};								
									screenInstance._eMgr.addEventObject(new scBusinessEventObj({
										name :eventName 
									},screenInstance));	
									return screenInstance.publish(eventName, eventArgsObj);
							},
							
							_raiseScreenContainerEvent: function(eventName, editor, parentScreen, dialog, eventDefn) {
								
								var screenInstance = null;
								var editorInstance = null;

								if ( editor.isInstanceOf(scEditor)){
									screenInstance = editor.getScreenInstance();
									editorInstance = editor;
								} else {
									screenInstance = editor;
								}
								editorInstance = editor;
								
								var screenInputData = screenInstance.get("_initialInputData");
								var screenClassName = screenInstance.get("declaredClass");
								var eventArgsObj = {
									"eventName": eventName,
									// screen is always the screen which is of interest to listener or the reference screen.
									"screen": screenInstance,
									"originatingControl": this,
									"screenInstance": screenInstance,
									"screenInputData": screenInputData,
									"screenClassName": screenClassName,
									"parentScreen": parentScreen,
									"dialog": dialog,
									"editor" : editorInstance
								};
								
								return this._globalEMgr.handlePublish(eventName, eventArgsObj, eventDefn);
							},
							/*
							TODO for Dialogs: Work with owner to come up with a solution for opening a screen as a dialog with "_initialInputData" and "comparisonAttributes".
							Also, work out if we must always use the MDI/SDI Containers + raise "before" events for dialog.
							*/
							_raiseScreenContainerEventsForDialog: function(args) {
								// TODO: might name to re-name and re-work if its confirmed that we need to handle only modal dialogs.
								if(args.popupAction == "open" || args.eventName == "modalDialogOpened") {
									this._raiseScreenContainerEvent("containerDialogOpened", args.screen, args.parentScreen, args.dialog);
								} else {
									if(args.closePopup) {

										// cheating.. just pass a blank event defn..
										var eventDefn = {};

										// TODO: raise beforeContainerDialogClosed and see if you get response as true and then proceed.
										var closePopup = this._raiseScreenContainerEvent("beforeContainerDialogClosed", args.screen, args.parentScreen, args.dialog, eventDefn);

										// If returned value is failure, don't close Popup. If undefined or anything else, close it out.
										var close = true;
										if ( closePopup && closePopup.response && closePopup.response.statusCode ) {
											var status = closePopup.response.statusCode;
											close = status != "failure";
										}
										if ( close ) {
											args.closePopup(args.dialog);
											this._raiseScreenContainerEvent("containerDialogClosed", args.screen, args.parentScreen, args.dialog);
										}
									}
								}
							},
							_handleCloseAllEditors: function() {
								var children = this.getChildren();
								//TODO: figure out arguments.
								dArray.forEach(children, this.closeEditor, this);
							},

							// IMPLEMENTING STACK CONTAINER STUFF + _ISCScreenContainer

							addChild: function(/*dijit/_Widget*/ editor, /*Integer?*/ insertIndex, /*Boolean?*/ skipExtensibilityCheck) {
								// TODO: raise beforeEditorOpened and see if you get response as true and then raise editorOpen.
								this._raiseScreenContainerEvent("beforeEditorOpened", editor, null, null);
								this.inherited(arguments);
								// Automatically calling selectChild()
								this.selectChild(editor, true, skipExtensibilityCheck);
							},
							selectChild: function(/*dijit/_Widget|String*/ editor, /*Boolean*/ animate, /*Boolean?*/ skipExtensibilityCheck) {
								if(scApplicationInfo.getIsExtensibilityActive() == true && skipExtensibilityCheck != true) {
						   			return;
								}
								editor = dRegistry.byId(editor);
								if(!this._preventSelectChild) {
									this.inherited(arguments);
								}
							},
							_transition: function(/*dijit/_Widget*/ newEditor, /*dijit/_Widget*/ oldEditor, /*Boolean*/ animate){
								// summary:
								//		Hide the old widget and display the new widget.
								//		Overriding StackContainer's method. Documentation mentions subclasses should override this method.
								//		Watch out for potential issues in future releases.
								// tags:
								//		protected extension

								if(oldEditor) {
									// TODO: raise beforeEditorFocusLost and see if you get response as true and then raise editorOpen.
									this._raiseScreenContainerEvent("beforeEditorFocusLost", oldEditor, null, null);
									this._hideChild(oldEditor);
									this._raiseScreenContainerEvent("editorFocusLost", oldEditor, null, null);
								}
								// TODO: raise beforeEditorFocusGained and see if you get response as true and then proceed.
								this._raiseScreenContainerEvent("beforeEditorFocusGained", newEditor, null, null);

								var d = this._showChild(newEditor);

								// Size the new widget, in case this is the first time it's being shown,
								// or I have been resized since the last time it was shown.
								// Note that page must be visible for resizing to work.
								if(newEditor.resize) {
									if(this.doLayout) {
										newEditor.resize(this._containerContentBox || this._contentBox);
									}else {
										// the child should pick it's own size but we still need to call resize()
										// (with no arguments) to let the widget lay itself out
										newEditor.resize();
									}
								}

								return d;	// If child has an href, promise that fires when the child's href finishes loading
							},
							closeChild: function(editor, /*Boolean?*/ skipExtensibilityCheck){
								if(scApplicationInfo.getIsExtensibilityActive() == true) {
									if(skipExtensibilityCheck == true) {
										this._removeEditor(editor, skipExtensibilityCheck);
									}
						   			return;
								}
								var remove = editor.onClose(this, editor);
								if(remove)
								{
									var screenInstance = null;
									var editorInstance = null;
							
									if (editor.isInstanceOf(scEditor)){
										screenInstance = editor.getScreenInstance();
										editorInstance = editor;
									} else {
										screenInstance = editor;
									}
									editorInstance = editor;
							
									var screenInputData = screenInstance.get("_initialInputData");
									var screenClassName = screenInstance.get("declaredClass");
									var editorId = editorInstance.id;
							
									var eventArgsObj = {
										"eventName": "beforeEditorClosed",
										"screen": screenInstance,
										"originatingControl": this,
										"screenInstance": screenInstance,
										"screenInputData": screenInputData,
							    		"screenClassName": screenClassName,
										"parentScreen": {},
										"dialog": {},
										"editor" : editorInstance,
										"editorId" : editorId 
									};

									var publishReturnValue = editor._eMgr.publish("beforeEditorClosed",{},eventArgsObj);
									if(publishReturnValue){ 
										if(publishReturnValue.individualResponses){
											if(typeof publishReturnValue.individualResponses[0] != 'undefined'){
												if(publishReturnValue.individualResponses[0].closeEditor  === false){
													return;
												}
											}
										}
										else if(publishReturnValue.closeEditor  === false){
											return;
										}
									}
									this._removeEditor(editor);
								}
							},
							
							// Wrapper written for removeChild as remove child should not be destroying the editor.
							_removeEditor: function(editor, /*Boolean?*/ skipExtensibilityCheck){
								if(!editor.isRemoved){
									this.removeChild(editor, skipExtensibilityCheck);
									editor.destroyRecursive(); // destroy editor instance
									editor.isRemoved = true;
								}
							},
							
							removeChild: function(/*dijit/_Widget*/ editor, /*Boolean?*/ skipExtensibilityCheck) {
								if(scApplicationInfo.getIsExtensibilityActive() == true && skipExtensibilityCheck != true) {
						   			return;
								}
								this._preventSelectChild = true;
								this.inherited(arguments);
								var currentEditor = this.getCurrentEditor();
								this._preventSelectChild = false;
								this.selectChild(currentEditor, true, skipExtensibilityCheck);
							},
							
							openEditor: function(editorInputJson, editorInstance, isExistingEditorUsed) {
								// summary: 
								//		Open an editor in MDI/SDI container.
								// description:
								//		Open an editor in MDI/SDI container.
								// editorInputJson: Object
								//		This is the input object to the MDI/SDI Container. It has the following attributes:
								//
								// 		screenClassName: String
								//			The fully qualified class name of the screen to open.
								// 		screenConfig: Object
								//			The config object to pass to the screen's constructor. Basically, the parameters for screen.
								//		inputData: Object
								//			The input data to screen. We would do a setModel on all available namespaces.
								//		comparisonAttributes: Array[String]
								//			This is an array of the attributes that would be unique in screenInputData.
								// editorInstance: Editor
								//		Editor instance in which the screen needs to be opened
								// isExistingEditorUsed: Boolean
								//		Flag which indicates if an existing editor is being used or a new one

								var screenClassName = editorInputJson["screenClassName"];
								var screenInstance = editorInputJson["screenInstance"];
								if(screenInstance){
									screenClassName = screenInstance.declaredClass;
								}
								var key = "openEditor_container_" + screenClassName;
								scLogUtils.startPerfDataCollection(key, "Time taken by openEditor in container to open screen " + screenClassName);

								if(editorInstance == null){
									var that = this;
									scUtil.getInstance(this._editorClass, null, null, function(newEditorInstance){
										that._openScreenInEditor(editorInputJson, newEditorInstance, false);
									});
								}
								else{
									this._openScreenInEditor(editorInputJson, editorInstance, isExistingEditorUsed);
								}
							},
							
							_openScreenInEditor: function(editorInputJson, editorInstance, isExistingEditorUsed) {
								// summary: 
								//		This method performs the actual opening of editor in MDI/SDI containers.
								// description:
								//		If the editor instance contains an screen, it is destroyed and a new screen if created
								// editorInputJson: Object
								//		This is the input object to the MDI/SDI Container. It has the following attributes:
								//
								// 		screenClassName: String
								//			The fully qualified class name of the screen to open.
								// 		screenConfig: Object
								//			The config object to pass to the screen's constructor. Basically, the parameters for screen.
								//		inputData: Object
								//			The input data to screen. We would do a setModel on all available namespaces.
								//		comparisonAttributes: Array[String]
								//			This is an array of the attributes that would be unique in screenInputData.
								// editorInstance: Editor
								//		Editor instance in which the screen needs to be opened
								// isExistingEditorUsed: Boolean
								//		Flag which indicates if an existing editor is being used or a new one
								
								// Check and remove the existing screen if present
								var existingScreen = editorInstance.getScreenInstance();
								if(existingScreen != null){
									existingScreen.destroyRecursive();
								}
								
								var screenClassName = editorInputJson["screenClassName"];
								var screenConfig = editorInputJson["screenConfig"] || {};
								var screenInstance = editorInputJson["screenInstance"];

								var inputData = editorInputJson["inputData"];
								var clonedInputData = dLang.clone(inputData); //TODO: Is cloning really required??

								var editorConfig = editorInputJson["editorConfig"] || {};									
							
								editorInstance.setInitialInputData(clonedInputData);
								
								if(screenInstance){
									screenClassName = screenInstance.declaredClass;
									editorConfig = screenInstance.editorConfig;
								}
								
								dLang.mixin(editorInstance, editorConfig);
								
								var defaultAttributes = {
									"closable": true,
									"class": "editortopscreen",
									"_initialInputData": clonedInputData
								};
								
								if(!screenInstance){
									dLang.mixin(screenConfig, defaultAttributes);
									dLang.mixin(screenConfig, {ownerScreen:editorInstance, editor:editorInstance});
									
									var that = this;
									scScreenExtnUtils.getScreenInstance(screenClassName, screenConfig, null, function(screen){
										screen.setInitialInputData(inputData);												
										for(var namespace in inputData) {
											screen.setModel(namespace, inputData[namespace]);
										}
										that.renderScreenInEditor(editorInstance, screen, isExistingEditorUsed);												
									});
								} 
								else{
									dLang.mixin(screenInstance, defaultAttributes);																					
									var screen = screenInstance;
									screen.ownerScreen = editorInstance;
									screen.editor = editorInstance;
									this.renderScreenInEditor(editorInstance, screen, isExistingEditorUsed);																			
								}

								if(!isExistingEditorUsed){
									this.addChild(editorInstance);
								}
								
								var key = "openEditor_container_" + screenClassName;
								scLogUtils.stopPerfDataCollection(key);
								
								this._raiseScreenEvent("afterScreenLoad", editorInstance, editorInstance);								
							},
							
							isEditorOpen: function(editorOrId, controlOrId, inputData, comparisonAttributes) {
								return (this._helperInstance.getOpenEditor(editorOrId, controlOrId, inputData, comparisonAttributes, this.getChildren()) != null);
							},
							closeEditor: function(editorOrId, controlOrId, inputData, comparisonAttributes) {
								if(scApplicationInfo.getIsExtensibilityActive() == true) {
						   			return;
								}
								if(typeof(editorOrId) == 'object'){
									if(editorOrId.declaredClass !== "isccs.editors.ISCCSEditor"){
										if(editorOrId.editor)
											editorOrId = editorOrId.editor;
										else
											scLogUtils.debug("Screen does not have editor object");
									}
								}
								var editorInstance = this._helperInstance.getOpenEditor(editorOrId, controlOrId, inputData, comparisonAttributes, this.getChildren());
								if(editorInstance != null) {
									this._removeEditor(editorInstance);
								}
							},
							getCurrentEditor: function() {
								var topmostEditorIndex = this._helperInstance._openEditorIdStack.length - 1;
								var currentEditorId = this._helperInstance._openEditorIdStack[topmostEditorIndex];
								return dRegistry.byId(currentEditorId);
							},
							/*
							TODO for Dialogs: Work with owner to come up with a solution for opening a screen as a dialog with "_initialInputData" and "comparisonAttributes".
							Also, work out if we must always use the MDI/SDI Containers.
							*/
							openDialog: function(popupParams, addnlParams, screen, callBackHandler) {
								scBaseUtils.openPopup(popupParams, addnlParams, screen, callBackHandler);
							},
							// isScreenOpenInDialog is not required as this takes care of that need.
							isDialogOpen: function(dialogOrId, controlOrId) {
								return (this._helperInstance.getOpenDialog(dialogOrId, controlOrId) != null);
							},
							closeDialog: function(dialogOrId, controlOrId) {
								var dialog = this._helperInstance.getOpenDialog(dialogOrId, controlOrId);
								if(dialog != null) {
									dialog.hide();
								}
							},
							closeAllDialogs: function(/*Boolean?*/ destroy, /*Boolean?*/ updateDialogArrayForExtensibility) {
								var dialogArray = this._helperInstance.getDialogArray();
								if(dialogArray.length > 0) {
									for(var i = 0; i < dialogArray.length; i++) {
										var aDialog = dialogArray[i];
										if(updateDialogArrayForExtensibility == true) {
											this._helperInstance.updateOpenDialogObjectStack_delete(aDialog.id);
										}
										if(destroy == true) {
											aDialog.destroyRecursive();
										} else {
											aDialog.hide();
										}
									}
								}
							},
							getCurrentDialog: function() {
								var dialogArray = this._helperInstance.getDialogArray();
								if(dialogArray.length > 0) {
									var currentDialog = dialogArray[dialogArray.length - 1];
									return currentDialog;
								}
								return null;
							},
							getCurentScreenInDialog: function() {
								var dialog = this.getCurrentDialog();
								return (dialog == null) ? null : dialog.contentScreen;
							},
							getTopmostScreen: function() {
								if(this._helperInstance.getDialogArray().length > 0) {
									var dialog = this.getCurrentDialog();
									return dialog.contentScreen;
								} else {
									return this.getCurrentEditor();
								}
							},
							setEditorClass : function(clazz) {
								this._editorClass = clazz;
							},

							getEditorClass: function(){ 
								// summary: 
								//		This method returns the default editor class defined in the MDI container
								// description:
								//		This method returns the default editor class defined in the MDI container
								
								return this._editorClass;
							},

							getMatchingEditor: function(editorInstance, editorInputJson){
								// summary: 
								//		This method returns the matching editor to the editor instance passed and input data.
								// description:
								//		This method returns the matching editor to the editor instance passed and input data.
								// editorInstance: Editor
								//		Editor instance in which the screen needs to be opened
								// editorInputJson: Object
								//		This is the input object to the MDI/SDI Container. It has the following attributes:
								//
								// 		screenClassName: String
								//			The fully qualified class name of the screen to open.
								// 		screenConfig: Object
								//			The config object to pass to the screen's constructor. Basically, the parameters for screen.
								//		inputData: Object
								//			The input data to screen. We would do a setModel on all available namespaces.
								//		comparisonAttributes: Array[String]
								//			This is an array of the attributes that would be unique in screenInputData.
								
								var matchingEditor = null;
								
								if(editorInstance != null && editorInputJson != null){
									var inputData = editorInputJson["inputData"];
									var openEditorArray = this.getChildren(); 
									var editorId = editorInstance.get("declaredClass");
								
									matchingEditor = this._helperInstance.getOpenEditorForGivenData(editorInstance.getComparisonAttributes(), inputData, 
																										editorId, openEditorArray);
								}
								
								return matchingEditor;
							},

							renderScreenInEditor : function(editorInstance, screen, isExistingEditorUsed) {
								editorInstance.setScreenInstance(screen);			
								editorInstance.renderScreen();
								
								if(!isExistingEditorUsed){
									editorInstance.setInitialTitle(editorInstance.getScreenTitle());
									editorInstance.set('title', editorInstance.getInitialTitle());
								}
								editorInstance.postScreenSetup();
							},
							
							buildRendering: function(){
								this.inherited(arguments);
								if(this.domNode.firstElementChild != null) {
									if(!this.domNode.firstElementChild.getAttribute("role")){
										this.domNode.firstElementChild.setAttribute("role", "navigation");
									}
									if(!this.domNode.firstElementChild.getAttribute("wairole")){
										this.domNode.firstElementChild.setAttribute("wairole", "navigation");
									}
								}
							}							
							
						});
						
						
	
});
