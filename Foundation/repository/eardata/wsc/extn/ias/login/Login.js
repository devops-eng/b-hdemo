/*
 * Licensed Materials - Property of IBM
 * IBM Sterling Order Management Store (5725-D10)
 * (C) Copyright IBM Corp. 2014 , 2015 All Rights Reserved. , 2015 All Rights Reserved.
 * US Government Users Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */
scDefine(["scbase/loader!dojo/_base/lang",
        "scbase/loader!dojo/_base/array",
        "scbase/loader!dojo/_base/connect",
        "scbase/loader!dojo/keys",
        "scbase/loader!dojo/aspect",
        "scbase/loader!dojo/dom",
        "scbase/loader!dijit/registry",
        "scbase/loader!idx/app/Header",
        "scbase/loader!idx/app/LoginFrame",
        "scbase/loader!sc/plat/dojo/utils/BundleUtils",
        "scbase/loader!ias",
        "scbase/loader!sc/plat/dojo/utils/BaseUtils"
    ],
    function(dLang, dArray, dConnect, dKeys, dAspect, dDom, dRegistry, idxHeader, idxLoginFrame, scBundleUtils, ias, _scBaseUtils) {

        var dLogin = dLang.getObject("login.Login", true, ias);

        var loginFrameId = "login";
        var userNameInputFieldId = loginFrameId + "UserName";
        var passwordInputFieldId = loginFrameId + "Password";
        var storeInputFieldId = loginFrameId + "StoreId";
        var loginFrame = null;

        dLogin.init = function(errorMessage) {

            var hdr = new idxHeader({
                //primaryTitle: scBundleUtils.getString("product_name")
            }, "appHeader");

            loginFrame = new idxLoginFrame({
                id: loginFrameId,
                userAutoComplete: false,
                loginCopyright: scBundleUtils.getString("copyright"),
                inactivityMessage: scBundleUtils.getString("inactivity_message"),
                loginTitle: scBundleUtils.getString("login_title"),
                loginSubTitle: scBundleUtils.getString("login_sub_title"),
                labelUserName: scBundleUtils.getString("login_User_ID"),
                labelPassword: scBundleUtils.getString("login_Password"),
                labelStoreNo: scBundleUtils.getString("Store ID"),
                labelSubmitButton: scBundleUtils.getString("login_Submit"),
                invalidMessageTitle: scBundleUtils.getString("invalidMessageTitle"),
                onSubmit: function(username, password, store) {
                    dDom.byId("sc_plat_dojo_widgets_ScreenDialogUnderlay_Initial").style.display = "block";
                    dDom.byId("displayUserId").value = username;
                    //dDom.byId("password").value = password;
                    //Fix for IE 11 not logining after hitting enter key
                    dDom.byId("password").value = dDom.byId("loginPassword").value;
                    dDom.byId("StoreId").value = dDom.byId("loginStoreId").value;
                    dDom.byId("fieldsForm").submit();
                }

            }, "loginFrame");

            loginFrame.loginStoreId.set("class", "loginStoreIdTextbox");
            loginFrame.loginUserName.set("class", "loginUserNameTextbox");
            loginFrame.loginPassword.set("class", "loginPasswordTextbox");



            //      dConnect.connect(dRegistry.byId(""));

            dArray.forEach([dRegistry.byId(userNameInputFieldId), dRegistry.byId(passwordInputFieldId), dRegistry.byId(storeInputFieldId)], function(node, index, array) {
                dConnect.connect(node, "onKeyUp", function(event) {
                    if (event.keyCode === dojo.keys.ENTER) {
                        dRegistry.byId("loginButton").focus();
                        loginFrame._onSubmitClick(event);
                    }
                });
            }, null);

            if (errorMessage) {
                errorMessage = scBundleUtils.getString(errorMessage);
                loginFrame.invalidMessageNode.innerHTML = errorMessage;
                loginFrame.invalidLoginDialog.show();


                // wait for dialog to close to revalidate the form and focus the first bad field
                var self = loginFrame;
                var dialogHandle = dAspect.after(loginFrame.invalidLoginDialog, "onHide", function() {
                    self.invalidMessageNode.innerHTML = self.invalidMessage;
                    self.loginUserName.focus();
                    if (dialogHandle) dialogHandle.remove();
                });
            }

        };

        dLogin.setFocusOnUsername = function() {
            loginFrame.loginUserName.focus();
        };

        return dLogin;
    });

	