define({ root:
{
	"copyright":"Licensed Materials - Property of IBM Corp, IBM Corporation and other(s) 2014 , 2015. IBM is a registered trademark of IBM Corporation, in the United States, other countries, or both.",
	"inactivity_message":"Please note, after some time of inactivity, the system will sign you out automatically and ask you to sign in again.",
	"login_title":"Welcome to IBM Application Shell",
	"login_sub_title":"Please enter your information.",
	"login_User_ID":"User name",
	"login_Password":"Password",
	"login_Submit":"Login",
	"invalidMessageTitle":"Login Failed",
	"Unsupported_Login_Procedure":"Unsupported login procedure",
	"Login_Failed":"Login credentials are incorrect. Please enter a valid username, password, and store number, and try again.",
	"login_failed_authorization_error":"User is not authorized to access the application.",
	"System_Timeout":"The session has timed out.",
	"OK":"OK"
}, //EOF
// START NON-TRANSLATABLE 
	"de" : true,
    "es" : true,
	"fr" : true,
	"it" : true,
	"ja" : true,
	"ko" : true,
	"pl" : true,
	"pt" : true,
	"ru" : true,
	"tr" : true,
	"zh" : true,
	"zh-tw" : true
});
// END NON-TRANSLATABLE
