<%
// Licensed Materials - Property of IBM
// IBM Sterling Order Management Store (5725-D10)
// (C) Copyright IBM Corp. 2014 , 2015 All Rights Reserved. , 2015 All Rights Reserved.
// US Government Users Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.ibm.wsc.core.WSCConstants"%>
<%@page import="com.sterlingcommerce.security.dv.SCEncoder"%>
<%@page import="com.sterlingcommerce.ui.web.framework.utils.SCUIContextHelper"%>
<%@page import="com.sterlingcommerce.ui.web.framework.context.SCUIContext"%>
<%@page import="com.sterlingcommerce.ui.web.framework.utils.SCUIJSONUtils"%>
<%@page import="com.sterlingcommerce.ui.web.framework.helpers.SCUILocalizationHelper"%>
<%@page import="com.sterlingcommerce.ui.web.framework.utils.SCUIUtils"%>
<%@page import="org.w3c.dom.Element"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="com.ibm.sterling.afc.dojo.util.ProfileUtil" %>
<%@ taglib uri="/WEB-INF/scui.tld" prefix="scuitag" %>
<%@ taglib uri="/WEB-INF/scuiimpl.tld" prefix="scuiimpltag" %>
<%@ page import="java.util.Locale "%>


<%
	response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
	response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
	response.setDateHeader("Expires", 0); 
			
	String enterpriseCode = request.getParameter(WSCConstants.LOGIN_ENTERPRISE_CODE);
	String storeId = request.getParameter(WSCConstants.LOGIN_STORE_ID);
	String storeLocaleCode = request.getParameter(WSCConstants.LOGIN_STORE_LOCALE_CODE);
	String embedded = request.getParameter(WSCConstants.EMBEDDED_MODE);
	
	if(SCUIUtils.isVoid(enterpriseCode)){
		enterpriseCode="";
	}
	if(SCUIUtils.isVoid(storeId)){
		storeId="";
	}
	if(SCUIUtils.isVoid(storeLocaleCode)){
		storeLocaleCode="";
	}
	if(SCUIUtils.isVoid(embedded)){
		embedded="";
	}
	
	SCUIContext uiContext = SCUIContextHelper.getUIContext(request, response);
	String localeCode = "";
	if(!SCUIUtils.isVoid(uiContext)){
	 localeCode = uiContext.getUserPreferences().getLocale().getLocaleCode();
	}
	if(SCUIUtils.isVoid(localeCode) && !SCUIUtils.isVoid(request.getParameter("LocaleCode"))  ){
		localeCode = request.getParameter("LocaleCode");  
	}else if(SCUIUtils.isVoid(localeCode)){
		localeCode = request.getLocale().toString();
	}
	
	if(!SCUIUtils.isVoid(storeLocaleCode)){
		localeCode = storeLocaleCode;
	}
	
	localeCode = localeCode.toString().replace('_', '-').toLowerCase();

	//Strip the -Ext at the end of the locale.
	//If the locale string contains a - then take the first 5 letters xx-xx
	//Else leave it as is to avoid  
	if (localeCode.length()>5 && localeCode.contains("-"))
		localeCode=localeCode.substring(0,5);

	response.setContentType("text/html;charset=UTF-8");
	String errorMsg = request.getParameter("ErrorMsg");
	if(SCUIUtils.isVoid(errorMsg)){
		errorMsg = (String)uiContext.getAttribute("ERROR_MESSAGE");
	}
	
	if(SCUIUtils.isVoid(errorMsg)){
		errorMsg = "";
	}

	Cookie storeCookie = new Cookie (WSCConstants.LOGIN_STORE_ID,storeId);	
	Cookie localeCodeCookie = new Cookie (WSCConstants.LOGIN_STORE_LOCALE_CODE,storeLocaleCode);
	Cookie embeddedCookie = new Cookie (WSCConstants.EMBEDDED_MODE,embedded);
	storeCookie.setPath(request.getContextPath());
	localeCodeCookie.setPath(request.getContextPath());
	embeddedCookie.setPath(request.getContextPath());
	response.addCookie(storeCookie);
	response.addCookie(localeCodeCookie);
	response.addCookie(embeddedCookie);
	
%>

<html lang="<%=SCEncoder.getEncoder().encodeForJavaScript(localeCode)%>">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title><%= SCUILocalizationHelper.getString(uiContext, "WSC_Product_Name")%></title>
		<link rel="SHORTCUT ICON" href="<%=request.getContextPath()%>/wsc/resources/css/icons/images/IBMLogoWindow.ico"/>

		<script type="text/javascript">
			var contextPath = '<%=request.getContextPath()%>';
			var dojoConfig = {
			    async:true,
			    locale:'<%=SCEncoder.getEncoder().encodeForJavaScript(localeCode)%>',
			    waitSeconds:5,
			    packages : [ {
						name : 'sc/plat',
						location :contextPath + "/platform/scripts/sc/plat"
					},
					{
						name : 'idx',
						location : contextPath + "/ibmjs/idx"
					},
					{
						name : 'scbase',
						location : contextPath + "/platform/scripts/sc/base"
					},					
					{
						name : 'wsc',
						location : contextPath + "/wsc"
					},
					{
						name : 'ias',
						location : contextPath + "/ias"
					},
					{
						name : 'gridx',
						location : contextPath + '/dojo/gridx'
					}

				]
			};
			dojoConfig.paths = {"sc/plat":contextPath+"/platform/scripts/sc/plat"};
		</script>


		<script src="<%=request.getContextPath()%>/dojo/dojo/dojo.js"></script>
		<script src="<%=request.getContextPath()%>/platform/scripts/sc/plat/dojo/base/loader.js"></script>

		<%
			if(SCUIUtils.isDevMode()) {
		%>
				<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dojo/dojo/resources/dojo.css"></link>
				<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dojo/dijit/themes/dijit.css"></link>
				<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/ibmjs/idx/themes/oneui/oneui.css"></link>
		<%
			} else {
		%>
				<link rel="stylesheet" type="text/css" href="<%=ProfileUtil.getInstance().getCSSFilePath("dojo","dojo/resources/dojo.css",false)%>"></link>
				<link rel="stylesheet" type="text/css" href="<%=ProfileUtil.getInstance().getCSSFilePath("dojo","dijit/themes/dijit.css",false)%>"></link>
				<link rel="stylesheet" type="text/css" href="<%=ProfileUtil.getInstance().getCSSFilePath("idx","idx/themes/oneui/oneui.css",false)%>"></link>
				<script src="<%=ProfileUtil.getInstance().getJSFilePath("dojo")%>"></script>
		<%
			}
		%>
		
	  	<script language="javascript" >
		
			scRequire(["scbase/loader!dojo/dom",
			           "scbase/loader!sc/plat/dojo/utils/BundleUtils",
			           "scbase/loader!ias/login/Login",
					   "dojo/ready","dojo/request/script"],
			           function(dDom,
			           	        scBundleUtils,
			           	        iasLogin,ready,script){

								   scBundleUtils.registerGlobalBundles("ias.resources.bundle","ias_login_bundle");
			           	           scBundleUtils.registerGlobalBundles("wsc.resources.bundle","wsc_login_bundle");
								   dDom.byId("sc_plat_dojo_widgets_ScreenDialogUnderlay_Initial").style.display="none";
			           	           iasLogin.init('<%=SCEncoder.getEncoder().encodeForJavaScript(errorMsg)%>');
			           	           
			           	           <% if(!SCUIUtils.isVoid(request.getParameter("EnterpriseCode"))) { %>
			           	              dDom.byId("enterpriseCode").value = '<%=SCEncoder.getEncoder().encodeForJavaScript(request.getParameter("EnterpriseCode"))%>';
			           	           <% } %>

								   ready(function(){
										<% if(SCUIUtils.isVoid(errorMsg)) { %>
											iasLogin.setFocusOnUsername();
										<% } %>
										<%
											if(!SCUIUtils.isDevMode()) {
										%>
										script.get("<%=ProfileUtil.getInstance().getJSFilePath("idx")%>");
										script.get("<%=ProfileUtil.getInstance().getJSFilePath("sc/plat")%>");										
										script.get("<%=ProfileUtil.getInstance().getJSFilePath("wsc","wsc_desktop")%>");

										script.get("<%=ProfileUtil.getInstance().getCSSFilePath("dojo","gridx/resources/Gridx.css",false)%>");
										script.get("<%=ProfileUtil.getInstance().getCSSFilePath("idx","idx/themes/oneui/idx/gridx/Gridx.css",false)%>");
										script.get("<%=ProfileUtil.getInstance().getCSSFilePath("idx","idx/themes/oneui/idx/gridx/pagination.css",false)%>");
										script.get("<%=ProfileUtil.getInstance().getCSSFilePath("sc/plat","sc/plat/dojo/themes/claro/platform.css",false)%>");
										script.get("<%=ProfileUtil.getInstance().getCSSFilePath("wsc","wsc/resources/css/desktop.css",false,"wsc_desktop")%>");

										<%
											}
										%>
     							  });
			  
			});
	 			
		</script>
		<style>
			.iasLoginError{
				margin-top: 10px;
				color: #FF0000;
				font-weight: bold;
			}
			
			.comapps {
				background-color: F9F9FA;
				background-image: url('<%=request.getContextPath()%>/wsc/resources/css/icons/images/Web_Login_Background.png');
			}

			.comapps .loginUserNameTextbox .idxLabel .idxRequiredIcon {
				background-image: url('<%=request.getContextPath()%>/wsc/resources/css/icons/images/user_white.png');
				width: 30px;
				height: 30px;
				background-repeat: no-repeat;
				position: absolute;	
				margin-top: 9px;
				margin-left: 5px;
				font-size: 0px;
				
			}

			

			.comapps .loginPasswordTextbox .idxLabel .idxRequiredIcon {
				background-image: url('<%=request.getContextPath()%>/wsc/resources/css/icons/images/password_white.png');
				width: 30px;
				height: 30px;
				background-repeat: no-repeat;
				position: absolute;
				margin-top: 9px;
				margin-left: 5px;
				font-size: 0px;
			}

			.comapps .idxLabel label {
				display:none;
			}

			
			.comapps .idxLoginFrame {
				text-align: center;
			}			
						
			.comapps .idxLoginFrameBox{
				background-color: transparent;
				border: none;
				box-shadow: none;
			}
			.comapps .idxLoginFrameTitle {
				font-family: "HelvNeueRomanforIBM", "Helvetica Neue",Helvetica, sans-serif, Arial, Tahoma, Verdana;
				font-size: 35px;
				color: #FFF;	
				margin-bottom: 50px;
			}
			.comapps .idxLoginFrameSubTitle {
				font-size: 15px;
				color: #FFF;
				display: none;			
			}
			
			.comapps .idxLoginFrameBox .idxLoginFrameInactivityMessage {
				font-family: "HelvNeueRomanforIBM", "Helvetica Neue",Helvetica, sans-serif, Arial, Tahoma, Verdana;
				font-size: 11px;
				color: #FFF;
				width: 360px;
				margin: 10px auto;
				display: none;			
			}

			.comapps .idxLoginFrameBox .idxLoginFrameButtonBar  {
				margin-right: 15px;
				margin-top: 40px
			}			
			
			.comapps .dijitValidationTextBox .dijitTextBox {
				height: 50px;
				width: 360px;
				background-color: #009dc2;
				border: none;			
			}
			.comapps .dijitValidationTextBox .dijitTextBox .dijitInputField {
				color: #FFF;
				font-size: 18px;
				padding-top: 14px;
				padding-left: 50px;
				background-color: transparent;				
			}
			
			.comapps .dijitValidationTextBox .dijitValidationTextBoxFocused .dijitInputContainer {
				background-color: none;
			}
			
			.comapps .idxLoginFrameBox .idxLoginFrameCopyright {
				margin-top: 20px;				
				position: absolute;
				width: 100%;
				left: 0;
				text-align: center;
				bottom: 0;
				padding-bottom: 40px;
				font-family: "HelvNeueRomanforIBM", "Helvetica Neue", Helvetica, sans-serif, Arial, Tahoma, Verdana;
				font-size: 10px;
				color: #FFF;
			}			

			.comapps .idxLoginFrame .dijitButton {
				margin-left: 0px;
			}

			.comapps .idxLoginFrame .dijitButton .dijitButtonNode{
				height: 40px;
				width: 340px;
				background: transparent;
				border: 1px solid #FFF;
			}
			
			.comapps .idxLoginFrame .dijitButton .dijitButtonNode .dijitButtonContents {
				margin-top: 11px;
				font-size: 18px;
				color: #FFF;
				font-weight: normal;
			}

			.oneui.comapps .idxHeaderContainer .idxHeaderPrimary {
				background: transparent;
				filter: inherit;
			}
			.oneui.comapps .idxHeaderContainer .idxHeaderPrimary .idxHeaderLogo {
				background: url('<%=request.getContextPath()%>/wsc/resources/css/icons/images/headerLogoWhite.png') no-repeat 35% 23%;
			}
			
			.oneui.comapps.oneui .idxHeaderContainer .idxHeaderBlueLip {
				background: transparent;
				filter: inherit;
			}

			.oneui.comapps .idxHeaderContainer .idxHeaderWidthLiquid .idxHeaderPrimary .idxHeaderLogoBox {
				background: transparent;
				border: none;
				box-shadow: none;
				filter: inherit;
			}
			
			.comapps .idxHeaderContainer .idxHeaderPrimary .idxHeaderPrimaryTitle {
				font-size: 1.3em;
				font-family: "HelvNeueRomanforIBM", "Helvetica Neue", Helvetica, Arial, Tahoma, Verdana, sans-serif;
				color: #ffffff;
				font-weight: normal;
			}			
				
				
				/*The entire section is a temp fix for idx. should remove when upgrade to new idx.
				The issue is in IE, the tool tip does not display at proper location when any error on field */
				
				.comapps .idxOneuiHoverHelpTooltip {
				
					position: absolute;
					z-index: 2000;
					display: block;
					left:0px;
					top: -10000px;
					overflow: visible; 
				}
				
				.comapps .idxOneuiHoverHelpTooltipRight {
					padding: 5px 5px 5px 9px;
					.idxOneuiHoverHelpTooltipCloseIcon{
						right:-8px;
						top: -8px;
					}
				}
				
				.comapps .idxOneuiHoverHelpTooltip, .comapps .idxOneuiHoverHelpTooltipDialog {
					background: transparent;
				}
				
				.comapps .idxOneuiHoverHelpTooltipContainer {
					background-color: #ffffff;
					background-repeat: repeat-x;
					background-position: bottom;
					border: 3px solid #999999;
					padding: 7px;
					-moz-border-radius: 4px;
					border-radius: 4px;
					-webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.25);
					-moz-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.25);
					box-shadow: 0 1px 6px rgba(0, 0, 0, 0.25);
					font-size: 1em;
					color: #222222;
					max-width: 600px;
				}
				
				.comapps .idxOneuiHoverHelpTooltipRight .idxOneuiHoverHelpTooltipConnector {
					left:-6px;
					background-position: -54px 0;
				}
				
				.comapps .idxOneuiHoverHelpTooltipConnector {
					position: absolute;
					border: 0;
					z-index: 2;
					background-image: url("../ibmjs/idx/themes/oneui/idx/widget/images/popupSharkfin.png");
					background-repeat: no-repeat;
					width: 18px;
					height: 18px;
				}
				
				.scMask {
					background-repeat: no-repeat;
					background-position: center;
					opacity: 0.5;
				}

				.scLoadingIconAndTextWrapper {
					position: absolute;
				}

				.scLoadingIcon {
					background-image: url('<%=request.getContextPath()%>/platform/scripts/sc/plat/dojo/themes/claro/images/loading_big.gif');
					background-repeat: no-repeat;
					background-position: 0px;
					height: 50px;
					width: 50px;
				}

				.scLoadingText {
					/*Make it so small that it is not visible.*/
					font-size: 0px;
				}
				
				
				
				
				/* Temp fix ends  */

				.comapps .loginStoreIdTextbox .idxLabel .idxRequiredIcon {
				background-image: url('<%=request.getContextPath()%>/wsc/resources/css/icons/images/store_logo.png');
				width: 30px;
				height: 30px;
				background-repeat: no-repeat;
				position: absolute;
				margin-top: 9px;
				margin-left: 5px;
				font-size: 0px;
				display:block !important;
			}

			
		</style>
 	</head>
	<body  class="oneui comapps">		
		<div id="appHeader"></div>
		<div role="main" aria-label="Login Frame" aria-live="assertive">
		<div id="loginFrame" />		
		<form id="fieldsForm"  class="iasLoginForm" method="POST" action="processLogin.do" aria-live="assertive">
			<input id="displayUserId" name="DisplayUserID" type="hidden" />
			<input id="password" name="Password" type="hidden" />
			<input id="enterpriseCode" name="EnterpriseCode" type="hidden" value='<%=SCEncoder.getEncoder().encodeForJavaScript(enterpriseCode)%>'/>
			<input id="StoreId" name="StoreId" type="hidden" value='<%=SCEncoder.getEncoder().encodeForJavaScript(storeId)%>'/>
			<input id="StoreLocaleCode" name="StoreLocaleCode" type="hidden" value='<%=SCEncoder.getEncoder().encodeForJavaScript(storeLocaleCode)%>'/>
			<input id="embedded" name="embedded" type="hidden" value='<%=SCEncoder.getEncoder().encodeForJavaScript(embedded)%>'/>
		
		</form>
		</div>		
		<div class="dijitDialogUnderlayWrapperLogin" role="alert" 
			id="sc_plat_dojo_widgets_ScreenDialogUnderlay_Initial" 
			widgetid="sc_plat_dojo_widgets_ScreenDialogUnderlay_Initial" 
			style="display: block; z-index: 949; top: 0px; left: 0px; position: fixed; width: 100%; height: 100%;">
			<div class="dijitDialogUnderlay scMask" dojoattachpoint="node" role="alert" 
				id="dialogId_underlay" style="width: 100%; height: 100%; top: -50px; left: -50px; text-align: center;">
			</div>
			<div style="width: 100%; height: 100%; top: -25px; left: -25px; position:absolute">
				<div class="scLoadingIconAndTextWrapper" dojoattachpoint="imageAndTextHolderNode" role="alert" style="top: 50%; left: 50%;">    
					<div class="scLoadingIcon" dojoattachpoint="loadingIconNode"> </div> 
					<div class="scLoadingText" dojoattachpoint="loadingTextNode" role="alert">&nbsp;<%= SCUILocalizationHelper.getString(uiContext, "Processing")%></div>    </div> 
				<iframe src='javascript:""' class="dijitBackgroundIframe" role="presentation" tabindex="-1" style="opacity: 0.1; width: 100%; height: 100%; border: none;"></iframe>
			</div>
		</div>
	</body> 
</html>

