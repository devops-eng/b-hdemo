/*
 * Licensed Materials - Property of IBM
 * IBM Sterling Order Management Store (5725-D10)
 * (C) Copyright IBM Corp. 2014 , 2015 All Rights Reserved. , 2015 All Rights Reserved.
 * US Government Users Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */
define({ root:
{
	"copyright":"IBM Sterling Store Licensed Materials - Property of IBM. (C) Copyright IBM Corporation and its licensors 2014, 2015. All Rights Reserved. IBM and the IBM logo are trademarks of IBM Corporation in the United States, other countries, or both.",
	"copyright_mobile":"IBM Sterling Store Licensed Materials - Property of IBM.<br>(C) Copyright IBM Corporation and its licensors 2014, 2015. All Rights Reserved.<br>IBM and the IBM logo are trademarks of IBM Corporation in the United States, other countries, or both.",
	"product_name":"IBM Sterling Store",
	"login_title":"IBM Sterling Store",	
	"login_failed_shipnode_authorization_error":"Invalid User. User does not belong to store",
	"No_Store_Permission":"User does not have store access.",
	"login_failed_shipnode_not_present_authorization_error":"Invalid User. User is not assigned to a store",
	"login_failed_shipnode_or_team_configuration_error":"Invalid User. User does not belong to store or Team configuration is not correct"
}, //EOF
// START NON-TRANSLATABLE 
	"de" : true,
    "es" : true,
	"fr" : true,
	"it" : true,
	"ja" : true,
	"ko" : true,
	"pl" : true,
	"pt" : true,
	"ru" : true,
	"tr" : true,
	"zh" : true,
	"zh-tw" : true
});
// END NON-TRANSLATABLE
