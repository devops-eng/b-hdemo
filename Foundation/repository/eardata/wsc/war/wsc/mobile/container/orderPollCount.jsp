<%
// Licensed Materials - Property of IBM
// IBM Sterling Field Sales
// (C) Copyright IBM Corp. 2009, 2011 All Rights Reserved.
// US Government Users Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
%>


<%@page import="com.sterlingcommerce.ui.web.framework.utils.SCUIUtils"%>
<%@page import="com.sterlingcommerce.framework.utils.SCXmlUtils"%>
<%@page import="com.sterlingcommerce.ui.web.framework.utils.SCUIContextHelper"%>
<%@page import="com.sterlingcommerce.ui.web.framework.context.SCUIUserPreferences"%>
<%@page import="com.sterlingcommerce.ui.web.framework.context.SCUIContext"%>
<%@page import="com.sterlingcommerce.ui.web.framework.utils.SCUIJSONUtils"%> 
<%@page import="org.w3c.dom.Element"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>

<%


SCUIContext uiContext = SCUIContextHelper.getUIContext(request, response);

Object ret = uiContext.getAttribute("getAlertBopusDashboardCount_output");
Element e = (Element)ret;
String json = SCUIJSONUtils.getJSONFromXML(e, uiContext);



%>
{output:<%=json%>}