scDefine([
	"scbase/loader!dojo/_base/lang",	
	"scbase/loader!wsc",	
	"scbase/loader!sc/plat/dojo/Userprefs",
	"scbase/loader!sc/plat/dojo/utils/BaseUtils",
	"scbase/loader!sc/plat/dojo/utils/BundleUtils",
	"scbase/loader!sc/plat/dojo/utils/ScreenUtils",
	"scbase/loader!sc/plat/dojo/utils/ModelUtils",	
	"scbase/loader!isccs/utils/UIUtils",
	"scbase/loader!isccs/utils/ContextUtils",
	"scbase/loader!isccs/utils/OrderUtils",
	"scbase/loader!isccs/utils/UOMUtils",
	"scbase/loader!sc/plat/dojo/utils/WidgetUtils",
	"scbase/loader!sc/plat/dojo/utils/EventUtils",
	"scbase/loader!sc/plat/dojo/widgets/Screen",
	"scbase/loader!sc/plat/dojo/utils/ControllerUtils",
	"scbase/loader!isccs/utils/BaseTemplateUtils",
	"scbase/loader!sc/plat/dojo/utils/EditorUtils",
	"scbase/loader!dojo/NodeList-manipulate"
	],
	function(dLang,wsc,scUserprefs,scBaseUtils,scBundleUtils,scScreenUtils,scModelUtils,isccsUIUtils,isccsContextUtils, scOrderUtils,scUOMUtils,scWidgetUtils,scEventUtils,scScreen,scControllerUtils,isccsBaseTemplateUtils,scEditorUtils){
		var customerPickUpUtils = dLang.getObject("utils.CustomerPickUpUtils", true,wsc);

		customerPickUpUtils.getPickupSummaryModel = function(resolutionModel,customerPickupOrderListModel,cancellationReasonCode){
			var pickUpSummaryModel = {};			
			pickUpSummaryModel.Shipment={};
			
			var shipment = scBaseUtils.cloneModel(customerPickupOrderListModel.Shipment);
				shipment.IncludedShipmentLines = {};
				shipment.RemainingShipmentLines = {};
				shipment.CancelledShipmentLines = {};
				shipment.IncludedShipmentLines.ShipmentLines={};
				shipment.RemainingShipmentLines.ShipmentLines={};
				shipment.CancelledShipmentLines.ShipmentLines={};
				shipment.IncludedShipmentLines.ShipmentLines.ShipmentLine = [];
				shipment.RemainingShipmentLines.ShipmentLines.ShipmentLine = [];
				shipment.CancelledShipmentLines.ShipmentLines.ShipmentLine = [];
				var k=0;
				var m=0;
				var c=0;
				var confirmedLine = null;
				var shortagesLine = null;
				for(j=0;j<resolutionModel.ConfirmedShipmentLines.ShipmentLine.length;j++){
					confirmedLine = resolutionModel.ConfirmedShipmentLines.ShipmentLine[j];
					if (confirmedLine.ShipmentKey == shipment.ShipmentKey){
						for(p=0;p<shipment.ShipmentLines.ShipmentLine.length;p++){
							if(confirmedLine.ShipmentLineKey==shipment.ShipmentLines.ShipmentLine[p].ShipmentLineKey){
								if(Number(confirmedLine.Quantity) > 0){
									shipment.IncludedShipmentLines.ShipmentLines.ShipmentLine[k] = scBaseUtils.cloneModel(shipment.ShipmentLines.ShipmentLine[p]);
									shipment.ShipmentLines.ShipmentLine[p].PickedQty=confirmedLine.Quantity;
									shipment.IncludedShipmentLines.ShipmentLines.ShipmentLine[k].Quantity = confirmedLine.Quantity;
									k++;
								}
								break;
							}
						}

					}
				}
				if(!scBaseUtils.isVoid(resolutionModel.ShortageShipmentLines)){
					for(j=0;j<resolutionModel.ShortageShipmentLines.ShipmentLine.length;j++){
						shortagesLine = resolutionModel.ShortageShipmentLines.ShipmentLine[j];
						if (shortagesLine.ShipmentKey == shipment.ShipmentKey){
							for(s=0;s<shipment.ShipmentLines.ShipmentLine.length;s++){
								if(shortagesLine.ShipmentLineKey==shipment.ShipmentLines.ShipmentLine[s].ShipmentLineKey){
									
									shipment.ShipmentLines.ShipmentLine[s].ShortageQuantity=shortagesLine.ShortageQuantity;
									
									shipment.ShipmentLines.ShipmentLine[s].ShortageReason=shortagesLine.ShortageReason;
									
									if(!scBaseUtils.isVoid(shortagesLine.ShortageReason) && shortagesLine.ShortageReason=="Cancel" ){
										shipment.ShipmentLines.ShipmentLine[s].ReasonCode=cancellationReasonCode.CancellationReasonCode;
										shipment.CancelledShipmentLines.ShipmentLines.ShipmentLine[c] = scBaseUtils.cloneModel(shipment.ShipmentLines.ShipmentLine[s]);
										shipment.CancelledShipmentLines.ShipmentLines.ShipmentLine[c].Quantity = shortagesLine.ShortageQuantity;
										c++;
									}
									else{
										shipment.RemainingShipmentLines.ShipmentLines.ShipmentLine[m] = scBaseUtils.cloneModel(shipment.ShipmentLines.ShipmentLine[s]);
										shipment.RemainingShipmentLines.ShipmentLines.ShipmentLine[m].Quantity = shortagesLine.ShortageQuantity;
										m++;
									}
									
									break;
								}
							}
						}
					}
				}
				//if(k>0){
					pickUpSummaryModel.Shipment=shipment;					
				//}
			
			//console.log(pickUpSummaryModel);
			return pickUpSummaryModel;
		};	

		customerPickUpUtils.validateShortageResolution = function(resolutionModel){
			/*var status = false;
			if( !scBaseUtils.isVoid(resolutionModel.ShortageReason) && !(resolutionModel.ShortageReason=="Cancel" && scBaseUtils.isVoid(resolutionModel.ReasonCode))){
				status = true;
			}*/
			return !scBaseUtils.isVoid(resolutionModel.ShortageReason);
		};

		customerPickUpUtils.validateResolutionModel=function(/*Model*/ resolutionModel){

			var shortageReason = null;
			var status=true;
			if(!scBaseUtils.isVoid(resolutionModel.ShortageShipmentLines)){
				for(i=0;i<resolutionModel.ShortageShipmentLines.ShipmentLine.length;i++){
					shortageReason=resolutionModel.ShortageShipmentLines.ShipmentLine[i].ShortageReason;
					//reasonCode=resolutionModel.ShortageShipmentLines.ShipmentLine[i].ReasonCode;
					if(scBaseUtils.isVoid(shortageReason)){
						status = false;
						break;
					}

				}
			}
			/*if(!scBaseUtils.isVoid(resolutionModel.OverageShipmentLines)){
				for(i=0;i<resolutionModel.OverageShipmentLines.ShipmentLine.length;i++){
					if(resolutionModel.OverageShipmentLines.ShipmentLine[i].ResolutionStatus!="Y"){
						overageResolved=false;
						break;
					}
				}
			}*/
			
			return status;// && overageResolved;
		};

		customerPickUpUtils.hasCancelLinesInResolutionModel=function(/*Model*/ resolutionModel){

			var shortageReason = null;		
			var status=false;
			if(!scBaseUtils.isVoid(resolutionModel.ShortageShipmentLines)){
				for(i=0;i<resolutionModel.ShortageShipmentLines.ShipmentLine.length;i++){
					shortageReason=resolutionModel.ShortageShipmentLines.ShipmentLine[i].ShortageReason;					
					if(shortageReason=="Cancel"){
						status = true;
						break;
					}
				}
			}			
			return status;
		};

		customerPickUpUtils.getShipmentLinesForSelectedShipments = function(pickUpOrderDetailsModel,selectedShipmentModel){
			//console.log("pickUpOrderDetailsModel : ",pickUpOrderDetailsModel);
			//console.log("selectedShipmentModel : ",selectedShipmentModel);

			var productScanShipmentLineListModel = {};
			productScanShipmentLineListModel.ShipmentLines = {};
			productScanShipmentLineListModel.ShipmentLines.ShipmentLine = [];

			var productScanShipmentArray = scBaseUtils.getNewArrayInstance();

			if(!scBaseUtils.isVoid(selectedShipmentModel.Shipments.Shipment.length)) {
				for(var i=0;i<selectedShipmentModel.Shipments.Shipment.length;i++) {
					if("Y" == selectedShipmentModel.Shipments.Shipment[i].IsSelectedForPickup) {
						var pickUpShipmentKey = selectedShipmentModel.Shipments.Shipment[i].ShipmentKey;
						if(!scBaseUtils.isVoid(pickUpOrderDetailsModel.Shipments.Shipment.length)) {
							for(var j=0;i<pickUpOrderDetailsModel.Shipments.Shipment.length;j++) {
									var tempShipmentKey = pickUpOrderDetailsModel.Shipments.Shipment[j].ShipmentKey;
									if(pickUpShipmentKey == tempShipmentKey) {
										if(!scBaseUtils.isVoid(pickUpOrderDetailsModel.Shipments.Shipment[j].ShipmentLines.ShipmentLine.length)) {

											for(var z=0;z<pickUpOrderDetailsModel.Shipments.Shipment[j].ShipmentLines.ShipmentLine.length;z++) {

												 if(productScanShipmentArray, pickUpOrderDetailsModel.Shipments.Shipment[j].ShipmentLines.ShipmentLine[z].OrderLine.IsBundleParent != "Y") {
													scBaseUtils.appendToArray(productScanShipmentArray, pickUpOrderDetailsModel.Shipments.Shipment[j].ShipmentLines.ShipmentLine[z]);
												}
											}
										}
										break;
									}
							}
						}
					}
				}
			}


			productScanShipmentLineListModel.ShipmentLines.ShipmentLine = productScanShipmentArray;
			//console.log("productScanShipmentLineList : ",productScanShipmentLineListModel);

			return productScanShipmentLineListModel;
		};

		/*customerPickUpUtils.getSelectedShipments = function(pickUpOrderDetailsModel,selectedShipmentModel){
			
			var selectedShipmentsModel = {};
			selectedShipmentsModel.Shipments = {};
			selectedShipmentsModel.Shipments.Shipment = [];
			selectedShipmentsModel.NoShipmentSelected="N";

			if(!scBaseUtils.isVoid(selectedShipmentModel.Shipments.Shipment.length)) {
				for(var i=0;i<selectedShipmentModel.Shipments.Shipment.length;i++) {
					if("Y" == selectedShipmentModel.Shipments.Shipment[i].IsSelectedForPickup) {
						var pickUpShipmentKey = selectedShipmentModel.Shipments.Shipment[i].ShipmentKey;
						if(!scBaseUtils.isVoid(pickUpOrderDetailsModel.Shipments.Shipment.length)) {
							for(var j=0;i<pickUpOrderDetailsModel.Shipments.Shipment.length;j++) {
								var tempShipmentKey = pickUpOrderDetailsModel.Shipments.Shipment[j].ShipmentKey;
								if(pickUpShipmentKey == tempShipmentKey) {
									selectedShipmentsModel.Shipments.Shipment[selectedShipmentsModel.Shipments.Shipment.length]=scBaseUtils.cloneModel(pickUpOrderDetailsModel.Shipments.Shipment[j]);
									break;
								}
							}
						}
					} 
				}
			}
			//console.log("length : ",selectedShipmentsModel.Shipments.Shipment.length);
			if(selectedShipmentsModel.Shipments.Shipment.length == "0") {
				selectedShipmentsModel.NoShipmentSelected="Y";
			}
			//console.log("selectedShipmentsModel : ",selectedShipmentsModel);
			
			return selectedShipmentsModel;
		};*/
		
		/*customerPickUpUtils.updateProductQuantity = function(currentProductGridModel, productDetailModel){

			//console.log("currentProductGridModel : ",currentProductGridModel);
			//console.log("productDetailModel : ",productDetailModel);
			var productItemFound = false;

			if(!scBaseUtils.isVoid(currentProductGridModel.ShipmentLines.ShipmentLine.length)) {
				for(var i=0;i<currentProductGridModel.ShipmentLines.ShipmentLine.length;i++) {
					var itemID = currentProductGridModel.ShipmentLines.ShipmentLine[i].OrderLine.ItemDetails.ItemID;
					//console.log("itemID :",itemID);
					//console.log("productItemFound : ",productItemFound);
					if( (!scBaseUtils.isVoid(productDetailModel.BarCode.Translations.Translation.length)) && !productItemFound) {
						for(var j=0;j<productDetailModel.BarCode.Translations.Translation.length;j++) {
							if(itemID == productDetailModel.BarCode.Translations.Translation[j].ItemContextualInfo.ItemID) {
								//console.log("asd : ",currentProductGridModel.ShipmentLines.ShipmentLine[i].PickedQuantity);
								if(scBaseUtils.isVoid(currentProductGridModel.ShipmentLines.ShipmentLine[i].PickedQuantity)) {
									currentProductGridModel.ShipmentLines.ShipmentLine[i].PickedQuantity = productDetailModel.BarCode.Translations.Translation[j].ItemContextualInfo.Quantity;
									//console.log("PickedQty : ",productDetailModel.BarCode.Translations.Translation[j].ItemContextualInfo.Quantity);
								} else {
									var pickedQuantity = currentProductGridModel.ShipmentLines.ShipmentLine[i].PickedQuantity;
									//console.log("PickedQty 2 : ",pickedQuantity);
									pickedQuantity = Number(pickedQuantity) + Number(productDetailModel.BarCode.Translations.Translation[j].ItemContextualInfo.Quantity);
									currentProductGridModel.ShipmentLines.ShipmentLine[i].PickedQuantity = pickedQuantity;
								}

								productItemFound = true;
								break;

							}
						}
					} else {
						break;
					}
				}
			}

			//console.log("currentProductGridModel : ",currentProductGridModel);

			return currentProductGridModel;
		};*/


		/*customerPickUpUtils.getRecordCustomerPickShipments = function(custVerificationNotesModel, summaryModel){
			var mulitApiInput = {};
			mulitApiInput.MultiApi={};
			mulitApiInput.MultiApi.API=[];
			var pickModel = {};
			pickModel.Shipments = {};
			var shipments = [];
			var shipment = null;
			var shipmentLine = null;
			var mShipment =null;
			var mShipmentLine = null;
			for(i=0;i<summaryModel.Shipments.Shipment.length;i++){
				mShipment = summaryModel.Shipments.Shipment[i];
				shipment = {};
				shipment.ShipmentKey = mShipment.ShipmentKey;
				shipment.TransactionId="CONFIRM_SHIPMENT";
				shipment.ShipmentLines={};
				shipment.ShipmentLines.ShipmentLine=[];
				for(j=0;j<mShipment.ShipmentLines.ShipmentLine.length;j++){
					mShipmentLine = mShipment.ShipmentLines.ShipmentLine[j];
					shipmentLine = {};
					shipmentLine.ShipmentLineKey = mShipmentLine.ShipmentLineKey;
					shipmentLine.ShipmentLineNo = mShipmentLine.ShipmentLineNo;
					shipmentLine.ShipmentSubLineNo = mShipmentLine.ShipmentSubLineNo;
					shipmentLine.Quantity = mShipmentLine.Quantity;
					if(scBaseUtils.stringEquals("Y",mShipmentLine.OrderLine.IsBundleParent)) {
						shipmentLine.PickedQty = mShipmentLine.Quantity;
					}
					if(!scBaseUtils.isVoid(mShipmentLine.PickedQty)){
						shipmentLine.PickedQty = mShipmentLine.PickedQty;
					}
					if(!scBaseUtils.isVoid(mShipmentLine.ShortageReason)){
						shipmentLine.ShortageQuantity = mShipmentLine.ShortageQuantity;
						shipmentLine.ShortageReason = mShipmentLine.ShortageReason;
						if(mShipmentLine.ShortageReason=="Cancel" && !scBaseUtils.isVoid(mShipmentLine.ReasonCode)){
							shipmentLine.ReasonCode = mShipmentLine.ReasonCode;
						}
					}
					if(mShipmentLine.Quantity==0 || mShipmentLine.Quantity=="0"){
						shipmentLine.PickedQty = "0";
					}
					shipment.ShipmentLines.ShipmentLine[j]=shipmentLine;
				}
				shipments[i] = shipment;
				shipment.Notes = custVerificationNotesModel.Notes;
				mulitApiInput.MultiApi.API[i] = {};
				mulitApiInput.MultiApi.API[i].Input={};
				mulitApiInput.MultiApi.API[i].Input.Shipment=shipment;
			}
			pickModel.Shipments.Shipment = shipments;
			//console.log(mulitApiInput);

			return mulitApiInput;
		};*/
		
		customerPickUpUtils.getRecordCustomerPickShipmentModel = function(custVerificationNotesModel,/*Model*/ summaryModel){			
			var pickModel = {};			
			var shipment = null;
			var shipmentLine = null;
			var mShipment =null;
			var mShipmentLine = null;
			
				mShipment = summaryModel.Shipment;
				shipment = {};
				shipment.ShipmentKey = mShipment.ShipmentKey;
				shipment.TransactionId="CONFIRM_SHIPMENT";
				shipment.ShipmentLines={};
				shipment.ShipmentLines.ShipmentLine=[];
				var k=0;
				for(j=0;j<mShipment.ShipmentLines.ShipmentLine.length;j++){
					mShipmentLine = mShipment.ShipmentLines.ShipmentLine[j];
					if(mShipmentLine.Quantity==0 || mShipmentLine.Quantity=="0"){
						continue ;
					}
					shipmentLine = {};
					shipmentLine.ShipmentLineKey = mShipmentLine.ShipmentLineKey;
					shipmentLine.ShipmentLineNo = mShipmentLine.ShipmentLineNo;
					shipmentLine.ShipmentSubLineNo = mShipmentLine.ShipmentSubLineNo;
					shipmentLine.Quantity = mShipmentLine.Quantity;
					if(scBaseUtils.stringEquals("Y",mShipmentLine.OrderLine.IsBundleParent)) {
						shipmentLine.PickedQty = mShipmentLine.Quantity;
					}
					if(!scBaseUtils.isVoid(mShipmentLine.PickedQty)){
						shipmentLine.PickedQty = mShipmentLine.PickedQty;
					}
					if(!scBaseUtils.isVoid(mShipmentLine.ShortageReason)){
						shipmentLine.ShortageQuantity = mShipmentLine.ShortageQuantity;
						shipmentLine.ShortageReason = mShipmentLine.ShortageReason;
						if(mShipmentLine.ShortageReason=="Cancel" && !scBaseUtils.isVoid(mShipmentLine.ReasonCode)){
							shipmentLine.ReasonCode = mShipmentLine.ReasonCode;
						}
					}
					
					shipment.ShipmentLines.ShipmentLine[k]=shipmentLine;
					k++;
				}
				
				shipment.Notes = custVerificationNotesModel.Notes;				
				pickModel.Shipment=shipment;
			return pickModel;
		};

		customerPickUpUtils.processRecordCustomerPickMultiApiOutput =function(/*Model*/ modelOutput){

			//console.log(modelOutput);
			return "Confirmed";
		};

		customerPickUpUtils.getShortageResolutionModel = function(/*Model*/ pickedModel){
			var shortageResolutionModel = {};
			//shortageResolutionModel.hasOverages = "N";
			shortageResolutionModel.hasShortages = "N";
			shortageResolutionModel.hasConfirmed = "N";
			shortageResolutionModel.hasShipmentsWithNoPickItems = "N";
			shortageResolutionModel.hasCancelledLines = "N";
			//shortageResolutionModel.OverageShipmentLines = {};
			shortageResolutionModel.ShortageShipmentLines = {};
			shortageResolutionModel.UnresolvedShipmentLines = {};
			shortageResolutionModel.ConfirmedShipmentLines = {};
			//shortageResolutionModel.OverageShipmentLines.ShipmentLine = [];
			shortageResolutionModel.ShortageShipmentLines.ShipmentLine = [];
			shortageResolutionModel.UnresolvedShipmentLines.ShipmentLine = [];
			shortageResolutionModel.ConfirmedShipmentLines.ShipmentLine = [];

			//var overages = shortageResolutionModel.OverageShipmentLines.ShipmentLine;
			var shortages = shortageResolutionModel.ShortageShipmentLines.ShipmentLine;
			var unresolved = shortageResolutionModel.UnresolvedShipmentLines.ShipmentLine;
			var confirmed = shortageResolutionModel.ConfirmedShipmentLines.ShipmentLine;
			var cLines = 0;
			//var oLines = 0;
			var sLines = 0;
			var uLines =0;
			var cancelledLines = 0;
			var shipmentPickCount=[];


			var pickedLines = pickedModel.ShipmentLines.ShipmentLine;
			var c=0;
			/*for(var i=0;i<pickedLines.length;i++){			
				
				var shipmentFound = false;
				for(var j=0;j<shipmentPickCount.length;j++){			
					
					if(shipmentPickCount[j].ShipmentKey==pickedLines[i].ShipmentKey){
						var shipmentFound = true;
						break;
					}		
				}
				if(!shipmentFound){
					shipmentPickCount[c]={};
					shipmentPickCount[c].ShipmentKey=pickedLines[i].ShipmentKey;
					var pickedQty = !scBaseUtils.isVoid(pickedLines[i].PickedQty)?Number(pickedLines[i].PickedQty):Number('0');
					shipmentPickCount[c].pickedQty=pickedQty;
					c++;
				}
				else{
					for(var k=0;k<shipmentPickCount.length;k++){			
					
						if(shipmentPickCount[k].ShipmentKey==pickedLines[i].ShipmentKey){
							var shipmentFound = true;
							var pickedQty = !scBaseUtils.isVoid(pickedLines[i].PickedQty)?Number(pickedLines[i].PickedQty):Number('0');
							shipmentPickCount[k].pickedQty+=pickedQty;
						}		
					}
				}
				
			}*/
			
			//console.log(shipmentPickCount);
			
			/*for(var k=0;k<shipmentPickCount.length;k++){
				if(shipmentPickCount[k].pickedQty==0){
					shortageResolutionModel.hasShipmentsWithNoPickItems="Y";
					break;
				}			
			}*/
			
			var format = "formattedQty";
			
			for(var i=0;i<pickedLines.length;i++){
				var pickedQty = !scBaseUtils.isVoid(pickedLines[i].PickedQty)?Number(pickedLines[i].PickedQty):Number('0');
				pickedLines[i].PickedQty=""+pickedQty;
				var resolutionQty = pickedQty - Number(pickedLines[i].Quantity);
				var pickedQuantity = scUOMUtils.getFormatedQuantityWithUom(""+pickedQty,'',format);				
				var shortageQuantity = scUOMUtils.getFormatedQuantityWithUom(-(resolutionQty)+'','',format);
				
				if(resolutionQty>=0){
					confirmed[cLines]=scBaseUtils.cloneModel(pickedLines[i]);					
					cLines++;
				}
				/*else if(resolutionQty>0){
					confirmed[cLines]=scBaseUtils.cloneModel(pickedLines[i]);
					overages[oLines]=scBaseUtils.cloneModel(pickedLines[i]);
					overages[oLines].Quantity = ""+resolutionQty;
					overages[oLines].ResolutionStatus="";
					cLines++;
					oLines++;
				}*/
				else if(resolutionQty<0){
					if(pickedQty>0){
						confirmed[cLines]=scBaseUtils.cloneModel(pickedLines[i]);
						confirmed[cLines].Quantity = pickedQuantity + "";
						cLines++;
					}
					var shortageLine = scBaseUtils.cloneModel(pickedLines[i]);
					if(!scBaseUtils.isVoid(shortageLine.ShortageReason)){
						shortages[sLines]=shortageLine;
						shortages[sLines].ShortageQuantity=shortageQuantity + "";
						shortages[sLines].ReasonCode="";						
						sLines++;
						if(shortageLine.ShortageReason=="Cancel"){
							cancelledLines++;
						}
					}
					else{
						unresolved[uLines]=shortageLine;
						unresolved[uLines].ShortageQuantity=shortageQuantity + "";
						unresolved[uLines].ReasonCode="";
						unresolved[uLines].ShortageReason="";
						uLines++;					
					}
					
				}
			}
			if(cLines>0){
				shortageResolutionModel.hasConfirmed = "Y";
			}
			/*if(oLines>0){
				shortageResolutionModel.hasOverages = "Y";
			}*/
			if(uLines>0){
				shortageResolutionModel.hasShortages = "Y";
			}
			if(cancelledLines>0){
				shortageResolutionModel.hasCancelledLines = "Y";				
			}

			//console.log(shortageResolutionModel);

			return shortageResolutionModel;

		};

		customerPickUpUtils.mergeResolutionModel=function(shortageResolutionModel,shortageResolutionModel_Resolved){
			var model = scBaseUtils.cloneModel(shortageResolutionModel);
			if(!scBaseUtils.isVoid(shortageResolutionModel_Resolved.ShortageShipmentLines) && shortageResolutionModel_Resolved.ShortageShipmentLines.ShipmentLine.length>0){
				var shortageCount = model.ShortageShipmentLines.ShipmentLine.length;
				for(i=0;i<shortageResolutionModel_Resolved.ShortageShipmentLines.ShipmentLine.length;i++){
					model.ShortageShipmentLines.ShipmentLine[shortageCount] = scBaseUtils.cloneModel(shortageResolutionModel_Resolved.ShortageShipmentLines.ShipmentLine[i]);
					shortageCount++;
				}			
			}			
			return model;
		};

		customerPickUpUtils.openInvalidProductScannedConfirmationPopup = function(screen, invalidProductScannedBundle){

			var localized = invalidProductScannedBundle;

			if (scScreenUtils.hasBundleKey(screen, invalidProductScannedBundle)) {
					localized = scScreenUtils.getString(screen, invalidProductScannedBundle);
			}
			//console.log("popupMsg : ",localized);
			textObj = {};
			textObj.OK=scScreenUtils.getString(screen,"Ok");
			scScreenUtils.showErrorMessageBox(screen,localized,"", textObj,"");


		};

		customerPickUpUtils.processScannedProduct = function(targetModel) {
			var PickAll_Input = {};
			var pickedQty=Number("0");
			
			if(scBaseUtils.isVoid(targetModel.PickedQty)) {
				pickedQty=Number("0");
			} else {
				pickedQty=Number(targetModel.PickedQty);
			}
			PickAll_Input.Quantity = Number(pickedQty) + Number("1");			
			return PickAll_Input;
		};

		/*customerPickUpUtils.isScannedProductValid = function(selectedShipmentLinesModel,productDetailModel) {

			//console.log("selectedShipmentLinesModel : ",selectedShipmentLinesModel);
			//console.log("productDetailModel : ",productDetailModel);

			var isProductValid = true;

			return isProductValid;


		};*/
		
		customerPickUpUtils.validateScannedQuantity = function(screen,targetModel, productScanImagePanel,cmbShortagesResolution) {
			
			//console.log("validateScannedQuantity");
			//var targetModel = scScreenUtils.getTargetModel(screen,"ShipmentLine_Output",null);
			//console.log("target Model : ",targetModel);
			scWidgetUtils.removeClass(screen,"shipmentLineDetailsContainer","completedLine");
			//scWidgetUtils.removeClass(screen,"shipmentLineDetailsContainer","shortedLine");	
			
			
			if(!scBaseUtils.isVoid(targetModel) && !scBaseUtils.isVoid(targetModel.ShipmentLine.PickedQty)) {
				if(scBaseUtils.numberGreaterThan(Number(targetModel.ShipmentLine.PickedQty),Number(targetModel.ShipmentLine.Quantity))){
					scWidgetUtils.hideWidget(screen,productScanImagePanel,false);
					scWidgetUtils.hideWidget(screen,cmbShortagesResolution,false);					
					textObj = {};
					textObj.OK=scScreenUtils.getString(screen,"Ok");
					scScreenUtils.showErrorMessageBox(screen,scScreenUtils.getString(screen,"ItemOverrage"),"", textObj,"");
				} else if(scBaseUtils.numberEquals(Number(targetModel.ShipmentLine.PickedQty),Number(targetModel.ShipmentLine.Quantity))) {
					scWidgetUtils.showWidget(screen,productScanImagePanel,false,"");
					scWidgetUtils.hideWidget(screen,cmbShortagesResolution,false);
					scWidgetUtils.addClass(screen,"shipmentLineDetailsContainer","completedLine");					
				} else if(scBaseUtils.numberLessThan(Number(targetModel.ShipmentLine.PickedQty),Number(targetModel.ShipmentLine.Quantity))) {
					scWidgetUtils.hideWidget(screen,productScanImagePanel,false);
					scWidgetUtils.showWidget(screen,cmbShortagesResolution,false,"");	
				} else if (scBaseUtils.numberEquals(Number("0"),Number(targetModel.ShipmentLine.PickedQty))){
					scWidgetUtils.hideWidget(screen,productScanImagePanel,false);
					scWidgetUtils.showWidget(screen,cmbShortagesResolution,false,"");	
				}
			}
			
			if(!scBaseUtils.isVoid(targetModel) && scBaseUtils.isVoid(targetModel.ShipmentLine.PickedQty)) {
				scWidgetUtils.hideWidget(screen,productScanImagePanel,false);
				scWidgetUtils.showWidget(screen,cmbShortagesResolution,false,"");	
			}
			
			
		};


		customerPickUpUtils.getPrintModel = function(summaryModel,modelType){
			var printModel={};
			printModel.ShipmentList={};
			printModel.ShipmentList.Shipment=[];

			if(modelType=="SummaryList"){		
				var p=0;
				/*for(i=0;i<summaryModel.Shipments.Shipment.length;i++){
					//if(summaryModel.Shipments.Shipment[i].IncludedShipmentLines.ShipmentLines.length>0){
						printModel.ShipmentList.Shipment[p]={};
						printModel.ShipmentList.Shipment[p].ShipmentKey=summaryModel.Shipments.Shipment[i].ShipmentKey;
						printModel.ShipmentList.Shipment[p].ShipNode=summaryModel.Shipments.Shipment[i].ShipNode;
						printModel.ShipmentList.Shipment[p].Currency=summaryModel.Shipments.Shipment[i].Currency;
						printModel.ShipmentList.Shipment[p].Loginid=scUserprefs.getUserId();
						
						
						p++;
					//}*
				}*/
				printModel.ShipmentList.Shipment[p]={};
				printModel.ShipmentList.Shipment[p].ShipmentKey=summaryModel.Shipment.ShipmentKey;
				printModel.ShipmentList.Shipment[p].ShipNode=summaryModel.Shipment.ShipNode;
				printModel.ShipmentList.Shipment[p].Currency=summaryModel.Shipment.Currency;
				printModel.ShipmentList.Shipment[p].Loginid=scUserprefs.getUserId();
			}
			else if (modelType=="MashupOutput"){
			// handle for mashup output
				if(!scBaseUtils.isVoid(summaryModel.MultiApi.API.length)){
					for(i=0;i<summaryModel.Shipments.Shipment.length;i++){
						printModel.ShipmentList.Shipment[i]={};
						printModel.ShipmentList.Shipment[i].ShipmentKey=summaryModel.MultiApi.API[i].Output.Shipment.ShipmentKey;
						printModel.ShipmentList.Shipment[i].ShipNode=summaryModel.MultiApi.API[i].Output.Shipment.ShipNode;
						printModel.ShipmentList.Shipment[i].Currency=summaryModel.MultiApi.API[i].Output.Shipment.Currency;
						printModel.ShipmentList.Shipment[i].Loginid=scUserprefs.getUserId();

					}
				}
			}
			else if(modelType=="PrintOrder"){
				//if(summaryModel.ShipmentList.Shipment.IncludedLineCount>0){
					printModel=summaryModel;
					printModel.ShipmentList.Shipment[0].Loginid=scUserprefs.getUserId();
				//}
			}
			//console.log(printModel);
			return printModel;
		};

		customerPickUpUtils.validateSearchPickUpOrderInput = function(pickUpOrderTargetModel){
			
			//console.log("pickUpOrderTargetModel : ",pickUpOrderTargetModel);
			
			var throwError = false, isNameComboValid = false, isNameEmpty = false;
			var hasOrderNo = false, hasEmailID = false, hasPhoneNo = false;
			var errorMsg = '';			
			var errorJSON = {};
			
			if(scBaseUtils.isVoid(pickUpOrderTargetModel.Shipment)) {
				throwError = true;
				errorMsg = "pickUpOrderSearchCriteriaMsg";	
			}

			if( !throwError && !scBaseUtils.isVoid(pickUpOrderTargetModel.Shipment.ShipmentLines) && !scBaseUtils.isVoid(pickUpOrderTargetModel.Shipment.ShipmentLines.ShipmentLine.OrderNo)) {
				hasOrderNo = true;
			}

			if(!throwError && !scBaseUtils.isVoid(pickUpOrderTargetModel.Shipment.BillToAddress)) {
			
				var firstName = pickUpOrderTargetModel.Shipment.BillToAddress.FirstName;
				var lastName = pickUpOrderTargetModel.Shipment.BillToAddress.LastName;
				var emailID = pickUpOrderTargetModel.Shipment.BillToAddress.EMailID;
				var phoneNo = pickUpOrderTargetModel.Shipment.BillToAddress.DayPhone;


				if(!scBaseUtils.isVoid(firstName)){
					if(scBaseUtils.isVoid(lastName)) {
						throwError = true;
						errorMsg = "firstAndLastNameMsg";
					} else {
						isNameComboValid = true;
					}
				} else {
					if(!scBaseUtils.isVoid(lastName)) {
						throwError = true;
						errorMsg = "firstAndLastNameMsg";
					} else{
						isNameEmpty = true;
					}
				}
				
				if(!scBaseUtils.isVoid(emailID)) {
					hasEmailID = true;
				}
				
				if(!scBaseUtils.isVoid(phoneNo)) {
					hasPhoneNo = true;
				}
				
			}
			
			if(throwError) {
				errorJSON.hasError = throwError;
				errorJSON.errorMsg = errorMsg;			
			} else if(!isNameComboValid && !hasOrderNo && !hasEmailID && !hasPhoneNo) {
				errorJSON.hasError = true;
				errorJSON.errorMsg = "pickUpOrderSearchCriteriaMsg";				
			}
			
			//console.log("errorJSON : ",errorJSON);
			
			return errorJSON; 
		};	
		
		
		
		/*customerPickUpUtils.showPickUpOrderInfoPanel = function(screen,msgPanelUId,infoMsg) {

			//console.log("msgPanelUId : ",msgPanelUId);
			//console.log("msgPanelUId : ",infoMsg);

			var messageOptions = {
				'messagePanelId': msgPanelUId
			};

			isccsBaseTemplateUtils.displayMessage(screen, infoMsg, 'information', messageOptions);
			scWidgetUtils.showWidget(screen, msgPanelUId);

		};*/


		customerPickUpUtils.concatCustomerName = function(screen,shipmentModel) {
		
			//console.log("screen : ",screen);
			//console.log("shipmentModel : ",shipmentModel);
			
			var customerName = "",isPersonInfoMarkForPresent = false;
			
			if(!scBaseUtils.isVoid(shipmentModel.Shipment.ShipmentLines.ShipmentLine.length)) {
			
				for(var i=0;i<shipmentModel.Shipment.ShipmentLines.ShipmentLine.length;i++) {
				
					if(!scBaseUtils.isVoid(shipmentModel.Shipment.ShipmentLines.ShipmentLine[i].OrderLine.PersonInfoMarkFor)) {
					
						isPersonInfoMarkForPresent = true;
						if(!scBaseUtils.isVoid(shipmentModel.Shipment.ShipmentLines.ShipmentLine[i].OrderLine.PersonInfoMarkFor.FirstName)) {
						
							customerName = shipmentModel.Shipment.ShipmentLines.ShipmentLine[i].OrderLine.PersonInfoMarkFor.FirstName +" "+shipmentModel.Shipment.ShipmentLines.ShipmentLine[i].OrderLine.PersonInfoMarkFor.LastName;	
							
						} else {
						
							customerName = shipmentModel.Shipment.ShipmentLines.ShipmentLine[i].OrderLine.PersonInfoMarkFor.LastName;	
						
						}

						break;
					
					}
				
				}
			
			}
			
			if(!isPersonInfoMarkForPresent && !scBaseUtils.isVoid(shipmentModel.Shipment.BillToAddress) && !scBaseUtils.isVoid(shipmentModel.Shipment.BillToAddress.FirstName) && !scBaseUtils.isVoid(shipmentModel.Shipment.BillToAddress.LastName)) {
			
				customerName = shipmentModel.Shipment.BillToAddress.FirstName+" "+shipmentModel.Shipment.BillToAddress.LastName;
				
			}
			
			/*if(!scBaseUtils.isVoid(shipmentModel.Shipment.GiftFlag) && scBaseUtils.equals(shipmentModel.Shipment.GiftFlag, "N")) {

				customerName = shipmentModel.Shipment.BillToAddress.FirstName+" "+shipmentModel.Shipment.BillToAddress.LastName;

			} else if(!scBaseUtils.isVoid(shipmentModel.Shipment.ShipmentLines.ShipmentLine.length) && scBaseUtils.equals(shipmentModel.Shipment.GiftFlag, "Y")){

				for(var i=0;i<shipmentModel.Shipment.ShipmentLines.ShipmentLine.length;i++) {
					customerName = shipmentModel.Shipment.ShipmentLines.ShipmentLine[i].OrderLine.PersonInfoMarkFor.FirstName+" "+shipmentModel.Shipment.ShipmentLines.ShipmentLine[i].OrderLine.PersonInfoMarkFor.LastName;
				}
			
			}*/
			
			return customerName;
		
		
		};
		
		customerPickUpUtils.getDayPhone = function(screen,shipmentModel) {
		
			//console.log("screen : ",screen);
			//console.log("shipmentModel : ",shipmentModel);
			
			var dayPhone = "", isPersonInfoMarkForPresent = false;
			
			
			if(!scBaseUtils.isVoid(shipmentModel.Shipment.ShipmentLines.ShipmentLine.length)) {
			
				for(var i=0;i<shipmentModel.Shipment.ShipmentLines.ShipmentLine.length;i++) {
				
			
					if(!scBaseUtils.isVoid(shipmentModel.Shipment.ShipmentLines.ShipmentLine[i].OrderLine.PersonInfoMarkFor)) {
					
						isPersonInfoMarkForPresent = true;
						dayPhone = shipmentModel.Shipment.ShipmentLines.ShipmentLine[i].OrderLine.PersonInfoMarkFor.DayPhone;
						break;
					
					}
				
				}
			
			}
			
			if(!isPersonInfoMarkForPresent && !scBaseUtils.isVoid(shipmentModel.Shipment.BillToAddress) && !scBaseUtils.isVoid(shipmentModel.Shipment.BillToAddress.DayPhone) ) {
			
				dayPhone = shipmentModel.Shipment.BillToAddress.DayPhone;
				
			}
			
			/*if(!scBaseUtils.isVoid(shipmentModel.Shipment.GiftFlag) && scBaseUtils.equals(shipmentModel.Shipment.GiftFlag, "N")) {
			
				dayPhone = shipmentModel.Shipment.BillToAddress.DayPhone;
				
			} else if(!scBaseUtils.isVoid(shipmentModel.Shipment.ShipmentLines.ShipmentLine.length) && scBaseUtils.equals(shipmentModel.Shipment.GiftFlag, "Y")){
			
				for(var i=0;i<shipmentModel.Shipment.ShipmentLines.ShipmentLine.length;i++) {
					dayPhone = shipmentModel.Shipment.ShipmentLines.ShipmentLine[i].OrderLine.PersonInfoMarkFor.DayPhone;
				}	
			
			}*/
			
			//console.log("dayPhone : ",dayPhone);
			
			return dayPhone;
		
		
		};
		
		customerPickUpUtils.getEmailID = function(screen,shipmentModel) {
		
			//console.log("screen : ",screen);
			//console.log("shipmentModel : ",shipmentModel);
			
			var emailID = "",isPersonInfoMarkForPresent = false;;
			
			if(!scBaseUtils.isVoid(shipmentModel.Shipment.ShipmentLines.ShipmentLine.length)) {
			
				for(var i=0;i<shipmentModel.Shipment.ShipmentLines.ShipmentLine.length;i++) {
				
			
					if(!scBaseUtils.isVoid(shipmentModel.Shipment.ShipmentLines.ShipmentLine[i].OrderLine.PersonInfoMarkFor)) {
					
						isPersonInfoMarkForPresent = true;
						emailID = shipmentModel.Shipment.ShipmentLines.ShipmentLine[i].OrderLine.PersonInfoMarkFor.EMailID;
						break;
					
					}
				
				}
			
			}
			
			if(!isPersonInfoMarkForPresent && !scBaseUtils.isVoid(shipmentModel.Shipment.BillToAddress) && !scBaseUtils.isVoid(shipmentModel.Shipment.BillToAddress.EMailID) ) {
			
				emailID = shipmentModel.Shipment.BillToAddress.EMailID;
				
			}
			
			/*if(!scBaseUtils.isVoid(shipmentModel.Shipment.GiftFlag) && scBaseUtils.equals(shipmentModel.Shipment.GiftFlag, "N")) {
			
				emailID = shipmentModel.Shipment.BillToAddress.EMailID;
				
			} else if(!scBaseUtils.isVoid(shipmentModel.Shipment.ShipmentLines.ShipmentLine.length) && scBaseUtils.equals(shipmentModel.Shipment.GiftFlag, "Y")){
			
				for(var i=0;i<shipmentModel.Shipment.ShipmentLines.ShipmentLine.length;i++) {
					emailID = shipmentModel.Shipment.ShipmentLines.ShipmentLine[i].OrderLine.PersonInfoMarkFor.EMailID;
				}
			
			}*/
			
			//console.log("emailID : ",emailID);
			
			return emailID;
		
		
		};
		
		customerPickUpUtils.getBillToAddressPath = function(screen,shipmentModel) {
		
			//console.log("screen : ",screen);
			//console.log("shipmentModel : ",shipmentModel);
			
			var billToAddressPath = "Shipment.BillToAddress";
			if(shipmentModel.Shipment.BillToAddress ==undefined || shipmentModel.Shipment.BillToAddress == '') {
			
				billToAddressPath = "Shipment.ShipmentLines.ShipmentLine.OrderLine.PersonInfoMarkFor";
				
			} 
			
			//console.log("billToAddressPath : ",billToAddressPath);
			
			return billToAddressPath;
		
		
		};
		customerPickUpUtils.computeSalesOrderNo = function(screen,shipmentModel) {

			//console.log("screen : ",screen);
			//console.log("shipmentModel : ",shipmentModel);

			var salesOrderNo = "";

			if(!scBaseUtils.isVoid(shipmentModel.Shipment.ShipmentLines.ShipmentLine.length)) {
				var prevSalesOrderNo = "";
				for(var i=0;i<shipmentModel.Shipment.ShipmentLines.ShipmentLine.length;i++) {
					var currSalesOrderNo = shipmentModel.Shipment.ShipmentLines.ShipmentLine[i].OrderNo;
					if(prevSalesOrderNo == "") {
						prevSalesOrderNo = currSalesOrderNo;
						salesOrderNo = currSalesOrderNo;
						continue;
					} else if(prevSalesOrderNo === currSalesOrderNo) {
						continue;
					}

					salesOrderNo = salesOrderNo+ "," + currSalesOrderNo;
					prevSalesOrderNo = currSalesOrderNo;

				}

			}

			//console.log("salesOrderNo : ",salesOrderNo);

			return salesOrderNo;


		};

		customerPickUpUtils.computePaymentMethod = function(screen,shipmentModel) {
		
			//console.log("screen : ",screen);
			//console.log("shipmentModel : ",shipmentModel);
			
			var paymetMethodDesc = "";
			
			if(!scBaseUtils.isVoid(shipmentModel.Shipment.ShipmentLines.ShipmentLine.length)) {
				for(var i=0;i<shipmentModel.Shipment.ShipmentLines.ShipmentLine.length;i++) {
					var prevPaymentMethodDesc = "";
					
					if(!scBaseUtils.isVoid(shipmentModel.Shipment.ShipmentLines.ShipmentLine[i].Order.PaymentMethods) && !scBaseUtils.isVoid(shipmentModel.Shipment.ShipmentLines.ShipmentLine[i].Order.PaymentMethods.PaymentMethod) &&  !scBaseUtils.isVoid(shipmentModel.Shipment.ShipmentLines.ShipmentLine[i].Order.PaymentMethods.PaymentMethod.length)) {


						for(var j=0;j<shipmentModel.Shipment.ShipmentLines.ShipmentLine[i].Order.PaymentMethods.PaymentMethod.length;j++) {
							var tempPaymentMethodDesc = "";
							var paymentMethodModel = shipmentModel.Shipment.ShipmentLines.ShipmentLine[i].Order.PaymentMethods.PaymentMethod[j];
							
							tempPaymentMethodDesc = scOrderUtils.getPaymentDescription(paymentMethodModel,screen);
							
							if(prevPaymentMethodDesc == "") {
								prevPaymentMethodDesc = tempPaymentMethodDesc;
								paymetMethodDesc = prevPaymentMethodDesc;
								continue;
							} else if(prevPaymentMethodDesc === tempPaymentMethodDesc){
								continue;
							}
							
							paymetMethodDesc = paymetMethodDesc+" , "+tempPaymentMethodDesc;
							prevPaymentMethodDesc = tempPaymentMethodDesc;

						}
					
					}
										
				}
				
			} 
			
			//console.log("paymentMethod : ",paymetMethodDesc);
			
			return paymetMethodDesc;
		
		
		};
		
		customerPickUpUtils.resetBarCodeTextField = function(uiEvent, businessEvent, control, args) {
		
			var emptyModel = null;
			var screen = scEventUtils.getScreenFromEventArguments(args);
			scScreenUtils.setModel(screen, "translateBarCode_output", emptyModel, null);
			scWidgetUtils.setFocusOnWidgetUsingUid(screen, "scanProductIdTxt");
			
		};
		
		customerPickUpUtils.resetSearchPickUpOrderInput = function(uiEvent, businessEvent, control, args) {
		
			var emptyModel = null;
			var screen = scEventUtils.getScreenFromEventArguments(args);
			scScreenUtils.setModel(screen, "searchPickUpOrder_dummyInput", emptyModel, null);
			
		};
		
		/*customerPickUpUtils.appendScreenIdToModelPath = function(screen, modelPath, shipmentLineModel){
			
			scModelUtils.setStringValueAtModelPath(modelPath,screen.id,shipmentLineModel);
			//console.log("Updated shipmentLineModel : ",shipmentLineModel);
			return shipmentLineModel;
		};*/
		
		customerPickUpUtils.updateShipmentLineContainer = function(screen, productScanDetailModel) {
		
			//console.log("updateShipmentLineContainer");
			//console.log("productScanDetailModel : ",productScanDetailModel);
			//console.log("shipmentLineContainerList : ",shipmentLineContainerList);
			
			var type = "wsc.shipment.customerpickup.PickupOrderLineDetails";
			var isOverragePresent = false, isProductFound= false;
			var lastProductScannedModel = {};
			lastProductScannedModel.ProductConsumed = "N";
			lastProductScannedModel.OrderLine = {};
			
			var PickAll_Input = {};
			var pickedQty=Number("0");
			var scannedItemID = "", inventoryUOM = "", kitCode= "", productClass = "",scannedItemPresent = false;
			
			var barCodeDataModel = productScanDetailModel.BarCode;
			
			if(!scBaseUtils.isVoid(productScanDetailModel) && !scBaseUtils.isVoid(productScanDetailModel.BarCode) && !scBaseUtils.isVoid(productScanDetailModel.BarCode.Translations))  {
			
				if(scBaseUtils.equals("0",productScanDetailModel.BarCode.Translations.TotalNumberOfRecords)) {
					textObj = {};
					textObj.OK=scScreenUtils.getString(screen,"Ok");
					scScreenUtils.showErrorMessageBox(screen,scScreenUtils.getString(screen,"ItemNotFound"),"", textObj,"");
					return;
				}  else if(!scBaseUtils.equals("1",productScanDetailModel.BarCode.Translations.TotalNumberOfRecords)) {
					textObj = {};
					textObj.OK=scScreenUtils.getString(screen,"Ok");
					scScreenUtils.showErrorMessageBox(screen,scScreenUtils.getString(screen,"MultipleItemsFound"),"", textObj,"");
					return;
				} 
				
				
				scannedItemPresent = true;
				
			} else {
				textObj = {};
				textObj.OK=scScreenUtils.getString(screen,"Ok");
				scScreenUtils.showErrorMessageBox(screen,scScreenUtils.getString(screen,"ItemNotFound"),"", textObj,"");
					return;
			
			}
			
			if(scannedItemPresent && !scBaseUtils.isVoid(productScanDetailModel.BarCode.Translations.Translation) && !scBaseUtils.isVoid(productScanDetailModel.BarCode.Translations.Translation.length) && !scBaseUtils.isVoid(productScanDetailModel.BarCode.Translations.Translation[0].ItemContextualInfo) && !scBaseUtils.isVoid(productScanDetailModel.BarCode.Translations.Translation[0].ItemContextualInfo.ItemID)) {
				scannedItemID = productScanDetailModel.BarCode.Translations.Translation[0].ItemContextualInfo.ItemID;
				inventoryUOM =  productScanDetailModel.BarCode.Translations.Translation[0].ItemContextualInfo.InventoryUOM;
				kitCode =  productScanDetailModel.BarCode.Translations.Translation[0].ItemContextualInfo.KitCode;
				productClass =  productScanDetailModel.BarCode.Translations.Translation[0].ItemContextualInfo.ProductClass;
			} else {
				textObj = {};
				textObj.OK=scScreenUtils.getString(screen,"Ok");
				scScreenUtils.showErrorMessageBox(screen,scScreenUtils.getString(screen,"ItemNotFound"),"", textObj,"");
					return;
			}
			
			//console.log("scannedItemID : ",scannedItemID);
			
			var container = screen.getWidgetByUId("PickUpItemContainer");
			//console.log("container : ",container);
			var listOfChildren = container.getChildren(container);
			//console.log("listOfChildren : ",listOfChildren);
			for (var i=0;i<listOfChildren.length;i++) {
				var child = listOfChildren[i];
				if (isccsUIUtils.instanceOf(child,type) || (child.screenId && child.screenId == type)) {
					if(child instanceof scScreen ){
						//console.log("FireChildEvent 1 : ",child.id);
						var shipmentLineOutput = scScreenUtils.getTargetModel(child, "ShipmentLine_Output", null);
						//console.log("shipmentLineOutput : ",shipmentLineOutput);				
						
						if(!scBaseUtils.isVoid(shipmentLineOutput.ShipmentLine.ItemID) && !scBaseUtils.stringEquals(scannedItemID,shipmentLineOutput.ShipmentLine.ItemID)) {
							continue;
						}
						
						if(!scBaseUtils.isVoid(shipmentLineOutput.ShipmentLine.UnitOfMeasure) && !scBaseUtils.stringEquals(inventoryUOM,shipmentLineOutput.ShipmentLine.UnitOfMeasure)) {
							continue;
						}
						
						if(!scBaseUtils.isVoid(shipmentLineOutput.ShipmentLine.KitCode) && !scBaseUtils.stringEquals(kitCode,shipmentLineOutput.ShipmentLine.KitCode)) {
							continue;
						}
						
						if(!scBaseUtils.isVoid(shipmentLineOutput.ShipmentLine.ProductClass) && !scBaseUtils.stringEquals(productClass,shipmentLineOutput.ShipmentLine.ProductClass)) {
							continue;
						}
						
								isProductFound = true;
						
						if(isProductFound) {
								
								if(scBaseUtils.isVoid(shipmentLineOutput.ShipmentLine.PickedQty)) {
									pickedQty=Number("0");
								} else {
									pickedQty=Number(shipmentLineOutput.ShipmentLine.PickedQty);
								}
								
								//console.log("pickedQty : ",pickedQty);
								
								if(Number(shipmentLineOutput.ShipmentLine.Quantity) < (Number(pickedQty) + Number("1"))) {
									continue;
								} else {
									
									lastProductScannedModel.ShipmentLineKey = shipmentLineOutput.ShipmentLine.ShipmentLineKey;
									lastProductScannedModel.ProductConsumed = "Y";
									lastProductScannedModel.screenUId = child.uId;
									//PickAll_Input.Quantity = Number(pickedQty) + Number("1");										
									//child.setModel("PickAll_Input", PickAll_Input, "");
									lastProductScannedModel.OrderLine = shipmentLineOutput.ShipmentLine.OrderLine;
									scEventUtils.fireEventInsideScreen(child, "updateScannedProductQuantity", null, {});
									
								}
								
								/*if(scBaseUtils.numberEquals(Number(shipmentLineOutput.ShipmentLine.Quantity),Number(PickAll_Input.Quantity))) {
									scWidgetUtils.showWidget(child,"productScanImagePanel",false,"");
								}*/
								
								break;
								
							}
						
					}
					
						
				}
				
			}
			
			//console.log("isProductFound : ",isProductFound);
			//console.log("lastProductScannedModel : ",lastProductScannedModel);
			
			if(isProductFound && scBaseUtils.stringEquals("N",lastProductScannedModel.ProductConsumed)) {
				textObj = {};
				textObj.OK=scScreenUtils.getString(screen,"Ok");
				scScreenUtils.showErrorMessageBox(child,scScreenUtils.getString(screen,"ItemOverrage"),"", textObj,"");			
			} else if(!isProductFound) {
				textObj = {};
				textObj.OK=scScreenUtils.getString(screen,"Ok");
				scScreenUtils.showErrorMessageBox(screen,scScreenUtils.getString(screen,"ItemNotInShipment"),"", textObj,"");
			} else if(isProductFound && scBaseUtils.stringEquals("Y",lastProductScannedModel.ProductConsumed)) {
			
					/*for(var j=0;j<shipmentLineContainerList.length;j++) {
						var shipmentLine = shipmentLineContainerList[j];
						if(scBaseUtils.stringEquals(lastProductScannedModel.ShipmentLineKey,shipmentLine.ShipmentLine.ShipmentLineKey)) {
							lastProductScannedModel.OrderLine = shipmentLine.ShipmentLine.OrderLine;
							break;
						}
					
					}*/
					/**
					* Highlight the Scanned Product Shipment Line
					*/
					var lastScannedModelInWizard = isccsUIUtils.getWizardModel(screen,"lastScannedProduct");
					if(!scBaseUtils.isVoid(lastScannedModelInWizard)) {
						// Remove CSS for previously highlighted panel 
						scWidgetUtils.removeClass(screen,lastScannedModelInWizard.screenUId,"highlightRepeatingPanel");
					}
					isccsUIUtils.setWizardModel(screen,"lastScannedProduct",lastProductScannedModel,null);
						// Add CSS to Highlight Last Product Scanned panel 
					scWidgetUtils.addClass(screen,lastProductScannedModel.screenUId,"highlightRepeatingPanel");
					//scScreenUtils.scrollToWidget(screen,lastProductScannedModel.screenUId);
					
					//console.log("Updating Last Product Scanned Model");
					scWidgetUtils.showWidget(screen,"lastScannedProductDetailPanel",false,null);
					var options = scBaseUtils.getNewBeanInstance();
					scBaseUtils.addBeanValueToBean("lastProductScannedModel",lastProductScannedModel,options);
					var childScreen = scScreenUtils.getChildScreen(screen, "lastProductScannedDetailsScreenRef");
					//console.log("childScreen : ",childScreen);
					if(scBaseUtils.isVoid(childScreen)) {
						scScreenUtils.showChildScreen(screen,"lastProductScannedDetailsScreenRef","","",options,lastProductScannedModel);
						childScreen = scScreenUtils.getChildScreen(screen, "lastProductScannedDetailsScreenRef");
					} 
					childScreen.setModel("lastProductScanned_output",lastProductScannedModel,"");
					scEventUtils.fireEventToChild(screen, "lastProductScannedDetailsScreenRef", "updateLastProductScanned", null);
			}
			
		};		
		
		customerPickUpUtils.populatePreviouslyPickedupShipmentPanel = function(screen,screenContainerUId,namespace,prevPickUpShipmentModel) {
		
			//console.log("prevPickUpShipmentModel : ",prevPickUpShipmentModel);
			//console.log("namespace : ",namespace);
			//console.log("screenContainerUId : ",screenContainerUId);
			
				
			var options = scBaseUtils.getNewBeanInstance();
			if(!scBaseUtils.isVoid(prevPickUpShipmentModel) && !scBaseUtils.isVoid(prevPickUpShipmentModel.Shipment) ) {
				
				var shipmentModel = prevPickUpShipmentModel.Shipment;
					
				if(!scBaseUtils.isVoid(shipmentModel) && !scBaseUtils.isVoid(shipmentModel.ShipmentLines) && !scBaseUtils.isVoid(shipmentModel.ShipmentLines.ShipmentLine) && !scBaseUtils.isVoid(shipmentModel.ShipmentLines.ShipmentLine.length)) {
					
					for(var j=0;j<shipmentModel.ShipmentLines.ShipmentLine.length;j++) {
						var shipmentLineModel = shipmentModel.ShipmentLines.ShipmentLine[j];
						
						if("Y" == shipmentLineModel.OrderLine.IsBundleParent || scBaseUtils.equals(Number("0"), Number(shipmentLineModel.Quantity))) {
							shipmentModel.ShipmentLines.ShipmentLine.splice(j,1);
							j--;
							continue;
						}	
						
					}
					
				}
			
				prevPickUpShipmentModel.Shipment = shipmentModel;
				prevPickUpShipmentModel.ShipmentLines = shipmentModel.ShipmentLines;
				
				//console.log("prevPickUpShipmentModel (new) : ",prevPickUpShipmentModel);
				//scScreenUtils.reloadScreen(screen);
				var childScreen = scScreenUtils.getChildScreen(screen, "previouslyPickedUpLinesScreenRef");
				//console.log("childScreen : ",childScreen);
				if(scBaseUtils.isVoid(childScreen)) {
					scScreenUtils.showChildScreen(screen,"previouslyPickedUpLinesScreenRef","","",options,prevPickUpShipmentModel);
					childScreen = scScreenUtils.getChildScreen(screen, "previouslyPickedUpLinesScreenRef");
					//console.log("childScreen : ",childScreen);
				} 
				childScreen.setModel(namespace,prevPickUpShipmentModel,"");
			}
		
		
		};
		
		customerPickUpUtils.setChildModel = function(screen,screenContainerUId,namespace,parentModel,parentPath,childRootPath) {
				
			var childModel = {};		
			childModel[childRootPath]= scModelUtils.getModelObjectFromPath(parentPath,parentModel);  
			var childScreen = scScreenUtils.getChildScreen(screen, screenContainerUId);
			//console.log("childScreen : ",childScreen);
			if(scBaseUtils.isVoid(childScreen)) {
				scScreenUtils.showChildScreen(screen,screenContainerUId,"","",{},{});
				childScreen = scScreenUtils.getChildScreen(screen, screenContainerUId);
				//console.log("childScreen : ",childScreen);
				//childScreen.setModel(namespace,childModel,"");
				//scEventUtils.fireEventToChild(screen, screenContainerUId,"afterScreenInit" , null);	
			}	
		};
		
		customerPickUpUtils.showChildScreen = function(screen,screenContainerUId) {
			var childScreen = scScreenUtils.getChildScreen(screen, screenContainerUId);
			console.log("childScreen : ",childScreen);
			if(scBaseUtils.isVoid(childScreen)) {
				scScreenUtils.showChildScreen(screen,screenContainerUId,"","",{},{});				
			}	
		};
		
		customerPickUpUtils.massageCustVerificationNotesModel = function(screen,sCustVerificationNotesModel,selectedShipmentsModel) {

			var customerName = "";
			var shipmentModel = {};
			var custVerificationNotesModel = {};
			custVerificationNotesModel.Notes = {};
			custVerificationNotesModel.Notes.Note = {};
			custVerificationNotesModel.Notes.Note.ReasonCode = "YCD_CUSTOMER_VERIFICATION";
			custVerificationNotesModel.Notes.Note.ContactUser = scUserprefs.getUserId();


			if(!scBaseUtils.isVoid(selectedShipmentsModel) && !scBaseUtils.isVoid(selectedShipmentsModel.Shipment)) {
				customerName = customerPickUpUtils.concatCustomerName(screen,selectedShipmentsModel);
			}
			
			if(scBaseUtils.isVoid(customerName)) {
				customerName = "Customer";
			}
			
			var verificationMethodListModel = scScreenUtils.getModel(screen, "common_getCustomerVerficationMethodList_output");
			
			if(!scBaseUtils.isVoid(verificationMethodListModel) && !scBaseUtils.isVoid(verificationMethodListModel.CommonCodeList) && !scBaseUtils.isVoid(verificationMethodListModel.CommonCodeList.CommonCode) && !scBaseUtils.isVoid(verificationMethodListModel.CommonCodeList.CommonCode.length)) {
				
				for(var i=0;i<verificationMethodListModel.CommonCodeList.CommonCode.length;i++) {
				
					if(scBaseUtils.equals(scModelUtils.getStringValueFromPath("Notes.Note.NoteText", sCustVerificationNotesModel), verificationMethodListModel.CommonCodeList.CommonCode[i].CodeValue)) {
						scModelUtils.setStringValueAtModelPath("Notes.Note.NoteText", verificationMethodListModel.CommonCodeList.CommonCode[i].CodeShortDescription,sCustVerificationNotesModel);
						break;
					}
				
				}
			
			}
			
			var dataArray = scBaseUtils.getNewArrayInstance();
			scBaseUtils.appendToArray(dataArray, customerName);
			scBaseUtils.appendToArray(dataArray, scModelUtils.getStringValueFromPath("Notes.Note.NoteText", sCustVerificationNotesModel));

			var formattedValue =  scScreenUtils.getFormattedString(screen,"CustVerificationDefaultNoteText",dataArray);

			custVerificationNotesModel.Notes.Note.NoteText = formattedValue;

			//console.log("custVerificationNotesModel: ",custVerificationNotesModel);

			return custVerificationNotesModel;

		};

		customerPickUpUtils.getRepeatingScreenData = function(repeatingScreenId,targetNamespaceMapBean,additionalParamsBean) {
			var returnValue = null;
			var constructorData = null;

			returnValue = scBaseUtils.getNewBeanInstance();
			scBaseUtils.addStringValueToBean("repeatingscreenID", repeatingScreenId, returnValue);

			constructorData = scBaseUtils.getNewBeanInstance();

			if(!scBaseUtils.isVoid(additionalParamsBean)){
				constructorData = additionalParamsBean;
			}

			if(!scBaseUtils.isVoid(targetNamespaceMapBean)){
				var namespaceMapArray = null;
				var mainTargetMappingBean = null;
				var targetMappingBean = null;

				namespaceMapArray = scBaseUtils.getNewArrayInstance();
				scBaseUtils.appendBeanToArray(namespaceMapArray, targetNamespaceMapBean);

				targetMappingBean = scBaseUtils.getNewBeanInstance();
				scBaseUtils.addArrayValueToBean("targetMapping", namespaceMapArray, targetMappingBean);

				mainTargetMappingBean = scBaseUtils.getNewBeanInstance();
				scBaseUtils.addBeanValueToBean("fwdbindingData", targetMappingBean, mainTargetMappingBean);
				scBaseUtils.addBeanValueToBean("bindingData", mainTargetMappingBean, constructorData);
			}

			scBaseUtils.addBeanValueToBean("constructorArguments", constructorData, returnValue);

			//console.log("returnValue : ",returnValue);
			return returnValue;
		};

		customerPickUpUtils.getCancellationReasonList = function(){
			return isccsContextUtils.getFromContext('CancellationReason');
		};

		customerPickUpUtils.getShortageReasonList = function(){		
			/*var resolutionType={
				"ShortageReason":[
					{
						"ShortageReasonCode":"AllInventoryShortage",
						"ShortageReasonDesc":""+scBundleUtils.getString('AllInventoryShortage')
					},
					{
						"ShortageReasonCode":"PickLater",
						"ShortageReasonDesc":""+scBundleUtils.getString('PickLater')
					},
					{
						"ShortageReasonCode":"Cancel",
						"ShortageReasonDesc":""+scBundleUtils.getString('Cancel')
					}
				]
			};
			return resolutionType;*/
			return isccsContextUtils.getFromContext('CPShortageReason');
			
		};
		
		customerPickUpUtils.getOrderLineQuantityFieldBinding = function(unformattedValue,widget,screen,namespace,shipmentLine) {
			
			//console.log("getOrderLineQuantityFieldBinding");
			
			var displayQtyUOM = "";
			if(!scBaseUtils.isVoid(shipmentLine)) {
				if(!scBaseUtils.isVoid(unformattedValue)){
					var sUnitOfMeasure = scUOMUtils.getOrderLineUOM(shipmentLine.ShipmentLine.OrderLine);
					var format = scUOMUtils.getOrderLineQuantityDisplayFormat(shipmentLine.ShipmentLine.OrderLine);
					displayQtyUOM = scUOMUtils.getFormatedQuantityWithUom(unformattedValue, sUnitOfMeasure, format);
				}else{
					displayQtyUOM =  scUOMUtils.getOrderLineQuantityDisplay(shipmentLine.ShipmentLine.OrderLine);
				}
			}
			
			//console.log("displayQtyUOM : ",displayQtyUOM);
			
			return displayQtyUOM;
		
		
		};
		
		customerPickUpUtils.getDisplayUOM = function(unformattedValue,widget,screen,namespace,shipmentLine) {
		
			var displayUOM = unformattedValue;
			if(scBaseUtils.isVoid(displayUOM)) {
				displayUOM =  shipmentLine.ShipmentLine.OrderLine.ItemDetails.UnitOfMeasure;
			} 
			//console.log("displayUOM : ",displayUOM);
			return displayUOM;
		
		};
		
		customerPickUpUtils.getFormattedPickUpDate = function(unformattedValue,screen,widgetId,namespace,shipment) {
		
			var actualPromiseDate = "", foundPromiseDate = false;
			//console.log("shipment : ",shipment);
			if(!scBaseUtils.isVoid(shipment) && !scBaseUtils.isVoid(shipment.Shipment.ShipmentLines.ShipmentLine.length)) {
				
				for(var j=0;j<shipment.Shipment.ShipmentLines.ShipmentLine.length;j++) {
					var shipmentLine = shipment.Shipment.ShipmentLines.ShipmentLine[j];
					
					if(!foundPromiseDate && !scBaseUtils.isVoid(shipmentLine) && !scBaseUtils.isVoid(shipmentLine.OrderLine.OrderDates.OrderDate.length)) {
					
						for(var i=0;i<shipmentLine.OrderLine.OrderDates.OrderDate.length;i++) {
							var orderDate = shipmentLine.OrderLine.OrderDates.OrderDate[i];
							if(scBaseUtils.stringEquals("YCD_FTC_FIRST_PROMISE_DATE",orderDate.DateTypeId)) {
								actualPromiseDate = orderDate.ActualDate;
								foundPromiseDate = true;
								break;
							}
						
						}

						
					} else {
						break;
					}
				
				}
				
				
			}
			
			actualPromiseDate = scBaseUtils.convertToUserFormat(new Date(actualPromiseDate),"DATE");
			
			//console.log("return actualPromiseDate : ",actualPromiseDate);
			
			return actualPromiseDate;
		};
		
		customerPickUpUtils.hasRemainingShipmentLines = function(shipmentModel){
			var status = true;
			if(shipmentModel.Shipment.RemainingShipmentLines.ShipmentLines.ShipmentLine.length==0){
				status = false;
			}
			return status;	
		};
		
		customerPickUpUtils.openSiteMap = function(){
			scControllerUtils.openScreenInEditor("wsc.editors.HomeEditorRT", {}, null, {}, {}, "wsc.editors.BlankEditor");

		};
		
		customerPickUpUtils.massagePickUpOrderSearchResult = function(screen,pickUpOrderSearchResultModel) {
			//console.log("massagePickUpOrderSearchResult");
			//console.log("pickUpOrderSearchResultModel : ",pickUpOrderSearchResultModel);
			
			var model = scBaseUtils.cloneModel(pickUpOrderSearchResultModel);
			
			if(!scBaseUtils.isVoid(model)) {				
				
					var shipmentModel = model.Shipment;
					shipmentModel.ReadyForPickupLines = {};
					shipmentModel.ReadyForPickupLines.ShipmentLines={};
					shipmentModel.ReadyForPickupLines.ShipmentLines.ShipmentLine = [];
					
					if(!scBaseUtils.isVoid(shipmentModel) && !scBaseUtils.isVoid(shipmentModel.ShipmentLines) && !scBaseUtils.isVoid(shipmentModel.ShipmentLines.ShipmentLine) && !scBaseUtils.isVoid(shipmentModel.ShipmentLines.ShipmentLine.length)) {
						var k= 0;
						for(var j=0;j<shipmentModel.ShipmentLines.ShipmentLine.length;j++) {
							var shipmentLineModel = shipmentModel.ShipmentLines.ShipmentLine[j];
							
							if(("Y" == shipmentLineModel.OrderLine.IsBundleParent) || scBaseUtils.equals(Number("0"),Number(shipmentLineModel.Quantity))) {
								continue;
							} else {
								shipmentModel.ReadyForPickupLines.ShipmentLines.ShipmentLine[k] = shipmentLineModel;
								k++;

							}							
						}
						
					}			
				
			
			}
			
			//console.log("model :",model);
			return model;
		};
		
		customerPickUpUtils.hideSystemMessage = function(screen) {
			var mashupContext = scControllerUtils.getMashupContext(screen);
			if(isccsUIUtils.shouldHideSystemMessage(mashupContext)) {
				isccsBaseTemplateUtils.hideMessage(screen);
			}
		};
		
		customerPickUpUtils.hasCancelShipmentLines = function(shipmentModel){
			var status = true;
			if(shipmentModel.Shipment.CancelledShipmentLines.ShipmentLines.ShipmentLine.length==0){
				status = false;
			}
			return status;	
		};				

		customerPickUpUtils.focusOnHomeEditor = function(){			
			var homeEditors = scEditorUtils.getSpecificOpenEditors("wsc.editors.HomeEditor");
			var editor = scBaseUtils.getArrayBeanItemByIndex(homeEditors,0);
			var editorInstance = scBaseUtils.getAttributeValue("editorInstance", false, editor);
			scEditorUtils.focusOnEditor(editorInstance, null);
		};
		
		customerPickUpUtils.addModelToModelPath = function(path, childModel, parentModel){	

			if(scBaseUtils.isVoid(parentModel)) {
				parentModel = {};
			}

			if(scBaseUtils.isVoid(parentModel[path])) {
				parentModel[path] = {};
			} 

			parentModel[path] = childModel;
			
			return parentModel;
		};
		
		
		customerPickUpUtils.appendModelToInitialInputData = function(screen, customerWizardModelContainer) {
		
			var initialInputData = scScreenUtils.getInitialInputData(screen);
			
			if(!scBaseUtils.isVoid(initialInputData)) {

				if(scBaseUtils.isVoid(initialInputData.InputToWizard)) {
					customerWizardModelContainer.InputToWizard = {};
					customerWizardModelContainer.InputToWizard = initialInputData;
				} 				
				
			}
			
			return customerWizardModelContainer;
		};
		
		
		return customerPickUpUtils;
	});