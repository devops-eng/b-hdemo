#!/bin/sh
export SMCFS_HOME=/usr/OMS/dev/9.1/Foundation
export INSTALL_DIR=$SMCFS_HOME
export JAVA_HOME=$INSTALL_DIR/jdk
export RCP_EXTN_FOLDER=$INSTALL_DIR/rcpextn
export ANT_HOME=$INSTALL_DIR/Migration/apache-ant-1.7.1
export ANT_OPTS="-Xms1024m -Xmx4096m -XX:MaxPermSize=256m"
export WS_HOME="/opt/WebSphere/dev/WAS_OMS"
export PATH=$JAVA_HOME/bin:$ANT_HOME/bin:$PATH

$ANT_HOME/bin/ant $*
