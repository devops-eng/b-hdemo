/**
 *  *#################################################################################################################################################################
 *          OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *########################################################################################################################################################################
      
             1.1             16/05/2017         Kumar Shashank              Changes made for (GCSTORE-6877) : Cancellation of free gift item when the Parent is cancelled			   		              
 *########################################################################################################################################################################
 */

package com.gc.agent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.ycp.japi.util.YCPBaseAgent;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCCancelBackOrderedLineAgent extends YCPBaseAgent{

	private static YFCLogCategory logger = YFCLogCategory.instance(GCCancelBackOrderedLineAgent.class);
	/**
	 * This method prepare the getOrderLineList input to get only backOrdered line and call the same API with required template.
	 * Filter out the orderlines which are having lineTotal <$200 then for those line getATP api is being called to check inventory
	 * if OnHand supply is zero then put these all order line into a map in format of changeOrder API input.
	 * @throws SAXException 
	 * @throws IOException 
	 * @throws ParserConfigurationException 
	 */
	public List<Document> getJobs(YFSEnvironment env, Document inDoc , Document docLastMsg) throws ParserConfigurationException, IOException, SAXException{
		logger.debug("Class: GCCancelOrderLineAgent:: " + "Method: getJobs:: Begin");
		logger.debug("Class: GCCancelOrderLineAgent:: " + "Method: getJobs Input document" +GCXMLUtil.getXMLString(inDoc));
		Element eleRootInDoc = inDoc.getDocumentElement();
		String strUnitPriceFrmCrtria = eleRootInDoc.getAttribute(GCConstants.UNIT_PRICE);
		if (YFCCommon.isVoid(strUnitPriceFrmCrtria)){
			strUnitPriceFrmCrtria = "200.00";
		}
		double dblUnitPriceFrmCrtria = Double.parseDouble(strUnitPriceFrmCrtria);
		String  strOrgCode= eleRootInDoc.getAttribute(GCConstants.INVENTORY_ORGANIZATION);
		if(YFCCommon.isVoid(strOrgCode)){
			strOrgCode = GCConstants.GCI;
		}
		String  strInvCheckReq= eleRootInDoc.getAttribute("InvCheckRequired");
		if (YFCCommon.isVoid(strInvCheckReq)){
			strInvCheckReq = "N";
		}
		String strMaxRecords = eleRootInDoc.getAttribute("MaximumRecords");
		if (YFCCommon.isVoid(strMaxRecords)){
			strMaxRecords = "1000";
		}
		String strItemType = eleRootInDoc.getAttribute("ItemType");
		if(YFCCommon.isVoid(strItemType)){
			strItemType = "C2-C3";
		}
		//This method prepare the input of getOrderLineList API and call the same with required template
		Document docGetOrdLinelistOP = callGetOrderLineList(env, docLastMsg,
				strMaxRecords,strUnitPriceFrmCrtria);
		Element eleRootGetOrdLne = docGetOrdLinelistOP.getDocumentElement();
		String strLastOrdLineKey = eleRootGetOrdLne.getAttribute("LastOrderLineKey");
		//This method return the Map with order lines in form of changeOrder input doc for eligible lines for cancellation.
		Map<String, Document> hmOrdLineForCancellation = selectLineforCancellation(env, docGetOrdLinelistOP, dblUnitPriceFrmCrtria, 
				strOrgCode,strInvCheckReq,strItemType);
		List<Document> listOrdLineBatch = new ArrayList<Document>();
		if(!hmOrdLineForCancellation.isEmpty()){
		for (Map.Entry<String, Document> entry : hmOrdLineForCancellation.entrySet()) {
		    Document docChangeOrderFrmMap = entry.getValue();
		    Element rootEle = docChangeOrderFrmMap.getDocumentElement();
		    rootEle.setAttribute("LastOrderLineKey", strLastOrdLineKey);
		    listOrdLineBatch.add(docChangeOrderFrmMap);
		    logger.debug("Document from hmOrdLineForCancellation"+GCXMLUtil.getXMLString(docChangeOrderFrmMap));
		 }
		logger.debug("Class: GCCancelOrderLineAgent:: - ArrayList" +listOrdLineBatch);
		return listOrdLineBatch;
		} else if(!YFCCommon.isVoid(strLastOrdLineKey)) {
			//If there is no order line eligible for cancellation then 
			//last order line key is being sent to execute job so that the same order line key 
			//can come back to getJobs and greater than to this key order lines would be fetched by getOrderLineList API.
			Document orderDoc = GCXMLUtil.createDocument(GCConstants.ORDER);
			Element eleOrderDocRoot = orderDoc.getDocumentElement();
			eleOrderDocRoot.setAttribute("LastOrderLineKey",strLastOrdLineKey);
			 listOrdLineBatch.add(orderDoc);
			 logger.debug("Dummy Document in case orderLines are not eligible for cancellation"+GCXMLUtil.getXMLString(orderDoc));
			 return listOrdLineBatch;
		}
		logger.debug("Class: GCCancelOrderLineAgent:: getJob End");
		return null;
	}
	
	/**
	 * This method calls changeOrder API to cancel the lines.
	 */
	public void executeJob(YFSEnvironment env, Document inDoc) throws Exception {
		logger.debug("Class: GCCancelOrderLineAgent:: executeJob Begin");
		logger.debug("Method - executeJob Input document"+GCXMLUtil.getXMLString(inDoc));
		Element eleChangeOrderInDoc = inDoc.getDocumentElement();
		String strOHKey = eleChangeOrderInDoc.getAttribute(GCConstants.ORDER_HEADER_KEY);
		//Added for GCSTORE-6877 : Start
		updateChangeOrderForGiftLineCancel(eleChangeOrderInDoc);
		//End
		if(!YFCCommon.isVoid(strOHKey)){
	     GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, inDoc);
		}
	logger.debug("Class: GCCancelOrderLineAgent:: executeJob End");
	}
	
	/**
	 * This method has been added for (GCSTORE-6877) the cancellation of free gift item associated with the parent
	 * when the parent is being cancelled
	 * @param eleOrder
	 */
	private void updateChangeOrderForGiftLineCancel(Element eleOrder) {

		logger.debug("Class: GCCancelOrderLineAgent:: updateChangeOrderForGiftLineCancel Begin");
		//Adding flag to identify system cancellation
		eleOrder.setAttribute(GCConstants.SYSTEM_CANCELLATION, GCConstants.YES);
		NodeList nlOrderLine = eleOrder.getElementsByTagName(GCConstants.ORDER_LINE);
		int orderLineCount = nlOrderLine.getLength();

		for(int i=0; i<orderLineCount; i++){
			Element eleOrderLine = (Element) nlOrderLine.item(i);
			eleOrderLine.setAttribute(GCConstants.IS_COMPLETE_LINE_CANCEL, GCConstants.YES);
		}
		logger.debug("Class: GCCancelOrderLineAgent:: updateChangeOrderForGiftLineCancel End");
	}

	/**
	 * This method prepares the input document of getOrderLineList API with status ="1300" (BackOrdered Lines) and call the same with required template.
	 * @param env
	 * @param docLastMsg
	 * @param strMaxRecords
	 * @return
	 */
	private Document callGetOrderLineList(YFSEnvironment env,
			Document docLastMsg, String strMaxRecords,String strLnTotlFrmCrtria) {
		logger.debug("Class: GCCancelOrderLineAgent:: " + "Method: callGetOrderLineList:: Begin");
		Document docGetOrdLinelistIP = GCXMLUtil.createDocument(GCConstants.ORDER_LINE);
		Element eleOrdLine = docGetOrdLinelistIP.getDocumentElement();
		eleOrdLine.setAttribute(GCConstants.STATUS, "1300");	
		eleOrdLine.setAttribute("MaximumRecords", strMaxRecords);
		Element eleLinePriceInfo = docGetOrdLinelistIP.createElement(GCConstants.LINE_PRICE_INFO);
		eleLinePriceInfo.setAttribute("UnitPrice", strLnTotlFrmCrtria);
		eleLinePriceInfo.setAttribute("UnitPriceQryType", "LE");
		Element eleOrder = docGetOrdLinelistIP.createElement(GCConstants.ORDER);
		eleOrder.setAttribute(GCConstants.DOCUMENT_TYPE, "0001");
		eleOrdLine.appendChild(eleLinePriceInfo);
		eleOrdLine.appendChild(eleOrder);
		if (null != docLastMsg) {
            Element lastMsgRtEle = docLastMsg.getDocumentElement();
            String lastOrdLineKey = lastMsgRtEle.getAttribute("LastOrderLineKey");
            eleOrdLine.setAttribute("OrderLineKeyQryType", "GT");
            eleOrdLine.setAttribute(GCConstants.ORDER_LINE_KEY, lastOrdLineKey);
        }
		logger.debug("Input document for getOrderLineList "+GCXMLUtil.getXMLString(docGetOrdLinelistIP));
		
		Document tmpDocGetOrdLineList = GCXMLUtil.getDocument("<OrderLineList LastOrderLineKey='' TotalLineList=''>" +
				"<OrderLine Status='' OrderHeaderKey='' OrderLineKey='' OrderedQty=''><LinePriceInfo UnitPrice='' /><Item ItemID='' UnitOfMeasure='' ProductClass='' ProductLine='' />" +
				"<ItemDetails ItemID=''><Extn ExtnIsClearanceItem='' ExtnIsUsedOrVintageItem='' /></ItemDetails></OrderLine></OrderLineList>");
		Document docGetOrdLinelistOP = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LINE_LIST, docGetOrdLinelistIP, tmpDocGetOrdLineList);
		logger.debug("Output document for getOrderLineList "+GCXMLUtil.getXMLString(docGetOrdLinelistOP));
		logger.debug("Class: GCCancelOrderLineAgent:: " + "Method: callGetOrderLineList:: End");
		return docGetOrdLinelistOP;
	}

/**
 * Eligible lines for cancellation are being stored in MAP in form of changeOrder API input.
 * @param env
 * @param indoc
 * @param strLnTotlFrmCrtria
 * @param strOrgCode
 * @return
 */
	private Map<String, Document> selectLineforCancellation(YFSEnvironment env , Document indoc,double strLnTotlFrmCrtria,
			String strOrgCode , String strInvCheckReq , String strItemType){
		logger.debug("Class: GCCancelOrderLineAgent:: " + "Method: selectLineforCancellation:: Begin");
		Element eleOrderLnLst = indoc.getDocumentElement();
		NodeList nlOrderLine = eleOrderLnLst.getElementsByTagName(GCConstants.ORDER_LINE);
		int nlLen = nlOrderLine.getLength();
		Map<String , Document> hmOrdLineForCancel = new HashMap<String, Document>();
		Map<String , String> hmItems = new HashMap<String, String>();
		StringBuffer sbItemID = new StringBuffer();
		for(int ordLine=0;ordLine<nlLen;ordLine++){
			Element eleOrderLine = (Element) nlOrderLine.item(ordLine);
			Element eleItem = (Element) eleOrderLine.getElementsByTagName(GCConstants.ITEM).item(0);
			Element eleItemDetails = (Element) eleOrderLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
			Element eleExtnItemDetails = (Element) eleItemDetails.getElementsByTagName(GCConstants.EXTN).item(0);
			String strProductLine = eleItem.getAttribute("ProductLine");
			String strExtnClearanceItem = eleExtnItemDetails.getAttribute("ExtnIsClearanceItem");
			String strExtnIsUsedOrVintageItem = eleExtnItemDetails.getAttribute("ExtnIsUsedOrVintageItem");
			if(strItemType.contains(strProductLine) || GCConstants.YES.equalsIgnoreCase(strExtnClearanceItem)
					|| GCConstants.YES.equalsIgnoreCase(strExtnIsUsedOrVintageItem)){
			 if(!GCConstants.YES.equalsIgnoreCase(strInvCheckReq)){
				 prepareMapToCancelOrdLine(hmOrdLineForCancel, eleOrderLine);
			 } else {
				 String strItemIDAfterConcat = addItemIDAndUOMInSB(sbItemID,
						eleItem);
				 if(!hmItems.containsKey(strItemIDAfterConcat)){
					 hmItems.put(strItemIDAfterConcat, null);
				 }
			}
			}
		}
		logger.debug("hmItems Map Method: selectLineforCancellation"+hmItems);
		if(!hmItems.isEmpty()){
		// This for loop iterate through the Map and then call getATP API.
		 for (Map.Entry<String, String> itemID : hmItems.entrySet()) {
			 String itemIDConcat = itemID.getKey();
			 String[] stingList = itemIDConcat.split("-");
			 String strItemID = stingList[0];
			 String strProductClass = stingList[1];
			 String strUOM = stingList[2];
			 
	     boolean blNoInvAvail = noInventoryAvailable(env ,strItemID,
					strProductClass, strUOM , strOrgCode);
	     if(!blNoInvAvail){
	    	 hmItems.remove(itemIDConcat); 
	     }
		 }
		 //This for loop prepare the changeOrder input and store in map if
		 //itemID ,productClass and UOM combination is being contained in hmItems
		 
		 for(int ordLine=0;ordLine<nlLen;ordLine++){
				Element eleOrderLine = (Element) nlOrderLine.item(ordLine);
				Element eleItem = (Element) eleOrderLine.getElementsByTagName(GCConstants.ITEM).item(0);
				String strProductLine = eleItem.getAttribute("ProductLine");
				Element eleItemDetails = (Element) eleOrderLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
				Element eleExtnItemDetails = (Element) eleItemDetails.getElementsByTagName(GCConstants.EXTN).item(0);
				//String strProductLine = eleItem.getAttribute("ProductLine");
				String strExtnClearanceItem = eleExtnItemDetails.getAttribute("ExtnIsClearanceItem");
				String strExtnIsUsedOrVintageItem = eleExtnItemDetails.getAttribute("ExtnIsUsedOrVintageItem");
				if(strItemType.contains(strProductLine) || GCConstants.YES.equalsIgnoreCase(strExtnClearanceItem)
						|| GCConstants.YES.equalsIgnoreCase(strExtnIsUsedOrVintageItem)){
					String strItemIDAfterConcat = addItemIDAndUOMInSB(sbItemID,
							eleItem);
					if(hmItems.containsKey(strItemIDAfterConcat)){
						prepareMapToCancelOrdLine(hmOrdLineForCancel, eleOrderLine);
					}
				}
				}
		}
		logger.debug("Class: GCCancelOrderLineAgent:: " + "Method: selectLineforCancellation:: End");
		return hmOrdLineForCancel;
		
	}
/**
 * This method preparer the string buffer and stores the ItemId,ProductClass and Unit of measure.
 * @param sbItemID
 * @param eleItem
 * @return
 */
private String addItemIDAndUOMInSB(StringBuffer sbItemID, Element eleItem) {
	logger.debug("Class: GCCancelOrderLineAgent:: " + "Method: addItemIDAndUOMInSB:: Begin");
	String strProductClass = eleItem.getAttribute(GCConstants.PRODUCT_CLASS).trim();
	 String strUOM = eleItem.getAttribute(GCConstants.UNIT_OF_MEASURE).trim();
	 String strItemID = eleItem.getAttribute(GCConstants.ITEM_ID).trim();
	 sbItemID.append(strItemID);
	 sbItemID.append("-");
	 sbItemID.append(strProductClass);
	 sbItemID.append("-");
	 sbItemID.append(strUOM);
	 String strItemIDAfterConcat = sbItemID.toString();
	 sbItemID.setLength(0);
	 logger.debug("Class: GCCancelOrderLineAgent:: String value from string buffer"+strItemIDAfterConcat);
	 logger.debug("Class: GCCancelOrderLineAgent:: " + "Method: addItemIDAndUOMInSB:: End");
	return strItemIDAfterConcat;
}
	/**
	 * This method prepares a map in form of change order API input for order lines which are eligible for cancellation.
	 * @param hmOrdLineForCancel
	 * @param eleOrderLine
	 */
private void prepareMapToCancelOrdLine(
	Map<String, Document> hmOrdLineForCancel, Element eleOrderLine) {
	logger.debug("Class: GCCancelOrderLineAgent:: " + "Method: prepareMapToCancelOrdLine:: Begin");
	String strOHKey = eleOrderLine.getAttribute(GCConstants.ORDER_HEADER_KEY);
	  if(!hmOrdLineForCancel.containsKey(strOHKey)){
		  Document docChngeOrd = GCXMLUtil.createDocument(GCConstants.ORDER);
		  Element eleOrder = docChngeOrd.getDocumentElement();
		  eleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, strOHKey);
		  eleOrder.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
		  Element eleChngdOrderLines = docChngeOrd.createElement(GCConstants.ORDER_LINES);
		  Element eleChngdOrdLine = docChngeOrd.createElement(GCConstants.ORDER_LINE);
		  eleChngdOrdLine.setAttribute(GCConstants.ACTION, GCConstants.CANCEL);
		  eleChngdOrdLine.setAttribute(GCConstants.ORDER_LINE_KEY,eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
		  eleChngdOrdLine.setAttribute(GCConstants.ORDERED_QTY,eleOrderLine.getAttribute(GCConstants.ORDERED_QTY));
		  eleOrder.appendChild(eleChngdOrderLines);
		  eleChngdOrderLines.appendChild(eleChngdOrdLine);
		  hmOrdLineForCancel.put(strOHKey, docChngeOrd);
	 } else{
		 Document docChngOrder = hmOrdLineForCancel.get(strOHKey);
		 Element eleOrdLines = (Element) docChngOrder.getElementsByTagName(GCConstants.ORDER_LINES).item(0);
		 Element eleOrdLine = docChngOrder.createElement(GCConstants.ORDER_LINE);
		 eleOrdLine.setAttribute(GCConstants.ACTION, GCConstants.CANCEL);
		 eleOrdLine.setAttribute(GCConstants.ORDER_LINE_KEY,eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
		 eleOrdLine.setAttribute(GCConstants.ORDERED_QTY,eleOrderLine.getAttribute(GCConstants.ORDERED_QTY));
		 eleOrdLines.appendChild(eleOrdLine);
	 }
	  logger.debug("Class: GCCancelOrderLineAgent:: " + "Method: prepareMapToCancelOrdLine:: End");
} 
	/**
	 * This Methods calls getATP API for checking the OnHand supply of an item. If there is no supply then method returns true else return false. 
	 * @param env
	 * @param eleItem
	 * @param strOrgCode
	 * @return  boolean
	 */
	private boolean noInventoryAvailable(YFSEnvironment env ,String strItemID,
			String strProductClass,String strUOM ,String strOrgCode){
		logger.debug("Class: GCCancelOrderLineAgent:: " + "Method: noInventoryAvailable:: Begin");
		boolean noInvntryAvail = true;
		Document docGetATPInput = GCXMLUtil.createDocument(GCConstants.GET_ATP);
		Element eleGetATP = docGetATPInput.getDocumentElement();
		eleGetATP.setAttribute(GCConstants.ITEM_ID, strItemID);
		eleGetATP.setAttribute(GCConstants.ORGANIZATION_CODE, strOrgCode);
		eleGetATP.setAttribute(GCConstants.PRODUCT_CLASS, strProductClass);
		eleGetATP.setAttribute(GCConstants.UNIT_OF_MEASURE, strUOM);
		logger.debug("noInventoryAvailable - getATP input document"+GCXMLUtil.getXMLString(docGetATPInput));
		Document docGetATPOP = GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ATP, docGetATPInput, null);
		logger.debug("noInventoryAvailable - getATP output document"+GCXMLUtil.getXMLString(docGetATPOP));
		Element eleAvlbleToPromseInv = (Element) docGetATPOP.getElementsByTagName("AvailableToPromiseInventory").item(0);
		String strAvailable = eleAvlbleToPromseInv.getAttribute("Available");
		logger.debug("Available Qty "+strAvailable);
		if(0.0 < Double.parseDouble(strAvailable)) {
			noInvntryAvail = false;
		}
		logger.debug("Class: GCCancelOrderLineAgent:: " + "Method: noInventoryAvailable:: End");
		return noInvntryAvail;
	}
}
