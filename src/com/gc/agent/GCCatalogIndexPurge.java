package com.gc.agent;

import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCCatalogIndexPurge implements YIFCustomApi{

  private static YFCLogCategory logger = YFCLogCategory.instance(GCCatalogIndexPurge.class);
  Properties oProperties = new Properties();

  public Document deleteCatalogIndex(YFSEnvironment env, Document inDoc) throws GCException {

    logger.debug("GCCatalogIndexPurge :: deleteIndex - Begin");
    logger.debug("Incoming XML \n " + GCXMLUtil.getXMLString(inDoc));

    Element eleInDoc = inDoc.getDocumentElement();
    String orgCode = eleInDoc.getAttribute(GCConstants.ORGANIZATION_CODE);
    String categoryDomain = eleInDoc.getAttribute(GCConstants.CATEGORY_DOMAIN);

    //Using System arguments in SDF to get the purge index days
    String strPurgeDays = oProperties.getProperty(GCConstants.PURGE_CAT_INDEX_DAYS);
    if (YFCCommon.isVoid(strPurgeDays)) {
      strPurgeDays = "5";
    }

    logger.debug("PurgeDays = " + strPurgeDays);

    // Calculate the time stamp before which all the indexes should be
    // deleted
    final String strDeleteBeforeTimeStamp = GCDateUtils.getPreviousDate(GCConstants.DATE_TIME_FORMAT,
        Integer.parseInt(strPurgeDays));

    logger.debug("DeleteBeforeTimeStamp = " + strPurgeDays);

    Document manageSearchIndexInput = GCXMLUtil.createDocument(GCConstants.SEARCH_INDEX_TRIGGER);

    Element eleSearchIndexTrigger = manageSearchIndexInput.getDocumentElement();
    eleSearchIndexTrigger.setAttribute(GCConstants.ORGANIZATION_CODE, orgCode);
    eleSearchIndexTrigger.setAttribute(GCConstants.CATEGORY_DOMAIN, categoryDomain);
    eleSearchIndexTrigger.setAttribute(GCConstants.DELETE_ANY_TRIGGERS_BEFORE_TIME_STAMP, strDeleteBeforeTimeStamp);

    logger.debug("manageSearchIndexInput Input XML : \n" + GCXMLUtil.getXMLString(manageSearchIndexInput));

    final Document manageSearchIndexOutput = GCCommonUtil.invokeAPI(env, GCConstants.MANAGE_SEARCH_INDEX_TRIGGER, manageSearchIndexInput);

    logger.debug("manageSearchIndexInput Output XML : \n" + GCXMLUtil.getXMLString(manageSearchIndexOutput));

    return manageSearchIndexOutput;

  }

  @Override
  public void setProperties(Properties arg0) {

    this.oProperties = arg0;

  }

}
