/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Fetch & delete the records from GC_COMP_ITEMS_FOR_INV_SYNC
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               03/03/2015         Alex/Mansi		       This class will fetch & delete the records from GC_COMP_ITEMS_FOR_INV_SYNC according
             															   to predefined Purge Criteria
 *#################################################################################################################################################################
 */
package com.gc.agent;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.ycp.japi.util.YCPBaseAgent;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 *
 * @author <a href="mailto:mansi.jain@expicient.com">Mansi</a>
 *
 */
public class GCCompItemsForInvSyncPurge extends YCPBaseAgent{

  private static YFCLogCategory logger = YFCLogCategory.instance(GCCatalogIndexPurge.class);

  /**
   * This method would return a list of records present in GC_COMP_ITEMS_FOR_INV_SYNC
   *  which are eligible for purging
   */
  @Override
  public List getJobs(YFSEnvironment env, Document inDoc) throws GCException {
    logger.debug("Class: GCCompItemsForInvSyncPurge:: " + "Method: getJobs:: ");

    List<Document> compItemsForInvSyncList = new ArrayList<Document>();
    //Fetch Purge Criteria from Common Codes
    Document getCommonCodeListInDoc=GCXMLUtil.getDocument("<CommonCode CodeValue='CompItemsForInvSync'/>");
    Document getCommonCodeListTemplateDoc=GCXMLUtil.getDocument("<CommonCodeList><CommonCode CodeShortDescription='' /></CommonCodeList>");
    Document getCommonCodeListOutDoc=GCCommonUtil.invokeAPI(env, "getCommonCodeList", getCommonCodeListInDoc, getCommonCodeListTemplateDoc);
    Element eleCommonCode=GCXMLUtil.getElementByXPath(getCommonCodeListOutDoc, "CommonCodeList/CommonCode");
    String strPurgeCriteria=eleCommonCode.getAttribute("CodeShortDescription");

    logger.debug("Purge Criteria: "+strPurgeCriteria);
    if (YFCCommon.isVoid(strPurgeCriteria)){
      return new ArrayList<Document>();
    }
    String strModifyts = GCDateUtils.getPreviousDate(GCConstants.DATE_TIME_FORMAT,Integer.parseInt(strPurgeCriteria));
    logger.debug("Modifyts: "+strModifyts);

    //Form input xml for GetCompItemListService
    Document getCompItemsForInvSyncListInDoc=GCXMLUtil.getDocument("<GCCompItemsForInvSync ModifytsQryType='LE' Modifyts='"+strModifyts+"'/>");

    logger.debug("Input to GetCompItemListService"+GCXMLUtil.getXMLString(getCompItemsForInvSyncListInDoc));

    //Invoke GetCompItemListService for fetching records from GC_COMP_ITEMS_FOR_INV_SYNC which are eligible for purge
    Document getCompItemsForInvSyncListOutDoc = GCCommonUtil.invokeService(env, "GetCompItemListService",getCompItemsForInvSyncListInDoc);

    logger.debug("The output of getCompItemsForInvSyncList is:"+GCXMLUtil.getXMLString(getCompItemsForInvSyncListOutDoc));

    //Fetch GCCompItemsForInvSync records & put it into array list which would be returned by this method
    NodeList compItemsForInvSyncNodeList=getCompItemsForInvSyncListOutDoc.getElementsByTagName("GCCompItemsForInvSync");
    int noOfCompItemsForInvSync = compItemsForInvSyncNodeList.getLength();

    logger.debug("length of node list"+noOfCompItemsForInvSync);

    if (noOfCompItemsForInvSync > 0) {
      for (int i = 0; i < noOfCompItemsForInvSync; i++) {
        Element compItemsForInvSyncEle = (Element) compItemsForInvSyncNodeList.item(i);
        compItemsForInvSyncList.add(GCXMLUtil.getDocument(compItemsForInvSyncEle, true));
      }
    } else {
      return new ArrayList<Document>();
    }

    return compItemsForInvSyncList;
  }

  /**
   * This method would delete the records from GC_COMP_ITEMS_FOR_INV_SYNC based on the list
   * fetched from getJobs()
   */
  @Override
  public void executeJob(YFSEnvironment env, Document inDoc) throws GCException {


    logger.debug("The input of GCDeleteCompItemsForInvSyncService is:"+GCXMLUtil.getXMLString(inDoc));

    //Invoke GCDeleteCompItemsForInvSyncService for deleting records from GC_COMP_ITEMS_FOR_INV_SYNC
    GCCommonUtil.invokeService(env, "GCDeleteCompItemsForInvSyncService", inDoc);

  }

}
