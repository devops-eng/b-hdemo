/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 * *###########################################
 * ######################################################
 * ################################################################ OBJECTIVE: This class is used to
 * apply promotions and shipping charges.
 * ###########################################################
 * #######################################
 * ############################################################### Version Date Modified By
 * Description
 * ######################################################################################
 * ########################################################################### 1.0 05/05/2014
 * Gowda,Naveen JIRA No: Description of issue.
 * ######################################################
 * ############################################
 * ###############################################################
 */
package com.gc.api;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;

import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCWebServiceUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSUserExitException;

/**
 * @author <a href="mailto:email Address">Naveen</a>
 */
public class GCATGPromoService implements YIFCustomApi {
  private String sSiteId;
  private String sSource;
  private Document docATGResponseOp = null;
  private Document docChangeOrder = null;
  private String sExtnFulFillmentType = "";
  private String sOrderHeaderKey = "";
  private String sPaymentMethodValue = "";
  private String sPaymentKey = "";
  private static final String CLASS_NAME = GCATGPromoService.class.getName();
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance( CLASS_NAME);

  String sSourceValue;

  /*
   * The properties
   */
  private Properties props;

  /**
   *
   * Description of getPromoDetails
   *
   * @param env
   * @param inDoc
   * @return
   * @throws Exception
   * @throws TransformerException
   * @throws YFSUserExitException
   *
   */

  public Document getPromoDetails(YFSEnvironment env, Document inDoc)
      throws GCException {
    try {
      LOGGER.debug("Class: GCATGPromoService Method: getPromoDetails BEGIN");
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("getPromoDetails method() - InDoc to ATG Service"
            + GCXMLUtil.getXMLString(inDoc));
      }
      Node nPriceRequest = XPathAPI.selectSingleNode(inDoc,
          "Envelope/Body/PricingRequest");
      sOrderHeaderKey = GCXMLUtil.getElementByXPath(inDoc,
          "Envelope/Body/PricingRequest/OrderHeaderKey")
          .getTextContent();
      String sLevelOfService = GCXMLUtil.getElementByXPath(inDoc,
          "Envelope/Body/PricingRequest/LevelOfService")
          .getTextContent();
      // Set TaxExempt, isInternational, isAPOFPO,
      setTaxExemptandOtherElementsAttributes(inDoc);

      String sShippingMethod = "";

      if(!YFCCommon.isVoid(sLevelOfService))
      {
        sShippingMethod = fetchShippingMethodUsingLOS(env, sLevelOfService);
      }
      Element eleShippingMethod = null;

      Node nFulfillmentType = XPathAPI.selectSingleNode(inDoc,
          "Envelope/Body/PricingRequest/FulfillmentType");
      String sFulfillmentType = nFulfillmentType.getTextContent();
      if (sFulfillmentType.contains("PICKUP_IN_STORE")) {
        eleShippingMethod = GCXMLUtil
            .getElementByXPath(
                inDoc,
                "Envelope/Body/PricingRequest/ShoppingCarts/ShoppingCart/ShippingInfo/ShippingMethodInfo/ShipToStore/ShippingMethod");
      } else {
        eleShippingMethod = GCXMLUtil
            .getElementByXPath(
                inDoc,
                "Envelope/Body/PricingRequest/ShoppingCarts/ShoppingCart/ShippingInfo/ShippingMethodInfo/ShipByCarrier/ShippingMethod");
      }
      if (!YFCCommon.isVoid(sShippingMethod)
          && !YFCCommon.isVoid(eleShippingMethod)) {
        eleShippingMethod.setTextContent(sShippingMethod);
      }
      Element eleSiteId = GCXMLUtil.getElementByXPath(inDoc,
          "Envelope/Body/PricingRequest/SiteId");
      Element eleSkipShippingChargeForPLCC = GCXMLUtil.getElementByXPath(
          inDoc,
          "Envelope/Body/PricingRequest/SkipShippingChargeForPLCC");
      String sSkipShippingChargeForPLCC = eleSkipShippingChargeForPLCC
          .getTextContent();
      nPriceRequest.removeChild(eleSkipShippingChargeForPLCC);
      eleSiteId.setTextContent(sSiteId);
      Element eleSource = GCXMLUtil.getElementByXPath(inDoc,
          "Envelope/Body/PricingRequest/Source");

      sSourceValue = eleSource.getTextContent();
      if(YFCUtils.isVoid(sSourceValue))
      {
        eleSource.setTextContent(sSource);
      }

      // Fix for 1026: Start
      if (YFCCommon.equals(sSourceValue, "MPOS")) {
        env.setTxnObject("EntryType", "mPOS");
      }
      // Fix for 1026: End

      // Remove unwanted data from XML.
      Node nOrderHdrKey = XPathAPI.selectSingleNode(inDoc,
          "Envelope/Body/PricingRequest/OrderHeaderKey");
      nPriceRequest.removeChild(nOrderHdrKey);

      nPriceRequest.removeChild(nFulfillmentType);

      Node nLevelOfService = XPathAPI.selectSingleNode(inDoc,
          "Envelope/Body/PricingRequest/LevelOfService");
      nPriceRequest.removeChild(nLevelOfService);

      Node nIsAPOFPOShipAdd = XPathAPI.selectSingleNode(inDoc,
          "Envelope/Body/PricingRequest/isAPOFPOShipAdd");
      Node nIsAPOFPOBillAdd = XPathAPI.selectSingleNode(inDoc,
          "Envelope/Body/PricingRequest/isAPOFPOBillAdd");
      nPriceRequest.removeChild(nIsAPOFPOShipAdd);
      nPriceRequest.removeChild(nIsAPOFPOBillAdd);
      Element elePaymentType = GCXMLUtil
          .getElementByXPath(inDoc,
              "Envelope/Body/PricingRequest/ShoppingCarts/ShoppingCart/PaymentType");
      NodeList nlPaymentMethod = elePaymentType
          .getElementsByTagName("v0:PaymentMethod");
      Element elePaymentMethodValue = null;
      Element eleTxnPaymentMethodValue = null;
      int iPaymentMethodListLength=nlPaymentMethod.getLength();
      for (int z = 0; z < iPaymentMethodListLength; z++) {
        Element elePaymentMethod = (Element) nlPaymentMethod.item(z);
        /* Preparing request structure for PLCC Scenario - Begin */
        elePaymentMethodValue = (Element) elePaymentMethod
            .getElementsByTagName("v0:PaymentMethodValue").item(0);
        Element elePaymentKey = (Element) elePaymentMethod
            .getElementsByTagName("v0:PaymentKey").item(0);
        String tmpPaymnetMethodValue = "";
        String tmpPaymentKey = "";
        if (!YFCCommon.isVoid(elePaymentMethodValue)) {
          tmpPaymnetMethodValue = elePaymentMethodValue
              .getTextContent();
        }

        if (!YFCCommon.isVoid(elePaymentKey)) {
          tmpPaymentKey = elePaymentKey.getTextContent();
        }
        // fetch removeble nodes from requests Start
        Node nPaymentMethodValue = elePaymentMethod
            .getElementsByTagName("v0:PaymentMethodValue").item(0);
        Node nPaymentKey = elePaymentMethod.getElementsByTagName(
            "v0:PaymentKey").item(0);
        Node nCreditCardType = elePaymentMethod.getElementsByTagName(
            "v0:CreditCardType").item(0);
        elePaymentMethod.removeChild(nPaymentKey);
        elePaymentMethod.removeChild(nPaymentMethodValue);
        elePaymentMethod.removeChild(nCreditCardType);

        if ((GCConstants.PRIVATE_LABEL_CARD)
            .equalsIgnoreCase(tmpPaymnetMethodValue) || GCConstants.PVT_LBL_CREDIT_CARD.equalsIgnoreCase(tmpPaymnetMethodValue)) {
          sPaymentKey = tmpPaymentKey;
          sPaymentMethodValue = tmpPaymnetMethodValue;
          eleTxnPaymentMethodValue = elePaymentMethodValue;
          Element elesPaymentMethodValueType = inDoc
              .createElement("v0:GearCard");
          elePaymentMethod.appendChild(elesPaymentMethodValueType);
        } else {
          elePaymentMethod.getParentNode().removeChild(elePaymentMethod);
          iPaymentMethodListLength--;
          z--;
        }
      }

      if(iPaymentMethodListLength==0){
        Node nPaymentMethod=inDoc.createElement("v0:PaymentMethod");
        elePaymentType.appendChild(nPaymentMethod);
      }
      /* Preparing request structure for PLCC Scenario - End */

      // Phase-2 changes begins
      String sIsATGPENCallRequired = GCXMLUtil.getElementByXPath(inDoc,
          "Envelope/Body/PricingRequest/IsATGPENCallRequired")
          .getTextContent();
      String[] parts = sIsATGPENCallRequired.split("\\.");
      int isATGCallRequiredInt = Integer.parseInt(parts[0]);

      boolean useShipChargeStoredInOMS = GCConstants.FALSE;

      useShipChargeStoredInOMS = isATGCallRequiredInt % 5 == 0;


      Node nodeIsATGPENCallRequired = XPathAPI.selectSingleNode(inDoc,
          "Envelope/Body/PricingRequest/IsATGPENCallRequired");
      nPriceRequest.removeChild(nodeIsATGPENCallRequired);
      // Phase-2 changes ends

      String sATGPromoURL = YFSSystem.getProperty(GCConstants.ATG_URL);
      if (YFCCommon.isVoid(sATGPromoURL)) {
        throw new YFCException(
            "ATG Promo URL is not configured in property file. Please retry after configuring it.");
      }
      LOGGER.debug("sGCURL :: " + sATGPromoURL);
      String sSoapAction = YFSSystem
          .getProperty(GCConstants.ATG_SOAP_ACTION);
      if (YFCCommon.isVoid(sSoapAction)) {
        throw new YFCException(
            "ATG Promo soap action is not configured in property file. Please retry after configuring it.");
      }
      Document docOut = GCXMLUtil.createDocument(GCConstants.ORDER);
      Element eleReturnDocRoot = docOut.getDocumentElement();
      eleReturnDocRoot.setAttribute(GCConstants.ORDER_HEADER_KEY,
          sOrderHeaderKey);
      eleReturnDocRoot.setAttribute("IsATGInteractSuccess",
          GCConstants.FLAG_Y);

      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Input Doc before webservice call"
            + GCXMLUtil.getXMLString(inDoc));
      }
      /* Calling getOrderList */
      Document docGetOrdIn = GCXMLUtil.createDocument(GCConstants.ORDER);
      Element eleGetOrderInRoot = docGetOrdIn.getDocumentElement();
      eleGetOrderInRoot.setAttribute(GCConstants.ORDER_HEADER_KEY,
          sOrderHeaderKey);
      Document docGetOrdOut = null;
      if (!YFCCommon.isVoid(sOrderHeaderKey)) {
        docGetOrdOut = GCCommonUtil
            .invokeAPI(env, docGetOrdIn,
                GCConstants.API_GET_ORDER_LIST,
                "/global/template/api/getOrderListTempForShippingProrationForGiftItem.xml");
        env.setTxnObject("getOrderListOP", docGetOrdOut);
      }
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("getOrderList OutDoc"
            + GCXMLUtil.getXMLString(docGetOrdOut));
      }

      // Phase-2 changes start
      YFCDocument orderListDoc = YFCDocument.getDocumentFor(docGetOrdOut);
      YFCElement orderListEle = orderListDoc.getDocumentElement();
      YFCElement orderEle = orderListEle
          .getChildElement(GCConstants.ORDER);

      Element eleOrder = GCXMLUtil.getElementByXPath(docGetOrdOut,
          "/OrderList/Order");
      Document docEleOrder = GCXMLUtil.getDocumentFromElement(eleOrder);
      // check if OrderList OutDoc contains cancelled lines
      // same can be exempted from getting shipping charge prorated
      GCGetOrderListForATGRequest
      .removeCancelledOrderLinesFromOrder(docEleOrder);
      String sMaxOrderStatus = orderEle
          .getAttribute(GCConstants.MAX_ORDER_STATUS);
      LOGGER.debug("sMaxOrderStatus after ATG call::" + sMaxOrderStatus);
      String[] status = sMaxOrderStatus.split("\\.");
      String sOrderStatus = status[0];
      LOGGER.debug("sOrderStatus after ATG call::" + sOrderStatus);
      Double dMaxOrderStatus = Double.parseDouble(sOrderStatus);
      boolean isOrderAmendement = dMaxOrderStatus >= 1100;
      // Phase-2 changes end

      GCWebServiceUtil obj = new GCWebServiceUtil();
      boolean isCashAndCarry = false;
      if (!YFCCommon.isVoid(env.getTxnObject("IsStorePickUp")) && YFCUtils.equals(sSourceValue, "MPOS")) {
        isCashAndCarry = true;
      }
      try {



        if (isCashAndCarry)
        {
          Element eleShoppingCart = GCXMLUtil.getElementByXPath(inDoc,
              "Envelope/Body/PricingRequest/ShoppingCarts/ShoppingCart");
          eleShoppingCart.removeChild(elePaymentType);
          Element eleShippingInfo = GCXMLUtil.getElementByXPath(inDoc,
              "Envelope/Body/PricingRequest/ShoppingCarts/ShoppingCart/ShippingInfo");
          eleShoppingCart.removeChild(eleShippingInfo);

          LOGGER.debug("Input Doc before webservice call"
              + GCXMLUtil.getXMLString(inDoc));
        }
        docATGResponseOp = obj.invokeSOAPWebService(inDoc,
            sATGPromoURL, sSoapAction);

      } catch (Exception e) {
        LOGGER.debug("ATG didn't give back the expected response.", e);
        // PHase-2 changes to use OMS stored data only when there is
        // change in Level of service
        /* This flag is set to N when ATG interaction fails */
        eleReturnDocRoot.setAttribute(
            GCConstants.IS_ATG_INTERACTION_SUCCES,
            GCConstants.FLAG_N);
        YFCDocument yfceDocOut = YFCDocument.getDocumentFor(docOut);
        YFCElement eleExtnChangeOrder = yfceDocOut.getDocumentElement()
            .getChildElement("Extn", true);
        eleExtnChangeOrder
        .setAttribute("ExtnIsATGPENCallRequired", "1");

        if ((useShipChargeStoredInOMS || !isOrderAmendement) && !isCashAndCarry) {
          LOGGER.debug("ATG didn't give back the expected response. But there is change in Level of service. So will be using OMS stored data");
          docChangeOrder = GCXMLUtil.getDocumentFromElement(eleOrder);
          // Phase-2 reseting the ATG PEN call required flag to 1. So
          // that it should not be called
          // again and again
          YFCElement yfceRootElement = YFCDocument.getDocumentFor(
              docChangeOrder).getDocumentElement();
          yfceRootElement.getChildElement("Extn", true).setAttribute(
              "ExtnIsATGPENCallRequired", "1");
          // Update Quantity to ship for the order -Phase-2 changes
          updateQuantityToShipOnOrder(docChangeOrder, inDoc);
          if (LOGGER.isVerboseEnabled()) {
            LOGGER.verbose("Change Order doc after updating quantity to ship\n"
                + GCXMLUtil.getXMLString(docChangeOrder));
          }
          // Calculating Shipping Charge Manually
          GCCalculateShippingCharge object = new GCCalculateShippingCharge();
          object.calShipCharge(env, docChangeOrder);

          // Applying oversize charges
          GCApplyOversizeChargesOnOrderLine applyOversizeCharge = new GCApplyOversizeChargesOnOrderLine();
          applyOversizeCharge.applyOversizeCharges(env,
              docChangeOrder);
        } else {
          GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER,
              docOut);
        }
        if (YFCCommon.equals(sSourceValue, "MPOS")) {
          YFCDocument getOrderListTemp = YFCDocument
              .getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_getOrderList.xml"));
          YFCDocument updatedOrderDoc = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST,
              YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "'/>"), getOrderListTemp);
          YFCDocument orderDoc =
              GCXMLUtil.getDocument(updatedOrderDoc.getElementsByTagName(GCConstants.ORDER).item(0), true);
          orderDoc.getDocumentElement().setAttribute(GCConstants.IS_ATG_INTERACTION_SUCCES, GCConstants.FLAG_N);
          return orderDoc.getDocument();
        }
        // Fix for mPOS-1281 : Stamping IsATGInteractionSuccess = N for final outDoc if ATG
        // connection fails.
        if (YFCCommon.equals(sSourceValue, "MPOS")) {
          docOut.getDocumentElement().setAttribute(GCConstants.IS_ATG_INTERACTION_SUCCES, GCConstants.FLAG_N);
        }
        // Fix for mPOS-1281 : End
        return docOut;
      }

      // In case of error response from ATG
      Element eleErrorCode = null;
      try {
        eleErrorCode = GCXMLUtil.getElementByXPath(docATGResponseOp,
            "Envelope/Body/Fault/detail/PEFault/ErrorCode");
      } catch (Exception e) {
        LOGGER.error(
            "Printing the exception when ATG returned error response",
            e);

        callChangeOrderForATGPENValReset(env);
        YFCDocument docError = YFCDocument.createDocument("Error");
        YFCElement eleError = docError.getDocumentElement();
        eleError.setAttribute("ErrorCode", "");
        eleError.setAttribute("ErrorDescription", e.getMessage());
        return docError.getDocument();
      }
      if (!YFCCommon.isVoid(eleErrorCode)) {
        String sErrorCode = eleErrorCode.getTextContent();
        Element eleErrorMsg = GCXMLUtil.getElementByXPath(
            docATGResponseOp,
            "Envelope/Body/Fault/detail/PEFault/ErrorMessage");
        String sErrorMsg = "";
        if (!YFCCommon.isVoid(eleErrorMsg)) {
          sErrorMsg = eleErrorMsg.getTextContent();
        }
        YFCException yfs = new YFCException(sErrorCode);
        yfs.setErrorDescription(sErrorMsg);
        callChangeOrderForATGPENValReset(env);

        YFCDocument docError = YFCDocument.createDocument("Order");
        YFCElement eleOrderError = docError.getDocumentElement();
        YFCElement eleError = eleOrderError.createChild("Error");
        eleError.setAttribute("ErrorCode", "");
        eleError.setAttribute("ErrorDescription", sErrorMsg);
        return docError.getDocument();
      }
      
      //MPOS-856 Error Handling: Start.
      Element elePricingResponse = GCXMLUtil.getElementByXPath(docATGResponseOp,
              "Envelope/Body/PricingResponse");
      
      if(YFCCommon.equalsIgnoreCase(sSourceValue, "MPOS") && YFCCommon.isVoid(eleErrorCode) && YFCCommon.isVoid(elePricingResponse))
      {
	      LOGGER.debug("ATG didn't give back the expected response. Fault with no Error Code and Error message");
	          /* This flag is set to N when ATG interaction fails */
	          eleReturnDocRoot.setAttribute(
	              GCConstants.IS_ATG_INTERACTION_SUCCES,
	              GCConstants.FLAG_N);
	          YFCDocument yfceDocOut = YFCDocument.getDocumentFor(docOut);
	          YFCElement eleExtnChangeOrder = yfceDocOut.getDocumentElement()
	              .getChildElement("Extn", true);
	          eleExtnChangeOrder
	          .setAttribute("ExtnIsATGPENCallRequired", "1");
	          GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER,
	                  docOut);
	          YFCDocument getOrderListTemp = YFCDocument
	                  .getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_getOrderList.xml"));
	              YFCDocument updatedOrderDoc = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST,
	                  YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "'/>"), getOrderListTemp);
	              YFCDocument orderDoc =
	                  GCXMLUtil.getDocument(updatedOrderDoc.getElementsByTagName(GCConstants.ORDER).item(0), true);
	              orderDoc.getDocumentElement().setAttribute(GCConstants.IS_ATG_INTERACTION_SUCCES, GCConstants.FLAG_N);
	              return orderDoc.getDocument();	
      }
      //MPOS-856: Error Handling: Stop.
      
      //PLCC-Start
      final Element elePromos = removePromos( docATGResponseOp);
      //PLCC-End
      Document docChangeOrderIn = null;
      docChangeOrderIn = processATGResponse(env, eleReturnDocRoot,
          sShippingMethod, inDoc, docGetOrdOut,
          sSkipShippingChargeForPLCC);

      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("getPromoDetails() - Input Doc before Change Order"
            + GCXMLUtil.getXMLString(docChangeOrderIn));
      }
      env.setTxnObject("elePaymentMethodValue", eleTxnPaymentMethodValue);
      YFCDocument yfceDocOut = YFCDocument
          .getDocumentFor(docChangeOrderIn);
      YFCElement eleExtnChangeOrder = yfceDocOut.getDocumentElement()
          .getChildElement("Extn", true);
      eleExtnChangeOrder.setAttribute("ExtnIsATGPENCallRequired", "1");
      docChangeOrderIn.getDocumentElement().setAttribute("SelectMethod", "WAIT");
      Document mPOSChangeOrderTemp =
          GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_getOrderDetails.xml");
      Document outputDoc =
          GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, docChangeOrderIn, mPOSChangeOrderTemp);
      Element eleOutput = outputDoc.getDocumentElement();
      eleOutput.setAttribute(GCConstants.PAYMENT_TYPE, eleReturnDocRoot.getAttribute(GCConstants.PAYMENT_TYPE));
      eleOutput.setAttribute(GCConstants.PAYMENT_KEY, eleReturnDocRoot.getAttribute(GCConstants.PAYMENT_KEY));
      eleOutput.setAttribute(GCConstants.PAYMENT_REFERENCE_8,
          eleReturnDocRoot.getAttribute(GCConstants.PAYMENT_REFERENCE_8));
      eleOutput.setAttribute(GCConstants.PAYMENT_REFERENCE_9,
          eleReturnDocRoot.getAttribute(GCConstants.PAYMENT_REFERENCE_9));

      Element elePromotions = outputDoc.createElement("Promotions");
      List<Element> nlOrderLine = GCXMLUtil.getElementListByXpath(outputDoc, "Order/OrderLines/OrderLine");
      Set<String> promotionSet = new HashSet<String>();
      for (int i = 0; i < nlOrderLine.size(); i++) {
        Element eleOrderLine = nlOrderLine.get(i);

        // mPOS-1121: Start : for free gift item
        Element eleExtn = (Element) eleOrderLine.getElementsByTagName(GCConstants.EXTN).item(0);
        String sExtnFreeGiftItemPromoID = eleExtn.getAttribute("ExtnFreeGiftItemPromoID");
        String sExtnFreeGiftItemPromoDesc = eleExtn.getAttribute("ExtnFreeGiftItemPromoDesc");
        String sExtnFreeGiftCouponID = eleExtn.getAttribute("ExtnFreeGiftCouponID");		//mPOS-1194

        // mPOS-1121: End
        NodeList nlLineCharge = eleOrderLine.getElementsByTagName(GCConstants.LINE_CHARGE);
        for (int j = 0; j < nlLineCharge.getLength(); j++) {
          Element eleLineCharge = (Element) nlLineCharge.item(j);
          String chargeCategory = eleLineCharge.getAttribute(GCConstants.CHARGE_CATEGORY);
          if (YFCUtils.equals(chargeCategory, GCConstants.PROMOTION)) {
            String chargeName = eleLineCharge.getAttribute(GCConstants.CHARGE_NAME);
            Double chargeAmount = Double.parseDouble(eleLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT));
            Element eleLineChargeExtn = (Element) eleLineCharge.getElementsByTagName(GCConstants.EXTN).item(0);
            if (!promotionSet.contains(chargeName) && !YFCCommon.isVoid(eleLineChargeExtn) && chargeAmount > 0) {
              Element elePromotion = outputDoc.createElement(GCConstants.PROMOTION);
              elePromotion.setAttribute("ExtnDiscountType", eleLineChargeExtn.getAttribute("ExtnDiscountType"));
              elePromotion.setAttribute("ExtnDiscountPercentage",
                  eleLineChargeExtn.getAttribute("ExtnDiscountPercentage"));
              elePromotion.setAttribute("ExtnPromotionDesc", eleLineChargeExtn.getAttribute("ExtnPromotionDesc"));
              elePromotion.setAttribute("ExtnCouponID", eleLineChargeExtn.getAttribute("ExtnCouponID"));
              elePromotion.setAttribute("ExtnPromotionID", eleLineChargeExtn.getAttribute("ExtnPromotionID"));
              elePromotion.setAttribute("ChargeName", eleLineCharge.getAttribute("ChargeName"));
              if (YFCCommon.isVoid(eleLineChargeExtn.getAttribute("ExtnCouponID"))) {
                elePromotion.setAttribute("AutoPromo", GCConstants.YES);

              }

              elePromotions.appendChild(elePromotion);
              promotionSet.add(chargeName);
            }
          }
        }

        // mPOS-1121 : Start: For gift item promotion display.
        if(!YFCCommon.isVoid(sExtnFreeGiftItemPromoID) && !YFCCommon.isVoid(sExtnFreeGiftItemPromoDesc))
        {
          Element elePromotion = outputDoc.createElement(GCConstants.PROMOTION);
          elePromotion.setAttribute("ExtnDiscountType", "");
          elePromotion.setAttribute("ExtnDiscountPercentage","");
          elePromotion.setAttribute("ExtnPromotionDesc", sExtnFreeGiftItemPromoDesc);
          elePromotion.setAttribute("ExtnCouponID", sExtnFreeGiftCouponID);
          elePromotion.setAttribute("ExtnPromotionID", sExtnFreeGiftItemPromoID);
          elePromotion.setAttribute("ChargeName", sExtnFreeGiftItemPromoID);

          if (YFCCommon.isVoid(sExtnFreeGiftCouponID)) {
            elePromotion.setAttribute("AutoPromo", GCConstants.YES);

          }


          elePromotions.appendChild(elePromotion);
          // promotionSet.add(sExtnFreeGiftItemPromoID);

        }
        // mPOS-1121 : End

      }

      eleOutput.appendChild(elePromotions);
      eleOutput.setAttribute(
          GCConstants.IS_ATG_INTERACTION_SUCCES, GCConstants.FLAG_Y);

      //PLCC-start
      updatePromos(elePromos, outputDoc);
      //PLCC-end
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Return Document from ATG Service"
            + GCXMLUtil.getXMLString(outputDoc));
      }
      LOGGER.debug("Class: GCATGPromoService Method: getPromoDetails END");
      return outputDoc;

    } catch (Exception e) {
      LOGGER.error("The exception is in getPromoDetails method", e);
      throw GCException.getYFCException(e.getMessage(), e);
    }
  }


  /**
   * <h3>Description:</h3>This method is used to update the Promos onto the output
   * @author Infosys Limited
   * @param elePLCCPromos The Promos element to be updated
   * @param docOut The output document
   */
  private void updatePromos(final Element elePLCCPromos, final Document docOut) {

    //log the method entry
    if( LOGGER.isDebugEnabled()){

      LOGGER.debug( CLASS_NAME + ":updatePromos:entry:Input="+GCXMLUtil.getXMLString(docOut));
    }

    //check if Promos element is present
    if( elePLCCPromos != null){

      //fetch the root of the output document
      final Element eleRoot = docOut.getDocumentElement();
      final Element eleProms = docOut.createElement( GCConstants.PLCC_PROMOS);
      eleRoot.appendChild(eleProms);

      //fetch the child element of the Promos
      final NodeList nlPLCCPromoType = elePLCCPromos.getElementsByTagName( "ns1:PLCCPromo");

      //iterate through each of the PromoType
      final int iPLCCPromoTypeLen = nlPLCCPromoType.getLength();
      final Map<String, Element> mapRankPromType = new TreeMap<String, Element>();
      for( int iPLCCPromoTypeIndex =0;iPLCCPromoTypeIndex<iPLCCPromoTypeLen;iPLCCPromoTypeIndex++ ){

        //fetch the PromoType Element
        final Element elePLCCPromoType= (Element)nlPLCCPromoType.item(iPLCCPromoTypeIndex);
        final Element elePromType = getPLCCPromoType( elePLCCPromoType, docOut);
        final Element eleRank = (Element)elePLCCPromoType.getElementsByTagName( "ns1:rank").item(0);
        final String strRank = eleRank.getTextContent();
        mapRankPromType.put(strRank,elePromType);
      }

      //add the sorted elePromoType element
      final Set<String> setRank = mapRankPromType.keySet();
      for( String strRank: setRank){

        //get the PromoType element and append
        final Element elePromoType = mapRankPromType.get(strRank);
        eleProms.appendChild(elePromoType);
      }
    }

    //log the method exit
    if( LOGGER.isDebugEnabled()){

      LOGGER.debug( CLASS_NAME + ":updatePromos:exit:Output="+GCXMLUtil.getXMLString(docOut));
    }
  }


  /**
   * <h3>Description:</h3>This method is used to get the PLCC Promo Type
   * @author Infosys Limited
   * @param elePLCCPromoType The PLCC Promotype
   * @param docOutput the output document
   * @return The updated output document
   */
  private Element getPLCCPromoType(final Element elePLCCPromoType, final Document docOutput) {

    //log the method entry
    LOGGER.debug( CLASS_NAME + ":getPLCCPromoType:entry``");

    //fetch the child elements
    final Element elePlanCode = (Element)elePLCCPromoType.getElementsByTagName( "ns1:planCode").item(0);
    final String strPlanCode = elePlanCode.getTextContent();
    final Element elePromoTitle = (Element)elePLCCPromoType.getElementsByTagName( "ns1:promoTitle").item(0);
    final String strPromoTitle = elePromoTitle.getTextContent();
    final Element elePromoDesc = (Element)elePLCCPromoType.getElementsByTagName( "ns1:promoDescription").item(0);
    final String strPromoDesc = elePromoDesc.getTextContent();
    final Element eleRank = (Element)elePLCCPromoType.getElementsByTagName( "ns1:rank").item(0);
    final String strRank = eleRank.getTextContent();
    final Element eleIsSpecialFinancing = (Element)elePLCCPromoType.getElementsByTagName( "ns1:isSpecialFinancing").item(0);
    final String strIsSpecialFianacing = eleIsSpecialFinancing.getTextContent();

    //create the return element
    final Element elePLCCPromotype = docOutput.createElement(  "PLCCPromo");
    elePLCCPromotype.setAttribute( GCConstants.PLAN_CODE, strPlanCode);
    elePLCCPromotype.setAttribute(GCConstants.PROMO_TITLE, strPromoTitle);
    elePLCCPromotype.setAttribute( GCConstants.PROMO_DESCRIPTION, strPromoDesc);
    elePLCCPromotype.setAttribute( GCConstants.RANK, strRank);
    elePLCCPromotype.setAttribute( GCConstants.IS_SPECIAL_FINANCING, strIsSpecialFianacing);
    final StringBuilder stbPromoCodeTxtVal = new StringBuilder(strPromoTitle);
    stbPromoCodeTxtVal.append( " - ( ");
    stbPromoCodeTxtVal.append( strPlanCode);
    stbPromoCodeTxtVal.append( " ) ");
    elePLCCPromotype.setAttribute( GCConstants.PROMO_CODE_TEXT_VAL, stbPromoCodeTxtVal.toString());

    //log the method exit
    LOGGER.debug( CLASS_NAME + ":getPLCCPromoType:exit");

    //return the updated docment
    return elePLCCPromotype;
  }


  /**
   * <h3>Description:</h3>This method is used to remove the Promos element from the response
   * @author Infosys Limited
   * @param docATGResponseOp2 The ATG reponse
   * @return The Promos element
   */
  private Element removePromos(final Document docATGResponse) {

    //logging the method entry
    if( LOGGER.isDebugEnabled()){

      LOGGER.debug( CLASS_NAME + ":removePromos:entry:Input="+GCXMLUtil.getXMLString(docATGResponse));
    }

    //return element
    final Element elePromos;

    //remove the Promos element
    final NodeList nlPromos = docATGResponse.getElementsByTagName("ns1:PLCCPromos");
    if( nlPromos != null && nlPromos.getLength()>0){

      elePromos=(Element)nlPromos.item(0);
      elePromos.getParentNode().removeChild(elePromos);
    }else{
      elePromos= null;
    }

    //logging the method exit
    if( LOGGER.isDebugEnabled()){

      LOGGER.debug( CLASS_NAME + ":removePromos:exit:Output="+GCXMLUtil.getXMLString(docATGResponse));
    }

    //log the Promos element
    return elePromos;
  }

  /**
   *
   * Description of fetchShippingMethodUsingLOS
   *
   * @param env
   * @param sLevelOfService
   * @return
   * @throws Exception
   *
   */
  private String fetchShippingMethodUsingLOS(YFSEnvironment env,
      String sLevelOfService) throws GCException {
    try {
      LOGGER.debug("Class: GCATGPromoService Method: fetchShippingMethodUsingLOS BEGIN");
      Document docGetCommonCodeIn = GCXMLUtil
          .createDocument(GCConstants.COMMON_CODE);
      Element eleRoot = docGetCommonCodeIn.getDocumentElement();
      eleRoot.setAttribute(GCConstants.CODE_TYPE,
          GCConstants.ATG_SHIPPING_MTHD);
      eleRoot.setAttribute(GCConstants.CODE_VALUE, sLevelOfService);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Input Doc to getCommonCodeList"
            + GCXMLUtil.getXMLString(docGetCommonCodeIn));
      }
      Document docGetCommonCodeOut = GCCommonUtil.invokeAPI(env,
          GCConstants.GET_COMMON_CODE_LIST, docGetCommonCodeIn);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("OutDoc to getCommonCodeList"
            + GCXMLUtil.getXMLString(docGetCommonCodeOut));
      }
      String sShippingMehtod = GCXMLUtil.getAttributeFromXPath(
          docGetCommonCodeOut, "//@CodeShortDescription");
      LOGGER.debug("Class: GCATGPromoService Method: fetchShippingMethodUsingLOS END");
      return sShippingMehtod;
    } catch (Exception e) {
      LOGGER.error(
          "The exception is in fetchShippingMethodUsingLOS method", e);
      throw new GCException(e);
    }
  }

  /**
   *
   * Description of setTaxExemptandOtherElementsAttributes
   *
   * @param inDoc
   * @throws DOMException
   * @throws TransformerException
   *
   */
  private void setTaxExemptandOtherElementsAttributes(Document inDoc)
      throws GCException {
    LOGGER.debug("Class: GCATGPromoService Method: setTaxExemptandOtherElementsAttributes BEGIN");
    try {
      String sIsAPOFPOShipAdd = GCXMLUtil.getElementByXPath(inDoc,
          "Envelope/Body/PricingRequest/isAPOFPOShipAdd")
          .getTextContent();
      String sIsAPOFPOBillAdd = GCXMLUtil.getElementByXPath(inDoc,
          "Envelope/Body/PricingRequest/isAPOFPOBillAdd")
          .getTextContent();
      String sTaxExempt = GCXMLUtil
          .getElementByXPath(inDoc,
              "Envelope/Body/PricingRequest/ShoppingCarts/ShoppingCart/TaxExempt")
          .getTextContent();

      Element eleTaxExempt = GCXMLUtil
          .getElementByXPath(inDoc,
              "Envelope/Body/PricingRequest/ShoppingCarts/ShoppingCart/TaxExempt");
      Element eleBillAddress = GCXMLUtil
          .getElementByXPath(
              inDoc,
              "Envelope/Body/PricingRequest/ShoppingCarts/ShoppingCart/PaymentType/BillingAddress");
      String sBillToCoutry = GCXMLUtil
          .getElementByXPath(
              inDoc,
              "Envelope/Body/PricingRequest/ShoppingCarts/ShoppingCart/PaymentType/BillingAddress/Country")
          .getTextContent();
      String isInternational = "";

      Element eleShipAddress = GCXMLUtil
          .getElementByXPath(
              inDoc,
              "Envelope/Body/PricingRequest/ShoppingCarts/ShoppingCart/ShippingInfo/ShippingMethodInfo/ShipByCarrier/ShippingAddress");

      if (!YFCCommon.isVoid(eleShipAddress)) {
        String sShipToCountry = GCXMLUtil
            .getElementByXPath(
                inDoc,
                "Envelope/Body/PricingRequest/ShoppingCarts/ShoppingCart/ShippingInfo/ShippingMethodInfo/ShipByCarrier/ShippingAddress/Country")
            .getTextContent();

        // Set isInternnational value for Ship To Address
        if ((!GCConstants.US.equalsIgnoreCase(sShipToCountry)) && (!YFCCommon.isVoid(sShipToCountry))) {
          isInternational = "true";
        } else {
          isInternational = "false";
        }
        eleShipAddress.setAttribute("v0:isInternational",
            isInternational);

        // Set ShipTo isAPOFPO
        if (GCConstants.FLAG_Y.equalsIgnoreCase(sIsAPOFPOShipAdd)) {
          sIsAPOFPOShipAdd = "true";
        } else {
          sIsAPOFPOShipAdd = "false";
        }
        eleShipAddress.setAttribute("v0:isAPOFPO", sIsAPOFPOShipAdd);
        // Set isInternational value
      }

      // Set isInternnational value for Bill To Address
      if ((!GCConstants.US.equalsIgnoreCase(sBillToCoutry)) && (!YFCCommon.isVoid(sBillToCoutry))) {
        isInternational = "true";
      } else {
        isInternational = "false";
      }
      eleBillAddress.setAttribute("v0:isInternational", isInternational);

      // Set TaxExempt to true or false

      if (GCConstants.FLAG_Y.equalsIgnoreCase(sTaxExempt)) {
        sTaxExempt = "true";
      } else {
        sTaxExempt = "false";
      }
      eleTaxExempt.setTextContent(sTaxExempt);

      // Set BillTo isAPOFPO
      if (GCConstants.FLAG_Y.equalsIgnoreCase(sIsAPOFPOBillAdd)) {
        sIsAPOFPOBillAdd = "true";
      } else {
        sIsAPOFPOBillAdd = "false";
      }
      eleBillAddress.setAttribute("v0:isAPOFPO", sIsAPOFPOBillAdd);
    } catch (Exception e) {
      LOGGER.error(
          "The exception is in setTaxExemptandOtherElementsAttributes method:",
          e);
      throw new GCException(e);
    }
    LOGGER.debug("Class: GCATGPromoService Method: setTaxExemptandOtherElementsAttributes END");
  }

  /**
   *
   * Description of processATGResponse
   *
   * @param sPaymentKey
   * @param inDoc
   * @param docGetOrdOut
   * @param sShippingMethod
   * @param sPaymentMethodValue
   * @param eleReturnDocRoot
   * @param sLevelOfService
   * @param sCouponCode
   *
   */
  private Document processATGResponse(YFSEnvironment env,
      Element eleReturnDocRoot, String sShippingMethod, Document inDoc,
      Document docGetOrdOut, String sSkipShippingChargeForPLCC)
          throws GCException {

    LOGGER.debug("Class: GCATGPromoService Method: processATGResponse BEGIN");

    try {

      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("processATGResponse() - ATG OutDoc"
            + GCXMLUtil.getXMLString(docATGResponseOp));
      }

      String sCouponCode = "";
      /*
       * Picking the last Coupon Code in the incoming xml which will be
       * stored on Order if it is valid.
       */
      NodeList nlCouponCode = GCXMLUtil
          .getNodeListByXpath(inDoc,
              "Envelope/Body/PricingRequest/ShoppingCarts/ShoppingCart/Coupons/CouponId");
      Element eleCouponCode = (Element) nlCouponCode.item(nlCouponCode
          .getLength() - 1);
      if (!YFCCommon.isVoid(eleCouponCode)) {
        sCouponCode = eleCouponCode.getTextContent();
        LOGGER.debug("Coupon Code Value::" + sCouponCode);
      }

      Element eleOrder = GCXMLUtil.getElementByXPath(docGetOrdOut,
          "/OrderList/Order");

      // Phase-2 changes start
      String sMaxOrderStatus = eleOrder
          .getAttribute(GCConstants.MAX_ORDER_STATUS);
      LOGGER.debug("sMaxOrderStatus after ATG call::" + sMaxOrderStatus);
      String[] parts = sMaxOrderStatus.split("\\.");
      String sOrderStatus = parts[0];
      LOGGER.debug("sOrderStatus after ATG call::" + sOrderStatus);
      Double dMaxOrderStatus = Double.parseDouble(sOrderStatus);
      boolean isOrderAmendement = dMaxOrderStatus >= 1100;
      // Phase-2 changes end

      docChangeOrder = GCXMLUtil.getDocumentFromElement(eleOrder);
      Element eleCOOrder = GCXMLUtil.getElementByXPath(docChangeOrder,
          "//Order");
      Element eleOverallTotalls = GCXMLUtil.getElementByXPath(
          docChangeOrder, "//OverallTotals");
      eleCOOrder.removeChild(eleOverallTotalls);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("processATGResponse() - getOrderList OutDoc"
            + GCXMLUtil.getXMLString(docChangeOrder));
      }
      Element eleRoot = docChangeOrder.getDocumentElement();
      Element eleOrderExtn = GCXMLUtil.getElementByXPath(docChangeOrder,
          "//Order/Extn");


      eleRoot.setAttribute(GCConstants.APPLY_CARRIER_SERVICE_CODE,
          GCConstants.FLAG_Y);

      /* This flag should be passed N for proration to take place. */
      if (YFCCommon.isVoid(env.getTxnObject("IsStorePickUp"))) {
        eleOrderExtn.setAttribute(GCConstants.EXTN_IS_SHIPPING_PRORATED,
            GCConstants.FLAG_N);
      }
      eleOrderExtn.setAttribute("ExtnIsATGCallRequired",
          GCConstants.FLAG_N);

      /* Stamping Coupon Code on Order */

      Object oCouponValue = env.getTxnObject("RemoveCoupon");
      String sCouponString = "";
      Object oDuplicateCoupon = env.getTxnObject("DuplicateCoupon");
      Set<String> couponSet = new HashSet<>();
      if (!YFCCommon.isVoid(env.getTxnObject("CouponsSet"))) {
        couponSet = (Set<String>) env.getTxnObject("CouponsSet");
      }

      LOGGER.verbose("Coupons added:" + couponSet.toString());
      if (!YFCCommon.isVoid(oCouponValue))
      {
        sCouponString = (String) oCouponValue;
        Document inputDoc = GCXMLUtil.getDocument("<GCOrderCoupon GCOrderCouponKey='" + sCouponString +"' OrderHeaderKey='" + sOrderHeaderKey + "'/>");
        LOGGER.debug("Input for GCDeleteOrderCoupon service"
            + GCXMLUtil.getXMLString(inputDoc));
        GCCommonUtil.invokeService(env, "GCDeleteOrderCoupon", inputDoc);

      }

      // Fix for mPOS-996 : Applying one more condition to check if coupon already exist in the
      // order coupon list.
      else if (!YFCCommon.isVoid(sCouponCode) && !isOrderAmendement && YFCCommon.isVoid(oDuplicateCoupon)
          && !couponSet.contains(sCouponCode)) {
        LOGGER.verbose("Coupon will be added to GCOrderCouponList");
        Element eleGCOrderCouponList = docChangeOrder
            .createElement(GCConstants.GC_ORDER_COUPON_LIST);
        Element eleGCOrderCoupon = docChangeOrder
            .createElement(GCConstants.GC_ORDER_COUPON);
        eleGCOrderCoupon.setAttribute(GCConstants.COUPON_CODE,
            sCouponCode);
        eleGCOrderCouponList.appendChild(eleGCOrderCoupon);
        eleOrderExtn.appendChild(eleGCOrderCouponList);
      }
      eleRoot.appendChild(eleOrderExtn);

      NodeList nlLineItems = XPathAPI.selectNodeList(docATGResponseOp,
          "Envelope/Body/PricingResponse/LineItems/LineItem");
      NodeList nlKitLineItem = XPathAPI.selectNodeList(docATGResponseOp,
          "Envelope/Body/PricingResponse/LineItems/KitLineItem");
      String sShippingAmount = "0.00";
      String sShippingPromotionID = "";
      String sShippingPromoDesc = "";

      NodeList nLShippingMethodInfo = XPathAPI.selectNodeList(
          docATGResponseOp,
          "Envelope/Body/PricingResponse/ShippingPriceInfo");
      /*
       * ATG sends back Shipping Charge for all applicable LOS. We are
       * storing it against a hangoff table
       */
      storeShippingChargesForDiffShipMethods(env, nLShippingMethodInfo,
          sOrderHeaderKey);

      Element eleShippingAmount = GCXMLUtil
          .getElementByXPath(docATGResponseOp,
              "Envelope/Body/PricingResponse/OrderPriceInfo/ShippingAmount");
      Element eleShippingPromoID = GCXMLUtil
          .getElementByXPath(
              docATGResponseOp,
              "Envelope/Body/PricingResponse/ShippingPriceInfo/Promotions/ShippingPromotion/PromotionId");
      Element eleShippingPromoDesc = GCXMLUtil
          .getElementByXPath(
              docATGResponseOp,
              "Envelope/Body/PricingResponse/ShippingPriceInfo/Promotions/ShippingPromotion/PromotionDescription");
      /* Shipping Amount for the cart from ATG */
      if (!YFCCommon.isVoid(eleShippingAmount)) {
        sShippingAmount = eleShippingAmount.getTextContent();
        if ("0.00".equals(sShippingAmount)
            || "0.0".equals(sShippingAmount)) {

          sShippingAmount = determineShippingAmountInCaseOfSTS(
              sShippingAmount, sShippingMethod);
        }
      }
      if (!YFCCommon.isVoid(eleShippingPromoID)) {
        sShippingPromotionID = GCXMLUtil
            .getElementByXPath(
                docATGResponseOp,
                "Envelope/Body/PricingResponse/ShippingPriceInfo/Promotions/ShippingPromotion/PromotionId")
            .getTextContent();
      }
      if (!YFCCommon.isVoid(eleShippingPromoDesc)) {
        sShippingPromoDesc = GCXMLUtil
            .getElementByXPath(
                docATGResponseOp,
                "Envelope/Body/PricingResponse/ShippingPriceInfo/Promotions/ShippingPromotion/PromotionDescription")
            .getTextContent();
      }
      // Phase-2 changes: skip payment method changes i.e. POS and OMS
      // code if it is order amendment
      // GCSTORE-2583 Order Amendment Condition Commented
      /* Finance Codes provided ATG in case of Private Label Card */
      Element elePOSCode = GCXMLUtil.getElementByXPath(docATGResponseOp,
          "Envelope/Body/PricingResponse/FinanceTerms/POSCode");
      Element eleOMSCode = GCXMLUtil.getElementByXPath(docATGResponseOp,
          "Envelope/Body/PricingResponse/FinanceTerms/OMSCode");
      String sPOSCode = "";
      String sOMSCode = "";
      if (!YFCCommon.isVoid(elePOSCode)) {
        sPOSCode = elePOSCode.getTextContent();
      }
      if (!YFCCommon.isVoid(eleOMSCode)) {
        sOMSCode = eleOMSCode.getTextContent();
      }
      /* Finance Code are stored against the Payment Type. */
      if (!YFCCommon.isVoid(sPOSCode) && !YFCCommon.isVoid(sOMSCode)) {
        Element elePaymentMethods = docChangeOrder
            .createElement(GCConstants.PAYMENT_METHODS);
        Element elePaymentMethod = docChangeOrder
            .createElement(GCConstants.PAYMENT_METHOD);
        elePaymentMethod.setAttribute(GCConstants.PAYMENT_TYPE, props.getProperty(GCConstants.PRIVATE_LABEL_CARD,
            GCConstants.PRIVATE_LABEL_CARD));
        elePaymentMethod.setAttribute(GCConstants.PAYMENT_KEY,
            sPaymentKey);
        elePaymentMethod.setAttribute(GCConstants.PAYMENT_REFERENCE_8,
            sPOSCode);
        elePaymentMethod.setAttribute(GCConstants.PAYMENT_REFERENCE_9,
            sOMSCode);

        /* Attributes to be returned to UI */
        eleReturnDocRoot.setAttribute(GCConstants.PAYMENT_REFERENCE_8,
            sPOSCode);
        eleReturnDocRoot.setAttribute(GCConstants.PAYMENT_REFERENCE_9,
            sOMSCode);
        eleReturnDocRoot.setAttribute(GCConstants.PAYMENT_TYPE,
            sPaymentMethodValue);
        eleReturnDocRoot.setAttribute(GCConstants.PAYMENT_KEY,
            sPaymentKey);

        elePaymentMethods.appendChild(elePaymentMethod);
        eleRoot.appendChild(elePaymentMethods);
      }
      else if(GCConstants.PVT_LBL_CREDIT_CARD.equalsIgnoreCase(sPaymentMethodValue)){
        eleReturnDocRoot.setAttribute(GCConstants.PAYMENT_TYPE,
            sPaymentMethodValue);
        eleReturnDocRoot.setAttribute(GCConstants.PAYMENT_KEY,
            sPaymentKey);
      }
      /*
       * Stamping Shipping Charges at Header Level. This will be prorated
       * in BeforechnageOrderUE
       */
      if (!YFCCommon.equals(sSkipShippingChargeForPLCC,
          GCConstants.FLAG_Y)) {
        Element eleHeaderCharges = docChangeOrder
            .createElement(GCConstants.HEADER_CHARGES);
        Element eleHeaderCharge = docChangeOrder
            .createElement(GCConstants.HEADER_CHARGE);
        if (YFCCommon.isVoid(eleHeaderCharge)) {
          eleHeaderCharge = docChangeOrder
              .createElement(GCConstants.HEADER_CHARGE);
        }
        Element eleHeaderChargeExtn = docChangeOrder
            .createElement(GCConstants.EXTN);
        eleHeaderChargeExtn.setAttribute(GCConstants.EXTN_PROMOTION_ID,
            sShippingPromotionID);
        eleHeaderChargeExtn.setAttribute(
            GCConstants.EXTN_PROMOTION_DESC, sShippingPromoDesc);
        eleHeaderCharge.setAttribute(GCConstants.CHARGE_AMOUNT,
            sShippingAmount);
        eleHeaderCharge.setAttribute(GCConstants.CHARGE_CATEGORY,
            GCConstants.SHIPPING);
        eleHeaderCharge.setAttribute(GCConstants.CHARGE_NAME,
            GCConstants.SHIPPING_CHARGE);

        eleHeaderCharge.appendChild(eleHeaderChargeExtn);
        eleHeaderCharges.appendChild(eleHeaderCharge);
        eleRoot.appendChild(eleHeaderCharges);
      } else {
        docChangeOrder.getDocumentElement().setAttribute(
            "SkipShippingChargeForPLCC", "Y");
      }
      // Phase-2 changes start
      updateQuantityToShipOnOrder(docChangeOrder, inDoc);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("Change Order doc after updating quantity to ship\n"
            + GCXMLUtil.getXMLString(docChangeOrder));
      }
      if (isOrderAmendement) {
        LOGGER.verbose("Returning changeOrder Doc as it is Order amendment and only Shipping charges need to be updated");
        return docChangeOrder;
      }
      // Phase-2 changes end
      /*
       * Order Level Fulfillment being fetched and stamped for free gift
       * items.
       */
      sExtnFulFillmentType = GCXMLUtil.getAttributeFromXPath(
          docGetOrdOut, "//Order/Extn/@ExtnFulfillmentType");

      /*
       * Remove free gift items before API call. These free gift items
       * will be added again if ATG provides them in response.
       */
      removeFreeGiftItem(docGetOrdOut);

      for (int i = 0; i < nlLineItems.getLength(); i++) {
        Element eleLineItem = (Element) nlLineItems.item(i);
        Document docLineItem = GCXMLUtil
            .getDocumentFromElement(eleLineItem);
        Element eleWarrItem = GCXMLUtil.getElementByXPath(docLineItem,
            "LineItem/WarrantyItem");
        NodeList nLOrderDiscount = XPathAPI.selectNodeList(docLineItem,
            "LineItem/ItemPriceInfo/OrderDiscounts/OrderDiscount");
        NodeList nLItemDiscount = XPathAPI.selectNodeList(docLineItem,
            "LineItem/ItemPriceInfo/Discounts/ItemDiscount");
        NodeList nLWarItemOrdrDiscount = XPathAPI
            .selectNodeList(docLineItem,
                "LineItem/WarrantyItem/ItemPriceInfo/OrderDiscounts/OrderDiscount");
        NodeList nLWarItemDiscount = XPathAPI
            .selectNodeList(docLineItem,
                "LineItem/WarrantyItem/ItemPriceInfo/Discounts/ItemDiscount");

        processLineItems(env, docLineItem, sExtnFulFillmentType,
            nLItemDiscount, nLOrderDiscount,
            sSkipShippingChargeForPLCC);

        if (!YFCCommon.isVoid(eleWarrItem)) {
          Document docWarrItem = GCXMLUtil
              .getDocumentFromElement(eleWarrItem);
          processWarrItem(env, docWarrItem, docChangeOrder,
              sExtnFulFillmentType, nLWarItemDiscount,
              nLWarItemOrdrDiscount, eleRoot);
        }

      }
      /* Process Kit Line Items */
      for (int i = 0; i < nlKitLineItem.getLength(); i++) {
        Element eleKitLineItem = (Element) nlKitLineItem.item(i);
        Document docKitLineItem = GCXMLUtil
            .getDocumentFromElement(eleKitLineItem);
        Element eleWarrItem = GCXMLUtil.getElementByXPath(
            docKitLineItem, "KitLineItem/WarrantyItem");
        NodeList nLOrderDiscount = XPathAPI
            .selectNodeList(docKitLineItem,
                "KitLineItem/HeaderItemPriceInfo/OrderDiscounts/OrderDiscount");
        NodeList nLItemDiscount = XPathAPI
            .selectNodeList(docKitLineItem,
                "KitLineItem/HeaderItemPriceInfo/Discounts/ItemDiscount");
        NodeList nLWarItemOrdrDiscount = XPathAPI
            .selectNodeList(docKitLineItem,
                "KitLineItem/WarrantyItem/ItemPriceInfo/OrderDiscounts/OrderDiscount");
        NodeList nLWarItemDiscount = XPathAPI
            .selectNodeList(docKitLineItem,
                "KitLineItem/WarrantyItem/ItemPriceInfo/Discounts/ItemDiscount");

        processKitLineItems(env, docKitLineItem, docChangeOrder,
            sExtnFulFillmentType, nLItemDiscount, nLOrderDiscount,
            eleRoot, sSkipShippingChargeForPLCC);

        if (!YFCCommon.isVoid(eleWarrItem)) {
          Document docWarrItem = GCXMLUtil
              .getDocumentFromElement(eleWarrItem);
          processWarrItem(env, docWarrItem, docChangeOrder,
              sExtnFulFillmentType, nLWarItemDiscount,
              nLWarItemOrdrDiscount, eleRoot);
        }

      }

      LOGGER.debug("Class: GCATGPromoService Method: processATGResponse END");
      return docChangeOrder;
    } catch (Exception e) {
      LOGGER.error("The exception is in processATGResponse method:", e);
      throw new GCException(e);
    }
  }

  /**
   * This methods updates QuantityToShip on change order input. QuantityToShip
   * is fetched from ATG request document
   *
   * @param docChangeOrder
   * @param inDoc
   * @throws TransformerException
   */
  private void updateQuantityToShipOnOrder(Document docChangeOrder,
      Document inDoc) throws TransformerException {
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input Documents to updateQuantityToShipOnOrder.GCATGPromoService:: Document docChangeOrder"
          + GCXMLUtil.getXMLString(docChangeOrder)
          + "Document inDoc"
          + GCXMLUtil.getXMLString(inDoc));
    }
    Map<String, String> primeLineNoQtyToShipMap = new HashMap<String, String>();
    NodeList nlLineItems = XPathAPI
        .selectNodeList(inDoc,
            "Envelope/Body/PricingRequest/ShoppingCarts/ShoppingCart/LineItems/LineItem");


    // Fix for 5554 : Apart from LineItem loop on KitLine items, to add QtyToShip for Kits.
    NodeList nlKitLineItems = XPathAPI
        .selectNodeList(inDoc,
            "Envelope/Body/PricingRequest/ShoppingCarts/ShoppingCart/LineItems/KitLineItem");

    for (int i = 0; i < nlKitLineItems.getLength(); i++) {
      Element lineItemEle = (Element) nlKitLineItems.item(i);
      Document docLineItem = GCXMLUtil
          .getDocumentFromElement(lineItemEle);
      Element lineItemId = (Element) docLineItem.getDocumentElement()
          .getElementsByTagName("v0:LineItemId").item(0);
      String primeLineNo = lineItemId.getTextContent();
      Element qtyToShipEle = (Element) docLineItem.getDocumentElement()
          .getElementsByTagName("v0:QuantityToShip").item(0);
      String qtyToShip = qtyToShipEle.getTextContent();
      if (!YFCCommon.isVoid(primeLineNo)) {
        primeLineNoQtyToShipMap.put(primeLineNo, qtyToShip);
      }
    }

    // Fix End.

    for (int i = 0; i < nlLineItems.getLength(); i++) {
      Element lineItemEle = (Element) nlLineItems.item(i);
      Document docLineItem = GCXMLUtil
          .getDocumentFromElement(lineItemEle);
      Element lineItemId = (Element) docLineItem.getDocumentElement()
          .getElementsByTagName("v0:LineItemId").item(0);
      String primeLineNo = lineItemId.getTextContent();
      Element qtyToShipEle = (Element) docLineItem.getDocumentElement()
          .getElementsByTagName("v0:QuantityToShip").item(0);
      String qtyToShip = qtyToShipEle.getTextContent();
      if (!YFCCommon.isVoid(primeLineNo)) {
        primeLineNoQtyToShipMap.put(primeLineNo, qtyToShip);
      }
    }

    YFCDocument yfcDocChangeOrder = YFCDocument
        .getDocumentFor(docChangeOrder);
    YFCElement orderEle = yfcDocChangeOrder.getDocumentElement();
    YFCElement orderLinesEle = orderEle
        .getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> orderLineList = orderLinesEle
        .getElementsByTagName(GCConstants.ORDER_LINE);
    for (YFCElement orderLineEle : orderLineList) {
      String primeLineNo = orderLineEle
          .getAttribute(GCConstants.PRIME_LINE_NO);
      if (!YFCCommon.isVoid(primeLineNo)) {
        orderLineEle.setAttribute("QuantityToShip",
            primeLineNoQtyToShipMap.get(primeLineNo));
      }
    }

  }

  private String determineShippingAmountInCaseOfSTS(String sShippingAmount,
      String sShippingMethod) throws GCException {

    try {
      NodeList nlShippingMethod = XPathAPI
          .selectNodeList(
              docATGResponseOp,
              "Envelope/Body/PricingResponse/ShippingPriceInfo/ShippingMethodInfo/ShipByCarrier/ShippingMethod");
      for (int i = 0; i < nlShippingMethod.getLength(); i++) {
        Node eleShippingPriceInfo = nlShippingMethod.item(i);
        String sShippingMethodOp = eleShippingPriceInfo
            .getTextContent();
        if (sShippingMethodOp.equals(sShippingMethod)) {
          Element eleShippingPrice = (Element) eleShippingPriceInfo
              .getParentNode().getParentNode().getParentNode();

          Document docShippingPriceInfo = GCXMLUtil.getDocument(
              eleShippingPrice, true);
          sShippingAmount = XPathAPI.selectSingleNode(
              docShippingPriceInfo,
              "ShippingPriceInfo/ShippingPrice").getTextContent();
        }
      }
      return sShippingAmount;
    } catch (Exception e) {
      LOGGER.error(
          "The exception is in determineShippingAmountInCaseOfSTS method:",
          e);
      throw new GCException(e);
    }
  }

  /**
   *
   * Description of removeFreeGiftItem - In this method we are removing
   * already existing free gift items
   *
   * @param docGetOrdOut
   * @param docOut
   * @param eleOrderLines
   * @throws Exception
   *
   */
  private void removeFreeGiftItem(Document docGetOrdOut) throws GCException {
    try {
      LOGGER.debug("Class: GCATGPromoService Method: removeFreeGiftItem BEGIN");
      NodeList nlOrderLine = GCXMLUtil.getNodeListByXpath(docGetOrdOut,
          "OrderList/Order/OrderLines/OrderLine");
      for (int iCounter = 0; iCounter < nlOrderLine.getLength(); iCounter++) {
        Element eleOrderLine = (Element) nlOrderLine.item(iCounter);
        String sOrderLineKey = eleOrderLine
            .getAttribute(GCConstants.ORDER_LINE_KEY);
        Document docEleOrderLine = GCXMLUtil
            .getDocumentFromElement(eleOrderLine);
        String sExtnIsFreeGiftItem = GCXMLUtil.getAttributeFromXPath(
            docEleOrderLine, "//@ExtnIsFreeGiftItem");
        if (GCConstants.FLAG_Y.equalsIgnoreCase(sExtnIsFreeGiftItem)) {
          Element eleCOOrderLine = GCXMLUtil.getElementByXPath(
              docChangeOrder, "//OrderLine[@OrderLineKey='"
                  + sOrderLineKey + "']");

          eleCOOrderLine.setAttribute(GCConstants.ACTION,
              GCConstants.REMOVE);
        }
      }

      LOGGER.debug("Class: GCATGPromoService Method: removeFreeGiftItem END");
    } catch (Exception e) {
      LOGGER.error("The exception is in removeFreeGiftItem method:", e);
      throw new GCException(e);
    }
  }

  /**
   *
   *
   * Description of processKitLineItems
   *
   * @param docKitLineItem
   * @param docOut
   * @param sExtnFulFillmentType
   * @param nLItemDiscount
   * @param nLOrderDiscount
   * @param eleOrderLines
   * @throws Exception
   *
   */
  private void processKitLineItems(YFSEnvironment env,
      Document docKitLineItem, Document docChangeOrder,
      String sExtnFulFillmentType, NodeList nLItemDiscount,
      NodeList nLOrderDiscount, Element eleRoot,
      String sSkipShippingChargeForPLCC) throws GCException {
    try {
      LOGGER.debug("Class: GCATGPromoService Method: processKitLineItems BEGIN");
      Boolean isFreeGiftItem = false;
      Element eleOrderLines = GCXMLUtil.getElementByXPath(docChangeOrder,
          "//Order/OrderLines");
      Element eleKitLineItemId = GCXMLUtil.getElementByXPath(
          docKitLineItem, "//KitLineItem/LineItemId");
      String sLineItemId = "";
      if (!YFCCommon.isVoid(eleKitLineItemId)) {
        sLineItemId = GCXMLUtil.getElementByXPath(docKitLineItem,
            "//KitLineItem/LineItemId").getTextContent();
      }
      String skuId = GCXMLUtil.getElementByXPath(docKitLineItem,
          "//KitLineItem/HeaderSkuId").getTextContent();
      String quantity = GCXMLUtil.getElementByXPath(docKitLineItem,
          "//KitLineItem/Quantity").getTextContent();

      Element eleItemDisc = GCXMLUtil.getElementByXPath(docKitLineItem,
          "//KitLineItem/HeaderItemPriceInfo/Discounts/ItemDiscount");

      if (YFCCommon.isVoid(eleKitLineItemId)) {
        Element eleItem = docChangeOrder
            .createElement(GCConstants.ITEM);
        isFreeGiftItem = true;
        Element eleOrderLineFG = docChangeOrder
            .createElement(GCConstants.ORDER_LINE);
        eleOrderLineFG.setAttribute(GCConstants.ACTION,
            GCConstants.CREATE);
        eleOrderLineFG.setAttribute(GCConstants.FULFILLMENT_TYPE,
            sExtnFulFillmentType);
        eleOrderLineFG.setAttribute(GCConstants.ORDERED_QTY, quantity);
        eleOrderLineFG.setAttribute(GCConstants.KIT_CODE,
            GCConstants.BUNDLE);
        eleItem.setAttribute(GCConstants.ITEM_ID, skuId);
        eleItem.setAttribute(GCConstants.PRODUCT_CLASS,
            GCConstants.REGULAR);
        eleItem.setAttribute(GCConstants.UOM, GCConstants.EACH);
        Element eleExtnOL = docChangeOrder
            .createElement(GCConstants.EXTN);
        eleExtnOL.setAttribute(GCConstants.EXTN_IS_FREE_GIFT_ITEM,
            GCConstants.FLAG_Y);

        /* Setting Unit Price to Zero. This is a requirement. */
        Element eleLinePriceInfo = docChangeOrder
            .createElement(GCConstants.LINE_PRICE_INFO);
        eleOrderLineFG.setAttribute(GCConstants.PRIME_LINE_NO,
            sLineItemId);
        eleLinePriceInfo.setAttribute(GCConstants.UNIT_PRICE,
            GCConstants.ZERO_AMOUNT);
        eleLinePriceInfo.setAttribute(GCConstants.IS_PRICE_LOCKED,
            GCConstants.FLAG_Y);
        eleOrderLineFG.appendChild(eleLinePriceInfo);

        eleOrderLineFG.appendChild(eleExtnOL);
        eleOrderLineFG.appendChild(eleItem);
      }
      Element eleOrderLine = GCXMLUtil.getElementByXPath(docChangeOrder,
          "//OrderLine[@PrimeLineNo='" + sLineItemId + "']");

      Element eleLineChargesOfOL = GCXMLUtil.getElementByXPath(
          docChangeOrder, "//OrderLine[@PrimeLineNo='" + sLineItemId
          + "']/LineCharges");
      eleOrderLine.removeChild(eleLineChargesOfOL);

      Element eleLineCharges = docChangeOrder
          .createElement(GCConstants.LINE_CHARGES);
      eleLineCharges.setAttribute(GCConstants.RESET, GCConstants.FLAG_Y);
      if (YFCCommon.equals(sSkipShippingChargeForPLCC, "Y")) {
        replaceLineShippingChargeToOldValue(eleLineChargesOfOL,
            eleLineCharges);
      }

      eleOrderLine.setAttribute(GCConstants.PRIME_LINE_NO, sLineItemId);

      /* Subline No is being hardcoded to 1 */
      eleOrderLine.setAttribute(GCConstants.SUB_LINE_NO,
          GCConstants.INT_ONE);

      applyItemAndOrderLevelDiscounts(env, eleItemDisc, nLItemDiscount,
          eleOrderLine, docKitLineItem, eleLineCharges,
          nLOrderDiscount, isFreeGiftItem);

      eleOrderLine.appendChild(eleLineCharges);
      eleOrderLines.appendChild(eleOrderLine);

      eleRoot.appendChild(eleOrderLines);

      LOGGER.debug("Class: GCATGPromoService Method: processKitLineItems END");
    } catch (Exception e) {
      LOGGER.error("The exception is in processKitLineItems method:", e);
      throw new GCException(e);
    }

  }

  private void processWarrItem(YFSEnvironment env, Document docWarrItem,
      Document docChangeOrder, String sExtnFulFillmentType,
      NodeList nLWarItemDiscount, NodeList nLWarItemOrdrDiscount,
      Element eleRoot) throws GCException, FactoryConfigurationError {
    try {
      LOGGER.debug("Class: GCATGPromoService Method: processWarrItem BEGIN");
      Element eleOrderLines = GCXMLUtil.getElementByXPath(docChangeOrder,
          "//Order/OrderLines");
      Boolean isFreeGiftItem = false;
      String sLineItemId = "";
      Element eleLineItemId = GCXMLUtil.getElementByXPath(docWarrItem,
          "//WarrantyItem/LineItemId");
      if (!YFCCommon.isVoid(eleLineItemId)) {
        sLineItemId = GCXMLUtil.getElementByXPath(docWarrItem,
            "//WarrantyItem/LineItemId").getTextContent();
      }
      Element eleItemDisc = GCXMLUtil.getElementByXPath(docWarrItem,
          "//WarrantyItem/ItemPriceInfo/Discounts/ItemDiscount");
      Element eleOrderLine = GCXMLUtil.getElementByXPath(docChangeOrder,
          "//OrderLine[@PrimeLineNo='" + sLineItemId + "']");
      Element eleLineChargesOfOL = GCXMLUtil.getElementByXPath(
          docChangeOrder, "//OrderLine[@PrimeLineNo='" + sLineItemId
          + "']/LineCharges");
      eleOrderLine.removeChild(eleLineChargesOfOL);

      Element eleLineCharges = docChangeOrder
          .createElement(GCConstants.LINE_CHARGES);
      eleLineCharges.setAttribute(GCConstants.RESET, GCConstants.FLAG_Y);
      eleOrderLine.setAttribute(GCConstants.PRIME_LINE_NO, sLineItemId);

      /* Subline No is being hardcoded to 1 */
      eleOrderLine.setAttribute(GCConstants.SUB_LINE_NO,
          GCConstants.INT_ONE);

      applyItemAndOrderLevelDiscounts(env, eleItemDisc,
          nLWarItemDiscount, eleOrderLine, docWarrItem,
          eleLineCharges, nLWarItemOrdrDiscount, isFreeGiftItem);

      Element eleFreeGiftSkuId = GCXMLUtil.getElementByXPath(docWarrItem,
          "//FreeGiftItem/SkuId");
      if (!YFCCommon.isVoid(eleFreeGiftSkuId)) {
        boolean bIsDependencyRqrd = true;
        LOGGER.debug("Calling handleFreeGiftsFromATG from processWarrItem");
        handleFreeGiftsFromATG(env, docWarrItem, sLineItemId,
            sExtnFulFillmentType, bIsDependencyRqrd);
      }
      eleOrderLine.appendChild(eleLineCharges);
      eleOrderLines.appendChild(eleOrderLine);
      eleRoot.appendChild(eleOrderLines);

      LOGGER.debug("Class: GCATGPromoService Method: processWarrItem END");
    } catch (Exception e) {
      LOGGER.error("The exception is in processWarrItem method:", e);
      throw new GCException(e);
    }
  }

  /**
   *
   * Description of processLineItems
   *
   * @param env
   *
   * @param docLineItem
   * @param docOut
   * @param sExtnFulFillmentType
   * @param nLItemDiscount
   * @param nLOrderDiscount
   * @param eleRoot
   * @throws Exception
   *
   */
  private void processLineItems(YFSEnvironment env, Document docLineItem,
      String sExtnFulFillmentType, NodeList nLItemDiscount,
      NodeList nLOrderDiscount, String sSkipShippingChargeForPLCC)
          throws GCException {
    try {
      LOGGER.debug("Class: GCATGPromoService Method: processLineItems BEGIN");
      String sLineItemId = "";
      /* This flag determines if an item is free gift item or not. */
      Boolean isFreeGiftItem = false;
      Element eleLineItemId = GCXMLUtil.getElementByXPath(docLineItem,
          "LineItem/LineItemId");
      if (!YFCCommon.isVoid(eleLineItemId)) {
        sLineItemId = GCXMLUtil.getElementByXPath(docLineItem,
            "LineItem/LineItemId").getTextContent();
      }
      Element eleItemDisc = GCXMLUtil.getElementByXPath(docLineItem,
          "LineItem/ItemPriceInfo/Discounts/ItemDiscount");

      Element eleOrderLine = GCXMLUtil.getElementByXPath(docChangeOrder,
          "//OrderLine[@PrimeLineNo='" + sLineItemId + "']");
      if (YFCCommon.isVoid(eleLineItemId)
          || YFCCommon.isVoid(eleOrderLine)) {
        isFreeGiftItem = true;
        boolean bIsDependencyRqrd = false;
        LOGGER.debug("Calling handleFreeGiftsFromATG from processLineItems");
        eleOrderLine = handleFreeGiftsFromATG(env, docLineItem,
            sLineItemId, sExtnFulFillmentType, bIsDependencyRqrd);
      }
      Element eleLineChargesOfOL = GCXMLUtil.getElementByXPath(
          docChangeOrder, "//OrderLine[@PrimeLineNo='" + sLineItemId
          + "']/LineCharges");
      if (!YFCCommon.isVoid(eleLineItemId)
          && !YFCCommon.isVoid(eleLineChargesOfOL)) {
        eleOrderLine.removeChild(eleLineChargesOfOL);
      }
      Element eleLineCharges = docChangeOrder
          .createElement(GCConstants.LINE_CHARGES);
      eleLineCharges.setAttribute(GCConstants.RESET, GCConstants.FLAG_Y);
      if (YFCCommon.equals(sSkipShippingChargeForPLCC, "Y")) {
        replaceLineShippingChargeToOldValue(eleLineChargesOfOL,
            eleLineCharges);
      }

      /*
       * Discounts are applied on free gift item as well as we need
       * promoID, promo desc. But promo amount will always be 0.00 for
       * free gift items.
       */
      applyItemAndOrderLevelDiscounts(env, eleItemDisc, nLItemDiscount,
          eleOrderLine, docLineItem, eleLineCharges, nLOrderDiscount,
          isFreeGiftItem);

      eleOrderLine.appendChild(eleLineCharges);

      LOGGER.debug("Class: GCATGPromoService Method: processLineItems END");
    } catch (Exception e) {
      LOGGER.error("The exception is in processLineItems method:", e);
      throw new GCException(e);
    }
  }

  private void modifyItemDetails(YFSEnvironment env, String skuId,
      Element eleItemDetails, Document docChangeOrder, String sOrgCode2)
          throws GCException {

    try {
      // Preparing inputDoc for getItemList API call
      Document docGtItmDtIp = GCXMLUtil.createDocument(GCConstants.ITEM);
      Element eleItemRoot = docGtItmDtIp.getDocumentElement();
      eleItemRoot.setAttribute(GCConstants.ITEM_ID, skuId);
      eleItemRoot.setAttribute(GCConstants.CALLING_ORGANIZATION_CODE,
          sOrgCode2);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("modifyItemDeatils() - get Items API input"
            + GCXMLUtil.getXMLString(docGtItmDtIp));
      }
      Document docGtItmDtTmp = GCXMLUtil
          .createDocument(GCConstants.ITEM_LIST);
      Element eleRootTemplate = docGtItmDtTmp.getDocumentElement();
      Element eleItem = docGtItmDtTmp.createElement(GCConstants.ITEM);
      eleItem.setAttribute("ItemKey", "");
      eleItem.setAttribute(GCConstants.ORGANIZATION_CODE, "");
      eleItem.setAttribute(GCConstants.UOM, "");

      Element elePrimInfo = docGtItmDtTmp
          .createElement(GCConstants.PRIMARY_INFORMATION);
      elePrimInfo.setAttribute(GCConstants.IS_HAZMAT, "");
      elePrimInfo.setAttribute(GCConstants.UNIT_WEIGHT, "");
      elePrimInfo.setAttribute(GCConstants.ITEM_TYPE, "");

      Element eleExtn = docGtItmDtTmp.createElement(GCConstants.EXTN);
      eleExtn.setAttribute(GCConstants.EXTN_IS_TRUCK_SHIP, "");
      eleExtn.setAttribute(GCConstants.EXTN_ITEM_STATUS, "");
      eleExtn.setAttribute(GCConstants.EXTN_SET_CODE, "");
      eleItem.appendChild(eleExtn);
      eleItem.appendChild(elePrimInfo);
      eleRootTemplate.appendChild(eleItem);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("template for getItemList API call"
            + GCXMLUtil.getXMLString(docGtItmDtTmp));
      }
      // Calling getItemList API.
      Document docGtItmDtOt = GCCommonUtil.invokeAPI(env,
          GCConstants.API_GET_COMPLETE_ITEM_LIST, docGtItmDtIp,
          docGtItmDtTmp);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("template for getItemList API call"
            + GCXMLUtil.getXMLString(docGtItmDtOt));
      }
      String sUOM = GCXMLUtil.getAttributeFromXPath(docGtItmDtOt,
          "//@UnitOfMeasure");
      String sOrgCode = GCXMLUtil.getAttributeFromXPath(docGtItmDtOt,
          "//@OrganizationCode");
      String sIsHazmat = GCXMLUtil.getAttributeFromXPath(docGtItmDtOt,
          "//@IsHazmat");
      String sItemType = GCXMLUtil.getAttributeFromXPath(docGtItmDtOt,
          "//@ItemType");
      String sItemKey = GCXMLUtil.getAttributeFromXPath(docGtItmDtOt,
          "//@ItemKey");
      String sUnitWeight = GCXMLUtil.getAttributeFromXPath(docGtItmDtOt,
          "//@UnitWeight");
      String sExtnTruckShip = GCXMLUtil.getAttributeFromXPath(
          docGtItmDtOt, "//@ExtnIsTruckShip");
      String sExtnItemStatus = GCXMLUtil.getAttributeFromXPath(
          docGtItmDtOt, "//@ExtnItemStatus");
      String sExtnSetCode = GCXMLUtil.getAttributeFromXPath(docGtItmDtOt,
          "Extn//@ExtnSetCode");

      eleItemDetails.setAttribute(GCConstants.ITEM_ID, skuId);
      eleItemDetails.setAttribute("ItemKey", sItemKey);
      eleItemDetails.setAttribute("OrganizationCode", sOrgCode);
      eleItemDetails.setAttribute("UnitOfMeasure", sUOM);
      eleItemDetails
      .setAttribute(GCConstants.EXTN_SET_CODE, sExtnSetCode);
      Element elePrimInformation = docChangeOrder
          .createElement(GCConstants.PRIMARY_INFORMATION);
      elePrimInformation.setAttribute(GCConstants.IS_HAZMAT, sIsHazmat);
      elePrimInformation.setAttribute(GCConstants.UNIT_WEIGHT,
          sUnitWeight);
      elePrimInformation.setAttribute(GCConstants.ITEM_TYPE, sItemType);

      Element eleExtnItemDetails = docChangeOrder
          .createElement(GCConstants.EXTN);
      eleExtnItemDetails.setAttribute(GCConstants.EXTN_IS_TRUCK_SHIP,
          sExtnTruckShip);
      eleExtnItemDetails.setAttribute(GCConstants.EXTN_ITEM_STATUS,
          sExtnItemStatus);
      eleItemDetails.appendChild(elePrimInformation);
      eleItemDetails.appendChild(eleExtnItemDetails);

    } catch (Exception e) {
      LOGGER.error("The exception is in modifyItemDetails method:", e);
      throw new GCException(e);
    }
  }

  /**
   *
   * Description of applyItemAndOrderLevelDiscounts
   *
   * @param eleItemDisc
   * @param nLItemDiscount
   * @param docOut
   * @param eleOrderLine
   * @param docLineItem
   * @param eleLineCharges
   * @param nLOrderDiscount
   * @param sExtnFulFillmentType
   * @param isFreeGiftItem
   * @throws Exception
   *
   */
  private void applyItemAndOrderLevelDiscounts(YFSEnvironment env,
      Element eleItemDisc, NodeList nLItemDiscount, Element eleOrderLine,
      Document docLineItem, Element eleLineCharges,
      NodeList nLOrderDiscount, Boolean isFreeGiftItem)
          throws GCException {
    try {
      LOGGER.debug("Class: GCATGPromoService Method: applyItemAndOrderLevelDiscounts BEGIN");

      Element eleOrderLines = GCXMLUtil.getElementByXPath(docChangeOrder,
          "//Order/OrderLines");
      String sItemDiscType = "";
      String sPromotionId = "";
      String sPromotionDescription = "";
      String sDiscountAmount = "0.00";
      String sExtnDiscountPercentage = "";
      String sItemDiscountCouponId = "";
      /* Fix added for 3483 - Begin */
      String sOrderDiscountDesc = "";
      String sOrderDiscountCouponId = "";
      String sLineItemId = "";
      Element eleLineItemId = GCXMLUtil.getElementByXPath(docLineItem,
          "LineItem/LineItemId");
      if (!YFCCommon.isVoid(eleLineItemId)) {
        sLineItemId = GCXMLUtil.getElementByXPath(docLineItem,
            "LineItem/LineItemId").getTextContent();
      }
	  
	  NodeList nLOrderHeaderDiscount =
          XPathAPI.selectNodeList(docATGResponseOp,
              "Envelope/Body/PricingResponse/OrderPriceInfo/OrderDiscounts/OrderDiscount");
 
	  /*Commentting out as a part of code merge as it is defined below as well inside the loop : Start   
	  Element eleOrderDiscountDesc = GCXMLUtil
          .getElementByXPath(
              docATGResponseOp,
              "Envelope/Body/PricingResponse/OrderPriceInfo/OrderDiscounts/OrderDiscount/PromotionDescription");
      Element eleOrderDisccountCouponId = GCXMLUtil
          .getElementByXPath(
              docATGResponseOp,
              "Envelope/Body/PricingResponse/OrderPriceInfo/OrderDiscounts/OrderDiscount/CouponId");
      if (!YFCCommon.isVoid(eleOrderDiscountDesc)) {
        sOrderDiscountDesc = eleOrderDiscountDesc.getTextContent();
      }
      if (!YFCCommon.isVoid(eleOrderDisccountCouponId)) {
        sOrderDiscountCouponId = eleOrderDisccountCouponId
            .getTextContent();
      }
      Commentting out as a part of code merge as it is defined below as well inside the loop : End */

      /* Fix added for 3483 - End */

      if (!YFCCommon.isVoid(eleItemDisc)) {
        for (int j = 0; j < nLItemDiscount.getLength(); j++) {
          eleItemDisc = (Element) nLItemDiscount.item(j);
          Document docItemDisc = GCXMLUtil
              .getDocumentFromElement(eleItemDisc);

          sItemDiscType = GCXMLUtil.getElementByXPath(docItemDisc,
              "/ItemDiscount/Type").getTextContent();
          sPromotionId = GCXMLUtil.getElementByXPath(docItemDisc,
              "/ItemDiscount/PromotionId").getTextContent();
          sPromotionDescription = GCXMLUtil.getElementByXPath(
              docItemDisc, "ItemDiscount/PromotionDescription")
              .getTextContent();
          sDiscountAmount = GCXMLUtil.getElementByXPath(docItemDisc,
              "/ItemDiscount/DiscountAmount").getTextContent();
          Element eleCouponId = GCXMLUtil.getElementByXPath(
              docItemDisc, "/ItemDiscount/CouponId");
          if (!YFCCommon.isVoid(eleCouponId)) {
            sItemDiscountCouponId = eleCouponId.getTextContent();
          }
          
          //mPOS-739 Start
          else{
        	  sItemDiscountCouponId = "";
          }
          
          //mPOS-739 End
          
          Double dDiscountAmount = Double
              .parseDouble(sDiscountAmount);
          dDiscountAmount = Math.abs(dDiscountAmount);
          sDiscountAmount = dDiscountAmount.toString();
          /* Free gift Item added with Zero Promo Charge */
          if (isFreeGiftItem) {
            sDiscountAmount = "0.00";
          }
          Element eleLineCharge = docChangeOrder
              .createElement(GCConstants.LINE_CHARGE);
          Element eleExtnLineCharge = docChangeOrder
              .createElement(GCConstants.EXTN);

          eleExtnLineCharge.setAttribute(
              GCConstants.EXTN_DISCOUNT_TYPE, sItemDiscType);
          eleExtnLineCharge.setAttribute(
              GCConstants.EXTN_PROMOTION_ID, sPromotionId);
          eleExtnLineCharge.setAttribute(
              GCConstants.EXTN_PROMOTION_DESC,
              sPromotionDescription);
          eleExtnLineCharge.setAttribute(
              GCConstants.EXTN_DISCOUNT_PERCENTAGE,
              sExtnDiscountPercentage);
          eleExtnLineCharge.setAttribute("ExtnCouponID",
              sItemDiscountCouponId);
          
          eleLineCharge.setAttribute(GCConstants.CHARGE_PER_LINE,
              sDiscountAmount);
          eleLineCharge.setAttribute(GCConstants.CHARGE_CATEGORY,
              GCConstants.PROMOTION);
          eleLineCharge.setAttribute(GCConstants.CHARGE_NAME,
              sPromotionId);
          eleLineCharge.appendChild(eleExtnLineCharge);
          eleLineCharges.appendChild(eleLineCharge);
        }
      }
      LOGGER.debug("nLOrderDiscount Length:"
          + nLOrderDiscount.getLength());
      for (int j = 0; j < nLOrderDiscount.getLength(); j++) {
        Element eleOrderDisc = (Element) nLOrderDiscount.item(j);
        Document docOrderDisc = GCXMLUtil
            .getDocumentFromElement(eleOrderDisc);
        String sOrderDiscPromoId = "";
        String sOrderDiscShare = "";
        Element eleLineCharge = docChangeOrder
            .createElement(GCConstants.LINE_CHARGE);
        Element eleExtnLineCharge = docChangeOrder
            .createElement(GCConstants.EXTN);

        sOrderDiscPromoId = GCXMLUtil.getElementByXPath(docOrderDisc,
            "OrderDiscount/OrderDiscountPromoId").getTextContent();
        sOrderDiscShare = GCXMLUtil.getElementByXPath(docOrderDisc,
            "OrderDiscount/OrderDiscountShare").getTextContent();
        for (int k = 0; k < nLOrderHeaderDiscount.getLength(); k++) {
          Element eleOrderHeaderDisc = (Element) nLOrderHeaderDiscount.item(k);
          Document docOrderHeaderDisc = GCXMLUtil.getDocumentFromElement(eleOrderHeaderDisc);

          String sOrderHeaderDiscPromoId =
              GCXMLUtil.getElementByXPath(docOrderHeaderDisc, "OrderDiscount/PromotionId").getTextContent();
          if (YFCUtils.equals(sOrderDiscPromoId, sOrderHeaderDiscPromoId)) {
            Element eleOrderDiscountDesc =
                GCXMLUtil.getElementByXPath(docOrderHeaderDisc, "OrderDiscount/PromotionDescription");
            Element eleOrderDisccountCouponId =
                GCXMLUtil.getElementByXPath(docOrderHeaderDisc,
                    "OrderDiscount/CouponId");
            if (!YFCCommon.isVoid(eleOrderDiscountDesc)) {
              sOrderDiscountDesc = eleOrderDiscountDesc.getTextContent();
            }
            if (!YFCCommon.isVoid(eleOrderDisccountCouponId)) {
              sOrderDiscountCouponId = eleOrderDisccountCouponId.getTextContent();
            }
            break;
          }
        }
        /* Free gift Item added with Zero Promo Charge */
        if (isFreeGiftItem) {
          sOrderDiscShare = "0.00";
        }
        eleLineCharge.setAttribute(GCConstants.CHARGE_PER_LINE,
            sOrderDiscShare);
        eleLineCharge.setAttribute(GCConstants.CHARGE_CATEGORY,
            GCConstants.PROMOTION);
        eleLineCharge.setAttribute(GCConstants.CHARGE_NAME,
            sOrderDiscPromoId);
        eleExtnLineCharge.setAttribute(GCConstants.EXTN_PROMOTION_ID,
            sOrderDiscPromoId);
        eleExtnLineCharge.setAttribute(GCConstants.EXTN_PROMOTION_DESC,
            sOrderDiscountDesc);
        eleExtnLineCharge.setAttribute("ExtnCouponID",
            sOrderDiscountCouponId);
        eleLineCharge.appendChild(eleExtnLineCharge);
        eleLineCharges.appendChild(eleLineCharge);
        eleOrderLines.appendChild(eleOrderLine);
        sOrderDiscountCouponId = "";  //mPOS-739 fix
      }
      Element eleFreeGiftSkuId = GCXMLUtil.getElementByXPath(docLineItem,
          "//FreeGiftItem/SkuId");
      if (!YFCCommon.isVoid(eleFreeGiftSkuId)) {
        boolean bIsDependencyRqrd = true;
        LOGGER.debug("Calling handleFreeGiftsFromATG from applyItemAndOrderLevelDiscounts");
        handleFreeGiftsFromATG(env, docLineItem, sLineItemId,
            sExtnFulFillmentType, bIsDependencyRqrd);
      }

      LOGGER.debug("Class: GCATGPromoService Method: applyItemAndOrderLevelDiscounts END");
    } catch (Exception e) {
      LOGGER.error(
          "The exception is in applyItemAndOrderLevelDiscounts method:",
          e);
      throw new GCException(e);
    }
  }

  /**
   *
   * Description of storeShippingChargesForDiffShipMethods
   *
   * @param env
   * @param nLShippingMethodInfo
   * @param sOrderHeaderKey
   * @throws Exception
   * @throws FactoryConfigurationError
   *
   */
  private void storeShippingChargesForDiffShipMethods(YFSEnvironment env,
      NodeList nLShippingMethodInfo, String sOrderHeaderKey)
          throws GCException, FactoryConfigurationError {
    try {
      LOGGER.debug("Class: GCATGPromoService Method: storeShippingChargesForDiffShipMethods BEGIN");
      Document docGetOrderShipChargeListIn = GCXMLUtil
          .createDocument(GCConstants.GC_ORDER_SHIPPING_CHARGE);
      Element eleRootGetOrderShip = docGetOrderShipChargeListIn
          .getDocumentElement();
      eleRootGetOrderShip.setAttribute(GCConstants.ORDER_HEADER_KEY,
          sOrderHeaderKey);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Input to GetOrderShipChargeList "
            + GCXMLUtil.getXMLString(docGetOrderShipChargeListIn));
      }
      Document docGetOrderShipChargeListOut = GCCommonUtil.invokeService(
          env, "GCGetOrderShippingChargeList",
          docGetOrderShipChargeListIn);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("OutDoc GetOrderShipChargeList "
            + GCXMLUtil.getXMLString(docGetOrderShipChargeListIn));
      }
      NodeList nLOrderShippingCharge = GCXMLUtil.getNodeListByXpath(
          docGetOrderShipChargeListOut,
          "GCOrderShippingChargeList/GCOrderShippingCharge");
      List<String> listOrderShippingCharge = new ArrayList<String>();
      int iNlOrderShipChargeLen = nLOrderShippingCharge.getLength();

      for (int z = 0; z < iNlOrderShipChargeLen; z++) {
        Element eleOrderShippingCharge = (Element) nLOrderShippingCharge
            .item(z);
        String sLevelOfService = eleOrderShippingCharge
            .getAttribute(GCConstants.LEVEL_OF_SERVICE);
        listOrderShippingCharge.add(sLevelOfService);
      }
      for (int i = 0; i < nLShippingMethodInfo.getLength(); i++) {
        Element eleShippingMethodInfo = (Element) nLShippingMethodInfo
            .item(i);
        Document docShippingMethodInfo = GCXMLUtil
            .getDocumentFromElement(eleShippingMethodInfo);
        Element eleShippingMethod = GCXMLUtil
            .getElementByXPath(docShippingMethodInfo,
                "ShippingPriceInfo/ShippingMethodInfo/ShipByCarrier/ShippingMethod");
        if (!YFCCommon.isVoid(eleShippingMethod)) {
          String sShippingMethod = GCXMLUtil
              .getElementByXPath(docShippingMethodInfo,
                  "ShippingPriceInfo/ShippingMethodInfo/ShipByCarrier/ShippingMethod")
              .getTextContent();
          String sLOS = fetchLevelOfServiceUsingCommonCode(env,
              sShippingMethod);
          String sShippingPrice = GCXMLUtil.getElementByXPath(
              docShippingMethodInfo,
              "ShippingPriceInfo/ShippingPrice").getTextContent();
          Document docCreateShipChargeIn = GCXMLUtil
              .createDocument(GCConstants.GC_ORDER_SHIPPING_CHARGE);
          Element eleRoot = docCreateShipChargeIn
              .getDocumentElement();
          eleRoot.setAttribute(GCConstants.LEVEL_OF_SERVICE, sLOS);
          eleRoot.setAttribute(GCConstants.ORDER_HEADER_KEY,
              sOrderHeaderKey);
          eleRoot.setAttribute(GCConstants.SHIPPING_CHARGE_AMOUNT,
              sShippingPrice);
          eleRoot.setAttribute(GCConstants.ATG_SERVICE_CODE,
              sShippingMethod);
          /*
           * If Shipping Charges for LOS is already stored against the
           * Order then it is modified using change extended API.
           */
          if (listOrderShippingCharge.contains(sLOS)) {
            if (LOGGER.isDebugEnabled()) {
              LOGGER.debug("Input to GCChangeOrderShippingCharge"
                  + GCXMLUtil
                  .getXMLString(docCreateShipChargeIn));
            }
            GCCommonUtil
            .invokeService(
                env,
                GCConstants.GC_CHANGE_ORDER_SHIPPING_CHARGE_SERVICE,
                docCreateShipChargeIn);
          } else {
            if (LOGGER.isDebugEnabled()) {
              LOGGER.debug("Input to GCCreateOrderShippingCharge"
                  + GCXMLUtil
                  .getXMLString(docCreateShipChargeIn));
            }
            GCCommonUtil
            .invokeService(
                env,
                GCConstants.GC_CREATE_ORDER_SHIPPING_CHARGE_SERVICE,
                docCreateShipChargeIn);
          }
        }
      }
      LOGGER.debug("Class: GCATGPromoService Method: storeShippingChargesForDiffShipMethods END");
    } catch (Exception e) {
      LOGGER.error(
          "The exception is in storeShippingChargesForDiffShipMethods method:",
          e);
      throw new GCException(e);
    }
  }

  /**
   *
   * Description of fetchLevelOfServiceUsingCommonCode
   *
   * @param env
   * @param sShippingMethod
   * @return
   * @throws Exception
   *
   */
  private String fetchLevelOfServiceUsingCommonCode(YFSEnvironment env,
      String sShippingMethod) throws GCException {
    try {
      LOGGER.debug("Class: GCATGPromoService Method: fetchLevelOfServiceUsingCommonCode BEGIN");
      Document docGetCommonCodeIn = GCXMLUtil
          .createDocument(GCConstants.COMMON_CODE);
      Element eleRoot = docGetCommonCodeIn.getDocumentElement();
      eleRoot.setAttribute(GCConstants.CODE_TYPE,
          GCConstants.ATG_SHIPPING_MTHD);
      eleRoot.setAttribute(GCConstants.CODE_SHORT_DESCRIPTION,
          sShippingMethod);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Input Doc to getCommonCodeList"
            + GCXMLUtil.getXMLString(docGetCommonCodeIn));
      }
      Document docGetCommonCodeOut = GCCommonUtil.invokeAPI(env,
          GCConstants.GET_COMMON_CODE_LIST, docGetCommonCodeIn);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("OutDoc to getCommonCodeList"
            + GCXMLUtil.getXMLString(docGetCommonCodeOut));
      }
      String sLOS = GCXMLUtil.getAttributeFromXPath(docGetCommonCodeOut,
          "//@CodeValue");

      LOGGER.debug("Class: GCATGPromoService Method: fetchLevelOfServiceUsingCommonCode END");
      return sLOS;
    } catch (Exception e) {
      LOGGER.error(
          "The exception is in fetchLevelOfServiceUsingCommonCode method:",
          e);
      throw new GCException(e);
    }
  }

  public void callChangeOrderForATGPENValReset(YFSEnvironment env) {
    YFCDocument yfceDocOut = YFCDocument.createDocument("Order");
    yfceDocOut.getDocumentElement().setAttribute(
        GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    YFCElement eleExtnChangeOrder = yfceDocOut.getDocumentElement()
        .getChildElement("Extn", true);
    eleExtnChangeOrder.setAttribute("ExtnIsATGPENCallRequired", "1");
    GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, yfceDocOut,
        null);
  }

  public void callChangeOrder(YFSEnvironment env, Document docChangeOrder) {

    LOGGER.debug("Class: GCATGPromoService Method: callChangeOrder BEGIN");
    Element elePaymentMethodValue = (Element) env
        .getTxnObject("elePaymentMethodValue");
    if (!YFCCommon.isVoid(elePaymentMethodValue)) {
      String sPaymentMethodVal = elePaymentMethodValue.getTextContent();
      if ((GCConstants.PRIVATE_LABEL_CARD)
          .equalsIgnoreCase(sPaymentMethodVal) || GCConstants.PVT_LBL_CREDIT_CARD.equalsIgnoreCase(sPaymentMethodVal)) {
        String sShipping = "Shipping";
        Element eleHeaderCharge = GCXMLUtil.getElementByXPath(
            docChangeOrder,
            "Order/HeaderCharges/HeaderCharge[@ChargeCategory='"
                + sShipping + "']");
        if (!YFCCommon.isVoid(eleHeaderCharge)) {
          String sHeaderChargeToAdd = getExistingShippingCharge(env);
          eleHeaderCharge.setAttribute(GCConstants.CHARGE_AMOUNT,
              sHeaderChargeToAdd);
          LOGGER.debug("if Payment Method is PRIVATE_LABEL_CARD then adding "
              + "HeaderCharge/ChargeAmount value from docGetOrdOut"
              + GCXMLUtil.getXMLString(docChangeOrder));
        }
      }
    }
    GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER,
        docChangeOrder);
    LOGGER.debug("Class: GCATGPromoService Method: callChangeOrder End");
  }

  /**
   * This method handles the response form ATG for free gift item. Free gift
   * item can come as independent item as well as dependent line item
   *
   * @param env
   * @param docLineItem
   * @param sLineItemId
   * @param sExtnFulFillmentType
   * @param bIsDependencyRqrd
   * @throws GCException
   */
  private Element handleFreeGiftsFromATG(YFSEnvironment env,
      Document docLineItem, String sLineItemId,
      String sExtnFulFillmentType, boolean bIsDependencyRqrd)
          throws GCException {
	LOGGER.debug("handleFreeGiftsFromATG - BEGIN");
	LOGGER.debug("docLineItem in handleFreeGiftsFromATG"+GCXMLUtil.getXMLString(docLineItem));
	LOGGER.debug("handleFreeGiftsFromATG - sLineItemId "+sLineItemId+"sExtnFulFillmentType"+sExtnFulFillmentType
			+"bIsDependencyRqrd"+bIsDependencyRqrd);
	
    String skuId = "";
    String quantity = "";
    String sExtnSourceCode = "";
    String sDeliveryMethod = "";

    // MPOS - 1126 : set Delivery Method of Free item as Delivery method of Parent line
    YFCDocument changeOrderDoc = YFCDocument.getDocumentFor(docChangeOrder);
    YFCElement eleExtn = changeOrderDoc.getDocumentElement().getChildElement(GCConstants.EXTN);
    if (!YFCCommon.isVoid(eleExtn)) {
      sExtnSourceCode = eleExtn.getAttribute(GCConstants.EXTN_SOURCE_CODE);

      if (YFCCommon.equalsIgnoreCase(sExtnSourceCode, "mPOS")) {
        Element eleOrderLineParent = GCXMLUtil.getElementByXPath(changeOrderDoc.getDocument(),
            "Order/OrderLines/OrderLine[@PrimeLineNo='" + sLineItemId + "']");

        if (!YFCCommon.isVoid(eleOrderLineParent)) {
          sDeliveryMethod = eleOrderLineParent.getAttribute(GCConstants.DELIVERY_METHOD);
        }
      }
    }

    if (!bIsDependencyRqrd) {
      skuId = GCXMLUtil.getElementByXPath(docLineItem, "LineItem/SkuId")
          .getTextContent();
      quantity = GCXMLUtil.getElementByXPath(docLineItem,
          "LineItem/Quantity").getTextContent();
    } else {
      Element eleFreeGiftSkuId = GCXMLUtil.getElementByXPath(docLineItem,
          "//FreeGiftItem/SkuId");
      Element eleFreeGiftQuantity = GCXMLUtil.getElementByXPath(
          docLineItem, "//FreeGiftItem/Quantity");
      if (!YFCCommon.isVoid(eleFreeGiftSkuId)) {
        skuId = eleFreeGiftSkuId.getTextContent();
      }
      if (!YFCCommon.isVoid(eleFreeGiftQuantity)) {
        quantity = eleFreeGiftQuantity.getTextContent();
      }
    }
    Element eleOrderLine = docChangeOrder
        .createElement(GCConstants.ORDER_LINE);
    Element eleOrderLines = GCXMLUtil.getElementByXPath(docChangeOrder,
        "//Order/OrderLines");
    Element eleItem = docChangeOrder.createElement(GCConstants.ITEM);
    Element eleItemDetails = docChangeOrder
        .createElement(GCConstants.ITEM_DETAILS);
    String sOrgCode = GCXMLUtil.getAttributeFromXPath(docChangeOrder,
        "//Order/@EnterpriseCode");
    modifyItemDetails(env, skuId, eleItemDetails, docChangeOrder, sOrgCode);
    if (!YFCCommon.isVoid(eleItemDetails)) {
      String sExtnSetCode = eleItemDetails
          .getAttribute(GCConstants.EXTN_SET_CODE);
      if (!YFCCommon.isVoid(sExtnSetCode)
          && YFCCommon.equals(sExtnSetCode, "1")) {
        eleItem.setAttribute("ProductClass", "SET");
      } else {
        eleItem.setAttribute("ProductClass", "REGULAR");
      }
    }
    eleOrderLine.appendChild(eleItemDetails);
    eleOrderLine.setAttribute(GCConstants.ACTION, GCConstants.CREATE);

    if (!YFCCommon.equalsIgnoreCase(sExtnSourceCode, "mPOS")) {
      eleOrderLine.setAttribute(GCConstants.FULFILLMENT_TYPE,
          sExtnFulFillmentType);
    } else {
      eleOrderLine.setAttribute(GCConstants.DELIVERY_METHOD, sDeliveryMethod);
    }
    eleOrderLine.setAttribute(GCConstants.ORDERED_QTY, quantity);
    eleItem.setAttribute(GCConstants.ITEM_ID, skuId);
    eleItem.setAttribute(GCConstants.PRODUCT_CLASS, GCConstants.REGULAR);
    eleItem.setAttribute(GCConstants.UOM, GCConstants.EACH);
    Element eleExtnOL = docChangeOrder.createElement(GCConstants.EXTN);
    eleExtnOL.setAttribute(GCConstants.EXTN_IS_FREE_GIFT_ITEM,
        GCConstants.FLAG_Y);

    // mPOS-1121 : Start: fetching promotionID and promotionDesc for free gift item.
    String sFreeGiftPromotionID = "";
    String sFreeGiftPromotionDesc = "";
    Element eleFreeGiftPromotionID = GCXMLUtil.getElementByXPath(docLineItem,
        "//FreeGiftItem/ItemPriceInfo/Discounts/ItemDiscount/PromotionId");
    if (!YFCCommon.isVoid(eleFreeGiftPromotionID))
    {
      sFreeGiftPromotionID = eleFreeGiftPromotionID.getTextContent();
      eleExtnOL.setAttribute("ExtnFreeGiftItemPromoID", sFreeGiftPromotionID);
    }

    Element eleFreeGiftPromotionDesc = GCXMLUtil.getElementByXPath(docLineItem,
        "//FreeGiftItem/ItemPriceInfo/Discounts/ItemDiscount/PromotionDescription");
    if (!YFCCommon.isVoid(eleFreeGiftPromotionDesc))
    {
      sFreeGiftPromotionDesc = eleFreeGiftPromotionDesc.getTextContent();
      eleExtnOL.setAttribute("ExtnFreeGiftItemPromoDesc", sFreeGiftPromotionDesc);
    }

    // mPOS-1121 : End


    //mPOS-1194 and mPOS-1179 : Start

    String sCouponID = "";
    Element eleCouponID = GCXMLUtil.getElementByXPath(docLineItem,
        "//FreeGiftItem/ItemPriceInfo/Discounts/ItemDiscount/CouponId");
    if (!YFCCommon.isVoid(eleCouponID))
    {
      sCouponID = eleCouponID.getTextContent();
      eleExtnOL.setAttribute("ExtnFreeGiftCouponID", sCouponID);
    }

    else
    {
      eleExtnOL.setAttribute("ExtnFreeGiftCouponID", sCouponID);
    }

    //mPOS-1194 and mPOS-1179 : End

    if (bIsDependencyRqrd) {
      Element eleDependency = docChangeOrder
          .createElement(GCConstants.DEPENDENCY);
      eleDependency.setAttribute(GCConstants.DEPENDENT_PRIME_LINE_NO,
          sLineItemId);
      eleDependency.setAttribute(GCConstants.DEPENDENT_SUB_LINE_NO,
          GCConstants.INT_ONE);
      eleOrderLine.appendChild(eleDependency);
    }
    /* Setting Unit Price to Zero. */
    Element eleLinePriceInfo = docChangeOrder
        .createElement(GCConstants.LINE_PRICE_INFO);
    eleOrderLine.setAttribute(GCConstants.PRIME_LINE_NO, "");
    eleLinePriceInfo.setAttribute(GCConstants.UNIT_PRICE,
        GCConstants.ZERO_AMOUNT);
    eleLinePriceInfo.setAttribute(GCConstants.IS_PRICE_LOCKED,
        GCConstants.FLAG_Y);
    eleOrderLine.appendChild(eleLinePriceInfo);
    eleOrderLine.appendChild(eleExtnOL);
    eleOrderLine.appendChild(eleItem);
    eleOrderLines.appendChild(eleOrderLine);
    
    LOGGER.debug("handleFreeGiftsFromATG - END");
    LOGGER.debug("eleOrderLine returned from handlefreeGiftItem "+GCXMLUtil.getXMLString(GCXMLUtil
            .getDocumentFromElement(eleOrderLine)));
    
    return eleOrderLine;
    
  }

  /**
   *
   * Description of setProperties
   *
   * @param arg0
   * @throws Exception
   * @see com.yantra.interop.japi.YIFCustomApi#setProperties(java.util.Properties)
   */
  @Override
  public void setProperties(Properties arg0) throws GCException {
    try {
      sSiteId = arg0.getProperty(GCConstants.SITE_ID);
      sSource = arg0.getProperty(GCConstants.SOURCE);
      props = arg0;
    } catch (Exception e) {
      LOGGER.error("The exception is in setProperties method:", e);
      throw new GCException(e);
    }
  }

  /**
   * This method copy existing shipping charges at line level to OrderLine
   * charge of changeOrder call This is required when Shipping charges is
   * skipped (in case of PLCC call). But to avoid reseting of charges to zero,
   * old shipping charge is retained
   *
   * @param eleLineChargesOld
   * @param eleLineChargesNew
   */
  public void replaceLineShippingChargeToOldValue(Element eleLineChargesOld,
      Element eleLineChargesNew) {
    LOGGER.beginTimer("Class: GCATGPromoService Method: replaceLineShippingChargeToOldValue Start");

    if (YFCCommon.isVoid(eleLineChargesOld)) {
      LOGGER.verbose("Input to replaceLineShippingChargeToOldValue: eleLineChargesOld"
          + "null");
      return;
    }

    NodeList nlLineCharges = eleLineChargesOld
        .getElementsByTagName("LineCharge");
    for (int i = 0; i < nlLineCharges.getLength(); i++) {
      Element eleLineCharge = (Element) nlLineCharges.item(i);

      String sChargeCategory = eleLineCharge
          .getAttribute(GCConstants.CHARGE_CATEGORY);
      String sChargeName = eleLineCharge
          .getAttribute(GCConstants.CHARGE_NAME);
      if (YFCCommon.equals(sChargeCategory, GCConstants.SHIPPING)
          && YFCCommon.equals(sChargeName,
              GCConstants.SHIPPING_CHARGE)) {
        Element newLineCharge = docChangeOrder
            .createElement("LineCharge");
        eleLineChargesNew.appendChild(newLineCharge);
        GCXMLUtil.copyAttributes(eleLineCharge, newLineCharge);

      }

    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("replaceLineShippingChargeToOldValue docChangeOrder"
          + GCXMLUtil.getXMLString(docChangeOrder));
    }
    LOGGER.beginTimer("Class: GCATGPromoService Method: replaceLineShippingChargeToOldValue End");
  }

  public String getExistingShippingCharge(YFSEnvironment env) {

    LOGGER.debug("Class: GCATGPromoService Method: getExistingShippingCharge BEGIN");
    Float fTotalShippingCharge = (float) 0.00;
    Document docGetOrdOutput = (Document) env
        .getTxnObject("getOrderListOP");
    NodeList nlLineCharge = GCXMLUtil.getNodeListByXpath(docGetOrdOutput,
        "OrderList/Order/OrderLines/OrderLine/LineCharges/LineCharge[@ChargeCategory='"
            + GCConstants.SHIPPING + "']");
    for (int iCounter = 0; iCounter < nlLineCharge.getLength(); iCounter++) {
      Element eleLineCharge = (Element) nlLineCharge.item(iCounter);
      Float fLineShippingCharge = Float.parseFloat(eleLineCharge
          .getAttribute(GCConstants.CHARGE_AMOUNT));
      fTotalShippingCharge = fTotalShippingCharge + fLineShippingCharge;
    }
    BigDecimal bg = new BigDecimal(fTotalShippingCharge);
    bg = bg.setScale(2, BigDecimal.ROUND_HALF_UP);
    String sTotalShippingCharge = bg.toString();
    LOGGER.debug("Class: GCATGPromoService Method : getExistingShippingCharge - Returns - Shipping charge :   "
        + sTotalShippingCharge);
    LOGGER.debug("Class: GCATGPromoService Method: getExistingShippingCharge END");
    return sTotalShippingCharge;
  }
  /*
   * Method processATGResponse(returnDoc, shippingMethod, changeOrderDoc,
   * inDoc, levelOfService) i/p: returnDoc � document required to be returned
   * by the service to send to UI : shippingMethod � shippingMethod for
   * selected leveloOfService : changeOrderDoc � document required to call
   * changeOrder API : inDoc � request doc sent to ATG � required to fetch
   * CouponCode of the order
   *
   * 1. Line level discount, shipping charges, promotion, POS & OMS code for
   * PLCC is received in ATG response. 2. If ShippingAmount is zero at Order
   * level (in response), it is considered as Ship to Store scenario and value
   * is fetched from Shipping Charge List coming in response for corresponding
   * shipping method. 3. Above received values are updated in ChangeOrder Doc
   * (phase-1 implementation). This part will be modified in phase-2
   * implementation for order amendment (i.e. Order status >=1100) to skip
   * update of changeOrder doc with response data other than below mentioned
   * attributes: Header Charge details with ChargeAmount =
   * ShippingChargeAmount, ChargeCategory=Shipping, ChargeName=ShippingCharge,
   * header level promotion id with Shipping promotion id 4. Remove
   * OverallTotals field from changeOrder input (Phase-1 implementation) 5.
   * Stamp ExtnIsShippingProrated so that updated shipping charge can be
   * prorated in beforeChangeOrderUE when changeOrder API is called (Phase-1
   * implementation) 6. Call changeOrder API(Phase-1 implementation)
   */

}
