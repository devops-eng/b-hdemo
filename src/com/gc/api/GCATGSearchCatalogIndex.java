/** 
 * Copyright � 2015, Expicient Inc.,  All rights reserved.
 * ###########################################################################################################################################################
*           OBJECTIVE: This class calls ATG to get JSON Response, Transforms it to XML and converts the XML into sterling Format
*#############################################################################################################################################################
*           Version            Date              Modified By                Description
*#############################################################################################################################################################
             1.0               12/04/2016        Chirag                     JIRA No: Description of issue.
*#############################################################################################################################################################
 * For questions contact ally@expicient.com
 */

package com.gc.api;

import java.io.IOException;
import java.util.Properties;

import net.sf.json.JSON;
import net.sf.json.JSONSerializer;
import net.sf.json.xml.XMLSerializer;

import org.apache.http.client.ClientProtocolException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

//import urn.ebay.apis.eBLBaseComponents.IncentiveItemType;




import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.utils.GCATGUtil;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;
/*import com.gc.common.utils.GCRestWebServiceUtil;*/
import com.gc.common.utils.GCWebServiceRestUtil;

/**
 * @author <a href="ally@expicient.com">Expicient</a>
 */

public class GCATGSearchCatalogIndex implements YIFCustomApi {

	private static YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCATGSearchCatalogIndex.class);

	public Document getSearchCatalogIndex(YFSEnvironment env,
			Document docCatSearch) throws GCException {

		LOGGER.verbose("GCATGSearchCatalogIndex.getSearchCatalogIndex() -- Start");
		LOGGER.debug("Catalog search Front End Input"
				+ GCXMLUtil.getXMLString(docCatSearch));
		Document docOutputXML = null;
		try {
			YFCDocument inDoc = YFCDocument.getDocumentFor(docCatSearch);
			YFCElement eleTerms = inDoc.getElementsByTagName("Terms").item(0);
			if(YFCCommon.isVoid(eleTerms)){
				LOGGER.debug("Terms element hasn't present. Revert back with dummy output");
				
				YFCElement eleIndocRoot = inDoc.getDocumentElement();
				String sPageSize = eleIndocRoot.getAttribute("PageSize");
				String sPageNumber = eleIndocRoot.getAttribute("PageNumber");
				
				YFCDocument responseDoc = YFCDocument.getDocumentFor("<CatalogSearch/>");
				YFCElement eleRoot = responseDoc.getDocumentElement();
				eleRoot.setAttribute("CallingOrganizationCode", "GC");
				eleRoot.setAttribute("PageNumber", sPageNumber);
				eleRoot.setAttribute("PageSize", "18");
				eleRoot.setAttribute("TotalHits", "0");
				eleRoot.setAttribute("TotalPages", "0");
				
				YFCElement eleFacetList = responseDoc.createElement("FacetList");
				eleRoot.appendChild(eleFacetList);
				YFCElement eleItemList = responseDoc.createElement("ItemList");
				eleItemList.setAttribute("CallingOrganizationCode", "GC");
				eleItemList.setAttribute("TotalHits", "0");
				eleRoot.appendChild(eleItemList);
				
				LOGGER.debug("Output sent to front end is:-" + GCXMLUtil.getXMLString(responseDoc.getDocument()));
				
				return responseDoc.getDocument();
			}

			Element eleCatSearchRoot = GCXMLUtil.getRootElement(docCatSearch);
			
			//MPOS-1222: ATG call for getting dimID on PLP page: Start.		
			String sSrc = "";
			String sStoreID = "";
			String sDimIdReceived="";
			Document docMPOSSrcAndDimID = null;
			
			sSrc = eleCatSearchRoot.getAttribute("Src");
			if(YFCCommon.equalsIgnoreCase(sSrc, GCConstants.MPOS))
			{
				sStoreID = eleCatSearchRoot.getAttribute("StoreId");
				if(!YFCCommon.isVoid(sStoreID))
				{
					
					LOGGER.verbose("Inside first ATG call for mPOS orders");
					//Get URL from Customer_overrides.properties.
					final String sATGUrl = YFSSystem.getProperty(GCConstants.GC_ATG_GETSTOREDIMID_URL);
					String sModifiedUrl = sATGUrl + "storeId=" + sStoreID;
					LOGGER.verbose("Calling ATG Rest webservice for mPOS orders to get dimID");
					// Call ATG Rest WebService
					Document docATGDimIDResponse = getATGResponseXML(sModifiedUrl);
					LOGGER.verbose("Document received from getDimIDForStore url-->" + GCXMLUtil.getXMLString(docATGDimIDResponse));
					YFCDocument yfcdoc = YFCDocument.getDocumentFor(docATGDimIDResponse);
					YFCElement eleRoot = yfcdoc.getDocumentElement();
					YFCElement eleDimID = eleRoot.getElementsByTagName("dimId").item(0);
					sDimIdReceived = eleDimID.getNodeValue();
					LOGGER.verbose("DimID-->" + sDimIdReceived);
					docMPOSSrcAndDimID = GCXMLUtil.createDocument("Dim");
					docMPOSSrcAndDimID.getDocumentElement().setAttribute("Src", sSrc);
					docMPOSSrcAndDimID.getDocumentElement().setAttribute("DimID", sDimIdReceived);
					docMPOSSrcAndDimID.getDocumentElement().setAttribute("StoreId", sStoreID);
					
					//Appending this Dim doc to catalogSearch root
					Element eleImport = (Element) docCatSearch.importNode(docMPOSSrcAndDimID.getDocumentElement(), false);
					eleCatSearchRoot.appendChild(eleImport);
					LOGGER.verbose("CatalogSearch input document modified to-->" + GCXMLUtil.getXMLString(docCatSearch));
				}
			}
			//MPOS-1222: ATG call for getting dimID on PLP page: End.
			
			// URL Modification
			String sATGInpUrl = modifyURLForCatalogSearch(eleCatSearchRoot);

			// Call ATG Rest WebService
			Document docGetATGXml = getATGResponseXML(sATGInpUrl);
			
			// Modify ATG output XML
			modifyATGOutputXML(eleCatSearchRoot, docGetATGXml);

			// Call XSLT service
			docOutputXML = GCCommonUtil.invokeService(env,
					GCConstants.SERVICE_TRANSFORM_ATG_RESPONSE, docGetATGXml);

			// Modify XSL output
			modifyXSLOutput(docOutputXML);
			
			//mPOS-1222: Stamping DimID at root level for MPOS record: Start:
			if(YFCCommon.equalsIgnoreCase(sSrc, GCConstants.MPOS) && !YFCCommon.isVoid(sStoreID))
			{
				LOGGER.verbose("Stamping DimID at root level if calue of DimID is received from ATG");
				docOutputXML.getDocumentElement().setAttribute("DimID", sDimIdReceived);
			}
			//mPOS-1222: Stamping DimID at root level for MPOS record: End:
			
		} catch (ClientProtocolException e) {
			LOGGER.error("Error caught at GCATGSearchCatalogIndex.getSearchCatalogIndex()",e);
			e.printStackTrace();
		} catch (IOException e) {
			LOGGER.error("Error caught at GCATGSearchCatalogIndex.getSearchCatalogIndex()",e);
			e.printStackTrace();
		}
	
		LOGGER.verbose("GCATGSearchCatalogIndex.getSearchCatalogIndex() -- End");
		return docOutputXML;
	}

	/**
	 * @param eleCatSearchRoot
	 * @param docGetATGXml
	 */
	private void modifyATGOutputXML(Element eleCatSearchRoot,
			Document docGetATGXml) {
		
		LOGGER.verbose("GCATGSearchCatalogIndex.modifyATGOutputXML() -- Start");
		
		Element eleAtgXMLRoot = GCXMLUtil.getRootElement(docGetATGXml);
		
		//Get Calling Organization Code from Input Document's root element CatalogSearch
		String sCOC = GCXMLUtil.getAttribute(eleCatSearchRoot,
				GCConstants.CALLING_ORGANIZATION_CODE);
		
		// Get PageNo from Input Document's root element CatalogSearch
		String sPageNo = GCXMLUtil.getAttribute(eleCatSearchRoot,
				GCConstants.PAGE_NO);
		// Set COC in ATG output XML
		eleAtgXMLRoot.setAttribute(GCConstants.CALLING_ORGANIZATION_CODE,
				sCOC);
		//Set PageNo in ATG Output XML
		eleAtgXMLRoot.setAttribute(GCConstants.PAGE_NO, sPageNo);
		
		LOGGER.verbose("GCATGSearchCatalogIndex.modifyATGOutputXML() -- End");

	}

	/**
	 * Description of modifyURLForCatalogSearch
	 * 
	 * @param eleCatSearchRoot
	 * @return
	 **/
	public String modifyURLForCatalogSearch(Element eleCatSearchRoot) {

		LOGGER.verbose("GCATGSearchCatalogIndex.modifyURLForCatalogSearch() -- Start");
		LOGGER.verbose("Input to GCATGSearchCatalogIndex.modifyURLForCatalogSearch()" + GCXMLUtil.getElementXMLString(eleCatSearchRoot));
		
		//Get URL from Customer_overrides.properties
		final String sURL = YFSSystem
			.getProperty(GCConstants.GC_ATG_SEARCHINDEX_URL);
		
		
		String sATGInpUrl = sURL;// Make one String variable
		LOGGER.verbose("sATGInputUrl -->" + sATGInpUrl);
		
		NodeList nlTerm = eleCatSearchRoot
				.getElementsByTagName(GCConstants.TERM);

		int iLengthNlTerm = nlTerm.getLength();
		
		//mPOS-1222: for mPOS order if dimID is sent in input then will be appended to the url: Start.
		String sDimID = "";
		String sSrc = "";
		String sStoreID = "";
		Element eleDim = (Element) eleCatSearchRoot.getElementsByTagName("Dim").item(0);
		if(!YFCCommon.isVoid(eleDim))
		{
			LOGGER.verbose("mPOS order with dimID presnet in element Dim");
			sDimID = eleDim.getAttribute("DimID");
			sSrc = eleDim.getAttribute("Src");
			sStoreID = eleDim.getAttribute("StoreId");
			LOGGER.verbose("DimId received = " + sDimID + " and Src is = " + sSrc + " and StoreId is = " + sStoreID);
		}
		//mPOS-1222: for mPOS order if dimID is sent in input then will be appended to the url: End.

		// Looping Over Term
		for (int indexOfTermEle = 0; indexOfTermEle < iLengthNlTerm; indexOfTermEle++) {

			Element eleTerm = (Element) nlTerm.item(indexOfTermEle);
			LOGGER.verbose("GCATGSearchCatalogIndex.modifyURLForCatalogSearch() For Loop" + GCXMLUtil.getElementXMLString(eleTerm));
			// Get Condition Attribute
			String sSearchCondition = GCXMLUtil.getAttribute(eleTerm,
					GCConstants.CONDITION);
			// Get Value Attribute
			String sConditionVal = GCXMLUtil.getAttribute(eleTerm,
					GCConstants.VALUE);

			// Get URL Input for ATG Rest Call
			sATGInpUrl = GCATGUtil.getATGUrlForCatalogSearch(sATGInpUrl,
					sSearchCondition, sConditionVal);
			
			//mPOS-1222: for mPOS orders if Filter "N" is already appended to url then appending DimID after that filter: Start.
			if(YFCCommon.equalsIgnoreCase(sSrc, GCConstants.MPOS) && !YFCCommon.isVoid(sStoreID))
			{
				if(YFCCommon.equalsIgnoreCase(sSearchCondition, "N"))
				{
					LOGGER.verbose("mPOS order with filter already present in the input");
					sATGInpUrl = sATGInpUrl + "+" + sDimID;
					LOGGER.verbose("Appending DimID received from ATG after filter value-->" + sATGInpUrl);
				}
				
				else if(indexOfTermEle == (iLengthNlTerm-1))
				{
					LOGGER.verbose("mPOS order with no filter present in the input");
					sATGInpUrl = sATGInpUrl + "&N=" + sDimID;
					LOGGER.verbose("Appending DimID received from ATG at the end of url-->" + sATGInpUrl);
				}
			}
			//mPOS-1222: for mPOS orders if Filter "N" is already appended to url then appending DimID after that filter: End.

		}
		LOGGER.verbose("Output from GCATGSearchCatalogIndex.modifyURLForCatalogSearch()" + sATGInpUrl);
		LOGGER.verbose("GCATGSearchCatalogIndex.modifyURLForCatalogSearch() -- End");
		return sATGInpUrl;
	}
	
	/**
	 * Description of getATGResponseXML
	 * 
	 * @param sATGInpUrl
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 **/
	private Document getATGResponseXML(String sATGInpUrl)
			throws ClientProtocolException, IOException {

		
		String sJSONResponse = GCWebServiceRestUtil
				.restClientService(sATGInpUrl);
		JSON json = JSONSerializer.toJSON(sJSONResponse);
		XMLSerializer serializer = new XMLSerializer();
		String sXMLResponse = serializer.write(json);

		Document docGetATGXml = GCXMLUtil.getDocument(sXMLResponse);
		return docGetATGXml;
	}

	/**
	 * Description of setPrimaryInfoAttributes
	 * 
	 * @param docOutputXML
	 * @param eleOutItem
	 * @throws DOMException
	 **/
	public void modifyXSLOutput(Document docOutputXML)
			throws DOMException {

		LOGGER.verbose("GCATGSearchCatalogIndex.setPrimaryInfoAttributes() -- Start");
		LOGGER.verbose("Input to GCATGSearchCatalogIndex.setPrimaryInfoAttributes()" + GCXMLUtil.getXMLString(docOutputXML));
		Element eleOutXMLRoot = GCXMLUtil.getRootElement(docOutputXML);
		NodeList nlOutItem = eleOutXMLRoot
				.getElementsByTagName(GCConstants.ITEM);

		
		int iLenItem = nlOutItem.getLength();
		
		// If only 1 Item is fetched, set ItemId=Child skuId
		if(iLenItem==1){
			LOGGER.verbose("GCATGSearchCatalogIndex.setPrimaryInfoAttributes(): Inside If loop with one item");
			setItemIdIfOneItemIsFetched(nlOutItem);
			eleOutXMLRoot.setAttribute(GCConstants.TOTAL_HITS, "1");
			eleOutXMLRoot.setAttribute(GCConstants.TOTAL_PAGES, "1");
		}

		for (int iItemIndex = 0; iItemIndex < iLenItem; iItemIndex++) {

			Element eleOutItem = (Element) nlOutItem.item(iItemIndex);
			LOGGER.verbose("GCATGSearchCatalogIndex.setPrimaryInfoAttributes(): Inside for loop");
			LOGGER.verbose("Item [" + iItemIndex +"] :"+ GCXMLUtil.getElementXMLString(eleOutItem));
			Element elePrimaryInformation = docOutputXML.createElement(GCConstants.PRIMARY_INFORMATION);
			eleOutItem.appendChild(elePrimaryInformation);
			// Set ImageId and ImageLocation from product image URL
			String sProductUrl = GCXMLUtil.getAttribute(eleOutItem,
					GCConstants.PRODUCT_IMAGE_URL);
			// Extract ImageId and ImageLocation from imageURL
			if(!XmlUtils.isVoid(sProductUrl))
			{
				String sImageId = sProductUrl.substring(sProductUrl
						.lastIndexOf('/') + 1);
				String sImageLocation = sProductUrl.substring(0,
						sProductUrl.lastIndexOf('/'));
				GCXMLUtil.setAttribute(elePrimaryInformation, GCConstants.IMAGE_ID,
						sImageId);
				GCXMLUtil.setAttribute(elePrimaryInformation, GCConstants.IMAGE_LOCATION,
						sImageLocation);
			}
			//get Low price
			String sLowPrice = GCXMLUtil.getAttribute(eleOutItem,GCConstants.LOW_PRICE);
			if(!XmlUtils.isVoid(sLowPrice))
			{
				Element eleComputedPrice = docOutputXML.createElement(GCConstants.COMPUTED_PRICE);
				eleComputedPrice.setAttribute(GCConstants.UNIT_PRICE, sLowPrice);
				eleOutItem.appendChild(eleComputedPrice);
			}
			//get product Name
			String sProductName = GCXMLUtil.getAttribute(eleOutItem, GCConstants.PRODUCT_NAME);
			if(!XmlUtils.isVoid(sProductName))
			{
				GCXMLUtil.setAttribute(elePrimaryInformation, GCConstants.EXTENDED_DISPLAY_DESCRIPTION, sProductName);
			}
		}
		
		// GCXMLUtil.appendChild(eleOutItem, elePrimaryInfo);
		LOGGER.verbose("GCATGSearchCatalogIndex.setPrimaryInfoAttributes() -- End");
	}  

	/**
	 * @param nlOutItem
	 */
	private void setItemIdIfOneItemIsFetched(NodeList nlOutItem) {
		LOGGER.verbose("GCATGSearchCatalogIndex.setItemIdIfOneItemIsFetched() -- Start");
		Element eleOutItem = (Element) nlOutItem.item(0);
		NodeList nlChildSku = eleOutItem
				.getElementsByTagName(GCConstants.CHILD_SKU);
		Element eleChildSku = (Element) nlChildSku.item(0);
		String sSkuId=eleChildSku.getAttribute(GCConstants.SKU_ENT_ID);
		String sSkuSubStrId = sSkuId.substring(sSkuId.length() - 3);
		if(XmlUtils.equals(sSkuSubStrId, "_CL"))
		{
			sSkuId = sSkuId.substring(0, sSkuId.length() - 3);
		}
		GCXMLUtil.setAttribute(eleOutItem, GCConstants.ITEM_ID, sSkuId);
		GCXMLUtil.setAttribute(eleOutItem, GCConstants.UNIT_OF_MEASURE, GCConstants.EACH);

		LOGGER.verbose("GCATGSearchCatalogIndex.setItemIdIfOneItemIsFetched() -- End");
	}

	@Override
	public void setProperties(Properties arg0) throws Exception {
		// TODO Auto-generated method stub

	}

}
