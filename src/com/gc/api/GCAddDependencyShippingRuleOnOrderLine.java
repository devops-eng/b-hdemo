/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0                         02/09/2014          Naveen, Gowda		JIRA No: This class used to add Dependency Shipping Rule
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:email Address">Name</a>
 */
public class GCAddDependencyShippingRuleOnOrderLine implements YIFCustomApi {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCAddDependencyShippingRuleOnOrderLine.class);

    /**
     * 
     * Description of addShippingRule
     * 
     * @param env
     * @param inDoc
     * @throws Exception
     * 
     */
    public void addShippingRule(YFSEnvironment env, Document inDoc)
	    throws GCException {
	try {

	    LOGGER.verbose("Class: GCAddDependencyShippingRuleOnOrderLine Method: addShippingRule BEGIN");
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug(" addShippingRule() method, Input document is"
			+ GCXMLUtil.getXMLString(inDoc));
	    }
	    String sOrderHeaderKey = GCXMLUtil.getAttributeFromXPath(inDoc,
		    "//@OrderHeaderKey");
	    NodeList nlOrderLine = GCXMLUtil.getNodeListByXpath(inDoc,
		    "Order/OrderLines/OrderLine[@DependentOnLineKey!='']");
	    LOGGER.debug("Lenght nodelist" + nlOrderLine.getLength());
	    Document docCOIn = GCXMLUtil.createDocument(GCConstants.ORDER);
	    Element eleRoot = docCOIn.getDocumentElement();
	    eleRoot.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
	    Element eleCOOrderLines = docCOIn
		    .createElement(GCConstants.ORDER_LINES);
	    for (int i = 0; i < nlOrderLine.getLength(); i++) {
		LOGGER.debug("*&^*&^Inside For)(&(*&(");
		Element eleOrderLine = (Element) nlOrderLine.item(i);
		String sDependentOnLineKey = eleOrderLine
			.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
		Element eleParentOrderLine = GCXMLUtil.getElementByXPath(inDoc,
			"//OrderLine[@OrderLineKey='" + sDependentOnLineKey
				+ "']");
		if (!YFCCommon.isVoid(eleParentOrderLine)) {
		    String sOrderLineKey = eleParentOrderLine
			    .getAttribute(GCConstants.ORDER_LINE_KEY);

		    Element eleCOOrderLine = docCOIn
			    .createElement(GCConstants.ORDER_LINE);
		    eleCOOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY,
			    sOrderLineKey);
		    eleCOOrderLine.setAttribute("DependencyShippingRule", "01");
		    eleCOOrderLines.appendChild(eleCOOrderLine);

		}
	    }
	    eleRoot.appendChild(eleCOOrderLines);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("addShippingRule Change Order Input"
			+ GCXMLUtil.getXMLString(docCOIn));
	    }
	    GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, docCOIn);

	    LOGGER.verbose("Class: GCAddDependencyShippingRuleOnOrderLine Method: addShippingRule END");
	} catch (Exception e) {
	    LOGGER.error("Exception is in addShippingRule", e);
	    throw new GCException(e);
	}
    }

    @Override
    public void setProperties(Properties arg0) {

    }
}
