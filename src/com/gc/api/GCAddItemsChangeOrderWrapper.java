package com.gc.api;

import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCAddItemsChangeOrderWrapper implements YIFCustomApi {

  public Document changeOrderOnAddItems(YFSEnvironment env, Document inDoc){
    GCCommonUtil.invokeAPI(env, "changeOrder", inDoc);
    String orderHeaderKey = inDoc.getDocumentElement().getAttribute("OrderHeaderKey");
    YFCDocument templateDoc = YFCDocument.getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/getOrderList_For_addItemsChangeOrder.xml"));
    YFCDocument getOrderListDoc = GCCommonUtil.invokeGetOrderListAPI(env, orderHeaderKey, templateDoc);
    YFCElement eleRoot = getOrderListDoc.getDocumentElement().getChildElement("Order");
    YFCDocument newDoc = YFCDocument.createDocument("Order");
    YFCElement elenewRoot = newDoc.getDocumentElement();
    GCXMLUtil.copyElement(newDoc, eleRoot, elenewRoot);

    YFCDocument newChangeOrderDoc = YFCDocument.createDocument("Order");
    YFCElement eleOrder = newChangeOrderDoc.getDocumentElement();
    eleOrder.setAttribute("OrderHeaderKey", orderHeaderKey);
    YFCElement elenewExtn = eleOrder.createChild("Extn");
    YFCElement eleGCUserActivityList = elenewExtn.createChild("GCUserActivityList");
    YFCNodeList<YFCElement> nlOrderLines = eleRoot.getElementsByTagName(GCConstants.ORDER_LINE);
    for(YFCElement eleOrderLine : nlOrderLines){
      YFCElement eleExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
      String sEmpId = eleExtn.getAttribute("ExtnOverridePriceEmpId");
      String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      String sOrderHeaderKey = getOrderListDoc.getDocumentElement().getAttribute(GCConstants.ORDER_HEADER_KEY);
      if(!YFCCommon.isVoid(sEmpId)){
        YFCDocument userActivityDoc = YFCDocument.createDocument("UserActivity");
        YFCElement eleUserActivity = userActivityDoc.getDocumentElement();
        eleUserActivity.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
        eleUserActivity.setAttribute(GCConstants.ORDER_LINE_KEY, sOrderLineKey);
        eleUserActivity.setAttribute(GCConstants.USER_ACTION, "PriceOverride");
        YFCDocument userActDoc = GCCommonUtil.invokeService(env,"GCFetchUserActivity",userActivityDoc);
        YFCElement eleDocElement = userActDoc.getDocumentElement();
        YFCElement eleGCUserActivity = eleDocElement.getChildElement("GCUserActivity");
        YFCElement elenewUserActivity = eleGCUserActivityList.createChild("GCUserActivity");
        if(YFCCommon.isVoid(eleGCUserActivity)){
          elenewUserActivity.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
          elenewUserActivity.setAttribute(GCConstants.ORDER_LINE_KEY, sOrderLineKey);
          elenewUserActivity.setAttribute(GCConstants.USER_ACTION, "PriceOverride");
        }else{
          elenewUserActivity.setAttribute("UserActivityKey", eleGCUserActivity.getAttribute("UserActivityKey"));
        }
        elenewUserActivity.setAttribute("UserID", sEmpId);
      }
    }
    if(eleGCUserActivityList.hasChildNodes()){
      GCCommonUtil.invokeAPI(env, "changeOrder", newChangeOrderDoc.getDocument());
    }
    return newDoc.getDocument();
  }

  @Override
  public void setProperties(Properties arg0) {

  }

}
