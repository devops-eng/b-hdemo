package com.gc.api;

import java.util.Properties;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.gc.eventhandler.GCProcessOrderOnSuccess;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
/**
 * @author <a href="mailto:rakesh.kumar@expicient.com">Rakesh</a>
 */
public class GCAddNotesToOrder implements YIFCustomApi{

  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCProcessOrderOnSuccess.class);

  private String noteText = "";

  /**
   *
   * Description of callChangeOrder This method calls ChangeOrderAPI for adding notes to the order.
   * For making change order input Xml it uses inDoc.
   *
   * @param env
   * @param inDoc
   * @return
   * @throws GCException
   * @throws TransformerException
   *
   */
  public Document callChangeOrder(YFSEnvironment env, Document inDoc) throws TransformerException {
    LOGGER.info("Enter Inside GCCallChangeOrder.callChangeOrder()");
    Document changeOrderInDoc = GCXMLUtil.getDocument("<Order OrderHeaderKey=''> "
        +"<Notes> <Note NoteText=''></Note>"
        +"</Notes></Order>");


    Element eleOrder=(Element)XPathAPI.selectSingleNode(inDoc,"OrderInvoice");
    if (YFCCommon.isVoid(eleOrder)) {
      eleOrder = (Element) XPathAPI.selectSingleNode(inDoc, "Order");
    }
    String orderHeaderKey=eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
    Element eleOrderChangeOrderIP=changeOrderInDoc.getDocumentElement();
    eleOrderChangeOrderIP.setAttribute(GCConstants.ORDER_HEADER_KEY,orderHeaderKey);
    Element eleNote=(Element)XPathAPI.selectSingleNode(eleOrderChangeOrderIP,"Notes/Note");
    eleNote.setAttribute("NoteText", noteText);// GCConstants.GC_SHIPMENT_CONFIRMATION_EMAIL_NOTE
	
    // fix for 4037 : Cancellation email note should not be stamped on the order if orderline is cancelled when order is in DraftOrderStatus
    String sDraftOrderFlag = eleOrder.getAttribute(GCConstants.DRAFT_ORDER_FLAG);
    
    if(YFCCommon.equals(sDraftOrderFlag, GCConstants.YES)){
    	eleOrderChangeOrderIP.setAttribute("DoNotSendCancellationEMail", "Y");
    	LOGGER.verbose("Value set as a Y");
    } else {
	//Fix for Defect-2988
	Object obCancellationFlag=env.getTxnObject("DoNotSendCancellationEMail");
    String sCancellationFlag="";
    if(!YFCCommon.isVoid(obCancellationFlag)){
    	sCancellationFlag = (String)obCancellationFlag;
    }
    LOGGER.verbose("sCancellationFlag " + sCancellationFlag);
    if(YFCCommon.equalsIgnoreCase(sCancellationFlag, "Y")){
    	eleOrderChangeOrderIP.setAttribute("DoNotSendCancellationEMail", "Y");
    	LOGGER.verbose("Value set as a " + sCancellationFlag);
    }else{
    	eleOrderChangeOrderIP.setAttribute("DoNotSendCancellationEMail", "N");
    	LOGGER.verbose("Value set as a " + sCancellationFlag);
    }
    }
	//Defect-2988 Ends Here
    return changeOrderInDoc;
  }

  @Override
  public void setProperties(Properties prop) {
    noteText = prop.getProperty(GCConstants.NOTE_TEXT);

  }

}
