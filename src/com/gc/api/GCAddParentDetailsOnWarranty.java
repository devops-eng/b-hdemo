package com.gc.api;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCAddParentDetailsOnWarranty {
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCAddParentDetailsOnWarranty.class);

  public Document addparentDetailsOnWarranty(YFSEnvironment env, Document inDoc) {
    LOGGER.verbose("Entering the addparentDetailsOnWarranty method");
    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement yfcEleRootInDoc = yfcInDoc.getDocumentElement();
    boolean bIfOnlyDependentLines = true;

    YFCElement yfcEleInvoiceHeader = yfcEleRootInDoc.getElementsByTagName(GCConstants.INVOICE_HEADER).item(0);
    String docType = yfcEleInvoiceHeader.getChildElement("Order").getAttribute("DocumentType");

    // Fix for GCSTORE-6333 - Starts - Stamping ExtnTargetSystem

    YFCDocument changeOrderInvoice = YFCDocument.createDocument(GCConstants.ORDER_INVOICE);
    YFCElement eleOrderInvoice = changeOrderInvoice.getDocumentElement();
    eleOrderInvoice.setAttribute(GCConstants.ORDER_INVOICE_KEY, yfcEleInvoiceHeader.getAttribute("OrderInvoiceKey"));
    YFCElement eleExtn1 = eleOrderInvoice.createChild(GCConstants.EXTN);
    eleExtn1.setAttribute("ExtnTargetSystem", "DAX");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("ChangeOrderInvoice Input :" + changeOrderInvoice.getString());
    }
    // Invoking the service to call changeOrderInvoice api.
    GCCommonUtil.invokeService(env, "GCAppeasementIntegService", changeOrderInvoice);

    // Fix for GCSTORE-6333 - Ends - Stamping ExtnTargetSystem

    YFCElement yfcEleLineDetails = yfcEleInvoiceHeader.getElementsByTagName(GCConstants.LINE_DETAILS).item(0);
    YFCNodeList<YFCElement> yfcNLOrderLine = yfcEleLineDetails.getElementsByTagName(GCConstants.ORDER_LINE);
    if (YFCCommon.equals(docType, "0001")) {
      for (int k = 0; k < yfcNLOrderLine.getLength(); k++) {
        YFCElement yfcEleOrderLine = yfcNLOrderLine.item(k);

        // Check condition if only warranty lines are processed
        if (bIfOnlyDependentLines) {
          String sDependentLineKey = yfcEleOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
          if (YFCCommon.isStringVoid(sDependentLineKey)) {
            bIfOnlyDependentLines = false;
          } else {
            onlyDependentLinesInvoiced(env, yfcEleRootInDoc, yfcEleOrderLine, sDependentLineKey);
          }
        }
      }
    } else if (YFCCommon.equals(docType, "0003")) {

      YFCNodeList<YFCElement> nlLineDetails = yfcEleInvoiceHeader.getElementsByTagName("LineDetail");
      String sOrderNo = yfcEleInvoiceHeader.getAttribute("OrderNo");
      YFCDocument yfcDocGetOrderListInput = YFCDocument.createDocument(GCConstants.ORDER);

      yfcDocGetOrderListInput.getDocumentElement().setAttribute(GCConstants.ORDER_NO, sOrderNo);

      Document docGetOrderListOutput =
          GCCommonUtil.invokeAPI(env, yfcDocGetOrderListInput.getDocument(), GCConstants.GET_ORDER_LIST,
              "global/template/api/getOrderListTempForPOSInvoice.xml");
      YFCNodeList<YFCElement> nlOrderLines =
          YFCDocument.getDocumentFor(docGetOrderListOutput).getElementsByTagName("OrderLine");
      String sSOOrderHeaderKey =
          YFCDocument.getDocumentFor(docGetOrderListOutput).getElementsByTagName("OrderLine").item(0)
          .getAttribute("DerivedFromOrderHeaderKey");
      YFCDocument getSOinDoc = YFCDocument.createDocument("Order");
      getSOinDoc.getDocumentElement().setAttribute("OrderHeaderKey", sSOOrderHeaderKey);
      Document docGetSOOrderList =
          GCCommonUtil.invokeAPI(env, getSOinDoc.getDocument(), GCConstants.GET_ORDER_LIST,
              "global/template/api/getOrderListTempForPOSInvoice.xml");
      Element eleSORoot = docGetSOOrderList.getDocumentElement();
      Element eleSOOrder = (Element) eleSORoot.getElementsByTagName("Order").item(0);
      String sSOOrderNo = eleSOOrder.getAttribute("OrderNo");
      String sOrderDate = eleSOOrder.getAttribute("OrderDate");
      YFCNodeList<YFCElement> nlSOOrderLines =
          YFCDocument.getDocumentFor(docGetSOOrderList).getElementsByTagName("OrderLine");
      for (YFCElement eleLineDetail : nlLineDetails) {
        String sItemID = null;
        String sNetPrice = null;
        YFCElement eleOrderLine = eleLineDetail.getChildElement("OrderLine");
        String sOrderLineKey = eleOrderLine.getAttribute("OrderLineKey");
        for (YFCElement eleROrderLine : nlOrderLines) {
          String sROrderLineKey = eleROrderLine.getAttribute("OrderLineKey");
          if (YFCCommon.equals(sROrderLineKey, sOrderLineKey)) {
            String sDOLK = eleROrderLine.getAttribute("DerivedFromOrderLineKey");
            for (YFCElement eleSOOrderLine : nlSOOrderLines) {
              String sSOOrderLineKey = eleSOOrderLine.getAttribute("OrderLineKey");
              if (YFCCommon.equals(sSOOrderLineKey, sDOLK)) {
                String sDepOLK = eleSOOrderLine.getAttribute("DependentOnLineKey");
                if (!YFCCommon.isVoid(sDepOLK)) {
                  Element eleDepOrderLine =
                      GCXMLUtil.getElementByXPath(docGetSOOrderList,
                          "OrderList/Order/OrderLines/OrderLine[@OrderLineKey='" + sDepOLK + "']");
                  Element eleItem = (Element) eleDepOrderLine.getElementsByTagName(GCConstants.ITEM).item(0);
                  // String productCLass =
                  // eleItem.getAttribute(GCConstants.PRODUCT_CLASS);
                  sItemID = eleItem.getAttribute("ItemID");
                  Element eleLineOverallTotals =
                      (Element) eleDepOrderLine.getElementsByTagName("LineOverallTotals").item(0);
                  String sUnitPrice = eleLineOverallTotals.getAttribute("UnitPrice");
                  double dUnitPrice = Double.parseDouble(sUnitPrice);
                  NodeList nlLineCharges = eleDepOrderLine.getElementsByTagName("LineCharge");
                  int len = nlLineCharges.getLength();
                  double dDiscount = 0;
                  for (int p = 0; p < len; p++) {
                    Element eleLineCharge = (Element) nlLineCharges.item(p);
                    String sChargeCategory = eleLineCharge.getAttribute("ChargeCategory");
                    String sIsDiscount = eleLineCharge.getAttribute("IsDiscount");
                    String sQty = eleDepOrderLine.getAttribute("OrderedQty");
                    if (YFCCommon.equals(sIsDiscount, "Y") && YFCCommon.equals(sChargeCategory, "Promotion")) {
                      String sChargeAmount = eleLineCharge.getAttribute("ChargeAmount");
                      double dChargeAmount = Double.parseDouble(sChargeAmount);
                      double dQty = Double.parseDouble(sQty);
                      dDiscount = dDiscount + (dChargeAmount / dQty);
                    }
                  }
                  double dNetPrice = dUnitPrice - dDiscount;
                  sNetPrice = String.valueOf(dNetPrice);
                }
              }
            }
          }
        }
        YFCElement eleOrder = yfcEleInvoiceHeader.getChildElement("LineDetails");
        YFCNodeList<YFCElement> nlOls = eleOrder.getElementsByTagName("OrderLine");
        for (YFCElement eleOl : nlOls) {
          String sOLK = eleOl.getAttribute("OrderLineKey");
          if (YFCCommon.equals(sOLK, sOrderLineKey)) {

            YFCElement eleExtn = eleOl.getChildElement("Extn", true);
            eleExtn.setAttribute("ExtnParentItemID", sItemID);
            eleExtn.setAttribute("ExtnParentLineNetUnitPrice", sNetPrice);
            eleExtn.setAttribute("ExtnSalesOrderNo", sSOOrderNo);
            eleExtn.setAttribute("ExtnOrderDate", sOrderDate);
          }
        }
      }
    }
    return yfcInDoc.getDocument();
  }

  private void onlyDependentLinesInvoiced(YFSEnvironment env, YFCElement yfcEleRootInDoc, YFCElement yfcEleOrderLine,
      String sDependentLineKey) {
    LOGGER.beginTimer("GCAddParentDetailsOnWarranty.onlyDependentLinesInvoiced()");
    LOGGER.verbose("GCAddParentDetailsOnWarranty - onlyDependentLinesInvoiced() : Start");
    YFCElement yfcEleOrder = yfcEleRootInDoc.getElementsByTagName(GCConstants.ORDER).item(0);
    Document docGetOrderListOutput = null;
    if (YFCCommon.isVoid(docGetOrderListOutput)) {
      // Invoke get order list API
      YFCDocument yfcDocGetOrderListInput = YFCDocument.createDocument(GCConstants.ORDER);

      yfcDocGetOrderListInput.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY,
          yfcEleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY));

      docGetOrderListOutput =
          GCCommonUtil.invokeAPI(env, yfcDocGetOrderListInput.getDocument(), GCConstants.GET_ORDER_LIST,
              "global/template/api/getOrderListTempForPOSInvoice.xml");
    }
    Element eleOrderLine =
        GCXMLUtil.getElementByXPath(docGetOrderListOutput, "OrderList/Order/OrderLines/OrderLine[@OrderLineKey='"
            + sDependentLineKey + "']");
    // Element eleOrderLineExtn = (Element)
    // eleOrderLine.getElementsByTagName(GCConstants.EXTN).item(0);
    Element eleItem = (Element) eleOrderLine.getElementsByTagName(GCConstants.ITEM).item(0);
    // String productCLass =
    // eleItem.getAttribute(GCConstants.PRODUCT_CLASS);
    String sItemID = eleItem.getAttribute("ItemID");
    Element eleLineOverallTotals = (Element) eleOrderLine.getElementsByTagName("LineOverallTotals").item(0);
    String sLineTotal = eleLineOverallTotals.getAttribute("UnitPrice");
    double dUnitPrice = Double.parseDouble(sLineTotal);
    NodeList nlLineCharges = eleOrderLine.getElementsByTagName("LineCharge");
    int len = nlLineCharges.getLength();
    double dDiscount = 0;
    for (int p = 0; p < len; p++) {
      Element eleLineCharge = (Element) nlLineCharges.item(p);
      String sChargeCategory = eleLineCharge.getAttribute("ChargeCategory");
      String sIsDiscount = eleLineCharge.getAttribute("IsDiscount");
      String sQty = eleOrderLine.getAttribute("OrderedQty");
      if (YFCCommon.equals(sIsDiscount, "Y") && YFCCommon.equals(sChargeCategory, "Promotion")) {
        String sChargeAmount = eleLineCharge.getAttribute("ChargeAmount");
        double dChargeAmount = Double.parseDouble(sChargeAmount);
        double dQty = Double.parseDouble(sQty);
        dDiscount = dDiscount + (dChargeAmount / dQty);
      }
    }
    double netUnitPrice = dUnitPrice - dDiscount;
    YFCNodeList<YFCElement> nlOrderLine = yfcEleOrder.getElementsByTagName("OrderLine");
    for (YFCElement yfceleOrderLine : nlOrderLine) {
      String sOrderLineKey = yfceleOrderLine.getAttribute("OrderLineKey");
      if (YFCCommon.equals(sOrderLineKey, sDependentLineKey)) {
        YFCElement eleExtn = yfcEleOrderLine.getChildElement("Extn", true);
        eleExtn.setAttribute("ExtnParentItemID", sItemID);
        eleExtn.setAttribute("ExtnParentLineNetUnitPrice", netUnitPrice);
      }
    }

    LOGGER.endTimer("GCAddParentDetailsOnWarranty.onlyDependentLinesInvoiced()");
    LOGGER.verbose("GCAddParentDetailsOnWarranty - onlyDependentLinesInvoiced() : End");

  }

}
