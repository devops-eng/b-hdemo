/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: This class is used to copy commission from SO to RO and add warranty items too if not added by CSR.
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0               10/05/2014        Gowda, Naveen      JIRA No:
             1.1			   27/01/2015        Soni, Karan        OMS:4723- Warranties for other items are being included on returns
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:email Address">Naveen</a>
 */
public class GCAddWarrantyLineToRO implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCAddWarrantyLineToRO.class);

  private Date dCurrentDate;
  private String sReturnWindow;

  /**
   *
   * Description of gcAddWarrantyLineToRO
   *
   * @param env
   * @param inDoc
   * @throws Exception
   * @throws TransformerException
   *
   */
  public void gcAddWarrantyLineToRO(YFSEnvironment env, Document inDoc) throws GCException {

    if(GCConstants.YES.equals(env.getTxnObject(GCConstants.ADD_WARRANTY_LINE_TO_RO))){
      env.setTxnObject(GCConstants.ADD_WARRANTY_LINE_TO_RO, GCConstants.NO);

      try {
        LOGGER.verbose("Class: GCAddWarrantyLineToRO Method: gcAddWarrantyLineToRO BEGIN");
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("gcAddWarrantyLineToRO() - Incoming Document" + GCXMLUtil.getXMLString(inDoc));
        }
        Document docUpdateOrderPrice = inDoc;
        NodeList nlOrderLine = GCXMLUtil.getNodeListByXpath(inDoc, "Order/OrderLines/OrderLine");
        if (nlOrderLine.getLength()>0) {
          String sOrderHeaderKey = GCXMLUtil.getAttributeFromXPath(inDoc, "//@OrderHeaderKey");
          String sDerivedOrdrHdrKey = GCXMLUtil.getAttributeFromXPath(inDoc, "//OrderLine/@DerivedFromOrderHeaderKey");
          Document docGetOrderDetailsIn = GCXMLUtil.createDocument(GCConstants.ORDER);
          Element eleRootGetOrdIn = docGetOrderDetailsIn.getDocumentElement();
          eleRootGetOrdIn.setAttribute(GCConstants.ORDER_HEADER_KEY, sDerivedOrdrHdrKey);

          Map<String, Element> mapCommission = new HashMap<String, Element>();
          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("gcAddWarrantyLineToRO() - Get Order Details Input Doc"
                + GCXMLUtil.getXMLString(docGetOrderDetailsIn));
          }
          Document docGetSalesOrderDetOut = null;
          if (!YFCCommon.isVoid(sDerivedOrdrHdrKey)) {
            docGetSalesOrderDetOut =
                GCCommonUtil.invokeAPI(env, docGetOrderDetailsIn, GCConstants.GET_ORDER_LIST,
                    "/global/template/api/getOrderListTempForAddWarrantyLineToRO.xml");
          }
          /* Call Return Order List */
          Document docRetOrderList = null;
          if (!YFCCommon.isVoid(sOrderHeaderKey)) {
            eleRootGetOrdIn.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
            docRetOrderList =
                GCCommonUtil
                .invokeAPI(env, docGetOrderDetailsIn,
                    GCConstants.GET_ORDER_LIST,
                    "/global/template/api/getOrderListTempForAddWarrantyLineToRO.xml");
          }
          Element eleOrder = GCXMLUtil.getElementByXPath(docGetSalesOrderDetOut, "/OrderList/Order");
          Document docEleOrder = GCXMLUtil.getDocumentFromElement(eleOrder);

          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("gcAddWarrantyLineToRO() - Get Order Details Output Doc" + GCXMLUtil.getXMLString(docEleOrder));
          }
          NodeList nLOrderLineSO = GCXMLUtil.getNodeListByXpath(docEleOrder, "/Order/OrderLines/OrderLine");
          Map<String, List<Element>> map = new HashMap<String, List<Element>>();
          List<Element> listOLWarrEle = new ArrayList<Element>();

          // Remember to check for Req Ship Date not greater than 60 days.
          for (int k = 0; k < nLOrderLineSO.getLength(); k++) {
            Element eleOrderLine = (Element) nLOrderLineSO.item(k);
            Document docEleOrderLine = GCXMLUtil.getDocumentFromElement(eleOrderLine);

            Element eleGCOLCommissionList = GCXMLUtil.getElementByXPath(docEleOrderLine, "//GCOrderLineCommissionList");
            LOGGER.debug("eleGCOLCommissionList element" + GCXMLUtil.getElementXMLString(eleGCOLCommissionList));
            String sPrimeLineNo = eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
            String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
            LOGGER.debug("sOrderLineKey::" + sOrderLineKey);
            if (!YFCCommon.isVoid(eleGCOLCommissionList)) {
              mapCommission.put(sPrimeLineNo, eleGCOLCommissionList);
            }

            NodeList nLOrderLineWarr =
                GCXMLUtil.getNodeListByXpath(docEleOrder, "/Order/OrderLines/OrderLine[@DependentOnLineKey='"
                    + sOrderLineKey + "' and @MaxLineStatus!='9000' and ItemDetails/PrimaryInformation[@ItemType='03']]");
            LOGGER.debug("nLOrderLineWarr Length::" + nLOrderLineWarr.getLength());
            //OMS-4723 Start
            List<Element> listOLWarrEleTemp = new ArrayList<Element>();
            for (int j = 0; j < nLOrderLineWarr.getLength(); j++) {
              Element eleOrderLineWarr = (Element) nLOrderLineWarr.item(j);
              listOLWarrEle.add(eleOrderLineWarr);
              listOLWarrEleTemp.add(eleOrderLineWarr);
              map.put(sOrderLineKey, listOLWarrEleTemp);
              //OMS-4723 End
            }

          }

          NodeList nLOrderLineRO = GCXMLUtil.getNodeListByXpath(inDoc, "/Order/OrderLines/OrderLine");
          Element eleOrderLinesRO = GCXMLUtil.getElementByXPath(inDoc, "/Order/OrderLines");
          for (int i = 0; i < nLOrderLineRO.getLength(); i++) {
            Element eleOrderLine = (Element) nLOrderLineRO.item(i);
            String sReturnReason = eleOrderLine.getAttribute(GCConstants.RETURN_REASON);
            String sPrimeLineNo = eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
            String sOrderedQty = eleOrderLine.getAttribute(GCConstants.ORDERED_QTY);
            
            Element eleDerivedFromOrderLine =
                GCXMLUtil.getElementByXPath(inDoc, "//OrderLine[@PrimeLineNo='" + sPrimeLineNo + "']/DerivedFromOrderLine");
            Document docEleOrderLine = GCXMLUtil.getDocumentFromElement(eleOrderLine);

            String sDerivedPrimeLineNo =
                GCXMLUtil.getAttributeFromXPath(docEleOrderLine, "//DerivedFromOrderLine/@PrimeLineNo");
            String sDerivedSubLineNo =
                GCXMLUtil.getAttributeFromXPath(docEleOrderLine, "//DerivedFromOrderLine/@SubLineNo");

            String sDerivedOrderLineKey = GCXMLUtil.getAttributeFromXPath(docEleOrderLine, "//@DerivedFromOrderLineKey");
            LOGGER.debug("sDerivedFromOrderLinePrimeLineNo" + sDerivedPrimeLineNo);
            listOLWarrEle = map.get(sDerivedOrderLineKey);
            if (!YFCCommon.isVoid(listOLWarrEle)) {
              LOGGER.debug("Inside warr if condition");
              for (int iCounter = 0; iCounter < listOLWarrEle.size(); iCounter++) {
                LOGGER.debug("listOLWarrEle.size::" + listOLWarrEle.size());
                Element eleOLWarr = listOLWarrEle.get(iCounter);
                LOGGER.debug("eleOLWarr Document" + GCXMLUtil.getElementString(eleOLWarr));
                String sWarrOrderLineKey = eleOLWarr.getAttribute(GCConstants.ORDER_LINE_KEY);
                Element eleWarrOrderLine =
                    GCXMLUtil.getElementByXPath(docGetSalesOrderDetOut, "OrderList/Order/OrderLines/OrderLine[@DerivedFromOrderLineKey='"
                        + sWarrOrderLineKey + "']");
                String sReturnableQty = eleOLWarr.getAttribute("ReturnableQty");
                double dReturnableQty = Double.parseDouble(sReturnableQty);
                double dOrderedQty = Double.parseDouble(sOrderedQty);
                
                Date dActualShipmentDate;
                //MPOS-2033: Stamp OrderDate as ShipmentDate for mPOS orders: Start.
                String sEntryType = GCXMLUtil.getAttributeFromXPath(docGetSalesOrderDetOut, "OrderList/Order/@EntryType");
                String sOrderDate = GCXMLUtil.getAttributeFromXPath(docGetSalesOrderDetOut, "OrderList/Order/@OrderDate");
                if(YFCCommon.equalsIgnoreCase(sEntryType, GCConstants.MPOS))
                {
                	LOGGER.verbose("OrderDate fetched to stamp as ShipmentDate for mPOS orders");
                	
                	String sActualShipmentDate = sOrderDate;
                	
                	SimpleDateFormat sdf = new SimpleDateFormat(GCConstants.DATE_TIME_FORMAT);

                	      Date date = sdf.parse(sActualShipmentDate);
                	      Calendar cActualShipmentDate = Calendar.getInstance();
                	      cActualShipmentDate.setTime(date);

                	      int iReturnWindow = Integer.parseInt(sReturnWindow);
                	      cActualShipmentDate.add(Calendar.DATE, iReturnWindow);

                	      sActualShipmentDate = sdf.format(cActualShipmentDate.getTime());
                	      dActualShipmentDate = sdf.parse(sActualShipmentDate);

                	      LOGGER.verbose("dActualShipmentDate-->"+ dActualShipmentDate);
                	      Calendar cCurrentDate = Calendar.getInstance();
                	      // Now use today date.
                	      cCurrentDate.setTime(new Date());

                	      String sCurrentDate = sdf.format(cCurrentDate.getTime());

                	      dCurrentDate = sdf.parse(sCurrentDate);
                	     
                          LOGGER.verbose("dCurrentDate-->"+ dCurrentDate);

                }
              //MPOS-2033: Stamp OrderDate as ShipmentDate for mPOS orders: End.
                
                else{
                	// Checking if Return Order is created within 60 days
                
                dActualShipmentDate = fetchActualShipmentDate(env, sDerivedOrderLineKey, sDerivedOrdrHdrKey);
                }
                Element eleExchangeOrder =
                    GCXMLUtil.getElementByXPath(docRetOrderList,
                        "//Order/ExchangeOrders/ExchangeOrder");
                
               // String eleWarrantyOLK = eleWarrOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
              //  Element eleReturnOrderLineForWarranty = GCXMLUtil.getElementByXPath(inDoc, "Order/OrderLines/OrderLine[@DerivedFromOrderLineKey='"+eleWarrantyOLK+"']"); 
                if (dActualShipmentDate.after(dCurrentDate) && YFCCommon.isVoid(eleWarrOrderLine)
                    && YFCCommon.isVoid(eleExchangeOrder) && !YFCCommon.equals(dReturnableQty, 0.0)) {
                	
                  String eleWarrantyOLK = eleOLWarr.getAttribute(GCConstants.ORDER_LINE_KEY);
                  LOGGER.debug("eleWarrantyOLK is :" + eleWarrantyOLK);
                  Element eleReturnOrderLineForWarranty = GCXMLUtil.getElementByXPath(inDoc, "Order/OrderLines/OrderLine[@DerivedFromOrderLineKey='"+eleWarrantyOLK+"']"); 
                     if(YFCCommon.isVoid(eleReturnOrderLineForWarranty)){
                  String sShipNode =  GCXMLUtil.getAttributeFromXPath(docRetOrderList, "OrderList/Order/OrderLines/OrderLine[@PrimeLineNo = '" + sPrimeLineNo + "']/@ShipNode");
                  String sConditionVariable1 =  GCXMLUtil.getAttributeFromXPath(docRetOrderList, "OrderList/Order/OrderLines/OrderLine[@PrimeLineNo = '" + sPrimeLineNo + "']/@ConditionVariable1");
                  
                  Element eleOrderLineRO = inDoc.createElement(GCConstants.ORDER_LINE);
                  if(dReturnableQty<dOrderedQty){
                	  eleOrderLineRO.setAttribute(GCConstants.ORDERED_QTY, sReturnableQty);
                  }
                  else{
                	  eleOrderLineRO.setAttribute(GCConstants.ORDERED_QTY, sOrderedQty);
                  }
                  eleOrderLineRO.setAttribute(GCConstants.ACTION, GCConstants.CREATE);
                  eleOrderLineRO.setAttribute(GCConstants.RETURN_REASON, sReturnReason);
                  if(!YFCCommon.isVoid(sShipNode)){
                	  eleOrderLineRO.setAttribute(GCConstants.RECEIVING_NODE, sShipNode);
                	  eleOrderLineRO.setAttribute(GCConstants.SHIP_NODE, sShipNode);
                  }
                  if(!YFCCommon.isVoid(sConditionVariable1)){
                	  eleOrderLineRO.setAttribute("ConditionVariable1", sConditionVariable1);
                  }

                  // Fix for 2297 : Start
                  eleOrderLineRO.setAttribute(GCConstants.DERIVED_FROM_ORDER_LINE_KEY, sWarrOrderLineKey);
                 // Fix for 2297 : End
                  Element eleDerivedFrom = inDoc.createElement(GCConstants.DERIVED_FROM);
                  eleDerivedFrom.setAttribute(GCConstants.ORDER_LINE_KEY, sWarrOrderLineKey);
                  Element eleDependancy = inDoc.createElement(GCConstants.DEPENDENCY);
                  eleDependancy.setAttribute(GCConstants.DEPENDENT_PRIME_LINE_NO, sPrimeLineNo);
                  eleDependancy.setAttribute(GCConstants.DEPENDENT_SUB_LINE_NO, sDerivedSubLineNo);
                  eleOrderLineRO.appendChild(eleDependancy);
                  eleOrderLineRO.appendChild(eleDerivedFrom);
                  callCopyCommissionToOrdLine(inDoc, docGetSalesOrderDetOut, sWarrOrderLineKey, eleOrderLineRO);
                  eleOrderLinesRO.appendChild(eleOrderLineRO);
                }
                }
                }
              
            }
            Element eleGCOLCommissionList = mapCommission.get(sDerivedPrimeLineNo);
            if (!YFCCommon.isVoid(eleGCOLCommissionList)) {
              copyCommissionToOrderLine(inDoc, eleGCOLCommissionList, eleOrderLine);
            }
            eleOrderLine.removeChild(eleDerivedFromOrderLine);
          }
          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("gcAddWarrantyLineToRO() - Input to change Order document" + GCXMLUtil.getXMLString(inDoc));
          }
          String strCOXML= GCXMLUtil.getXMLString(inDoc);
          env.setTxnObject("CO_XML", strCOXML);
          /*
           * Calling the below service again as Tax was not getting calculated for Warranty Item
           */
          GCCommonUtil.invokeService(env, GCConstants.GC_UPDATE_RETURN_ORDER_PRICE_SERVICE, docUpdateOrderPrice);
          LOGGER.verbose("Class: GCAddWarrantyLineToRO Method: gcAddWarrantyLineToRO END");

        }

      } catch (Exception e) {
        LOGGER.error("The exception is in gcAddWarrantyLineToROmethod:", e);
        throw new GCException(e);
      }
    }
  }

  /**
   *
   * @param inDoc
   * @param docGetSalesOrderDetOut
   * @param sWarrOrderLineKey
   * @param eleOrderLineRO
   * @throws ParserConfigurationException
   * @throws FactoryConfigurationError
   * @throws TransformerException
   */
  private void callCopyCommissionToOrdLine(Document inDoc, Document docGetSalesOrderDetOut, String sWarrOrderLineKey,
      Element eleOrderLineRO) throws ParserConfigurationException, FactoryConfigurationError, TransformerException {

    //Refactored method for Sonar Violation
    Element eleGCOLCommissionList =
        GCXMLUtil.getElementByXPath(docGetSalesOrderDetOut, "//Order/OrderLines/OrderLine[@OrderLineKey='"
            + sWarrOrderLineKey + "']/Extn/GCOrderLineCommissionList");
    Element eleSOWarrantyExtn = GCXMLUtil.getElementByXPath(docGetSalesOrderDetOut, "//Order/OrderLines/OrderLine[@OrderLineKey='"
    		+ sWarrOrderLineKey + "']/Extn");
    if (!YFCCommon.isVoid(eleGCOLCommissionList)) {
      LOGGER.debug("*^%^&^&%&^%&^%&^");
      copyCommissionToOrderLine(inDoc, eleGCOLCommissionList, eleOrderLineRO);
    }
    if(!YFCCommon.isVoid(eleSOWarrantyExtn)){
    	Element eleROWarrantyExtn = (Element) eleOrderLineRO.getElementsByTagName("Extn").item(0);
    	if(YFCCommon.isVoid(eleROWarrantyExtn)){
    		eleROWarrantyExtn = inDoc.createElement("Extn");
    		eleOrderLineRO.appendChild(eleROWarrantyExtn);
    	}
    	eleROWarrantyExtn.setAttribute("ExtnPOSSalesTicketNo", eleSOWarrantyExtn.getAttribute("ExtnPOSSalesTicketNo"));
    	eleROWarrantyExtn.setAttribute("ExtnIsStoreFulfilled", eleSOWarrantyExtn.getAttribute("ExtnIsStoreFulfilled"));
    	eleROWarrantyExtn.setAttribute("ExtnPOSSKUNumber", eleSOWarrantyExtn.getAttribute("ExtnPOSSKUNumber"));
    	//eleROWarrantyExtn.setAttribute("ExtnInventoryCost", eleSOWarrantyExtn.getAttribute("ExtnInventoryCost"));
    	eleROWarrantyExtn.setAttribute("ExtnIsFreeGiftItem", eleSOWarrantyExtn.getAttribute("ExtnIsFreeGiftItem"));
    	eleROWarrantyExtn.setAttribute("ExtnIsWarrantyItem", eleSOWarrantyExtn.getAttribute("ExtnIsWarrantyItem"));
    	
    }
  }

  /**
   *
   * Description of copyCommissionToOrderLine
   *
   * @param inDoc
   * @param eleGCOLCommissionList
   * @param eleOrderLine
   * @throws ParserConfigurationException
   * @throws FactoryConfigurationError
   * @throws TransformerException
   *
   */
  private void copyCommissionToOrderLine(Document inDoc,
      Element eleGCOLCommissionList, Element eleOrderLine)
          throws ParserConfigurationException, FactoryConfigurationError,
          TransformerException {
    LOGGER.verbose("Class: GCAddWarrantyLineToRO Method: copyCommissionToOrderLine END");

    Element eleOLExtn = inDoc.createElement(GCConstants.EXTN);
    Element eleGCOrdrLineCommList = inDoc
        .createElement(GCConstants.GC_ORDER_LINE_COMMISSION_LIST);
    Document docEleGCOrdrLineCommList = GCXMLUtil
        .getDocumentFromElement(eleGCOLCommissionList);
    NodeList nLGCOrderLineComm = GCXMLUtil.getNodeListByXpath(
        docEleGCOrdrLineCommList,
        "GCOrderLineCommissionList/GCOrderLineCommission");
    for (int k = 0; k < nLGCOrderLineComm.getLength(); k++) {

      Element eleGCOLineCommission = (Element) nLGCOrderLineComm.item(k);
      Element eleGCOLCommOut = inDoc
          .createElement(GCConstants.GC_ORDER_LINE_COMISSION);
      GCXMLUtil.copyAttributes(eleGCOLineCommission, eleGCOLCommOut);
      eleGCOLCommOut.removeAttribute(GCConstants.ORDER_LINE_KEY);
      eleGCOLCommOut
      .removeAttribute(GCConstants.ORDER_LINE_COMMISSION_KEY);
      eleGCOLCommOut.removeAttribute(GCConstants.CREATE_PROG_ID);
      eleGCOLCommOut.removeAttribute(GCConstants.CREATE_TS);
      eleGCOLCommOut.removeAttribute(GCConstants.CREATE_USER_ID);
      eleGCOLCommOut.removeAttribute(GCConstants.LOCK_ID);
      eleGCOLCommOut.removeAttribute(GCConstants.MODIFY_PROG_ID);
      eleGCOLCommOut.removeAttribute(GCConstants.MODIFY_TS);
      eleGCOLCommOut.removeAttribute(GCConstants.MODIFY_USER_ID);
      eleGCOLCommOut.removeAttribute(GCConstants.IS_HISTORY);
      eleGCOrdrLineCommList.appendChild(eleGCOLCommOut);
    }
    eleOLExtn.appendChild(eleGCOrdrLineCommList);
    eleOrderLine.appendChild(eleOLExtn);

  }

  /**
   *
   * Description of fetchActualShipmentDate
   *
   * @param env
   * @param sDerivedOrderLineKey
   * @param sDerivedOrdrHdrKey
   * @return
   * @throws Exception
   *
   */
  private Date fetchActualShipmentDate(YFSEnvironment env,
      String sDerivedOrderLineKey, String sDerivedOrdrHdrKey)
          throws GCException {
    try {
      Document docShipmentListForOrderIn = GCXMLUtil
          .createDocument(GCConstants.ORDER);
      Element eleRoot = docShipmentListForOrderIn.getDocumentElement();
      eleRoot.setAttribute(GCConstants.ORDER_HEADER_KEY,
          sDerivedOrdrHdrKey);
      eleRoot.setAttribute(GCConstants.ORDER_LINE_KEY,
          sDerivedOrderLineKey);

      Document docShipmentList = GCCommonUtil.invokeAPI(env,
          GCConstants.API_GET_SHIPMENT_LIST_FOR_ORDER,
          docShipmentListForOrderIn);
      String sActualShipmentDate = GCXMLUtil.getAttributeFromXPath(
          docShipmentList, "//Shipment/@ActualShipmentDate");

      SimpleDateFormat sdf = new SimpleDateFormat(
          GCConstants.DATE_TIME_FORMAT);

      Date date = sdf.parse(sActualShipmentDate);
      Calendar cActualShipmentDate = Calendar.getInstance();
      cActualShipmentDate.setTime(date);

      int iReturnWindow = Integer.parseInt(sReturnWindow);
      cActualShipmentDate.add(Calendar.DATE, iReturnWindow);

      sActualShipmentDate = sdf.format(cActualShipmentDate.getTime());
      Date dActualShipmentDate = sdf.parse(sActualShipmentDate);

      Calendar cCurrentDate = Calendar.getInstance();
      // Now use today date.
      cCurrentDate.setTime(new Date());

      String sCurrentDate = sdf.format(cCurrentDate.getTime());

      dCurrentDate = sdf.parse(sCurrentDate);

      LOGGER.debug("sActualShipmentDate::" + sActualShipmentDate
          + "   sCurrentDate::" + sCurrentDate);

      return dActualShipmentDate;
    } catch (Exception e) {
      LOGGER.error("The Exception is in fetchActualShipmentDate method:",
          e);
      throw new GCException(e);
    }
  }

  @Override
  public void setProperties(Properties arg0) throws GCException {
    try {
      sReturnWindow = arg0
          .getProperty(GCConstants.RETURN_WINDOW_FOR_WARRANTY_ADDITION);
      LOGGER.debug("Return Window:::" + sReturnWindow);
    } catch (Exception e) {
      LOGGER.error("The Exception is in setProperties method:", e);
      throw new GCException(e);
    }

  }

}