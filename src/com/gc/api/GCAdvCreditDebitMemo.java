/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class creates debit memo for Advance Credit Return Type on Order Invoice Creation
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0              14/05/2014        Soni, Karan		           Initial Version
 *#################################################################################################################################################################
 */
package com.gc.api;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author karan
 */
public class GCAdvCreditDebitMemo {
    // initialize logger
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCCreateReturnOnSuccess.class);

    public Document createDebitMemo(YFSEnvironment env, Document inDoc)
	    throws GCException {
	try {
	    LOGGER.verbose("Entering the createdebitMemo method");
	    LOGGER.debug("Input XML for createdebitMemo Method :- " + GCXMLUtil.getXMLString(inDoc));
	    Element eleOrderInvoice = inDoc.getDocumentElement();
	    Element eleOrder = (Element) XPathAPI.selectSingleNode(
		    eleOrderInvoice, "Order");
	    String strOrderType = eleOrder.getAttribute(GCConstants.ORDER_TYPE);
	    if (GCConstants.RETURN_WITH_ADV_CREDIT.equals(strOrderType)) {
		String strCurrency = GCXMLUtil.getAttribute(eleOrderInvoice,
			GCConstants.CURRENCY);
		String strOrderNo = eleOrderInvoice
			.getAttribute(GCConstants.ORDER_NO);
		String strSellerOrg = eleOrder
			.getAttribute(GCConstants.SELLER_ORGANIZATION_CODE);
		String strOrderDate = eleOrder
			.getAttribute(GCConstants.ORDER_DATE);
		String strEnterpriseCode = eleOrder
			.getAttribute(GCConstants.ENTERPRISE_CODE);
		String strOHK = GCXMLUtil
			.getAttributeFromXPath(eleOrder,
				"OrderLines/OrderLine/DerivedFromOrder/@OrderHeaderKey");
		
// GCSTORE-5597 modification due to avoid consolidation logic 
		
		String strTotalInvoiceAmount = eleOrderInvoice.getAttribute("TotalAmount");
		
		String strTotalAmount = GCXMLUtil.getAttributeFromXPath(
			eleOrderInvoice, "Order/PriceInfo/@TotalAmount");
		
		String strAmountToProcess = String.valueOf(-Double.valueOf(strTotalInvoiceAmount));	

		//GCSTORE-5597-starts
		if (YFCCommon.isVoid(strOHK)) {
				    LOGGER.debug("Order HeaderKey is Null");
				    strOHK=GCXMLUtil.getAttributeFromXPath(inDoc, "//@DerivedFromOrderHeaderKey");
				    LOGGER.debug("strOHK :: "+strOHK);
				}
				//GCSTORE-5597-ends
		
		 Document recordExternalChargesDoc = GCXMLUtil.createDocument(GCConstants.RECORD_EXTERNAL_CHARGES);
		 Element eleRecordExternalCharges = GCXMLUtil.getRootElement(recordExternalChargesDoc);
         eleRecordExternalCharges.setAttribute("OrderHeaderKey", strOHK);
	     Element elePaymentMethod = recordExternalChargesDoc.createElement(GCConstants.PAYMENT_METHOD);
	     Element elePaymentDetailsList = recordExternalChargesDoc.createElement("PaymentDetailsList");
	     Element elePaymentDetails = recordExternalChargesDoc.createElement("PaymentDetails");
	     eleRecordExternalCharges.appendChild(elePaymentMethod);
	     elePaymentMethod.appendChild(elePaymentDetailsList);
	     elePaymentDetailsList.appendChild(elePaymentDetails);
	   
		 elePaymentMethod.setAttribute(GCConstants.PAYMENT_TYPE, "REVRS_ADV_CRDT");
		 elePaymentMethod.setAttribute(GCConstants.MAX_CHARGE_LIMIT, "5000");
		 elePaymentMethod.setAttribute("PaymentReference1", "1234567890");	  
		 elePaymentDetails.setAttribute(GCConstants.CHARGE_TYPE, "CHARGE");
		 elePaymentDetails.setAttribute(GCConstants.PAYMENT_PROCESSED_AMOUNT, strTotalInvoiceAmount);
		 elePaymentDetails.setAttribute(GCConstants.REQUEST_AMOUNT_PAYMENT, "");
		 LOGGER.debug("Calling the Record External Charges API");
		 LOGGER.debug("Input for Calling the Record External Charges API for the first time " + GCXMLUtil.getXMLString(recordExternalChargesDoc));
		 GCCommonUtil.invokeAPI(env, GCConstants.RECORD_EXTERNAL_CHARGES, recordExternalChargesDoc);
		
		

		String strCreditMemo = "<OrderInvoice Currency='" + strCurrency
			+ "' DateInvoiced='" + strOrderDate + "' OrderNo='"
			+ strOrderNo + "' DocumentType='0001' EnterpriseCode='"
			+ strEnterpriseCode + "' InvoiceType='"
			+ GCConstants.DEBIT_MEMO + "' OrderHeaderKey='"
			+ strOHK + "' SellerOrganizationCode='" + strSellerOrg
			+ "'><HeaderChargeList/></OrderInvoice>";
		Document inDocDebitMemo = GCXMLUtil.getDocument(strCreditMemo);
		Element eleHeaderChargeList = GCXMLUtil.getElementByXPath(
				inDocDebitMemo, "OrderInvoice/HeaderChargeList");
		if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("The recordInvoiceCreation indoc after appending header is"
			+ GCXMLUtil.getXMLString(inDocDebitMemo));
		}
		Element eleHeaderCharge = inDocDebitMemo
			.createElement(GCConstants.HEADER_CHARGE);
		eleHeaderChargeList.appendChild(eleHeaderCharge);
		eleHeaderCharge.setAttribute(GCConstants.CHARGE_CATEGORY,
			GCConstants.DEBIT_ON_INVOICE);
		eleHeaderCharge.setAttribute(GCConstants.CHARGE_NAME,
			GCConstants.DEBIT_ON_INVOICE);
		eleHeaderCharge.setAttribute(GCConstants.CHARGE_AMOUNT,
				strAmountToProcess);
		 LOGGER.debug("Input for Calling the recordInvoiceCreation indoc " + GCXMLUtil.getXMLString(inDocDebitMemo));
		GCCommonUtil.invokeAPI(env,
			GCConstants.RECORD_INVOICE_CREATION, inDocDebitMemo);
		LOGGER.verbose("Exiting the createDebitMemo method");
		
		 elePaymentDetails.setAttribute(GCConstants.PAYMENT_PROCESSED_AMOUNT, strAmountToProcess);

		 LOGGER.debug("Input for Calling the Record External Charges API for the Second time " + GCXMLUtil.getXMLString(recordExternalChargesDoc));
		 GCCommonUtil.invokeAPI(env, GCConstants.RECORD_EXTERNAL_CHARGES, recordExternalChargesDoc);
		 
		 String strRequestCollection =
		            "<Order OrderHeaderKey='" + strOHK + "' />";
		 
		 GCCommonUtil.invokeAPI(env, GCConstants.REQUEST_COLLECTION, YFCDocument.getDocumentFor(strRequestCollection),
			        YFCDocument.createDocument("Order"));
	    }
	    return inDoc;

	} catch (Exception e) {
	    LOGGER.error("Inside GCAdvCreditDebitMemo.createDebitMemo():",e);
	    throw new GCException(e);
	}
    }
}
