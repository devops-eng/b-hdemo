/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 * *###########################################
 * ######################################################
 * ################################################################ OBJECTIVE: This class applies
 * level of service code to the line level
 * ##########################################################
 * ########################################
 * ############################################################### Version Date Modified By
 * Description
 * ######################################################################################
 * ########################################################################### 1.0 14/03/2014
 * Kumar,Prateek Created 1.1 19/03/2015 Ravi, Divya GCSTORE-803: Development - Carrier Service code
 * stamping 1.2 21/04/2015 Ravi, Divya GCSTORE-1847 - Code to remove carrier service code for non DC
 * scheduled lines
 * ##################################################################################
 * ###############################################################################
 */
package com.gc.api;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.gc.userexit.GCBeforeChangeReturnOrderUE;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCApplyCarrierServiceCodeToOrderLine {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCApplyCarrierServiceCodeToOrderLine.class);
  YFSEnvironment env = null;

  /**
   *
   * @param env
   * @param inDoc
   * @throws Exception
   */
  public Document applyLevelOfServiceToOrderLine(YFSEnvironment env, Document inDoc) throws GCException {
    try {
      final String logInfo = "GCApplyCarrierServiceCodeToOrderLine :: applyShippingMethodConstraints :: ";
      LOGGER.verbose(logInfo + "Begin");
      this.env = env;

      LOGGER.debug(logInfo + "Input Doc\n" + GCXMLUtil.getXMLString(inDoc));

      Document docGetShippingLevelFlagIp = GCXMLUtil.createDocument(GCConstants.SHIPPING_LEVEL_FLAG);
      Document docGetShippingLevelFlagOp =
          GCCommonUtil.invokeService(env, GCConstants.GC_GET_SHIPPING_LEVEL_FLAG, docGetShippingLevelFlagIp);
      LOGGER.debug(logInfo + "docGetShippingLevelFlagOp\n" + GCXMLUtil.getXMLString(docGetShippingLevelFlagOp));

      String sLevelOfService = GCXMLUtil.getAttributeFromXPath(inDoc, "//Order/@LevelOfService");
      if (!YFCCommon.isVoid(sLevelOfService)) {

        String sExpedite =
            GCXMLUtil.getAttributeFromXPath(docGetShippingLevelFlagOp, "//GCShippingLevelFlag[@LevelOfService='"
                + sLevelOfService + "']/@Expedite");
        LOGGER.debug(logInfo + "sExpedite :: " + sExpedite);
        // Stamp Expedite flag

        Element eleExtn = (Element) XPathAPI.selectSingleNode(inDoc, "//Order/Extn");
        if (YFCCommon.isVoid(eleExtn)) {
          eleExtn = inDoc.createElement(GCConstants.EXTN);
          Element eleRootIp = inDoc.getDocumentElement();
          eleRootIp.appendChild(eleExtn);
        }
        eleExtn.setAttribute(GCConstants.EXTN_EXPEDITE_FLAG, sExpedite);

        //
        String sModeOfDelivery = "";

        if (!YFCCommon.isVoid(XPathAPI.selectSingleNode(inDoc, "//PrimaryInformation[@IsHazmat='Y']"))) {
          sModeOfDelivery = "02";
        } else if (GCConstants.DIGITAL_DELIVERY.equals(sLevelOfService)
            || GCConstants.EMPLOYEE_PICKUP.equals(sLevelOfService) || GCConstants.DROP_SHIP.equals(sLevelOfService)
            || GCConstants.FIFTY_ONE_GROUND.equals(sLevelOfService)) {
          sModeOfDelivery =
              GCXMLUtil.getAttributeFromXPath(docGetShippingLevelFlagOp, "//GCShippingLevelFlag[@LevelOfService='"
                  + sLevelOfService + "']/@ModeOfDelivery");

        }
        LOGGER.debug(logInfo + "sModeOfDelivery :: " + sModeOfDelivery);

        if (!"".equals(sModeOfDelivery)) {
          stampSameModeOfDeliveryToAllLines(inDoc, sModeOfDelivery);
        } else {
          stampModeOfDeliveryForEachLine(inDoc, sLevelOfService);
        }
      }
      LOGGER.verbose(logInfo + "End");

      return inDoc;
    } catch (Exception e) {
      LOGGER.error("The Exception is in applyLevelOfServiceToOrderLine method:", e);
      throw new GCException(e);
    }
  }

  /**
   *
   * @param inDoc
   * @param sModeOfDelivery
   * @param docChangeOrderIp
   * @param eleOrderLines
   * @throws Exception
   */
  private void stampSameModeOfDeliveryToAllLines(Document inDoc, String sModeOfDelivery) throws GCException {
    try {
      final String logInfo = "GCApplyCarrierServiceCodeToOrderLine :: stampSameModeOfDeliveryToAllLines :: ";
      LOGGER.verbose(logInfo + "Begin");

      // Phase 2 changes Order Fulfillment - filtering lines fulfilled from MFI
      NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc, "//OrderLine");
      for (int i = 0; i < nlOrderLine.getLength(); i++) {
        Element eleOrderLine = (Element) nlOrderLine.item(i);
        Element eleExtn = (Element) eleOrderLine.getElementsByTagName(GCConstants.EXTN).item(0);
        String carrierServiceCode = eleOrderLine.getAttribute(GCConstants.CARRIER_SERVICE_CODE);
        String isWarranty = eleExtn.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);

        String shipNode = GCXMLUtil.getAttributeFromXPath(eleOrderLine, "Schedules/Schedule/@ShipNode");
        LOGGER.verbose("ShipNode : " + shipNode);
        if (!YFCCommon.isVoid(shipNode)) {
          GCBeforeChangeReturnOrderUE obj = new GCBeforeChangeReturnOrderUE();
          String sNodeType = obj.fetchNodeType(env, shipNode);
          if (YFCUtils.equals(GCConstants.NO, isWarranty)
              || ((YFCUtils.equals(GCConstants.YES, isWarranty) && YFCCommon.isVoid(carrierServiceCode)))) {
            if (!YFCCommon.equals(GCConstants.STORE, sNodeType)) {
              Element eleBundleParentLine =
                  (Element) eleOrderLine.getElementsByTagName(GCConstants.BUNDLE_PARENT_LINE).item(0);
              // if order line is bundle component then don't
              // apply carrier service code in it.
              // if (YFCCommon.isVoid(eleBundleParentLine)) {

              eleOrderLine.setAttribute(GCConstants.CARRIER_SERVICE_CODE, sModeOfDelivery);
              LOGGER.debug(logInfo + "stampSameModeOfDeliveryToAllLines if :: " + sModeOfDelivery);
              // }
            } else {
              // GCSTORE-1847 blank out carrier service for non DC sourced lines
              eleOrderLine.setAttribute(GCConstants.CARRIER_SERVICE_CODE, "");
              LOGGER.debug(logInfo + "stampSameModeOfDeliveryToAllLines else :: ");
            }
          }
        }
        LOGGER.verbose(logInfo + "End");
      }
    } catch (Exception e) {
      LOGGER.error("The Exception is in stampSameModeOfDeliveryToAllLines method:", e);
      throw new GCException(e);
    }

  }

  /**
   *
   * @param inDoc
   * @param sLevelOfService
   * @param docChangeOrderIp
   * @param eleOrderLines
   * @throws Exception
   */
  private void stampModeOfDeliveryForEachLine(Document inDoc, String sLevelOfService) throws GCException {
    try {
      final String logInfo = "GCApplyCarrierServiceCodeToOrderLine :: stampModeOfDeliveryForEachLine :: ";
      LOGGER.verbose(logInfo + "Begin");

      String sExtnIsAPOFPO = GCXMLUtil.getAttributeFromXPath(inDoc, "//PersonInfoShipTo/Extn/@ExtnIsAPOFPO");
      LOGGER.debug(logInfo + "sExtnIsAPOFPO ::" + sExtnIsAPOFPO);
      String sExtnIsPOBox = GCXMLUtil.getAttributeFromXPath(inDoc, "//PersonInfoShipTo/Extn/@ExtnIsPOBox");
      LOGGER.debug(logInfo + "sExtnIsPOBox ::" + sExtnIsPOBox);
      String sShipToCountry = GCXMLUtil.getAttributeFromXPath(inDoc, "//PersonInfoShipTo/@Country");
      LOGGER.debug(logInfo + "sShipToCountry ::" + sShipToCountry);

      // Phase 2 changes Order Fulfillment - filtering lines shipped from MFI
      NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc, "//OrderLine");
      for (int i = 0; i < nlOrderLine.getLength(); i++) {

        Element eleOrderLine = (Element) nlOrderLine.item(i);
        Element eleExtn = (Element) eleOrderLine.getElementsByTagName(GCConstants.EXTN).item(0);
        String carrierServiceCode = eleOrderLine.getAttribute(GCConstants.CARRIER_SERVICE_CODE);
        String isWarranty = eleExtn.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);

        String shipNode = GCXMLUtil.getAttributeFromXPath(eleOrderLine, "Schedules/Schedule/@ShipNode");
        LOGGER.verbose("ShipNode : " + shipNode);
        if (!YFCCommon.isVoid(shipNode)) {
          if (YFCUtils.equals(GCConstants.NO, isWarranty)
              || ((YFCUtils.equals(GCConstants.YES, isWarranty) && YFCCommon.isVoid(carrierServiceCode)))) {
            GCBeforeChangeReturnOrderUE obj = new GCBeforeChangeReturnOrderUE();
            String sNodeType = obj.fetchNodeType(env, shipNode);
            if (!YFCCommon.equals(GCConstants.STORE, sNodeType)) {
              Document docOrderLine = GCXMLUtil.getDocument((Element) nlOrderLine.item(i), true);

            Element eleBundleParentLine =
                (Element) eleOrderLine.getElementsByTagName(GCConstants.BUNDLE_PARENT_LINE).item(0);
            // if order line is bundle component then don't carrier service
            // code
            // in it.
            // if (YFCCommon.isVoid(eleBundleParentLine)) {

            String sExtnIsTruck = GCXMLUtil.getAttributeFromXPath(docOrderLine, "//@ExtnIsTruckShip");
            LOGGER.debug(logInfo + "sExtnIsTruck ::" + sExtnIsTruck);

            String sModeOfDelivery = "";
             if (GCConstants.DIGITAL_ITEM_TYPE.equals(GCXMLUtil.getAttributeFromXPath(docOrderLine, "//@ItemType"))
                || "10".equals(GCXMLUtil.getAttributeFromXPath(docOrderLine, "//@ItemType"))) {
              sModeOfDelivery = "99";
            } else if (GCConstants.YES.equals(sExtnIsTruck) || !GCConstants.US.equals(sShipToCountry)) {
              sModeOfDelivery = "04";
            } else if (GCConstants.YES.equals(sExtnIsAPOFPO) || GCConstants.YES.equals(sExtnIsPOBox)) {
              sModeOfDelivery = "05";
            } else {
              if (GCConstants.ECONOMY_GROUND.equals(sLevelOfService)) {
                sModeOfDelivery = "02";
              } else if (GCConstants.STANDARD_GROUND.equals(sLevelOfService)) {
                sModeOfDelivery = "37";
              } else if (GCConstants.SECOND_DAY_EXPRESS.equals(sLevelOfService)) {
                sModeOfDelivery = "01";
              } else if (GCConstants.NEXT_DAY_AIR.equals(sLevelOfService)) {
                sModeOfDelivery = "52";
              }else if ("STANDARD_INTL".equals(sLevelOfService)) {
                  sModeOfDelivery = "20";
                } 
			   else {
                // Defaulting to Standard Ground--- Fix for GCSTORE-4339
                sModeOfDelivery = "37";
              }
            }
            eleOrderLine.setAttribute(GCConstants.CARRIER_SERVICE_CODE, sModeOfDelivery);
            LOGGER.debug(logInfo + "stampModeOfDeliveryForEachLine if :: " + sModeOfDelivery);
            // }
          } else {
            // GCSTORE-1847 blank out carrier service for non DC sourced lines
            eleOrderLine.setAttribute(GCConstants.CARRIER_SERVICE_CODE, "");
            LOGGER.debug(logInfo + "stampModeOfDeliveryForEachLine else :: ");
          }
}
        }

        LOGGER.verbose(logInfo + "End");
      }
    } catch (Exception e) {
      LOGGER.error("The exception is in stampModeOfDeliveryForEachLine method:", e);
      throw new GCException(e);
    }
  }

}
