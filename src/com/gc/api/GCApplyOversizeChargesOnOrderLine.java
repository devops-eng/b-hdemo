/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class is used to add oversize charges items for which ExtnTruckShip is Y.
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               01/06/2014        Gowda, Naveen		JIRA No: This class is used to add oversize charges items for which ExtnTruckShip is Y.
 *#################################################################################################################################################################
 */
package com.gc.api;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:email Address">Name</a>
 */
public class GCApplyOversizeChargesOnOrderLine {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCApplyOversizeChargesOnOrderLine.class.getName());

    /**
     * 
     * Description of applyOversizeCharges
     * 
     * @param env
     * @param inDoc
     * @throws Exception
     * 
     */
    public void applyOversizeCharges(YFSEnvironment env, Document docChangeOrder)
	    throws GCException {
	try {
	    LOGGER.debug("Class: GCApplyOversizeChargesOnOrderLine Method: applyOversizeCharges BEGIN");
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug(" applyOversizeCharges() method, Input document is"
			+ GCXMLUtil.getXMLString(docChangeOrder));
	    }
	    NodeList nLOrderLine = GCXMLUtil.getNodeListByXpath(docChangeOrder,
		    "OrderList/Order/OrderLines/OrderLine");
	    String sEnterpriseCode = GCXMLUtil.getAttributeFromXPath(
		    docChangeOrder, "//Order/@EnterpriseCode");
	    String sOversizeChargeVal = fetchOversizeChargeValue(env,
		    sEnterpriseCode);

	    for (int i = 0; i < nLOrderLine.getLength(); i++) {
		Element eleOrderLine = (Element) nLOrderLine.item(i);
		String sOrderLineKey = eleOrderLine
			.getAttribute(GCConstants.ORDER_LINE_KEY);
		Document docOrderLine = GCXMLUtil
			.getDocumentFromElement(eleOrderLine);
		String sExtnIsTruckShip = GCXMLUtil.getAttributeFromXPath(
			docOrderLine, "//ItemDetails/Extn/@ExtnIsTruckShip");
		if (GCConstants.FLAG_Y.equalsIgnoreCase(sExtnIsTruckShip)) {
		    LOGGER.debug("Inside If condtion");
		    Element eleOrderLineCO = GCXMLUtil.getElementByXPath(
			    docChangeOrder, "//OrderLine[@OrderLineKey='"
				    + sOrderLineKey + "']");
		    Element eleLineChargesOfOL = GCXMLUtil.getElementByXPath(
			    docChangeOrder, "//OrderLine[@OrderLineKey='"
				    + sOrderLineKey + "']/LineCharges");
		    eleOrderLine.removeChild(eleLineChargesOfOL);

		    Element eleLineCharges = docChangeOrder
			    .createElement(GCConstants.LINE_CHARGES);
		    Element eleLineCharge = docChangeOrder
			    .createElement(GCConstants.LINE_CHARGE);
		    eleLineCharge.setAttribute(GCConstants.CHARGE_CATEGORY,
			    GCConstants.SHIPPING);
		    eleLineCharge.setAttribute(GCConstants.CHARGE_NAME,
			    GCConstants.OVERSIZE_CHARGE);
		    eleLineCharge.setAttribute(GCConstants.CHARGE_PER_UNIT,
			    sOversizeChargeVal);
		    eleLineCharges.appendChild(eleLineCharge);
		    eleOrderLineCO.appendChild(eleLineCharges);
		}
	    }
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("applyOversizeCharges() method - changeOrder InDoc"
			+ GCXMLUtil.getXMLString(docChangeOrder));
	    }
	    new GCATGPromoService().callChangeOrder(env,docChangeOrder);
	    LOGGER.debug("Class: GCApplyOversizeChargesOnOrderLine Method: applyOversizeCharges END");
	} catch (Exception e) {
	    LOGGER.error("The Exception is in applyOversizeCharges method:", e);
	    throw new GCException(e);
	}
    }

    /**
     * 
     * Description of fetchOversizeChargeValue
     * 
     * @param env
     * @param sEnterpriseCode
     * @return
     * @throws Exception
     * 
     */
    private String fetchOversizeChargeValue(YFSEnvironment env,
	    String sEnterpriseCode) throws GCException {
	try {
	    LOGGER.debug("Class: GCApplyOversizeChargesOnOrderLine Method: fetchOversizeChargeValue BEGIN");

	    Document docInGetCommon = GCXMLUtil
		    .createDocument(GCConstants.COMMON_CODE);
	    Element eleRoot = docInGetCommon.getDocumentElement();
	    eleRoot.setAttribute(GCConstants.CODE_TYPE,
		    GCConstants.OVERSIZE_SURCHARGE);
	    eleRoot.setAttribute(GCConstants.ORGANIZATION_CODE, sEnterpriseCode);
	    LOGGER.debug("getCommonCodeList InDoc"
		    + GCXMLUtil.getXMLString(docInGetCommon));
	    Document docGetCommonCodeOut = GCCommonUtil.invokeAPI(env,
		    GCConstants.API_GET_COMMON_CODE_LIST, docInGetCommon);
	    LOGGER.debug("getCommonCodeList OutDoc"
		    + GCXMLUtil.getXMLString(docGetCommonCodeOut));
	    String sOverSizeChargeValue = GCXMLUtil.getAttributeFromXPath(
		    docGetCommonCodeOut,
		    "//CommonCodeList/CommonCode/@CodeValue");
	    LOGGER.debug("sOverSizeChargeValue:: " + sOverSizeChargeValue);
	    LOGGER.debug("Class: GCApplyOversizeChargesOnOrderLine Method: fetchOversizeChargeValue END");

	    return sOverSizeChargeValue;
	} catch (Exception e) {
	    LOGGER.error("The Exception is in applyOversizeCharges method:", e);
	    throw new GCException(e);
	}
    }
}
