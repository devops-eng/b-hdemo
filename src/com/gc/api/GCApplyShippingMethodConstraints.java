/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class return the shipping method after applying shipping method constrain.
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               14/03/2014        Kumar,Prateek		Created
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCApplyShippingMethodConstraints {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCApplyShippingMethodConstraints.class.getName());
  YFSEnvironment env = null;

  /**
   *
   * @param env
   * @param inDoc
   * @return
   * @throws Exception
   */
  public Document applyShippingMethodConstraints(YFSEnvironment env,
      Document inDoc) throws GCException {
    try {
      final String logInfo = "GCApplyShippingMethodConstraints :: applyShippingMethodConstraints :: ";
      LOGGER.verbose(logInfo + "Begin");
      LOGGER.debug(logInfo + "Input Doc\n"
          + GCXMLUtil.getXMLString(inDoc));

      this.env = env;

      Document outDoc = GCXMLUtil
          .createDocument(GCConstants.ALLOWED_SHIPPING_METHOD_LIST);
      Element eleOutRoot = outDoc.getDocumentElement();
      // OMS-3843: Taken cancelled status into consideration while
      // returning LOS
      // Requirement GCSTORE-154 Starts - DC employee pickup process

      YFCDocument inDocument = YFCDocument.getDocumentFor(inDoc);
      YFCElement eleOrder = inDocument.getDocumentElement();
      YFCElement elePersonInfoShipTo = eleOrder.getChildElement(GCConstants.PERSON_INFO_SHIP_TO);
      YFCElement eleExtn = eleOrder.getChildElement(GCConstants.EXTN);
      String sSourceCode ="";
      if(!YFCCommon.isVoid(eleExtn)){
        sSourceCode = eleExtn.getAttribute(GCConstants.EXTN_SOURCE_CODE);
      }
      String sState = elePersonInfoShipTo.getAttribute(GCConstants.STATE);
      String sIsUSTerritory = checkState(sState);
      if (YFCCommon.equals(sSourceCode, GCConstants.DCEMPLOYEE)) {
        Element eleShippingMethod = outDoc.createElement(GCConstants.ALLOWED_SHIPPING_METHOD);
        eleOutRoot.appendChild(eleShippingMethod);
        eleShippingMethod.setAttribute(GCConstants.SHIPPING_METHOD, GCConstants.EMPLOYEE_PICKUP);
        eleShippingMethod.setAttribute(GCConstants.SHIPPING_METHOD_DESCRIPTION, GCConstants.EMPLOYEE_PICKUP_DESC);

        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose(logInfo + "Output Doc\n" + GCXMLUtil.getXMLString(outDoc));
        }
        LOGGER.verbose(logInfo + "End");
        return outDoc;
        // Requirement GCSTORE-154 Ends - DC employee pickup process
      } else if (!YFCCommon
          .isVoid(XPathAPI
              .selectSingleNode(
                  inDoc,
                  "//OrderLine[ItemDetails/PrimaryInformation[@IsHazmat='Y'] and OrderStatuses/OrderStatus[@Status!='9000']]"))) {

        Element eleShippingMethod = outDoc
            .createElement(GCConstants.ALLOWED_SHIPPING_METHOD);
        eleOutRoot.appendChild(eleShippingMethod);
        eleShippingMethod.setAttribute(GCConstants.SHIPPING_METHOD,
            GCConstants.ECONOMY_GROUND);
        eleShippingMethod.setAttribute(
            GCConstants.SHIPPING_METHOD_DESCRIPTION,
            GCConstants.ECONOMY_GROUND_DESC);
        /*
         * OMS 3385 - For Hazmat both Standard Ground and Economy Ground
         * should be enabled - Begin
         */
        Element eleSGShippingMethod = outDoc
            .createElement(GCConstants.ALLOWED_SHIPPING_METHOD);
        eleOutRoot.appendChild(eleSGShippingMethod);
        eleSGShippingMethod.setAttribute(GCConstants.SHIPPING_METHOD,
            GCConstants.STANDARD_GROUND);
        eleSGShippingMethod.setAttribute(
            GCConstants.SHIPPING_METHOD_DESCRIPTION,
            GCConstants.STANDARD_GROUND_DESC);
        /* OMS 3385 - END */
        LOGGER.debug(logInfo + "Output Doc\n"
            + GCXMLUtil.getXMLString(outDoc));
        LOGGER.debug(logInfo + "End");
        return outDoc;

      } else if (YFCCommon
          .isVoid(XPathAPI
              .selectSingleNode(inDoc,
                  "//PrimaryInformation[@ItemType!='05' and @ItemType!='10']"))) {
        Element eleShippingMethod = outDoc
            .createElement(GCConstants.ALLOWED_SHIPPING_METHOD);
        eleOutRoot.appendChild(eleShippingMethod);
        eleShippingMethod.setAttribute(GCConstants.SHIPPING_METHOD,
            GCConstants.DIGITAL_DELIVERY);
        eleShippingMethod.setAttribute(
            GCConstants.SHIPPING_METHOD_DESCRIPTION,
            GCConstants.DIGITAL_DELIVERY_DESC);
        LOGGER.debug(logInfo + "Output Doc\n"
            + GCXMLUtil.getXMLString(outDoc));
        LOGGER.debug(logInfo + "End");
        return outDoc;
     // Requirement GCSTORE-154 Ends - DC employee pickup process GCSTORE-6302 Start
      }  else if (!YFCCommon.isVoid(sIsUSTerritory)) {
    	List<String> sLos = Arrays.asList(sIsUSTerritory.split("\\s*,\\s*"));
  	    Iterator<String> iterator = sLos.iterator();
          while (iterator.hasNext()) {
          	String sLosStr = iterator.next();
          	String sCodeDesc[] = sLosStr.split(":");
          	if(sCodeDesc.length ==2)
        	{
	          	String sCode = sCodeDesc[0]; 
	          	String sDesc= sCodeDesc[1];
	          	Element eleShippingMethod = outDoc.createElement(GCConstants.ALLOWED_SHIPPING_METHOD);
	          	 eleOutRoot.appendChild(eleShippingMethod);
	               eleShippingMethod.setAttribute(GCConstants.SHIPPING_METHOD,sCode);
	               eleShippingMethod.setAttribute(GCConstants.SHIPPING_METHOD_DESCRIPTION, sDesc);
        	} // gcstore 6302 end
          	}
      } else {
        getShippingMethodList(inDoc, outDoc);
      }

      //Checking the Gift Item
      checkGiftItem(inDoc,outDoc);

      LOGGER.debug(logInfo + "Output Doc\n"
          + GCXMLUtil.getXMLString(outDoc));
      LOGGER.verbose(logInfo + "End");
      return outDoc;
    } catch (Exception e) {
      LOGGER.error(
          "The Exception is in applyShippingMethodConstraints method:",
          e);
      throw new GCException(e);
    }
  }

  /**
   *
   * @param env
   * @param outDoc
   * @param docGetOrderListOp
   * @return
   * @throws Exception
   */

  private Document getShippingMethodList(Document inDoc, Document outDoc)
      throws GCException {
    try {
      final String logInfo = "GCApplyShippingMethodConstraints :: getShippingMethodList :: ";
      LOGGER.verbose(logInfo + "Begin");

      Document docGetShippingLevelFlagIp = GCXMLUtil
          .createDocument(GCConstants.SHIPPING_LEVEL_FLAG);
      Document docGetShippingLevelFlagOp = GCCommonUtil.invokeService(
          env, GCConstants.GC_GET_SHIPPING_LEVEL_FLAG,
          docGetShippingLevelFlagIp);
      // Remove digital delivery and employee pickup from shipping level flag op as it wont be
      // shown in the UI drop down
      // GCStore -154
      Element eleGetShippingLevelFlagOp = docGetShippingLevelFlagOp.getDocumentElement();

      NodeList nlShippingLevelFlag = eleGetShippingLevelFlagOp.getElementsByTagName(GCConstants.GC_SHIPPING_LEVEL_FLAG);
      List<Element> elemns = new ArrayList<Element>();
      for (int i = 0; i < nlShippingLevelFlag.getLength(); i++) {
        Element eleShippingLevelFlag = (Element) nlShippingLevelFlag.item(i);
        String sLevelOfService = eleShippingLevelFlag.getAttribute(GCConstants.LEVEL_OF_SERVICE);
        if (YFCCommon.equals(sLevelOfService, GCConstants.DIGITAL_DELIVERY)
            || YFCCommon.equals(sLevelOfService, GCConstants.EMPLOYEE_PICKUP)
            || YFCCommon.equals(sLevelOfService, GCConstants.FIFTY_ONE_GROUND)
            || YFCCommon.equals(sLevelOfService,GCConstants.STANDARD_INTL)) {
          elemns.add(eleShippingLevelFlag);
        }
      }
      for (Element elem : elemns) {
        eleGetShippingLevelFlagOp.removeChild(elem);
      }
      // End GCStore -154

      LOGGER.debug(logInfo + "docGetShippingLevelFlagOp\n"
          + GCXMLUtil.getXMLString(docGetShippingLevelFlagOp));

      Set<String> setAddressLevelAllowedLevelOfService = new HashSet<String>();
      Map<String, Set<String>> mapItemLevelAllowedLevelOfService = new HashMap<String, Set<String>>();

      // Below method will check the address level restriction
      checkAddressLevelRestriction(inDoc,
          setAddressLevelAllowedLevelOfService,
          docGetShippingLevelFlagOp);

      // Below method will check the item level restriction
      checkItemLevelRestriction(inDoc, mapItemLevelAllowedLevelOfService,
          docGetShippingLevelFlagOp);

      LOGGER.debug(logInfo + "setAddressLevelAllowedLevelOfService ::\n"
          + setAddressLevelAllowedLevelOfService);
      LOGGER.debug(logInfo + "mapItemLevelAllowedLevelOfService ::\n"
          + mapItemLevelAllowedLevelOfService);

      // Iterate through address level allowed level of service
      Iterator<String> iterator = setAddressLevelAllowedLevelOfService
          .iterator();

      while (iterator.hasNext()) {
        String sShippingMethod = iterator.next();
        boolean bShippingMethodApplicable = true;

        // Iterate through item level allowed level of service
        Iterator iteratorMap = mapItemLevelAllowedLevelOfService
            .entrySet().iterator();
        while (iteratorMap.hasNext()) {
          Map.Entry pairs = (Map.Entry) iteratorMap.next();
          Set<String> setItemLevelAllowedLevelOfService = (Set<String>) pairs
              .getValue();
          if (!(setItemLevelAllowedLevelOfService.isEmpty() || setItemLevelAllowedLevelOfService
              .contains(sShippingMethod))) {
            bShippingMethodApplicable = false;
            break;
          }
        }

        if (bShippingMethodApplicable) {
          Element eleShippingMethod = outDoc
              .createElement(GCConstants.ALLOWED_SHIPPING_METHOD);
          outDoc.getDocumentElement().appendChild(eleShippingMethod);
          eleShippingMethod.setAttribute(GCConstants.SHIPPING_METHOD,
              sShippingMethod);
          String sShippingMethodDesc = GCXMLUtil
              .getAttributeFromXPath(docGetShippingLevelFlagOp,
                  "GCShippingLevelFlagList/GCShippingLevelFlag[@LevelOfService='"
                      + sShippingMethod
                      + "']/@LevelOfServiceDescription");
          eleShippingMethod.setAttribute(
              GCConstants.SHIPPING_METHOD_DESCRIPTION,
              sShippingMethodDesc);
        }
      }
      LOGGER.verbose(logInfo + "End");
      return outDoc;
    } catch (Exception e) {
      LOGGER.error("The Exception is in getShippingMethodList method:", e);
      throw new GCException(e);
    }
  }

  /**
   * Below method will check the item level restriction
   *
   * @param mapItemLevelAllowedLevelOfService
   * @param docGetShippingLevelFlagOp
   * @throws Exception
   * @throws IllegalArgumentException
   */
  private void checkItemLevelRestriction(Document inDoc,
      Map<String, Set<String>> mapItemLevelAllowedLevelService,
      Document docGetShippingLevelFlagOp) throws GCException {
    try {
      final String logInfo = "GCApplyShippingMethodConstraints :: checkItemLevelRestriction :: ";
      LOGGER.verbose(logInfo + "Begin");

      NodeList nlOrderLine = XPathAPI
          .selectNodeList(inDoc, "//OrderLine");

      for (int i = 0; i < nlOrderLine.getLength(); i++) {

        Element eleOrderLine = (Element) nlOrderLine.item(i);
        String sPrimeLineNo = eleOrderLine
            .getAttribute(GCConstants.PRIME_LINE_NO);
        Document docOrderLine = GCXMLUtil.getDocument(eleOrderLine,
            true);
        String sGiftCertificateValid = GCConstants.NO;
        // check if item has gift certificate valid or not
        if (GCConstants.GIFT_CERTIFICATE_CODE.equals(GCXMLUtil
            .getAttributeFromXPath(docOrderLine, "//@ItemType"))) {
          sGiftCertificateValid = GCConstants.YES;
        }

        String sExtnIsTruckShip = GCXMLUtil.getAttributeFromXPath(
            docOrderLine, "//@ExtnIsTruckShip");

        String sOver70Pound = GCConstants.NO;
        String sUnitWeight = GCXMLUtil.getAttributeFromXPath(
            docOrderLine, "//@UnitWeight");
        // check if an item weighs more than 70 pound or not
        if (!YFCCommon.isVoid(sUnitWeight)
            && 70 < Double.parseDouble(sUnitWeight)) {
          sOver70Pound = GCConstants.YES;
        }

        Set<String> setItemLevelAllowedLevelOfService = new HashSet<String>();
        // Apply gift certificate restriction
        if (GCConstants.YES.equals(sGiftCertificateValid)) {
          NodeList nlGetItemShippingLevelFlag = XPathAPI
              .selectNodeList(docGetShippingLevelFlagOp,
                  "//GCShippingLevelFlag"
                      + "[@GiftCertificateValid='"
                      + sGiftCertificateValid + "']");

          for (int iItemShippingLevelCounter = 0; iItemShippingLevelCounter < nlGetItemShippingLevelFlag
              .getLength(); iItemShippingLevelCounter++) {
            Element eleGetItemShippingLevelFlag = (Element) nlGetItemShippingLevelFlag
                .item(iItemShippingLevelCounter);
            String sLevelOfService = eleGetItemShippingLevelFlag
                .getAttribute(GCConstants.LEVEL_OF_SERVICE);
            setItemLevelAllowedLevelOfService.add(sLevelOfService);
          }
        } else if (GCConstants.YES.equals(sExtnIsTruckShip)) {
          // Apply truck ship restriction
          NodeList nlGetItemShippingLevelFlag = XPathAPI
              .selectNodeList(docGetShippingLevelFlagOp,
                  "//GCShippingLevelFlag"
                      + "[@AllowTruckItem='"
                      + sExtnIsTruckShip + "']");

          for (int iItemShippingLevelCounter = 0; iItemShippingLevelCounter < nlGetItemShippingLevelFlag
              .getLength(); iItemShippingLevelCounter++) {
            Element eleGetItemShippingLevelFlag = (Element) nlGetItemShippingLevelFlag
                .item(iItemShippingLevelCounter);
            String sLevelOfService = eleGetItemShippingLevelFlag
                .getAttribute(GCConstants.LEVEL_OF_SERVICE);
            setItemLevelAllowedLevelOfService.add(sLevelOfService);
          }
        } else if (GCConstants.YES.equals(sOver70Pound)) {
          // Apply over 70 pound restriction
          NodeList nlGetItemShippingLevelFlag = XPathAPI
              .selectNodeList(docGetShippingLevelFlagOp,
                  "//GCShippingLevelFlag" + "[@Over70Pound='"
                      + sOver70Pound + "']");

          for (int iItemShippingLevelCounter = 0; iItemShippingLevelCounter < nlGetItemShippingLevelFlag
              .getLength(); iItemShippingLevelCounter++) {
            Element eleGetItemShippingLevelFlag = (Element) nlGetItemShippingLevelFlag
                .item(iItemShippingLevelCounter);
            String sLevelOfService = eleGetItemShippingLevelFlag
                .getAttribute(GCConstants.LEVEL_OF_SERVICE);
            setItemLevelAllowedLevelOfService.add(sLevelOfService);
          }

        }else{
          NodeList nlShippingLevelFlag = XPathAPI.selectNodeList(
              docGetShippingLevelFlagOp, "//GCShippingLevelFlag");
          for (int iItemShippingLevelCounter = 0; iItemShippingLevelCounter < nlShippingLevelFlag
              .getLength(); iItemShippingLevelCounter++) {
            Element eleShippingLevelFlag = (Element) nlShippingLevelFlag
                .item(iItemShippingLevelCounter);
            setItemLevelAllowedLevelOfService
            .add(eleShippingLevelFlag
                .getAttribute(GCConstants.LEVEL_OF_SERVICE));
          }
        }
        if (!setItemLevelAllowedLevelOfService.isEmpty()) {
          mapItemLevelAllowedLevelService.put(sPrimeLineNo,
              setItemLevelAllowedLevelOfService);
        }
      }
      LOGGER.verbose(logInfo + "End");
    } catch (Exception e) {
      LOGGER.error(
          "The Exception is in checkItemLevelRestriction method:", e);
      throw new GCException(e);
    }
  }

  /**
   * Below method will check the address level restriction
   *
   * @param setAllowedLevelOfService
   * @param docGetShippingLevelFlagOp
   * @throws Exception
   */
  private void checkAddressLevelRestriction(Document inDoc,
      Set<String> setAddressLevelAllowedLevelOfService,
      Document docGetShippingLevelFlagOp) throws GCException {
    try {
      final String logInfo = "GCApplyShippingMethodConstraints :: checkAddressLevelRestriction :: ";
      LOGGER.verbose(logInfo + "Begin");

      String sIsPOBoxValid = GCXMLUtil.getAttributeFromXPath(inDoc,
          "//PersonInfoShipTo/Extn/@ExtnIsPOBox");
      String sIsAPOFPO = GCXMLUtil.getAttributeFromXPath(inDoc,
          "//PersonInfoShipTo/Extn/@ExtnIsAPOFPO");
      String sState = GCXMLUtil.getAttributeFromXPath(inDoc,
          "//PersonInfoShipTo/@State");
      String sIsValidUSTerritory = checkState(sState);
      String sHawaiAlaskaValid = GCConstants.NO;
      if (GCConstants.ALASKA_STATE.equalsIgnoreCase(sState)
          || GCConstants.HAWAI_STATE.equalsIgnoreCase(sState)) {
        sHawaiAlaskaValid = GCConstants.YES;
      }

      // Apply PO box restriction
      if (GCConstants.YES.equals(sIsPOBoxValid)) {
        NodeList nlGetShippingLevelFlag = XPathAPI.selectNodeList(
            docGetShippingLevelFlagOp,
            "//GCShippingLevelFlag[@POBoxValid='" + sIsPOBoxValid
            + "']");

        for (int iShippingLevelCounter = 0; iShippingLevelCounter < nlGetShippingLevelFlag
            .getLength(); iShippingLevelCounter++) {

          Element eleGetShippingLevelFlag = (Element) nlGetShippingLevelFlag
              .item(iShippingLevelCounter);
          String sLevelOfService = eleGetShippingLevelFlag
              .getAttribute(GCConstants.LEVEL_OF_SERVICE);
          setAddressLevelAllowedLevelOfService.add(sLevelOfService);
        }
      } else if (GCConstants.YES.equals(sIsAPOFPO)) {
        // Apply apo fpo restriction
        NodeList nlGetShippingLevelFlag = XPathAPI.selectNodeList(
            docGetShippingLevelFlagOp,
            "//GCShippingLevelFlag[@ApoFpoValid='" + sIsAPOFPO
            + "']");

        for (int iShippingLevelCounter = 0; iShippingLevelCounter < nlGetShippingLevelFlag
            .getLength(); iShippingLevelCounter++) {

          Element eleGetShippingLevelFlag = (Element) nlGetShippingLevelFlag
              .item(iShippingLevelCounter);
          String sLevelOfService = eleGetShippingLevelFlag
              .getAttribute(GCConstants.LEVEL_OF_SERVICE);
          setAddressLevelAllowedLevelOfService.add(sLevelOfService);
        }
      } else if (GCConstants.YES.equals(sIsValidUSTerritory)) {
        // Apply valid US territory restriction
        NodeList nlGetShippingLevelFlag = XPathAPI.selectNodeList(
            docGetShippingLevelFlagOp,
            "//GCShippingLevelFlag[@USTerritoriesValid='"
                + sIsValidUSTerritory + "']");

        for (int iShippingLevelCounter = 0; iShippingLevelCounter < nlGetShippingLevelFlag
            .getLength(); iShippingLevelCounter++) {

          Element eleGetShippingLevelFlag = (Element) nlGetShippingLevelFlag
              .item(iShippingLevelCounter);
          String sLevelOfService = eleGetShippingLevelFlag
              .getAttribute(GCConstants.LEVEL_OF_SERVICE);
          setAddressLevelAllowedLevelOfService.add(sLevelOfService);
        }
      } else if (GCConstants.YES.equals(sHawaiAlaskaValid)) {
        // Apply Hawai Alaska restriction
        NodeList nlGetShippingLevelFlag = XPathAPI.selectNodeList(
            docGetShippingLevelFlagOp,
            "//GCShippingLevelFlag[@HawaiAlaskaValid='"
                + sHawaiAlaskaValid + "']");

        for (int iShippingLevelCounter = 0; iShippingLevelCounter < nlGetShippingLevelFlag
            .getLength(); iShippingLevelCounter++) {

          Element eleGetShippingLevelFlag = (Element) nlGetShippingLevelFlag
              .item(iShippingLevelCounter);
          String sLevelOfService = eleGetShippingLevelFlag
              .getAttribute(GCConstants.LEVEL_OF_SERVICE);
          setAddressLevelAllowedLevelOfService.add(sLevelOfService);
        }

      } else {
        NodeList nlShippingLevelFlag = XPathAPI.selectNodeList(
            docGetShippingLevelFlagOp, "//GCShippingLevelFlag");
        for (int i = 0; i < nlShippingLevelFlag.getLength(); i++) {
          Element eleShippingLevelFlag = (Element) nlShippingLevelFlag
              .item(i);
          setAddressLevelAllowedLevelOfService
          .add(eleShippingLevelFlag
              .getAttribute(GCConstants.LEVEL_OF_SERVICE));
        }
      }
      LOGGER.verbose(logInfo + "End");
    } catch (Exception e) {
      LOGGER.error(
          "The Exception is in checkAddressLevelRestriction method:",
          e);
      throw new GCException(e);
    }
  }

  /**
   *
   * @param env
   * @param sState
   * @return
   * @throws Exception
   */
  private String checkState(String sState) throws GCException {
    try {
      final String logInfo = "GCApplyShippingMethodConstraints :: checkState :: ";
      LOGGER.verbose(logInfo + "Begin");
      String sIsValidUSTerritory = "";
      String sCodeLongDesc = "";
      Document docGetCommonCodeListIp = GCXMLUtil
          .createDocument(GCConstants.COMMON_CODE);
      Element eleGetCommonCodeListIpRoot = docGetCommonCodeListIp
          .getDocumentElement();
      eleGetCommonCodeListIpRoot.setAttribute(GCConstants.CODE_TYPE,
          "US_TERRITORY");
      eleGetCommonCodeListIpRoot.setAttribute(GCConstants.CODE_VALUE,
          sState);
      LOGGER.debug(logInfo + "docGetCommonCodeListIp\n"
          + GCXMLUtil.getXMLString(docGetCommonCodeListIp));
      Document docGetCommonCodeListOp = GCCommonUtil.invokeAPI(env,
          GCConstants.GET_COMMON_CODE_LIST, docGetCommonCodeListIp);
      LOGGER.debug(logInfo + "docGetCommonCodeListOp\n"
          + GCXMLUtil.getXMLString(docGetCommonCodeListOp));
      Element eleCommonCode = (Element) XPathAPI.selectSingleNode(
          docGetCommonCodeListOp, "//CommonCode");
      if (!YFCCommon.isVoid(eleCommonCode)) {
        sIsValidUSTerritory = eleCommonCode.getAttribute(GCConstants.CODE_LONG_DESCRIPTION);
        String sUSTerritoryFlag = eleCommonCode.getAttribute(GCConstants.CODE_SHORT_DESCRIPTION);
        if(YFCCommon.equals(sUSTerritoryFlag, GCConstants.FLAG_Y))
        	sCodeLongDesc =  sIsValidUSTerritory;
        else
        	sCodeLongDesc = "";
      }
      LOGGER.verbose(logInfo + "End");
      return sCodeLongDesc;
    } catch (Exception e) {
      LOGGER.error("The Exception is in checkState method:", e);
      throw new GCException(e);
    }
  }

  /**
   *
   * @param env
   * @param inDoc
   * @param outDoc
   */
  private void checkGiftItem(Document inDoc, Document outDoc){
    LOGGER.beginTimer("GCApplyShippingMethodConstraints.checkGiftItem");
    LOGGER.verbose("Class: GCApplyShippingMethodConstraints Method: checkGiftItem BEGIN");

    YFCDocument outputDoc=YFCDocument.getDocumentFor(outDoc);
    YFCDocument inputDoc=YFCDocument.getDocumentFor(inDoc);
    YFCElement eleRoot= inputDoc.getDocumentElement();
    YFCElement eleOrderLines=eleRoot.getChildElement(GCConstants.ORDER_LINES);

    YFCNodeList<YFCElement> listOrderLine=eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
    boolean flag=false;
    for(YFCElement eleOrderLine:listOrderLine){
      YFCElement eleItemDetails=eleOrderLine.getChildElement(GCConstants.ITEM_DETAILS);
      YFCElement elePrimaryInfo=eleItemDetails.getChildElement(GCConstants.PRIMARY_INFORMATION);
      String sItemType=elePrimaryInfo.getAttribute(GCConstants.ITEM_TYPE);

      if(YFCCommon.equals(sItemType,"04")){
        flag=true;
        YFCElement eleOutDocRoot=outputDoc.getDocumentElement();
        YFCNodeList<YFCElement> listAllowedShippingMethod= eleOutDocRoot.getElementsByTagName(GCConstants.ALLOWED_SHIPPING_METHOD);

        for(YFCElement eleAllowedShippingMethod:listAllowedShippingMethod){
          String sShippingMethod=eleAllowedShippingMethod.getAttribute(GCConstants.SHIPPING_METHOD);
          if(YFCCommon.equals(sShippingMethod,GCConstants.NEXT_DAY_AIR)||YFCCommon.equals(sShippingMethod,GCConstants.SECOND_DAY_EXPRESS)){
            eleOutDocRoot.removeChild(eleAllowedShippingMethod);
          }
        }
      }
      if(YFCCommon.equals(flag,GCConstants.TRUE)){
        break;
      }
    }
    LOGGER.endTimer("GCApplyShippingMethodConstraints.checkGiftItem");
  }
}
