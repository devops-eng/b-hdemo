/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class is used to apply Tax Exempt Hold
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               06/02/2014        Gowda, Naveen		JIRA No: This class is used to apply Tax Exempt Hold.
 *#################################################################################################################################################################
 */
package com.gc.api;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:email Address">Naveen</a>
 */
public class GCApplyTaxExemptHold {
    /**
     * 
     * Description of applyTaxExemptHold
     * 
     * @param env
     * @param inDoc
     * @throws Exception
     * 
     */

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCManageCustomer.class.getName());

    public void applyTaxExemptHold(YFSEnvironment env, Document inDoc)
	    throws GCException {
    	
    	//Fix to avoid Tax Exempt hold on case of mPOS orders.
    	Element eleInDocRoot = inDoc.getDocumentElement();
    	Element eleInDocExtn = (Element) eleInDocRoot.getElementsByTagName(GCConstants.EXTN).item(0);
	    String sOrderCaptureChannel = eleInDocExtn.getAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL);
	    if(!YFCCommon.equalsIgnoreCase(sOrderCaptureChannel, GCConstants.MPOS_ORDER_CHANNEL))
		{
		try {
		    LOGGER.debug("Class: GCApplyTaxExemptHold Method: applyTaxExemptHold BEGIN");
	
		    if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(" applyTaxExemptHold() method, Input document is"
				+ GCXMLUtil.getXMLString(inDoc));
		    }
		    
		    String sBillToID = GCXMLUtil.getAttributeFromXPath(inDoc,
			    "Order/@BillToID");
		    String sOrderHeaderKey = GCXMLUtil.getAttributeFromXPath(inDoc,
			    "Order/@OrderHeaderKey");
	
		    Document docGetCustListIn = GCXMLUtil
			    .createDocument(GCConstants.CUSTOMER);
		    Element eleInputRoot = docGetCustListIn.getDocumentElement();
		    eleInputRoot.setAttribute(GCConstants.CUSTOMER_ID, sBillToID);
	
		    // Template
		    Document docGetCustListTemplate = GCXMLUtil
			    .createDocument(GCConstants.CUSTOMER_LIST);
		    Element eleRootTemplate = docGetCustListTemplate
			    .getDocumentElement();
		    Element eleCustomer = docGetCustListTemplate
			    .createElement(GCConstants.CUSTOMER);
		    eleRootTemplate.appendChild(eleCustomer);
		    Element eleExtn = docGetCustListTemplate
			    .createElement(GCConstants.EXTN);
		    eleCustomer.appendChild(eleExtn);
		    if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("applyTaxExemptHold() method - Input Document for getCustomerList"
				+ GCXMLUtil.getXMLString(docGetCustListIn));
		    }
		    Document docGetCustListOut = GCCommonUtil.invokeAPI(env,
			    GCConstants.GET_CUSTOMER_LIST, docGetCustListIn,
			    docGetCustListTemplate);
		    if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("applyTaxExemptHold() method - Out Document for getCustomerList"
				+ GCXMLUtil.getXMLString(docGetCustListOut));
		    }
		    String sExtnTaxExemptReqStatus = GCXMLUtil.getAttributeFromXPath(
			    docGetCustListOut,
			    "//Customer/Extn/@ExtnTaxExemptReqStatus");
		    if (GCConstants.IN_PROGRESS
			    .equalsIgnoreCase(sExtnTaxExemptReqStatus)) {
	
			Document docChangeOrderIn = GCXMLUtil
				.createDocument(GCConstants.ORDER);
			Element eleOrder = docChangeOrderIn.getDocumentElement();
			eleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY,
				sOrderHeaderKey);
	
			Element eleOrderHoldTypes = docChangeOrderIn
				.createElement(GCConstants.ORDER_HOLD_TYPES);
			Element eleOrderholdType = docChangeOrderIn
				.createElement(GCConstants.ORDER_HOLD_TYPE);
			eleOrderholdType.setAttribute(GCConstants.HOLD_TYPE,
				GCConstants.TAX_EXEMPT_HOLD);
			eleOrderholdType.setAttribute(GCConstants.REASON_TEXT,
				GCConstants.TAX_EXEMPT);
			eleOrderholdType.setAttribute(GCConstants.STATUS,
				GCConstants.APPLY_HOLD_CODE);
			eleOrderHoldTypes.appendChild(eleOrderholdType);
			eleOrder.appendChild(eleOrderHoldTypes);
			if (LOGGER.isDebugEnabled()) {
			    LOGGER.debug("applyTaxExemptHold() - Input Document before change Order"
				    + GCXMLUtil.getXMLString(docChangeOrderIn));
			}
			GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER,
				docChangeOrderIn);
			LOGGER.debug("Class: GCApplyTaxExemptHold Method: applyTaxExemptHold END");
	
		    }
		} catch (Exception e) {
		    LOGGER.error("The exception is in applyTaxExemptHold method:", e);
		    throw new GCException(e);
		}
		}
    }

}
