/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class checks the condition to apply Buyer Remorse Hold on Order or not
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               21/05/2014        Soni, Karan		      Initial Version
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.util.YFCUtils;
import com.yantra.ycp.japi.YCPDynamicConditionEx;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author karan
 */
public class GCBuyerRemorseHoldCondition implements YCPDynamicConditionEx {

    // initiaize Logger
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCBuyerRemorseHoldCondition.class);
    @Override
    public boolean evaluateCondition(YFSEnvironment env, String strInput,
	    Map mapInput, Document inDoc) {
	boolean returnFlag = false;
	try {
	    LOGGER.verbose("Entering the evaluateCondition method of BuyerRemorseHold Condition class");
	    Element eleRoot = inDoc.getDocumentElement();
	    String strExpediteFlag = GCXMLUtil.getAttributeFromXPath(eleRoot,
		    "Extn/@ExtnExpediteFlag");
	    String levelOfService = eleRoot.getAttribute(GCConstants.LEVEL_OF_SERVICE);
	    String strSellerOrg = eleRoot
		    .getAttribute(GCConstants.SELLER_ORGANIZATION_CODE);
	    String strOrderPurpose = eleRoot
		    .getAttribute(GCConstants.ORDER_PURPOSE);
	    LOGGER.debug("The Seller Organization is" + strSellerOrg
		    + "The Order Purpose is" + strOrderPurpose
		    + "The ExtnExpediteFlag is " + strExpediteFlag);
	    
	     // Fix to avoid Buyer Remorse Hold on mPOS Orders
        Element eleInputExtn = (Element) eleRoot.getElementsByTagName(GCConstants.EXTN).item(0);
        String sExtnOrderCaptureChannel = eleInputExtn.getAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL);
        
        if(sExtnOrderCaptureChannel.equalsIgnoreCase(GCConstants.MPOS_ORDER_CHANNEL)){
          return false;
        }
        
	    if (!GCConstants.YES.equals(strExpediteFlag)
		    && !GCConstants.INTL_WEB.equals(strSellerOrg)
		    && YFCCommon.isVoid(strOrderPurpose) && !YFCUtils.equals(GCConstants.STORE_PICKUP, levelOfService)) {
		LOGGER.debug("Entering if condition satisfy");
		returnFlag = true;
		LOGGER.debug("Exiting if condition satisfy");
	    } else {
		returnFlag = false;
	    }
	    LOGGER.debug("The final returned flag is" + returnFlag);
	    LOGGER.verbose("Exiting the evaluateCondition method of BuyerRemorseHold Condition class");
	} catch (Exception e) {
	    LOGGER.error("Inside GCBuyerRemorseHoldCondition.evaluateCondition():",e);
	}
	return returnFlag;
    }

    @Override
    public void setProperties(Map arg0) {
    }

}
