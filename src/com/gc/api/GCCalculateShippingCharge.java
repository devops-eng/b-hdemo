/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class is required to calculate shipping charges manually.
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               06/03/2014        Gowda, Naveen		JIRA No: This class is used to Calculate Shipping Charge manually in case ATG is down. 
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.math.BigDecimal;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:email Address">Name</a>
 */
public class GCCalculateShippingCharge {

    /**
     * 
     * Description of calShipCharge - This method is used to Calculate Shipping
     * Charge manually in case ATG is down.
     * 
     * @param env
     * @param inDoc
     * @throws Exception
     * 
     */
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCCalculateShippingCharge.class.getName());

    public void calShipCharge(YFSEnvironment env, Document docChangeOrder)
	    throws GCException {
	try {
	    LOGGER.debug("Class: GCCalculateShippingCharge Method: calShipCharge BEGIN");
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("Incoming XML"
			+ GCXMLUtil.getXMLString(docChangeOrder));
	    }
	    Element eleCORoot = docChangeOrder.getDocumentElement();
	    String sOrderHeaderKey = GCXMLUtil.getAttributeFromXPath(
		    docChangeOrder, "//Order/@OrderHeaderKey");

	    String sLevOfSer = GCXMLUtil.getAttributeFromXPath(docChangeOrder,
		    "//Order/@LevelOfService");
	    String sGrandTotal = GCXMLUtil.getAttributeFromXPath(
		    docChangeOrder, "//OverallTotals/@GrandTotal");
	    Element eleCOOrder = GCXMLUtil.getElementByXPath(docChangeOrder,
		    "//Order");
	    Element eleOverallTotalls = GCXMLUtil.getElementByXPath(
		    docChangeOrder, "//OverallTotals");
	    eleCOOrder.removeChild(eleOverallTotalls);

	    Double dGrandTotal = Double.parseDouble(sGrandTotal);
	    LOGGER.debug("dGrandTotal" + dGrandTotal);
	    Double dShipChrgAmt = 0.00;
	    String sShippingMethod = "";

	    Element eleOrderExtn = GCXMLUtil.getElementByXPath(docChangeOrder,
		    "//Order/Extn");
	    eleOrderExtn.setAttribute(GCConstants.EXTN_IS_SHIPPING_PRORATED,
		    GCConstants.FLAG_N);
	    eleOrderExtn.setAttribute("ExtnIsATGCallRequired",
		    GCConstants.FLAG_N);

	    /*
	     * In case ATG is down storing shipping charges for all Level Of
	     * Services.
	     */
	    calculateAndStoreShippingChargeForAllLOS(env, docChangeOrder,
		    eleOrderExtn, sOrderHeaderKey, dGrandTotal);

	    if ((GCConstants.EMPLOYEE_PICKUP).equalsIgnoreCase(sLevOfSer)
		    || (GCConstants.DIGITAL_DELIVERY)
			    .equalsIgnoreCase(sLevOfSer)) {
		dShipChrgAmt = 0.00;
	    }
	    if ((GCConstants.SECOND_DAY_EXPRESS).equalsIgnoreCase(sLevOfSer)) {
		sShippingMethod = GCConstants.SECOND_DAY_EXPRESS;
		dShipChrgAmt = fetchShippingChargeAmount(env, dGrandTotal,
			sShippingMethod);
	    }
	    if ((GCConstants.ECONOMY_GROUND).equalsIgnoreCase(sLevOfSer)
		    || (GCConstants.VENDOR_DROP).equalsIgnoreCase(sLevOfSer)
		    || (GCConstants.STANDARD_GROUND)
			    .equalsIgnoreCase(sLevOfSer)) {
		sShippingMethod = GCConstants.STANDARD_GROUND;
		dShipChrgAmt = fetchShippingChargeAmount(env, dGrandTotal,
			sShippingMethod);
		if ((GCConstants.STANDARD_GROUND).equalsIgnoreCase(sLevOfSer)) {
		    dShipChrgAmt = dShipChrgAmt + 4.99;
		}

	    }

	    if ((GCConstants.NEXT_DAY_AIR).equalsIgnoreCase(sLevOfSer)) {
		sShippingMethod = GCConstants.NEXT_DAY_AIR;
		dShipChrgAmt = fetchShippingChargeAmount(env, dGrandTotal,
			sShippingMethod);

	    }
	    String sShippingAmount = dShipChrgAmt.toString();
	    BigDecimal a = new BigDecimal(sShippingAmount);
	    BigDecimal bROUnitPrice = a.setScale(2, BigDecimal.ROUND_HALF_EVEN);
	    sShippingAmount = bROUnitPrice.toString();

	    Element eleHeaderCharges = docChangeOrder
		    .createElement(GCConstants.HEADER_CHARGES);
	    Element eleHeaderCharge = docChangeOrder
		    .createElement(GCConstants.HEADER_CHARGE);
	    eleHeaderCharge.setAttribute(GCConstants.CHARGE_AMOUNT,
		    sShippingAmount);
	    eleHeaderCharge.setAttribute(GCConstants.CHARGE_CATEGORY,
		    GCConstants.SHIPPING);
	    eleHeaderCharge.setAttribute(GCConstants.CHARGE_NAME,
		    GCConstants.SHIPPING_CHARGE);
	    eleHeaderCharges.appendChild(eleHeaderCharge);
	    eleCORoot.appendChild(eleHeaderCharges);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("Change Order In doc"
			+ GCXMLUtil.getXMLString(docChangeOrder));
	    }
	    LOGGER.debug("Class: GCCalculateShippingCharge Method: calShipCharge END");
	} catch (Exception e) {
	    LOGGER.error("The exception is in calShipCharge method:", e);
	    throw new GCException(e);
	}
    }

    /**
     * 
     * Description of calculateAndStoreShippingChargeForAllLOS
     * 
     * @param env
     * 
     * @param docChangeOrderIn
     * @param eleOrderExtn
     * @param sOrderHeaderKey
     * @param dGrandTotal
     * @param dShipChrgAmt
     * @throws Exception
     * 
     */
    private void calculateAndStoreShippingChargeForAllLOS(YFSEnvironment env,
	    Document docChangeOrder, Element eleOrderExtn,
	    String sOrderHeaderKey, Double dGrandTotal)
	    throws GCException {
	try {
	    LOGGER.debug("Class: GCCalculateShippingCharge Method: calculateAndStoreShippingChargeForAllLOS BEGIN");

	    Element eleOrderShippingChargeList = docChangeOrder
		    .createElement(GCConstants.GC_ORDER_SHIPPING_CHARGE_LIST);

	    /* For Employee Pickup */
	    Element eleOrderShipCharEP = docChangeOrder
		    .createElement(GCConstants.GC_ORDER_SHIPPING_CHARGE);

	    eleOrderShipCharEP.setAttribute(GCConstants.ORDER_HEADER_KEY,
		    sOrderHeaderKey);
	    eleOrderShipCharEP.setAttribute(GCConstants.LEVEL_OF_SERVICE,
		    GCConstants.EMPLOYEE_PICKUP);
	    eleOrderShipCharEP.setAttribute(GCConstants.SHIPPING_CHARGE_AMOUNT,
		    "0.00");
	    String sATGServiceCode = fetchShippingMethodUsingLOS(env,
		    GCConstants.EMPLOYEE_PICKUP);
	    eleOrderShipCharEP.setAttribute(GCConstants.ATG_SERVICE_CODE,
		    sATGServiceCode);
	    eleOrderShippingChargeList.appendChild(eleOrderShipCharEP);

	    /* For Digital Delivery */
	    Element eleOrderShipCharDD = docChangeOrder
		    .createElement(GCConstants.GC_ORDER_SHIPPING_CHARGE);

	    eleOrderShipCharDD.setAttribute(GCConstants.ORDER_HEADER_KEY,
		    sOrderHeaderKey);
	    eleOrderShipCharDD.setAttribute(GCConstants.LEVEL_OF_SERVICE,
		    GCConstants.DIGITAL_DELIVERY);
	    sATGServiceCode = fetchShippingMethodUsingLOS(env,
		    GCConstants.DIGITAL_DELIVERY);
	    eleOrderShipCharDD.setAttribute(GCConstants.SHIPPING_CHARGE_AMOUNT,
		    "0.00");
	    eleOrderShipCharDD.setAttribute(GCConstants.ATG_SERVICE_CODE,
		    sATGServiceCode);
	    eleOrderShippingChargeList.appendChild(eleOrderShipCharDD);

	    /* Second Day Express */
	    Element eleOrderShipCharSDE = docChangeOrder
		    .createElement(GCConstants.GC_ORDER_SHIPPING_CHARGE);

	    eleOrderShipCharSDE.setAttribute(GCConstants.ORDER_HEADER_KEY,
		    sOrderHeaderKey);
	    eleOrderShipCharSDE.setAttribute(GCConstants.LEVEL_OF_SERVICE,
		    GCConstants.SECOND_DAY_EXPRESS);
	    sATGServiceCode = fetchShippingMethodUsingLOS(env,
		    GCConstants.SECOND_DAY_EXPRESS);
	    Double dShippingChargeAmount = fetchShippingChargeAmount(env,
		    dGrandTotal, GCConstants.SECOND_DAY_EXPRESS);
	    String sShippingChargeAmount = dShippingChargeAmount.toString();
	    eleOrderShipCharSDE.setAttribute(
		    GCConstants.SHIPPING_CHARGE_AMOUNT, sShippingChargeAmount);
	    eleOrderShipCharSDE.setAttribute(GCConstants.ATG_SERVICE_CODE,
		    sATGServiceCode);
	    eleOrderShippingChargeList.appendChild(eleOrderShipCharSDE);

	    /* Next Day Air */
	    Element eleOrderShipCharNDA = docChangeOrder
		    .createElement(GCConstants.GC_ORDER_SHIPPING_CHARGE);

	    eleOrderShipCharNDA.setAttribute(GCConstants.ORDER_HEADER_KEY,
		    sOrderHeaderKey);
	    eleOrderShipCharNDA.setAttribute(GCConstants.LEVEL_OF_SERVICE,
		    GCConstants.NEXT_DAY_AIR);
	    sATGServiceCode = fetchShippingMethodUsingLOS(env,
		    GCConstants.NEXT_DAY_AIR);
	    dShippingChargeAmount = fetchShippingChargeAmount(env, dGrandTotal,
		    GCConstants.NEXT_DAY_AIR);
	    sShippingChargeAmount = dShippingChargeAmount.toString();
	    eleOrderShipCharNDA.setAttribute(
		    GCConstants.SHIPPING_CHARGE_AMOUNT, sShippingChargeAmount);
	    eleOrderShipCharNDA.setAttribute(GCConstants.ATG_SERVICE_CODE,
		    sATGServiceCode);
	    eleOrderShippingChargeList.appendChild(eleOrderShipCharNDA);

	    /* Economy Ground */
	    Element eleOrderShipCharEG = docChangeOrder
		    .createElement(GCConstants.GC_ORDER_SHIPPING_CHARGE);

	    eleOrderShipCharEG.setAttribute(GCConstants.ORDER_HEADER_KEY,
		    sOrderHeaderKey);
	    eleOrderShipCharEG.setAttribute(GCConstants.LEVEL_OF_SERVICE,
		    GCConstants.ECONOMY_GROUND);
	    sATGServiceCode = fetchShippingMethodUsingLOS(env,
		    GCConstants.ECONOMY_GROUND);
	    /*
	     * For Economy Ground Shipping Charge Search table for Standard
	     * Ground itself. There is no Shipping Charge stored separately for
	     * Economy Ground in the table
	     */
	    dShippingChargeAmount = fetchShippingChargeAmount(env, dGrandTotal,
		    GCConstants.STANDARD_GROUND);
	    sShippingChargeAmount = dShippingChargeAmount.toString();
	    eleOrderShipCharEG.setAttribute(GCConstants.SHIPPING_CHARGE_AMOUNT,
		    sShippingChargeAmount);
	    eleOrderShipCharEG.setAttribute(GCConstants.ATG_SERVICE_CODE,
		    sATGServiceCode);
	    eleOrderShippingChargeList.appendChild(eleOrderShipCharEG);

	    /* Standard Ground */
	    Element eleOrderShipCharSG = docChangeOrder
		    .createElement(GCConstants.GC_ORDER_SHIPPING_CHARGE);

	    eleOrderShipCharSG.setAttribute(GCConstants.ORDER_HEADER_KEY,
		    sOrderHeaderKey);
	    eleOrderShipCharSG.setAttribute(GCConstants.LEVEL_OF_SERVICE,
		    GCConstants.STANDARD_GROUND);
	    sATGServiceCode = fetchShippingMethodUsingLOS(env,
		    GCConstants.STANDARD_GROUND);
	    /* Standard Ground = Economy Ground + 4.99 */
	    dShippingChargeAmount = dShippingChargeAmount + 4.99;
	    sShippingChargeAmount = dShippingChargeAmount.toString();
	    BigDecimal a = new BigDecimal(sShippingChargeAmount);
	    BigDecimal bROUnitPrice = a.setScale(2, BigDecimal.ROUND_HALF_EVEN);
	    sShippingChargeAmount = bROUnitPrice.toString();
	    eleOrderShipCharSG.setAttribute(GCConstants.SHIPPING_CHARGE_AMOUNT,
		    sShippingChargeAmount);
	    eleOrderShipCharSG.setAttribute(GCConstants.ATG_SERVICE_CODE,
		    sATGServiceCode);
	    eleOrderShippingChargeList.appendChild(eleOrderShipCharSG);

	    eleOrderExtn.appendChild(eleOrderShippingChargeList);

	    LOGGER.debug("Class: GCCalculateShippingCharge Method: calculateAndStoreShippingChargeForAllLOS END");
	} catch (Exception e) {
	    LOGGER.error(
		    "The exception is in calculateAndStoreShippingChargeForAllLOS method:",
		    e);
	    throw new GCException(e);
	}
    }

    /**
     * 
     * Description of fetchShippingMethodUsingLOS
     * 
     * @param env
     * @param sLevelOfService
     * @return
     * @throws Exception
     * 
     */
    public String fetchShippingMethodUsingLOS(YFSEnvironment env,
	    String sLevelOfService) throws GCException {
	try {
	    LOGGER.debug("Class: GCCalculateShippingCharge Method: fetchShippingMethodUsingLOS BEGIN");

	    Document docGetCommonCodeIn = GCXMLUtil
		    .createDocument(GCConstants.COMMON_CODE);
	    Element eleRoot = docGetCommonCodeIn.getDocumentElement();
	    eleRoot.setAttribute(GCConstants.CODE_TYPE,
		    GCConstants.ATG_SHIPPING_MTHD);
	    eleRoot.setAttribute(GCConstants.CODE_VALUE, sLevelOfService);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("Input Doc to getCommonCodeList"
			+ GCXMLUtil.getXMLString(docGetCommonCodeIn));
	    }
	    Document docGetCommonCodeOut = GCCommonUtil.invokeAPI(env,
		    GCConstants.GET_COMMON_CODE_LIST, docGetCommonCodeIn);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("OutDoc to getCommonCodeList"
			+ GCXMLUtil.getXMLString(docGetCommonCodeOut));
	    }
	    String sShippingMehtod = GCXMLUtil.getAttributeFromXPath(
		    docGetCommonCodeOut, "//@CodeShortDescription");
	    LOGGER.debug("Class: GCCalculateShippingCharge Method: fetchShippingMethodUsingLOS END");

	    return sShippingMehtod;
	} catch (Exception e) {
	    LOGGER.error(
		    "The exception is in fetchShippingMethodUsingLOS method:",
		    e);
	    throw new GCException(e);
	}
    }

    /**
     * 
     * Description of fetchShippingChargeAmount
     * 
     * @param env
     * @param dGrandTotal
     * @param dShipChrgAmt
     * @param sShippingMethod
     * @return
     * @throws Exception
     * 
     */

    private Double fetchShippingChargeAmount(YFSEnvironment env,
	    Double dGrandTotal, String sShippingMethod) throws GCException {
	try {
	    LOGGER.debug("Class: GCCalculateShippingCharge Method: fetchShippingChargeAmount BEGIN");
	    double dShipNewChrgAmt = 0.0;
	    Document docShipChargIn = GCXMLUtil
		    .createDocument(GCConstants.GC_SHIPPING_CHARGE);
	    Element eleRoot = docShipChargIn.getDocumentElement();
	    eleRoot.setAttribute(GCConstants.SHIPPING_METHOD, sShippingMethod);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("GCGetShippingChargeService Input"
			+ GCXMLUtil.getXMLString(docShipChargIn));
	    }
	    Document docShipChargeOut = GCCommonUtil.invokeService(env,
		    "GCGetShippingChargeService", docShipChargIn);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("GCGetShippingChargeService Output"
			+ GCXMLUtil.getXMLString(docShipChargeOut));
	    }
	    NodeList nLGCShippingCharge = GCXMLUtil.getNodeListByXpath(
		    docShipChargeOut, "/GCShippingChargeList/GCShippingCharge");
	    for (int i = 0; i < nLGCShippingCharge.getLength(); i++) {
		Element eleShipCharge = (Element) nLGCShippingCharge.item(i);
		String strOrderAmtFrom = eleShipCharge
			.getAttribute(GCConstants.ORDER_AMOUNT_FROM);
		Double dOrderAmtFrom = Double.parseDouble(strOrderAmtFrom);
		String strOrderAmtTo = eleShipCharge
			.getAttribute(GCConstants.ORDER_AMOUNT_TO);
		Double dOrderAmtTo = Double.parseDouble(strOrderAmtTo);

		if (dGrandTotal >= dOrderAmtFrom && dGrandTotal <= dOrderAmtTo) {
		    LOGGER.debug("Inside If");
		    String strShipChrgAmt = eleShipCharge
			    .getAttribute(GCConstants.SHIPPING_CHARGE_AMOUNT);
		    dShipNewChrgAmt = Double.parseDouble(strShipChrgAmt);
		}
	    }
	    LOGGER.debug("dShipChrgAmt:" + dShipNewChrgAmt);
	    LOGGER.debug("Class: GCCalculateShippingCharge Method: fetchShippingChargeAmount END");

	    return dShipNewChrgAmt;
	} catch (Exception e) {
	    LOGGER.error(
		    "The exception is in fetchShippingChargeAmount method:", e);
	    throw new GCException(e);
	}
    }
}