package com.gc.api;

import java.util.Properties;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.gc.eventhandler.GCProcessOrderOnSuccess;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
/**
 * @author <a href="mailto:rakesh.kumar@expicient.com">Rakesh</a>
 */
public class GCCallChangeOrder implements YIFCustomApi{

	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCProcessOrderOnSuccess.class);	
	/**
	 * 
	 * Description of callChangeOrder This method calls ChangeOrderAPI for adding notes to the order.
	 * For making change order input Xml it uses inDoc.
	 *
	 * @param env
	 * @param inDoc
	 * @return
	 * @throws GCException 
	 *
	 */
	public Document callChangeOrder(YFSEnvironment env, Document inDoc) throws GCException {
		LOGGER.info("Enter Inside GCCallChangeOrder.callChangeOrder()");
		Document changeOrderInDoc = GCXMLUtil.getDocument("<Order OrderHeaderKey=''> "
				+"<Notes> <Note NoteText=''></Note>"
				+"</Notes></Order>");

		try {
			Element eleOrder=(Element)XPathAPI.selectSingleNode(inDoc,"OrderInvoice");
			String orderHeaderKey=eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
			Element eleOrderChangeOrderIP=changeOrderInDoc.getDocumentElement();
			eleOrderChangeOrderIP.setAttribute(GCConstants.ORDER_HEADER_KEY,orderHeaderKey);
			Element eleNote=(Element)XPathAPI.selectSingleNode(eleOrderChangeOrderIP,"Notes/Note");
			eleNote.setAttribute("NoteText", GCConstants.GC_SHIPMENT_CONFIRMATION_EMAIL_NOTE);
			LOGGER.debug("Calling ChangeOrder API");
			GCCommonUtil.invokeAPI(
					env,
					GCConstants.CHANGE_ORDER,
					changeOrderInDoc);
			LOGGER.debug("ChangeOrder API executed Successfully");           
			LOGGER.info("Exiting Inside GCCallChangeOrder.callChangeOrder()");
		} catch (Exception e) {
			LOGGER.error("Inside catch of GCCallChangeOrder.callChangeOrder()",e);
			throw new GCException(e);
		}
		return inDoc;
	}

	@Override
	public void setProperties(Properties arg0) {

	}

}
