package com.gc.api;

import java.util.Properties;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCCallChangeOrderOnReturnReceive implements YIFCustomApi{
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCCallChangeOrderOnReturnReceive.class);
  /**
   *
   * Description of callChangeOrder This method calls ChangeOrderAPI for adding notes to the order.
   * For making change order input Xml it uses inDoc.
   *
   * @param env
   * @param inDoc
   * @return
   * @throws GCException
   *
   */
  public Document callChangeOrder(YFSEnvironment env, Document inDoc) throws GCException {
    LOGGER.info("Enter Inside GCCallChangeOrderOnReturnReceive.callChangeOrder()");
    Document changeOrderInDoc = GCXMLUtil.getDocument("<Order OrderHeaderKey=''> "
        +"<Notes> <Note NoteText=''></Note>"
        +"</Notes></Order>");
    LOGGER.verbose("Class: GCCallChangeOrderOnReturnReceive Method: callChangeOrder Begin");
    try {
      Element eleOrder=(Element)XPathAPI.selectSingleNode(inDoc,GCConstants.RECEIPT);
      String orderHeaderKey=eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
      Element eleOrderChangeOrderIP=changeOrderInDoc.getDocumentElement();
      eleOrderChangeOrderIP.setAttribute(GCConstants.ORDER_HEADER_KEY,orderHeaderKey);
      Element eleNote=(Element)XPathAPI.selectSingleNode(eleOrderChangeOrderIP,"Notes/Note");
      eleNote.setAttribute("NoteText", GCConstants.GC_RETURN_RECIEVED_EMAIL_NOTE);
      LOGGER.debug("Calling ChangeOrder API");
      GCCommonUtil.invokeAPI(
          env,
          GCConstants.CHANGE_ORDER,
          changeOrderInDoc);
      LOGGER.debug("ChangeOrder API executed Successfully");
      LOGGER.info("Exiting Inside GCCallChangeOrderOnReturnReceive.callChangeOrder()");
    } catch (Exception e) {
      LOGGER.error("Inside catch of GCCallChangeOrderOnReturnReceive.callChangeOrder()",e);
      throw new GCException(e);
    }
    return inDoc;
  }
  @Override
  public void setProperties(Properties properties) {

  }



}
