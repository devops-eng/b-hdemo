/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0               06/02/2014        Last Name, First Name      JIRA No: Description of issue.
             1.1               27/01/2015        Mishra, Deepak             GCSTORE-6726: Order with Kit and regular item, kit is stuck in Scheduled status
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:deepak.mishra@expicient.com">Mishra,Deepak</a>
 */

public class GCCallScheduleOnBackorderRelease {

	// initialize logger
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCProcessBackordersAPI.class);

	public void schedulePODAXOnRelese(YFSEnvironment env, Document inDoc)
			throws ParseException, TransformerException {

		boolean isBackOrderFromRelease = false;
		HashMap<String, String> mOrderLineValue = new HashMap<String, String>();
		YFCDocument eleDoc = YFCDocument.getDocumentFor(inDoc);
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(" schedulePODAXOnRelese() method, Input document is"
					+ GCXMLUtil.getXMLString(inDoc));
		}
		YFCElement eleRoot = eleDoc.getDocumentElement();
		YFCNodeList<YFCElement> nlOrderLine = eleRoot
				.getElementsByTagName(GCConstants.ORDER_LINE);
		for (int i = 0; i < nlOrderLine.getLength(); i++) {
			YFCElement eleOrderLine = nlOrderLine.item(i);
			String sOrderLineKey = eleOrderLine
					.getAttribute(GCConstants.ORDER_LINE_KEY);
			YFCElement eleItem = eleOrderLine.getElementsByTagName(
					GCConstants.ITEM).item(0);
			String sItemID = eleItem.getAttribute(GCConstants.ITEM_ID);
			String sUOM = eleItem.getAttribute(GCConstants.UOM);
			String sProdClass = eleItem.getAttribute(GCConstants.PRODUCT_CLASS);
			String statusMax = eleOrderLine
					.getAttribute(GCConstants.MAX_LINE_STATUS);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("ItemID for getItemList Input" + sItemID);
				LOGGER.debug("UOM for getItemList Input" + sUOM);
			}
			if (GCConstants.BACKORDERED_STATUS.equalsIgnoreCase(statusMax)) {
				YFCDocument getItemListOutDoc = GCCommonUtil
						.getItemList(
								env,
								sItemID,
								sUOM,
								"",
								true,
								YFCDocument
										.getDocumentFor(GCConstants.GET_ITEM_LIST_TEMPLATE_RELEASE_RESCH));
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("GetItemList Output document is"
							+ getItemListOutDoc);
				}
				YFCElement eleGetItemList = getItemListOutDoc
						.getDocumentElement();
				YFCElement eleItemInvPar = eleGetItemList.getElementsByTagName(
						GCConstants.INVENTORY_PARAMETERS).item(0);
				String sUseUnpalnned = eleItemInvPar
						.getAttribute(GCConstants.USE_UNPLANNED_INVENTORY);
				YFCElement eleItemListOutPut = eleGetItemList
						.getElementsByTagName(GCConstants.ITEM).item(0);
				String sOrganizationCode = eleItemListOutPut
						.getAttribute(GCConstants.ORGANIZATION_CODE);
				if (GCConstants.YES.equalsIgnoreCase(sUseUnpalnned)) {
					mOrderLineValue.put(sOrderLineKey, "Y");
					isBackOrderFromRelease = true;
				} else if (GCConstants.NO.equalsIgnoreCase(sUseUnpalnned)
						|| GCConstants.BLANK_STRING
								.equalsIgnoreCase(sUseUnpalnned)) {
					Integer extendedDays = callCommonCodeToGetNumberOfDaysAdded(env);
					String sSysTime = GCDateUtils.getCurrentTime(GCConstants.YYYY_MM_DD);
					SimpleDateFormat sdf = new SimpleDateFormat(GCConstants.YYYY_MM_DD);
					String sEndShipDate = GCDateUtils.addDays(sSysTime, extendedDays, 1);
					Date dSysDate = sdf.parse(sEndShipDate);
					String sFinalEndShipDate = GCDateUtils.formatDate(dSysDate,
							GCConstants.YYYY_MM_DD);
					YFCDocument getSupplyDetailsinDoc = YFCDocument
							.createDocument(GCConstants.GET_SUPPLY_DETAILS);
					YFCElement eleRootSupplyDetails = getSupplyDetailsinDoc
							.getDocumentElement();
					eleRootSupplyDetails.setAttribute(
							GCConstants.ORGANIZATION_CODE, sOrganizationCode);
					eleRootSupplyDetails.setAttribute(
							GCConstants.PRODUCT_CLASS, sProdClass);
					eleRootSupplyDetails.setAttribute(
							GCConstants.UNIT_OF_MEASURE, sUOM);
					eleRootSupplyDetails.setAttribute(GCConstants.END_DATE,
							sFinalEndShipDate);
					eleRootSupplyDetails.setAttribute(GCConstants.SHIP_DATE,
							sFinalEndShipDate);
					eleRootSupplyDetails.setAttribute(GCConstants.ITEM_ID,
							sItemID);
					if (LOGGER.isVerboseEnabled()) {
						LOGGER.verbose("Get Supply Details InDoc :"
								+ getSupplyDetailsinDoc.getString());
					}
					YFCDocument getSupplyDetailsTemplate = YFCDocument
							.getDocumentFor(GCConstants.GET_SUPPLY_DETAILS_TEMPLATE);
					YFCDocument outDoc = GCCommonUtil.invokeAPI(env,
							GCConstants.GET_SUPPLY_DETAILS,
							getSupplyDetailsinDoc, getSupplyDetailsTemplate);
					if (LOGGER.isVerboseEnabled()) {
						LOGGER.verbose("Get Supply Details OutDoc for Component:"
								+ sItemID + outDoc.getString());
					}
					Document getSupplyDoc = outDoc.getDocument();
					Element eleShipNode = GCXMLUtil.getElementByXPath(
							getSupplyDoc,
							"Item/ShipNodes/ShipNode[@ShipNode='MFI']");
					if (!YFCCommon.isVoid(eleShipNode)) {
						Element eleSupplyOnPO = (Element) XPathAPI
								.selectSingleNode(eleShipNode,
										"Supplies/Supply[@SupplyType='PO_PLACED']");
						if (!YFCCommon.isVoid(eleSupplyOnPO)) {
							String onPOQty = eleSupplyOnPO
									.getAttribute(GCConstants.AVAILABLE_QTY);
							if (0 < Integer.parseInt(onPOQty)) {
								mOrderLineValue.put(sOrderLineKey, "Y");
								isBackOrderFromRelease = true;
							}
						}
					}
				}
			}
		}
		if (isBackOrderFromRelease) {
			env.setTxnObject("ResedulingTheBackorderLines", mOrderLineValue);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Final Map output" + mOrderLineValue);
			}
		}
	}

	private Integer callCommonCodeToGetNumberOfDaysAdded(YFSEnvironment env) {
		
		Integer extendedDays = 0;
		String codeType = GCConstants.EXTENDED_DAYS_FOR_FUTURE_PO;
		String codeValue = GCConstants.CODE_VAULE_FOR_EXTENDED_DAYS;
		YFCDocument getCommonCodeListOutForItemType = GCCommonUtil
				.getCommonCodeListByTypeAndValue(env, codeType, GCConstants.GC,
						codeValue);
		if (!YFCCommon.isVoid(getCommonCodeListOutForItemType)) {
			Document outDoc = getCommonCodeListOutForItemType.getDocument();
			Element eleCommonCode = GCXMLUtil.getElementByXPath(outDoc,
					"/CommonCodeList/CommonCode");
			if (!YFCCommon.isVoid(eleCommonCode)) {
				extendedDays = Integer.parseInt(eleCommonCode
						.getAttribute(GCConstants.CODE_SHORT_DESCRIPTION));
			} else {
				extendedDays = 180; // stamping 180 days
			}
		}
		return extendedDays;
	}
}
