package com.gc.api;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCWebServiceUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCCancelCoupons {

  String sOrderNo = "";
  String sATGURL = "";
  Document inDoc = null;
  String sSoapAction = "";
  String sCouponCode = "";
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCCancelCoupons.class.getName());

  public void cancelCoupons(YFSEnvironment env, Document inDoc)
      throws GCException {
    try {
      this.inDoc = inDoc;
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Incoming XML" + GCXMLUtil.getXMLString(inDoc));
      }
      String sMinOrderStatus = GCXMLUtil.getAttributeFromXPath(inDoc,
          "//@MinOrderStatus");
      String sMaxOrderStatus = GCXMLUtil.getAttributeFromXPath(inDoc,
          "//@MaxOrderStatus");

      if ("9000".equals(sMaxOrderStatus)
          && "9000".equals(sMinOrderStatus)) {
        sOrderNo = XPathAPI.selectSingleNode(inDoc, "//@OrderNo")
            .getNodeValue();
        NodeList nlGCOrderCoupon = XPathAPI.selectNodeList(inDoc,
            "//GCOrderCoupon");

        if (nlGCOrderCoupon.getLength() > 0) {
          sATGURL = YFSSystem.getProperty(GCConstants.ATG_URL);
          if (YFCCommon.isVoid(sATGURL)) {
            throw new YFCException(
                "ATG Promo URL is not configured in property file. Please retry after configuring it.");
          }
          LOGGER.debug("sGCURL :: " + sATGURL);
          String sSoapAct = YFSSystem
              .getProperty(GCConstants.ATG_VOID_SOAP_ACTION);
          if (YFCCommon.isVoid(sSoapAct)) {
            throw new YFCException(
                "ATG Promo soap action is not configured in property file. Please retry after configuring it.");
          }
        }

        for (int i = 0; i < nlGCOrderCoupon.getLength(); i++) {

          sCouponCode = ((Element) nlGCOrderCoupon.item(i))
              .getAttribute("CouponCode");
          Document docCancelCouponIp = prepareCancelCouponIpDoc();

          GCWebServiceUtil obj = new GCWebServiceUtil();
          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("docCancelCouponIp Input Doc to Web Service"
                + GCXMLUtil.getXMLString(docCancelCouponIp));
          }
          Document docFinalizeCouponOp = obj.invokeSOAPWebService(
              docCancelCouponIp, sATGURL, sSoapAction);

          Element eleMessage = GCXMLUtil.getElementByXPath(
              docFinalizeCouponOp,
              "Envelope/Body/VoidResponse/VoidResult/Message");
          if (!YFCCommon.isVoid(eleMessage)) {
            String sErrorMessage = eleMessage.getTextContent();
            raiseException(env, sErrorMessage);
          }
        }
      }
    } catch (Exception e) {
      LOGGER.error("The exception is in cancelCoupons method:", e);
      throw GCException.getYFCException(e.getMessage(), e);
    }
  }

  /**
   * 
   * @param env
   * @param sErrorMessage
   * @throws Exception
   */
  private void raiseException(YFSEnvironment env, String sErrorMessage)
      throws GCException {
    try {
      final String logInfo = "GCMassOperationProcess :: createException :: ";
      LOGGER.debug(logInfo + "Begin");

      LOGGER.debug(logInfo + "sResultMessage :: " + "CouppnCode "
          + sCouponCode + " Cancellation  Failed");
      LOGGER.debug(logInfo + "sExceptionType :: "
          + "CouponCancellationFailed");
      LOGGER.debug(logInfo + "SQueueID :: " + "CSR_GROUP");

      Document docCreateExceptionIp = GCXMLUtil
          .createDocument(GCConstants.INBOX);

      Element eleRoot = docCreateExceptionIp.getDocumentElement();

      eleRoot.setAttribute(GCConstants.EXCEPTION_TYPE,
          GCConstants.GC_COUPON_CANCELLATION_FAILED);
      eleRoot.setAttribute(GCConstants.QUEUE_ID, GCConstants.GC_COUPON_CANCELATION_FAILED_ALERT_QUEUE);

      Element eleInboxReferencesList = docCreateExceptionIp
          .createElement(GCConstants.INBOX_REFERENCE_LIST);
      eleRoot.appendChild(eleInboxReferencesList);

      Element eleInboxReferences1 = docCreateExceptionIp
          .createElement(GCConstants.INBOX_REFERENCES);
      eleInboxReferencesList.appendChild(eleInboxReferences1);
      eleInboxReferences1.setAttribute(GCConstants.NAME,
          GCConstants.RESULT_MESSAGE);
      eleInboxReferences1.setAttribute(GCConstants.VALUE, "CouponCode "
          + sCouponCode + " cancellation failed");
      eleInboxReferences1.setAttribute(GCConstants.REFERENCE_TYPE,
          GCConstants.TEXT);

      Element eleInboxReferences2 = docCreateExceptionIp
          .createElement(GCConstants.INBOX_REFERENCES);
      eleInboxReferences2.setAttribute(GCConstants.NAME,
          "ATGErrorMessage");
      eleInboxReferences2.setAttribute(GCConstants.VALUE, sErrorMessage);
      eleInboxReferences2.setAttribute(GCConstants.REFERENCE_TYPE,
          GCConstants.TEXT);
      eleInboxReferencesList.appendChild(eleInboxReferences2);

      Element eleOrder = docCreateExceptionIp
          .createElement(GCConstants.ORDER);
      eleRoot.appendChild(eleOrder);
      eleOrder.setAttribute(GCConstants.ORDER_NO, sOrderNo);
      eleOrder.setAttribute(GCConstants.ENTERPRISE_CODE, XPathAPI
          .selectSingleNode(inDoc, "//@EnterpriseCode")
          .getNodeValue());
      eleOrder.setAttribute(GCConstants.DOCUMENT_TYPE, XPathAPI
          .selectSingleNode(inDoc, "//@DocumentType").getNodeValue());
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(logInfo + "docCreateExceptionIp\n"
            + GCXMLUtil.getXMLString(docCreateExceptionIp));
      }
      GCCommonUtil.invokeAPI(env, GCConstants.API_CREATE_EXCEPTION,
          docCreateExceptionIp);
    } catch (Exception e) {
      LOGGER.error("The exception is in raiseException method:", e);
      throw new GCException(e);
    }

  }

  private Document prepareCancelCouponIpDoc()
      throws ParserConfigurationException {

    Document docFinalizeCouponIp = GCXMLUtil
        .createDocument("soapenv:Envelope");
    Element eleRoot = docFinalizeCouponIp.getDocumentElement();

    eleRoot.setAttribute("xmlns:soapenv",
        "http://schemas.xmlsoap.org/soap/envelope/");
    eleRoot.setAttribute("xmlns:v0",
        "http://www.guitarcenter.com/schemas/PromoEngine/V0.1");

    Element eleHeader = docFinalizeCouponIp.createElement("soapenv:Header");
    eleRoot.appendChild(eleHeader);

    Element eleBody = docFinalizeCouponIp.createElement("soapenv:Body");
    eleRoot.appendChild(eleBody);

    Element eleVoidRequest = docFinalizeCouponIp
        .createElement("v0:VoidRequest");
    eleBody.appendChild(eleVoidRequest);

    Element elesource = docFinalizeCouponIp.createElement("v0:source");
    eleVoidRequest.appendChild(elesource);
    elesource.setTextContent("CALL_CENTER");

    Element eleCouponCode = docFinalizeCouponIp
        .createElement("v0:couponCode");
    eleVoidRequest.appendChild(eleCouponCode);
    eleCouponCode.setTextContent(sCouponCode);

    Element elesalesTicketNumber = docFinalizeCouponIp
        .createElement("v0:salesTicketNumber");
    eleVoidRequest.appendChild(elesalesTicketNumber);
    elesalesTicketNumber.setTextContent(sOrderNo);
    return docFinalizeCouponIp;

  }
}
