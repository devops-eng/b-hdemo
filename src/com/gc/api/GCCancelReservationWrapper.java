/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 * *#################################################################################################################################################################
 * OBJECTIVE: This class is a wrapper around CancelReservation api.This class is exposed to ATG for Reservation Cancellation.
 * #################################################################################################################################################################
 * Version                     Date                            Modified By                              Description
 * #################################################################################################################################################################
 * 1.0                       29/12/2014                        Saxena, Ekansh                           GCSTORE-67- Order Reservation Cancellation
 * 2.0                       03/19/2015                        Saxena, Ekansh                           Added code for bundle with tag component.
 * #################################################################################################################################################################
 */

package com.gc.api;

import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCOrderUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class is wrapper around cancelReservation API. ATG wil call this class synchronously to
 * cancel Reservations.
 *
 * @author Ekansh, Saxena
 *
 */
public class GCCancelReservationWrapper implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCCancelReservationWrapper.class);

  /**
   *
   * This method will loop through all the CancelReservation Elements in the request and processes
   * and calls cancelReservation API for every element.
   *
   * @param env
   * @param inDoc
   * @return
   *
   */

  public Document cancelReservation(YFSEnvironment env, Document inputDoc) {

    LOGGER.verbose(" Entering GCCancelReservationWrapperService.cancelReservation() method ");
    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Incoming XML Doc" + inDoc.toString());
    }

    YFCDocument docCancelReservation = null;
    Document outDoc = GCXMLUtil.createDocument(GCXmlLiterals.CANCEL_RESERVATION_LINE_LIST);

    YFCElement eleExtn;

    // Fetch CancelReservation node list from inputDoc
    YFCNodeList<YFCElement> nlCancelReservation = inDoc.getElementsByTagName(GCXmlLiterals.CANCEL_RESERVATION);
    String sItemID;
    String sUOM;
    String sBatchNo = null;
    String productClass;
    YFCDocument getItemListOp;
    YFCDocument getItemLstTmpl;
    String sOrganizationCode = GCConstants.GCI;
    for (YFCElement eleCancelReservation : nlCancelReservation) {

      // Prepare inputDoc for CancelReservation API
      docCancelReservation = YFCDocument.createDocument(GCXmlLiterals.CANCEL_RESERVATION);
      YFCElement eleCnclReservation = docCancelReservation.getDocumentElement();
      copyAttributes(eleCancelReservation, eleCnclReservation);
      sItemID = eleCancelReservation.getAttribute(GCConstants.ITEM_ID);
      sUOM = eleCancelReservation.getAttribute(GCConstants.UNIT_OF_MEASURE);
      String sQtyToBeCancelled = eleCancelReservation.getAttribute(GCConstants.QTY_TO_BE_CANCELLED);
      double cancelledQty = Double.parseDouble(sQtyToBeCancelled);
      // get getItemList Template
      getItemLstTmpl =
          YFCDocument.getDocumentFor(GCXMLUtil.getDocument(GCConstants.GET_ITEM_LIST_TEMPLATE_ORDER_PROMISING));
      // call getItemList
      getItemListOp = GCCommonUtil.getItemList(env, sItemID, sUOM, sOrganizationCode, true, getItemLstTmpl);

      YFCElement eleItemList = getItemListOp.getDocumentElement();
      YFCElement eleItem = eleItemList.getChildElement(GCConstants.ITEM);
      YFCElement elePrimaryInfo = eleItem.getChildElement(GCConstants.PRIMARY_INFORMATION);
      String sKitCode = elePrimaryInfo.getAttribute(GCConstants.KIT_CODE);

      eleExtn = eleCancelReservation.getChildElement(GCConstants.EXTN);
      if (!YFCCommon.isVoid(eleExtn)) {
        sBatchNo = eleExtn.getAttribute(GCXmlLiterals.EXTN_BATCH_NO);
      }

      // Check if item is bundle with tag component
      if (YFCCommon.equals(GCConstants.BUNDLE, sKitCode, false) && !YFCCommon.isStringVoid(sBatchNo)) {
        YFCNodeList<YFCElement> n1Component = getItemListOp.getElementsByTagName(GCConstants.COMPONENT);
        for (YFCElement eleComponent : n1Component) {
          String sComponentItemID = eleComponent.getAttribute(GCConstants.COMPONENT_ITEM_ID);
          String sComponentUOM = eleComponent.getAttribute(GCConstants.COMPONENT_UOM);
          // get the product class
          productClass = GCOrderUtil.getProductClass(getItemListOp);
          LOGGER.verbose("Product Class for the item is " + productClass);
          // Stamp ProductClass
          eleCnclReservation.setAttribute(GCConstants.PRODUCT_CLASS, productClass);
          eleExtn = eleCancelReservation.getChildElement(GCConstants.EXTN);
          String sKitQty = eleComponent.getAttribute(GCConstants.KIT_QUANTITY);
          double kitQty = Double.parseDouble(sKitQty);
          double cnclQty = kitQty * cancelledQty;
          eleCnclReservation.setAttribute(GCConstants.ITEM_ID, sComponentItemID);
          eleCnclReservation.setAttribute(GCConstants.QTY_TO_BE_CANCELLED, Double.toString(cnclQty));
          YFCDocument getItemTagOutTemplate = YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LIST_API_TMPL);
          YFCDocument getItemTagoutDoc =
              GCCommonUtil.getItemList(env, sComponentItemID, sComponentUOM, GCConstants.GCI, true,getItemTagOutTemplate);
          YFCElement eleTagItemList = getItemTagoutDoc.getDocumentElement();
          YFCElement eleTagItem = eleTagItemList.getChildElement(GCConstants.ITEM);
          YFCElement eleInvParameters = eleTagItem.getChildElement(GCConstants.INVENTORY_PARAMETERS);
          String sTagControlFlag = eleInvParameters.getAttribute(GCXmlLiterals.TAG_CONTROL_FLAG);
          if (YFCCommon.equals(GCConstants.SOMETIMES_TAG_CONTROLLED, sTagControlFlag, false)) {
            LOGGER.verbose("There is a Serial Item in Cancel Reservation Request");
            // Prepare inputDoc for getInventorySupply to fetch InventoryTagKey
            sBatchNo = eleExtn.getAttribute(GCXmlLiterals.EXTN_BATCH_NO);

            // preparing getReservation API input
            YFCDocument docGetReservation = YFCDocument.createDocument(GCXmlLiterals.GET_RESERVATION);
            YFCElement eleGetReservation = docGetReservation.getDocumentElement();
            eleGetReservation.setAttribute(GCConstants.ITEM_ID, sComponentItemID);
            eleGetReservation.setAttribute(GCConstants.ORGANIZATION_CODE,
                eleCancelReservation.getAttribute(GCConstants.ORGANIZATION_CODE));
            eleGetReservation.setAttribute(GCConstants.PRODUCT_CLASS, productClass);
            eleGetReservation.setAttribute(GCConstants.RESERVATION_ID,
                eleCancelReservation.getAttribute(GCConstants.RESERVATION_ID));
            eleGetReservation.setAttribute(GCConstants.UOM, eleCancelReservation.getAttribute(GCConstants.UOM));
            eleGetReservation.setAttribute(GCConstants.SHIP_NODE,
                eleCancelReservation.getAttribute(GCConstants.SHIP_NODE));
            if (LOGGER.isVerboseEnabled()) {
              LOGGER.verbose("getReservation API input" + docGetReservation.toString());
            }

            // Call getReservation API.
            YFCDocument outDocGetReservation =
                GCCommonUtil.invokeAPI(env, GCConstants.API_GET_RESERVATION, docGetReservation, docGetReservation);

            // Fetch InventoryTagKey
            YFCNodeList<YFCElement> nlItemReservation =
                outDocGetReservation.getElementsByTagName(GCXmlLiterals.ITEM_RESERVATION);

            for (YFCElement eleItemReservation : nlItemReservation) {

              if (YFCCommon.equals(sBatchNo, eleItemReservation.getAttribute(GCXmlLiterals.TAG_NUMBER),
                  GCConstants.FALSE)) {
                String sInventoryTagKey = eleItemReservation.getAttribute(GCXmlLiterals.TAG_KEY);
                eleCnclReservation.setAttribute(GCXmlLiterals.INVENTORY_TAG_KEY, sInventoryTagKey);
                break;
              }
            }
          }
          if (LOGGER.isVerboseEnabled()) {
            LOGGER.verbose("cancelReservation API input" + docCancelReservation.toString());
          }
          // Call CancelReservation API
          GCCommonUtil.invokeAPI(env, GCConstants.API_CANCEL_RESERVATION, docCancelReservation, docCancelReservation);
        }
      } else {
        // get the product class
        productClass = GCOrderUtil.getProductClass(getItemListOp);
        LOGGER.verbose("Product Class for the item is " + productClass);
        // Stamp ProductClass
        eleCnclReservation.setAttribute(GCConstants.PRODUCT_CLASS, productClass);
        eleExtn = eleCancelReservation.getChildElement(GCConstants.EXTN);
        // Check if item is serial item. If serial item, stamp InventoryTagKey
        if ((!YFCCommon.isVoid(eleExtn) && !YFCCommon.isStringVoid(eleExtn.getAttribute(GCXmlLiterals.EXTN_BATCH_NO)))) {
          LOGGER.verbose("There is a Serial Item in Cancel Reservation Request");
          // Prepare inputDoc for getInventorySupply to fetch InventoryTagKey
          sBatchNo = eleExtn.getAttribute(GCXmlLiterals.EXTN_BATCH_NO);

          // preparing getReservation API input
          YFCDocument docGetReservation = YFCDocument.createDocument(GCXmlLiterals.GET_RESERVATION);
          YFCElement eleGetReservation = docGetReservation.getDocumentElement();
          eleGetReservation.setAttribute(GCConstants.ITEM_ID, eleCancelReservation.getAttribute(GCConstants.ITEM_ID));
          eleGetReservation.setAttribute(GCConstants.ORGANIZATION_CODE,
              eleCancelReservation.getAttribute(GCConstants.ORGANIZATION_CODE));
          eleGetReservation.setAttribute(GCConstants.PRODUCT_CLASS, productClass);
          eleGetReservation.setAttribute(GCConstants.RESERVATION_ID,
              eleCancelReservation.getAttribute(GCConstants.RESERVATION_ID));
          eleGetReservation.setAttribute(GCConstants.UOM, eleCancelReservation.getAttribute(GCConstants.UOM));
          eleGetReservation.setAttribute(GCConstants.SHIP_NODE,
              eleCancelReservation.getAttribute(GCConstants.SHIP_NODE));
          if (LOGGER.isVerboseEnabled()) {
            LOGGER.verbose("getReservation API input" + docGetReservation.toString());
          }

          // Call getReservation API.
          YFCDocument outDocGetReservation =
              GCCommonUtil.invokeAPI(env, GCConstants.API_GET_RESERVATION, docGetReservation, docGetReservation);

          // Fetch InventoryTagKey
          YFCNodeList<YFCElement> nlItemReservation =
              outDocGetReservation.getElementsByTagName(GCXmlLiterals.ITEM_RESERVATION);

          for (YFCElement eleItemReservation : nlItemReservation) {

            if (YFCCommon
                .equals(sBatchNo, eleItemReservation.getAttribute(GCXmlLiterals.TAG_NUMBER), GCConstants.FALSE)) {
              String sInventoryTagKey = eleItemReservation.getAttribute(GCXmlLiterals.TAG_KEY);
              eleCnclReservation.setAttribute(GCXmlLiterals.INVENTORY_TAG_KEY, sInventoryTagKey);
              break;
            }
          }
        }


        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("cancelReservation API input" + docCancelReservation.toString());
        }
        // Call CancelReservation API
        GCCommonUtil.invokeAPI(env, GCConstants.API_CANCEL_RESERVATION, docCancelReservation, docCancelReservation);
      }
    }
    LOGGER.verbose(" Exiting GCCancelReservation.CancelReservation() method ");

    return outDoc;
  }

  /**
   * This method copies all attributes from destElem to srcElem elements.
   *
   * @param srcElem
   * @param destElem
   */
  public void copyAttributes(YFCElement srcElem, YFCElement destElem) {
    Map<String, String> attrMap = srcElem.getAttributes();
    Set<Map.Entry<String, String>> entrySet = attrMap.entrySet();
    Iterator<Map.Entry<String, String>> itr = entrySet.iterator();
    while (itr.hasNext()) {
      Map.Entry<String, String> attrEntry = itr.next();
      destElem.setAttribute(attrEntry.getKey(), attrEntry.getValue());
    }
  }


  @Override
  public void setProperties(Properties prop) {

  }

}