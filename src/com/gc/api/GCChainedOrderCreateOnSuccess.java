/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Processes On Success of Create Chained Order
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               25/03/2014        Singh, Gurpreet            Development Chained Order PO
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:gurpreet.singh@expicient.com">Gurpreet</a>
 */
public class GCChainedOrderCreateOnSuccess implements YIFCustomApi {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCChainedOrderCreateOnSuccess.class);

    /**
     * 
     * Description of chainedOrderCreateOnSuccess Check If there are warranty
     * lines: Make the quantity of that order line as 0
     * 
     * @param env
     * @param inDoc
     * @return
     * @throws GCException
     * 
     */
    public Document chainedOrderCreateOnSuccess(YFSEnvironment env,
	    Document inDoc) throws GCException {
	LOGGER.debug(" Entering GCChainedOrderCreateOnSuccess.chainedOrderCreateOnSuccess() method ");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" chainedOrderCreateOnSuccess() method, Input document is"
		    + GCXMLUtil.getXMLString(inDoc));
	}
	try {
	    Element eleOrder = inDoc.getDocumentElement();
	    String strOrderNo = eleOrder.getAttribute(GCConstants.ORDER_NO);
		 String salesOrderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
      Element eleOrdExtn = (Element) eleOrder.getElementsByTagName(GCConstants.EXTN).item(0);
      String pickUpStore = eleOrdExtn.getAttribute(GCConstants.EXTN_PICK_UP_STORE_ID);
      String purchaseOrderNoStr = eleOrdExtn.getAttribute(GCConstants.EXTN_PURCHASE_ORDER_NUMBERS);
      String origPurchaseStr = purchaseOrderNoStr;
      if (!YFCCommon.isVoid(pickUpStore)) {
        List<Element> nlChainedOrder = GCXMLUtil.getElementListByXpath(inDoc, "Order/ChainedOrderList/Order");
        Iterator<Element> chainedOrderIterator = nlChainedOrder.iterator();
        while (chainedOrderIterator.hasNext()) {
          Element eleChainedOrder = chainedOrderIterator.next();
          String chainedOrderNo = eleChainedOrder.getAttribute(GCConstants.ORDER_NO);
          String docType = eleChainedOrder.getAttribute(GCConstants.DOCUMENT_TYPE);
          if (YFCUtils.equals(GCConstants.ZERO_ZERO_ZERO_FIVE, docType)) {
            if (YFCCommon.isVoid(purchaseOrderNoStr)) {
              purchaseOrderNoStr = chainedOrderNo;
            } else {
              purchaseOrderNoStr = purchaseOrderNoStr + GCConstants.UNDERSCORE + chainedOrderNo;
            }
          }
        }
        if (!YFCUtils.equals(purchaseOrderNoStr, origPurchaseStr)) {
          YFCDocument changeOrdrIp = YFCDocument.createDocument(GCConstants.ORDER);
          YFCElement eleChangeOrderIp = changeOrdrIp.getDocumentElement();
          eleChangeOrderIp.setAttribute(GCConstants.ORDER_HEADER_KEY, salesOrderHeaderKey);
          YFCElement eleOrdExtnIp = eleChangeOrderIp.createChild(GCConstants.EXTN);
          eleOrdExtnIp.setAttribute(GCConstants.EXTN_PURCHASE_ORDER_NUMBERS, purchaseOrderNoStr);
          YFCDocument changeOrdTemp = YFCDocument.createDocument(GCConstants.API_SUCCESS);
          GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, changeOrdrIp, changeOrdTemp);
        }
      }
	    if (!YFCCommon.isVoid(strOrderNo)) {
		eleOrder.setAttribute(GCConstants.SALES_ORDER_NO, strOrderNo);
		eleOrder.removeAttribute(GCConstants.ORDER_NO);
	    }
	    Element eleOrderLines = GCXMLUtil.getElementByXPath(inDoc,
		    "Order/ChainedOrderList/Order/OrderLines");
	    List<Element> nlOrderLine = GCXMLUtil.getElementListByXpath(inDoc,
		    "Order/ChainedOrderList/Order/OrderLines/OrderLine");
	    Iterator<Element> listIterator = nlOrderLine.iterator();
	    while (listIterator.hasNext()) {
		Element eleOrderLine = listIterator.next();
		String sExtnIsWarrantyItem = GCXMLUtil.getAttributeFromXPath(
			eleOrderLine, "Extn/@ExtnIsWarrantyItem");
		if (GCConstants.YES.equalsIgnoreCase(sExtnIsWarrantyItem)) {
		    GCXMLUtil.removeChild(eleOrderLines, eleOrderLine);
		}
	    }
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCChainedOrderCreateOnSuccess.chainedOrderCreateOnSuccess()",
		    e);
	    throw new GCException(e);
	}
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" Updated input document is"
		    + GCXMLUtil.getXMLString(inDoc));
	}
	LOGGER.debug(" Exiting GCChainedOrderCreateOnSuccess.chainedOrderCreateOnSuccess() method ");
	return inDoc;
    }

    public void setProperties(Properties arg0) {

    }

}
