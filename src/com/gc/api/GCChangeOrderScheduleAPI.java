package com.gc.api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class is developed as a fix for defect GCSTORE-3516
 *
 * @author rashmataj
 *
 */
public class GCChangeOrderScheduleAPI {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCChangeOrderScheduleAPI.class);

  /**
   * This Method moves the expected shipment date to higer value if the current value if future
   * date.
   *
   * @param env
   * @param inputDoc
   * @return
   * @throws ParseException
   */
  public Document processChangeOrderSchedules(YFSEnvironment env, Document inputDoc) throws ParseException {
    LOGGER.beginTimer("processChangeOrderSchedules");
    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input to processChangeOrderSchedules::" + inDoc.toString());
    }
    String isProcessingNotRequired = (String) env.getTxnObject("IsManualSchedule");
    LOGGER.verbose("isProcessing Not Required::" + isProcessingNotRequired);
    if (!YFCCommon.equals(isProcessingNotRequired, "Y")) {
      String orderHeaderKey = inDoc.getDocumentElement().getAttribute("OrderHeaderKey");
      // create GetOrderList Input Doc
      YFCDocument getOrderListInDoc = YFCDocument.createDocument("Order");
      getOrderListInDoc.getDocumentElement().setAttribute("OrderHeaderKey", orderHeaderKey);
      // create getOrderList Templ Doc
      YFCDocument getOrderListTmplDoc = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_TMPL_FOR_SCHEDULES);
      // invoke getOrderList API

      YFCDocument getOrderListOutDoc =
          GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListInDoc, getOrderListTmplDoc);

      YFCElement eleOrder = getOrderListOutDoc.getDocumentElement().getChildElement("Order");
      boolean callChangeOrderSchdeule = false;
      YFCDocument changeOrderScheduleInDoc = YFCDocument.createDocument("ChangeSchedule");
      YFCElement changeOrderScheduleRoot = changeOrderScheduleInDoc.getDocumentElement();
      changeOrderScheduleRoot.setAttribute("OrderHeaderKey", orderHeaderKey);
      changeOrderScheduleRoot.setAttribute("Override", "Y");
      YFCElement fromSchedulesEle = changeOrderScheduleRoot.createChild("FromSchedules");
      YFCNodeList<YFCElement> orderLinesNL = eleOrder.getElementsByTagName("OrderLine");
      YFCDocument changeOrderInDoc = YFCDocument.createDocument("Order");
      YFCElement eleInDocOrder = changeOrderInDoc.getDocumentElement();
      eleInDocOrder.setAttribute("OrderHeaderKey", orderHeaderKey);
      YFCElement eleOls = eleInDocOrder.createChild("OrderLines");
      for (YFCElement eleOrderLine : orderLinesNL) {
        String orderLineKey = eleOrderLine.getAttribute("OrderLineKey");
        String maxLineStatus = eleOrderLine.getAttribute("MaxLineStatus");
        String minLineStatus = eleOrderLine.getAttribute("MinLineStatus");
        if (YFCCommon.equals(maxLineStatus, "1500.10") && YFCCommon.equals(minLineStatus, "1500.10")) {
          YFCElement schedulesEle = eleOrderLine.getChildElement("Schedules");
          YFCElement scheduleEle = schedulesEle.getChildElement("Schedule");
          String expectedShipDateStr = scheduleEle.getAttribute("ExpectedShipmentDate");
          Calendar calSysDate = Calendar.getInstance();
          // Fix for 4247
          Date expectedShipDate = new SimpleDateFormat(GCConstants.DATE_FORMAT).parse(expectedShipDateStr);
          Calendar esd = Calendar.getInstance();
          esd.setTime(expectedShipDate);
          if (esd.getTime().compareTo(calSysDate.getTime()) > 0) {
            callChangeOrderSchdeule = true;
            YFCElement fromScheduleEle = fromSchedulesEle.createChild("FromSchedule");
            fromScheduleEle.setAttribute("FromExpectedShipDate", expectedShipDateStr);
            fromScheduleEle.setAttribute("FromStatus", "1500.10");
            fromScheduleEle.setAttribute("OrderLineKey", orderLineKey);
            YFCElement toSchedulesEle = fromScheduleEle.createChild("ToSchedules");
            YFCElement toScheduleEle = toSchedulesEle.createChild("ToSchedule");
            toScheduleEle.setAttribute("Quantity", eleOrderLine.getAttribute("OrderedQty"));
            //YFCElement eleExtnToSchedule = toScheduleEle.createChild("Extn");
            //eleExtnToSchedule.setAttribute("ExtnExpectedShipDate", expectedShipDateStr);
            //YFCElement eleExtn  = fromScheduleEle.createChild("Extn");
            //eleExtn.setAttribute("ExtnExpectedShipDate", expectedShipDateStr);
            String numOfDaysToAdd = getNumOfDaysToADD(env, eleOrder.getAttribute("EnterpriseCode"));
            esd.add(Calendar.DATE, Integer.parseInt(numOfDaysToAdd));
            SimpleDateFormat dateFormat = new SimpleDateFormat(GCConstants.DATE_FORMAT);

            toScheduleEle.setAttribute("ToExpectedShipDate", dateFormat.format(esd.getTime()));
            
            
            YFCElement eleOrderLine1 = eleOls.createChild("OrderLine");
            eleOrderLine1.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey);
            YFCElement eleExtn = eleOrderLine1.createChild("Extn");
            eleExtn.setAttribute("ExtnExpectedShipDate", expectedShipDateStr);
          }
        }
      }
      if (callChangeOrderSchdeule) {
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("Input XML to changeOrderSchedule API::" + changeOrderScheduleInDoc);
        }
        GCCommonUtil.invokeAPI(env, "changeOrderSchedule", changeOrderScheduleInDoc, null);
        // call manageTaskQueue to delete the record which is inserted by the changeOrderSchedule
        // API.
        GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, changeOrderInDoc,null);
        
                
        callManageTaskQueue(env, orderHeaderKey);
      }
    }
    LOGGER.endTimer("processChangeOrderSchedules");
    return inputDoc;

  }

  /**
   * GCSTORE-4096 and 3516::This method calls manageTaskQueue API to delete the records of this
   * order against schedule transcation.
   *
   * @param env
   * @param orderHeaderKey
   */
  private void callManageTaskQueue(YFSEnvironment env, String orderHeaderKey) {
    YFCDocument manageTaskQueueInDoc = YFCDocument.createDocument("TaskQueue");
    YFCElement taskQueueEle = manageTaskQueueInDoc.getDocumentElement();
    taskQueueEle.setAttribute("DataKey", orderHeaderKey);
    taskQueueEle.setAttribute("DataType", "OrderHeaderKey");
    taskQueueEle.setAttribute("Operation", "Delete");
    taskQueueEle.setAttribute("TransactionKey", "SCHEDULE.0001");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input XML to manageTaskQueue API::" + manageTaskQueueInDoc.toString());
    }
    GCCommonUtil.invokeAPI(env, "manageTaskQueue", manageTaskQueueInDoc, null);

  }

  private String getNumOfDaysToADD(YFSEnvironment env, String orgCode) {
    LOGGER.beginTimer("getNumOfDaysToADD");
    Document getCommonCodeListOutDoc = GCCommonUtil.getCommonCodeListByType(env, "GCExpectedShipDelay", orgCode);
    YFCDocument getCommonCdeLstOutDoc = YFCDocument.getDocumentFor(getCommonCodeListOutDoc);
    YFCElement commonCdeLst = getCommonCdeLstOutDoc.getDocumentElement();
    YFCElement commonCode = commonCdeLst.getChildElement("CommonCode");
    String codeValue = commonCode.getAttribute("CodeValue");
    LOGGER.endTimer("getNumOfDaysToADD");
    return codeValue;
  }


}
