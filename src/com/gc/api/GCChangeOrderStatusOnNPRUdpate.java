/**
 * Copyright 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: 
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
 *          1.0                07/01/2014        Susai, John Melvin			Initial Version
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="john.melvin@expicient.com">John Melvin</a>
 */
public class GCChangeOrderStatusOnNPRUdpate implements YIFCustomApi {

    // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCChangeOrderStatusOnNPRUdpate.class.getName());

    /**
     * 
     * Description of changeOrderStatusOnNPRUpdate
     * 
     * 
     * @param env
     * @param inDoc
     * @return
     * @throws Exception
     * 
     */
    public Document changeOrderStatusOnNPRUpdate(YFSEnvironment env,
	    Document inDoc) throws GCException {
	LOGGER.verbose(" Entering changeOrderStatusOnNPRUpdate() method ");
	LOGGER.debug(" changeOrderStatusOnNPRUpdate() method, Input document is"
		+ GCXMLUtil.getXMLString(inDoc));
	Document respDoc = null;
	try {
	    String sGetCommonCodeListInDoc = "<CommonCode CodeType=\"RETURN_REASON\" />";
	    Document getCommonCodeListInDoc = GCXMLUtil
		    .getDocument(sGetCommonCodeListInDoc);

	    String sGetCommonCodeListTemplate = "<CommonCodeList><CommonCode CodeLongDescription=\"\" CodeShortDescription=\"\" CodeType=\"\" CodeValue=\"\" /></CommonCodeList>";
	    Document getCommonCodeListTemplate = GCXMLUtil
		    .getDocument(sGetCommonCodeListTemplate);

	    Document getCommonCodeListOutDoc = GCCommonUtil.invokeAPI(env,
		    GCConstants.GET_COMMON_CODE_LIST, getCommonCodeListInDoc,
		    getCommonCodeListTemplate);

	    List<String> nprReasonCodeValueList = new ArrayList<String>();
	    for (int i = 0; i < getCommonCodeListOutDoc.getElementsByTagName(
		    "CommonCode").getLength(); i++) {
		String sCodeShortDescription = ((Element) getCommonCodeListOutDoc
			.getElementsByTagName("CommonCode").item(i))
			.getAttribute("CodeShortDescription");
		if (sCodeShortDescription.startsWith("NPR")) {
		    nprReasonCodeValueList
			    .add(((Element) getCommonCodeListOutDoc
				    .getElementsByTagName("CommonCode").item(i))
				    .getAttribute("CodeValue"));
		}
	    }

	    String sOrderHeaderKey = inDoc.getDocumentElement().getAttribute(
		    GCConstants.ORDER_HEADER_KEY);

	    List<String> nprUpdatedOrderLineList = new ArrayList<String>();

	    Element orderAuditLevelsEle = (Element) inDoc.getElementsByTagName(
		    "OrderAuditLevels").item(0);
	    for (int i = 0; i < orderAuditLevelsEle.getElementsByTagName(
		    "OrderAuditLevel").getLength(); i++) {
		String sModificationLevel = ((Element) orderAuditLevelsEle
			.getElementsByTagName("OrderAuditLevel").item(i))
			.getAttribute("ModificationLevel");
		String sOrderLineKey = ((Element) orderAuditLevelsEle
			.getElementsByTagName("OrderAuditLevel").item(i))
			.getAttribute(GCConstants.ORDER_LINE_KEY);
		if ("ORDER_LINE".equalsIgnoreCase(sModificationLevel)) {
		    Element orderAuditDetailsEle = (Element) ((Element) orderAuditLevelsEle
			    .getElementsByTagName("OrderAuditLevel").item(i))
			    .getElementsByTagName("OrderAuditDetails").item(0);
		    for (int j = 0; j < orderAuditDetailsEle
			    .getElementsByTagName("OrderAuditDetail")
			    .getLength(); j++) {
			Element attributesEle = (Element) ((Element) orderAuditDetailsEle
				.getElementsByTagName("OrderAuditDetail").item(
					j)).getElementsByTagName("Attributes")
				.item(0);
			for (int k = 0; k < attributesEle.getElementsByTagName(
				"Attribute").getLength(); k++) {
			    Element attributeEle = (Element) attributesEle
				    .getElementsByTagName("Attribute").item(k);
			    String sName = attributeEle
				    .getAttribute(GCConstants.NAME);
			    String sNewValue = attributeEle
				    .getAttribute(GCConstants.NEW_VALUE);
			    String sOldValue = attributeEle
				    .getAttribute(GCConstants.OLD_VALUE);
			    if (!nprReasonCodeValueList.contains(sOldValue)
				    && "ReturnReason".equalsIgnoreCase(sName)
				    && nprReasonCodeValueList
					    .contains(sNewValue)
				    && !nprUpdatedOrderLineList
					    .contains(sOrderLineKey)) {
				nprUpdatedOrderLineList.add(sOrderLineKey);
			    }
			}
		    }
		}
	    }

	    if (!nprUpdatedOrderLineList.isEmpty()) {
		Document docChangeOrderStatusIp = GCXMLUtil
			.createDocument("OrderStatusChange");
		Element eleRoot = docChangeOrderStatusIp.getDocumentElement();
		eleRoot.setAttribute(GCConstants.ORDER_HEADER_KEY,
			sOrderHeaderKey);
		eleRoot.setAttribute(GCConstants.TRANSACTION_ID,
			"ChangeNPROrderStatus.0003.ex");

		Element eleOrderLines = docChangeOrderStatusIp
			.createElement("OrderLines");
		eleRoot.appendChild(eleOrderLines);
		for (int i = 0; i < nprUpdatedOrderLineList.size(); i++) {
		    Element eleOrderLine = docChangeOrderStatusIp
			    .createElement(GCConstants.ORDER_LINE);
		    eleOrderLines.appendChild(eleOrderLine);
		    eleOrderLine.setAttribute(GCConstants.BASE_DROP_STATUS,
			    "1100.100");
		    eleOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY,
			    nprUpdatedOrderLineList.get(i));
		    eleOrderLine.setAttribute(
			    GCConstants.CHANGE_FOR_ALL_AVAIL_QTY, "Y");

		}

		LOGGER.debug("*****changeOrderStatus input*****"
			+ GCXMLUtil.getXMLString(docChangeOrderStatusIp));
		GCCommonUtil.invokeAPI(env, "changeOrderStatus",
			docChangeOrderStatusIp);

		respDoc = GCXMLUtil.createDocument("Response");
		Element respEle = respDoc.getDocumentElement();
		respEle.setAttribute("ResponseCode", "SUCCESS");
		respEle.setAttribute("ResponseDescription",
			"Order Status Changed Successfully");
	    } else {
		respDoc = GCXMLUtil.createDocument("Response");
		Element respEle = respDoc.getDocumentElement();
		respEle.setAttribute("ResponseCode", "ERROR");
		respEle.setAttribute("ResponseDescription", "No Record Found");
	    }

	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCChangeOrderStatusOnNPRUdpate.changeOrderStatusOnNPRUpdate()",
		    e);
	    throw new GCException(e);
	}
	LOGGER.verbose(" Exiting GCChangeOrderStatusOnNPRUdpate.changeOrderStatusOnNPRUpdate() method ");
	return respDoc;
    }

    public void setProperties(Properties arg0) {

    }

}
