/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class close the receipt.
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0             14/05/2014        Soni, Karan		          Initial Version
 *#################################################################################################################################################################
 */
package com.gc.api;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author karan
 */
public class GCCloseReceipt {
    // initialize logger
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCCloseReceipt.class);
	
    /**
     * 
     * This method closes the receipt on receipt create on Success
     * 
     * @param env
     * @param inDocument
     * @return
     * 
     */
    public Document closeReceipt(YFSEnvironment env, Document inDocument) {
	try {
	    LOGGER.verbose("Entering the closeReceipt method");
	    Element eleRoot = inDocument.getDocumentElement();
	    String strReceiptHeaderKey = eleRoot
		    .getAttribute(GCConstants.RECEIPT_HEADER_KEY);
	    String strDocumentType = GCXMLUtil.getAttributeFromXPath(eleRoot,
		    "Shipment/@DocumentType");
	    String strEnterpriseCode = eleRoot
		    .getAttribute(GCConstants.ENTERPRISE_CODE);
	    Element eleShipment = (Element) XPathAPI.selectSingleNode(eleRoot,
		    GCConstants.SHIPMENT);
	    String strOrderNo = eleShipment.getAttribute(GCConstants.ORDER_NO);
	    String strShipmentNo = eleShipment
		    .getAttribute(GCConstants.SHIPMENT_NO);
	    String strCloseReceipt = "<Receipt DocumentType='"
		    + strDocumentType + "' ReceiptHeaderKey='"
		    + strReceiptHeaderKey + "'><Shipment DocumentType='"
		    + strDocumentType + "' EnterpriseCode='"
		    + strEnterpriseCode + "' OrderNo='" + strOrderNo
		    + "' ShipmentNo='" + strShipmentNo + "'/></Receipt>";
	    Document inDocCloseReceipt = GCXMLUtil.getDocument(strCloseReceipt);
	    if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug("The xml for closeReceipt is"
		    + GCXMLUtil.getXMLString(inDocCloseReceipt));
	    }
	    GCCommonUtil.invokeAPI(env, GCConstants.CLOSE_RECEIPT,
		    inDocCloseReceipt);
	    LOGGER.verbose("Exiting the closeReceipt method");
	} catch (Exception e) {
	    LOGGER.verbose("Entering the exception inside closeReceipt:",e);
	   	}
	return inDocument;
    }
}
