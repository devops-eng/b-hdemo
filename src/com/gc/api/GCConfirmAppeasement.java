package com.gc.api;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCConfirmAppeasement implements YIFCustomApi {

  YFSEnvironment env = null;
  Document inDoc = null;
  Boolean isValidAppeasement = false;
  String sMaxRefund = GCConstants.BLANK_STRING;
  String sOrderHeaderKey = null;
  String sExtnIsRefundOnAccount = null;
  String sReasonCode=null;
  String sUserID = null;
  String sUserAction = null;
  String sReference1 = "";
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCConfirmAppeasement.class);
  private String sOriginalOfferAmount = "";
  @Override
  public void setProperties(Properties arg0) throws GCException {

  }

  public Document confirmAppeasement(YFSEnvironment env, Document inDoc) throws GCException {
    try {
      final String logInfo = "GCConfirmAppeasement :: confirmAppeasement :: ";
      LOGGER.verbose(logInfo + "Begin");
      LOGGER.debug("Incoming XML" + GCXMLUtil.getXMLString(inDoc));

      this.env = env;
      this.inDoc = inDoc;
      String sGCAppeaseType = XPathAPI.selectSingleNode(inDoc, "//@GCAppeaseType").getNodeValue();
      // Fix for 3114
      sReference1 = inDoc.getDocumentElement().getAttribute("Reference1");
      sReasonCode=XPathAPI.selectSingleNode(inDoc,"//@InvoiceCreationReason").getNodeValue();
      sOrderHeaderKey = XPathAPI.selectSingleNode(inDoc, "//@OrderHeaderKey").getNodeValue();
      sExtnIsRefundOnAccount = XPathAPI.selectSingleNode(inDoc, "//@ExtnIsRefundOnAccount").getNodeValue();
      sOriginalOfferAmount = XPathAPI.selectSingleNode(inDoc,
          "//@OfferAmount").getNodeValue();
      sUserID = XPathAPI.selectSingleNode(inDoc, "//@UserID").getNodeValue();
      sUserAction = XPathAPI.selectSingleNode(inDoc, "//@UserAction").getNodeValue();

      Float sFloatOfferAmount = Float.parseFloat(sOriginalOfferAmount);

      Element inDocRoot = inDoc.getDocumentElement();
      inDocRoot.removeAttribute("AppeasementType");

      Document docOrder = GCXMLUtil.createDocument("Order");
      Element eleOrderRoot = docOrder.getDocumentElement();
      eleOrderRoot.setAttribute("OrderHeaderKey", sOrderHeaderKey);
      Document docGetOrderDetail =
          GCCommonUtil.invokeAPI(env, docOrder, GCConstants.API_GET_ORDER_DETAILS,
              GCConstants.GET_ORDER_DETAILS_TEMP_FOR_CONFIRM_APPEASEMENT);

      Element orderInvoiceEle = (Element) XPathAPI.selectSingleNode(inDoc, "//OrderInvoice");
      NodeList nlLineDetailList = XPathAPI.selectNodeList(inDoc, "//LineDetails/LineDetail");
      Element headerChargeEle = (Element) XPathAPI.selectSingleNode(orderInvoiceEle, "//HeaderChargeList");
      orderInvoiceEle.removeChild(headerChargeEle);
      for (int i = 0; i < nlLineDetailList.getLength(); i++) {
        Element eleLineDetail = (Element) nlLineDetailList.item(i);
        String sOrderLineKey = eleLineDetail.getAttribute(GCConstants.ORDER_LINE_KEY);

        if (YFCCommon.equals("SHIPPING", sGCAppeaseType)) {
          amountToRefundForShippingLevel(sFloatOfferAmount,
              sOrderLineKey, docGetOrderDetail);
        } else {
          amountToRefundForLineLevel(sFloatOfferAmount,
              sOrderLineKey, inDoc, docGetOrderDetail);
        }
      }

      inDocRoot.removeAttribute(GCConstants.OFFER_AMOUNT);
      createSeparateInvoiceForShipNode(inDoc, docGetOrderDetail);

      Document docReturn = GCXMLUtil.createDocument(GCConstants.ORDER_INVOICE);
      Element eleRoot = docReturn.getDocumentElement();
      eleRoot.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);

      // Manager Approval
      YFCDocument outDoc = GCCommonUtil.userGroupList(env, sUserID);
      YFCElement eleRoot1 = outDoc.getDocumentElement();

      String sExtnEmpID = eleRoot1.getAttribute(GCConstants.EXTN_EMPLOYEE_ID);

      YFCDocument userActivityDoc = YFCDocument.createDocument("UserActivity");
      YFCElement eleUserActivity = userActivityDoc.getDocumentElement();
      eleUserActivity.setAttribute(GCConstants.USER_ID, sExtnEmpID);
      eleUserActivity.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
      eleUserActivity.setAttribute("UserAction", sUserAction);
      GCCommonUtil.invokeService(env, GCConstants.GC_INSERT_SHPMNT_ACTIVITY, userActivityDoc.getDocument());
      // Manager Approval end.

      return docReturn;
    } catch (Exception e) {
      LOGGER.error("The exception is in confirmAppeasement method:", e);
      throw new GCException(e);
    }
  }

  /**
   * This method is used to create separate invoice for each ship node
   *
   * @param inDoc
   * @param docGetOrderDetail
   */
  private void createSeparateInvoiceForShipNode(Document inDoc, Document docGetOrderDetail) {


    LOGGER.beginTimer("GCConfirmAppeasement.createSeparateInvoiceForShipNode");
    LOGGER.verbose("Class: GCConfirmAppeasement Method: createSeparateInvoiceForShipNode BEGIN");
    LOGGER.debug("Incoming inDoc XML " + GCXMLUtil.getXMLString(inDoc));
    LOGGER.debug("Incoming docGetOrderDetail XML " + GCXMLUtil.getXMLString(docGetOrderDetail));
    YFCDocument docYFCGetOrderDetail = YFCDocument.getDocumentFor(docGetOrderDetail);

    YFCNodeList<YFCElement> listOrderLine = docYFCGetOrderDetail.getElementsByTagName(GCConstants.ORDER_LINE);

    Map<String, List<String>> mapLineKeyAndShipNode = new HashMap<String, List<String>>();
    int iLengthOrderLine = listOrderLine.getLength();
    for (int iCounterOrderLine = 0; iCounterOrderLine < iLengthOrderLine; iCounterOrderLine++) {
      YFCElement eleOrderLine = listOrderLine.item(iCounterOrderLine);
      String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      YFCElement eleOrderStatuses = eleOrderLine.getChildElement(GCConstants.ORDER_STATUSES);
      YFCElement eleOrderStatus = eleOrderStatuses.getChildElement(GCConstants.ORDER_STATUS);
      String sShipNode = eleOrderStatus.getAttribute(GCConstants.SHIP_NODE);
      if (!YFCCommon.isVoid(sShipNode)) {
        List<String> listOrderLineKey = mapLineKeyAndShipNode.get(sShipNode);
        if(YFCCommon.isVoid(listOrderLineKey)) {
          listOrderLineKey = new ArrayList<String>();

        }
        listOrderLineKey.add(sOrderLineKey);
        listOrderLineKey =  mapLineKeyAndShipNode.put(sShipNode,listOrderLineKey );
      }
    }

    YFCDocument docYFCInvoicDoc = YFCDocument.getDocumentFor(inDoc);
    YFCNodeList<YFCElement> lineDetail = docYFCInvoicDoc.getElementsByTagName(GCConstants.LINE_DETAIL);

    Map<String, YFCElement> mapLineDetail = new HashMap<String, YFCElement>();

    int iLengthLineDetail = lineDetail.getLength();
    for (int iCounterLineDetail = 0; iCounterLineDetail < iLengthLineDetail; iCounterLineDetail++) {
      YFCElement eleLineDetail = lineDetail.item(iCounterLineDetail);
      String sOrderLineKey = eleLineDetail.getAttribute(GCConstants.ORDER_LINE_KEY);

      mapLineDetail.put(sOrderLineKey, eleLineDetail);

    }

    Set entries = mapLineKeyAndShipNode.entrySet();
    Iterator iterator = entries.iterator();
    while (iterator.hasNext()) {
      Map.Entry entry = (Map.Entry) iterator.next();
      String key = (String) entry.getKey();
      Document docInCreRecInvoice =
          GCXMLUtil
          .getDocument("<OrderInvoice Reference1='"
              + "' IgnoreOrdering='Y' InvoiceType='CREDIT_MEMO' InvoiceCreationReason=''"
              + " OrderHeaderKey='' UseOrderLineCharges='N'><LineDetails></LineDetails><Extn ExtnIsRefundOnAccount='N'/>"
              + "</OrderInvoice>");
      YFCDocument yfcDocInRecInvoice = YFCDocument.getDocumentFor(docInCreRecInvoice);
      YFCElement eleRoot = yfcDocInRecInvoice.getDocumentElement();
      eleRoot.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
      eleRoot.setAttribute("InvoiceCreationReason", sReasonCode);
      eleRoot.setAttribute("Reference1", sReference1);
      YFCElement eleExtn = eleRoot.getChildElement(GCConstants.EXTN);
      eleExtn.setAttribute(GCConstants.EXTN_IS_REFUND_ON_ACCOUNT, sExtnIsRefundOnAccount);
      YFCElement eleLineDetails = eleRoot.getChildElement(GCConstants.LINE_DETAILS);
      Boolean bIsApiCallRequired = false;
      List<String> listOrderLineKey = mapLineKeyAndShipNode.get(key);
      for(int i=0; i< listOrderLineKey.size(); i++) {
        String sOrderLineKey = listOrderLineKey.get(i);
        YFCElement eleLineDetail = mapLineDetail.get(sOrderLineKey);

        if(!YFCCommon.isVoid(eleLineDetail)) {
          YFCNode eleInDocLineDetail = yfcDocInRecInvoice.importNode(eleLineDetail, true);

          eleRoot.setAttribute(GCConstants.SHIP_NODE, key);
          eleLineDetails.appendChild(eleInDocLineDetail);
          bIsApiCallRequired = true;
        }
      }
      if(bIsApiCallRequired) {

        GCCommonUtil.invokeAPI(env, GCConstants.API_RECORD_INVOICE_CREATION, yfcDocInRecInvoice, null);
      }
    }
    LOGGER.verbose("Class: GCConfirmAppeasement Method: createSeparateInvoiceForShipNode END");
    LOGGER.endTimer("GCConfirmAppeasement.createSeparateInvoiceForShipNode");
  }

  /**
   * This method determines the shipping level appeasement considering charges
   *
   * @param fOfferAmount
   * @param orderLineKey
   * @param inDoc2
   * @param getOrderDet
   * @throws TransformerException
   */
  private void amountToRefundForShippingLevel(Float fOfferAmount, String orderLineKey,
      Document getOrderDet) throws TransformerException {


    LOGGER.beginTimer("GCConfirmAppeasement.amountToRefundForShippingLevel");
    LOGGER.verbose("Class: GCConfirmAppeasement Method: amountToRefundForShippingLevel BEGIN");
    
  //GCSTORE-5934-starts
    String strParentOrderLineKey=null;
    Element eleBundleParentLine=GCXMLUtil.getElementByXPath(getOrderDet, "/Order/OrderLines/OrderLine[@OrderLineKey='"+orderLineKey+"']/BundleParentLine");
    if(!YFCCommon.isVoid(eleBundleParentLine))
    {
        strParentOrderLineKey=eleBundleParentLine.getAttribute("OrderLineKey"); 
    }
  
    LOGGER.debug("GCConfirmAppeasement :: amountToRefundForShippingLevel :: strParentOrderLineKey :: "+strParentOrderLineKey);
    Element eleorderLine=GCXMLUtil.getElementByXPath(getOrderDet, "/Order/OrderLines/OrderLine[@OrderLineKey='"+orderLineKey+"']");
    
    if(!YFCCommon.isVoid(eleorderLine))
    {
        LOGGER.debug("GCConfirmAppeasement :: amountToRefundForShippingLevel :: eleorderLine :: \n "+GCXMLUtil.getElementString(eleorderLine));
        
    }
  //GCSTORE-5934-ends
    
    Float fAmountToRefund = (float) 0.0;
    Element eleLineCharge =
        (Element) XPathAPI.selectSingleNode(getOrderDet, "Order/OrderLines/OrderLine[@OrderLineKey='" + orderLineKey
            + "']/LineCharges/LineCharge[@ChargeName='ShippingCharge']");
    Element eleLineTax =
        (Element) XPathAPI.selectSingleNode(getOrderDet, "Order/OrderLines/OrderLine[@OrderLineKey='" + orderLineKey
            + "']/LineTaxes/LineTax[@ChargeName='ShippingCharge']");

    String sShippingCharge = "0.00";
    String sShippingTax = "0.00";
    // Fix to stamp TaxPercentage
    String sShippingTaxPercentage = "0.00";
    
    if (!YFCCommon.isVoid(eleLineCharge)) {
      sShippingCharge = eleLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT);
    }
    if (!YFCCommon.isVoid(eleLineTax)) {
      sShippingTax = eleLineTax.getAttribute(GCConstants.TAX);
      sShippingTaxPercentage = eleLineTax.getAttribute(GCConstants.TAX_PERCENTAGE);
      if(YFCCommon.isVoid(sShippingTaxPercentage)){
    	  sShippingTaxPercentage = "0.00";
      }
    }
    
    
  //GCSTORE-5934-starts
    if(!YFCCommon.isVoid(strParentOrderLineKey))
    {
       LOGGER.debug("GCConfirmAppeasement :: amountToRefundForShippingLevel :: It is kit component ");
       Element eleExtn=(Element) eleorderLine.getElementsByTagName("Extn").item(0);
       if(!YFCCommon.isVoid(eleExtn))
       {
           LOGGER.debug("GCConfirmAppeasement :: amountToRefundForShippingLevel :: eleExtn :: \n"+GCXMLUtil.getElementString(eleExtn));
           Document docExtn=GCXMLUtil.getDocumentFromElement(eleExtn);   
           sShippingCharge=GCXMLUtil.getAttributeFromXPath(docExtn, "/Extn/GCOrderLineComponentsList/GCOrderLineComponents[@ChargeCategory='Shipping'][@ChargeName='ShippingCharge']/@Amount"); 
           sShippingTax=GCXMLUtil.getAttributeFromXPath(docExtn, "/Extn/GCOrderLineComponentsList/GCOrderLineComponents[@ChargeCategory='Shipping'][@ChargeName='ShippingChargeTax']/@Amount");       
           sShippingTaxPercentage=GCXMLUtil.getAttributeFromXPath(getOrderDet,"Order/OrderLines/OrderLine[@OrderLineKey='" + strParentOrderLineKey
                    + "']/LineTaxes/LineTax[@ChargeName='ShippingCharge']/@TaxPercentage");
           if(YFCCommon.isVoid(sShippingCharge))
           {
               sShippingCharge="0.00";
           }
           if(YFCCommon.isVoid(sShippingTax))
           {
               sShippingTax="0.00";
           }
           if(YFCCommon.isVoid(sShippingTaxPercentage))
           {
               sShippingTaxPercentage="0.00";
           }
           LOGGER.debug("GCConfirmAppeasement :: amountToRefundForShippingLevel :: sShippingCharge :: "+sShippingCharge+" :: sShippingTaxPercentage ::"+sShippingTaxPercentage);
       }
    }
  //GCSTORE-5934-ends  
    
    Float fShippingCharge = Float.parseFloat(sShippingCharge);
    Float fShippingTax = Float.parseFloat(sShippingTax);
    Float fTax=(float)0.0;
    fAmountToRefund = fOfferAmount;
    if (fShippingTax!= 0) {
      fTax=((fOfferAmount / fShippingCharge) * fShippingTax);
    }
    BigDecimal bgTax = new BigDecimal(fTax);
    bgTax = bgTax.setScale(2, BigDecimal.ROUND_HALF_UP);
    String sTax = "-" + bgTax.toString();
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleRoot = inputDoc.getDocumentElement();
    YFCElement eleLineDetails=eleRoot.getChildElement(GCConstants.LINE_DETAILS);
    YFCElement eleLineDetail=eleLineDetails.getChildElement(GCConstants.LINE_DETAIL);
    if(!YFCCommon.isVoid(eleLineDetail)){
      YFCElement eleLineTaxList=eleLineDetail.createChild(GCConstants.LINE_TAX_LIST);
      YFCElement eleLinetax=eleLineTaxList.createChild(GCConstants.LINE_TAX);
      eleLinetax.setAttribute(GCConstants.CHARGE_CATEGORY, "CUSTOMER_APPEASEMENT");
      eleLinetax.setAttribute(GCConstants.CHARGE_NAME, "SHIPPING_APPEASEMENT");
      eleLinetax.setAttribute(GCConstants.TAX, sTax);
      eleLinetax.setAttribute(GCConstants.TAX_PERCENTAGE, sShippingTaxPercentage);
      
    }

    BigDecimal bgRefund = new BigDecimal(fAmountToRefund);
    bgRefund = bgRefund.setScale(2, BigDecimal.ROUND_HALF_UP);
    Element eLineCharge =
        (Element) XPathAPI.selectSingleNode(inDoc, "OrderInvoice/LineDetails/LineDetail[@OrderLineKey='"
            + orderLineKey + "']/LineChargeList/LineCharge");
    String sAmountToRefund = "-" + bgRefund.toString();
    eLineCharge.setAttribute(GCConstants.CHARGE_PER_LINE, sAmountToRefund);
    eLineCharge.setAttribute(GCConstants.REFERENCE, sOriginalOfferAmount);

    LOGGER.verbose("Class: GCConfirmAppeasement Method: amountToRefundForShippingLevel END");
    LOGGER.endTimer("GCConfirmAppeasement.amountToRefundForShippingLevel");
  }

  /**
   * Description: This method checks the amount to refund for line level
   *
   * @param Offer Amount
   * @param Order Line Key
   * @param InDoc
   * @throws GetOrderList Doc
   */
  private void amountToRefundForLineLevel(Float fOfferAmount, String orderLineKey, Document inDoc, Document getOrderDet)
      throws TransformerException {

    LOGGER.beginTimer("GCConfirmAppeasement.amountToRefundForLineLevel");
    LOGGER.verbose("Class: GCConfirmAppeasement Method: amountToRefundForLineLevel BEGIN");

    //GCSTORE-5934-starts
    String strParentOrderLineKey=null;
    Element eleBundleParentLine=GCXMLUtil.getElementByXPath(getOrderDet, "/Order/OrderLines/OrderLine[@OrderLineKey='"+orderLineKey+"']/BundleParentLine");
    if(!YFCCommon.isVoid(eleBundleParentLine))
    {
        strParentOrderLineKey=eleBundleParentLine.getAttribute("OrderLineKey"); 
    }
  
    LOGGER.debug("GCConfirmAppeasement :: amountToRefundForLineLevel :: strParentOrderLineKey :: "+strParentOrderLineKey);
    
    Element eleorderLine=GCXMLUtil.getElementByXPath(getOrderDet, "/Order/OrderLines/OrderLine[@OrderLineKey='"+orderLineKey+"']");
    if(!YFCCommon.isVoid(eleorderLine))
    {
        LOGGER.debug("GCConfirmAppeasement :: amountToRefundForLineLevel :: eleorderLine :: \n "+GCXMLUtil.getElementString(eleorderLine));
        
    }
    
  //GCSTORE-5934-ends

    Float fAmountToRefund = (float) 0.0;
    Element eleLineTax =
        (Element) XPathAPI.selectSingleNode(getOrderDet, "/Order/OrderLines/OrderLine[@OrderLineKey='" + orderLineKey
            + "']/LineTaxes/LineTax[@ChargeName='LinePriceTax']");

    Element eleLineOverallTotal =
        (Element) XPathAPI.selectSingleNode(getOrderDet, "Order/OrderLines/OrderLine[@OrderLineKey='" + orderLineKey
            + "']/LineOverallTotals");

    Element eleLineCharge =
        (Element) XPathAPI.selectSingleNode(getOrderDet, "Order/OrderLines/OrderLine[@OrderLineKey='" + orderLineKey
            + "']/LineCharges/LineCharge[@ChargeName='ShippingCharge']");

    String sShippingCharge = "0.00";
    //GCSTORE-5934---Start
    String sLineTax;
    //GCSTORE-5934--End
    if (!YFCCommon.isVoid(eleLineCharge)) {
      sShippingCharge = eleLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT);
    }
    Float fLineTax =(float)0.0;
    Float fTaxPercentage = (float)0.0;
    Float fLineTotalWithoutTax = (float)0.0;
    Float fShippingCharge = Float.parseFloat(sShippingCharge);
    String sTaxPercentage;
    if(!YFCCommon.isVoid(eleLineTax)){
	    sLineTax = eleLineTax.getAttribute(GCConstants.TAX);
	    sTaxPercentage=eleLineTax.getAttribute(GCConstants.TAX_PERCENTAGE);
	    ;
	   
	    fLineTax = Float.parseFloat(sLineTax);
	    fTaxPercentage =Float.parseFloat(sTaxPercentage);
	    
    }
    String sLineTotalWithoutTax = eleLineOverallTotal.getAttribute(GCConstants.LINE_TOTAL_WITHOUT_TAX);
    fLineTotalWithoutTax = Float.parseFloat(sLineTotalWithoutTax);
    Float fLineToatlWithoutShipping=fLineTotalWithoutTax-fShippingCharge;
 //GCSTORE-5934-starts
    
    if(!YFCCommon.isVoid(strParentOrderLineKey))
    {
        LOGGER.debug("GCConfirmAppeasement :: amountToRefundForLineLevel :: It is kit component ");
       Element eleExtn=(Element) eleorderLine.getElementsByTagName("Extn").item(0);
       if(!YFCCommon.isVoid(eleExtn))
       {
           LOGGER.debug("GCConfirmAppeasement :: amountToRefundForLineLevel :: eleExtn :: \n"+GCXMLUtil.getElementString(eleExtn));
           Document docExtn=GCXMLUtil.getDocumentFromElement(eleExtn);   
           sShippingCharge=GCXMLUtil.getAttributeFromXPath(docExtn, "/Extn/GCOrderLineComponentsList/GCOrderLineComponents[@ChargeCategory='Shipping'][@ChargeName='ShippingCharge']/@Amount"); 
           if(!YFCCommon.isVoid(sShippingCharge))
           {
               fShippingCharge = Float.parseFloat(sShippingCharge);
           }
       
           
           sLineTax=GCXMLUtil.getAttributeFromXPath(docExtn, "/Extn/GCOrderLineComponentsList/GCOrderLineComponents[@ChargeCategory='LinePrice'][@ChargeName='LinePriceTax']/@Amount");       
           
           if(!YFCCommon.isVoid(sLineTax))
           {
               fLineTax = Float.parseFloat(sLineTax);
           }
           sTaxPercentage=GCXMLUtil.getAttributeFromXPath(getOrderDet,"Order/OrderLines/OrderLine[@OrderLineKey='" + strParentOrderLineKey
                    + "']/LineTaxes/LineTax[@ChargeName='LinePriceTax']/@TaxPercentage");
          
           if(!YFCCommon.isVoid(sTaxPercentage))
           {
               fTaxPercentage = Float.parseFloat(sTaxPercentage);
           }
          
          Float amount=Float.parseFloat(GCXMLUtil.getAttributeFromXPath(docExtn, "/Extn/GCOrderLineComponentsList/GCOrderLineComponents[@ChargeName='UnitPrice'][@ChargeCategory='UnitPrice']/@Amount"));
          Float quantity=Float.parseFloat(GCXMLUtil.getAttributeFromXPath(docExtn, "/Extn/GCOrderLineComponentsList/GCOrderLineComponents[@ChargeName='UnitPrice'][@ChargeCategory='UnitPrice']/@Quantity"));
          
          if(!YFCCommon.isVoid(amount) && !YFCCommon.isVoid(quantity) )
          {
              fLineToatlWithoutShipping=amount*quantity;
          }
          LOGGER.debug("GCConfirmAppeasement :: amountToRefundForLineLevel :: sShippingCharge :: "+sShippingCharge+" :: sLineTax ::"+sLineTax+":: sTaxPercentage ::"+sTaxPercentage+ ":: amount ::"+amount+" :: quantity ::"+quantity);
          
       }
    }
    
    //GCSTORE-5934-ends

    Float fTax=(float)0.0;
    fAmountToRefund = fOfferAmount;
    if (fLineTotalWithoutTax!= 0) {
      fTax=((fOfferAmount / fLineToatlWithoutShipping) * fLineTax);
    }
    BigDecimal bgTaxPercentage=new BigDecimal(fTaxPercentage);
    bgTaxPercentage=bgTaxPercentage.setScale(2,BigDecimal.ROUND_HALF_UP);
    sTaxPercentage=bgTaxPercentage.toString();
    
    BigDecimal bgTax = new BigDecimal(fTax);
    bgTax = bgTax.setScale(2, BigDecimal.ROUND_HALF_UP);
    String sTax = "-" + bgTax.toString();
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleRoot = inputDoc.getDocumentElement();
    YFCElement eleLineDetails=eleRoot.getChildElement(GCConstants.LINE_DETAILS);
    YFCElement eleLineDetail=eleLineDetails.getChildElement(GCConstants.LINE_DETAIL);
    if(!YFCCommon.isVoid(eleLineDetail)){
      YFCElement eleLineTaxList=eleLineDetail.createChild(GCConstants.LINE_TAX_LIST);
      YFCElement eleLinetax=eleLineTaxList.createChild(GCConstants.LINE_TAX);
      eleLinetax.setAttribute(GCConstants.CHARGE_CATEGORY, GCConstants.CUSTOMER_APPEASEMENT);
      eleLinetax.setAttribute(GCConstants.CHARGE_NAME, GCConstants.CUSTOMER_APPEASEMENT);
      eleLinetax.setAttribute(GCConstants.TAX, sTax);
      eleLinetax.setAttribute(GCConstants.TAX_PERCENTAGE,sTaxPercentage);
    }

    BigDecimal bgRefund = new BigDecimal(fAmountToRefund);
    bgRefund = bgRefund.setScale(2, BigDecimal.ROUND_HALF_UP);
    Element eLineCharge =
        (Element) XPathAPI.selectSingleNode(inDoc, "OrderInvoice/LineDetails/LineDetail[@OrderLineKey='" + orderLineKey
            + "']/LineChargeList/LineCharge");
    String sAmountToRefund = "-" + bgRefund.toString();
    eLineCharge.setAttribute(GCConstants.CHARGE_PER_LINE, sAmountToRefund);
    eLineCharge.setAttribute(GCConstants.REFERENCE, sOriginalOfferAmount);

    LOGGER.verbose("Class: GCConfirmAppeasement Method: amountToRefundForLineLevel END");
    LOGGER.endTimer("GCConfirmAppeasement.amountToRefundForLineLevel");
  }
}
