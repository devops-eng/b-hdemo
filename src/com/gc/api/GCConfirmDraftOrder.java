/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Handle the functionalities required for COMPARING ORDER DATE at Draft Creation with order confirmation
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               05/02/2015        Soni, Karan    		OMS-482 Buyer Remorse Hold clearing 30 min after beginning order instead of completing             
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.xerces.util.XMLCatalogResolver;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.comergent.api.xml.XMLUtils;
import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCConfirmDraftOrder {

	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCCreateReturnOnSuccess.class);

	public void validateOrderDate(YFSEnvironment env, Document inDoc)
			throws GCException {
		try {
				LOGGER.verbose("Entering the validateOrderDate method");
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("com.gc.api.GCConfirmDraftOrder.validateOrderDate" + GCXMLUtil.getXMLString(inDoc));
				}
				Element eleOrder= inDoc.getDocumentElement();
				String strOrderHeaderKey= eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
				String strOrderDate= eleOrder.getAttribute(GCConstants.ORDER_DATE);
				Date orderDate = new SimpleDateFormat(GCConstants.DATE_TIME_FORMAT)
			    .parse(strOrderDate);
				Calendar calSysDate = Calendar.getInstance();
				Calendar calDraftOrderTime = Calendar.getInstance();
				calDraftOrderTime.setTime(orderDate);
				LOGGER.debug("The OrderHeaderKey is===="+ strOrderHeaderKey+ "The OrderDate is ===="+ strOrderDate+ "The Calendar Date is ===="+ calSysDate.getTime().toString());
				
				Document inDocCO =GCXMLUtil.createDocument(GCConstants.ORDER);
				YFCDocument docChangeOrd = YFCDocument.getDocumentFor(inDocCO);
				YFCElement elechangeOrder = docChangeOrd.getDocumentElement();
				elechangeOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, strOrderHeaderKey);
				elechangeOrder.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
				boolean chngOrderCallToUpdtDate = false;
				boolean hasPriceLockStamped = true;
				if(calSysDate.getTime().compareTo(calDraftOrderTime.getTime()) > 0) {
					LOGGER.debug("Entering the If Current time different from Order Date");
					SimpleDateFormat dateFormat = new SimpleDateFormat(
							GCConstants.DATE_TIME_FORMAT);
					String strModifiedOD = dateFormat.format(calSysDate
							.getTime());
					elechangeOrder.setAttribute(GCConstants.ORDER_DATE, strModifiedOD);
					//Document inDocCO= GCXMLUtil.getDocument("<Order OrderHeaderKey='"+strOrderHeaderKey+"' Override='Y' OrderDate='"+strModifiedOD+"'/>");
				//	YFCDocument docChangeOrd = YFCDocument.getDocumentFor(inDocCO);
                   
                    Element eleExtn = (Element) eleOrder.getElementsByTagName(GCConstants.EXTN).item(0);
                    if (!YFCCommon.isVoid(eleExtn)) {
                     String orderCaptureChannel = eleExtn.getAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL);
                     if (YFCCommon.equals(orderCaptureChannel, GCConstants.CALL_CENTER_ORDER_CHANNEL)
                         || YFCCommon.equals(orderCaptureChannel, GCConstants.GCSTORE)) {
                    	 hasPriceLockStamped = false;
                      stampIsPriceLocked(inDoc, docChangeOrd);
                     }
                   }
                    chngOrderCallToUpdtDate= true;
                    LOGGER.debug("Exiting the If Current time different from Order Date");
				}
				
				if(hasPriceLockStamped){
					LOGGER.debug("Flow hasn't gone inside DateTime condition");
					Element eleExtn = (Element) eleOrder.getElementsByTagName(GCConstants.EXTN).item(0);
                    if (!YFCCommon.isVoid(eleExtn)) {
                     String orderCaptureChannel = eleExtn.getAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL);
                     if (YFCCommon.equals(orderCaptureChannel, GCConstants.CALL_CENTER_ORDER_CHANNEL)
                         || YFCCommon.equals(orderCaptureChannel, GCConstants.GCSTORE)) {
                    	 hasPriceLockStamped = false;
                    	 stampIsPriceLocked(inDoc, docChangeOrd);
                     }
                   }
                    YFCDocument tempDoc = YFCDocument.createDocument(GCConstants.ORDER);
					LOGGER.debug("The inDocument for ChangeOrder is 2" + docChangeOrd.toString());
                    GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, docChangeOrd, tempDoc);
				}
				
				boolean chngOrdCallToUpdateSoftwareLine = copyPrimySuplrToShipNodeForSoftwareLN(inDoc , docChangeOrd);
				// GCSTORE-5128 - OMS is sending Software skus to HJ in Release.
				if(chngOrderCallToUpdtDate || chngOrdCallToUpdateSoftwareLine){
					 YFCDocument tempDoc = YFCDocument.createDocument(GCConstants.ORDER);
					LOGGER.debug("The inDocument for ChangeOrder is" + docChangeOrd.toString());
                    GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, docChangeOrd, tempDoc);
				}
				
		}catch(Exception e){
			LOGGER.error("Inside GCConfirmDraftOrder.validateOrderDate():",e);
			throw new GCException(e);
		}

	}
	/**
	 * This method will get the PrimarySupplier from ItemDetail element of software order line only
	 * and stamp it to shipNode at line level of change order.
	 * @param inDoc
	 * @param docChangeOrd
	 * @return
	 */
	public boolean copyPrimySuplrToShipNodeForSoftwareLN(Document inDoc,YFCDocument docChangeOrd){
		LOGGER.debug(" Begin copyPrimySuplrToShipNodeForSoftwareLN changeOrderDoc " + docChangeOrd);
		YFCElement chngeOrdRootEle = docChangeOrd.getDocumentElement();
		YFCElement eleChngOrdLines = docChangeOrd.createElement(GCConstants.ORDER_LINES);
		chngeOrdRootEle.appendChild(eleChngOrdLines);
		boolean chngOrderApiCallReq = false;
		NodeList nlOrderLine = GCXMLUtil.getNodeListByXpath(inDoc, "Order/OrderLines/OrderLine");
		int iOrderLineLen = nlOrderLine.getLength();
		YFCNodeList<YFCElement> nlChngeOrderLine = chngeOrdRootEle.getElementsByTagName(GCConstants.ORDER_LINE);
		for(int orderLine = 0; orderLine < iOrderLineLen; orderLine++){
			Element eleOrderLine = (Element) nlOrderLine.item(orderLine);
			Element eleItem = (Element) eleOrderLine.getElementsByTagName(GCConstants.ITEM).item(0);
			String strProductLine = eleItem.getAttribute("ProductLine");
			String strBundleParentOLKey = eleOrderLine.getAttribute(GCConstants.BUNDLE_PARENT_ORDERLINEKEY);
			//If order line contains software item and also this line is a component then only stamp primarySupplier to shipNode.
			if("PV".equalsIgnoreCase(strProductLine) && null != strBundleParentOLKey 
					&& !GCConstants.BLANK.equalsIgnoreCase(strBundleParentOLKey) ){
				String strOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
				Element eleItemDetail = (Element) eleOrderLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
				Element elePrimaryInfo = (Element) eleItemDetail.getElementsByTagName(GCConstants.PRIMARY_INFORMATION).item(0);
				String strPrimarySupplier = elePrimaryInfo.getAttribute("PrimarySupplier");
				boolean findOrdLineInChngOrd = false;
				for(YFCElement eleChngeOrderLine :nlChngeOrderLine){
				   String strChngOrdLineKey = eleChngeOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
				  if(strOrderLineKey.equalsIgnoreCase(strChngOrdLineKey)){
						eleChngeOrderLine.setAttribute(GCConstants.SHIP_NODE, strPrimarySupplier);
						eleChngeOrderLine.setAttribute("IsFirmPredefinedNode", "Y");
						findOrdLineInChngOrd = true;
						break;
					}
				}
				if(!findOrdLineInChngOrd){
				YFCElement eleChngOrdLine = docChangeOrd.createElement(GCConstants.ORDER_LINE);
				eleChngOrdLine.setAttribute(GCConstants.ORDER_LINE_KEY,strOrderLineKey);
				eleChngOrdLine.setAttribute(GCConstants.SHIP_NODE, strPrimarySupplier);
				eleChngOrdLine.setAttribute("IsFirmPredefinedNode", "Y");
				eleChngOrdLines.appendChild(eleChngOrdLine);
				}
				chngOrderApiCallReq = true;
			}
		}
		LOGGER.debug(" End copyPrimySuplrToShipNodeForSoftwareLN changeOrderDoc " + docChangeOrd);
		return chngOrderApiCallReq;
	}
	
	private void stampIsPriceLocked(Document inDoc, YFCDocument docChangeOrd) {
    NodeList nlOrderLine = GCXMLUtil.getNodeListByXpath(inDoc, "Order/OrderLines/OrderLine");
    YFCElement eleChngOrd = docChangeOrd.getDocumentElement();
    YFCElement eleOrderLinesIp = null;
    for (int iLineCount = 0; iLineCount < nlOrderLine.getLength(); iLineCount++) {
      Element eleOrderLine = (Element) nlOrderLine.item(iLineCount);
      if (YFCCommon.isVoid(eleOrderLinesIp)) {
        eleOrderLinesIp = eleChngOrd.createChild(GCConstants.ORDER_LINES);
      }
      String orderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      YFCElement eleOrderLineIp = eleOrderLinesIp.createChild(GCConstants.ORDER_LINE);
      eleOrderLineIp.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey);
      YFCElement eleLinePriceInfoIp = eleOrderLineIp.createChild(GCConstants.LINE_PRICE_INFO);
      eleLinePriceInfoIp.setAttribute(GCConstants.IS_PRICE_LOCKED, GCConstants.YES);
    }

  }
}
    
