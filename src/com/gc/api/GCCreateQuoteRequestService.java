package com.gc.api;


import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCWebServiceUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCCreateQuoteRequestService {


	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCCreateQuoteRequestService.class);

	private ArrayList<String> lOrderLineKey;
	/**
	 * This method will prepare the create Quote atg request and will call atg webservice. 
	 * Based on the response it will modify inDoc and will call change order.
	 * @param env
	 * @param inDoc
	 * @return
	 * @throws TransformerException
	 * @throws IOException
	 */
	public Document createQuoteService(YFSEnvironment env, Document inDoc) throws TransformerException {

		LOGGER.debug("GCCreateQuoteRequestService :: createQuoteService :: Begin");
		LOGGER.debug("GCCreateQuoteRequestService :: createQuoteService :: Input doc"+GCXMLUtil.getXMLString(inDoc));
		Document docGetOrderListOp = null;
		//append start date and quote expiration date
		appendExpirationStartAndEndDate(inDoc);
		// create input doc for quote request service
		Document docCreateQuoteReq = GCXMLUtil.newDocument();
		Node copiedRoot = docCreateQuoteReq.importNode(inDoc.getDocumentElement(), true);
		docCreateQuoteReq.appendChild(copiedRoot);
		Element eleOrderExtn1 = (Element) XPathAPI.selectSingleNode(docCreateQuoteReq, "//Order/Extn");
		String sCustomerName = eleOrderExtn1.getAttribute(GCConstants.CUSTOMER_NAME);
		String sfirstName ="";
		String sLastName ="";
		if(sCustomerName.contains(" ")){
			String []Name= sCustomerName.split(" ",2);
			sfirstName = Name[0];
			sLastName = Name[1];
		}else{
			sfirstName = sCustomerName;
		}
		eleOrderExtn1.setAttribute("FirstName", sfirstName);
		eleOrderExtn1.setAttribute("LastName", sLastName);

		
		Element eleRoot = docCreateQuoteReq.getDocumentElement();
		Document docATGCreateQuoteResponse = null;
		if(!YFCCommon.isVoid(eleRoot)){
			String sOrderHeaderKey = eleRoot.getAttribute(GCConstants.ORDER_HEADER_KEY);		
			docGetOrderListOp = GCCommonUtil.invokeGetOrderListAPITemp(env, sOrderHeaderKey, "/global/template/api/getOrderListTempForQuoteRequest.xml");
			
			appendMissingfieldAndOrderLine(docGetOrderListOp, docCreateQuoteReq);

			Element eleOrderExtn = (Element) XPathAPI.selectSingleNode(docGetOrderListOp, "//Order/Extn");
			String sExtnQuoteSent = GCConstants.NO;
			if(!YFCCommon.isVoid(eleOrderExtn)){
				sExtnQuoteSent=eleOrderExtn.getAttribute(GCConstants.EXTN_QUOTE_SENT);
			}
			try {
				if(GCConstants.YES.equalsIgnoreCase(sExtnQuoteSent)){
					
					Element eleAtgReqRoot = docCreateQuoteReq.getDocumentElement();
					eleAtgReqRoot.setAttribute(GCConstants.EXTN_SUPPORT_TICKET_NO, eleOrderExtn.getAttribute(GCConstants.EXTN_SUPPORT_TICKET_NO));
					eleAtgReqRoot.setAttribute(GCConstants.EXTN_SOURCE_ORDER_NO, eleOrderExtn.getAttribute(GCConstants.EXTN_SOURCE_ORDER_NO));
					docATGCreateQuoteResponse = callAtgUpdateQuoteService(env, docCreateQuoteReq);
				}
				else{
				docATGCreateQuoteResponse = callAtgQuoteService(env, docCreateQuoteReq);
				}
			} catch (IOException e) {
				LOGGER.error(" Inside catch block of GCExpireQuoteRequestService.createQuoteService().docATGCreateQuoteResponse, Exception is :", e);
				e.printStackTrace();
			}

		}
		String sIsSuccess = GCConstants.NO;
		String sQuoteId = "";
		String sSupportTicketNo = "";
		if(docATGCreateQuoteResponse!=null){			
			Node nIsSuccess = XPathAPI.selectSingleNode(docATGCreateQuoteResponse, "Envelope/Body/createQuoteResponse/isSuccess");			
			if(!YFCCommon.isVoid(nIsSuccess)&&GCConstants.TRUE_STRING.equalsIgnoreCase(nIsSuccess.getTextContent())){
				sIsSuccess = GCConstants.YES;
			}
			else{
				nIsSuccess = XPathAPI.selectSingleNode(docATGCreateQuoteResponse, "Envelope/Body/updateQuoteResponse/isSuccess");
				if(!YFCCommon.isVoid(nIsSuccess)&&GCConstants.TRUE_STRING.equalsIgnoreCase(nIsSuccess.getTextContent())){
					sIsSuccess = GCConstants.YES;
				}
			}
			Node nQuoteId = XPathAPI.selectSingleNode(docATGCreateQuoteResponse, "Envelope/Body/createQuoteResponse/quoteId");
			if(!YFCCommon.isVoid(nQuoteId)){
				sQuoteId = nQuoteId.getTextContent();
			}

			Node nSupportTicketId = XPathAPI.selectSingleNode(docATGCreateQuoteResponse, "Envelope/Body/createQuoteResponse/supportTicketId");
			if(!YFCCommon.isVoid(nSupportTicketId)){
				sSupportTicketNo = nSupportTicketId.getTextContent();
			}
		}

		modifyChangeOrderIpDoc(inDoc,sQuoteId,sSupportTicketNo,sIsSuccess, docGetOrderListOp);		
		Document docModifyFulfillmentOptionsOp = GCCommonUtil.invokeAPI(env, GCConstants.MODIFY_FULFILLMENT_OPTIONS, inDoc);
		Element eleModifyFulfillmentOptionsOp = docModifyFulfillmentOptionsOp.getDocumentElement();
		eleModifyFulfillmentOptionsOp.setAttribute(GCConstants.QUOTE_SENT_SUCCESSFULLY, sIsSuccess);
		LOGGER.debug("GCCreateQuoteRequestService :: createQuoteService :: sIsSuccess :: "+sIsSuccess);
		LOGGER.debug("GCCreateQuoteRequestService :: createQuoteService :: End");
		return docModifyFulfillmentOptionsOp;		 
	}

	/**
	 * 
	 * @param env
	 * @param docCreateQuoteReq
	 * @return
	 * @throws IOException
	 */
	private Document callAtgUpdateQuoteService(YFSEnvironment env,
			Document docCreateQuoteReq) throws IOException {
		String sATGQuoteURL = YFSSystem.getProperty(GCConstants.ATG_QUOTE_URL);
		if (YFCCommon.isVoid(sATGQuoteURL)) {
			throw new YFCException(
					"ATG quote URL is not configured in property file. Please retry after configuring it.");
		}
		LOGGER.debug("sGCURL :: " + sATGQuoteURL);
		String sATGQuoteSoapAction = YFSSystem
				.getProperty(GCConstants.ATG_UPDATE_QUOTE_SOAP_ACTION);
		if (YFCCommon.isVoid(sATGQuoteSoapAction)) {
			throw new YFCException(
					"ATG quote soap action is not configured in property file. Please retry after configuring it.");
		}
		Document docATGCreateQuoteReq = GCCommonUtil.invokeService(env, GCConstants.GC_UPDATE_QUOTE_REQUEST_XSL_SERVICE, docCreateQuoteReq);
		GCWebServiceUtil obj = new GCWebServiceUtil();
		Document docATGCreateQuoteResponse = obj.invokeSOAPWebService(docATGCreateQuoteReq, sATGQuoteURL, sATGQuoteSoapAction);
		
		LOGGER.debug("GCCreateQuoteRequestService :: callAtgQuoteService :: End");
		return docATGCreateQuoteResponse;
	}

	/**
	 * This method will append the missing order line/field in create quote request doc as all the line and field won't come in InDoc.
	 * 
	 * @param docGetOrderListOp
	 * @param docCreateQuoteReq
	 * @throws TransformerException
	 */
	private void appendMissingfieldAndOrderLine(Document docGetOrderListOp, Document docCreateQuoteReq) throws TransformerException {

		LOGGER.debug("GCCreateQuoteRequestService :: appendMissingfieldAndOrderLine :: Begin");
		//Add commission detail to quote request input
		Element eleGCOrderCommissionList = (Element) XPathAPI.selectSingleNode(docGetOrderListOp, "//GCOrderCommissionList");		
		Node copiedRoot = docCreateQuoteReq.importNode(eleGCOrderCommissionList, true);
		Element eleOrderExtn = (Element) XPathAPI.selectSingleNode(docCreateQuoteReq, "//Order/Extn");
		String sCustomerName = eleOrderExtn.getAttribute(GCConstants.CUSTOMER_NAME);
		String sfirstName ="";
		String sLastName ="";
		if(sCustomerName.contains(" ")){
			String []Name= sCustomerName.split(" ",2);
			sfirstName = Name[0];
			sLastName = Name[1];
		}else{
			sfirstName = sCustomerName;
		}
		eleOrderExtn.setAttribute("FirstName", sfirstName);
		eleOrderExtn.setAttribute("LastName", sLastName);
		eleOrderExtn.appendChild(copiedRoot);

		NodeList nlOrderLine = XPathAPI.selectNodeList(docCreateQuoteReq, "//OrderLine");

		lOrderLineKey = new ArrayList<String>();
		for(int i=0;i<nlOrderLine.getLength();i++){
			Element eleOrderLine = (Element)nlOrderLine.item(i);
			// String sPrimeLineNo = eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
			String sOrderLineKey =  eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
			NodeList nlBPL = eleOrderLine.getElementsByTagName("BundleParentLine");
		
            //Changes for quote 4480- Start
			Element eleExtn = (Element) eleOrderLine.getElementsByTagName(GCConstants.EXTN).item(0);
			String StringParentWarLineNo="";
			if(!YFCCommon.isVoid(eleExtn)){
			  StringParentWarLineNo = eleExtn.getAttribute(GCConstants.EXTN_WARRANTY_PARENT_LINE_NUMBER);
			}
			if(!YFCCommon.isVoid(StringParentWarLineNo)){
			for(int j=0;j<nlOrderLine.getLength();j++){
				Element eleOrderLine1 = (Element)nlOrderLine.item(j);
				String transLineId = eleOrderLine1.getAttribute(GCConstants.TRANSACTION_LINE_ID);
				if(transLineId.equalsIgnoreCase(StringParentWarLineNo)){
					Element eleItem = (Element) eleOrderLine1.getElementsByTagName(GCConstants.ITEM).item(0);
					String ItemId = eleItem.getAttribute(GCConstants.ITEM_ID);
					eleExtn.setAttribute(GCConstants.EXTN_WARRANTY_PARENT_ITEM_ID,ItemId );
					break;
				}
			}
			}
			//Changes for quote 4480- End
		
			if(StringUtils.isNotBlank(sOrderLineKey) && (nlBPL.getLength()==0)){
				lOrderLineKey.add(sOrderLineKey);								
			}
			Element eleLinePriceInfo = (Element) eleOrderLine.getElementsByTagName(GCConstants.LINE_PRICE_INFO).item(i);
			if(!YFCCommon.isVoid(eleLinePriceInfo)){
				
				String sUnitPrice = eleLinePriceInfo.getAttribute(GCConstants.UNIT_PRICE);
				if(StringUtils.isNotBlank(sUnitPrice)){
					eleLinePriceInfo.setAttribute(GCConstants.LIST_PRICE, sUnitPrice);
				}
			}
			validateLineExtnLevelField(eleOrderLine, sOrderLineKey, docGetOrderListOp);
			validateLineNotesLevelField(eleOrderLine, sOrderLineKey, docGetOrderListOp);	
		}
		Element eleOrderLines = (Element) XPathAPI.selectSingleNode(docCreateQuoteReq, "//OrderLines");	
		if(YFCCommon.isVoid(eleOrderLines)){
			eleOrderLines = docCreateQuoteReq.createElement(GCConstants.ORDER_LINES);
			docCreateQuoteReq.getDocumentElement().appendChild(eleOrderLines);
		}

		NodeList nlOrderLineGetOrderListOp = XPathAPI.selectNodeList(docGetOrderListOp, "//OrderLine");
		for(int i=0;i<nlOrderLineGetOrderListOp.getLength();i++){

			Element eleOrderLineGetOrderListOp =  (Element) nlOrderLineGetOrderListOp.item(i);
			String sOrderLineKeyGetOrderListOp = eleOrderLineGetOrderListOp.getAttribute(GCConstants.ORDER_LINE_KEY);

			//changes for quote 4480- Start
			String sDependentOrderLineKey = eleOrderLineGetOrderListOp.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
			Element eleExtn = (Element) eleOrderLineGetOrderListOp.getElementsByTagName(GCConstants.EXTN).item(0);
			if(!YFCCommon.isVoid(sDependentOrderLineKey)){
				for(int j=0;j<nlOrderLineGetOrderListOp.getLength();j++){
					Element eleOrderLine =  (Element) nlOrderLineGetOrderListOp.item(j);
					String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
					if((sDependentOrderLineKey).equalsIgnoreCase(sOrderLineKey)){
						Element eleItem =  (Element) eleOrderLine.getElementsByTagName(GCConstants.ITEM).item(0);
						String ParentItemId=eleItem.getAttribute(GCConstants.ITEM_ID);
						if(!YFCCommon.isVoid(eleExtn)){
						eleExtn.setAttribute(GCConstants.EXTN_WARRANTY_PARENT_ITEM_ID,ParentItemId);
						break;
						}
					}
				}
			}
			
			//changes for quote 4480- End
			if(!lOrderLineKey.contains(sOrderLineKeyGetOrderListOp)){
				NodeList nlGetOrderOpNote=eleOrderLineGetOrderListOp.getElementsByTagName(GCConstants.NOTE);
				
				for(int j=0;j<nlGetOrderOpNote.getLength();j++){
					Element eleNoteGetOrderListOp = (Element) nlGetOrderOpNote.item(j);
					String sNoteTextGetOrderListOp = eleNoteGetOrderListOp.getAttribute(GCConstants.NOTE_TEXT);
					if(sNoteTextGetOrderListOp.contains("PriceOverrideReasonCode:")){
						eleOrderLineGetOrderListOp.setAttribute(GCConstants.PRICE_OVERRIDE_REASON_CODE, eleNoteGetOrderListOp.getAttribute("ReasonCode"));
						break;
					}
				}				
				Element eleCopiedOrderLine = (Element) docCreateQuoteReq.importNode(eleOrderLineGetOrderListOp, true);
				NodeList nlBPL = eleCopiedOrderLine.getElementsByTagName("BundleParentLine");
				if(nlBPL.getLength() == 0){
					eleOrderLines.appendChild(eleCopiedOrderLine);
				}
			}
		}			
		LOGGER.debug("GCCreateQuoteRequestService :: appendMissingfieldAndOrderLine :: End");
	}

	/**
	 * This method will call ATG webservice with create quote url and soap action and will return the response
	 * 
	 * @param env
	 * @param docCreateQuoteReq
	 * @return
	 * @throws IOException
	 */
	private Document callAtgQuoteService(YFSEnvironment env,
			Document docCreateQuoteReq) throws IOException  {
		
		LOGGER.debug("GCCreateQuoteRequestService :: callAtgQuoteService :: Begin");
		
		String sATGQuoteURL = YFSSystem.getProperty(GCConstants.ATG_QUOTE_URL);
		if (YFCCommon.isVoid(sATGQuoteURL)) {
			throw new YFCException(
					"ATG quote URL is not configured in property file. Please retry after configuring it.");
		}
		LOGGER.debug("sGCURL :: " + sATGQuoteURL);
		String sATGQuoteSoapAction = YFSSystem
				.getProperty(GCConstants.ATG_QUOTE_SOAP_ACTION);
		if (YFCCommon.isVoid(sATGQuoteSoapAction)) {
			throw new YFCException(
					"ATG quote soap action is not configured in property file. Please retry after configuring it.");
		}
		Document docATGCreateQuoteReq = GCCommonUtil.invokeService(env, GCConstants.GC_CREATE_QUOTE_REQUEST_XSL_SERVICE, docCreateQuoteReq);
		GCWebServiceUtil obj = new GCWebServiceUtil();
		Document docATGCreateQuoteResponse = obj.invokeSOAPWebService(docATGCreateQuoteReq, sATGQuoteURL, sATGQuoteSoapAction);
		
		LOGGER.debug("GCCreateQuoteRequestService :: callAtgQuoteService :: End");
		return docATGCreateQuoteResponse;
	}

	/**
	 * This method will append the start and end expiration date in inDoc for change order input and atg request service
	 * 
	 * @param docCreateQuoteReq
	 * @throws TransformerException
	 */
	private void appendExpirationStartAndEndDate(Document inDoc) throws TransformerException {

		LOGGER.debug("GCCreateQuoteRequestService :: appendExpirationStartAndEndDate :: Begin");
		Element eleExtn = (Element) XPathAPI.selectSingleNode(inDoc, "//Order/Extn");
		String quoteExpirationPeriod = eleExtn.getAttribute(GCConstants.QUOTE_EXPIRATION_PERIOD);

		//DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		Calendar cal = Calendar.getInstance();

		eleExtn.setAttribute(GCConstants.EXTN_QUOTE_START_DATE, dateFormat.format(cal.getTime()));

		//calculate expiration end date
		int iQuoteExpirationPeriod = Integer.valueOf(quoteExpirationPeriod);
		cal.add(Calendar.DATE, iQuoteExpirationPeriod);
		eleExtn.setAttribute(GCConstants.EXTN_QUOTE_EXPIRATION_DATE, dateFormat.format(cal.getTime()));
		
		LOGGER.debug("GCCreateQuoteRequestService :: appendExpirationStartAndEndDate :: End");
	}

	/**
	 * This method will save quote related detail in order notes
	 * 
	 * @param inDoc
	 * @param sQuoteId
	 * @param sSupportTicketNo
	 * @param sIsSuccess
	 * @param docGetOrderListOp 
	 * @throws TransformerException
	 */


	private void modifyChangeOrderIpDoc(Document inDoc, String sQuoteId, String sSupportTicketNo, String sIsSuccess, Document docGetOrderListOp) throws TransformerException {

		LOGGER.debug("GCCreateQuoteRequestService :: modifyChangeOrderIpDoc :: Begin");
		Element eleRoot = inDoc.getDocumentElement();
		Element eleOrderExtn = (Element) XPathAPI.selectSingleNode(inDoc, "//Order/Extn");
		Element eleNotes = (Element) XPathAPI.selectSingleNode(inDoc, "//Order/Notes");
		
		eleOrderExtn.removeAttribute(GCConstants.EXTN_QUOTE_START_DATE);
		if(GCConstants.YES.equalsIgnoreCase(sIsSuccess)){
			
			if(YFCCommon.isVoid(eleNotes)){
				eleNotes = inDoc.createElement(GCConstants.NOTES);
				eleRoot.appendChild(eleNotes);
			}
			
			NodeList nlOrderLineExtn = XPathAPI.selectNodeList(inDoc, "//OrderLine/Extn");
			for(int i=0;i<nlOrderLineExtn.getLength();i++){
				Element eleExtn = (Element) nlOrderLineExtn.item(i);
				eleExtn.setAttribute(GCConstants.EXTN_QUOTE_LINE_SENT, GCConstants.YES);
			}
			
			//Added customer name to note
			String sCustomerName = eleOrderExtn.getAttribute(GCConstants.CUSTOMER_NAME);
			String sCustomerEmail = eleOrderExtn.getAttribute(GCConstants.CUSTOMER_EMAIL);
			String sQuoteExpirationDate ="";
			sQuoteExpirationDate = eleOrderExtn.getAttribute(GCConstants.EXTN_QUOTE_EXPIRATION_DATE);
			String[] dateComponents=sQuoteExpirationDate.split("-");
			//GCSTORE-5493 Changing date from YYYY-MM-DD to MM/DD/YYYY
			String strFinalDate=dateComponents[1]+"/"+dateComponents[2]+"/"+dateComponents[0];
			
			Element eleCustomerNameNote = inDoc.createElement(GCConstants.NOTE);
			eleNotes.appendChild(eleCustomerNameNote);
			eleCustomerNameNote.setAttribute(GCConstants.NOTE_TEXT, "Quote Sent to : "+sCustomerName + "( "+ sCustomerEmail+" )" + "  Quote Expiration Date : "+ strFinalDate);
			eleCustomerNameNote.setAttribute(GCConstants.REASON_CODE, "Customer Quote");
			//Added customer email to note
			
			if(StringUtils.isNotBlank(sQuoteId)){
				eleOrderExtn.setAttribute(GCConstants.EXTN_SOURCE_ORDER_NO, sQuoteId);
			}
			if(StringUtils.isNotBlank(sSupportTicketNo)){
				eleOrderExtn.setAttribute(GCConstants.EXTN_SUPPORT_TICKET_NO, sSupportTicketNo);
			}
			eleOrderExtn.setAttribute(GCConstants.EXTN_QUOTE_SENT, GCConstants.YES);
		}else{
			eleOrderExtn.removeAttribute(GCConstants.EXTN_QUOTE_EXPIRATION_DATE);
		}
		
		appendExtnQuoteLineSentAttribute(inDoc,docGetOrderListOp);
		LOGGER.debug("GCCreateQuoteRequestService :: modifyChangeOrderIpDoc :: End");
	}

	/**
	 * Set extn quote line sent attribute to Y for those line which didn't come in input document. 
	 * @param inDoc
	 * @param docGetOrderListOp
	 * @throws TransformerException 
	 */
	private void appendExtnQuoteLineSentAttribute(Document inDoc,
			Document docGetOrderListOp) throws TransformerException {

		NodeList nlOrderLineGetOrderListOp = XPathAPI.selectNodeList(docGetOrderListOp, "//OrderLine");
		Element eleOrderLines = (Element) inDoc.getDocumentElement().getElementsByTagName(GCConstants.ORDER_LINES).item(0);
		if(YFCCommon.isVoid(eleOrderLines)){
			eleOrderLines = inDoc.createElement(GCConstants.ORDER_LINES);
			inDoc.getDocumentElement().appendChild(eleOrderLines);
		}
		for(int i=0;i<nlOrderLineGetOrderListOp.getLength();i++){

			Element eleOrderLineGetOrderListOp =  (Element) nlOrderLineGetOrderListOp.item(i);
			String sOrderLineKeyGetOrderListOp = eleOrderLineGetOrderListOp.getAttribute(GCConstants.ORDER_LINE_KEY);

			Element eleExtnGetOrderListOp = (Element) eleOrderLineGetOrderListOp.getElementsByTagName(GCConstants.EXTN).item(0);
			String sExtnQuoteLineSent = eleExtnGetOrderListOp.getAttribute(GCConstants.EXTN_QUOTE_LINE_SENT);
			if(!GCConstants.YES.equalsIgnoreCase(sExtnQuoteLineSent)&&!lOrderLineKey.contains(sOrderLineKeyGetOrderListOp)){											

				Element eleOrderLine = inDoc.createElement(GCConstants.ORDER_LINE);
				eleOrderLines.appendChild(eleOrderLine);
				eleOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, sOrderLineKeyGetOrderListOp);				
				Element eleExtn = inDoc.createElement(GCConstants.EXTN);
				eleOrderLine.appendChild(eleExtn);
				eleExtn.setAttribute(GCConstants.EXTN_QUOTE_LINE_SENT, GCConstants.YES);
			}			
		}		
	}

	/**
	 * This method
	 * 
	 * @param eleOrderLine
	 * @param sOrderLineKey
	 * @param docGetOrderListOp
	 * @throws TransformerException 
	 */
	private void validateLineNotesLevelField(Element eleOrderLine,
			String sOrderLineKey, Document docGetOrderListOp) throws TransformerException {

		LOGGER.debug("GCCreateQuoteRequestService :: validateLineNotesLevelField :: Begin");
		
			NodeList nlLineNote = eleOrderLine.getElementsByTagName(GCConstants.NOTE);
			boolean bPriceOverrideNoteExist = false;

			for(int i=0;i<nlLineNote.getLength();i++){
				Element eleNote = (Element) nlLineNote.item(i);
				String sNoteText = eleNote.getAttribute(GCConstants.NOTE_TEXT);
				if(sNoteText.contains("Price overridden due to")){
					eleOrderLine.setAttribute(GCConstants.PRICE_OVERRIDE_REASON_CODE, eleNote.getAttribute("ReasonCode2"));
					bPriceOverrideNoteExist=true;
					break;
				}
			}
			//if price override notes is not present in quote request doc, it fetches from get order list op and append in the quote request doc
			if(!bPriceOverrideNoteExist){
				NodeList nlLineNoteGetOrderListOp = XPathAPI.selectNodeList(docGetOrderListOp, "//OrderLine[@OrderLineKey='"+sOrderLineKey+"']/Notes/Note");			
				for(int i=0;i<nlLineNoteGetOrderListOp.getLength();i++){
					Element eleNoteGetOrderListOp = (Element) nlLineNoteGetOrderListOp.item(i);
					String sNoteTextGetOrderListOp = eleNoteGetOrderListOp.getAttribute(GCConstants.NOTE_TEXT);
					if(sNoteTextGetOrderListOp.contains("PriceOverrideReasonCode:")){
						eleOrderLine.setAttribute(GCConstants.PRICE_OVERRIDE_REASON_CODE, eleNoteGetOrderListOp.getAttribute("ReasonCode"));
						break;
					}
				}
			}
				
		LOGGER.debug("GCCreateQuoteRequestService :: validateLineNotesLevelField :: End");
	}

	/**
	 * 
	 * @param eleOrderLine
	 * @param sOrderLineKey
	 * @param docGetOrderListOp
	 * @throws TransformerException
	 */
	private void validateLineExtnLevelField(Element eleOrderLine, String sOrderLineKey, Document docGetOrderListOp) throws TransformerException {

		LOGGER.debug("GCCreateQuoteRequestService :: validateLineExtnLevelField :: Begin");
		Element eleExtnGetOrderListOp = (Element) XPathAPI.selectSingleNode(docGetOrderListOp, "//OrderLine[@OrderLineKey='"+sOrderLineKey+"']/Extn");
		Element eleExtn = (Element) eleOrderLine.getElementsByTagName(GCConstants.EXTN).item(0);
		//Price override approver id
		if(!YFCCommon.isVoid(eleExtnGetOrderListOp)){
			String sExtnOverridePriceEmpId = eleExtn.getAttribute(GCConstants.EXTN_OVERRIDE_PRICE_EMP_ID);
			if(StringUtils.isEmpty(sExtnOverridePriceEmpId)){
				eleExtn.setAttribute(GCConstants.EXTN_OVERRIDE_PRICE_EMP_ID, eleExtnGetOrderListOp.getAttribute(GCConstants.EXTN_OVERRIDE_PRICE_EMP_ID));
			}
			// Quote min qty
			String sExtnQuoteMinQty = eleExtn.getAttribute(GCConstants.EXTN_QUOTE_MIN_QTY);
			// Few changes made below for GCSTORE - 6022	
			if(StringUtils.isEmpty(sExtnQuoteMinQty)){
				if(YFCCommon.isVoid(eleExtnGetOrderListOp.getAttribute(GCConstants.EXTN_QUOTE_MIN_QTY))){
					eleExtn.setAttribute(GCConstants.EXTN_QUOTE_MIN_QTY, "1");
				}else{
					eleExtn.setAttribute(GCConstants.EXTN_QUOTE_MIN_QTY, eleExtnGetOrderListOp.getAttribute(GCConstants.EXTN_QUOTE_MIN_QTY));
				}
			}
			//Quote max qty
			String sExtnQuoteMaxQty = eleExtn.getAttribute(GCConstants.EXTN_QUOTE_MAX_QTY);
			if(StringUtils.isEmpty(sExtnQuoteMaxQty)){
				if(YFCCommon.isVoid(eleExtnGetOrderListOp.getAttribute(GCConstants.EXTN_QUOTE_MAX_QTY))){
					eleExtn.setAttribute(GCConstants.EXTN_QUOTE_MAX_QTY, "1");
				}else{
					eleExtn.setAttribute(GCConstants.EXTN_QUOTE_MAX_QTY, eleExtnGetOrderListOp.getAttribute(GCConstants.EXTN_QUOTE_MAX_QTY));
				}
			}
		}
		LOGGER.debug("GCCreateQuoteRequestService :: validateLineExtnLevelField :: End");
	}
}
