/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *##################################################################################################################################################################################
 *	        OBJECTIVE: This class in invoked on the return create On_Success event
 *#####################################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#####################################################################################################################################################################################
             1.0               02/05/2014        Soni, Karan		Initial Version\
             1.1			   10/13/2014        Soni, Karan		OMS-3931 All ReturnRequest messages to DAX resulting in return with no source code
             1.2			   11/18/2014        Soni, Karan		OMS-4015 Updating the unit price in case of bundle component returned individually
             1.3               15/12/2014        Soni, Karan        OMS-4341 Proration Logic of Prices for SIKO component item does not consider promotion applied on the Order.
             1.4               16/12/2014        Soni, Karan        OMS-4362 Add Payment Method on Return Not Working in case of Adv return
             1.5               26/02/2015        Soni, Karan        OMS-4934 COA Refund is being refunded to OPM for credit memos on advance credit returns
             1.6	           03/03/2015	     Soni, Karan        OMS-4893: Component return uses wrong price and throws an error - PMR 68065,227,000
             1.7               22/04/2015        Singh, Gurpreet    GCSTORE-1733: GC_QA2: Refund amount for a return order does not includes the Line taxes.
 *#####################################################################################################################################################################################
 */
package com.gc.api;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author karan
 */
public class GCCreateReturnOnSuccess {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCCreateReturnOnSuccess.class);

  public Document createCreditMemo(YFSEnvironment env, Document inDoc)
      throws GCException {
    try {
      LOGGER.verbose("Entering the createCreditMemo method");
      Element eleOrder = inDoc.getDocumentElement();
      String strSellerOrg = eleOrder
          .getAttribute(GCConstants.SELLER_ORGANIZATION_CODE);
      String strSOOrderHdrKey =GCXMLUtil.getAttributeFromXPath(
          eleOrder,
          "OrderLines/OrderLine/@DerivedFromOrderHeaderKey");
      String strOrderType = eleOrder.getAttribute(GCConstants.ORDER_TYPE);
      LOGGER.debug("The Order Type is" + strOrderType);
      String strIsRefundOnAccount;
      //OMS-4362 Start
      String strRefundPaymentKey;
      //OMS-4362 End
      if (GCConstants.RETURN_WITH_ADV_CREDIT.equals(strOrderType)) {
        LOGGER.debug("Entering the Advance credit return if block");
        //OMS-4934 Start
        strIsRefundOnAccount = GCXMLUtil.getAttributeFromXPath(
            eleOrder, "Extn/@ExtnIsRefundOnAccount");
        //OMS-4934 End
        strIsRefundOnAccount = (YFCCommon.isVoid(strIsRefundOnAccount) ? ""
            : strIsRefundOnAccount);
        //OMS-4362 Start
        strRefundPaymentKey = GCXMLUtil.getAttributeFromXPath(
            eleOrder, "Extn/@ExtnRefundPaymentKey");
        strRefundPaymentKey = (YFCCommon.isVoid(strRefundPaymentKey) ? ""
            : strRefundPaymentKey);
        //OMS-4362 End
        String strCurrency = GCXMLUtil.getAttributeFromXPath(eleOrder,
            "PriceInfo/@Currency");
        String strTotalAmt = GCXMLUtil.getAttributeFromXPath(eleOrder,
            "OverallTotals/@GrandTotal");
        LOGGER.debug("The strOrderTotal is" + strTotalAmt);
        String strOrderDate = eleOrder
            .getAttribute(GCConstants.ORDER_DATE);
        String strEnterpriseCode = eleOrder
            .getAttribute(GCConstants.ENTERPRISE_CODE);

        String strOrderNo = eleOrder.getAttribute(GCConstants.ORDER_NO);
        //OMS-4362 Start
        //OMS-4934 Start
        String strCreditMemo = "<OrderInvoice Currency='" + strCurrency
            + "' DateInvoiced='" + strOrderDate + "' OrderNo='"
            + strOrderNo + "' DocumentType='0001'"
            + " EnterpriseCode='" + strEnterpriseCode
            + "' InvoiceType='" + GCConstants.CREDIT_MEMO
            + "' OrderHeaderKey='" + strSOOrderHdrKey
            + "' SellerOrganizationCode='" + strSellerOrg
            + "'><Extn ExtnIsRefundOnAccount='"
            + strIsRefundOnAccount
            + "' ExtnRefundPaymentKey='"+strRefundPaymentKey+"' /><HeaderChargeList/></OrderInvoice>";
        //OMS-4934 End
        //OMS-4362 End
        Document inDocCreditMemo = GCXMLUtil.getDocument(strCreditMemo);
        Element eleHeaderChargeList = GCXMLUtil.getElementByXPath(
            inDocCreditMemo, "OrderInvoice/HeaderChargeList");
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("The recordInvoiceCreation indoc after appending header is"
              + GCXMLUtil.getXMLString(inDocCreditMemo));
        }

        Element eleHeaderCharge = inDocCreditMemo
            .createElement(GCConstants.HEADER_CHARGE);
        eleHeaderChargeList.appendChild(eleHeaderCharge);
        eleHeaderCharge.setAttribute(GCConstants.CHARGE_CATEGORY,
            GCConstants.ADVANCE_CREDIT);
        eleHeaderCharge.setAttribute(GCConstants.CHARGE_NAME,
            GCConstants.ADVANCE_CREDIT);
        eleHeaderCharge.setAttribute(GCConstants.CHARGE_AMOUNT,
            strTotalAmt);
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("The inDocCreditMemo document is"
              + GCXMLUtil.getXMLString(inDocCreditMemo));
        }
        // Invoking recordInvoiceCreation to make credit memo in case of
        // advance credit return
        GCCommonUtil.invokeAPI(env,
            GCConstants.RECORD_INVOICE_CREATION, inDocCreditMemo);
        LOGGER.debug("Exiting the Advance credit return if block");
      }
      String strROOrderHdrKey= eleOrder
          .getAttribute(GCConstants.ORDER_HEADER_KEY);
      if (GCConstants.RETURN_WITH_ADV_CREDIT.equals(strOrderType)
          || GCConstants.RETURN_WITH_ADV_EXCHANGE
          .equals(strOrderType)) {
        String strReturnWindowDays = getReturnWindowDaysOfSeller(env,
            strSellerOrg);
        String strOrderLineDate = GCXMLUtil.getAttributeFromXPath(
            eleOrder, "@OrderDate");
        Date orderLineDate = GCDateUtils.convertDate(strOrderLineDate);
        Date returnWindowDate = GCDateUtils.addToDate(orderLineDate,
            Calendar.DAY_OF_MONTH,
            Integer.valueOf(strReturnWindowDays));
        String returnWindowDateStr = GCDateUtils.formatDate(
            returnWindowDate, GCConstants.TS_FORMAT_NO_TIMEZONE);
        Document changeOrderInDoc = GCXMLUtil
            .getDocument("<Order Action='Modify' OrderHeaderKey='"
                + strROOrderHdrKey
                + "' Override='Y'><Notes><Note/></Notes><OrderDates><OrderDate/></OrderDates></Order>");
        Element eleOrderDate = GCXMLUtil.getElementByXPath(
            changeOrderInDoc, "Order/OrderDates/OrderDate");
        Element eleNote = GCXMLUtil.getElementByXPath(changeOrderInDoc,
            "Order/Notes/Note");
        eleNote.setAttribute(GCConstants.NOTE_TEXT,
            "Credit Memo issued for Advance Credit Return");
        eleOrderDate.setAttribute("ExpectedDate", returnWindowDateStr);
        eleOrderDate.setAttribute("DateTypeId", "RETURN_WINDOW_DATE");
        GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER,
            changeOrderInDoc);
      }
      LOGGER.verbose("Exiting the createCreditMemo method");
      return inDoc;
    } catch (Exception e) {
      LOGGER.error("Inside GCCreateReturnOnSuccess.createCreditMemo():",e);
      throw new GCException(e);
    }
  }

  /**
   * This method is created to fetch the return window days for the seller
   * organization
   *
   * @param env
   * @param strSellerOrganizationCode
   * @return
   * @throws Exception
   */
  private static String getReturnWindowDaysOfSeller(YFSEnvironment env,
      String strSellerOrganizationCode) throws GCException {
    try{
      Document outXML = GCCommonUtil.getCommonCodeListByType(env,
          GCConstants.ZCR_RTN_DAYS, strSellerOrganizationCode);
      String returnWindow = GCXMLUtil.getElementByXPath(outXML,
          "//CommonCode").getAttribute(GCConstants.CODE_VALUE);
      return returnWindow;
    }catch(Exception e){
      LOGGER.error("The exception is in getReturnWindowDaysOfSeller method:", e);
      throw new GCException(e);
    }
  }

  //OMS-4015 Start
  /**
   * This method stamps the updated unit price LineCharges and LineTaxes in case of component returned individually
   * @param env
   * @param inDocument
   * @return
   * @throws GCException
   */
  public static Document updateBundleUnitPrice(YFSEnvironment env, Document inDocument, Document inDocCO) throws GCException{ try{
    LOGGER.info("Entering the updateBundleUnitPrice method");
    String strSOOHK= GCXMLUtil.getAttributeFromXPath(inDocument, "OrderList/Order/OrderLines/OrderLine/@DerivedFromOrderHeaderKey");
    String strSOGOD= "<Order OrderHeaderKey='"+strSOOHK+"'/>";
    Document inDocSOGOD= GCXMLUtil.getDocument(strSOGOD);
    Document inDocTempGOD= GCCommonUtil.getDocumentFromPath(GCConstants.GC_GET_ORDER_LIST_TEMPLATE);
    Document outDocGOD= GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, inDocSOGOD, inDocTempGOD);
    outDocGOD = GCXMLUtil.getDocumentFromElement((Element) outDocGOD.getDocumentElement().getFirstChild());
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("The output xml for GetOrderList SO is: "+ GCXMLUtil.getXMLString(outDocGOD));
    }

    NodeList nlOrderLine= XPathAPI.selectNodeList(inDocument, "OrderList/Order/OrderLines/OrderLine");
    Set<String> setOrderLineKey=new HashSet<String>();
    Document docPoratedTaxesCharges= null;
    for(int i= 0; i< nlOrderLine.getLength(); i++){
      Element eleOrderLine= (Element)nlOrderLine.item(i);
      String strKitCode= eleOrderLine.getAttribute(GCConstants.KIT_CODE);
      String strOLK= eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);

      if(GCConstants.BUNDLE.equals(strKitCode)){
        setOrderLineKey.add(strOLK);
      }
      if(!setOrderLineKey.contains(strOLK)){
        Element eleBundleParent = (Element)XPathAPI.selectSingleNode(eleOrderLine, "BundleParentLine");
        if(!YFCCommon.isVoid(eleBundleParent)){
          String strBundleOLK= eleBundleParent.getAttribute(GCConstants.ORDER_LINE_KEY);
          Element eleBundleExist= GCXMLUtil.getElementByXPath(inDocument, "OrderList/Order/OrderLines/OrderLine[@OrderLineKey='"+strBundleOLK+"']");
          if(!YFCCommon.isVoid(eleBundleExist)){
            setOrderLineKey.add(strBundleOLK);
            // GCSTORE-1733 : Start
            Element eleOL =
                GCXMLUtil.getElementByXPath(inDocCO, "Order/OrderLines/OrderLine[@OrderLineKey='" + strOLK + "']");
            if(!YFCCommon.isVoid(eleOL)){
            Element eleLinePriceinfo = (Element) XPathAPI.selectSingleNode(eleOL, "LinePriceInfo");
            eleLinePriceinfo.setAttribute("IsLinePriceForInformationOnly", GCConstants.YES);
            // GCSTORE-1733 : End
            continue;
            }
          }
        } else {
          if(YFCCommon.isVoid(docPoratedTaxesCharges)){
            docPoratedTaxesCharges= GCCommonUtil.invokeService(env, "GCBundleCompProrateCharges", outDocGOD);
          }
          String strItemID= GCXMLUtil.getAttributeFromXPath(eleOrderLine, "Item/@ItemID");
          String strQuantityReturned= eleOrderLine.getAttribute(GCConstants.ORDERED_QTY);
          Element eleSOOL= GCXMLUtil.getElementByXPath(outDocGOD, "Order/OrderLines/OrderLine/Item[@ItemID='"+strItemID+"']/..");
          String strSOQty= eleSOOL.getAttribute(GCConstants.ORDERED_QTY);
          Element eleIsBundle= (Element)XPathAPI.selectSingleNode(eleSOOL, "BundleParentLine");
          if(!YFCCommon.isVoid(eleIsBundle)){
            String strUnitPrice= GCXMLUtil.getAttributeFromXPath(docPoratedTaxesCharges, "Order/OrderLines/OrderLine/Item[@ItemID='"+strItemID+"']/../LinePriceInfo/@UnitPrice");
            //OMS-4341 Start
            Double dChargeAmount = 0.00;
            String sChargeAmount = "0.00";
            NodeList nLLinePrice = GCXMLUtil
                .getNodeListByXpath(docPoratedTaxesCharges,"Order/OrderLines/OrderLine/Item[@ItemID='"+strItemID+"']/../LineCharges/LineCharge[@ChargeCategory='Promotion']");
            for (int iCounter = 0; iCounter < nLLinePrice.getLength(); iCounter++) {
              Element eleLinePrice = (Element) nLLinePrice.item(iCounter);
              sChargeAmount = eleLinePrice
                  .getAttribute(GCConstants.CHARGE_PER_LINE);
			  if (!YFCCommon.isVoid(sChargeAmount)){
            	  dChargeAmount = dChargeAmount
            			  + Double.parseDouble(sChargeAmount);
              }
            }
            if (dChargeAmount > 0.00) {
              dChargeAmount= dChargeAmount/Double.parseDouble(strSOQty);
              strUnitPrice= String.valueOf(Double.parseDouble(strUnitPrice)- dChargeAmount);
              BigDecimal a = new BigDecimal(strUnitPrice);
              BigDecimal bROUnitPrice = a.setScale(2,
                  BigDecimal.ROUND_HALF_EVEN);
              strUnitPrice = bROUnitPrice.toString();
            }
            //OMS-4341 End
            Element eleLineTaxesProrated= GCXMLUtil.getElementByXPath(docPoratedTaxesCharges, "Order/OrderLines/OrderLine/Item[@ItemID='"+strItemID+"']/../LineTaxes");
            if (LOGGER.isDebugEnabled()) {
              LOGGER.debug("The prorated line Taxes are "+ GCXMLUtil.getElementString(eleLineTaxesProrated));
            }
            //OMS-4893 Start
            Element eleOL= GCXMLUtil.getElementByXPath(inDocCO, "Order/OrderLines/OrderLine[@OrderLineKey='"+strOLK+"']");
            if(!YFCCommon.isVoid(eleOL)){
            Element eleLinePriceinfo= (Element)XPathAPI.selectSingleNode(eleOL, "LinePriceInfo");
            eleLinePriceinfo.setAttribute(GCConstants.UNIT_PRICE, strUnitPrice);
            eleLinePriceinfo.setAttribute("IsLinePriceForInformationOnly", GCConstants.NO);
            Element eleLTRemove= (Element)XPathAPI.selectSingleNode(eleOL, "LineTaxes");
            eleOL.removeChild(eleLTRemove);
            Element eleLineTaxes= inDocCO.createElement(GCConstants.LINE_TAXES);
            eleOL.appendChild(eleLineTaxes);
            //OMS-4893 End
            NodeList nlShippingTax= XPathAPI.selectNodeList(eleLineTaxesProrated, "LineTax[@ChargeCategory='Shipping']");
            for(int j=0; j< nlShippingTax.getLength(); j++){
              Element eleShipping= (Element)nlShippingTax.item(j);
              eleLineTaxesProrated.removeChild(eleShipping);
            }
            if(strSOQty.equals(strQuantityReturned)){
              GCXMLUtil.copyElement(inDocCO, eleLineTaxesProrated, eleLineTaxes);
            } else{
              NodeList nlLineTax= GCXMLUtil.getNodeListByXpath(docPoratedTaxesCharges, "Order/OrderLines/OrderLine/Item[@ItemID='"+strItemID+"']/../LineTaxes/LineTax");
              for(int k=0; k<nlLineTax.getLength() ;k++){
                Element eleLineTax= (Element)nlLineTax.item(k);
                String strTax= eleLineTax.getAttribute(GCConstants.TAX);
                  double dProratedTax =
                      Double.valueOf(strTax) * Double.valueOf(strQuantityReturned) / Double.valueOf(strSOQty);
                eleLineTax.setAttribute(GCConstants.TAX, String.valueOf(dProratedTax));
              }
              GCXMLUtil.copyElement(inDocCO, eleLineTaxesProrated, eleLineTaxes);
            }
            
            //GCSTORE-5934-starts
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("GCSTORE-5934-starts");
            }
            NodeList nlLineTaxFromCO=eleLTRemove.getElementsByTagName("LineTax");
            
            for(int j=0;j<nlLineTaxFromCO.getLength();j++)
            {
               Element eleLineTax=(Element) nlLineTaxFromCO.item(j);

               if (LOGGER.isDebugEnabled()) {
                   LOGGER.debug("Inside for loop :: eleLineTax :: "+GCXMLUtil.getElementString(eleLineTax));
                 }
               if(GCConstants.CUSTOMER_APPEASEMENT.equals(eleLineTax.getAttribute("ChargeCategory")) && GCConstants.CUSTOMER_APPEASEMENT.equals(eleLineTax.getAttribute("ChargeName")))
               {
                   eleLineTaxes.appendChild(eleLineTax);
                   break;
                   
               }
              
            }
            LOGGER.debug("GCSTORE-5934-ends"); 
            //GCSTORE-5934-ends
            
          }
          }
        }
      }
    }
    LOGGER.info("Exiting the updateBundleUnitPrice method :: Returned Document :: \n"+inDocCO);
    //OMS-4893 Start
    return inDocCO;
    //OMS-4893 End
  }catch(Exception e){
    LOGGER.error("The exception is in updateBundleUnitPrice method:", e);
    throw new GCException(e);
  }
  }
  //OMS-4015 End
}
