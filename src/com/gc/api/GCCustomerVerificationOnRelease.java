package com.gc.api;

import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCCustomerVerificationOnRelease implements YIFCustomApi{
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCCustomerVerificationOnRelease.class);

  public Document customerVerificationOnRelease(YFSEnvironment env,Document inDoc) throws GCException{
    LOGGER.debug("Entering inside GCCustomerVerificationOnRelease :: customerVerificationOnRelease ::");

    try {
      String sOrderHeaderKey=GCXMLUtil.getAttributeFromXPath(inDoc,
          "Order/@OrderHeaderKey");

      Document getOrderListDoc=GCCommonUtil.invokeAPI(env,GCConstants.GET_ORDER_LIST,
          GCXMLUtil.getDocument("<Order OrderHeaderKey='"
              + sOrderHeaderKey + "'/> "),
              GCXMLUtil.getDocument("<OrderList><Order BillToID='' OrderHeaderKey='' OrderNo='' EnterpriseCode=''>"
                  + "<Extn ExtnDAXCustomerID=''/></Order></OrderList>"));
      String sBillToID = GCXMLUtil.getAttributeFromXPath(getOrderListDoc,
          "OrderList/Order/@BillToID");

      if(sBillToID!=null){
        String sExtnDAXCustomerID = GCXMLUtil.getAttributeFromXPath(getOrderListDoc,
            "OrderList/Order/Extn/@ExtnDAXCustomerID");
        if (!YFCCommon.isVoid(sExtnDAXCustomerID)) {
        	Document docGetCustomerListIp = GCXMLUtil
        			.getDocument("<Customer> <Extn ExtnDAXCustomerID=\""
        					+ sExtnDAXCustomerID + "\"/> </Customer>");
        	Document docGetCustomerListOp = GCCommonUtil
        			.invokeAPI(env, docGetCustomerListIp,
        					GCConstants.GET_CUSTOMER_LIST,
        					"global/template/api/getCustomerListForInactiveAccountHold");
        	String sCustomerID = GCXMLUtil.getAttributeFromXPath(
        			docGetCustomerListOp,
        			"CustomerList/Customer/@CustomerID");
        	if(sCustomerID==null){
        		Document docOrderAmendmentParameter=GCXMLUtil.createDocument("OrderAmendmentParameter");
        		Element eleOrderAmendmentParameter=docOrderAmendmentParameter.getDocumentElement();
        		eleOrderAmendmentParameter.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
        		eleOrderAmendmentParameter.setAttribute(GCConstants.OPERATION_IDENTIFIER, GCConstants.CUSTOMER_DOES_NOT_EXIST);
        		GCCommonUtil.invokeService(env,GCConstants.GC_ORDR_AMNDMNT_ON_CSTMR_VRFCSN,docOrderAmendmentParameter);  
        		throw new GCException("Order Release Customer Verification- OMSCustomer does not exist");
        	}else{
        		return inDoc;
        	}
        }else{
          Document docOrderAmendmentParameter=GCXMLUtil.createDocument("OrderAmendmentParameter");
          Element eleOrderAmendmentParameter=docOrderAmendmentParameter.getDocumentElement();
          eleOrderAmendmentParameter.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
          eleOrderAmendmentParameter.setAttribute(GCConstants.OPERATION_IDENTIFIER, GCConstants.CUSTOMER_DOES_NOT_EXIST);
          GCCommonUtil.invokeService(env,GCConstants.GC_ORDR_AMNDMNT_ON_CSTMR_VRFCSN,docOrderAmendmentParameter);
          throw new GCException("Order Release Customer Verification- OMSCustomer does not exist");
        }
      }else{
        String sExtnDAXCustomerID = GCXMLUtil.getAttributeFromXPath(getOrderListDoc,
            "OrderList/Order/Extn/@ExtnDAXCustomerID");

        if (!YFCCommon.isVoid(sExtnDAXCustomerID)) {
          Document docGetCustomerListIp = GCXMLUtil
              .getDocument("<Customer> <Extn ExtnDAXCustomerID=\""
                  + sExtnDAXCustomerID + "\"/> </Customer>");
          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("GCCheckOrderBeforeProcessingUE ::  checkOrderBeforeProcessing :: docGetCustomerListIp"
                + GCXMLUtil.getXMLString(docGetCustomerListIp));
          }
          Document docGetCustomerListOp = GCCommonUtil
              .invokeAPI(env, docGetCustomerListIp,
                  GCConstants.GET_CUSTOMER_LIST,
                  "global/template/api/getCustomerListForInactiveAccountHold");
          String sCustomerID = GCXMLUtil.getAttributeFromXPath(
              docGetCustomerListOp,
              "CustomerList/Customer/@CustomerID");
          String sCustomerStatus = GCXMLUtil.getAttributeFromXPath(
              docGetCustomerListOp,
              "CustomerList/Customer/@Status");

          Document docOrderAmendmentParameter=GCXMLUtil.createDocument("OrderAmendmentParameter");
          Element eleOrderAmendmentParameter=docOrderAmendmentParameter.getDocumentElement();

          if(sCustomerID==null){
            eleOrderAmendmentParameter.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
            eleOrderAmendmentParameter.setAttribute(GCConstants.OPERATION_IDENTIFIER, GCConstants.CUSTOMER_DOES_NOT_EXIST);
            GCCommonUtil.invokeService(env,GCConstants.GC_ORDR_AMNDMNT_ON_CSTMR_VRFCSN,docOrderAmendmentParameter);
            throw new GCException("Order Release Customer Verification- OMSCustomer does not exist");
          }else if(!sCustomerID.isEmpty() && sCustomerStatus.equalsIgnoreCase(GCConstants.CUSTOMER_ACTIVE_STATUS)){
            eleOrderAmendmentParameter.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
            eleOrderAmendmentParameter.setAttribute(GCConstants.OPERATION_IDENTIFIER, GCConstants.ACTIVE_CUSTOMER);
            eleOrderAmendmentParameter.setAttribute(GCConstants.CUSTOMER_ID, sCustomerID);
            GCCommonUtil.invokeService(env,GCConstants.GC_ORDR_AMNDMNT_ON_CSTMR_VRFCSN,docOrderAmendmentParameter);
            throw new GCException("Order Release Customer Verification- BillToID Does not Exist on the Order,Active Customer");
          }else if(!sCustomerID.isEmpty() && !sCustomerStatus.equalsIgnoreCase(GCConstants.CUSTOMER_ACTIVE_STATUS)){
            String sEnterpriseCode = GCXMLUtil.getAttributeFromXPath(getOrderListDoc,
                "OrderList/Order/@EnterpriseCode");
            eleOrderAmendmentParameter.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
            eleOrderAmendmentParameter.setAttribute(GCConstants.OPERATION_IDENTIFIER, GCConstants.INACTIVE_CUSTOMER);
            eleOrderAmendmentParameter.setAttribute(GCConstants.ENTERPRISE_CODE, sEnterpriseCode);
            eleOrderAmendmentParameter.setAttribute(GCConstants.CUSTOMER_ID, sCustomerID);
            GCCommonUtil.invokeService(env,GCConstants.GC_ORDR_AMNDMNT_ON_CSTMR_VRFCSN,docOrderAmendmentParameter);
            throw new GCException("Order Release Customer Verification- Inactive Customer");
          }
        }else{
            Document docOrderAmendmentParameter=GCXMLUtil.createDocument("OrderAmendmentParameter");
            Element eleOrderAmendmentParameter=docOrderAmendmentParameter.getDocumentElement();
            eleOrderAmendmentParameter.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
            eleOrderAmendmentParameter.setAttribute(GCConstants.OPERATION_IDENTIFIER, GCConstants.CUSTOMER_DOES_NOT_EXIST);
            GCCommonUtil.invokeService(env,GCConstants.GC_ORDR_AMNDMNT_ON_CSTMR_VRFCSN,docOrderAmendmentParameter);
            throw new GCException("Order Release Customer Verification- OMSCustomer does not exist");
        }
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCCustomerVerificationOnRelease.customerVerificationOnRelease(), Exception is :",
          e);

      throw new GCException(e);
    }
    return inDoc;
  }
  @Override
  public void setProperties(Properties arg0) {
  }

}
