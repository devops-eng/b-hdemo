/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 * *###########################################
 * ######################################################
 * ################################################################ OBJECTIVE: Write what this class
 * is about
 * #########################################################################################
 * ######################################################################## Version Date Modified By
 * Description
 * ######################################################################################
 * ########################################################################### 1.0 22/04/2014
 * Mittal, Yashu JIRA No: Description of issue.
 * #####################################################
 * #############################################
 * ###############################################################
 */
package com.gc.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCPaymentSecurity;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:yashu.mittal@expicient.com">Yashu Mittal</a>
 */

public class GCDAXSyncMessageAPI implements YIFCustomApi {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCDAXSyncMessageAPI.class.getName());

  /**
   * This method get the order details and do pre-processing like encrypt/decrypt payment methods
   * before syncing it with DAX.
   *
   * @param env
   * @param inDoc
   * @throws GCException
   * @see com.yantra.interop.japi.YIFCustomApi#setProperties(java.util.Properties)
   */

  public Document getOrderListAndProcess(YFSEnvironment env, Document inDoc) throws GCException {

    try {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(" Entering GCGetOrderListAndPostProcessing.getOrderListAndProcess() method ");
        LOGGER.debug(" getOrderListAndProcess() method, Input document is" + GCXMLUtil.getXMLString(inDoc));
      }

      Element eleRootInDoc = inDoc.getDocumentElement();
      String sOrderHeaderKey = "";

      // List of OrderLineKey of cancelled OrderLines from ScheduledPODAX or Backordered status.
      List<String> cancelledOdrLineKeyList = new ArrayList<String>();

      String sInDocRootElementName = eleRootInDoc.getNodeName();

      if (GCConstants.ORDER.equalsIgnoreCase(sInDocRootElementName)) {
        sOrderHeaderKey = eleRootInDoc.getAttribute(GCConstants.ORDER_HEADER_KEY);

        // check for orderlines which are cancelled from ScheduledPODAX or Backordered status
        listCancelledOdrLine(inDoc, cancelledOdrLineKeyList);

      } else if (GCConstants.RECEIPT.equalsIgnoreCase(sInDocRootElementName)) {
        Element eleReceiptLine = (Element) eleRootInDoc.getElementsByTagName("ReceiptLine").item(0);
        sOrderHeaderKey = eleReceiptLine.getAttribute(GCConstants.ORDER_HEADER_KEY);
      }

      // Invoking getOrderList with OrderHeaderKey and template passed
      Document getOrderListOutDoc =
          GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST,
              GCXMLUtil.getDocument("<Order OrderHeaderKey='" + sOrderHeaderKey + "'/>"),
              GCCommonUtil.getDocumentFromPath("/global/template/api/OMS_DAX_OrderSync_TemplateXML.xml"));
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(GCXMLUtil.getXMLString(getOrderListOutDoc));
      }
      //GCSTORE-6696--Start
      HashMap<String, YFCElement> inMap = new HashMap<String, YFCElement>();
      addInDocOrderLinesToMap(inDoc,inMap);
    //GCSTORE-6696--End
      // to remove the lines from document which are not in scheduledPODAX or Backordered status.
      modifyOutputDoc(env,getOrderListOutDoc, cancelledOdrLineKeyList,inMap);

      List<Element> listPaymentMethod =
          GCXMLUtil.getElementListByXpath(getOrderListOutDoc, GCConstants.ORDERLIST_ORDER_PAYMENT_METHPD_XPATH);

      // Encrypt/Decrypt CC, PLCC or GC payment method present in order.
      for (Element elePaymentMethod : listPaymentMethod) {

        String sPaymentType = elePaymentMethod.getAttribute(GCConstants.PAYMENT_TYPE);

        if (sPaymentType.equalsIgnoreCase(GCConstants.CREDIT_CARD) || sPaymentType.equalsIgnoreCase(GCConstants.PLCC)
            || sPaymentType.equalsIgnoreCase(GCConstants.GIFT_CARD)) {

          // Decrypting/Encrypting Svc no in case of any payment
          // method.
          String sSvcNo = elePaymentMethod.getAttribute(GCConstants.SVC_NO);

          if (!YFCCommon.isVoid(sSvcNo)) {

            String sDecryptedSvcNo = GCPaymentSecurity.decryptCreditCard(sSvcNo, "OMS");

            String sEncryptedSvcNo = GCPaymentSecurity.encryptCreditCard(sDecryptedSvcNo, "DAX");

            elePaymentMethod.setAttribute(GCConstants.SVC_NO, sEncryptedSvcNo);
          }

          if (sPaymentType.equalsIgnoreCase(GCConstants.CREDIT_CARD) || sPaymentType.equalsIgnoreCase(GCConstants.PLCC)) {
            // Decrypting/Encrypting CC No. no in case of CC or PLCC
            // Payment Method

            String sCreditCardNo = elePaymentMethod.getAttribute(GCConstants.CREDIT_CARD_NO);

            if (!YFCCommon.isVoid(sCreditCardNo)) {

              String sDecryptedCCNo = GCPaymentSecurity.decryptCreditCard(sCreditCardNo, "OMS");

              String sEncryptedCCNo = GCPaymentSecurity.encryptCreditCard(sDecryptedCCNo, "DAX");

              elePaymentMethod.setAttribute(GCConstants.CREDIT_CARD_NO, sEncryptedCCNo);

            }
          }
        }
      }

      // Change For 5193 - Start
      stampStatusAndQuantityOnSetBundleLine(getOrderListOutDoc);
      // Change for 5193 - End
      stampCancellationReasonText(env, getOrderListOutDoc);
      return getOrderListOutDoc;
    } catch (Exception e) {
      LOGGER.error("Inside catch block of GCGetOrderListAndPostProcessing.getOrderListAndProcess method. Error is : ",
          e);
      throw new GCException(e);
    }

  }
//GCSTORE-6696--Start
  private void addInDocOrderLinesToMap(Document inDoc,HashMap<String, YFCElement> inMap) {
	  YFCDocument getOrderDoc = YFCDocument.getDocumentFor(inDoc);
	    YFCElement eleOdrOutput = getOrderDoc.getDocumentElement();
	    YFCElement eleOdrLines = eleOdrOutput.getChildElement(GCConstants.ORDER_LINES);
	    YFCNodeList<YFCElement> nlOdrLine = eleOdrLines.getElementsByTagName(GCConstants.ORDER_LINE);

	    for (int i = 0; i < nlOdrLine.getLength(); i++) {
	    	YFCElement eleOdrLine = nlOdrLine.item(i);
	    	String orderLineKey = eleOdrLine.getAttribute(GCConstants.ORDER_LINE_KEY);
	    	inMap.put(orderLineKey, eleOdrLine);
	    }
	
	
}
//GCSTORE-6696--End

  /**
   * This method is used to list the orderlines which are cancelled from ScheduledPODAX or
   * Backordered status.
   *
   * @param inDoc
   * @param cancelledOdrLineKeyList
   */
  private void listCancelledOdrLine(Document inDoc, List<String> cancelledOdrLineKeyList) {

    LOGGER.beginTimer("GCDAXSyncMessageAPI.listCancelledOdrLine");
    LOGGER.verbose("GCDAXSyncMessageAPI.listCancelledOdrLine -- Begin");
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement rootEle = inputDoc.getDocumentElement();
    YFCElement eleOrderLines = rootEle.getChildElement(GCConstants.ORDER_LINES);
    if (!YFCCommon.isVoid(eleOrderLines)) {
      YFCNodeList<YFCElement> nlOrderLine = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
      for (YFCElement eleOrderLine : nlOrderLine) {
        String maxLineStatus = eleOrderLine.getAttribute(GCConstants.MAX_LINE_STATUS);
        if (YFCUtils.equals(GCConstants.CANCELLED_ORDER_STATUS_CODE, maxLineStatus)) {
          YFCElement eleStatusBreakupForCanceledQty = eleOrderLine.getChildElement(GCConstants.STATUS_BREAKUP_QTY);
          // GCSTORE-4178::Begin
          if (!YFCCommon.isVoid(eleStatusBreakupForCanceledQty)) {
            // GCSTORE-4178::End
            YFCElement eleCanceledFrom = eleStatusBreakupForCanceledQty.getChildElement(GCConstants.CANCELLED_FROM);
            String status = eleCanceledFrom.getAttribute(GCConstants.STATUS);
            if (YFCUtils.equals(GCConstants.SCHEDULED_PO_DAX_STATUS, status)
                || YFCUtils.equals(GCConstants.BACKORDERED_STATUS, status)) {
              cancelledOdrLineKeyList.add(eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
            }
          }
        }
      }
    }
    LOGGER.verbose("GCDAXSyncMessageAPI.listCancelledOdrLine -- End");
    LOGGER.endTimer("GCDAXSyncMessageAPI.listCancelledOdrLine");
  }

  private void stampCancellationReasonText(YFSEnvironment env, Document getOrderListOutDoc) {

    String sCancellationReasonCode = (String) env.getTxnObject(GCConstants.CANCELLATION_REASON_CODE);
    if (!YFCCommon.isVoid(sCancellationReasonCode)) {

      Document docGetCommonCodeListIp = GCXMLUtil.createDocument(GCConstants.COMMON_CODE);
      Element eleGetCommonCodeListIpRoot = docGetCommonCodeListIp.getDocumentElement();
      eleGetCommonCodeListIpRoot.setAttribute(GCConstants.CODE_TYPE, GCConstants.YCD_CANCEL_REASON);
      eleGetCommonCodeListIpRoot.setAttribute(GCConstants.CODE_VALUE, sCancellationReasonCode);

      Document docGetCommonCodeListOp =
          GCCommonUtil.invokeAPI(env, GCConstants.API_GET_COMMON_CODE_LIST, docGetCommonCodeListIp);
      String sCodeLongDescription =
          GCXMLUtil.getAttributeFromXPath(docGetCommonCodeListOp, "CommonCodeList/CommonCode/@CodeLongDescription");

      // Stamp code long description as note text at header level in
      // return document

      Element eleNotes = GCXMLUtil.getElementByXPath(getOrderListOutDoc, "/OrderList/Order/Notes");

      if (YFCCommon.isVoid(eleNotes)) {
        eleNotes = getOrderListOutDoc.createElement(GCConstants.NOTES);
        Element eleOrder = GCXMLUtil.getElementByXPath(getOrderListOutDoc, "/OrderList/Order");
        eleOrder.appendChild(eleNotes);
      }

      Element eleNote = getOrderListOutDoc.createElement(GCConstants.NOTES);
      eleNotes.appendChild(eleNote);
      eleNote.setAttribute(GCConstants.NOTE_TEXT, GCConstants.CANCELLATION_REASON_CODE_PREFIX + sCodeLongDescription);
    }
  }


  /**
   *
   * This method will stamp StatusQuantity on BundleLines with ProductClass as SET. The status
   * quantity and status will be based on the component.
   *
   *
   * @param inDoc
   *
   */
  private static void stampStatusAndQuantityOnSetBundleLine(Document inDoc) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.info("Inside stampStatusAndQuantityOnSetBundleLine" + GCXMLUtil.getXMLString(inDoc));
    }

    final List<Element> listBundleParentOrderLine =
        GCXMLUtil.getElementListByXpath(inDoc,
            "OrderList/Order/OrderLines/OrderLine[@KitCode='BUNDLE' and Item[@ProductClass='SET']]");



    for (Element eleBundleParentOrderLine : listBundleParentOrderLine) {

      // Fetching the bundle orderlinekey
      final String strBundleOrderLineKey = eleBundleParentOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);


      // Fetching the Order Statuses from the parent
      final Element eleOrderStatuses =
          GCXMLUtil.getFirstElementByName(eleBundleParentOrderLine, GCConstants.ORDER_STATUSES);


      // Fetch the OrderStatus tag from the bundle line
      final NodeList nlBundleStatus = eleBundleParentOrderLine.getElementsByTagName(GCConstants.ORDER_STATUS);



      // Fetching one of the Component OrderLine
      final Element eleOrderLine =
          GCXMLUtil.getElementByXPath(inDoc, "OrderList/Order/OrderLines/OrderLine[@BundleParentOrderLineKey='"
              + strBundleOrderLineKey + "']");

      // Component Line Status Element
      final NodeList nlOrderStatus = eleOrderLine.getElementsByTagName(GCConstants.ORDER_STATUS);

      String strOrderLineScheduleKey = "";
      String strOrderReleaseStatusKey = "";
      String strPipelineKey = "";
      int iBundleOrderedQty = 0;
      Element eleBundleOrderStatus;
      final int iBundleStatusCount = nlBundleStatus.getLength();
      for (int iBundleStatusIndex = 0; iBundleStatusIndex < iBundleStatusCount; iBundleStatusIndex++) {

        eleBundleOrderStatus = (Element) nlBundleStatus.item(iBundleStatusIndex);

        if (eleBundleOrderStatus != null) {
          strOrderLineScheduleKey = eleBundleOrderStatus.getAttribute(GCConstants.ORDER_LINE_SCHEDULE_KEY);
          strOrderReleaseStatusKey = eleBundleOrderStatus.getAttribute(GCConstants.ORDER_RELEASE_STATUS_KEY);
          strPipelineKey = eleBundleOrderStatus.getAttribute(GCConstants.PIPELINE_KEY);
          iBundleOrderedQty = Integer.valueOf(eleBundleOrderStatus.getAttribute(GCConstants.TOTAL_QUANTITY));

          if (eleOrderStatuses != null) {
            eleOrderStatuses.removeChild(eleBundleOrderStatus);
          }
        }
      }

      final int iOrderStatusLen = nlOrderStatus.getLength();
      Element eleComponentOrderStatus;
      int iOrderedQty;
      int iStatusQty;
      int iBundleStatusQty;
      Element eleOrderStatus;

      // Fetching the Order Status List from the component Lines
      for (int iOrderStatusIndex = 0; iOrderStatusIndex < iOrderStatusLen; iOrderStatusIndex++) {

        eleComponentOrderStatus = (Element) nlOrderStatus.item(iOrderStatusIndex);

        if (eleComponentOrderStatus != null) {

          iOrderedQty = Integer.valueOf(eleComponentOrderStatus.getAttribute(GCConstants.TOTAL_QUANTITY));

          iStatusQty = Integer.valueOf(eleComponentOrderStatus.getAttribute(GCConstants.STATUS_QTY));

          if (iOrderedQty > 0) {
            iBundleStatusQty = (iStatusQty * iBundleOrderedQty) / iOrderedQty;
          } else {
            iBundleStatusQty = 0;
          }

          eleOrderStatus = GCXMLUtil.createElement(inDoc, GCConstants.ORDER_STATUS, null);
          eleOrderStatus.setAttribute(GCConstants.ORDER_LINE_SCHEDULE_KEY, strOrderLineScheduleKey);
          eleOrderStatus.setAttribute(GCConstants.ORDER_RELEASE_STATUS_KEY, strOrderReleaseStatusKey);
          eleOrderStatus.setAttribute(GCConstants.PIPELINE_KEY, strPipelineKey);
          eleOrderStatus.setAttribute(GCConstants.ORDER_HEADER_KEY,
              eleComponentOrderStatus.getAttribute(GCConstants.ORDER_HEADER_KEY));
          eleOrderStatus.setAttribute(GCConstants.ORDER_LINE_KEY, strBundleOrderLineKey);
          eleOrderStatus.setAttribute(GCConstants.SHIP_NODE,
              eleComponentOrderStatus.getAttribute(GCConstants.SHIP_NODE));
          eleOrderStatus.setAttribute(GCConstants.STATUS, eleComponentOrderStatus.getAttribute(GCConstants.STATUS));
          eleOrderStatus.setAttribute(GCConstants.STATUS_DATE,
              eleComponentOrderStatus.getAttribute(GCConstants.STATUS_DATE));
          eleOrderStatus.setAttribute(GCConstants.STATUS_DESCRIPTION,
              eleComponentOrderStatus.getAttribute(GCConstants.STATUS_DESCRIPTION));
          eleOrderStatus.setAttribute(GCConstants.STATUS_QTY, String.valueOf(iBundleStatusQty));
          eleOrderStatus.setAttribute(GCConstants.TOTAL_QUANTITY, String.valueOf(iBundleOrderedQty));
          eleOrderStatuses.appendChild(eleOrderStatus);

        }
      }
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" Updated document is" + GCXMLUtil.getXMLString(inDoc));
    }
  }


  /**
   * This method is used to remove the lines from document which are not in scheduledPODAX or
   * Backordered status.
   *
   * @param getOrderListOutDoc
   * @param cancelledOdrLineKeyList
   */
  private void modifyOutputDoc(YFSEnvironment env,Document getOrderListOutDoc, List<String> cancelledOdrLineKeyList, HashMap<String, YFCElement> inMap) {

    LOGGER.beginTimer("GCDAXSyncMessageAPI.modifyOutputDoc");
    LOGGER.verbose("GCDAXSyncMessageAPI.modifyOutputDoc -- Begin");
    // GCSTORE-3699
    Document getOrderListClone = GCXMLUtil.createDocument("OrderList");
    getOrderListClone.getDocumentElement().appendChild(
        getOrderListClone.importNode(getOrderListOutDoc.getDocumentElement().getFirstChild(), true));
    // GCSTORE-3699
    YFCDocument getOdrLstOutDoc = YFCDocument.getDocumentFor(getOrderListOutDoc);
    YFCElement eleOdrOutput = getOdrLstOutDoc.getDocumentElement().getChildElement(GCConstants.ORDER);
    YFCElement eleOdrLines = eleOdrOutput.getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> nlOdrLine = eleOdrLines.getElementsByTagName(GCConstants.ORDER_LINE);
    // GCSTORE-6763:Start
    Object obj = env.getTxnObject("SendCancellationToDAXObj");
    ArrayList<String> orderLineKeyL = null;
    if(!YFCCommon.isVoid(obj)){
    	orderLineKeyL = (ArrayList<String>) obj;
    }
    // GCSTORE-6763:End
    for (int i = 0; i < nlOdrLine.getLength(); i++) {
      YFCElement eleOdrLine = nlOdrLine.item(i);
      String maxLineStatus = eleOdrLine.getAttribute(GCConstants.MAX_LINE_STATUS);
      String minLineStatus = eleOdrLine.getAttribute(GCConstants.MIN_LINE_STATUS);
      // GCSTORE-6763:Start
      if(null != orderLineKeyL && !orderLineKeyL.isEmpty()){
    	 String sOrderLineKey = eleOdrLine.getAttribute(GCConstants.ORDER_LINE_KEY); 
    	 if(orderLineKeyL.contains(sOrderLineKey)){
    		 eleOdrLine.setAttribute(GCConstants.ORDERED_QTY, GCConstants.ZERO);
    		 continue;
    	 }else{
    		 eleOdrLines.removeChild(eleOdrLine);
    		 i--;
    	 }
      }// GCSTORE-6763:End
      else{

      if (YFCUtils.equals(maxLineStatus, GCConstants.SCHEDULED_PO_DAX_STATUS)
          || YFCUtils.equals(minLineStatus, GCConstants.SCHEDULED_PO_DAX_STATUS)) {

        continue;
      } else if (cancelledOdrLineKeyList.contains(eleOdrLine.getAttribute(GCConstants.ORDER_LINE_KEY))) {
        continue;
      //GCSTORE-6696--Start
      } else if(YFCUtils.equals(maxLineStatus, GCConstants.BACKORDERED_STATUS)
              || YFCUtils.equals(minLineStatus, GCConstants.BACKORDERED_STATUS)){
    	  if(inMap.containsKey(eleOdrLine.getAttribute(GCConstants.ORDER_LINE_KEY))){
    		  YFCElement eleOrderLine = inMap.get(eleOdrLine.getAttribute(GCConstants.ORDER_LINE_KEY));
    		  String shipNode = eleOrderLine.getAttribute(GCConstants.SHIP_NODE);
    		  String strIsFirmPredefinedNode = eleOrderLine.getAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE);
    		  if(GCConstants.YES.equalsIgnoreCase(strIsFirmPredefinedNode) && !YFCCommon.isStringVoid(shipNode)){
    			  eleOdrLines.removeChild(eleOdrLine);
     		      i--;
    		  }else{
    			continue;
    		  }
    	  }
      }//GCSTORE-6696--End
      else {
        eleOdrLines.removeChild(eleOdrLine);
        i--;
      }
    }
   }
    // GCSTORE-3699::Begin
    addBundleSetParentOrderLine(getOrderListOutDoc, getOrderListClone,orderLineKeyL);
 // GCSTORE-GCSTORE-3699::End
  //GCSTORE-6696--Start
    if(0 == nlOdrLine.getLength()){
    	eleOdrOutput.setAttribute("doNotPublishToDAX", GCConstants.YES);
    }
  //GCSTORE-6696--End
    LOGGER.verbose("GCDAXSyncMessageAPI.modifyOutputDoc -- End");
    LOGGER.endTimer("GCDAXSyncMessageAPI.modifyOutputDoc");
  }


  /**
   * GCSTORE-3699::This Method adds BundleSetParentItem OrderLine.
   *
   * @param getOrderListClone
   *
   * @param getOrderListOutDoc
   */
  private void addBundleSetParentOrderLine(Document inDoc, Document getOrderListClone, ArrayList<String> orderLineKeyL) {
    LOGGER.beginTimer("addBundleSetParentOrderLine");

    final List<Element> listBundleComponentOrderLine =
        GCXMLUtil.getElementListByXpath(inDoc,
            "OrderList/Order/OrderLines/OrderLine[Item/@ProductClass='SET']");
    for (Element componentEle : listBundleComponentOrderLine) {
      Element eleBundleParentLine = (Element) componentEle.getElementsByTagName("BundleParentLine").item(0);
      String parentLineKey = eleBundleParentLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      List<Element> bundleParentLineList =
          GCXMLUtil.getElementListByXpath(inDoc, "OrderList/Order/OrderLines/OrderLine[@OrderLineKey='"
              + parentLineKey
              + "']");
      if (YFCCommon.isVoid(bundleParentLineList) || bundleParentLineList.size() <= 0) {
        bundleParentLineList =
            GCXMLUtil.getElementListByXpath(getOrderListClone, "OrderList/Order/OrderLines/OrderLine[@OrderLineKey='"
                + parentLineKey + "']");

        if (!YFCCommon.isVoid(bundleParentLineList) && bundleParentLineList.size() > 0) {
          System.out.println("Appending it from clone::ParentLineKey===" + parentLineKey);
          Element bundleParent = bundleParentLineList.get(0);
          String orderedQty = bundleParent.getAttribute(GCConstants.ORDERED_QTY);
          if(!GCConstants.ZERO.equalsIgnoreCase(orderedQty) && (null != orderLineKeyL && !orderLineKeyL.isEmpty())){
        	  bundleParent.setAttribute(GCConstants.ORDERED_QTY, GCConstants.ZERO); 
          }
          componentEle.getParentNode().appendChild(inDoc.importNode(bundleParent, true));
        }
      }

    }

    LOGGER.endTimer("addBundleSetParentOrderLine");
  }

  /**
   * Description of setProperties
   *
   * @param arg0
   * @throws Exception
   * @see com.yantra.interop.japi.YIFCustomApi#setProperties(java.util.Properties)
   */

  @Override
  public void setProperties(Properties arg0) {

  }

}
