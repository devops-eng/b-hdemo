/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: This class is used to resolve the holds on the warranty lines when their parent line is released.
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0             05/05/2015          Saxena, Ekansh      This class is used to apply the holds on the warranty lines.
 #################################################################################################################################################################
 */
package com.gc.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class is used to apply the holds on the warranty lines.
 *
 * @author Ekansh Saxena
 *
 */

public class GCDependentLineHold {

	// initialize logger
	private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCDependentLineHold.class);

	public Document applyHoldOnDependentLine(YFSEnvironment env, Document inDoc) {

		LOGGER.beginTimer(" Entering GCDependentLineHold.applyHoldOnDependentLine() method ");
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(" applyHoldOnDependentLine() method, Input document is" + GCXMLUtil.getXMLString(inDoc));
		}

		YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
		YFCElement targetOrderEle = inputDoc.getDocumentElement();

		String sOrderHeaderKey = targetOrderEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
		YFCDocument getOrderListIp = YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "'/>");
		YFCDocument getOrderListTemp =
				YFCDocument.getDocumentFor("<OrderList><Order OrderHeaderKey='' OrderNo=''>"
						+ "<OrderLines><OrderLine KitCode='' DependentOnLineKey='' OrderLineKey='' MaxLineStatus='' MinLineStatus=''>"
						+ "<Extn ExtnIsWarrantyItem='' ExtnWarrantyLineNumber='' ExtnIsFreeGiftItem=''/>"
						+ "<BundleParentLine OrderLineKey=''/></OrderLine></OrderLines></Order></OrderList>");
		YFCDocument getOrderListOp =
				GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListIp, getOrderListTemp);
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(" Get OrderList Output document is" + getOrderListOp.toString());
		}
		YFCElement eleGetOrderList = getOrderListOp.getDocumentElement();
		YFCElement eleGetOrder = eleGetOrderList.getChildElement(GCConstants.ORDER);
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(" Going to call processOrderToStampDependentLineHold with sourceOrderEle = " +eleGetOrder +"\n and targetOrderEle="+targetOrderEle);
		}
		processOrderToStampDependentLineHold(env,eleGetOrder,targetOrderEle);
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(" applyHoldOnDependentLine() method, Input document is" + inputDoc.toString());
		}
		LOGGER.endTimer(" Exiting GCDependentLineHold.applyHoldOnDependentLine() method ");

		return inputDoc.getDocument();

	}

	public void processOrderToStampDependentLineHold(YFSEnvironment env, YFCElement sourceOrderEle, YFCElement targetOrderEle){
		LOGGER.beginTimer(" Exiting GCDependentLineHold.processOrderToStampDependentLineHold() method ");
		// Iterate through all the orderlines (find out all the orderlines through get order list api
		// call
		// If any orderline is warranty with status less than Release, consider this as candidate for
		// warranty hold
		// also stores all the lines which are parent or regular with their line status (for bundle,
		// stores components status)
		String sExtnIsWarrantyItem = "";
		String sExtnIsFreeGiftItem = "";
		List<String> sOrderLineKeyForHold = new ArrayList<String>();
		Map<String, String> oLKeyStatusMap = new HashMap<String, String>();
		YFCElement eleGetOrderLines =sourceOrderEle.getChildElement(GCConstants.ORDER_LINES);
		YFCNodeList<YFCElement> nlGetOrderLine = eleGetOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
		for (YFCElement eleGetOrderLine : nlGetOrderLine) {
			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose(" Iterating through lines to fill the hold list" + eleGetOrderLine);
			}
			String sDependentOnLineKey = eleGetOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
			String sOrderLineKey = eleGetOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
			String sMaxOrderLineStatus = eleGetOrderLine.getAttribute(GCConstants.ORDER_LINE_MAX_STATUS);
			String kitCode = eleGetOrderLine.getAttribute(GCConstants.KIT_CODE);


			YFCElement eleGetOrderLineExtn = eleGetOrderLine.getChildElement(GCConstants.EXTN);

			if (!YFCCommon.isVoid(eleGetOrderLineExtn)) {
				sExtnIsWarrantyItem = eleGetOrderLineExtn.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
				sExtnIsFreeGiftItem = eleGetOrderLineExtn.getAttribute(GCConstants.EXTN_IS_FREE_GIFT_ITEM);
			}

			if ((YFCCommon.equalsIgnoreCase(GCConstants.YES, sExtnIsWarrantyItem) || YFCCommon.equalsIgnoreCase(GCConstants.YES, sExtnIsFreeGiftItem))
					&& !YFCCommon.isStringVoid(sDependentOnLineKey) && "2100".compareTo(sMaxOrderLineStatus) > 0) {
				LOGGER.verbose(" This is an associated warranty or gift item with status less than Released of the warranty or gift item");
				sOrderLineKeyForHold.add(sOrderLineKey);
			}
			// In case of bundle, do not store orderline key with its status. As status of bundle parent
			// is never Released
			// Instead if there is component, store the status of component with parent key
			YFCElement bundleParentLineEle = eleGetOrderLine.getChildElement(GCConstants.BUNDLE_PARENT_LINE);
			if (!GCConstants.BUNDLE.equalsIgnoreCase(kitCode) && YFCCommon.isVoid(bundleParentLineEle)) {
				LOGGER.verbose(" This is not a bundle. So going to store its orderline key with maxline status");
				oLKeyStatusMap.put(sOrderLineKey, sMaxOrderLineStatus);
			}

			if (!YFCCommon.isVoid(bundleParentLineEle)) {
				LOGGER.verbose(" This is a component. So going to store its status with its parent orderline key");
				String sBundleOrderLineKey = bundleParentLineEle.getAttribute("OrderLineKey");
				if (!oLKeyStatusMap.containsKey(sBundleOrderLineKey)) {
					LOGGER.verbose(" parent orderline key is not there in the map. So will have to store it");
					oLKeyStatusMap.put(sBundleOrderLineKey, sMaxOrderLineStatus);
				}
			}
		}

		// Remove the orderlines which have been selected as candidate for orderline key but due to its
		// parent status, may not go through hold
		LOGGER.verbose(" Going for further filter out based upon parents status with list of orderlinekey"
				+ sOrderLineKeyForHold);
		removeOrderLineKeyNotEligibleForHold(nlGetOrderLine, sOrderLineKeyForHold, oLKeyStatusMap);
		LOGGER.verbose(" Got the filtered out list of orderlinekey" + sOrderLineKeyForHold);
		YFCElement eleOrderLines = targetOrderEle.getChildElement(GCConstants.ORDER_LINES);
		if (!YFCCommon.isVoid(eleOrderLines)) {
			YFCNodeList<YFCElement> nlOrderLine = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
			for (YFCElement eleOrderLine : nlOrderLine) {
				if (LOGGER.isVerboseEnabled()) {
					LOGGER.verbose(" Iterating through lines to stamp hold on warranty line or gift item line" + eleOrderLine);
				}
				String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
				if (sOrderLineKeyForHold.contains(sOrderLineKey)) {
					LOGGER.verbose(" OrderLine key is found in list. Indeed a candidate for dependent line hold ");
					YFCElement eleOrderHoldTypes = eleOrderLine.createChild(GCConstants.ORDER_HOLD_TYPES);
					YFCElement eleOrderHoldType = eleOrderHoldTypes.createChild(GCConstants.ORDER_HOLD_TYPE);
					eleOrderHoldType.setAttribute(GCConstants.HOLD_TYPE, "DEPENDENT_LINE_HOLD");
					eleOrderHoldType.setAttribute(GCConstants.STATUS, "1100");
					sOrderLineKeyForHold.remove(sOrderLineKey);
				}
			}
		}

		if (!sOrderLineKeyForHold.isEmpty()) {
			LOGGER
			.verbose(" There are still some orderline key which are not present in input doc. But eligible for hold. Going to stamp it separatly for change order ");
			if (YFCCommon.isVoid(eleOrderLines)) {
				eleOrderLines = targetOrderEle.createChild(GCConstants.ORDER_LINES);
				LOGGER.verbose(" Could not find OrderLines, so created it");
			}
			for (String sOLKey : sOrderLineKeyForHold) {
				LOGGER.verbose(" Now iterating thorugh hold list and creating hold for these warranties");
				YFCElement eleOrderLine = eleOrderLines.createChild(GCConstants.ORDER_LINE);
				eleOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, sOLKey);
				YFCElement eleOrderHoldTypes = eleOrderLine.createChild(GCConstants.ORDER_HOLD_TYPES);
				YFCElement eleOrderHoldType = eleOrderHoldTypes.createChild(GCConstants.ORDER_HOLD_TYPE);
				eleOrderHoldType.setAttribute(GCConstants.HOLD_TYPE, "DEPENDENT_LINE_HOLD");
				eleOrderHoldType.setAttribute(GCConstants.STATUS, "1100");
			}
		}

		LOGGER.endTimer(" Exiting GCDependentLineHold.processOrderToStampDependentLineHold() method ");
	}

	/**
	 * This method iterates through each lines of the order. if any of the orderline has been selected
	 * for hold a nd its parent's status is beyond release then hold should not get applied
	 *
	 * @param nlGetOrderLine
	 * @param sOrderLineKeyForHold
	 * @param oLKeyStatusMap
	 */
	private void removeOrderLineKeyNotEligibleForHold(YFCNodeList<YFCElement> nlGetOrderLine,
			List<String> sOrderLineKeyForHold, Map<String, String> oLKeyStatusMap) {
		LOGGER.beginTimer(" Entering GCDependentLineHold.removeOrderLineKeyNotEligibleForHold() method ");
		for (YFCElement eleGetOrderLine : nlGetOrderLine) {
			String sDependentOnLineKey = eleGetOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
			String sOrderLineKey = eleGetOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(" Iterating through lines" + eleGetOrderLine + " with sOrderLineKey=" + sOrderLineKey
						+ " sDependentOnLineKey=" + sDependentOnLineKey);
			}
			if (sOrderLineKeyForHold.contains(sOrderLineKey)) {
				LOGGER.verbose("OrderLineKey=" + sOrderLineKey + " have been selected for hold");
				String sParentLineStatus = oLKeyStatusMap.get(sDependentOnLineKey);
				if (!YFCCommon.isVoid(sParentLineStatus) && "2100".compareTo(sParentLineStatus) <= 0) {
					LOGGER.verbose("OrderLineKey=" + sOrderLineKey + " 's parent status is =" + sParentLineStatus
							+ ". Hence removing it for hold");
					sOrderLineKeyForHold.remove(sOrderLineKey);
				}
			}
		}
		LOGGER.endTimer(" Entering GCDependentLineHold.removeOrderLineKeyNotEligibleForHold() method ");
	}

	/**
	 * This method apply hold on the warranty line for draft orders.
	 *
	 * @param env
	 * @param inDoc
	 * @return
	 */

	public void applyHoldOnDependentLineOfDraftOrder(YFSEnvironment env, Document inDoc) {

		LOGGER.beginTimer(" Entering GCDependentLineHold.applyHoldOnDependentLineOfDraftOrder() method ");
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(" applyHoldOnDependentLine() method, Input document is" + GCXMLUtil.getXMLString(inDoc));
		}
		YFCDocument originalDoc = YFCDocument.getDocumentFor(inDoc);
		YFCElement eleRoot = originalDoc.getDocumentElement();
		String sOrderHeaderKey = eleRoot.getAttribute(GCConstants.ORDER_HEADER_KEY);
		YFCDocument inputDoc = YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "'/>");

		Document updatedInDoc = applyHoldOnDependentLine(env, inputDoc.getDocument());

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(" applyHoldOnDependentLine() method, Input document is" + GCXMLUtil.getXMLString(updatedInDoc));
		}

		YFCDocument changeOrderDoc = YFCDocument.getDocumentFor(updatedInDoc);
		YFCElement eleOrderLines = changeOrderDoc.getDocumentElement().getChildElement(GCConstants.ORDER_LINES);
		if (!YFCCommon.isVoid(eleOrderLines)) {
			YFCNodeList<YFCElement> nlOrderLine = eleOrderLines
					.getElementsByTagName("OrderLine");
			for (YFCElement eleOrderLine : nlOrderLine) {
				rmvBundleFulfillmentModeFromOrderLine(eleOrderLine);
			}
			GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, changeOrderDoc, null);
		}
		LOGGER.endTimer(" Exiting GCDependentLineHold.applyHoldOnDependentLineOfDraftOrder() method ");
	}

	private static void rmvBundleFulfillmentModeFromOrderLine(
			YFCElement eleOrderLine) {
		LOGGER.beginTimer(" Entering GCDependentLineHold.rmvBundleFulfillmentModeFromOrderLine() method ");
		LOGGER.verbose("Input eleOrderLine:" + eleOrderLine.toString());
		YFCElement eleItem = eleOrderLine.getChildElement("Item");
		if (!YFCCommon.isVoid(eleItem)) {
			String sBundleFulfillmentMode = eleItem
					.getAttribute("BundleFulfillmentMode");
			if (!YFCCommon.isVoid(sBundleFulfillmentMode)) {
				eleItem.removeAttribute("BundleFulfillmentMode");
			}
		}
		LOGGER.verbose("Changed eleOrderLine:" + eleOrderLine.toString());
		LOGGER.endTimer(" Exiting GCDependentLineHold.rmvBundleFulfillmentModeFromOrderLine() method ");
	}
}
