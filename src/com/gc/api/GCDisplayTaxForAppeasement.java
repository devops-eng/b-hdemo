/**
 * Copyright © 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: Foundation Class
 *#################################################################################################################################################################
 *          Version       CR                Date              Modified By                Description
 *#################################################################################################################################################################
             1.0     Initial Version     02/11/2015         Shinde, Abhishek              GCSTORE-5085 Calculate Taxes for Appeasement 
			 
#################################################################################################################################################################
 */
package com.gc.api;

import java.math.BigDecimal;
import java.util.Properties;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCDisplayTaxForAppeasement implements YIFCustomApi{
    //GCSTORE-5934
    private static final YFCLogCategory LOGGER = YFCLogCategory
            .instance(GCManageItem.class.getName());
     YFSEnvironment env = null;
      //GCSTORE-5934--End

	  Document inDoc = null;
	  private String sOriginalOfferAmount = "";
	
	public Document displayTaxForAppeasement (YFSEnvironment env, Document inDoc) throws TransformerException{
	    LOGGER.debug("GCDisplayTaxForAppeasment :: displayTaxForAppeasement :: inDoc :: \n"+GCXMLUtil.getXMLString(inDoc));
	      this.env = env;
	      this.inDoc = inDoc;
		Element eleOrder = inDoc.getDocumentElement();
		String sGCAppeaseType = XPathAPI.selectSingleNode(inDoc, "//@GCAppeaseType").getNodeValue();
		String sOrderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
		 Document docOrder = GCXMLUtil.createDocument("Order");
	      Element eleOrderRoot = docOrder.getDocumentElement();
	      eleOrderRoot.setAttribute("OrderHeaderKey", sOrderHeaderKey);
	      Document docGetOrderDetail =
	          GCCommonUtil.invokeAPI(env, docOrder, GCConstants.API_GET_ORDER_DETAILS,
	              GCConstants.GET_ORDER_DETAILS_TEMP_FOR_CONFIRM_APPEASEMENT);
	      LOGGER.debug("GCDisplayTaxForAppeasment :: displayTaxForAppeasement :: docGetOrderDetail :: \n"+GCXMLUtil.getXMLString(docGetOrderDetail));
	      Element orderInvoiceEle = (Element) XPathAPI.selectSingleNode(inDoc, "//OrderInvoice");
	      NodeList nlLineDetailList = XPathAPI.selectNodeList(inDoc, "//LineDetails/LineDetail");
	      sOriginalOfferAmount = XPathAPI.selectSingleNode(inDoc,
	              "//@OfferAmount").getNodeValue();
	      Element headerChargeEle = (Element) XPathAPI.selectSingleNode(orderInvoiceEle, "//HeaderChargeList");
	      orderInvoiceEle.removeChild(headerChargeEle);
	      Float sFloatOfferAmount = Float.parseFloat(sOriginalOfferAmount);
	      String sTaxAmount = "";
	      for (int i = 0; i < nlLineDetailList.getLength(); i++) {
	          Element eleLineDetail = (Element) nlLineDetailList.item(i);
	          String sOrderLineKey = eleLineDetail.getAttribute(GCConstants.ORDER_LINE_KEY);
	          
	          if (YFCCommon.equals("SHIPPING", sGCAppeaseType)) {
	            sTaxAmount = amountToRefundForShippingLevel(sFloatOfferAmount,
	                sOrderLineKey, docGetOrderDetail);
	          } else {
	            sTaxAmount = amountToRefundForLineLevel(sFloatOfferAmount,
	                sOrderLineKey, inDoc, docGetOrderDetail);
	          }
	        }
	      	
	      Document outDoc = GCXMLUtil.createDocument("Order");
	      Element eleOutDocRoot = outDoc.getDocumentElement();
	      eleOutDocRoot.setAttribute("TaxAmount", sTaxAmount);
	      LOGGER.debug("GCDisplayTaxForAppeasment :: displayTaxForAppeasement :: Returned Document :: \n"+GCXMLUtil.getXMLString(outDoc)); 
		return outDoc;
	}
	
	
	public String amountToRefundForShippingLevel(Float fOfferAmount, String orderLineKey,
		      Document getOrderDet) throws TransformerException {

	    //GCSTORE-5934-starts
	    String strParentOrderLineKey=null;
        Element eleBundleParentLine=GCXMLUtil.getElementByXPath(getOrderDet, "/Order/OrderLines/OrderLine[@OrderLineKey='"+orderLineKey+"']/BundleParentLine");
        if(!YFCCommon.isVoid(eleBundleParentLine))
        {
            strParentOrderLineKey=eleBundleParentLine.getAttribute("OrderLineKey"); 
        }
      
        LOGGER.debug("GCDisplayTaxForAppeasment :: amountToRefundForShippingLevel :: strParentOrderLineKey :: "+strParentOrderLineKey);
        Element eleorderLine=GCXMLUtil.getElementByXPath(getOrderDet, "/Order/OrderLines/OrderLine[@OrderLineKey='"+orderLineKey+"']");
	    
        if(!YFCCommon.isVoid(eleorderLine))
        {
            LOGGER.debug("GCDisplayTaxForAppeasment :: amountToRefundForShippingLevel :: eleorderLine :: \n "+GCXMLUtil.getElementString(eleorderLine));

        }
        //GCSTORE-5934-ends
		    Float fAmountToRefund = (float) 0.0;
		    Element eleLineCharge =
		        (Element) XPathAPI.selectSingleNode(getOrderDet, "Order/OrderLines/OrderLine[@OrderLineKey='" + orderLineKey
		            + "']/LineCharges/LineCharge[@ChargeName='ShippingCharge']");
		    Element eleLineTax =
		        (Element) XPathAPI.selectSingleNode(getOrderDet, "Order/OrderLines/OrderLine[@OrderLineKey='" + orderLineKey
		            + "']/LineTaxes/LineTax[@ChargeName='ShippingCharge']");

		    String sShippingCharge = "0.00";
		    String sShippingTax = "0.00";
		    // Fix to stamp TaxPercentage
		    String sShippingTaxPercentage = "0.00";
		    
		    if (!YFCCommon.isVoid(eleLineCharge)) {
		      sShippingCharge = eleLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT);
		    }
		    if (!YFCCommon.isVoid(eleLineTax)) {
		      sShippingTax = eleLineTax.getAttribute(GCConstants.TAX);
		      sShippingTaxPercentage = eleLineTax.getAttribute(GCConstants.TAX_PERCENTAGE);
		      if(YFCCommon.isVoid(sShippingTaxPercentage)){
		    	  sShippingTaxPercentage = "0.00";
		      }
		    }
		    
		    //GCSTORE-5934-starts
		    if(!YFCCommon.isVoid(strParentOrderLineKey))
		    {
		        LOGGER.debug("GCDisplayTaxForAppeasment :: amountToRefundForShippingLevel :: It is kit component ");
		       Element eleExtn=(Element) eleorderLine.getElementsByTagName("Extn").item(0);
		       if(!YFCCommon.isVoid(eleExtn))
		       {
		           LOGGER.debug("GCDisplayTaxForAppeasment :: amountToRefundForShippingLevel :: eleExtn :: \n"+GCXMLUtil.getElementString(eleExtn));
		           Document docExtn=GCXMLUtil.getDocumentFromElement(eleExtn);   
		           sShippingCharge=GCXMLUtil.getAttributeFromXPath(docExtn, "/Extn/GCOrderLineComponentsList/GCOrderLineComponents[@ChargeCategory='Shipping'][@ChargeName='ShippingCharge']/@Amount"); 
		           sShippingTax=GCXMLUtil.getAttributeFromXPath(docExtn, "/Extn/GCOrderLineComponentsList/GCOrderLineComponents[@ChargeCategory='Shipping'][@ChargeName='ShippingChargeTax']/@Amount");       
		           sShippingTaxPercentage=GCXMLUtil.getAttributeFromXPath(getOrderDet,"Order/OrderLines/OrderLine[@OrderLineKey='" + strParentOrderLineKey
		                    + "']/LineTaxes/LineTax[@ChargeName='ShippingCharge']/@TaxPercentage");
		           if(YFCCommon.isVoid(sShippingCharge))
		           {
		               sShippingCharge="0.00";
		           }
		           if(YFCCommon.isVoid(sShippingTax))
		           {
		               sShippingTax="0.00";
		           }
		           if(YFCCommon.isVoid(sShippingTaxPercentage))
                   {
		               sShippingTaxPercentage="0.00";
                   }
		           LOGGER.debug("GCDisplayTaxForAppeasment :: amountToRefundForShippingLevel :: sShippingCharge :: "+sShippingCharge+" :: sShippingTaxPercentage ::"+sShippingTaxPercentage);
		       }
		    }
		  //GCSTORE-5934-ends  
		    
		    Float fShippingCharge = Float.parseFloat(sShippingCharge);
		    Float fShippingTax = Float.parseFloat(sShippingTax);
		    Float fTax=(float)0.0;
		    fAmountToRefund = fOfferAmount;
		    if (fShippingTax!= 0) {
		      fTax=((fOfferAmount / fShippingCharge) * fShippingTax);
		    }
		    BigDecimal bgTax = new BigDecimal(fTax);
		    bgTax = bgTax.setScale(2, BigDecimal.ROUND_HALF_UP);
		    String sTax = bgTax.toString();
		    LOGGER.debug("GCDisplayTaxForAppeasment :: amountToRefundForShippingLevel :: Returned Tax ::"+sTax);
			return sTax;
		  
		  }

	public String amountToRefundForLineLevel(Float fOfferAmount, String orderLineKey, Document inDoc, Document getOrderDet)
		      throws TransformerException {

	  //GCSTORE-5934-starts
        String strParentOrderLineKey=null;
        Element eleBundleParentLine=GCXMLUtil.getElementByXPath(getOrderDet, "/Order/OrderLines/OrderLine[@OrderLineKey='"+orderLineKey+"']/BundleParentLine");
        if(!YFCCommon.isVoid(eleBundleParentLine))
        {
            strParentOrderLineKey=eleBundleParentLine.getAttribute("OrderLineKey"); 
        }
      
        LOGGER.debug("GCDisplayTaxForAppeasment :: amountToRefundForLineLevel :: strParentOrderLineKey :: "+strParentOrderLineKey);
        
        Element eleorderLine=GCXMLUtil.getElementByXPath(getOrderDet, "/Order/OrderLines/OrderLine[@OrderLineKey='"+orderLineKey+"']");
        if(!YFCCommon.isVoid(eleorderLine))
        {
            LOGGER.debug("GCDisplayTaxForAppeasment :: amountToRefundForLineLevel :: eleorderLine :: \n "+GCXMLUtil.getElementString(eleorderLine));
            
        }
        
      //GCSTORE-5934-ends
		   
		    Float fAmountToRefund = (float) 0.0;
		    Element eleLineTax =
		        (Element) XPathAPI.selectSingleNode(getOrderDet, "/Order/OrderLines/OrderLine[@OrderLineKey='" + orderLineKey
		            + "']/LineTaxes/LineTax[@ChargeName='LinePriceTax']");

		    Element eleLineOverallTotal =
		        (Element) XPathAPI.selectSingleNode(getOrderDet, "Order/OrderLines/OrderLine[@OrderLineKey='" + orderLineKey
		            + "']/LineOverallTotals");

		    Element eleLineCharge =
		        (Element) XPathAPI.selectSingleNode(getOrderDet, "Order/OrderLines/OrderLine[@OrderLineKey='" + orderLineKey
		            + "']/LineCharges/LineCharge[@ChargeName='ShippingCharge']");

		    String sShippingCharge = "0.00";
		    if (!YFCCommon.isVoid(eleLineCharge)) {
		      sShippingCharge = eleLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT);
		    }
		    
		    // GCSTORE-6058 Begin
		    if (YFCCommon.isVoid(sShippingCharge)) {
		    	sShippingCharge="0.00";
		    }
		    // GCSTORE-6058 End
		    
		    Float fLineTax =(float)0.0;
		    Float fTaxPercentage = (float)0.0;
		    Float fLineTotalWithoutTax = (float)0.0;
		    Float fShippingCharge = Float.parseFloat(sShippingCharge);
		    String sTaxPercentage;
            //GCSTORE-5934--Start
		    String sLineTax;
		    String sLineTotalWithoutTax;
            //GCSTORE-5934--End
		    if(!YFCCommon.isVoid(eleLineTax)){
                //GCSTORE-5934--Start
			    sLineTax = eleLineTax.getAttribute(GCConstants.TAX);
			    sTaxPercentage=eleLineTax.getAttribute(GCConstants.TAX_PERCENTAGE);
			   sLineTotalWithoutTax = eleLineOverallTotal.getAttribute(GCConstants.LINE_TOTAL_WITHOUT_TAX);
			   //GCSTORE-5934--End
			    fLineTax = Float.parseFloat(sLineTax);
			    fTaxPercentage =Float.parseFloat(sTaxPercentage);
			    fLineTotalWithoutTax = Float.parseFloat(sLineTotalWithoutTax);
		    }
		    Float fLineToatlWithoutShipping=fLineTotalWithoutTax-fShippingCharge;
               //GCSTORE-5934-starts
            
            if(!YFCCommon.isVoid(strParentOrderLineKey))
            {
                LOGGER.debug("GCDisplayTaxForAppeasment :: amountToRefundForLineLevel :: It is kit component ");
               Element eleExtn=(Element) eleorderLine.getElementsByTagName("Extn").item(0);
               if(!YFCCommon.isVoid(eleExtn))
               {
                   LOGGER.debug("GCDisplayTaxForAppeasment :: amountToRefundForLineLevel :: eleExtn :: \n"+GCXMLUtil.getElementString(eleExtn));
                   Document docExtn=GCXMLUtil.getDocumentFromElement(eleExtn);   
                   sShippingCharge=GCXMLUtil.getAttributeFromXPath(docExtn, "/Extn/GCOrderLineComponentsList/GCOrderLineComponents[@ChargeCategory='Shipping'][@ChargeName='ShippingCharge']/@Amount"); 
                   if(!YFCCommon.isVoid(sShippingCharge))
                   {
                       fShippingCharge = Float.parseFloat(sShippingCharge);
                   }
               
                   
                   sLineTax=GCXMLUtil.getAttributeFromXPath(docExtn, "/Extn/GCOrderLineComponentsList/GCOrderLineComponents[@ChargeCategory='LinePrice'][@ChargeName='LinePriceTax']/@Amount");       
                   
                   if(!YFCCommon.isVoid(sLineTax))
                   {
                       fLineTax = Float.parseFloat(sLineTax);
                   }
                   sTaxPercentage=GCXMLUtil.getAttributeFromXPath(getOrderDet,"Order/OrderLines/OrderLine[@OrderLineKey='" + strParentOrderLineKey
                            + "']/LineTaxes/LineTax[@ChargeName='LinePriceTax']/@TaxPercentage");
                  
                   if(!YFCCommon.isVoid(sTaxPercentage))
                   {
                       fTaxPercentage = Float.parseFloat(sTaxPercentage);
                   }
                  
                  Float amount=Float.parseFloat(GCXMLUtil.getAttributeFromXPath(docExtn, "/Extn/GCOrderLineComponentsList/GCOrderLineComponents[@ChargeName='UnitPrice'][@ChargeCategory='UnitPrice']/@Amount"));
                  Float quantity=Float.parseFloat(GCXMLUtil.getAttributeFromXPath(docExtn, "/Extn/GCOrderLineComponentsList/GCOrderLineComponents[@ChargeName='UnitPrice'][@ChargeCategory='UnitPrice']/@Quantity"));
                  
                  if(!YFCCommon.isVoid(amount) && !YFCCommon.isVoid(quantity) )
                  {
                      fLineToatlWithoutShipping=amount*quantity;
                  }
                  LOGGER.debug("GCDisplayTaxForAppeasment :: amountToRefundForLineLevel :: sShippingCharge :: "+sShippingCharge+" :: sLineTax ::"+sLineTax+":: sTaxPercentage ::"+sTaxPercentage+ ":: amount ::"+amount+" :: quantity ::"+quantity);
                  
               }
            }
            
            //GCSTORE-5934-ends

		    Float fTax=(float)0.0;
		    fAmountToRefund = fOfferAmount;
		    if (fLineTotalWithoutTax!= 0) {
		      fTax=((fOfferAmount / fLineToatlWithoutShipping) * fLineTax);
		    }
		    BigDecimal bgTaxPercentage=new BigDecimal(fTaxPercentage);
		    bgTaxPercentage=bgTaxPercentage.setScale(2,BigDecimal.ROUND_HALF_UP);
		    sTaxPercentage=bgTaxPercentage.toString();
		    
		    BigDecimal bgTax = new BigDecimal(fTax);
		    bgTax = bgTax.setScale(2, BigDecimal.ROUND_HALF_UP);
		    String sTax = bgTax.toString();
		    LOGGER.debug("GCDisplayTaxForAppeasment :: amountToRefundForLineLevel :: Returned Tax Amount :: "+sTax);
		    return sTax;
		  }
	@Override
	public void setProperties(Properties arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
