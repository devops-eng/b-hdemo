package com.gc.api;

import java.io.IOException;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCWebServiceUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCExpireQuoteRequestService {
			
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCExpireQuoteRequestService.class);
	
	public Document expireQuoteService(YFSEnvironment env, Document inDoc) throws TransformerException{
		
		LOGGER.debug("GCExpireQuoteRequestService :: expireQuoteService :: inDoc :: "+GCXMLUtil.getXMLString(inDoc));
		String sIsSuccess = GCConstants.YES;
		Element eleRoot = inDoc.getDocumentElement();
		String sOrderHeaderKey = eleRoot.getAttribute(GCConstants.ORDER_HEADER_KEY);

		Document docGetOrderListOp = GCCommonUtil.invokeGetOrderListAPITemp(env, sOrderHeaderKey, "/global/template/api/getOrderListTempForQuoteRequest.xml");
		
		Element eleOrderExtn = (Element) XPathAPI.selectSingleNode(docGetOrderListOp, "//Order/Extn");
		if(!YFCCommon.isVoid(eleOrderExtn)){
			String sExtnQuoteSent=eleOrderExtn.getAttribute(GCConstants.EXTN_QUOTE_SENT);
			String sExtnSourceOrderNo = eleOrderExtn.getAttribute(GCConstants.EXTN_SOURCE_ORDER_NO);
			YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
			YFCElement eleOrder = yfcInDoc.getDocumentElement();
			YFCElement eleExtn = eleOrder.getChildElement("Extn", true);
			eleExtn.setAttribute(GCConstants.EXTN_SOURCE_ORDER_NO, sExtnSourceOrderNo);
			if(GCConstants.YES.equalsIgnoreCase(sExtnQuoteSent)){

				LOGGER.debug("GCExpireQuoteRequestService :: expireQuoteService :: Begin");
				String sATGQuoteURL = YFSSystem.getProperty(GCConstants.ATG_QUOTE_URL);
				if (YFCCommon.isVoid(sATGQuoteURL)) {
					throw new YFCException(
							"ATG expired quote URL is not configured in property file. Please retry after configuring it.");
				}
				LOGGER.debug("sGCURL :: " + sATGQuoteURL);
				String sATGQuoteSoapAction = YFSSystem
						.getProperty(GCConstants.ATG_EXP_QUOTE_SOAP_ACTION);
				if (YFCCommon.isVoid(sATGQuoteSoapAction)) {
					throw new YFCException(
							"ATG expired quote soap action is not configured in property file. Please retry after configuring it.");
				}
				Document docATGCreateQuoteReq = GCCommonUtil.invokeService(env, GCConstants.GC_EXPIRE_QUOTE_REQUEST_XSL_SERVICE, inDoc);
				GCWebServiceUtil obj = new GCWebServiceUtil();
				Document docATGExpQuoteResponse = null;
				Node nIsSuccess = null;

				LOGGER.debug("docATGCreateQuoteReq :: " + sATGQuoteURL);
				try {
					docATGExpQuoteResponse = obj.invokeSOAPWebService(docATGCreateQuoteReq, sATGQuoteURL, sATGQuoteSoapAction);
				} catch (IOException e1) {
					LOGGER.error(" Inside catch block of GCExpireQuoteRequestService.expireQuoteService().docATGCreateQuoteResponse, Exception is :", e1);
					sIsSuccess = GCConstants.NO;
				}		
				try {
					nIsSuccess = XPathAPI.selectSingleNode(docATGExpQuoteResponse, "Envelope/Body/expireQuoteResponse/isSuccess");
				} catch (TransformerException e) {
					LOGGER.error(" Inside catch block of GCExpireQuoteRequestService.expireQuoteService().nIsSuccess, Exception is :", e);
					sIsSuccess = GCConstants.NO;
				}				
				if(!YFCCommon.isVoid(nIsSuccess)&&GCConstants.TRUE_STRING.equalsIgnoreCase(nIsSuccess.getTextContent())){
					sIsSuccess = GCConstants.YES;
				}
			}
		}		
		
		Document docExpireQuoteResponse = GCXMLUtil.createDocument(GCConstants.EXPIRED_QUOTE_RESPONSE);
		Element eleResponse = docExpireQuoteResponse.getDocumentElement();
		eleResponse.setAttribute(GCConstants.IS_SUCCESS, sIsSuccess);
		
		LOGGER.debug("GCExpireQuoteRequestService :: expireQuoteService :: IS_SUCCESS :: "+sIsSuccess);
		LOGGER.debug("GCExpireQuoteRequestService :: expireQuoteService :: End");
		return docExpireQuoteResponse;
		
	}
}
