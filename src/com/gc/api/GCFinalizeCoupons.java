package com.gc.api;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCWebServiceUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCFinalizeCoupons {

  String sOrderNo = "";
  String sATGURL = "";
  Document inDoc = null;
  String sSoapAction = "";
  String sCouponCode = "";
  String sEntryType = "";
  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCFinalizeCoupons.class.getName());

  public void finalizeCoupons(YFSEnvironment env, Document inDoc)
      throws GCException {
    try {
      this.inDoc = inDoc;
      
      //for mPOS orders
      sEntryType = inDoc.getDocumentElement().getAttribute(GCConstants.ENTRY_TYPE);
      
      sOrderNo = XPathAPI.selectSingleNode(inDoc, "//@OrderNo")
          .getNodeValue();
      NodeList nlGCOrderCoupon = XPathAPI.selectNodeList(inDoc,
          "//GCOrderCoupon");

      if (nlGCOrderCoupon.getLength() > 0) {
        sATGURL = YFSSystem.getProperty(GCConstants.ATG_URL);
        if (YFCCommon.isVoid(sATGURL)) {
          throw new YFCException(
              "ATG Promo URL is not configured in property file. Please retry after configuring it.");
        }
        LOGGER.debug("sGCURL :: " + sATGURL);
        String sSoapAct = YFSSystem
            .getProperty(GCConstants.ATG_FINALIZE_SOAP_ACTION);
        if (YFCCommon.isVoid(sSoapAct)) {
          throw new YFCException(
              "ATG Promo soap action is not configured in property file. Please retry after configuring it.");
        }
      }

      for (int i = 0; i < nlGCOrderCoupon.getLength(); i++) {

        sCouponCode = ((Element) nlGCOrderCoupon.item(i))
            .getAttribute("CouponCode");
        Document docFinalizeCouponIp = prepareFinalizeCouponIpDoc(sEntryType);

        GCWebServiceUtil obj = new GCWebServiceUtil();
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("Indocument"
              + GCXMLUtil.getXMLString(docFinalizeCouponIp));
        }
        Document docFinalizeCouponOp = obj.invokeSOAPWebService(
            docFinalizeCouponIp, sATGURL, sSoapAction);

        LOGGER.verbose("FinalizeCoupons Response-->:" + GCXMLUtil.getXMLString(docFinalizeCouponOp));
        Element eleMessage = GCXMLUtil
            .getElementByXPath(docFinalizeCouponOp,
                "Envelope/Body/FinalizeResponse/FinalizeResult/Message");

        if (!YFCCommon.isVoid(eleMessage)) {
          String sErrorMessage = eleMessage.getTextContent();
          raiseException(env, sErrorMessage, sEntryType);
        }
      }
    } catch (Exception e) {
      LOGGER.error("The exception is in finalizeCoupons method:", e);
      throw GCException.getYFCException(e.getMessage(), e);
    }
  }

  /**
   * 
   * @param env
   * @param sErrorMessage
   * @throws Exception
   */
  private void raiseException(YFSEnvironment env, String sErrorMessage, String sEntryType)
      throws GCException {
    try {
      final String logInfo = "GCMassOperationProcess :: createException :: ";
      LOGGER.debug(logInfo + "Begin");

      LOGGER.debug(logInfo + "sResultMessage :: " + "CouppnCode "
          + sCouponCode + " Finalization Failed");
      LOGGER.debug(logInfo + "sExceptionType :: " + "CouponFinalizeFail");
      LOGGER.debug(logInfo + "SQueueID :: " + "CSR_GROUP");

      Document docCreateExceptionIp = GCXMLUtil
          .createDocument(GCConstants.INBOX);

      Element eleRoot = docCreateExceptionIp.getDocumentElement();

      eleRoot.setAttribute(GCConstants.EXCEPTION_TYPE,
          GCConstants.GC_COUPON_FINALIZE_FAILED);
      
      //mPOS-538: Error handling for mPOS orders. New Queue created for alerts for mPOS EntryType: Start.
      if(YFCCommon.equalsIgnoreCase(sEntryType, GCConstants.MPOS))
      {
    	  eleRoot.setAttribute(GCConstants.QUEUE_ID, GCConstants.GC_MPOS_COUPON_FAILED_ALERT_QUEUE);
      }
      else
      {
    	  eleRoot.setAttribute(GCConstants.QUEUE_ID, GCConstants.GC_COUPON_FAILED_ALERT_QUEUE);
      }
    //mPOS-538: Error handling for mPOS orders. New Queue created for alerts for mPOS EntryType: End.

      Element eleInboxReferencesList = docCreateExceptionIp
          .createElement(GCConstants.INBOX_REFERENCE_LIST);
      eleRoot.appendChild(eleInboxReferencesList);

      Element eleInboxReferences1 = docCreateExceptionIp
          .createElement(GCConstants.INBOX_REFERENCES);
      eleInboxReferencesList.appendChild(eleInboxReferences1);
      eleInboxReferences1.setAttribute(GCConstants.NAME,
          GCConstants.RESULT_MESSAGE);
      eleInboxReferences1.setAttribute(GCConstants.VALUE, "CouponCode "
          + sCouponCode + " finalization failed");
      eleInboxReferences1.setAttribute(GCConstants.REFERENCE_TYPE,
          GCConstants.TEXT);

      Element eleInboxReferences2 = docCreateExceptionIp
          .createElement(GCConstants.INBOX_REFERENCES);
      eleInboxReferences2.setAttribute(GCConstants.NAME,
          "ATGErrorMessage");
      eleInboxReferences2.setAttribute(GCConstants.VALUE, sErrorMessage);
      eleInboxReferences2.setAttribute(GCConstants.REFERENCE_TYPE,
          GCConstants.TEXT);
      eleInboxReferencesList.appendChild(eleInboxReferences2);

      Element eleOrder = docCreateExceptionIp
          .createElement(GCConstants.ORDER);
      eleRoot.appendChild(eleOrder);
      eleOrder.setAttribute(GCConstants.ORDER_NO, sOrderNo);
      eleOrder.setAttribute(GCConstants.ENTERPRISE_CODE, XPathAPI
          .selectSingleNode(inDoc, "//@EnterpriseCode")
          .getNodeValue());
      eleOrder.setAttribute(GCConstants.DOCUMENT_TYPE, XPathAPI
          .selectSingleNode(inDoc, "//@DocumentType").getNodeValue());
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(logInfo + "docCreateExceptionIp\n"
            + GCXMLUtil.getXMLString(docCreateExceptionIp));
      }
      GCCommonUtil.invokeAPI(env, GCConstants.API_CREATE_EXCEPTION,
          docCreateExceptionIp);
    } catch (Exception e) {
      LOGGER.error("The exception is in raiseException method:", e);
      throw new GCException(e);
    }

  }

  /**
   * 
   * @param inDoc
   * @param sCouponCode
   * @return
   * @throws ParserConfigurationException
   */
  private Document prepareFinalizeCouponIpDoc(String sEntryType)
      throws ParserConfigurationException {

    Document docFinalizeCouponIp = GCXMLUtil
        .createDocument("soapenv:Envelope");
    Element eleRoot = docFinalizeCouponIp.getDocumentElement();

    eleRoot.setAttribute("xmlns:soapenv",
        "http://schemas.xmlsoap.org/soap/envelope/");
    eleRoot.setAttribute("xmlns:v0",
        "http://www.guitarcenter.com/schemas/PromoEngine/V0.1");

    Element eleHeader = docFinalizeCouponIp.createElement("soapenv:Header");
    eleRoot.appendChild(eleHeader);

    Element eleBody = docFinalizeCouponIp.createElement("soapenv:Body");
    eleRoot.appendChild(eleBody);

    Element eleFinalize = docFinalizeCouponIp.createElement("v0:Finalize");
    eleBody.appendChild(eleFinalize);

    Element elesource = docFinalizeCouponIp.createElement("v0:source");
    eleFinalize.appendChild(elesource);
    
    //stamping source = 'mPOS' for mPOS orders.
    if(!YFCCommon.isVoid(sEntryType) && YFCCommon.equalsIgnoreCase(sEntryType, GCConstants.MPOS))
    {
    	elesource.setTextContent(sEntryType);
    }
    else
    {
    	elesource.setTextContent("CALL_CENTER");
    }

    Element eleCouponCode = docFinalizeCouponIp
        .createElement("v0:couponCode");
    eleFinalize.appendChild(eleCouponCode);
    eleCouponCode.setTextContent(sCouponCode);

    Element elesalesTicketNumber = docFinalizeCouponIp
        .createElement("v0:salesTicketNumber");
    eleFinalize.appendChild(elesalesTicketNumber);
    elesalesTicketNumber.setTextContent(sOrderNo);
    return docFinalizeCouponIp;

  }
}
