/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: This class is a wrapper around findInventory api.This class is exposed to ATG for real time availability check.
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0             29/12/2014          Taj, Rashma     GCSTORE-59- Real-Time Inventory Check - Ship to Customer Orders
                                                                 GCSTORE-62 - Real-Time Inventory Check - Pickup in Store Orders
             2.0             03/19/2015         Saxena, Ekansh        Added code for bundle with tag component.
 ######################################################################################################################################
 */

package com.gc.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCOrderUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class is wrapper around findInventory API.ATG calls this class synchronously to know the
 * real time availability picture for an item in OMS.
 *
 * @author rashmataj
 *
 */
public class GCFindInventoryWrapper implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCFindInventoryWrapper.class.getName());

  /**
   * This Method is used to get the service arguments.
   */
  @Override
  public void setProperties(Properties prop) {
  }

  /**
   * This method validates\stamps required Input for findInventory API returns the output.
   *
   * @param env
   * @param inputDoc
   * @return
   */
  public Document findInventory(YFSEnvironment env, Document inputDoc) {
    LOGGER.beginTimer("findInventory");

    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Incoming XML Doc" + inDoc.toString());
    }
    YFCDocument originalInDoc = inDoc.getCopy();
    YFCDocument outDoc;
    YFCElement promiseEle = inDoc.getDocumentElement();
    YFCNodeList<YFCElement> promiseLineNodeList = inDoc.getElementsByTagName(GCConstants.PROMISE_LINE);

    String sItemID;
    String sBatchNo = null;
    String sUOM;
    String productClass;
    YFCDocument getItemListOp;
    YFCDocument getItemLstTmpl;
    boolean isShipNodeBlank = false;
    boolean isPickUpInStore = false;
    boolean isUnplannedInventoryReqd = false;
    // get the fulfillmentType
    String fulfilmntType = promiseEle.getAttribute(GCXmlLiterals.FULFILLMENT_TYPE);

    Map<String, FindInventoryPromiseLineInfo> hmLineIdAndPromiseLineInfo =
        new HashMap<String, FindInventoryPromiseLineInfo>();

    for (YFCElement promiseLineEle : promiseLineNodeList) {
      filterOutNodesNotAllowedForSourcing(env, promiseLineEle);
      isShipNodeBlank = isShipNodeBlank || isShipNodeBlank(promiseLineEle);
      // call shipNode validation for serial Item
      GCOrderUtil.validateShipNodeForSerialItem(promiseLineEle, GCErrorConstants.GCE00001);
      sItemID = promiseLineEle.getAttribute(GCConstants.ITEM_ID);
      sUOM = promiseLineEle.getAttribute(GCConstants.UNIT_OF_MEASURE);
      if (YFCCommon.isVoid(fulfilmntType)) {
        fulfilmntType = promiseLineEle.getAttribute(GCConstants.FULFILLMENT_TYPE);
      }
      isPickUpInStore =
          isPickUpInStore
          || (!YFCCommon.isVoid(fulfilmntType) && fulfilmntType.indexOf(GCConstants.PICKUP_IN_STORE) >= 0);
      // get getItemList Template
      getItemLstTmpl =
          YFCDocument.getDocumentFor(GCXMLUtil.getDocument(GCConstants.GET_ITEM_LIST_TEMPLATE_ORDER_PROMISING));

      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("GetItemList Output Template XML::" + getItemLstTmpl.toString());
      }
      // call getItemList
      getItemListOp = GCCommonUtil.getItemList(env, sItemID, sUOM, GCConstants.GCI, true, getItemLstTmpl);

      YFCElement eleItemList = getItemListOp.getDocumentElement();
      YFCElement eleItem = eleItemList.getChildElement(GCConstants.ITEM);
      if(!YFCCommon.isVoid(eleItem)){
      YFCElement elePrimaryInfo = eleItem.getChildElement(GCConstants.PRIMARY_INFORMATION);
      String sKitCode = elePrimaryInfo.getAttribute(GCConstants.KIT_CODE);

      YFCElement eleTag = promiseLineEle.getChildElement(GCXmlLiterals.TAG);
      if (!YFCCommon.isVoid(eleTag)) {
        sBatchNo = eleTag.getAttribute(GCConstants.BATCH_NO);
      }

      if (YFCCommon.equals(GCConstants.BUNDLE, sKitCode, false) && !YFCCommon.isStringVoid(sBatchNo)) {
        FindInventoryPromiseLineInfo promiseLineInfo = new FindInventoryPromiseLineInfo();
        promiseLineInfo.setGetItemListOp(getItemListOp);
        hmLineIdAndPromiseLineInfo.put(promiseLineEle.getAttribute(GCConstants.LINE_ID), promiseLineInfo);
      }

      boolean isClearanceItem = GCOrderUtil.isClearanceItem(getItemListOp);

      boolean isReqRelatedToClearanceItem =
          GCOrderUtil.stampFulfillmentTypeForClearance(promiseLineEle, isClearanceItem, fulfilmntType);
      // validate low value check
      if (!isReqRelatedToClearanceItem) {
        GCOrderUtil.stampFulfillmentTypeForLowValueItems(env, promiseLineEle, getItemListOp, inDoc);
      }
      productClass = promiseLineEle.getAttribute(GCConstants.PRODUCT_CLASS);
      if (YFCCommon.isVoid(productClass)) {
        // get the product class
        productClass = GCOrderUtil.getProductClass(getItemListOp);
        LOGGER.verbose("Product Class of the Item" + sItemID + "is ::" + productClass);

          // stamp ProductClass
          promiseLineEle.setAttribute(GCConstants.PRODUCT_CLASS, productClass);
        }
        String sFulfillmentType = promiseEle.getAttribute(GCXmlLiterals.FULFILLMENT_TYPE);
        boolean isKit = GCOrderUtil.isRegularKit(env, getItemListOp);
        if (isKit) {
          String promiseLineFulfillmentType = promiseLineEle.getAttribute(GCXmlLiterals.FULFILLMENT_TYPE);
          if (YFCCommon.isVoid(promiseLineFulfillmentType)) {
            promiseLineFulfillmentType = sFulfillmentType;
          }
          if (YFCCommon.equals(promiseLineFulfillmentType, GCConstants.SHIP_2_CUSTOMER, false)) {
            promiseLineFulfillmentType = GCConstants.SHIP_2_CUSTOMER_KIT;
          }
          if (YFCCommon.equals(promiseLineFulfillmentType, GCConstants.PICKUP_IN_STORE, false)) {
            promiseLineFulfillmentType = GCConstants.PICKUP_IN_STORE_KIT;
          }
          if (YFCCommon.equals(sFulfillmentType, GCConstants.SHIP_2_CUSTOMER_ONHAND, false)) {
            sFulfillmentType = GCConstants.SHIP_2_CUSTOMER_KIT_ONHAND;
          }
          if (YFCCommon.equals(sFulfillmentType, GCConstants.PICKUP_IN_STORE_ONHAND, false)) {
            sFulfillmentType = GCConstants.PICKUP_IN_STORE_KIT_ONHAND;
          }
          promiseLineEle.setAttribute(GCConstants.FULFILLMENT_TYPE, promiseLineFulfillmentType);

          YFCElement shipNodesEle = promiseLineEle.getChildElement(GCXmlLiterals.SHIP_NODES);
          if (!YFCCommon.isVoid(shipNodesEle)) {
            YFCNodeList<YFCElement> shipNodeList = shipNodesEle.getElementsByTagName(GCXmlLiterals.SHIP_NODE);
            for (int i = 0; i < shipNodeList.getLength(); i++) {
              YFCElement shipNodeEle = shipNodeList.item(i);
              String shipNode = shipNodeEle.getAttribute(GCConstants.NODE);
              if (!YFCUtils.equals(GCConstants.MFI, shipNode)) {
                YFCElement eleExcludedShipNodes = promiseLineEle.getChildElement("ExcludedShipNodes");
                if (YFCCommon.isVoid(eleExcludedShipNodes)) {
                  eleExcludedShipNodes = promiseLineEle.createChild("ExcludedShipNodes");
                }
                YFCElement eleExcludedShipNode = eleExcludedShipNodes.createChild("ExcludedShipNode");
                eleExcludedShipNode.setAttribute(GCConstants.NODE, shipNode);
                eleExcludedShipNode.setAttribute("SupressSourcing", GCConstants.YES);

              }

            }
          }
        }
      }
    }

    // Fix for defect GCSTORE-1089
    // AllocationRuleId is set further as:
    // if ShipNode(s) coming in the input and FulfillmentType is PICKUP_IN_STORE
    // if ConsiderPartialShipment is Y then it will consider GC_SHP_SGL - This Scheduling Rule
    // considers Single Node but not complete Shipment in findInventory
    // this is required from Call Center to Check quanity even partially available and for ATG PEN
    // Service call
    // if ConsiderPartialShipment is N or Blank then consider GC_PICK - This Schedulign Rule
    // considers Single Node and ship Complete
    // this is required from ATG call
    // else, GC_DEFAULT, it is default scheduling Rule for GC enterprise. To avoid picking SYSTEM
    // (of phase-1)
    isPickUpInStore =
        isPickUpInStore
        || (!YFCCommon.isVoid(fulfilmntType) && fulfilmntType.indexOf(GCConstants.PICKUP_IN_STORE) >= 0);
    if (!isShipNodeBlank && isPickUpInStore) {
      String considerPartialShipment = promiseEle.getAttribute("ConsiderPartialShipment");
      if (YFCCommon.equals("Y", considerPartialShipment, false)) {
        promiseEle.setAttribute(GCXmlLiterals.ALLOCATION_RULE_ID, GCConstants.GC_SHIP_SINGLE_NODE);
        // GCSTORE-3315::Begin
        YFCElement eleExtn = promiseEle.getChildElement(GCConstants.EXTN);
        if (YFCCommon.isVoid(eleExtn)) {
          eleExtn = promiseEle.createChild(GCConstants.EXTN);
        }
        eleExtn.setAttribute(GCConstants.EXTN_PICK_UP_STORE_ID, GCConstants.MFI);
        // GCSTORE-3315::End
      } else {
        promiseEle.setAttribute(GCXmlLiterals.ALLOCATION_RULE_ID, GCConstants.GC_PICK);
      }
    } else {
      promiseEle.setAttribute(GCXmlLiterals.ALLOCATION_RULE_ID, GCConstants.GC_DEFAULT);
    }

    // To break bundle with tagged component into components - Start
    if (!hmLineIdAndPromiseLineInfo.isEmpty()) {
      inDoc = splitInputForSerialBundles(env, inDoc, hmLineIdAndPromiseLineInfo);
    }
    // To break bundle with tagged component into components - End

    // GCPhase2::SplitLine Logic for ship to store scenario::Begin
    String sExtnPickStoreId = "";
    YFCElement eleExtn = promiseEle.getChildElement("Extn");
    if (!YFCCommon.isVoid(eleExtn)) {
      sExtnPickStoreId = eleExtn.getAttribute(GCConstants.EXTN_PICK_UP_STORE_ID);
    }
    if (isPickUpInStore && !YFCCommon.isVoid(sExtnPickStoreId)) {
      // Check if UseUnplannedInventory="N" is required


      boolean isShipToStore = GCOrderUtil.isShipToStoreScenario(env,inDoc, sExtnPickStoreId);
      LOGGER.verbose("IsShipToStore Scenario===>" + isShipToStore);
      isUnplannedInventoryReqd = getUseUnplannedInventoryUpdated(env, inDoc);
      if (isUnplannedInventoryReqd) {
        promiseEle.setAttribute("UseUnplannedInventory", GCConstants.N);
      }
      if (isShipToStore) {
        outDoc = callFindInventoryAPI(env, inDoc);
        YFCElement elePromiseOut = outDoc.getDocumentElement();
        YFCElement eleSuggestedOption = elePromiseOut.getChildElement("SuggestedOption");
        YFCNodeList<YFCElement> unavailableLineNodeList = eleSuggestedOption.getElementsByTagName("UnavailableLine");
        if (unavailableLineNodeList.getLength() == 0) {
          // Remove all Assignment using Unplanned Inventory from stores
          stampUnavailableLinesIfUnplanned(env, outDoc);
        }

        unavailableLineNodeList = eleSuggestedOption.getElementsByTagName("UnavailableLine");
        if (unavailableLineNodeList.getLength() == 0) {
          return outDoc.getDocument();
        } else {
          // loop through all unavailable lines
          for (YFCElement unavailableLineEle : unavailableLineNodeList) {
            String lineId = unavailableLineEle.getAttribute("LineId");
            Double assignedQty = unavailableLineEle.getDoubleAttribute("AssignedQty");
            // call split lines to split the promiseLines, so that assigned qty is sourced from
            // pickup store passed and rest of the quantity based on sourcing rules defined.
            splitPromiseLines(inDoc, lineId, assignedQty, sExtnPickStoreId);
          }
        }
      }
    }
    LOGGER.verbose("Final FindInventory Input::" + inDoc.toString());
    // GCPhase2::SplitLine Logic for ship to store scenario::End

    outDoc = callFindInventoryAPI(env, inDoc);

    // Stamping Bundle item if having tag component
    if (!hmLineIdAndPromiseLineInfo.isEmpty()) {
      outDoc = stampBundleTagItemPromiseLine(env, originalInDoc, outDoc, hmLineIdAndPromiseLineInfo);
    }

    // Remove all Assignment using Unplanned Inventory from stores
    stampUnavailableLinesIfUnplanned(env, outDoc);
    Document outputDoc = outDoc.getDocument();
    // End

    LOGGER.endTimer("findInventory");
    return outputDoc;
  }

  /**
   * This method will split the promiseLine corresponding to passed LineId to source quantity
   * available at pickupStore from pickup Store and rest of the qty based on sourcing rules setup.
   *
   * @param inDoc
   * @param lineId
   * @param assignedQty
   * @param sExtnPickStoreId
   */
  private void splitPromiseLines(YFCDocument inDoc, String lineId, Double assignedQty, String sExtnPickStoreId) {
    LOGGER.beginTimer("splitPromiseLines");
    YFCElement elePromise = inDoc.getDocumentElement();
    elePromise.removeAttribute("UseUnplannedInventory");
    YFCNodeList<YFCElement> promiseLineNodeList = inDoc.getElementsByTagName("PromiseLine");
    for (YFCElement promiseLineEle : promiseLineNodeList) {
      Double requiredQty = promiseLineEle.getDoubleAttribute("RequiredQty");
      String lineIdIn = promiseLineEle.getAttribute("LineId");
      if (YFCCommon.equals(lineId, lineIdIn)) {
        // Split Lines
        // To fix if assigned qty is zero::Begin
        if (assignedQty == 0) {
          promiseLineEle.setAttribute("ShipNode", "");
          YFCElement eleShipNodes = promiseLineEle.getChildElement("ShipNodes");
          if (!YFCCommon.isVoid(eleShipNodes)) {
            eleShipNodes.getParentNode().removeChild(eleShipNodes);
          }
          YFCElement excludedShipNodesEle = inDoc.createElement("ExcludedShipNodes");
          YFCElement excludedShipNodeEle = inDoc.createElement("ExcludedShipNode");
          excludedShipNodesEle.appendChild(excludedShipNodeEle);
          promiseLineEle.appendChild(excludedShipNodesEle);

          excludedShipNodeEle.setAttribute("Node", sExtnPickStoreId);
          excludedShipNodeEle.setAttribute("SupressSourcing", "Y");
          return;
        }
        // To fix if assigned qty is zero::End
        YFCElement elePromiseLineClone = (YFCElement) promiseLineEle.cloneNode(true);
        YFCElement promiseLinesEle = inDoc.getElementsByTagName("PromiseLines").item(0);
        promiseLinesEle.appendChild(elePromiseLineClone);
        // set required qty for original line as assigned qty
        promiseLineEle.setAttribute("RequiredQty", assignedQty);
        // set required qty for cloned line as required qty- assigned qty
        elePromiseLineClone.setAttribute("RequiredQty", requiredQty - assignedQty);
        // blankout shipNode for cloned line so that the sourcing is done based on sourcing rules
        // setup
        elePromiseLineClone.setAttribute("ShipNode", "");
        YFCElement eleShipNodes = elePromiseLineClone.getChildElement("ShipNodes");
        if (!YFCCommon.isVoid(eleShipNodes)) {
          eleShipNodes.getParentNode().removeChild(eleShipNodes);
        }
        // change line id for cloned node
        elePromiseLineClone.setAttribute("LineId", lineId + "_Split");
        // mark excluded shipNode as pickup store for cloned line
        YFCElement excludedShipNodesEle = inDoc.createElement("ExcludedShipNodes");
        YFCElement excludedShipNodeEle = inDoc.createElement("ExcludedShipNode");
        excludedShipNodesEle.appendChild(excludedShipNodeEle);
        elePromiseLineClone.appendChild(excludedShipNodesEle);

        excludedShipNodeEle.setAttribute("Node", sExtnPickStoreId);
        excludedShipNodeEle.setAttribute("SupressSourcing", "Y");
        break;
      }
    }
    LOGGER.endTimer("splitPromiseLines");
  }

  /**
   * This method removes all the ShipNode from ShipNodes element of PromiseLine for which Sourcing
   * is not allowed as per ExtnIsSourceFromStoreAllowed flag of the store
   *
   * @param env
   * @param promiseLineEle
   */
  private void filterOutNodesNotAllowedForSourcing(YFSEnvironment env, YFCElement promiseLineEle) {
    YFCElement shipNodesEle = promiseLineEle.getChildElement(GCXmlLiterals.SHIP_NODES);
    if (YFCCommon.isVoid(shipNodesEle)) {
      return;
    }
    YFCNodeList<YFCElement> shipNodeList = shipNodesEle.getElementsByTagName(GCXmlLiterals.SHIP_NODE);
    if (shipNodeList.getLength() == 0) {
      return;
    }
    List<String> listOfShipNodesInInput = new ArrayList<String>();
    for (YFCElement shipNodeEle : shipNodeList) {
      String node = shipNodeEle.getAttribute(GCConstants.NODE);
      if (!YFCCommon.isVoid(node)) {
        listOfShipNodesInInput.add(shipNodeEle.getAttribute(GCConstants.NODE));
      }
    }
    List<String> storesNotAllowedForSourcing = GCOrderUtil.getStoreNotAllowedForSourcing(env, listOfShipNodesInInput);
    for (int i = 0; i < shipNodeList.getLength(); i++) {
      YFCElement shipNodeEle = shipNodeList.item(i);
      String shipNode = shipNodeEle.getAttribute(GCConstants.NODE);
      if (storesNotAllowedForSourcing.contains(shipNode)) {
        shipNodesEle.removeChild(shipNodeEle);
        i--;
      }
    }
  }

  /**
   * This method checks if the given promise Line has ShipNode stamped or not. If shipnode is not
   * stamped it returns true.
   *
   * @param promiseLineEle
   * @return
   */
  private boolean isShipNodeBlank(YFCElement promiseLineEle) {
    LOGGER.beginTimer("isShipNodeBlank");
    boolean blankShipNode = false;
    YFCElement shipNodesEle = promiseLineEle.getChildElement(GCXmlLiterals.SHIP_NODES);
    if (YFCCommon.isVoid(shipNodesEle)) {
      blankShipNode = true;
    } else {
      YFCElement shipNodeEle = shipNodesEle.getChildElement(GCConstants.SHIP_NODE);
      if (YFCCommon.isVoid(shipNodeEle)) {
        blankShipNode = true;
      } else {
        String shipNodeStr = shipNodeEle.getAttribute(GCConstants.NODE);
        if (YFCCommon.isVoid(shipNodeStr)) {
          blankShipNode = true;
        }
      }
    }
    LOGGER.verbose("Ship Node is Blank?" + blankShipNode);
    LOGGER.beginTimer("isShipNodeBlank");
    return blankShipNode;
  }

  /**
   * This method reads the template from file for findInventory API and calls findInventory API.
   *
   * @param env
   * @param inDoc
   * @return
   */
  private YFCDocument callFindInventoryAPI(YFSEnvironment env, YFCDocument inDoc) {
    LOGGER.beginTimer("callFindInventoryAPI");
    Document tmplStr = GCCommonUtil.getDocumentFromPath(GCConstants.PATH_FOR_FIND_INVENTORY_ATG_INTEG_TEMPLATE);
    YFCDocument findInvTmplDoc = YFCDocument.getDocumentFor(tmplStr);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("FindInventory Input XML" + inDoc.toString());
    }
    YFCDocument findInvOutpt = GCCommonUtil.invokeAPI(env, GCXmlLiterals.FIND_INVENTORY_API, inDoc, findInvTmplDoc);
    LOGGER.endTimer("callFindInventoryAPI");
    return findInvOutpt;

  }

  /**
   * This method checks if the given inDoc requires UseUnplannedInventory="N"
   *
   * @param inDoc
   * @return
   */

  public boolean getUseUnplannedInventoryUpdated(YFSEnvironment env, YFCDocument inDoc) {
    LOGGER.beginTimer("getUseUnplannedInventoryUpdated");
    boolean sUnplannedInventoryFlag = false;
    int counter = 0;
    YFCNodeList<YFCElement> promiseLineNodeList = inDoc.getElementsByTagName(GCConstants.PROMISE_LINE);
    for (YFCElement promiseLineEle : promiseLineNodeList) {
      String sLineShipNode = promiseLineEle.getAttribute(GCConstants.SHIP_NODE);
      if (!YFCCommon.isStringVoid(sLineShipNode)) {
        counter++;
      } else {
        YFCElement eleShipNodes = promiseLineEle.getChildElement(GCConstants.SHIP_NODES);
        if (!YFCCommon.isVoid(eleShipNodes)) {
          YFCElement eleShipNode = eleShipNodes.getChildElement(GCConstants.SHIP_NODE);
          if (!YFCCommon.isVoid(eleShipNode)) {
            String sShipNode = eleShipNode.getAttribute(GCConstants.NODE);
            if (!YFCCommon.isStringVoid(sShipNode)) {
              counter++;
            }
          }
        }
      }
    }
    if (promiseLineNodeList.getLength() == counter) {
      sUnplannedInventoryFlag = true;
    }
    LOGGER.endTimer("getUseUnplannedInventoryUpdated");
    return sUnplannedInventoryFlag;
  }

  /**
   * This method removes all the Lines on which Unplanned Inventory is picked from Store
   *
   * @param inDoc
   * @return
   */

  public YFCDocument stampUnavailableLinesIfUnplanned(YFSEnvironment env, YFCDocument inDoc) {
    LOGGER.beginTimer("stampUnavailableLinesIfUnplanned");

    double qtyUnplndInv = 0.00;
    double assignedQty = 0.00;
    YFCElement eleOutPromise = inDoc.getDocumentElement();
    YFCElement eleOutSuggOption = eleOutPromise.getChildElement(GCXmlLiterals.SUGGESTED_OPTION);
    YFCNodeList<YFCElement> outPromiseLineNodeList = eleOutSuggOption.getElementsByTagName(GCConstants.PROMISE_LINE);
    for (YFCElement eleOutPromiseLine : outPromiseLineNodeList) {
      YFCElement eleOutAssignments = eleOutPromiseLine.getChildElement(GCXmlLiterals.ASSIGNMENTS);
      YFCNodeList<YFCElement> outAssignmentNodeList = eleOutAssignments.getElementsByTagName(GCXmlLiterals.ASSIGNMENT);
      for (YFCElement eleOutAssignment : outAssignmentNodeList) {
        String sQtyFrmUnplndInv = eleOutAssignment.getAttribute("QuantityFromUnplannedInventory");
        if (!YFCCommon.isStringVoid(sQtyFrmUnplndInv)) {
          qtyUnplndInv = Double.parseDouble(sQtyFrmUnplndInv);
        }
        String sOutShipNode = eleOutAssignment.getAttribute(GCConstants.SHIP_NODE);
        if (qtyUnplndInv > 0 && !YFCCommon.equals(sOutShipNode, GCConstants.MFI, false)) {
          eleOutAssignment.getParentNode().removeChild(eleOutAssignment);
          YFCElement eleOutUnavailableLines = eleOutSuggOption.getChildElement("UnavailableLines", true);
          YFCElement eleOutUnavailableLine = eleOutUnavailableLines.createChild("UnavailableLine");
          eleOutUnavailableLine.setAttribute(GCConstants.ITEM_ID, eleOutPromiseLine.getAttribute(GCConstants.ITEM_ID));
          eleOutUnavailableLine.setAttribute(GCConstants.LINE_ID, eleOutPromiseLine.getAttribute(GCConstants.LINE_ID));
          eleOutUnavailableLine.setAttribute(GCConstants.PRODUCT_CLASS,
              eleOutPromiseLine.getAttribute(GCConstants.PRODUCT_CLASS));
          eleOutUnavailableLine.setAttribute(GCConstants.UOM, eleOutPromiseLine.getAttribute(GCConstants.UOM));
        } else {
          String sQty = eleOutAssignment.getAttribute(GCConstants.QUANTITY);
          double qty = Double.parseDouble(sQty);
          assignedQty = assignedQty + qty;
        }
      }
      YFCElement eleOutUnavailableLines = eleOutSuggOption.getChildElement("UnavailableLines");
      if (!YFCCommon.isVoid(eleOutUnavailableLines)) {
        YFCElement eleOutUnavailableLine = eleOutUnavailableLines.getChildElement("UnavailableLine");
        if (!YFCCommon.isVoid(eleOutUnavailableLine)) {
          eleOutUnavailableLine.setAttribute("AssignedQty", Double.toString(assignedQty));
        }
      }
    }

    return inDoc;
  }

  /**
   *
   * This method will create a new updated input doc if there is a bundle with tag component. So,
   * this method will take the input doc and break the tag bundle into its components and will make
   * two different promise line for the same.
   *
   * @param hmLineIdAndPromiseLineInfo
   *
   * @param YFSEnvironment variable : Env
   * @return inputDoc
   *
   */
  public YFCDocument splitInputForSerialBundles(YFSEnvironment env, YFCDocument inDoc,
      Map<String, FindInventoryPromiseLineInfo> hmLineIdAndPromiseLineInfo) {
    LOGGER.beginTimer("splitInputForSerialBundles");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input Doc"
          + inDoc.toString());
    }
    YFCDocument getItemListoutDoc;
    String sLineId;

    YFCElement elePromise = inDoc.getDocumentElement();
    YFCElement elePromiseLines = elePromise.getChildElement(GCConstants.PROMISE_LINES);
    YFCNodeList<YFCElement> nlPromiseLine = inDoc.getElementsByTagName(GCConstants.PROMISE_LINE);
    for (int i=0; i<nlPromiseLine.getLength(); i++) {
      YFCElement elePromiseLine = nlPromiseLine.item(i);
      sLineId = elePromiseLine.getAttribute(GCConstants.LINE_ID);
      String sRequiredQty = elePromiseLine.getAttribute(GCConstants.REQUIRED_QTY);
      YFCElement eleTag = elePromiseLine.getChildElement(GCXmlLiterals.TAG);
      double reqQty = Double.parseDouble(sRequiredQty);
      FindInventoryPromiseLineInfo promiseLineInfo =
          hmLineIdAndPromiseLineInfo.get(elePromiseLine.getAttribute(GCConstants.LINE_ID));
      if (promiseLineInfo != null) {
        int lineIdEnd = 1;
        getItemListoutDoc = promiseLineInfo.getGetItemListOp();
        YFCNodeList<YFCElement> n1Component = getItemListoutDoc.getElementsByTagName(GCConstants.COMPONENT);
        for (YFCElement eleComponent : n1Component) {
          String sComponentItemID = eleComponent.getAttribute(GCConstants.COMPONENT_ITEM_ID);
          String sComponentUOM = eleComponent.getAttribute(GCConstants.COMPONENT_UOM);
          String sKitQty = eleComponent.getAttribute(GCConstants.KIT_QUANTITY);
          double kitQty = Double.parseDouble(sKitQty);
          double reqrdQty = kitQty * reqQty;
          YFCDocument getItemTagOutTemplate = YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LIST_API_TMPL);
          YFCDocument getItemTagoutDoc =
              GCCommonUtil.getItemList(env, sComponentItemID, sComponentUOM, GCConstants.GCI, true, getItemTagOutTemplate);
          YFCElement eleTagItemList = getItemTagoutDoc.getDocumentElement();
          YFCElement eleTagItem = eleTagItemList.getChildElement(GCConstants.ITEM);
          YFCElement eleInvParameters = eleTagItem.getChildElement(GCConstants.INVENTORY_PARAMETERS);
          String sTagControlFlag = eleInvParameters.getAttribute(GCXmlLiterals.TAG_CONTROL_FLAG);
          YFCElement targetPromiseLineEle = elePromiseLines.createChild(GCConstants.PROMISE_LINE);
          GCXMLUtil.copyAttributes(elePromiseLine, targetPromiseLineEle);
          targetPromiseLineEle.setAttribute(GCConstants.ITEM_ID, sComponentItemID);
          targetPromiseLineEle.setAttribute(GCConstants.UOM, sComponentUOM);
          targetPromiseLineEle.setAttribute(GCConstants.KIT_CODE, "");
          targetPromiseLineEle.setAttribute(GCConstants.PRODUCT_CLASS, GCOrderUtil.getProductClass(getItemListoutDoc));
          targetPromiseLineEle.setAttribute(GCConstants.REQUIRED_QTY, Double.toString(reqrdQty));
          targetPromiseLineEle.setAttribute(GCConstants.LINE_ID, sLineId + "_Split" + Integer.toString(lineIdEnd));
          lineIdEnd++;
          YFCElement shipNodesEle = targetPromiseLineEle.createChild(GCConstants.SHIP_NODES);
          YFCElement eleShipNodes = elePromiseLine.getChildElement(GCConstants.SHIP_NODES);
          if (!YFCCommon.isVoid(eleShipNodes)) {
            YFCNodeList<YFCElement> n1ShipNodeEle = eleShipNodes.getElementsByTagName(GCConstants.SHIP_NODE);
            for (YFCElement eleShipNode : n1ShipNodeEle) {
              YFCElement shipNodeEle = shipNodesEle.createChild(GCConstants.SHIP_NODE);
              GCXMLUtil.copyAttributes(eleShipNode, shipNodeEle);
            }
          }
          YFCElement extnEle = targetPromiseLineEle.createChild(GCConstants.EXTN);
          GCXMLUtil.copyAttributes(elePromiseLine.getChildElement(GCConstants.EXTN), extnEle);
          if (YFCCommon.equals(sTagControlFlag, GCConstants.SOMETIMES_TAG_CONTROLLED, false)
              && !(YFCCommon.isVoid(eleTag))) {
            YFCElement tagEle = targetPromiseLineEle.createChild(GCXmlLiterals.TAG);
            GCXMLUtil.copyAttributes(elePromiseLine.getChildElement(GCXmlLiterals.TAG), tagEle);
          }
          YFCElement shipAddressEle = targetPromiseLineEle.createChild(GCConstants.SHIP_TO_ADDRESS);
          if(!YFCCommon.isVoid(elePromiseLine.getChildElement(GCConstants.SHIP_TO_ADDRESS))){
            GCXMLUtil.copyAttributes(elePromiseLine.getChildElement(GCConstants.SHIP_TO_ADDRESS), shipAddressEle);
          }
        }
        elePromiseLine.getParentNode().removeChild(elePromiseLine);
        i--;
      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input Doc after modification"
          + elePromise.toString());
    }
    LOGGER.endTimer("splitInputForSerialBundles");
    return inDoc;
  }

  /**
   * This method will stamp the bundle item with tag in the output of findInventory
   *
   * @param inDoc,outDoc
   * @return outDoc
   */
  public YFCDocument stampBundleTagItemPromiseLine(YFSEnvironment env, YFCDocument inDoc, YFCDocument outDoc,
      Map<String, FindInventoryPromiseLineInfo> hmLineIdAndPromiseLineInfo) {
    LOGGER.beginTimer("stampBundleTagItemPromiseLine");

    String sTagItemID = null;
    String sBundleItemID = null;
    String sLineId;
    YFCDocument getItemListoutDoc;
    int lineIdEnd = 1;

    LOGGER.verbose("FindInventory Input::" + inDoc.toString());
    LOGGER.verbose("FindInventory Output::" + outDoc.toString());
    YFCNodeList<YFCElement> promiseLineNodeList = inDoc.getElementsByTagName(GCConstants.PROMISE_LINE);
    for (YFCElement elePromiseLine : promiseLineNodeList) {
      sLineId = elePromiseLine.getAttribute(GCConstants.LINE_ID);
      FindInventoryPromiseLineInfo promiseLineInfo =
          hmLineIdAndPromiseLineInfo.get(elePromiseLine.getAttribute(GCConstants.LINE_ID));
      if (promiseLineInfo != null) {
        String sItemID = elePromiseLine.getAttribute(GCConstants.ITEM_ID);
        String sProductClass = elePromiseLine.getAttribute(GCConstants.PRODUCT_CLASS);
        getItemListoutDoc = promiseLineInfo.getGetItemListOp();
        sBundleItemID = sItemID;
        YFCNodeList<YFCElement> n1Component = getItemListoutDoc.getElementsByTagName(GCConstants.COMPONENT);
        for (YFCElement eleComponent : n1Component) {
          String sComponentItemID = eleComponent.getAttribute(GCConstants.COMPONENT_ITEM_ID);
          String sComponentUOM = eleComponent.getAttribute(GCConstants.COMPONENT_UOM);
          YFCDocument getItemTagOutTemplate = YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LIST_API_TMPL);
          YFCDocument getItemTagoutDoc =
              GCCommonUtil.getItemList(env, sComponentItemID, sComponentUOM, GCConstants.GCI, true, getItemTagOutTemplate);
          YFCElement eleTagItemList = getItemTagoutDoc.getDocumentElement();
          YFCElement eleTagItem = eleTagItemList.getChildElement(GCConstants.ITEM);
          YFCElement eleInvParameters = eleTagItem.getChildElement(GCConstants.INVENTORY_PARAMETERS);
          String sTagControlFlag = eleInvParameters.getAttribute(GCXmlLiterals.TAG_CONTROL_FLAG);
          if (YFCCommon.equals(sTagControlFlag, GCConstants.SOMETIMES_TAG_CONTROLLED, false)) {
            sTagItemID = sComponentItemID;
            YFCElement eleOutPromise = outDoc.getDocumentElement();
            YFCElement eleOutSuggOption = eleOutPromise.getChildElement(GCXmlLiterals.SUGGESTED_OPTION);
            YFCElement eleOutOption = eleOutSuggOption.getChildElement(GCXmlLiterals.OPTION);
            if (!YFCCommon.isVoid(eleOutOption)) {
              lineIdEnd = setBundlePromsLnAttr(sTagItemID, sBundleItemID,
					sLineId, lineIdEnd, eleOutOption);
            }else{
              YFCElement eleUnavailableLines = eleOutSuggOption.getChildElement("UnavailableLines");
              YFCElement eleOutBundlePromiseLine = eleUnavailableLines.createChild("UnavailableLine");
              eleOutBundlePromiseLine.setAttribute("AssignedQty", "0.00");
              eleOutBundlePromiseLine.setAttribute(GCConstants.ITEM_ID, sBundleItemID);
              eleOutBundlePromiseLine.setAttribute(GCConstants.LINE_ID, sLineId+"_Bun"+ Integer.toString(lineIdEnd));
              lineIdEnd++;
              eleOutBundlePromiseLine.setAttribute(GCConstants.UOM, sComponentUOM);
              eleOutBundlePromiseLine.setAttribute(GCConstants.PRODUCT_CLASS, sProductClass);
              break;
            }
          }
        }
      }
    }
    LOGGER.verbose("FindInventory Input::" + inDoc.toString());
    LOGGER.verbose("Final FindInventory Output::" + outDoc.toString());
    LOGGER.endTimer("stampBundleTagItemPromiseLine");
    return outDoc;
  }

private int setBundlePromsLnAttr(String sTagItemID, String sBundleItemID,
		String sLineId, int lineIdEnd, YFCElement eleOutOption) {
	YFCElement eleOutPromiseLines = eleOutOption.getChildElement(GCConstants.PROMISE_LINES);
	  YFCNodeList<YFCElement> outPromiseLineNodeList =
	      eleOutPromiseLines.getElementsByTagName(GCConstants.PROMISE_LINE);
	  for (YFCElement eleOutPromiseLine : outPromiseLineNodeList) {
	    String sOutItemID = eleOutPromiseLine.getAttribute(GCConstants.ITEM_ID);
	    if (YFCCommon.equals(sOutItemID, sTagItemID, false)) {
	      YFCElement eleOutBundlePromiseLine = eleOutPromiseLines.createChild(GCConstants.PROMISE_LINE);
	      GCXMLUtil.copyAttributes(eleOutPromiseLine, eleOutBundlePromiseLine);
	      eleOutBundlePromiseLine.setAttribute(GCConstants.ITEM_ID, sBundleItemID);
	      eleOutBundlePromiseLine.setAttribute(GCConstants.IS_BUNDLE_PARENT, GCConstants.YES);
	      eleOutBundlePromiseLine.setAttribute(GCConstants.KIT_CODE,GCConstants.BUNDLE);
	      eleOutBundlePromiseLine.setAttribute(GCConstants.LINE_ID,
	          sLineId + "_Bun" + Integer.toString(lineIdEnd));
	      lineIdEnd++;
	      YFCElement eleOutBundleAssignments = eleOutBundlePromiseLine.createChild(GCXmlLiterals.ASSIGNMENTS);
	      YFCElement eleOutAssignments = eleOutPromiseLine.getChildElement(GCXmlLiterals.ASSIGNMENTS);
	      GCXMLUtil.copyAttributes(eleOutAssignments, eleOutBundleAssignments);
	      YFCElement eleOutBundleAssignment = eleOutBundleAssignments.createChild(GCXmlLiterals.ASSIGNMENT);
	      YFCElement eleOutAssignment = eleOutAssignments.getChildElement(GCXmlLiterals.ASSIGNMENT);
	      GCXMLUtil.copyAttributes(eleOutAssignment, eleOutBundleAssignment);
	      YFCElement eleOutBundleSupplies = eleOutAssignment.createChild(GCConstants.SUPPLIES);
	      eleOutBundleAssignment.appendChild(eleOutBundleSupplies);
	      break;
	    }
	  }
	return lineIdEnd;
}

}


/**
 * This class is written to fetch the ItemList output for each item in the PromiseLine.
 */

class FindInventoryPromiseLineInfo {
  YFCDocument getItemListOp;

  public YFCDocument getGetItemListOp() {
    return getItemListOp;
  }

  public void setGetItemListOp(YFCDocument getItemListOp) {
    this.getItemListOp = getItemListOp;
  }
}