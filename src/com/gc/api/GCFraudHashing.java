/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class is called before Fraud request is sent and it generate the hashCode for CC No, PLC No, GC No
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               19/05/2014        Soni, Karan		      Iniatial Version
             1.1               10/06/2017        Arora, Shamim		      GCSTORE-7044 -Added the logic for adding LastOrderDate in Fraud Request
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCPaymentSecurity;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author karan
 */
public class GCFraudHashing {
    // initialize logger
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCFraudHashing.class);
    
	Document inDocUpdate = null;
	
    public Document hashCardNo(YFSEnvironment env, Document inDoc)
	    throws GCException {
	try {
	    LOGGER.verbose("Entering the hashCardNo method");
	    NodeList nlPaymentMethod = GCXMLUtil.getNodeListByXpath(inDoc,
		    "OrderList/Order/PaymentMethods/PaymentMethod");
	    for (int i = 0; i < nlPaymentMethod.getLength(); i++) {
		LOGGER.verbose("Entering the for loop");
		Element elePaymentMethod = (Element) nlPaymentMethod.item(i);
		String strPaymentType = elePaymentMethod
			.getAttribute(GCConstants.PAYMENT_TYPE);
		LOGGER.debug("The payment type is" + strPaymentType);
		if (GCConstants.CREDIT_CARD.equals(strPaymentType)
			|| GCConstants.PRIVATE_LABEL_CARD
				.equals(strPaymentType) || GCConstants.PVT_LBL_CREDIT_CARD.equalsIgnoreCase(strPaymentType)) {
		    LOGGER.debug("Entering the if Credit Card, PLC");
		    String strCardNo = elePaymentMethod
			    .getAttribute(GCConstants.CREDIT_CARD_NO);
		    String strDecryptedCardNo = GCPaymentSecurity
			    .decryptCreditCard(strCardNo, "OMS");
		    String strHashedCardNo = GCPaymentSecurity
			    .ccHashing(strDecryptedCardNo);
		    elePaymentMethod.setAttribute(GCConstants.CREDIT_CARD_NO,
			    strHashedCardNo);
		    LOGGER.debug("Exiting the if Credit Card, PLC");
		}
		if (GCConstants.GIFT_CARD.equals(strPaymentType)) {
		    LOGGER.debug("Entering the if Gift Card");
		    String strGiftCardNo = elePaymentMethod
			    .getAttribute(GCConstants.SVC_NO);
		    String strDecryptedCardNo = GCPaymentSecurity
			    .decryptCreditCard(strGiftCardNo, "OMS");
		    String strHashedCardNo = GCPaymentSecurity
			    .ccHashing(strDecryptedCardNo);
		    elePaymentMethod.setAttribute(GCConstants.SVC_NO,
			    strHashedCardNo);
		    LOGGER.debug("Exiting the if Gift Card");
		}
		if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("The final doc after update is"
			+ GCXMLUtil.getXMLString(inDoc));
		}
		LOGGER.verbose("Eiting the for loop");
	    }
	    LOGGER.verbose("Exiting the hashCardNo method");
	    
	      inDocUpdate = getCustomerLastOrderDate(env, inDoc);
			if (LOGGER.isDebugEnabled()) {
				
				LOGGER.debug("The final doc returned from GCFraudHashing is"
					+ GCXMLUtil.getXMLString(inDocUpdate));
				}	    
	    
	    
	} catch (Exception e) {
	    LOGGER.error("Inside GCFraudHashing.hashCardNo() :",e);
	    throw new GCException(e);
	}
	return inDocUpdate;
    }
	
    private Document getCustomerLastOrderDate(YFSEnvironment env, Document inDoc) throws Exception {
    	

        LOGGER.debug("Entering the Customer Last Date method of Fraud Hashing Class : Incoming Document:"+GCXMLUtil.getXMLString(inDoc));
        Element elemOrder = GCXMLUtil.getElementByXPath(inDoc,
                "OrderList/Order");
        Element eleReferences = GCXMLUtil.getElementByXPath(inDoc,
                "OrderList/Order/References");
        NodeList nlReferenceList = GCXMLUtil.getNodeListByXpath(inDoc,
    		    "OrderList/Order/References/Reference");
    	    for (int i = 0; i < nlReferenceList.getLength(); i++) {
    		LOGGER.verbose("Entering the for loop for Reference Value");
    		Element eleReference = (Element) nlReferenceList.item(i);
    		String strReferenceName = eleReference
    			.getAttribute("Name");
    
	    	if ((GCConstants.IS_GUEST_CHECK_OUT).equals(strReferenceName)) {
	    		
	    		String strReferenceValue = eleReference
	        			.getAttribute("Value");
	    		elemOrder.setAttribute(strReferenceName, strReferenceValue);
	    		elemOrder.removeChild(eleReferences);
	    		
	    		}
    	    }
    	    
    	Element eleOrderExtn = GCXMLUtil.getElementByXPath(inDoc,
                    "OrderList/Order/Extn");
    	String strOrderCaptureChannel = eleOrderExtn.getAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL);
    	if (!YFCCommon.equalsIgnoreCase(strOrderCaptureChannel, "Web")) {
    		LOGGER.verbose("Entering if Condition for Order Capture Channel Other than Web");
    		elemOrder.removeAttribute(GCConstants.IS_GUEST_CHECK_OUT);
    	}
 
        String lastOrdDate = "";
    	Calendar cOrderDate = Calendar.getInstance();
        Calendar clastOrdDate = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(
                GCConstants.DATE_TIME_FORMAT);
        
        //Fetch Organization Code & Customer ID from input document
        String strOrderNo = elemOrder.getAttribute(GCConstants.ORDER_NO);
        String strCustomerEmailID = elemOrder.getAttribute(GCConstants.CUSTOMER_EMAIL_ID);
        String strBillToID = elemOrder.getAttribute(GCConstants.BILL_TO_ID);
        String strOrderDate = elemOrder.getAttribute(GCConstants.ORDER_DATE);
        
        Date orderDate = sdf.parse(strOrderDate);
            cOrderDate.setTime(orderDate);
        
		YFCDocument inDoChangeOrder = YFCDocument
				.createDocument(GCConstants.ORDER);
		YFCElement eleOrder = inDoChangeOrder
				.getDocumentElement();
		eleOrder.setAttribute(GCConstants.ORDER_NO,strOrderNo);
		eleOrder.setAttribute(GCConstants.CUSTOMER_EMAIL_ID,strCustomerEmailID);
	    eleOrder.setAttribute(GCConstants.BILL_TO_ID,strBillToID);
	    eleOrder.setAttribute(GCConstants.DRAFT_ORDER_FLAG,"N");
	    eleOrder.setAttribute("OrderNoQryType","NE");
	    
		YFCElement eleOrderOnHold = eleOrder
				.createChild(GCConstants.ORDER_ON_HOLD);
		YFCElement eleOLComplexQuery = eleOrderOnHold
				.createChild(GCConstants.COMPLEX_QUERY);
		eleOLComplexQuery.setAttribute(GCConstants.OPERATOR,GCConstants.AND);
		YFCElement eleComplexQueryAnd = eleOLComplexQuery
				.createChild(GCConstants.AND);
		YFCElement eleCQOr = eleComplexQueryAnd
				.createChild(GCConstants.OR);
		YFCElement eleExp = eleCQOr
				.createChild(GCConstants.EXP);
		eleExp.setAttribute(GCConstants.NAME,GCConstants.STATUS);
		eleExp.setAttribute(GCConstants.VALUE,"1100");
		eleExp.setAttribute("StatusQryType","EQ");
		YFCElement eleExp1 = eleCQOr
				.createChild(GCConstants.EXP);
		eleExp1.setAttribute(GCConstants.NAME,GCConstants.STATUS);
		eleExp1.setAttribute(GCConstants.VALUE,"1300");
		eleExp1.setAttribute("StatusQryType","EQ");
		
		YFCElement eleCQOr1 = eleComplexQueryAnd
				.createChild(GCConstants.OR);
		YFCElement eleExp2 = eleCQOr1
				.createChild(GCConstants.EXP);
		eleExp2.setAttribute(GCConstants.NAME,GCConstants.HOLD_TYPE);
		eleExp2.setAttribute(GCConstants.VALUE,GCConstants.FRAUD_HOLD);
		eleExp2.setAttribute("StatusQryType","EQ");
		YFCElement eleExp3 = eleCQOr1
				.createChild(GCConstants.EXP);
		eleExp3.setAttribute(GCConstants.NAME,GCConstants.HOLD_TYPE);
		eleExp3.setAttribute(GCConstants.VALUE,GCConstants.FRAUD_REVIEW);
		eleExp3.setAttribute("StatusQryType","EQ");
		
		YFCElement eleOrderBy = eleOrder
				.createChild(GCConstants.ORDER_BY);
		YFCElement eleAttribute = eleOrderBy
				.createChild(GCConstants.ATTRIBUTE);
		
		eleAttribute.setAttribute(GCConstants.DESC,GCConstants.YES);
		eleAttribute.setAttribute(GCConstants.NAME,GCConstants.ORDER_DATE);
		
		  if (LOGGER.isDebugEnabled()) {
              LOGGER.debug("Input for getOrderList API call with Complex Query"
                  + GCXMLUtil.getXMLString(inDoChangeOrder.getDocument()));
            }

	    YFCDocument docGtOrderListTmp = YFCDocument
		.createDocument(GCConstants.ORDER_LIST);
	    YFCElement eleGtOrderList = docGtOrderListTmp
		.getDocumentElement();
	    eleGtOrderList.setAttribute(GCConstants.TOTAL_ORDER_LIST,"");
	    YFCElement eleOrderListOut = eleGtOrderList
	    		.createChild(GCConstants.ORDER);
	    eleOrderListOut.setAttribute(GCConstants.ORDER_DATE,"");
	    eleOrderListOut.setAttribute(GCConstants.ORDER_NO,"");

            if (LOGGER.isDebugEnabled()) {
              LOGGER.debug("template for getOrderList API call"
                  + GCXMLUtil.getXMLString(docGtOrderListTmp.getDocument()));
            }
            
            if(!(YFCCommon.isVoid(strBillToID) && YFCCommon.isVoid(strCustomerEmailID))){
            	
            	// Calling getOrderList API.
                YFCDocument docGtOrderListOutput = GCCommonUtil.invokeAPI(env,
                        GCConstants.API_GET_ORDER_LIST, inDoChangeOrder,
                        docGtOrderListTmp);
                    if (LOGGER.isDebugEnabled()) {
                      LOGGER.debug("Output for getItemList API call"
                          + GCXMLUtil.getXMLString(docGtOrderListOutput.getDocument()));
                    }
                       
            		if (null != docGtOrderListOutput) {
                        
                       YFCElement eleLastOrder= docGtOrderListOutput.getDocumentElement();        
            	       YFCNodeList<YFCElement> nlOrderLine = eleLastOrder.getElementsByTagName(GCConstants.ORDER);
            	       int nlLen = nlOrderLine.getLength();
            	       
            	       for(int ordLine=0;ordLine<nlLen;ordLine++){
            	        	
            	       YFCElement eleOrderLine = nlOrderLine.item(ordLine);
            	       String strOrderDateValue = eleOrderLine.getAttribute("OrderDate");
            	       Date orderDateValue = sdf.parse(strOrderDateValue);
            	       clastOrdDate.setTime(orderDateValue);
            	       
            	       if (cOrderDate.after(clastOrdDate)){		
            	        lastOrdDate = strOrderDateValue;
            	        	break;	
            	        	}
            	        }               
            	     LOGGER.debug("Last Order Date: "+ lastOrdDate );
                     elemOrder.setAttribute("LastOrderDate",lastOrdDate);
                    } 	 
            }
               
    		
    		if (LOGGER.isDebugEnabled()) {
	              LOGGER.debug("Document returned is"
	                  + GCXMLUtil.getXMLString(inDoc));
	            }
                    
    		return inDoc;
    	
    }

}
