/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class checks whether the Fraud hold needs to be applied or not on order
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0             21/05/2014          Soni, Karan		          Initial Version
             1.1             05/02/2015          Soni, Karan                  OMS- 4746: Fraud Hold gets applied for orders with pre-paid payment method and
                                                                              Payment Hold gets applied for orders with non-prepaid payment methods
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.util.YFCUtils;
import com.yantra.ycp.japi.YCPDynamicConditionEx;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author karan
 */
public class GCFraudHoldCondition implements YCPDynamicConditionEx {

	Document docResolveExceptionIp =null;

	  /*
	   * Class name
	   */
	  private static final String CLASS_NAME = GCFraudHoldCondition.class.getName();

	  // initiaize Logger
	  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(CLASS_NAME);


	@Override
	public boolean evaluateCondition(YFSEnvironment env, String strInput,
			Map mpInput, Document inDoc) {
		boolean returnFlag = false;
		try {
			if (YFCCommon.isVoid(env.getTxnObject("IsHoldResolve")) && YFCCommon.isVoid(env.getTxnObject("IsResetFraudAttribute"))) {
				LOGGER.debug("Entering the evaluateCondition method of Fraud Condition class");
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug(" evaluateCondition() method, Input document is"
							+ GCXMLUtil.getXMLString(inDoc));
				}
				Element eleRoot = inDoc.getDocumentElement();
				String strSellerOrg = eleRoot
						.getAttribute(GCConstants.SELLER_ORGANIZATION_CODE);
				String strOrderPurpose = eleRoot
						.getAttribute(GCConstants.ORDER_PURPOSE);
				String sExtnSentForFraudCheck = GCXMLUtil
						.getAttributeFromXPath(eleRoot,
								"Extn/@ExtnSentForFraudCheck");
				LOGGER.debug("The Seller Organization is" + strSellerOrg
						+ "The Order Purpose is" + strOrderPurpose
						+ "sExtnSentForFraudCheck is" + sExtnSentForFraudCheck);
				String sIspaymentChangeDueToRefund = GCXMLUtil.getAttributeFromXPath(eleRoot, "Extn/@ExtnIsRefPaymentChng");

				boolean bIsFraudHoldReqd = false;
				Document docGetPaymentTypeListIP = GCXMLUtil
						.createDocument(GCConstants.PAYMENT_TYPE);
				Element eleGetPaymentTypeList = docGetPaymentTypeListIP
						.getDocumentElement();
				eleGetPaymentTypeList.setAttribute(
						GCConstants.ORGANIZATION_CODE,
						eleRoot.getAttribute(GCConstants.ENTERPRISE_CODE));
				eleGetPaymentTypeList.setAttribute(
						GCConstants.NEW_REFUND_PAYMENT_TYPE,
						GCConstants.CREDIT_ON_ACCOUNT);
				eleGetPaymentTypeList.setAttribute(
						GCConstants.PAYMENT_TYPE_GROUP, GCConstants.OTHER);
				Document docGetPaymentTypeListOP = GCCommonUtil.invokeAPI(env,
						GCConstants.API_GET_PAYMENT_TYPE_LIST,
						docGetPaymentTypeListIP);
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug(" evaluateCondition() method, docGetPaymentTypeListOP document is"
							+ GCXMLUtil.getXMLString(docGetPaymentTypeListOP));
				}
				NodeList nlPaymentMethod = XPathAPI.selectNodeList(inDoc,
						"Order/PaymentMethods/PaymentMethod");
				for (int iCounter = 0; iCounter < nlPaymentMethod.getLength(); iCounter++) {
					Element elePaymentMethod = (Element) nlPaymentMethod
							.item(iCounter);
					String sPaymentType = elePaymentMethod
							.getAttribute(GCConstants.PAYMENT_TYPE);
					Element elePaymentType = GCXMLUtil.getElementByXPath(
							docGetPaymentTypeListOP,
							"PaymentTypeList/PaymentType[@PaymentType='"
									+ sPaymentType + "']");
					if (YFCCommon.isVoid(elePaymentType)
							&& !GCConstants.CREDIT_ON_ACCOUNT
									.equals(sPaymentType)
							&& !GCConstants.GIFT_CARD.equals(sPaymentType)
							&& !GCConstants.STORE_CREDIT_PAYMENT_TYPE.equals(sPaymentType)
							&& GCConstants.NO.equals(sExtnSentForFraudCheck)) {
						LOGGER.debug(" Inside If !COA and !GC and !SentForFraudCheck");
						String strMaxChargeLimit = elePaymentMethod
								.getAttribute(GCConstants.MAX_CHARGE_LIMIT);
						String strSuspendAnyMoreCharges = elePaymentMethod
								.getAttribute(GCConstants.SUSPEND_ANY_MORE_CHARGES);
						if (strSuspendAnyMoreCharges.equals(GCConstants.NO)
								&& Double.valueOf(strMaxChargeLimit) > 0.0) {
							bIsFraudHoldReqd = true;
							break;
						}
					}
				}

				// Fix to avoid Fraud Hold on mPOS Orders
                Element eleInputExtn = (Element) eleRoot.getElementsByTagName(GCConstants.EXTN).item(0);
                String sExtnOrderCaptureChannel = eleInputExtn.getAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL);
                
                if(sExtnOrderCaptureChannel.equalsIgnoreCase(GCConstants.MPOS_ORDER_CHANNEL)){
                  return false;
                }
				if (!GCConstants.INTL_WEB.equals(strSellerOrg)
						&& YFCCommon.isVoid(strOrderPurpose)
						&& bIsFraudHoldReqd && !YFCUtils.equals(GCConstants.YES, sIspaymentChangeDueToRefund)) {
					returnFlag = true;
				} else {
					returnFlag = false;
				}
				if (YFCUtils.equals(GCConstants.YES, sIspaymentChangeDueToRefund)) {
          Document changeOrderInDoc = GCXMLUtil.createDocument("Order");
          Element eleRootChangeOrder = changeOrderInDoc.getDocumentElement();
          eleRootChangeOrder.setAttribute(GCConstants.ORDER_HEADER_KEY,
              eleRoot.getAttribute(GCConstants.ORDER_HEADER_KEY));
          eleRootChangeOrder.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
          Element eleExtn = changeOrderInDoc.createElement(GCConstants.EXTN);
          eleExtn.setAttribute(GCConstants.EXTN_IS_REF_PAYMENT_CHNG, GCConstants.NO);
          eleRootChangeOrder.appendChild(eleExtn);
          env.setTxnObject("IsResetFraudAttribute", "Y");
          GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, changeOrderInDoc);
        }
				// OMS-4746 Start
				String strOHK = eleRoot
						.getAttribute(GCConstants.ORDER_HEADER_KEY);
				Document inDocGOL = GCXMLUtil
						.getDocument("<Order OrderHeaderKey='" + strOHK + "'/>");
				Document inDocTemplate = GCXMLUtil
						.getDocument("<OrderList><Order><OrderHoldTypes><OrderHoldType/></OrderHoldTypes><PaymentMethods><PaymentMethod/></PaymentMethods></Order></OrderList>");
				Document outDocGOL = GCCommonUtil.invokeAPI(env,
						GCConstants.GET_ORDER_LIST, inDocGOL, inDocTemplate);
				LOGGER.debug("The Order Header Key is" + strOHK
						+ "The output of getOrderList is"
						+ GCXMLUtil.getXMLString(outDocGOL));
				NodeList nlHoldType = GCXMLUtil.getNodeListByXpath(outDocGOL,
						"OrderList/Order/OrderHoldTypes/OrderHoldType");
				List<String> listPrepaidPaymentTypes = Arrays.asList(
						"WIRE_TRANSFER", "CHECK", "CASH", "MONEY_ORDER",
						"WESTERN_UNION");

				List<String> listCreditCardPaymentTypes = Arrays.asList(
						"CREDIT_CARD", "PRIVATE_LABEL_CARD");
				Document inDocCO = GCXMLUtil
						.getDocument("<Order OrderHeaderKey='" + strOHK
								+ "' Override='Y' ><OrderHoldTypes/></Order>");
				boolean isActivePrepaid = false;

				for (int i = 0; i < nlHoldType.getLength(); i++) {
					Element eleHoldType = (Element) nlHoldType.item(i);
					String strHoldType = eleHoldType
							.getAttribute(GCConstants.HOLD_TYPE);
					LOGGER.debug("The hold type is =====" + strHoldType);
					if (GCConstants.FRAUD_HOLD.equals(strHoldType)
							&& !returnFlag) {

						LOGGER.debug("Entering the Fraud hold if");

						boolean isActiveCreditCard = false;
						for (int iCounter = 0; iCounter < nlPaymentMethod
								.getLength(); iCounter++) {
							Element elePaymentMethod = (Element) nlPaymentMethod
									.item(iCounter);
							String strPaymentType = elePaymentMethod
									.getAttribute(GCConstants.PAYMENT_TYPE);
							String sSuspendAnyMoreCharges = elePaymentMethod
									.getAttribute(GCConstants.SUSPEND_ANY_MORE_CHARGES);
							if (listCreditCardPaymentTypes
									.contains(strPaymentType)
									&& GCConstants.NO
											.equalsIgnoreCase(sSuspendAnyMoreCharges) || GCConstants.NO.equalsIgnoreCase(env.getTxnObject("ResolveHold").toString())) {
								isActiveCreditCard = true;
								break;
							}
							LOGGER.debug("The bolean value returned by Fraud Hold is"
									+ isActiveCreditCard);
						}

						if (!isActiveCreditCard) {

							Element eleOrderHoldType = inDocCO
									.createElement(GCConstants.ORDER_HOLD_TYPE);
							Element eleOrderHoldTypes = GCXMLUtil
									.getElementByXPath(inDocCO,
											"Order/OrderHoldTypes");
							eleOrderHoldTypes.appendChild(eleOrderHoldType);
							eleOrderHoldType.setAttribute(
									GCConstants.HOLD_TYPE,
									GCConstants.FRAUD_HOLD);
							eleOrderHoldType.setAttribute(GCConstants.STATUS,
									GCConstants.RESOLVE_HOLD_CODE);
							LOGGER.debug("Exiting the Fraud hold if");
						}
					}

					// Fix for 4191 : Removed Code to resolve hold automatically.
					/*if (GCConstants.PAYMENT_HOLD.equals(strHoldType)) {
						LOGGER.debug("Entering the Payment hold if");

						for (int iCounter = 0; iCounter < nlPaymentMethod
								.getLength(); iCounter++) {
							Element elePaymentMethod = (Element) nlPaymentMethod
									.item(iCounter);
							String strPaymentType = elePaymentMethod
									.getAttribute(GCConstants.PAYMENT_TYPE);
							String sSuspendAnyMoreCharges = elePaymentMethod
									.getAttribute(GCConstants.SUSPEND_ANY_MORE_CHARGES);
							if (listPrepaidPaymentTypes
									.contains(strPaymentType)
									&& GCConstants.NO
											.equalsIgnoreCase(sSuspendAnyMoreCharges)) {
								isActivePrepaid = true;
								break;
							}
							LOGGER.debug("The bolean value returned by Payment Hold is"
									+ isActivePrepaid);
						}
						if (!isActivePrepaid) {
							Element eleOrderHoldType = inDocCO
									.createElement(GCConstants.ORDER_HOLD_TYPE);
							Element eleOrderHoldTypes = GCXMLUtil
									.getElementByXPath(inDocCO,
											"Order/OrderHoldTypes");
							eleOrderHoldTypes.appendChild(eleOrderHoldType);
							eleOrderHoldType.setAttribute(
									GCConstants.HOLD_TYPE,
									GCConstants.PAYMENT_HOLD);
							eleOrderHoldType.setAttribute(GCConstants.STATUS,
									GCConstants.RESOLVE_HOLD_CODE);
							resolvePaymentHoldAlert(env, strOHK);
						}
						LOGGER.debug("Exiting the Fraud hold if");
					}*/
				}
				LOGGER.debug("The change order document is "
						+ GCXMLUtil.getXMLString(inDocCO));
				Element eleChangeOrder = GCXMLUtil.getElementByXPath(inDocCO,
						"Order/OrderHoldTypes/OrderHoldType");

				if (!YFCCommon.isVoid(eleChangeOrder)) {
					env.setTxnObject("IsHoldResolve", "Y");
					GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER,
							inDocCO);
				}
				// OMS-4746 End
				LOGGER.debug("The final returned flag is" + returnFlag);
				LOGGER.verbose("Exiting the evaluateCondition method of Fraud Condition class");
			}
		} catch (Exception e) {
			LOGGER.error("Inside GCFraudHoldCondition.evaluateCondition(): ", e);
		}
		return returnFlag;
	}
	
	
	/**
	 * This method resolve payment hold alert raised by prepaid card
	 *
	 * @param env
	 * @param strOHK
	 */
	private void resolvePaymentHoldAlert(YFSEnvironment env, String strOHK) {

		// Fetch Inbox key from getExceptionList Op
		Document docGetExceptionListIp = GCXMLUtil
				.createDocument(GCConstants.INBOX);
		Element eleGetExceptionListIpRoot = docGetExceptionListIp
				.getDocumentElement();
		eleGetExceptionListIpRoot.setAttribute(GCConstants.ORDER_HEADER_KEY,
				strOHK);
		eleGetExceptionListIpRoot.setAttribute(GCConstants.EXCEPTION_TYPE,
				GCConstants.GC_PAYMENT_HOLD_NOTIFICATION);

		Document docGetExceptionListOp = GCCommonUtil
				.invokeAPI(
						env,
						GCConstants.API_GET_EXCEPTION_LIST,
						docGetExceptionListIp,
						GCCommonUtil
								.getDocumentFromPath("/global/template/api/getExceptionList.xml"));

		String sInboxKey = GCXMLUtil
				.getAttributeFromXPath(docGetExceptionListOp,
						"InboxList/Inbox[@ExceptionType='GCPaymentHoldNotification']/@InboxKey");

		// Use Inbox key to resolve payment hold alert
		docResolveExceptionIp = GCXMLUtil
				.createDocument(GCConstants.RESOLUTION_DETAILS);
		Element eleResolveExceptionIpRoot = docResolveExceptionIp
				.getDocumentElement();
		Element eleInbox = docResolveExceptionIp
				.createElement(GCConstants.INBOX);
		eleResolveExceptionIpRoot.appendChild(eleInbox);
		eleInbox.setAttribute(GCConstants.INBOX_KEY, sInboxKey);

		GCCommonUtil.invokeAPI(env, GCConstants.API_RESOLVE_EXCEPTION,
				docResolveExceptionIp);
	}

	@Override
	public void setProperties(Map arg0) {
	}

}
