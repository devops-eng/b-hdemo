package com.gc.api;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.gc.mpos.GCAddShippingChargesDetail;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCFraudHoldForUnevenExc {
	  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCFraudHoldForUnevenExc.class.getName());
	  
	  public void applyConditionalFraudHold (YFSEnvironment env, Document doc){
		  LOGGER.beginTimer("GCPushMPOSOrderToPOS.pushToPOS()");
		  LOGGER.verbose("GCPushMPOSOrderToPOS - pushToPOS() : Start" + GCXMLUtil.getXMLString(doc));
		  
		  YFCDocument inDoc = YFCDocument.getDocumentFor(doc);
		  YFCElement eleRoot = inDoc.getDocumentElement();
		  
		  String sOrderHeaderKey = eleRoot.getAttribute(GCConstants.ORDER_HEADER_KEY);
		  
		  YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "'/>");
		  YFCDocument getOrderListTemplate = YFCDocument.getDocumentFor("<OrderList><Order OrderNo='' OrderPurpose='' ExchangeType='' ><PaymentMethods><PaymentMethod PaymentType=''/>"
		  		+ "</PaymentMethods></Order></OrderList>");
		  
		  YFCDocument getOrderList = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListInput, getOrderListTemplate);
		  
		  YFCElement eleOrder = getOrderList.getDocumentElement().getElementsByTagName(GCConstants.ORDER).item(0);
		  String sOrderPurpose = eleOrder.getAttribute(GCConstants.ORDER_PURPOSE);
		  String sExchangeType = eleOrder.getAttribute(GCConstants.EXCHANGE_TYPE);
		  
		  if(YFCCommon.equalsIgnoreCase(sOrderPurpose, "EXCHANGE")){
			  YFCNodeList<YFCElement> nlPaymentMethod = getOrderList.getElementsByTagName(GCConstants.PAYMENT_METHOD);
			  
			  boolean isFraudHoldRequired = false;
			  String sPaymentMethod = "";
			  for(int i=0; i<nlPaymentMethod.getLength(); i++){
				  YFCElement elePaymentMethod = nlPaymentMethod.item(i);
				  sPaymentMethod = elePaymentMethod.getAttribute(GCConstants.PAYMENT_TYPE);
				  if(YFCCommon.equalsIgnoreCase(sPaymentMethod, "CREDIT_CARD") || YFCCommon.equalsIgnoreCase(sPaymentMethod, "PVT_LBL_CREDIT_CARD")){
					  isFraudHoldRequired = true;
					  break;
				  }
			  }
			  
			  if(isFraudHoldRequired){
				  YFCDocument changeOrderInput = YFCDocument.getDocumentFor("<Order/>");
				  YFCElement eleChangeOrderRoot = changeOrderInput.getDocumentElement();
				  eleChangeOrderRoot.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
				  
				  YFCElement eleOrderHoldTypes = changeOrderInput.createElement(GCConstants.ORDER_HOLD_TYPES);
				  YFCElement eleOrderholdType = changeOrderInput.createElement(GCConstants.ORDER_HOLD_TYPE);
				  eleOrderholdType.setAttribute(GCConstants.HOLD_TYPE, GCConstants.FRAUD_HOLD);
				  eleOrderholdType.setAttribute(GCConstants.REASON_TEXT, GCConstants.FRAUD_PENDING_REVIEW);
				  eleOrderholdType.setAttribute(GCConstants.STATUS, GCConstants.APPLY_HOLD_CODE);
				  eleOrderHoldTypes.appendChild(eleOrderholdType);
				  eleChangeOrderRoot.appendChild(eleOrderHoldTypes);
						
				   LOGGER.debug("applyTaxExemptHold() - Input Document before change Order" + GCXMLUtil.getXMLString(changeOrderInput.getDocument()));
				   
				   GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, changeOrderInput.getDocument());				  
			  }				
		  }
		  		  
	  }
}