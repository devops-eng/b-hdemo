/**Copyright 2013 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.

    Change Log
######################################################################################################################################
     OBJECTIVE: Foundation Class
######################################################################################################################################
        Version    CR                 Date                       Modified By                   Description
######################################################################################################################################
        1.0    Initial Version      19/03/2015                Zuzar Inder Singh          GCSTORE-791 - ATGPEN shipping charge Service
                                                     This Wrapper class takes the inDoc and invokes the GCGetATGPromoService for change
                                                      item/ item quantity or Address change just before fulfillmentSummary screen.
######################################################################################################################################
 */
package com.gc.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCGetATGPromoWrapper implements YIFCustomApi {
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCGetATGPromoWrapper.class);

	@Override
	public void setProperties(Properties prop) {

	}

	/**
	 * this method invokes the ATGPromo service for fetching the shipping charge
	 * and returning it to the fulfillment summary screen.
	 *
	 * @param env
	 * @param doc
	 * @return
	 */
	public Document invokeGetATGPromoService(YFSEnvironment env, Document doc) {
		LOGGER.beginTimer("GCGetATGPromoWrapper.invokeGetATGPromoService");

		YFCDocument inDoc = YFCDocument.getDocumentFor(doc);
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Input Document invokeGetATGPromoWrapper: "
					+ inDoc.toString());
		}
		YFCElement eleRoot = inDoc.getDocumentElement();
		String sIsCancelOrderFlow = eleRoot.getAttribute("IsCancelOrderFlow");

		eleRoot.setAttribute("CallATGForInit", "Y");

		YFCDocument outDocATGPromo = GCCommonUtil.invokeService(env,
				"GCGetATGPromoService", inDoc);
		
		//GCSTORE4091
		String sOHK = eleRoot.getAttribute("OrderHeaderKey");
		Document tempDoc = GCCommonUtil.getDocumentFromPath("/global/template/api/getOrderList.xml");
		Document outGOLDoc = GCCommonUtil.invokeGetOrderListAPI(env, sOHK, tempDoc);
		YFCDocument yfcGOLDoc = YFCDocument.getDocumentFor(outGOLDoc);
		YFCElement eleExtn = yfcGOLDoc.getDocumentElement().getChildElement("Order").getChildElement("Extn");
		String sExtnStampUnit = eleExtn.getAttribute("ExtnStampUnitCost");
		if(YFCCommon.equals(sExtnStampUnit, "Y")){
			YFCDocument changeOrderDoc = YFCDocument.createDocument("Order");
			YFCElement eleOrderElement = changeOrderDoc.getDocumentElement();
			eleOrderElement.setAttribute("OrderHeaderKey", sOHK);
			eleOrderElement.setAttribute("StampUnitCost", "Y");
			YFCElement eleChangeOrderExtn = eleOrderElement.getChildElement("Extn",true);
			eleChangeOrderExtn.setAttribute("ExtnStampUnitCost", "N");
			
			GCCommonUtil.invokeAPI(env, "changeOrder", changeOrderDoc.getDocument());
		}
		//GCSTORE4091
		YFCElement eleDocumentRoot = outDocATGPromo.getDocumentElement();
		YFCElement eleError = eleDocumentRoot.getChildElement("Error");
		String sEleErrorTag = "";
		if (!YFCCommon.isVoid(eleError)) {
			sEleErrorTag = eleError.getTagName();
		}
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Output Document GCGetATGPromoService: "
					+ outDocATGPromo.toString());
		}

		if (YFCCommon.equals(sIsCancelOrderFlow, GCConstants.FLAG_Y)) {
			eleRoot.removeAttribute("IsCancelOrderFlow");
			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("Return Document invokeGetATGPromoWrapper: "
						+ inDoc.toString());
			}
			LOGGER.endTimer("Exiting invokeGetATGPromoWrapper");
			return inDoc.getDocument();
		}

		Document outDoc = getFilteredLinesToDisplayOnScreen(env,
				inDoc.getDocument());
		YFCDocument docToScreen = YFCDocument.getDocumentFor(outDoc);
		if (YFCCommon.equals(sEleErrorTag, "Error")) {

			YFCElement errorEle = docToScreen.getDocumentElement().createChild(
					sEleErrorTag);
			errorEle.setAttribute("ErrorCode",
					eleError.getAttribute("ErrorCode"));
			errorEle.setAttribute("ErrorDescription",
					eleError.getAttribute("ErrorDescription"));
		}
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Output Document getFulfillmentSummaryDetails: "
					+ docToScreen.toString());
		}

		LOGGER.endTimer("Exiting invokeGetATGPromoWrapper");
		return docToScreen.getDocument();
	}

  public Document getFilteredLinesToDisplayOnScreen(YFSEnvironment env, Document inDoc) {

    LOGGER.verbose("Input Document getFilteredLinesToDisplayOnScreen: " + GCXMLUtil.getXMLString(inDoc));

    // GCSTORE - 3715, 5360
    String sOrderHeaderKey = inDoc.getDocumentElement().getAttribute(GCConstants.ORDER_HEADER_KEY);
    Document getOrderListInDoc = GCXMLUtil.getDocument("<Order OrderHeaderKey='" + sOrderHeaderKey + "'/>");
    Document tempDoc = GCCommonUtil.getDocumentFromPath("/global/template/api/getOrderList.xml");
    Document outGOLDoc = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListInDoc, tempDoc);

    LOGGER.verbose("getOrderList is " + GCXMLUtil.getXMLString(outGOLDoc));

    List<String> toUpdateSET_Parent_Y_Comp_N = new ArrayList<String>();
    List<String> toUpdateSET_Parent_N_Comp_Y = new ArrayList<String>();
    boolean isCompUnPlan_N = false;

    List<Element> listBundleParentLineDetail =
        GCXMLUtil.getElementListByXpath(outGOLDoc, "OrderList/Order/OrderLines/OrderLine[@KitCode='BUNDLE']");

    for (int n = 0; n < listBundleParentLineDetail.size(); n++) {
      Element eleOrderLine = listBundleParentLineDetail.get(n);
      Element eleItemDetails = (Element) eleOrderLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
      Element eleItemDetailsExtn = (Element) eleItemDetails.getElementsByTagName(GCConstants.EXTN).item(0);
      if (!YFCCommon.isVoid(eleItemDetailsExtn)) {
        String sExtnIsBackorderable = eleItemDetailsExtn.getAttribute("ExtnIsBackorderable");
        String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);

        if (YFCCommon.equalsIgnoreCase(sExtnIsBackorderable, "Y")) {
          List<Element> listBundleComponents = GCXMLUtil.getElementListByXpath(outGOLDoc,
              "OrderList/Order/OrderLines/OrderLine[@BundleParentOrderLineKey='" + sOrderLineKey + "']");

          for (int z = 0; z < listBundleComponents.size(); z++) {
            Element eleCompOrderLine = listBundleComponents.get(z);
            Element eleCompItemDetails =
                (Element) eleCompOrderLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
            Element eleCompItemDetailsExtn =
                (Element) eleCompItemDetails.getElementsByTagName(GCConstants.EXTN).item(0);
            if (!YFCCommon.isVoid(eleCompItemDetailsExtn)) {
              String sCompExtnIsBackorderable = eleCompItemDetailsExtn.getAttribute("ExtnIsBackorderable");
              if (YFCCommon.equalsIgnoreCase(sCompExtnIsBackorderable, "N")) {
                LOGGER.verbose("Comp with UseUnplannedInv found");
                isCompUnPlan_N = true;
              }
            }
          }
          if (isCompUnPlan_N) {
            toUpdateSET_Parent_Y_Comp_N.add(sOrderLineKey);
          }
        } else if (YFCCommon.equalsIgnoreCase(sExtnIsBackorderable, "N")) {
          List<Element> listBundleComponents = GCXMLUtil.getElementListByXpath(outGOLDoc,
              "OrderList/Order/OrderLines/OrderLine[@BundleParentOrderLineKey='" + sOrderLineKey + "']");

          for (int z = 0; z < listBundleComponents.size(); z++) {
            Element eleCompOrderLine = listBundleComponents.get(z);
            Element eleCompItemDetails =
                (Element) eleCompOrderLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
            Element eleCompItemDetailsExtn =
                (Element) eleCompItemDetails.getElementsByTagName(GCConstants.EXTN).item(0);
            if (!YFCCommon.isVoid(eleCompItemDetailsExtn)) {
              String sCompExtnIsBackorderable = eleCompItemDetailsExtn.getAttribute("ExtnIsBackorderable");
              if (YFCCommon.equalsIgnoreCase(sCompExtnIsBackorderable, "N")) {
                LOGGER.verbose("Comp with UseUnplannedInv found");
                isCompUnPlan_N = true;
          }
        }
          }
          if (!isCompUnPlan_N) {
            toUpdateSET_Parent_N_Comp_Y.add(sOrderLineKey);
          }

        }
        isCompUnPlan_N = false;
      }
    }



		String sTemplatePath = "/global/template/api/getFulfillmentSummaryDetailsForATGRequest.xml";
		Document tmplStr = GCCommonUtil.getDocumentFromPath(sTemplatePath);
		YFCDocument getFulfillmentTmpl = YFCDocument.getDocumentFor(tmplStr);
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Template Document getFulfillmentSummaryDetails: "
					+ getFulfillmentTmpl.toString());
		}
		YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
		YFCElement eleInRoot = yfcInDoc.getDocumentElement();
		eleInRoot.setAttribute("IsLargeOrder","N");
		YFCDocument docToScreen = GCCommonUtil.invokeAPI(env,
				"getFulfillmentSummaryDetails", yfcInDoc, getFulfillmentTmpl);
		YFCElement eleOrder = docToScreen.getDocumentElement();


    // Start: Unavai Lines --> Available if IsBackOrderableFlag = "Y" for Parent and "N" for Comps
    YFCElement eleShippingLines_Un = docToScreen.getElementsByTagName("ShippingLines").item(0);
    if (!YFCCommon.isVoid(eleShippingLines_Un)) {
      YFCNodeList<YFCElement> nlUnavailableOrderLines =
          eleShippingLines_Un.getElementsByTagName(GCConstants.ORDER_LINE);

      for (int n = 0; n < nlUnavailableOrderLines.getLength(); n++) {
        YFCElement eleOrderLine = nlUnavailableOrderLines.item(n);
        String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
        String sEarliestShipDate = eleOrderLine.getAttribute("EarliestShipDate");

        YFCElement eleShippingGroups_Un = null;
        if (toUpdateSET_Parent_Y_Comp_N.contains(sOrderLineKey)) {
          LOGGER.verbose("Need to update API output");

          eleShippingGroups_Un = docToScreen.getElementsByTagName("ShippingGroups").item(0);
          boolean shippingGroupsPresent = false;
          boolean sameShipDateShippingGroup = false;
          if (YFCCommon.isVoid(eleShippingGroups_Un)) {
            shippingGroupsPresent = false;
            LOGGER.verbose("ShippingGroups element not present. Making Element and adding");
            addShippingGroup(docToScreen, eleOrderLine, shippingGroupsPresent);

          } else {
            shippingGroupsPresent = true;
            YFCNodeList<YFCElement> nlShippingGroup = eleShippingGroups_Un.getElementsByTagName("ShippingGroup");
            for (int i = 0; i < nlShippingGroup.getLength(); i++) {
              YFCElement eleShippingGroup = nlShippingGroup.item(n);
              String sNode = eleShippingGroup.getAttribute(GCConstants.NODE);
              if (YFCCommon.equalsIgnoreCase(sNode, "MFI")) {
                LOGGER.verbose("Shipping Group with MFI found.");
                YFCElement eleShipGroupOrderLine =
                    eleShippingGroup.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
                String sEarlyShipDate = eleShipGroupOrderLine.getAttribute("EarliestShipDate");
                if (YFCCommon.equals(sEarlyShipDate, sEarliestShipDate)) {
                  LOGGER.verbose("Same Shipping Group found. Appending Unavailable OL to this group.");
                  sameShipDateShippingGroup = true;
            YFCElement eleOrderLines = eleShippingGroups_Un.getElementsByTagName(GCConstants.ORDER_LINES).item(0);
            YFCElement copyEleOrderLine = docToScreen.createElement(GCConstants.ORDER_LINE);
            GCXMLUtil.copyElement(docToScreen, eleOrderLine, copyEleOrderLine);
            copyEleOrderLine.setAttribute("Node", "MFI");
            copyEleOrderLine.setAttribute(GCConstants.FULFILLMENT_TYPE, "SHIP_2_CUSTOMER");
            copyEleOrderLine.setAttribute("AvailableQty", eleOrderLine.getAttribute("UnavailableQty"));
            copyEleOrderLine.removeAttribute("UnavailableQty");
            eleOrderLines.appendChild(copyEleOrderLine);
                  break;
                } else {
                  sameShipDateShippingGroup = false;
                }
              }
            }
            if (!sameShipDateShippingGroup) {
              LOGGER.verbose("No same Shipping group present. Creating new and appending.");
              addShippingGroup(docToScreen, eleOrderLine, shippingGroupsPresent);
              shippingGroupsPresent = false;
            }

          }
          eleOrderLine.getParentNode().removeChild(eleOrderLine);
          n--;
        }
      }

      YFCNodeList<YFCElement> nlRemaininOrderLines = eleShippingLines_Un.getElementsByTagName(GCConstants.ORDER_LINE);
      if (nlRemaininOrderLines.getLength() == 0) {
        eleShippingLines_Un.getParentNode().getParentNode()
            .removeChild(docToScreen.getElementsByTagName("UnavailableLines").item(0));

      }
    }
    // End: Unavai Lines --> Available if IsBackOrderableFlag = "Y" for Parent and "N" for Comps

    // Start: Avai --> UnAvai lines if IsBackOrderableFlag = "N" for Parent and "Y" for Comps
    YFCNodeList<YFCElement> nlShippingGroup_Avai = docToScreen.getElementsByTagName("ShippingGroup");
    boolean unAvailLinesPresent = false;
    if (nlShippingGroup_Avai.getLength() > 0) {

      for (int n = 0; n < nlShippingGroup_Avai.getLength(); n++) {
        YFCElement eleShippingGroup = nlShippingGroup_Avai.item(n);

        YFCNodeList<YFCElement> nlOrderLine = eleShippingGroup.getElementsByTagName(GCConstants.ORDER_LINE);
        for (int x = 0; x < nlOrderLine.getLength(); x++) {
          YFCElement eleOrderLine_Available = nlOrderLine.item(x);
          String sOrderLineKey = eleOrderLine_Available.getAttribute(GCConstants.ORDER_LINE_KEY);



          if (toUpdateSET_Parent_N_Comp_Y.contains(sOrderLineKey)) {
            LOGGER.verbose("Need to update API output");
            YFCElement eleShippingLines_Unavai = docToScreen.getElementsByTagName("ShippingLines").item(0);
            if (nlOrderLine.getLength() > 1) {
              if (!YFCCommon.isVoid(eleShippingLines_Unavai)) {
                unAvailLinesPresent = true;
              }
              LOGGER.verbose("Multi OL --> just delete OL");
              addShippingLines(docToScreen, eleOrderLine_Available, unAvailLinesPresent);
              eleOrderLine_Available.getParentNode().removeChild(eleOrderLine_Available);
              x--;
            } else {
              // One OrderLine in Shipping Group --> delete Line --> delete Shipping Group
              if (!YFCCommon.isVoid(eleShippingLines_Unavai)) {
                unAvailLinesPresent = true;
              }
              LOGGER.verbose("Single OL --> delete OL & ShippingGroup");
              addShippingLines(docToScreen, eleOrderLine_Available, unAvailLinesPresent);
              eleOrderLine_Available.getParentNode().removeChild(eleOrderLine_Available);
              x--;
              eleShippingGroup.getParentNode().removeChild(eleShippingGroup);
              n--;
            }
          }
        }
      }
      if (nlShippingGroup_Avai.getLength() == 0) {
        YFCElement eleShippingGroups = docToScreen.getDocumentElement().getElementsByTagName("ShippingGroups").item(0);
        eleShippingGroups.getParentNode().removeChild(eleShippingGroups);
      }
    }
    // End: Avai --> UnAvai lines if IsBackOrderableFlag = "N" for Parent and "Y" for Comps

    String sourcingClassification = eleOrder.getAttribute("SourcingClassification");
		if (!YFCCommon.isVoid(sourcingClassification)) {
			YFCElement eleUnAvailableLines = eleOrder
					.getChildElement("UnavailableLines");
			YFCElement eleShippingGroups = eleOrder
					.getChildElement("ShippingGroups");
			if (!YFCCommon.isVoid(eleShippingGroups)) {
				YFCNodeList<YFCElement> nlShippingGroup = eleShippingGroups
						.getElementsByTagName("ShippingGroup");
				for (YFCElement eleShippingGroup : nlShippingGroup) {
					YFCNodeList<YFCElement> nlOrderLine = eleShippingGroup
							.getElementsByTagName("OrderLine");
					YFCElement eleOrderLines = eleShippingGroup
							.getChildElement("OrderLines");
					for (YFCElement eleOrderLine : nlOrderLine) {
						String shipNode = eleOrderLine.getAttribute("ShipNode");
						if ((YFCCommon.equals(sourcingClassification,
								"DC_SOURCED") || YFCCommon.equals(
								sourcingClassification, "PREPAID"))
								&& !YFCCommon.isVoid(shipNode)) {

							YFCDocument getShipNodeDoc = YFCDocument
									.createDocument("ShipNode");
							YFCElement eleShipNode = getShipNodeDoc
									.getDocumentElement();
							eleShipNode.setAttribute("NodeOrgCode", shipNode);
							YFCDocument templateDoc = YFCDocument
									.getDocumentFor("<ShipNodeList><ShipNode NodeType='' NodeOrgCode=''/></ShipNodeList>");
							YFCDocument outDoc = GCCommonUtil.invokeAPI(env,
									"getShipNodeList", getShipNodeDoc,
									templateDoc);
							String nodeType = outDoc.getDocumentElement()
									.getChildElement("ShipNode")
									.getAttribute("NodeType");
							if (YFCCommon.equals(nodeType, "Store")) {
								eleOrderLines.removeChild(eleOrderLine);
								if (YFCCommon.isVoid(eleUnAvailableLines)) {
									eleUnAvailableLines = eleOrder
											.createChild("UnavailableLines");
								}
								YFCElement eleShippingLines = eleUnAvailableLines
										.getChildElement("ShippingLines", true);
								eleShippingLines.appendChild(eleOrderLine);
								String availableQty = eleOrderLine
										.getAttribute("AvailableQty");
								eleOrderLine.setAttribute("UnavailableQty",
										availableQty);
								eleOrderLine.removeAttribute("AvailableQty");
							}
						}
					}
					YFCNodeList<YFCElement> nlOrderLines1 = eleShippingGroup
							.getElementsByTagName("OrderLine");
					if (YFCCommon.equals(nlOrderLines1.getLength(), 0)) {
						eleShippingGroups.removeChild(eleShippingGroup);
					}
				}
				YFCNodeList<YFCElement> nlShippingGroups = eleShippingGroups
						.getElementsByTagName("ShippingGroup");
				if (YFCCommon.equals(nlShippingGroups.getLength(), 0)) {
					eleOrder.removeChild(eleShippingGroups);
				}
			}
		}
		return docToScreen.getDocument();
	}

  public void addShippingGroup(YFCDocument docToScreen, YFCElement eleOrderLine, boolean shippinGroupPresent) {
    LOGGER.verbose("addShippingGroup() : Start");
    YFCElement eleShippingGroups_Un;
    if (!shippinGroupPresent) {
      eleShippingGroups_Un = docToScreen.createElement("ShippingGroups");
      docToScreen.getDocumentElement().appendChild(eleShippingGroups_Un);
    }
    eleShippingGroups_Un = docToScreen.getElementsByTagName("ShippingGroups").item(0);
    YFCElement eleShippingGroup = docToScreen.createElement("ShippingGroup");
    eleShippingGroups_Un.appendChild(eleShippingGroup);
    eleShippingGroup.setAttribute("Node", "MFI");
    YFCElement elePersonInfoShipTo = docToScreen.getElementsByTagName(GCConstants.PERSON_INFO_SHIP_TO).item(0);
    eleShippingGroup.appendChild(elePersonInfoShipTo);
    YFCElement eleOrderLines = docToScreen.createElement(GCConstants.ORDER_LINES);
    eleShippingGroup.appendChild(eleOrderLines);

    YFCElement copyEleOrderLine = docToScreen.createElement(GCConstants.ORDER_LINE);
    GCXMLUtil.copyElement(docToScreen, eleOrderLine, copyEleOrderLine);
    copyEleOrderLine.setAttribute("Node", "MFI");

    copyEleOrderLine.setAttribute(GCConstants.FULFILLMENT_TYPE, "SHP");
    copyEleOrderLine.setAttribute("AvailableQty", eleOrderLine.getAttribute("UnavailableQty"));
    copyEleOrderLine.removeAttribute("UnavailableQty");
    eleOrderLines.appendChild(copyEleOrderLine);
  }

  public void addShippingLines(YFCDocument docToScreen, YFCElement eleOrderLine, boolean shippingLinesPresent) {
    LOGGER.verbose("addShippingGroup() : Start");
    YFCElement eleShippingLines;
    if (!shippingLinesPresent) {
      YFCElement eleUnavailableLines_Un = docToScreen.createElement("UnavailableLines");
      docToScreen.getDocumentElement().appendChild(eleUnavailableLines_Un);
      eleShippingLines = docToScreen.createElement("ShippingLines");
      eleUnavailableLines_Un.appendChild(eleShippingLines);
    }
    eleShippingLines = docToScreen.getElementsByTagName("ShippingLines").item(0);

    YFCElement copyEleOrderLine = docToScreen.createElement(GCConstants.ORDER_LINE);
    GCXMLUtil.copyElement(docToScreen, eleOrderLine, copyEleOrderLine);
    eleShippingLines.appendChild(copyEleOrderLine);
    copyEleOrderLine.removeAttribute("Node");
    copyEleOrderLine.removeAttribute(GCConstants.FULFILLMENT_TYPE);
    copyEleOrderLine.setAttribute("UnavailableQty", copyEleOrderLine.getAttribute("AvailableQty"));
    copyEleOrderLine.removeAttribute("AvailableQty");

  }

}
