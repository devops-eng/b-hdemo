package com.gc.api;

import java.text.SimpleDateFormat;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCGetAllowedShipToStore {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCGetAllowedShipToStore.class.getName());

  public Document getAllowedShipToStore(YFSEnvironment env, Document inDoc)
      throws GCException {
    try {
      final String logInfo = "GCGetAllowedShipToStore :: GetAllowedShipToStore :: ";
      LOGGER.verbose(logInfo + "Begin");
      LOGGER.debug(logInfo + "Input Doc\n"
          + GCXMLUtil.getXMLString(inDoc));
      Element eleShipToAddress = GCXMLUtil.getElementByXPath(inDoc, "//ShipToAddress");
      if(!YFCCommon.isVoid(eleShipToAddress)) {
        String sCity = eleShipToAddress.getAttribute(GCConstants.CITY);
        if(!YFCCommon.isVoid(sCity)) {
          /* function to convert city to camel case
           *  irrespective of what is entered on the UI
           */
          sCity = converStringToCamelCase(sCity);
          eleShipToAddress.setAttribute(GCConstants.CITY, sCity);
        }
      }
      YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
      /*
      YFCNodeList<YFCElement> listOrderLine = yfcInDoc.getElementsByTagName(GCConstants.ORDER_LINE);

      int ilengthOrderLine = listOrderLine.getLength();
      for (int iCounterOrderLine = 0; iCounterOrderLine < ilengthOrderLine; iCounterOrderLine++) {
        YFCElement eleOrderLine = listOrderLine.item(iCounterOrderLine);
        YFCElement eleItem = eleOrderLine.getChildElement(GCConstants.ITEM);
        String sItemID = eleItem.getAttribute(GCConstants.ITEM_ID);
        String sUOM = eleItem.getAttribute(GCConstants.UNIT_OF_MEASURE);
        YFCDocument docGetItemListTemp =
            YFCDocument.getDocumentFor("<ItemList><Item ItemID=''><Extn ExtnSetCode=''/></Item></ItemList>");
        YFCDocument getItemListDoc = GCCommonUtil.getItemList(env, sItemID, sUOM, GCConstants.GCI, true, docGetItemListTemp);
        YFCElement eleRootItemList = getItemListDoc.getDocumentElement();
        YFCElement eleItemGetItemList = eleRootItemList.getChildElement(GCConstants.ITEM);
        YFCElement eleExtn = eleItemGetItemList.getChildElement(GCConstants.EXTN);
        String sExtnSetCode = eleExtn.getAttribute(GCConstants.EXTN_SET_CODE);
        if(YFCCommon.equals(GCConstants.EXTN_SET_CODE_VALUE, sExtnSetCode)) {
          eleItem.setAttribute(GCConstants.PRODUCT_CLASS, GCConstants.SET);
        } else {
          eleItem.setAttribute(GCConstants.PRODUCT_CLASS, GCConstants.REGULAR);
        }
      }
*/	
      YFCElement eleAlternateStores = yfcInDoc.getDocumentElement();
      String orderHeaderKey =  eleAlternateStores.getChildElement("Order").getAttribute("OrderHeaderKey");
      
      String docXML = "<OrderList><Order OrderNo='' >"
          + "<OrderLines><OrderLine OrderLineKey='' OrderedQty='' KitCode=''> <Item ItemID='' ProductClass='' UnitOfMeasure=''/><BundleParentLine/>"
          + "</OrderLine></OrderLines></Order></OrderList>";
      boolean isKit = false;
      Document tempDoc = YFCDocument.getDocumentFor(docXML).getDocument();
      Document outDoc = GCCommonUtil.invokeGetOrderListAPI(env, orderHeaderKey, tempDoc);
      YFCDocument yfcGetOrderListDoc = YFCDocument.getDocumentFor(outDoc);
      YFCElement eleRoot = yfcGetOrderListDoc.getDocumentElement();
      YFCNodeList<YFCElement> nlOrderLines = eleRoot.getElementsByTagName("OrderLine");
      YFCElement eleOrderLines = eleAlternateStores.getChildElement("OrderLines");
      eleAlternateStores.removeChild(eleOrderLines);
      YFCElement eleOLs = eleAlternateStores.createChild("OrderLines");
      
      for(YFCElement eleOrderLine : nlOrderLines){
        if(YFCCommon.isVoid(eleOrderLine.getChildElement("BundleParentLine"))){
          YFCElement eleItems = eleOrderLine.getChildElement("Item");
          String kitCode = eleOrderLine.getAttribute(GCConstants.KIT_CODE);
          String prodClass = eleItems.getAttribute("ProductClass");
          if (YFCUtils.equals(GCConstants.BUNDLE, kitCode) && YFCUtils.equals(GCConstants.REGULAR, prodClass)) {
            isKit = true;
          }
          YFCElement eleOL = eleOLs.createChild("OrderLine");
          eleOL.setAttribute("RequiredQty", eleOrderLine.getAttribute("OrderedQty"));
          YFCElement eleItem = eleOL.createChild("Item");
          eleItem.setAttribute("ItemID", eleItems.getAttribute("ItemID"));
          eleItem.setAttribute("ProductClass", prodClass);
          eleItem.setAttribute("UnitOfMeasure", eleItems.getAttribute("UnitOfMeasure"));
        }
      }
      LOGGER.debug("Input to getAlternateStoreAvailability API" + GCXMLUtil.getXMLString(inDoc));

      YFCDocument getYFCAlternateStoreAvailabilityTemplate = YFCDocument.getDocumentFor(GCConstants.GC_GET_ALTERNATE_STORE_AVAILABILITY_TEMPLATE);

      Document getAlternateStoreAvailabilityTemplate=getYFCAlternateStoreAvailabilityTemplate.getDocument();
      Document docGetAlternateStoreAvailabilityOp = GCCommonUtil.invokeAPI(env, GCConstants.GET_ALTERNATE_STORE_AVAILABILITY, inDoc,
          getAlternateStoreAvailabilityTemplate);
      LOGGER.debug(logInfo
          + "docGetAlternateStoreAvailabilityOp\n"
          + GCXMLUtil
          .getXMLString(docGetAlternateStoreAvailabilityOp));

      Element eleNodeList = (Element) XPathAPI.selectSingleNode(
          docGetAlternateStoreAvailabilityOp, "//NodeList");
      NodeList nlNode = XPathAPI.selectNodeList(
          docGetAlternateStoreAvailabilityOp, "//Node");
      for (int i = 0; i < nlNode.getLength(); i++) {

        Element eleNode = (Element) nlNode.item(i);
        String sShipNode = eleNode.getAttribute(GCConstants.SHIP_NODE);
        boolean isShipNodeAllowedForSTS = checkShipNode(env, sShipNode);
        Element eleAvailability = (Element) eleNode.getElementsByTagName("Availability").item(0);
        String sIsAvailable = eleAvailability.getAttribute("IsAvailable");
        if(YFCCommon.equals(sIsAvailable, "Y")){
          if (isKit) {
            eleAvailability.setAttribute("IsAvailable", "N");
          } else {
            String sFirstAvailableDate = eleAvailability.getAttribute("AvailableDate");
            if(!YFCCommon.isVoid(sFirstAvailableDate)){
              java.util.Date availableDate = new SimpleDateFormat(GCConstants.DATE_FORMAT).parse(sFirstAvailableDate);
              java.util.Date today = new java.util.Date();
              if(!YFCCommon.equals(GCDateUtils.getDifferenceInDays(availableDate, today),0)){
                eleAvailability.setAttribute("IsAvailable", "N");
              }
            }
          }
        }
        if (!isShipNodeAllowedForSTS) {
          eleNodeList.removeChild(eleNode);
        }
      }
      LOGGER.debug(logInfo
          + "Out Doc\n"
          + GCXMLUtil
          .getXMLString(docGetAlternateStoreAvailabilityOp));
      LOGGER.verbose(logInfo + "End");
      return docGetAlternateStoreAvailabilityOp;
    } catch (Exception e) {
      LOGGER.error("The exception is in GetAllowedShipToStore method:", e);
      throw new GCException(e);
    }
  }

  /**
   *
   * @param sCity
   * @return
   */
  private String converStringToCamelCase(String sCity) throws GCException {

    try {
      LOGGER.debug("converStringToCamelCase:: Begin");
      char[] chars = sCity.toCharArray();
      boolean mustBeUpper = true;
      for (int i=0; i<chars.length; i++) {
        if (mustBeUpper && Character.isLetter(chars[i])) {
          // This character must be upper-cased
          chars[i] = Character.toUpperCase(chars[i]);
          // but no more after this until we reach a "space"
          mustBeUpper = false;
        } else {
          chars[i] = Character.toLowerCase(chars[i]);
        }
        if (Character.isWhitespace(chars[i])) {
          // Next letter must be upper-cased
          mustBeUpper = true;
        }
      }
      sCity = new String(chars);
      LOGGER.debug("String value  " + sCity);
      LOGGER.debug("converStringToCamelCase:: Begin");
      return sCity;
    } catch (Exception e) {
      LOGGER.error("The exception is in converStringToCamelCase method:", e);
      throw new GCException(e);
    }
  }


  /**
   *
   * @param env
   * @param sShipNode
   * @return
   * @throws Exception
   */
  private boolean checkShipNode(YFSEnvironment env, String sShipNode)
      throws GCException {
    try {
      final String logInfo = "GCGetAllowedShipToStore :: checkShipNode :: ";
      LOGGER.verbose(logInfo + "Begin");

      LOGGER.debug(logInfo + "SHip Node :: " + sShipNode);
      boolean isShipNodeAllowedSTS = false;
      Document docGetOrganizationListIp = GCXMLUtil
          .createDocument(GCConstants.ORGANIZATION);
      Element eleGetOrganizationListIp = docGetOrganizationListIp
          .getDocumentElement();
      eleGetOrganizationListIp.setAttribute(
          GCConstants.ORGANIZATION_CODE, sShipNode);

      Element eleExtn = docGetOrganizationListIp
          .createElement(GCConstants.EXTN);
      eleExtn.setAttribute(GCConstants.EXTN_IS_SHIP_TO_STORE_ALLOWED,
          GCConstants.YES);
      eleGetOrganizationListIp.appendChild(eleExtn);
      LOGGER.debug(logInfo + "docGetOrganizationListIp\n"
          + GCXMLUtil.getXMLString(docGetOrganizationListIp));

      Document docGetOrgListTemplate = GCXMLUtil
          .createDocument(GCConstants.ORGANIZATION_LT);
      Element eleGetOrgListTemRoot = docGetOrgListTemplate
          .getDocumentElement();
      Element eleOrganizationTemplate = docGetOrgListTemplate
          .createElement(GCConstants.ORGANIZATION);
      eleOrganizationTemplate.setAttribute(GCConstants.ORGANIZATION_CODE,
          "");
      eleGetOrgListTemRoot.appendChild(eleOrganizationTemplate);

      Document docGetOrganizationListOp = GCCommonUtil.invokeAPI(env,
          GCConstants.GET_ORGANIZATION_LIST,
          docGetOrganizationListIp, docGetOrgListTemplate);
      LOGGER.debug(logInfo + "docGetOrganizationListOp\n"
          + GCXMLUtil.getXMLString(docGetOrganizationListOp));
      Element eleOrganization = (Element) XPathAPI.selectSingleNode(
          docGetOrganizationListOp, "//Organization");
      if (!YFCCommon.isVoid(eleOrganization)) {
        isShipNodeAllowedSTS = true;
      }
      LOGGER.debug(logInfo + "isShipNodeAllowedSTS :: "
          + isShipNodeAllowedSTS);
      LOGGER.verbose(logInfo + "End");
      return isShipNodeAllowedSTS;
    } catch (Exception e) {
      LOGGER.error("The exeption is in checkShipNode method", e);
      throw new GCException(e);
    }
  }

}
