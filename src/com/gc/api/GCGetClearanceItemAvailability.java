/**Copyright 2013 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.

	Change Log
######################################################################################################################################
     OBJECTIVE: Foundation Class
######################################################################################################################################
        Version    CR/ User Story                 Date                       Modified By                   Description
######################################################################################################################################
        1.0        GCSTORE-158                   03/05/2015                   Jain,Shubham           Development of Get Clearance Item Availability 
######################################################################################################################################
 */
package com.gc.api;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * GCStory-158 
 * This class is used to get the clearance Item availability for a particular Item from all stores.
 * 
 * @author shubham.jain
 * <a href="mailto:shubham.jain@expicient.com">Shubham</a>
 *
 */
public class GCGetClearanceItemAvailability{
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCGetClearanceItemAvailability.class.getName());

	/**
	 * This method is used to calculate clearance availability for a particular item for all stores.
	 * @param env
	 * @param inDoc
	 * @return
	 */

	public Document checkForItemAvailability(YFSEnvironment env, Document inDoc){
		LOGGER.beginTimer("GCGetClearanceItemAvailability.checkForItemAvailability");
		LOGGER.verbose("Class: GCGetClearanceItemAvailability Method: checkForItemAvailability BEGIN");

		Double clearanceQuantity=0.0;
		YFCDocument inputDoc=YFCDocument.getDocumentFor(inDoc);

		if (LOGGER.isVerboseEnabled()){
			LOGGER.verbose("Input to Service :" + inputDoc.getString());
		}

		YFCElement eleInDocPromise=inputDoc.getDocumentElement();
		String sMaxRecords = eleInDocPromise.getAttribute("MaximumRecords");
        if(YFCCommon.isStringVoid(sMaxRecords)){
          eleInDocPromise.setAttribute("MaximumRecords", "5");
        }
		YFCElement eleInDocPromiseLines=eleInDocPromise.getChildElement(GCConstants.PROMISE_LINES);
		YFCElement eleInDocPromiseLine=eleInDocPromiseLines.getChildElement(GCConstants.PROMISE_LINE);
		String sInDocItemID=eleInDocPromiseLine.getAttribute(GCConstants.ITEM_ID);

		YFCDocument outDoc=GCCommonUtil.invokeService(env,GCConstants.GC_FIND_INVENTORY_WRAPPER_SERVICE,inputDoc);

		if (LOGGER.isVerboseEnabled()){
			LOGGER.verbose("OutDoc from Service :" + outDoc.getString());
		}

		YFCNodeList<YFCElement> listPromiseLine=outDoc.getElementsByTagName(GCConstants.PROMISE_LINE);

		for(YFCElement elePromiseLine:listPromiseLine){
			String sKitCode=elePromiseLine.getAttribute(GCConstants.KIT_CODE);			
			String sItemID=elePromiseLine.getAttribute(GCConstants.ITEM_ID);
			
			YFCNodeList<YFCElement> listAssignments=elePromiseLine.getElementsByTagName("Assignment");

			for(YFCElement eleAssignment: listAssignments){
				Double availableQuantity=0.0;

				if(YFCCommon.equals(sKitCode,GCConstants.BUNDLE) && YFCCommon.equals(sInDocItemID, sItemID)){
					availableQuantity=eleAssignment.getDoubleAttribute("InvQty");
					LOGGER.verbose("For Bundle Available Quantity:" + availableQuantity);
				}
				if(!YFCCommon.equals(sKitCode,GCConstants.BUNDLE)){
					YFCElement eleSupplies=eleAssignment.getChildElement(GCConstants.SUPPLIES);
					YFCElement eleSupply=eleSupplies.getChildElement(GCConstants.SUPPLY);
					if(!YFCCommon.isVoid(eleSupply)){
						availableQuantity=eleSupply.getDoubleAttribute(GCConstants.AVAILABLE_QUANTIY);
					}
					LOGGER.verbose("For Regular Available Quantity:" + availableQuantity);
				}
				clearanceQuantity=clearanceQuantity+availableQuantity;
				LOGGER.verbose("Total Clearance Available Quantity:" + clearanceQuantity);
			}
		}
		YFCDocument clearanceAvailableQuantity=YFCDocument.createDocument(GCConstants.PROMISE); 
		YFCElement  eleAvailablePromise=clearanceAvailableQuantity.getDocumentElement();
		eleAvailablePromise.setAttribute(GCConstants.AVAILABLE_QUANTIY, clearanceQuantity);
		LOGGER.endTimer("GCGetClearanceItemAvailability.checkForItemAvailability");
		return clearanceAvailableQuantity.getDocument();
	}

}