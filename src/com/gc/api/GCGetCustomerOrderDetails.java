package com.gc.api;

import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 *
 * User Story: GCSTORE-346 This code will get complete order details along with Audit details if
 * line is Cancelled and return the consolidated output.
 *
 * @author <a href="mailto:manish.kumar@expicient.com">Manish Kumar</a>
 *
 */

public class GCGetCustomerOrderDetails implements YIFCustomApi {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCGetCustomerOrderDetails.class.getName());

  public Document fetchCompleteOrderDetails(YFSEnvironment env, Document inDoc) throws GCException {
    LOGGER.beginTimer("GCGetCustomerOrderDetails.fetchCompleteOrderDetails");
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(" Entering GCGetCustomerOrderDetails.fetchCompleteOrderDetails() method ");
      LOGGER.debug(" fetchCompleteOrderDetails() method, Input document is" + GCXMLUtil.getXMLString(inDoc));
    }
    YFCDocument doc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleRootInDoc = doc.getDocumentElement();
    String sEnterpriseCode = eleRootInDoc.getAttribute(GCConstants.ENTERPRISE_CODE);

    String sOrderNo = eleRootInDoc.getAttribute(GCConstants.ORDER_NO);

    YFCDocument getCompleteOrderDetailsIn =
        YFCDocument.getDocumentFor("<Order EnterpriseCode='" + sEnterpriseCode + "' OrderNo='" + sOrderNo + "'/>");
    Document getCompleteOrderDetailsTemplate =
        GCCommonUtil.getDocumentFromPath("/global/template/api/getCompleteOrderDetailsTempForCustomerOrderDetails.xml");
    YFCDocument getCompleteOrderDetailsOut =
        GCCommonUtil.invokeAPI(env, GCConstants.API_GET_COMPLETE_ORDER_DETAILS, getCompleteOrderDetailsIn,
            YFCDocument.getDocumentFor(getCompleteOrderDetailsTemplate));

    if (LOGGER.isDebugEnabled()) {
      LOGGER.verbose("getCompleteOrderDetailsOut : " + getCompleteOrderDetailsOut.toString());
    }
    LOGGER.endTimer("GCGetCustomerOrderDetails.fetchCompleteOrderDetails()");
    return getCompleteOrderDetailsOut.getDocument();
  }
  @Override
  public void setProperties(Properties arg0) {

  }

}
