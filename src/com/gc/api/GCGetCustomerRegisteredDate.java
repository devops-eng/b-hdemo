/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class is called before Fraud request is sent and is used to fetch Member Create Date
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               16/02/2015        Kumar Prateek  		  OMS-4871 CR78 - Production - Accertify is missing data in the "Member create Date" field
 *#################################################################################################################################################################
 */
package com.gc.api;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCGetCustomerRegisteredDate {

	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCGetCustomerRegisteredDate.class);

	/**
	 * Description : Fetch Registered Date for customer
	 * @param env
	 * @param inDoc
	 * @return
	 * @throws GCException
	 */
	public Document getCustomerRegisteredDate(YFSEnvironment env, Document inDoc)
			throws GCException {
	    try {
	        LOGGER.debug("Entering the Customer Registered Date method Incoming Document:"+GCXMLUtil.getXMLString(inDoc));
	        Element eleOrder = GCXMLUtil.getElementByXPath(inDoc,
	                "OrderList/Order");

	        //Fetch Organization Code & Customer ID from input document
	        String strOrganizationCode = eleOrder.getAttribute(GCConstants.ORGANIZATION_CODE);
	        String strCustomerID = eleOrder.getAttribute(GCConstants.BILL_TO_ID);
	        //Form input xml for getCustomerList API
	        String strGetCustomerList = "<Customer CustomerID='"
	                + strCustomerID + "' OrganizationCode='"
	                + strOrganizationCode + "'/>";
	        Document inDocGetCustomerList = GCXMLUtil.getDocument(strGetCustomerList);

	        //Invoke getCustomerList API
	        Document outDocGetCustomerList=GCCommonUtil.invokeAPI(env, GCConstants.GET_CUSTOMER_LIST,
	                inDocGetCustomerList, GCXMLUtil.getDocument("<CustomerList><Customer RegisteredDate='' /></CustomerList>"));

	        //Extract RegisteredDate from output xml of getCustomerList API
	        String strMemberCreateDate = GCXMLUtil.getAttributeFromXPath(outDocGetCustomerList,
	                "CustomerList/Customer/@RegisteredDate");

	        //Stamp Registered Date as Member Create Date
	        Element eleOrderExtn=GCXMLUtil.getElementByXPath(inDoc,
	                "OrderList/Order/Extn");

	        eleOrderExtn.setAttribute(GCConstants.EXTN_MEMBER_CREATE_DATE,strMemberCreateDate);
	        LOGGER.debug("Exiting the Customer Registered Date method Output Document:"+GCXMLUtil.getXMLString(inDoc));
	    } catch (Exception e) {
			LOGGER.error("Inside GCGetCustomerRegisteredDate.getCustomerRegisteredDate() :",e);
			throw new GCException(e);
		}
		return inDoc;
	}

}

