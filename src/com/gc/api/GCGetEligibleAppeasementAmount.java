/**
 * Copyright 2014 Guitar Center. All rights reserved. Guitar Center PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 *
 * Change Log
 * #######################################################################################
 * ###########
 * #######################################################################################
 * ############################ OBJECTIVE: Compute the eligible Amount for Appeasement.
 * #############
 * #####################################################################################
 * #############
 * #####################################################################################
 * ################## Version CR/ User Story Date Modified By Description
 * ###########################
 * #######################################################################
 * ###########################
 * ######################################################################################### 1.0
 * GCSTORE-661 03/27/2015 Jain,Shubham Compute the eligible Amount for Appeasement.
 * #################
 * #################################################################################
 * #################
 * #################################################################################
 * ##################
 */
package com.gc.api;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/** This class is used to compute the eligible amount for Appeasement.
 *
 * @author shubham.jain
 * <a href="mailto:shubham.jain@expicient.com">Shubham</a>
 *
 */

public class GCGetEligibleAppeasementAmount {

  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCGetEligibleAppeasementAmount.class.getName());

  /** This method is used to calculate all the amount for each order line and store it an array.
   * And then retun the minimum amount from the array which will be eligible amount for appeasement.
   *
   * @param env
   * @param inDoc
   * @return
   */
  public Document getEligibleAppeasementAmount(YFSEnvironment env, Document inDoc){

    LOGGER.beginTimer("GCGetEligibleAppeasementAmount.getEligibleAppeasementAmount");
    LOGGER.verbose("Class: GCGetEligibleAppeasementAmount Method: getEligibleAppeasementAmount BEGIN");

    Double invoiceChargeAmountSum=0.0;
    Double invoiceChargeAmount=0.0;
    Double eligibleAmount=0.0;
    Double shippingChargeAmount=0.0;
    Double prevShippingAppeasement=0.0;
    Double returnChargeAmountSum=0.0;
    Double prevShippingRefunds=0.0;
    Double prevAmount=0.0;

    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);

    if (LOGGER.isVerboseEnabled()){
      LOGGER.verbose("Input Document :" + inputDoc.getString());
    }

    YFCElement eleRoot = inputDoc.getDocumentElement();
    YFCElement eleOrder = eleRoot.getChildElement(GCConstants.ORDER);
    YFCElement eleAppeasement = eleOrder.getChildElement("AppeasementReason");
    String sOrderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
    String sGCAppeaseType=eleAppeasement.getAttribute("GCAppeaseType");
    YFCElement eleOrderLines= eleOrder.getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> listOrderLine=eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);

    List<String> orderLineKeyList = new ArrayList<String>(listOrderLine.getLength());
    for(YFCElement eleOrderLine:listOrderLine){
      String sOrderLineKey=eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      orderLineKeyList.add(sOrderLineKey);
    }


    //Creating a Input Document for getCompleteOrderDetailsAPI
    YFCDocument getOrderDetailsIndoc=YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleOrderIndoc=getOrderDetailsIndoc.getDocumentElement();
    eleOrderIndoc.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);

    //Template of getCompleteOrderDetails API
    YFCDocument getCompleteOrderDetailsTemplate = YFCDocument.getDocumentFor(GCConstants.GC_GET_COMPLETE_ORDER_DETAILS_TEMPLATE);

    //Calling  getCompleteOrderDetails API
    YFCDocument getCompleteOrderDetailsOutDoc=GCCommonUtil.invokeAPI(env, "getCompleteOrderDetails", getOrderDetailsIndoc, getCompleteOrderDetailsTemplate);
    YFCElement eleOutDocOrder= getCompleteOrderDetailsOutDoc.getDocumentElement();
    YFCElement eleOutDocOrderLines=eleOutDocOrder.getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> listOutDocOrderLine=eleOutDocOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);



    // Creating Input Document for getOrderInvoiceDetailList API
    YFCDocument docGetOrderInvoiceDetailListIndoc = YFCDocument.createDocument(GCConstants.ORDER_INVOICE_DETAIL);
    YFCElement eleInvoiceRoot = docGetOrderInvoiceDetailListIndoc.getDocumentElement();
    YFCElement eleInvoiceHeader = docGetOrderInvoiceDetailListIndoc.createElement(GCConstants.INVOICE_HEADER);
    eleInvoiceRoot.appendChild(eleInvoiceHeader);
    eleInvoiceHeader.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);

    if (LOGGER.isVerboseEnabled()){
      LOGGER.verbose("getOrderInvoiceDetailList API Input :" + docGetOrderInvoiceDetailListIndoc.getString());
    }

    //Template of getOrderInvoiceDetailList API
    YFCDocument getOrderInvoiceDetailsListTemplate = YFCDocument.getDocumentFor(GCConstants.GC_GET_ORDER_INVOICE_DETAILS_LIST_API_TEMPLATE);

    //Calling  getOrderInvoiceDetailList API
    YFCDocument getOrderInvoiceDetailsOutDoc=GCCommonUtil.invokeAPI(env, "getOrderInvoiceDetailList", docGetOrderInvoiceDetailListIndoc, getOrderInvoiceDetailsListTemplate);
    YFCElement eleOrderInvoiceDetailList=getOrderInvoiceDetailsOutDoc.getDocumentElement();
    YFCNodeList<YFCElement> listOrderInvoiceDetail=eleOrderInvoiceDetailList.getElementsByTagName(GCConstants.ORDER_INVOICE_DETAIL);

    //Creating an Array to store Eligible Amount for Appeasement
    Double[] eligibleAmonutArray= new Double[orderLineKeyList.size()];
    int i=0;


    if(YFCCommon.equals(sGCAppeaseType,"ORDER")){
      //Iterating for each OrderLine of getCompleteOrderDetails Output
      for(YFCElement eleOutDocOrderLine : listOutDocOrderLine){
        String sOutDocOrderLineKey=eleOutDocOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);

        Double returnedAmount = 0.0;
        //GCSTORE-5934
        Double extendedPrice=0.0;
        //GCSTORE-5934--End
        if(orderLineKeyList.contains(sOutDocOrderLineKey)){
            //GCTSTORE-5934-starts
            String strParentOrderLineKey=null;
            YFCElement eleBundleParentLine=eleOutDocOrderLine.getChildElement("BundleParentLine");
            if(!YFCCommon.isVoid(eleBundleParentLine))
            {
                strParentOrderLineKey=eleBundleParentLine.getAttribute("OrderLineKey"); 
            }
          
            LOGGER.debug("strParentOrderLineKey :: "+strParentOrderLineKey);
            //GCSTORE-5934--End
            YFCElement eleOutDocLineOverallTotals=eleOutDocOrderLine.getChildElement("LineOverallTotals");
            //GCSTORE-5934--Start
            if(YFCCommon.isVoid(strParentOrderLineKey))
            {
                extendedPrice=eleOutDocLineOverallTotals.getDoubleAttribute("ExtendedPrice");
            }
            else
            {
                LOGGER.debug("Its a kit component.");
                extendedPrice=calculateAmountForKitComponent(eleOutDocOrderLine, GCConstants.UNIT_PRICE, GCConstants.UNIT_PRICE); 
            }
            //GSTORE-5934-ends
        

          LOGGER.verbose("ExtendedPrice:" + extendedPrice);

          //Iterating for each InvoiceOrderDetail of getOrderInvoiceDetailList Output
          for(YFCElement eleOrderInvoiceDetail : listOrderInvoiceDetail ){
            invoiceChargeAmount=invoiceAmount(eleOrderInvoiceDetail,GCConstants.CUSTOMER_APPEASEMENT, sOutDocOrderLineKey);
            LOGGER.verbose("InvoiceChargeAmount:" + invoiceChargeAmount);
            invoiceChargeAmountSum=invoiceChargeAmountSum+invoiceChargeAmount;
            LOGGER.verbose("InvoiceChargeAmountSum:" + invoiceChargeAmountSum);

          }

          // Calculating total returned amount for current order line
          returnedAmount = calculateReturnedAmount(eleOutDocOrder, sOutDocOrderLineKey);

          // Discount applied on line
          //GCSTORE-5934 --Starts
          double discount=0.0;
          if(YFCCommon.isVoid(strParentOrderLineKey))
          {
              discount = eleOutDocLineOverallTotals.getDoubleAttribute("Discount");
          }
          //GCSTORE-5934-starts
          else
          {
              discount=calculateDiscountForKitComponent(eleOutDocOrderLine,GCConstants.PROMOTION)+calculateDiscountForKitComponent(eleOutDocOrderLine,GCConstants.SHIPPING_PROMOTION);
              
          }
          //GCSTORE-5934-ends

          eligibleAmount = extendedPrice - invoiceChargeAmountSum - returnedAmount - discount;
          LOGGER.verbose("Eligible Amount:" + eligibleAmount);
          eligibleAmonutArray[i]=eligibleAmount;
          i++;
          invoiceChargeAmountSum=0.0;
        }
      }
    }else{
      for(YFCElement eleOutDocOrderLine : listOutDocOrderLine){
        String sOutDocOrderLineKey=eleOutDocOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
        if(orderLineKeyList.contains(sOutDocOrderLineKey)){
            
            
          //GCTSTORE-5934-for shipping appeasement
            String strParentOrderLineKey=null;
            YFCElement eleBundleParentLine=eleOutDocOrderLine.getChildElement("BundleParentLine");
            if(!YFCCommon.isVoid(eleBundleParentLine))
            {
                strParentOrderLineKey=eleBundleParentLine.getAttribute("OrderLineKey"); 
            }
            LOGGER.debug("strParentOrderLineKey :: "+strParentOrderLineKey);
            if(YFCCommon.isVoid(strParentOrderLineKey))
            {
            //GCTSTORE-5934-for shipping appeasement--End
          YFCElement eleLOutDocLineCharges=eleOutDocOrderLine.getChildElement(GCConstants.LINE_CHARGES);

          YFCNodeList<YFCElement> listOutDocLineCharge=eleLOutDocLineCharges.getElementsByTagName(GCConstants.LINE_CHARGE);
          for (YFCElement eleOutDocLineCharge : listOutDocLineCharge) {
            String sChargeCategory=eleOutDocLineCharge.getAttribute(GCConstants.CHARGE_CATEGORY);
            if(YFCCommon.equals(sChargeCategory,"Shipping")){

              shippingChargeAmount=eleOutDocLineCharge.getDoubleAttribute(GCConstants.CHARGE_AMOUNT);
              if (LOGGER.isVerboseEnabled()) {
                LOGGER.verbose("Shipping Charge Amount :" + shippingChargeAmount);
              }
             //GCSTORE-5934--Start
              }
            }
            }
            else
            {
                shippingChargeAmount=calculateAmountForKitComponent(eleOutDocOrderLine, GCConstants.SHIPPING_CHARGE, GCConstants.SHIPPING); 
            }
             //GCSTORE-5934--End
          
              //Iterating for each InvoiceOrderDetail of getOrderInvoiceDetailList Output
              for(YFCElement eleOrderInvoiceDetail : listOrderInvoiceDetail ){
                invoiceChargeAmount=invoiceAmount(eleOrderInvoiceDetail,"SHIPPING_APPEASEMENT", sOutDocOrderLineKey);
                LOGGER.verbose("InvoiceChargeAmount:" + invoiceChargeAmount);
                invoiceChargeAmountSum=invoiceChargeAmountSum+invoiceChargeAmount;
                LOGGER.verbose("InvoiceChargeAmountSum:" + invoiceChargeAmountSum);
              }
          prevShippingAppeasement=invoiceChargeAmountSum;
          invoiceChargeAmountSum=0.0;

          //Return Order
          YFCElement eleOutDocReturnOrders=eleOutDocOrder.getChildElement("ReturnOrders");
          YFCNodeList<YFCElement> nlOutDocReturnOrder = eleOutDocReturnOrders.getElementsByTagName("ReturnOrder");
          for (YFCElement eleOutDocReturnOrder : nlOutDocReturnOrder) {
            YFCElement eleOutDocReturnOrderLines=eleOutDocReturnOrder.getChildElement(GCConstants.ORDER_LINES);
            YFCNodeList<YFCElement> listOutDocReturnOrderLine=eleOutDocReturnOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);

            //Iterating for each ReturnOrderLine of getCompleteOrderDetails Output
            for(YFCElement eleOutDocReturnOrderLine : listOutDocReturnOrderLine){
              String sOutDocReturnDerivedOrderLineKey=eleOutDocReturnOrderLine.getAttribute(GCConstants.DERIVED_FROM_ORDER_LINE_KEY);
              YFCElement eleLineCharges=eleOutDocReturnOrderLine.getChildElement(GCConstants.LINE_CHARGES);
              returnChargeAmountSum = getReturnChargeAmtSum(
                  returnChargeAmountSum, sOutDocOrderLineKey,
                  sOutDocReturnDerivedOrderLineKey,
                  eleLineCharges);
            }
          }
          prevShippingRefunds = returnChargeAmountSum;
          returnChargeAmountSum = 0.0;
          prevAmount=prevShippingAppeasement+prevShippingRefunds;
          LOGGER.verbose("PrevAmount:" + prevAmount);
          eligibleAmount=shippingChargeAmount-prevAmount;
          LOGGER.verbose("EligibleAmount:" + eligibleAmount);
          eligibleAmonutArray[i]=eligibleAmount;
          i++;
        }
      }
    }
    //Sorting the Array
    Arrays.sort(eligibleAmonutArray);
    eligibleAmount=eligibleAmonutArray[0];
    LOGGER.verbose("Final Eligible Amount:" + eligibleAmount);

    //Creating the  return Document
    YFCDocument eligibleAmountDocument=YFCDocument.createDocument("Appeasement");
    YFCElement  eleAmount=eligibleAmountDocument.getDocumentElement();
    eleAmount.setAttribute("EligibleLineAppeasement", eligibleAmount);

    LOGGER.endTimer("GCGetEligibleAppeasementAmount.getEligibleAppeasementAmount");
    return eligibleAmountDocument.getDocument();

  }
//GCSTORE-5934--Start
public double calculateDiscountForKitComponent(YFCElement eleOutDocOrderLine,String strChargeCategory) {
    LOGGER.debug("GCGetEligibleAppeasementAmount :: calculateDiscountForKitComponent :: Begin");
    
    LOGGER.debug("GCGetEligibleAppeasementAmount :: calculateDiscountForKitComponent :: eleOutDocOrderLine :: +\n"+eleOutDocOrderLine.toString());
    
    
    YFCElement eleExtn=eleOutDocOrderLine.getChildElement("Extn");
    Double returnAmount=0.0;
    if(!YFCCommon.isVoid(eleExtn))
    {
     YFCElement eleGCOrderLineComponentsList=eleExtn.getChildElement("GCOrderLineComponentsList");
     String strXml=eleGCOrderLineComponentsList.getString();
     Document docGCOrderLineComponentsList=GCXMLUtil.getDocument(strXml);
     NodeList nlGCOrderLineComponentsList=docGCOrderLineComponentsList.getElementsByTagName("GCOrderLineComponents");
      if(nlGCOrderLineComponentsList.getLength()>=1)    
          
      { 
          for(int i=0;i<nlGCOrderLineComponentsList.getLength();i++)
        {
            Element eleOrderLineComponent= (Element) nlGCOrderLineComponentsList.item(i);
           if(strChargeCategory.equals(eleOrderLineComponent.getAttribute("ChargeCategory")))
           {
               returnAmount=returnAmount+ Double.parseDouble(eleOrderLineComponent.getAttribute("Amount"));
               
           }
         
        }
      }
    }
    LOGGER.debug("GCGetEligibleAppeasementAmount :: calculateDiscountForKitComponent :: ReturnAmount :: "+returnAmount);
    LOGGER.debug("GCGetEligibleAppeasementAmount :: calculateDiscountForKitComponent :: End");
    return returnAmount;
    
}

/**
 * This method is used to calculate prorated amount for kit component
 * @param eleOutDocOrderLine
 * @param strChargeName
 * @param strChargeCategory
 * @return returnAmount
 */
  
public Double calculateAmountForKitComponent(YFCElement eleOutDocOrderLine,String strChargeName,String strChargeCategory )
{
    LOGGER.debug("GCGetEligibleAppeasementAmount :: calculateAmountForKitComponent :: Begin");
    
    LOGGER.debug("GCGetEligibleAppeasementAmount :: calculateAmountForKitComponent :: eleOutDocOrderLine :: +\n"+eleOutDocOrderLine.toString());
    LOGGER.debug("GCGetEligibleAppeasementAmount :: calculateAmountForKitComponent :: strChargeName :: "+strChargeName);
    LOGGER.debug("GCGetEligibleAppeasementAmount :: calculateAmountForKitComponent :: strChargeCategory :: "+strChargeCategory);
    
    YFCElement eleExtn=eleOutDocOrderLine.getChildElement("Extn");
    Double amount=0.0;
    Double returnAmount=0.0;
    Double quantity=0.0;
    if(!YFCCommon.isVoid(eleExtn))
    {
     YFCElement eleGCOrderLineComponentsList=eleExtn.getChildElement("GCOrderLineComponentsList");
     String strXml=eleGCOrderLineComponentsList.getString();
     Document docGCOrderLineComponentsList=GCXMLUtil.getDocument(strXml);

        if(eleGCOrderLineComponentsList.hasChildNodes() && !YFCCommon.isVoid(strChargeName) && !YFCCommon.isVoid(strChargeCategory))
        {
           Element eleGCOrderLineComponent=GCXMLUtil.getElementByXPath(docGCOrderLineComponentsList, "/GCOrderLineComponentsList/GCOrderLineComponents[@ChargeName='"+ strChargeName + "'][@ChargeCategory='"+strChargeCategory+"']"); 
           if(!YFCCommon.isVoid(eleGCOrderLineComponent))
           {
           LOGGER.debug("GCGetEligibleAppeasementAmount :: calculateAmountForKitComponent :: eleGCOrderLineComponent :: "+GCXMLUtil.getElementString(eleGCOrderLineComponent));
           amount=Double.parseDouble(GCXMLUtil.getAttributeFromXPath(docGCOrderLineComponentsList, "/GCOrderLineComponentsList/GCOrderLineComponents[@ChargeName='"+ strChargeName + "'][@ChargeCategory='"+strChargeCategory+"']/@Amount"));
           quantity=Double.parseDouble(GCXMLUtil.getAttributeFromXPath(docGCOrderLineComponentsList, "/GCOrderLineComponentsList/GCOrderLineComponents[@ChargeName='"+ strChargeName + "'][@ChargeCategory='"+strChargeCategory+"']/@Quantity"));
           }
           LOGGER.debug("GCGetEligibleAppeasementAmount :: calculateAmountForKitComponent :: amount :: "+amount);
           LOGGER.debug("GCGetEligibleAppeasementAmount :: calculateAmountForKitComponent :: quantity :: "+quantity);
            
         if(GCConstants.UNIT_PRICE.contains(strChargeCategory))
         {
             returnAmount=amount*quantity;
         }
         else if(GCConstants.SHIPPING.contains(strChargeCategory))
         {
             returnAmount=amount;
         }
        }
    }
    LOGGER.debug("GCGetEligibleAppeasementAmount :: calculateAmountForKitComponent :: ReturnAmount :: "+returnAmount);
    LOGGER.debug("GCGetEligibleAppeasementAmount :: calculateAmountForKitComponent :: End");
    return returnAmount;
    
}
  //GCSTORE-5934--End
  
  /**
   * This method is used to calculate total returned amount of a particular line
   *
   * @param eleOutDocOrder
   * @param sOutDocOrderLineKey
   * @return totalReturnedAmount
   */
  private Double calculateReturnedAmount(YFCElement eleOutDocOrder, String sOutDocOrderLineKey) {

    LOGGER.beginTimer("GCGetEligibleAppeasementAmount.calculateReturnedAmount");
    LOGGER.verbose("Class: GCGetEligibleAppeasementAmount Method: calculateReturnedAmount BEGIN");

    LOGGER.verbose("eleOutDocOrder : " + eleOutDocOrder.toString());
    LOGGER.verbose("sOutDocOrderLineKey :" + sOutDocOrderLineKey);
    double returnedAmt = 0.0;
    YFCElement eleOutDocReturnOrders = eleOutDocOrder.getChildElement("ReturnOrders");
    YFCNodeList<YFCElement> nlOutDocReturnOrder = eleOutDocReturnOrders.getElementsByTagName("ReturnOrder");
    for (YFCElement eleOutDocReturnOrder : nlOutDocReturnOrder) {
      YFCElement eleOutDocReturnOrderLines = eleOutDocReturnOrder.getChildElement(GCConstants.ORDER_LINES);
      YFCNodeList<YFCElement> listOutDocReturnOrderLine =
          eleOutDocReturnOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);

      // Iterating for each ReturnOrderLine of getCompleteOrderDetails Output
      for (YFCElement eleOutDocReturnOrderLine : listOutDocReturnOrderLine) {
        String sOutDocReturnDerivedOrderLineKey =
            eleOutDocReturnOrderLine.getAttribute(GCConstants.DERIVED_FROM_ORDER_LINE_KEY);
        YFCElement eleLineOverallTotals = eleOutDocReturnOrderLine.getChildElement("LineOverallTotals");
        if (!YFCCommon.isVoid(eleLineOverallTotals)
            && YFCCommon.equals(sOutDocOrderLineKey, sOutDocReturnDerivedOrderLineKey)) {
          returnedAmt = returnedAmt + eleLineOverallTotals.getDoubleAttribute("ExtendedPrice");
        }

      }
    }

    LOGGER.verbose("returnedAmt :" + returnedAmt);
    LOGGER.verbose("Class: GCGetEligibleAppeasementAmount Method: calculateReturnedAmount END");
    LOGGER.endTimer("GCGetEligibleAppeasementAmount.calculateReturnedAmount");
    return returnedAmt;
  }

  private Double getReturnChargeAmtSum(Double returnChargeAmountSum,
      String sOutDocOrderLineKey,
      String sOutDocReturnDerivedOrderLineKey, YFCElement eleLineCharges) {
    Double returnChargeAmount;
    if(!YFCCommon.isVoid(eleLineCharges)){
      YFCElement eleLineCharge=eleLineCharges.getChildElement(GCConstants.LINE_CHARGE);
      if(!YFCCommon.isVoid(eleLineCharge)){
        String sChargeName=eleLineCharge.getAttribute(GCConstants.CHARGE_NAME);

        if((YFCCommon.equals(sOutDocOrderLineKey, sOutDocReturnDerivedOrderLineKey)) && (YFCCommon.equals(sChargeName, GCConstants.SHIPPING_REFUND))){
          returnChargeAmount=eleLineCharge.getDoubleAttribute(GCConstants.CHARGE_AMOUNT);
          LOGGER.verbose("ReturnChargeAmount:" + returnChargeAmount);
          returnChargeAmountSum= returnChargeAmountSum + returnChargeAmount;
          LOGGER.verbose("ReturnChargeAmountSum:" + returnChargeAmountSum);
        }
      }
    }
    return returnChargeAmountSum;
  }

  private Double invoiceAmount(YFCElement eleOrderInvoiceDetail,String appeasementType, String sOutDocOrderLineKey ){
    LOGGER.beginTimer("GCGetEligibleAppeasementAmount.invoiceAmount");
    LOGGER.verbose("Class: GCGetEligibleAppeasementAmount Method: invoiceAmount BEGIN");

    Double invoiceChargeAmount=0.0;
    YFCElement eleLineCharges=eleOrderInvoiceDetail.getChildElement(GCConstants.LINE_CHARGES);
    if(!YFCCommon.isVoid(eleLineCharges)){
      YFCElement eleLineCharge=eleLineCharges.getChildElement(GCConstants.LINE_CHARGE);
      if(!YFCCommon.isVoid(eleLineCharge)){
        String sChargeName=eleLineCharge.getAttribute(GCConstants.CHARGE_NAME);
        String sInvoiceOrderLineKey=eleOrderInvoiceDetail.getAttribute(GCConstants.ORDER_LINE_KEY);
        if((YFCCommon.equals(sOutDocOrderLineKey, sInvoiceOrderLineKey)) && (YFCCommon.equals(sChargeName,appeasementType ))){
          invoiceChargeAmount=eleLineCharge.getDoubleAttribute(GCConstants.CHARGE_AMOUNT);
        }
      }
    }

    Double invoiceTax=0.0;
    YFCElement eleLineTaxes=eleOrderInvoiceDetail.getChildElement(GCConstants.LINE_TAXES);
    if(!YFCCommon.isVoid(eleLineTaxes)){
      YFCElement eleLineTax=eleLineTaxes.getChildElement(GCConstants.LINE_TAX);
      if(!YFCCommon.isVoid(eleLineTax)){
        String sChargeName=eleLineTax.getAttribute(GCConstants.CHARGE_NAME);
        String sInvoiceOrderLineKey=eleOrderInvoiceDetail.getAttribute(GCConstants.ORDER_LINE_KEY);
        if((YFCCommon.equals(sOutDocOrderLineKey, sInvoiceOrderLineKey)) && (YFCCommon.equals(sChargeName,appeasementType ))){
          invoiceTax=eleLineTax.getDoubleAttribute(GCConstants.TAX);
        }
      }
    }


    LOGGER.endTimer("GCGetEligibleAppeasementAmount.invoiceAmount");
    return (invoiceChargeAmount);
  }
}