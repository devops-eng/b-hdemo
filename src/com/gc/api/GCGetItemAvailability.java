/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class is used to know the availability of an item.
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               06/03/2014        Gowda, Naveen		JIRA No: This class is used to know the availability of an item.
 *#################################################################################################################################################################
             1.0               08/01/2017        Mishra, Deepak		JIRA No:3706
 *#################################################################################################################################################################

 */
package com.gc.api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * @author <a href="mailto:email Address">Naveen</a>
 */
public class GCGetItemAvailability {

	// initialize logger
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCGetItemAvailability.class.getName());

	/**
	 *
	 * Description of getAvailableInventory - This method is used to check if
	 * inventory is available.
	 *
	 * @param env
	 * @param inDoc
	 * @return
	 * @throws Exception
	 *
	 */
	public Document getAvailableInventory(YFSEnvironment env, Document inDoc)
			throws GCException {
			try {
			LOGGER.debug("Class: GCGetItemAvailability Method: getAvailableInventory BEGIN");
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Incoming XML" + GCXMLUtil.getXMLString(inDoc));
			}

			Element eleRoot = inDoc.getDocumentElement();
			
			String sIsOrderAmend = GCXMLUtil.getAttributeFromXPath(inDoc,
					"//Item/@IsOrderAmendment");

			String sLevelOfService = eleRoot
					.getAttribute(GCConstants.LEVEL_OF_SERVICE);

			eleRoot.removeAttribute(GCConstants.FULFILLMENT_TYPE);
			eleRoot.removeAttribute(GCConstants.IS_ORDER_AMENDMENT);
			eleRoot.removeAttribute(GCConstants.LEVEL_OF_SERVICE);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("geTcompleteList Indoc "
						+ GCXMLUtil.getXMLString(inDoc));
			}
			Document docGetCompItmLstOut = GCCommonUtil.invokeAPI(env, inDoc,
					GCConstants.API_GET_COMPLETE_ITEM_LIST,
					"/global/template/api/getCompleteItemList.xml");
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("getCompleteItemList Output in GCGetItemAvailabilityService"
						+ GCXMLUtil.getXMLString(docGetCompItmLstOut));
			}
			Element eleItem = GCXMLUtil.getElementByXPath(docGetCompItmLstOut,
					"//Item");
			String sBarCodeData = GCXMLUtil.getAttributeFromXPath(inDoc,
					"//@BarCodeData");
			Element eleBarCode = GCXMLUtil.getElementByXPath(inDoc,
					"//BarCode");
			
			if (YFCCommon.isVoid(eleItem)) {

				GCXMLUtil.removeChild(eleRoot, eleBarCode);

				Element eleExtn = inDoc.createElement(GCConstants.EXTN);
				eleExtn.setAttribute("ExtnPOSItemID", sBarCodeData);
				eleRoot.appendChild(eleExtn);
				docGetCompItmLstOut = GCCommonUtil.invokeAPI(env, inDoc,
						GCConstants.API_GET_COMPLETE_ITEM_LIST,
						"/global/template/api/getCompleteItemList.xml");
				eleItem = GCXMLUtil.getElementByXPath(docGetCompItmLstOut,
						"//Item");
				
			}
			//5947 - Begin
			eleItem = GCXMLUtil.getElementByXPath(docGetCompItmLstOut,
					"//Item");
			if (YFCCommon.isVoid(eleItem)) {

				Element eleExtn = GCXMLUtil.getElementByXPath(inDoc, "//Extn");
				eleExtn.setAttribute("ExtnPOSItemID", "");
				eleExtn.setAttribute("ExtnEnterpriseItemID", sBarCodeData);
				//eleRoot.appendChild(eleExtn);
				docGetCompItmLstOut = GCCommonUtil.invokeAPI(env, inDoc,
						GCConstants.API_GET_COMPLETE_ITEM_LIST,
						"/global/template/api/getCompleteItemList.xml");
				eleItem = GCXMLUtil.getElementByXPath(docGetCompItmLstOut,
						"//Item");
				if (YFCCommon.isVoid(eleItem)) {
					YFCException yfs = new YFCException(
							GCErrorConstants.INVALID_ITEM_ERROR_CODE);
					yfs.setErrorDescription(GCErrorConstants.INVALID_ITEM_ERROR_DESCRIPTION);
					throw yfs;
				}
			}
			
			//5947 - End 
			// GCSTORE-194:Start
			String sStoreId = eleRoot.getAttribute("StoreId");
			eleRoot.removeAttribute("StoreId");
			String sItemID = eleItem.getAttribute("ItemID");
			Element eleExtn = (Element) eleItem.getElementsByTagName("Extn")
					.item(0);
			String sExtnBrandId = eleExtn.getAttribute("ExtnBrandId");
			// check if storeId is not null input Document and ExtnBrandId is
			// not null in getCompleteItemDetails, call the method
			// isItemSellableFromLoggedInStore()
			if (!YFCCommon.isVoid(sStoreId) && !YFCCommon.isVoid(sExtnBrandId)) {
				isItemSellableFromLoggedInStore(env, sStoreId, sExtnBrandId,
						sItemID);
			}
			// GCSTORE-194:End
			/*
			 * OMS - 3599 - Unable to add warranty in shipped status for an
			 * order. Fix: If OrderMinStatus is greater than 3200 then only
			 * warranty items can be added. Exception will be thrown on adding
			 * regular items after released status.
			 */
			String sMaxOrderStatus = GCXMLUtil.getAttributeFromXPath(inDoc,
					"//Item/@MaxOrderStatus");
			String sItemType = GCXMLUtil.getAttributeFromXPath(
					docGetCompItmLstOut, "//Item/PrimaryInformation/@ItemType");

			/* GCSTORE - 557 - Start */
			YFCDocument docYFCGetCompList = YFCDocument
					.getDocumentFor(docGetCompItmLstOut);
			YFCElement eleGetCompListRoot = docYFCGetCompList
					.getDocumentElement();
			YFCElement eleGetCompListItem = eleGetCompListRoot
					.getChildElement(GCConstants.ITEM);
			YFCElement eleGetCompListItemExtn = eleGetCompListItem
					.getChildElement(GCConstants.EXTN);
			String sExtnIsUsedOrVintage = eleGetCompListItemExtn
					.getAttribute("ExtnIsUsedOrVintageItem");
			String sReqShipDate = eleRoot
					.getAttribute(GCConstants.REQ_SHIP_DATE);
			if (YFCCommon.equals(GCConstants.FLAG_Y, sExtnIsUsedOrVintage)
					&& !YFCCommon.isVoid(sReqShipDate)) {
				checkIfFutureShipDateIsStamped(sReqShipDate);
			}
			/* GCSTORE - 557 - End */
			
			// GCSTORE-2314
			YFCElement eleYFCItem = eleGetCompListRoot.getChildElement(GCConstants.ITEM);
			YFCElement eleComputedPrice = eleYFCItem.getChildElement(GCConstants.COMPUTED_PRICE);
			String sListPrice = eleComputedPrice.getAttribute(GCConstants.LIST_PRICE);
			if(YFCCommon.isVoid(sListPrice)) {
				YFCException yfs = new YFCException(GCErrorConstants.ITEM_LIST_PRICE_NOT_DEFINED_CODE);
				yfs.setErrorDescription(GCErrorConstants.ITEM_LIST_PRICE_NOT_DEFINED_DESCRIPTION);
				throw yfs;
			}
			//GCSTORE-2314
			
			if (YFCCommon.equals(sItemType, "04")
					&& (YFCCommon.equals(sLevelOfService,
							GCConstants.NEXT_DAY_AIR) || YFCCommon.equals(
							sLevelOfService, GCConstants.SECOND_DAY_EXPRESS))) {
				YFCException yfs = new YFCException(GCErrorConstants.GIFT_ITEM_ERROR_CODE);
				yfs.setErrorDescription(GCErrorConstants.GIFT_ITEM_ERROR_DESC);
				throw yfs;
			}

			if (!YFCCommon.isVoid(sMaxOrderStatus)) {
				String[] parts = sMaxOrderStatus.split("\\.");
				String sOrderStatus = parts[0];
				LOGGER.debug("sOrderStatus::" + sOrderStatus);

				Double dMaxOrderStatus = Double.parseDouble(sOrderStatus);
				Double dShippedStatus = Double
						.parseDouble(GCConstants.SHIPPED_STATUS);
				Double dReleasedStatus = Double
						.parseDouble(GCConstants.RELEASED_STATUS);
				String storeUser = eleRoot
				.getAttribute(GCConstants.STORE_USER);
				Double dCreatedStatus = Double
						.parseDouble(GCConstants.STATUS_CREATED);
				LOGGER.debug("dMaxOrderStatus:::" + dMaxOrderStatus);
				if (dMaxOrderStatus >= dReleasedStatus
						&& dMaxOrderStatus <= dShippedStatus
						&& !GCConstants.ITEM_TYPE_03
								.equalsIgnoreCase(sItemType)) {
					YFCException yfs = new YFCException(
							GCErrorConstants.ITEM_CANNOT_BE_ADDED_ERROR_CODE);
					yfs.setErrorDescription(GCErrorConstants.ITEM_CANNOT_BE_ADDED_ERROR_DESC);
					throw yfs;
				}
				if(!YFCCommon.isVoid(storeUser) && YFCCommon.equals(storeUser, "Y") && dMaxOrderStatus >= dCreatedStatus)
				{
				
					YFCException yfs = new YFCException(
							GCErrorConstants.ITEM_CANNOT_BE_ADDED_ERROR_CODE);
					yfs.setErrorDescription(GCErrorConstants.ITEM_CANNOT_BE_ADDED_ERROR_DESC);
					throw yfs;
					
				}
			}

			if (GCConstants.FLAG_Y.equalsIgnoreCase(sIsOrderAmend)) {

				processItemInCaseOfOrderAmendmends(env, docGetCompItmLstOut,
						inDoc);
			}
			
			updateOnHandDCandStores(env,docGetCompItmLstOut);
			
			LOGGER.debug("Return  Doc"
					+ GCXMLUtil.getXMLString(docGetCompItmLstOut));
			LOGGER.debug("Class: GCGetItemAvailability Method: getAvailableInventory END");
			return docGetCompItmLstOut;
		} catch (Exception e) {
			LOGGER.error("The exception is in getAvailableInventory method:", e);
			throw GCException.getYFCException(e.getMessage(), e);
		}


	}
	
	/**
	 * GCSTORE - 4828
	 * This method will stamp the outdoc with OnHandAvailability of Stores and DC separately.
	 * Call GCGetItemAvailabilityOnProductDetail service to achieve the same 
	 *
	 * @param sReqShipDate
	 * @throws ParseException
	 */

	private void updateOnHandDCandStores(YFSEnvironment env,
			Document docGetCompItmLstOut) {
		// 
		LOGGER.verbose("updateOnHandDCandStores method: BEGIN");
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Incoming XML to updateOnHandDCandStores " + GCXMLUtil.getXMLString(docGetCompItmLstOut));
		}
		
		//int onHandQtyStore = 0;
		//Added for 3706--Start
		String poPlacedAvl = GCConstants.ZERO;
		String firstFinalAvailDate = GCConstants.BLANK_STRING;
		//End
		String onHandQtyDC = GCConstants.ZERO;
		String onHandQtyStore = GCConstants.ZERO;
		String onHandDropShip = GCConstants.ZERO;
		String nonPickable = GCConstants.ZERO;
		String callingOrgCode = GCXMLUtil.getAttributeFromXPath(docGetCompItmLstOut,
				"/ItemList/@CallingOrganizationCode");
		String itemID = GCXMLUtil.getAttributeFromXPath(docGetCompItmLstOut,
				"/ItemList/Item/@ItemID");
		String unitOfMeasure = GCXMLUtil.getAttributeFromXPath(docGetCompItmLstOut,
				"/ItemList/Item/@UnitOfMeasure");
		
		YFCDocument inDocGetAvailability = YFCDocument.createDocument(GCConstants.ITEM);
	    YFCElement eleItem = inDocGetAvailability.getDocumentElement();
	    eleItem.setAttribute(GCConstants.CALLING_ORGANIZATION_CODE, callingOrgCode);
	    eleItem.setAttribute(GCConstants.ITEM_ID, itemID);
	    eleItem.setAttribute(GCConstants.UOM, unitOfMeasure);
	    		
		YFCDocument docOutGetAvailability = GCCommonUtil.invokeService(env,
				"GCGetItemAvailabilityOnProductDetail", inDocGetAvailability);
		
		YFCElement eleItemListOut = docOutGetAvailability.getDocumentElement();
		YFCElement eleItemOut = eleItemListOut.getChildElement("Item");
		YFCElement eleAvailOut = eleItemOut.getChildElement("Availability");
		if(!(YFCCommon.isVoid(eleAvailOut))){
			onHandQtyDC = eleAvailOut.getAttribute("OnhandAvailableQuantityDC");
			onHandQtyStore = eleAvailOut.getAttribute("OnhandAvailableQuantityStore");
			onHandDropShip = eleAvailOut.getAttribute("OnhandAvailableDropShipQuantity");
			nonPickable = eleAvailOut.getAttribute("NonPickableQty"); // GCSTORE 5553
			//Added for 3706
			poPlacedAvl = eleAvailOut.getAttribute("FutureAvailableQuantity");
			firstFinalAvailDate = eleAvailOut.getAttribute("FirstFutureAvailableDate");
		    //End
}
		
				
		Element eleItemAvail = GCXMLUtil.getElementByXPath(docGetCompItmLstOut,"/ItemList/Item/Availability");
		if(!(YFCCommon.isVoid(eleItemAvail))){
			eleItemAvail.setAttribute("OnhandAvailableQuantityDC",onHandQtyDC);
			eleItemAvail.setAttribute("OnhandAvailableQuantityStore",onHandQtyStore);
			eleItemAvail.setAttribute("OnhandAvailableDropShipQuantity",onHandDropShip);
			eleItemAvail.setAttribute("NonPickableQty",nonPickable); // GCSTORE 5553
			//Added for 3706
            eleItemAvail.setAttribute("FutureAvailableQuantity", poPlacedAvl);
			eleItemAvail.setAttribute("FirstFutureAvailableDate", firstFinalAvailDate);
		    //End
        }
		LOGGER.verbose("updateOnHandDCandStores method: END");
	}

	/**
	 * This method is used to check if future ship date is stamped on the order.
	 * If yes, single source items will not be allowed to be added to the order
	 *
	 * @param sReqShipDate
	 * @throws ParseException
	 */
	private void checkIfFutureShipDateIsStamped(String sReqShipDate) {

		SimpleDateFormat sdf = new SimpleDateFormat(GCConstants.DATE_FORMAT);
		Calendar cCurrentDate = Calendar.getInstance();
		cCurrentDate.setTime(new Date());

		String sCurrentDate = sdf.format(cCurrentDate.getTime());
		Date dCurrentDate = null;
		Date dReqShipdate = null;
		try {
			dCurrentDate = sdf.parse(sCurrentDate);
			dReqShipdate = sdf.parse(sReqShipDate);
		} catch (ParseException yfsEx) {
			YFSException e = new YFSException(yfsEx.getMessage());
			LOGGER.error(
					"Date parsing failed in checkIfFutureShipDateIsStamped method",
					yfsEx);
			throw e;
		}

		if (dReqShipdate.after(dCurrentDate)) {
			YFCException yfs = new YFCException(
					GCErrorConstants.SINGLE_SOURCE_FUTURE_DATE_ADD_PROD);
			yfs.setErrorDescription(GCErrorConstants.SINGLE_SOURCE_FUTURE_DATE_ADD_PROD_DESC);
			throw yfs;
		}

	}


	/**
	 *
	 * @param env
	 * @param docGetCompItmLstOut
	 * @param inDoc
	 */
	private void processItemInCaseOfOrderAmendmends(YFSEnvironment env,
			Document docGetCompItmLstOut, Document inDoc) throws GCException {

		try {
			LOGGER.debug("Inside Order Amendment Logic");
			String sItemType = GCXMLUtil.getAttributeFromXPath(
					docGetCompItmLstOut, "//Item/PrimaryInformation/@ItemType");
			String sLevelOfService = GCXMLUtil.getAttributeFromXPath(inDoc,
					"//Item/@LevelOfService");
			Element eleOutRoot = docGetCompItmLstOut.getDocumentElement();
			String sIsOver70Pound = "";
			String sIsGiftCertificateValid = "";

			if (GCConstants.ITEM_TYPE_03.equalsIgnoreCase(sItemType)) {
				eleOutRoot.setAttribute("IsItemEligibleForAddition",
						GCConstants.FLAG_Y);
			} else {
				String sIsHazmat = GCXMLUtil.getAttributeFromXPath(
						docGetCompItmLstOut,
						"//Item/PrimaryInformation/@IsHazmat");
				String sUnitWeight = GCXMLUtil.getAttributeFromXPath(
						docGetCompItmLstOut,
						"//Item/PrimaryInformation/@UnitWeight");
				if (YFCCommon.isVoid(sUnitWeight)) {
					sUnitWeight = "0";
				}
				Double dUnitWeight = Double.parseDouble(sUnitWeight);
				String sAllowTruckItem = GCXMLUtil.getAttributeFromXPath(
						docGetCompItmLstOut, "//Item/Extn/@ExtnIsTruckShip");

				if ("09".equalsIgnoreCase(sItemType)) {
					sIsGiftCertificateValid = GCConstants.FLAG_Y;
				} else {
					sIsGiftCertificateValid = GCConstants.FLAG_N;
				}

				if (dUnitWeight > 70) {
					sIsOver70Pound = GCConstants.FLAG_Y;
				} else {
					sIsOver70Pound = GCConstants.FLAG_N;
				}

				Document docInShippingFlag = GCXMLUtil
						.createDocument(GCConstants.GC_SHIPPING_LEVEL_FLAG);
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("In Doc to GCGetShippingLevelFlag Service"
							+ GCXMLUtil.getXMLString(docInShippingFlag));
				}
				Document docOutShippingFlag = GCCommonUtil.invokeService(env,
						GCConstants.GC_GET_SHIPPING_LEVEL_FLAG,
						docInShippingFlag);
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("Out Doc to GCGetShippingLevelFlag Service"
							+ GCXMLUtil.getXMLString(docOutShippingFlag));
				}
				boolean bIsItemEligibleForAddition = false;
				Element eleGCShipLevelFlag = null;
				if (GCConstants.YES.equals(sIsGiftCertificateValid)) {
					eleGCShipLevelFlag = GCXMLUtil.getElementByXPath(
							docOutShippingFlag,
							"GCShippingLevelFlagList/GCShippingLevelFlag[@LevelOfService='"
									+ sLevelOfService
									+ "' and @GiftCertificateValid='"
									+ sIsGiftCertificateValid + "']");
				} else if (GCConstants.YES.equals(sIsHazmat)) {
					eleGCShipLevelFlag = GCXMLUtil.getElementByXPath(
							docOutShippingFlag,
							"GCShippingLevelFlagList/GCShippingLevelFlag[@LevelOfService='"
									+ sLevelOfService + "' and @Hazmat='"
									+ sIsHazmat + "']");
				} else if (GCConstants.YES.equals(sAllowTruckItem)) {
					// Apply truck ship restriction
					eleGCShipLevelFlag = GCXMLUtil.getElementByXPath(
							docOutShippingFlag,
							"GCShippingLevelFlagList/GCShippingLevelFlag[@LevelOfService='"
									+ sLevelOfService
									+ "' and @AllowTruckItem='"
									+ sAllowTruckItem + "']");

				} else if (GCConstants.YES.equals(sIsOver70Pound)) {
					eleGCShipLevelFlag = GCXMLUtil.getElementByXPath(
							docOutShippingFlag,
							"GCShippingLevelFlagList/GCShippingLevelFlag[@LevelOfService='"
									+ sLevelOfService + "' and @Over70Pound='"
									+ sIsOver70Pound + "']");
				} else {
					bIsItemEligibleForAddition = true;
				}

				if (!YFCCommon.isVoid(eleGCShipLevelFlag)
						|| bIsItemEligibleForAddition) {
					eleOutRoot.setAttribute(GCConstants.IS_ITEM_ADDITION,
							GCConstants.FLAG_Y);
				} else {
					eleOutRoot.setAttribute(GCConstants.IS_ITEM_ADDITION,
							GCConstants.FLAG_N);
					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("Return Doc in case Item not eligible"
								+ GCXMLUtil.getXMLString(docGetCompItmLstOut));
					}
				}

			}
		} catch (Exception e) {
			LOGGER.error(
					"The exception is in processItemInCaseOfOrderAmendmends method:",
					e);
			throw new GCException(e);
		}
	}

	/**
	 * @author zuzarinder.singh
	 * @param env
	 * @param sStoreId
	 * @param sExtnBrandId
	 * @param sItemID
	 *            -->This method takes the parameters and calls the extended DB
	 *            API to fetch the records for Store-Vendor Entitlement from
	 *            Custom table "GC_VENDOR_PRODUCT_ENTITLEMENT"
	 */
	// --GCSTORE-194:Start
  public static void isItemSellableFromLoggedInStore(YFSEnvironment env,
			String sStoreId, String sExtnBrandId, String sItemID) {
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose(" StoreId:" + sStoreId + " ExtnBrandId:"
					+ sExtnBrandId + " ItemID: " + sItemID);
		}

		// create input Document for extendedDBApi
		YFCDocument getGCVendorProductEntitlementListInDoc = YFCDocument
				.createDocument("GetGCVendorProductEntitlement");
		YFCElement eleGCVendorProductEntitlement = getGCVendorProductEntitlementListInDoc
				.getDocumentElement();
		eleGCVendorProductEntitlement.setAttribute("StoreId", sStoreId);
		eleGCVendorProductEntitlement.setAttribute("BrandId", sExtnBrandId);
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("getGCVendorProductEntitlementListInDoc:"
					+ getGCVendorProductEntitlementListInDoc.toString());
		}

		YFCDocument getGCVendorProductEntitlementListOutDoc = GCCommonUtil
				.invokeService(env, "GCGetVendorProductEntitlementList",
						getGCVendorProductEntitlementListInDoc);
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("getGCVendorProductEntitlementListOutDoc:"
					+ getGCVendorProductEntitlementListOutDoc.toString());
		}
		YFCElement eleGCVendorProductEntitlementList = getGCVendorProductEntitlementListOutDoc
				.getDocumentElement();
		YFCNodeList<YFCElement> nlGCVendorProductEntitlementList = eleGCVendorProductEntitlementList
				.getElementsByTagName("GCVendorProductEntitlement");
		if (nlGCVendorProductEntitlementList.getLength() != 0) {
			// even if one record is returned, that means item cannnot be sold
			// from corresponding logged in store as per Store Vendor
			// Entitlements
			YFCException yfcExcepStoreEntitlement = new YFCException(
					GCErrorConstants.EXTN_GCE00101);
			yfcExcepStoreEntitlement.setErrorDescription(sItemID
					+ " belongs to brand " + sExtnBrandId
					+ " which is not allowed to be sold from " + sStoreId);
throw yfcExcepStoreEntitlement;
		}
	}
	// --GCSTORE-194:End
}