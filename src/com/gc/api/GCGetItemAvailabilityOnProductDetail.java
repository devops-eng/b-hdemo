/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 * *###########################################
 * ######################################################
 * ################################################################ OBJECTIVE: This class is used to
 * know the availability of an item on Product detail Page.
 * ################################################################
 * #################################################################################################
 * Version Date Modified By Description
 * #############################################################
 * #####################################
 * ############################################################### 1.0 03/12/2015 Jethuri, Bhawana
 * JIRA No 5533/5194: This class is used to know the availability of an item.
 * ######################################
 * ############################################################
 * ###############################################################
 *  * JIRA No 6762: 05/01/2017 Mishra, Deepak This is to fix for SET item.
 * ######################################
 * ############################################################
 * ###############################################################
 *  * JIRA No 3706: 14/04/2017 Mishra, Deepak Fix for PO
 * ######################################
 * ############################################################
 * ###############################################################
 */
package com.gc.api;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:email Address">Naveen</a>
 */
public class GCGetItemAvailabilityOnProductDetail {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCGetItemAvailabilityOnProductDetail.class
      .getName());
  int onHandAvDC = 0;
  int onHandAvStore = 0;
  int onHandAvDropShip=0;
  int onHandAv = 0;
  int nonPickable = 0;

  /**
   * 
   * Description of getAvailableInventory - This method is used to check if inventory is available
   * on Product Detail Page.
   * 
   * @param env
   * @param inDoc
   * @return
   * @throws Exception
   * 
   */
  public Document getAvailableInventory(YFSEnvironment env, Document inDoc) throws GCException {


    try {
      LOGGER.debug("Class: GCGetItemAvailabilityOnProductDetail Method: getAvailableInventory BEGIN");
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Incoming XML:getCompleteList Indoc" + GCXMLUtil.getXMLString(inDoc));
      }
      
      Element eleItemInput = inDoc.getDocumentElement();
      eleItemInput.setAttribute("GetAvailabilityFromCache", "Y");
      
      if(LOGGER.isDebugEnabled()){
    	  LOGGER.debug("Input to getCompleteItemList" + GCXMLUtil.getXMLString(inDoc));
      }


      Document docGetCompItmLstOut =
          GCCommonUtil.invokeAPI(env, inDoc, GCConstants.API_GET_COMPLETE_ITEM_LIST,
              "/global/template/api/getCompleteItemList_ProductDetail.xml");
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("getCompleteItemList Output" + GCXMLUtil.getXMLString(docGetCompItmLstOut));
      }


      Element eleItem = GCXMLUtil.getElementByXPath(docGetCompItmLstOut, "//Item");
      if (YFCCommon.isVoid(eleItem)) {
        YFCException yfs = new YFCException(GCErrorConstants.INVALID_ITEM_ERROR_CODE);
        yfs.setErrorDescription(GCErrorConstants.INVALID_ITEM_ERROR_DESCRIPTION);
        throw yfs;
      }

      String sProdClass =
          GCXMLUtil.getAttributeFromXPath(docGetCompItmLstOut, "//Item/PrimaryInformation/@DefaultProductClass");
      String sKitCode = GCXMLUtil.getAttributeFromXPath(docGetCompItmLstOut, "//Item/PrimaryInformation/@KitCode");
      String sItemID = eleItem.getAttribute(GCConstants.ITEM_ID);
      String sOrganizationCode = eleItem.getAttribute(GCConstants.ORGANIZATION_CODE);
      String sUOM = eleItem.getAttribute(GCConstants.UNIT_OF_MEASURE);
      YFCDocument getSupplyDetailsinDoc = YFCDocument.createDocument(GCConstants.GET_SUPPLY_DETAILS);
      YFCElement eleGetSupplyDetails = getSupplyDetailsinDoc.getDocumentElement();
      eleGetSupplyDetails.setAttribute(GCConstants.ORGANIZATION_CODE, sOrganizationCode);
      eleGetSupplyDetails.setAttribute(GCConstants.PRODUCT_CLASS, sProdClass);
      eleGetSupplyDetails.setAttribute(GCConstants.UNIT_OF_MEASURE, sUOM);
      Element eleAvailability = (Element) eleItem.getElementsByTagName(GCConstants.AVAILABILITY).item(0);

      if(!YFCUtils.equals(sKitCode, GCConstants.BUNDLE)){
        eleGetSupplyDetails.setAttribute(GCConstants.ITEM_ID, sItemID);
        //Added for 3706--Start
        stampAvlQtyAndDateForPO(env,eleItem,eleAvailability,sKitCode,new ArrayList<Element>());
        //Added for 3706--End
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("Get Supply Details InDoc :" + getSupplyDetailsinDoc.getString());
        }
        YFCDocument getSupplyDetailsTemplate = YFCDocument.getDocumentFor(GCConstants.GET_SUPPLY_DETAILS_TEMPLATE);
        YFCDocument outDoc =
            GCCommonUtil.invokeAPI(env, GCConstants.GET_SUPPLY_DETAILS, getSupplyDetailsinDoc, getSupplyDetailsTemplate);
        Document getSupplyDoc = outDoc.getDocument();

        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("Get Supply Details OutDoc :" + outDoc.getString());
        }


        List<Element> listShipNode = GCXMLUtil.getElementListByXpath(getSupplyDoc, "Item/ShipNodes/ShipNode");
        if (listShipNode.size() > 0) {
          for (int i = 0; i < listShipNode.size(); i++) {
            Element eleShipNode = listShipNode.get(i);
            String tracked = eleShipNode.getAttribute("Tracked");
            if (YFCUtils.equals(GCConstants.YES, tracked)) {
              Element eleSupplyNonPick =
                  (Element) XPathAPI.selectSingleNode(eleShipNode, "Supplies/Supply[@SupplyType='NON_PICKABLE.ex']");
              if (!YFCCommon.isVoid(eleSupplyNonPick)) {
                String nonPickableQty = eleSupplyNonPick.getAttribute("AvailableQty");
                nonPickable = nonPickable + Integer.parseInt(nonPickableQty);
              }
              Element eleSupplyOnHand =
                  (Element) XPathAPI.selectSingleNode(eleShipNode, "Supplies/Supply[@SupplyType='ONHAND']");
              if (!YFCCommon.isVoid(eleSupplyOnHand)) {
                String onHandQty = eleSupplyOnHand.getAttribute("AvailableQty");
                onHandAv = onHandAv + Integer.parseInt(onHandQty);
              }
			  String shipNode = eleShipNode.getAttribute("ShipNode");
			  if (YFCUtils.equals(shipNode, "MFI")) {
				if (!YFCCommon.isVoid(eleSupplyOnHand)) {
					String onHandQtyDC = "0";
					onHandQtyDC = eleSupplyOnHand.getAttribute("AvailableQty");
					onHandAvDC += Integer.parseInt(onHandQtyDC);
				}
              }else if (YFCUtils.equals(shipNode, "GCVendorNode")) {
            	  if (!YFCCommon.isVoid(eleSupplyOnHand)) {
            		String OnhandDropShipQuantity = "0";
            		OnhandDropShipQuantity = eleSupplyOnHand.getAttribute("AvailableQty");
  					onHandAvDropShip += Integer.parseInt(OnhandDropShipQuantity);
            	  }
              }else{
            	  if (!YFCCommon.isVoid(eleSupplyOnHand)) {
            		String onHandQtyStore = "0";
  					onHandQtyStore = eleSupplyOnHand.getAttribute("AvailableQty");
  					int onHandQtyStoreSafetyFac = getOnHandAfterSafetyFactor(env, Integer.parseInt(onHandQtyStore),shipNode, 
  	              			sOrganizationCode, sItemID, sProdClass, sUOM);
  					onHandAvStore += onHandQtyStoreSafetyFac;
            	  }
              }
			  
            }
          }
        }
        
      
      
      LOGGER.debug("Not a bundle Item onHandAvStore= "+onHandAvStore+"onHandAv= "+onHandAv+"onHandAvDC= "+onHandAvDC+"onHandAvDropShip= "+onHandAvDropShip);
      }

      else {

        List<Element> listComponent =
            GCXMLUtil.getElementListByXpath(docGetCompItmLstOut, "ItemList/Item/Components/Component");
        
        getAvailabilityForBundle(listComponent, getSupplyDetailsinDoc, env, sProdClass,sOrganizationCode,docGetCompItmLstOut);
        //Added for 3706--Start
        stampAvlQtyAndDateForPO(env,eleItem,eleAvailability,sKitCode,listComponent);
        //Added for 3706--End
      }
      
      
      if (!YFCCommon.isVoid(eleAvailability)) {
    	  
    	 
        eleAvailability.setAttribute("OnhandAvailableQuantity", Integer.toString(onHandAv));	
		eleAvailability.setAttribute("OnhandAvailableQuantityStore", Integer.toString(onHandAvStore));
		eleAvailability.setAttribute("OnhandAvailableDropShipQuantity", Integer.toString(onHandAvDropShip));
        eleAvailability.setAttribute("OnhandAvailableQuantityDC", Integer.toString(onHandAvDC));
		eleAvailability.setAttribute("NonPickableQty", Integer.toString(nonPickable));

      }


      if(LOGGER.isDebugEnabled()){
    	  LOGGER.debug("Return  Doc of GCGetItemAvailabilityOnProductDetail" + GCXMLUtil.getXMLString(docGetCompItmLstOut));
      }
      LOGGER.debug("Class: GCGetItemAvailabilityOnProductDetail Method: getAvailableInventory END");
      return docGetCompItmLstOut;
    } catch (Exception e) {
      LOGGER.error("The exception is in getAvailableInventory method:", e);
      throw GCException.getYFCException(e.getMessage(), e);
    }
    
  }

  /**
   * 
   * Description of getAvailableInventory - This method is used to set future available date and quantity
   * on basis of use unplanned inventory and default date received.
   * @param eleItem
   * @param eleAvailability
   * @param sKitCode
   * @param listComponent
   * @return
 * @throws ParseException 
   * @throws Exception
   * GCSTORE-3706--Start
   */
private void stampAvlQtyAndDateForPO(YFSEnvironment env,Element eleItem, Element eleAvailability,
		String sKitCode, List<Element> listComponent) throws ParseException {
	String sUseUnplannedInv = "";
	String sLeadTime = "";
	//Setting future available date and quantity for Regular item
	if(!YFCCommon.isVoid(eleAvailability)){
	if(!YFCUtils.equals(sKitCode, GCConstants.BUNDLE)){
		Element eleInvParameter = (Element) eleItem.getElementsByTagName(GCConstants.INVENTORY_PARAMETERS).item(0);
	    sUseUnplannedInv = eleInvParameter.getAttribute(GCConstants.USE_UNPLANNED_INVENTORY);
		sLeadTime = eleInvParameter.getAttribute(GCConstants.LEAD_TIME);
		if(GCConstants.NO.equalsIgnoreCase(sUseUnplannedInv) || (GCConstants.BLANK_STRING.equalsIgnoreCase(sUseUnplannedInv))){
			eleAvailability.setAttribute("FutureAvailableQuantity", GCConstants.ZERO);
			eleAvailability.setAttribute("FirstFutureAvailableDate", GCConstants.BLANK_STRING);
		}
		else if (GCConstants.YES.equalsIgnoreCase(sUseUnplannedInv))
		{
			settingAvlDateAndTimeForJunkValuesPO(env,sLeadTime,eleAvailability);
		}
	}//End
	//Setting future available date and quantity for Bundle items
	else{
		for(int i=0;i<listComponent.size();i++){
			Element eleItemComp = listComponent.get(i);
			Element seleInvParameterForComp = (Element) eleItemComp.getElementsByTagName(GCConstants.INVENTORY_PARAMETERS).item(0);
			String sUseUnplannedInvForComp = seleInvParameterForComp.getAttribute(GCConstants.USE_UNPLANNED_INVENTORY);
			String tempLeadTime = seleInvParameterForComp.getAttribute(GCConstants.LEAD_TIME);
			if(i==0){
				sLeadTime = tempLeadTime;
			}else{
			if(tempLeadTime.compareTo(sLeadTime)>0){
				sLeadTime = tempLeadTime;
			}
			}
			if(GCConstants.NO.equalsIgnoreCase(sUseUnplannedInvForComp) || (GCConstants.BLANK_STRING.equalsIgnoreCase(sUseUnplannedInvForComp))){
				eleAvailability.setAttribute("FutureAvailableQuantity", GCConstants.ZERO);
				eleAvailability.setAttribute("FirstFutureAvailableDate", GCConstants.BLANK_STRING);
				break;
			}
			else if (GCConstants.YES.equalsIgnoreCase(sUseUnplannedInvForComp) && GCConstants.BUNDLE.equals(sKitCode))
			{
				settingAvlDateAndTimeForJunkValuesPO(env,sLeadTime,eleAvailability);
				sKitCode="";
			}
		}
	}
	}//End
}
//GCSTORE-3706--End

/**
 * 
 * Description of getAvailableInventory - This method is used to get the data to set for PO available date and quantity
 * @param env
 * @param sLeadTime
 * @param eleAvailability
 * @return
 * @throws ParseException 
 * @throws Exception
 * GCSTORE-3706--Start
 */
private void settingAvlDateAndTimeForJunkValuesPO(YFSEnvironment env, String sLeadTime,Element eleAvailability) throws ParseException {
	String sSysTime = GCDateUtils.getCurrentTime(GCConstants.YYYY_MM_DD);
	SimpleDateFormat sdf = new SimpleDateFormat(GCConstants.YYYY_MM_DD);
	String sFirstFutureAvlDate = eleAvailability.getAttribute(GCConstants.FIRST_FUTURE_AVAILABLE_DATE);
	String codeType = GCConstants.EXTENDED_DAYS_FOR_FUTURE_PO;
	if(GCConstants.DEFALUT_DATE.equalsIgnoreCase(sFirstFutureAvlDate)){
		String codeValue = GCConstants.CODE_VAULE_FOR_INVALID_DATE;
		Integer extendedDays = callCommonCodeToGetNumberOfDaysAdded(env,codeType,codeValue);
		int iExtnOriginalLeadTime = Integer.parseInt(sLeadTime);
		 iExtnOriginalLeadTime = iExtnOriginalLeadTime+extendedDays;
		 String sFutrAvlDate = GCDateUtils.addDays(sSysTime, iExtnOriginalLeadTime, 1);
		 Date dSysDate = sdf.parse(sFutrAvlDate);
	     String strFirstFutDate = GCDateUtils.formatDate(dSysDate, GCConstants.YYYY_MM_DD);
         eleAvailability.setAttribute(GCConstants.FIRST_FUTURE_AVAILABLE_DATE, strFirstFutDate);
	}else{
		String codeValue = GCConstants.CODE_VALUE_FOR_VALID_DATE;
		Integer extendedDays = callCommonCodeToGetNumberOfDaysAdded(env,codeType,codeValue);
		String sFutAvlDate = GCDateUtils.addDays(sFirstFutureAvlDate, extendedDays, 1);
        Date dSysDate = sdf.parse(sFutAvlDate);
        String strFirstFutDate = GCDateUtils.formatDate(dSysDate, GCConstants.YYYY_MM_DD);
        eleAvailability.setAttribute(GCConstants.FIRST_FUTURE_AVAILABLE_DATE, strFirstFutDate);
	}
	
}

//GCSTORE-3706--End

private Integer callCommonCodeToGetNumberOfDaysAdded(YFSEnvironment env,String codeType,String codeValue) {
	//Common code to fetch extended number of days - //GCSTORE - 3706 --Start
  	Integer extendedDays = 0;
      YFCDocument getCommonCodeListOutForItemType = GCCommonUtil
              .getCommonCodeListByTypeAndValue(
              		env,codeType,GCConstants.GC, codeValue);
      if(!YFCCommon.isVoid(getCommonCodeListOutForItemType)){
      Document outDoc = getCommonCodeListOutForItemType.getDocument();
      Element eleCommonCode = GCXMLUtil.getElementByXPath(outDoc,"/CommonCodeList/CommonCode");
      if(!YFCCommon.isVoid(eleCommonCode)){
      extendedDays = Integer.parseInt(eleCommonCode.getAttribute(GCConstants.CODE_SHORT_DESCRIPTION));
      }else{
    	  extendedDays=0; //stamping 0 days
      }
      }
     return extendedDays; 
}
	//GCSTORE - 3706 --End

private void getAvailabilityForBundle(List<Element> listComponent,
      YFCDocument getSupplyDetailsinDoc, YFSEnvironment env, String sProdClass, String sOrganizationCode, Document docGetCompItmLstOut) throws TransformerException, ParserConfigurationException, SAXException, IOException {

    Map<String, Integer> shipNodeQtyMapNp = new HashMap<String, Integer>();
    Map<String, Integer> shipNodeQtyMapOn = new HashMap<String, Integer>();
    Element eleItemAvail = GCXMLUtil.getElementByXPath(docGetCompItmLstOut,"/ItemList/Item/Availability");
    Element sPrimaryInformation= GCXMLUtil.getElementByXPath(docGetCompItmLstOut, "/ItemList/Item/PrimaryInformation");
    String sProductClass = sPrimaryInformation.getAttribute("DefaultProductClass");
    
    for (int i = 0; i < listComponent.size(); i++) {
      Element eleComponent = listComponent.get(i);
      int kitQty = Integer.parseInt(eleComponent.getAttribute(GCConstants.KIT_QUANTITY));
      Element eleItem = (Element) eleComponent.getElementsByTagName(GCConstants.ITEM).item(0);
      if (!YFCCommon.isVoid(eleItem)) {
        String itemID = eleItem.getAttribute(GCConstants.ITEM_ID);
        String sUOM = eleItem.getAttribute(GCConstants.UNIT_OF_MEASURE);
        YFCElement eleGetSupplyDetails = getSupplyDetailsinDoc.getDocumentElement();
        eleGetSupplyDetails.setAttribute(GCConstants.ITEM_ID, itemID);
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("Get Supply Details InDoc :" + getSupplyDetailsinDoc.getString());
        }
        YFCDocument getSupplyDetailsTemplate = YFCDocument.getDocumentFor(GCConstants.GET_SUPPLY_DETAILS_TEMPLATE);
        YFCDocument outDoc =
            GCCommonUtil
            .invokeAPI(env, GCConstants.GET_SUPPLY_DETAILS, getSupplyDetailsinDoc, getSupplyDetailsTemplate);
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("Get Supply Details OutDoc for Component:" + itemID + outDoc.getString());
        }
        Document getSupplyDoc = outDoc.getDocument();
        if (YFCUtils.equals(sProdClass, GCConstants.REGULAR)) {
          int onHandQty =0;
          int onHandQtyComp = 0;
          int nonPickableQtyComp = 0;
          Element eleShipNode = GCXMLUtil.getElementByXPath(getSupplyDoc, "Item/ShipNodes/ShipNode[@ShipNode='MFI']");
          if (!YFCCommon.isVoid(eleShipNode)) {
            Element eleSupplyOnHand =
                (Element) XPathAPI.selectSingleNode(eleShipNode, "Supplies/Supply[@SupplyType='ONHAND']");
            if (!YFCCommon.isVoid(eleSupplyOnHand)) {
            	onHandQty = Integer.parseInt(eleSupplyOnHand.getAttribute("AvailableQty"));
            	onHandQtyComp = onHandQty / kitQty;
            }

            Element eleSupplyNonPick =
                (Element) XPathAPI.selectSingleNode(eleShipNode, "Supplies/Supply[@SupplyType='NON_PICKABLE.ex']");
            if (!YFCCommon.isVoid(eleSupplyNonPick)) {
              nonPickableQtyComp = Integer.parseInt(eleSupplyNonPick.getAttribute("AvailableQty")) / kitQty;
            }
          }
          if (i == 0) {
            onHandAv = onHandQtyComp;
            nonPickable = nonPickableQtyComp;
          } else {
            onHandAv = Math.min(onHandQtyComp, onHandAv);
            nonPickable = Math.min(nonPickableQtyComp, nonPickable);
			onHandAvStore = 0;
			onHandAvDC = onHandAv;
			
			
          }
          if (onHandAv == 0 && nonPickable == 0) {
            break;
          }
        }
        else if ((YFCUtils.equals(sProdClass, GCConstants.SET))) {
          List<Element> listShipNode = GCXMLUtil.getElementListByXpath(getSupplyDoc, "Item/ShipNodes/ShipNode");
          int onHandQtySafetyFac = 0;
          if (i == 0) {
            for (int j = 0; j < listShipNode.size(); j++) {
              Element eleShipNode = listShipNode.get(j);
              String shipNode = eleShipNode.getAttribute(GCConstants.SHIP_NODE);
              String tracked = eleShipNode.getAttribute("Tracked");
              if (YFCUtils.equals(GCConstants.YES, tracked)) {
                Element eleSupplyNonPick =
                    (Element) XPathAPI.selectSingleNode(eleShipNode, "Supplies/Supply[@SupplyType='NON_PICKABLE.ex']");
                if (!YFCCommon.isVoid(eleSupplyNonPick)) {
                  String sNonPickableQty = eleSupplyNonPick.getAttribute("AvailableQty");
                  int nonPickableQty = Integer.parseInt(sNonPickableQty) / kitQty;
                  shipNodeQtyMapNp.put(shipNode, nonPickableQty);
                }
                Element eleSupplyOnHand =
                    (Element) XPathAPI.selectSingleNode(eleShipNode, "Supplies/Supply[@SupplyType='ONHAND']");
                String sOnHandQty = GCConstants.ZERO;
                //Fix for 6762
                String sSoftMatchedDemand = GCConstants.ZERO;
                int iSoftMatchDemand = 0;
                //End
                if (!YFCCommon.isVoid(eleSupplyOnHand)) {
                  sOnHandQty = eleSupplyOnHand.getAttribute("AvailableQty");
                //Fix for 6762
                  Element eleSoftMatchedDemand = (Element) eleSupplyOnHand.getElementsByTagName(GCConstants.SOFT_MATCH_DEMAND).item(0);
                  if(!YFCCommon.isVoid(eleSoftMatchedDemand)){
                	  sSoftMatchedDemand =  eleSoftMatchedDemand.getAttribute("TotalQuantity"); 
                	  iSoftMatchDemand = Integer.parseInt(sSoftMatchedDemand);
                  }
                  //End
                  int onHandQty = Integer.parseInt(sOnHandQty); 
                  //Fix for 6762
                  onHandQty = onHandQty - iSoftMatchDemand;
                  //End
                  onHandQtySafetyFac = getOnHandAfterSafetyFactor(env, onHandQty,shipNode, 
              			sOrganizationCode, itemID, sProductClass, sUOM);
                  int onHndQty = onHandQtySafetyFac / kitQty;
                  shipNodeQtyMapOn.put(shipNode, onHndQty);
                }
              }
            }
          } else {
            for (Iterator<Entry<String, Integer>> entry2 = shipNodeQtyMapNp.entrySet().iterator(); entry2.hasNext();) {
              Entry<String, Integer> e = entry2.next();
              String shipNode = e.getKey();
              Element eleShipNode =
                  GCXMLUtil.getElementByXPath(getSupplyDoc, "Item/ShipNodes/ShipNode[@ShipNode='" + shipNode + "']");
              if (!YFCCommon.isVoid(eleShipNode)) {
                String tracked = eleShipNode.getAttribute("Tracked");
                if (YFCUtils.equals(tracked, GCConstants.YES)) {
                  Element eleSupplyNonPick =
                      GCXMLUtil.getElementByXPath(getSupplyDoc, "Item/ShipNodes/ShipNode[@ShipNode='" + shipNode
                          + "']/Supplies/Supply[@SupplyType='NON_PICKABLE.ex']");
                  if (!YFCCommon.isVoid(eleSupplyNonPick)) {
                    String sNonPickableQty = eleSupplyNonPick.getAttribute("AvailableQty");
                    int nonPickableQty = Integer.parseInt(sNonPickableQty) / kitQty;
                    int npMapQty = e.getValue();
                    shipNodeQtyMapNp.put(shipNode, Math.min(nonPickableQty, npMapQty));
                  } else {
                    entry2.remove();
                  }
                }
              }
            }
            for (Iterator<Entry<String, Integer>> entry3 = shipNodeQtyMapOn.entrySet().iterator(); entry3.hasNext();) {
              Entry<String, Integer> e2 = entry3.next();
              String shipNode = e2.getKey();
              Element eleShipNode =
                  GCXMLUtil.getElementByXPath(getSupplyDoc, "Item/ShipNodes/ShipNode[@ShipNode='" + shipNode + "']");
              if (!YFCCommon.isVoid(eleShipNode)) {
                String tracked = eleShipNode.getAttribute("Tracked");
                if (YFCUtils.equals(tracked, GCConstants.YES)) {
                  Element eleSupplyOnHand =
                      GCXMLUtil.getElementByXPath(getSupplyDoc, "Item/ShipNodes/ShipNode[@ShipNode='" + shipNode
                          + "']/Supplies/Supply[@SupplyType='ONHAND']");
                  //Fix for 6762
                  String sSoftMatchedDemand = GCConstants.ZERO;
                  int iSoftMatchDemand = 0;
                  //End
                  if (!YFCCommon.isVoid(eleSupplyOnHand)) {
                    String sOnHandQty = eleSupplyOnHand.getAttribute("AvailableQty");
                  //Fix for 6762
                    Element eleSoftMatchedDemand = (Element) eleSupplyOnHand.getElementsByTagName(GCConstants.SOFT_MATCH_DEMAND).item(0);
                    if(!YFCCommon.isVoid(eleSoftMatchedDemand)){
                  	  sSoftMatchedDemand =  eleSoftMatchedDemand.getAttribute("TotalQuantity"); 
                  	  iSoftMatchDemand = Integer.parseInt(sSoftMatchedDemand);
                    }
                    int isOnHandQty = Integer.parseInt(sOnHandQty);
                    isOnHandQty = isOnHandQty - iSoftMatchDemand;
                    //End
                    onHandQtySafetyFac = getOnHandAfterSafetyFactor(env,isOnHandQty,shipNode, 
                  			sOrganizationCode, itemID, sProductClass, sUOM);
                    int onHndQty = onHandQtySafetyFac / kitQty;
                    int onMapQty = e2.getValue();
                    shipNodeQtyMapOn.put(shipNode, Math.min(onHndQty, onMapQty));
                  } else {
                    entry3.remove();
                  }
                }
              }else{
            	  entry3.remove(); // GCSTORE - 4828 
              }
              if (shipNodeQtyMapNp.isEmpty() && shipNodeQtyMapOn.isEmpty()) {
                break;
              }
            }
          }
        }

      }
    }
    if(YFCUtils.equals(sProdClass, GCConstants.SET)){
      for (Map.Entry<String, Integer> entry : shipNodeQtyMapOn.entrySet()) {
        int quantity = entry.getValue();
        onHandAv = onHandAv + quantity;
		String shipNode = entry.getKey();
		if (YFCUtils.equals(shipNode, "MFI")) {
			onHandAvDC += quantity;
		}else{
			onHandAvStore += quantity;
		}
		

      }
    
      // GCSTORE -- 4828
      if(!(YFCCommon.isVoid(eleItemAvail))){
    	  String onHandTotalCompItemList = eleItemAvail.getAttribute("OnhandAvailableQuantity");
    	  if(YFCCommon.isVoid(onHandTotalCompItemList)){
			onHandTotalCompItemList = GCConstants.ZERO;	
    	  }
    	  else{
    		  onHandAv = Integer.parseInt(onHandTotalCompItemList);
    	  }
	  }
      // GCSTORE -- 4828 END
      
      //onHandAvStore = onHandAv - onHandAvDC;

      for (Map.Entry<String, Integer> entry1 : shipNodeQtyMapNp.entrySet()) {
        int quantity = entry1.getValue();
        nonPickable = nonPickable + quantity;

      }
    }


  }

private int getOnHandAfterSafetyFactor(YFSEnvironment env, int onHandQty, String nodeOrgCode, 
		String sOrganizationCode, String sItemID, String sProductClass, String sUnitOfMeasure) {
	// TODO Auto-generated method stub
	if (LOGGER.isVerboseEnabled()) {
    	LOGGER.verbose("input to getOnHandAfterSafetyFactor, onHandQty :" + onHandQty );
    } 
	
	if(!YFCCommon.equals(nodeOrgCode, "MFI")){
	    
	    YFCDocument yfcIndocItemList = YFCDocument.createDocument("Item");
	    YFCElement eleRootItemListIndoc = yfcIndocItemList.getDocumentElement();
	    eleRootItemListIndoc.setAttribute(GCConstants.CALLING_ORGANIZATION_CODE, sOrganizationCode);
	    eleRootItemListIndoc.setAttribute(GCConstants.ITEM_ID, sItemID);
	    eleRootItemListIndoc.setAttribute(GCConstants.PRODUCT_CLASS, sProductClass);
	    eleRootItemListIndoc.setAttribute(GCConstants.UOM, sUnitOfMeasure);
	    
	    YFCDocument yfcgetItemListTemp =
	            YFCDocument
	            .getDocumentFor("<ItemList> <Item> <InventoryParameters/> </Item> </ItemList>");
	    
	    if (LOGGER.isVerboseEnabled()) {
	    	LOGGER.verbose("inputDocument for  getItemList in getOnHandAfterSafetyFactor :" + yfcIndocItemList.toString());
	    }    
	    YFCDocument yfcgetItemListOutput =
	        GCCommonUtil.invokeAPI(env, GCConstants.GET_ITEM_LIST, yfcIndocItemList, yfcgetItemListTemp);
	    if (LOGGER.isVerboseEnabled()) {
	    	LOGGER.verbose("outputDocument for  getItemList in getOnHandAfterSafetyFactor :" + yfcgetItemListOutput.toString());
	    }
	    
	    String onHandSafetyFactor = GCConstants.ZERO;
	    
	    Element eleInvParameters =
	            GCXMLUtil.getElementByXPath(yfcgetItemListOutput.getDocument(),
	                "ItemList/Item/InventoryParameters");
	    
	    if (!YFCCommon.isVoid(eleInvParameters)) {
	    	onHandSafetyFactor = eleInvParameters.getAttribute("OnhandSafetyFactorQuantity");
	    	if(YFCCommon.isVoid(onHandSafetyFactor)){
	    		onHandSafetyFactor = GCConstants.ZERO;
	    	}
	    	LOGGER.debug("onHandSafetyFactor for item ID :"+sItemID+" is "+onHandSafetyFactor);
	    	
	    	int availableQtyInt = onHandQty - Integer.parseInt(onHandSafetyFactor);
	    	
	    	if(availableQtyInt < 0){
	    		onHandQty = 0;
	    	}
	    	else{
	    		onHandQty = availableQtyInt;
	    	}
	    }
    }
	
	if (LOGGER.isVerboseEnabled()) {
    	LOGGER.verbose("output getOnHandAfterSafetyFactor, onHandQty :" + onHandQty );
    } 

	return onHandQty;
}
}