package com.gc.api;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCGetOrderAuditList {

  //Initialize Logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCGetOrderAuditList.class);

  public Document getOrderAuditList(YFSEnvironment env, Document inDoc)
      throws GCException {
    try{
      Document getOrderAuditListOp = GCCommonUtil.invokeAPI(env, inDoc,
          "getOrderAuditList",
          "/global/template/api/getOrderAuditList.xml");
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("getOrderAuditList :: "
            + GCXMLUtil.getXMLString(getOrderAuditListOp));}

      NodeList nlOrderAudit = XPathAPI.selectNodeList(getOrderAuditListOp,
          "//OrderAudit");

      for (int i = 0; i < nlOrderAudit.getLength(); i++) {

        Element eleOrderAudit = (Element) nlOrderAudit.item(i);
        String sOrderAuditKey = eleOrderAudit.getAttribute("OrderAuditKey");

        NodeList nlOrderAuditLevel = XPathAPI.selectNodeList(
            getOrderAuditListOp, "//OrderAudit[@OrderAuditKey='"
                + sOrderAuditKey
                + "']/OrderAuditLevels/OrderAuditLevel");

        for (int j = 0; j < nlOrderAuditLevel.getLength(); j++) {

          Element eleOrderAuditLevel = (Element) nlOrderAuditLevel
              .item(j);

          Element eleModificationTypes = (Element) eleOrderAuditLevel
              .getElementsByTagName("ModificationTypes").item(0);

          if (YFCCommon.isVoid(eleModificationTypes)) {
            eleModificationTypes = getOrderAuditListOp
                .createElement("ModificationTypes");
            eleOrderAuditLevel.appendChild(eleModificationTypes);
          } else {
            Element eleModificationType = (Element) XPathAPI
                .selectSingleNode(
                    getOrderAuditListOp,
                    "//OrderAudit[@OrderAuditKey='"
                        + sOrderAuditKey
                        + "']/OrderAuditLevels/OrderAuditLevel/ModificationTypes/ModificationType[@Name='CHANGE_STATUS']");
            if (!YFCCommon.isVoid(eleModificationType)) {
              eleModificationType.setAttribute("ScreenName",
                  "Change Status");
            }
          }

          String sModificationLevelScreenName = eleOrderAuditLevel
              .getAttribute("ModificationLevelScreenName");
          if (YFCCommon.isVoid(sModificationLevelScreenName)) {
            String sModificationLevel = eleOrderAuditLevel
                .getAttribute("ModificationLevel");
            if ("ORDER".equalsIgnoreCase(sModificationLevel)) {
              eleOrderAuditLevel.setAttribute(
                  "ModificationLevelScreenName", "Order");
            } else if ("ORDER_LINE"
                .equalsIgnoreCase(sModificationLevel)) {
              eleOrderAuditLevel.setAttribute(
                  "ModificationLevelScreenName", "Order Line");
            } else if ("ORDER_RELEASE_LINE"
                .equalsIgnoreCase(sModificationLevel)) {
              eleOrderAuditLevel.setAttribute(
                  "ModificationLevelScreenName",
                  "Order Release Line");
            }
          }

          NodeList nlOrderAuditDetail = eleOrderAuditLevel
              .getElementsByTagName("OrderAuditDetail");
          for (int k = 0; k < nlOrderAuditDetail.getLength(); k++) {

            Document docOrderAuditDetail = GCXMLUtil.getDocument(
                (Element) nlOrderAuditDetail.item(k), true);

            // Order audit for add line: Begin
            Element eleAttributeItemID = (Element) XPathAPI
                .selectSingleNode(docOrderAuditDetail,
                    "//Attribute[@Name='ItemID' and @OldValue='']");

            if (!YFCCommon.isVoid(eleAttributeItemID)) {

              Element eleModificationType = (Element) XPathAPI
                  .selectSingleNode(
                      getOrderAuditListOp,
                      "//OrderAudit[@OrderAuditKey='"
                          + sOrderAuditKey
                          + "']/OrderAuditLevels/OrderAuditLevel/ModificationTypes/ModificationType[@Name='ADD_LINE']");
              if (YFCCommon.isVoid(eleModificationType)) {
                eleModificationType = getOrderAuditListOp
                    .createElement("ModificationType");
                eleModificationTypes
                .appendChild(eleModificationType);
                eleModificationType
                .setAttribute("Name", "ADD_LINE");
                eleModificationType.setAttribute("ScreenName",
                    "Add Line");
              }
            }

            // Order audit for add line: End

            // Order audit for customer appeasement: Begin
            Element eleAttributeAppeasement = (Element) XPathAPI
                .selectSingleNode(docOrderAuditDetail,
                    "//Attribute[@NewValue='YCD_CUSTOMER_APPEASE' and @OldValue='']");

            if (!YFCCommon.isVoid(eleAttributeAppeasement)) {
              Element eleModificationType = (Element) XPathAPI
                  .selectSingleNode(
                      getOrderAuditListOp,
                      "//OrderAudit[@OrderAuditKey='"
                          + sOrderAuditKey
                          + "']/OrderAuditLevels/OrderAuditLevel/ModificationTypes/ModificationType[@Name='CUSTOMER_APPEASED']");
              if (YFCCommon.isVoid(eleModificationType)) {
                eleModificationType = getOrderAuditListOp
                    .createElement("ModificationType");
                eleModificationTypes
                .appendChild(eleModificationType);
                eleModificationType.setAttribute("Name",
                    "CUSTOMER_APPEASED");
                eleModificationType.setAttribute("ScreenName",
                    "Customer Appeased");
              }
            }

            // Order audit for customer appeasement: End

            // Order audit for Shipment: Begin

            Element eleAttributeShip = (Element) XPathAPI
                .selectSingleNode(docOrderAuditDetail,
                    "//Attribute[@Name='ShippedQuantity' and @OldValue='0.00']");

            if (!YFCCommon.isVoid(eleAttributeShip)) {
              Element eleModificationType = (Element) XPathAPI
                  .selectSingleNode(
                      getOrderAuditListOp,
                      "//OrderAudit[@OrderAuditKey='"
                          + sOrderAuditKey
                          + "']/OrderAuditLevels/OrderAuditLevel/ModificationTypes/ModificationType[@Name='SHIPPED']");
              if (YFCCommon.isVoid(eleModificationType)) {
                eleModificationType = getOrderAuditListOp
                    .createElement("ModificationType");
                eleModificationTypes
                .appendChild(eleModificationType);
                eleModificationType.setAttribute("Name", "SHIPPED");
                eleModificationType.setAttribute("ScreenName",
                    "Shipped");
              }
            }
            // Order audit for Shipment: End

            // Order audit for charge category: Begin
            Element eleIDChargeCategory = (Element) XPathAPI
                .selectSingleNode(docOrderAuditDetail,
                    "//ID[@Name='ChargeCategory']");

            if (!YFCCommon.isVoid(eleIDChargeCategory)) {
              String sValue = eleIDChargeCategory
                  .getAttribute("Value");
              Element eleModificationType = (Element) XPathAPI
                  .selectSingleNode(
                      getOrderAuditListOp,
                      "//OrderAudit[@OrderAuditKey='"
                          + sOrderAuditKey
                          + "']/OrderAuditLevels/OrderAuditLevel/ModificationTypes/ModificationType[@ScreenName='"
                          + sValue + "']");
              if (YFCCommon.isVoid(eleModificationType)) {
                eleModificationType = getOrderAuditListOp
                    .createElement("ModificationType");
                eleModificationTypes
                .appendChild(eleModificationType);
                eleModificationType.setAttribute("ScreenName",
                    sValue);
              }
            }

            // Order audit for charge category: End

            // Order audit for cancellation: Begin

            Element eleOrderedQty = (Element) XPathAPI
                .selectSingleNode(docOrderAuditDetail,
                    "//Attribute[@Name='OrderedQty' and @NewValue='0.00']");

            if (!YFCCommon.isVoid(eleOrderedQty)) {

              Element eleModificationType = (Element) XPathAPI
                  .selectSingleNode(
                      getOrderAuditListOp,
                      "//OrderAudit[@OrderAuditKey='"
                          + sOrderAuditKey
                          + "']/OrderAuditLevels/OrderAuditLevel/ModificationTypes/ModificationType[@Value='Cancelled']");
              if (YFCCommon.isVoid(eleModificationType)) {
                eleModificationType = getOrderAuditListOp
                    .createElement("ModificationType");
                eleModificationTypes
                .appendChild(eleModificationType);
                eleModificationType.setAttribute("Name", "CANCEL");
                eleModificationType.setAttribute("ScreenName",
                    "Cancel");
              }
            }

            // Order audit for cancellation: End

            // Order audit for customer commission: Begin

            orderAuditsForCommmision(docOrderAuditDetail, getOrderAuditListOp, eleModificationTypes, sOrderAuditKey);

            // Order audit for customer commission: End

            // Order audit for ftc exempt: Begin
            Element eleAttributeFTC = (Element) XPathAPI
                .selectSingleNode(docOrderAuditDetail,
                    "//Attribute[@Name='ExtnFTCExempt']");
            if (!YFCCommon.isVoid(eleAttributeFTC)) {
              Element eleModificationType = getOrderAuditListOp
                  .createElement("ModificationType");
              eleModificationTypes.appendChild(eleModificationType);
              eleModificationType.setAttribute("ScreenName",
                  "FTC Exempt");
            }

            // Order audit for ftc exempt: End
          }
        }
      }
      return getOrderAuditListOp;
    }catch(Exception e){
      LOGGER.error("The exception is in getOrderAuditList method:", e);
      throw new GCException(e);
    }
  }

  /**
   * @param sOrderAuditKey
   * @param eleModificationTypes
   * @param docOrderAuditDetail2
   * @param docOrderAuditDetail
   *
   */
  private void orderAuditsForCommmision(Document docOrderAuditDetail, Document getOrderAuditListOp,
      Element eleModificationTypes, String sOrderAuditKey) throws GCException {
    try {
      String sAuditType = docOrderAuditDetail.getDocumentElement().getAttribute("AuditType");

      if (!YFCCommon.isVoid(sAuditType) && "GCOrderCommission".equals(sAuditType)) {

        Element eleAllocationPercentage =
            (Element) XPathAPI
            .selectSingleNode(
                getOrderAuditListOp,
                "//OrderAudit[@OrderAuditKey='"
                    + sOrderAuditKey
                    + "']/OrderAuditLevels/OrderAuditLevel/OrderAuditDetails/OrderAuditDetail/CustomAuditDetails/OrderAuditDetail[@AuditType='GCOrderCommission']/Attributes/Attribute[@Name='AllocationPercentage']");

        Element eleUserName =
            (Element) XPathAPI
            .selectSingleNode(
                getOrderAuditListOp,
                "//OrderAudit[@OrderAuditKey='"
                    + sOrderAuditKey
                    + "']/OrderAuditLevels/OrderAuditLevel/OrderAuditDetails/OrderAuditDetail/CustomAuditDetails/OrderAuditDetail[@AuditType='GCOrderCommission']/Attributes/Attribute[@Name='UserName']");

        Element eleCommissionType =
            (Element) XPathAPI
            .selectSingleNode(
                getOrderAuditListOp,
                "//OrderAudit[@OrderAuditKey='"
                    + sOrderAuditKey
                    + "']/OrderAuditLevels/OrderAuditLevel/OrderAuditDetails/OrderAuditDetail/CustomAuditDetails/OrderAuditDetail[@AuditType='GCOrderCommission']/Attributes/Attribute[@Name='CommissionType']");

        if (!YFCCommon.isVoid(eleAllocationPercentage)) {
          Element eleModificationType = getOrderAuditListOp.createElement("ModificationType");
          eleModificationTypes.appendChild(eleModificationType);
          eleModificationType.setAttribute("ScreenName", "Commission Allocation Percentage");

        }

        if (!YFCCommon.isVoid(eleUserName)) {
          Element eleModificationType = getOrderAuditListOp.createElement("ModificationType");
          eleModificationTypes.appendChild(eleModificationType);
          eleModificationType.setAttribute("ScreenName", "Commission User Name");

        }

        if (!YFCCommon.isVoid(eleCommissionType)) {
          Element eleModificationType = getOrderAuditListOp.createElement("ModificationType");
          eleModificationTypes.appendChild(eleModificationType);
          eleModificationType.setAttribute("ScreenName", "Commission Type");

        }
      }
    } catch (Exception e) {
      LOGGER.error("The exception is in orderAuditsForCommmision method:", e);
      throw new GCException(e);
    }

  }
}
