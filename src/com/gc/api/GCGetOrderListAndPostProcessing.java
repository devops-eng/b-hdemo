/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               22/04/2014        Mittal, Yashu		JIRA No: Description of issue.
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.util.List;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCPaymentSecurity;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:yashu.mittal@expicient.com">Yashu Mittal</a>
 */

public class GCGetOrderListAndPostProcessing implements YIFCustomApi {

	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCGetOrderListAndPostProcessing.class.getName());

	/**
	 * This method get the order details and do pre-processing like
	 * encrypt/decrypt payment methods before syncing it with DAX.
	 *
	 * @param env
	 * @param inDoc
	 * @throws GCException
	 * @see com.yantra.interop.japi.YIFCustomApi#setProperties(java.util.Properties)
	 */

	public Document getOrderListAndProcess(YFSEnvironment env, Document inDoc)
			throws GCException {

		try {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(" Entering GCGetOrderListAndPostProcessing.getOrderListAndProcess() method ");
				LOGGER.debug(" getOrderListAndProcess() method, Input document is"
						+ GCXMLUtil.getXMLString(inDoc));
			}

			Element eleRootInDoc = inDoc.getDocumentElement();
			String sOrderHeaderKey = "";

			String sInDocRootElementName = eleRootInDoc.getNodeName();

			if (GCConstants.ORDER.equalsIgnoreCase(sInDocRootElementName)) {
				sOrderHeaderKey = eleRootInDoc
						.getAttribute(GCConstants.ORDER_HEADER_KEY);
			} else if (GCConstants.RECEIPT
					.equalsIgnoreCase(sInDocRootElementName)) {
				Element eleReceiptLine = (Element) eleRootInDoc
						.getElementsByTagName("ReceiptLine").item(0);
				sOrderHeaderKey = eleReceiptLine
						.getAttribute(GCConstants.ORDER_HEADER_KEY);
			}

			// Invoking getOrderList with OrderHeaderKey and template passed
			Document getOrderListOutDoc = GCCommonUtil
					.invokeAPI(
							env,
							GCConstants.GET_ORDER_LIST,
							GCXMLUtil.getDocument("<Order OrderHeaderKey='"
									+ sOrderHeaderKey + "'/>"),
							GCCommonUtil
									.getDocumentFromPath("/global/template/api/OMS_DAX_OrderSync_TemplateXML.xml"));
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(GCXMLUtil.getXMLString(getOrderListOutDoc));
			}
			List<Element> listPaymentMethod = GCXMLUtil.getElementListByXpath(
					getOrderListOutDoc,
					GCConstants.ORDERLIST_ORDER_PAYMENT_METHPD_XPATH);

			// Encrypt/Decrypt CC, PLCC or GC payment method present in order.
			for (Element elePaymentMethod : listPaymentMethod) {

				String sPaymentType = elePaymentMethod
						.getAttribute(GCConstants.PAYMENT_TYPE);

				if (sPaymentType.equalsIgnoreCase(GCConstants.CREDIT_CARD)
						|| sPaymentType.equalsIgnoreCase(GCConstants.PLCC)
						|| sPaymentType.equalsIgnoreCase(GCConstants.GIFT_CARD)) {

					// Decrypting/Encrypting Svc no in case of any payment
					// method.
					String sSvcNo = elePaymentMethod
							.getAttribute(GCConstants.SVC_NO);

					if (!YFCCommon.isVoid(sSvcNo)) {

						String sDecryptedSvcNo = GCPaymentSecurity
								.decryptCreditCard(sSvcNo, "OMS");

						String sEncryptedSvcNo = GCPaymentSecurity
								.encryptCreditCard(sDecryptedSvcNo, "DAX");

						elePaymentMethod.setAttribute(GCConstants.SVC_NO,
								sEncryptedSvcNo);
					}

					if (sPaymentType.equalsIgnoreCase(GCConstants.CREDIT_CARD)
							|| sPaymentType.equalsIgnoreCase(GCConstants.PLCC)) {
						// Decrypting/Encrypting CC No. no in case of CC or PLCC
						// Payment Method

						String sCreditCardNo = elePaymentMethod
								.getAttribute(GCConstants.CREDIT_CARD_NO);

						if (!YFCCommon.isVoid(sCreditCardNo)) {

							String sDecryptedCCNo = GCPaymentSecurity
									.decryptCreditCard(sCreditCardNo, "OMS");

							String sEncryptedCCNo = GCPaymentSecurity
									.encryptCreditCard(sDecryptedCCNo, "DAX");

							elePaymentMethod.setAttribute(
									GCConstants.CREDIT_CARD_NO, sEncryptedCCNo);

						}
					}
				}
			}

      // Change For 5193 - Start
      stampStatusAndQuantityOnSetBundleLine(getOrderListOutDoc);
      // Change for 5193 - End
      stampCancellationReasonText(env, getOrderListOutDoc);
      return getOrderListOutDoc;
    } catch (Exception e) {
      LOGGER.error(
          "Inside catch block of GCGetOrderListAndPostProcessing.getOrderListAndProcess method. Error is : ",
          e);
      throw new GCException(e);
    }

	}

	private void stampCancellationReasonText(YFSEnvironment env,
			Document getOrderListOutDoc) {

		String sCancellationReasonCode = (String) env
				.getTxnObject(GCConstants.CANCELLATION_REASON_CODE);
		if (!YFCCommon.isVoid(sCancellationReasonCode)) {

			Document docGetCommonCodeListIp = GCXMLUtil
					.createDocument(GCConstants.COMMON_CODE);
			Element eleGetCommonCodeListIpRoot = docGetCommonCodeListIp
					.getDocumentElement();
			eleGetCommonCodeListIpRoot.setAttribute(GCConstants.CODE_TYPE,
					GCConstants.YCD_CANCEL_REASON);
			eleGetCommonCodeListIpRoot.setAttribute(GCConstants.CODE_VALUE,
					sCancellationReasonCode);

			Document docGetCommonCodeListOp = GCCommonUtil.invokeAPI(env,
					GCConstants.API_GET_COMMON_CODE_LIST,
					docGetCommonCodeListIp);
			String sCodeLongDescription = GCXMLUtil.getAttributeFromXPath(
					docGetCommonCodeListOp,
					"CommonCodeList/CommonCode/@CodeLongDescription");

			// Stamp code long description as note text at header level in
			// return document

			Element eleNotes = GCXMLUtil.getElementByXPath(getOrderListOutDoc,
					"/OrderList/Order/Notes");

			if (YFCCommon.isVoid(eleNotes)) {
				eleNotes = getOrderListOutDoc.createElement(GCConstants.NOTES);
				Element eleOrder = GCXMLUtil.getElementByXPath(
						getOrderListOutDoc, "/OrderList/Order");
				eleOrder.appendChild(eleNotes);
			}

			Element eleNote = getOrderListOutDoc
					.createElement(GCConstants.NOTES);
			eleNotes.appendChild(eleNote);
			eleNote.setAttribute(GCConstants.NOTE_TEXT,
					GCConstants.CANCELLATION_REASON_CODE_PREFIX
							+ sCodeLongDescription);
		}
	}


  /**
   *
   * This method will stamp StatusQuantity on BundleLines with ProductClass as SET. The status
   * quantity and status will be based on the component.
   *
   *
   * @param inDoc
   *
   */
  private static void stampStatusAndQuantityOnSetBundleLine(Document inDoc) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.info("Inside stampStatusAndQuantityOnSetBundleLine" + GCXMLUtil.getXMLString(inDoc));
    }

    final List<Element> listBundleParentOrderLine =
        GCXMLUtil.getElementListByXpath(inDoc,
            "OrderList/Order/OrderLines/OrderLine[@KitCode='BUNDLE' and Item[@ProductClass='SET']]");



    for (Element eleBundleParentOrderLine : listBundleParentOrderLine) {

      // Fetching the bundle orderlinekey
      final String strBundleOrderLineKey = eleBundleParentOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);


      // Fetching the Order Statuses from the parent
      final Element eleOrderStatuses =
          GCXMLUtil.getFirstElementByName(eleBundleParentOrderLine, GCConstants.ORDER_STATUSES);


      // Fetch the OrderStatus tag from the bundle line
      final NodeList nlBundleStatus = eleBundleParentOrderLine.getElementsByTagName(GCConstants.ORDER_STATUS);



      // Fetching one of the Component OrderLine
      final Element eleOrderLine =
          GCXMLUtil.getElementByXPath(inDoc, "OrderList/Order/OrderLines/OrderLine[@BundleParentOrderLineKey='"
              + strBundleOrderLineKey + "']");

      // Component Line Status Element
      final NodeList nlOrderStatus = eleOrderLine.getElementsByTagName(GCConstants.ORDER_STATUS);

      String strOrderLineScheduleKey = "";
      String strOrderReleaseStatusKey = "";
      String strPipelineKey = "";
      int iBundleOrderedQty = 0;
      Element eleBundleOrderStatus;
      final int iBundleStatusCount = nlBundleStatus.getLength();
      for (int iBundleStatusIndex = 0; iBundleStatusIndex < iBundleStatusCount; iBundleStatusIndex++) {

        eleBundleOrderStatus = (Element) nlBundleStatus.item(iBundleStatusIndex);

        if (eleBundleOrderStatus != null) {
          strOrderLineScheduleKey = eleBundleOrderStatus.getAttribute(GCConstants.ORDER_LINE_SCHEDULE_KEY);
          strOrderReleaseStatusKey = eleBundleOrderStatus.getAttribute(GCConstants.ORDER_RELEASE_STATUS_KEY);
          strPipelineKey = eleBundleOrderStatus.getAttribute(GCConstants.PIPELINE_KEY);
          iBundleOrderedQty = Integer.valueOf(eleBundleOrderStatus.getAttribute(GCConstants.TOTAL_QUANTITY));

          if (eleOrderStatuses != null) {
            eleOrderStatuses.removeChild(eleBundleOrderStatus);
          }
        }
      }

      final int iOrderStatusLen = nlOrderStatus.getLength();
      Element eleComponentOrderStatus;
      int iOrderedQty;
      int iStatusQty;
      int iBundleStatusQty;
      Element eleOrderStatus;

      // Fetching the Order Status List from the component Lines
      for (int iOrderStatusIndex = 0; iOrderStatusIndex < iOrderStatusLen; iOrderStatusIndex++) {

        eleComponentOrderStatus = (Element) nlOrderStatus.item(iOrderStatusIndex);

        if (eleComponentOrderStatus != null) {

          iOrderedQty = Integer.valueOf(eleComponentOrderStatus.getAttribute(GCConstants.TOTAL_QUANTITY));

          iStatusQty = Integer.valueOf(eleComponentOrderStatus.getAttribute(GCConstants.STATUS_QTY));

          if (iOrderedQty > 0) {
            iBundleStatusQty = (iStatusQty * iBundleOrderedQty) / iOrderedQty;
          } else {
            iBundleStatusQty = 0;
          }

          eleOrderStatus = GCXMLUtil.createElement(inDoc, GCConstants.ORDER_STATUS, null);
          eleOrderStatus.setAttribute(GCConstants.ORDER_LINE_SCHEDULE_KEY, strOrderLineScheduleKey);
          eleOrderStatus.setAttribute(GCConstants.ORDER_RELEASE_STATUS_KEY, strOrderReleaseStatusKey);
          eleOrderStatus.setAttribute(GCConstants.PIPELINE_KEY, strPipelineKey);
          eleOrderStatus.setAttribute(GCConstants.ORDER_HEADER_KEY,
              eleComponentOrderStatus.getAttribute(GCConstants.ORDER_HEADER_KEY));
          eleOrderStatus.setAttribute(GCConstants.ORDER_LINE_KEY, strBundleOrderLineKey);
          eleOrderStatus.setAttribute(GCConstants.SHIP_NODE,
              eleComponentOrderStatus.getAttribute(GCConstants.SHIP_NODE));
          eleOrderStatus.setAttribute(GCConstants.STATUS, eleComponentOrderStatus.getAttribute(GCConstants.STATUS));
          eleOrderStatus.setAttribute(GCConstants.STATUS_DATE,
              eleComponentOrderStatus.getAttribute(GCConstants.STATUS_DATE));
          eleOrderStatus.setAttribute(GCConstants.STATUS_DESCRIPTION,
              eleComponentOrderStatus.getAttribute(GCConstants.STATUS_DESCRIPTION));
          eleOrderStatus.setAttribute(GCConstants.STATUS_QTY, String.valueOf(iBundleStatusQty));
          eleOrderStatus.setAttribute(GCConstants.TOTAL_QUANTITY, String.valueOf(iBundleOrderedQty));
          eleOrderStatuses.appendChild(eleOrderStatus);

        }
      }
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" Updated document is" + GCXMLUtil.getXMLString(inDoc));
    }
  }





  /**
   * Description of setProperties
   *
   * @param arg0
   * @throws Exception
   * @see com.yantra.interop.japi.YIFCustomApi#setProperties(java.util.Properties)
   */

	@Override
	public void setProperties(Properties arg0) {

	}

}
