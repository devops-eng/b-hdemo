/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               06/02/2014                    Naveen Gowda		JIRA No: This class is used to fetch details for ATG request.
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:email Address">Name</a>
 */
public class GCGetOrderListForATGRequest {

	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCManageItem.class.getName());

	/**
	 *
	 * Description of prepareATGRequest
	 *
	 * @param env
	 * @param inDoc
	 * @return
	 * @throws GCException
	 *
	 */
	public Document prepareATGRequest(YFSEnvironment env, Document inDoc)
			throws GCException {
		try {
			LOGGER.debug("Class: GCGetOrderListForATGRequest Method: prepareATGRequest BEGIN");
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Incoming XML" + GCXMLUtil.getXMLString(inDoc));
			}
			String sOrderHeaderKey = GCXMLUtil.getAttributeFromXPath(inDoc,
					"/Order/@OrderHeaderKey");
			String sSkipShippingChargeForPLCC = GCXMLUtil
					.getAttributeFromXPath(inDoc,
							"/Order/@SkipShippingChargeForPLCC");
			String sCouponCode = GCXMLUtil.getAttributeFromXPath(inDoc,
					"/Order/@CouponCode");
      String sRemoveCouponFlag = GCXMLUtil.getAttributeFromXPath(inDoc,
          "/Order/@RemoveCoupon");
			if (!YFCCommon.isVoid(sCouponCode)) {
				sCouponCode = sCouponCode.toLowerCase();
				LOGGER.debug("Coupon Code" + sCouponCode);
			}

			/* Calling getOrderList */
			Document docGetOrdIn = GCXMLUtil.createDocument(GCConstants.ORDER);
			Element eleGetOrderInRoot = docGetOrdIn.getDocumentElement();
			eleGetOrderInRoot.setAttribute(GCConstants.ORDER_HEADER_KEY,
					sOrderHeaderKey);
			Document docGetOrdOut = null;
			if (!YFCCommon.isVoid(sOrderHeaderKey)) {
				docGetOrdOut = GCCommonUtil.invokeAPI(env, docGetOrdIn,
						GCConstants.API_GET_ORDER_LIST,
						"/global/template/api/getOrderListForATGRequest.xml");
				LOGGER.debug("Incoming XML" + GCXMLUtil.getXMLString(inDoc));

			}
			Element eleOrder = GCXMLUtil.getElementByXPath(docGetOrdOut,
					"/OrderList/Order");
			Document docEleOrder = GCXMLUtil.getDocumentFromElement(eleOrder);
      String sBillToID = docEleOrder.getDocumentElement().getAttribute("BillToID");
      String sEntryType_GCPro = docEleOrder.getDocumentElement().getAttribute("EntryType");
			// if OrderList Output has some or all OrderLines in cancelled
			// status then delete them from ATG input request.
			boolean bIsOrderCancelled = removeCancelledOrderLinesFromOrder(docEleOrder);
      Element eleRootElement = docEleOrder.getDocumentElement();
      Set<String> couponsSet = new HashSet<>();
			if (!YFCCommon.isVoid(sCouponCode)) {
        LOGGER.verbose("Coupon code is received:" + sCouponCode);
        Element eleGCCouponOrderList = GCXMLUtil.getElementByXPath(
						docEleOrder, "Order/Extn/GCOrderCouponList");
        NodeList orderCouponNodeList = eleRootElement.getElementsByTagName("GCOrderCoupon");
        int nLength = orderCouponNodeList.getLength();
        for(int i = 0; i < nLength; i++)
        {
          Element eleCouponCode = (Element) orderCouponNodeList.item(i);
          GCXMLUtil.getElementXMLString(eleCouponCode);
          String coupon = eleCouponCode.getAttribute("CouponCode");
          String sCouponKey = eleCouponCode.getAttribute("GCOrderCouponKey");

          if (YFCCommon.equalsIgnoreCase(coupon, sCouponCode) && YFCUtils.equals(sRemoveCouponFlag, GCConstants.YES))
          {
            env.setTxnObject("RemoveCoupon", sCouponKey);
            eleGCCouponOrderList.removeChild(eleCouponCode);
            nLength--;
            i--;
            break;
          }
          else if (YFCCommon.equalsIgnoreCase(coupon, sCouponCode)
              && !YFCUtils.equals(sRemoveCouponFlag, GCConstants.YES)) {
            env.setTxnObject("DuplicateCoupon", "Y");
            break;
          }

        }

        if (!YFCUtils.equals(sRemoveCouponFlag, GCConstants.YES)) {
          Element eleGCOrderCoupon = docEleOrder.createElement(GCConstants.GC_ORDER_COUPON);
          eleGCOrderCoupon.setAttribute(GCConstants.COUPON_CODE, sCouponCode);
          eleGCCouponOrderList.appendChild(eleGCOrderCoupon);
        }

      } else {
        // Fix for mPOS-996 : Start
        LOGGER.verbose("No Coupon code received. Adding existing coupons in a SET.");
        NodeList orderCouponNodeList = eleRootElement.getElementsByTagName("GCOrderCoupon");
        int nLength = orderCouponNodeList.getLength();
        // Fix for mPOS-996 : Start
        for (int i = 0; i < nLength; i++) {
          Element eleCouponCode = (Element) orderCouponNodeList.item(i);
          GCXMLUtil.getElementXMLString(eleCouponCode);
          String coupon = eleCouponCode.getAttribute("CouponCode");
          couponsSet.add(coupon);
        }
        LOGGER.verbose("Existing coupon set:" + couponsSet.toString());
      }
      env.setTxnObject("CouponsSet", couponsSet);
      // Fix for mPOS-996 : End
			// GCStore-791-Phase-2 Start
			YFCDocument yfcedocGetOrdOut = YFCDocument
					.getDocumentFor(docGetOrdOut);
			YFCElement eleOrderAPIOut = yfcedocGetOrdOut.getDocumentElement()
					.getChildElement("Order");
			String sMaxOrderStatus = eleOrderAPIOut
					.getAttribute("MaxOrderStatus");
			Double dMaxOrderStatus = formatMaxOrderStatus(sMaxOrderStatus);
			boolean bCallATGForInit = false;
			boolean bIsOrderAmendment = false;
			boolean bIsPromotionOrPLCCCall = false;
			if (dMaxOrderStatus >= 1100) {
				bIsOrderAmendment = true;
			}
      String sEntryType = eleOrder.getAttribute(GCConstants.ENTRY_TYPE);
      if(YFCUtils.equals(sEntryType, GCConstants.MPOS))
      {
        eleRootElement.setAttribute("Source","MPOS");
      }

			Element rootEle = inDoc.getDocumentElement();
			String sCallATGForInit = rootEle.getAttribute("CallATGForInit");
			if (YFCCommon.equals(sCallATGForInit, GCConstants.FLAG_Y)) {
				bCallATGForInit = true;
			}
			rootEle.removeAttribute("CallATGForInit");
			String sCallDuringPromotionAndPLCC = rootEle
					.getAttribute("PromotionOrPLCCCall");
			if (YFCCommon.equals(sCallDuringPromotionAndPLCC,
					GCConstants.FLAG_Y)) {
				bIsPromotionOrPLCCCall = true;
			}

			boolean isQuantityToShipUpdated = false;
			if (!bIsOrderCancelled
					&& (((bIsOrderAmendment ^ bIsPromotionOrPLCCCall) || (!bCallATGForInit ^ (bIsOrderAmendment && bIsPromotionOrPLCCCall))) || bIsPromotionOrPLCCCall)) {
				YFCElement eleOrderExtnAPIOut = eleOrderAPIOut
						.getChildElement("Extn");
				String sExtnIsATGPENCallRequired = eleOrderExtnAPIOut
						.getAttribute("ExtnIsATGPENCallRequired");
				double dExtnIsATGPENCallRequired = Double
						.parseDouble(sExtnIsATGPENCallRequired);
				if ((dExtnIsATGPENCallRequired > 1 || bIsPromotionOrPLCCCall)
						&& dMaxOrderStatus < 3800) { 
					docEleOrder.getDocumentElement().setAttribute(
							"ToXSLComponent", GCConstants.FLAG_Y);
					docEleOrder.getDocumentElement().setAttribute(
							"SkipShippingChargeForPLCC",
							sSkipShippingChargeForPLCC);
					updateQuantityToShipForOrder(env, docEleOrder);
					isQuantityToShipUpdated = true;
				}
			}
			if (!isQuantityToShipUpdated) {
				updateQuantityToShipForOrder(env, docEleOrder);
			}
      boolean isOrderStorePickup = isOrderStorePickup(docEleOrder);
      if (isOrderStorePickup) {
        env.setTxnObject("IsStorePickUp", "Y");
      }

      if (bCallATGForInit && !bIsOrderCancelled) {

        YFCDocument yfcDocChangeOrder = YFCDocument
            .createDocument("Order");
        yfcDocChangeOrder.getDocumentElement().setAttribute(
            "OrderHeaderKey", sOrderHeaderKey);


        if (isOrderStorePickup && !(YFCUtils.equals(sEntryType, GCConstants.MPOS))) {

          yfcDocChangeOrder.getDocumentElement().setAttribute(
              "LevelOfService", "STORE_PICKUP");
        }


        else {
          String los = eleOrder.getAttribute("LevelOfService");
          if(YFCCommon.equals(los, "STORE_PICKUP")){
            yfcDocChangeOrder.getDocumentElement().setAttribute(
                "LevelOfService", "");
          }
        }
        yfcDocChangeOrder.getDocumentElement().setAttribute("Override", "Y");
        String sLevelOfService = yfcDocChangeOrder.getDocumentElement().getAttribute("LevelOfService");
        if(!YFCCommon.isVoid(sLevelOfService)){
          GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER,
              yfcDocChangeOrder.getDocument());
        }
      }
      
   // changes related to GCPro
      if(!YFCCommon.equalsIgnoreCase(sEntryType_GCPro, "mPOS")){
       YFCDocument getCustomerListInDoc = YFCDocument.getDocumentFor("<Customer CustomerID='" + sBillToID + "'/>");
       YFCDocument getCustomerListTemp = YFCDocument.getDocumentFor("<CustomerList><Customer CustomerID='' CustomerType=''><Extn /></Customer></CustomerList>");
       
       YFCDocument getCustomerList = GCCommonUtil.invokeAPI(env, GCConstants.GET_CUSTOMER_LIST, getCustomerListInDoc, getCustomerListTemp);
       YFCElement eleCustomerExtn = getCustomerList.getElementsByTagName("Extn").item(0);
       
       if(!YFCCommon.isVoid(eleCustomerExtn)){
    	   LOGGER.debug("Verufying Customer validations");
        String sExtnIsGcProCustomer = eleCustomerExtn.getAttribute(GCConstants.EXTN_IS_GCPRO_CUSTOMER);
        if(!YFCCommon.isVoid(sExtnIsGcProCustomer) && YFCCommon.equalsIgnoreCase("Y", sExtnIsGcProCustomer)){
        	LOGGER.debug("Customer is a GCPro Customer");
         String sExtnGcProStatus = eleCustomerExtn.getAttribute(GCConstants.EXTN_GCPRO_STATUS);
         if(!YFCCommon.isVoid(sExtnGcProStatus)){
          if(YFCCommon.equalsIgnoreCase(sExtnGcProStatus, GCConstants.PENDING) || YFCCommon.equalsIgnoreCase(sExtnGcProStatus, GCConstants.APPROVED_LOWERCASE)){
        	  LOGGER.debug("Customer status is pending/approved");
           Element eledocEleOrderExtn = (Element) docEleOrder.getElementsByTagName("Extn").item(0);
           eledocEleOrderExtn.setAttribute("ExtnGCProCustomerValidation", "Y");
          }
          else{
        	  Element eledocEleOrderExtn = (Element) docEleOrder.getElementsByTagName("Extn").item(0);
              eledocEleOrderExtn.setAttribute("ExtnGCProCustomerValidation", "N");
          }
         }
         else{
        	 Element eledocEleOrderExtn = (Element) docEleOrder.getElementsByTagName("Extn").item(0);
             eledocEleOrderExtn.setAttribute("ExtnGCProCustomerValidation", "N");
         }
         }
        else{
        	LOGGER.debug("Customer is not a GCPro Customer");
        	Element eledocEleOrderExtn = (Element) docEleOrder.getElementsByTagName("Extn").item(0);
            eledocEleOrderExtn.setAttribute("ExtnGCProCustomerValidation", "N");
        }
        }
       }
      // GCStore-791-Phase-2 End
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Document to XSL Document"
            + GCXMLUtil.getXMLString(docEleOrder));
      }
      LOGGER.debug("Class: GCGetOrderListForATGRequest Method: prepareATGRequest END");

			return docEleOrder;

		} catch (Exception e) {
			LOGGER.error("The exception is in prepareATGRequest method:", e);
			throw new GCException(e);
		}
	}
	
	// Handling the scenario where maxStatus is of the format 1.2.3
  private double formatMaxOrderStatus(String maxOrderStatus) {
		// TODO Auto-generated method stub
	  
      String[] maxStatusSubList = maxOrderStatus.split("\\.");
      final int noOfSubStatus = maxStatusSubList.length;
      LOGGER.verbose("noOfSubStatus :" + noOfSubStatus);
      Double dMaxOrderStatus = 0.0;
      if (noOfSubStatus > 2) {
        String newSubStatus = maxStatusSubList[0] + "." + maxStatusSubList[1];
        dMaxOrderStatus = Double.parseDouble(newSubStatus);
      } else {
        dMaxOrderStatus = Double.parseDouble(maxOrderStatus);
      }
      return dMaxOrderStatus;
  }

// GCStore-791-Phase-2 starts
  /**
   * This method is invoked to get the Quantity that will be shipped for all
   * the orderlines. QuantityToShip is required for Shipping charge
   * calculation
   *
   * @param env
   * @param docEleOrder
   * @param dExtnIsATGCallRequired
   */
  private void updateQuantityToShipForOrder(YFSEnvironment env,
      Document docEleOrder) {
    LOGGER.beginTimer("Entering: updateQuantityToShipForOrder");
    YFCDocument yfceGetOrderListOutputDoc = YFCDocument
        .getDocumentFor(docEleOrder);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Incoming XML to updateQuantityToShipForOrder:"
          + yfceGetOrderListOutputDoc.toString());
    }
    YFCElement eleOrder = yfceGetOrderListOutputDoc.getDocumentElement();
    String sOrganizationCode = eleOrder
        .getAttribute(GCConstants.ENTERPRISE_CODE);
    String sSourcingClassification = eleOrder
        .getAttribute("SourcingClassification");
    YFCElement eleExtnOrder = eleOrder.getChildElement(GCConstants.EXTN);
    String sExtnPickupStoreID = eleExtnOrder
        .getAttribute(GCConstants.EXTN_PICK_UP_STORE_ID);
    YFCElement eleOrderlines = eleOrder
        .getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> nlOrderLine = eleOrderlines
        .getElementsByTagName(GCConstants.ORDER_LINE);

		YFCDocument indocFindInventory = null;
		YFCElement elePromiseLines = null;


    indocFindInventory = YFCDocument
        .createDocument(GCConstants.PROMISE);
    YFCElement elePromise = indocFindInventory.getDocumentElement();
    elePromise.setAttribute(GCConstants.ORGANIZATION_CODE,
        sOrganizationCode);
    elePromise.setAttribute("AggregateSupplyOfNonRequestedTag",
        GCConstants.FLAG_Y);
    elePromise.setAttribute("UseUnplannedInventory", GCConstants.FLAG_N);
    elePromise.setAttribute("ConsiderPartialShipment", GCConstants.YES);
    YFCElement elePromiseExtn = elePromise
        .createChild(GCConstants.EXTN);
    elePromiseExtn.setAttribute(GCConstants.EXTN_PICK_UP_STORE_ID,
        sExtnPickupStoreID);
    elePromiseLines = elePromise.createChild(GCConstants.PROMISE_LINES);
		Map<String, String> mBundleKeyDetails = new HashMap<String, String>();
		boolean hasBundleKeyMapUpdated = false;
    for (YFCElement eleOrderLine : nlOrderLine) {
      String sDeliveryMethod = eleOrderLine.getAttribute(GCConstants.DELIVERY_METHOD);
      if(YFCUtils.equals(sDeliveryMethod, "CARRY"))
      {
        eleOrderLine.setAttribute("QuantityToShip", "0");
      }

      else{
      String sFulfillmentType = eleOrderLine
          .getAttribute(GCConstants.FULFILLMENT_TYPE);
      YFCElement eleBundleParentLine = eleOrderLine
          .getChildElement(GCConstants.BUNDLE_PARENT_LINE);
      if (YFCCommon.isVoid(eleBundleParentLine)) {
        double dOrderedQty = eleOrderLine
            .getDoubleAttribute(GCConstants.ORDERED_QTY);
        if (sFulfillmentType.contains(GCConstants.PICKUP_IN_STORE)) {
          String shipNodeAtOL = eleOrderLine.getAttribute(GCConstants.SHIP_NODE);
          if (!YFCCommon.isStringVoid(shipNodeAtOL)) {
            if (YFCCommon.equals(shipNodeAtOL, sExtnPickupStoreID, false)) {
              eleOrderLine.setAttribute("QuantityToShip", "0");
            } else {
              eleOrderLine.setDoubleAttribute("QuantityToShip", dOrderedQty);
            }
          } else {
            boolean hasQuantityToShipUpdated = updateQtyToShipForSerialItem(eleOrderLine);
            if(!hasQuantityToShipUpdated){
              hasQuantityToShipUpdated =
                  updateQtyToShipAsPerSourcing(eleOrderLine, sSourcingClassification);
            }
            if (!hasQuantityToShipUpdated) {
              hasQuantityToShipUpdated = updateQtyToShipForClearanceItem(eleOrderLine);
	if (!hasBundleKeyMapUpdated) {
									updateBundleKeyReservedQtyForBundle(
											docEleOrder,
											hasBundleKeyMapUpdated,
											mBundleKeyDetails);
									hasBundleKeyMapUpdated = true;
								}
								updateQtyToShipForReservation(eleOrderLine,
										mBundleKeyDetails, sExtnPickupStoreID);
								createPromiseLineForFindInventory(eleOrderLine,
						                  elePromiseLines, sExtnPickupStoreID);
            }
          }

				} else {
					eleOrderLine.setDoubleAttribute("QuantityToShip",
							dOrderedQty);
				}
			}

		}
		if (!YFCCommon.isVoid(indocFindInventory)) {
			YFCNodeList<YFCElement> nlPromiseLine = indocFindInventory
					.getElementsByTagName(GCConstants.PROMISE_LINE);
			if (nlPromiseLine.getLength() > 0) {
				if (LOGGER.isVerboseEnabled()) {
					LOGGER.verbose("Incoming XML to GCFindInventoryWrapperService:"
							+ indocFindInventory.toString());
				}

				YFCDocument findInvOutpt = GCCommonUtil.invokeService(env,
						"GCFindInventoryWrapperService", indocFindInventory);
				updateQtyToShipFromFindInvCall(findInvOutpt, nlOrderLine,
						sExtnPickupStoreID);
				if (LOGGER.isVerboseEnabled()) {
					LOGGER.verbose("Output XML from GCFindInventoryWrapperService:"
							+ findInvOutpt.toString());
					LOGGER.endTimer("Exiting: updateQuantityToShipForOrder");
				}
			}
		}
    }
	}

  /**
   * this is the main method which updates the QuantityToShip for bundle lines and
   *  calls the method for calculating the same for regular lines
   * @param eleOrderLine
   * @param mBundleKeyDetails
   * @param sExtnPickupStoreID
   */
  public static void updateQtyToShipForReservation(YFCElement eleOrderLine,
			Map<String, String> mBundleKeyDetails, String sExtnPickupStoreID) {
		LOGGER.beginTimer("Entering: updateQtyToShipForReservation");
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Input to updateQtyToShipForReservation()"
					+ eleOrderLine.toString() +"ExtnPickupStoreID"+sExtnPickupStoreID);
		}

		String sIsBundleParent = eleOrderLine
				.getAttribute("IsBundleParent");
		String sOrderLineKey = eleOrderLine.getAttribute("OrderLineKey");
		if (YFCCommon.equals(sIsBundleParent, "Y")) {
			double dOrderedQtyBundleParent = eleOrderLine
					.getIntAttribute("OrderedQty");
			YFCElement eleItemDetails = eleOrderLine
					.getChildElement("ItemDetails");
			YFCElement eleComponents = eleItemDetails
					.getChildElement("Components");
			YFCNodeList<YFCElement> nlComponent = eleComponents
					.getElementsByTagName("Component");
			double dTotalComponentReservedQty = 0.0;
			double dReservedQtyBundleParent = 0.0;
			for (YFCElement eleComponent : nlComponent) {
				double dKitQuantity = eleComponent
						.getDoubleAttribute("KitQuantity");
				String sItemIDRsvrQty = mBundleKeyDetails
						.get(sOrderLineKey);
				String sComponentItemID = eleComponent
						.getAttribute("ComponentItemID");
				if (!YFCCommon.isVoid(sItemIDRsvrQty)) {
					String[] parts = sItemIDRsvrQty.split(":");
					String sComponentItemIDfromMAP = parts[0];
					String sComponentReservedQty = parts[1];
					dTotalComponentReservedQty = Double
							.parseDouble(sComponentReservedQty);
					if (YFCCommon.equals(sComponentItemID,
							sComponentItemIDfromMAP)) {
						dReservedQtyBundleParent = dTotalComponentReservedQty
								/ dKitQuantity;
					}
				}
			}
			double dAllocatedQuantity = 0;
			dAllocatedQuantity = getAllocatedQuantity(eleOrderLine);
			eleOrderLine.setAttribute("QuantityToShip", dOrderedQtyBundleParent
					- dReservedQtyBundleParent - dAllocatedQuantity);
		} else {
			updateQtyToShipForRegularReservations(eleOrderLine,
					sExtnPickupStoreID);
		}

		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Changed Input from updateQtyToShipForReservation()"
					+ eleOrderLine.toString());
		}
		LOGGER.endTimer("Exiting: updateQtyToShipForReservation");
	}
  /**
   * This method updated the QuantityToShip attribute for regular items
   * @param eleOrderLine
   * @param sExtnPickupStoreID
   */
  private static void updateQtyToShipForRegularReservations(
			YFCElement eleOrderLine,
			String sExtnPickupStoreID) {
	  LOGGER.beginTimer("Entering: updateQtyToShipForRegularReservations");
	  if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose(" Input to updateQtyToShipForRegularReservations()"
					+ eleOrderLine.toString()+"ExtnPickupStoreID"+sExtnPickupStoreID);
		}
		double dOrderedQty = eleOrderLine
				.getDoubleAttribute(GCConstants.ORDERED_QTY);
		double dQuantityAvailable = 0;
		double dAllocatedQuantity = 0;
		dAllocatedQuantity = getAllocatedQuantity(eleOrderLine);
		YFCElement eleOrderLineReservations = eleOrderLine
				.getChildElement(GCConstants.ORDER_LINE_RESERVATIONS);
		if (!YFCCommon.isVoid(eleOrderLineReservations)) {

			YFCNodeList<YFCElement> nlOrderLineReservation = eleOrderLineReservations
					.getElementsByTagName(GCConstants.ORDER_LINE_RESERVATION);

			double dRsrvQuantity = 0;
			for (int h = 0; h < nlOrderLineReservation.getLength(); h++) {
				YFCElement eleOrderLineReservation = nlOrderLineReservation
						.item(h);
				String sNode = eleOrderLineReservation
						.getAttribute(GCConstants.NODE);
				dRsrvQuantity = eleOrderLineReservation
						.getDoubleAttribute(GCConstants.QUANTITY);
				if (YFCCommon.equals(sNode, sExtnPickupStoreID)) {
					dQuantityAvailable = dQuantityAvailable + dRsrvQuantity;
				}
			}

		}
		eleOrderLine.setAttribute("QuantityToShip",
				String.valueOf(dOrderedQty - dQuantityAvailable-dAllocatedQuantity));

		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Changed Input from updateQtyToShipForRegularReservations()"
					+ eleOrderLine.toString());
		}
		LOGGER.endTimer("Exiting: updateQtyToShipForRegularReservations");
	}
 /**
  * This method creates map for bundle parent line keys with components' reserved quantity
  *   which are required  for calculating the reserved quantity of bundle
  * @param docEleOrder
  * @param hasBundleKeyMapUpdated
  * @param mBundleKeyDetails
  */
	private void updateBundleKeyReservedQtyForBundle(Document docEleOrder,
			boolean hasBundleKeyMapUpdated,
			Map<String, String> mBundleKeyDetails) {
		  LOGGER.endTimer("Entering: updateBundleKeyReservedQtyForBundle");
		  if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("Input to updateBundleKeyReservedQtyForBundle()"
						+ docEleOrder.toString()+"hasBundleKeyMapUpdated"+hasBundleKeyMapUpdated+"mBundleKeyDetails"+mBundleKeyDetails);
			}
		YFCDocument yfcOrderDoc = YFCDocument.getDocumentFor(docEleOrder);
		YFCElement eleOrder = yfcOrderDoc.getDocumentElement();
		YFCElement eleExtnOrder = eleOrder.getChildElement(GCConstants.EXTN);
		String sExtnPickupStoreID = eleExtnOrder
				.getAttribute(GCConstants.EXTN_PICK_UP_STORE_ID);
		YFCElement eleOrderlines = eleOrder
				.getChildElement(GCConstants.ORDER_LINES);
		YFCNodeList<YFCElement> nlOrderLine = eleOrderlines
				.getElementsByTagName(GCConstants.ORDER_LINE);

		for (YFCElement eleOrderLine : nlOrderLine) {
			YFCElement sBundleParentLine = eleOrderLine
					.getChildElement("BundleParentLine");
			if (!YFCCommon.isVoid(sBundleParentLine)) {
				String sParentOrderLineKey = sBundleParentLine
						.getAttribute("OrderLineKey");
				if (!mBundleKeyDetails.containsKey(sParentOrderLineKey)) {

					YFCElement eleItem = eleOrderLine.getChildElement("Item");
					String sComponentItemID = eleItem.getAttribute("ItemID");
					YFCElement eleOrderLineReservations = eleOrderLine
							.getChildElement("OrderLineReservations");
					if (!YFCCommon.isVoid(eleOrderLineReservations)) {
						double dTotalComponentReservedQty = 0;
						YFCNodeList<YFCElement> nlOrderLineReservation = eleOrderLineReservations
								.getElementsByTagName(GCConstants.ORDER_LINE_RESERVATION);

						for (int h = 0; h < nlOrderLineReservation.getLength(); h++) {
							YFCElement eleOrderLineReservation = nlOrderLineReservation
									.item(h);
							String sRsvrShipNode = eleOrderLineReservation
									.getAttribute(GCConstants.NODE);
							double dComponentReservedQty = eleOrderLineReservation
									.getDoubleAttribute(GCConstants.QUANTITY);
							if (YFCCommon.equals(sRsvrShipNode,
									sExtnPickupStoreID)) {
								dTotalComponentReservedQty = dTotalComponentReservedQty
										+ dComponentReservedQty;
								LOGGER.verbose("Toatl component reserved Quantity"
										+ dTotalComponentReservedQty);
							}
						}
						String sValue = sComponentItemID + ":"
								+ dTotalComponentReservedQty;
						mBundleKeyDetails.put(sParentOrderLineKey, sValue);
					}
					}
				}
			}
		  if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("Changed Input from updateBundleKeyReservedQtyForBundle()"
						+ docEleOrder.toString()+"hasBundleKeyMapUpdated"+hasBundleKeyMapUpdated+"mBundleKeyDetails"+mBundleKeyDetails);
			}
		  LOGGER.endTimer("Exititng: updateBundleKeyReservedQtyForBundle");
		}
	 /**
	   * In case of DC sourced item (APO/FPO/PO Box address) or prepaid, it has to be fulfilled from DC
	   * only. In that case QuantityToShip is always same as ordered qty
	   *
	   * @param eleOrderLine
	   * @param sSourcingClassification
	   * @return
	   */
  private boolean updateQtyToShipAsPerSourcing(YFCElement eleOrderLine, String sSourcingClassification) {
    double dOrderedQty = eleOrderLine.getDoubleAttribute(GCConstants.ORDERED_QTY);
    if (GCConstants.DC_SOURCED_SOURCING.equalsIgnoreCase(sSourcingClassification)
        || GCConstants.PREPAID_SOURCING.equalsIgnoreCase(sSourcingClassification)) {
      eleOrderLine.setDoubleAttribute("QuantityToShip", dOrderedQty);
      return true;
    }
    return false;
  }

  /**
   * This method creates PromiseLine element required for findInventory call.
   * findInventory is called to get Quantity that is being picked from the
   * pickup store
   *
   * @param eleOrderLine
   * @param elePromiseLines
   * @param sExtnPickupStoreID
   */
  private void createPromiseLineForFindInventory(YFCElement eleOrderLine,
      YFCElement elePromiseLines, String sExtnPickupStoreID) {
    // check if the order line is a bundle item
    LOGGER.endTimer("Entering: createPromiseLineForFindInventory");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input elements to createPromiseLineForFindInventory:"
          + "OrderLine:"
          + eleOrderLine.toString()
          + " PromiseLines:"
          + elePromiseLines.toString()
          + " ExtnPickupStoreID:"
          + sExtnPickupStoreID);
    }
    String sPrimeLineNo = eleOrderLine
        .getAttribute(GCConstants.PRIME_LINE_NO);
    YFCElement eleLinePriceInfo = eleOrderLine
        .getChildElement("LinePriceInfo");
    String sUnitPrice = eleLinePriceInfo
        .getAttribute(GCConstants.UNIT_PRICE);

		YFCElement eleItem = eleOrderLine.getChildElement(GCConstants.ITEM);
		String sItemID = eleItem.getAttribute(GCConstants.ITEM_ID);
		String sUnitOfMeasure = eleItem
				.getAttribute(GCConstants.UNIT_OF_MEASURE);
		String sProductClass = eleItem.getAttribute(GCConstants.PRODUCT_CLASS);
		double dOrderedQty = eleOrderLine.getDoubleAttribute("QuantityToShip");
		String sFulfillmentType = eleOrderLine
				.getAttribute(GCConstants.FULFILLMENT_TYPE);

		YFCElement elePromiseLine = elePromiseLines
				.createChild(GCConstants.PROMISE_LINE);
		elePromiseLine.setAttribute(GCConstants.LINE_ID, sPrimeLineNo);
		elePromiseLine.setAttribute(GCConstants.ITEM_ID, sItemID);
		elePromiseLine
		.setAttribute(GCConstants.UNIT_OF_MEASURE, sUnitOfMeasure);
		elePromiseLine.setAttribute(GCConstants.PRODUCT_CLASS, sProductClass);
		elePromiseLine.setAttribute(GCConstants.REQUIRED_QTY,
				String.valueOf(dOrderedQty));
		elePromiseLine.setAttribute(GCConstants.FULFILLMENT_TYPE,
				sFulfillmentType);
		YFCElement elePromiseLineExt = elePromiseLine.getChildElement(GCConstants.EXTN, true);
		elePromiseLineExt.setAttribute("ExtnUnitPrice", sUnitPrice);
		YFCElement eleOrderLineExt = eleOrderLine.getChildElement(GCConstants.EXTN);
		if(!YFCCommon.isVoid(eleOrderLineExt)&&!YFCCommon.isVoid(eleOrderLineExt.getAttribute("ExtnIsStoreClearance"))){
			elePromiseLineExt.setAttribute("ExtnIsStoreClearance", eleOrderLineExt.getAttribute("ExtnIsStoreClearance"));
		}
		YFCElement eleShipNodes = elePromiseLine
				.createChild(GCConstants.SHIP_NODES);
		YFCElement eleShipNode = eleShipNodes
				.createChild(GCConstants.SHIP_NODE);
		eleShipNode.setAttribute(GCConstants.NODE, sExtnPickupStoreID);

	}

	/**
	 * This method updates the Quantity to Ship after receiving pickup store
	 * quantity from find Inventory
	 *
	 * @param findInvOutpt
	 * @param nlOrderLine
	 * @param sExtnPickupStoreID
	 */
	private void updateQtyToShipFromFindInvCall(YFCDocument findInvOutpt,
			YFCNodeList<YFCElement> nlOrderLine, String sExtnPickupStoreID) {
		LOGGER.beginTimer("Entering: updateQtyToShipFromFindInvCall");
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Input elements to updateQtyToShipFromFindInvCall:"
					+ "FindInventoryOutput:" + findInvOutpt.toString()
					+ " NodeListOrderLine:" + nlOrderLine.toString()
					+ " ExtnPickupStoreID:" + sExtnPickupStoreID);
		}
		YFCElement elePromiseOut = findInvOutpt.getDocumentElement();
		YFCElement eleOptionOut = elePromiseOut.getChildElement(
				"SuggestedOption").getChildElement("Option");
		if (!YFCCommon.isVoid(eleOptionOut)) {
			YFCNodeList<YFCElement> nlPromiselineOut = eleOptionOut
					.getChildElement(GCConstants.PROMISE_LINES)
					.getElementsByTagName(GCConstants.PROMISE_LINE);

			for (YFCElement elePromiselineOut : nlPromiselineOut) {

				YFCNodeList<YFCElement> nlAssignmentOut = elePromiselineOut
						.getChildElement("Assignments").getElementsByTagName(
								"Assignment");
				for (YFCElement eleAssignment : nlAssignmentOut) {
					double dAvailableQty = eleAssignment
							.getDoubleAttribute(GCConstants.QUANTITY);
					String sShipNode = eleAssignment
							.getAttribute(GCConstants.SHIP_NODE);
					if (YFCCommon.equals(sExtnPickupStoreID, sShipNode)) {
						for (YFCElement eleOrderLine : nlOrderLine) {
							executeFinalUpdateQtyToShip(eleOrderLine,
									elePromiselineOut, dAvailableQty);
						}

					}
				}
			}
		}
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Changed elements from updateQtyToShipFromFindInvCall:"
					+ " NodeListOrderLine:"
					+ nlOrderLine.toString()
					+ " ExtnPickupStoreID:" + sExtnPickupStoreID);
			LOGGER.endTimer("Exiting: updateQtyToShipFromFindInvCall");
		}
	}

	/**
	 * This methods update Qty to ship for Serial and Vintage Item. This is
	 * marked as "0". Because it has to be shipped from pick up store in casae
	 * of PICKUP_IN_STORE fulfillmenttype
	 *
	 * @param eleOrderLine
	 * @return
	 */
	private boolean updateQtyToShipForSerialItem(
			YFCElement eleOrderLine) {
		LOGGER.beginTimer("Entering: updateQtyToShipForSerialAndVintageItem");
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Input elements to updateQtyToShipForSerialAndVintageItem:"
					+ " Element OrderLine:" + eleOrderLine.toString());
		}
		YFCElement eleItemOrderLineInvAttRequest = eleOrderLine
				.getChildElement("OrderLineInvAttRequest");
		if (!(YFCCommon.isVoid(eleItemOrderLineInvAttRequest) || YFCCommon
				.isVoid(eleItemOrderLineInvAttRequest
						.getAttribute(GCConstants.BATCH_NO)))) {
			eleOrderLine.setAttribute("QuantityToShip", "0");
			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("Changed elements from updateQtyToShipForSerialAndVintageItem:"
						+ " Element OrderLine:"
						+ eleOrderLine.toString()
						+ " updateQtyToShipForSerialAndVintageItem: true");
				LOGGER.endTimer("Exiting: updateQtyToShipFromFindInvCall");
			}
			return true;
		}
		LOGGER.endTimer("Exiting: updateQtyToShipForSerialAndVintageItem");
		return false;
	}

	/**
	 * This method is updates the qty to ship for clearance item. in case of
	 * clearance item but request is not clearance. All quantity has to be
	 * shipped from MFI hence QuantityToShip=OrderedQty
	 *
	 * @param eleOrderLine
	 * @return
	 */
	private boolean updateQtyToShipForClearanceItem(YFCElement eleOrderLine) {
		LOGGER.beginTimer("Entering: updateQtyToShipForSerialAndVintageItem");
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Input elements to updateQtyToShipForClearanceItem:"
					+ " Element OrderLine:" + eleOrderLine.toString());
		}
		YFCElement eleOrderLineExtn = eleOrderLine
				.getChildElement(GCConstants.EXTN);
		String sExtnIsStoreClearance = eleOrderLineExtn
				.getAttribute("ExtnIsStoreClearance");
		String sExtnIsClearanceItem = eleOrderLine
				.getChildElement(GCConstants.ITEM_DETAILS)
				.getChildElement(GCConstants.EXTN)
				.getAttribute("ExtnIsClearanceItem");
		if (YFCCommon.equals(sExtnIsClearanceItem, GCConstants.FLAG_Y)
				&& !YFCCommon.equals(sExtnIsStoreClearance, GCConstants.FLAG_Y)) {
			double dOrderedQty = eleOrderLine
					.getDoubleAttribute(GCConstants.ORDERED_QTY);
			eleOrderLine.setAttribute("QuantityToShip",
					String.valueOf(dOrderedQty));
			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("Changed elements from updateQtyToShipForClearanceItem:"
						+ " Element OrderLine:"
						+ eleOrderLine.toString()
						+ "updateQtyToShipForSerialAndVintageItem: true");
			}
			LOGGER.endTimer("Exiting: updateQtyToShipForSerialAndVintageItem");
			return true;
		}
		LOGGER.endTimer("Exiting: updateQtyToShipForSerialAndVintageItem: false");
		return false;

	}

  /**
   * This method is used to update QuantityToShip at the last when
   * findInventory call is already made and latest quantity is available which
   * can not be picked from the same store
   *
   * @param eleOrderLine
   * @param elePromiselineOut
   * @param dAvailableQty
   */
  private void executeFinalUpdateQtyToShip(YFCElement eleOrderLine,
      YFCElement elePromiselineOut, double dAvailableQty) {
    LOGGER.beginTimer("Entering: executeFinalUpdateQtyToShip");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input elements to executeFinalUpdateQtyToShip:"
          + " Element OrderLine:" + eleOrderLine.toString()
          + " Element PromiseLineOut:" + elePromiselineOut.toString()
          + " AvailableQty:" + dAvailableQty);
    }
    String sPrimeLineNo = eleOrderLine
        .getAttribute(GCConstants.PRIME_LINE_NO);

    String sItemIDIn = "";
    YFCElement itemEle = eleOrderLine.getChildElement("Item");
    if (!YFCCommon.isVoid(itemEle)) {
      sItemIDIn = itemEle.getAttribute(GCConstants.ITEM_ID);
    }

    double dOrderedQty = eleOrderLine
        .getDoubleAttribute(GCConstants.ORDERED_QTY);
    String sItemIDOut = elePromiselineOut.getAttribute(GCConstants.ITEM_ID);
    String sLineId = elePromiselineOut.getAttribute(GCConstants.LINE_ID);

		if (YFCCommon.equals(sLineId, sPrimeLineNo)
				&& YFCCommon.equals(sItemIDIn, sItemIDOut) && dAvailableQty > 0) {
			double dUnAvailableQty = dOrderedQty - dAvailableQty;

			if (dUnAvailableQty <= 0) {
				eleOrderLine.setAttribute("QuantityToShip", "0");
			} else {
				eleOrderLine.setAttribute("QuantityToShip",
						String.valueOf(dUnAvailableQty));
			}
		}
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Changed elements from executeFinalUpdateQtyToShip:"
					+ " Element OrderLine:" + eleOrderLine.toString()
					+ " Element PromiseLineOut:" + elePromiselineOut.toString()
					+ " AvailableQty:" + dAvailableQty);
			LOGGER.endTimer("Exiting: executeFinalUpdateQtyToShip");
		}
	}

	public static boolean removeCancelledOrderLinesFromOrder(
			Document docEleOrder) {
		YFCDocument yfceDocEleOrder = YFCDocument.getDocumentFor(docEleOrder);
		YFCElement eleOrder = yfceDocEleOrder.getDocumentElement();
		YFCElement eleOrderLines = eleOrder.getChildElement("OrderLines");
		double dMaxLineStatus = 0;
		double dMinLineStatus = 0;
		YFCNodeList<YFCElement> nlOrderLine = eleOrderLines
				.getElementsByTagName("OrderLine");
		int nlLength = nlOrderLine.getLength();
		for (int i = 0; i < nlLength; i++) {
			YFCElement eleOrderLine = nlOrderLine.item(i);
			dMaxLineStatus = eleOrderLine.getDoubleAttribute("MaxLineStatus");
			dMinLineStatus = eleOrderLine.getDoubleAttribute("MinLineStatus");
			if (YFCCommon.equals(dMaxLineStatus, 9000)
					&& YFCCommon.equals(dMinLineStatus, 9000)) {
				eleOrderLine.getParentNode().removeChild(eleOrderLine);
				nlLength--;
				i--;
			}
		}
		if (nlLength == 0) {
			// if All the OrderLines are cancelled and deleted return true
			return true;
		}
		// if some or none are cancelled and all of those lines are deleted
		// then return false
		return false;
		// GCStore-791-Phase-2 Ends
	}

	private static double getAllocatedQuantity(YFCElement eleOrderLine) {
		LOGGER.beginTimer("getAllocatedQuantity");
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Input to getAllocatedQuantity:"
					+ eleOrderLine.toString());
		}
		YFCElement eleOrderStatuses = eleOrderLine
				.getChildElement("OrderStatuses");
		YFCNodeList<YFCElement> nlOrderStatus = eleOrderStatuses
				.getElementsByTagName("OrderStatus");
		double dAllocatedQty = 0;
		for (YFCElement eleOrderStatus : nlOrderStatus) {
			String sStatus = eleOrderStatus.getAttribute("Status");
			int iStatusDiff = YFCCommon.compareStrings(sStatus, "3200");
			if (YFCCommon.equals(sStatus, "1500") || (iStatusDiff >= 0
					&& !YFCCommon.equals(sStatus, "9000"))) {
				dAllocatedQty = dAllocatedQty
						+ eleOrderStatus.getDoubleAttribute("StatusQty");
			}
		}
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("dAllocatedQty"
					+ dAllocatedQty);
		}
		LOGGER.endTimer("getAllocatedQuantity");
		return dAllocatedQty;
	}

	public static boolean isOrderStorePickup(Document docEleOrder) {
		YFCDocument yfcOrderDoc = YFCDocument.getDocumentFor(docEleOrder);

		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Input to isOrderStorePickup:"
					+ yfcOrderDoc.toString());
		}

		YFCElement eleOrder = yfcOrderDoc.getDocumentElement();
		YFCElement eleOrderlines = eleOrder
				.getChildElement(GCConstants.ORDER_LINES);
		YFCNodeList<YFCElement> nlOrderLine = eleOrderlines
				.getElementsByTagName(GCConstants.ORDER_LINE);
		for (YFCElement eleOrderLine : nlOrderLine) {
			double sQuantityToShip = eleOrderLine
					.getDoubleAttribute("QuantityToShip");
			if (sQuantityToShip > 0) {
				if (LOGGER.isVerboseEnabled()) {
					LOGGER.verbose("Output from isOrderStorePickup:" + "false");
				}
				return false;
			}
		}
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Output from isOrderStorePickup:" + "true");
		}
		return true;
	}
}
