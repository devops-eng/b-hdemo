package com.gc.api;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCGetReturnPaymentMethod {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCGetReturnPaymentMethod.class);

  public Document getReturnPaymentMethod(YFSEnvironment env, Document inDoc) throws GCException {

    Document docPaymentMethod = GCXMLUtil.getDocument("<ReturnOrder RefundType='OPM'><PaymentMethods/></ReturnOrder >");
    try {
      LOGGER.debug("Entering getReturnPaymentMethod method()");
      Document tempGOD =
          GCXMLUtil
              .getDocument("<Order><Extn/><PaymentMethods><PaymentMethod PaymentKey='' CreditCardType='' CreditCardExpDate='' CreditCardName='' CreditCardNo=''  DisplayCreditCardNo='' PaymentType='' RequestedRefundAmount='' /></PaymentMethods><OrderLines><OrderLine DerivedFromOrderHeaderKey='' /></OrderLines></Order>");
      Document outDocGOD = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_DETAILS, inDoc, tempGOD);
      String strRefundOnAccount = GCXMLUtil.getAttributeFromXPath(outDocGOD, "Order/Extn/@ExtnIsRefundOnAccount");
      String strPaymentKey = GCXMLUtil.getAttributeFromXPath(outDocGOD, "Order/Extn/@ExtnRefundPaymentKey");
      // If refund is on credit on account
      if (GCConstants.YES.equals(strRefundOnAccount)) {
        docPaymentMethod.getDocumentElement().setAttribute(GCConstants.REFUND_TYPE, GCConstants.COA);
      } else if (!YFCCommon.isVoid(strPaymentKey)) {
        // If refund is on new credit card payment type
        String strSOOHK =
            GCXMLUtil.getAttributeFromXPath(outDocGOD, "Order/OrderLines/OrderLine/@DerivedFromOrderHeaderKey");
        Document inDocSOGOD = GCXMLUtil.getDocument("<Order OrderHeaderKey='" + strSOOHK + "'/>");
        Document outDocSOGOD = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_DETAILS, inDocSOGOD, tempGOD);
        docPaymentMethod.getDocumentElement().setAttribute(GCConstants.REFUND_TYPE,
            GCConstants.NEW_REFUND_PAYMENT_METHOD);
        Element elePaymentMethod =
            GCXMLUtil.getElementByXPath(outDocSOGOD, "Order/PaymentMethods/PaymentMethod[@PaymentKey='" + strPaymentKey
                + "']");
        Element elePaymentMethodDest = docPaymentMethod.createElement(GCConstants.PAYMENT_METHOD);
        Element elePaymentMethods = GCXMLUtil.getElementByXPath(docPaymentMethod, "ReturnOrder/PaymentMethods");
        elePaymentMethods.appendChild(elePaymentMethodDest);
        GCXMLUtil.copyElement(docPaymentMethod, elePaymentMethod, elePaymentMethodDest);
      }
      LOGGER.debug("Exiting getReturnPaymentMethod method()");
    } catch (Exception e) {
      LOGGER.error(" Inside catch of GCPaymentUtils.getReturnPaymentMethod()", e);
      throw new GCException(e);
    }
    return docPaymentMethod;
  }
}
