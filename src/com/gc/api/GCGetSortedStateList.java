/**
 * Copyright © 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: Foundation Class
 *#################################################################################################################################################################
 *          Version       CR                Date              Modified By                Description
 *#################################################################################################################################################################
             1.0     Initial Version     21/05/2015         Gupta, Akash              GCSTORE-1662 Sorting the state list alphabetically
			 
#################################################################################################################################################################
 */

package com.gc.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;


public class GCGetSortedStateList {
	// initialize logger
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCGetSortedStateList.class.getName());

	public Document getSortedStateList(YFSEnvironment env, Document inDoc) {
		LOGGER.debug("Class: GCGetSortedStateList Method: getSortedStateList BEGIN");
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Incoming Doc to the method"
					+ GCXMLUtil.getXMLString(inDoc));
		}
		Document docOut = GCCommonUtil.invokeAPI(env, "getStateRegionList",
				inDoc,
				GCXMLUtil.getDocument(GCConstants.GET_STATE_REGION_LIST_TEMP));
		YFCDocument regionListOutDoc = YFCDocument.getDocumentFor(docOut);
		YFCElement regionListDoc = regionListOutDoc.getDocumentElement();
		YFCElement regionSchemaListEle = regionListDoc
				.getChildElement("RegionSchema");

		YFCNodeList<YFCElement> regionEleList = regionSchemaListEle
				.getElementsByTagName("Region");
		List<YFCElement> regionList = new ArrayList<YFCElement>(
				regionEleList.getLength());
		for (YFCElement regionEle : regionEleList) {
			regionList.add(regionEle);
		}
		Collections.sort(regionList, new SortState());
		YFCDocument newDoc = YFCDocument.createDocument("Regions");
		YFCElement elenewRoot = newDoc.getDocumentElement();
		YFCElement eleoutRegionSchema = elenewRoot.createChild("RegionSchema");
		eleoutRegionSchema.setAttribute("RegionSchemaKey",
				regionSchemaListEle.getAttribute("RegionSchemaKey"));
		for (YFCElement eleRegion : regionList) {
			YFCElement elenewRegion = newDoc.createElement("Region");
			GCXMLUtil.copyElement(newDoc, eleRegion, elenewRegion);
			eleoutRegionSchema.appendChild(elenewRegion);
		}
		return newDoc.getDocument();
	}

	static class SortState implements Comparator<YFCElement>,Serializable {

		@Override
		public int compare(YFCElement elem1, YFCElement elem2) {
			String value1 = elem1.getAttribute("RegionName");
			String value2 = elem2.getAttribute("RegionName");
			return value1.compareTo(value2);
		}

	}

}
