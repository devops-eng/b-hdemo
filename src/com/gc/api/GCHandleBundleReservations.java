package com.gc.api;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCHandleBundleReservations {


  private boolean callChangeOrder;

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCHandleBundleReservations.class);

  public boolean isCallChangeOrder() {
    return callChangeOrder;
  }

  public void setCallChangeOrder(boolean callChangeOrder) {
    this.callChangeOrder = callChangeOrder;
  }


  public Document resetParentReservations(YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("resetParentReservations");
    Document updatedInDoc = handleBundleReservations(env, inDoc);
    if (callChangeOrder) {
      YFCDocument changeOrderInputDoc = prepareChangeOrderInputDoc(YFCDocument.getDocumentFor(updatedInDoc));
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("Input Document to change Order::" + changeOrderInputDoc.toString());
      }
      GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, changeOrderInputDoc, null);
    }
    LOGGER.endTimer("resetParentReservations");
    return inDoc;

  }

  private YFCDocument prepareChangeOrderInputDoc(YFCDocument updatedInDoc) {
    LOGGER.beginTimer("prepareChangeOrderInputDoc");
    YFCDocument changeOrderInputDoc = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement changeOrderEle = changeOrderInputDoc.getDocumentElement();
    changeOrderEle.setAttribute(GCConstants.ORDER_HEADER_KEY,
        updatedInDoc.getDocumentElement().getAttribute(GCConstants.ORDER_HEADER_KEY));
    changeOrderEle.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
    YFCElement orderLines = changeOrderEle.createChild(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> orderLineNodeList = updatedInDoc.getElementsByTagName(GCConstants.ORDER_LINE);
    for (YFCElement orderLine : orderLineNodeList) {
      YFCElement orderLineEle = orderLines.createChild(GCConstants.ORDER_LINE);
      orderLineEle.setAttribute(GCConstants.ORDER_LINE_KEY, orderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
      YFCElement orderLineReservations = orderLine.getChildElement(GCConstants.ORDER_LINE_RESERVATIONS);
      orderLineEle.importNode(orderLineReservations);
    }
    LOGGER.endTimer("prepareChangeOrderInputDoc");
    return changeOrderInputDoc;
  }
  public Document handleBundleReservations(YFSEnvironment env, Document inputDoc) {
    LOGGER.beginTimer("handleBundleReservations");

    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input XML to handleBundleReservations method::" + inDoc.toString());
    }

    YFCNodeList<YFCElement> orderLineNodeList = inDoc.getElementsByTagName(GCConstants.ORDER_LINE);
    Map<String, YFCElement> orderLineMap = new HashMap<String, YFCElement>();
    for (YFCElement orderLineEle : orderLineNodeList) {
      String orderLineKey = orderLineEle.getAttribute(GCConstants.ORDER_LINE_KEY);
      orderLineMap.put(orderLineKey, orderLineEle);

    }

    for (YFCElement orderLineEle : orderLineNodeList) {
      if (LOGGER.isVerboseEnabled()) {
    	      LOGGER.verbose("Iterating for OrderLine element " + orderLineEle);
      }
      boolean isBundleParent = orderLineEle.getBooleanAttribute(GCConstants.IS_BUNDLE_PARENT);
      if (isBundleParent) {
        YFCElement orderLineReservations = orderLineEle.getChildElement(GCConstants.ORDER_LINE_RESERVATIONS);
        if (!YFCCommon.isVoid(orderLineReservations)) {
          YFCNodeList<YFCElement> orderLineReservationNodeList =
              orderLineReservations.getElementsByTagName(GCConstants.ORDER_LINE_RESERVATION);
          for (int i = 0; i < orderLineReservationNodeList.getLength(); i++) {
            callChangeOrder = true;
            YFCElement orderLineReservationEle = orderLineReservationNodeList.item(i);
            String itemId = orderLineReservationEle.getAttribute(GCConstants.ITEM_ID);
            String qty = orderLineReservationEle.getAttribute(GCConstants.QUANTITY);
            String orderLineKey = getComponentOrderLineKey(itemId, qty, orderLineEle, orderLineMap);
            if(YFCCommon.isVoid(orderLineKey)){
            	LOGGER.verbose("Could not find component's orderline key so going for next iteration.");
            	continue;
            }
            YFCElement componentOrderLineEle = orderLineMap.get(orderLineKey);
            YFCElement componentOrderLineReservations =
                componentOrderLineEle.getChildElement(GCConstants.ORDER_LINE_RESERVATIONS, true);
            YFCElement componentOrderLineReservation =
                componentOrderLineReservations.createChild(GCConstants.ORDER_LINE_RESERVATION);
            componentOrderLineReservation.setAttribute(GCConstants.ITEM_ID, itemId);
            componentOrderLineReservation.setAttribute(GCConstants.PRODUCT_CLASS,
                orderLineReservationEle.getAttribute(GCConstants.PRODUCT_CLASS));
            componentOrderLineReservation.setAttribute(GCConstants.UNIT_OF_MEASURE,
                orderLineReservationEle.getAttribute(GCConstants.UNIT_OF_MEASURE));
            componentOrderLineReservation.setAttribute(GCConstants.QUANTITY,
                orderLineReservationEle.getAttribute(GCConstants.QUANTITY));
            componentOrderLineReservation.setAttribute(GCConstants.NODE,
                orderLineReservationEle.getAttribute(GCConstants.NODE));
            componentOrderLineReservation.setAttribute(GCConstants.REQUESTED_RESERVATION_DATE,
                orderLineReservationEle.getAttribute(GCConstants.REQUESTED_RESERVATION_DATE));
            componentOrderLineReservation.setAttribute(GCConstants.PRODUCT_AVAILABILITY_DATE,
                orderLineReservationEle.getAttribute(GCConstants.PRODUCT_AVAILABILITY_DATE));
            componentOrderLineReservation.setAttribute(GCConstants.BATCH_NO,
                orderLineReservationEle.getAttribute(GCConstants.BATCH_NO));
            // remove parent reservations
            orderLineReservationEle.getParentNode().removeChild(orderLineReservationEle);
            i--;
          }
          orderLineReservations.setAttribute(GCConstants.RESET, GCConstants.YES);
        }
      }
    }
    LOGGER.endTimer("handleBundleReservations");
    return inDoc.getDocument();

  }

  private String getComponentOrderLineKey(String itemId, String qty, YFCElement orderLineEle,
      Map<String, YFCElement> orderLineMap) {
    LOGGER.beginTimer("getComponentOrderLineKey");
    YFCNodeList<YFCElement> bundleComponentNodeList = orderLineEle.getElementsByTagName("BundleComponent");
    for (YFCElement bundleComponentEle : bundleComponentNodeList) {
      String orderLineKey = bundleComponentEle.getAttribute(GCConstants.ORDER_LINE_KEY);
      YFCElement componentOrderLineEle = orderLineMap.get(orderLineKey);
    //  String orderedQty = componentOrderLineEle.getAttribute(GCConstants.ORDERED_QTY);
    //  Double dOrderedQty = Double.parseDouble(orderedQty);
      LOGGER.verbose("Iterating for component orderline key="+orderLineKey);
     // Double dQty = Double.parseDouble(qty);
      YFCElement itemEle = componentOrderLineEle.getChildElement(GCConstants.ITEM);
      String componentItemId = itemEle.getAttribute(GCConstants.ITEM_ID);
      if (YFCCommon.equals(itemId, componentItemId)) {
    	  LOGGER.verbose("orderline key="+orderLineKey+ " is matching with reservation element. So returning this.");
        LOGGER.endTimer("getComponentOrderLineKey");
        return orderLineKey;
      }
    }
    LOGGER.endTimer("getComponentOrderLineKey");
    return null;
  }

}
