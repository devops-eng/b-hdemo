package com.gc.api;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCLogUtils;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.shared.ycp.YFSContext;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class GCImportOrdersToOMS implements YIFCustomApi {
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCManageCustomer.class.getName());

  /**
   * 
   * This class is used to import the orders to OMS after performing some steps .It receives
   * multiapi document as an input.If the order to be imported is already present in OMS it
   * deactivates the existing order by changing its enterprise code.
   * 
   * @param env
   * @param inDoc
   * @return
   * @throws GCException , SQLException
   */
  public void importOrders(YFSEnvironment env, Document inDoc) throws GCException, SQLException {
    LOGGER.beginTimer("GCImportOrdersToOMS.importOrders");
    GCLogUtils.verboseLog(LOGGER, "GCImportOrdersToOMS.importOrders--Begin");
    GCLogUtils.verboseLog(LOGGER, "Input Document  : ", GCXMLUtil.getXMLString(inDoc));
    stampBillToId(env, inDoc);
    deactivateOMSOrder(env, inDoc);
    GCLogUtils.verboseLog(LOGGER, "Input to MultiAPI  : ", GCXMLUtil.getXMLString(inDoc));
    Document outDoc = GCCommonUtil.invokeAPI(env, GCConstants.MULTI_API, inDoc);
    GCLogUtils.verboseLog(LOGGER, "Output of  MultiAPI  : ", GCXMLUtil.getXMLString(outDoc));
    GCLogUtils.verboseLog(LOGGER, "GCImportOrdersToOMS.importOrders--End");
    LOGGER.endTimer("GCImportOrdersToOMS.importOrders");
  }


  /**
   * 
   * This method checks if the order to be imported is already present in OMS. If present , it
   * deactivates them by changing the EnterpriseCode for existing orders
   * 
   * @param env
   * @param inDoc
   * @return
   * @throws GCException , SQLException
   */
  private void deactivateOMSOrder(YFSEnvironment env, Document inDoc) throws GCException, SQLException {
    LOGGER.beginTimer("GCImportOrdersToOMS.deactivateOMSOrder");
    GCLogUtils.verboseLog(LOGGER, "GCImportOrdersToOMS.deactivateOMSOrder--Begin");
    GCLogUtils.verboseLog(LOGGER, "Input Document to  deactivateOMSOrder method : ", GCXMLUtil.getXMLString(inDoc));
    String orderHeaderKey = null;
    Element eleOrd =
        GCXMLUtil.getElementByXPath(inDoc, "MultiApi/API[@Name='importOrder']/Input/Order[@DocumentType='0001']");
    Element eleExtn = (Element) eleOrd.getElementsByTagName((GCConstants.EXTN)).item(0);
    String sourceOrdNo = eleExtn.getAttribute(GCConstants.EXTN_SOURCE_ORDER_NO);
    YFCDocument getOrderListIp = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleOrdIp = getOrderListIp.getDocumentElement();
    YFCElement eleOrdExtn = eleOrdIp.createChild(GCConstants.EXTN);
    eleOrdExtn.setAttribute(GCConstants.EXTN_SOURCE_ORDER_NO, sourceOrdNo);
    YFCDocument getOrdListTemp = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_TEMP_DATA_LOAD);
    YFCDocument getOrderListOp =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListIp, getOrdListTemp);
    YFCElement eleOrdListOp = getOrderListOp.getDocumentElement();
    String orderCount = eleOrdListOp.getAttribute(GCConstants.TOTAL_ORDER_LIST);
    int count = Integer.parseInt(orderCount);
    if (count == 1) {
      YFCElement eleOrder = eleOrdListOp.getChildElement(GCConstants.ORDER);
      orderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
      String ordComplete = eleOrder.getAttribute(GCConstants.ORDER_COMPLETE);
      if (!YFCCommon.equals(GCConstants.YES, ordComplete)) {
        YFSException ex = new YFSException();
        ex.setErrorCode("Existing OMS order is not complete");
        ex.setErrorDescription("Existing OMS order is not complete , it cannot be disabled");
        throw ex;

      }
      NodeList nlShipment = GCXMLUtil.getNodeListByXpath(inDoc, "MultiApi/API[@Name='importShipment']/Input/Shipment");
      for (int i = 0; i < nlShipment.getLength(); i++) {
        Element eleShipment = (Element) nlShipment.item(i);
        eleShipment.setAttribute(GCConstants.SHIPMENT_NO, sourceOrdNo + "_" + count + "_" + i + 1);
      }

    } else if (count > 1) {
      orderHeaderKey = getHeaderKeyForLatestOrder(eleOrdListOp);
      NodeList nlShipment = GCXMLUtil.getNodeListByXpath(inDoc, "MultiApi/API[@Name='importShipment']/Input/Shipment");
      for (int i = 0; i < nlShipment.getLength(); i++) {
        Element eleShipment = (Element) nlShipment.item(i);
        eleShipment.setAttribute(GCConstants.SHIPMENT_NO, sourceOrdNo + "_" + count + "_" + i + 1);
      }
      if (!YFCCommon.isVoid(orderHeaderKey) ) {
        deactivateCompleteOrder(env, orderHeaderKey, count);
      }

    }
    GCLogUtils.verboseLog(LOGGER, "GCImportOrdersToOMS.deactivateOMSOrder--End");
    LOGGER.endTimer("GCImportOrdersToOMS.deactivateOMSOrder");
  }



  /**
   * 
   * This method deactivates the existing OMS order and corresponding shipments and return orders.
   * 
   * @param env
   * @param orderHeaderKey
   * @param count
   * @return
   * @throws GCException , SQLException
   */
  private void deactivateCompleteOrder(YFSEnvironment env, String orderHeaderKey, int count) throws GCException,
  SQLException {
    updateEnterpriseCode(env, orderHeaderKey, count);
    Set<String> derivedKeySet = getDerivedHeaderKeySet(env, orderHeaderKey);
    if (!derivedKeySet.isEmpty()) {
      for (String derivedHeaderKey : derivedKeySet) {
        updateEnterpriseCode(env, derivedHeaderKey, count);
      }
    }
    YFCDocument getShipmentListIp = getShipmentListInput(orderHeaderKey);
    YFCDocument getShipmentListOpTemp = YFCDocument.getDocumentFor(GCConstants.GET_SHIPMENT_LIST_TMP_DATA_LOAD);
    YFCDocument getShipmentListOp =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, getShipmentListIp, getShipmentListOpTemp);
    YFCNodeList<YFCElement> nlShipment = getShipmentListOp.getElementsByTagName(GCConstants.SHIPMENT);
    for (YFCElement eleShipment : nlShipment) {
      String shipmentKey = eleShipment.getAttribute(GCConstants.SHIPMENT_KEY);
      updateEntCodeForShipment(env, shipmentKey, count);
    }

  }



  /**
   * 
   * This method deactivates the existing shipments by updating EnterpriseCode in database
   * 
   * @param env
   * @param shipmentKey
   * @param count
   * @return
   * @throws GCException , SQLException
   */
  private void updateEntCodeForShipment(YFSEnvironment env, String shipmentKey, int count) throws GCException,
  SQLException {
    LOGGER.beginTimer("GCImportOrdersToOMS.updateEntCodeForShipment");
    GCLogUtils.verboseLog(LOGGER, "GCImportOrdersToOMS.updateEntCodeForShipment--Begin");
    Connection conObj = null;
    Statement objStatement = null;
    ResultSet objResultSet = null;
    try {
      conObj = ((YFSContext) env).getConnectionForTableType(GCConstants.GUITAR);
      objStatement = conObj.createStatement();
      int updatedCount = count - 1;
      objResultSet =
          objStatement.executeQuery("Update YFS_SHIPMENT set Enterprise_Code='GC_IMPORT_" + updatedCount
              + "'where Shipment_Key='" + shipmentKey + "'");

    } catch (Exception e) {
      LOGGER.error(" Inside catch block of GCImportOrdersToOMS.updateEnterpriseCode(), Exception is :", e);
      throw new GCException(e);
    } finally {
      if (objResultSet != null) {
        objResultSet.close();
      }
      if (objStatement != null) {
        objStatement.close();
      }
    }
    GCLogUtils.verboseLog(LOGGER, "GCImportOrdersToOMS.updateEntCodeForShipment--End");
    LOGGER.endTimer("GCImportOrdersToOMS.updateEntCodeForShipment");

  }


private PreparedStatement extracted(Connection conObj, String sStatement)
		throws SQLException {
	PreparedStatement prepareStatement;
	prepareStatement = conObj.prepareStatement(sStatement);
	return prepareStatement;
}

  /**
   * 
   * This method prepares the input for getShipmentList to retrieve the shipments corresponding to
   * the given OrderHeaderKey
   * 
   * @param orderHeaderKey
   * @return
   */
  private YFCDocument getShipmentListInput(String orderHeaderKey) {
    LOGGER.beginTimer("GCImportOrdersToOMS.getShipmentListInput");
    GCLogUtils.verboseLog(LOGGER, "GCImportOrdersToOMS.getShipmentListInput--Begin");

    // Preparing Input For getShipmentList API call
    YFCDocument getShipmentListIp = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement rootEle = getShipmentListIp.getDocumentElement();
    YFCElement eleShipmentLines = rootEle.createChild(GCConstants.SHIPMENT_LINES);
    YFCElement eleShipmentLine = eleShipmentLines.createChild(GCConstants.SHIPMENT_LINE);
    eleShipmentLine.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);

    GCLogUtils.verboseLog(LOGGER, "getShipmentList input  : ", getShipmentListIp.toString());
    GCLogUtils.verboseLog(LOGGER, "GCImportOrdersToOMS.getShipmentListInput--End");
    LOGGER.endTimer("GCImportOrdersToOMS.getShipmentListInput");
    return getShipmentListIp;
  }


  /**
   * 
   * This method retrieves a set of return order header keys derived from the order whose
   * OrderHeaderKey is provided in the input
   * 
   * @param env
   * @param orderHeaderKey
   * @return
   */
  private Set<String> getDerivedHeaderKeySet(YFSEnvironment env, String orderHeaderKey) {
    LOGGER.beginTimer("GCImportOrdersToOMS.getDerivedHeaderKeySet");
    GCLogUtils.verboseLog(LOGGER, "GCImportOrdersToOMS.getDerivedHeaderKeySet--Begin");
    Set<String> derivedKeySet = new HashSet<String>();
    YFCDocument getOrdLineListIp = YFCDocument.createDocument(GCConstants.ORDER_LINE);
    YFCElement eleIp = getOrdLineListIp.getDocumentElement();
    eleIp.setAttribute(GCConstants.DERIVED_FROM_ORDER_HEADER_KEY, orderHeaderKey);
    YFCDocument templateDoc = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LINE_LIST_TEMP_EMAIL);
    GCLogUtils.verboseLog(LOGGER, "getOrderLineList input  : ", getOrdLineListIp.toString());
    YFCDocument opDoc = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LINE_LIST, getOrdLineListIp, templateDoc);
    GCLogUtils.verboseLog(LOGGER, "getOrderLineList output  : ", opDoc.toString());
    YFCElement eleOp = opDoc.getDocumentElement();
    YFCNodeList<YFCElement> nlOrderLine = eleOp.getElementsByTagName(GCConstants.ORDER_LINE);
    for(YFCElement eleOrderLine : nlOrderLine){
      YFCElement eleDerivedOrder = eleOrderLine.getChildElement(GCConstants.ORDER);
      String docType = eleDerivedOrder.getAttribute(GCConstants.DOCUMENT_TYPE);
      if (YFCCommon.equals(docType, GCConstants.ZERO_ZERO_ZERO_THREE)){
        String derOrderHeaderKey = eleDerivedOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
        derivedKeySet.add(derOrderHeaderKey);
      }

    }
    GCLogUtils.verboseLog(LOGGER, "GCImportOrdersToOMS.getDerivedHeaderKeySet--End");
    LOGGER.endTimer("GCImportOrdersToOMS.getDerivedHeaderKeySet");
    return derivedKeySet;
  }


  /**
   * 
   * This method retrieves the OrderHeaderKey of the latest imported order from the list of existing
   * orders in OMS provided in input
   * 
   * @param eleOrdListOp
   * @return
   */
  private String getHeaderKeyForLatestOrder(YFCElement eleOrdListOp) {
    LOGGER.beginTimer("GCImportOrdersToOMS.getHeaderKeyForLatestOrder");
    GCLogUtils.verboseLog(LOGGER, "GCImportOrdersToOMS.getHeaderKeyForLatestOrder--Begin");
    String orderHeaderKey = null;
    YFCNodeList<YFCElement> nlOrder = eleOrdListOp.getElementsByTagName(GCConstants.ORDER);
    for (YFCElement eleOrder : nlOrder) {
      String entCode = eleOrder.getAttribute(GCConstants.ENTERPRISE_CODE);
      if (YFCUtils.equals(GCConstants.GC_IMPORT, entCode)) {
        orderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
        break;
      }
    }
    GCLogUtils.verboseLog(LOGGER, "GCImportOrdersToOMS.getHeaderKeyForLatestOrder--End");
    LOGGER.endTimer("GCImportOrdersToOMS.getHeaderKeyForLatestOrder");
    return orderHeaderKey;
  }


  /**
   * 
   * This method deactivates the existing orders by updating EnterpriseCode in database. If it is
   * original order , then EnterpriseCode is updated to GC_DUMMY else the count is increased for the
   * import orders.
   * 
   * @param env
   * @param orderHeaderKey
   * @param count
   * @return
   * @throws GCException , SQLException
   */
  private void updateEnterpriseCode(YFSEnvironment env, String orderHeaderKey, int count) throws GCException,
  SQLException {
    LOGGER.beginTimer("GCImportOrdersToOMS.updateEnterpriseCode");
    GCLogUtils.verboseLog(LOGGER, "GCImportOrdersToOMS.updateEnterpriseCode--Begin");
    Connection conObj = null;
    Statement objStatement = null;
    ResultSet objResultSet = null;
    try {
      conObj = ((YFSContext) env).getConnectionForTableType(GCConstants.GUITAR);
      objStatement = conObj.createStatement();
      int updatedCount = count - 1;
      objResultSet =
          objStatement.executeQuery("update YFS_ORDER_HEADER set Enterprise_Key='GC_IMPORT_" + updatedCount
              + "' where Order_Header_Key='" + orderHeaderKey + "'");

    } catch (Exception e) {
      LOGGER.error(" Inside catch block of GCImportOrdersToOMS.updateEnterpriseCode(), Exception is :", e);
      throw new GCException(e);
    } finally {
      if (objResultSet != null) {
        objResultSet.close();
      }
      if (objStatement != null) {
        objStatement.close();
      }
    }
    GCLogUtils.verboseLog(LOGGER, "GCImportOrdersToOMS.updateEnterpriseCode--End");
    LOGGER.endTimer("GCImportOrdersToOMS.updateEnterpriseCode");
  }


  /**
   * 
   * This method stamps BillToID in import Order xml input for Sales order if it is not present
   * 
   * @param env
   * @param inDoc
   * @return
   * @throws GCException
   */
  private void stampBillToId(YFSEnvironment env, Document inDoc) throws GCException {
    LOGGER.beginTimer("GCImportOrdersToOMS.stampBillToId");
    GCLogUtils.verboseLog(LOGGER, "GCImportOrdersToOMS.stampBillToId--Begin");
    String billToId = "";
    GCManageCustomer manageCust = new GCManageCustomer();
    Element eleOrd =
        GCXMLUtil.getElementByXPath(inDoc, "MultiApi/API[@Name='importOrder']/Input/Order[@DocumentType='0001']");
    Document inputOrderDoc = GCXMLUtil.getDocumentFromElement(eleOrd);
    GCLogUtils.verboseLog(LOGGER, "Sales Order Input Document  : ", GCXMLUtil.getXMLString(inputOrderDoc));
    String billToIdIp = eleOrd.getAttribute(GCConstants.BILL_TO_ID);
    if (YFCCommon.isVoid(billToIdIp)) {
      YFCDocument getCustListOp =
          manageCust.checkCustomerProfileID(env, YFCDocument.getDocumentFor(inputOrderDoc), GCConstants.GC);
      YFCElement eleCustOp = getCustListOp.getDocumentElement();
      if (eleCustOp.hasChildNodes()) {
        YFCElement eleCustomer = eleCustOp.getElementsByTagName(GCConstants.CUSTOMER).item(0);
        String custId = eleCustomer.getAttribute(GCConstants.CUSTOMER_ID);
        eleOrd.setAttribute(GCConstants.BILL_TO_ID, custId);
      } else {

        Map<String, Element> uniqueaddressInOrderMap = new HashMap<String, Element>();
        manageCust.stampOrderAddressOnMap(inputOrderDoc, uniqueaddressInOrderMap);
        billToId = createCustomerFromOrder(env, inputOrderDoc, uniqueaddressInOrderMap, manageCust);
        eleOrd.setAttribute(GCConstants.BILL_TO_ID, billToId);
      }
    }

    GCLogUtils.verboseLog(LOGGER, "GCImportOrdersToOMS.stampBillToId--End");
    LOGGER.endTimer("GCImportOrdersToOMS.stampBillToId");
  }


  /**
   * 
   * This method creates a new customer from the details present in order
   * 
   * @param env
   * @param inputOrderDoc
   * @return
   */
  private String createCustomerFromOrder(YFSEnvironment env, Document inputOrderDoc,
      Map<String, Element> uniqueaddressInOrderMap, GCManageCustomer manageCust) {
    LOGGER.beginTimer("GCImportOrdersToOMS.createCustomerFromOrder");
    GCLogUtils.verboseLog(LOGGER, "GCImportOrdersToOMS.createCustomerFromOrder--Begin");
    Element eleInput = inputOrderDoc.getDocumentElement();
    Element eleExtnIp = (Element) eleInput.getElementsByTagName(GCConstants.EXTN).item(0);
    String sourceCustId = eleExtnIp.getAttribute(GCConstants.EXTN_SOURCE_CUSTOMER_ID);
    String daxCustId = eleExtnIp.getAttribute(GCConstants.EXTN_DAX_CUSTOMER_ID);
    Document manageCustomerIn = GCXMLUtil.createDocument(GCConstants.CUSTOMER);
    Element eleManageCustomerRootElement = manageCustomerIn.getDocumentElement();
    Element eleManageCustomerExtn = manageCustomerIn.createElement(GCConstants.EXTN);
    eleManageCustomerRootElement.appendChild(eleManageCustomerExtn);
    eleManageCustomerExtn.setAttribute(GCConstants.EXTN_SOURCE_CUSTOMER_ID, sourceCustId);
    eleManageCustomerExtn.setAttribute(GCConstants.EXTN_DAX_CUSTOMER_ID, daxCustId);
    eleManageCustomerRootElement.setAttribute(GCConstants.STATUS, GCConstants.CUSTOMER_ACTIVE_STATUS);
    eleManageCustomerRootElement.setAttribute(GCConstants.CUSTOMER_TYPE, GCConstants.CONSUMER_CUSTOMER_CODE);
    eleManageCustomerRootElement.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GC);
    Element eleCustomerContactList = manageCustomerIn.createElement(GCConstants.CUSTOMER_CONTACT_LIST);
    eleManageCustomerRootElement.appendChild(eleCustomerContactList);
    Element eleCustomerContact = manageCustomerIn.createElement(GCConstants.CUSTOMER_CONTACT);
    eleCustomerContactList.appendChild(eleCustomerContact);
    Element eleCustomerAdditionalAddressList =
        manageCustomerIn.createElement(GCConstants.CUSTOMER_ADDITIONAL_ADDRESS_LIST);
    eleCustomerContact.appendChild(eleCustomerAdditionalAddressList);
    // stamping attributes from PersonInfoBillTo on CustomerContact
    // Element
    String sDayFaxNo = "";
    String sDayPhone = "";
    String sEMailID = "";
    String sEveningPhone = "";
    String sFirstName = "";
    String sLastName = "";
    String sMiddleName = "";
    String sMobilePhone = "";
    String sTitle = "";

    // iterate on map to append all unique address on customer.
    Iterator<Map.Entry<String, Element>> mapEntries = uniqueaddressInOrderMap.entrySet().iterator();

    while (mapEntries.hasNext()) {

      Element elePersonInfoOnMapExtn = null;
      Map.Entry<String, Element> mapEntry = mapEntries.next();
      String uniqueAddressCombination = mapEntry.getKey();
      Element elePersonInfoOnMap = mapEntry.getValue();

      Element eleCustomerAdditionalAddress = manageCustomerIn.createElement("CustomerAdditionalAddress");
      if (uniqueAddressCombination.concat("|BillToAddress").equalsIgnoreCase(manageCust.sOrderBillToAddressIdentifier)) {
        eleCustomerAdditionalAddress.setAttribute(GCConstants.IS_DEFAULT_BILLTO, GCConstants.YES);
        eleCustomerAdditionalAddress.setAttribute(GCConstants.IS_BILLTO, GCConstants.YES);

        // stamp ExtnBirthday on customerContact birthday
        if (!YFCCommon.isVoid(manageCust.sExtnBirthdate)) {
          eleCustomerContact.setAttribute(GCConstants.DATE_OF_BIRTH, manageCust.sExtnBirthdate);
        }

        Element elePersonInfo = manageCustomerIn.createElement("PersonInfo");
        eleCustomerAdditionalAddress.appendChild(elePersonInfo);

        GCXMLUtil.copyAttributes(elePersonInfoOnMap, elePersonInfo);
        elePersonInfoOnMapExtn = (Element) elePersonInfoOnMap.getElementsByTagName("Extn").item(0);
        if (!YFCCommon.isVoid(elePersonInfoOnMapExtn)) {
          Element elePersonInfoExtn = manageCustomerIn.createElement("Extn");
          GCXMLUtil.copyAttributes(elePersonInfoOnMapExtn, elePersonInfoExtn);
          elePersonInfo.appendChild(elePersonInfoExtn);
        }

        sDayFaxNo = elePersonInfoOnMap.getAttribute("DayFaxNo");
        sDayPhone = elePersonInfoOnMap.getAttribute("DayPhone");
        sEMailID = elePersonInfoOnMap.getAttribute("EMailID");
        sEveningPhone = elePersonInfoOnMap.getAttribute("EveningPhone");
        sFirstName = elePersonInfoOnMap.getAttribute("FirstName");
        sLastName = elePersonInfoOnMap.getAttribute("LastName");
        sMiddleName = elePersonInfoOnMap.getAttribute("MiddleName");
        sMobilePhone = elePersonInfoOnMap.getAttribute("MobilePhone");
        sTitle = elePersonInfoOnMap.getAttribute("Title");

      } else {
        Element elePersonInfo = manageCustomerIn.createElement("PersonInfo");
        eleCustomerAdditionalAddress.appendChild(elePersonInfo);

        GCXMLUtil.copyAttributes(elePersonInfoOnMap, elePersonInfo);
        elePersonInfoOnMapExtn = (Element) elePersonInfoOnMap.getElementsByTagName("Extn").item(0);
        if (!YFCCommon.isVoid(elePersonInfoOnMapExtn)) {
          Element elePersonInfoExtn = manageCustomerIn.createElement("Extn");
          GCXMLUtil.copyAttributes(elePersonInfoOnMapExtn, elePersonInfoExtn);
          elePersonInfo.appendChild(elePersonInfoExtn);
        }
      }
      eleCustomerAdditionalAddressList.appendChild(eleCustomerAdditionalAddress);
    }
    eleCustomerContact.setAttribute("DayFaxNo", sDayFaxNo);
    eleCustomerContact.setAttribute("DayPhone", sDayPhone);
    eleCustomerContact.setAttribute("EmailID", sEMailID);
    eleCustomerContact.setAttribute("EveningPhone", sEveningPhone);
    eleCustomerContact.setAttribute("FirstName", sFirstName);
    eleCustomerContact.setAttribute("LastName", sLastName);
    eleCustomerContact.setAttribute("MiddleName", sMiddleName);
    eleCustomerContact.setAttribute("MobilePhone", sMobilePhone);
    eleCustomerContact.setAttribute("Title", sTitle);
    GCLogUtils.verboseLog(LOGGER, "Input Document to  manageCustomer method : ",
        GCXMLUtil.getXMLString(manageCustomerIn));
    Document manageCustomerOutDoc = GCCommonUtil.invokeAPI(env, manageCustomerIn, GCConstants.MANAGE_CUSTOMER_API, "");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Output doc of managecustomer is" + GCXMLUtil.getXMLString(manageCustomerOutDoc));
    }
    // Stamp newly created customerid on the order.
    Element eleManageCustOp = manageCustomerOutDoc.getDocumentElement();
    String sCustomerID = eleManageCustOp.getAttribute(GCConstants.CUSTOMER_ID);
    GCLogUtils.verboseLog(LOGGER, "GCImportOrdersToOMS.createCustomerFromOrder--End");
    LOGGER.endTimer("GCImportOrdersToOMS.createCustomerFromOrder");
    return sCustomerID;
  }


  public void setProperties(Properties arg0){
  }
}
