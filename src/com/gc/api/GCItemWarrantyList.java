/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               06/03/2014        Gowda,Naveen		JIRA No: This class is used to fetch the warranty items associated with an item.
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:email Address">Naveen</a>
 */
public class GCItemWarrantyList {

  //initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCItemWarrantyList.class.getName());

  public Document getItemWarrantyList(YFSEnvironment env, Document inDoc)
      throws GCException {
    try {
      LOGGER.debug("Class: GCItemWarrantyList Method: getItemWarrantyList BEGIN");
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Incoming Doc to the method"
            + GCXMLUtil.getXMLString(inDoc));
      }
      boolean bIsSKUTypeValid =true;
      String sExtnWarrSKUType = "";
      String sExtnWarrCond = "";
      String sCallOrgCode = GCXMLUtil.getAttributeFromXPath(inDoc,
          "/ItemList/@CallingOrganizationCode");
      String sExtnCondCode = GCXMLUtil.getAttributeFromXPath(inDoc,
          "//Item/Extn/@ExtnConditionCode");
      String sKitCode = GCXMLUtil.getAttributeFromXPath(inDoc,
          "//PrimaryInformation/@KitCode");
      if (YFCCommon.isVoid(sKitCode)) {
        sExtnWarrSKUType = GCConstants.STANDARD;
      } else {
        sExtnWarrSKUType = GCConstants.KIT;
      }

      if (GCConstants.ZERO_ZERO_ZERO.equalsIgnoreCase(sExtnCondCode)
          || GCConstants.ZERO_ZERO_ONE
          .equalsIgnoreCase(sExtnCondCode)
          || GCConstants.ZERO_ZERO_TWO
          .equalsIgnoreCase(sExtnCondCode)
          || GCConstants.ZERO_ZERO_THREE
          .equalsIgnoreCase(sExtnCondCode)) {
        sExtnWarrCond = GCConstants.NEW;
      } else {
        sExtnWarrCond = GCConstants.USED;
      }
      String sExtnWebCategory = GCXMLUtil.getAttributeFromXPath(inDoc,
          "//Item/Extn/@ExtnWebCategory");

      /*
       * Splitting ExtnWebCategory into sub categories. Ex: AAG will be
       * split as AAG, AA and A
       */

      List<String> lValidCatCodes = new ArrayList<String>();
      int iStrLen = sExtnWebCategory.length();
      for (int i = 0; i < sExtnWebCategory.length(); i++) {
        String strCatCode = sExtnWebCategory.substring(0, iStrLen - i);
        lValidCatCodes.add(strCatCode);
      }
      /*
       * Printing Category Code values. These will be looked up to in
       * Warranty table.
       */
      for (int i = 0; i < lValidCatCodes.size(); i++) {
        LOGGER.debug(lValidCatCodes.get(i));
      }

      String sItemUnitPrice = GCXMLUtil.getAttributeFromXPath(inDoc,
          "//Item/ComputedPrice/@ListPrice");

      if (YFCCommon.isVoid(sItemUnitPrice)) {
        sItemUnitPrice = GCConstants.ZERO_AMOUNT;
      }
      Document docGetCompItmLstOut = callGetItemCompleteList(env,
          sCallOrgCode, sExtnWarrCond,
          sItemUnitPrice);


      Element eleItemRoot = docGetCompItmLstOut.getDocumentElement();
      NodeList nLItem = GCXMLUtil.getNodeListByXpath(docGetCompItmLstOut,
          "/ItemList/Item");


      for (int i = 0; i < nLItem.getLength(); i++) {
        Element eleItem = (Element) nLItem.item(i);
        Document docEleItem = GCXMLUtil.getDocumentFromElement(eleItem);
        Element eleExtn = GCXMLUtil.getElementByXPath(docEleItem,
            "//Extn");
        String sExtnWarrAllCat = eleExtn
            .getAttribute(GCConstants.EXTN_WARRANTY_ALL_CATEGORIES);

        String sExtnWarrantySKUType = eleExtn
            .getAttribute(GCConstants.EXTN_WARANTY_SKU_TYPE);

        if (sExtnWarrSKUType.equalsIgnoreCase(sExtnWarrantySKUType)
            || (GCConstants.ALL.equalsIgnoreCase(sExtnWarrantySKUType))) {
          bIsSKUTypeValid = true;
        } else {
          bIsSKUTypeValid= false;
        }


        NodeList nLGCWarrCatCodList = GCXMLUtil
            .getNodeListByXpath(docEleItem,
                "//GCWarrantyCategoryCodesList/GCWarrantyCategoryCodes");

        LOGGER.debug("nLGCWarrCatCodList Length"
            + nLGCWarrCatCodList.getLength());
        String sCatCode = "";
        String sCatType = "";
        boolean bIsWarrSame = false;

        if (GCConstants.FLAG_Y.equalsIgnoreCase(sExtnWarrAllCat)) {
          bIsWarrSame = true;
        } else {
          if (GCConstants.FLAG_N.equalsIgnoreCase(sExtnWarrAllCat)) {
            for (int j = 0; j < nLGCWarrCatCodList.getLength(); j++) {
              Element eleGCWarrCatCodes = (Element) nLGCWarrCatCodList
                  .item(j);
              sCatCode = eleGCWarrCatCodes
                  .getAttribute(GCConstants.CATEGORY_CODE);
              sCatType = eleGCWarrCatCodes
                  .getAttribute(GCConstants.CATEGORY_TYPE);

              if (lValidCatCodes.contains(sCatCode)
                  && GCConstants.ECOM
                  .equalsIgnoreCase(sCatType)) {
                bIsWarrSame = true;
              }
            }
          }
        }
        if (!bIsWarrSame || !bIsSKUTypeValid) {

          GCXMLUtil.removeChild(eleItemRoot, eleItem);
        }

      }

      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Return Doc"
            + GCXMLUtil.getXMLString(docGetCompItmLstOut));
      }
      LOGGER.debug("Class: GCItemWarrantyList Method: getItemWarrantyList END");
      // GCSTORE 3122 START
      docGetCompItmLstOut = sortOutDocByUnitPrice(env, docGetCompItmLstOut);
      // GCSTORE 3122 END
      return docGetCompItmLstOut;
    } catch (Exception e) {
      LOGGER.error("The exception is in getItemWarrantyList method:", e);
      throw new GCException(e);
    }
  }

  /**
   *
   * Description of callGetItemCompleteList - This method calls
   * getCompleteItemList for a Category and fetches all items including
   * warranty items.
   *
   * @param env
   * @param sExtnWarrSKUType
   * @param sExtnWarrCondType
   * @param sExtnCatCode
   * @param sItemPrice
   * @return
   * @throws Exception
   *
   */
  private Document callGetItemCompleteList(YFSEnvironment env,
      String sCallOrgCode,
      String sExtnWarrCondType, String sItemUnitPrice) throws GCException {
    try {
      LOGGER.debug("Class: GCItemWarrantyList Method: callGetItemCompleteList BEGIN");

      Document docInput = GCXMLUtil.createDocument(GCConstants.ITEM);
      Element eleRoot = docInput.getDocumentElement();
      eleRoot.setAttribute(GCConstants.CALLING_ORGANIZATION_CODE,
          sCallOrgCode);
      eleRoot.setAttribute("IgnoreIsSoldSeparately", "Y");
      Element eleExtn = docInput.createElement(GCConstants.EXTN);
      eleExtn.setAttribute(GCConstants.EXTN_WARRANTY_IS_ACTIVE,
          GCConstants.FLAG_Y);
      eleExtn.setAttribute(GCConstants.EXTN_WARRANTY_CONDITION,
          sExtnWarrCondType);
      eleExtn.setAttribute("ExtnWarrantyMinPriceQryType", "LE");
      eleExtn.setAttribute("ExtnWarrantyMinPrice", sItemUnitPrice);
      eleExtn.setAttribute("ExtnWarrantyMaxPriceQryType", "GE");
      eleExtn.setAttribute("ExtnWarrantyMaxPrice", sItemUnitPrice);
      eleRoot.appendChild(eleExtn);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("getCompleteItemList InDoc"
            + GCXMLUtil.getXMLString(docInput));
      }
      Document docOut = GCCommonUtil
          .invokeAPI(
              env,
              GCConstants.API_GET_COMPLETE_ITEM_LIST,
              docInput,
              GCXMLUtil
.getDocument(
              "<ItemList><Item><PrimaryInformation ImageLocation='' ImageID='' ExtendedDisplayDescription='' ShortDescription=''/><ComputedPrice UnitPrice='' ListPrice=''/><Extn><GCWarrantyCategoryCodesList>"
                  + "<GCWarrantyCategoryCodes/></GCWarrantyCategoryCodesList></Extn></Item></ItemList>"));
      
      
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("getCompleteItemList OutDoc"
            + GCXMLUtil.getXMLString(docOut));
      }
      LOGGER.debug("Class: GCItemWarrantyList Method: callGetItemCompleteList END");
      return docOut;
    } catch (Exception e) {
      LOGGER.error("The exception is in callGetItemCompleteList method:",e);
      throw new GCException(e);
    }
  }

  
  	// GCSTORE - 3122 START 
  
private Document sortOutDocByUnitPrice(YFSEnvironment env, Document docOut) {

	LOGGER.beginTimer("sortOutDocByUnitPrice");
    if (LOGGER.isDebugEnabled()) {
	      LOGGER.debug("Incoming doc for sorting" + GCXMLUtil.getXMLString(docOut));
	}
	
	YFCDocument itemListDoc = YFCDocument.getDocumentFor(docOut);
	YFCElement itemListEle = itemListDoc.getDocumentElement();
	
	YFCNodeList<YFCElement> itemNodeList = itemListEle
			.getElementsByTagName("Item");
	List<YFCElement> itemList = new ArrayList<YFCElement>(
			itemNodeList.getLength());
	for (YFCElement item : itemNodeList) {
		itemList.add(item);
	}
	Collections.sort(itemList, new WarrantyUnitPriceComparator());
	
	YFCDocument newDoc = YFCDocument.createDocument(GCConstants.ITEM_LIST);
	YFCElement elenewRoot = newDoc.getDocumentElement();
	GCXMLUtil.copyAttributes(itemListEle, elenewRoot);

	for (YFCElement eleItem : itemList) {
		YFCElement elenewItem = newDoc.createElement("Item");
		GCXMLUtil.copyElement(newDoc, eleItem , elenewItem);
		elenewRoot.appendChild(elenewItem);
	}
	

	if (LOGGER.isDebugEnabled()) {
	      LOGGER.debug("Outgoing doc for sorting" + GCXMLUtil.getXMLString(newDoc.getDocument()));
	}

	return newDoc.getDocument();
	
}

static class WarrantyUnitPriceComparator implements Comparator<YFCElement>,Serializable {

		@Override
	public int compare(YFCElement elem1, YFCElement elem2) {
		
		YFCElement compPriceEle1 = elem1.getChildElement(GCConstants.COMPUTED_PRICE);
		YFCElement compPriceEle2 = elem2.getChildElement(GCConstants.COMPUTED_PRICE);
		
		Double unitPrice1 = compPriceEle1.getDoubleAttribute("UnitPrice",0.0);
		Double unitPrice2 = compPriceEle2.getDoubleAttribute("UnitPrice",0.0);
		
		return unitPrice2.compareTo(unitPrice1);
	}

}

//GCSTORE - 3122 END

}
	