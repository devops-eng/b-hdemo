/**
F * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               06/02/2014        Last Name, First Name		JIRA No: Description of issue.
 *#################################################################################################################################################################
 */
package com.gc.api;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * @author <a href="mailto:email Address">Name</a>
 */
public class GCManageCommission {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCManageCommission.class.getName());

  /**
   *
   * Description of manageCommission
   *
   * @param env
   * @param inDoc
   * @throws GCException
   * @throws TransformerException
   * @throws Exception
   *
   */

  public Document manageCommission(YFSEnvironment env, Document inDoc) throws GCException {

    LOGGER.debug("Class: GCManageCommission Method: manageCommission BEGIN");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Incoming XML" + GCXMLUtil.getXMLString(inDoc));
    }
    Element eleRoot = inDoc.getDocumentElement();

    String sOrderHeaderKey = eleRoot
        .getAttribute(GCConstants.ORDER_HEADER_KEY);
    // Call UserList
    

    NodeList nLGCOrdrLnComm = GCXMLUtil.getNodeListByXpath(inDoc,
        "//Order/Extn/GCOrderCommissionList/GCOrderCommission");
    for (int j = 0; j < nLGCOrdrLnComm.getLength(); j++) {
      LOGGER.debug("GCOrderCommList Length:"
          + nLGCOrdrLnComm.getLength());

      Element eleGCOrdrLnComm = (Element) nLGCOrdrLnComm.item(j);

      String sUserID = eleGCOrdrLnComm
          .getAttribute(GCConstants.USER_ID);
      Document docGetUserList = callGetUserListOut(env,sUserID);
      // Below method will validate the user
      String sUsername = getUsername(sUserID, docGetUserList);
      eleGCOrdrLnComm.setAttribute(GCConstants.ORDER_USER_NAME,
          sUsername);
      eleGCOrdrLnComm.setAttribute(GCConstants.COMMISSION_TYPE,
          GCConstants.SALES_COMMISSION);

    }

    // Call getOrderList
    Document docGetOrderListOut = callGetOrderList(env, sOrderHeaderKey);

    // Below is the logic to delete the existing commission
    NodeList nLGCOrderComm = GCXMLUtil.getNodeListByXpath(
        docGetOrderListOut,
        "//Order/Extn/GCOrderCommissionList/GCOrderCommission");

    for (int k = 0; k < nLGCOrderComm.getLength(); k++) {
      Element eleGCOrdrLnComm = (Element) nLGCOrderComm.item(k);
      String sUserID = eleGCOrdrLnComm
          .getAttribute(GCConstants.USER_ID);
      try {
        Element eleInOrderCommission =
            (Element) XPathAPI.selectSingleNode(inDoc, "//Order/Extn/GCOrderCommissionList/GCOrderCommission[@UserID='"
                + sUserID + "']");
        if (YFCCommon.isVoid(eleInOrderCommission)) {
          deleteOrderCommission(env, eleGCOrdrLnComm);
          // Below is the logic to delete the existing orderLine commission
          NodeList nLGCOrderLine = GCXMLUtil.getNodeListByXpath(docGetOrderListOut, "//Order/OrderLines/OrderLine");
          int count = nLGCOrderLine.getLength();
          for (int i = 0; i < count; i++) {
            Element eleGCOrderLine = (Element) nLGCOrderLine.item(i);
            Element eleGCOrderLnCommision =
                (Element) XPathAPI.selectSingleNode(eleGCOrderLine,
                    "Extn/GCOrderLineCommissionList/GCOrderLineCommission[@UserID='" + sUserID + "']");
            if (eleGCOrderLnCommision != null) {
              deleteOrderLineCommission(env, eleGCOrderLnCommision);
            }
          }
        }
      } catch (Exception e) {
        LOGGER.error("The exception is in manageCommission method:", e);
        throw new GCException(e);
      }
    }

    return inDoc;
  }

  /**
   *
   * @param env
   * @param sOrderHeaderKey
   * @return
   * @throws Exception
   */
  private Document callGetOrderList(YFSEnvironment env, String sOrderHeaderKey) {

    LOGGER.debug("Class: GCManageCommission Method: callGetOrderList BEGIN");
    Document docGetOrdrListIn = GCXMLUtil
        .createDocument(GCConstants.ORDER);
    Element eleRootGetOrdrIn = docGetOrdrListIn.getDocumentElement();
    eleRootGetOrdrIn.setAttribute(GCConstants.ORDER_HEADER_KEY,
        sOrderHeaderKey);
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("docGetOrderListIn"
          + GCXMLUtil.getXMLString(docGetOrdrListIn));
    }
    Document docGetOrderListOut = GCCommonUtil
        .invokeAPI(env, docGetOrdrListIn,
            GCConstants.API_GET_ORDER_LIST,
            "/global/template/api/getOrderListTempForOrderCommission.xml");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("docGetOrderListOut"
          + GCXMLUtil.getXMLString(docGetOrderListOut));
    }
    LOGGER.debug("Class: GCManageCommission Method: callGetOrderList END");
    return docGetOrderListOut;

  }

  /**
   *
   * Description of callGetUserListOut
   *
   * @param env
   * @return
   * @throws Exception
   *
   */
  private Document callGetUserListOut(YFSEnvironment env, String sUserId) {

    LOGGER.debug("Class: GCManageCommission Method: callGetUserListOut BEGIN");
    Document docGtUsrLstIn = GCXMLUtil.createDocument(GCConstants.USER);
    Element eleRoot = docGtUsrLstIn.getDocumentElement();
    eleRoot.setAttribute(GCConstants.USER, "");
    Element eleRootExtn = docGtUsrLstIn.createElement("Extn");
    eleRoot.appendChild(eleRootExtn);
    eleRootExtn.setAttribute("ExtnEmployeeID",sUserId);
    Document docGtUsrLstTemp = GCXMLUtil
        .createDocument(GCConstants.USERLIST);
    Element eleTempDocRoot = docGtUsrLstTemp.getDocumentElement();
    Element eleUser = docGtUsrLstTemp.createElement(GCConstants.USER);
    Element eleExtn = docGtUsrLstTemp.createElement(GCConstants.EXTN);
    eleUser.appendChild(eleExtn);
    eleTempDocRoot.appendChild(eleUser);
    Document docOut = GCCommonUtil.invokeAPI(env,
        GCConstants.API_GET_USER_LIST, docGtUsrLstIn,
        docGtUsrLstTemp);
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Get User List" + GCXMLUtil.getXMLString(docOut));
    }
    LOGGER.debug("Class: GCManageCommission Method: callGetUserListOut END");
    return docOut;

  }

  /**
   *
   * Description of deleteOLComm
   *
   * @param env
   * @param eleGCOrdrLnComm
   * @throws Exception
   *
   */
  private void deleteOrderCommission(YFSEnvironment env, Element eleGCOrdrLnComm) {

    LOGGER.debug("Class: GCManageCommission Method: deleteOrderCommission BEGIN");
    Document docInput = GCXMLUtil
        .getDocumentFromElement(eleGCOrdrLnComm);
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Input Doc to Service"
          + GCXMLUtil.getXMLString(docInput));
    }
    GCCommonUtil.invokeService(env, "GCDeleteOrderCommissionService",
        docInput);
    LOGGER.debug("Class: GCManageCommission Method: deleteOrderCommission END");

  }
  /**
   *
   * Description of getUserName
   *
   * @param env
   * @param sUserID
   * @param docGetUserList
   * @return
   * @throws Exception
   *
   */

  private String getUsername(String sUserID, Document docGetUserList) {

    String sUsername = "";
    LOGGER.debug("Class: GCManageCommission Method: getUserName BEGIN");
    LOGGER.debug("UserID" + sUserID);
    Element eleUser = GCXMLUtil.getElementByXPath(docGetUserList,
        "//User[Extn[@ExtnEmployeeID='" + sUserID + "']]");
    if (YFCCommon.isVoid(eleUser)) {
      LOGGER.debug("Invalid user");
      YFSException yfsEx = new YFSException();

      yfsEx.setErrorCode(GCErrorConstants.INVALID_USER_ERROR_CODE);
      yfsEx.setErrorDescription(GCErrorConstants.INVALID_USER_ERROR_DESCRIPTION + ":" + sUserID);
      throw yfsEx;
    } else {
      sUsername = eleUser.getAttribute(GCConstants.USRNAME);
    }

    LOGGER.debug("Class: GCManageCommission Method: getUserName END");
    return sUsername;

  }
  /**
   *
   * Description of deleteOrderLineCommission this method deletes the GCOrderLineCommission from orderlines which have been removed from order level
   *
   * @param env
   * @param eleGCOrderLnCommision
   * @throws GCException
   *
   */
  private void deleteOrderLineCommission(YFSEnvironment env,
      Element eleGCOrderLnCommision) {

    LOGGER.debug("Class: GCManageCommission Method: deleteOrderLineCommission BEGIN");
    Document docInput = GCXMLUtil
        .getDocumentFromElement(eleGCOrderLnCommision);
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Input Doc to Service"
          + GCXMLUtil.getXMLString(docInput));
    }
    GCCommonUtil.invokeService(env, "GCDeleteOrderLineCommissionService",
        docInput);
    LOGGER.debug("Class: GCManageCommission Method: deleteOrderLineCommission END");


	}
	/**
	 *
	 * Description of updateOrderLineCommissions this method adds GCOrderLineCommissionList inside OrderLine/Extn by copying it from Order/Extn
	 *
	 * @param inDoc
	 * @return
	 * @throws Exception
	 *
	 */
	public void updateOrderLineCommissions(Document inDoc) {
		LOGGER.debug("Class: GCManageCommission Method: updateOrderLineCommissions Start");
		try {
			NodeList nlGCOrderCommission=XPathAPI.selectNodeList(inDoc,"Order/Extn/GCOrderCommissionList/GCOrderCommission");
			int gcOrderCommissionCount = nlGCOrderCommission.getLength();
			if (gcOrderCommissionCount != 0) {
				Element eleGCOrderLineCommissionList = GCXMLUtil.createElement(inDoc,
						GCConstants.GC_ORDER_LINE_COMMISSION_LIST, null);
				for (int counter = 0; counter < gcOrderCommissionCount; counter++) {
					Element eleGCOrderCommission=(Element)nlGCOrderCommission.item(counter);
					Element eleGCOrderLineCommission = GCXMLUtil.createElement(inDoc,
							GCConstants.GC_ORDER_LINE_COMISSION, null);
					eleGCOrderLineCommission.setAttribute(GCConstants.ALLOCATION_PERCENTAGE,eleGCOrderCommission.getAttribute(GCConstants.ALLOCATION_PERCENTAGE));
					eleGCOrderLineCommission.setAttribute(GCConstants.COMMISSION_TYPE,eleGCOrderCommission.getAttribute(GCConstants.COMMISSION_TYPE));
					eleGCOrderLineCommission.setAttribute(GCConstants.ORDER_HEADER_KEY,eleGCOrderCommission.getAttribute(GCConstants.ORDER_HEADER_KEY));
					eleGCOrderLineCommission.setAttribute(GCConstants.USER_ID,eleGCOrderCommission.getAttribute(GCConstants.USER_ID));
					eleGCOrderLineCommission.setAttribute(GCConstants.USERNAME,eleGCOrderCommission.getAttribute(GCConstants.USERNAME));
					eleGCOrderLineCommissionList.appendChild(eleGCOrderLineCommission);
				}
				NodeList nlOrderLine=XPathAPI.selectNodeList(inDoc,"Order/OrderLines/OrderLine");
				int nlOrderLineCount=nlOrderLine.getLength();
				Element eleOrderLineExtn=null;
				for(int count=0;count<nlOrderLineCount;count++){
					Element eleOrderLine=(Element)nlOrderLine.item(count);
					String sOrderLineAction=eleOrderLine.getAttribute(GCConstants.ACTION);
					if (!(!sOrderLineAction.isEmpty() && ("CANCEL".equals(sOrderLineAction)))) {
						eleOrderLineExtn=(Element)XPathAPI.selectSingleNode(eleOrderLine,"Extn");
						Element elemGCOrderLineCommissionList=GCXMLUtil.createElement(inDoc, GCConstants.GC_ORDER_LINE_COMMISSION_LIST, null);
						if(eleOrderLineExtn==null){
							Element eleExtn=GCXMLUtil.createElement(inDoc, GCConstants.EXTN, null);
							GCXMLUtil.copyElement(inDoc,eleGCOrderLineCommissionList,elemGCOrderLineCommissionList);
							eleExtn.appendChild(elemGCOrderLineCommissionList);
							eleOrderLine.appendChild(eleExtn);
						}else{
							GCXMLUtil.copyElement(inDoc,eleGCOrderLineCommissionList,elemGCOrderLineCommissionList);
							eleOrderLineExtn.appendChild(elemGCOrderLineCommissionList);
							eleOrderLine.appendChild(eleOrderLineExtn);
						}
					}
					rmvBundleFulfillmentModeFromOrderLine(eleOrderLine);
				}
			}
			LOGGER.debug("Class: GCManageCommission Method: updateOrderLineCommissions End");

		} catch (Exception e) {
			LOGGER.error("The exception is in updateOrderLineCommissions method:", e);
			YFCException ex = new YFCException(GCErrorConstants.MANAGE_COMMISSION_FAILURE);
			ex.setErrorDescription(e.getMessage());
			throw ex;

		}
	}

	private static void rmvBundleFulfillmentModeFromOrderLine(
			Element eleOrderLine) throws TransformerException {
		Element eleItem = (Element) XPathAPI.selectSingleNode(eleOrderLine,
				"Item");
		if (!YFCCommon.isVoid(eleItem)) {
			String sBundleFulfillmentMode = eleItem
					.getAttribute("BundleFulfillmentMode");
			if (!YFCCommon.isVoid(sBundleFulfillmentMode)) {
				eleItem.removeAttribute("BundleFulfillmentMode");
			}
		}
	}
}
