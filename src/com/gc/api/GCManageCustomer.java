/**
F * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class is used to create and modify customers from web orders or call center orders.
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             	1.0            06/02/2014        Mittal, Yashu		JIRA No: Description of issue.
             	1.1			   27/11/2014		 Soni, Karan		CR-48: Put validation into the email field when creating a new customer account, or modifying an existing account,
             	 													to not let users enter an email that exists on another existing account
             	1.2            01/04/2015        Saxena, Ekansh     Changed return type of the method manageCustomer
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * @author <a href="mailto:yashu.mittal@expicient.com">Yashu Mittal</a>
 */
public class GCManageCustomer implements YIFCustomApi {

	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCManageCustomer.class.getName());

	String sOrderBillToAddressIdentifier = "";
	String sExtnBirthdate = "";

	/**
	 *
	 * manageCustomer method is used to created/update customer from web orders,
	 * call center orders or initial customer feed.
	 *
	 * @param env
	 * @param inDoc
	 * @throws GCException
	 *
	 */

	public Document manageCustomer(YFSEnvironment env, Document inDoc)
			throws GCException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(" Entering GCManageCustomer.manageCustomer() method ");
			LOGGER.debug(" manageCustomer() method, Input document is"
					+ GCXMLUtil.getXMLString(inDoc));
		}
		try {
			Element eleRootInDoc = inDoc.getDocumentElement();

			String sCustomerID = "";
			String sOrganizationCode = "";
			String sExtnNewDAXCustomerID = "";
			String sOrderHeaderKey = "";

			String sInDocRootElementName = eleRootInDoc.getNodeName();
			if (sInDocRootElementName.equalsIgnoreCase(GCConstants.CUSTOMER)) {

				sCustomerID = eleRootInDoc
						.getAttribute(GCConstants.CUSTOMER_ID);
				sOrganizationCode = eleRootInDoc
						.getAttribute(GCConstants.ORGANIZATION_CODE);
				Element eleExtn = (Element) eleRootInDoc.getElementsByTagName(
						"Extn").item(0);

				// Phase-2 : Start
				String eExtnSourceSystem = eleExtn
						.getAttribute(GCXmlLiterals.EXTN_SOURCE_SYSTEM);
				if (YFCCommon.equals(GCConstants.ORDER_CATURE_SYSTEM_ATG,
						eExtnSourceSystem)) {
					Document cloneInDoc = (Document) inDoc.cloneNode(true);
					if (LOGGER.isVerboseEnabled()) {
						LOGGER.verbose("cloneInDoc: \t" + cloneInDoc);
					}
					createWebCustomer(env, cloneInDoc, sOrganizationCode);
				} else {
					// Phase-2 : End

					sExtnNewDAXCustomerID = eleExtn
							.getAttribute(GCConstants.EXTN_DAX_CUSTOMER_ID);

					if (!YFCCommon.isVoid(sCustomerID)
							&& !YFCCommon.isVoid(sOrganizationCode)) {

						LOGGER.debug("CustomerId is present on inDoc. So Customer update to stamp DAX CustomerID ");

						Document getCustomerListOutDoc = GCCommonUtil
								.invokeAPI(
										env,
										GCConstants.GET_CUSTOMER_LIST,
										GCXMLUtil
												.getDocument("<Customer CustomerID='"
														+ sCustomerID
														+ "' OrganizationCode='"
														+ sOrganizationCode
														+ "'/>"),
										GCCommonUtil
												.getDocumentFromPath("/global/template/api/getCustomerList_TemplateXML.xml"));

						if (getCustomerListOutDoc.getDocumentElement()
								.hasChildNodes()) {

							LOGGER.debug("CustomerPresent");
							String sExtnDAXCustomerID = GCXMLUtil
									.getAttributeFromXPath(
											getCustomerListOutDoc,
											"CustomerList/Customer/Extn/@ExtnDAXCustomerID");
							LOGGER.debug("ExtnDAXCustomerID = "
									+ sExtnDAXCustomerID);

							if (YFCCommon.isVoid(sExtnDAXCustomerID)
									&& !YFCCommon.isVoid(sExtnNewDAXCustomerID)) {
								stampDAXCustomerID(env, sCustomerID,
										sOrganizationCode,
										sExtnNewDAXCustomerID);
							}
						}
					} else {
						LOGGER.debug("New Customer to be Created");

						Document getCustomerListOutDoc = GCCommonUtil
								.invokeAPI(
										env,
										GCConstants.GET_CUSTOMER_LIST,
										GCXMLUtil
												.getDocument("<Customer><Extn ExtnDAXCustomerID='"
														+ sExtnNewDAXCustomerID
														+ "'/></Customer>"),
										GCXMLUtil
												.getDocument("<CustomerList><Customer CustomerID='' CustomerKey='' /></CustomerList>"));
						if ((getCustomerListOutDoc.getDocumentElement()
								.hasChildNodes())) {
							// Customer already exists, deleting the present
							// customer
							// and create a new one with the same customerID.
							Element eleCustomer = (Element) getCustomerListOutDoc
									.getElementsByTagName("Customer").item(0);
							sCustomerID = eleCustomer
									.getAttribute(GCConstants.CUSTOMER_ID);
							String sCustomerKey = eleCustomer
									.getAttribute("CustomerKey");

							// Deleting Customer
							Document deleteCustomerDoc = GCXMLUtil
									.createDocument("Customer");
							deleteCustomerDoc.getDocumentElement()
									.setAttribute("Operation", "Delete");
							deleteCustomerDoc.getDocumentElement()
									.setAttribute("CustomerKey", sCustomerKey);

							GCCommonUtil.invokeAPI(env,
									GCConstants.MANAGE_CUSTOMER_API,
									deleteCustomerDoc);

							// Creating Customer with same CustomerID.

							inDoc.getDocumentElement().setAttribute(
									GCConstants.CUSTOMER_ID, sCustomerID);
							inDoc.getDocumentElement().setAttribute("Status",
									GCConstants.CUSTOMER_ACTIVE_STATUS);
							// CR-48 Start
							boolean isManageApiCallReq = checkCustomerExist(
									env, inDoc);
							if (!isManageApiCallReq) {
								GCCommonUtil.invokeAPI(env,
										GCConstants.MANAGE_CUSTOMER_API, inDoc);
							} else {
								YFSException ex = new YFSException();
								ex.setErrorCode("The Email Id already exist in record");
								ex.setErrorDescription("The Email Id entered already exist in record, use other Email ID ");
								throw ex;
							}
						} else {
							boolean isManageApiCallReq = checkCustomerExist(
									env, inDoc);
							if (!isManageApiCallReq) {
								inDoc.getDocumentElement().setAttribute(
										"Status",
										GCConstants.CUSTOMER_ACTIVE_STATUS);
								GCCommonUtil.invokeAPI(env,
										GCConstants.MANAGE_CUSTOMER_API, inDoc);
							} else {
								YFSException ex = new YFSException();
								ex.setErrorCode("The Email Id already exist in record");
								ex.setErrorDescription("The Email Id entered already exist in record, use other Email ID ");
								throw ex;
							}
							// CR-48 End
						}
					}

				}
			} else if (sInDocRootElementName
					.equalsIgnoreCase(GCConstants.ORDER)) {

				sCustomerID = eleRootInDoc.getAttribute("BillToID");
				sOrganizationCode = eleRootInDoc
						.getAttribute(GCConstants.ENTERPRISE_CODE);
				Element eleOrderExtn = (Element) eleRootInDoc
						.getElementsByTagName("Extn").item(0);
				sOrderHeaderKey = eleRootInDoc
						.getAttribute(GCConstants.ORDER_HEADER_KEY);

				// Phase-2: Start
				String sExtnSourceSystem = eleOrderExtn
						.getAttribute(GCXmlLiterals.EXTN_SOURCE_SYSTEM);
				if (YFCCommon.equalsIgnoreCase(
						GCConstants.ORDER_CATURE_SYSTEM_ATG, sExtnSourceSystem)) {
					Document cloneOrdInDoc = (Document) inDoc.cloneNode(true);
					if (LOGGER.isVerboseEnabled()) {
						LOGGER.verbose("cloneInDoc: \t" + cloneOrdInDoc);
					}
					manageWebCustomer(env, cloneOrdInDoc, sOrganizationCode,
							sOrderHeaderKey);
				} else {
					// Phase-2 :End

					sExtnNewDAXCustomerID = eleOrderExtn
							.getAttribute(GCConstants.EXTN_DAX_CUSTOMER_ID);

					LOGGER.debug("OrderHeaderKey is :" + sOrderHeaderKey);

					Map<String, Element> uniqueaddressInOrderMap = new HashMap<String, Element>();

					stampOrderAddressOnMap(inDoc, uniqueaddressInOrderMap);

					if (!YFCCommon.isVoid(sCustomerID)
							&& !YFCCommon.isVoid(sOrganizationCode)) {
						LOGGER.debug("CustomerId is present on inDoc. So Customer Details of Order is possibly from Call Center");

						Document getCustomerListOutDoc = GCCommonUtil
								.invokeAPI(
										env,
										GCConstants.GET_CUSTOMER_LIST,
										GCXMLUtil
												.getDocument("<Customer CustomerID='"
														+ sCustomerID
														+ "' OrganizationCode='"
														+ sOrganizationCode
														+ "'/>"),
										GCCommonUtil
												.getDocumentFromPath("/global/template/api/getCustomerList_TemplateXML.xml"));

						if (getCustomerListOutDoc.getDocumentElement()
								.hasChildNodes()) {

							LOGGER.debug("CustomerPresent in OMS");
							// check for change in address
							checkChangeInAddress(env, getCustomerListOutDoc,
									uniqueaddressInOrderMap, inDoc);

							String sExtnDAXCustomerID = GCXMLUtil
									.getAttributeFromXPath(
											getCustomerListOutDoc,
											"CustomerList/Customer/Extn/@ExtnDAXCustomerID");
							LOGGER.debug("ExtnDAXCustomerID = "
									+ sExtnDAXCustomerID);

							if (!YFCCommon.isVoid(sExtnDAXCustomerID)) {
								LOGGER.debug("DAX ID is present for the given CustomerId in OMS. Order from call center and customer already updated in OMS. "
										+ "Stamp ExtnDAXCustomerID on order");

								// Fix for 2740 Begin : Stamp ExtnDAXCustomerID
								// on
								// order.
								/*Document inDocOrderDetails = GCCommonUtil
										.invokeAPI(
												env,
												GCConstants.GET_ORDER_DETAILS,
												GCXMLUtil
														.getDocument("<Order OrderHeaderKey='"
																+ sOrderHeaderKey
																+ "'/>"),
												GCXMLUtil
														.getDocument("<Order OrderHeaderKey=''/>"));
								if (LOGGER.isDebugEnabled()) {
									LOGGER.debug(GCXMLUtil
											.getXMLString(inDocOrderDetails));
								}*/
								if (!YFCCommon.isVoid(sOrderHeaderKey)) {
									GCCommonUtil
											.invokeAPI(
													env,
													GCConstants.CHANGE_ORDER,
													GCXMLUtil
															.getDocument("<Order OrderHeaderKey='"
																	+ sOrderHeaderKey
																	+ "' SelectMethod='NO_LOCK'><Extn ExtnDAXCustomerID='"
																	+ sExtnDAXCustomerID
																	+ "'/></Order>"));
								}
								// Fix for 2740 End
							}

						} else {
							YFSException ex = new YFSException();
							ex.setErrorCode("CustomerID is present on order from call center, but customer do not exist in OMS");
							ex.setErrorDescription("Customer is not present in OMS for which this order is created. OrderHeaderKey for order is : "
									+ sOrderHeaderKey);
							throw ex;
						}
					} else {
						LOGGER.debug("CustomerId is not present on inDoc. So Customer Details of Order is from Web Order. Customer is to be created or modified.");

						LOGGER.debug(sExtnNewDAXCustomerID);

						Document getCustomerListOutDoc = GCCommonUtil
								.invokeAPI(
										env,
										GCConstants.GET_CUSTOMER_LIST,
										GCXMLUtil
												.getDocument("<Customer><Extn ExtnDAXCustomerID='"
														+ sExtnNewDAXCustomerID
														+ "'/></Customer>"),
										GCCommonUtil
												.getDocumentFromPath("/global/template/api/getCustomerList_TemplateXML.xml"));

						if ((getCustomerListOutDoc.getDocumentElement()
								.hasChildNodes())
								&& (!YFCCommon.isVoid(sExtnNewDAXCustomerID))) {
							LOGGER.debug("Customer is present in OMS with updated DAX CustomerID. Check for change in address and stamp customer id on order");
							// Check for Bill to And ShipTo address changes
							checkChangeInAddress(env, getCustomerListOutDoc,
									uniqueaddressInOrderMap, inDoc);

							Element eleCustomer = (Element) getCustomerListOutDoc
									.getElementsByTagName("Customer").item(0);
							sCustomerID = eleCustomer
									.getAttribute(GCConstants.CUSTOMER_ID);

							if (!YFCCommon.isVoid(sOrderHeaderKey)) {
								YFCDocument inDoChangeOrder = YFCDocument
										.createDocument("Order");
								YFCElement eleOrder = inDoChangeOrder
										.getDocumentElement();
								eleOrder.setAttribute("OrderHeaderKey",
										sOrderHeaderKey);
							    eleOrder.setAttribute(GCConstants.SELECT_METHOD, "NO_LOCK");
								eleOrder.setAttribute("BillToID", sCustomerID);
								YFCElement eleOrderHoldTypes = eleOrder
										.createChild("OrderHoldTypes");
								YFCElement eleOrderHoldType = eleOrderHoldTypes
										.createChild("OrderHoldType");
								eleOrderHoldType.setAttribute("HoldType",
										GCConstants.ATGNULLBILLTOIDHOLD);
								eleOrderHoldType.setAttribute("ReasonText",
										"Bill To ID not Stamped for ATG Order");
								eleOrderHoldType.setAttribute("Status", "1300");
								Document changeOrder=inDoChangeOrder.getDocument();
								GCCommonUtil.invokeAPI(env,
										GCConstants.CHANGE_ORDER,changeOrder);
							}

						} else if (!YFCCommon.isVoid(sExtnNewDAXCustomerID)) {
							LOGGER.debug("Customer do not exist. Create customer from order and publish to DAX");
							// Call create customer method and pass the map.
							createOrUpdateCustomerFromOrder(env,
									uniqueaddressInOrderMap,
									sExtnNewDAXCustomerID, sOrganizationCode,
									GCConstants.BLANK_STRING,
									GCConstants.BLANK_STRING, sOrderHeaderKey,
									inDoc);
						} else {
							// throw exception. No customer info present on
							// inDoc.
							YFSException ex = new YFSException();
							ex.setErrorCode("Both CustomerContactID and ExtnDAXCustomerID are empty for Order");
							ex.setErrorDescription("The order does not have CustomerContactID or ExtnDAXCustomerID. OrderHeaderKey for order is : "
									+ sOrderHeaderKey);
							throw ex;
						}
					}
				}
			} else {
				// Throw exception : I/P document passed is not correct.
				YFSException ex = new YFSException();
				ex.setErrorCode("Input Document Passed is not correct.");
				ex.setErrorDescription("Input Document passed must have 'Order' or 'Customer' as document element");
				throw ex;
			}
		} catch (Exception e) {
			LOGGER.error(
					"Inside catch block of GCManageCustomer.manageCustomer method. Exception is",
					e);
			throw new GCException(e);
		}
		return inDoc;
	}

	/**
	 *
	 * @param env
	 * @param sCustomerID
	 * @param sOrganizationCode
	 * @param sExtnNewDAXCustomerID
	 */
	private void stampDAXCustomerID(YFSEnvironment env, String sCustomerID,
			String sOrganizationCode, String sExtnNewDAXCustomerID) {

		// Refactored code for Sonar Fixes
		String sOrderHeaderKey;
		LOGGER.debug("DAX id is not present in OMS for customer. This is an update. Stamp ExtnNewDAXCustomerID on customer and on all orders");

		// get orderlist from customer. Stamp dax id from
		// inDoc
		// on the customer and on each order and update the
		// order.

		env.setTxnObject("Publish", "N");

		Document manageCustomerInDoc = GCXMLUtil
				.getDocument("<Customer CustomerID='" + sCustomerID
						+ "' OrganizationCode='" + sOrganizationCode + "'>"
						+ "<Extn ExtnDAXCustomerID='" + sExtnNewDAXCustomerID
						+ "'/></Customer>");

		GCCommonUtil.invokeAPI(env, GCConstants.MANAGE_CUSTOMER_API,
				manageCustomerInDoc);

		Document getOrderListOutDoc = GCCommonUtil
				.invokeAPI(
						env,
						GCConstants.GET_ORDER_LIST,
						GCXMLUtil.getDocument("<Order BillToID='" + sCustomerID
								+ "'/>"),
						GCXMLUtil
								.getDocument("<OrderList><Order OrderHeaderKey='' MaxOrderStatus=''><Extn ExtnDAXCustomerID=''/></Order></OrderList>"));
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(GCXMLUtil.getXMLString(getOrderListOutDoc));
		}
		List<Element> listOrderList = GCXMLUtil.getElementListByXpath(
				getOrderListOutDoc, "/OrderList/Order");
		String sMaxOrderStatus = "";
		for (Element eleOrder : listOrderList) {
			sOrderHeaderKey = eleOrder
					.getAttribute(GCConstants.ORDER_HEADER_KEY);
			sMaxOrderStatus = eleOrder.getAttribute("MaxOrderStatus");
			// getORderList and stamp.
			// If complete order is already in cancelled
			// status.
			// Do not stamp ExtnDAXCustomerID.
			if (!sMaxOrderStatus
					.equalsIgnoreCase(GCConstants.CANCELLED_ORDER_STATUS_CODE)) {
				GCCommonUtil.invokeAPI(
						env,
						GCConstants.CHANGE_ORDER,
						GCXMLUtil.getDocument("<Order OrderHeaderKey='"
								+ sOrderHeaderKey
								+ "' SelectMethod='NO_LOCK'><Extn ExtnDAXCustomerID='"
								+ sExtnNewDAXCustomerID + "'/></Order>"));
			}
		}
	}

	/**
	 * This method will be invoked when the order has been captured via ATG.
	 * This method will be used to create and update customer account in OMS.
	 * This method reuse phase-1 code.
	 *
	 * @param env
	 * @param cloneOrdInDoc
	 * @param organizationCode
	 * @param sOrderHeaderKey
	 * @throws GCException
	 */

	private void manageWebCustomer(YFSEnvironment env, Document cloneOrdInDoc,
			String organizationCode, String sOrderHeaderKey) {
		LOGGER.beginTimer("GCManageCustomer.manageWebCustomer");
		LOGGER.verbose("Class: GCManageCustomer Method: manageWebCustomer:: BEGIN");
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("manageWebCustomer() method, Input document is"
					+ YFCDocument.getDocumentFor(cloneOrdInDoc));
		}
		YFCDocument yfcOrderInDoc = YFCDocument.getDocumentFor(cloneOrdInDoc);
		String eExtnSourceCustomerID = yfcOrderInDoc.getDocumentElement()
				.getChildElement(GCConstants.EXTN)
				.getAttribute(GCXmlLiterals.EXTN_SRC_CSTOMER_ID);

		YFCDocument getCustomerListOp;
		if (!YFCCommon.isVoid(eExtnSourceCustomerID)) {
			getCustomerListOp = checkCustomerProfileID(env, yfcOrderInDoc,
					organizationCode);
		} else {
			getCustomerListOp = callGetCustomerListWithEmailId(env,
					yfcOrderInDoc);
		}

		try {
			Map<String, Element> uniqueaddressInOrderMap = new HashMap<String, Element>();
			stampOrderAddressOnMap(cloneOrdInDoc, uniqueaddressInOrderMap);
			YFCElement eCustomerList = getCustomerListOp.getDocumentElement();
			YFCNodeList<YFCElement> nlCustomer = eCustomerList
					.getElementsByTagName(GCConstants.CUSTOMER);
			if (nlCustomer.getLength() > 0) {
				checkChangeInAddress(env, getCustomerListOp.getDocument(),
						uniqueaddressInOrderMap, cloneOrdInDoc);
			} else {
				// createOrUpdateCustomerFromOrder
				createOrUpdateCustomerFromOrder(env, uniqueaddressInOrderMap,
						GCConstants.BLANK_STRING, organizationCode,
						GCConstants.BLANK_STRING, GCConstants.BLANK_STRING,
						sOrderHeaderKey, cloneOrdInDoc);
			}
		} catch (GCException e) {
			LOGGER.error("exeception thrown", e);
			YFCException excep = new YFCException(e);
			excep.setAttribute(GCXmlLiterals.EXTN_SRC_CSTOMER_ID,
					eExtnSourceCustomerID);
			excep.setAttribute(GCConstants.ORGANIZATION_CODE, organizationCode);
			excep.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
			LOGGER.endTimer("raiseException");
			throw excep;
		}
		LOGGER.endTimer("GCManageCustomer.manageWebCustomer");
		LOGGER.verbose("GCManageCustomer.manageWebCustomer method :: END");
	}

	/**
	 * This method will be used for creating/updating customer account.Source of
	 * data to this method will be customer details received via ATG. This
	 * method calls checkCustomerProfileID method to get Customer Details with
	 * ExtnSourceCustomerID and calls callGetCustomerListWithEmailId method with
	 * Input Customer Email.
	 *
	 * @param env
	 * @param cloneInDoc
	 * @param organizationCode
	 */
	private void createWebCustomer(YFSEnvironment env, Document cloneInDoc,
			String organizationCode) {
		LOGGER.beginTimer("GCManageCustomer.createWebCustomer");
		LOGGER.verbose("Class: GCManageCustomer Method: createWebCustomer:: BEGIN");
		YFCDocument yfcInDoc = YFCDocument.getDocumentFor(cloneInDoc);
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("clone of createWebCustomer Input document:\n"
					+ yfcInDoc.toString());
		}
		YFCDocument getCustomerListOp = checkCustomerProfileID(env, yfcInDoc,
				organizationCode);
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("createWebCustomer::getCustomerListOp::\n"
					+ getCustomerListOp.toString());
		}
		YFCNodeList<YFCElement> nlCustomer = getCustomerListOp
				.getDocumentElement()
				.getElementsByTagName(GCConstants.CUSTOMER);
		if (nlCustomer.getLength() > 0) {
			LOGGER.verbose("createWebCustomer:: Customer Exist in getCustomerList API call::\n");
			LOGGER.verbose("Get CustomerIdFrom getOrderListOutput and stamp to input Customer\n");
			// Fetch Customer with Last modifyts
			YFCElement customerIdFromOp = nlCustomer.item(0);
			String customerIDFromOp = customerIdFromOp
					.getAttribute(GCConstants.CUSTOMER_ID);
			yfcInDoc.getDocumentElement().setAttribute(GCConstants.CUSTOMER_ID,
					customerIDFromOp);
			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("Clone Indoc after stamping CustomerID::\n"
						+ yfcInDoc.toString());
			}
			LOGGER.verbose("call validateAndUpdateUniqueInputCustomerAddress");
			validateAndUpdateUniqueInputCustomerAddress(yfcInDoc,
					getCustomerListOp);
		}
		// code fix for GCPRO-302 begin
		// Publish customer details on create or update to DAX for  GCPRO Customers
		// boolean isGcPro = false;
		// YFCElement eleCustomer = yfcInDoc.getDocumentElement();
		// YFCElement eleExtn = eleCustomer.getChildElement(GCConstants.EXTN);
	    // String sExtnIsGcProCustomer = eleExtn.getAttribute(GCConstants.EXTN_IS_GCPRO_CUSTOMER);
	       // if (YFCCommon.equals(sExtnIsGcProCustomer, GCConstants.FLAG_Y) || YFCCommon.equals(sExtnIsGcProCustomer, GCConstants.FLAG_N)) {	  
					// isGcPro = true;			
		  // }
		// if(!isGcPro){
			// env.setTxnObject("Publish", "N");	
		// }
		// code fix for GCPRO-302 End
		
		invokeManageCustomer(env, yfcInDoc);
		LOGGER.endTimer("GCManageCustomer.createWebCustomer");
		LOGGER.verbose("Class: GCManageCustomer Method: createWebCustomer:: END");
	}

	/**
	 * This method calls getCustomerList API with ExtnSourceCustomerID and
	 * return output document of API if Customer exists. If Customer do not
	 * exist than call method callGetCustomerListWithEmailId.
	 *
	 * @param env
	 * @param yfcInDoc
	 * @param organizationCode
	 * @return getCustomerListOp : if CustomerExist for ExtnSourceCustomerID
	 */
	public YFCDocument checkCustomerProfileID(YFSEnvironment env,
			YFCDocument yfcInDoc, String organizationCode) {
		LOGGER.beginTimer("GCManageCustomer.checkCustomerProfileID");
		LOGGER.verbose("Class: GCManageCustomer Method: checkCustomerProfileID:: BEGIN");
		YFCDocument getCustomerListIp = YFCDocument
				.createDocument(GCConstants.CUSTOMER);
		YFCElement customerEle = getCustomerListIp.getDocumentElement();
		customerEle.setAttribute(GCConstants.ORGANIZATION_CODE,
				organizationCode);
		YFCElement eExtn = customerEle.createChild(GCConstants.EXTN);
		eExtn.setAttribute(GCXmlLiterals.EXTN_SRC_CSTOMER_ID, yfcInDoc
				.getDocumentElement().getChildElement(GCConstants.EXTN)
				.getAttribute(GCXmlLiterals.EXTN_SRC_CSTOMER_ID));
		YFCDocument getCustomerListTemp = YFCDocument
				.getDocumentFor(GCCommonUtil
						.getDocumentFromPath("/global/template/api/getCustomerList_TemplateXML.xml"));
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("checkCustomerProfileID::getCustomerListTemp :\n "
					+ getCustomerListTemp.toString());
		}
		YFCDocument getCustomerListOp = GCCommonUtil.invokeAPI(env,
				GCConstants.GET_CUSTOMER_LIST, getCustomerListIp,
				getCustomerListTemp);
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("checkCustomerProfileID::getCustomerListOp for ExtnSourceCustomerID :\n "
					+ getCustomerListOp.toString());
		}
		// getCustomerListOp can contain maximum one Customer as called with
		// ExtnSourceCustomerID
		YFCNodeList<YFCElement> nlCustomer = getCustomerListOp
				.getDocumentElement()
				.getElementsByTagName(GCConstants.CUSTOMER);
		if (nlCustomer.getLength() > 0) {
			LOGGER.verbose("return getCustomerList output if Customer exist for ExtnSourceCustomerID");
			return getCustomerListOp;
		} else {
			LOGGER.verbose("Customer do not exist for input ExtnSourceCustomerID.Call checkCustomerEmail method\n");
			LOGGER.verbose("return getCustomerList output if called on EmailID as Input");
			return callGetCustomerListWithEmailId(env, yfcInDoc);
		}
	}

	/**
	 * This method returns getCustomerList output which is called on EmailID as
	 * input.
	 * 
	 * @param env
	 * @param yfcInDoc
	 * @return
	 */
	private YFCDocument callGetCustomerListWithEmailId(YFSEnvironment env,
			YFCDocument yfcInDoc) {
		LOGGER.beginTimer("GCManageCustomer.callGetCustomerListWithEmailId");
		LOGGER.verbose("Class: GCManageCustomer Method: callGetCustomerListWithEmailId:: BEGIN");
		YFCElement rootEle = yfcInDoc.getDocumentElement();
		String rootElementName = rootEle.getNodeName();
		String orgCode = "";
		String sEmailID = "";
		if (YFCCommon.equals(GCXmlLiterals.CUSTOMER, rootElementName)) {
			// EmailID from Customer Feed
			orgCode = rootEle.getAttribute(GCConstants.ORGANIZATION_CODE);
			YFCElement eCustomerContactList = rootEle
					.getChildElement(GCConstants.CUSTOMER_CONTACT_LIST);
			YFCElement eCustomerContact = eCustomerContactList
					.getChildElement(GCXmlLiterals.CUSTOMER_CONTACT);
			sEmailID = eCustomerContact
					.getAttribute(GCXmlLiterals.CUSTOMER_CONTACT_EMAIL_ID);
		} else if (YFCCommon.equals(GCConstants.ORDER, rootElementName)) {
			// EMailID from Web Orders. Order will root element name
			orgCode = rootEle.getAttribute(GCConstants.ENTERPRISE_CODE);
			YFCElement ePersonInfoBillTo = rootEle
					.getChildElement(GCXmlLiterals.PERSON_INFO_BILL_TO);
			sEmailID = ePersonInfoBillTo
					.getAttribute(GCXmlLiterals.PERSON_INFO_BILL_TO_EMAIL_ID);
		} else {
			LOGGER.verbose("Input Document passed must have 'Order' or 'Customer' as root element.raiseException");
			YFCException excep = new YFCException(
					GCErrorConstants.INVALID_CUSTOMER_SYNC_ROOT_ELE);
			excep.setAttribute(GCXmlLiterals.ORGANIZATION_CODE, orgCode);
			excep.setAttribute(GCConstants.EMAIL_ID, sEmailID);
			LOGGER.endTimer("raiseException");
		}
		if (!YFCCommon.isVoid(sEmailID)) {
			LOGGER.verbose("EmailID is not void");
			YFCDocument getCustomerListIp = YFCDocument
					.createDocument(GCConstants.CUSTOMER);
			YFCElement customerEle = getCustomerListIp.getDocumentElement();
			customerEle.setAttribute(GCConstants.ORGANIZATION_CODE, orgCode);
			// logic added in input to get latest modified customer as first
			// customer
			YFCElement eOrderBy = customerEle
					.createChild(GCXmlLiterals.ORDER_BY);
			YFCElement eAttribute = eOrderBy
					.createChild(GCXmlLiterals.ATTRIBUTE);
			eAttribute.setAttribute(GCXmlLiterals.DESC, GCConstants.YES);
			eAttribute.setAttribute(GCXmlLiterals.NAME, GCXmlLiterals.MODIFYTS);
			YFCElement customerContactListEle = customerEle
					.createChild(GCXmlLiterals.CUSTOMER_CONTACT_LIST);
			YFCElement customerContactEle = customerContactListEle
					.createChild(GCXmlLiterals.CUSTOMER_CONTACT);
			// manageCustomer accept EmailID and not PersonInfoBillto EMailID
			customerContactEle.setAttribute(
					GCXmlLiterals.CUSTOMER_CONTACT_EMAIL_ID, sEmailID);
			YFCDocument getCustomerListTemp = YFCDocument
					.getDocumentFor(GCCommonUtil
							.getDocumentFromPath("/global/template/api/getCustomerList_TemplateXML.xml"));
			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("callGetCustomerListWithEmailId::getCustomerList template:\n "
						+ getCustomerListTemp);
			}
			return GCCommonUtil.invokeAPI(env, GCConstants.GET_CUSTOMER_LIST,
					getCustomerListIp, getCustomerListTemp);
		} else {
			LOGGER.error("Input Document passed must have EMAILID");
			YFCException excep = new YFCException(
					GCErrorConstants.EMAIL_ID_MISSING);
			excep.setAttribute(GCXmlLiterals.ORGANIZATION_CODE, orgCode);
			excep.setAttribute(GCConstants.EMAIL_ID, sEmailID);
			LOGGER.endTimer("raiseException");
			throw excep;
		}
	}

	/**
	 * This method calls manageCustomer API with Output Template
	 *
	 * @param env
	 * @param yfcInDoc
	 */
	private void invokeManageCustomer(YFSEnvironment env, YFCDocument yfcInDoc) {
		LOGGER.beginTimer("GCManageCustomer.invokeManageCustomer");
		LOGGER.verbose("Class: GCManageCustomer Method: invokeManageCustomer:: BEGIN");

		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("invokeManageCustomer:: manageCustomer Template"
					+ YFCDocument
							.getDocumentFor(GCConstants.MANAGE_CUSTOMER_API_TEMP));
			LOGGER.verbose("invokeManageCustomer::manageCustomer Input"
					+ yfcInDoc);
		}
		YFCDocument manageCustomerDoc = GCCommonUtil.invokeAPI(env,
				GCConstants.MANAGE_CUSTOMER_API, yfcInDoc, YFCDocument
						.getDocumentFor(GCConstants.MANAGE_CUSTOMER_API_TEMP));
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("invokeManageCustomer::manageCustomer Output"
					+ manageCustomerDoc);
		}
		LOGGER.endTimer("GCManageCustomer.invokeManageCustomer");
		LOGGER.verbose("GCManageCustomer.invokeManageCustomer method :: END");

	}

	/**
	 * This method Validate CustomerAdditionalAddress as unique based on
	 * AddressLine1, AddressLine2, AddressLine3 and DayPhone of input xml and
	 * getCustomerList Output. This method removes CustomerAdditionalAddress
	 * from input if it is same as Output xml of getCustomerList output
	 *
	 * @param env
	 * @param yfcInDoc
	 * @param getCustomerListOp
	 */
	private void validateAndUpdateUniqueInputCustomerAddress(
			YFCDocument yfcInDoc, YFCDocument getCustomerListOp) {
		LOGGER.beginTimer("GCManageCustomer.validateAndUpdateUniqueInputCustomerAddress");
		LOGGER.verbose("Class: GCManageCustomer Method: validateAndUpdateUniqueInputCustomerAddress:: BEGIN");
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.debug(" validateAndUpdateUniqueInputCustomerAddress() method, Input document is"
					+ yfcInDoc.toString());
			LOGGER.debug(" validateAndUpdateUniqueInputCustomerAddress() method, getCustomerListOp document is"
					+ getCustomerListOp.toString());
		}
		// List to contain unique key of PersonInfo from Op
		Map<String, YFCElement> uniqueCustomerAdditionalAddressMap = new HashMap<String, YFCElement>();
		// List to capture CustomerContactID
		String sCustomerContactIdFromOp = "";
		YFCElement eCustomerListFromOp = getCustomerListOp.getDocumentElement();
		// first customer of getCustomerList will contain latest Customer
		YFCElement eCustomerFromOp = eCustomerListFromOp.getElementsByTagName(
				GCConstants.CUSTOMER).item(0);
		YFCElement eCustomerContactListOp = eCustomerFromOp
				.getChildElement(GCXmlLiterals.CUSTOMER_CONTACT_LIST);
		Iterator<YFCElement> itrCustContact = eCustomerContactListOp
				.getChildren().iterator();
		while (itrCustContact.hasNext()) {
			YFCElement eCustomerContactFromOp = itrCustContact.next();
			// get and set customerContact id
			sCustomerContactIdFromOp = eCustomerContactFromOp
					.getAttribute(GCXmlLiterals.CUSTOMER_CONTACT_ID);
			YFCElement eCustAdditionalAddListOp = eCustomerContactFromOp
					.getChildElement(GCXmlLiterals.CUSTOMER_ADDITIONAL_ADD_LIST);
			if (!YFCCommon.isVoid(eCustAdditionalAddListOp)) {
				Iterator<YFCElement> itrCustomerAdditionalAdd = eCustAdditionalAddListOp
						.getChildren().iterator();
				while (itrCustomerAdditionalAdd.hasNext()) {
					YFCElement eCustomerAdditionalAddFromOp = itrCustomerAdditionalAdd
							.next();
					YFCElement ePersonInfoFromOp = eCustomerAdditionalAddFromOp
							.getChildElement(GCConstants.PERSON_INFO);
					if (!YFCCommon.isVoid(ePersonInfoFromOp)) {
						StringBuilder uniqueCustomerAddCombinationForOp = new StringBuilder();
						// Start GCSTORE-2495
						String sAddressLine1 = ePersonInfoFromOp
								.getAttribute(GCConstants.ADDRESS_LINE1);
						String sAddressLine2 = ePersonInfoFromOp
								.getAttribute(GCConstants.ADDRESS_LINE2);
						String sAddressLine3 = ePersonInfoFromOp
								.getAttribute(GCConstants.ADDRESS_LINE3);
						String sDayPhone = ePersonInfoFromOp
								.getAttribute(GCConstants.DAY_PHONE);
						uniqueCustomerAddCombinationForOp
								.append(sAddressLine1 != null ? sAddressLine1
										.toUpperCase().trim() : "")
								.append("|")
								.append(sAddressLine2 != null ? sAddressLine2
										.toUpperCase().trim() : "")
								.append("|")
								.append(sAddressLine3 != null ? sAddressLine3
										.toUpperCase().trim() : "")
								.append("|")
								.append(sDayPhone != null ? sDayPhone
										.toUpperCase().trim() : "").append("|");
						// End GCSTORE-2495
						uniqueCustomerAdditionalAddressMap.put(
								uniqueCustomerAddCombinationForOp.toString(),
								eCustomerAdditionalAddFromOp);
						uniqueCustomerAddCombinationForOp.setLength(0);
					}
				}
			}
		}

		// Check Person of input in list and remove if it exist in op list
		YFCElement inDocCustomerEle = yfcInDoc.getDocumentElement();
		YFCElement customerContactListEle = inDocCustomerEle.getChildElement(
				GCXmlLiterals.CUSTOMER_CONTACT_LIST, true);
		Iterator<YFCElement> itrCustomerContactIp = customerContactListEle
				.getChildren().iterator();
		while (itrCustomerContactIp.hasNext()) {
			YFCElement customerContactEle = itrCustomerContactIp.next();
			customerContactEle.setAttribute(GCXmlLiterals.CUSTOMER_CONTACT_ID,
					sCustomerContactIdFromOp);
			YFCElement customerAdditionalAddressListEle = customerContactEle
					.getChildElement(GCXmlLiterals.CUSTOMER_ADDITIONAL_ADD_LIST);
			if (!YFCCommon.isVoid(customerAdditionalAddressListEle)) {

				Iterator<YFCElement> itrCustomerAdditionalAddIp = customerAdditionalAddressListEle
						.getChildren().iterator();
				while (itrCustomerAdditionalAddIp.hasNext()) {
					YFCElement customerAdditionalAddressEle = itrCustomerAdditionalAddIp
							.next();
					YFCElement personInfoEle = customerAdditionalAddressEle
							.getChildElement(GCConstants.PERSON_INFO);
					StringBuilder uniqueCustomerAddCombinationForIp = null;
					if (!YFCCommon.isVoid(personInfoEle)) {
						uniqueCustomerAddCombinationForIp = new StringBuilder();
						// Getting PersonInfo details
						// Start GCSTORE-2495
						String sAddressLine1 = personInfoEle
								.getAttribute(GCConstants.ADDRESS_LINE1);
						String sAddressLine2 = personInfoEle
								.getAttribute(GCConstants.ADDRESS_LINE2);
						String sAddressLine3 = personInfoEle
								.getAttribute(GCConstants.ADDRESS_LINE3);
						String sDayPhone = personInfoEle
								.getAttribute(GCConstants.DAY_PHONE);
						uniqueCustomerAddCombinationForIp
								.append(sAddressLine1 != null ? sAddressLine1
										.toUpperCase().trim() : "")
								.append("|")
								.append(sAddressLine2 != null ? sAddressLine2
										.toUpperCase().trim() : "")
								.append("|")
								.append(sAddressLine3 != null ? sAddressLine3
										.toUpperCase().trim() : "")
								.append("|")
								.append(sDayPhone != null ? sDayPhone
										.toUpperCase().trim() : "").append("|");
						// End GCSTORE-2495
						if (uniqueCustomerAdditionalAddressMap
								.containsKey(uniqueCustomerAddCombinationForIp
										.toString())) {
							String sCustomerAdditionalAddressIDFromMap = uniqueCustomerAdditionalAddressMap
									.get(uniqueCustomerAddCombinationForIp
											.toString())
									.getAttribute(
											GCConstants.CUSTOMER_ADDITIONAL_ADDRESS_ID);
							customerAdditionalAddressEle.setAttribute(
									GCConstants.CUSTOMER_ADDITIONAL_ADDRESS_ID,
									sCustomerAdditionalAddressIDFromMap);
						}
					}
				}
			}
		}
		LOGGER.endTimer("GCManageCustomer.validateAndUpdateUniqueInputCustomerAddress");
		LOGGER.verbose("GCManageCustomer.validateAndUpdateUniqueInputCustomerAddress method :: END");
	}

	/**
	 *
	 * stampOrderAddressOnMap method is used to stamp all addresses info
	 * (PerosnInfoKey and PersonInfoShipTo/PersonInfoBillTo) elements on a map.
	 *
	 * @param inDoc
	 * @param uniqueaddressInOrderMap
	 * @throws GCException
	 *
	 */

	public void stampOrderAddressOnMap(Document inDoc,
			Map<String, Element> uniqueaddressInOrderMap) throws GCException {

		// Stamping all addresses present on order in a map.
		LOGGER.debug(" Entering GCManageCustomer.stampOrderAddressOnMap() method ");

		try {
			Element eleRootInDoc = inDoc.getDocumentElement();

			// Getting PersonInfoBillTo details
			Element eleHeaderPersonInfoBillTo = (Element) eleRootInDoc
					.getElementsByTagName("PersonInfoBillTo").item(0);

			if (!YFCCommon.isVoid(eleHeaderPersonInfoBillTo)) {

				StringBuilder uniqueAddressCombination = new StringBuilder();

				uniqueAddressCombination
						.append(eleHeaderPersonInfoBillTo
								.getAttribute("AddressLine1").toUpperCase()
								.trim())
						.append("|")
						.append(eleHeaderPersonInfoBillTo
								.getAttribute("AddressLine2").toUpperCase()
								.trim())
						.append("|")
						.append(eleHeaderPersonInfoBillTo
								.getAttribute("AddressLine3").toUpperCase()
								.trim())
						.append("|")
						.append(eleHeaderPersonInfoBillTo
								.getAttribute("DayPhone").toUpperCase().trim())
						.append("|");

				Element eleExtn = (Element) eleHeaderPersonInfoBillTo
						.getElementsByTagName("Extn").item(0);
				if (!YFCCommon.isVoid(eleExtn)) {
					sExtnBirthdate = eleExtn.getAttribute("ExtnBirthday");
				}

				uniqueaddressInOrderMap.put(
						uniqueAddressCombination.toString(),
						eleHeaderPersonInfoBillTo);
				sOrderBillToAddressIdentifier = uniqueAddressCombination
						.append("|BillToAddress").toString();
				uniqueAddressCombination.setLength(0);
			}

			// Getting PersonInfoShipTo line details
			List<Element> listOrderLine = GCXMLUtil.getElementListByXpath(
					inDoc, "/Order/OrderLines/OrderLine");

			for (Element eleOrderline : listOrderLine) {

				Element elePersonInfoShipTo = (Element) eleOrderline
						.getElementsByTagName("PersonInfoShipTo").item(0);

				if (!YFCCommon.isVoid(elePersonInfoShipTo)) {

					StringBuilder uniqueAddressCombination = new StringBuilder();

					uniqueAddressCombination
							.append(elePersonInfoShipTo
									.getAttribute("AddressLine1").toUpperCase()
									.trim())
							.append("|")
							.append(elePersonInfoShipTo
									.getAttribute("AddressLine2").toUpperCase()
									.trim())
							.append("|")
							.append(elePersonInfoShipTo
									.getAttribute("AddressLine3").toUpperCase()
									.trim())
							.append("|")
							.append(elePersonInfoShipTo
									.getAttribute("DayPhone").toUpperCase()
									.trim()).append("|");

					uniqueaddressInOrderMap.put(
							uniqueAddressCombination.toString(),
							elePersonInfoShipTo);
					uniqueAddressCombination.setLength(0);
				}
			}

			// Getting PersonInfoShipTo details
			Element eleHeaderPersonInfoShipTo = GCXMLUtil.getElementByXPath(
					inDoc, "Order/PersonInfoShipTo");

			if (!YFCCommon.isVoid(eleHeaderPersonInfoShipTo)) {

				StringBuilder uniqueAddressCombination = new StringBuilder();

				uniqueAddressCombination
						.append(eleHeaderPersonInfoShipTo
								.getAttribute("AddressLine1").toUpperCase()
								.trim())
						.append("|")
						.append(eleHeaderPersonInfoShipTo
								.getAttribute("AddressLine2").toUpperCase()
								.trim())
						.append("|")
						.append(eleHeaderPersonInfoShipTo
								.getAttribute("AddressLine3").toUpperCase()
								.trim())
						.append("|")
						.append(eleHeaderPersonInfoShipTo
								.getAttribute("DayPhone").toUpperCase().trim())
						.append("|");

				uniqueaddressInOrderMap.put(
						uniqueAddressCombination.toString(),
						eleHeaderPersonInfoShipTo);
				uniqueAddressCombination.setLength(0);
			}
		} catch (Exception e) {
			LOGGER.error(
					"Inside catch block of GCManageCustomer.stampOrderAddressOnMap method. Exception is",
					e);
			throw new GCException(e);

		}
	}

	/**
	 *
	 * checkChangeInAddress method is used to check if the address present on
	 * order for a particular customer changed.
	 *
	 * @param env
	 * @param getCustomerListOutDoc
	 * @param uniqueaddressInOrderMap
	 * @throws GCException
	 *
	 */

	public void checkChangeInAddress(YFSEnvironment env,
			Document getCustomerListOutDoc,
			Map<String, Element> uniqueaddressInOrderMap, Document inDoc)
			throws GCException {

		try {
			List<Element> listPresentCustomerPersonInfoList = GCXMLUtil
					.getElementListByXpath(
							getCustomerListOutDoc,
							"CustomerList/Customer/CustomerContactList/CustomerContact/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo");

			Element eleCustomerContact = (Element) getCustomerListOutDoc
					.getElementsByTagName("CustomerContact").item(0);
			String sCustomerContactKey = eleCustomerContact
					.getAttribute("CustomerContactKey");
			Element eleCustomer = (Element) getCustomerListOutDoc
					.getElementsByTagName("Customer").item(0);
			String sCustomerID = eleCustomer
					.getAttribute(GCConstants.CUSTOMER_ID);
			String sOrganizationCode = eleCustomer
					.getAttribute(GCConstants.ORGANIZATION_CODE);

			for (Element elePersonInfo : listPresentCustomerPersonInfoList) {

				StringBuilder uniqueAddressCombination = new StringBuilder();

				uniqueAddressCombination
						.append(elePersonInfo.getAttribute("AddressLine1")
								.toUpperCase().trim())
						.append("|")
						.append(elePersonInfo.getAttribute("AddressLine2")
								.toUpperCase().trim())
						.append("|")
						.append(elePersonInfo.getAttribute("AddressLine3")
								.toUpperCase().trim())
						.append("|")
						.append(elePersonInfo.getAttribute("DayPhone")
								.toUpperCase().trim()).append("|");

				if (uniqueaddressInOrderMap
						.containsKey(uniqueAddressCombination.toString())) {

					uniqueaddressInOrderMap.remove(uniqueAddressCombination
							.toString());
				} else {
					// GCSTORE-1979::Begin
					LOGGER.verbose("GCSTORE::1979::Begin");
					Iterator<Map.Entry<String, Element>> entries = uniqueaddressInOrderMap
							.entrySet().iterator();
					while (entries.hasNext()) {
						Map.Entry<String, Element> entry = entries.next();
						String uniqueaddressInOrderMapEntry = entry.getKey()
								+ "junk";
						String[] uniqueaddressInOrderMapEntrySplit = uniqueaddressInOrderMapEntry
								.split("\\|");
						String dayPhone = uniqueaddressInOrderMapEntrySplit[3];
						// customer DayPhone
						/* Production Issue Fix : Added junk in uniqueAddressCombination to avoid ArrayOutOfBoundException if customer address does not have dayPhone. */
						uniqueAddressCombination.append("junk");
						// Fix End
						String[] uniqueAddressCombinationSplit = uniqueAddressCombination
								.toString().split("\\|");
						String customerDayPhone = uniqueAddressCombinationSplit[3];
						LOGGER.verbose("Order Day Phone::" + dayPhone
								+ "::Customer Day Phone::" + customerDayPhone);
						if (YFCCommon.isVoid(dayPhone)
								&& !YFCCommon.isVoid(customerDayPhone)) {
							StringBuilder uniqueAddressCombinationWithBlankDayPhone = new StringBuilder();

							uniqueAddressCombinationWithBlankDayPhone
									.append(elePersonInfo
											.getAttribute("AddressLine1")
											.toUpperCase().trim())
									.append("|")
									.append(elePersonInfo
											.getAttribute("AddressLine2")
											.toUpperCase().trim())
									.append("|")
									.append(elePersonInfo
											.getAttribute("AddressLine3")
											.toUpperCase().trim()).append("|")
									.append("|");
							LOGGER.verbose("AddressKeywithoutDayPhoen of Customer::"
									+ uniqueAddressCombinationWithBlankDayPhone
											.toString());
							if (uniqueaddressInOrderMap
									.containsKey(uniqueAddressCombinationWithBlankDayPhone
											.toString())) {
								LOGGER.verbose("removing this from map ");
								uniqueaddressInOrderMap
										.remove(uniqueAddressCombinationWithBlankDayPhone
												.toString());
								entries = uniqueaddressInOrderMap.entrySet()
										.iterator();
							}

						}
					}
					LOGGER.verbose("GCSTORE::1979::End");
					// GCSTORE-1979::End
				}
				uniqueAddressCombination.setLength(0);
			}

			if (!uniqueaddressInOrderMap.isEmpty()) {
				createOrUpdateCustomerFromOrder(env, uniqueaddressInOrderMap,
						GCConstants.BLANK, sOrganizationCode, sCustomerID,
						sCustomerContactKey, inDoc.getDocumentElement()
								.getAttribute(GCConstants.ORDER_HEADER_KEY),
						inDoc);
			} else if (YFCCommon.isVoid(inDoc.getDocumentElement()
					.getAttribute(GCConstants.BILL_TO_ID))) {
				YFCDocument changeOrderDoc = YFCDocument.createDocument("Order");
				YFCElement eleOrder = changeOrderDoc.getDocumentElement();
				eleOrder.setAttribute(
						"OrderHeaderKey",
						inDoc.getDocumentElement().getAttribute(
								GCConstants.ORDER_HEADER_KEY));
			    eleOrder.setAttribute(GCConstants.SELECT_METHOD, "NO_LOCK");
				eleOrder.setAttribute("BillToID", sCustomerID);
				YFCElement eleOrderHoldTypes = eleOrder
						.createChild("OrderHoldTypes");
				YFCElement eleOrderHoldType = eleOrderHoldTypes
						.createChild("OrderHoldType");
				eleOrderHoldType.setAttribute("HoldType",
						GCConstants.ATGNULLBILLTOIDHOLD);
				eleOrderHoldType.setAttribute("ReasonText",
						"Bill To ID not Stamped for ATG Order");
				eleOrderHoldType.setAttribute("Status", "1300");
				Document changeOrder=changeOrderDoc.getDocument();
				GCCommonUtil.invokeAPI(env,
						GCConstants.CHANGE_ORDER,changeOrder);
			}
		} catch (Exception e) {
			LOGGER.error(
					"Inside catch block of GCManageCustomer.checkChangeInAddress method. Exception is",
					e);
			throw new GCException(e);
		}
	}

	/**
	 *
	 * createOrUpdateCustomerFromOrder method is used to create/modify customer.
	 *
	 * @param env
	 * @param uniqueaddressInOrderMap
	 * @param sExtnDAXCustomerId
	 * @param sOrganizationCode
	 * @param sCustomerID
	 * @param sCustomerContactKey
	 * @throws GCException
	 *
	 */

	public void createOrUpdateCustomerFromOrder(YFSEnvironment env,
			Map<String, Element> uniqueaddressInOrderMap,
			String sExtnDAXCustomerId, String sOrganizationCode,
			String sCustomerID, String sCustomerContactKey,
			String sOrderHeaderKey, Document inDoc) throws GCException {
		try {
			// Phase-2 Start
			Element inDocExtnEle = (Element) inDoc.getDocumentElement()
					.getElementsByTagName("Extn").item(0);
			String sExtnSourceSystemFrmIndoc = inDocExtnEle
					.getAttribute("ExtnSourceSystem");
			String sExtnSourceCustomerIDFrmIndoc = "";
			if (YFCCommon.equals("ATG", sExtnSourceSystemFrmIndoc)) {
				sExtnSourceCustomerIDFrmIndoc = inDocExtnEle
						.getAttribute("ExtnSourceCustomerID");
			}
			// Phase-2 End
			Document manageCustomerIn = GCXMLUtil
					.createDocument(GCConstants.CUSTOMER);
			Element eleManageCustomerRootElement = manageCustomerIn
					.getDocumentElement();
			Element eleManageCustomerExtn = manageCustomerIn
					.createElement("Extn");
			eleManageCustomerRootElement.appendChild(eleManageCustomerExtn);
			Element eleCustomerContactList = manageCustomerIn
					.createElement("CustomerContactList");
			eleManageCustomerRootElement.appendChild(eleCustomerContactList);
			Element eleCustomerContact = manageCustomerIn
					.createElement("CustomerContact");
			eleCustomerContactList.appendChild(eleCustomerContact);
			Element eleCustomerAdditionalAddressList = manageCustomerIn
					.createElement("CustomerAdditionalAddressList");
			eleCustomerContact.appendChild(eleCustomerAdditionalAddressList);
			eleManageCustomerRootElement.setAttribute(
					GCConstants.CUSTOMER_TYPE,
					GCConstants.CONSUMER_CUSTOMER_CODE);
			eleManageCustomerRootElement.setAttribute(
					GCConstants.ORGANIZATION_CODE, sOrganizationCode);

			// stamping attributes from PersonInfoBillTo on CustomerContact
			// Element
			String sDayFaxNo = "";
			String sDayPhone = "";
			String sEMailID = "";
			String sEveningPhone = "";
			String sFirstName = "";
			String sLastName = "";
			String sMiddleName = "";
			String sMobilePhone = "";
			String sTitle = "";

			// iterate on map to append all unique address on customer.
			Iterator<Map.Entry<String, Element>> mapEntries = uniqueaddressInOrderMap
					.entrySet().iterator();

			while (mapEntries.hasNext()) {

				Element elePersonInfoOnMapExtn = null;
				Map.Entry<String, Element> mapEntry = mapEntries.next();
				String uniqueAddressCombination = mapEntry.getKey();
				Element elePersonInfoOnMap = mapEntry.getValue();

				Element eleCustomerAdditionalAddress = manageCustomerIn
						.createElement("CustomerAdditionalAddress");
				if (uniqueAddressCombination.concat("|BillToAddress")
						.equalsIgnoreCase(sOrderBillToAddressIdentifier)) {
					eleCustomerAdditionalAddress.setAttribute(
							GCConstants.IS_DEFAULT_BILLTO, GCConstants.YES);
					eleCustomerAdditionalAddress.setAttribute(
							GCConstants.IS_BILLTO, GCConstants.YES);

					// stamp ExtnBirthday on customerContact birthday
					if (!YFCCommon.isVoid(sExtnBirthdate)) {
						eleCustomerContact.setAttribute(
								GCConstants.DATE_OF_BIRTH, sExtnBirthdate);
					}

					Element elePersonInfo = manageCustomerIn
							.createElement("PersonInfo");
					eleCustomerAdditionalAddress.appendChild(elePersonInfo);

					GCXMLUtil.copyAttributes(elePersonInfoOnMap, elePersonInfo);
					elePersonInfoOnMapExtn = (Element) elePersonInfoOnMap
							.getElementsByTagName("Extn").item(0);
					if (!YFCCommon.isVoid(elePersonInfoOnMapExtn)) {
						Element elePersonInfoExtn = manageCustomerIn
								.createElement("Extn");
						GCXMLUtil.copyAttributes(elePersonInfoOnMapExtn,
								elePersonInfoExtn);
						elePersonInfo.appendChild(elePersonInfoExtn);
					}

					sDayFaxNo = elePersonInfoOnMap.getAttribute("DayFaxNo");
					sDayPhone = elePersonInfoOnMap.getAttribute("DayPhone");
					sEMailID = elePersonInfoOnMap.getAttribute("EMailID");
					sEveningPhone = elePersonInfoOnMap
							.getAttribute("EveningPhone");
					sFirstName = elePersonInfoOnMap.getAttribute("FirstName");
					sLastName = elePersonInfoOnMap.getAttribute("LastName");
					sMiddleName = elePersonInfoOnMap.getAttribute("MiddleName");
					sMobilePhone = elePersonInfoOnMap
							.getAttribute("MobilePhone");
					sTitle = elePersonInfoOnMap.getAttribute("Title");

				} else {
					Element elePersonInfo = manageCustomerIn
							.createElement("PersonInfo");
					eleCustomerAdditionalAddress.appendChild(elePersonInfo);

					GCXMLUtil.copyAttributes(elePersonInfoOnMap, elePersonInfo);
					elePersonInfoOnMapExtn = (Element) elePersonInfoOnMap
							.getElementsByTagName("Extn").item(0);
					if (!YFCCommon.isVoid(elePersonInfoOnMapExtn)) {
						Element elePersonInfoExtn = manageCustomerIn
								.createElement("Extn");
						GCXMLUtil.copyAttributes(elePersonInfoOnMapExtn,
								elePersonInfoExtn);
						elePersonInfo.appendChild(elePersonInfoExtn);
					}
				}
				eleCustomerAdditionalAddressList
						.appendChild(eleCustomerAdditionalAddress);
			}

			// if customer address is to be updated
			if (!YFCCommon.isVoid(sCustomerID)
					&& !YFCCommon.isVoid(sCustomerContactKey)) {
				eleManageCustomerRootElement.setAttribute(
						GCConstants.CUSTOMER_ID, sCustomerID);
				eleCustomerContact.setAttribute("CustomerContactKey",
						sCustomerContactKey);
				// Phase-2: Start
				// Add ExtnSourceCustomerID for ATG Orders
				if (YFCCommon.equals("ATG", sExtnSourceSystemFrmIndoc)) {
					eleManageCustomerExtn.setAttribute("ExtnSourceCustomerID",
							sExtnSourceCustomerIDFrmIndoc);
					// Phase-2: End
				}
			} else {
				// if customer is created
				// Phase-2 Start
				// Add ExtnSourceCustomerID for ATG Orders
				if (YFCCommon.equals("ATG", sExtnSourceSystemFrmIndoc)) {
					eleManageCustomerExtn.setAttribute("ExtnSourceCustomerID",
							sExtnSourceCustomerIDFrmIndoc);
				}
				// Phase-2 End
				eleManageCustomerExtn.setAttribute(
						GCConstants.EXTN_DAX_CUSTOMER_ID, sExtnDAXCustomerId);
				eleManageCustomerRootElement.setAttribute("Status",
						GCConstants.CUSTOMER_ACTIVE_STATUS);
				eleCustomerContact.setAttribute("DayFaxNo", sDayFaxNo);
				eleCustomerContact.setAttribute("DayPhone", sDayPhone);
				eleCustomerContact.setAttribute("EmailID", sEMailID);
				eleCustomerContact.setAttribute("EveningPhone", sEveningPhone);
				eleCustomerContact.setAttribute("FirstName", sFirstName);
				eleCustomerContact.setAttribute("LastName", sLastName);
				eleCustomerContact.setAttribute("MiddleName", sMiddleName);
				eleCustomerContact.setAttribute("MobilePhone", sMobilePhone);
				eleCustomerContact.setAttribute("Title", sTitle);
			}
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(GCXMLUtil.getXMLString(manageCustomerIn));
			}
			// CR-48 Start
			// This checks when the xml is coming from create order on success
			// event and the customer do
			// not exist in DB
			Element extnEle = (Element) inDoc.getDocumentElement()
					.getElementsByTagName(GCConstants.EXTN).item(0);
			String sExtnSourceSystem = extnEle.getAttribute("ExtnSourceSystem");

			boolean isManageApiCallReq = false;
			if (!YFCCommon.equals("ATG", sExtnSourceSystem)) {
				isManageApiCallReq = checkCustomerExist(env, inDoc);
			}

			Document manageCustomerOutDoc = null;
			if (!isManageApiCallReq) {
				manageCustomerOutDoc = GCCommonUtil.invokeAPI(env,
						manageCustomerIn, GCConstants.MANAGE_CUSTOMER_API, "");
			}
			//GCSTORE-6957 -The Email Id entered already exist in record issue fix Start-
			/*else {
				YFSException ex = new YFSException();
				ex.setErrorCode("The Email Id already exist in record");
				ex.setErrorDescription("The Email Id entered already exist in record, use other Email ID ");
				throw ex;
			} */ //End
			// CR-48 End
			/*if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Output doc of managecustomer is"
						+ GCXMLUtil.getXMLString(manageCustomerOutDoc));
			} */
			// Stamp newly created customerid on the order.
			String sNewCustomerID = null;
			if(!YFCCommon.isVoid(manageCustomerOutDoc)){
			   sNewCustomerID = manageCustomerOutDoc.getDocumentElement()
					.getAttribute(GCConstants.CUSTOMER_ID);
			}
			/* Production Issue Fix : Added condition YFCCommon.equals("ATG", sExtnSourceSystem) to apply or resolved BillToID hold only in case of ATG Customer. */
			if (!YFCCommon.isVoid(sOrderHeaderKey) && YFCCommon.equals("ATG", sExtnSourceSystem)) {
				YFCDocument changeOrderDoc = YFCDocument.createDocument("Order");
				YFCElement eleOrder = changeOrderDoc.getDocumentElement();
				eleOrder.setAttribute("OrderHeaderKey", sOrderHeaderKey);
				eleOrder.setAttribute(GCConstants.SELECT_METHOD, "NO_LOCK");
				eleOrder.setAttribute("BillToID", sNewCustomerID);
				YFCElement eleOrderHoldTypes = eleOrder
						.createChild("OrderHoldTypes");
				YFCElement eleOrderHoldType = eleOrderHoldTypes
						.createChild("OrderHoldType");
				eleOrderHoldType.setAttribute("HoldType",
						GCConstants.ATGNULLBILLTOIDHOLD);
				eleOrderHoldType.setAttribute("ReasonText",
						"Bill To ID not Stamped for ATG Order");
				eleOrderHoldType.setAttribute("Status", "1300");
				Document changeOrder=changeOrderDoc.getDocument();
				GCCommonUtil.invokeAPI(env,
						GCConstants.CHANGE_ORDER,changeOrder);
			}
		} catch (Exception e) {
			LOGGER.error(
					"Inside catch block of GCManageCustomer.createOrUpdateCustomerFromOrder method. Exception is",
					e);
			throw new GCException(e);
		}

	}

	// CR-48 Start
	/**
	 * This method checks whether there exists a customer with the specified
	 * Email ID
	 *
	 * @param env
	 * @param inDoc
	 * @return
	 * @throws GCException
	 */
	public boolean checkCustomerExist(YFSEnvironment env, Document inDoc)
			throws GCException {
		try {
			LOGGER.info("Entering the checkCustomerExist method");
			boolean isEmailExist = false;
			Element eleRootInDoc = inDoc.getDocumentElement();
			String strEmailId = "";
			String sInDocRootElementName = eleRootInDoc.getNodeName();

			// OMS-5287 Fix Start
			String sExtnDAXCustomerID = "";
			if (GCConstants.CUSTOMER.equals(sInDocRootElementName)) {
				strEmailId = GCXMLUtil
						.getAttributeFromXPath(inDoc,
								"Customer/CustomerContactList/CustomerContact/@EmailID");
				sExtnDAXCustomerID = GCXMLUtil.getAttributeFromXPath(inDoc,
						"Customer/Extn/@ExtnDAXCustomerID");
			} else {
				strEmailId = GCXMLUtil.getAttributeFromXPath(inDoc,
						"Order/PersonInfoBillTo/@EMailID");
				sExtnDAXCustomerID = GCXMLUtil.getAttributeFromXPath(inDoc,
						"Order/Extn/@ExtnDAXCustomerID");
			}
			Document getCustomerListOutDoc = GCCommonUtil
					.invokeAPI(
							env,
							GCConstants.GET_CUSTOMER_LIST,
							GCXMLUtil
									.getDocument("<Customer><CustomerContactList><CustomerContact EmailID='"
											+ strEmailId
											+ "' /></CustomerContactList><Extn ExtnDAXCustomerIDQryType='NE' ExtnDAXCustomerID='"
											+ sExtnDAXCustomerID
											+ "'/></Customer>"),
							GCCommonUtil
									.getDocumentFromPath("/global/template/api/getCustomerList_TemplateXML.xml"));
			// OMS-5287 Fix End
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("The getCustomerListOutDoc output document is"
						+ GCXMLUtil.getXMLString(getCustomerListOutDoc));
			}
			if (getCustomerListOutDoc.getDocumentElement().hasChildNodes()) {
				isEmailExist = true;
			}
			LOGGER.debug("The isEmailExist returned vale is" + isEmailExist);
			LOGGER.info("Exiting the checkCustomerExist method");
			return isEmailExist;
		} catch (Exception e) {
			LOGGER.error(
					"Inside catch block of checkCustomerExist method. Exception is",
					e);
			throw new GCException(e);
		}
	}

	/**
	 * This method will be called when the manage customer api is called from
	 * COM
	 *
	 * @param env
	 * @param inDoc
	 * @return
	 * @throws GCException
	 */
	public Document checkCustomerEmailID(YFSEnvironment env, Document inDoc)
			throws GCException {
		try {
			LOGGER.info("Entering the checkCustomerEmailID method");
			String strProgId = env.getProgId();
			String strOperation = inDoc.getDocumentElement().getAttribute(
					"Operation");
			String strEmailId = GCXMLUtil.getAttributeFromXPath(inDoc,
					"Customer/CustomerContactList/CustomerContact/@EmailID");
			String sCustomerIDInput = GCXMLUtil.getAttributeFromXPath(inDoc,
					"Customer/@CustomerID");
			if (GCConstants.PAYMENT_CALL_CALL_CENTER
					.equalsIgnoreCase(strProgId)
					&& GCConstants.CREATE.equalsIgnoreCase(strOperation)
					&& !YFCCommon.isVoid(strEmailId)) {

				Document getCustomerListOutDoc = GCCommonUtil
						.invokeAPI(
								env,
								GCConstants.GET_CUSTOMER_LIST,
								GCXMLUtil
										.getDocument("<Customer><CustomerContactList><CustomerContact EmailID='"
												+ strEmailId
												+ "' /></CustomerContactList></Customer>"),
								GCCommonUtil
										.getDocumentFromPath("/global/template/api/getCustomerList_TemplateXML.xml"));
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("The getCustomerListOutDoc output document is"
							+ GCXMLUtil.getXMLString(getCustomerListOutDoc));
				}
				NodeList nlCustomerContact = XPathAPI
						.selectNodeList(getCustomerListOutDoc,
								"CustomerList/Customer/CustomerContactList/CustomerContact");
				if (nlCustomerContact.getLength() > 1) {
					LOGGER.info("Entering the exception block  if customer with same Email Id exists");
					YFCException yfs = new YFCException(
							"The Email Id already exist in record");
					yfs.setErrorDescription("The Email Id entered already exist in record, use other Email ID ");
					throw yfs;
				}
			} else if (GCConstants.PAYMENT_CALL_CALL_CENTER
					.equalsIgnoreCase(strProgId)
					&& GCConstants.MODIFY.equalsIgnoreCase(strOperation)
					&& !YFCCommon.isVoid(strEmailId)) {
				Document getCustomerListOutDoc = GCCommonUtil
						.invokeAPI(
								env,
								GCConstants.GET_CUSTOMER_LIST,
								GCXMLUtil
										.getDocument("<Customer><CustomerContactList><CustomerContact EmailID='"
												+ strEmailId
												+ "' /></CustomerContactList></Customer>"),
								GCCommonUtil
										.getDocumentFromPath("/global/template/api/getCustomerList_TemplateXML.xml"));
				String sCustomerIDEmail = GCXMLUtil.getAttributeFromXPath(
						getCustomerListOutDoc,
						"CustomerList/Customer/@CustomerID");
				if (!sCustomerIDInput.equalsIgnoreCase(sCustomerIDEmail)) {
					YFCException yfs = new YFCException(
							"The Email Id already exist in record");
					yfs.setErrorDescription("The Email Id entered already exist in record, use other Email ID ");
					throw yfs;
				}
			}
			LOGGER.info("Exiting the checkCustomerEmailID method");
			return inDoc;
		} catch (Exception e) {
			LOGGER.error(
					"Inside catch block of checkCustomerEmailID method. Exception is",
					e);
			throw GCException.getYFCException(e.getMessage(), e);
		}
	}

	// /CR-48 End

	@Override
	public void setProperties(Properties arg0) {
	}

}