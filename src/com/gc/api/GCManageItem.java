/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class is used to create items and apply necessary check and modifications on the items.
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               25/02/2014        Mittal, Yashu		OMS-2010,OMS-2011,OMS-2012 : Item Feed
             2.0               16/12/2014        Mittal, Yashu      GCSTORE-63, GCSTORE-81
             3.0               21/01/2015        Saxena, Ekansh     GCSTORE-166,168,170
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.shared.ycp.YFSContext;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:yashu.mittal@expicient.com">Yashu</a>
 */
public class GCManageItem implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCManageItem.class.getName());

  /**
   * manageItem method is used for following: 1)Stamp image id url's. 2)Stamp
   * short description to description if description is blank. 3)Associate
   * item's to their respective categories.
   *
   * @param env
   * @param inDoc
   * @throws GCException
   */
  public void manageItem(YFSEnvironment env, Document inDoc)
      throws GCException {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" Entering GCManageItem.manageItem() method ");
      LOGGER.debug(" manageItem() method, Input document is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    try {

      // getStaticImage URL Value
      String imgStatURL = "";
      String imgIDSuf = "";

      //Defect - 1862 - Check for IsSolsSeperately flag
      String isSoldSeperately = GCConstants.BLANK;
      //Changes -- END

      Document inDocGetCommonCode = GCXMLUtil
          .getDocument("<CommonCode CodeType='ImageStaticURL'/>");

      Document outDocGetCommonCode = GCCommonUtil.invokeAPI(env,
          GCConstants.GET_COMMON_CODE_LIST, inDocGetCommonCode);

      Element getCommonCodeList = outDocGetCommonCode
          .getDocumentElement();
      Element commonCodeEle = (Element) getCommonCodeList
          .getElementsByTagName("CommonCode").item(0);
      imgStatURL = commonCodeEle.getAttribute("CodeValue");
      imgIDSuf = commonCodeEle.getAttribute("CodeShortDescription");

      // create modifyCategoryItem I/P doc
      Document modifyCategoryItemInput = GCXMLUtil
          .createDocument("ModifyCategoryItems");
      Element elemodifyCategoryItemRootElement = modifyCategoryItemInput
          .getDocumentElement();
      Element eleCategory = modifyCategoryItemInput
          .createElement("Category");
      elemodifyCategoryItemRootElement.appendChild(eleCategory);
      eleCategory.setAttribute(GCConstants.CATEGORY_PATH,
          GCConstants.WEB_ITEMS_CATEGORY_PATH);
      eleCategory.setAttribute(GCConstants.ORGANIZATION_CODE,
          GCConstants.GUITAR_CENTER_INC);
      Element eleCategoryItemList = modifyCategoryItemInput
          .createElement("CategoryItemList");
      eleCategory.appendChild(eleCategoryItemList);

      Set<String> sWebOnlyItems = new HashSet<String>();

      // Requirement GCSTORE-81 Starts - Include Recommended Accessories with Heiler Item Updates
      // Create MultiAPI input

      Document multiAPIIn = GCXMLUtil.createDocument(GCXmlLiterals.MULTI_API);

      // Requirement GCSTORE-81 Ends

      // Fetch item list
      List<Element> listItem = GCXMLUtil.getElementListByXpath(inDoc,
          GCConstants.ITEM_LIST_ITEM_XPATH);
      for (Element eleItem : listItem) {
        // 5702
        Element eleExtnItem = (Element) eleItem.getElementsByTagName(GCConstants.EXTN).item(0);
        if (!YFCCommon.isVoid(eleExtnItem)) {
          String statusCode = eleExtnItem.getAttribute(GCXmlLiterals.EXTN_POS_STATUS_CODE);
          if (YFCUtils.equals(statusCode, "F") || YFCUtils.equals(statusCode, "I") || YFCUtils.equals(statusCode, "S")
              || YFCUtils.equals(statusCode, "Z")) {
            updateInventoryToZero(eleItem, env);
          }
        }
        String sItemId = eleItem.getAttribute(GCConstants.ITEM_ID);
        String sOrganizationCode = eleItem.getAttribute(GCConstants.ORGANIZATION_CODE);

        LOGGER.debug("sItemID " + sItemId);

        // GCSTORE-2775 start
        LOGGER.debug("Going to process Bundle item for Platinum flag");
        processBundlePlantinumItem(env, eleItem);
        // GCSTORE-2275 end

        LOGGER.debug("after processing Bundle item for Platinum flag");
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("Item xml after processing bundle item for platinum flag="
              + GCXMLUtil.getElementXMLString(eleItem));
        }
        // Requirement GCSTORE-63 Starts - Add Safety Stock Thresholds to Retail Only and Matched
        // Catalog Items
        Element eleInventoryParameters = (Element) eleItem.getElementsByTagName(GCXmlLiterals.INVENTORY_PARAMETERS).item(0);
        checkIfClearanceItem(env, sItemId, eleInventoryParameters, sOrganizationCode);

        // Requirement GCSTORE-63 End

        // Requirement GCSTORE-81 Starts - Include Recommended Accessories with Heiler Item Updates
        Element eleAssociationList = (Element) eleItem.getElementsByTagName(GCXmlLiterals.ASSOCIATION_LIST).item(0);
        if (!YFCCommon.isVoid(eleAssociationList)) {
          createMultiAPIModifyItemAssociationsIn(multiAPIIn, eleItem, eleAssociationList);
        }

        // Requirement GCSTORE-81 Ends

        // Add short description to description field if description is
        // empty

        Element elePrimaryInformation = (Element) XPathAPI
            .selectSingleNode(eleItem, "//Item/PrimaryInformation");
        Element eleExtn = (Element) eleItem.getElementsByTagName(
                GCConstants.EXTN).item(0);

        if (!YFCCommon.isVoid(elePrimaryInformation)) {

          //Defect - 1862 - Check for IsSoldSeperately flag
          if(YFCUtils.equals(GCConstants.NO, elePrimaryInformation.getAttribute(GCConstants.IS_SOLD_SEPERATELY))){
            isSoldSeperately = elePrimaryInformation.getAttribute(GCConstants.IS_SOLD_SEPERATELY);
          }
          //Changes -- END

          String sShortDescription = elePrimaryInformation
              .getAttribute(GCConstants.SHORT_DESCRIPTION);

          // Copying the ShortDescription attribute value to the
          // Description value, only when the short description
          // attribute is present in the input
          if (((!elePrimaryInformation
              .hasAttribute(GCConstants.DESCRIPTION)) || YFCCommon
              .isVoid(elePrimaryInformation
                  .getAttribute(GCConstants.DESCRIPTION)))
                  && (elePrimaryInformation
                      .hasAttribute(GCConstants.SHORT_DESCRIPTION))) {
            elePrimaryInformation.setAttribute(
                GCConstants.DESCRIPTION, sShortDescription);
          }

          if (!YFCCommon.isVoid(sItemId)
              && !YFCCommon.isVoid(imgIDSuf)
              && !YFCCommon.isVoid(imgStatURL)) {
				String newItemId = "";
				if (YFCCommon.equals(eleExtn
						.getAttribute(GCConstants.EXTN_VINTAGE_ITEM),
						"Y")
						|| YFCCommon
								.equals(sItemId,
										eleExtn.getAttribute(GCConstants.EXTN_POSITEM_ID))) {
					newItemId = "000000" + sItemId;
				} else {
					newItemId = sItemId;
				}
				elePrimaryInformation.setAttribute("ImageID", newItemId
						+ imgIDSuf);
            elePrimaryInformation.setAttribute("ImageLocation",
                imgStatURL);
          }
        }

        if (!YFCCommon.isVoid(eleExtn)) {
          String sExtnCategory = eleExtn
              .getAttribute(GCConstants.EXTN_CATEGORY);
          String sExtnWebCategory = eleExtn
              .getAttribute(GCConstants.EXTN_WEB_CATEGORY);
          String sExtnDCTCategory = eleExtn
              .getAttribute(GCConstants.EXTN_DCT_CATEGORY);

          // calling method addItemToWebCategory to add Web only items
          // to GCWEB category.
          if ("GCWEB".equalsIgnoreCase(sExtnCategory)) {
            addItemToWebCategory(env, sExtnCategory, sItemId,
                sWebOnlyItems);
          }

          // calling method to add items to their specific catalog
          // category
          if (!YFCCommon.isVoid(sExtnWebCategory)) {
            manageItemCategory(env, sExtnWebCategory,
                GCConstants.BLANK, sItemId,
                modifyCategoryItemInput);
          }
          if (!YFCCommon.isVoid(sExtnDCTCategory)) {
            manageItemCategory(env, GCConstants.BLANK,
                sExtnDCTCategory, sItemId,
                modifyCategoryItemInput);
          }
          //GCSTORE-5348::Begin
		  //GCSTORE-5785::Begin
		   if(eleExtn.hasAttribute(GCConstants.EXTN_CATEGORY)){
		    //GCSTORE-5785::End
			if ("".equalsIgnoreCase(sExtnCategory)) {
              removeItemFromCategories(env, modifyCategoryItemInput, sItemId);
            }
			}
          
          //GCSTORE-5348::End
          // Requirement GCSTORE- 166, GCSTORE-168, GCSTORE-170 starts

          // Check for Clearance item and call manageClearanceItem
          String sExtnIsClearanceItem = eleExtn.getAttribute(GCXmlLiterals.EXTN_IS_CLEARANCE_ITEM);
          if (YFCCommon.equals(sExtnIsClearanceItem, GCConstants.YES, GCConstants.FALSE)) {
            manageClearanceItem(env, sItemId, inDoc, eleExtn);
          }

        }


      }
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("***input xml****" + GCXMLUtil.getXMLString(inDoc));
      }


      // call manageItem API
      GCCommonUtil.invokeAPI(env, GCConstants.API_MANAGE_ITEM, inDoc);


      // call createInventoryActivity method to raise inventory activity update

      List<Element> eleListItem = GCXMLUtil.getElementListByXpath(inDoc, GCConstants.ITEM_LIST_ITEM_XPATH);
      for (Element eleItem : eleListItem) {
        String sItemId = eleItem.getAttribute(GCConstants.ITEM_ID);
        Element eleExtn = (Element) eleItem.getElementsByTagName(GCConstants.EXTN).item(0);
        String sExtnIsClearanceItem = null;
        if(!YFCCommon.isVoid(eleExtn)){
          sExtnIsClearanceItem = eleExtn.getAttribute(GCXmlLiterals.EXTN_IS_CLEARANCE_ITEM);
        }
        Element elePrimaryInfo = (Element) eleItem.getElementsByTagName(GCConstants.PRIMARY_INFORMATION).item(0);
        if(!YFCCommon.isVoid(elePrimaryInfo)){
          String sKitCode = elePrimaryInfo.getAttribute(GCConstants.KIT_CODE);
          if (YFCCommon.equals(sExtnIsClearanceItem, GCConstants.YES, GCConstants.FALSE)
              && !YFCCommon.equals(sKitCode, GCConstants.BUNDLE, GCConstants.FALSE)) {
            YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
            createInventoryActivity(env, sItemId, inputDoc);
          }

        }
      }

      // Requirement GCSTORE- 166, GCSTORE-168, GCSTORE-170 ends

      // Requirement GCSTORE-81 Starts - Include Recommended Accessories with Heiler Item Updates

      // call multiAPI to add all associations.
      if (multiAPIIn.getDocumentElement().hasChildNodes()) {
        GCCommonUtil.invokeAPI(env, GCConstants.API_MULTIAPI, multiAPIIn);
      }

      // Requirement GCSTORE-81 Ends

     
	  
      for (String sItem : sWebOnlyItems) {
        Element eleCategoryItem = modifyCategoryItemInput
            .createElement("CategoryItem");
        eleCategoryItem.setAttribute(GCConstants.ITEM_ID, sItem);
        eleCategoryItem.setAttribute(GCConstants.ORGANIZATION_CODE,
            GCConstants.GUITAR_CENTER_INC);
        eleCategoryItem.setAttribute(GCConstants.UNIT_OF_MEASURE,
            GCConstants.EACH);
        eleCategoryItemList.appendChild(eleCategoryItem);
      }

      // calling modifyCategoryItem API
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(GCXMLUtil.getXMLString(modifyCategoryItemInput));
      }
      
   
      
      //Defect - 1862 - check for isSoldSeperately flag
      if (!YFCUtils.equals(GCConstants.NO, isSoldSeperately) && modifyCategoryItemInput.hasChildNodes()) { 
          LOGGER.debug("GCSTORE-6580 start");
          
        	  NodeList nlCategoryItems=eleCategoryItemList.getElementsByTagName(GCConstants.CATEGORY_ITEM);
        	  int count= nlCategoryItems.getLength();
        	  for(int k=0;k<count;k++)
        	  {
        		  Element categoryItemEle=(Element)nlCategoryItems.item(k);
        		  categoryItemEle.setAttribute(GCConstants.IGNORE_IS_SOLD_SEPERATELY, GCConstants.YES);
        	  }
          
          if (LOGGER.isDebugEnabled()) {
              LOGGER.debug("GCSTORE-6580 end" +GCXMLUtil.getXMLString(modifyCategoryItemInput));
            }
          
        GCCommonUtil.invokeAPI(env,
            GCConstants.API_MODIFY_CATEGORY_ITEM,
            modifyCategoryItemInput);
      }
      //Changes -- End



      // calling method adjustDropShipItemInventory to adjust inventory of
      // all Dropship and Digital Download items.

      //ColdFusion GCStory-1875 Start:
			//ItemList/Item/Extn/@ExtnIsStoreUsedOrVintageItem and if it is Y

			YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
			YFCElement eleRoot = inputDoc.getDocumentElement();

			if (LOGGER.isVerboseEnabled()){
				LOGGER.verbose("Input Document of ColdFusion :" + inputDoc.getString());
			}

			YFCNodeList<YFCElement> itemList=eleRoot.getElementsByTagName(GCConstants.ITEM);

			for(YFCElement eleItem : itemList){
				YFCElement elePrimaryInfo=eleItem.getChildElement(GCConstants.PRIMARY_INFORMATION);
				YFCElement eleExtn=eleItem.getChildElement(GCConstants.EXTN);
				String sExtnIsUsedOrVintageItem=eleExtn.getAttribute("ExtnIsUsedOrVintageItem");
				LOGGER.verbose("ExtnIsUsedOrVintageItem :" + sExtnIsUsedOrVintageItem);				
				if(YFCCommon.equals(sExtnIsUsedOrVintageItem, GCConstants.FLAG_Y)){
					String sItemID=eleItem.getAttribute(GCConstants.ITEM_ID);
					String sProductClass=elePrimaryInfo.getAttribute("DefaultProductClass");
					String sUOM=eleItem.getAttribute(GCConstants.UNIT_OF_MEASURE);
					String sOrganizationCode=eleItem.getAttribute(GCConstants.ORGANIZATION_CODE);


					YFCDocument getSupplyDetailsinDoc=YFCDocument.createDocument(GCConstants.GET_SUPPLY_DETAILS);
					YFCElement eleGetSupplyDetails=getSupplyDetailsinDoc.getDocumentElement();
					eleGetSupplyDetails.setAttribute(GCConstants.ITEM_ID, sItemID);
					eleGetSupplyDetails.setAttribute(GCConstants.ORGANIZATION_CODE, sOrganizationCode);
					eleGetSupplyDetails.setAttribute(GCConstants.PRODUCT_CLASS,sProductClass);
					eleGetSupplyDetails.setAttribute(GCConstants.UNIT_OF_MEASURE,sUOM);


					if (LOGGER.isVerboseEnabled()){
						LOGGER.verbose("Get Supply Details InDoc :" + getSupplyDetailsinDoc.getString());
					}
					YFCDocument getSupplyDetailsTemplate=YFCDocument.getDocumentFor(GCConstants.GET_SUPPLY_DETAILS_TEMPLATE);
					YFCDocument outDoc= GCCommonUtil.invokeAPI(env, GCConstants.GET_SUPPLY_DETAILS, getSupplyDetailsinDoc,getSupplyDetailsTemplate);

					if (LOGGER.isVerboseEnabled()){
						LOGGER.verbose("Get Supply Details OutDoc :" + outDoc.getString());
					}

					YFCElement eleOutDocRoot = outDoc.getDocumentElement();

					YFCElement eleShipNodes=eleOutDocRoot.getChildElement(GCConstants.SHIP_NODES);
					YFCNodeList<YFCElement> listShipNode=eleShipNodes.getElementsByTagName(GCConstants.SHIP_NODE);
					if(listShipNode.getLength()>0){
						for(YFCElement eleShipNode:listShipNode){
							String sShipNode=eleShipNode.getAttribute(GCConstants.SHIP_NODE);
							YFCDocument createInventoryActivityInDoc=YFCDocument.createDocument("InventoryActivity");
							YFCElement eleInventoryActivity=createInventoryActivityInDoc.getDocumentElement();
							eleInventoryActivity.setAttribute(GCConstants.ITEM_ID, sItemID);
							eleInventoryActivity.setAttribute(GCConstants.ORGANIZATION_CODE, sOrganizationCode);
							eleInventoryActivity.setAttribute(GCConstants.NODE, sShipNode);
							eleInventoryActivity.setAttribute(GCConstants.PRODUCT_CLASS, sProductClass);
							eleInventoryActivity.setAttribute(GCConstants.UNIT_OF_MEASURE, sUOM);

							if (LOGGER.isVerboseEnabled()){
								LOGGER.verbose("CreateInventoryActivityInDoc :" + createInventoryActivityInDoc.getString());
							}
							GCCommonUtil.invokeAPI(env, GCConstants.CREATE_INVENTORY_ACTIVITY, createInventoryActivityInDoc.getDocument());
						}
					}

				}
			}

        //Cold Fusion Ends Here


    } catch (Exception e) {
      LOGGER.error(
          "Inside catch block of GCManageItem.manageItem method. Exception is",
          e);
      throw new GCException(e);
    }
  }

  private void updateInventoryToZero(Element eleItem, YFSEnvironment env) throws TransformerException {
    YFCDocument docAdjustInv = YFCDocument.createDocument("Items");
    YFCElement eleItems = docAdjustInv.getDocumentElement();
    String itemID = eleItem.getAttribute(GCConstants.ITEM_ID);
    Element elePrimaryInfo = (Element) eleItem.getElementsByTagName(GCConstants.PRIMARY_INFORMATION).item(0);
    String sProdClass = elePrimaryInfo.getAttribute("DefaultProductClass");

    String kitCode = elePrimaryInfo.getAttribute(GCConstants.KIT_CODE);
    if (!YFCUtils.equals(GCConstants.BUNDLE, kitCode)) {
      prepareAdjustInvInput(eleItem, eleItems, sProdClass, itemID, env);
    }
    YFCNodeList<YFCElement> nlItem = eleItems.getElementsByTagName(GCConstants.ITEM);
    if (nlItem.getLength() > 0) {
      GCCommonUtil.invokeService(env, "GCPostToAdjustInventoryQ", docAdjustInv.getDocument());
    }
  }



  private void prepareAdjustInvInput(Element eleItem, YFCElement eleItems, String sProdClass, String itemID,
      YFSEnvironment env) throws TransformerException {
    String sOrganizationCode = eleItem.getAttribute(GCConstants.ORGANIZATION_CODE);
    String sUOM = eleItem.getAttribute(GCConstants.UNIT_OF_MEASURE);
    YFCDocument getSupplyDetailsinDoc = YFCDocument.createDocument(GCConstants.GET_SUPPLY_DETAILS);
    YFCElement eleGetSupplyDetails = getSupplyDetailsinDoc.getDocumentElement();
    eleGetSupplyDetails.setAttribute(GCConstants.ORGANIZATION_CODE, sOrganizationCode);
    eleGetSupplyDetails.setAttribute(GCConstants.PRODUCT_CLASS, sProdClass);
    eleGetSupplyDetails.setAttribute(GCConstants.UNIT_OF_MEASURE, sUOM);
    eleGetSupplyDetails.setAttribute(GCConstants.ITEM_ID, itemID);


    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Get Supply Details InDoc :" + getSupplyDetailsinDoc.getString());
    }
    YFCDocument getSupplyDetailsTemplate = YFCDocument.getDocumentFor(GCConstants.GET_SUPPLY_DETAILS_TEMPLATE);
    YFCDocument outDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_SUPPLY_DETAILS, getSupplyDetailsinDoc, getSupplyDetailsTemplate);
    Document getSupplyDoc = outDoc.getDocument();

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Get Supply Details OutDoc :" + outDoc.getString());
    }


    List<Element> listShipNode = GCXMLUtil.getElementListByXpath(getSupplyDoc, "Item/ShipNodes/ShipNode");
    for (int j = 0; j < listShipNode.size(); j++) {
      Element eleShipNode = listShipNode.get(j);
      String shipNode = eleShipNode.getAttribute(GCConstants.SHIP_NODE);
      String tracked = eleShipNode.getAttribute("Tracked");
	  if (YFCUtils.equals(GCConstants.YES, tracked)) {
      Element eleSupplyNonPick =
          (Element) XPathAPI.selectSingleNode(eleShipNode, "Supplies/Supply[@SupplyType='NON_PICKABLE.ex']");
      if (!YFCCommon.isVoid(eleSupplyNonPick)) {
        String nonPickableQty = eleSupplyNonPick.getAttribute("AvailableQty");
        int nonPickable = Integer.parseInt(nonPickableQty);
        if (nonPickable > 0 && !YFCUtils.equals(shipNode, GCConstants.MFI)) {
          YFCElement eleItemIp = eleItems.createChild(GCConstants.ITEM);
          eleItemIp.setAttribute(GCConstants.ADJUSTMENT_TYPE, GCConstants.ADJUSTMENT);
          eleItemIp.setAttribute(GCConstants.SHIP_NODE, shipNode);
          eleItemIp.setAttribute(GCConstants.SUPPLY_TYPE, "NON_PICKABLE.ex");
          eleItemIp.setAttribute(GCConstants.UNIT_OF_MEASURE, GCConstants.EACH);
          eleItemIp.setAttribute(GCConstants.ITEM_ID, itemID);
          eleItemIp.setAttribute(GCConstants.QUANTITY, "-" + nonPickable);
          eleItemIp.setAttribute(GCConstants.PRODUCT_CLASS, sProdClass);
        }
      }
      Element eleSupplyOnHand =
          (Element) XPathAPI.selectSingleNode(eleShipNode, "Supplies/Supply[@SupplyType='ONHAND']");
      if (!YFCCommon.isVoid(eleSupplyOnHand)) {
        String onHandQty = eleSupplyOnHand.getAttribute("AvailableQty");
        int onHand = Integer.parseInt(onHandQty);
        if (onHand > 0 && !YFCUtils.equals(shipNode, GCConstants.MFI)) {
          YFCElement eleItemIp = eleItems.createChild(GCConstants.ITEM);
          eleItemIp.setAttribute(GCConstants.ADJUSTMENT_TYPE, GCConstants.ADJUSTMENT);
          eleItemIp.setAttribute(GCConstants.SHIP_NODE, shipNode);
          eleItemIp.setAttribute(GCConstants.SUPPLY_TYPE, GCConstants.ONHAND);
          eleItemIp.setAttribute(GCConstants.UNIT_OF_MEASURE, GCConstants.EACH);
          eleItemIp.setAttribute(GCConstants.ITEM_ID, itemID);
          eleItemIp.setAttribute(GCConstants.QUANTITY, "-" + onHand);
          eleItemIp.setAttribute(GCConstants.PRODUCT_CLASS, sProdClass);
        }
      }
	 }
    }
  }



  /**
   * GCSTORE-5348:This method removes the item from web category.
   * Description of removeItemFromoWebCategory
   *
   * @param modifyCategoryItemInput
   * @param sItemId 
   *
   */
  private void removeItemFromCategories(YFSEnvironment env , Document modifyCategoryItemInput,
        String sItemId) {
      
	  //remove item from webcategory
      String sCategoryPath = GCConstants.WEB_ITEMS_CATEGORY_PATH;
	  removeItemFromCategory(modifyCategoryItemInput, sItemId, sCategoryPath);
      //call getitemlist
    Document getItemListInput = GCXMLUtil
        .getDocument("<Item ItemID='"
            + sItemId
            + "' GetUnpublishedItems='Y' IgnoreIsSoldSeparately='Y' OrganizationCode='"
            + GCConstants.GUITAR_CENTER_INC + "'/>");

    Document getItemListOutDoc = GCCommonUtil
        .invokeAPI(
            env,
            GCConstants.API_GET_ITEM_LIST,
            getItemListInput,
            GCXMLUtil
            .getDocument("<ItemList><Item ItemID=''><CategoryList><Category CategoryPath='' /></CategoryList></Item></ItemList>"));

    if ((getItemListOutDoc.getDocumentElement().hasChildNodes())) {
      //get all the categories to which item is associated
      NodeList categoryList = getItemListOutDoc.getElementsByTagName("Category");
      for(int i=0; i< categoryList.getLength(); i++){
        Element categoryEle = (Element)categoryList.item(i);
        String categoryPath = categoryEle.getAttribute("CategoryPath");
        //remove item from each category
        removeItemFromCategory(modifyCategoryItemInput, sItemId, categoryPath);
      }
    }
  }

/**
   * This method is implemented for improvement ticket GCSTORE-2775 if the item in feed is Bundle,
   * then based upon its component's ExtnSerialComponent and other attribute related to platinum/
   * unique is stamped by this method
   *
   * @param env
   * @param eleItem
 * @throws SQLException 
 * @throws GCException 
   */
  private void processBundlePlantinumItem(YFSEnvironment env, Element eleItem) throws GCException, SQLException {
    LOGGER.beginTimer("GCManageItem.processBundlePlantinumItem");
    NodeList primaryInfoNL = eleItem.getElementsByTagName(GCXmlLiterals.PRIMARY_INFORMATION);
    if (primaryInfoNL.getLength() <= 0) {
      LOGGER.debug("Did not find any PrimaryInformation element for item. So returning to caller method");
      return;
    }
    Element primaryInfoEle = (Element) primaryInfoNL.item(0);
    String kitCode = primaryInfoEle.getAttribute(GCXmlLiterals.KIT_CODE);
    if (!YFCCommon.equals(kitCode, GCConstants.BUNDLE, false)) {
      LOGGER.debug("This is not a Bundle Item. So returning to caller method");
      return;
    }

    NodeList componentsNL = eleItem.getElementsByTagName(GCXmlLiterals.COMPONENTS);
    if (componentsNL.getLength() <= 0) {
      LOGGER.debug("No components are found for the bundle. So returning to caller method");
      return;
    }

    LOGGER.debug("Going to look into components for Bundle item");
    NodeList componentNL = ((Element) componentsNL.item(0)).getElementsByTagName(GCXmlLiterals.COMPONENT);

    YFCDocument inputDoc =
            YFCDocument.getDocumentFor(GCXMLUtil.getDocument(GCConstants.GET_ITEM_LIST_INPUT_FOR_PLATINUM_BUNDLE));
        YFCElement rootEle = inputDoc.getDocumentElement();
        YFCElement complexQryEle = rootEle.getChildElement("ComplexQuery");
        YFCElement andEle = complexQryEle.getChildElement("And");
        YFCElement orEle = andEle.getChildElement("Or");
        for (int i = 0; i < componentNL.getLength(); i++) {
          String componentItemID = ((Element) componentNL.item(i)).getAttribute(GCConstants.COMPONENT_ITEM_ID);
          LOGGER.debug("Found the component item id = " + componentItemID);
          YFCElement expEle = orEle.createChild("Exp");
          expEle.setAttribute("Name", "ItemID");
          expEle.setAttribute("QryType", "EQ");
          expEle.setAttribute("Value", componentItemID);
        }
        YFCDocument templDoc =
            YFCDocument.getDocumentFor(GCXMLUtil.getDocument(GCConstants.GET_ITEM_LIST_TEMPLATE_FOR_PLATINUM_BUNDLE));
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("Going to call getItemList API with input\n" + inputDoc + "\n and output\n  " + templDoc);
        }
        YFCDocument outDocument = GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ITEM_LIST, inputDoc, templDoc);
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("Output document from getItemList=\n" + outDocument);
        }
        YFCElement itemListOutEle = outDocument.getDocumentElement();
        YFCElement itemOutEle = itemListOutEle.getChildElement(GCConstants.ITEM);
        if (YFCCommon.isVoid(itemOutEle)) {
          LOGGER.debug("No Item found. So returning");
          return;
        }

    NodeList inventoryParametersNL = eleItem.getElementsByTagName(GCXmlLiterals.INVENTORY_PARAMETERS);
    Element inventoryParameterEle = null;
    if (inventoryParametersNL.getLength() <= 0) {
      LOGGER.debug("inventoryParameterEle is not found in item feed. So going to create it");
      inventoryParameterEle = eleItem.getOwnerDocument().createElement(GCXmlLiterals.INVENTORY_PARAMETERS);
      eleItem.appendChild(inventoryParameterEle);
    } else {
      LOGGER.debug("inventoryParameterEle is found in item feed. So going to use it");
      inventoryParameterEle = (Element) inventoryParametersNL.item(0);
    }

    NodeList extnItemNL = eleItem.getElementsByTagName(GCXmlLiterals.EXTN);
    Element extnItemEle = null;
    if (extnItemNL.getLength() <= 0) {
      LOGGER.debug("extnItemEle is not found in item feed. So going to create it");
      extnItemEle = eleItem.getOwnerDocument().createElement(GCXmlLiterals.EXTN);
      eleItem.appendChild(extnItemEle);
    } else {
      LOGGER.debug("extnItemEle is found in item feed. So going to use it");
      extnItemEle = (Element) extnItemNL.item(0);
    }
    YFCElement extnItemOutEle = itemOutEle.getChildElement(GCConstants.EXTN);
    inventoryParameterEle.setAttribute("TagControlFlag", GCConstants.SOMETIMES_TAG_CONTROLLED);
    extnItemEle.setAttribute(GCConstants.EXTN_SERIAL_COMPONENT, itemOutEle.getAttribute(GCConstants.ITEM_ID));
    extnItemEle.setAttribute(GCConstants.EXTN_IS_UNIQUE_ITEM,
        extnItemOutEle.getAttribute(GCConstants.EXTN_IS_UNIQUE_ITEM));
    extnItemEle.setAttribute("ExtnIsPlatinumItem", extnItemOutEle.getAttribute("ExtnIsPlatinumItem"));

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Returning Item element after processing for Platinum Bundle item as \n"
          + GCXMLUtil.getElementXMLString(eleItem));
    }
    LOGGER.endTimer("GCManageItem.processBundlePlantinumItem");


  }

  /**
   * This methods take component's item id in input and get the fetches the related serial
   * information to stamp on bundle parent
   *
   * @param env
   * @param itemIDList
   * @return
   */
  private Map<String, String> getSerialComponentDetails(YFSEnvironment env, List<String> itemIDList) {
    LOGGER.beginTimer("GCManageItem.getSerialComponentDetails");
    Map<String, String> itemAttrMap = new HashMap<String, String>();
    YFCDocument inputDoc =
        YFCDocument.getDocumentFor(GCXMLUtil.getDocument(GCConstants.GET_ITEM_LIST_INPUT_FOR_PLATINUM_BUNDLE));
    YFCElement rootEle = inputDoc.getDocumentElement();
    YFCElement complexQryEle = rootEle.getChildElement("ComplexQuery");
    YFCElement andEle = complexQryEle.getChildElement("And");
    YFCElement orEle = andEle.getChildElement("Or");
    for (int i = 0; i < itemIDList.size(); i++) {
      YFCElement expEle = orEle.createChild("Exp");
      expEle.setAttribute("Name", "ItemID");
      expEle.setAttribute("QryType", "EQ");
      expEle.setAttribute("Value", itemIDList.get(i));
    }

    YFCDocument templateDoc =
        YFCDocument.getDocumentFor(GCXMLUtil.getDocument(GCConstants.GET_ITEM_LIST_TEMPLATE_FOR_PLATINUM_BUNDLE));
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Going to call getItemList with input=\n" + inputDoc + "\n and template\n" + templateDoc);
    }
    YFCDocument getItemListOut = GCCommonUtil.invokeAPI(env, GCConstants.GET_ITEM_LIST, inputDoc, templateDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Output of getItemList is" + getItemListOut);
    }
    YFCElement itemListEle = getItemListOut.getDocumentElement();
    YFCElement itemEle = itemListEle.getChildElement(GCConstants.ITEM);
    if (!YFCCommon.isVoid(itemEle)) {
      YFCElement extnItemEle = itemEle.getChildElement(GCConstants.EXTN);
      if (!YFCCommon.isVoid(extnItemEle)) {
        String sItemID = itemEle.getAttribute(GCConstants.ITEM_ID);
        String sIsPlatinumFlag = extnItemEle.getAttribute(GCXmlLiterals.EXTN_IS_PLATINUM_ITEM);
        String sIsUniqueFlag = extnItemEle.getAttribute(GCConstants.EXTN_IS_UNIQUE_ITEM);
        itemAttrMap.put(GCConstants.EXTN_SERIAL_COMPONENT, sItemID);
        itemAttrMap.put(GCXmlLiterals.EXTN_IS_PLATINUM_ITEM, sIsPlatinumFlag);
        itemAttrMap.put(GCConstants.EXTN_IS_UNIQUE_ITEM, sIsUniqueFlag);
      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Returning itemAttrMap as " + itemAttrMap);
    }
    LOGGER.endTimer("GCManageItem.getSerialComponentDetails");
    return itemAttrMap;
  }

  /**
   * This method get the item details for provided list of ItemID
   * @param env
   * @param sPOSItemID
   * @return
   * @throws GCException
   * @throws SQLException
   */

  private Map<String, String> getItemListUsingQuery(YFSEnvironment env, List<String> itemIDList) throws GCException, SQLException {
    LOGGER.debug(" Entering getItemListUsingQuery method ");
    Map<String, String> itemAttrMap = new HashMap<String, String>();
    if(itemIDList.size()<1) {
      return itemAttrMap;
    }
    Statement objStatement = null;
    ResultSet objResultSet = null;
    Connection con = null;
    String itemListQuery = "Select ITEM_ID, EXTN_IS_PLATINUM_ITEM, EXTN_IS_UNIQUE_ITEM from yfs_item where TAG_CONTROL_FLAG='S' AND ( ITEM_ID='"+itemIDList.get(0)+"' ";
    for(int i=1;i<itemIDList.size();i++){
      itemListQuery = itemListQuery + " OR ITEM_ID='"+itemIDList.get(i)+"' ";
    }

    itemListQuery = itemListQuery +" ) ";
    LOGGER.debug(" Going to fetch items with DB query: "+itemListQuery);
    try {
      // setting up the connection with database
      con = ((YFSContext) env)
          .getConnectionForTableType(GCConstants.GUITAR);
      objStatement = getPrepareStatement(con);
      objResultSet = objStatement
          .executeQuery(itemListQuery);
      while (objResultSet.next()) {
        LOGGER.debug(" Inside while(objResultSet.next()) ");
        String sItemID = objResultSet.getString("ITEM_ID");
        String sIsPlatinumFlag = objResultSet.getString("EXTN_IS_PLATINUM_ITEM");
        String sIsUniqueFlag = objResultSet.getString("EXTN_IS_UNIQUE_ITEM");

        if(!YFCCommon.isVoid(sItemID)){
          sItemID=sItemID.trim();
        }
        if(!YFCCommon.isVoid(sItemID)){
          sIsPlatinumFlag=sIsPlatinumFlag.trim();
        }
        if(!YFCCommon.isVoid(sItemID)){
          sIsUniqueFlag=sIsUniqueFlag.trim();
        }
        itemAttrMap.put(GCConstants.EXTN_SERIAL_COMPONENT, sItemID);
        itemAttrMap.put(GCXmlLiterals.EXTN_IS_PLATINUM_ITEM, sIsPlatinumFlag);
        itemAttrMap.put(GCConstants.EXTN_IS_UNIQUE_ITEM, sIsUniqueFlag);
      }

    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of getItemListUsingQuery, Exception is :",
          e);
      throw new GCException(e);
    } finally {
      if(objResultSet!=null){
        objResultSet.close();
      }
      if(objStatement!=null){
        objStatement.close();
      }
    }

    LOGGER.debug(" Exiting getItemListUsingQuery method ");
    return itemAttrMap;
  }


  private Statement getPrepareStatement(Connection con) throws SQLException {
    Statement objStatement;
    objStatement = con.createStatement();
    return objStatement;
  }

  /**
   * Method to check Extncategory of items, and add the item to seller entitlement if item category
   * is GCWEB
   *
   * @param env
   * @param eleItem
   * @param sItemID
   * @param sWebOnlyItems
   * @throws GCException
   */

  public void addItemToWebCategory(YFSEnvironment env, String sExtnCategory,
      String sItemId, Set<String> sWebOnlyItem) throws GCException {

    LOGGER.debug(" Entering GCManageItem.addItemToWebCategory() method ");

    try {
      // creating getItemList i/p doc by category filter
      Document getItemListInput = GCXMLUtil
          .getDocument("<Item ItemID='"
              + sItemId
              + "' GetUnpublishedItems='Y' IgnoreIsSoldSeparately='Y' ><CategoryFilter CategoryPath='"
              + GCConstants.WEB_ITEMS_CATEGORY_PATH
              + "' OrganizationCode='"
              + GCConstants.GUITAR_CENTER_INC + "'/></Item>");

      Document getItemListOutDoc = GCCommonUtil
          .invokeAPI(
              env,
              GCConstants.API_GET_ITEM_LIST,
              getItemListInput,
              GCXMLUtil
              .getDocument("<ItemList><Item ItemID=''/></ItemList>"));
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("getItemList Output is : "
            + GCXMLUtil.getXMLString(getItemListOutDoc));
      }
      if (!(getItemListOutDoc.getDocumentElement().hasChildNodes())) {
        LOGGER.debug("Item category is GCWEB, so add it to GCWEB Category");
        sWebOnlyItem.add(sItemId);
      }

    } catch (Exception e) {
      LOGGER.error(
          "Inside catch block of GCManageItem.checkItemCategory method. Exception is",
          e);
      throw new GCException(e);
    }
  }


  /**
   * Method to check ExtnWebCategory and ExtnDCTCategory of items, and add the
   * items to their corresponding category.
   *
   * @param env
   * @param eleItem
   * @param sItemID
   * @param modifyCategoryItemInput
   * @throws GCException
   */

  public void manageItemCategory(YFSEnvironment env, String sExtnWebCategory,
      String sExtnDCTCategory, String sItemId,
      Document modifyCategoryItemInput) throws GCException {

    LOGGER.debug(" Entering GCManageItem.manageItemCategory() method ");

    try {
      String sCategoryPath = null;

      Document getCategoryListIn = GCXMLUtil
          .createDocument(GCConstants.CATEGORY);
      // Check if category is sExtnWebCategory or sExtnDCTCategory.

      if (!YFCCommon.isVoid(sExtnWebCategory)) {
        getCategoryListIn.getDocumentElement().setAttribute(
            GCConstants.CATEGORY_ID, sExtnWebCategory);
      } else {
        getCategoryListIn.getDocumentElement().setAttribute(
            GCConstants.CATEGORY_ID, sExtnDCTCategory);
      }

      Document getCategoryListOut = GCCommonUtil
          .invokeAPI(
              env,
              GCConstants.API_GET_CATEGORY_LIST,
              getCategoryListIn,
              GCXMLUtil
              .getDocument("<CategoryList><Category CategoryPath=''/></CategoryList>"));

      List<Element> eleCategoryList = GCXMLUtil.getElementListByXpath(
          getCategoryListOut, "CategoryList/Category");

      for (Element eleCategory : eleCategoryList) {

        String sCurrentCategoryPath = eleCategory
            .getAttribute(GCConstants.CATEGORY_PATH);

        if (!YFCCommon.isVoid(sExtnWebCategory)) {
          if (sCurrentCategoryPath
              .startsWith("/GCIMasterCatalog/ECOM")) {
            sCategoryPath = sCurrentCategoryPath;
            break;
          }
        } else {
          if (sCurrentCategoryPath
              .startsWith("/GCIMasterCatalog/DCT")) {
            sCategoryPath = sCurrentCategoryPath;
            break;
          }
        }
      }

      if (!YFCCommon.isVoid(sCategoryPath)) {

        LOGGER.debug(sCategoryPath);
        // creating getItemList i/p doc by category filter
        Document getItemListInput = GCXMLUtil
            .getDocument("<Item ItemID='"
                + sItemId
                + "' GetUnpublishedItems='Y' IgnoreIsSoldSeparately='Y' ><CategoryFilter CategoryPath='"
                + sCategoryPath + "' OrganizationCode='"
                + GCConstants.GUITAR_CENTER_INC + "'/></Item>");

        Document getItemListOutDoc = GCCommonUtil
            .invokeAPI(
                env,
                GCConstants.API_GET_ITEM_LIST,
                getItemListInput,
                GCXMLUtil
                .getDocument("<ItemList><Item ItemID=''/></ItemList>"));

        if (!(getItemListOutDoc.getDocumentElement().hasChildNodes())) {

          // Check if category is already created, create if not
          // created.
          Element eleCategoryCheck = GCXMLUtil.getElementByXPath(
              modifyCategoryItemInput,
              "/ModifyCategoryItems/Category[@CategoryPath='"
                  + sCategoryPath + "']");

          if (YFCCommon.isVoid(eleCategoryCheck)) {

            LOGGER.debug("Category Does not already exist in modifyItemCategoryInput. So creating category element and appending item.");

            Element eleCategory = modifyCategoryItemInput
                .createElement("Category");
            eleCategory.setAttribute(GCConstants.CATEGORY_PATH,
                sCategoryPath);
            eleCategory.setAttribute(GCConstants.ORGANIZATION_CODE,
                GCConstants.GUITAR_CENTER_INC);
            modifyCategoryItemInput.getDocumentElement()
            .appendChild(eleCategory);
            Element eleCategoryItemList = modifyCategoryItemInput
                .createElement("CategoryItemList");
            eleCategory.appendChild(eleCategoryItemList);
            Element eleCategoryItem = modifyCategoryItemInput
                .createElement("CategoryItem");
            eleCategoryItem.setAttribute(GCConstants.ITEM_ID,
                sItemId);
            eleCategoryItem.setAttribute(
                GCConstants.ORGANIZATION_CODE,
                GCConstants.GUITAR_CENTER_INC);
            eleCategoryItem.setAttribute(
                GCConstants.UNIT_OF_MEASURE, GCConstants.EACH);
            eleCategoryItemList.appendChild(eleCategoryItem);
          } else {
            // Category already exists.
            LOGGER.debug("Category already exist in modifyItemCategoryInput. So appending item in the same.");
            // Check if item already exist in the category and skip
            // adding if its already present.
            Element eleItemCheck = GCXMLUtil
                .getElementByXPath(
                    modifyCategoryItemInput,
                    "/ModifyCategoryItems/Category[@CategoryPath='"
                        + sCategoryPath
                        + "']/"
                        + "CategoryItemList/CategoryItem[@ItemID='"
                        + sItemId + "']");
            if (YFCCommon.isVoid(eleItemCheck)) {
              Element eleCategoryItem = modifyCategoryItemInput
                  .createElement("CategoryItem");
              eleCategoryItem.setAttribute(GCConstants.ITEM_ID,
                  sItemId);
              eleCategoryItem.setAttribute(
                  GCConstants.ORGANIZATION_CODE,
                  GCConstants.GUITAR_CENTER_INC);
              eleCategoryItem.setAttribute(
                  GCConstants.UNIT_OF_MEASURE,
                  GCConstants.EACH);
              eleCategoryCheck
              .getElementsByTagName(
                  GCConstants.CATEGORY_ITEM_LIST)
                  .item(0).appendChild(eleCategoryItem);
            }
          }
        }

      }
    } catch (Exception e) {
      LOGGER.error(
          "Inside catch block of GCManageItem.manageItemCategory method. Exception is",
          e);
      throw new GCException(e);
    }
  }

  /**
   *
   * This method is used to check if item is clearance item. If yes, then
   * OnhandSafetyFactorPercentage for the item is changed to zero.<br>
   *
   * @param env : YFSEnvironment variable.
   * @param sItemID : ItemID
   * @param eleInventoryParameters : InventoryParameter element from Item I/P XML.
   *
   */
  public void checkIfClearanceItem(YFSEnvironment env, String sItemID, Element eleInventoryParameters, String sOrganizationCode) {

    LOGGER.beginTimer("GCManageItem.checkIfClearanceItem");
    Document getItemListIn =
        GCXMLUtil.getDocument("<Item ItemID='" + sItemID + "' OrganizationCode='" + sOrganizationCode + "'><Extn ExtnIsClearanceItem='Y'/></Item>");
    Document getItemListTemplate = GCXMLUtil.getDocument("<ItemList><Item ItemID=''/></ItemList>");
    Document getItemListOut = GCCommonUtil.invokeAPI(env, GCConstants.GET_ITEM_LIST, getItemListIn, getItemListTemplate);
    //Defect - 2178 - Null check for InventoryParameter element.
    if (getItemListOut.getDocumentElement().hasChildNodes() && !YFCCommon.isVoid(eleInventoryParameters)) {
      eleInventoryParameters.setAttribute(GCXmlLiterals.ONHAND_SAFETY_FACTOR_QUANTITY, GCConstants.ZERO);
    }
    LOGGER.endTimer("GCManageItem.checkIfClearanceItem");
  }

  /**
   *
   * This method is used to fetch AssociationList from each item and add it to the input of
   * multiAPI. The element is then removed from the Item I/P XML.<br>
   * GCSTORE-81- Include Recommended Accessories with Heiler Item Updates
   *
   * @param multiAPIIn : I/P document of the multiAPI containing modifyItemAssociations Input.
   * @param eleItem : Item element from item feed i/p XML.
   * @param eleAssociationList : AssociationList element from Item I/P XML.
   *
   */
  public void createMultiAPIModifyItemAssociationsIn(Document multiAPIIn, Element eleItem, Element eleAssociationList) {

    LOGGER.beginTimer("GCManageItem.createMultiAPIModifyItemAssociationsIn");
    Element eleAPI = multiAPIIn.createElement(GCXmlLiterals.API);
    eleAPI.setAttribute(GCXmlLiterals.NAME, GCConstants.MODIFY_ITEM_ASSOCIATIONS);
    multiAPIIn.getDocumentElement().appendChild(eleAPI);
    Element eleInput = multiAPIIn.createElement(GCXmlLiterals.INPUT);
    eleAPI.appendChild(eleInput);
    Element eleDummyElement = multiAPIIn.createElement(GCXmlLiterals.ASSOCIATION_LIST);
    GCXMLUtil.copyElement(multiAPIIn, eleAssociationList, eleDummyElement);
    eleInput.appendChild(eleDummyElement);
    GCXMLUtil.removeChild(eleItem, eleAssociationList);
    LOGGER.endTimer("GCManageItem.createMultiAPIModifyItemAssociationsIn");
  }

  /**
   *
   * This method is used to check if item is clearance item. If yes, the item is updated for
   * theExtnClearancePrice attribute.
   *
   * GCSTORE-166,GCSTORE-168,GCSTORE-170
   *
   * @param env
   * @param sItemID
   * @param inDoc
   * @param eleExtn
   * @throws Exception
   */

  public void manageClearanceItem(YFSEnvironment env, String sItemId, Document inDoc, Element eleExtn) {
    LOGGER.beginTimer("GCManageItem.manageClearanceItem");
    YFCDocument getItemPriceInput =
        YFCDocument.getDocumentFor("<ItemPrice Currency='USD' OrganizationCode='GC' ><LineItems><LineItem ItemID='"
            + sItemId + "' Quantity='1' UnitOfMeasure='" + GCConstants.EACH
            + "'/></LineItems></ItemPrice>");
    YFCDocument getItemPriceOutDocTemplate =
        YFCDocument
        .getDocumentFor("<ItemPrice OrganizationCode='' Currency=''><LineItems><LineItem UnitOfMeasure='' Quantity='' ListPrice='' ItemID=''></LineItem></LineItems></ItemPrice>");
    YFCDocument getItemPriceOutDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ITEM_PRICE, getItemPriceInput, getItemPriceOutDocTemplate);
    YFCNodeList<YFCElement> eleLineItems = getItemPriceOutDoc.getElementsByTagName(GCConstants.LINE_ITEMS);
    for (YFCElement lineItemsEle : eleLineItems) {
      YFCElement eleLineItem = lineItemsEle.getChildElement(GCConstants.LINE_ITEM);
      String sListPrice= eleLineItem.getAttribute(GCConstants.LIST_PRICE);
      eleExtn.setAttribute(GCXmlLiterals.EXTN_CLEARANCE_PRICE, sListPrice);
    }
    LOGGER.endTimer("GCManageItem.manageClearanceItem");
  }

  /**
   *
   * This Method is used to call createInventoryActivity which will be used for publishing inventory
   * change details to ATG for clearance item.
   *
   * GCSTORE-166,GCSTORE-168,GCSTORE-170
   *
   * @param env
   * @param sItemID
   * @param inDoc
   */

  public void createInventoryActivity(YFSEnvironment env, String sItemId, YFCDocument inDoc) {
    LOGGER.beginTimer("GCManageItem.createInventoryActivity");
    String sProductClass;
    YFCNodeList<YFCElement> nlItemList = inDoc.getElementsByTagName(GCConstants.ITEM);
    YFCElement eleItem = nlItemList.item(0);
    YFCElement eleExtn = eleItem.getChildElement(GCConstants.EXTN);
    String sExtnSetCode = eleExtn.getAttribute(GCConstants.EXTN_SET_CODE);
    if (YFCCommon.equals(sExtnSetCode, GCConstants.ONE, false)) {
      sProductClass = GCConstants.SET;
    } else {
      sProductClass = GCConstants.REGULAR;
    }
    YFCDocument adjustInventoryInput =
        YFCDocument.getDocumentFor("<Items><Item AdjustmentType='" + GCConstants.ADJUSTMENT + "' ItemID='" + sItemId
            + "' OrganizationCode='" + GCConstants.GUITAR_CENTER_INC + "' ProductClass='" + sProductClass
            + "' Quantity='0' ShipNode='" + GCConstants.MFI + "' SupplyType='" + GCConstants.ONHAND
            + "' UnitOfMeasure='"
            + GCConstants.EACH + "'/></Items>");
    GCCommonUtil.invokeAPI(env, GCConstants.API_ADJUST_INVENTORY, adjustInventoryInput, adjustInventoryInput);
    YFCDocument getCommonCodeListInput =
        YFCDocument.getDocumentFor("<CommonCode CodeType='CLEARANCE_DIST_ID'></CommonCode>");
    YFCDocument getCommonCodeListOutput =
        GCCommonUtil.invokeAPI(env, GCConstants.API_GET_COMMON_CODE_LIST, getCommonCodeListInput,
            getCommonCodeListInput);
    YFCNodeList<YFCElement> eleCommonCode = getCommonCodeListOutput.getElementsByTagName(GCConstants.COMMON_CODE);
    for (YFCElement commonCodeEle : eleCommonCode) {
      String sDistributionRuleId = commonCodeEle.getAttribute(GCConstants.CODE_SHORT_DESCRIPTION);
      YFCDocument createInventoryActivityInput =
          YFCDocument.getDocumentFor("<InventoryActivity DistributionRuleId='" + sDistributionRuleId + "' ItemID='"
              + sItemId + "' OrganizationCode='" + GCConstants.GUITAR_CENTER_INC + "' ProductClass='" + sProductClass
              + "' ProcessedFlag='' UnitOfMeasure='" + GCConstants.EACH + "'/>");
      GCCommonUtil.invokeAPI(env, GCXmlLiterals.API_CREATE_INVENTORY_ACTIVITY, createInventoryActivityInput,
          createInventoryActivityInput);
    }
    LOGGER.endTimer("GCManageItem.createInventoryActivity");
  }

  
  
   /**
   * This method adds item/category to modifyItemCategory input for the supplied category path.
   * Description of removeItemFromCategory
   *
   * @param modifyCategoryItemInput
   * @param sItemId
   * @param sCategoryPath
   *
   */
  private void removeItemFromCategory(Document modifyCategoryItemInput, String sItemId, String sCategoryPath){
    // Check if category is already created, create if not
    // created.
    Element eleCategoryCheck = GCXMLUtil.getElementByXPath(
        modifyCategoryItemInput,
        "/ModifyCategoryItems/Category[@CategoryPath='"
            + sCategoryPath + "']");

    if (YFCCommon.isVoid(eleCategoryCheck)) {

      LOGGER.debug("Category Does not already exist in modifyItemCategoryInput. So creating category element and appending item with delete action.");

      Element eleCategory = modifyCategoryItemInput
          .createElement("Category");
      eleCategory.setAttribute(GCConstants.CATEGORY_PATH,
          sCategoryPath);
      eleCategory.setAttribute(GCConstants.ORGANIZATION_CODE,
          GCConstants.GUITAR_CENTER_INC);
      modifyCategoryItemInput.getDocumentElement()
      .appendChild(eleCategory);
      Element eleCategoryItemList = modifyCategoryItemInput
          .createElement("CategoryItemList");
      eleCategory.appendChild(eleCategoryItemList);
      Element eleCategoryItem = modifyCategoryItemInput
          .createElement("CategoryItem");
      eleCategoryItem.setAttribute(GCConstants.ITEM_ID,
          sItemId);
      eleCategoryItem.setAttribute(
          GCConstants.ORGANIZATION_CODE,
          GCConstants.GUITAR_CENTER_INC);
      eleCategoryItem.setAttribute(
          GCConstants.UNIT_OF_MEASURE, GCConstants.EACH);
      eleCategoryItem.setAttribute("Action", "Delete");
      eleCategoryItemList.appendChild(eleCategoryItem);
    } else {
      // Category already exists.
      LOGGER.debug("Category already exist in modifyItemCategoryInput. So appending item with action delete in the same.");
      // Check if item already exist in the category and skip
      // adding if its already present.
      Element eleItemCheck = GCXMLUtil
          .getElementByXPath(
              modifyCategoryItemInput,
              "/ModifyCategoryItems/Category[@CategoryPath='"
                  + sCategoryPath
                  + "']/"
                  + "CategoryItemList/CategoryItem[@ItemID='"
                  + sItemId + "']");
      if (YFCCommon.isVoid(eleItemCheck)) {
        Element eleCategoryItem = modifyCategoryItemInput
            .createElement("CategoryItem");
        eleCategoryItem.setAttribute("Action", "Delete");
        eleCategoryItem.setAttribute(GCConstants.ITEM_ID,
            sItemId);
        eleCategoryItem.setAttribute(
            GCConstants.ORGANIZATION_CODE,
            GCConstants.GUITAR_CENTER_INC);
        eleCategoryItem.setAttribute(
            GCConstants.UNIT_OF_MEASURE,
            GCConstants.EACH);
        eleCategoryCheck
        .getElementsByTagName(
            GCConstants.CATEGORY_ITEM_LIST)
            .item(0).appendChild(eleCategoryItem);
      } else {
        //if category and item already exists in input to modifyCategoryItem then just add action as delete.
        eleItemCheck.setAttribute("Action", "Delete");
      }
    }

  }

  
  
  
  
  
  
  
  
  
  /**
   * Description of setProperties
   *
   * @param arg0
   * @throws Exception
   * @see com.yantra.interop.japi.YIFCustomApi#setProperties(java.util.Properties)
   */

  @Override
  public void setProperties(Properties arg0) {
  }

}
