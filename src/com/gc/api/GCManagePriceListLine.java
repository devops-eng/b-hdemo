/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *#################################################################################################################################################################
 *          OBJECTIVE: This class is used to manage pricelist for items
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0               21/01/2015       Saxena, Ekansh             GCSTORE-396
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class will update the pricelist of the items.
 *
 * @author Ekansh, Saxena
 */

public class GCManagePriceListLine implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCManagePriceListLine.class.getName());
  Properties properties;

  /**
   * This Method is used to get the service arguments.
   */
  @Override
  public void setProperties(Properties properties) {
      LOGGER.debug("inside setProperties ....");

      if (properties == null) {
          LOGGER.debug("properties is NULL");

          this.properties = new Properties();
      }

      this.properties = properties;

  }

  /**
   * managePriceListLine method is used to manage pricelist for items.
   *
   * @param env
   * @param inputDoc
 * @throws InterruptedException 
   */

  public void managePriceListLine(YFSEnvironment env, Document inputDoc) throws InterruptedException {
    
    LOGGER.beginTimer("managePriceListLine");
    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Incoming XML Doc" + inDoc.toString());
    }

    YFCNodeList<YFCElement> elePriceListLine = inDoc.getElementsByTagName(GCXmlLiterals.PRICELIST_LINE);
    for (YFCElement promiseListLineEle : elePriceListLine) {
      String sItemId = promiseListLineEle.getAttribute(GCConstants.ITEM_ID);
      String sUOM = promiseListLineEle.getAttribute(GCConstants.UOM);
      YFCElement eleExtn = promiseListLineEle.getChildElement(GCConstants.EXTN);
      if(!YFCCommon.isVoid(eleExtn)){
        String sExtnPOSPriceFeed = eleExtn.getAttribute(GCXmlLiterals.EXTN_POS_PRICE_FEED);

        if (YFCCommon.equals(sExtnPOSPriceFeed, GCConstants.YES, GCConstants.FALSE)) {
          YFCDocument cloneinDoc = YFCDocument.getDocumentFor(inputDoc);
          YFCDocument getItemListInput =
              YFCDocument.getDocumentFor("<Item OrganizationCode='" + GCConstants.GUITAR_CENTER_INC
                  + "' UnitOfMeasure='"
                  + sUOM + "'><Extn ExtnPOSItemID='" + sItemId + "'/></Item>");
          YFCDocument getItemListOutputTemplate =
              YFCDocument
              .getDocumentFor("<ItemList TotalItemList=''><Item UnitOfMeasure='' OrganizationCode='' ItemID='' ><Extn ExtnIsClearanceItem='' ExtnPOSItemID=''/></Item></ItemList>");
          YFCDocument getItemListOutputDoc =
              GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ITEM_LIST, getItemListInput, getItemListOutputTemplate);
          //GCSTORE-6316-starts
          YFCElement outItemEle = getItemListOutputDoc.getElementsByTagName(GCXmlLiterals.ITEM).item(0);
          String strIsThreadSleep=this.properties.getProperty("THREAD_SLEEP");
          String strSleepTimeInterval=this.properties.getProperty("SLEEP_TIME");
          String strRetryCount=this.properties.getProperty("RETRY_COUNT");
          int counter=0;
          boolean flag=false;
          if (YFCCommon.isVoid(outItemEle)) {
              if(!YFCCommon.isVoid(strIsThreadSleep) && GCConstants.YES.equalsIgnoreCase(strIsThreadSleep) && !YFCCommon.isVoid(strSleepTimeInterval) && !YFCCommon.isVoid(strRetryCount) )
              {
                  
              while(counter<Integer.parseInt(strRetryCount))
              {
                LOGGER.debug("GCManagePriceListLine :: Counter ::"+counter);
                long lngSleepTime = Long.parseLong(strSleepTimeInterval);
                Thread.sleep(lngSleepTime);
                counter++;
                getItemListOutputDoc =
                GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ITEM_LIST, getItemListInput, getItemListOutputTemplate);
                outItemEle = getItemListOutputDoc.getElementsByTagName(GCXmlLiterals.ITEM).item(0); 
                LOGGER.debug("GCManagePriceListLine :: outItemEle ::"+outItemEle);
                LOGGER.debug("GCManagePriceListLine :: flag ::"+flag);
              if(!YFCCommon.isVoid(outItemEle))
              {
                flag=true;
                break;  
              }
                    
              }
              }
            if(!flag)
              {
              YFCException ex = new YFCException(GCErrorConstants.GCE00503);
              ex.setAttribute(GCConstants.ITEM_ID, sItemId);
              throw ex;
              }

              
          }
          //GCSTORE-6316-ends
            String sOutItemId = outItemEle.getAttribute(GCConstants.ITEM_ID);
            YFCElement eleOutExtn = outItemEle.getChildElement(GCConstants.EXTN);
            String sOutExtnIsClearanceItem = eleOutExtn.getAttribute(GCXmlLiterals.EXTN_IS_CLEARANCE_ITEM);
            String sOutExtnPOSItemId = eleOutExtn.getAttribute(GCXmlLiterals.EXTN_POS_ITEM_ID);

            YFCNodeList<YFCElement> eleClonePriceListLine = cloneinDoc.getElementsByTagName(GCXmlLiterals.PRICELIST_LINE);
            for (YFCElement clonePriceListLineEle : eleClonePriceListLine) {
              clonePriceListLineEle.setAttribute(GCConstants.ITEM_ID, sOutItemId);
              if (YFCCommon.equals(sOutExtnIsClearanceItem, GCConstants.YES, GCConstants.FALSE)) {
                modifyClearancePrice(env, cloneinDoc, sOutItemId);
              }
              if (YFCCommon.equals(sOutItemId, sOutExtnPOSItemId, GCConstants.FALSE)) {
                GCCommonUtil.invokeAPI(env, GCXmlLiterals.API_MANAGE_PRICELIST_LINE, inDoc, inDoc);
              }
            }
          }
         else {
          GCCommonUtil.invokeAPI(env, GCXmlLiterals.API_MANAGE_PRICELIST_LINE, inDoc, inDoc);
        }
      }else {
        // Fix for MPOS-441 : Switch POSItemID to OMSItemID for store specific price feed.
        checkifStoreSpecifcPriceFeed(env, inDoc);
        GCCommonUtil.invokeAPI(env, GCXmlLiterals.API_MANAGE_PRICELIST_LINE, inDoc, inDoc);
      }
    
    LOGGER.verbose(" Exiting GCManagePriceListLine.managePriceListLine() method ");
    }
  }

  /**
   *
   * This method is used to update the ExtnClearancePrice attribute on the item levels
   *
   * @param env : YFSEnvironment variable.
   * @param sItemID : ItemID
   * @param inDoc
   */

  public void modifyClearancePrice(YFSEnvironment env, YFCDocument inDoc, String sItemId) {
    LOGGER.beginTimer("GCManagePriceListLine.modifyClearancePrice");
    YFCNodeList<YFCElement> elePriceListLine = inDoc.getElementsByTagName(GCXmlLiterals.PRICELIST_LINE);
    for (YFCElement promiseListLineEle : elePriceListLine) {
      String sExtnClearancePrice = promiseListLineEle.getAttribute(GCConstants.LIST_PRICE);
      YFCDocument manageItemInput =
          YFCDocument.getDocumentFor("<ItemList><Item Action='Manage' ItemID='" + sItemId + "' OrganizationCode='"
              + GCConstants.GUITAR_CENTER_INC + "' UnitOfMeasure='" + GCConstants.EACH + "'><Extn ExtnClearancePrice='"
              + sExtnClearancePrice + "' ></Extn></Item></ItemList>");
      GCCommonUtil.invokeAPI(env, GCConstants.API_MANAGE_ITEM, manageItemInput, manageItemInput);
    }
    LOGGER.endTimer("GCManagePriceListLine.modifyClearancePrice");
  }

  /**
   *
   * This method check if the pricelist line is store specific. If yes switch the POSItem to OMS
   * ItemID.
   *
   * @param env
   * @param inDoc
   *
   */
  private void checkifStoreSpecifcPriceFeed(YFSEnvironment env, YFCDocument inDoc) {
    LOGGER.beginTimer("GCManagePriceListLine.checkifStoreSpecifcPriceFeed");
    YFCElement elePriceListHeader = inDoc.getElementsByTagName("PricelistHeader").item(0);
    String sPriceListName = elePriceListHeader.getAttribute("PricelistName");
    if (YFCCommon.equals(GCConstants.STORE_PRICELIST_NAME, sPriceListName)) {
      YFCElement elePricelistLine = inDoc.getElementsByTagName(GCXmlLiterals.PRICELIST_LINE).item(0);
      String sPOSItemID = elePricelistLine.getAttribute(GCConstants.ITEM_ID);
      YFCDocument getItemListIn = YFCDocument.getDocumentFor("<Item><Extn ExtnPOSItemID='" + sPOSItemID + "'/></Item>");
      YFCDocument getItemListOut = GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ITEM_LIST, getItemListIn,
          YFCDocument.getDocumentFor("<ItemList><Item ItemID=''/></ItemList>"));
      YFCElement eleRoot = getItemListOut.getDocumentElement();
      if (eleRoot.hasChildNodes()) {
        String sOMSItemID = eleRoot.getChildElement("Item").getAttribute(GCConstants.ITEM_ID);
        elePricelistLine.setAttribute(GCConstants.ITEM_ID, sOMSItemID);
      }
    }
    LOGGER.endTimer("GCManagePriceListLine.checkifStoreSpecifcPriceFeed");



  }
}
