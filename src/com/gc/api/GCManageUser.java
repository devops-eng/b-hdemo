/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 * *###########################################
 * ######################################################
 * ################################################################ OBJECTIVE: This class is part of
 * Base config DD and have logic to create, modify and delete users.
 * ################################
 * ##################################################################
 * ############################################################### Version Date Modified By
 * Description
 * ######################################################################################
 * ########################################################################### 1.0 07/04/2014
 * Mittal, Yashu JIRA No: Description of issue. 1.1 02/03/2015 Kumar, Rakesh OMS-4826:Automatically
 * assign user�s to alert queues based on their user group - CR 75
 * ##################################
 * ################################################################
 * ###############################################################
 */
package com.gc.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * @author <a href="mailto:yashu.mittal@expicient.com">Yashu Mittal</a>
 */
public class GCManageUser implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCManageUser.class.getName());

  /**
   * This method is used to create, modify or delete users
   *
   * @param env
   * @param inDoc
   * @throws GCException
   */

  public void manageUser(YFSEnvironment env, Document inDoc)
      throws GCException {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" Entering GCManageUser.manageUser() method Class Reload");
      LOGGER.debug(" manageUser() method, Input document is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    try {

      Element eleUser = inDoc.getDocumentElement();
      String sAction = eleUser.getAttribute(GCConstants.ACTION);
      String sLoginid = eleUser.getAttribute(GCConstants.LOGINID);

      eleUser.setAttribute("OrganizationKey", "DEFAULT");

      LOGGER.debug("Loginid:" + sLoginid);

      if (!YFCCommon.isVoid(sLoginid)) {

        Element eleExtn = (Element) eleUser.getElementsByTagName(
            GCConstants.EXTN).item(0);

        String sUserGroup = GCConstants.BLANK_STRING;

        // Fetching UserGroup corresponding to LoginId from Custom Table
        // GCLoginIdUserGroup
        Document gcLoginIDUserGroupOutDoc = GCCommonUtil.invokeService(
            env,
            GCConstants.GC_LOGINID_USER_GROUP_SERVICE,
            GCXMLUtil.getDocument("<GCLoginIDUserGroup Loginid=' "
                + sLoginid + "'/>"));
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("GCLoginIDUserGroupOutDoc: "
              + GCXMLUtil.getXMLString(gcLoginIDUserGroupOutDoc));
        }
        Element eleGCLoginIDUserGroupList = gcLoginIDUserGroupOutDoc
            .getDocumentElement();

        if (eleGCLoginIDUserGroupList.hasChildNodes()) {
          LOGGER.debug("UserGroup is present in GCLoginIDUserGroup Table");
          sUserGroup = ((Element) eleGCLoginIDUserGroupList
              .getElementsByTagName(
                  GCConstants.GC_LOGINID_USERGROUP).item(0))
                  .getAttribute(GCConstants.USER_GROUP);
        }

        LOGGER.debug("UserGroup in GCLoginIDUserGroup Table:"
            + sUserGroup);

        if (GCConstants.ACTION_CREATE.equalsIgnoreCase(sAction)) {
          LOGGER.debug("Action : Create");
          if (!YFCCommon.isVoid(sUserGroup)) {

            LOGGER.debug("Action is create and user group is fetched from GC_LOGINID_USERGROUP Table");

            // invoke stampUserGroup Method
            stampUserGroup(env, inDoc, eleUser, sUserGroup);
            // invoke createUserHierarchy api
            appendQueueList(env, sUserGroup, inDoc);
            GCCommonUtil.invokeAPI(env,
                GCConstants.API_CREATE_USER_HIERARCHY, inDoc);
          } else {

            // invoke fetch usergroup method from GC_USERGROUP Table
            // and stamp it on inDoc
            LOGGER.debug("Action is create and user group is to be fetched from GC_USERGROUP Table");
            String stUserGroup = fetchUserGroup(env, eleExtn);
            stampUserGroup(env, inDoc, eleUser,
                stUserGroup);
            appendQueueList(env, stUserGroup, inDoc);
            GCCommonUtil.invokeAPI(env,
                GCConstants.API_CREATE_USER_HIERARCHY, inDoc);
          }
        } else if (GCConstants.ACTION_MODIFY.equalsIgnoreCase(sAction)) {
          LOGGER.debug("Action : Modify");

          Document getUserListOutDoc = GCCommonUtil
              .invokeAPI(
                  env,
                  GCConstants.GET_USER_LIST,
                  GCXMLUtil.getDocument("<User Loginid='"
                      + sLoginid + "'/>"),
                      GCXMLUtil
                      .getDocument("<UserList><User><UserGroupLists><UserGroupList UsergroupId=''></UserGroupList></UserGroupLists></User></UserList>"));
          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getUserListOutDoc: "
                + GCXMLUtil.getXMLString(getUserListOutDoc));
          }
          if (getUserListOutDoc.getDocumentElement().hasChildNodes()) {
            LOGGER.debug("UserList has the given user");

            String sUsergroupId = GCXMLUtil
                .getAttributeFromXPath(getUserListOutDoc,
                    "/UserList/User/UserGroupLists/UserGroupList/@UsergroupId");
            LOGGER.debug(sUsergroupId);

            if (!YFCCommon.isVoid(sUsergroupId)
                && sUsergroupId.equalsIgnoreCase(sUserGroup)) {
              LOGGER.debug("UserGroupID and User Group are same");
              appendQueueList(env, sUserGroup, inDoc);
              GCCommonUtil.invokeAPI(env,
                  GCConstants.API_MODIFY_USER_HIERARCHY,
                  inDoc);
            } else if (!YFCCommon.isVoid(sUsergroupId)
                && (!sUsergroupId.equalsIgnoreCase(sUserGroup))
                && (!YFCCommon.isVoid(sUserGroup))) {
              LOGGER.debug("UserGroupId and User Group are different");
              stampUserGroup(env, inDoc, eleUser, sUserGroup);
              appendQueueList(env, sUserGroup, inDoc);
              GCCommonUtil.invokeAPI(env,
                  GCConstants.API_MODIFY_USER_HIERARCHY,
                  inDoc);
            } else if (YFCCommon.isVoid(sUserGroup)) {
              LOGGER.debug("User Group is blank so fetching user group from GC_USERGROUP Table");
              String stUserGroup = fetchUserGroup(env, eleExtn);
              stampUserGroup(env, inDoc, eleUser,
                  stUserGroup);
              appendQueueList(env, stUserGroup, inDoc);
              GCCommonUtil.invokeAPI(env,
                  GCConstants.API_MODIFY_USER_HIERARCHY,
                  inDoc);
            }
          } else {
            LOGGER.debug("Action is modify but user is not present so creating user");
            eleUser.setAttribute(GCConstants.ACTION,
                GCConstants.ACTION_CREATE);
            manageUser(env, inDoc);
          }
        } else if (GCConstants.ACTION_DELETE.equalsIgnoreCase(sAction)) {
          LOGGER.debug("Action : Delete");
          GCCommonUtil.invokeAPI(
              env,
              GCConstants.API_DELETE_USER_HIERARCHY,
              GCXMLUtil.getDocument("<User Loginid='" + sLoginid
                  + "'/>"));
        }
      } else {
        LOGGER.debug("Login id is empty");
        YFSException e = new YFSException("Blank LoginId is passed");
        throw e;
      }
    } catch (Exception e) {
      LOGGER.error(
          "Inside Catch block of GCManageUser.manageUser method. Exception is",
          e);
      throw new GCException(e);
    }
  }

  /**
   * This method will stamp userGroup, menu id and password
   *
   * @param env
   * @param inDoc
   * @param eleUser
   * @throws GCException
   */

  public void stampUserGroup(YFSEnvironment env, Document inDoc,
      Element eleUser, String sUserGroup) throws GCException {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" Entering GCManageUser.stampUserGroup() method ");
      LOGGER.debug("stampUserGroup() method, Input document is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    try {
      eleUser.setAttribute(GCConstants.PASSWORD_ATTRIBUTE,
          GCConstants.PASSWORD_VALUE);
      // Stamp user Group and Menu if user group is not blank.

      if (!YFCCommon.isVoid(sUserGroup)) {

        eleUser.setAttribute(GCConstants.MENU_ID, sUserGroup + "_MENU");
        Element eleUserGroupLists = inDoc
            .createElement(GCConstants.USER_GROUP_LISTS);
        eleUser.appendChild(eleUserGroupLists);
        eleUserGroupLists.setAttribute(GCConstants.RESET,
            GCConstants.YES);
        Element eleUserGroupList = inDoc
            .createElement(GCConstants.USER_GROUP_LIST);
        eleUserGroupLists.appendChild(eleUserGroupList);

        if (YFCUtils.equals(sUserGroup, "Store Operations") || YFCUtils.equals(sUserGroup, "Store Management")
            || YFCUtils.equals(sUserGroup, "Store Admin")) {
          // Set TeamID
          eleUser.setAttribute(GCConstants.DATA_SECURITY_GROUP_ID, "GCStoreTeam");
        } else {
          // Set TeamID
          eleUser.setAttribute(GCConstants.DATA_SECURITY_GROUP_ID, "GCTeam");
        }
        eleUserGroupList.setAttribute(GCConstants.USER_GROUP_ID,
            sUserGroup);
      } else {
        LOGGER.debug("User Group is not present in any table. Creating user without any UserGroup and menu assigned");
      }
    } catch (Exception e) {
      LOGGER.error(
          "Inside Catch block of GCManageUser.stampUserGroup method. Exception is :",
          e);
      throw new GCException(e);

    }
  }

  /**
   * This method will fetch UserGroup corresponding to StoreId,
   * ManageLevelCode and StoreMenuLevel from Custom Table GCUserGroup
   *
   * @param env
   * @param eleExtn
   * @throws GCException
   */

  public String fetchUserGroup(YFSEnvironment env, Element eleExtn)
      throws GCException {

    LOGGER.debug(" Entering GCManageUser.fetchUserGroup() method ");
    try {
      // Fetching Extn Attributes
      if (!YFCCommon.isVoid(eleExtn)) {

        String sExtnManagerLevelCode = null;
        String sExtnStoreMenuLevel = eleExtn.getAttribute(GCConstants.EXTN_STORE_MENU_LEVEL);

        String sExtnStoreID = fetchStoreID(env, eleExtn);
        if (YFCUtils.equals(sExtnStoreID, eleExtn.getAttribute(GCConstants.EXTN_STORE_ID))) {
          sExtnManagerLevelCode = eleExtn.getAttribute(GCConstants.EXTN_MANAGER_LEVEL_CODE);

          Document gcUserGroupOutDoc = GCCommonUtil.invokeService(
              env,
              GCConstants.GC_USER_GROUP_SERVICE,
              GCXMLUtil.getDocument("<GCUserGroup StoreID='"
                  + sExtnStoreID + "' ManagerLevelCode=' "
                  + sExtnManagerLevelCode + "' StoreMenuLevel=' "
                  + sExtnStoreMenuLevel + "' />"));
          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("GCUserGroupOutDoc:"
                + GCXMLUtil.getXMLString(gcUserGroupOutDoc));
          }
          Element eleGCUserGroupList = gcUserGroupOutDoc
              .getDocumentElement();

          if (eleGCUserGroupList.hasChildNodes()) {
            String sUserGroup = ((Element) eleGCUserGroupList
                .getElementsByTagName(GCConstants.GC_USERGROUP)
                .item(0)).getAttribute(GCConstants.USER_GROUP);
            return sUserGroup;
          }

        } else {
          sExtnManagerLevelCode = "0-7";

          //List to store retail domains
          List<String> retailDomainList = new ArrayList<String> ();
          fetchRetailDomainNames(env, retailDomainList);
          if(retailDomainList.contains(eleExtn.getAttribute(GCConstants.EXTN_USR_DOMAIN))){
            // query the GC_USERGROUP custom table
            Document gcUserGroupOutDoc =
                GCCommonUtil.invokeService(
                    env,
                    GCConstants.GC_USER_GROUP_SERVICE,
                    GCXMLUtil.getDocument("<GCUserGroup StoreID='" + sExtnStoreID + "' ManagerLevelCode=' "
                        + sExtnManagerLevelCode + "' StoreMenuLevel=' " + sExtnStoreMenuLevel + "' />"));
            if (LOGGER.isDebugEnabled()) {
              LOGGER.debug("GCUserGroupOutDoc:" + GCXMLUtil.getXMLString(gcUserGroupOutDoc));
            }
            Element eleGCUserGroupList = gcUserGroupOutDoc.getDocumentElement();

            if (eleGCUserGroupList.hasChildNodes()) {
              String sUserGroup =
                  ((Element) eleGCUserGroupList.getElementsByTagName(GCConstants.GC_USERGROUP).item(0))
                  .getAttribute(GCConstants.USER_GROUP);
              return sUserGroup;
            }
          } else {
            YFCDocument commonCodeOutputNotRetail =
                GCCommonUtil.getCommonCodeListByTypeAndValue(env, "STR_ADMIN_USR", GCConstants.GC,
                    eleExtn.getAttribute("Non Retail"));
            YFCElement eleCommonCodeListNotRetail = commonCodeOutputNotRetail.getDocumentElement();
            if (eleCommonCodeListNotRetail.hasChildNodes()) {
              String sUserGroup =
                  eleCommonCodeListNotRetail.getElementsByTagName(GCConstants.COMMON_CODE).item(0)
                  .getAttribute(GCConstants.CODE_SHORT_DESCRIPTION);
              return sUserGroup;
            }
          }
        }
      }
      return null;
    } catch (Exception e) {
      LOGGER.error(
          "Inside Catch block of GCManageUser.fetchUserGroup method. Exception is :",
          e);
      throw new GCException(e);
    }
  }

  /**
   * This method is used to prepare a list of retail domain names fetched from common code.
   *
   * @param env
   * @param retailDomainList
   */
  private void fetchRetailDomainNames(YFSEnvironment env, List<String> retailDomainList) {

    YFCDocument commonCodeOutput =
        YFCDocument.getDocumentFor(GCCommonUtil.getCommonCodeListByType(env, "GC_RETAIL_DOMAIN", GCConstants.GC));
    YFCElement eleCommonCodeList = commonCodeOutput.getDocumentElement();
    if (eleCommonCodeList.hasChildNodes()) {
      YFCNodeList<YFCElement> nlCommonCode = eleCommonCodeList.getElementsByTagName(GCConstants.COMMON_CODE);
      for (YFCElement eleCommonCode : nlCommonCode) {
        retailDomainList.add(eleCommonCode.getAttribute(GCConstants.CODE_VALUE));
      }
    }
  }

  /**
   * This method is used to call getCommonCodeList API and stamp storeID. StoreID will be a range of
   * value(start-end) if it lies in the range fetched from common code.
   *
   * @param env
   * @param eleExtn
   * @return
   */
  private String fetchStoreID(YFSEnvironment env, Element eleExtn) {
    LOGGER.beginTimer("GCManageUser.fetchStoreID");
    LOGGER.verbose("GCManageUser.fetchStoreID--Begin");
    YFCDocument commonCodeOutput =
        YFCDocument.getDocumentFor(GCCommonUtil.getCommonCodeListByType(env, GCConstants.STORE_PHY_LOC_SCR_NR,
            GCConstants.GC));
    YFCElement eleCommomCodeList = commonCodeOutput.getDocumentElement();
    YFCIterable<YFCElement> nlCommonCode = eleCommomCodeList.getChildren();
    String startValue = null;
    String endValue = null;
    String storeID = eleExtn.getAttribute(GCConstants.EXTN_STORE_ID);

    for (YFCElement eleCommonCode : nlCommonCode) {
      final String codeValue = eleCommonCode.getAttribute(GCConstants.CODE_VALUE);
      final String codeShortDesc = eleCommonCode.getAttribute(GCConstants.CODE_SHORT_DESCRIPTION);
      if (YFCUtils.equals(codeValue, "start_val")) {
        startValue = codeShortDesc;
      } else if (YFCUtils.equals(codeValue, "end_val")) {
        endValue = codeShortDesc;
      }
    }
    LOGGER.verbose("startValue : " + startValue);
    LOGGER.verbose("endValue : " + endValue);
    if (!YFCCommon.isVoid(startValue) && !YFCCommon.isVoid(endValue) && NumberUtils.isNumber(storeID)) {
      int strtVal = Float.valueOf(startValue).intValue();
      int endVal = Float.valueOf(endValue).intValue();
      int strID = Float.valueOf(storeID).intValue();

      if (strtVal <= strID && endVal >= strID) {
        storeID = startValue + "-" + endValue;
      }
    }
    LOGGER.verbose("storeID : " + storeID);
    LOGGER.verbose("GCManageUser.fetchStoreID--end");
    LOGGER.endTimer("GCManageUser.fetchStoreID");
    return storeID;
  }

  /**
   *
   * Description of appendQueueList: this method finds the Queues corresponding to the input
   * UserGroup and appends the QueueSubscriptions in the inDoc
   *
   * @param env
   * @param sUserGroup
   * @param inDoc
   * @throws GCException
   *
   */

  private void appendQueueList(YFSEnvironment env, String sUserGroup, Document inDoc) throws GCException {
    LOGGER.debug(" Entering GCManageUser.appendQueueList() method ");
    try {
      Document getUserGroupQueueMappingListOutDoc =
          GCCommonUtil.invokeService(env, GCConstants.GC_GET_USER_GROUP_QUEUE_MAPPING_LIST,
              GCXMLUtil.getDocument("<GCUserGroupQueueMapping ExtnUserGroupName='" + sUserGroup + "'/>"));
      Element eleGCUserGroupQueueMappingList = getUserGroupQueueMappingListOutDoc.getDocumentElement();
      NodeList nlGCUserGroupQueueMapping =
          XPathAPI.selectNodeList(eleGCUserGroupQueueMappingList, "GCUserGroupQueueMapping");
      int count = nlGCUserGroupQueueMapping.getLength();
      if (count > 0) {
        Element eleQueueSubscriptionList = GCXMLUtil.createElement(inDoc, GCConstants.QUEUE_SUBSCRIPTION_LIST, null);
        eleQueueSubscriptionList.setAttribute(GCConstants.RESET, GCConstants.YES);

        // This List contains QueueKeys Subscribed to a UserGroup.
        List<String> queueKeyList = new ArrayList<String>();

        for (int i = 0; i < count; i++) {
          Element eleGCUserGroupQueueMapping = (Element) nlGCUserGroupQueueMapping.item(i);

          // finding QueueKey
          String sQueueName = eleGCUserGroupQueueMapping.getAttribute("ExtnQueueName");
          if (sQueueName.contains("&")) {
            sQueueName = sQueueName.replace("&", "&amp;");
          }
          Document getQueueListOutDoc =
              GCCommonUtil.invokeAPI(env, GCConstants.GET_QUEUE_LIST,
                  GCXMLUtil.getDocument("<Queue QueueName='" + sQueueName + "'/>"),
                  GCXMLUtil.getDocument("<QueueList><Queue QueueKey='' QueueName=''/></QueueList>"));
          Element eleQueueList = getQueueListOutDoc.getDocumentElement();
          String sQueueKey = GCXMLUtil.getAttributeFromXPath(eleQueueList, "Queue/@QueueKey");

          // Avoiding null & duplicate QueueKey from subscription
          if (YFCCommon.isVoid(sQueueKey) || queueKeyList.contains(sQueueKey)) {
            continue;
          } else {
            queueKeyList.add(sQueueKey);
          }

          // appending QueueSubscriptionList
          Element eleQueueSubscription = GCXMLUtil.createElement(inDoc, GCConstants.QUEUE_SUBSCRIPTION, null);
          eleQueueSubscription.setAttribute(GCConstants.QUEUE_KEY, sQueueKey);
          eleQueueSubscriptionList.appendChild(eleQueueSubscription);
        }
        Element eleUserInDoc = inDoc.getDocumentElement();
        eleUserInDoc.appendChild(eleQueueSubscriptionList);
      }
    } catch (Exception e) {
      LOGGER.error("Inside Catch block of GCManageUser.appendQueueList method. Exception is :", e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCManageUser.appendQueueList() method ");
  }
  /**
   * Description of setProperties
   *
   * @param arg0
   * @throws Exception
   * @see com.yantra.interop.japi.YIFCustomApi#setProperties(java.util.Properties)
   */

  @Override
  public void setProperties(Properties arg0) {

  }

}
