/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class fetches get order line list output based on input criteria parameter
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               04/18/2014        Kumar,Prateek		 		Initital Draft
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.util.Properties;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCMassOperationGetOrderLineList implements YIFCustomApi {

  YFSEnvironment env = null;
  String sEnterpriseCode = "";
  String sItemID = "";
  String sStatusCode = "";
  String sFromUnitPrice = "";
  String sToUnitPrice = "";
  String sFromOrderDate = "";
  String sToOrderDate = "";
  String sOrderCaptureChannel = "";
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCMassOperationGetOrderLineList.class.getName());

  @Override
  public void setProperties(Properties arg0) throws GCException {

  }

  public Document massOperationGetOrderLineList(YFSEnvironment env,
      Document inDoc) throws GCException {
    try {
      final String logInfo = "GCMassOperationGetOrderLineList :: massOperationGetOrderLineList :: ";
      LOGGER.verbose(logInfo + "Begin");

      LOGGER.debug(logInfo + "Input Document"
          + GCXMLUtil.getXMLString(inDoc));

      this.env = env;

      sEnterpriseCode = XPathAPI.selectSingleNode(inDoc,
          "//@EnterpriseCode").getNodeValue();
      sItemID = XPathAPI.selectSingleNode(inDoc, "//@ItemID")
          .getNodeValue();
      String sStatus = XPathAPI.selectSingleNode(inDoc, "//@Status")
          .getNodeValue();
      sFromOrderDate = XPathAPI.selectSingleNode(inDoc,
          "//@FromOrderDate").getNodeValue();
      sToOrderDate = XPathAPI.selectSingleNode(inDoc, "//@ToOrderDate")
          .getNodeValue();

      Element eleOrderLineIp = inDoc.getDocumentElement();

      sFromUnitPrice = eleOrderLineIp
          .getAttribute(GCConstants.FROM_UNIT_PRICE);
      if (YFCCommon.isVoid(sFromUnitPrice)) {
        sFromUnitPrice = "";
      }
      sToUnitPrice = eleOrderLineIp
          .getAttribute(GCConstants.TO_UNIT_PRICE);
      if (YFCCommon.isVoid(sToUnitPrice)) {
        sToUnitPrice = "";
      }

      Element eleOrder = (Element) XPathAPI.selectSingleNode(inDoc, "//Order");
      sOrderCaptureChannel = eleOrder.getAttribute(GCConstants.ENTRY_TYPE);
      if (YFCCommon.isVoid(sOrderCaptureChannel)) {
        sOrderCaptureChannel = "";
      }

      if (GCConstants.CREATED.equalsIgnoreCase(sStatus)) {
        sStatusCode = GCConstants.STATUS_CREATED;
      } else if (GCConstants.SCHEDULED.equalsIgnoreCase(sStatus)) {
        sStatusCode = GCConstants.STATUS_SCHEDULED;
      } else if (GCConstants.BACKORDERED.equalsIgnoreCase(sStatus)) {
        sStatusCode = GCConstants.STATUS_BACKORDERED;
      }

      Document docGetOrderLineListOp = callGetOrderLineList();
      Element eleRootGetOrderLineListOp = docGetOrderLineListOp
          .getDocumentElement();
      NodeList nlOrderLine = XPathAPI.selectNodeList(
          docGetOrderLineListOp, "OrderLineList/OrderLine");
      for (int i = 0; i < nlOrderLine.getLength(); i++) {

        Element eleOrderLine = (Element) nlOrderLine.item(i);
        Document docOrderLine = GCXMLUtil.getDocument(eleOrderLine,
            true);
        String sLineTotal = XPathAPI.selectSingleNode(docOrderLine,
            "//OrderLine/LinePriceInfo/@LineTotal").getNodeValue();
        String sOrderedQty = XPathAPI.selectSingleNode(docOrderLine,
            "//OrderLine/@OrderedQty").getNodeValue();

        double[] dFinalUnitPriceArray = GCCommonUtil
            .splitNumberForDecimalPrecision(
                (Double.valueOf(sLineTotal))
                / (Double.valueOf(sOrderedQty)), 2);
        double dFinalUnitPrice = dFinalUnitPriceArray[0];
        LOGGER.debug(logInfo + "dFinalUnitPrice :: " + dFinalUnitPrice);

        boolean bRemoveOrderLine = false;
        if (!"".equals(sFromUnitPrice)) {

          double dFromUnitPrice = Double.valueOf(sFromUnitPrice);
          LOGGER.debug(logInfo + "dFromUnitPrice :: "
              + dFromUnitPrice);
          if (dFinalUnitPrice < dFromUnitPrice) {
            bRemoveOrderLine = true;
          }
        }

        if (!"".equals(sToUnitPrice)) {
          double dToUnitPrice = Double.valueOf(sToUnitPrice);

          LOGGER.debug(logInfo + "dToUnitPrice :: " + dToUnitPrice);
          if (dFinalUnitPrice > dToUnitPrice) {
            bRemoveOrderLine = true;
          }
        }

        if (bRemoveOrderLine) {
          eleRootGetOrderLineListOp.removeChild(eleOrderLine);
        }
        // If the OrderLine includes PROMOTION, an additional flag is
        // added
        // to the
        // OrderLine element for UI purpose.
        String sHasPromotion = GCConstants.NO;
        Element eleLineCharge = (Element) XPathAPI.selectSingleNode(
            docOrderLine,
            "//LineCharge[@ChargeCategory='PROMOTION']");
        if (!YFCCommon.isVoid(eleLineCharge)) {
          sHasPromotion = GCConstants.YES;
        }
        LOGGER.debug(logInfo + "sHasPromotion :: " + sHasPromotion);
        eleOrderLine.setAttribute(GCConstants.HAS_PROMOTION,
            sHasPromotion);
      }
      LOGGER.debug(logInfo + "Output Document"
          + GCXMLUtil.getXMLString(docGetOrderLineListOp));
      LOGGER.verbose(logInfo + "End");
      return docGetOrderLineListOp;
    } catch (Exception e) {
      LOGGER.error(
          "Inside Catch block of GCMassOperationGetOrderLineList.massOperationGetOrderLineList method. Exception is :",
          e);
      throw new GCException(e);
    }
  }

  /**
   *
   * @return
   * @throws Exception
   */
  private Document callGetOrderLineList() throws GCException {
    try {
      final String logInfo = "GCMassOperationGetOrderLineList :: callGetOrderLineList :: ";
      LOGGER.verbose(logInfo + "Begin");

      Document docGetOrderLineListIp = GCXMLUtil
          .createDocument(GCConstants.ORDER_LINE);
      Element eleGetOrderLineListIpRoot = docGetOrderLineListIp
          .getDocumentElement();
      eleGetOrderLineListIpRoot.setAttribute(GCConstants.ENTERPRISE_CODE,
          sEnterpriseCode);
      eleGetOrderLineListIpRoot.setAttribute(GCConstants.STATUS,
          sStatusCode);
      // Append Item
      Element eleItem = docGetOrderLineListIp
          .createElement(GCConstants.ITEM);
      eleGetOrderLineListIpRoot.appendChild(eleItem);
      eleItem.setAttribute(GCConstants.ITEM_ID, sItemID);
      // Append order level details
      Element eleOrder = docGetOrderLineListIp
          .createElement(GCConstants.ORDER);
      eleGetOrderLineListIpRoot.appendChild(eleOrder);
      eleOrder.setAttribute(GCConstants.ORDER_DATE_QRY_TYPE,
          GCConstants.DATERANGE);
      eleOrder.setAttribute(GCConstants.FROM_ORDER_DATE, sFromOrderDate);
      eleOrder.setAttribute(GCConstants.TO_ORDER_DATE, sToOrderDate);
      Element eleOrderExtn = docGetOrderLineListIp.createElement(GCConstants.EXTN);
      eleOrderExtn.setAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL, sOrderCaptureChannel);
      eleOrder.appendChild(eleOrderExtn);
      // call get order line list api
      LOGGER.debug(logInfo + "docGetOrderLineListIp"
          + GCXMLUtil.getXMLString(docGetOrderLineListIp));
      Document docGetOrderLineListOp = GCCommonUtil.invokeAPI(env,
          docGetOrderLineListIp, GCConstants.API_GET_ORDER_LINE_LIST,
          GCConstants.GET_ORDER_LINE_LIST_TEMP_FOR_MASS_OPERATION);
      LOGGER.debug(logInfo + "docGetOrderLineListOp"
          + GCXMLUtil.getXMLString(docGetOrderLineListOp));
      LOGGER.verbose(logInfo + "End");
      return docGetOrderLineListOp;
    } catch (Exception e) {
      LOGGER.error(
          "Inside Catch block of GCMassOperationGetOrderLineList.callGetOrderLineList method. Exception is :",
          e);
      throw new GCException(e);
    }
  }

}
