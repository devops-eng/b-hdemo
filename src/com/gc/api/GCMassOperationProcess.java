/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class do mass cancellation or substitution based on action
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               04/18/2014        Kumar,Prateek		 		Initital Draft
 *#################################################################################################################################################################
 */
package com.gc.api;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCMassOperationProcess {

  YFSEnvironment env = null;
  Document ipDoc = null;
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCMassOperationProcess.class.getName());
  String sSubbedItemPrice = "";
  Document docGetCompleteItemListOp = null;

  /**
   *
   * @param env
   * @param ipDoc
   * @return
   * @throws Exception
   */
  public Document massChangeOrder(YFSEnvironment env, Document ipDoc)
      throws GCException {
    try {
      final String logInfo = "GCMassOperationProcess :: massChangeOrder :: ";
      LOGGER.verbose(logInfo + "Begin");

      LOGGER.debug(logInfo + "Input Document\n"
          + GCXMLUtil.getXMLString(ipDoc));
      this.ipDoc = ipDoc;
      this.env = env;
      String sAction = XPathAPI.selectSingleNode(ipDoc,
          "/OrderLine/@Action").getNodeValue();

      if (GCConstants.CANCEL.equalsIgnoreCase(sAction)) {
        cancelOrderLine();

      } else if (GCConstants.SUBSTITUTE.equalsIgnoreCase(sAction)) {
        substituteOrderLine();
      }
      //temp, will remove after debugging
      LOGGER.info("ENDMASSREQUEST");
      return ipDoc;
    } catch (Exception e) {
      LOGGER.error("The exception is in massChangeOrder method:", e);
      throw new GCException(e);
    }
  }

  /**
   * @throws TransformerException
   * @throws DOMException
   * @throws Exception
   *
   *
   */
  private void substituteOrderLine() throws GCException {
    final String logInfo = "GCMassOperationProcess :: substituteOrderLine :: ";
    LOGGER.info(logInfo + "Begin");
    try {
      String sSubbedItemID = GCXMLUtil.getAttributeFromXPath(ipDoc,
          "//@SubbedItemID");
      String sSubbedItemPriceType = GCXMLUtil.getAttributeFromXPath(ipDoc,
          "//@SubbedItemPriceType");
      String sFulfillmentType = GCXMLUtil.getAttributeFromXPath(ipDoc,
          "//OrderLine/@FulfillmentType");
      String sStatus = GCXMLUtil.getAttributeFromXPath(ipDoc,
          "OrderLine/@MassOperationStatus");
      LOGGER.debug(logInfo + "sStatus :: " + sStatus);
      String sStatusQty = GCXMLUtil.getAttributeFromXPath(
          ipDoc,
          "//OrderStatuses/OrderStatus[@StatusDescription='" + sStatus
          + "']/@StatusQty");
      LOGGER.debug(logInfo + "sStatusQty :: " + sStatusQty);
      String sOrderedQty = GCXMLUtil.getAttributeFromXPath(ipDoc,
          "//OrderLine/@OrderedQty");
      LOGGER.debug(logInfo + "sOrderedQty :: " + sOrderedQty);
      String sOrderLineTranQuantity = String.valueOf(Double
          .valueOf(sOrderedQty) - Double.valueOf(sStatusQty));
      LOGGER.debug(logInfo + "sOrderLineTranQuantity :: "
          + sOrderLineTranQuantity);
      // call get complete item list to get Item details and item price
      callGetCompleteItemList();

      if (GCConstants.INT_ZERO.equals(sSubbedItemPriceType)) {
        // subbed in item price is set to its default price
        Element eleComputedPrice = GCXMLUtil.getElementByXPath(
            docGetCompleteItemListOp, "//ComputedPrice");
        if (!YFCCommon.isVoid(eleComputedPrice)) {
          sSubbedItemPrice = eleComputedPrice
              .getAttribute(GCConstants.LIST_PRICE);
        }
      } else if (GCConstants.INT_ONE.equals(sSubbedItemPriceType)) {
        // subbed in item price is set to match the replaced item price
        sSubbedItemPrice = GCXMLUtil.getAttributeFromXPath(ipDoc,
            "//OrderLine/LinePriceInfo/@UnitPrice");
      } else if (GCConstants.INT_TWO.equals(sSubbedItemPriceType)) {
        // subbed in item price is set to the manually overridden price
        sSubbedItemPrice = GCXMLUtil.getAttributeFromXPath(ipDoc,
            "OrderLine/@SubbedItemPrice");
     
      }
      LOGGER.debug(logInfo + "sSubbedItemPrice :: " + sSubbedItemPrice);

      callChangeOrderForItemSubstitution(sSubbedItemPrice, sSubbedItemID,
          sOrderLineTranQuantity, sStatusQty, sSubbedItemPriceType,
          sFulfillmentType);
    } catch (Exception e) {
      LOGGER.error("Exception during Item substitution",e);
      LOGGER.error("Mass item substitution failed for OrderNo "
          + GCXMLUtil.getAttributeFromXPath(ipDoc, "//@OrderNo"));
      createException(GCConstants.MASS_ITEM_SUB_RESULT_MESSAGE,
          GCConstants.MASS_ITEM_SUB_EXCEPTION_TYPE,
          GCConstants.MASS_ITEM_SUB_QUEUE_ID);
    }
    LOGGER.verbose(logInfo + "End");
  }

  /**
   *
   * @param sSubbedItemPrice
   * @param sSubbedItemID
   * @param sOrderLineTranQuantity
   * @param sStatusQty
   * @param sSubbedItemPriceType
   * @param sFulfillmentType
   * @throws Exception
   */
  private void callChangeOrderForItemSubstitution(String sSubbedItemPrice,
      String sSubbedItemID, String sOrderLineTranQuantity,
      String sStatusQty, String sSubbedItemPriceType,
      String sFulfillmentType) throws GCException {
    try {
      final String logInfo = "GCMassOperationProcess :: callChangeOrderForItemSubstitution :: ";
      LOGGER.info(logInfo + "Begin");

      Document docChangeOrderIp = GCXMLUtil
          .createDocument(GCConstants.ORDER);
      Element eleRootIp = docChangeOrderIp.getDocumentElement();
      eleRootIp.setAttribute(GCConstants.ORDER_HEADER_KEY, XPathAPI
          .selectSingleNode(ipDoc, "//@OrderHeaderKey")
          .getNodeValue());

      Document docGetOrderList = GCCommonUtil.invokeAPI(env,
          GCConstants.API_GET_ORDER_LIST, docChangeOrderIp);
      logOrderLength(docGetOrderList);
      String sOrderLineKey = XPathAPI.selectSingleNode(ipDoc,
          "//@OrderLineKey").getNodeValue();

      String sShippingCharge = GCXMLUtil
          .getAttributeFromXPath(
              docGetOrderList,
              "//OrderLines/OrderLine[@OrderLineKey='"
                  + sOrderLineKey
                  + "']/LineCharges/LineCharge[@ChargeName='ShippingCharge']/@ChargeAmount");
      LOGGER.debug("sShippingCharge" + sShippingCharge);
      Element eleOrderLineWarr = GCXMLUtil.getElementByXPath(
          docGetOrderList,
          "//OrderLines/OrderLine[@DependentOnLineKey='"
              + sOrderLineKey + "']");
      Element eleOrderLinesIp = docChangeOrderIp
          .createElement(GCConstants.ORDER_LINES);
      eleRootIp.appendChild(eleOrderLinesIp);

      // Append original line to cancel
      Element eleOrderLineCancelIp = docChangeOrderIp
          .createElement(GCConstants.ORDER_LINE);
      eleOrderLinesIp.appendChild(eleOrderLineCancelIp);
      eleOrderLineCancelIp.setAttribute(GCConstants.ORDER_LINE_KEY,
          XPathAPI.selectSingleNode(ipDoc, "//@OrderLineKey")
          .getNodeValue());

      Element eleOrderLineTranQuantityIp = docChangeOrderIp
          .createElement(GCConstants.ORDER_LINE_TRAN_QUANTITY);
      eleOrderLineCancelIp.appendChild(eleOrderLineTranQuantityIp);
      eleOrderLineTranQuantityIp.setAttribute(GCConstants.ORDERED_QTY,
          sOrderLineTranQuantity);

      // Append associated warranty line to cancel
      if (!YFCCommon.isVoid(eleOrderLineWarr)) {
        Element eleWarrOrderLineCancelIp = docChangeOrderIp
            .createElement(GCConstants.ORDER_LINE);
        eleOrderLinesIp.appendChild(eleWarrOrderLineCancelIp);
        String sWarOrderLineKey = eleOrderLineWarr
            .getAttribute("OrderLineKey");
        eleWarrOrderLineCancelIp.setAttribute(
            GCConstants.ORDER_LINE_KEY, sWarOrderLineKey);

        Element eleWarrOrderLineTranQuantityIp = docChangeOrderIp
            .createElement(GCConstants.ORDER_LINE_TRAN_QUANTITY);
        eleWarrOrderLineCancelIp
        .appendChild(eleWarrOrderLineTranQuantityIp);
        eleWarrOrderLineTranQuantityIp.setAttribute(
            GCConstants.ORDERED_QTY, sOrderLineTranQuantity);
      }

      // Append line for item substitution
      Element eleOrderLineSubIp = docChangeOrderIp
          .createElement(GCConstants.ORDER_LINE);
      eleOrderLinesIp.appendChild(eleOrderLineSubIp);
      eleOrderLineSubIp.setAttribute(GCConstants.ORDERED_QTY, sStatusQty);
      eleOrderLineSubIp.setAttribute(GCConstants.FULFILLMENT_TYPE,
          sFulfillmentType);

      Element eleLineCharges = docChangeOrderIp
          .createElement(GCConstants.LINE_CHARGES);
      Element eleLineCharge = docChangeOrderIp
          .createElement(GCConstants.LINE_CHARGE);

      if (!YFCCommon.isVoid(sShippingCharge)) {
        eleLineCharge.setAttribute(GCConstants.CHARGE_CATEGORY,
            GCConstants.SHIPPING);
        eleLineCharge.setAttribute(GCConstants.CHARGE_NAME,
            "ShippingCharge");
        eleLineCharge.setAttribute(GCConstants.CHARGE_PER_LINE,
            sShippingCharge);
        eleLineCharges.appendChild(eleLineCharge);
        eleOrderLineSubIp.appendChild(eleLineCharges);
      }

      Element eleItemIp = docChangeOrderIp
          .createElement(GCConstants.ITEM);
      eleOrderLineSubIp.appendChild(eleItemIp);
      eleItemIp.setAttribute(GCConstants.ITEM_ID, sSubbedItemID);
      eleItemIp
      .setAttribute(GCConstants.UNIT_OF_MEASURE, XPathAPI
          .selectSingleNode(ipDoc, "//@UnitOfMeasure")
          .getNodeValue());
      eleItemIp
      .setAttribute(GCConstants.PRODUCT_CLASS, getProductClass());
      eleItemIp.setAttribute(
          GCConstants.ITEM_SHORT_DESCRIPTION,
          XPathAPI.selectSingleNode(docGetCompleteItemListOp,
              "//@ShortDescription").getNodeValue());

      Element eleLinePriceInfo = docChangeOrderIp
          .createElement(GCConstants.LINE_PRICE_INFO);
      eleOrderLineSubIp.appendChild(eleLinePriceInfo);
      eleLinePriceInfo.setAttribute(GCConstants.IS_PRICE_LOCKED,
          GCConstants.YES);
      eleLinePriceInfo.setAttribute(GCConstants.UNIT_PRICE,
          sSubbedItemPrice);

      if (GCConstants.INT_ONE.equals(sSubbedItemPriceType)
          || GCConstants.INT_TWO.equals(sSubbedItemPriceType)) {
        Element eleNotes = docChangeOrderIp
            .createElement(GCConstants.NOTES);
        eleOrderLineSubIp.appendChild(eleNotes);

        Element eleNote = docChangeOrderIp
            .createElement(GCConstants.NOTE);
        eleNotes.appendChild(eleNote);
        eleNote.setAttribute(GCConstants.NOTE_TEXT,
            "Price Overridden due to Item Substitutions");
        eleNote.setAttribute(GCConstants.PRIORITY, GCConstants.INT_ZERO);
        eleNote.setAttribute(GCConstants.REASON_CODE,
            GCConstants.INT_TWENTY_TWO);
        eleNote.setAttribute(GCConstants.VISIBLE_TO_ALL,
            GCConstants.YES);
      }
      LOGGER.debug(logInfo + "docChangeOrderIp\n"
          + GCXMLUtil.getXMLString(docChangeOrderIp));
      GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER,
          docChangeOrderIp);
      LOGGER.verbose(logInfo + "End");
    } catch (Exception e) {
      LOGGER.error(
          "The exception is in callChangeOrderForItemSubstitution method:",
          e);
      throw new GCException(e);
    }
  }

  /**
   *
   * @return
   * @throws TransformerException
   */
  private String getProductClass() throws TransformerException {

    String sProductClass = "";
    Element eleExtn = GCXMLUtil.getElementByXPath(docGetCompleteItemListOp,
        "ItemList/Item/Extn");

    if (!YFCCommon.isVoid(eleExtn.getAttribute("ExtnSetCode"))
        && GCConstants.ONE.equals(eleExtn.getAttribute("ExtnSetCode"))) {
      sProductClass = GCConstants.SET;
    } else {
      sProductClass = GCConstants.REGULAR;
    }
    return sProductClass;
  }

  /**
   * @throws Exception
   *
   */
  private void cancelOrderLine() throws GCException {

    final String logInfo = "GCMassOperationProcess :: cancelOrderLine :: ";
    LOGGER.verbose(logInfo + "Begin");
    try {
      String sStatus = GCXMLUtil.getAttributeFromXPath(ipDoc,
          "/OrderLine/@MassOperationStatus");
      String sStatusQty = GCXMLUtil.getAttributeFromXPath(ipDoc,
          "//OrderStatuses/OrderStatus[@StatusDescription='"
              + sStatus + "']/@StatusQty");
      String sOrderedQty = GCXMLUtil.getAttributeFromXPath(ipDoc,
          "//OrderLine/@OrderedQty");
      String sOrderLineTranQuantity = String.valueOf(Double
          .valueOf(sOrderedQty) - Double.valueOf(sStatusQty));

      Document docChangeOrderIp = GCXMLUtil
          .createDocument(GCConstants.ORDER);
      Element eleChangeOrderIpRoot = docChangeOrderIp
          .getDocumentElement();
      eleChangeOrderIpRoot.setAttribute(GCConstants.ORDER_HEADER_KEY,
          GCXMLUtil.getAttributeFromXPath(ipDoc, "//@OrderHeaderKey"));

      Element eleOrderLines = docChangeOrderIp
          .createElement(GCConstants.ORDER_LINES);
      eleChangeOrderIpRoot.appendChild(eleOrderLines);

      Element eleOrderLine = docChangeOrderIp
          .createElement(GCConstants.ORDER_LINE);
      eleOrderLines.appendChild(eleOrderLine);
      eleOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY,
          GCXMLUtil.getAttributeFromXPath(ipDoc, "//@OrderLineKey"));

      Element eleOrderLineTranQuantity = docChangeOrderIp
          .createElement(GCConstants.ORDER_LINE_TRAN_QUANTITY);
      eleOrderLine.appendChild(eleOrderLineTranQuantity);
      eleOrderLineTranQuantity.setAttribute(GCConstants.ORDERED_QTY,
          sOrderLineTranQuantity);

      LOGGER.debug(logInfo + "docChangeOrderIp\n"
          + GCXMLUtil.getXMLString(docChangeOrderIp));

      GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER,
          docChangeOrderIp);

    }  catch (Exception e) {
      LOGGER.error("mass item cancellation failed", e);
      LOGGER.error("Mass item cancellation failed for OrderNo "
          + GCXMLUtil.getAttributeFromXPath(ipDoc, "//@OrderNo"));
      LOGGER.error(e.getMessage());
      createException(GCConstants.MASS_ITEM_CANCELLATION_RESULT_MESSAGE,
          GCConstants.MASS_ITEM_CANCELLATION_EXCEPTION_TYPE,
          GCConstants.MASS_ITEM_CANCELLATION_QUEUE_ID);

    }
    LOGGER.verbose(logInfo + "End");
  }



  /**
   *
   * @return
   * @throws Exception
   */
  public void callGetCompleteItemList() throws GCException {
    try {
      final String logInfo = "GCMassOperationProcess :: callGetCompleteItemList :: ";
      LOGGER.verbose(logInfo + "Begin");

      // Get complete item list ip doc
      Document docGetCompleteItemListIp = GCXMLUtil
          .createDocument(GCConstants.ITEM);
      Element eleRootGetCompleteItemListIp = docGetCompleteItemListIp
          .getDocumentElement();
      eleRootGetCompleteItemListIp.setAttribute(
          GCConstants.CALLING_ORGANIZATION_CODE, GCConstants.GC);
      eleRootGetCompleteItemListIp.setAttribute(GCConstants.ITEM_ID,
          XPathAPI.selectSingleNode(ipDoc, "//@ItemID")
          .getNodeValue());
      // Get complete item list template doc

      Document docGetCompleteItemListTemp = GCXMLUtil
          .createDocument(GCConstants.ITEM_LIST);
      Element eleRoot = docGetCompleteItemListTemp.getDocumentElement();

      Element eleItem = docGetCompleteItemListTemp
          .createElement(GCConstants.ITEM);
      eleRoot.appendChild(eleItem);
      eleItem.setAttribute(GCConstants.ITEM_ID, "");
      eleItem.setAttribute(GCConstants.UNIT_OF_MEASURE, "");

      Element elePrimaryInformation = docGetCompleteItemListTemp
          .createElement(GCConstants.PRIMARY_INFORMATION);
      eleItem.appendChild(elePrimaryInformation);
      elePrimaryInformation.setAttribute(GCConstants.SHORT_DESCRIPTION,
          "");

      Element eleComputedPrice = docGetCompleteItemListTemp
          .createElement(GCConstants.COMPUTED_PRICE);
      eleItem.appendChild(eleComputedPrice);
      eleComputedPrice.setAttribute(GCConstants.LIST_PRICE, "");

      Element eleExtn = docGetCompleteItemListTemp
          .createElement(GCConstants.EXTN);
      eleItem.appendChild(eleExtn);

      docGetCompleteItemListOp = GCCommonUtil.invokeAPI(env,
          GCConstants.API_GET_COMPLETE_ITEM_LIST,
          docGetCompleteItemListIp, docGetCompleteItemListTemp);

      logItemLength(docGetCompleteItemListOp);

      LOGGER.verbose(logInfo + "End");
    } catch (Exception e) {
      LOGGER.error("The exception is in callGetCompleteItemList method",
          e);
      throw new GCException(e);
    }

  }

  public void logItemLength(Document docGetCompleteItemListOp){
    try {
      LOGGER.info("MASSREQUEST size of item"+GCXMLUtil.getNodeListByXpath(docGetCompleteItemListOp, "/ItemList/Item").getLength());
    } catch (Exception e) {
      LOGGER.log("Exception ", e);
      LOGGER.error(e);
    }
  }

  public void logOrderLength(Document docGetCompleteItemListOp){
    try {
      LOGGER.info("MASSREQUEST size of item"+GCXMLUtil.getNodeListByXpath(docGetCompleteItemListOp, "/OrderList/Order").getLength());
    } catch (Exception e) {
      LOGGER.log("Exception ", e);
      LOGGER.error(e);
    }
  }

  /**
   *
   * @param sResultMessage
   * @param sExceptionType
   * @param sQueueID
   * @throws Exception
   */
  private void createException(String sResultMessage, String sExceptionType,
      String sQueueID) throws GCException {
    try {
      final String logInfo = "GCMassOperationProcess :: createException :: ";
      LOGGER.verbose(logInfo + "Begin");

      LOGGER.debug(logInfo + "sResultMessage :: " + sResultMessage);
      LOGGER.debug(logInfo + "sExceptionType :: " + sExceptionType);
      LOGGER.debug(logInfo + "SQueueID :: " + sQueueID);

      Document docCreateExceptionIp = GCXMLUtil
          .createDocument(GCConstants.INBOX);

      Element eleRoot = docCreateExceptionIp.getDocumentElement();

      eleRoot.setAttribute(GCConstants.EXCEPTION_TYPE, sExceptionType);
      eleRoot.setAttribute(GCConstants.QUEUE_ID, sQueueID);

      Element eleInboxReferencesList = docCreateExceptionIp
          .createElement(GCConstants.INBOX_REFERENCE_LIST);
      eleRoot.appendChild(eleInboxReferencesList);

      Element eleInboxReferences = docCreateExceptionIp
          .createElement(GCConstants.INBOX_REFERENCES);
      eleInboxReferencesList.appendChild(eleInboxReferences);
      eleInboxReferences.setAttribute(GCConstants.NAME,
          GCConstants.RESULT_MESSAGE);
      eleInboxReferences.setAttribute(GCConstants.VALUE, sResultMessage);
      eleInboxReferences.setAttribute(GCConstants.REFERENCE_TYPE,
          GCConstants.TEXT);

      Element eleOrder = docCreateExceptionIp
          .createElement(GCConstants.ORDER);
      eleRoot.appendChild(eleOrder);
      eleOrder.setAttribute(GCConstants.ORDER_NO,
          GCXMLUtil.getAttributeFromXPath(ipDoc, "//@OrderNo"));
      eleOrder.setAttribute(GCConstants.ENTERPRISE_CODE,
          GCXMLUtil.getAttributeFromXPath(ipDoc, "//@EnterpriseCode"));
      eleOrder.setAttribute(GCConstants.DOCUMENT_TYPE,
          GCXMLUtil.getAttributeFromXPath(ipDoc, "//@DocumentType"));

      LOGGER.debug(logInfo + "docCreateExceptionIp\n"
          + GCXMLUtil.getXMLString(docCreateExceptionIp));
      GCCommonUtil.invokeAPI(env, GCConstants.API_CREATE_EXCEPTION,
          docCreateExceptionIp);
    } catch (Exception e) {
      LOGGER.error("The exception is in createException method", e);
      throw new GCException(e);
    }

  }
}
