/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class post single order line element to GC mass operation post service one by one
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               04/18/2014        Kumar,Prateek		 		Initital Draft
 *#################################################################################################################################################################
 */
package com.gc.api;

import com.yantra.yfc.log.YFCLogCategory;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCMassOperationUI {

    private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCMassOperationUI.class);

    public void massOperationUI(YFSEnvironment env, Document inDoc)
	    throws GCException {
    try{	
	final String logInfo = "GCMassOperationUI :: massOperationUI :: ";
	LOGGER.verbose(logInfo + "Begin");

	LOGGER.debug(logInfo + "Input Document" + GCXMLUtil.getXMLString(inDoc));

	NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc,
		"OrderLineList/OrderLine");
	for (int i = 0; i < nlOrderLine.getLength(); i++) {
	    Element eleOrderLine = (Element) nlOrderLine.item(i);
	    Document docOrderLine = GCXMLUtil.getDocument(eleOrderLine, true);
	    GCCommonUtil.invokeService(env,
		    GCConstants.GC_MASS_OPERATION_POST_SERVICE, docOrderLine);
	}
	LOGGER.verbose(logInfo + "End");
    }catch(Exception e){
    	LOGGER.error("The  exception is in massOperationUI method:", e);
    	throw new GCException(e);
    }
	}
}
