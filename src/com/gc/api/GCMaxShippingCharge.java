/**Copyright 2013 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.

	Change Log
#####################################################################################################################################################################################################################
     OBJECTIVE: Used to Validate the Call Center Manager Approval and User Authentication
######################################################################################################################################################################################################################
        Version    CR/ User Story                 				Date                       Modified By                   Description
######################################################################################################################################################################################################################
        1.0         Call Center Return Orders                  04/02/2015                 Abhishek,Shinde	          Used to calculate Max Shipping Charge to be refunded
        
######################################################################################################################################################################################################################
 */

package com.gc.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCMaxShippingCharge implements YIFCustomApi{
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCMaxShippingCharge.class.getName());

	public Document getMaxShippingRefundCharge(YFSEnvironment env, Document inDoc){
		LOGGER.beginTimer("GCMaxShippingCharge.getMaxShippingRefundCharge");
		LOGGER.verbose("Class: GCMaxShippingCharge Method: getMaxShippingRefundCharge BEGIN");
		if (LOGGER.isDebugEnabled()) {
	        LOGGER.debug("Incoming XML:GCMaxShippingCharge Indoc" + GCXMLUtil.getXMLString(inDoc));
	      }
		
		YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
		YFCElement eleOrder =yfcInDoc.getDocumentElement();
		String sOrderHeaderKey = eleOrder.getAttribute(GCConstants.DERIVED_FROM_ORDER_HEADER_KEY);
		String rOrderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
		YFCDocument tempDocForGetOrderList =
        YFCDocument
        .getDocumentFor("<Order> <OrderLines><OrderLine OrderLineKey='' KitCode='' BundleParentOrderLineKey=''> <Item ItemID='' ProductClass=''/> <Extn> <GCOrderLineComponentsList> <GCOrderLineComponents/> </GCOrderLineComponentsList> </Extn> <ItemDetails ItemID=''><PrimaryInformation DefaultProductClass=''/><Components><Component><Item ItemID=''></Item></Component></Components></ItemDetails><LineCharges><LineCharge/></LineCharges></OrderLine></OrderLines>"
            + " <ReturnOrders><ReturnOrder><OrderLines><OrderLine><LineCharges><LineCharge/></LineCharges></OrderLine></OrderLines></ReturnOrder></ReturnOrders></Order>");
   

		YFCDocument inputDoc = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderHeaderKey+"'/>");
		LOGGER.verbose("Input for getCompleteOrderDetails"+inputDoc.toString());
		YFCDocument soOrderDetailsDoc = GCCommonUtil.invokeAPI(env, "getCompleteOrderDetails", inputDoc, tempDocForGetOrderList);
		Document docOrderDetailsDoc=soOrderDetailsDoc.getDocument();
		//GCSTORE-5934-starts
		@SuppressWarnings("unchecked")
        List<Element> lstOrderLines= GCXMLUtil.getElementListByXpath(docOrderDetailsDoc, "/Order/OrderLines/OrderLine");
		
		
		for(int i=0;i<lstOrderLines.size();i++)
		{
		  
		    Element eleOrderLine=lstOrderLines.get(i);
		    LOGGER.debug("GCMaxShippingCharge :: getMaxShippingRefundCharge :: eleOrderLine ::\n"+GCXMLUtil.getElementString(eleOrderLine));
		    
		    String xml=GCXMLUtil.getElementString(eleOrderLine);
		    YFCElement yfcEleOrderLine=YFCDocument.getDocumentFor(xml).getDocumentElement();
		    YFCElement yfcEleItem=yfcEleOrderLine.getChildElement("Item");
		    String strProductClass=null;
		    if(!YFCCommon.isVoid(yfcEleItem))
		    {
		        strProductClass=yfcEleItem.getAttribute("ProductClass");
		          
		    }
		    //checking if it is a kit component
		    if(!YFCCommon.isVoid(yfcEleOrderLine.getAttribute("BundleParentOrderLineKey")) && !YFCCommon.isVoid(strProductClass) && GCConstants.REGULAR.equals(strProductClass))
		    {
		    LOGGER.debug("GCMaxShippingCharge :: getMaxShippingRefundCharge :: ProductClass :: "+strProductClass); 
		    GCGetEligibleAppeasementAmount objApp=new GCGetEligibleAppeasementAmount();
		    Double amtShippingCharge= objApp.calculateAmountForKitComponent(yfcEleOrderLine, GCConstants.SHIPPING_CHARGE, GCConstants.SHIPPING); 
		    
		    if(amtShippingCharge>0)
		    {
		    Element eleSOLineCharges = (Element) eleOrderLine.getElementsByTagName(GCConstants.LINE_CHARGES).item(0);
		   
            Element eleSOLineCharge= docOrderDetailsDoc.createElement(GCConstants.LINE_CHARGE);
            eleSOLineCharge.setAttribute(GCConstants.CHARGE_CATEGORY, GCConstants.SHIPPING);
            eleSOLineCharge.setAttribute(GCConstants.CHARGE_NAME, GCConstants.SHIPPING_CHARGE);
            eleSOLineCharge.setAttribute(GCConstants.CHARGE_AMOUNT, amtShippingCharge.toString());
            eleSOLineCharges.appendChild(eleSOLineCharge);
		    }
            
            LOGGER.debug("GCMaxShippingCharge :: getMaxShippingRefundCharge :: eleOrderLine After Modification ::\n"+GCXMLUtil.getElementString(eleOrderLine));
            
		   }
          }
		
		LOGGER.debug("GCMaxShippingCharge :: getMaxShippingRefundCharge :: soOrderDetailsDoc::"+soOrderDetailsDoc.getString());
		//GCSTORE-5934-ends
		YFCElement eleSOOrderLines = soOrderDetailsDoc.getDocumentElement().getChildElement(GCConstants.ORDER_LINES);
		YFCElement eleReturnOrders = soOrderDetailsDoc.getDocumentElement().getChildElement("ReturnOrders");
		 List<String> returnOrderLines = getOrderLinesInReturn(eleReturnOrders, rOrderHeaderKey);
		YFCNodeList<YFCElement> nlSOOrderLines = eleSOOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
		Map<String, Double> maxShippingCharge = new HashMap<String, Double>();
		Map<String, String> componentMap = new HashMap<String, String>();
		YFCDocument returnDoc = YFCDocument.createDocument("OrderLineList");
		YFCElement eleOrderLineList = returnDoc.getDocumentElement();
		
 		for(YFCElement eleSOOrderLine : nlSOOrderLines){
 			double roChargeAmount = 0.0;
			double chargeAmount = 0.0;
			String orderLineKey = null; 
			List<String> components = new ArrayList<String>();
			String soOrderLineKey = eleSOOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
			//GCTORE-5934--Start
			orderLineKey=soOrderLineKey;
            //GCTORE-5934--End
			String parentLineKey = eleSOOrderLine.getAttribute(GCConstants.BUNDLE_PARENT_ORDERLINEKEY);
            YFCElement eleItemDetails = eleSOOrderLine.getChildElement(GCConstants.ITEM_DETAILS);
            YFCElement elePrimaryInfo = eleItemDetails.getChildElement(GCConstants.PRIMARY_INFORMATION);
            String prodClass = elePrimaryInfo.getAttribute("DefaultProductClass");
		    String kitCode = eleSOOrderLine.getAttribute(GCConstants.KIT_CODE);
      if (YFCUtils.equals(GCConstants.BUNDLE, kitCode) && YFCUtils.equals(GCConstants.REGULAR, prodClass)) {
        YFCElement eleComponents = eleItemDetails.getChildElement(GCConstants.COMPONENTS);
        if (!YFCCommon.isVoid(eleComponents)) {
          components =
              getComponentOrderLineKey(nlSOOrderLines, eleComponents, soOrderLineKey, returnOrderLines, componentMap);
        }
      }
			YFCNodeList<YFCElement> nlSOLineCharges = eleSOOrderLine.getElementsByTagName(GCConstants.LINE_CHARGE);
			for(YFCElement eleSOLineCharge : nlSOLineCharges){
				
				String chargeCategory = eleSOLineCharge.getAttribute(GCConstants.CHARGE_CATEGORY);
				String chargeName = eleSOLineCharge.getAttribute(GCConstants.CHARGE_NAME);
				if(YFCCommon.equals(GCConstants.SHIPPING, chargeCategory) && YFCCommon.equals(GCConstants.SHIPPING_CHARGE, chargeName)){
					 chargeAmount = eleSOLineCharge.getDoubleAttribute(GCConstants.CHARGE_AMOUNT);
					
				}
			}
			if(!YFCCommon.isVoid(eleReturnOrders)){
				YFCNodeList<YFCElement> nlROOrderLines = eleReturnOrders.getElementsByTagName(GCConstants.ORDER_LINE);
			
			for(YFCElement eleROOrderLine : nlROOrderLines){
				String sRODerivedOrderLineKey = eleROOrderLine.getAttribute(GCConstants.DERIVED_FROM_ORDER_LINE_KEY);
				
				if(YFCCommon.equals(soOrderLineKey, sRODerivedOrderLineKey)){
					if (YFCCommon.isVoid(parentLineKey)) {
              orderLineKey = sRODerivedOrderLineKey;
              YFCNodeList<YFCElement> nlLineCharges = eleROOrderLine.getElementsByTagName(GCConstants.LINE_CHARGE);
              for (YFCElement eleROLineCharge : nlLineCharges) {
                String roChargeCategory = eleROLineCharge.getAttribute(GCConstants.CHARGE_CATEGORY);
                String roChargeName = eleROLineCharge.getAttribute(GCConstants.CHARGE_NAME);
                if (YFCCommon.equals(GCConstants.SHIPPING_REFUND, roChargeCategory)
                    && YFCCommon.equals(GCConstants.SHIPPING_REFUND, roChargeName)) {
                  roChargeAmount = roChargeAmount + eleROLineCharge.getDoubleAttribute(GCConstants.CHARGE_AMOUNT);
                }
              }
            }
				}
				    else if (components.contains(sRODerivedOrderLineKey)) {
            orderLineKey = soOrderLineKey;
            YFCNodeList<YFCElement> nlLineCharges = eleROOrderLine.getElementsByTagName(GCConstants.LINE_CHARGE);
            for (YFCElement eleROLineCharge : nlLineCharges) {
              String roChargeCategory = eleROLineCharge.getAttribute(GCConstants.CHARGE_CATEGORY);
              String roChargeName = eleROLineCharge.getAttribute(GCConstants.CHARGE_NAME);
              if (YFCCommon.equals(GCConstants.SHIPPING_REFUND, roChargeCategory)
                  && YFCCommon.equals(GCConstants.SHIPPING_REFUND, roChargeName)) {
                roChargeAmount = roChargeAmount + eleROLineCharge.getDoubleAttribute(GCConstants.CHARGE_AMOUNT);
              }
            }
          }
			}
			
			}
			double shippingCharge = chargeAmount - roChargeAmount;
			if(!YFCCommon.isVoid(orderLineKey)){
				maxShippingCharge.put(orderLineKey, shippingCharge);
				
			}
		}
 		YFCDocument inDocForGetInvoiceDetails = YFCDocument.getDocumentFor("<OrderInvoiceDetail><InvoiceHeader InvoiceType='CREDIT_MEMO'> <Order OrderHeaderKey='"+ sOrderHeaderKey+"'/> </InvoiceHeader></OrderInvoiceDetail>");
 		YFCDocument templateDoc = null;
 		
 		YFCDocument invoiceDetailsOutDoc = GCCommonUtil.invokeAPI(env, "getOrderInvoiceDetailList", inDocForGetInvoiceDetails, templateDoc);
 		LOGGER.verbose("Output of getOrderInvoiceDetailList"+invoiceDetailsOutDoc.toString());
 		YFCElement eleOrderInvoiceDetailsList = invoiceDetailsOutDoc.getDocumentElement();
 		YFCNodeList<YFCElement> nlOrderInvoiceDetails  = eleOrderInvoiceDetailsList.getElementsByTagName("OrderInvoiceDetail");
 		if(!YFCCommon.isVoid(nlOrderInvoiceDetails)){
 		for(YFCElement eleOrderInvoiceDetail : nlOrderInvoiceDetails){
 			double chargeAmount = 0.0;
 			String sInvoiceOrderLineKey = eleOrderInvoiceDetail.getAttribute(GCConstants.ORDER_LINE_KEY);
 			YFCNodeList<YFCElement> nlLineCharges = eleOrderInvoiceDetail.getElementsByTagName(GCConstants.LINE_CHARGE);
 			for(YFCElement eleLineCharge : nlLineCharges){
 				String chargeCategory = eleLineCharge.getAttribute(GCConstants.CHARGE_CATEGORY);
				String chargeName = eleLineCharge.getAttribute(GCConstants.CHARGE_NAME);
				if(YFCCommon.equals("CUSTOMER_APPEASEMENT", chargeCategory) && YFCCommon.equals("SHIPPING_APPEASEMENT", chargeName)){
					 chargeAmount = eleLineCharge.getDoubleAttribute(GCConstants.CHARGE_AMOUNT);
					 LOGGER.info(chargeAmount);
					
				}
 			}
 			double maxAmount = maxShippingCharge.get(sInvoiceOrderLineKey);
 			maxAmount = maxAmount - chargeAmount;
 			maxShippingCharge.put(sInvoiceOrderLineKey, maxAmount);

			
 		}
 		}		
 		//GCSTORE-5934-starts
 		for(Map.Entry<String, Double> pair : maxShippingCharge.entrySet()){
 			String olK = pair.getKey();
 			double charge =  pair.getValue();
			 /* if (!componentMap.containsValue(olK)) {*/
 			YFCElement eleOrderLine = eleOrderLineList.createChild(GCConstants.ORDER_LINE);
			eleOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, olK);
			eleOrderLine.setAttribute("MaxShippingCharge", charge);
			  //}
			  }
		    /*for (Map.Entry<String, String> comp : componentMap.entrySet()) {
      String parentKey = comp.getKey();
      String ordLinekey = comp.getValue();
      double charge = maxShippingCharge.get(parentKey);
      YFCElement eleOrderLine = eleOrderLineList.createChild(GCConstants.ORDER_LINE);
      eleOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, ordLinekey);
      eleOrderLine.setAttribute("MaxShippingCharge", charge);

    }*/
 	//GCSTORE-5934-ends	
 		LOGGER.endTimer("GCMaxShippingCharge.getMaxShippingRefundCharge");
 		LOGGER.debug("GCMaxShippingCharge.getMaxShippingRefundCharge :: returnDoc::\n "+returnDoc.getString());
		return returnDoc.getDocument();
		
	}

	
  private List<String> getOrderLinesInReturn(YFCElement eleReturnOrders, String rOrderHeaderKey) {
    List<String> returnLinesList = new ArrayList<String>();
    if (!YFCCommon.isVoid(eleReturnOrders)) {
      YFCNodeList<YFCElement> nlReturnOrders = eleReturnOrders.getElementsByTagName("ReturnOrder");
      for (YFCElement eleRetOrd : nlReturnOrders) {
        String orderHeaderKey = eleRetOrd.getAttribute(GCConstants.ORDER_HEADER_KEY);
        if (YFCUtils.equals(rOrderHeaderKey, orderHeaderKey)) {
          YFCElement eleOrderLines = eleRetOrd.getChildElement(GCConstants.ORDER_LINES);
          YFCNodeList<YFCElement> nlReturnOrderLines = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
          for (YFCElement eleOrderLine : nlReturnOrderLines) {
            String derivedLineKey = eleOrderLine.getAttribute(GCConstants.DERIVED_FROM_ORDER_LINE_KEY);
            returnLinesList.add(derivedLineKey);
          }
        }
      }
    }
    return returnLinesList;
  }


  private List<String> getComponentOrderLineKey(YFCNodeList<YFCElement> nlSOOrderLines, YFCElement eleComponents,
      String soOrderLineKey, List<String> returnOrderLines, Map<String, String> componentMap) {
    boolean comp=false;
    List<String> components = new ArrayList<String>();
    YFCNodeList<YFCElement> nlComponent = eleComponents.getElementsByTagName(GCConstants.COMPONENT);
    for (YFCElement eleComponent : nlComponent) {
      YFCElement eleItem = eleComponent.getChildElement(GCConstants.ITEM);
      String compItemId = eleItem.getAttribute(GCConstants.ITEM_ID);
      for (YFCElement eleOrderLine : nlSOOrderLines) {
        String bundleLineKey = eleOrderLine.getAttribute(GCConstants.BUNDLE_PARENT_ORDERLINEKEY);
        YFCElement eleItemDetails = eleOrderLine.getChildElement(GCConstants.ITEM_DETAILS);
        String itemId = eleItemDetails.getAttribute(GCConstants.ITEM_ID);
        if (YFCUtils.equals(soOrderLineKey, bundleLineKey) && YFCUtils.equals(itemId, compItemId)) {
          String orderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
          if(returnOrderLines.contains(orderLineKey) && !comp){
            componentMap.put(soOrderLineKey, orderLineKey);
            comp = true;
          }
          components.add(orderLineKey);
        }
      }
    }
    return components;
  }
  
	public void setProperties(Properties prop) {
		
		
	}
}
