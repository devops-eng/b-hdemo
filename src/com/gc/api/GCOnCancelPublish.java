/**
 * Copyright 2014 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  ######################################################################################################################################
 *   OBJECTIVE: This class is used to publish Invoice data to DAx on Cancel..
 *  ######################################################################################################################################
 *        Version    Date          Modified By           CR                Description
 *  ######################################################################################################################################
 *        1.0       15/04/2014     Soni, Karan                             Initial Version Publish Invoice to DAX in case of
 *        																	Prepaid order cancel.
 *   
 * *  ######################################################################################################################################
 */

package com.gc.api;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author Soni Karan
 */
public class GCOnCancelPublish {

    // Initialize Logger
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCOnCancelPublish.class);

    public void createCancelData(YFSEnvironment env, Document inDoc)
	    throws GCException {
	try {
	    LOGGER.verbose("Entering the createCancelData method");
	    Element eleOrder = inDoc.getDocumentElement();
	    String strEnterpriseCode = eleOrder
		    .getAttribute(GCConstants.ENTERPRISE_CODE);
	    String strOrderHeaderKey = eleOrder
		    .getAttribute(GCConstants.ORDER_HEADER_KEY);
	    LOGGER.debug("The strEnterpriseCode is:" + strEnterpriseCode
		    + "strOrderHeaderKey" + strOrderHeaderKey);
	    Element elePaymentMethod = GCXMLUtil
		    .getElementByXPath(
			    inDoc,
			    "Order/ChargeTransactionDetails/ChargeTransactionDetail[@ChargeType='CANCEL' and @Status='OPEN']");
	    if (!YFCCommon.isVoid(elePaymentMethod)) {
	    	if (LOGGER.isDebugEnabled()) {	
	    		LOGGER.debug("The element with open charge is"
	    				+ GCXMLUtil.getElementString(elePaymentMethod));
	    		}
		env.setTxnObject("IsPrepaidCancel", "Y");
		String strReqCollection = "<Order OrderHeaderKey='"
			+ strOrderHeaderKey + "'/>";
		Document inDocRC = GCXMLUtil.getDocument(strReqCollection);
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("The request collection input document"
					+ GCXMLUtil.getXMLString(inDocRC));
			}
		GCCommonUtil.invokeAPI(env, GCConstants.REQUEST_COLLECTION,
			inDocRC);
	    }
	    LOGGER.verbose("Exiting the createCancelData method");
	} catch (Exception e) {
	    LOGGER.error("Inside GCOnCancelPublish.createCancelData(): ",e);
	    throw new GCException(e);
	}

    }

}
