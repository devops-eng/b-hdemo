/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               06/02/2014        Singh, Gurpreet            OMS-1465: Order Release
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:gurpreet.singh@expicient.com">Gurpreet</a>
 */
public class GCOnReleaseCreationOrChange implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCOnReleaseCreationOrChange.class);

  /**
   *
   * Description of onReleaseCreationOrChange This method will concat
   * SalesOrder, RelaseNo in the format 'SalesOrder+R+ReleaseNo' and also
   * fires changeRelease API to append it on order
   *
   * @param env
   * @param inDoc
   * @throws GCException
   *
   */
  public Document onReleaseCreationOrChange(YFSEnvironment env, Document inDoc)
      throws GCException {
    LOGGER.debug(" Entering GCOnReleaseCreationOrChange.onReleaseCreationOrChange() method ");

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" onReleaseCreationOrChange() method, Input document is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    try {

      // OMS-4892 : CR:80 Start
      stampStatusQuantityOnSetBundleLine(inDoc);

      // GCSTORE-3174
      //  stampStoreID(inDoc);
      // GCSTORE-3174

      // OMS-4892 : CR:80 End

      // Fix for GCSTORE#3795-- Starts
      List<String> parentLineKeyList = new ArrayList<String>();
      prepareBundleParentLineKeyList(inDoc, parentLineKeyList);
      LOGGER.verbose("parentLineKeyList" + parentLineKeyList);
      // Fix for GCSTORE#3795-- Ends

      Element eleRootEleOfInDoc = inDoc.getDocumentElement();
      String sSalesOrderNo = eleRootEleOfInDoc
          .getAttribute(GCConstants.SALES_ORDER_NO);
      String sReleaseNo = eleRootEleOfInDoc
          .getAttribute(GCConstants.RELEASE_NO);
      String sEnterpriseCode = eleRootEleOfInDoc
          .getAttribute(GCConstants.ENTERPRISE_CODE);
      Element eleExtn = (Element) XPathAPI.selectSingleNode(
          eleRootEleOfInDoc, GCConstants.EXTN);
      if (YFCCommon.isVoid(eleExtn)) {
        eleExtn = GCXMLUtil
            .createElement(inDoc, GCConstants.EXTN, null);
        eleRootEleOfInDoc.appendChild(eleExtn);
      }
      String sExtnPickRequestNo = sSalesOrderNo + "R" + sReleaseNo;
      eleExtn.setAttribute(GCConstants.EXTN_PICK_REQUEST_NO,
          sExtnPickRequestNo);
      LOGGER.debug(" sSalesOrderNo " + sSalesOrderNo + " sReleaseNo "
          + sReleaseNo + " sExtnPickRequestNo " + sExtnPickRequestNo);

      Document docChangeReleaseIP = GCXMLUtil
          .createDocument(GCConstants.ORDER_RELEASE);
      Element eleChangeRelease = docChangeReleaseIP.getDocumentElement();
      eleChangeRelease.setAttribute(GCConstants.ACTION,
          GCConstants.MODIFY);
      eleChangeRelease
      .setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
      eleChangeRelease.setAttribute(GCConstants.ORDER_NO, sSalesOrderNo);
      eleChangeRelease.setAttribute(GCConstants.ENTERPRISE_CODE,
          sEnterpriseCode);
      eleChangeRelease.setAttribute(GCConstants.DOCUMENT_TYPE,
          GCConstants.SALES_ORDER_DOCUMENT_TYPE);
      eleChangeRelease.setAttribute(GCConstants.RELEASE_NO,
          eleRootEleOfInDoc.getAttribute(GCConstants.RELEASE_NO));
      eleChangeRelease.setAttribute(GCConstants.SHIP_NODE,
          eleRootEleOfInDoc.getAttribute(GCConstants.SHIP_NODE));

      Element eleExtnRelease = GCXMLUtil.createElement(
          docChangeReleaseIP, GCConstants.EXTN, null);
      eleExtnRelease.setAttribute(GCConstants.EXTN_PICK_REQUEST_NO,
          sExtnPickRequestNo);
      eleChangeRelease.appendChild(eleExtnRelease);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(" docChangeReleaseIP document is"
            + GCXMLUtil.getXMLString(docChangeReleaseIP));
      }
      GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_RELEASE,
          docChangeReleaseIP);

      List<Element> nlOrderLine = GCXMLUtil.getElementListByXpath(inDoc,
          "OrderRelease/OrderLine");
      Iterator<Element> listIterator = nlOrderLine.iterator();
      while (listIterator.hasNext()) {
        Element eleOrderLine = listIterator.next();

        String sDependentOnLineKey = eleOrderLine
            .getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
        String sExtnIsWarrantyItem = GCXMLUtil.getAttributeFromXPath(
            eleOrderLine, "Extn/@ExtnIsWarrantyItem");
        if (!YFCCommon.isVoid(sDependentOnLineKey)
            && GCConstants.YES
            .equalsIgnoreCase(sExtnIsWarrantyItem)) {
          Element eleDependentLine = GCXMLUtil.getElementByXPath(
              inDoc, "OrderRelease/OrderLine[@OrderLineKey='"
                  + sDependentOnLineKey + "']");
          // check if it is a warranty line and
          // its dependent parent line is not present in the release
          // it means parent line is having ExtnShipAlone='Y'
          // remove the warranty line from the XML
          if (YFCCommon.isVoid(eleDependentLine) && !parentLineKeyList.contains(sDependentOnLineKey)) {
            GCXMLUtil.removeChild(inDoc.getDocumentElement(),
                eleOrderLine);
          } else {
            // else it means parent line is present in the release
            // set quantity as 0 at the warranty line
            eleOrderLine.setAttribute(GCConstants.ORDERED_QTY,
                GCConstants.ZERO_AMOUNT);
            eleOrderLine.setAttribute(GCConstants.STATUS_QUANTITY,
                GCConstants.ZERO_AMOUNT);
          }
        }

        String sExtnNonHJOrderLine = GCXMLUtil.getAttributeFromXPath(
            eleOrderLine, "Extn/@ExtnNonHJOrderLine");
        if (GCConstants.YES.equalsIgnoreCase(sExtnNonHJOrderLine)) {

          Document docGetOrderLineListIP = GCXMLUtil
              .createDocument(GCConstants.ORDER_LINE);
          docGetOrderLineListIP.getDocumentElement().setAttribute(
              GCConstants.ORDER_LINE_KEY, sDependentOnLineKey);
          Document docGetOrderLineListTemplate = GCXMLUtil
              .getDocument(GCConstants.GET_ORDER_LINE_LIST_TEMP);
          Document docGetOrderLineListOP = GCCommonUtil.invokeAPI(
              env, GCConstants.API_GET_ORDER_LINE_LIST,
              docGetOrderLineListIP, docGetOrderLineListTemplate);
          String sMinLineStatus = GCXMLUtil.getAttributeFromXPath(
              docGetOrderLineListOP,
              "OrderLineList/OrderLine/@MinLineStatus");
          if (GCConstants.RELEASED_STATUS
              .equalsIgnoreCase(sMinLineStatus)) {
            String sShipNode = GCXMLUtil
                .getAttributeFromXPath(docGetOrderLineListOP,
                    "OrderLineList/OrderLine/OrderStatuses/OrderStatus/@ShipNode");
            eleOrderLine.setAttribute(GCConstants.SHIP_NODE,
                sShipNode);

          } else if (GCConstants.SHIPPED_STATUS
              .equalsIgnoreCase(sMinLineStatus)
              || "3700.005".equalsIgnoreCase(sMinLineStatus) || "3700.006".equalsIgnoreCase(sMinLineStatus)
              || "3700.007".equalsIgnoreCase(sMinLineStatus) || "3700.008".equalsIgnoreCase(sMinLineStatus)) {
            String sShipNode = GCXMLUtil.getAttributeFromXPath(
                docGetOrderLineListOP,
                "OrderLineList/OrderLine/@ShipNode");
            eleOrderLine.setAttribute(GCConstants.SHIP_NODE,
                sShipNode);

            handleShippedParentWarranty(env, inDoc, eleOrderLine);
          }
        }
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCOnReleaseCreationOrChange.onReleaseCreationOrChange()",
          e);
      throw new GCException(e);
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" Updated input document is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    LOGGER.debug(" Exiting GCOnReleaseCreationOrChange.onReleaseCreationOrChange() method ");

    return inDoc;
  }

  /**
   * This method is used to create & confirm Shipment of warranty lines whose parent line is already
   * shipped. And then creating shipment invoice for the new shipment.
   *
   * @param inDoc
   * @param env
   * @param eleOrderLine
   */
  private void handleShippedParentWarranty(YFSEnvironment env, Document inDoc, Element eleOrderLine) {

    LOGGER.beginTimer("GCOnReleaseCreationOrChange.handleShippedParentWarranty");
    LOGGER.verbose("GCOnReleaseCreationOrChange.handleShippedParentWarranty--Starts");

    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    LOGGER.verbose("inputDoc : " + inputDoc.toString());
    YFCElement eleIndocRoot = inputDoc.getDocumentElement();

    String orderNo = eleIndocRoot.getAttribute(GCConstants.SALES_ORDER_NO);
    String shipNode = eleIndocRoot.getAttribute(GCConstants.SHIP_NODE);
    String releaseNo = eleIndocRoot.getAttribute(GCConstants.RELEASE_NO);
    String odrHdrKey = eleIndocRoot.getAttribute(GCConstants.ORDER_HEADER_KEY);
    YFCDocument confirmShpIp = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement eleRoot = confirmShpIp.getDocumentElement();
    eleRoot.setAttribute(GCConstants.DOCUMENT_TYPE, GCConstants.ZERO_ZERO_ZERO_ONE);
    eleRoot.setAttribute(GCConstants.ENTERPRISE_CODE, GCConstants.GC);
    eleRoot.setAttribute(GCConstants.SHIP_NODE, shipNode);
    eleRoot.setAttribute(GCConstants.BACKORDER_NON_SHIPPED_QTY, GCConstants.YES);
    YFCElement eleShipmentlinesConfShp = eleRoot.createChild(GCConstants.SHIPMENT_LINES);
    String primeLineNo = eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
    String subLineNo = eleOrderLine.getAttribute(GCConstants.SUB_LINE_NO);
    String qty = eleOrderLine.getAttribute(GCConstants.ORDERED_QTY);

    Element eleItem = (Element) eleOrderLine.getElementsByTagName(GCConstants.ITEM).item(0);
    String itemId = eleItem.getAttribute(GCConstants.ITEM_ID);
    String productClass = eleItem.getAttribute(GCConstants.PRODUCT_CLASS);
    String uom = eleItem.getAttribute(GCConstants.UNIT_OF_MEASURE);
    YFCElement eleShipmentLine = eleShipmentlinesConfShp.createChild(GCConstants.SHIPMENT_LINE);
    eleShipmentLine.setAttribute(GCConstants.PRIME_LINE_NO, primeLineNo);
    eleShipmentLine.setAttribute(GCConstants.ORDER_NO, orderNo);
    eleShipmentLine.setAttribute(GCConstants.ORDER_HEADER_KEY, odrHdrKey);
    eleShipmentLine.setAttribute(GCConstants.QUANTITY, qty);
    eleShipmentLine.setAttribute(GCConstants.ITEM_ID, itemId);
    eleShipmentLine.setAttribute(GCConstants.PRODUCT_CLASS, productClass);
    eleShipmentLine.setAttribute(GCConstants.SUB_LINE_NO, subLineNo);
    eleShipmentLine.setAttribute(GCConstants.UNIT_OF_MEASURE, uom);
    eleShipmentLine.setAttribute(GCConstants.RELEASE_NO, releaseNo);

    YFCElement eleContainers = eleRoot.createChild(GCConstants.CONTAINERS);
    YFCElement eleContainer = eleContainers.createChild(GCConstants.CONTAINER);
    YFCElement eleContainerDtls = eleContainer.createChild(GCConstants.CONTAINER_DETAILS);
    YFCElement eleContainerDtl = eleContainerDtls.createChild(GCConstants.CONTAINER_DETAIL);
    eleContainerDtl.setAttribute(GCConstants.QUANTITY, qty);
    YFCElement eleContainerShipLine = eleContainerDtl.createChild(GCConstants.SHIPMENT_LINE);
    eleContainerShipLine.setAttribute(GCConstants.PRIME_LINE_NO, primeLineNo);
    eleContainerShipLine.setAttribute(GCConstants.ORDER_NO, orderNo);
    eleContainerShipLine.setAttribute(GCConstants.ORDER_HEADER_KEY, odrHdrKey);
    eleContainerShipLine.setAttribute(GCConstants.ITEM_ID, itemId);
    eleContainerShipLine.setAttribute(GCConstants.PRODUCT_CLASS, productClass);
    eleContainerShipLine.setAttribute(GCConstants.SUB_LINE_NO, subLineNo);
    eleContainerShipLine.setAttribute(GCConstants.UNIT_OF_MEASURE, uom);
    eleContainerShipLine.setAttribute(GCConstants.RELEASE_NO, releaseNo);

    LOGGER.verbose("confirmShpIp: " + confirmShpIp.toString());
    YFCDocument opTemp = YFCDocument.getDocumentFor(GCConstants.CONFIRM_SHIP_TEMP);
    YFCDocument confirmShpOp = GCCommonUtil.invokeAPI(env, GCConstants.CONFIRM_SHIPMENT_API, confirmShpIp, opTemp);
    YFCElement eleShpInvoiceIp = confirmShpOp.getDocumentElement();
    eleShpInvoiceIp.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.CREATE_SHPMNT_INVC);
    YFCDocument createShpInvpiceOpTemp = YFCDocument.createDocument(GCConstants.API_SUCCESS);

    LOGGER.verbose("confirmShpOp: " + confirmShpOp.toString());
    GCCommonUtil.invokeAPI(env, GCConstants.CREATE_SHPMNT_INVC_API, confirmShpOp, createShpInvpiceOpTemp);

    LOGGER.verbose("GCOnReleaseCreationOrChange.handleShippedParentWarranty--Ends");
    LOGGER.endTimer("GCOnReleaseCreationOrChange.handleShippedParentWarranty");
  }

  /**
   * This method is used to prepare a list of BundleParentLineKey in the current release.
   *
   * @param inDoc
   * @param parentLineKeyList
   * @return
   */
  private void prepareBundleParentLineKeyList(Document inDoc, List<String> parentLineKeyList) {

    LOGGER.debug(" Entering GCOnReleaseCreationOrChange.prepareBundleParentLineKeyList() method ");
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleOdrRelease = inputDoc.getDocumentElement();
    YFCNodeList<YFCElement> nlOdrLine = eleOdrRelease.getElementsByTagName(GCConstants.ORDER_LINE);
    for (YFCElement eleOdrLine : nlOdrLine) {
      String parentLineKey = eleOdrLine.getAttribute(GCConstants.BUNDLE_PARENT_ORDERLINEKEY);
      if (!YFCCommon.isVoid(parentLineKey) && !parentLineKeyList.contains(parentLineKey)) {
        parentLineKeyList.add(parentLineKey);
      }
    }
    LOGGER.debug(" Exiting GCOnReleaseCreationOrChange.prepareBundleParentLineKeyList() method ");
  }

  private void stampStoreID(Document inDoc) {
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleRoot = inputDoc.getDocumentElement();
    YFCElement eleOrder = eleRoot.getChildElement(GCConstants.ORDER);
    YFCElement eleOrdExtn = eleOrder.getChildElement(GCConstants.EXTN);
    if (!YFCCommon.isVoid(eleOrdExtn)) {
      String pickUpStore = eleOrdExtn.getAttribute(GCConstants.EXTN_PICK_UP_STORE_ID);
      if (!YFCCommon.isVoid(pickUpStore)) {
        YFCElement elePersonInfoShipTo = eleRoot.getChildElement(GCConstants.PERSON_INFO_SHIP_TO);
        elePersonInfoShipTo.setAttribute(GCConstants.COMPANY, pickUpStore);
      }
    }
  }

  private static Document prepareOrderInvoiceDoc(Document docChangeOrderStatus)
      throws GCException {
    LOGGER.debug(" Entering GCProcessConfirmShipmentAPI.prepareOrderInvoiceDoc() method ");
    Document docCreateOrderInvoice = null;
    try {
      docCreateOrderInvoice = GCXMLUtil.createDocument(GCConstants.ORDER);
      Element eleCreateOrderInvoice = docCreateOrderInvoice
          .getDocumentElement();
      eleCreateOrderInvoice.setAttribute(
          GCConstants.ORDER_HEADER_KEY,
          docChangeOrderStatus.getDocumentElement().getAttribute(
              GCConstants.ORDER_HEADER_KEY));
      eleCreateOrderInvoice.setAttribute(GCConstants.TRANSACTION_ID,
          "CREATE_SO_INVOICE.0001.ex");
      eleCreateOrderInvoice.setAttribute("IgnoreStatusCheck",
          GCConstants.YES);
      eleCreateOrderInvoice.setAttribute("IgnoreTransactionDependencies",
          GCConstants.YES);

      Element eleOLsChgOrderStatus = (Element) XPathAPI.selectSingleNode(
          docChangeOrderStatus, "OrderStatusChange/OrderLines");

      Element eleOLsOrderInvoice = GCXMLUtil.createElement(
          docCreateOrderInvoice, GCConstants.ORDER_LINES, null);
      eleCreateOrderInvoice.appendChild(eleOLsOrderInvoice);

      GCXMLUtil.copyElement(docCreateOrderInvoice, eleOLsChgOrderStatus,
          eleOLsOrderInvoice);

    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCProcessConfirmShipmentAPI.prepareOrderInvoiceDoc(), Exception is :",
          e);
      throw new GCException(e);
    }
    return docCreateOrderInvoice;
  }

  // OMS-4892 : CR:80 Start
  /**
   *
   * This method will stamp StatusQuantity on BundleLines with ProductClass as SET. The status quantity will be based on the ratio of any component.
   *
   * @param env
   * @param inDoc
   *
   */
  private static void stampStatusQuantityOnSetBundleLine(Document inDoc){

    LOGGER.info("Inside  stampStatusQuantityOnSetBundleLine");
    List<Element> nlBundleParentOrderLine = GCXMLUtil.getElementListByXpath(inDoc,
        "OrderRelease/BundleParentOrderLine[@KitCode='BUNDLE']");

    for(Element eleBundleParentOrderLine : nlBundleParentOrderLine) {
      Element eleItem = (Element) eleBundleParentOrderLine.getElementsByTagName(GCConstants.ITEM).item(0);

      String sProductClass = eleItem.getAttribute(GCConstants.PRODUCT_CLASS);
      LOGGER.debug("Check if product class is SET. Then only stamp the status qty.");
      if(YFCCommon.equals(sProductClass, GCConstants.PRODUCT_CLASS_SET, true)){

        LOGGER.debug("Getting any one component orderline of the BundleParentOrderLine");
        String sBundleOrderLineKey = eleBundleParentOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
        Element eleOrderLine = GCXMLUtil.getElementByXPath(inDoc, "OrderRelease/OrderLine[@BundleParentOrderLineKey='"+ sBundleOrderLineKey + "']");

        double dOrderedQty = Double.valueOf(eleOrderLine.getAttribute(GCConstants.ORDERED_QTY));
        double dStatusQty = Double.valueOf(eleOrderLine.getAttribute(GCConstants.STATUS_QUANTITY));

        if(YFCCommon.equals(dOrderedQty,dStatusQty)){
          eleBundleParentOrderLine.setAttribute(GCConstants.STATUS_QUANTITY, eleBundleParentOrderLine.getAttribute(GCConstants.ORDERED_QTY));
        } else{
          double dBundleOrderedQty = Double.valueOf(eleBundleParentOrderLine.getAttribute(GCConstants.ORDERED_QTY));
          double dBundleStatusQty = (dStatusQty*dBundleOrderedQty)/dOrderedQty;
          eleBundleParentOrderLine.setAttribute(GCConstants.STATUS_QUANTITY,String.valueOf(dBundleStatusQty));
        }
      }
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" Updated input document is"
          + GCXMLUtil.getXMLString(inDoc));
    }
  }
  // OMS-4892 : CR:80 End
  @Override
  public void setProperties(Properties arg0) throws GCException {

  }
}
