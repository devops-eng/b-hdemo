/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class in invoked on Advance credit return and Adv exchange order type return orderline cancel.
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               14/05/2014        Soni, Karan		Initial Version
 *#################################################################################################################################################################
 */
package com.gc.api;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author karan
 */
public class GCOnReturnCancel {

	// initialize logger
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCOnReturnCancel.class);

	public Document createDebitMemo(YFSEnvironment env, Document inDoc)
			throws GCException {
		try {
			LOGGER.verbose("Entering the createdebitMemo method");

			Document changeOrderInXMLDoc = GCXMLUtil
					.createDocument(GCConstants.ORDER);
			Element order = GCXMLUtil.getRootElement(changeOrderInXMLDoc);

			String orderHeaderKey = GCXMLUtil.getAttributeFromXPath(inDoc,
					GCConstants.MONITORCONSOLIDATION_ORDERHEADERKEY_XPATH);
			String strEnterpriseCode = GCXMLUtil.getAttributeFromXPath(inDoc,
					"MonitorConsolidation/Order/@EnterpriseCode");
			GCXMLUtil.setAttribute(order, GCConstants.ACTION,
					GCConstants.MODIFY);
			GCXMLUtil.setAttribute(order, GCConstants.ORDER_HEADER_KEY,
					orderHeaderKey);
			Element eleOrderStatuses = GCXMLUtil.getElementByXPath(inDoc,
					"MonitorConsolidation/Order/OrderStatuses");
			Element orderLines = changeOrderInXMLDoc
					.createElement(GCConstants.ORDER_LINES);
			GCXMLUtil.appendChild(order, orderLines);
			NodeList nlOrderLine = GCXMLUtil.getNodeListByXpath(inDoc,
					"MonitorConsolidation/Order/OrderLines/OrderLine");
			for (int i = 0; i < nlOrderLine.getLength(); i++) {
				Element eleOrderLine = (Element) nlOrderLine.item(i);
				String strOrderLineKey = eleOrderLine
						.getAttribute(GCConstants.ORDER_LINE_KEY);
				String strQuantity = XPathAPI.eval(
						eleOrderStatuses,
						"sum(OrderStatus[@OrderLineKey='" + strOrderLineKey
								+ "']/@StatusQty)").toString();
				if (Double.parseDouble(strQuantity) > 0.0) {
					Element orderLine = changeOrderInXMLDoc
							.createElement(GCConstants.ORDER_LINE);
					GCXMLUtil.appendChild(orderLines, orderLine);
					GCXMLUtil.setAttribute(orderLine, GCConstants.ACTION,
							GCConstants.CANCEL);
					GCXMLUtil.setAttribute(orderLine,
							GCConstants.ORDER_LINE_KEY, strOrderLineKey);
					GCXMLUtil.setAttribute(orderLine, GCConstants.ORDERED_QTY,
							"0");
				}
			}
			boolean isDebitMemoReq= false;
			Element eleOrderLine = GCXMLUtil.getElementByXPath(
					changeOrderInXMLDoc, "Order/OrderLines/OrderLine");
			if (!YFCCommon.isVoid(eleOrderLine)) {
				if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Change Order XML: "
						+ GCXMLUtil.getXMLString(changeOrderInXMLDoc));
				}
				isDebitMemoReq= true;
				GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER,
						changeOrderInXMLDoc);
			}

			Element eleMonitorConsolidation = inDoc.getDocumentElement();
			Element eleOrder = (Element) XPathAPI.selectSingleNode(
					eleMonitorConsolidation, GCConstants.ORDER);
			String strOrderType = eleOrder.getAttribute(GCConstants.ORDER_TYPE);
			if (GCConstants.RETURN_WITH_ADV_CREDIT.equals(strOrderType) && isDebitMemoReq) {
				LOGGER.debug("Entering the Advance credit return if block");
				String strOrderAuditInput = "<OrderAudit  EnterpriseCode='"
						+ strEnterpriseCode + "'  OrderHeaderKey='"
						+ orderHeaderKey + "' />";
				Document inDocOrderAudit = GCXMLUtil
						.getDocument(strOrderAuditInput);
				Document inDocTemplate = GCCommonUtil
						.getDocumentFromPath(GCConstants.GET_ORDER_AUDIT_LIST_TEMPLATE);
				Document outDoc = GCCommonUtil.invokeAPI(env,
						"getOrderAuditList", inDocOrderAudit, inDocTemplate);

				Element eleAttribute = GCXMLUtil
						.getElementByXPath(
								outDoc,
								"OrderAuditList/OrderAudit/OrderAuditLevels/OrderAuditLevel/OrderAuditDetails/OrderAuditDetail/Attributes/Attribute[@Name='TotalAmount']");
				String strOldValue = eleAttribute
						.getAttribute(GCConstants.OLD_VALUE);
				String strNewValue = eleAttribute
						.getAttribute(GCConstants.NEW_VALUE);
				String strCurrency = GCXMLUtil.getAttributeFromXPath(eleOrder,
						"PriceInfo/@Currency");
				String strOrderDate = eleOrder
						.getAttribute(GCConstants.ORDER_DATE);
				String strOHK = GCXMLUtil
						.getAttributeFromXPath(eleOrder,
								"OrderLines/OrderLine/DerivedFromOrder/@OrderHeaderKey");
				String strSellerOrg = eleOrder
						.getAttribute(GCConstants.SELLER_ORGANIZATION_CODE);
				String strCreditMemo = "<OrderInvoice Currency='" + strCurrency
						+ "' DateInvoiced='" + strOrderDate
						+ "'  DocumentType='0001' EnterpriseCode='"
						+ strEnterpriseCode + "' InvoiceType='"
						+ GCConstants.DEBIT_MEMO + "' OrderHeaderKey='"
						+ strOHK + "' SellerOrganizationCode='" + strSellerOrg
						+ "'><HeaderChargeList/></OrderInvoice>";
				Document inDocCreditMemo = GCXMLUtil.getDocument(strCreditMemo);
				Element eleHeaderChargeList = GCXMLUtil.getElementByXPath(
						inDocCreditMemo, "OrderInvoice/HeaderChargeList");
				if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("The recordInvoiceCreation indoc after appending header is"
						+ GCXMLUtil.getXMLString(inDocCreditMemo));
				}
				Element eleHeaderCharge = inDocCreditMemo
						.createElement(GCConstants.HEADER_CHARGE);
				eleHeaderChargeList.appendChild(eleHeaderCharge);
				eleHeaderCharge.setAttribute(GCConstants.CHARGE_CATEGORY,
						GCConstants.DEBIT_ON_CANCEL);
				eleHeaderCharge.setAttribute(GCConstants.CHARGE_NAME,
						GCConstants.DEBIT_ON_CANCEL);
				eleHeaderCharge.setAttribute(
						GCConstants.CHARGE_AMOUNT,
						String.valueOf(Double.valueOf(strOldValue)
								- Double.valueOf(strNewValue)));
				if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("The final recordInvoiceCreation indoc"
						+ GCXMLUtil.getXMLString(inDocCreditMemo));
				}
				// Invoking recordInvoiceCreation to make credit memo in case of
				// advance credit return
				GCCommonUtil.invokeAPI(env,
						GCConstants.RECORD_INVOICE_CREATION, inDocCreditMemo);
				LOGGER.debug("Exiting the Advance credit return if block");
			}
			if(isDebitMemoReq){
				GCCommonUtil.invokeService(env, "GCCapturePaymentAlert", inDoc);
			}
			LOGGER.verbose("Exiting the createDebitMemo method");
			return inDoc;
		} catch (Exception e) {
		    LOGGER.error("Inside GCOnReturnCancel.createDebitMemo():",e);
		    throw new GCException(e);
		}
	}
	
	
	/**
	 * This method is used to cancel Exchange Order when the return order is cancelled.
	 * @param env
	 * @param inDoc
	 * @return
   * @throws GCException
	 */
  public Document cancelExchangeOrder(YFSEnvironment env, Document inDoc) throws GCException {
	  LOGGER.beginTimer(" Entering GCOnReturnCancel.cancelExchangeOrder() method ");
	    if (LOGGER.isDebugEnabled()) {
	      LOGGER.debug(" cancelExchangeOrder() method, Input document is" + GCXMLUtil.getXMLString(inDoc));
	    }
	  YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
	  YFCElement eleOrder = inputDoc.getDocumentElement();
	  String sOrderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
	  
	  YFCDocument getOrderListInDoc = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderHeaderKey+"'/>");

	    YFCDocument getOrderListTemp = YFCDocument.getDocumentFor(
	        "<OrderList><Order OrderHeaderKey='' OrderNo='' MaxOrderStatus='' MinOrderStatus='' OrderType=''><OrderLines>"
	          + "<OrderLine OrderedQty='' MaxLineStatus='' MinLineStatus=''><LineOverallTotals/><OrderStatuses><OrderStatus Status='' StatusQty='' TotalQuantity=''/></OrderStatuses></OrderLine>"	
		      + "</OrderLines><OverallTotals /><ExchangeOrders><ExchangeOrder OrderHeaderKey='' MaxOrderStatus='' OrderNo='' ExchangeType=''/>"
		      + "</ExchangeOrders></Order></OrderList>");
	  
	  YFCDocument getOrderListOutDoc = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListInDoc, getOrderListTemp);
	  
	  if (LOGGER.isDebugEnabled()) {
	      LOGGER.debug("Return GetOrderList Output document is" + getOrderListOutDoc.toString());
	    }
	  
	  YFCElement eleRoot = getOrderListOutDoc.getDocumentElement();
	  YFCElement eleReturnOrder = eleRoot.getChildElement(GCConstants.ORDER);
	  if (!YFCCommon.isVoid(eleReturnOrder) && YFCCommon.equalsIgnoreCase("9000", eleReturnOrder.getAttribute("MinOrderStatus"))){
	    String sOrderType = eleReturnOrder.getAttribute(GCConstants.ORDER_TYPE);
	    if (YFCCommon.equalsIgnoreCase(sOrderType, GCConstants.RETURN_WITH_ADV_EXCHANGE)){
	      YFCElement eleExchangeOrders = eleReturnOrder.getChildElement("ExchangeOrders");
	      YFCElement eleExchangeOrder = eleExchangeOrders.getChildElement("ExchangeOrder");
	      if(!YFCCommon.isVoid(eleExchangeOrder) && (Double.parseDouble(eleExchangeOrder.getAttribute(GCConstants.MAX_ORDER_STATUS))<3200)){
	        String sExchOHK = eleExchangeOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
	        YFCDocument changeOrderDoc = YFCDocument.getDocumentFor("<Order Action='CANCEL' OrderHeaderKey='"+sExchOHK+"' Override='Y'/>");
	        if (LOGGER.isDebugEnabled()) {
	          LOGGER.debug("changeOrder document is" + changeOrderDoc.toString());
	        }
	        GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, changeOrderDoc, null);
        } else {
        	LOGGER.debug("Going to first else condition of cancelExchangeOrder Method : 1");
          appyHoldWithDummyAmount(env, inputDoc, sOrderType, eleReturnOrder);
        }
      } else if (YFCCommon.equalsIgnoreCase(sOrderType, "RTRN_WITH_EXCHNG")) {
        YFCElement eleExchangeOrders = eleReturnOrder.getChildElement("ExchangeOrders");
        YFCElement eleExchangeOrder = eleExchangeOrders.getChildElement("ExchangeOrder");
        if (!YFCCommon.isVoid(eleExchangeOrder)) {
          String sExchOHK = eleExchangeOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
          YFCDocument changeOrderDoc =
              YFCDocument.getDocumentFor("<Order Action='CANCEL' OrderHeaderKey='" + sExchOHK + "' Override='Y'/>");
          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("changeOrder document is" + changeOrderDoc.toString());
          }
          GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, changeOrderDoc, null);
        }
      }
    }

    else {
      String sOrderType = eleReturnOrder.getAttribute(GCConstants.ORDER_TYPE);
      LOGGER.debug("Going to 2nd else condition of cancelExchangeOrder Method : 2");
      appyHoldWithDummyAmount(env, inputDoc, sOrderType, eleReturnOrder);

    }
    LOGGER.endTimer(" Exiting GCOnReturnCancel.cancelExchangeOrder() method ");
    return inDoc;
  }

  public void appyHoldWithDummyAmount(YFSEnvironment env, YFCDocument inputDoc, String sOrderType,
      YFCElement eleReturnOrder) throws GCException {
    LOGGER.debug("Exchange Order details:-" + GCXMLUtil.getXMLString(inputDoc.getDocument()));

      if (YFCCommon.equalsIgnoreCase(sOrderType, GCConstants.RETURN_WITH_ADV_EXCHANGE)) {
        YFCElement eleExchangeOrders = eleReturnOrder.getChildElement("ExchangeOrders");
        YFCElement eleExchangeOrder = eleExchangeOrders.getChildElement("ExchangeOrder");
        
        String sExchangeOrderHeaderKey = eleExchangeOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
    	  
    	  YFCDocument getExchangeOrderListInDoc = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sExchangeOrderHeaderKey+"'/>");
        YFCDocument getExchangeOrderListTemp = YFCDocument.getDocumentFor(
            "<OrderList><Order OrderHeaderKey='' OrderNo='' MaxOrderStatus='' MinOrderStatus='' OrderType=''><OrderLines>"
              + "<OrderLine OrderedQty='' MaxLineStatus='' MinLineStatus=''><LineOverallTotals/><OrderStatuses><OrderStatus Status='' StatusQty='' TotalQuantity=''/></OrderStatuses></OrderLine>"	
    	      + "</OrderLines><OverallTotals /></Order></OrderList>");
  	  
        YFCDocument getExchangeOrderListOutDoc =
            GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getExchangeOrderListInDoc, getExchangeOrderListTemp);
        
    	  if (LOGGER.isDebugEnabled()) {
    	      LOGGER.debug("Exchange Order GetOrderList Output document is" + getExchangeOrderListOutDoc.toString());
    	    }
    	  
    	 YFCElement eleExchangeOverallTotals = getExchangeOrderListOutDoc.getDocumentElement().getElementsByTagName("OverallTotals").item(0);
       String sExchangeGrandTotal = eleExchangeOverallTotals.getAttribute("GrandTotal");
       Double exchangeGrantTotal = Double.parseDouble(sExchangeGrandTotal);
       LOGGER.debug("exchangeGrantTotal Amount = " + exchangeGrantTotal);  
        if (!YFCCommon.isVoid(eleExchangeOrder)
            && (Double.parseDouble(eleExchangeOrder.getAttribute(GCConstants.MAX_ORDER_STATUS)) < 3200)) {
          LOGGER.debug("Do nothing");
        } else {
          String sExchOHK = eleExchangeOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
          YFCDocument changeOrder = YFCDocument.getDocumentFor("<Order/>");
          YFCElement eleChangeOrderRoot = changeOrder.getDocumentElement();
          eleChangeOrderRoot.setAttribute(GCConstants.ORDER_HEADER_KEY, sExchOHK);

          // Adding Dummy Payment Hold
          YFCElement eleOrderHoldTypes = changeOrder.createElement(GCConstants.ORDER_HOLD_TYPES);
          YFCElement eleOrderholdType = changeOrder.createElement(GCConstants.ORDER_HOLD_TYPE);
          eleOrderholdType.setAttribute(GCConstants.HOLD_TYPE, "Apply_Dummy_Hold");
          eleOrderholdType.setAttribute(GCConstants.REASON_TEXT, "Return is Cancelled");
          eleOrderholdType.setAttribute(GCConstants.STATUS, GCConstants.APPLY_HOLD_CODE);
          eleOrderHoldTypes.appendChild(eleOrderholdType);
          eleChangeOrderRoot.appendChild(eleOrderHoldTypes);

          LOGGER.debug("Order with hold tags = " + GCXMLUtil.getXMLString(changeOrder.getDocument()));

          // Adding dummy amount to add logic
          double dummyAmount = 0.0;
          YFCNodeList<YFCElement> nlOrderAuditLevel = inputDoc.getElementsByTagName("OrderAuditLevel");
          for (int y = 0; y < nlOrderAuditLevel.getLength(); y++) {
            YFCElement eleOrderAuditLevel = nlOrderAuditLevel.item(y);
            if (YFCCommon.equalsIgnoreCase(eleOrderAuditLevel.getAttribute("ModificationLevel"), "ORDER")) {
              YFCNodeList<YFCElement> nlAttribute = eleOrderAuditLevel.getElementsByTagName("Attribute");
              for (int z = 0; z < nlAttribute.getLength(); z++) {
                YFCElement eleAttribute = nlAttribute.item(z);
                if (YFCCommon.equals(eleAttribute.getAttribute("Name"), "TotalAmount")) {
                  dummyAmount = Double.parseDouble(eleAttribute.getAttribute("OldValue"))
                      - Double.parseDouble(eleAttribute.getAttribute("NewValue"));
                }
              }
            }
          }

          LOGGER.debug("Dummy Amount = " + dummyAmount);

        //LOGGER.debug("Current return amount can be seen "
          //  + GCXMLUtil.getXMLString(GCXMLUtil.getDocumentFromElement((Element) eleReturnOrder)));

        GCUpdateReturnOrderPrice.updateReturnOrderPrice(env, inputDoc.getDocument());

        String sReturnOrderHeaderKey = eleReturnOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
        YFCDocument DocGOD =
            YFCDocument.getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/getOrderList.xml"));
        YFCDocument returnGODInput =
            YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sReturnOrderHeaderKey + "'/>");
        YFCDocument changedReturnDetails =
            GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST, returnGODInput, DocGOD);

        LOGGER.debug("Changed return amount can be seen " + GCXMLUtil.getXMLString(changedReturnDetails.getDocument()));

        YFCElement eleOverallTotals = inputDoc.getDocumentElement().getElementsByTagName("OverallTotals").item(0);
        String sGrandTotal = eleOverallTotals.getAttribute("GrandTotal");
        Double grantTotal = Double.parseDouble(sGrandTotal);

        eleOverallTotals = changedReturnDetails.getDocumentElement().getElementsByTagName("OverallTotals").item(0);
        String sUpdatedGrandTotal = eleOverallTotals.getAttribute("GrandTotal");
        Double updatedGrantTotal = Double.parseDouble(sUpdatedGrandTotal);

        dummyAmount += (grantTotal - updatedGrantTotal);
        LOGGER.debug("Updated Dummy Amount after prorations= " + dummyAmount);


          YFCElement eleExtn = changeOrder.createElement("Extn");
          eleChangeOrderRoot.appendChild(eleExtn);
          eleExtn.setAttribute("ExtnRetRecDummyAmount", String.valueOf(dummyAmount));

          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Apply Dummy Payment Hold() - Input Document before change Order"
                + GCXMLUtil.getXMLString(changeOrder.getDocument()));
          }

          Double retExcAmntDiffer = updatedGrantTotal - exchangeGrantTotal ;
          
          LOGGER.debug("Difference between the Pending Return Amount and Exchange Order Amount = " + retExcAmntDiffer);
          
          if(retExcAmntDiffer<0.0){
        	  
        	  GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, changeOrder.getDocument());
              LOGGER.debug("Class: GCApplyTaxExemptHold Method: applyTaxExemptHold END");
          }
         
	      }
	    }
	  }

}
