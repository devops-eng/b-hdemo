package com.gc.api;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCOnSaveSuccessChangeOrder{

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCOnSaveSuccessChangeOrder.class);
  /**
   *
   * Description of onSaveSuccessChangeOrder:
   * this method corrects the parent and warranty Orderlines associations information
   * like ExtnWarrantyLineNumber,ExtnWarrantyParentLineNumber
   *
   * @param env
   * @param inDoc
   * @return
   * @throws GCException
   *
   */
  public Document onSaveSuccessChangeOrder(YFSEnvironment env, Document inDoc) throws GCException{
    LOGGER.debug("Entering GCOnSaveSuccessChangeOrder.onSaveSuceeChangeOrder(), inDoc:"+ GCXMLUtil.getXMLString(inDoc));
    try {
      String sDocType=GCXMLUtil.getAttributeFromXPath(inDoc,"Order/@DocumentType");
      if("0001".equals(sDocType)){
        boolean isSomethingChanged=false;
        NodeList nlOrderLine=XPathAPI.selectNodeList(inDoc,"Order/OrderLines/OrderLine");
        int count = nlOrderLine.getLength();
        if(count!=0){
          callChangeOrderAPI(env, inDoc, isSomethingChanged, nlOrderLine, count);
        }
      }
    } catch (Exception e) {
      LOGGER.error("Exception is in Class: GCOnSaveSuccessChangeOrder Method: onSaveSuccessChangeOrder()", e);
      throw new GCException(e);
    }
    LOGGER.debug("Exiting GCOnSaveSuccessChangeOrder.onSaveSuceeChangeOrder(), inDoc:"+ GCXMLUtil.getXMLString(inDoc));
    return inDoc;
  }

  /**
   *
   * @param env
   * @param inDoc
   * @param isSomethingChanged
   * @param nlOrderLine
   * @param count
   * @throws TransformerException
   * @throws GCException
   */
  private void callChangeOrderAPI(YFSEnvironment env, Document inDoc, boolean isSomethingChanged,
      NodeList nlOrderLine, int count) throws TransformerException, GCException {

    //Refactored code fro Sonar Fix
    String sOrderHeaderKey=GCXMLUtil.getAttributeFromXPath(inDoc,"Order/@OrderHeaderKey");
    Document docChangeOrder = GCXMLUtil
        .createDocument(GCConstants.ORDER);
    Element eleChangeOrder = docChangeOrder.getDocumentElement();
    eleChangeOrder.setAttribute(GCConstants.ORDER_HEADER_KEY,
        sOrderHeaderKey);
    eleChangeOrder.setAttribute(GCConstants.ACTION, GCConstants.MODIFY);
    Element eleChangeOrderLines = GCXMLUtil.createElement(
        docChangeOrder, GCConstants.ORDER_LINES, null);
    eleChangeOrder.appendChild(eleChangeOrderLines);

    for(int i=0;i<count;i++){
      Element eleOrderLine=(Element) nlOrderLine.item(i);
      Element eleOrderLineExtn = (Element) XPathAPI.selectSingleNode(
          eleOrderLine, GCConstants.EXTN);
      if(!YFCCommon.isVoid(eleOrderLineExtn)){
        String sExtnIsWarrantyItem=eleOrderLineExtn.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
        if("Y".equals(sExtnIsWarrantyItem)){
          String sExtnWarrantyParentLineNumber = eleOrderLineExtn.getAttribute(GCConstants.EXTN_WARRANTY_PARENT_LINE_NUMBER);
          String sDependentOnLineKey=eleOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
          String sPrimeLineNo=eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
          String sSubLineNo=eleOrderLine.getAttribute(GCConstants.SUB_LINE_NO);

          Element eleParentOrderLine=(Element)XPathAPI.selectSingleNode(inDoc,"Order/OrderLines/OrderLine[@OrderLineKey='"+ sDependentOnLineKey + "']");
          if(!YFCCommon.isVoid(eleParentOrderLine)){
            String sParentPrimeLineNo=eleParentOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
            String sParentSubLineNo=eleParentOrderLine.getAttribute(GCConstants.SUB_LINE_NO);
            Element eleParentOrderLineExtn = (Element) XPathAPI.selectSingleNode(
                eleParentOrderLine, GCConstants.EXTN);
            String sParentExtnWarrantyLineNumber=eleParentOrderLineExtn.getAttribute(GCConstants.EXTN_WARRANTY_LINE_NUMBER);
            if(!(sExtnWarrantyParentLineNumber.equals(sParentPrimeLineNo) && sPrimeLineNo.equals(sParentExtnWarrantyLineNumber))){
              eleOrderLineExtn.setAttribute(GCConstants.EXTN_WARRANTY_PARENT_LINE_NUMBER,sParentPrimeLineNo);
              eleParentOrderLine.setAttribute(GCConstants.EXTN_WARRANTY_LINE_NUMBER,sPrimeLineNo);
              eleChangeOrderLines=appendChangeOrderLine(eleChangeOrderLines,docChangeOrder,sParentPrimeLineNo,sParentSubLineNo,sPrimeLineNo,sSubLineNo);
              isSomethingChanged=true;
            }
          }
        }
      }
    }
    if(isSomethingChanged){
      LOGGER.debug("CallingChangeOrder, input Doc:"+ GCXMLUtil.getXMLString(docChangeOrder));
      GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER,
          docChangeOrder);
    }
  }
  /**
   *
   * Description of appendChangeOrderLine: this method appends modifying orderlines in the changeOrder input document
   *
   * @param eleChangeOrderLines
   * @param docChangeOrder
   * @param sParentPrimeLineNo
   * @param sParentSubLineNo
   * @param sWarrantyPrimeLineNo
   * @param sWarrantySubLineNo
   * @return
   * @throws GCException
   *
   */
  private static Element appendChangeOrderLine(Element eleChangeOrderLines, Document docChangeOrder,
      String sParentPrimeLineNo,String sParentSubLineNo, String sWarrantyPrimeLineNo,String sWarrantySubLineNo)
          throws GCException {
    LOGGER.debug(" Entering GCOnSaveSuccessChangeOrder.appendChangeOrderLine() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" Input document is"
          + GCXMLUtil.getXMLString(docChangeOrder));
    }
    Element eleParentChangeOrderLine = GCXMLUtil.createElement(docChangeOrder,
        GCConstants.ORDER_LINE, null);
    Element eleParentOrderLineExtn = GCXMLUtil.createElement(docChangeOrder,
        GCConstants.EXTN, null);
    Element eleWarrantyChangeOrderLine = GCXMLUtil.createElement(docChangeOrder,
        GCConstants.ORDER_LINE, null);
    Element eleWarrantyOrderLineExtn = GCXMLUtil.createElement(docChangeOrder,
        GCConstants.EXTN, null);
    try {
      eleParentChangeOrderLine.setAttribute(GCConstants.ACTION,
          GCConstants.MODIFY);
      eleParentChangeOrderLine.setAttribute(GCConstants.PRIME_LINE_NO,
          sParentPrimeLineNo);
      eleParentChangeOrderLine
      .setAttribute(GCConstants.SUB_LINE_NO, sParentSubLineNo);
      eleParentOrderLineExtn.setAttribute(GCConstants.EXTN_WARRANTY_LINE_NUMBER, sWarrantyPrimeLineNo);
      eleParentChangeOrderLine.appendChild(eleParentOrderLineExtn);
      eleChangeOrderLines.appendChild(eleParentChangeOrderLine);

      eleWarrantyChangeOrderLine.setAttribute(GCConstants.ACTION,
          GCConstants.MODIFY);
      eleWarrantyChangeOrderLine.setAttribute(GCConstants.PRIME_LINE_NO,
          sWarrantyPrimeLineNo);
      eleWarrantyChangeOrderLine
      .setAttribute(GCConstants.SUB_LINE_NO, sWarrantySubLineNo);
      eleWarrantyOrderLineExtn.setAttribute(GCConstants.EXTN_WARRANTY_PARENT_LINE_NUMBER, sParentPrimeLineNo);
      eleWarrantyChangeOrderLine.appendChild(eleWarrantyOrderLineExtn);
      eleChangeOrderLines.appendChild(eleWarrantyChangeOrderLine);
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCOnSaveSuccessChangeOrder.appendChangeOrderLine(), Exception is :",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCOnSaveSuccessChangeOrder.appendChangeOrderLine() method ");
    return eleChangeOrderLines;
  }



}
