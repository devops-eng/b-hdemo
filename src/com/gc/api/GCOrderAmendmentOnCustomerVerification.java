package com.gc.api;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCOrderAmendmentOnCustomerVerification {
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCOrderAmendmentOnCustomerVerification.class);

  public void orderAmendmentOnCustomerVerification(YFSEnvironment env,Document inDoc)throws GCException{
    LOGGER.debug("Entering inside GCOrderAmendmentOnCustomerVerification.orderAmendmentOnCustomerVerification(),"
        + " InDoc:"+GCXMLUtil.getXMLString(inDoc));

    try {
      Element eleOrderAmendmentParameter=inDoc.getDocumentElement();
      String sOperationIdentifier=eleOrderAmendmentParameter.getAttribute(GCConstants.OPERATION_IDENTIFIER);
      String sOrderHeaderKey=eleOrderAmendmentParameter.getAttribute(GCConstants.ORDER_HEADER_KEY);

      if(sOperationIdentifier.equals(GCConstants.CUSTOMER_DOES_NOT_EXIST)){
        LOGGER.debug(" inside if when CustomerDoesNotExist");

        Document dManageTaskQinDoc=GCXMLUtil.createDocument("TaskQueue");
        Element eleTaskQueue=dManageTaskQinDoc.getDocumentElement();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, 15);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String newTime = df.format(cal.getTime());
        eleTaskQueue.setAttribute(GCConstants.AVAILABLE_DATE, newTime);
        eleTaskQueue.setAttribute(GCConstants.DATA_TYPE, "OrderHeaderKey");
        eleTaskQueue.setAttribute(GCConstants.DATA_KEY, sOrderHeaderKey);
        eleTaskQueue.setAttribute(GCConstants.TRANSACTION_KEY, GCConstants.RELEASE_TRAN_ID);
        eleTaskQueue.setAttribute(GCConstants.OPERATION, "Modify");
        GCCommonUtil.invokeAPI(env,GCConstants.API_MANAGE_TASK_QUEUE,dManageTaskQinDoc);
      }else if(sOperationIdentifier.equals(GCConstants.ACTIVE_CUSTOMER)){
        LOGGER.debug(" inside if when Customer is Active");
        String sCustomerID=eleOrderAmendmentParameter.getAttribute(GCConstants.CUSTOMER_ID);
        Document dChangeOrderinDoc=GCXMLUtil.createDocument("Order");
        Element eleOrder=dChangeOrderinDoc.getDocumentElement();
        eleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY,sOrderHeaderKey);
        eleOrder.setAttribute(GCConstants.BILL_TO_ID, sCustomerID);
        LOGGER.debug(" docChangeOrder is"
            + GCXMLUtil.getXMLString(dChangeOrderinDoc));

        GCCommonUtil.invokeAPI(env,GCConstants.CHANGE_ORDER,dChangeOrderinDoc);
        Document dManageTaskQinDoc=GCXMLUtil.createDocument("TaskQueue");
        Element eleTaskQueue=dManageTaskQinDoc.getDocumentElement();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, 5);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String newTime = df.format(cal.getTime());
        eleTaskQueue.setAttribute(GCConstants.AVAILABLE_DATE, newTime);
        eleTaskQueue.setAttribute(GCConstants.DATA_TYPE, "OrderHeaderKey");
        eleTaskQueue.setAttribute(GCConstants.DATA_KEY, sOrderHeaderKey);
        eleTaskQueue.setAttribute(GCConstants.TRANSACTION_KEY, GCConstants.RELEASE_TRAN_ID);
        eleTaskQueue.setAttribute(GCConstants.OPERATION, "Modify");
        GCCommonUtil.invokeAPI(env,GCConstants.API_MANAGE_TASK_QUEUE,dManageTaskQinDoc);
      }else if(sOperationIdentifier.equals(GCConstants.INACTIVE_CUSTOMER)){
        LOGGER.debug(" inside if when Customer is Inactive");
        String sCustomerID=eleOrderAmendmentParameter.getAttribute(GCConstants.CUSTOMER_ID);
        String sEnterpriseCode=eleOrderAmendmentParameter.getAttribute(GCConstants.ENTERPRISE_CODE);
        Document raiseAlertIP=GCXMLUtil.createDocument("Order");
        Element eleOrder=raiseAlertIP.getDocumentElement();
        eleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
        eleOrder.setAttribute(GCConstants.BILL_TO_ID, sCustomerID);
        eleOrder.setAttribute(GCConstants.ENTERPRISE_CODE, sEnterpriseCode);
        eleOrder.setAttribute(GCConstants.CUSTOMER_ID, sCustomerID);
        GCCommonUtil.invokeService(env, GCConstants.GC_INACTIVE_ACCOUNT_ALERT, raiseAlertIP);

        //applying Hold coding starts
        Document docChangeOrder = GCXMLUtil
            .createDocument(GCConstants.ORDER);
        Element eleRootOfChangeOrder = docChangeOrder
            .getDocumentElement();
        eleRootOfChangeOrder.setAttribute(GCConstants.ORDER_HEADER_KEY,sOrderHeaderKey);
        eleRootOfChangeOrder.setAttribute(GCConstants.OVERRIDE,
            GCConstants.YES);

        Element eleOrderHoldTypes = GCXMLUtil.createElement(
            docChangeOrder, GCConstants.ORDER_HOLD_TYPES,
            null);
        eleRootOfChangeOrder.appendChild(eleOrderHoldTypes);
        Element eleOrderHoldType = GCXMLUtil.createElement(
            docChangeOrder, GCConstants.ORDER_HOLD_TYPE,
            null);
        eleOrderHoldType.setAttribute(GCConstants.HOLD_TYPE,
            GCConstants.INACTIVE_ACCOUNT_HOLD);
        eleOrderHoldType.setAttribute(GCConstants.STATUS,
            GCConstants.APPLY_HOLD_CODE);
        eleOrderHoldTypes.appendChild(eleOrderHoldType);
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug(" docChangeOrder is"
              + GCXMLUtil.getXMLString(docChangeOrder));
        }
        GCCommonUtil.invokeAPI(env,
            GCConstants.API_CHANGE_ORDER, docChangeOrder);

      }
      LOGGER.debug("Exiting GCOrderAmendmentOnCustomerVerification :: orderAmendmentOnCustomerVerification ::");
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCOrderAmendmentOnCustomerVerification.orderAmendmentOnCustomerVerification(), Exception is :",
          e);
      throw new GCException(e);
    }
  }

}
