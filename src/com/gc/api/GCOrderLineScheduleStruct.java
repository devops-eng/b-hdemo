/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################################
 *	        OBJECTIVE: Contains the logic for storing data to prepare the changeSchedule's Input.
 *###################################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *################################################################################################################################################################################
             1.0               30/03/2015        Khakha, Johnson            changeSchedule
 *##################################################################################################################################################################################
 */
package com.gc.api;

import java.util.ArrayList;
import java.util.List;

import com.yantra.yfc.core.YFCObject;

/**
 * @author <a href="mailto:johnson.khakha@expicient.com">Johnson</a>
 */
public class GCOrderLineScheduleStruct {
  private String orderLineKey = null;
  private double orderedQty = -1;
  private String toExpectedShipDate = null;
  private String fromExpectedShipDate = null;
  private double daxQuantity = 0.0;
  private double ratio = 0.0;
  private double alreadyScheduledQty = 0.0;
  private List<GCOrderLineSchedule> schedules = null;

  /**
   * Constructor.
   * @param orderLineKey - orderLineKey
   * @param toExpectedShipDate - toExpectedShipDate
   * @param fromExpectedShipDate - fromExpectedShipDate
   * @param sDaxQuantity - sDaxQuantity
   * @param sOrderedQty - sOrderedQty
   */
  public GCOrderLineScheduleStruct(String orderLineKey, String toExpectedShipDate, String fromExpectedShipDate, String sDaxQuantity, String sOrderedQty){
    this.orderLineKey = orderLineKey;
    this.toExpectedShipDate = toExpectedShipDate;
    this.fromExpectedShipDate = fromExpectedShipDate;
    this.daxQuantity = YFCObject.isVoid(sDaxQuantity) ? 0.0 : Double.parseDouble(sDaxQuantity);
    this.orderedQty = (YFCObject.isVoid(sOrderedQty) ? 0.0 : Double.parseDouble(sOrderedQty));
  }

  /**
   * Constructor.
   * @param orderLineKey - orderLineKey
   * @param alreadyScheduledQty - alreadyScheduledQty
   * @param ratio - ratio
   * @param orderedQty - orderedQty
   * @param schedules - schedules
   */
  public GCOrderLineScheduleStruct(String orderLineKey, double alreadyScheduledQty, double ratio,
      double orderedQty, List<GCOrderLineSchedule> schedules){
    this.orderLineKey = orderLineKey;
    this.alreadyScheduledQty = alreadyScheduledQty;
    this.ratio = ratio;
    this.orderedQty = orderedQty;
    this.schedules = schedules;
  }

  public String getOrderLineKey() {
    return orderLineKey;
  }
  public void setOrderLineKey(String orderLineKey) {
    this.orderLineKey = orderLineKey;
  }
  public String getToExpectedShipDate() {
    return toExpectedShipDate;
  }
  public void setToExpectedShipDate(String toExpectedShipDate) {
    this.toExpectedShipDate = toExpectedShipDate;
  }
  public String getFromExpectedShipDate() {
    return fromExpectedShipDate;
  }
  public void setFromExpectedShipDate(String fromExpectedShipDate) {
    this.fromExpectedShipDate = fromExpectedShipDate;
  }

  public void setDaxQuantity(double daxQuantity) {
    this.daxQuantity = daxQuantity;
  }

  public double getDaxQuantity() {
    return daxQuantity;
  }

  public double getOrderedQty() {
    return orderedQty;
  }

  public void setOrderedQty(double orderedQty) {
    this.orderedQty = orderedQty;
  }

  public double getRatio() {
    return ratio;
  }

  public void setRatio(double ratio) {
    this.ratio = ratio;
  }

  public double getAlreadyScheduledQty() {
    return alreadyScheduledQty;
  }

  public void setAlreadyScheduledQty(double alreadyScheduledQty) {
    this.alreadyScheduledQty = alreadyScheduledQty;
  }

  public double getBundleQtyForComponent() {
    return Math.floor(((this.alreadyScheduledQty + this.daxQuantity)/this.ratio));
  }

  public List<GCOrderLineSchedule> getSchedules() {
    return schedules;
  }

  public void setSchedules(List<GCOrderLineSchedule> schedules) {
    this.schedules = schedules;
  }

  public void addSchedules(String fromExpShipDate, double quantity) {
    if(this.schedules == null){
      this.schedules = new ArrayList<GCOrderLineSchedule>();
    }
    this.schedules.add(new GCOrderLineSchedule(fromExpShipDate, quantity));
  }

  public static class GCOrderLineSchedule{
    private String fromExpShipDate = null;
    private double quantity = 0.0;

    public GCOrderLineSchedule(String fromExpShipDate, double quantity){
      this.fromExpShipDate = fromExpShipDate;
      this.quantity = quantity;
    }

    public String getFromExpShipDate() {
      return fromExpShipDate;
    }
    public void setFromExpShipDate(String fromExpShipDate) {
      this.fromExpShipDate = fromExpShipDate;
    }
    public double getQuantity() {
      return quantity;
    }
    public void setQuantity(double quantity) {
      this.quantity = quantity;
    }
  }

}
