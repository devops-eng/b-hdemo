/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################################
 *	        OBJECTIVE: Contains the OrderLine Information used to form changeOrderStatus and changeOrderSchdeule APIs input XML for Kit and Set Items.
 *###################################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *################################################################################################################################################################################
             1.0               17/04/2015        Khakha, Johnson            Initial Version
 *##################################################################################################################################################################################
 */
package com.gc.api;

import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;

/**
 * @author <a href="mailto:johnson.khakha@expicient.com">Johnson</a>
 */
public class GCOrderLineStructForKitNSetItem {

  /**
   * Logger for debugging purpose.
   */
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCOrderLineStructForKitNSetItem.class);

  /**
   * Minimum Ratio used to for the Kit.
   */
  private int minRatio = -1 ;

  /**
   * iCompRatio: is the Component Ratio in an order.
   */
  private int iCompRatio = -1;

  /**
   * iKitRatio: based on the dax message ratio eligible for forming a kit.
   */
  private int iKitRatio = -1;

  /**
   * OrderLine key of the orderline.
   */
  private String sOrdLineKey = null;

  /**
   * Ordered quantity of the orderline.
   */
  private double dOrderedQty = -1;

  /**
   * Allocation qunatity sent from the Dax.
   */
  private double dDaxQuantity = 0.0;

  /**
   * Status qunatity for SCHEDULE_PODAX and RECIEVED_PODAX status.
   * key as status , value as status qty.
   */
  private Map<String, Double> initQty = new HashMap<String, Double>();

  /**
   * Requested ship date.
   */
  private String sToExpectedShipDate = null;

  /**
   * Expected ship date for SCHEDULE_PODAX and RECIEVED_PODAX status.
   * Key as status, value as expected ship date.
   */
  private Map<String, String> fromExpectedShpDateMap = new HashMap<String, String>();

  /**
   * Boolean used to check if all the values are already set.
   */
  private boolean isAllInfoAvailable = false;

  /**
   * Quantity to be moved from schedule PODAX to PODAX unschedule.
   */
  private double dQtyFromSchdPDXToPDXUnschd = 0.0;

  /**
   * Quantity to be moved from schedule PODAX to received PODAX.
   */
  private double dQtyFromSchdPDXToRecvPDX = 0.0;

  /**
   * Quantity to be moved from  received PODAX to PODAX unschedule.
   */
  private double dQtyFromRecvPDXToPDXUnSchd = 0.0;  

  /**
   * Default Constructor.
   */
  public GCOrderLineStructForKitNSetItem() {

  }

  /**
   * Constructor.
   * @param childElement - child Order Line.
   * @param sBundleQty - Ordered quantity of the parent item.
   * @param dDAXQty - allocation qunatity sent from DAX
   * @param sToExpectedShipDate - Requested ship date of the order.
   * @param isSetItem
   * @throws TransformerException - TransformerException.
   */
  public GCOrderLineStructForKitNSetItem(Element childElement, String sBundleQty, double dDAXQty,
      String sToExpectedShipDate, boolean isSetItem) throws TransformerException {

    LOGGER.debug(" Entering GCOrderLineStructForKitItem.GCOrderLineStructForKitItem() method");

    if(!YFCCommon.isVoid(childElement)) {
      this.sOrdLineKey = childElement.getAttribute(GCConstants.ORDER_LINE_KEY);
      String sOrderedQty = childElement.getAttribute(GCConstants.ORDERED_QTY);
      this.dOrderedQty = YFCObject.isVoid(sOrderedQty) ? 0.0 : Double.parseDouble(sOrderedQty);
      this.sToExpectedShipDate = sToExpectedShipDate;

      double dBundleQty = YFCObject.isVoid(sBundleQty) ? 0.0 : Double.parseDouble(sBundleQty);

      this.iCompRatio = (int) ((int) this.dOrderedQty / dBundleQty);

      if(isSetItem) {
        this.dDaxQuantity = dDAXQty * this.iCompRatio;
      } else {
        this.dDaxQuantity = dDAXQty;
      }

      setInitialQtyAndExpShipDateAttr(childElement);

      double dRecvPDXQty = 0.0;
      if(initQty.containsKey(GCConstants.STATUS_RECIEVED_PODAX)) {
        dRecvPDXQty = initQty.get(GCConstants.STATUS_RECIEVED_PODAX);
      }
      this.iKitRatio = (int) ((dRecvPDXQty + this.dDaxQuantity) / this.iCompRatio);
    }

    LOGGER.debug("Exiting GCOrderLineStructForKitItem.GCOrderLineStructForKitItem() method");
  }

  /**
   * This method is used to set the status qty and expected ship date for each orderline status.
   * @param childElement - childElement.
   * @throws TransformerException - TransformerException
   */
  private void setInitialQtyAndExpShipDateAttr(Element childElement) throws TransformerException {

    LOGGER.debug("Entering GCOrderLineStructForKitItem.setInitialQtyAndExpShipDateAttr() method");

    NodeList orderStatusesNL = XPathAPI.selectNodeList(childElement,
        GCConstants.XPATH_ORD_STATUSES_ORD_STATUS);

    int ordStatusNLSize = orderStatusesNL.getLength();

    for(int i = 0 ; i <ordStatusNLSize; i++) {

      Element orderStatusEle =  (Element) orderStatusesNL.item(i);
      String status = orderStatusEle.getAttribute(GCConstants.STATUS).trim();

      if(GCConstants.STATUS_SCHEDULE_PODAX.equals(status)
          && !this.initQty.containsKey(GCConstants.STATUS_SCHEDULE_PODAX)){
        String sStatusQty = orderStatusEle.getAttribute(GCConstants.STATUS_QTY);
        Double dStatusQty = YFCObject.isVoid(sStatusQty) ? 0.0 : Double.parseDouble(sStatusQty);
        String sordLnSchKey = orderStatusEle.getAttribute(GCConstants.ORDER_LINE_SCHEDULE_KEY);
        this.initQty.put(GCConstants.STATUS_SCHEDULE_PODAX, dStatusQty);
        Element ordLnScheduleEle = (Element) XPathAPI.selectSingleNode(childElement,
            "Schedules/Schedule[@OrderLineScheduleKey='"+sordLnSchKey+"']");
        String expectedShipDate = ordLnScheduleEle.getAttribute(GCConstants.EXPECTED_SHIPMENT_DATE);
        this.fromExpectedShpDateMap.put(status, expectedShipDate);

      } else if(GCConstants.STATUS_RECIEVED_PODAX.equals(status)
          && !this.initQty.containsKey(GCConstants.STATUS_SCHEDULE_PODAX)){
        String sStatusQty = orderStatusEle.getAttribute(GCConstants.STATUS_QTY);
        Double dStatusQty = YFCObject.isVoid(sStatusQty) ? 0.0 : Double.parseDouble(sStatusQty);
        String sordLnSchKey = orderStatusEle.getAttribute(GCConstants.ORDER_LINE_SCHEDULE_KEY);
        this.initQty.put(GCConstants.STATUS_RECIEVED_PODAX, dStatusQty);
        Element ordLnScheduleEle = (Element) XPathAPI.selectSingleNode(childElement,
            "Schedules/Schedule[@OrderLineScheduleKey='"+sordLnSchKey+"']");
        String expectedShipDate = ordLnScheduleEle.getAttribute(GCConstants.EXPECTED_SHIPMENT_DATE);
        this.fromExpectedShpDateMap.put(status, expectedShipDate);
      }
    }

    LOGGER.debug("Exiting GCOrderLineStructForKitItem.setInitialQtyAndExpShipDateAttr() method");
  }

  /**
   * Setter to update all the global variable.
   */
  public void setAllValues() {
    double minQtyToBeshipped = this.minRatio * this.iCompRatio;
    this.dQtyFromSchdPDXToPDXUnschd = Math.min(this.dDaxQuantity, minQtyToBeshipped);
    this.dQtyFromRecvPDXToPDXUnSchd = minQtyToBeshipped - this.dQtyFromSchdPDXToPDXUnschd;
    this.dQtyFromSchdPDXToRecvPDX = this.dDaxQuantity - this.dQtyFromSchdPDXToPDXUnschd;
    this.isAllInfoAvailable = true;
  }

  /**
   * Getter for sOrdLineKey
   *
   * @return sOrdLineKey
   */
  public String getsOrdLineKey() {
    return sOrdLineKey;
  }

  /**
   * Setter for sOrdLineKey
   * @param sOrdLineKey - sOrdLineKey.
   */
  public void setsOrdLineKey(String sOrdLineKey) {
    this.sOrdLineKey = sOrdLineKey;
  }

  /**
   * Getter for getdOrderedQty
   * @return dOrderedQty
   */
  public double getdOrderedQty() {
    return dOrderedQty;
  }

  /**
   * Setter for dOrderedQty
   * @param dOrderedQty - dOrderedQty
   */
  public void setdOrderedQty(double dOrderedQty) {
    this.dOrderedQty = dOrderedQty;
  }

  /**
   * Getter for sToExpectedShipDate
   * @return sToExpectedShipDate
   */
  public String getsToExpectedShipDate() {
    return sToExpectedShipDate;
  }

  /**
   * Setter for sFromExpectedShipDate
   * @param sFromExpectedShipDate - sFromExpectedShipDate
   */
  public void setsToExpectedShipDate(String sFromExpectedShipDate) {
    this.sToExpectedShipDate = sFromExpectedShipDate;
  }

  /**
   * Getter for dDaxQuantity
   * @return dDaxQuantity
   */
  public double getdDaxQuantity() {
    return dDaxQuantity;
  }

  /**
   * Setter for  dDaxQuantity
   * @param dDaxQuantity - dDaxQuantity
   */
  public void setdDaxQuantity(double dDaxQuantity) {
    this.dDaxQuantity = dDaxQuantity;
  }

  /**
   * Getter for iCompRatio
   * @return iCompRatio
   */
  public int getdRatio() {
    return iCompRatio;
  }

  /**
   * Setter for dRatio
   * @param dRatio dRatio
   */
  public void setdRatio(int dRatio) {
    this.iCompRatio = dRatio;
  }

  /**
   * Getter for fromExpectedShpDateMap
   * @return fromExpectedShpDateMap
   */
  public Map<String, String> getFromExpectedShpDateMap() {
    return fromExpectedShpDateMap;
  }

  /**
   * Setter for toExpectedShpDateMap
   * @param toExpectedShpDateMap toExpectedShpDateMap
   */
  public void setFromExpectedShpDateMap(Map<String, String> toExpectedShpDateMap) {
    this.fromExpectedShpDateMap = toExpectedShpDateMap;
  }

  /**
   * Getter for initQty
   * @return initQty
   */
  public Map<String, Double> getInitQty() {
    return initQty;
  }

  /**
   * Setter for initQty
   * @param initQty initQty
   */
  public void setInitQty(Map<String, Double> initQty) {
    this.initQty = initQty;
  }

  /**
   * Getter for iKitRatio
   * @return iKitRatio
   */
  public int getKitRatio() {
    return iKitRatio;
  }

  /**
   * Setter for dKitRatio
   * @param dKitRatio dKitRatio
   */
  public void setKitRatio(int dKitRatio) {
    this.iKitRatio = dKitRatio;
  }

  /**
   * Getter for isAllInfoAvailable
   * @return isAllInfoAvailable
   */
  public boolean isAllInfoAvailable() {
    return isAllInfoAvailable;
  }

  /**
   * Setter for isAllInfoAvailable
   * @param isAllInfoAvailable isAllInfoAvailable
   */
  public void setAllInfoAvailable(boolean isAllInfoAvailable) {
    this.isAllInfoAvailable = isAllInfoAvailable;
  }

  /**
   * Getter for dQtyFromSchdPDXToPDXUnschd
   * @return dQtyFromSchdPDXToPDXUnschd
   */
  public double getdQtyFromSchdPDXToPDXUnschd() {
    return dQtyFromSchdPDXToPDXUnschd;
  }

  /**
   * Setter for dQtyFromSchdPDXToPDXUnschd
   * @param dQtyFromSchdPDXToPDXUnschd dQtyFromSchdPDXToPDXUnschd
   */
  public void setdQtyFromSchdPDXToPDXUnschd(double dQtyFromSchdPDXToPDXUnschd) {
    this.dQtyFromSchdPDXToPDXUnschd = dQtyFromSchdPDXToPDXUnschd;
  }

  /**
   * Getter for dQtyFromSchdPDXToRecvPDX
   * @return dQtyFromSchdPDXToRecvPDX
   */
  public double getdQtyFromSchdPDXToRecvPDX() {
    return dQtyFromSchdPDXToRecvPDX;
  }

  /**
   * Setter for  dQtyFromSchdPDXToRecvPDX
   * @param dQtyFromSchdPDXToRecvPDX dQtyFromSchdPDXToRecvPDX
   */
  public void setdQtyFromSchdPDXToRecvPDX(double dQtyFromSchdPDXToRecvPDX) {
    this.dQtyFromSchdPDXToRecvPDX = dQtyFromSchdPDXToRecvPDX;
  }

  /**
   * Getter for dQtyFromRecvPDXToPDXUnSchd
   * @return dQtyFromRecvPDXToPDXUnSchd
   */
  public double getdQtyFromRecvPDXToPDXUnSchd() {
    return dQtyFromRecvPDXToPDXUnSchd;
  }

  /**
   * Setter for dQtyFromRecvPDXToPDXUnSchd
   * @param dQtyFromRecvPDXToPDXUnSchd dQtyFromRecvPDXToPDXUnSchd
   */
  public void setdQtyFromRecvPDXToPDXUnSchd(double dQtyFromRecvPDXToPDXUnSchd) {
    this.dQtyFromRecvPDXToPDXUnSchd = dQtyFromRecvPDXToPDXUnSchd;
  }

  /**
   * Getter for minRatio
   * @return minRatio
   */
  public  int getMinRatio() {
    return this.minRatio;
  }

  /**
   * Setter for minRatio
   * @param minRatio minRatio
   */
  public  void setMinRatio(int minRatio) {
    this.minRatio = minRatio;
  }


}
