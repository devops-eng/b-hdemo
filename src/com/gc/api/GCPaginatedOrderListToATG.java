package com.gc.api;

import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.utils.GCCommonUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCPaginatedOrderListToATG implements YIFCustomApi {

  @Override
  public void setProperties(Properties arg0) throws Exception {

  }

  public Document insertTemplate(YFSEnvironment env, Document inDoc) {

    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleAPI = inputDoc.getElementsByTagName("API").item(0);
    Document getOrderListTemplate =
        GCCommonUtil.getDocumentFromPath("/global/template/api/getOrderListForATGRequest.xml");
    String templateDoc = YFCDocument.getDocumentFor(getOrderListTemplate).toString();
    YFCElement eleActTempl = inputDoc.createElement(templateDoc);
    YFCElement eleTemplate = eleAPI.createChild("Template");
    eleTemplate.appendChild(eleActTempl);
    return inputDoc.getDocument();

  }
}
