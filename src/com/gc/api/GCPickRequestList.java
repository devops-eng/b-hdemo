package com.gc.api;

import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCPickRequestList implements YIFCustomApi {

  YFSEnvironment env = null;
  String sOrderReleaseKey = "";
  String sReleaseNo = "";
  String sOrderNo = "";
  String sShipNode = "";
  String sReqShipDate = "";
  String sLineNo = "";
  String sItemID = "";
  String sItemDesc = "";
  String sQuantity = "";
  String sStatus = "";
  String sNodeType = "";
  String sFreeGiftItem = "";
  String sDependentOnLineKey = "";
  String sOrderLineKey = "";

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCPickRequestList.class.getName());

  @Override
  public void setProperties(Properties arg0) throws GCException {

  }

  public Document getPickRequestList(YFSEnvironment env, Document inDoc)
      throws GCException {
    try {
      final String logInfo = "GCGetPickRequestList :: getPickRequestList :: ";
      LOGGER.verbose(logInfo + "Begin");

      LOGGER.debug(logInfo + "Input Document"
          + GCXMLUtil.getXMLString(inDoc));

      this.env = env;

      Document docPickRequestList = GCXMLUtil
          .createDocument("PickRequestList");
      Element elePickRequestListRoot = docPickRequestList
          .getDocumentElement();
      Document docGetOrderReleaseList = GCCommonUtil
          .invokeAPI(env, inDoc,
              GCConstants.API_GET_ORDER_RELEASE_LIST,
              "/global/template/api/getOrderReleaseListForPickRequest.xml");
      NodeList nlOrderRelease = GCXMLUtil.getNodeListByXpath(
          docGetOrderReleaseList, "/OrderReleaseList/OrderRelease");
      for (int i = 0; i < nlOrderRelease.getLength(); i++) {
        Element eleItem = (Element) nlOrderRelease.item(i);
        sOrderReleaseKey = GCXMLUtil.getAttributeFromXPath(
            GCXMLUtil.getDocument(eleItem, true),
            "//OrderRelease/@OrderReleaseKey");
        sReleaseNo = GCXMLUtil.getAttributeFromXPath(
            GCXMLUtil.getDocument(eleItem, true),
            "//OrderRelease/@ReleaseNo");
        sOrderNo = GCXMLUtil.getAttributeFromXPath(
            GCXMLUtil.getDocument(eleItem, true),
            "//OrderRelease/Order/@OrderNo");
        Element elePickRequest = docPickRequestList
            .createElement("PickRequest");
        elePickRequestListRoot.appendChild(elePickRequest);

        elePickRequest
        .setAttribute("OrderReleaseKey", sOrderReleaseKey);
        elePickRequest.setAttribute("ReleaseNo", sReleaseNo);
        elePickRequest.setAttribute("OrderNo", sOrderNo);

        Document docOrderReleaseDetail = GCXMLUtil
            .createDocument("OrderReleaseDetail");
        Element eleReleaseDetail = docOrderReleaseDetail
            .getDocumentElement();
        eleReleaseDetail.setAttribute("OrderReleaseKey",
            sOrderReleaseKey);

        Document docGetOrderReleaseDetail = GCCommonUtil
            .invokeAPI(env, docOrderReleaseDetail,
                GCConstants.API_GET_ORDER_RELEASE_DETAILS,
                "/global/template/api/getOrderReleaseDetailForPickRequest.xml");
        Element eleOrderReleaseRoot = docGetOrderReleaseDetail
            .getDocumentElement();
        sShipNode = GCXMLUtil.getAttributeFromXPath(
            GCXMLUtil.getDocument(eleOrderReleaseRoot, true),
            "//OrderRelease/@ShipNode");
        sReqShipDate = GCXMLUtil.getAttributeFromXPath(
            GCXMLUtil.getDocument(eleOrderReleaseRoot, true),
            "//OrderRelease/@ReqShipDate");
        NodeList nLOrderLineItem = GCXMLUtil.getNodeListByXpath(
            docGetOrderReleaseDetail,
            "/OrderRelease/OrderLines/OrderLine");
        // GCSTORE-635 start
        try {
          sNodeType = GCCommonUtil.getOrganizationNodeType(env, sShipNode);
        } catch (Exception e) {
          LOGGER.error("Inside catch block of pickRequestList.java. Exception is", e);
          throw GCException.getYFCException(e.getMessage(), e);
        }
        // GCSTORE-635 end
        for (int j = 0; j < nLOrderLineItem.getLength(); j++) {
          Element eleOrderLineItem = (Element) nLOrderLineItem
              .item(j);
          Element elePickRequestLine = docPickRequestList
              .createElement("PickRequestLine");
          elePickRequest.appendChild(elePickRequestLine);
          sLineNo = eleOrderLineItem.getAttribute("PrimeLineNo");
          sItemID = GCXMLUtil.getAttributeFromXPath(
              GCXMLUtil.getDocument(eleOrderLineItem, true),
              "//OrderLine//Item/@ItemID");
          sItemDesc = GCXMLUtil.getAttributeFromXPath(
              GCXMLUtil.getDocument(eleOrderLineItem, true),
              "//OrderLine//Item/@ItemDesc");
          sQuantity = eleOrderLineItem.getAttribute("StatusQuantity");
          sStatus = eleOrderLineItem.getAttribute("Status");
          sFreeGiftItem = GCXMLUtil.getAttributeFromXPath( 
			  GCXMLUtil.getDocument(eleOrderLineItem, true),
		      "//OrderLine//Extn/@ExtnIsFreeGiftItem");
          sDependentOnLineKey = eleOrderLineItem.getAttribute("DependentOnLineKey");
          sOrderLineKey = eleOrderLineItem.getAttribute("OrderLineKey");
          elePickRequestLine.setAttribute("LineNo", sLineNo);
          elePickRequestLine.setAttribute("ItemID", sItemID);
          elePickRequestLine.setAttribute("ItemDesc", sItemDesc);
          elePickRequestLine.setAttribute("Quantity", sQuantity);
          elePickRequestLine.setAttribute("ShipNode", sShipNode);
          elePickRequestLine.setAttribute("FreeGiftItem", sFreeGiftItem);
          if(!YFCCommon.isVoid(sDependentOnLineKey)) {
        	  elePickRequestLine.setAttribute("DependentOnLineKey", sDependentOnLineKey);  
          }
          elePickRequestLine.setAttribute("OrderLineKey", sOrderLineKey);
          elePickRequestLine.setAttribute("RequestedShipDate",
              sReqShipDate);
          elePickRequestLine.setAttribute("Status", sStatus);
          elePickRequestLine.setAttribute("NodeType", sNodeType);
        }
      }
      return docPickRequestList;

    } catch (Exception e) {
      LOGGER.error("Inside GCPickRequestList.getPickRequestList():", e);
      throw new GCException(e);
    }
  }
}
