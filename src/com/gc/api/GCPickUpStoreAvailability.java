/**

 * Copyright © 2014, Guitar Center, All rights reserved.
 * *###########################################
 * ######################################################
 * ################################################################ OBJECTIVE: Foundation Class
 * #####
 * #############################################################################################
 * ############################################################### Version CR Date Modified By
 * Description
 * ######################################################################################
 * ########################################################################### 1.0 Initial Version
 * 02/03/2015 Shinde,Abhishek Wrapper Class for Find Inventory in View Store Screen 1.1 GCSTORE-328
 * 02/10/2015 Zuzar Inder Singh Added ExtnShiptoStoreAloowed flag check.
 * ############################
 * ######################################################################
 * ###############################################################
 */

package com.gc.api;

import java.io.IOException;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.api.ycp.page.getPage;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class GCPickUpStoreAvailability implements YIFCustomApi  {
  // class comments
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCPickUpStoreAvailability.class.getName());

  /**
   * getStoreAvailaility method creates input for GCfindInventoryWrapperService and creates the
   * output xml for the View Store Screen
   *
   * @param env
   * @param inDoc
   * @return
   * @throws ParseException
   * @throws NumberFormatException
 * @throws ParserConfigurationException 
 * @throws IOException 
 * @throws SAXException 
   */
  public Document getStoreAvailability(YFSEnvironment env, Document inDoc) throws NumberFormatException, ParseException, ParserConfigurationException, SAXException, IOException {
    LOGGER.beginTimer("GCPickUpStoreAvailability.getStoreAvailabiliy");
    
    //GCSTORE 6074 BEGIN
    
    int pageNum = 1;
    int pageSize = 5;
    //GCSTORE 6074 END
    Double dRequiredQty = 1.0;
    YFCDocument doc = YFCDocument.getDocumentFor(inDoc);
    
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input to getStoreAvailability " + doc.toString());
    }
    YFCElement eleAlternateStoreSearch = doc.getDocumentElement();
    
    // GCSTORE 6074 START
    String sPageNum=eleAlternateStoreSearch.getAttribute("PageNumber");
    String sPageSize=eleAlternateStoreSearch.getAttribute("PageSize");
    
    pageNum = Integer.parseInt(sPageNum);
    pageSize = Integer.parseInt(sPageSize);
    eleAlternateStoreSearch.removeAttribute("PageNumber");
    eleAlternateStoreSearch.removeAttribute("PageSize");
    // GCSTORE 6074 END
    
    String sOrganizationCode = eleAlternateStoreSearch.getAttribute(GCConstants.ORGANIZATION_CODE);
    YFCElement eleNodeSearch = eleAlternateStoreSearch.getChildElement(GCConstants.NODE_SEARCH);
    YFCElement eleShipToAddress = eleNodeSearch.getChildElement(GCConstants.SHIP_TO_ADDRESS);
    String sDistanceToConsider = eleNodeSearch.getAttribute("DistanceToConsider");
    String sCity = eleShipToAddress.getAttribute(GCConstants.CITY);
    String sState = eleShipToAddress.getAttribute(GCConstants.STATE);
    String sCountry = eleShipToAddress.getAttribute(GCConstants.COUNTRY);
    String sZipCode = eleShipToAddress.getAttribute(GCConstants.ZIP_CODE);
    YFCElement eleOrderLines = eleAlternateStoreSearch.getChildElement(GCConstants.ORDER_LINES);
    YFCElement eleOrderLine = eleOrderLines.getChildElement(GCConstants.ORDER_LINE);
    // create variable instead of two getAttribute
    // ele.getDouble
    if (!YFCCommon.isVoid(eleOrderLine.getAttribute(GCConstants.REQUIRED_QTY))) {
      String sRequiredQty = eleOrderLine.getAttribute(GCConstants.REQUIRED_QTY);
      dRequiredQty = Double.parseDouble(sRequiredQty);
    }
    YFCElement eleItem = eleOrderLine.getChildElement(GCConstants.ITEM);
    YFCElement eleExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
    String sExtnIsStoreClearance = "";
    String sExtnUnitPrice = "";
    if (!YFCCommon.isVoid(eleExtn)) {
      sExtnIsStoreClearance = eleExtn.getAttribute(GCXmlLiterals.EXTN_IS_STORE_CLEARANCE);
      sExtnUnitPrice = eleExtn.getAttribute(GCXmlLiterals.EXTN_UNIT_PRICE);
    }
    String sItemID = eleItem.getAttribute(GCConstants.ITEM_ID);
    String sProductClass = eleItem.getAttribute(GCConstants.PRODUCT_CLASS);
    String sUnitOfMeasure = eleItem.getAttribute(GCConstants.UNIT_OF_MEASURE);
    String sKitCode = eleItem.getAttribute(GCConstants.KIT_CODE);
    String sIsVintage = eleItem.getAttribute("IsVintage");
    YFCElement eleTag = eleItem.getChildElement(GCXmlLiterals.TAG);
    YFCDocument outDocument = YFCDocument.createDocument(GCXmlLiterals.ALTERNATE_STORES);
    YFCElement eleAlternateStores = outDocument.getDocumentElement();
    eleAlternateStores.setAttribute(GCConstants.ORGANIZATION_CODE, sOrganizationCode);
    YFCElement eleOutNodeList = outDocument.createElement(GCConstants.NODE_LIST);
    YFCDocument multiAPIInDoc = YFCDocument.getDocumentFor(GCConstants.MULTIP_API_INPUT);

    YFCElement eleInput;
    YFCElement eleGetSurroundingNodeList;
    YFCNodeList<YFCElement> nlAPIs = multiAPIInDoc.getElementsByTagName(GCXmlLiterals.API);
    for (YFCElement eleAPI : nlAPIs) {
      eleInput = eleAPI.getChildElement(GCXmlLiterals.INPUT);
      eleGetSurroundingNodeList = eleInput.getChildElement(GCConstants.GET_SURROUNDING_NODE_LIST_ELEMENT);
      eleGetSurroundingNodeList.setAttribute(GCConstants.ORGANIZATION_CODE, sOrganizationCode);
      eleShipToAddress = eleGetSurroundingNodeList.getChildElement(GCConstants.SHIP_TO_ADDRESS);
      eleShipToAddress.setAttribute(GCConstants.CITY, sCity);
      eleShipToAddress.setAttribute(GCConstants.STATE, sState);
      eleShipToAddress.setAttribute(GCConstants.COUNTRY, sCountry);
      eleShipToAddress.setAttribute(GCConstants.ZIP_CODE, sZipCode);
      String sNodeType = eleGetSurroundingNodeList.getAttribute(GCConstants.NODE_TYPE);
      if (YFCCommon.equals("Store", sNodeType)) {
        if (YFCCommon.equals(sIsVintage, "Y")) {
          eleGetSurroundingNodeList.setAttribute(GCXmlLiterals.DISTANCE_TO_CONSIDER, "999999");
        } else {
          eleGetSurroundingNodeList.setAttribute(GCXmlLiterals.DISTANCE_TO_CONSIDER, sDistanceToConsider);
        }
      }
    }
    YFCDocument templateDoc = YFCDocument.createDocument(GCXmlLiterals.MULTI_API);
    YFCDocument multiRespDoc = GCCommonUtil.invokeAPI(env, GCXmlLiterals.MULTIAPI, multiAPIInDoc, templateDoc);
    YFCElement eleRespMultiApi = multiRespDoc.getDocumentElement();
    YFCNodeList<YFCElement> nlAPIout = eleRespMultiApi.getElementsByTagName(GCXmlLiterals.API);
    YFCNodeList<YFCElement> nlTempNodeList = multiRespDoc.getElementsByTagName(GCConstants.NODE);
    YFCDocument docPromise = YFCDocument.getDocumentFor(GCConstants.ORDER_PROMISE_INPUT);
    YFCElement elePromise = docPromise.getDocumentElement();
    elePromise.setAttribute(GCConstants.ORGANIZATION_CODE, sOrganizationCode);
    YFCElement elePromiseLines = elePromise.getChildElement(GCConstants.PROMISE_LINES);
    YFCElement elePromiseLine = elePromiseLines.getChildElement(GCConstants.PROMISE_LINE);
    YFCElement eleShipNodes = elePromiseLine.getChildElement(GCConstants.SHIP_NODES);
    elePromiseLine.setAttribute(GCConstants.ITEM_ID, sItemID);
    elePromiseLine.setAttribute(GCConstants.PRODUCT_CLASS, sProductClass);
    elePromiseLine.setAttribute(GCConstants.UNIT_OF_MEASURE, sUnitOfMeasure);

    YFCElement eleExtnin = elePromiseLine.getChildElement(GCConstants.EXTN);
    eleExtnin.setAttribute(GCXmlLiterals.EXTN_IS_STORE_CLEARANCE, sExtnIsStoreClearance);
    eleExtnin.setAttribute(GCXmlLiterals.EXTN_UNIT_PRICE, sExtnUnitPrice);
    if (!YFCCommon.isVoid(eleTag)) {
      String sBatchNo = eleTag.getAttribute(GCXmlLiterals.BATCH_NO);
      YFCElement eleTagin = docPromise.createElement(GCXmlLiterals.TAG);
      eleTagin.setAttribute(GCXmlLiterals.BATCH_NO, sBatchNo);
    }
    for (YFCElement eleAPI : nlAPIout) {
      YFCElement eleOutput = eleAPI.getChildElement(GCXmlLiterals.OUTPUT);
      YFCElement eleNewGetSurroundingNodeList =
          eleOutput.getChildElement(GCConstants.GET_SURROUNDING_NODE_LIST_ELEMENT);
      YFCNodeList<YFCElement> nlNodeList = eleNewGetSurroundingNodeList.getElementsByTagName(GCConstants.NODE);
      for (YFCElement eleNode : nlNodeList) {
        YFCElement eleShipNode = docPromise.createElement(GCConstants.SHIP_NODE);
        // GCSTORE-328 Start
        String sNodeOrgCode = eleNode.getAttribute(GCXmlLiterals.NODE_ORG_CODE);

        eleShipNode.setAttribute(GCConstants.NODE, sNodeOrgCode);
        if (!YFCCommon.equals(sExtnIsStoreClearance, "Y") || !YFCCommon.equals(sNodeOrgCode, "MFI")) {
          eleShipNodes.appendChild(eleShipNode);
        }

        // GCSTORE-328 End

      }
    }
    if (!YFCCommon.equals(nlTempNodeList.getLength(), 0)) {
      Map<String, YFCElement> assignMap = checkAvailableInventory(env, docPromise, dRequiredQty, sKitCode, sItemID);
      for (YFCElement eletempNode : nlTempNodeList) {
        String sNodeOrgCode = eletempNode.getAttribute(GCXmlLiterals.NODE_ORG_CODE);
        if (assignMap.containsKey(sNodeOrgCode)) {
          YFCElement eleoutNode = outDocument.createElement(GCConstants.NODE);
          GCXMLUtil.copyElement(outDocument, eletempNode, eleoutNode);
          YFCElement eleAvailability = eleoutNode.createChild(GCConstants.AVAILABILITY);
          YFCElement elenew = assignMap.get(sNodeOrgCode);
          if (YFCCommon.equals(GCConstants.BUNDLE, sKitCode, true)) {
            eleAvailability.setAttribute(GCXmlLiterals.AVAILABLE_QTY, elenew.getAttribute(GCXmlLiterals.INV_QTY));
            eleAvailability.setAttribute(GCXmlLiterals.FIRST_AVAILABLE_DATE,
                elenew.getAttribute(GCConstants.PRODUCT_AVAIL_DATE));
          } else {
            int supply = 0;

            YFCNodeList<YFCElement> nlSupply = elenew.getElementsByTagName(GCConstants.SUPPLY);
            for (YFCElement eleSupply : nlSupply) {

                int supplyAv = Integer.parseInt(eleSupply.getAttribute(GCConstants.AVAILABLE_QUANTIY));
                supply = supplyAv + supply;

              
            }
            eleAvailability.setAttribute(GCXmlLiterals.AVAILABLE_QTY, Integer.toString(supply));
            eleAvailability.setAttribute(GCXmlLiterals.FIRST_AVAILABLE_DATE, 
					elenew.getAttribute(GCConstants.FIRST_SHIP_DATE));
          }
          eleAvailability.setAttribute(GCConstants.SHIP_NODE, sNodeOrgCode);
          eleAvailability.setAttribute(GCXmlLiterals.IS_AVAILABLE, GCConstants.YES);
          eleoutNode.appendChild(eleAvailability);
          eleOutNodeList.appendChild(eleoutNode);
        } else {
          if (YFCCommon.equals(GCConstants.MFI, sNodeOrgCode)) {
            YFCElement eleoutNode = outDocument.createElement(GCConstants.NODE);
            GCXMLUtil.copyElement(outDocument, eletempNode, eleoutNode);
            YFCElement eleAvailability = eleoutNode.createChild(GCConstants.AVAILABILITY);
            eleAvailability.setAttribute(GCXmlLiterals.AVAILABLE_QTY, GCConstants.ZERO);
            eleAvailability.setAttribute(GCXmlLiterals.FIRST_AVAILABLE_DATE, "");
            eleAvailability.setAttribute(GCConstants.SHIP_NODE, GCConstants.MFI);
            eleAvailability.setAttribute(GCXmlLiterals.IS_AVAILABLE, GCConstants.NO);
            eleoutNode.appendChild(eleAvailability);
            eleOutNodeList.appendChild(eleoutNode);
          }
        }
      }
    }
    eleAlternateStores.appendChild(eleOutNodeList);
    
    LOGGER.verbose("outDocument before transformation :" + outDocument.toString());
    updateAvailability(env, outDocument, sItemID, sProductClass, sUnitOfMeasure, sOrganizationCode, sKitCode);
    LOGGER.verbose("outDocument after transformation :" + outDocument.toString());
    LOGGER.verbose("outDocument before sorting :" + outDocument.toString());
    outDocument = sortOutDocByDistance(env,outDocument);
    outDocument = trimOutDoc(env, outDocument, pageNum, pageSize);
    LOGGER.verbose("outDocument after trimming to fit the page:" + outDocument.toString());
    LOGGER.endTimer("GCPickUpStoreAvailability.getStoreAvailabiliy");
    
    return outDocument.getDocument();
  }
  
  
  //GCTSORE -- 6074 START
  private YFCDocument trimOutDoc(YFSEnvironment env, YFCDocument sortedDoc, int pageNum, int pageSize) {
		// TODO Auto-generated method stub
		  YFCElement altStoreEle = sortedDoc.getDocumentElement();
		  YFCElement nodeListEle = altStoreEle.getChildElement(GCConstants.NODE_LIST);
		  YFCNodeList<YFCElement> nodesList = nodeListEle.getElementsByTagName(GCConstants.NODE);
		  
		  List<YFCElement> listNodeList = new ArrayList<YFCElement>(
				  					nodesList.getLength());
			
		  for (YFCElement node : nodesList ) {
			  listNodeList.add(node);
		  }
		  
		  int start = (pageNum*pageSize) - (pageSize);
		  int end = (pageNum*pageSize);
		  int length = end - start;
		  
		  YFCDocument requiredElementsDoc = YFCDocument.createDocument("AlternateStores");
		  YFCElement requiredElementsDocRoot = requiredElementsDoc.getDocumentElement();
		  GCXMLUtil.copyAttributes(nodeListEle, requiredElementsDocRoot);
		  requiredElementsDocRoot.createChild("NodeList");
		  YFCElement eleNewNodeList = requiredElementsDocRoot.getChildElement("NodeList");

		  for (int i = start; i < listNodeList.size() && i < end ; i++ ){
			  YFCElement elenewNode = requiredElementsDoc .createElement("Node");
			  GCXMLUtil.copyElement(requiredElementsDoc , listNodeList.get(i), elenewNode);
			  eleNewNodeList.appendChild(elenewNode);
		  }
		  
		  
		  
		return requiredElementsDoc;
	}

  private YFCDocument sortOutDocByDistance(YFSEnvironment env, YFCDocument outDocument) {
	// TODO Auto-generated method stub
	  
	  LOGGER.beginTimer("sortOutDocByDistance");
	    if (LOGGER.isDebugEnabled()) {
		      LOGGER.debug("Incoming doc for sorting" + GCXMLUtil.getXMLString(outDocument.getDocument()));
		}
	  
	 
		YFCElement alternateStoreEle = outDocument.getDocumentElement();
		YFCElement nodeListEle = alternateStoreEle
				.getChildElement("NodeList");

		YFCNodeList<YFCElement> nodeList = nodeListEle
				.getElementsByTagName("Node");
		if(nodeList.getLength() > 1){
			List<YFCElement> newNodeListStores = new ArrayList<YFCElement>(
					nodeList.getLength()-1);
			List<YFCElement> newNodeListMFI= new ArrayList<YFCElement>(1);
			
			for (int i = 0; i < nodeList.getLength() ; i++ ){
				if(i==0){
					newNodeListMFI.add(nodeList.item(i));
				}
				else{
					newNodeListStores.add(nodeList.item(i));
				}
				
			}
			
			Collections.sort(newNodeListStores, new DistanceComparator());
			
			YFCDocument newDoc = YFCDocument.createDocument("AlternateStores");
			YFCElement eleNewRoot = newDoc.getDocumentElement();
			GCXMLUtil.copyAttributes(alternateStoreEle, eleNewRoot);
			eleNewRoot.createChild("NodeList");
			YFCElement eleNewNodeList = eleNewRoot.getChildElement("NodeList");
			
			YFCElement elenewNode = newDoc.createElement("Node");
			GCXMLUtil.copyElement(newDoc, newNodeListMFI.get(0), elenewNode);
			eleNewNodeList.appendChild(elenewNode);
			
			for (YFCElement eleNode : newNodeListStores) {
				YFCElement elenewNode1 = newDoc.createElement("Node");
				GCXMLUtil.copyElement(newDoc, eleNode, elenewNode1);
				eleNewNodeList.appendChild(elenewNode1);
			}
			if (LOGGER.isDebugEnabled()) {
			      LOGGER.debug("Outgoing doc for sorting" + GCXMLUtil.getXMLString(newDoc.getDocument()));
			}
			return newDoc;
		}else{
			if (LOGGER.isDebugEnabled()) {
			      LOGGER.debug("Outgoing doc for sorting" + GCXMLUtil.getXMLString(outDocument.getDocument()));
			}
			return outDocument;
			
		}
	
  }

  static class DistanceComparator implements Comparator<YFCElement>,Serializable {

		@Override
	public int compare(YFCElement elem1, YFCElement elem2) {
		
		/*YFCElement compPriceEle1 = elem1.getChildElement(GCConstants.COMPUTED_PRICE);
		YFCElement compPriceEle2 = elem2.getChildElement(GCConstants.COMPUTED_PRICE);
		*/
		
		Double distance1 = elem1.getDoubleAttribute("DistanceFromShipToAddress",0.0);
		Double distance2 = elem2.getDoubleAttribute("DistanceFromShipToAddress",0.0);
		
		return distance1.compareTo(distance2);
	}

}

  
  
  //GCTSORE -- 6074 END

/**
   * This method is used to update the availabilty on each node on the basis of ONHAND supply only
   * by calling getSupplyDetails API.
   *
   * @param env
   * @param outDocument
   * @param sItemID
   * @param sOrganizationCode
   * @param sUnitOfMeasure
   * @param sProductClass
   * @param sKitCode
   */
  private void updateAvailability(YFSEnvironment env, YFCDocument outDocument, String sItemID, String sProductClass,
      String sUnitOfMeasure, String sOrganizationCode, String sKitCode) {

    LOGGER.verbose("updateAvailability method: BEGIN");
    String availableQty = GCConstants.ZERO;
    YFCElement eleAltStores = outDocument.getDocumentElement();
    YFCElement eleNodeList = eleAltStores.getChildElement(GCConstants.NODE_LIST);
    YFCNodeList<YFCElement> nlNode = eleNodeList.getElementsByTagName(GCConstants.NODE);
    for (YFCElement eleNode : nlNode) {
      String nodeOrgCode = eleNode.getAttribute(GCXmlLiterals.NODE_ORG_CODE);
      if (!YFCUtils.equals(sKitCode, GCConstants.BUNDLE)) {
        availableQty = callGetSupplyDetails(env, nodeOrgCode, sItemID, sProductClass, sUnitOfMeasure, sOrganizationCode);
      } else {
        availableQty =
            handleBundleItems(env, outDocument, sItemID, sProductClass, sUnitOfMeasure, sOrganizationCode, sKitCode,
                nodeOrgCode);
      }
      LOGGER.verbose("availableQty : " + availableQty);
      YFCElement eleAvailability = eleNode.getChildElement("Availability");
      if (!YFCCommon.isVoid(eleAvailability)) {
        eleAvailability.setAttribute("AvailableQty", availableQty);
      }
    }
    LOGGER.verbose("updateAvailability method: END");
  }

  /**
   * This method is used to calculate on-hand availability of bundle items by calculating inventory
   * as min (C1Inv, C2Inv) where C1Inv and C2Inv is on-hand inventory of bundle components in the
   * node.
   *
   * @param env
   * @param outDocument
   * @param sItemID
   * @param sProductClass
   * @param sUnitOfMeasure
   * @param sOrganizationCode
   * @param sKitCode
   * @param nodeOrgCode
   * @return
   */
  private String handleBundleItems(YFSEnvironment env, YFCDocument outDocument, String sItemID, String sProductClass,
      String sUnitOfMeasure, String sOrganizationCode, String sKitCode, String nodeOrgCode) {
	  
	  LOGGER.verbose("handleBundleItems method: BEGIN");
	  
    YFCDocument itemListInDoc = YFCDocument.createDocument(GCConstants.ITEM);
    YFCElement eleItem = itemListInDoc.getDocumentElement();
    eleItem.setAttribute(GCConstants.ITEM_ID, sItemID);
    eleItem.setAttribute(GCConstants.PRODUCT_CLASS, sProductClass);
    eleItem.setAttribute(GCConstants.UOM, sUnitOfMeasure);
    eleItem.setAttribute(GCConstants.CALLING_ORGANIZATION_CODE, sOrganizationCode);

    Document docGetCompItmLstOut =
        GCCommonUtil.invokeAPI(env, itemListInDoc.getDocument(), GCConstants.API_GET_COMPLETE_ITEM_LIST,
            "/global/template/api/getCompleteItemList_ProductDetail.xml");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("getCompleteItemList Output" + GCXMLUtil.getXMLString(docGetCompItmLstOut));
    }

    YFCDocument itemLstOutDoc = YFCDocument.getDocumentFor(docGetCompItmLstOut);
    YFCElement eleOItemList = itemLstOutDoc.getDocumentElement();
    YFCElement eleOItem = eleOItemList.getChildElement(GCConstants.ITEM);
    YFCElement eleOComponents = eleOItem.getChildElement(GCConstants.COMPONENTS);
    String finalAvailQty = null;
    if (!YFCCommon.isVoid(eleOComponents)) {
      YFCNodeList<YFCElement> nlComponent = eleOComponents.getElementsByTagName(GCConstants.COMPONENT);
      for (YFCElement eleComponent : nlComponent) {
        int tempFinalAvailQty = 0;
        String compItemID = eleComponent.getAttribute(GCConstants.COMPONENT_ITEM_ID);
        String compAvailQty = GCConstants.ZERO;
        int kitQty = Integer.parseInt(eleComponent.getAttribute(GCConstants.KIT_QUANTITY));
        compAvailQty =
            callGetSupplyDetails(env, nodeOrgCode, compItemID, sProductClass, sUnitOfMeasure, sOrganizationCode);
        tempFinalAvailQty = Integer.parseInt(compAvailQty) / kitQty;

        if (YFCCommon.isVoid(finalAvailQty)) {
          finalAvailQty = String.valueOf(tempFinalAvailQty);
        } else if (tempFinalAvailQty < Integer.parseInt(finalAvailQty)) {
          finalAvailQty = String.valueOf(tempFinalAvailQty);
        }
      }
    }

    if (YFCCommon.isVoid(finalAvailQty)) {
      finalAvailQty = GCConstants.ZERO;
    }
    return finalAvailQty;
  }

  /**
   * This method is used to call getSupplyDetails api to fetch ONHAND Available quantity.
   *
   * @param env
   * @param nodeOrgCode
   * @param sItemID
   * @param sOrganizationCode
   * @param sUnitOfMeasure
   * @param sProductClass
   */
  private String callGetSupplyDetails(YFSEnvironment env, String nodeOrgCode, String sItemID, String sProductClass,
      String sUnitOfMeasure, String sOrganizationCode) {

    LOGGER.verbose("callGetSupplyDetails method: BEGIN");
    YFCDocument yfcIndoc = YFCDocument.createDocument("getSupplyDetails");
    YFCElement eleRoot = yfcIndoc.getDocumentElement();
    eleRoot.setAttribute(GCConstants.ORGANIZATION_CODE, sOrganizationCode);
    eleRoot.setAttribute(GCConstants.ITEM_ID, sItemID);
    eleRoot.setAttribute(GCConstants.SHIP_NODE, nodeOrgCode);
    eleRoot.setAttribute(GCConstants.PRODUCT_CLASS, sProductClass);
    eleRoot.setAttribute(GCConstants.UOM, sUnitOfMeasure);

    YFCDocument yfcgetSupplyDetailsTemp =
        YFCDocument
        .getDocumentFor("<Item ItemID='' ProductClass='' UnitOfMeasure=''><ShipNodes><ShipNode AvailableQty='' Priority='' "
            + "ShipNode='' Tracked=''><Supplies><Supply AvailableQty='' SupplyType='' TotalQuantity='' >"
            + "<SupplyDetails AvailabilityType='' ><ExactMatchedDemands TotalQuantity=''><DemandDetails OrganizationCode='' />"
            + "</ExactMatchedDemands></SupplyDetails></Supply></Supplies></ShipNode></ShipNodes></Item>");
    if (LOGGER.isVerboseEnabled()) {
    	LOGGER.verbose("inputDocument for  getSupplyDetails :" + yfcIndoc.toString());
    }
    YFCDocument yfcgetSupplyDtlOutput =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_SUPPLY_DETAILS, yfcIndoc, yfcgetSupplyDetailsTemp);
    if (LOGGER.isVerboseEnabled()) {
    	LOGGER.verbose("outputDocument for  getSupplyDetails :" + yfcgetSupplyDtlOutput.toString());
    }
    
    String availableQty = GCConstants.ZERO;
    //Fix for 6762
    String sSoftMatchedDemand = GCConstants.ZERO;
    int availQtyTemp = 0;
    //End
    Element eleAvailable =GCXMLUtil.getElementByXPath(yfcgetSupplyDtlOutput.getDocument(),
            "Item/ShipNodes/ShipNode/Supplies/Supply[@SupplyType='ONHAND']");
    if (!YFCCommon.isVoid(eleAvailable)) {
      availableQty = eleAvailable.getAttribute("AvailableQty");
    //Fix for 6762
      Element eleSoftMatchedDemand = (Element) eleAvailable.getElementsByTagName(GCConstants.SOFT_MATCH_DEMAND).item(0);
      if(!YFCCommon.isVoid(eleSoftMatchedDemand)){
    	  sSoftMatchedDemand =  eleSoftMatchedDemand.getAttribute("TotalQuantity"); 
    	  availQtyTemp = Integer.parseInt(availableQty)-Integer.parseInt(sSoftMatchedDemand);
    	  availableQty = String.valueOf(availQtyTemp);
      }
      //End
    }
    
    LOGGER.debug("Available Quantity before safety factor consideration at "+nodeOrgCode+" is "+availableQty);
    
    /*checking for safety factor quantity by calling getItemList and subtracting it */
    
    if(!YFCCommon.equals(nodeOrgCode, "MFI")){
    
	    YFCDocument yfcIndocItemList = YFCDocument.createDocument("Item");
	    YFCElement eleRootItemListIndoc = yfcIndocItemList.getDocumentElement();
	    eleRootItemListIndoc.setAttribute(GCConstants.CALLING_ORGANIZATION_CODE, sOrganizationCode);
	    eleRootItemListIndoc.setAttribute(GCConstants.ITEM_ID, sItemID);
	    eleRootItemListIndoc.setAttribute(GCConstants.PRODUCT_CLASS, sProductClass);
	    eleRootItemListIndoc.setAttribute(GCConstants.UOM, sUnitOfMeasure);
	    
	    YFCDocument yfcgetItemListTemp =
	            YFCDocument
	            .getDocumentFor("<ItemList> <Item> <InventoryParameters/> </Item> </ItemList>");
	    
	    if (LOGGER.isVerboseEnabled()) {
	    	LOGGER.verbose("inputDocument for  getItemList :" + yfcIndocItemList.toString());
	    }    
	    YFCDocument yfcgetItemListOutput =
	        GCCommonUtil.invokeAPI(env, GCConstants.GET_ITEM_LIST, yfcIndocItemList, yfcgetItemListTemp);
	    if (LOGGER.isVerboseEnabled()) {
	    	LOGGER.verbose("outputDocument for  getItemList :" + yfcgetItemListOutput.toString());
	    }
	    
	    String onHandSafetyFactor = GCConstants.ZERO;
	    
	    Element eleInvParameters =
	            GCXMLUtil.getElementByXPath(yfcgetItemListOutput.getDocument(),
	                "ItemList/Item/InventoryParameters");
	    
	    if (!YFCCommon.isVoid(eleInvParameters)) {
	    	onHandSafetyFactor = eleInvParameters.getAttribute("OnhandSafetyFactorQuantity");
	    	if(YFCCommon.isVoid(onHandSafetyFactor)){
	    		onHandSafetyFactor = GCConstants.ZERO;
	    	}
	    	
	    	int availableQtyInt = Integer.parseInt(availableQty) - Integer.parseInt(onHandSafetyFactor);
	    	
	    	if(availableQtyInt < 0){
	    		availableQty = GCConstants.ZERO;
	    	}
	    	else{
	    		availableQty = Integer.toString(availableQtyInt);
	    	}
	    }
    }

    /*checking for safety factor quantity by calling getItemList and subtracting it -- END*/
    LOGGER.debug("Available Quantity after safety factor consideration at "+nodeOrgCode+" is "+availableQty);
    
    LOGGER.verbose("callGetSupplyDetails method: END");
    return availableQty;
  }

  /**
   * checkAvailableInventory will invoke GCFindInventoryWrapperService and return the map Containing
   * the ShipNode as key and Available Quantity as value
   *
   * @param env
   * @param inDoc
   * @param dRequiredQty
   * @param sKitCode
   * @param sItemID
   * @return
   */
  public Map<String, YFCElement> checkAvailableInventory(YFSEnvironment env, YFCDocument inDoc, double dRequiredQty,
      String sKitCode, String sItemID) {
    LOGGER.beginTimer("GCPickUpStoreAvailability.checkAvailableInventory");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input to Inventory " + (inDoc.toString()));
    }
    YFCDocument promiseOutDoc = GCCommonUtil.invokeService(env, GCConstants.GC_FIND_INVENTORY_WRAPPER_SERVICE, inDoc);

    YFCElement elePromise = promiseOutDoc.getDocumentElement();
    String sOrganizationCode = elePromise.getAttribute(GCConstants.ORGANIZATION_CODE);
    YFCElement eleSuggestedOption = elePromise.getChildElement(GCXmlLiterals.SUGGESTED_OPTION);
    YFCElement eleOption = eleSuggestedOption.getChildElement(GCXmlLiterals.OPTION);
    Map<String, YFCElement> assignMap = new HashMap<String, YFCElement>();
    YFCNodeList<YFCElement> nlTemp = elePromise.getElementsByTagName(GCConstants.PROMISE_LINE);
    if (!YFCCommon.equals(nlTemp.getLength(), 0)) {

      if (!YFCCommon.equals(GCConstants.BUNDLE, sKitCode, true) || YFCCommon.isVoid(sKitCode)) {
        YFCElement elePromiseLines = eleOption.getChildElement(GCConstants.PROMISE_LINES);
        YFCElement elePromiseLine = elePromiseLines.getChildElement(GCConstants.PROMISE_LINE);
        YFCNodeList<YFCElement> nlAssignments = elePromiseLine.getElementsByTagName(GCXmlLiterals.ASSIGNMENT);
        for (YFCElement eleAssignment : nlAssignments) {
          String sShipNode = eleAssignment.getAttribute(GCConstants.SHIP_NODE);
          YFCElement eleSupplies = eleAssignment.getChildElement(GCConstants.SUPPLIES);
          YFCNodeList<YFCElement> nlSupplies = eleAssignment.getElementsByTagName(GCConstants.SUPPLY);
          if (!YFCCommon.equals(nlSupplies.getLength(), 0)) {
            YFCElement eleSupply = nlSupplies.item(0);
            String sAvailableQuantity = eleSupply.getAttribute(GCConstants.AVAILABLE_QUANTIY);
            double dAvailableQuantity = Double.parseDouble(sAvailableQuantity);
            if (dAvailableQuantity >= dRequiredQty && !YFCCommon.equals(GCConstants.MFI, sShipNode, false)) {
              assignMap.put(sShipNode, eleSupplies);
            } else {
              if (YFCCommon.equals(GCConstants.MFI, sShipNode, false)) {
                assignMap.put(sShipNode, eleSupplies);
              }
            }
          }
        }
        YFCDocument outDoc = YFCDocument.createDocument(GCXmlLiterals.ALTERNATE_STORES);
        YFCElement eleAlternateStores = outDoc.getDocumentElement();
        eleAlternateStores.setAttribute(GCConstants.ORGANIZATION_CODE, sOrganizationCode);
        YFCElement eleOptions = elePromise.getChildElement(GCXmlLiterals.OPTIONS);
        YFCNodeList<YFCElement> nlOption = eleOptions.getElementsByTagName(GCXmlLiterals.OPTION);
        for (YFCElement eleOption1 : nlOption) {
          elePromiseLines = eleOption1.getChildElement(GCConstants.PROMISE_LINES);
          elePromiseLine = elePromiseLines.getChildElement(GCConstants.PROMISE_LINE);
          YFCNodeList<YFCElement> nlnewAssignments = elePromiseLine.getElementsByTagName(GCXmlLiterals.ASSIGNMENT);
          for (YFCElement eleAssignment : nlnewAssignments) {
            String sShipNode = eleAssignment.getAttribute(GCConstants.SHIP_NODE);
            YFCNodeList<YFCElement> nlSupplies = eleAssignment.getElementsByTagName(GCConstants.SUPPLY);
            YFCElement eleSupplies = eleAssignment.getChildElement(GCConstants.SUPPLIES);
            if (!YFCCommon.equals(nlSupplies.getLength(), 0)) {
              YFCElement eleSupply = nlSupplies.item(0);
              double dAvailableQuantity = eleSupply.getDoubleAttribute(GCConstants.AVAILABLE_QUANTIY);
              if (dAvailableQuantity >= dRequiredQty && !YFCCommon.equals(GCConstants.MFI, sShipNode, false)) {
                assignMap.put(sShipNode, eleSupplies);
              } else if (YFCCommon.equals(GCConstants.MFI, sShipNode, false)) {
                assignMap.put(sShipNode, eleSupplies);

              }
            }
          }
        }
      } else {
        YFCNodeList<YFCElement> nlPromiseLine = promiseOutDoc.getElementsByTagName(GCConstants.PROMISE_LINE);
        for (YFCElement elePromiseLine : nlPromiseLine) {
          if (YFCCommon.equals(sItemID, elePromiseLine.getAttribute(GCConstants.ITEM_ID), true)) {
            YFCNodeList<YFCElement> nlAssignment = elePromiseLine.getElementsByTagName(GCXmlLiterals.ASSIGNMENT);
            for (YFCElement eleAssignment : nlAssignment) {

              double dAvailableQuantity = eleAssignment.getDoubleAttribute(GCXmlLiterals.INV_QTY);
              String sShipNode = eleAssignment.getAttribute(GCConstants.SHIP_NODE);
              if (!YFCCommon.isVoid(dAvailableQuantity) && !YFCCommon.equals(GCConstants.MFI, sShipNode, true)
                  && (dAvailableQuantity >= dRequiredQty)) {

                assignMap.put(sShipNode, eleAssignment);

              } else if (YFCCommon.equals(GCConstants.MFI, sShipNode, false)) {
                assignMap.put(sShipNode, eleAssignment);
              }

            }
          }
        }
      }

    }
    LOGGER.endTimer("GCPickUpStoreAvailability.checkAvailableInventory");
    return assignMap;
  }

  @Override
  public void setProperties(Properties prop) {

  }

}