/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               19/02/2014        Soni, Karan		       Catalog PriceList import component
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.util.HashSet;
import java.util.Set;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author karan soni
 */
public class GCPriceListUpdate {

    // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCPriceListUpdate.class.getName());
  
    public void updatePromoPriceList(YFSEnvironment env, Document docPriceList)
	    throws GCException {
	try {
	    LOGGER.verbose("Entering theupdatePromoPriceList method ");
	    String strOrganizationCode = docPriceList.getDocumentElement()
		    .getAttribute(GCConstants.ORGANIZATION_CODE);
	    Set<String> setPriceList = new HashSet<String>();
	    Document inDoc = GCXMLUtil.createDocument("PricelistLineList");
	    inDoc.getDocumentElement().setAttribute(
		    GCConstants.ORGANIZATION_CODE, strOrganizationCode);
	    LOGGER.debug("The Organization code is " + strOrganizationCode);
	    NodeList nlPricelistLine = GCXMLUtil.getNodeListByXpath(
		    docPriceList, "PricelistLineList/PricelistLine");
	    for (int i = 0; i < nlPricelistLine.getLength(); i++) {
		LOGGER.debug("Entering the for loop");
		Element elePricelistLineIn = (Element) nlPricelistLine.item(i);
		String strEndActiveDate = elePricelistLineIn
			.getAttribute(GCConstants.END_DATE_ACTIVE);
		String strStartDateActive = elePricelistLineIn
			.getAttribute(GCConstants.START_DATE_ACTIVE);
		String strPriceListName = GCXMLUtil.getAttributeFromXPath(
			elePricelistLineIn, "PricelistHeader/@PricelistName");
		LOGGER.debug("The strEndActiveDate" + strEndActiveDate
			+ "The strStartDateActive" + strStartDateActive
			+ "The strPriceListName" + strPriceListName);
		Element eledocPriceListHeader = null;
		if (!setPriceList.contains(strPriceListName)) {
		    String strGetPriceListHeader = "<PricelistHeader EndDateActive='"
			    + strEndActiveDate
			    + "' OrganizationCode='"
			    + strOrganizationCode
			    + "' StartDateActive='"
			    + strStartDateActive
			    + "' PricelistName='"
			    + strPriceListName + "'/>";
		    Document docGetPriceListHeader = GCXMLUtil
			    .getDocument(strGetPriceListHeader);
		    LOGGER.debug("The docGetPriceListHeader input doc "
			    + GCXMLUtil.getXMLString(docGetPriceListHeader));
		    Document docPriceListHeaderOut = GCCommonUtil.invokeAPI(
			    env, "getPricelistHeaderList",
			    docGetPriceListHeader);
		    eledocPriceListHeader = GCXMLUtil.getElementByXPath(
			    docPriceListHeaderOut,
			    "PricelistHeaderList/PricelistHeader");
		}
		if (YFCCommon.isVoid(eledocPriceListHeader)) {
		    String strManagePriceListHdr = "<PricelistHeader   Currency='USD' Description='"
			    + strPriceListName
			    + "' EndDateActive='"
			    + strEndActiveDate
			    + "' Operation='Manage' OrganizationCode='"
			    + strOrganizationCode
			    + "'  PricelistName='"
			    + strPriceListName
			    + "' PricingStatus='ACTIVE'  StartDateActive='"
			    + strStartDateActive + "'/>";
		    Document docManagePriceListHdr = GCXMLUtil
			    .getDocument(strManagePriceListHdr);
		    LOGGER.debug("The docManagePriceListHdr input document "
			    + GCXMLUtil.getXMLString(docManagePriceListHdr));
		    GCCommonUtil.invokeAPI(env, "managePricelistHeader",
			    docManagePriceListHdr);
		    String strCustomerAssgn = "<PricelistAssignment CustomerType='04' EnterpriseCode='"
			    + strOrganizationCode
			    + "'><PricelistHeader PricelistName='"
			    + strPriceListName + "'/></PricelistAssignment>";
		    Document docCustomerAssgn = GCXMLUtil
			    .getDocument(strCustomerAssgn);
		    LOGGER.debug("The docCustomerAssgn input document"
			    + GCXMLUtil.getXMLString(docCustomerAssgn));
		    GCCommonUtil.invokeAPI(env, "managePricelistAssignment",
			    docCustomerAssgn);
		}
		Element elePriceList = inDoc.createElement("PricelistLine");
		GCXMLUtil.copyAttributes(elePricelistLineIn, elePriceList);
		inDoc.getDocumentElement().appendChild(elePriceList);
		Element elePriceListHdr = inDoc
			.createElement("PricelistHeader");
		elePriceListHdr.setAttribute("PricelistName", strPriceListName);
		elePriceList.appendChild(elePriceListHdr);
		LOGGER.debug("The inDoc input document"
			+ GCXMLUtil.getXMLString(inDoc));
		GCCommonUtil.invokeAPI(env, "managePricelistLine", inDoc);
		setPriceList.add(strPriceListName);
		LOGGER.verbose("Exiting theupdatePromoPriceList method ");
	    }
	} catch (Exception e) {
	    LOGGER.error("Inside GCPriceListUpdate.updatePromoPriceList():", e);
	    throw new GCException(e);
	}
    }
}
