/**
 * Copyright � 2017, Guitar Center, All rights reserved.
 * *#################################################################################################################################################################
 *                                          OBJECTIVE: Handle the functionalities required for handling DAX messages to OMS for canceling BackOrdered Status OrderLines
 * #################################################################################################################################################################
 * Version          Date                Modified By                 Description
 * #################################################################################################################################################################
 * 1.0              18/05/2017          Expicient              GCSTORE-6548: Development - How to handle Feedback to OMS from DAX to permanently cancel a back-ordered OMS item
 * * #################################################################################################################################################################
 */

package com.gc.api;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCProcessBackOrderCancellation {

	// initialize LOGGER
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCProcessBackOrderCancellation.class.getName());

	public void processBOCancellation(YFSEnvironment env, Document inDoc)
			throws GCException {

		LOGGER.beginTimer("GCProcessBOCancellation");
		LOGGER.verbose("Class: GCProcessBOCancellation Method: processBOCancellation BEGIN");

		YFCDocument yfcDocinDoc = YFCDocument.getDocumentFor(inDoc);
		if (LOGGER.isDebugEnabled()){
		LOGGER.verbose("Input Document to the method:" +yfcDocinDoc);
		}

		validateMandatoryParameters(inDoc);
		YFCElement yfcEleinDocRoot = yfcDocinDoc.getDocumentElement();
		LOGGER.verbose("Root element of Input Doc:" +yfcEleinDocRoot);
		
		YFCElement yfcEleRoot = yfcDocinDoc.getDocumentElement();
		String sOrderNo = yfcEleRoot.getAttribute(GCConstants.ORDER_NO);
		String sEntCode = yfcEleRoot.getAttribute(GCConstants.ENTERPRISE_CODE);
		YFCElement yfcEleOrderLines = yfcEleRoot.getChildElement(GCConstants.ORDER_LINES);
		YFCElement yfcEleOrderLine = yfcEleOrderLines.getChildElement(GCConstants.ORDER_LINE);
		String sQuantityToCancel = yfcEleOrderLine.getAttribute(GCConstants.QUANTITY_TO_CANCEL);
		
		Double dQuantityToCancel = Double.parseDouble(sQuantityToCancel);
		
		LOGGER.verbose("OrderNo:" +sOrderNo);
		LOGGER.verbose("EnterpriseCode:" +sEntCode);
		
		Document getOrderListIn = GCXMLUtil.getDocument("<Order OrderNo ='" + sOrderNo + "' EnterpriseCode = '" +sEntCode+ "'/>");	
		if (LOGGER.isDebugEnabled()){
		LOGGER.verbose("getOrderList Input:" +getOrderListIn);
		}
		
		Document getOrderListTemplate = GCCommonUtil.getDocumentFromPath("/global/template/api/getOrderList.xml");
		
		Document docGetOrderListOutput = GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST, getOrderListIn,getOrderListTemplate );
		if (LOGGER.isDebugEnabled()){
		LOGGER.verbose("getOrderList Output:" +docGetOrderListOutput);
		}	
		
		YFCDocument yfcDocGetOrderListOutput = YFCDocument.getDocumentFor(docGetOrderListOutput);
		if (LOGGER.isDebugEnabled()){
			LOGGER.verbose("yfcDocGetOrderListOutput Output:" +yfcDocGetOrderListOutput);
			}	
		//extracting PrimeLineNo and SubLineNo from the input feed
		String sIpPrimeLineNo = GCXMLUtil.getAttributeFromXPath(inDoc, "Order/OrderLines/OrderLine/@PrimeLineNo");
		String sIpSubLineNo = GCXMLUtil.getAttributeFromXPath(inDoc, "Order/OrderLines/OrderLine/@SubLineNo");
		
		//searching the output of the getOrderList using the PrimeLineNo and SubLineNo to get the correct orderline
		 Element eleOrderLine = GCXMLUtil.getElementByXPath(docGetOrderListOutput, "//Order/OrderLines/OrderLine" + "[@PrimeLineNo='" 
				 	+ sIpPrimeLineNo + "' " + "and @SubLineNo='" + sIpSubLineNo + "']");
		
		 //fetch bundle parent order line key
		 String sBundleParentOrderLineKey = eleOrderLine.getAttribute(GCConstants.BUNDLE_PARENT_ORDERLINEKEY);
		 
		 //fetch ExtnSetCode for differentiating between SET and KIT
		 Element eleItemDetails = (Element) eleOrderLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
		 Element eleExtnItemDetails = (Element) eleItemDetails.getElementsByTagName(GCConstants.EXTN).item(0);
		 String sExtnSetCode = eleExtnItemDetails.getAttribute(GCConstants.EXTN_SET_CODE);
		 
		 
		 //fetch the orderedQty from the output of getOrderList
		 String sOrderedQty = eleOrderLine.getAttribute(GCConstants.ORDERED_QUANITY);
			LOGGER.verbose("sOrderedQty:" +sOrderedQty);
			Double dOrderedQty = Double.parseDouble(sOrderedQty);
			LOGGER.verbose("dOrderedQty:" +dOrderedQty);
			Double dQtyToKeep = 0.00;
			
			dQtyToKeep = dOrderedQty-dQuantityToCancel;
			LOGGER.verbose("dQtyToKeep:" +dQtyToKeep);
			
			if(YFCCommon.isVoid(sBundleParentOrderLineKey))
			{	
				//condition when BundleParentOrderLineKey is not present - for REGULAR item
			if (dQtyToKeep<0)
			{	
				//setting the OrderedQty to zero because the quantity to order is greater than existing quantity
				yfcEleinDocRoot.getElementsByTagName(GCConstants.ORDER_LINE).item(0).setAttribute(GCConstants.ORDERED_QUANITY, GCConstants.ZERO);
				LOGGER.verbose("yfcDocinDoc:" +yfcDocinDoc);
				modifyWarrLineschangeOrderAPICall(yfcDocinDoc, env, yfcDocGetOrderListOutput, docGetOrderListOutput, sIpPrimeLineNo, 
						sIpSubLineNo, sExtnSetCode, dQtyToKeep, sBundleParentOrderLineKey, inDoc);
			}
			else
			{	//setting the OrderedQty to calculated Quantity
				yfcEleinDocRoot.getElementsByTagName(GCConstants.ORDER_LINE).item(0).setAttribute(GCConstants.ORDERED_QUANITY, dQtyToKeep);
				LOGGER.verbose("For REGULAR when OrderedQty being reduced: yfcDocinDoc:" +yfcDocinDoc);
				modifyWarrLineschangeOrderAPICall(yfcDocinDoc, env, yfcDocGetOrderListOutput, docGetOrderListOutput, sIpPrimeLineNo, 
						sIpSubLineNo, sExtnSetCode, dQtyToKeep, sBundleParentOrderLineKey, inDoc);
			}
		}  
			else
			{	//condition when BundleParentOrderLineKey is present
				LOGGER.verbose("sBundleParentOrderLineKey:" +sBundleParentOrderLineKey);
				//obtaining the orderline where the bundleParentOLK is equal to OLK
				Element eleBundleOrderLine = GCXMLUtil.getElementByXPath(docGetOrderListOutput, "//Order/OrderLines/OrderLine[@OrderLineKey='"
					       + sBundleParentOrderLineKey + "']");	
				//extract out the primelineno and sublineno of the above orderline
				String sParentPrimeLineNo = eleBundleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
				String sParentSubLineNo = eleBundleOrderLine.getAttribute(GCConstants.SUB_LINE_NO);
				//setting the new values of primelineno and sublineno in the input feed for calling changeOrderAPI, going to the ParentOrderLine
				yfcEleinDocRoot.getElementsByTagName(GCConstants.ORDER_LINE).item(0).setAttribute(GCConstants.PRIME_LINE_NO, sParentPrimeLineNo);
				yfcEleinDocRoot.getElementsByTagName(GCConstants.ORDER_LINE).item(0).setAttribute(GCConstants.SUB_LINE_NO, sParentSubLineNo);
				
				//check for SET parent line being sent in the input feed
				if(YFCUtils.equals(GCConstants.ONE, sExtnSetCode))
				{
					if (dQtyToKeep<0)
					{	//setting the OrderedQty to zero because the quantity to order is greater than existing quantity
						yfcEleinDocRoot.getElementsByTagName(GCConstants.ORDER_LINE).item(0).setAttribute(GCConstants.ORDERED_QUANITY, GCConstants.ZERO);
						LOGGER.verbose("For SET items where orderedQty reduced to zero: yfcDocinDoc:" +yfcDocinDoc);
						modifyWarrLineschangeOrderAPICall(yfcDocinDoc, env, yfcDocGetOrderListOutput, docGetOrderListOutput, sIpPrimeLineNo, 
								sIpSubLineNo, sExtnSetCode, dQtyToKeep, sBundleParentOrderLineKey, inDoc);
						}
					else
					{	//setting the OrderedQty to calculated Quantity
						yfcEleinDocRoot.getElementsByTagName(GCConstants.ORDER_LINE).item(0).setAttribute(GCConstants.ORDERED_QUANITY, dQtyToKeep.toString());
						LOGGER.verbose("For SET item where OrderedQty is being reduced: yfcDocinDoc:" +yfcDocinDoc);
						modifyWarrLineschangeOrderAPICall(yfcDocinDoc, env, yfcDocGetOrderListOutput, docGetOrderListOutput, sIpPrimeLineNo, 
								sIpSubLineNo, sExtnSetCode, dQtyToKeep, sBundleParentOrderLineKey, inDoc);
						}
				}
				//the condition for handling KIT components being sent in the input feed
				else
				{	
					NodeList eleKitBundleComponent = eleBundleOrderLine.getElementsByTagName(GCConstants.BUNDLE_COMPONENT);
					
					for(int  count=0; count<eleKitBundleComponent.getLength(); count++)
					{	
						Element eleKitCompBundle = (Element) eleKitBundleComponent.item(count); 
						//fetching the primelineno and sublineno of all the bundle components
						String sCompPrimeLineNo = eleKitCompBundle.getAttribute(GCConstants.PRIME_LINE_NO);
						String sCompSubLineNo = eleKitCompBundle.getAttribute(GCConstants.SUB_LINE_NO);
						
						if(YFCUtils.equals(GCConstants.ZERO, Integer.toString(count)))
						{
						//setting the primelineno and sublineno obtained in the input feed for the changeOrderAPI call
						yfcEleinDocRoot.getElementsByTagName(GCConstants.ORDER_LINE).item(0).setAttribute(GCConstants.PRIME_LINE_NO, sCompPrimeLineNo);
						yfcEleinDocRoot.getElementsByTagName(GCConstants.ORDER_LINE).item(0).setAttribute(GCConstants.SUB_LINE_NO, sCompSubLineNo);
						yfcEleinDocRoot.getElementsByTagName(GCConstants.ORDER_LINE).item(0).setAttribute(GCConstants.ORDERED_QUANITY, GCConstants.ZERO);
						LOGGER.verbose("For KIT items: yfcDocinDoc after 1st iteration:" +yfcDocinDoc);
						}
						else
						{
						YFCElement yfcEleinDocAppendOrderLines = yfcEleinDocRoot.getChildElement(GCConstants.ORDER_LINES);
						YFCElement yfcEleinDocAppendOrderLine = yfcEleinDocAppendOrderLines.createChild(GCConstants.ORDER_LINE);
						yfcEleinDocAppendOrderLine.setAttribute(GCConstants.PRIME_LINE_NO, sCompPrimeLineNo);
						yfcEleinDocAppendOrderLine.setAttribute(GCConstants.SUB_LINE_NO, sCompSubLineNo);
						yfcEleinDocAppendOrderLine.setAttribute(GCConstants.ORDERED_QUANITY, GCConstants.ZERO);
						LOGGER.verbose("For KIT items: yfcDocinDoc after adding additional OrderLine:" +yfcDocinDoc);
						}
					}
					modifyWarrLineschangeOrderAPICall(yfcDocinDoc, env, yfcDocGetOrderListOutput, docGetOrderListOutput, sIpPrimeLineNo, 
							sIpSubLineNo, sExtnSetCode, dQtyToKeep, sBundleParentOrderLineKey, inDoc);
				}
			}
			}
	
	private void modifyWarrLineschangeOrderAPICall(YFCDocument yfcDocinDoc, YFSEnvironment env, YFCDocument yfcDocGetOrderListOutput, Document docGetOrderListOutput, 
			String sIpPrimeLineNo, String sIpSubLineNo, String sExtnSetCode, Double dQtyToKeep, String sBundleParentOrderLineKey, Document inDoc  )
	{	
	YFCDocument yfcDocInDocWarrLineModified = modifyWarrantyLines(yfcDocGetOrderListOutput, docGetOrderListOutput, sIpPrimeLineNo, sIpSubLineNo, sExtnSetCode, dQtyToKeep,
			sBundleParentOrderLineKey, env, inDoc);
		GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, yfcDocInDocWarrLineModified.getDocument());
	}

	private void validateMandatoryParameters(Document inDoc) throws GCException {

		String sBackOrderCancelMsg = GCXMLUtil.getAttributeFromXPath(inDoc, "Order/@BackOrderCancelMsg");
		if (YFCCommon.isVoid(sBackOrderCancelMsg)) {
            LOGGER.debug("Throwing BackOrderCancelMsg is Missing' exception ");
            GCException gcException = new GCException("BackOrderCancelMsg is Missing ");
            throw gcException;
          }
		
		String sOrderNo = GCXMLUtil.getAttributeFromXPath(inDoc, "Order/@OrderNo");
		if (YFCCommon.isVoid(sOrderNo)) {
            LOGGER.debug("Throwing OrderNo is Missing' exception ");
            GCException gcException = new GCException("Order Number is Missing ");
            throw gcException;
          }
		
		String sDocType = GCXMLUtil.getAttributeFromXPath(inDoc, "Order/@DocumentType");
		if (YFCCommon.isVoid(sDocType)) {
            LOGGER.debug(" Throwing DocumentType is Missing' exception ");
            GCException gcException = new GCException("Document Type is Missing ");
            throw gcException;
          }
		
		String sEntCode = GCXMLUtil.getAttributeFromXPath(inDoc, "Order/@EnterpriseCode");
		if (YFCCommon.isVoid(sEntCode)) {
            LOGGER.debug(" Throwing EnterpriseCode is Missing' exception ");
            GCException gcException = new GCException("Enterprise Code is Missing ");
            throw gcException;
          }
		
		String sModReasonCode = GCXMLUtil.getAttributeFromXPath(inDoc, "Order/@ModificationReasonCode");
		if (YFCCommon.isVoid(sModReasonCode)) {
            LOGGER.debug(" Throwing ModificationReasonCode is Missing' exception ");
            GCException gcException = new GCException("ModificationReasonCode is Missing ");
            throw gcException;
          }
		
		String sQuantityToCancel = GCXMLUtil.getAttributeFromXPath(inDoc, "Order/OrderLines/OrderLine/@QuantityToCancel");
		if (YFCCommon.isVoid(sQuantityToCancel)) {
            LOGGER.debug(" Throwing QuantityToCancel is Missing' exception ");
            GCException gcException = new GCException("QuantityToCancel is Missing ");
            throw gcException;
          }
		
		String sPrimeLineNo = GCXMLUtil.getAttributeFromXPath(inDoc, "Order/OrderLines/OrderLine/@PrimeLineNo");
		if (YFCCommon.isVoid(sPrimeLineNo)) {
            LOGGER.debug(" Throwing PrimeLineNo is Missing' exception ");
            GCException gcException = new GCException("PrimeLineNo is Missing ");
            throw gcException;
          }
		
		String sSubLineNo = GCXMLUtil.getAttributeFromXPath(inDoc, "Order/OrderLines/OrderLine/@SubLineNo");
		if (YFCCommon.isVoid(sSubLineNo)) {
            LOGGER.debug(" Throwing SubLineNo is Missing' exception ");
            GCException gcException = new GCException("SubLineNo is Missing ");
            throw gcException;
          }
		
	}
	
private YFCDocument modifyWarrantyLines(YFCDocument yfcDocGetOrderListOutput, Document docGetOrderListOutput, String sIpPrimeLineNo, 
		String sIpSubLineNo, String sExtnSetCode, Double dQtyToKeep, String sBundleParentOrderLineKey, YFSEnvironment env, Document inDoc)	

	{	
		
		//fetch ExtnIsWarranty tag and DependentOnLineKey for the orderline of the warranty item
		YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
		YFCElement yfcInDocRoot = yfcInDoc.getDocumentElement();
		
		YFCNodeList<YFCElement> yfcnlOrderLine = yfcDocGetOrderListOutput.getElementsByTagName(GCConstants.ORDER_LINE);
		LOGGER.verbose("Output of OrderLine for getOrderList API call:" +yfcnlOrderLine);
		
		//iterate over OrderLine Elements
		for (YFCElement yfcOrderLine : yfcnlOrderLine)
		 { 
			 String sKitCode = yfcOrderLine.getAttribute(GCConstants.KIT_CODE);
			 LOGGER.verbose("Kit Code:" +sKitCode);
			 String sOrderLineDepOLK = yfcOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY); 
			 LOGGER.verbose("Dependent On Line Key:" +sOrderLineDepOLK);
			 
			 //to check if any warranties exist for REGULAR, SET or KIT
			 if (!YFCUtils.isVoid(sOrderLineDepOLK))
			 {
				 YFCElement yfcEleExtn = yfcOrderLine.getChildElement(GCConstants.EXTN);
				 String sExtnIsWarrantyItem = yfcEleExtn.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
				 LOGGER.verbose("ExtnIsWarrantyItem: "+sExtnIsWarrantyItem);
				 if (YFCUtils.equals("Y", sExtnIsWarrantyItem))
				 { //extracting the OrderLine with which the Warranty is associated with
					 Element eParentOLWarrAssc = GCXMLUtil.getElementByXPath(docGetOrderListOutput,"//Order/OrderLines/OrderLine[@OrderLineKey='"
				       + sOrderLineDepOLK + "']");
					 String sOLKeParentOLWarrAssc = eParentOLWarrAssc.getAttribute(GCConstants.ORDER_LINE_KEY);
					 LOGGER.verbose("ParentOrderLineAssoicatedwiththeWarrantyLine: "+GCXMLUtil.getElementXMLString(eParentOLWarrAssc));
					 
					 //check if the parent order line exists
					 if (!YFCCommon.isVoid(eParentOLWarrAssc))
					 { 	
						 //condition for KIT
						 if (!YFCUtils.isVoid(sBundleParentOrderLineKey) && !YFCUtils.isVoid(sOLKeParentOLWarrAssc))
						 {	
							if (YFCUtils.equals(sOLKeParentOLWarrAssc, sBundleParentOrderLineKey)) 
							{
								 sIpPrimeLineNo = eParentOLWarrAssc.getAttribute(GCConstants.PRIME_LINE_NO);
								 sIpSubLineNo = eParentOLWarrAssc.getAttribute(GCConstants.SUB_LINE_NO);
							}
							
						 }
							 String sParentOLWarrAsscPLNo = eParentOLWarrAssc.getAttribute(GCConstants.PRIME_LINE_NO);
							 String sParentOLWarrAsscSLNo = eParentOLWarrAssc.getAttribute(GCConstants.SUB_LINE_NO);
						  
						 if(YFCUtils.equals(sParentOLWarrAsscPLNo, sIpPrimeLineNo) && YFCUtils.equals(sParentOLWarrAsscSLNo, sIpSubLineNo))
						 { //extracting the primelineno and sublineno of the warranty orderline
						 String eWarrantyPrimeLineNo = yfcOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
						 String eWarrantySubLineNo = yfcOrderLine.getAttribute(GCConstants.SUB_LINE_NO);
						 
						 //setting the attribute of primelineno and sublineno for the input of change order
						 	YFCElement yfcEleinDocAppendWarrLines = yfcInDocRoot.getChildElement(GCConstants.ORDER_LINES);
							YFCElement yfcEleinDocAppendWarrLine = yfcEleinDocAppendWarrLines.createChild(GCConstants.ORDER_LINE);
							yfcEleinDocAppendWarrLine.setAttribute(GCConstants.PRIME_LINE_NO, eWarrantyPrimeLineNo);
							yfcEleinDocAppendWarrLine.setAttribute(GCConstants.SUB_LINE_NO, eWarrantySubLineNo);
					 
						 if(YFCCommon.isVoid(sBundleParentOrderLineKey) || YFCUtils.equals(GCConstants.ONE, sExtnSetCode))
							{	
								//condition when BundleParentOrderLineKey is not present - for REGULAR item (OR) SET item
							if (dQtyToKeep<0)
							{	yfcEleinDocAppendWarrLine.setAttribute(GCConstants.ORDERED_QUANITY,GCConstants.ZERO);
							}
							else{
								yfcEleinDocAppendWarrLine.setAttribute(GCConstants.ORDERED_QUANITY,dQtyToKeep.toString());
							}
							}
						 else
						 { 	//Setting Qty for KIT Parent Warranty
							 if(YFCUtils.equals(sBundleParentOrderLineKey, sOrderLineDepOLK))
							 { yfcEleinDocAppendWarrLine.setAttribute(GCConstants.ORDERED_QUANITY,GCConstants.ZERO);}
							 //else the ordered qty of the warranty line remains the same if that is the warranty line not associated with the correct parent line which 
							 //is to be cancelled
						 }	
					 }
		 }
		}
			 }
						LOGGER.verbose("The yfcDocument after modifyWarrantyLines: " +yfcDocGetOrderListOutput);
						LOGGER.verbose("The Input Document modified after appending of warrantyLines: " +yfcInDoc);
						
}
						return yfcInDoc;
}
}


