package com.gc.api;

import java.util.ArrayList;
import java.util.HashMap;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCProcessBackorderManual {
	
	// initialize logger
		private static final YFCLogCategory LOGGER = YFCLogCategory
				.instance(GCProcessBackordersAPI.class);
	
	public void scheduleBackorderedOrder(YFSEnvironment env, Document inDoc) {
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(" scheduleBackorderedOrder() method, Input document is"
					+ GCXMLUtil.getXMLString(inDoc));
		}
		YFCDocument inDocument = YFCDocument.getDocumentFor(inDoc);
		YFCElement eleRoot = inDocument.getDocumentElement();
		String sOrderHeaderKey = eleRoot.getAttribute(GCConstants.ORDER_HEADER_KEY);
		String sEnterpriseCode = eleRoot.getAttribute(GCConstants.ENTERPRISE_CODE);
		YFCNodeList<YFCElement> nlOrderLineInput = eleRoot.getElementsByTagName(GCConstants.ORDER_LINE);
		HashMap<String, String> hMap = new HashMap<String, String>();
		ArrayList<String> orderLineListInput =  new ArrayList<String>();
		//To get all the orderlines which has shipnode passed
		for(int count =0;count<nlOrderLineInput.getLength();count++){
			YFCElement eleInput = nlOrderLineInput.item(count);
			String shipNode = eleInput.getAttribute(GCConstants.SHIP_NODE);
			String orderLineKey = eleInput.getAttribute(GCConstants.ORDER_LINE_KEY);
			if(!YFCCommon.isVoid(orderLineKey)){
			orderLineListInput.add(orderLineKey);
			}
			if(!YFCCommon.isVoid(shipNode)){
				hMap.put(orderLineKey, shipNode);
			}
		}
		//Preparing Input for getOrderList
		YFCDocument inDocForGetOrderList = YFCDocument.createDocument(GCConstants.ORDER);
		YFCElement eleOrderLElement = inDocForGetOrderList.getDocumentElement();
		if(!YFCCommon.isVoid(sOrderHeaderKey)){
		eleOrderLElement.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
		}
		if(!YFCCommon.isVoid(sEnterpriseCode)){
		eleOrderLElement.setAttribute(GCConstants.ENTERPRISE_CODE, sEnterpriseCode);
		}
		//Calling GetOrderList API
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("getOrderList, Input document is"
					+ GCXMLUtil.getXMLString(inDocForGetOrderList.getDocument()));
		}
		Document getOrderListOutDoc =
		          GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST,
		        		  inDocForGetOrderList.getDocument(),null);
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("getOrderList, Output document is"
					+ GCXMLUtil.getXMLString(getOrderListOutDoc));
		}
		YFCDocument getOrderListOut = YFCDocument.getDocumentFor(getOrderListOutDoc);
		YFCElement eleOrderLineListOut = getOrderListOut.getDocumentElement();
		
		YFCNodeList<YFCElement> nlOrderLine = eleOrderLineListOut.getElementsByTagName(GCConstants.ORDER_LINE);
		
		//Preparing Input for changeOrder
		YFCDocument inDocChangeOrder = YFCDocument.createDocument(GCConstants.ORDER);
		YFCElement cEleChangeOrderRoot = inDocChangeOrder.getDocumentElement();
		cEleChangeOrderRoot.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
		YFCElement cEleOrderLines = cEleChangeOrderRoot.createChild(GCConstants.ORDER_LINES);
		
		for(int count=0;count<nlOrderLine.getLength();count++){
			YFCElement eleOrderLine = nlOrderLine.item(count);
			String sMaxLineStatus = eleOrderLine.getAttribute(GCConstants.MAX_LINE_STATUS);
			String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
			String sShipNode = hMap.get(sOrderLineKey);
			if(("1300".equalsIgnoreCase(sMaxLineStatus) || "1500.10".equalsIgnoreCase(sMaxLineStatus)) && orderLineListInput.contains(sOrderLineKey)){
				if(!YFCCommon.isVoid(sShipNode)){
					eleOrderLine.setAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE, GCConstants.YES);
					eleOrderLine.setAttribute(GCConstants.SHIP_NODE, sShipNode);
					cEleOrderLines.importNode(eleOrderLine);
				}else{
					YFCElement nOrderLineSourcingCtrls = eleOrderLine.getElementsByTagName(GCConstants.ORDER_LINE_SOURCING_CNTRLS).item(0);
					YFCElement newEleSourcingControl = null;
					if(YFCCommon.isVoid(nOrderLineSourcingCtrls)){
						YFCElement newEleSourcingControls = eleOrderLine.createChild(GCConstants.ORDER_LINE_SOURCING_CNTRLS);
						newEleSourcingControl = newEleSourcingControls.createChild(GCConstants.ORDER_LINE_SOURCING_CNTRL);
					} else {
						newEleSourcingControl = nOrderLineSourcingCtrls.createChild(GCConstants.ORDER_LINE_SOURCING_CNTRL);
					}
					newEleSourcingControl.setAttribute(GCConstants.NODE, "MFI");
					newEleSourcingControl.setAttribute(GCConstants.SUPPRESS_SOURCING, GCConstants.YES);
					eleOrderLine.setAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE, GCConstants.NO);
					eleOrderLine.setAttribute(GCConstants.SHIP_NODE, " ");
					cEleOrderLines.importNode(eleOrderLine);
				}
			}
		}
		if(cEleOrderLines.hasChildNodes()){
		//Calling change order API
		if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("changeOrder, Input document is"
						+ GCXMLUtil.getXMLString(inDocChangeOrder.getDocument()));
		}
		/*Document outDoc = */GCCommonUtil.invokeAPI(env, inDocChangeOrder.getDocument(),
				GCConstants.API_CHANGE_ORDER, null);
		//YFCDocument outDocChangeOrder = YFCDocument.getDocumentFor(outDoc);
		//Preparing Input for changeOrderStatus
		YFCDocument docChangeOrderStatus = YFCDocument.createDocument(GCConstants.ORDER_STATUS_CHANGE);
	    YFCElement eleChangeOrderStatus = docChangeOrderStatus.getDocumentElement();
	    eleChangeOrderStatus.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
	    //eleChangeOrderStatus.setAttribute(GCConstants.CHANGE_FOR_ALL_AVAIL_QTY, GCConstants.YES);
	    eleChangeOrderStatus.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.CHANGE_CONFIRM_STATUS);
	    //eleChangeOrderStatus.setAttribute(GCConstants.BASE_DROP_STATUS, GCConstants.BACKORDERED_OMS_STATUS);
	    YFCElement eleOrderLinesChngStatus = eleChangeOrderStatus.createChild(GCConstants.ORDER_LINES);
	    //Preparing Input for unSchedule Order
	    YFCDocument unscheduleOrderDoc = YFCDocument.createDocument("UnScheduleOrder");
	    YFCElement eleUnScheduleOrder = unscheduleOrderDoc.getDocumentElement();
	    eleUnScheduleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
	    YFCElement eleUnscheduleOrderLines = eleUnScheduleOrder.createChild(GCConstants.ORDER_LINES);
	    
		//YFCElement eleChangeOrder = outDocChangeOrder.getDocumentElement();
		//YFCNodeList<YFCElement> nChangeOrderLine = eleChangeOrder.getElementsByTagName(GCConstants.ORDER_LINE);
		for(int count=0;count<nlOrderLine.getLength();count++){
			YFCElement eleOrderLine = nlOrderLine.item(count);
			String maxLineStatus = eleOrderLine.getAttribute(GCConstants.MAX_LINE_STATUS);
			String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
			if(orderLineListInput.contains(sOrderLineKey)){
			if("1300".equalsIgnoreCase(maxLineStatus)){
				  String strOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
				  YFCElement eleOrderLineChngStatus = eleOrderLinesChngStatus.createChild(GCConstants.ORDER_LINE);
		          eleOrderLineChngStatus.setAttribute(GCConstants.ORDER_LINE_KEY, strOrderLineKey);
		          eleOrderLineChngStatus.setAttribute(GCConstants.BASE_DROP_STATUS, GCConstants.BACKORDERED_OMS_STATUS);
		          eleOrderLineChngStatus.setAttribute(GCConstants.CHANGE_FOR_ALL_AVAIL_QTY, GCConstants.YES);
			}else if("1500.10".equalsIgnoreCase(maxLineStatus)){
				String strOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
				  YFCElement eleOrderLineChngStatus = eleUnscheduleOrderLines.createChild(GCConstants.ORDER_LINE);
		          eleOrderLineChngStatus.setAttribute(GCConstants.ORDER_LINE_KEY, strOrderLineKey);
			}
			}
		}
		if(eleOrderLinesChngStatus.hasChildNodes()){
			GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER_STATUS, docChangeOrderStatus, null);
		}else if(eleUnscheduleOrderLines.hasChildNodes()){
			GCCommonUtil.invokeAPI(env, GCConstants.UNSCHEDULE_ORDER, unscheduleOrderDoc, null);
		}
		//Preparing input for Schedule order
		YFCDocument schedOrderInDoc =
	              YFCDocument.getDocumentFor("<ScheduleOrder OrderHeaderKey='"
	                  + sOrderHeaderKey + "'></ScheduleOrder>");
		//Calling schedule order API
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("scheduleOrder, Input document is"
					+ GCXMLUtil.getXMLString(schedOrderInDoc.getDocument()));
	    }
		
	    GCCommonUtil.invokeAPI(env, GCConstants.API_SCHEDULE_ORDER, schedOrderInDoc, null);
	    //Calling get order list API
	    if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("getOrderList Final, Input document is"
					+ GCXMLUtil.getXMLString(inDocForGetOrderList.getDocument()));
	    }
	    YFCDocument getOrderListTemp = YFCDocument
	              .getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/getOrderList.xml"));
	    YFCDocument getOrderListOutDocLast = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST,
	    		inDocForGetOrderList, getOrderListTemp);
	    
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("getOrderList Final, Output document is"
					+ getOrderListOutDocLast);
	    }
		YFCElement eleOrderList = getOrderListOutDocLast.getDocumentElement();
		YFCElement eleOrder = eleOrderList.getChildElement(GCConstants.ORDER);
		YFCNodeList<YFCElement> nOrderLine = eleOrder.getElementsByTagName(GCConstants.ORDER_LINE);
		//Change order call to remove ORDER_LINE_SOURCING_CNTRL
		YFCDocument inDocChangeOrderF = YFCDocument.createDocument(GCConstants.ORDER);
		YFCElement cEleChangeOrderRootF = inDocChangeOrderF.getDocumentElement();
		cEleChangeOrderRootF.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
		YFCElement cEleOrderLinesF = cEleChangeOrderRootF.createChild(GCConstants.ORDER_LINES);
		
		for(int count=0;count<nOrderLine.getLength();count++){
				YFCElement eleOrderLine = nOrderLine.item(count);
				YFCNodeList<YFCElement> nOrderLineSourcingCtrl = eleOrderLine.getElementsByTagName(GCConstants.ORDER_LINE_SOURCING_CNTRL);
				for(int num=0;num<nOrderLineSourcingCtrl.getLength();num++){
					YFCElement eleOrderLineSourcingCtrl = nOrderLineSourcingCtrl.item(num);
						if(!YFCCommon.isVoid(eleOrderLineSourcingCtrl)){
							eleOrderLineSourcingCtrl.setAttribute(GCConstants.ACTION, GCConstants.REMOVE);
							cEleOrderLinesF.importNode(eleOrderLine);
						}
					}
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("changeOrder, Input document for removing ORDER_LINE_SOURCING_CNTRL"
					+ GCXMLUtil.getXMLString(inDocChangeOrderF.getDocument()));
		     }
		if(!YFCElement.isVoid(cEleOrderLinesF)){
			GCCommonUtil.invokeAPI(env, inDocChangeOrderF.getDocument(),
		    GCConstants.API_CHANGE_ORDER, null);
		}
		//Preparing Input to POST to DAX
		YFCDocument DocForDAXInput = YFCDocument.createDocument(GCConstants.ORDER);
		YFCElement eleForDAXInput = DocForDAXInput.getDocumentElement();
		eleForDAXInput.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
		YFCElement eleOrderLinesForDAXInput = eleForDAXInput.createChild(GCConstants.ORDER_LINES);
		ArrayList<String> orderLineKeyList = new ArrayList<String>();
		for(int count=0;count<nOrderLine.getLength();count++){
			  YFCElement eleOrderLine = nOrderLine.item(count);
			  String sMaxLineStatus = eleOrderLine.getAttribute(GCConstants.MAX_LINE_STATUS);
			  String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
			  if("1500".equalsIgnoreCase(sMaxLineStatus) && orderLineListInput.contains(sOrderLineKey)){
				  eleOrderLine.setAttribute(GCConstants.ORDERED_QTY, GCConstants.ZERO);
				  orderLineKeyList.add(eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
				  eleOrderLinesForDAXInput.importNode(eleOrderLine);
			  }
		}
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Input for GCDAXOrderSyncPublishService Service:"
					+ GCXMLUtil.getXMLString(DocForDAXInput.getDocument()));
	    }
		if(eleOrderLinesForDAXInput.hasChildNodes()){
			 env.setTxnObject("SendCancellationToDAXObj", orderLineKeyList);
			 GCCommonUtil.invokeService(env, "GCDAXOrderSyncPublishService", DocForDAXInput);
		  }
	  }
   }

}
