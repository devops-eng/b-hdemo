/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 * *###########################################
 * ######################################################
 * ################################################################ OBJECTIVE: Contains the logic
 * for looking at backordered lines and apply DAX allocation hold.
 * ##################################
 * ################################################################
 * ############################################################### Version Date Modified By
 * Description
 * ######################################################################################
 * ########################################################################### 1.0 13/05/2014 Singh,
 * Gurpreet Order Schedule and Release 1.1 29/05/2014 Soni, Karan Made changes as per new design 1.2
 * 09/03/2015 Sinha, Rajiv GCSTORE-319/ 329 changes for sourcing. 1.3 22/04/2015 Ravi, Divya
 * Refactored entire code to handle split on many scenarios
 * #########################################
 * #########################################################
 * ###############################################################
 */
package com.gc.api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.api.order.GCProcessATGOrders;
import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCOrderUtil;
import com.gc.common.utils.GCXMLUtil;
import com.gc.exstore.utils.GCShipmentUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:gurpreet.singh@expicient.com">Gurpreet</a>
 */
public class GCProcessBackordersAPI implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCProcessBackordersAPI.class);
  Properties prop = new Properties();


  /**
   *
   * Description of processBackOrders Contains the logic for looking at backordered lines and apply
   * DAX allocation hold. And move the OrderLine to ScheduledPO status for future inventory
   *
   * @param env
   * @param inDoc
   * @return
   * @throws GCException
   *
   */
  public Document processBackOrders(YFSEnvironment env, Document inDoc) throws GCException {
    LOGGER.debug(" Entering GCProcessBackordersAPI.processBackOrders() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" processBackOrders() method, Input document is" + GCXMLUtil.getXMLString(inDoc));
    }
    try {
      Element eleRootInDoc = inDoc.getDocumentElement();
      String sOrderHeaderKey = eleRootInDoc.getAttribute(GCConstants.ORDER_HEADER_KEY);

      Map<String, String> createBOLines = new HashMap<String, String>();
      List<YFCElement> changeBOStatusLines = new ArrayList<YFCElement>();
      boolean rescheduleOrder = splitOrderOnMultiAssignments(env, inDoc, createBOLines, changeBOStatusLines);

      if (!changeBOStatusLines.isEmpty()) {
        LOGGER.debug("changeBOStatusLines is not empty!!");
        changeBOLinesStatus(env, inDoc, changeBOStatusLines);
        rescheduleOrder = true;
      }
      // complete order scheduled against future will reach this block
      if (!rescheduleOrder) {
        LOGGER.debug("Update for scheduledPODax");

        processOrdersOnScheduled(env, inDoc, sOrderHeaderKey);
      }

      boolean bSchedBOLines = false;
      if (!createBOLines.isEmpty()) {
        LOGGER.debug("BO qty getting created :: " + createBOLines.toString());
        // For a line with multi schedules and BO status, remove the BO
        // status qty and split it separately
        changeBOLinesToCreatedLines(env, createBOLines, inDoc);
        bSchedBOLines = true;
      }

      if (bSchedBOLines || rescheduleOrder) {
        LOGGER.debug("Reschedule BO lines");
        // Reschedule the order for moving created BO lines to BO status
        rescheduleBOLines(env, inDoc);
      }
      // Stamp shipNode for release Consolidation
      if (!bSchedBOLines && !rescheduleOrder) {
        LOGGER.debug("Stamp ship Node at Line level!!");
        stampShipNodeOnOrderLine(env, inDoc);
      }

    } catch (Exception e) {
      LOGGER.debug(" Inside catch of GCProcessBackordersAPI.processBackOrders()", e.getMessage());
      YFCException yfce = new YFCException(GCErrorConstants.ERROR_ON_SCHEDULING);
      throw yfce;
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" processBackOrders() method, Input document is" + GCXMLUtil.getXMLString(inDoc));
    }
    LOGGER.debug(" Exiting GCProcessBackordersAPI.processBackOrders() method ");
    return inDoc;
  }

  /**
   * BO lines status changed to one of the pick up status of schedule transaction
   *
   * @param env
   * @param inDoc
   * @param changeBOStatusLines
   */
  private void changeBOLinesStatus(YFSEnvironment env, Document inDoc, List<YFCElement> changeBOStatusLines) {

    LOGGER.beginTimer("GCProcessBackordersAPI.changeBOLinesStatus()");
    String orderHdrKey = inDoc.getDocumentElement().getAttribute(GCConstants.ORDER_HEADER_KEY);
    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);

    YFCDocument newInputDoc = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement newOrderEle = newInputDoc.getDocumentElement();
    newOrderEle.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
    newOrderEle.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHdrKey);

    YFCElement newOrderLinesEle = newOrderEle.createChild(GCConstants.ORDER_LINES);

    Document docChangeOrderStatus = GCXMLUtil.createDocument(GCConstants.ORDER_STATUS_CHANGE);
    Element eleChangeOrderStatus = docChangeOrderStatus.getDocumentElement();
    eleChangeOrderStatus.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHdrKey);
    eleChangeOrderStatus.setAttribute(GCConstants.IGNORE_TRANSACTION_DEPENDENCIES, GCConstants.YES);
    eleChangeOrderStatus.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.CHANGE_SCHEDULE_STATUS);
    Element eleOrderLinesChngStatus = GCXMLUtil.createElement(docChangeOrderStatus, GCConstants.ORDER_LINES, null);
    eleChangeOrderStatus.appendChild(eleOrderLinesChngStatus);
    for (YFCElement orderLineElem : changeBOStatusLines) {
      String isChangeOrderCallReq = orderLineElem.getAttribute("IsChangeOrderCallReq");
      if (YFCCommon.equals(isChangeOrderCallReq, "Y")) {
        orderLineElem.removeAttribute("IsChangeOrderCallReq");
        orderLineElem.removeAttribute("CompositionRatio");
        orderLineElem.setAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE, "N");
        orderLineElem.setAttribute(GCConstants.SHIP_NODE, "");
        removeAttNElementsForChangeOrderCall(orderLineElem);
        newOrderLinesEle.importNode(orderLineElem);
      }
      String orderLineKey = orderLineElem.getAttribute("OrderLineKey");
      String kitCode = orderLineElem.getAttribute("KitCode");
      if (GCConstants.BUNDLE.equals(kitCode)) {
        YFCElement orderEle = yfcInDoc.getDocumentElement();
        YFCElement orderLinesEle = orderEle.getChildElement(GCConstants.ORDER_LINES);
        YFCNodeList<YFCElement> orderLineNList = orderLinesEle.getElementsByTagName(GCConstants.ORDER_LINE);
        for (YFCElement orderLineEle : orderLineNList) {
          String parentOrderLineKey = "";
          YFCElement bundleParentLineEle = orderLineEle.getChildElement("BundleParentLine");
          if (!YFCCommon.isVoid(bundleParentLineEle)) {
            parentOrderLineKey = bundleParentLineEle.getAttribute("OrderLineKey");
            if (parentOrderLineKey.equals(orderLineKey)) {
              if (YFCCommon.equals(isChangeOrderCallReq, "Y")) {
                orderLineEle.removeAttribute("IsChangeOrderCallReq");
                orderLineEle.removeAttribute("CompositionRatio");
                orderLineEle.setAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE, "N");
                orderLineEle.setAttribute(GCConstants.SHIP_NODE, "");
                removeAttNElementsForChangeOrderCall(orderLineEle);
                newOrderLinesEle.importNode(orderLineEle);
              }
              Element eleOrderLineChngStatus =
                  GCXMLUtil.createElement(docChangeOrderStatus, GCConstants.ORDER_LINE, null);
              eleOrderLineChngStatus.setAttribute(GCConstants.ORDER_LINE_KEY,
                  orderLineEle.getAttribute(GCConstants.ORDER_LINE_KEY));
              eleOrderLineChngStatus.setAttribute(GCConstants.BASE_DROP_STATUS,
                  GCConstants.BACKORDERED_DAX_ALLOCATED_STATUS);
              eleOrderLineChngStatus.setAttribute(GCConstants.QUANTITY,
                  orderLineEle.getAttribute(GCConstants.ORDERED_QTY));
              eleOrderLinesChngStatus.appendChild(eleOrderLineChngStatus);

            }
          }
        }
      } else {
        Element eleOrderLineChngStatus = GCXMLUtil.createElement(docChangeOrderStatus, GCConstants.ORDER_LINE, null);
        eleOrderLineChngStatus.setAttribute(GCConstants.ORDER_LINE_KEY,
            orderLineElem.getAttribute(GCConstants.ORDER_LINE_KEY));
        eleOrderLineChngStatus.setAttribute(GCConstants.BASE_DROP_STATUS, GCConstants.BACKORDERED_DAX_ALLOCATED_STATUS);
        eleOrderLineChngStatus.setAttribute(GCConstants.QUANTITY, orderLineElem.getAttribute(GCConstants.ORDERED_QTY));
        eleOrderLinesChngStatus.appendChild(eleOrderLineChngStatus);
      }
    }
    LOGGER.debug("GCProcessBackordersAPI.changeBOLinesStatus() :: " + GCXMLUtil.getXMLString(docChangeOrderStatus));
    env.setTxnObject("DoNotSendCancellationEMail", "Y");
    try {
      GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER_STATUS, docChangeOrderStatus);
    } catch (Exception e) {
      LOGGER.debug(" Inside catch of GCProcessBackordersAPI.changeBOLinesStatus()", e.getMessage());
      YFCException yfce = new YFCException(GCErrorConstants.ERROR_ON_SCHEDULING);
      throw yfce;
    }

    YFCNodeList<YFCElement> newOrderLineNList = newOrderLinesEle.getElementsByTagName(GCConstants.ORDER_LINE);
    if (newOrderLineNList.getLength() > 0) {

      // Setting Override = "Y"
      newInputDoc.getDocumentElement().setAttribute(GCConstants.OVERRIDE, GCConstants.FLAG_Y);

      // invoking change order to remove predefined nodes
      LOGGER.debug("GCProcessBackordersAPI.changeBOLinesStatus() :: " + newInputDoc.toString());
      env.setTxnObject("DoNotSendCancellationEMail", "Y");
      env.setTxnObject("DoNotPublishToDAX", "Y");
      try {
        GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, newInputDoc.getDocument());
      } catch (Exception e) {
        LOGGER.debug(" Inside catch of GCProcessBackordersAPI.changeBOLinesStatus()", e.getMessage());
        YFCException yfce = new YFCException(GCErrorConstants.ERROR_ON_SCHEDULING);
        throw yfce;
      }
    }
    LOGGER.endTimer("GCProcessBackordersAPI.changeBOLinesStatus()");
  }

  /**
   * Calling schedule again to reschedule the BO lines
   *
   * @param env
   * @param inDoc
   */

  private void rescheduleBOLines(YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("GCProcessBackordersAPI.rescheduleBOLines()");
    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);

    YFCElement orderEle = yfcInDoc.getDocumentElement();
    String orderHeaderKey = orderEle.getAttribute(GCConstants.ORDER_HEADER_KEY);

    String allocationRuleID =
        !YFCCommon.isVoid(orderEle.getAttribute("AllocationRuleID")) ? orderEle.getAttribute("AllocationRuleID") : "";

        YFCDocument orderListTempDoc = YFCDocument.getDocumentFor("<OrderList><Order PaymentStatus=''/></OrderList>");
        YFCDocument orderDoc = YFCDocument.createDocument("Order");
        orderDoc.getDocumentElement().setAttribute("OrderHeaderKey", orderHeaderKey);
        YFCDocument getOrderList = GCCommonUtil.invokeGetOrderListAPI(env, orderHeaderKey, orderListTempDoc);
        YFCElement orderElement = getOrderList.getDocumentElement().getChildElement("Order");
        String paymentStatusBeforeSchedule = orderElement.getAttribute("PaymentStatus");
        // getPaymentStatusToScheduleOrder(env,
        // orderHeaderKey);
        if (YFCCommon.equals(paymentStatusBeforeSchedule, "AWAIT_AUTH")
            || YFCCommon.equals(paymentStatusBeforeSchedule, "AWAIT_PAY_INFO")) {
          GCCommonUtil.invokeService(env, "GCProcessPaymentForScheduleOnSuccess", orderDoc);
          getOrderList = GCCommonUtil.invokeGetOrderListAPI(env, orderHeaderKey, orderListTempDoc);
          orderElement = getOrderList.getDocumentElement().getChildElement("Order");
          paymentStatusBeforeSchedule = orderElement.getAttribute("PaymentStatus");
        }
        if (!YFCCommon.equals(paymentStatusBeforeSchedule, "AWAIT_AUTH")
            && !YFCCommon.equals(paymentStatusBeforeSchedule, "AWAIT_PAY_INFO")) {
          // Unschedule Eligible Lines
          unscheduleEligibleLines(env, orderHeaderKey);

          LOGGER.verbose("Unscheduled the order");

          YFCDocument schedOrderDoc =
              YFCDocument.getDocumentFor("<ScheduleOrder AllocationRuleID='" + allocationRuleID + "' OrderHeaderKey='"
                  + orderHeaderKey + "'></ScheduleOrder>");

          LOGGER.verbose("Going to call scheduleorder API::");
          env.setTxnObject("DoNotIgnoreDAXDemand", "Y");
          LOGGER.verbose("DoNotIgnoreDAXDemand=Y");
          env.setTxnObject("IsManualSchedule", "Y");
          LOGGER.verbose("IsManualSchedule=Y");

          try {
            GCCommonUtil.invokeAPI(env, GCConstants.API_SCHEDULE_ORDER, schedOrderDoc, null);
            env.setTxnObject("DoNotIgnoreDAXDemand", "N");
            LOGGER.verbose("DoNotIgnoreDAXDemand=N");
            env.setTxnObject("IsManualSchedule", "N");

            LOGGER.verbose("IsManualSchedule=N");
          } catch (Exception e) {
            LOGGER.debug(" Inside catch of GCProcessBackordersAPI.rescheduleBOLines()", e.getMessage());
            YFCException yfce = new YFCException(GCErrorConstants.ERROR_ON_SCHEDULING);
            throw yfce;
          }
          LOGGER.endTimer("GCProcessBackordersAPI.rescheduleBOLines()");
        }

        LOGGER.endTimer("GCProcessBackordersAPI.rescheduleBOLines()");
  }

  /**
   * GCSTORE-1832: Method added to move BO status qty to a separate line in CO status
   *
   * @param env
   * @param createBOLines
   * @param inDoc
   */

  private void changeBOLinesToCreatedLines(YFSEnvironment env, Map<String, String> createBOLines, Document inDoc) {
    LOGGER.beginTimer("GCProcessBackordersAPI.changeBOLinesToCreatedLines()");
    try {
      YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
      YFCDocument newInputDoc = YFCDocument.createDocument(GCConstants.ORDER);

      YFCElement orderEle = yfcInDoc.getDocumentElement();
      String orderHeaderKey = orderEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
      String orgCode = orderEle.getAttribute(GCConstants.ENTERPRISE_CODE);

      YFCElement newOrderEle = newInputDoc.getDocumentElement();
      newOrderEle.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
      newOrderEle.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
      newOrderEle.setAttribute(GCConstants.ENTERPRISE_CODE, orgCode);

      YFCElement orderLinesEle = orderEle.getChildElement(GCConstants.ORDER_LINES);
      YFCElement newOrderLinesEle = newOrderEle.createChild(GCConstants.ORDER_LINES);

      YFCNodeList<YFCElement> orderLineNList = orderLinesEle.getElementsByTagName(GCConstants.ORDER_LINE);
      for (YFCElement orderLineEle : orderLineNList) {
        String orderLineKey = orderLineEle.getAttribute(GCConstants.ORDER_LINE_KEY);
        if (!orderLineEle.hasAttribute("KitQty") && createBOLines.containsKey(orderLineKey)) {
          YFCElement newOrderLineEle = (YFCElement) orderLineEle.cloneNode(true);
          removeAttNElementsForChangeOrderCall(newOrderLineEle);
          newOrderLineEle.setAttribute(GCConstants.ORDERED_QTY, createBOLines.get(orderLineKey));
          newOrderLineEle.setAttribute(GCConstants.PRIME_LINE_NO, "");
          newOrderLineEle.setAttribute(GCConstants.SUB_LINE_NO, "");
          newOrderLineEle.setAttribute(GCConstants.ORDER_LINE_KEY, "");
          newOrderLineEle.setAttribute(GCConstants.ACTION, GCConstants.CREATE);
          newOrderLinesEle.importNode(newOrderLineEle);
        }
      }

      if (newOrderLinesEle.hasChildNodes()) {

        // Setting Override = "Y"
        newInputDoc.getDocumentElement().setAttribute(GCConstants.OVERRIDE, GCConstants.FLAG_Y);

        env.setTxnObject("Publish", "N");
        env.setTxnObject("DoNotSendCancellationEMail", "Y");
        env.setTxnObject("DoNotPublishToDAX", "Y");
        Document docChangeOrderOP = GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, newInputDoc.getDocument());
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("\n docChangeOrderOP output Doc" + YFCDocument.getDocumentFor(docChangeOrderOP));
        }
      }
    } catch (Exception e) {
      LOGGER.debug(" Inside catch of GCProcessBackordersAPI.changeBOLinesToCreatedLines()", e.getMessage());
      YFCException yfce = new YFCException(GCErrorConstants.ERROR_ON_SCHEDULING);
      throw yfce;
    }
    LOGGER.endTimer("GCProcessBackordersAPI.changeBOLinesToCreatedLines()");
  }

  /**
   * GCSTORE-1832 - Method which would split multi status line into multiple lines and move BO to CO
   * status
   *
   * @param env
   * @param inDoc
   * @param createBOLines
   * @return
   * @throws ParseException
   * @throws GCException
   */
  private boolean splitOrderOnMultiAssignments(YFSEnvironment env, Document inDoc, Map<String, String> createBOLines,
      List<YFCElement> changeBOStatusLines) throws GCException {

    LOGGER.beginTimer("GCProcessBackordersAPI.splitOrderOnMultiAssignments");
    boolean rescheduleOrder = false;
    try {
      YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
      YFCDocument newInputDoc = YFCDocument.createDocument(GCConstants.ORDER);

      YFCElement orderEle = yfcInDoc.getDocumentElement();
      String orderHeaderKey = orderEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
      String orgCode = orderEle.getAttribute(GCConstants.ENTERPRISE_CODE);
      String allocationRuleID =
          !YFCCommon.isVoid(orderEle.getAttribute("AllocationRuleID")) ? orderEle.getAttribute("AllocationRuleID") : "";
          String isScheduleEvent = orderEle.getAttribute("IsScheduleEvent");
          LOGGER.debug("splitOrderOnMultiAssignments :: isScheduleEvent ::" + isScheduleEvent);

          YFCElement newOrderEle = newInputDoc.getDocumentElement();
          newOrderEle.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
          newOrderEle.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
          newOrderEle.setAttribute(GCConstants.ENTERPRISE_CODE, orgCode);

          Map<String, String> oLKeyCompRatio = new HashMap<String, String>();
          YFCElement orderLinesEle = orderEle.getChildElement(GCConstants.ORDER_LINES);
          YFCElement newOrderLinesEle = newOrderEle.createChild(GCConstants.ORDER_LINES);

          boolean reschedulePISOrder = false;
          boolean isBackorderForStoreItem = false;
          Map<String, Boolean> isScheduleAgainstFutureDate = new HashMap<String, Boolean>();
          Map<String, String> bundleStoreClearanceFlag = new HashMap<String, String>();

          YFCNodeList<YFCElement> orderLineNList = orderLinesEle.getElementsByTagName(GCConstants.ORDER_LINE);
          for (YFCElement orderLineEle : orderLineNList) {
            String orderLineKey = orderLineEle.getAttribute(GCConstants.ORDER_LINE_KEY);
            String orderedQty = orderLineEle.getAttribute(GCConstants.ORDERED_QTY);

            String maxLineStatus = orderLineEle.getAttribute("MaxLineStatus");
            String fulfillmentType = orderLineEle.getAttribute(GCConstants.FULFILLMENT_TYPE);
            String isFirmPredefinedNode = orderLineEle.getAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE);
            String shipNode = orderLineEle.getAttribute(GCConstants.SHIP_NODE);

            //Defect-3021
            YFCElement eleExtn= orderLineEle.getChildElement("Extn");
            if(!YFCCommon.isVoid(eleExtn)){
              Double extnQtyCancelledInSplit=eleExtn.getDoubleAttribute("ExtnQtyCancelledInSplit");
              YFCElement newOrderLineExtn = newOrderEle.createChild("Extn");
              newOrderLineExtn.setAttribute("ExtnQtyCancelledInSplit", extnQtyCancelledInSplit);
            }
            //Defect-3021 Ends Here

            if (YFCCommon.equals(maxLineStatus, "1100", false)) {
              YFCElement newOrderLineExtnEle = orderLineEle.getChildElement("Extn");
              if (!YFCCommon.isVoid(newOrderLineExtnEle)) {
                bundleStoreClearanceFlag.put(orderLineKey, newOrderLineExtnEle.getAttribute("ExtnIsStoreClearance"));
              }
            }

            String parentOrderLineKey = "";
            YFCElement bundleParentLineEle = orderLineEle.getChildElement("BundleParentLine");
            if (!YFCCommon.isVoid(bundleParentLineEle)) {
              parentOrderLineKey = bundleParentLineEle.getAttribute("OrderLineKey");
              LOGGER.verbose("This is a component line with parentOrderLineKey=" + parentOrderLineKey);
              if (bundleStoreClearanceFlag.containsKey(parentOrderLineKey)) {
                YFCElement orderLineEleExtn = orderLineEle.getChildElement("Extn");
                orderLineEleExtn.setAttribute("ExtnIsStoreClearance", bundleStoreClearanceFlag.get(parentOrderLineKey));
              } else {
                YFCElement orderLineEleExtn = orderLineEle.getChildElement("Extn");
                bundleStoreClearanceFlag.put(parentOrderLineKey,
                    getParentStoreClearanceFlag(parentOrderLineKey, orderLineNList));
                orderLineEleExtn.setAttribute("ExtnIsStoreClearance", bundleStoreClearanceFlag.get(parentOrderLineKey));
              }
            }

            if ((YFCCommon.equals(maxLineStatus, "1300", false) && fulfillmentType.indexOf(GCConstants.PICKUP_IN_STORE) >= 0
                && !canIgnoreIsFirmPredefinedNode(env, orderLineEle)
                && YFCCommon.equals(isFirmPredefinedNode, GCConstants.YES, false) && !YFCCommon.isVoid(shipNode))
                || (!YFCCommon.equals(isScheduleEvent, "Y") && YFCCommon.equals(maxLineStatus, "1300", false))) {
              orderLineEle.setAttribute("IsChangeOrderCallReq", "Y");
              changeBOStatusLines.add(orderLineEle);
              // Needs STS alert for PIS backorder
              if (fulfillmentType.indexOf(GCConstants.PICKUP_IN_STORE) >= 0
                  && YFCCommon.equals(isFirmPredefinedNode, GCConstants.YES, false) && !YFCCommon.isVoid(shipNode)) {
                reschedulePISOrder = true;
              }
              continue;
            }

            String compositionRatio = "";
            YFCElement orderStatusesEle = orderLineEle.getChildElement("OrderStatuses");
            YFCNodeList<YFCElement> orderStatusesNList = orderStatusesEle.getElementsByTagName("OrderStatus");
            String statusQty = "";
            boolean isRatioForSched = false;

            for (YFCElement orderLineStatus : orderStatusesNList) {
              String status = orderLineStatus.getAttribute(GCConstants.STATUS);
              statusQty = orderLineStatus.getAttribute(GCConstants.STATUS_QTY);
              int iStatusQty = ((int) Double.parseDouble(statusQty));
              if (!isRatioForSched && !YFCCommon.equals(status, "1400") && !YFCCommon.equals(status, "1300")
                  && !YFCCommon.equals(status, "9000")) {
                compositionRatio =
                    getScheduledQtyRatio(orderLineKey, orderEle, orderLineEle, compositionRatio, isScheduleAgainstFutureDate);
                isRatioForSched = true;
              } else if (YFCCommon.equals(status, "1300")) {
                if (!createBOLines.containsKey(orderLineKey)) {
                  createBOLines.put(orderLineKey, statusQty);
                } else {
                  int boQty = iStatusQty + (int) Double.parseDouble(createBOLines.get(orderLineKey));
                  createBOLines.put(orderLineKey, Integer.toString(boQty));
                }
              }
            }

            if (YFCCommon.isVoid(compositionRatio) && !createBOLines.isEmpty()) {
              // completely BO line, hence remove from this
              int iQty = (int) Double.parseDouble(orderedQty);
              int boQty = (int) Double.parseDouble(createBOLines.get(orderLineKey));
              createBOLines.remove(orderLineKey);
              if (iQty == boQty) {
                boolean tempIsBackorderForStoreItem = isStoreItemNonBackorderable(orderLineEle);
                isBackorderForStoreItem = isBackorderForStoreItem || tempIsBackorderForStoreItem;
                continue;
              } else {
                int remainQty = iQty - boQty;
                compositionRatio = Integer.toString(boQty).concat(":").concat(Integer.toString(remainQty));
              }
            }

            if (!compositionRatio.contains(":")) {
              if (!createBOLines.isEmpty() && !YFCCommon.equals(maxLineStatus, "1100", false)) {
                // line contains partly scheduled and partly BO status
                compositionRatio = (int) Double.parseDouble(createBOLines.get(orderLineKey)) + ":" + compositionRatio;
                isScheduleAgainstFutureDate.put(orderLineKey, true);
                createBOLines.remove(orderLineKey);
              } else if (!isScheduleAgainstFutureDate.isEmpty() && isScheduleAgainstFutureDate.containsKey(orderLineKey)) {
                // line completely scheduled against Future inventory with
                // PreDefined Node, hence move ahead
              } else {
                continue;
              }
            }

            if (YFCCommon.isVoid(parentOrderLineKey)) {
              // This is regular item. Now go for split and reschedule of this
              // line only if it was PIS
              // backordered or partial backorded (neither full or zero
              // backorder)
              YFCElement newOrderLineEle = (YFCElement) orderLineEle.cloneNode(true);
              newOrderLineEle.setAttribute("CompositionRatio", compositionRatio);
              if (fulfillmentType.indexOf(GCConstants.PICKUP_IN_STORE) >= 0
                  && !canIgnoreIsFirmPredefinedNode(env, newOrderLineEle)
                  && YFCCommon.equals(isFirmPredefinedNode, GCConstants.YES, false) && !YFCCommon.isVoid(shipNode)
                  && isScheduleAgainstFutureDate.containsKey(orderLineKey)) {
                newOrderLineEle.setAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE, "N");
                newOrderLineEle.setAttribute(GCConstants.SHIP_NODE, "");
                reschedulePISOrder = true;
              } else if (changeBOStatusLines.contains(orderLineEle)) {
                changeBOStatusLines.remove(orderLineEle);
              }
              int indexOfDelimiter = compositionRatio.indexOf(':');
              if (indexOfDelimiter > 0 && indexOfDelimiter < compositionRatio.length() - 1) {
                newOrderLinesEle.importNode(newOrderLineEle);
              } else if (reschedulePISOrder) {
                newOrderLineEle.setAttribute("IsChangeOrderCallReq", "Y");
                changeBOStatusLines.add(newOrderLineEle);
              }
            } else if (!oLKeyCompRatio.containsKey(parentOrderLineKey)) {
              String kitQty = orderLineEle.getAttribute("KitQty");
              int kQty = (int) Double.parseDouble(kitQty);
              String[] compQtys = compositionRatio.split(":");

              String updatedCompositionRatio = "";
              for (int i = 0; i < compQtys.length; i++) {
                if (i == compQtys.length - 1) {
                  updatedCompositionRatio =
                      updatedCompositionRatio.concat(Integer.toString((Integer.parseInt(compQtys[i]) / kQty)));
                } else {
                  updatedCompositionRatio =
                      updatedCompositionRatio.concat(Integer.toString((Integer.parseInt(compQtys[i]) / kQty))).concat(":");
                }
              }
              oLKeyCompRatio.put(parentOrderLineKey, updatedCompositionRatio);
              if (createBOLines.containsKey(orderLineKey)) {
                int compBoQty = (int) Double.parseDouble(createBOLines.get(orderLineKey));
                createBOLines.put(parentOrderLineKey, Integer.toString(compBoQty / kQty));
                createBOLines.remove(orderLineKey);
              }
              // remove component lines from change BO line status list
              if (changeBOStatusLines.contains(orderLineEle)) {
                changeBOStatusLines.remove(orderLineEle);
              }
              LOGGER.verbose("do not consider the component line after storing the ParentLineKey=" + parentOrderLineKey
                  + " and Composition Ratio=" + compositionRatio);
            } else {
              LOGGER
              .verbose("do not consider the component line because parent is already stored with storing the ParentLineKey and Composition Ratio");
            }
            if (LOGGER.isVerboseEnabled()) {
              LOGGER.verbose("Calling ChangeOrder to split backordered lines with document =\n" + newInputDoc.toString());
            }
          }
          reschedulePISOrder =
              reschedulePISOrder
              || filterOrderLineForBundleLines(env, newOrderLinesEle, orderLinesEle, oLKeyCompRatio,
                  isScheduleAgainstFutureDate, changeBOStatusLines);
          // call service to split the order with newInputDoc as input to the
          // service. it has only
          // OrderHeaderKey, Override="Y", EnterpriseCode and only respective
          // lines

          YFCNodeList<YFCElement> newOrderLineNList = newOrderLinesEle.getElementsByTagName(GCConstants.ORDER_LINE);
          if (newOrderLineNList.getLength() > 0) {
            for (YFCElement orderLine : newOrderLineNList) {
              removeAttNElementsForChangeOrderCall(orderLine);
            }

            // Split Dependent Line
            newInputDoc = addDependentLineForSplit(newInputDoc, inDoc);


            // Improvement Defect-2485 Start
            String sLatestLOS = "";
            String sLevelOfService = orderEle.getAttribute(GCConstants.LEVEL_OF_SERVICE);
            if (reschedulePISOrder && YFCCommon.equals(sLevelOfService, "STORE_PICKUP")) {
              sLevelOfService = prop.getProperty("DEFAULT_S2S_LOS");
              // Standard_Ground
              if (!YFCCommon.isVoid(sLevelOfService)) {
                sLatestLOS = sLevelOfService;
              }
            }
            // Defect-2485 Ends

            // call order split service
            rescheduleOrder = splitAndCallChangeOrder(env, newInputDoc, orderHeaderKey, allocationRuleID, sLatestLOS);

          } else {
            // Improvement Defect-2485 Start
            String sLatestLOS = "";
            String sLevelOfService = orderEle.getAttribute(GCConstants.LEVEL_OF_SERVICE);

            // This will get executed when it is completely backordered or complete ship to store scenario
            if (reschedulePISOrder && YFCCommon.equals(sLevelOfService, "STORE_PICKUP")) {

              sLevelOfService = prop.getProperty("DEFAULT_S2S_LOS");
              // Standard_Ground

              if (!YFCCommon.isVoid(sLevelOfService)) {
                sLatestLOS = sLevelOfService;

              }
              callChangeOrderToUpdateLOS(env, orderEle, sLatestLOS);
            }
            // Defect-2485 Ends
          }
          // Do not send to DAX if there is reschedule due to PIS or split due to
          // partial backorder
          // If there is any reschedule due to PIS backorder, raise alert.
          if (reschedulePISOrder) {
            LOGGER
            .verbose("Going to raise alert. Because Pick up in store has been backordered, moving the ordered to Ship to store");
            GCCommonUtil.invokeService(env, "GCRaiseAlertForShipToStoreService", inDoc);
          }

    } catch (Exception e) {
      LOGGER.debug(" Inside catch of GCProcessBackordersAPI.splitOrderOnMultiAssignments()", e.getMessage());
      YFCException yfce = new YFCException(GCErrorConstants.ERROR_ON_SCHEDULING);
      throw yfce;
    }
    LOGGER.endTimer("GCProcessBackordersAPI.splitOrderOnMultiAssignments");
    return rescheduleOrder;
  }

  /**
   * This method calls changeOrder API to update level of service with the latest level of service.
   *
   * @param env
   * @param orderEle
   * @param sLatestLOS
   */
  public void callChangeOrderToUpdateLOS(YFSEnvironment env, YFCElement orderEle, String sLatestLOS) {
    try {
      String orderHeaderKey = orderEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
      YFCDocument changeOrdrInDoc = YFCDocument.createDocument(GCConstants.ORDER);
      YFCElement root = changeOrdrInDoc.getDocumentElement();
      root.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
      root.setAttribute(GCConstants.LEVEL_OF_SERVICE, sLatestLOS);
      root.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("Input Doc to change Order API to update level of service::" + changeOrdrInDoc.toString());
      }
      GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, changeOrdrInDoc, null);
    } catch (Exception e) {
      LOGGER.debug(" Inside catch of GCProcessBackordersAPI.callChangeOrderToUpdateLOS()", e.getMessage());
      YFCException yfce = new YFCException(GCErrorConstants.ERROR_ON_SCHEDULING);
      throw yfce;
    }
  }

  /**
   * Method to get the value of store clearance flag from bundle parent
   *
   * @param parentOrderLineKey
   * @param orderLineNList
   */
  private String getParentStoreClearanceFlag(String parentOrderLineKey, YFCNodeList<YFCElement> orderLineNList) {
    try {
      for (YFCElement orderLineEle : orderLineNList) {
        String orderLineKey = orderLineEle.getAttribute(GCConstants.ORDER_LINE_KEY);
        if (YFCCommon.equals(orderLineKey, parentOrderLineKey)) {
          YFCElement extnElem = orderLineEle.getChildElement("Extn");
          return extnElem.getAttribute("ExtnIsStoreClearance");
        }
      }
    } catch (Exception e) {
      LOGGER.debug(" Inside catch of GCProcessBackordersAPI.getParentStoreClearanceFlag()", e.getMessage());
      YFCException yfce = new YFCException(GCErrorConstants.ERROR_ON_SCHEDULING);
      throw yfce;
    }
    return "";

  }

  /**
   * Split the lines scheduled and invoke change order
   *
   * @param env
   * @param newInputDoc
   * @param orderHeaderKey
   * @param allocationRuleID
   * @return
   */
  private boolean splitAndCallChangeOrder(YFSEnvironment env, YFCDocument newInputDoc, String orderHeaderKey,
      String allocationRuleID, String sLevelOfService) {
    LOGGER.verbose("Call splitOrderLines method of GCProcessATGOrders for OrderLine Splitting if required");
    try {
      GCProcessATGOrders gcProcessATGOrdObj = new GCProcessATGOrders();

      Document splitOrderLinesOp = gcProcessATGOrdObj.splitOrderLines(env, newInputDoc.getDocument());
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("\n splitted OrderLinesOp" + YFCDocument.getDocumentFor(splitOrderLinesOp).toString());
      }
      YFCElement splitOrderEle = YFCDocument.getDocumentFor(splitOrderLinesOp).getDocumentElement();
      YFCElement splitOrderLinesEle = splitOrderEle.getChildElement(GCConstants.ORDER_LINES);
      YFCNodeList<YFCElement> splitOrderLineNList = splitOrderLinesEle.getElementsByTagName(GCConstants.ORDER_LINE);

      for (YFCElement splitOrderLineEle : splitOrderLineNList) {
        String compositionRatioAttr = splitOrderLineEle.getAttribute("CompositionRatio");
        if (!YFCCommon.isVoid(compositionRatioAttr)) {
          splitOrderLineEle.removeAttribute("CompositionRatio");
        }
      }

      callChangeOrderApi(env, splitOrderLinesOp, sLevelOfService);

      YFCDocument orderListTempDoc = YFCDocument.getDocumentFor("<OrderList><Order PaymentStatus=''/></OrderList>");
      YFCDocument getOrderList = GCCommonUtil.invokeGetOrderListAPI(env, orderHeaderKey, orderListTempDoc);
      YFCElement orderElem = getOrderList.getDocumentElement().getChildElement(GCConstants.ORDER);

      String paymentStatusBeforeSchedule = orderElem.getAttribute(GCConstants.PAYMENT_STATUS);
      // getPaymentStatusToScheduleOrder(
      // env, orderHeaderKey);
      if (YFCCommon.equals(paymentStatusBeforeSchedule, "AWAIT_AUTH")
          || YFCCommon.equals(paymentStatusBeforeSchedule, "AWAIT_PAY_INFO")) {
        YFCDocument orderDoc = YFCDocument.createDocument("Order");
        orderDoc.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
        GCCommonUtil.invokeService(env, "GCProcessPaymentForScheduleOnSuccess", orderDoc);
        getOrderList = GCCommonUtil.invokeGetOrderListAPI(env, orderHeaderKey, orderListTempDoc);
        YFCElement orderElement = getOrderList.getDocumentElement().getChildElement("Order");
        paymentStatusBeforeSchedule = orderElement.getAttribute("PaymentStatus");
      }
      if (!YFCCommon.equals(paymentStatusBeforeSchedule, "AWAIT_AUTH")
          && !YFCCommon.equals(paymentStatusBeforeSchedule, "AWAIT_PAY_INFO")) {

        // Unschedule Eligible Lines
        unscheduleEligibleLines(env, orderHeaderKey);

        LOGGER.verbose("Unscheduled the order");

        YFCDocument schedOrderDoc =
            YFCDocument.getDocumentFor("<ScheduleOrder AllocationRuleID='" + allocationRuleID + "' OrderHeaderKey='"
                + orderHeaderKey + "'></ScheduleOrder>");
        LOGGER.verbose("Going to call scheduleOrderAPI:");
        env.setTxnObject("DoNotIgnoreDAXDemand", "Y");
        LOGGER.verbose("DoNotIgnoreDAXDemand=Y");
        env.setTxnObject("IsManualSchedule", "Y");
        LOGGER.verbose("IsManualSchedule=Y");

        try {
          GCCommonUtil.invokeAPI(env, GCConstants.API_SCHEDULE_ORDER, schedOrderDoc, null);
          env.setTxnObject("DoNotIgnoreDAXDemand", "N");
          LOGGER.verbose("DoNotIgnoreDAXDemand=N");
          env.setTxnObject("IsManualSchedule", "N");

          LOGGER.verbose("IsManualSchedule=N");
        } catch (Exception e) {
          LOGGER.debug(" Inside catch of GCProcessBackordersAPI.splitAndCallChangeOrder()", e.getMessage());
          YFCException yfce = new YFCException(GCErrorConstants.ERROR_ON_SCHEDULING);
          throw yfce;
        }
      }
    } catch (Exception e) {
      LOGGER.debug(" Inside catch of GCProcessBackordersAPI.splitAndCallChangeOrder()", e.getMessage());
      YFCException yfce = new YFCException(GCErrorConstants.ERROR_ON_SCHEDULING);
      throw yfce;
    }
    return true;

  }

  /**
   * A method which computes the split ratio for all scheduled quantity
   *
   * @param orderLineKey
   * @param orderEle
   * @param orderLineEle
   * @param compositionRatio
   * @param isScheduleAgainstFutureDate
   * @param changeBOStatusLines
   * @return
   * @throws GCException
   */
  private String getScheduledQtyRatio(String orderLineKey, YFCElement orderEle, YFCElement orderLineEle,
      String compositionRatio, Map<String, Boolean> isScheduleAgainstFutureDate) throws GCException {

    LOGGER.debug(" Entering GCProcessBackordersAPI.getScheduledQtyRatio() method ");
    LOGGER.beginTimer(" GCProcessBackordersAPI.getScheduledQtyRatio() method ");
    try {
      LOGGER.debug("orderLineKey" + orderLineKey);
      LOGGER.debug("orderEle" + orderEle);
      LOGGER.debug("orderLineEle" + orderLineEle);
      LOGGER.debug("compositionRatio" + compositionRatio);
      LOGGER.debug("isScheduleAgainstFutureDate" + isScheduleAgainstFutureDate);
      boolean bAssignmentFound = false;
      Calendar calSysDate = Calendar.getInstance();

      YFCElement promiseLinesEle = orderEle.getElementsByTagName("PromiseLines").item(0);
      if (!YFCCommon.isVoid(promiseLinesEle)) {
        YFCNodeList<YFCElement> promiseLineNList = promiseLinesEle.getElementsByTagName("PromiseLine");
        for (YFCElement promiseLine : promiseLineNList) {
          String orderLKey = promiseLine.getAttribute(GCConstants.ORDER_LINE_KEY);
          if (orderLineKey.equals(orderLKey)) {
            bAssignmentFound = true;
            YFCElement assignmentsEle = promiseLine.getChildElement("Assignments");
            YFCNodeList<YFCElement> assignNList = assignmentsEle.getElementsByTagName("Assignment");
            for (YFCElement assignment : assignNList) {
              YFCElement suppliesEle = assignment.getChildElement("Supplies");
              // Fix for GCSTORE-2901 & 2904 start
              boolean isItOnhandInvForStore = false; // This is to make sure that if scheduling is
              // happening for next day when store is closed
              // then do not move it to SchedulePODAX
              String shipNode = assignment.getAttribute("ShipNode");
              Double qtyFromUnplannedInv = assignment.getDoubleAttribute("QuantityFromUnplannedInventory");
              if (!YFCCommon.equals("MFI", shipNode, true) && qtyFromUnplannedInv <= 0.00) {
                isItOnhandInvForStore = true;
              }
              // Fix for GCSTORE-2901 & 2904 End
              if (!YFCCommon.isVoid(suppliesEle)) {
                compositionRatio =
                    getCompositionRatio(orderLineKey, compositionRatio, isScheduleAgainstFutureDate, calSysDate,
                        assignment, isItOnhandInvForStore);
              }
            }
          }
        }
      }
      // This if is considered if inner split occurs for a completely
      // scheduled order line - rare scenario
      String maxLineStatus = orderLineEle.getAttribute("MaxLineStatus");
      String sMaxLineStatus = GCShipmentUtil.checkAndConvertOrderStatus(maxLineStatus);
      Double dmaxLineStatus = Double.parseDouble(sMaxLineStatus);
      if (!bAssignmentFound && YFCCommon.isVoid(compositionRatio) && YFCCommon.equals(maxLineStatus, "1500")) {
        YFCElement schedElements = orderLineEle.getElementsByTagName("Schedules").item(0);
        if (!YFCCommon.isVoid(schedElements)) {
          YFCNodeList<YFCElement> schedNList = schedElements.getElementsByTagName("Schedule");
          for (YFCElement schedule : schedNList) {
            if (schedNList.getLength() > 1) {
              String qty = schedule.getAttribute(GCConstants.QUANTITY);
              if (!YFCCommon.isVoid(compositionRatio)) {
                compositionRatio = compositionRatio.concat(":");
              }
              compositionRatio = compositionRatio.concat(Integer.toString((int) Double.parseDouble(qty)));
            }
          }
        }
      }
      
      // To split for Partial SchedulePODAX Allocation
      String sMinLineStatus = orderLineEle.getAttribute(GCConstants.MIN_LINE_STATUS);
      if (bAssignmentFound && !YFCCommon.isVoid(compositionRatio) && YFCCommon.equals(maxLineStatus, "1500.10") && YFCCommon.equalsIgnoreCase(sMinLineStatus, "1500")) {
        String ordQty = orderLineEle.getAttribute(GCConstants.ORDERED_QTY);
        String qtyLeft = Integer.toString((int)(Double.parseDouble(ordQty) - Double.parseDouble(compositionRatio)));
        compositionRatio = compositionRatio.concat(":").concat(qtyLeft);
      }
      
      // To fix ExStore Issue::Begin
      if (bAssignmentFound && !YFCCommon.isVoid(compositionRatio) && dmaxLineStatus > 3200) {
        YFCElement statusElements = orderLineEle.getElementsByTagName("OrderStatuses").item(0);
        if (!YFCCommon.isVoid(statusElements)) {
          YFCNodeList<YFCElement> statsNList = statusElements.getElementsByTagName("OrderStatus");
          for (YFCElement status : statsNList) {
            String statusNo = status.getAttribute("Status");
            Double dStatusNo = Double.parseDouble(statusNo);
            if (dStatusNo > 3200 && dStatusNo != 9000) {
              String qty = status.getAttribute("StatusQty");
              if (!YFCCommon.isVoid(compositionRatio)) {
                compositionRatio = compositionRatio.concat(":");
              }
              compositionRatio = compositionRatio.concat(Integer.toString((int) Double.parseDouble(qty)));
            }
          }
        }
      }
      // To fix ExStore Issue::End
      LOGGER.debug("output compositionRatio" + compositionRatio);
      LOGGER.debug(" Exiting GCProcessBackordersAPI.getScheduledQtyRatio() method ");
      LOGGER.endTimer(" GCProcessBackordersAPI.getScheduledQtyRatio() method ");
      return compositionRatio;
    } catch (Exception e) {
      LOGGER.debug(" Inside catch of GCProcessBackordersAPI.getScheduledQtyRatio()", e.getMessage());
      YFCException yfce = new YFCException(GCErrorConstants.ERROR_ON_SCHEDULING);
      throw yfce;
    }
  }

  /*
   * private String getCompositionRatio(String orderLineKey, String compositionRatio, Map<String,
   * Boolean> isScheduleAgainstFutureDate, Calendar calSysDate, YFCElement suppliesEle, boolean
   * isItOnhandInvForStore) throws ParseException {
   * LOGGER.debug(" Entering GCProcessBackordersAPI.getCompositionRatio() method ");
   * LOGGER.debug("orderLineKey : " + orderLineKey); LOGGER.debug("compositionRatio : " +
   * compositionRatio); LOGGER.debug("isScheduleAgainstFutureDate : " +
   * isScheduleAgainstFutureDate); LOGGER.debug("suppliesEle : " + suppliesEle);
   * LOGGER.debug("isItOnhandInvForStore : " + isItOnhandInvForStore); YFCNodeList<YFCElement>
   * supplyNList = suppliesEle.getElementsByTagName("Supply"); for (YFCElement supply : supplyNList)
   * { String strFirstShipDate = supply.getAttribute(GCConstants.FIRST_SHIP_DATE); String
   * sConsumedQuantity = supply.getAttribute(GCConstants.CONSUMED_QUANTITY); Calendar
   * calFirstShipDate = Calendar.getInstance(); Date productAvailabeDate = new
   * SimpleDateFormat(GCConstants.DATE_FORMAT).parse(strFirstShipDate);
   * calFirstShipDate.setTime(productAvailabeDate); if
   * (calFirstShipDate.getTime().compareTo(calSysDate.getTime()) > 0 && !isItOnhandInvForStore) {
   * isScheduleAgainstFutureDate.put(orderLineKey, true); if (!YFCCommon.isVoid(compositionRatio)) {
   * compositionRatio = ":".concat(compositionRatio); } compositionRatio = Integer.toString((int)
   * Double.parseDouble(sConsumedQuantity)).concat(compositionRatio); } else { if
   * (!YFCCommon.isVoid(compositionRatio)) { compositionRatio = compositionRatio.concat(":"); }
   * compositionRatio = compositionRatio.concat(Integer.toString((int)
   * Double.parseDouble(sConsumedQuantity))); } }
   *
   * LOGGER.debug("output compositionRatio : " + compositionRatio);
   * LOGGER.debug(" Exiting GCProcessBackordersAPI.getCompositionRatio() method "); return
   * compositionRatio; }
   */


  private String getCompositionRatio(String orderLineKey, String compositionRatio,
      Map<String, Boolean> isScheduleAgainstFutureDate, Calendar calSysDate, YFCElement assignment,
      boolean isItOnhandInvForStore) throws ParseException {
    try {
      LOGGER.debug(" Entering GCProcessBackordersAPI.getCompositionRatio() method ");
      LOGGER.debug("orderLineKey : " + orderLineKey);
      LOGGER.debug("compositionRatio : " + compositionRatio);
      LOGGER.debug("isScheduleAgainstFutureDate : " + isScheduleAgainstFutureDate);
      LOGGER.debug("assignment : " + assignment);
      LOGGER.debug("isItOnhandInvForStore : " + isItOnhandInvForStore);

      String strFirstShipDate = assignment.getAttribute(GCConstants.SHIP_DATE);
      if (!YFCCommon.isVoid(strFirstShipDate)) {
        String sConsumedQuantity = assignment.getAttribute(GCConstants.QUANTITY);
        Calendar calFirstShipDate = Calendar.getInstance();
        Date productAvailabeDate = new SimpleDateFormat(GCConstants.DATE_FORMAT).parse(strFirstShipDate);
        calFirstShipDate.setTime(productAvailabeDate);
        if (calFirstShipDate.getTime().compareTo(calSysDate.getTime()) > 0 && !isItOnhandInvForStore) {
          isScheduleAgainstFutureDate.put(orderLineKey, true);
          if (!YFCCommon.isVoid(compositionRatio)) {
            compositionRatio = ":".concat(compositionRatio);
          }
          compositionRatio = Integer.toString((int) Double.parseDouble(sConsumedQuantity)).concat(compositionRatio);
        } else {
          if (!YFCCommon.isVoid(compositionRatio)) {
            compositionRatio = compositionRatio.concat(":");
          }
          compositionRatio = compositionRatio.concat(Integer.toString((int) Double.parseDouble(sConsumedQuantity)));
        }
      }
    } catch (Exception e) {
      LOGGER.debug(" Inside catch of GCProcessBackordersAPI.getCompositionRatio()", e.getMessage());
      YFCException yfce = new YFCException(GCErrorConstants.ERROR_ON_SCHEDULING);
      throw yfce;
    }
    LOGGER.debug("output compositionRatio : " + compositionRatio);
    LOGGER.debug(" Exiting GCProcessBackordersAPI.getCompositionRatio() method ");
    return compositionRatio;
  }


  /**
   * This method checks the Order Line and see if it has any non-backorderable item with serial/
   * unique/ store clearance request
   *
   * @param env
   * @param orderLineEle
   * @return
   */
  private boolean isStoreItemNonBackorderable(YFCElement orderLineEle) {
    try {
      YFCElement orderExtnEle = orderLineEle.getChildElement("Extn");
      if (!YFCCommon.isVoid(orderExtnEle)
          && YFCCommon.equals(orderExtnEle.getAttribute("ExtnIsStoreClearance"), GCConstants.YES, false)) {
        return true;
      }

      YFCElement orderLineInvAttReqEle = orderLineEle.getChildElement("OrderLineInvAttRequest");
      if (!YFCCommon.isVoid(orderLineInvAttReqEle) && !YFCCommon.isVoid(orderLineInvAttReqEle.getAttribute("BatchNo"))) {
        return true;
      }

      YFCElement itemDetailsEle = orderLineEle.getChildElement("ItemDetails");
      if (!YFCCommon.isVoid(itemDetailsEle)) {
        YFCElement itemExtnEle = itemDetailsEle.getChildElement("Extn");
        if (!YFCCommon.isVoid(itemExtnEle)
            && YFCCommon.equals(itemExtnEle.getAttribute("ExtnIsUsedOrVintageItem"), GCConstants.YES, false)) {
          return true;
        }
      }
    } catch (Exception e) {
      LOGGER.debug(" Inside catch of GCProcessBackordersAPI.isStoreItemNonBackorderable()", e.getMessage());
      YFCException yfce = new YFCException(GCErrorConstants.ERROR_ON_SCHEDULING);
      throw yfce;
    }
    return false;
  }

  /**
   * This Method calls changeOrder API for OrderLines Which are Splitted and first Bundle Component
   * OrderLine
   *
   * @param env
   * @param splitOrderLinesOp
   */
  private void callChangeOrderApi(YFSEnvironment env, Document splitOrderLinesOp, String sLevelOfService) {
    LOGGER.beginTimer("GCSplitOrderLineService.callChangeOrderApi");
    LOGGER.verbose("Class: GCSplitOrderLineService Method: callChangeOrderApi ::BEGIN");
    try {
      YFCDocument changeOrdIp = YFCDocument.createDocument(GCConstants.ORDER);
      YFCElement orderEleForChangeOrder = changeOrdIp.getDocumentElement();
      orderEleForChangeOrder.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
      if (!YFCCommon.isVoid(sLevelOfService)) {
        orderEleForChangeOrder.setAttribute(GCConstants.LEVEL_OF_SERVICE, sLevelOfService);
      }
      orderEleForChangeOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, YFCDocument.getDocumentFor(splitOrderLinesOp)
          .getDocumentElement().getAttribute(GCConstants.ORDER_HEADER_KEY));
      orderEleForChangeOrder.setAttribute(GCConstants.ENTERPRISE_CODE, YFCDocument.getDocumentFor(splitOrderLinesOp)
          .getDocumentElement().getAttribute(GCConstants.ENTERPRISE_CODE));
      YFCElement ordLinesForChangeOrder = orderEleForChangeOrder.createChild(GCConstants.ORDER_LINES);
      YFCNodeList<YFCElement> nlOrdLineAfterSplitCall =
          YFCDocument.getDocumentFor(splitOrderLinesOp).getDocumentElement()
          .getChildElement(GCConstants.ORDER_LINES, true).getElementsByTagName(GCConstants.ORDER_LINE);
      LOGGER.verbose("find OrderLine which are splitted");
      for (YFCElement eOrderLine : nlOrdLineAfterSplitCall) {
        if (YFCCommon.isVoid(eOrderLine.getAttribute(GCConstants.PRIME_LINE_NO))) {
          LOGGER.verbose("OrderLine with PrimeLineNo as Blank is splitted OrderLine");
          YFCElement splittedOrdLineClone = (YFCElement) eOrderLine.cloneNode(true);
          YFCElement bundleParentLine = splittedOrdLineClone.getChildElement(GCConstants.BUNDLE_PARENT_LINE);
          LOGGER.verbose("OrderLine with BundleParentLine Element are component OrderLine of Parent Bundle");
          if (YFCCommon.equals(GCConstants.CREATE, splittedOrdLineClone.getAttribute(GCConstants.ACTION))
              && !YFCCommon.isVoid(bundleParentLine)) {
            LOGGER.verbose("first splitted Bundle Component OrderLine is included in ChangeOrder API call");
            continue;
          }

          removeAttNElementsForChangeOrderCall(splittedOrdLineClone);
          ordLinesForChangeOrder.importNode(splittedOrdLineClone);
        }
      }
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("\n changeOrdIp" + changeOrdIp.toString());
      }

      // Setting Override = "Y"
      changeOrdIp.getDocumentElement().setAttribute(GCConstants.OVERRIDE, GCConstants.FLAG_Y);

      env.setTxnObject("Publish", "N");
      env.setTxnObject("DoNotPublishToDAX", "Y");
      env.setTxnObject("DoNotSendCancellationEMail", "Y");
      Document docChangeOrderOP = GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, changeOrdIp.getDocument());

      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("\n docChangeOrderOP output Doc" + YFCDocument.getDocumentFor(docChangeOrderOP).toString());
      }
    } catch (Exception e) {
      LOGGER.debug(" Inside catch of GCProcessBackordersAPI.callChangeOrderApi()", e.getMessage());
      YFCException yfce = new YFCException(GCErrorConstants.ERROR_ON_SCHEDULING);
      throw yfce;
    }
    LOGGER.verbose("GCSplitOrderLineService.callChangeOrderApi method :: END");
    LOGGER.endTimer("GCSplitOrderLineService.callChangeOrderApi");
  }

  /**
   * This method removes attributes and Elements from OrderLine for ChangeOrder API Call.
   *
   * @param cloneOrderLine
   * @param cloneItem
   */
  private void removeAttNElementsForChangeOrderCall(YFCElement cloneOrderLine) {
    LOGGER.beginTimer("GCSplitOrderLineService.removeAttNElementsForChangeOrderCall");
    LOGGER.verbose("Class: GCSplitOrderLineService Method: removeAttNElementsForChangeOrderCall ::BEGIN");
    try {
      cloneOrderLine.removeAttribute(GCXmlLiterals.ALLOCATION_DATE);
      cloneOrderLine.removeAttribute(GCXmlLiterals.REMAINING_QTY);
      cloneOrderLine.removeAttribute(GCConstants.STATUS);
      cloneOrderLine.removeAttribute(GCXmlLiterals.MAX_LINE_STATUS);
      cloneOrderLine.removeAttribute(GCXmlLiterals.MAX_LINE_STATUS_DESC);
      cloneOrderLine.removeAttribute(GCXmlLiterals.MIN_LINE_STATUS);
      cloneOrderLine.removeAttribute(GCXmlLiterals.MIN_LINE_STATUS_DESC);
      cloneOrderLine.removeAttribute(GCXmlLiterals.OPEN_QTY);
      cloneOrderLine.removeAttribute(GCXmlLiterals.OTHER_CHARGES);
      cloneOrderLine.removeAttribute(GCXmlLiterals.ORG_ORD_QTY);
      cloneOrderLine.removeAttribute(GCConstants.STATUS_QUANTITY);
      cloneOrderLine.removeAttribute(GCXmlLiterals.INVOICE_QTY);
      cloneOrderLine.removeAttribute(GCXmlLiterals.LINE_SEQ_NO);

      YFCElement linePriceInfo = cloneOrderLine.getChildElement(GCConstants.LINE_PRICE_INFO);
      if (!YFCCommon.isVoid(linePriceInfo)) {
        linePriceInfo.removeAttribute(GCConstants.LINE_TOTAL);
        linePriceInfo.removeAttribute(GCConstants.LINE_TOTAL_WITHOUT_TAX);
      }
      // Handling Bundle Items
      YFCElement cloneItem = cloneOrderLine.getChildElement(GCConstants.ITEM);
      if (!YFCCommon.isVoid(cloneItem)
          && YFCCommon.equals(GCConstants.BUNDLE, cloneOrderLine.getAttribute(GCConstants.KIT_CODE))
          && !YFCCommon.isVoid(GCXmlLiterals.BUNDLE_FULLFILLMENT_MODE)) {
        cloneItem.removeAttribute(GCXmlLiterals.BUNDLE_FULLFILLMENT_MODE);
      }
      YFCElement cloneLineTaxes = cloneOrderLine.getChildElement(GCConstants.LINE_TAXES);
      if (!YFCCommon.isVoid(cloneLineTaxes)) {
        YFCElement sTaxSummary = cloneLineTaxes.getChildElement(GCXmlLiterals.TAX_SUMMARY);
        if (!YFCCommon.isVoid(sTaxSummary)) {
          cloneLineTaxes.removeChild(sTaxSummary);
        }
        YFCNodeList<YFCElement> nlLineTax = cloneLineTaxes.getElementsByTagName(GCConstants.LINE_TAX);
        for (int i = 0; i < nlLineTax.getLength(); i++) {
          YFCElement lineTax = nlLineTax.item(i);
          lineTax.removeAttribute(GCXmlLiterals.INVOICE_TAX);
          lineTax.removeAttribute(GCXmlLiterals.REF_1);
          lineTax.removeAttribute(GCXmlLiterals.REF_2);
          lineTax.removeAttribute(GCXmlLiterals.REF_3);
          lineTax.removeAttribute(GCXmlLiterals.REMAINING_TAX);
          // GCSTORE-3786::Begin
          // lineTax.removeAttribute(GCXmlLiterals.TAX_PERCENTAGE);
          // GCSTORE-3786::End
        }
      }
      YFCElement cloneLineCharges = cloneOrderLine.getChildElement(GCConstants.LINE_CHARGES);
      if (!YFCCommon.isVoid(cloneLineCharges)) {
        YFCNodeList<YFCElement> nlLineCharge = cloneLineCharges.getElementsByTagName(GCConstants.LINE_CHARGE);
        for (int j = 0; j < nlLineCharge.getLength(); j++) {
          YFCElement lineCharge = nlLineCharge.item(j);
          lineCharge.removeAttribute(GCConstants.CHARGE_AMOUNT);
          lineCharge.removeAttribute(GCXmlLiterals.INVOICE_CHRGE_AMOUNT);
          lineCharge.removeAttribute(GCXmlLiterals.INVOICE_CHRGE_PER_LINE);
          lineCharge.removeAttribute(GCXmlLiterals.INVOICE_CHRGE_PER_UNIT);
          lineCharge.removeAttribute(GCXmlLiterals.REMAINING_CHARGE_AMOUNT);
          lineCharge.removeAttribute(GCXmlLiterals.REMAINING_CHARGE_PER_LINE);
          lineCharge.removeAttribute(GCXmlLiterals.REMAINING_CHARGE_PER_UNIT);
          lineCharge.removeAttribute(GCConstants.REFERENCE);
        }
      }
      YFCElement clonePersonInfoShipTo = cloneOrderLine.getChildElement(GCConstants.PERSON_INFO_SHIP_TO);
      if (!YFCCommon.isVoid(clonePersonInfoShipTo)) {
        cloneOrderLine.removeChild(clonePersonInfoShipTo);
      }
      YFCElement cloneOrderlineSchedules = cloneOrderLine.getChildElement(GCConstants.SCHEDULES);
      if (!YFCCommon.isVoid(cloneOrderlineSchedules)) {
        cloneOrderLine.removeChild(cloneOrderlineSchedules);
      }
      YFCElement cloneOrderlineStatuses = cloneOrderLine.getChildElement(GCConstants.ORDER_STATUSES);
      if (!YFCCommon.isVoid(cloneOrderlineStatuses)) {
        cloneOrderLine.removeChild(cloneOrderlineStatuses);
      }
      YFCElement cloneParentOrdLineRelationships =
          cloneOrderLine.getChildElement(GCXmlLiterals.PARENT_ORD_LINE_RELATIONSHIP);
      if (!YFCCommon.isVoid(cloneParentOrdLineRelationships)) {
        cloneOrderLine.removeChild(cloneParentOrdLineRelationships);
      }
      YFCElement cloneLineOverallTotals = cloneOrderLine.getChildElement(GCXmlLiterals.LINE_OVERALL_TOTALS);
      if (!YFCCommon.isVoid(cloneLineOverallTotals)) {
        cloneOrderLine.removeChild(cloneLineOverallTotals);
      }
      YFCElement cloneLineRemainingTotals = cloneOrderLine.getChildElement(GCXmlLiterals.LINE_REMAINING_TOTALS);
      if (!YFCCommon.isVoid(cloneLineRemainingTotals)) {
        cloneOrderLine.removeChild(cloneLineRemainingTotals);
      }
      YFCElement cloneLineInvoicedTotals = cloneOrderLine.getChildElement(GCXmlLiterals.LINE_INVOICE_TOTALS);
      if (!YFCCommon.isVoid(cloneLineInvoicedTotals)) {
        cloneOrderLine.removeChild(cloneLineInvoicedTotals);
      }
      YFCElement cloneAdditionalAddresses = cloneOrderLine.getChildElement(GCXmlLiterals.ADDITIONAL_ADDRESSES);
      if (!YFCCommon.isVoid(cloneAdditionalAddresses)) {
        cloneOrderLine.removeChild(cloneAdditionalAddresses);
      }
      YFCElement cloneKitLines = cloneOrderLine.getChildElement(GCXmlLiterals.KIT_LINES);
      if (!YFCCommon.isVoid(cloneKitLines)) {
        cloneOrderLine.removeChild(cloneKitLines);
      }
      YFCElement cloneReferences = cloneOrderLine.getChildElement(GCConstants.REFERENCES);
      if (!YFCCommon.isVoid(cloneReferences)) {
        cloneOrderLine.removeChild(cloneReferences);
      }
      YFCElement cloneInstructions = cloneOrderLine.getChildElement(GCXmlLiterals.INSTRUCTIONS);
      if (!YFCCommon.isVoid(cloneInstructions)) {
        cloneOrderLine.removeChild(cloneInstructions);
      }
      YFCElement cloneOrderLineReservations = cloneOrderLine.getChildElement(GCXmlLiterals.ORD_LINE_RESERVATIONS);
      if (!YFCCommon.isVoid(cloneOrderLineReservations) && cloneOrderLineReservations.hasChildNodes()) {
        cloneOrderLineReservations.setAttribute(GCConstants.RESET, GCConstants.YES);
      }
      YFCElement orderLineSourcingControls = cloneOrderLine.getChildElement("OrderLineSourcingControls");
      if (!YFCCommon.isVoid(orderLineSourcingControls)) {
        YFCNodeList<YFCElement> nlOrderLineSourcingCntrl =
            orderLineSourcingControls.getElementsByTagName("OrderLineSourcingCntrl");
        for (YFCElement orderLineSourcingCntrl : nlOrderLineSourcingCntrl) {
          orderLineSourcingCntrl.removeAttribute("OrderLineKey");
          orderLineSourcingCntrl.removeAttribute("OrderLineSourcingCntrlKey");
        }
      }
    } catch (Exception e) {
      LOGGER.debug(" Inside catch of GCProcessBackordersAPI.removeAttFromOrderLineAndItemEle()", e.getMessage());
      YFCException yfce = new YFCException(GCErrorConstants.ERROR_ON_SCHEDULING);
      throw yfce;
    }
    LOGGER.verbose("GCSplitOrderLineService.removeAttFromOrderLineAndItemEle method :: END");
    LOGGER.endTimer("GCSplitOrderLineService.removeAttFromOrderLineAndItemEle");
  }

  /**
   * This method set IsFirmPredefinedNode based upon if it is gift/ single source/ warranty item (do
   * not set "N") or not
   *
   * @param env
   * @param newOrderLineEle
   * @param orgCode
   */
  private boolean canIgnoreIsFirmPredefinedNode(YFSEnvironment env, YFCElement newOrderLineEle) {

    LOGGER.beginTimer("GCProcessBackordersAPI.canIgnoreIsFirmPredefinedNode");
    try {
      String isFirmPredefinedNode = newOrderLineEle.getAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE);
      String shipNode = newOrderLineEle.getAttribute(GCConstants.SHIP_NODE);
      String isStoreClearance = "";
      YFCElement newOrderLineExtnEle = newOrderLineEle.getChildElement("Extn");
      if (!YFCCommon.isVoid(newOrderLineExtnEle)) {
        isStoreClearance = newOrderLineExtnEle.getAttribute("ExtnIsStoreClearance");
      }
      if (YFCCommon.equals(isFirmPredefinedNode, GCConstants.YES, false) && !YFCCommon.isVoid(shipNode)) {
        // if fulfillment type is pick up in store and if
        // IsFirmPredefinedNode=Y and shipNode is not
        // blank
        // get the itemDetails
        LOGGER.verbose("OrderLine has ShipNode=" + shipNode + " with isFirmPredefinedNode=Y");
        YFCElement itemEle = newOrderLineEle.getElementsByTagName("Item").item(0);
        String itemId = itemEle.getAttribute("ItemID");
        String uom = itemEle.getAttribute("UnitOfMeasure");
        // call getItemlist api
        YFCDocument getItemListOutDoc =
            GCCommonUtil.getItemList(env, itemId, uom, "", true,
                YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LIST_TEMPLATE_ORDER_PROMISING));
        boolean isSingleSourced = GCOrderUtil.isSingleSourced(getItemListOutDoc);
        boolean isDigital = GCOrderUtil.isDigital(getItemListOutDoc);
        boolean isGiftCard = GCOrderUtil.isGiftCard(getItemListOutDoc);
        boolean isClearanceItem = GCOrderUtil.isClearanceItem(getItemListOutDoc);

        boolean clearanceItemWithoutReq = !YFCCommon.equals(isStoreClearance, GCConstants.YES, false) && isClearanceItem;

        boolean isSerialRequest = false;
        YFCElement orderLineInvAttReq = newOrderLineEle.getChildElement("OrderLineInvAttRequest");
        if (!YFCCommon.isVoid(orderLineInvAttReq)) {
          isSerialRequest = !YFCCommon.isVoid(orderLineInvAttReq.getAttribute("BatchNo"));
        }
        // check if the item is Not SingleSourced and Not Digital and Not
        // GiftCard and Not regular
        // version of store clearance item (to be fulfilled from MFI only)
        if (isSingleSourced || isDigital || isGiftCard || clearanceItemWithoutReq || isSerialRequest) {
          LOGGER.verbose("OrderLine has isSingleSourced=" + isSingleSourced + ", isDigital=" + isDigital
              + ", isGiftCard=" + isGiftCard + ", Normal request for Clearance Item=" + clearanceItemWithoutReq
              + ", isSerialRequest=" + isSerialRequest);
          LOGGER.verbose("Returning true as FirmPreDefined is for unique items/ inventory and can be ignored");
          return true;
        }
      }
    } catch (Exception e) {
      LOGGER.debug(" Inside catch of GCProcessBackordersAPI.canIgnoreIsFirmPredefinedNode()", e.getMessage());
      YFCException yfce = new YFCException(GCErrorConstants.ERROR_ON_SCHEDULING);
      throw yfce;
    }
    LOGGER.endTimer("GCProcessBackordersAPI.canIgnoreIsFirmPredefinedNode");
    LOGGER.verbose("Returning false as FirmPreDefined is not for unique items/ inventory and can not be ignored");
    return false;
  }

  /**
   * This method iterates through Bundle item and if no partial backorder happens for a bundle. it
   * has been removed from the document Else left in the document to process split
   *
   * @param orderLinesEle
   * @param orderLinesEle2
   * @param oLKeyCompRatio
   * @param orgCode
   * @param isScheduleAgainstFutureDate
   * @throws ParseException
   * @throws GCException
   */

  private boolean filterOrderLineForBundleLines(YFSEnvironment env, YFCElement newOrderLinesEle,
      YFCElement orderLinesEle, Map<String, String> oLKeyCompRatio, Map<String, Boolean> isScheduleAgainstFutureDate,
      List<YFCElement> changeBOStatusLines) throws GCException {
    // Iterating through OrderLine again to retain the bundle which are
    // there in the map (orderline
    // key and composition ratio) and removing the rest
    LOGGER.beginTimer("GCProcessBackordersAPI.filterOrderLineForBundleLines");
    boolean rescheduleOrderForPIS = false;
    try {

      boolean isScheduledAgainstFutureDate = false;
      YFCNodeList<YFCElement> orderLineNList = orderLinesEle.getElementsByTagName(GCConstants.ORDER_LINE);
      for (YFCElement orderLineEle : orderLineNList) {
        String maxLineStatus = orderLineEle.getAttribute("MaxLineStatus");
        String compositionRatio = "";
        String orderLineKey = orderLineEle.getAttribute("OrderLineKey");
        if (!isScheduledAgainstFutureDate && !isScheduleAgainstFutureDate.isEmpty()
            && isScheduleAgainstFutureDate.containsKey(orderLineKey)) {
          isScheduledAgainstFutureDate = isScheduleAgainstFutureDate.get(orderLineKey);
        }
        if (YFCCommon.equals(maxLineStatus, GCConstants.STATUS_CREATED, false)) {
          compositionRatio = oLKeyCompRatio.get(orderLineKey);
          if (!YFCCommon.isVoid(compositionRatio)) {
            LOGGER.verbose("Found the composition ratio as " + compositionRatio);
            YFCElement newOrderLineEle = (YFCElement) orderLineEle.cloneNode(true);
            newOrderLineEle.setAttribute("CompositionRatio", compositionRatio);
            String isFirmPredefinedNode = newOrderLineEle.getAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE);
            String fulfillmentType = newOrderLineEle.getAttribute(GCConstants.FULFILLMENT_TYPE);
            String shipNode = newOrderLineEle.getAttribute(GCConstants.SHIP_NODE);
            if (fulfillmentType.indexOf(GCConstants.PICKUP_IN_STORE) >= 0
                && !canIgnoreIsFirmPredefinedNode(env, newOrderLineEle)
                && YFCCommon.equals(isFirmPredefinedNode, GCConstants.YES, false) && !YFCCommon.isVoid(shipNode)
                && isScheduledAgainstFutureDate) {
              newOrderLineEle.setAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE, "N");
              newOrderLineEle.setAttribute(GCConstants.SHIP_NODE, "");
              // Reschedule is always required in case of PIS and
              // atleast some qty is backordered
              rescheduleOrderForPIS = true;
            }

            int indexOfDelimiter = compositionRatio.indexOf(':');
            if (indexOfDelimiter > 0 && indexOfDelimiter < compositionRatio.length() - 1) {
              // go for split if it is partial scenario
              newOrderLinesEle.importNode(newOrderLineEle);
            } else if (rescheduleOrderForPIS) {
              orderLineEle.setAttribute("IsChangeOrderCallReq", "Y");
              changeBOStatusLines.add(orderLineEle);
            }

            LOGGER.verbose("stamped updated composition ratio as " + compositionRatio);

          }
          oLKeyCompRatio.remove(orderLineKey);
        } else {
          LOGGER.verbose("Do not consider Order Line of Bundle for which there is no entry in the map");
        }
      }
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("new document after processing bundle item document is =\n"
            + newOrderLinesEle.getOwnerDocument().toString());
      }
    } catch (Exception e) {
      LOGGER.debug(" Inside catch of GCProcessBackordersAPI.filterOrderLineForBundleLines()", e.getMessage());
      YFCException yfce = new YFCException(GCErrorConstants.ERROR_ON_SCHEDULING);
      throw yfce;
    }
    LOGGER.endTimer("GCProcessBackordersAPI.filterOrderLineForBundleLines");
    return rescheduleOrderForPIS;
  }

  /**
   * This method change the order line status for DAX allocation. only those lines which are
   * backordered move to DAX allocation
   *
   * @param env
   * @param inDoc
   * @param sOrderHeaderKey
   */

  public void changeOrderStatusForDAXAllocation(YFSEnvironment env, Document inDoc, String sOrderHeaderKey) {

    LOGGER.beginTimer("GCProcessBackordersAPI.changeOrderStatusForDAXAllocation");
    try {
      YFCDocument docChangeOrderStatus = YFCDocument.createDocument(GCConstants.ORDER_STATUS_CHANGE);

      YFCElement eleChangeOrderStatus = docChangeOrderStatus.getDocumentElement();
      eleChangeOrderStatus.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
      eleChangeOrderStatus.setAttribute(GCConstants.IGNORE_TRANSACTION_DEPENDENCIES, GCConstants.YES);
      eleChangeOrderStatus.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.CHANGE_SCHEDULE_STATUS);

      YFCElement eleOrderLinesChngStatus = eleChangeOrderStatus.createChild(GCConstants.ORDER_LINES);

      YFCDocument orderDoc = YFCDocument.getDocumentFor(inDoc);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("Input document to changeOrderStatusForDAXAllocation =\n" + orderDoc.toString());
      }

      YFCElement orderEle = orderDoc.getDocumentElement();
      YFCElement orderLinesEle = orderEle.getChildElement(GCConstants.ORDER_LINES);
      boolean callChangeOrderStatus = false;

      YFCNodeList<YFCElement> orderLineNList = orderLinesEle.getElementsByTagName(GCConstants.ORDER_LINE);
      for (YFCElement orderLineEle : orderLineNList) {
        String maxLineStatus = orderLineEle.getAttribute("MaxLineStatus");
        String minLineStatus = orderLineEle.getAttribute("MinLineStatus");
        if (YFCCommon.equals(maxLineStatus, minLineStatus, true)
            && YFCCommon.equals(minLineStatus, GCConstants.STATUS_BACKORDERED, true)) {
          String strOrderLineKey = orderLineEle.getAttribute(GCConstants.ORDER_LINE_KEY);
          YFCElement eleOrderLineChngStatus = eleOrderLinesChngStatus.createChild(GCConstants.ORDER_LINE);

          eleOrderLineChngStatus.setAttribute(GCConstants.ORDER_LINE_KEY, strOrderLineKey);
          eleOrderLineChngStatus.setAttribute(GCConstants.BASE_DROP_STATUS, GCConstants.SCHEDULED_PO_DAX_STATUS);
          eleOrderLineChngStatus.setAttribute(GCConstants.CHANGE_FOR_ALL_AVAIL_QTY, GCConstants.YES);
          callChangeOrderStatus = true;
        }
      }

      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("Input document to changeOrderStatus API =\n" + docChangeOrderStatus.toString());
      }

      if (callChangeOrderStatus) {
        LOGGER.verbose("Invoking ChangeOrderStatus API");
        env.setTxnObject("DoNotSendCancellationEMail", "Y");
        GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER_STATUS, docChangeOrderStatus, null);
        LOGGER.verbose("API ChangeOrderStatus invoked Successfully!");
      }
    } catch (Exception e) {
      LOGGER.debug(" Inside catch of GCProcessBackordersAPI.changeOrderStatusForDAXAllocation()", e.getMessage());
      YFCException yfce = new YFCException(GCErrorConstants.ERROR_ON_SCHEDULING);
      throw yfce;
    }
    LOGGER.endTimer("GCProcessBackordersAPI.changeOrderStatusForDAXAllocation");
  }

  /**
   *
   * This method checks the inventory position and moves the quantity into ScheduledPODAX and
   * BackOrderedDax status
   *
   * @param env
   * @param inDoc
   * @param sOrderHeaderKey
   * @throws GCException
   *
   */

  public void processOrdersOnScheduled(YFSEnvironment env, Document inDoc, String sOrderHeaderKey) throws GCException {
    try {
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("Input xml to processOrdersOnScheduled method::" + GCXMLUtil.getXMLString(inDoc));
      }
      Document docChangeOrderStatus = GCXMLUtil.createDocument(GCConstants.ORDER_STATUS_CHANGE);
      Element eleChangeOrderStatus = docChangeOrderStatus.getDocumentElement();
      eleChangeOrderStatus.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
      eleChangeOrderStatus.setAttribute(GCConstants.IGNORE_TRANSACTION_DEPENDENCIES, GCConstants.YES);
      eleChangeOrderStatus.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.CHANGE_SCHEDULE_STATUS);
      Element eleOrderLinesChngStatus = GCXMLUtil.createElement(docChangeOrderStatus, GCConstants.ORDER_LINES, null);
      eleChangeOrderStatus.appendChild(eleOrderLinesChngStatus);

      NodeList nlPromiseLine = XPathAPI.selectNodeList(inDoc, "Order/SuggestedOption/Option/PromiseLines/PromiseLine");
      for (int iCounter = 0; iCounter < nlPromiseLine.getLength(); iCounter++) {
        LOGGER.debug("Inside for loop iterating on PromiseLine elements");
        String strFromExpectedShipDate = "";
        Element elePromiseLine = (Element) nlPromiseLine.item(iCounter);
        // GCSTORE-1644::Begin
        String strOrderLineKey = elePromiseLine.getAttribute(GCConstants.ORDER_LINE_KEY);
        String isBundleParent = elePromiseLine.getAttribute(GCConstants.IS_BUNDLE_PARENT);
        //Added for GCSTORE-6726 --  Start
        String sForSchedulePODAX = "N";
        Object obj = env.getTxnObject("ResedulingTheBackorderLines");
        if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Env Obj for ResedulingTheBackorderLines" + obj);
		}
        if(!YFCCommon.isVoid(obj)){
		HashMap<String, String> hashM = (HashMap<String, String>) obj;
        if(hashM.containsKey(strOrderLineKey)){
        	sForSchedulePODAX = "Y";
        }
        }
        LOGGER.debug("sForSchedulePODAX Value" + sForSchedulePODAX);
       //Added for GCSTORE-6726 --  End
        // String bundleParentOrderLineKey =
        // elePromiseLine.getAttribute(GCConstants.BUNDLE_PARENT_ORDERLINEKEY);
        // if (!YFCCommon.isVoid(bundleParentOrderLineKey)) {
        // LOGGER.verbose("This is a component line. So skip the logic::" + strOrderLineKey);
        // continue;
        // }
        // GCSTORE-1644::End
        // GCSTORE-1644::Begin-reverted these chnages
        if (YFCCommon.equals(isBundleParent, GCConstants.YES)) {

          continue;
        }
        // GCSTORE-1644::End
        Calendar calSysDate = Calendar.getInstance();
        NodeList nlAssignment = XPathAPI.selectNodeList(elePromiseLine, "Assignments/Assignment");
        for (int iCount = 0; iCount < nlAssignment.getLength(); iCount++) {
          LOGGER.debug("Inside for loop iterating on Assignment elements");
          Element eleAssignment = (Element) nlAssignment.item(iCount);
          String shipNode = eleAssignment.getAttribute(GCConstants.SHIP_NODE);
          // Move the line to SchedulePODAX only if it is sourced form MFI
          if (YFCUtils.equals(GCConstants.MFI, shipNode)) {
            String strFirstShipDate = eleAssignment.getAttribute(GCConstants.PRODUCT_AVAIL_DATE);
            Calendar calRemorseDate1 = Calendar.getInstance();
            if (YFCCommon.isVoid(strFirstShipDate)) {
              continue;
            }
            Date productAvailabeDate = new SimpleDateFormat(GCConstants.DATE_FORMAT).parse(strFirstShipDate);
            calRemorseDate1.setTime(productAvailabeDate);
            //Added for GCSTORE-6726--Start
            if ((calRemorseDate1.getTime().compareTo(calSysDate.getTime()) > 0) || GCConstants.YES.equalsIgnoreCase(sForSchedulePODAX)) {
            //Added for GCSTORE-6726--End
              strFromExpectedShipDate = strFirstShipDate;
              Calendar calFromExpectedShipDate = Calendar.getInstance();
              Date fromExpectedShipDate = new SimpleDateFormat(GCConstants.DATE_FORMAT).parse(strFirstShipDate);
              calFromExpectedShipDate.setTime(fromExpectedShipDate);

              String strShipDate = eleAssignment.getAttribute(GCConstants.PRODUCT_AVAIL_DATE);
              Calendar calShipDate = Calendar.getInstance();
              Date shipDate = new SimpleDateFormat(GCConstants.DATE_FORMAT).parse(strShipDate);
              calShipDate.setTime(shipDate);
              // check if ShipDate is after FromExpectedShipDate,
              // this is done to have the maximum date for the
              // FromExpectedShipDate attr, for changeOrderStatus
              // doc IP
              if (calShipDate.getTime().compareTo(calFromExpectedShipDate.getTime()) > 0) {
                strFromExpectedShipDate = strShipDate;

                LOGGER
                .debug("inside if checking FromExpectedShipDate can not be before ShipDate, strFromExpectedShipDate="
                    + strFromExpectedShipDate);
              }

              String sConsumedQuantity = eleAssignment.getAttribute(GCConstants.QUANTITY);
              double dConsumedQuantity =
                  (!YFCCommon.isVoid(sConsumedQuantity)) ? Double.valueOf(sConsumedQuantity) : 0.00;
                  if (dConsumedQuantity > 0.0) {

                    LOGGER.debug("inside if ConsumedQuantity > 0.0");
                    Element eleOrderLineChngStatus =
                        GCXMLUtil.createElement(docChangeOrderStatus, GCConstants.ORDER_LINE, null);
                    eleOrderLineChngStatus.setAttribute(GCConstants.ORDER_LINE_KEY, strOrderLineKey);
                    eleOrderLineChngStatus.setAttribute(GCConstants.BASE_DROP_STATUS, GCConstants.SCHEDULED_PO_DAX_STATUS);
                    eleOrderLineChngStatus.setAttribute(GCConstants.QUANTITY, String.valueOf(dConsumedQuantity));
                    //eleOrderLineChngStatus.setAttribute("FromExpectedShipDate", strFromExpectedShipDate);
                    eleOrderLinesChngStatus.appendChild(eleOrderLineChngStatus);
                  }
            }
          }
        }
      }

      Element eleOrderLine =
          GCXMLUtil.getElementByXPath(docChangeOrderStatus, "OrderStatusChange/OrderLines/OrderLine");
      if (!YFCCommon.isVoid(eleOrderLine)) {
        LOGGER.debug("Invoking ChangeOrderStatus API");
        env.setTxnObject("DoNotSendCancellationEMail", "Y");
        GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER_STATUS, docChangeOrderStatus);
        LOGGER.debug("API ChangeOrderStatus invoked Successfully!");
      }
    } catch (Exception e) {
      LOGGER.debug(" Inside catch of GCProcessBackordersAPI.processOrdersOnScheduled()", e.getMessage());
      YFCException yfce = new YFCException(GCErrorConstants.ERROR_ON_SCHEDULING);
      throw yfce;
    }
  }

  /**
   * GCSTORE-1644::This method takes parent line key and moves its component lines to scheduled po
   * dax status.
   *
   * @param strOrderLineKey
   * @param eleOrderLinesChngStatus
   * @param docChangeOrderStatus
   * @param strFromExpectedShipDate
   * @param inDoc
   * @throws TransformerException
   */
  // private void changeComponentStatusToScheduledPODAX(String strOrderLineKey, Element
  // eleOrderLinesChngStatus,
  // Document docChangeOrderStatus, String strFromExpectedShipDate, Document inDoc) throws
  // TransformerException {
  // LOGGER.beginTimer("changeComponentStatusToScheduledPODAX");
  // LOGGER.verbose("Inside changeComponentStatusToScheduledPODAX method::");
  // try {
  // LOGGER.verbose("ParentOrderLineKey::" + strOrderLineKey);
  // NodeList compnentLineNodeList =
  // XPathAPI.selectNodeList(inDoc, "Order/OrderLines/OrderLine[BundleParentLine/@OrderLineKey='" +
  // strOrderLineKey
  // + "']");
  // LOGGER.verbose("no of components::" + compnentLineNodeList.getLength());
  // for (int i = 0; i < compnentLineNodeList.getLength(); i++) {
  // Element eleComponentLine = (Element) compnentLineNodeList.item(i);
  // String orderedQty = eleComponentLine.getAttribute(GCConstants.ORDERED_QTY);
  // String orderLineKey = eleComponentLine.getAttribute(GCConstants.ORDER_LINE_KEY);
  // Element eleOrderLineChngStatus = GCXMLUtil.createElement(docChangeOrderStatus,
  // GCConstants.ORDER_LINE, null);
  // eleOrderLineChngStatus.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey);
  // eleOrderLineChngStatus.setAttribute(GCConstants.BASE_DROP_STATUS,
  // GCConstants.SCHEDULED_PO_DAX_STATUS);
  // eleOrderLineChngStatus.setAttribute(GCConstants.QUANTITY, String.valueOf(orderedQty));
  // eleOrderLineChngStatus.setAttribute("FromExpectedShipDate", strFromExpectedShipDate);
  // eleOrderLinesChngStatus.appendChild(eleOrderLineChngStatus);
  // }
  // } catch (Exception e) {
  // LOGGER.debug(" Inside catch of GCProcessBackordersAPI.changeComponentStatusToScheduledPODAX()",
  // e.getMessage());
  // YFCException yfce = new YFCException(GCErrorConstants.ERROR_ON_SCHEDULING);
  // throw yfce;
  // }
  // LOGGER.endTimer("changeComponentStatusToScheduledPODAX");
  //
  // }

  /**
   *
   * This method stamps the ship Node on order for release consolidation
   *
   * @param env
   * @param inDoc
   * @param sOrderHeaderKey
   * @throws GCException
   *
   */

  public void stampShipNodeOnOrderLine(YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("GCProcessBackordersAPI.stampShipNodeOnOrderLine");
    try {
      YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
      YFCElement eleOrder = inputDoc.getDocumentElement();
      String sOrderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
      String sOrgCode = eleOrder.getAttribute(GCConstants.ENTERPRISE_CODE);
      YFCElement eleOrderLines = eleOrder.getChildElement(GCConstants.ORDER_LINES);

      YFCDocument docChangeOrderInput = YFCDocument.createDocument(GCConstants.ORDER);
      YFCElement eleChangeOrder = docChangeOrderInput.getDocumentElement();
      eleChangeOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
      eleChangeOrder.setAttribute(GCConstants.ENTERPRISE_CODE, sOrgCode);
      eleChangeOrder.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
      YFCElement eleChangeOrderLines = eleChangeOrder.createChild(GCConstants.ORDER_LINES);

      YFCNodeList<YFCElement> nlOrderLine = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
      for (YFCElement eleOrderLine : nlOrderLine) {
        String sMaxLineStatus = eleOrderLine.getAttribute(GCConstants.MAX_LINE_STATUS);
        String sMinLineStatus = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_MIN_STATUS);
        String sShipNode = eleOrderLine.getAttribute(GCConstants.SHIP_NODE);
        String sIsFirmPredefinedNode = eleOrderLine.getAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE);
        String sKitCode = eleOrderLine.getAttribute(GCConstants.KIT_CODE);

        List<String> bundleOrderLineKeyList = new ArrayList<String>();
        if(YFCCommon.equalsIgnoreCase(sKitCode, GCConstants.BUNDLE)){
          bundleOrderLineKeyList.add(eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
        }


        if (YFCCommon.equalsIgnoreCase(sMaxLineStatus, GCConstants.STATUS_SCHEDULED)
            && YFCCommon.equalsIgnoreCase(sMinLineStatus, GCConstants.STATUS_SCHEDULED)
            && (YFCCommon.isStringVoid(sShipNode) || YFCCommon.equalsIgnoreCase(sIsFirmPredefinedNode, GCConstants.N))) {

          YFCElement schedElements = eleOrderLine.getElementsByTagName("Schedules").item(0);
          if (!YFCCommon.isVoid(schedElements)) {
            YFCNodeList<YFCElement> schedNList = schedElements.getElementsByTagName("Schedule");
            for (YFCElement schedule : schedNList) {
              String shipNode = schedule.getAttribute(GCConstants.SHIP_NODE);
              if (!YFCCommon.isVoid(shipNode)) {
                String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
                YFCElement eleBundleParentLine = eleOrderLine.getChildElement(GCConstants.BUNDLE_PARENT_LINE);
                if(!YFCCommon.isVoid(eleBundleParentLine) && bundleOrderLineKeyList.contains(eleBundleParentLine.getAttribute(GCConstants.ORDER_LINE_KEY))){
                  YFCElement eleChangeBundleOrderLine = eleChangeOrderLines.createChild(GCConstants.ORDER_LINE);
                  eleChangeBundleOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, eleBundleParentLine.getAttribute(GCConstants.ORDER_LINE_KEY));
                  eleChangeBundleOrderLine.setAttribute(GCConstants.SHIP_NODE, shipNode);
                  eleChangeBundleOrderLine.setAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE, GCConstants.N);
                  eleChangeOrderLines.appendChild(eleChangeBundleOrderLine);
                  bundleOrderLineKeyList.remove(eleBundleParentLine.getAttribute(GCConstants.ORDER_LINE_KEY));
                }
                YFCElement eleChangeOrderLine = eleChangeOrderLines.createChild(GCConstants.ORDER_LINE);
                eleChangeOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, sOrderLineKey);
                eleChangeOrderLine.setAttribute(GCConstants.SHIP_NODE, shipNode);
                eleChangeOrderLine.setAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE, GCConstants.N);
                eleChangeOrderLines.appendChild(eleChangeOrderLine);
                break;
              }
            }
          }
        }
      }

      if (eleChangeOrderLines.hasChildNodes()) {

        // Setting Override = "Y"
        docChangeOrderInput.getDocumentElement().setAttribute(GCConstants.OVERRIDE, GCConstants.FLAG_Y);

        LOGGER.debug("Invoking ChangeOrderStatus API");
        env.setTxnObject("DoNotSendCancellationEMail", "Y");
        env.setTxnObject("DoNotPublishToDAX", "Y");
        GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, docChangeOrderInput, null);
        LOGGER.debug("API ChangeOrderStatus invoked Successfully!");
      }

      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("Input document" + inDoc.toString());
      }
    } catch (Exception e) {
      LOGGER.debug(" Inside catch of GCProcessBackordersAPI.stampShipNodeOnOrderLine()", e.getMessage());
      YFCException yfce = new YFCException(GCErrorConstants.ERROR_ON_SCHEDULING);
      throw yfce;
    }
    LOGGER.endTimer("GCProcessBackordersAPI.stampShipNodeOnOrderLine");
  }

  /**
   * This method add the dependent line in split doc.
   *
   * @param env
   * @param inDoc
   * @return
   */

  private YFCDocument addDependentLineForSplit(YFCDocument splitOrderDoc, Document inDoc) {

    LOGGER.beginTimer(" Entering GCProcessBackordersAPI.addDependentLineForSplit() method ");
    try {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Input document is" + splitOrderDoc.toString());
      }
      List<String> splitOrderLineKeyList = new ArrayList<String>();
      YFCElement eleSplitOrder = splitOrderDoc.getDocumentElement();
      YFCElement eleSplitOrderLines = eleSplitOrder.getChildElement(GCConstants.ORDER_LINES);
      YFCNodeList<YFCElement> nlSplitOrderLine = eleSplitOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
      Map<String, String> ordLineKeyPrimeLineNoMap = new HashMap<String, String>();
      ordLineKeyPrimeLineNoMap = prepareMapForOrderLineKeyNPrimeLineNo(ordLineKeyPrimeLineNoMap, nlSplitOrderLine);
      boolean putHoldOnDependentLine = false;
      for (YFCElement eleSplitOrderLine : nlSplitOrderLine) {
        String sSplitOrderLineKey = eleSplitOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
        splitOrderLineKeyList.add(sSplitOrderLineKey);
        if (!YFCCommon.isVoid(eleSplitOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY))
            && !YFCCommon.isVoid(eleSplitOrderLine.getAttribute("CompositionRatio"))) {
          putHoldOnDependentLine = true;
        }
      }
      YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
      YFCElement eleOrder = inputDoc.getDocumentElement();
      YFCElement eleOrderLines = eleOrder.getChildElement(GCConstants.ORDER_LINES);
      YFCNodeList<YFCElement> nlOrderLine = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
      for (YFCElement eleOrderLine : nlOrderLine) {
        String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
        String sDependentOnLineKey = eleOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
        if (!YFCCommon.isStringVoid(sDependentOnLineKey) && splitOrderLineKeyList.contains(sDependentOnLineKey)
            && !splitOrderLineKeyList.contains(sOrderLineKey)) {
          eleSplitOrderLines.importNode(eleOrderLine);
          putHoldOnDependentLine = true;
        }
      }

      if (putHoldOnDependentLine) {
        for (YFCElement eleSplitOrderLine : nlSplitOrderLine) {
          String sDependentOnLineKey = eleSplitOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
          if (!YFCCommon.isVoid(sDependentOnLineKey) && ordLineKeyPrimeLineNoMap.containsKey(sDependentOnLineKey)) {
            YFCElement eDependency = eleSplitOrderLine.createChild(GCConstants.DEPENDENCY);
            eDependency.setAttribute(GCConstants.DEPENDENT_PRIME_LINE_NO,
                ordLineKeyPrimeLineNoMap.get(sDependentOnLineKey));
            YFCElement eleOrderHoldTypes = eleSplitOrderLine.createChild(GCConstants.ORDER_HOLD_TYPES);
            YFCElement eleOrderHoldType = eleOrderHoldTypes.createChild(GCConstants.ORDER_HOLD_TYPE);
            eleOrderHoldType.setAttribute(GCConstants.HOLD_TYPE, "DEPENDENT_LINE_HOLD");
            eleOrderHoldType.setAttribute(GCConstants.STATUS, "1100");
            if (LOGGER.isVerboseEnabled()) {
              LOGGER.verbose("OrderLine which has Dependency :\n" + eleSplitOrderLine.toString());
            }
          }
        }
      }
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Updated document is" + splitOrderDoc.toString());
      }
    } catch (Exception e) {
      LOGGER.debug(" Inside catch of GCProcessBackordersAPI.addDependentLineForSplit()", e.getMessage());
      YFCException yfce = new YFCException(GCErrorConstants.ERROR_ON_SCHEDULING);
      throw yfce;
    }
    LOGGER.endTimer(" Exiting GCProcessBackordersAPI.addDependentLineForSplit() method ");
    return splitOrderDoc;
  }

  /**
   * This method unschedule the eligible order lines.
   *
   * @param env
   * @param inDoc
   * @return
   */

  public void unscheduleEligibleLines(YFSEnvironment env, String orderHeaderKey) {
    LOGGER.beginTimer(" Entering GCProcessBackordersAPI.unscheduleEligibleLines() method ");
    try {
      List<String> orderLineKeyForUnschedule = new ArrayList<String>();
      YFCDocument orderListTempDoc =
          YFCDocument.getDocumentFor("<OrderList><Order OrderHeaderKey=''>"
              + "<OrderLines><OrderLine OrderLineKey='' MaxLineStatus=''/>" + "</OrderLines></Order></OrderList>");
      YFCDocument getOrderList = GCCommonUtil.invokeGetOrderListAPI(env, orderHeaderKey, orderListTempDoc);
      YFCElement eleOrderList = getOrderList.getDocumentElement();
      YFCElement eleOrder = eleOrderList.getChildElement(GCConstants.ORDER);
      YFCElement eleOrderLines = eleOrder.getChildElement(GCConstants.ORDER_LINES);
      YFCNodeList<YFCElement> nlOrderLine = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
      for (YFCElement eleOrderLine : nlOrderLine) {
        String sMaxLineStatus = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_MAX_STATUS);
        if (YFCCommon.equalsIgnoreCase(sMaxLineStatus, GCConstants.STATUS_SCHEDULED)) {
          String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
          orderLineKeyForUnschedule.add(sOrderLineKey);
        }
      }
      YFCDocument unscheduleOrderDoc = YFCDocument.createDocument("UnScheduleOrder");
      YFCElement eleUnScheduleOrder = unscheduleOrderDoc.getDocumentElement();
      eleUnScheduleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
      YFCElement eleUnscheduleOrderLines = eleUnScheduleOrder.createChild(GCConstants.ORDER_LINES);
      for (String sOLKey : orderLineKeyForUnschedule) {
        YFCElement eleUnscheduleOrderLine = eleUnscheduleOrderLines.createChild(GCConstants.ORDER_LINE);
        eleUnscheduleOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, sOLKey);
      }
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("\n Unschedule Order Doc is" + unscheduleOrderDoc.toString());
      }
      if (!orderLineKeyForUnschedule.isEmpty()) {
        GCCommonUtil.invokeAPI(env, GCConstants.UNSCHEDULE_ORDER, unscheduleOrderDoc, null);
      }
    } catch (Exception e) {
      LOGGER.debug(" Inside catch of GCProcessBackordersAPI.unscheduleEligibleLines()", e.getMessage());
      YFCException yfce = new YFCException(GCErrorConstants.ERROR_ON_SCHEDULING);
      throw yfce;
    }
    LOGGER.endTimer(" Exiting GCProcessBackordersAPI.unscheduleEligibleLines() method ");
  }

  /**
   * This Method Prepare Map for OrderLineKey And PrimeLineNo.
   *
   * @param ordLineKeyPrimeLineNoMap
   * @param nlInDocOrdLine
   * @return
   */
  private Map<String, String> prepareMapForOrderLineKeyNPrimeLineNo(Map<String, String> ordLineKeyPrimeLineNoMap,
      YFCNodeList<YFCElement> nlInDocOrdLine) {
    try {
      for (YFCElement inDocOrdLine : nlInDocOrdLine) {
        ordLineKeyPrimeLineNoMap.put(inDocOrdLine.getAttribute(GCConstants.ORDER_LINE_KEY),
            inDocOrdLine.getAttribute(GCConstants.PRIME_LINE_NO));
      }
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("\n ordLineKeyPrimeLineNoMap" + ordLineKeyPrimeLineNoMap.toString());
      }
    } catch (Exception e) {
      LOGGER.debug(" Inside catch of GCProcessBackordersAPI.prepareMapForOrderLineKeyNPrimeLineNo()", e.getMessage());
      YFCException yfce = new YFCException(GCErrorConstants.ERROR_ON_SCHEDULING);
      throw yfce;
    }
    return ordLineKeyPrimeLineNoMap;
  }

  @Override
  public void setProperties(Properties paramProperties) throws GCException {
    prop = paramProperties;
  }

}
