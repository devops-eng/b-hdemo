/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Contains the logic for looking at backordered lines and apply DAX allocation hold.
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               13/05/2014        Singh, Gurpreet            Order Schedule and Release
             1.1               29/05/2014        Soni, Karan                Made changes as per new design
             1.2               09/03/2015        Sinha, Rajiv               GCSTORE-319/ 329 changes for sourcing.
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.api.order.GCProcessATGOrders;
import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCOrderUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:gurpreet.singh@expicient.com">Gurpreet</a>
 */
public class GCProcessBackordersAPIOld implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCProcessBackordersAPIOld.class);

  /**
   *
   * Description of processBackOrders Contains the logic for looking at
   * backordered lines and apply DAX allocation hold. And move the OrderLine
   * to ScheduledPO status for future inventory
   *
   * @param env
   * @param inDoc
   * @return
   * @throws GCException
   *
   */
  public Document processBackOrders(YFSEnvironment env, Document inDoc)
      throws GCException {
    LOGGER.debug(" Entering GCProcessBackordersAPI.processBackOrders() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" processBackOrders() method, Input document is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    try {
      Element eleRootInDoc = inDoc.getDocumentElement();
      String sOrderHeaderKey = eleRootInDoc
          .getAttribute(GCConstants.ORDER_HEADER_KEY);
      // GCPhase2: Begin===========>

      boolean rescheduleOrder = splitOrderOnBackorder(env, inDoc);
      // GCPhase2: End===========>
      // else change order status and send order details to DAX
      // Preparing change order status document
      if (!rescheduleOrder) {
        // commenting phase-1 method and using phase-2 method to move backordered qty to DAX
        // allocation.
        processOrdersOnScheduled(env, inDoc, sOrderHeaderKey);
      }

    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCProcessBackordersAPI.processBackOrders() ",
          e);
      throw new GCException(e);
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" processBackOrders() method, Input document is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    LOGGER.debug(" Exiting GCProcessBackordersAPI.processBackOrders() method ");
    return inDoc;
  }


  /**
   * This method splits orders if there is any backorder. It works as: if Order Line is for regular
   * item and partially/ completely backordered then consider for split (if partial) and reschedule
   * if Order Line is for regular item and completely schedule then no need to consider for split or
   * reschedule if Order Line is for Component item and partially/ completely backordered then
   * consider its bundle for split and reschedule and remove the component line
   *
   * @param env
   * @param inDoc
   * @param rescheduleOrder
   * @return
   */
  private boolean splitOrderOnBackorder(YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("GCProcessBackordersAPI.splitOrderOnBackorder");
    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    YFCDocument newInputDoc = YFCDocument.createDocument(GCConstants.ORDER);
    boolean rescheduleOrderForPIS = false;
    boolean isBackorderForStoreItem = false;

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("\n inputDoc to splitOrderOnBackorder\n" + yfcInDoc.toString());
    }
    YFCElement orderEle = yfcInDoc.getDocumentElement();
    String minOrderStatus = orderEle.getAttribute("MinOrderStatus");
    if(YFCCommon.equals(minOrderStatus, GCConstants.STATUS_SCHEDULED, false)){
      return false;
    }

    String orderHeaderKey = orderEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
    String orgCode = orderEle.getAttribute(GCConstants.ENTERPRISE_CODE);

    YFCElement newOrderEle = newInputDoc.getDocumentElement();
    newOrderEle.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
    newOrderEle.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
    newOrderEle.setAttribute(GCConstants.ENTERPRISE_CODE, orgCode);

    Map<String, String> oLKeyCompRatio = new HashMap<String, String>();
    YFCElement orderLinesEle = orderEle.getChildElement(GCConstants.ORDER_LINES);
    YFCElement newOrderLinesEle = newOrderEle.createChild(GCConstants.ORDER_LINES);

    YFCNodeList<YFCElement> orderLineNList = orderLinesEle.getElementsByTagName(GCConstants.ORDER_LINE);
    for(YFCElement orderLineEle:orderLineNList ){
      String maxLineStatus = orderLineEle.getAttribute("MaxLineStatus");
      String minLineStatus = orderLineEle.getAttribute("MinLineStatus");


      if(YFCCommon.equals(maxLineStatus, GCConstants.STATUS_CREATED, false)){
        // This condition when it is Bundle Line. Do not do anything to bundle
        // if MaxLineStatus is Created then it is Bundle item. Do not remove this, till we get
        // confirmation from its component that it has not be backordered
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("Found Bundle Line Line element =\n" + orderLineEle.toString());
          LOGGER.verbose("Keeping Bundle on hold moving to the next line");
        }
      } else if (minLineStatus.indexOf(GCConstants.STATUS_BACKORDERED) >= 0) {
        // send only partial or complete backorder
        boolean tempRescheduleOrder =
            filterOrderLineForNonBundleLines(env, newOrderLinesEle, orderLineEle, oLKeyCompRatio, orgCode);
        rescheduleOrderForPIS = rescheduleOrderForPIS || tempRescheduleOrder;

        boolean tempIsBackorderForStoreItem = isStoreItemNonBackorderable(env, orderLineEle);
        isBackorderForStoreItem = isBackorderForStoreItem || tempIsBackorderForStoreItem;
      }

      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("new document after processing regular item is =\n" + newInputDoc.toString());
      }
    }

    boolean tempRescheduleOrder =
        filterOrderLineForBundleLines(env, newOrderLinesEle, orderLinesEle, oLKeyCompRatio, orgCode);
    rescheduleOrderForPIS = rescheduleOrderForPIS || tempRescheduleOrder;

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Calling ChangeOrder to split backordered lines with document =\n" + newInputDoc.toString());
    }

    // call service to split the order with newInputDoc as input to the service. it has only
    // OrderHeaderKey, Override="Y", EnterpriseCode and only respective lines
    YFCNodeList<YFCElement> newOrderLineNList = newOrderLinesEle.getElementsByTagName(GCConstants.ORDER_LINE);
    if (newOrderLineNList.getLength() > 0) {
      // call order split service
      LOGGER.verbose("Call splitOrderLines method of GCProcessATGOrders for OrderLine Splitting if required");
      GCProcessATGOrders gcProcessATGOrdObj = new GCProcessATGOrders();
      Document splitOrderLinesOp = gcProcessATGOrdObj.splitOrderLines(env, newInputDoc.getDocument());
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("\n splitted OrderLinesOp" + YFCDocument.getDocumentFor(splitOrderLinesOp).toString());
      }
      YFCElement splitOrderEle = YFCDocument.getDocumentFor(splitOrderLinesOp).getDocumentElement();
      YFCElement splitOrderLinesEle = splitOrderEle.getChildElement(GCConstants.ORDER_LINES);
      YFCNodeList<YFCElement> splitOrderLineNList = splitOrderLinesEle.getElementsByTagName(GCConstants.ORDER_LINE);

      for (YFCElement splitOrderLineEle : splitOrderLineNList) {
        String compositionRatioAttr = splitOrderLineEle.getAttribute("CompositionRatio");
        // Fix for Schedule
        if (!YFCCommon.equals(GCConstants.CREATE, splitOrderLineEle.getAttribute(GCConstants.ACTION))) {
          splitOrderLineEle.setAttribute("IsScheduledOrderLine", "Y");
        }

        if (!YFCCommon.isVoid(compositionRatioAttr)) {
          splitOrderLineEle.removeAttribute("CompositionRatio");
        }
      }
      GCCommonUtil.invokeService(env, GCConstants.GC_RESERVE_INVENTORY_SERVICE, splitOrderLinesOp);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("\n split OrderLinesOp futher split based on reservation"
            + YFCDocument.getDocumentFor(splitOrderLinesOp).toString());
      }
      splitOrderLinesOp = gcProcessATGOrdObj.splitOrderLines(env, splitOrderLinesOp);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("\n after reservation and split of orderline new document is\n"
            + YFCDocument.getDocumentFor(splitOrderLinesOp).toString());
      }

      callChangeOrderApi(env, splitOrderLinesOp);
    }
    LOGGER.endTimer("GCProcessBackordersAPI.splitOrderOnBackorder");
    // Do not send to DAX if there is reschedule due to PIS or split due to partial backorder
    // If there is any reschedule due to PIS backorder, raise alert.
    if (rescheduleOrderForPIS) {
      LOGGER
      .verbose("Going to raise alert. Because Pick up in store has been backordered, moving the ordered to Ship to store");
      GCCommonUtil.invokeService(env, "GCRaiseAlertForShipToStoreService", inDoc);
    }
    // If there is backorder for non-backorderable item such as Serial/ Unique/ Store Clearance,
    // raise Exception
    if (isBackorderForStoreItem) {
      LOGGER.verbose("Going to raise alert. Because non-backorderable item has been backordered");
      GCCommonUtil.invokeService(env, "GCRaiseAlertForItemsBackorderedService", inDoc);
    }
    return rescheduleOrderForPIS || newOrderLineNList.getLength() > 0;
  }

  /**
   * This method checks the Order Line and see if it has any non-backorderable item with serial/
   * unique/ store clearance request
   *
   * @param env
   * @param orderLineEle
   * @return
   */
  private boolean isStoreItemNonBackorderable(YFSEnvironment env, YFCElement orderLineEle) {
    YFCElement orderExtnEle = orderLineEle.getChildElement("Extn");
    if (!YFCCommon.isVoid(orderExtnEle)
        && YFCCommon.equals(orderExtnEle.getAttribute("ExtnIsStoreClearance"), GCConstants.YES, false)) {
      return true;
    }

    YFCElement orderLineInvAttReqEle = orderLineEle.getChildElement("OrderLineInvAttRequest");
    if (!YFCCommon.isVoid(orderLineInvAttReqEle) && !YFCCommon.isVoid(orderLineInvAttReqEle.getAttribute("BatchNo"))) {
      return true;
    }

    YFCElement itemDetailsEle = orderLineEle.getChildElement("ItemDetails");
    if (!YFCCommon.isVoid(itemDetailsEle)) {
      YFCElement itemExtnEle = itemDetailsEle.getChildElement("Extn");
      if (!YFCCommon.isVoid(itemExtnEle)
          && YFCCommon.equals(itemExtnEle.getAttribute("ExtnIsUsedOrVintageItem"), GCConstants.YES, false)) {
        return true;
      }
    }
    return false;
  }


  /**
   * This Method calls changeOrder API for OrderLines Which are Splitted and first Bundle Component
   * OrderLine
   *
   * @param env
   * @param splitOrderLinesOp
   */
  private void callChangeOrderApi(YFSEnvironment env, Document splitOrderLinesOp) {
    LOGGER.beginTimer("GCSplitOrderLineService.callChangeOrderApi");
    LOGGER.verbose("Class: GCSplitOrderLineService Method: callChangeOrderApi ::BEGIN");
    YFCDocument changeOrdIp = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement orderEleForChangeOrder = changeOrdIp.getDocumentElement();
    orderEleForChangeOrder.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
    orderEleForChangeOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, YFCDocument.getDocumentFor(splitOrderLinesOp)
        .getDocumentElement().getAttribute(GCConstants.ORDER_HEADER_KEY));
    orderEleForChangeOrder.setAttribute(GCConstants.ENTERPRISE_CODE, YFCDocument.getDocumentFor(splitOrderLinesOp)
        .getDocumentElement().getAttribute(GCConstants.ENTERPRISE_CODE));
    YFCElement ordLinesForChangeOrder = orderEleForChangeOrder.createChild(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> nlOrdLineAfterSplitCall =
        YFCDocument.getDocumentFor(splitOrderLinesOp).getDocumentElement()
        .getChildElement(GCConstants.ORDER_LINES, true).getElementsByTagName(GCConstants.ORDER_LINE);
    LOGGER.verbose("find OrderLine which are splitted");
    for (YFCElement eOrderLine : nlOrdLineAfterSplitCall) {
      if (YFCCommon.isVoid(eOrderLine.getAttribute(GCConstants.PRIME_LINE_NO))) {
        LOGGER.verbose("OrderLine with PrimeLineNo as Blank is splitted OrderLine");
        YFCElement splittedOrdLineClone = (YFCElement) eOrderLine.cloneNode(true);
        YFCElement bundleParentLine = splittedOrdLineClone.getChildElement(GCConstants.BUNDLE_PARENT_LINE);
        LOGGER.verbose("OrderLine with BundleParentLine Element are component OrderLine of Parent Bundle");
        if (YFCCommon.equals(GCConstants.CREATE, splittedOrdLineClone.getAttribute(GCConstants.ACTION))
            && !YFCCommon.isVoid(bundleParentLine)) {
          LOGGER.verbose("first splitted Bundle Component OrderLine is included in ChangeOrder API call");
          continue;
        }

        removeAttNElementsForChangeOrderCall(splittedOrdLineClone);
        ordLinesForChangeOrder.importNode(splittedOrdLineClone);
      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("\n changeOrdIp" + changeOrdIp.toString());
    }
    env.setTxnObject("Publish", "N");
    Document docChangeOrderOP = GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, changeOrdIp.getDocument());
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("\n docChangeOrderOP output Doc" + YFCDocument.getDocumentFor(docChangeOrderOP).toString());
    }
    LOGGER.verbose("GCSplitOrderLineService.callChangeOrderApi method :: END");
    LOGGER.endTimer("GCSplitOrderLineService.callChangeOrderApi");
  }

  /**
   * This method removes attributes and Elements from OrderLine for ChangeOrder API Call.
   *
   * @param cloneOrderLine
   * @param cloneItem
   */
  private void removeAttNElementsForChangeOrderCall(YFCElement cloneOrderLine) {
    LOGGER.beginTimer("GCSplitOrderLineService.removeAttNElementsForChangeOrderCall");
    LOGGER.verbose("Class: GCSplitOrderLineService Method: removeAttNElementsForChangeOrderCall ::BEGIN");
    cloneOrderLine.removeAttribute(GCXmlLiterals.ALLOCATION_DATE);
    cloneOrderLine.removeAttribute(GCXmlLiterals.REMAINING_QTY);
    cloneOrderLine.removeAttribute(GCConstants.STATUS);
    cloneOrderLine.removeAttribute(GCXmlLiterals.MAX_LINE_STATUS);
    cloneOrderLine.removeAttribute(GCXmlLiterals.MAX_LINE_STATUS_DESC);
    cloneOrderLine.removeAttribute(GCXmlLiterals.MIN_LINE_STATUS);
    cloneOrderLine.removeAttribute(GCXmlLiterals.MIN_LINE_STATUS_DESC);
    cloneOrderLine.removeAttribute(GCXmlLiterals.OPEN_QTY);
    cloneOrderLine.removeAttribute(GCXmlLiterals.OTHER_CHARGES);
    cloneOrderLine.removeAttribute(GCXmlLiterals.ORG_ORD_QTY);
    cloneOrderLine.removeAttribute(GCConstants.STATUS_QUANTITY);
    cloneOrderLine.removeAttribute(GCXmlLiterals.INVOICE_QTY);
    cloneOrderLine.removeAttribute(GCXmlLiterals.LINE_SEQ_NO);
    // Handling Bundle Items
    YFCElement cloneItem = cloneOrderLine.getChildElement(GCConstants.ITEM);
    if (!YFCCommon.isVoid(cloneItem)
        && YFCCommon.equals(GCConstants.BUNDLE, cloneOrderLine.getAttribute(GCConstants.KIT_CODE))
        && !YFCCommon.isVoid(GCXmlLiterals.BUNDLE_FULLFILLMENT_MODE)) {
      cloneItem.removeAttribute(GCXmlLiterals.BUNDLE_FULLFILLMENT_MODE);
    }
    YFCElement cloneLineTaxes = cloneOrderLine.getChildElement(GCConstants.LINE_TAXES);
    if (!YFCCommon.isVoid(cloneLineTaxes)) {
      YFCElement sTaxSummary = cloneLineTaxes.getChildElement(GCXmlLiterals.TAX_SUMMARY);
      if (!YFCCommon.isVoid(sTaxSummary)) {
        cloneLineTaxes.removeChild(sTaxSummary);
      }
      YFCNodeList<YFCElement> nlLineTax = cloneLineTaxes.getElementsByTagName(GCConstants.LINE_TAX);
      for (int i = 0; i < nlLineTax.getLength(); i++) {
        YFCElement lineTax = nlLineTax.item(i);
        lineTax.removeAttribute(GCXmlLiterals.INVOICE_TAX);
        lineTax.removeAttribute(GCXmlLiterals.REF_1);
        lineTax.removeAttribute(GCXmlLiterals.REF_2);
        lineTax.removeAttribute(GCXmlLiterals.REF_3);
        lineTax.removeAttribute(GCXmlLiterals.REMAINING_TAX);
        lineTax.removeAttribute(GCXmlLiterals.TAX_PERCENTAGE);
      }
    }
    YFCElement cloneLineCharges = cloneOrderLine.getChildElement(GCConstants.LINE_CHARGES);
    if (!YFCCommon.isVoid(cloneLineCharges)) {
      YFCNodeList<YFCElement> nlLineCharge = cloneLineCharges.getElementsByTagName(GCConstants.LINE_CHARGE);
      for (int j = 0; j < nlLineCharge.getLength(); j++) {
        YFCElement lineCharge = nlLineCharge.item(j);
        lineCharge.removeAttribute(GCConstants.CHARGE_AMOUNT);
        lineCharge.removeAttribute(GCXmlLiterals.INVOICE_CHRGE_AMOUNT);
        lineCharge.removeAttribute(GCXmlLiterals.INVOICE_CHRGE_PER_LINE);
        lineCharge.removeAttribute(GCXmlLiterals.INVOICE_CHRGE_PER_UNIT);
        lineCharge.removeAttribute(GCXmlLiterals.REMAINING_CHARGE_AMOUNT);
        lineCharge.removeAttribute(GCXmlLiterals.REMAINING_CHARGE_PER_LINE);
        lineCharge.removeAttribute(GCXmlLiterals.REMAINING_CHARGE_PER_UNIT);
        lineCharge.removeAttribute(GCConstants.REFERENCE);
      }
    }
    YFCElement clonePersonInfoShipTo = cloneOrderLine.getChildElement(GCConstants.PERSON_INFO_SHIP_TO);
    if (!YFCCommon.isVoid(clonePersonInfoShipTo)) {
      cloneOrderLine.removeChild(clonePersonInfoShipTo);
    }
    YFCElement cloneOrderlineSchedules = cloneOrderLine.getChildElement(GCConstants.SCHEDULES);
    if (!YFCCommon.isVoid(cloneOrderlineSchedules)) {
      cloneOrderLine.removeChild(cloneOrderlineSchedules);
    }
    YFCElement cloneOrderlineStatuses = cloneOrderLine.getChildElement(GCConstants.ORDER_STATUSES);
    if (!YFCCommon.isVoid(cloneOrderlineStatuses)) {
      cloneOrderLine.removeChild(cloneOrderlineStatuses);
    }
    YFCElement cloneParentOrdLineRelationships =
        cloneOrderLine.getChildElement(GCXmlLiterals.PARENT_ORD_LINE_RELATIONSHIP);
    if (!YFCCommon.isVoid(cloneParentOrdLineRelationships)) {
      cloneOrderLine.removeChild(cloneParentOrdLineRelationships);
    }
    YFCElement cloneLineOverallTotals = cloneOrderLine.getChildElement(GCXmlLiterals.LINE_OVERALL_TOTALS);
    if (!YFCCommon.isVoid(cloneLineOverallTotals)) {
      cloneOrderLine.removeChild(cloneLineOverallTotals);
    }
    YFCElement cloneLineRemainingTotals = cloneOrderLine.getChildElement(GCXmlLiterals.LINE_REMAINING_TOTALS);
    if (!YFCCommon.isVoid(cloneLineRemainingTotals)) {
      cloneOrderLine.removeChild(cloneLineRemainingTotals);
    }
    YFCElement cloneLineInvoicedTotals = cloneOrderLine.getChildElement(GCXmlLiterals.LINE_INVOICE_TOTALS);
    if (!YFCCommon.isVoid(cloneLineInvoicedTotals)) {
      cloneOrderLine.removeChild(cloneLineInvoicedTotals);
    }
    YFCElement cloneAdditionalAddresses = cloneOrderLine.getChildElement(GCXmlLiterals.ADDITIONAL_ADDRESSES);
    if (!YFCCommon.isVoid(cloneAdditionalAddresses)) {
      cloneOrderLine.removeChild(cloneAdditionalAddresses);
    }
    YFCElement cloneKitLines = cloneOrderLine.getChildElement(GCXmlLiterals.KIT_LINES);
    if (!YFCCommon.isVoid(cloneKitLines)) {
      cloneOrderLine.removeChild(cloneKitLines);
    }
    YFCElement cloneReferences = cloneOrderLine.getChildElement(GCConstants.REFERENCES);
    if (!YFCCommon.isVoid(cloneReferences)) {
      cloneOrderLine.removeChild(cloneReferences);
    }
    YFCElement cloneInstructions = cloneOrderLine.getChildElement(GCXmlLiterals.INSTRUCTIONS);
    if (!YFCCommon.isVoid(cloneInstructions)) {
      cloneOrderLine.removeChild(cloneInstructions);
    }
    YFCElement cloneOrderLineReservations = cloneOrderLine.getChildElement(GCXmlLiterals.ORD_LINE_RESERVATIONS);
    if (!YFCCommon.isVoid(cloneOrderLineReservations) && cloneOrderLineReservations.hasChildNodes()) {
      cloneOrderLineReservations.setAttribute(GCConstants.RESET, GCConstants.YES);
    }
	YFCElement orderLineSourcingControls = cloneOrderLine.getChildElement("OrderLineSourcingControls");
    if (!YFCCommon.isVoid(orderLineSourcingControls)) {
		YFCNodeList<YFCElement> nlOrderLineSourcingCntrl = orderLineSourcingControls
				.getElementsByTagName("OrderLineSourcingCntrl");
		for (YFCElement orderLineSourcingCntrl : nlOrderLineSourcingCntrl) {
			orderLineSourcingCntrl.removeAttribute("OrderLineKey");
			orderLineSourcingCntrl.removeAttribute("OrderLineSourcingCntrlKey");
		}
	}
    LOGGER.verbose("GCSplitOrderLineService.removeAttFromOrderLineAndItemEle method :: END");
    LOGGER.endTimer("GCSplitOrderLineService.removeAttFromOrderLineAndItemEle");
  }


  /**
   * This method process orderline element for Non bundle lines. i.e. Regular and component lines.
   *
   * @param orderLinesEle
   * @param orderLineEle
   * @param oLKeyCompRatio
   * @param orgCode
   */
  private boolean filterOrderLineForNonBundleLines(YFSEnvironment env, YFCElement newOrderLinesEle,
      YFCElement orderLineEle, Map<String, String> oLKeyCompRatio, String orgCode) {

    LOGGER.beginTimer("GCProcessBackordersAPI.filterOrderLineForNonBundleLines");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Found Regular or Component Line Line element with partial or complete backorder"
          + orderLineEle.toString());
    }
    boolean rescheduleOrderForPIS = false;
    String compositionRatio = "";
    YFCElement orderStatusesEle = orderLineEle.getChildElement("OrderStatuses");
    YFCNodeList<YFCElement> orderStatusesNList = orderStatusesEle.getElementsByTagName("OrderStatus");
    int backOrderedQty = 0;
    int scheduledQty = 0;
    for (YFCElement orderStatusEle : orderStatusesNList) {
      String status = orderStatusEle.getAttribute("Status");
      String statusQty = orderStatusEle.getAttribute("StatusQty");

      if (status.indexOf(GCConstants.STATUS_BACKORDERED) >= 0 && !YFCCommon.isVoid(statusQty)) {
        backOrderedQty = backOrderedQty + Integer.parseInt(statusQty);
      } else if (YFCCommon.equals(status, "1400", false)) {
        // Do nothing if it is Backorder from Node. This is just an additional status added by OMS
      } else {
        scheduledQty = scheduledQty + Integer.parseInt(statusQty);
      }
    }
    // provide composition ratio as Backordered:Scheduled
    if (backOrderedQty > 0) {
      compositionRatio = backOrderedQty + ":";
    }

    if (scheduledQty > 0) {
      compositionRatio = compositionRatio + scheduledQty;
    }

    String parentOrderLineKey = "";
    YFCElement bundleParentLineEle = orderLineEle.getChildElement("BundleParentLine");
    if (!YFCCommon.isVoid(bundleParentLineEle)) {
      parentOrderLineKey = bundleParentLineEle.getAttribute("OrderLineKey");
      LOGGER.verbose("This is a component line with parentOrderLineKey=" + parentOrderLineKey);
    }

    if (YFCCommon.isVoid(parentOrderLineKey)) {
      // This is regular item. Now go for split and reschedule of this line only if it was PIS
      // backordered or partial backorded (neither full or zero backorder)
      YFCElement newOrderLineEle = (YFCElement) orderLineEle.cloneNode(true);
      newOrderLineEle.setAttribute("CompositionRatio", compositionRatio);
      String isFirmPredefinedNode = newOrderLineEle.getAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE);
      String fulfillmentType = newOrderLineEle.getAttribute(GCConstants.FULFILLMENT_TYPE);
      String shipNode = newOrderLineEle.getAttribute(GCConstants.SHIP_NODE);
      if (fulfillmentType.indexOf(GCConstants.PICKUP_IN_STORE) >= 0
          && !canIgnoreIsFirmPredefinedNode(env, newOrderLineEle, orgCode)
          && YFCCommon.equals(isFirmPredefinedNode, GCConstants.YES, false) && !YFCCommon.isVoid(shipNode)) {
        newOrderLineEle.setAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE, "N");
        newOrderLineEle.setAttribute(GCConstants.SHIP_NODE, "");
        // rescheduling is always required in PIS if there is some backordered qty
        rescheduleOrderForPIS = true;
      }
      int indexOfDelimiter = compositionRatio.indexOf(':');
      if (indexOfDelimiter > 0 && indexOfDelimiter < compositionRatio.length() - 1) {
        newOrderLinesEle.importNode(newOrderLineEle);
      }
      LOGGER.verbose("Consider the line with Setting CompositionRatio for Regular line as: " + compositionRatio);
    } else if (!oLKeyCompRatio.containsKey(parentOrderLineKey)) {
      oLKeyCompRatio.put(parentOrderLineKey, compositionRatio);
      LOGGER.verbose("do not consider the component line after storing the ParentLineKey=" + parentOrderLineKey
          + " and Composition Ratio=" + compositionRatio);
    } else {
      LOGGER
      .verbose("do not consider the component line because parent is already stored with storing the ParentLineKey and Composition Ratio");
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Updated new document for split " + newOrderLinesEle.getOwnerDocument().toString());
    }
    LOGGER.endTimer("GCProcessBackordersAPI.filterOrderLineForNonBundleLines");
    return rescheduleOrderForPIS;
  }

  /**
   * This method set IsFirmPredefinedNode based upon if it is gift/ single source/ warranty item (do
   * not set "N") or not
   *
   * @param env
   * @param newOrderLineEle
   * @param orgCode
   */
  private boolean canIgnoreIsFirmPredefinedNode(YFSEnvironment env, YFCElement newOrderLineEle, String orgCode) {

    LOGGER.beginTimer("GCProcessBackordersAPI.canIgnoreIsFirmPredefinedNode");
    String isFirmPredefinedNode = newOrderLineEle.getAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE);
    String shipNode = newOrderLineEle.getAttribute(GCConstants.SHIP_NODE);
    String isStoreClearance = "";
    YFCElement newOrderLineExtnEle = newOrderLineEle.getChildElement("Extn");
    if (!YFCCommon.isVoid(newOrderLineExtnEle)) {
      isStoreClearance = newOrderLineExtnEle.getAttribute("ExtnIsStoreClearance");
    }
    if (YFCCommon.equals(isFirmPredefinedNode, GCConstants.YES, false) && !YFCCommon.isVoid(shipNode)) {
      // if fulfillment type is pick up in store and if IsFirmPredefinedNode=Y and shipNode is not
      // blank
      // get the itemDetails
      LOGGER.verbose("OrderLine has ShipNode=" + shipNode + " with isFirmPredefinedNode=Y");
      YFCElement itemEle = newOrderLineEle.getElementsByTagName("Item").item(0);
      String itemId = itemEle.getAttribute("ItemID");
      String uom = itemEle.getAttribute("UnitOfMeasure");
      // call getItemlist api
      YFCDocument getItemListOutDoc =
          GCCommonUtil.getItemList(env, itemId, uom, orgCode,
              YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LIST_TEMPLATE_ORDER_PROMISING));
      boolean isSingleSourced = GCOrderUtil.isSingleSourced(getItemListOutDoc);
      boolean isDigital = GCOrderUtil.isDigital(getItemListOutDoc);
      boolean isGiftCard = GCOrderUtil.isGiftCard(getItemListOutDoc);
      boolean isClearanceItem = GCOrderUtil.isClearanceItem(getItemListOutDoc);

      boolean clearanceItemWithoutReq = !YFCCommon.equals(isStoreClearance, GCConstants.YES, false) && isClearanceItem;

      boolean isSerialRequest = false;
      YFCElement orderLineInvAttReq = newOrderLineEle.getChildElement("OrderLineInvAttRequest");
      if (!YFCCommon.isVoid(orderLineInvAttReq)) {
        isSerialRequest = !YFCCommon.isVoid(orderLineInvAttReq.getAttribute("BatchNo"));
      }
      // check if the item is Not SingleSourced and Not Digital and Not GiftCard and Not regular
      // version of store clearance item (to be fulfilled from MFI only)
      if (isSingleSourced || isDigital || isGiftCard || clearanceItemWithoutReq || isSerialRequest) {
        LOGGER.verbose("OrderLine has isSingleSourced=" + isSingleSourced + ", isDigital=" + isDigital
            + ", isGiftCard=" + isGiftCard + ", Normal request for Clearance Item=" + clearanceItemWithoutReq
            + ", isSerialRequest=" + isSerialRequest);
        LOGGER.verbose("Returning true as FirmPreDefined is for unique items/ inventory and can be ignored");
        return true;
      }
    }
    LOGGER.endTimer("GCProcessBackordersAPI.canIgnoreIsFirmPredefinedNode");
    LOGGER.verbose("Returning false as FirmPreDefined is not for unique items/ inventory and can not be ignored");
    return false;
  }

  /**
   * This method iterates through Bundle item and if no partial backorder happens for a bundle. it
   * has been removed from the document Else left in the document to process split
   *
   * @param orderLinesEle
   * @param orderLinesEle2
   * @param oLKeyCompRatio
   * @param orgCode
   */

  private boolean filterOrderLineForBundleLines(YFSEnvironment env, YFCElement newOrderLinesEle,
      YFCElement orderLinesEle,
      Map<String, String> oLKeyCompRatio, String orgCode) {
    // Iterating through OrderLine again to retain the bundle which are there in the map (orderline
    // key and composition ratio) and removing the rest
    LOGGER.beginTimer("GCProcessBackordersAPI.filterOrderLineForBundleLines");
    boolean rescheduleOrderForPIS = false;
    YFCNodeList<YFCElement> orderLineNList = orderLinesEle.getElementsByTagName(GCConstants.ORDER_LINE);
    for (YFCElement orderLineEle : orderLineNList) {
      String maxLineStatus = orderLineEle.getAttribute("MaxLineStatus");
      String compositionRatio = "";
      String orderLineKey = orderLineEle.getAttribute("OrderLineKey");
      if (YFCCommon.equals(maxLineStatus, GCConstants.STATUS_CREATED, false)) {
        compositionRatio = oLKeyCompRatio.get(orderLineKey);
        if (!YFCCommon.isVoid(compositionRatio)) {
          LOGGER.verbose("Found the composition ratio as " + compositionRatio);
          String statusQty = orderLineEle.getAttribute("StatusQuantity");
          int indexOfDelimiter = compositionRatio.indexOf(':');
          int bordQty = Integer.parseInt(compositionRatio.substring(0, indexOfDelimiter));
          int schQty = 0;
          if (compositionRatio.length() > indexOfDelimiter + 1) {
            schQty = Integer.parseInt(compositionRatio.substring(indexOfDelimiter + 1));
          }
          int statusQtyInt = Integer.parseInt(statusQty);
          int totalQty = bordQty + schQty;
          String updatedCompositionRatio =
              "" + (bordQty * statusQtyInt) / totalQty + ":" + (schQty * statusQtyInt) / totalQty;

          YFCElement newOrderLineEle = (YFCElement) orderLineEle.cloneNode(true);
          newOrderLineEle.setAttribute("CompositionRatio", updatedCompositionRatio);
          String isFirmPredefinedNode = newOrderLineEle.getAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE);
          String fulfillmentType = newOrderLineEle.getAttribute(GCConstants.FULFILLMENT_TYPE);
          String shipNode = newOrderLineEle.getAttribute(GCConstants.SHIP_NODE);
          if (bordQty > 0 && fulfillmentType.indexOf(GCConstants.PICKUP_IN_STORE) >= 0
              && !canIgnoreIsFirmPredefinedNode(env, newOrderLineEle, orgCode)
              && YFCCommon.equals(isFirmPredefinedNode, GCConstants.YES, false) && !YFCCommon.isVoid(shipNode)) {
            newOrderLineEle.setAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE, "N");
            newOrderLineEle.setAttribute(GCConstants.SHIP_NODE, "");
            // Reschedule is always required in case of PIS and atleast some qty is backordered
            rescheduleOrderForPIS = true;
          }

          if (bordQty>0&&schQty>0) {
            // go for split if it is partial scenario
            newOrderLinesEle.importNode(newOrderLineEle);
          }

          LOGGER.verbose("stamped updated composition ratio as " + updatedCompositionRatio);

        }
        oLKeyCompRatio.remove(orderLineKey);
      } else {
        LOGGER.verbose("Do not consider Order Line of Bundle for which there is no entry in the map");
      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("new document after processing bundle item document is =\n"
          + newOrderLinesEle.getOwnerDocument().toString());
    }
    LOGGER.endTimer("GCProcessBackordersAPI.filterOrderLineForBundleLines");
    return rescheduleOrderForPIS;
  }


  /**
   * This method change the order line status for DAX allocation. only those lines which are
   * backordered move to DAX allocation
   *
   * @param env
   * @param inDoc
   * @param sOrderHeaderKey
   */

  public void changeOrderStatusForDAXAllocation(YFSEnvironment env, Document inDoc, String sOrderHeaderKey) {

    LOGGER.beginTimer("GCProcessBackordersAPI.changeOrderStatusForDAXAllocation");

    YFCDocument docChangeOrderStatus = YFCDocument.createDocument(GCConstants.ORDER_STATUS_CHANGE);

    YFCElement eleChangeOrderStatus = docChangeOrderStatus.getDocumentElement();
    eleChangeOrderStatus.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    eleChangeOrderStatus.setAttribute(GCConstants.IGNORE_TRANSACTION_DEPENDENCIES, GCConstants.YES);
    eleChangeOrderStatus.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.CHANGE_SCHEDULE_STATUS);

    YFCElement eleOrderLinesChngStatus = eleChangeOrderStatus.createChild(GCConstants.ORDER_LINES);

    YFCDocument orderDoc = YFCDocument.getDocumentFor(inDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input document to changeOrderStatusForDAXAllocation =\n" + orderDoc.toString());
    }

    YFCElement orderEle = orderDoc.getDocumentElement();
    YFCElement orderLinesEle = orderEle.getChildElement(GCConstants.ORDER_LINES);
    boolean callChangeOrderStatus = false;

    YFCNodeList<YFCElement> orderLineNList = orderLinesEle.getElementsByTagName(GCConstants.ORDER_LINE);
    for (YFCElement orderLineEle : orderLineNList) {
      String maxLineStatus = orderLineEle.getAttribute("MaxLineStatus");
      String minLineStatus = orderLineEle.getAttribute("MinLineStatus");
      if (YFCCommon.equals(maxLineStatus, minLineStatus, true)
          && YFCCommon.equals(minLineStatus, GCConstants.STATUS_BACKORDERED, true)) {
        String strOrderLineKey = orderLineEle.getAttribute(GCConstants.ORDER_LINE_KEY);
        YFCElement eleOrderLineChngStatus = eleOrderLinesChngStatus.createChild(GCConstants.ORDER_LINE);

        eleOrderLineChngStatus.setAttribute(GCConstants.ORDER_LINE_KEY, strOrderLineKey);
        eleOrderLineChngStatus.setAttribute(GCConstants.BASE_DROP_STATUS, GCConstants.SCHEDULED_PO_DAX_STATUS);
        eleOrderLineChngStatus.setAttribute(GCConstants.CHANGE_FOR_ALL_AVAIL_QTY, GCConstants.YES);
        callChangeOrderStatus = true;
      }
    }

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input document to changeOrderStatus API =\n" + docChangeOrderStatus.toString());
    }

    if (callChangeOrderStatus) {
      LOGGER.verbose("Invoking ChangeOrderStatus API");
      GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER_STATUS, docChangeOrderStatus, null);
      LOGGER.verbose("API ChangeOrderStatus invoked Successfully!");
    }
    LOGGER.endTimer("GCProcessBackordersAPI.changeOrderStatusForDAXAllocation");
  }

  /**
   *
   * This method checks the inventory position and moves the quantity into ScheduledPODAX and
   * BackOrderedDax status
   *
   * @param env
   * @param inDoc
   * @param sOrderHeaderKey
   * @throws GCException
   *
   */

  public void processOrdersOnScheduled(YFSEnvironment env, Document inDoc, String sOrderHeaderKey) throws GCException {
    try {
      Document docChangeOrderStatus = GCXMLUtil.createDocument(GCConstants.ORDER_STATUS_CHANGE);
      Element eleChangeOrderStatus = docChangeOrderStatus.getDocumentElement();
      eleChangeOrderStatus.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
      eleChangeOrderStatus.setAttribute(GCConstants.IGNORE_TRANSACTION_DEPENDENCIES, GCConstants.YES);
      eleChangeOrderStatus.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.CHANGE_SCHEDULE_STATUS);
      Element eleOrderLinesChngStatus = GCXMLUtil.createElement(docChangeOrderStatus, GCConstants.ORDER_LINES, null);
      eleChangeOrderStatus.appendChild(eleOrderLinesChngStatus);

      NodeList nlPromiseLine = XPathAPI.selectNodeList(inDoc, "Order/SuggestedOption/Option/PromiseLines/PromiseLine");
      for (int iCounter = 0; iCounter < nlPromiseLine.getLength(); iCounter++) {
        LOGGER.debug("Inside for loop iterating on PromiseLine elements");
        String strFromExpectedShipDate = "";
        Element elePromiseLine = (Element) nlPromiseLine.item(iCounter);
        String strOrderLineKey = elePromiseLine.getAttribute(GCConstants.ORDER_LINE_KEY);
        Calendar calSysDate = Calendar.getInstance();
        NodeList nlAssignment = XPathAPI.selectNodeList(elePromiseLine, "Assignments/Assignment");
        for (int iCount = 0; iCount < nlAssignment.getLength(); iCount++) {
          LOGGER.debug("Inside for loop iterating on Assignment elements");
          Element eleAssignment = (Element) nlAssignment.item(iCount);
          NodeList nlSupply = XPathAPI.selectNodeList(eleAssignment, "Supplies/Supply");
          for (int j = 0; j < nlSupply.getLength(); j++) {
            LOGGER.debug("Inside for loop iterating on Supply elements");
            Element eleSupply = (Element) nlSupply.item(j);
            String strFirstShipDate = eleSupply.getAttribute(GCConstants.FIRST_SHIP_DATE);
            Calendar calRemorseDate1 = Calendar.getInstance();
            Date productAvailabeDate = new SimpleDateFormat(GCConstants.DATE_FORMAT).parse(strFirstShipDate);
            calRemorseDate1.setTime(productAvailabeDate);
            if (calRemorseDate1.getTime().compareTo(calSysDate.getTime()) > 0) {
              strFromExpectedShipDate = strFirstShipDate;
              Calendar calFromExpectedShipDate = Calendar.getInstance();
              Date fromExpectedShipDate = new SimpleDateFormat(
                  GCConstants.DATE_FORMAT)
              .parse(strFirstShipDate);
              calFromExpectedShipDate.setTime(fromExpectedShipDate);

              String strShipDate = eleAssignment.getAttribute(GCConstants.SHIP_DATE);
              Calendar calShipDate = Calendar.getInstance();
              Date shipDate = new SimpleDateFormat(GCConstants.DATE_FORMAT).parse(strShipDate);
              calShipDate.setTime(shipDate);
              // check if ShipDate is after FromExpectedShipDate,
              // this is done to have the maximum date for the
              // FromExpectedShipDate attr, for changeOrderStatus
              // doc IP
              if (calShipDate.getTime().compareTo(calFromExpectedShipDate.getTime()) > 0) {
                strFromExpectedShipDate = strShipDate;
                LOGGER
                .debug("inside if checking FromExpectedShipDate can not be before ShipDate, strFromExpectedShipDate="
                    + strFromExpectedShipDate);
              }

              String sConsumedQuantity = eleSupply.getAttribute(GCConstants.CONSUMED_QUANTITY);
              double dConsumedQuantity =
                  (!YFCCommon.isVoid(sConsumedQuantity)) ? Double.valueOf(sConsumedQuantity) : 0.00;
                  if (dConsumedQuantity > 0.0) {
                    LOGGER.debug("inside if ConsumedQuantity > 0.0");
                    Element eleOrderLineChngStatus =
                        GCXMLUtil.createElement(docChangeOrderStatus, GCConstants.ORDER_LINE, null);
                    eleOrderLineChngStatus.setAttribute(GCConstants.ORDER_LINE_KEY, strOrderLineKey);
                    eleOrderLineChngStatus.setAttribute(GCConstants.BASE_DROP_STATUS, GCConstants.SCHEDULED_PO_DAX_STATUS);
                    eleOrderLineChngStatus.setAttribute(GCConstants.QUANTITY, String.valueOf(dConsumedQuantity));
                    eleOrderLineChngStatus.setAttribute("FromExpectedShipDate", strFromExpectedShipDate);
                    eleOrderLinesChngStatus.appendChild(eleOrderLineChngStatus);
                  }
            }
          }
        }
      }
      Element eleOrderLine =
          GCXMLUtil.getElementByXPath(docChangeOrderStatus, "OrderStatusChange/OrderLines/OrderLine");
      if (!YFCCommon.isVoid(eleOrderLine)) {
        LOGGER.debug("Invoking ChangeOrderStatus API");
        GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER_STATUS, docChangeOrderStatus);
        LOGGER.debug("API ChangeOrderStatus invoked Successfully!");
      }
    } catch (Exception e) {
      LOGGER.error(" Inside catch of  processOrdersOnScheduled()", e);
      throw new GCException(e);
    }
  }


  @Override
  public void setProperties(Properties arg0) throws GCException {

  }

}
