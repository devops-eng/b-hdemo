/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 * *###########################################
 * ######################################################
 * ################################################################################ OBJECTIVE:
 * Contains the logic of processing the shipment confirmation message
 * ###############################
 * ###################################################################
 * ################################################################################# Version Date
 * Modified By Description
 * ##########################################################################
 * ########################
 * ############################################################################## 1.0 15/04/2014
 * Singh, Gurpreet Shipment 1.1 20/01/2015 Soni, Karan OMS:4660 Alert generated in console for
 * shipment notification for OMS order with Ship Alone item 1.2 05/02/2015 Kumar, Rakesh OMS:4668
 * Warranty item status does not change when parent item is shipped for drop ship order 1.3
 * 27/02/2015 Soni, Karan OMS:4932 Alert is getting generated for shipment notification for order
 * having set item in console 1.4 19/03/2015 Ravi, Divya GCSTORE-811 - Phase 2 Order fulfillment
 * design changes 1.5 25/03/2015 Ravi, Divya GCSTORE-1831 - Modification rule exception - fixed
 * using OVerride flag 1.6 02/04/2015 Ravi, Divya GCSTORE-1889 - Fix done for completely short pick
 * order lines 1.7 07/04/2015 Taj, Rashma GCSTORE-1901 - OrderLine is moving to Awaiting Chained PO
 * Status for a dropship Item in case of Partial fulfillment from Drop ship Vendor 1.8 07/04/2015
 * Ravi, Divya GCSTORE-1833- Status of ShortPick Quantity is BackorderedDAX instead of
 * BackorderedOMS in order console 1.9 22/04/2015 Taj, Rashma GCSTORE-2272 - OrderLine is moving to
 * Awaiting Chained PO Status for a dropship Item in case of sending the complete inventory short
 * Pick Message from DRV
 * 06/11/2015        Infosys Limited            OMS-5588  Merge of bundle shipments
 * ############################################################################
 * ######################
 * ################################################################################
 */
package com.gc.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.gc.exstore.api.packing.GCConfirmShipmentAPI;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:gurpreet.singh@expicient.com">Gurpreet</a>
 */
public class GCProcessConfirmShipmentAPI implements YIFCustomApi {

	/*
	 * The class name
	 */
	private static final String CLASS_NAME = GCProcessConfirmShipmentAPI.class.getName();

	// initialize logger
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCProcessConfirmShipmentAPI.class.getName());
	Document docGetCompOrdDtlsIn = null;

	/*
	 * The property to hold the service arguments
	 */
	private static Properties props;

	/**
	 *
	 * <h3>Description:</h3>This method is used to confirm the shipment
	 *
	 * @author Infosys Limited
	 * @param yfsEnv The yfsenvironment reference
	 * @param docInput The input document
	 * @return The input document
	 * @throws GCException
	 */
	public Document shipmentConfirmation(final YFSEnvironment yfsEnv, final Document docInput) throws GCException {

		// log the method entry
		LOGGER.info(CLASS_NAME + ":shipmentConfirmation:entry");

		// log the method input
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":shipmentConfirmation:Input=" + GCXMLUtil.getXMLString(docInput));
		}

		// updated document
		final Document docUpdatedInput;

		// fetch the order related details of the shipment confirmation
		final Map<String, String> mapOrdDtls = getKeyOrderDtls(docInput);

		// check if the flow needs to be disabled
		if (getFlagValue("DISABLE_NO_PREPROCESS")) {

			// no reprocess
			docUpdatedInput = docInput;
		} else {

			// preprocess the shipment confirmation
			docUpdatedInput = preprocessShpConfirmation(yfsEnv, docInput, mapOrdDtls);
		}

		// document post reprocess logic
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":shipmentConfirmation:Post Processing output="
					+ GCXMLUtil.getXMLString(docUpdatedInput));
		}

		//check if the legacy code needs to be invoked
		if (checkIfShpLnsExist(docUpdatedInput)) {

			// invoke the shipment confirmation legacy code
			confShipment(yfsEnv, docUpdatedInput);
		}

		// check if post process is required
		if (!getFlagValue("DISABLE_NO_POSTPROCESS")) {

			//post process of shipment confirmation
			postprocessShpConfirmation(mapOrdDtls, yfsEnv);
		}

		// log the method exit
		LOGGER.info(CLASS_NAME + ":shipmentConfirmation:exit");

		// return the input document
		return docInput;
	}


	/**
	 * <h3>Description:</h3>This method is used to post process the order coressponding to the
	 * shipment confirmation
	 *
	 * @author Infosys Limited
	 * @param mapOrdDtls The key order details
	 * @throws GCException Thrown if the legacy code throws the exception
	 */
	private void postprocessShpConfirmation(final Map<String, String> mapOrdDtls, final YFSEnvironment yfsEnv)
			throws GCException {

		// logging the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":postprocessShpConfirmation:entry:Input=" + mapOrdDtls);
		}

		// fetch the new order details
		final Document docOrderDtls = getCompleteOrderDetails(yfsEnv, mapOrdDtls);

		// fetch the held shipments
		final List listHldShpmnts =
				GCXMLUtil.getElementListByXpath(docOrderDtls, "/Order/Shipments/Shipment[@Status='1100']");

		// check if shipments are on hold
		if (listHldShpmnts.size() > 0) {

			// process the helpd shipment
			processCompleteHeldKits(docOrderDtls, yfsEnv, listHldShpmnts);
		} else {

			LOGGER.debug(CLASS_NAME + ":postprocessShpConfirmation:No shipments in held state to proces");
		}

		// logging the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":postprocessShpConfirmation:exit");
		}
	}


	/**
	 * <h3>Description:</h3>This method is used to prepare the getCompleteOrder details input
	 *
	 * @author Infosys Limited
	 * @param mapOrdDtls The order details
	 * @return The getCompleteOrderDetails api input
	 */
	private Document prepNewGetOrderIn(final Map<String, String> mapOrdDtls) {

		// logging the method entry
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":prepNewGetOrderIn:entry:Input=" + mapOrdDtls);
		}

		// get the document type and enterprise code
		final String strDocType = mapOrdDtls.get(GCConstants.DOCUMENT_TYPE);
		final String strEntprCode = mapOrdDtls.get(GCConstants.ENTERPRISE_CODE);

		// fetch the order no
		final String strOrderNo = mapOrdDtls.get(GCConstants.ORDER_NO);

		// Create the getOrderList api input
		final Document docOrder = GCXMLUtil.createDocument(GCConstants.ORDER);
		final Element eleOrdRt = docOrder.getDocumentElement();
		eleOrdRt.setAttribute(GCConstants.DOCUMENT_TYPE, strDocType);
		eleOrdRt.setAttribute(GCConstants.ENTERPRISE_CODE, strEntprCode);
		eleOrdRt.setAttribute(GCConstants.ORDER_NO, strOrderNo);


		// logging the method exit
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":prepNewGetOrderIn:exit:output=" + GCXMLUtil.getXMLString(docOrder));
		}

		// return the document created
		return docOrder;
	}


	/**
	 * <h3>Description:</h3>This method is used to get the Key order details
	 *
	 * @author Infosys Limited
	 * @param docInput The shipemt confirmation message
	 * @return The key order details
	 */
	private Map<String, String> getKeyOrderDtls(final Document docInput) {

		// logging method entry
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":getKeyOrderDtls:entry:Input=" + GCXMLUtil.getXMLString(docInput));
		}

		// get the document type
		final Element eleShp = docInput.getDocumentElement();
		final String strDocType = eleShp.getAttribute(GCConstants.DOCUMENT_TYPE);
		final String strEntprCode = eleShp.getAttribute(GCConstants.ENTERPRISE_CODE);

		// fetch the order no
		final Element eleShpLn = GCXMLUtil.getElementByXPath(docInput, "/Shipment/ShipmentLines/ShipmentLine");
		final String strOrderNo = eleShpLn.getAttribute(GCConstants.ORDER_NO);

		// add the key order details to a map
		final Map<String, String> mapKeyordDtls = new HashMap<String, String>();
		mapKeyordDtls.put(GCConstants.DOCUMENT_TYPE, strDocType);
		mapKeyordDtls.put(GCConstants.ENTERPRISE_CODE, strEntprCode);
		mapKeyordDtls.put(GCConstants.ORDER_NO, strOrderNo);

		// logging the output
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":getKeyOrderDtls:exit:Output=" + mapKeyordDtls);
		}

		// return the key order details
		return mapKeyordDtls;
	}


	/**
	 * <h3>Description:</h3>This method is used to invoke the legacy flow
	 *
	 * @author Infosys Limited
	 * @param yfsEnv The YFS environment reference
	 * @param docShpConfIn The shipment confirmation message
	 * @throws GCException Thrown if any failure while processing
	 */
	private void confShipment(final YFSEnvironment yfsEnv, final Document docShpConfIn) throws GCException {

		// logging the method entry
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":confShipment:entry:Input=" + GCXMLUtil.getXMLString(docShpConfIn));
		}

		// check if the old flow needs to be executed
		if (getFlagValue("DISABLE_OLD_FLOW")) {

			// invoke the old flow
			confirmShipment(yfsEnv, docShpConfIn);
		} else {

			// invoke the legacy logic to process the shipment
			processShipmentConfirmation(yfsEnv, docShpConfIn);
		}

		// logging the method exit
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":confShipment:exit");
		}
	}


	/**
	 * <h3>Description:</h3>This method is used to check if any shiplines exist in the document
	 *
	 * @author Infosys Limited
	 * @param docUpdatedInput The input to the legacy code
	 * @return true if any shipment line exists in the input else returns false
	 */
	private boolean checkIfShpLnsExist(final Document docUpdatedInput) {

		// logging the input
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":checkIfShpLnsExist:entry:Input=" + GCXMLUtil.getXMLString(docUpdatedInput));
		}

		// get the shipment lines
		final List listShpLns = GCXMLUtil.getElementListByXpath(docUpdatedInput, "/Shipment/ShipmentLines/ShipmentLine");

		// the return flag
		final boolean bShpLnPresent;

		// check if shipment lines present
		if (listShpLns.size() > 0) {

			bShpLnPresent = true;
		} else {

			bShpLnPresent = false;
		}

		// logging the exit
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":checkIfShpLnsExist:exit:output=" + bShpLnPresent);
		}

		// return the flag
		return bShpLnPresent;
	}


	/**
	 * <h3>Description :</h3> This method is used to preprocess the shipment confirmation
	 *
	 * @author Infosys Limited
	 * @param yfsEnv The YFS environment reference
	 * @param docInput The input document
	 * @param mapOrdDtls the key order details
	 * @return The updated input document
	 * @throws GCException Thrown if the complete shipment processing throws exception
	 */
	private Document preprocessShpConfirmation(final YFSEnvironment yfsEnv, final Document docInput,
			final Map<String, String> mapOrdDtls) throws GCException {

		// logging the method input
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":preprocessShpConfirmation:Input=" + GCXMLUtil.getXMLString(docInput));
		}

		// get the Complete Order Details
		final Document docCompleteOrderDtlsOut = getCompleteOrderDetails(yfsEnv, mapOrdDtls);

		//identify the kits that are supposed to be in the shipment confirmation
		final Map<String, Set<String>> mapKitParentDtls =
				getKitParentInShpConfirmationMsg(docInput, docCompleteOrderDtlsOut);

		// check if any kits present in the ship confirmation message
		final Set<String> setKitParent = mapKitParentDtls.keySet();
		if (setKitParent.isEmpty()) {

			LOGGER.debug(CLASS_NAME + ":preprocessShpConfirmation:No kits to preprocess");
		} else {

			//get the document to be moved to held status and manipulate the input document if required
			final Document docHoldShpmnts = getHoldShipments(mapKitParentDtls, docInput, docCompleteOrderDtlsOut);

			// check if any api calls present
			if (isAnyApiCallPresent(docHoldShpmnts)) {

				// check and update if any existing shipment is present
				updateExistingShipments(docHoldShpmnts, docCompleteOrderDtlsOut);

				// logging the service input
				if (LOGGER.isDebugEnabled()) {

					LOGGER.debug(CLASS_NAME + ":preprocessShpConfirmation:Multi Api input="
							+ GCXMLUtil.getXMLString(docHoldShpmnts));
				}

				// invoke the multiAPI to save the shipments to be on hold
				final String strSvcName = props.getProperty("MULTI_API_SVC_NAME", "GCCreateShipmentMultiapi_svc");// TODO
				final Document docMultApiOut = GCCommonUtil.invokeService(yfsEnv, strSvcName, docHoldShpmnts);

				// log the output
				if (LOGGER.isDebugEnabled()) {

					LOGGER.debug(CLASS_NAME + ":preprocessShpConfirmation:Multi Api output="
							+ GCXMLUtil.getXMLString(docMultApiOut));
				}

				// check if the consolidated shipments are ready to be confirmed
				// confirmCompletedShipments(docMultApiOut, docCompleteOrderDtlsOut, yfsEnv);
			}
		}

		// logging the method output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":preprocessShpConfirmation:Output=" + GCXMLUtil.getXMLString(docInput));
		}

		//return the updated input document
		return docInput;
	}


	/**
	 * <h3>Description:</h3>This method is used to confirm the shipments which are complete post
	 * consolidation
	 *
	 * @author Infosys Limited
	 * @param docMultApiOut The multi api output
	 * @param docOldOrderDtls The old complete order details
	 * @param yfsEnv The yfs environment reference
	 * @throws GCException Thrown if the legacy shipment confirmation throws exception
	 */
	private void confirmCompletedShipments(final Document docMultApiOut, final Document docOldOrderDtls,
			final YFSEnvironment yfsEnv) throws GCException {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":confirmCompletedShipments:entry");
		}

		// get the shipments
		final List listShipment = GCXMLUtil.getElementListByXpath(docMultApiOut, "/MultiApi/API/Output/Shipment");

		// process complete shipments
		processCompleteHeldKits(docOldOrderDtls, yfsEnv, listShipment);

		// log the method exit
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":confirmCompletedShipments:exit");
		}
	}


	/**
	 * <h3.Description:</h3> This method is used to process the held kit shipment
	 *
	 * @author Infosys Limited
	 * @param docOldOrderDtls the order details
	 * @param yfsEnv The YFS environment reference
	 * @param listShipment The list of held shipments
	 * @throws GCException thrown if legacy shipment confirmation throws exception
	 */
	private void processCompleteHeldKits(final Document docOldOrderDtls, final YFSEnvironment yfsEnv,
			final List listShipment) throws GCException {

		// logging method entry
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":processCompleteHeldKits:entry:Input:Shipment list=" + listShipment
					+ ":The order details=" + GCXMLUtil.getXMLString(docOldOrderDtls));
		}

		// cancel shipment service name
		final String strCancelShpmntSvc = props.getProperty("CANCEL_SHIPMENT_SVC", "GCCancelCompleteShipment_svc");

		Element eleShipment;
		Document docShpConf;
		Document docCancelShipmentIn;
		Document docCancelShipmentOut;

		// iterate through the Shipment
		for (Object objShipment : listShipment) {

			// fetch the shipment element
			eleShipment = (Element) objShipment;

			// check if the shipment is complete
			if (isShipmentComplete(eleShipment, docOldOrderDtls)) {

				// prepare the confirmation message
				docShpConf = createShipConfMsg(eleShipment, docOldOrderDtls);

				// prepare the cancel shipment message
				docCancelShipmentIn = prepareCancelShipmentInForComplete(eleShipment);

				// invoke the cancel shipment
				docCancelShipmentOut = GCCommonUtil.invokeService(yfsEnv, strCancelShpmntSvc, docCancelShipmentIn);

				// log the cancel shipment output
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug(CLASS_NAME + ":processCompleteHeldKits:CancelShipment service out="
							+ GCXMLUtil.getXMLString(docCancelShipmentOut));
				}

				// invoke the legacy shipment confirmation
				confShipment(yfsEnv, docShpConf);
			}
		}

		// logging the method exit
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":processCompleteHeldKits:exit");
		}
	}


	/**
	 * <h3>Description:</h3>This method is used to prepare the cancel shipment input xml
	 *
	 * @author Infosys Limite
	 * @param eleShipment The shipment element
	 * @return The document for shipment cancellation
	 */
	private Document prepareCancelShipmentInForComplete(final Element eleShipment) {

		//logging the method entry
		if( LOGGER.isDebugEnabled()){
			LOGGER.debug( CLASS_NAME + ":prepareCancelShipmentInForComplete:entry:Input="+GCXMLUtil.getElementXMLString(eleShipment));
		}

		//fetch the shipment key
		final String strShpKey = eleShipment.getAttribute( GCConstants.SHIPMENT_KEY);

		//create the document for Cancellation
		final Document docCancelShipmentIn = GCXMLUtil.createDocument( GCConstants.SHIPMENT);
		final Element eleShp = docCancelShipmentIn.getDocumentElement();
		eleShp.setAttribute(GCConstants.SHIPMENT_KEY, strShpKey);

		//logging the output
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":prepareCancelShipmentInForComplete:exit:Output="
					+ GCXMLUtil.getXMLString(docCancelShipmentIn));
		}

		// return the cancel shipment
		return docCancelShipmentIn;
	}


	/**
	 * <h3>Description:</h3> This method is used to create the shipment confirmation message to
	 * confirm
	 *
	 * @author Infosys Limited
	 * @param eleShipment The shipment element
	 * @return The shipment confirmation message to be used
	 */
	private Document createShipConfMsg(final Element eleShipment, final Document docOldOrdDtls) {

		// logging the input
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":createShipConfMsg:entry");
		}

		// fetch the kit parent prime line no
		final String strKitParentPLNo = getParentkitPlNo(eleShipment);

		// get the set of prime line nos present in the shipment
		// final Set<String> setKitCompDtls = getKitComponentDtls(strKitParentPLNo, docOldOrdDtls);

		// create the shipment confirmation msg
		final Document docConfMsg = GCXMLUtil.getDocumentFromElement(eleShipment);

		// remove the shipmetn no and shipment key
		final Element eleRoot = docConfMsg.getDocumentElement();
		eleRoot.removeAttribute(GCConstants.SHIPMENT_NO);
		eleRoot.removeAttribute(GCConstants.SHIPMENT_KEY);
		eleRoot.removeAttribute(GCConstants.STATUS);

		// set the attribute values
		final String strIsBckOrdNonShpQty = props.getProperty("IS_BACKORDER_NON_SHIPPED_QTY", GCConstants.YES);
		eleRoot.setAttribute(GCConstants.BACKORDER_NON_SHIPPED_QTY, strIsBckOrdNonShpQty);

		// get the shipment line
		final List listShpLns = GCXMLUtil.getElementListByXpath(docConfMsg, "/Shipment/ShipmentLines/ShipmentLine");

		Element eleShpLn;
		String strPrmLnNo;
		Element eleExtn;

		//iterate through each shipment line
		for (Object objShpLn : listShpLns) {

			// typecast
			eleShpLn = (Element) objShpLn;

			// fetch the prime line no
			strPrmLnNo = eleShpLn.getAttribute(GCConstants.PRIME_LINE_NO);

			// check if the prime line no matches
			if (strKitParentPLNo != null && strKitParentPLNo.equals(strPrmLnNo)) {

				// fetch the parent and remove
				final Node ndParent = eleShpLn.getParentNode();
				ndParent.removeChild(eleShpLn);
			} else {

				// remove key attributes
				eleShpLn.removeAttribute(GCConstants.SHIPMENT_KEY);
				eleShpLn.removeAttribute(GCConstants.SHIPMENT_LINE_KEY);
				eleShpLn.removeAttribute(GCConstants.ORDER_LINE_KEY);

				// add the Extn element
				eleExtn = docConfMsg.createElement(GCConstants.EXTN);
				eleExtn.setAttribute(GCConstants.EXTN_EXPECTED_QUANTITY, eleShpLn.getAttribute(GCConstants.QUANTITY));
				eleShpLn.appendChild(eleExtn);
			}
		}

		// logging the output
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":createShipConfMsg:exit:Output=" + GCXMLUtil.getXMLString(docConfMsg));
		}

		// return the msg
		return docConfMsg;
	}


	/**
	 * <h3>Description:</h3>This method is used to check if the shipment is complete to be confirmed
	 *
	 * @author Infosys Limited
	 * @param eleShipment The shipment to be validated if complete
	 * @param docOldOrderDtls The old order details
	 * @return true if the input shipment element is complete
	 */
	private boolean isShipmentComplete(final Element eleShipment, final Document docOldOrderDtls) {

		// logging the method entry
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":isShipmentComplete:entry:Input:Shipment element="
					+ GCXMLUtil.getElementXMLString(eleShipment));
		}

		// fetch the kit parent prime line no
		final String strKitParentPLNo = getParentkitPlNo(eleShipment);

		// get the set of prime line nos present in the shipment
		final Set<String> setKitCompDtls = getKitComponentDtls(strKitParentPLNo, docOldOrderDtls);

		// flag to indicate that the shipment is complete
		boolean bIsShipmentComplete = true;

		// iterate through the set and check if each of the shipment line is completely present
		for (String strCompPrimLnNo : setKitCompDtls) {

			// check if the complete line present
			if (setKitCompDtls.contains(strCompPrimLnNo)) {

				// check if the qty is complete
				if (getFlagValue("DISABLE_SHP_QTY_CHECK") && isOrderLineNotComplete(strCompPrimLnNo, docOldOrderDtls)) {

					// shipment missing
					bIsShipmentComplete = false;
					break;
				}

			} else {

				// shipment missing
				bIsShipmentComplete = false;
				break;
			}
		}

		// logging the method exit
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":isShipmentComplete:exit:Output=" + bIsShipmentComplete);
		}

		// return the flag
		return bIsShipmentComplete;
	}


	/**
	 * <h3>Description:</h3>This method is used to check if the order line is not complete
	 *
	 * @author Infosys Limited
	 * @param strCompPrimLnNo The prime line no
	 * @param docOldOrderDtls The order details
	 * @return true if the order not completely fulfilled else returns false
	 */
	private boolean isOrderLineNotComplete(final String strCompPrimLnNo, final Document docOldOrderDtls) {

		// logging the method input
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":isOrderLineNotComplete:entry:Input:Component line prime line no=" + strCompPrimLnNo
					+ ":Order details=" + GCXMLUtil.getXMLString(docOldOrderDtls));
		}

		// xpath to get the orderLine details
		final StringBuilder stbOrdLnXpth = new StringBuilder("/Order/OrderLines/OrderLine[@PrimeLineNo='");
		stbOrdLnXpth.append(strCompPrimLnNo);
		stbOrdLnXpth.append("']");
		final String strOrdLnXpth = stbOrdLnXpth.toString();

		// logging the xpath
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":isOrderLineNotComplete:Xpath for the orderline=" + strOrdLnXpth);
		}

		// fetch the orderline
		final Element eleOrderline = GCXMLUtil.getElementByXPath(docOldOrderDtls, strOrdLnXpth);

		// fetch the ordered qty
		final String strOrderedQty = eleOrderline.getAttribute(GCConstants.ORDERED_QTY);
		final int iOrderedQty = Integer.parseInt(strOrderedQty);

		// get the ship qty for the order line
		final int iShpQty = getShippedQty(strCompPrimLnNo, docOldOrderDtls);

		// the return flag
		final boolean bIsOlNotComplete;

		// check if the complete order line is shipped
		if (iShpQty == iOrderedQty) {

			// the order line completely shipped
			bIsOlNotComplete = false;
		} else {

			// the order line is not completely shipped
			bIsOlNotComplete = true;
		}

		// logging the method output
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":isOrderLineNotComplete:exit:Output=" + bIsOlNotComplete);
		}

		// return the flag
		return bIsOlNotComplete;
	}


	/**
	 * <h3>Description:</h3>This method is used to get the shipped qty for the orderline
	 *
	 * @author Infosys Limited
	 * @param strCompPrimLnNo The prime line no
	 * @param docOldOrderDtls The complete order details
	 * @return The shipped qty
	 */
	private int getShippedQty(final String strCompPrimLnNo, final Document docOldOrderDtls) {

		// log the method input
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":getShippedQty:entry:Input:Prime Line no=" + strCompPrimLnNo
					+ ":The complete order details=" + GCXMLUtil.getXMLString(docOldOrderDtls));
		}

		// xpath for the shipment line
		final StringBuilder stbShpLnXpth =
				new StringBuilder("/Order/Shipments/Shipment/ShipmentLines/ShipmentLine[@PrimeLineNo='");
		stbShpLnXpth.append(strCompPrimLnNo);
		stbShpLnXpth.append("']");
		final String strShpLnXpth = stbShpLnXpth.toString();

		// log the xpath
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":getShippedQty:Ther xpath for the shipmetn lines=" + strShpLnXpth);
		}

		// fetch the shipment line
		final List listShpLn = GCXMLUtil.getElementListByXpath(docOldOrderDtls, strShpLnXpth);

		// the variable to hold the shipped qty
		int iShippedQty = 0;

		Element eleShpLn;
		String strShpLnQty;

		// iterate through each of the shipment line
		for (Object objShpLn : listShpLn) {

			// typecast
			eleShpLn = (Element) objShpLn;

			// get the shipped qty for each line
			strShpLnQty = eleShpLn.getAttribute(GCConstants.QUANTITY);

			// sum the shipped qty
			iShippedQty = iShippedQty + Integer.parseInt(strShpLnQty);
		}

		// logging the output
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":getShippedQty:exit:Output=" + iShippedQty);
		}

		// return the shipped qty
		return iShippedQty;
	}


	/**
	 * <h3>Description:</h3>This method is used to get the set of Prime line nos of the shipment lines
	 * in the shipment
	 *
	 * @author Infosys Limited
	 * @param eleShipment The shipment element
	 * @return The set of prime line nos in the shipment
	 */
	private Set<String> getPLNoOfShpLnInShipment(final Element eleShipment) {

		// log the input
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":getPLNoOfShpLnInShipment:entry");
		}

		//get the shipment lines
		final NodeList nlShpLn = eleShipment.getElementsByTagName( GCConstants.SHIPMENT_LINE);
		final int iNoOfShpLn = nlShpLn.getLength();

		// set to return
		final Set<String> setPrimLnNo = new HashSet<String>();

		Element eleShpLn;
		String strPrimLnNo;

		// iterate through each of the shipment line
		for (int iShpLnIndex = 0; iShpLnIndex < iNoOfShpLn; iShpLnIndex++) {

			// fetch the shipment line
			eleShpLn = (Element) nlShpLn.item(iShpLnIndex);

			// fetch the prime line no
			strPrimLnNo = eleShpLn.getAttribute(GCConstants.PRIME_LINE_NO);

			// add to the set
			setPrimLnNo.add(strPrimLnNo);
		}

		// log the output
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":getPLNoOfShpLnInShipment:exit:Output=" + setPrimLnNo);
		}

		// return the set
		return setPrimLnNo;
	}


	/**
	 * <h3>Description:</h3>This method is used to get the kit parent prime line no from the temp
	 * shipment
	 *
	 * @author Infosys Limited
	 * @param eleShipment The shipment element
	 * @return the prime line no of the shipment else returns null
	 */
	private String getParentkitPlNo(final Element eleShipment) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":getParentkitPlNo:entry:Input=" + GCXMLUtil.getElementXMLString(eleShipment));
		}

		// get the parent kit no
		final String strCharDelimiter = props.getProperty("SHP_NO_CHAR_DELIMITER", "K");
		final String strShpNo = eleShipment.getAttribute(GCConstants.SHIPMENT_NO);

		final String[] strarrShpNo = strShpNo.split(strCharDelimiter);

		final String strParentKitPLNo;

		if (strarrShpNo != null && strarrShpNo.length > 1) {

			strParentKitPLNo = strarrShpNo[1];
		} else {

			strParentKitPLNo = null;
		}

		// logging the mthod exit
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":getParentkitPlNo:exit:Output=" + strParentKitPLNo);
		}

		return strParentKitPLNo;
	}


	/**
	 * <h3>Description:</h3>This method is used to update the multi api input document withe existing
	 * shipment details and stamp the order header key for each of the shipment
	 *
	 * @author Infosys Limited
	 * @param docHoldShpmnts
	 * @param docCompleteOrderDtlsOut
	 */
	private void updateExistingShipments(final Document docHoldShpmnts, final Document docCompleteOrderDtlsOut) {

		// logging the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":updateExistingShipments:entry:Input=" + GCXMLUtil.getXMLString(docHoldShpmnts));
		}

		// fetch the shipments
		final List listShp = GCXMLUtil.getElementListByXpath(docHoldShpmnts, "/MultiApi/API/Input/Shipment");

		String strShipmentNo;
		Element eleExistingShpmnt;
		Element eleShpnt;

		// iterate through each of the element
		for (Object objShp : listShp) {

			//get the ExtnKitLineNo
			eleShpnt = (Element) objShp;
			strShipmentNo = eleShpnt.getAttribute(GCConstants.SHIPMENT_NO);

			//get any shipment if exists already
			eleExistingShpmnt = getExistingShpmntForKitLineNo(strShipmentNo, docCompleteOrderDtlsOut);

			// check if the shipment exist
			if (eleExistingShpmnt != null) {

				// update the multi api input
				updateMultiApiShpmnt(eleShpnt, eleExistingShpmnt, docCompleteOrderDtlsOut);
			}
		}

		// logging the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":updateExistingShipments:exit:Output=" + GCXMLUtil.getXMLString(docHoldShpmnts));
		}
	}


	/**
	 * <h3>Description:</h3> This method is used to update the input to multi api if shipment exists
	 *
	 * @author Infosys Limited
	 * @param eleShpnt The shipment element to be updated
	 * @param eleExistingShpmnt The existing shipment element from complete order details output
	 * @param docCompleteOrderDtlsOut The complete order details output
	 */
	private void updateMultiApiShpmnt(final Element eleShpnt, final Element eleExistingShpmnt, final Document docCompleteOrderDtlsOut) {

		// logging the method entry
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":updateMultiApiShpmnt:entry:Input:Shipment before update="
					+ GCXMLUtil.getElementXMLString(eleShpnt) + ":Shipment in order details="
					+ GCXMLUtil.getElementXMLString(eleExistingShpmnt));
		}

		// get the Shipment lines for the shipment
		final NodeList nlShipmentLine = eleShpnt.getElementsByTagName(GCConstants.SHIPMENT_LINE);
		final int iNoOfShpLns = nlShipmentLine.getLength();

		Element eleShpLn;
		String strPriLnNo;
		Element eleExistingShpLn;
		String strExistingQty;
		String strQty;
		int iQty;
		String strShpLnKey;
		String strShpKey;

		// iterate through each of the shipment line
		for (int iShpLnIndex = 0; iShpLnIndex < iNoOfShpLns; iShpLnIndex++) {

			// shipment line
			eleShpLn = (Element) nlShipmentLine.item(iShpLnIndex);
			strPriLnNo = eleShpLn.getAttribute(GCConstants.PRIME_LINE_NO);

			// fetch the shipment line
			eleExistingShpLn = getShipmentLnFromShipment(strPriLnNo, eleExistingShpmnt);

			// check if shipment line is not null
			if (eleExistingShpLn != null) {

				// fetch the Qty
				strExistingQty = eleExistingShpLn.getAttribute(GCConstants.QUANTITY);
				strQty = eleShpLn.getAttribute(GCConstants.QUANTITY);
				iQty = Integer.parseInt(strQty) + Integer.parseInt(strExistingQty);
				eleShpLn.setAttribute(GCConstants.QUANTITY, Integer.toString(iQty));

				// fetch the keys and set
				strShpLnKey = eleExistingShpLn.getAttribute(GCConstants.SHIPMENT_LINE_KEY);
				eleShpLn.setAttribute(GCConstants.SHIPMENT_LINE_KEY, strShpLnKey);
				strShpKey = eleExistingShpLn.getAttribute(GCConstants.SHIPMENT_KEY);
				eleShpnt.setAttribute(GCConstants.SHIPMENT_KEY, strShpKey);
			}
		}

		// log the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":updateMultiApiShpmnt:exit:Output=" + GCXMLUtil.getElementXMLString(eleShpnt));
		}
	}


	/**
	 * <h3.Description:</h3> This method is used to get the shipment line fro the input prime line no
	 *
	 * @author Infosys Limited
	 * @param strPriLnNo The prime line no
	 * @param eleExistingShpmnt The shipment from the complete order details
	 * @return The shipment line for the input shipment
	 */
	private Element getShipmentLnFromShipment(final String strPriLnNo, final Element eleExistingShpmnt) {

		// logging the mehtod entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getShipmentLnFromShipment:entry:Input:Prime Line no=" + strPriLnNo
					+ ":Existing shipment=" + GCXMLUtil.getElementXMLString(eleExistingShpmnt));
		}

		// get the shipment lines
		final NodeList nlShipmentLine = eleExistingShpmnt.getElementsByTagName(GCConstants.SHIPMENT_LINE);
		final int iNoOfShpLn = nlShipmentLine.getLength();

		// return Shipment line
		Element eleShpLn = null;

		Element eleExistingShpLn;
		String strExistingPlNo;

		// iterate through each of the shipment line
		for (int iShpLnIndex = 0; iShpLnIndex < iNoOfShpLn; iShpLnIndex++) {

			// get the shipment line
			eleExistingShpLn = (Element) nlShipmentLine.item(iShpLnIndex);

			// fetch the prime line no
			strExistingPlNo = eleExistingShpLn.getAttribute(GCConstants.PRIME_LINE_NO);

			// check if the prime line no matches
			if (StringUtils.isNotEmpty(strExistingPlNo) && strExistingPlNo.equals(strPriLnNo)) {

				eleShpLn = eleExistingShpLn;
				break;
			}
		}

		// logging the mehtod exit
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":getShipmentLnFromShipment:exit");
		}

		// return the shipment line
		return eleShpLn;
	}


	/**
	 * <h3>Description:</h3> This method is used to get the shipment get the existing shipment for the
	 * kit line
	 *
	 * @author Infosys Limited
	 * @param strShipmentNo The shipment no associate with the shipment
	 * @param docCompleteOrderDtlsOut The complete order details
	 * @return The shipment element from the complete order details output
	 */
	private Element getExistingShpmntForKitLineNo(final String strShipmentNo, final Document docCompleteOrderDtlsOut) {

		// logging the method entry
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":getExistingShpmntForKitLineNo:entry:Input:Kit Line no=" + strShipmentNo);
		}

		// xpath for shipment element
		final StringBuilder stbShpmntXpth = new StringBuilder("/Order/Shipments/Shipment[@ShipmentNo='");
		stbShpmntXpth.append(strShipmentNo);
		stbShpmntXpth.append("']");
		final String strShpmntXpth = stbShpmntXpth.toString();

		// logging the xpath
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getExistingShpmntForKitLineNo:Xpath for Shipment=" + strShpmntXpth);
		}

		// fetch the shipment
		final Element eleShpmnt = GCXMLUtil.getElementByXPath(docCompleteOrderDtlsOut, strShpmntXpth);

		// logging the method exit
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":getExistingShpmntForKitLineNo:exit:Output=" + eleShpmnt);
		}

		// return the shipment element
		return eleShpmnt;
	}


	/**
	 * <h3>Description:</h3>This method is used to check if any api call is present in the input
	 * document
	 *
	 * @author Infosys Limited
	 * @param docHoldShpmnts The multi api input
	 * @return True if any API element is present in the input else false
	 */
	private boolean isAnyApiCallPresent(final Document docHoldShpmnts) {

		// logging the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":isAnyApiCallPresent:entry");
		}

		// check if any API element is present
		final List listApis = GCXMLUtil.getElementListByXpath(docHoldShpmnts, "/MultiApi/API");

		// the flag indicating if any api call is present
		final boolean bApiCallPresent;

		// check if any API element is present
		if (listApis.size() > 0) {

			bApiCallPresent = true;
		} else {
			bApiCallPresent = false;
		}

		// logging the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":isAnyApiCallPresent:exit:Output=" + bApiCallPresent);
		}

		// return the flag
		return bApiCallPresent;
	}


	/**
	 * <h3>Description:</h3>This method is used to prepare the input to multi api input for creating
	 * shipments on hold
	 *
	 * @author Infosys Limited
	 * @param mapKitParentDtls The map containing the kit parent details
	 * @param docInput The input shipment confirmation message that will be updated
	 * @param docCompleteOrderDtlsOut The complete order details output
	 * @return The multiapi input document for creating the
	 */
	private Document getHoldShipments(final Map<String, Set<String>> mapKitParentDtls, final Document docInput,
			final Document docCompleteOrderDtlsOut) {

		// logging the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getHoldShipments:entry");
		}

		// the multiapi input document
		final Document docMultiApiIn = GCXMLUtil.createDocument(GCConstants.MULTI_API_TAG);

		// get the set of kits corresponding to the shipment
		final Set<String> setKits = mapKitParentDtls.keySet();

		Set<String> setCompPrimLns;
		boolean bValidateShpConfContr = false;

		// iterate through each of the kit parent
		for (String strKitParentPrmLnNo : setKits) {

			// get the component line details
			setCompPrimLns = mapKitParentDtls.get(strKitParentPrmLnNo);

			// check if the kit is completely Fulfilled
			if (isKitNotCompletelyFulfilled(setCompPrimLns, docInput, docCompleteOrderDtlsOut)) {

				// update the shipment confirmation message and update the multiapi input
				updateShpConfMsg(strKitParentPrmLnNo, setCompPrimLns, docInput, docMultiApiIn, docCompleteOrderDtlsOut);
				bValidateShpConfContr = true;
			}
		}

		// validate the shipment confirmation message
		if (bValidateShpConfContr) {

			// update the shipment container
			updateContrForShpMsg(docInput);
		}

		// logging the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getHoldShipments:exit:Output=" + GCXMLUtil.getXMLString(docMultiApiIn));
		}

		// return the multi api input
		return docMultiApiIn;
	}


	/**
	 * <h3>Description:</h3> This method is used to update the containers element
	 *
	 * @author Infosys Limited
	 * @param docInput The shipment advice message
	 */
	private void updateContrForShpMsg(final Document docInput) {

		// logging the method input
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":updateContrForShpMsg:entry:Input=" + GCXMLUtil.getXMLString(docInput));
		}

		// get the set of prime line nos
		final Set<String> setPrmLnnos = getPrimeLnNosFromShpLn(docInput);
		final int iNoOfPLNos = setPrmLnnos.size();

		// check if the no of prime line nos >0
		if (iNoOfPLNos > 0) {

			// remove the shipment lines not in the set
			removeShpLnsNotInSet(docInput, setPrmLnnos);

			// remove containers which do not have shipment lines
			removeEmptyContainers(docInput);
		}
		// logging the outpu
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":updateContrForShpMsg:exit:Output=" + GCXMLUtil.getXMLString(docInput));
		}

	}


	/**
	 * <h3>Description:</h3>This method is used to remove the conatiners wit no shipment lines in it
	 *
	 * @author Infosys Limited
	 * @param docInput the ship conf msg
	 */
	public void removeEmptyContainers(final Document docInput) {

		// logging the method entry
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":removeEmptyContainers:entry" + GCXMLUtil.getXMLString(docInput));
		}

		// fetch the container elements
		final List listContainer = GCXMLUtil.getElementListByXpath(docInput, "/Shipment/Containers/Container");

		Element eleContainer;
		NodeList nlShpLn;
		int iNoOfShpLn;
		Node ndParent;

		// iterate through each of the container element
		for (Object objContainer : listContainer) {

			// type cast
			eleContainer = (Element) objContainer;

			// get the shipment line
			nlShpLn = eleContainer.getElementsByTagName(GCConstants.SHIPMENT_LINE);
			iNoOfShpLn = nlShpLn.getLength();

			// check if the shipment lines exist
			if (iNoOfShpLn == 0) {

				// fetch the parent of the container to delete the container
				ndParent = eleContainer.getParentNode();

				// check if the container parent not null
				if (ndParent != null) {

					// remove the empty container
					ndParent.removeChild(eleContainer);
				}
			}
		}

		// logging the method exit
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":removeEmptyContainers:exit" + GCXMLUtil.getXMLString(docInput));
		}
	}


	/**
	 * <h3>Description:</h3> This method is used to remove the shipment line not in the set
	 *
	 * @author Infosys Limited
	 * @param docInput The ship confirmation message
	 * @param setPrmLnnos The set of prime line nos
	 */
	private void removeShpLnsNotInSet(final Document docInput, final Set<String> setPrmLnnos) {

		// logging the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":removeShpLnsNotInSet:entry");
		}

		//xpath to fetch the shipment lines
		final StringBuilder stbContShpLnXpth =
				new StringBuilder(
						"/Shipment/Containers/Container/ContainerDetails/ContainerDetail[./ShipmentLine[not(contains('_");

		//append the list of prime line nos
		for(String strPrimLnNo:setPrmLnnos){
			stbContShpLnXpth.append( strPrimLnNo);
			stbContShpLnXpth.append("_");
		}

		stbContShpLnXpth.append("' , concat('_',@PrimeLineNo,'_')))]]");
		final String strContShpLnXpth = stbContShpLnXpth.toString();

		// logging the xpth
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":removeShpLnsNotInSet:Xpath for Shipment Line in Container to be deleted="
					+ strContShpLnXpth);
		}

		// fetch the shipment lines
		final List listContShpLn = GCXMLUtil.getElementListByXpath(docInput, strContShpLnXpth);

		Element eleShpLn;
		Node ndParent;

		// iterate and remove the child
		for (Object objShpLn : listContShpLn) {

			// typecast and fetch parent
			eleShpLn = (Element) objShpLn;
			ndParent = eleShpLn.getParentNode();

			// remove the shipment line
			ndParent.removeChild(eleShpLn);
		}

		// logging method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":removeShpLnsNotInSet:exit");
		}
	}


	/**
	 * <h3>Description:</h3> This method is used to get the list of prime line nos in the shipment
	 * lines
	 *
	 * @author Infosys Limited
	 * @param docInput The Shipment conf message
	 * @return the set of prime line nos in the shipment line
	 */
	private Set<String> getPrimeLnNosFromShpLn(final Document docInput) {

		// logging the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getPrimeLnNosFromShpLn:entry:Input=" + GCXMLUtil.getXMLString(docInput));
		}

		// fetch the shipment lines
		final List listShpLn = GCXMLUtil.getElementListByXpath(docInput, "/Shipment/ShipmentLines/ShipmentLine");

		// set to hold all the prime line nos
		final Set<String> setShpLnPrmLnNos = new HashSet<String>();

		Element eleShpLn;
		String strPrimLnNo;

		// iterate through the shipment lines
		for (Object objShpLn : listShpLn) {

			//typecast
			eleShpLn = (Element) objShpLn;

			// get the Prime line no
			strPrimLnNo = eleShpLn.getAttribute(GCConstants.PRIME_LINE_NO);
			setShpLnPrmLnNos.add(strPrimLnNo);
		}

		// logging the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getPrimeLnNosFromShpLn:exit:Output=" + setShpLnPrmLnNos);
		}

		// return the set
		return setShpLnPrmLnNos;
	}


	/**
	 * <h3>Description:</h3> This method is used update the shipment advice message for partial
	 * shipment advices
	 *
	 * @author Infosys Limited
	 * @param strKitParentPrmLnNo The kit parent prime line no
	 * @param setCompPrimLns The set of prime line nos of kit components
	 * @param docInput The shipment confirmation message to be updated
	 * @param docMultiApiIn The multi api input
	 * @param docCompleteOrderDtlsOut The complete order details
	 */
	private void updateShpConfMsg(final String strKitParentPrmLnNo, final Set<String> setCompPrimLns,
			final Document docInput, final Document docMultiApiIn, final Document docCompleteOrderDtlsOut) {

		// logging the method input
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":updateShpConfMsg:entry");
		}

		// update multi api input to create a Shipment for hold
		final Element eleInput = docMultiApiIn.createElement(GCConstants.INPUT);

		// prepare the input for multi Api
		prepareChangeShipmentInput(eleInput, strKitParentPrmLnNo, setCompPrimLns, docInput, docCompleteOrderDtlsOut);

		// check if the child elments added
		if (eleInput.hasChildNodes()) {

			// create the API element
			final Element eleMultiApi = docMultiApiIn.getDocumentElement();
			final Element eleApi = docMultiApiIn.createElement(GCConstants.API);
			eleMultiApi.appendChild(eleApi);
			eleApi.appendChild(eleInput);
			final Element eleTemplate = docMultiApiIn.createElement(GCConstants.TEMPLATE_TAG);
			eleApi.appendChild(eleTemplate);

			// set the flow name
			final String strFlowName = props.getProperty("CHANGE_SHIPMENT_SVC");// TODO
			eleApi.setAttribute(GCConstants.FLOW_NAME, strFlowName);
		}

		// logging the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":updateShpConfMsg:exit");
		}
	}


	/**
	 * <h3>Description:</h3> This method is used to prepare the input to the changeShipment api
	 *
	 * @author Infosys Limited
	 * @param eleInput The input element
	 * @param strKitParentPrmLnNo the kit parent prime line number
	 * @param setCompPrimLns The set of component prime line number
	 * @param docInput The shipment confirmation message
	 * @param docCompleteOrderDtlsOut The complete order details
	 */
	private void prepareChangeShipmentInput(final Element eleInput, final String strKitParentPrmLnNo,
			final Set<String> setCompPrimLns, final Document docInput, final Document docCompleteOrderDtlsOut) {

		//log the method entry
		if( LOGGER.isDebugEnabled()){

			LOGGER.debug( CLASS_NAME + ":prepareChangeShipmentInput:entry");
		}

		// get the order header key
		final Element eleOrder = docCompleteOrderDtlsOut.getDocumentElement();
		final String strOrdHdrKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
		final String strOrdNo = eleOrder.getAttribute(GCConstants.ORDER_NO);
		final String strSellrOrgCode = eleOrder.getAttribute(GCConstants.SELLER_ORGANIZATION_CODE);

		// prepare the Shipment
		final Document docMultiApiIn = eleInput.getOwnerDocument();
		final Element eleShpmnt = docInput.getDocumentElement();
		final Element eleImpShpmnt = (Element) docMultiApiIn.importNode(eleShpmnt, false);
		eleImpShpmnt.setAttribute(GCConstants.ORDER_HEADER_KEY, strOrdHdrKey);
		eleImpShpmnt.setAttribute(GCConstants.SELLER_ORGANIZATION_CODE, strSellrOrgCode);
		eleImpShpmnt.setAttribute(GCConstants.SHIPMENT_NO, getShpNoForHoldShpmnt(strOrdNo, strKitParentPrmLnNo));
		eleInput.appendChild(eleImpShpmnt);

		// create the shipment lines and containers
		final Element eleShpLns = docMultiApiIn.createElement(GCConstants.SHIPMENT_LINES);
		eleImpShpmnt.appendChild(eleShpLns);
		final Element eleContainers = docMultiApiIn.createElement(GCConstants.CONTAINERS);

		// fetch the shipment Lines element of shp advice
		final Node ndShpLnsFoShpAdv = eleShpmnt.getElementsByTagName(GCConstants.SHIPMENT_LINES).item(0);

		// set for all the containers
		final Set<String> setContNos = new HashSet<String>();

		Element eleShpLn;
		Element eleImpShlLn;
		List listContr;
		Element eleContr;
		Element eleImpContr;
		String strContrNo;

		//iterate through the components set
		for (String strCompPrimLnNo : setCompPrimLns) {

			// fetch the shipment line from the shipment confirmation message and add it to the
			eleShpLn = fetchShipmentLine(strCompPrimLnNo, docInput);

			// check if shipment line is not null
			if (eleShpLn == null) {

				LOGGER.warn(CLASS_NAME + ":prepareChangeShipmentInput:The shipment line element is null!!!");
			} else {

				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug(CLASS_NAME + ":prepareChangeShipmentInput:The shipment line being removed="
							+ GCXMLUtil.getElementXMLString(eleShpLn));
				}

				// the shipment line to be removed
				eleImpShlLn = (Element) docMultiApiIn.importNode(eleShpLn, true);
				eleImpShlLn.setAttribute(GCConstants.ORDER_HEADER_KEY, strOrdHdrKey);
				eleShpLns.appendChild(eleImpShlLn);

				// remove the shipment line from the shipment advice
				ndShpLnsFoShpAdv.removeChild(eleShpLn);

				// fetch the Container element associated with the shipment line
				listContr = fetchContainer(strCompPrimLnNo, docInput);

				// iterate through the containers
				for (Object objContr : listContr) {

					// check if the container is null
					if (objContr == null) {

						LOGGER.warn(CLASS_NAME + ":prepareChangeShipmentInput: The Container element is null!!!");
					} else {

						// typecast to element
						eleContr = (Element) objContr;

						if (LOGGER.isDebugEnabled()) {
							LOGGER.debug(CLASS_NAME + ":prepareChangeShipmentInput:Set before updat=" + setContNos
									+ "The Container element chosen=" + GCXMLUtil.getElementXMLString(eleContr));
						}

						// fetch the container no
						strContrNo = eleContr.getAttribute(GCConstants.CONTAINER_NO);

						if (setContNos.contains(strContrNo)) {

							LOGGER.warn(CLASS_NAME
									+ ":prepareChangeShipmentInput:The container already present, hence not adding again!!");
						} else {

							// add the container no to the set
							setContNos.add(strContrNo);

							// import and add the container
							eleImpContr = (Element) docMultiApiIn.importNode(eleContr, true);
							eleContainers.appendChild(eleImpContr);

							// update the shipmentliine with order header key
							updateShpLnWithOrdHdrKey(eleImpContr, strOrdHdrKey);
						}
					}
				}
			}
		}

		// check if the child nodes are present
		if (eleShpLns.hasChildNodes() && eleContainers.hasChildNodes()) {

			// filter and update the input message
			updateMultiApiIn(setCompPrimLns, eleContainers);

			// add the shipment to the Input elemnt
			eleImpShpmnt.appendChild(eleContainers);
		}

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":prepareChangeShipmentInput:exit:Output=" + GCXMLUtil.getElementXMLString(eleInput));
		}
	}


	/**
	 * <h3>Description:</h3>This method is used to get the Shipment No for the hold shipment
	 *
	 * @author Infosys Limited
	 * @param strOrdNo The order no
	 * @param strKitParentPrmLnNo The prime line no of the kit parent
	 * @return The shipment no to be used for the shipment on hold
	 */
	private String getShpNoForHoldShpmnt(final String strOrdNo, final String strKitParentPrmLnNo) {

		// logging the method input
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":getShpNoForHoldShpmnt:entry:Input:OrderNo=" + strOrdNo + ":Kit aprent prime line no="
					+ strKitParentPrmLnNo);
		}

		// the format decided OrderNO<ConfiguredChar>Kit Parent PL No
		final StringBuilder stbShpNo = new StringBuilder(strOrdNo);
		final String strCharDelimiter = props.getProperty("SHP_NO_CHAR_DELIMITER", "K");
		stbShpNo.append(strCharDelimiter);
		stbShpNo.append(strKitParentPrmLnNo);
		final String strShpNo = stbShpNo.toString();

		// logging the method exit
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":getShpNoForHoldShpmnt:exit:Shipment No=" + strShpNo);
		}

		// return the shipment no
		return strShpNo;
	}


	/**
	 * <h3>Description:</h3> This method is used to update the shipment lines with orer header key
	 *
	 * @author Infosys Limited
	 * @param eleContr The container element
	 * @param strOrdHdrKey The order header key to be set
	 */
	private void updateShpLnWithOrdHdrKey(final Element eleContr, final String strOrdHdrKey) {

		// logging the method input
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":updateShpLnWithOrdHdrKey:entry:Input:OrderHeaderKey=" + strOrdHdrKey
					+ ":The container element=" + GCXMLUtil.getElementXMLString(eleContr));
		}

		// fetch the shipment line element
		final NodeList nlShpLn = eleContr.getElementsByTagName(GCConstants.SHIPMENT_LINE);
		final int iNoOfShpLn = nlShpLn.getLength();

		Element eleShpLn;

		// iterate through the shipment line
		for (int iShpLnIndex = 0; iShpLnIndex < iNoOfShpLn; iShpLnIndex++) {

			// typecast the shipment line
			eleShpLn = (Element) nlShpLn.item(iShpLnIndex);

			// set the order header key
			eleShpLn.setAttribute(GCConstants.ORDER_HEADER_KEY, strOrdHdrKey);
		}

		// logging the method exit
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":updateShpLnWithOrdHdrKey:exit:Ouptut=" + GCXMLUtil.getElementXMLString(eleContr));
		}
	}


	/**
	 * <h3>Description:</h3> This method is used to fetch the list of Container element corresponding
	 * to the prime line no of the component
	 *
	 * @author Infosys Limited
	 * @param strCompPrimLnNo The prime line of the component
	 * @param docInput The shipment advice
	 * @return The list of Container element
	 */
	private List fetchContainer(final String strCompPrimLnNo, final Document docInput) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":fetchContainer:entry:Input:Component Prime Line No=" + strCompPrimLnNo
					+ ":Input doc=" + GCXMLUtil.getXMLString(docInput));
		}

		// prepare the xpath
		final StringBuilder stbConXpth =
				new StringBuilder(
						"/Shipment/Containers/Container[./ContainerDetails/ContainerDetail/ShipmentLine/@PrimeLineNo='");
		stbConXpth.append(strCompPrimLnNo);
		stbConXpth.append("']");
		final String strContXpth = stbConXpth.toString();

		// logging the xpath
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":fetchContainer:Xpth for Container=" + strContXpth);
		}

		// fetch the list of Container element
		final List listCont = GCXMLUtil.getElementListByXpath(docInput, strContXpth);

		// logging the method output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":fetchContainer:exit:Output=" + listCont);
		}

		// return the container
		return listCont;
	}


	/**
	 * <h3>Description:</h3> This method is used to get the update Multi api input xmls with container
	 * details
	 *
	 * @author Infosys Limited
	 * @param setCompPrimLns The set of prime line nos
	 * @param eleContainers The Containers element
	 */
	private void updateMultiApiIn(final Set<String> setCompPrimLns, final Element eleContainers) {

		// logging the method entry
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":updateMultiApiIn:entry:Input:Set of prime line nos=" + setCompPrimLns
					+ ":Containers element=" + GCXMLUtil.getElementXMLString(eleContainers));
		}

		// fetch the Shipment Line elements
		final NodeList nlShipmentLn = eleContainers.getElementsByTagName(GCConstants.SHIPMENT_LINE);

		// a list to hold
		final List<Element> listShpLnsToRemove = new ArrayList<Element>();
		final int iNoOfShpLns = nlShipmentLn.getLength();

		Element eleShpLn;
		String strPrmLnNo;

		// iterate through each of the shipment lines
		for (int iShpLnIndex = 0; iShpLnIndex < iNoOfShpLns; iShpLnIndex++) {

			// get the Shipment Line element
			eleShpLn = (Element) nlShipmentLn.item(iShpLnIndex);

			// fetch the Prime Line no
			strPrmLnNo = eleShpLn.getAttribute(GCConstants.PRIME_LINE_NO);

			// check if the prime line no is present in the set
			if (!setCompPrimLns.contains(strPrmLnNo)) {

				// add to the list to remove
				listShpLnsToRemove.add(eleShpLn);
			}
		}

		Node ndContDtl;
		Node ndContDtls;

		//remove the shipment lines in the list
		for (Element eleShpLnToRemove : listShpLnsToRemove) {

			//fetch the parent of the shipment line
			ndContDtl = eleShpLnToRemove.getParentNode();
			if (ndContDtl != null) {

				// fetch the container details element
				ndContDtls = ndContDtl.getParentNode();

				// check if the parent is not null
				if (ndContDtls != null) {

					ndContDtls.removeChild(ndContDtl);
				}
			}
		}

		// logging the updated output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":updateMultiApiIn:exit:Output=" + GCXMLUtil.getElementXMLString(eleContainers));
		}
	}


	/**
	 * <h3>Description:</h3> This method is used to fetch the shipment line from the shipment advice
	 *
	 * @author Infosys Limited
	 * @param strCompPrimLnNo The component prime line no
	 * @param docInput The shipment advice msg
	 * @return the shipment line corresponding to the input strCompPrimLnNo
	 */
	private Element fetchShipmentLine(final String strCompPrimLnNo, final Document docInput) {

		// logging the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":fetchShipmentLine:entry:Input:Prime Line No=" + strCompPrimLnNo + ":Input Doc="
					+ GCXMLUtil.getXMLString(docInput));
		}

		// prepare the xpath to fetch the shipment line
		final StringBuilder stbShpLnXpth = new StringBuilder("/Shipment/ShipmentLines/ShipmentLine[@PrimeLineNo='");
		stbShpLnXpth.append(strCompPrimLnNo);
		stbShpLnXpth.append("']");
		final String strShpLnXpth = stbShpLnXpth.toString();

		// fetch the shipment line
		final Element eleShpLn = GCXMLUtil.getElementByXPath(docInput, strShpLnXpth);

		// logging the method exit
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":fetchShipmentLine:exit" + eleShpLn);
		}

		// return the shipment line
		return eleShpLn;
	}


	/**
	 * <h3>Description:</h3>This method is used to check if the kit is partially fulfilled
	 *
	 * @author Infosys Limited
	 * @param setCompPrimLns The kit components prime line nos
	 * @param docInput The input document
	 * @param docCompleteOrderDtlsOut The complete order details output
	 * @return
	 */
	private boolean isKitNotCompletelyFulfilled(final Set<String> setCompPrimLns, final Document docInput,
			final Document docCompleteOrderDtlsOut) {

		// logging the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":isKitNotCompletelyFulfilled:entry");
		}

		// the return flag
		boolean bIsCompletelyFulfilled = true;

		// iterate through the component lines
		for (String strCompPrmLnNo : setCompPrimLns) {

			// validate if the component present in the shipment
			if (isCompLnPresentInShpMsg(strCompPrmLnNo, docInput)) {

				// check if the kit component is completely fulfilled
				if (getFlagValue("DISABLE_BO_CHECK")
						&& getModQtyForKitCompLn(strCompPrmLnNo, docInput, docCompleteOrderDtlsOut) > 0) {// TODO

					// the flag needs to be set to false and break
					bIsCompletelyFulfilled = false;
					break;
				}
			} else {

				// the component shipment line is not present
				bIsCompletelyFulfilled = false;
				break;
			}
		}

		// the return variable
		final boolean bIsNotCompleteFulfilled = !bIsCompletelyFulfilled;

		// logging the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":isKitNotCompletelyFulfilled:exit:Output=" + bIsNotCompleteFulfilled);
		}

		// return the flag
		return bIsNotCompleteFulfilled;
	}


	/**
	 * <h3>Description:</h3> This method is used used check if the component line is present in the
	 * shipment message
	 *
	 * @author Infosys Limited
	 * @param strCompPrmLnNo The component prime line no
	 * @param docInput The shp conf msg
	 * @return True if the line is present else returns false
	 */
	private boolean isCompLnPresentInShpMsg(final String strCompPrmLnNo, final Document docInput) {

		// logging the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":isCompLnPresentInShpMsg:entry:Input:Comp prime line no=" + strCompPrmLnNo
					+ ":Shp Conf Msg=" + GCXMLUtil.getXMLString(docInput));
		}

		// xpath to fetch the shipment line
		final StringBuilder stbShpLnXpth = new StringBuilder("/Shipment/ShipmentLines/ShipmentLine[@PrimeLineNo='");
		stbShpLnXpth.append(strCompPrmLnNo);
		stbShpLnXpth.append("']");
		final String strShpLnXpth = stbShpLnXpth.toString();

		//logging the xpath
		if( LOGGER.isDebugEnabled()){

			LOGGER.debug(CLASS_NAME + ":isCompLnPresentInShpMsg:Shipment Line xpth=" + strShpLnXpth);
		}

		// fetch the shipment lines
		final List listShpLn = GCXMLUtil.getElementListByXpath(docInput, strShpLnXpth);

		// the return flag
		final boolean bShpLnPresent;

		// check if the shipment lines are present
		if (listShpLn.size() > 0) {

			bShpLnPresent = true;
		} else {

			bShpLnPresent = false;
		}

		// return the flag
		return bShpLnPresent;
	}


	/**
	 * <h3>Description:</h3> This method is used get the partial qty that do not complete the kit
	 *
	 * @author Infosys Limited
	 * @param strCompPrmLnNo The component prime line no
	 * @param docInput The Current shipment confirmaiton msg
	 * @param docCompleteOrderDtlsOut The complete order details
	 * @return the partial qty that does nto complete the kit
	 */
	private int getModQtyForKitCompLn(final String strCompPrmLnNo, final Document docInput,
			final Document docCompleteOrderDtlsOut) {

		// logging the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getModQtyForKitCompLn:entry: Input:Component Prime Line no=" + strCompPrmLnNo
					+ ":Current Ship Conf msg=" + GCXMLUtil.getXMLString(docInput));
		}

		// fetch the kit qty
		final int iKitQty = getKitQtyForCompLine(strCompPrmLnNo, docCompleteOrderDtlsOut);

		// get the qty
		final int iQty;

		//check which qty needs to be fetched
		if (getFlagValue("DISABLE_SHP_QTY_FETCH")) {

			iQty=getShippedQtyForCompLn(strCompPrmLnNo, docInput);
		}else{

			iQty=getExpectedQtyForCompLn(strCompPrmLnNo, docInput);
		}

		// check if the component is completely fulfilled
		final int iDiffQty = iQty % iKitQty;

		// logging the output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getModQtyForKitCompLn:exit:Output=" + iDiffQty);
		}

		// return the difference
		return iDiffQty;
	}


	/**
	 * <h3>Description:</h3> This method is used to get the shipped qty from the Ship confirmation msg
	 *
	 * @author Infosys Limited
	 * @param strCompPrmLnNo The component prime line no
	 * @param docInput The shipment advice
	 * @return returns the shipped qty in the shipment advice else return zero
	 */
	private int getShippedQtyForCompLn(final String strCompPrmLnNo, final Document docInput) {

		// logging the method entry
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":getShippedQtyForCompLn:entry:Input:Component Line no=" + strCompPrmLnNo
					+ ":The ship conf document=" + GCXMLUtil.getXMLString(docInput));
		}

		// prepare the xpath to fetch the shipped qty
		final StringBuilder stbShpLnXpth = new StringBuilder("/Shipment/ShipmentLines/ShipmentLine[ @PrimeLineNo='");
		stbShpLnXpth.append(strCompPrmLnNo);
		stbShpLnXpth.append("']");
		final String strShpLnXpth = stbShpLnXpth.toString();

		// logging the output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getShippedQtyForCompLn:The Xpath for fetching Shipment Line for the component="
					+ strShpLnXpth);
		}

		// get the shipment line
		final Element eleShpLn = GCXMLUtil.getElementByXPath(docInput, strShpLnXpth);

		// the shipped qty to be returned
		final int iShippedQty;

		// check if the shipment line is present
		if (eleShpLn == null) {

			// default to zero
			iShippedQty = 0;
		} else {

			// fetch the shipped qty
			final String strShippedQty = eleShpLn.getAttribute(GCConstants.QUANTITY);

			// check if the shipped qty is numeric
			if (StringUtils.isNotEmpty(strShippedQty) && StringUtils.isNumeric(strShippedQty)) {

				// parse to get the numeric value
				iShippedQty = Integer.parseInt(strShippedQty);

			} else {

				// default to zero
				iShippedQty = 0;
			}
		}

		// logging the output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getShippedQtyForCompLn:exit:Output=" + iShippedQty);
		}

		// return the shipped qty
		return iShippedQty;
	}


	/**
	 * <h3>Description:</h3> This method is used to get the expected qty from the Ship confirmation
	 * msg
	 *
	 * @author Infosys Limited
	 * @param strCompPrmLnNo The component prime line no
	 * @param docInput The shipment advice
	 * @return returns the expected qty in the shipment advice else return zero
	 */
	private int getExpectedQtyForCompLn(final String strCompPrmLnNo, final Document docInput) {

		// logging the method entry
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":getExpectedQtyForCompLn:entry:Input:Component Line no=" + strCompPrmLnNo
					+ ":The ship conf document=" + GCXMLUtil.getXMLString(docInput));
		}

		// prepare the xpath to fetch the expected qty
		final StringBuilder stbShpLnExtnXpth = new StringBuilder("/Shipment/ShipmentLines/ShipmentLine[ @PrimeLineNo='");
		stbShpLnExtnXpth.append(strCompPrmLnNo);
		stbShpLnExtnXpth.append("']/Extn");
		final String strShpLnExtnXpth = stbShpLnExtnXpth.toString();

		// logging the output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getExpectedQtyForCompLn:The Xpath for fetching Shipment Line Extn for the component="
					+ strShpLnExtnXpth);
		}

		// get the shipment line
		final Element eleShpLnExtn = GCXMLUtil.getElementByXPath(docInput, strShpLnExtnXpth);

		// the shipped qty to be returned
		final int iExpectedQty;

		// check if the shipment line is present
		if (eleShpLnExtn == null) {

			// default to zero
			iExpectedQty = 0;
		} else {

			// fetch the expected qty
			final String strShippedQty = eleShpLnExtn.getAttribute(GCConstants.QUANTITY);

			// check if the expected qty is numeric
			if (StringUtils.isNotEmpty(strShippedQty) && StringUtils.isNumeric(strShippedQty)) {

				// parse to get the numeric value
				iExpectedQty = Integer.parseInt(strShippedQty);

			} else {

				// default to zero
				iExpectedQty = 0;
			}
		}

		// logging the output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getExpectedQtyForCompLn:exit:Output=" + iExpectedQty);
		}

		// return the expected qty
		return iExpectedQty;
	}


	/**
	 * <h3>Description:</h3>This method is used to get the kit qty for the input order line
	 *
	 * @author Infosys Limited
	 * @param strCompPrmLnNo The component prime line no
	 * @param docCompleteOrderDtlsOut The complete order details
	 * @return the kit qty for the component if present else returns 1
	 */
	private int getKitQtyForCompLine(final String strCompPrmLnNo, final Document docCompleteOrderDtlsOut) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getKitQtyForCompLine:entry:Input:Component Prime Line No=" + strCompPrmLnNo);
		}

		// prepare the xpath
		final StringBuilder stbCompOrdLnXpth = new StringBuilder("/Order/OrderLines/OrderLine[@PrimeLineNo='");
		stbCompOrdLnXpth.append(strCompPrmLnNo);
		stbCompOrdLnXpth.append("']");
		final String strCompOrdLnXpth = stbCompOrdLnXpth.toString();

		// logging the xpath
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":isKitNotCompletelyFulfilled:Xpath for component line=" + strCompOrdLnXpth);
		}

		// fetch the component orderline
		final Element eleCompOrdLn = GCXMLUtil.getElementByXPath(docCompleteOrderDtlsOut, strCompOrdLnXpth);

		// the return variable
		final int iKitQty;

		// check for null
		if (eleCompOrdLn == null) {

			// default to 1
			iKitQty = 1;
		} else {

			// fetch the kit qty
			final String strKitQty = eleCompOrdLn.getAttribute(GCConstants.KIT_QUANTITY);

			// check if the kit qty is not empty
			if (StringUtils.isNotEmpty(strKitQty) && StringUtils.isNumeric(strKitQty)) {

				iKitQty = Integer.parseInt(strKitQty);
			} else {

				// default to 1
				iKitQty = 1;
			}
		}

		// logging the method output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getKitQtyForCompLine:exit:Output=" + iKitQty);
		}

		// return the kit qty
		return iKitQty;
	}


	/**
	 * <h3>Description:<h3>This method is used to get the Kit Parent and line details for the lines in
	 * Shipment Confirmation message
	 *
	 * @author Infosys Limited
	 * @param docInput The shipment confirmation message
	 * @param docCompleteOrderDtlsOut : The complete order details
	 * @return The map with the kit parent as the key and component details as the value
	 */
	private Map<String, Set<String>> getKitParentInShpConfirmationMsg(final Document docInput,
			final Document docCompleteOrderDtlsOut) {

		// log the method entry
		LOGGER.debug(CLASS_NAME + ":getKitParentInShpConfirmationMsg:entry");

		// get the shipment lines
		final List listShpLns = GCXMLUtil.getElementListByXpath(docInput, "/Shipment/ShipmentLines/ShipmentLine");

		//the map to hold the kit parent details
		final Map<String, Set<String>> mapKitParentDtls = new HashMap<String, Set<String>>();

		Element eleShpLn;
		String strShpLnPrmLnNo;
		String strKitParentPrmLnNo;
		Set<String> setKitCompDtls;
		Set<String> setExistingKitDtls;

		// iterate tthrough the shipment lines
		for (Object objShpLn : listShpLns) {

			// typecast and fetch the shipmentLine element
			eleShpLn = (Element) objShpLn;

			// fetch the prime line no
			strShpLnPrmLnNo = eleShpLn.getAttribute(GCConstants.PRIME_LINE_NO);

			//fetch the parent component details
			strKitParentPrmLnNo = getKitParentForComponent(strShpLnPrmLnNo, docCompleteOrderDtlsOut);

			//check if the parent exist
			if( StringUtils.isNotBlank(strKitParentPrmLnNo )){

				//check if the map already has the parent details
				setExistingKitDtls = mapKitParentDtls.get(strKitParentPrmLnNo);
				if (setExistingKitDtls == null) {

					// fetch the kit component details
					setKitCompDtls = getKitComponentDtls(strKitParentPrmLnNo, docCompleteOrderDtlsOut);

					// add the detail to the map
					mapKitParentDtls.put(strKitParentPrmLnNo, setKitCompDtls);
				}
			}
		}

		// log the method exit
		LOGGER.debug(CLASS_NAME + ":getKitParentInShpConfirmationMsg:exit:Output=" + mapKitParentDtls);

		// return the map
		return mapKitParentDtls;
	}


	/**
	 * <h3>Description:<h3>This method is used to get the kit component details
	 *
	 * @author Infosys Limited
	 * @param strKitParentPrmLnNo The kit parent prime line number
	 * @param docCompleteOrderDtlsOut The complete order details
	 * @return The set of component prime line number
	 */
	private Set<String> getKitComponentDtls(final String strKitParentPrmLnNo, final Document docCompleteOrderDtlsOut) {

		// log the method entry
		LOGGER.debug(CLASS_NAME + ":getKitComponentDtls:entry");

		// forming the xpath
		final StringBuilder stbKitCompXPth = new StringBuilder("/Order/OrderLines/OrderLine[./BundleParentLine/@PrimeLineNo='");
		stbKitCompXPth.append( strKitParentPrmLnNo);
		stbKitCompXPth.append( "']");
		final String strKitCompXpth = stbKitCompXPth.toString();

		// logging the xpath
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getKitComponentDtls:The xpath for fetching kti components=" + strKitCompXpth);
		}

		//get the kit components for the input parent line
		final List listKitComponents = GCXMLUtil.getElementListByXpath(docCompleteOrderDtlsOut, strKitCompXpth);

		// the return set
		final Set<String> setKitComponents = new HashSet<String>();

		Element eleOrdLn;
		String strKitCompPrmLnNo;

		// iterate through each of the kit component orderlines
		for (Object objKitCompOrdLn : listKitComponents) {

			// type cast to fetch the order line element
			eleOrdLn = (Element) objKitCompOrdLn;

			// fetch the Prime Line no
			strKitCompPrmLnNo = eleOrdLn.getAttribute(GCConstants.PRIME_LINE_NO);

			// add the details to the set
			setKitComponents.add(strKitCompPrmLnNo);
		}

		// log the method exit
		LOGGER.debug(CLASS_NAME + ":getKitComponentDtls:exit:Output=" + setKitComponents);

		// return the set of kit component details
		return setKitComponents;
	}


	/**
	 * <h3>Description:</h3>This method is used to fetch the kit parent for the input component.
	 *
	 * @author Infosys Limited
	 * @param strShpLnPrmLnNo The prime line number
	 * @param docCompleteOrderDtlsOut The complete Order details
	 * @return The prime line number of the parent order line for the input component prime Line
	 *         number if present else returns null
	 */
	private String getKitParentForComponent(final String strShpLnPrmLnNo, final Document docCompleteOrderDtlsOut) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getKitParentForComponent:entry:Input prime Line number=" + strShpLnPrmLnNo);
		}

		// create the xpath for fetching the parent OrderLine
		final StringBuilder stbKitParentForCompXpth = new StringBuilder("/Order/OrderLines/OrderLine[@PrimeLineNo='");
		stbKitParentForCompXpth.append(strShpLnPrmLnNo);
		stbKitParentForCompXpth.append("']/BundleParentLine");
		final String strKitParentForCompXpth = stbKitParentForCompXpth.toString();

		// logging the xpath
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getKitParentForComponent:The xpath to fetch the kit parent="
					+ strKitParentForCompXpth);
		}

		// get the BundleParent element
		final Element eleBndlParentLine = GCXMLUtil.getElementByXPath(docCompleteOrderDtlsOut, strKitParentForCompXpth);

		// the return parent prime ln no;
		final String strKitParentPrmLnNo;

		// fetch the parent prime line number
		if (eleBndlParentLine != null) {

			// fetch the bundle parent prime line no
			strKitParentPrmLnNo = eleBndlParentLine.getAttribute(GCConstants.PRIME_LINE_NO);

		} else {

			// default value set to null
			strKitParentPrmLnNo = null;
		}

		// log the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getKitParentForComponent:exit:Output kit parent Prime line number="
					+ strKitParentPrmLnNo);
		}

		// return the parent prime line number
		return strKitParentPrmLnNo;
	}


	/**
	 * <h3>Description:</h3> This method is used to getCompleteOrderDetails output
	 *
	 * @author Infosys Limited
	 * @param yfsEnv The YFS environment reference
	 * @param docInput The input document
	 * @return The Complete order details output
	 */
	private Document getCompleteOrderDetails(final YFSEnvironment yfsEnv, final Map<String, String> mapOrdDtls) {

		// logging the method entry
		LOGGER.debug(CLASS_NAME + ":getCompleteOrderDetails:entry");

		// Prepare the getCompleteOrderDetails input input
		final Document docGetCompOrdDtlsIn = prepNewGetOrderIn(mapOrdDtls);

		// getCompleteOrderDetails output
		final Document docGetCompleteOrdDtls = getCompleteOrderDtls(yfsEnv, docGetCompOrdDtlsIn);

		// logging the method exit
		LOGGER.debug(CLASS_NAME + ":getCompleteOrderDetails:exit");

		// return the getCompleteOrderDetails output
		return docGetCompleteOrdDtls;
	}


	/**
	 *
	 * <h3>Description:</h3>This method is used to confirm the shipment
	 *
	 * @author Infosys Limited
	 * @param yfsEnv The yfsenvironment reference
	 * @param docInput The input document
	 * @return The input document
	 * @throws GCException
	 */
	public Document confirmShipment(final YFSEnvironment yfsEnv, final Document docInput) throws GCException {

		// log the method entry
		LOGGER.info(CLASS_NAME + ":confirmShipment:entry");

		// log the method input
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":confirmShipment:Input=" + GCXMLUtil.getXMLString(docInput));
		}

		// flag to indicate shipAlone item
		final boolean bShpAlone;

		// the complete order details
		final Document docGetCompleteOrdDtls;

		//flag to indicate if the shipalone shipment was processed
		final boolean bIsShipAloneShpProcessed;
		docGetCompOrdDtlsIn = prepGetOrderIn(docInput);

		// flag to indicate that so is being processed
		final boolean bIsSOAndShpLnExist = isSOAndShpLnExist(docInput);

		// check if SO and shipment exist
		if (bIsSOAndShpLnExist) {

			// get the Order details for the shipment
			docGetCompleteOrdDtls = getCompleteOrderDtls(yfsEnv, docGetCompOrdDtlsIn);

			//check ifShipAlone item
			if( isShipAloneItem(docInput, docGetCompleteOrdDtls)){

				// flag for ship alone items
				bShpAlone = true;

				// process the ship alone item if shipment exist
				bIsShipAloneShpProcessed=processShipAloneItemIfShipmentExists(yfsEnv, docInput, docGetCompleteOrdDtls);
			}else{

				bShpAlone = false;
				bIsShipAloneShpProcessed = false;
			}
		} else {

			// assign default values to the flag
			docGetCompleteOrdDtls = null;
			bShpAlone = false;
			bIsShipAloneShpProcessed = false;
		}

		// process the ship alone line if ship alone item and shipment exists
		if (!bIsShipAloneShpProcessed) {

			// invoke the legacy logic to process the shipment
			processShipmentConfirmation(yfsEnv, docInput);

			// process non ship alone SO shipments to add any ship alone shipments related to it
			if (bIsSOAndShpLnExist && !bShpAlone) {

				// get the Order details for the shipment
				final Document docCurrGetCompleteOrdDtls = getCompleteOrderDtls(yfsEnv, docGetCompOrdDtlsIn);


				// check if any shipalone shipment exist for the created shipment if yes merge it
				mergeAnyShipAloneShipment(yfsEnv, docInput, docCurrGetCompleteOrdDtls, docGetCompleteOrdDtls);
			}
		}

		// post process the shipments to check if the shipment needs to unconfirmed if partial for SO
		if (bIsSOAndShpLnExist) {

			postProcessOrderForUnconfirm(yfsEnv, docInput);
		}

		// log the method exit
		LOGGER.info(CLASS_NAME + ":confirmShipment:exit");

		return docInput;
	}
	/**
	 * <h3>Description:</h3>This method is used to post process the input document for unconfirm the
	 * partial ship alone shipments
	 *
	 * @author Infosys Limited
	 * @param yfsEnv The yfs environment reference
	 * @param docInput The shipment advice
	 */
	private void postProcessOrderForUnconfirm(final YFSEnvironment yfsEnv, final Document docInput) {

		// log the input
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":postProcessOrderForUnconfirm:entry:Input=" + GCXMLUtil.getXMLString(docInput));
		}

		// get the Order details for the shipment
		final Document docGetCompleteOrdDtls = getCompleteOrderDtls(yfsEnv, docGetCompOrdDtlsIn);

		// get set of shipments which are confirmed and not invoiced and has partial ship alone items
		final Set<String> setUnconfirmShpKeys = getSetOfConfInvPartialShpAloneItems(docGetCompleteOrdDtls);

		//unconfirm the shipments
		if (!setUnconfirmShpKeys.isEmpty()) {

			// unconfirm the shipment
			unconfirmShipments(yfsEnv, setUnconfirmShpKeys);

			// delete the task q entries for invoice generation
			deleteTaskQForInvoiceForUnconfShp(yfsEnv, setUnconfirmShpKeys);
		}

		// log the exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":postProcessOrderForUnconfirm:exit");
		}

	}


	/**
	 * <h3>Description:</h3>This method is used to delete the task Q entries generated for the
	 * unconfirmed shipments
	 *
	 * @author Infosys Limited
	 * @param yfsEnv YFS environment environment
	 * @param setUnconfirmShpKeys The set of the unconfirmed shipment keys
	 */
	private void deleteTaskQForInvoiceForUnconfShp(final YFSEnvironment yfsEnv, final Set<String> setUnconfirmShpKeys) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":deleteTaskQForInvoiceForUnconfShp:entry:Input=" + setUnconfirmShpKeys);
		}

		Document docManageTaskQIn;
		Document docManageTaskQOut;

		// iterate through shipment keys
		for (String strShpKey : setUnconfirmShpKeys) {

			// generate the input for the manageTask q api input
			docManageTaskQIn = prepManageTaskQIn(strShpKey);

			// handle and suppress the exception if any
			try {

				// invoke the manage task q
				docManageTaskQOut =
						GCCommonUtil.invokeService(yfsEnv, "GCDeleteTaskQ_UnconfirmShipmentInvoice_svc", docManageTaskQIn);

				// log the output
				if (LOGGER.isDebugEnabled()) {

					LOGGER.debug(CLASS_NAME + ":deleteTaskQForInvoiceForUnconfShp:Output of manageTaskQueue api output="
							+ GCXMLUtil.getXMLString(docManageTaskQOut));
				}
			} catch (final Exception excep) {

				// suppress the exception
				LOGGER
				.warn(
						CLASS_NAME
						+ ":deleteTaskQForInvoiceForUnconfShp:Suppressed the exception thrown during manage task queue invocation to remove the task q record for shipment invoice",
						excep);
			}
		}

		// log the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":deleteTaskQForInvoiceForUnconfShp:exit");
		}
	}

	/**
	 * <h3>Description:</h3>This method is used to prepare the manageTaskQueue api input
	 *
	 * @author Infosys Limited
	 * @param strShpKey The shipment key
	 * @return document to be used for manageTask q api input
	 */
	private Document prepManageTaskQIn(final String strShpKey) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":prepManageTaskQIn:entry:Input=" + strShpKey);
		}

		// create the document
		final Document docManageTaskQIn = GCXMLUtil.createDocument(GCConstants.TASK_QUEUE);

		// populate the data key
		final Element eleTaskQueue = docManageTaskQIn.getDocumentElement();
		eleTaskQueue.setAttribute(GCConstants.DATA_KEY, strShpKey);

		// log the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":prepManageTaskQIn:exit:Output=" + GCXMLUtil.getXMLString(docManageTaskQIn));
		}

		// return the document for manageTaskQueue api
		return docManageTaskQIn;
	}
	/**
	 * <h3>Description:</h3>This method is used to unconfirm the set of input shipments
	 *
	 * @author Infosys Limited
	 * @param yfsEnv the yfs environment reference
	 * @param setUnconfirmShpKeys The set of shipment keys
	 */
	private void unconfirmShipments(final YFSEnvironment yfsEnv, final Set<String> setUnconfirmShpKeys) {

		// log the input
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":unconfirmShipments:entry:Input=" + setUnconfirmShpKeys);
		}

		Document docUnConfShpIn;
		Document docUnConfShpOut;

		// iterate through each of the shipment key
		for (String strShpKey : setUnconfirmShpKeys) {

			// prepare the unconfirm shipment input
			docUnConfShpIn = prepUnConfAndConfShipmentDoc(strShpKey);

			// invoke unconfirm shipment api
			docUnConfShpOut =
					GCCommonUtil.invokeService(yfsEnv, "GCUnconfirmShipment_NonCompleteShipped_svc", docUnConfShpIn);

			// log the output
			if (LOGGER.isDebugEnabled()) {

				LOGGER.debug(CLASS_NAME + ":unconfirmShipments:Output of unconfirmShipment="
						+ GCXMLUtil.getXMLString(docUnConfShpOut));
			}
		}

		// log the output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":unconfirmShipments:exit");
		}
	}
	/**
	 * <h3>Description:</h3> This method is used to get the set of confirmed non invoice shipments
	 * which have partial ship alone items
	 *
	 * @author Infosys Limited
	 * @param docGetCompleteOrdDtls The complete order details
	 * @return the set of shipment keys which are confirmed and not invoiced and have partial ship
	 *         alone items
	 */
	private Set<String> getSetOfConfInvPartialShpAloneItems(final Document docGetCompleteOrdDtls) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getSetOfConfInvPartialShpAloneItems:entry");
		}

		// get the list of shipped shipments and not invoiced and has kit components
		final List listOfShippedShipments =
				GCXMLUtil.getElementListByXpath(docGetCompleteOrdDtls, "/Order/Shipments/Shipment[@Status='1400']");

		Element eleShipment;
		String strShpKey;

		// set of shipment not fullfilled completly for kits
		final Set<String> setUnConfirmShipKeys = new HashSet<String>();

		// iterate through each fo the shipment and check if it needs to be unconfirmed
		for (Object objShpmnt : listOfShippedShipments) {

			// get the list of the shipment element getListOfShipAloneItemsAlreadyPresent
			eleShipment = (Element) objShpmnt;
			strShpKey = eleShipment.getAttribute( GCConstants.SHIPMENT_KEY);

			// check if all the kit components present in the shipment
			if (!isShipmentCompletelyFulfilled(strShpKey, docGetCompleteOrdDtls)) {

				// add the shipment key to the set
				setUnConfirmShipKeys.add(strShpKey);
			}
		}

		// log the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getSetOfConfInvPartialShpAloneItems:exit:Output=" + setUnConfirmShipKeys);
		}

		// return the shipment keys to be unconfirmed
		return setUnConfirmShipKeys;
	}
	/**
	 * <h3>Description:</h3> This method is used to check if the shipment is completely fulfilled
	 *
	 * @author Infosys Limited
	 * @param strShpKey The shipment key
	 * @param docGetCompleteOrdDtls The complete order details
	 * @return true if the shipment is completely fulfilled else false
	 */
	private boolean isShipmentCompletelyFulfilled(final String strShpKey, final Document docGetCompleteOrdDtls) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":isShipmentCompletelyFulfilled:entry:Input=" + strShpKey);
		}

		final StringBuilder stbXpthBundlShpLn = new StringBuilder("/Order/Shipments/Shipment[@ShipmentKey='");
		stbXpthBundlShpLn.append(strShpKey);
		stbXpthBundlShpLn
		.append("']/ShipmentLines/ShipmentLine[@PrimeLineNo=../../../../OrderLines/OrderLine[@KitCode='BUNDLE' and ./Item/@ProductClass='REGULAR']/@PrimeLineNo]");

		// fetch the list of shipment lines
		final List listShpLn = GCXMLUtil.getElementListByXpath(docGetCompleteOrdDtls, stbXpthBundlShpLn.toString());

		// flag to indicate that the shipment is completely fulfilled
		boolean bIsShipmentCompletelyFulfilled = true;

		// check if any shipment line exist with kits
		if (listShpLn.size() > 0) {

			// fetch the list of item ids in the shipment
			final Set<String> setItemIds = getItemIdSetForShipment(strShpKey, docGetCompleteOrdDtls);

			Element eleShpLn;
			String strPrimeLnNo;
			String strSubLnNo;

			// iterate through each of the shipment line
			for (Object objShpLn : listShpLn) {

				// fetch the shipment line details
				eleShpLn = (Element) objShpLn;
				strPrimeLnNo = eleShpLn.getAttribute( GCConstants.PRIME_LINE_NO);
				strSubLnNo = eleShpLn.getAttribute( GCConstants.SUB_LINE_NO);

				//check if all the components of this bundle are present in the shipment
				if (!isAllComponentOfBundlePresentInShp(strShpKey, strPrimeLnNo, strSubLnNo, docGetCompleteOrdDtls, setItemIds)) {

					// set the value to false;
					bIsShipmentCompletelyFulfilled = false;
					break;
				}
			}
		}

		// log the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":isShipmentCompletelyFulfilled:exit:Output=" + bIsShipmentCompletelyFulfilled);
		}

		// return the flag
		return bIsShipmentCompletelyFulfilled;
	}
	/**
	 * <h3>Description:</h3>This method is used to get the set of Item ids in the shipment
	 *
	 * @author Infosys Limited
	 * @param strShpKey The shipment key
	 * @param docGetCompleteOrdDtls The complete order details
	 * @return set of item ids in the shipment
	 */
	private Set<String> getItemIdSetForShipment(final String strShpKey, final Document docGetCompleteOrdDtls) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getItemIdSetForShipment:entry:Shipment key=" + strShpKey);
		}

		// fetch the items in the shipment
		final StringBuilder stbItmDtls = new StringBuilder("/Order/Shipments/Shipment[@ShipmentKey='");
		stbItmDtls.append(strShpKey);
		stbItmDtls.append("']/ShipmentLines/ShipmentLine");
		final List listShpLn = GCXMLUtil.getElementListByXpath(docGetCompleteOrdDtls, stbItmDtls.toString());

		Element eleShpLn;
		String strItmId;

		// set of item ids
		final Set<String> setItemIds = new HashSet<String>();

		// iterate through each of the shipment line and get the set of item ids
		for (Object objShpLn : listShpLn) {

			// add the item ids to the set
			eleShpLn = (Element) objShpLn;
			strItmId = eleShpLn.getAttribute(GCConstants.ITEM_ID);
			setItemIds.add(strItmId);
		}

		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getItemIdSetForShipment:exit:Output=" + setItemIds);
		}

		// return the set
		return setItemIds;
	}

	/**
	 * <h3>Description:</h3> This method is used to check if all components of the input primeline no
	 * and subline number which corresponds to a bundle is present in the input shipment
	 *
	 * @author Infosys Limited
	 * @param strShpKey The shipment key
	 * @param strPrimeLnNo The prime line number corresponding to the bundle
	 * @param strSubLnNo the subline number corresponding to the bundle
	 * @param docGetCompleteOrdDtls The complet order details
	 * @param setItemsInShipment The set of item ids present in the set
	 * @return true is all the componenets of the bundle are present in the input shipment else false;
	 */
	private boolean isAllComponentOfBundlePresentInShp(final String strShpKey, final String strPrimeLnNo,
			final String strSubLnNo, final Document docGetCompleteOrdDtls, final Set<String> setItemsInShipment) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":isAllComponentOfBundlePresentInShp:entry:Input:Shipment key=" + strShpKey
					+ ":PrimeLine No=" + strPrimeLnNo + ":Subline number=" + strSubLnNo);
		}

		// fetch all the componenets of the shipment line
		final Set<String> setComponentInShpLn =
				getAllComponentOfBundlePresentInShpLn(strShpKey, strPrimeLnNo, strSubLnNo, docGetCompleteOrdDtls);

		//flag to indicate if all the componenets are present in the shipment
		boolean bAreAllComponentsPresentInShpment = true;

		// check if all the components present in shipment
		for (String strComponentShpLn : setComponentInShpLn) {

			//check if the compoent is present
			if( !setItemsInShipment.contains( strComponentShpLn)){

				// if the component is not present in the shipment
				bAreAllComponentsPresentInShpment = false;
				break;
			}
		}

		// log the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":isAllComponentOfBundlePresentInShp:exit:Output=" + bAreAllComponentsPresentInShpment);
		}

		// return the flag
		return bAreAllComponentsPresentInShpment;
	}
	/**
	 * <h3>Description:</h3>This method is used to get the set of all the components of the bundle
	 * shipment line
	 *
	 * @author Infosys Limited
	 * @param strShpKey shipment key
	 * @param strPrimeLnNo The prime line number
	 * @param strSubLnNo The subline number
	 * @param docGetCompleteOrdDtls The complete order details
	 * @return the set of all the component item ids
	 */
	private Set<String> getAllComponentOfBundlePresentInShpLn(final String strShpKey, final String strPrimeLnNo,
			final String strSubLnNo, final Document docGetCompleteOrdDtls) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getAllComponentOfBundlePresentInShpLn:entry:Input:Shipment Key=" + strShpKey
					+ ": Prime Line No=" + strPrimeLnNo + ":Sub Line no=" + strSubLnNo);;
		}

		// fetch the list of components
		final StringBuilder stbXpthCompItmDtl =
				new StringBuilder("/Order/OrderLines/OrderLine[./BundleParentLine/@OrderLineKey=../OrderLine[@PrimeLineNo='");
		stbXpthCompItmDtl.append(strPrimeLnNo);
		stbXpthCompItmDtl.append("' and @SubLineNo='");
		stbXpthCompItmDtl.append(strSubLnNo);
		stbXpthCompItmDtl.append("']/@OrderLineKey]/ItemDetails");
		final List listComponent = GCXMLUtil.getElementListByXpath(docGetCompleteOrdDtls, stbXpthCompItmDtl.toString());

		// get the lsit of component item ids
		final Set<String> setComponentId = getComponentIdSet(listComponent);

		// log the output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getAllComponentOfBundlePresentInShpLn:exit:Output=" + setComponentId);
		}

		// return the set
		return setComponentId;
	}
	/**
	 * <h3>Description:</h3>This method is used to fetch the set of item ids from the input list of
	 * Item details element
	 *
	 * @author Infosys Limited
	 * @param listComponent The list of component item details
	 * @return the set of item ids
	 */
	private Set<String> getComponentIdSet(final List listComponent) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":getComponentIdSet:entry:Input=" + listComponent);
		}

		Element eleItemDtls;
		String strItemId;

		// the set to hold the item ids
		final Set<String> setCompItemIds = new HashSet<String>();

		// iterate through the list of components
		for (Object objCompItmDtls : listComponent) {

			// fetch the item ids
			eleItemDtls = (Element) objCompItmDtls;
			strItemId = eleItemDtls.getAttribute(GCConstants.ITEM_ID);

			// add the item ids to the set
			setCompItemIds.add(strItemId);
		}

		// log the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getComponentIdSet:exit:Output=" + setCompItemIds);
		}

		// return the set of item ids
		return setCompItemIds;
	}

	/**
	 * <h3>Description:</h3>This method is used to merge any ship alone shipments associated with the
	 * shipment of the shipment advice
	 *
	 * @author Infosys Limited
	 * @param yfsEnv The YFS Environment reference
	 * @param docInput The input document
	 * @param docGetCompleteOrdDtls The complete order details
	 * @param docOldOrdDetails The order details prior to confirm shipment
	 */
	private void mergeAnyShipAloneShipment(final YFSEnvironment yfsEnv, final Document docInput,
			final Document docGetCompleteOrdDtls, final Document docOldOrdDetails) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":mergeAnyShipAloneShipment:entry");
		}

		// get the current list of shipment
		final List listOldShipment = GCXMLUtil.getElementListByXpath(docOldOrdDetails, "/Order/Shipments/Shipment");
		final Set<String> setOldShpKey = getShipmentKeySet(listOldShipment);

		// check if this not the first shipment
		if (setOldShpKey.size() > 0) {

			// get the current shipment key
			final String strShpKey = getNewShipmentKey(docGetCompleteOrdDtls, setOldShpKey);

			// get the eligible ship Alone shipments to be merged
			final Set<String> setShpKeyForMerge =
					getEligibleShipAloneItemsShpKeysForMerge(docInput, docGetCompleteOrdDtls, strShpKey);

			// check if any shipment to merge
			if (!setShpKeyForMerge.isEmpty()) {

				// merge these shipments to the current shipment
				mergeEligibleShipments(yfsEnv, strShpKey, setShpKeyForMerge, docGetCompleteOrdDtls);
			}
		}

		// log the exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":mergeAnyShipAloneShipment:exit");
		}
	}
	/**
	 * <h3>Description:</h3>This method is used to merge the set of shipments-setShpKeyForMerge from
	 * the input shipment to the shipment with shipment key strShpKey
	 *
	 * @author Infosys Limited
	 * @param yfsEnv The yfs environment reference
	 * @param strShpKey The shipment key
	 * @param setShpKeyForMerge The set of shipment key to merge
	 * @param docGetCompleteOrdDtls The complete Order details
	 */
	private void mergeEligibleShipments(final YFSEnvironment yfsEnv, final String strShpKey,
			final Set<String> setShpKeyForMerge, final Document docGetCompleteOrdDtls) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":mergeEligibleShipments:entry:Input:Current Shipment Key=" + strShpKey
					+ ":set to be merged=" + setShpKeyForMerge);
		}

		// prepare the changeShipment header
		final Document docChangeShipmentIn = prepChangeShipmentIn(strShpKey);

		//prepare the unconfirmShipment input
		final Document docUnconfirmShipmentIn = GCXMLUtil.createDocument( GCConstants.SHIPMENT);
		final Element eleShipment = docUnconfirmShipmentIn.getDocumentElement();

		//unconfirm and update the change shipment input
		for (String strUnConfShpKey : setShpKeyForMerge) {

			// update the unconfirm shipment document
			eleShipment.setAttribute(GCConstants.SHIPMENT_KEY, strUnConfShpKey);

			// uconfirm and update the changeShipment api input
			unconfirmAndCancelShipmentIn(yfsEnv, docUnconfirmShipmentIn, docChangeShipmentIn, strUnConfShpKey, docGetCompleteOrdDtls);
		}

		// invoke unconfirm , change shipment and confirm the shipment
		unconfirmChangeAndConfirmShipment(yfsEnv, docChangeShipmentIn, strShpKey);

		// log the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":mergeEligibleShipments:exit");
		}
	}
	/**
	 * <h3>Description:</h3>This method is used to unconfirm the shipment. Update the shipment and
	 * then confirm the shipment
	 *
	 * @author Infosys Limited
	 * @param yfsEnv The yfs environment reference
	 * @param docChangeShipmentIn The change shipment api input
	 * @param strShpKey The shipment key
	 */
	private void unconfirmChangeAndConfirmShipment(final YFSEnvironment yfsEnv, final Document docChangeShipmentIn,
			final String strShpKey) {

		// log the input
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":unconfirmChangeAndConfirmShipment:entry:Input:Shipment key=" + strShpKey
					+ ": changeShipment api input=" + GCXMLUtil.getXMLString(docChangeShipmentIn));
		}

		//get the document for unconfirming and confirming
		final Document docShipIn = prepUnConfAndConfShipmentDoc( strShpKey);

		//unconfirm the shipment
		final Document docUnConfShpOut =
				GCCommonUtil.invokeService(yfsEnv, "GCUnConfirmShipment_UpdateShipAlone_svc", docShipIn);

		//log the output
		if( LOGGER.isDebugEnabled()){

			LOGGER.debug( CLASS_NAME + ":unconfirmChangeAndConfirmShipment:Unconfirm SHipment output="+ GCXMLUtil.getXMLString(docUnConfShpOut));
		}

		// update the shipment
		final Document docChgShpOut =
				GCCommonUtil.invokeService(yfsEnv, "GCChangeShipment_UpdateShipAlone_svc", docChangeShipmentIn);

		// log the output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":unconfirmChangeAndConfirmShipment:ChangeShipment output="
					+ GCXMLUtil.getXMLString(docChgShpOut));
		}

		// unconfirm the shipment
		final Document docConfOut = GCCommonUtil.invokeService(yfsEnv, "GCConfirmhipment_UpdateShipAlone_svc", docShipIn);

		// log the output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":unconfirmChangeAndConfirmShipment:Confirm SHipment output="
					+ GCXMLUtil.getXMLString(docConfOut));
			LOGGER.debug(CLASS_NAME + ":unconfirmChangeAndConfirmShipment:exit");
		}
	}

	/**
	 * <h3>Description:</h3> This method is used to prepare the input document for unconfirm shipment
	 * and confirm shipment
	 *
	 * @author Infosys Limited
	 * @param strShpKey The shipment key
	 * @return The document used for unconfirm and confirm shipment
	 */
	private Document prepUnConfAndConfShipmentDoc(final String strShpKey) {

		// log the method input
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":prepUnConfAndConfShipmentDoc:entry:Input=" + strShpKey);
		}

		// prepare the unconfirm and confirm shipment input
		final Document docUnconfAnConfIn = GCXMLUtil.createDocument(GCConstants.SHIPMENT);
		final Element eleShipment = docUnconfAnConfIn.getDocumentElement();
		eleShipment.setAttribute(GCConstants.SHIPMENT_KEY, strShpKey);

		// log the method output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":prepUnConfAndConfShipmentDoc:exit:Output="
					+ GCXMLUtil.getXMLString(docUnconfAnConfIn));
		}

		// return the document created
		return docUnconfAnConfIn;
	}
	/**
	 * <h3>Description:</h3> This method is used to unconfirm the shipment and cancel the shipment.
	 * Also update the change Shipment input document
	 *
	 * @author Infosys Limited
	 * @param yfsEnv The YFS environment reference
	 * @param docUnconfirmAndCancelShipmentIn The unconfirm and cancel shipment input
	 * @param docChangeShipmentIn changeShipment api input document to be updated
	 * @param docGetCompleteOrdDtls The complete order details
	 * @param strUnConfShpKey The shipment key of the shipment being unconfirmed
	 */
	private void unconfirmAndCancelShipmentIn(final YFSEnvironment yfsEnv,
			final Document docUnconfirmAndCancelShipmentIn,
			final Document docChangeShipmentIn, final String strUnConfShpKey,
			final Document docGetCompleteOrdDtls) {

		// log the method input
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":unconfirmAndCancelShipmentIn:entry:Input:Unconfirm shipment input="
					+ GCXMLUtil.getXMLString(docUnconfirmAndCancelShipmentIn) + ":ChangeShipment input="
					+ GCXMLUtil.getXMLString(docChangeShipmentIn));
		}

		try{

			// unconfirm the shipment
			final Document docUnConfirmShipmentOut =
					GCCommonUtil.invokeService(yfsEnv, "GCUnconfirmShipment_ShipAlone_svc", docUnconfirmAndCancelShipmentIn);

			// log the unconfirm shipment output
			if (LOGGER.isDebugEnabled()) {

				LOGGER.debug(CLASS_NAME + ":unconfirmAndCancelShipmentIn: Output of unconfirm shipment ="
						+ GCXMLUtil.getXMLString(docUnConfirmShipmentOut));
			}
		} catch (final Exception excep) {

			// handling any exception thrown
			LOGGER.warn(CLASS_NAME + ":unconfirmAndCancelShipmentIn: Unable to unconfirm the shipment:"
					+ GCXMLUtil.getXMLString(docUnconfirmAndCancelShipmentIn) + ":Exception thrown=" + excep.getMessage());
		}

		// add the unconfirmed shipment details onto the shipment
		updateChangeShipmentInWithShipmentDtls(docChangeShipmentIn, strUnConfShpKey, docGetCompleteOrdDtls);
		// updateChangeShipmentInWithUnconfirmDtls(docChangeShipmentIn, docUnConfirmShipmentOut);

		// cancel the shipment
		final Document docCancelShipmentOut =
				GCCommonUtil.invokeService(yfsEnv, "GCCancelShipment_ShipAlone_svc", docUnconfirmAndCancelShipmentIn);

		// log the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":unconfirmAndCancelShipmentIn:Cancel Shipment output="
					+ GCXMLUtil.getXMLString(docCancelShipmentOut));
			LOGGER.debug(CLASS_NAME + ":unconfirmAndCancelShipmentIn:exit:Updated changeShipment api input="
					+ GCXMLUtil.getXMLString(docChangeShipmentIn));
		}
	}


	/**
	 * <h3>Description:</h3>This method is used to update the change shipment details input witht he
	 * shipment details
	 *
	 * @author Infosys Limited
	 * @param docChangeShipmentIn The changeShipment api input
	 * @param strUnConfShpKey The shipment being cancelled
	 * @param docGetCompleteOrdDtls The complete order details
	 */
	private void updateChangeShipmentInWithShipmentDtls(final Document docChangeShipmentIn, final String strUnConfShpKey,
			final Document docGetCompleteOrdDtls) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":updateChangeShipmentInWithShipmentDtls:entry:Input:Shipment key=" + strUnConfShpKey);
		}

		// copy the Shipment Line details
		final StringBuilder stbShpLnXpth = new StringBuilder("/Order/Shipments/Shipment[@ShipmentKey='");
		stbShpLnXpth.append( strUnConfShpKey);
		stbShpLnXpth.append( "']/ShipmentLines/ShipmentLine");
		final List listShpLn = GCXMLUtil.getElementListByXpath(docGetCompleteOrdDtls, stbShpLnXpth.toString());
		final Node ndShipmentLines = docChangeShipmentIn.getElementsByTagName(GCConstants.SHIPMENT_LINES).item(0);

		Element eleShpLn;
		Element eleImpShpLn;

		//copy the shipmentline
		for( Object objShpLn:listShpLn){

			eleShpLn = (Element)objShpLn;
			eleImpShpLn = (Element) docChangeShipmentIn.importNode(eleShpLn, true);
			eleImpShpLn.removeAttribute( GCConstants.SHIPMENT_KEY);
			ndShipmentLines.appendChild(eleImpShpLn);
		}

		// copy the container details
		final StringBuilder stbContainerXpth = new StringBuilder("/Order/Shipments/Shipment[@ShipmentKey='");
		stbContainerXpth.append( strUnConfShpKey);
		stbContainerXpth.append( "']/Containers/Container");
		final List listContainers = GCXMLUtil.getElementListByXpath(docGetCompleteOrdDtls, stbContainerXpth.toString());
		final Node ndContainers = docChangeShipmentIn.getElementsByTagName(GCConstants.CONTAINERS).item(0);

		Element eleContainer;
		Node ndImpContainer;

		// copy the container
		for (Object objContainer : listContainers) {

			eleContainer = (Element) objContainer;
			ndImpContainer = docChangeShipmentIn.importNode(eleContainer, true);
			ndContainers.appendChild(ndImpContainer);
		}

		// log the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":updateChangeShipmentInWithShipmentDtls:exit:Output="
					+ GCXMLUtil.getXMLString(docChangeShipmentIn));
		}
	}


	/**
	 * <h3>Description:</h3>This method is used update the changeShipment api input with the
	 * unconfirmed shipment details
	 *
	 * @author Infosys Limited
	 * @param docChangeShipmentIn The change shipment api input
	 * @param docUnConfirmShipmentOut The unconfirmshipment output
	 */
	private void updateChangeShipmentInWithUnconfirmDtls(final Document docChangeShipmentIn,
			final Document docUnConfirmShipmentOut) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":updateChangeShipmentInWithUnconfirmDtls:entry");
		}

		// copy the Shipment Line details
		final Element eleShipmentLine =
				GCXMLUtil.getElementByXPath(docUnConfirmShipmentOut, "/Shipment/ShipmentLines/ShipmentLine");
		final Node ndImpShpLn = docChangeShipmentIn.importNode(eleShipmentLine, true);
		final Node ndShipmentLines = docChangeShipmentIn.getElementsByTagName(GCConstants.SHIPMENT_LINES).item(0);
		ndShipmentLines.appendChild(ndImpShpLn);

		// copy the container details
		final Element eleContainer = GCXMLUtil.getElementByXPath(docUnConfirmShipmentOut, "/Shipment/Containers/Container");
		final Node ndImpContainer = docChangeShipmentIn.importNode(eleContainer, true);
		final Node ndContainers = docChangeShipmentIn.getElementsByTagName(GCConstants.CONTAINERS).item(0);
		ndContainers.appendChild(ndImpContainer);

		// log the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":updateChangeShipmentInWithUnconfirmDtls:exit");
		}
	}
	/**
	 * <h3>Description:</h3>This method is used to prepare the header of the changeShipment Input
	 *
	 * @author Infosys Limited
	 * @param strShpKey The shipment key
	 * @return The change shipment input
	 */
	private Document prepChangeShipmentIn(final String strShpKey) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":prepChangeShipmentIn:entry:Input=" + strShpKey);
		}

		// prepare the document for changeShipment api input
		final Document docChangeShipmentIn = GCXMLUtil.createDocument(GCConstants.SHIPMENT);

		// add the shipment key
		final Element eleChangeShipmentInRt = docChangeShipmentIn.getDocumentElement();
		eleChangeShipmentInRt.setAttribute(GCConstants.SHIPMENT_KEY, strShpKey);

		// add the shipment lines and containers element
		final Element eleShipmentLines = docChangeShipmentIn.createElement(GCConstants.SHIPMENT_LINES);
		eleChangeShipmentInRt.appendChild(eleShipmentLines);
		final Element eleContainers = docChangeShipmentIn.createElement(GCConstants.CONTAINERS);
		eleChangeShipmentInRt.appendChild(eleContainers);

		// log the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":prepChangeShipmentIn:exit" + GCXMLUtil.getXMLString(docChangeShipmentIn));
		}

		// return the changeShipment document
		return docChangeShipmentIn;
	}


	/**
	 * <h3>Description:</h3> This method is used to get the new shipment key
	 *
	 * @author Infosys Limited
	 * @param docGetCompleteOrdDtls The current complete order details
	 * @param setOldShpKey The set of old shipments keys
	 * @return The shipment key of the new shipment created
	 */
	private String getNewShipmentKey(final Document docGetCompleteOrdDtls, final Set<String> setOldShpKey) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getNewShipmentKey:entry");
		}

		String strNewShpKey = "";
		final StringBuilder stbXpthNewShp = new StringBuilder("/Order/Shipments/Shipment[ not(");

		boolean bIsNotFirst = false;

		// consruct the xpath
		for (String strOldShpKey : setOldShpKey) {

			if (bIsNotFirst) {

				stbXpthNewShp.append(" or ");

			} else {
				bIsNotFirst = true;
			}
			stbXpthNewShp.append(" @ShipmentKey='");
			stbXpthNewShp.append(strOldShpKey);
			stbXpthNewShp.append("' ");
		}

		stbXpthNewShp.append(")]");

		// get the shipment
		final Element eleNewShp = GCXMLUtil.getElementByXPath(docGetCompleteOrdDtls, stbXpthNewShp.toString());

		// fetch the shipment key
		if (!YFCCommon.isVoid(eleNewShp)) {
			strNewShpKey = eleNewShp.getAttribute(GCConstants.SHIPMENT_KEY);
		}

		// log the exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getNewShipmentKey:exit:Output=" + strNewShpKey);
		}

		// return the new shipment key
		return strNewShpKey;
	}


	/**
	 * <h3>Description:</h3>This method is used to get the set of shipment keys from the input list of
	 * shipments
	 *
	 * @author Infosys Limited
	 * @param listCurrShipment The list of shipment
	 * @return the set of shipment key
	 */
	private Set<String> getShipmentKeySet(final List listCurrShipment) {

		// log the method input
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getShipmentKeySet:entry:Input=");
		}

		Element eleShipment;
		String strShpKey;

		// the set of shipment keys
		final Set<String> setShipmentKey = new HashSet<String>();

		// iterate through the shipments
		for (Object objShipment : listCurrShipment) {

			eleShipment = (Element) objShipment;
			strShpKey = eleShipment.getAttribute(GCConstants.SHIPMENT_KEY);
			setShipmentKey.add(strShpKey);
		}

		// log the method output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getShipmentKeySet:exit:Output=" + setShipmentKey);
		}

		// return the set of shipment key
		return setShipmentKey;
	}
	/**
	 * <h3>Description:</h3> This method is used to get the list shipment keys eligible for merge
	 *
	 * @author Infosys Limited
	 * @param docInput The input document
	 * @param docGetCompleteOrdDtls The complete order details
	 * @param strShpKey The current shipment key
	 * @return the set of eligible shipment keys for merge
	 */
	private Set<String> getEligibleShipAloneItemsShpKeysForMerge(final Document docInput,
			final Document docGetCompleteOrdDtls,
			final String strShpKey) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getEligibleShipAloneItemsShpKeysForMerge:entry");
		}

		// return variable;
		final Set<String> setFilteredShpKey;

		// fetch the parent orderline keys
		final Set<String> setParentOLKeys = getParentKeys(docInput, docGetCompleteOrdDtls);

		if (setParentOLKeys.isEmpty()) {

			// set an empty set
			setFilteredShpKey = setParentOLKeys;
		} else {

			// check if any of the components have ship alone
			final Set<String> setShpAloneOLKeys = getShipAloneItemOLKeys(setParentOLKeys, docGetCompleteOrdDtls);

			// check if any ship alone orderline keys exist
			if (setShpAloneOLKeys.isEmpty()) {

				// set the empty set
				setFilteredShpKey = setShpAloneOLKeys;
			} else {

				// get the list of shipments to be merged
				final Set<String> setEligibleShpKey = getElegibleShipmentKeysForMerge(setShpAloneOLKeys, docGetCompleteOrdDtls);

				// check if any eligible shipment keys exist
				if (setEligibleShpKey.isEmpty()) {

					// return the mepty set
					setFilteredShpKey = setEligibleShpKey;
				} else {

					// filter the items from the current shipment already has the ship alone shipment
					setFilteredShpKey =
							filterShipmentsWhichISAlreadyMerged(setEligibleShpKey, strShpKey, docGetCompleteOrdDtls);
				}
			}
		}

		// log the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getEligibleShipAloneItemsShpKeysForMerge:exit:Output=" + setFilteredShpKey);
		}

		// return the eligible shipment keys for merge
		return setFilteredShpKey;
	}


	/**
	 * <h3>Description:</h3>This method is used to filter the shipment which is already present in the
	 * current shipment
	 *
	 * @author Infosys Limited
	 * @param setEligibleShpKey The set of eligible shipment keys
	 * @param strShpKey The current shipment key
	 * @param docOrdDtls The complete order details
	 * @return The filter set of shipment key
	 */
	private Set<String> filterShipmentsWhichISAlreadyMerged(final Set<String> setEligibleShpKey, final String strShpKey,
			final Document docOrdDtls) {

		// log the input
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":filterShipmentsWhichISAlreadyMerged:entry:Input=" + strShpKey + ":Eligible shpKeys="
					+ setEligibleShpKey);
		}

		// get the ship alone items in the current shipment
		final Set<String> setShpAloneItemsAlreadyPresent = getListOfShipAloneItemsAlreadyPresent(strShpKey, docOrdDtls);

		//the set for filtered shipments
		final Set<String> setFilteredShpKey;

		// check if the ship alone is already empty
		if (!setShpAloneItemsAlreadyPresent.isEmpty()) {

			setFilteredShpKey = new HashSet<String>();
			Element eleShpLn;
			String strItemId;
			final String strXpth1ShpLn = "/Order/Shipments/Shipment[@ShipmentKey='";
			final String strXpth2ShpLn = "']/ShipmentLines/ShipmentLine";
			StringBuilder stbXpthShpLn;

			// iterate through each of the shipment
			for (String strShipmentKey : setEligibleShpKey) {

				//create the xpath
				stbXpthShpLn = new StringBuilder(strXpth1ShpLn);
				stbXpthShpLn.append(strShipmentKey);
				stbXpthShpLn.append(strXpth2ShpLn);

				eleShpLn = GCXMLUtil.getElementByXPath(docOrdDtls, stbXpthShpLn.toString());

				strItemId = eleShpLn.getAttribute(GCConstants.ITEM_ID);

				// check if the item id is present in the shipment
				if (!setShpAloneItemsAlreadyPresent.contains(strItemId)) {

					setFilteredShpKey.add(strShipmentKey);
				}
			}
		}else{

			setFilteredShpKey = setEligibleShpKey;
		}

		// log the output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":filterShipmentsWhichISAlreadyMerged:exit:Output=" + setFilteredShpKey);
		}

		// return the updated set
		return setFilteredShpKey;
	}


	/**
	 * <h3>Description:</h3> This method is used to get the list of ship alone items already present
	 * in the current shipment
	 *
	 * @author Infosys Limited
	 * @param strShpKey The current shipment key
	 * @param docOrdDtls The order details
	 * @return
	 */
	private Set<String> getListOfShipAloneItemsAlreadyPresent(final String strShpKey, final Document docOrdDtls) {

		// log the input
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getListOfShipAloneItemsAlreadyPresent:entry:Input=" + strShpKey);
		}

		//get the shipment line
		final List listCurrShpAloneItems =
				GCXMLUtil.getElementListByXpath(docOrdDtls, "/Order/Shipments/Shipment[@ShipmentKey='" + strShpKey
						+ "']/ShipmentLines/ShipmentLine[ @OrderLineKey=../../../OrderLines/OrderLine[./ItemDetails/Extn/@ExtnIsShipAlone='Y']/@OrderLineKey]");


		// get the list of ship items
		final Set<String> setItemIds = getItemsSetForAlreadyPresentShipAlone(listCurrShpAloneItems);

		// log the output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getListOfShipAloneItemsAlreadyPresent:exit:Output=" + setItemIds);
		}

		// return the list of items already present
		return setItemIds;
	}
	/**
	 * <h3>Description:</h3>This method is used to get the set of items from input list of shipment
	 * lines
	 *
	 * @author Infosys Limited
	 * @param listCurrShpAloneItems The list of ship alone items
	 * @return the set of items from the input list of shipment lines
	 */
	private Set<String> getItemsSetForAlreadyPresentShipAlone(final List listCurrShpAloneItems) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getItemsSetForAlreadyPresentShipAlone:entry");
		}

		Element eleShpLn;
		String strItemId;

		// Set to return
		final Set<String> setShipAloneItem = new HashSet<String>();

		// iterate the list
		for (Object objShpLn : listCurrShpAloneItems) {

			eleShpLn = (Element) objShpLn;
			strItemId = eleShpLn.getAttribute(GCConstants.ITEM_ID);
			setShipAloneItem.add(strItemId);
		}

		// log the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getItemsSetForAlreadyPresentShipAlone:exit:Output=" + setShipAloneItem);
		}

		// return the set of ship alone items
		return setShipAloneItem;
	}


	/**
	 * <h3>Description:</h3>This method is used to fetch the set of shipment keys of ship alone items
	 * eligible for merge
	 *
	 * @author Infosys Limited
	 * @param setShpAloneOLKeys The list of shipment keys corresponding to the ship alone items
	 * @param docGetCompleteOrdDtls The complete order details
	 * @return The set of shipments eligible for merge
	 */
	private Set<String> getElegibleShipmentKeysForMerge(final Set<String> setShpAloneOLKeys,
			final Document docGetCompleteOrdDtls) {

		// log the input
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":getElegibleShipmentKeysForMerge:entry:Input=" + setShpAloneOLKeys);
		}

		// the list of ship alone shipment
		final List listShpAloneShipment = getEligibleShipmentForMerge(setShpAloneOLKeys, docGetCompleteOrdDtls);

		Element eleShipment;
		String strShipmentKey;

		// the set of eligible shipment keys
		final Set<String> setEligibleShpKey = new HashSet<String>();

		// iterate and fetch the shipments
		for (Object objShipment : listShpAloneShipment) {

			eleShipment = (Element) objShipment;
			strShipmentKey = eleShipment.getAttribute(GCConstants.SHIPMENT_KEY);
			setEligibleShpKey.add(strShipmentKey);
		}

		// log the output
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":getElegibleShipmentKeysForMerge:exit:Output=" + setEligibleShpKey);
		}

		// return the shipment key set
		return setEligibleShpKey;
	}


	/**
	 * <h3>Description:</h3>This method is used to get the eligible shipment elements for merge
	 *
	 * @author Infosys Limited
	 * @param setShpAloneOLKeys The set of ship alone order line keys
	 * @param docGetCompleteOrdDtls The order details
	 * @return The list of shipment elements eligible for merge
	 */
	private List getEligibleShipmentForMerge(final Set<String> setShpAloneOLKeys, final Document docGetCompleteOrdDtls) {

		// log the input
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getEligibleShipmentForMerge:entry:input=" + setShpAloneOLKeys);
		}

		// form the xpath to fetch the shipalone shipment eligible
		final StringBuilder stbXpthShp =
				new StringBuilder(
						"/Order/Shipments/Shipment[count(./ShipmentLines/ShipmentLine)=2 and ./ShipmentLines/ShipmentLine[");

		boolean bIsNotFirst = false;

		for (String strOLKey : setShpAloneOLKeys) {

			if (bIsNotFirst) {

				stbXpthShp.append(" or ");
			} else {

				bIsNotFirst = true;
			}

			stbXpthShp.append("@OrderLineKey='");
			stbXpthShp.append(strOLKey);
			stbXpthShp.append("'");
		}

		stbXpthShp.append("]]");

		// fetch the shipments which have just ship alone shipment and in the list
		final List listShpAloneShipment = GCXMLUtil.getElementListByXpath(docGetCompleteOrdDtls, stbXpthShp.toString());

		// log the output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getEligibleShipmentForMerge:exit:Output=" + listShpAloneShipment);
		}

		// return the shipment list
		return listShpAloneShipment;
	}
	/**
	 * <h3>Description:</h3>This method is used to set of ship alone items eligible to merge
	 *
	 * @param setParentOLKeys The parent order line keys
	 * @param docGetCompleteOrdDtls The complete order details
	 * @return The set of order line key of
	 */
	private Set<String> getShipAloneItemOLKeys(final Set<String> setParentOLKeys, final Document docGetCompleteOrdDtls) {

		// log the input
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getShipAloneItemOLKeys:entry:Input=" + setParentOLKeys);
		}

		final String strXpath1OLShpAlone =
				"/Order/OrderLines/OrderLine[./ItemDetails/Extn/@ExtnIsShipAlone='Y' and ./BundleParentLine/@OrderLineKey='";
		final String strXpath2OLShpAlone = "']";

		List listShpAloneOL;
		StringBuilder stbXpathShpAlone;
		List<String> listShipAloneOLKey;

		// return list
		final Set<String> setOLShipAloneKeys = new HashSet<String>();

		// iterate the set of parent order line keys
		for (String strParentOLKey : setParentOLKeys) {

			stbXpathShpAlone = new StringBuilder(strXpath1OLShpAlone);
			stbXpathShpAlone.append(strParentOLKey);
			stbXpathShpAlone.append(strXpath2OLShpAlone);

			// fetch the ship alone kit components
			listShpAloneOL = GCXMLUtil.getElementListByXpath(docGetCompleteOrdDtls, stbXpathShpAlone.toString());

			listShipAloneOLKey = listShipAloneOLKeygetListOfOLKeys(listShpAloneOL);

			setOLShipAloneKeys.addAll(listShipAloneOLKey);
		}

		// log the output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getShipAloneItemOLKeys:exit:output=" + setOLShipAloneKeys);
		}

		// return the set ship alone orderline keys
		return setOLShipAloneKeys;
	}


	/**
	 * <h3>Description:</h3>This method is used to get the list of orderline keys from the list of
	 * orderline element input
	 *
	 * @author Infosys Limited
	 * @param listShpAloneOL The list of order line element
	 * @return The list of orderline keys corresponding to the input list
	 */
	private List<String> listShipAloneOLKeygetListOfOLKeys(final List listShpAloneOL) {

		// log the input
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":listShipAloneOLKeygetListOfOLKeys:entry:Input=" + listShpAloneOL);
		}

		// list of the Order line keys
		final List<String> listShipAloneOLKey = new ArrayList<String>();

		Element eleOrderLine;
		String strOLKey;

		// iterate through each of the order line
		for (Object objOrderLine : listShpAloneOL) {

			// get the order line
			eleOrderLine = (Element) objOrderLine;
			strOLKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);

			listShipAloneOLKey.add(strOLKey);
		}

		// log the output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":listShipAloneOLKeygetListOfOLKeys:exit:Output=" + listShipAloneOLKey);
		}

		// return the populated list
		return listShipAloneOLKey;
	}


	/**
	 * <h3>Description:</h3>This method is used to get the set of the parent order line keys for the
	 * kit component parents
	 *
	 * @author Infosys Limited
	 * @param docInput the shipment advice
	 * @param docGetCompleteOrdDtls The complete order details
	 * @return The set of parent order line keys of the kits belonging to the shipment advice
	 */
	private Set<String> getParentKeys(final Document docInput, final Document docGetCompleteOrdDtls) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getParentKeys:entry");
		}

		final Set<String> setParentLineKeys = new HashSet<String>();
		// get List of current shipment
		final List listShpLn = GCXMLUtil.getElementListByXpath(docInput, "/Shipment/ShipmentLines/ShipmentLine");

		Element eleShpLn;
		String strPrmLnNo;
		String strSubLnNo;
		StringBuilder stbXpthBndlParent;
		Element eleBundleParent;
		String strParentOLKey;

		final String strXpath1BndlParentLine = "/Order/OrderLines/OrderLine[@PrimeLineNo='";
		final String strXpath2BndlParentLine = "' and @SubLineNo='";
		final String strXpath3BndlParentLine = "' and ./Item/@ProductClass='REGULAR']/BundleParentLine";

		// get the list of parent line key

		for (Object objShpLn : listShpLn) {

			eleShpLn = (Element) objShpLn;
			strPrmLnNo = eleShpLn.getAttribute(GCConstants.PRIME_LINE_NO);
			strSubLnNo = eleShpLn.getAttribute(GCConstants.SUB_LINE_NO);

			// create the xpath
			stbXpthBndlParent = new StringBuilder(strXpath1BndlParentLine);
			stbXpthBndlParent.append( strPrmLnNo);
			stbXpthBndlParent.append( strXpath2BndlParentLine);
			stbXpthBndlParent.append( strSubLnNo);
			stbXpthBndlParent.append( strXpath3BndlParentLine);

			// fetch the parent line key
			eleBundleParent = GCXMLUtil.getElementByXPath(docGetCompleteOrdDtls, stbXpthBndlParent.toString());

			// check if the bundle parent exists
			if (eleBundleParent != null) {

				// fetch the parent line key
				strParentOLKey = eleBundleParent.getAttribute(GCConstants.ORDER_LINE_KEY);
				setParentLineKeys.add(strParentOLKey);
			}
		}

		// log the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getParentKeys:exit:Output=" + setParentLineKeys);
		}

		// return the parent line keys
		return setParentLineKeys;
	}


	/**
	 * <h3>Description:</h3>This method is used to check if the input shipment is for a SO and has
	 * shipment line
	 *
	 * @author Infosys Limited
	 * @param docInput The shipment advice
	 * @return true of the input document is for sales order and has shipment line else false
	 */
	private boolean isSOAndShpLnExist(final Document docInput) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":isSOAndShpLnExist:entry:Input=" + GCXMLUtil.getXMLString(docInput));
		}

		// check if only one line present in the input
		final NodeList nlShpLn = docInput.getElementsByTagName(GCConstants.SHIPMENT_LINE);

		// fetch the document type
		final Element eleShpRt = docInput.getDocumentElement();
		final String strDocType = eleShpRt.getAttribute(GCConstants.DOCUMENT_TYPE);

		// flag to indicate if sales order and shipment line exist
		final boolean bIsSOAndShpLnExist =
				GCConstants.SALES_ORDER_DOCUMENT_TYPE.equals(strDocType) && nlShpLn.getLength() > 0;

				// log the method entry
				if (LOGGER.isDebugEnabled()) {

					LOGGER.debug(CLASS_NAME + ":isSOAndShpLnExist:exit:Output=" + bIsSOAndShpLnExist);
				}

				// return the flag
				return bIsSOAndShpLnExist;
	}


	/**
	 * <h3>Description:</h3>This method is used to process ship alone items if the shipment exists
	 *
	 * @author Infosys Limited
	 * @param yfsEnv The yfs environment reference
	 * @param docInput The input document
	 * @param docGetCompleteOrdDtls The complete Order details output
	 * @return true if the ship alone items are to be processed else false
	 */
	private boolean processShipAloneItemIfShipmentExists(final YFSEnvironment yfsEnv, final Document docInput, final Document docGetCompleteOrdDtls) {

		// log the entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":processShipAloneItemIfShipmentExists:entry");
		}

		// get the list of shipments
		final Set<String> setShpKey = getShipmentListForKit(docInput, docGetCompleteOrdDtls);

		// flag to indicate if the shipAlone item is processed
		final boolean bIsShipAloneItemProcessed;

		// check if the shipment exist
		if (!setShpKey.isEmpty()) {

			// process the shipAloneItem
			bIsShipAloneItemProcessed = processShipAloneItem(yfsEnv, docInput, docGetCompleteOrdDtls, setShpKey);
		} else {
			bIsShipAloneItemProcessed = false;
		}

		// log the exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":processShipAloneItemIfShipmentExists:exit:Output=" + bIsShipAloneItemProcessed);
		}

		// return flag bIsHipAloneItem
		return bIsShipAloneItemProcessed;
	}


	/**
	 * <h3>Description:</h3>This method is used to check if the shipment exists for the parent of the
	 * ship alone component
	 *
	 * @author Infosys Limited
	 * @param docInput The shipment advice
	 * @param docGetCompleteOrdDtls The complete order details document
	 * @return true if the shipment exists for the bundle parent else false
	 */
	private Set<String> getShipmentListForKit(final Document docInput, final Document docGetCompleteOrdDtls) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getShipmentListForKit:entry");
		}

		// fetch the prime line number and the subline number from the shipment line
		final Element eleShpLn = GCXMLUtil.getElementByXPath(docInput, "/Shipment/ShipmentLines/ShipmentLine");

		final String strPrimeLnNo = eleShpLn.getAttribute(GCConstants.PRIME_LINE_NO);
		final String strSubLnNo = eleShpLn.getAttribute(GCConstants.SUB_LINE_NO);

		final StringBuilder stbParentXpth =
				new StringBuilder(
						"/Order/Shipments/Shipment[@Status!='9000']/ShipmentLines/ShipmentLine[@PrimeLineNo=../../../../OrderLines/OrderLine[ ./BundleParentLine/@OrderLineKey=../OrderLine[@PrimeLineNo='");
		stbParentXpth.append( strPrimeLnNo);
		stbParentXpth.append( "' and @SubLineNo='");
		stbParentXpth.append( strSubLnNo);
		stbParentXpth.append("']/BundleParentLine/@OrderLineKey]/@PrimeLineNo]");


		// fetch the parent order line
		final List listShlLn = GCXMLUtil.getElementListByXpath(docGetCompleteOrdDtls, stbParentXpth.toString());

		Element eleOrdShpLn;
		String strShpKey;
		final Set<String> setShipmentkeys = new HashSet<String>();

		// get the list of shipments
		for (Object objOrdShpLn : listShlLn) {

			// fetch the shipment Line
			eleOrdShpLn = (Element) objOrdShpLn;
			strShpKey = eleOrdShpLn.getAttribute(GCConstants.SHIPMENT_KEY);
			setShipmentkeys.add(strShpKey);
		}

		// log the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getShipmentListForKit:exit:Output=" + setShipmentkeys);
		}

		// list of shipment keys
		return setShipmentkeys;
	}


	/**
	 * <h3>Description:</h3>This method is used to get the complete Order details
	 *
	 * @author Infosys Limited
	 * @param yfsEnv The yfs environment reference
	 * @param docInput The shipment advice
	 * @return The complete order details api output
	 */
	private Document getCompleteOrderDtls(final YFSEnvironment yfsEnv, final Document docInput) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getCompleteOrderDtls:entry");
		}


		// invoke the getCompleteOrderDetails api
		final Document docCompOrdDtls =
				GCCommonUtil.invokeService(yfsEnv, "GCGetCompleteOrderDetails_ConfirmShipment_svc", docInput);

		// log the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getCompleteOrderDtls:exit:Output=" + GCXMLUtil.getXMLString(docCompOrdDtls));
		}

		// return the complete order details document
		return docCompOrdDtls;
	}


	/**
	 * <h3>Description:</h3>This method is used to process the ship alone items
	 *
	 * @author Infosys Limited
	 * @param yfsEnv YFS Environment reference
	 * @param docInput The shipment advice
	 * @param docOrderDtls The order list output
	 * @param setShpKey The set of shipment key of the bundle to which the ship alone line belongs to
	 */
	private boolean processShipAloneItem(final YFSEnvironment yfsEnv, final Document docInput,
			final Document docOrderDtls, final Set<String> setShpKey) {

		// log the method input
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":processShipAloneItem:entry");
		}

		// get the shipment to which the ship alone item needs to be added
		final String strShpKey = getShipmentToAddTheItem(docInput, docOrderDtls, setShpKey);

		//the return flag
		final boolean bIsprocessed = StringUtils.isNotBlank(strShpKey);

		//check if the change shipment is required
		if (bIsprocessed) {

			// add the shipments
			addShipAloneItemToShpmnt(yfsEnv, docInput, strShpKey, docOrderDtls);
		}

		// log the method input
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":processShipAloneItem:entry:Output=" + bIsprocessed);
		}

		// return the flag
		return bIsprocessed;
	}


	/**
	 * <h3>Description:</h3>This method is used to add the ship alone line to the shipment
	 *
	 * @author Infosys Limited
	 * @param yfsEnv YFS Environment reference
	 * @param docInput The shipment advice
	 * @param strShpKey The shipment key of the chipment to which this shipment needs to be added
	 * @param docOrderDtls the complete order details
	 */
	private void addShipAloneItemToShpmnt(final YFSEnvironment yfsEnv, final Document docInput, final String strShpKey,
			final Document docOrderDtls) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":addShipAloneItemToShpmnt:entry:Input:Shipment Key=" + strShpKey + ":Shipment advice="
					+ GCXMLUtil.getXMLString(docInput) + ":Complete Order Details=" + GCXMLUtil.getXMLString(docOrderDtls));
		}

		// create the change Shipment input
		final Document docChgShipmentIn = prepChgShipmentIn(strShpKey, docInput);

		// invoke change Shipment
		final Document docChgShmtOut =
				GCCommonUtil.invokeService(yfsEnv, "GCUnConfirmAndChangeShipment_ShipAlone_svc", docChgShipmentIn);

		// log the output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":addShipAloneItemToShpmnt:Output of changeShipment api="
					+ GCXMLUtil.getXMLString(docChgShmtOut));
		}

		// prepare the confirm shipment input
		final Document docConfShpmntIn = prepConfShpmntForShipAloneChgIn(docChgShmtOut);

		// invoke the confirm shipment
		final Document docConfShpOut =
				GCCommonUtil.invokeService(yfsEnv, "GCConfirmShipment_ShipAlone_svc", docConfShpmntIn);

		// log the exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":addShipAloneItemToShpmnt:exit:Output=" + GCXMLUtil.getXMLString(docConfShpOut));
		}
	}


	/**
	 * <h3>Description:</h3>This method is used to prepare the input for the confirm shipment
	 *
	 * @author Infosys Limited
	 * @param docChgShmtOut The shipment xml based on which confirmShipment api
	 * @return document for confirmShipment
	 */
	private Document prepConfShpmntForShipAloneChgIn(final Document docChgShmtOut) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER
			.debug(CLASS_NAME + ":prepConfShpmntForShipAloneChgIn:entry:Input=" + GCXMLUtil.getXMLString(docChgShmtOut));
		}

		// fetch the shipment key
		final Element eleShipment = docChgShmtOut.getDocumentElement();
		final String strShpKey = eleShipment.getAttribute(GCConstants.SHIPMENT_KEY);

		// create the shipment document
		final Document docConfirmShipmentIn = GCXMLUtil.createDocument(GCConstants.SHIPMENT);

		// populate the confirm shipment document
		final Element eleShp = docConfirmShipmentIn.getDocumentElement();
		eleShp.setAttribute(GCConstants.SHIPMENT_KEY, strShpKey);

		// log the output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":prepConfShpmntForShipAloneChgIn:exit:Output="
					+ GCXMLUtil.getXMLString(docConfirmShipmentIn));
		}

		// return the confirm shipment document
		return docConfirmShipmentIn;
	}


	/**
	 * <h3>Description:</h3>This method is used to prepare the changeShipment api input based on the
	 * input
	 *
	 * @author Infosys Limited
	 * @param strShpKey The shipment key of the shipment to which the shipment needs to be added
	 * @param docInput The shipment advice
	 * @return The changeShipment api input
	 */
	private Document prepChgShipmentIn(final String strShpKey, final Document docInput) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":prepChgShipmentIn:entry:Input:Shipment Key=" + strShpKey + ":Shipment Adv="
					+ GCXMLUtil.getXMLString(docInput));
		}

		// create the document
		final Document docChgShpIn = GCXMLUtil.createDocument(GCConstants.SHIPMENT);
		final Element eleShipment = docChgShpIn.getDocumentElement();

		// add the shipment key
		eleShipment.setAttribute(GCConstants.SHIPMENT_KEY, strShpKey);

		// create the shipment lines
		final Element eleShpLns = docChgShpIn.createElement(GCConstants.SHIPMENT_LINES);
		eleShipment.appendChild(eleShpLns);

		// create container
		final Element eleContainers = docChgShpIn.createElement(GCConstants.CONTAINERS);
		eleShipment.appendChild(eleContainers);

		// get the shipment line info from the shipment advice
		final List listShipmentLine = GCXMLUtil.getElementListByXpath(docInput, "/Shipment/ShipmentLines/ShipmentLine");

		// the set of container no
		final List<String> listAlreadyAddedContNo = new ArrayList<String>();

		Element eleShipLn;
		List listOfContainersForShpLn;

		// iterate through the loop
		for (Object objShpLn : listShipmentLine) {

			eleShipLn = (Element) objShpLn;

			// add the shipment line to the changeShipment in
			addShipmentLineToChgShpIn(eleShipLn, eleShpLns, docChgShpIn);

			// get the corresponding container element
			listOfContainersForShpLn = getContainerForShpLn(eleShipLn, docInput);

			// add the container after checking if the container not already present
			addContainerDtlsToChgShpIn(listOfContainersForShpLn, listAlreadyAddedContNo, docChgShpIn, eleContainers);
		}

		// log the output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":prepChgShipmentIn:exit:Output=" + GCXMLUtil.getXMLString(docChgShpIn));
		}

		// return the updated document
		return docChgShpIn;
	}


	/**
	 * <h3>Description:</h3> This method is used to add the container details onto the shipment
	 *
	 * @author Infosys Limited
	 * @param listOfContainersForShpLn The list of container elments to be added
	 * @param listAlreadyAddedContNo The list of container nos already present in the changeShipment
	 *        api input
	 * @param docChgShpIn The updated change shipment input document
	 * @param eleContainers The Conatiners elemnt of the changeShipment api input
	 */
	private void addContainerDtlsToChgShpIn(final List listOfContainersForShpLn,
			final List<String> listAlreadyAddedContNo, final Document docChgShpIn, final Element eleContainers) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":addContainerDtlsToChgShpIn:entry");
		}

		Element eleContainer;
		String strContainerNo;
		Node ndContainer;

		// iterate through each of the container element
		for (Object objContainer : listOfContainersForShpLn) {

			eleContainer = (Element) objContainer;

			// get the container number
			strContainerNo = eleContainer.getAttribute(GCConstants.CONTAINER_NO);

			// check if the container not already present
			if (!listAlreadyAddedContNo.contains(strContainerNo)) {

				// add the container number to the list
				listAlreadyAddedContNo.add(strContainerNo);

				// add the container to the changeShipment api
				ndContainer = docChgShpIn.importNode(eleContainer, true);
				eleContainers.appendChild(ndContainer);
			}
		}

		// log the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":addContainerDtlsToChgShpIn:exit:Output=" + GCXMLUtil.getXMLString(docChgShpIn));
		}
	}


	/**
	 * <h3>Description:</h3>This method is used to get the container element corresponding to the
	 * input shipment line
	 *
	 * @author Infosys Limited
	 * @param eleShpLn The shipment line from the shipment advice
	 * @param docInput The input document
	 * @return The list of container element from the shipment advice
	 */
	private List getContainerForShpLn(final Element eleShpLn, final Document docInput) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getContainerForShpLn:entry");
		}

		// fetch the prime line number and subline number
		final String strPrmLnNo = eleShpLn.getAttribute(GCConstants.PRIME_LINE_NO);
		final String strSubLnNo = eleShpLn.getAttribute(GCConstants.SUB_LINE_NO);
		final StringBuilder stbContainerXpth = new StringBuilder("/Shipment/Containers/Container[./ContainerDetails/ContainerDetail/ShipmentLine[@PrimeLineNo='");
		stbContainerXpth.append( strPrmLnNo);
		stbContainerXpth.append( "' and @SubLineNo='");
		stbContainerXpth.append( strSubLnNo);
		stbContainerXpth.append("']]");

		// fetch the container elements
		final List listContainer = GCXMLUtil.getElementListByXpath(docInput, stbContainerXpth.toString());

		// log the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getContainerForShpLn:exit:Output=" + listContainer);
		}

		// return the list of container elements
		return listContainer;
	}


	/**
	 * <h3>Description:</h3> This method is used to add the shipment line to the shipment lines
	 * element in the change Shipment
	 *
	 * @author Infosys Limited
	 * @param eleShpLn the shipment line of the shipment advice
	 * @param eleShpLns The shipment lines element of the changeShipment api input
	 * @param docChgShpIn The changeShipment api input
	 */
	private void addShipmentLineToChgShpIn(final Element eleShpLn, final Element eleShpLns, final Document docChgShpIn) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":addShipmentLineToChgShpIn:entry:Input:Element of Shipment Advice="
					+ GCXMLUtil.getElementXMLString(eleShpLn) + ":ChangeShipment in=" + GCXMLUtil.getXMLString(docChgShpIn));
		}

		// import the shipment line
		final Node ndShpLn = docChgShpIn.importNode(eleShpLn, false);
		eleShpLns.appendChild(ndShpLn);

		// log the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":addShipmentLineToChgShpIn:exit:Updated change shipment input="
					+ GCXMLUtil.getXMLString(docChgShpIn));
		}
	}


	/**
	 * <h3>Description:</h3>This method is used to identify the shipment to which the shipalone item
	 * needs to be added to
	 *
	 * @author Infosys Limited
	 * @param docInput The shipment advice document
	 * @param docOrderDtls the complete order details document
	 * @param setShpKey The set of shipment key
	 * @return The shipment key to which the ship alone item needs to be added to else returns null
	 */
	private String getShipmentToAddTheItem(final Document docInput, final Document docOrderDtls,
			final Set<String> setShpKey) {
		// get the item id of the shipment
		final Element eleShpLn = GCXMLUtil.getElementByXPath(docInput, "/Shipment/ShipmentLines/ShipmentLine");
		final String strItmId = eleShpLn.getAttribute(GCConstants.ITEM_ID);
		final String strQty = eleShpLn.getAttribute(GCConstants.QUANTITY);
		final int iQty = Integer.parseInt(strQty);

		//the xpath constants
		final String strXpth1ShpLn = "/Order/Shipments/Shipment[@ShipmentKey='";
		final String strXpth2ShpLn = "']/ShipmentLines/ShipmentLine[@ItemID='";
		final String strXpth3ShpLn = "']";

		StringBuilder stbXpthShpLn;
		Element eleShipLn;
		String strQuantity;
		int iQuantity;

		// the shipmentkey of the shipment to whcih the shipment is to be added
		String strShipmentKey = null;

		// iterate through each of the possible
		for (String strShKey : setShpKey) {

			// check if the shipment has the item shipped
			stbXpthShpLn = new StringBuilder( strXpth1ShpLn);
			stbXpthShpLn.append( strShKey);
			stbXpthShpLn.append( strXpth2ShpLn);
			stbXpthShpLn.append( strItmId);
			stbXpthShpLn.append( strXpth3ShpLn);
			eleShipLn = GCXMLUtil.getElementByXPath(docOrderDtls, stbXpthShpLn.toString());

			// check if the item is present in the shipment
			if (eleShipLn == null) {

				// if the item is not present in the shipment then this the shipment
				strShipmentKey = strShKey;
				break;
			} else {

				// Get the qty
				strQuantity = eleShipLn.getAttribute(GCConstants.QUANTITY);
				iQuantity = Integer.parseInt(strQuantity);

				// check if the quantity is eligible to be added to the shipment- part of backorder
			}
		}

		// return the shipment
		return strShipmentKey;
	}


	/**
	 * <h3>Description:</h3> This method is used to check if the item is ship alone
	 *
	 * @author Infosys Limited
	 * @param docInput The input document
	 * @param docGetCompleteOrdDtls The getCompleteOrderDetails output document
	 * @return True if shipalone item is present else false
	 */
	private boolean isShipAloneItem(final Document docInput, final Document docGetCompleteOrdDtls) {

		// log the input
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":isShipAloneItem:entry");
		}

		// fetch the item id from the shipment line
		final Element eleShpLn = GCXMLUtil.getElementByXPath(docInput, "/Shipment/ShipmentLines/ShipmentLine");
		final String strItemId = eleShpLn.getAttribute(GCConstants.ITEM_ID);

		//fetch the Extn element of the ItemDetails
		final StringBuilder stbItmDtlsExtnXpth = new StringBuilder("/Order/OrderLines/OrderLine/ItemDetails[ @ItemID='");
		stbItmDtlsExtnXpth.append(strItemId);
		stbItmDtlsExtnXpth.append("']/Extn");
		final Element eleItmDtlsExtn = GCXMLUtil.getElementByXPath(docGetCompleteOrdDtls, stbItmDtlsExtnXpth.toString());

		// fetch the extn is ship alone attribute
		final String strExtnIsShipAlone = eleItmDtlsExtn.getAttribute(GCConstants.EXTN_IS_SHIP_ALONE);

		// return flag
		final boolean bIsShipAlone;

		// set the return value based on the flag
		if (StringUtils.isNotBlank(strExtnIsShipAlone) && GCConstants.YES.equalsIgnoreCase(strExtnIsShipAlone.trim())) {

			bIsShipAlone = true;
		} else {

			bIsShipAlone = false;
		}

		// log the output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":isShipAloneItem:exit:Output=" + bIsShipAlone);
		}

		// return the flag
		return bIsShipAlone;
	}


	/**
	 * <h3>Description:</h3> This method is used to prepare the getOrderList or
	 * getCompleteOrderDetails api input
	 *
	 * @author Infosys Limited
	 * @param docInput The input document
	 * @return The getOrderList api input
	 */
	private Document prepGetOrderIn(final Document docInput) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":prepGetOrderIn:entry");
		}

		// get the document type
		final Element eleShp = docInput.getDocumentElement();
		final String strDocType = eleShp.getAttribute(GCConstants.DOCUMENT_TYPE);
		final String strEntprCode = eleShp.getAttribute(GCConstants.ENTERPRISE_CODE);

		//fetch the order no
		final Element eleShpLn = GCXMLUtil.getElementByXPath(docInput, "/Shipment/ShipmentLines/ShipmentLine");
		final String strOrderNo = eleShpLn.getAttribute(GCConstants.ORDER_NO);

		// Create the getOrderList api input
		final Document docOrder = GCXMLUtil.createDocument(GCConstants.ORDER);
		final Element eleOrdRt = docOrder.getDocumentElement();
		eleOrdRt.setAttribute(GCConstants.DOCUMENT_TYPE, strDocType);
		eleOrdRt.setAttribute(GCConstants.ENTERPRISE_CODE, strEntprCode);
		eleOrdRt.setAttribute(GCConstants.ORDER_NO, strOrderNo);

		// log the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":prepGetOrderIn:exit:Output=" + GCXMLUtil.getXMLString(docOrder));
		}

		// return the getOrderList output
		return docOrder;
	}
	/**
	 *
	 * Description of processShipmentConfirmation Contains the logic of
	 * processing the shipment confirmation message and confirm Shipment
	 * accordingly. If complete qty is not getting shipped, then backorder the
	 * remaining qty Move warranty lines for the parent line to shipped status
	 *
	 * @param env
	 * @param inDoc
	 * @throws GCException
	 *
	 */


	public static void processShipmentConfirmation(YFSEnvironment env,
			Document inDoc) throws GCException {
		LOGGER.verbose(" Entering GCProcessConfirmShipmentAPI.processShipmentConfirmation() method ");
		boolean isCompleteBackordered = false;
		if (LOGGER.isDebugEnabled()) {
			LOGGER.verbose(" Input document is" + GCXMLUtil.getXMLString(inDoc));
		}
		try {


			boolean isSetItem= false;
			Element eleShipment = inDoc.getDocumentElement();
			String sDocumentType = eleShipment.getAttribute(GCConstants.DOCUMENT_TYPE);
			String sOrderNo = GCXMLUtil.getAttributeFromXPath(eleShipment, "ShipmentLines/ShipmentLine/@OrderNo");
			LOGGER.verbose(" sDocumentType " + sDocumentType + " sOrderNo " + sOrderNo);
			Document docGetOrderListIP = GCXMLUtil.createDocument(GCConstants.ORDER);
			Element eleGetOrderListIP = docGetOrderListIP.getDocumentElement();
			eleGetOrderListIP.setAttribute(GCConstants.ORDER_NO, sOrderNo);
			eleGetOrderListIP.setAttribute(GCConstants.DOCUMENT_TYPE, sDocumentType);

			Document docGetOrderListTmp= GCCommonUtil.getDocumentFromPath("/global/template/api/getOrderList.xml");
			Document docGetOrderListOP = GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST, docGetOrderListIP, docGetOrderListTmp);


			LOGGER.verbose("GetOrderList called successfully");
			// Code block for handling Warranty associated with bundle item.
			//Begin:
			NodeList nlShipmntLine=XPathAPI.selectNodeList(inDoc, "Shipment/ShipmentLines/ShipmentLine");
			int nlShipmentLineCount=nlShipmntLine.getLength();
			String sprimeLineNo=null;
			List<String> lsPrimeLineNo=new ArrayList<String>();
			//finding the primelineNo of all the shipment lines
			for(int counter=0;counter<nlShipmentLineCount;counter++){
				Element eleShipmentLine=(Element)nlShipmntLine.item(counter);
				sprimeLineNo=eleShipmentLine.getAttribute(GCConstants.PRIME_LINE_NO);
				lsPrimeLineNo.add(sprimeLineNo);
				LOGGER.verbose("sprimeLineNo added successfully : " + sprimeLineNo);
			}
			Map<String, String> mWarrantyInfo=new HashMap<String,String>();
			int lsPrimeLineNoCount=lsPrimeLineNo.size();
			// String sOrderReleaseKey = fetchOrderReleaseKey(env, inDoc, sOrderNo)
			List<String> lsWarrantyLines=new ArrayList<String>();
			String bundleParentOrderLineKey=null,warrantyPrimeLineNo=null;
			for(int c=0;c<lsPrimeLineNoCount;c++){
				bundleParentOrderLineKey = GCXMLUtil.getAttributeFromXPath(docGetOrderListOP, "OrderList/Order/OrderLines/OrderLine[(@MaxLineStatus='3200' or MaxLineStatus='3200.05' or MaxLineStatus='3200.07'"  
						+ "or @MinLineStatus='3200' or @MinLineStatus='3200.05' or @MinLineStatus='3200.07' ) and @PrimeLineNo='"+ lsPrimeLineNo.get(c)+ "']/BundleParentLine/@OrderLineKey");

				if(bundleParentOrderLineKey!=null){
					warrantyPrimeLineNo =
							GCXMLUtil.getAttributeFromXPath(docGetOrderListOP,
									"OrderList/Order/OrderLines/OrderLine[(@MaxLineStatus='3200' or MaxLineStatus='3200.05' or MaxLineStatus='3200.07' "
											+ "or @MinLineStatus='3200' or @MinLineStatus='3200.05' or @MinLineStatus='3200.07') and @DependentOnLineKey='" + bundleParentOrderLineKey
											+ "']/@PrimeLineNo");
					//finding all the warranty lines PrimeLineNos
					if(warrantyPrimeLineNo!=null && !mWarrantyInfo.containsKey(warrantyPrimeLineNo)){
						mWarrantyInfo.put(warrantyPrimeLineNo,bundleParentOrderLineKey);
						lsWarrantyLines.add(warrantyPrimeLineNo);
						LOGGER.verbose("warrantyPrimeLineNo added successfully to lsWarrantyLines : " + warrantyPrimeLineNo);
					}
				}
			}
			//if there is any warranty associated with bundle item
			if(!mWarrantyInfo.isEmpty() && !lsWarrantyLines.isEmpty()){

				LOGGER.verbose("Inside if - !mWarrantyInfo.isEmpty() && !lsWarrantyLines.isEmpty() ");
				Map<String,List<String>> mComponentsWithBundleParentOrderLineKey=findListOfComponentsForAssociatedToWarranty(mWarrantyInfo, docGetOrderListOP,lsWarrantyLines);
				int listSize=lsWarrantyLines.size();
				String primeLineNo=null;
				for(int i=0;i<listSize;i++){
					primeLineNo=lsWarrantyLines.get(i);
					List<String> lsComponentItemId=mComponentsWithBundleParentOrderLineKey.get(primeLineNo);
					int lsComponentItemIdCount=lsComponentItemId.size();
					double dLargestRatio=0.0;
					String compItemId=null;
					for(int c=0;c<lsComponentItemIdCount;c++){
						String componentItemId=lsComponentItemId.get(c);
						Element eleShipmentLine =
								(Element) XPathAPI.selectSingleNode(inDoc, "Shipment/ShipmentLines/ShipmentLine[@ItemID='"
										+ componentItemId + "']");
						if (!YFCCommon.isVoid(eleShipmentLine)) {
							double dQuantity =
									Double.parseDouble(GCXMLUtil.getAttributeFromXPath(inDoc,
											"Shipment/ShipmentLines/ShipmentLine[@ItemID='" + componentItemId + "']/@Quantity"));
							double dExtnExpectedQuantity =
									Double.parseDouble(GCXMLUtil.getAttributeFromXPath(inDoc,
											"Shipment/ShipmentLines/ShipmentLine[@ItemID='" + componentItemId
											+ "']/Extn/@ExtnExpectedQuantity"));
							Double ratio = dExtnExpectedQuantity / dQuantity;
							LOGGER.verbose("componentItemId : " + componentItemId);
							LOGGER.verbose("dQuantity : " + dQuantity);
							LOGGER.verbose("dExtnExpectedQuantity : " + dExtnExpectedQuantity);
							LOGGER.verbose("ratio : " + ratio);
							dLargestRatio = ratio > dLargestRatio ? ratio : dLargestRatio;
							compItemId = componentItemId;
							LOGGER.verbose("compItemId : " + compItemId);
						}
					}
					for(int c=0;c<lsComponentItemIdCount;c++){
						LOGGER.verbose("Inside lsComponentItemId for loop");
						String componentItemId=lsComponentItemId.get(c);
						LOGGER.verbose("componentItemId : " + componentItemId);
						Element eleShipmentLine =
								(Element) XPathAPI.selectSingleNode(inDoc, "Shipment/ShipmentLines/ShipmentLine[@ItemID='"
										+ componentItemId + "']");
						if (!YFCCommon.isVoid(eleShipmentLine)) {
							double dExtnExpectedQuantity =
									Double.parseDouble(GCXMLUtil.getAttributeFromXPath(inDoc,
											"Shipment/ShipmentLines/ShipmentLine[@ItemID='" + componentItemId
											+ "']/Extn/@ExtnExpectedQuantity"));
							LOGGER.verbose("dExtnExpectedQuantity : " + dExtnExpectedQuantity);
							int iShippingQuantityComponent = (int) (dExtnExpectedQuantity / dLargestRatio);
							LOGGER.verbose("iShippingQuantityComponent : " + iShippingQuantityComponent);
							Element eleComponent =
									(Element) XPathAPI.selectSingleNode(inDoc, "Shipment/ShipmentLines/ShipmentLine[@ItemID='"
											+ componentItemId + "']");
							GCXMLUtil.setAttribute(eleComponent, GCConstants.QUANTITY, iShippingQuantityComponent + "");
						}
					}
					//creating and appending new shipmentline for warranty associated to bundle item

					LOGGER.verbose("creating and appending new shipmentline for warranty associated to bundle item");
					Element eleNewShipmentLine = GCXMLUtil.createElement(inDoc,
							GCConstants.SHIPMENT_LINE, null);
					Element eleWarrantyOrderLine=(Element)XPathAPI.selectSingleNode(docGetOrderListOP, "OrderList/Order/OrderLines/OrderLine[@PrimeLineNo='"+primeLineNo+"']");
					Element eleWarrantyItem=(Element)XPathAPI.selectSingleNode(eleWarrantyOrderLine, "Item");

					eleNewShipmentLine.setAttribute(GCConstants.ITEM_ID,eleWarrantyItem.getAttribute(GCConstants.ITEM_ID));
					eleNewShipmentLine.setAttribute(GCConstants.ORDER_NO,sOrderNo);
					eleNewShipmentLine.setAttribute(GCConstants.PRIME_LINE_NO,eleWarrantyOrderLine.getAttribute(GCConstants.PRIME_LINE_NO));
					eleNewShipmentLine.setAttribute(GCConstants.SUB_LINE_NO,eleWarrantyOrderLine.getAttribute(GCConstants.SUB_LINE_NO));
					int iShippingQuantityWarranty=(int)((Double.parseDouble(eleWarrantyOrderLine.getAttribute(GCConstants.ORDERED_QTY)))/dLargestRatio);
					eleNewShipmentLine.setAttribute(GCConstants.QUANTITY,iShippingQuantityWarranty+"");
					String releaseNo = fetchReleaseNo(env, eleWarrantyOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
					eleNewShipmentLine.setAttribute(GCConstants.RELEASE_NO, releaseNo);
					eleNewShipmentLine.setAttribute(GCConstants.PRODUCT_CLASS,eleWarrantyItem.getAttribute(GCConstants.PRODUCT_CLASS));
					eleNewShipmentLine.setAttribute(GCConstants.UNIT_OF_MEASURE,eleWarrantyItem.getAttribute(GCConstants.UNIT_OF_MEASURE));

					Element eleContainerDetails=(Element)XPathAPI.selectSingleNode(inDoc, "Shipment/Containers/Container/ContainerDetails");
					Element eleNewContainerDetail=GCXMLUtil.createElement(inDoc,
							"ContainerDetail", null);
					eleNewContainerDetail.setAttribute(GCConstants.QUANTITY, iShippingQuantityWarranty+"");
					Element eleContainerDetailsShipmentLine=GCXMLUtil.createElement(inDoc,
							"ShipmentLine", null);
					eleNewContainerDetail.appendChild(eleContainerDetailsShipmentLine);
					eleContainerDetails.appendChild(eleNewContainerDetail);
					GCXMLUtil.copyElement(inDoc,eleNewShipmentLine,eleContainerDetailsShipmentLine);
					Element eleNewShipmentLineExtn=GCXMLUtil.createElement(inDoc,
							GCConstants.EXTN, null);
					eleNewShipmentLineExtn.setAttribute(GCConstants.EXTN_EXPECTED_QUANTITY, eleWarrantyOrderLine.getAttribute(GCConstants.ORDERED_QTY));
					eleNewShipmentLine.appendChild(eleNewShipmentLineExtn);
					Element eleShipmentLine=(Element)XPathAPI.selectSingleNode(inDoc, "Shipment/ShipmentLines");
					eleShipmentLine.appendChild(eleNewShipmentLine);
					LOGGER.verbose("new Line added successfully");
				}
			}
			// Code block for handling Warranty associated with bundle item.
			//Ends

			if (LOGGER.isDebugEnabled()) {
				LOGGER.verbose(" Output document is docGetOrderListOP "
						+ GCXMLUtil.getXMLString(docGetOrderListOP));
			}
			Element eleOrder = (Element) XPathAPI.selectSingleNode(
					docGetOrderListOP, "OrderList/Order");
			String sOrderHeaderKey = eleOrder
					.getAttribute(GCConstants.ORDER_HEADER_KEY);
			String sEnterpriseCode = eleOrder
					.getAttribute(GCConstants.ENTERPRISE_CODE);
			String sOrganizationCode = eleOrder
					.getAttribute(GCConstants.SELLER_ORGANIZATION_CODE);
			LOGGER.verbose(" sEnterpriseCode " + sEnterpriseCode
					+ " sOrganizationCode " + sOrganizationCode);
			Element extnInOrder = (Element) XPathAPI.selectSingleNode(eleOrder, "Extn");
			String extnPickUpStoreID = null;
			String extnPhaseMode = null;
			if(!YFCCommon.isVoid(extnInOrder)){
				extnPickUpStoreID = extnInOrder.getAttribute(GCConstants.EXTN_PICK_UP_STORE_ID);
				extnPhaseMode = extnInOrder.getAttribute(GCConstants.EXTN_ORDER_PHASE);
			}

			eleShipment.setAttribute(GCConstants.ENTERPRISE_CODE,
					sEnterpriseCode);
			eleShipment.setAttribute(GCConstants.SELLER_ORGANIZATION_CODE,
					sOrganizationCode);

			isCompleteBackordered =
					backOrderNonShippedReleaseLines(env, docGetOrderListOP, inDoc, sOrderNo, sEnterpriseCode, eleShipment,
							isCompleteBackordered);
			Document docShipmentIP = GCXMLUtil
					.createDocument(GCConstants.SHIPMENT);
			Element eleShipmentIP = docShipmentIP.getDocumentElement();
			GCXMLUtil.copyElement(docShipmentIP, eleShipment, eleShipmentIP);
			LOGGER.verbose(" docShipmentIP is"
					+ GCXMLUtil.getXMLString(docShipmentIP));
			boolean bIsCreateShipmentRequired = false;

			// Prepare ChangeOrder document to cancel the shortage quantity for
			// PO
			Document docChangeOrder = GCXMLUtil
					.createDocument(GCConstants.ORDER);
			Element eleChangeOrder = docChangeOrder.getDocumentElement();
			eleChangeOrder.setAttribute(GCConstants.ORDER_HEADER_KEY,
					sOrderHeaderKey);
			//GCSTORE-1831 Fix for modification rule exception
			eleChangeOrder.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
			eleChangeOrder.setAttribute(GCConstants.ACTION, GCConstants.MODIFY);
			Element eleChangeOrderLines = GCXMLUtil.createElement(
					docChangeOrder, GCConstants.ORDER_LINES, null);
			eleChangeOrder.appendChild(eleChangeOrderLines);
			// Prepare ChangeOrderStatus document to move Backordered Qty to
			// BackorderedDAX status
			Document docChangeBackOrderStatus = GCCommonUtil
					.prepareChangeOrderStatusHdrDoc(sOrderHeaderKey,
							GCConstants.CHANGE_CONFIRM_STATUS);
			//Phase 2 Order Fulfillment changes
			String shipNode = eleShipment.getAttribute(GCConstants.SHIP_NODE);
			Element eleShipmentLines = (Element) XPathAPI.selectSingleNode(
					eleShipment, GCConstants.SHIPMENT_LINES);
			NodeList nlShipmentLine = XPathAPI.selectNodeList(eleShipment,
					"ShipmentLines/ShipmentLine");

			// GCSTORE-1901::Begin
			Document salesOrderChangeOrdrip = GCXMLUtil.createDocument(GCConstants.ORDER);
			Element soChangeOrdrEle = salesOrderChangeOrdrip.getDocumentElement();
			soChangeOrdrEle.setAttribute(GCConstants.ACTION, GCConstants.MODIFY);
			soChangeOrdrEle.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
			Element soChangeOrdrOrderLinesEle =
					GCXMLUtil.createElement(salesOrderChangeOrdrip, GCConstants.ORDER_LINES, null);
			soChangeOrdrEle.appendChild(soChangeOrdrOrderLinesEle);
			// GCSTORE-1901::end
			for (int iCounter = 0; iCounter < nlShipmentLine.getLength(); iCounter++) {
				Element eleShipmentLine = (Element) nlShipmentLine
						.item(iCounter);
				String sPrimeLineNo = eleShipmentLine
						.getAttribute(GCConstants.PRIME_LINE_NO);
				String sSubLineNo = eleShipmentLine
						.getAttribute(GCConstants.SUB_LINE_NO);
				String sReleaseNo = eleShipmentLine
						.getAttribute(GCConstants.RELEASE_NO);
				String sShippedQty = eleShipmentLine
						.getAttribute(GCConstants.QUANTITY);
				double dShippedQty = (!YFCCommon.isVoid(sShippedQty)) ? Double
						.valueOf(sShippedQty) : 0.00;
						String sExtnExpectedQuantity = GCXMLUtil.getAttributeFromXPath(
								eleShipmentLine, "Extn/@ExtnExpectedQuantity");
						double dExtnExpectedQuantity = (!YFCCommon.isVoid(sExtnExpectedQuantity)) ? Double.valueOf(sExtnExpectedQuantity) : 0.00;
						LOGGER.verbose(" sPrimeLineNo " + sPrimeLineNo + " sSubLineNo "+ sSubLineNo + " sReleaseNo" + sReleaseNo
								+ " sShippedQty " + sShippedQty
								+ " sExtnExpectedQuantity " + sExtnExpectedQuantity);
						//GCSTORE-1889 - Condition added for complete short pick fix
						if(dShippedQty==0 && GCConstants.SALES_ORDER_DOCUMENT_TYPE.equals(sDocumentType)){
							// GCSTORE-1833 - do nothing
						}else if((dExtnExpectedQuantity != dShippedQty)
								&& GCConstants.SALES_ORDER_DOCUMENT_TYPE.equals(sDocumentType)) {
							bIsCreateShipmentRequired = true;
							double dQtyToBeBackorder = dExtnExpectedQuantity - dShippedQty;
							GCCommonUtil.addLinesToChangeOrderStatusDoc(docChangeBackOrderStatus, eleShipmentLine,

									String.valueOf(dQtyToBeBackorder), GCConstants.BACKORDERED_OMS_STATUS);


							eleShipmentLine.setAttribute(GCConstants.QUANTITY,
									sExtnExpectedQuantity);
							LOGGER.verbose(" If  dExtnExpectedQuantity!=dShippedQty, bIsCreateShipmentRequired"
									+ bIsCreateShipmentRequired);
						}

						if (GCConstants.PO_DOCUMENT_TYPE.equals(sDocumentType)) {
							String sShortageQty = eleShipmentLine .getAttribute(GCConstants.SHORTAGE_QTY);
							double dShortageQty = (!YFCCommon.isVoid(sShortageQty)) ? Double.valueOf(sShortageQty) : 0.00;
							//Phase 2 Order fulfillment changes
							if (dShortageQty > 0.00) {
								String sOrderedQty = GCXMLUtil.getAttributeFromXPath(docGetOrderListOP,
										"OrderList/Order/OrderLines/" + "OrderLine[@PrimeLineNo='" + sPrimeLineNo + "' and @SubLineNo='"
												+ sSubLineNo + "']/@OrderedQty");
								double dOrderedQty = (!YFCCommon.isVoid(sOrderedQty) ? Double.valueOf(sOrderedQty) : 0.00);
								double dRemainingQty = dOrderedQty - dShortageQty;
								//Phase 2 Order Fulfillment changes
								// GCSTORE-1901::Begin
								
								//VDS - 87 --> pass ExtnVendorID in below function --> If GCVendorNode backorders then suppress PrimaryVendor too
								Element eleShipmentLine_SOLine = GCXMLUtil.getElementByXPath(docGetOrderListOP, "OrderList/Order/OrderLines/OrderLine[@PrimeLineNo='" + sPrimeLineNo + "' and @SubLineNo='"
												+ sSubLineNo + "']");
								Element eleItemDetails = (Element) eleShipmentLine_SOLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
								Element eleItemDetailsExtn = (Element) eleItemDetails.getElementsByTagName(GCConstants.EXTN).item(0);
								String sExtnVendorId = "";
								if(!YFCCommon.isVoid(eleItemDetailsExtn)){
									sExtnVendorId = eleItemDetailsExtn.getAttribute(GCConstants.EXTN_VENDOR_ID);
								}
								salesOrderChangeOrdrip.getDocumentElement().setAttribute("ExtnVendorId", sExtnVendorId);
								Element soChangeOrdrOrderLineEle =
										appendSOChangeOrderLine(salesOrderChangeOrdrip, sPrimeLineNo, sSubLineNo, shipNode);
								soChangeOrdrOrderLinesEle.appendChild(soChangeOrdrOrderLineEle);
								// GCSTORE-1901::end
								Element eleChangeOrderLine = appendChangeOrderLine(docChangeOrder, sPrimeLineNo, sSubLineNo, dRemainingQty);
								eleChangeOrderLines.appendChild(eleChangeOrderLine);
							}
						}

						Element eleContainerDetails =
								(Element) XPathAPI.selectSingleNode(inDoc, "Shipment/Containers/Container/"
										+ "ContainerDetails/ContainerDetail" + "[ShipmentLine/@PrimeLineNo='" + sPrimeLineNo
										+ "' and ShipmentLine/@SubLineNo='" + sSubLineNo + "']/..");
						Element eleContainerDetail =
								(Element) XPathAPI.selectSingleNode(inDoc, "Shipment/Containers/Container/"
										+ "ContainerDetails/ContainerDetail" + "[ShipmentLine/@PrimeLineNo='" + sPrimeLineNo
										+ "' and ShipmentLine/@SubLineNo='" + sSubLineNo + "']");


						Element eleParentLine = (Element) XPathAPI.selectSingleNode(docGetOrderListOP,
								"OrderList/Order/OrderLines/OrderLine" + "[@PrimeLineNo='" + sPrimeLineNo + "' and @SubLineNo='" + sSubLineNo + "']");
						String sParentLineKey = eleParentLine.getAttribute(GCConstants.ORDER_LINE_KEY);

						String sExtnSetCode = GCXMLUtil.getAttributeFromXPath( docGetOrderListOP,
								"OrderList/Order/OrderLines/OrderLine" + "[@PrimeLineNo='" + sPrimeLineNo + "' and @SubLineNo='" + sSubLineNo
								+ "']/ItemDetails/Extn/@ExtnSetCode");
						LOGGER.verbose(" sParentLineKey" + sParentLineKey
								+ " sExtnSetCode" + sExtnSetCode);

						// If ExtnSetCode='1', then remove parent line and add component
						// lines to the shipment xml
						if (GCConstants.STRING_ONE.equalsIgnoreCase(sExtnSetCode)) {
							LOGGER.verbose(" Inside If ExtnSetCode is 1 ");
							isSetItem= true;
							eleShipmentLines.removeChild(eleShipmentLine);
							eleContainerDetails.removeChild(eleContainerDetail);
							String sParentQty = eleParentLine.getAttribute(GCConstants.ORDERED_QTY);
							double dParentQty = (!YFCCommon.isVoid(sParentQty)) ? Double.valueOf(sParentQty) : 0.00;
							NodeList nlCompOrderLine = XPathAPI.selectNodeList(docGetOrderListOP,
									"OrderList/Order/OrderLines/OrderLine" + "[BundleParentLine/@OrderLineKey='" + sParentLineKey + "']");
							for (int iCount = 0; iCount < nlCompOrderLine.getLength(); iCount++) {
								Element eleCompOrderLine = (Element) nlCompOrderLine.item(iCount);
								Element eleItem = (Element) XPathAPI.selectSingleNode(eleCompOrderLine, GCConstants.ITEM);
								String sCompQty = eleCompOrderLine.getAttribute(GCConstants.ORDERED_QTY);
								double dCompQty = (!YFCCommon.isVoid(sCompQty)) ? Double.valueOf(sCompQty) : 0.00;
								double dCompShippedQty = dShippedQty * dCompQty / dParentQty;
								LOGGER.verbose(" dCompQty" + dCompQty + " dCompShippedQty" + dCompShippedQty);
								Element eleNewContainerDetail = GCXMLUtil.createElement(inDoc, GCConstants.CONTAINER_DETAIL, null);
								eleNewContainerDetail.setAttribute(GCConstants.QUANTITY, String.valueOf(dCompShippedQty));
								eleContainerDetails.appendChild(eleNewContainerDetail);
								Element eleNewShipmentLine = GCXMLUtil.createElement(
										inDoc, GCConstants.SHIPMENT_LINE, null);
								eleNewShipmentLine.setAttribute(GCConstants.ITEM_ID,
										eleItem.getAttribute(GCConstants.ITEM_ID));
								eleNewShipmentLine.setAttribute(GCConstants.ORDER_NO,
										sOrderNo);
								eleNewShipmentLine
								.setAttribute(
										GCConstants.PRIME_LINE_NO,
										eleCompOrderLine
										.getAttribute(GCConstants.PRIME_LINE_NO));
								eleNewShipmentLine.setAttribute(
										GCConstants.SUB_LINE_NO, eleCompOrderLine
										.getAttribute(GCConstants.SUB_LINE_NO));
								eleNewShipmentLine.setAttribute(GCConstants.QUANTITY,
										String.valueOf(dCompShippedQty));
								eleNewShipmentLine.setAttribute(GCConstants.RELEASE_NO,
										sReleaseNo);
								eleNewShipmentLine
								.setAttribute(
										GCConstants.PRODUCT_CLASS,
										eleItem.getAttribute(GCConstants.PRODUCT_CLASS));
								eleNewShipmentLine.setAttribute(GCConstants.UOM,
										eleItem.getAttribute(GCConstants.UOM));

								eleNewContainerDetail.appendChild(eleNewShipmentLine);
								Element eleNewShipmentLine1 = GCXMLUtil.createElement(inDoc, GCConstants.SHIPMENT_LINE, null);
								GCXMLUtil.copyElement(inDoc, eleNewShipmentLine, eleNewShipmentLine1);
								Element eleNewExtn = GCXMLUtil.createElement(inDoc, GCConstants.EXTN, null);
								eleNewExtn.setAttribute(GCConstants.EXTN_EXPECTED_QUANTITY, sCompQty);
								eleNewShipmentLine1.appendChild(eleNewExtn);
								eleShipmentLines.appendChild(eleNewShipmentLine1);
							}
						}
						if (LOGGER.isDebugEnabled()) {
							LOGGER.debug(CLASS_NAME + ":Input Document after adding container detail for set components="
									+ GCXMLUtil.getXMLString(inDoc));
						}
						// Warranty logic, move the warranty lines to shipped status for
						// the respective parent line
						String parentLineReleasedStatusStr=GCXMLUtil.getAttributeFromXPath(docGetOrderListOP, 
								"OrderList/Order/OrderLines/OrderLine" + "[@OrderLineKey='" + sParentLineKey +"']/OrderStatuses/OrderStatus"
										+"[@Status='" + GCConstants.RELEASED_STATUS +"']/@StatusQty");
						double parentLineReleasedStatusQty=0;
						if (!YFCCommon.isVoid(parentLineReleasedStatusStr)) {
							parentLineReleasedStatusQty=Double.valueOf(parentLineReleasedStatusStr);
						}

						NodeList nlDependentOrderLine = XPathAPI.selectNodeList( docGetOrderListOP,
								"OrderList/Order/OrderLines/OrderLine" + "[@DependentOnLineKey='" + sParentLineKey + "' and @MaxLineStatus!=9000 and Extn/@ExtnIsWarrantyItem='Y']");
						for (int iCount = 0; iCount < nlDependentOrderLine.getLength(); iCount++) {
							LOGGER.verbose(" Looping on the Warranty Lines ");
							Element eleDependentOrderLine = (Element) nlDependentOrderLine.item(iCount);
							String sDependentOrderLineKey = eleDependentOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
							Element eleItem = (Element) XPathAPI.selectSingleNode( eleDependentOrderLine, GCConstants.ITEM);

							Element eleNewContainerDtl = GCXMLUtil.createElement(inDoc, GCConstants.CONTAINER_DETAIL, null);
							Element eleNewShipmentLine = GCXMLUtil.createElement(inDoc, GCConstants.SHIPMENT_LINE, null);
							eleNewShipmentLine.setAttribute(GCConstants.ITEM_ID, eleItem.getAttribute(GCConstants.ITEM_ID));
							eleNewShipmentLine.setAttribute(GCConstants.PRODUCT_CLASS, eleItem.getAttribute(GCConstants.PRODUCT_CLASS));
							eleNewShipmentLine.setAttribute(GCConstants.UOM, eleItem.getAttribute(GCConstants.UOM));
							eleNewShipmentLine.setAttribute(GCConstants.ORDER_NO, sOrderNo);
							eleNewShipmentLine.setAttribute(GCConstants.PRIME_LINE_NO, eleDependentOrderLine.getAttribute(GCConstants.PRIME_LINE_NO));
							eleNewShipmentLine.setAttribute(GCConstants.SUB_LINE_NO, eleDependentOrderLine.getAttribute(GCConstants.SUB_LINE_NO));

							String strReleaseNo = fetchReleaseNo(env, sDependentOrderLineKey);
							eleNewShipmentLine.setAttribute(GCConstants.RELEASE_NO, strReleaseNo);

							String dependentLineReleasedStatusQty=GCXMLUtil.getAttributeFromXPath(docGetOrderListOP, 
									"OrderList/Order/OrderLines/OrderLine" + "[@OrderLineKey='" + sDependentOrderLineKey +"']/OrderStatuses/OrderStatus"
											+"[@Status='" + GCConstants.RELEASED_STATUS +"']/@StatusQty");

							// GCSTORE - 6581 start
							//Setting warranty line qty based on input message,parent line released qty and warranty line released qty
							if (!YFCCommon.isVoid(parentLineReleasedStatusStr)) {
								if(dShippedQty==parentLineReleasedStatusQty){
									eleNewContainerDtl.setAttribute(GCConstants.QUANTITY, String.valueOf(dependentLineReleasedStatusQty));

								}
								else if(dShippedQty<parentLineReleasedStatusQty)
								{
									double minQty= Math.min(dShippedQty, Double.parseDouble(dependentLineReleasedStatusQty));
									eleNewContainerDtl.setAttribute(GCConstants.QUANTITY, String.valueOf(minQty));
								}
								// GCSTORE - 6581 End  
							}else{
								eleNewContainerDtl.setAttribute(GCConstants.QUANTITY, String.valueOf(dShippedQty));
							}
							eleNewContainerDtl.appendChild(eleNewShipmentLine);
							eleContainerDetails.appendChild(eleNewContainerDtl);
							Element eleNewShipLineInShipLines = GCXMLUtil.createElement(inDoc, GCConstants.SHIPMENT_LINE, null);
							GCXMLUtil.copyElement(inDoc, eleNewShipmentLine, eleNewShipLineInShipLines);
							// GCSTORE - 6581 start
							if (!YFCCommon.isVoid(parentLineReleasedStatusStr)) {
								if(dShippedQty==parentLineReleasedStatusQty)
								{
									eleNewShipLineInShipLines.setAttribute(GCConstants.QUANTITY,String.valueOf(dependentLineReleasedStatusQty));
								}
								else if(dShippedQty<parentLineReleasedStatusQty)
								{
									double minQty= Math.min(dShippedQty, Double.parseDouble(dependentLineReleasedStatusQty));

									eleNewShipLineInShipLines.setAttribute(GCConstants.QUANTITY,String.valueOf(minQty));
								}
								// GCSTORE - 6581 End
							}
							else{
								eleNewShipLineInShipLines.setAttribute(GCConstants.QUANTITY,String.valueOf(dShippedQty));
							}

							eleShipmentLines.appendChild(eleNewShipLineInShipLines);
						}
			}

			NodeList nlChangeOrderLine = XPathAPI.selectNodeList(
					docChangeOrder, "Order/OrderLines/OrderLine");
			if (nlChangeOrderLine.getLength() > 0) {
				GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER,
						docChangeOrder);
			}

			LOGGER.verbose("Ship Node fetched :" +shipNode);
			YFCDocument getOrgListIndoc = YFCDocument.createDocument(GCConstants.ORGANIZATION);
			YFCElement organizationEle = getOrgListIndoc.getDocumentElement();
			organizationEle.setAttribute(GCConstants.ORGANIZATION_CODE, shipNode);
			LOGGER.verbose("getOrganizationList Indoc :" +getOrgListIndoc.toString());
			YFCDocument getOrgListTemp = YFCDocument.getDocumentFor(GCConstants.GET_ORG_LIST_TEMPLATE);
			LOGGER.verbose("getOrganizationList template :" +getOrgListTemp.toString());
			YFCDocument getOrgListOutdoc = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORGANIZATION_LIST, getOrgListIndoc, getOrgListTemp);
			LOGGER.verbose("getOrganizationList Output :" +getOrgListOutdoc.toString());

			YFCElement organizationList = getOrgListOutdoc.getDocumentElement();
			YFCElement organization = organizationList.getChildElement(GCConstants.ORGANIZATION);
			YFCElement node = organization.getChildElement(GCConstants.NODE);
			String nodeType = node.getAttribute(GCConstants.NODE_TYPE);
			LOGGER.verbose("Node Type :" +nodeType);
			boolean isDigitalOrWarranty = true;

			YFCDocument getOrderListOutput = YFCDocument.getDocumentFor(docGetOrderListOP);
			LOGGER.verbose("Converted getOrderList Outdoc :" +getOrderListOutput.toString());
			YFCElement orderListEle = getOrderListOutput.getDocumentElement();
			YFCElement orderEle = orderListEle.getChildElement(GCConstants.ORDER);
			YFCElement orderLinesEle = orderEle.getChildElement(GCConstants.ORDER_LINES);
			YFCElement extnInOrderEle = orderEle.getChildElement(GCConstants.EXTN);
			String extnPickUpStoreId = GCConstants.BLANK_STRING;
			if(extnInOrderEle.hasAttribute(GCConstants.EXTN_PICK_UP_STORE_ID)){
				extnPickUpStoreId = extnInOrderEle.getAttribute(GCConstants.EXTN_PICK_UP_STORE_ID);
			}

			YFCIterable<YFCElement> orderLineList = orderLinesEle.getChildren();
			YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
			LOGGER.verbose("Converted Indoc :" +inputDoc.toString());
			YFCElement shipmentEle = inputDoc.getDocumentElement();
			YFCElement shipmentLinesEle = shipmentEle.getChildElement(GCConstants.SHIPMENT_LINES);
			YFCIterable<YFCElement> shipmentLineList = shipmentLinesEle.getChildren();
			for(YFCElement shipmentLineEle : shipmentLineList){
				String itemID = shipmentLineEle.getAttribute(GCConstants.ITEM_ID);
				String uom = shipmentLineEle.getAttribute(GCConstants.UNIT_OF_MEASURE);
				LOGGER.verbose("Item ID fetched:" +itemID);
				LOGGER.verbose("UOM Fetched :" +uom);
				YFCDocument getItemListOutdoc = GCCommonUtil.getItemList(env, itemID, uom, GCConstants.GCI, true,YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LIST_TEMP_PACK));
				YFCElement itemList = getItemListOutdoc.getDocumentElement();
				LOGGER.verbose("getItemList Outdoc :" +itemList.toString());
				YFCElement itemEle = itemList.getChildElement(GCConstants.ITEM);
				YFCElement primaryInfoEle = itemEle.getChildElement(GCConstants.PRIMARY_INFORMATION);
				String itemType = primaryInfoEle.getAttribute(GCConstants.ITEM_TYPE);
				LOGGER.verbose("Item Type :" +itemType);
				for(YFCElement orderLineEle : orderLineList){
					if(YFCUtils.equals(shipmentLineEle.getAttribute(GCConstants.PRIME_LINE_NO), orderLineEle.getAttribute(GCConstants.PRIME_LINE_NO))){
						YFCElement extnEle = orderLineEle.getChildElement(GCConstants.EXTN);
						LOGGER.verbose("Extn element fetched :" +extnEle.toString());
						if (!(YFCUtils.equals(GCConstants.YES, extnEle.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM))
								|| YFCUtils.equals(GCConstants.DIGITAL_ITEM_TYPE, itemType) || YFCUtils.equals(
										GCConstants.DIGITAL_ITEM_TYPE_10, itemType))) {
							LOGGER.verbose("Warranty!!");
							isDigitalOrWarranty = false;
							break;
						}
					}
				}
			}

			// GCSTORE-2272::Begin
			if (GCConstants.PO_DOCUMENT_TYPE.equals(sDocumentType)) {
				String salesOrdrHdrKey = getSalesOrderHeaderKey(env, sOrderHeaderKey);
				callChangeOrdrOnSalesOrder(env, salesOrdrHdrKey, salesOrderChangeOrdrip);
			}
			// GCSTORE-2272::End
			boolean isInvoiceRequired = false;
			YFCDocument confirmShipOutdoc = null;

			// GCSTORE-1889, check added to remove 0 qty shipment lines
			if (removeShipmentLinesWithZeroQty(inDoc)) {
				if (bIsCreateShipmentRequired) {
					/*
					 * If complete qty is not getting shipped, then do createShipment at first and then do
					 * confirmShipment with BackOrderNonShippedQuantity='Y', it will backorder the unshipped
					 * qty
					 */
					LOGGER.verbose(" Inside Create Shipment Required ");
					// Bug 4338: When for scenario (Regular + warranty) shipment Quantity=0 starts
					NodeList nlistShipmentLine =
							XPathAPI.selectNodeList(inDoc, "Shipment/ShipmentLines/ShipmentLine[@Quantity='" + "0.0" + "']");
					if (nlistShipmentLine != null) {
						int count = nlistShipmentLine.getLength();
						for (int iCounter = 0; iCounter < count; iCounter++) {
							Element eleShipmentLine = (Element) nlistShipmentLine.item(iCounter);
							String sPrimeLineNo = eleShipmentLine.getAttribute(GCConstants.PRIME_LINE_NO);
							String sSubLineNo = eleShipmentLine.getAttribute(GCConstants.SUB_LINE_NO);
							String sReleaseNo = eleShipmentLine.getAttribute(GCConstants.RELEASE_NO);
							String sShippedQty = eleShipmentLine.getAttribute(GCConstants.QUANTITY);
							double dShippedQty = (!YFCCommon.isVoid(sShippedQty)) ? Double.valueOf(sShippedQty) : 0.00;
							String sDependentOnLineKey =
									GCXMLUtil.getAttributeFromXPath(docGetOrderListOP,
											"OrderList/Order/OrderLines/OrderLine[@PrimeLineNo='" + sPrimeLineNo + "']/@DependentOnLineKey");
							String itemId =
									GCXMLUtil.getAttributeFromXPath(docGetOrderListOP,
											"OrderList/Order/OrderLines/OrderLine[@OrderLineKey='" + sDependentOnLineKey + "']/Item/@ItemID");


							String sExtnExpectedQuantity =
									GCXMLUtil.getAttributeFromXPath(inDoc, "Shipment/ShipmentLines/ShipmentLine[@ItemID='" + itemId
											+ "']/Extn/@ExtnExpectedQuantity");
							double dExtnExpectedQuantity =
									(!YFCCommon.isVoid(sExtnExpectedQuantity)) ? Double.valueOf(sExtnExpectedQuantity) : 0.00;
									LOGGER.verbose(" sPrimeLineNo " + sPrimeLineNo + " sSubLineNo " + sSubLineNo + " sReleaseNo" + sReleaseNo
											+ " sShippedQty " + sShippedQty + " sExtnExpectedQuantity " + sExtnExpectedQuantity);
									double dQtyToBeBackorder = dExtnExpectedQuantity - dShippedQty;
									eleShipmentLine.setAttribute(GCConstants.QUANTITY, String.valueOf(dQtyToBeBackorder));


									// creating and appending new ShipmentLines/Shipmentline for warranty associated to
									// regular item
									Element eleNewShipmentLine = GCXMLUtil.createElement(docShipmentIP, GCConstants.SHIPMENT_LINE, null);
									Element eleWarrantyOrderLine =
											(Element) XPathAPI.selectSingleNode(docGetOrderListOP,
													"OrderList/Order/OrderLines/OrderLine[@PrimeLineNo='" + sPrimeLineNo + "']");
									Element eleWarrantyItem = (Element) XPathAPI.selectSingleNode(eleWarrantyOrderLine, "Item");

									eleNewShipmentLine.setAttribute(GCConstants.ITEM_ID, eleWarrantyItem.getAttribute(GCConstants.ITEM_ID));
									eleNewShipmentLine.setAttribute(GCConstants.ORDER_NO, sOrderNo);
									eleNewShipmentLine.setAttribute(GCConstants.PRIME_LINE_NO,
											eleWarrantyOrderLine.getAttribute(GCConstants.PRIME_LINE_NO));
									eleNewShipmentLine.setAttribute(GCConstants.SUB_LINE_NO,
											eleWarrantyOrderLine.getAttribute(GCConstants.SUB_LINE_NO));
									eleNewShipmentLine.setAttribute(GCConstants.QUANTITY, sShippedQty);
									eleNewShipmentLine.setAttribute(GCConstants.RELEASE_NO, sReleaseNo);
									eleNewShipmentLine.setAttribute(GCConstants.PRODUCT_CLASS,
											eleWarrantyItem.getAttribute(GCConstants.PRODUCT_CLASS));
									eleNewShipmentLine.setAttribute(GCConstants.UNIT_OF_MEASURE,
											eleWarrantyItem.getAttribute(GCConstants.UNIT_OF_MEASURE));
									Element docShipmentIPShipmentLines =
											(Element) XPathAPI.selectSingleNode(docShipmentIP, "Shipment/ShipmentLines");
									docShipmentIPShipmentLines.appendChild(eleNewShipmentLine);
									Element eleNewShipmentLineContainer =
											GCXMLUtil.createElement(docShipmentIP, GCConstants.SHIPMENT_LINE, null);

									// creating and appending new ContainerDetails/Shipmentline for warranty associated to
									// regular item
									eleNewShipmentLineContainer.setAttribute(GCConstants.ITEM_ID,
											eleWarrantyItem.getAttribute(GCConstants.ITEM_ID));
									eleNewShipmentLineContainer.setAttribute(GCConstants.ORDER_NO, sOrderNo);
									eleNewShipmentLineContainer.setAttribute(GCConstants.PRIME_LINE_NO,
											eleWarrantyOrderLine.getAttribute(GCConstants.PRIME_LINE_NO));
									eleNewShipmentLineContainer.setAttribute(GCConstants.SUB_LINE_NO,
											eleWarrantyOrderLine.getAttribute(GCConstants.SUB_LINE_NO));
									eleNewShipmentLineContainer.setAttribute(GCConstants.RELEASE_NO, sReleaseNo);
									eleNewShipmentLineContainer.setAttribute(GCConstants.PRODUCT_CLASS,
											eleWarrantyItem.getAttribute(GCConstants.PRODUCT_CLASS));
									eleNewShipmentLineContainer.setAttribute(GCConstants.UNIT_OF_MEASURE,
											eleWarrantyItem.getAttribute(GCConstants.UNIT_OF_MEASURE));

									Element eleContainerDetails =
											(Element) XPathAPI.selectSingleNode(docShipmentIP, "Shipment/Containers/Container/ContainerDetails");
									Element eleNewContainerDetail = GCXMLUtil.createElement(docShipmentIP, "ContainerDetail", null);
									eleNewContainerDetail.setAttribute(GCConstants.QUANTITY, GCConstants.ZERO);
									Element eleContainerDetailsShipmentLine = GCXMLUtil.createElement(docShipmentIP, "ShipmentLine", null);
									eleNewContainerDetail.appendChild(eleContainerDetailsShipmentLine);
									eleContainerDetails.appendChild(eleNewContainerDetail);
									GCXMLUtil.copyElement(docShipmentIP, eleNewShipmentLineContainer, eleContainerDetailsShipmentLine);
									docShipmentIP.getDocumentElement().setAttribute("OverrideModificationRules", "Y");
						}
					}
					// Bug 4338: Coding Ends here

					Document inDocConfirmShpmnt = GCXMLUtil.getDocument(GCXMLUtil.getXMLString(inDoc));
					Document docCreateShpmtOP = GCCommonUtil.invokeAPI(env, GCConstants.API_CREATE_SHIPMENT, inDoc);
					String sShipmentNo = docCreateShpmtOP.getDocumentElement().getAttribute(GCConstants.SHIPMENT_NO);
					LOGGER.verbose(" sShipmentNo " + sShipmentNo);
					if (isSetItem) {
						inDocConfirmShpmnt.getDocumentElement().setAttribute(GCConstants.SHIPMENT_NO, sShipmentNo);
						inDocConfirmShpmnt.getDocumentElement()
						.setAttribute(GCConstants.BACKORDER_NON_SHIPPED_QTY, GCConstants.YES);
						if ((YFCUtils.equals(GCConstants.DC, nodeType) || YFCUtils.equals(GCConstants.PO_DOCUMENT_TYPE,
								sDocumentType)) && !YFCCommon.isVoid(extnPickUpStoreId)) {
							if (!isDigitalOrWarranty) {
								YFCDocument inputConfirmShpmnt = YFCDocument.getDocumentFor(inDocConfirmShpmnt);
								YFCElement shipmentInConfirmShip = inputConfirmShpmnt.getDocumentElement();
								prepareInputForConfirmShip(shipmentInConfirmShip);
								inDocConfirmShpmnt = inputConfirmShpmnt.getDocument();
							}
							isInvoiceRequired = true;
						}
						confirmShipOutdoc =
								GCCommonUtil.invokeAPI(env, GCConstants.API_CONFIRM_SHIPMENT,
										YFCDocument.getDocumentFor(inDocConfirmShpmnt),
										YFCDocument.getDocumentFor(GCConstants.CONFIRM_SHIP_TEMP));
					} else {
						docShipmentIP.getDocumentElement().setAttribute(GCConstants.SHIPMENT_NO, sShipmentNo);
						docShipmentIP.getDocumentElement().setAttribute(GCConstants.BACKORDER_NON_SHIPPED_QTY, GCConstants.YES);
						if ((YFCUtils.equals(GCConstants.DC, nodeType) || YFCUtils.equals(GCConstants.PO_DOCUMENT_TYPE,
								sDocumentType)) && !YFCCommon.isVoid(extnPickUpStoreId)) {
							if (!isDigitalOrWarranty) {
								YFCDocument docShipmentIndoc = YFCDocument.getDocumentFor(docShipmentIP);
								YFCElement shipmentInConfirmShip = docShipmentIndoc.getDocumentElement();
								prepareInputForConfirmShip(shipmentInConfirmShip);
								docShipmentIP = docShipmentIndoc.getDocument();
							}
							isInvoiceRequired = true;
						}


						confirmShipOutdoc =
								GCCommonUtil.invokeAPI(env, GCConstants.API_CONFIRM_SHIPMENT,
										YFCDocument.getDocumentFor(docShipmentIP),
										YFCDocument.getDocumentFor(GCConstants.CONFIRM_SHIP_TEMP));
					}

					if (LOGGER.isVerboseEnabled()) {
						LOGGER.verbose("Confirm Shipment Indoc :" + YFCDocument.getDocumentFor(docShipmentIP).toString());
					}
					GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER_STATUS, docChangeBackOrderStatus);

				} else {

					LOGGER.verbose("PickupStoreID :" + extnPickUpStoreId);
					if ((YFCUtils.equals(GCConstants.DC, nodeType) || YFCUtils
							.equals(GCConstants.PO_DOCUMENT_TYPE, sDocumentType)) && !YFCCommon.isVoid(extnPickUpStoreId)) {
						if (!isDigitalOrWarranty) {
							prepareInputForConfirmShip(shipmentEle);
							inDoc = inputDoc.getDocument();
							if (LOGGER.isVerboseEnabled()) {
								LOGGER.verbose("Converting back" + YFCDocument.getDocumentFor(inDoc).toString());
							}
						}
						isInvoiceRequired = true;
					}
					if (LOGGER.isVerboseEnabled()) {
						LOGGER.verbose("Confirm Shipment Indoc :" + YFCDocument.getDocumentFor(inDoc).toString());
					}

					if (!isCompleteBackordered) {
						confirmShipOutdoc =
								GCCommonUtil.invokeAPI(env, GCConstants.API_CONFIRM_SHIPMENT, YFCDocument.getDocumentFor(inDoc),
										YFCDocument.getDocumentFor(GCConstants.CONFIRM_SHIP_TEMP));

					}
					if (GCConstants.PO_DOCUMENT_TYPE.equals(sDocumentType)) {
						// GCSTORE-1901::Begin
						createSOConfirmShipmentXml(env, inDoc);
						// GCSTORE-1901::End
					}

				}

				if (!YFCCommon.isVoid(confirmShipOutdoc)) {
					YFCElement shipOutEle = confirmShipOutdoc.getDocumentElement();
					if (LOGGER.isVerboseEnabled()) {
						LOGGER.verbose("Confirm Shipment Outdoc :" + shipOutEle.toString());
					}

					if (YFCUtils.equals(GCConstants.PO_DOCUMENT_TYPE, sDocumentType)) {
						YFCDocument createShipInvoiceIndoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
						YFCElement shipInvoiceEle = createShipInvoiceIndoc.getDocumentElement();
						shipInvoiceEle.setAttribute(GCConstants.SELLER_ORGANIZATION_CODE,
								shipOutEle.getAttribute(GCConstants.SELLER_ORGANIZATION_CODE));
						shipInvoiceEle.setAttribute(GCConstants.SHIP_NODE, shipOutEle.getAttribute(GCConstants.SHIP_NODE));
						shipInvoiceEle.setAttribute(GCConstants.SHIPMENT_KEY, shipOutEle.getAttribute(GCConstants.SHIPMENT_KEY));
						shipInvoiceEle.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.PO_CREATE_SHIP_INVOICE);
						if (LOGGER.isVerboseEnabled()) {
							LOGGER.verbose("createShipInvoice Indoc :" + createShipInvoiceIndoc.toString());
						}
						GCCommonUtil.invokeAPI(env, GCConstants.CREATE_SHPMNT_INVC_API, createShipInvoiceIndoc.getDocument());
					} else {

						// enable ship alone changes
						if (getFlagValue("DISABLE_SHP_INVOICE_1")) {

							GCConfirmShipmentAPI invObj = new GCConfirmShipmentAPI();
							invObj.callCreateShipInvoiceAPI(env, shipOutEle);
						}
					}

					if (isInvoiceRequired && !isDigitalOrWarranty && YFCUtils.equals(GCConstants.PHASE_TWO, extnPhaseMode)) {
						if (YFCUtils.equals(GCConstants.PO_DOCUMENT_TYPE, sDocumentType)) {
							callChangeShipStatus(env, shipOutEle, GCConstants.CHNG_TO_PENDING_RCPT);
						} else if (getFlagValue("DISABLE_SHP_STATUS_UPDATE_1")) {
							callChangeShipStatus(env, shipOutEle, GCConstants.SHIP_STATUS_UPDATE);
						}
					}
				}
			}
			// Check if seller organization is Border free - INTL_WEB
			if (YFCUtils.equals(GCConstants.INTL_WEB, sOrganizationCode)) {
				Element eleExtn = (Element)eleOrder.getElementsByTagName(GCConstants.EXTN).item(0);
				String sExtnIntlWebOrderNo = eleExtn.getAttribute(GCConstants.EXTN_INTL_WEB_ORDER_NO);
				createAsyncRequest(env, inDoc, sExtnIntlWebOrderNo);
			}
		} catch (Exception e) {
			LOGGER.error(
					" Inside catch block of GCProcessConfirmShipmentAPI.processShipmentConfirmation(), Exception is :",
					e);
			YFCException ex = new YFCException(e);
			throw ex;
		}
		LOGGER.verbose(" Exiting GCProcessConfirmShipmentAPI.processShipmentConfirmation() method ");
	}

	/**
	 * This method gets the sales order header key of a purcahse order.
	 *
	 * @param env
	 * @param purChaseOrderHeaderKey
	 * @return
	 */
	private static String getSalesOrderHeaderKey(YFSEnvironment env, String purChaseOrderHeaderKey) {
		LOGGER.beginTimer("getSalesOrderHeaderKey");
		Document getPurchaseOrderListOutDoc =
				GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST,
						GCXMLUtil.getDocument("<Order OrderHeaderKey='" + purChaseOrderHeaderKey + "' />"), GCXMLUtil
						.getDocument("<OrderList><Order OrderNo=''><OrderLines>"
								+ "<OrderLine ChainedFromOrderHeaderKey='' ChainedFromOrderLineKey='' />" + "</OrderLines>"
								+ "</Order>" + "</OrderList>"));
		Element eleOrderLine = (Element) getPurchaseOrderListOutDoc.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
		String sChainedFromOrderHeaderKey = eleOrderLine.getAttribute(GCConstants.CHAINED_FROM_ORDER_HEADER_KEY);
		LOGGER.verbose("Sales Order Header Key::" + sChainedFromOrderHeaderKey);
		LOGGER.endTimer("getSalesOrderHeaderKey");
		return sChainedFromOrderHeaderKey;
	}

	/**
	 * This method calls change order api with the given input.
	 *
	 * @param env
	 * @param salesOrdrHdrKey
	 * @param salesOrderChangeOrdrip
	 * @throws TransformerException
	 */
	private static void callChangeOrdrOnSalesOrder(YFSEnvironment env, String salesOrdrHdrKey,
			Document salesOrderChangeOrdrip) throws TransformerException {
		LOGGER.beginTimer("callChangeOrdrOnSalesOrder");
		NodeList nlSoChangeOrderLine = XPathAPI.selectNodeList(salesOrderChangeOrdrip, "Order/OrderLines/OrderLine");
		if (nlSoChangeOrderLine.getLength() > 0 && !YFCCommon.isVoid(salesOrdrHdrKey)) {
			salesOrderChangeOrdrip.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, salesOrdrHdrKey);
			LOGGER.verbose("Input to changeOrder API for sales Order Updation:: "
					+ GCXMLUtil.getXMLString(salesOrderChangeOrdrip));
			GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, salesOrderChangeOrdrip);
		}
		LOGGER.endTimer("callChangeOrdrOnSalesOrder");
	}


	/**
	 * GCSTORE-1889 - Method added to remove 0 quantity shipment lines
	 * @param inDoc
	 * @return
	 */
	private static Boolean removeShipmentLinesWithZeroQty(Document inDoc) {
		LOGGER.beginTimer("GCProcessConfirmShipmentAPI.removeShipmentLinesWithZeroQty");
		Element containerDetailElem = GCXMLUtil.getElementByXPath(inDoc, "//ContainerDetails/ContainerDetail");
		Element shipLinesElem = (Element) inDoc.getElementsByTagName(GCConstants.SHIPMENT_LINES).item(0);
		List<Element> shipmentLines = GCXMLUtil.getElementListByXpath(inDoc, "//ShipmentLines/ShipmentLine");
		for (Element shipmentLine : shipmentLines) {
			String quantity = shipmentLine.getAttribute(GCConstants.QUANTITY);
			//GCSTORE-5928---Start
			Double qty = Double.valueOf(quantity);
			//GCSTORE-5928---End
			String primeLineNo = shipmentLine.getAttribute(GCConstants.PRIME_LINE_NO);
			//GCSTORE-5928---Start
			if (YFCCommon.isVoid(quantity) || qty == 0.0) {
		    //GCSTORE-5928---End
				shipLinesElem.removeChild(shipmentLine);
				if (!YFCCommon.isVoid(containerDetailElem)) {
					Element shipLineElem =
							GCXMLUtil.getElementByXPath(inDoc, "//ContainerDetails/ContainerDetail/ShipmentLine[@PrimeLineNo='"
									+ primeLineNo + "']");
					final Node ndShpLnParent = shipLineElem.getParentNode();
					if( !YFCCommon.isVoid(ndShpLnParent)){
						ndShpLnParent.removeChild(shipLineElem);
						final Node ndCntrDetailParent = ndShpLnParent.getParentNode();
						ndCntrDetailParent.removeChild(ndShpLnParent);
					}
				}
			}
		}
		shipmentLines = GCXMLUtil.getElementListByXpath(inDoc, "//ShipmentLine");
		LOGGER.endTimer("GCProcessConfirmShipmentAPI.removeShipmentLinesWithZeroQty");
		return !shipmentLines.isEmpty();
	}

	/**
	 * This method will create borderfree confirm shipment confirmation message and post it to async
	 * request agent
	 *
	 * @param env
	 * @param inputDoc
	 * @param sExtnIntlWebOrderNo
	 */
	private static void createAsyncRequest(YFSEnvironment env, Document inputDoc, String sExtnIntlWebOrderNo) {
		LOGGER.beginTimer("GCProcessConfirmShipmentAPI.createAsyncRequest");
		LOGGER.verbose("GCProcessConfirmShipmentAPI.createAsyncRequest--Begin");

		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
		YFCElement inDocRootEle = inDoc.getDocumentElement();
		String shipDate = inDocRootEle.getAttribute(GCConstants.ACTUAL_SHIPMENT_DATE);
		YFCElement containers = inDocRootEle.getChildElement(GCConstants.CONTAINERS);
		YFCNodeList<YFCElement> listContainer = containers.getElementsByTagName(GCConstants.CONTAINER);

		// Loop over all containers for the shipment
		for (int i = 0; i < listContainer.getLength(); i++) {
			YFCElement container = listContainer.item(i);
			String trackingNo = container.getAttribute(GCConstants.TRACKING_NO);
			YFCElement extn = container.getChildElement(GCConstants.EXTN);
			String carrierServiceCode = extn.getAttribute(GCConstants.EXTN_CARRIER_SERVICE_CODE);
			String sTrackingURL = GCConstants.BLANK;
			String sCarrierId = GCConstants.BLANK;
			String sModeOfDeliveryDesc = GCConstants.BLANK;

			YFCElement containerDetails = container.getChildElement(GCConstants.CONTAINER_DETAILS);
			YFCNodeList<YFCElement> listContainerDetail = containerDetails.getElementsByTagName(GCConstants.CONTAINER_DETAIL);

			String yfsUser = YFSSystem.getProperty(GCConstants.GC_BORDERFREE_WEBSERVICE_USERNAME);
			String yfsPassword = YFSSystem.getProperty(GCConstants.GC_BORDERFREE_WEBSERVICE_PASSWORD);

			// prepared root element
			YFCDocument yfcDocCreateBorderFreeMsg = YFCDocument.createDocument(GCConstants.SOAP_ENVELOPE);
			YFCElement eleSoapEnvelope = yfcDocCreateBorderFreeMsg.getDocumentElement();
			eleSoapEnvelope.setAttribute(GCConstants.XMLNS_SOAP, GCConstants.BORDERFREE_URL_XMLNS_SOAP);
			eleSoapEnvelope.setAttribute(GCConstants.XMLNS_V1, GCConstants.BORDERFREE_URL_XMLNS_V1);

			// Added header element
			YFCElement eleHeader =
					(YFCElement) eleSoapEnvelope.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.SOAP_HEADER));

			// Added Security element
			YFCElement eleSecurity =
					(YFCElement) eleHeader.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.WSSE_SECURITY));
			eleSecurity.setAttribute(GCConstants.XMLNS_WSSE, GCConstants.BORDERFREE_URL_XMLNS_WSSE);

			// Added UserToken element
			YFCElement eleUserToken =
					(YFCElement) eleSecurity.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.USER_NAME_TOKEN));

			// Added UserName element
			YFCElement eleUserName =
					(YFCElement) eleUserToken.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.WSSE_USERNAME));
			eleUserName.setNodeValue(yfsUser);

			// Added UserPassword element
			YFCElement elePassword =
					(YFCElement) eleUserToken.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.WSSE_PASSWORD));
			elePassword.setNodeValue(yfsPassword);

			// Added Body element
			YFCElement eleBody =
					(YFCElement) eleSoapEnvelope.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.SOAP_BODY));

			// Added parcelShipmentNotification element
			YFCElement eleparcelShipmentNotification =
					(YFCElement) eleBody.appendChild(yfcDocCreateBorderFreeMsg
							.createElement(GCConstants.PARCEL_SHIPMENT_NOTIFICATION));

			// Added parcel element
			YFCElement eleparcel =
					(YFCElement) eleparcelShipmentNotification.appendChild(yfcDocCreateBorderFreeMsg
							.createElement(GCConstants.PARCEL));

			YFCElement containerDetail = listContainerDetail.item(0);
			YFCElement shipmentLine = containerDetail.getChildElement(GCConstants.SHIPMENT_LINE);
			String sLevelOfService = shipmentLine.getAttribute(GCConstants.LEVEL_OF_SERVICE);

			// Fetch Carrier name and tracking URL details from custom table
			YFCDocument yfcDocGetShipmentLevelFlagInp = YFCDocument.createDocument(GCConstants.GC_SHIPPING_LEVEL_FLAG);
			YFCElement yfcEleGetShipmentRoot = yfcDocGetShipmentLevelFlagInp.getDocumentElement();
			yfcEleGetShipmentRoot.setAttribute(GCConstants.MODE_OF_DELIVERY, carrierServiceCode);
			yfcEleGetShipmentRoot.setAttribute(GCConstants.LEVEL_OF_SERVICE, sLevelOfService);

			YFCDocument yfcDocGetShipmentLevelFlagOup =
					GCCommonUtil.invokeService(env, GCConstants.GC_GET_SHIPPING_LEVEL_FLAG, yfcDocGetShipmentLevelFlagInp);
			YFCElement yfcEleGetShipmentLevelFlag =
					yfcDocGetShipmentLevelFlagOup.getElementsByTagName(GCConstants.GC_SHIPPING_LEVEL_FLAG).item(0);
			if (!YFCCommon.isVoid(yfcEleGetShipmentLevelFlag)) {
				sTrackingURL = yfcEleGetShipmentLevelFlag.getAttribute(GCConstants.TRACKING_URL_BORDER_FREE);
				sCarrierId = yfcEleGetShipmentLevelFlag.getAttribute(GCConstants.SHIPPING_CARRIER_ID);
				sModeOfDeliveryDesc = yfcEleGetShipmentLevelFlag.getAttribute(GCConstants.MODE_OF_DELIVERY_DESCRIPTION);
			}
			if (YFCCommon.isVoid(sTrackingURL)) {
				sTrackingURL = trackingNo;
			} else {
				sTrackingURL = sTrackingURL.concat(trackingNo);
			}
			// End of custom table logic

			// Set Carrier name
			YFCElement elecarrierName =
					(YFCElement) eleparcel.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.CARRIER_NAME));
			elecarrierName.setNodeValue(sCarrierId);

			// Set carrier service
			YFCElement elecarrierService =
					(YFCElement) eleparcel.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.CARRIER_SERVICE));
			elecarrierService.setNodeValue(sModeOfDeliveryDesc);

			// Loop and set sku & quantity
			for (int j = 0; j < listContainerDetail.getLength(); j++) {
				containerDetail = listContainerDetail.item(j);
				String quantity = containerDetail.getAttribute(GCConstants.QUANTITY);
				shipmentLine = containerDetail.getChildElement(GCConstants.SHIPMENT_LINE);
				String itemID = shipmentLine.getAttribute(GCConstants.ITEM_ID);
				YFCElement eleitems =
						(YFCElement) eleparcel.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.ITEMS));
				YFCElement elequantity =
						(YFCElement) eleitems.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.QUANTITY_SMALL));
				elequantity.setNodeValue(quantity);
				YFCElement elesku =
						(YFCElement) eleitems.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.SKU_SMALL));
				elesku.setNodeValue(itemID);
			}

			// Set orderId
			YFCElement eleorderId =
					(YFCElement) eleparcel.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.ORDER_ID_SMALL));
			eleorderId.setNodeValue(sExtnIntlWebOrderNo);

			// Set Parcel Id
			YFCElement eleparcelId =
					(YFCElement) eleparcel.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.PARCEL_ID));
			eleparcelId.setNodeValue(trackingNo);

			// Set Parcel reference
			YFCElement eleparcelReference =
					(YFCElement) eleparcel.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.PARCEL_REFERENCE));
			eleparcelReference.setNodeValue(GCConstants.BLANK);

			// Set shipping date
			YFCElement eleshippingDate =
					(YFCElement) eleparcel.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.SHIPPING_DATE_SMALL));
			eleshippingDate.setNodeValue(shipDate);

			// Set tracking URL
			//Commented for GCSTORE-3154
			/*
			 * YFCElement eletrackingURL = (YFCElement)
			 * eleparcel.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.TRACKING_URL));
			 * eletrackingURL.setNodeValue(sTrackingURL);
			 */

			// Added requestPackingSlip element
			YFCElement elerequestPackingSlip =
					(YFCElement) eleparcelShipmentNotification.appendChild(yfcDocCreateBorderFreeMsg
							.createElement(GCConstants.REQUEST_PACKING_SLIP));
			elerequestPackingSlip.setNodeValue(GCConstants.TRUE);

			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("yfcDocCreateBorderFreeMsg Document: " + yfcDocCreateBorderFreeMsg.toString());
			}

			GCCommonUtil.invokeService(env, GCConstants.POST_BF_RESPONSE_MSG_TO_Q_SERVICE, yfcDocCreateBorderFreeMsg);
		}
		LOGGER.verbose("GCProcessConfirmShipmentAPI.createAsyncRequest--End");
		LOGGER.endTimer("GCProcessConfirmShipmentAPI.createAsyncRequest");
	}


	/**
	 *
	 * @param env
	 * @param shipOutEle
	 * @param transactionID
	 */
	private static void callChangeShipStatus(YFSEnvironment env, YFCElement shipOutEle, String transactionID) {

		YFCDocument changeShipStatusIndoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
		YFCElement shipment = changeShipStatusIndoc.getDocumentElement();
		shipment.setAttribute(GCConstants.TRANSACTION_ID, transactionID);
		shipment.setAttribute(GCConstants.SHIPMENT_KEY, shipOutEle.getAttribute(GCConstants.SHIPMENT_KEY));
		shipment.setAttribute(GCConstants.BASE_DROP_STATUS, GCConstants.PENDING_RECEIPT_STATUS);
		if(LOGGER.isVerboseEnabled()){
			LOGGER.verbose("changeShipStatus Indoc :" +changeShipStatusIndoc.toString());
		}

		GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_SHIPMENT_STATUS, changeShipStatusIndoc.getDocument());

	}

	/**
	 *
	 * @param shipmentEle
	 */
	private static void prepareInputForConfirmShip(YFCElement shipmentEle) {

		LOGGER.verbose("Change Confirm Input for Pending Receipt :" +shipmentEle.toString());
		YDate date = new YDate(false);
		YFCElement extnInShipment = shipmentEle.getChildElement(GCConstants.EXTN);
		if(YFCCommon.isVoid(extnInShipment)){
			extnInShipment = shipmentEle.createChild(GCConstants.EXTN);
		}
		extnInShipment.setAttribute(GCConstants.EXTN_PENDING_RECEIPT_DATE, date.getString(GCConstants.DATE_TIME_FORMAT));
		YFCElement containersEle = shipmentEle.getChildElement(GCConstants.CONTAINERS);
		if(!YFCCommon.isVoid(containersEle) && containersEle.hasChildNodes()){
			YFCIterable<YFCElement> containerList = containersEle.getChildren();
			for(YFCElement containerEle : containerList){
				YFCElement extnInContainerEle = containerEle.getChildElement(GCConstants.EXTN);
				if(YFCCommon.isVoid(extnInContainerEle)){
					extnInContainerEle = containerEle.createChild(GCConstants.EXTN);
				}
				extnInContainerEle.setAttribute(GCConstants.EXTN_CONTAINER_STATUS, GCConstants.PENDING_RECEIPT);
			}
		}
		LOGGER.verbose("After the changes :" +shipmentEle.toString());
	}


	/**
	 *
	 * The method invokes changeRelase API to backorder the nonshipped release lines to backorder
	 * them. It also moves them to BackorderedDAX status so that they are not sent to DAX for
	 * allocation.
	 *
	 * @param isCompleteBackordered
	 *
	 *
	 */
	private static boolean backOrderNonShippedReleaseLines(YFSEnvironment env,
			Document docGetOrderListOP, Document inDoc, String sOrderNo,
			String sEnterpriseCode, Element eleShipment, boolean isCompleteBackordered)
					throws GCException {
		try {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.verbose(" inDoc is inside backOrderNonShippedReleaseLines"
						+ GCXMLUtil.getXMLString(inDoc));
			}
			String sOrderReleaseKey = fetchOrderReleaseKey(env, inDoc, sOrderNo);
			// Prepare changeRelease to backOrder Non-Shipped lines
			List<Element> listOrderLine = GCXMLUtil.getElementListByXpath(docGetOrderListOP,
					"OrderList/Order/OrderLines/OrderLine[(@MaxLineStatus='3200' " + "or @MinLineStatus='3200') and OrderStatuses/OrderStatus/@OrderReleaseKey='"
							+ sOrderReleaseKey + "']");

			List<Element> listShipmentLine = GCXMLUtil.getElementListByXpath(inDoc, "Shipment/ShipmentLines/ShipmentLine");
			List<Element> alNonShippedOrderLines = new ArrayList<Element>();
			List<String> alShippedPrimeLines = new ArrayList<String>();

			for (Element eleShipmentLine : listShipmentLine) {

				String primeLineNoSL = eleShipmentLine.getAttribute("PrimeLineNo");
				//GCSTORE-1833 Fix added
				String shippedQty = eleShipmentLine.getAttribute("Quantity");
				if ((int)Double.parseDouble(shippedQty) > 0) {
					alShippedPrimeLines.add(primeLineNoSL);
				}
				LOGGER.verbose(" alShippedPrimeLines is inside backOrderNonShippedReleaseLines"
						+ alShippedPrimeLines);
				//OMS-4932 Start: This is done to handlle the Set item shipment message when only parent will be coming in shipment xml
				Element eleOrderLineSetParent= GCXMLUtil.getElementByXPath(docGetOrderListOP, "OrderList/Order/OrderLines/OrderLine[@PrimeLineNo='"+primeLineNoSL+"']");
				String strProductClass= GCXMLUtil.getAttributeFromXPath(eleOrderLineSetParent, "Item/@ProductClass");
				String strExtnSetCode= GCXMLUtil.getAttributeFromXPath(eleOrderLineSetParent, "ItemDetails/Extn/@ExtnSetCode");

				if (GCConstants.PRODUCT_CLASS_SET.equals(strProductClass) && GCConstants.ONE.equals(strExtnSetCode)
						&& alShippedPrimeLines.contains(primeLineNoSL)) {
					LOGGER.verbose("Inside-- if Parent");
					String strOrderLineKey= eleOrderLineSetParent.getAttribute(GCConstants.ORDER_LINE_KEY);
					NodeList nlChildLine= GCXMLUtil.getNodeListByXpath(docGetOrderListOP, "OrderList/Order/OrderLines/OrderLine[@BundleParentOrderLineKey='"+strOrderLineKey+"']");
					for(int i=0; i< nlChildLine.getLength(); i++){
						LOGGER.verbose("Inside-- if Parent-- for loop");
						Element eleChildLine= (Element) nlChildLine.item(i);
						String strPrimeLineNo= eleChildLine.getAttribute(GCConstants.PRIME_LINE_NO);
						alShippedPrimeLines.add(strPrimeLineNo);
					}
					LOGGER.verbose(" alShippedPrimeLines is inside backOrderNonShippedReleaseLines--2"
							+ alShippedPrimeLines);

					//OMS-5024 Start
					String sWarrantyPrimeLineNo = GCXMLUtil
							.getAttributeFromXPath(
									docGetOrderListOP,
									"OrderList/Order/OrderLines/OrderLine[@MaxLineStatus='3200' or @MinLineStatus='3200']"
											+ " [ @DependentOnLineKey='"
											+ strOrderLineKey
											+ "']/@PrimeLineNo");
					String sExtnIsWarrantyItem = GCXMLUtil
							.getAttributeFromXPath(docGetOrderListOP,
									"OrderList/Order/OrderLines/OrderLine[@PrimeLineNo='"+sWarrantyPrimeLineNo+"']/Extn/@ExtnIsWarrantyItem");
					LOGGER.verbose("sWarrantyPrimeLineNo : " + sWarrantyPrimeLineNo);
					LOGGER.verbose("sExtnIsWarrantyItem : "+ sExtnIsWarrantyItem);
					//OMS-5209
					if (!YFCCommon.isVoid(sWarrantyPrimeLineNo) && GCConstants.FLAG_Y.equalsIgnoreCase(sExtnIsWarrantyItem)) {
						alShippedPrimeLines.add(sWarrantyPrimeLineNo);
						LOGGER.verbose(" alShippedPrimeLines is inside backOrderNonShippedReleaseLines--3"
								+ alShippedPrimeLines);
						//OMS-5209
					}
					//OMS-5024 End
				}
				//OMS-4932 End

			}

			for (Element eleOrderline : listOrderLine) {
				String primeLineNoOL = eleOrderline.getAttribute("PrimeLineNo");
				if (!alShippedPrimeLines.contains(primeLineNoOL)) {
					LOGGER.verbose("Non Shipped lines are present");
					String sExtnIsWarrantyItem = GCXMLUtil.getAttributeFromXPath(eleOrderline,
							"Extn/@ExtnIsWarrantyItem");
					String sDependentOnLineKey = eleOrderline.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
					String sDependentPrimeLineNo = GCXMLUtil.getAttributeFromXPath(docGetOrderListOP,
							"OrderList/Order/OrderLines/OrderLine[@MaxLineStatus='3200' or @MinLineStatus='3200'][@OrderLineKey='"
									+ sDependentOnLineKey
									+ "']/@PrimeLineNo");
					if (!(alShippedPrimeLines.contains(sDependentPrimeLineNo) && GCConstants.YES.equalsIgnoreCase(sExtnIsWarrantyItem))) {
						alNonShippedOrderLines.add(eleOrderline);
						LOGGER.verbose(" alNonShippedOrderLines is inside backOrderNonShippedReleaseLines"
								+ alNonShippedOrderLines);
					}
				}
			}

			if (!alNonShippedOrderLines.isEmpty()) {

				String sReleaseNo = GCXMLUtil.getAttributeFromXPath(eleShipment, "ShipmentLines/ShipmentLine/@ReleaseNo");

				Document docChangeReleaseIP = GCXMLUtil.createDocument(GCConstants.ORDER_RELEASE);
				Element eleChangeReleaseIP = docChangeReleaseIP.getDocumentElement();
				eleChangeReleaseIP.setAttribute(GCConstants.ACTION, "MODIFY");
				eleChangeReleaseIP.setAttribute("PutInventoryOnBackOrder", "Y");
				eleChangeReleaseIP.setAttribute("DocumentType", "0001");
				eleChangeReleaseIP.setAttribute("Override", "Y");
				eleChangeReleaseIP.setAttribute("ReleaseNo", sReleaseNo);
				eleChangeReleaseIP.setAttribute("OrderNo", sOrderNo);
				eleChangeReleaseIP.setAttribute("EnterpriseCode", sEnterpriseCode);
				Element eleOrderLinesChngRelease = GCXMLUtil.createElement(docChangeReleaseIP, GCConstants.ORDER_LINES, null);
				eleChangeReleaseIP.appendChild(eleOrderLinesChngRelease);

				// Move the Backordered lines to BackOrderedDAX so that they are
				// not
				// sent to DAX for allocation
				Element eleOrder = (Element) XPathAPI.selectSingleNode(docGetOrderListOP, "OrderList/Order");
				String sOrderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
				Document docChangeBackOrderStatus = GCCommonUtil.prepareChangeOrderStatusHdrDoc(sOrderHeaderKey,
						GCConstants.CHANGE_CONFIRM_STATUS);

				for (Element eleOrderline : alNonShippedOrderLines) {

					Element eleOrderLineChngRelease = GCXMLUtil.createElement(docChangeReleaseIP, GCConstants.ORDER_LINE, null);
					eleOrderLineChngRelease.setAttribute(GCConstants.ACTION,"BACKORDER");
					eleOrderLineChngRelease.setAttribute("ChangeInQuantity", "-" + eleOrderline.getAttribute("StatusQuantity"));
					eleOrderLineChngRelease.setAttribute("PrimeLineNo", eleOrderline.getAttribute("PrimeLineNo"));
					eleOrderLineChngRelease.setAttribute("SubLineNo", eleOrderline.getAttribute("SubLineNo"));
					eleOrderLinesChngRelease.appendChild(eleOrderLineChngRelease);

					GCCommonUtil.addLinesToChangeOrderStatusDoc(docChangeBackOrderStatus, eleOrderline,

							eleOrderline.getAttribute("StatusQuantity"), GCConstants.BACKORDERED_OMS_STATUS);

				}
				if (LOGGER.isDebugEnabled()) {
					LOGGER.verbose(" docChangeReleaseIP is"
							+ GCXMLUtil.getXMLString(docChangeReleaseIP));
				}
				GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_RELEASE, docChangeReleaseIP, null);

				if (LOGGER.isDebugEnabled()) {
					LOGGER.verbose("backOrderNonShippedReleaseLines: Input document for changeOrderStatus after changeRelease is"
							+ GCXMLUtil.getXMLString(docChangeBackOrderStatus));
				}
				if (alShippedPrimeLines.isEmpty()) {
					isCompleteBackordered = true;
				}
				GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER_STATUS, docChangeBackOrderStatus);

			}

		} catch (Exception e) {
			LOGGER.error(
					" Inside catch of GCProcessConfirmShipmentAPI.processOrdersOnScheduled()",
					e);
			throw new GCException(e);
		}
		return isCompleteBackordered;

	}

	/**
	 *
	 * GCSTORE-1901:This method adds sourcing controls to suppress sourcing from the given shipnode
	 * for the orderline.
	 *
	 * @param docChangeOrder
	 * @param sPrimeLineNo
	 * @param sSubLineNo
	 * @param shipNode
	 * @return
	 * @throws GCException
	 *
	 */
	private static Element appendSOChangeOrderLine(Document docChangeOrder, String sPrimeLineNo, String sSubLineNo,
			String shipNode) throws GCException {
		LOGGER.verbose(" Entering GCProcessConfirmShipmentAPI.appendSOChangeOrderLine() method ");
		if (LOGGER.isDebugEnabled()) {
			LOGGER.verbose(" Input document is" + GCXMLUtil.getXMLString(docChangeOrder));
		}
		//VDS - 87 --> IF ShipNode = "GCVendorNode" --> suppress sourcing from PrimaryVendor node too
		String sExtnVendorId = docChangeOrder.getDocumentElement().getAttribute("ExtnVendorId");
		docChangeOrder.getDocumentElement().removeAttribute("ExtnVendorId");
		Element eleChangeOrderLine = GCXMLUtil.createElement(docChangeOrder, GCConstants.ORDER_LINE, null);

		eleChangeOrderLine.setAttribute(GCConstants.ACTION, GCConstants.MODIFY);
		eleChangeOrderLine.setAttribute(GCConstants.PRIME_LINE_NO, sPrimeLineNo);
		eleChangeOrderLine.setAttribute(GCConstants.SUB_LINE_NO, sSubLineNo);
		Element eleOrderLineSourcingCntrls =
				GCXMLUtil.createElement(docChangeOrder, GCConstants.ORDER_LINE_SOURCING_CNTRLS, null);
		Element eleOrderLineSourcingCntrl =
				GCXMLUtil.createElement(docChangeOrder, GCConstants.ORDER_LINE_SOURCING_CNTRL, null);
		eleOrderLineSourcingCntrl.setAttribute(GCConstants.NODE, shipNode);
		eleOrderLineSourcingCntrl.setAttribute(GCConstants.SUPPRESS_SOURCING, GCConstants.YES);
		eleOrderLineSourcingCntrls.appendChild(eleOrderLineSourcingCntrl);
		if(YFCCommon.equalsIgnoreCase(shipNode, "GCVendorNode")){
			Element eleOrderLineSourcingCntrlforPrimaryVendor =
					GCXMLUtil.createElement(docChangeOrder, GCConstants.ORDER_LINE_SOURCING_CNTRL, null);
			eleOrderLineSourcingCntrlforPrimaryVendor.setAttribute(GCConstants.NODE, sExtnVendorId);
			eleOrderLineSourcingCntrlforPrimaryVendor.setAttribute(GCConstants.SUPPRESS_SOURCING, GCConstants.YES);
			eleOrderLineSourcingCntrls.appendChild(eleOrderLineSourcingCntrlforPrimaryVendor);
		}
		eleChangeOrderLine.appendChild(eleOrderLineSourcingCntrls);
		LOGGER.verbose(" Exiting GCProcessConfirmShipmentAPI.appendSOChangeOrderLine() method ");
		return eleChangeOrderLine;
	}

	/**
	 *
	 * Description of appendChangeOrderLine Append OrderLine to the Change Order Document
	 *
	 * @param docChangeOrder
	 * @param sPrimeLineNo
	 * @param sSubLineNo
	 * @param shipNode
	 * @param dRemainingQty
	 * @return
	 * @throws GCException
	 *
	 */
	private static Element appendChangeOrderLine(Document docChangeOrder,
			String sPrimeLineNo, String sSubLineNo, double dRemainingQty)
					throws GCException {
		LOGGER.verbose(" Entering GCProcessConfirmShipmentAPI.appendChangeOrderLine() method ");
		if (LOGGER.isDebugEnabled()) {
			LOGGER.verbose(" Input document is"
					+ GCXMLUtil.getXMLString(docChangeOrder));
		}
		LOGGER.verbose(" dRemainingQty " + dRemainingQty);
		Element eleChangeOrderLine = GCXMLUtil.createElement(docChangeOrder,
				GCConstants.ORDER_LINE, null);
		try {
			eleChangeOrderLine.setAttribute(GCConstants.ACTION, GCConstants.MODIFY);
			eleChangeOrderLine.setAttribute(GCConstants.PRIME_LINE_NO, sPrimeLineNo);
			eleChangeOrderLine.setAttribute(GCConstants.SUB_LINE_NO, sSubLineNo);
			eleChangeOrderLine.setAttribute(GCConstants.ORDERED_QTY, String.valueOf(dRemainingQty));
		} catch (Exception e) {
			LOGGER.error(
					" Inside catch block of GCProcessConfirmShipmentAPI.appendChangeOrderLine(), Exception is :",
					e);
			throw new GCException(e);
		}
		LOGGER.verbose(" Exiting GCProcessConfirmShipmentAPI.appendChangeOrderLine() method ");
		return eleChangeOrderLine;
	}

	/**
	 *
	 * @param env
	 * @param sDependentOrderLineKey
	 * @return
	 * @throws GCException
	 */
	private static String fetchReleaseNo(YFSEnvironment env,
			String sDependentOrderLineKey) throws GCException {
		LOGGER.verbose(" Entering GCProcessConfirmShipmentAPI.fetchReleaseNo() method ");
		String sReleaseNo = "";
		try {
			Document docGetOrderReleaseList = GCXMLUtil.getDocument("<OrderRelease>" + "<OrderLine OrderLineKey='"
					+ sDependentOrderLineKey + "'/>" + "</OrderRelease>");
			Document docGetOrderReleaseListTmp = GCXMLUtil.getDocument("<OrderReleaseList LastOrderReleaseKey=''>"
					+ "<OrderRelease ReleaseNo='' OrderReleaseKey=''/></OrderReleaseList>");
			Document docGetOrderReleaseListOP = GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_RELEASE_LIST,
					docGetOrderReleaseList, docGetOrderReleaseListTmp);

			//OMS-4660 Starts
			String strLastOrderRK= docGetOrderReleaseListOP.getDocumentElement().getAttribute(GCConstants.LAST_ORDER_RELEASE_KEY);
			sReleaseNo = GCXMLUtil.getAttributeFromXPath(docGetOrderReleaseListOP,
					"OrderReleaseList/OrderRelease[@OrderReleaseKey='"+strLastOrderRK+"']/@ReleaseNo");
			//OMS-4660 End

			LOGGER.verbose(" sReleaseNo " + sReleaseNo);

		} catch (Exception e) {
			LOGGER.error(
					" Inside catch block of GCProcessConfirmShipmentAPI.fetchReleaseNo(), Exception is :",
					e);
			throw new GCException(e);
		}
		LOGGER.verbose(" Exiting GCProcessConfirmShipmentAPI.fetchReleaseNo() method ");
		return sReleaseNo;
	}

	/**
	 *
	 * @param env
	 * @param inDoc
	 * @param sOrderNo
	 * @return
	 * @throws GCException
	 */
	private static String fetchOrderReleaseKey(YFSEnvironment env,
			Document inDoc, String sOrderNo) throws GCException {
		String sOrderReleaseKey = "";
		try {
			String sReleaseNo = GCXMLUtil.getAttributeFromXPath(inDoc,
					"Shipment/ShipmentLines/ShipmentLine/@ReleaseNo");
			Document docGetOrderReleaseList = GCXMLUtil.createDocument(GCConstants.ORDER_RELEASE);
			Element eleRoot = docGetOrderReleaseList.getDocumentElement();
			eleRoot.setAttribute(GCConstants.RELEASE_NO, sReleaseNo);
			eleRoot.setAttribute(GCConstants.SALES_ORDER_NO, sOrderNo);
			eleRoot.setAttribute(GCConstants.SHIP_NODE, inDoc.getDocumentElement().getAttribute(GCConstants.SHIP_NODE));

			Document docGetOrderReleaseListTmp = GCXMLUtil.getDocument("<OrderReleaseList>"
					+ "<OrderRelease OrderReleaseKey=''/></OrderReleaseList>");
			Document docGetOrderReleaseListOP = GCCommonUtil.invokeAPI(env,GCConstants.API_GET_ORDER_RELEASE_LIST,
					docGetOrderReleaseList, docGetOrderReleaseListTmp);
			sOrderReleaseKey = GCXMLUtil.getAttributeFromXPath(docGetOrderReleaseListOP,
					"OrderReleaseList/OrderRelease/@OrderReleaseKey");

			LOGGER.verbose(" sOrderReleaseKey " + sOrderReleaseKey);

		} catch (Exception e) {
			LOGGER.error(
					" Inside catch block of GCProcessConfirmShipmentAPI.fetchOrderReleaseList(), Exception is :",
					e);
			throw new GCException(e);
		}
		return sOrderReleaseKey;
	}

	/**
	 * <h3>Description:</h3> This method is used set the service arguments
	 *
	 * @param props The service arguments
	 */
	@Override
	public void setProperties(final Properties props) {

		// configure as a static value
		this.props = props;
	}


	/**
	 * <h3>Description:</h3> This method is used to get the flag value for the input argument
	 *
	 * @author Infosys Limited
	 * @param strFlagName the flag name
	 * @return The boolean value of the flag from the configured property if config not present
	 *         returns true
	 */
	private static boolean getFlagValue(final String strFlagName) {

		// logging the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getFlagValue:entry:Input=" + strFlagName);
		}

		// the return flag
		boolean bRetFlag = true;

		// check if the properties set
		if (props != null && StringUtils.isNotBlank(strFlagName)) {

			// fetch the argument
			final String strFlagValue = props.getProperty(strFlagName.trim());

			// check if the string is not blank and Y
			if (GCConstants.YES.equalsIgnoreCase(strFlagValue)) {

				// set the flag to false
				bRetFlag = false;
			}
		}

		// logging the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getFlagValue:exit:Value=" + bRetFlag);
		}

		// return the flag
		return bRetFlag;
	}


	/**
	 *
	 * @param mWarrantyInfo
	 * @param docGetOrderListOP
	 * @param lsWarrantyLines
	 * @return
	 * @throws GCException
	 */
	private static Map<String,List<String>> findListOfComponentsForAssociatedToWarranty(Map<String,String> mWarrantyInfo,Document docGetOrderListOP,List<String> lsWarrantyLines)throws GCException{
		int listSize=lsWarrantyLines.size();
		String warrantyPrimeLineNo=null;
		Map<String,List<String>> mComponentsWithBundleParentOrderLineKey=new HashMap<String,List<String>>();
		try {
			for(int count=0;count<listSize;count++){
				List<String> componentsItemIds=new ArrayList<String>();
				warrantyPrimeLineNo=lsWarrantyLines.get(count);
				String warrantyOrderLineKey=mWarrantyInfo.get(warrantyPrimeLineNo);
				NodeList nlOrderLine=XPathAPI.selectNodeList(docGetOrderListOP,  "OrderList/Order/OrderLines/OrderLine[BundleParentLine/@OrderLineKey='"
						+ warrantyOrderLineKey + "']" );
				int nlOrderLineCount=nlOrderLine.getLength();
				for(int i=0;i<nlOrderLineCount;i++){
					Element eleOrderLine=(Element)nlOrderLine.item(i);
					Element eleItem=(Element)XPathAPI.selectSingleNode(eleOrderLine,"Item");
					String sItemId=eleItem.getAttribute("ItemID");
					componentsItemIds.add(sItemId);
				}
				mComponentsWithBundleParentOrderLineKey.put(warrantyPrimeLineNo, componentsItemIds);
			}

		} catch (Exception e) {
			LOGGER.error(
					" Inside catch block of GCProcessConfirmShipmentAPI.findListOfComponentsForAssociatedToWarranty(), Exception is :",
					e);
			throw new GCException(e);
		}
		return mComponentsWithBundleParentOrderLineKey;
	}

	/**
	 *
	 * Description of createSOConfirmShipmentXml: this method finds the sales order information
	 * corresponding to the PO and makes confirmShipment xml corresponding to it.
	 *
	 * @param env
	 * @param inDoc
	 * @throws GCException
	 *
	 */
	private static String createSOConfirmShipmentXml(YFSEnvironment env, Document inDoc) throws GCException {
		LOGGER.verbose("Entering inside createSOConfirmShipmentXml method of GCProcessConfirmShipment class, inDoc" +GCXMLUtil.getXMLString(inDoc));
		try {
			//OMS-4668 Begins:
			YFCDocument confirmShipOutdoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
			NodeList nlistShipmentLine=XPathAPI.selectNodeList(inDoc,"Shipment/ShipmentLines/ShipmentLine");
			int count=nlistShipmentLine.getLength();
			String strOrderNo="";
			if(count>0){
				Element eleShipmentLine= (Element) nlistShipmentLine.item(0);
				strOrderNo=eleShipmentLine.getAttribute("OrderNo");
				Document getPurchaseOrderListOutDoc=GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST,
						GCXMLUtil.getDocument("<Order OrderNo='"+strOrderNo+"' />"),
						GCXMLUtil.getDocument("<OrderList><Order OrderNo=''><OrderLines>"
								+"<OrderLine KitCode='' ChainedFromOrderHeaderKey='' ChainedFromOrderLineKey='' PrimeLineNo=''>"
								+"<ItemDetails ItemID='' />"
								+"</OrderLine>"
								+"</OrderLines>"
								+"</Order>"
								+"</OrderList>"));

				NodeList nlistOrderLine=XPathAPI.selectNodeList(getPurchaseOrderListOutDoc,"OrderList/Order/OrderLines/OrderLine");
				int length=nlistOrderLine.getLength();
				String sChainedFromOrderHeaderKey="";
				String sChainedFromOrderLineKey="";
				Element elePOShipment=inDoc.getDocumentElement();

				Document docSOConfirmShipmentInput=GCXMLUtil.createDocument("Shipment");
				Element eleShipment=docSOConfirmShipmentInput.getDocumentElement();
				eleShipment.setAttribute(GCConstants.BACKORDER_NON_SHIPPED_QTY,elePOShipment.getAttribute(GCConstants.BACKORDER_NON_SHIPPED_QTY));

				Element eleShipmentExtn = GCXMLUtil.createElement(docSOConfirmShipmentInput,
						GCConstants.EXTN, null);
				eleShipment.appendChild(eleShipmentExtn);

				Element eleShipmentLines = GCXMLUtil.createElement(docSOConfirmShipmentInput,
						GCConstants.SHIPMENT_LINES, null);
				eleShipment.appendChild(eleShipmentLines);

				Element eleContainers=GCXMLUtil.createElement(docSOConfirmShipmentInput, GCConstants.CONTAINERS, null);
				eleShipment.appendChild(eleContainers);

				Element eleContainer=GCXMLUtil.createElement(docSOConfirmShipmentInput, GCConstants.CONTAINER, null);
				eleContainer.setAttribute(GCConstants.TRACKING_NO, elePOShipment.getAttribute("TrackingNo"));
				eleContainers.appendChild(eleContainer);

				Element eleContainerDetails=GCXMLUtil.createElement(docSOConfirmShipmentInput, GCConstants.CONTAINER_DETAILS, null);
				eleContainer.appendChild(eleContainerDetails);

				Element eleContainerExtn=GCXMLUtil.createElement(docSOConfirmShipmentInput, GCConstants.EXTN, null);
				eleContainerExtn.setAttribute(GCConstants.EXTN_CARRIER_SERVICE_CODE,elePOShipment.getAttribute(GCConstants.CARRIER_SERVICE_CODE));
				eleContainer.appendChild(eleContainerExtn);
				boolean alreadyAppended=false,warrantyExist=false;
				for(int j=0;j<length;j++){
				    
					Element eleOrderLine=(Element)nlistOrderLine.item(j);
					
					//Fix to avoid blank getOrderList call-starts
                    String strKitCode=eleOrderLine.getAttribute("KitCode");
                    LOGGER.debug("createSOConfirmShipmentXml :: Kit Code ::"+strKitCode);
                    if(!YFCCommon.isVoid(strKitCode) && GCConstants.BUNDLE.equals(strKitCode))
                    {
                        LOGGER.debug("Skipping curent iteration, as it is a Bundle orderline");
                        continue;
                    }
                    //Fix to avoid blank getOrderList call-ends
					sChainedFromOrderHeaderKey=eleOrderLine.getAttribute(GCConstants.CHAINED_FROM_ORDER_HEADER_KEY);
					if(!YFCCommon.isVoid(sChainedFromOrderHeaderKey))
                    {
					sChainedFromOrderLineKey=eleOrderLine.getAttribute(GCConstants.CHAINED_FROM_ORDER_LINE_KEY);
					String sPrimeLineNo=eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
					Document getSalesOrderListOutDoc=GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST,
							GCXMLUtil.getDocument("<Order OrderHeaderKey='"+sChainedFromOrderHeaderKey+"' />"),
							GCXMLUtil.getDocument("<OrderList><Order OrderNo='' DocumentType='' EnterpriseCode=''><OrderLines>"
									+ "<OrderLine ShipNode='' PrimeLineNo='' SubLineNo='' Quantity='' ReleaseNo='' OrderLineKey='' DependentOnLineKey='' >"
									+"<ItemDetails ItemID='' UnitOfMeasure='' />"
									+"<OrderStatuses>"
									+"<OrderStatus StatusQty='' ShipNode='' TotalQuantity='' OrderReleaseKey='' Status=''/>"
									+"</OrderStatuses>"
									+"</OrderLine>"
									+"</OrderLines>"
									+"</Order>"
									+"</OrderList>"));
					Element eleSalesOrder=(Element)XPathAPI.selectSingleNode(getSalesOrderListOutDoc,"OrderList/Order");
					Element eleSalesOrderLine=(Element)XPathAPI.selectSingleNode(eleSalesOrder,"OrderLines/OrderLine[@DependentOnLineKey='"+sChainedFromOrderLineKey+"']");
					if(eleSalesOrderLine!=null){
						Element eleItemDetails=(Element)XPathAPI.selectSingleNode(eleSalesOrderLine,"ItemDetails");
						Element eleOrderStatus=(Element)XPathAPI.selectSingleNode(eleSalesOrderLine,"OrderStatuses/OrderStatus");
						String sStatus=eleOrderStatus.getAttribute(GCConstants.STATUS);
						if("3200".equals(sStatus) || "3700".equals(sStatus)){
							String sOrderReleaseKey=eleOrderStatus.getAttribute(GCConstants.ORDER_RELEASE_KEY);
							Document getOrderReleaseListOutDoc=GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_RELEASE_LIST,
									GCXMLUtil.getDocument("<OrderRelease OrderReleaseKey='"+sOrderReleaseKey+"'/>"),
									GCXMLUtil.getDocument("<OrderReleaseList><OrderRelease ReleaseNo='' /></OrderReleaseList>"));
							Element eleOrderRelease=(Element)XPathAPI.selectSingleNode(getOrderReleaseListOutDoc,"OrderReleaseList/OrderRelease");
							String sReleaseNo=eleOrderRelease.getAttribute(GCConstants.RELEASE_NO);

							//Making ShipmentLine and ShipmentLine/Extn Elements and appending them in correct hierarachy
							Element eleNewShipmentLine = GCXMLUtil.createElement(docSOConfirmShipmentInput,
									GCConstants.SHIPMENT_LINE, null);
							eleNewShipmentLine.setAttribute(GCConstants.UNIT_OF_MEASURE,eleItemDetails.getAttribute(GCConstants.UNIT_OF_MEASURE));
							eleNewShipmentLine.setAttribute(GCConstants.PRIME_LINE_NO,eleSalesOrderLine.getAttribute(GCConstants.PRIME_LINE_NO));
							eleNewShipmentLine.setAttribute(GCConstants.ORDER_NO,eleSalesOrder.getAttribute(GCConstants.ORDER_NO));
							eleNewShipmentLine.setAttribute(GCConstants.ITEM_ID,eleItemDetails.getAttribute(GCConstants.ITEM_ID));
							String sQuantity=GCXMLUtil.getAttributeFromXPath(inDoc, "Shipment/ShipmentLines/ShipmentLine[@PrimeLineNo='"+sPrimeLineNo+"']/@Quantity");
							eleNewShipmentLine.setAttribute(GCConstants.QUANTITY,sQuantity);
							eleNewShipmentLine.setAttribute(GCConstants.RELEASE_NO,sReleaseNo);
							eleNewShipmentLine.setAttribute(GCConstants.SUB_LINE_NO,eleSalesOrderLine.getAttribute(GCConstants.SUB_LINE_NO));

							Element eleExtn = GCXMLUtil.createElement(docSOConfirmShipmentInput,
									GCConstants.EXTN, null);
							eleExtn.setAttribute(GCConstants.EXTN_EXPECTED_QUANTITY, eleOrderStatus.getAttribute(GCConstants.TOTAL_QUANTITY));
							eleNewShipmentLine.appendChild(eleExtn);
							eleShipmentLines.appendChild(eleNewShipmentLine);



							//Making Containers element and appending it in correct hierarchy

							Element eleContainerShipmentLine = GCXMLUtil.createElement(docSOConfirmShipmentInput,
									GCConstants.SHIPMENT_LINE, null);
							eleContainerShipmentLine.setAttribute(GCConstants.UNIT_OF_MEASURE,eleItemDetails.getAttribute(GCConstants.UNIT_OF_MEASURE));
							eleContainerShipmentLine.setAttribute(GCConstants.PRIME_LINE_NO,eleSalesOrderLine.getAttribute(GCConstants.PRIME_LINE_NO));
							eleContainerShipmentLine.setAttribute(GCConstants.ORDER_NO,eleSalesOrder.getAttribute(GCConstants.ORDER_NO));
							eleContainerShipmentLine.setAttribute(GCConstants.ITEM_ID,eleItemDetails.getAttribute(GCConstants.ITEM_ID));
							eleContainerShipmentLine.setAttribute(GCConstants.RELEASE_NO,sReleaseNo);
							eleContainerShipmentLine.setAttribute(GCConstants.SUB_LINE_NO,eleSalesOrderLine.getAttribute(GCConstants.SUB_LINE_NO));

							Element eleContainerDetail=GCXMLUtil.createElement(docSOConfirmShipmentInput, GCConstants.CONTAINER_DETAIL, null);
							eleContainerDetail.setAttribute(GCConstants.QUANTITY, sQuantity);
							eleContainerDetail.appendChild(eleContainerShipmentLine);
							eleContainerDetails.appendChild(eleContainerDetail);
							warrantyExist=true;
							if(!alreadyAppended){
								eleShipment.setAttribute(GCConstants.SHIP_NODE,eleOrderStatus.getAttribute(GCConstants.SHIP_NODE));
								eleShipment.setAttribute(GCConstants.DOCUMENT_TYPE,eleSalesOrder.getAttribute(GCConstants.DOCUMENT_TYPE));
								eleShipment.setAttribute(GCConstants.ENTERPRISE_CODE,eleSalesOrder.getAttribute(GCConstants.ENTERPRISE_CODE));
								eleShipmentExtn.setAttribute(GCConstants.EXTN_PICK_REQUEST_NO,eleSalesOrder.getAttribute(GCConstants.ORDER_NO)+"R1");
								alreadyAppended=true;
							}

						} else if ("1600".equals(sStatus)) {
							// call createChainedOrder API to make the warranty purcahse order move to
							// ChainedOrderCreated Status.
							YFCDocument createChainedOrdrInDoc = YFCDocument.createDocument(GCConstants.ORDER);
							createChainedOrdrInDoc.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY,
									sChainedFromOrderHeaderKey);
							GCCommonUtil.invokeAPI(env, "createChainedOrder", createChainedOrdrInDoc, null);
							// code to ship the PO Associated to the warranty line
							// String orderLineKey = eleSalesOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
							// String shipNode = eleSalesOrderLine.getAttribute(GCConstants.SHIP_NODE);
							// shipWarrantyPO(env, orderLineKey, shipNode);

						} else if ("2100".equals(sStatus)) {
							// code to ship the PO Associated to the warranty line
							String orderLineKey = eleSalesOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
							String shipNode = eleSalesOrderLine.getAttribute(GCConstants.SHIP_NODE);
							shipWarrantyPO(env, orderLineKey, shipNode);
						} else {
							YFCException yfcEx = new YFCException(GCErrorConstants.CONFIRM_SHIPMENT_FAILURE);
							yfcEx.setErrorDescription("OrderLine Status is invalid for shipping");
							throw yfcEx;
						}
					}
				}
				}
				if(warrantyExist){
					LOGGER.verbose(
							" Warranty exist in Sales Order corresponding to Purchase Order Item ");
					confirmShipOutdoc = GCCommonUtil.invokeAPI(env, GCConstants.API_CONFIRM_SHIPMENT,
							YFCDocument.getDocumentFor(docSOConfirmShipmentInput), YFCDocument.getDocumentFor(GCConstants.CONFIRM_SHIP_TEMP));

					YFCElement shipOutEle = confirmShipOutdoc.getDocumentElement();
					if(LOGGER.isVerboseEnabled()){
						LOGGER.verbose("Confirm Shipment Outdoc :" +shipOutEle.toString());
					}

					GCConfirmShipmentAPI invObj = new GCConfirmShipmentAPI();
					invObj.callCreateShipInvoiceAPI(env, shipOutEle);

				}else{
					LOGGER.verbose(
							"Warranty does not exist in Sales Order corresponding to Purchase Order Item ");
				}
				// GCSTORE-1901:Begin
				return sChainedFromOrderHeaderKey;
				// GCSTORE-1901::end
			}
			LOGGER.verbose("Exiting from createSOConfirmShipmentXml method of GCProcessConfirmShipment class");
		} catch (Exception e) {
			LOGGER.error(
					" Inside catch block of GCProcessConfirmShipmentAPI.createSOConfirmShipmentXml(), Exception is :",
					e);
			throw new GCException(e);

		}

		return null;
	}

	private static void shipWarrantyPO(YFSEnvironment env, String orderLineKey, String shipNode) {
		LOGGER.beginTimer("shipWarrantyPO");
		YFCDocument getOrderLineListInDoc = YFCDocument.createDocument(GCConstants.ORDER_LINE);
		getOrderLineListInDoc.getDocumentElement().setAttribute(GCConstants.CHAINED_FROM_ORDER_LINE_KEY, orderLineKey);
		YFCDocument getOrderLineListOutDoc =
				GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LINE_LIST, getOrderLineListInDoc, null);
		YFCElement orderLineEle = getOrderLineListOutDoc.getDocumentElement().getChildElement(GCConstants.ORDER_LINE);
		if (!YFCCommon.isVoid(orderLineEle)) {
			String purchanseOrderHdrKey = orderLineEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
			YFCDocument changeOrderInDoc = YFCDocument.createDocument(GCConstants.ORDER);
			changeOrderInDoc.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, purchanseOrderHdrKey);
			changeOrderInDoc.getDocumentElement().setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
			YFCElement changeOrderOrderLines = changeOrderInDoc.getDocumentElement().createChild(GCConstants.ORDER_LINES);
			YFCDocument getOrderListInDoc = YFCDocument.createDocument(GCConstants.ORDER);
			getOrderListInDoc.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, purchanseOrderHdrKey);
			YFCDocument gteOrderListTmpl =
					YFCDocument
					.getDocumentFor("<OrderList><Order OrderNo='' DocumentType='' EnterpriseCode='' SellerOrganizationCode='' ><OrderLines>"
							+ "<OrderLine PrimeLineNo='' SubLineNo='' OrderedQty=''  OrderLineKey='' ShipNode='' >"
							+ "<Item ItemID='' UnitOfMeasure='' ProductClass='' />"
							+ "</OrderLine>"
							+ "</OrderLines>"
							+ "</Order>" + "</OrderList>");

			YFCDocument getOrderListOutDoc =
					GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListInDoc, gteOrderListTmpl);
			YFCElement orderEle = getOrderListOutDoc.getDocumentElement().getChildElement(GCConstants.ORDER);
			String orderNo = orderEle.getAttribute(GCConstants.ORDER_NO);
			YFCDocument confirmShipmentInDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
			YFCElement shipmentEle = confirmShipmentInDoc.getDocumentElement();
			shipmentEle.setAttribute("OverrideModificationRules", "Y");
			shipmentEle.setAttribute("DocumentType", "0005");
			shipmentEle.setAttribute("EnterpriseCode", orderEle.getAttribute(GCConstants.ENTERPRISE_CODE));
			shipmentEle.setAttribute("DocumentType", "0005");
			shipmentEle.setAttribute("SellerOrganizationCode", orderEle.getAttribute(GCConstants.SELLER_ORGANIZATION_CODE));
			shipmentEle.setAttribute(GCConstants.SHIP_NODE, shipNode);
			YFCElement shipmentLinesEle = shipmentEle.createChild(GCConstants.SHIPMENT_LINES);
			YFCNodeList<YFCElement> orderLineNodeLst = orderEle.getElementsByTagName(GCConstants.ORDER_LINE);
			for (YFCElement eleOrderLine : orderLineNodeLst) {
				YFCElement changeOrderLine = changeOrderOrderLines.createChild(GCConstants.ORDER_LINE);
				changeOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
				changeOrderLine.setAttribute(GCConstants.SHIP_NODE, shipNode);
				YFCElement shipmentLineEle = shipmentLinesEle.createChild(GCConstants.SHIPMENT_LINE);
				shipmentLineEle.setAttribute(GCConstants.ORDER_NO, orderNo);
				shipmentLineEle.setAttribute(GCConstants.PRIME_LINE_NO, eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO));
				shipmentLineEle.setAttribute(GCConstants.SUB_LINE_NO, eleOrderLine.getAttribute(GCConstants.SUB_LINE_NO));
				shipmentLineEle.setAttribute(GCConstants.QUANTITY, eleOrderLine.getAttribute(GCConstants.ORDERED_QTY));
				YFCElement itemEle = eleOrderLine.getChildElement(GCConstants.ITEM);
				shipmentLineEle.setAttribute(GCConstants.ITEM_ID, itemEle.getAttribute(GCConstants.ITEM_ID));
				shipmentLineEle.setAttribute(GCConstants.UOM, itemEle.getAttribute(GCConstants.UOM));
				shipmentLineEle.setAttribute(GCConstants.PRODUCT_CLASS, itemEle.getAttribute(GCConstants.PRODUCT_CLASS));


			}
			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("ChangeOrder InDoc::" + changeOrderInDoc.toString());
			}
			GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, changeOrderInDoc, null);
			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("ConfirmShipment XML for warranty PO shipment::" + confirmShipmentInDoc.toString());
			}

			YFCDocument getConfirmShipTemp = YFCDocument.getDocumentFor("<Shipment ShipmentKey='' />");
			YFCDocument shipOutDoc =
					GCCommonUtil.invokeAPI(env, GCConstants.CONFIRM_SHIPMENT_API, confirmShipmentInDoc, getConfirmShipTemp);

			// Creating Shipment Invoice
			YFCDocument createShipInvoiceIndoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
			YFCElement shipInvoiceEle = createShipInvoiceIndoc.getDocumentElement();
			shipInvoiceEle.setAttribute(GCConstants.SELLER_ORGANIZATION_CODE,
					orderEle.getAttribute(GCConstants.SELLER_ORGANIZATION_CODE));
			shipInvoiceEle.setAttribute(GCConstants.SHIP_NODE, shipNode);
			shipInvoiceEle.setAttribute(GCConstants.SHIPMENT_KEY,
					shipOutDoc.getDocumentElement().getAttribute(GCConstants.SHIPMENT_KEY));
			shipInvoiceEle.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.PO_CREATE_SHIP_INVOICE);
			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("createShipInvoice Indoc :" + createShipInvoiceIndoc.toString());
			}
			GCCommonUtil.invokeAPI(env, GCConstants.CREATE_SHPMNT_INVC_API, createShipInvoiceIndoc.getDocument());

		}
		LOGGER.endTimer("shipWarrantyPO");
	}
}

