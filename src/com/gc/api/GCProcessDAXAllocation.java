/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Processes the response from DAX
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               10/03/2014        Singh, Gurpreet            OMS-1473: DAX Processing
             1.1               29/05/2014        Soni, Karan                Made changes as per new design
             1.2               17/03/2015        Singh,Jaipreet             OMS-5094: Order status does not move from Partially Scheduled to Released for kit order
              																		  after backorder allocation message is received for one component
             1.3               30/03/2015        Khakha, Johnson            OMS- 5209: Order sent a release for qty 1 too great
             1.4               30/03/2015        Khakha, Johnson            OMS-5275: DAX is sending the backorder allocation message only for the set parent item.
			 1.5               25/05/2015        Infosys Ltd.               OMS-5574: DAX is sending the backorder allocation message only for the set parent item.
			 1.6               29/05/2015        Infosys Ltd.               OMS-5457: Change the line status to Scheduled PO DAX Allocated
 *#################################################################################################################################################################
 */
package com.gc.api;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.gc.exstore.utils.GCShipmentUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:gurpreet.singh ">Gurpreet</a>
 */
public class GCProcessDAXAllocation implements YIFCustomApi {

	/*
	 * The class name
	 */
	private static final String CLASS_NAME = GCProcessDAXAllocation.class
			.getName();

	/**
	 * Logger for Debugging purpose.
	 */
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCProcessDAXAllocation.class);

	/*
	 * The configurations
	 */
	private Properties props;
	private Map<String, Integer> olKeySchQtyMap = new HashMap<String, Integer>();
	private Map<String, Integer> olKeyNewSchQtyMap = new HashMap<String, Integer>();

	/**
	 * Description of resolveDAXAllocationHold Contains the logic for allocating
	 * the inventory on the Order Lines.
	 * 
	 * @param env
	 *            - YFSEnvironment transaction Variable used to invoke APIs.
	 * @param inDoc
	 *            - Dax Allocation Message.
	 * @return inDoc.
	 * 
	 */
	public Document processDAXAllocation(YFSEnvironment env, Document inDoc)
			throws GCException {

		LOGGER.debug(" Entering GCProcessDAXAllocation.resolveDAXAllocationHold() method");
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(" resolveDAXAllocationHold() method, Input document is "
					+ GCXMLUtil.getXMLString(inDoc));
		}
		try {
			Document docGetOrderListOP = getOrderListOutputDoc(env, inDoc);

			// Update the inputDoc with the actual ordered Qty if input has Qty
			// greater than ordered Qty
			updateQty(docGetOrderListOP, inDoc);

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(" resolveDAXAllocationHold() method, docChangeOrderStatusCreate "
						+ GCXMLUtil.getXMLString(docGetOrderListOP));
			}

			String sOrderHeaderKey = GCXMLUtil.getAttributeFromXPath(
					docGetOrderListOP,
					GCConstants.XPATH_ORDLST_ORD_ATTR_ORDHDRKEY);
			/*
			 * docChgOrdStatsFrmSchdPDXToOthr is a document used to change the
			 * Orderline Status from SchedulePODAX Status to PODAXUnschedule or
			 * RecievedPODAX or both. And the Document is formed for set, kit
			 * and Regular Item.
			 */
			Document docChgOrdStatsFrmSchdPDXToOthr = GCCommonUtil
					.prepareChangeOrderStatusHdrDoc(sOrderHeaderKey,
							GCConstants.CHANGE_SCHEDULE_STATUS);
			/*
			 * docChgOrdStatsFrmRecvPDXToPDXUnSchd is a document used to change
			 * the Orderline Status from RecievedPODAX Status to
			 * PODAXUnschedule. And the Document is formed for set, kit and
			 * Regular Item.
			 */
			Document docChgOrdStatsFrmRecvPDXToPDXUnSchd = GCCommonUtil
					.prepareChangeOrderStatusHdrDoc(sOrderHeaderKey,
							GCConstants.GC_RECIEVED_PODAX_TRANS_ID);
			/*
			 * docChangeOrderSchedule is a document used to prepare
			 * changeOrderSchedule input XML.
			 */
			Document docChangeOrderSchedule = prepareChangeScheduleDoc(sOrderHeaderKey);
			Element eleFromShedules = GCXMLUtil.createElement(
					docChangeOrderSchedule, GCConstants.FROM_SCHEDULES, null);
			docChangeOrderSchedule.getDocumentElement().appendChild(
					eleFromShedules);

			/*
			 * affectedKitParentList contains the Kit's Parent OrderLineKey as
			 * key and its ordered quantity as its value.
			 */
			Map<String, String> affectedKitParentList = new HashMap<String, String>();

			/*
			 * kitOrdLinesDataMap contain the kit's parent orderLineKey (say: P)
			 * as its Key and a Map (say: M) as it's value.This map (M) contains
			 * the information of all its associated component used to form the
			 * changeOrderStatus and changeOrderSchedule input document.
			 */
			Map<String, LinkedHashMap<String, GCOrderLineStructForKitNSetItem>> kitOrdLinesDataMap = new HashMap<String, LinkedHashMap<String, GCOrderLineStructForKitNSetItem>>();

			String sPDXUnSchdNSchdOrdCall = GCConstants.STRING_00;

			NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc,
					GCConstants.XPATH_ORDER_ORDERLINES_ORDERLINE);
			int nlOrdLnLen = nlOrderLine.getLength();

			String sterlingCurrentDateTime = GCDateUtils
					.getCurrentTime(GCConstants.DATE_TIME_FORMAT);

			String sReqShipDate = GCXMLUtil.getAttributeFromXPath(
					docGetOrderListOP,
					GCConstants.XPATH_ORDERLIST_ORDER_ATTR_REQ_SHIP_DATE);

			String sToExpectedDate = null;

			if (YFCCommon.isVoid(sReqShipDate)) {
				sToExpectedDate = sterlingCurrentDateTime;
			} else {
				sToExpectedDate = sReqShipDate;
			}

			for (int iCounter = 0; iCounter < nlOrdLnLen; iCounter++) {

				Element eleOrderLine = (Element) nlOrderLine.item(iCounter);
				String sQuantityDax = eleOrderLine
						.getAttribute(GCConstants.QUANTITY);

				double dQuantityDax = (!YFCCommon.isVoid(sQuantityDax)) ? Double
						.valueOf(sQuantityDax) : 0.00;

				Element eleOrderLineGetOrderList = getAffectedOrdLnEle(
						docGetOrderListOP, eleOrderLine);

				String sOrderLineKey = eleOrderLineGetOrderList
						.getAttribute(GCConstants.ORDER_LINE_KEY);

				String bundleParentLineKey = null;
				boolean isSetOrdrLine = false;
				String kitCode = eleOrderLineGetOrderList
						.getAttribute(GCConstants.KIT_CODE);
				String sKitBundleQty = null;
				Element eleBundleParentLineElem = (Element) XPathAPI
						.selectSingleNode(eleOrderLineGetOrderList,
								GCConstants.BUNDLE_PARENT_LINE);
				Element parentOrdLnForkit = null;
				String sBundleQty = null;

				if (eleBundleParentLineElem != null) {

					bundleParentLineKey = eleBundleParentLineElem
							.getAttribute(GCConstants.ORDER_LINE_KEY);
					if (!affectedKitParentList.containsKey(bundleParentLineKey)) {
						parentOrdLnForkit = (Element) XPathAPI
								.selectSingleNode(docGetOrderListOP,
										"/OrderList/Order/OrderLines/OrderLine[@OrderLineKey='"
												+ bundleParentLineKey + "']");
						sBundleQty = parentOrdLnForkit
								.getAttribute(GCConstants.ORDERED_QTY);
					}

				} else if (GCConstants.BUNDLE.equals(kitCode)) {

					Element itemDetailsExtnEle = (Element) XPathAPI
							.selectSingleNode(eleOrderLineGetOrderList,
									GCConstants.XPATH_ITEM_DETAILS_EXTN);
					String extnSetCode = itemDetailsExtnEle
							.getAttribute(GCConstants.EXTN_SET_CODE);
					if (GCConstants.STRING_ONE.equals(extnSetCode)) {
						sKitBundleQty = eleOrderLineGetOrderList
								.getAttribute(GCConstants.ORDERED_QTY);
						isSetOrdrLine = true;
					}
				}

				Element ordLnOrdStatusELe = getRequiredOrdLnStatusElement(
						eleOrderLineGetOrderList, isSetOrdrLine);

				if (!YFCCommon.isVoid(ordLnOrdStatusELe)) {

					String sExpectedShipmentDate = getExpectShipDate(
							eleOrderLineGetOrderList, ordLnOrdStatusELe);

					sPDXUnSchdNSchdOrdCall = GCConstants.STRING_11;

					if (isSetOrdrLine) {
						formChngOrdStatusXMLForSetItem(sOrderLineKey,
								sKitBundleQty, dQuantityDax, sToExpectedDate,
								docGetOrderListOP,
								docChgOrdStatsFrmSchdPDXToOthr,
								docChgOrdStatsFrmRecvPDXToPDXUnSchd);
					} else if (!YFCCommon.isVoid(bundleParentLineKey)) {

						if (!affectedKitParentList
								.containsKey(bundleParentLineKey)) {
							affectedKitParentList.put(bundleParentLineKey,
									sBundleQty);
						} else {
							sBundleQty = affectedKitParentList
									.get(bundleParentLineKey);
						}

						formChngOrdStatusXMLForKitItem(dQuantityDax,
								sToExpectedDate, sBundleQty,
								bundleParentLineKey, eleOrderLineGetOrderList,
								kitOrdLinesDataMap);
					} else {
						GCCommonUtil.addLinesToChangeOrderStatusDoc(
								docChgOrdStatsFrmSchdPDXToOthr,
								eleOrderLineGetOrderList, sQuantityDax,
								GCConstants.PO_DAX_UNSCHEDULE_STATUS,
								sExpectedShipmentDate);
					}
				}

				/*
				 * Prepare input XML for changeOrderStatus API to move the
				 * OrderLine to BackOrderDaxAllocated Status.
				 */

				if (isSetOrdrLine) {

					sPDXUnSchdNSchdOrdCall = moveOrdLnsToBckOrdAllocForSet(
							docGetOrderListOP, docChgOrdStatsFrmSchdPDXToOthr,
							sPDXUnSchdNSchdOrdCall, dQuantityDax,
							sOrderLineKey, sKitBundleQty);

				} else {

					sPDXUnSchdNSchdOrdCall = prepareXMLToMoveLinesToBckOrdDXAllc(
							docChgOrdStatsFrmSchdPDXToOthr,
							sPDXUnSchdNSchdOrdCall, dQuantityDax,
							eleOrderLineGetOrderList);
				}

			}

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(" resolveDAXAllocationHold() method, docChangeOrderStatusCreate "
						+ GCXMLUtil
								.getXMLString(docChgOrdStatsFrmSchdPDXToOthr));
			}

			/*
			 * Prepare changeOrderStatus Input Doc to move the Orderlines of Kit
			 * Item to PODAXUnschedule , RecievedPODAX or both.
			 */
			getChngOrdStatusFrmSchdPDXDocForKit(sToExpectedDate,
					affectedKitParentList, kitOrdLinesDataMap,
					docGetOrderListOP, docChgOrdStatsFrmSchdPDXToOthr,
					docChgOrdStatsFrmRecvPDXToPDXUnSchd);
			/*
			 * Call ChangeOrderStatus to move the Order to PODAXUnschedule or
			 * ReceivedPODAX or both. Call changeOrderSchedule API to change the
			 * dates of lines in PODAXUnschedule to reqested Ship date. And Call
			 * schedule order API.
			 */
			callAPIsToMoveOrdToOthrStatuses(env, sOrderHeaderKey,
					docChgOrdStatsFrmSchdPDXToOthr,
					docChgOrdStatsFrmRecvPDXToPDXUnSchd,
					docChangeOrderSchedule, eleFromShedules,
					sPDXUnSchdNSchdOrdCall, sToExpectedDate);

			// GCSTORE-5796 and 6224 - This method has been written to fix this Jira.
		    //  moveWrntyLineToSchdleStatus(env, sOrderHeaderKey);
		} catch (Exception ex) {
			LOGGER.error(
					" Inside catch of GCProcessDAXAllocation.resolveDAXAllocationHold()",
					ex);
			throw new GCException(ex);
		}
		LOGGER.debug("Exiting GCProcessDAXAllocation.resolveDAXAllocationHold() method.");

		return inDoc;
	}

	
	private void moveWrntyLineToSchdleStatus(YFSEnvironment env, Document docGetOrderListOp) {
	    LOGGER.debug("Begin GCProcessDAXAllocation.moveWrntyLineToSchdleStatus() method.");
	   LOGGER.debug("The output Document for getOrderList" + GCXMLUtil.getXMLString(docGetOrderListOp));
	    Element eleRoot = docGetOrderListOp.getDocumentElement();
	    //ChangeOrderInput 
	    Document docChngOrdInput = GCXMLUtil.createDocument(GCConstants.ORDER);
	    Element eleOrder = (Element) eleRoot.getElementsByTagName(GCConstants.ORDER).item(0);
	    NodeList nlOrderLine = eleOrder.getElementsByTagName(GCConstants.ORDER_LINE);
	    int nlOrderLineLength = nlOrderLine.getLength();
	    Map<String, Element> hmWarrantyLine = new HashMap<>();
	    Map<String, String> hmOrderLineWithPODAXAllocated = new HashMap<>();
	  //This method will iterate through all the bundle component line and if any of the component line is in a status where this 
	    //is not eligible for release that bundleParrentOrderLine key will be added to Map to restrict bundle warranty line to be scheduled.
	    
	    
	/* Map<String, String> hmOrderLineNotMoveInPODAXAllocated = findOrdLineNotMovingInPODAXAllocated(
			nlOrderLine, nlOrderLineLength);*/
	    for (int ordLine = 0; ordLine < nlOrderLineLength; ordLine++) {
	      Element eleOrderLine = (Element) nlOrderLine.item(ordLine);
	      String strOrderLine = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
	      String strMinStatus = eleOrderLine.getAttribute(GCConstants.MIN_LINE_STATUS);
	      String strMaxStatus = eleOrderLine.getAttribute(GCConstants.MAX_LINE_STATUS);
	      Element eleOrderLineExtn = (Element) eleOrderLine.getElementsByTagName(GCConstants.EXTN).item(0);
	      String sExtnIsWarrantyItem = eleOrderLineExtn.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
	      Element eleOrderLineHoldTypes = (Element) eleOrderLine.getElementsByTagName(GCConstants.ORDER_HOLD_TYPES).item(0);
	      if (!YFCCommon.isVoid(eleOrderLineHoldTypes)
	          && (YFCCommon.equalsIgnoreCase(sExtnIsWarrantyItem, GCConstants.YES))
	          && !YFCCommon.isVoid(eleOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY))
	          && "1100".equalsIgnoreCase(strMinStatus) && "1100".equalsIgnoreCase(strMaxStatus)) {
	        prepareWarrantyOLMap(hmWarrantyLine, eleOrderLine, strOrderLine, eleOrderLineHoldTypes);
	      } else {
	        preparePODAXAllocateOrderLineMap(hmOrderLineWithPODAXAllocated, eleOrderLine);
	      }
	    }
	    if (!hmOrderLineWithPODAXAllocated.isEmpty() && !hmWarrantyLine.isEmpty()) {
	      String strOrderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
	      Element eleChangeOrder = docChngOrdInput.getDocumentElement();
	      eleChangeOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, strOrderHeaderKey);
	      Element eleOrderLines = docChngOrdInput.createElement(GCConstants.ORDER_LINES);
	      eleChangeOrder.appendChild(eleOrderLines);
	      boolean changeOrderReq = false;
	      for (Map.Entry<String, Element> entry : hmWarrantyLine.entrySet()) {
	        String keyOL = entry.getKey();
	        Element eleWarrantyOL = entry.getValue();
	        String strDependentLineKey = eleWarrantyOL.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
	        if (hmOrderLineWithPODAXAllocated.containsKey(strDependentLineKey)) {
	          String strShipNodeFrmmap = hmOrderLineWithPODAXAllocated.get(strDependentLineKey);
	          Element eleOrderLine = docChngOrdInput.createElement(GCConstants.ORDER_LINE);
	          eleOrderLine.setAttribute(GCConstants.SHIP_NODE, strShipNodeFrmmap);
	          eleOrderLine.setAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE, GCConstants.YES);
	          eleOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, keyOL);
	          Element eleOrdHoldTypes = docChngOrdInput.createElement(GCConstants.ORDER_HOLD_TYPES);
	          Element eleOrdHoldType = docChngOrdInput.createElement(GCConstants.ORDER_HOLD_TYPE);
	          eleOrdHoldType.setAttribute(GCConstants.HOLD_TYPE, "DEPENDENT_LINE_HOLD");
	          eleOrdHoldType.setAttribute(GCConstants.STATUS, "1300");
	          eleOrdHoldTypes.appendChild(eleOrdHoldType);
	          eleOrderLine.appendChild(eleOrdHoldTypes);
	          eleOrderLines.appendChild(eleOrderLine);
	          changeOrderReq = true;
	        }
	      }
	     if (changeOrderReq) {
	        LOGGER.debug("The inDocument for ChangeOrder is to resolve DEPENDENT_LINE_HOLD hold"
	            + GCXMLUtil.getXMLString(docChngOrdInput));
	        GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, docChngOrdInput, null);
	     }
	    }
	    LOGGER.debug("End moveWrntyLineToSchdleStatus");
		}

/**
 * This method will iterate through all the bundle component line and if any of the component line is in a status where this 
is not eligible for release that bundleParrentOrderLine key will be added to Map to restrict bundle warranty line to be scheduled.
 * @param nlOrderLine
 * @param nlOrderLineLength
 * @return
 *//*
	private Map<String, String> findOrdLineNotMovingInPODAXAllocated(
			NodeList nlOrderLine, int nlOrderLineLength ) {
		 LOGGER.debug("Begin checkBundleWarrantyEligibleForSchedule" );
		Map<String, String> hmOrderLineNotMoveInPODAXAllocated = new HashMap<String, String>();
		  String  strNotEligibleToScheduleWarrnyLine = props.getProperty("ORD_STATUSES_NOT_ELIGIBLE_TO_SCHEDULE_WARRANTY_LINE");
		   final String[] strarrEliOrdStats = strNotEligibleToScheduleWarrnyLine.split(GCConstants.COLON);
		   List<String> strStatusCompArrayList = Arrays.asList(strarrEliOrdStats);
		    for (int ordLine = 0; ordLine < nlOrderLineLength; ordLine++) {
		    	  Element eleOrderLine = (Element) nlOrderLine.item(ordLine);
			      String strOrderLine = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
			      Element eleOrderLineExtn = (Element) eleOrderLine.getElementsByTagName(GCConstants.EXTN).item(0);
			      String sExtnIsWarrantyItem = eleOrderLineExtn.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
			      String sBundleOrdLineKey = eleOrderLine.getAttribute(GCConstants.BUNDLE_PARENT_ORDERLINEKEY);
				  String strKitCode = eleOrderLine.getAttribute(GCConstants.KIT_CODE);
			      if(!GCConstants.YES.equalsIgnoreCase(sExtnIsWarrantyItem) && !GCConstants.BUNDLE.equalsIgnoreCase(strKitCode)) {
			      NodeList nlOrdStatuses = eleOrderLine.getElementsByTagName(GCConstants.ORDER_STATUS);
			      int nlOrdStatusLen = nlOrdStatuses.getLength();
			      for(int ordStatus =0;ordStatus<nlOrdStatusLen;ordStatus++){
			    	  Element eleOrdStatus = (Element) nlOrdStatuses.item(ordStatus);
			    	  String sStatus = eleOrdStatus.getAttribute(GCConstants.STATUS);
			    	if(strStatusCompArrayList.contains(sStatus) && !YFCCommon.isVoid(sBundleOrdLineKey)
			    			&&  !strOrderLine.equalsIgnoreCase(sBundleOrdLineKey)&& !hmOrderLineNotMoveInPODAXAllocated.containsKey(sBundleOrdLineKey) ){
			    		  hmOrderLineNotMoveInPODAXAllocated.put(sBundleOrdLineKey, null);
			    	  } else if(strStatusCompArrayList.contains(sStatus) && YFCCommon.isVoid(sBundleOrdLineKey)){
			    		  hmOrderLineNotMoveInPODAXAllocated.put(strOrderLine, null);
			    	  }
			      }
			      }
		    }
		    LOGGER.debug("End checkBundleWarrantyEligibleForSchedule" );
		return hmOrderLineNotMoveInPODAXAllocated;
	}*/
	/**
	 * This method calls changeOrder APi to apply Dependent line hold on the warranty line in case warranty line is scheduled.
	 * @param env
	 * @param docChngOrdInput
	 */
	private void callChngOrdToApplyDependentHold(YFSEnvironment env , Document docUpdateOrdList){
		LOGGER.debug("Begin callChngOrdToApplyDependentHold" );
		Element eleChangeOrdInput = docUpdateOrdList.getDocumentElement();
		Element eleOrder = (Element) eleChangeOrdInput.getElementsByTagName(GCConstants.ORDER).item(0);
		String strOrderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
		Element eleOrderLines = (Element) eleChangeOrdInput.getElementsByTagName(GCConstants.ORDER_LINES).item(0);
		if(!YFCCommon.isVoid(eleOrderLines) && eleOrderLines.hasChildNodes()) {
		NodeList nlOrderLine = eleChangeOrdInput.getElementsByTagName(GCConstants.ORDER_LINE);
		//Create changeOrder API input 
		Document dodChangeOrderInput = GCXMLUtil.createDocument(GCConstants.ORDER);
		Element eleChangeOrder = dodChangeOrderInput.getDocumentElement();
		eleChangeOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, strOrderHeaderKey);
	     Element eleChngOrdOrderLines = dodChangeOrderInput.createElement(GCConstants.ORDER_LINES);
	      eleChangeOrder.appendChild(eleChngOrdOrderLines);
	//	if(!YFCCommon.isVoid(nlchangeOrdInputOrderLine)){
	    boolean  changeOrderReq = false;
        int nlLen = nlOrderLine.getLength();
        for (int ordLine = 0; ordLine < nlLen; ordLine++) {
          Element eleOrderLine = (Element) nlOrderLine.item(ordLine);
          String strOrdLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
          Element eleExtnOrdLine = (Element) eleOrderLine.getElementsByTagName(GCConstants.EXTN).item(0);
          String strExtnWarrantyItem = eleExtnOrdLine.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
          if(GCConstants.YES.equalsIgnoreCase(strExtnWarrantyItem)){
          Element eleChngOrdLine = dodChangeOrderInput.createElement(GCConstants.ORDER_LINE);
          eleChngOrdLine.setAttribute(GCConstants.ORDER_LINE_KEY, strOrdLineKey);
          Element eleOrdHoldTypes = dodChangeOrderInput.createElement(GCConstants.ORDER_HOLD_TYPES);
          Element eleOrdHoldType = dodChangeOrderInput.createElement(GCConstants.ORDER_HOLD_TYPE);
          eleOrdHoldType.setAttribute(GCConstants.HOLD_TYPE, "DEPENDENT_LINE_HOLD");
          eleOrdHoldType.setAttribute(GCConstants.STATUS, "1100");
          eleOrdHoldTypes.appendChild(eleOrdHoldType);
          eleChngOrdLine.appendChild(eleOrdHoldTypes);
          eleChngOrdOrderLines.appendChild(eleChngOrdLine);
          changeOrderReq = true;
          }
         }
        if(changeOrderReq){
        LOGGER.debug("The inDocument for ChangeOrder is to apply DEPENDENT_LINE_HOLD hold"
            + GCXMLUtil.getXMLString(dodChangeOrderInput));
        System.out.println("kdjf"+GCXMLUtil.getXMLString(dodChangeOrderInput));
        GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, dodChangeOrderInput, null);
        LOGGER.debug("End callChngOrdToApplyDependentHold" );
		}
		}
		}	
	/**
	   * This method prepare the map of only warranty lines which all are having DEPENDENT_ON_HOLD hold
	   * on it.
	   * 
	   * @param hmWarrantyLine
	   * @param eleOrderLine
	   * @param strOrderLine
	   * @param eleOrderLineHoldTypes
	   */
	  private void prepareWarrantyOLMap(Map<String, Element> hmWarrantyLine, Element eleOrderLine, String strOrderLine,
	      Element eleOrderLineHoldTypes) {
	    LOGGER.debug("Begining of prepareWarrantyOLMap method");
	    NodeList nlOrderLineHoldType = eleOrderLineHoldTypes.getElementsByTagName(GCConstants.ORDER_HOLD_TYPE);
	    int nlOrderLineHoldTypeLength = nlOrderLineHoldType.getLength();
	    for (int iHoldType = 0; iHoldType < nlOrderLineHoldTypeLength; iHoldType++) {
	      Element eleOrderLineHoldType = (Element) nlOrderLineHoldType.item(iHoldType);
	      String sHoldType = eleOrderLineHoldType.getAttribute(GCConstants.HOLD_TYPE);
	      String sStatus = eleOrderLineHoldType.getAttribute(GCConstants.STATUS);
	      if (YFCCommon.equalsIgnoreCase(sHoldType, "DEPENDENT_LINE_HOLD") && YFCCommon.equalsIgnoreCase(sStatus, "1100")) {
	        hmWarrantyLine.put(strOrderLine, eleOrderLine);
	      }
	    }
	    LOGGER.debug("Begining of prepareWarrantyOLMap method");
	  }
	  /**
	   * This method prepare the map of order lines which are in PODAXAllocated status.
	   * 
	   * @param hmOrderLineWithPODAXAllocated
	   * @param eleOrderLine
	   */
	  private void preparePODAXAllocateOrderLineMap(Map<String, String> hmOrderLineWithPODAXAllocated, Element eleOrderLine) {
	    LOGGER.debug("Begining of preparePODAXAllocateOrderLineMap method");
	   String strOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
	   String strBundleParentOLKey = eleOrderLine.getAttribute(GCConstants.BUNDLE_PARENT_ORDERLINEKEY);
	   String strKitCode = eleOrderLine.getAttribute(GCConstants.KIT_CODE);
	    if(YFCCommon.isVoid(strBundleParentOLKey) || (!GCConstants.BUNDLE.equalsIgnoreCase(strKitCode) &&
	    		!hmOrderLineWithPODAXAllocated.containsKey(strBundleParentOLKey))){
	    	String strEligibleOrdStats = ""; 
		    strEligibleOrdStats = props.getProperty("ORD_STATUSES_ELIGIBLE_TO_SCHEDULE_WARRANTY_LINE");
		     // StringBuffer sb = new StringBuffer();
		      final String[] strarrEliOrdStats = strEligibleOrdStats.split(GCConstants.COLON);
		      List<String> strStatusArrayList = Arrays.asList(strarrEliOrdStats);
		      boolean statusMatched = false;
		      NodeList nlOrdStatuses = eleOrderLine.getElementsByTagName(GCConstants.ORDER_STATUS);
		      int nlOrdStatusLen = nlOrdStatuses.getLength();
		      for(int ordStatus =0;ordStatus<nlOrdStatusLen;ordStatus++){
		    	  Element eleOrdStatus = (Element) nlOrdStatuses.item(ordStatus);
		    	  String sStatus = eleOrdStatus.getAttribute(GCConstants.STATUS);
		    	 if(strStatusArrayList.contains(sStatus)){
		    		 statusMatched = true;
		    		 break;
		    	  }
		    	}
		      if(statusMatched){
		     String strShipNode = eleOrderLine.getAttribute(GCConstants.SHIP_NODE);
		      if (YFCCommon.isVoid(strBundleParentOLKey)) {
		        hmOrderLineWithPODAXAllocated.put(strOrderLineKey, strShipNode);
		      } else {
		        hmOrderLineWithPODAXAllocated.put(strBundleParentOLKey, strShipNode);
		        
		      }
		    }
	    }
	   LOGGER.debug("PODAXAllocated MAP" + hmOrderLineWithPODAXAllocated);
	    LOGGER.debug("Ending of preparePODAXAllocateOrderLineMap method");
}
	/**
	 * This method is used to move the OrderLine to Back Order Allocated for Set
	 * Items
	 * 
	 * @param docGetOrderListOP
	 *            - docGetOrderListOP
	 * @param docChgOrdStatsFrmSchdPDXToOthr
	 *            - docChgOrdStatsFrmSchdPDXToOthr
	 * @param sPDXUnSchdNSchdOrdCall
	 *            - sPDXUnSchdNSchdOrdCall
	 * @param dQuantityDax
	 *            - dQuantityDax
	 * @param sOrderLineKey
	 *            - sOrderLineKey
	 * @param sKitBundleQty
	 *            - sKitBundleQty
	 * @return boolean
	 * @throws TransformerException
	 *             - TransformerException
	 */
	private String moveOrdLnsToBckOrdAllocForSet(Document docGetOrderListOP,
			Document docChgOrdStatsFrmSchdPDXToOthr,
			String sPDXUnSchdNSchdOrdCall, double dQuantityDax,
			String sOrderLineKey, String sKitBundleQty)
			throws TransformerException {
		NodeList childOrdLineNL = XPathAPI.selectNodeList(docGetOrderListOP,
				"OrderList/Order/OrderLines/OrderLine/BundleParentLine[@OrderLineKey='"
						+ sOrderLineKey + "']/..");
		int childNLLen = childOrdLineNL.getLength();

		Map<String, Integer> kitCompRatioMap = getBundleCompRatio(
				sKitBundleQty, childOrdLineNL, childNLLen);

		for (int i = 0; i < childNLLen; i++) {
			Element childele = (Element) childOrdLineNL.item(i);
			String childOrdLnKey = childele
					.getAttribute(GCConstants.ORDER_LINE_KEY);
			Element eleBackorderedDAX = (Element) XPathAPI
					.selectSingleNode(
							childele,
							GCConstants.XPATH_ORDERSTATUES_ORDERSTATUS_ATTR_STATUS_EQUALTO_1300);
			if (!YFCCommon.isVoid(eleBackorderedDAX)) {

				String sStatusQty = eleBackorderedDAX
						.getAttribute(GCConstants.STATUS_QTY);
				double dStatusQty = (!YFCCommon.isVoid(sStatusQty)) ? Double
						.valueOf(sStatusQty) : 0.00;
				double dQtyToBeAllocated = 0.00;
				int ratio = kitCompRatioMap.get(childOrdLnKey);
				double newDaxQty = dQuantityDax * ratio;
				if (newDaxQty >= dStatusQty) {
					dQtyToBeAllocated = dStatusQty;
					newDaxQty = newDaxQty - dQtyToBeAllocated;
				} else {
					dQtyToBeAllocated = newDaxQty;
					newDaxQty = 0.00;
				}

				if (!GCConstants.STRING_11.equals(sPDXUnSchdNSchdOrdCall)) {
					sPDXUnSchdNSchdOrdCall = GCConstants.STRING_01;
				}

				GCCommonUtil.addLinesToChangeOrderStatusDoc(
						docChgOrdStatsFrmSchdPDXToOthr, childele,
						String.valueOf(dQtyToBeAllocated),
						GCConstants.BACKORDERED_DAX_ALLOCATED_STATUS, null);
			}
		}
		return sPDXUnSchdNSchdOrdCall;
	}

	private Map<String, Integer> getBundleCompRatio(String sKitBundleQty,
			NodeList childOrdLineNL, int childNLLen) {
		Map<String, Integer> kitCompRatioMap = new HashMap<String, Integer>();

		for (int j = 0; j < childNLLen; j++) {

			Element childele = (Element) childOrdLineNL.item(j);
			String childOrdLnKey = childele
					.getAttribute(GCConstants.ORDER_LINE_KEY);
			String sChildOrdQty = childele
					.getAttribute(GCConstants.ORDERED_QTY);
			double dKitBundleQty = Double.parseDouble(sKitBundleQty);
			double dChildOrdQty = Double.parseDouble(sChildOrdQty);
			int ratio = (int) ((int) dChildOrdQty / dKitBundleQty);
			kitCompRatioMap.put(childOrdLnKey, ratio);

		}
		return kitCompRatioMap;
	}

	/**
	 * This method is used to call various APIs : changeOrderStatus,
	 * ChangeOrderSchedule and ScheduleOrder API.
	 * 
	 * @param env
	 *            - YFSEnvironment Transaction variable used at invoke APIs.
	 * @param sOrderHeaderKey
	 *            - Order Header Key.
	 * 
	 * @param docChgOrdStatsFrmSchdPDXToOthr
	 *            - Input XML for ChangeOrderStatus API used to move the
	 *            OrderLine from SchedulePODAX to RecievedPODAX, or
	 *            PODAXUnschedule or Both.
	 * 
	 * @param docChgOrdStatsFrmRecvPDXToPDXUnSchd
	 *            - Input XML for ChangeOrderStatus API used to move the
	 *            OrderLine from RecievedPODAX, to PODAXUnschedule.
	 * 
	 * @param docChangeOrderSchedule
	 *            - Input XML used for changeOrderSchedule API and is used to
	 *            change the date of the orderline so that it can get released
	 *            from schedule status.
	 * 
	 * @param eleFromShedules
	 *            - eleFromShedules
	 * @param sPDXUnSchdNCallSchdOrd
	 *            - sPDXUnSchdNCallSchdOrd
	 * @param sToExpectedDate
	 *            - sToExpectedDate
	 * @throws TransformerException
	 *             - TransformerException
	 * @throws GCException
	 *             - GCException
	 */
	private void callAPIsToMoveOrdToOthrStatuses(YFSEnvironment env,
			String sOrderHeaderKey, Document docChgOrdStatsFrmSchdPDXToOthr,
			Document docChgOrdStatsFrmRecvPDXToPDXUnSchd,
			Document docChangeOrderSchedule, Element eleFromShedules,
			String sPDXUnSchdNCallSchdOrd, String sToExpectedDate)
			throws TransformerException, GCException {

		LOGGER.debug(" Entering GCProcessDAXAllocation.callAPIsToMoveOrdToOthrStatuses() method");

		if (GCConstants.STRING_11.equals(sPDXUnSchdNCallSchdOrd)
				|| GCConstants.STRING_01.equals(sPDXUnSchdNCallSchdOrd)) {

			Node schdPDXToOtheNode = XPathAPI.selectSingleNode(
					docChgOrdStatsFrmSchdPDXToOthr,
					GCConstants.XPATH_ORDERSTATUSCHANGE_ORDERLINES_ORDERLINE);

			Node rcvPDXToPNode = XPathAPI.selectSingleNode(
					docChgOrdStatsFrmRecvPDXToPDXUnSchd,
					GCConstants.XPATH_ORDERSTATUSCHANGE_ORDERLINES_ORDERLINE);

			if (!YFCCommon.isVoid(rcvPDXToPNode)) {
				callChangeOrderStatus(env, docChgOrdStatsFrmRecvPDXToPDXUnSchd);
				prepareChngOrdSchdDoc(sToExpectedDate, docChangeOrderSchedule,
						eleFromShedules, docChgOrdStatsFrmRecvPDXToPDXUnSchd);
			}

			if (!YFCCommon.isVoid(schdPDXToOtheNode)) {
				callChangeOrderStatus(env, docChgOrdStatsFrmSchdPDXToOthr);
				prepareChngOrdSchdDoc(sToExpectedDate, docChangeOrderSchedule,
						eleFromShedules, docChgOrdStatsFrmSchdPDXToOthr);
			}

			Node changeOrdSchdNode = XPathAPI.selectSingleNode(
					docChangeOrderSchedule,
					"ChangeSchedule/FromSchedules/FromSchedule");
			if (!YFCCommon.isVoid(changeOrdSchdNode)) {
				GCCommonUtil.invokeAPI(env,
						GCConstants.CHANGE_ORDER_SCHEDULE_API,
						docChangeOrderSchedule);
			}

			Document docScheduleOrder = prepareScheduleOrderDoc(sOrderHeaderKey);

			// OMS-5457-start
			// get the current order status
			final Document docUpdOrdList = getOrderListBeforeSchedule(env,
					sOrderHeaderKey);

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("GCSTORE 5954: Start");
			}
			Map<String, String> ordLineKeyMapBfrSchedule = getOrderLineKeyMap(docUpdOrdList);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("GCSTORE 5954: End");
			}
			
			// OMS-5457-end
			env.setTxnObject("DAXAllocationProcessing", "Y");
			
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("GCSTORE 6224: Start");
			}  
			String moveWarrantyLineToSchedule = props.getProperty("ENABLE_6224_FIX");
			boolean authExpired = false;
			//If this flag is set to Y in service argument then only 6224 fix works.
			if(GCConstants.YES.equalsIgnoreCase(moveWarrantyLineToSchedule)){
			double dbTotalAuthorizedAmt = 0.0;
		   Element eleOrdListRoot = docUpdOrdList.getDocumentElement();
			NodeList ndPaymentMethod = eleOrdListRoot.getElementsByTagName(GCConstants.PAYMENT_METHOD);
		   int 	ndPaymentMethodLen = ndPaymentMethod.getLength();
		   //To get the total authorized amount for the order and match with order total.
		   //If this is not matching it means initial authorization got expired. If it expired then only invoke fix for 6224.
		   for(int paymentMethod = 0;paymentMethod< ndPaymentMethodLen; paymentMethod++){
			   Element elePaymentMethod = (Element) ndPaymentMethod.item(paymentMethod);
			   String strTotalAuthorized = elePaymentMethod.getAttribute("TotalAuthorized");
			   double dbTotalAuthorized = Double.parseDouble(strTotalAuthorized);
			   dbTotalAuthorizedAmt = dbTotalAuthorizedAmt + dbTotalAuthorized;
			 }
		    Element elePriceInfo = (Element) eleOrdListRoot.getElementsByTagName(GCConstants.PRICE_INFO).item(0);
			String strOrdTotal = elePriceInfo.getAttribute("TotalAmount");
			double dblOrdTotal = Double.parseDouble(strOrdTotal);
			if(dbTotalAuthorizedAmt < dblOrdTotal){
				authExpired = true;
			}
			if(authExpired) {
			// GCSTORE-6224 - This method has been written to fix this Jira.
		      moveWrntyLineToSchdleStatus(env, docUpdOrdList);
			}
			//END 6224
			} 
			GCCommonUtil.invokeAPI(env, GCConstants.API_SCHEDULE_ORDER,
					docScheduleOrder);
		//Start 6224 Call ChangeOrder to Apply Dependent line hold
			
			Document docUpdateOrdList = getOrderListBeforeSchedule(env,
					sOrderHeaderKey);
			
			if(authExpired){
				callChngOrdToApplyDependentHold(env , docUpdateOrdList);
				}
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("GCSTORE 6224: End");
				}
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("GCSTORE 5954: Start");
			}
			Map<String, String> ordLineKeyMapAftSchedule = getOrderLineKeyMap(docUpdateOrdList);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("GCSTORE 5954: End");
			}
			// OMS-5457-start
			// change the order line which were just scheduled to Scheduled PO
			// DAX Allocated
			updateScheduledOrderLineStatus(docUpdOrdList, env, sOrderHeaderKey,
					ordLineKeyMapAftSchedule, ordLineKeyMapBfrSchedule);

			// OMS-5457-end

		}

		LOGGER.debug(" Exiting GCProcessDAXAllocation.callAPIsToMoveOrdToOthrStatuses() method ");
	}

	/**
	 * <h3>Description:</h3> This method is used to change the status of the
	 * lines scheduled in the current transaction to Scheduled PO DAX Allocated
	 * 
	 * @author Infosys Limited
	 * @param docOrderList
	 *            The getOrderList output before the schedule transaction
	 * @param yfsEnv
	 *            The yfs environment reference
	 * @param strOrderHeaderKey
	 *            The order header key
	 */
	private void updateScheduledOrderLineStatus(final Document docOrderList,
			final YFSEnvironment yfsEnv, final String strOrderHeaderKey,
			Map<String, String> ordLineKeyMapAftSchedule,
			Map<String, String> ordLineKeyMapBfrSchedule) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":updateScheduledOrderLineStatus:entry");
		}

		// prepare the docure required to move the lines to Scheduled PO DAX
		// Allocated status
		final Document docChfOrdStatIn = prepChgOrdStatusInForSchPODAXAllocInDoc(
				docOrderList, strOrderHeaderKey, ordLineKeyMapAftSchedule,
				ordLineKeyMapBfrSchedule);

		// check if any lines eligible
		if (anyLineEligibleForSchedulePODAXAllocated(docChfOrdStatIn)) {

			// change the status of orderline to Scheuled PO DAX Allocated
			// status
			final Document docChgOrdStOut = GCCommonUtil.invokeService(yfsEnv,
					"GCChangeOrderStatus_SchPODXAlloc_svc", docChfOrdStatIn);

			// log the output
			if (LOGGER.isDebugEnabled()) {

				LOGGER.debug(CLASS_NAME
						+ ":updateScheduledOrderLineStatus:changeOrderstatus="
						+ GCXMLUtil.getXMLString(docChgOrdStOut));
			}

		} else {

			LOGGER.info(CLASS_NAME
					+ ":updateScheduledOrderLineStatus:changeOrderStatus api not called as no lines eligible for the same.");
		}

		// log the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":updateScheduledOrderLineStatus:exit");
		}
	}

	/**
	 * <h3>Description:</h3>This method is used to check if the any order lines
	 * are eligible for changeOrderStatus
	 * 
	 * @author Infosys Limited
	 * @param docChfOrdStatIn
	 *            The changeOrderStatus api input
	 * @return true - if any line eligible for changeOrderStatus else returns
	 *         false
	 */
	private boolean anyLineEligibleForSchedulePODAXAllocated(
			final Document docChfOrdStatIn) {

		// logging the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME
					+ ":anyLineEligibleForSchedulePODAXAllocated:entry");
		}

		// get the number of order lines eligible for the changeOrder status
		final List listEleOLs = GCXMLUtil.getElementListByXpath(
				docChfOrdStatIn, "/OrderStatusChange/OrderLines/OrderLine");
		final int iEligibleNumOfOLs = listEleOLs.size();

		// the return variable
		final boolean bIsEligibleForChgOrdStatus;

		if (iEligibleNumOfOLs > 0) {

			bIsEligibleForChgOrdStatus = true;
		} else {

			bIsEligibleForChgOrdStatus = false;
		}

		// logging the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME
					+ ":anyLineEligibleForSchedulePODAXAllocated:exit:Output="
					+ bIsEligibleForChgOrdStatus);
		}

		// return the flag
		return bIsEligibleForChgOrdStatus;
	}

	/**
	 * <h3.Description:</h3> This method is used to prepare the
	 * changeOrderStatus api input
	 * 
	 * @author Infosys Limited
	 * @param docOrderList
	 *            The getOrderList output before scheduling
	 * @param strOrderHeaderKey
	 *            The order header key
	 * @return The changeOrder status for changing the status of line to
	 *         Scheduled PO DAX Allocated
	 */
	private Document prepChgOrdStatusInForSchPODAXAllocInDoc(
			final Document docOrderList, final String strOrderHeaderKey,
			Map<String, String> ordLineKeyMapAftSchedule,
			Map<String, String> ordLineKeyMapBfrSchedule) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME
					+ ":prepChgOrdStatusInForSchPODAXAllocInDoc:entry");
		}

		// get the list of Order status elements
		final List listOrdStatus = getEligibleOrdStatusEleList(docOrderList);

		// get the changeoRderstatus header
		final Document docChgOrdStatus = GCCommonUtil
				.prepareChangeOrderStatusHdrDoc(strOrderHeaderKey,
						GCConstants.CHANGE_SCHEDULE_STATUS);

		// get the base drop status
		final String strBaseDropStatus = getBaseDropStatus();

		Element eleOrdStat;
		String strOrdLineKey;
		String strQty;

		boolean isSplitScenario = false;
		for (Map.Entry<String, String> keyMap : ordLineKeyMapAftSchedule
				.entrySet()) {
			String sOrderLineKey = keyMap.getKey();
			if (!ordLineKeyMapBfrSchedule.containsKey(sOrderLineKey)) {
				String sQty = ordLineKeyMapAftSchedule.get(sOrderLineKey);
				addLinesToChangeOrderStatusDoc(docChgOrdStatus, sOrderLineKey,
						sQty, strBaseDropStatus);
				isSplitScenario = true;
			}
		}
		// update the orderlines
		if (!isSplitScenario) {
		for (Object objOrdStat : listOrdStatus) {

			// get the order line details
			eleOrdStat = (Element) objOrdStat;
				strOrdLineKey = eleOrdStat
						.getAttribute(GCConstants.ORDER_LINE_KEY);
			strQty = eleOrdStat.getAttribute(GCConstants.STATUS_QTY);

			// append the orderline details
			addLinesToChangeOrderStatusDoc(docChgOrdStatus, strOrdLineKey,
					strQty, strBaseDropStatus);
		}
		}
		// log the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME
					+ ":prepChgOrdStatusInForSchPODAXAllocInDoc:exit:Output="
					+ GCXMLUtil.getXMLString(docChgOrdStatus));
		}

		// return the change order status
		return docChgOrdStatus;
	}

	/**
	 * <h3>Description:</h3> Get the base drop status
	 * 
	 * @author Infosys Limited
	 * @return The base drop status
	 */
	private String getBaseDropStatus() {

		// logging the method entry
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":getBaseDropStatus:entry");
		}

		// get the base drop status configured
		final String strBaseDropStatus = props.getProperty("BASE_DROP_STATUS");

		// logging the method exit
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASS_NAME + ":getBaseDropStatus:exit:output="
					+ strBaseDropStatus);
		}

		// retunr the base drop status
		return strBaseDropStatus;
	}

	/**
	 * <h3>Description:</h3> This method is used to add lines to the changeOrder
	 * status document
	 * 
	 * @author Infosys Limited
	 * @param docChangeOrderStatus
	 *            The changeOrder status document to be updated
	 * @param strOrderLineKey
	 *            The orderLine Key
	 * @param strQuantity
	 *            The quantity
	 * @param strBaseDropStatus
	 *            the base drop status
	 */
	private void addLinesToChangeOrderStatusDoc(
			final Document docChangeOrderStatus, final String strOrderLineKey,
			final String strQuantity, final String strBaseDropStatus) {

		// log method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME
					+ ":addLinesToChangeOrderStatusDoc:entry:Input:OrderLineKey="
					+ strOrderLineKey + ":Qty=" + strQuantity
					+ ":BaseDropStatus=" + strBaseDropStatus
					+ ":ChangeOrderStatusDoc="
					+ GCXMLUtil.getXMLString(docChangeOrderStatus));
		}

		final Element eleOrderLinesChngStatus = GCXMLUtil.getElementByXPath(
				docChangeOrderStatus, "OrderStatusChange/OrderLines");

		// check if the OrderLines element is present
		if (YFCCommon.isVoid(eleOrderLinesChngStatus)) {

			final Element eleOrderLines = GCXMLUtil.createElement(
					docChangeOrderStatus, GCConstants.ORDER_LINES, null);
			final Element eleRoot = docChangeOrderStatus.getDocumentElement();
			eleRoot.appendChild(eleOrderLines);

		}

		// append the orderLine details
		final Element eleOrderLineChngStatus = GCXMLUtil.createElement(
				docChangeOrderStatus, GCConstants.ORDER_LINE, null);
		eleOrderLineChngStatus.setAttribute(GCConstants.ORDER_LINE_KEY,
				strOrderLineKey);
		eleOrderLineChngStatus.setAttribute(GCConstants.BASE_DROP_STATUS,
				strBaseDropStatus);
		eleOrderLineChngStatus.setAttribute(GCConstants.QUANTITY, strQuantity);
		eleOrderLinesChngStatus.appendChild(eleOrderLineChngStatus);

		// log output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME
					+ ":addLinesToChangeOrderStatusDoc:exit:Updated Doc="
					+ GCXMLUtil.getXMLString(docChangeOrderStatus));
		}
	}

	/**
	 * <h3>Description:</h3> This method is used to get list of Order status
	 * elements which are eligible for changeOrderStatus
	 * 
	 * @param docOrderList
	 *            The orderlist output before scheduling the order
	 * @return The list of order status elements which are eligible for
	 *         changeOrderStatus
	 */
	private List getEligibleOrdStatusEleList(final Document docOrderList) {

		// log the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getEligibleOrdStatusEleList:entry");
		}

		// get the xpath to be used
		final String strOrdStXpth = getOrderStatusXpth();

		// apply xpath
		final List listEleOrdStat = GCXMLUtil.getElementListByXpath(
				docOrderList, strOrdStXpth);

		// log the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME
					+ ":getEligibleOrdStatusEleList:exit:Output="
					+ listEleOrdStat);
		}

		// return the list
		return listEleOrdStat;
	}

	/**
	 * <h3>Description:</h3>This method is used to prepare the xpath to get the
	 * orderstatus elements
	 * 
	 * @return The xpath to get the order status
	 */
	private String getOrderStatusXpth() {

		// logging the method entry
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getOrderStatusXpth:entry");
		}

		// the xpath to get the list of order status
		final StringBuilder stbXpthOrderStatus = new StringBuilder(
				"/OrderList/Order/OrderLines/OrderLine/OrderStatuses/OrderStatus[");
		String strEligibleOrdStats = null;
		// get the configured list of statuses eligible status
		if (YFCCommon.equalsIgnoreCase(props.getProperty("DAX_FLAG_5954"), "Y")) {
			strEligibleOrdStats = props
					.getProperty("ORDER_STATUS_DAX_ALLOCATION");
		} else {
			strEligibleOrdStats = props
				.getProperty("ELIGIBLE_ORDER_STATUSES_LIST");
		}
		final String[] strarrEliOrdStats = strEligibleOrdStats
				.split(GCConstants.COLON);

		// prepare the xpath
		boolean bIsNotFirstOrdStat = false;
		for (String strOrdStat : strarrEliOrdStats) {

			// check if the first element
			if (bIsNotFirstOrdStat) {
				stbXpthOrderStatus.append(" or ");
			} else {
				bIsNotFirstOrdStat = true;
			}

			stbXpthOrderStatus.append(" @Status='");
			stbXpthOrderStatus.append(strOrdStat);
			stbXpthOrderStatus.append("' ");
		}

		stbXpthOrderStatus.append("]");

		// the xpath to be used
		final String strOrdStXpth = stbXpthOrderStatus.toString();

		// logging the method exit
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":getOrderStatusXpth:exit:Output="
					+ strOrdStXpth);
		}

		// return the xpath to fetch the order status
		return strOrdStXpth;
	}

	/**
	 * <h3>Description:</h3>This method is used to get the order details before
	 * schedule
	 * 
	 * @author Infosys Limited
	 * @param yfsEnv
	 *            The yfs environment reference
	 * @param strOrderHeaderKey
	 *            The order header key
	 * @return The latest order details
	 */
	private Document getOrderListBeforeSchedule(final YFSEnvironment yfsEnv,
			final String strOrderHeaderKey) {

		// logging the input
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME
					+ ":getOrderListBeforeSchedule:entry:Input="
					+ strOrderHeaderKey);
		}

		// getOrderList api input
		final Document docGetOrdListIn = prepGetOrdListInDoc(strOrderHeaderKey);

		// invoke the getOrderList api
		final Document docGetOrdLstOut = GCCommonUtil.invokeService(yfsEnv,
				"GCGetOrderList_Before_Schedule_svc", docGetOrdListIn);

		// log the output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME
					+ ":getOrderListBeforeSchedule:exit:Output="
					+ GCXMLUtil.getXMLString(docGetOrdLstOut));
		}

		// return the getOrderList output
		return docGetOrdLstOut;
	}

	/**
	 * <h3>Description:</h3> This method is used to prepare the getOrderList api
	 * input
	 * 
	 * @author Infosys Limited
	 * @param strOrderHeaderKey
	 *            The order header key
	 * @return The getOrderList api input
	 */
	private Document prepGetOrdListInDoc(final String strOrderHeaderKey) {

		// log the input
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":prepGetOrdListInDoc:entry:Input="
					+ strOrderHeaderKey);
		}

		// create the document with Order as root element
		final Document docOrder = GCXMLUtil.createDocument(GCConstants.ORDER);

		// set the order header key
		final Element eleOrder = docOrder.getDocumentElement();
		eleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, strOrderHeaderKey);

		// log the output
		if (LOGGER.isDebugEnabled()) {

			LOGGER.debug(CLASS_NAME + ":prepGetOrdListInDoc:exit:Output="
					+ GCXMLUtil.getXMLString(docOrder));
		}

		// return the order document
		return docOrder;
	}

	/**
	 * This method is used to add the orderline to changeOrderstatus input to
	 * move the orderline to BackOrdered Dax Allocated status.
	 * 
	 * @param docChgOrdStatsFrmSchdPDXToOthr
	 *            - docChgOrdStatsFrmSchdPDXToOthr.
	 * @param sPDXUnSchdNCallSchdOrd
	 *            - sPDXUnSchdNCallSchdOrd.
	 * @param sQuantityDax
	 *            - sQuantityDax.
	 * @param dQuantityDax
	 *            - dQuantityDax.
	 * @param eleOrderLineGetOrderList
	 *            - eleOrderLineGetOrderList.
	 * @return String.
	 * @throws TransformerException
	 *             - TransformerException
	 */
	private String prepareXMLToMoveLinesToBckOrdDXAllc(
			Document docChgOrdStatsFrmSchdPDXToOthr,
			String sPDXUnSchdNCallSchdOrd, double dQuantityDax,
			Element eleOrderLineGetOrderList) throws TransformerException {

		LOGGER.debug("Entering GCProcessDAXAllocation.prepareXMLToMoveLinesToBckOrdDXAllc() method.");

		Element eleBackorderedDAX = (Element) XPathAPI
				.selectSingleNode(
						eleOrderLineGetOrderList,
						GCConstants.XPATH_ORDERSTATUES_ORDERSTATUS_ATTR_STATUS_EQUALTO_1300);

		if (!YFCCommon.isVoid(eleBackorderedDAX)) {
			String sStatusQty = eleBackorderedDAX
					.getAttribute(GCConstants.STATUS_QTY);
			double dStatusQty = (!YFCCommon.isVoid(sStatusQty)) ? Double
					.valueOf(sStatusQty) : 0.00;
			double dQtyToBeAllocated = 0.00;
			if (dQuantityDax >= dStatusQty) {
				dQtyToBeAllocated = dStatusQty;
			} else {
				dQtyToBeAllocated = dQuantityDax;
				dQuantityDax = 0.00;
			}

			if (!GCConstants.STRING_11.equals(sPDXUnSchdNCallSchdOrd)) {
				sPDXUnSchdNCallSchdOrd = GCConstants.STRING_01;
			}

			GCCommonUtil.addLinesToChangeOrderStatusDoc(
					docChgOrdStatsFrmSchdPDXToOthr, eleOrderLineGetOrderList,
					String.valueOf(dQtyToBeAllocated),
					GCConstants.BACKORDERED_DAX_ALLOCATED_STATUS, null);
		}

		LOGGER.debug("Exiting GCProcessDAXAllocation.prepareXMLToMoveLinesToBckOrdDXAllc() method.");

		return sPDXUnSchdNCallSchdOrd;
	}

	/**
	 * This method is used get OrderStatusElement.
	 * 
	 * @param eleOrderLineGetOrderList
	 *            - eleOrderLineGetOrderList
	 * @param isSetOrdrLine
	 *            - isSetOrdrLine
	 * @return OrderStatusElement.
	 * @throws TransformerException
	 *             - TransformerException
	 */
	private Element getRequiredOrdLnStatusElement(
			Element eleOrderLineGetOrderList, boolean isSetOrdrLine)
			throws TransformerException {

		LOGGER.debug("Entering GCProcessDAXAllocation.getRequiredOrdLnStatusElement() method.");

		Element ordLnOrdStatusELe = null;

		if (isSetOrdrLine) {

			String ordLineTotalQty = eleOrderLineGetOrderList
					.getAttribute(GCConstants.ORDERED_QTY);
			ordLnOrdStatusELe = (Element) XPathAPI.selectSingleNode(
					eleOrderLineGetOrderList,
					"OrderStatuses/OrderStatus[@Status='1100']");
		} else {
			ordLnOrdStatusELe = (Element) XPathAPI.selectSingleNode(
					eleOrderLineGetOrderList,
					"OrderStatuses/OrderStatus[@Status='1500.10']");
		}

		LOGGER.debug("Exiting GCProcessDAXAllocation.getRequiredOrdLnStatusElement() method.");

		return ordLnOrdStatusELe;
	}

	/**
	 * This method is used to get the Affected Parent line.
	 * 
	 * @param docGetOrderListOP
	 *            - docGetOrderListOP
	 * @param eleOrderLine
	 *            - eleOrderLine
	 * @return eleOrderLineGetOrderList
	 * @throws TransformerException
	 *             - TransformerException
	 */

	private Element getAffectedOrdLnEle(Document docGetOrderListOP,
			Element eleOrderLine) throws TransformerException {

		LOGGER.debug("Entering GCProcessDAXAllocation.getAffectedOrdLnEle() method.");

		String sPrimeLineNo = eleOrderLine
				.getAttribute(GCConstants.PRIME_LINE_NO);
		String sSubLineNo = eleOrderLine.getAttribute(GCConstants.SUB_LINE_NO);
		Element eleOrderLineGetOrderList = (Element) XPathAPI.selectSingleNode(
				docGetOrderListOP, "/OrderList/Order/OrderLines/"
						+ "OrderLine[@PrimeLineNo ='" + sPrimeLineNo
						+ "' and @SubLineNo='" + sSubLineNo + "']");

		LOGGER.debug("Exiting GCProcessDAXAllocation.getAffectedOrdLnEle() method.");

		return eleOrderLineGetOrderList;
	}

	/**
	 * This method is used to changeOrderSchedule XML.
	 * 
	 * @param toExpectedShipDate
	 *            - toExpectedShipDate
	 * @param docChangeOrderSchedule
	 *            - docChangeOrderSchedule
	 * @param eleFromShedules
	 *            - eleFromShedules
	 * @param docChgOrdStatsFrmSchdPDXToOthr
	 *            - docChgOrdStatsFrmSchdPDXToOthr
	 * @throws TransformerException
	 *             - TransformerException
	 */
	private void prepareChngOrdSchdDoc(String toExpectedShipDate,
			Document docChangeOrderSchedule, Element eleFromShedules,
			Document docChgOrdStatsFrmSchdPDXToOthr)
			throws TransformerException {

		LOGGER.debug("Entering GCProcessDAXAllocation.prepareChngOrdSchdDoc() method.");

		NodeList orderLineNL = XPathAPI.selectNodeList(
				docChgOrdStatsFrmSchdPDXToOthr,
				GCConstants.XPATH_ORDERSTATUSCHANGE_ORDERLINES_ORDERLINE);

		int orderLineNLSize = orderLineNL.getLength();

		for (int i = 0; i < orderLineNLSize; i++) {
			Element orderLineEle = (Element) orderLineNL.item(i);

			String sOrderLineKey = orderLineEle
					.getAttribute(GCConstants.ORDER_LINE_KEY);
			String fromExpectedShipDate = orderLineEle
					.getAttribute(GCConstants.FROM_EXPECTED_SHIPMENT_DATE);
			String sDaxQty = orderLineEle.getAttribute(GCConstants.QUANTITY);
			String fromStatus = orderLineEle
					.getAttribute(GCConstants.BASE_DROP_STATUS);
			if (!GCConstants.STATUS_RECIEVED_PODAX.equals(fromStatus)
					&& !GCConstants.BACKORDERED_DAX_ALLOCATED_STATUS
							.equals(fromStatus)) {
				addLinesToChangeScheduleEle(docChangeOrderSchedule,
						eleFromShedules, sOrderLineKey, fromExpectedShipDate,
						sDaxQty, toExpectedShipDate, fromStatus);
			}
		}

		LOGGER.debug("Exiting GCProcessDAXAllocation.prepareChngOrdSchdDoc() method.");
	}

	/**
	 * This method is used to get the changeOrderStatus XML.
	 * 
	 * @param sReqShipDate
	 *            - sReqShipDate
	 * @param affectedKitParentList
	 *            - affectedKitParentList
	 * @param kitOrdLinesDataMap
	 *            - kitOrdLinesDataMap
	 * @param docGetOrderListOP
	 *            - docGetOrderListOP
	 * @param docChgOrdStatsFrmSchdPDXToOthr
	 *            - docChgOrdStatsFrmSchdPDXToOthr
	 * @param docChgOrdStatsFrmRecvPDXToPDXUnSchd
	 *            - docChgOrdStatsFrmRecvPDXToPDXUnSchd
	 * @throws TransformerException
	 *             - TransformerException
	 */

	private void getChngOrdStatusFrmSchdPDXDocForKit(
			String sReqShipDate,
			Map<String, String> affectedKitParentList,
			Map<String, LinkedHashMap<String, GCOrderLineStructForKitNSetItem>> kitOrdLinesDataMap,
			Document docGetOrderListOP,
			Document docChgOrdStatsFrmSchdPDXToOthr,
			Document docChgOrdStatsFrmRecvPDXToPDXUnSchd)
			throws TransformerException {

		LOGGER.debug("Entering GCProcessDAXAllocation.getChngOrdStatusFrmSchdPDXDoc() method.");

		Iterator<Entry<String, String>> entryItr = affectedKitParentList
				.entrySet().iterator();

		while (entryItr.hasNext()) {

			Entry<String, String> entry = entryItr.next();
			String parentOrdLnKey = entry.getKey();

			Map<String, GCOrderLineStructForKitNSetItem> childOrdLnDataMap = kitOrdLinesDataMap
					.get(parentOrdLnKey);

			NodeList allChildOrdLnsForParentNL = XPathAPI.selectNodeList(
					docGetOrderListOP,
					"OrderList/Order/OrderLines/OrderLine/BundleParentLine[@OrderLineKey='"
							+ parentOrdLnKey + "']/..");
			int allOrdLnsLen = allChildOrdLnsForParentNL.getLength();

			String sBundleQty = affectedKitParentList.get(parentOrdLnKey);

			for (int i = 0; i < allOrdLnsLen; i++) {

				Element childOrdLnEle = (Element) allChildOrdLnsForParentNL
						.item(i);
				String childOrdLnKey = childOrdLnEle
						.getAttribute(GCConstants.ORDER_LINE_KEY);
				String maxLineStatus = childOrdLnEle
						.getAttribute(GCConstants.MAX_LINE_STATUS);
				maxLineStatus = GCShipmentUtil
						.checkAndConvertOrderStatus(maxLineStatus);
				Double dMaxLineStatus = Double.parseDouble(maxLineStatus);
				Double releasedStatus = Double
						.parseDouble(GCConstants.RELEASED_STATUS);
				if (!childOrdLnDataMap.containsKey(childOrdLnKey)
						&& !YFCUtils.equals("1500", maxLineStatus)
						&& !(dMaxLineStatus >= releasedStatus)) {
					childOrdLnDataMap.put(childOrdLnKey,
							new GCOrderLineStructForKitNSetItem(childOrdLnEle,
									sBundleQty, GCConstants.ZEROAMOUNT,
									sReqShipDate, false));
				}
			}

			int minRatio = -1;
			Iterator<Entry<String, GCOrderLineStructForKitNSetItem>> childOrdLnDataItr = childOrdLnDataMap
					.entrySet().iterator();

			while (childOrdLnDataItr.hasNext()) {

				Entry<String, GCOrderLineStructForKitNSetItem> childOrdLnDataEntry = childOrdLnDataItr
						.next();
				String childOrdLnKy = childOrdLnDataEntry.getKey();

				GCOrderLineStructForKitNSetItem childOrdLnData = childOrdLnDataMap
						.get(childOrdLnKy);
				int kitRatio = childOrdLnData.getKitRatio();

				if (minRatio < 0) {
					minRatio = kitRatio;
				} else if (minRatio > kitRatio) {
					minRatio = kitRatio;
				}
			}

			Iterator<Entry<String, GCOrderLineStructForKitNSetItem>> childOrdLnKyEntryItr = childOrdLnDataMap
					.entrySet().iterator();

			while (childOrdLnKyEntryItr.hasNext()) {

				Entry<String, GCOrderLineStructForKitNSetItem> childOrdLnKyEntry = childOrdLnKyEntryItr
						.next();
				String childOrdLnKy = childOrdLnKyEntry.getKey();

				GCOrderLineStructForKitNSetItem childOrdLnData = childOrdLnDataMap
						.get(childOrdLnKy);
				childOrdLnData.setMinRatio(minRatio);

				if (!childOrdLnData.isAllInfoAvailable()) {
					childOrdLnData.setAllValues();
				}

				if (childOrdLnData.getdQtyFromSchdPDXToPDXUnschd() > 0) {
					addLnsToChngOrdStatusDocFrKit(
							docChgOrdStatsFrmSchdPDXToOthr, childOrdLnData,
							GCConstants.PO_DAX_UNSCHEDULE_STATUS);
				}

				if (childOrdLnData.getdQtyFromSchdPDXToRecvPDX() > 0) {
					addLnsToChngOrdStatusDocFrKit(
							docChgOrdStatsFrmSchdPDXToOthr, childOrdLnData,
							GCConstants.STATUS_RECIEVED_PODAX);
				}

				if (childOrdLnData.getdQtyFromRecvPDXToPDXUnSchd() > 0) {
					addLnsToChngOrdStatusDocFrKit(
							docChgOrdStatsFrmRecvPDXToPDXUnSchd,
							childOrdLnData,
							GCConstants.STATUS_MOVE_FROM_RCVPDX_TO_PODAX_UNSCHD);
				}
			}
		}

		LOGGER.debug("Exiting GCProcessDAXAllocation.getChngOrdStatusFrmSchdPDXDoc() method.");
	}

	/**
	 * This method is used to get the ExpectedShipmentDate.
	 * 
	 * @param eleOrderLineGetOrderList
	 *            - eleOrderLineGetOrderList
	 * @param ordLnOrdStatusELe
	 *            - ordLnOrdStatusELe
	 * @return expectedShipDate.
	 * @throws TransformerException
	 *             - TransformerException
	 */
	private String getExpectShipDate(Element eleOrderLineGetOrderList,
			Element ordLnOrdStatusELe) throws TransformerException {

		LOGGER.debug("Entering GCProcessDAXAllocation.getExpectShipDate() method.");

		String ordLnScheduleKey = ordLnOrdStatusELe
				.getAttribute(GCConstants.ORDER_LINE_SCHEDULE_KEY);

		Element ordLnSchdlEle = (Element) XPathAPI.selectSingleNode(
				eleOrderLineGetOrderList, "Schedules/Schedule[@"
						+ GCConstants.ORDER_LINE_SCHEDULE_KEY + " ='"
						+ ordLnScheduleKey + "']");

		String sExpectedShipmentDate = ordLnSchdlEle
				.getAttribute(GCConstants.EXPECTED_SHIPMENT_DATE);

		LOGGER.debug("Exiting GCProcessDAXAllocation.getExpectShipDate() method.");

		return sExpectedShipmentDate;

	}

	/**
	 * This method is used to get the getOrderList output Document.
	 * 
	 * @param env
	 *            - YFSEnvironment transaction variable.
	 * @param inDoc
	 *            - input Doc for getOrderList Call.
	 * @return getOrderList Out Document.
	 */

	private Document getOrderListOutputDoc(YFSEnvironment env, Document inDoc) {

		LOGGER.debug("Entering GCProcessDAXAllocation.getOrderListOutputDoc() method.");

		Element eleRootOfInDoc = inDoc.getDocumentElement();
		String sOrderNo = eleRootOfInDoc.getAttribute(GCConstants.ORDER_NO);
		String sDocumentType = eleRootOfInDoc
				.getAttribute(GCConstants.DOCUMENT_TYPE);
		String sEnterpriseCode = eleRootOfInDoc
				.getAttribute(GCConstants.ENTERPRISE_CODE);

		Document docGetOrderListIP = GCXMLUtil
				.createDocument(GCConstants.ORDER);
		Element eleGetOrderListIP = docGetOrderListIP.getDocumentElement();
		eleGetOrderListIP.setAttribute(GCConstants.ORDER_NO, sOrderNo);
		eleGetOrderListIP
				.setAttribute(GCConstants.DOCUMENT_TYPE, sDocumentType);
		eleGetOrderListIP.setAttribute(GCConstants.ENTERPRISE_CODE,
				sEnterpriseCode);
		Document docGetOrderListTemplate = GCXMLUtil
				.getDocument(GCConstants.GET_ORDER_LIST_STATUSES_TEMPLATE);
		Document docGetOrderListOP = GCCommonUtil.invokeAPI(env,
				GCConstants.API_GET_ORDER_LIST, docGetOrderListIP,
				docGetOrderListTemplate);

		LOGGER.debug("Exiting GCProcessDAXAllocation.getOrderListOutputDoc() method.");

		return docGetOrderListOP;
	}

	/**
	 * This method is used to form changeOrderStatus Document for Kit Item.
	 * 
	 * @param dQuantityDax
	 *            - dQuantityDax
	 * @param sReqShipDate
	 *            - sReqShipDate
	 * @param sBundleQty
	 *            - sBundleQty
	 * @param bundleParentLineKey
	 *            - bundleParentLineKey
	 * @param eleOrderLine
	 *            - eleOrderLine
	 * @param kitOrdLinesDataMap
	 *            - kitOrdLinesDataMap
	 * @throws TransformerException
	 *             - TransformerException
	 */

	private void formChngOrdStatusXMLForKitItem(
			double dQuantityDax,
			String sReqShipDate,
			String sBundleQty,
			String bundleParentLineKey,
			Element eleOrderLine,
			Map<String, LinkedHashMap<String, GCOrderLineStructForKitNSetItem>> kitOrdLinesDataMap)
			throws TransformerException {
		LOGGER.debug("Entering GCProcessDAXAllocation.formChngOrdStatusXMLForKitItem() method.");

		GCOrderLineStructForKitNSetItem ordLnDataForKitItem = new GCOrderLineStructForKitNSetItem(
				eleOrderLine, sBundleQty, dQuantityDax, sReqShipDate, false);
		String childOrdLnKey = ordLnDataForKitItem.getsOrdLineKey();

		if (!kitOrdLinesDataMap.containsKey(bundleParentLineKey)) {

			LinkedHashMap<String, GCOrderLineStructForKitNSetItem> childDataMap = new LinkedHashMap<String, GCOrderLineStructForKitNSetItem>();
			childDataMap.put(childOrdLnKey, ordLnDataForKitItem);

			kitOrdLinesDataMap.put(bundleParentLineKey, childDataMap);
		} else {
			kitOrdLinesDataMap.get(bundleParentLineKey).put(childOrdLnKey,
					ordLnDataForKitItem);
		}
		LOGGER.debug("Exiting GCProcessDAXAllocation.formChngOrdStatusXMLForKitItem() method.");
	}

	/**
	 * This method is used to prepare the changeOrderStatus XML for Set Items.
	 * 
	 * @param sOrderLineKey
	 *            - sOrderLineKey
	 * @param sKitBundleQty
	 *            - sKitBundleQty
	 * @param dDaxQty
	 *            - dDaxQty
	 * @param toExpectedShipDate
	 *            - toExpectedShipDate
	 * @param docGetOrderListOP
	 *            - docGetOrderListOP
	 * @param docChgOrdStatusFrmSchdPDXToOthr
	 *            - docChgOrdStatusFrmSchdPDXToOthr
	 * @param docChgOrdStatsFrmRecvPDXToPDXUnSchd
	 *            - docChgOrdStatsFrmRecvPDXToPDXUnSchd
	 * @throws TransformerException
	 *             - TransformerException
	 */
	private void formChngOrdStatusXMLForSetItem(String sOrderLineKey,
			String sKitBundleQty, double dDaxQty, String toExpectedShipDate,
			Document docGetOrderListOP,
			Document docChgOrdStatusFrmSchdPDXToOthr,
			Document docChgOrdStatsFrmRecvPDXToPDXUnSchd)
			throws TransformerException {

		LOGGER.debug("Entering GCProcessDAXAllocation.formChngOrdStatusXMLForSetItem() method.");

		NodeList childOrdLinesForSetItem = XPathAPI.selectNodeList(
				docGetOrderListOP,
				"OrderList/Order/OrderLines/OrderLine/BundleParentLine[@OrderLineKey='"
						+ sOrderLineKey + "']/..");

		int childOrdLnsLen = childOrdLinesForSetItem.getLength();
		List<GCOrderLineStructForKitNSetItem> childOrdLnDataList = new ArrayList<GCOrderLineStructForKitNSetItem>();

		for (int i = 0; i < childOrdLnsLen; i++) {
			Element childOrdLnEle = (Element) childOrdLinesForSetItem.item(i);
			childOrdLnDataList.add(new GCOrderLineStructForKitNSetItem(
					childOrdLnEle, sKitBundleQty, dDaxQty, toExpectedShipDate,
					true));
		}

		int minRatio = -1;

		Iterator<GCOrderLineStructForKitNSetItem> itrToCalculateMinRatio = childOrdLnDataList
				.iterator();

		while (itrToCalculateMinRatio.hasNext()) {
			GCOrderLineStructForKitNSetItem childOrdLnData = itrToCalculateMinRatio
					.next();
			int kitRatio = childOrdLnData.getKitRatio();

			if (minRatio < 0) {
				minRatio = kitRatio;
			} else if (minRatio > kitRatio) {
				minRatio = kitRatio;
			}
		}

		Iterator<GCOrderLineStructForKitNSetItem> childOrdLnDataItr = childOrdLnDataList
				.iterator();

		while (childOrdLnDataItr.hasNext()) {
			GCOrderLineStructForKitNSetItem childOrdLnData = childOrdLnDataItr
					.next();
			childOrdLnData.setMinRatio(minRatio);
			if (!childOrdLnData.isAllInfoAvailable()) {
				childOrdLnData.setAllValues();
			}
			if (childOrdLnData.getdQtyFromSchdPDXToPDXUnschd() > 0) {
				addLnsToChngOrdStatusFrSet(docChgOrdStatusFrmSchdPDXToOthr,
						childOrdLnData, GCConstants.PO_DAX_UNSCHEDULE_STATUS);
			}

			if (childOrdLnData.getdQtyFromSchdPDXToRecvPDX() > 0) {
				addLnsToChngOrdStatusFrSet(docChgOrdStatusFrmSchdPDXToOthr,
						childOrdLnData, GCConstants.STATUS_RECIEVED_PODAX);
			}

			if (childOrdLnData.getdQtyFromRecvPDXToPDXUnSchd() > 0) {
				addLnsToChngOrdStatusFrSet(docChgOrdStatsFrmRecvPDXToPDXUnSchd,
						childOrdLnData,
						GCConstants.STATUS_MOVE_FROM_RCVPDX_TO_PODAX_UNSCHD);
			}
		}

		LOGGER.debug("Exiting GCProcessDAXAllocation.formChngOrdStatusXMLForSetItem() method.");
	}

	/**
	 * This method is used to add orderline in the input of changeSchedule API
	 * 
	 * @param docChangeSchedule
	 *            - docChangeSchedule
	 * @param eleFromShedules
	 *            - eleFromShedules
	 * @param sOrderLineKey
	 *            - sOrderLineKey
	 * @param fromExpectedShipDate
	 *            - fromExpectedShipDate
	 * @param sQuantityDax
	 *            - sQuantityDax
	 * @param toExpectedShipDate
	 *            - toExpectedShipDate
	 * @param fromStatus
	 *            - fromStatus
	 */
	private void addLinesToChangeScheduleEle(Document docChangeSchedule,
			Element eleFromShedules, String sOrderLineKey,
			String fromExpectedShipDate, String sQuantityDax,
			String toExpectedShipDate, String fromStatus) {

		Element eleFromSchedule = GCXMLUtil.createElement(docChangeSchedule,
				GCConstants.FROM_SCHEDULE, null);
		eleFromSchedule.setAttribute(GCConstants.ORDER_LINE_KEY, sOrderLineKey);
		eleFromSchedule.setAttribute(GCConstants.FROM_EXPECTED_SHIPMENT_DATE,
				fromExpectedShipDate);
		eleFromSchedule.setAttribute(GCConstants.FROM_STATUS, fromStatus);

		Element eleToSchedules = GCXMLUtil.createElement(docChangeSchedule,
				GCConstants.TO_SCHEDULES, null);
		Element eleToSchedule = GCXMLUtil.createElement(docChangeSchedule,
				GCConstants.TO_SCHEDULE, null);
		eleToSchedule.setAttribute(GCConstants.QUANTITY, sQuantityDax);
		eleToSchedule.setAttribute(GCConstants.TO_EXPECTED_SHIP_DATE,
				toExpectedShipDate);

		eleToSchedules.appendChild(eleToSchedule);
		eleFromSchedule.appendChild(eleToSchedules);
		eleFromShedules.appendChild(eleFromSchedule);
	}

	/**
	 * Desc - Removing nested try catch.
	 * 
	 * @param env
	 * @param docChgOrdStatusSchAndBack
	 */
	private void callChangeOrderStatus(YFSEnvironment env,
			Document docChgOrdStatusSchAndBack) {
		try {
			GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER_STATUS,
					docChgOrdStatusSchAndBack);
		} catch (Exception e) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(" API changeOrderStatus failed, docChangeOrderStatusCreate "
						+ GCXMLUtil.getXMLString(docChgOrdStatusSchAndBack));
			}
			LOGGER.error(" Error while changeORderStatus ", e);
		}
	}

	/**
	 * This method is used to prepare the schedule Order input Document.
	 * 
	 * @param sOrderHeaderKey
	 *            - Order Header Key
	 * @return input doc for scheduleOrder API
	 * @throws GCException
	 *             - GC Exception
	 */
	private Document prepareScheduleOrderDoc(String sOrderHeaderKey)
			throws GCException {
		LOGGER.debug(" Entering GCProcessDAXAllocation.prepareScheduleOrderDoc() method ");
		Document docScheduleOrder = null;
		try {
			docScheduleOrder = GCXMLUtil
					.createDocument(GCConstants.SCHEDULE_ORDER);
			docScheduleOrder.getDocumentElement().setAttribute(
					GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
		} catch (Exception e) {
			LOGGER.error(
					" Inside catch of GCProcessDAXAllocation.prepareScheduleOrderDoc()",
					e);
			throw new GCException(e);
		}
		LOGGER.debug(" Exiting GCProcessDAXAllocation.prepareScheduleOrderDoc() method ");
		return docScheduleOrder;
	}

	/**
	 * 
	 * Description of prepareUnscheduleOrderDoc Prepare Unschedule Order Doc
	 * 
	 * @param sOrderHeaderKey
	 * @return
	 * @throws GCException
	 * 
	 */
	private Document prepareChangeScheduleDoc(String sOrderHeaderKey) {
		Document docChangeSchedule = GCXMLUtil.createDocument("ChangeSchedule");
		docChangeSchedule.getDocumentElement().setAttribute(
				GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
		docChangeSchedule.getDocumentElement().setAttribute(
				GCConstants.OVERRIDE, GCConstants.FLAG_Y);

		return docChangeSchedule;
	}

	/**
	 * This method is used to set the properties.
	 */
	@Override
	public void setProperties(final Properties props) {

		this.props = props;
	}

	/**
	 * This method is used to add the lines to changeOrderStatus input document
	 * for set items.
	 * 
	 * @param docChangeOrderStatus
	 *            - docChangeOrderStatus
	 * @param childOrdLnData
	 *            - childOrdLnData
	 * @param sBaseDropStatus
	 *            - sBaseDropStatus
	 * @throws TransformerException
	 *             - TransformerException
	 */
	public void addLnsToChngOrdStatusFrSet(Document docChangeOrderStatus,
			GCOrderLineStructForKitNSetItem childOrdLnData,
			String sBaseDropStatus) throws TransformerException {
		LOGGER.verbose("Entering GCCommonUtil.addLinesToChangeOrderStatusDoc");
		Element eleOrderLinesChngStatus = GCXMLUtil.getElementByXPath(
				docChangeOrderStatus, "OrderStatusChange/OrderLines");
		Element eleOrderLineChngStatus = GCXMLUtil.createElement(
				docChangeOrderStatus, GCConstants.ORDER_LINE, null);
		eleOrderLineChngStatus.setAttribute(GCConstants.ORDER_LINE_KEY,
				childOrdLnData.getsOrdLineKey());

		if (GCConstants.PO_DAX_UNSCHEDULE_STATUS.equals(sBaseDropStatus)) {
			String fromExpectedDate = childOrdLnData
					.getFromExpectedShpDateMap().get(
							GCConstants.STATUS_SCHEDULE_PODAX);
			eleOrderLineChngStatus.setAttribute(
					GCConstants.FROM_EXPECTED_SHIPMENT_DATE, fromExpectedDate);
			eleOrderLineChngStatus.setAttribute(GCConstants.BASE_DROP_STATUS,
					sBaseDropStatus);
			eleOrderLineChngStatus.setAttribute(GCConstants.QUANTITY, String
					.valueOf(childOrdLnData.getdQtyFromSchdPDXToPDXUnschd()));
		} else if (GCConstants.STATUS_RECIEVED_PODAX.equals(sBaseDropStatus)) {
			String fromExpectedDate = childOrdLnData
					.getFromExpectedShpDateMap().get(
							GCConstants.STATUS_SCHEDULE_PODAX);
			eleOrderLineChngStatus.setAttribute(
					GCConstants.FROM_EXPECTED_SHIPMENT_DATE, fromExpectedDate);
			eleOrderLineChngStatus.setAttribute(GCConstants.BASE_DROP_STATUS,
					sBaseDropStatus);
			eleOrderLineChngStatus.setAttribute(GCConstants.QUANTITY, String
					.valueOf(childOrdLnData.getdQtyFromSchdPDXToRecvPDX()));
		} else if (GCConstants.STATUS_MOVE_FROM_RCVPDX_TO_PODAX_UNSCHD
				.equals(sBaseDropStatus)) {
			String fromExpectedDate = childOrdLnData
					.getFromExpectedShpDateMap().get(
							GCConstants.STATUS_RECIEVED_PODAX);
			eleOrderLineChngStatus.setAttribute(
					GCConstants.FROM_EXPECTED_SHIPMENT_DATE, fromExpectedDate);
			eleOrderLineChngStatus.setAttribute(GCConstants.BASE_DROP_STATUS,
					GCConstants.PO_DAX_UNSCHEDULE_STATUS);
			eleOrderLineChngStatus.setAttribute(GCConstants.QUANTITY, String
					.valueOf(childOrdLnData.getdQtyFromRecvPDXToPDXUnSchd()));
		}

		eleOrderLinesChngStatus.appendChild(eleOrderLineChngStatus);
		LOGGER.verbose("Exiting GCCommonUtil.addLinesToChangeOrderStatusDoc");
	}

	/**
	 * 
	 * @param docChangeOrderStatus
	 * @param childOrdLnData
	 * @param sBaseDropStatus
	 * @throws TransformerException
	 */

	public void addLnsToChngOrdStatusDocFrKit(Document docChangeOrderStatus,
			GCOrderLineStructForKitNSetItem childOrdLnData,
			String sBaseDropStatus) throws TransformerException {
		LOGGER.verbose("Entering GCCommonUtil.addLnsToChngOrdStatusDocFrKit");
		Element eleOrderLinesChngStatus = GCXMLUtil.getElementByXPath(
				docChangeOrderStatus,
				GCConstants.ORDER_STATUS_CHANGE_ORDER_LINES);
		Element eleOrderLineChngStatus = GCXMLUtil.createElement(
				docChangeOrderStatus, GCConstants.ORDER_LINE, null);
		eleOrderLineChngStatus.setAttribute(GCConstants.ORDER_LINE_KEY,
				childOrdLnData.getsOrdLineKey());

		if (GCConstants.PO_DAX_UNSCHEDULE_STATUS.equals(sBaseDropStatus)) {
			String fromExpectedDate = childOrdLnData
					.getFromExpectedShpDateMap().get(
							GCConstants.STATUS_SCHEDULE_PODAX);
			eleOrderLineChngStatus.setAttribute(
					GCConstants.FROM_EXPECTED_SHIPMENT_DATE, fromExpectedDate);
			eleOrderLineChngStatus.setAttribute(GCConstants.BASE_DROP_STATUS,
					sBaseDropStatus);
			eleOrderLineChngStatus.setAttribute(GCConstants.QUANTITY, String
					.valueOf(childOrdLnData.getdQtyFromSchdPDXToPDXUnschd()));
		} else if (GCConstants.STATUS_RECIEVED_PODAX.equals(sBaseDropStatus)) {
			String fromExpectedDate = childOrdLnData
					.getFromExpectedShpDateMap().get(
							GCConstants.STATUS_SCHEDULE_PODAX);
			eleOrderLineChngStatus.setAttribute(
					GCConstants.FROM_EXPECTED_SHIPMENT_DATE, fromExpectedDate);
			eleOrderLineChngStatus.setAttribute(GCConstants.BASE_DROP_STATUS,
					sBaseDropStatus);
			eleOrderLineChngStatus.setAttribute(GCConstants.QUANTITY, String
					.valueOf(childOrdLnData.getdQtyFromSchdPDXToRecvPDX()));
		} else if (GCConstants.STATUS_MOVE_FROM_RCVPDX_TO_PODAX_UNSCHD
				.equals(sBaseDropStatus)) {
			String fromExpectedDate = childOrdLnData
					.getFromExpectedShpDateMap().get(
							GCConstants.STATUS_RECIEVED_PODAX);
			eleOrderLineChngStatus.setAttribute(
					GCConstants.FROM_EXPECTED_SHIPMENT_DATE, fromExpectedDate);
			eleOrderLineChngStatus.setAttribute(GCConstants.BASE_DROP_STATUS,
					GCConstants.PO_DAX_UNSCHEDULE_STATUS);
			eleOrderLineChngStatus.setAttribute(GCConstants.QUANTITY, String
					.valueOf(childOrdLnData.getdQtyFromRecvPDXToPDXUnSchd()));
		}

		eleOrderLinesChngStatus.appendChild(eleOrderLineChngStatus);
		LOGGER.verbose("Exiting GCCommonUtil.addLnsToChngOrdStatusDocFrKit");
	}

	private void prepareInitialSchQtyMap(Document docUpdOrdList) {
		LOGGER.beginTimer("GCPickDetailActionsManager.prepareInitialSchQtyMap");
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Input getOrderList doc to prepare scheduled qty map :"
					+ docUpdOrdList.toString());
		}
		String orderLineKey = null;
		YFCDocument docGetOdtLstOutput = YFCDocument
				.getDocumentFor(docUpdOrdList);
		YFCElement docGetOrderRootEle = docGetOdtLstOutput.getDocumentElement();
		YFCElement orderEle = docGetOrderRootEle
				.getChildElement(GCConstants.ORDER);
		YFCNodeList<YFCElement> nlOrderLineEle = orderEle
				.getElementsByTagName(GCConstants.ORDER_LINE);
		for (YFCElement eleOrderLine : nlOrderLineEle) {
			int statusQty = 0;
			orderLineKey = eleOrderLine
					.getAttribute(GCConstants.ORDER_LINE_KEY);
			YFCElement orderStatusesEle = eleOrderLine
					.getChildElement(GCConstants.ORDER_STATUSES);
			YFCNodeList<YFCElement> nlStatusEle = orderStatusesEle
					.getElementsByTagName(GCConstants.ORDER_STATUS);
			for (YFCElement eleStatusLine : nlStatusEle) {
				if (YFCCommon.equalsIgnoreCase(
						eleStatusLine.getAttribute(GCConstants.STATUS),
						GCConstants.STATUS_SCHEDULED)) {
					statusQty = statusQty
							+ Integer.parseInt(eleStatusLine
									.getAttribute(GCConstants.STATUS_QTY));
				}
			}
			olKeySchQtyMap.put(orderLineKey, statusQty);
		}
		LOGGER.endTimer("GCPickDetailActionsManager.prepareInitialSchQtyMap");
	}
	private void prepareUpdatedSchQtyMap(Document newDocUpdOrdList) {
		LOGGER.beginTimer("GCPickDetailActionsManager.prepareUpdatedSchQtyMap");
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Input getOrderList doc to prepare updated scheduled qty map :"
					+ newDocUpdOrdList.toString());
		}
		NodeList nlOrderLine = newDocUpdOrdList
				.getElementsByTagName(GCConstants.ORDER_LINE);
		int count = nlOrderLine.getLength();
		for (int i = 0; i < count; i++) {
			Element eleOrderLine = (Element) nlOrderLine.item(i);
			String ordLineKey = eleOrderLine
					.getAttribute(GCConstants.ORDER_LINE_KEY);
			int changeInStatusQty = 0;
			int newScheduledQty = 0;
			Element orderStatusesEle = (Element) eleOrderLine
					.getElementsByTagName(GCConstants.ORDER_STATUSES).item(0);
			NodeList nlStatusEle = orderStatusesEle
					.getElementsByTagName(GCConstants.ORDER_STATUS);
			int len = nlStatusEle.getLength();
			for (int j = 0; j < len; j++) {
				Element eleStatusLine = (Element) nlStatusEle.item(j);
				if (YFCCommon.equalsIgnoreCase(
						eleStatusLine.getAttribute(GCConstants.STATUS),
						GCConstants.STATUS_SCHEDULED)) {
					newScheduledQty = newScheduledQty
							+ Integer.parseInt(eleStatusLine
									.getAttribute(GCConstants.STATUS_QTY));
				}
			}
			if (olKeySchQtyMap.containsKey(ordLineKey)) {
				changeInStatusQty = newScheduledQty
						- olKeySchQtyMap.get(ordLineKey);
			} else {
				changeInStatusQty = newScheduledQty;
			}
			if (changeInStatusQty > 0) {
				olKeyNewSchQtyMap.put(ordLineKey, changeInStatusQty);
			}
		}
		LOGGER.endTimer("GCPickDetailActionsManager.prepareUpdatedSchQtyMap");
	}
	private Map<String, String> getOrderLineKeyMap(Document docUpdOrdList)
			throws TransformerException {
		Map<String, String> ordLineKeyMap = new HashMap<String, String>();
		NodeList nlOrderLine = XPathAPI.selectNodeList(docUpdOrdList,
				"OrderList/Order/OrderLines/OrderLine");
		for (int i = 0; i < nlOrderLine.getLength(); i++) {
			Element eleOrderLine = (Element) nlOrderLine.item(i);
			String sOrderLineKey = eleOrderLine
					.getAttribute(GCConstants.ORDER_LINE_KEY);
			String sOrdQty = eleOrderLine.getAttribute(GCConstants.ORDERED_QTY);
			ordLineKeyMap.put(sOrderLineKey, sOrdQty);
		}
		return ordLineKeyMap;
	}
	/**
	 * This method is used to update the input Doc with the actual quantity on
	 * the order.
	 * 
	 * @param docGetOrderListOP
	 * @param inDoc
	 * @throws TransformerException
	 */
	private void updateQty(Document docGetOrderListOP, Document inDoc)
			throws TransformerException {
		LOGGER.beginTimer("GCProcessDAXAllocation.updateQty");
		Map<String, String> primeLineQtyMap = new HashMap<String, String>();
		NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc,
				GCConstants.XPATH_ORDER_ORDERLINES_ORDERLINE);
		NodeList nlGetOrderLine = docGetOrderListOP
				.getElementsByTagName(GCConstants.ORDER_LINE);
		for (int i = 0; i < nlGetOrderLine.getLength(); i++) {
			Element eleOrderLine = (Element) nlGetOrderLine.item(i);
			String sPrimeLineNo = eleOrderLine
					.getAttribute(GCConstants.PRIME_LINE_NO);
			String sQty = eleOrderLine.getAttribute(GCConstants.ORDERED_QTY);
			primeLineQtyMap.put(sPrimeLineNo, sQty);
		}
		for (int j = 0; j < nlOrderLine.getLength(); j++) {
			Element eleOrderLine = (Element) nlOrderLine.item(j);
			String sInputPrimeLineNo = eleOrderLine
					.getAttribute(GCConstants.PRIME_LINE_NO);
			String sInputQty = eleOrderLine.getAttribute(GCConstants.QUANTITY);
			if (primeLineQtyMap.containsKey(sInputPrimeLineNo)
					&& (Double.parseDouble(sInputQty) > Double
							.parseDouble(primeLineQtyMap.get(sInputPrimeLineNo)))) {
				eleOrderLine.setAttribute(GCConstants.QUANTITY,
						primeLineQtyMap.get(sInputPrimeLineNo));
			}
		}
		LOGGER.endTimer("GCProcessDAXAllocation.updateQty");
	}
}
