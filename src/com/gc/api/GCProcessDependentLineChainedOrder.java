package com.gc.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This Class is used to remove the dependent line hold from warranty item if its parent is in
 * chained order created status then removing the hold from warranty line and scheduling it after
 * stamping the same shipNode as that of its parent.
 *
 * @author shanil.sharma
 *
 */
public class GCProcessDependentLineChainedOrder implements YIFCustomApi {

  @Override
  public void setProperties(Properties arg0) {

  }

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCProcessDependentLineChainedOrder.class);

  public Document processDependentLine(YFSEnvironment env, Document inDoc) {

    LOGGER.debug(" Entering GCProcessDependentLineChainedOrder.processDependentLine() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" processDependentLine() method, Input document is" + inDoc.toString());
    }

    boolean isChangeOrderReqd = false;
    List<String> sOrderLineKeyForSchedule = new ArrayList<String>();
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleRoot = inputDoc.getDocumentElement();
    String salesOrderHeaderKey = eleRoot.getAttribute(GCConstants.ORDER_HEADER_KEY);

    // Started preparing changeOrder Doc
    YFCDocument docChangeOrder = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleChangeOrder = docChangeOrder.getDocumentElement();
    eleChangeOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, salesOrderHeaderKey);
    YFCElement eleChangeOrderLines = eleChangeOrder.createChild(GCConstants.ORDER_LINES);

    // Creating getOrderList InputDoc
    YFCDocument docGetOrderListIP = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleOrderGetOrderList = docGetOrderListIP.getDocumentElement();
    eleOrderGetOrderList.setAttribute(GCConstants.ORDER_HEADER_KEY, salesOrderHeaderKey);
    YFCDocument docGetOrderListIPTemplate =
        YFCDocument
        .getDocumentFor("<OrderList><Order OrderNo='' OrderHeaderKey=''>"
            + "<OrderLines><OrderLine PrimeLineNo='' KitCode='' FulfillmentType='' CarrierAccountNo='' CarrierServiceCode='' CostCurrency='' "
            + "CurrentWorkOrderKey='' DeliveryCode='' FreightTerms='' GiftFlag='' LevelOfService='' MergeNode='' PackListType='' "
            + "ReceivingNode='' SCAC='' ShipNode='' ShipToID='' ShipToKey='' MaxLineStatus='' MinLineStatus='' "
            + "DependentOnLineKey='' OrderLineKey=''>"
            + "<Extn ExtnIsWarrantyItem='' ExtnWarrantyLineNumber='' ExtnIsShipAlone='' ExtnIsFreeGiftItem=''/>"
            + "<OrderHoldTypes><OrderHoldType HoldType='' Status=''/></OrderHoldTypes>"
            + "<BundleParentLine OrderLineKey='' PrimeLineNo=''/>"
            + "<OrderStatuses><OrderStatus ShipNode='' Status='' /></OrderStatuses></OrderLine></OrderLines></Order></OrderList>");

    YFCDocument docGetOrderListOp =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, docGetOrderListIP, docGetOrderListIPTemplate);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("GetOrderList Output document is" + docGetOrderListOp.toString());
    }

    YFCElement eleOrderList = docGetOrderListOp.getDocumentElement();
    YFCElement eleOrder = eleOrderList.getChildElement(GCConstants.ORDER);
    YFCElement eleOrderLines = eleOrder.getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> nlOrderLine = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);

    Map<String, String> orderLineKeyStatusMap = new HashMap<String, String>();
    getParentLineKeyWithLineStatus(nlOrderLine, orderLineKeyStatusMap);

    Map<String, YFCElement> orderLineMap = new HashMap<String, YFCElement>();
    getParentLineKeyWithLine(nlOrderLine, orderLineMap);

    for (YFCElement eleOrderLine : nlOrderLine) {
      boolean isHoldThereOnDependentLine = false;
      YFCElement eleOrderLineExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
      String sExtnIsWarrantyItem = eleOrderLineExtn.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
      String sMaxLineStatus = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_MAX_STATUS);
      YFCElement eleOrderLineHoldTypes = eleOrderLine.getChildElement(GCConstants.ORDER_HOLD_TYPES);
      if (!YFCCommon.isVoid(eleOrderLineHoldTypes) && YFCCommon.equalsIgnoreCase(sExtnIsWarrantyItem, GCConstants.YES)) {
        YFCNodeList<YFCElement> nlOrderLineHoldType =
            eleOrderLineHoldTypes.getElementsByTagName(GCConstants.ORDER_HOLD_TYPE);
        for (YFCElement eleOrderLineHoldType : nlOrderLineHoldType) {
          String sHoldType = eleOrderLineHoldType.getAttribute(GCConstants.HOLD_TYPE);
          String sStatus = eleOrderLineHoldType.getAttribute(GCConstants.STATUS);
          if (YFCCommon.equalsIgnoreCase(sHoldType, "DEPENDENT_LINE_HOLD")
              && YFCCommon.equalsIgnoreCase(sStatus, "1100")) {
            isHoldThereOnDependentLine = true;
          }
        }
      }

      boolean isHoldResolveNeeded = false;

      if ((YFCCommon.equalsIgnoreCase(GCConstants.YES, sExtnIsWarrantyItem)) && ("2100".compareTo(sMaxLineStatus) > 0)
          && isHoldThereOnDependentLine) {
        String sDependentOnLineKey = eleOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
        isHoldResolveNeeded = checkIfHoldResolveNeededForDependentLine(sDependentOnLineKey, orderLineKeyStatusMap);
        if (isHoldResolveNeeded) {
          YFCElement eleChangeOrderLine = eleChangeOrderLines.createChild(GCConstants.ORDER_LINE);
          eleChangeOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY,
              eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));

          // Stamp ShipNode on warranty as that of parent
          if (YFCCommon.equalsIgnoreCase(GCConstants.YES, sExtnIsWarrantyItem)) {
            stampShipNode(sDependentOnLineKey, eleChangeOrderLine, orderLineMap);
          }

          sOrderLineKeyForSchedule.add(eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
          YFCElement eleChangeOrderLineOrderHoldTypes = eleChangeOrderLine.createChild(GCConstants.ORDER_HOLD_TYPES);
          YFCElement eleChangeOrderLineOrderHoldType =
              eleChangeOrderLineOrderHoldTypes.createChild(GCConstants.ORDER_HOLD_TYPE);
          eleChangeOrderLineOrderHoldType.setAttribute(GCConstants.HOLD_TYPE, "DEPENDENT_LINE_HOLD");
          eleChangeOrderLineOrderHoldType.setAttribute(GCConstants.STATUS, "1300");
          isChangeOrderReqd = true;
        }
      }
      if (sExtnIsWarrantyItem.equals(GCConstants.YES) && "2100".equals(sMaxLineStatus)) {
        String sDependentOnLineKey = eleOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
        if (orderLineKeyStatusMap.containsKey(sDependentOnLineKey)) {
          String parentLineStatus = orderLineKeyStatusMap.get(sDependentOnLineKey);
          if ("3700".equals(parentLineStatus)) {
            String orderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
            String shipNode = eleOrderLine.getAttribute(GCConstants.SHIP_NODE);
            Element chainedOrderEle =
                GCXMLUtil.getElementByXPath(inDoc,
                    "Order/ChainedOrderList/Order[OrderLines/OrderLine/ChainedFromOrderLine/@OrderLineKey='"
                        + orderLineKey + "']");
            String purchaseOrderHdrKey = chainedOrderEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
            autoShipWarranty(env, purchaseOrderHdrKey, shipNode);
          }
        }
      }
    }
    if (isChangeOrderReqd) {
      GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, docChangeOrder, null);
    }

    for (YFCElement eleOrderLine : nlOrderLine) {
      String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      if (sOrderLineKeyForSchedule.contains(sOrderLineKey)) {
        YFCDocument docScheduleOrder = YFCDocument.createDocument(GCConstants.ORDER);
        YFCElement eleScheduleOrder = docScheduleOrder.getDocumentElement();
        eleScheduleOrder.setAttribute(GCXmlLiterals.ALLOCATION_RULE_ID, GCConstants.GC_DEFAULT);
        eleScheduleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, salesOrderHeaderKey);
        eleScheduleOrder.setAttribute(GCConstants.ORDER_LINE_KEY, sOrderLineKey);
        GCCommonUtil.invokeAPI(env, GCConstants.API_SCHEDULE_ORDER, docScheduleOrder, null);
        sOrderLineKeyForSchedule.remove(sOrderLineKey);
      }
    }
    return inDoc;

  }

  private void autoShipWarranty(YFSEnvironment env, String purchanseOrderHdrKey, String shipNode) {


    YFCDocument changeOrderInDoc = YFCDocument.createDocument(GCConstants.ORDER);
    changeOrderInDoc.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, purchanseOrderHdrKey);
    changeOrderInDoc.getDocumentElement().setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
    YFCElement changeOrderOrderLines = changeOrderInDoc.getDocumentElement().createChild(GCConstants.ORDER_LINES);
    YFCDocument getOrderListInDoc = YFCDocument.createDocument(GCConstants.ORDER);
    getOrderListInDoc.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, purchanseOrderHdrKey);
    YFCDocument gteOrderListTmpl =
        YFCDocument
        .getDocumentFor("<OrderList><Order OrderNo='' DocumentType='' EnterpriseCode='' SellerOrganizationCode='' ><OrderLines>"
            + "<OrderLine PrimeLineNo='' SubLineNo='' OrderedQty=''  OrderLineKey='' ShipNode='' >"
            + "<Item ItemID='' UnitOfMeasure='' ProductClass='' />"
            + "</OrderLine>"
            + "</OrderLines>"
            + "</Order>"
            + "</OrderList>");

    YFCDocument getOrderListOutDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListInDoc, gteOrderListTmpl);
    YFCElement orderEle = getOrderListOutDoc.getDocumentElement().getChildElement(GCConstants.ORDER);
    String orderNo = orderEle.getAttribute(GCConstants.ORDER_NO);
    YFCDocument confirmShipmentInDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement shipmentEle = confirmShipmentInDoc.getDocumentElement();
    shipmentEle.setAttribute("OverrideModificationRules", "Y");
    shipmentEle.setAttribute("DocumentType", "0005");
    shipmentEle.setAttribute("EnterpriseCode", orderEle.getAttribute(GCConstants.ENTERPRISE_CODE));
    // shipmentEle.setAttribute("DocumentType", "0005");
    shipmentEle.setAttribute("SellerOrganizationCode", orderEle.getAttribute(GCConstants.SELLER_ORGANIZATION_CODE));
    shipmentEle.setAttribute(GCConstants.SHIP_NODE, shipNode);
    YFCElement shipmentLinesEle = shipmentEle.createChild(GCConstants.SHIPMENT_LINES);
    YFCNodeList<YFCElement> orderLineNodeLst = orderEle.getElementsByTagName(GCConstants.ORDER_LINE);
    for (YFCElement eleOrderLine : orderLineNodeLst) {
      YFCElement changeOrderLine = changeOrderOrderLines.createChild(GCConstants.ORDER_LINE);
      changeOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
      changeOrderLine.setAttribute(GCConstants.SHIP_NODE, shipNode);
      YFCElement shipmentLineEle = shipmentLinesEle.createChild(GCConstants.SHIPMENT_LINE);
      shipmentLineEle.setAttribute(GCConstants.ORDER_NO, orderNo);
      shipmentLineEle.setAttribute(GCConstants.PRIME_LINE_NO, eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO));
      shipmentLineEle.setAttribute(GCConstants.SUB_LINE_NO, eleOrderLine.getAttribute(GCConstants.SUB_LINE_NO));
      shipmentLineEle.setAttribute(GCConstants.QUANTITY, eleOrderLine.getAttribute(GCConstants.ORDERED_QTY));
      YFCElement itemEle = eleOrderLine.getChildElement(GCConstants.ITEM);
      shipmentLineEle.setAttribute(GCConstants.ITEM_ID, itemEle.getAttribute(GCConstants.ITEM_ID));
      shipmentLineEle.setAttribute(GCConstants.UOM, itemEle.getAttribute(GCConstants.UOM));
      shipmentLineEle.setAttribute(GCConstants.PRODUCT_CLASS, itemEle.getAttribute(GCConstants.PRODUCT_CLASS));


    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("ChangeOrder InDoc::" + changeOrderInDoc.toString());
    }
    GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, changeOrderInDoc, null);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("ConfirmShipment XML for warranty PO shipment::" + confirmShipmentInDoc.toString());
    }
    YFCDocument getConfirmShipTemp = YFCDocument.getDocumentFor("<Shipment ShipmentKey='' />");
    YFCDocument shipOutDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.CONFIRM_SHIPMENT_API, confirmShipmentInDoc, getConfirmShipTemp);

    // Creating Shipment Invoice
    YFCDocument createShipInvoiceIndoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement shipInvoiceEle = createShipInvoiceIndoc.getDocumentElement();
    shipInvoiceEle.setAttribute(GCConstants.SELLER_ORGANIZATION_CODE,
        orderEle.getAttribute(GCConstants.SELLER_ORGANIZATION_CODE));
    shipInvoiceEle.setAttribute(GCConstants.SHIP_NODE, shipNode);
    shipInvoiceEle.setAttribute(GCConstants.SHIPMENT_KEY,
        shipOutDoc.getDocumentElement().getAttribute(GCConstants.SHIPMENT_KEY));
    shipInvoiceEle.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.PO_CREATE_SHIP_INVOICE);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("createShipInvoice Indoc :" + createShipInvoiceIndoc.toString());
    }
    GCCommonUtil.invokeAPI(env, GCConstants.CREATE_SHPMNT_INVC_API, createShipInvoiceIndoc.getDocument());


  }

  private void stampShipNode(String sDependentOnLineKey, YFCElement eleChangeOrderLine,
      Map<String, YFCElement> orderLineMap) {

    LOGGER.debug(" Entering GCProcessDependentLineChainedOrder.stampShipNode() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Dependent Line Key is" + sDependentOnLineKey);
      LOGGER.debug("OrderLineKey Map is" + orderLineMap);
    }
    YFCElement eleOrderLine = orderLineMap.get(sDependentOnLineKey);
    YFCNodeList<YFCElement> nlOrderStatus = eleOrderLine.getElementsByTagName(GCConstants.ORDER_STATUS);
    for (YFCElement eleOrderStatus : nlOrderStatus) {

      if (YFCUtils.equals("2100", eleOrderStatus.getAttribute(GCConstants.STATUS))) {
        eleChangeOrderLine.setAttribute(GCConstants.SHIP_NODE, eleOrderStatus.getAttribute(GCConstants.SHIP_NODE));
        break;
      }
    }
    LOGGER.debug(" Exiting GCProcessDependentLineChainedOrder.stampShipNode() method ");
  }

  /**
   * This method checks if Hold resolve is necessary for any warranty line.
   *
   * @param env
   * @param inDoc
   * @return
   */
  private boolean checkIfHoldResolveNeededForDependentLine(String sDependentOnLineKey,
      Map<String, String> orderLineKeyStatusMap) {
    LOGGER
    .beginTimer(" Entering GCProcessDependentLineChainedOrder.checkIfHoldResolveNeededForDependentLine() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Dependent Line Key is" + sDependentOnLineKey);
      LOGGER.debug("OrderLineKey Map is" + orderLineKeyStatusMap);
    }

    String sMaxLineStatus = orderLineKeyStatusMap.get(sDependentOnLineKey);
    if ("2100".compareTo(sMaxLineStatus) <= 0) {
      return true;
    }

    LOGGER.endTimer(" Exiting GCProcessDependentLineChainedOrder.checkIfHoldResolveNeededForDependentLine() method ");
    return false;
  }

  /**
   * This method gets the parent line key with its line status. In case of bundle, it does not do
   * any thing but for component it updates on behalf of its parent
   *
   * @param nlOrderLine
   * @param orderLineKeyStatusMap
   */
  private void getParentLineKeyWithLineStatus(YFCNodeList<YFCElement> nlOrderLine,
      Map<String, String> orderLineKeyStatusMap) {
    for (YFCElement eleOrderLine : nlOrderLine) {
      String sMaxLineStatus = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_MAX_STATUS);
      String kitCode = eleOrderLine.getAttribute(GCConstants.KIT_CODE);
      String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      YFCElement bundleParentLineEle = eleOrderLine.getChildElement(GCConstants.BUNDLE_PARENT_LINE);
      if (!GCConstants.BUNDLE.equalsIgnoreCase(kitCode) && YFCCommon.isVoid(bundleParentLineEle)) {
        LOGGER.verbose(" This is not a bundle. So going to store its orderline key with maxline status");
        orderLineKeyStatusMap.put(sOrderLineKey, sMaxLineStatus);
      }

      if (!YFCCommon.isVoid(bundleParentLineEle)) {
        LOGGER.verbose(" This is a component. So going to store its status with its parent orderline key");
        String sBundleOrderLineKey = bundleParentLineEle.getAttribute("OrderLineKey");
        if (!orderLineKeyStatusMap.containsKey(sBundleOrderLineKey)) {
          LOGGER.verbose(" parent orderline key is not there in the map. So will have to store it");
          orderLineKeyStatusMap.put(sBundleOrderLineKey, sMaxLineStatus);
        }
      }
    }

  }

  /**
   * This method gets the parent line key with its line . In case of bundle, it does not do any
   * thing but for component it updates on behalf of its parent
   *
   * @param nlOrderLine
   * @param orderLineKeyStatusMap
   */
  private void getParentLineKeyWithLine(YFCNodeList<YFCElement> nlOrderLine, Map<String, YFCElement> orderLineMap) {
    for (YFCElement eleOrderLine : nlOrderLine) {
      String kitCode = eleOrderLine.getAttribute(GCConstants.KIT_CODE);
      String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      YFCElement bundleParentLineEle = eleOrderLine.getChildElement(GCConstants.BUNDLE_PARENT_LINE);
      if (!GCConstants.BUNDLE.equalsIgnoreCase(kitCode) && YFCCommon.isVoid(bundleParentLineEle)) {
        LOGGER.verbose(" This is not a bundle. So going to store its orderline key with maxline status");
        orderLineMap.put(sOrderLineKey, eleOrderLine);
      }

      if (!YFCCommon.isVoid(bundleParentLineEle)) {
        LOGGER.verbose(" This is a component. So going to store its status with its parent orderline key");
        String sBundleOrderLineKey = bundleParentLineEle.getAttribute("OrderLineKey");
        if (!orderLineMap.containsKey(sBundleOrderLineKey)) {
          LOGGER.verbose(" parent orderline key is not there in the map. So will have to store it");
          orderLineMap.put(sBundleOrderLineKey, eleOrderLine);
        }
      }
    }

  }
}
