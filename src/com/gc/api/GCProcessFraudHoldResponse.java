/**
 * * Copyright � 2014, Guitar Center,  All rights reserved.
 *#################################################################################################################################################################
 *	        OBJECTIVE: Process Fraud Response 
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               19/02/2014        Singh, Gurpreet            OMS-1144: Fraud
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.util.Properties;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:gurpreet.singh@expicient.com">Gurpreet</a>
 */
public class GCProcessFraudHoldResponse implements YIFCustomApi {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCProcessFraudHoldResponse.class);

    /**
     * Method to process Fraud Response Handles FRAUD_HOLD and FRAUD_REVIEW_HOLD
     * 
     * @param env
     * @param inDoc
     * @throws GCException
     */
    public void processFraudResponse(YFSEnvironment env, Document inDoc)
	    throws GCException {

	LOGGER.debug(" Entering GCProcessFraudHoldResponse.processFraudResponse() method ");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" processFraudResponse() method, Input document is"
		    + GCXMLUtil.getXMLString(inDoc));
	}
	try {
	    Element eleRoot = inDoc.getDocumentElement();
	    String sExtnFraudResponseCode = GCXMLUtil.getAttributeFromXPath(
		    eleRoot, "Extn/@ExtnFraudResponseCode");
	    boolean bFraudResp= !YFCCommon.isVoid(sExtnFraudResponseCode) && !(GCConstants.ACCEPT.equalsIgnoreCase(sExtnFraudResponseCode)
	            || GCConstants.REVIEW.equalsIgnoreCase(sExtnFraudResponseCode) 
	            || GCConstants.REJECT.equalsIgnoreCase(sExtnFraudResponseCode));
	    if (bFraudResp) {
		    Exception ex = new Exception(" Invalid Response Code  "
			    + sExtnFraudResponseCode);
		    throw ex;
	    }

	    String sOrderNo = eleRoot.getAttribute(GCConstants.ORDER_NO);
	    LOGGER.debug("sCustomerPONo" + sOrderNo);

	    // Get order list -
	    Document docGetOrderListInput = GCXMLUtil
		    .createDocument(GCConstants.ORDER);
	    docGetOrderListInput.getDocumentElement().setAttribute(
		    GCConstants.ORDER_NO, sOrderNo);

	    Document docGetOrderListTemplate = GCXMLUtil
		    .getDocument(GCConstants.GET_ORDER_LIST_FRAUD_TEMPLATE);
	    Document docGetOrderListOutput = GCCommonUtil.invokeAPI(env,
		    GCConstants.API_GET_ORDER_LIST, docGetOrderListInput,
		    docGetOrderListTemplate);

	    boolean bIsResponseForPendingReview = false;

	    Element eleOrderHoldFraudHold = GCXMLUtil.getElementByXPath(
		    docGetOrderListOutput, "//OrderHoldType[@HoldType='"
			    + GCConstants.FRAUD_HOLD + "' and @Status='1100']");
	    Element eleOrderHoldPendingReview = GCXMLUtil.getElementByXPath(
		    docGetOrderListOutput, "//OrderHoldType[@HoldType='"
			    + GCConstants.FRAUD_REVIEW
			    + "' and @Status='1100']");

	    if (YFCCommon.isVoid(eleOrderHoldFraudHold)
		    && YFCCommon.isVoid(eleOrderHoldPendingReview)) {
		// fraud hold and fraud review hold are not present on order
		return;
	    } else if (YFCCommon.isVoid(eleOrderHoldFraudHold)
		    && !YFCCommon.isVoid(eleOrderHoldPendingReview)) {
		bIsResponseForPendingReview = true;
	    }

	    Document docChangeOrderInput = createChangeOrder(
		    sExtnFraudResponseCode, docGetOrderListOutput,
		    bIsResponseForPendingReview);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("docChangeOrderInput : "
			+ GCXMLUtil.getXMLString(docChangeOrderInput));
	    }

	    appendNotes(inDoc, docChangeOrderInput);
	    // Fix for Jira 4875 - Adding selectMethod to changeOrder input
	    docChangeOrderInput.getDocumentElement().setAttribute(GCConstants.SELECT_METHOD, GCConstants.WAIT);
	    //Jira 4875 Ends
	    GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER,
		    docChangeOrderInput, null);
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCProcessFraudHoldResponse.processFraudResponse()",
		    e);
	    throw new GCException(e);
	}
	LOGGER.debug(" Exiting GCProcessFraudHoldResponse.processFraudResponse() method ");
    }

    /**
     * Method to create change order document to update fraud
     * 
     * @param eleRoot
     * @param docGetOrderListOutput
     * @param bIsFraudAlreadyResolved
     * @param bIsResponseForPendingReview
     * @return
     * @throws GCException
     */
    public Document createChangeOrder(String sExtnFraudResponseCode,
	    Document docGetOrderListOutput, boolean bIsResponseForPendingReview)
	    throws GCException {

	LOGGER.debug(" Entering GCProcessFraudHoldResponse.createChangeOrder() method ");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" createChangeOrder() method, docGetOrderListOutput is"
		    + GCXMLUtil.getXMLString(docGetOrderListOutput));
	}
	LOGGER.debug(" bIsResponseForPendingReview is : "
		+ bIsResponseForPendingReview);
	Document docChangeOrderInput = null;
	try {
	    String sOrderHeaderKey = GCXMLUtil.getAttributeFromXPath(
		    docGetOrderListOutput, "OrderList/Order/@OrderHeaderKey");

	    // Create changeOrderDoc
	    docChangeOrderInput = GCXMLUtil.createDocument(GCConstants.ORDER);
	    Element eleChangeOrderRootElement = docChangeOrderInput
		    .getDocumentElement();
	    eleChangeOrderRootElement.setAttribute(
		    GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
	    eleChangeOrderRootElement.setAttribute(GCConstants.OVERRIDE,
		    GCConstants.YES);

	    Element eleChangeOrderOrderHoldTypes = docChangeOrderInput
		    .createElement(GCConstants.ORDER_HOLD_TYPES);
	    eleChangeOrderRootElement.appendChild(eleChangeOrderOrderHoldTypes);

	    if (GCConstants.ACCEPT.equalsIgnoreCase(sExtnFraudResponseCode)) {
		LOGGER.debug("Response is Accept from Accertify");
		Element eleFraudHoldType = docChangeOrderInput
			.createElement(GCConstants.ORDER_HOLD_TYPE);
		eleChangeOrderOrderHoldTypes.appendChild(eleFraudHoldType);

		if (bIsResponseForPendingReview) {
		    LOGGER.debug("Resolve Fraud Pending Hold");
		    eleFraudHoldType.setAttribute(GCConstants.HOLD_TYPE,
			    GCConstants.FRAUD_REVIEW);
		} else {
		    LOGGER.debug("Resolve Fraud Hold");
		    eleFraudHoldType.setAttribute(GCConstants.HOLD_TYPE,
			    GCConstants.FRAUD_HOLD);
		}
		eleFraudHoldType.setAttribute(GCConstants.STATUS,
			GCConstants.RESOLVE_HOLD_CODE);

	    } else if (GCConstants.REJECT
		    .equalsIgnoreCase(sExtnFraudResponseCode)) {
		LOGGER.debug("Response is Reject from Accertify, Cancel the order");

		eleChangeOrderRootElement.setAttribute(GCConstants.ACTION,
			GCConstants.ACTION_CANCEL);

	    } else if (GCConstants.REVIEW
		    .equalsIgnoreCase(sExtnFraudResponseCode)) {
		LOGGER.debug("Response is Review from Accertify, resolve this hold & apply pending review hold");

		Element eleFraudHoldType = docChangeOrderInput
			.createElement(GCConstants.ORDER_HOLD_TYPE);
		eleChangeOrderOrderHoldTypes.appendChild(eleFraudHoldType);

		eleFraudHoldType.setAttribute(GCConstants.HOLD_TYPE,
			GCConstants.FRAUD_HOLD);
		eleFraudHoldType.setAttribute(GCConstants.STATUS,
			GCConstants.RESOLVE_HOLD_CODE);

		// Apply fraud pending hold
		Element eleFraudPendingHoldType = docChangeOrderInput
			.createElement(GCConstants.ORDER_HOLD_TYPE);
		eleChangeOrderOrderHoldTypes
			.appendChild(eleFraudPendingHoldType);
		eleFraudPendingHoldType.setAttribute(GCConstants.HOLD_TYPE,
			GCConstants.FRAUD_REVIEW);
		eleFraudPendingHoldType.setAttribute(GCConstants.STATUS,
			GCConstants.APPLY_HOLD_CODE);
	    }

	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCProcessFraudHoldResponse.createChangeOrder()",
		    e);
	    throw new GCException(e);
	}
	LOGGER.debug("AldoFraudHoldProcessResponse : createChangeOrder - End");
	return docChangeOrderInput;
    }

    private void appendNotes(Document inDoc, Document docChangeOrderInput)
	    throws GCException {
	LOGGER.debug(" Entering GCProcessFraudHoldResponse.appendNotes() method ");
	try {

	    Element eleInDocNotes = (Element) XPathAPI.selectSingleNode(inDoc,
		    "Order/Notes");
	    Element eleChgOrdersNotes = GCXMLUtil.createElement(
		    docChangeOrderInput, GCConstants.NOTES, null);
	    docChangeOrderInput.getDocumentElement().appendChild(
		    eleChgOrdersNotes);
	    GCXMLUtil.copyElement(docChangeOrderInput, eleInDocNotes,
		    eleChgOrdersNotes);
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCProcessFraudHoldResponse.appendNotes()",
		    e);
	    throw new GCException(e);
	}
	LOGGER.debug(" Exiting GCProcessFraudHoldResponse.appendNotes() method ");
    }

    /**
     * 
     * Description of setProperties Properties
     * 
     * @param arg0
     * @throws Exception
     * @see com.yantra.interop.japi.YIFCustomApi#setProperties(java.util.Properties)
     */
    public void setProperties(Properties arg0) throws GCException {
	/**
	 * No property is configured
	 */
    }

}
