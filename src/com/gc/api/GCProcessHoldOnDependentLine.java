/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: This class is used to resolve the holds on the warranty lines when their parent line is released.
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0             04/05/2015          Saxena, Ekansh      This class is used to resolve the holds on the warranty lines when their parent line is released.
 #################################################################################################################################################################
 */
package com.gc.api;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class is used to resolve the holds on the warranty lines when their parent line is released.
 *
 * @author Ekansh Saxena
 *
 */

public class GCProcessHoldOnDependentLine {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCProcessHoldOnDependentLine.class);

  public Document processHoldOnDependentOrderLine(YFSEnvironment env, Document inDoc) {

    LOGGER.beginTimer(" Entering GCProcessHoldOnDependentLine.processHoldOnDependentOrderLine() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" processHoldOnDependentOrderLine() method, Input document is" + GCXMLUtil.getXMLString(inDoc));
    }
    boolean isChangeOrderReqd = false;
    List<String> sOrderLineKeyForSchedule = new ArrayList<String>();
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleRoot = inputDoc.getDocumentElement();
    String sOrderHeaderKey = eleRoot.getAttribute(GCConstants.ORDER_HEADER_KEY);

    // This map is used to store ReleaseKey as key & ReleaseNo as value in the map
    Map<String, String> releaseKeyAndNoMap = new HashMap<String, String>();
    List<String> releaseKeyList = new ArrayList<String>();
    callReleaseListAndPrepareMap(env, sOrderHeaderKey, releaseKeyAndNoMap, releaseKeyList);

    YFCDocument docChangeOrder = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleChangeOrder = docChangeOrder.getDocumentElement();
    eleChangeOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    YFCElement eleChangeOrderLines = eleChangeOrder.createChild(GCConstants.ORDER_LINES);

    YFCDocument docGetOrderListIP = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleOrderGetOrderList = docGetOrderListIP.getDocumentElement();
    eleOrderGetOrderList.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    YFCDocument docGetOrderListIPTemplate =
        YFCDocument
        .getDocumentFor("<OrderList><Order OrderNo='' OrderHeaderKey=''>"
            + "<OrderLines><OrderLine PrimeLineNo='' KitCode='' FulfillmentType='' CarrierAccountNo='' CarrierServiceCode='' CostCurrency='' "
            + "CurrentWorkOrderKey='' DeliveryCode='' FreightTerms='' GiftFlag='' LevelOfService='' MergeNode='' PackListType='' "
            + "ReceivingNode='' SCAC='' ShipNode='' ShipToID='' ShipToKey='' MaxLineStatus='' MinLineStatus='' "
            + "DependentOnLineKey='' OrderLineKey=''>"
            + "<Extn ExtnIsWarrantyItem='' ExtnWarrantyLineNumber='' ExtnIsShipAlone='' ExtnIsFreeGiftItem=''/>"
            + "<OrderHoldTypes><OrderHoldType HoldType='' Status=''/></OrderHoldTypes>"
            + "<BundleParentLine OrderLineKey='' PrimeLineNo=''/>"
            + "<OrderStatuses><OrderStatus/></OrderStatuses></OrderLine></OrderLines></Order></OrderList>");

    YFCDocument docGetOrderListOp =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, docGetOrderListIP, docGetOrderListIPTemplate);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("GetOrderList Output document is" + docGetOrderListOp.toString());
    }

    YFCElement eleOrderList = docGetOrderListOp.getDocumentElement();
    YFCElement eleOrder = eleOrderList.getChildElement(GCConstants.ORDER);
    YFCElement eleOrderLines = eleOrder.getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> nlOrderLine = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);

    // prepare dependentReleaseKeyMap Map
    Map<String, String> dependentReleaseKeyMap = new HashMap<String, String>();

    // Prepare a list for ShipAlone OrderLineKey
    List<String> shipAloneLineKeyList = new ArrayList<String>();

    prepareDependentReleaseKeyMap(dependentReleaseKeyMap, nlOrderLine, eleChangeOrderLines, releaseKeyAndNoMap,
        shipAloneLineKeyList);

    for (String releaseKey : releaseKeyList) {
      YFCDocument changeReleaseIndoc = YFCDocument.createDocument(GCConstants.ORDER_RELEASE);
      YFCElement eleChangeRelease = changeReleaseIndoc.getDocumentElement();
      eleChangeRelease.setAttribute(GCConstants.ORDER_RELEASE_KEY, releaseKey);
      eleChangeRelease.setAttribute("PacklistType", releaseKeyAndNoMap.get(releaseKey));
      GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_RELEASE, changeReleaseIndoc, null);
    }

    Map<String, String> orderLineKeyStatusMap = new HashMap<String, String>();
    getParentLineKeyWithLineStatus(nlOrderLine, orderLineKeyStatusMap);
    Map<String, YFCElement> orderLineMap = new HashMap<String, YFCElement>();
    getParentLineKeyWithLine(nlOrderLine, orderLineMap);
    for (YFCElement eleOrderLine : nlOrderLine) {
      boolean isHoldThereOnDependentLine = false;
      YFCElement eleOrderLineExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
      String sExtnIsWarrantyItem = eleOrderLineExtn.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
      String sExtnIsFreeGiftItem = eleOrderLineExtn.getAttribute(GCConstants.EXTN_IS_FREE_GIFT_ITEM);
      String sMaxLineStatus = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_MAX_STATUS);
      YFCElement eleOrderLineHoldTypes = eleOrderLine.getChildElement(GCConstants.ORDER_HOLD_TYPES);
      if (!YFCCommon.isVoid(eleOrderLineHoldTypes)
          && (YFCCommon.equalsIgnoreCase(sExtnIsWarrantyItem, GCConstants.YES) || YFCCommon.equalsIgnoreCase(
              sExtnIsFreeGiftItem, GCConstants.YES))) {
        YFCNodeList<YFCElement> nlOrderLineHoldType =
            eleOrderLineHoldTypes.getElementsByTagName(GCConstants.ORDER_HOLD_TYPE);
        for (YFCElement eleOrderLineHoldType : nlOrderLineHoldType) {
          String sHoldType = eleOrderLineHoldType.getAttribute(GCConstants.HOLD_TYPE);
          String sStatus = eleOrderLineHoldType.getAttribute(GCConstants.STATUS);
          if (YFCCommon.equalsIgnoreCase(sHoldType, "DEPENDENT_LINE_HOLD")
              && YFCCommon.equalsIgnoreCase(sStatus, "1100")) {
            isHoldThereOnDependentLine = true;
          }
        }
      }

      boolean isHoldResolveNeeded = false;

      if ((YFCCommon.equalsIgnoreCase(GCConstants.YES, sExtnIsWarrantyItem) || YFCCommon.equalsIgnoreCase(
          sExtnIsFreeGiftItem, GCConstants.YES))
          && ("3200".compareTo(sMaxLineStatus) > 0)
          && isHoldThereOnDependentLine) {
        String sDependentOnLineKey = eleOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
        isHoldResolveNeeded = checkIfHoldResolveNeededForDependentLine(sDependentOnLineKey, orderLineKeyStatusMap);
        if (isHoldResolveNeeded) {
          YFCElement eleChangeOrderLine = eleChangeOrderLines.createChild(GCConstants.ORDER_LINE);
          // Check if warranty then stamp parent attributes for release consolidation
          if (YFCCommon.equalsIgnoreCase(GCConstants.YES, sExtnIsWarrantyItem)
              && !shipAloneLineKeyList.contains(eleOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY))) {
            stampParentLineAttributesOnDependentLine(sDependentOnLineKey, eleChangeOrderLine, orderLineMap,
                dependentReleaseKeyMap, releaseKeyAndNoMap);
          } else if (shipAloneLineKeyList.contains(eleOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY))) {

            YFCElement eleOrderLineShipALone = orderLineMap.get(sDependentOnLineKey);
            eleChangeOrderLine.setAttribute("ShipNode", eleOrderLineShipALone.getAttribute("ShipNode"));
            eleChangeOrderLine.setAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE, GCConstants.YES);
          }
          sOrderLineKeyForSchedule.add(eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
          eleChangeOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY,
              eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
          YFCElement eleChangeOrderLineOrderHoldTypes = eleChangeOrderLine.createChild(GCConstants.ORDER_HOLD_TYPES);
          YFCElement eleChangeOrderLineOrderHoldType =
              eleChangeOrderLineOrderHoldTypes.createChild(GCConstants.ORDER_HOLD_TYPE);
          eleChangeOrderLineOrderHoldType.setAttribute(GCConstants.HOLD_TYPE, "DEPENDENT_LINE_HOLD");
          eleChangeOrderLineOrderHoldType.setAttribute(GCConstants.STATUS, "1300");
          isChangeOrderReqd = true;
        }
      }
    }
    if (isChangeOrderReqd) {
      GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, docChangeOrder, null);
    }

    boolean isReleaseOrderReqd = false;

    for (YFCElement eleOrderLine : nlOrderLine) {
      String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      if (sOrderLineKeyForSchedule.contains(sOrderLineKey)) {
        YFCDocument docScheduleOrder = YFCDocument.createDocument(GCConstants.ORDER);
        YFCElement eleScheduleOrder = docScheduleOrder.getDocumentElement();
        eleScheduleOrder.setAttribute(GCXmlLiterals.ALLOCATION_RULE_ID, GCConstants.GC_DEFAULT);
        eleScheduleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
        eleScheduleOrder.setAttribute(GCConstants.ORDER_LINE_KEY, sOrderLineKey);
        GCCommonUtil.invokeAPI(env, GCConstants.API_SCHEDULE_ORDER, docScheduleOrder, null);
        sOrderLineKeyForSchedule.remove(sOrderLineKey);
        isReleaseOrderReqd = true;
      }
    }

    if (isReleaseOrderReqd) {
      YFCDocument docReleaseOrder = YFCDocument.createDocument(GCConstants.ORDER);
      YFCElement eleReleaseOrder = docReleaseOrder.getDocumentElement();
      eleReleaseOrder.setAttribute(GCXmlLiterals.ALLOCATION_RULE_ID, GCConstants.GC_DEFAULT);
      eleReleaseOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
      eleReleaseOrder.setAttribute(GCConstants.IGNORE_RELEASE_DATE, GCConstants.YES);
      GCCommonUtil.invokeAPI(env, GCConstants.API_RELEASE_ORDER, docReleaseOrder, null);
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Updated document is" + inputDoc.toString());
    }
    LOGGER.endTimer(" Exiting GCProcessHoldOnDependentLine.processHoldOnDependentOrderLine() method ");
    return inputDoc.getDocument();
  }

  /**
   * This method is used to store releaseNo & releaseKey in the map
   *
   * @param env
   * @param sOrderHeaderKey
   * @param releaseKeyAndNoMap
   * @param releaseKeyList
   */
  private void callReleaseListAndPrepareMap(YFSEnvironment env, String sOrderHeaderKey,
      Map<String, String> releaseKeyAndNoMap, List<String> releaseKeyList) {

    YFCDocument releaseListIndoc = YFCDocument.createDocument(GCConstants.ORDER_RELEASE);
    YFCElement eleIndocRoot = releaseListIndoc.getDocumentElement();
    eleIndocRoot.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    YFCDocument docGetReleaseListIPTemplate =
        YFCDocument
        .getDocumentFor("<OrderReleaseList><OrderRelease ReleaseNo='' OrderReleaseKey='' OrderHeaderKey=''/>"
            + "</OrderReleaseList>");
    YFCDocument releaseListOutdoc =
        GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_RELEASE_LIST, releaseListIndoc,
            docGetReleaseListIPTemplate);
    YFCElement eleRootOutdoc = releaseListOutdoc.getDocumentElement();
    YFCNodeList<YFCElement> nlOdrRelease = eleRootOutdoc.getElementsByTagName(GCConstants.ORDER_RELEASE);
    for (YFCElement eleOdrRelease : nlOdrRelease) {
      releaseKeyAndNoMap.put(eleOdrRelease.getAttribute(GCConstants.ORDER_RELEASE_KEY),
          eleOdrRelease.getAttribute(GCConstants.RELEASE_NO));
      releaseKeyList.add(eleOdrRelease.getAttribute(GCConstants.ORDER_RELEASE_KEY));
    }
  }

  /**
   * This method is used to prepare dependentReleaseKeyMap Map which stores OrderLineKey or or
   * parent OrderLineKey(in case of set) as key & OrderReleaseKey as value.
   *
   * @param dependentReleaseKeyMap
   * @param nlOrderLine
   * @param eleChangeOrderLines
   * @param releaseKeyAndNoMap
   * @param shipAloneLineKeyList
   */
  private void prepareDependentReleaseKeyMap(Map<String, String> dependentReleaseKeyMap,
      YFCNodeList<YFCElement> nlOrderLine, YFCElement eleChangeOrderLines, Map<String, String> releaseKeyAndNoMap,
      List<String> shipAloneLineKeyList) {

    LOGGER.beginTimer(" Entering GCProcessHoldOnDependentLine.checkIfHoldResolveNeededForDependentLine() method ");
    LOGGER.debug(" Entering GCProcessHoldOnDependentLine.checkIfHoldResolveNeededForDependentLine() method-- Start ");
    String latestReleaseKey = null;
    BigDecimal bdLatestReleaseKey = new BigDecimal("0");
    for (YFCElement eleOrderLine : nlOrderLine) {
      String orderLineKey = null;
      String dependentOnLineKey = eleOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
      YFCElement eleOdrLineExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
      YFCElement bundleParentLineEle = eleOrderLine.getChildElement(GCConstants.BUNDLE_PARENT_LINE);
      if (!YFCCommon.isVoid(eleOdrLineExtn.getAttribute(GCConstants.EXTN_IS_SHIP_ALONE))) {
        shipAloneLineKeyList.add(eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
        if (!YFCCommon.isVoid(bundleParentLineEle)) {
          shipAloneLineKeyList.add(bundleParentLineEle.getAttribute(GCConstants.ORDER_LINE_KEY));
        }
      }
      if ((YFCUtils.equals("3200", eleOrderLine.getAttribute(GCConstants.MIN_LINE_STATUS)))) {

        YFCElement eleOdrLineStatuses = eleOrderLine.getChildElement(GCConstants.ORDER_STATUSES);
        YFCNodeList<YFCElement> nlOdrLineStatus = eleOdrLineStatuses.getElementsByTagName(GCConstants.ORDER_STATUS);
        for(YFCElement eleOdrLineStatus : nlOdrLineStatus){

          String currentReleaseKey = eleOdrLineStatus.getAttribute(GCConstants.ORDER_RELEASE_KEY);
          if (!YFCCommon.isVoid(bundleParentLineEle) && YFCUtils.equals("3200", eleOdrLineStatus.getAttribute(GCConstants.STATUS))) {
            orderLineKey = bundleParentLineEle.getAttribute(GCConstants.ORDER_LINE_KEY);
            dependentReleaseKeyMap.put(orderLineKey, currentReleaseKey);

            YFCElement eleChangeOrderLine = eleChangeOrderLines.createChild(GCConstants.ORDER_LINE);
            eleChangeOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY,
                eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
            eleChangeOrderLine.setAttribute("PackListType", releaseKeyAndNoMap.get(currentReleaseKey));
            if (!YFCCommon.isVoid(latestReleaseKey)) {
              bdLatestReleaseKey = new BigDecimal(latestReleaseKey);
            }
            BigDecimal bdReleaseKey = new BigDecimal(currentReleaseKey);
            // fetching latest releaseKey
            if (YFCCommon.isVoid(latestReleaseKey) || (bdReleaseKey.compareTo(bdLatestReleaseKey) > 0)) {
              latestReleaseKey = currentReleaseKey;
            }
          } else if (YFCCommon.isVoid(dependentOnLineKey) && YFCUtils.equals("3200", eleOdrLineStatus.getAttribute(GCConstants.STATUS))) {
            orderLineKey = eleOdrLineStatus.getAttribute(GCConstants.ORDER_LINE_KEY);
            dependentReleaseKeyMap.put(orderLineKey, currentReleaseKey);

            YFCElement eleChangeOrderLine = eleChangeOrderLines.createChild(GCConstants.ORDER_LINE);
            eleChangeOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY,
                eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
            eleChangeOrderLine.setAttribute("PackListType", releaseKeyAndNoMap.get(currentReleaseKey));
            if (!YFCCommon.isVoid(latestReleaseKey)) {
              bdLatestReleaseKey = new BigDecimal(latestReleaseKey);
            }
            BigDecimal bdReleaseKey = new BigDecimal(currentReleaseKey);
            // fetching Latest releaseKey
            if (YFCCommon.isVoid(latestReleaseKey) || (bdReleaseKey.compareTo(bdLatestReleaseKey) > 0)) {
              latestReleaseKey = currentReleaseKey;
            }
          }
        }
      }
    }
    LOGGER.debug(" Entering GCProcessHoldOnDependentLine.checkIfHoldResolveNeededForDependentLine() method-- End ");
    LOGGER.endTimer(" Entering GCProcessHoldOnDependentLine.checkIfHoldResolveNeededForDependentLine() method ");
  }

  /**
   * This method checks if Hold resolve is necessary for any warranty line.
   *
   * @param env
   * @param inDoc
   * @return
   */
  private boolean checkIfHoldResolveNeededForDependentLine(String sDependentOnLineKey, Map<String, String> orderLineKeyStatusMap) {
    LOGGER.beginTimer(" Entering GCProcessHoldOnDependentLine.checkIfHoldResolveNeededForDependentLine() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Dependent Line Key is" + sDependentOnLineKey);
      LOGGER.debug("OrderLineKey Map is" + orderLineKeyStatusMap);
    }

    String sMaxLineStatus = orderLineKeyStatusMap.get(sDependentOnLineKey);
    if ("3200".compareTo(sMaxLineStatus) <= 0) {
      return true;
    }

    LOGGER.endTimer(" Exiting GCProcessHoldOnDependentLine.checkIfHoldResolveNeededForDependentLine() method ");
    return false;
  }


  /**
   * This method stamps the parent line attribute on the warranty line.
   *
   * @param releaseKeyAndNoMap
   *
   * @param dependentReleaseKeyMap
   *
   * @param env
   * @param inDoc
   * @return
   */
  private YFCElement stampParentLineAttributesOnDependentLine(String sDependentOnLineKey,
      YFCElement eleChangeOrderLine, Map<String, YFCElement> orderLineMap, Map<String, String> dependentReleaseKeyMap,
      Map<String, String> releaseKeyAndNoMap) {
    LOGGER.beginTimer(" Entering GCProcessHoldOnDependentLine.stampParentLineAttributesOnDependentLine() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Dependent Line Key is" + sDependentOnLineKey);
      LOGGER.debug("OrderLineKey Map is" + orderLineMap);
      LOGGER.debug("dependentReleaseKeyMap Map is" + dependentReleaseKeyMap);
      LOGGER.debug("releaseKeyAndNoMap Map is" + releaseKeyAndNoMap);
    }
    YFCElement eleOrderLine = orderLineMap.get(sDependentOnLineKey);

    eleChangeOrderLine.setAttribute("FulfillmentType", eleOrderLine.getAttribute("FulfillmentType"));
    eleChangeOrderLine.setAttribute("CarrierAccountNo", eleOrderLine.getAttribute("CarrierAccountNo"));
    eleChangeOrderLine.setAttribute("CarrierServiceCode", eleOrderLine.getAttribute("CarrierServiceCode"));
    eleChangeOrderLine.setAttribute("CostCurrency", eleOrderLine.getAttribute("CostCurrency"));
    eleChangeOrderLine.setAttribute("CurrentWorkOrderKey", eleOrderLine.getAttribute("CurrentWorkOrderKey"));
    eleChangeOrderLine.setAttribute("DeliveryCode", eleOrderLine.getAttribute("DeliveryCode"));
    eleChangeOrderLine.setAttribute("FreightTerms", eleOrderLine.getAttribute("FreightTerms"));
    eleChangeOrderLine.setAttribute("GiftFlag", eleOrderLine.getAttribute("GiftFlag"));
    eleChangeOrderLine.setAttribute("LevelOfService", eleOrderLine.getAttribute("LevelOfService"));
    eleChangeOrderLine.setAttribute("MergeNode", eleOrderLine.getAttribute("MergeNode"));
    eleChangeOrderLine.setAttribute("ReceivingNode", eleOrderLine.getAttribute("ReceivingNode"));
    eleChangeOrderLine.setAttribute("SCAC", eleOrderLine.getAttribute("SCAC"));
    eleChangeOrderLine.setAttribute("ShipNode", eleOrderLine.getAttribute("ShipNode"));
    eleChangeOrderLine.setAttribute("ShipToID", eleOrderLine.getAttribute("ShipToID"));
    eleChangeOrderLine.setAttribute("ShipToKey", eleOrderLine.getAttribute("ShipToKey"));
    eleChangeOrderLine.setAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE, GCConstants.YES);
    String releaseKey = dependentReleaseKeyMap.get(sDependentOnLineKey);
    eleChangeOrderLine.setAttribute("PackListType", releaseKeyAndNoMap.get(releaseKey));
    YFCElement eleExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
    if(!YFCCommon.isVoid(eleExtn)){
      String sExtnIsShipAlone = eleExtn.getAttribute("ExtnIsShipAlone");
      YFCElement eleChangeOrderLineExtn = eleChangeOrderLine.getChildElement(GCConstants.EXTN, true);
      eleChangeOrderLineExtn.setAttribute("ExtnIsShipAlone", sExtnIsShipAlone);
    }
    LOGGER.endTimer(" Exiting GCProcessHoldOnDependentLine.stampParentLineAttributesOnDependentLine() method ");
    return eleChangeOrderLine;
  }


  /**
   * This method gets the parent line key with its line status. In case of bundle, it does not do
   * any thing but for component it updates on behalf of its parent
   *
   * @param nlOrderLine
   * @param orderLineKeyStatusMap
   */
  private void getParentLineKeyWithLineStatus(YFCNodeList<YFCElement> nlOrderLine,
      Map<String, String> orderLineKeyStatusMap) {
    for (YFCElement eleOrderLine : nlOrderLine) {
      String sMaxLineStatus = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_MAX_STATUS);
      String kitCode = eleOrderLine.getAttribute(GCConstants.KIT_CODE);
      String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      YFCElement bundleParentLineEle = eleOrderLine.getChildElement(GCConstants.BUNDLE_PARENT_LINE);
      if (!GCConstants.BUNDLE.equalsIgnoreCase(kitCode) && YFCCommon.isVoid(bundleParentLineEle)) {
        LOGGER.verbose(" This is not a bundle. So going to store its orderline key with maxline status");
        orderLineKeyStatusMap.put(sOrderLineKey, sMaxLineStatus);
      }

      if (!YFCCommon.isVoid(bundleParentLineEle)) {
        LOGGER.verbose(" This is a component. So going to store its status with its parent orderline key");
        String sBundleOrderLineKey = bundleParentLineEle.getAttribute("OrderLineKey");
        if (!orderLineKeyStatusMap.containsKey(sBundleOrderLineKey)) {
          LOGGER.verbose(" parent orderline key is not there in the map. So will have to store it");
          orderLineKeyStatusMap.put(sBundleOrderLineKey, sMaxLineStatus);
        }
      }
    }

  }

  /**
   * This method gets the parent line key with its line . In case of bundle, it does not do
   * any thing but for component it updates on behalf of its parent
   *
   * @param nlOrderLine
   * @param orderLineKeyStatusMap
   */
  private void getParentLineKeyWithLine(YFCNodeList<YFCElement> nlOrderLine,
      Map<String, YFCElement> orderLineMap) {
    for (YFCElement eleOrderLine : nlOrderLine) {
      String kitCode = eleOrderLine.getAttribute(GCConstants.KIT_CODE);
      String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      YFCElement bundleParentLineEle = eleOrderLine.getChildElement(GCConstants.BUNDLE_PARENT_LINE);
      if (!GCConstants.BUNDLE.equalsIgnoreCase(kitCode) && YFCCommon.isVoid(bundleParentLineEle)) {
        LOGGER.verbose(" This is not a bundle. So going to store its orderline key with maxline status");
        orderLineMap.put(sOrderLineKey, eleOrderLine);
      }

      if (!YFCCommon.isVoid(bundleParentLineEle)) {
        LOGGER.verbose(" This is a component. So going to store its status with its parent orderline key");
        String sBundleOrderLineKey = bundleParentLineEle.getAttribute("OrderLineKey");
        if (!orderLineMap.containsKey(sBundleOrderLineKey)) {
          LOGGER.verbose(" parent orderline key is not there in the map. So will have to store it");
          orderLineMap.put(sBundleOrderLineKey, eleOrderLine);
        }
      }
    }

  }
}
