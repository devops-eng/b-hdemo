package com.gc.api;

import java.util.Properties;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSUserExitException;

public class GCProcessItemSubstitution implements YIFCustomApi {
	YFSEnvironment env = null;
	Document ipDoc = null;

	Document docGetCompleteItemListOp = null;
	String sSubbedItemID = "";

	private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCProcessItemSubstitution.class.getName());


	@Override
	public void setProperties(Properties arg0) throws GCException {

	}

  public void substituteOrderLine(YFSEnvironment env, Document ipDoc) throws GCException {

		final String logInfo = "GCProcessItemSubstitution :: substituteOrderLine :: ";
		LOGGER.verbose(logInfo + "Begin");

		LOGGER.debug(logInfo + "Input Document" + GCXMLUtil.getXMLString(ipDoc));

		this.env = env;
		this.ipDoc = ipDoc;
		try {
			sSubbedItemID = XPathAPI.selectSingleNode(ipDoc, "//@SubbedItemID").getNodeValue();
			callGetCompleteItemList(sSubbedItemID);

		} catch (Exception e) {
			LOGGER.error("The exception is in substituteOrderLine method", e);
			throw GCException.getYFCException(e.getMessage(), e);
		}

		try {
			checkIfOrderIsLessThanReleasedStatus(env, ipDoc);
			String sSubbedItemPriceType = GCXMLUtil.getAttributeFromXPath(ipDoc, "//@SubbedItemPriceType");

			String sOrderedQty = XPathAPI.selectSingleNode(ipDoc, "//OrderLine/@OrderedQty").getNodeValue();
			LOGGER.debug(logInfo + "sOrderedQty :: " + sOrderedQty);

			String sSubbedItemPrice = "";
			String sFulfillmentType = GCXMLUtil.getAttributeFromXPath(ipDoc, "//OrderLine/@FulfillmentType");
			if (GCConstants.INT_ZERO.equals(sSubbedItemPriceType)) {
				// subbed in item price is set to its default price
				Element eleComputedPrice = (Element) XPathAPI.selectSingleNode(docGetCompleteItemListOp, "//ComputedPrice");
				if (!YFCCommon.isVoid(eleComputedPrice)) {
					sSubbedItemPrice = eleComputedPrice.getAttribute(GCConstants.LIST_PRICE);
				}
			} else if (GCConstants.INT_ONE.equals(sSubbedItemPriceType)) {
				// subbed in item price is set to match the replaced item price
				sSubbedItemPrice = GCXMLUtil.getAttributeFromXPath(ipDoc, "//OrderLine/LinePriceInfo/@UnitPrice");
			} else if (GCConstants.INT_TWO.equals(sSubbedItemPriceType)) {
				// subbed in item price is set to the manually overridden price
				sSubbedItemPrice = GCXMLUtil.getAttributeFromXPath(ipDoc, "//OrderLine/@SubbedItemPrice");
			}
			LOGGER.debug(logInfo + "sSubbedItemPrice :: " + sSubbedItemPrice);
			String sStatusQty = null;
			String sOrderLineTranQuantity = null;
			String sTotalStatusQty = "0";
			NodeList nlStatusList =
					XPathAPI.selectNodeList(ipDoc,
							"//OrderStatuses/OrderStatus[@StatusDescription!='Shipped' and @StatusDescription!='Cancelled']");
			for (int i = 0; i < nlStatusList.getLength(); i++) {
				Element eleStatus = (Element) nlStatusList.item(i);
				sStatusQty =
						GCXMLUtil.getAttributeFromXPath(GCXMLUtil.getDocument(eleStatus, true), "//OrderStatus/@StatusQty");
				sTotalStatusQty = String.valueOf(Double.valueOf(sTotalStatusQty) + Double.valueOf(sStatusQty));
				LOGGER.debug(logInfo + "sOrderLineTranQuantity :: " + sOrderLineTranQuantity);
			}
			sOrderLineTranQuantity = String.valueOf(Double.valueOf(sOrderedQty) - Double.valueOf(sTotalStatusQty));
			callChangeOrderForItemSubstitution(sSubbedItemPrice, sSubbedItemID, sOrderLineTranQuantity, sTotalStatusQty,
					sSubbedItemPriceType, sFulfillmentType);

		} catch (Exception e) {
			LOGGER.error("The exception is in substituteOrderLine method", e);
			throw GCException.getYFCException(e.getMessage(), e);
		}
		LOGGER.verbose(logInfo + "End");
	}

	/**
	 *
	 * @param env
	 * @param ipDoc2
	 * @return
	 * @throws GCException
	 */
	private void checkIfOrderIsLessThanReleasedStatus(YFSEnvironment env, Document ipDoc2) throws GCException {

		try {
			LOGGER.debug("Method:: checkIfOrderIsLessThanReleasedStatus:: Begin");
			String sOrderHeaderKey = GCXMLUtil.getAttributeFromXPath(ipDoc2, "//@OrderHeaderKey");
			Document docGetOrderListTemplate = null;
			if (!YFCCommon.isVoid(sOrderHeaderKey)) {
				LOGGER.debug("Inside if of checkIfOrderIsLessThanReleasedStatus");
				docGetOrderListTemplate = GCXMLUtil.createDocument(GCConstants.ORDER_LIST);
				Element eleOrderList = docGetOrderListTemplate.getDocumentElement();
				Element eleOrder = docGetOrderListTemplate.createElement(GCConstants.ORDER);
				eleOrder.setAttribute(GCConstants.MAX_ORDER_STATUS, "");
				eleOrder.setAttribute(GCConstants.ORDER_NO, "");
				eleOrderList.appendChild(eleOrder);

				Document docGetOrderListOp = GCCommonUtil.invokeGetOrderListAPI(env, sOrderHeaderKey, docGetOrderListTemplate);
				LOGGER.debug("getorderList outdoc" + GCXMLUtil.getXMLString(docGetOrderListOp));
				String sMaxOrderStatus = GCXMLUtil.getAttributeFromXPath(docGetOrderListOp, "//@MaxOrderStatus");

				if (!YFCCommon.isVoid(sMaxOrderStatus)) {
					String[] parts = sMaxOrderStatus.split("\\.");
					String sOrderStatus = parts[0];
					LOGGER.debug("sOrderStatus::" + sOrderStatus);

					Double dMaxOrderStatus = Double.parseDouble(sOrderStatus);
					Double dReleasedStatus = Double.parseDouble(GCConstants.RELEASED_STATUS);
					if (dMaxOrderStatus >= dReleasedStatus) {
						YFCException yfs = new YFCException(GCErrorConstants.ITEM_CANNOT_BE_ADDED_ERROR_CODE);
						yfs.setErrorDescription(GCConstants.ITEM_CANNOT_BE_SUBSTITUTED_AT_CURRENT_STATUS);
						throw yfs;
					}

				}
			}

			LOGGER.debug("Method:: checkIfOrderIsLessThanReleasedStatus:: End");
		} catch (Exception e) {
			LOGGER.error(" Inside catch block of checkIfOrderIsLessThanReleasedStatus(), Exception is :", e);
			throw GCException.getYFCException(e.getMessage(), e);
		}

	}



	/**
	 *
	 * @param sSubbedItemPrice
	 * @param sSubbedItemID
	 * @param sOrderLineTranQuantity
	 * @param sStatusQty
	 * @param sSubbedItemPriceType
	 * @param sFulfillmentType
	 * @throws Exception
	 */
	public void callChangeOrderForItemSubstitution(String sSubbedItemPrice, String sSubbedItemID,
			String sOrderLineTranQuantity, String sStatusQty, String sSubbedItemPriceType, String sFulfillmentType)
					throws GCException {
		try {
			final String logInfo = "GCItemSubstitution :: callChangeOrderForItemSubstitution :: ";
			LOGGER.verbose(logInfo + "Begin");

			Document docChangeOrderIp = GCXMLUtil.createDocument(GCConstants.ORDER);
			Element eleRootIp = docChangeOrderIp.getDocumentElement();
			eleRootIp.setAttribute(GCConstants.ORDER_HEADER_KEY, XPathAPI.selectSingleNode(ipDoc, "//@OrderHeaderKey")
					.getNodeValue());

			Document docGetOrderList =
					GCCommonUtil.invokeAPI(env, docChangeOrderIp, GCConstants.API_GET_ORDER_LIST,
							"/global/template/api/getOrderListForItemSubstitution.xml");
			String sOrderLineKey = XPathAPI.selectSingleNode(ipDoc, "//@OrderLineKey").getNodeValue();

			String sShippingCharge =
					GCXMLUtil.getAttributeFromXPath(docGetOrderList, "//OrderLines/OrderLine[@OrderLineKey='" + sOrderLineKey
							+ "']/LineCharges/LineCharge[@ChargeName='ShippingCharge']/@ChargeAmount");
			if (YFCCommon.isVoid(sShippingCharge)) {
				sShippingCharge = "0.00";
			}
			LOGGER.debug("sShippingCharge" + sShippingCharge);
			Element eleOrderLineWarr =
					GCXMLUtil.getElementByXPath(docGetOrderList, "//OrderLines/OrderLine[@DependentOnLineKey='" + sOrderLineKey
							+ "']");

			eleRootIp.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);

			Element eleOrderLinesIp = docChangeOrderIp.createElement(GCConstants.ORDER_LINES);
			eleRootIp.appendChild(eleOrderLinesIp);

			// Append original line to cancel
			Element eleOrderLineCancelIp = docChangeOrderIp.createElement(GCConstants.ORDER_LINE);
			eleOrderLinesIp.appendChild(eleOrderLineCancelIp);
			eleOrderLineCancelIp.setAttribute(GCConstants.ORDER_LINE_KEY, XPathAPI.selectSingleNode(ipDoc, "//@OrderLineKey")
					.getNodeValue());

			Element eleOrderLineTranQuantityIp = docChangeOrderIp.createElement(GCConstants.ORDER_LINE_TRAN_QUANTITY);
			eleOrderLineCancelIp.appendChild(eleOrderLineTranQuantityIp);
			eleOrderLineTranQuantityIp.setAttribute(GCConstants.ORDERED_QTY, sOrderLineTranQuantity);

			// Append associated warranty line to cancel
			if (!YFCCommon.isVoid(eleOrderLineWarr)) {
				Element eleWarrOrderLineCancelIp = docChangeOrderIp.createElement(GCConstants.ORDER_LINE);
				eleOrderLinesIp.appendChild(eleWarrOrderLineCancelIp);
				String sWarOrderLineKey = eleOrderLineWarr.getAttribute(GCConstants.ORDER_LINE_KEY);
				eleWarrOrderLineCancelIp.setAttribute(GCConstants.ORDER_LINE_KEY, sWarOrderLineKey);

				Element eleWarrOrderLineTranQuantityIp = docChangeOrderIp.createElement(GCConstants.ORDER_LINE_TRAN_QUANTITY);
				eleWarrOrderLineCancelIp.appendChild(eleWarrOrderLineTranQuantityIp);
				eleWarrOrderLineTranQuantityIp.setAttribute(GCConstants.ORDERED_QTY, sOrderLineTranQuantity);
			}

			// Append line for item substitution
			Element eleOrderLineSubIp = docChangeOrderIp.createElement(GCConstants.ORDER_LINE);
			eleOrderLinesIp.appendChild(eleOrderLineSubIp);
			eleOrderLineSubIp.setAttribute(GCConstants.ORDERED_QTY, sStatusQty);
			eleOrderLineSubIp.setAttribute(GCConstants.FULFILLMENT_TYPE, sFulfillmentType);
			Element eleLineCharges = docChangeOrderIp.createElement(GCConstants.LINE_CHARGES);
			Element eleLineCharge = docChangeOrderIp.createElement(GCConstants.LINE_CHARGE);

			if (!YFCCommon.isVoid(sShippingCharge)) {
				eleLineCharge.setAttribute(GCConstants.CHARGE_CATEGORY, GCConstants.SHIPPING);
				eleLineCharge.setAttribute(GCConstants.CHARGE_NAME, GCConstants.SHIPPING_CHARGE);
				eleLineCharge.setAttribute(GCConstants.CHARGE_PER_LINE, sShippingCharge);
				eleLineCharges.appendChild(eleLineCharge);
				eleOrderLineSubIp.appendChild(eleLineCharges);
			}

			Element eleItemIp = docChangeOrderIp.createElement(GCConstants.ITEM);
			eleOrderLineSubIp.appendChild(eleItemIp);
			eleItemIp.setAttribute(GCConstants.ITEM_ID, sSubbedItemID);
			eleItemIp.setAttribute(GCConstants.UNIT_OF_MEASURE, XPathAPI.selectSingleNode(ipDoc, "//@UnitOfMeasure")
					.getNodeValue());
			eleItemIp.setAttribute(GCConstants.PRODUCT_CLASS, getProductClass());
			eleItemIp.setAttribute(GCConstants.ITEM_SHORT_DESCRIPTION,
					XPathAPI.selectSingleNode(docGetCompleteItemListOp, "//@ShortDescription").getNodeValue());

			Element eleLinePriceInfo = docChangeOrderIp.createElement(GCConstants.LINE_PRICE_INFO);
			eleOrderLineSubIp.appendChild(eleLinePriceInfo);
			eleLinePriceInfo.setAttribute(GCConstants.IS_PRICE_LOCKED, GCConstants.YES);
			eleLinePriceInfo.setAttribute(GCConstants.UNIT_PRICE, sSubbedItemPrice);

			if (GCConstants.INT_ONE.equals(sSubbedItemPriceType) || GCConstants.INT_TWO.equals(sSubbedItemPriceType)) {
				Element eleNotes = docChangeOrderIp.createElement(GCConstants.NOTES);
				eleOrderLineSubIp.appendChild(eleNotes);

				Element eleNote = docChangeOrderIp.createElement(GCConstants.NOTE);
				eleNotes.appendChild(eleNote);
				eleNote.setAttribute(GCConstants.NOTE_TEXT, "Price Overridden due to Item Substitutions");
				eleNote.setAttribute(GCConstants.PRIORITY, GCConstants.INT_ZERO);
				eleNote.setAttribute(GCConstants.REASON_CODE, GCConstants.INT_TWENTY_TWO);
				eleNote.setAttribute(GCConstants.VISIBLE_TO_ALL, GCConstants.YES);
			}
			LOGGER.debug(logInfo + "docChangeOrderIp\n" + GCXMLUtil.getXMLString(docChangeOrderIp));

			GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, docChangeOrderIp);

			LOGGER.verbose(logInfo + "End");
		} catch (Exception e) {
			LOGGER.error(" Inside catch of GCProcessItemSubstitution.callChangeOrderForItemSubstitution()", e);
			processFailedItemSubstituion();      
		}
	}

	/**
	 *
	 * @return
	 * @throws TransformerException
	 */
	private String getProductClass() throws GCException {
		try {
			String sProductClass = "";
			Element eleExtn = GCXMLUtil.getElementByXPath(docGetCompleteItemListOp, "ItemList/Item/Extn");

			if (!YFCCommon.isVoid(eleExtn.getAttribute("ExtnSetCode"))
					&& GCConstants.ONE.equals(eleExtn.getAttribute("ExtnSetCode"))) {
				sProductClass = GCConstants.SET;
			} else {
				sProductClass = GCConstants.REGULAR;
			}
			return sProductClass;
		} catch (Exception e) {
			LOGGER.error(" Inside catch of GCProcessItemSubstitution.getProductClass()", e);
			throw GCException.getYFCException(e.getMessage(), e);
		}
	}

	/**
	 * 
	 * @throws GCException
	 */
	private void processFailedItemSubstituion() throws GCException {
		try {
			LOGGER.error("Item substitution failed for OrderNo " + GCXMLUtil.getAttributeFromXPath(ipDoc, "//@OrderNo"));
			createException(GCConstants.ITEM_SUB_RESULT_MESSAGE, GCConstants.ITEM_SUB_EXCEPTION_TYPE,
					GCConstants.ITEM_SUB_QUEUE_ID);
		} catch (Exception e) {
			LOGGER.error(" Inside catch block of processFailedItemSubstituion(), Exception is :", e);
			throw new GCException(e);
		}
	}

	/**
	 * 
	 * @param sResultMessage
	 * @param sExceptionType
	 * @param sQueueID
	 * @throws Exception
	 */
	private void createException(String sResultMessage, String sExceptionType, String sQueueID) throws GCException {
		try {
			final String logInfo = "GCItemSubstitution :: createException :: ";
			LOGGER.verbose(logInfo + "Begin");

			LOGGER.debug(logInfo + "sResultMessage :: " + sResultMessage);
			LOGGER.debug(logInfo + "sExceptionType :: " + sExceptionType);
			LOGGER.debug(logInfo + "SQueueID :: " + sQueueID);

			Document docCreateExceptionIp = GCXMLUtil.createDocument(GCConstants.INBOX);

			Element eleRoot = docCreateExceptionIp.getDocumentElement();

			eleRoot.setAttribute(GCConstants.EXCEPTION_TYPE, sExceptionType);
			eleRoot.setAttribute(GCConstants.QUEUE_ID, sQueueID);

			Element eleInboxReferencesList = docCreateExceptionIp.createElement(GCConstants.INBOX_REFERENCE_LIST);
			eleRoot.appendChild(eleInboxReferencesList);

			Element eleInboxReferences = docCreateExceptionIp.createElement(GCConstants.INBOX_REFERENCES);
			eleInboxReferencesList.appendChild(eleInboxReferences);
			eleInboxReferences.setAttribute(GCConstants.NAME, GCConstants.RESULT_MESSAGE);
			eleInboxReferences.setAttribute(GCConstants.VALUE, sResultMessage);
			eleInboxReferences.setAttribute(GCConstants.REFERENCE_TYPE, GCConstants.TEXT);

			Element eleOrder = docCreateExceptionIp.createElement(GCConstants.ORDER);
			eleRoot.appendChild(eleOrder);
			eleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, XPathAPI.selectSingleNode(ipDoc, "//@OrderHeaderKey")
					.getNodeValue());

			LOGGER.debug(logInfo + "docCreateExceptionIp\n" + GCXMLUtil.getXMLString(docCreateExceptionIp));
			GCCommonUtil.invokeAPI(env, GCConstants.API_CREATE_EXCEPTION, docCreateExceptionIp);
		} catch (Exception e) {
			LOGGER.error(" Inside catch of GCProcessItemSubstitution.createException()", e);
			throw new GCException(e);
		}
	}

	/**
	 * 
	 * @param sSubbedItemID
	 * @throws GCException
	 */
	public void callGetCompleteItemList(String sSubbedItemID) throws GCException {
		try {
			final String logInfo = "GCMassOperationProcess :: callGetCompleteItemList :: ";
			LOGGER.verbose(logInfo + "Begin");

			// Get complete item list ip doc
			Document docGetCompleteItemListIp = GCXMLUtil.createDocument(GCConstants.ITEM);
			Element eleRootGetCompleteItemListIp = docGetCompleteItemListIp.getDocumentElement();
			eleRootGetCompleteItemListIp.setAttribute(GCConstants.CALLING_ORGANIZATION_CODE, GCConstants.GC);
			eleRootGetCompleteItemListIp.setAttribute(GCConstants.ITEM_ID, sSubbedItemID);
			// Get complete item list template doc

			Document docGetCompleteItemListTemp = GCXMLUtil.createDocument(GCConstants.ITEM_LIST);
			Element eleRoot = docGetCompleteItemListTemp.getDocumentElement();

			Element eleItem = docGetCompleteItemListTemp.createElement(GCConstants.ITEM);
			eleRoot.appendChild(eleItem);
			eleItem.setAttribute(GCConstants.ITEM_ID, "");
			eleItem.setAttribute(GCConstants.UNIT_OF_MEASURE, "");

			Element elePrimaryInformation = docGetCompleteItemListTemp.createElement(GCConstants.PRIMARY_INFORMATION);
			eleItem.appendChild(elePrimaryInformation);
			elePrimaryInformation.setAttribute(GCConstants.SHORT_DESCRIPTION, "");

			Element eleComputedPrice = docGetCompleteItemListTemp.createElement(GCConstants.COMPUTED_PRICE);
			eleItem.appendChild(eleComputedPrice);
			eleComputedPrice.setAttribute(GCConstants.LIST_PRICE, "");

			Element eleExtn = docGetCompleteItemListTemp.createElement(GCConstants.EXTN);
			eleItem.appendChild(eleExtn);

			docGetCompleteItemListOp =
					GCCommonUtil.invokeAPI(env, GCConstants.API_GET_COMPLETE_ITEM_LIST, docGetCompleteItemListIp,
							docGetCompleteItemListTemp);
			Element eleItemOp = GCXMLUtil.getElementByXPath(docGetCompleteItemListOp, "/ItemList/Item");
			if (YFCCommon.isVoid(eleItemOp)) {
				YFCException yfs = new YFCException(GCErrorConstants.INVALID_ITEM_ERROR_DESCRIPTION);
				yfs.setErrorDescription(GCErrorConstants.INVALID_ITEM_ERROR_DESCRIPTION);
				throw yfs;
			}

			LOGGER.verbose(logInfo + "End");
		} catch (Exception e) {
			LOGGER.error(" Inside catch of GCProcessItemSubstitution.callGetCompleteItemList()", e);
			throw GCException.getYFCException(e.getMessage(), e);
		}
	}

}
