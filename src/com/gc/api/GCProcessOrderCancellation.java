/**
 * Copyright � 2015, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This Java class is called from GCProcessOrderCancellation_svc, (Provice details)
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               *********         Infosys Ltd		    (Provide Description)
 *#################################################################################################################################################################
 */
package com.gc.api;

//import java.util.List;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
//import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.gc.userexit.GCBeforeChangeOrderUE;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;


//import javax.xml.transform.TransformerException;
//import org.apache.commons.lang.StringUtils;
import org.apache.xpath.XPathAPI;

/**
 *@author Infosys Limited
 */

public class GCProcessOrderCancellation implements YIFCustomApi{

	private static final String CLASSNAME = GCProcessOrderCancellation.class.getName();

	private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCProcessOrderCancellation.class);

	/**This method gets called from GCProcessOrderCancellation_svc. Method is used to process the Order cancellation by finding if the line selected for cancellation is chained to PO, if yes move PO status to cancel requested,else call changeOrder to cancel the order
	 *
	 * @param inDoc
	 *            Order Cancellation Input
	 *@param env
	 *            The YFS Environment reference
	 * @throws GCException 
	 */
	public Document processOrderCancellation(YFSEnvironment env, final Document inDoc) throws GCException{

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASSNAME + ":processOrderCancellation:Entry:input="
					+ GCXMLUtil.getXMLString(inDoc));
		}

		Document docChangeOrderOp=null;
		Document docPurchaseOrderOp=null;

		try{

			//Configure a service to prepare getOrderList Input using XSL

			final Document docGetOrdLstOp = GCCommonUtil.invokeService(env, "GCGetOrderList_svc", inDoc);//service with xsl component API component 
			
			  //GCSTORE-5725 - begin
			stampCancellationNotes(env, inDoc);
			  //GCSTORE-5725 - end
			
			LOGGER.debug(":getOrderList Output::"+ GCXMLUtil.getXMLString(docGetOrdLstOp));

			final Element eleRoot=docGetOrdLstOp.getDocumentElement();

			final String sTotalNumberOfRecords=eleRoot.getAttribute(GCConstants.TOTAL_NO_OF_RECORDS);

			//check if there are any PO's chained to the order 

			if (!GCConstants.ZERO.equalsIgnoreCase(sTotalNumberOfRecords)) {
				//Prepare changeOrderStatus Input 

				final Document docChangeOrderStatusIp = prepareChangeOrderStatusIp(env,inDoc,docGetOrdLstOp);
				//Invoke changeOrderStatus API for PO orderlines which is chained to SO and selected for cancellation

				//Document docChangeOrderStatusOp = GCCommonUtil.invokeService(env, "GCChangeOrderStatus_svc", docChangeOrderStatusIp);	

				//Invoke changeOrder API excluding the order lines,which is linked to PO
				LOGGER.debug("indoc after PO removal:Output="+ GCXMLUtil.getXMLString(inDoc));
				NodeList nlOrderLine=XPathAPI.selectNodeList(inDoc,"Order/OrderLines/OrderLine");
                LOGGER.debug("nlOrderLine value="+ nlOrderLine);
				if(nlOrderLine.getLength()> 0){
					LOGGER.debug("indoc after PO removal:Output="+ GCXMLUtil.getXMLString(inDoc));
					docChangeOrderOp = GCCommonUtil.invokeService(env, "GCChangeOrder_svc", inDoc);		
					if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(":processOrderCancellation:Entry:Output="+ GCXMLUtil.getXMLString(docChangeOrderOp));
		}
				}
				/*else{
					//prepare output document by invoking a service which transforms the changeOrder status output similar to the changeOrder api output
					docChangeOrderOp = GCCommonUtil.invokeService(env, "GCPrepareChangeOrderOp_svc", docChangeOrderStatusOp);
				}*/

			}

			//If there are no PO's linked with a SO, call change Order (similar to OOB)
			else{
			    String sExtnIsNonDependentWarranty = eleRoot.getAttribute("ExtnIsNonDependentWarranty");
			    //GCSTORE-5985
			    if(!YFCCommon.isVoid(sExtnIsNonDependentWarranty)){
			        GCBeforeChangeOrderUE objChangeOrderUE=new GCBeforeChangeOrderUE();
			        objChangeOrderUE.applyAssociationHold(env, inDoc,sExtnIsNonDependentWarranty);
			    }
			    //GCSTORE-5985
				docChangeOrderOp =GCCommonUtil.invokeService(env, "GCChangeOrder_svc", inDoc);
				if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(":processOrderCancellation:Entry:Output="+ GCXMLUtil.getXMLString(docChangeOrderOp));
		}
			}
		}
		catch (Exception e) {

			LOGGER.error("The exception is in processOrderCancellation method:", e);
			throw new GCException(e);
		}	

		if (!YFCCommon.isVoid(docChangeOrderOp)){
			LOGGER.debug(":returning docChangeOrderOp="+ GCXMLUtil.getXMLString(docChangeOrderOp));
			return docChangeOrderOp;
		}
		else{
			docPurchaseOrderOp = GCCommonUtil.invokeService(env, "GCGetOrderListPO_svc", inDoc);
			LOGGER.debug(":returning docPurchaseOrderOp="+ GCXMLUtil.getXMLString(docPurchaseOrderOp));
			return docPurchaseOrderOp;
		}
		
	}



	 /**
	   * 
	   * @param env
	   * @param inDoc
	   * @throws TransformerException
	   */
	   private void stampCancellationNotes(YFSEnvironment env, Document inDoc) throws TransformerException {
	 	
	 	  LOGGER.debug("GCBeforeChangeOrderUE :: stampCancellationNotes :: Begin");
	 	  String sModificationReasonCode = inDoc.getDocumentElement().getAttribute("ModificationReasonCode");
	 	  String sNoteText = "";
	 	  if(!YFCCommon.isVoid(sModificationReasonCode)){
	 		  
	 		  Document docGetCommonCodeListIp = GCXMLUtil.createDocument(GCConstants.COMMON_CODE);
	 		  Element eleRoot = docGetCommonCodeListIp.getDocumentElement();
	 		  eleRoot.setAttribute(GCConstants.CODE_VALUE, sModificationReasonCode);
	 		  eleRoot.setAttribute(GCConstants.CODE_TYPE, "YCD_CANCEL_REASON");
	 		  LOGGER.debug("GCBeforeChangeOrderUE :: stampCancellationNotes :: docGetCommonCodeListIp\n"
	 			        + GCXMLUtil.getXMLString(docGetCommonCodeListIp));
	 		  
	 		  Document docGetCommonCodeListOp = GCCommonUtil.invokeAPI(env, GCConstants.GET_COMMON_CODE_LIST, docGetCommonCodeListIp);
	 		  LOGGER.debug("GCBeforeChangeOrderUE :: stampCancellationNotes :: docGetCommonCodeListOp\n"
	 			        + GCXMLUtil.getXMLString(docGetCommonCodeListOp));
	 		 
	 		  Element eleCommonCode = (Element) docGetCommonCodeListOp.getDocumentElement().getElementsByTagName(GCConstants.COMMON_CODE).item(0);
	 		  if(!YFCCommon.isVoid(eleCommonCode)){
	 			  String sShortDesc = eleCommonCode.getAttribute(GCConstants.CODE_SHORT_DESCRIPTION);
	 			  Calendar cal = Calendar.getInstance();
	 			  DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");	

	 			  sNoteText = "CancellationReasonCode: Order cancelled due to "+sShortDesc+" on "+ dateFormat.format(cal.getTime());
	 			  LOGGER.debug("GCBeforeChangeOrderUE :: stampCancellationNotes :: Cancellation note text :: "+ sNoteText);
	 		  }		  
	 		  
	 		  Element eleNotes = (Element) XPathAPI.selectSingleNode(inDoc, "//Order/Notes");
	 		  if(YFCCommon.isVoid(eleNotes)){
	 			  eleNotes = inDoc.createElement(GCConstants.NOTES);
	 			  inDoc.getDocumentElement().appendChild(eleNotes);
	 		  }
	 		  
	 		  Element eleNote = inDoc.createElement(GCConstants.NOTE);
	 		  eleNotes.appendChild(eleNote);
	 		  eleNote.setAttribute(GCConstants.NOTE_TEXT, sNoteText);
	 		  
	 		 
	 	  }	
	 	  LOGGER.debug("GCBeforeChangeOrderUE :: stampCancellationNotes :: End");
	   }



	/**
	 * <h3>Description :</h3> This method is used to prepare input XML for GCChangeOrderStatus_svc

	 * @param inDoc
	 *            Order Cancellation Input
	 * @return The document is the input XML to GCChangeOrderStatus_svc 
	 * @throws GCException 
	 */
	public Document prepareChangeOrderStatusIp(final YFSEnvironment env,final Document inDoc,final Document docGetOrdLstOp ) throws GCException {

		// logging the method entry
		if( LOGGER.isDebugEnabled()){
			LOGGER.debug( CLASSNAME+ ":prepareChangeOrderStatusIp:Entry");
		}
		Document docChangeOrderStatusIp =null;
		Document docCreateExceptionOp =null;
		Document docChangeOrderStatusOp =null;
		try{
			Element  eleCancelOrderLines=GCXMLUtil.getElementByXPath(inDoc,"Order/OrderLines");
			NodeList nlOrderLine=XPathAPI.selectNodeList(inDoc,"Order/OrderLines/OrderLine");
			int iOrderLineCount = nlOrderLine.getLength();//count of orderlines selected for cancellation

			//Prepare changeOrderStatus Input for 	
			docChangeOrderStatusIp = GCXMLUtil.createDocument(GCConstants.ORDER_STATUS_CHANGE);
			Element eleChangeOrderStatus = docChangeOrderStatusIp.getDocumentElement();
			Element eleOrderLines = GCXMLUtil.createElement(docChangeOrderStatusIp, GCConstants.ORDER_LINES, null);
			eleChangeOrderStatus.appendChild(eleOrderLines);
			eleChangeOrderStatus.setAttribute(GCConstants.ENTERPRISE_CODE, GCConstants.GC);
			eleChangeOrderStatus.setAttribute(GCConstants.TRANSACTION_ID,GCConstants.CHANGE_ORDER_STATUS_PO);
			eleChangeOrderStatus.setAttribute(GCConstants.DOCUMENT_TYPE,GCConstants.PO_DOCUMENT_TYPE);


			Element elOrdLine = GCXMLUtil.createElement(docChangeOrderStatusIp, "OrderLine", null);

			String strPOOrderLineKey=null;

			String strPOOrderHeaderKey=null;

			String strPOOrderNo=null;

                 LOGGER.debug("iOrderLineCount is::"+ iOrderLineCount);    
			//Loop through each orderline selected for cancellation

			for (int iOrderLineCounter = 0; iOrderLineCounter < iOrderLineCount; iOrderLineCounter++) {

				Element eleSOOrderLine = (Element) nlOrderLine.item(iOrderLineCounter);

				String strOrderLineKey=eleSOOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
				LOGGER.debug("strOrderLineKey is::"+ strOrderLineKey);

				String strCancelQty=eleSOOrderLine.getAttribute(GCConstants.QUANTITY_TO_CANCEL);
				//Element elePOOrderLine =GCXMLUtil.getElementByXPath(docGetOrdLstOp, "/OrderList/Order/OrderLines/OrderLine[@ChainedFromOrderLineKey =" + strOrderLineKey + "]");

				NodeList nlOrder=GCXMLUtil.getNodeListByXpath(docGetOrdLstOp, "OrderList/Order");

				int iOrderCount = nlOrder.getLength();
				

				for (int iOrderCounter = 0; iOrderCounter < iOrderCount; iOrderCounter++) {

					Element eleOrder =  (Element) nlOrder.item(iOrderCounter);

					Document docOrder=GCXMLUtil.getDocumentFromElement(eleOrder);
					
					LOGGER.debug("docOrder is::"+ GCXMLUtil.getXMLString(docOrder));

					//check if the line selected for cancellation has any associated PO
					Element elePOOrderLine =GCXMLUtil.getElementByXPath(docOrder, "Order/OrderLines/OrderLine[@ChainedFromOrderLineKey ='" + strOrderLineKey + "']");
                             
					//Prepare changeOrderStatus API for the PO
					if (!YFCCommon.isVoid(elePOOrderLine)) {
						strPOOrderLineKey=elePOOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
						strPOOrderHeaderKey=elePOOrderLine.getAttribute(GCConstants.ORDER_HEADER_KEY);
						eleChangeOrderStatus.setAttribute(GCConstants.ORDER_HEADER_KEY,strPOOrderHeaderKey);
						elOrdLine.setAttribute(GCConstants.BASE_DROP_STATUS, GCConstants.CANCEL_REQUESTED_STATUS);
						elOrdLine.setAttribute(GCConstants.QUANTITY,strCancelQty);
						elOrdLine.setAttribute(GCConstants.ORDER_LINE_KEY,strPOOrderLineKey);
						eleOrderLines.appendChild(elOrdLine);
						// logging the input to changeOrderStatus API
						if( LOGGER.isDebugEnabled()){
							LOGGER.debug("The input XML prepared for changeOrderStatus API"+ GCXMLUtil.getXMLString(docChangeOrderStatusIp));
						}

                        docChangeOrderStatusOp= GCCommonUtil.invokeService(env, "GCChangeOrderStatus_svc", docChangeOrderStatusIp);//service with API component
						LOGGER.debug("ChangeOrderStatusOutput::"+ GCXMLUtil.getXMLString(docChangeOrderStatusOp));

						docCreateExceptionOp= GCCommonUtil.invokeService(env, "GCRaisePOCancelRequestAlert_svc", docChangeOrderStatusIp);//service with xsl component, defaulting component, api component
						
						LOGGER.debug("createExceptionOp::" + GCXMLUtil.getXMLString(docCreateExceptionOp));

						//Line selected for cancellation has PO- Hence remove the line from changeOrder API call
						eleCancelOrderLines.removeChild(eleSOOrderLine);
					}



				}
			}

		}

		catch (Exception e) {

			LOGGER.error("The exception is in prepareChangeOrderStatusIp method:", e);
			throw new GCException(e);
		}
		// logging the method exit
		if( LOGGER.isDebugEnabled()){
			LOGGER.debug(CLASSNAME+":prepareChangeOrderStatusIp:Exit");
		}
		// returning the input document prepared
		return docChangeOrderStatusIp;
	}


	@Override
	public void setProperties(Properties arg0) throws Exception {
		// TODO Auto-generated method stub

	}
}










