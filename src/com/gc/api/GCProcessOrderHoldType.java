/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Processes the holds present on order
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               19/02/2014        Singh, Gurpreet            OMS-1144: Fraud
             2.0               28/05/2015        Singh, Gurpreet            GCSTORE-2514: Line Level Taxes (CR-022)
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.util.Properties;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="gurpreet.singh@expicient.com">Gurpreet</a>
 */
public class GCProcessOrderHoldType implements YIFCustomApi {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCProcessOrderHoldType.class);

    /**
     * 
     * Description of processOrderHoldType Identifies the hold present on order
     * and call the respective service to process that hold
     * 
     * @param env
     * @param inDoc
     * @return
     * @throws GCException
     * 
     */
    public Document processOrderHoldType(YFSEnvironment env, Document inDoc)
	    throws GCException {
	LOGGER.debug(" Entering GCProcessOrderHoldType.processOrderHoldType() method ");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" processOrderHoldType() method, Input document is"
		    + GCXMLUtil.getXMLString(inDoc));
	}
	try {
	    String sOrderHeaderKey = inDoc.getDocumentElement().getAttribute(
		    GCConstants.ORDER_HEADER_KEY);
	    String sPaymentStatus = inDoc.getDocumentElement().getAttribute(
		    GCConstants.PAYMENT_STATUS);

	    // Fraud Hold
	    Element eleFraudOrderHoldType = (Element) XPathAPI
		    .selectSingleNode(inDoc,
			    "//OrderHoldType[@HoldType='FRAUD_HOLD'"
				    + " and @Status='1100']");
	    Document docGetOrderList = GCXMLUtil
		    .getDocument("<Order OrderHeaderKey='" + sOrderHeaderKey
			    + "' />");
	    Document docGetOrderListTmp = GCXMLUtil
		    .getDocument("<OrderList><Order OrderNo=''>"
			    + "<Extn ExtnSentForFraudCheck='' /></Order></OrderList>");
	    Document docGetOrderListOP = GCCommonUtil.invokeAPI(env,
		    GCConstants.API_GET_ORDER_LIST, docGetOrderList,
		    docGetOrderListTmp);
	    String sExtnSentForFraudCheck = GCXMLUtil.getAttributeFromXPath(
		    docGetOrderListOP,
		    "OrderList/Order/Extn/@ExtnSentForFraudCheck");

	    // Check if FraudHold is present and is in created status and
	    // PaymentStatus is Authorized, Send Fraud Request and
	    // Apply Fraud Review hold
	    if (!YFCCommon.isVoid(eleFraudOrderHoldType)
		    && GCConstants.AUTHORIZED.equalsIgnoreCase(sPaymentStatus)
		    && !GCConstants.YES
			    .equalsIgnoreCase(sExtnSentForFraudCheck)) {
		LOGGER.debug("Fraud hold request sent, Inside if (!YFCCommon.isVoid(eleFraudOrderHoldType)");

		Document docRequest = GCXMLUtil
			.createDocument(GCConstants.ORDER);
		docRequest.getDocumentElement().setAttribute(
			GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
		GCCommonUtil.invokeService(env,
			GCConstants.GC_SEND_FRAUD_REQUEST_SERVICE, docRequest);
		setFraudRequestSentFlag(env, sOrderHeaderKey);
	    }

	    // GCSTORE-2514 : Start
	    Element eleOrderHoldType = (Element) XPathAPI.selectSingleNode(
		    inDoc, "//OrderHoldType[(@HoldType='BUYER_REMORSE_HOLD'"
			    + " and @Status='1100') or (@HoldType='VERTEX_CALL_FAILED'"
                + " and @Status='1100')]");
	    // GCSTORE-2514 : End
	    // Check if BuyerRemorseHold, VertexCallFailed is present and and is in created status
	    if (!YFCCommon.isVoid(eleOrderHoldType)) {
		LOGGER.debug(" Inside if (!YFCCommon.isVoid(eleOrderHoldType))");

		GCCommonUtil
			.invokeService(
				env,
				GCConstants.GC_VALIDATE_AND_RESOLVE_HOLDS_WITH_PERIOD_SERVICE,
				inDoc);
	    }
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCProcessOrderHoldType.processOrderHoldType()",
		    e);
	    throw new GCException(e);
	}
	LOGGER.debug(" Exiting GCProcessOrderHoldType.processOrderHoldType() method ");
	return inDoc;
    }

    private void setFraudRequestSentFlag(YFSEnvironment env,
	    String sOrderHeaderKey) throws GCException {
	LOGGER.debug(" Entering GCProcessOrderHoldType.setFraudRequestSentFlag() method ");
	try {
	    // Create changeOrderDoc
	    Document docChangeOrderInput = GCXMLUtil
		    .createDocument(GCConstants.ORDER);
	    Element eleChangeOrderRootElement = docChangeOrderInput
		    .getDocumentElement();
	    eleChangeOrderRootElement.setAttribute(
		    GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
	    eleChangeOrderRootElement.setAttribute(GCConstants.OVERRIDE,
		    GCConstants.YES);
	    Element eleExtn = docChangeOrderInput
		    .createElement(GCConstants.EXTN);
	    eleExtn.setAttribute(GCConstants.EXTN_SENT_FOR_FRAUD_CHECK,
		    GCConstants.YES);
	    eleChangeOrderRootElement.appendChild(eleExtn);

	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug(" setFraudRequestSentFlag() method, docChangeOrderInput is"
			+ GCXMLUtil.getXMLString(docChangeOrderInput));
	    }
	    GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER,
		    docChangeOrderInput, null);
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCProcessOrderHoldType.setFraudRequestSentFlag()",
		    e);
	    throw new GCException(e);
	}
	LOGGER.debug(" Exiting GCProcessOrderHoldType.setFraudRequestSentFlag() method ");
    }

    @Override
    public void setProperties(Properties arg0) throws GCException {

    }

}
