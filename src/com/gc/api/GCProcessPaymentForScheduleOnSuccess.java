/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 * *###########################################
 * ######################################################
 * ################################################################ OBJECTIVE: Contains the logic
 * re processing the payments for status change.
 * ##################################
 * ################################################################
 * ############################################################### Version Date Modified By
 * Description
 * ######################################################################################
 * ########################################################################### 1.0 09/06/2015Zuzar Inder Singh,
 * #########################################
 * #########################################################
 * ###############################################################
 */
package com.gc.api;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCProcessPaymentForScheduleOnSuccess {
  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCProcessBackordersAPI.class);

  public Document processOrderPayments(YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("Entering GCProcessPaymentForScheduleOnSuccessV2.processOrderPayments()");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input to requestCollection API for the first time:"
          + inDoc);
    }
    GCCommonUtil.invokeAPI(env, "requestCollection", YFCDocument.getDocumentFor(inDoc),
        YFCDocument.createDocument("Order"));
    // Document for execute collection
    YFCDocument docExecuteCollection = YFCDocument
        .createDocument("ExecuteCollection");
    docExecuteCollection.getDocumentElement().setAttribute(
        GCConstants.ORDER_HEADER_KEY,
        inDoc.getDocumentElement().getAttribute(
            GCConstants.ORDER_HEADER_KEY));
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input to executeCollection API"
          + docExecuteCollection);
    }
    GCCommonUtil.invokeAPI(env, "executeCollection", docExecuteCollection,
        YFCDocument.createDocument("ExecuteCollection"));
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input to requestCollection API for the second time"
          + inDoc);
    }
    GCCommonUtil.invokeAPI(env, "requestCollection", YFCDocument.getDocumentFor(inDoc),
        YFCDocument.createDocument("Order"));
    LOGGER.endTimer("Exiting GCProcessPaymentForScheduleOnSuccessV2.processOrderPayments()");
    return inDoc;
  }

  public Document doNothing(YFSEnvironment env, Document inDoc) {
    return inDoc;
  }
}