/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 * *###########################################
 * ######################################################
 * ################################################################ OBJECTIVE: Contains the logic
 * re processing the payments for status change.
 * ##################################
 * ################################################################
 * ############################################################### Version Date Modified By
 * Description
 * ######################################################################################
 * ########################################################################### 1.0 09/06/2015Zuzar Inder Singh,
 * #########################################
 * #########################################################
 * ###############################################################
 */
package com.gc.api;

import com.gc.common.utils.GCCommonUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCProcessPaymentForScheduleOnSuccessOld {
	// initialize logger
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCProcessBackordersAPI.class);

	public void processOrderPayments(YFSEnvironment env, YFCDocument inDoc) {
		LOGGER.beginTimer("Entering GCProcessPaymentForScheduleOnSuccess.processOrderPayments()");
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Input to processOrderPayments" + inDoc.toString());
		}
		GCCommonUtil.invokeAPI(env, "processOrderPayments", inDoc,
				YFCDocument.createDocument("Order"));
		LOGGER.endTimer("Exiting GCProcessPaymentForScheduleOnSuccess.processOrderPayments()");
	}
}