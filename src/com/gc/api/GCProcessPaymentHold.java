/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               02/04/2014        Soni, Karan		This class is used in case of Prepaid payment tyepe to update processed amt.
 *#################################################################################################################################################################
 */
package com.gc.api;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCPaymentUtils;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author soni, karan
 */
public class GCProcessPaymentHold {

    // initiaize Logger
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCProcessPaymentHold.class);
     /**
     * This method updates the processed amt for payment type prepaid.
     * @param env
     * @param inDoc
     * @throws Exception
     */
    public void capturePaymentPrepaidType(YFSEnvironment env,
	    Document inDoc) throws GCException {
	try {
	    LOGGER.verbose("Entering the capturePaymentPrepaidPT method");
	    if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug("The indoc is-----> " + GCXMLUtil.getXMLString(inDoc));
	    }
	    Element eleOrderHoldType = inDoc.getDocumentElement();
	    String strHoldType = eleOrderHoldType
		    .getAttribute(GCConstants.HOLD_TYPE);
	    // Status will be used in the case when hold is resolved it's value
	    // will be 1100
	    String strFromStatus = eleOrderHoldType
		    .getAttribute(GCConstants.FROM_STATUS);
	    String strSellerOrgCode = GCXMLUtil.getAttributeFromXPath(
		    eleOrderHoldType, "Order/@SellerOrganizationCode");
	    String strEnterpriseCode = GCXMLUtil.getAttributeFromXPath(
		    eleOrderHoldType, "Order/@EnterpriseCode");
	    String strOrderHeaderKey = GCXMLUtil.getAttributeFromXPath(
		    eleOrderHoldType, "Order/@OrderHeaderKey");
	    Document docGetPaymentTypeList = GCPaymentUtils
		  .invokeGetPaymentTypeList(env, strSellerOrgCode);
	    if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug("The GetPaymentTypeList output document"
		    + GCXMLUtil.getXMLString(docGetPaymentTypeList));
	    }
	    if (GCConstants.PAYMENT_HOLD.equals(strHoldType)
		    && strFromStatus.equals(GCConstants.STATUS_CREATED)) {
		LOGGER.verbose("Entering If");
		// This nodelist will have that list that are authorized and are
		// in status checked
		NodeList nlChargeTransactionDetail = GCXMLUtil
			.getNodeListByXpath(
				inDoc,
				"OrderHoldType/Order/ChargeTransactionDetails/ChargeTransactionDetail[@ChargeType='AUTHORIZATION' and @Status='CHECKED']");
		for (int i = 0; i < nlChargeTransactionDetail.getLength(); i++) {
		    Element eleChargeTransactionDetail = (Element) nlChargeTransactionDetail
			    .item(i);
		    String strPaymentType = GCXMLUtil.getAttributeFromXPath(
			    eleChargeTransactionDetail,
			    "PaymentMethod/@PaymentType");
		    String strPaymentKey = GCXMLUtil.getAttributeFromXPath(
			    eleChargeTransactionDetail,
			    "PaymentMethod/@PaymentKey");
		    String sSuspendAnyMoreCharges= GCXMLUtil.getAttributeFromXPath(
		    	eleChargeTransactionDetail,
		    	"PaymentMethod/@SuspendAnyMoreCharges");
		    LOGGER.debug("The Charge Transaction Payment TYpe"
			    + strPaymentType);
		   Element elePaymentType = GCXMLUtil.getElementByXPath(
			    docGetPaymentTypeList,
			   "PaymentTypeList/PaymentType[@PaymentType='"
				    + strPaymentType + "']");
		    // This element filter that the prepaid payment type that is
		    // charged earlier will not be picked, if this element
		    // exists that mean this payment type is already charged
		    Element eleChargeExist = GCXMLUtil
			    .getElementByXPath(
				    inDoc,
				    "OrderHoldType/Order/ChargeTransactionDetails/ChargeTransactionDetail[@ChargeType='CHARGE' and @Status='CHECKED']/PaymentMethod[@PaymentType='"
					    + strPaymentType + "']");
		    if (!YFCCommon.isVoid(elePaymentType)
			    && YFCCommon.isVoid(eleChargeExist) && YFCCommon.equals(sSuspendAnyMoreCharges, "N")) {
			String strRequestAmt = eleChargeTransactionDetail
				.getAttribute(GCConstants.PAYMENT_REQUEST_AMOUNT);
			Document inDocument = GCXMLUtil
				.getDocument("<RecordExternalCharges DocumentType='"
					+ GCConstants.SALES_ORDER_DOCUMENT_TYPE
					+ "' EnterpriseCode='"
					+ strEnterpriseCode
					+ "' OrderHeaderKey='"
					+ strOrderHeaderKey
					+ "' ><PaymentMethod  PaymentKey='"
					+ strPaymentKey
					+ "' PaymentReference1=''  PaymentType='"
					+ strPaymentType
					+ "'><PaymentDetailsList>"
					+"<PaymentDetails ChargeType='AUTHORIZATION' RequestAmount='-"+ strRequestAmt+"' />"
					+ "<PaymentDetails AuthCode='CHARGE_SUCCESS' AuthReturnCode='0' ChargeType='CHARGE' ProcessedAmount='"
					+ strRequestAmt
					+ "' /></PaymentDetailsList></PaymentMethod></RecordExternalCharges>");
			if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("The input doc for recordCollection"
				+ GCXMLUtil.getXMLString(inDocument));
			}
			GCCommonUtil
				.invokeAPI(env,
					GCConstants.RECORD_EXTERNAL_CHARGES,
					inDocument);
		    }
		}
	    }
	    LOGGER.verbose("Exiting the capturePaymentPrepaidPT method");
	} catch (Exception e) {
	  LOGGER.error("Inside GCProcessPaymentHold.capturePaymentPrepaidType():",e);
	    throw new GCException(e);
	}
    }
}
