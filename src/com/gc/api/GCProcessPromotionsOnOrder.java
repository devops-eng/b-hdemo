/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               06/02/2014        Last Name, First Name		JIRA No: Description of issue.
 *#################################################################################################################################################################
 */
package com.gc.api;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:email Address">Name</a>
 */
public class GCProcessPromotionsOnOrder {
  private static final Logger LOGGER = Logger
  .getLogger(GCProcessPromotionsOnOrder.class.getName());

  public void processPromotions(YFSEnvironment env, Document inDoc)
  throws GCException {

    try {
      String sIsATGCallRequired = "";
      Element eleExtn = GCXMLUtil
      .getElementByXPath(inDoc, "//Order/Extn");
      if (!YFCCommon.isVoid(eleExtn)) {
        sIsATGCallRequired = eleExtn
        .getAttribute(GCConstants.EXTN_IS_ATG_CALL_REQUIRED);
      }
      if (GCConstants.FLAG_Y.equalsIgnoreCase(sIsATGCallRequired)) {
        NodeList nlOrderLine = GCXMLUtil.getNodeListByXpath(inDoc, "//Order/OrderLines/OrderLine");
        if(nlOrderLine.getLength()>0) {
          LOGGER.debug("Interacting with ATG after changeOrder");
          GCCommonUtil.invokeService(env, GCConstants.GC_GET_ATG_PROMO_SERVICE, inDoc);
        }
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCProcessPromotionsOnOrder.processPromotions(), Exception is :",
          e);
      throw GCException.getYFCException(e.getMessage(), e);
    }
  }
}