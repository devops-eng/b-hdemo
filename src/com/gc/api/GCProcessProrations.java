/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *##########################################################################################################################################################################################
 *	        OBJECTIVE: Prorates Header Charges to Line and Bundle Charges to Components
 *#############################################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#############################################################################################################################################################################################
             1.0               08/08/2014        Singh, Gurpreet		    Proration Logic
             2.0			   11/08/2014		 Soni, Karan				Logic to prorate charges on Invoice and Release
             2.3               12/18/2014        Soni, Karan                OMS-4341: Proration Logic of Prices for SIKO component item does not consider promotion applied on the Order.
             2.4               16/01/2014        Soni, Karan                OMS-4637: Prorated price on a kit component inaccurate (floating point division issue)
             2.5               30/01/2015        Kumar,Rakesh               OMS-4759: OMS Outbound kit components prices are incorrect when more than one kit is on the sales order.
             2.6               31/01/2015        Soni, Karan                OMS-4743: ShippingCharge element added to order message for kit component in error.
             2.7               16/02/2015        Kumar,Rakesh               OMS-4881: Unable to cancel a return order created for an order containing a kit item.
			 2.8			   16/03/2015		 Infosys Ltd.				OMS-5010: OMS is adding extra shipping charge when warranty is getting added post order creation
			 2.9               31/03/2015        Khakha,Johnson             OMS-5164: Unable to prorate the Tax and Reference_2 for KIT item to its component.
			                                                                          Stamping the appropriate Tax at InvoiceDetail/InvoiceHeader/LineDetails/LineDetail/@Tax
			 3.0			   13/04/2015		 Infosys Ltd.				OMS-5379: Only 2 of 3 invoices for the order got to TIBCO from OMS
			 3.1			   04/05/2015		 Infosys Ltd.				OMS-5219/5533: Tax Proration Error
			 3.2			   11/05/2015		 Infosys Ltd.				OMS-5467: G/L posting for Returns posted as credit, but should have been a debit
			 3.3			   11/05/2015		 Infosys Ltd.				OMS-5584: Web Order Creation Failure with Kit Item having Shipping Charges as 0
			 3.4			   04/11/2015 		 Infosys Ltd.				GCSTORE-5189: No transactionID from the original PayPal order from OMS in ReturnNotifyInvoiceSyncflow.
*#############################################################################################################################################################################################
 */
package com.gc.api;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;


/**
 * @author <a href="mailto:gurpreet.singh@expicient.com">Gurpreet</a>
 */
public class GCProcessProrations implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCProcessProrations.class);

  private static final String CLASS_NAME=GCProcessProrations.class.getName();
  private Properties properties;

  public void prorateHeaderCharges(YFSEnvironment env, Document inDoc)
      throws GCException {
    LOGGER.debug(" Entering GCProcessProrations.prorateHeaderCharges() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" prorateHeaderCharges() method, Input document is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    try {
      String sTotalBillableWeight = calculateTotalBillableWeight(env,
          inDoc);
      double dTotalBillableWeight = (!YFCCommon
          .isVoid(sTotalBillableWeight) ? Double
              .valueOf(sTotalBillableWeight) : 0.00);

      String sTotalQty = caclulateTotalQuantities(inDoc);
      double dTotalQty = (!YFCCommon.isVoid(sTotalQty) ? Double
          .valueOf(sTotalQty) : 0.00);

      Map<String, BigDecimal> chargeMap = new HashMap<String, BigDecimal>();
      Map<String, Double> taxMap = new HashMap<String, Double>();
      NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc,
          "Order/OrderLines/OrderLine");
      for (int iCounter = 0; iCounter < nlOrderLine.getLength(); iCounter++) {
        LOGGER.debug(" Inside for loop on Order Lines");

        Element eleOrderLine = (Element) nlOrderLine.item(iCounter);
        String sExtnIsFreeGiftItem = GCXMLUtil.getAttributeFromXPath(
            eleOrderLine, "Extn/@ExtnIsFreeGiftItem");
        Element eleBundleParentLine = (Element) XPathAPI
            .selectSingleNode(eleOrderLine, "BundleParentLine");

        if (YFCCommon.isVoid(eleBundleParentLine)
            && !GCConstants.YES.equals(sExtnIsFreeGiftItem)) {
          LOGGER.debug(" Inside if YFCCommon.isVoid(eleBundleParentLine) && !GCConstants.YES.equals(sExtnIsFreeGiftItem)");

          String sItemID = GCXMLUtil.getAttributeFromXPath(
              eleOrderLine, "Item/@ItemID");
          String sUOM = GCXMLUtil.getAttributeFromXPath(eleOrderLine,
              "Item/@UnitOfMeasure");
          Document docGetItemListOP = GCCommonUtil.getItemList(env,
              sItemID, sUOM, GCConstants.GCI);
          String sItemBillableWeight = GCXMLUtil
              .getAttributeFromXPath(docGetItemListOP,
                  "ItemList/Item/Extn/@ExtnBillableWeight");
          double dItemBillableWeight = (!YFCCommon
              .isVoid(sItemBillableWeight) ? Double
                  .valueOf(sItemBillableWeight) : 0.00);
          String sKitCode = GCXMLUtil.getAttributeFromXPath(
              docGetItemListOP,
              "ItemList/Item/PrimaryInformation/@KitCode");
          // Fix 5010 - Start
          eleOrderLine.setAttribute(GCConstants.BILLABLE_WEIGHT, sItemBillableWeight);
          // Fix 5010 - End

          if (GCConstants.BUNDLE.equalsIgnoreCase(sKitCode)
              && dItemBillableWeight == 0.00) {
            // If billable weight of parent line is 0, then
            // calculate billable weight of components
            NodeList nlComponent = XPathAPI.selectNodeList(
                docGetItemListOP,
                "ItemList/Item/Components/Component");
            for (int iCount = 0; iCount < nlComponent.getLength(); iCount++) {
              Element eleComponent = (Element) nlComponent
                  .item(iCount);
              String sCompItemID = eleComponent
                  .getAttribute(GCConstants.COMPONENT_ITEM_ID);
              String sCompUOM = eleComponent
                  .getAttribute(GCConstants.COMPONENT_UOM);
              String sKitQuantity = GCXMLUtil
                  .getAttributeFromXPath(
                      docGetItemListOP,
                      "ItemList/Item/Components/"
                          + "Component[@ComponentItemID='"
                          + sCompItemID
                          + "']/@KitQuantity");
              double dKitQuantity = (!YFCCommon
                  .isVoid(sKitQuantity) ? Double
                      .valueOf(sKitQuantity) : 0.00);

              Document docGetCompItemListOP = GCCommonUtil
                  .getItemList(env, sCompItemID, sCompUOM,
                      GCConstants.GCI);
              String sCompBillableWeight = GCXMLUtil
                  .getAttributeFromXPath(
                      docGetCompItemListOP,
                      "ItemList/Item/Extn/@ExtnBillableWeight");
              double dCompBillableWeight = (!YFCCommon
                  .isVoid(sCompBillableWeight) ? Double
                      .valueOf(sCompBillableWeight) : 0.00);

              dItemBillableWeight = dItemBillableWeight
                  + (dCompBillableWeight * dKitQuantity);
            }
          }

          // Phase-2 changes for quantity to ship - starts
          double dOrderLineQty;
          if (eleOrderLine.hasAttribute("QuantityToShip")) {
            String sQtyToship = eleOrderLine.getAttribute("QuantityToShip");
            dOrderLineQty = (!YFCCommon.isVoid(sQtyToship) ? Double.valueOf(sQtyToship) : 0.00);
          } else {
            String sOrderLineQty = eleOrderLine.getAttribute(GCConstants.ORDERED_QTY);
            dOrderLineQty = (!YFCCommon.isVoid(sOrderLineQty) ? Double.valueOf(sOrderLineQty) : 0.00);
          }
          // Phase-2 changes for quantity to ship - ends

          dItemBillableWeight = dItemBillableWeight * dOrderLineQty;
          boolean bIsLastLine = false;
          if (nlOrderLine.getLength() == (iCounter + 1)) {
            bIsLastLine = true;
          }
          LOGGER.debug(" sItemBillableWeight ::"
              + sItemBillableWeight + " dOrderLineQty ::" + dOrderLineQty
              + " bIsLastLine" + bIsLastLine);

          double dProrationRatio = calculateProrationRatio(
              dTotalBillableWeight, dItemBillableWeight,
              dTotalQty, dOrderLineQty);
          eleOrderLine.setAttribute(GCConstants.PRORATION_RATIO,
              String.valueOf(dProrationRatio));

          NodeList nlHeaderCharge = XPathAPI.selectNodeList(inDoc,
              "//HeaderCharges/HeaderCharge");
          prorateCharges(inDoc, nlHeaderCharge, bIsLastLine,
              eleOrderLine, chargeMap);
          // NodeList nlHeaderTax = XPathAPI.selectNodeList(inDoc,
          // "//HeaderTaxes/HeaderTax");
          // prorateTaxes(inDoc, nlHeaderTax, bIsLastLine, eleOrderLine,
          // taxMap);
        }
      }
      // zeroing out the header level charges and taxes
      zeroingHeaderCharges(inDoc);
      Element eleExtn = GCXMLUtil.getElementByXPath(inDoc, "Order/Extn");
      if(!YFCCommon.isVoid(eleExtn)){
        eleExtn.setAttribute("ExtnIsShippingChargeProrated",
            GCConstants.YES);
        env.setTxnObject("isProration", GCConstants.YES);
      }
      
    
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCProcessProrations.prorateHeaderCharges(), Exception is :",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCProcessProrations.prorateHeaderCharges() method ");
  }

  public Document prorateBundleParentCharges(YFSEnvironment env,
      Document inDoc) throws GCException {
    LOGGER.debug(" Entering GCProcessProrations.prorateBundleParentCharges() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" prorateBundleParentCharges() method, Input document is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    try {
      Set setList = properties.keySet();
      Iterator<String> listIterator = setList.iterator();
      String strOrderLineRoot = "";
      String strOrderLineXPath = "";
      String strParentXpath = "";
      String strLineChargeXPath = "";
      String strLineTaxXPath = "";
      String strLinePriceLevel = "";
      String isInvoice="";

      // Fix for 3567
      boolean bIsOrderLineCompService = false;

      while (listIterator.hasNext()) {

        String strParamName = listIterator.next();
        String strParamValue = properties.getProperty(strParamName);
        if (GCConstants.ORDER_LINE_ROOT.equals(strParamName)) {
          strOrderLineRoot = strParamValue;
        }
        if (GCConstants.ORDER_LINE_XPATH.equals(strParamName)) {
          strOrderLineXPath = strParamValue;
        }
        if (GCConstants.PARENT_LINE_XPATH.equals(strParamName)) {
          strParentXpath = strParamValue;
        }
        if (GCConstants.LINE_CHARGE_XPATH.equals(strParamName)) {
          strLineChargeXPath = strParamValue;
        }
        if (GCConstants.LINE_TAX_XPATH.equals(strParamName)) {
          strLineTaxXPath = strParamValue;
        }
        if (GCConstants.LINE_PRICE_LEVEL.equals(strParamName)) {
          strLinePriceLevel = strParamValue;
        }
        if (GCConstants.IS_INVOICE_ARG.equals(strParamName)) {
          isInvoice = strParamValue;
        }
        // Fix for 3567
        if (YFCCommon.equals("IsStampOrderLineCompService", strParamName)) {
          bIsOrderLineCompService = true;
        }
      }
      strOrderLineRoot = strOrderLineRoot + strOrderLineXPath;
      LOGGER.debug("Inside GCProcessProrations.prorateBundleParentCharges: The indoc is "
          + GCXMLUtil.getXMLString(inDoc));
      NodeList nlParentLineList = XPathAPI.selectNodeList(inDoc,
          strOrderLineRoot);
      // Fix for OMS-5379 Start
      final String strOrigParentXPath = strParentXpath;
      final String strOrigLinePriceLevel = strLinePriceLevel;
      // Fix for OMS-5379 End
      for (int iParentLineCounter = 0; iParentLineCounter < nlParentLineList
          .getLength(); iParentLineCounter++) {
        LOGGER.debug(" Inside Loop on parent lines, iParentLineCounter is :"
            + iParentLineCounter);

        // Fix for OMS-5379 Start
        strParentXpath = strOrigParentXPath;
        strLinePriceLevel = strOrigLinePriceLevel;

        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("GCProcessProrations.prorateBundleParentCharges: Parent XPath is :" + strParentXpath
              + "LinePriceLevel XPath is :" + strLinePriceLevel);
        }

        // Fix for OMS-5379 End
        Element eleParentLine = (Element) nlParentLineList
            .item(iParentLineCounter);

        /* ---------------- OMS-5164 & OMS-5178 Starts -------------- */

        Element extnEle = (Element) XPathAPI.selectSingleNode(eleParentLine,
            GCConstants.XPATH_ITEM_DETAILS_EXTN);
        String extnSetCode = extnEle.getAttribute(GCConstants.EXTN_SET_CODE);

        //GCSTORE-5934--Start
        String sParentLineKey = eleParentLine.getAttribute(GCConstants.ORDER_LINE_KEY);
        LOGGER.debug(" sParentLineKey is :" + sParentLineKey);
        String strParentXPShipNode = "";
        strParentXPShipNode = strParentXpath.replace("String", sParentLineKey);
        strParentXPShipNode = strParentXPShipNode + "/@ShipNode";
        String shipNode = "";
        shipNode = GCXMLUtil.getAttributeFromXPath(inDoc, strParentXPShipNode);
        //GCSTORE-5934--End
        // Fix for 3567
        if (!bIsOrderLineCompService) {
          if (GCConstants.ONE.equals(extnSetCode) && !YFCUtils.equals(GCConstants.MFI, shipNode)) {
            //GCSTORE-5934-Start
            continue;
            //GCSTORE-5934--End
          }
        }


        /* ---------------- OMS-5164 & OMS-5178 Ends --------------- */


        Map<String, Double> mCostsAndMargins = new HashMap<String, Double>();
        String sTotalCompsRetailCost = getAllcomponentLinesTotalRetailCost(
            inDoc, sParentLineKey, strParentXpath);
        double dTotalCompsRetailCost = Double
            .valueOf(sTotalCompsRetailCost);
        mCostsAndMargins.put("TotalCompsRetailCost",
            dTotalCompsRetailCost);

        String sTotalCompsUnitCost = getAllcomponentLinesTotalUnitCost(
            inDoc, sParentLineKey, strParentXpath);
        double dTotalCompsUnitCost = (!YFCCommon
            .isVoid(sTotalCompsUnitCost) ? Double
                .valueOf(sTotalCompsUnitCost) : 0.00);
        mCostsAndMargins.put("TotalCompsUnitCost", dTotalCompsUnitCost);

        double dTotalGrossMargin = dTotalCompsRetailCost
            - dTotalCompsUnitCost;
        mCostsAndMargins.put("TotalGrossMargin", dTotalGrossMargin);

        // Phase-2 changes for quantity to ship - starts
        String sOrderedQty;
        if (eleParentLine.hasAttribute("QuantityToShip")) {
          sOrderedQty = eleParentLine.getAttribute("QuantityToShip");
        } else {
          sOrderedQty = eleParentLine.getAttribute(GCConstants.ORDERED_QTY);
        }
        // Phase-2 changes for quantity to ship - ends

        double dOrderedQty = Double.valueOf(sOrderedQty);

        String sBundleLineCost = GCXMLUtil.getAttributeFromXPath(
            eleParentLine, strLinePriceLevel + "/@UnitPrice");
        double dBundleLineCost = (!YFCCommon.isVoid(sBundleLineCost) ? Double
            .valueOf(sBundleLineCost) : 0.00);
        if (dBundleLineCost == 0.00) {
          sBundleLineCost = GCXMLUtil.getAttributeFromXPath(
              eleParentLine, strLinePriceLevel + "/@ListPrice");
          dBundleLineCost = (!YFCCommon.isVoid(sBundleLineCost) ? Double
              .valueOf(sBundleLineCost) : 0.00);
        }
        //OMS-4015 Start
        mCostsAndMargins.put("BundleUnitPrice", dBundleLineCost);
        //OMS-4015 End
        dBundleLineCost = dBundleLineCost * dOrderedQty;
        mCostsAndMargins.put("BundleLineCost", dBundleLineCost);
        double dKitGrossMargin = dBundleLineCost - dTotalCompsUnitCost;
        mCostsAndMargins.put("KitGrossMargin", dKitGrossMargin);

        // Fetching sum of Quantities from all the child lines for the
        // bundle parent line
        strParentXpath = strParentXpath.replace("String",
            sParentLineKey);
        String sTotalQuantities = XPathAPI.eval(inDoc,
            "sum(" + strParentXpath + "/@OrderedQty)").toString();
        double dTotalQuantities = Double.valueOf(sTotalQuantities);

        Map<String, BigDecimal> chargeMap = new HashMap<String, BigDecimal>();
        Map<String, Double> taxMap = new HashMap<String, Double>();
        double dSumProratedLinesTotal = 0.00;
        // Fetching the OrderLines having ParentLine as Bundle Line
        if (GCConstants.FLAG_Y.equalsIgnoreCase(isInvoice)) {
          strParentXpath = strParentXpath + GCConstants.XPATH_ONE_STEP_BACK;
          strLinePriceLevel = GCConstants.MODIFY_LINE_PRICE_LEVEL_FOR_INVOICE
              + strLinePriceLevel;
        }
        NodeList nlChildLineList = XPathAPI.selectNodeList(inDoc,
            strParentXpath);
        for (int iChildLineCounter = 0; iChildLineCounter < nlChildLineList
            .getLength(); iChildLineCounter++) {
          LOGGER.debug(" Inside Loop on child lines, iChildLineCounter is :"
              + iChildLineCounter);

          Element eleChildLine = (Element) nlChildLineList
              .item(iChildLineCounter);
          String sCompListPrice = GCXMLUtil.getAttributeFromXPath(
              eleChildLine, strLinePriceLevel + "/@ListPrice");
          if(sCompListPrice==null){
            sCompListPrice = GCXMLUtil.getAttributeFromXPath(
                eleChildLine, strLinePriceLevel + "/@UnitPrice");
          }
          double dCompListPrice = (!YFCCommon.isVoid(sCompListPrice) ? Double
              .valueOf(sCompListPrice) : 0.00);
          String sComponentQuantity = "";
          String sCompUnitCost = "";
          if(GCConstants.FLAG_Y.equalsIgnoreCase(isInvoice)){
            sComponentQuantity = eleChildLine
                .getAttribute(GCConstants.QUANTITY);
            sCompUnitCost = GCXMLUtil.getAttributeFromXPath(
                eleChildLine, GCConstants.XPATH_ORDLN_ITEM_DETAILS_PRIMARY_INFO_UNIT_COST);
          }else{
            sComponentQuantity = eleChildLine
                .getAttribute(GCConstants.ORDERED_QTY);
            sCompUnitCost = GCXMLUtil.getAttributeFromXPath(
                eleChildLine, GCConstants.XPATH_ITEM_DETAILS_PRIMARY_INFO_UNIT_COST);
          }

          double dCompUnitCost = (!YFCCommon.isVoid(sCompUnitCost) ? Double
              .valueOf(sCompUnitCost) : 0.00);

          double dComponentQuantity = (!YFCCommon
              .isVoid(sComponentQuantity)) ? Double
                  .valueOf(sComponentQuantity) : 0;
                  mCostsAndMargins.put("ComponentQuantity",
                      dComponentQuantity);
                  double dCompGrossMargin = (dCompListPrice - dCompUnitCost)
                      * dComponentQuantity;
                  mCostsAndMargins.put("CompGrossMargin", dCompGrossMargin);
                  double dCompTotalUnitCost = dCompUnitCost
                      * dComponentQuantity;
                  mCostsAndMargins.put("CompTotalUnitCost",
                      dCompTotalUnitCost);

                  boolean bIsLastLine = false;
                  if (nlChildLineList.getLength() == iChildLineCounter + 1) {
                    LOGGER.debug(" Inside if(nlChildLineList.getLength() == iChildLineCounter+1)");
                    bIsLastLine = true;
                  }

                  double dCompPercentage = calculateComponentPercentage(
                      eleChildLine, mCostsAndMargins, dTotalQuantities);
                  mCostsAndMargins.put("CompPercentage", dCompPercentage);

                  dSumProratedLinesTotal = prorateUnitPrice(inDoc,
                      eleChildLine, mCostsAndMargins,
                      dSumProratedLinesTotal, bIsLastLine,
                      strLinePriceLevel, isInvoice);
                  NodeList nlLineCharge = XPathAPI.selectNodeList(
                      eleParentLine, strLineChargeXPath);
                  NodeList nlLineTax = XPathAPI.selectNodeList(eleParentLine,
                      strLineTaxXPath);
                  prorateCharges(inDoc, nlLineCharge, bIsLastLine,
                      eleChildLine, chargeMap);
                  prorateTaxes(inDoc, nlLineTax, bIsLastLine, eleChildLine,
                      taxMap);
                  if(GCConstants.FLAG_Y.equalsIgnoreCase(isInvoice)){
                    String promoRatioAttr = eleChildLine.getAttribute(GCConstants.PRORATION_RATIO);
                    eleChildLine.removeAttribute(GCConstants.PRORATION_RATIO);
                    Element orderLineEle = (Element) XPathAPI.selectSingleNode(eleChildLine,
                        GCConstants.ORDER_LINE);
                    orderLineEle.setAttribute(GCConstants.PRORATION_RATIO, promoRatioAttr);
                  }
        }
        strParentXpath = strParentXpath.replace(sParentLineKey,
            "String");
        //OMS-4743 Start
        //GCSTORE-4107 Start
        YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
        YFCElement eleInvoiceDetail = yfcInDoc.getDocumentElement().getChildElement("InvoiceHeader");
        // Fix for GCSTORE-4516 
        if(!YFCCommon.isVoid(eleInvoiceDetail)){
        	String sInvoiceType = eleInvoiceDetail.getAttribute("InvoiceType");
            int dCompLinesLen = nlChildLineList.getLength();
            if( !YFCCommon.equals(dCompLinesLen, 0) || !YFCCommon.equals(sInvoiceType, "CREDIT_MEMO")){
    	        Element eleLineCharges= (Element)XPathAPI.selectSingleNode(eleParentLine, "../LineCharges");
    	        if(!YFCCommon.isVoid(eleLineCharges)){
    	          Element eleLineDetailsParent= (Element)XPathAPI.selectSingleNode(eleParentLine, "..");
    	        //Fix for GCSTORE-5934/7368-- Start
				if(!GCConstants.ONE.equals(extnSetCode)){
    	          eleLineDetailsParent.removeChild(eleLineCharges);
				}
				//Fix for GCSTORE-5934/7368-- End
    	        }
    	        Element eleLineTaxes = (Element)XPathAPI.selectSingleNode(eleParentLine, "../LineTaxes");
    	        if(!YFCCommon.isVoid(eleLineTaxes)){
    	          Element eleLineDetailsParent= (Element)XPathAPI.selectSingleNode(eleParentLine, "..");
    	        //Fix for GCSTORE-5934/7368-- Start
				if(!GCConstants.ONE.equals(extnSetCode)){
    	          eleLineDetailsParent.removeChild(eleLineTaxes);
				}
				//Fix for GCSTORE-5934/7368-- End
    	        }
            }
        }
        
        //OMS-4743 End
        // OMS-5164 Start
        //FIX for 5934--Start
        if(!GCConstants.ONE.equals(extnSetCode)){
        ((Element) eleParentLine.getParentNode()).setAttribute(GCConstants.TAX,
            GCConstants.ZERO_AMOUNT);
        }
        //FIX for 5934--End
        // OMS-5164 Ends
      }
      // OMS 5467 Start
      if (GCConstants.FLAG_Y.equalsIgnoreCase(isInvoice)) {
        final Element eleInvoiceHeader = GCXMLUtil.getElementByXPath(inDoc, "/InvoiceDetail/InvoiceHeader");
        final String strDocumenttype = eleInvoiceHeader.getAttribute(GCConstants.DOCUMENT_TYPE);

        if (GCConstants.RETURN_ORDER_DOCUMENT_TYPE.equalsIgnoreCase(strDocumenttype)) {

          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(CLASS_NAME + "Inside Return Order Unit Price");
          }
          updateReturnOrderUnitPrice(inDoc);
		  copyPaypalTransactionID(env, inDoc);
        }
      //GCSTORE-6133-starts
        if(GCConstants.SALES_ORDER_DOCUMENT_TYPE.equalsIgnoreCase(strDocumenttype))
        {
      
        Element eleInGCPaypalRefundDistList =GCXMLUtil.getElementByXPath(inDoc, "/InvoiceDetail/InvoiceHeader/Order/Extn/GCPaypalRefundDistList");
        
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Element eleInGCPaypalRefundDistList ::"+GCXMLUtil.getElementString(eleInGCPaypalRefundDistList));
        }
        
        NodeList nlGCPaypalRefundDistList =eleInGCPaypalRefundDistList.getElementsByTagName("GCPaypalRefundDist");
        
       @SuppressWarnings("unchecked")
         List <String> lstChargeTxnKey=GCXMLUtil.getAttributeStringListByXpath(inDoc, "/InvoiceDetail/InvoiceHeader/CollectionDetails/CollectionDetail/PaymentMethod[@PaymentType='"+GCConstants.PAYPAL+"']/../@ChargeTransactionKey");
         int flag=0;
         Element eleGCPaypalRefundDist=null;
      
         
        for(int i=0 ; i<nlGCPaypalRefundDistList.getLength();i++)
             {
             eleGCPaypalRefundDist=(Element) nlGCPaypalRefundDistList.item(i);
             LOGGER.debug("Inside outer for loop :: Element eleGCPaypalRefundDist ::"+GCXMLUtil.getElementString(eleGCPaypalRefundDist));
            String strChargeTxnKey1=eleGCPaypalRefundDist.getAttribute("ChargeTransactionKey");
            LOGGER.debug("Inside outer for loop :: strChargeTxnKey1::"+strChargeTxnKey1);
            for(int k=0;k<lstChargeTxnKey.size();k++)
            {
            LOGGER.debug("Inside inner for loop :: strChargeTxnKey1::"+strChargeTxnKey1);
            if(!YFCCommon.isVoid(strChargeTxnKey1) && !YFCCommon.isVoid(lstChargeTxnKey.get(k)) && strChargeTxnKey1.equals(lstChargeTxnKey.get(k)))
                {
                LOGGER.debug("Inside inner for loop Equals condition satisfied for chargeTxnKey"+strChargeTxnKey1);
                flag=1;
                break;
                     
                }
         
          
            }
     if(flag==0)
     {
         LOGGER.debug("Inside outer for loop Equals condition not satisfied for chargeTxnKey :: "+strChargeTxnKey1+"Removing element eleGCPaypalRefundDist from eleInGCPaypalRefundDistList" );
         eleInGCPaypalRefundDistList.removeChild(eleGCPaypalRefundDist);
         LOGGER.debug("eleInGCPaypalRefundDistList ::"+GCXMLUtil.getElementString(eleInGCPaypalRefundDistList));
         i=i-1;
         
     }
     flag=0;
     if (!YFCCommon.isVoid(eleGCPaypalRefundDist.getAttribute("ChargeTransactionKey"))) {
         eleGCPaypalRefundDist.removeAttribute("ChargeTransactionKey");
     }
    } 
       } 
        //GCSTORE-6133-ends
      }
      
      
      

      
      // OMS 5467 End
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Inside GCProcessProrations.prorateBundleParentCharges: The outDoc is "
            + GCXMLUtil.getXMLString(inDoc));
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCProcessProrations.prorateBundleParentCharges(), Exception is :",
          e);
      throw new GCException(e);
    }
    return inDoc;
  }
  
  /**
   * This method copies the paypal transaction ID from sales order to return order
   *
   *
   * @param inDoc
  */
  private void copyPaypalTransactionID(YFSEnvironment env, Document inDoc) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(CLASS_NAME + "Inside copyPaypalTransactionID");
    }
    // check if paypal is one of the refund payment methods
    final Element elePaymentMethod =
        GCXMLUtil.getElementByXPath(inDoc, GCConstants.PAYPAL_PAYMENT_XPATH);

    if (!YFCObject.isNull(elePaymentMethod)) {

      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(CLASS_NAME + "Atleast One Payment Method is Paypal");
      }

      final String strDerivedFromOrderHeaderKey =
          GCXMLUtil.getAttributeFromXPath(inDoc, GCConstants.DERIVED_FROM_ORDERHEADERKEY_XPATH);

      if (!YFCObject.isVoid(strDerivedFromOrderHeaderKey)) {

        final Element eleOutGCPaypalDistList = getGCPaypalRefundDistList(env, strDerivedFromOrderHeaderKey);

        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug(CLASS_NAME + "GCPaypalDistList Output Element"
              + GCXMLUtil.getXMLString(GCXMLUtil.getDocumentFromElement(eleOutGCPaypalDistList)));
        }

        Element eleOrderExtn = GCXMLUtil.getElementByXPath(inDoc, GCConstants.INVOICE_ORDER_EXTN);
        final Element eleInGCPaypalRefundDistList =
            GCXMLUtil.getFirstElementByName(eleOrderExtn, GCConstants.GC_PAYPAL_REFUNDDIST_LIST);
        eleOrderExtn.removeChild(eleInGCPaypalRefundDistList);
        final Element eleGCPaypal = (Element) inDoc.importNode(eleOutGCPaypalDistList, true);
        eleOrderExtn.appendChild(eleGCPaypal);

      }

    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(CLASS_NAME + "Output from copyPaypalTransactionID" + GCXMLUtil.getXMLString(inDoc));
    }


  }

  /**
   * @
   *
   * This method calls getGCPaypalRefundDistList API to fetch the Paypal TransactionID
   *
   * @param strDerivedFromOrderHeaderKey
   * @return
   */
  private Element getGCPaypalRefundDistList(YFSEnvironment env, String strDerivedFromOrderHeaderKey) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(CLASS_NAME + "Inside getGCPaypalRefundDistList");
    }
    final Document docInGCPaypalRefundDist = GCXMLUtil.createDocument(GCConstants.GC_PAYPAL_REFUND_DIST);
    Element eleGCPaypalRefundDist = docInGCPaypalRefundDist.getDocumentElement();
    eleGCPaypalRefundDist.setAttribute(GCConstants.ORDER_HEADER_KEY, strDerivedFromOrderHeaderKey);

    Document docOutGetGCPaypalDistList =
        GCCommonUtil.invokeService(env, GCConstants.GC_GET_PP_REFUND_DIST_LIST_SERVICE, docInGCPaypalRefundDist);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(CLASS_NAME + "Service Output" + GCXMLUtil.getXMLString(docOutGetGCPaypalDistList));
    }
    final Element eleOutGetGCPaypalDistList = docOutGetGCPaypalDistList.getDocumentElement();

    return eleOutGetGCPaypalDistList;
  }

  /**
   *
   * Description of getAllcomponentLinesTotalCost Calculate the total retail
   * cost of all the components as: ListPrice of Comp1 * Qty of Comp1 + .....
   * + ListPrice of Comp(n) * Qty of Comp(n)
   *
   * @param inDoc
   * @param eleParentLine
   * @param sParentLineKey
   * @return
   * @throws Exception
   *
   */
  private static String getAllcomponentLinesTotalRetailCost(Document inDoc,
      String sParentLineKey, String strParentXpath) throws GCException {
    LOGGER.debug(" Entering GCProcessProrations.getAllcomponentLinesTotalRetailCost() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" getAllcomponentLinesTotalRetailCost() method, Input document is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    LOGGER.debug(" sParentLineKey" + sParentLineKey);
    double dTotalComponentsCost = 0.00;
    try {
      String strParentXP = "";
      strParentXP = strParentXpath.replace("String", sParentLineKey);
      NodeList nlChildLineList = GCXMLUtil.getNodeListByXpath(inDoc,
          strParentXP);
      for (int iChildLineCounter = 0; iChildLineCounter < nlChildLineList
          .getLength(); iChildLineCounter++) {
        LOGGER.debug(" Inside Loop on child lines, iChildLineCounter is :"
            + iChildLineCounter);
        Element eleChildLine = (Element) nlChildLineList
            .item(iChildLineCounter);
        String sComponentCost = GCXMLUtil.getAttributeFromXPath(
            eleChildLine, "LinePriceInfo/@ListPrice");
        if(sComponentCost==null){
          sComponentCost = GCXMLUtil.getAttributeFromXPath(
              eleChildLine, "LinePriceInfo/@UnitPrice");
        }
        double dComponentCost = (!YFCCommon.isVoid(sComponentCost)) ? Double
            .valueOf(sComponentCost) : 0.00;
            String sComponentQty = eleChildLine
                .getAttribute(GCConstants.ORDERED_QTY);
            double dComponentQty = Double.valueOf(sComponentQty);
            LOGGER.debug(" sComponentCost" + sComponentCost
                + " sComponentQty" + sComponentQty);

            dTotalComponentsCost = dTotalComponentsCost
                + (dComponentCost * dComponentQty);
            LOGGER.debug(" dTotalComponentsCost:" + dTotalComponentsCost);
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCProcessProrations.getAllcomponentLinesTotalRetailCost(), Exception is :",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" dTotalComponentsCost:" + dTotalComponentsCost);
    LOGGER.debug(" Exiting GCProcessProrations.getAllcomponentLinesTotalRetailCost() method ");
    return String.valueOf(dTotalComponentsCost);
  }

  /**
   *
   * Description of getAllcomponentLinesTotalUnitCost Calculate the total unit
   * cost of all the components as: UnitPrice of Comp1 * Qty of Comp1 + .....
   * + UnitPrice of Comp(n) * Qty of Comp(n)
   *
   * @param inDoc
   * @param sParentLineKey
   * @return
   * @throws Exception
   *
   */
  private static String getAllcomponentLinesTotalUnitCost(Document inDoc,
      String sParentLineKey, String strParentXPath) throws GCException {
    LOGGER.debug(" Entering GCProcessProrations.getAllcomponentLinesTotalUnitCost() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" getAllcomponentLinesTotalUnitCost() method, Input document is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    LOGGER.debug(" sParentLineKey" + sParentLineKey);
    double dTotalComponentsCost = 0.00;
    try {
      String strParentXP = "";
      strParentXP = strParentXPath.replace("String", sParentLineKey);
      NodeList nlChildLineList = XPathAPI.selectNodeList(inDoc,
          strParentXP);
      for (int iChildLineCounter = 0; iChildLineCounter < nlChildLineList
          .getLength(); iChildLineCounter++) {
        LOGGER.debug(" Inside Loop on child lines, iChildLineCounter is :"
            + iChildLineCounter);

        Element eleChildLine = (Element) nlChildLineList
            .item(iChildLineCounter);
        String sComponentCost = GCXMLUtil.getAttributeFromXPath(
            eleChildLine,
            "ItemDetails/PrimaryInformation/@UnitCost");
        double dComponentCost = (!YFCCommon.isVoid(sComponentCost)) ? Double
            .valueOf(sComponentCost) : 0.00;
            String sComponentQty = eleChildLine
                .getAttribute(GCConstants.ORDERED_QTY);
            double dComponentQty = Double.valueOf(sComponentQty);
            LOGGER.debug(" sComponentCost" + sComponentCost
                + " sComponentQty" + sComponentQty);

            dTotalComponentsCost = dTotalComponentsCost
                + (dComponentCost * dComponentQty);
            LOGGER.debug(" dTotalComponentsCost:" + dTotalComponentsCost);
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCProcessProrations.getAllcomponentLinesTotalUnitCost(), Exception is :",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" dTotalComponentsCost:" + dTotalComponentsCost);
    LOGGER.debug(" Exiting GCProcessProrations.getAllcomponentLinesTotalUnitCost() method ");
    return String.valueOf(dTotalComponentsCost);
  }

  /**
   *
   * Description of calculateComponentPercentage Calculate proration ratio on
   * the basis of: a) Component Gross Margin /Total Gross Margin b) if Total
   * Gross Margin is zero, then calculate it as: ComponentQty/TotalQtys of all
   * components
   *
   * @param eleChildLine
   * @param dTotalComponentsCost
   * @param dTotalQuantities
   * @param dOrderedQty
   * @throws GCException
   *
   */
  public static double calculateComponentPercentage(Element eleChildLine,
      Map<String, Double> mCostsAndMargins, double dTotalQuantities)
          throws GCException {
    LOGGER.debug(" Entering GCProcessProrations.calculateComponentPercentage() method ");
    LOGGER.debug(" dTotalQuantities" + dTotalQuantities);
    double dCompPercentage = 0.00;
    double dProrationRatio = 0.00;
    try {
      double dTotalGrossMargin = mCostsAndMargins.get("TotalGrossMargin");
      double dCompGrossMargin = mCostsAndMargins.get("CompGrossMargin");
      double dComponentQuantity = mCostsAndMargins
          .get("ComponentQuantity");

      LOGGER.debug(" dTotalGrossMargin is " + dTotalGrossMargin
          + " dCompGrossMargin is " + dCompGrossMargin
          + " dComponentQuantity ::" + dComponentQuantity);

      if (dTotalGrossMargin > 0.00) {
        LOGGER.debug(" Inside if(dTotalGrossMargin != 0.00) ");
        if(dTotalGrossMargin==0){
          dProrationRatio = 0.0;
        }else{
          dProrationRatio = dCompGrossMargin / dTotalGrossMargin;
        }
        LOGGER.debug(" dCompPercentage" + dCompPercentage);
      } else {
        LOGGER.debug(" Inside else  dTotalGrossMargin != 0.00");
        if(dTotalQuantities==0){
          dProrationRatio = 0.0;
        }else{
          dProrationRatio = dComponentQuantity / dTotalQuantities;
        }
        LOGGER.debug(" dCompPercentage" + dCompPercentage);
      }
      BigDecimal a = new BigDecimal(String.valueOf(dProrationRatio));
      BigDecimal bProratedRatio = a.setScale(3,
          BigDecimal.ROUND_HALF_EVEN);
      eleChildLine.setAttribute(GCConstants.PRORATION_RATIO,
          bProratedRatio.toString());

    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCProcessProrations.calculateComponentPercentage(), Exception is :",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCProcessProrations.calculateProrationRatio() method ");
    dCompPercentage = dProrationRatio * 100;
    return dCompPercentage;
  }

  /**
   *
   * Description of prorateUnitPrice Prorate UnitPrice on ChildLines on the
   * basis of Component Percentage
   *
   * @param docChangeOrder
   * @param eleChildLine
   * @param dBundleLineCost
   * @param dSumProratedLinesTotal
   * @param dCompUnitCost
   * @param bIsLastLine
   * @return
   * @throws GCException
   *
   */
  public static double prorateUnitPrice(Document inDoc, Element eleChildLine,
      Map<String, Double> mCostsAndMargins,
      double dSumProratedLinesTotal, boolean bIsLastLine,
      String strLinePriceLevel, String isInvoice) throws GCException {
    LOGGER.debug(" Entering GCProcessProrations.prorateUnitPrice() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" prorateUnitPrice() method, docChangeOrder is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    try {
      Element eleLinePriceInfo = (Element) XPathAPI.selectSingleNode(
          eleChildLine, strLinePriceLevel);
      double dKitCompRetailPrice=0.0;
      double dKitGrossMargin = mCostsAndMargins.get("KitGrossMargin");
      double dCompPercentage = mCostsAndMargins.get("CompPercentage");
      double dComponentQuantity = mCostsAndMargins
          .get("ComponentQuantity");
      double dBundleLineCost = mCostsAndMargins.get("BundleLineCost");
      double dBundleUnitPrice= mCostsAndMargins.get("BundleUnitPrice");
      //OMS-4015 Start
      double dCompTotalUnitCost = mCostsAndMargins
          .get("CompTotalUnitCost");

      double dTotalKitCompRetailPrice = (dKitGrossMargin * dCompPercentage)
          / 100 + dCompTotalUnitCost;

      dSumProratedLinesTotal = dSumProratedLinesTotal
          + dTotalKitCompRetailPrice;
      LOGGER.debug(" dSumProratedLinesTotal is " + dSumProratedLinesTotal
          + " dTotalKitCompRetailPrice is "
          + dTotalKitCompRetailPrice + " dKitGrossMargin ::"
          + dKitGrossMargin + " dCompPercentage ::" + dCompPercentage
          + " dComponentQuantity ::" + dComponentQuantity
          + " dBundleLineCost ::" + dBundleLineCost
          + " dCompTotalUnitCost ::" + dCompTotalUnitCost);
      if(dComponentQuantity==0){
        dKitCompRetailPrice=0.0;
      }else{
        dKitCompRetailPrice = dTotalKitCompRetailPrice
            / dComponentQuantity;
      }
      //OMS-4637 Start
      BigDecimal a = new BigDecimal(String.valueOf(dKitCompRetailPrice));
      BigDecimal bUnitPrice = a.setScale(3,
          BigDecimal.ROUND_HALF_EVEN);
      String strUnitPrice = bUnitPrice.toString();
      eleLinePriceInfo.setAttribute(GCConstants.UNIT_PRICE,
          String.valueOf(strUnitPrice));
      //OMS-4637 End
      eleLinePriceInfo.setAttribute(GCConstants.IS_PRICE_LOCKED,
          GCConstants.YES);
      LOGGER.debug(" dKitCompRetailPrice is " + strUnitPrice);

      // If difference of (Bundle cost of Order and total prorated cost)
      // is >0 and it is last line, then updated the
      // remaining charges on last line
      if (bIsLastLine) {
        if ((dBundleLineCost - dSumProratedLinesTotal) > 0.00) {
          LOGGER.debug(" Inside if (dBundleLineCost-dSumProratedLinesTotal)>0.00)");
          dTotalKitCompRetailPrice = dTotalKitCompRetailPrice
              + (dBundleLineCost - dSumProratedLinesTotal);
          LOGGER.debug(" dTotalKitCompRetailPrice is "
              + dTotalKitCompRetailPrice);
        } else if ((dSumProratedLinesTotal - dBundleLineCost) > 0.00) {
          LOGGER.debug(" Inside if else if((dSumProratedLinesTotal-dBundleLineCost)>0.00)");
          dTotalKitCompRetailPrice = dTotalKitCompRetailPrice
              - (dSumProratedLinesTotal - dBundleLineCost);
          LOGGER.debug(" dTotalKitCompRetailPrice is "
              + dTotalKitCompRetailPrice);
        }
        if(dComponentQuantity==0){
          dKitCompRetailPrice=0.0;
        }else{
          dKitCompRetailPrice = dTotalKitCompRetailPrice
              / dComponentQuantity;
        }
      }
      //OMS-4637 Start
      BigDecimal obj = new BigDecimal(String.valueOf(dKitCompRetailPrice));
      BigDecimal bUnitPriceLastLine = obj.setScale(3,
          BigDecimal.ROUND_HALF_EVEN);
      String strUnitPriceLastLine = bUnitPriceLastLine.toString();
      eleLinePriceInfo.setAttribute(GCConstants.UNIT_PRICE,
          String.valueOf(strUnitPriceLastLine));
      //OMS-4637 End
      eleLinePriceInfo.setAttribute(GCConstants.IS_PRICE_LOCKED,
          GCConstants.YES);
      if(!GCConstants.FLAG_Y.equalsIgnoreCase(isInvoice)) {
        eleChildLine.appendChild(eleLinePriceInfo);
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCProcessProrations.prorateUnitPrice()",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCProcessProrations.prorateUnitPrice() method ");
    return dSumProratedLinesTotal;
  }

  /**
   * This method prorated the charges LineCharges or HederCharges on the basis
   * on proration ratio.
   *
   * @param nlCharge
   * @param bIsLastLine
   * @return
   */
  public static void prorateCharges(Document inDoc, NodeList nlCharge,
      boolean bIsLastLine, Element eleChildLine,
      Map<String, BigDecimal> chargeMap) throws GCException {
    LOGGER.debug(" Entering GCProcessProrations.prorateCharges() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" prorateCharges() method, docChangeOrder is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    try {
      Element eleLineCharges = (Element) XPathAPI.selectSingleNode(
          eleChildLine, "LineCharges");
      if (YFCCommon.isVoid(eleLineCharges)) {
        eleLineCharges = GCXMLUtil.createElement(inDoc,
            GCConstants.LINE_CHARGES, null);
        eleChildLine.appendChild(eleLineCharges);
      }
      String sMapKey = null;
      BigDecimal dMapCharge = new BigDecimal("0.00");
      String sProrationRatio = eleChildLine
          .getAttribute(GCConstants.PRORATION_RATIO);
      double dProrationRatio = (!YFCCommon.isVoid(sProrationRatio)) ? Double
          .valueOf(sProrationRatio) : 0.00;
          LOGGER.debug(" dProrationRatio" + dProrationRatio);
          for (int iCounter = 0; iCounter < nlCharge.getLength(); iCounter++) {
            Element eleCharge = (Element) nlCharge.item(iCounter);
            String sChargeCategory = eleCharge
                .getAttribute(GCConstants.CHARGE_CATEGORY);
            String sChargeName = eleCharge
                .getAttribute(GCConstants.CHARGE_NAME);
            String sIsDiscount = eleCharge
                .getAttribute(GCConstants.IS_DISCOUNT);

            String sCharge = eleCharge
                .getAttribute(GCConstants.CHARGE_PER_LINE);
            boolean bIsHeaderCharge = false;
            if (YFCCommon.isVoid(sCharge) || YFCCommon.equals(Double.parseDouble(sCharge), 0.00)) {
              sCharge = eleCharge.getAttribute(GCConstants.CHARGE_AMOUNT);
              if (!YFCCommon.isVoid(sCharge)) {
                bIsHeaderCharge = true;
                LOGGER.debug(" true bIsHeaderCharge is :"
                    + bIsHeaderCharge);
              }
            }

            // Change for 5010 - begin

            final String sItemBillableWeight = eleChildLine.getAttribute(GCConstants.BILLABLE_WEIGHT);

            final double dItemBillableWeight =
                (!YFCCommon.isVoid(sItemBillableWeight) ? Double.valueOf(sItemBillableWeight) : 0.00);

            boolean isItemShippable = checkForShippableLines(dItemBillableWeight, sChargeCategory, bIsHeaderCharge);


            if (LOGGER.isDebugEnabled()) {
              LOGGER.debug(CLASS_NAME + "Return from checkForShippableLines" + isItemShippable);
            }

            // Change for 5010 - End

            double dCharge = (!YFCCommon.isVoid(sCharge)) ? Double
                .valueOf(sCharge) : 0.00;
                LOGGER.debug(" sChargeCategory ::" + sChargeCategory
                    + " sChargeName ::" + sChargeName + " sIsDisocunt :: "
                    + sIsDiscount + " sChargePerLine ::" + sCharge);
                Element eleChildLineCharge = (Element) XPathAPI
                    .selectSingleNode(eleChildLine,
                        "LineCharges/LineCharge" + "[@ChargeCategory='"
                            + sChargeCategory
                            + "' and @ChargeName='" + sChargeName
                            + "']");
                if (YFCCommon.isVoid(eleChildLineCharge)) {
                  LOGGER.debug(" Inside if YFCCommon.isVoid(eleChildLineCharge)");
                  eleChildLineCharge = GCXMLUtil.createElement(inDoc,
                      GCConstants.LINE_CHARGE, null);
                  eleChildLineCharge.setAttribute(GCConstants.CHARGE_CATEGORY, sChargeCategory);
                  eleChildLineCharge.setAttribute(GCConstants.CHARGE_NAME, sChargeName);
                  if (!YFCCommon.isVoid(sIsDiscount)) {
                    eleChildLineCharge.setAttribute(GCConstants.IS_DISCOUNT, sIsDiscount);
                  }
                  eleLineCharges.appendChild(eleChildLineCharge);
                }

                if (!YFCCommon.isVoid(sChargeCategory)) {
                  sMapKey = sChargeCategory;
                }
                if (!YFCCommon.isVoid(sChargeName)) {
                  sMapKey = sMapKey + sChargeName;
                }


                LOGGER.debug(" bIsHeaderCharge is :" + bIsHeaderCharge);
                /*
                 * Check if charge is greater than 0.00, then do the proration
                 * Put the charge category, charge name as a combination for map
                 * key, It will track charges for the particular key combination
                 * + charge per line
                 */
                if (dCharge != 0.00) {
                  LOGGER.debug(" Inside if(dChargePerLine>0.00)");
                  double dProratedCharge = dCharge * dProrationRatio;
                  BigDecimal proratedCharge = new BigDecimal(dProratedCharge+"").setScale(2, BigDecimal.ROUND_DOWN);
                  
                  eleChildLineCharge.setAttribute(
                      GCConstants.CHARGE_PER_LINE,
                      proratedCharge.toString());
                  LOGGER.debug(" Inside dProratedChargePerLine"
                      + proratedCharge);
                  if (chargeMap.containsKey(sMapKey)) {
                      proratedCharge = proratedCharge.add(chargeMap.get(sMapKey));
                  }

                  chargeMap.put(sMapKey, proratedCharge);
                  dMapCharge = proratedCharge;
                  LOGGER.debug(" Inside dMapChargePerLine" + dMapCharge);
                } else if (bIsHeaderCharge
                    && GCConstants.SHIPPING
                    .equalsIgnoreCase(sChargeCategory)) {
                  LOGGER.debug(" Inside else if it is header charge");
                  eleChildLineCharge.setAttribute(
                      GCConstants.CHARGE_PER_LINE, "0.00");
                }

                // Check if it is the last line
                if ((bIsLastLine && isItemShippable) && dCharge > 0.00) {
                  /*
                   * Check if the total ChargePerLine on the parent line is
                   * still greater than the prorated ChargePerLine then add
                   * all the remaining ChargePerLine on the last line
                   */
                    BigDecimal actualCharge = new BigDecimal(dCharge+"");
                  BigDecimal dVarianceAmount = actualCharge.subtract(dMapCharge);
                  BigDecimal zero = new BigDecimal("0.00");
                  if (dVarianceAmount.compareTo(zero) >0) {
                    LOGGER.debug(" Inside if(dVarianceAmount!=0.00)");
                    BigDecimal dProratedChargePerLine = new BigDecimal(eleChildLineCharge.getAttribute(GCConstants.CHARGE_PER_LINE)).add(dVarianceAmount);
                           
                    LOGGER.debug(" dProratedChargePerLine is "
                        + dProratedChargePerLine);
                    
                    eleChildLineCharge.setAttribute(
                        GCConstants.CHARGE_PER_LINE, dProratedChargePerLine.toString());
                  }
                }
          }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCProcessProrations.prorateCharges()", e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCProcessProrations.prorateCharges() method ");
  }

  /**
   *
   * Description of prorateTaxes Prorate the taxes to the child lines on the
   * basis of proration ratio
   *
   * @param docChangeOrder
   * @param eleChildLine
   * @param eleChangeOrderLine
   * @param eleParentLine
   * @param bIsLastLine
   * @param lineTaxMap
   * @throws Exception
   *
   */
  public static void prorateTaxes(Document inDoc, NodeList nlTax,
      boolean bIsLastLine, Element eleChildLine,
      Map<String, Double> taxMap) throws GCException {
    LOGGER.debug(" Entering GCProcessProrations.prorateTaxes() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" prorateTaxes() method, docChangeOrder is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    try {
      Element eleLineTaxes = (Element) XPathAPI.selectSingleNode(
          eleChildLine, "LineTaxes");
      if (YFCCommon.isVoid(eleLineTaxes)) {
        eleLineTaxes = GCXMLUtil.createElement(inDoc,
            GCConstants.LINE_TAXES, null);
        eleChildLine.appendChild(eleLineTaxes);
      }
      String sMapKey = null;
      double dMapTax = 0.00;
      String sProrationRatio = eleChildLine
          .getAttribute(GCConstants.PRORATION_RATIO);
      double dProrationRatio = (!YFCCommon.isVoid(sProrationRatio)) ? Double
          .valueOf(sProrationRatio) : 0.00;
          LOGGER.debug(" dProrationRatio" + dProrationRatio);
          for (int iCounter = 0; iCounter < nlTax.getLength(); iCounter++) {
            Element eleLineTax = (Element) nlTax.item(iCounter);
			//GCSTORE-7131: Checking if value of Charge Category/Change Name/TaxName is null then set it to blank
			String sChargeCategory = YFCCommon.isVoid(eleLineTax.getAttribute(GCConstants.CHARGE_CATEGORY)) ? "":eleLineTax.getAttribute(GCConstants.CHARGE_CATEGORY);
			String sChargeName = YFCCommon.isVoid(eleLineTax.getAttribute(GCConstants.CHARGE_NAME)) ? "":eleLineTax.getAttribute(GCConstants.CHARGE_NAME);
			String sTaxName = YFCCommon.isVoid(eleLineTax.getAttribute(GCConstants.TAX_NAME)) ? "" : eleLineTax.getAttribute(GCConstants.TAX_NAME);
						
            String sIsDisocunt = eleLineTax
                .getAttribute(GCConstants.IS_DISCOUNT);
            String sTax = eleLineTax.getAttribute(GCConstants.TAX);
            double dTax = (!YFCCommon.isVoid(sTax)) ? Double.valueOf(sTax)
                : 0.00;
            /* -----------OMS-5164 Starts ----------------*/
            String sRef2 = eleLineTax.getAttribute(GCConstants.REFERENCE_2);
            double dRef2 = (!YFCCommon.isVoid(sRef2)) ? Double.valueOf(sRef2)
                : 0.00;
            /* -----------OMS-5164 Ends ----------------*/
            String sTaxPercentage = eleLineTax
                .getAttribute(GCConstants.TAX_PERCENTAGE);
            LOGGER.debug(" sChargeCategory ::" + sChargeCategory
                + " sChargeName ::" + sChargeName + " sTaxName ::"
                + sTaxName + " sIsDisocunt :: " + sIsDisocunt
                + " sTax ::" + sTax + "sTaxPercentage" + sTaxPercentage);

            Element eleChildLineTax = (Element) XPathAPI.selectSingleNode(
                eleChildLine, "LineTaxes/LineTax"
                    + "[@ChargeCategory='" + sChargeCategory
                    + "' and @ChargeName='" + sChargeName + "']");
                //GCSTORE-7131: Set the value of variable 'sMapKey'
				sMapKey = sChargeCategory + sChargeName + sTaxName;

            if (YFCCommon.isVoid(eleChildLineTax)) {
							eleChildLineTax = GCXMLUtil.createElement(inDoc,
									GCConstants.LINE_TAX, null);
							eleLineTaxes.appendChild(eleChildLineTax);

							if (!YFCCommon.isVoid(sChargeCategory)) {
								eleChildLineTax.setAttribute(
										GCConstants.CHARGE_CATEGORY, sChargeCategory);
							}
							if (!YFCCommon.isVoid(sChargeName)) {
								eleChildLineTax.setAttribute(GCConstants.CHARGE_NAME,
										sChargeName);
							}
							if (!YFCCommon.isVoid(sTaxName)) {
								eleChildLineTax.setAttribute(GCConstants.TAX_NAME,
										sTaxName);
							}
							if (!YFCCommon.isVoid(sTaxPercentage)) {
								eleChildLineTax.setAttribute(
										GCConstants.TAX_PERCENTAGE, sTaxPercentage);
							}
							if (!YFCCommon.isVoid(sIsDisocunt)) {
								eleChildLineTax.setAttribute(GCConstants.IS_DISCOUNT,
										sIsDisocunt);
							}

							//GCSTORE-6546  Added Tax/Reference_2 attribute on the element -- START
							eleChildLineTax.setAttribute(GCConstants.TAX,
									"0.00");
							eleChildLineTax.setAttribute(GCConstants.REFERENCE_2,
									"0.00");
							//GCSTORE-6546  Added Tax attribute on the element END

						} 
						LOGGER.debug(" sMapKey" + sMapKey);
            /*
             * Check if Tax is greater than 0.00, then do the proration Put
             * the charge category, charge name, tax name as a combination
             * for map key, It will track taxes for the particular key
             * combination
             */
        if (dTax != 0.00) {
              String attrToBeSet = GCConstants.TAX;
              prorateGivenItem(bIsLastLine, taxMap, sMapKey, dProrationRatio, dTax,
                  eleChildLineTax, attrToBeSet);
            }

            if (dRef2 > 0.00) {
              String attrToBeSet = GCConstants.REFERENCE_2;
              sMapKey = sMapKey + "_REF";
              prorateGivenItem(bIsLastLine, taxMap, sMapKey, dProrationRatio, dRef2,
                  eleChildLineTax, attrToBeSet);
            }
          }
    
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCProcessProrations.prorateTaxes(), Exception is :",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCProcessProrations.prorateTaxes() method ");
  }

  /**
   * This method is used to prorated the Value passed(dValueToBeProrated).
   * Defect fix: OMS-5164
   * @param bIsLastLine
   *            - Tells it the childLine element is the last childLine of the
   *            parent Element
   * @param valueMap
   *            - Map contains the prorated value.
   * @param sMapKey
   *            - reference key or Tax key
   * @param dProrationRatio
   *            - Proration Ratio
   * @param dValueToBeProrated
   *            - the value that needs to be prorated.
   * @param eleChildLineTax
   *            - eleChildLineTax
   * @param attrToBeSet
   *            - refernce 2 or Tax
   */
  private static void prorateGivenItem(boolean bIsLastLine, Map<String, Double> valueMap,
      String sMapKey, double dProrationRatio, double dValueToBeProrated,
      Element eleChildLineTax, String attrToBeSet) {
//7131- removing dMapValue = 0.00;    

    double dProratedVal = dValueToBeProrated * dProrationRatio;
    dProratedVal = GCCommonUtil.splitNumberForDecimalPrecision(dProratedVal, 2)[0];
    eleChildLineTax.setAttribute(attrToBeSet, String.valueOf(dProratedVal));

    if(GCConstants.TAX.equals(attrToBeSet)) {
      Element lineDetailEle = (Element) eleChildLineTax.getParentNode().getParentNode();
      String sTax = lineDetailEle.getAttribute(GCConstants.TAX);
      Double dTax = (!YFCCommon.isVoid(sTax)) ? Double.valueOf(sTax)
          : 0.00;
      dTax = dTax + dProratedVal;
      lineDetailEle.setAttribute(GCConstants.TAX, String.valueOf(dTax));
    }

    if(LOGGER.isDebugEnabled()){
      LOGGER.debug(" dProratedTax " + dProratedVal);
    }
    //7131- adding dMapValue
      double dMapValue = 0.00;
    if (valueMap.containsKey(sMapKey)) {
      dProratedVal = dProratedVal + valueMap.get(sMapKey);
    }
    LOGGER.debug(" dProratedTax into Map " + dProratedVal);
    valueMap.put(sMapKey, dProratedVal);

    dMapValue = dProratedVal;
    /*
     * Check if it is the last line and the Total Tax on the parent line is
     * still greater than the prorated tax then add all the remaining tax on
     * the last line
     */
    if (bIsLastLine) {
      double dVarianceAmount = dValueToBeProrated - dMapValue;
      if ((dVarianceAmount) != 0.00) {
        LOGGER.debug(" Inside if(dVarianceAmount)!=0.00");
        dProratedVal = Double.valueOf(eleChildLineTax.getAttribute(attrToBeSet))
            + (dVarianceAmount);
        dProratedVal = GCCommonUtil.splitNumberForDecimalPrecision(dProratedVal, 2)[0];
        LOGGER.debug(" dProratedTax is " + dProratedVal);
        eleChildLineTax.setAttribute(attrToBeSet, String.valueOf(dProratedVal));
      }
    }
  }

  /**
   *
   * Description of zeroingHeaderShippingCharges Creating the change order
   * document and zeroing out the header level shipping charges
   *
   * @param docChangeOrder
   * @return
   * @throws Exception
   *
   */
  private static void zeroingHeaderCharges(Document inDoc) throws GCException {
    LOGGER.debug(" Entering GCProcessProrations.zeroingHeaderShippingCharges() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" zeroingHeaderShippingCharges() method, in inDoc is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    try {

      NodeList nlHeaderCharge = XPathAPI.selectNodeList(inDoc,
          "//HeaderCharges/HeaderCharge");
      for (int iChargeCounter = 0; iChargeCounter < nlHeaderCharge
          .getLength(); iChargeCounter++) {
        Element eleHeaderCharge = (Element) nlHeaderCharge
            .item(iChargeCounter);
        eleHeaderCharge.setAttribute(GCConstants.CHARGE_AMOUNT, "0.00");
      }

      NodeList nlHeaderTax = XPathAPI.selectNodeList(inDoc,
          "//HeaderTaxes/HeaderTax");
      for (int iTaxCounter = 0; iTaxCounter < nlHeaderTax.getLength(); iTaxCounter++) {
        Element eleHeaderTax = (Element) nlHeaderTax.item(iTaxCounter);
        eleHeaderTax.setAttribute(GCConstants.TAX, "0.00");
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCProcessProrations.zeroingHeaderShippingCharges(), Exception is :",
          e);
      throw new GCException(e);
    }
  }

  /**
   *
   * Description of calculateProrationRatio Calculate Proration ratio with
   * below formulae If TotalBillableWeight > 0 proration ratio =
   * Item.BillableWeight / TotalBillableWeight Else Proration ratio = Item.Qty
   * / TotalLineQuantity
   *
   * @param dTotalBillableWeight
   * @param dItemBillableWeight
   * @param dTotalQty
   * @param dOrderLineQty
   * @return
   * @throws Exception
   *
   */
  private static double calculateProrationRatio(double dTotalBillableWeight,
      double dItemBillableWeight, double dTotalQty, double dOrderLineQty)
          throws GCException {
    LOGGER.debug(" Entering GCProcessProrations.calculateProrationRatio() method ");
    LOGGER.debug(" dTotalBillableWeight ::" + dTotalBillableWeight
        + " dItemBillableWeight ::" + dItemBillableWeight
        + " dTotalQty" + dTotalQty + " dOrderLineQty" + dOrderLineQty);

    double dProrationRatio = 0.00;
    try {
      if (dTotalBillableWeight > 0.00) {
        LOGGER.debug(" Inside If Total Billable Weight is > 0");
        dProrationRatio = dItemBillableWeight / dTotalBillableWeight;
      } else {
        LOGGER.debug(" Inside ELSE Total Billable Weight is > 0");
        dProrationRatio = dOrderLineQty / dTotalQty;
      }
      // Setting prorationRatio as zero, in case ProrationRatio is calculated as Nan : Add-on done in already provided fix of 5378
      if(Double.isNaN(dProrationRatio)){
    	  dProrationRatio = 0.0;
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCProcessProrations.calculateProrationRatio()",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" dProrationRatio is " + dProrationRatio);
    LOGGER.debug(" Exiting GCProcessProrations.calculateProrationRatio() method ");
    return dProrationRatio;
  }

  /**
   *
   * Description of calculateTotalBillableWeight Calculate Total Billable
   * weight for all items as Billable Weight * Qty (Item 1) + ...... +
   * Billable Weight * Qty (Item (n))
   *
   * @param env
   * @param inDoc
   * @return
   * @throws Exception
   *
   */
  private static String calculateTotalBillableWeight(YFSEnvironment env,
      Document inDoc) throws GCException {
    LOGGER.debug(" Entering GCProcessProrations.calculateTotalBillableWeight() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" calculateTotalBillableWeight() method, inDoc is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    double dItemTotalBillableWeight = 0.00;
    try {
      NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc,
          "Order/OrderLines/OrderLine");
      for (int iCounter = 0; iCounter < nlOrderLine.getLength(); iCounter++) {
        Element eleOrderLine = (Element) nlOrderLine.item(iCounter);
        String sExtnIsFreeGiftItem = GCXMLUtil.getAttributeFromXPath(
            eleOrderLine, "Extn/@ExtnIsFreeGiftItem");
        Element eleBundleParentLine = (Element) XPathAPI
            .selectSingleNode(eleOrderLine, "BundleParentLine");
        if (YFCCommon.isVoid(eleBundleParentLine)
            && !GCConstants.YES.equals(sExtnIsFreeGiftItem)) {
          String sItemID = GCXMLUtil.getAttributeFromXPath(
              eleOrderLine, "Item/@ItemID");
          String sUOM = GCXMLUtil.getAttributeFromXPath(eleOrderLine,
              "Item/@UnitOfMeasure");
          Document docGetItemListOP = GCCommonUtil.getItemList(env,
              sItemID, sUOM, GCConstants.GCI);

          String sBillableWeight = GCXMLUtil.getAttributeFromXPath(
              docGetItemListOP,
              "ItemList/Item/Extn/@ExtnBillableWeight");
          double dBillableWeight = (!YFCCommon
              .isVoid(sBillableWeight) ? Double
                  .valueOf(sBillableWeight) : 0.00);
          String sKitCode = GCXMLUtil.getAttributeFromXPath(
              docGetItemListOP,
              "ItemList/Item/PrimaryInformation/@KitCode");
          if (GCConstants.BUNDLE.equalsIgnoreCase(sKitCode)
              && dBillableWeight == 0.00) {
            // If billable weight of parent line is 0, then
            // calculate billable weight of components
            NodeList nlComponent = XPathAPI.selectNodeList(
                docGetItemListOP,
                "ItemList/Item/Components/Component");
            for (int iCount = 0; iCount < nlComponent.getLength(); iCount++) {
              Element eleComponent = (Element) nlComponent
                  .item(iCount);
              String sCompItemID = eleComponent
                  .getAttribute(GCConstants.COMPONENT_ITEM_ID);
              String sCompUOM = eleComponent
                  .getAttribute(GCConstants.COMPONENT_UOM);
              String sKitQuantity = GCXMLUtil
                  .getAttributeFromXPath(
                      docGetItemListOP,
                      "ItemList/Item/Components/"
                          + "Component[@ComponentItemID='"
                          + sCompItemID
                          + "']/@KitQuantity");
              double dKitQuantity = (!YFCCommon
                  .isVoid(sKitQuantity) ? Double
                      .valueOf(sKitQuantity) : 0.00);

              Document docGetCompItemListOP = GCCommonUtil
                  .getItemList(env, sCompItemID, sCompUOM,
                      GCConstants.GCI);
              String sCompBillableWeight = GCXMLUtil
                  .getAttributeFromXPath(
                      docGetCompItemListOP,
                      "ItemList/Item/Extn/@ExtnBillableWeight");
              double dCompBillableWeight = (!YFCCommon
                  .isVoid(sCompBillableWeight) ? Double
                      .valueOf(sCompBillableWeight) : 0.00);

              dBillableWeight = dBillableWeight
                  + (dCompBillableWeight * dKitQuantity);
            }
          }

          // Phase-2 changes for quantity to ship - starts
          double dOrderLineQty;
          if (eleOrderLine.hasAttribute("QuantityToShip")) {
            String sQtyToship = eleOrderLine.getAttribute("QuantityToShip");
            dOrderLineQty = (!YFCCommon.isVoid(sQtyToship) ? Double.valueOf(sQtyToship) : 0.00);
          } else {
            String sOrderLineQty = eleOrderLine
                .getAttribute(GCConstants.ORDERED_QTY);
            dOrderLineQty = (!YFCCommon.isVoid(sOrderLineQty) ? Double
                .valueOf(sOrderLineQty) : 0.00);
          }
          dItemTotalBillableWeight = dItemTotalBillableWeight
              + (dBillableWeight * dOrderLineQty);
        }
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCProcessProrations.calculateTotalBillableWeight()",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" dItemTotalBillableWeight " + dItemTotalBillableWeight);
    LOGGER.debug(" Exiting GCProcessProrations.calculateTotalBillableWeight() method ");
    return String.valueOf(dItemTotalBillableWeight);
  }

  private static String caclulateTotalQuantities(Document inDoc)
      throws GCException {
    double dTotalQty = 0.00;
    try {
      NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc,
          "Order/OrderLines/OrderLine");
      for (int iCounter = 0; iCounter < nlOrderLine.getLength(); iCounter++) {
        Element eleOrderLine = (Element) nlOrderLine.item(iCounter);
        String sExtnIsFreeGiftItem = GCXMLUtil.getAttributeFromXPath(
            eleOrderLine, "Extn/@ExtnIsFreeGiftItem");
        Element eleBundleParentLine = (Element) XPathAPI
            .selectSingleNode(eleOrderLine, "BundleParentLine");
        if (YFCCommon.isVoid(eleBundleParentLine)
            && !GCConstants.YES.equals(sExtnIsFreeGiftItem)) {
          // Phase-2 changes for quantity to ship - starts

          if (eleOrderLine.hasAttribute("QuantityToShip")) {
            String sQtyToship = eleOrderLine.getAttribute("QuantityToShip");
            double dQtyToship = (!YFCCommon.isVoid(sQtyToship) ? Double.valueOf(sQtyToship) : 0.00);
            dTotalQty = dTotalQty + dQtyToship;
          } else {
            String sOrderedQty = eleOrderLine
                .getAttribute(GCConstants.ORDERED_QTY);
            double dOrderedQty = (!YFCCommon.isVoid(sOrderedQty) ? Double
                .valueOf(sOrderedQty) : 0.00);
            dTotalQty = dTotalQty + dOrderedQty;
          }
          // Phase-2 changes for quantity to ship - ends
        }
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCProcessProrations.caclulateTotalQuantities()",
          e);
      throw new GCException(e);
    }
    return String.valueOf(dTotalQty);
  }

  /**
   * @author Infosys Ltd.
   *
   * OMS 5467: Add logic to flip the sign on the unit price for return orders
   *
   *
   * @param docInXML
   * @throws TransformerException
   */

  private static void updateReturnOrderUnitPrice(Document docInXML) throws TransformerException {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(CLASS_NAME + "updateReturnOrderUnitPriceMethod" + docInXML);
    }

    NodeList nlLineDetail = XPathAPI.selectNodeList(docInXML, "/InvoiceDetail/InvoiceHeader/LineDetails/LineDetail");

    final int iLineDetailCounter = nlLineDetail.getLength();

    for (int iLineDetail = 0; iLineDetail < iLineDetailCounter; iLineDetail++) {

      Element eleLineDetail = (Element) nlLineDetail.item(iLineDetail);
      Element eleLinePriceInfo = (Element) XPathAPI.selectSingleNode(eleLineDetail, "OrderLine/LinePriceInfo");

      // Check LinePriceInfo is not blank
      if (!YFCCommon.isVoid(eleLinePriceInfo)) {

        String sUnitPrice = eleLinePriceInfo.getAttribute(GCConstants.UNIT_PRICE);

        // Check Unit Price is not null
        if (!YFCCommon.isVoid(sUnitPrice)) {

          Double dUnitPrice = Double.valueOf(sUnitPrice);

          // Check if Price is greater than 0
          if (dUnitPrice > 0) {
            Double dNegUnitPrice = -dUnitPrice;

            if (LOGGER.isDebugEnabled()) {
              LOGGER.debug(CLASS_NAME + "Negating the unit price for line" + iLineDetailCounter + dUnitPrice
                  + dNegUnitPrice);
            }
            eleLinePriceInfo.setAttribute(GCConstants.UNIT_PRICE, String.valueOf(dNegUnitPrice));
          }
        }
      }
    }
  }


  /**
   * <h3>Fix for 5010 : This method takes item type, IsHeaderCharge and charge category
   *
   * @param dBillableWeight - Billable weight check
   * @param bIsHeaderCharge - boolean attribute to check if it is header charge
   * @param sChargeCategory - Charge Category on the header charges
   *
   * @return boolean attribute if lines are shippable or not.
   *
   */

  private static boolean checkForShippableLines(final double dBillableWeight, final String sChargeCategory,
      final boolean bIsHeaderCharge) {

    boolean bIsShippable = true;

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(CLASS_NAME + "Fix 5010 : Inside checkForShippableLine function : Shippable flag is"
          + dBillableWeight + sChargeCategory + bIsHeaderCharge);
    }

    if ((!(dBillableWeight > 0.0))
        && (GCConstants.SHIPPING.equals(sChargeCategory)) && (bIsHeaderCharge)) {

      bIsShippable = false;

      if (LOGGER.isDebugEnabled()) {
        LOGGER
        .debug(CLASS_NAME + "Fix 5010 : Inside checkForShippableLine function : Shippable flag is" + bIsShippable);
      }
    }

    return bIsShippable;
  }

  @Override
  public void setProperties(Properties arg0) {
    properties = arg0;
  }
}
