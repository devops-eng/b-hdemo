package com.gc.api;

import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCProcessStopShipmentRequest implements YIFCustomApi {
    YFSEnvironment env = null;
    String sOrderReleaseKey = "";

    private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCProcessStopShipmentRequest.class.getName());

    public void setProperties(Properties arg0) throws GCException {

    }

    public Document processStopShipmentRequest(YFSEnvironment env,
	    Document inDoc) throws GCException {
	try {
	    final String logInfo = "GCGetPickRequestList :: getPickRequestList :: ";
	    LOGGER.verbose(logInfo + "Begin");

	    LOGGER.debug(logInfo + "Input Document"
		    + GCXMLUtil.getXMLString(inDoc));

	    this.env = env;

	    Document docProcessStopShipment = GCXMLUtil
		    .createDocument("OrderReleaseDetail");
	    Element eleProcessStopShipmentRoot = docProcessStopShipment
		    .getDocumentElement();
	    eleProcessStopShipmentRoot.setAttribute("ResponseCode",
		    GCConstants.SUCCESS_CODE);

	    return docProcessStopShipment;
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch block of GCProcessStopShipmentRequest.processStopShipmentRequest(), Exception is :",
		    e);
	    throw new GCException(e);
	}
    }
}
