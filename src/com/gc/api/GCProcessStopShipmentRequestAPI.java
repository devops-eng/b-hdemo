/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Preparing the request XML for stop shipment and sending it to TIBCO
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
              1.0              13/05/2014        Singh, Gurpreet            Stop Shipment
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.util.Properties;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:gurpreet.singh@expicient.com">Gurpreet</a>
 */
public class GCProcessStopShipmentRequestAPI implements YIFCustomApi {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCProcessStopShipmentRequestAPI.class);

    /**
     * 
     * Description of processStopShipmentRequest Preparing the request XML for
     * stop shipment and change order status to Stop Shipment Requested
     * 
     * @param env
     * @param inDoc
     * @throws GCException
     * 
     */
    public static Document processStopShipmentRequest(YFSEnvironment env,
	    Document inDoc) throws GCException {
	LOGGER.debug(" Entering GCProcessStopShipmentRequestAPI.processStopShipmentRequest() method ");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" Input document is" + GCXMLUtil.getXMLString(inDoc));
	}
	Document docGetOrderReleaseDetailOP = null;
	try {
	    Element eleRootOfInDoc = inDoc.getDocumentElement();
	    String sOrderNo = eleRootOfInDoc.getAttribute(GCConstants.ORDER_NO);
	    String sReleaseNo = eleRootOfInDoc
		    .getAttribute(GCConstants.RELEASE_NO);

	    LOGGER.debug(" sOrderNo " + sOrderNo + " sReleaseNo " + sReleaseNo);

	    Document docGetOrderReleaseDetailTmp = GCCommonUtil
		    .getDocumentFromPath("/global/template/api/getOrderReleaseDetail.xml");
	    docGetOrderReleaseDetailOP = GCCommonUtil.invokeAPI(env,
		    GCConstants.API_GET_ORDER_RELEASE_DETAILS, inDoc,
		    docGetOrderReleaseDetailTmp);
	    Element eleGetOrderReleaseDetail = docGetOrderReleaseDetailOP
		    .getDocumentElement();
	    eleGetOrderReleaseDetail.setAttribute(GCConstants.ACTION, "Cancel");
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug(" docGetOrderReleaseDetailOP "
			+ GCXMLUtil.getXMLString(docGetOrderReleaseDetailOP));
	    }
	    Document docChangeOrderStatus = GCProcessStopShipmentResponseAPI
		    .prepareChangeOrderStatusHdrDoc(eleGetOrderReleaseDetail,
			    GCConstants.CHANGE_RELEASE_STATUS_TRAN_ID);
	    Element eleOrderLinesChngStatus = GCXMLUtil.createElement(
		    docChangeOrderStatus, GCConstants.ORDER_LINES, null);
	    docChangeOrderStatus.getDocumentElement().appendChild(
		    eleOrderLinesChngStatus);

	    NodeList nlOrderLine = XPathAPI.selectNodeList(
		    docGetOrderReleaseDetailOP, "OrderRelease/OrderLine");
	    for (int iCounter = 0; iCounter < nlOrderLine.getLength(); iCounter++) {
		LOGGER.debug(" Inside looping on release order lines ");
		Element eleOrderLine = (Element) nlOrderLine.item(iCounter);
		String sStatusQuantity = eleOrderLine
			.getAttribute(GCConstants.STATUS_QUANTITY);
		LOGGER.debug(" StatusQuantity " + sStatusQuantity);

		GCProcessStopShipmentResponseAPI
			.addOrderLinesToChangeOrderStatus(docChangeOrderStatus,
				eleOrderLinesChngStatus, eleOrderLine,
				GCConstants.STOP_SHIPMENT_REQUESTED_STATUS,
				sStatusQuantity, sReleaseNo);
	    }
	    LOGGER.debug(" updated docChangeOrderStatus "
		    + GCXMLUtil.getXMLString(docChangeOrderStatus));
	    NodeList nlChangeOrderLine = XPathAPI.selectNodeList(
		    docChangeOrderStatus,
		    "OrderStatusChange/OrderLines/OrderLine");
	    if (nlChangeOrderLine.getLength() > 0) {
		LOGGER.debug(" Inside if to fire Change Order Status ");
		GCCommonUtil.invokeAPI(env,
			GCConstants.API_CHANGE_ORDER_STATUS,
			docChangeOrderStatus);
	    }
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch block of GCProcessStopShipmentRequestAPI.processStopShipmentRequest(), Exception is :",
		    e);
	    throw new GCException(e);
	}
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" docGetOrderReleaseDetailOP is"
		    + GCXMLUtil.getXMLString(docGetOrderReleaseDetailOP));
	}
	LOGGER.debug(" Exiting GCProcessStopShipmentRequestAPI.processStopShipmentRequest() method ");
	return docGetOrderReleaseDetailOP;
    }

    public void setProperties(Properties arg0) throws GCException {

    }

}
