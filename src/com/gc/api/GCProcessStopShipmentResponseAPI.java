/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Contains the logic of receiving the stop shipment response and cancelling the release within OMS
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               13/05/2014        Singh, Gurpreet            Stop Shipment
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.util.Properties;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:gurpreet.singh@expicient.com">Gurpreet</a>
 */
public class GCProcessStopShipmentResponseAPI implements YIFCustomApi {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCProcessStopShipmentResponseAPI.class);

    /**
     * 
     * Description of processStopShipmentResponse
     * 
     * @param env
     * @param inDoc
     * @return
     * @throws GCException
     * 
     */
    public Document processStopShipmentResponse(YFSEnvironment env,
	    Document inDoc) throws GCException {
	LOGGER.debug(" Entering GCProcessStopShipmentResponseAPI.processStopShipmentResponse() method ");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" Input document is" + GCXMLUtil.getXMLString(inDoc));
	}
	try {
	    Element eleRootOfInDoc = inDoc.getDocumentElement();
	    String sOrderNo = eleRootOfInDoc
		    .getAttribute(GCConstants.SALES_ORDER_NO);
	    String sReleaseNo = eleRootOfInDoc
		    .getAttribute(GCConstants.RELEASE_NO);
	    String sStatus = eleRootOfInDoc.getAttribute(GCConstants.STATUS);
	    LOGGER.debug(" sOrderNo " + sOrderNo + " sReleaseNo " + sReleaseNo
		    + " sStatus " + sStatus);

      //        Commenting the below code as a part of defect fix no. 5119
      //------------------------------------------------------------------------
      //
      //-------------------------------------------------------------------------

      Document docGetOrderReleaseDetailOP = fetchGetOrderReleaseDetail(
          env, eleRootOfInDoc, sOrderNo, sReleaseNo);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(" docGetOrderReleaseDetailOP "
            + GCXMLUtil.getXMLString(docGetOrderReleaseDetailOP));
      }
      boolean bIsOnlyChangeOrderStatusReq = false;
      if (GCConstants.SUCCESS.equalsIgnoreCase(sStatus)) {
        performOrderChanges(env, docGetOrderReleaseDetailOP,
            bIsOnlyChangeOrderStatusReq, sReleaseNo);
      } else if (GCConstants.FAILURE.equalsIgnoreCase(sStatus)) {
        bIsOnlyChangeOrderStatusReq = true;
        performOrderChanges(env, docGetOrderReleaseDetailOP,
            bIsOnlyChangeOrderStatusReq, sReleaseNo);
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCProcessStopShipmentResponseAPI.processStopShipmentResponse(), Exception is :",
          e);
      throw new GCException(e);
    }
    return inDoc;
  }

    /**
     * 
     * Description of fetchGetOrderReleaseDetail Fetch getOrderReleaseDetail and
     * return the output
     * 
     * @param env
     * @param eleOrder
     * @param sOrderNo
     * @param sReleaseNo
     * @return
     * @throws GCException
     * 
     */
    public static Document fetchGetOrderReleaseDetail(YFSEnvironment env,
	    Element eleOrder, String sOrderNo, String sReleaseNo)
	    throws GCException {
	LOGGER.debug(" Entering GCProcessStopShipmentResponseAPI.fetchGetOrderReleaseDetail() method ");
	LOGGER.debug(" sOrderNo " + sOrderNo + " sReleaseNo " + sReleaseNo);
	Document docGetOrderReleaseListOP = null;
	try {
	    String sEnterpriseCode = eleOrder
		    .getAttribute(GCConstants.ENTERPRISE_CODE);
	    String sDocumentType = eleOrder
		    .getAttribute(GCConstants.DOCUMENT_TYPE);
	    Document docGetOrderReleaseDetailIP = GCXMLUtil
		    .createDocument(GCConstants.ORDER_RELEASE_DETAIL);
	    Element eleGetOrderRelease = docGetOrderReleaseDetailIP
		    .getDocumentElement();
	    eleGetOrderRelease.setAttribute(GCConstants.ORDER_NO, sOrderNo);
	    eleGetOrderRelease.setAttribute(GCConstants.RELEASE_NO, sReleaseNo);
	    eleGetOrderRelease.setAttribute(GCConstants.ENTERPRISE_CODE,
		    sEnterpriseCode);
	    eleGetOrderRelease.setAttribute(GCConstants.DOCUMENT_TYPE,
		    sDocumentType);

	    Document docGetOrderReleaseDetailTmp = GCCommonUtil
		    .getDocumentFromPath("/global/template/api/getOrderReleaseDetail.xml");

	    docGetOrderReleaseListOP = GCCommonUtil.invokeAPI(env,
		    GCConstants.API_GET_ORDER_RELEASE_DETAILS,
		    docGetOrderReleaseDetailIP, docGetOrderReleaseDetailTmp);
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch block of GCProcessStopShipmentResponseAPI.fetchGetOrderReleaseDetail(), Exception is :",
		    e);
	    throw new GCException(e);
	}
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" fetchGetOrderReleaseDetail() method, docGetOrderReleaseListOP is"
		    + GCXMLUtil.getXMLString(docGetOrderReleaseListOP));
	}
	LOGGER.debug(" Exiting GCProcessStopShipmentResponseAPI.fetchGetOrderReleaseDetail() method ");
	return docGetOrderReleaseListOP;
    }

    /**
     * 
     * Description of performOrderChanges This method will perform the below
     * API's - Invoke changeRelease API to backorder the complete quantity -
     * Invoke changeOrder API to apply �Shipment Cancellation Hold� on the order
     * line - Invoke changeOrderStatus API to change the order status to
     * �Shipment Stopped� status
     * 
     * @param env
     * @param docGetOrderReleaseDetailOP
     * @param bIsOnlyChangeOrderStatusReq
     * @return
     * @throws Exception
     * 
     */
    private static void performOrderChanges(YFSEnvironment env,
	    Document docGetOrderReleaseDetailOP,
	    boolean bIsOnlyChangeOrderStatusReq, String sReleaseNo)
	    throws GCException {
	LOGGER.debug(" Entering GCProcessStopShipmentResponseAPI.performOrderChanges() method ");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" docGetOrderReleaseDetailOP is"
		    + GCXMLUtil.getXMLString(docGetOrderReleaseDetailOP));
	}
	LOGGER.debug(" bIsOnlyChangeOrderStatusReq is"
		+ bIsOnlyChangeOrderStatusReq);
	try {
	    Element eleGetOrderReleaseDetailDoc = docGetOrderReleaseDetailOP
		    .getDocumentElement();
	    Document docChangeReleaseIP = null;
	    Document docChangeOrderIP = null;
	    Element eleChgReleaseOrderLines = null;
	    Element eleChgOrderLines = null;

	    // if it bIsOnlyChangeOrderStatusReq is false, fire changeRelease
	    // and changeOrder API's also
	    // it will be true only when the response is 'Failure'
	    if (!bIsOnlyChangeOrderStatusReq) {
		docChangeReleaseIP = prepareChangeReleaseHdrDoc(eleGetOrderReleaseDetailDoc);
		eleChgReleaseOrderLines = GCXMLUtil.createElement(
			docChangeReleaseIP, GCConstants.ORDER_LINES, null);
		docChangeReleaseIP.getDocumentElement().appendChild(
			eleChgReleaseOrderLines);

		docChangeOrderIP = prepareChangeOrderHdrDoc(env, eleGetOrderReleaseDetailDoc);
		eleChgOrderLines = GCXMLUtil.createElement(docChangeOrderIP,
			GCConstants.ORDER_LINES, null);
		docChangeOrderIP.getDocumentElement().appendChild(
			eleChgOrderLines);
	    }
	    Document docChangeOrderStatus = prepareChangeOrderStatusHdrDoc(
		    eleGetOrderReleaseDetailDoc,
		    GCConstants.STOP_SHIPMENT_TRAN_ID);
	    Element eleOrderLinesChngStatus = GCXMLUtil.createElement(
		    docChangeOrderStatus, GCConstants.ORDER_LINES, null);
	    docChangeOrderStatus.getDocumentElement().appendChild(
		    eleOrderLinesChngStatus);

	    NodeList nlReleaseLine = XPathAPI.selectNodeList(
		    docGetOrderReleaseDetailOP, "OrderRelease/OrderLine");
	    for (int iCounter = 0; iCounter < nlReleaseLine.getLength(); iCounter++) {
		Element eleReleaseLine = (Element) nlReleaseLine.item(iCounter);
		String sStatusQuantity = eleReleaseLine
			.getAttribute(GCConstants.STATUS_QUANTITY);
		LOGGER.debug(" StatusQuantity " + sStatusQuantity);
		String sBaseDropStatus = null;
		if (!bIsOnlyChangeOrderStatusReq) {
		    addOrderLinesToChangeRelease(docChangeReleaseIP,
			    eleChgReleaseOrderLines, eleReleaseLine);
		    addOrderLinesToChangeOrder(docChangeOrderIP,
			    eleChgOrderLines, eleReleaseLine);
		    sBaseDropStatus = GCConstants.SHIPMENT_STOPPED_STATUS;
		    sReleaseNo = "";
		} else {
		    sBaseDropStatus = GCConstants.STOP_SHIPMENT_FAILED_STATUS;
		}
		addOrderLinesToChangeOrderStatus(docChangeOrderStatus,
			eleOrderLinesChngStatus, eleReleaseLine,
			sBaseDropStatus, sStatusQuantity, sReleaseNo);
	    }
      /* fix for OMS 5294 - Begin */

      if (!bIsOnlyChangeOrderStatusReq) {
        NodeList nlBundleParentLine = XPathAPI.selectNodeList(
            docGetOrderReleaseDetailOP,
            "OrderRelease/BundleParentOrderLine");
        int numOfBundleLines = nlBundleParentLine.getLength();
        for (int iCounter = 0; iCounter < numOfBundleLines; iCounter++) {
          Element eleBundleLine = (Element) nlBundleParentLine
              .item(iCounter);


          addOrderLinesToChangeOrder(docChangeOrderIP,
              eleChgOrderLines,eleBundleLine );

        }
      }

      /* fix for OMS 5294 - End */
	    if (!bIsOnlyChangeOrderStatusReq) {
		NodeList nlChangeRelease = XPathAPI
			.selectNodeList(docChangeReleaseIP,
				"OrderRelease/OrderLines/OrderLine");
		if (nlChangeRelease.getLength() > 0) {
		    GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_RELEASE,
			    docChangeReleaseIP);
		}

		NodeList nlChangeOrder = XPathAPI.selectNodeList(
			docChangeOrderIP, "Order/OrderLines/OrderLine");
		if (nlChangeOrder.getLength() > 0) {
		    GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER,
			    docChangeOrderIP);
		}
	    }
	    NodeList nlChangeOrderLine = XPathAPI.selectNodeList(
		    docChangeOrderStatus,
		    "OrderStatusChange/OrderLines/OrderLine");
	    if (nlChangeOrderLine.getLength() > 0) {
		GCCommonUtil.invokeAPI(env,
			GCConstants.API_CHANGE_ORDER_STATUS,
			docChangeOrderStatus);
	    }

	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch block of GCProcessStopShipmentResponseAPI.performOrderChanges(), Exception is :",
		    e);
	    throw new GCException(e);
	}
	LOGGER.debug(" Exiting GCProcessStopShipmentResponseAPI.performOrderChanges() method ");
    }

  /**
   *
   * Description of prepareChangeReleaseHdrDoc Prepare changeRelease Header
   * Document
   *
   * @param eleRootOfInDoc
   * @return
   * @throws Exception
   *
   */
  private static Document prepareChangeReleaseHdrDoc(
      Element eleGetOrderReleaseDetail) throws GCException {
    LOGGER.debug(" Entering GCProcessStopShipmentResponseAPI.prepareChangeReleaseHdrDoc() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" eleGetOrderReleaseDetail is"
          + GCXMLUtil.getElementXMLString(eleGetOrderReleaseDetail));
    }
    Document docChangeReleaseIP = null;
    try {

      docChangeReleaseIP = GCXMLUtil
          .createDocument(GCConstants.ORDER_RELEASE);
      Element eleChangeRelease = docChangeReleaseIP.getDocumentElement();
      eleChangeRelease.setAttribute(GCConstants.ACTION,
          GCConstants.MODIFY);
      eleChangeRelease.setAttribute(GCConstants.ORDER_NO, eleGetOrderReleaseDetail
          .getAttribute(GCConstants.SALES_ORDER_NO));
      eleChangeRelease.setAttribute(GCConstants.ENTERPRISE_CODE,
          eleGetOrderReleaseDetail
          .getAttribute(GCConstants.ENTERPRISE_CODE));
      eleChangeRelease.setAttribute(GCConstants.DOCUMENT_TYPE,
          GCConstants.SALES_ORDER_DOCUMENT_TYPE);
      eleChangeRelease.setAttribute(GCConstants.RELEASE_NO,
          eleGetOrderReleaseDetail
          .getAttribute(GCConstants.RELEASE_NO));
      eleChangeRelease.setAttribute(GCConstants.SHIP_NODE,
          eleGetOrderReleaseDetail
          .getAttribute(GCConstants.SHIP_NODE));
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCProcessStopShipmentResponseAPI.prepareChangeReleaseHdrDoc(), Exception is :",
          e);
      throw new GCException(e);
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" prepareChangeReleaseHdrDoc(), docChangeOrderStatus is"
          + GCXMLUtil.getXMLString(docChangeReleaseIP));
    }
    LOGGER.debug(" Exiting GCProcessStopShipmentResponseAPI.prepareChangeReleaseHdrDoc() method ");
    return docChangeReleaseIP;
  }

    /**
     * 
     * Description of prepareChangeOrderHdrDoc Prepare changeOrder Header
     * Document
     * 
     * @param eleGetOrderReleaseDetail
     * @return
     * @throws Exception
     * 
     */
    private static Document prepareChangeOrderHdrDoc(YFSEnvironment env,
	    Element eleGetOrderReleaseDetail) throws GCException {
	LOGGER.debug(" Entering GCProcessStopShipmentResponseAPI.prepareChangeOrderHdrDoc() method ");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" eleGetOrderReleaseDetail is"
		    + GCXMLUtil.getElementXMLString(eleGetOrderReleaseDetail));
	}
	Document docChangeOrderIP = null;
	try {
	    docChangeOrderIP = GCXMLUtil.createDocument(GCConstants.ORDER);
	    Element eleChangeOrder = docChangeOrderIP.getDocumentElement();
	    eleChangeOrder.setAttribute(GCConstants.ORDER_NO,
		    eleGetOrderReleaseDetail
			    .getAttribute(GCConstants.SALES_ORDER_NO));
	    eleChangeOrder.setAttribute(GCConstants.ENTERPRISE_CODE,
		    eleGetOrderReleaseDetail
			    .getAttribute(GCConstants.ENTERPRISE_CODE));
	    eleChangeOrder.setAttribute(GCConstants.DOCUMENT_TYPE,
		    GCConstants.SALES_ORDER_DOCUMENT_TYPE);
	    String sOrderNo = eleGetOrderReleaseDetail.getAttribute(GCConstants.SALES_ORDER_NO);
	    Document getOrderListInput = GCXMLUtil.createDocument(GCConstants.ORDER);
	    Element eleOrder = getOrderListInput.getDocumentElement();
	    eleOrder.setAttribute(GCConstants.ORDER_NO, sOrderNo);
	    
	    Document docGetOrderListTmp = GCXMLUtil
	            .getDocument("<OrderList ><Order OrderHeaderKey=''/></OrderList>");

	    Document getOrderListOutput = GCCommonUtil.invokeAPI(env,
	            GCConstants.API_GET_ORDER_LIST, getOrderListInput,
	            docGetOrderListTmp);
	    Element eleOutOrder = (Element) XPathAPI.selectSingleNode(getOrderListOutput, "OrderList/Order");
	    String sOrderHeaderKey = eleOutOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
	    eleChangeOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);	    
	    
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch block of GCProcessStopShipmentResponseAPI.prepareChangeOrderHdrDoc(), Exception is :",
		    e);
	    throw new GCException(e);
	}
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" prepareChangeOrderHdrDoc(), docChangeOrderStatus is"
		    + GCXMLUtil.getXMLString(docChangeOrderIP));
	}
	LOGGER.debug(" Exiting GCProcessStopShipmentResponseAPI.prepareChangeOrderHdrDoc() method ");
	return docChangeOrderIP;
    }

    /**
     * 
     * Description of prepareChangeOrderStatusHdrDoc Prepare changeOrderStatus
     * Header Document
     * 
     * @param eleGetOrderReleaseDetail
     * @param sTransactionID
     * @return
     * @throws Exception
     * 
     */
    public static Document prepareChangeOrderStatusHdrDoc(
	    Element eleGetOrderReleaseDetail, String sTransactionID)
	    throws GCException {
	LOGGER.debug(" Entering GCProcessStopShipmentResponseAPI.prepareChangeOrderStatusHdrDoc() method ");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" eleGetOrderReleaseDetail is"
		    + GCXMLUtil.getElementXMLString(eleGetOrderReleaseDetail));
	}
	LOGGER.debug(" sTransactionID is" + sTransactionID);
	Document docChangeOrderStatus = null;
	try {
	    docChangeOrderStatus = GCXMLUtil
		    .createDocument(GCConstants.ORDER_STATUS_CHANGE);
	    Element eleChangeOrderStatus = docChangeOrderStatus
		    .getDocumentElement();
	    eleChangeOrderStatus.setAttribute(GCConstants.ORDER_NO,
		    eleGetOrderReleaseDetail
			    .getAttribute(GCConstants.SALES_ORDER_NO));
	    eleChangeOrderStatus.setAttribute(GCConstants.ENTERPRISE_CODE,
		    eleGetOrderReleaseDetail
			    .getAttribute(GCConstants.ENTERPRISE_CODE));
	    eleChangeOrderStatus.setAttribute(GCConstants.DOCUMENT_TYPE,
		    GCConstants.SALES_ORDER_DOCUMENT_TYPE);
	    eleChangeOrderStatus.setAttribute(
		    GCConstants.IGNORE_TRANSACTION_DEPENDENCIES,
		    GCConstants.YES);
	    eleChangeOrderStatus.setAttribute(GCConstants.TRANSACTION_ID,
		    sTransactionID);
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch block of GCProcessStopShipmentResponseAPI.prepareChangeOrderStatusHdrDoc(), Exception is :",
		    e);
	    throw new GCException(e);
	}
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" prepareChangeOrderStatusHdrDoc(), docChangeOrderStatus is"
		    + GCXMLUtil.getXMLString(docChangeOrderStatus));
	}
	LOGGER.debug(" Exiting GCProcessStopShipmentResponseAPI.prepareChangeOrderStatusHdrDoc() method ");
	return docChangeOrderStatus;
    }

    /**
     * 
     * Description of addOrderLinesToChangeRelease Add OrderLine to
     * changeRelease document
     * 
     * @param docChangeReleaseIP
     * @param eleChgReleaseOrderLines
     * @param eleReleaseLine
     * @throws GCException
     * 
     */
    private static void addOrderLinesToChangeRelease(
	    Document docChangeReleaseIP, Element eleChgReleaseOrderLines,
	    Element eleReleaseLine) throws GCException {
	LOGGER.debug(" Entering GCProcessStopShipmentResponseAPI.addOrderLinesToChangeRelease() method ");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" docChangeReleaseIP is"
		    + GCXMLUtil.getXMLString(docChangeReleaseIP));
	    LOGGER.debug(" eleChgReleaseOrderLines is"
		    + GCXMLUtil.getElementXMLString(eleChgReleaseOrderLines));
	    LOGGER.debug(" eleReleaseLine is"
		    + GCXMLUtil.getElementXMLString(eleReleaseLine));
	}
	try {
	    Element eleChgReleaseOrderLine = GCXMLUtil.createElement(
		    docChangeReleaseIP, GCConstants.ORDER_LINE, null);
	    eleChgReleaseOrderLine.setAttribute(GCConstants.ACTION,
		    GCConstants.BACKORDER);
	    eleChgReleaseOrderLine.setAttribute(
		    GCConstants.CHANGE_IN_QUANTITY,
		    "-"
			    + eleReleaseLine
				    .getAttribute(GCConstants.STATUS_QUANTITY));
	    eleChgReleaseOrderLine.setAttribute(GCConstants.PRIME_LINE_NO,
		    eleReleaseLine.getAttribute(GCConstants.PRIME_LINE_NO));
	    eleChgReleaseOrderLine.setAttribute(GCConstants.SUB_LINE_NO,
		    eleReleaseLine.getAttribute(GCConstants.SUB_LINE_NO));
	    eleChgReleaseOrderLines.appendChild(eleChgReleaseOrderLine);
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch block of GCProcessStopShipmentResponseAPI.addOrderLinesToChangeRelease(), Exception is :",
		    e);
	    throw new GCException(e);
	}
	LOGGER.debug(" Exiting GCProcessStopShipmentResponseAPI.addOrderLinesToChangeRelease() method ");
    }

    /**
     * 
     * Description of addOrderLinesToChangeOrder Add OrderLine to changeOrder
     * document
     * 
     * @param docChangeOrderIP
     * @param eleChgOrderLines
     * @param eleReleaseLine
     * @throws GCException
     * 
     */
    private static void addOrderLinesToChangeOrder(Document docChangeOrderIP,
	    Element eleChgOrderLines, Element eleReleaseLine)
	    throws GCException {
	LOGGER.debug(" Entering GCProcessStopShipmentResponseAPI.addOrderLinesToChangeOrder() method ");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" docChangeOrderIP is"
		    + GCXMLUtil.getXMLString(docChangeOrderIP));
	    LOGGER.debug(" eleChgOrderLines is"
		    + GCXMLUtil.getElementXMLString(eleChgOrderLines));
	    LOGGER.debug(" eleReleaseLine is"
		    + GCXMLUtil.getElementXMLString(eleReleaseLine));
	}
	try {
	    Element eleChgOrderLine = GCXMLUtil.createElement(docChangeOrderIP,
		    GCConstants.ORDER_LINE, null);
	    eleChgOrderLine.setAttribute(GCConstants.PRIME_LINE_NO,
		    eleReleaseLine.getAttribute(GCConstants.PRIME_LINE_NO));
	    eleChgOrderLine.setAttribute(GCConstants.SUB_LINE_NO,
		    eleReleaseLine.getAttribute(GCConstants.SUB_LINE_NO));
	    eleChgOrderLines.appendChild(eleChgOrderLine);

	    Element eleOrderHoldTypes = GCXMLUtil.createElement(
		    docChangeOrderIP, GCConstants.ORDER_HOLD_TYPES, null);
	    eleChgOrderLine.appendChild(eleOrderHoldTypes);
	    Element eleOrderHoldType = GCXMLUtil.createElement(
		    docChangeOrderIP, GCConstants.ORDER_HOLD_TYPE, null);
	    eleOrderHoldType.setAttribute(GCConstants.HOLD_TYPE,
		    GCConstants.SHIPMENT_CANCEL_HOLD);
	    eleOrderHoldType.setAttribute(GCConstants.STATUS,
		    GCConstants.APPLY_HOLD_CODE);
	    eleOrderHoldTypes.appendChild(eleOrderHoldType);
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch block of GCProcessStopShipmentResponseAPI.addOrderLinesToChangeOrder(), Exception is :",
		    e);
	    throw new GCException(e);
	}
	LOGGER.debug(" Exiting GCProcessStopShipmentResponseAPI.addOrderLinesToChangeOrder() method ");
    }

    /**
     * 
     * Description of addOrderLinesToChangeOrderStatus Add OrderLine to
     * changeOrderStatus document
     * 
     * @param docChangeOrderStatus
     * @param eleOrderLinesChngStatus
     * @param eleReleaseLine
     * @param sBaseDropStatus
     * @param sQuantity
     * @param sReleaseNo
     * @throws Exception
     * 
     */
    public static void addOrderLinesToChangeOrderStatus(
	    Document docChangeOrderStatus, Element eleOrderLinesChngStatus,
	    Element eleReleaseLine, String sBaseDropStatus, String sQuantity,
	    String sReleaseNo) throws GCException {
	LOGGER.debug(" Entering GCProcessStopShipmentResponseAPI.addOrderLinesToChangeOrderStatus() method ");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" docChangeOrderStatus is"
		    + GCXMLUtil.getXMLString(docChangeOrderStatus));
	    LOGGER.debug(" eleOrderLinesChngStatus is"
		    + GCXMLUtil.getElementXMLString(eleOrderLinesChngStatus));
	    LOGGER.debug(" eleReleaseLine is"
		    + GCXMLUtil.getElementXMLString(eleReleaseLine));
	}
	LOGGER.debug(" sBaseDropStatus ::" + sBaseDropStatus + " sQuantity ::"
		+ sQuantity + " sReleaseNo ::" + sReleaseNo);
	try {
	    Element eleOrderLineChngStatus = GCXMLUtil.createElement(
		    docChangeOrderStatus, GCConstants.ORDER_LINE, null);
	    eleOrderLineChngStatus.setAttribute(GCConstants.PRIME_LINE_NO,
		    eleReleaseLine.getAttribute(GCConstants.PRIME_LINE_NO));
	    eleOrderLineChngStatus.setAttribute(GCConstants.SUB_LINE_NO,
		    eleReleaseLine.getAttribute(GCConstants.SUB_LINE_NO));
	    eleOrderLineChngStatus.setAttribute(GCConstants.RELEASE_NO,
		    sReleaseNo);
	    eleOrderLineChngStatus.setAttribute(GCConstants.BASE_DROP_STATUS,
		    sBaseDropStatus);
	    eleOrderLineChngStatus
		    .setAttribute(GCConstants.QUANTITY, sQuantity);
	    eleOrderLinesChngStatus.appendChild(eleOrderLineChngStatus);
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch block of GCProcessStopShipmentResponseAPI.addOrderLinesToChangeOrderStatus(), Exception is :",
		    e);
	    throw new GCException(e);
	}
	LOGGER.debug(" Exiting GCProcessStopShipmentResponseAPI.addOrderLinesToChangeOrderStatus() method ");
    }

    public void setProperties(Properties arg0) throws GCException {

    }

}
