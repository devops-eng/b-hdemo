/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Accept document as input and will return document with LineTaxes.
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               19/07/2014        Singh, Gurpreet            Vertex utility to return document with taxes

             1.1              02/02/2015          Kumar, Rakesh              OMS:4727 Production OMS: No sales tax was applied to a production order in OMS

             1.2               03/09/2015        Pande, Anuj                POS Invoicing changes
             1.3               05/19/2015        Singh, Gurpreet            GCSTORE-2514: Line Level Taxes (CR-022)
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCWebServiceUtil;
import com.gc.common.utils.GCXMLUtil;
import com.gc.userexit.GCRecalculateLineTaxUE;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:gurpreet.singh@expicient.com">Gurpreet</a>
 */
public class GCProcessTaxCalculationsAPI implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCProcessTaxCalculationsAPI.class);

  int iLineItemNumber = 0;

  /**
   *
   * Description of processTaxCalculations Accept document as input and will
   * return document with LineTaxes If the vertex call fails, then setting
   * Order/OrderLines/OrderLine/LineTaxes/LineTax/@Refernce3=�ERROR�.
   *
   * @param env
   * @param inDoc
   * @return
   * @throws GCException
   *
   */
  public Document processTaxCalculations(YFSEnvironment env, Document inDoc)
      throws GCException {
    Document docVertexWebServiceOP = null;
    LOGGER.debug(" Entering GCProcessTaxCalculationsAPI.processTaxCalculations() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" processTaxCalculations() method, Input document is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    try {
      String sExtnSourceSystem ="";
      inDoc=modifyInDoc(env,inDoc);
      //GCSTORE-4770::Begin
      Element eExtn = GCXMLUtil.getElementByXPath(inDoc, "Order/Extn");
      if (!YFCCommon.isVoid(eExtn)) {
        sExtnSourceSystem = eExtn.getAttribute("ExtnSourceSystem");
      }
      if (YFCUtils.equals(sExtnSourceSystem, GCConstants.ATG)) {
        boolean isCustomerTaxExempted = evaluateIfATGCustomerTaxExempted(env, inDoc);
        if(isCustomerTaxExempted){
          inDoc.getDocumentElement().setAttribute(GCConstants.TAX_EXEMPT_FLAG, GCConstants.YES);
        }
      }
      //GCSTORE-4770::End
      Document docVertexCallIP = prepareVertexWebServiceIP(env,inDoc);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(" processTaxCalculations() method, docVertexCallIP is"
            + GCXMLUtil.getXMLString(docVertexCallIP));
      }
      try {
        GCWebServiceUtil obj = new GCWebServiceUtil();

        docVertexWebServiceOP =
            obj.invokeSOAPWebService(docVertexCallIP, YFSSystem.getProperty(GCConstants.VERTEX_WEB_SERVICE_URL), null);
      } catch (Exception e) {
        LOGGER.error("Inside catch block - Vertex call Fail");
        LOGGER.log("Exception ::", e);
        env.setTxnObject("IsVertexCallFailed", "Y");
        inDoc = addZeroTaxForCharges(inDoc);
        LOGGER.debug("Exiting GCProcessTaxCalculationsAPI.processTaxCalculations() method");
        return inDoc;
      }
      Element eleTaxVertexOP = GCXMLUtil.getElementByXPath(
          docVertexWebServiceOP, "Envelope/Body/VertexEnvelope"
              + "/QuotationResponse/TotalTax");
      if (YFCCommon.isVoid(eleTaxVertexOP)) {
        LOGGER.debug(" Inside if Vertex OP is blank ");
        inDoc = addZeroTaxForCharges(inDoc);
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug(" processTaxCalculations() method, Error document is"
              + GCXMLUtil.getXMLString(inDoc));
        }
        LOGGER.debug(" returning error document, Exiting GCProcessTaxCalculationsAPI.processTaxCalculations() method");
        return inDoc;
      }
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(" processTaxCalculations() method, docVertexWebServiceOP is"
            + GCXMLUtil.getXMLString(docVertexWebServiceOP));
      }
      processVertexResponse(inDoc, docVertexWebServiceOP);
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCProcessTaxCalculationsAPI.processTaxCalculations()",
          e);
      throw new GCException(e);
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" processTaxCalculations() method, Updated document is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    LOGGER.debug(" Exiting GCProcessTaxCalculationsAPI.processTaxCalculations() method ");
    return inDoc;
  }
  /**
   * GCSTORE-4770:: This method evaluates if the Customer in ATG Order is Tax Exempted or not.
   *
   * @param env
   * @param inDoc
   * @return- returns true if the customer is tax exempted else returns false.
   * @throws TransformerException
   *
   */
  public boolean evaluateIfATGCustomerTaxExempted(YFSEnvironment env,
      Document inDoc) throws TransformerException {
    LOGGER.beginTimer("evaluateIfATGCustomerTaxExempted");
    if(LOGGER.isVerboseEnabled()){
      LOGGER.verbose("Input Document::" + GCXMLUtil.getXMLString(inDoc));
    }

    Element eleOrder = inDoc.getDocumentElement();
    String taxExemptFlag = eleOrder.getAttribute(GCConstants.TAX_EXEMPT_FLAG);
    if(YFCCommon.isVoid(taxExemptFlag)){
      Element eleExtn = (Element)XPathAPI.selectSingleNode(eleOrder, GCConstants.EXTN);
      if(!YFCCommon.isVoid(eleExtn)){
        String extnSourceCustomerId = eleExtn.getAttribute(GCConstants.EXTN_SOURCE_CUSTOMER_ID);

                YFCDocument getCustomerListIp = YFCDocument
                        .createDocument(GCConstants.CUSTOMER);
                YFCElement customerEle = getCustomerListIp.getDocumentElement();
                customerEle.setAttribute(GCConstants.ORGANIZATION_CODE,
                        eleOrder.getAttribute(GCConstants.ENTERPRISE_CODE));
                YFCElement eExtn = customerEle.createChild(GCConstants.EXTN);
                eExtn.setAttribute(GCXmlLiterals.EXTN_SRC_CSTOMER_ID, extnSourceCustomerId);
                YFCDocument getCustomerListTmpl = YFCDocument.getDocumentFor("<CustomerList><Customer><Extn /></Customer></CustomerList>");
                YFCDocument getCustomerListOutDoc = GCCommonUtil.invokeAPI(env, GCConstants.GET_CUSTOMER_LIST, getCustomerListIp, getCustomerListTmpl);
                if(LOGGER.isVerboseEnabled()){
                    LOGGER.verbose("GetCustomerList OutDoc::" + getCustomerListOutDoc.toString());
                }
                YFCElement eleCustomer = getCustomerListOutDoc.getDocumentElement().getChildElement(GCConstants.CUSTOMER);
                if(!YFCCommon.isVoid(eleCustomer)){
                    YFCElement eleCustomerExtn = eleCustomer.getChildElement(GCConstants.EXTN);
                    String taxExemptState = eleCustomerExtn.getAttribute("ExtnTaxExemptState");
                    Element elePersonInfoShipToOrder = (Element) XPathAPI
                            .selectSingleNode(eleOrder,
                                    GCXmlLiterals.PERSON_INFO_SHIP_TO);
                    String sState = elePersonInfoShipToOrder
                            .getAttribute(GCXmlLiterals.STATE);
                    LOGGER.verbose("PersonInfoShipTo State==" + sState + "::Customer Tax Exempt State==" + taxExemptState);
                    if(YFCCommon.equals(sState, taxExemptState)){
                        LOGGER.verbose("Returning true as Customer is Tax Exempted");
                        LOGGER.endTimer("evaluateIfATGCustomerTaxExempted");
                        return true;
                    }
                }


            }

        }
        LOGGER.endTimer("evaluateIfATGCustomerTaxExempted");
        return false;
    }
  
  /**
   *
   * Description of prepareVertexWebServiceIP Prepare Input for the Vertex
   * Call
   *
   * @param inDoc
   * @return
   * @throws Exception
   *
   */
  public Document prepareVertexWebServiceIP(YFSEnvironment env, Document inDoc)
      throws GCException {
    LOGGER.debug(" Entering GCProcessTaxCalculationsAPI.prepareVertexWebServiceIP() method ");
    Document docVertexCallIP = null;
    try {
      docVertexCallIP = GCCommonUtil
          .getDocumentFromPath("/global/template/api/extn_VertexCallRequest.xml");
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(" prepareVertexWebServiceIP() method, docVertexCallIP is"
            + GCXMLUtil.getXMLString(docVertexCallIP));
      }
      Element eleOrder = inDoc.getDocumentElement();
      String sOrderNo = eleOrder.getAttribute(GCConstants.ORDER_NO);
      String sTaxExemptFlag = eleOrder
          .getAttribute(GCConstants.TAX_EXEMPT_FLAG);
      if (GCConstants.YES.equals(sTaxExemptFlag)) {
        sTaxExemptFlag = "true";
      } else if (GCConstants.NO.equals(sTaxExemptFlag)) {
        sTaxExemptFlag = "false";
      }

      String sOrderDate = eleOrder.getAttribute(GCConstants.ORDER_DATE);
      if (YFCCommon.isVoid(sOrderDate)) {
        Calendar calRemorseDate = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat(
            GCConstants.DATE_TIME_FORMAT);
        sOrderDate = dateFormat.format(calRemorseDate.getTime());
        LOGGER.debug(" sOrderDate " + sOrderDate);
      }
      String sDocumentType = eleOrder
          .getAttribute(GCConstants.DOCUMENT_TYPE);
      //OMS-4727 starts
      Element eTrustedId=GCXMLUtil.getElementByXPath(docVertexCallIP,"Envelope/Body/VertexEnvelope/Login/TrustedId");
      String sTrustedId=YFSSystem.getProperty(GCConstants.VERTEX_CALL_REQUEST_TRUSTED_ID);
      eTrustedId.setTextContent(sTrustedId);
      //OMS-4727 Ends
      Element eQuotationRequest = GCXMLUtil.getElementByXPath(
          docVertexCallIP,
          "Envelope/Body/VertexEnvelope/QuotationRequest");
      if (!YFCCommon.isVoid(eQuotationRequest)) {
        LOGGER.debug(" Inside if(!YFCCommon.isVoid(eQuotationRequest)");

        eQuotationRequest.setAttribute("documentDate", sOrderDate);
        eQuotationRequest.setAttribute("transactionId", sOrderNo);
        eQuotationRequest.setAttribute("transactionType", "SALE");

        XPathAPI.selectSingleNode(eQuotationRequest, "Seller/Company")
        .setTextContent("99");
        XPathAPI.selectSingleNode(eQuotationRequest, "Seller/Division")
        .setTextContent("02");

        Element eleHeaderCustomer = (Element) XPathAPI
            .selectSingleNode(eQuotationRequest, "Customer");
        eleHeaderCustomer.setAttribute("isTaxExempt", sTaxExemptFlag);
        Element eleHeaderCustomerCode = (Element) XPathAPI
            .selectSingleNode(eleHeaderCustomer, "CustomerCode");
        eleHeaderCustomerCode.setAttribute("classCode", "MAIN");
        Element eleHeaderDestination = (Element) XPathAPI.selectSingleNode(eleHeaderCustomer, "Destination");

        // Append Header Ship To element into the Vertex I/P XML
        // Adding MPOS logic in Tax Calculation class
        YFCDocument storeAddress = null;
        Element eleHeaderPersonInfoShipTo = (Element) eleOrder.getElementsByTagName("PersonInfoShipTo").item(0);
        String sEntryType = eleOrder.getAttribute(GCConstants.ENTRY_TYPE);

        if (YFCCommon.equalsIgnoreCase(sEntryType, "mPOS")) {
          String sOrgainzationCode = eleOrder.getAttribute(GCConstants.SELLER_ORGANIZATION_CODE);
          YFCDocument getOrganizationListInDoc =
              YFCDocument.getDocumentFor("<Organization OrganizationCode='" + sOrgainzationCode + "'" + "/>");
          YFCDocument getOrganizationListTemp = YFCDocument
              .getDocumentFor("<OrganizationList><Organization OrganizationCode='' OrganizationKey='' TaxExemptFlag='' "
                  + "TaxExemptionCertificate=''><CorporatePersonInfo/></Organization></OrganizationList>");

          storeAddress = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORGANIZATION_LIST,
              getOrganizationListInDoc, getOrganizationListTemp);

          eleHeaderCustomer.removeChild(eleHeaderDestination);

        } else {
          if (!YFCCommon.isVoid(eleHeaderPersonInfoShipTo)) {
            LOGGER.debug("Inside if append header person info ship to");
            appendPersonInfoShipTo(docVertexCallIP,
                eleHeaderPersonInfoShipTo, eleHeaderDestination);
          }
        }
        NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc,
            "Order/OrderLines/OrderLine");
        for (int iLineCounter = 0; iLineCounter < nlOrderLine
            .getLength(); iLineCounter++) {
          LOGGER.debug(" looping on the orderlines");
          Element eleOrderLine = (Element) nlOrderLine
              .item(iLineCounter);
          String sOrderLineKey = eleOrderLine
              .getAttribute(GCConstants.ORDER_LINE_KEY);
          if (YFCCommon.isVoid(sOrderLineKey)) {
            sOrderLineKey = eleOrderLine
                .getAttribute(GCConstants.LINE_ID);
            // GCSTORE-2514 : Start
            if (YFCCommon.isVoid(sOrderLineKey)) {
              sOrderLineKey = eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
            }
            // GCSTORE-2514 : End
            // GCSTORe-5042 : Start
            if (YFCCommon.isVoid(sOrderLineKey)) {
                sOrderLineKey = eleOrderLine.getAttribute("TransactionalLineId");
              }
         // GCSTORe-5042 : End
          }
          String sOrderedQty = eleOrderLine
              .getAttribute(GCConstants.ORDERED_QTY);
          double dOrderedQty = (!YFCCommon.isVoid(sOrderedQty)) ? Double
              .valueOf(sOrderedQty) : 0.00;
              Element eleItem = (Element) XPathAPI.selectSingleNode(
                  eleOrderLine, GCConstants.ITEM);
              String sItemID="";
              if(!YFCCommon.isVoid(eleItem)){
                sItemID = eleItem.getAttribute(GCConstants.ITEM_ID);
              }
              String sTaxProductCode = GCXMLUtil.getAttributeFromXPath(
                  eleOrderLine,
                  "ItemDetails/ClassificationCodes/@TaxProductCode");

              boolean bIsPriceCharge = false;

              if (!bIsPriceCharge
                  && GCConstants.SALES_ORDER_DOCUMENT_TYPE
                  .equals(sDocumentType)) {
                LOGGER.debug(" Inside if charge category is Price ");
                String sPrice = GCXMLUtil.getAttributeFromXPath(eleOrderLine,
                    "LinePriceInfo/@UnitPrice");
                if(!YFCCommon.isVoid(sPrice)){
                  appendPriceToVertexIP(env, docVertexCallIP,
                      eQuotationRequest, sOrderDate, sTaxExemptFlag,
                      eleOrderLine, sEntryType, storeAddress, eleHeaderPersonInfoShipTo);
                }
              }

              NodeList nlLineCharge = XPathAPI.selectNodeList(
                  eleOrderLine, "LineCharges/LineCharge");
              for (int iLineChargeCounter = 0; iLineChargeCounter < nlLineCharge
                  .getLength(); iLineChargeCounter++) {
                Element eleLineCharge = (Element) nlLineCharge
                    .item(iLineChargeCounter);
                String sChargeCategory = eleLineCharge
                    .getAttribute(GCConstants.CHARGE_CATEGORY);
                String sChargeName = eleLineCharge
                    .getAttribute(GCConstants.CHARGE_NAME);
                String sLineItemId = GCRecalculateLineTaxUE
                    .fetchLineItemId(sChargeCategory, sChargeName,
                        sOrderLineKey);

                if (!(GCConstants.SHIPPING_PROMOTION
                    .equalsIgnoreCase(sChargeCategory) || GCConstants.PROMOTION
                    .equalsIgnoreCase(sChargeCategory))) {
                  LOGGER.debug(" Inside if charge category is not Shipping Promotion or Promotion ");
                  String sChargePerUnit = eleLineCharge
                      .getAttribute(GCConstants.CHARGE_PER_UNIT);
                  double dChargePerUnit = (!YFCCommon
                      .isVoid(sChargePerUnit)) ? Double
                          .valueOf(sChargePerUnit) : 0.00;
                          String sChargePerLine = eleLineCharge
                              .getAttribute(GCConstants.CHARGE_PER_LINE);
                          double dChargePerLine = (!YFCCommon
                              .isVoid(sChargePerLine)) ? Double
                                  .valueOf(sChargePerLine) : 0.00;
                                  double dChargeAmount = 0.00;
                                  String sPrice = "0.00";

                                  LOGGER.debug("sChargePerUnit" + sChargePerUnit
                                      + "sChargePerLine" + sChargePerLine);
                                  Element eleLineItem = GCXMLUtil.createElement(
                                      docVertexCallIP, "LineItem", null);

                                  addLineItemAttributes(eleLineItem, sLineItemId,
                                      sOrderDate);

                                  if (GCConstants.SHIPPING
                                      .equalsIgnoreCase(sChargeCategory)
                                      && sChargeName.contains("ShippingCharge")) {
                                    LOGGER.debug(" Inside if Charge Category is Shipping ");
                                    NodeList nlShippingPromotion = XPathAPI
                                        .selectNodeList(eleOrderLine,
                                            "LineCharges/LineCharge[@ChargeCategory='ShippingPromotion']");
                                    for (int iCounter = 0; iCounter < nlShippingPromotion
                                        .getLength(); iCounter++) {
                                      LOGGER.debug(" Looping on ShippingPromotion charges ");
                                      Element eleShippingPromotion = (Element) nlShippingPromotion
                                          .item(iCounter);
                                      String sShippingPromotion = eleShippingPromotion
                                          .getAttribute(GCConstants.CHARGE_PER_LINE);
                                      double dShippingPromotion = (!YFCCommon
                                          .isVoid(sShippingPromotion)) ? Double
                                              .valueOf(sShippingPromotion) : 0.00;
                                              dChargePerLine = dChargePerLine
                                                  - dShippingPromotion;
                                    }
                                  }

                                  if (dChargePerUnit > 0.00) {
                                    sPrice = sChargePerUnit;
                                    dChargeAmount = dChargePerUnit * dOrderedQty;
                                  } else if (dChargePerLine > 0.00) {
                                    double dPrice = (dChargePerLine / dOrderedQty);
                                    sPrice = String.valueOf(dPrice);
                                    dChargeAmount = dChargePerLine;
                                  }
                                  eQuotationRequest.appendChild(eleLineItem);

                                  GCRecalculateLineTaxUE.addProductInfo(
                                      docVertexCallIP, eleLineItem,
                                      sChargeCategory, sItemID, sTaxProductCode);

                                  Element eleQuantity = GCXMLUtil.createElement(
                                      docVertexCallIP, "Quantity", null);
                                  eleQuantity.setTextContent(String
                                      .valueOf(dOrderedQty));
                                  eleLineItem.appendChild(eleQuantity);

                                  Element eleFreight = GCXMLUtil.createElement(
                                      docVertexCallIP, "Freight", null);
                                  eleFreight.setTextContent("0");
                                  eleLineItem.appendChild(eleFreight);

                                  Element eleUnitPrice = GCXMLUtil.createElement(
                                      docVertexCallIP, "UnitPrice", null);
                                  eleUnitPrice.setTextContent(sPrice);
                                  eleLineItem.appendChild(eleUnitPrice);

                                  Element eleExtendedPrice = GCXMLUtil.createElement(
                                      docVertexCallIP, "ExtendedPrice", null);
                                  eleExtendedPrice.setTextContent(String
                                      .valueOf(dChargeAmount));
                                  eleLineItem.appendChild(eleExtendedPrice);

                                  Element eleCustomer = GCXMLUtil.createElement(
                                      docVertexCallIP, "Customer", null);
                                  eleCustomer.setAttribute("isTaxExempt",
                                      sTaxExemptFlag);
                                  eleLineItem.appendChild(eleCustomer);

                                  Element eleCustomerCode = GCXMLUtil.createElement(
                                      docVertexCallIP, "CustomerCode", null);
                                  eleCustomerCode.setAttribute("classCode", "MAIN");
                                  eleCustomer.appendChild(eleCustomerCode);

                                  if (YFCCommon.equalsIgnoreCase(sEntryType, "mPOS")) {
                                    Element eleDestination = GCXMLUtil.createElement(docVertexCallIP, "Destination", null);
                                    eleCustomer.appendChild(eleDestination);
                                    if (YFCCommon.equalsIgnoreCase(eleOrderLine.getAttribute(GCConstants.DELIVERY_METHOD), "CARRY")) {
                                      Element eleStoreAddress =
                                          (Element) storeAddress.getDocument().getElementsByTagName("CorporatePersonInfo").item(0);
                                      appendPersonInfoShipTo(docVertexCallIP, eleStoreAddress, eleDestination);
                                    } else {
                                      Element eleShipToInfo = (Element) XPathAPI.selectSingleNode(eleOrder, "PersonInfoShipTo");
                                      appendPersonInfoShipTo(docVertexCallIP, eleShipToInfo, eleDestination);
                                    }
                                  } else {
                                    Element elePersonInfoShipTo = (Element) XPathAPI
                                        .selectSingleNode(eleOrderLine,
                                          "PersonInfoShipTo");
                                  if (!YFCCommon.isVoid(elePersonInfoShipTo)) {
                                    Element eleDestination = GCXMLUtil
                                        .createElement(docVertexCallIP,
                                            "Destination", null);
                                    eleCustomer.appendChild(eleDestination);
                                    appendPersonInfoShipTo(docVertexCallIP,
                                          elePersonInfoShipTo, eleDestination);
                                    }
                                  }
                }
              }
        }
      }

    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCProcessTaxCalculationsAPI.prepareVertexWebServiceIP() ",
          e);
      throw new GCException(e);
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" GCProcessTaxCalculationsAPI.prepareVertexWebServiceIP() method, updated docVertexCallIP is"
          + GCXMLUtil.getXMLString(docVertexCallIP));
    }
    LOGGER.debug(" Exiting GCProcessTaxCalculationsAPI.prepareVertexWebServiceIP() method ");
    return docVertexCallIP;
  }

  /**
   * This method calls getItemList with the given item id and gets its TaxProductCode.
   *
   * @param env
   * @param sItemID
   * @return
   */
  private String getTaxProdCodeFromGetItemList(YFSEnvironment env, String sItemID) {
    LOGGER.beginTimer("getTaxProdCodeFromGetItemList");
    String sTaxProdCode = "";
    YFCDocument getItemListInDoc = YFCDocument.createDocument(GCConstants.ITEM);
    YFCElement eleItem = getItemListInDoc.getDocumentElement();
    eleItem.setAttribute(GCConstants.ITEM_ID, sItemID);
    eleItem.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GCI);
    eleItem.setAttribute(GCConstants.UNIT_OF_MEASURE, GCConstants.EACH);
    eleItem.setAttribute("GetUnpublishedItems", GCConstants.YES);
    YFCDocument getItemListTmpl =
        YFCDocument
        .getDocumentFor("<ItemList><Item ItemID=''><ClassificationCodes TaxProductCode='' /></Item></ItemList>");
    YFCDocument getItemListOutputDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_ITEM_LIST, getItemListInDoc, getItemListTmpl);
    YFCElement getItemListRoot = getItemListOutputDoc.getDocumentElement();
    YFCElement getItemListItem = getItemListRoot.getChildElement(GCConstants.ITEM);
    if (!YFCCommon.isVoid(getItemListItem)) {
      LOGGER.verbose("ItemExists in DB ");
      YFCElement classificationCodes = getItemListItem.getChildElement("ClassificationCodes");
      sTaxProdCode = classificationCodes.getAttribute(GCConstants.TAX_PRODUCT_CODE);
      LOGGER.verbose("TaxProductCode::" + sTaxProdCode);
    }
    LOGGER.verbose("sTaxProdCode=====>" + sTaxProdCode);
    LOGGER.endTimer("getTaxProdCodeFromGetItemList");
    return sTaxProdCode;
  }

  /**
   *
   * Description of addZeroTaxForCharges If the vertex call fails, then
   * setting Order/OrderLines/OrderLine/LineTaxes/LineTax/@Refernce3=�ERROR�
   * and append the Tax value as 0
   *
   * @param inDoc
   * @return
   * @throws GCException
   *
   */
  private Document addZeroTaxForCharges(Document inDoc) throws GCException {
    LOGGER.debug(" Entering GCProcessTaxCalculationsAPI.addZeroTaxForCharges() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" addZeroTaxForCharges() method, inDoc is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    try {
      NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc,
          "Order/OrderLines/OrderLine");
      for (int iLineCounter = 0; iLineCounter < nlOrderLine.getLength(); iLineCounter++) {
        LOGGER.debug(" Looping on OrderLines ");
        Element eleOrderLine = (Element) nlOrderLine.item(iLineCounter);
        Element eleLineTaxes = (Element) XPathAPI.selectSingleNode(
            eleOrderLine, GCConstants.LINE_TAXES);
        if (YFCCommon.isVoid(eleLineTaxes)) {
          eleLineTaxes = GCXMLUtil.createElement(inDoc,
              GCConstants.LINE_TAXES, null);
          eleOrderLine.appendChild(eleLineTaxes);
        }
        boolean bIsPriceCharge = false;
        if (!bIsPriceCharge) {
          LOGGER.debug(" Inside if(!bIsPriceCharge) ");
          String sChargeCategory = "LinePrice";
          String sChargeName = "LinePriceTax";
          Element eleLineTax = GCXMLUtil.createElement(inDoc,
              GCConstants.LINE_TAX, null);
          String sResponseTax = "0.00";
          String sTaxPercentage = "0.00";
          appendLineTaxes(eleLineTax, sChargeCategory, sChargeName,
              sResponseTax, sTaxPercentage);
          eleLineTax.setAttribute("Reference3", "ERROR");
          eleLineTaxes.appendChild(eleLineTax);
          bIsPriceCharge = true;
        }
        NodeList nlLineCharge = XPathAPI.selectNodeList(eleOrderLine,
            "LineCharges/LineCharge");
        for (int iChargeCounter = 0; iChargeCounter < nlLineCharge
            .getLength(); iChargeCounter++) {
          LOGGER.debug(" Looping on LineCharges ");
          Element eleLineCharge = (Element) nlLineCharge
              .item(iChargeCounter);
          String sChargeCategory = eleLineCharge
              .getAttribute(GCConstants.CHARGE_CATEGORY);
          String sChargeName = eleLineCharge
              .getAttribute(GCConstants.CHARGE_NAME);
          if (!(GCConstants.SHIPPING_PROMOTION
              .equalsIgnoreCase(sChargeCategory) || GCConstants.PROMOTION
              .equalsIgnoreCase(sChargeCategory))) {
            LOGGER.debug(" Inside if charges are not promotional ");
            String sResponseTax = "0.00";
            String sTaxPercentage = "0.00";
            Element eleLineTax = GCXMLUtil.createElement(inDoc,
                GCConstants.LINE_TAX, null);
            appendLineTaxes(eleLineTax, sChargeCategory,
                sChargeName, sResponseTax, sTaxPercentage);
            eleLineTax.setAttribute("Reference3", "ERROR");
            eleLineTaxes.appendChild(eleLineTax);
          }
        }
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCProcessTaxCalculationsAPI.addZeroTaxForCharges() ",
          e);
      throw new GCException(e);
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" GCProcessTaxCalculationsAPI.addZeroTaxForCharges() method, updated inDoc is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    LOGGER.debug(" Exiting GCProcessTaxCalculationsAPI.addZeroTaxForCharges() method ");
    return inDoc;
  }

  /**
   *
   * Description of processVertexResponse Process vertex response and update
   * LineTaxes for particular LineCharges
   *
   * @param inDoc
   * @param docVertexWebServiceOP
   * @throws Exception
   *
   */
  private void processVertexResponse(Document inDoc,
      Document docVertexWebServiceOP) throws GCException {
    LOGGER.debug(" Entering GCProcessTaxCalculationsAPI.processVertexResponse() method inDoc:"+GCXMLUtil.getXMLString(inDoc));
    try {
      NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc,
          "Order/OrderLines/OrderLine");
      for (int iLineCounter = 0; iLineCounter < nlOrderLine.getLength(); iLineCounter++) {
        LOGGER.debug(" Looping on OrderLines ");
        Element eleOrderLine = (Element) nlOrderLine.item(iLineCounter);
        String sOrderLineKey = eleOrderLine
            .getAttribute(GCConstants.ORDER_LINE_KEY);
        if (YFCCommon.isVoid(sOrderLineKey)) {
          sOrderLineKey = eleOrderLine
              .getAttribute(GCConstants.LINE_ID);
          eleOrderLine.removeAttribute(GCConstants.LINE_ID);
          // CR:22 Start
          if (YFCCommon.isVoid(sOrderLineKey)) {
            sOrderLineKey = eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
          }
          // CR:22 End
       // GCSTORe-5042 : Start
          if (YFCCommon.isVoid(sOrderLineKey)) {
              sOrderLineKey = eleOrderLine.getAttribute("TransactionalLineId");
            }
       // GCSTORe-5042 : End
        }
        Element eleLineTaxes = (Element) XPathAPI.selectSingleNode(
            eleOrderLine, GCConstants.LINE_TAXES);
        if (YFCCommon.isVoid(eleLineTaxes)) {
          eleLineTaxes = GCXMLUtil.createElement(inDoc,
              GCConstants.LINE_TAXES, null);
          eleOrderLine.appendChild(eleLineTaxes);
        }
        boolean bIsPriceCharge = false;
        String sPrice = GCXMLUtil.getAttributeFromXPath(eleOrderLine,
            "LinePriceInfo/@UnitPrice");
        if ((!bIsPriceCharge) && (!YFCCommon.isVoid(sPrice))) {
          LOGGER.debug(" Inside if(!bIsPriceCharge) ");
          String sChargeCategory = "LinePrice";
          String sChargeName = "LinePriceTax";
          String sLineItemIdPrefix = "L-" + sOrderLineKey;
          Element eleLineItem = (Element) XPathAPI
              .selectSingleNode(
                  docVertexWebServiceOP,
                  "Envelope/Body/VertexEnvelope/"
                      + "QuotationResponse/LineItem[@lineItemId='"
                      + sLineItemIdPrefix + "']");
          String sResponseTax = "0.00";
          Double dTaxPercentage = 0.00;
          if (!YFCCommon.isVoid(eleLineItem)) {
            sResponseTax = XPathAPI.selectSingleNode(eleLineItem,
                "TotalTax").getTextContent();
            /* POS Invoicing - Start */
            dTaxPercentage = sumEffectiveTaxRate(eleLineItem, dTaxPercentage);
            /* POS Invoicing - End */
          }
          LOGGER.debug(" sChargeCategory" + sChargeCategory
              + " sChargeName" + sChargeName
              + " sLineItemIdPrefix" + sLineItemIdPrefix);
          Element eleLineTax =
              (Element) XPathAPI.selectSingleNode(eleOrderLine, "LineTaxes/LineTax[@ChargeCategory='" + sChargeCategory
                  + "' and @ChargeName='" + sChargeName + "']");
          if (YFCCommon.isVoid(eleLineTax)) {
            eleLineTax = GCXMLUtil.createElement(inDoc,
                GCConstants.LINE_TAX, null);
          }
          appendLineTaxes(eleLineTax, sChargeCategory, sChargeName,
              sResponseTax, dTaxPercentage.toString());
          eleLineTaxes.appendChild(eleLineTax);
          bIsPriceCharge = true;
        }

        NodeList nlLineCharge = XPathAPI.selectNodeList(eleOrderLine,
            "LineCharges/LineCharge");
        for (int iChargeCounter = 0; iChargeCounter < nlLineCharge
            .getLength(); iChargeCounter++) {
          LOGGER.debug(" Looping on LineCharges ");
          Element eleLineCharge = (Element) nlLineCharge
              .item(iChargeCounter);
          String sChargeCategory = eleLineCharge
              .getAttribute(GCConstants.CHARGE_CATEGORY);
          String sChargeName = eleLineCharge
              .getAttribute(GCConstants.CHARGE_NAME);

          String sLineItemId = GCRecalculateLineTaxUE
              .fetchLineItemId(sChargeCategory, sChargeName,
                  sOrderLineKey);
          Element eleLineItem = (Element) XPathAPI
              .selectSingleNode(
                  docVertexWebServiceOP,
                  "Envelope/Body/VertexEnvelope/"
                      + "QuotationResponse/LineItem[@lineItemId='"
                      + sLineItemId + "']");
          String sResponseTax = "0.00";
          Double dTaxPercentage = 0.00;
          if (!YFCCommon.isVoid(eleLineItem)) {
            sResponseTax = XPathAPI.selectSingleNode(eleLineItem,
                "TotalTax").getTextContent();
            Element eleLineTax =
                (Element) XPathAPI.selectSingleNode(eleOrderLine, "LineTaxes/LineTax[@ChargeCategory='"
                    + sChargeCategory + "' and @ChargeName='" + sChargeName + "']");
            if (YFCCommon.isVoid(eleLineTax)) {
              eleLineTax = GCXMLUtil.createElement(inDoc,
                  GCConstants.LINE_TAX, null);
            }
            dTaxPercentage = sumEffectiveTaxRate(eleLineItem, dTaxPercentage);
            /* POS Invoicing - End */
            appendLineTaxes(eleLineTax, sChargeCategory,
                sChargeName, sResponseTax, dTaxPercentage.toString());
            eleLineTaxes.appendChild(eleLineTax);
          }
        }
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCProcessTaxCalculationsAPI.processVertexResponse() ",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCProcessTaxCalculationsAPI.processVertexResponse() method outDoc:"+GCXMLUtil.getXMLString(inDoc));
  }

  /**
   *
   * @param eleLineItem
   * @param dTaxPercentage
   * @return
   */
  public Double sumEffectiveTaxRate(Element eleLineItem, Double dTaxPercentage) {
    LOGGER.beginTimer("GCProcessTaxCalculationsAPI.sumEffectiveTaxRate");
    LOGGER.verbose("GCProcessTaxCalculationsAPI.sumEffectiveTaxRate");
    NodeList nlTaxes = eleLineItem.getElementsByTagName("Taxes");

    String sTaxPercentage = GCConstants.BLANK;
    for (int i = 0; i < nlTaxes.getLength(); i++) {
      Element eleTaxes = (Element) nlTaxes.item(i);
      Element eleEffectiveRate = (Element) eleTaxes.getElementsByTagName("EffectiveRate").item(0);
      sTaxPercentage = eleEffectiveRate.getTextContent();
      if (!YFCCommon.isStringVoid(sTaxPercentage)) {
        dTaxPercentage += Double.parseDouble(sTaxPercentage);
      }
    }
    LOGGER.verbose("GCProcessTaxCalculationsAPI.sumEffectiveTaxRate");
    LOGGER.endTimer("GCProcessTaxCalculationsAPI.sumEffectiveTaxRate");
    return dTaxPercentage;
  }

  private void appendLineTaxes(Element eleLineTax, String sChargeCategory,
      String sChargeName, String sResponseTax, String sTaxPercentage) throws GCException {
    LOGGER.debug(" Entering GCProcessTaxCalculationsAPI.appendLineTaxes() method ");
    try {
      String sTaxName = ("LinePriceTax".equalsIgnoreCase(sChargeName)) ? sChargeName
          : sChargeName + "Tax";
      eleLineTax.setAttribute(GCConstants.CHARGE_CATEGORY,
          sChargeCategory);
      eleLineTax.setAttribute(GCConstants.CHARGE_NAME, sChargeName);
      eleLineTax.setAttribute(GCConstants.TAX_NAME, sTaxName);
      eleLineTax.setAttribute(GCConstants.TAX, sResponseTax);
      eleLineTax.setAttribute(GCConstants.TAX_PERCENTAGE, sTaxPercentage);
      String sReference3 = eleLineTax.getAttribute(GCConstants.REFERENCE_3);
      if (YFCCommon.equals(GCConstants.ERROR, sReference3)) {
        eleLineTax.setAttribute(GCConstants.REFERENCE_3, "");
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCProcessTaxCalculationsAPI.appendLineTaxes() ",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCProcessTaxCalculationsAPI.appendLineTaxes() method ");
  }

  private void appendPersonInfoShipTo(Document docVertexCallIP,
      Element elePersonInfoShipTo, Element eleDestination)
          throws GCException {
    LOGGER.debug(" Entering GCRecalculateLineTaxUE.appendPersonInfoShipTo() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" appendPersonInfoShipTo() method, docVertexCallIP is"
          + GCXMLUtil.getXMLString(docVertexCallIP));
      LOGGER.debug(" appendPersonInfoShipTo() method, elePersonInfoShipTo is"
          + GCXMLUtil.getElementXMLString(elePersonInfoShipTo));
      LOGGER.debug(" appendPersonInfoShipTo() method, eleDestination is"
          + GCXMLUtil.getElementXMLString(eleDestination));
    }
    try {
      Element eleStreetAddress1 = GCXMLUtil.createElement(
          docVertexCallIP, "StreetAddress1", null);
      eleStreetAddress1.setTextContent(elePersonInfoShipTo
          .getAttribute(GCConstants.ADDRESS_LINE1));
      eleDestination.appendChild(eleStreetAddress1);

      Element eleCity = GCXMLUtil.createElement(docVertexCallIP, "City",
          null);
      eleCity.setTextContent(elePersonInfoShipTo
          .getAttribute(GCConstants.CITY));
      eleDestination.appendChild(eleCity);

      Element eleMainDivision = GCXMLUtil.createElement(docVertexCallIP,
          "MainDivision", null);
      eleMainDivision.setTextContent(elePersonInfoShipTo
          .getAttribute(GCConstants.STATE));
      eleDestination.appendChild(eleMainDivision);

      Element elePostalCode = GCXMLUtil.createElement(docVertexCallIP,
          "PostalCode", null);
      elePostalCode.setTextContent(elePersonInfoShipTo
          .getAttribute(GCConstants.ZIP_CODE));
      eleDestination.appendChild(elePostalCode);

      Element eleCountry = GCXMLUtil.createElement(docVertexCallIP,
          "Country", null);
      eleCountry.setTextContent(elePersonInfoShipTo
          .getAttribute(GCConstants.COUNTRY));
      eleDestination.appendChild(eleCountry);

    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateLineTaxUE.appendPersonInfoShipTo() ",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCRecalculateLineTaxUE.appendPersonInfoShipTo() method outDoc:"+GCXMLUtil.getXMLString(docVertexCallIP));
  }

  /**
   *
   * Description of appendPriceToVertexIP Append Price to the Vertex IP XML
   *
   * @param docVertexCallIP
   * @param eQuotationRequest
   * @param sOrderDate
   * @param sTaxExemptFlag
   * @param eleOrderLine
   * @throws GCException
   *
   */
  private void appendPriceToVertexIP(YFSEnvironment env, Document docVertexCallIP,
      Element eQuotationRequest, String sOrderDate,
      String sTaxExemptFlag, Element eleOrderLine, String sEntryType, YFCDocument storeAddress,
      Element eleHeaderPersonInfoShipTo) throws GCException {
    LOGGER.debug(" Entering GCProcessTaxCalculationsAPI.appendPriceToVertexIP() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" appendPriceToVertexIP() method, docVertexCallIP is"
          + GCXMLUtil.getXMLString(docVertexCallIP));
    }
    try {
      Element eleLineItem = GCXMLUtil.createElement(docVertexCallIP,
          "LineItem", null);
      String sOrderLineKey = eleOrderLine
          .getAttribute(GCConstants.ORDER_LINE_KEY);
      if (YFCCommon.isVoid(sOrderLineKey)) {
        sOrderLineKey = eleOrderLine.getAttribute(GCConstants.LINE_ID);
        // GCSTORE-2514 : Start
        if (YFCCommon.isVoid(sOrderLineKey)) {
          sOrderLineKey = eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
        }
        // GCSTORE-2514 : End
     // GCSTORe-5042 : Start
        if (YFCCommon.isVoid(sOrderLineKey)) {
            sOrderLineKey = eleOrderLine.getAttribute("TransactionalLineId");
          }
     // GCSTORe-5042 : End
      }
      String sOrderedQty = eleOrderLine
          .getAttribute(GCConstants.ORDERED_QTY);
      String sItemID = GCXMLUtil.getAttributeFromXPath(eleOrderLine,
          "Item/@ItemID");
      String sTaxProductCode = GCXMLUtil.getAttributeFromXPath(
          eleOrderLine,
          "ItemDetails/ClassificationCodes/@TaxProductCode");
      if (YFCCommon.isVoid(sTaxProductCode)) {
        sTaxProductCode = getTaxProdCodeFromGetItemList(env, sItemID);
      }

      double dOrderedQty = (!YFCCommon.isVoid(sOrderedQty)) ? Double
          .valueOf(sOrderedQty) : 0.00;
          String sLineItemId = "L-" + sOrderLineKey;
          addLineItemAttributes(eleLineItem, sLineItemId, sOrderDate);

          String strChargeCategory = "LinePrice";

          String sPrice = GCXMLUtil.getAttributeFromXPath(eleOrderLine,
              "LinePriceInfo/@UnitPrice");

          double dPrice = (!YFCCommon.isVoid(sPrice)) ? Double
              .valueOf(sPrice) : 0.00;
              double dChargeAmount = dPrice * dOrderedQty;

              LOGGER.debug(" Inside appendPriceToVertexIP(), sPrice" + sPrice
                  + " dChargeAmount" + dChargeAmount + " dPrice " + dPrice
                  + " sOrderedQty " + sOrderedQty);

              NodeList nlPromotion = XPathAPI.selectNodeList(eleOrderLine,
                  "LineCharges/LineCharge[@ChargeCategory='Promotion']");
              for (int iCounter = 0; iCounter < nlPromotion.getLength(); iCounter++) {
                LOGGER.debug(" Looping on Promotion Charges ");
                Element elePromotion = (Element) nlPromotion.item(iCounter);
                String sPromotion = elePromotion
                    .getAttribute(GCConstants.CHARGE_PER_LINE);
                double dPromotion = (!YFCCommon.isVoid(sPromotion)) ? Double
                    .valueOf(sPromotion) : 0.00;
                    dChargeAmount = dChargeAmount - dPromotion;
                    dPrice = dChargeAmount / dOrderedQty;
                    LOGGER.debug(" Inside loop, dPrice" + dPrice + " dChargeAmount"
                        + dChargeAmount);
              }
              LOGGER.debug(" sOrderLineKey" + sOrderLineKey + " dChargeAmount"
                  + dChargeAmount);
              eQuotationRequest.appendChild(eleLineItem);
              GCRecalculateLineTaxUE.addProductInfo(docVertexCallIP, eleLineItem,
                  strChargeCategory, sItemID, sTaxProductCode);
              Element eleQuantity = GCXMLUtil.createElement(docVertexCallIP,
                  "Quantity", null);
              eleQuantity.setTextContent(sOrderedQty);
              eleLineItem.appendChild(eleQuantity);

              Element eleFreight = GCXMLUtil.createElement(docVertexCallIP,
                  "Freight", null);
              eleFreight.setTextContent("0");
              eleLineItem.appendChild(eleFreight);

              Element eleUnitPrice = GCXMLUtil.createElement(docVertexCallIP,
                  "UnitPrice", null);
              eleUnitPrice.setTextContent(String.valueOf(dPrice));
              eleLineItem.appendChild(eleUnitPrice);

              Element eleExtendedPrice = GCXMLUtil.createElement(docVertexCallIP,
                  "ExtendedPrice", null);
              eleExtendedPrice.setTextContent(String.valueOf(dChargeAmount));
              eleLineItem.appendChild(eleExtendedPrice);

              Element eleCustomer = GCXMLUtil.createElement(docVertexCallIP,
                  "Customer", null);
              eleCustomer.setAttribute("isTaxExempt", sTaxExemptFlag);
              eleLineItem.appendChild(eleCustomer);

              Element eleCustomerCode = GCXMLUtil.createElement(docVertexCallIP,
                  "CustomerCode", null);
              eleCustomerCode.setAttribute("classCode", "MAIN");
              eleCustomer.appendChild(eleCustomerCode);

              if (YFCCommon.equalsIgnoreCase(sEntryType, "mPOS")) {
                Element eleDestination = GCXMLUtil.createElement(docVertexCallIP, "Destination", null);
                eleCustomer.appendChild(eleDestination);
                if (YFCCommon.equalsIgnoreCase(eleOrderLine.getAttribute(GCConstants.DELIVERY_METHOD), "CARRY")) {
                  Element eleStoreAddress =
                      (Element) storeAddress.getDocument().getElementsByTagName("CorporatePersonInfo").item(0);
                  appendPersonInfoShipTo(docVertexCallIP, eleStoreAddress, eleDestination);
                } else {

                  appendPersonInfoShipTo(docVertexCallIP, eleHeaderPersonInfoShipTo, eleDestination);
                }
              } else {

                Element elePersonInfoShipTo = (Element) XPathAPI.selectSingleNode(eleOrderLine, "PersonInfoShipTo");
                if (!YFCCommon.isVoid(elePersonInfoShipTo)) {
                  Element eleDestination = GCXMLUtil.createElement(docVertexCallIP, "Destination", null);
                  eleCustomer.appendChild(eleDestination);
                  appendPersonInfoShipTo(docVertexCallIP, elePersonInfoShipTo, eleDestination);
                }
              }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCProcessTaxCalculationsAPI.appendPriceToVertexIP() ",
          e);
      throw new GCException(e);
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" GCProcessTaxCalculationsAPI.appendPriceToVertexIP() method, updated docVertexCallIP is"
          + GCXMLUtil.getXMLString(docVertexCallIP));
    }
    LOGGER.debug(" Exiting GCProcessTaxCalculationsAPI.appendPriceToVertexIP() method ");
  }

  private void addLineItemAttributes(Element eleLineItem, String sLineItemId,
      String sDate) throws GCException {
    LOGGER.debug(" Entering GCProcessTaxCalculationsAPI.addLineItemAttributes() method ");
    LOGGER.debug("sLineItemId" + sLineItemId + " sDate" + sDate);
    try {
      eleLineItem.setAttribute("lineItemId", sLineItemId);
      eleLineItem.setAttribute("isMulticomponent", "false");
      eleLineItem.setAttribute("lineItemNumber",
          String.valueOf(iLineItemNumber));
      eleLineItem.setAttribute("taxdate", sDate);
      iLineItemNumber = iLineItemNumber + 1;
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCProcessTaxCalculationsAPI.addLineItemAttributes() ",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCProcessTaxCalculationsAPI.addLineItemAttributes() method ");
  }

  /**
   *
   * Description of modifyInDoc this method appends all the required elements and attributes in the inDoc which are missing
   *
   * @param env
   * @param inDoc
   * @return
   * @throws TransformerException TransformerException
   * @throws GCException  TransformerException
   *
   */
  private Document modifyInDoc(YFSEnvironment env, Document inDoc) throws TransformerException, GCException{
    LOGGER.debug(" Entering modifyInDoc() method"
        + GCXMLUtil.getXMLString(inDoc));

    Element eleOrder = inDoc.getDocumentElement();
    String sOrderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
    String sAPIName=eleOrder.getAttribute("APIName");
    // GCSTORE-2514 : Start
    Element eExtn = GCXMLUtil.getElementByXPath(inDoc, "Order/Extn");
    if (!YFCCommon.isVoid(eExtn)
        && YFCCommon.equals(GCConstants.ORDER_CATURE_SYSTEM_ATG, eExtn.getAttribute(GCXmlLiterals.EXTN_SOURCE_SYSTEM))) {
      return inDoc;
    }
    // GCSTORE-2514 : End
    // OMS-5269
    if (YFCCommon.isVoid(sOrderHeaderKey)) {
      if (YFCCommon.equals(GCConstants.EXCHANGE, eleOrder.getAttribute(GCConstants.ORDER_PURPOSE)) || YFCCommon.equals(sAPIName, "copyOrder")) {
        return inDoc;
      }
      throw new GCException("System error, please try again. If the error persists contact system administrator");
    }
    // OMS-5269
    Document docGetOrderListIP = GCXMLUtil
        .createDocument(GCConstants.ORDER);
    Element eleGetOrderListIP = docGetOrderListIP
        .getDocumentElement();
    eleGetOrderListIP.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    Document docGetOrderListTmp = GCCommonUtil
        .getDocumentFromPath("/global/template/api/getOrderList.xml");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" modifyInDoc() method, docGetOrderListTmp is"
          + GCXMLUtil.getXMLString(docGetOrderListTmp));
    }

   Document docGetOrderListOP =
      GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST, docGetOrderListIP, docGetOrderListTmp);
   
            
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" modifyInDoc() method, docGetOrderListOP is" + GCXMLUtil.getXMLString(docGetOrderListOP));
    }

    NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc,
        "Order/OrderLines/OrderLine");
    int count=nlOrderLine.getLength();
    for(int iLineCounter=0;iLineCounter<count;iLineCounter++){
      Element eleOrderLine = (Element) nlOrderLine
          .item(iLineCounter);
      String sOrderLineKey=eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      // Fix for GCSTORE-4162 : Skipping adding line if Line is FreeGiftLine
      Element eleExtn = (Element) eleOrderLine.getElementsByTagName(GCConstants.EXTN).item(0);
      String sExtnIsFreeGiftItem = eleExtn.getAttribute(GCConstants.EXTN_IS_FREE_GIFT_ITEM);
      
      if(!YFCCommon.equals(sExtnIsFreeGiftItem, GCConstants.YES)){
    	  Element eleOrderLineGetOrderList=(Element)XPathAPI.selectSingleNode(docGetOrderListOP,"OrderList/Order/OrderLines/OrderLine[@OrderLineKey='"+sOrderLineKey +"']");

    	  //fetching LinePriceInfo
    	  Element eleLinePriceInfo=(Element)XPathAPI.selectSingleNode(eleOrderLine,"LinePriceInfo");
    	  if(YFCCommon.isVoid(eleLinePriceInfo)){
    		  Element eleLinePriceInfoGetOrderList=(Element)XPathAPI.selectSingleNode(eleOrderLineGetOrderList,"LinePriceInfo");
    		  eleLinePriceInfo=GCXMLUtil.createElement(inDoc, "LinePriceInfo", null);
    		  eleLinePriceInfo.setAttribute(GCConstants.IS_PRICE_LOCKED, eleLinePriceInfoGetOrderList.getAttribute(GCConstants.IS_PRICE_LOCKED));
    		  eleLinePriceInfo.setAttribute(GCConstants.UNIT_PRICE, eleLinePriceInfoGetOrderList.getAttribute(GCConstants.UNIT_PRICE));
    		  eleLinePriceInfo.setAttribute(GCConstants.LIST_PRICE, eleLinePriceInfoGetOrderList.getAttribute(GCConstants.LIST_PRICE));
    		  eleOrderLine.appendChild(eleLinePriceInfo);
    	  }

    	  //fetching itemDetails
    	  Element eleItemDetails=(Element)XPathAPI.selectSingleNode(eleOrderLine,"ItemDetails");
    	  if(YFCCommon.isVoid(eleItemDetails)){
    		  Element eleItemDetailsGetOrderList=(Element)XPathAPI.selectSingleNode(eleOrderLineGetOrderList,"ItemDetails");
    		  Element eleClassificationCodesGetOrderList=(Element)XPathAPI.selectSingleNode(eleItemDetailsGetOrderList,"ClassificationCodes");
    		  eleItemDetails=GCXMLUtil.createElement(inDoc, "ItemDetails", null);
    		  eleOrderLine.appendChild(eleItemDetails);
    		  Element eleClassificationCodes=GCXMLUtil.createElement(inDoc, "ClassificationCodes", null);
    		  eleItemDetails.appendChild(eleClassificationCodes);
    		  eleClassificationCodes.setAttribute(GCConstants.TAX_PRODUCT_CODE, eleClassificationCodesGetOrderList.getAttribute(GCConstants.TAX_PRODUCT_CODE));
    	  }else if(!YFCCommon.isVoid(eleItemDetails)){
    		  Element eleClassificationCodes=(Element)XPathAPI.selectSingleNode(eleItemDetails,"ClassificationCodes");
    		  if(YFCCommon.isVoid(eleClassificationCodes)){
    			  Element eleClassificationCodesGetOrderList=(Element)XPathAPI.selectSingleNode(eleOrderLineGetOrderList,"ItemDetails/ClassificationCodes");
    			  eleClassificationCodes = GCXMLUtil.createElement(inDoc, "ClassificationCodes", null);
    			  eleItemDetails.appendChild(eleClassificationCodes);
    			  eleClassificationCodes.setAttribute(GCConstants.TAX_PRODUCT_CODE, eleClassificationCodesGetOrderList.getAttribute(GCConstants.TAX_PRODUCT_CODE));
    		  }
    	  }
      }
      
    }
    //fetching PersonInfoShipTo
    // Putting condition for mPOS --> as mPOS dont have Person Info Addresses

    if (!YFCCommon.equalsIgnoreCase("mPOS", eleOrder.getAttribute(GCConstants.ENTRY_TYPE))) {
      Element elePersonInfoShipTo=(Element)XPathAPI.selectSingleNode(eleOrder, "PersonInfoShipTo");
      if(YFCCommon.isVoid(elePersonInfoShipTo)){
        Element elePersonInfoShipToGetOrderList=(Element)XPathAPI.selectSingleNode(docGetOrderListOP,"OrderList/Order/PersonInfoShipTo");
        elePersonInfoShipTo=GCXMLUtil.createElement(inDoc, "PersonInfoShipTo", null);
        eleOrder.appendChild(elePersonInfoShipTo);
        elePersonInfoShipTo.setAttribute(GCConstants.ADDRESS_LINE1, elePersonInfoShipToGetOrderList.getAttribute(GCConstants.ADDRESS_LINE1));
        elePersonInfoShipTo.setAttribute(GCConstants.ADDRESS_LINE2, elePersonInfoShipToGetOrderList.getAttribute(GCConstants.ADDRESS_LINE2));
        elePersonInfoShipTo.setAttribute(GCConstants.ADDRESS_LINE3, elePersonInfoShipToGetOrderList.getAttribute(GCConstants.ADDRESS_LINE3));
        elePersonInfoShipTo.setAttribute("AddressLine4", elePersonInfoShipToGetOrderList.getAttribute("AddressLine4"));
        elePersonInfoShipTo.setAttribute("AddressLine5", elePersonInfoShipToGetOrderList.getAttribute("AddressLine5"));
        elePersonInfoShipTo.setAttribute("AddressLine6", elePersonInfoShipToGetOrderList.getAttribute("AddressLine6"));
        elePersonInfoShipTo.setAttribute("AlternateEmailID", elePersonInfoShipToGetOrderList.getAttribute("AlternateEmailID"));
        elePersonInfoShipTo.setAttribute("City", elePersonInfoShipToGetOrderList.getAttribute("City"));
        elePersonInfoShipTo.setAttribute("Beeper", elePersonInfoShipToGetOrderList.getAttribute("Beeper"));
        elePersonInfoShipTo.setAttribute("Company", elePersonInfoShipToGetOrderList.getAttribute("Company"));
        elePersonInfoShipTo.setAttribute("Country", elePersonInfoShipToGetOrderList.getAttribute("Country"));
        elePersonInfoShipTo.setAttribute("DayFaxNo", elePersonInfoShipToGetOrderList.getAttribute("DayFaxNo"));
        elePersonInfoShipTo.setAttribute("DayPhone", elePersonInfoShipToGetOrderList.getAttribute("DayPhone"));
        elePersonInfoShipTo.setAttribute("Department", elePersonInfoShipToGetOrderList.getAttribute("Department"));
        elePersonInfoShipTo.setAttribute("EMailID", elePersonInfoShipToGetOrderList.getAttribute("EMailID"));
        elePersonInfoShipTo.setAttribute("EveningFaxNo", elePersonInfoShipToGetOrderList.getAttribute("EveningFaxNo"));
        elePersonInfoShipTo.setAttribute("EveningPhone", elePersonInfoShipToGetOrderList.getAttribute("EveningPhone"));
        elePersonInfoShipTo.setAttribute("IsAddressVerified", elePersonInfoShipToGetOrderList.getAttribute("IsAddressVerified"));
        elePersonInfoShipTo.setAttribute("IsCommercialAddress", elePersonInfoShipToGetOrderList.getAttribute("IsCommercialAddress"));
        elePersonInfoShipTo.setAttribute("JobTitle", elePersonInfoShipToGetOrderList.getAttribute("JobTitle"));
        elePersonInfoShipTo.setAttribute("LastName", elePersonInfoShipToGetOrderList.getAttribute("LastName"));
        elePersonInfoShipTo.setAttribute("MiddleName", elePersonInfoShipToGetOrderList.getAttribute("MiddleName"));
        elePersonInfoShipTo.setAttribute("MobilePhone", elePersonInfoShipToGetOrderList.getAttribute("MobilePhone"));
        elePersonInfoShipTo.setAttribute("OtherPhone", elePersonInfoShipToGetOrderList.getAttribute("OtherPhone"));
        elePersonInfoShipTo.setAttribute("PersonID", elePersonInfoShipToGetOrderList.getAttribute("PersonID"));
        elePersonInfoShipTo.setAttribute("PersonInfoKey", elePersonInfoShipToGetOrderList.getAttribute("PersonInfoKey"));
        elePersonInfoShipTo.setAttribute("State", elePersonInfoShipToGetOrderList.getAttribute("State"));
        elePersonInfoShipTo.setAttribute("Suffix", elePersonInfoShipToGetOrderList.getAttribute("Suffix"));
        elePersonInfoShipTo.setAttribute("Title", elePersonInfoShipToGetOrderList.getAttribute("Title"));
        elePersonInfoShipTo.setAttribute("ZipCode", elePersonInfoShipToGetOrderList.getAttribute("ZipCode"));

        Element eleExtn=GCXMLUtil.createElement(inDoc, "Extn", null);
        elePersonInfoShipTo.appendChild(eleExtn);

        Element eleExtnGetOrderList = (Element) docGetOrderListOP.getElementsByTagName(GCConstants.EXTN).item(0);
        eleExtn.setAttribute("ExtnIsAPOFPO", eleExtnGetOrderList.getAttribute("ExtnIsAPOFPO"));
        eleExtn.setAttribute("ExtnIsAPOFPODefault", eleExtnGetOrderList.getAttribute("ExtnIsAPOFPODefault"));
        eleExtn.setAttribute("ExtnIsPOBox", eleExtnGetOrderList.getAttribute("ExtnIsPOBox"));
      }

    //fetching PersonInfoShipTo
    Element elePersonInfoBillTo=(Element)XPathAPI.selectSingleNode(eleOrder, "PersonInfoBillTo");
    if(YFCCommon.isVoid(elePersonInfoBillTo)){
      Element elePersonInfoBillToGetOrderList=(Element)XPathAPI.selectSingleNode(docGetOrderListOP,"OrderList/Order/PersonInfoBillTo");
      elePersonInfoBillTo=GCXMLUtil.createElement(inDoc, "PersonInfoBillTo", null);
      eleOrder.appendChild(elePersonInfoBillTo);
      elePersonInfoBillTo.setAttribute(GCConstants.ADDRESS_LINE1, elePersonInfoBillToGetOrderList.getAttribute(GCConstants.ADDRESS_LINE1));
      elePersonInfoBillTo.setAttribute(GCConstants.ADDRESS_LINE2, elePersonInfoBillToGetOrderList.getAttribute(GCConstants.ADDRESS_LINE2));
      elePersonInfoBillTo.setAttribute(GCConstants.ADDRESS_LINE3, elePersonInfoBillToGetOrderList.getAttribute(GCConstants.ADDRESS_LINE3));
      elePersonInfoBillTo.setAttribute("AddressLine4", elePersonInfoBillToGetOrderList.getAttribute("AddressLine4"));
      elePersonInfoBillTo.setAttribute("AddressLine5", elePersonInfoBillToGetOrderList.getAttribute("AddressLine5"));
      elePersonInfoBillTo.setAttribute("AddressLine6", elePersonInfoBillToGetOrderList.getAttribute("AddressLine6"));
      elePersonInfoBillTo.setAttribute("AlternateEmailID", elePersonInfoBillToGetOrderList.getAttribute("AlternateEmailID"));
      elePersonInfoBillTo.setAttribute("City", elePersonInfoBillToGetOrderList.getAttribute("City"));
      elePersonInfoBillTo.setAttribute("Beeper", elePersonInfoBillToGetOrderList.getAttribute("Beeper"));
      elePersonInfoBillTo.setAttribute("Company", elePersonInfoBillToGetOrderList.getAttribute("Company"));
      elePersonInfoBillTo.setAttribute("Country", elePersonInfoBillToGetOrderList.getAttribute("Country"));
      elePersonInfoBillTo.setAttribute("DayFaxNo", elePersonInfoBillToGetOrderList.getAttribute("DayFaxNo"));
      elePersonInfoBillTo.setAttribute("DayPhone", elePersonInfoBillToGetOrderList.getAttribute("DayPhone"));
      elePersonInfoBillTo.setAttribute("Department", elePersonInfoBillToGetOrderList.getAttribute("Department"));
      elePersonInfoBillTo.setAttribute("EMailID", elePersonInfoBillToGetOrderList.getAttribute("EMailID"));
      elePersonInfoBillTo.setAttribute("EveningFaxNo", elePersonInfoBillToGetOrderList.getAttribute("EveningFaxNo"));
      elePersonInfoBillTo.setAttribute("EveningPhone", elePersonInfoBillToGetOrderList.getAttribute("EveningPhone"));
      elePersonInfoBillTo.setAttribute("IsAddressVerified", elePersonInfoBillToGetOrderList.getAttribute("IsAddressVerified"));
      elePersonInfoBillTo.setAttribute("JobTitle", elePersonInfoBillToGetOrderList.getAttribute("JobTitle"));
      elePersonInfoBillTo.setAttribute("LastName", elePersonInfoBillToGetOrderList.getAttribute("LastName"));
      elePersonInfoBillTo.setAttribute("MiddleName", elePersonInfoBillToGetOrderList.getAttribute("MiddleName"));
      elePersonInfoBillTo.setAttribute("MobilePhone", elePersonInfoBillToGetOrderList.getAttribute("MobilePhone"));
      elePersonInfoBillTo.setAttribute("OtherPhone", elePersonInfoBillToGetOrderList.getAttribute("OtherPhone"));
      elePersonInfoBillTo.setAttribute("PersonID", elePersonInfoBillToGetOrderList.getAttribute("PersonID"));
      elePersonInfoBillTo.setAttribute("PersonInfoKey", elePersonInfoBillToGetOrderList.getAttribute("PersonInfoKey"));
      elePersonInfoBillTo.setAttribute("State", elePersonInfoBillToGetOrderList.getAttribute("State"));
      elePersonInfoBillTo.setAttribute("Suffix", elePersonInfoBillToGetOrderList.getAttribute("Suffix"));
      elePersonInfoBillTo.setAttribute("Title", elePersonInfoBillToGetOrderList.getAttribute("Title"));
      elePersonInfoBillTo.setAttribute("ZipCode", elePersonInfoBillToGetOrderList.getAttribute("ZipCode"));
      }
    }
    LOGGER.debug(" Exiting modifyInDoc() method. Return doc is " + GCXMLUtil.getXMLString(inDoc));
    return inDoc;
  }
  @Override
  public void setProperties(Properties arg0) throws GCException {

  }

}
