/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               06/02/2014        Last Name, First Name		JIRA No: Description of issue.
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.util.Map;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.yantra.ycp.japi.YCPDynamicConditionEx;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:email Address">Name</a>
 */
public class GCPublishCustomerCondition implements YCPDynamicConditionEx {

    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCPublishCustomerCondition.class.getName());

    @Override
    public boolean evaluateCondition(YFSEnvironment env, String arg1, Map arg2,
	    Document inDoc) {

	String sEnvObject = (String) env.getTxnObject("Publish");
	if (!YFCCommon.isVoid(sEnvObject)) {
	    LOGGER.debug("InsideCondition");
	    if (sEnvObject.equalsIgnoreCase(GCConstants.NO)) {
		LOGGER.debug("Do not publish");
		return false;
	    }
	}
	return true;
    }

  @Override
  public void setProperties(Map arg0) {

    }

}
