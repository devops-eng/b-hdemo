/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class make changes to the invoice publish in case of Store only set item
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               07/05/2014        Soni, Karan		        Initial Version
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author karan
 */

public class GCPublishInvoice {

    // initialize logger
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCPublishInvoice.class);
	private Properties props;
    
    public Document sendInvoice(YFSEnvironment env, Document inDoc)
	    throws GCException {
	try {
	    LOGGER.verbose("Entering the sendInvoice method");
	    NodeList nlOrderLine = GCXMLUtil
		    .getNodeListByXpath(
			    inDoc,
			    "InvoiceDetail/InvoiceHeader/LineDetails/LineDetail/OrderLine/ItemDetails/PrimaryInformation[@KitCode='BUNDLE']/../Extn[@ExtnSetCode='1']/../..");
	    for (int i = 0; i < nlOrderLine.getLength(); i++) {
		LOGGER.debug("Entering the for loop of ExtnSetCode equals 1");
		Element eleOrderLine = (Element) nlOrderLine.item(i);
		String strOrderLineKey = eleOrderLine
			.getAttribute(GCConstants.ORDER_LINE_KEY);
		LOGGER.debug("The orderLineKey is " + strOrderLineKey);
		NodeList nlOrderLn = GCXMLUtil
			.getNodeListByXpath(
				inDoc,
				"InvoiceDetail/InvoiceHeader/LineDetails/LineDetail/OrderLine[@BundleParentOrderLineKey='"
					+ strOrderLineKey + "']");
		for (int j = 0; j < nlOrderLn.getLength(); j++) {
		    LOGGER.debug("Entering the for loop of ExtnSetCode equals 1: child");
		    Element eleOL = (Element) nlOrderLn.item(j);
		    Element eleLineDetail = (Element) XPathAPI
			    .selectSingleNode(eleOL, "..");
		    eleLineDetail.getParentNode().removeChild(eleLineDetail);
		    LOGGER.debug("Exiting the for loop of ExtnSetCode equals 1: child");
		}
		LOGGER.debug("Exiting the for loop of ExtnSetCode equals 1");
	    }

	    NodeList nlLineDetail = GCXMLUtil
		    .getNodeListByXpath(
			    inDoc,
			    "InvoiceDetail/InvoiceHeader/LineDetails/LineDetail/OrderLine/ItemDetails/PrimaryInformation[@KitCode='BUNDLE']/../Extn[@ExtnSetCode!='1']/../../..");
	    for (int i = 0; i < nlLineDetail.getLength(); i++) {
		LOGGER.debug("Entering the for loop of ExtnSetCode not equals 1");
		Element eleLineDetail = (Element) nlLineDetail.item(i);
		Element eleLineCharges = (Element) XPathAPI.selectSingleNode(
			eleLineDetail, "LineCharges");
		eleLineCharges.getParentNode().removeChild(eleLineCharges);
		Element eleLineTaxes = (Element) XPathAPI.selectSingleNode(
			eleLineDetail, "LineTaxes");
		eleLineTaxes.getParentNode().removeChild(eleLineTaxes);
		LOGGER.debug("Exiting the for loop of ExtnSetCode not equals 1");
	    }
	    LOGGER.verbose("Exiting the sendInvoice method");
	} catch (Exception e) {
	    LOGGER.error("Inside GCPublishInvoice.sendInvoice():",e);
	    throw new GCException(e);
	}
	return inDoc;
    }
    
    
    /**
     * This method is used to stamp the Credit Memo on the Return Invoice.
     * 
     * @param env
     * @param inDoc
     * @return
     * @throws ParseException
     */
    public Document stampCreditMemo(YFSEnvironment env, Document inDoc) throws ParseException{
      
      LOGGER.beginTimer("GCPublishInvoice.stampCreditMemo");
      LOGGER.verbose("Class: GCPublishInvoice.stampCreditMemo START");
      
      String sOrderType="";
      String sOrderDate="";
      String sInvoiceNo="";
      boolean isStampingReqd = false;
      
      YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
      
      if(LOGGER.isDebugEnabled()){
        LOGGER.verbose("Input doc to GCPublishInvoice.stampCreditMemo method is:--> " + inputDoc.toString());
      }
      
      YFCElement eleOrder = inputDoc.getElementsByTagName(GCConstants.ORDER).item(0);
      String sDocumentType = eleOrder.getAttribute(GCConstants.DOCUMENT_TYPE);
      String sOrderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
      if(YFCCommon.equalsIgnoreCase(sDocumentType, GCConstants.RETURN_ORDER_DOCUMENT_TYPE)){
        YFCDocument getOrdListInDoc = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderHeaderKey+"'/>");
        YFCDocument getOrdListTemp = YFCDocument.getDocumentFor("<OrderList><Order Createts='' OrderType='' OrderNo=''/></OrderList>");
        YFCDocument getOrdListOutDoc = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrdListInDoc, getOrdListTemp);
        
        if(LOGGER.isDebugEnabled()){
          LOGGER.verbose("GetOrderList output :--> " + getOrdListOutDoc.toString());
        }
        
        YFCElement eleReturnOrder = getOrdListOutDoc.getElementsByTagName(GCConstants.ORDER).item(0);
        if(!YFCCommon.isVoid(eleReturnOrder)){
          sOrderType = eleReturnOrder.getAttribute(GCConstants.ORDER_TYPE);
          sOrderDate = eleReturnOrder.getAttribute(GCConstants.CREATE_TS);
        }
        if(YFCCommon.equalsIgnoreCase(sOrderType, GCConstants.RETURN_WITH_ADV_CREDIT)){
          YFCElement eleRoot = inputDoc.getDocumentElement();
          YFCElement eleInvoiceHeader = eleRoot.getElementsByTagName(GCConstants.INVOICE_HEADER).item(0);
          String sSalesOrderHeaderKey = eleInvoiceHeader.getAttribute(GCConstants.DERIVED_FROM_ORDER_HEADER_KEY);
          if(!YFCCommon.isStringVoid(sSalesOrderHeaderKey)){
            YFCDocument getInvoiceListDoc = YFCDocument.getDocumentFor("<OrderInvoice OrderHeaderKey='"+sSalesOrderHeaderKey+"'/>");
            YFCDocument getInvoiceListTemp = YFCDocument.getDocumentFor("<OrderInvoiceList><OrderInvoice DateInvoiced='' InvoiceNo='' InvoiceType=''/>"
                + "</OrderInvoiceList>");
            YFCDocument getInvoiceListOutDoc = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_INVOICE_LIST, getInvoiceListDoc, getInvoiceListTemp);
            
            if(LOGGER.isDebugEnabled()){
              LOGGER.verbose("GetOrderInvoiceList output :--> " + getInvoiceListOutDoc.toString());
            }
            
            YFCNodeList<YFCElement> nlOrderInvoice = getInvoiceListOutDoc.getElementsByTagName(GCConstants.ORDER_INVOICE);
            for(YFCElement eleOrderInvoice : nlOrderInvoice){
              String sInvoiceType = eleOrderInvoice.getAttribute("InvoiceType");
              if(YFCCommon.equalsIgnoreCase(sInvoiceType, GCConstants.CREDIT_MEMO)){
                String sInvoiceDate = eleOrderInvoice.getAttribute("DateInvoiced");
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                Date invoiceDate = format.parse(sInvoiceDate);
                Date orderDate = format.parse(sOrderDate);
                double timegap = (double)((invoiceDate.getTime()/60000) - (orderDate.getTime()/60000));
                String sTimeGap = props.getProperty("TIME_GAP");
                if(timegap<=Double.parseDouble(sTimeGap) && timegap>=0.0){
                  sInvoiceNo = eleOrderInvoice.getAttribute("InvoiceNo");
                  isStampingReqd = true;
                  break;
                }
              }
            }
          }
        }
      }
      if (isStampingReqd) {
        YFCElement eleInvoiceHeader = inputDoc.getElementsByTagName(GCConstants.INVOICE_HEADER).item(0);
        YFCElement eleExtn = eleInvoiceHeader.getChildElement(GCConstants.EXTN);
        if (!YFCCommon.isVoid(eleExtn)) {
          eleExtn.setAttribute("ExtnCreditMemoNo", sInvoiceNo);
        } else {
          YFCElement eleNewExtn = eleInvoiceHeader.createChild(GCConstants.EXTN);
          eleNewExtn.setAttribute("ExtnCreditMemoNo", sInvoiceNo);
        }
      }
      
      if(LOGGER.isDebugEnabled()){
        LOGGER.verbose("Output doc to GCPublishInvoice.stampCreditMemo method is:--> " + inputDoc.toString());
      }
      return inDoc;
    }
    
    /**
     * This method is used to set the properties.
     */
    public void setProperties(final Properties props) {
      this.props = props;
    }
}
