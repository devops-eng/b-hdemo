package com.gc.api;

import org.apache.log4j.Logger;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCRaiseAlertAndNoteUponVertexDown {
	private static final Logger LOGGER = Logger.getLogger(GCRaiseAlertAndNoteUponVertexDown.class.getName());

	public void raiseAlertAndNote(YFSEnvironment env, Document inDoc) throws GCException {
		try {
      LOGGER.debug("Class: GCRaiseAlertAndNoteUponVertexDown Method: raiseAlertAndNote BEGIN");
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("raiseAlertAndNote method() - InDoc ::" + GCXMLUtil.getXMLString(inDoc));
      }

			Element eleRootOrder = inDoc.getDocumentElement();

			String sMaxOrderStatus = GCXMLUtil.getAttribute(eleRootOrder, GCConstants.MAX_ORDER_STATUS);
			String sSellingOrgCode = GCXMLUtil.getAttribute(eleRootOrder, GCConstants.SELLER_ORGANIZATION_CODE);

			if (GCConstants.GC.equalsIgnoreCase(sSellingOrgCode)&&!GCConstants.MAX_ORDER_STATUS_CODE.equals(sMaxOrderStatus)) {
				Element eleLineTax =
						(Element) XPathAPI.selectSingleNode(inDoc, "//Order/OrderLines/OrderLine/LineTaxes/LineTax[@Reference_3='"
								+ GCConstants.ERROR + "']");

				if (!YFCCommon.isVoid(eleLineTax)) {

					callChangeOrderToUpdateNotes(env, inDoc);

          GCCommonUtil.invokeService(env, GCConstants.GC_RAISE_VERTEX_FAILURE_ALERT, inDoc);
        }
      }
      LOGGER.debug("Class: GCRaiseAlertAndNoteUponVertexDown Method: raiseAlertAndNote END");
    }catch (Exception e) {
      LOGGER.error(" Inside catch block of GCRaiseAlertAndNoteUponVertexDown.raiseAlertAndNote(), Exception is :", e);
      throw new GCException(e);
    }

	}

	/**
	 * 
	 * @param env
	 * @param inDoc
	 * @param sOrderHeaderKey
	 * @throws Exception
	 */


	private void callChangeOrderToUpdateNotes(YFSEnvironment env, Document inDoc) throws GCException {
		try {
      LOGGER.debug("Class: GCRaiseAlertAndNoteUponVertexDown Method: callChangeOrderToUpdateNotes BEGIN");
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("callChangeOrderToUpdateNotes method() - InDoc ::" + GCXMLUtil.getXMLString(inDoc));
      }

			Element eleOrder = inDoc.getDocumentElement();

			Document docChangeOrderIp = GCXMLUtil.createDocument(GCConstants.ORDER);
			Element eleRootChangeOrderIp = docChangeOrderIp.getDocumentElement();

			eleRootChangeOrderIp.setAttribute(GCConstants.ORDER_HEADER_KEY,
					eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY));

			Element eleNotes = docChangeOrderIp.createElement(GCConstants.NOTES);
			eleRootChangeOrderIp.appendChild(eleNotes);

			Element eleNote = docChangeOrderIp.createElement(GCConstants.NOTE);
			eleNotes.appendChild(eleNote);
			eleNote.setAttribute(GCConstants.NOTE_TEXT, GCConstants.VERTEX_FAILURE_NOTE_TEXT);

			Element eleOrderLines = docChangeOrderIp.createElement(GCConstants.ORDER_LINES);
			eleRootChangeOrderIp.appendChild(eleOrderLines);

			NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc, "//Order//OrderLines//OrderLine");

			for (int iOrderCounter = 0; iOrderCounter < nlOrderLine.getLength(); iOrderCounter++){
				Element eleOrderLineInFor = (Element) nlOrderLine.item(iOrderCounter);
				String sPrimeLineNo = eleOrderLineInFor.getAttribute(GCConstants.PRIME_LINE_NO);
				String sSubLineNo = eleOrderLineInFor.getAttribute(GCConstants.SUB_LINE_NO);

				// Create LineTaxes element for change order input
				Element eleLineTaxesChangeOrderIp = docChangeOrderIp.createElement(GCConstants.LINE_TAXES);


				boolean bAddOrderLine = false;

				NodeList nlLineTax = eleOrderLineInFor.getElementsByTagName(GCConstants.LINE_TAX);
				for (int iLineTaxCounter = 0; iLineTaxCounter < nlLineTax.getLength(); iLineTaxCounter++) {


					Element eleLineTaxInFor = (Element) nlLineTax.item(iLineTaxCounter);

					String sRef3 = eleLineTaxInFor.getAttribute(GCConstants.REFERENCE_3);
					if (GCConstants.ERROR.equalsIgnoreCase(sRef3)){
						bAddOrderLine = true;
						String sChargeCategory = eleLineTaxInFor.getAttribute(GCConstants.CHARGE_CATEGORY);
						String sChargeName = eleLineTaxInFor.getAttribute(GCConstants.CHARGE_NAME);
						String sTaxName = eleLineTaxInFor.getAttribute(GCConstants.TAX_NAME);

						Element eleLineTaxChangeOrderIp = docChangeOrderIp.createElement(GCConstants.LINE_TAX);

						eleLineTaxChangeOrderIp.setAttribute(GCConstants.CHARGE_CATEGORY, sChargeCategory);
						eleLineTaxChangeOrderIp.setAttribute(GCConstants.CHARGE_NAME, sChargeName);
						eleLineTaxChangeOrderIp.setAttribute(GCConstants.TAX_NAME, sTaxName);
						eleLineTaxChangeOrderIp.setAttribute(GCConstants.REFERENCE3, "");
						eleLineTaxesChangeOrderIp.appendChild(eleLineTaxChangeOrderIp);
					}

				}

				if (bAddOrderLine) {

					// CREATING ELEMENT ORDERLINE
					Element eleOrderLineChangeOrderIp = docChangeOrderIp.createElement(GCConstants.ORDER_LINE);
					eleOrderLineChangeOrderIp.setAttribute(GCConstants.PRIME_LINE_NO, sPrimeLineNo);
					eleOrderLineChangeOrderIp.setAttribute(GCConstants.SUB_LINE_NO, sSubLineNo);
					eleOrderLines.appendChild(eleOrderLineChangeOrderIp);

					eleOrderLineChangeOrderIp.appendChild(eleLineTaxesChangeOrderIp);
				}
			}

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("docChangeOrderip\n" + GCXMLUtil.getXMLString(docChangeOrderIp));
			}
			GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, docChangeOrderIp);

      LOGGER.debug("GCRaiseAlertAndNoteUponVertexDown " + "Method: callChangeOrderToUpdateNotes END");


    }catch (Exception e) {
      LOGGER.error(
"Inside GCRaiseAlertAndNoteUponVertexDown.callChangeOrderToUpdateNotes():",
          e);
      throw new GCException(e);
    }

	}
}