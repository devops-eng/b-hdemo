/**
 * Copyright � 2015, Infosys Ltd,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This Java class is called from GCKitCompBackorderAlert service, to raise  alert if any of the KIT components gets backordered
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               10/06/2015          Infosys Ltd		     JIRA No:5588
 *#################################################################################################################################################################
 */
package com.gc.api;

import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:email Address">Name</a>
 */

public class GCRaiseAlertOnKITCompBackOrder{

  final private String CLASSNAME = GCRaiseAlertOnKITCompBackOrder.class.getName();

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCRaiseAlertOnKITCompBackOrder.class);

  /**
   *
   * Description of raiseAlertOnKITComponentBackOrder:
   * this method checks if the orderline is a component of a Kit and
   * orderline status change is from 3350 to 1300(included in shipment to backOrdered) and call createException
   *
   * @param env
   * @param inDoc
   * @return
   * @throws GCException
   *
   */
  public Document raiseKITCompBackOrderAlert(YFSEnvironment env, Document inDoc) throws GCException {

	  if (LOGGER.isDebugEnabled()) {
		  LOGGER.debug(CLASSNAME + "Entering GCRaiseAlertOnKITCompBackOrder.raiseKITCompBackOrderAlert(), inDoc:"
				  + GCXMLUtil.getXMLString(inDoc));
	  }

    try {

      final String sDocType = GCXMLUtil.getAttributeFromXPath(inDoc, "Order/@DocumentType");

      if(GCConstants.SALES_ORDER_DOCUMENT_TYPE.equalsIgnoreCase(sDocType)){

        NodeList nlOrderLine=XPathAPI.selectNodeList(inDoc,"Order/OrderLines/OrderLine");

        int iOrderLineCount = nlOrderLine.getLength();

        Element eleFromOrderReleaseStatus =
            (Element) XPathAPI.selectSingleNode(inDoc,
                "Order/OrderLines/OrderLine/FromOrderReleaseStatuses/FromOrderReleaseStatus[@Status ="
                    + GCConstants.STATUS_INCLUDED_IN_SHIPMENT + "]");

        // int relStatusCount = nlFromOrderReleaseStatus.getLength();
        if (iOrderLineCount != 0) {

          // flag to invoke manageTaskQueue
          boolean bInvokeManageTaskQ = true;

          for (int iOrderLineCounter = 0; iOrderLineCounter < iOrderLineCount; iOrderLineCounter++) {

            // Fetch the OrderLine Element
            Element eleOrderLine = (Element) nlOrderLine.item(iOrderLineCounter);

            // Check if the order line is a KIT component
            final boolean isKitComponent = checkIfOrderLineKitComponent(eleOrderLine);

            // Check if it is a KIT component
            if (isKitComponent) {

              // Check if the From Order Release status is not blank

              if (!YFCCommon.isVoid(eleFromOrderReleaseStatus)) {

                // Get the ToOrderReleaseElement with status as 1300
                Element eleToOrderReleaseStatus =
                    (Element) XPathAPI.selectSingleNode(eleFromOrderReleaseStatus,
                        "ToOrderReleaseStatuses/ToOrderReleaseStatus[@Status =" + GCConstants.STATUS_BACKORDERED + "]");


                // check if ToOrderReleaseStatus is not blank
                if (!YFCCommon.isVoid(eleToOrderReleaseStatus)) {

                  // invoke the mangeTaskQ
                  if (bInvokeManageTaskQ) {

                    // remove any task q entry for shipment Invoice
                    removeAnyTaskQEntryForCurrentShipment(env);

                    // do not invoke the manage task q again
                    bInvokeManageTaskQ = false;
                  }

                  // Prepare the input for the create Exception API
                  Document docCreateExceptionIp =
                      prepareCreateExceptionIp(inDoc, eleOrderLine, eleToOrderReleaseStatus);

                  // Invoke create Exception Service with the input
                  callCreateExceptionSvc(env,docCreateExceptionIp);
                }
              }
            }
          }

        }
      }

    }

    catch (Exception e) {
      LOGGER.error("Exception is in Class: GCRaiseAlertOnKITCompBackOrder Method: raiseKITCompBackOrderAlert()", e);
      throw new GCException(e);
    }
    LOGGER.debug("Exiting GCRaiseAlertOnKITCompBackOrder.raiseKITCompBackOrderAlert()");
    return inDoc;
  }


  /**
   * <h3>Description:</h3>This method is used to remove any task queue entry present for the current
   * shipment for shipment invoice as it is backordered
   *
   * @param yfsEnv The yfs environment reference
   */
  private void removeAnyTaskQEntryForCurrentShipment(final YFSEnvironment yfsEnv) {

    // logging the method entry
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASSNAME + ":removeAnyTaskQEntryForCurrentShipment:entry");
    }

    // fetch the shipment key
    final String strShpKey = (String) yfsEnv.getTxnObject(GCConstants.CURRENT_TRANSACTION_SHIPMENT_KEY);

    // check if the shipment key is present
    if (StringUtils.isNotBlank(strShpKey)) {

      // prepare the input to manage task q
      final Document docManageTaskQIn = prepManageTaskQIn(strShpKey);

      // invoke the manage task q api to delete the entry and suppress any exception if any
      try {

        // prepare the manage task q input
        final Document docManageTaskQOut =
            GCCommonUtil.invokeService(yfsEnv, "GCManageTaskQueue_RemoveShipInvoice_svc", docManageTaskQIn);

        // log the output
        if (LOGGER.isDebugEnabled()) {

          LOGGER.debug(CLASSNAME + ":removeAnyTaskQEntryForCurrentShipment:Output of Manage Task q ="
              + GCXMLUtil.getXMLString(docManageTaskQOut));
        }

      } catch (final Exception excep) {

        // suppress the exception
        LOGGER
        .warn(
            CLASSNAME
            + ":removeAnyTaskQEntryForCurrentShipment:Suppressed the exception thrown during manage task queue invocation to remove the task q record for shipment invoice",
            excep);
      }
    } else {

      // log a warning
      LOGGER.warn(CLASSNAME
          + ":removeAnyTaskQEntryForCurrentShipment: Unable to fetch the shipment key from the transaction object!!!");
    }

    // logging the method exit
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASSNAME + ":removeAnyTaskQEntryForCurrentShipment:exit");
    }
  }


  /**
   * <h3>Description:</h3>This method is used to prepare the manageTaskQueue api input
   *
   * @param strShpKey The shipment key
   * @return document to be used for manageTask q api input
   */
  private Document prepManageTaskQIn(final String strShpKey) {

    // log the method entry
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASSNAME + ":prepManageTaskQIn:entry:Input=" + strShpKey);
    }

    // create the document
    final Document docManageTaskQIn = GCXMLUtil.createDocument(GCConstants.TASK_QUEUE);

    // populate the data key
    final Element eleTaskQueue = docManageTaskQIn.getDocumentElement();
    eleTaskQueue.setAttribute(GCConstants.DATA_KEY, strShpKey);

    // log the method exit
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASSNAME + ":prepManageTaskQIn:exit:Output=" + GCXMLUtil.getXMLString(docManageTaskQIn));
    }

    // return the document for manageTaskQueue api
    return docManageTaskQIn;
  }


  /**
   * @
   *
   * <h3>Description :</h3> This method is used to check if an order line is kit or not
   *
   *
   * @param eleOrderLine
   * @return
   * @throws TransformerException
   */
  private boolean checkIfOrderLineKitComponent(Element eleOrderLine) throws TransformerException {

    String strBundleParentOrderLineKey=eleOrderLine.getAttribute(GCConstants.BUNDLE_PARENT_ORDERLINEKEY);

    Element eleBundleParentLine = (Element) XPathAPI.selectSingleNode(eleOrderLine, GCConstants.BUNDLE_PARENT_LINE);

    if(!YFCCommon.isVoid(eleBundleParentLine) && !YFCCommon.isVoid(strBundleParentOrderLineKey)){
      return true;
    }
    else{
      return false;
    }

  }


  /**
   * <h3>Description :</h3> This method is used to prepare input XML for createExceptionSvc
   *
   * @param sOrderNo
   *            Order Number
   * @param sOrderLineKey
   *            order Line Key
   * @return The document is the input XML to createException Svc
   */
  public Document prepareCreateExceptionIp(Document inDoc, Element eleOrderLine, Element eleToOrderReleaseStatus) {

    // logging the method entry
    if( LOGGER.isDebugEnabled()){

      LOGGER.debug(CLASSNAME + ":prepareCreateExceptionIp:Entry");
    }
    // create a input document to createException Svc
    Document docCreateExceptionIp = GCXMLUtil.createDocument(GCConstants.INBOX);

    final String sOrderNo = GCXMLUtil.getAttributeFromXPath(inDoc, "Order/@OrderNo");
    final String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
    final String sOrderHeaderKey = GCXMLUtil.getAttributeFromXPath(inDoc, "Order/@OrderHeaderKey");
    final String sStatus = eleToOrderReleaseStatus.getAttribute(GCConstants.STATUS);
    final String sPrimeLineNo = eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
    final String sShipNode = GCXMLUtil.getAttributeFromXPath(eleToOrderReleaseStatus, "ToOrderLineSchedule/@ShipNode");
	final String sQuantity = eleToOrderReleaseStatus.getAttribute(GCConstants.STATUS_QTY);

    Element eleInbox = docCreateExceptionIp.getDocumentElement();

    // Set the Inbox Attributes
    eleInbox.setAttribute(GCConstants.EXCEPTION_TYPE,GCConstants.GC_KIT_COMP_BACKORDER_NOTIFICATION);
    eleInbox.setAttribute(GCConstants.ORDER_NO,sOrderNo );
    eleInbox.setAttribute(GCConstants.ORDER_LINE_KEY,sOrderLineKey);
    eleInbox.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    eleInbox.setAttribute(GCConstants.CONSOLIDATE, GCConstants.YES);
    eleInbox.setAttribute(GCConstants.SHIP_NODEKEY, sShipNode);
	
	//Create the Inbox ReferenceList
	final Element eleInboxReferenceList = docCreateExceptionIp.createElement(GCConstants.INBOX_REFERENCE_LIST);

	//Create the Inobx Reference
    Element eleInboxReferences = docCreateExceptionIp.createElement(GCConstants.INBOX_REFERENCES);

	//Adding the reference for backorder quantity
    eleInboxReferences.setAttribute(GCConstants.NAME, GCConstants.BACKORDERED_QTY);
    eleInboxReferences.setAttribute(GCConstants.VALUE, sQuantity);
	eleInboxReferences.setAttribute(GCConstants.REFERENCE_TYPE, GCConstants.TEXT);
      
    eleInboxReferenceList.appendChild(eleInboxReferences);
    eleInbox.appendChild(eleInboxReferenceList);

    // logging the input to createExceptionSvc
    if( LOGGER.isDebugEnabled()){
      LOGGER.debug("The input XML prepared for createExceptionSvc::"+ GCXMLUtil.getXMLString(docCreateExceptionIp));
    }

    // logging the method exit
    if( LOGGER.isDebugEnabled()){

      LOGGER.debug(CLASSNAME + ":prepareCreateExceptionIp:Exit");
    }

    // returning the input document prepared
    return docCreateExceptionIp;

  }


  /**
   * <h3>Description :</h3> This method is used to call createExceptionSvc
   *
   * @param env
   *            The Environment reference
   * @param docCreateExceptionIp
   *            createExceptionSvc Input
   */


  private void callCreateExceptionSvc(YFSEnvironment env, Document docCreateExceptionIp) throws GCException{

    try{
      // logging the method entry
      if( LOGGER.isDebugEnabled()){

        LOGGER.debug(CLASSNAME + ":callCreateExceptionSvc::Entry");
      }

      // logging the input to the API
      if( LOGGER.isDebugEnabled()){

        LOGGER.debug("The input going to CreateExceptionSvc " + GCXMLUtil.getXMLString(docCreateExceptionIp));
      }

      //call CreateExceptionSvc
      final Document docCreateExceptionOp =
          GCCommonUtil.invokeService(env, GCConstants.CREATE_EXCEPTION_SVC, docCreateExceptionIp);

      // logging the output from the API
      if( LOGGER.isDebugEnabled()){

        LOGGER.debug("The output from CreateExceptionSvc " + GCXMLUtil.getXMLString(docCreateExceptionOp));
      }

      // logging the method exit
      if( LOGGER.isDebugEnabled()){

        LOGGER.debug(CLASSNAME + ":callCreateExceptionSvc:Exit");
      }
    }
    catch (Exception e) {
      LOGGER.error("The exception is in callCreateExceptionSvc method:", e);
      throw new GCException(e);
    }
  }

}
