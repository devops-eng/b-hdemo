/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0                          06/02/2014        Gowda, Naveen		JIRA No: This class is used to raise alert for all orders against the customer based
                                                                                         on Tax Exempt Request Date.
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:email Address">Naveen</a>
 */
public class GCRaiseAlertOnTaxRequestDecline {

    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCManageItem.class.getName());

    /**
     * 
     * Description of raiseAlertsForAllOrders
     * 
     * @param env
     * @param inDoc
     * @throws GCException
     * 
     */
    public void raiseAlertsForAllOrders(YFSEnvironment env, Document inDoc)
	    throws GCException {
	try {
	    LOGGER.debug("Class:GCRaiseAlertOnTaxRequestDecline "
		    + "Method: raiseAlertsForAllOrders BEGIN");
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("raiseAlertsForAllOrders() method - Input Document is"
			+ GCXMLUtil.getXMLString(inDoc));
	    }
	    Element eleInRoot = inDoc.getDocumentElement();
	    String sCustomerID = eleInRoot
		    .getAttribute(GCConstants.CUSTOMER_ID);
	    String sExtnTaxExemptReqDate = GCXMLUtil.getAttributeFromXPath(
		    inDoc, "//@ExtnTaxExemptReqDate");
	    SimpleDateFormat sdf = new SimpleDateFormat(
		    GCConstants.DATE_TIME_FORMAT);

	    // prepared input doc for get order list api
	    Document docGetOrderListIp = GCXMLUtil
		    .createDocument(GCConstants.ORDER);
	    Element eleGetOrderListIpRoot = docGetOrderListIp
		    .getDocumentElement();
	    eleGetOrderListIpRoot.setAttribute(GCConstants.BILL_TO_ID,
		    sCustomerID);
	    Element eleOrderOnHold = docGetOrderListIp
		    .createElement(GCConstants.ORDER_ON_HOLD);
	    eleOrderOnHold.setAttribute(GCConstants.HOLD_TYPE,
		    GCConstants.TAX_EXEMPT_HOLD);
	    eleOrderOnHold.setAttribute(GCConstants.STATUS,
		    GCConstants.APPLY_HOLD_CODE);
	    eleGetOrderListIpRoot.appendChild(eleOrderOnHold);
	    // prepared template doc for get order list api
	    Document docGetOrderListTemp = GCXMLUtil
		    .createDocument(GCConstants.ORDER_LIST);
	    Element eleGetOrderListTempRoot = docGetOrderListTemp
		    .getDocumentElement();
	    Element eleGetOrderListTempOrder = docGetOrderListTemp
		    .createElement(GCConstants.ORDER);
	    eleGetOrderListTempRoot.appendChild(eleGetOrderListTempOrder);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("docGetOrderListIp :: \n"
			+ GCXMLUtil.getXMLString(docGetOrderListIp));
	    }
	    Document docGetOrderListOp = GCCommonUtil.invokeAPI(env,
		    GCConstants.GET_ORDER_LIST, docGetOrderListIp,
		    docGetOrderListTemp);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("docGetOrderListOp :: \n"
			+ GCXMLUtil.getXMLString(docGetOrderListOp));
	    }
	    NodeList nlOrder = XPathAPI.selectNodeList(docGetOrderListOp,
		    "//Order");
	    for (int iOrderCounter = 0; iOrderCounter < nlOrder.getLength(); iOrderCounter++) {

		Element eleOrder = (Element) nlOrder.item(iOrderCounter);
		String sOrderNo = eleOrder.getAttribute(GCConstants.ORDER_NO);
		String sEnterpriseCode = eleOrder
			.getAttribute(GCConstants.ENTERPRISE_CODE);
		String sDocumentType = eleOrder
			.getAttribute(GCConstants.DOCUMENT_TYPE);
		String sOrderDate = eleOrder
			.getAttribute(GCConstants.ORDER_DATE);
		if (!YFCCommon.isVoid(sOrderDate)
			&& !YFCCommon.isVoid(sExtnTaxExemptReqDate)) {
		    Date dOrderdate = sdf.parse(sOrderDate);
		    Date dExtnTaxExemptReqDate = sdf
			    .parse(sExtnTaxExemptReqDate);
		    /*
		     * Raising alerts only for orders placed after Tax Exempt
		     * Request
		     */
		    if (dOrderdate.after(dExtnTaxExemptReqDate)) {
			LOGGER.debug("rasing alert for order::" + sOrderNo);
			raiseException(env, sOrderNo, sEnterpriseCode,
				sDocumentType);
		    }
		}
	    }
	    LOGGER.debug("Class:GCRaiseAlertOnTaxRequestDecline "
		    + "Method: raiseAlertsForAllOrders END");
	} catch (Exception e) {
	    LOGGER.error(
		    "Inside Class:GCRaiseAlertOnTaxRequestDecline.raiseAlertsForAllOrders():",
		    e);
	    throw new GCException(e);
	}
    }

    /**
     * 
     * @param env
     * @param sDocumentType
     * @param sEnterpriseCode
     * @param sErrorMessage
     * @throws Exception
     */
    private void raiseException(YFSEnvironment env, String sOrderNo,
	    String sEnterpriseCode, String sDocumentType) throws GCException {
	try {
	    LOGGER.debug("Class:GCRaiseAlertOnTaxRequestDecline Method: "
		    + "raiseException BEGIN");

	    Document docCreateExceptionIp = GCXMLUtil
		    .createDocument(GCConstants.INBOX);

	    Element eleRoot = docCreateExceptionIp.getDocumentElement();

	    eleRoot.setAttribute(GCConstants.EXCEPTION_TYPE,
		    GCConstants.GC_TAX_EXEMPT_DECLINE_NOTIFICATION);
	    eleRoot.setAttribute(GCConstants.QUEUE_ID,
		    GCConstants.GC_TAX_EXEMPT_DECLINE_ALERT_QUEUE);

	    Element eleInboxReferencesList = docCreateExceptionIp
		    .createElement(GCConstants.INBOX_REFERENCE_LIST);
	    eleRoot.appendChild(eleInboxReferencesList);

	    Element eleInboxReferences1 = docCreateExceptionIp
		    .createElement(GCConstants.INBOX_REFERENCES);
	    eleInboxReferencesList.appendChild(eleInboxReferences1);
	    eleInboxReferences1.setAttribute(GCConstants.NAME,
		    GCConstants.RESULT_MESSAGE);
	    eleInboxReferences1.setAttribute(GCConstants.VALUE,
		    GCConstants.GC_TAX_EXEMPT_DECLINE_NOTIFICATION_VALUE);
	    eleInboxReferences1.setAttribute(GCConstants.REFERENCE_TYPE,
		    GCConstants.TEXT);

	    Element eleOrder = docCreateExceptionIp
		    .createElement(GCConstants.ORDER);
	    eleRoot.appendChild(eleOrder);
	    eleOrder.setAttribute(GCConstants.ORDER_NO, sOrderNo);
	    eleOrder.setAttribute(GCConstants.ENTERPRISE_CODE, sEnterpriseCode);
	    eleOrder.setAttribute(GCConstants.DOCUMENT_TYPE, sDocumentType);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("docCreateExceptionIp\n"
			+ GCXMLUtil.getXMLString(docCreateExceptionIp));
	    }
	    GCCommonUtil.invokeAPI(env, GCConstants.API_CREATE_EXCEPTION,
		    docCreateExceptionIp);

	    LOGGER.debug("Class:GCRaiseAlertOnTaxRequestDecline "
		    + "Method: raiseException END");

	} catch (Exception e) {
	    LOGGER.error(
		    "Inside Class:GCRaiseAlertOnTaxRequestDecline.raiseException():",
		    e);
	    throw new GCException(e);
	}
    }
}