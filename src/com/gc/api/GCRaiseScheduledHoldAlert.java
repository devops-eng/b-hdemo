/**
 * Copyright � 2015, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This Java class is called from GCRaiseScheduledHoldAlert_svc.
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               *********         Infosys Ltd		    (Provide Description)
 *#################################################################################################################################################################
 */
package com.gc.api;


import java.util.Properties;

import javax.xml.transform.TransformerException;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:email Address">Name</a>
 */

public class GCRaiseScheduledHoldAlert implements YIFCustomApi{
	
	private static final String CLASSNAME = GCRaiseScheduledHoldAlert.class.getName();

	private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCRaiseScheduledHoldAlert.class);
	
	/**Method Description
	 *
	 * @param inDoc
	 * 
	 *@param env
	 *            The YFS Environment reference
	 * @throws GCException 
	 */
	
	public Document raiseScheduledHoldAlert(YFSEnvironment env, final Document inDoc) throws GCException, TransformerException{
	
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(CLASSNAME + ":raiseScheduledHoldAlert:Entry:input=" + GCXMLUtil.getXMLString(inDoc));
		}
	
	NodeList nlInbox=XPathAPI.selectNodeList(inDoc,"InboxList/Inbox");

	int iInboxCount = nlInbox.getLength();
	
	Element eleInbox =null;

	Document docInbox=null;

	for (int iInboxCounter = 0; iInboxCounter < iInboxCount; iInboxCounter++) {

		eleInbox= (Element) nlInbox.item(iInboxCounter);

		docInbox=GCXMLUtil.getDocumentFromElement(eleInbox);

		GCCommonUtil.invokeService(env, "GCRaiseExceptionOnUnscheduledSOLines_svc", docInbox);	

	}
	
	if (LOGGER.isDebugEnabled()) {
		
		LOGGER.debug(CLASSNAME +":raiseScheduledHoldAlert:Exit");
	}
	
	return inDoc;
	}
	 @Override
	public void setProperties(Properties arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}
	

}










