package com.gc.api;

import com.yantra.yfc.log.YFCLogCategory;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCRecalculateTax {

    // initialize logger
     private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCReceiveReturn.class);

    public Document recalculateTax(YFSEnvironment env, Document inDoc)
	    throws GCException {
	try {
	    String sIsTaxAvailable = GCConstants.NO;
	    Document docGetOrderListOp = GCCommonUtil.invokeAPI(env, inDoc,
		    GCConstants.API_GET_ORDER_LIST,
		    "/global/template/api/getOrderListForVertexCall.xml");

	    Document docProcessTaxCalculationsServiceIp = GCXMLUtil
		    .getDocument((Element) XPathAPI.selectSingleNode(
			    docGetOrderListOp, "//Order"), true);

	    Document docProcessTaxCalculationsServiceOp = GCCommonUtil
		    .invokeService(env, "GCProcessTaxCalculationsService",
			    docProcessTaxCalculationsServiceIp);

	    // check whether tax got calculated or not
	    Element eleLineTax = (Element) XPathAPI
		    .selectSingleNode(docProcessTaxCalculationsServiceOp,
			    "Order/OrderLines/OrderLine/LineTaxes/LineTax[@Refernce3!='ERROR']");

	    if (!YFCCommon.isVoid(eleLineTax)) {
		sIsTaxAvailable = GCConstants.YES;
		updateTaxOnTheOrder(env, docProcessTaxCalculationsServiceOp);
	    }
	    Document outDoc = GCXMLUtil.createDocument(GCConstants.ORDER);
	    Element outRoot = outDoc.getDocumentElement();
	    outRoot.setAttribute("IsTaxAvailable", sIsTaxAvailable);

	    return outDoc;
	} catch (Exception e) {
	    LOGGER.error("Inside GCRecalculateTax.recalculateTax():", e);
	    throw new GCException(e);
	}
    }

    /**
     * 
     * @param env
     * @param docProcessTaxCalculationsServiceOp
     * @throws Exception
     * @throws DOMException
     */
    private void updateTaxOnTheOrder(YFSEnvironment env,
	    Document docProcessTaxCalculationsServiceOp) throws GCException {
	try {
	    Document docChangeOrderIp = GCXMLUtil
		    .createDocument(GCConstants.ORDER);
	    Element eleChangeOrderIpRoot = docChangeOrderIp
		    .getDocumentElement();
	    eleChangeOrderIpRoot.setAttribute(GCConstants.ORDER_HEADER_KEY,
		    GCXMLUtil.getAttributeFromXPath(
			    docProcessTaxCalculationsServiceOp,
			    "Order/@OrderHeaderKey"));

	    Element eleOrderLines = docChangeOrderIp
		    .createElement(GCConstants.ORDER_LINES);
	    eleChangeOrderIpRoot.appendChild(eleOrderLines);

	    NodeList nlOrderLineTaxServiceOp = XPathAPI.selectNodeList(
		    docProcessTaxCalculationsServiceOp, "//OrderLine");

	    for (int i = 0; i < nlOrderLineTaxServiceOp.getLength(); i++) {

		Element eleOrderLineTaxServiceOp = (Element) nlOrderLineTaxServiceOp
			.item(i);
		Element eleOrderLine = docChangeOrderIp
			.createElement(GCConstants.ORDER_LINE);
		eleOrderLines.appendChild(eleOrderLine);
		eleOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY,
			eleOrderLineTaxServiceOp
				.getAttribute(GCConstants.ORDER_LINE_KEY));
		Element eleLineTaxes = (Element) docChangeOrderIp.importNode(
			eleOrderLineTaxServiceOp.getElementsByTagName(
				GCConstants.LINE_TAXES).item(0), true);
		eleOrderLine.appendChild(eleLineTaxes);
	    }
	    GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER,
		    docChangeOrderIp);
	} catch (Exception e) {
	    LOGGER.error("Inside GCRecalculateTax.updateTaxOnTheOrder():", e);
	    throw new GCException(e);
	}
    }
}
