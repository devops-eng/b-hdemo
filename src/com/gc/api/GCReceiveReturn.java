/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class is invoked when the return order lines are received.
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               06/05/2014        Soni, Karan		        Initial Version
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.util.HashSet;
import java.util.Set;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author karan
 */
public class GCReceiveReturn {

    // initialize logger
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCReceiveReturn.class);

    public Document receiptCloseAndInvoice(YFSEnvironment env, Document inDoc)
	    throws GCException {
	try {
	    LOGGER.verbose("Entering the receiptCloseAndInvoice method");
	    Set<String> stBundleOrderLineKey = new HashSet<String>();
	    NodeList nlReceiptLine = GCXMLUtil.getNodeListByXpath(inDoc,
		    "Receipt/ReceiptLines/ReceiptLine");
	    String strOHK = "";
	    Document docCloseReceipt = GCXMLUtil.createDocument("Invoice");
	    for (int i = 0; i < nlReceiptLine.getLength(); i++) {
		Element eleReceiptLine = (Element) nlReceiptLine.item(i);
		strOHK = eleReceiptLine
			.getAttribute(GCConstants.ORDER_HEADER_KEY);
		String strOrderLineKey = eleReceiptLine
			.getAttribute(GCConstants.ORDER_LINE_KEY);

		String strBundleParentOrderLineKey = GCXMLUtil
			.getAttributeFromXPath(eleReceiptLine,
				"OrderLine/BundleParentLine/@OrderLineKey");
		LOGGER.debug("The Order HeaderKey is " + strOHK
			+ "The Order Line Key " + strOrderLineKey
			+ "The bundle parent order line key is "
			+ strBundleParentOrderLineKey);
		// For Bundle OrderLine checking that bundle parent orlinekey
		// exists and it is not processed earlier
		if (!YFCCommon.isVoid(strBundleParentOrderLineKey)
			&& !stBundleOrderLineKey
				.contains(strBundleParentOrderLineKey)) {
		    Element eleSoldSeparately = (Element) XPathAPI
			    .selectSingleNode(eleReceiptLine,
				    "OrderLine/BundleParentLine[@OrderLineKey='"
					    + strBundleParentOrderLineKey
					    + "']");
		    if (!YFCCommon.isVoid(eleSoldSeparately)) {
			String strGetOrderLineList = "<OrderLine OrderLineKey='"
				+ strBundleParentOrderLineKey + "'/>";
			Document inDocGetOrderLineList = GCXMLUtil
				.getDocument(strGetOrderLineList);
			Document outDocGetOrderLineList = GCCommonUtil
				.invokeAPI(env,
					GCConstants.GET_ORDER_LINE_LIST,
					inDocGetOrderLineList);
			if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("The getOrderLineList document is "
				+ GCXMLUtil
					.getXMLString(outDocGetOrderLineList));
			}
			String strMinLineStatus = GCXMLUtil
				.getAttributeFromXPath(outDocGetOrderLineList,
					"OrderLineList/OrderLine/@MinLineStatus");
			LOGGER.debug("The minLineStatus is " + strMinLineStatus);
			// If the parent order line in case of bundle is
			// received
			if (GCConstants.RECEIVED.equals(strMinLineStatus)) {
			    LOGGER.debug("Entering the if Min Line status is 3900 block");
			    stBundleOrderLineKey
				    .add(strBundleParentOrderLineKey);
			    String strShipNode = GCXMLUtil
				    .getAttributeFromXPath(eleReceiptLine,
					    "OrderLine/@ShipNode");
			    LOGGER.debug("The shipnode is " + strShipNode);
			    Element eleShipNode = GCXMLUtil.getElementByXPath(
				    docCloseReceipt,
				    "Invoice/OrderLines[@ShipNode='"
					    + strShipNode + "']");
			    // If the shipnode element do not exists i.e its
			    // first time for specific node a orderline is
			    // processed
			    if (YFCCommon.isVoid(eleShipNode)) {
				Element eleOrderLines = docCloseReceipt
					.createElement(GCConstants.ORDER_LINES);
				eleOrderLines.setAttribute(
					GCConstants.SHIP_NODE, strShipNode);
				Element eleOrderLine = docCloseReceipt
					.createElement(GCConstants.ORDER_LINE);
				eleOrderLine.setAttribute(
					GCConstants.ORDER_LINE_KEY,
					strBundleParentOrderLineKey);
				Element eleRoot = docCloseReceipt
					.getDocumentElement();
				eleRoot.appendChild(eleOrderLines);
				eleOrderLines.appendChild(eleOrderLine);
			    }else {
			        // If the shipnode element exists i.e for specific
	                // node a orderline is processed earlier
				Element eleOrderLine = docCloseReceipt
					.createElement(GCConstants.ORDER_LINE);
				eleOrderLine.setAttribute(
					GCConstants.ORDER_LINE_KEY,
					strBundleParentOrderLineKey);
				eleShipNode.appendChild(eleOrderLine);
			    }
			    LOGGER.debug("Exiting the if Min Line status is 3900 block");
			}
		    }else {
		        // Handling Sold Separately in case of Bundle item
		        String strShipNode = GCXMLUtil.getAttributeFromXPath(
		                eleReceiptLine, "OrderLine/@ShipNode");
		        Element eleShipNode = GCXMLUtil.getElementByXPath(
		                docCloseReceipt,
		                "Invoice/OrderLines[@ShipNode='" + strShipNode
		                + "']");
		        // If the shipnode element do not exists i.e its first
		        // time for specific node a orderline is processed
		        if (YFCCommon.isVoid(eleShipNode)) {
			    LOGGER.debug("Entering the if ShipNode block is null");
			    Element eleOrderLines = docCloseReceipt
				    .createElement(GCConstants.ORDER_LINES);
			    eleOrderLines.setAttribute(GCConstants.SHIP_NODE,
				    strShipNode);
			    Element eleOrderLine = docCloseReceipt
				    .createElement(GCConstants.ORDER_LINE);
			    eleOrderLine
				    .setAttribute(GCConstants.ORDER_LINE_KEY,
					    strOrderLineKey);
			    Element eleRoot = docCloseReceipt
				    .getDocumentElement();
			    eleRoot.appendChild(eleOrderLines);
			    eleOrderLines.appendChild(eleOrderLine);
			    LOGGER.debug("Exiting the if ShipNode block is null");
			}else {
			    // If the shipnode element exists i.e for specific node
	            // a orderline is processed earlier
			    Element eleOrderLine = docCloseReceipt
				    .createElement(GCConstants.ORDER_LINE);
			    eleOrderLine
				    .setAttribute(GCConstants.ORDER_LINE_KEY,
					    strOrderLineKey);
			    eleShipNode.appendChild(eleOrderLine);
			}
		    }
		    LOGGER.debug("Exiting the if block as BundleParentOrderLineKeyExists");
		}else if (YFCCommon.isVoid(strBundleParentOrderLineKey)) {
		    // For regular OrderLine
		    String strShipNode = GCXMLUtil.getAttributeFromXPath(
			    eleReceiptLine, "OrderLine/@ShipNode");
		    Element eleShipNode = GCXMLUtil.getElementByXPath(
			    docCloseReceipt, "Invoice/OrderLines[@ShipNode='"
				    + strShipNode + "']");
		    // If the shipnode element do not exists i.e its first time
		    // for specific node a orderline is processed
		    if (YFCCommon.isVoid(eleShipNode)) {
			Element eleOrderLines = docCloseReceipt
				.createElement(GCConstants.ORDER_LINES);
			eleOrderLines.setAttribute(GCConstants.SHIP_NODE,
				strShipNode);
			Element eleOrderLine = docCloseReceipt
				.createElement(GCConstants.ORDER_LINE);
			eleOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY,
				strOrderLineKey);
			Element eleRoot = docCloseReceipt.getDocumentElement();
			eleRoot.appendChild(eleOrderLines);
			eleOrderLines.appendChild(eleOrderLine);
		    }else {
		     // If the shipnode element exists i.e for specific node a
	         // orderline is processed earlier
			Element eleOrderLine = docCloseReceipt
				.createElement(GCConstants.ORDER_LINE);
			eleOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY,
				strOrderLineKey);
			eleShipNode.appendChild(eleOrderLine);
		    }
		}
		LOGGER.debug("Exiting the Receipt Line for loop");}

	    // Making Invoice
	    NodeList nlOrderLines = GCXMLUtil.getNodeListByXpath(
		    docCloseReceipt, "Invoice/OrderLines");
	    for (int i = 0; i < nlOrderLines.getLength(); i++) {
		String strCreateOrderInvoice = "<Order OrderHeaderKey='"
			+ strOHK
			+ "' TransactionId='CREATE_ORDER_INVOICE.0003'><OrderLines/></Order>";
		Document inDocOrderInvoice = GCXMLUtil
			.getDocument(strCreateOrderInvoice);
		Element eleOrderLines = (Element) nlOrderLines.item(i);
		eleOrderLines.removeAttribute(GCConstants.SHIP_NODE);
		Element eleOrderLn = GCXMLUtil.getElementByXPath(
			inDocOrderInvoice, "Order/OrderLines");
		GCXMLUtil.copyElement(inDocOrderInvoice, eleOrderLines,
			eleOrderLn);
		if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("The final doc of createOrderInvoice is "
			+ GCXMLUtil.getXMLString(inDocOrderInvoice));
		}
		GCCommonUtil.invokeAPI(env, GCConstants.CREATE_ORDER_INVOICE,
			inDocOrderInvoice);
	    }
	    LOGGER.debug("Exiting the Order Type equals Return with adv credit if");
	    LOGGER.verbose("Exiting the receiptCloseAndInvoice method");
	} catch (Exception e) {
	   LOGGER.error("Inside GCReceiveReturn.receiptCloseAndInvoice():",e);
	    throw new GCException(e);
	}
	return inDoc;
    }

}
