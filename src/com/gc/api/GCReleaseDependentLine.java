/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: This class is used to resolve the holds on the warranty lines when their parent line is released.
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0             08/05/2015          Saxena, Ekansh      This class is used to release the dependent lines if parent are released.
 #################################################################################################################################################################
 */
package com.gc.api;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class is used to release the dependent lines if parent are released.
 * 
 * @author Ekansh Saxena
 *
 */

public class GCReleaseDependentLine {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCReleaseDependentLine.class);

  public Document processDependentLine(YFSEnvironment env, Document inDoc) {

    LOGGER.beginTimer(" Entering GCReleaseDependentLine.processDependentLine() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" processDependentLine() method, Input document is" + GCXMLUtil.getXMLString(inDoc));
    }
    
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleRoot = inputDoc.getDocumentElement();
    String sOrderHeaderKey = eleRoot.getAttribute(GCConstants.ORDER_HEADER_KEY);
    YFCDocument getOrderListIp = YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "'/>");
    YFCDocument getOrderListTemp =
        YFCDocument.getDocumentFor("<OrderList><Order OrderHeaderKey='' OrderNo=''>"
            + "<OrderLines><OrderLine DependentOnLineKey='' OrderLineKey='' MaxLineStatus='' MinLineStatus=''>"
            + "<Extn ExtnIsWarrantyItem='' ExtnWarrantyLineNumber='' />"
            + "<OrderHoldTypes><OrderHoldType HoldType='' Status=''/></OrderHoldTypes>"
            + "</OrderLine></OrderLines></Order></OrderList>");
    YFCDocument getOrderListOp =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListIp, getOrderListTemp);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" Get OrderList Output document is" + getOrderListOp.toString());
    }

    YFCElement eleGetOrderList = getOrderListOp.getDocumentElement();
    YFCElement eleGetOrderLines =
        eleGetOrderList.getChildElement(GCConstants.ORDER).getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> nlGetOrderLine = eleGetOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
    for (YFCElement eleGetOrderLine : nlGetOrderLine) {
      String sDependentOnLineKey = eleGetOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
      String sMaxOrderStatus = eleGetOrderLine.getAttribute(GCConstants.ORDER_LINE_MAX_STATUS);
      String sExtnIsWarrantyItem = "";
      YFCElement eleGetOrderLineExtn = eleGetOrderLine.getChildElement(GCConstants.EXTN);
      if (!YFCCommon.isVoid(eleGetOrderLineExtn)) {
        sExtnIsWarrantyItem = eleGetOrderLineExtn.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
      }
      
      // Check if ParentLine is released
      boolean isParentLineReleased = checkIfParentLineReleased(eleGetOrderLines, sDependentOnLineKey);

      if (YFCCommon.equalsIgnoreCase(sExtnIsWarrantyItem, GCConstants.YES) && isParentLineReleased
          && "3200".compareTo(sMaxOrderStatus) > 0) {
        YFCElement eleOrderLineHoldTypes = eleGetOrderLine.getChildElement(GCConstants.ORDER_HOLD_TYPES);
        if (!YFCCommon.isVoid(eleOrderLineHoldTypes)) {
          YFCNodeList<YFCElement> nlOrderLineHoldType =
              eleOrderLineHoldTypes.getElementsByTagName(GCConstants.ORDER_HOLD_TYPE);
          for (YFCElement eleOrderLineHoldType : nlOrderLineHoldType) {
            String sHoldType = eleOrderLineHoldType.getAttribute(GCConstants.HOLD_TYPE);
            String sStatus = eleOrderLineHoldType.getAttribute(GCConstants.STATUS);
            if (YFCCommon.equalsIgnoreCase(sHoldType, "DEPEND_WARRANTY_HOLD")
                && YFCCommon.equalsIgnoreCase(sStatus, "1100")) {
              removeHoldOnDependentLineAndRelease(env, eleGetOrderLine, sOrderHeaderKey);
            } else {
              releaseDependentLine(env, eleGetOrderLine, sOrderHeaderKey);
            }
          }
        }
      }
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Updated document is" + inputDoc.toString());
    }
    LOGGER.endTimer(" Exiting GCReleaseDependentLine.processDependentLine() method ");
    return inDoc;
  }

  private void releaseDependentLine(YFSEnvironment env, YFCElement eleGetOrderLine, String sOrderHeaderKey) {
    LOGGER.beginTimer(" Entering GCReleaseDependentLine.releaseDependentLine() method ");
    String sOrderLineKey = eleGetOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
    YFCDocument docScheduleOrder = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleScheduleOrder = docScheduleOrder.getDocumentElement();
    eleScheduleOrder.setAttribute(GCXmlLiterals.ALLOCATION_RULE_ID, GCConstants.GC_DEFAULT);
    eleScheduleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    eleScheduleOrder.setAttribute(GCConstants.ORDER_LINE_KEY, sOrderLineKey);
    GCCommonUtil.invokeAPI(env, GCConstants.API_SCHEDULE_ORDER, docScheduleOrder, null);
    
    YFCDocument docReleaseOrder = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleReleaseOrder = docReleaseOrder.getDocumentElement();
    eleReleaseOrder.setAttribute(GCXmlLiterals.ALLOCATION_RULE_ID, GCConstants.GC_DEFAULT);
    eleReleaseOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    GCCommonUtil.invokeAPI(env, GCConstants.API_RELEASE_ORDER, docReleaseOrder, null);

    LOGGER.endTimer(" Exiting GCReleaseDependentLine.releaseDependentLine() method ");
  }

  private void removeHoldOnDependentLineAndRelease(YFSEnvironment env, YFCElement eleGetOrderLine, String sOrderHeaderKey) {
    LOGGER.beginTimer(" Entering GCReleaseDependentLine.removeHoldOnDependentLineAndRelease() method ");
    
    YFCDocument docChangeOrder = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleOrder = docChangeOrder.getDocumentElement();
    eleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    YFCElement eleOrderLines = eleOrder.createChild(GCConstants.ORDER_LINES);
    YFCElement eleOrderLine = eleOrderLines.createChild(GCConstants.ORDER_LINE);
    eleOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, eleGetOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
    YFCElement eleOrderLineHoldTypes = eleOrderLine.createChild(GCConstants.ORDER_HOLD_TYPES);
    YFCElement eleOrderLineHoldType = eleOrderLineHoldTypes.createChild(GCConstants.ORDER_HOLD_TYPE);
    eleOrderLineHoldType.setAttribute(GCConstants.HOLD_TYPE, "DEPEND_WARRANTY_HOLD");
    eleOrderLineHoldType.setAttribute(GCConstants.STATUS, "1300");
    GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, docChangeOrder, null);
    
    String sOrderLineKey = eleGetOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
    YFCDocument docScheduleOrder = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleScheduleOrder = docScheduleOrder.getDocumentElement();
    eleScheduleOrder.setAttribute(GCXmlLiterals.ALLOCATION_RULE_ID, GCConstants.GC_DEFAULT);
    eleScheduleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    eleScheduleOrder.setAttribute(GCConstants.ORDER_LINE_KEY, sOrderLineKey);
    GCCommonUtil.invokeAPI(env, GCConstants.API_SCHEDULE_ORDER, docScheduleOrder, null);
    
    YFCDocument docReleaseOrder = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleReleaseOrder = docReleaseOrder.getDocumentElement();
    eleReleaseOrder.setAttribute(GCXmlLiterals.ALLOCATION_RULE_ID, GCConstants.GC_DEFAULT);
    eleReleaseOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    GCCommonUtil.invokeAPI(env, GCConstants.API_RELEASE_ORDER, docReleaseOrder, null);
    
    LOGGER.endTimer(" Exiting GCReleaseDependentLine.removeHoldOnDependentLineAndRelease() method ");
  }

  /**
   * This method checks if parent line is released or in above release status.
   *
   * @param eleOrderLines 
   * @param sDependentOnLineKey
   * @return
   */
  
  private boolean checkIfParentLineReleased(YFCElement eleGetOrderLines, String sDependentOnLineKey) {

    LOGGER.beginTimer(" Entering GCReleaseDependentLine.checkIfParentLineReleased() method ");
    YFCNodeList<YFCElement> nlOrderLine = eleGetOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
    for (YFCElement eleOrderLine : nlOrderLine) {
      String sMaxLineStatus = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_MAX_STATUS);
      String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      if ("3200".compareTo(sMaxLineStatus) <= 0 && YFCCommon.equalsIgnoreCase(sDependentOnLineKey, sOrderLineKey)) {
        return true;
      }
    }
    LOGGER.endTimer(" Exiting GCReleaseDependentLine.checkIfParentLineReleased() method ");
    return false;
  }
}
