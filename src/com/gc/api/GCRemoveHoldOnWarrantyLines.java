/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: This class is used to resolve the holds on the warranty lines when their parent line is released.
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0             04/05/2015          Saxena, Ekansh      This class isused to resolve the holds on the warranty lines when their parent line is released.
 #################################################################################################################################################################
 */
package com.gc.api;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class is used to resolve the holds on the warranty lines when their parent line is released.
 * 
 * @author Ekansh Saxena
 *
 */

public class GCRemoveHoldOnWarrantyLines {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCRemoveHoldOnWarrantyLines.class);

  public Document processHoldOnWarrantyOrderLine(YFSEnvironment env, Document inDoc) {

    LOGGER.beginTimer(" Entering GCRemoveHoldOnWarrantyLines.processHoldOnWarrantyOrderLine() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" processHoldOnWarrantyOrderLine() method, Input document is" + GCXMLUtil.getXMLString(inDoc));
    }
    boolean isHoldResolveNeeded = false;
    boolean isChangeOrderReqd = false;
    boolean isReleaseOrderReqd = false;
    List<String> sOrderLineKeyForSchedule = new ArrayList<String>();
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleRoot = inputDoc.getDocumentElement();
    String sOrderHeaderKey = eleRoot.getAttribute(GCConstants.ORDER_HEADER_KEY);

    YFCDocument docChangeOrder = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleChangeOrder = docChangeOrder.getDocumentElement();
    eleChangeOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    YFCElement eleChangeOrderLines = eleChangeOrder.createChild(GCConstants.ORDER_LINES);

    YFCDocument docGetOrderListIP = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleOrderGetOrderList = docGetOrderListIP.getDocumentElement();
    eleOrderGetOrderList.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    YFCDocument docGetOrderListIPTemplate =
        YFCDocument
            .getDocumentFor("<OrderList><Order OrderNo='' OrderHeaderKey=''>"
                + "<OrderLines><OrderLine FulfillmentType='' CarrierAccountNo='' CarrierServiceCode='' CostCurrency='' "
                + "CurrentWorkOrderKey='' DeliveryCode='' FreightTerms='' GiftFlag='' LevelOfService='' MergeNode='' PackListType='' "
                + "ReceivingNode='' SCAC='' ShipNode='' ShipToID='' ShipToKey='' MaxLineStatus='' MinLineStatus='' "
                + "DependentOnLineKey='' OrderLineKey=''>"
                + "<Extn ExtnIsWarrantyItem='' ExtnWarrantyLineNumber='' ExtnIsShipAlone=''/>"
                + "</OrderLine></OrderLines></Order></OrderList>");

    YFCDocument docGetOrderListOp =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, docGetOrderListIP, docGetOrderListIPTemplate);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("GetOrderList Output document is" + docGetOrderListOp.toString());
    }

    YFCElement eleOrderList = docGetOrderListOp.getDocumentElement();
    YFCElement eleOrder = eleOrderList.getChildElement(GCConstants.ORDER);
    YFCElement eleOrderLines = eleOrder.getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> nlOrderLine = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
    for (YFCElement eleOrderLine : nlOrderLine) {
      YFCElement eleOrderLineExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
      String sExtnIsWarrantyItem = eleOrderLineExtn.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
      String sMaxLineStatus = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_MAX_STATUS);
      if (YFCCommon.equalsIgnoreCase(GCConstants.YES, sExtnIsWarrantyItem)
          && !YFCCommon.equalsIgnoreCase(sMaxLineStatus, GCConstants.RELEASED_STATUS)) {
        String sDependentOnLineKey = eleOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
        isHoldResolveNeeded = checkIfHoldResolveNeededForWarranty(env, docGetOrderListOp, sDependentOnLineKey);
        if (isHoldResolveNeeded) {
          YFCElement eleChangeOrderLine = eleChangeOrderLines.createChild(GCConstants.ORDER_LINE);
          stampParentLineAttributesOnWarrantyLine(env, docGetOrderListOp, sDependentOnLineKey, eleChangeOrderLine);
          sOrderLineKeyForSchedule.add(eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
          eleChangeOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
          eleChangeOrderLine.setAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE, GCConstants.YES);
          YFCElement eleChangeOrderLineExtn = eleChangeOrderLine.createChild(GCConstants.EXTN);
          eleChangeOrderLineExtn.setAttribute("ExtnIsShipAlone", eleOrderLine.getChildElement(GCConstants.EXTN).getAttribute("ExtnIsShipAlone"));
          YFCElement eleChangeOrderLineOrderHoldTypes = eleChangeOrderLine.createChild(GCConstants.ORDER_HOLD_TYPES);
          YFCElement eleChangeOrderLineOrderHoldType =
              eleChangeOrderLineOrderHoldTypes.createChild(GCConstants.ORDER_HOLD_TYPE);
          eleChangeOrderLineOrderHoldType.setAttribute(GCConstants.HOLD_TYPE, "DEPEND_WARRANTY_HOLD");
          eleChangeOrderLineOrderHoldType.setAttribute(GCConstants.STATUS, "1300");
          isChangeOrderReqd = true;
        }
      }
    }
    if (isChangeOrderReqd) {
      GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, docChangeOrder, null);
    }
    for (YFCElement eleOrderLine : nlOrderLine) {
      String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      if (sOrderLineKeyForSchedule.contains(sOrderLineKey)) {
        YFCDocument docScheduleOrder = YFCDocument.createDocument(GCConstants.ORDER);
        YFCElement eleScheduleOrder = docScheduleOrder.getDocumentElement();
        eleScheduleOrder.setAttribute(GCXmlLiterals.ALLOCATION_RULE_ID, GCConstants.GC_DEFAULT);
        eleScheduleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
        eleScheduleOrder.setAttribute(GCConstants.ORDER_LINE_KEY, sOrderLineKey);
        GCCommonUtil.invokeAPI(env, GCConstants.API_SCHEDULE_ORDER, docScheduleOrder, null);
        sOrderLineKeyForSchedule.remove(sOrderLineKey);
        isReleaseOrderReqd = true;
      }
    }

    if (isReleaseOrderReqd) {
      YFCDocument docReleaseOrder = YFCDocument.createDocument(GCConstants.ORDER);
      YFCElement eleReleaseOrder = docReleaseOrder.getDocumentElement();
      eleReleaseOrder.setAttribute(GCXmlLiterals.ALLOCATION_RULE_ID, GCConstants.GC_DEFAULT);
      eleReleaseOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
      GCCommonUtil.invokeAPI(env, GCConstants.API_RELEASE_ORDER, docReleaseOrder, null);
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Updated document is" + inputDoc.toString());
    }
    LOGGER.endTimer(" Entering GCRemoveHoldOnWarrantyLines.processHoldOnWarrantyOrderLine() method ");
    return inputDoc.getDocument();
  }


  /**
   * This method checks if Hold resolve is necessary for any warranty line.
   *
   * @param env
   * @param inDoc
   * @return
   */
  private boolean checkIfHoldResolveNeededForWarranty(YFSEnvironment env, YFCDocument inputDoc,
      String sDependentOnLineKey) {
    LOGGER.beginTimer(" Entering GCRemoveHoldOnWarrantyLines.checkIfHoldResolveNeededForWarranty() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Input document is" + inputDoc.toString());
    }
    YFCElement eleOrderLines =
        inputDoc.getDocumentElement().getChildElement(GCConstants.ORDER).getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> nlOrderLine = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
    for (YFCElement eleOrderLine : nlOrderLine) {
      String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      if (YFCCommon.equalsIgnoreCase(sDependentOnLineKey, sOrderLineKey)) {
        String sMaxLineStatus = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_MAX_STATUS);
        if ( "3200".compareTo(sMaxLineStatus) <= 0 ) {
          return true;
        }
      }
    }
    LOGGER.endTimer(" Entering GCRemoveHoldOnWarrantyLines.checkIfHoldResolveNeededForWarranty() method ");
    return false;
  }


  /**
   * This method stamps the parent line attribute on the warranty line.
   *
   * @param env
   * @param inDoc
   * @return
   */
  private YFCElement stampParentLineAttributesOnWarrantyLine(YFSEnvironment env, YFCDocument inputDoc, String sDependentOnLineKey, YFCElement eleChangeOrderLine) {
    LOGGER.beginTimer(" Entering GCRemoveHoldOnWarrantyLines.checkIfHoldResolveNeededForWarranty() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Input document is" + inputDoc.toString());
    }
    YFCElement eleOrderLines =
        inputDoc.getDocumentElement().getChildElement(GCConstants.ORDER).getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> nlOrderLine = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
    for (YFCElement eleOrderLine : nlOrderLine) {
      String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      if (YFCCommon.equalsIgnoreCase(sDependentOnLineKey, sOrderLineKey)) {
        eleChangeOrderLine.setAttribute("FulfillmentType", eleOrderLine.getAttribute("FulfillmentType"));
        eleChangeOrderLine.setAttribute("CarrierAccountNo", eleOrderLine.getAttribute("CarrierAccountNo"));
        eleChangeOrderLine.setAttribute("CarrierServiceCode", eleOrderLine.getAttribute("CarrierServiceCode"));
        eleChangeOrderLine.setAttribute("CostCurrency", eleOrderLine.getAttribute("CostCurrency"));
        eleChangeOrderLine.setAttribute("CurrentWorkOrderKey", eleOrderLine.getAttribute("CurrentWorkOrderKey"));
        eleChangeOrderLine.setAttribute("DeliveryCode", eleOrderLine.getAttribute("DeliveryCode"));
        eleChangeOrderLine.setAttribute("FreightTerms", eleOrderLine.getAttribute("FreightTerms"));
        eleChangeOrderLine.setAttribute("GiftFlag", eleOrderLine.getAttribute("GiftFlag"));
        eleChangeOrderLine.setAttribute("LevelOfService", eleOrderLine.getAttribute("LevelOfService"));
        eleChangeOrderLine.setAttribute("MergeNode", eleOrderLine.getAttribute("MergeNode"));
        eleChangeOrderLine.setAttribute("ReceivingNode", eleOrderLine.getAttribute("ReceivingNode"));
        eleChangeOrderLine.setAttribute("SCAC", eleOrderLine.getAttribute("SCAC"));
        eleChangeOrderLine.setAttribute("ShipNode", eleOrderLine.getAttribute("ShipNode"));
        eleChangeOrderLine.setAttribute("ShipToID", eleOrderLine.getAttribute("ShipToID"));
        eleChangeOrderLine.setAttribute("ShipToKey", eleOrderLine.getAttribute("ShipToKey"));
      }
    }
    LOGGER.endTimer(" Entering GCRemoveHoldOnWarrantyLines.checkIfHoldResolveNeededForWarranty() method ");
    return eleChangeOrderLine;
  }
}
