/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: This class is used to remove Ship node from Bundle Parent Line.
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0               08/19/2016        Saxena, Ekansh                Initial Version
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCRemoveNodeFromBundleParent {
  
  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
          .instance(GCRemoveNodeFromBundleParent.class);

  public Document removeShipNodeFromParent(YFSEnvironment env, Document inDoc){
    
    LOGGER.beginTimer("GCRemoveNodeFromBundleParent.removeShipNodeFromParent");
    LOGGER.verbose("GCRemoveNodeFromBundleParent.removeShipNodeFromParent--Begin");
      
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("input Doc : " + inputDoc.toString());
    }

    YFCElement eleOrder = inputDoc.getDocumentElement();
    String sOrderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
    YFCElement eleOrderLines = eleOrder.getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> nlOrderLine = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
    for(YFCElement eleOrderLine : nlOrderLine){
      String sKitCode = eleOrderLine.getAttribute(GCConstants.KIT_CODE);
      String sShipNode = eleOrderLine.getAttribute(GCConstants.SHIP_NODE);
      if(YFCCommon.equalsIgnoreCase(sKitCode, GCConstants.BUNDLE) && !YFCCommon.isStringVoid(sShipNode)){
        String sBundleLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
        Map<String,String> orderLineKeyNodeMap = new HashMap<String,String>();
        orderLineKeyNodeMap = odrLineKeyShipNode(eleOrderLines, sBundleLineKey);
        YFCDocument changeOrderParentDoc = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderHeaderKey+"' Override='Y'>"
            + "<OrderLines><OrderLine OrderLineKey='"+sBundleLineKey+"' ShipNode=''/></OrderLines></Order>");
        
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("Change Order input Doc for Parent Item : " + changeOrderParentDoc.toString());
        }
        
        GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, changeOrderParentDoc, null);
        YFCDocument changeOrderCompDoc = YFCDocument.createDocument(GCConstants.ORDER);
        YFCElement eleChangeOrder = changeOrderCompDoc.getDocumentElement();
        eleChangeOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
        eleChangeOrder.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
        YFCElement eleChangeOrderLines = eleChangeOrder.createChild(GCConstants.ORDER_LINES);
        boolean isCompChangeOrderReqd=false;
        for(Map.Entry<String, String> entry : orderLineKeyNodeMap.entrySet()){
          YFCElement eleChangeOrderLine = eleChangeOrderLines.createChild(GCConstants.ORDER_LINE);
          eleChangeOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, entry.getKey());
          eleChangeOrderLine.setAttribute(GCConstants.SHIP_NODE, entry.getValue());
          isCompChangeOrderReqd = true;
        }
        if(isCompChangeOrderReqd){
          if (LOGGER.isVerboseEnabled()) {
            LOGGER.verbose("Change Order input Doc for Component Item : " + changeOrderCompDoc.toString());
          }
          GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, changeOrderCompDoc, null);
        }   
      }
    }
    LOGGER.verbose("GCRemoveNodeFromBundleParent.removeShipNodeFromParent--End");
    LOGGER.endTimer("GCRemoveNodeFromBundleParent.removeShipNodeFromParent");
    return inDoc;
  }
  
  private Map<String,String> odrLineKeyShipNode(YFCElement eleOrderLines, String sBundleLineKey){
    Map<String,String> orderLineKeyNodeMap = new HashMap<String,String>();
    YFCNodeList<YFCElement> nlOrderLine = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
    for(YFCElement eleOrderLine : nlOrderLine){
      YFCElement eleBundleParentLine = eleOrderLine.getChildElement(GCConstants.BUNDLE_PARENT_LINE);
      if(!YFCCommon.isVoid(eleBundleParentLine)){
        String sBundleParentLineKey = eleBundleParentLine.getAttribute(GCConstants.ORDER_LINE_KEY);
        if(YFCCommon.equalsIgnoreCase(sBundleLineKey, sBundleParentLineKey)){
          String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
          String sShipNode = eleOrderLine.getAttribute(GCConstants.SHIP_NODE);
          orderLineKeyNodeMap.put(sOrderLineKey, sShipNode);
        }
      }
    }
    return orderLineKeyNodeMap;
  }
}
