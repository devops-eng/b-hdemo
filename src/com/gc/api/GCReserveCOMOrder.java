/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################################
 *          OBJECTIVE: Contains logic to do reservation for COM Orders.
 *###################################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *################################################################################################################################################################################
             1.0               13/04/2015       Taj, Rashma                 Contains logic to do reservation for COM Orders.
 *##################################################################################################################################################################################
 */
package com.gc.api;

import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This Class is called from Fulfillment Summary Screen of call center to reserve the UnReserved
 * Quantity on the order.
 *
 * @author rashmataj
 *
 */
public class GCReserveCOMOrder implements YIFCustomApi {
  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCReserveCOMOrder.class);

  /**
   * This method calls modifyFulfillmentOptions API with the input xml recieved and then calls
   * GCReserveInventoryService to reserved Inventory for call center orders.
   *
   * @param env
   * @param inputDoc
   * @return
   */
  public Document reserveCOMOrder(YFSEnvironment env, Document inputDoc) {
    LOGGER.beginTimer("reserveCOMOrder");
    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input XML to the method::" + inDoc.toString());
    }
    // call modifyFulfillmentOptions API
    GCCommonUtil.invokeAPI(env, GCConstants.MODIFY_FULFILLMENT_OPTIONS, inDoc,
        YFCDocument.createDocument(GCConstants.ORDER));
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input XML to GCGetATGPromoService"
          + inDoc.toString());
    }
    YFCDocument outDocGCGetATGPromoService = GCCommonUtil.invokeService(
        env, "GCGetATGPromoService", inDoc);
    if (LOGGER.isVerboseEnabled()
        && !YFCCommon.isVoid(outDocGCGetATGPromoService)) {
      LOGGER.verbose("Output XMLfrom GCGetATGPromoService"
          + outDocGCGetATGPromoService.toString());
    }
    String ordrHdrKey = inDoc.getDocumentElement().getAttribute("OrderHeaderKey");
    // prepare getOrderList Input XML
    YFCDocument getOrdrListinDoc = YFCDocument.createDocument("Order");
    getOrdrListinDoc.getDocumentElement().setAttribute("OrderHeaderKey", ordrHdrKey);

    // prepare getOrderList Output Template
    YFCDocument getOrdrLstTmpl = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_OUTPUT_TMPL_FOR_RSRVATION);
    // call getOrderList API
    YFCDocument getOrdrLstOpDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrdrListinDoc, getOrdrLstTmpl);
    YFCElement orderEle = getOrdrLstOpDoc.getDocumentElement().getChildElement("Order");
    if (!YFCCommon.isVoid(orderEle)) {
      double maxOrderStatus = orderEle.getDoubleAttribute("MaxOrderStatus");
      if (maxOrderStatus < 2100) {
        // resetting all reservedqty
        resetOrderLineReservations(env, orderEle);
        // GCSTORE-2707::Begin
        getOrdrLstOpDoc = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrdrListinDoc, getOrdrLstTmpl);
        orderEle = getOrdrLstOpDoc.getDocumentElement().getChildElement("Order");
        YFCDocument reserveInvServInput = YFCDocument.getDocumentFor(orderEle.toString());
        YFCDocument reserveInvServOutput =
            GCCommonUtil.invokeService(env, GCConstants.GC_RESERVE_INVENTORY_SERVICE, reserveInvServInput);
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("ReserveInventoryService Output Document::" + reserveInvServOutput.toString());
        }

        YFCDocument handleBundleReservation =
            GCCommonUtil.invokeService(env, GCConstants.GC_HANDLE_BUNDLE_RESERVATION_CALL_CENTER_SERV,
                reserveInvServOutput);

        YFCElement changeOrderRoot = handleBundleReservation.getDocumentElement();
        //GCSTORE-5454::Begin
        emptyExtnPickUpStoreIDForShipToCustOrders(getOrdrLstOpDoc, changeOrderRoot);
        //GCSTORE-5454:End
        changeOrderRoot.setAttribute(GCConstants.SELECT_METHOD, "WAIT");
        GCCommonUtil
        .invokeAPI(env, GCConstants.CHANGE_ORDER, handleBundleReservation,
            YFCDocument.createDocument("Order"));
      }
    }
    LOGGER.endTimer("reserveCOMOrder");
    return inDoc.getDocument();
  }

  /**
   * This method blanks out ExtnPickupStoreID if present, for a ship to customer order.
   * Description of emptyExtnPickUpStoreIDForShipToCustOrders
   *
   * @param getOrdrLstOpDoc
   * @param changeOrderRoot 
   *
   */
  private void emptyExtnPickUpStoreIDForShipToCustOrders(
      YFCDocument getOrdrLstOpDoc, YFCElement changeOrderRoot) {
    YFCElement orderEle = getOrdrLstOpDoc.getDocumentElement().getChildElement("Order");
    YFCElement extnEle = orderEle.getChildElement(GCConstants.EXTN);
    String extnFulfillmentType = extnEle.getAttribute(GCConstants.EXTN_FULFILLMENT_TYPE);
    if((!YFCCommon.isVoid(extnFulfillmentType)) && extnFulfillmentType.startsWith(GCConstants.SHIP_2_CUSTOMER)){
      String extnPickUpStoreID = extnEle.getAttribute(GCConstants.EXTN_PICKUP_STORE_ID);
      if(!YFCCommon.isVoid(extnPickUpStoreID)){
        YFCElement eleExtnChangeOrdr = changeOrderRoot.getChildElement(GCConstants.EXTN);
        if(YFCCommon.isVoid(eleExtnChangeOrdr)){
          eleExtnChangeOrdr = changeOrderRoot.createChild(GCConstants.EXTN);
        }
        eleExtnChangeOrdr.setAttribute(GCConstants.EXTN_PICKUP_STORE_ID, GCConstants.BLANK);
      }
    }

  }

  public static void resetOrderLineReservations(YFSEnvironment env, YFCElement orderEle) {
    LOGGER.beginTimer("resetOrderLineReservations");
    YFCDocument changeOrderInputDoc = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement changeOrderRoot = changeOrderInputDoc.getDocumentElement();
    changeOrderRoot.setAttribute(GCConstants.ORDER_HEADER_KEY, orderEle.getAttribute(GCConstants.ORDER_HEADER_KEY));
    changeOrderRoot.setAttribute(GCConstants.SELECT_METHOD, "WAIT");
    YFCElement changeOrdrOrderLines = changeOrderRoot.createChild(GCConstants.ORDER_LINES);
    boolean isChangeOrdrCalledRequired = false;
    YFCElement orderLines = orderEle.getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> orderLineNL = orderLines.getElementsByTagName(GCConstants.ORDER_LINE);
    for (YFCElement orderLineEle : orderLineNL) {
      isChangeOrdrCalledRequired = true;
      YFCElement changeOrdrOrderLine = changeOrdrOrderLines.createChild(GCConstants.ORDER_LINE);
      changeOrdrOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY,
          orderLineEle.getAttribute(GCConstants.ORDER_LINE_KEY));
      YFCElement eleOrderLineReservation = changeOrdrOrderLine.createChild(GCConstants.ORDER_LINE_RESERVATIONS);
      eleOrderLineReservation.setAttribute(GCConstants.RESET, GCConstants.YES);
      stampUnitCost(env, orderLineEle, changeOrdrOrderLine);

    }
    if (isChangeOrdrCalledRequired) {

      GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, changeOrderInputDoc,
          YFCDocument.createDocument(GCConstants.ORDER));
    }
    LOGGER.endTimer("resetOrderLineReservations");

  }
  @Override
  public void setProperties(Properties arg0) {


  }

  /**
   * GCSTORE-3266::This method calls getItemList to get the ExtnInventoryCost of an item and updates
   * it at orderLine unit cost.
   *
   * @param inDoc
   */
  public static void stampUnitCost(YFSEnvironment env, YFCElement eleOrderLine, YFCElement changeOrdrOrderLine) {
    LOGGER.beginTimer("stampUnitCost::Begin");

    YFCElement eleItem = eleOrderLine.getChildElement(GCConstants.ITEM);
    String itemId = eleItem.getAttribute(GCConstants.ITEM_ID);
    String productClass = eleItem.getAttribute(GCConstants.PRODUCT_CLASS);
    String uom = eleItem.getAttribute(GCConstants.UOM);
    YFCDocument getItemListTmpl =
        YFCDocument
        .getDocumentFor("<ItemList><Item ItemID=''><PrimaryInformation UnitCost=''/><Extn ExtnInventoryCost='' /></Item></ItemList>");
    YFCDocument getItemListOutput =
        GCCommonUtil.getItemList(env, itemId, GCConstants.EACH, GCConstants.GCI, true, getItemListTmpl);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("GetItemList Output::" + getItemListOutput.toString());
    }
    YFCElement itemListEle = getItemListOutput.getDocumentElement();
    YFCElement itemEle = itemListEle.getChildElement(GCConstants.ITEM);
    if (!YFCCommon.isVoid(itemEle)) {
      YFCElement eleExtn = itemEle.getChildElement(GCConstants.EXTN);
      String extnInvCost = eleExtn.getAttribute("ExtnInventoryCost");
      YFCElement elePrimaryInfo = itemEle.getChildElement(GCConstants.PRIMARY_INFORMATION);
      String sUnitCost = elePrimaryInfo.getAttribute("UnitCost");
      Double dUnitCost = elePrimaryInfo.getDoubleAttribute("UnitCost");
      Double dInvCost = eleExtn.getDoubleAttribute("ExtnInventoryCost");
      LOGGER.verbose("ItemID::" + itemId + "::ExtnInventoryCost::" + extnInvCost);
      if (!YFCCommon.isVoid(extnInvCost) && dInvCost != 0.00) {
        YFCElement eleItemInChangeOrdrDoc = changeOrdrOrderLine.createChild(GCConstants.ITEM);
        eleItemInChangeOrdrDoc.setAttribute("UnitCost", extnInvCost);
        eleItemInChangeOrdrDoc.setAttribute(GCConstants.ITEM_ID, itemId);
        eleItemInChangeOrdrDoc.setAttribute(GCConstants.PRODUCT_CLASS, productClass);
        eleItemInChangeOrdrDoc.setAttribute(GCConstants.UOM, uom);

      } else if (!YFCCommon.isVoid(sUnitCost) && dUnitCost != 0.00) {
        YFCElement eleItemInChangeOrdrDoc = changeOrdrOrderLine.createChild(GCConstants.ITEM);
        eleItemInChangeOrdrDoc.setAttribute("UnitCost", sUnitCost);
        eleItemInChangeOrdrDoc.setAttribute(GCConstants.ITEM_ID, itemId);
        eleItemInChangeOrdrDoc.setAttribute(GCConstants.PRODUCT_CLASS, productClass);
        eleItemInChangeOrdrDoc.setAttribute(GCConstants.UOM, uom);

      } else {
        YFCElement eleItemInChangeOrdrDoc = changeOrdrOrderLine.createChild(GCConstants.ITEM);
        eleItemInChangeOrdrDoc.setAttribute("UnitCost", "0.00");
        eleItemInChangeOrdrDoc.setAttribute(GCConstants.ITEM_ID, itemId);
        eleItemInChangeOrdrDoc.setAttribute(GCConstants.PRODUCT_CLASS, productClass);
        eleItemInChangeOrdrDoc.setAttribute(GCConstants.UOM, uom);

      }
    }

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input OrderLine After appending UnitCost:::" + eleOrderLine.toString());
    }
    LOGGER.endTimer("stampUnitCost::End");
  }

}
