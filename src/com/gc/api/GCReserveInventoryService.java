/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 * *###########################################
 * ######################################################
 * ################################################################ OBJECTIVE: Generate an
 * reservationId and do reservation for that id
 * #####################################################
 * #############################################
 * ############################################################### Version Date Modified By
 * Description
 * ######################################################################################
 * ########################################################################### 1.0 17/02/2014 Singh,
 * Gurpreet OMS-1142: Inventory Reservations - Internal Orders
 * ######################################
 * ############################################################
 * ###############################################################
 */
package com.gc.api;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.StringTokenizer;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCOrderUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author Gurpreet
 */
public class GCReserveInventoryService implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCReserveInventoryService.class);
  Map<String, Double> itemQuantityMap = new HashMap<String, Double>();
  Properties prop = new Properties();

  /**
   *
   * Description of reserveInventory This method will generate the reservationID and will reserve
   * the inventory corresponding to that reservationID
   *
   * @param env
   * @param inDoc
   * @return
   *
   */
  public Document reserveInventory(YFSEnvironment env, Document inDoc) throws GCException {
    LOGGER.debug(" Entering GCReserveInventoryService.reserveInventory() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" reserveInventory() method, Input document is" + GCXMLUtil.getXMLString(inDoc));
    }
    try {
      Document docFindInvOutput = null;
      NodeList nlPromise = inDoc.getDocumentElement().getElementsByTagName(GCConstants.PROMISE);
      if (!YFCCommon.isVoid(nlPromise) && nlPromise.getLength() > 0) {
        Node nPromise = nlPromise.item(0);
        Document cloneDoc = (Document) inDoc.cloneNode(true);
        Element elePromise = (Element) cloneDoc.getElementsByTagName(GCConstants.PROMISE).item(0);
        docFindInvOutput = GCXMLUtil.getDocumentFromElement(elePromise);
        YFCDocument findInventory = YFCDocument.getDocumentFor(docFindInvOutput);

        inDoc.getDocumentElement().removeChild(nPromise);
      }
      Element eleRootOfInDoc = inDoc.getDocumentElement();
      // Generating the ReservationID on the basis of date time and random
      // number
      String sReservationID = getReservationID();
      LOGGER.debug(" sReservationID is" + sReservationID);

      // Fetch OrderLine nodelist from inputDoc
      NodeList nlOrderLine = XPathAPI.selectNodeList(eleRootOfInDoc, "OrderLines/OrderLine");
      // Check if OrderLine exist in doc
      if (!YFCCommon.isVoid(nlOrderLine) && nlOrderLine.getLength() > 0) {
        // Prepare Input doc for findInventory API, fetch the required
        // info from inDoc

        String sEntryType = inDoc.getDocumentElement().getAttribute(GCConstants.ENTRY_TYPE);

        if (!YFCCommon.equalsIgnoreCase(sEntryType, "mPOS")) {
          Document docFindInventory = preparefindInventoryDoc(env, nlOrderLine, inDoc);
          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(" docFindInventory is : " + GCXMLUtil.getXMLString(docFindInventory));
          }
          // Call findInventory API
          // GC-Phase2::Begin==> Logic for excluding OrderLines which are already reserved.
          NodeList promiseLines = docFindInventory.getElementsByTagName(GCConstants.PROMISE_LINE);
          if (!YFCCommon.isVoid(promiseLines) && promiseLines.getLength() > 0) {
            LOGGER.verbose("FindInventoryWrapper Input XML::" + GCXMLUtil.getXMLString(docFindInventory));
            // GC-Phase2::End==> Logic for excluding OrderLines which are already reserved.

            Element findInventoryInEle = docFindInventory.getDocumentElement();
            String canCallFindInventory = findInventoryInEle.getAttribute("CallFindInventory");
            if(YFCCommon.equals(canCallFindInventory, GCConstants.YES, false)){
              findInventoryInEle.removeAttribute("CallFindInventory");
              docFindInvOutput=GCCommonUtil.invokeService(env, "GCFindInventoryWrapperService", docFindInventory);
              // GC-Phase2::End
              if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(" docFindInvOutput is : " + GCXMLUtil.getXMLString(docFindInvOutput));
              }
            }
          }
          // Update the inDoc with ReservationID, ShipNode and other
          // fields
        }

        if(!YFCCommon.isVoid(docFindInvOutput)){
          updateInputDoc(inDoc, docFindInvOutput, sReservationID);

          LOGGER.debug("doc after update is: " + GCXMLUtil.getXMLString(inDoc));
          // GCSTORE-1229::Begin
          removePartialReservations(inDoc, env);

          // GCSTORE-1229::End
          // GCSTORE-2206::Begin
          stampReservationDate(inDoc);
          // GCSTORE-2206::End
          LOGGER.debug("Output from GCReserveInventoryService::" + GCXMLUtil.getXMLString(inDoc));
        }
      }

    } catch (Exception e) {
      LOGGER.error(" Inside catch of GCReserveInventoryService.reserveInventory()", e);
      throw new GCException(e);
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" Updated Input document is" + GCXMLUtil.getXMLString(inDoc));
    }
    LOGGER.debug(" Exiting GCReserveInventoryService.reserveInventory() method ");
    return inDoc;
  }

  /**
   * GCSTORE-2206::This method stamps OrderReservationDate as current Date.
   *
   * @param inDoc
   */
  private void stampReservationDate(Document inDoc) {
    LOGGER.beginTimer("stampReservationDate");
    Element orderDates;
    NodeList orderDatesNodeList = inDoc.getElementsByTagName(GCConstants.ORDER_DATES);
    if (YFCCommon.isVoid(orderDatesNodeList) || orderDatesNodeList.getLength() == 0) {
      orderDates = inDoc.createElement(GCConstants.ORDER_DATES);
      inDoc.getDocumentElement().appendChild(orderDates);
    } else {
      orderDates = (Element) orderDatesNodeList.item(0);
    }
    Element orderDateEle = inDoc.createElement(GCConstants.ORDER_DATE);
    orderDateEle.setAttribute(GCConstants.ACTUAL_DATE, GCDateUtils.getCurrentTime(GCConstants.DATE_TIME_FORMAT));
    orderDateEle.setAttribute(GCConstants.DATE_TYPE_ID, GCConstants.GC_ORDER_RESERVATION_DATE);
    orderDates.appendChild(orderDateEle);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("XML After appending reservation Date::" + GCXMLUtil.getXMLString(inDoc));
    }
    LOGGER.endTimer("stampReservationDate");
  }

  /**
   * This method removes all OrderLineReservation elements if only partial ordered qty is reserved
   * at that line.
   *
   * @param inDoc
   */
  private void removePartialReservations(Document inDoc, YFSEnvironment env) {
	boolean reservationsRemovedFlag = false;
    NodeList orderLineNodeList = inDoc.getElementsByTagName(GCConstants.ORDER_LINE);
    for (int i = 0; i < orderLineNodeList.getLength(); i++) {
      Element eleOrderLine = (Element) orderLineNodeList.item(i);
      String kitCode = eleOrderLine.getAttribute(GCConstants.KIT_CODE);
      Double orderedQty = Double.parseDouble(eleOrderLine.getAttribute(GCConstants.ORDERED_QTY));
      Double reservedQty = 0.0;
      if (!YFCCommon.equals(kitCode, GCConstants.BUNDLE)) {
        NodeList eleOrderLineReservations = eleOrderLine.getElementsByTagName(GCConstants.ORDER_LINE_RESERVATION);
        if (!YFCCommon.isVoid(eleOrderLineReservations) && eleOrderLineReservations.getLength() > 0) {
          NodeList orderLineReservation = eleOrderLine.getElementsByTagName(GCConstants.ORDER_LINE_RESERVATION);
          for (int j = 0; j < orderLineReservation.getLength(); j++) {
            Element eleOrderLineReservation = (Element) orderLineReservation.item(j);
            String sQty = eleOrderLineReservation.getAttribute(GCConstants.QUANTITY);
            Double qty = Double.parseDouble(sQty);
            reservedQty = reservedQty + qty;
          }
          if (orderedQty > reservedQty) {
            eleOrderLineReservations.item(0).getParentNode().removeChild(eleOrderLineReservations.item(0));
          }

        }
      }
      // GCSTORE-1644::Begin--Reverted these changes for 1644
      // Below code is to check if partial qty is reserved for a bundle if so removes the
      // reservations.
      // also it keeps batch no only for serialized component and removes from other components
      // reservations.

      String isBundleParent = eleOrderLine.getAttribute(GCConstants.IS_BUNDLE_PARENT);


      String orderHeaderKey = inDoc.getDocumentElement().getAttribute(GCConstants.ORDER_HEADER_KEY);
      if (YFCCommon.equals(isBundleParent, GCConstants.YES)
          || (YFCCommon.isVoid(orderHeaderKey) && YFCCommon.equals(kitCode, GCConstants.BUNDLE))) {
        // if it is bundle parent order line

        Node orderLineReservationsNode =
            eleOrderLine.getElementsByTagName(GCConstants.ORDER_LINE_RESERVATIONS).item(0);
        NodeList orderLineReservationNodeList = eleOrderLine.getElementsByTagName(GCConstants.ORDER_LINE_RESERVATION);
        if (orderLineReservationNodeList.getLength() > 0) {

          Element eleItem = (Element) eleOrderLine.getElementsByTagName(GCConstants.ITEM).item(0);
          String itemId = eleItem.getAttribute(GCConstants.ITEM_ID);

          String uom = eleItem.getAttribute(GCConstants.UNIT_OF_MEASURE);
          YFCDocument getItemListTmpl = YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LIST_TEMP);
          YFCDocument getItemListOutputDoc = GCCommonUtil.getItemList(env, itemId, uom, "GCI", true, getItemListTmpl);
          YFCElement eleExtn = getItemListOutputDoc.getElementsByTagName(GCConstants.EXTN).item(0);
          String extnSerialComponentItemId = eleExtn.getAttribute(GCConstants.EXTN_SERIAL_COMPONENT);
          YFCNodeList<YFCElement> componentList = getItemListOutputDoc.getElementsByTagName(GCConstants.COMPONENT);
          for (int k = 0; k < componentList.getLength(); k++) {
            YFCElement componentEle = componentList.item(k);
            String compItemID = componentEle.getAttribute(GCConstants.COMPONENT_ITEM_ID);
            String kitQty = componentEle.getAttribute(GCConstants.KIT_QUANTITY);
            Double dKitQty = Double.parseDouble(kitQty);
            Double totalReserved = 0.0;
            for (int j = 0; j < orderLineReservationNodeList.getLength(); j++) {
              Element orderLineReservationEle = (Element) orderLineReservationNodeList.item(j);

              String itemID = orderLineReservationEle.getAttribute(GCConstants.ITEM_ID);

              if (YFCCommon.equals(itemID, compItemID)) {
                String reservedQtytmp = orderLineReservationEle.getAttribute(GCConstants.QUANTITY);
                Double dReserved =Double.parseDouble(reservedQtytmp);
                String batchNo = orderLineReservationEle.getAttribute(GCConstants.BATCH_NO);
                totalReserved = totalReserved + dReserved;
                if (YFCCommon.isVoid(batchNo) || !YFCCommon.equals(itemID, extnSerialComponentItemId)) {
                  orderLineReservationEle.removeAttribute(GCConstants.BATCH_NO);
                }

              } else {
                continue;
              }
            }
            if (totalReserved != (orderedQty * dKitQty) && !YFCCommon.isVoid(orderLineReservationsNode)
                && !reservationsRemovedFlag) {
              orderLineReservationsNode.getParentNode().removeChild(orderLineReservationsNode);
              reservationsRemovedFlag = true;
            }


          }
        }
        // GCSTORE-1644::End
      }

    }
  }

  /**
   *
   * Description of getReservationID Generate reservationID from date time in millis and random no.
   *
   * @return reservationID
   *
   */
  public String getReservationID() {
    LOGGER.debug("Entering getReservationID method");
    String sReservationID = null;
    Calendar calSysDate = Calendar.getInstance();
    // Fetching the date time value in millis
    String sDateTime = String.valueOf(calSysDate.getTimeInMillis());

    int iRandomNo = new Random().nextInt(101);
    String sRandomNo = String.valueOf(iRandomNo);
    // Appending random number to DateTime
    sReservationID = sDateTime + sRandomNo;
    LOGGER.debug(" sDateTime" + sDateTime + "sRandomNo " + sRandomNo + " sReservationID " + sReservationID);
    LOGGER.debug("Exiting getReservationID method");
    return sReservationID;
  }

  /**
   *
   * Description of preparefindInventoryDoc Prepare input doc required for the findInventory API
   *
   * @param nlOrderLines
   * @param sReservationID
   * @param eleRootOfInDoc
   * @return ReserveInventoryInputDoc
   * @throws GCException
   *
   */
  public Document preparefindInventoryDoc(YFSEnvironment env, NodeList nlOrderLine, Document inDoc) throws GCException {
    LOGGER.debug("Entering preparefindInventoryDoc() method");
    Document docfindInventory = null;
    try {
      String sOrganizationCode = inDoc.getDocumentElement().getAttribute(GCConstants.ENTERPRISE_CODE);
      String srcClassification = inDoc.getDocumentElement().getAttribute("SourcingClassification");
      LOGGER.debug(" sOrganizationCode" + sOrganizationCode);
      docfindInventory = GCXMLUtil.createDocument(GCConstants.PROMISE);
      Element elePromise = docfindInventory.getDocumentElement();
      elePromise.setAttribute(GCConstants.ORGANIZATION_CODE, sOrganizationCode);
      // Adding sourcing classification required for DC sourced/ prepaid orders
      elePromise.setAttribute("SourcingClassification", srcClassification);
      Element elePromiseLines = GCXMLUtil.createElement(docfindInventory, GCConstants.PROMISE_LINES, null);
      elePromise.appendChild(elePromiseLines);
      // GCPhase2::Order Promising Ship to Store scenario::Begin
      LOGGER.verbose("InputDoc====>" + GCXMLUtil.getXMLString(inDoc));
      NodeList eleOrderExtnNL = GCXMLUtil.getNodeListByXpath(inDoc, "Order/Extn");
      String sExtnSourceSystem = "";
      if (!YFCCommon.isVoid(eleOrderExtnNL) && eleOrderExtnNL.getLength() > 0) {
        Element eleOrderExtn = (Element) eleOrderExtnNL.item(0);
        LOGGER.verbose("Order Extn ele==>" + GCXMLUtil.getXMLString(GCXMLUtil.getDocument(eleOrderExtn, true)));
        Element eleExtn = GCXMLUtil.createElement(docfindInventory, GCConstants.EXTN, null);
        eleExtn.setAttribute(GCConstants.EXTN_PICK_UP_STORE_ID,
            eleOrderExtn.getAttribute(GCConstants.EXTN_PICK_UP_STORE_ID));
        elePromise.appendChild(eleExtn);
        sExtnSourceSystem = eleOrderExtn.getAttribute(GCXmlLiterals.EXTN_SOURCE_SYSTEM);
      }
      // GCPhase2::Order Promising Ship to Store scenario::End
      // Iterating over the OrderLines and generating the PromiseLines to
      // reserve the inventory
      for (int counter = 0; counter < nlOrderLine.getLength(); counter++) {
        Element eleOrderLine = (Element) nlOrderLine.item(counter);
        // GCSTORE:2293::Begin
        Element eleItem = (Element) XPathAPI.selectSingleNode(eleOrderLine, GCConstants.ITEM);
        // GCSTORE;1229::Begin
        String bundleFulfillmentMode = eleItem.getAttribute(GCConstants.BUNDLE_FULFILLMENT_MODE);
        if (!YFCCommon.isVoid(bundleFulfillmentMode)) {
          eleItem.removeAttribute(GCConstants.BUNDLE_FULFILLMENT_MODE);
        }
        // GCSTORE:2293::End
        // GCSTORE-1229::Begin
        String sOrderedQty = eleOrderLine.getAttribute(GCConstants.ORDERED_QTY);
        // GCSTORE-2707::Removed code to calculate reserved qty as its not needed any more.
        eleOrderLine.setAttribute("UnReservedQty", sOrderedQty);
        // GCPhase2::Begin==>Logic for excluding OrderLines which are already reserved.

        NodeList eleOrderLineReservations = eleOrderLine.getElementsByTagName(GCConstants.ORDER_LINE_RESERVATIONS);
        if (!YFCCommon.isVoid(eleOrderLineReservations) && eleOrderLineReservations.getLength() > 0) {
          // Order Capture : Begin
          if (YFCCommon.equals(GCConstants.ORDER_CATURE_SYSTEM_ATG, sExtnSourceSystem)) {
            eleOrderLine.setAttribute("PreReserved", "Y");
            continue;
          }
          // Order Capture : End

        }
        // GCSTORE-1229::End

        // GCPhase2::End==>Logic for excluding OrderLines which are already reserved.
        String isScheduledOrderLine = eleOrderLine.getAttribute("IsScheduledOrderLine");
        if (!YFCCommon.isVoid(isScheduledOrderLine)) {
          continue;
        }

        Element eleBundleParentLine = (Element) XPathAPI.selectSingleNode(eleOrderLine, GCConstants.BUNDLE_PARENT_LINE);
        if (YFCCommon.isVoid(eleBundleParentLine)) {


          Element elePromiseLine = GCXMLUtil.createElement(docfindInventory, GCConstants.PROMISE_LINE, null);
          elePromiseLine.setAttribute(GCConstants.ITEM_ID, eleItem.getAttribute(GCConstants.ITEM_ID));
          elePromiseLine.setAttribute(GCConstants.PRODUCT_CLASS, eleItem.getAttribute(GCConstants.PRODUCT_CLASS));
          elePromiseLine.setAttribute(GCConstants.UOM, eleItem.getAttribute(GCConstants.UOM));
          // GCSTORE-1229::Begin
          elePromiseLine.setAttribute(GCConstants.REQUIRED_QTY, eleOrderLine.getAttribute("UnReservedQty"));
          // GCSTORE-1229::End
          elePromiseLine.setAttribute(GCConstants.FULFILLMENT_TYPE,
              eleOrderLine.getAttribute(GCConstants.FULFILLMENT_TYPE));

          // GCPhase2::Begin
          if (!YFCCommon.isVoid(eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO))) {
            // if call center order
            elePromiseLine.setAttribute(GCConstants.LINE_ID, eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO));
          } else {
            // if ATG Order without reservation
            elePromiseLine.setAttribute(GCConstants.LINE_ID, eleOrderLine.getAttribute("TransactionalLineId"));
          }
          Element eleExtn = GCXMLUtil.createElement(docfindInventory, GCConstants.EXTN, null);
          elePromiseLine.appendChild(eleExtn);
          // --Start::Changed due to call center's existing logic

          Element linePriceInfoEle = (Element) eleOrderLine.getElementsByTagName("LinePriceInfo").item(0);
          String sUnitPrice = linePriceInfoEle.getAttribute("UnitPrice");
          LOGGER.verbose("UnitPrice in Log stmt=" + sUnitPrice);
          LOGGER.verbose("ListPrice in Log stmt=" + linePriceInfoEle.getAttribute("ListPrice"));
          if (YFCCommon.isVoid(sUnitPrice)) {
            eleExtn.setAttribute("ExtnUnitPrice", linePriceInfoEle.getAttribute("ListPrice"));
          } else {
            eleExtn.setAttribute("ExtnUnitPrice", linePriceInfoEle.getAttribute("UnitPrice"));
          }
          LOGGER.verbose("ExtnUnitPrice in Log stmt=" + eleExtn.getAttribute("ExtnUnitPrice"));
          String storeClearanceReq = GCXMLUtil.getAttributeFromXPath(eleOrderLine, "Extn/@ExtnIsStoreClearance");

          if (YFCCommon.equals(storeClearanceReq, GCConstants.YES,
              true)) {
            eleExtn.setAttribute("ExtnIsStoreClearance",
                storeClearanceReq);
          }
          // --End::
          elePromiseLines.appendChild(elePromiseLine);
          //This attribute is to make sure that PromiseLine element has been added and findInventory call can be made. else do not call findInventory.
          elePromise.setAttribute("CallFindInventory", "Y");

          String isFirmPredefinedNode = eleOrderLine.getAttribute("IsFirmPredefinedNode");
          String shipNode = eleOrderLine.getAttribute("ShipNode");

          if (YFCCommon.equals(isFirmPredefinedNode, "Y", false) && !YFCCommon.isVoid(shipNode)) {
            // Filteration of sourcing not allowed stores starts
            List<String> shipNodeList = new ArrayList<String>();
            shipNodeList.add(shipNode);
            shipNodeList = GCOrderUtil.getStoresAllowedForSourcing(env, shipNodeList);
            if (!shipNodeList.isEmpty()) {
              // Filteration of sourcing not allowed stores ends
              elePromiseLine.setAttribute("ShipNode", shipNode);
            }
          }
          boolean isTagItem = checkIfTagItem(eleOrderLine);
          if (isTagItem) {
            Element eleTag = GCXMLUtil.createElement(docfindInventory, GCXmlLiterals.TAG, null);
            elePromiseLine.appendChild(eleTag);
            eleTag
            .setAttribute("BatchNo",
                ((Element) eleOrderLine.getElementsByTagName("OrderLineInvAttRequest").item(0))
                .getAttribute("BatchNo"));
          }
          // GCPhase2::End
        }
      }

    } catch (Exception e) {
      LOGGER.error(" Inside catch of GCReserveInventoryService.preparefindInventoryDoc()", e);
      throw new GCException(e);
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" final docReserveInventory is : " + GCXMLUtil.getXMLString(docfindInventory));
    }
    LOGGER.debug("Exiting preparefindInventoryDoc() method");
    return docfindInventory;
  }

  /**
   * GCSTORE-1229:: Removes cancelledQty from OrderLineReservation Element. First from Stores other
   * then pickup store, then MFi , then from Pickup Store.
   *
   * @param inDoc
   * @param eleOrderLine
   * @throws TransformerException
   */
  private void removeCancelledQtyFromReservation(Document inDoc, Element eleOrderLine) throws TransformerException {
    LOGGER.beginTimer("removeCancelledQtyFromReservation");
    LOGGER.verbose("removeCancelledQtyFromReservation" + GCXMLUtil.getXMLString(inDoc));
    Element eleOrder = inDoc.getDocumentElement();
    Element orderExtn = (Element) XPathAPI.selectSingleNode(eleOrder, GCConstants.EXTN);
    String extnPickUpStoreId = orderExtn.getAttribute(GCConstants.EXTN_PICK_UP_STORE_ID);
    String sUnReservedQty = eleOrderLine.getAttribute("UnReservedQty");
    LOGGER.verbose("UnReservedQty===" + sUnReservedQty);
    Double dUnReservedQty = Double.parseDouble(sUnReservedQty);
    Double qtyLeftToRemove = Math.abs(dUnReservedQty);
    NodeList eleOrderLineReservations = eleOrderLine.getElementsByTagName(GCConstants.ORDER_LINE_RESERVATIONS);
    if (!YFCCommon.isVoid(eleOrderLineReservations) && eleOrderLineReservations.getLength() > 0) {
      NodeList orderLineReservationNL = eleOrderLine.getElementsByTagName(GCConstants.ORDER_LINE_RESERVATION);
      if (orderLineReservationNL.getLength() > 0) {
        while (qtyLeftToRemove > 0) {
          int mfiReservationIndex = 999999;
          int pickUpStoreReservaionIndex = 999999;
          LOGGER.verbose("OrderLineReservation Elements==" + orderLineReservationNL.getLength());
          for (int i = 0; i < orderLineReservationNL.getLength(); i++) {
            Element orderLineReservation = (Element) orderLineReservationNL.item(i);
            String sQty = orderLineReservation.getAttribute("Quantity");
            Double dQty = Double.parseDouble(sQty);
            String node = orderLineReservation.getAttribute("Node");
            Double qty = 0.0;
            LOGGER.verbose("Iteration::" + i + "==qty::" + dQty + "==Node::" + node + "==QtyLeftToRemove::"
                + qtyLeftToRemove);
            if (!YFCCommon.equals(node, extnPickUpStoreId) && !YFCCommon.equals(node, GCConstants.MFI)) {
              LOGGER.verbose("Other store::");
              if (dQty >= qtyLeftToRemove) {
                qty = dQty - qtyLeftToRemove;
                qtyLeftToRemove = 0.0;
              } else {
                qty = 0.0;
                qtyLeftToRemove = qtyLeftToRemove - dQty;
              }
            } else if (qtyLeftToRemove > 0) {
              if (YFCCommon.equals(node, GCConstants.MFI)) {
                mfiReservationIndex = i;
                continue;
              } else if (YFCCommon.equals(node, extnPickUpStoreId)) {
                pickUpStoreReservaionIndex = i;
                continue;
              }
            }
            LOGGER.verbose("Quantity at OrderLine Reservation after removing cancelled qty is ::" + qty
                + "QuantityLeftToRemove::" + qtyLeftToRemove);
            if (qty == 0) {
              orderLineReservation.getParentNode().removeChild(orderLineReservation);
              i--;
            } else {
              orderLineReservation.setAttribute("Quantity", qty + GCConstants.BLANK);
            }

          }
          if (qtyLeftToRemove > 0) {
            if (!YFCCommon.equals(mfiReservationIndex, 999999)) {
              Element orderLineReservation = (Element) orderLineReservationNL.item(mfiReservationIndex);
              String sQty = orderLineReservation.getAttribute("Quantity");
              Double dQty = Double.parseDouble(sQty);
              Double qty = 0.0;
              if (dQty >= qtyLeftToRemove) {
                qty = dQty - qtyLeftToRemove;
                qtyLeftToRemove = 0.0;
              } else {
                qty = 0.0;
                qtyLeftToRemove = qtyLeftToRemove - dQty;
              }
              LOGGER.verbose("Quantity at OrderLine Reservation after removing cancelled qty from MFI is ::" + qty);
              if (qty == 0) {
                orderLineReservation.getParentNode().removeChild(orderLineReservation);
                break;
              } else {
                orderLineReservation.setAttribute("Quantity", qty + GCConstants.BLANK);
              }
            }
            if (qtyLeftToRemove > 0 && !YFCCommon.equals(pickUpStoreReservaionIndex, 999999)) {
              Element orderLineReservation = (Element) orderLineReservationNL.item(pickUpStoreReservaionIndex);
              String sQty = orderLineReservation.getAttribute("Quantity");
              Double dQty = Double.parseDouble(sQty);
              Double qty = 0.0;
              if (dQty >= qtyLeftToRemove) {
                qty = dQty - qtyLeftToRemove;
                qtyLeftToRemove = 0.0;
              } else {
                qty = 0.0;
                qtyLeftToRemove = qtyLeftToRemove - dQty;
              }
              LOGGER
              .verbose("Quantity at OrderLine Reservation after removing cancelled qty from PickupStoreId is ::"
                  + qty);
              if (qty == 0) {
                orderLineReservation.getParentNode().removeChild(orderLineReservation);
                break;
              } else {
                orderLineReservation.setAttribute("Quantity", qty + GCConstants.BLANK);
              }
            }
          }
        }
      }
    }

    LOGGER.endTimer("removeCancelledQtyFromReservation");
  }

  /**
   * GCPhase2:This method checks if the given orderline has Tagged item or not.
   *
   * @param eleOrderLine
   * @return
   */
  private boolean checkIfTagItem(Element eleOrderLine) {
    Element eleOrderLineInvAttRequest = (Element) eleOrderLine.getElementsByTagName("OrderLineInvAttRequest").item(0);
    if (!YFCCommon.isVoid(eleOrderLineInvAttRequest) && eleOrderLineInvAttRequest.hasAttribute("BatchNo")) {
      return true;
    }
    return false;
  }

  /**
   *
   * Description of updateInputDoc Iterate on the ReserveInventory output doc and fetch OrderLine
   * having same ItemID from the inDoc and add OrderLineReservation element to it, append ShipNode,
   * reservationID to it
   *
   * @param inDoc
   * @param docFindInvOutput
   * @param nlOrderLines
   * @param sReservationID
   * @throws GCException
   *
   */
  public void updateInputDoc(Document inDoc, Document docFindInvOutput, String sReservationID) throws GCException {
    LOGGER.verbose("UpdateInputDoc Method:::" + GCXMLUtil.getXMLString(inDoc));
    LOGGER.debug("Entering updateInputDoc() method");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" docFindInvOutput is : " + GCXMLUtil.getXMLString(docFindInvOutput));
    }
    try {
      Calendar calRemorseDate = Calendar.getInstance();
      String sOrderDate = inDoc.getDocumentElement().getAttribute(GCConstants.ORDER_DATE);
      if (!YFCCommon.isVoid(sOrderDate)) {
        Date strOrderDate = new SimpleDateFormat(GCConstants.DATE_FORMAT).parse(sOrderDate);
        calRemorseDate.setTime(strOrderDate);
      }

      // Fetch PromiseLine node list, iterate on PromiseLine and fetch
      // shipnode from each
      NodeList nlPromiseLine =
          XPathAPI.selectNodeList(docFindInvOutput, "Promise/SuggestedOption/Option/PromiseLines/PromiseLine");
      Element eleOrderLine = null;
      for (int iCount = 0; iCount < nlPromiseLine.getLength(); iCount++) {
        Element elePromiseLine = (Element) nlPromiseLine.item(iCount);
        String sItemID = elePromiseLine.getAttribute(GCConstants.ITEM_ID);
        String sIsBundleParent = elePromiseLine.getAttribute(GCConstants.IS_BUNDLE_PARENT);
        // GCPhase2::OrderPromising ShiptoStore scenario Enahancement::Begin
        String sLineId = getLineId(elePromiseLine);
        // GCPhase2::OrderPromising ShiptoStore scenario Enahancement::End
        String sKitQty = elePromiseLine.getAttribute("KitQty");
        LOGGER.debug(" sItemID is :" + sItemID);

        // Removed below condition to use PrimeLineNo/ TransactionLineId to use only for non bundle
        // and ItemID for bundle to map findInventory output
        eleOrderLine =
            (Element) XPathAPI.selectSingleNode(inDoc, "Order/OrderLines/OrderLine" + "[@PrimeLineNo='" + sLineId
                + "']");
        if (YFCCommon.isVoid(eleOrderLine)) {
          eleOrderLine =
              (Element) XPathAPI.selectSingleNode(inDoc, "Order/OrderLines/OrderLine" + "[@TransactionalLineId='"
                  + sLineId + "']");
        }
        //  TEMP fix for Order Capture End

        if (!YFCCommon.isVoid(eleOrderLine)) {
          // GCSTORE-2879::Begin
          NodeList eleOrderLineInvAttr = eleOrderLine.getElementsByTagName(GCConstants.ORDER_INV_ATTR_REQ);
          String batchNo = "";
          if (!YFCCommon.isVoid(eleOrderLineInvAttr) && eleOrderLineInvAttr.getLength() > 0) {
            batchNo = ((Element) eleOrderLineInvAttr.item(0)).getAttribute(GCConstants.BATCH_NO);
          }
          // GCSTORE-2879::End
          // GCPhase2::OrderPromising ShiptoStore scenario Enahancement::Begin
          Element eleOrderLineReservations;
          NodeList orderLineReservationElem = eleOrderLine.getElementsByTagName(GCConstants.ORDER_LINE_RESERVATIONS);

          if (YFCCommon.isVoid(orderLineReservationElem) || orderLineReservationElem.getLength() == 0) {
            eleOrderLineReservations = GCXMLUtil.createElement(inDoc, GCConstants.ORDER_LINE_RESERVATIONS, null);
            eleOrderLine.appendChild(eleOrderLineReservations);
          } else {
            eleOrderLineReservations = (Element) orderLineReservationElem.item(0);
          }
          // GCPhase2::OrderPromising ShiptoStore scenario Enahancement::End


          // Check if it is bundle parent line
          if (GCConstants.YES.equalsIgnoreCase(sIsBundleParent)) {
            LOGGER.debug(" Inside if is bundle parent ");
            NodeList nlComponentLine =
                XPathAPI.selectNodeList(docFindInvOutput, "Promise/SuggestedOption/Option"
                    + "/PromiseLines/PromiseLine[contains(@LineId,'" + sLineId + "::')]");
            for (int iCounter = 0; iCounter < nlComponentLine.getLength(); iCounter++) {
              Element eleComponentLine = (Element) nlComponentLine.item(iCounter);
              // GCSTORE-1229::Begin
              stampOLReservationEle(inDoc, eleComponentLine, eleOrderLineReservations, sReservationID,
                  eleOrderLine.getAttribute("UnReservedQty"), batchNo);
              // GCSTORE-1229::End
            }
          } else if (YFCCommon.isVoid(sKitQty)) {
            // Check if it is a regular line (it should not be
            // component
            // line)
            // GCSTORE-1229::Begin
            stampOLReservationEle(inDoc, elePromiseLine, eleOrderLineReservations, sReservationID,
                eleOrderLine.getAttribute("UnReservedQty"), batchNo);
            // GCSTORE-1229::End
          }

        }
      }
    } catch (Exception e) {
      LOGGER.error(" Inside catch of GCReserveInventoryService.updateInputDoc()", e);
      throw new GCException(e);
    }
    LOGGER.debug("Exiting updateInputDoc() method");
  }

  /**
   * This Method will get the LineId from given promiseLine and if the lineId contains '_' then it
   * will get the first token from lineId and return.
   *
   * @param elePromiseLine
   * @return
   */
  private String getLineId(Element elePromiseLine) {
    String lineId;
    String lineIdTokenizerStr = elePromiseLine.getAttribute(GCConstants.LINE_ID);
    if (!lineIdTokenizerStr.contains("_")) {
      lineId = lineIdTokenizerStr;
    } else {
      StringTokenizer lineIdTokens = new StringTokenizer(lineIdTokenizerStr, "_");
      lineId = lineIdTokens.nextToken();
    }
    return lineId;
  }

  /**
   *
   * Description of stampOLReservationEle
   *
   * @param inDoc
   * @param eleLine
   * @param eleOrderLineReservations
   * @param eleOrderLine
   * @param sReservationID unReservedQty * @throws Exception
   * @param batchNo
   *
   */
  private void stampOLReservationEle(Document inDoc, Element eleLine, Element eleOrderLineReservations,
      String sReservationID, String unReservedQty, String batchNo) throws GCException {
    LOGGER.verbose("UnReservedQty in stampOLReservationEle ====" + unReservedQty);
    try {
      NodeList nlAssignment = XPathAPI.selectNodeList(eleLine, "Assignments/Assignment");
      for (int iCountReservation = 0; iCountReservation < nlAssignment.getLength(); iCountReservation++) {
        Element eleAssignment = (Element) nlAssignment.item(iCountReservation);
        String sShipNode = eleAssignment.getAttribute(GCConstants.SHIP_NODE);

        if (!YFCCommon.isVoid(sShipNode)) {
          // GCSTORE-1229::Begin
          appendOrderLineReservations(inDoc, eleAssignment, sReservationID, eleLine, eleOrderLineReservations, batchNo);
          // GCSTORE-1229::End
        }
      }
    } catch (Exception e) {
      LOGGER.error(" Inside catch of stampOLReservationEle()", e);
      throw new GCException(e);
    }
  }

  /**
   *
   * Description of appendOrderLineReservations This method will add attributes to
   * OrderLineReservation element including ShipNode, reservationID
   *
   * @param inDoc
   * @param eleAssignment
   * @param sReservationID
   * @param batchNo
   * @return eleOrderLineReservation
   * @throws Exception
   *
   */
  private void appendOrderLineReservations(Document inDoc, Element eleAssignment, String sReservationID,
      Element eleLine, Element eleOrderLineReservations, String batchNo) throws GCException {
    LOGGER.debug("Entering appendOrderLineReservations() method");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" inDoc is : " + GCXMLUtil.getXMLString(inDoc));
      LOGGER.debug(" elePromiseLine is : " + GCXMLUtil.getElementXMLString(eleAssignment));
    }
    LOGGER.debug(" sReservationID is : " + sReservationID);
    Element eleOrderLineReservation = null;
    try {
      // GCSTORE-2330::Begin



      String sQuantity = eleAssignment.getAttribute(GCConstants.QUANTITY);
      String sShipNode = eleAssignment.getAttribute(GCConstants.SHIP_NODE);
      String sProductAvailabilityDate = eleAssignment.getAttribute(GCConstants.PRODUCT_AVAIL_DATE);
      String itemId = eleLine.getAttribute(GCConstants.ITEM_ID);
      LOGGER.debug(" sQuantity is : " + sQuantity + " sShipNode " + sShipNode + " sProductAvailabilityDate "
          + sProductAvailabilityDate);

      eleOrderLineReservation = GCXMLUtil.createElement(inDoc, GCConstants.ORDER_LINE_RESERVATION, null);
      eleOrderLineReservation.setAttribute(GCConstants.BATCH_NO, batchNo);
      eleOrderLineReservation.setAttribute(GCConstants.RESERVATION_ID, sReservationID);
      eleOrderLineReservation.setAttribute(GCConstants.NODE, sShipNode);
      eleOrderLineReservation.setAttribute(GCConstants.UOM, eleLine.getAttribute(GCConstants.UOM));
      eleOrderLineReservation
      .setAttribute(GCConstants.PRODUCT_CLASS, eleLine.getAttribute(GCConstants.PRODUCT_CLASS));
      eleOrderLineReservation.setAttribute(GCConstants.ITEM_ID, itemId);
      eleOrderLineReservation.setAttribute(GCConstants.QUANTITY, sQuantity);
      eleOrderLineReservation.setAttribute(GCConstants.REQUESTED_RESERVATION_DATE,
          GCDateUtils.getCurrentTime(GCConstants.DATE_FORMAT));
      eleOrderLineReservation.setAttribute(GCConstants.PRODUCT_AVAILABILITY_DATE, sProductAvailabilityDate);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(" eleOrderLineReservation is : " + GCXMLUtil.getElementXMLString(eleOrderLineReservation));
      }

      eleOrderLineReservations.appendChild(eleOrderLineReservation);


    } catch (Exception e) {
      LOGGER.error(" Inside catch of appendOrderLineReservations()", e);
      throw new GCException(e);
    }
    LOGGER.debug("Exiting appendOrderLineReservations() method");
    return;
  }

  @Override
  public void setProperties(Properties props) throws GCException {
    prop = props;
  }

}
