/**Copyright 2013 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.

    Change Log
######################################################################################################################################
     OBJECTIVE: Foundation Class
######################################################################################################################################
        Version    CR                 Date                       Modified By                   Description
######################################################################################################################################
        1.0    Initial Version      12/18/2014                   Shinde,Abhishek           GCSTORE-66 - Reserve Inventory Service
        2.0    None                 02/12/2015                   Mittal, Yashu             Added method getReservationExpirationDate.
        3.0    None                 03/19/2015                   Saxena, Ekansh            Added code for bundle with tag component.
######################################################################################################################################
 */
package com.gc.api;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCOrderUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * This is Wrapper class for Reserve Inventory. ATG will call this class as webservice. This class
 * will validate\stamp required input for reserve Inventory.
 *
 * @author abhishek.shinde
 *
 */
public class GCReserveInventoryWrapper implements YIFCustomApi {
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCReserveInventoryService.class);

  @Override
  public void setProperties(Properties prop) {

  }

  /**
   * reserverInventory method will first apply low value item check, and then validate the input for
   * reservation and get the reservationID and call reserveAvaliableInventory API.
   *
   * @param env
   * @param doc
   * @return
   */
  public Document reserveInventory(YFSEnvironment env, Document doc) {
    LOGGER.beginTimer("GCReserveInventoryWrapper.reserveInventory");
    YFCDocument inDoc = YFCDocument.getDocumentFor(doc);
    YFCDocument originalInDoc = inDoc.getCopy();
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Incoming XML Doc" + inDoc.toString());
    }

    String sUOM;
    String sItemID;
    String sProductClass;
    String sBatchNo = null;
    YFCDocument getItemListoutDoc;
    YFCDocument getItemListOpTmpl;
    boolean isShipNodeBlank = false;
    boolean isPickUpInStore = false;
    boolean isUnplannedInventoryReqd = false;
    GCReserveInventoryService oReserveInventoryService = new GCReserveInventoryService();
    // get the reservation id
    String sResId = oReserveInventoryService.getReservationID();

    YFCElement elePromise = inDoc.getDocumentElement();
    String pFulfillmentType = elePromise.getAttribute(GCConstants.FULFILLMENT_TYPE);
    YFCNodeList<YFCElement> nlPromiseLine = inDoc.getElementsByTagName(GCConstants.PROMISE_LINE);

    Map<String, ReserveInventoryPromiseLineInfo> hmLineIdAndPromiseLineInfo =
        new HashMap<String, ReserveInventoryPromiseLineInfo>();

    for (YFCElement elePromiseLine : nlPromiseLine) {
      // Added to filter out
      filterOutNodesNotAllowedForSourcing(env, elePromiseLine);
      isShipNodeBlank = isShipNodeBlank || isShipNodeBlank(elePromiseLine);
      // validate low value check
      sUOM = elePromiseLine.getAttribute(GCConstants.UNIT_OF_MEASURE);
      sItemID = elePromiseLine.getAttribute(GCConstants.ITEM_ID);
      String lineLevelFulfillmentType = elePromiseLine.getAttribute(GCConstants.FULFILLMENT_TYPE);
      getItemListOpTmpl = YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LIST_TEMPLATE_ORDER_PROMISING);
      getItemListoutDoc = GCCommonUtil.getItemList(env, sItemID, sUOM, GCConstants.GCI, true, getItemListOpTmpl);
      GCOrderUtil.stampFulfillmentTypeForLowValueItems(env, elePromiseLine, getItemListoutDoc, inDoc);

      // validate shipNode if serial Item is requested
      GCOrderUtil.validateShipNodeForSerialItem(elePromiseLine, GCErrorConstants.GCE00002);

      sProductClass = GCOrderUtil.getProductClass(getItemListoutDoc);
      LOGGER.verbose("Product Class for the Item is ::" + sProductClass);
      elePromiseLine.setAttribute(GCConstants.PRODUCT_CLASS, sProductClass);

      StringBuilder sReservationId = new StringBuilder(sResId);
      sReservationId.append(GCConstants.UNDERSCORE);
      sReservationId.append(sItemID);
      YFCElement eleReservationParameters = elePromiseLine.createChild(GCConstants.RESERVATION_PARAMETERS);
      eleReservationParameters.setAttribute(GCConstants.RESERVATION_ID, sReservationId.toString());
      // Stamping ExpirationDate : Start
      eleReservationParameters.setAttribute("ExpirationDate", getReservationExpirationDate(env));
      // Stamping ExpirationDate : End

      YFCElement eleItemList = getItemListoutDoc.getDocumentElement();
      YFCElement eleItem = eleItemList.getChildElement(GCConstants.ITEM);
      if(!YFCCommon.isVoid(eleItem)){
      YFCElement elePrimaryInfo = eleItem.getChildElement(GCConstants.PRIMARY_INFORMATION);
      String sKitCode = elePrimaryInfo.getAttribute(GCConstants.KIT_CODE);

      YFCElement eleTag = elePromiseLine.getChildElement(GCXmlLiterals.TAG);
      if (!YFCCommon.isVoid(eleTag)) {
        sBatchNo = eleTag.getAttribute(GCConstants.BATCH_NO);
      }

      if (YFCCommon.equals(GCConstants.BUNDLE, sKitCode, false) && !YFCCommon.isStringVoid(sBatchNo)) {
        ReserveInventoryPromiseLineInfo promiseLineInfo = new ReserveInventoryPromiseLineInfo();
        promiseLineInfo.setGetItemListOp(getItemListoutDoc);
        hmLineIdAndPromiseLineInfo.put(elePromiseLine.getAttribute(GCConstants.LINE_ID), promiseLineInfo);
      }

      boolean isClearanceItem = GCOrderUtil.isClearanceItem(getItemListoutDoc);
      boolean isReqRelatedToClearanceItem =
          GCOrderUtil.stampFulfillmentTypeForClearance(elePromiseLine, isClearanceItem, pFulfillmentType);
      if (!isReqRelatedToClearanceItem) {
        boolean isOnHandReq = GCOrderUtil.stampFulfilmentTypeForOnhand(elePromiseLine, pFulfillmentType);
        if (!isOnHandReq) {
          GCOrderUtil.stampFulfillmentTypeForLowValueItems(env, elePromiseLine, getItemListoutDoc, inDoc);
        }
      }
      }
      isPickUpInStore =
          isPickUpInStore
          || (!YFCCommon.isVoid(lineLevelFulfillmentType) && lineLevelFulfillmentType
              .indexOf(GCConstants.PICKUP_IN_STORE) >= 0);
      if (YFCCommon.equals(pFulfillmentType, GCConstants.PICKUP_IN_STORE, false)) {
        YFCElement eleShipToAddress = elePromiseLine.getChildElement(GCXmlLiterals.SHIP_TO_ADDRESS);
        if (YFCCommon.isVoid(eleShipToAddress)) {
          YFSException ex = new YFSException();
          ex.setErrorCode(GCErrorConstants.GCE00003);
          ex.setErrorDescription("ShipToAddress is mandatory for PickUpInStore FulfillmentType");
          throw ex;
        }
      }
      String sFulfillmentType = elePromise.getAttribute(GCXmlLiterals.FULFILLMENT_TYPE);
      boolean isKit = GCOrderUtil.isRegularKit(env, getItemListoutDoc);
      if (isKit) {
        String promiseLineFulfillmentType = elePromiseLine.getAttribute(GCXmlLiterals.FULFILLMENT_TYPE);
        if (YFCCommon.isVoid(promiseLineFulfillmentType)) {
          promiseLineFulfillmentType = sFulfillmentType;
        }
        if (YFCCommon.equals(promiseLineFulfillmentType, GCConstants.SHIP_2_CUSTOMER, false)) {
          promiseLineFulfillmentType = GCConstants.SHIP_2_CUSTOMER_KIT;
        }
        if (YFCCommon.equals(promiseLineFulfillmentType, GCConstants.PICKUP_IN_STORE, false)) {
          promiseLineFulfillmentType = GCConstants.PICKUP_IN_STORE_KIT;
        }
        if (YFCCommon.equals(sFulfillmentType, GCConstants.SHIP_2_CUSTOMER_ONHAND, false)) {
          sFulfillmentType = GCConstants.SHIP_2_CUSTOMER_KIT_ONHAND;
        }
        if (YFCCommon.equals(sFulfillmentType, GCConstants.PICKUP_IN_STORE_ONHAND, false)) {
          sFulfillmentType = GCConstants.PICKUP_IN_STORE_KIT_ONHAND;
        }
        elePromiseLine.setAttribute(GCConstants.FULFILLMENT_TYPE, promiseLineFulfillmentType);
        YFCElement shipNodesEle = elePromiseLine.getChildElement(GCXmlLiterals.SHIP_NODES);
        if (!YFCCommon.isVoid(shipNodesEle)) {
          YFCNodeList<YFCElement> shipNodeList = shipNodesEle.getElementsByTagName(GCXmlLiterals.SHIP_NODE);
          for (int i = 0; i < shipNodeList.getLength(); i++) {
            YFCElement shipNodeEle = shipNodeList.item(i);
            String shipNode = shipNodeEle.getAttribute(GCConstants.NODE);
            if (!YFCUtils.equals(GCConstants.MFI, shipNode)) {
              YFCElement eleExcludedShipNodes = elePromiseLine.getChildElement("ExcludedShipNodes");
              if (YFCCommon.isVoid(eleExcludedShipNodes)) {
                eleExcludedShipNodes = elePromiseLine.createChild("ExcludedShipNodes");
              }
              YFCElement eleExcludedShipNode = eleExcludedShipNodes.createChild("ExcludedShipNode");
              eleExcludedShipNode.setAttribute(GCConstants.NODE, shipNode);
              eleExcludedShipNode.setAttribute("SupressSourcing", GCConstants.YES);
            }
          }
        }
      }
    }
    isPickUpInStore =
        isPickUpInStore
        || (!YFCCommon.isVoid(pFulfillmentType) && pFulfillmentType.indexOf(GCConstants.PICKUP_IN_STORE) >= 0);

    // Stamp AllcoationRuleID
    if (!isShipNodeBlank && isPickUpInStore) {
      elePromise.setAttribute(GCXmlLiterals.ALLOCATION_RULE_ID, GCConstants.GC_SHIP_SINGLE_NODE);
    } else {
      elePromise.setAttribute(GCXmlLiterals.ALLOCATION_RULE_ID, GCConstants.GC_DEFAULT);
    }

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Modified ReserveAvailableInventory Input XML" + doc.toString());
    }

    // To break bundle with tagged component into components - Start
    if (!hmLineIdAndPromiseLineInfo.isEmpty()) {
      inDoc = splitInputForSerialBundles(env, inDoc, hmLineIdAndPromiseLineInfo);
    }
    // To break bundle with tagged component into components - End

    // GCPhase2::Splitting logic::Begin

    String sExtnPickStoreId;
    // Defect Fix:: GCSTORE-1639::Begin
    Document docTmpl = GCCommonUtil.getDocumentFromPath(GCConstants.PATH_FOR_RESERVE_INVENTORY_TMPL_XML);
    YFCDocument templateDoc = YFCDocument.getDocumentFor(docTmpl);
    // Defect Fix:: GCSTORE-1639::End

    if (YFCCommon.equals(pFulfillmentType, GCConstants.PICKUP_IN_STORE)) {
      YFCElement eleExtn = elePromise.getChildElement(GCConstants.EXTN);
      if (!YFCCommon.isVoid(eleExtn)) {
        sExtnPickStoreId = eleExtn.getAttribute(GCXmlLiterals.EXTN_PICK_UP_STORE_ID);
        if (YFCCommon.isVoid(sExtnPickStoreId)) {
          YFSException ex = new YFSException();
          ex.setErrorCode(GCErrorConstants.GCE00004);
          throw ex;
        }
      } else {
        YFSException ex = new YFSException();
        ex.setErrorCode(GCErrorConstants.GCE00004);
        throw ex;
      }
            
      boolean isShipToStore = GCOrderUtil.isShipToStoreScenario(env, inDoc, sExtnPickStoreId);
      // Check if UseUnplannedInventory="N" is required
      isUnplannedInventoryReqd = getUseUnplannedInventoryUpdated(env, inDoc);
      if (isUnplannedInventoryReqd) {
        elePromise.setAttribute("UseUnplannedInventory", GCConstants.N);
      }
      if (isShipToStore) {
        // ship to store scenario
        YFCDocument outDoc = GCCommonUtil.invokeAPI(env, GCConstants.RESERVE_AVAILABLE_INVENTORY, inDoc, templateDoc);
        YFCDocument inDocClone = inDoc.getCopy();
        YFCNodeList<YFCElement> promiseLineNodeList = inDoc.getElementsByTagName("PromiseLine");
        for (YFCElement elePromiseLine : promiseLineNodeList) {
          String lineId = elePromiseLine.getAttribute("LineId");
          Map<String, YFCElement> reservationOutputMap = getPromiseLineMapFromReservationOutput(outDoc);
          YFCElement outPromiseLineEle = reservationOutputMap.get(lineId);

          YFCElement reservationEle = outPromiseLineEle.getChildElement("Reservations");
          Double totalReservedQuantity = reservationEle.getDoubleAttribute("TotalReservedQty");
          Map<String, YFCElement> rservationInputPromiseLineMap = getReservationPromiseLineInputMap(inDocClone);
          if (totalReservedQuantity == 0.0) {
            Double availableQy = reservationEle.getDoubleAttribute("AvailableQty");
            Double requiredQty = elePromiseLine.getDoubleAttribute("RequiredQty");
            if (availableQy == 0.0 && !YFCCommon.isVoid(rservationInputPromiseLineMap)) {
              YFCElement promiseLineEle = rservationInputPromiseLineMap.get(lineId);
              promiseLineEle.setAttribute("ShipNode", "");
              YFCElement eleShipNodes = promiseLineEle.getChildElement("ShipNodes");
              if (!YFCCommon.isVoid(eleShipNodes)) {
                eleShipNodes.getParentNode().removeChild(eleShipNodes);
              }
            } else if (availableQy < requiredQty) {
              // Split Lines so that the qty available at pickup store will get reserved from pickup
              // store id passed and rest of the qty based on sourcing rules defined.
              YFCElement elePromiseClone = inDocClone.getDocumentElement();
              elePromiseClone.removeAttribute("UseUnplannedInventory");
              YFCElement elePromiseLineClone = inDocClone.createElement("PromiseLine");
              elePromiseLineClone = (YFCElement) elePromiseLine.cloneNode(true);
              YFCElement promiseLinesEle = inDocClone.getElementsByTagName("PromiseLines").item(0);

              // set attributes
              // set requiredQty for existing promiseLine as the available qty at the given pickup
              // store
              if (!YFCCommon.isVoid(rservationInputPromiseLineMap)) {
                YFCElement promiseLineEle = rservationInputPromiseLineMap.get(lineId);
                promiseLineEle.setAttribute("RequiredQty", availableQy);
              }

              // set required qty for clone.
              elePromiseLineClone.setAttribute("RequiredQty", requiredQty - availableQy);
              // blankout shipNode for cloned line.
              elePromiseLineClone.setAttribute("ShipNode", "");
              YFCElement eleShipNodes = elePromiseLineClone.getChildElement("ShipNodes");
              if (!YFCCommon.isVoid(eleShipNodes)) {
                eleShipNodes.getParentNode().removeChild(eleShipNodes);
              }
              // set line id for cloned line
              elePromiseLineClone.setAttribute("LineId", lineId + "_Split");
              // set excluded shipNode as pickup store for cloned node
              YFCElement excludedShipNodesEle = inDocClone.createElement("ExcludedShipNodes");
              YFCElement excludedShipNodeEle = inDocClone.createElement("ExcludedShipNode");
              excludedShipNodeEle.setAttribute("Node", sExtnPickStoreId);
              excludedShipNodeEle.setAttribute("SupressSourcing", "Y");
              excludedShipNodesEle.appendChild(excludedShipNodeEle);
              elePromiseLineClone.importNode(excludedShipNodesEle);

              promiseLinesEle.importNode(elePromiseLineClone);
            }
          } else {
            // if the reservation was successfull and all the required inventory is reserved.then
            // return the output
            return outDoc.getDocument();
          }
        }
        inDoc = inDocClone.getCopy();
      }


    }
    // GCPhase2:Splitting logic::Ends
    YFCDocument retDoc = GCCommonUtil.invokeAPI(env, GCConstants.RESERVE_AVAILABLE_INVENTORY, inDoc, templateDoc);

    // Stamping Bundle item if having tag component
    if (!hmLineIdAndPromiseLineInfo.isEmpty()) {
      retDoc = stampBundleTagItemPromiseLine(env, originalInDoc, retDoc, hmLineIdAndPromiseLineInfo);
    }

    LOGGER.endTimer("GCReserveinventoryWarpper.reserveInventory");
    return retDoc.getDocument();
  }

  /**
   * This method will create a Map of promiselines from reserveInventory Input and will maintain
   * LineId as Key for the Map.
   *
   * @param inDocClone
   */
  private Map<String, YFCElement> getReservationPromiseLineInputMap(YFCDocument inDocClone) {
    LOGGER.beginTimer("getReservationPromiseLineInputMap");
    Map<String, YFCElement> reserveInputPromiseLines = new HashMap<String, YFCElement>();
    YFCNodeList<YFCElement> promiseLineNodeList = inDocClone.getElementsByTagName("PromiseLine");
    for (YFCElement promiseLineEle : promiseLineNodeList) {
      String lineId = promiseLineEle.getAttribute("LineId");
      reserveInputPromiseLines.put(lineId, promiseLineEle);
    }
    LOGGER.endTimer("getReservationPromiseLineInputMap");
    return reserveInputPromiseLines;
  }

  /**
   * This method will create a Map which containsall promiseLines from reserveInventory output and
   * will maintain LineId as Key for the Map.
   *
   * @param reserveAvailableInvOutDoc
   * @return
   */
  Map<String, YFCElement> getPromiseLineMapFromReservationOutput(YFCDocument reserveAvailableInvOutDoc) {
    LOGGER.beginTimer("getPromiseLineMapFromReservationOutput");
    Map<String, YFCElement> reservationOutput = new HashMap<String, YFCElement>();
    // GCSTORE-1639::Begin
    YFCElement promiseLines = reserveAvailableInvOutDoc.getDocumentElement().getChildElement("PromiseLines");
    YFCNodeList<YFCElement> promiseLineNodeList = promiseLines.getElementsByTagName("PromiseLine");
    // GCSTORE-1639::End
    for (YFCElement promiseLine : promiseLineNodeList) {
      String lineIdOut = promiseLine.getAttribute("LineId");
      reservationOutput.put(lineIdOut, promiseLine);
    }
    LOGGER.endTimer("getPromiseLineMapFromReservationOutput");
    return reservationOutput;
  }



  /**
   *
   * This method will fetch reservation expiration minutes from commonCode and will stamp the same
   * before creating ATG order reservations.
   *
   * @param YFSEnvironment variable : Env
   * @return
   *
   */
  private static String getReservationExpirationDate(YFSEnvironment env) {

    LOGGER.beginTimer("GCReserveInventoryWrapper.getReservationExpirationDate");
    LOGGER.verbose("Class: GCReserveInventoryWrapper Method: getReservationExpirationDate START");
    String sReservationExpirationDate = "";
    Document commondCodeDoc = GCCommonUtil.getCommonCodeListByType(env, GCConstants.GC_RSRV_EXPIRATION, GCConstants.GC);
    if (commondCodeDoc.hasChildNodes()) {
      Element eleCommonCode = (Element) commondCodeDoc.getElementsByTagName(GCConstants.COMMON_CODE).item(0);
      String sExpirationMinutes = eleCommonCode.getAttribute(GCConstants.CODE_VALUE);

      Calendar calendarInstance = Calendar.getInstance();
      SimpleDateFormat dateFormat = new SimpleDateFormat(GCConstants.DATE_TIME_FORMAT);
      calendarInstance.add(Calendar.MINUTE, Integer.parseInt(sExpirationMinutes));
      sReservationExpirationDate = dateFormat.format(calendarInstance.getTime());

      LOGGER.verbose("Class: GCReserveInventoryWrapper Method: getReservationExpirationDate END");
      LOGGER.endTimer("GCReserveInventoryWrapper.getReservationExpirationDate");

    }
    return sReservationExpirationDate;
  }


  /**
   * This method removes all the ShipNode from ShipNodes element of PromiseLine for which Sourcing
   * is not allowed as per ExtnIsSourceFromStoreAllowed flag of the store
   *
   * @param env
   * @param promiseLineEle
   */
  private void filterOutNodesNotAllowedForSourcing(YFSEnvironment env, YFCElement promiseLineEle) {
    LOGGER.beginTimer("GCReserveInventoryWrapper.filterOutNodesNotAllowedForSourcing");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("promiseLineEle in  filterOutNodesNotAllowedForSourcing before modification"
          + promiseLineEle.toString());
    }
    YFCElement shipNodesEle = promiseLineEle.getChildElement(GCXmlLiterals.SHIP_NODES);
    if (YFCCommon.isVoid(shipNodesEle)) {
      return;
    }
    YFCNodeList<YFCElement> shipNodeList = shipNodesEle.getElementsByTagName(GCXmlLiterals.SHIP_NODE);
    List<String> listOfShipNodesInInput = new ArrayList<String>();
    for (YFCElement shipNodeEle : shipNodeList) {
      listOfShipNodesInInput.add(shipNodeEle.getAttribute(GCConstants.NODE));
    }
    List<String> storesNotAllowedForSourcing = GCOrderUtil.getStoreNotAllowedForSourcing(env, listOfShipNodesInInput);
    for (int i = 0; i < shipNodeList.getLength(); i++) {
      YFCElement shipNodeEle = shipNodeList.item(i);
      String shipNode = shipNodeEle.getAttribute(GCConstants.NODE);
      if (storesNotAllowedForSourcing.contains(shipNode)) {
        shipNodesEle.removeChild(shipNodeEle);
        i--;
      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("promiseLineEle in  filterOutNodesNotAllowedForSourcing after modification"
          + promiseLineEle.toString());
    }
    LOGGER.endTimer("GCReserveInventoryWrapper.filterOutNodesNotAllowedForSourcing");
  }

  /**
   * This method checks if the given promise Line has ShipNode stamped or not. If shipnode is not
   * stamped it returns true.
   *
   * @param promiseLineEle
   * @return
   */
  private boolean isShipNodeBlank(YFCElement promiseLineEle) {
    LOGGER.beginTimer("isShipNodeBlank");
    boolean blankShipNode = false;
    YFCElement shipNodesEle = promiseLineEle.getChildElement(GCXmlLiterals.SHIP_NODES);
    if (YFCCommon.isVoid(shipNodesEle)) {
      blankShipNode = true;
    } else {
      YFCElement shipNodeEle = shipNodesEle.getChildElement(GCConstants.SHIP_NODE);
      if (YFCCommon.isVoid(shipNodeEle)) {
        blankShipNode = true;
      } else {
        String shipNodeStr = shipNodeEle.getAttribute(GCConstants.NODE);
        if (YFCCommon.isVoid(shipNodeStr)) {
          blankShipNode = true;
        }
      }
    }
    LOGGER.verbose("Ship Node is Blank?" + blankShipNode);
    LOGGER.beginTimer("isShipNodeBlank");
    return blankShipNode;
  }

  /**
   *
   * This method will create a new updated input doc if there is a bundle with tag component. So,
   * this method will take the input doc and break the tag bundle into its components and will make
   * two different promise line for the same.
   *
   * @param YFSEnvironment variable : Env
   * @return inputDoc
   *
   */

  public YFCDocument splitInputForSerialBundles(YFSEnvironment env, YFCDocument inDoc,
      Map<String, ReserveInventoryPromiseLineInfo> hmLineIdAndPromiseLineInfo) {
    LOGGER.beginTimer("splitInputForSerialBundles");

    YFCDocument getItemListoutDoc;
    String sLineId;
    YFCElement elePromise = inDoc.getDocumentElement();
    YFCElement elePromiseLines = elePromise.getChildElement(GCConstants.PROMISE_LINES);
    YFCNodeList<YFCElement> nlPromiseLine = inDoc.getElementsByTagName(GCConstants.PROMISE_LINE);
    for (int i=0; i<nlPromiseLine.getLength(); i++) {
      YFCElement elePromiseLine = nlPromiseLine.item(i);
      sLineId = elePromiseLine.getAttribute(GCConstants.LINE_ID);
      ReserveInventoryPromiseLineInfo promiseLineInfo =
          hmLineIdAndPromiseLineInfo.get(elePromiseLine.getAttribute(GCConstants.LINE_ID));
      if (promiseLineInfo != null) {
        int lineIdEnd = 1;
        getItemListoutDoc = promiseLineInfo.getGetItemListOp();
        String sRequiredQty = elePromiseLine.getAttribute(GCConstants.REQUIRED_QTY);
        YFCElement eleTag = elePromiseLine.getChildElement(GCXmlLiterals.TAG);
        double reqQty = Double.parseDouble(sRequiredQty);
        YFCNodeList<YFCElement> n1Component = getItemListoutDoc.getElementsByTagName(GCConstants.COMPONENT);
        for (YFCElement eleComponent : n1Component) {
          String sComponentItemID = eleComponent.getAttribute(GCConstants.COMPONENT_ITEM_ID);
          String sComponentUOM = eleComponent.getAttribute(GCConstants.COMPONENT_UOM);
          String sKitQty = eleComponent.getAttribute(GCConstants.KIT_QUANTITY);
          double kitQty = Double.parseDouble(sKitQty);
          double reqrdQty = kitQty * reqQty;
          YFCDocument getItemTagOutTemplate = YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LIST_API_TMPL);
          YFCDocument getItemTagoutDoc =
              GCCommonUtil.getItemList(env, sComponentItemID, sComponentUOM, GCConstants.GCI, true,getItemTagOutTemplate);
          YFCElement eleTagItemList = getItemTagoutDoc.getDocumentElement();
          YFCElement eleTagItem = eleTagItemList.getChildElement(GCConstants.ITEM);
          YFCElement eleInvParameters = eleTagItem.getChildElement(GCConstants.INVENTORY_PARAMETERS);
          String sTagControlFlag = eleInvParameters.getAttribute(GCXmlLiterals.TAG_CONTROL_FLAG);
          YFCElement targetPromiseLineEle = elePromiseLines.createChild(GCConstants.PROMISE_LINE);
          GCXMLUtil.copyAttributes(elePromiseLine, targetPromiseLineEle);
          targetPromiseLineEle.setAttribute(GCConstants.ITEM_ID, sComponentItemID);
          targetPromiseLineEle.setAttribute(GCConstants.UOM, sComponentUOM);
          targetPromiseLineEle.setAttribute(GCConstants.PRODUCT_CLASS, GCOrderUtil.getProductClass(getItemListoutDoc));
          targetPromiseLineEle.setAttribute(GCConstants.REQUIRED_QTY, Double.toString(reqrdQty));
          targetPromiseLineEle.setAttribute(GCConstants.LINE_ID, sLineId + "_Split" + Integer.toString(lineIdEnd));
          lineIdEnd++;

          YFCElement reservationParametersEle = targetPromiseLineEle.createChild(GCConstants.RESERVATION_PARAMETERS);
          GCXMLUtil.copyAttributes(elePromiseLine.getChildElement(GCConstants.RESERVATION_PARAMETERS),
              reservationParametersEle);

          YFCElement shipNodesEle = targetPromiseLineEle.createChild(GCConstants.SHIP_NODES);
          YFCElement eleShipNodes = elePromiseLine.getChildElement(GCConstants.SHIP_NODES);
          if (!YFCCommon.isVoid(eleShipNodes)) {
            YFCNodeList<YFCElement> n1ShipNodeEle = eleShipNodes.getElementsByTagName(GCConstants.SHIP_NODE);
            int len = n1ShipNodeEle.getLength();
            if (len > 1) {
              YFSException ex = new YFSException();
              ex.setErrorDescription("Bundle Serial can't be send with more than one ShipNode");
              throw ex;
            }
            for (YFCElement eleShipNode : n1ShipNodeEle) {
              YFCElement shipNodeEle = shipNodesEle.createChild(GCConstants.SHIP_NODE);
              GCXMLUtil.copyAttributes(eleShipNode, shipNodeEle);
            }
          }
          YFCElement extnEle = targetPromiseLineEle.createChild(GCConstants.EXTN);
          GCXMLUtil.copyAttributes(elePromiseLine.getChildElement(GCConstants.EXTN), extnEle);
          if (YFCCommon.equals(sTagControlFlag, GCConstants.SOMETIMES_TAG_CONTROLLED, false)
              && !(YFCCommon.isVoid(eleTag))) {
            YFCElement tagEle = targetPromiseLineEle.createChild(GCXmlLiterals.TAG);
            GCXMLUtil.copyAttributes(elePromiseLine.getChildElement(GCXmlLiterals.TAG), tagEle);
          }
          YFCElement shipAddressEle = targetPromiseLineEle.createChild(GCConstants.SHIP_TO_ADDRESS);
          GCXMLUtil.copyAttributes(elePromiseLine.getChildElement(GCConstants.SHIP_TO_ADDRESS), shipAddressEle);
        }
        elePromiseLine.getParentNode().removeChild(elePromiseLine);
        i--;
      }
    }
    LOGGER.endTimer("splitInputForSerialBundles");
    return inDoc;
  }

  /**
   * This method checks if the given inDoc requires UseUnplannedInventory="N"
   *
   * @param inDoc
   * @return
   */

  public boolean getUseUnplannedInventoryUpdated(YFSEnvironment env, YFCDocument inDoc) {
    boolean sUnplannedInventoryFlag = false;
    int counter = 0;
    YFCNodeList<YFCElement> promiseLineNodeList = inDoc.getElementsByTagName(GCConstants.PROMISE_LINE);
    for (YFCElement promiseLineEle : promiseLineNodeList) {
      String sLineShipNode = promiseLineEle.getAttribute(GCConstants.SHIP_NODE);
      if (!YFCCommon.isStringVoid(sLineShipNode)) {
        counter++;
      } else {
        YFCElement eleShipNodes = promiseLineEle.getChildElement(GCConstants.SHIP_NODES);
        if (!YFCCommon.isVoid(eleShipNodes)) {
          YFCElement eleShipNode = eleShipNodes.getChildElement(GCConstants.SHIP_NODE);
          if (!YFCCommon.isVoid(eleShipNode)) {
            String sShipNode = eleShipNode.getAttribute(GCConstants.SHIP_NODE);
            if (!YFCCommon.isStringVoid(sShipNode)) {
              counter++;
            }
          }
        }
      }
    }
    if (promiseLineNodeList.getLength() == counter) {
      sUnplannedInventoryFlag = true;
    }
    return sUnplannedInventoryFlag;
  }

  /**
   * This method will stamp the bundle item with tag in the output of reserveInventory
   *
   * @param inDoc,outDoc
   * @return outDoc
   */
  public YFCDocument stampBundleTagItemPromiseLine(YFSEnvironment env, YFCDocument inDoc, YFCDocument outDoc,
      Map<String, ReserveInventoryPromiseLineInfo> hmLineIdAndPromiseLineInfo) {
    LOGGER.beginTimer("stampBundleTagItemPromiseLine");

    String sBundleItemID = null;
    String sLineId;
    YFCDocument getItemListoutDoc;
    int lineIdEnd = 1;

    YFCNodeList<YFCElement> promiseLineNodeList = inDoc.getElementsByTagName(GCConstants.PROMISE_LINE);
    for (YFCElement elePromiseLine : promiseLineNodeList) {
      sLineId = elePromiseLine.getAttribute(GCConstants.LINE_ID);
      ReserveInventoryPromiseLineInfo promiseLineInfo =
          hmLineIdAndPromiseLineInfo.get(elePromiseLine.getAttribute(GCConstants.LINE_ID));
      if (promiseLineInfo != null) {
        String sItemID = elePromiseLine.getAttribute(GCConstants.ITEM_ID);
        getItemListoutDoc = promiseLineInfo.getGetItemListOp();
        sBundleItemID = sItemID;
        YFCNodeList<YFCElement> n1Component = getItemListoutDoc.getElementsByTagName(GCConstants.COMPONENT);
        for (YFCElement eleComponent : n1Component) {
          String sComponentItemID = eleComponent.getAttribute(GCConstants.COMPONENT_ITEM_ID);
          String sComponentUOM = eleComponent.getAttribute(GCConstants.COMPONENT_UOM);
          YFCDocument getItemTagOutTemplate = YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LIST_API_TMPL);
          YFCDocument getItemTagoutDoc =
              GCCommonUtil.getItemList(env, sComponentItemID, sComponentUOM, GCConstants.GCI, true, getItemTagOutTemplate);
          YFCElement eleTagItemList = getItemTagoutDoc.getDocumentElement();
          YFCElement eleTagItem = eleTagItemList.getChildElement(GCConstants.ITEM);
          YFCElement eleInvParameters = eleTagItem.getChildElement(GCConstants.INVENTORY_PARAMETERS);
          String sTagControlFlag = eleInvParameters.getAttribute(GCXmlLiterals.TAG_CONTROL_FLAG);
          if (YFCCommon.equals(sTagControlFlag, GCConstants.SOMETIMES_TAG_CONTROLLED, false)) {
            lineIdEnd = copyTagComponentAttributes(outDoc, sBundleItemID, sLineId, lineIdEnd, sComponentItemID);
          }
        }
      }
    }
    LOGGER.endTimer("stampBundleTagItemPromiseLine");
    return outDoc;
  }

  /**
   *
   * @param outDoc
   * @param sBundleItemID
   * @param sLineId
   * @param lineIdEnd
   * @param sComponentItemID
   * @return
   */
  private int copyTagComponentAttributes(YFCDocument outDoc, String sBundleItemID, String sLineId, int lineIdEnd,
      String sComponentItemID) {

    //Refactored code for Sonar Violation fixes.
    String sTagItemID;
    sTagItemID = sComponentItemID;
    YFCElement eleOutPromiseHeader = outDoc.getDocumentElement();
    YFCElement eleOutPromiseLines = eleOutPromiseHeader.getChildElement(GCConstants.PROMISE_LINES);
    YFCNodeList<YFCElement> outPromiseLineNodeList = outDoc.getElementsByTagName(GCConstants.PROMISE_LINE);
    for (YFCElement eleOutPromiseLine : outPromiseLineNodeList) {
      String sOutItemID = eleOutPromiseLine.getAttribute(GCConstants.ITEM_ID);
      if (YFCCommon.equals(sOutItemID, sTagItemID, false)) {
        YFCElement eleOutBundlePromiseLine = eleOutPromiseLines.createChild(GCConstants.PROMISE_LINE);
        GCXMLUtil.copyAttributes(eleOutPromiseLine, eleOutBundlePromiseLine);
        eleOutBundlePromiseLine.setAttribute(GCConstants.ITEM_ID, sBundleItemID);
        eleOutBundlePromiseLine.setAttribute(GCConstants.IS_BUNDLE_PARENT, GCConstants.YES);
        eleOutBundlePromiseLine.setAttribute(GCConstants.LINE_ID, sLineId + "_Bun" + Integer.toString(lineIdEnd));
        lineIdEnd++;
        YFCElement eleOutBundleReservations = eleOutBundlePromiseLine.createChild(GCXmlLiterals.RESERVATIONS);
        YFCElement eleOutReservations = eleOutPromiseLine.getChildElement(GCXmlLiterals.RESERVATIONS);
        //GCSTORE-2215::Begin-adding null check
        if(!YFCCommon.isVoid(eleOutReservations)){
        GCXMLUtil.copyAttributes(eleOutReservations, eleOutBundleReservations);
        YFCElement eleOutBundleReservation = eleOutBundleReservations.createChild(GCXmlLiterals.RESERVATION);
        YFCElement eleOutReservation = eleOutReservations.getChildElement(GCXmlLiterals.RESERVATION);
        if (!YFCCommon.isVoid(eleOutReservation)) {
          GCXMLUtil.copyAttributes(eleOutReservation, eleOutBundleReservation);
          eleOutBundleReservation.setAttribute(GCConstants.ITEM_ID, sBundleItemID);
        }
      }
      break;
      }
    }
    return lineIdEnd;
  }
}


/**
 * This class is written to fetch the ItemList output for each item in the PromiseLine.
 */

class ReserveInventoryPromiseLineInfo {
  YFCDocument getItemListOp;

  public YFCDocument getGetItemListOp() {
    return getItemListOp;
  }

  public void setGetItemListOp(YFCDocument getItemListOp) {
    this.getItemListOp = getItemListOp;
  }
}