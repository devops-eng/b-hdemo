package com.gc.api;

import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCResetDraftOrderReservation implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCResetDraftOrderReservation.class);

  public Document resetReservation(YFSEnvironment env, Document inputDoc) {

    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input XML to resetReservation method::" + inDoc.toString());
    }
    YFCElement orderEle = inDoc.getDocumentElement().getChildElement(GCConstants.ORDER);
    String orderHeaderKey = orderEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
    YFCDocument getOrderListInDoc = YFCDocument.createDocument(GCConstants.ORDER);
    getOrderListInDoc.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
    YFCDocument getOrderListOutputTmpl =
        YFCDocument
        .getDocumentFor("<OrderList><Order OrderHeaderKey='' DraftOrderFlag='' ><OrderLines><OrderLine OrderLineKey='' ><OrderLineReservations><OrderLineReservation/></OrderLineReservations></OrderLine></OrderLines></Order></OrderList>");

    YFCDocument getOrdrListOutputDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListInDoc, getOrderListOutputTmpl);

    YFCElement eleOrder = getOrdrListOutputDoc.getDocumentElement().getChildElement(GCConstants.ORDER);

    boolean isReservationPresent = isDraftOrderReserved(eleOrder);

    LOGGER.verbose("isReservationPresent??" + isReservationPresent);
    if (isReservationPresent) {
      resetOrderLineReservations(env, eleOrder);
    }
    return inputDoc;
  }

  private boolean isDraftOrderReserved(YFCElement eleOrder) {
    YFCNodeList<YFCElement> orderLineNL = eleOrder.getElementsByTagName(GCConstants.ORDER_LINE);
    for(YFCElement orderLineEle : orderLineNL){
      YFCElement eleOrderLineReservations = orderLineEle.getChildElement(GCConstants.ORDER_LINE_RESERVATIONS);
      if(eleOrderLineReservations.hasChildNodes()){
        return true;
      }
    }
    return false;
  }

  private void resetOrderLineReservations(YFSEnvironment env, YFCElement eleOrder) {
    LOGGER.beginTimer("resetOrderLineReservations");
    YFCDocument changeOrdrInputDoc = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement changeOrdrRoot = changeOrdrInputDoc.getDocumentElement();
    changeOrdrRoot.setAttribute(GCConstants.ORDER_HEADER_KEY, eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY));
    changeOrdrRoot.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
    YFCElement orderDates = changeOrdrRoot.createChild(GCConstants.ORDER_DATES);
    YFCElement orderDate = orderDates.createChild(GCConstants.ORDER_DATE);
    orderDate.setAttribute(GCConstants.DATE_TYPE_ID, GCConstants.GC_ORDER_RESERVATION_DATE);
    orderDate.setAttribute(GCConstants.ACTUAL_DATE, "2500-01-01");
    YFCElement orderLines = changeOrdrRoot.createChild(GCConstants.ORDER_LINES);
    boolean changeOrderCallRequired = false;
    YFCNodeList<YFCElement> orderLineNodeList = eleOrder.getElementsByTagName(GCConstants.ORDER_LINE);
    for (YFCElement orderLine : orderLineNodeList) {
      String lineKey = orderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      YFCElement eleOrderLine = orderLines.createChild(GCConstants.ORDER_LINE);
      eleOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, lineKey);
      YFCElement orderLineReservations = eleOrderLine.createChild(GCConstants.ORDER_LINE_RESERVATIONS);
      orderLineReservations.setAttribute(GCConstants.RESET, GCConstants.YES);
      changeOrderCallRequired = true;
    }
    if (changeOrderCallRequired) {
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("ChangeOrder input to reset OrderLine Reservations::" + changeOrdrInputDoc.toString());
      }
      YFCDocument changeOrdTemp = YFCDocument.getDocumentFor("<Order OrderHeaderKey='' />");
      GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, changeOrdrInputDoc, changeOrdTemp);
    }
    LOGGER.endTimer("resetOrderLineReservations");
  }



  @Override
  public void setProperties(Properties arg0) {


  }

}
