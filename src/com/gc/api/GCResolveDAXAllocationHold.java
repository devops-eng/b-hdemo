/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Processes the response from DAX
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               10/03/2014        Singh, Gurpreet            OMS-1473: DAX Processing
             1.1               29/05/2014        Soni, Karan                Made changes as per new design
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.util.Properties;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:gurpreet.singh">Gurpreet</a>
 */
public class GCResolveDAXAllocationHold implements YIFCustomApi {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCResolveDAXAllocationHold.class);

    /**
     * 
     * Description of resolveDAXAllocationHold Contains the logic for resolving
     * GC DAX Allocation Hold applied on the order lines
     * 
     * @param env
     * @param inDoc
     * @return
     * @throws GCException
     * 
     */
    public Document resolveDAXAllocationHold(YFSEnvironment env, Document inDoc)
	    throws GCException {
	LOGGER.verbose(" Entering GCResolveDAXAllocationHold.resolveDAXAllocationHold() method ");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" resolveDAXAllocationHold() method, Input document is"
		    + GCXMLUtil.getXMLString(inDoc));
	}
	try {
	    Element eleRootOfInDoc = inDoc.getDocumentElement();
	    String sOrderNo = eleRootOfInDoc.getAttribute(GCConstants.ORDER_NO);
	    String sDocumentType = eleRootOfInDoc
		    .getAttribute(GCConstants.DOCUMENT_TYPE);
	    String sEnterpriseCode = eleRootOfInDoc
		    .getAttribute(GCConstants.ENTERPRISE_CODE);

	    Document docGetOrderListIP = GCXMLUtil
		    .createDocument(GCConstants.ORDER);
	    Element eleGetOrderListIP = docGetOrderListIP.getDocumentElement();
	    eleGetOrderListIP.setAttribute(GCConstants.ORDER_NO, sOrderNo);
	    eleGetOrderListIP.setAttribute(GCConstants.DOCUMENT_TYPE,
		    sDocumentType);
	    eleGetOrderListIP.setAttribute(GCConstants.ENTERPRISE_CODE,
		    sEnterpriseCode);

	    Document docGetOrderListTemplate = GCXMLUtil
		    .getDocument(GCConstants.GET_ORDER_LIST_TEMPLATE);

	    Document docGetOrderListOP = GCCommonUtil.invokeAPI(env,
		    GCConstants.API_GET_ORDER_LIST, docGetOrderListIP,
		    docGetOrderListTemplate);

	    Document docChangeStatusIP = GCXMLUtil
		    .createDocument(GCConstants.ORDER_STATUS_CHANGE);
	    Element eleChangeOrder = docChangeStatusIP.getDocumentElement();
	    eleChangeOrder.setAttribute(GCConstants.ORDER_NO, sOrderNo);
	    eleChangeOrder.setAttribute(GCConstants.DOCUMENT_TYPE,
		    sDocumentType);
	    eleChangeOrder.setAttribute(GCConstants.ENTERPRISE_CODE,
		    sEnterpriseCode);
	    eleChangeOrder.setAttribute(
		    GCConstants.IGNORE_TRANSACTION_DEPENDENCIES,
		    GCConstants.YES);
	    eleChangeOrder.setAttribute(GCConstants.TRANSACTION_ID,
		    "SCHEDULE.0001");
	    Element eleOrderLinesChangeOrderStatus = GCXMLUtil.createElement(
		    docChangeStatusIP, GCConstants.ORDER_LINES, null);
	    eleChangeOrder.appendChild(eleOrderLinesChangeOrderStatus);

	    NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc,
		    "Order/OrderLines/OrderLine");
	    for (int iCounter = 0; iCounter < nlOrderLine.getLength(); iCounter++) {
		Element eleOrderLine = (Element) nlOrderLine.item(iCounter);
		String sQuantityDax = eleOrderLine
			.getAttribute(GCConstants.QUANTITY);
		String sPrimeLineNo = eleOrderLine
			.getAttribute(GCConstants.PRIME_LINE_NO);
		String sSubLineNo = eleOrderLine
			.getAttribute(GCConstants.SUB_LINE_NO);
		double dQuantityDax = (!YFCCommon.isVoid(sQuantityDax)) ? Double
			.valueOf(sQuantityDax) : 0.00;

		// The method will return the valid SubLineNo for which the new
		// split line will be created

		Element eleOrderLineGetOrderList = (Element) XPathAPI
			.selectSingleNode(docGetOrderListOP,
				"/OrderList/Order/OrderLines/"
					+ "OrderLine[@PrimeLineNo='"
					+ sPrimeLineNo + "' and @SubLineNo='"
					+ sSubLineNo + "']");
		double dOrderedQty = 0.00;
		if (!YFCCommon.isVoid(eleOrderLineGetOrderList)) {
		    String sOrderedQty = eleOrderLineGetOrderList
			    .getAttribute(GCConstants.ORDERED_QTY);
		    dOrderedQty = (!YFCCommon.isVoid(sOrderedQty)) ? Double
			    .valueOf(sOrderedQty) : 0.00;
		}

		if (dQuantityDax <= dOrderedQty) {
		    Element eleOrderLineOrderStatus = GCXMLUtil.createElement(
			    docChangeStatusIP, GCConstants.ORDER_LINE, null);
		    eleOrderLineOrderStatus.setAttribute(
			    GCConstants.PRIME_LINE_NO, sPrimeLineNo);
		    eleOrderLineOrderStatus.setAttribute(
			    GCConstants.SUB_LINE_NO, sSubLineNo);
		    eleOrderLineOrderStatus.setAttribute(GCConstants.QUANTITY,
			    sQuantityDax);
		    eleOrderLineOrderStatus.setAttribute(
			    GCConstants.BASE_DROP_STATUS,
			    GCConstants.STATUS_SCHEDULED);
		    eleOrderLinesChangeOrderStatus
			    .appendChild(eleOrderLineOrderStatus);
		}
	    }
	    NodeList nlChangeOrderLine = XPathAPI.selectNodeList(
		    docChangeStatusIP, "Order/OrderLines/OrderLine");
	    if (nlChangeOrderLine.getLength() > 0) {
		GCCommonUtil.invokeAPI(env,
			GCConstants.API_CHANGE_ORDER_STATUS, docChangeStatusIP);
	    }
	} catch (Exception ex) {
	    LOGGER.error(
		    " Inside catch of GCResolveDAXAllocationHold.resolveDAXAllocationHold()",
		    ex);
	    throw new GCException(ex);
	}
	LOGGER.debug(" Exiting GCResolveDAXAllocationHold.resolveDAXAllocationHold() method ");
	return inDoc;
    }

    public void setProperties(Properties arg0) throws GCException {

    }
}
