package com.gc.api;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCResolveTaxExemptHold {

	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCResolveTaxExemptHold.class.getName());

	public void resolveTaxExemptHold(YFSEnvironment env, Document inDoc)
			throws GCException {
		try {
			final String logInfo = "GCResolveTaxExemptHold :: resolveTaxExemptHold :: ";
			LOGGER.debug(logInfo + "Begin");
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(logInfo + "Input Document"
						+ GCXMLUtil.getXMLString(inDoc));
			}
			Element eleInRoot = inDoc.getDocumentElement();
			String eleCustomerID = eleInRoot
					.getAttribute(GCConstants.CUSTOMER_ID);
			// prepared input doc for get order list api
			Document docGetOrderListIp = GCXMLUtil
					.createDocument(GCConstants.ORDER);
			Element eleGetOrderListIpRoot = docGetOrderListIp
					.getDocumentElement();
			eleGetOrderListIpRoot.setAttribute(GCConstants.BILL_TO_ID,
					eleCustomerID);
			Element eleOrderOnHold = docGetOrderListIp
					.createElement(GCConstants.ORDER_ON_HOLD);
			eleOrderOnHold.setAttribute(GCConstants.HOLD_TYPE,
					GCConstants.TAX_EXEMPT_HOLD);
			eleOrderOnHold.setAttribute(GCConstants.STATUS,
					GCConstants.APPLY_HOLD_CODE);
			eleGetOrderListIpRoot.appendChild(eleOrderOnHold);
			// prepared template doc for get order list api
			Document docGetOrderListTemp = GCXMLUtil
					.createDocument(GCConstants.ORDER_LIST);
			Element eleGetOrderListTempRoot = docGetOrderListTemp
					.getDocumentElement();
			Element eleGetOrderListTempOrder = docGetOrderListTemp
					.createElement(GCConstants.ORDER);
			// GCStore-328 Start
			Element eleGetOrderListTempPersonInfoShipTo = docGetOrderListTemp
					.createElement(GCXmlLiterals.PERSON_INFO_SHIP_TO);

			eleGetOrderListTempRoot.appendChild(eleGetOrderListTempOrder);
			eleGetOrderListTempOrder
					.appendChild(eleGetOrderListTempPersonInfoShipTo);
			// GCStore-328 End
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(logInfo + "docGetOrderListIp :: \n"
						+ GCXMLUtil.getXMLString(docGetOrderListIp));
			}
			Document docGetOrderListOp = GCCommonUtil.invokeAPI(env,
					GCConstants.GET_ORDER_LIST, docGetOrderListIp,
					docGetOrderListTemp);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(logInfo + "docGetOrderListOp :: \n"
						+ GCXMLUtil.getXMLString(docGetOrderListOp));
			}
			NodeList nlOrder = XPathAPI.selectNodeList(docGetOrderListOp,
					"//Order");
			for (int iOrderCounter = 0; iOrderCounter < nlOrder.getLength(); iOrderCounter++) {

				Element eleOrder = (Element) nlOrder.item(iOrderCounter);
				String sOrderHeaderKey = eleOrder
						.getAttribute(GCConstants.ORDER_HEADER_KEY);
				Document docChangeOrderIp = GCXMLUtil
						.createDocument(GCConstants.ORDER);
				Element eleChangeOrderRoot = docChangeOrderIp
						.getDocumentElement();
				eleChangeOrderRoot.setAttribute(GCConstants.ORDER_HEADER_KEY,
						sOrderHeaderKey);
				// GCSTORE-328 Start
				YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
				YFCElement eleCustomerExtn = yfcInDoc.getDocumentElement()
						.getChildElement(GCXmlLiterals.EXTN, true);
				String sCustomerState = eleCustomerExtn
						.getAttribute(GCXmlLiterals.EXTN_TAX_EXEMPT_STATE);
				
					Element elePersonInfoShipToOrder = (Element) XPathAPI
							.selectSingleNode(eleOrder,
									GCXmlLiterals.PERSON_INFO_SHIP_TO);
					String sState = elePersonInfoShipToOrder
							.getAttribute(GCXmlLiterals.STATE);
					if (YFCCommon.equals(sCustomerState, sState)) {
						// GCSTORE-328 End
				eleChangeOrderRoot.setAttribute(GCConstants.TAX_EXEMPT_FLAG,
						GCConstants.YES);
					} else {
					// GCSTORE-328 Start(2)
					eleChangeOrderRoot.setAttribute(GCConstants.TAX_EXEMPT_FLAG,
							GCConstants.NO);
				}
					// GCSTORE-328 End(2)
				Element eleOrderHoldTypes = docChangeOrderIp
						.createElement(GCConstants.ORDER_HOLD_TYPES);
				eleChangeOrderRoot.appendChild(eleOrderHoldTypes);

				Element eleOrderHoldType = docChangeOrderIp
						.createElement(GCConstants.ORDER_HOLD_TYPE);
				eleOrderHoldTypes.appendChild(eleOrderHoldType);
				eleOrderHoldType.setAttribute(GCConstants.HOLD_TYPE,
						GCConstants.TAX_EXEMPT_HOLD);
				eleOrderHoldType.setAttribute(GCConstants.STATUS,
						GCConstants.RESOLVE_HOLD_CODE);
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug(logInfo + "docChangeOrderIp ::\n"
							+ GCXMLUtil.getXMLString(docChangeOrderIp));
				}
				GCCommonUtil.invokeService(env,
						GCConstants.GC_TO_Q_TO_RESOLVE_TAX_EXEMPT_HOLD_SERVICE,
						docChangeOrderIp);
			

			}
		} catch (Exception e) {
			LOGGER.error(
					" Inside catch block of GCResolveTaxExemptHold.resolveTaxExemptHold(), Exception is :",
					e);
			throw new GCException(e);
		}
	}
}
