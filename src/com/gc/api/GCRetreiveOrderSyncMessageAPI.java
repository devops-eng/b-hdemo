/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               06/02/2014        Last Name, First Name		JIRA No: Description of issue.
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.util.List;
import java.util.Properties;

import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:email Address">Name</a>
 */
public class GCRetreiveOrderSyncMessageAPI implements YIFCustomApi {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCRetreiveOrderSyncMessageAPI.class.getName());


  /**
   * This method sync all eligible order amendments in OMS with DAX.
   *
   * @param env
   * @param inDoc
   * @throws Exception
   */

  public Document retreiveOrderSyncMessage(YFSEnvironment env, Document inDoc) throws GCException {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" Entering GCRetreiveOrderSyncMessageAPI.retreiveOrderSyncMessage() method ");
      LOGGER.debug(" retreiveOrderSyncMessage() method, Input document is" + GCXMLUtil.getXMLString(inDoc));
    }
    try {
      // Call method isEligibleForOrderSync
			if (isEligibleForOrderSync(inDoc, env)) {

        LOGGER.debug("Order is Eligible for Sync");

        String sOrderHeaderKey = inDoc.getDocumentElement().getAttribute(GCConstants.ORDER_HEADER_KEY);

        Document getOrderListOutDoc =
            GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST,
                GCXMLUtil.getDocument("<Order OrderHeaderKey='" + sOrderHeaderKey + "'/>"),
                GCCommonUtil.getDocumentFromPath("/global/template/api/OMS_DAX_OrderSync_TemplateXML.xml"));
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug(GCXMLUtil.getXMLString(getOrderListOutDoc));
        }
        return getOrderListOutDoc;
      }

      LOGGER.debug("Order is not Eligible for Sync");
      return GCXMLUtil.getDocument("<Order OrderHeaderKey=''/>");
    } catch (Exception e) {
      LOGGER.error("Inside Catch block of GCRetreiveOrderSyncMessageAPI.retreiveOrderSyncMessage() Exception e: ", e);
      throw new GCException(e);
    }
  }

  	/**
	 * This method checks if the order amendment done in OMS is eligible for
	 * sync to DAX.
	 * 
	 * @param inDoc
	 * @param env
	 * @throws Exception
	 */

	private boolean isEligibleForOrderSync(Document inDoc, YFSEnvironment env)
			throws GCException {

    try {
      LOGGER.debug(" Entering GCRetreiveOrderSyncMessageAPI.isEligibleForOrderSync() method ");

      String sMaxOrderStatus = GCXMLUtil.getAttributeFromXPath(inDoc, "/Order/@MaxOrderStatus");

      LOGGER.debug("MaxOrderStatus is : " + sMaxOrderStatus);

      if (!YFCCommon.isVoid(sMaxOrderStatus)) {

        int statusValue = sMaxOrderStatus.compareTo("1200");

        if (statusValue < 0 || statusValue == 0
            || GCConstants.CANCELLED_ORDER_STATUS_CODE.equalsIgnoreCase(sMaxOrderStatus)) {

          LOGGER.debug("MaxOrderStatus is less than 1200 or equal to 9000");

					return checkModificationForOrderBeforeSchedule(inDoc, env);

        } else if (statusValue > 0) {

          LOGGER.verbose("MaxOrderStatus is greater than 1200");
          return checkModificationForOrderAfterSchedule(inDoc);

        }
      }
    } catch (Exception e) {
      LOGGER.error("Inside GCRetreiveOrderSyncMessageAPI.isEligibleForOrderSync():", e);
      throw new GCException(e);
    }
    return false;

  }


	private boolean checkModificationForOrderBeforeSchedule(Document inDoc,
			YFSEnvironment env) throws TransformerException {

    Element eleModficationTypeCancel =
        GCXMLUtil.getElementByXPath(inDoc, GCConstants.ORDER_AUDIT_MODIFICATION_TYPE_XPATH
            + "[@Name='CANCEL' and @ScreenName='Cancel']");

    if (!YFCCommon.isVoid(eleModficationTypeCancel)) {
			LOGGER.debug("MaxOrderStatus less than 1200 and Modification type is Cancel");
			env.setTxnObject(GCConstants.CANCELLATION_REASON_CODE, GCXMLUtil
					.getAttributeFromXPath(inDoc,
							"Order/OrderAudit/@ReasonCode"));
      return true;
    } else {
      Element elePrimeLineNoAttribute = GCXMLUtil
          .getElementByXPath(inDoc,
              GCConstants.ORDER_AUDIT_ATTRIBUTE_XPATH
              + "[@Name='PrimeLineNo']");
      if (!YFCCommon.isVoid(elePrimeLineNoAttribute)) {

        LOGGER.debug("Status less tha 1200 and Attribute Name is PrimeLine");
        String sNewValue = elePrimeLineNoAttribute.getAttribute(GCConstants.NEW_VALUE);
        String sOldValue = elePrimeLineNoAttribute.getAttribute(GCConstants.OLD_VALUE);

        if (!sNewValue.equalsIgnoreCase(sOldValue)) {
          return true;
        }
      }
    }
    return false;
  }

  private boolean checkModificationForOrderAfterSchedule(Document inDoc) throws TransformerException {

    List<Element> listModificationType =
        GCXMLUtil.getElementListByXpath(inDoc, GCConstants.ORDER_AUDIT_MODIFICATION_TYPE_XPATH);

    for (Element eleModificationType : listModificationType) {

      String sModifcationName = eleModificationType.getAttribute(GCConstants.MODIFICATION_TYPE_NAME);
      String sModifcationScreenName = eleModificationType.getAttribute(GCConstants.MODIFICATION_TYPE_SCREEN_NAME);

      LOGGER.debug(sModifcationName);
      LOGGER.debug(sModifcationScreenName);

      Boolean isModificationDone =
          (GCConstants.ACTION_CANCEL.equalsIgnoreCase(sModifcationName) && GCConstants.ACTION_CANCEL
              .equalsIgnoreCase(sModifcationScreenName))
              || (GCConstants.MODIFICATION_SHIPTO.equalsIgnoreCase(sModifcationName) && GCConstants.MODIFICATION_SCREEN_NAME_SHIPTO
                  .equalsIgnoreCase(sModifcationScreenName))
                  || (GCConstants.MODIFICATION_PAYMENT_METHOD.equalsIgnoreCase(sModifcationName) && "Change Payment Method"
                      .equalsIgnoreCase(sModifcationScreenName))
                      || (GCConstants.MODIFICATION_CARRIER_SERVICE_CODE.equalsIgnoreCase(sModifcationName) && GCConstants.MODIFICATION_SCREEN_NAME_CARRIER_SERVICE_CODE
                          .equalsIgnoreCase(sModifcationScreenName))
                          || (GCConstants.MODIFICATION_PRICE.equalsIgnoreCase(sModifcationName) && GCConstants.MODIFICATION_SCREEN_NAME_PRICE
                              .equalsIgnoreCase(sModifcationScreenName));

      if (isModificationDone) {
        return true;
      }
    }

    List<Element> listChangedAttribute =
        GCXMLUtil.getElementListByXpath(inDoc, GCConstants.ORDER_AUDIT_ATTRIBUTE_XPATH);

    for (Element eleChangedAttribute : listChangedAttribute) {

      String sAttributeName = eleChangedAttribute.getAttribute(GCConstants.ATTRIBUTE_NAME);
      String sNewValue = eleChangedAttribute.getAttribute(GCConstants.NEW_VALUE);
      String sOldValue = eleChangedAttribute.getAttribute(GCConstants.OLD_VALUE);

      LOGGER.debug(sAttributeName);
      LOGGER.debug(sNewValue);
      LOGGER.debug(sOldValue);

      Boolean isAttributeChanged =
          (GCConstants.PRIME_LINE_NO.equalsIgnoreCase(sAttributeName) && (!sNewValue.equalsIgnoreCase(sOldValue)))
          || (GCConstants.FULFILLMENT_TYPE.equalsIgnoreCase(sAttributeName) && (!sNewValue
              .equalsIgnoreCase(sOldValue)))
              || (GCConstants.REASON_CODE.equalsIgnoreCase(sAttributeName) && (sNewValue
                  .equalsIgnoreCase(GCConstants.YCD_CUSTOMER_APPEASE) || sNewValue
                  .equalsIgnoreCase(GCConstants.YCD_ADD_MODIFY_CHARGES)));

      if (isAttributeChanged) {
        return true;
      }
    }
    return false;
  }


  /**
   * Description of setProperties
   *
   * @param arg0
   * @throws Exception
   * @see com.yantra.interop.japi.YIFCustomApi#setProperties(java.util.Properties)
   */
  @Override
  public void setProperties(Properties arg0) throws GCException {


  }

}
