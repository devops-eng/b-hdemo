package com.gc.api;

import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCShipmentConfirmationAddNoteWrapperAPI implements YIFCustomApi{

	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCShipmentConfirmationAddNoteWrapperAPI.class);
	String changeOrderDelayInMS = "";

	/**
	 *
	 * Description of callChangeOrder This method calls ChangeOrderAPI for adding notes to the order.
	 * For making change order input Xml it uses inDoc.
	 * @param env
	 * @param inDoc
	 *
	 */
	public void callChangeOrder(YFSEnvironment env, Document inDoc) {
		LOGGER.beginTimer("Entering GCShipmentConfirmationAddNoteWrapperAPI.callChangeOrder()");
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Incoming XML" + GCXMLUtil.getXMLString(inDoc));
		}
		YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
		if (YFCCommon.isVoid(changeOrderDelayInMS)) {
			changeOrderDelayInMS = "10";
		}
		int changeOrderDelay = Integer.parseInt(changeOrderDelayInMS);
		LOGGER.verbose("changeOrderDelay" + changeOrderDelay);
		try {
			Thread.sleep(changeOrderDelay);
		} catch (InterruptedException ex) {
			LOGGER.error("Found error in GCShipmentConfirmationAddNoteWrapperAPI.callChangeOrder()", ex);
		}
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Calling change order with input doc=" + changeOrderDelay);
		}
		GCCommonUtil.invokeAPI(env, "changeOrder", yfcInDoc,
				YFCDocument.createDocument("Order"));
		LOGGER.verbose(" Calling changeOrder Successfull");
		LOGGER.endTimer("Exiting GCShipmentConfirmationAddNoteWrapperAPI.callChangeOrder()");
	}
	@Override
	public void setProperties(Properties prop) {
		changeOrderDelayInMS = prop.getProperty("ChangeOrderDelayInMS");

	}
}
