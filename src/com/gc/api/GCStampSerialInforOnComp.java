/**
 * Copyright © 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: Foundation Class
 *#################################################################################################################################################################
 *          Version       CR                Date              Modified By                Description
 *#################################################################################################################################################################
             1.0     Initial Version     02/03/2015         Shinde,Abhishek              GC Store -160 Stamp Bundle Parent Information of Serial Itemson Component
			 
#################################################################################################################################################################
 */

package com.gc.api;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/*
 *  GC Store 160, 159 This Class is used to stamp serial No on Components after 
 *  Change Order on Add item page
 */
public class GCStampSerialInforOnComp {

	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCStampSerialInforOnComp.class.getName());

	/*
	 * This method stamps Serial No and ship node of bundle parent on components
	 */
	public void stampSerialAndNodeOnOrderLine(YFSEnvironment env, Document inDoc) {
		YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
		LOGGER.beginTimer("GCStampSerialInforOnComp.stampSerialAndNodeOnOrderLine");
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Input to stampSerialAndNodeOnOrderLine "
					+ yfcInDoc.toString());
		}

		YFCElement eleOrder = yfcInDoc.getDocumentElement();
		YFCElement eleExtn = eleOrder.getChildElement(GCConstants.EXTN);
		String sExtnPropSerailComp = eleExtn
				.getAttribute(GCXmlLiterals.EXTN_PROPOGATE_SERIAL_TO_COMP);
		if (YFCCommon.equals(GCConstants.FLAG_Y, sExtnPropSerailComp)) {
			YFCElement eleOrderLines = eleOrder
					.getChildElement(GCConstants.ORDER_LINES);
			YFCNodeList<YFCElement> nlOrderLines = eleOrderLines
					.getElementsByTagName(GCConstants.ORDER_LINE);
			for (YFCElement eleOrderLine : nlOrderLines) {
				String sKitCode = eleOrderLine
						.getAttribute(GCConstants.KIT_CODE);
				if (YFCCommon.equals(GCConstants.BUNDLE, sKitCode)) {
					String sOrderLineKey = eleOrderLine
							.getAttribute(GCConstants.ORDER_LINE_KEY);
					YFCElement eleItemDetails = eleOrderLine
							.getChildElement(GCConstants.ITEM_DETAILS);
					YFCElement eleOLExtn = eleItemDetails
							.getChildElement(GCConstants.EXTN);
					String sExtnSerialComp = eleOLExtn
							.getAttribute(GCXmlLiterals.EXTN_SERIAL_COMPONENT);
					for (YFCElement eleNewOrderline : nlOrderLines) {
						String bpOrderLineKey = eleNewOrderline
								.getAttribute(GCConstants.BUNDLE_PARENT_ORDERLINEKEY);
						YFCElement eleNewItemDetails = eleNewOrderline
								.getChildElement(GCConstants.ITEM_DETAILS);

						String sItemID = eleNewItemDetails
								.getAttribute(GCConstants.ITEM_ID);
						if (YFCCommon.equals(sOrderLineKey, bpOrderLineKey)
								&& YFCCommon.equals(sItemID, sExtnSerialComp)) {
							YFCElement eleOrderLineInvAttRequest = eleOrderLine
									.getChildElement(GCXmlLiterals.ORDERLINE_INVATT_REQUEST);
							String sBatchNo = eleOrderLineInvAttRequest
									.getAttribute(GCConstants.BATCH_NO);
							YFCElement elenewOrderLineInvAttRequest = eleNewOrderline
									.getChildElement(
											GCXmlLiterals.ORDERLINE_INVATT_REQUEST,
											true);
							elenewOrderLineInvAttRequest.setAttribute(
									GCConstants.BATCH_NO, sBatchNo);
							String sShipNode = eleOrderLine
									.getAttribute(GCConstants.SHIP_NODE);
							eleNewOrderline.setAttribute(GCConstants.SHIP_NODE,
									sShipNode);
						}

					}
				}
			}
		}
		eleExtn.setAttribute(GCXmlLiterals.EXTN_PROPOGATE_SERIAL_TO_COMP,
				GCConstants.FLAG_N);
		eleOrder.setAttribute(GCConstants.OVERRIDE, GCConstants.FLAG_Y);
		GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER,
				yfcInDoc.getDocument());
		LOGGER.endTimer("GCStampSerialInforOnComp.stampSerialAndNodeOnOrderLine");
	}

}
