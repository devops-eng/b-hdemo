/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0               05/16/2016        GC Enhancement Team      GCSTORE-6053: 2 DAX customer accounts to 1 OMS account.
 *#################################################################################################################################################################
 */
package com.gc.api;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCException;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCStampTransactionObject {
    private static final YFCLogCategory LOGGER = YFCLogCategory
            .instance(GCStampTransactionObject.class.getName());
    
    /**
    *
    * stampTransactionObject method is used to stamp transaction object to N,
    * so as to restrict duplicate customer messages to DAX
    * @param env
    * @param inDoc
    * @throws GCException
    *
    */

    public Document stampTransactionObject(YFSEnvironment env, Document inDoc)
            throws GCException{
      try
      {
        env.setTxnObject("Publish", GCConstants.NO);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Publish Txn Object value :: "+env.getTxnObject("Publish"));
        }
        return inDoc;
      }
      catch (Exception e) {
          LOGGER.error(
                  "Inside catch block of GCStampTransactionObject.stampTransactionObject method. Exception is",
                  e);
          throw new GCException(e);
      }
    }
        
   }


