/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               06/02/2014        Gowda, Naveen		JIRA No: This service interacts with the UPS and validates the address.
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.util.Properties;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCWebServiceUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:email Address">Naveen</a>
 */
public class GCUPSAddressValidationStreetLevel implements YIFCustomApi {

    Document inDoc = null;
    String sCountry = "";
    String sUsername = YFSSystem.getProperty("gc.ups.username");
    String sPassword = YFSSystem.getProperty("gc.ups.password");
    String sAccLicenNum = YFSSystem.getProperty("gc.ups.accesslicensenumber");
    String sFirstName = "";
    String sLastName = "";
    String sAddrLine1 = "";
    String sAddrLine2 = "";
    String sCity = "";
    String sZipCode = "";
    String sState = "";
    String sTitle = "";
    String sDayPhone = "";
    String sEvenPhone = "";
    String sEmailID = "";
    String sAddressDesc = "";
    String sCompName = "";
    String sAddrLine3 = "";
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCUPSAddressValidationStreetLevel.class.getName());

    public Document upsAddressValidationStreetLevel(YFSEnvironment env,
	    Document inDoc) throws GCException {

	final String logInfo = "GCUPSService :: upsAddressValidationStreetLevel :: ";
	LOGGER.debug(logInfo + "Begin");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(logInfo + "Input Document"
		    + GCXMLUtil.getXMLString(inDoc));
	}
	this.inDoc = inDoc;
	Document outDoc = null;
	try {
	    Node nAddressKeyFormat = XPathAPI.selectSingleNode(inDoc,
		    "Envelope/Body/XAVRequest/AddressKeyFormat");

	    sCountry = XPathAPI.selectSingleNode(inDoc,
		    "Envelope/Body/XAVRequest/AddressKeyFormat/CountryCode")
		    .getTextContent();
	    sCity = XPathAPI
		    .selectSingleNode(inDoc,
			    "Envelope/Body/XAVRequest/AddressKeyFormat/PoliticalDivision2")
		    .getTextContent();
	    sState = XPathAPI
		    .selectSingleNode(inDoc,
			    "Envelope/Body/XAVRequest/AddressKeyFormat/PoliticalDivision1")
		    .getTextContent();
	    sZipCode = XPathAPI
		    .selectSingleNode(inDoc,
			    "Envelope/Body/XAVRequest/AddressKeyFormat/PostcodePrimaryLow")
		    .getTextContent();

	    LOGGER.debug("CountryCode" + sCountry);

	    Node nFirstName = XPathAPI.selectSingleNode(inDoc,
		    "Envelope/Body/XAVRequest/AddressKeyFormat/FirstName");
	    sFirstName = nFirstName.getTextContent();

	    Node nLastName = XPathAPI.selectSingleNode(inDoc,
		    "Envelope/Body/XAVRequest/AddressKeyFormat/LastName");
	    sLastName = nLastName.getTextContent();

	    Node nAddressLine1 = XPathAPI.selectSingleNode(inDoc,
		    "Envelope/Body/XAVRequest/AddressKeyFormat/AddressLine1");
	    sAddrLine1 = nAddressLine1.getTextContent();

	    Node nAddressLine2 = XPathAPI.selectSingleNode(inDoc,
		    "Envelope/Body/XAVRequest/AddressKeyFormat/AddressLine2");
	    sAddrLine2 = nAddressLine2.getTextContent();
	    /* OMS - 3572 - Begin */
	    Node nAddressLine3 = XPathAPI.selectSingleNode(inDoc,
		    "Envelope/Body/XAVRequest/AddressKeyFormat/AddressLine3");
	    sAddrLine3 = nAddressLine3.getTextContent();

	    Node nCompName = XPathAPI.selectSingleNode(inDoc,
		    "Envelope/Body/XAVRequest/AddressKeyFormat/Company");
	    sCompName = nCompName.getTextContent();

	    /* OMS - 3572 - End */
	    Node nTitle = XPathAPI.selectSingleNode(inDoc,
		    "Envelope/Body/XAVRequest/AddressKeyFormat/Title");
	    sTitle = nTitle.getTextContent();

	    Node nDayPhone = XPathAPI.selectSingleNode(inDoc,
		    "Envelope/Body/XAVRequest/AddressKeyFormat/DayPhone");
	    sDayPhone = nDayPhone.getTextContent();

	    Node nEvenPhone = XPathAPI.selectSingleNode(inDoc,
		    "Envelope/Body/XAVRequest/AddressKeyFormat/EveningPhone");
	    sEvenPhone = nEvenPhone.getTextContent();

	    Node nEmailID = XPathAPI.selectSingleNode(inDoc,
		    "Envelope/Body/XAVRequest/AddressKeyFormat/EMailID");
	    sEmailID = nEmailID.getTextContent();

	    Node nAddressID = XPathAPI.selectSingleNode(inDoc,
		    "Envelope/Body/XAVRequest/AddressKeyFormat/AddressID");
	    sAddressDesc = nAddressID.getTextContent();

	    nAddressKeyFormat.removeChild(nFirstName);
	    nAddressKeyFormat.removeChild(nLastName);
	    nAddressKeyFormat.removeChild(nAddressLine1);
	    nAddressKeyFormat.removeChild(nAddressLine2);
	    nAddressKeyFormat.removeChild(nTitle);
	    nAddressKeyFormat.removeChild(nDayPhone);
	    nAddressKeyFormat.removeChild(nEvenPhone);
	    nAddressKeyFormat.removeChild(nEmailID);
	    nAddressKeyFormat.removeChild(nAddressID);

	    if (GCConstants.US.equals(sCountry)) {

		Element eleUsername = (Element) XPathAPI.selectSingleNode(
			inDoc,
			"Envelope/Header/UPSSecurity/UsernameToken/Username");
		eleUsername.setTextContent(sUsername);
		Element elePassword = (Element) XPathAPI.selectSingleNode(
			inDoc,
			"Envelope/Header/UPSSecurity/UsernameToken/Password");
		elePassword.setTextContent(sPassword);

		Element eleAccessLicNum = (Element) XPathAPI
			.selectSingleNode(inDoc,
				"Envelope/Header/UPSSecurity/ServiceAccessToken/AccessLicenseNumber");
		eleAccessLicNum.setTextContent(sAccLicenNum);
		String sUPSSURL = YFSSystem.getProperty(GCConstants.GC_UPS_URL);
		if (YFCCommon.isVoid(sUPSSURL)) {
		    throw new YFCException(
			    "UPS URL is not configured in property file. Please retry after configuring it.");
		}
		LOGGER.debug(logInfo + "sUPSURL :: " + sUPSSURL);
		String sSoapAction = YFSSystem
			.getProperty(GCConstants.GC_UPS_SOAP_ACTION);
		if (YFCCommon.isVoid(sSoapAction)) {
		    throw new YFCException(
			    "UPS soap action is not configured in property file. Please retry after configuring it.");
		}
		LOGGER.debug(logInfo + "sSoapAction :: " + sSoapAction);

		Document docUPSOp = null;
		GCWebServiceUtil obj = new GCWebServiceUtil();
		
		try {
		    docUPSOp = obj.invokeSOAPWebService(inDoc, sUPSSURL,
			    sSoapAction);

		} catch (Exception e) {
		    LOGGER.error("UPS didn't give back the expected response.",
			    e);
		    outDoc = GCXMLUtil
			    .createDocument(GCConstants.PERSON_INFO_LIST);
		    Element eleRoot = outDoc.getDocumentElement();
		    Element elePersonInfo = outDoc
			    .createElement(GCConstants.PERSON_INFO);
		    eleRoot.appendChild(elePersonInfo);
		    elePersonInfo.setAttribute(GCConstants.AVS_RETURN_CODE,
			    GCConstants.AVS_DOWN);
		    if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(logInfo + "outDoc :: "
				+ GCXMLUtil.getXMLString(outDoc));
		    }
		    return outDoc;
		}
		if (LOGGER.isDebugEnabled()) {
		    LOGGER.debug(logInfo + "docUPSOp :: "
			    + GCXMLUtil.getXMLString(docUPSOp));
		}
		outDoc = processUPSresponse(docUPSOp);
		outDoc = sortUPSresponse(outDoc);

	    } else {
		outDoc = GCXMLUtil.createDocument(GCConstants.PERSON_INFO_LIST);
		Element eleRoot = outDoc.getDocumentElement();
		eleRoot.setAttribute(
			GCConstants.PROCEED_WITH_SINGLE_AVS_RESULT,
			GCConstants.YES);
		Element elePersonInfo = outDoc
			.createElement(GCConstants.PERSON_INFO);
		elePersonInfo.setAttribute(GCConstants.ADDRESS_LINE1,
			sAddrLine1);
		elePersonInfo.setAttribute(GCConstants.ADDRESS_LINE2,
			sAddrLine2);

		elePersonInfo.setAttribute(GCConstants.ADDRESS_LINE3,
			sAddrLine3);
		elePersonInfo.setAttribute("Company", sCompName);

		elePersonInfo.setAttribute(GCConstants.CITY, sCity);
		elePersonInfo.setAttribute(GCConstants.STATE, sState);
		elePersonInfo.setAttribute(GCConstants.COUNTRY, sCountry);
		elePersonInfo.setAttribute(GCConstants.ZIP_CODE, sZipCode);
		elePersonInfo.setAttribute(GCConstants.FIRST_NAME, sFirstName);
		elePersonInfo.setAttribute(GCConstants.LAST_NAME, sLastName);

		elePersonInfo.setAttribute(GCConstants.TITLE, sTitle);
		elePersonInfo.setAttribute(GCConstants.DAY_PHONE, sDayPhone);
		elePersonInfo.setAttribute(GCConstants.EVENING_PHONE,
			sEvenPhone);
		elePersonInfo.setAttribute(GCConstants.EMAIL_ID, sEmailID);
		elePersonInfo
			.setAttribute(GCConstants.ADDRESS_ID, sAddressDesc);
		eleRoot.appendChild(elePersonInfo);
	    }
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch block of GCUPSAddressValidationStreetLevel.upsAddressValidationStreetLevel(), Exception is :",
		    e);
	    throw GCException.getYFCException(e.getMessage(), e);
	}
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(logInfo + "outDoc :: "
		    + GCXMLUtil.getXMLString(outDoc));
	}
	LOGGER.debug(logInfo + "End");
	return outDoc;
    }

    /**
     * CR - 34 Description of sortUPSresponse - This method is used to sort UPS
     * response so that address which are in the range are show in the end.
     * Sorting is done based on "+" character.
     * 
     * @param outDoc
     * @return
     * @throws Exception
     * @throws IllegalArgumentException
     * 
     */
    private Document sortUPSresponse(Document outDoc) throws GCException {
	try {
	    Element elePersonInfoList = GCXMLUtil.getElementByXPath(outDoc,
		    "/PersonInfoList");
	    NodeList nlPersonInfo = GCXMLUtil.getNodeListByXpath(outDoc,
		    "/PersonInfoList/PersonInfo");
	    /* Sorting AddressLine2 */
	    for (int i = 0; i < nlPersonInfo.getLength(); i++) {
		Element elePersonInfo = (Element) nlPersonInfo.item(i);
		String sAddressLine2 = elePersonInfo
			.getAttribute(GCConstants.ADDRESS_LINE2);
		String sAddressLine1 = elePersonInfo
			.getAttribute(GCConstants.ADDRESS_LINE1);
		String sAddressLine = sAddressLine1.concat(sAddressLine2);
		if (sAddressLine.contains("-")) {
		    Element eleNewPersonInfoElement = outDoc
			    .createElement(GCConstants.PERSON_INFO);
		    GCXMLUtil.copyAttributes(elePersonInfo,
			    eleNewPersonInfoElement);
		    GCXMLUtil.removeChild(elePersonInfoList, elePersonInfo);
		    elePersonInfoList.appendChild(eleNewPersonInfoElement);
		}
	    }
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch block of GCUPSAddressValidationStreetLevel.sortUPSresponse(), Exception is :",
		    e);
	    throw new GCException(e);
	}
	return outDoc;
    }

    /**
     * 
     * Description of processUPSresponse - This method is used to processing UPS
     * soap response to XML document.
     * 
     * @param docUPSOp
     * @return
     * @throws Exception
     * 
     */
    private Document processUPSresponse(Document docUPSOp) throws GCException {
	try {
	    Document docOut = GCXMLUtil
		    .createDocument(GCConstants.PERSON_INFO_LIST);
	    Element eleRoot = docOut.getDocumentElement();
	    eleRoot.setAttribute(GCConstants.PROCEED_WITH_SINGLE_AVS_RESULT,
		    GCConstants.YES);

	    NodeList nlCandidate = XPathAPI.selectNodeList(docUPSOp,
		    "Envelope/Body/XAVResponse/Candidate");
	    LOGGER.debug("Nodelist Length" + nlCandidate.getLength());

	    if (nlCandidate.getLength() == 0) {

		Element eleAddressVerificationResponseMessages = docOut
			.createElement(GCConstants.ADDRESS_VERIFIFICATION_RESPONSE_MESSAGES);
		eleRoot.appendChild(eleAddressVerificationResponseMessages);
		Element eleAddressVerificationResponseMessage = docOut
			.createElement(GCConstants.ADDRESS_VERIFIFICATION_RESPONSE_MESSAGE);
		eleAddressVerificationResponseMessages
			.appendChild(eleAddressVerificationResponseMessage);
		Element elePersonInfo = docOut
			.createElement(GCConstants.PERSON_INFO);
		eleRoot.appendChild(elePersonInfo);
		elePersonInfo.setAttribute(GCConstants.AVS_RETURN_CODE,
			GCConstants.FAILED);
		eleAddressVerificationResponseMessage
			.setAttribute("MessageText",
				"Address verification service was unable to verify the address from the data.");
		
		//Attribute added to identify validation status: Start.
		Element eleNoCandidatesIndicator = (Element) XPathAPI.selectSingleNode(docUPSOp,
			    "Envelope/Body/XAVResponse/NoCandidatesIndicator");
		 if(!YFCCommon.isVoid(eleNoCandidatesIndicator))
		 {
			 eleRoot.setAttribute("AddressIndicator", "NoCandidatesIndicator");
		 }
		//Attribute added to identify validation status: End.
		return docOut;
	    }

	    if (nlCandidate.getLength() > 0) {
		// ProceedWithSingleAVSResult is set to N, to give option to CSR
		// to
		// select between UPS return address and customer provided
		// address
		eleRoot.setAttribute(
			GCConstants.PROCEED_WITH_SINGLE_AVS_RESULT,
			GCConstants.NO);

	    }
	    LOGGER.debug("Candidate Nodelist length" + nlCandidate.getLength());
	    for (int i = 0; i < nlCandidate.getLength(); i++) {

		Element eleCandidate = (Element) nlCandidate.item(i);
		Element elePersonInfo = docOut
			.createElement(GCConstants.PERSON_INFO);

		NodeList nlAddressLine = XPathAPI.selectNodeList(eleCandidate,
			"AddressKeyFormat/AddressLine");
		Element eleAddressLine1 = (Element) nlAddressLine.item(0);
		String sAddressLine2 = "";
		Element eleAddressLine2 = (Element) nlAddressLine.item(1);
		if (!YFCCommon.isVoid(eleAddressLine2)) {
		    sAddressLine2 = eleAddressLine2.getTextContent();
		}
		elePersonInfo.setAttribute(GCConstants.ADDRESS_LINE2,
			sAddressLine2);

		Element elePolDiv2 = (Element) XPathAPI.selectSingleNode(
			eleCandidate, "AddressKeyFormat/PoliticalDivision2");
		Element elePolDiv1 = (Element) XPathAPI.selectSingleNode(
			eleCandidate, "AddressKeyFormat/PoliticalDivision1");
		Element eleCountCode = (Element) XPathAPI.selectSingleNode(
			eleCandidate, "AddressKeyFormat/CountryCode");
		Element elePCodePriLow = (Element) XPathAPI.selectSingleNode(
			eleCandidate, "AddressKeyFormat/PostcodePrimaryLow");
		String sAddrClassCode = XPathAPI.selectSingleNode(eleCandidate,
			"AddressClassification/Code").getTextContent();

		if (GCConstants.INT_ZERO.equalsIgnoreCase(sAddrClassCode)) {
		    elePersonInfo.setAttribute(
			    GCConstants.IS_COMMERCIAL_ADDRESS, "");
		} else if (GCConstants.INT_ONE.equalsIgnoreCase(sAddrClassCode)) {
		    elePersonInfo.setAttribute(
			    GCConstants.IS_COMMERCIAL_ADDRESS,
			    GCConstants.FLAG_Y);
		} else {
		    elePersonInfo.setAttribute(
			    GCConstants.IS_COMMERCIAL_ADDRESS,
			    GCConstants.FLAG_N);
		}
		elePersonInfo.setAttribute(GCConstants.ADDRESS_LINE1,
			eleAddressLine1.getTextContent());
		elePersonInfo.setAttribute(GCConstants.CITY,
			elePolDiv2.getTextContent());
		elePersonInfo.setAttribute(GCConstants.STATE,
			elePolDiv1.getTextContent());
		elePersonInfo.setAttribute(GCConstants.COUNTRY,
			eleCountCode.getTextContent());
		elePersonInfo.setAttribute(GCConstants.ZIP_CODE,
			elePCodePriLow.getTextContent());
		elePersonInfo.setAttribute(GCConstants.FIRST_NAME, sFirstName);
		elePersonInfo.setAttribute(GCConstants.LAST_NAME, sLastName);
		elePersonInfo.setAttribute(GCConstants.TITLE, sTitle);
		elePersonInfo.setAttribute(GCConstants.DAY_PHONE, sDayPhone);
		elePersonInfo.setAttribute(GCConstants.EVENING_PHONE,
			sEvenPhone);
		elePersonInfo.setAttribute(GCConstants.EMAIL_ID, sEmailID);
		elePersonInfo
			.setAttribute(GCConstants.ADDRESS_ID, sAddressDesc);
		elePersonInfo.setAttribute(GCConstants.ADDRESS_LINE3,
			sAddrLine3);
		elePersonInfo.setAttribute("Company", sCompName);
		eleRoot.appendChild(elePersonInfo);

	    }
	    
	  //Attribute added to identify validation status: Start.
		 Element eleValidAddressIndicator = (Element) XPathAPI.selectSingleNode(docUPSOp,
				    "Envelope/Body/XAVResponse/ValidAddressIndicator");
		 if(!YFCCommon.isVoid(eleValidAddressIndicator))
	    {
			 eleRoot.setAttribute("AddressIndicator", "ValidAddressIndicator");
	    }
		 Element eleAmbiguousAddressIndicator = (Element) XPathAPI.selectSingleNode(docUPSOp,
					    "Envelope/Body/XAVResponse/AmbiguousAddressIndicator");
		 if(!YFCCommon.isVoid(eleAmbiguousAddressIndicator))
		 {
			 eleRoot.setAttribute("AddressIndicator", "AmbiguousAddressIndicator");
		 }
		//Attribute added to identify validation status: End.
	    
	    
	    return docOut;
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch block of GCUPSAddressValidationStreetLevel.processUPSresponse(), Exception is :",
		    e);
	    throw new GCException(e);
	}
    }

    public void setProperties(Properties arg0) throws GCException {

    }
}
