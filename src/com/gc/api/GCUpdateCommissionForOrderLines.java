/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *
 *#################################################################################################################################################################
 *           Version            Date              Modified By                Description
 *#################################################################################################################################################################
 *             1.0              04/28/2015       Infosys Limited            Initial Version
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.util.Properties;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * <h3>Description :</h3>This java class is called from '' Service to check if the commission is
 * added on the orderlines. If not prepare the input to the changeOrder to add the commission
 *
 * @author Infosys Limited
 */
public class GCUpdateCommissionForOrderLines implements YIFCustomApi {


  /*
   * The class name
   */
  private static final String CLASS_NAME = GCUpdateCommissionForOrderLines.class.getName();

  /*
   * The logger reference
   */
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(CLASS_NAME);

  /**
   * <h3>Description:<h3>This method is used set the arguments configured in the service as
   * properties
   */
  @Override
  public void setProperties(final Properties props) {
	  
  }


  /**
   * <h3>Description:</h3>This method is used to prepare the changeOrder api input to update the
   * commission
   *
   * @param env The YFSEnvironment reference
   * @param docInput The onsuccess event xml
   * @return the change order input document to update the commission
   * @throws TransformerException Thrown if the incorrect xpath configured
   */
  public Document updateMissingCommission(final YFSEnvironment env, final Document docInput)
      throws TransformerException {

    LOGGER.info(CLASS_NAME + "Inside GCUpdateMissingCommission - Start");

    // fetch the order headerkey
    final String strOrderHeaderKey = GCXMLUtil.getAttributeFromXPath(docInput, "Order/@OrderHeaderKey");

    // Prepare Input for Change Order
    final Document docChangeOrder = GCXMLUtil.createDocument(GCConstants.ORDER);
    final Element eleChangeOrder = docChangeOrder.getDocumentElement();
    eleChangeOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, strOrderHeaderKey);
    eleChangeOrder.setAttribute(GCConstants.ACTION, GCConstants.MODIFY);
    
    Element eleRoot = docInput.getDocumentElement();
    String sEntryType = eleRoot.getAttribute(GCConstants.ENTRY_TYPE);
    
    if(YFCCommon.equalsIgnoreCase(sEntryType, "mPOS"))
    {
    	eleChangeOrder.setAttribute(GCConstants.ENTRY_TYPE, sEntryType);
    }

    // check if the GCOrderCommission exists
    final NodeList nlGCOrderCommission =
        XPathAPI.selectNodeList(docInput, "Order/Extn/GCOrderCommissionList/GCOrderCommission");
    final int gcOrderCommissionCount = nlGCOrderCommission.getLength();
    if (gcOrderCommissionCount != 0) {

      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(CLASS_NAME + "Inside GCOrderCommission:Count of GC Order Commission=" + gcOrderCommissionCount);
      }

      // prepare the orderline commission list
      final Element eleGCOrderLineCommissionList =
          prepareGCOLCommListElement(docInput, nlGCOrderCommission, gcOrderCommissionCount);

      final Element eleOrderLines = docChangeOrder.createElement(GCConstants.ORDER_LINES);
      final NodeList nlOrderLine = XPathAPI.selectNodeList(docInput, "Order/OrderLines/OrderLine");
      int nlOrderLineCount = nlOrderLine.getLength();
      Element eleOrderLine;
      Element eleChangeOrderLine;
      for (int iOLIndex = 0; iOLIndex < nlOrderLineCount; iOLIndex++) {

        // fetch the orderline element
        eleOrderLine = (Element) nlOrderLine.item(iOLIndex);

        // update the OrderLines element
        eleChangeOrderLine = getChangeOrderOL(docChangeOrder, eleGCOrderLineCommissionList, eleOrderLine);

        // check if the order is present
        if (eleChangeOrderLine != null) {

          // append the orderline to the orderlines element
          eleOrderLines.appendChild(eleChangeOrderLine);
        }
      }

      // append the orderline element to the order element
      eleChangeOrder.appendChild(eleOrderLines);
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(CLASS_NAME + "Doc Change Order XML=" + GCXMLUtil.getXMLString(docChangeOrder));
    }


    LOGGER.info(CLASS_NAME + "Inside GCUpdateMissingCommission - End");

    // return the changorder document
    return docChangeOrder;
  }


  /**
   * <h3>Description:</h3>This method is used to update OrderLine element of the changeOrder
   *
   * @param docChangeOrder The changeOrder api doc
   * @param eleGCOrderLineCommissionList The GCOrderLineCommissionList element
   * @param eleOrderLines The orderlines element to be udpated for changeOrder api input
   * @param eleOrderLine The orderLine element
   * @return returns null of the order line is not elegible to be updated else the orderline to be
   *         updated
   * @throws TransformerException thrown if the Extn element path is incorrect
   */
  private Element getChangeOrderOL(final Document docChangeOrder, final Element eleGCOrderLineCommissionList,
      final Element eleOrderLine) throws TransformerException {

    // the order line element to be returned
    final Element eleChangeOrderLine;

    // Check Orderline status is not cancelled and OrderLine does not have commission
    if (isOLElegibleForChangeOrderIn(eleOrderLine)) {

      eleChangeOrderLine = (Element) docChangeOrder.importNode(eleOrderLine, false);
      final Element elemGCOrderLineCommissionList =
          GCXMLUtil.createElement(docChangeOrder, GCConstants.GC_ORDER_LINE_COMMISSION_LIST, null);
      final Element eleExtn = GCXMLUtil.createElement(docChangeOrder, GCConstants.EXTN, null);
      GCXMLUtil.copyElement(docChangeOrder, eleGCOrderLineCommissionList, elemGCOrderLineCommissionList);
      eleExtn.appendChild(elemGCOrderLineCommissionList);
      eleChangeOrderLine.appendChild(eleExtn);

    } else {

      // set default value as null
      eleChangeOrderLine = null;
    }

    // return the orderLine element
    return eleChangeOrderLine;
  }


  /**
   * <h3>Description:</h3>This method is used to check if the Orderline is eligible to changeOrder
   * api input
   *
   * @param eleOrderLine The orderline to be checked
   * @return true if the input line is eligible for api input else false
   * @throws TransformerException thrwon if xpath is incorrectly configured
   */
  private boolean isOLElegibleForChangeOrderIn(final Element eleOrderLine) throws TransformerException {

    // fetch the max line status
    final String sOrderLineStatus = eleOrderLine.getAttribute(GCConstants.MAX_LINE_STATUS);
    final Element eleOrderLineExtn = (Element) XPathAPI.selectSingleNode(eleOrderLine, GCConstants.EXTN);
    final NodeList nlInputGCOrderLineCommissionList =
        eleOrderLineExtn.getElementsByTagName(GCConstants.GC_ORDERLINE_COMMISSION);
    final int iGCOrderLineCommissionCount = nlInputGCOrderLineCommissionList.getLength();

    // the flag to check if the orderline is eligible for changeOrder api input
    final boolean isEligible;

    // Check Orderline status is not cancelled and OrderLine does not have commission
    if ((!GCConstants.CANCELLED_ORDER_STATUS_CODE.equals(sOrderLineStatus)) && iGCOrderLineCommissionCount == 0) {

      // set default value as true
      isEligible = true;
    }else{

      //not elegible
      isEligible = false;
    }

    //return the flag
    return isEligible;
  }


  /**
   * <h3>Description:</h3> This method is used to prepare the GCOrderLineCommissionList element
   *
   * @param docInput The input document
   * @param nlGCOrderCommission The GCOrderCommission elment
   * @param gcOrderCommissionCount The count of the Order Commission
   * @return the GCOrderLineCommissionList element
   */
  private Element prepareGCOLCommListElement(final Document docInput, final NodeList nlGCOrderCommission,
      final int gcOrderCommissionCount) {

    // Prepare the OrderLine Commission from Order Commission
    final Element eleGCOrderLineCommissionList =
        GCXMLUtil.createElement(docInput, GCConstants.GC_ORDER_LINE_COMMISSION_LIST, null);

    // iterate through each of the GCOrderCommission element
    Element eleGCOrderCommission;
    Element eleGCOrderLineCommission;
    for (int iGCOrdCommIndex = 0; iGCOrdCommIndex < gcOrderCommissionCount; iGCOrdCommIndex++) {

      // fetch the GCOrderCommission
      eleGCOrderCommission = (Element) nlGCOrderCommission.item(iGCOrdCommIndex);

      // prepare the GCOrderLineCommission element
      eleGCOrderLineCommission = preGCOLCommissionElement(docInput, eleGCOrderCommission);
      eleGCOrderLineCommissionList.appendChild(eleGCOrderLineCommission);
    }

    // return the updated GCOrderLineCommissionList elment
    return eleGCOrderLineCommissionList;
  }


  /**
   * <h3>Description:</h3>This method is used to prepare the GCOrderLineCommission element from the
   * input document
   *
   * @param docInput The api input document
   * @param eleGCOrderCommission The GCOrderCommission elment from which the GCOrderLineCommission
   *        element is to be prepared
   * @return The GCOrderLineCommission elment
   */
  private Element preGCOLCommissionElement(final Document docInput, Element eleGCOrderCommission) {

    // prepare the GCOrderLineCommission element
    final Element eleGCOrderLineCommission =
        GCXMLUtil.createElement(docInput, GCConstants.GC_ORDER_LINE_COMISSION, null);
    eleGCOrderLineCommission.setAttribute(GCConstants.ALLOCATION_PERCENTAGE,
        eleGCOrderCommission.getAttribute(GCConstants.ALLOCATION_PERCENTAGE));
    eleGCOrderLineCommission.setAttribute(GCConstants.COMMISSION_TYPE,
        eleGCOrderCommission.getAttribute(GCConstants.COMMISSION_TYPE));
    eleGCOrderLineCommission.setAttribute(GCConstants.ORDER_HEADER_KEY,
        eleGCOrderCommission.getAttribute(GCConstants.ORDER_HEADER_KEY));
    eleGCOrderLineCommission.setAttribute(GCConstants.USER_ID,
        eleGCOrderCommission.getAttribute(GCConstants.USER_ID));
    eleGCOrderLineCommission.setAttribute(GCConstants.USERNAME1,
        eleGCOrderCommission.getAttribute(GCConstants.USERNAME1));

    // return the GCOrderLineCommission element
    return eleGCOrderLineCommission;
  }


}
