/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This call is used update shipping charges on an order.
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               06/05/2014        Gowda, Naveen		JIRA No: This class is used to update Shipping Charge on Order.
 *#################################################################################################################################################################
 */
package com.gc.api;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:email Address">Name</a>
 */
public class GCUpdateOrderShippingCharge {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCUpdateOrderShippingCharge.class.getName());

  public void updateShippingCharges(YFSEnvironment env, Document inDoc)
      throws GCException {

    LOGGER.debug("Class: GCUpdateOrderShippingCharge Method: updateShippingCharges BEGIN");
    try {
      String sOrderHeaderKey = GCXMLUtil.getAttributeFromXPath(inDoc,
          "//Order/@OrderHeaderKey");
      String sLevelOfService = GCXMLUtil.getAttributeFromXPath(inDoc,
          "//Order/@LevelOfService");

      Document docGetOrderShipChargeIn = GCXMLUtil
          .createDocument(GCConstants.GC_ORDER_SHIPPING_CHARGE);
      Element eleRoot = docGetOrderShipChargeIn.getDocumentElement();
      eleRoot.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
      eleRoot.setAttribute(GCConstants.LEVEL_OF_SERVICE, sLevelOfService);

      String sShippingChargeAmount = "0.00";
      if (GCConstants.EMPLOYEE_PICKUP.equalsIgnoreCase(sLevelOfService)
          || GCConstants.DIGITAL_DELIVERY
          .equalsIgnoreCase(sLevelOfService)) {
        sShippingChargeAmount = "0.00";
      } else {
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("GCGetOrderShippingCharge Input"
              + GCXMLUtil.getXMLString(docGetOrderShipChargeIn));
        }
        Document docGetOrderShipChargeOut = GCCommonUtil.invokeService(
            env, GCConstants.GC_GET_ORDER_SHIPPING_CHARGE_SERVICE,
            docGetOrderShipChargeIn);

        if (!YFCCommon.isVoid(docGetOrderShipChargeOut)) {

          Element eleGetOrderShipRoot = docGetOrderShipChargeOut
              .getDocumentElement();
          if (eleGetOrderShipRoot.hasAttributes()
              && !YFCCommon.isVoid(eleGetOrderShipRoot)) {
            sShippingChargeAmount = GCXMLUtil
                .getAttributeFromXPath(
                    docGetOrderShipChargeOut,
                    "//GCOrderShippingCharge/@ShippingChargeAmount");
            if (LOGGER.isDebugEnabled()) {
              LOGGER.debug("docGetOrderShipChargeOut Output"
                  + GCXMLUtil
                  .getXMLString(docGetOrderShipChargeOut));
            }
            callShippingProrationMethod(env, sOrderHeaderKey, sShippingChargeAmount);
          }
        }
      }

      LOGGER.debug("sShippingChargeAmount::" + sShippingChargeAmount);

    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCUpdateOrderShippingCharge.updateShippingCharges(), Exception is :",
          e);
      throw new GCException(e);
    }
    LOGGER.debug("Class: GCUpdateOrderShippingCharge Method: updateShippingCharges BEGIN");

  }

  /**
   * 
   * Description of callShippingProrationMethod
   * 
   * @param env
   * @param sOrderHeaderKey
   * @param sShippingChargeAmount
   * @throws Exception
   * 
   */
  public void callShippingProrationMethod(YFSEnvironment env,
      String sOrderHeaderKey, String sShippingChargeAmount)
          throws GCException {

    LOGGER.debug("Class: GCUpdateOrderShippingCharge Method: callShippingProrationMethod BEGIN");
    try {
      Document docGetOrdIn = GCXMLUtil.createDocument(GCConstants.ORDER);
      Element eleGetOrdrInRoot = docGetOrdIn.getDocumentElement();
      eleGetOrdrInRoot.setAttribute(GCConstants.ORDER_HEADER_KEY,
          sOrderHeaderKey);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("get Order List Input"
            + GCXMLUtil.getXMLString(docGetOrdIn));
      }
      Document docGetOrdOut = null;
      if (!YFCCommon.isVoid(sOrderHeaderKey)) {
        docGetOrdOut = GCCommonUtil
            .invokeAPI(env, docGetOrdIn,
                GCConstants.API_GET_ORDER_LIST,
                "/global/template/api/getOrderListTempForShippingProration.xml");
      }
      Element eleOrderRO = GCXMLUtil.getElementByXPath(docGetOrdOut,
          "/OrderList/Order");
      Document docEleOrderRO = GCXMLUtil
          .getDocumentFromElement(eleOrderRO);
      Element eleRoot = docEleOrderRO.getDocumentElement();
      Element eleOrderExtn = GCXMLUtil.getElementByXPath(docEleOrderRO,
          "//Order/Extn");
      eleOrderExtn.setAttribute(GCConstants.EXTN_IS_SHIPPING_PRORATED,
          GCConstants.FLAG_N);

      Element eleHeaderCharge = GCXMLUtil
          .getElementByXPath(docEleOrderRO,
              "/Order/HeaderCharges/HeaderCharge[@ChargeCategory='Shipping']");
      if (YFCCommon.isVoid(eleHeaderCharge)) {
        Element eleHeaderCharges = docEleOrderRO
            .createElement(GCConstants.HEADER_CHARGES);
        eleHeaderCharge = docEleOrderRO
            .createElement(GCConstants.HEADER_CHARGE);
        eleHeaderCharges.appendChild(eleHeaderCharge);
        eleRoot.appendChild(eleHeaderCharges);

      }
      eleHeaderCharge.setAttribute(GCConstants.CHARGE_AMOUNT,
          sShippingChargeAmount);
      eleHeaderCharge.setAttribute(GCConstants.CHARGE_CATEGORY,
          GCConstants.SHIPPING);
      eleHeaderCharge.setAttribute(GCConstants.CHARGE_NAME,
          GCConstants.SHIPPING_CHARGE);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Inside GCUpdateOrderShippingCharge.callShippingProrationMethod: Input to changeOrder"
            + GCXMLUtil.getXMLString(docEleOrderRO));
      }
      GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER,
          docEleOrderRO);
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCUpdateOrderShippingCharge.callShippingProrationMethod(), Exception is :",
          e);
      throw new GCException(e);
    }
    LOGGER.debug("Class: GCUpdateOrderShippingCharge Method: callShippingProrationMethod BEGIN");

  }
}