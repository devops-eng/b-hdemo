/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class is used to refund Shipping Charges on RO
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               06/02/2014        Gowda, Naveen		JIRA No: Description of issue.
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:email Address">Naveen</a>
 */
public class GCUpdateRefundShippingTax {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCUpdateRefundShippingTax.class.getName());

  /**
   *
   * Description of updateRefundShippingTax
   *
   * @param env
   * @param inDoc
   * @param eleHeaderCharge
   * @param obj
   * @throws Exception
   *
   */
  public Document updateRefundShippingTax(YFSEnvironment env, Document inDoc,
      Element eleHeaderCharge, Object obj) throws GCException {

    LOGGER.debug("Class: GCUpdateRefundShippingTax Method: gcAddWarrantyLineToRO BEGIN");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Inside GCUpdateRefundShippingTax.updateRefundShippingTax:  updateRefundShippingTax + Incoming XML"
          + GCXMLUtil.getXMLString(inDoc));
    }
    try {
      Map<String, Element> map = new HashMap<String, Element>();

      Element eleRoot = inDoc.getDocumentElement();
      String sOrderHeaderKeyRO = eleRoot
          .getAttribute(GCConstants.ORDER_HEADER_KEY);

      Document docGetOrdIn = GCXMLUtil.createDocument(GCConstants.ORDER);
      Element eleGetOrdrInRoot = docGetOrdIn.getDocumentElement();
      eleGetOrdrInRoot.setAttribute(GCConstants.ORDER_HEADER_KEY,
          sOrderHeaderKeyRO);
      Document docGetROOrdOut = null;
      if (!YFCCommon.isVoid(sOrderHeaderKeyRO)) {
        docGetROOrdOut = GCCommonUtil
            .invokeAPI(env, docGetOrdIn,
                GCConstants.API_GET_ORDER_LIST,
                "/global/template/api/getOrderListTempForShippingProration.xml");
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("Inside GCUpdateRefundShippingTax.updateRefundShippingTax:  getOrderList RO OutDoc"
              + GCXMLUtil.getXMLString(docGetROOrdOut));
        }
      }
      Element eleOrderRO = GCXMLUtil.getElementByXPath(docGetROOrdOut,
          "/OrderList/Order");
      Document docEleOrderRO = GCXMLUtil
          .getDocumentFromElement(eleOrderRO);
      Element eleDocEleOrderRORoot = docEleOrderRO.getDocumentElement();
      Element eleOrderExtn = GCXMLUtil.getElementByXPath(docEleOrderRO,
          "//Order/Extn");
      eleOrderExtn.setAttribute(GCConstants.EXTN_IS_SHIPPING_PRORATED,
          GCConstants.FLAG_N);

      if (!YFCCommon.isVoid(eleHeaderCharge) && (YFCCommon.isVoid(obj))) {
        String sDerivedOrdrHdrKey = GCXMLUtil.getAttributeFromXPath(
            docGetROOrdOut,
            "//OrderLine/@DerivedFromOrderHeaderKey");
        Document docGetOrderDetailsIn = GCXMLUtil
            .createDocument(GCConstants.ORDER);
        Element eleRootGetOrdIn = docGetOrderDetailsIn
            .getDocumentElement();
        eleRootGetOrdIn.setAttribute(GCConstants.ORDER_HEADER_KEY,
            sDerivedOrdrHdrKey);
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("Inside GCUpdateRefundShippingTax.updateRefundShippingTax:  docGetSOOrderDetIn::"
              + GCXMLUtil.getXMLString(docGetOrderDetailsIn));
        }
        Document docGetSOOrderDetOut = null;
        if (!YFCCommon.isVoid(sDerivedOrdrHdrKey)) {
          docGetSOOrderDetOut = GCCommonUtil
              .invokeAPI(env, docGetOrderDetailsIn,
                  GCConstants.API_GET_ORDER_LIST,
                  "/global/template/api/getOrderListTempForUpdateROPrice.xml");
          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Inside GCUpdateRefundShippingTax.updateRefundShippingTax:  getOrderList SO OutDoc"
                + GCXMLUtil.getXMLString(docGetSOOrderDetOut));
          }
        }
        Element eleOrder = GCXMLUtil.getElementByXPath(
            docGetSOOrderDetOut, "/OrderList/Order");
        Document docEleOrder = GCXMLUtil
            .getDocumentFromElement(eleOrder);

        NodeList nLOrderLineSO = GCXMLUtil.getNodeListByXpath(
            docEleOrder, "//Order/OrderLines/OrderLine");

        for (int i = 0; i < nLOrderLineSO.getLength(); i++) {
          Element eleOrderLineSO = (Element) nLOrderLineSO.item(i);
          String sOrderLineKey = eleOrderLineSO
              .getAttribute(GCConstants.ORDER_LINE_KEY);

          map.put(sOrderLineKey, eleOrderLineSO);
        }

        Element eleHeaderChargeRO = docEleOrderRO
            .createElement(GCConstants.HEADER_CHARGE);
        GCXMLUtil.copyAttributes(eleHeaderCharge, eleHeaderChargeRO);
        LOGGER.debug("Inside GCUpdateRefundShippingTax.updateRefundShippingTax:  eleHeaderCharge"
            + GCXMLUtil.getElementString(eleHeaderChargeRO));

        Element eleHeaderChargesRO = GCXMLUtil.getElementByXPath(
            docEleOrderRO, "//HeaderCharges");
        eleDocEleOrderRORoot.removeChild(eleHeaderChargesRO);

        eleHeaderChargesRO = docEleOrderRO
            .createElement(GCConstants.HEADER_CHARGES);

        eleHeaderChargesRO.appendChild(eleHeaderChargeRO);
        eleDocEleOrderRORoot.appendChild(eleHeaderChargesRO);
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("Inside GCUpdateRefundShippingTax.updateRefundShippingTax:  changeOrder document"
              + GCXMLUtil.getXMLString(docEleOrderRO));
        }
        env.setTxnObject("isProration", GCConstants.YES);
        Document docCO = GCCommonUtil.invokeAPI(env,
            GCConstants.API_CHANGE_ORDER, docEleOrderRO);
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("Inside GCUpdateRefundShippingTax.updateRefundShippingTax:  CO OUT DOC"
              + GCXMLUtil.getXMLString(docCO));
        }

      }
      if (!YFCCommon.isVoid(sOrderHeaderKeyRO)) {
        docGetROOrdOut = GCCommonUtil
            .invokeAPI(env, docGetOrdIn,
                GCConstants.API_GET_ORDER_LIST,
                "/global/template/api/getOrderListTempForShippingProration.xml");
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("Inside GCUpdateRefundShippingTax.updateRefundShippingTax:  getOrderList after Shipping refund is prorated"
              + GCXMLUtil.getXMLString(docGetROOrdOut));
        }
      }
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Inside GCUpdateRefundShippingTax.GCUpdateRefundShippingTax:  get Return Order List Output"
            + GCXMLUtil.getXMLString(docGetROOrdOut));
      }
      Element changeOrdrROShipTax = GCXMLUtil.getElementByXPath(
          docGetROOrdOut, "/OrderList/Order");
      Document docChangeOrderRO = GCXMLUtil
          .getDocumentFromElement(changeOrdrROShipTax);

      NodeList nLOrderLineRO = GCXMLUtil.getNodeListByXpath(
          docChangeOrderRO, "//Order/OrderLines/OrderLine");

      for (int j = 0; j < nLOrderLineRO.getLength(); j++) {
        Element eleOrderLineRO = (Element) nLOrderLineRO.item(j);
        String sOrderLineKey = eleOrderLineRO
            .getAttribute(GCConstants.ORDER_LINE_KEY);
        Document docEleOrderLineRO = GCXMLUtil
            .getDocumentFromElement(eleOrderLineRO);
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("Inside GCUpdateRefundShippingTax.GCUpdateRefundShippingTax:  docEleOrderLineRO"
              + GCXMLUtil.getXMLString(docEleOrderLineRO));
        }
        // Remember:: Shipping Refund
        String sShippingRefund = GCXMLUtil
            .getAttributeFromXPath(docEleOrderLineRO,
                "//LineCharges/LineCharge[@ChargeName='ShippingRefund']/@ChargeAmount");
        if (YFCCommon.isVoid(sShippingRefund)) {
          sShippingRefund = "0.00";
        }

        String sDerivedFromOrderLineKey = eleOrderLineRO
            .getAttribute(GCConstants.DERIVED_FROM_ORDER_LINE_KEY);

        Iterator iterator = map.keySet().iterator();

        while (iterator.hasNext()) {
          String key = iterator.next().toString();
          String value = map.get(key).toString();

          LOGGER.debug("Inside GCUpdateRefundShippingTax.GCUpdateRefundShippingTax:  key="
              + key + "  value=" + value);
        }
        Element eleOrderLineSO = map.get(sDerivedFromOrderLineKey);
        LOGGER.debug("Inside GCUpdateRefundShippingTax.GCUpdateRefundShippingTax:  eleOrderLineSO"
            + GCXMLUtil.getElementString(eleOrderLineSO));
        Document docEleOrderLineSO = GCXMLUtil
            .getDocumentFromElement(eleOrderLineSO);

        if (!YFCCommon.isVoid(eleOrderLineSO)) {
          /* Shipping Refund Tax Calculation */

          calculateShippingRefundTax(docEleOrderLineSO, docChangeOrderRO, sOrderLineKey, sShippingRefund);
        }
      }
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Inside GCUpdateRefundShippingTax.GCUpdateRefundShippingTax:  changeOrder doc during Shipping Refund and Tax calculation"
            + GCXMLUtil.getXMLString(docChangeOrderRO));
      }
      LOGGER.debug("Class: GCUpdateRefundShippingTax Method: gcAddWarrantyLineToRO END");
      return docChangeOrderRO;
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCUpdateRefundShippingTax.updateRefundShippingTax(), Exception is :",
          e);
      throw new GCException(e);
    }
  }

  /**
   * 
   * @param docEleOrderLineSO
   * @param docChangeOrderRO
   * @param sOrderLineKey
   * @param sShippingRefund
   */
  private void calculateShippingRefundTax(Document docEleOrderLineSO, Document docChangeOrderRO, String sOrderLineKey,
      String sShippingRefund) throws GCException {

    try {
      String sSOShippingLineTax =
          GCXMLUtil.getAttributeFromXPath(docEleOrderLineSO, "//LineTaxes/LineTax[@ChargeName='ShippingCharge']/@Tax");
      if (YFCCommon.isVoid(sSOShippingLineTax)) {
        sSOShippingLineTax = "0.00";
      }

      String sSOShippingLineCharge =
          GCXMLUtil.getAttributeFromXPath(docEleOrderLineSO,
              "//LineCharges/LineCharge[@ChargeName='ShippingCharge']/@ChargeAmount");
      if (!YFCCommon.isVoid(sSOShippingLineCharge)) {
        String sShippingRefundTax = "0.00";
        Element eleLineTaxes =
            GCXMLUtil.getElementByXPath(docChangeOrderRO, "//OrderLine[@OrderLineKey='" + sOrderLineKey
                + "']/LineTaxes");
        Element eleLineTax =
            (Element) XPathAPI.selectSingleNode(docChangeOrderRO, "//OrderLine[@OrderLineKey='" + sOrderLineKey
                + "']/LineTaxes/LineTax[@ChargeName='ShippingRefund']");
        if (YFCCommon.isVoid(eleLineTax)) {
          eleLineTax = docChangeOrderRO.createElement(GCConstants.LINE_TAX);
          eleLineTax.setAttribute(GCConstants.CHARGE_CATEGORY, GCConstants.SHIPPING_REFUND);
          eleLineTax.setAttribute(GCConstants.CHARGE_NAME, GCConstants.SHIPPING_REFUND);
          eleLineTax.setAttribute(GCConstants.TAX_NAME, GCConstants.SHIPPING_REFUND_TAX);
          eleLineTax.setAttribute(GCConstants.TAX, sShippingRefundTax);
          eleLineTaxes.appendChild(eleLineTax);
        }
        Double dSOShippingLineCharge = Double.parseDouble(sSOShippingLineCharge);
        LOGGER.debug("Inside GCUpdateRefundShippingTax.GCUpdateRefundShippingTax:  sSOShippingLineTax:"
            + sSOShippingLineTax + " sSOShippingLineCharge:: " + sSOShippingLineCharge + " sShippingRefund:: "
            + sShippingRefund);

        if (dSOShippingLineCharge != 0) {
          Double dSOLineTaxPercentage = (Double.parseDouble(sSOShippingLineTax) * 100) / dSOShippingLineCharge;
          Double dShippingRefundTax = (dSOLineTaxPercentage * Double.parseDouble(sShippingRefund)) / 100;
          LOGGER.debug("Inside GCUpdateRefundShippingTax.GCUpdateRefundShippingTax:  dShippingRefundTax::"
              + dShippingRefundTax);

          sShippingRefundTax = dShippingRefundTax.toString();
          LOGGER.debug("Inside GCUpdateRefundShippingTax.GCUpdateRefundShippingTax:  sShippingRefundTax::"
              + sShippingRefundTax);

          BigDecimal a = new BigDecimal(sShippingRefundTax);
          //BigDecimal bROLineTax = a.setScale(2, BigDecimal.ROUND_HALF_EVEN);
          //For GCSTORE- 6649
          BigDecimal bROLineTax = a.setScale(2, BigDecimal.ROUND_DOWN);
          sShippingRefundTax = bROLineTax.toString();
          LOGGER.debug("sShippingRefundTax: "+sShippingRefundTax);
        }
        eleLineTax.setAttribute(GCConstants.TAX, sShippingRefundTax);
      }
    } catch (Exception e) {
      LOGGER.error("The exception is in orderAuditsForCommmision method:", e);
      throw new GCException(e);
    }

  }

}
