/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 * *###########################################
 * ######################################################
 * ################################################################ OBJECTIVE: This class is used to
 * update RO price by removing the promotions.
 * ######################################################
 * ############################################
 * ############################################################### Version Date Modified By
 * Description
 * ######################################################################################
 * ########################################################################### 1.0 16/05/2014 Gowda,
 * Naveen JIRA No: Description of issue. 1.1 03/03/2015 Soni, Karan OMS-4893: Component return uses
 * wrong price and throws an error - PMR 68065,227,000 1.2 27/04/2015 Singh, Gurpreet GCSTORE-2358:
 * Appeasement amount is not considered when return is created for partial qty. of an Orderline
 * #####
 * #############################################################################################
 * ###############################################################
 */
package com.gc.api;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:email Address">Naveen</a>
 */
public class GCUpdateReturnOrderPrice {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCUpdateReturnOrderPrice.class.getName());

  // Document docGetOrderInvoiceDetailListOp = null;

  /**
   *
   * Description of updateReturnOrderPrice
   *
   * @param env
   * @param inDoc
   * @throws Exception
   *
   */
  public static void updateReturnOrderPrice(YFSEnvironment env, Document inDoc) throws GCException {

    LOGGER.debug("Class: GCUpdateReturnOrderPrice Method: updateReturnOrderPrice BEGIN");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Incoming Docuemnt:" + GCXMLUtil.getXMLString(inDoc));
    }
    try {
      Document docGetOrderInvoiceDetailListOp = null;

      String sOrderType = inDoc.getDocumentElement().getAttribute(GCConstants.ORDER_TYPE);



      String strCOXml = (String)env.getTxnObject("CO_XML");
      LOGGER.debug("The  CO xmml at input is"+ strCOXml);
      Document docEleROOrder = null;

      if (YFCCommon.equalsIgnoreCase(sOrderType, "RTRN_WITH_ADVEXCHNG")) {
        docEleROOrder = inDoc;
      } else {

        docEleROOrder = GCXMLUtil.getDocument(strCOXml);
      }
      LOGGER.debug("The  CO xmm at input is transformed to Doc is"+ GCXMLUtil.getXMLString(docEleROOrder));
      String sOrderHeaderKey = GCXMLUtil.getAttributeFromXPath(inDoc,
          "//@OrderHeaderKey");
      String sDerivedOrdrHdrKey = GCXMLUtil.getAttributeFromXPath(inDoc,
          "//OrderLine/@DerivedFromOrderHeaderKey");
      Document docGetOrderDetailsIn = GCXMLUtil
          .createDocument(GCConstants.ORDER);
      Element eleRootGetOrdIn = docGetOrderDetailsIn.getDocumentElement();
      eleRootGetOrdIn.setAttribute(GCConstants.ORDER_HEADER_KEY,
          sDerivedOrdrHdrKey);
      eleRootGetOrdIn.setAttribute(GCConstants.DOCUMENT_TYPE,GCConstants.SALES_ORDER_DOCUMENT_TYPE);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("docGetSOOrderDetOut:"
            + GCXMLUtil.getXMLString(docGetOrderDetailsIn));
      }
      Document docGetSOOrderDetOut = null;
      if (!YFCCommon.isVoid(sDerivedOrdrHdrKey)) {
        docGetSOOrderDetOut = GCCommonUtil
            .invokeAPI(env, docGetOrderDetailsIn,
                GCConstants.GET_ORDER_LIST,
                "/global/template/api/getOrderListTempForUpdateROPrice.xml");
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("docGetSOOrderDetOut:"
              + GCXMLUtil.getXMLString(docGetSOOrderDetOut));
        }
        //CR
        docGetOrderInvoiceDetailListOp = callGetOrderInvoiceDetailListAPI(env, sDerivedOrdrHdrKey);
        //
      }

      Element eleOrder = GCXMLUtil.getElementByXPath(docGetSOOrderDetOut, "/OrderList/Order");
      Document docEleOrder = GCXMLUtil.getDocumentFromElement(eleOrder);

      NodeList nLOrderLineSO = GCXMLUtil.getNodeListByXpath(docEleOrder, "/Order/OrderLines/OrderLine");
      Map<String, Element> map = new HashMap<String, Element>();

      for (int i = 0; i < nLOrderLineSO.getLength(); i++) {
        Element eleOrderLineSO = (Element) nLOrderLineSO.item(i);
        String sOrderLineKey = eleOrderLineSO.getAttribute(GCConstants.ORDER_LINE_KEY);
        map.put(sOrderLineKey, eleOrderLineSO);
      }

      eleRootGetOrdIn.setAttribute(GCConstants.ORDER_HEADER_KEY,
          sOrderHeaderKey);
      eleRootGetOrdIn.setAttribute(GCConstants.DOCUMENT_TYPE,GCConstants.RETURN_ORDER_DOCUMENT_TYPE);

      Document docGetROOrderDetOut = null;
      if (!YFCCommon.isVoid(sDerivedOrdrHdrKey)) {
        docGetROOrderDetOut =
            GCCommonUtil.invokeAPI(env, docGetOrderDetailsIn, GCConstants.GET_ORDER_LIST,
                "/global/template/api/getOrderListTempForUpdateROPrice.xml");
      }

      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("docGetROOrderDetOut:" + GCXMLUtil.getXMLString(docEleROOrder));
      }
      NodeList nLOrderLineRO = GCXMLUtil.getNodeListByXpath(docEleROOrder, "/Order/OrderLines/OrderLine");
      for (int j = 0; j < nLOrderLineRO.getLength(); j++) {
        Element eleOrderLineRO = (Element) nLOrderLineRO.item(j);
        eleOrderLineRO.removeAttribute(GCConstants.LOCK_ID);
        Document docEleOrderLineRO = GCXMLUtil.getDocumentFromElement(eleOrderLineRO);
        String sOrderLineKey = eleOrderLineRO.getAttribute(GCConstants.ORDER_LINE_KEY);
        String sDerivedOrderLineKey = GCXMLUtil.getAttributeFromXPath(docEleOrderLineRO, "//@DerivedFromOrderLineKey");
        if (YFCCommon.isVoid(sDerivedOrderLineKey)) {
          sDerivedOrderLineKey = GCXMLUtil.getAttributeFromXPath(eleOrderLineRO, "DerivedFrom/@OrderLineKey");
        }
        Element eleLineOverallTotals =
            (Element) XPathAPI.selectSingleNode(docEleROOrder, "/Order/OrderLines/OrderLine[@OrderLineKey='"
                + sOrderLineKey + "']/LineOverallTotals");
        String sROOrderedQty = eleOrderLineRO.getAttribute(GCConstants.ORDERED_QTY);

        String sOrderedQtySO =
            GCXMLUtil.getAttributeFromXPath(docEleOrder, "//OrderLine[@OrderLineKey='" + sDerivedOrderLineKey
                + "']/@OrderedQty");
        String sUnitPrice =
            GCXMLUtil.getAttributeFromXPath(docEleOrder, "//OrderLine[@OrderLineKey='" + sDerivedOrderLineKey
                + "']/LinePriceInfo/@UnitPrice");
        Double dChargeAmount = 0.00;
        String sChargeAmount = "0.00";
        NodeList nLLinePrice =
            GCXMLUtil.getNodeListByXpath(docEleOrder, "/Order/OrderLines/OrderLine[@OrderLineKey='"
                + sDerivedOrderLineKey + "']/LineCharges/LineCharge[@ChargeCategory='Promotion']");

        for (int iCounter = 0; iCounter < nLLinePrice.getLength(); iCounter++) {
          Element eleLinePrice = (Element) nLLinePrice.item(iCounter);
          sChargeAmount = eleLinePrice.getAttribute(GCConstants.CHARGE_AMOUNT);
          dChargeAmount = dChargeAmount + Double.parseDouble(sChargeAmount);
        }

        LOGGER.debug("sOrderedQtySO:" + sOrderedQtySO + "sUnitPrice:" + sUnitPrice + "sChargeAmount:" + sChargeAmount);

        if (!(YFCCommon.isVoid(sUnitPrice) || YFCCommon.isVoid(sOrderedQtySO))) {
        	 /* 6729 Begin -Perform Round Up and then subtract, instead of Half round down later*/
        	//  Double dROUnitPrice = Double.parseDouble(sUnitPrice) - (dChargeAmount) / Double.parseDouble(sOrderedQtySO);

        	Double dDiscPerUnit = (dChargeAmount) / Double.parseDouble(sOrderedQtySO);
            String sDiscPerUnit = dDiscPerUnit.toString();
            BigDecimal a = new BigDecimal(sDiscPerUnit);
            BigDecimal bDiscPerUnit = a.setScale(2, BigDecimal.ROUND_UP); 
            Double dDiscPerUnitUpdated = bDiscPerUnit.doubleValue();
            LOGGER.debug("dDiscPerUnitUpdated" + dDiscPerUnitUpdated.toString());
            Double dROUnitPrice = Double.parseDouble(sUnitPrice) -dDiscPerUnitUpdated;
            String sROUnitPrice = dROUnitPrice.toString();
       
//          String sROUnitPrice = dROUnitPrice.toString();
//          BigDecimal a = new BigDecimal(sROUnitPrice);
//          BigDecimal bROUnitPrice = a.setScale(2, BigDecimal.ROUND_HALF_DOWN); // Fix for
//          // GCSTORE-5370
//          sROUnitPrice = bROUnitPrice.toString();
            /* 6729 End */
          LOGGER.debug("sROUnitPrice" + sROUnitPrice);
          Element eleLinePriceInfo =
              GCXMLUtil.getElementByXPath(docEleROOrder, "//OrderLine[@OrderLineKey='" + sOrderLineKey
                  + "']/LinePriceInfo");
          if (YFCCommon.isVoid(eleLinePriceInfo)) {
            eleLinePriceInfo = docEleROOrder.createElement(GCConstants.LINE_PRICE_INFO);
          }
          eleLinePriceInfo.setAttribute(GCConstants.UNIT_PRICE, sROUnitPrice);
          eleLinePriceInfo.setAttribute(GCConstants.IS_PRICE_LOCKED,
              "Y");
          eleOrderLineRO.appendChild(eleLinePriceInfo);
        }
        String sSOLineTax =
            GCXMLUtil.getAttributeFromXPath(docEleOrder, "//OrderLine[@OrderLineKey='" + sDerivedOrderLineKey
                + "']/LineOverallTotals/@Tax");
        if (YFCCommon.isVoid(sSOLineTax)) {
          sSOLineTax = "0.00";
        }
        String sSOLineShippingTax =
            GCXMLUtil.getAttributeFromXPath(docEleOrder, "//OrderLine[@OrderLineKey='" + sDerivedOrderLineKey
                + "']/LineTaxes/LineTax[@ChargeName='ShippingCharge']/@Tax");

        // Fix to add tax percentage on Return orderline.
        String sTaxRate =
            GCXMLUtil.getAttributeFromXPath(docEleOrder, "//OrderLine[@OrderLineKey='" + sDerivedOrderLineKey
                + "']/LineTaxes/LineTax[@ChargeName='LinePriceTax']/@TaxPercentage");
        // End

        if (YFCCommon.isVoid(sSOLineShippingTax)) {
          sSOLineShippingTax = "0.00";
        }

        LOGGER.debug("sROOrderedQty:" + sROOrderedQty + "sSOLineTax:" + sSOLineTax + "sSOLineShippingTax:"
            + sSOLineShippingTax + "sOrderedQtySO:" + sOrderedQtySO);
        Double dROLineTax =
            Double.parseDouble(sROOrderedQty)
            * (Double.parseDouble(sSOLineTax) - Double.parseDouble(sSOLineShippingTax))         
            / Double.parseDouble(sOrderedQtySO);
        String sROLineTax = dROLineTax.toString();
        BigDecimal a = new BigDecimal(sROLineTax);
        //BigDecimal bROLineTax = a.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        
        /*GCSTORE-6649 - Begin */
        BigDecimal bROLineTax = a.setScale(2, BigDecimal.ROUND_DOWN);
        /*GCSTORE-6649 - End */
        sROLineTax = bROLineTax.toString();
        LOGGER.debug("sROLineTax:" + sROLineTax);
        Element eleLineTaxes =
            GCXMLUtil.getElementByXPath(docEleROOrder, "//OrderLine[@OrderLineKey='" + sOrderLineKey + "']/LineTaxes");
        if (YFCCommon.isVoid(eleLineTaxes)) {
          eleLineTaxes = docEleROOrder.createElement(GCConstants.LINE_TAXES);
        }
        Element eleLineTax =
            GCXMLUtil.getElementByXPath(docEleROOrder, "//OrderLine[@OrderLineKey='" + sOrderLineKey
                + "']/LineTaxes/LineTax[@ChargeName='LinePriceTax']");
        if (YFCCommon.isVoid(eleLineTax)) {
          eleLineTax = docEleROOrder.createElement(GCConstants.LINE_TAX);
          eleLineTax.setAttribute(GCConstants.TAX, sROLineTax);
          eleLineTax.setAttribute(GCConstants.CHARGE_CATEGORY, GCConstants.LINE_PRICE);
          eleLineTax.setAttribute(GCConstants.CHARGE_NAME, GCConstants.LINE_PRICE_TAX);
          eleLineTax.setAttribute(GCConstants.TAX_NAME, GCConstants.LINE_PRICE_TAX);
          eleLineTax.setAttribute(GCConstants.TAX_PERCENTAGE, sTaxRate);
        }
        eleLineTax.setAttribute(GCConstants.TAX, sROLineTax);
        eleLineTaxes.appendChild(eleLineTax);
        eleOrderLineRO.appendChild(eleLineTaxes);
        if (!YFCCommon.isVoid(eleLineOverallTotals)) {
          eleOrderLineRO.removeChild(eleLineOverallTotals);
        }
        updateChangeOrderDocForAppeasement(docGetROOrderDetOut, eleOrderLineRO, docGetOrderInvoiceDetailListOp);

      }
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("The input xml for changeorder passed to component proration is  "+ GCXMLUtil.getXMLString(docEleROOrder));
      }
      //OMS-4893 Start
      docEleROOrder= GCCreateReturnOnSuccess.updateBundleUnitPrice(env, docGetROOrderDetOut, docEleROOrder);
      Element eleOLChangeOrder= GCXMLUtil.getElementByXPath(docEleROOrder, "Order/OrderLines/OrderLine");
      if(!YFCCommon.isVoid(eleOLChangeOrder)){
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("The input xml for changeorder is "+ GCXMLUtil.getXMLString(docEleROOrder));
        }
        env.setTxnObject("BundleTaxesProratedForReturn", GCConstants.YES);
        docEleROOrder.getDocumentElement().setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
        GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, docEleROOrder);
      }
      //OMS-4893 End
      LOGGER.debug("Class: GCUpdateReturnOrderPrice Method: updateReturnOrderPrice END");
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCUpdateReturnOrderPrice.updateReturnOrderPrice(), Exception is :",
          e);
      throw new GCException(e);
    }
  }

  /**
   *
   * @param env
   * @param sDerivedOrdrHdrKey
   */
  public static Document callGetOrderInvoiceDetailListAPI(YFSEnvironment env, String sDerivedOrdrHdrKey) {

    Document docGetOrderInvoiceDetailListIp = GCXMLUtil.createDocument(GCConstants.ORDER_INVOICE_DETAIL);
    Element eleRoot = docGetOrderInvoiceDetailListIp.getDocumentElement();

    Element eleInvoiceHeader = docGetOrderInvoiceDetailListIp.createElement(GCConstants.INVOICE_HEADER);
    eleRoot.appendChild(eleInvoiceHeader);
    eleInvoiceHeader.setAttribute(GCConstants.ORDER_HEADER_KEY, sDerivedOrdrHdrKey);


    
    Document docGetOrderInvoiceDetailListOp = GCCommonUtil.invokeAPI(env, docGetOrderInvoiceDetailListIp, GCConstants.GET_ORDER_INVOICE_DETAIL_LIST, "/global/template/api/getOrderInvoiceDetailListForAppeasement.xml");


    LOGGER.debug(
        "Class: GCUpdateReturnOrderPrice Method: callGetOrderInvoiceDetailListAPI END ,docGetOrderInvoiceDetailListOp: "
            + GCXMLUtil.getXMLString(docGetOrderInvoiceDetailListOp));

    return docGetOrderInvoiceDetailListOp;
  }

  /**
   *
   * @param docGetROOrderDetOut
   * @param eleOrderLineRO
   * @throws TransformerException
   */
  private static void updateChangeOrderDocForAppeasement(Document docGetROOrderDetOut, Element eleOrderLineRO,
      Document docGetOrderInvoiceDetailListOp) throws TransformerException {

    String sDerivedFromOrderLineKey = eleOrderLineRO.getAttribute(GCConstants.DERIVED_FROM_ORDER_LINE_KEY);
    String sOrderLineKey = eleOrderLineRO.getAttribute(GCConstants.ORDER_LINE_KEY);
    String sReturnedQty = eleOrderLineRO.getAttribute(GCConstants.ORDERED_QTY);
    double dReturnedQty = (!YFCCommon.isVoid(sReturnedQty)) ? Double.valueOf(sReturnedQty) : 0.00;
    Element eleLineChargeAppeasement =
        (Element) XPathAPI.selectSingleNode(docGetROOrderDetOut, "//Order/OrderLines/OrderLine[@OrderLineKey='"
            + sOrderLineKey
            + "']/LineCharges/LineCharge[@ChargeCategory='CUSTOMER_APPEASEMENT' and @ChargeName='CUSTOMER_APPEASEMENT']");

    // Fix for GCSTORE-5914--Starts
    // if (YFCCommon.isVoid(eleLineChargeAppeasement)) {
    // Fix for GCSTORE-5914--Ends
    NodeList nlLineCharge =
        XPathAPI.selectNodeList(docGetOrderInvoiceDetailListOp,
            "OrderInvoiceDetailList/OrderInvoiceDetail[@OrderLineKey='" + sDerivedFromOrderLineKey + "']/"
                + "LineCharges/LineCharge[@ChargeCategory='CUSTOMER_APPEASEMENT' and @ChargeName='CUSTOMER_APPEASEMENT']");
    String sQuantity =
        GCXMLUtil.getAttributeFromXPath(docGetOrderInvoiceDetailListOp,
            "OrderInvoiceDetailList/OrderInvoiceDetail[@OrderLineKey='" + sDerivedFromOrderLineKey + "']/@Quantity");
    if (!YFCCommon.isVoid(sQuantity) && nlLineCharge.getLength() > 0) {
      Double dQuanity = Double.parseDouble(sQuantity);
      Double dAppeasedAmount = 0.00;
      for (int i = 0; i < nlLineCharge.getLength(); i++) {
        Element elelLineCharge = (Element) nlLineCharge.item(i);
        String chargeAmount = elelLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT);
        dAppeasedAmount = dAppeasedAmount + Double.parseDouble(chargeAmount);
      }

      Double dAppeasedAmountPerUnit = dAppeasedAmount / dQuanity;
      String sAppeasedAmountPerUnit = dAppeasedAmountPerUnit.toString();

      BigDecimal bd = new BigDecimal(sAppeasedAmountPerUnit);
      BigDecimal bsAppeasedAmountPerUnit = bd.setScale(2,
          BigDecimal.ROUND_UP);
      sAppeasedAmountPerUnit = bsAppeasedAmountPerUnit.toString();
      if (Double.parseDouble(bsAppeasedAmountPerUnit.toString()) > 0) {
        Element eleLineCharges = (Element) eleOrderLineRO.getElementsByTagName(GCConstants.LINE_CHARGES).item(0);
        if (YFCCommon.isVoid(eleLineCharges)) {
          eleLineCharges = eleOrderLineRO.getOwnerDocument().createElement(GCConstants.LINE_CHARGES);
          eleOrderLineRO.appendChild(eleLineCharges);
        }
        Element eleLineCharge = eleOrderLineRO.getOwnerDocument().createElement(GCConstants.LINE_CHARGE);
        eleLineCharges.appendChild(eleLineCharge);
        eleLineCharge.setAttribute(GCConstants.CHARGE_PER_UNIT, sAppeasedAmountPerUnit);
        eleLineCharge.setAttribute(GCConstants.CHARGE_CATEGORY, GCConstants.CUSTOMER_APPEASEMENT);
        eleLineCharge.setAttribute(GCConstants.CHARGE_NAME, GCConstants.CUSTOMER_APPEASEMENT);
        // GCSTORE-2358 Start
        prorateAppeasementTaxes(eleOrderLineRO, sDerivedFromOrderLineKey, dQuanity, dReturnedQty,
            docGetOrderInvoiceDetailListOp);
        // GCSTORE-2358 End
      }
    }
    // Fix for GCSTORE-5914--Starts
    // }
    // Fix for GCSTORE-5914--Ends
    LOGGER.debug("Class: GCUpdateReturnOrderPrice Method: callGetOrderInvoiceDetailListAPI Ends ,eleOrderLineRO: "+GCXMLUtil.getElementString(eleOrderLineRO));
  }

  // GCSTORE-2358 Start
  private static void prorateAppeasementTaxes(Element eleOrderLineRO, String sDerivedFromOrderLineKey, Double dQuantity,
      double dReturnedQty, Document docGetOrderInvoiceDetailListOp) throws TransformerException {
    NodeList nlLineTaxes = XPathAPI.selectNodeList(docGetOrderInvoiceDetailListOp,
            "OrderInvoiceDetailList/OrderInvoiceDetail[@OrderLineKey='" + sDerivedFromOrderLineKey + "']/"
                + "LineTaxes/LineTax[@ChargeCategory='CUSTOMER_APPEASEMENT' and @ChargeName='CUSTOMER_APPEASEMENT']");

    Double dAppeasedTax = 0.00;
    for (int i = 0; i < nlLineTaxes.getLength(); i++) {
      Element elelLineTax = (Element) nlLineTaxes.item(i);
      String tax = elelLineTax.getAttribute(GCConstants.TAX);
      dAppeasedTax = dAppeasedTax + Double.parseDouble(tax);
    }

    Double dProratedAppeasedTax = (dAppeasedTax * dReturnedQty) / dQuantity;
    String sProratedAppeasedTax = dProratedAppeasedTax.toString();

    BigDecimal bd = new BigDecimal(sProratedAppeasedTax);
    BigDecimal bsAppeasedTaxPerUnit = bd.setScale(2, BigDecimal.ROUND_UP);
    sProratedAppeasedTax = bsAppeasedTaxPerUnit.toString();

    Element eleLineTaxes = (Element) eleOrderLineRO.getElementsByTagName(GCConstants.LINE_TAXES).item(0);
    if (YFCCommon.isVoid(eleLineTaxes)) {
      eleLineTaxes = eleOrderLineRO.getOwnerDocument().createElement(GCConstants.LINE_TAXES);
      eleOrderLineRO.appendChild(eleLineTaxes);
    }
    Element eleLineTax = eleOrderLineRO.getOwnerDocument().createElement(GCConstants.LINE_TAX);
    eleLineTaxes.appendChild(eleLineTax);
    eleLineTax.setAttribute(GCConstants.TAX, sProratedAppeasedTax);
    eleLineTax.setAttribute(GCConstants.CHARGE_CATEGORY, GCConstants.CUSTOMER_APPEASEMENT);
    eleLineTax.setAttribute(GCConstants.CHARGE_NAME, GCConstants.CUSTOMER_APPEASEMENT);
    eleLineTax.setAttribute(GCConstants.TAX_NAME, GCConstants.CUSTOMER_APPEASEMENT + "Tax");
  }
  // GCSTORE - 2358 End
}
