package com.gc.api;
/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################################
 *          OBJECTIVE: Contains the logic of stamping monitor date
 *###################################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *################################################################################################################################################################################
             1.0               01/04/2015        Ravi, Divya                Class which stamps GC_EXPECTED_SHIP_DATE
             1.1			   04/22/2015        Ravi, Divya                Stamping ship time
 *##################################################################################################################################################################################
 */
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;


/**
 * @author <a href="mailto:divya.ravi@expicient.com">Divya</a>
 */

public class GCUpdateShipMonitorDateOnConfirmShipment implements YIFCustomApi {

  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCProcessConfirmShipmentAPI.class.getName());

  private String monitorCutOffTime = "";

  public Document updateShipMonitorDate(YFSEnvironment env, Document inDoc)
      throws RemoteException, ParserConfigurationException, YIFClientCreationException, ParseException {

    LOGGER.debug(" Entering GCUpdateShipMonitorDateOnConfirmShipment.updateShipMonitorDate() method ");

    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    LOGGER.debug(" GCUpdateShipMonitorDateOnConfirmShipment.updateShipMonitorDate() method :: inDoc::" +  inputDoc);
    YFCElement shipElem = inputDoc.getDocumentElement();
    YFCDocument shipmentDocument = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement eleShipment = shipmentDocument.getDocumentElement();
    String shipNode = shipElem.getAttribute(GCConstants.SHIP_NODE);
    eleShipment.setAttribute(GCConstants.SHIPMENT_NO, shipElem.getAttribute(GCConstants.SHIPMENT_NO));
    eleShipment.setAttribute(GCConstants.ENTERPRISE_CODE, shipElem.getAttribute(GCConstants.ENTERPRISE_CODE));
    eleShipment.setAttribute(GCConstants.SELLER_ORGANIZATION_CODE, shipElem.getAttribute(GCConstants.SELLER_ORGANIZATION_CODE));
    eleShipment.setAttribute(GCConstants.SHIP_NODE, shipNode);
    YFCElement eleAdditionalDates = eleShipment.createChild("AdditionalDates");
    YFCElement eleAdditionalDate = eleAdditionalDates.createChild("AdditionalDate");
    eleAdditionalDate.setAttribute("DateTypeId", "GC_EXPECTED_SHIP_DATE");
    String expectedShipDateStamped = shipElem.getAttribute("ExpectedShipmentDate");

    // Input doc for getOrganizationList API
    YFCDocument getOrgListInput = YFCDocument.createDocument(GCConstants.ORGANIZATION);
    YFCElement orgListEle = getOrgListInput.getDocumentElement();
    orgListEle.setAttribute(GCConstants.ORGANIZATION_CODE, shipNode);
    YFCDocument getOrgListTemp = YFCDocument.getDocumentFor("<OrganizationList>"
        + "<Organization LocaleCode='' OrganizationCode='' PrimaryEnterpriseKey='' ><Node Localecode='' ShipNode='' TimeDiff='' >"
        + "<ShippingCalendar /></Node></Organization></OrganizationList>");
    // Call getOrganizationList
    YFCDocument getOrgListOutput = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORGANIZATION_LIST, getOrgListInput, getOrgListTemp);
    // fetch LocaleCode from getOrganizationList output
    YFCElement orgListOut = getOrgListOutput.getDocumentElement();
    YFCElement orgEle = orgListOut.getChildElement(GCConstants.ORGANIZATION);
    String localeCode = orgEle.getAttribute(GCConstants.LOCALE_CODE);

    //Fetch TimeZone for the Node.
    String nodeTimezone = GCDateUtils.getTimezoneForLocale(env, localeCode);

    //Fetch system timezone.
    TimeZone systemTimezone = TimeZone.getDefault();

    //Fetch the converted ESD in the node's timezone.
    String convertedTimeStamp = GCDateUtils.getQueryDate(expectedShipDateStamped, systemTimezone.getID(), nodeTimezone);

    //Fetch CalendarKey for the Node
    YFCElement nodeEle = orgEle.getChildElement(GCConstants.NODE);
    YFCElement shipCalendar = nodeEle.getChildElement(GCConstants.SHIPPING_CALENDAR);
    String calendarKey = "";
    if(!YFCCommon.isVoid(shipCalendar)){
      calendarKey = shipCalendar.getAttribute(GCConstants.CALENDAR_KEY);
    }

    //Check for WorkingDay and Shift
    int index = convertedTimeStamp.indexOf('T');
    int length = convertedTimeStamp.length();
    String monitorFromDate = convertedTimeStamp.substring(0, index);
    String monitorToTimeStamp = GCDateUtils.addDays(convertedTimeStamp, 7, 1);
    index  = monitorToTimeStamp.indexOf('T');
    String monitorToDate = monitorToTimeStamp.substring(0, index);
    String monitorTime = convertedTimeStamp.substring(index+1, length);
    //Call getCalendarDayDetails API
    if(!YFCCommon.isVoid(calendarKey)){
      YFCDocument getCalDayDetailsIndoc = YFCDocument.createDocument(GCConstants.CALENDAR);
      YFCElement calendarEle = getCalDayDetailsIndoc.getDocumentElement();
      calendarEle.setAttribute(GCConstants.CALENDAR_KEY, calendarKey);
      calendarEle.setAttribute(GCConstants.FROM_DATE, monitorFromDate);
      calendarEle.setAttribute(GCConstants.TO_DATE, monitorToDate);

      Document getCalDayDetailsOutput = GCCommonUtil.invokeAPI(env, GCConstants.GET_CALENDAR_DAY_DTLS, getCalDayDetailsIndoc.getDocument());
      YFCDocument getCalDayDetailsOutdoc = YFCDocument.getDocumentFor(getCalDayDetailsOutput);
      YFCElement calendarOutEle = getCalDayDetailsOutdoc.getDocumentElement();
      YFCElement datesEle = calendarOutEle.getChildElement(GCConstants.DATES);
      YFCElement monitordateEle = datesEle.getElementsByTagName(GCConstants.DATE).item(0);
      YFCElement shiftsEle = monitordateEle.getChildElement(GCConstants.SHIFTS);
      YFCElement shiftEle = shiftsEle.getChildElement(GCConstants.SHIFT);

      String validForDay = shiftEle.getAttribute(GCConstants.VALID_FOR_DAY);

      SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
      Date currentTime = timeFormat.parse(monitorTime);
      Date shiftEndTime = timeFormat.parse(monitorCutOffTime);
      double difference = currentTime.getTime() - shiftEndTime.getTime();

      if(difference<0){
        if(YFCUtils.equals(GCConstants.YES, validForDay)){
          eleShipment.setAttribute("ExpectedShipmentDate", monitorFromDate+ "T" +monitorCutOffTime);
          eleAdditionalDate.setAttribute("RequestedDate", monitorFromDate+ "T" +monitorCutOffTime);
        } else {
          for(int i=1; i<7; i++){
            YFCElement nextDayEle = datesEle.getElementsByTagName(GCConstants.DATE).item(i);
            String isWorkingDay = nextDayEle.getAttribute(GCConstants.VALID_FOR_DAY);
            if(YFCUtils.equals(GCConstants.YES, isWorkingDay)){
              String updatedExpShipDate = GCDateUtils.addDays(convertedTimeStamp, i, 1);
              int timeIndex = updatedExpShipDate.indexOf('T');
              String monitorDate = updatedExpShipDate.substring(0, timeIndex);
              eleShipment.setAttribute("ExpectedShipmentDate", monitorDate + "T" +monitorCutOffTime);
              eleAdditionalDate.setAttribute("RequestedDate", monitorDate+ "T" +monitorCutOffTime);
              break;
            }
          }
        }
      } else {
        for(int i=1; i<7; i++){
          YFCElement nextDayEle = datesEle.getElementsByTagName(GCConstants.DATE).item(i);
          String isWorkingDay = nextDayEle.getAttribute(GCConstants.VALID_FOR_DAY);
          if(YFCUtils.equals(GCConstants.YES, isWorkingDay)){
            String updatedExpShipDate = GCDateUtils.addDays(convertedTimeStamp, i, 1);
            int timeIndex = updatedExpShipDate.indexOf('T');
            String monitorDate = updatedExpShipDate.substring(0, timeIndex);
            eleShipment.setAttribute("ExpectedShipmentDate", monitorDate + "T" +monitorCutOffTime);
            eleAdditionalDate.setAttribute("RequestedDate", monitorDate+ "T" +monitorCutOffTime);
            break;
          }
        }
      }

      LOGGER.debug(" GCUpdateShipMonitorDateOnConfirmShipment.updateShipMonitorDate() method :: shipmentDocument::" +  shipmentDocument);
      GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_SHIPMENT, shipmentDocument.getDocument());

      LOGGER.debug(" Exiting GCUpdateShipMonitorDateOnConfirmShipment.updateShipMonitorDate() method ");

    }
    return inDoc;
  }



  @Override
  public void setProperties(Properties properties) {
    monitorCutOffTime = properties.getProperty("MonitorAlertTime");
  }
}
