/**Copyright 2014 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.

    Change Log
######################################################################################################################################
     OBJECTIVE: To validate manager approval process
######################################################################################################################################
        Version          CR                 Date                       Modified By                       Description
######################################################################################################################################
         1.0        Initial Version      12/19/2014                  Gowda, Naveen           This class is used to validate manager's
                                                                                              login credentials.
######################################################################################################################################
 */

package com.gc.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * GCSTORE-131 & 132 - This class is used during manager approval process. If credentials are valid
 * and sufficient manager'sExtnEmployeeID is fetched and returned in the response doc.
 *
 * @author <a href="mailto:naveen.s@expicient.com">Naveen</a>
 */

public class GCUserAuthentication implements YIFCustomApi {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCUserAuthentication.class.getName());

  /**
   * Description: This is the main method which implements manager login authentication.
   *
   * @param env
   * @param inDoc
   * @return
   * @throws GCException
   */
  public Document validateManagerCredentials(YFSEnvironment env, Document inputDoc) {

    LOGGER.beginTimer("GCUserAuthentication.validateManagerCredentials");
    LOGGER.verbose("Class: GCUserAuthentication Method: validateManagerCredentials BEGIN");

    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input XML to login API" + inDoc.toString());
    }
    YFCElement eleRoot = inDoc.getDocumentElement();
    String sUsername = eleRoot.getAttribute(GCConstants.LOGIN_ID);
    String sDomain = eleRoot.getAttribute(GCConstants.DOMAIN);
    String sIsStoreUser = eleRoot.getAttribute(GCConstants.IS_STORE_USER);

    /* Removing unwanted attributes in the input XML */
    eleRoot.removeAttribute(GCConstants.DOMAIN);
    eleRoot.removeAttribute(GCConstants.IS_STORE_USER);

    /* Appendind domain to username */
    sUsername = sDomain + "@" + sUsername;
    eleRoot.setAttribute(GCConstants.LOGIN_ID, sUsername);

    Element eleInDocRoot = inputDoc.getDocumentElement();

    callLoginAPI(env, inDoc);

    YFCDocument getUserHierarchyOp = callGetUserHeirarchyAPI(env, sUsername);
    YFCElement eleRootGetUser = getUserHierarchyOp.getDocumentElement();
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Output XML of getUserHierarchy API" + getUserHierarchyOp.toString());
    }
    YFCElement eleUserExtn = eleRootGetUser.getChildElement(GCConstants.EXTN);
    String sExtnEmployeeID = eleUserExtn.getAttribute(GCConstants.EXTN_EMPLOYEE_ID);
    String sUserName = eleRootGetUser.getAttribute("Username");
    if (!YFCCommon.isVoid(sExtnEmployeeID)) {
      eleInDocRoot.setAttribute(GCConstants.EXTN_EMPLOYEE_ID, sExtnEmployeeID);
      eleInDocRoot.setAttribute(GCConstants.USER_NAM, sUserName);
    }
    YFCDocument getCommonCodeOp = null;

    /* Calling getCommonCodeList to fetch the valid values for UserGroupId */

    if (YFCCommon.equalsIgnoreCase(GCConstants.TRUE, sIsStoreUser)) {
      getCommonCodeOp = callGetCommonCodeListAPI(env, "GCPriceOverrideStore");
    } else {
      getCommonCodeOp = callGetCommonCodeListAPI(env, "GCPriceOverrideCSR");
    }

    /* Validate if the user credentials are of manager or not */
    validateIfSufficientCredentials(getUserHierarchyOp, getCommonCodeOp);

    LOGGER.verbose("Return XML from GCUserAuthentication" + GCXMLUtil.getXMLString(inputDoc));
    LOGGER.verbose("Class: GCUserAuthentication Method: validateManagerCredentials END");
    LOGGER.endTimer("GCUserAuthentication.validateManagerCredentials");
    return inputDoc;

  }

  /**
   * Description: Based on the user type, common codes are matched with UserGroupID
   *
   * @param getCommonCodeOp
   * @param getUserHierarchyOp
   *
   */
  private void validateIfSufficientCredentials(YFCDocument getUserHierarchyOp, YFCDocument getCommonCodeOp) {

    LOGGER.beginTimer("GCUserAuthentication.validateIfSufficientCredentials");
    LOGGER.verbose("Class: GCUserAuthentication Method: validateIfSufficientCredentials BEGIN");

    Boolean isManager = false;

    YFCNodeList<YFCElement> listUserGroup = getUserHierarchyOp.getElementsByTagName(GCConstants.USER_GROUP);

    int lengthUserGroup = listUserGroup.getLength();
    List<String> listManagerGroupId = new ArrayList<String>();
    YFCNodeList<YFCElement> listCommonCode = getCommonCodeOp.getElementsByTagName(GCConstants.COMMON_CODE);

    int ilengthCommonCode = listCommonCode.getLength();
    for (int iCounterCommonCode = 0; iCounterCommonCode < ilengthCommonCode; iCounterCommonCode++) {
      YFCElement eleCommonCode = listCommonCode.item(iCounterCommonCode);
      String sCodeValue = eleCommonCode.getAttribute(GCConstants.CODE_VALUE);
      listManagerGroupId.add(sCodeValue);

    }

    for (int iCounterUserGroup = 0; iCounterUserGroup < lengthUserGroup && !isManager; iCounterUserGroup++) {
      YFCElement eleUserGroup = listUserGroup.item(iCounterUserGroup);
      String sUserGroupId = eleUserGroup.getAttribute(GCConstants.USER_GROUP_ID);
      if (listManagerGroupId.contains(sUserGroupId)) {
        isManager = true;
      }

    }

    if (!isManager) {
      YFCException yfs = new YFCException(GCErrorConstants.INSUFFICIENT_CREDENTIAL_ERROR_CODE);
      yfs.setErrorDescription(GCErrorConstants.INSUFFICIENT_CREDENTIAL_ERROR_DESC);
      throw yfs;
    }
    LOGGER.verbose("Class: GCUserAuthentication Method: validateIfSufficientCredentials END");
    LOGGER.endTimer("GCUserAuthentication.validateIfSufficientCredentials");

  }

  /**
   *
   * @param gcStoreGroup
   * @param env
   * @return
   */
  private YFCDocument callGetCommonCodeListAPI(YFSEnvironment env, String gcStoreGroup) {

    LOGGER.beginTimer("GCUserAuthentication.callGetCommonCodeListAPI");
    LOGGER.verbose("Class: GCUserAuthentication Method: callGetCommonCodeListAPI BEGIN");

    YFCDocument docGetCommonCodeIp = YFCDocument.createDocument(GCConstants.COMMON_CODE);
    YFCElement eleRoot = docGetCommonCodeIp.getDocumentElement();

    eleRoot.setAttribute(GCConstants.CODE_TYPE, gcStoreGroup);
    YFCDocument docGetCommonCodeListTemp = YFCDocument.getDocumentFor(GCConstants.COMMON_CODE_TEMPLATE);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input XML of getCommonCodeList API" + docGetCommonCodeIp.toString());
    }

    YFCDocument docGetCommonCdeOp =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_COMMON_CODE_LIST, docGetCommonCodeIp, docGetCommonCodeListTemp);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Output XML of getCommonCodeList API" + docGetCommonCdeOp.toString());
    }
    LOGGER.verbose("Class: GCUserAuthentication Method: callGetCommonCodeListAPI END");
    LOGGER.endTimer("GCUserAuthentication.callGetCommonCodeListAPI");
    return docGetCommonCdeOp;

  }

  /**
   * Description - Calling getUserHeirarchy API for the user.
   *
   * @param env
   * @param sUsername
   * @return
   */
  private YFCDocument callGetUserHeirarchyAPI(YFSEnvironment env, String sUsername) {

    LOGGER.beginTimer("GCUserAuthentication.callGetUserHeirarchyAPI");
    LOGGER.verbose("Class: GCUserAuthentication Method: callGetUserHeirarchyAPI BEGIN");

    YFCDocument docGetUserHierarchyIn = YFCDocument.createDocument(GCConstants.USER);
    YFCElement eleRoot = docGetUserHierarchyIn.getDocumentElement();
    eleRoot.setAttribute(GCConstants.LOGINID, sUsername);

    YFCDocument docGetUserHierarchyTemp = YFCDocument.getDocumentFor(GCConstants.GET_USER_HEIRARCHY_TEMPLATE);
    YFCDocument getUserHierarchyOp =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_USER_HEIRARCHY_API, docGetUserHierarchyIn, docGetUserHierarchyTemp);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Output XML of getUserHierarchy API" + getUserHierarchyOp.toString());
    }

    LOGGER.verbose("Class: GCUserAuthentication Method: callGetUserHeirarchyAPI END");
    LOGGER.endTimer("GCUserAuthentication.callGetUserHeirarchyAPI");

    return getUserHierarchyOp;

  }

  /**
   * Description: This method is used to invoke loginAPI.
   *
   * @param env
   * @param inDoc
   */
  private YFCDocument callLoginAPI(YFSEnvironment env, YFCDocument inDoc) {

    LOGGER.beginTimer("GCUserAuthentication.callLoginAPI");
    LOGGER.verbose("Class: GCUserAuthentication Method: callLoginAPI BEGIN");
    YFCDocument docLoginOp = null;
    YFCDocument docLoginTemplate = YFCDocument.getDocumentFor(GCConstants.COMMON_CODE_TEMPLATE);

    try {
      docLoginOp = GCCommonUtil.invokeAPI(env, GCConstants.LOGIN_API, inDoc, docLoginTemplate);
    } catch (Exception e) {

      LOGGER.error("The exception is in callLoginAPI method:", e);
      YFCException yfs = new YFCException(GCErrorConstants.INVALID_CREDENTIAL_ERROR_CODE);
      yfs.setErrorDescription(GCErrorConstants.INVALID_CREDENTIAL_ERROR_DESC);
      throw yfs;
    }

    LOGGER.verbose("Class: GCUserAuthentication Method: callLoginAPI END");
    LOGGER.endTimer("GCUserAuthentication.callLoginAPI");
    return docLoginOp;

  }

  /**
   * Description: This method is used to fetch values from service args.
   *
   * @param arg0
   * @throws Exception
   */
  @Override
  public void setProperties(Properties prop) {

  }
}
