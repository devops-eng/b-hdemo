/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Validate the time period of buyer remorse hold and resolve the hold
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               19/02/2014        Singh, Gurpreet            Resolve holds with period
             2.0               28/05/2015        Singh, Gurpreet            GCSTORE-2514: Line Level Taxes (CR-022)
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="gurpreet.singh@expicient.com">Gurpreet</a>
 */
public class GCValidateAndResolveHoldsWithPeriod implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCReserveInventoryService.class);

  /**
   *
   * Description of validateHoldPeriod This method will call the respective
   * methods to holds
   *
   * @param env
   * @param inDoc
   * @throws GCException
   *
   */
  public void validateHoldPeriod(YFSEnvironment env, Document inDoc)
      throws GCException {
    LOGGER.debug(" Entering GCValidateAndResolveHoldsWithPeriod.validateHoldPeriod() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" validateHoldPeriod() method, Input document is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    try {
      Element eleBuyerRemorseHold =
          (Element) XPathAPI.selectSingleNode(inDoc,
              "//OrderHoldType[@HoldType='BUYER_REMORSE_HOLD' and @Status='1100']");
      if (!YFCCommon.isVoid(eleBuyerRemorseHold)) {
        validateRemorsePeriod(env, inDoc);
      }
      // GCSTORE-2514 : Start
      Element eleVertexCallFailedHold =
          (Element) XPathAPI.selectSingleNode(inDoc,
              "//OrderHoldType[@HoldType='VERTEX_CALL_FAILED' and @Status='1100']");
      if (!YFCCommon.isVoid(eleVertexCallFailedHold)) {
        LOGGER.debug(" If Vertex Call Failed Hold exists ");
        validateAndResolveVertexCallFailedHold(env, inDoc);
      }
      // GCSTORE-2514 : End
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCValidateAndResolveHoldsWithPeriod.validateHoldPeriod()",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCValidateAndResolveHoldsWithPeriod.validateHoldPeriod() method ");
  }

  /**
   *
   * Description of validateRemorsePeriod Resolve the Buyer Remorse Hold if
   * the time period for this hold is passed Time period is fetched from a
   * common code
   *
   * @param env
   * @param inDoc
   * @throws Exception
   *
   */
  private static void validateRemorsePeriod(YFSEnvironment env, Document inDoc)
      throws GCException {
    LOGGER.debug(" Entering GCValidateAndResolveHoldsWithPeriod.validateRemorsePeriod() method ");
    try {
      Element eleOrder = inDoc.getDocumentElement();
      String sOrderHeaderKey = eleOrder
          .getAttribute(GCConstants.ORDER_HEADER_KEY);
      LOGGER.debug("sOrderHeaderKey is : " + sOrderHeaderKey);

      // Create changeOrderDoc
      Document docChangeOrderInput = GCXMLUtil
          .createDocument(GCConstants.ORDER);
      Element eleChangeOrderRootElement = docChangeOrderInput
          .getDocumentElement();
      eleChangeOrderRootElement.setAttribute(
          GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
      eleChangeOrderRootElement.setAttribute(GCConstants.OVERRIDE,
          GCConstants.YES);

      // Fetch OrderDate.
      Date orderDate = new SimpleDateFormat(GCConstants.DATE_TIME_FORMAT)
      .parse(eleOrder.getAttribute(GCConstants.ORDER_DATE));

      // Create XML for getCommonCodeList and Enter Attributes Values
      Document docGetCommonCodeListInput = GCXMLUtil
          .createDocument(GCConstants.COMMON_CODE);
      docGetCommonCodeListInput.getDocumentElement().setAttribute(
          GCConstants.CODE_TYPE, GCConstants.BUYER_REMORSE_PERIOD);
      docGetCommonCodeListInput.getDocumentElement().setAttribute(
          GCConstants.ORGANIZATION_CODE,
          eleOrder.getAttribute(GCConstants.ENTERPRISE_CODE));
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("docGetCommonCodeListInput:"
            + GCXMLUtil.getXMLString(docGetCommonCodeListInput));
      }
      // Invoke getCommonCodeList
      Document docGetCommonCodeListOutput = GCCommonUtil.invokeAPI(env,
          GCConstants.API_GET_COMMON_CODE_LIST,
          docGetCommonCodeListInput, null);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("docGetCommonCodeListOutput: "
            + GCXMLUtil.getXMLString(docGetCommonCodeListOutput));
      }

      String sRemorsePeriod = GCXMLUtil.getAttributeFromXPath(
          docGetCommonCodeListOutput,
          "CommonCodeList/CommonCode/@CodeValue");
      Integer intRemorsePeriod = (!YFCCommon.isVoid(sRemorsePeriod)) ? Integer
          .parseInt(sRemorsePeriod) : 0;
          LOGGER.debug("Remorse Period:" + intRemorsePeriod);

          // Date manipulation
          Calendar calSysDate = Calendar.getInstance();
          Calendar calRemorseDate = Calendar.getInstance();

          calRemorseDate.setTime(orderDate);
          calRemorseDate.add(Calendar.MINUTE, intRemorsePeriod);

          // Check if Buyer Remorse Period is over.
          if (calSysDate.getTime().compareTo(calRemorseDate.getTime()) > 0) {
            LOGGER.debug("Remorse Period for order is over");

            Element eleOrderHoldTypes = docChangeOrderInput
                .createElement(GCConstants.ORDER_HOLD_TYPES);
            Element eleOrderHoldType = docChangeOrderInput
                .createElement(GCConstants.ORDER_HOLD_TYPE);

            eleOrderHoldType.setAttribute(GCConstants.HOLD_TYPE,
                GCConstants.BUYER_REMORSE_HOLD);
            eleOrderHoldType.setAttribute(GCConstants.STATUS,
                GCConstants.RESOLVE_HOLD_CODE);

            eleOrderHoldTypes.appendChild(eleOrderHoldType);
            eleChangeOrderRootElement.appendChild(eleOrderHoldTypes);
            if (LOGGER.isDebugEnabled()) {
              LOGGER.debug("Change order Output XML: "
                  + GCXMLUtil.getXMLString(docChangeOrderInput));
            }
            GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER,
                docChangeOrderInput, null);
          }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCValidateAndResolveHoldsWithPeriod.validateRemorsePeriod()",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCValidateAndResolveHoldsWithPeriod.validateRemorsePeriod() method ");
  }


  /**
   * Invoke vertex and if the response is successful, resolve the hold and update taxes, if not
   * update LastHoldTypeDate to current date
   *
   * @param env
   * @param inDoc
   * @throws GCException
   */
  private void validateAndResolveVertexCallFailedHold(YFSEnvironment env, Document inDoc) throws GCException {
    LOGGER.beginTimer("GCValidateAndResolveHoldsWithPeriod.validateAndResolveVertexCallFailedHold");
    LOGGER.verbose("Class: GCValidateAndResolveHoldsWithPeriod Method: validateAndResolveVertexCallFailedHold START");
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" validateAndResolveVertexCallFailedHold() method, inputDoc is" + inputDoc.toString());
    }
    YFCElement eleOrder = inputDoc.getDocumentElement();
    YFCElement eleOrderHoldTypes = eleOrder.getChildElement(GCXmlLiterals.HOLD_TYPES_TO_PROCESS);
    YFCElement eleOrderHoldType = eleOrderHoldTypes.getChildElement(GCConstants.ORDER_HOLD_TYPE);
    Date dLastHoldTypeDate = eleOrderHoldType.getDateAttribute(GCXmlLiterals.LAST_HOLD_TYPE_DATE);
    String sOrderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
    LOGGER.verbose("sOrderHeaderKey" + sOrderHeaderKey);
    // Create XML for getCommonCodeList and Enter Attributes Values
    Document docGetCommonCodeListInput = GCXMLUtil.createDocument(GCConstants.COMMON_CODE);
    docGetCommonCodeListInput.getDocumentElement().setAttribute(GCConstants.CODE_TYPE,
        GCXmlLiterals.VERTEX_RETRY_PERIOD);
    docGetCommonCodeListInput.getDocumentElement().setAttribute(GCConstants.ORGANIZATION_CODE,
        eleOrder.getAttribute(GCConstants.ENTERPRISE_CODE));
    if (LOGGER.isDebugEnabled()) {
      LOGGER.verbose("docGetCommonCodeListInput:" + GCXMLUtil.getXMLString(docGetCommonCodeListInput));
    }
    // Invoke getCommonCodeList
    Document docGetCommonCodeListOutput =
        GCCommonUtil.invokeAPI(env, GCConstants.API_GET_COMMON_CODE_LIST, docGetCommonCodeListInput, null);
    if (LOGGER.isDebugEnabled()) {
      LOGGER.verbose("docGetCommonCodeListOutput: " + GCXMLUtil.getXMLString(docGetCommonCodeListOutput));
    }
    String sRetryPeriod =
        GCXMLUtil.getAttributeFromXPath(docGetCommonCodeListOutput, "CommonCodeList/CommonCode/@CodeValue");
    Integer intRetryPeriod = (!YFCCommon.isVoid(sRetryPeriod)) ? Integer.parseInt(sRetryPeriod) : 0;
    LOGGER.verbose("Retry Period:" + intRetryPeriod);

    // Date manipulation
    Calendar calSysDate = Calendar.getInstance();
    Calendar calLastHoldTypeDate = Calendar.getInstance();

    calLastHoldTypeDate.setTime(dLastHoldTypeDate);
    calLastHoldTypeDate.add(Calendar.MINUTE, intRetryPeriod);
    if (calSysDate.getTime().compareTo(calLastHoldTypeDate.getTime()) > 0) {
      LOGGER.verbose(" Inside if current date is greater than updated calLastHoldTypeDate");
      Document docGetOrderListTmp =GCCommonUtil.getDocumentFromPath("/global/template/api/getOrderListForVertexCall.xml");
      Document docGetOrderListOP = GCCommonUtil.invokeGetOrderListAPI(env, sOrderHeaderKey, docGetOrderListTmp);
      Element eOrder = GCXMLUtil.getElementByXPath(docGetOrderListOP, "OrderList/Order");
      Document updatedDoc = GCXMLUtil.getDocumentFromElement(eOrder);
      Document updatedDocWithTaxes =GCCommonUtil.invokeService(env, GCConstants.GC_PROCESS_TAX_CALCULATIONS_SERVICE, updatedDoc);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose(" validateAndResolveVertexCallFailedHold() method, updatedDoc is" + updatedDoc.toString());
      }

      processUpdatedDocWithTaxes(env, updatedDocWithTaxes, sOrderHeaderKey);
    }
    LOGGER.verbose("Class: GCValidateAndResolveHoldsWithPeriod Method: validateAndResolveVertexCallFailedHold END");
    LOGGER.endTimer("GCValidateAndResolveHoldsWithPeriod.validateAndResolveVertexCallFailedHold");
  }

  /**
   * Invoke change orde rPAI to update LastHoldTypeDate as current date
   *
   * @param env
   * @param inDoc
   * @param sOrderHeaderKey
   */
  private void callChangeOrderToUpdateHoldDate(YFSEnvironment env, String sOrderHeaderKey) {
    LOGGER.beginTimer("GCValidateAndResolveHoldsWithPeriod.callChangeOrderToUpdateHoldDate");
    LOGGER.verbose("Class: GCValidateAndResolveHoldsWithPeriod Method: callChangeOrderToUpdateHoldDate START");
    Document docChangeOrderInput = GCXMLUtil.createDocument(GCConstants.ORDER);
    Element eleChangeOrderRootElement = docChangeOrderInput.getDocumentElement();
    eleChangeOrderRootElement.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    Element eleOrderHoldTypes = docChangeOrderInput.createElement(GCConstants.ORDER_HOLD_TYPES);
    Element eleOrderHoldType = docChangeOrderInput.createElement(GCConstants.ORDER_HOLD_TYPE);
    Calendar calSysDate = Calendar.getInstance();
    String strCurentDate = new SimpleDateFormat(GCConstants.DATE_TIME_FORMAT).format(calSysDate.getTime());
    eleOrderHoldType.setAttribute(GCConstants.HOLD_TYPE, GCXmlLiterals.VERTEX_CALL_FAILED);
    eleOrderHoldType.setAttribute(GCConstants.STATUS, GCConstants.APPLY_HOLD_CODE);
    eleOrderHoldType.setAttribute(GCXmlLiterals.LAST_HOLD_TYPE_DATE, strCurentDate);
    eleOrderHoldTypes.appendChild(eleOrderHoldType);
    eleChangeOrderRootElement.appendChild(eleOrderHoldTypes);
    if (LOGGER.isDebugEnabled()) {
      LOGGER.verbose("Change order Output XML: " + GCXMLUtil.getXMLString(docChangeOrderInput));
    }
    GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, docChangeOrderInput, null);
    LOGGER.verbose("Class: GCValidateAndResolveHoldsWithPeriod Method: callChangeOrderToUpdateHoldDate END");
    LOGGER.endTimer("GCValidateAndResolveHoldsWithPeriod.callChangeOrderToUpdateHoldDate");
  }

  private void processUpdatedDocWithTaxes(YFSEnvironment env, Document updatedDocWithTaxes, String sOrderHeaderKey)
      throws GCException {
    LOGGER.beginTimer("GCValidateAndResolveHoldsWithPeriod.processUpdatedDocWithTaxes");
    LOGGER.verbose("Class: GCValidateAndResolveHoldsWithPeriod Method: processUpdatedDocWithTaxes START");
    Element eOrder = updatedDocWithTaxes.getDocumentElement();
    YFCDocument inputDoc = YFCDocument.getDocumentFor(updatedDocWithTaxes);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" processUpdatedDocWithTaxes() method, inputDoc is" + inputDoc.toString());
    }
    YFCElement eleOrder = inputDoc.getDocumentElement();
    YFCElement eleLineTax = eleOrder.getElementsByTagName(GCConstants.LINE_TAX).item(0);
    String sReference3 = eleLineTax.getAttribute(GCConstants.REFERENCE3);
    if (YFCCommon.isVoid(sReference3)) {
      sReference3 = eleLineTax.getAttribute(GCConstants.REFERENCE_3);
    }
    LOGGER.verbose("sReference3 is" + sReference3);
    YFCNodeList<YFCElement> nlHeaderTax = eleOrder.getElementsByTagName(GCConstants.HEADER_TAX);
    YFCElement eleTotalTax = null;
    for (YFCElement eleHeaderTax : nlHeaderTax) {
      LOGGER.verbose(" Looping on header tax elements ");
      String sChargeCategory = eleHeaderTax.getAttribute(GCConstants.CHARGE_CATEGORY);
      if (GCXmlLiterals.TOTAL_TAX.equalsIgnoreCase(sChargeCategory)) {
        LOGGER.verbose(" Inside if charge category is TotalTax ");
        eleTotalTax = eleHeaderTax;
        break;
      }
    }
    if (YFCCommon.equals(GCConstants.ERROR, sReference3)) {
      LOGGER.verbose(" Inside if Reference3 is Error ");
      if (!YFCCommon.isVoid(eleTotalTax)) {
        LOGGER.verbose(" Inside if refernce 3 is error ");
        callChangeOrderToUpdateHoldDate(env, sOrderHeaderKey);
      }
    } else {
      double dTotalTax = eleTotalTax.getDoubleAttribute(GCConstants.TAX);
      double dCalculatedTax = calculateOrderTotalTax(eleOrder);
      LOGGER.verbose(" dTotalTax is " + dTotalTax + " dCalculatedTax is " + dCalculatedTax);
      removeHeaderTaxElement(updatedDocWithTaxes);
      if (dCalculatedTax > dTotalTax) {
        LOGGER.verbose(" Inside if dCalculatedTax is greater than dTotalTax ");
        YFCElement elePaymentMethods=eleOrder.getChildElement(GCConstants.PAYMENT_METHODS);
        YFCElement elePaymentMethod = elePaymentMethods.getElementsByTagName(GCConstants.PAYMENT_METHOD).item(0);
        String sPaymentType = elePaymentMethod.getAttribute(GCConstants.PAYMENT_TYPE);
        double dIncreasedTax = dCalculatedTax - dTotalTax;
        if (YFCCommon.equals(GCConstants.BML, sPaymentType) || YFCCommon.equals(GCConstants.PAYPAL, sPaymentType)) {
          LOGGER.verbose(" If PaymentType is PAYPAL or BML, then raise an alert ");
          LOGGER.verbose(" dIncreasedTax is " + dIncreasedTax);
          eOrder.setAttribute("IncreasedTax", String.valueOf(dIncreasedTax));
          GCCommonUtil.invokeService(env, GCConstants.GC_RAISE_ALERT_FOR_NEW_AUTH_SERVICE, updatedDocWithTaxes);
          eOrder.removeAttribute("IncreasedTax");
        } else {
          LOGGER.verbose("Inside else, increase request amount");
          double dMaxChargeLimit = elePaymentMethod.getDoubleAttribute(GCConstants.MAX_CHARGE_LIMIT);
          dMaxChargeLimit = dMaxChargeLimit + dIncreasedTax;
          String sPaymentKey=elePaymentMethod.getAttribute(GCConstants.PAYMENT_KEY);
          elePaymentMethods.removeChild(elePaymentMethod);
          YFCElement elePaymentMethodNew = elePaymentMethods.createChild(GCConstants.PAYMENT_METHOD);
          elePaymentMethodNew.setAttribute(GCConstants.PAYMENT_KEY,sPaymentKey );
          elePaymentMethodNew.setAttribute(GCConstants.MAX_CHARGE_LIMIT, dMaxChargeLimit);
          /*
           * YFCElement elePaymentDetails =
           * elePaymentMethod.createChild(GCXmlLiterals.PAYMENT_DETAILS);
           * elePaymentDetails.setAttribute(GCConstants.PAYMENT_REQUEST_AMOUNT, dIncreasedTax);
           * elePaymentDetails.setAttribute(GCConstants.CHARGE_TYPE, GCConstants.AUTHORIZATION);
           * elePaymentMethod.appendChild(elePaymentDetails);
           */
        }
      }
      Element eOrderHoldTypes = updatedDocWithTaxes.createElement(GCConstants.ORDER_HOLD_TYPES);
      Element eOrderHoldType = updatedDocWithTaxes.createElement(GCConstants.ORDER_HOLD_TYPE);
      eOrderHoldType.setAttribute(GCConstants.HOLD_TYPE, GCXmlLiterals.VERTEX_CALL_FAILED);
      eOrderHoldType.setAttribute(GCConstants.STATUS, GCConstants.RESOLVE_HOLD_CODE);
      eOrderHoldTypes.appendChild(eOrderHoldType);
      updatedDocWithTaxes.getDocumentElement().appendChild(eOrderHoldTypes);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose(" validateAndResolveVertexCallFailedHold() method, updatedDocWithTaxes is"
            + updatedDocWithTaxes.toString());
      }
      GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, updatedDocWithTaxes);
      GCCommonUtil.invokeService(env, GCConstants.GC_ORDER_CONFIRMATION_EMAIL_SERVICE, updatedDocWithTaxes);
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" processUpdatedDocWithTaxes() method, updated inputDoc is" + inputDoc.toString());
    }
    LOGGER.verbose("Class: GCValidateAndResolveHoldsWithPeriod Method: processUpdatedDocWithTaxes END");
    LOGGER.endTimer("GCValidateAndResolveHoldsWithPeriod.processUpdatedDocWithTaxes");
  }

  // GCSTORE-2514 : Start
  /**
   *
   * @param eleOrder
   * @return
   */
  public double calculateOrderTotalTax(YFCElement eleOrder) {
    LOGGER.beginTimer("GCBeforeCreateOrderUE.calculateOrderTotalTax");
    LOGGER.verbose("Class: GCBeforeCreateOrderUE Method: calculateOrderTotalTax START");
    YFCNodeList<YFCElement> nlLineTax = eleOrder.getElementsByTagName(GCConstants.LINE_TAX);
    double dCalculatedTax = 0.00;
    for (YFCElement eleLineTax : nlLineTax) {
      double dTax = eleLineTax.getDoubleAttribute(GCConstants.TAX);
      dCalculatedTax = dCalculatedTax + dTax;
    }
    LOGGER.verbose("Class: GCBeforeCreateOrderUE Method: calculateOrderTotalTax END");
    LOGGER.endTimer("GCBeforeCreateOrderUE.calculateOrderTotalTax");
    return dCalculatedTax;
  }

  // GCSTORE-2514 : End
  // GCSTORE-2514 : Start
  /**
   * This method removes the header tax element from input document having charge category TotalTax
   *
   * @param inDoc
   */
  public void removeHeaderTaxElement(Document inDoc) {
    LOGGER.verbose("Entering GCBeforeCreateOrderUE.removeHeaderTaxElement()");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.verbose(" inDoc is" + GCXMLUtil.getXMLString(inDoc));
    }
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleOrder = inputDoc.getDocumentElement();
    YFCElement eleHeaderTaxes = eleOrder.getChildElement(GCConstants.HEADER_TAXES);
    boolean bTotalTaxExists = false;
    if (!YFCCommon.isVoid(eleHeaderTaxes)) {
      YFCNodeList<YFCElement> nlHeaderTax = eleHeaderTaxes.getElementsByTagName(GCConstants.HEADER_TAX);
      for (YFCElement eleHeaderTax : nlHeaderTax) {
        String sChargeCategory = eleHeaderTax.getAttribute(GCConstants.CHARGE_CATEGORY);
        if (GCXmlLiterals.TOTAL_TAX.equalsIgnoreCase(sChargeCategory)) {
          LOGGER.verbose(" removeHeaderTaxElement(), Inside if charge category is total tax");
          bTotalTaxExists = true;
          break;
        }
      }
    }
    if (bTotalTaxExists) {
      eleOrder.removeChild(eleHeaderTaxes);
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" removeHeaderTaxElement() method, updated inputDoc is" + inputDoc.toString());
    }
    LOGGER.verbose("Exiting from GCBeforeCreateOrderUE.removeHeaderTaxElement()");
  }

  // GCSTORE-2514 : End
  @Override
  public void setProperties(Properties arg0) throws GCException {

  }

}
