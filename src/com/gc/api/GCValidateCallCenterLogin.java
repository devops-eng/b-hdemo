/**Copyright 2013 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.

	Change Log
#####################################################################################################################################################################################################################
     OBJECTIVE: Used to Validate the Call Center Manager Approval and User Authentication
######################################################################################################################################################################################################################
        Version    CR/ User Story                 Date                       Modified By                   Description
######################################################################################################################################################################################################################
        1.0         GCSTORE-790                   03/13/2015                 Jain,Shubham          Used to Validate the Call Center Manager Approval and User Authentication
######################################################################################################################################################################################################################
 */
package com.gc.api;

import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * THis class is used to Validate the Call Center Manager Approval and User
 * Authentication.
 * 
 * @author <a href="mailto:shubham.jain@expicient.com">Shubham Jain</a>
 */
public class GCValidateCallCenterLogin implements YIFCustomApi {

	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCValidateCallCenterLogin.class.getName());

	/**
	 * This Method is used to Validate the Call Center Manager Approval and User
	 * Authentication.
	 * 
	 * @param yfsEnv
	 * @param inDoc
	 * @return
	 */

	public Document validateCCLogin(YFSEnvironment yfsEnv, Document inDoc) {

		LOGGER.beginTimer("GCValidateCallCenterLogin.validateCCLogin");
		LOGGER.verbose("Class: GCValidateCallCenterLogin Method: validateCCLogin BEGIN");

		YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
		YFCElement rootEle = inputDoc.getDocumentElement();
		String loginID = rootEle.getAttribute(GCConstants.LOGIN_ID);
		String domain = rootEle.getAttribute(GCConstants.DOMAIN);
		String suserDN = domain + GCConstants.DOMAIN_LOGINID_SEPARATOR + loginID;
		String sldapDN = domain + GCConstants.LDAP_DOMAIN_LOGINID_SEPARATOR + loginID;
		String sOrderHeaderKey = rootEle
				.getAttribute(GCConstants.ORDER_HEADER_KEY);
		String sGroupValidationFlag = rootEle
				.getAttribute(GCConstants.IS_GROUP_VALIDATION_REQUIRED);
		String sAction = rootEle.getAttribute(GCConstants.ACTION);

		// LdapAuthentication

		 GCCommonUtil.ldapConnection(rootEle,sldapDN);

		boolean isAuthSuccess = false;

		// User Hierarchy
		YFCDocument outDoc = GCCommonUtil.userGroupList(yfsEnv, suserDN);

    YFCElement eleRoot = outDoc.getDocumentElement();
    String userGrpID = eleRoot.getAttribute(GCConstants.USER_GROUP_ID);
    String sExtnEmpID = eleRoot.getAttribute(GCConstants.EXTN_EMPLOYEE_ID);
    String sUserName = eleRoot.getAttribute("Username");

		if (YFCCommon.isVoid(sExtnEmpID)) {
			YFCException yfce = new YFCException(
					GCErrorConstants.INVALID_CREDENTIAL_ERRORCODE);
			throw yfce;
		}

		YFCDocument getCommonCodeListIndoc = YFCDocument
				.createDocument(GCConstants.COMMON_CODE);
		YFCElement commonCode = getCommonCodeListIndoc.getDocumentElement();
		commonCode.setAttribute(GCConstants.CODE_TYPE,
				GCConstants.GC_STORE_GROUP);
		boolean isManagerApp = rootEle
				.getBooleanAttribute(GCConstants.IS_MANAGER_APPROVAL);

		boolean isManagerID = false;
		if (!YFCCommon.equalsIgnoreCase(sAction, "ConfirmReturn")) {
			if (isManagerApp) {
				commonCode.setAttribute(GCConstants.CODE_SHORT_DESCRIPTION,
						GCConstants.MANAGEMENT);
				isManagerID = true;
			} else {
				commonCode.setAttribute(GCConstants.CODE_SHORT_DESCRIPTION,
						GCConstants.USER);
			}
		}

		if (YFCCommon.isVoid(sGroupValidationFlag)
				|| YFCCommon.equals(sGroupValidationFlag, GCConstants.FLAG_Y)) {
			Document getCommonCodeListOutput = GCCommonUtil.invokeAPI(yfsEnv,
					GCConstants.GET_COMMON_CODE_LIST,
					getCommonCodeListIndoc.getDocument());
			YFCDocument getCommonCodeListOutdoc = YFCDocument
					.getDocumentFor(getCommonCodeListOutput);
			YFCElement commonCodeList = getCommonCodeListOutdoc
					.getDocumentElement();
			YFCIterable<YFCElement> commonCodeEleList = commonCodeList
					.getChildren();
			for (YFCElement commonCodeEle : commonCodeEleList) {
				if (YFCUtils.equals(userGrpID,
						commonCodeEle.getAttribute(GCConstants.CODE_VALUE))) {
					rootEle.setAttribute(GCConstants.IS_SUCCESS,
							GCConstants.YES);
					isAuthSuccess = true;
				}
			}

			if (!isAuthSuccess) {
				YFCException yfce = new YFCException(
						GCErrorConstants.INSUFFICIENT_CREDENTIAL_ERRORCODE);
				throw yfce;
			}
		}
		YFCDocument userActivityDoc = YFCDocument
				.createDocument("UserActivity");
		YFCElement eleUserActivity = userActivityDoc.getDocumentElement();

		if (!YFCCommon.isVoid(sOrderHeaderKey)) {
			if (isManagerID) {
				eleUserActivity
						.setAttribute(GCConstants.MANAGER_ID, sExtnEmpID);
			} else {
				eleUserActivity.setAttribute(GCConstants.USER_ID, sExtnEmpID);
			}
			eleUserActivity.setAttribute(GCConstants.ORDER_HEADER_KEY,
					sOrderHeaderKey);
			eleUserActivity.setAttribute(GCConstants.USER_ACTION, sAction);
      eleUserActivity.setAttribute("Username", sUserName);
      GCCommonUtil.invokeService(yfsEnv,
					GCConstants.GC_INSERT_SHPMNT_ACTIVITY,
					userActivityDoc.getDocument());
			eleUserActivity.setAttribute(GCConstants.USER_GROUP_ID, userGrpID);
		} else {
			YFCNodeList<YFCElement> listOrderLine = inputDoc
					.getElementsByTagName(GCConstants.ORDER_LINE);

			int ilengthOrderLine = listOrderLine.getLength();
			for (int iCounterOrderLine = 0; iCounterOrderLine < ilengthOrderLine; iCounterOrderLine++) {
				YFCElement eleOrderLine = listOrderLine.item(iCounterOrderLine);
				String sOrdHrdKey = eleOrderLine
						.getAttribute(GCConstants.ORDER_HEADER_KEY);
				String sOrdLineKey = eleOrderLine
						.getAttribute(GCConstants.ORDER_LINE_KEY);

				eleUserActivity
						.setAttribute(GCConstants.MANAGER_ID, sExtnEmpID);
				eleUserActivity.setAttribute(GCConstants.ORDER_HEADER_KEY,
						sOrdHrdKey);
				eleUserActivity.setAttribute(GCConstants.ORDER_LINE_KEY,
						sOrdLineKey);
				eleUserActivity.setAttribute(GCConstants.USER_ACTION, sAction);
        eleUserActivity.setAttribute("Username", sUserName);
        GCCommonUtil.invokeService(yfsEnv,
						GCConstants.GC_INSERT_SHPMNT_ACTIVITY,
						userActivityDoc.getDocument());

			}
		}
		LOGGER.endTimer("GCValidateCallCenterLogin.validateCCLogin");
		return userActivityDoc.getDocument();
	}

	@Override
	public void setProperties(Properties arg0) {
	}
}
