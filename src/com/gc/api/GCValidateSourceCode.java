/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class is used to validate source codes.
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               20/05/2014        Gowda, Naveen		        JIRA No: This class is used to validate source codes.
 *#################################################################################################################################################################
 */
package com.gc.api;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto: naveen.s@expicient.com ">Naveen</a>
 */
public class GCValidateSourceCode {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCValidateSourceCode.class.getName());

    /**
     * 
     * Description of validateSourceCode
     * 
     * @param env
     * @param inDoc
     * @return
     * @throws Exception
     * 
     */
    public void validateSourceCode(YFSEnvironment env, Document inDoc)
	    throws GCException {

	LOGGER.debug("Class: GCValidateSourceCode Method: validateSourceCode BEGIN");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug("Incoming XML" + GCXMLUtil.getXMLString(inDoc));
	}
	try {
	    Element eleRoot = inDoc.getDocumentElement();
	    String sEnterpriseCode = eleRoot
		    .getAttribute(GCConstants.ENTERPRISE_CODE);
	    Element eleOrderExtn = GCXMLUtil.getElementByXPath(inDoc,
		    "Order/Extn");
	    String sExtnSourceCode = eleOrderExtn
		    .getAttribute(GCConstants.EXTN_SOURCE_CODE);
	    String sEntryType = "";
	    if (!YFCCommon.isVoid(sExtnSourceCode)) {
		Document docGetSrcCodeLstIn = GCXMLUtil
			.createDocument(GCConstants.GC_SOURCE_CODE);
		Element eleRootDocGetSrc = docGetSrcCodeLstIn
			.getDocumentElement();
		eleRootDocGetSrc.setAttribute(GCConstants.SOURCE_CODE,
			sExtnSourceCode);
		Document docGetSrcCodeListOut = GCCommonUtil.invokeService(env,
			GCConstants.GC_GET_SC_LIST_SERVICE, docGetSrcCodeLstIn);
		if (LOGGER.isDebugEnabled()) {
		    LOGGER.debug("docGetSrcCodeListOut"
			    + GCXMLUtil.getXMLString(docGetSrcCodeListOut));
		}
		NodeList nLGCSourceCode = GCXMLUtil.getNodeListByXpath(
			docGetSrcCodeListOut, "GCSourceCodeList/GCSourceCode");
		if (nLGCSourceCode.getLength() == 0) {

		    YFCException yfs = new YFCException(
			    GCErrorConstants.INVALID_SC_ERROR_CODE);
		    yfs.setErrorDescription(GCErrorConstants.INVALID_SOURCE_CODE_ERROR_DESC);
		    throw yfs;

		} else {
		    NodeList nLGCSourceCodeEntCode = GCXMLUtil
			    .getNodeListByXpath(docGetSrcCodeListOut,
				    "GCSourceCodeList/GCSourceCode[@EnterpriseCode='"
					    + sEnterpriseCode + "']");
		    if (nLGCSourceCodeEntCode.getLength() == 0) {

			YFCException yfs = new YFCException(
				GCErrorConstants.INVALID_SC_ERROR_CODE_FOR_ENTERPRISE);
			yfs.setErrorDescription(GCErrorConstants.INVALID_SOURCE_CODE_FOR_ENTERPRISE_DESC);
			throw yfs;
		    }
		    int iCount = 0;
		    String sOrderDate = eleRoot
			    .getAttribute(GCConstants.ORDER_DATE);

		    SimpleDateFormat sdf = new SimpleDateFormat(
			    GCConstants.DATE_FORMAT);

		    Date dateOrderDate = sdf.parse(sOrderDate);

		    for (int i = 0; i < nLGCSourceCodeEntCode.getLength(); i++) {
			Element eleGCSourceCode = (Element) nLGCSourceCodeEntCode
				.item(i);
			String sStartDate = eleGCSourceCode
				.getAttribute(GCConstants.START_DATE);
			Date dateStartDate = sdf.parse(sStartDate);
			String sEndDate = eleGCSourceCode
				.getAttribute(GCConstants.END_DATE);
			Date dateEndDate = sdf.parse(sEndDate);

			//OMS-5463- Start
			if (((dateOrderDate.after(dateStartDate) || dateOrderDate.equals(dateStartDate)) && (dateOrderDate
					.before(dateEndDate) || dateOrderDate.equals(dateStartDate)))) {
			//OMS-5463- End
			    LOGGER.debug("dateOrderDate" + dateOrderDate);
			    iCount++;
			    sEntryType = eleGCSourceCode
				    .getAttribute(GCConstants.ENTRY_TYPE);

			    eleRoot.setAttribute(GCConstants.ENTRY_TYPE,
				    sEntryType);

			}
		    }
		    if (iCount == 0) {

			YFCException yfs = new YFCException(
				GCErrorConstants.INACTIVE_SC_ERROR_CODE_FOR_ENTERPRISE);
			yfs.setErrorDescription(GCErrorConstants.INACTIVE_SOURCE_CODE_FOR_ENTERPRISE_DESC);
			throw yfs;
		    }
		}
		if (LOGGER.isDebugEnabled()) {
		    LOGGER.debug("changeOrder doc"
			    + GCXMLUtil.getXMLString(inDoc));
		}
		LOGGER.debug("Class: GCValidateSourceCode Method: validateSourceCode END");

		GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, inDoc);
	    }
	} catch (Exception e) {

	    LOGGER.error("Got exception while calling source code", e);
	    throw GCException.getYFCException(e.getMessage(), e);
	}

    }
}
