package com.gc.api;

import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCVendorLoadAPI implements YIFCustomApi  {
    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
            .instance(GCVendorLoadAPI.class);

    /**
     * This Method Creates Vendor Nodes and Vendor Node Enterprise and adds the node to Vendor DG.
     * Description of loadVendor
     *
     * @param env
     * @param inDoc
     * @return 
     *
     */
    public Document loadVendor(YFSEnvironment env, Document inDoc){
        LOGGER.beginTimer("loadVendor");
        YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
        inputDoc.getDocumentElement().setAttribute("IsSeller", "Y");
        createENTOrg(env, inputDoc);
        if(LOGGER.isVerboseEnabled()){
            LOGGER.verbose("Input to create Vendor Node::" + inputDoc.toString());
        }
        GCCommonUtil.invokeAPI(env, GCConstants.MANAGE_ORGANIZATION_HIERARCHY, inputDoc, YFCDocument.createDocument(GCConstants.ORGANIZATION));
        LOGGER.verbose("Calling manageDistributionRule to Add Vendor Node in Vendor DG");
        addVendorToDG(env, inputDoc.getDocumentElement().getAttribute(GCConstants.ORGANIZATION_CODE));
        LOGGER.endTimer("createENTOrg");
        LOGGER.endTimer("loadVendor");
        return inDoc;
    }

    /**
     * This Method adds the vendor node to Vendor DG.
     * Description of addVendorToDG
     *
     * @param env
     * @param node 
     *
     */
    private void addVendorToDG(YFSEnvironment env, String node) {
        LOGGER.beginTimer("addVendorToDG");
        YFCDocument manageDistributionRuleInDoc = YFCDocument.createDocument("DistributionRule");
        YFCElement rootEle = manageDistributionRuleInDoc.getDocumentElement();
        rootEle.setAttribute("DistributionRuleId", "GC_VENDOR_NODES");
        rootEle.setAttribute("OwnerKey", "GC");
        rootEle.setAttribute("Purpose", "SOURCING");
        rootEle.setAttribute("ItemGroupCode", "PROD");
        YFCElement itemShipNodesEle = rootEle.createChild("ItemShipNodes");
        YFCElement itemShipNodeEle = itemShipNodesEle.createChild("ItemShipNode");
        itemShipNodeEle.setAttribute("ItemId", "ALL");
        itemShipNodeEle.setAttribute("ShipnodeKey", node);
        itemShipNodeEle.setAttribute("Priority", "10");
        itemShipNodeEle.setAttribute("ActiveFlag", "Y");
        
        GCCommonUtil.invokeAPI(env, GCConstants.MANAGE_DISTRIBUTION_RULE, manageDistributionRuleInDoc, YFCDocument.createDocument("DistributionRule"));
        LOGGER.endTimer("addVendorToDG");
        
    }

    /**
     * This Method creates the Vendor Enterprise for the given vendor node.
     * Description of createENTOrg
     *
     * @param env
     * @param inputDoc 
     *
     */
    private void createENTOrg(YFSEnvironment env, YFCDocument inputDoc) {
        LOGGER.beginTimer("createENTOrg");
        YFCDocument inputClone = YFCDocument.getDocumentFor(inputDoc.toString());
        YFCElement rootEle = inputClone.getDocumentElement();
        String orgCode = rootEle.getAttribute(GCConstants.ORGANIZATION_CODE) + "_ENT";
        rootEle.setAttribute(GCConstants.ORGANIZATION_CODE, orgCode);
        rootEle.setAttribute(GCConstants.PARENT_ORG_CODE, GCConstants.GCI);
        rootEle.setAttribute("IsNode", "N");
        YFCElement eleNode = rootEle.getChildElement(GCConstants.NODE);
        if(!YFCCommon.isVoid(eleNode)){
            rootEle.removeChild(eleNode);
        }
        YFCElement enterpriseEle = rootEle.createChild("Enterprise");
        enterpriseEle.setAttribute("Enterprisecode", orgCode);
        if(LOGGER.isVerboseEnabled()){
            LOGGER.verbose("Input to create " + orgCode + "Enterprise::" + inputClone.toString());
        }
        GCCommonUtil.invokeAPI(env, GCConstants.MANAGE_ORGANIZATION_HIERARCHY, inputClone, YFCDocument.createDocument(GCConstants.ORGANIZATION));
        LOGGER.endTimer("createENTOrg");

    }

    public void setProperties(Properties arg0) throws Exception {


    }

}
