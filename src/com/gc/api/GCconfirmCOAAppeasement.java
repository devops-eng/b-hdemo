/**
 * Copyright 2014 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  ######################################################################################################################################
 *   OBJECTIVE: This class is is used to give Appeasements.
 *  ######################################################################################################################################
 *        Version    Date          Modified By           CR                Description
 *  ######################################################################################################################################
 *        1.0       04/07/2014      --                             Initial Version ConfirmAppeasement.
 *        1.1       14/05/2017     Arora, Shamim                           GCSTORE-7013: Multiple COA Appeasement,takes OPM as appeasement 
 * *  ######################################################################################################################################
 */
package com.gc.api;

import java.util.Properties;
import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCconfirmCOAAppeasement implements YIFCustomApi {

  YFSEnvironment env = null;
  Document inDoc = null;

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCconfirmCOAAppeasement.class);

  @Override
  public void setProperties(Properties arg0) throws GCException {

  }

  /**
   * This method will call processOrderPayments API for confirm Appeasement
   *
   * @param env
   * @param inDoc
   * @return
   * @throws GCException
   */
public Document confirmCOAAppeasement(YFSEnvironment env, Document inDoc) {
	  
    try {
      
    	LOGGER.beginTimer("GCconfirmCOAAppeasement.confirmCOAAppeasement()");
        LOGGER.verbose("GCconfirmCOAAppeasement - confirmCOAAppeasement() : Start");
    	
      LOGGER.debug("Incoming XML for processOrderPayments API call in confirmCOAAppeasement: " + GCXMLUtil.getXMLString(inDoc));

  
          GCCommonUtil.invokeAPI(env, inDoc, GCConstants.PROCESS_ORDER_PAYMENTS,
              null);
          
          LOGGER.debug("processOrderPayments API call successful in confirmCOAAppeasement: " + GCXMLUtil.getXMLString(inDoc));
          
      return inDoc;
      
    } catch (Exception e) {
      LOGGER.error("The exception is in GCconfirmCOAAppeasement method:", e);
      LOGGER.error("Error caught at GCconfirmCOAAppeasement",e);
		e.printStackTrace();
    }
    
	return inDoc;

  }
}
