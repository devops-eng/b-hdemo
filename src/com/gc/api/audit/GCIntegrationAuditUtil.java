/**
 * Copyright 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Captures XML messages posted to Queue
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
 *          1.0                03/02/2015        Ravi, Divya			Initial Version
 *#################################################################################################################################################################
 */
package com.gc.api.audit;

import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="divya.ravi@expicient.com">Divya Ravi</a>
 */

public class GCIntegrationAuditUtil implements YIFCustomApi {

  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCIntegrationAuditUtil.class.getName());

  private Properties props;

  /**
   * Implementation of setProperties() method.
   *
   * @param properties
   *            Properties object
   *
   * @throws Exception
   *             generic exception
   */
  @Override
  public final void setProperties(final Properties properties) {
    LOGGER.info("inside setProperties ....");

    if (properties == null) {
      LOGGER.info("properties is NULL!!!!");

      this.props = new Properties();
    }

    this.props = properties;
  }

  /**
   * Creation of integration audit message, saved to GC_INTEGRATION_AUDIT.
   *
   * @param yfsEnv
   *            YFSEnvironment
   * @param docIp
   *            input document
   *
   * @return Document
   *
   */
  public final Document createIntegrationAuditMessage(final YFSEnvironment yfsEnv, final Document docIp) {
    LOGGER.beginTimer("GCCreateIntegrationAuditService");
    LOGGER.info("GCIntegrationAuditUtil :: createIntegrationAuditMessage :: Start");
    LOGGER.debug("createIntegrationAuditMessage :: docIp :: \n" + GCXMLUtil.getXMLString(docIp));
    try {
      // Getting values of Properties incoming from service invoking this
      // Audit class
      final String strMsgFrom = this.props.getProperty("MSG_FROM");
      final String strMsgTo = this.props.getProperty("MSG_TO");
      final String strQueueId = this.props.getProperty("QUEUE_ID");
      final String strActive = this.props.getProperty("IS_ACTIVE");
      final String strMsgId = this.props.getProperty("MSG_IDENTIFIER");
      String strMsgDesc = this.props.getProperty("MSG_DESCRIPTION");

      if ("N".equals(strActive)) {
        return docIp;
      }

      if (strMsgDesc == null) {
        strMsgDesc = "";
      } else {
        strMsgDesc = strMsgDesc.trim();
      }

      String strMsgIdentifier = GCXMLUtil.getAttributeFromXPath(docIp, strMsgId);

      if (!StringUtils.isBlank(strMsgIdentifier)) {
        strMsgDesc = strMsgDesc + " [" + strMsgIdentifier.trim() + "]";
      }

      if ((strMsgDesc != null) && (strMsgDesc.length() > 50)) {
        strMsgDesc = strMsgDesc.substring(0, 50);
      }

      String xmlData = GCXMLUtil.getXMLString(docIp);
      xmlData = xmlData.replaceAll("\"", "&quot;");
      xmlData = xmlData.replaceAll(">", "&gt;");
      xmlData = xmlData.replaceAll("<", "&lt;");

      final YFCDocument inputDocForFlow = YFCDocument.createDocument(GCConstants.GC_INTEGRATION_AUDIT);
      inputDocForFlow.getDocumentElement().setAttribute(GCConstants.MSG_FROM, strMsgFrom);
      inputDocForFlow.getDocumentElement().setAttribute(GCConstants.MSG_TO, strMsgTo);
      inputDocForFlow.getDocumentElement().setAttribute(GCConstants.MSG_DESCRIPTION, strMsgDesc);
      inputDocForFlow.getDocumentElement().setAttribute(GCConstants.MSG_XML, xmlData);
      inputDocForFlow.getDocumentElement().setAttribute(GCConstants.QUEUE_ID, strQueueId);
      inputDocForFlow.getDocumentElement().setAttribute(GCConstants.MSG_IDENTIFIER, strMsgIdentifier);

      // calling database API to perform entry of xml in the integration
      // audit table

      GCCommonUtil.invokeService(yfsEnv,  GCConstants.GC_CREATE_INTEGRATION_AUDIT_SERVICE, inputDocForFlow.getDocument());
      LOGGER.debug("GC Integration class ends ");

      LOGGER.endTimer("GCCreateIntegrationAuditService");

    } catch (Exception ex) {
      LOGGER.error("******Exception occured in createIntegrationAuditMessage method, of GCIntegrationAuditUtil.class*******");
      LOGGER.error(ex);
      LOGGER.log("Exception ::", ex);
    }
    LOGGER.info("GCIntegrationAuditUtil :: createIntegrationAuditMessage :: End");
    return docIp;

  }

}