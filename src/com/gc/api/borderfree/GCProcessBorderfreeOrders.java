/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *##############################################################################################################################################################
 *          OBJECTIVE: This class will process response from border free for border free response either as Accepted or Rejected
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0               04/06/2015        Kumar, Gunjan              GCSTORE- 765/815/809/1442 : Design- Borderfree orders processing
 *#################################################################################################################################################################
 */

package com.gc.api.borderfree;

import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 *
 * @author gunjankumar
 *
 */
public class GCProcessBorderfreeOrders implements YIFCustomApi {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCProcessBorderfreeOrders.class.getName());

  /**
   * This method checks for the response of borderfree and based on the response either 1) resolves
   * the hold and sends order confirmation message 2) cancels the order
   * 
   * @param env
   * @param inputDoc
   */
  public void checkAndProcessINTLWebHold(YFSEnvironment env, Document inputDoc) {
    LOGGER.beginTimer("GCProcessBorderfreeOrders.checkAndProcessINTLWebHold");
    LOGGER.verbose("GCProcessBorderfreeOrders.checkAndProcessINTLWebHold--Begin");
    // fetching the attributes from the inputDoc that is returned from the queue
    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    YFCElement inDocRootEle = inDoc.getDocumentElement();

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("inDoc Document: " + inDoc.toString());
    }

    boolean bIsStatusRejected = false;
    String sAction = inDocRootEle.getAttribute(GCConstants.ACTION);
    String orderNo = inDocRootEle.getAttribute(GCConstants.ORDER_NO);
    String sIntWebOrderNo =
        inDocRootEle.getChildElement(GCConstants.EXTN).getAttribute(GCConstants.EXTN_INTL_WEB_ORDER_NO);
    String enterpriseCode = inDocRootEle.getAttribute(GCConstants.ENTERPRISE_CODE);

    if (!YFCCommon.isStringVoid(sAction) && YFCCommon.equalsIgnoreCase(GCConstants.ACTION_CANCEL, sAction)) {
      bIsStatusRejected = true;
      // Fetch cancellation reason code and description from common code
      YFCDocument yfcDocGetCommonCodeList =
          GCCommonUtil.getCommonCodeListByTypeAndValue(env, GCConstants.CANCEL_CODE_TYPE, enterpriseCode,
              GCConstants.BORDERFREE_CANCEL_CODE_VALUE);
      YFCElement yfcEleCommonCode =
          yfcDocGetCommonCodeList.getDocumentElement().getChildElement(GCConstants.COMMON_CODE);
      String sReasonCode = yfcEleCommonCode.getAttribute(GCConstants.CODE_SHORT_DESCRIPTION);
      String sReasonText = yfcEleCommonCode.getAttribute(GCConstants.CODE_LONG_DESCRIPTION);
      inDocRootEle.setAttribute(GCConstants.MODIFICATION_REASON_CODE, sReasonCode);
      inDocRootEle.setAttribute(GCConstants.MODIFICATION_REASON_TEXT, sReasonText);
    }

    // creating an input for getOrderList and calling getOrderList API
    YFCDocument getOrderListInDoc = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement getOrderListInDocRootEle = getOrderListInDoc.getDocumentElement();
    getOrderListInDocRootEle.setAttribute(GCConstants.ORDER_NO, orderNo);
    getOrderListInDocRootEle.setAttribute(GCConstants.ENTERPRISE_CODE, enterpriseCode);
    getOrderListInDocRootEle.setAttribute(GCConstants.DOCUMENT_TYPE, GCConstants.SALES_ORDER_DOCUMENT_TYPE);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getOrderListInDoc Document: " + getOrderListInDoc.toString());
    }

    YFCDocument getOrderListTempDoc = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_TEMP_BORDERFREE);
    YFCDocument getOrderListOutDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST, getOrderListInDoc, getOrderListTempDoc);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getOrderListOutDoc Document: " + getOrderListOutDoc.toString());
    }

    YFCElement getOrderListOutDocRootele = getOrderListOutDoc.getDocumentElement();
    YFCElement yfcEleOrder = getOrderListOutDocRootele.getChildElement(GCConstants.ORDER);

    if (YFCCommon.isVoid(yfcEleOrder)) {
      LOGGER.debug("Order does not exist in the OMS");
      LOGGER.verbose("GCProcessBorderfreeOrders.checkAndProcessINTLWebHold--End");
      LOGGER.endTimer("GCProcessBorderfreeOrders.checkAndProcessINTLWebHold");
      throw new YFCException(GCErrorConstants.ORDER_DOESNOT_EXISTS);
    }
    YFCElement elePersonInfoShipTo = inDocRootEle.getChildElement(GCConstants.PERSON_INFO_SHIP_TO);
    if (!YFCCommon.isVoid(elePersonInfoShipTo)) {
      elePersonInfoShipTo.setAttribute(GCConstants.ADDRESS_LINE2, sIntWebOrderNo);
    }
    String sOrderHeaderKey = yfcEleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
    inDocRootEle.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
    inDocRootEle.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, inDoc, null);

    if (!bIsStatusRejected) {
      // Order is accepted by border free
      YFCDocument borderFreeWebServiceInputMsgDoc = prepareBorderfreeWebServiceMsg(sIntWebOrderNo, yfcEleOrder);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("borderFreeWebServiceInputMsgDoc Document: " + borderFreeWebServiceInputMsgDoc.toString());
      }
      GCCommonUtil.invokeService(env, GCConstants.POST_BF_RESPONSE_MSG_TO_Q_SERVICE, borderFreeWebServiceInputMsgDoc);
    }

    LOGGER.verbose("GCProcessBorderfreeOrders.checkAndProcessINTLWebHold--End");
    LOGGER.endTimer("GCProcessBorderfreeOrders.checkAndProcessINTLWebHold");
  }



  

  /**
   * This method will prepare borderfree create async request - order confirmation web service call
   * message
   * 
   * @param sIntWebOrderNo
   * @param yfcOrderEle
   * @return
   */
  private YFCDocument prepareBorderfreeWebServiceMsg(String sIntWebOrderNo, YFCElement yfcOrderEle) {
    LOGGER.beginTimer("GCProcessBorderfreeOrders.prepareBorderfreeWebServiceMsg");
    LOGGER.verbose("GCProcessBorderfreeOrders.prepareBorderfreeWebServiceMsg--Start");

    String sMerchantOrderId =
        yfcOrderEle.getChildElement(GCConstants.EXTN).getAttribute(GCConstants.EXTN_SOURCE_ORDER_NO);
    // prepared root element
    YFCDocument yfcDocCreateBorderFreeMsg = YFCDocument.createDocument(GCConstants.SOAP_ENVELOPE);
    YFCElement eleSoapEnvelope = yfcDocCreateBorderFreeMsg.getDocumentElement();
    eleSoapEnvelope.setAttribute(GCConstants.XMLNS_SOAP, GCConstants.BORDERFREE_URL_XMLNS_SOAP);
    eleSoapEnvelope.setAttribute(GCConstants.XMLNS_V1, GCConstants.BORDERFREE_URL_XMLNS_V1);
    // Added header element
    YFCElement eleHeader =
        (YFCElement) eleSoapEnvelope.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.SOAP_HEADER));
    // Added Security element
    YFCElement eleSecurity =
        (YFCElement) eleHeader.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.WSSE_SECURITY));
    eleSecurity.setAttribute(GCConstants.XMLNS_WSSE, GCConstants.BORDERFREE_URL_XMLNS_WSSE);
    // Added UserToken element
    YFCElement eleUserToken =
        (YFCElement) eleSecurity.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.USER_NAME_TOKEN));
    String yfsUser = YFSSystem.getProperty(GCConstants.GC_BORDERFREE_WEBSERVICE_USERNAME);
    String yfsPassword = YFSSystem.getProperty(GCConstants.GC_BORDERFREE_WEBSERVICE_PASSWORD);
    // Added UserName element
    YFCElement eleUserName =
        (YFCElement) eleUserToken.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.WSSE_USERNAME));
    eleUserName.setNodeValue(yfsUser);
    // Added UserPassword element
    YFCElement elePassword =
        (YFCElement) eleUserToken.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.WSSE_PASSWORD));
    elePassword.setNodeValue(yfsPassword);
    // Added Body element
    YFCElement eleBody =
        (YFCElement) eleSoapEnvelope.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.SOAP_BODY));
    // Added orderConfirmation element
    YFCElement eleorderConfirmation =
        (YFCElement) eleBody.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.ORDER_CONFIRMATION));
    // Added order element
    YFCElement eleorder =
        (YFCElement) eleorderConfirmation.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.ORDER_SMALL));
    // Added merchantOrderId element
    YFCElement eleMerchantOrderId =
        (YFCElement) eleorder.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.MERCHANT_ORDER_ID));
    // Append Merchant Order Id
    eleMerchantOrderId.setNodeValue(sMerchantOrderId);
    // Added order element
    YFCElement eleOrderId =
        (YFCElement) eleorder.appendChild(yfcDocCreateBorderFreeMsg.createElement(GCConstants.ORDER_ID_SMALL));
    eleOrderId.setNodeValue(sIntWebOrderNo);

    LOGGER.verbose("GCProcessBorderfreeOrders.prepareBorderfreeWebServiceMsg--End");
    LOGGER.endTimer("GCProcessBorderfreeOrders.prepareBorderfreeWebServiceMsg");
    return yfcDocCreateBorderFreeMsg;
  }

  @Override
  public void setProperties(Properties arg0) {
    
  }

}