/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *##############################################################################################################################################################
 *          OBJECTIVE: This class will call border free web service to post Order confirmation
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0               04/06/2015        Kumar, Gunjan              GCSTORE- 765/815/809/1442 : Design- Borderfree orders processing
 *#################################################################################################################################################################
 */

package com.gc.api.borderfree;

import java.io.IOException;
import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.utils.GCWebServiceUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 *
 * @author gunjankumar
 *
 */
public class GCPublishBorderFreeStatus implements YIFCustomApi {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCProcessBorderfreeOrders.class.getName());

  /**
   * This method is invoked by integration server
   * @param env
   * @param inDocSoap
   */
  public void postNPublishBFStatusUpdates(YFSEnvironment env, Document inDocSoap) {
    LOGGER.beginTimer("GCPublishBorderFreeStatus.postNPublishBFStatusUpdates");
    LOGGER.verbose("GCPublishBorderFreeStatus.postNPublishBFStatusUpdates--Begin");

    // Is invoked on reprocess from support team, invoke web service call
    String sSoapUrlForATG = YFSSystem.getProperty(GCConstants.GC_BORDERFREE_WEBSERVICE_URL);
    String contentTypeValue = GCConstants.CONTENT_TYPE_VAL;
    GCWebServiceUtil obj = new GCWebServiceUtil();
    Document docBFResponse = null;
    try {
      // invoke BF Sync Webservice
      docBFResponse =
          obj.invokeSOAPWebService(inDocSoap, sSoapUrlForATG, GCConstants.BLANK, GCConstants.BLANK, contentTypeValue);
    } catch (IOException | YFCException yfsExcep) {
      LOGGER.error("Border free web service call failed - ", yfsExcep);
    }

    if (YFCCommon.isVoid(docBFResponse)) {
      YFCException excep = new YFCException(GCErrorConstants.WEB_SERVICE_BORDERFREE_TIMEOUT_CODE);
      LOGGER.verbose("GCPublishBorderFreeStatus.postNPublishBFStatusUpdates--End");
      LOGGER.endTimer("GCPublishBorderFreeStatus.postNPublishBFStatusUpdates");
      throw excep;
    } else {
      YFCDocument docBFResponseOutput = YFCDocument.getDocumentFor(docBFResponse);

      if(LOGGER.isVerboseEnabled()){
        LOGGER.verbose("docBFResponseOutput : "+docBFResponseOutput.toString());
      }

      YFCElement yfcEleSFault = docBFResponseOutput.getElementsByTagName(GCConstants.S_FAULT).item(0);
      if (!YFCCommon.isVoid(yfcEleSFault)) {
        String errorCode = GCConstants.BLANK;
        String sErrorMessage = GCConstants.BLANK;
        String sErrorDetails = GCConstants.BLANK;
        String bfErrorMessage = GCConstants.BLANK;

        YFCElement msgError = docBFResponseOutput.getElementsByTagName(GCConstants.ERROR_SMALL).item(0);
        if (!YFCCommon.isVoid(msgError)) {
          errorCode = msgError.getAttribute(GCConstants.CODE_SMALL);
        }
        YFCElement errorMessage = docBFResponseOutput.getElementsByTagName(GCConstants.MESSAGE).item(0);
        if (!YFCCommon.isVoid(errorMessage)) {
          sErrorMessage = errorMessage.getNodeValue();
        }
        YFCElement details = docBFResponseOutput.getElementsByTagName(GCConstants.DETAILS).item(0);
        if (!YFCCommon.isVoid(details)) {
          sErrorDetails = details.getNodeValue();
        }
        bfErrorMessage = new StringBuilder(sErrorMessage).append(". ").append(sErrorDetails).toString();
        YFCException excep = new YFCException(errorCode);
        excep.setErrorDescription(bfErrorMessage);
        LOGGER.verbose("GCPublishBorderFreeStatus.postNPublishBFStatusUpdates--End");
        LOGGER.endTimer("GCPublishBorderFreeStatus.postNPublishBFStatusUpdates");
        throw excep;
      }
    }
    LOGGER.verbose("GCPublishBorderFreeStatus.postNPublishBFStatusUpdates--End");
    LOGGER.endTimer("GCPublishBorderFreeStatus.postNPublishBFStatusUpdates");
  }

  @Override
  public void setProperties(Properties arg0) {

  }
}
