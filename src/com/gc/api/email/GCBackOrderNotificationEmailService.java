/**
 * Copyright 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Publishes order details to TIBCO queue for sending BackOrderNotification email to the customer
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
 *          1.0                06/24/2014        Susai, John Melvin			Initial Version
 *          1.1                09/09/2014          Gupta, Himanshu             Code Re-factoring
 *#################################################################################################################################################################
 */
package com.gc.api.email;

import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCChangeOrderUtil;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCEmailUtils;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.gc.condition.GCSendEmailConditions;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="john.melvin@expicient.com">John Melvin</a>
 */
public class GCBackOrderNotificationEmailService implements YIFCustomApi {

    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCBackOrderNotificationEmailService.class.getName());

    /**
     * 
     * Description of sendOrderDetails Publishes order details to TIBCO queue
     * for sending email to the customer
     * 
     * @param env
     * @param inDoc
     * @return
     * @throws Exception
     * 
     */
    public Document sendOrderDetails(YFSEnvironment env, Document inDoc)
	    throws GCException {
	LOGGER.debug(" Entering sendOrderDetails() method ");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" sendOrderDetails() method, Input document is"
		    + GCXMLUtil.getXMLString(inDoc));
	}
	Document response = null;
	try {
	    String sOrderHeaderKey = ((Element) inDoc.getElementsByTagName(
		    "Order").item(0))
		    .getAttribute(GCConstants.ORDER_HEADER_KEY);

	    Document getOrderListInDoc = GCXMLUtil
		    .getDocument("<Order OrderHeaderKey=\"" + sOrderHeaderKey
			    + "\"></Order>");

	    Document getOrderListOutDoc = GCCommonUtil
		    .invokeAPI(
			    env,
			    GCConstants.GET_ORDER_LIST,
			    getOrderListInDoc,
			    GCCommonUtil
				    .getDocumentFromPath("/global/template/api/getOrderList_For_BackOrderNotification_Email.xml"));

	    Element ordEle = (Element) getOrderListOutDoc.getElementsByTagName(
		    "Order").item(0);

	    Element extnEle = (Element) ordEle.getElementsByTagName("Extn")
		    .item(0);
	    String orderCaptureChannel = extnEle
		    .getAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL);
	    //String extnFTCExempt = extnEle.getAttribute("ExtnFTCExempt");

	    if (/*!GCSendEmailConditions.isOrderFTCExempt(env, extnFTCExempt)
		    &&*/ (GCSendEmailConditions.isCallCenterOrder(env,
			    orderCaptureChannel) || GCSendEmailConditions
			    .isJBSDCSourced(env, orderCaptureChannel))) {
		Document ordDoc = GCXMLUtil.getDocumentFromElement(ordEle);
		response = formOrderOutput(env, ordDoc);
		GCChangeOrderUtil.addNoteToOrder(env,
			GCConstants.GC_BACKORDER_NOTIFICATION_EMAIL_NOTE,
			sOrderHeaderKey);
	    } else {
		response = GCEmailUtils.returnNoRecordFoundDoc();
	    }

	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCBackOrderNotificationEmailService.sendOrderDetails()",
		    e);
	    throw new GCException(e);
	}
	LOGGER.debug(" Exiting GCBackOrderNotificationEmailService.sendOrderDetails() method ");
	return response;
    }

    public void setProperties(Properties arg0) {
    }

    public Document formOrderOutput(YFSEnvironment env, Document inputDoc)
	    throws GCException {
	try {
	    Element orderEle = inputDoc.getDocumentElement();
	    orderEle.setAttribute(GCConstants.EMAIL_TYPE, "FTC_NOTIFICATION");
	    Document orderDoc = GCXMLUtil.getDocumentFromElement(orderEle);

	    Document backorderNotifyXSLTOutDoc = GCCommonUtil.invokeService(
		    env, GCConstants.GC_BACKORDER_NOTIFICATION_XSLT, orderDoc);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("****Backorder Notification Email Output*****"
			+ GCXMLUtil.getXMLString(backorderNotifyXSLTOutDoc));
	    }
	    GCCommonUtil
		    .invokeService(
			    env,
			    GCConstants.GC_PUBLISH_TO_EMAIL_BACKORDER_NOTIFICATION_INBOUND_QUEUE,
			    backorderNotifyXSLTOutDoc);
	    return GCEmailUtils.sendSuccessMessageDoc();
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCBackOrderNotificationEmailService.formOrderOutput()",
		    e);
	    throw new GCException(e);
	}
    }

}
