/**
 * Copyright 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Publishes order details to TIBCO queue for sending BackOrderNotification email on success event of order create transaction to the customer
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
 *          1.0                06/24/2014        Susai, John Melvin			Initial Version
 *#################################################################################################################################################################
 */
package com.gc.api.email;

import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCChangeOrderUtil;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCEmailUtils;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.gc.condition.GCSendEmailConditions;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="john.melvin@expicient.com">John Melvin</a>
 */
public class GCBackOrderNotificationEmailServiceOnOrderCreation implements
	YIFCustomApi {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCBackOrderNotificationEmailServiceOnOrderCreation.class
		    .getName());

    /**
     * 
     * Description of sendOrderDetails Publishes order details to TIBCO queue
     * for sending email to the customer
     * 
     * @param env
     * @param inDoc
     * @return
     * @throws Exception
     * 
     */
    public Document sendOrderDetails(YFSEnvironment env, Document inDoc)
	    throws GCException {
	LOGGER.debug(" Entering sendOrderDetails() method ");
	if(LOGGER.isDebugEnabled()){
	LOGGER.debug(" sendOrderDetails() method, Input document is"
		+ GCXMLUtil.getXMLString(inDoc));
	}
	Document respDoc = null;
	try {
	    String sOrderHeaderKey = inDoc.getDocumentElement().getAttribute(
		    GCConstants.ORDER_HEADER_KEY);

	    String minOrderStatus = inDoc.getDocumentElement().getAttribute(
		    "MinOrderStatus");
	    Element extnEle = (Element) inDoc.getDocumentElement()
		    .getElementsByTagName("Extn").item(0);
	    String orderCaptureChannel = extnEle
		    .getAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL);
	    String extnFTCExempt = extnEle.getAttribute("ExtnFTCExempt");
	    if (GCConstants.STATUS_CREATED.equalsIgnoreCase(minOrderStatus)) {
		if (!GCSendEmailConditions.isOrderFTCExempt(env, extnFTCExempt)
			&& (GCSendEmailConditions.isCallCenterOrder(env,
				orderCaptureChannel) || GCSendEmailConditions
				.isJBSDCSourced(env, orderCaptureChannel))) {
		    respDoc = formOrderOutput(env, inDoc);
		    GCChangeOrderUtil.addNoteAndUpdateExtnIsBackOrderEmailSent(
			    env,
			    GCConstants.GC_BACKORDER_NOTIFICATION_EMAIL_NOTE,
			    sOrderHeaderKey,
			    inDoc.getDocumentElement(),
			    GCConstants.STATUS_CREATED);
		} else {
		    respDoc = GCEmailUtils.returnNoRecordFoundDoc();
		}
	    } else {
		respDoc = GCEmailUtils.returnNoRecordFoundDoc();
	    }

	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCBackOrderNotificationEmailServiceOnOrderCreation.sendOrderDetails()",
		    e);
	    throw new GCException(e);
	}
	LOGGER.debug(" Exiting GCBackOrderNotificationEmailServiceOnOrderCreation.sendOrderDetails() method ");
	return respDoc;
    }

    public void setProperties(Properties arg0) {

    }

    public Document formOrderOutput(YFSEnvironment env, Document inputDoc)
	    throws GCException {
	try {
	    Element orderEle = inputDoc.getDocumentElement();
	    orderEle.setAttribute(GCConstants.EMAIL_TYPE, "FTC_NOTIFICATION");
	    Document orderDoc = GCXMLUtil.getDocumentFromElement(orderEle);

	    Document backorderNotifyXSLTOutDoc = GCCommonUtil.invokeService(
		    env, GCConstants.GC_BACKORDER_NOTIFICATION_XSLT, orderDoc);
	    if(LOGGER.isDebugEnabled()){
	    LOGGER.debug("****Backorder Notification Email Output*****"
		    + GCXMLUtil.getXMLString(backorderNotifyXSLTOutDoc));
	    }
	    GCCommonUtil
		    .invokeService(
			    env,
			    GCConstants.GC_PUBLISH_TO_EMAIL_BACKORDER_NOTIFICATION_INBOUND_QUEUE,
			    backorderNotifyXSLTOutDoc);
	    return GCEmailUtils.sendSuccessMessageDoc();
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCBackOrderNotificationEmailServiceOnOrderCreation.formOrderOutput()",
		    e);
	    throw new GCException(e);
	}
    }

}
