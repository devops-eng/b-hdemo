/**
 * Copyright 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Publishes order details to TIBCO queue for sending BackOrderNotification email on backorder event of Schedule transaction to the customer
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
 *          1.0                06/24/2014        Susai, John Melvin			Initial Version
 *#################################################################################################################################################################
 */
package com.gc.api.email;

import java.util.HashMap;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCChangeOrderUtil;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCEmailUtils;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.gc.condition.GCSendEmailConditions;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="john.melvin@expicient.com">John Melvin</a>
 */
public class GCBackOrderNotificationEmailServiceOnSchedule implements
	YIFCustomApi {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCBackOrderNotificationEmailServiceOnSchedule.class
		    .getName());

    /**
     * 
     * Description of sendOrderDetails Publishes order details to TIBCO queue
     * for sending email to the customer
     * 
     * @param env
     * @param inDoc
     * @return
     * @throws Exception
     * 
     */
    public Document sendOrderDetails(YFSEnvironment env, Document inDoc)
	    throws GCException {
	LOGGER.debug(" Entering sendOrderDetails() method ");
	if(LOGGER.isDebugEnabled()){
	LOGGER.debug(" sendOrderDetails() method, Input document is"
		+ GCXMLUtil.getXMLString(inDoc));
	}
	Document respDoc = null;
	try {
	    String sOrderHeaderKey = inDoc.getDocumentElement().getAttribute(
		    GCConstants.ORDER_HEADER_KEY);

	    String sGetOrderListInDoc = "<Order OrderHeaderKey=\""
		    + sOrderHeaderKey + "\"></Order>";
	    Document getOrderListInDoc = GCXMLUtil
		    .getDocument(sGetOrderListInDoc);

	    Document getOrderListOutDoc = GCCommonUtil
		    .invokeAPI(
			    env,
			    GCConstants.GET_ORDER_LIST,
			    getOrderListInDoc,
			    GCCommonUtil
				    .getDocumentFromPath("/global/template/api/getOrderList_For_BackOrderNotification_Email.xml"));

	    Element ordEle = (Element) getOrderListOutDoc.getElementsByTagName(
		    "Order").item(0);

	    Element extnEle = (Element) ordEle.getElementsByTagName("Extn")
		    .item(0);
	    String orderCaptureChannel = extnEle
		    .getAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL);
	   // String extnFTCExempt = extnEle.getAttribute("ExtnFTCExempt");
	    String extnIsBackOrderEmailSent = "";
	    boolean emailSent = false;
	  
	    //GCSTORE-6005 and GCSTORE-6009
	
	    String minOrderStatus=null;
	    HashMap<String, String> mapOrderLine=GCCommonUtil.checkforBundleParent(ordEle);
	    String strOrderLineKey=null;
        for (String key : mapOrderLine.keySet()) {
            minOrderStatus = mapOrderLine.get(key);
            strOrderLineKey=key;
          }
        
      
        extnIsBackOrderEmailSent=GCXMLUtil.getAttributeFromXPath(ordEle, "//OrderLine[@OrderLineKey='"+strOrderLineKey+"']/Extn/@ExtnIsBackOrderEmailSent");
            if(LOGGER.isDebugEnabled()){
            
            LOGGER.debug(" GCBackOrderNotificationEmailServiceOnSchedule:: sendOrderDetails :: minOrderstatus ::"+minOrderStatus + "OrderLineKey ::"+strOrderLineKey+ "extnIsBackOrderEmailSent ::"+extnIsBackOrderEmailSent) ;
            }
		if ("N".equalsIgnoreCase(extnIsBackOrderEmailSent)
			|| "".equalsIgnoreCase(extnIsBackOrderEmailSent)) {
		  
		    //checking for backordered status
		    if(GCConstants.BACKORDERED_STATUS.equalsIgnoreCase(minOrderStatus))
		    {
		    
		    if ((GCSendEmailConditions.isCallCenterOrder(env,
				    orderCaptureChannel) || GCSendEmailConditions
				    .isJBSDCSourced(env, orderCaptureChannel))) {
			Document ordDoc = GCXMLUtil
				.getDocumentFromElement(ordEle);
			respDoc = formOrderOutput(env, ordDoc);

			GCChangeOrderUtil
				.addNoteAndUpdateExtnIsBackOrderEmailSent(
					env,
					GCConstants.GC_BACKORDER_NOTIFICATION_EMAIL_NOTE,
					sOrderHeaderKey, ordEle, "1300");
			emailSent = true;

		    } else {
			respDoc = GCEmailUtils.returnNoRecordFoundDoc();
		    }
		    }
		    else{
		        respDoc = GCEmailUtils.returnNoRecordFoundDoc();
		    }
		}


	    if (!emailSent) {
		respDoc = GCEmailUtils.returnNoRecordFoundDoc();
	    }

	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCBackOrderNotificationEmailServiceOnSchedule.sendOrderDetails()",
		    e);
	    throw new GCException(e);
	}
	LOGGER.debug(" Exiting GCBackOrderNotificationEmailServiceOnSchedule.sendOrderDetails() method ");
	return respDoc;
    }



    public void setProperties(Properties arg0) {

    }

    public Document formOrderOutput(YFSEnvironment env, Document inputDoc)
	    throws GCException {
	try {
	    Element orderEle = inputDoc.getDocumentElement();
	    orderEle.setAttribute(GCConstants.EMAIL_TYPE, "FTC_NOTIFICATION");
	    Document orderDoc = GCXMLUtil.getDocumentFromElement(orderEle);

	    Document backorderNotifyXSLTOutDoc = GCCommonUtil.invokeService(
		    env, GCConstants.GC_BACKORDER_NOTIFICATION_XSLT, orderDoc);
	    if(LOGGER.isDebugEnabled()){
	    LOGGER.debug("****Backorder Notification Email Output*****"
		    + GCXMLUtil.getXMLString(backorderNotifyXSLTOutDoc));
	    }
	    GCCommonUtil
		    .invokeService(
			    env,
			    GCConstants.GC_PUBLISH_TO_EMAIL_BACKORDER_NOTIFICATION_INBOUND_QUEUE,
			    backorderNotifyXSLTOutDoc);
	    return GCEmailUtils.sendSuccessMessageDoc();
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCBackOrderNotificationEmailServiceOnSchedule.formOrderOutput()",
		    e);
	    throw new GCException(e);
	}
    }

}
