/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Logic to cancel the backordered quantities after 75 days
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0			   11/11/2014		 Soni, Karan				Logic to cancel the backordered quantities after 75 days of BACKORDER_NOTIFY_REF_DATE date
             1.1               21/06/2016        Singh,Jaipreet             GCSTORE-5660: Prod: If item is cancelled by FTC timer the dependent line is not cancelled       
 *#################################################################################################################################################################
 */
package com.gc.api.email;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.gc.condition.GCSendEmailConditions;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCCancelBackOrderLines {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCCancelBackOrderLines.class);

  public void cancelBOLines (YFSEnvironment env, Document inDoc)  throws GCException {
      try {
          LOGGER.verbose("Inside cancelBOLines method");
          String strBOQty=null;
          String strOHK= GCXMLUtil.getAttributeFromXPath(inDoc, "MonitorConsolidation/Order/@OrderHeaderKey");
          String strIsFTCExcempt= GCXMLUtil.getAttributeFromXPath(inDoc, "MonitorConsolidation/Order/Extn/@ExtnFTCExempt");
          boolean bIsFTCExcempt= GCSendEmailConditions.isOrderFTCExempt(env, strIsFTCExcempt);
          if(!bIsFTCExcempt){
            Document inDocCO= GCXMLUtil.getDocument("<Order Action='Modify' OrderHeaderKey='"+strOHK+"' Override='Y'><OrderLines/></Order>");
            NodeList nlOrderLine= GCXMLUtil.getNodeListByXpath(inDoc, "MonitorConsolidation/Order/OrderLines/OrderLine");
            for(int i=0; i< nlOrderLine.getLength(); i++){
                if(LOGGER.isDebugEnabled())
                {
                    LOGGER.debug("The OHK is"+ strOHK);
                }
          
              Element eleOrderLine= (Element)nlOrderLine.item(i);
              String strOrderLineKey= eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
              String strOrderedQty= eleOrderLine.getAttribute(GCConstants.ORDERED_QTY);
              //GCSTORE-5660-starts
              String strExtnWarrantyItem=null;
              String strExtnFreeGiftItem=null;
              Document docOrderLine=GCXMLUtil.getDocumentFromElement(eleOrderLine);
              Element eleExtn=GCXMLUtil.getElementByXPath(docOrderLine, "/OrderLine/Extn");
              
              if(LOGGER.isDebugEnabled())
              { 
              LOGGER.debug("OrderLine::"+strOrderLineKey);
              }
                if(!YFCCommon.isVoid(eleExtn))
                {
                    strExtnWarrantyItem=eleExtn.getAttribute("ExtnIsWarrantyItem");
                    strExtnFreeGiftItem=eleExtn.getAttribute("ExtnIsFreeGiftItem");
                    if(LOGGER.isDebugEnabled())
                    {
                    LOGGER.debug("Extn Element :: \n"+GCXMLUtil.getElementString(eleExtn));
                    }
                    
                }
                //checking for warranty item or free gift item
              if((!YFCCommon.isVoid(strExtnWarrantyItem) && GCConstants.YES.equalsIgnoreCase(strExtnWarrantyItem)) || (!YFCCommon.isVoid(strExtnFreeGiftItem) && GCConstants.YES.equalsIgnoreCase(strExtnFreeGiftItem)))
              {
                  if(LOGGER.isDebugEnabled())
                  {
                  LOGGER.debug("It is a Warranty/Free Gift Item");
                  }
                  String strDependentOnLineKey=eleOrderLine.getAttribute("DependentOnLineKey");
                  if(!YFCCommon.isVoid(strDependentOnLineKey))
                  {
                      if(LOGGER.isDebugEnabled())
                      {
                          LOGGER.debug("strDependentOnLineKey ::"+ strDependentOnLineKey);
                      }
                    Element eleParentOrderLine=GCXMLUtil.getElementByXPath(inDoc, "/MonitorConsolidation/Order/OrderLines/OrderLine[@OrderLineKey='"+strDependentOnLineKey+ "']");
                    if(!YFCCommon.isVoid(eleParentOrderLine))  
                    { 
                        if(LOGGER.isDebugEnabled())
                        {
                            LOGGER.debug("eleParentOrderLine ::"+ GCXMLUtil.getElementString(eleParentOrderLine));
                        }
                        //if warranty/free gift item belongs to bundle parent
                           if(!YFCCommon.isVoid(eleParentOrderLine.getAttribute("KitCode")) && GCConstants.BUNDLE.equalsIgnoreCase(eleParentOrderLine.getAttribute("KitCode")))
                           {
                              NodeList nlChildOrderLine= GCXMLUtil.getNodeListByXpath(inDoc, "/MonitorConsolidation/Order/OrderLines/OrderLine[@BundleParentOrderLineKey='"+strDependentOnLineKey+"']");
                               
                              for(int k=0;k<nlChildOrderLine.getLength();k++)
                              {
                                  Element eleChildOrderLine=(Element) nlChildOrderLine.item(k);
                              
                               if(LOGGER.isDebugEnabled())
                               {
                                   LOGGER.debug("Inside for loop warranty/free gift item :: eleChildOrderLine ::"+ GCXMLUtil.getElementString(eleChildOrderLine));
                               }
                               String strChildBOQty=XPathAPI.eval(eleChildOrderLine, "sum(OrderStatuses/OrderStatus[@Status='1300' or @Status='1300.10' or @Status='1500.10']/@StatusQty)").toString();
                               if(LOGGER.isDebugEnabled())
                               {
                                   LOGGER.debug("strChildBOQty ::"+ strChildBOQty);
                               }
                               Integer intChildBOQty=Integer.parseInt(strChildBOQty); 
                               if(intChildBOQty>=1)
                               {
                                   strBOQty=XPathAPI.eval(eleParentOrderLine, "sum(OrderStatuses/OrderStatus/@StatusQty)").toString();
                                   if(LOGGER.isDebugEnabled())
                                   {
                                       LOGGER.debug("strBOQty for warranty item/free gift of bundle parent ::"+ strBOQty);
                                   }
                                   break;
                               }    
                               
                              }
                              
                           }
                           else
                           {   //warranty item/free gift item for regular item
                               strBOQty=XPathAPI.eval(eleParentOrderLine, "sum(OrderStatuses/OrderStatus[@Status='1300' or @Status='1300.10' or @Status='1500.10']/@StatusQty)").toString();  
                               if(LOGGER.isDebugEnabled())
                               {
                                   LOGGER.debug("strBOQty for warranty/free gift item of regular item ::"+ strBOQty);
                               }
                           }
                            
                      }
                   }
              }
              
              else
              {
                  String strBundleParentOrderLineKey=eleOrderLine.getAttribute("BundleParentOrderLineKey");
                  if(!YFCCommon.isVoid(strBundleParentOrderLineKey))
                  {
                      continue;
                  }
                  
                  else if(!YFCCommon.isVoid(eleOrderLine.getAttribute("KitCode")) && GCConstants.BUNDLE.equalsIgnoreCase(eleOrderLine.getAttribute("KitCode")))
                   {
                      NodeList nlChildOrderLine= GCXMLUtil.getNodeListByXpath(inDoc, "/MonitorConsolidation/Order/OrderLines/OrderLine[@BundleParentOrderLineKey='"+eleOrderLine.getAttribute("OrderLineKey")+"']");
                      
                      for(int k=0;k<nlChildOrderLine.getLength();k++)
                      {
                          Element eleChildOrderLine=(Element) nlChildOrderLine.item(k);
                      
                       if(LOGGER.isDebugEnabled())
                       {
                           LOGGER.debug("Inside for loop for bundle item cancellation :: eleChildOrderLine ::"+ GCXMLUtil.getElementString(eleChildOrderLine));
                       }
                       String strChildBOQty=XPathAPI.eval(eleChildOrderLine, "sum(OrderStatuses/OrderStatus[@Status='1300' or @Status='1300.10' or @Status='1500.10']/@StatusQty)").toString();
                       if(LOGGER.isDebugEnabled())
                       {
                           LOGGER.debug("strChildBOQty ::"+ strChildBOQty);
                       }
                       Integer intChildBOQty=Integer.parseInt(strChildBOQty); 
                       if(intChildBOQty>=1)
                       {
                           strBOQty=XPathAPI.eval(eleOrderLine, "sum(OrderStatuses/OrderStatus/@StatusQty)").toString();
                           if(LOGGER.isDebugEnabled())
                           {
                               LOGGER.debug("One of the component of Bundle is backorderd, the backorder qty for bundle is ::"+ strBOQty);
                           }
                           break;
                       }    
                       
                      }
                    }
                  
                  else
                  {
                  strBOQty= XPathAPI.eval(eleOrderLine, "sum(OrderStatuses/OrderStatus[@Status='1300' or @Status='1300.10' or @Status='1500.10']/@StatusQty)").toString();
                  }
              }
            //GCSTORE-5660-ends   
              if(!YFCCommon.isVoid(strBOQty)){
                  if(LOGGER.isDebugEnabled())
                  {
                      LOGGER.debug("The BO Qty is"+ strBOQty);
                  }
                Element eleOrderLn= inDocCO.createElement(GCConstants.ORDER_LINE);
                eleOrderLn.setAttribute(GCConstants.ORDER_LINE_KEY, strOrderLineKey);
                eleOrderLn.setAttribute(GCConstants.ORDERED_QTY , String.valueOf(Double.parseDouble(strOrderedQty) - Double.parseDouble(strBOQty)));
                Element eleOrderLines= GCXMLUtil.getElementByXPath(inDocCO, "Order/OrderLines");
                eleOrderLines.appendChild(eleOrderLn);

              }
              strBOQty=null;
            }
            Element eleOL= GCXMLUtil.getElementByXPath(inDocCO, "Order/OrderLines/OrderLine");
            if(!YFCCommon.isVoid(eleOL)){
              if (LOGGER.isDebugEnabled()){
                LOGGER.debug("The Change Order document is"+ GCXMLUtil.getXMLString(inDocCO));
              }
              GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, inDocCO);
            }
            LOGGER.verbose("Exiting cancelBOLines method");
          }
        } catch (Exception e) {
          LOGGER.error(
              "Inside catch block of GCCancelBackOrderLines.cancelBOLines(), Exception is :",
              e);
          throw new GCException(e);
        }
      }
  

}
