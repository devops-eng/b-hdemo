/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 * *#################################################################################################################################################################
 * OBJECTIVE: This class is invoked on every email service to stamp latest customer e-mail ID on the incoming document. 
 * #################################################################################################################################################################
 * Version                     Date                            Modified By                              Description
 * #################################################################################################################################################################
 * 1.0                       03/24/2015                        Saxena, Ekansh                           Stamp latest Customer EMailID on the input
 * #################################################################################################################################################################
 */
package com.gc.api.email;

import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class is invoked on every email service to stamp latest customer e-mail ID on the incoming
 * document.
 *
 * @author Ekansh, Saxena
 *
 */

public class GCEmailPreProcessing implements YIFCustomApi {

	// initialize logger
	private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCEmailPreProcessing.class);


	/**
	 * This method takes the latest email ID from the customer list and stamps it on the incoming document.
	 * 
	 * @param env
	 * @param doc
	 * @return
	 */

	public Document preProcessEmailMessage(YFSEnvironment env, Document doc) {

		LOGGER.beginTimer("GCEmailPreProcessing.preProcessEmailMessage");
		YFCDocument inDoc = YFCDocument.getDocumentFor(doc);

		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Incoming XML Doc" + inDoc.toString());
		}

		String sBillToID;
		String sEmailID;
		YFCElement eleRootOfInDoc = inDoc.getDocumentElement();
		sBillToID = eleRootOfInDoc.getAttribute(GCConstants.BILL_TO_ID);
		if (YFCCommon.isStringVoid(sBillToID)) {
			String sOrderHeaderKey = eleRootOfInDoc.getAttribute(GCConstants.ORDER_HEADER_KEY);
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "' />");
			YFCDocument getOrderListTemplate =
					YFCDocument.getDocumentFor("<OrderList><Order OrderHeaderKey='' BillToID='' /></OrderList>");
			YFCDocument getOrderListOutput =
					GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListInput, getOrderListTemplate);
			YFCElement eleOrderList = getOrderListOutput.getDocumentElement();
			YFCElement eleOrder = eleOrderList.getChildElement(GCConstants.ORDER);
			sBillToID = eleOrder.getAttribute(GCConstants.BILL_TO_ID);
		}

		String sDocumentType = eleRootOfInDoc.getAttribute(GCConstants.DOCUMENT_TYPE);

		if (YFCCommon.equalsIgnoreCase(sDocumentType, GCConstants.RETURN_ORDER_DOCUMENT_TYPE)) {
			YFCElement orderLineEle = eleRootOfInDoc.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
			String derivedFromOrdHdrKey = orderLineEle.getAttribute(GCConstants.DERIVED_FROM_ORDER_HEADER_KEY);
			if (!YFCCommon.isVoid(derivedFromOrdHdrKey)) {
				YFCDocument inDocOrderList =
						YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + derivedFromOrdHdrKey + "'/>");
				YFCDocument orderListTemp =
						YFCDocument.getDocumentFor("<OrderList><Order OrderHeaderKey='' BillToID='' /></OrderList>");
				YFCDocument outDocOrderList =
						GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, inDocOrderList, orderListTemp);
				YFCElement orderListEle = outDocOrderList.getDocumentElement();
				YFCElement orderEle = orderListEle.getChildElement(GCConstants.ORDER);
				sBillToID = orderEle.getAttribute(GCConstants.BILL_TO_ID);
			}
		}

		// Throw exception if BillToID not found
		/*   if(YFCCommon.isStringVoid(sBillToID)){
      YFCException e = new YFCException(GCConstants.ERROR_DESC, "Bill To ID not found.");
      throw e;
    }
		 */

		if(!YFCCommon.isStringVoid(sBillToID)){
			YFCDocument getCustomerListInput = YFCDocument.getDocumentFor("<Customer CustomerID='" + sBillToID + "'/>");
			YFCDocument getCustomerListTemplate =
					YFCDocument.getDocumentFor(GCConstants.GET_CUSTOMER_LIST_TEMPLATE_FOR_EMAIL_ID);
			YFCDocument getCustomerListOutput =
					GCCommonUtil.invokeAPI(env, GCConstants.GET_CUSTOMER_LIST, getCustomerListInput, getCustomerListTemplate);
			YFCElement eleCustomerList = getCustomerListOutput.getDocumentElement();
			YFCElement eleCustomer = eleCustomerList.getChildElement(GCConstants.CUSTOMER);
			if(!YFCCommon.isVoid(eleCustomer)){
				YFCElement eleCustomerContactList =eleCustomer.getChildElement(GCConstants.CUSTOMER_CONTACT_LIST);
				if(!YFCCommon.isVoid(eleCustomerContactList)){
					YFCElement eleCustomerContact =	eleCustomerContactList.getChildElement(GCConstants.CUSTOMER_CONTACT);
					if(!YFCCommon.isVoid(eleCustomerContact)){
						sEmailID = eleCustomerContact.getAttribute("EmailID");
						eleRootOfInDoc.setAttribute(GCConstants.CUSTOMER_EMAIL_ID, sEmailID);
					}
				}
			}
			// Stamp updated EMailID on the inputDoc

		}


		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Modified inputDoc " + inDoc.toString());
		}
		LOGGER.endTimer("GCEmailPreProcessing.preProcessEmailMessage");
		return inDoc.getDocument();
	}

	@Override
	public void setProperties(Properties arg0) {

	}
}
