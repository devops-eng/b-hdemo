package com.gc.api.email;

import java.util.Map;
import java.util.Map.Entry;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class formats the order details to send Order Cancellation message
 *
 * @author rajiv.sinha
 *
 */
public class GCFormatCancellationEmail {


  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCFormatCancellationEmail.class.getName());

  public Document getFormattedDocForEmail(YFSEnvironment env, Document doc) {
    LOGGER.beginTimer("getFormattedDocForEmail");
    YFCDocument inputDoc = YFCDocument.getDocumentFor(doc);
    YFCDocument outputDoc = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement orderOutEle = outputDoc.getDocumentElement();
    YFCElement orderInEle = inputDoc.getDocumentElement();
    copyOrderHeaderAttribute(orderInEle, orderOutEle);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("After copyOrderHeaderAttribute output doc is=\n" + outputDoc);
    }
    YFCElement extnInEle = orderInEle.getChildElement(GCConstants.EXTN);
    if(!YFCCommon.isVoid(extnInEle)){
      YFCElement extnOutEle = orderOutEle.createChild(GCConstants.EXTN);
      GCXMLUtil.copyAttributes(extnInEle, extnOutEle);
    }

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("After Extn element output doc is=\n" + outputDoc);
    }

    YFCElement orderLinesInEle = orderInEle.getChildElement(GCConstants.ORDER_LINES);
    if (!YFCCommon.isVoid(orderLinesInEle)) {
      YFCElement orderLinesOutEle = orderOutEle.createChild(GCConstants.ORDER_LINES);
      YFCNodeList<YFCElement> orderLineInNL = orderLinesInEle.getElementsByTagName(GCConstants.ORDER_LINE);
      for (YFCElement orderLineInEle : orderLineInNL) {
        YFCElement orderLineOutEle = orderLinesOutEle.createChild(GCConstants.ORDER_LINE);
        copyOrderLineDetails(orderLineInEle, orderLineOutEle);
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("After copying line \n" + orderLineOutEle + "\n output doc is=\n" + outputDoc);
        }
      }
    }

    YFCElement paymentMethodsToInEle = orderInEle.getChildElement("PaymentMethods");
    if(!YFCCommon.isVoid(paymentMethodsToInEle)){
      YFCElement paymentMethodsToOutEle = orderOutEle.createChild("PaymentMethods");
      copyElement(outputDoc, paymentMethodsToInEle, paymentMethodsToOutEle);
    }

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("After paymentMethodsToInEle element output doc is=\n" + outputDoc);
    }

    YFCElement personInfoShipToInEle = orderInEle.getChildElement("PersonInfoShipTo");
    if(!YFCCommon.isVoid(personInfoShipToInEle)){
      YFCElement personInfoShipToOutEle =orderOutEle.createChild("PersonInfoShipTo");
      copyElement(outputDoc, personInfoShipToInEle, personInfoShipToOutEle);
    }

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("After personInfoShipToInEle element output doc is=\n" + outputDoc);
    }

    YFCElement personInfoBillToInEle = orderInEle.getChildElement("PersonInfoBillTo");
    if(!YFCCommon.isVoid(personInfoBillToInEle)){
      YFCElement personInfoBillToOutEle =orderOutEle.createChild("PersonInfoBillTo");
      copyElement(outputDoc, personInfoBillToInEle, personInfoBillToOutEle);
    }

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("After personInfoBillToInEle element output doc is=\n" + outputDoc);
    }

    YFCElement overallTotalsInEle = orderInEle.getChildElement("OverallTotals");
    if (!YFCCommon.isVoid(overallTotalsInEle)) {
      YFCElement overallTotalsOutEle = orderOutEle.createChild("OverallTotals");
      copyElement(outputDoc, overallTotalsInEle, overallTotalsOutEle);
    }

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Finally the return doc is=\n" + outputDoc);
    }
    LOGGER.endTimer("getFormattedDocForEmail");
    return outputDoc.getDocument();
  }


  /**
   * This methods copy OrderLine level Attributes and Element
   *
   * @param orderLineInEle
   * @param orderLineOutEle
   */

  private void copyOrderLineDetails(YFCElement orderLineInEle, YFCElement orderLineOutEle) {
    LOGGER.beginTimer("copyOrderLineAttributes");
    orderLineOutEle.setAttribute("CancelledQty", orderLineInEle.getAttribute("CancelledQty"));
    orderLineOutEle.setAttribute("PrimeLineNo", orderLineInEle.getAttribute("PrimeLineNo"));
    orderLineOutEle.setAttribute("SubLineNo", orderLineInEle.getAttribute("SubLineNo"));
    orderLineOutEle.setAttribute("KitCode", orderLineInEle.getAttribute("KitCode"));
    orderLineOutEle.setAttribute("OriginalOrderedQty", orderLineInEle.getAttribute("OriginalOrderedQty"));
    orderLineOutEle.setAttribute("RemainingQty", orderLineInEle.getAttribute("RemainingQty"));
    orderLineOutEle.setAttribute("ShipNode", orderLineInEle.getAttribute("ShipNode"));
    orderLineOutEle.setAttribute("Status", orderLineInEle.getAttribute("Status"));
    orderLineOutEle.setAttribute("StatusQuantity", orderLineInEle.getAttribute("StatusQuantity"));
    orderLineOutEle.setAttribute("OrderLineKey", orderLineInEle.getAttribute("OrderLineKey"));

    YFCElement itemInEle = orderLineInEle.getChildElement(GCConstants.ITEM);
    if(!YFCCommon.isVoid(itemInEle)){
      YFCElement itemOutEle =orderLineOutEle.createChild(GCConstants.ITEM);
      copyElement(orderLineOutEle.getOwnerDocument(), itemInEle, itemOutEle);
    }

    YFCElement lineChrgesInEle = orderLineInEle.getChildElement(GCConstants.LINE_CHARGES);
    if(!YFCCommon.isVoid(lineChrgesInEle)){
      YFCElement lineChrgesOutEle =orderLineOutEle.createChild(GCConstants.LINE_CHARGES);
      copyElement(orderLineOutEle.getOwnerDocument(), lineChrgesInEle, lineChrgesOutEle);
    }

    YFCElement lineTaxesInEle = orderLineInEle.getChildElement(GCConstants.LINE_TAXES);
    if(!YFCCommon.isVoid(lineTaxesInEle)){
      YFCElement lineTaxesOutEle =orderLineOutEle.createChild(GCConstants.LINE_TAXES);
      copyElement(orderLineOutEle.getOwnerDocument(), lineTaxesInEle, lineTaxesOutEle);
    }

    String dependentOrderLineKey = orderLineInEle.getAttribute("DependentOnLineKey");
    if(!YFCCommon.isVoid(dependentOrderLineKey)){
      YFCElement dependencyOutEle = orderLineOutEle.createChild("Dependency");
      dependencyOutEle.setAttribute("DependentOnLineKey", dependentOrderLineKey);
    }

    YFCElement bundleParentLineInEle = orderLineInEle.getChildElement(GCConstants.BUNDLE_PARENT_LINE);
    if(!YFCCommon.isVoid(bundleParentLineInEle)){
      YFCElement bundleParentLineOutEle =orderLineOutEle.createChild(GCConstants.BUNDLE_PARENT_LINE);
      copyElement(orderLineOutEle.getOwnerDocument(), bundleParentLineInEle, bundleParentLineOutEle);
    }

    YFCElement lineOverallTotalsInEle = orderLineInEle.getChildElement("LineOverallTotals");
    if(!YFCCommon.isVoid(lineOverallTotalsInEle)){
      YFCElement lineOverallTotalsOutEle =orderLineOutEle.createChild("LineOverallTotals");
      copyElement(orderLineOutEle.getOwnerDocument(), lineOverallTotalsInEle, lineOverallTotalsOutEle);
    }

    YFCElement personInfoShipToInEle = orderLineInEle.getChildElement("PersonInfoShipTo");
    if(!YFCCommon.isVoid(personInfoShipToInEle)){
      YFCElement personInfoShipToOutEle =orderLineOutEle.createChild("PersonInfoShipTo");
      copyElement(orderLineOutEle.getOwnerDocument(), personInfoShipToInEle, personInfoShipToOutEle);
    }
    LOGGER.endTimer("copyOrderLineAttributes");
  }

  /**
   * This method copy order header attributes
   *
   * @param orderInEle
   * @param orderOutEle
   */

  private void copyOrderHeaderAttribute(YFCElement orderInEle, YFCElement orderOutEle) {
    LOGGER.beginTimer("copyOrderHeaderAttribute");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("input doc is =\n" + orderInEle);
    }
    String sEmailType = orderInEle.getAttribute(GCConstants.EMAIL_TYPE);
    String sCustomerEmailID= orderInEle.getAttribute(GCConstants.CUSTOMER_EMAIL_ID);
    String orderDate=orderInEle.getAttribute(GCConstants.ORDER_DATE);
    String orderNo= orderInEle.getAttribute(GCConstants.ORDER_NO);
    String status= orderInEle.getAttribute(GCConstants.STATUS);
    String entryType=orderInEle.getAttribute(GCConstants.ENTRY_TYPE);
    String enterpriseCode=orderInEle.getAttribute(GCConstants.ENTERPRISE_CODE);
    String sellerOrgCode=orderInEle.getAttribute(GCConstants.SELLER_ORGANIZATION_CODE);

    orderOutEle.setAttribute(GCConstants.EMAIL_TYPE,sEmailType);
    orderOutEle.setAttribute(GCConstants.CUSTOMER_EMAIL_ID, sCustomerEmailID);
    orderOutEle.setAttribute(GCConstants.ORDER_DATE, orderDate);
    orderOutEle.setAttribute(GCConstants.ORDER_NO, orderNo);
    orderOutEle.setAttribute(GCConstants.STATUS, status);
    orderOutEle.setAttribute(GCConstants.ENTRY_TYPE, entryType);
    orderOutEle.setAttribute(GCConstants.ENTERPRISE_CODE,enterpriseCode);
    orderOutEle.setAttribute(GCConstants.SELLER_ORGANIZATION_CODE, sellerOrgCode);

    if(LOGGER.isVerboseEnabled()){
      LOGGER.verbose("After setting order header attribute the output doc is =\n"+orderOutEle);
    }

    LOGGER.endTimer("copyOrderHeaderAttribute");
  }

  /**
   *
   * This method copy one element to another element object which can be appended to a new document.
   *
   * @param destDoc : Document in which new element is to be added.
   * @param srcElem : Element to be copied.
   * @param destElem : Copy of the original element.
   *
   */
  public static void copyElement(YFCDocument destDoc, YFCElement srcElem, YFCElement destElem) {
    Map<String, String> attrMap = srcElem.getAttributes();
    for (Entry<String, String> entry : attrMap.entrySet()) {
      String attrName = entry.getKey();
      String attrValue = entry.getValue();
      destElem.setAttribute(attrName, attrValue);
    }

    if (srcElem.hasChildNodes()) {
      YFCNodeList<YFCNode> childList = srcElem.getChildNodes();
      for (YFCNode eleChild : childList) {
        if (eleChild instanceof YFCElement) {
          YFCElement childSrcElem = (YFCElement) eleChild;
          YFCElement childDestElem = appendChild(destDoc, destElem, childSrcElem.getNodeName());
          copyElement(destDoc, childSrcElem, childDestElem);
        }
      }
    }
  }

  /**
   *
   * This method is for adding child Nodes to parent YFC node element, the child element has to be
   * created first.
   *
   * @param doc : the document in which element is to be appended.
   * @param parentElement : Parent Element under which the new Element should be present
   * @param elementName : Name of the element to be created
   * @param value : Can be either a String ,just the element value if it is a single attribute OR
   *        Hashtable containing the name-value pair of multiple attributes.
   * @return : childElement
   *
   */
  public static YFCElement appendChild(YFCDocument doc, YFCElement parentElement, String elementName) {
    YFCElement childElement = parentElement.createChild(elementName);
    return childElement;
  }

}
