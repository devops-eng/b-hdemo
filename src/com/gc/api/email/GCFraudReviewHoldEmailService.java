/**
 * Copyright 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Publishes order details to TIBCO queue for sending fraud review hold email to the customer
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
 *          1.0                05/20/2014        Susai, John Melvin			Initial Version
 *#################################################################################################################################################################
 */
package com.gc.api.email;

import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCChangeOrderUtil;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCEmailUtils;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.gc.condition.GCSendEmailConditions;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="john.melvin@expicient.com">John Melvin</a>
 */
public class GCFraudReviewHoldEmailService implements YIFCustomApi {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCFraudReviewHoldEmailService.class.getName());

    /**
     * 
     * Description of sendReturnDetails Publishes order details to TIBCO queue
     * for sending email to the customer
     * 
     * @param env
     * @param inDoc
     * @return
     * @throws GCException
     * 
     */
    public Document sendFraudNoteDetails(YFSEnvironment env, Document inDoc)
	    throws GCException {
	LOGGER.debug(" Entering sendFraudNoteDetails() method ");
	if(LOGGER.isDebugEnabled()){
	LOGGER.debug(" sendFraudNoteDetails() method, Input document is"
		+ GCXMLUtil.getXMLString(inDoc));
	}
	Document respDoc = null;
	try {

	    String sOrderHeaderKey = ((Element) inDoc.getElementsByTagName(
		    "Order").item(0))
		    .getAttribute(GCConstants.ORDER_HEADER_KEY);

	    String sGetOrderListInDoc = "<Order OrderHeaderKey=\""
		    + sOrderHeaderKey + "\"></Order>";
	    Document getOrderListInDoc = GCXMLUtil
		    .getDocument(sGetOrderListInDoc);

	    Element eleRoot = getOrderListInDoc.getDocumentElement();
	    Element eleOrderHoldType = getOrderListInDoc
		    .createElement("OrderHoldType");
	    eleRoot.appendChild(eleOrderHoldType);
	    eleOrderHoldType.setAttribute(GCConstants.HOLD_TYPE,
		    GCConstants.FRAUD_REVIEW);

	    Document getOrderListOutDoc = GCCommonUtil
		    .invokeAPI(
			    env,
			    GCConstants.GET_ORDER_LIST,
			    getOrderListInDoc,
			    GCCommonUtil
				    .getDocumentFromPath("/global/template/api/getOrderList_For_FraudVerification.xml"));

	    String nTotalOrdList = getOrderListOutDoc.getDocumentElement()
		    .getAttribute("TotalOrderList");

	    if (Double.parseDouble(nTotalOrdList) == 0) {
		respDoc = GCXMLUtil.createDocument("Response");
		Element respEle = respDoc.getDocumentElement();
		respEle.setAttribute("ResponseCode", "ERROR");
		respEle.setAttribute("ResponseDescription", "No Record Found");
		return respDoc;
	    } else {
		Element ordEle = (Element) getOrderListOutDoc
			.getElementsByTagName("Order").item(0);
		Element extnEle = (Element) ordEle.getElementsByTagName("Extn")
			.item(0);
		String orderCaptureChannel = extnEle
			.getAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL);

		if (GCSendEmailConditions.isCallCenterOrder(env,
			orderCaptureChannel)
			|| GCSendEmailConditions.isJBSDCSourced(env,
				orderCaptureChannel)) {
		    Node orderLinesEle = ordEle.getElementsByTagName(
			    "OrderLines").item(0);
		    ordEle.removeChild(orderLinesEle);

		    Document ordDoc = GCXMLUtil.getDocumentFromElement(ordEle);
		    respDoc = formOrderOutput(env, ordDoc);

		    GCChangeOrderUtil.addNoteToOrder(env,
			    GCConstants.GC_FRAUD_REVIEW_HOLD_EMAIL_NOTE,
			    sOrderHeaderKey);
		} else {
		    respDoc = GCEmailUtils.returnNoRecordFoundDoc();
		}

	    }

	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCFraudReviewHoldEmailService.sendFraudNoteDetails()",
		    e);
	    throw new GCException(e);
	}
	LOGGER.debug(" Exiting GCFraudReviewHoldEmailService.sendFraudNoteDetails() method ");
	return respDoc;
    }

    public void setProperties(Properties arg0) {

    }

    public Document formOrderOutput(YFSEnvironment env, Document inputDoc)
	    throws GCException {
	try {
	    Element ordEle = inputDoc.getDocumentElement();
	    ordEle.setAttribute(GCConstants.EMAIL_TYPE, "FRAUD_NOTE");
	    Document orderDoc = GCXMLUtil.getDocumentFromElement(ordEle);
	    GCCommonUtil
		    .invokeService(
			    env,
			    GCConstants.GC_PUBLISH_TO_EMAIL_FRAUD_VERIFICATION_INBOUND_QUEUE,
			    orderDoc);
	    return GCEmailUtils.sendSuccessMessageDoc();
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCFraudReviewHoldEmailService.formOrderOutput()",
		    e);
	    throw new GCException(e);
	}
    }

}
