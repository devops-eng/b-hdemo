/**
 * Copyright 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Publishes order details to TIBCO queue for sending order cancellation email to the customer
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
 *          1.0                05/20/2014        Susai, John Melvin			Initial Version
 *#################################################################################################################################################################
 */
package com.gc.api.email;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCChangeOrderUtil;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCEmailUtils;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.gc.condition.GCSendEmailConditions;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="john.melvin@expicient.com">John Melvin</a>
 */
public class GCOrderCancellationEmailService implements YIFCustomApi {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCOrderCancellationEmailService.class.getName());

    /**
     * 
     * Description of sendOrderCancellationDetails Publishes order details to
     * TIBCO queue for sending email to the customer
     * 
     * @param env
     * @param inDoc
     * @return
     * @throws Exception
     * 
     */
    public Document sendOrderCancellationDetails(YFSEnvironment env,
	    Document inDoc) throws GCException {
	LOGGER.debug(" Entering sendOrderCancellationDetails() method ");
	if(LOGGER.isDebugEnabled()){
	LOGGER.debug(" sendOrderCancellationDetails() method, Input document is"
		+ GCXMLUtil.getXMLString(inDoc));
	}
	Document respDoc = null;
	try {

	    String sOrderHeaderKey = ((Element) inDoc.getElementsByTagName(
		    "Order").item(0))
		    .getAttribute(GCConstants.ORDER_HEADER_KEY);
	    String sGetOrderListInDoc = "<Order OrderHeaderKey=\""
		    + sOrderHeaderKey + "\"></Order>";
	    Document getOrderListInDoc = GCXMLUtil
		    .getDocument(sGetOrderListInDoc);

	    Document getOrderListOutDoc = GCCommonUtil
		    .invokeAPI(
			    env,
			    GCConstants.GET_ORDER_LIST,
			    getOrderListInDoc,
			    GCCommonUtil
				    .getDocumentFromPath("/global/template/api/getOrderList_For_OrderCancellation_Email.xml"));

	    Element ordEle = (Element) getOrderListOutDoc.getElementsByTagName(
		    "Order").item(0);
	    ordEle.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
	    Element extnEle = (Element) ordEle.getElementsByTagName("Extn")
		    .item(0);
	    String orderCaptureChannel = extnEle
		    .getAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL);
	    String draftOrdflag = ordEle.getAttribute("DraftOrderFlag");

	    if ((GCSendEmailConditions.isCallCenterOrder(env,
		    orderCaptureChannel) || GCSendEmailConditions
		    .isJBSDCSourced(env, orderCaptureChannel))
		    && "N".equalsIgnoreCase(draftOrdflag)) {
		Document ordDoc = GCXMLUtil.getDocumentFromElement(ordEle);
		respDoc = formOrderOutput(env, ordDoc);
	    } else {
		respDoc = GCEmailUtils.returnNoRecordFoundDoc();
	    }

	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCOrderCancellationEmailService.sendOrderCancellationDetails()",
		    e);
	    throw new GCException(e);
	}
	LOGGER.debug(" Exiting GCOrderCancellationEmailService.sendOrderCancellationDetails() method ");
	return respDoc;
    }

    public void setProperties(Properties arg0) {

    }

    public Document formOrderOutput(YFSEnvironment env, Document inputDoc)
	    throws GCException {
	try {
		final String logInfo = "GCOrderCancellationEmailService :: formOrderOutout :: ";
		LOGGER.verbose("Input doc to formOrderOutput function" + GCXMLUtil.getXMLString(inputDoc));
		//Fix for Defect-2988
		Object obCancellationFlag=env.getTxnObject("DoNotSendCancellationEMail");
		String sCancellationFlag="";
		if(!YFCCommon.isVoid(obCancellationFlag)){
			sCancellationFlag = (String)obCancellationFlag;
		}
		LOGGER.verbose("sCancellationFlag" + sCancellationFlag);
		if(!YFCCommon.equalsIgnoreCase(sCancellationFlag, "Y")){
			Element ordEle = inputDoc.getDocumentElement();
			ordEle.setAttribute(GCConstants.EMAIL_TYPE, "ORDER_CANCELLATION");
			Document ordDoc = GCXMLUtil.getDocumentFromElement(ordEle);
			
			//GCSTORE-7056- Starts			
				
				 Element eleOrdLines = (Element) ordDoc.getElementsByTagName(
						    "OrderLines").item(0);			       
			      
			      NodeList nlOrderLine = eleOrdLines.getElementsByTagName(GCConstants.ORDER_LINE);			      
			     
			      for (int i = 0; i < nlOrderLine.getLength(); i++) {
			    	  
			        Element eleOrderLine = (Element) nlOrderLine.item(i);
			        Element eleItemID = (Element) eleOrderLine.getElementsByTagName(GCConstants.ITEM).item(0);
			        String sItemIDOnOrder = eleItemID.getAttribute("ItemID");
			        Document docGetCommonCodeListIp = GCXMLUtil
					          .createDocument(GCConstants.COMMON_CODE);
					      Element eleGetCommonCodeListIpRoot = docGetCommonCodeListIp
					          .getDocumentElement();
					      eleGetCommonCodeListIpRoot.setAttribute(GCConstants.CODE_TYPE,
					          "SPECIAL_ORDER_ITEM");
					      LOGGER.debug(logInfo + "docGetCommonCodeListIp\n"
					          + GCXMLUtil.getXMLString(docGetCommonCodeListIp));
					      Document docGetCommonCodeListOp = GCCommonUtil.invokeAPI(env,
					          GCConstants.GET_COMMON_CODE_LIST, docGetCommonCodeListIp);
					      LOGGER.debug(logInfo + "docGetCommonCodeListOp\n"
					          + GCXMLUtil.getXMLString(docGetCommonCodeListOp));
					      
					      Element eleCommonCodeList = docGetCommonCodeListOp.getDocumentElement();
					      NodeList nlCommonCode = eleCommonCodeList.getElementsByTagName(GCConstants.COMMON_CODE);
					      
					      for (int j = 0; j < nlCommonCode.getLength(); j++) {
						        Element eleCommonCode = (Element) nlCommonCode.item(j);	
						        String sSspecialItemID = eleCommonCode.getAttribute(GCConstants.CODE_VALUE);					        
						       if(YFCCommon.equals(sSspecialItemID, sItemIDOnOrder))
						       {						    	     
						    	   eleOrdLines.removeChild(eleOrderLine) ;
						    	   i--;
						    	  break;
						       }					       
					  }			             
			}
			boolean bIsCancelledQty= getCancelledQtyUpdated(env, ordDoc);

			if(bIsCancelledQty)
			{
			Document ordCancelXSLTOutDoc = GCCommonUtil.invokeService(env,
					GCConstants.GC_ORDER_CANCELLATION_XSLT, ordDoc);
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("****Order Cancellation Output*****"
						+ GCXMLUtil.getXMLString(ordCancelXSLTOutDoc));
			}
			GCCommonUtil
			.invokeService(
					env,
					GCConstants.GC_PUBLISH_TO_EMAIL_ORDER_CANCELLATION_INBOUND_QUEUE,
					ordCancelXSLTOutDoc);
			LOGGER.verbose("Email posted to the Queue");

			// Adding Notes
		    if (!YFCCommon.equalsIgnoreCase("",
		    		ordEle.getAttribute("OrderHeaderKey"))) {
		    	LOGGER.verbose("Inside condtion to add Cancellation Notes on order");
			GCChangeOrderUtil.addNoteToOrder(env,
				GCConstants.GC_ORDER_CANCELLATION_EMAIL_NOTE,
				ordEle.getAttribute("OrderHeaderKey"));
		    }
			return GCEmailUtils.sendSuccessMessageDoc();
		}
		//Defect-2988 Ends Here 
		else return GCEmailUtils.returnNoRecordFoundDoc();
		}
		
			return GCEmailUtils.sendSuccessMessageDoc();
			
			//GCSTORE-7056- ends
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCOrderCancellationEmailService.formOrderOutput()",
		    e);
	    throw new GCException(e);
	}
    }

    /** Fix for GCSTORE-3021
     * This method updates Cancelled quantity of the order which has been cancelled by user i.e. Total cancelled qty - qty cancelled during split
     * @param env
     * @param ordDoc
     */
	private boolean getCancelledQtyUpdated(YFSEnvironment env, Document ordDoc) {
		LOGGER.beginTimer("getCancelledQtyUpdated");
		YFCDocument inputDoc = YFCDocument.getDocumentFor(ordDoc);
		if(LOGGER.isVerboseEnabled()){
			LOGGER.verbose("Input to getCancelledQtyUpdated\n"+inputDoc);
		}
		YFCElement rootEle = inputDoc.getDocumentElement();
		YFCElement orderLinesEle = rootEle.getChildElement(GCConstants.ORDER_LINES);
		if(YFCCommon.isVoid(orderLinesEle)){
			LOGGER.verbose("OrderLines is void so returning from getCancelledQtyUpdated");
			LOGGER.endTimer("getCancelledQtyUpdated");
			return false ;
		}
			//7056 starts
		YFCNodeList<YFCElement> orderLineNL = orderLinesEle.getElementsByTagName(GCConstants.ORDER_LINE);
		double length= orderLineNL.getLength();
		
		if(YFCCommon.equals(length, 0.00))
			{
			return false ;
			}
		
		//GCSTORE-7056- ends
		for(YFCElement orderLineEle:orderLineNL ){
			YFCElement orderStatusesEle = orderLineEle.getChildElement(GCConstants.ORDER_STATUSES);
			if(LOGGER.isVerboseEnabled()){
				LOGGER.verbose("Going to work with OrderLine \n"+orderLineEle);
			}
			if(YFCCommon.isVoid(orderStatusesEle)){
				LOGGER.verbose("OrderStatuses is missing in this OrderLine so going for next OrderLine");
				continue;
			}
				
			YFCNodeList<YFCElement> orderLineStatusNL = orderStatusesEle.getElementsByTagName(GCConstants.ORDER_STATUS);
			double cancelledQty=0.0;
			for(YFCElement orderLineStatus:orderLineStatusNL ){
				if(YFCCommon.equals(orderLineStatus.getAttribute(GCConstants.STATUS), GCConstants.CANCELLED_ORDER_STATUS_CODE,false)){
					double cancelledStatusQty = orderLineStatus.getDoubleAttribute(GCConstants.STATUS_QTY,0.0);
					LOGGER.verbose("Cancelled status is found with qty="+cancelledStatusQty);
					cancelledQty = cancelledQty + cancelledStatusQty;
				}
			}
			double qtyCancelledInSplit = 0.0;
			YFCElement orderLineExtn = orderLineEle.getChildElement(GCConstants.EXTN);
			if(!YFCCommon.isVoid(orderLineExtn)){
				qtyCancelledInSplit = orderLineExtn.getDoubleAttribute("ExtnQtyCancelledInSplit",0.0);
				LOGGER.verbose("Extn element found with ExtnQtyCancelledInSplit as "+ qtyCancelledInSplit);
				orderLineEle.removeChild(orderLineExtn);
				LOGGER.verbose("Removed Extn element");
			}
			orderLineEle.setAttribute("CancelledQty", ""+(cancelledQty-qtyCancelledInSplit));
			orderLineEle.removeChild(orderStatusesEle);
			LOGGER.verbose("Removed OrderStatuses element");
			
		}
		if(LOGGER.isVerboseEnabled()){
			LOGGER.verbose("Output from getCancelledQtyUpdated\n"+inputDoc);
		}
		LOGGER.endTimer("getCancelledQtyUpdated");
		return true;
		
	}

}
