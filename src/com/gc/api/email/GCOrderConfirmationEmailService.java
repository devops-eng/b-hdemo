/**
 * Copyright ¬© 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Processes the holds present on order
 *#################################################################################################################################################################
 *	        Version			   	Date				Modified By             	Description
 *#################################################################################################################################################################
 1.0               	21/05/2014        	Nainar, Alex               	Order Confirmation
 1.1				06/02/2014			Susai, John Melvin			Refactoring Adding Notes
 1.2                09/09/2014          Gupta, Himanshu             Code Re-factoring
 1.3                13/02/2015          Kumar Rakesh                OMS-4655 Order Confirmation Email is incorrect for kit items
 *#################################################################################################################################################################
 */
package com.gc.api.email;

import java.util.Properties;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCChangeOrderUtil;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCEmailUtils;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="alex.nainar@expicient.com">Alex Nainar</a>
 */
public class GCOrderConfirmationEmailService implements YIFCustomApi {

    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCOrderConfirmationEmailService.class.getName());

    /**
     * 
     * Description of sendOrderDetails Send Order Confirmation Email and Add
     * Notes on the Order
     * 
     * @param env
     * @param inDoc
     * @return
     * @throws Exception
     * 
     */
    public Document sendOrderDetails(YFSEnvironment env, Document inDoc)
	    throws GCException {
	LOGGER.debug(" Entering sendOrderDetails() method ");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" sendOrderDetails() method, Input document is"
		    + GCXMLUtil.getXMLString(inDoc));
	}
	Document response = null;
	try {
	    String sOrderHeaderKey = inDoc.getDocumentElement().getAttribute(
		    GCConstants.ORDER_HEADER_KEY);
	    String emailType = inDoc.getDocumentElement().getAttribute(
		    GCConstants.EMAIL_TYPE);

	    if (YFCCommon.equalsIgnoreCase("ORDER_CONFIRMATION", emailType)) {
		// getOrderList call
		Document getOrderListInDoc = GCXMLUtil
			.getDocument("<Order OrderHeaderKey=\""
				+ sOrderHeaderKey + "\"></Order>");
		Document getOrderListTemp=GCCommonUtil
				.getDocumentFromPath("/global/template/api/getOrderList_For_OrderConfirmation_Email.xml");
		if (LOGGER.isDebugEnabled()) {
		    LOGGER.debug(" getOrderList Template:"
			    + GCXMLUtil.getXMLString(getOrderListTemp));
		}
		Document getOrderListOutDoc = GCCommonUtil
			.invokeAPI(
				env,
				GCConstants.GET_ORDER_LIST,
				getOrderListInDoc,getOrderListTemp);
		if (LOGGER.isDebugEnabled()) {
		    LOGGER.debug(" Output getOrderList:"
			    + GCXMLUtil.getXMLString(getOrderListOutDoc));
		}

		String nTotalOrdList = getOrderListOutDoc.getDocumentElement()
			.getAttribute("TotalOrderList");

		if (Double.parseDouble(nTotalOrdList) == 0) {
		    return GCEmailUtils.returnNoRecordFoundDoc();
		} else {
		    Element ordetElem = (Element) getOrderListOutDoc
			    .getElementsByTagName("Order").item(0);
		    Document orderDoc = GCXMLUtil
			    .getDocumentFromElement(ordetElem);
		    response = formOrderOutput(env, orderDoc);
		}
	    } else {
		response = formOrderOutput(env, inDoc);
	    }
	} catch (Exception e) {
	    LOGGER.error(" Inside catch of sendOrderDetails", e);
	    throw new GCException(e);
	}
	LOGGER.debug(" Exiting sendOrderDetails method ");
	return response;
    }

    public void setProperties(Properties arg0) {
    }

    public Document formOrderOutput(YFSEnvironment env, Document inputDoc)
	    throws GCException {
    	if (LOGGER.isDebugEnabled()) {
		    LOGGER.debug(" formOrderOutput Input Document:"
			    + GCXMLUtil.getXMLString(inputDoc));
		}
	try {
	    Element orderElem = (Element) inputDoc
		    .getElementsByTagName("Order").item(0);
	    orderElem
		    .setAttribute(GCConstants.EMAIL_TYPE, "ORDER_CONFIRMATION");
	    Document orderDoc = GCXMLUtil.getDocumentFromElement(orderElem);
	    Document xslOutDoc = GCCommonUtil.invokeService(env,
		    GCConstants.GC_ORDER_CONFIRMATION_XSLT, orderDoc);
	    
	    //code for appending BundleParentOrderLineKey to bundle components lines begin
	    Document getOrderListOutDoc=GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST, 
	            GCXMLUtil.getDocument("<Order OrderHeaderKey='"+orderElem.getAttribute("OrderHeaderKey")+"'/>") , 
                GCXMLUtil.getDocument("<OrderList><Order OrderNo='' DocumentType='' OrderHeaderKey=''><OrderLines>"
                        + "<OrderLine OrderLineKey='' BundleParentOrderLineKey=''/>"
                        + "</OrderLines></Order></OrderList>"));
	    NodeList nlOrderLine=XPathAPI.selectNodeList(getOrderListOutDoc, "OrderList/Order/OrderLines/OrderLine");
	    int count=nlOrderLine.getLength();
	    for(int i=0;i<count;i++){
	        Element eleOrderLine=(Element)nlOrderLine.item(i);
	        String sBundleParentOrderLineKey=eleOrderLine.getAttribute(GCConstants.BUNDLE_PARENT_ORDERLINEKEY);
	        if(!sBundleParentOrderLineKey.isEmpty()){
	            String sOrderLineKey=eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
	            Element eleOrderLineXslDoc=(Element)XPathAPI.selectSingleNode(xslOutDoc, "Order/OrderLines/OrderLine[@OrderLineKey='"+sOrderLineKey+"']");
	            if(!YFCCommon.isVoid(eleOrderLineXslDoc)){
	                eleOrderLineXslDoc.setAttribute(GCConstants.BUNDLE_PARENT_ORDERLINEKEY, sBundleParentOrderLineKey);
	            }
	        }
	    }
	    //Ends
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("****Order Confirmation Output*****"
			+ GCXMLUtil.getXMLString(xslOutDoc));
	    }

	    GCCommonUtil
		    .invokeService(
			    env,
			    GCConstants.GC_PUBLISH_TO_EMAIL_ORDER_CONFIRMATION_INBOUND_QUEUE,
			    xslOutDoc);

	    // Adding Notes
	    if (!YFCCommon.equalsIgnoreCase("",
		    orderElem.getAttribute("OrderHeaderKey"))) {
		GCChangeOrderUtil.addNoteToOrder(env,
			GCConstants.GC_ORDER_CONFIRMATION_EMAIL_NOTE,
			orderElem.getAttribute("OrderHeaderKey"));
	    }
	    return GCEmailUtils.sendSuccessMessageDoc();
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCOrderConfirmationEmailService.formOrderOutput()",
		    e);
	    throw new GCException(e);
	}
    }

}
