/**
 * Copyright 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        Publishes payment details to TIBCO queue for sending payment declination email to the customer
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
 *          1.0                05/20/2014        Susai, John Melvin			Initial Version
 *          1.1                05/19/2017        Gosai, Pinky               GCSTORE-6851 : Prod - Declined WEB orders not sending Decline Letters
 *#################################################################################################################################################################
 */
package com.gc.api.email;

import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCEmailUtils;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.gc.condition.GCSendEmailConditions;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="john.melvin@expicient.com">John Melvin</a>
 */
public class GCPaymentDeclineEmailService implements YIFCustomApi {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCPaymentDeclineEmailService.class.getName());

    /**
     * 
     * Description of sendPaymentDeclineDetails Publishes payment details to
     * TIBCO queue for sending email to the customer
     * 
     * @param env
     * @param inDoc
     * @return
     * @throws Exception
     * 
     */
    public Document sendPaymentDeclineDetails(YFSEnvironment env, Document inDoc) throws GCException {
        LOGGER.debug(" Entering sendPaymentDeclineDetails() method ");
        if(LOGGER.isDebugEnabled()){
        LOGGER.debug(" sendPaymentDeclineDetails() method, Input document is" + GCXMLUtil.getXMLString(inDoc));
        }
        Document respDoc = null;
        try {
        	if(YFCCommon.isVoid(env.getTxnObject("DeclineEmail"))){
            String sOrderHeaderKey = inDoc.getDocumentElement().getAttribute(GCConstants.ORDER_HEADER_KEY);
            String sGetOrderListInDoc = "<Order OrderHeaderKey=\"" + sOrderHeaderKey + "\"></Order>";
            Document getOrderListInDoc = GCXMLUtil.getDocument(sGetOrderListInDoc);
            Document getOrderListOutDoc = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListInDoc,
                    GCCommonUtil.getDocumentFromPath("/global/template/api/getOrderList_For_CreditCardDeclined_Email.xml"));
            if(LOGGER.isDebugEnabled()){
            LOGGER.debug(" getOrderListOutDoc Output document is" + GCXMLUtil.getXMLString(getOrderListOutDoc));
            }
            String nTotalOrdList = getOrderListOutDoc.getDocumentElement().getAttribute("TotalOrderList");

            if (Double.parseDouble(nTotalOrdList) == 0) {
                respDoc = GCEmailUtils.returnNoRecordFoundDoc();
                return respDoc;
            } else {
                Element ordEle = (Element) getOrderListOutDoc.getElementsByTagName("Order").item(0);
                Element extnEle = (Element) ordEle.getElementsByTagName("Extn").item(0);
                String orderCaptureChannel = extnEle.getAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL);

                Element eleOrd = (Element) getOrderListOutDoc.getElementsByTagName("Order").item(0);
                String paymentStat = eleOrd.getAttribute("PaymentStatus");
               // String strReauth=(String) env.getTxnObject("ReauthEmail");

                if ((GCSendEmailConditions.isCallCenterOrder(env, orderCaptureChannel) || GCSendEmailConditions.isJBSDCSourced(env, orderCaptureChannel))
                        && ("AWAIT_AUTH".equalsIgnoreCase(paymentStat))) {

                    Set<String> declinedPayments = new HashSet<String>();

                    for (int i = 0; i < ordEle.getElementsByTagName("ChargeTransactionDetail").getLength(); i++) {
                        Element chargeTransactionDetailEle = (Element) ordEle.getElementsByTagName("ChargeTransactionDetail").item(i);
                        
                        // GCSTORE-6851
                        String strAuth = chargeTransactionDetailEle.getAttribute("AuthorizationID");
                        if (YFCCommon.isVoid(strAuth))
                        {
                        	strAuth = inDoc.getDocumentElement().getAttribute("AuthId");
                        }
                        LOGGER.debug("strAuth= " + strAuth);
                                                
                        //OMS-5403 added Or condition for charge type as 'CHARGE' 
                        if (("AUTHORIZATION".equalsIgnoreCase(chargeTransactionDetailEle.getAttribute("ChargeType")) || GCConstants.PAYMENT_STATUS_CHARGE.equalsIgnoreCase(chargeTransactionDetailEle.getAttribute("ChargeType")))  
                             && GCConstants.DECLINED.equalsIgnoreCase(strAuth)) 
                        {
                        	LOGGER.debug("Inside if with declined AuthID");
                        	
                        	if (chargeTransactionDetailEle.getElementsByTagName("PaymentMethod").getLength() > 0)
                        	{
                        		LOGGER.debug("if (chargeTransactionDetailEle.getElementsByTagName(PaymentMethod)");
                        		Element chargeTransactionDetailPaymentMethodEle = (Element) chargeTransactionDetailEle.getElementsByTagName("PaymentMethod")
                                        .item(0);
                                
                               	String strSuspendPayment = inDoc.getDocumentElement().getAttribute("SuspendPayment");
                               	LOGGER.debug("strSuspendPayment " + strSuspendPayment);
                               	
                               	// TEMP fix for OMS-3826, SuspendAnyMoreCharges comes Y only in case of hard decline, we should send email only in case of hard decline.
                               	if(GCConstants.YES.equalsIgnoreCase(strSuspendPayment))
                               	{
                               		LOGGER.debug("Inside if strSuspendPayment= " + strSuspendPayment);
                               		declinedPayments.add(chargeTransactionDetailPaymentMethodEle.getAttribute("PaymentKey"));    
                               	}
                        	}
                        	
                     }
                 }
                    //GCSTORE-5516
                  
                    
                    if(!YFCCommon.isVoid((String) env.getTxnObject("ReauthEmail")))
                    {
                        if(LOGGER.isDebugEnabled())
                        {
                            LOGGER.debug(" Inside If :: GCPayment Email Reauth Transaction Object ::"+(String) env.getTxnObject("ReauthEmail")); 
                        }
                    
                        declinedPayments.add((String) env.getTxnObject("ReauthEmail"));
                        env.setTxnObject("ReauthEmail", "");
                        
                    }
                    
                    
                    

                    Element paymentMethodsEle = (Element) ordEle.getElementsByTagName("PaymentMethods").item(0);
                    for (int j = 0; j < paymentMethodsEle.getElementsByTagName("PaymentMethod").getLength(); j++) {
                        Element paymentMethodEle = (Element) paymentMethodsEle.getElementsByTagName("PaymentMethod").item(j);
                        if (declinedPayments.contains(paymentMethodEle.getAttribute("PaymentKey"))
                                && isEmailRequiredForPaymentMethod(paymentMethodEle.getAttribute(GCConstants.PAYMENT_TYPE),
                                        paymentMethodEle.getAttribute(GCConstants.CREDIT_CARD_TYPE))) {
                            Document ordDoc = GCXMLUtil.getDocumentFromElement(ordEle);
                            Element tempOrdEle = ordDoc.getDocumentElement();
                            Element tempPaymentMethodEle = (Element) tempOrdEle.getElementsByTagName("PaymentMethod").item(j);

                            // remove OderLines element which was used to get
                            // the ShipNode information
                            Element tempOrdLinesEle = (Element) tempOrdEle.getElementsByTagName("OrderLines").item(0);
                            tempOrdEle.removeChild(tempOrdLinesEle);

                            // remove ChargeTransactionDetails element
                            Element tempChargeTransactionDetailsEle = (Element) tempOrdEle.getElementsByTagName("ChargeTransactionDetails").item(0);
                            tempOrdEle.removeChild(tempChargeTransactionDetailsEle);

                            // remove PaymentMethods complete list from the
                            // getOrderListOutDoc and create a PaymentMethods
                            // list for the current PaymentMethod and publish
                            // the xml to TIBCO queue
                            Element tempPaymentMethodsEle = (Element) tempOrdEle.getElementsByTagName("PaymentMethods").item(0);
                            tempOrdEle.removeChild(tempPaymentMethodsEle);

                            Element elePaymentMethods = ordDoc.createElement("PaymentMethods");
                            tempOrdEle.appendChild(elePaymentMethods);
                            elePaymentMethods.appendChild(tempPaymentMethodEle);

                            LOGGER.verbose(" ordDoc " + GCXMLUtil.getXMLString(ordDoc));
                            respDoc = formOrderOutput(env, ordDoc);
                            
                            addNoteToOrder(env, GCConstants.GC_CREDITCARD_DECLINED_EMAIL_NOTE, sOrderHeaderKey);
                        }

                    }
                    env.setTxnObject("DeclineEmail", GCConstants.YES);

                } else {
                    respDoc = GCEmailUtils.returnNoRecordFoundDoc();
                }
            }
        }
        } catch (Exception e) {
            LOGGER.error(" Inside catch of GCPaymentDeclineEmailService.sendPaymentDeclineDetails()", e);
            throw new GCException(e);
        }
        LOGGER.verbose(" Exiting GCPaymentDeclineEmailService.sendPaymentDeclineDetails() method ");
        return respDoc;
    }

    public void setProperties(Properties arg0) {

    }

    public boolean isEmailRequiredForPaymentMethod(String paymentType, String creditCardType) throws GCException {
        try {
            if (paymentType.equalsIgnoreCase(GCConstants.CREDIT_CARD)) {
                if ("VISA".equalsIgnoreCase(creditCardType) || "Amex".equalsIgnoreCase(creditCardType) || "MasterCard".equalsIgnoreCase(creditCardType)
                        || "Discover".equalsIgnoreCase(creditCardType)) {
                    return true;
                }
            } else if (paymentType.equalsIgnoreCase(GCConstants.PRIVATE_LABEL_CARD) || paymentType.equalsIgnoreCase(GCConstants.GIFT_CARD) || GCConstants.PVT_LBL_CREDIT_CARD.equalsIgnoreCase(paymentType)) {
                return true;
            }

            return false;
        } catch (Exception e) {
            LOGGER.error(" Inside catch of GCPaymentDeclineEmailService.isEmailRequiredForPaymentMethod()", e);
            throw new GCException(e);
        }
    }

    public Document formOrderOutput(YFSEnvironment env, Document inputDoc) throws GCException {
        try {
            Element ordEle = inputDoc.getDocumentElement();
            ordEle.setAttribute(GCConstants.EMAIL_TYPE, "CC_DECLINED");
            Document orderDoc = GCXMLUtil.getDocumentFromElement(ordEle);
            GCCommonUtil.invokeService(env, GCConstants.GC_PUBLISH_TO_EMAIL_FRAUD_VERIFICATION_INBOUND_QUEUE, orderDoc);
            return GCEmailUtils.sendSuccessMessageDoc();
        } catch (Exception e) {
            LOGGER.error(" Inside catch of GCPaymentDeclineEmailService.formOrderOutput()", e);
            throw new GCException(e);
        }

    }

    public static void addNoteToOrder(YFSEnvironment env, String sNoteText, String sOrderHeaderKey) throws GCException {
        try {
            // Adding Note
            Document docChangeOrderIp = GCXMLUtil.createDocument("Order");
            Element eleRoot = docChangeOrderIp.getDocumentElement();
            eleRoot.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);

            Element eleNotes = docChangeOrderIp.createElement("Notes");
            eleRoot.appendChild(eleNotes);
            Element eleNote = docChangeOrderIp.createElement("Note");
            eleNotes.appendChild(eleNote);
            eleNote.setAttribute("NoteText", sNoteText);
            if(LOGGER.isDebugEnabled()){
            LOGGER.debug("*****changeOrder input*****" + GCXMLUtil.getXMLString(docChangeOrderIp));
            }
            GCCommonUtil.invokeAPI(env, "changeOrder", docChangeOrderIp);
        } catch (Exception e) {
            LOGGER.error(" Inside catch of GCPaymentDeclineEmailService.addNoteToOrder()", e);
            throw new GCException(e);
        }
    }

}
