
package com.gc.api.email;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class is used to send Ready For Customer email and Customer Picked Up email for all the
 * lines going to this status in ExStore and for all lines going being Resend by Call Center.
 *
 * @author Anuj shrivastava <a href="mailto:anuj.srivastava@expicient.com">AnujS</a>
 *
 */
public class GCReadyForCstmrAndCstmrPickedUpEmail implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCReadyForCstmrAndCstmrPickedUpEmail.class
      .getName());

  Properties oProperties = new Properties();

  /**
   *
   * sendReadyForCstmrAndCstmrPickedUpEmail Publishes Email sent to customers when containers are
   * moved to �Ready For Customer Pickup� and "Customer picked up status
   *
   * @param env
   * @param inDoc
   * @return
   * @throws Exception
   *
   */
  public Document sendReadyForCstmrAndCstmrPickedUpEmail(YFSEnvironment env, Document inDoc) throws GCException {

    LOGGER.beginTimer("sendReadyForCstmrAndCstmrPickedUpEmail");
    LOGGER.verbose(" Entering sendReadyForCstmrAndCstmrPickedUpEmail() method ");

    YFCDocument docReadyForCustomerOp = null;



    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" sendReadyForCstmrAndCstmrPickedUpEmail() method, Input document is" + inputDoc.getString());
    }
    YFCElement eleRoot = inputDoc.getDocumentElement();
    String sOrderHeaderKey = eleRoot.getAttribute(GCConstants.ORDER_HEADER_KEY);

    // Preparing input XML for calling getOrderList API

    YFCDocument docInGetOrderList = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleInGetOrderList = docInGetOrderList.getDocumentElement();
    eleInGetOrderList.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);

    // Preparing output template for calling getOrderList API-Please use
    // from file
    YFCDocument getOrderListOpTmp =
        YFCDocument
        .getDocumentFor(GCCommonUtil
            .getDocumentFromPath("/global/template/api/getorderListOpTmplt_For_ReadyforCustomerAndCustomerPickedUp.xml"));
    YFCDocument getOrderListOpDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, docInGetOrderList, getOrderListOpTmp);
    YFCElement eleRootOPGtOdrLst = getOrderListOpDoc.getDocumentElement();

    // Fetching elements to copy to return document
    YFCElement eleOrder = eleRootOPGtOdrLst.getElementsByTagName(GCConstants.ORDER).item(0);

    // Creating hash map to store shipment key
    Map<String, Double> hMapExStoreOrderLinesAndQuantities = new HashMap<String, Double>();

    // Preparing return document
    docReadyForCustomerOp = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleRootReturn = docReadyForCustomerOp.getDocumentElement();

    // Copying order level elements from getOrderList output to return
    // document
    GCXMLUtil.copyElement(docReadyForCustomerOp, eleOrder, eleRootReturn);

    YFCElement eleReturnDocOrdLines = eleRootReturn.getElementsByTagName(GCConstants.ORDER_LINES).item(0);

    // Creating NodeList of OrderLines from the getOrderList output
    YFCNodeList<YFCElement> nOrderLine = docReadyForCustomerOp.getElementsByTagName(GCConstants.ORDER_LINE);

    // Finding root element name from input doc
    String eleRootName = eleRoot.getNodeName();

    // If the inDoc is getShipmentList output, if root element is
    // Shipment then it means sent from ExStore
    if (eleRootName.equals(GCConstants.SHIPMENT)) {
      YFCNodeList<YFCElement> nShipLine = eleRoot.getElementsByTagName(GCConstants.SHIPMENT_LINE);

      for (YFCElement eleShipLineKey : nShipLine) {
        String strOrderLineKey = eleShipLineKey.getAttribute(GCConstants.ORDER_LINE_KEY);
        String strQty = eleShipLineKey.getAttribute(GCConstants.QUANTITY);

        // adding order line key and quantity to HashMap for each
        // shipment line key
        // if orderlinekey is not present in Map then insert a new
        // key-value pair
        if (!hMapExStoreOrderLinesAndQuantities.containsKey(strOrderLineKey)) {
          hMapExStoreOrderLinesAndQuantities.put(strOrderLineKey, Double.parseDouble(strQty));
        } else {
          double dQty = hMapExStoreOrderLinesAndQuantities.get(strOrderLineKey);
          hMapExStoreOrderLinesAndQuantities.put(strOrderLineKey, Double.parseDouble(strQty) + dQty);
        }
      }

      // orderLineCount is incremented conditionally as order line is
      // being removed in else block.
      for (int orderLineCount = 0; orderLineCount < nOrderLine.getLength();) {
        YFCElement eleOrderLine = nOrderLine.item(orderLineCount);
        String ordLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
        // if order line key was present in shipment document

        if (hMapExStoreOrderLinesAndQuantities.containsKey(ordLineKey)) {
          eleOrderLine.setAttribute(GCConstants.STATUS_QUANTITY,
              String.valueOf(hMapExStoreOrderLinesAndQuantities.get(ordLineKey)));
          // Deleting line price info, person ship to , Order
          // Status
          eleOrderLine.removeChild(eleOrderLine.getElementsByTagName(GCConstants.ORDER_STATUSES).item(0));
          orderLineCount++;
        } else {
          YFCNode nodeOrderLine = eleOrderLine;
          eleReturnDocOrdLines.removeChild(nodeOrderLine);
        }
      }
    } else {
      boolean bNoEligibleLines = false;
      String sOrdLineStat = oProperties.getProperty(GCConstants.EMAIL_TYPE);
      String sStatusCode = GCConstants.BLANK;
      if (YFCUtils.equals(sOrdLineStat, GCConstants.READY_FOR_CUSTOMER_PICK_UP)) {
        sStatusCode = GCConstants.READY_FOR_CUSTOMER_PICKUP;
      } else {
        sStatusCode = GCConstants.CUSTOMER_PICKED_UP_ORDER_STATUS;
      }

      for (int ordLineIterator = 0; ordLineIterator < nOrderLine.getLength();) {

        double statusMatchedQty = 0;
        YFCElement eleOrderLine = nOrderLine.item(ordLineIterator);
        String sMaxLineStatus = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_MAX_STATUS);
        String sMinLineStatus = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_MIN_STATUS);
        // comparing minimum line status of each order line
        if ((sMaxLineStatus.compareToIgnoreCase(sStatusCode) < 0)
            || (sMinLineStatus.compareToIgnoreCase(sStatusCode) > 0)) {
          YFCNode nodeOrderLine = eleOrderLine;
          eleReturnDocOrdLines.removeChild(nodeOrderLine);
        } else {

          YFCNodeList<YFCElement> nOrderStatus = eleOrderLine.getElementsByTagName(GCConstants.ORDER_STATUS);
          int ordStatLine = nOrderStatus.getLength();

          for (int ordStatusIterator = 0; ordStatusIterator < ordStatLine; ordStatusIterator++) { // ////
            YFCElement eleOrderStatus = nOrderStatus.item(ordStatusIterator);
            String sStatus = eleOrderStatus.getAttribute(GCConstants.STATUS);

            if (YFCUtils.equals(sStatus, sStatusCode)) {
              String statusQty = eleOrderStatus.getAttribute("StatusQty");
              statusMatchedQty = statusMatchedQty + Double.parseDouble(statusQty);
            }
          }
          if (statusMatchedQty != 0) {
            bNoEligibleLines = true;
            eleOrderLine.setAttribute(GCConstants.STATUS_QUANTITY, String.valueOf(statusMatchedQty));
          }
          eleOrderLine.removeChild(eleOrderLine.getElementsByTagName(GCConstants.ORDER_STATUSES).item(0));
          ordLineIterator++;
        }

      }
      if (!bNoEligibleLines) {

        YFCException yfs =
            new YFCException(GCErrorConstants.EXTN_GCE00701, "No line quantities are in eligible status");

        throw yfs;

      }

    }


    LOGGER.verbose(" Exiting sendReadyForCstmrAndCstmrPickedUpEmail() method ");
    LOGGER.endTimer("sendReadyForCstmrAndCstmrPickedUpEmail");

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Output  sendReadyForCstmrAndCstmrPickedUpEmail:" + docReadyForCustomerOp.getString());
    }

    //Fix for defect - 1935. Fetch Email Type from service args
    String emailType = oProperties.getProperty(GCConstants.EMAIL_TYPE);
    if(YFCUtils.equals(emailType, GCConstants.READY_FOR_CUSTOMER_PICK_UP)){
      eleRootReturn.setAttribute(GCConstants.EMAIL_TYPE, GCConstants.EMAIL_TYPE_RDY_FOR_CUST_PICKUP);
      GCCommonUtil.invokeService(env, GCConstants.GC_PUBLISH_TO_RDY_FOR_CUST_PICKUP_QUEUE, docReadyForCustomerOp);
      //Call the service to add notes on the order.
      GCCommonUtil.invokeService(env, GCConstants.GC_ADDNOTE_FOR_RDY_FOR_CUST_PICKUP, docInGetOrderList);
    } else {
      eleRootReturn.setAttribute(GCConstants.EMAIL_TYPE, GCConstants.EMAIL_TYPE_CUST_PICKEDUP);
      GCCommonUtil.invokeService(env, GCConstants.GC_PUBLISH_TO_CUST_PICKUP_QUEUE, docReadyForCustomerOp);
      //Call the service to add notes on the order.
      GCCommonUtil.invokeService(env, GCConstants.GC_ADDNOTE_FOR_CUST_PICKUP, docInGetOrderList);
    }

    Document respDoc = GCXMLUtil.createDocument("Response");
    Element respEle = respDoc.getDocumentElement();
    respEle.setAttribute("ResponseCode", "SUCCESS");
    respEle.setAttribute("ResponseDescription",
        "Email Sent Successfully");
    //Changes -- End

    return respDoc;
  }

  @Override
  public void setProperties(Properties prop) {

    this.oProperties = prop;

  }

}
