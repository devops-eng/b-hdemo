/**
 * Copyright 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Publishes return order details to TIBCO queue for sending return acceptance email to the customer
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
 *          1.0                05/20/2014        Susai, John Melvin			Initial Version
 *          1.1                04/01/2015        Saxena, Ekansh             Made changes in inputDoc for getOrderList
 *#################################################################################################################################################################
 */
package com.gc.api.email;

import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCChangeOrderUtil;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.gc.condition.GCSendEmailConditions;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="john.melvin@expicient.com">John Melvin</a>
 */
public class GCReturnAcceptanceEmailService implements YIFCustomApi {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCReturnAcceptanceEmailService.class.getName());

    /**
     * 
     * Description of sendReturnDetails Publishes return order details to TIBCO
     * queue for sending email to the customer
     * 
     * @param env
     * @param inDoc
     * @return
     * @throws Exception
     * 
     */
    public Document sendReturnDetails(YFSEnvironment env, Document inDoc)
	    throws GCException {
	LOGGER.debug(" Entering sendReturnDetails() method ");
	if(LOGGER.isDebugEnabled()){
	LOGGER.debug(" sendReturnDetails() method, Input document is"
		+ GCXMLUtil.getXMLString(inDoc));
	}
	Document respDoc = null;
	try {

	    String sOrderHeaderKey = inDoc.getDocumentElement().getAttribute(
		    GCConstants.ORDER_HEADER_KEY);
	    String emailType = inDoc.getDocumentElement().getAttribute(
		    GCConstants.EMAIL_TYPE);

	    // Check if the email was triggered manually
	    if ("RETURN_CONFIRMATION".equalsIgnoreCase(emailType)) {
		String nTotalOrdList = "";
		Document getOrderListInDoc = GCXMLUtil.createDocument(GCConstants.ORDER);
		Element eleRoot = getOrderListInDoc.getDocumentElement();
		eleRoot.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);

		Document getOrderListOutDoc = GCCommonUtil
			.invokeAPI(
				env,
				GCConstants.GET_ORDER_LIST,
				getOrderListInDoc,
				GCCommonUtil
					.getDocumentFromPath("/global/template/api/getOrderList_For_ReturnAcceptance_Email.xml"));

		nTotalOrdList = getOrderListOutDoc.getDocumentElement()
			.getAttribute("TotalOrderList");

		if (Double.parseDouble(nTotalOrdList) == 0) {
		    respDoc = GCXMLUtil.createDocument("Response");
		    Element respEle = respDoc.getDocumentElement();
		    respEle.setAttribute("ResponseCode", "ERROR");
		    respEle.setAttribute("ResponseDescription",
			    "No Record Found");
		    return respDoc;
		} else {
		    Element ordListEle = getOrderListOutDoc
			    .getDocumentElement();
		    for (int i = 0; i < ordListEle
			    .getElementsByTagName("Order").getLength(); i++) {
			Element ordEle = (Element) ordListEle
				.getElementsByTagName("Order").item(i);
			String orderType = ordEle
				.getAttribute(GCConstants.ORDER_TYPE);

			if (GCSendEmailConditions.isStoreReturnOrder(env,
				orderType)) {
			    ordListEle.removeChild(ordEle);
			}
		    }

		    if (ordListEle.getElementsByTagName("Order").getLength() > 0) {
			Document ordListDoc = GCXMLUtil
				.getDocumentFromElement(ordListEle);
			respDoc = formOrderListOutput(env, ordListDoc);

			for (int i = 0; i < ordListEle.getElementsByTagName(
				"Order").getLength(); i++) {
			    Element ordEle = (Element) ordListEle
				    .getElementsByTagName("Order").item(i);
			    GCChangeOrderUtil
				    .addNoteToOrder(
					    env,
					    GCConstants.GC_RETURN_ACCEPTANCE_EMAIL_NOTE,
					    ordEle.getAttribute(GCConstants.ORDER_HEADER_KEY));
			}
		    }
		}

	    } else {
		Element ordEle = (Element) inDoc.getElementsByTagName("Order")
			.item(0);
		String orderType = ordEle.getAttribute(GCConstants.ORDER_TYPE);

		if (!GCSendEmailConditions.isStoreReturnOrder(env, orderType)) {
		    Document ordListDoc = GCXMLUtil.createDocument("OrderList");
		    Node order = ordListDoc.importNode(ordEle, true);
		    GCXMLUtil.appendChild((Element) ordListDoc
			    .getElementsByTagName("OrderList").item(0),
			    (Element) order);
		    respDoc = formOrderListOutput(env, ordListDoc);
		    if(LOGGER.isDebugEnabled()){
		    LOGGER.debug("****input to NPR****"
			    + GCXMLUtil.getXMLString(ordListDoc));
		    }
		    formOrderListOutputForNPR(env, ordListDoc, sOrderHeaderKey);

		    GCChangeOrderUtil.addNoteToOrder(env,
			    GCConstants.GC_RETURN_ACCEPTANCE_EMAIL_NOTE,
			    sOrderHeaderKey);

		} else {
		    respDoc = GCXMLUtil.createDocument("Response");
		    Element respEle = respDoc.getDocumentElement();
		    respEle.setAttribute("ResponseCode", "ERROR");
		    respEle.setAttribute("ResponseDescription",
			    "No Record Found");
		}

	    }

	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCReturnAcceptanceEmailService.sendReturnDetails()",
		    e);
	    throw new GCException(e);
	}
	LOGGER.debug(" Exiting GCReturnAcceptanceEmailService.sendReturnDetails() method ");
	return respDoc;
    }

    public void setProperties(Properties arg0) {

    }

    public Document formOrderListOutput(YFSEnvironment env, Document inputDoc)
	    throws GCException {
	try {
	    Element orderListEle = (Element) inputDoc.getElementsByTagName(
		    "OrderList").item(0);
	    orderListEle.setAttribute(GCConstants.EMAIL_TYPE,
		    "RETURN_CONFIRMATION");
	    Document orderListDoc = GCXMLUtil
		    .getDocumentFromElement(orderListEle);

	    Document returnAcceptanceXSLTOutDoc = GCCommonUtil.invokeService(
		    env, GCConstants.GC_RETURN_ACCEPTANCE_XSLT, orderListDoc);
	    if(LOGGER.isDebugEnabled()){
	    LOGGER.debug("****Return Acceptance Output*****"
		    + GCXMLUtil.getXMLString(returnAcceptanceXSLTOutDoc));
	    }
	    GCCommonUtil
		    .invokeService(
			    env,
			    GCConstants.GC_PUBLISH_TO_EMAIL_RETURN_AUTHORIZATION_INBOUND_QUEUE,
			    returnAcceptanceXSLTOutDoc);
	    Document respDoc = GCXMLUtil.createDocument("Response");
	    Element respEle = respDoc.getDocumentElement();
	    respEle.setAttribute("ResponseCode", "SUCCESS");
	    respEle.setAttribute("ResponseDescription",
		    "Email Sent Successfully");
	    return respDoc;
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCReturnAcceptanceEmailService.formOrderListOutput()",
		    e);
	    throw new GCException(e);
	}
    }

    public Document formOrderListOutputForNPR(YFSEnvironment env,
	    Document inputDoc, String ordHdrKey) throws GCException {
	try {
	    Element orderListEle = (Element) inputDoc.getElementsByTagName(
		    "OrderList").item(0);
	    orderListEle
		    .setAttribute(GCConstants.EMAIL_TYPE, "RETURN_RECEIVED");
	    Document orderListDoc = GCXMLUtil
		    .getDocumentFromElement(orderListEle);
	    if(LOGGER.isDebugEnabled()){
	    LOGGER.debug("****input to NPR****"
		    + GCXMLUtil.getXMLString(orderListDoc));
	    }
	    Document returnAcceptanceXSLTOutDoc = GCCommonUtil.invokeService(
		    env, GCConstants.GC_RETURN_RECEIVED_FOR_NPR_XSLT,
		    orderListDoc);
	    LOGGER.debug("****Return Received For NPR *****"
		    + GCXMLUtil.getXMLString(returnAcceptanceXSLTOutDoc));
	    NodeList recptLines = returnAcceptanceXSLTOutDoc
		    .getElementsByTagName("ReceiptLine");

	    if (recptLines.getLength() >= 1) {

		GCCommonUtil
			.invokeService(
				env,
				GCConstants.GC_PUBLISH_TO_EMAIL_RETURNRECIEVED_NOTIFICATION_INBOUND_QUEUE,
				returnAcceptanceXSLTOutDoc);

		GCChangeOrderUtil.addNoteToOrder(env,
			GCConstants.GC_RETURN_RECIEVED_EMAIL_NOTE, ordHdrKey);
	    }

	    return returnAcceptanceXSLTOutDoc;
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCReturnAcceptanceEmailService.formOrderListOutputForNPR()",
		    e);
	    throw new GCException(e);
	}
    }

}
