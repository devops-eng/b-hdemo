/**
 * Copyright 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Publishes receipt details to TIBCO queue for sending return received email to the customer
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
 *          1.0                05/20/2014        Susai, John Melvin			Initial Version
 *          1.1                03/26/2015        Saxena, Ekansh             Added code to fetch OrderHeaderKey from inputDoc - Phase2
 *          1.2				   04/05/2015	     Mittal, Yashu				Fix for GCSTORE-2446
 *#################################################################################################################################################################
 */
package com.gc.api.email;

import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCChangeOrderUtil;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCEmailUtils;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.gc.condition.GCSendEmailConditions;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="john.melvin@expicient.com">John Melvin</a>
 */
public class GCReturnReceivedEmailService implements YIFCustomApi {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCReturnReceivedEmailService.class.getName());

    /**
     * 
     * Description of sendReturnReceivedDetails Publishes receipt details to
     * TIBCO queue for sending email to the customer
     * 
     * @param env
     * @param inDoc
     * @return
     * @throws Exception
     * 
     */
    public Document sendReturnReceivedDetails(YFSEnvironment env, Document inDoc)
	    throws GCException {
	LOGGER.debug(" Entering sendReturnReceivedDetails() method ");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" sendReturnReceivedDetails() method, Input document is"
		    + GCXMLUtil.getXMLString(inDoc));
	}
	Document respDoc = null;
	try {

		// Fix for GCSTORE-2446 : Start
		 String sOrderHeaderKey = "";
		    Element eleReceiptLine = (Element) inDoc.getDocumentElement().getElementsByTagName("ReceiptLine").item(0);
		    if(!YFCCommon.isVoid(eleReceiptLine)){
		    	sOrderHeaderKey = eleReceiptLine.getAttribute(GCConstants.ORDER_HEADER_KEY);
		    } else{
		    	Element eleOrder = inDoc.getDocumentElement();
		    	sOrderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
		    }
		    
		 // Fix for GCSTORE-2446 : End 
	    String sGetOrderListInDoc = "<Order OrderHeaderKey=\""
		    + sOrderHeaderKey + "\"></Order>";
	    Document getOrderListInDoc = GCXMLUtil
		    .getDocument(sGetOrderListInDoc);

	    Document getOrderListOutDoc = GCCommonUtil
		    .invokeAPI(
			    env,
			    GCConstants.GET_ORDER_LIST,
			    getOrderListInDoc,
			    GCCommonUtil
				    .getDocumentFromPath("/global/template/api/getOrderList_For_ReturnRecieved_Email.xml"));

	    Element ordEle = (Element) getOrderListOutDoc.getElementsByTagName(
		    "Order").item(0);
	    String orderType = ordEle.getAttribute(GCConstants.ORDER_TYPE);

      if (!GCSendEmailConditions.isStoreReturnOrder(env, orderType)) {
        // Copy Order from getOrderListOutDoc to inDoc
        Node order = inDoc.importNode(getOrderListOutDoc
            .getElementsByTagName("Order").item(0), true);
        GCXMLUtil
        .appendChild(
            (Element) inDoc.getElementsByTagName("Receipt")
            .item(0), (Element) order);
        respDoc = formOrderOutput(env, inDoc);

        // Adding Notes
        if (!YFCCommon.equalsIgnoreCase("", sOrderHeaderKey)) {
            GCChangeOrderUtil.addNoteToOrder(env,
                GCConstants.GC_RETURN_RECIEVED_EMAIL_NOTE,
                sOrderHeaderKey);
        }

	    } else {
		respDoc = GCEmailUtils.returnNoRecordFoundDoc();
	    }

	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCReturnReceivedEmailService.sendReturnReceivedDetails()",
		    e);
	    throw new GCException(e);
	}
	LOGGER.debug(" Exiting GCReturnReceivedEmailService.sendReturnReceivedDetails() method ");
	return respDoc;
    }

  @Override
  public void setProperties(Properties arg0) {

    }

    public Document formOrderOutput(YFSEnvironment env, Document inputDoc)
	    throws GCException {
	try {
	    Element orderElem = (Element) inputDoc.getElementsByTagName(
		    "Receipt").item(0);
	    orderElem.setAttribute(GCConstants.EMAIL_TYPE, "RETURN_RECEIVED");
	    Document ordDoc = GCXMLUtil.getDocumentFromElement(orderElem);

	    Document returnReceivedXSLTOutDoc = GCCommonUtil.invokeService(env,
		    GCConstants.GC_RETURN_RECEIVED_XSLT, ordDoc);
	    if(LOGGER.isDebugEnabled()){
	    LOGGER.debug("****Return Received Output*****"
		    + GCXMLUtil.getXMLString(returnReceivedXSLTOutDoc));
	    }
	    GCCommonUtil
		    .invokeService(
			    env,
			    GCConstants.GC_PUBLISH_TO_EMAIL_RETURNRECIEVED_NOTIFICATION_INBOUND_QUEUE,
			    returnReceivedXSLTOutDoc);
	    return GCEmailUtils.sendSuccessMessageDoc();
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCReturnReceivedEmailService.formOrderOutput()",
		    e);
	    throw new GCException(e);
	}
    }

}
