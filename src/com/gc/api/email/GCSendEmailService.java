/**
 * Copyright 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Based on the EmailType, invokes the corresponsing service
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
 *          1.0                06/04/2014        Susai, John Melvin			Initial Version
 *          1.1                03/26/2015        Saxena, Ekansh             Updated code for Phase 2 - GCSTORE-769 (GCEmails)
 *#################################################################################################################################################################
 */
package com.gc.api.email;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.gc.exstore.utils.GCShipmentUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="john.melvin@expicient.com">John Melvin</a>
 */
public class GCSendEmailService implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCSendEmailService.class.getName());

  /**
   *
   * Description of sendEmail Publishes receipt details to TIBCO queue for
   * sending email to the customer
   *
   * @param env
   * @param inDoc
   * @return
   * @throws Exception
   *
   */
  public Document sendEmail(YFSEnvironment env, Document inDoc)
      throws GCException {
    LOGGER.debug(" Entering sendEmail() method ");
    if(LOGGER.isDebugEnabled()){
      LOGGER.debug(" sendEmail() method, Input document is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    Document respDoc = null;
    try {

      String emailType = inDoc.getDocumentElement().getAttribute(
          GCConstants.EMAIL_TYPE);

      if ("ORDER_CONFIRMATION".equalsIgnoreCase(emailType)) {
        respDoc = GCCommonUtil.invokeService(env,
            GCConstants.GC_ORDER_CONFIRMATION_EMAIL_SERVICE, inDoc);
      } else if ("SHIPMENT_CONFIRMATION".equalsIgnoreCase(emailType)
          || "SOFTWARE_ITEM_KEY".equalsIgnoreCase(emailType)) {
        respDoc = GCCommonUtil.invokeService(env,
            GCConstants.GC_SHIPMENT_CONFIRMATION_EMAIL_SERVICE,
            inDoc);
      } else if ("FRAUD_NOTE".equalsIgnoreCase(emailType)) {
        respDoc = GCCommonUtil.invokeService(env,
            GCConstants.GC_FRAUD_REVIEW_HOLD_EMAIL_SERVICE, inDoc);
      } else if (YFCCommon.equalsIgnoreCase("RETURN_CONFIRMATION", emailType)
          || YFCCommon.equalsIgnoreCase("RETURN_RECEIVED", emailType)) {
        List<String> derivedKeyList = new ArrayList<String>();
        String orderHeaderKey = inDoc.getDocumentElement().getAttribute(GCConstants.ORDER_HEADER_KEY);
        YFCDocument getOrdLineListIp = YFCDocument.createDocument(GCConstants.ORDER_LINE);
        YFCElement eleIp = getOrdLineListIp.getDocumentElement();
        eleIp.setAttribute(GCConstants.DERIVED_FROM_ORDER_HEADER_KEY, orderHeaderKey);
        YFCDocument templateDoc = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LINE_LIST_TEMP_EMAIL);
        YFCDocument opDoc = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LINE_LIST, getOrdLineListIp, templateDoc);
        YFCElement eleOp = opDoc.getDocumentElement();
        YFCNodeList<YFCElement> nlOrderLine = eleOp.getElementsByTagName(GCConstants.ORDER_LINE);
        for(YFCElement eleOrderLine : nlOrderLine){
          YFCElement eleDerivedOrder = eleOrderLine.getChildElement(GCConstants.ORDER);
          String docType = eleDerivedOrder.getAttribute(GCConstants.DOCUMENT_TYPE);
          String status = eleDerivedOrder.getAttribute(GCConstants.MAX_ORDER_STATUS);
          status = GCShipmentUtil.checkAndConvertOrderStatus(status);
          Double dStatus = Double.parseDouble(status);
          String derOrderHeaderKey = eleDerivedOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
          if (derivedKeyList.isEmpty() || !derivedKeyList.contains(derOrderHeaderKey)) {
            if(YFCCommon.equalsIgnoreCase("RETURN_CONFIRMATION", emailType)){
              Double createdStatus = Double.parseDouble(GCConstants.CREATED_STATUS);
              if (YFCCommon.equals(docType, GCConstants.ZERO_ZERO_ZERO_THREE) && dStatus >= createdStatus) {
                inDoc.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, derOrderHeaderKey);
                respDoc = GCCommonUtil.invokeService(env,
                    GCConstants.GC_RETURN_ACCEPTANCE_EMAIL_SERVICE, inDoc);
                derivedKeyList.add(derOrderHeaderKey);
              }
            } else {
              Double receivedStatus = Double.parseDouble(GCConstants.RECEIVED_STATUS);
              if (YFCCommon.equals(docType, GCConstants.ZERO_ZERO_ZERO_THREE) && dStatus >= receivedStatus) {
                Element eleInDoc = inDoc.getDocumentElement();
                String resendReq = eleInDoc.getAttribute(GCConstants.IS_RESEND_REQUEST);
                Document reciptOpDoc = getReceiptDetails(env, derOrderHeaderKey);
                Element eleReceiptOp = reciptOpDoc.getDocumentElement();
                if (eleReceiptOp.hasChildNodes()) {
                  respDoc = callRetrnRecievEmailServ(env, respDoc,
						derOrderHeaderKey, resendReq, eleReceiptOp);
                }
                derivedKeyList.add(derOrderHeaderKey);
              }
            }
          }
        }
      } else if ("FTC_NOTIFICATION".equalsIgnoreCase(emailType)) {
        respDoc = GCCommonUtil.invokeService(env,
            GCConstants.GC_BACKORDER_NOTIFICATION_EMAIL_SERVICE,
            inDoc);
      } else if ("CC_DECLINED".equalsIgnoreCase(emailType)) {
        respDoc = GCCommonUtil.invokeService(env,
            GCConstants.GC_PAYMENT_DECLINE_EMAIL_SERVICE, inDoc);
      } else if (YFCCommon.equalsIgnoreCase("READY_FOR_CUSTOMER_PICKUP", emailType)) {
        // Phase 2 GCSTORE-769 - Start
        respDoc = GCCommonUtil.invokeService(env, GCConstants.GC_READY_FOR_CUSTOMER_PICKUP_EMAIL_SERVICE, inDoc);
      } else if (YFCCommon.equalsIgnoreCase("CUSTOMER_PICKEDUP", emailType)) {
        respDoc = GCCommonUtil.invokeService(env, GCConstants.GC_CUSTOMER_PICKEDUP_EMAIL_SERVICE, inDoc);
        //Phase 2 GCSTORE-769 - End
      } else if(YFCUtils.equals("ORDER_CANCELLATION", emailType)) {
        respDoc = GCCommonUtil.invokeService(env, GCConstants.GC_ORDER_CANCELLATION_EMAIL_SERVICE, inDoc);
      }


      if(LOGGER.isDebugEnabled()){
        LOGGER.debug(" sendEmail() method, Input document is"
            + GCXMLUtil.getXMLString(respDoc));
      }

    } catch (Exception e) {
      LOGGER.error(" Inside catch of GCSendEmailService.sendEmail()", e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCSendEmailService.sendEmail() method ");
    return respDoc;
  }
private Document callRetrnRecievEmailServ(YFSEnvironment env, Document respDoc,
		String derOrderHeaderKey, String resendReq, Element eleReceiptOp) {
	NodeList nlReceipt = eleReceiptOp.getElementsByTagName(GCConstants.RECEIPT);
	  for (int i = 0; i < nlReceipt.getLength(); i++) {
	    Element eleReceipt = (Element) nlReceipt.item(i);
	    eleReceipt.setAttribute(GCConstants.ORDER_HEADER_KEY, derOrderHeaderKey);
	    eleReceipt.setAttribute(GCConstants.IS_RESEND_REQUEST, resendReq);
	    Document receiptDoc = GCXMLUtil.getDocumentFromElement(eleReceipt);
	    respDoc = GCCommonUtil.invokeService(env, GCConstants.GC_RETURN_RECEIVED_EMAIL_SERVICE, receiptDoc);
	  }
	return respDoc;
}
  private Document getReceiptDetails(YFSEnvironment env, String orderHeaderKey) {
    YFCDocument receiptDoc = YFCDocument.createDocument(GCConstants.RECEIPT);
    YFCElement eleReceipt = receiptDoc.getDocumentElement();
    YFCElement eleOrdRecpt = eleReceipt.createChild(GCConstants.ORDER);
    eleOrdRecpt.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
    Document opDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_RECEIPT_LIST, receiptDoc.getDocument(),
            GCCommonUtil.getDocumentFromPath("/global/template/api/getReceiptListTemp.xml"));
    return opDoc;
  }

  @Override
  public void setProperties(Properties arg0) {

  }

}
