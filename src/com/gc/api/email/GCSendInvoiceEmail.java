package com.gc.api.email;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCChangeOrderUtil;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCSendInvoiceEmail {
	
	 private static final YFCLogCategory LOGGER = YFCLogCategory
			    .instance(GCSendInvoiceEmail.class.getName());
	 
	 public Document sendInvoiceEmail(YFSEnvironment env, Document inDoc) throws GCException {
			LOGGER.debug("Entering GCSendInvoiceEmail.sendInvoiceEmail() method ");
			Document docInvoiceDetOutput = null;
			if (LOGGER.isDebugEnabled()) {
			    LOGGER.debug("GCSendInvoiceEmail sendInvoiceEmail() method, Input document is"
				    + GCXMLUtil.getXMLString(inDoc));
			}
		    try {
		    	Element eleRootOfInDoc = inDoc.getDocumentElement();
		    	String sEmailId = eleRootOfInDoc.getAttribute(GCConstants.CUSTOMER_EMAIL_ID);
		    	String sOrderHeaderKey = eleRootOfInDoc.getAttribute(GCConstants.ORDER_HEADER_KEY);
		    	String sInvoiceNo = eleRootOfInDoc.getAttribute(GCConstants.INVOICE_NO);
		    	if(!YFCCommon.isVoid(sEmailId)) {
		    		eleRootOfInDoc.removeAttribute(GCConstants.CUSTOMER_EMAIL_ID);
		    	}
		    	docInvoiceDetOutput = GCCommonUtil.invokeService(env, GCConstants.GC_GET_ORDER_INVOICE_DETAIL_FOR_EMAIL_SERVICE, inDoc);
				Element eleInvoiceDetail = docInvoiceDetOutput.getDocumentElement();
			    if(!YFCCommon.isVoid(sEmailId)){
			    	   Element elePersonInfoBillTo = (Element) eleInvoiceDetail.getElementsByTagName(GCConstants.PERSON_INFO_BILL_TO).item(0);
			    	   elePersonInfoBillTo.setAttribute(GCConstants.EMAIL_ID, sEmailId);
			    }
			    if (!YFCCommon.equalsIgnoreCase("", sOrderHeaderKey)) {
			    	String sNote = "Invoice Email " + sInvoiceNo + " Sent to the Customer";
		            GCChangeOrderUtil.addNoteToOrder(env,sNote,sOrderHeaderKey);
		        }
		    } catch (Exception e) {
			    LOGGER.error(" Inside catch of GCSendInvoiceEmail.sendInvoiceEmail()", e);
				    throw new GCException(e);
			}
	    if (LOGGER.isDebugEnabled()) {
	        LOGGER.debug("Output from GCSendInvoiceEmail.sendInvoiceEmail()  is" + GCXMLUtil.getXMLString(docInvoiceDetOutput));
	    }
		LOGGER.debug("Exiting GCSendInvoiceEmail.sendInvoiceEmail() method ");
		return docInvoiceDetOutput;
	 }
}
