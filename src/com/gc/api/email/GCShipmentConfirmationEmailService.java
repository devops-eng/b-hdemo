/**
 * Copyright © 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Processes the holds present on order
 *#################################################################################################################################################################
 *	        Version			   	Date              	Modified By             	Description
 *#################################################################################################################################################################
 1.0               	21/05/2014        	Nainar, Alex              	Shipment Confirmation
 1.1				06/02/2014			Susai, John Melvin			Refactoring Adding Notes
 1.2                06/17/2014          Nainar, Alex              	Updated the resend code and invoice details
 1.3                03/25/2015          Saxena, Ekansh              Updated the code to check if email is allowed or not - Phase2
 *#################################################################################################################################################################
 */
package com.gc.api.email;

import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="alex.nainar@expicient.com">Alex Nainar</a>
 */
public class GCShipmentConfirmationEmailService implements YIFCustomApi {

	// initialize logger
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCShipmentConfirmationEmailService.class.getName());

	/**
	 *
	 * Description of processOrderHoldType Identifies the hold present on order
	 * and call the respective service to process that hold-
	 *
	 * @param env
	 * @param inDoc
	 * @return
	 * @throws Exception
	 *
	 */
	public Document sendShipmentDetails(YFSEnvironment env, Document inDoc)
			throws GCException {
		LOGGER.debug(" Entering sendShipmentDetails() method ");
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug(" sendShipmentDetails() method, Input document is"
					+ GCXMLUtil.getXMLString(inDoc));
		}
		Document respDoc = null;
		Document ordInvDoc = null;
		Element invShpEle = null;
		String invComp = "";
		String invKey = "";
		String sExtnPickupStoreID = null;
		String isImport = null;
		String sShipNode;
		boolean isEmailAllowed=false;
		try {

			String sOrderHeaderKey = inDoc.getDocumentElement().getAttribute(
					GCConstants.ORDER_HEADER_KEY);
			String emailType = inDoc.getDocumentElement().getAttribute(
					GCConstants.EMAIL_TYPE);

			if ("SHIPMENT_CONFIRMATION".equalsIgnoreCase(emailType)
					|| "SOFTWARE_ITEM_KEY".equalsIgnoreCase(emailType)) {
				NodeList nTotalShipmntList = null;
				String shpmtListTemp = "<ShipmentList><Shipment ShipmentKey='' InvoiceComplete='' ShipNode=''/></ShipmentList>";

				Document getShipmentistOutDoc = GCCommonUtil.invokeAPI(env,
						"getShipmentListForOrder", inDoc,
						GCXMLUtil.getDocument(shpmtListTemp));
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("********Get Shipment List Api Output*****"
							+ GCXMLUtil.getXMLString(getShipmentistOutDoc));
				}

				nTotalShipmntList = getShipmentistOutDoc
						.getElementsByTagName("Shipment");

				invShpEle = (Element) XPathAPI.selectSingleNode(
						getShipmentistOutDoc,
						"ShipmentList/Shipment[@InvoiceComplete='Y']");

				if (nTotalShipmntList.getLength() == 0 || (invShpEle == null)) {
					respDoc = GCXMLUtil.createDocument("Response");
					Element respEle = respDoc.getDocumentElement();
					respEle.setAttribute("ResponseCode", "ERROR");
					respEle.setAttribute("ResponseDescription",
							"No Record Found");
					return respDoc;
				} else {

					// Fetch ExtnPickupStoreID (Phase 2 GCSTORE-769)- Start
					Document getOrderListOutDoc = getOrderList(env, sOrderHeaderKey);
					Element eleOrder = (Element) getOrderListOutDoc.getElementsByTagName(GCConstants.ORDER).item(0);
					Element eleExtn = (Element) eleOrder.getElementsByTagName(GCConstants.EXTN).item(0);
					if (!YFCCommon.isVoid(eleExtn)) {
						sExtnPickupStoreID = eleExtn.getAttribute(GCConstants.EXTN_PICKUP_STORE_ID);
					}
					// Fetch ExtnPickupStoreID (Phase 2 GCSTORE-769)- End

					//Phase 2 GCSTORE-769
					if (YFCCommon.isStringVoid(sExtnPickupStoreID)) {
						isEmailAllowed = true;
					}

					Document shpmntListDoc = GCXMLUtil
							.createDocument("ShipmentList");
					Element shpmntLstEle = shpmntListDoc.getDocumentElement();

					for (int i = 0; i < nTotalShipmntList.getLength(); i++) {
						Element shpmntEle = (Element) getShipmentistOutDoc
								.getElementsByTagName("Shipment").item(i);
						invComp = shpmntEle.getAttribute("InvoiceComplete");
						sShipNode=shpmntEle.getAttribute(GCConstants.SHIP_NODE);

						//email is not allowed when ExtnPickupStoreID and ShipNode is same (Phase 2 GCSTORE-769)
						if(!YFCCommon.isStringVoid(sExtnPickupStoreID) && !YFCCommon.equals(sExtnPickupStoreID, sShipNode, false)){
							isEmailAllowed=true;
						}

						Document shipmentInputDoc = GCXMLUtil
								.createDocument("Shipment");
						Element shipmentEle = shipmentInputDoc
								.getDocumentElement();
						shipmentEle.setAttribute("ShipmentKey",
								shpmntEle.getAttribute("ShipmentKey"));

						Document getShpmntOutDoc = GCCommonUtil
								.invokeAPI(
										env,
										GCConstants.GET_SHIPMENT_DTL,
										shipmentInputDoc,
										GCCommonUtil
										.getDocumentFromPath("/global/template/api/getShipmentList_For_ShipmentConfirmation_Email.xml"));

						if (GCConstants.YES.equalsIgnoreCase(invComp)) {
							
							// Fix for GCSTORE:3135

							NodeList nlInvEle = getShpmntOutDoc.getElementsByTagName("OrderInvoice");
							
							if(nlInvEle.getLength()>0){
								Element invEle = (Element) nlInvEle.item(0);
								invKey = invEle.getAttribute("OrderInvoiceKey");

								ordInvDoc = GCXMLUtil
										.createDocument("GetOrderInvoiceDetails");
								Element ordInvEle = ordInvDoc.getDocumentElement();
								ordInvEle.setAttribute("InvoiceKey", invKey);

								Document shpmntInvDoc = GCCommonUtil.invokeService(
										env,
										GCConstants.GC_GET_INVOICE_DETAILS_SERVICE,
										ordInvDoc);
								if(LOGGER.isDebugEnabled()){
									LOGGER.debug("******Get Invoice Details Output******"
											+ GCXMLUtil.getXMLString(shpmntInvDoc));
								}
								Node node = getShpmntOutDoc.importNode(
										shpmntInvDoc.getDocumentElement(), true);

								getShpmntOutDoc.getDocumentElement().appendChild(
										node);
								
								if(LOGGER.isDebugEnabled()){
									LOGGER.debug("******ordDocReqForShpmnt******"
											+ GCXMLUtil.getXMLString(shpmntInvDoc));
								}
								
								// Fix for GCSTORE-3135 : End
							}
							
							// Adding the Order Level Attributes:
							Document ordDocReqForShpmnt = getOrderList(env,
									sOrderHeaderKey);
							
							Element ordEle = (Element) ordDocReqForShpmnt
									.getElementsByTagName("Order").item(0);
							Element ordLineEle = (Element) ordDocReqForShpmnt
									.getElementsByTagName("OrderLine").item(0);

							getShpmntOutDoc.getDocumentElement().setAttribute(
									"OrderNo", ordEle.getAttribute("OrderNo"));
							getShpmntOutDoc.getDocumentElement().setAttribute(
									"FulfillmentType",
									ordLineEle.getAttribute("FulfillmentType"));

							Element ordExnEle = (Element) ordDocReqForShpmnt
									.getElementsByTagName("Extn").item(0);
							// Added for Defect No -GCStore-2073
							getShpmntOutDoc.getDocumentElement().setAttribute(
									"CustomerEMailID",
									ordEle.getAttribute("CustomerEMailID"));
							// Added for Defect No -GCStore-2073
							
							// GCPRO - 366: Start.
							getShpmntOutDoc.getDocumentElement().setAttribute(
									GCConstants.ENTRY_TYPE,
									ordEle.getAttribute(GCConstants.ENTRY_TYPE));
							// GCPRO - 366: End.
							
							if (ordExnEle != null) {
								getShpmntOutDoc
								.getDocumentElement()
								.setAttribute(
										"ExtnDAXOrderNo",
										ordExnEle
										.getAttribute("ExtnDAXOrderNo"));
								getShpmntOutDoc
								.getDocumentElement()
								.setAttribute(
										"ExtnShipToStoreID",
										ordExnEle
										.getAttribute("ExtnShipToStoreID"));
								getShpmntOutDoc
								.getDocumentElement()
								.setAttribute(
										"ExtnSourceOrderNo",
										ordExnEle
										.getAttribute("ExtnSourceOrderNo"));
							isImport = ordExnEle.getAttribute("ExtnIsImportedOrder");
							}
                             // GCSTORE3182
              if (YFCUtils.equals(GCConstants.YES, isImport)) {
                Element eleShipment = getShpmntOutDoc.getDocumentElement();
                Element eleLineDetails = getShpmntOutDoc.createElement(GCConstants.LINE_DETAILS);
                eleShipment.appendChild(eleLineDetails);
                NodeList nlShipmentLine =
                    XPathAPI.selectNodeList(getShpmntOutDoc, "Shipment/ShipmentLines/ShipmentLine");
                for (int k = 0; k < nlShipmentLine.getLength(); k++) {
                  Element eleShipmentLine = (Element) nlShipmentLine.item(k);
                  String orderLineKey = eleShipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY);
                  NodeList nlOrderLine =
                      XPathAPI.selectNodeList(ordDocReqForShpmnt, "OrderList/Order/OrderLines/OrderLine");
                  for (int j = 0; j < nlOrderLine.getLength(); j++) {
                    Element eleOrderLine = (Element) nlOrderLine.item(j);
                    String orderLineKey1 = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
                    if (YFCUtils.equals(orderLineKey, orderLineKey1)) {
                      Element eleLinePriceInfo =
                          (Element) eleOrderLine.getElementsByTagName(GCConstants.LINE_PRICE_INFO).item(0);
                      String unitPrice = eleLinePriceInfo.getAttribute(GCConstants.UNIT_PRICE);
                      Element eleLineDetail = getShpmntOutDoc.createElement(GCConstants.LINE_DETAIL);
                      eleLineDetail.setAttribute(GCConstants.UNIT_PRICE, unitPrice);
                      eleLineDetail.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey1);
                      Element eleLineCharges =
                          (Element) XPathAPI.selectSingleNode(eleOrderLine, GCConstants.LINE_CHARGES);
                      Element eleShipmentLineCharges = getShpmntOutDoc.createElement(GCConstants.LINE_CHARGES);
                      GCXMLUtil.copyElement(getShpmntOutDoc, eleLineCharges, eleShipmentLineCharges);
                      eleLineDetail.appendChild(eleShipmentLineCharges);
                      Element eleLineTaxes = (Element) XPathAPI.selectSingleNode(eleOrderLine, GCConstants.LINE_TAXES);
                      Element eleShipmentLineTaxes = getShpmntOutDoc.createElement(GCConstants.LINE_TAXES);
                      GCXMLUtil.copyElement(getShpmntOutDoc, eleLineTaxes, eleShipmentLineTaxes);
                      eleLineDetail.appendChild(eleShipmentLineTaxes);
                      eleLineDetails.appendChild(eleLineDetail);

                    }
                  }

                }
                eleShipment.appendChild(eleLineDetails);
              }
							// Remove the OrderInvoiceList element
							Element invEleCheck = (Element) getShpmntOutDoc
									.getElementsByTagName("OrderInvoiceList")
									.item(0);

							if (invEleCheck != null) {
								GCXMLUtil.removeChild(
										getShpmntOutDoc.getDocumentElement(),
										invEleCheck);
							}

							// Append the Shipment Element to the ShipmentList
							// element
							Node node1 = shpmntListDoc.importNode(
									getShpmntOutDoc.getDocumentElement(), true);
							shpmntListDoc.getDocumentElement().appendChild(
									node1);

						} else {
							continue;
						}

					}

					if ("SHIPMENT_CONFIRMATION".equalsIgnoreCase(emailType)) {
						shpmntLstEle.setAttribute("EmailType",
								"SHIPMENT_CONFIRMATION");
					} else if ("SOFTWARE_ITEM_KEY".equalsIgnoreCase(emailType)) {
						shpmntLstEle.setAttribute("EmailType",
								"SOFTWARE_ITEM_KEY");
					}

					// Check if email is allowed, then publish Shipment details (Phase 2 GCSTORE-769)
					if (isEmailAllowed) {
						respDoc = publishShipmentDetails(env, shpmntListDoc, sOrderHeaderKey);
					}
				}

			} else {

				// From the on_success event xml

				sShipNode = inDoc.getDocumentElement().getAttribute(GCConstants.SHIP_NODE);
				String sShipmentKey = inDoc.getDocumentElement().getAttribute(
						GCConstants.SHIPMENT_KEY);
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("***getShipmentList output document****"
							+ GCXMLUtil.getXMLString(inDoc));
				}
				Document shipmentInputDoc = GCXMLUtil
						.createDocument("Shipment");
				Element shipmentEle = shipmentInputDoc.getDocumentElement();
				shipmentEle.setAttribute("ShipmentKey", sShipmentKey);

				Document getShpmntOutDoc = GCCommonUtil
						.invokeAPI(
								env,
								GCConstants.GET_SHIPMENT_DTL,
								shipmentInputDoc,
								GCCommonUtil
								.getDocumentFromPath("/global/template/api/getShipmentList_For_ShipmentConfirmation_Email.xml"));

				Element invEle = (Element) inDoc.getElementsByTagName(
						"OrderInvoice").item(0);
				invKey = invEle.getAttribute("OrderInvoiceKey");

				ordInvDoc = GCXMLUtil.createDocument("GetOrderInvoiceDetails");
				Element ordInvEle = ordInvDoc.getDocumentElement();
				ordInvEle.setAttribute("InvoiceKey", invKey);

				Document shpmntInvDoc = GCCommonUtil.invokeService(env,
						GCConstants.GC_GET_INVOICE_DETAILS_SERVICE, ordInvDoc);
				Element shpmntEle = getShpmntOutDoc.getDocumentElement();

				Node nodeInv = getShpmntOutDoc.importNode(
						shpmntInvDoc.getDocumentElement(), true);
				shpmntEle.appendChild(nodeInv);

				// Adding the Order Level Attributes:
				Document ordDocReqForShpmnt = getOrderList(env, sOrderHeaderKey);
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("****ordDocReqForShpmnt****"
							+ GCXMLUtil.getXMLString(ordDocReqForShpmnt));
				}
				Element ordEle = (Element) ordDocReqForShpmnt
						.getElementsByTagName("Order").item(0);
				Element ordLineEle = (Element) ordDocReqForShpmnt
						.getElementsByTagName("OrderLine").item(0);
				shpmntEle.setAttribute("OrderNo",
						ordEle.getAttribute("OrderNo"));
				shpmntEle.setAttribute("FulfillmentType",
						ordLineEle.getAttribute("FulfillmentType"));

				Element ordExnEle = (Element) ordDocReqForShpmnt
						.getElementsByTagName("Extn").item(0);
				// Added for Defect No -GCStore-2073
				shpmntEle.setAttribute(
						"CustomerEMailID",
						ordEle.getAttribute("CustomerEMailID"));
				// Added for Defect No -GCStore-2073
				
				// GCPRO - 366: Start.
				shpmntEle.setAttribute(
						GCConstants.ENTRY_TYPE,
						ordEle.getAttribute(GCConstants.ENTRY_TYPE));
				// GCPRO - 366: End.

				if (ordExnEle != null) {
					shpmntEle.setAttribute("ExtnDAXOrderNo",
							ordExnEle.getAttribute("ExtnDAXOrderNo"));
					shpmntEle.setAttribute("ExtnShipToStoreID",
							ordExnEle.getAttribute("ExtnShipToStoreID"));
					shpmntEle.setAttribute("ExtnSourceOrderNo",
							ordExnEle.getAttribute("ExtnSourceOrderNo"));
					//Phase 2 GCSTORE-769 - fetch ExtnPickupStoreID
					sExtnPickupStoreID = ordExnEle.getAttribute(GCConstants.EXTN_PICKUP_STORE_ID);
				}

				// Check if email is allowed - Phase 2 GCSTORE-769
				if (YFCCommon.isStringVoid(sExtnPickupStoreID)) {
					isEmailAllowed = true;
				} else if (!YFCCommon.equals(sExtnPickupStoreID, sShipNode, false)) {
					isEmailAllowed = true;
				}

				// Check if email is allowed, then publish Shipment details -Phase 2 GCSTORE-769
				if (isEmailAllowed) {
					respDoc = publishShipmentDetails(env, getShpmntOutDoc, sOrderHeaderKey);
				}

			}

		} catch (Exception e) {
			LOGGER.error(
					" Inside catch of GCProcessOrderHoldType.processOrderHoldType()",
					e);
			throw new GCException(e);
		}
		LOGGER.debug(" Exiting GCProcessOrderHoldType.processOrderHoldType() method ");

		// If Email is not allowed, send response as Email not allowed - Phase 2 GCSTORE-769
		if (!isEmailAllowed) {
			respDoc = GCXMLUtil.createDocument("Response");
			Element respEle = respDoc.getDocumentElement();
			respEle.setAttribute("ResponseCode", "ERROR");
			respEle.setAttribute("ResponseDescription", "Email Not Allowed");
		}

		return respDoc;
	}

	@Override
	public void setProperties(Properties arg0) {

	}

	public Document publishShipmentDetails(YFSEnvironment env,
			Document inputDoc, String ordHdrKey) throws GCException {
			 Document respDoc = null;
		try {
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("****Publish Shipment Details method input*****"
						+ GCXMLUtil.getXMLString(inputDoc));
			}

			Element eleDoc = inputDoc.getDocumentElement();

			if (!"ShipmentList".equalsIgnoreCase(eleDoc.getNodeName())) {
				Element invEle = (Element) inputDoc.getElementsByTagName(
						"OrderInvoiceList").item(0);
				GCXMLUtil.removeChild(inputDoc.getDocumentElement(), invEle);

				Document shpListDoc = GCXMLUtil.createDocument("ShipmentList");
				Node shpmnt = shpListDoc.importNode(
						inputDoc.getDocumentElement(), true);

				GCXMLUtil.appendChild(shpListDoc.getDocumentElement(),
						(Element) shpmnt);
				 boolean isDigitalItems = checkForDigitalItems(env, eleDoc);
        if (isDigitalItems) {
          shpListDoc.getDocumentElement().setAttribute("EmailType", "SOFTWARE_ITEM_KEY");
        } else {
          shpListDoc.getDocumentElement().setAttribute("EmailType", "SHIPMENT_CONFIRMATION");
        }
		inputDoc = shpListDoc;
      }
      else{
        updateShipmentConfirmationAndSoftwareKey(env, eleDoc);
      }
			
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("****Shipment Confirmation Output*****"
						+ GCXMLUtil.getXMLString(inputDoc));
			}

			 NodeList shipmentLineList = eleDoc.getElementsByTagName("ShipmentLine");
      if (shipmentLineList.getLength() > 0) {
        YFCDocument changeOrdIndoc = YFCDocument.createDocument(GCConstants.ORDER);
        YFCElement orderEle = changeOrdIndoc.getDocumentElement();
        orderEle.setAttribute(GCConstants.ORDER_HEADER_KEY, ordHdrKey);

        GCCommonUtil.invokeService(env, GCConstants.ADD_NOTE_ON_SHIP_CONFIRM, changeOrdIndoc);

        GCCommonUtil
        .invokeService(
            env,
            GCConstants.GC_PUBLISH_TO_EMAIL_SHIPPING_CONFIRMATION_INBOUND_QUEUE,
            inputDoc);

        respDoc = GCXMLUtil.createDocument("Response");
        Element respEle = respDoc.getDocumentElement();
        respEle.setAttribute("ResponseCode", "SUCCESS");
        respEle.setAttribute("ResponseDescription",
            "Email Sent Successfully");

      } else {
        respDoc = GCXMLUtil.createDocument("Response");
        Element respEle = respDoc.getDocumentElement();
        respEle.setAttribute("ResponseCode", "ERROR");
        respEle.setAttribute("ResponseDescription", "No Record Found");

      }
      return respDoc;
		} catch (Exception e) {
			LOGGER.error(
					" Inside catch of GCShipmentConfirmationEmailService.publishShipmentDetails()",
					e);
			throw new GCException(e);
		}
	}

	 public boolean checkForDigitalItems(YFSEnvironment env, Element eleDoc) {
    boolean isDigitalItems = false;
    NodeList shipmentLineList = eleDoc.getElementsByTagName("ShipmentLine");
    int shipmentLineLength = shipmentLineList.getLength();
    for (int count = 0; count < shipmentLineLength; count++) {
      Element eleShipmentLine = (Element) shipmentLineList.item(count);
      Element eleExtn = (Element) eleShipmentLine.getElementsByTagName("Extn").item(0);
      String digitalDownLoadKey = eleExtn.getAttribute("ExtnDigitalDownloadKey");
      if (!YFCCommon.isVoid(digitalDownLoadKey)) {
        isDigitalItems = true;
        break;
      }
    }
    return isDigitalItems;
  }



  public void updateShipmentConfirmationAndSoftwareKey(YFSEnvironment env, Element eleDoc) {
    NodeList shipmentList = eleDoc.getChildNodes();
    Set<String> setForShipmentConfirmation = new HashSet<String>();
    Set<String> setForSoftwareKey = new HashSet<String>();
    String emailType = eleDoc.getAttribute("EmailType");
    NodeList shipmentLineList = eleDoc.getElementsByTagName("ShipmentLine");
    int shipmentLineLength = shipmentLineList.getLength();
    for (int count = 0; count < shipmentLineLength; count++) {
      Element eleShipmentLine = (Element) shipmentLineList.item(count);
      String shipmentKey = eleShipmentLine.getAttribute("ShipmentKey");
      Element eleExtn = (Element) eleShipmentLine.getElementsByTagName("Extn").item(0);
      String digitalDownLoadKey = eleExtn.getAttribute("ExtnDigitalDownloadKey");
      if ("SHIPMENT_CONFIRMATION".equals(emailType) && !YFCCommon.isVoid(digitalDownLoadKey)) {
        setForShipmentConfirmation.add(shipmentKey);
        eleShipmentLine.removeAttribute("ShipmentKey");
      } else if ("SOFTWARE_ITEM_KEY".equals(emailType) && YFCCommon.isVoid(digitalDownLoadKey)) {
        setForSoftwareKey.add(shipmentKey);
        eleShipmentLine.removeAttribute("ShipmentKey");
      }
      else {
        if (!YFCCommon.isVoid(shipmentKey)) {
          eleShipmentLine.removeAttribute("ShipmentKey");
        }
      }
    }

    for (int shipmentCount = 0; shipmentCount < shipmentList.getLength(); shipmentCount++) {

      Element eleShipment = (Element) shipmentList.item(shipmentCount);
      String shipmentKey = eleShipment.getAttribute("ShipmentKey");
      if (setForShipmentConfirmation.contains(shipmentKey) || setForSoftwareKey.contains(shipmentKey)) {
        eleShipment.removeAttribute(shipmentKey);
        eleShipment.getParentNode().removeChild(eleShipment);
        --shipmentCount;
      } else {
        if (!YFCCommon.isVoid(shipmentKey)) {
          eleShipment.removeAttribute("ShipmentKey");
        }
      }
    }
  }

	
	
	public Document getOrderList(YFSEnvironment env, String ordHdrKey)
			throws GCException {
		try {
			Document ordDoc = GCXMLUtil.createDocument("Order");
			Element ordEle = ordDoc.getDocumentElement();
			ordEle.setAttribute("OrderHeaderKey", ordHdrKey);
			String ordListTemp =
					"<OrderList><Order CustomerEMailID='' OrderNo='' OrderHeaderKey='' EntryType=''><Extn ExtnDAXOrderNo='' ExtnShipToStoreID='' ExtnSourceOrderNo='' ExtnPickupStoreID='' ExtnIsImportedOrder=''/><OrderLines><OrderLine FulfillmentType='' OrderLineKey=''><LinePriceInfo UnitPrice=''/><LineCharges><LineCharge ChargePerLine='' ChargeCategory=''/></LineCharges><LineTaxes><LineTax ChargeCategory='' Reference_1='' Tax='' TaxName=''/></LineTaxes></OrderLine></OrderLines></Order></OrderList>";
			Document ordLstDoc = GCCommonUtil.invokeAPI(env, "getOrderList",
					ordDoc, GCXMLUtil.getDocument(ordListTemp));
			return ordLstDoc;
		} catch (Exception e) {
			LOGGER.error(
					" Inside catch of GCShipmentConfirmationEmailService.getOrderList()",
					e);
			throw new GCException(e);
		}
	}

}
