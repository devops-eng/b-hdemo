/**
 * Copyright 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE:
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
 *          1.0                06/25/2014        Susai, John Melvin			Initial Version
 *#################################################################################################################################################################
 */
package com.gc.api.email;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="john.melvin@expicient.com">John Melvin</a>
 */
public class GCUpdateBackOrderDateForEmailNotification implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCUpdateBackOrderDateForEmailNotification.class.getName());

  /**
   *
   * Description of updateFTCAndBODate
   *
   *
   * @param env
   * @param inDoc
   * @return
   * @throws Exception
   *
   */
  public void updateFTCAndBODate(YFSEnvironment env, Document inDoc)
      throws GCException {
    LOGGER.debug(" Entering updateFTCAndBODate() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" updateFTCAndBODate() method, Input document is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    try {
      String sPaymentStatus = ((Element) inDoc.getElementsByTagName(
          "Order").item(0)).getAttribute(GCConstants.PAYMENT_STATUS);


      if (GCConstants.AUTHORIZED.equalsIgnoreCase(sPaymentStatus)) {
        String sOrderHeaderKey = ((Element) inDoc.getElementsByTagName(
            "Order").item(0))
            .getAttribute(GCConstants.ORDER_HEADER_KEY);

        if (!YFCCommon.isVoid(sOrderHeaderKey)) {

          String sGetOrderListInDoc = "<Order OrderHeaderKey=\""
              + sOrderHeaderKey + "\"></Order>";
          Document getOrderListInDoc = GCXMLUtil
              .getDocument(sGetOrderListInDoc);

          Document getOrderListOutDoc = GCCommonUtil
              .invokeAPI(
                  env,
                  GCConstants.GET_ORDER_LIST,
                  getOrderListInDoc,
                  GCCommonUtil
                  .getDocumentFromPath("/global/template/api/getOrderList_For_UpdateFTCAndBODate.xml"));

          Element orderEle = (Element) getOrderListOutDoc
              .getElementsByTagName("Order").item(0);

          List<String> orderDateList = new ArrayList<String>();
          for (int i = 0; i < orderEle.getElementsByTagName(
              "OrderDate").getLength(); i++) {
            orderDateList.add(((Element) orderEle
                .getElementsByTagName("OrderDate").item(i))
                .getAttribute("DateTypeId"));
          }

          if (!orderDateList.contains("BACKORDER_NOTIFY_REF_DATE")) {

            String sGetCommonCodeListInDoc = "<CommonCode CodeType=\"GC_EMAIL_HOLD_TYPE\" />";
            Document getCommonCodeListInDoc = GCXMLUtil
                .getDocument(sGetCommonCodeListInDoc);

            String sGetCommonCodeListTemplate = "<CommonCodeList><CommonCode CodeType=\"\" CodeValue=\"\" /></CommonCodeList>";
            Document getCommonCodeListTemplate = GCXMLUtil
                .getDocument(sGetCommonCodeListTemplate);

            Document getCommonCodeListOutDoc = GCCommonUtil
                .invokeAPI(env,
                    GCConstants.GET_COMMON_CODE_LIST,
                    getCommonCodeListInDoc,
                    getCommonCodeListTemplate);

            List<String> commonCodeList = new ArrayList<String>();
            for (int i = 0; i < getCommonCodeListOutDoc
                .getElementsByTagName("CommonCode").getLength(); i++) {
              commonCodeList
              .add(((Element) getCommonCodeListOutDoc
                  .getElementsByTagName("CommonCode")
                  .item(i)).getAttribute("CodeValue"));
            }
            if (!YFCCommon.isVoid(sOrderHeaderKey) && isUpdationRequired(orderEle, commonCodeList)) {
              Document docChangeOrderIp = GCXMLUtil
                  .createDocument("Order");
              Element eleRoot = docChangeOrderIp
                  .getDocumentElement();
              eleRoot.setAttribute(
                  GCConstants.ORDER_HEADER_KEY,
                  sOrderHeaderKey);

              // //Create current date object
              SimpleDateFormat dateFormat = new SimpleDateFormat(
                  "yyyy-MM-dd");
              SimpleDateFormat timeFormat = new SimpleDateFormat(
                  "HH:mm:ss");

              String currDate = "";
              currDate = dateFormat.format(new Date()) + "T"
                  + timeFormat.format(new Date());

              Element eleOrderDates = docChangeOrderIp
                  .createElement("OrderDates");
              eleRoot.appendChild(eleOrderDates);
              Element eleOrderDate = docChangeOrderIp
                  .createElement("OrderDate");
              eleOrderDates.appendChild(eleOrderDate);
              eleOrderDate.setAttribute("DateTypeId",
                  "BACKORDER_NOTIFY_REF_DATE");
              eleOrderDate.setAttribute("ActualDate",
                  currDate);

              Element eleOrderLines = docChangeOrderIp
                  .createElement("OrderLines");
              eleRoot.appendChild(eleOrderLines);
              for (int i = 0; i < orderEle
                  .getElementsByTagName("OrderLine")
                  .getLength(); i++) {
                Element eleOrderLine = docChangeOrderIp
                    .createElement("OrderLine");
                eleOrderLines.appendChild(eleOrderLine);
                eleOrderLine
                .setAttribute(
                    "OrderLineKey",
                    ((Element) orderEle
                        .getElementsByTagName(
                            "OrderLine")
                            .item(i))
                            .getAttribute("OrderLineKey"));

                Element eleTempOrderDates = docChangeOrderIp
                    .createElement("OrderDates");
                eleOrderLine.appendChild(eleTempOrderDates);
                Element eleTempOrderDate = docChangeOrderIp
                    .createElement("OrderDate");
                eleTempOrderDates
                .appendChild(eleTempOrderDate);
                eleTempOrderDate.setAttribute("DateTypeId",
                    "YCD_FTC_FIRST_PROMISE_DATE");
                eleTempOrderDate.setAttribute("ActualDate",
                    currDate);
              }
              if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("*****changeOrder input*****"
                    + GCXMLUtil
                    .getXMLString(docChangeOrderIp));
              }
              GCCommonUtil.invokeAPI(env, "changeOrder",
                  docChangeOrderIp);
            }
          }
        }
      }

    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCUpdateBackOrderDateForEmailNotification.updateFTCAndBODate()",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCUpdateBackOrderDateForEmailNotification.updateFTCAndBODate() method ");
    return;
  }

  @Override
  public void setProperties(Properties arg0) {

  }

  public boolean isUpdationRequired(Element orderEle,
      List<String> commonCodeList) throws GCException {
    try {
      Element orderHoldTypesEle = (Element) orderEle
          .getElementsByTagName("OrderHoldTypes").item(0);
      for (int i = 0; i < orderHoldTypesEle.getElementsByTagName(
          "OrderHoldType").getLength(); i++) {
        Element orderHoldTypeEle = (Element) orderHoldTypesEle
            .getElementsByTagName("OrderHoldType").item(i);
        String sStatus = orderHoldTypeEle
            .getAttribute(GCConstants.STATUS);
        if (GCConstants.STATUS_CREATED.equalsIgnoreCase(sStatus)
            || "1200".equalsIgnoreCase(sStatus)) {
          String sHoldType = orderHoldTypeEle
              .getAttribute(GCConstants.HOLD_TYPE);
          if (commonCodeList.contains(sHoldType)) {
            return false;
          }
        }
      }
      return true;
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCUpdateBackOrderDateForEmailNotification.isUpdationRequired()",
          e);
      throw new GCException(e);
    }
  }
}
