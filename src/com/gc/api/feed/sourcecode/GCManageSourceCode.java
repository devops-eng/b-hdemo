package com.gc.api.feed.sourcecode;

//imports
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;


/**
 * <h3>Description :</h3>This java class is called from 'GCCheckSourceCodeSrvc' Service to check if
 * the source code already exists in GC_SOURCE_CODE table and accordingly create or update the
 * record
 *
 * @author Infosys Limited
 */
public class GCManageSourceCode implements YIFCustomApi{

  //the class name
  final String className =GCManageSourceCode.class.getName();

  // Initialize Logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCManageSourceCode.class);

  /*
   * The configurations from configurator
   */
  Properties props;


  /**
   * <h3>Description :</h3> The method to set the arguments from configurator.
   */
  @Override
  public void setProperties(final Properties props){

    //setting the properties
    this.props=props;
  }


  /**
   * <h3>Description :</h3> This method is used to invoke various methods to prepare input for
   * getGCSourceCodeList, to call getGCSourceCodeList API, etc.
   *
   * @param yfsEnv The YFS Environment reference
   * @param docInXML The input coming from DAX
   * @return The document used to update/create a source code record in GC_SOURCE_CODE table
   */
  public Document manageGCSourceCode( final YFSEnvironment yfsEnv, final Document docInXML){

    // logging the method entry
    LOGGER.info( className +":manageGCSourceCode:Entry");

    // logging the method input
    if( LOGGER.isDebugEnabled()){

      LOGGER.debug("feed from DAX" + GCXMLUtil.getXMLString(docInXML));
    }

    // Calling method prepareIpDocForGcSouceCodeList
    final Document docGetGcSrCodeLstIp = prepareIpDocForGcSouceCodeList( docInXML);

    // Calling method callGetGcSrcCodeLstApi
    final Document docGetGcSrCodeLstOp = callGetGcSrcCodeLstApi(yfsEnv, docGetGcSrCodeLstIp);

    // Calling method checkApiOutput
    final boolean bUpdateSrcCode = isSourceCodePresent( docGetGcSrCodeLstOp);

    // Calling method IsChangeOrCreate
    final Document docOutput = updateOrCreateSrcCode(yfsEnv, bUpdateSrcCode, docInXML, docGetGcSrCodeLstOp);

    // logging the method exit
    LOGGER.info(className+":manageGCSourceCode:Exit");

    // returning the document
    return docOutput;

  }

  /**
   * <h3>Description :</h3> This method is used to prepare input XML for getGCSourceCodeList
   *
   * @param env
   *            The YFS Environment reference
   * @param docInXML
   *            The input coming from DAX
   * @return The document is the input XML to getGCSourceCodeList API
   */
  public Document prepareIpDocForGcSouceCodeList( final Document docInXML) {

    // logging the method entry
    if( LOGGER.isDebugEnabled()){

      LOGGER.debug( className+":prepareIpDocForGcSouceCodeList:Entry");
    }

    // fetch the source code from input XML
    final String strGcSourceCode = docInXML.getDocumentElement().getAttribute(GCConstants.SOURCE_CODE);

    // logging source code attribute
    if( LOGGER.isDebugEnabled()){

      LOGGER.debug("The strGcSourceCode is" + strGcSourceCode);
    }

    // create a input document to getGCOrderListAPI
    final Document docGetGcSrCodeLstIp = GCXMLUtil.createDocument(GCConstants.ELEMENT_GC_SOURCE_CODE);

    final Element eleSourceCode = docGetGcSrCodeLstIp.getDocumentElement();
    eleSourceCode.setAttribute(GCConstants.SOURCE_CODE,	strGcSourceCode);

    // logging the input to getGCSourceCodeList API
    if( LOGGER.isDebugEnabled()){

      LOGGER.debug("The input XML prepared for getGCSourceCodeList API"
          + GCXMLUtil.getXMLString(docGetGcSrCodeLstIp));
    }

    // logging the method exit
    if( LOGGER.isDebugEnabled()){

      LOGGER.debug(className+":prepareIpDocForGcSouceCodeList:Exit");
    }

    // returning the input document prepared
    return docGetGcSrCodeLstIp;

  }


  /**
   * <h3>Description :</h3>
   * This method is used to call  getGCSourceCodeList API
   *
   * @param yfsEnv
   *            The YFS Environment reference
   * @param docGetGcSrCodeLstIp
   *            The input xml to getGCSourceCodeList API
   * @return The document is the output XML from  getGCSourceCodeList API
   */
  private Document callGetGcSrcCodeLstApi(final YFSEnvironment yfsEnv, final Document docGetGcSrCodeLstIp){

    // logging the method entry
    if( LOGGER.isDebugEnabled()){

      LOGGER.debug( className+":callGetGcSrcCodeLstApi::Entry");
    }

    // logging the input to the API
    if( LOGGER.isDebugEnabled()){

      LOGGER.debug("The input going to  getGcSourceCodeList API" + GCXMLUtil.getXMLString(docGetGcSrCodeLstIp));
    }

    //call API getGCSourceCodeList
    final Document docGetGcSrCodeLstOp =
        GCCommonUtil.invokeService(yfsEnv, GCConstants.GET_SOURCE_CODE_LIST_SVC, docGetGcSrCodeLstIp);

    // logging the output from the API
    if( LOGGER.isDebugEnabled()){

      LOGGER.debug("The output from getGcSourceCodeList API" + GCXMLUtil.getXMLString(docGetGcSrCodeLstOp));
    }

    // logging the method exit
    if( LOGGER.isDebugEnabled()){

      LOGGER.debug( className+":callGetGcSrcCodeLstApi:Exit");
    }

    return docGetGcSrCodeLstOp;

  }


  /**
   * <h3>Description :</h3>
   * This method is used to check if the output from getGcSourceCode API contains any records
   *
   * @param env
   *            The YFS Environment reference
   * @param docGetGcSrCodeLstOp
   *            The output from  getGCSourceCodeList API
   * @return true if the getSourceCodeList output contains records else false
   */
  public boolean isSourceCodePresent(	final Document docGetGcSrCodeLstOp){

    // logging the method entry
    if( LOGGER.isDebugEnabled()){

      LOGGER.debug(className+":checkApiOutput:Entry");
    }

    // logging the output from getGCOrderListAPI
    if( LOGGER.isDebugEnabled()){

      LOGGER.debug("The output from getGcSourceCodeList API"
          + GCXMLUtil.getXMLString(docGetGcSrCodeLstOp));
    }

    // fetch  GCSourceCode element
    final NodeList nlSourceCode = GCXMLUtil.getNodeListByXpath(docGetGcSrCodeLstOp,
        "//GCSourceCodeList/GCSourceCode");

    final int iSrcCodeLen = nlSourceCode.getLength();
    final boolean isSourceCodePresent;
    if (iSrcCodeLen > 0) {

      isSourceCodePresent= true;
    } else{

      isSourceCodePresent= false;
    }

    // logging the method exit
    if( LOGGER.isDebugEnabled()){

      LOGGER.debug(className+":checkApiOutput:Exit");
    }

    return isSourceCodePresent;

  }


  /**
   * <h3>Description :</h3> This method is used to call changeGCSourceCode/createGCSourceCode API
   *  based on the result returned from 'checkApiOutput' method
   *
   * @param yfsEnv
   *            The YFS Environment reference
   *  @param isRecordExists
   *            boolean variable indicating if the records exists
   * @param docInXML
   *             The input coming from Tibco
   *  @param docGetSrcCodeLstOp The getSourceCodeList ouptput
   *
   * @return the document from the API calls
   */
  public Document updateOrCreateSrcCode(final YFSEnvironment yfsEnv, final boolean isRecordExists,
      final Document docInXML, final Document docGetSrcCodeLstOp) {

    // logging the method entry
    if( LOGGER.isDebugEnabled()){

      LOGGER.debug(className+":updateOrCreateSrcCode:Entry");
    }

    //return document
    final Document docOutput;

    //check if the record exist
    if (isRecordExists) {

      //method to update the input document for changeSourceCode api
      final Document docChgSrcCodeIn = prepareChgSrcCodeIn(docInXML, docGetSrcCodeLstOp);

      //call API changeGCSourceCode
      docOutput = GCCommonUtil.invokeService(yfsEnv, GCConstants.CHANGE_GC_SOURCE_CODE_SVC, docChgSrcCodeIn);
    }else{

      //call API createGCSourceCode
      docOutput = GCCommonUtil.invokeService(yfsEnv,GCConstants.CREATE_GC_SOURCE_CODE_SC, docInXML);
    }

    // logging the method exit
    if( LOGGER.isDebugEnabled()){

      LOGGER.debug(className+":updateOrCreateSrcCode:Exit");
    }

    return docOutput;

  }


  /**
   * <h3>Description :</h3> This method is used to update the input xml with the source code key
   * from the document passed in the second parameter
   *
   * @param docInXML the document for which the source code key needs to be updated
   * @param docGetSrcCodeLstOp The document from which the source code key is fetched
   * @return The updated document containing the sourcecode key
   */
  private Document prepareChgSrcCodeIn(final Document docInXML, final	Document docGetSrcCodeLstOp) {

    // logging the method entry
    if( LOGGER.isDebugEnabled()){

      LOGGER.debug(className+":prepareChgSrcCodeIn:Entry");
    }

    //fetch the SourceCodeKey from the getSourceCodeList api outpu
    final Element eleGetSrcCdLstOp = docGetSrcCodeLstOp.getDocumentElement();
    final Element eleGCSrcCode = (Element) eleGetSrcCdLstOp.getElementsByTagName(GCConstants.GC_SOURCE_CODE).item(0);
    final String strSourceCodeKey = eleGCSrcCode.getAttribute( GCConstants.SOURCE_CODE_KEY);

    //set the source code key in the input xml
    final Element eleInRoot = docInXML.getDocumentElement();
    eleInRoot.setAttribute( GCConstants.SOURCE_CODE_KEY, strSourceCodeKey);

    // logging the method output and exit
    if( LOGGER.isDebugEnabled()){

      LOGGER.debug( className +":prepareChgSrcCodeIn:Output:" + GCXMLUtil.getXMLString( docInXML));

      LOGGER.debug(className+":prepareChgSrcCodeIn:Exit");
    }

    //return the updated input xml
    return docInXML;
  }


}// End of the class GCManageSourceCode