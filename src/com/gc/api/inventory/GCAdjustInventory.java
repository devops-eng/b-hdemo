/** 
 * Copyright © 2014, Guitar Center,  All rights reserved.
 *#################################################################################################################################################################
 *	        OBJECTIVE: This is class is used to adjust inventory of items.
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
 *            1.0               11/02/2014       Gowda, Naveen		OMS-230: As an OMS, I want to receive web inventory shipment messages so that I can adjust 
 *                                                                            available inventory and open reservations to provide accurate inventory levels to customers. 
 *                                                                  OMS-228: As an OMS, I want to receive web physical inventory adjustment messages so that I can 
 *                                                                            adjust inventory levels and provide accurate inventory levels to customers          
 *#################################################################################################################################################################
 */
package com.gc.api.inventory;

import java.util.Properties;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:naveen.s@expicient.com">Naveen</a>
 */
public class GCAdjustInventory implements YIFCustomApi {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCAdjustInventory.class.getName());

    /**
     * 
     * Description of adjustInventory - This method is used to adjust Inventory
     * based on the feed from HighJump. The quantity can be a positive or
     * negative value.
     * 
     * @param env
     *            YFSEnvironment
     * @param inDoc
     *            Input XML Document
     * @throws Exception
     *             Exception
     * 
     */
    public void adjustInventory(YFSEnvironment env, Document inDoc)
	    throws GCException {

	LOGGER.debug("Class: GCAdjustInventory Method: adjustInventory BEGIN");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug("Incoming Document" + GCXMLUtil.getXMLString(inDoc));
	}
	try {
	    Element eleRoot = inDoc.getDocumentElement();
	    NodeList nodeListItems = GCXMLUtil.getNodeListByXpath(inDoc,
		    GCConstants.ITEMS_ITEM_XPATH);
	    LOGGER.debug("nodeListItems Length=" + nodeListItems.getLength());
	    for (int i = 0; i < nodeListItems.getLength(); i++) {
		Element eleItem = (Element) nodeListItems.item(i);
        String uom = eleItem.getAttribute(GCConstants.UNIT_OF_MEASURE);
        String orgCode = eleItem.getAttribute(GCConstants.ORGANIZATION_CODE);
        String itemID = eleItem.getAttribute(GCConstants.ITEM_ID);
        Double qty = Double.parseDouble(eleItem.getAttribute(GCConstants.QUANTITY));
		Document docGtItmDtOt = fetchingItemDetails(env, eleItem);

		// Fetching Kit Code and Extn Set Code
		String sKitCode = GCXMLUtil.getAttributeFromXPath(docGtItmDtOt,
			GCConstants.ITEMLIST_ITEM_PRIMARYINFO_KIT_CODE_XPATH);
		String sSetCode = GCXMLUtil.getAttributeFromXPath(docGtItmDtOt,
			GCConstants.ITEMLIST_ITEM_EXTN_SET_CODE_XPATH);
	    String sUsed = GCXMLUtil.getAttributeFromXPath(docGtItmDtOt,
            "ItemList/Item/Extn/@ExtnIsUsedOrVintageItem");
        if (YFCUtils.equals(sUsed, GCConstants.YES) && qty < 0) {
          YFCDocument manageItemIndoc = YFCDocument.createDocument(GCConstants.ITEM_LIST);
          YFCElement eleItemList = manageItemIndoc.getDocumentElement();
          YFCElement eleItemIp = eleItemList.createChild(GCConstants.ITEM);
          eleItemIp.setAttribute(GCConstants.ACTION, "Manage");
          eleItemIp.setAttribute(GCConstants.UNIT_OF_MEASURE, uom);
          eleItemIp.setAttribute(GCConstants.ORGANIZATION_CODE, orgCode);
          eleItemIp.setAttribute(GCConstants.ITEM_ID, itemID);
          YFCElement eleExtn = eleItemIp.createChild(GCConstants.EXTN);
          eleExtn.setAttribute(GCConstants.EXTN_CATEGORY, "");
          GCCommonUtil.invokeService(env, "GCPostToManageItemQ", manageItemIndoc.getDocument());
        }
		// If Item is a bundle them exploding components.
		if ((GCConstants.BUNDLE).equalsIgnoreCase(sKitCode)) {
		    getComponentDetails(docGtItmDtOt, eleItem, inDoc, eleRoot,
			    sSetCode);
		}
	    }
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("Input Doc" + GCXMLUtil.getXMLString(inDoc));
	    }
	    GCCommonUtil
		    .invokeAPI(env, GCConstants.API_ADJUST_INVENTORY, inDoc);
	} catch (Exception e) {
	    LOGGER.error("Exception GCAdjustInventory:adjustInventory ", e);
	    throw new GCException(e);
	}
	LOGGER.debug("Class: GCAdjustInventory Method: adjustInventory END");
    }

    /**
     * 
     * Description of fetchingItemDetails - This method is used to fetch item
     * details and determine whether it is a bundle item or not.
     * 
     * @param env
     *            YFSEnvironment
     * @param eleItem
     *            Item Element
     * @return Return getItemList Output Document
     * @throws Exception
     * 
     */
    public Document fetchingItemDetails(YFSEnvironment env, Element eleItem)
	    throws GCException {

	LOGGER.debug("Class: GCAdjustInventory Method: fetchingItemDetails BEGIN");
	// Fetching required attributes for getItemList API call.
	String strItemID = eleItem.getAttribute(GCConstants.ITEM_ID);
	Document docGtItmDtOt = null;
	try {
	    // Preparing inputDoc for getItemList API call
	    Document docGtItmDtIp = GCXMLUtil.createDocument(GCConstants.ITEM);
	    Element eleItemRoot = docGtItmDtIp.getDocumentElement();
	    eleItemRoot.setAttribute(GCConstants.ITEM_ID, strItemID);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("get Items API input"
			+ GCXMLUtil.getXMLString(docGtItmDtIp));
	    }
	    Document docGtItmDtTmp = GCXMLUtil
		    .getDocument(GCConstants.GET_ITEM_LIST_OUT_TEMP);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("template for getItemList API call"
			+ GCXMLUtil.getXMLString(docGtItmDtTmp));
	    }
	    // Calling getItemList API.
	    docGtItmDtOt = GCCommonUtil.invokeAPI(env,
		    GCConstants.API_GET_ITEM_LIST, docGtItmDtIp, docGtItmDtTmp);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("getItemList OutDoc"
			+ GCXMLUtil.getXMLString(docGtItmDtOt));
	    }
	    LOGGER.debug("Class: GCAdjustInventory Method: fetchingItemDetails END");
	} catch (Exception e) {
	    throw new GCException(e);
	}
	return docGtItmDtOt;
    }

    /**
     * 
     * Description of getComponentDetails - This method is used to replace
     * bundle item in the input xml with it components before the API call.
     * 
     * @param docGtItmDtOt
     *            Output document of getItemList API call.
     * @param eleItem
     *            Item Element
     * @param inDoc
     *            Input XML Document.
     * @param eleRoot
     *            Root element of Input Document.
     * @param sSetCode
     *            ExtnSetCode value.
     * @throws ParserConfigurationException
     *             ParserConfigurationException
     * @throws TransformerException
     *             TransformerException
     * 
     */
    public void getComponentDetails(Document docGtItmDtOt, Element eleItem,
	    Document inDoc, Element eleRoot, String sSetCode)
	    throws GCException {
	LOGGER.debug("Class: GCAdjustInventory Method: getComponentDetails BEGIN");
	try {
	    NodeList nodeListComponents = GCXMLUtil.getNodeListByXpath(
		    docGtItmDtOt,
		    GCConstants.ITEMLIST_ITEM_COMPONENTS_COMPONENT_XPATH);
	    LOGGER.debug("nodeListComponents Length="
		    + nodeListComponents.getLength());
	    Boolean booIsTrue = true;
	    // Looping through component items.
	    for (int j = 0; j < nodeListComponents.getLength(); j++) {

		Element eleCmpnt = (Element) nodeListComponents.item(j);

		Element eleCompItem = inDoc.createElement(GCConstants.ITEM);
		// Copying attributes of parent item to component item element
		GCXMLUtil.copyAttributes(eleItem, eleCompItem);
		if (booIsTrue) {
		    // Removing bundle item from the XML.
		    GCXMLUtil.removeChild(eleRoot, eleItem);
		    booIsTrue = false;

		}
		String sComItmID = eleCmpnt
			.getAttribute(GCConstants.COMPONENT_ITEM_ID);
		String sKitQty = eleCmpnt
			.getAttribute(GCConstants.KIT_QUANTITY);
		String strQty = eleCompItem.getAttribute(GCConstants.QUANTITY);
		// Component ratio is multiplied against the quantity.
		Double dFinalQty = Double.parseDouble(sKitQty)
			* Double.parseDouble(strQty);
		String sfinalQty = dFinalQty.toString();
		eleCompItem.setAttribute(GCConstants.QUANTITY, sfinalQty);
		eleCompItem.setAttribute(GCConstants.ITEM_ID, sComItmID);
		// If ExtnSetCode is Y then component's product class is changed
		// to
		// SET.
		if ((GCConstants.INT_ONE).equalsIgnoreCase(sSetCode)) {
		    eleCompItem.setAttribute(GCConstants.PRODUCT_CLASS,
			    GCConstants.SET);
		}
		eleRoot.appendChild(eleCompItem);

	    }
	} catch (Exception e) {
	    throw new GCException(e);
	}
	LOGGER.debug("Class: GCAdjustInventory Method: getComponentDetails END");
    }

    @Override
    public void setProperties(Properties arg0) {

    }
}
