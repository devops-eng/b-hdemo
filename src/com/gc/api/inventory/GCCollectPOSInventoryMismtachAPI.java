/**Copyright 2015 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.

	Change Log
######################################################################################################################################
     OBJECTIVE: Foundation Class
######################################################################################################################################
        Version    CR/ User Story                 Date                       Modified By                   Description
######################################################################################################################################
        1.0        Defect-1940                   04/24/2014                   Jain,Shubham           Development of RTAM Inventory Published from POS to ATG
######################################################################################################################################
 */package com.gc.api.inventory;

 import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

 /**
  * This class is used to publish the Inventory from POS to ATG.
  * @author shubham.jain
  *
  */
 public class GCCollectPOSInventoryMismtachAPI {
   private static final YFCLogCategory LOGGER = YFCLogCategory
       .instance(GCCollectPOSInventoryMismtachAPI.class.getName());

   /**
    * This method is create the indoc for adjustInventory from syncLoadedInventory Indoc and Invoke the
    * adjustInventory API.
    * @param env
    * @param inDoc
    */
   public Document collectPOSInventoryMismatch(YFSEnvironment env, Document inDoc) {

     LOGGER.beginTimer("GCCollectPOSInventoryMismtachAPI.collectPOSInventoryMismatch");
     LOGGER.verbose("Class: GCCollectPOSInventoryMismtachAPI Method: collectPOSInventoryMismatch BEGIN");

     YFCDocument inputDoc=YFCDocument.getDocumentFor(inDoc);
     YFCElement eleRoot= inputDoc.getDocumentElement();

     if (LOGGER.isVerboseEnabled()){
       LOGGER.verbose("Input Document :" + inputDoc.getString());
     }

     String sShipNode=eleRoot.getAttribute(GCConstants.SHIP_NODE);
     if(!YFCCommon.equals(sShipNode, "MFI")){

       String sInventoryOrgCode=eleRoot.getAttribute("InventoryOrganizationCode");
       String sItemID=eleRoot.getAttribute(GCConstants.ITEM_ID);
       String sProductClass=eleRoot.getAttribute(GCConstants.PRODUCT_CLASS);
       String sUOM=eleRoot.getAttribute(GCConstants.UNIT_OF_MEASURE);


       YFCDocument adjustInventoryinDoc=YFCDocument.createDocument("Items");
       YFCElement eleItems=adjustInventoryinDoc.getDocumentElement();


       YFCElement eleSupplies=eleRoot.getChildElement(GCConstants.SUPPLIES);
       YFCNodeList<YFCElement> listSupply=eleSupplies.getElementsByTagName(GCConstants.SUPPLY);
       for(YFCElement eleSupply:listSupply){

         String sAvailabilityType=eleSupply.getAttribute(GCConstants.AVAILABILITY_TYPE);
         String sChangedQuantity=eleSupply.getAttribute("ChangedQuantity");
         String sSupplyType=eleSupply.getAttribute(GCConstants.SUPPLY_TYPE);

         YFCElement eleItem = eleItems.createChild(GCConstants.ITEM);
         eleItem.setAttribute(GCConstants.ADJUSTMENT_TYPE, "ADJUSTMENT");
         eleItem.setAttribute(GCConstants.ITEM_ID, sItemID);
         eleItem.setAttribute(GCConstants.PRODUCT_CLASS, sProductClass);
         eleItem.setAttribute(GCConstants.SHIP_NODE, sShipNode);
         eleItem.setAttribute(GCConstants.UNIT_OF_MEASURE, sUOM);
         eleItem.setAttribute(GCConstants.ORGANIZATION_CODE, sInventoryOrgCode);
         eleItem.setAttribute(GCConstants.AVAILABILITY_TYPE,sAvailabilityType);
         eleItem.setAttribute(GCConstants.QUANTITY, sChangedQuantity);
         eleItem.setAttribute(GCConstants.SUPPLY_TYPE, sSupplyType);
         //GCSTORE-7162, this fix will address removing entries from YFS_INVENTORY_NODE_CONTROL table 
         eleItem.setAttribute(GCConstants.REMOVE_INVENTORY_NODE_CONTROL, GCConstants.YES);

         YFCElement eleTag=eleSupply.getChildElement(GCConstants.TAG);
         if(!YFCCommon.isVoid(eleTag)){
           String sBatchNo=eleTag.getAttribute(GCConstants.BATCH_NO);
           String sLotAttribute=eleTag.getAttribute("LotAttribute1");


           YFCElement eleTagAdjustInv=eleItem.createChild(GCConstants.TAG);
           eleTagAdjustInv.setAttribute(GCConstants.BATCH_NO, sBatchNo);
           eleTagAdjustInv.setAttribute("LotAttribute1",sLotAttribute);

         }
       }

       if (LOGGER.isVerboseEnabled()){
         LOGGER.verbose("Input of Adjust Inventory :" + adjustInventoryinDoc.getString());
       }

       YFCNodeList<YFCElement> itemInAdjInv = eleItems.getElementsByTagName(GCConstants.ITEM);
       if (itemInAdjInv.getLength() > 0) {
         LOGGER.verbose("Going to call adjustInventory");
         GCCommonUtil.invokeAPI(env, "adjustInventory", adjustInventoryinDoc.getDocument());
         LOGGER.verbose("Seems adjustInventory was successful");
       }
     }
     LOGGER.endTimer("GCCollectPOSInventoryMismtachAPI.collectPOSInventoryMismatch");
    return inDoc;
   }
 }






































































