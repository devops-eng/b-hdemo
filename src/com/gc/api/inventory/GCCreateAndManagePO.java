/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 * *#################################################################################################################################################################
 *          OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
 *            1.0              20/02/2014        Gowda, Naveen              OMS-240: As an OMS, I want to receive updates for PO's that have changed so that I can 
 *                                                                                   modify my PO/On Order quantity that is reserved against supply�
 *            2.0              29/08/2015        Infosys Ltd.               GCSTORE-4338: Prod: PO Updates from DAX to OMS fail if any PO line in OMS is closed
 *#################################################################################################################################################################
 */
package com.gc.api.inventory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * @author <a href="mailto:naveen.s@expicient.com">Naveen</a>
 */
public class GCCreateAndManagePO {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
        .instance(GCCreateAndManagePO.class.getName());

    /**
     * 
     * Description of managePO - This method is used to create or change
     * purchase order.
     * 
     * @param env
     *            YFSEnvironment
     * @param inDoc
     *            Input Document
     * @throws Exception
     *             Exception
     * 
     */
    public void managePO(YFSEnvironment env, Document inDoc) throws GCException {

    LOGGER.debug("Class: GCCreateAndManagePO Method:managePO BEGIN");
    if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Incoming Document" + GCXMLUtil.getXMLString(inDoc));
    }
    try {
        Element eleRoot = inDoc.getDocumentElement();
        String sDAXOrderNo = "";
        sDAXOrderNo = GCXMLUtil.getAttributeFromXPath(inDoc,
            "//Extn/@ExtnDAXOrderNo");
        eleRoot.setAttribute(GCConstants.ORDER_NO, sDAXOrderNo);
        
        //GCSTORE-4338 - Begin
        Element elePersonInfoBillTo = GCXMLUtil.getElementByXPath(inDoc, "Order/PersonInfoBillTo");
        //GCSTORE-4338 - End
        
        String sAction = eleRoot.getAttribute(GCConstants.ACTION);

        if (YFCCommon.isVoid(sAction)) {
        final YFSException yfsEx = new YFSException();
        yfsEx.setErrorCode(GCErrorConstants.ACTION_NULL_ERROR_CODE);
        yfsEx.setErrorDescription(GCErrorConstants.ACTION_NULL_ERROR_DESCRIPTION);
        throw yfsEx;
        }
        if ((GCConstants.MODIFY).equalsIgnoreCase(sAction)
            || (GCConstants.ACTION_CANCEL).equalsIgnoreCase(sAction)) {
        String sOrderNo = fetchOrderNoUsingDAXOrderNo(env, sDAXOrderNo);
        eleRoot.setAttribute(GCConstants.ORDER_NO, sOrderNo);
        }

        NodeList nLOrderLines = GCXMLUtil.getNodeListByXpath(inDoc,
            GCConstants.ORDER_ORDERLINES_ORDERLINE_XPATH);
        if(LOGGER.isDebugEnabled()) {
        LOGGER.debug("nLOrderLines Length=" + nLOrderLines.getLength());
        }
        if(!YFCCommon.isVoid(sAction) && (GCConstants.CREATE).equalsIgnoreCase(sAction))
        {
        for (int i = 0; i < nLOrderLines.getLength(); i++) {
        Element eleOrderLine = (Element) nLOrderLines.item(i);
        Element eleItem = GCXMLUtil.getFirstElementByName(eleOrderLine,
            GCConstants.ITEM);
        GCAdjustInventory obj = new GCAdjustInventory();
        Document docGtItmDtOt = obj.fetchingItemDetails(env, eleItem);

        // Fetching Kit Code and Extn Set Code
        String sKitCode = GCXMLUtil.getAttributeFromXPath(docGtItmDtOt,
            GCConstants.ITEMLIST_ITEM_PRIMARYINFO_KIT_CODE_XPATH);
        String sSetCode = GCXMLUtil.getAttributeFromXPath(docGtItmDtOt,
            GCConstants.ITEMLIST_ITEM_EXTN_SET_CODE_XPATH);
        // If Item is a bundle them exploding components.
        
        if ((GCConstants.BUNDLE).equalsIgnoreCase(sKitCode)) {
            eleOrderLine.setAttribute(GCConstants.KIT_CODE,
                GCConstants.BUNDLE);
            if ((GCConstants.INT_ONE).equalsIgnoreCase(sSetCode)) {
            eleItem.setAttribute(GCConstants.PRODUCT_CLASS,
                GCConstants.SET);
            }
        }
        
        

     }
        }
        //GCSTORE-6281-If Action is Modify, then fetching the product class from existing PO OrderLine
        else if(!YFCCommon.isVoid(sAction) && ((GCConstants.MODIFY).equalsIgnoreCase(sAction) || (GCConstants.CANCEL).equalsIgnoreCase(sAction)))
        {
            Document docGetOrderListInp=GCXMLUtil.createDocument("Order");
            Element eleOrder=docGetOrderListInp.getDocumentElement();
            Element eleInDoc=inDoc.getDocumentElement();
            eleOrder.setAttribute("OrderNo", eleInDoc.getAttribute("OrderNo"));
            eleOrder.setAttribute("EnterpriseCode", eleInDoc.getAttribute("EnterpriseCode"));
            eleOrder.setAttribute("DocumentType", eleInDoc.getAttribute("DocumentType"));
            
            
            Document docGetOrderListTemp=GCXMLUtil.getDocument("<OrderList> <Order OrderHeaderKey='' OrderNo=''> <OrderLines> <OrderLine PrimeLineNo='' SubLineNo=''> "
                    + "<Item UnitOfMeasure='' ProductClass='' ItemID=''/></OrderLine> </OrderLines> </Order> </OrderList>");
         
            Document docGetOrderListOp=  GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST, docGetOrderListInp,docGetOrderListTemp);
            NodeList nlOrderLinesGetOrderListOp=docGetOrderListOp.getElementsByTagName("OrderLine");
            if(LOGGER.isDebugEnabled()) {
            LOGGER.debug("GCCreateAndManagePO :: managePO :: docGetOrderListOp :: \n"+GCXMLUtil.getXMLString(docGetOrderListOp));
            }
            
            for (int i = 0; i < nlOrderLinesGetOrderListOp.getLength(); i++) {
                
                Element eleOrderLineGetOrderListOp=(Element) nlOrderLinesGetOrderListOp.item(i);
                if(LOGGER.isDebugEnabled()) {
                LOGGER.debug("GCCreateAndManagePO :: managePO :: eleOrderLineGetOrderListOp :: \n"+GCXMLUtil.getElementString(eleOrderLineGetOrderListOp));
                }
                
                Element eleItemGetOrderListOp=(Element) eleOrderLineGetOrderListOp.getElementsByTagName("Item").item(0);
                if(LOGGER.isDebugEnabled()) {
                LOGGER.debug("GCCreateAndManagePO :: managePO :: eleItemGetOrderListOp :: \n"+GCXMLUtil.getElementString(eleItemGetOrderListOp));
                }
                
                String strProductClassFromGetOrderList=eleItemGetOrderListOp.getAttribute("ProductClass");
                
                if(LOGGER.isDebugEnabled()) {
                LOGGER.debug("GCCreateAndManagePO :: managePO :: strProductClassFromGetOrderList :: \n"+strProductClassFromGetOrderList);
                }

                  for(int j=0;j<nLOrderLines.getLength();j++)
                  {
                    Element eleOrderLine=(Element) nLOrderLines.item(j);
                    if(LOGGER.isDebugEnabled()) {
                    LOGGER.debug("GCCreateAndManagePO :: managePO :: eleOrderLine :: \n"+GCXMLUtil.getElementString(eleOrderLine));  
                    }
                    
                    if(eleOrderLine.getAttribute("PrimeLineNo").equals(eleOrderLineGetOrderListOp.getAttribute("PrimeLineNo")) && eleOrderLine.getAttribute("SubLineNo").equals(eleOrderLineGetOrderListOp.getAttribute("SubLineNo")))
                    { 
                        Element eleItemInDoc=(Element) eleOrderLine.getElementsByTagName("Item").item(0);
                        if(LOGGER.isDebugEnabled()) {
                        LOGGER.debug("GCCreateAndManagePO :: managePO :: eleItemInDoc :: \n"+GCXMLUtil.getElementString(eleItemInDoc));  
                        }
                        
                        String strProductClassFromInDoc=eleItemInDoc.getAttribute("ProductClass");
                        if(LOGGER.isDebugEnabled()) {
                        LOGGER.debug("GCCreateAndManagePO :: managePO :: strProductClassFromInDoc :: \n"+strProductClassFromInDoc); 
                        }
                       
                    
                    if(!YFCCommon.isVoid(strProductClassFromGetOrderList) && !YFCCommon.isVoid(strProductClassFromInDoc) && !strProductClassFromGetOrderList.equals(strProductClassFromInDoc) )
                    {
                        if(LOGGER.isDebugEnabled()) {
                        LOGGER.debug("GCCreateAndManagePO :: managePO :: not equal");
                        }
                        eleItemInDoc.setAttribute("ProductClass", strProductClassFromGetOrderList);
                        break;
                    }
                   
                    }
                  }
            }
            if(LOGGER.isDebugEnabled()) {
            LOGGER.debug("GCCreateAndManagePO :: managePO :: inDOC :: \n"+GCXMLUtil.getXMLString(inDoc));
            }
        }
        
        
        if ((GCConstants.CREATE).equalsIgnoreCase(sAction)) {
        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug("InDoc to createOrder API call"
                + GCXMLUtil.getXMLString(inDoc));
        }
        Document docOut = GCCommonUtil.invokeAPI(env,
            GCConstants.API_CREATE_ORDER, inDoc);
        if(LOGGER.isDebugEnabled()) {
        LOGGER.debug("OutDoc of createOrder API call"
            + GCXMLUtil.getXMLString(docOut));
        }
        } else if ((GCConstants.MODIFY).equalsIgnoreCase(sAction)) {
        
        //GCSTORE-4338 - Begin
        if(!YFCCommon.isVoid(elePersonInfoBillTo)){
        eleRoot.removeChild(elePersonInfoBillTo);
        }
        //GCSTORE-4338 - End
        if(LOGGER.isDebugEnabled()) {
        LOGGER.debug("InDoc to changeOrder API call"
            + GCXMLUtil.getXMLString(inDoc));
        }
        Document docOut = GCCommonUtil.invokeAPI(env,
            GCConstants.API_CHANGE_ORDER, inDoc);
        if(LOGGER.isDebugEnabled()) {
        LOGGER.debug("OutDoc of changeOrder API call"
            + GCXMLUtil.getXMLString(docOut));
        }
        
        publishRTAMUpdateForChangeInPO(env, inDoc);
        
        } else if ((GCConstants.CANCEL).equalsIgnoreCase(sAction)) {
        //GCSTORE-4338 - Begin
        if(!YFCCommon.isVoid(elePersonInfoBillTo)){
        eleRoot.removeChild(elePersonInfoBillTo);
        }
        //GCSTORE-4338 - End
        GCCommonUtil
            .invokeAPI(env, GCConstants.API_CANCEL_ORDER, inDoc);

        } else {
        final YFSException yfsEx = new YFSException();
        yfsEx.setErrorCode(GCErrorConstants.INVALID_ACTION_ERROR_CODE);
        yfsEx.setErrorDescription(GCErrorConstants.INVALID_ACTION_ERROR_DESCRIPTION);
        throw yfsEx;
        }
    } catch (Exception e) {
        throw new GCException(e);
    }
    if(LOGGER.isDebugEnabled()) {
    LOGGER.debug("Class: GCCreateAndManagePO Method:managePO END");
    }
    }

    private void publishRTAMUpdateForChangeInPO(YFSEnvironment env, Document inDoc){
        YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
        YFCElement eleRoot = inputDoc.getDocumentElement();
        String sOrganizationCode=eleRoot.getAttribute(GCConstants.ENTERPRISE_CODE);
        YFCElement eleOrderLines=eleRoot.getChildElement(GCConstants.ORDER_LINES);
        YFCNodeList<YFCElement> listOrderLine=eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
        
        for(YFCElement eleOrderLine:listOrderLine){
            String sReceivingNode=eleOrderLine.getAttribute(GCConstants.RECEIVING_NODE);
            YFCElement eleItem=eleOrderLine.getChildElement(GCConstants.ITEM);
            String sItemID=eleItem.getAttribute(GCConstants.ITEM_ID);
            String sProductClass=eleItem.getAttribute(GCConstants.PRODUCT_CLASS);
            String sUOM=eleItem.getAttribute(GCConstants.UNIT_OF_MEASURE);
            
            
            YFCDocument createInventoryActivityInDoc=YFCDocument.createDocument("InventoryActivity");
            YFCElement eleInventoryActivity=createInventoryActivityInDoc.getDocumentElement();
            eleInventoryActivity.setAttribute(GCConstants.ITEM_ID, sItemID);
            eleInventoryActivity.setAttribute(GCConstants.ORGANIZATION_CODE, sOrganizationCode);
            eleInventoryActivity.setAttribute(GCConstants.NODE, sReceivingNode);
            eleInventoryActivity.setAttribute(GCConstants.PRODUCT_CLASS, sProductClass);
            eleInventoryActivity.setAttribute(GCConstants.UNIT_OF_MEASURE, sUOM);
             
            GCCommonUtil.invokeAPI(env, GCConstants.CREATE_INVENTORY_ACTIVITY, createInventoryActivityInDoc.getDocument());
         
        }
        
    }
    /**
     * 
     * Description of fetchOrderNoUsingDAXOrderNo
     * 
     * @param env
     * @param sDAXOrderNo
     * @return
     * @throws GCException
     * 
     */
    public String fetchOrderNoUsingDAXOrderNo(YFSEnvironment env,
        String sDAXOrderNo) throws GCException {
    String sOrderNo = null;
    try {
        Document docGetOrderLstIn = GCXMLUtil
            .createDocument(GCConstants.ORDER);
        Element eleRoot = docGetOrderLstIn.getDocumentElement();
        Element eleExtn = docGetOrderLstIn.createElement(GCConstants.EXTN);
        eleExtn.setAttribute(GCConstants.EXTN_DAX_ORDER_NO, sDAXOrderNo);
        eleRoot.appendChild(eleExtn);
        Document docGetOrderListTemp = GCXMLUtil
            .getDocument(GCConstants.GET_ORDER_LIST_TEMPLATE_FOR_DAX_NO);
        if(LOGGER.isDebugEnabled()) {
        LOGGER.debug("Input document to getOrderList API"
            + GCXMLUtil.getXMLString(docGetOrderLstIn));
        }
        Document docGetOrdrLstOut = GCCommonUtil.invokeAPI(env,
            GCConstants.API_GET_ORDER_LIST, docGetOrderLstIn,
            docGetOrderListTemp);
        if(LOGGER.isDebugEnabled()) {
        LOGGER.debug("Output document of getOrderList API"
            + GCXMLUtil.getXMLString(docGetOrdrLstOut));
        }
        sOrderNo = GCXMLUtil.getAttributeFromXPath(docGetOrdrLstOut,
            "//Order/@OrderNo");
    } catch (Exception e) {
        throw new GCException(e);
    }
    return sOrderNo;
    }
}
