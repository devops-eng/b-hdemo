/**Copyright 2014 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.

    Change Log
######################################################################################################################################
     OBJECTIVE: Foundation Class
######################################################################################################################################
        Version          CR                 Date                       Modified By                       Description
######################################################################################################################################
         1.0        Initial Version      12/19/2014                Rangarajan,Shwetha           Development of Demand Sync message to be
                                                                                                 published to DAX
         1.1        Modified Version     01/05/2015                Sinha, Rajiv                 Updated to publish demand in case no Demand element received in getDemandSummary
          																						call irrespective of whether it is infinite inventory item or not
######################################################################################################################################
 */
package com.gc.api.inventory;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * GCSTORE-53 - Demand Feed to DAX. This class is used to capture the demand picture for an item at
 * a particular point of time and publish the same to DAX for achieving demand sync.
 * <p>
 * The class invokes getDemandSummary API to obtain the demand details of an item. The logic to
 * calculate the total demand is implemented and this consolidated demand is to be published to DAX
 *
 * @author <a href="mailto:shwetha.rangarajan@expicient.com">Shwetha</a>
 */
public class GCDemandSyncToDAXAPI implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCDemandSyncToDAXAPI.class.getName());

  /**
   * This is the main method which implements the overall logic of publishing the demand sync to
   * DAX.
   *
   * @param env - YFSEnvironment variable used to call APIs.
   * @param inDoc - Input document to the method.
   * @throws GCException
   */

  public Document generateDemandSyncToDAX(YFSEnvironment env, Document inputDoc) {

    LOGGER.beginTimer("GCDemandSyncToDAXAPI.generateDemandSyncToDAX");
    LOGGER.verbose("Class: GCDemandSyncToDAXAPI Method: generateDemandSyncToDAX BEGIN");


    // Fetch the inDoc
    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    // Initialize outDoc
    YFCDocument outDoc = YFCDocument.createDocument(GCXmlLiterals.DEMAND_SUMMARY);
    boolean isDemandPublished = false;
    // Fetch the root element from inDoc to check for the event invoked.
    YFCElement rootEle = inDoc.getDocumentElement();
    String rootEleStr = rootEle.getNodeName();

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input to generateDemandSyncToDAX method :" + inDoc.toString());
    }

    if (YFCCommon.equals(rootEleStr, GCConstants.DEMAND, false)) {
      isDemandPublished = publishOnDemandChange(env, outDoc, rootEle);
      //GCSTORE-5272- Removed some code here which was adding current demand to getDemandSummary Output.
      LOGGER.verbose("Didnt change the quantity");
      if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("Message published to be published to DAX from Demand Change Event :" + outDoc.toString());
        }
      
    } else if (YFCCommon.equals(rootEleStr, GCConstants.ORDER, false)) {
      isDemandPublished = publishOnOrderCancel(env, outDoc, rootEle);
      if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("Message published to be published to DAX from Change Order Event :" + outDoc.toString());
        }
    } else if (YFCCommon.equals(rootEleStr, GCXmlLiterals.ITEMS, false)) {
      isDemandPublished = publishOnRsrvnChange(env, outDoc, rootEle);
      if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("Message published to be published to DAX from Reservation change Event :" + outDoc.toString());
        }
    }

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Demand picture obtained :" + outDoc.toString());
    }

    if (isDemandPublished) {
      // Stamp current time to the message to be published
      YDate date = new YDate(false);
      YFCElement demandSmryEle = outDoc.getDocumentElement();
      demandSmryEle.setAttribute(GCConstants.DATE_TIME_STAMP, date.getString(GCConstants.TIME_STAMP));

      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("Message published to DAX :" + outDoc.toString());
      }
      // Invoke a service that publishes DemandSync message to DAX
      GCCommonUtil.invokeService(env, GCConstants.GC_PUBLISH_DMNDMSG_TO_DAX, outDoc.getDocument());
    }

    LOGGER.verbose("Exiting GCDemandSyncToDAXAPI.generateDemandSyncToDAX() method");
    LOGGER.endTimer("GCDemandSyncToDAXAPI.generateDemandSyncToDAX");
    return inputDoc;
  }

  /**
   * The method implements logic to publish demand of an item on any reservation change.
   *
   * @param env
   * @param outDoc
   * @param rootEle
   * @return
   */
  private boolean publishOnRsrvnChange(YFSEnvironment env, YFCDocument outDoc, YFCElement rootEle) {

    LOGGER.beginTimer("GCDemandSyncToDAXAPI.publishOnRsrvnChange");
    LOGGER.verbose("EVENT - INVENTORYCHANGE.RESERVATION_CHANGE");

    YFCIterable<YFCElement> itemList = rootEle.getChildren();
    String itemDetails;
    boolean isDemandPublished = false;

    // Initialize the ItemArrayList
    List<String> itemListFinal = new ArrayList<String>();

    // Loop through Item list to fetch item details.
    for (YFCElement itemEle : itemList) {

      String itemID = itemEle.getAttribute(GCConstants.ITEM_ID);
      String unitOfMeasure = itemEle.getAttribute(GCConstants.UNIT_OF_MEASURE);
      String productClass = itemEle.getAttribute(GCConstants.PRODUCT_CLASS);

      YFCElement rsrvnChngsEle = itemEle.getChildElement(GCXmlLiterals.RESERVATION_CHANGES);
      YFCElement reservationEle = rsrvnChngsEle.getChildElement(GCXmlLiterals.RESERVATION);
      String shipNode = reservationEle.getAttribute(GCConstants.SHIP_NODE);
      if(!YFCCommon.equals(GCConstants.MFI, shipNode, false)){
        continue;
      }
      if (YFCCommon.equals(GCConstants.SET, productClass, false)) {
        String reservationID = reservationEle.getAttribute(GCConstants.RESERVATION_ID);
        int index = reservationID.indexOf(GCConstants.UNDERSCORE);
        itemID = reservationID.substring(index + 1);
      }

      LOGGER.verbose("ItemID fetched : " + itemID);
      LOGGER.verbose("ProductClass fetched :" + productClass);
      itemDetails = itemID + ":" + unitOfMeasure + ":" + productClass;

      if (!itemListFinal.contains(itemDetails)) {
        itemListFinal.add(itemDetails);
        YFCDocument getDmndSmryIndoc = YFCDocument.createDocument(GCXmlLiterals.DEMAND_SUMMARY);
        YFCElement dmndSmryInEle = getDmndSmryIndoc.getDocumentElement();
        dmndSmryInEle.setAttribute(GCConstants.ITEM_ID, itemID);
        dmndSmryInEle.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GCI);
        dmndSmryInEle.setAttribute(GCConstants.PRODUCT_CLASS, productClass);
        dmndSmryInEle.setAttribute(GCConstants.SHIP_NODE, shipNode);
        dmndSmryInEle.setAttribute(GCConstants.UNIT_OF_MEASURE, unitOfMeasure);
        boolean tempCanPublishDemand = getDemandSummaryForItem(env, getDmndSmryIndoc, outDoc);
        isDemandPublished = isDemandPublished || tempCanPublishDemand;
      }
    }

    LOGGER.verbose("Class: GCDemandSyncToDAXAPI Method: publishOnRsrvnChange END");
    LOGGER.endTimer("GCDemandSyncToDAXAPI.publishOnRsrvnChange");
    return isDemandPublished;
  }

  /**
   * The method implements logic to publish demand for an item on Order cancellation.
   *
   * @param env
   * @param outDoc
   * @param rootEle
   * @return
   */
  private boolean publishOnOrderCancel(YFSEnvironment env, YFCDocument outDoc, YFCElement rootEle) {

    LOGGER.beginTimer("GCDemandSyncToDAXAPI.publishOnOrderCancel");
    LOGGER.verbose("EVENT - ORDERCHANGE.ONCANCEL");

    // Fetch and loop through the OrderLine tag
    YFCElement orderLinesEle = rootEle.getChildElement(GCConstants.ORDER_LINES);
    YFCIterable<YFCElement> orderLineList = orderLinesEle.getChildren();

    // Initialize the ItemArrayList
    List<String> itemListFinal = new ArrayList<String>();
    boolean isDemandPublished = false;

    // Loop through OrderLines to fetch item details.
    for (YFCElement orderLineEle : orderLineList) {

      double chngInOrdQty = orderLineEle.getDoubleAttribute(GCXmlLiterals.CHANGE_IN_ORDER_QUANTITY);
      String isBundleParent = orderLineEle.getAttribute(GCConstants.IS_BUNDLE_PARENT);

      YFCElement statusBrkupEle = orderLineEle.getChildElement(GCXmlLiterals.STATUS_BRKUP_ELE);
      YFCElement cncldFrmEle = statusBrkupEle.getChildElement(GCXmlLiterals.CANCELED_FROM);
      YFCElement detailsEle = cncldFrmEle.getChildElement(GCXmlLiterals.DETAILS);
      String shipNode = detailsEle.getAttribute(GCConstants.SHIP_NODE);

      YFCElement itemEle = orderLineEle.getChildElement(GCConstants.ITEM);
      String productClass = itemEle.getAttribute(GCConstants.PRODUCT_CLASS);
      String itemID = itemEle.getAttribute(GCConstants.ITEM_ID);
      String unitOfMeasure = itemEle.getAttribute(GCConstants.UNIT_OF_MEASURE);

      LOGGER.verbose("ChangeInOrderQty :" + chngInOrdQty);
      LOGGER.verbose("ShipNode :" + shipNode);

      // If the Order is canceled and shipped from MFI
      if (chngInOrdQty < 0 && (YFCCommon.equals(GCConstants.MFI, shipNode, false) || YFCCommon.isVoid(shipNode))) {

        String itemDetails;
        // Input for getDemandSummary API
        YFCDocument getDmndSmryIndoc = YFCDocument.createDocument(GCXmlLiterals.DEMAND_SUMMARY);
        YFCElement dmndSmryInEle = getDmndSmryIndoc.getDocumentElement();
        dmndSmryInEle.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GCI);
        dmndSmryInEle.setAttribute(GCConstants.SHIP_NODE, GCConstants.MFI);
        dmndSmryInEle.setAttribute(GCConstants.PRODUCT_CLASS, productClass);

        // if the OrderLine has a BundleParent and is not a SET item
        // Call getItemList to fetch component details and getDemandSummary for them.
        if (YFCCommon.equals(GCConstants.YES, isBundleParent, false)
            && !YFCCommon.equals(GCConstants.SET, productClass, false)) {

          YFCDocument getItemListTemp = YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LIST_TEMP);
          if (LOGGER.isVerboseEnabled()) {
            LOGGER.verbose("Item is a bundle parent and ProductClass is Regular");
            LOGGER.verbose("getItemList template :" + getItemListTemp.toString());
          }

          YFCDocument getItemListOutdoc =
              GCCommonUtil.getItemList(env, itemID, unitOfMeasure, GCConstants.GCI, true,getItemListTemp);

          if (LOGGER.isVerboseEnabled()) {
            LOGGER.verbose("getItemList output :" + getItemListOutdoc.toString());
          }
          YFCElement itemListEle = getItemListOutdoc.getDocumentElement();
          YFCElement itemChildEle = itemListEle.getChildElement(GCConstants.ITEM);

          if (YFCCommon.isVoid(itemChildEle)) {
            return false;
          }

          YFCElement componentsEle = itemChildEle.getChildElement(GCXmlLiterals.COMPONENTS);
          YFCIterable<YFCElement> componentList = componentsEle.getChildren();

          for (YFCElement componentEle : componentList) {

            String componentItemID = componentEle.getAttribute(GCConstants.COMPONENT_ITEM_ID);
            String componentUOM = componentEle.getAttribute(GCConstants.COMPONENT_UOM);

            itemDetails = componentItemID + ":" + componentUOM + ":" + productClass;

            // if the item is not present in the array
            if (!itemListFinal.contains(itemDetails)) {
              // Add the item details to the array and call getDemandSummary for the Item
              itemListFinal.add(itemDetails);
              dmndSmryInEle.setAttribute(GCConstants.ITEM_ID, componentItemID);
              dmndSmryInEle.setAttribute(GCConstants.UNIT_OF_MEASURE, componentUOM);
              boolean tempCanPublishDemand = getDemandSummaryForItem(env, getDmndSmryIndoc, outDoc);
              isDemandPublished = isDemandPublished || tempCanPublishDemand;
            }
          }
        } else {

          // if not a bundle or the item is a SET item fetch the item details from inDoc and
          // call getDemandSummary API

          itemDetails = itemID + ":" + unitOfMeasure + ":" + productClass;
          if (!itemListFinal.contains(itemDetails)) {
            itemListFinal.add(itemDetails);
            dmndSmryInEle.setAttribute(GCConstants.ITEM_ID, itemID);
            dmndSmryInEle.setAttribute(GCConstants.UNIT_OF_MEASURE, unitOfMeasure);
            boolean tempCanPublishDemand = getDemandSummaryForItem(env, getDmndSmryIndoc, outDoc);
            isDemandPublished = isDemandPublished || tempCanPublishDemand;
          }
        }
      }
    }
    LOGGER.verbose("Class: GCDemandSyncToDAXAPI Method: publishOnOrderCancel END");
    LOGGER.endTimer("GCDemandSyncToDAXAPI.publishOnOrderCancel");
    return isDemandPublished;
  }

  /**
   * The method implements the logic to publish the demand message to DAX on demand change for any
   * item.
   *
   * @param env - YFSEnvironemnt variable used to make API calls.
   * @param outDoc - Document to be published to DAX.
   * @param rootEle
   * @return
   */
  private boolean publishOnDemandChange(YFSEnvironment env, YFCDocument outDoc, YFCElement rootEle) {

    LOGGER.beginTimer("GCDemandSyncToDAXAPI.publishOnDemandChange");
    LOGGER.verbose("Class: GCDemandSyncToDAXAPI Method: publishOnDemandChange BEGIN");
    LOGGER.verbose("Event - INVENTORYCHANGE.DEMAND_CHANGE");
    boolean isDemandPublished = false;

    if (isEligibleForDemandPublish(rootEle)) {
      // Fetch Item attributes required from inDoc
      String itemID = rootEle.getAttribute(GCConstants.ITEM_ID);
      // When Order moves from Scheduled to Backorder - Demand is to be published.
      // Hence stamp ShipNode
      String shipNode = rootEle.getAttribute(GCConstants.SHIP_NODE);
      if (YFCCommon.isVoid(shipNode)) {
        shipNode = GCConstants.MFI;
      }
      String unitOfMeaurse = rootEle.getAttribute(GCConstants.UNIT_OF_MEASURE);
      String pdctClass = rootEle.getAttribute(GCConstants.PRODUCT_CLASS);
      // Fetch OrderLineKey from the event XML
      String orderLineKey = rootEle.getAttribute(GCConstants.ORDER_LINE_KEY);

      // Prepare input for API
      YFCDocument getDmndSmryIndoc = YFCDocument.createDocument(GCXmlLiterals.DEMAND_SUMMARY);
      YFCElement dmndSmryInEle = getDmndSmryIndoc.getDocumentElement();
      dmndSmryInEle.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GCI);
      dmndSmryInEle.setAttribute(GCConstants.PRODUCT_CLASS, pdctClass);
      dmndSmryInEle.setAttribute(GCConstants.SHIP_NODE, shipNode);
      dmndSmryInEle.setAttribute(GCConstants.UNIT_OF_MEASURE, unitOfMeaurse);

      // if ProductClass is 'SET' then call getOrderLineList to fetch item ID for the SET item
      if (YFCCommon.equals(GCConstants.SET, pdctClass, false) && !YFCCommon.isVoid(orderLineKey)) {

        // Prepare the inDoc for getOrderLineList API
        /*YFCDocument getOrdLineListIndoc = YFCDocument.createDocument(GCConstants.ORDER_LINE);
        YFCElement orderLineEle = getOrdLineListIndoc.getDocumentElement();
        orderLineEle.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey);
        // Fetch the template and call getOrderLineList API
        YFCDocument getOrdLineListTemp = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LINELIST_TEMP);
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("getOrderLineList Input :" + getOrdLineListIndoc.toString());
          LOGGER.verbose("getOrderLineList Template :" + getOrdLineListTemp.toString());
        }

        YFCDocument getOrdLineListOutdoc =
            GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LINE_LIST, getOrdLineListIndoc, getOrdLineListTemp);
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("getOrderLineList Output :" + getOrdLineListOutdoc.toString());
        }

        YFCElement ordLineListEle = getOrdLineListOutdoc.getDocumentElement();
        YFCElement ordLineEle = ordLineListEle.getChildElement(GCConstants.ORDER_LINE);
        YFCElement bundleParentLine = ordLineEle.getChildElement(GCConstants.BUNDLE_PARENT_LINE);
        YFCElement bundleItemEle = bundleParentLine.getChildElement(GCConstants.ITEM);
        String setItemID = bundleItemEle.getAttribute(GCConstants.ITEM_ID);*/

        dmndSmryInEle.setAttribute(GCConstants.ITEM_ID, itemID);
        // Call getDemandSummary API
        isDemandPublished = getDemandSummaryForItem(env, getDmndSmryIndoc, outDoc);
      } else if (!YFCCommon.equals(GCConstants.SET, pdctClass, false)) {

        // If not a SET Item stamp the ItemID from the XML to the getDemandSummary API input
        dmndSmryInEle.setAttribute(GCConstants.ITEM_ID, itemID);
        // Call getDemandSummary API
        isDemandPublished = getDemandSummaryForItem(env, getDmndSmryIndoc, outDoc);
      }

    }

    LOGGER.verbose("Class: GCDemandSyncToDAXAPI Method: publishOnDemandChange END");
    LOGGER.endTimer("GCDemandSyncToDAXAPI.publishOnDemandChange");
    return isDemandPublished;
  }


  /**
   * This method calls getDemandSummary API and calculates the overall demand.
   *
   * @param env - YFSEnvironment variable used to call APIs
   * @param getDmndSmryIndoc - getDemandSummary API Input
   * @param outDoc - The output message to be published to DAX
   * @return boolean -
   */
  private boolean getDemandSummaryForItem(YFSEnvironment env, YFCDocument getDmndSmryIndoc, YFCDocument outDoc) {

    LOGGER.beginTimer("GCDemandSyncToDAXAPI.getDemandSummaryForItem");
    LOGGER.verbose("Method : getDemandSummaryForItem : START");

    YFCElement rootEle = getDmndSmryIndoc.getDocumentElement();
    String pdctClass = rootEle.getAttribute(GCConstants.PRODUCT_CLASS);
    String itemID = rootEle.getAttribute(GCConstants.ITEM_ID);
    String shipNode = rootEle.getAttribute(GCConstants.SHIP_NODE);

    // 22-12-2014 - Changes for SET items
    // If ShipNode is blank - Do not publish any message.
    if (YFCCommon.isVoid(shipNode)) {
      return false;
    }
    YFCElement demandSmryInEle = getDmndSmryIndoc.getDocumentElement();

    YFCElement demandSmryEle = outDoc.getDocumentElement();

    double overallQty = 0;

    String unitOfMeasure = rootEle.getAttribute(GCConstants.UNIT_OF_MEASURE);

    // If Item's product class is SET, fetch the parent details and publish demand for the Parent
    // item
    if (YFCCommon.equals(GCConstants.SET, pdctClass, false)) {

      YFCDocument getItemListTemp = YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LIST_TEMP);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("getItemList template :" + getItemListTemp.toString());
      }
      YFCDocument getItemListOutdoc =
          GCCommonUtil.getItemList(env, itemID, unitOfMeasure, GCConstants.GCI, true, getItemListTemp);

      YFCElement itemListEle = getItemListOutdoc.getDocumentElement();
      YFCElement itemChildEle = itemListEle.getChildElement(GCConstants.ITEM);

      if (YFCCommon.isVoid(itemChildEle)) {
        return false;
      }

      YFCElement componentsEle = itemChildEle.getChildElement(GCXmlLiterals.COMPONENTS);
      YFCIterable<YFCElement> componentList = componentsEle.getChildren();
      YFCElement eleExtn = itemChildEle.getChildElement(GCConstants.EXTN);
      String sExtnPrimaryComponentID = eleExtn.getAttribute(GCXmlLiterals.EXTN_PRIMARY_COMPONENT_ITEM);

      for (YFCElement componentEle : componentList) {

        String componentItemID = componentEle.getAttribute(GCConstants.COMPONENT_ITEM_ID);
        String componentUOM = componentEle.getAttribute(GCConstants.COMPONENT_UOM);
        Double kitQty = componentEle.getDoubleAttribute(GCConstants.KIT_QUANTITY);

        if(YFCCommon.equals(sExtnPrimaryComponentID, componentItemID)){
        	getDmndSmryIndoc = YFCDocument.createDocument(GCXmlLiterals.DEMAND_SUMMARY);
            demandSmryInEle = getDmndSmryIndoc.getDocumentElement();
            demandSmryInEle.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GCI);
            demandSmryInEle.setAttribute(GCConstants.ITEM_ID, componentItemID);
            demandSmryInEle.setAttribute(GCConstants.PRODUCT_CLASS, pdctClass);
            demandSmryInEle.setAttribute(GCConstants.SHIP_NODE, shipNode);
            demandSmryInEle.setAttribute(GCConstants.UNIT_OF_MEASURE, componentUOM);

            double sumOfQty = calculateDemandSmry(env, getDmndSmryIndoc);
            if (sumOfQty > 0) {
              double demandQty = sumOfQty / kitQty;
              if ((overallQty > demandQty) || YFCCommon.equals(overallQty,0 )) {
                overallQty = demandQty;
              }
            }
            break;
          }   
        }
        
    } else {
      overallQty = calculateDemandSmry(env, getDmndSmryIndoc);
    }
    LOGGER.verbose("Total Demand calculated :" + overallQty);

    YFCElement demandEle = demandSmryEle.createChild(GCConstants.DEMAND);
    demandEle.setAttribute(GCConstants.ITEM_ID, itemID);
    demandEle.setAttribute(GCConstants.PRODUCT_CLASS, pdctClass);
    demandEle.setAttribute(GCConstants.SHIP_NODE, shipNode);
    demandEle.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GCI);
    demandEle.setAttribute(GCConstants.UNIT_OF_MEASURE, unitOfMeasure);
    demandEle.setAttribute(GCConstants.QUANTITY, "" + overallQty);

    LOGGER.verbose("Method : getDemandSummaryForItem : END");
    LOGGER.endTimer("GCDemandSyncToDAXAPI.getDemandSummaryForItem");
    return true;
  }

  /**
   * This method calls getDemandSummary API and calculates the total demand.
   *
   * @param env - YFSEnvironment used to make API calls.
   * @param getDmndSmryIndoc - getDemandSummary Input
   * @param isDemandChangeValid
   * @return total demand calculated
   */
  private double calculateDemandSmry(YFSEnvironment env, YFCDocument getDmndSmryIndoc) {

    LOGGER.beginTimer("GCDemandSyncToDAXAPI.calculateDemandSmry");
    LOGGER.verbose("Method : calculateDemandSmry : START");
    double overallQty = 0;

    YFCDocument getDmndSmryTempdoc = YFCDocument.getDocumentFor(GCConstants.GET_DMND_SMRY_TEMP);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getDemandSummary template :" + getDmndSmryTempdoc.toString());
    }

    YFCDocument getDmndSmryOutdoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_DMND_SMRY, getDmndSmryIndoc, getDmndSmryTempdoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getDemandSummary output :" + getDmndSmryOutdoc.toString());
    }

    YFCElement demandSmryOutEle = getDmndSmryOutdoc.getDocumentElement();
    YFCIterable<YFCElement> demandList = demandSmryOutEle.getChildren();

    for (YFCElement demandOutEle : demandList) {
      double quantity = demandOutEle.getDoubleAttribute(GCConstants.QUANTITY);
      String demandType = demandOutEle.getAttribute(GCConstants.DEMAND_TYPE);
      if (YFCUtils.equals("DAX_RESERVED.ex", demandType)) {
        continue;
      }

      overallQty += quantity;
    }

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Overall demand calculated :" + overallQty);
      LOGGER.verbose("Method : calculateDemandSmry : END");
    }
    LOGGER.endTimer("GCDemandSyncToDAXAPI.calculateDemandSmry");
    return overallQty;
  }


  /**
   * The method is used to filter the scenarios where in the messages are to be published to DAX.
   *
   * @param rootEle - Indoc to the method (DEMAND_CHANGE event XML)
   * @return - boolean
   */
  private boolean isEligibleForDemandPublish(YFCElement rootEle) {

    LOGGER.beginTimer("GCDemandSyncToDAXAPI.isEligibleForDemandPublish");
    LOGGER.verbose("Method:isEligibleForDemandPublish :START");

    List<String> demandStatusList = new ArrayList<String>();
    demandStatusList.add("OPEN_ORDER");
    demandStatusList.add("RSRV_ORDER");
    demandStatusList.add("RESERVED");
    demandStatusList.add("SCHEDULED");
    demandStatusList.add("BACKORDER");

    // Fetch ShipNode attribute from the inDoc
    String shipNode = rootEle.getAttribute(GCConstants.SHIP_NODE);
    String confirmShipment = rootEle.getAttribute(GCConstants.CONFIRM_SHIPMENT);
    String demandType = rootEle.getAttribute(GCConstants.DEMAND_TYPE);

    // Since demand snapshot has to published only for MFI node, go ahead for demand publishing only
    // when
    // 1. ShipNode = MFI
    // or 2. ShipNode = blank and Order is moving to BackOrder (could be the case when Backorder
    // happens of DC fulfilled order
    if (!(YFCCommon.equals(GCConstants.MFI, shipNode, false) || (YFCCommon.isVoid(shipNode) && YFCCommon.equals(
        GCConstants.BACKORDER, demandType, false)))) {
      return false;
    }
    // Fetch Quantity and DemandType attributes
    double quantity = rootEle.getDoubleAttribute(GCConstants.QUANTITY);

    LOGGER.verbose("Quantity fetched :" + quantity + " and DemandType fetched :" + demandType);

    // Whenever deamand is created for following demandTypes publish the message to DAX
    // 1.OPEN_ORDER 2.RSRV_ORDR 3.RESERVED 4.SCHEDULED 5.BACKORDERED
    if (quantity > 0 && demandStatusList.contains(demandType)) {
      return true;
    }
	//GCSTORE-5272::Begin
    if (quantity < 0 && "RSRV_ORDER".equals(demandType)) {
        return true;
      }
    //GCSTORE-5272::End
    if (YFCCommon.equals(GCConstants.ALLOCATED, demandType, false)
        && YFCCommon.equals(GCConstants.YES, confirmShipment, false)) {
      return true;
    }
    LOGGER.verbose("Method:isEligibleForDemandPublish :END");
    LOGGER.endTimer("GCDemandSyncToDAXAPI.isEligibleForDemandPublish");
    return false;
  }

  @Override
  public void setProperties(Properties arg0) {
    // This method is used to fetch values from service args.
  }

}
