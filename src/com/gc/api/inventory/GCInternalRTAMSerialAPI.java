/**
 * Copyright 2013 Guitar Center. All rights reserved. Guitar Center PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 *
 * Change Log
 * #######################################################################################
 * ############################################### OBJECTIVE: Foundation Class
 * ######################
 * ############################################################################
 * #################################### Version CR/ User Story Date Modified By Description
 * #########
 * #########################################################################################
 * #################################### 1.0 GCSTORE-52 12/22/2014 Jain,Shubham Development of RTAM
 * Published from OMS to ATG 1.1 GCSTORE-160 02/11/2015 Jain,Shubham Development for Tags Controlled
 * Items for COM 1.2 GCSTORE - 934 31/03/2015 Rangarajan, Shwetha Changed Tag Attribute to
 * LotAttribute1.
 * ###################################################################################
 * ###################################################
 */

package com.gc.api.inventory;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCOrderUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * GCSTORE-52 Inventory Sync from OMS to ATG This class is used for customization of RTAM messages
 * getting published to ATG from OMS if Item is Serialized and if Inventory is available in OMS.
 * <p>
 * In this class getSupplyDetails API will be invoked and it will check for the BatchNo and
 * LotNumber.If BatchNo is available then it will append that Tag element with BatchNo and LotNumber
 * of that particular Item into the inputDoc Document. And then it will published to ATG by invoking
 * service GCPublishToATG.
 *
 * @author Shubham Jain <a href="mailto:shubham.jain@expicient.com">Shubham</a>
 *
 */

public class GCInternalRTAMSerialAPI implements YIFCustomApi {
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCInternalRTAMSerialAPI.class.getName());

  /**
   * This method is used to append the Tag elements into the inDoc Document if the Item is
   * Serialized and for those Tag details having inventory in OMS.
   *
   * @param env
   * @param inputDoc
   */

  public Document getSerialAvailability(YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("GCInternalRTAMSerialAPI.getSerialAvailability");
    LOGGER.verbose("Class: GCInternalRTAMSerialAPI Method: getSerialAvailability BEGIN");

    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleRoot = inputDoc.getDocumentElement();
    String sShipNode = eleRoot.getAttribute(GCConstants.NODE);
    Double dOnhandAvailableQuantity = eleRoot.getDoubleAttribute("OnhandAvailableQuantity");

    if (!YFCCommon.isVoid(sShipNode)) {
      YFCElement eleItem = eleRoot.getChildElement(GCConstants.ITEM);
      String sItemID = eleItem.getAttribute(GCConstants.ITEM_ID);
      String sOrganizationCode = eleItem.getAttribute(GCConstants.ORGANIZATION_CODE);
      String sProductClass = eleItem.getAttribute(GCConstants.PRODUCT_CLASS);
      String sUnitOfMeasure = eleItem.getAttribute(GCConstants.UNIT_OF_MEASURE);
      YFCElement elePriInfo = eleItem.getChildElement(GCConstants.PRIMARY_INFORMATION);
      String sKitCode = elePriInfo.getAttribute(GCConstants.KIT_CODE);

      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("Input to RTAM :" + inputDoc.getString());
      }

      if (dOnhandAvailableQuantity > 0) {

        // Creating a New Document of getSupplyDetails
        YFCDocument getSupplyDetailsinDoc = YFCDocument.createDocument(GCConstants.GET_SUPPLY_DETAILS);
        YFCElement eleGetSupplyDetails = getSupplyDetailsinDoc.getDocumentElement();
        eleGetSupplyDetails.setAttribute(GCConstants.ITEM_ID, sItemID);
        eleGetSupplyDetails.setAttribute(GCConstants.ORGANIZATION_CODE, sOrganizationCode);
        eleGetSupplyDetails.setAttribute(GCConstants.PRODUCT_CLASS, sProductClass);
        eleGetSupplyDetails.setAttribute(GCConstants.SHIP_NODE, sShipNode);
        eleGetSupplyDetails.setAttribute(GCConstants.UNIT_OF_MEASURE, sUnitOfMeasure);


        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("Input to getSupplyDetails API : " + getSupplyDetailsinDoc.getString());
        }

        YFCDocument getSupplyDetailsTemplate = YFCDocument.getDocumentFor(GCConstants.GET_SUPPLY_DETAILS_TEMPLATE);

        if (sKitCode.equals(GCConstants.BUNDLE)) {
          YFCElement eleExtn = eleItem.getChildElement(GCConstants.EXTN);
          String sExtnSerialCmpt = eleExtn.getAttribute(GCXmlLiterals.EXTN_SERIAL_COMPONENT);
          if (!YFCCommon.isVoid(sExtnSerialCmpt)) {
            eleGetSupplyDetails.setAttribute(GCConstants.ITEM_ID, sExtnSerialCmpt);
          } else {
            GCCommonUtil.invokeService(env, GCConstants.GC_PUBLISH_TO_ATG, inputDoc);
            return inputDoc.getDocument();
          }
        }
        // calling getSupplyDetails to get all the Tag supply and demand available in OMS for this
        // item
        YFCDocument outDoc =
            GCCommonUtil
            .invokeAPI(env, GCConstants.GET_SUPPLY_DETAILS, getSupplyDetailsinDoc, getSupplyDetailsTemplate);

        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("Output from getSupplyDetails :" + outDoc.getString());
        }

        YFCElement eleOutDocItem = outDoc.getDocumentElement();
        YFCElement eleOutDocShipNodes = eleOutDocItem.getChildElement(GCXmlLiterals.SHIP_NODES);
        YFCElement eleOutDocShipNode = eleOutDocShipNodes.getChildElement(GCConstants.SHIP_NODE);

        YFCElement eleOutDocSupplies = eleOutDocShipNode.getChildElement(GCConstants.SUPPLIES);

        YFCNodeList<YFCElement> listSupply = eleOutDocSupplies.getElementsByTagName(GCConstants.SUPPLY);

        for (YFCElement eleNodeSupply : listSupply) {
          String sOutDocSupplyType = eleNodeSupply.getAttribute(GCConstants.SUPPLY_TYPE);

          if (sOutDocSupplyType.equals(GCConstants.ONHAND) || sOutDocSupplyType.equals(GCXmlLiterals.NON_PICKABLE)) {
            YFCNodeList<YFCElement> listSupplyDetails =
                eleNodeSupply.getElementsByTagName(GCXmlLiterals.SUPPLY_DETAILS);

            for (YFCElement eleNodeSupplyDetails : listSupplyDetails) {
              appendTagElement(eleRoot, eleNodeSupplyDetails);
            }
          }
        }
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("inputDoc" + inputDoc.getString());
        }
        GCCommonUtil.invokeService(env, GCConstants.GC_PUBLISH_TO_ATG, inputDoc);
      }
    }
    LOGGER.endTimer("GCInternalRTAMSerialAPI.getSerialAvailability");
    return inputDoc.getDocument();
  }

  /**
   * This method is used to return the Document with all Tag Elements with Ship Node attribute.
   *
   * @param env
   * @param inputDoc
   */
  public Document fetchSerialInfo(YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("GCInternalRTAMSerialAPI.getSerialAvailability");
    LOGGER.verbose("Class: GCInternalRTAMSerialAPI Method: getSerialAvailability BEGIN");
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);

    YFCElement getSupplyDetailsEle = inputDoc.getDocumentElement();
    String sProductClass = getSupplyDetailsEle.getAttribute("ProductClass");
    String sUnitOfMeasure = getSupplyDetailsEle.getAttribute("UnitOfMeasure");
    String sExtnBundleParentID = getSupplyDetailsEle.getAttribute("ExtnBundleParentID");
    List<String> sShipNodesList = new ArrayList<String>();
    // GCSTORE-2021 - if serial list request is for component, ExtnBundleParentID will not be null
    // and the ItemID will be Bundle's Component
    boolean isItemBundleComponent = !YFCCommon.isVoid(sExtnBundleParentID);
    // Creating a New Tags Document
    YFCDocument tagsOutDoc = YFCDocument.createDocument(GCXmlLiterals.TAGS);
    YFCElement eleTags = tagsOutDoc.getDocumentElement();

    YFCDocument getSupplyDetailsTemplate = YFCDocument.getDocumentFor(GCConstants.GET_SUPPLY_DETAILS_TEMPLATE);

    // calling getSupplyDetails to get all the Tag supply and demand available in OMS for this item
    YFCDocument outDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_SUPPLY_DETAILS, inputDoc, getSupplyDetailsTemplate);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Output from getSupplyDetails :" + outDoc.getString());
    }

    YFCElement eleOutDocItem = outDoc.getDocumentElement();
    YFCElement eleOutDocShipNodes = eleOutDocItem.getChildElement(GCXmlLiterals.SHIP_NODES);

    YFCNodeList<YFCElement> listShipNode = eleOutDocShipNodes.getElementsByTagName(GCConstants.SHIP_NODE);

    for (YFCElement eleOutDocShipNode : listShipNode) {
      String sOutDocShipNode = eleOutDocShipNode.getAttribute(GCConstants.SHIP_NODE);

      YFCElement eleOutDocSupplies = eleOutDocShipNode.getChildElement(GCConstants.SUPPLIES);

      YFCNodeList<YFCElement> listSupply = eleOutDocSupplies.getElementsByTagName(GCConstants.SUPPLY);

      for (YFCElement eleNodeSupply : listSupply) {
        String sOutDocSupplyType = eleNodeSupply.getAttribute(GCConstants.SUPPLY_TYPE);

        if (sOutDocSupplyType.equals(GCConstants.ONHAND) || sOutDocSupplyType.equals(GCXmlLiterals.NON_PICKABLE)) {
          YFCNodeList<YFCElement> listSupplyDetails = eleNodeSupply.getElementsByTagName(GCXmlLiterals.SUPPLY_DETAILS);

          for (YFCElement eleNodeSupplyDetails : listSupplyDetails) {
            sShipNodesList =
                appendTagElementWithShipNode(eleNodeSupplyDetails, sOutDocShipNode, eleTags, isItemBundleComponent,sShipNodesList);
          }
        }
      }
    }
    // Fix for GCSTORE-2021
    // Filtering out stores for which sourcing not allowed
    LOGGER.verbose("Going to filterout stores for which sourcing are not allowed");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" before filtering out due to sourcing not allowed. sShipNodesList=" + sShipNodesList.toString());
    }
    sShipNodesList = GCOrderUtil.getStoresAllowedForSourcing(env, sShipNodesList);
    LOGGER.verbose("filtered out stores for which sourcing are not allowed");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" after filtering out due to sourcing not allowed. sShipNodesList=" + sShipNodesList.toString());
    }
    // GCSTORE-2021, if it is Bundle component then before sending serial list to COM, remove the
    // serial for which bundle avaialbility is zero
    if (isItemBundleComponent) {
      // GCSTORE-2021, removing the serial for which Bundle availability is zero through
      // findInventory OOB API call.
      LOGGER.verbose("This is a bundle component so going to call findInventory");

      sShipNodesList =
          callFindInventoryToFilterUnavailableBundle(env, sShipNodesList, sExtnBundleParentID, sProductClass,
              sUnitOfMeasure);
      filterOutShipNodeWithoutAvailability(eleTags, sShipNodesList);

    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("TagsOutDoc" + tagsOutDoc.getString());
    }
    LOGGER.endTimer("GCInternalRTAMSerialAPI.getSerialAvailability");
    return tagsOutDoc.getDocument();
  }

  // GCSTORE-2021 - start
  /**
   * Remove all the ShipNode from the eleTags if ShipNode is not present in sShipNodesList
   *
   * @param eleTags
   * @param sShipNodesList
   */
  private void filterOutShipNodeWithoutAvailability(YFCElement eleTags, List<String> sShipNodesList) {
    LOGGER.beginTimer("GCInternalRTAMSerialAPI.filterOutShipNodeWithoutAvailability");
    LOGGER.verbose("Before filtering out the ShipNode");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("eleTags" + eleTags.getString());
      LOGGER.verbose("sShipNodesList" + sShipNodesList.toString());
    }
    YFCNodeList<YFCElement> tagEleList = eleTags.getElementsByTagName(GCXmlLiterals.TAG);

    for (int i = 0; i < tagEleList.getLength(); i++) {
      YFCElement tagEle = tagEleList.item(i);
      String shipNode = tagEle.getAttribute(GCConstants.SHIP_NODE);
      if (!sShipNodesList.contains(shipNode)) {
        eleTags.removeChild(tagEle);
        i--;
      }
    }
    LOGGER.verbose("After filtering out the ShipNode");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("final eleTags" + eleTags.getString());
      LOGGER.verbose("final sShipNodesList" + sShipNodesList.toString());
    }
    LOGGER.endTimer("GCInternalRTAMSerialAPI.filterOutShipNodeWithoutAvailability");
  }



  /**
   * This method takes shipNode at which serial inventories are availabe for component as well as
   * these shipnodes are allowed for sourcing subsequently this method calls findInventory to filter
   * out those shipnode which does not have bundle availability although serials are available for
   * the component
   *
   * @param sShipNodesList
   * @param sUnitOfMeasure
   * @param sProductClass
   * @param sExtnBundleParentID
   */
  private List<String> callFindInventoryToFilterUnavailableBundle(YFSEnvironment env, List<String> sShipNodesList,
      String sExtnBundleParentID, String sProductClass, String sUnitOfMeasure) {
    LOGGER.beginTimer("GCInternalRTAMSerialAPI.callFindInventoryToFilterUnavailableBundle");
    List<String> shipNodeWithAvailList = new ArrayList<String>();
    YFCDocument indocFindInventory = YFCDocument.createDocument(GCConstants.PROMISE);
    YFCElement elePromise = indocFindInventory.getDocumentElement();
    elePromise.setAttribute(GCConstants.ORGANIZATION_CODE, "GC");
    elePromise.setAttribute("AggregateSupplyOfNonRequestedTag", GCConstants.FLAG_Y);
    elePromise.setAttribute("UseUnplannedInventory", GCConstants.FLAG_N);
    elePromise.setAttribute("AllocationRuleID", "GC_DEFAULT");
    elePromise.setAttribute("MaximumRecords", "5");
    YFCElement elePromiseLines = elePromise.createChild(GCConstants.PROMISE_LINES);
    YFCElement elePromiseLine = elePromiseLines.createChild(GCConstants.PROMISE_LINE);
    elePromiseLine.setAttribute(GCConstants.LINE_ID, "1");
    elePromiseLine.setAttribute(GCConstants.ITEM_ID, sExtnBundleParentID);
    elePromiseLine.setAttribute(GCConstants.UNIT_OF_MEASURE, sUnitOfMeasure);
    elePromiseLine.setAttribute(GCConstants.PRODUCT_CLASS, sProductClass);
    elePromiseLine.setAttribute(GCConstants.REQUIRED_QTY, "1");
    YFCElement eleShipNodes = elePromiseLine.createChild(GCConstants.SHIP_NODES);
    for (int i = 0; i < sShipNodesList.size(); i++) {
      YFCElement eleShipNode = eleShipNodes.createChild(GCConstants.SHIP_NODE);
      eleShipNode.setAttribute(GCConstants.NODE, sShipNodesList.get(i));
    }
    Document tmplStr = GCCommonUtil.getDocumentFromPath(GCConstants.PATH_FOR_FIND_INVENTORY_ATG_INTEG_TEMPLATE);

    YFCDocument findInvTmplDoc = YFCDocument.getDocumentFor(tmplStr);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" Calling findInventory with input=" + findInvTmplDoc);
      LOGGER.verbose("\n and  template=" + GCXMLUtil.getXMLString(tmplStr));
    }
    YFCDocument outDocFindInv =
        GCCommonUtil.invokeAPI(env, GCConstants.FIND_INVENTORY, indocFindInventory, findInvTmplDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("findInventory output=" + outDocFindInv);
    }

    YFCElement eleSuggestedOption = outDocFindInv.getDocumentElement().getChildElement("SuggestedOption");
    YFCElement eleOption = eleSuggestedOption.getChildElement("Option");
    YFCElement eleUnavailableLines = eleSuggestedOption.getChildElement("UnavailableLines");

    if (!YFCCommon.isVoid(eleUnavailableLines)) {
      // if none of ShipNode has availablity remove then don't select any ship node
      LOGGER.verbose("No availability is found across all the provided shipnodes");
    } else if (!YFCCommon.isVoid(eleOption)) {
      // Now check for SuggestedOption/Option element and get if there is any assignment for the
      // ShipNode
      LOGGER.verbose("Iterating through SuggestedOption");
      YFCElement elePromiseLinesOutput = eleOption.getChildElement("PromiseLines");
      YFCElement elePromiseLineOutput = elePromiseLinesOutput.getChildElement("PromiseLine");
      YFCElement eleAssignmentsOutput = elePromiseLineOutput.getChildElement("Assignments");
      YFCElement eleAssignmentOutput = eleAssignmentsOutput.getChildElement("Assignment");

      String sShipNodeOutput = eleAssignmentOutput.getAttribute("ShipNode");
      if (!shipNodeWithAvailList.contains(sShipNodeOutput)) {
        LOGGER.verbose("Adding availablity shipnode=" + sShipNodeOutput);
        shipNodeWithAvailList.add(sShipNodeOutput);
      }
    }
    YFCElement eleOptions = outDocFindInv.getDocumentElement().getChildElement("Options");
    if (!YFCCommon.isVoid(eleOptions)) {
      YFCNodeList<YFCElement> nlOption = eleOptions.getElementsByTagName("Option");
      for (YFCElement eleNonSuggestedOption : nlOption) {
        LOGGER.verbose("Iterating through Option");
        YFCElement elePromiseLinesOutput = eleNonSuggestedOption.getChildElement("PromiseLines");
        YFCElement elePromiseLineOutput = elePromiseLinesOutput.getChildElement("PromiseLine");
        YFCElement eleAssignmentsOutput = elePromiseLineOutput.getChildElement("Assignments");
        YFCElement eleAssignmentOutput = eleAssignmentsOutput.getChildElement("Assignment");
        String sShipNodeOutput = eleAssignmentOutput.getAttribute("ShipNode");
        if (!shipNodeWithAvailList.contains(sShipNodeOutput)) {
          shipNodeWithAvailList.add(sShipNodeOutput);
        }
      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Udpate list of ShipNode=" + shipNodeWithAvailList);
    }
    LOGGER.endTimer("GCInternalRTAMSerialAPI.callFindInventoryToFilterUnavailableBundle");
    return shipNodeWithAvailList;
  }

  // GCSTORE-2021 End
  /**
   * This method is used to create the Tags and Tag elements and if the Total Availability is
   * positive then append the Tags and Tag element into the inputDoc Document.
   *
   * @param eleRoot
   * @param eleNodeSupplyDetails
   * @param eleTags
   */
  private void appendTagElement(YFCElement eleRoot, YFCElement eleNodeSupplyDetails) {
    LOGGER.beginTimer("GCInternalRTAMSerialAPI.appendTagElement");
    LOGGER.verbose("Class: GCInternalRTAMSerialAPI Method: appendTagElement BEGIN");

    YFCElement eleNodeTag = eleNodeSupplyDetails.getChildElement(GCXmlLiterals.TAG);

    if (!YFCCommon.isVoid(eleNodeTag)) {
      Double onhandSupply = eleNodeTag.getDoubleAttribute(GCXmlLiterals.TOTAL_ONHAND_SUPPLY);
      LOGGER.verbose("Total OnHand Supply" + onhandSupply);

      Double totalDemand = eleNodeTag.getDoubleAttribute(GCXmlLiterals.TOTAL_DEMAND);
      LOGGER.verbose("Total Demand" + totalDemand);

      Double totalAvailability = onhandSupply - totalDemand;
      LOGGER.verbose("Total Availability" + totalAvailability);

      if (totalAvailability > 0) {

        String sBatchNo = eleNodeTag.getAttribute(GCXmlLiterals.BATCH_NO);
        String sLotAttr = eleNodeTag.getAttribute(GCXmlLiterals.LOT_ATTR1);

        YFCElement eleTags = eleRoot.getChildElement(GCXmlLiterals.TAGS, true);
        YFCElement eleTag = eleTags.createChild(GCXmlLiterals.TAG);

        eleTag.setAttribute(GCXmlLiterals.BATCH_NO, sBatchNo);
        eleTag.setAttribute(GCXmlLiterals.LOT_ATTR1, sLotAttr);

      }
    }
    LOGGER.endTimer("GCInternalRTAMSerialAPI.appendTagElement");
  }

  /**
   * This method is used to append all Tag elements with the Ship Node attribute in the tagsOutDoc.
   *
   * @param eleNodeSupplyDetails
   * @param sOutDocShipNode
   * @param eleTags
   * @param isItemBundleComponent
   */

  private List<String> appendTagElementWithShipNode(YFCElement eleNodeSupplyDetails, String sOutDocShipNode,
      YFCElement eleTags, boolean isItemBundleComponent, List<String> shipNodeWithSerialAvail) {
    LOGGER.beginTimer("GCInternalRTAMSerialAPI.appendTagElementWithShipNode");
    LOGGER.verbose("Class: GCInternalRTAMSerialAPI Method: appendTagElementWithShipNode BEGIN");

    YFCElement eleNodeTag = eleNodeSupplyDetails.getChildElement(GCXmlLiterals.TAG);
     
    if (!YFCCommon.isVoid(eleNodeTag)) {
      Double onhandSupply = eleNodeTag.getDoubleAttribute(GCXmlLiterals.TOTAL_ONHAND_SUPPLY);
      LOGGER.verbose("Total OnHand Supply" + onhandSupply);

      Double totalDemand = eleNodeTag.getDoubleAttribute(GCXmlLiterals.TOTAL_DEMAND);
      LOGGER.verbose("Total Demand" + totalDemand);

      Double totalAvailability = onhandSupply - totalDemand;
      LOGGER.verbose("Total Availability" + totalAvailability);
      if (totalAvailability > 0) {
        String sBatchNo = eleNodeTag.getAttribute(GCXmlLiterals.BATCH_NO);
        String sLotAttr = eleNodeTag.getAttribute(GCXmlLiterals.LOT_ATTR1);
        YFCElement eleTag = eleTags.createChild(GCXmlLiterals.TAG);
        eleTag.setAttribute(GCXmlLiterals.BATCH_NO, sBatchNo);
        eleTag.setAttribute(GCXmlLiterals.LOT_ATTR1, sLotAttr);
        eleTag.setAttribute(GCConstants.SHIP_NODE, sOutDocShipNode);
        if (isItemBundleComponent && !shipNodeWithSerialAvail.contains(sOutDocShipNode)) {
          shipNodeWithSerialAvail.add(sOutDocShipNode);
        }
      }
    }
    LOGGER.endTimer("GCInternalRTAMSerialAPI.appendTagElementWithShipNode");
    return shipNodeWithSerialAvail;
  }

  @Override
  public void setProperties(Properties prop) {
	  
  }
}
