/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 * *#################################################################################################################################################################
 *	        OBJECTIVE: This class is used to call loadInventoryMistmatch API 
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
 *            1.0               11/02/2014       Gowda, Naveen		        OMS-226: As an OMS, I want to receive web inventory levels so that I can promise inventory 
 *                                                                          to customers for sales�
 *#################################################################################################################################################################
 */
package com.gc.api.inventory;

import java.util.Properties;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:naveen.s@expicient.com">Naveen</a>
 * 
 */
public class GCLoadInventoryMismatch implements YIFCustomApi {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCLoadInventoryMismatch.class.getName());

    /**
     * 
     * Description of syncInventory: This method is used to call
     * loadInventoryMismatch API.
     * 
     * @param env
     *            YFSEnvironment
     * @param inDoc
     *            Input XML Document
     * @throws Exception
     *             Exception
     * 
     */
    public void loadInventory(YFSEnvironment env, Document inDoc)
	    throws GCException {

	LOGGER.debug("Class: GCLoadInventoryMismatch Method: loadInventory BEGIN");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug("Incoming Document" + GCXMLUtil.getXMLString(inDoc));
	}
	try {
	    Element eleRoot = inDoc.getDocumentElement();
	    String strRoot = eleRoot.getNodeName();
	    if ("EOF".equals(strRoot)) {
		// Fetching attributes from EOF message required for API call.
		String shipNode = eleRoot.getAttribute(GCConstants.SHIP_NODE);
		String yantraGrpMsgID = eleRoot
			.getAttribute(GCConstants.YANTRA_GROUP_MESSAGE_ID);

		// Preparing input document for API call.
		Document inputDoc = GCXMLUtil
			.createDocument(GCConstants.INVENTORY);
		Element eleInputDocRoot = inputDoc.getDocumentElement();

		eleInputDocRoot.setAttribute(GCConstants.APPLY_DIFFERENCES,
			GCConstants.FLAG_Y);
		// Setting mandatory attributes for API call.
		eleInputDocRoot.setAttribute(GCConstants.SHIP_NODE, shipNode);
		eleInputDocRoot.setAttribute(
			GCConstants.YANTRA_GROUP_MESSAGE_ID, yantraGrpMsgID);
		eleInputDocRoot.setAttribute("CompleteInventoryFlag",
			GCConstants.FLAG_Y);
		eleInputDocRoot.setAttribute("SynchNonOnHandInventory",
			GCConstants.FLAG_N);
		GCCommonUtil.invokeAPI(env,
			GCConstants.API_SYNC_LOADED_INVENTORY, inputDoc);
	    } else {
		eleRoot.setAttribute(GCConstants.VALIDATE_ITEMS,
			GCConstants.FLAG_Y);

		NodeList nodeListItems = GCXMLUtil.getNodeListByXpath(inDoc,
			GCConstants.ITEMS_ITEM_XPATH);
		LOGGER.debug("nodeListItems Length="
			+ nodeListItems.getLength());
		for (int i = 0; i < nodeListItems.getLength(); i++) {
		    Element eleItem = (Element) nodeListItems.item(i);

		    GCAdjustInventory obj = new GCAdjustInventory();
		    Document docGtItmDtOt = obj.fetchingItemDetails(env,
			    eleItem);

		    // Fetching Kit Code and Extn Set Code
		    String sKitCode = GCXMLUtil
			    .getAttributeFromXPath(
				    docGtItmDtOt,
				    GCConstants.ITEMLIST_ITEM_PRIMARYINFO_KIT_CODE_XPATH);
		    String sSetCode = GCXMLUtil.getAttributeFromXPath(
			    docGtItmDtOt,
			    GCConstants.ITEMLIST_ITEM_EXTN_SET_CODE_XPATH);
		    // If Item is a bundle them exploding components.
		    if ((GCConstants.BUNDLE).equalsIgnoreCase(sKitCode)) {
			getComponentDetails(docGtItmDtOt, eleItem, inDoc,
				eleRoot, sSetCode);

		    }
		}
		if (LOGGER.isDebugEnabled()) {
		    LOGGER.debug("In Doc to API call"
			    + GCXMLUtil.getXMLString(inDoc));
		}
		GCCommonUtil.invokeAPI(env,
			GCConstants.API_LOAD_INVENTORY_MISMATCH, inDoc);
		LOGGER.debug("Class: GCLoadInventoryMismatch Method: loadInventory END");

	    }
	} catch (Exception e) {
	    LOGGER.error("Exception in GCLoadInventoryMismatch:loadInventory ",
		    e);
	    throw new GCException(e);
	}
    }

    /**
     * 
     * Description of getComponentDetails - This method is used to replace
     * bundle item in the input xml with it components before the API call.
     * 
     * @param docGtItmDtOt
     *            Output document of getItemList API call.
     * @param eleItem
     *            Item Element
     * @param inDoc
     *            Input XML Document.
     * @param eleRoot
     *            Root element of Input Document.
     * @param sSetCode
     *            ExtnSetCode value.
     * @throws ParserConfigurationException
     *             ParserConfigurationException
     * @throws TransformerException
     *             TransformerException
     * 
     */
    public void getComponentDetails(Document docGtItmDtOt, Element eleItem,
	    Document inDoc, Element eleRoot, String sSetCode)
	    throws GCException {

	LOGGER.debug("Class: GCLoadInventoryMismatch Method: getComponentDetails BEGIN");
	try {
	    NodeList nodeListComponents = GCXMLUtil.getNodeListByXpath(
		    docGtItmDtOt,
		    GCConstants.ITEMLIST_ITEM_COMPONENTS_COMPONENT_XPATH);
	    LOGGER.debug("nodeListComponents Length="
		    + nodeListComponents.getLength());
	    Boolean booIsTrue = true;

	    // Looping through component items.
	    for (int j = 0; j < nodeListComponents.getLength(); j++) {

		Element eleCmpnt = (Element) nodeListComponents.item(j);

		Element eleCompItem = inDoc.createElement(GCConstants.ITEM);
		Element eleCompSupplies = inDoc
			.createElement(GCConstants.SUPPLIES);

		// Copying attributes of parent item to component item element
		GCXMLUtil.copyAttributes(eleItem, eleCompItem);
		if (booIsTrue) {
		    // Removing bundle item from the XML.
		    GCXMLUtil.removeChild(eleRoot, eleItem);
		    booIsTrue = false;
		}
		String strComItmID = eleCmpnt
			.getAttribute(GCConstants.COMPONENT_ITEM_ID);
		String strKitQty = eleCmpnt
			.getAttribute(GCConstants.KIT_QUANTITY);

		Document docItem = GCXMLUtil.getDocumentFromElement(eleItem);
		if (LOGGER.isDebugEnabled()) {
		    LOGGER.debug("Item Document"
			    + GCXMLUtil.getXMLString(docItem));
		}
		NodeList nLSupply = GCXMLUtil.getNodeListByXpath(docItem,
			"Item/Supplies/Supply");
		for (int k = 0; k < nLSupply.getLength(); k++) {

		    Element eleCompSupply = inDoc
			    .createElement(GCConstants.SUPPLY);
		    LOGGER.debug("Supplies Lenght" + nLSupply.getLength());
		    Element eleSupply = (Element) nLSupply.item(k);
		    String sQty = eleSupply.getAttribute(GCConstants.QUANTITY);
		    String sAvailType = eleSupply
			    .getAttribute(GCConstants.AVAILABILITY_TYPE);
		    String sSupplyType = eleSupply
			    .getAttribute(GCConstants.SUPPLY_TYPE);

		    // Component ratio is multiplied against the quantity.
		    Double dFinalQty = Double.parseDouble(strKitQty)
			    * Double.parseDouble(sQty);
		    String sfinalQty = dFinalQty.toString();

		    eleCompSupply.setAttribute(GCConstants.QUANTITY, sfinalQty);
		    eleCompSupply.setAttribute(GCConstants.AVAILABILITY_TYPE,
			    sAvailType);
		    eleCompSupply.setAttribute(GCConstants.SUPPLY_TYPE,
			    sSupplyType);
		    eleCompItem.setAttribute(GCConstants.ITEM_ID, strComItmID);

		    eleCompSupplies.appendChild(eleCompSupply);
		}
		// If ExtnSetCode is Y then component's product class is changed
		// to
		// SET.
		if ((GCConstants.INT_ONE).equalsIgnoreCase(sSetCode)) {
		    eleCompItem.setAttribute(GCConstants.PRODUCT_CLASS,
			    GCConstants.SET);
		}

		eleCompItem.appendChild(eleCompSupplies);
		eleRoot.appendChild(eleCompItem);

		LOGGER.debug("Class: GCLoadInventoryMismatch Method: getComponentDetails END");
	    }
	} catch (Exception e) {
	    throw new GCException(e);
	}
    }

    @Override
    public void setProperties(Properties arg0) {

    }

}
