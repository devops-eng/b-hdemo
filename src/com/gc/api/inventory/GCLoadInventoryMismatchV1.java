/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 * *#################################################################################################################################################################
 *	        OBJECTIVE: This class is used to call loadInventoryMistmatch API
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
 *            1.0               11/02/2014       Gowda, Naveen		        OMS-226: As an OMS, I want to receive web inventory levels so that I can promise inventory
 *                                                                          to customers for sales�
 *#################################################################################################################################################################
 */
package com.gc.api.inventory;

import java.util.Properties;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:naveen.s@expicient.com">Naveen</a>
 *
 */
public class GCLoadInventoryMismatchV1 implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCLoadInventoryMismatchV1.class);

  /**
   *
   * Description of syncInventory: This method is used to call
   * loadInventoryMismatch API.
   *
   * @param env
   *            YFSEnvironment
   * @param inDoc
   *            Input XML Document
   * @throws Exception
   *             Exception
   *
   */
  public void loadInventory(YFSEnvironment env, Document inDoc)
      throws GCException {

    LOGGER
    .debug("Class: GCLoadInventoryMismatch Method: loadInventory BEGIN");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Incoming Document" + GCXMLUtil.getXMLString(inDoc));
    }
    try {
      Element eleRoot = inDoc.getDocumentElement();
      String strRoot = eleRoot.getNodeName();
      String yantraGrpMsgID = "";
      String sShipNode = "";
      if ("Inventory".equals(strRoot)) {
        if (LOGGER.isDebugEnabled()) {
          LOGGER
          .debug("*******SYNC LOADED INVENTORY INPUT RECEIVED******"
              + GCXMLUtil.getXMLString(inDoc));
        }
        GCCommonUtil.invokeAPI(env,
            GCConstants.API_SYNC_LOADED_INVENTORY, inDoc);
      } else {
        eleRoot.setAttribute(GCConstants.VALIDATE_ITEMS,
            GCConstants.FLAG_N);
        yantraGrpMsgID = eleRoot.getAttribute("YantraMessageGroupID");
        sShipNode = eleRoot.getAttribute("ShipNode");

        NodeList nodeListItems = GCXMLUtil.getNodeListByXpath(inDoc,
            GCConstants.ITEMS_ITEM_XPATH);
        LOGGER.debug("nodeListItems Length="
            + nodeListItems.getLength());
        for (int i = 0; i < nodeListItems.getLength(); i++) {
          Element eleItem = (Element) nodeListItems.item(i);

          GCAdjustInventory obj = new GCAdjustInventory();
          Document docGtItmDtOt = obj.fetchingItemDetails(env,
              eleItem);

          // Fetching Kit Code and Extn Set Code
          String sSetCode = GCXMLUtil.getAttributeFromXPath(
              docGtItmDtOt,
              GCConstants.ITEMLIST_ITEM_EXTN_SET_CODE_XPATH);
          // If Item is a bundle them exploding components.

          if ((GCConstants.INT_ONE).equalsIgnoreCase(sSetCode)) {
            getComponentDetails(env, docGtItmDtOt, eleItem,
                eleRoot, yantraGrpMsgID, sShipNode);

          }
        }
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("*******LOAD INVENTORY MISMATCH INPUT*******"
              + GCXMLUtil.getXMLString(inDoc));
        }
        GCCommonUtil.invokeAPI(env,
            GCConstants.API_LOAD_INVENTORY_MISMATCH, inDoc);

      }
    } catch (Exception e) {
      LOGGER.debug("Exception in GCLoadInventoryMismatch:loadInventory",
          e);
      throw new GCException(e);
    }
    LOGGER
    .debug("Class: GCLoadInventoryMismatch Method: loadInventory END");
  }

  /**
   *
   * Description of getComponentDetails - This method is used to replace
   * bundle item in the input xml with it components before the API call.
   *
   * @param docGtItmDtOt
   *            Output document of getItemList API call.
   * @param eleItem
   *            Item Element
   * @param inDoc
   *            Input XML Document.
   * @param eleRoot
   *            Root element of Input Document.
   * @param sSetCode
   *            ExtnSetCode value.
   * @throws ParserConfigurationException
   *             ParserConfigurationException
   * @throws TransformerException
   *             TransformerException
   *
   */
  private void getComponentDetails(YFSEnvironment env, Document docGtItmDtOt,
      Element eleItem, Element eleRoot, String yantraMsgGroupID,
      String sShipNode) throws GCException {

    LOGGER
    .debug("Class: GCLoadInventoryMismatch Method: getComponentDetails BEGIN");
    try {

      NodeList nodeListComponents = GCXMLUtil.getNodeListByXpath(
          docGtItmDtOt,
          GCConstants.ITEMLIST_ITEM_COMPONENTS_COMPONENT_XPATH);
      LOGGER.debug("nodeListComponents Length="
          + nodeListComponents.getLength());
      Boolean booIsTrue = true;

      // Looping through component items.
      for (int j = 0; j < nodeListComponents.getLength(); j++) {
        Element eleCmpnt = (Element) nodeListComponents.item(j);
        if (booIsTrue) {
          // Removing bundle item from the XML.
          GCXMLUtil.removeChild(eleRoot, eleItem);
          booIsTrue = false;
        }
        String strComItmID = eleCmpnt
            .getAttribute(GCConstants.COMPONENT_ITEM_ID);
        String strKitQty = eleCmpnt
            .getAttribute(GCConstants.KIT_QUANTITY);

        Document docItem = GCXMLUtil.getDocumentFromElement(eleItem);
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("Item Document"
              + GCXMLUtil.getXMLString(docItem));
        }
        NodeList nLSupply = GCXMLUtil.getNodeListByXpath(docItem,
            "Item/Supplies/Supply");
        for (int k = 0; k < nLSupply.getLength(); k++) {
          LOGGER.debug("Supplies Length" + nLSupply.getLength());
          Document compItemsDoc = GCXMLUtil
              .createDocument("GCCompItemsForInvSync");
          Element eleCompItmsForInvSyn = compItemsDoc
              .getDocumentElement();
          Element eleSupply = (Element) nLSupply.item(k);
          String sQty = eleSupply.getAttribute(GCConstants.QUANTITY);
          String sAvailType = eleSupply
              .getAttribute(GCConstants.AVAILABILITY_TYPE);
          String sSupplyType = eleSupply
              .getAttribute(GCConstants.SUPPLY_TYPE);
          // Component ratio is multiplied against the quantity.
          Double dFinalQty = Double.parseDouble(strKitQty)
              * Double.parseDouble(sQty);
          String sfinalQty = dFinalQty.toString();
          eleCompItmsForInvSyn.setAttribute(
              GCConstants.AVAILABILITY_TYPE, sAvailType);
          eleCompItmsForInvSyn
          .setAttribute(
              GCConstants.INVENTORY_ORGANIZATION,
              eleItem
              .getAttribute(GCConstants.INVENTORY_ORGANIZATION));
          eleCompItmsForInvSyn.setAttribute(GCConstants.ITEM_ID,
              strComItmID);
          eleCompItmsForInvSyn.setAttribute(
              GCConstants.PRODUCT_CLASS, GCConstants.SET);
          eleCompItmsForInvSyn.setAttribute(GCConstants.SUPPLY_TYPE,
              sSupplyType);
          eleCompItmsForInvSyn.setAttribute(
              GCConstants.UNIT_OF_MEASURE, eleItem
              .getAttribute("UnitOfMeasure"));
          eleCompItmsForInvSyn.setAttribute("ShipNode", sShipNode);
          eleCompItmsForInvSyn.setAttribute("YantraMessageGroupID",
              yantraMsgGroupID);
          eleCompItmsForInvSyn.setAttribute(GCConstants.QUANTITY,
              sfinalQty);
          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("*****component item input *******"
                + GCXMLUtil.getXMLString(compItemsDoc));
          }
          GCCommonUtil.invokeService(env, "CreateCompItemService",
              compItemsDoc);
        }

      }

    } catch (Exception e) {
      throw new GCException(e);
    }
    LOGGER
    .debug("Class: GCLoadInventoryMismatch Method: getComponentDetails END");
  }

  @Override
  public void setProperties(Properties arg0) {

  }

}
