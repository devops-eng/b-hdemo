/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################################
 *          OBJECTIVE: GCSTORE-73 - POS Inventory Sync. This class is used to receive store inventory messages from POS
 * and update in OMS
 *###################################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *################################################################################################################################################################################
             1.0                               Kumar, Gunjan                  Shipment
             1.1            25/03/2015       Rngarajan, Shwetha         Code changes for handling Supply Sync for Platinum Items.
             1.2            10/04/2017        Anuj Srivastava           Code changes for handling duplicate supplies within OMS 
 *##################################################################################################################################################################################
 */
package com.gc.api.inventory;



import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * GCSTORE-73 - POS Inventory Sync. This class is used to receive store inventory messages from POS
 * and update in OMS
 *
 * This class gets inventory update messages from POS which is current supply picture of the item at
 * a store. OMS sync those supply into OMS including tagged supply. If tagged inventory of inventory
 * message is not found then those inventory supply is adjusted as non-tagged inventory in OMS
 *
 * @author gunjankumar
 *
 */
public class GCLoadPOSInventoryMismatchAPI implements YIFCustomApi {

	// initialize logger
	private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCLoadPOSInventoryMismatchAPI.class.getName());

	/**
	 * This method receives inventory update messages from POS, pocess tag supplies and call OOB API
	 * loadInventoryMismatch to load inventory in YFS_INVENTORY_SUPPLY_TEMP table of OMS
	 *
	 * @param env - Environment variable
	 * @param inputDoc - POS inventory message
	 * @return inputDoc - POS inventory message
	 * @throws GCException
	 * @throws SAXException 
	 * @throws IOException 
	 * @throws ParserConfigurationException 
	 * @throws SQLException
	 */
	public Document loadPOSInventoryUpdate(YFSEnvironment env, Document inputDoc) throws GCException, ParserConfigurationException, IOException, SAXException {

		LOGGER.beginTimer("GCLoadPOSInventoryMismatchAPI.loadPOSInventoryUpdate");
		LOGGER.verbose("Class: GCLoadPOSInventoryMismatchAPI Method: loadPOSInventoryUpdate BEGIN");

		Document cloneDoc = (Document) inputDoc.cloneNode(true);

		YFCDocument yfcInputDoc = YFCDocument.getDocumentFor(cloneDoc);
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("\nyfcInputDoc CopyDoc" + YFCDocument.getDocumentFor(yfcInputDoc.getDocument()));
		}

		YFCElement rootElement = yfcInputDoc.getDocumentElement();
		String rootStr = rootElement.getTagName();

		if (!YFCCommon.equalsIgnoreCase("EOF", rootStr)) {
			
			
			processTagSupplies(env, yfcInputDoc);

			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("\nloadInventoryMismatch API Input Doc" + YFCDocument.getDocumentFor(yfcInputDoc.getDocument()));
			}
			
			Document loadInvMismatch = GCCommonUtil.invokeAPI(env, GCConstants.API_LOAD_INVENTORY_MISMATCH, yfcInputDoc.getDocument());
			
			GCCommonUtil.invokeService(env, GCConstants.GC_POST_INV_NODE_CONTRL_MSG, yfcInputDoc.getDocument());
		}
		LOGGER.verbose("Exiting GCLoadPOSInventoryMismatchAPI.loadPOSInventoryUpdate() method");
		LOGGER.endTimer("GCLoadPOSInventoryMismatchAPI.loadPOSInventoryUpdate");
		return inputDoc;
	}


	
	//5130 Start 2
	/**
	 * This method processes the tag and non-tag supplies of the Product class which is not existing in POS but in OMS only and does not come in POS inventory messages. 
	 * If the supply with such non-existing product class exist in oms then it zero out such supplies including the Tag supplies. 
	 *
	 * @param env Environment variable
	 * @param posItemEle Item element 
	 * @param getItemListOpForOMSItemID GetItemList output for an item
	 * @param yfcInputDoc Cloned input inventory message
	 * @throws GCException
	 * @throws SAXException 
	 * @throws IOException 
	 * @throws ParserConfigurationException 
	 * @throws SQLException
	 */
	private void validateProductClass(YFSEnvironment env,YFCElement posItemEle, YFCDocument yfcInputDoc, YFCDocument getItemListOpForOMSItemID,
			YFCElement omsItemEleForOMSItemID) throws GCException, ParserConfigurationException, IOException, SAXException {
		
		
		LOGGER.beginTimer("GCLoadPOSInventoryMismatchAPI.validateProductClass");
		LOGGER.verbose("Class: GCLoadPOSInventoryMismatchAPI Method: validateProductClass BEGIN");

		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("\nvalidateProductClass method Input Doc" + YFCDocument.getDocumentFor(yfcInputDoc.getDocument()));		
			LOGGER.verbose("\nvalidateProductClass method Item Element" + omsItemEleForOMSItemID);	
		}
		
		
			
		YFCElement posItemsEle = yfcInputDoc.getDocumentElement();
		String shipNode = posItemsEle.getAttribute(GCConstants.SHIP_NODE);
		
		
			String sProductClassIp = posItemEle.getAttribute(GCConstants.PRODUCT_CLASS);
			
			//To fetch OMS_Item_id from POS_Input, fetching outDoc of CallGetItemList
			 //YFCElement eleGetItemListOp = getItemListOpForOMSItemID.getDocumentElement();
			 //YFCElement omsItemEleForOMSItemID = filterDuplicateItems(env, eleGetItemListOp);
   			// YFCElement omsItemEleForOMSItemID = eleGetItemListOp.getChildElement(GCConstants.ITEM);
   			 String sOmsItemID = omsItemEleForOMSItemID.getAttribute(GCConstants.ITEM_ID);

   			 
   			YFCElement InvParametersInItemEle = omsItemEleForOMSItemID.getChildElement(GCConstants.INVENTORY_PARAMETERS);
			String IsTagControlled = InvParametersInItemEle.getAttribute(GCConstants.TAG_CONTROLLED);
			
			YFCDocument getInventoryItemListIp = YFCDocument.createDocument(GCConstants.INVENTORY_ITEM);
			YFCElement eleRootInvItemList = getInventoryItemListIp.getDocumentElement();
			
			eleRootInvItemList.setAttribute(GCConstants.ITEM_ID, sOmsItemID);
			eleRootInvItemList.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.INV_ORGANIZATION_CODE);
			eleRootInvItemList.setAttribute(GCConstants.INVENTORY_ORG_CODE, GCConstants.INVENTORY_ORG_FOR_GC);
			eleRootInvItemList.setAttribute(GCConstants.SHIP_NODE, shipNode);
			
			  YFCDocument getInvItemLstTmpl =
				          YFCDocument.getDocumentFor(GCXMLUtil.getDocument(GCConstants.GET_INVENTORY_ITEM_LIST_TEMP));
				  
			
			YFCDocument getInventoryItemListOp =
					GCCommonUtil.invokeAPI(env, GCConstants.GET_INVENTORY_ITEM_LIST, getInventoryItemListIp, getInvItemLstTmpl);
			
			YFCElement eleGetInventoryItemListOp = getInventoryItemListOp.getDocumentElement();			
			YFCNodeList<YFCElement> nInventoryItem = eleGetInventoryItemListOp.getElementsByTagName(GCConstants.INVENTORY_ITEM);
			
			
			//Iterating over getInventoryItem List for Inventory item element
	          for(int itemCount1 = 0; itemCount1< nInventoryItem.getLength(); itemCount1++ )
	          {	              
	        		YFCElement eleOmsInvItem = nInventoryItem.item(itemCount1);	        		
	        	    String sProductClassOp = eleOmsInvItem.getAttribute(GCConstants.PRODUCT_CLASS);
	        			
		    		//Checking if POS Input product class is not there in OMS(get inventory item list)		     	
					if(!sProductClassOp.equalsIgnoreCase(sProductClassIp)) {
	        			LOGGER.verbose("IsTagControlled?" +IsTagControlled);

					// Fetch tag inventory details from OMS and adjust in Inventory message, only if it is some
					// time or always tag controlled
					
					//try with TagControlled flag for Sometime tag controlled also (mixed- non-tag + tag)
					if (YFCUtils.equals("S", IsTagControlled)) {
												                                                            
						YFCElement eleNewItemZeroQty = posItemsEle.createChild(GCConstants.ITEM);
						eleNewItemZeroQty.setAttribute(GCConstants.INVENTORY_ORG_CODE, GCConstants.INV_ORGANIZATION_CODE);
						eleNewItemZeroQty.setAttribute(GCConstants.ITEM_ID, sOmsItemID);
						//Setting the non exisiting product class which is not present in POS feed but in OMS.
						eleNewItemZeroQty.setAttribute(GCConstants.PRODUCT_CLASS, sProductClassOp);
						eleNewItemZeroQty.setAttribute(GCConstants.UOM, posItemEle.getAttribute(GCConstants.UOM));
						YFCElement eleNewSupplies = eleNewItemZeroQty.createChild(GCConstants.SUPPLIES);
						
						// to set non-tagged supply Zero along with Tagged inventory in same Item element in output						
						
						YFCDocument getInvSupplyOpDoc= callGetItemInventorySupplyForNonTaggedSupply(env, shipNode, sOmsItemID,sProductClassOp);
					
						YFCElement eleRootGetInvSupplyOp = getInvSupplyOpDoc.getDocumentElement();
						YFCElement outSuppliesEle = eleRootGetInvSupplyOp.getChildElement(GCConstants.SUPPLIES);
		     			YFCNodeList<YFCElement> invSupplyOpList = outSuppliesEle.getElementsByTagName(GCXmlLiterals.INVENTORY_SUPPLY);
						
		     			for (YFCElement invSupplyEle : invSupplyOpList) {
		     				YFCElement eleOutTag = invSupplyEle.getChildElement(GCXmlLiterals.TAG);
		     				
		     				//if (YFCCommon.isVoid(eleOutTag) && omsSupplyQty > 0.0) {

								YFCElement eleSupplyNew = eleNewSupplies.createChild(GCConstants.SUPPLY);
								eleSupplyNew.setAttribute(GCConstants.AVAILABILITY_TYPE, GCConstants.TRACK_VAL);
								eleSupplyNew.setAttribute(GCConstants.SUPPLY_TYPE, GCConstants.SUPPLY_TYPE_ONHAND);
								eleSupplyNew.setAttribute(GCConstants.QUANTITY, GCConstants.ZERO);
		     					
								if(!YFCCommon.isVoid(eleOutTag)){
																		
									    String sBatchNo = eleOutTag.getAttribute(GCXmlLiterals.BATCH_NO);
									    String sLotNumber = eleOutTag.getAttribute(GCXmlLiterals.LOT_ATTR1);
									    YFCElement newTagEle = eleSupplyNew.createChild(GCXmlLiterals.TAG);
										newTagEle.setAttribute(GCXmlLiterals.BATCH_NO, sBatchNo);
										newTagEle.setAttribute(GCXmlLiterals.LOT_ATTR1, sLotNumber);								    
								}
		     			}											
						
					} else {
						
						//Non-tag controlled
								LOGGER.verbose("Regular Item without any tag inventory");
								YFCElement eleNewItemZeroQty = posItemsEle.createChild(GCConstants.ITEM);
								eleNewItemZeroQty.setAttribute(GCConstants.INVENTORY_ORG_CODE, GCConstants.INV_ORGANIZATION_CODE);
							    eleNewItemZeroQty.setAttribute(GCConstants.ITEM_ID, sOmsItemID);
							  //Setting the non exisiting product class which is not present in POS feed but in OMS.
							    eleNewItemZeroQty.setAttribute(GCConstants.PRODUCT_CLASS, sProductClassOp);
								eleNewItemZeroQty.setAttribute(GCConstants.UOM, posItemEle.getAttribute(GCConstants.UOM));
		                          
								YFCElement eleNewSupplies = eleNewItemZeroQty.createChild(GCConstants.SUPPLIES);
								YFCElement eleSupplyNew = eleNewSupplies.createChild(GCConstants.SUPPLY);
								eleSupplyNew.setAttribute(GCConstants.AVAILABILITY_TYPE, GCConstants.TRACK_VAL);
								eleSupplyNew.setAttribute(GCConstants.SUPPLY_TYPE, GCConstants.SUPPLY_TYPE_ONHAND);
								eleSupplyNew.setAttribute(GCConstants.QUANTITY, GCConstants.ZERO);
	                          
					}
					
	        	 }
	        	 // If the product class is same 
	        	 else 
	        	 {	        		
						if (YFCUtils.equals("S", IsTagControlled)) {
							 
							//passing ShipNode and complete item element from POS feed input
							Map<String, String> omsTagNoLotNoMap = getListOfExistingTagsInOMSWithProductClass(env, shipNode,sOmsItemID, sProductClassOp);
							LOGGER.verbose("Map formed :" +omsTagNoLotNoMap.toString());
							//************Map formed with batchNo(Tag no)  and Lot no. from OMS output ++++++++++++++++++++ 
							//iterating over POS Input
							YFCNodeList<YFCElement> posSuppliesNodeList = posItemEle.getElementsByTagName(GCConstants.SUPPLIES);
							YFCElement posSuppliesEle = posSuppliesNodeList.item(0);
							YFCNodeList<YFCElement> posSupplyNodeList = posSuppliesEle.getElementsByTagName(GCConstants.SUPPLY);
							int posSupplyNListSize = posSupplyNodeList.getLength();
	                        							
							//This method is only removing the Tag attributes from POS input feed which are there in the map(getInventorySupply_output)
							//but different tag will not be removed from Map
							for (int posSupplyCount = 0; posSupplyCount < posSupplyNListSize; posSupplyCount++) {
								
								YFCElement posSupplyEle = posSupplyNodeList.item(posSupplyCount);
								LOGGER.verbose("Supply element fetched :" +posSupplyEle.toString());
								//Tag of supply
								YFCElement posSupplyTagEle = posSupplyEle.getChildElement(GCXmlLiterals.TAG);
								LOGGER.verbose("Tag element :" +posSupplyTagEle);

								//remove BatchNo from map which exists in POS Input as it is already going to updated
								// through this Input message so OMS only TAG messages should updated.
								//Note- not removing different tag entry from map
								if(!YFCCommon.isVoid(posSupplyTagEle)&&omsTagNoLotNoMap.containsKey(posSupplyTagEle.getAttribute(GCXmlLiterals.BATCH_NO))) {
									String batchNoInMap = posSupplyTagEle.getAttribute(GCXmlLiterals.BATCH_NO);
									omsTagNoLotNoMap.remove(batchNoInMap);
									LOGGER.verbose("Map after removing map entry" +omsTagNoLotNoMap.toString());

								}
							}
							
							// Iterate through Map to stamp Supply Quantity as 0 for BatchNo which exist in OMS but
							// not present in ip XML, as input xml's batch already removed in above method.
							for (Map.Entry<String, String> entry : omsTagNoLotNoMap.entrySet()) {								
								String batchNoInMap = entry.getKey();
								String lotAttr1 = entry.getValue();
								LOGGER.verbose("batchNoInMap: " + batchNoInMap + "\t" + "lotNoInMap: " + lotAttr1);
								
								YFCElement eleSupplyNew = posSuppliesEle.createChild(GCConstants.SUPPLY);
								eleSupplyNew.setAttribute(GCConstants.AVAILABILITY_TYPE, GCConstants.TRACK_VAL);
								eleSupplyNew.setAttribute(GCConstants.SUPPLY_TYPE, GCConstants.SUPPLY_TYPE_ONHAND);
								eleSupplyNew.setAttribute(GCConstants.QUANTITY, GCConstants.ZERO);
								YFCElement newTagEle = eleSupplyNew.createChild(GCXmlLiterals.TAG);
								newTagEle.setAttribute(GCXmlLiterals.BATCH_NO, batchNoInMap);
								newTagEle.setAttribute(GCXmlLiterals.LOT_ATTR1, lotAttr1);
								if (LOGGER.isVerboseEnabled()) {
									LOGGER.verbose("newTagEle\n" + newTagEle);
								}
														
							}							
						 }
	        		 
	        	      }   			
	        	    	
	     		  }    
	           }
	
	
	//5130 End 2
	
		
	
	/**
	 * This method processes tag supplies coming in POS inventory messages. It retains those tag
	 * inventory supply of POS message which are present in OMS. if tag inventory supply of POS
	 * message is not found in OMS, it is loaded in OMS as non-tagged inventory supply
	 *
	 * @param env Environment variable
	 * @param yfcInputDoc Cloned input inventory message
	 * @param yfcOrigInputDoc Original input inventory message
	 * @throws GCException
	 * @throws SAXException 
	 * @throws IOException 
	 * @throws ParserConfigurationException 
	 * @throws SQLException
	 */
	private void processTagSupplies(YFSEnvironment env, YFCDocument yfcInputDoc) throws GCException, ParserConfigurationException, IOException, SAXException {
		LOGGER.beginTimer("GCLoadPOSInventoryMismatchAPI.processTagSupplies");
		LOGGER.verbose("Class: GCLoadPOSInventoryMismatchAPI Method: processTagSupplies BEGIN");

		YFCElement posItemsEle = yfcInputDoc.getDocumentElement();
		posItemsEle.setAttribute(GCConstants.APPLY_DIFFERENCES,
				GCConstants.FLAG_N);

		YFCNodeList<YFCElement> posInvItemList = posItemsEle.getElementsByTagName(GCConstants.ITEM);

		int posItemLstSize = posInvItemList.getLength();
		String shipNode = posItemsEle.getAttribute(GCConstants.SHIP_NODE);

		for (int itemCount = 0; itemCount < posItemLstSize; itemCount++) {

			YFCElement posItemEle = posInvItemList.item(itemCount);
			String posItemID = posItemEle.getAttribute(GCConstants.ITEM_ID);

			YFCDocument getItemListOp = callGetItemListAPI(env, posItemEle);

			YFCElement getItemListOpEle = getItemListOp.getDocumentElement();
			//GCSTORE-6934--Start
			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("filterDuplicateItems Input" + getItemListOpEle);
			}
			YFCElement omsItemEle = filterDuplicateItems(env, getItemListOpEle);
			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("filterDuplicateItems output" + omsItemEle);
			}
			//GCSTORE-6934--End
			if (YFCCommon.isVoid(omsItemEle)) {
				YFCElement posSupplies= posItemEle.getChildElement(GCConstants.SUPPLIES);

				YFCNodeList<YFCElement> posSupplyList = posSupplies.getElementsByTagName(GCConstants.SUPPLY);
				if(posSupplyList.getLength()>1){
					GCException e = new GCException("Multiple Supply elements found for POS Item " +posItemID+ " that does not exist in OMS");
					throw new GCException(e);
				}
				YFCElement posSupply=posSupplyList.item(0);
				YFCElement posTag= posSupply.getChildElement(GCConstants.TAG);
				if(!YFCCommon.isVoid(posTag)){
					GCException e = new GCException("Tag element is found for POS Item " +posItemID+ " that does not exist in OMS");
					throw new GCException(e);
				}
				posItemEle.setAttribute(GCConstants.ITEM_ID,posItemID);
			} else {

				String omsItemID = omsItemEle.getAttribute(GCConstants.ITEM_ID);
				posItemEle.setAttribute(GCConstants.ITEM_ID, omsItemID);

				//GCSTORE- 934 New Serialized Platinum Item Process -- Changes START

				YFCElement extnInItemEle = omsItemEle.getChildElement(GCConstants.EXTN);
				String extnIsPlatinumItem = extnInItemEle.getAttribute(GCXmlLiterals.EXTN_IS_PLATINUM_ITEM);
				LOGGER.verbose("IsPlatinum?" +extnIsPlatinumItem);

				// Fetch tag inventory details from OMS and adjust in Inventory message, only if it is some
				// time or always tag controlled
				if (YFCUtils.equals(GCConstants.YES, extnIsPlatinumItem)) {
					Map<String, String> omsTagNoLotNoMap = getListOfExistingTagsInOMS(env, shipNode, posItemEle);
					LOGGER.verbose("Map formed :" +omsTagNoLotNoMap.toString());

					YFCNodeList<YFCElement> posSuppliesNodeList = posItemEle.getElementsByTagName(GCConstants.SUPPLIES);
					YFCElement posSuppliesEle = posSuppliesNodeList.item(0);
					YFCNodeList<YFCElement> posSupplyNodeList = posSuppliesEle.getElementsByTagName(GCConstants.SUPPLY);
					int posSupplyNListSize = posSupplyNodeList.getLength();

					for (int posSupplyCount = 0; posSupplyCount < posSupplyNListSize; posSupplyCount++) {
						YFCElement posSupplyEle = posSupplyNodeList.item(posSupplyCount);
						LOGGER.verbose("Supply element fetched :" +posSupplyEle.toString());
						YFCElement posSupplyTagEle = posSupplyEle.getChildElement(GCXmlLiterals.TAG);
						LOGGER.verbose("Tag element :" +posSupplyTagEle);

						// Tag/@BatchNo value is available in tagNoLotNoMap
						// remove BatchNo from map which exists in OMS as it is already going to updated
						// through this message
						if(!YFCCommon.isVoid(posSupplyTagEle)&&omsTagNoLotNoMap.containsKey(posSupplyTagEle.getAttribute(GCXmlLiterals.BATCH_NO))) {
							String batchNoInMap = posSupplyTagEle.getAttribute(GCXmlLiterals.BATCH_NO);
							omsTagNoLotNoMap.remove(batchNoInMap);
							LOGGER.verbose("Map after removing map entry" +omsTagNoLotNoMap.toString());
						}
					}
					// Iterate through Map to stamp Supply Quantity as 0 for BatchNo which exist in OMS but
					// not present in ip XML, this is as a workaround because OMS does not delete the supply
					// if that tag inventory is not passed
					for (Map.Entry<String, String> entry : omsTagNoLotNoMap.entrySet()) {
						String batchNoInMap = entry.getKey();
						String lotAttr1 = entry.getValue();
						LOGGER.verbose("batchNoInMap: " + batchNoInMap + "\t" + "lotNoInMap: " + lotAttr1);
						YFCElement supplyEleForQty0 = posSuppliesEle.createChild(GCConstants.SUPPLY);
						supplyEleForQty0.setAttribute(GCConstants.AVAILABILITY_TYPE, GCConstants.TRACK_VAL);
						supplyEleForQty0.setAttribute(GCConstants.QUANTITY, "0");
						supplyEleForQty0.setAttribute(GCConstants.SUPPLY_TYPE, GCConstants.SUPPLY_TYPE_ONHAND);
						YFCElement newTagEle = supplyEleForQty0.createChild(GCXmlLiterals.TAG);
						newTagEle.setAttribute(GCXmlLiterals.BATCH_NO, batchNoInMap);
						newTagEle.setAttribute(GCXmlLiterals.LOT_ATTR1, lotAttr1);
						if (LOGGER.isVerboseEnabled()) {
							LOGGER.verbose("newTagEle\n" + newTagEle);
						}
					}
				} else {

					double noOfTagDiscarded = 0;
					YFCElement supplyEleOfNoTag = null;
					YFCNodeList<YFCElement> posSuppliesNodeList = posItemEle.getElementsByTagName(GCConstants.SUPPLIES);
					YFCElement posSuppliesEle = posSuppliesNodeList.item(0);
					YFCNodeList<YFCElement> posSupplyNodeList = posSuppliesEle.getElementsByTagName(GCConstants.SUPPLY);
					int posSupplyNListSize = posSupplyNodeList.getLength();

					for (int posSupplyCount = 0; posSupplyCount < posSupplyNListSize; posSupplyCount++) {
						YFCElement posSupplyEle = posSupplyNodeList.item(posSupplyCount);
						YFCElement posSupplyTagEle = posSupplyEle.getChildElement(GCXmlLiterals.TAG);
						//Tag element is not void for the item
						if (YFCCommon.isVoid(posSupplyTagEle)) {
							supplyEleOfNoTag = posSupplyEle;
							LOGGER.verbose("Regular Item");

						} else {
							// Tag/@BatchNo value is not available in tagNoLotNoMap
							LOGGER.verbose("Not platinum but tag controlled");
							noOfTagDiscarded+= posSupplyEle.getDoubleAttribute(GCConstants.QUANTITY);
							posSuppliesEle.removeChild(posSupplyEle);
							posSupplyCount --;
							posSupplyNListSize --;
						}
					}

					// if non-tag supply is coming in inventory message then adjust discarded tag inventory to
					// it. else create a new non-tag supply and add discarded qty.
					if (YFCCommon.isVoid(supplyEleOfNoTag)) {
						YFCElement eleSupplyNew = posSuppliesEle.createChild(GCConstants.SUPPLY);
						eleSupplyNew.setAttribute(GCConstants.AVAILABILITY_TYPE, GCConstants.TRACK_VAL);
						eleSupplyNew.setAttribute(GCConstants.SUPPLY_TYPE, GCConstants.SUPPLY_TYPE_ONHAND);
						eleSupplyNew.setAttribute(GCConstants.QUANTITY, noOfTagDiscarded);

					} else if (noOfTagDiscarded > 0) {
						double quantity = supplyEleOfNoTag.getDoubleAttribute(GCConstants.QUANTITY);
						quantity = quantity + noOfTagDiscarded;
						supplyEleOfNoTag.setAttribute(GCConstants.QUANTITY, quantity);
					}
				}
				//GCSTORE-5130 -- Start
		        if (LOGGER.isVerboseEnabled()) {
			  			LOGGER.verbose("\nvalidateProductClass method Input Doc" + YFCDocument.getDocumentFor(yfcInputDoc.getDocument()));			
			  	}
				validateProductClass(env, posItemEle, yfcInputDoc, getItemListOp,omsItemEle);
		        if (LOGGER.isVerboseEnabled()) {
			  			LOGGER.verbose("\nvalidateProductClass method Output Doc" + YFCDocument.getDocumentFor(yfcInputDoc.getDocument()));			
			  	}
		      //GCSTORE-5130 -- End
				//GCSTORE- 934 New Serialized Platinum Item Process -- Changes END
			}
		}
		// Raise Exception If POS Feed contains Invalid Items based on invalidItemLstSize as it contains
		// invalid itemsID if any
		// As per change in requirement no alert is raised when item is not found in OMS
		//raiseAlertForInvalidPOSItem(env, invalidPosItemLst, yfcOrigInputDoc);

		LOGGER.verbose("Exiting GCLoadPOSInventoryMismatchAPI.processTagSupplies() method");
		LOGGER.endTimer("GCLoadPOSInventoryMismatchAPI.processTagSupplies");
	}

	/**
	 * This method used to get the latest item if out of all 15 digits items in OMS
	 * @return omsItemEle
	 * GCSTORE-6934 -- Start
	 */
	private YFCElement filterDuplicateItems(YFSEnvironment env,
			YFCElement getItemListOpEle) {

		String latestItemKey = "";
		YFCElement omsItemEle = null;
        
		HashMap<String, YFCElement> dublicateMapData = new HashMap<String, YFCElement>();
		String sTotItemList = getItemListOpEle.getAttribute(GCConstants.TOTAL_ITEM_LIST);
		YFCNodeList<YFCElement> nItemList = getItemListOpEle.getElementsByTagName(GCConstants.ITEM);

		if(Integer.parseInt(sTotItemList) >1){
			
			Integer itemLength = getCommonCodeForItemLength(env);
			
			for(int count=0;count <nItemList.getLength();count++){
				
				YFCElement eleItem = nItemList.item(count);
				
				if(!YFCCommon.isVoid(eleItem)){
					String sItemID = eleItem.getAttribute(GCConstants.ITEM_ID);
					String sItemKey = eleItem.getAttribute(GCConstants.ITEM_KEY);
					
					if(!YFCCommon.isVoid(sItemID)){
						if(sItemID.length() >= itemLength){
							dublicateMapData.put(sItemKey, eleItem);
						}
					}
				}
			}
			
			for (Map.Entry<String, YFCElement> entry : dublicateMapData.entrySet()) {
				String tempLatestItemKey = entry.getKey();
				if(tempLatestItemKey.compareTo(latestItemKey) >0){
					latestItemKey = tempLatestItemKey;
				}
			}
			if(!YFCCommon.isVoid(dublicateMapData)){
			omsItemEle = dublicateMapData.get(latestItemKey);
			}else{
				omsItemEle = getItemListOpEle.getChildElement(GCConstants.ITEM);
			}
			
		} else {
			
			omsItemEle = getItemListOpEle.getChildElement(GCConstants.ITEM);
		}
		return omsItemEle;
	}
	//GCSTORE-6934 --- End

	/**
	 * This method used to get the length limit from common code
	 * @return itemNumberLimit
	 * GCSTORE-6934 -- Start
	 */
	private Integer getCommonCodeForItemLength(YFSEnvironment env) {
		String codeType = GCConstants.GET_POS_ITEM_ID_LIMIT;
		String codeValue = GCConstants.GET_POS_ITEM_ID_LIMIT_VALUE;
		Integer itemNumberLimit = 0;
		YFCDocument getCommonCodeListOutForItemType = GCCommonUtil.getCommonCodeListByTypeAndValue(env, codeType, GCConstants.GC, codeValue);
		if(!YFCCommon.isVoid(getCommonCodeListOutForItemType)){
		      Document outDoc = getCommonCodeListOutForItemType.getDocument();
		      Element eleCommonCode = GCXMLUtil.getElementByXPath(outDoc,"/CommonCodeList/CommonCode");
		      if(!YFCCommon.isVoid(eleCommonCode)){
		      itemNumberLimit = Integer.parseInt(eleCommonCode.getAttribute(GCConstants.CODE_SHORT_DESCRIPTION));
		      }else{
		    	  itemNumberLimit=0; //stamping 0 days
		      }
		}
		return itemNumberLimit;
	}
    //GCSTORE-6934 -- End


	/**
	 * This method call OOB API getItemList to fetch Item ID present in OMS for corresponding POS Item
	 * ID coming in inventory update messages
	 *
	 * @param env Environment variable
	 * @param posInvItemEle Item element of the inventory update message for which BatchNo and
	 *        LotNumber are getting fetched
	 * @return getItemListOp Output of getItemList API
	 * @throws SQLException
	 * @throws GCException
	 * @throws SAXException 
	 * @throws IOException 
	 * @throws ParserConfigurationException 
	 */
	private YFCDocument callGetItemListAPI(YFSEnvironment env, YFCElement posInvItemEle) throws GCException, ParserConfigurationException, IOException, SAXException {
		LOGGER.beginTimer("GCLoadPOSInventoryMismatchAPI.callGetItemListAPI");
		LOGGER.verbose("Class: GCLoadPOSInventoryMismatchAPI Method: callGetItemListAPI BEGIN");

		YFCDocument tmpGetItemList = YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LIST_API_TMPL);
		// getItemList input Doc
		YFCDocument getItemListIp = YFCDocument.createDocument(GCConstants.ITEM);
		YFCElement itemEle = getItemListIp.getDocumentElement();
		YFCElement extnEle = getItemListIp.createElement(GCConstants.EXTN);
		itemEle.appendChild(extnEle);
		itemEle.setAttribute("IgnoreIsSoldSeparately", "Y");
		String posItemID = posInvItemEle.getAttribute(GCConstants.ITEM_ID);
		extnEle.setAttribute(GCXmlLiterals.EXTN_POS_ITEM_ID, posItemID);
		if (LOGGER.isVerboseEnabled()) {
			LOGGER
			.verbose("\nInput Doc For getItemList API Call\n" + getItemListIp);
		}

		YFCDocument getItemListOp =
				GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ITEM_LIST, getItemListIp, tmpGetItemList);
		
		
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("\nOutput Doc For getItemList API Call\n"
					+ YFCDocument.getDocumentFor(getItemListOp.getDocument()));
		}
		LOGGER.verbose("Exiting GCLoadPOSInventoryMismatchAPI.callGetItemListAPI() method");
		LOGGER.endTimer("GCLoadPOSInventoryMismatchAPI.callGetItemListAPI");
		return getItemListOp;
	}

	

	/**
	 * This method returns Map containing BatchNo and LotNo of Existing Tags.
	 *
	 * @param env Environment variable
	 * @param shipNode ShipNode associated to inventory update message
	 * @param posItemEle Item element of the inventory update message for which BatchNo and LotNumber
	 *        are getting fetched
	 * @return tagNoLotNoMap Map containing BatchNo and LotNo of Existing Tags
	 * @throws SAXException 
	 * @throws IOException 
	 * @throws ParserConfigurationException 
	 */                                          
	
	
	//Input Item element - Here we are fetching all the tags in a map, also 
	
	private Map<String, String> getListOfExistingTagsInOMS(YFSEnvironment env, String shipNode, YFCElement posItemEle) throws ParserConfigurationException, IOException, SAXException {
		LOGGER.beginTimer("GCLoadPOSInventoryMismatchAPI.getListOfExistingTagsInOMS");
		LOGGER.verbose("Class: GCLoadPOSInventoryMismatchAPI Method: getListOfExistingTagsInOMS BEGIN");
		// Input Doc of getInventorySupply API
		YFCDocument inDocGetInvSupply = YFCDocument.createDocument(GCXmlLiterals.INVENTORY_SUPPLY);
		YFCElement eleRootInvSupp = inDocGetInvSupply.getDocumentElement();
		eleRootInvSupp.setAttribute(GCConstants.ITEM_ID, posItemEle.getAttribute(GCConstants.ITEM_ID));
		eleRootInvSupp.setAttribute(GCConstants.ORGANIZATION_CODE, posItemEle.getAttribute(GCXmlLiterals.INV_ORG_CODE));
		eleRootInvSupp.setAttribute(GCConstants.PRODUCT_CLASS, posItemEle.getAttribute(GCConstants.PRODUCT_CLASS));
		eleRootInvSupp.setAttribute(GCConstants.SHIP_NODE, shipNode);
		eleRootInvSupp.setAttribute(GCConstants.SUPPLY_TYPE, GCConstants.SUPPLY_TYPE_ONHAND);
		eleRootInvSupp.setAttribute(GCConstants.UNIT_OF_MEASURE, posItemEle.getAttribute(GCConstants.UNIT_OF_MEASURE));
		YFCDocument getInvSupplyTempDoc = YFCDocument.getDocumentFor(GCConstants.GET_INV_SUPPLY_TMP);
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("\n getInvSupplyTempDoc:" + getInvSupplyTempDoc);
			LOGGER.verbose("\n getInvSupplyInDoc:" + YFCDocument.getDocumentFor(inDocGetInvSupply.getDocument()));
		}
		
		
		YFCDocument getInvSupplyOp =
				GCCommonUtil.invokeAPI(env, GCXmlLiterals.API_GET_INV_SUPPLY, inDocGetInvSupply, getInvSupplyTempDoc);
		
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("\nOutput outDocGetInvSupply \n" + YFCDocument.getDocumentFor(getInvSupplyOp.getDocument()));
		}
		
		
		// for tag inventory
				//outItemEle = Item
		YFCElement outItemEle = getInvSupplyOp.getDocumentElement();

		YFCElement outSuppliesEle = outItemEle.getChildElement(GCConstants.SUPPLIES);
		YFCNodeList<YFCElement> invSupplyOpList = outSuppliesEle.getElementsByTagName(GCXmlLiterals.INVENTORY_SUPPLY);

		Map<String, String> tagNoLotNoMap = new HashMap<String, String>();
		for (YFCElement invSupplyEle : invSupplyOpList) {
			YFCElement eleOutTag = invSupplyEle.getChildElement(GCXmlLiterals.TAG);
			double omsSupplyQty = invSupplyEle.getDoubleAttribute(GCConstants.QUANTITY);
			if (!YFCCommon.isVoid(eleOutTag) && omsSupplyQty > 0.0) {
				String sBatchNo = eleOutTag.getAttribute(GCXmlLiterals.BATCH_NO);
				String sLotNumber = eleOutTag.getAttribute(GCXmlLiterals.LOT_ATTR1);
				tagNoLotNoMap.put(sBatchNo, sLotNumber);
				LOGGER.verbose("Storing in map as: BatchNo=" + sBatchNo + "LotNumber=" + sLotNumber);
			}
		}

		LOGGER.verbose("Exiting GCLoadPOSInventoryMismatchAPI.getListOfExistingTagsInOMS() method");
		LOGGER.endTimer("GCLoadPOSInventoryMismatchAPI.getListOfExistingTagsInOMS");
		return tagNoLotNoMap;
	}

	//5130- new method
	
	private Map<String, String> getListOfExistingTagsInOMSWithProductClass(YFSEnvironment env, String shipNode, String sOmsItemID, String sProductClassOp) throws ParserConfigurationException, IOException, SAXException {
		LOGGER.beginTimer("GCLoadPOSInventoryMismatchAPI.getListOfExistingTagsInOMS");
		LOGGER.verbose("Class: GCLoadPOSInventoryMismatchAPI Method: getListOfExistingTagsInOMS BEGIN");
		// Input Doc of getInventorySupply API
		YFCDocument inDocGetInvSupply = YFCDocument.createDocument(GCXmlLiterals.INVENTORY_SUPPLY);
		YFCElement eleRootInvSupp = inDocGetInvSupply.getDocumentElement();
		eleRootInvSupp.setAttribute(GCConstants.ITEM_ID, sOmsItemID);
		eleRootInvSupp.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.INV_ORGANIZATION_CODE);
		eleRootInvSupp.setAttribute(GCConstants.PRODUCT_CLASS, sProductClassOp);
		eleRootInvSupp.setAttribute(GCConstants.SHIP_NODE, shipNode);
		eleRootInvSupp.setAttribute(GCConstants.SUPPLY_TYPE, GCConstants.SUPPLY_TYPE_ONHAND);
		eleRootInvSupp.setAttribute(GCConstants.UNIT_OF_MEASURE, GCConstants.EACH);
		YFCDocument getInvSupplyTempDoc = YFCDocument.getDocumentFor(GCConstants.GET_INV_SUPPLY_TMP);
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("\n getInvSupplyTempDoc:" + getInvSupplyTempDoc);
			LOGGER.verbose("\n getInvSupplyInDoc:" + YFCDocument.getDocumentFor(inDocGetInvSupply.getDocument()));
		}
		

		
		YFCDocument getInvSupplyOp =
				GCCommonUtil.invokeAPI(env, GCXmlLiterals.API_GET_INV_SUPPLY, inDocGetInvSupply, getInvSupplyTempDoc);
				
//		
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("\nOutput outDocGetInvSupply \n" + YFCDocument.getDocumentFor(getInvSupplyOp.getDocument()));
		}
		
		
	
		// for tag inventory
				//outItemEle = Item
		YFCElement outItemEle = getInvSupplyOp.getDocumentElement();

		YFCElement outSuppliesEle = outItemEle.getChildElement(GCConstants.SUPPLIES);
		YFCNodeList<YFCElement> invSupplyOpList = outSuppliesEle.getElementsByTagName(GCXmlLiterals.INVENTORY_SUPPLY);

		Map<String, String> tagNoLotNoMap = new HashMap<String, String>();
		for (YFCElement invSupplyEle : invSupplyOpList) {
			YFCElement eleOutTag = invSupplyEle.getChildElement(GCXmlLiterals.TAG);
			double omsSupplyQty = invSupplyEle.getDoubleAttribute(GCConstants.QUANTITY);
			if (!YFCCommon.isVoid(eleOutTag) && omsSupplyQty > 0.0) {
				String sBatchNo = eleOutTag.getAttribute(GCXmlLiterals.BATCH_NO);
				String sLotNumber = eleOutTag.getAttribute(GCXmlLiterals.LOT_ATTR1);
				tagNoLotNoMap.put(sBatchNo, sLotNumber);
				LOGGER.verbose("Storing in map as: BatchNo=" + sBatchNo + "LotNumber=" + sLotNumber);
			}
		}

		LOGGER.verbose("Exiting GCLoadPOSInventoryMismatchAPI.getListOfExistingTagsInOMS() method");
		LOGGER.endTimer("GCLoadPOSInventoryMismatchAPI.getListOfExistingTagsInOMS");
		return tagNoLotNoMap;
	}
	
	
	
	
	private YFCDocument callGetItemInventorySupplyForNonTaggedSupply(YFSEnvironment env, String shipNode, String sOmsItemID, String sProductClassOp) throws GCException, ParserConfigurationException, IOException, SAXException {
		LOGGER.beginTimer("GCLoadPOSInventoryMismatchAPI.callGetItemListAPI");
		// Input Doc of getInventorySupply API
				YFCDocument inDocGetInvSupply = YFCDocument.createDocument(GCXmlLiterals.INVENTORY_SUPPLY);
				YFCElement eleRootInvSupp = inDocGetInvSupply.getDocumentElement();
				eleRootInvSupp.setAttribute(GCConstants.ITEM_ID, sOmsItemID);
				eleRootInvSupp.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.INV_ORGANIZATION_CODE);
				eleRootInvSupp.setAttribute(GCConstants.PRODUCT_CLASS, sProductClassOp);
				eleRootInvSupp.setAttribute(GCConstants.SHIP_NODE, shipNode);
				eleRootInvSupp.setAttribute(GCConstants.SUPPLY_TYPE, GCConstants.SUPPLY_TYPE_ONHAND);
				eleRootInvSupp.setAttribute(GCConstants.UNIT_OF_MEASURE, GCConstants.EACH);
				YFCDocument getInvSupplyTempDoc = YFCDocument.getDocumentFor(GCConstants.GET_INV_SUPPLY_TMP);
				if (LOGGER.isVerboseEnabled()) {
					LOGGER.verbose("\n getInvSupplyTempDoc:" + getInvSupplyTempDoc);
					LOGGER.verbose("\n getInvSupplyInDoc:" + YFCDocument.getDocumentFor(inDocGetInvSupply.getDocument()));
				}

				YFCDocument getInvSupplyOp =
						GCCommonUtil.invokeAPI(env, GCXmlLiterals.API_GET_INV_SUPPLY, inDocGetInvSupply, getInvSupplyTempDoc);
				
				return getInvSupplyOp;
		
		
	}
	
	
	@Override
	public void setProperties(Properties props) {
	}
}

