/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################################
 *          OBJECTIVE: GCSTORE-73 - POS Inventory Sync. This class is used to receive store inventory messages from POS
 * and update in OMS
 *###################################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *################################################################################################################################################################################
             1.0                               Kumar, Gunjan                  Shipment
             1.1            25/03/2015       Rngarajan, Shwetha         Code changes for handling Supply Sync for Platinum Items.
 *##################################################################################################################################################################################
 */
package com.gc.api.inventory;



import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.w3c.dom.Document;
import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * GCSTORE-73 - Bizlink Inventory Sync. This class is used to receive store inventory messages from Bizlink
 * and update in OMS
 *
 * This class gets inventory update messages from Bizlink which is current supply picture of the item at
 * a Drop Ship Vendor Node. 
 *
 * @author shamimArora
 *
 */
public class GCLoadVendorNodeInventoryMismatchAPI implements YIFCustomApi {

	// initialize logger
	private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCLoadVendorNodeInventoryMismatchAPI.class.getName());

	/**
	 * This method receives inventory update messages from Bizlink, call OOB API
	 * loadInventoryMismatch to load inventory in YFS_INVENTORY_SUPPLY_TEMP table of OMS
	 *
	 * @param env - Environment variable
	 * @param inputDoc - Bizlink inventory message
	 * @return inputDoc - Bizlink inventory message
	 * @throws GCException
	 * @throws SQLException
	 */
	public Document loadVendorNodeInventoryUpdate(YFSEnvironment env, Document inputDoc) throws GCException {

		LOGGER.beginTimer("GCLoadVendorNodeInventoryMismatchAPI.loadVendorNodeInventoryUpdate");
		LOGGER.verbose("Class: GCLoadVendorNodeInventoryMismatchAPI Method: loadVendorNodeInventoryUpdate BEGIN");

		Document cloneDoc = (Document) inputDoc.cloneNode(true);

		YFCDocument yfcInputDoc = YFCDocument.getDocumentFor(cloneDoc);
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("\nyfcInputDoc CopyDoc" + YFCDocument.getDocumentFor(yfcInputDoc.getDocument()));
		}

		YFCElement rootElement = yfcInputDoc.getDocumentElement();
		
		String rootStr = rootElement.getTagName();

		if (!YFCCommon.equalsIgnoreCase("EOF", rootStr)) {
			
			YFCNodeList<YFCElement> vendorNodeInvItemList = rootElement.getElementsByTagName(GCConstants.ITEM);

			int vendorItemLstSize = vendorNodeInvItemList.getLength();
			String shipNode = rootElement.getAttribute(GCConstants.SHIP_NODE);
			
			for (int itemCount = 0; itemCount < vendorItemLstSize; itemCount++) {

				YFCElement vendorNodeItemEle = vendorNodeInvItemList.item(itemCount);
				String vendorNodeItemID = vendorNodeItemEle.getAttribute(GCConstants.ITEM_ID);

				if (YFCCommon.isVoid(vendorNodeItemID) || vendorNodeItemID.equalsIgnoreCase(GCConstants.BLANK)){
				
					YFCDocument getItemListOp = callGetItemListAPI(env, vendorNodeItemEle);
					YFCElement getItemListOpEle = getItemListOp.getDocumentElement();
					YFCElement vItemEle = getItemListOpEle.getChildElement(GCConstants.ITEM);
					if(!YFCCommon.isVoid(vItemEle)){
						 vendorNodeItemID = vItemEle.getAttribute(GCConstants.ITEM_ID);
						 vendorNodeItemEle.setAttribute(GCConstants.ITEM_ID, vendorNodeItemID);
						 LOGGER.verbose("Element Item"+vendorNodeItemEle.toString());
					}

				}		
			}
					
			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("\nloadInventoryMismatch API Input Doc" + YFCDocument.getDocumentFor(yfcInputDoc.getDocument()));
			}
			Document loadInvMismatch = GCCommonUtil.invokeAPI(env, GCConstants.API_LOAD_INVENTORY_MISMATCH, yfcInputDoc.getDocument());
			LOGGER.verbose("LoadInvMismatch Outdoc :" +YFCDocument.getDocumentFor(loadInvMismatch).toString());
		}
		LOGGER.verbose("Exiting GCLoadVendorNodeInventoryMismatchAPI.loadVendorNodeInventoryUpdate() method");
		LOGGER.endTimer("GCLoadVendorNodeInventoryMismatchAPI.loadVendorNodeInventoryUpdate");
		return inputDoc;
	}

	/**
	 * This method call OOB API getItemList to fetch Item ID present in OMS for corresponding Vendor Node Item
	 * ID coming in inventory update messages
	 *
	 * @param env Environment variable
	 * @param posInvItemEle Item element of the inventory update message
	 * @return getItemListOp Output of getItemList API
	 * @throws SQLException
	 * @throws GCException
	 */
	private YFCDocument callGetItemListAPI(YFSEnvironment env, YFCElement posInvItemEle) throws GCException {
		LOGGER.beginTimer("GCLoadVendorNodeInventoryMismatchAPI.callGetItemListAPI");
		LOGGER.verbose("Class: GCLoadVendorNodeInventoryMismatchAPI Method: callGetItemListAPI BEGIN");
		
	   String itemAliasValue = posInvItemEle.getAttribute(GCConstants.UPC_CODE);

		YFCDocument tmpGetItemList = YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LIST_API_TMPL);
		// getItemList input Doc
		YFCDocument getItemListIp = YFCDocument.createDocument(GCConstants.ITEM);
		YFCElement itemEle = getItemListIp.getDocumentElement();
		YFCElement itemAliasList = getItemListIp.createElement(GCConstants.ITEM_ALIAS_LIST);
		YFCElement itemAlias = getItemListIp.createElement(GCConstants.ITEM_ALIAS);
		itemAlias.setAttribute(GCConstants.ALIAS_VALUE, itemAliasValue);
		itemAliasList.appendChild(itemAlias);
		itemEle.appendChild(itemAliasList);
		if (LOGGER.isVerboseEnabled()) {
			LOGGER
			.verbose("\nInput Doc For getItemList API Call\n" + getItemListIp);
		}

		YFCDocument getItemListOp =
				GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ITEM_LIST, getItemListIp, tmpGetItemList);
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("\nOutput Doc For getItemList API Call\n"
					+ YFCDocument.getDocumentFor(getItemListOp.getDocument()));
		}
		LOGGER.verbose("Exiting GCLoadVendorNodeInventoryMismatchAPI.callGetItemListAPI() method");
		LOGGER.endTimer("GCLoadVendorNodeInventoryMismatchAPI.callGetItemListAPI");
		return getItemListOp;
	}

	
	@Override
	public void setProperties(Properties props) {
	}
}

