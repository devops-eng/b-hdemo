/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################################
 *          OBJECTIVE: GCSTORE-73 - POS Inventory Sync. This class is used to receive store inventory messages from POS
 * and update in OMS
 *###################################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *################################################################################################################################################################################
             1.0                               Kumar, Shashank               Fix for GCSTORE-7612
 *##################################################################################################################################################################################
 */
package com.gc.api.inventory;



import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.shared.ycp.YFSContext;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * GCSTORE-7612 - This class gets the same XML as loadInvenotryMismatch API but with OMS Item Ids
 *
 * This class first invokes getInventoryNodeControlList API to get the list of item+node combination in YFS_INVENTORY_NODE_CONTROL table and 
 * then invokes manageNodeInventoryControl API for which entry is found. While invoking manageInventoryNodeControl API it will set InvPicIncorrectTillDate 
 * to current system date + 30mins (which will be configurable)
 *
 * @author shashankkumar
 *
 */
public class GCManageInvCntrlOnLoadMismatch implements YIFCustomApi {

	String sNode = null;
	String sProdClass = null;

	// initialize logger
	private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCManageInvCntrlOnLoadMismatch.class.getName());

	/**
	 * This method receives inventory update messages from LoadInvMismatch class, and invokes get
	 *
	 * @param env - Environment variable
	 * @param inputDoc - POS inventory message
	 * @return inputDoc - POS inventory message
	 * @throws GCException
	 * @throws SQLException
	 */
	public Document manageNodeInvControlAPI (YFSEnvironment env, Document inputDoc) throws GCException {

		LOGGER.beginTimer("GCManageNodeInvCntrlOnLoadMismatchAPI.manageNodeInvControl");
		LOGGER.verbose("Class: GCManageNodeInvCntrlOnLoadMismatchAPI Method: manageNodeInvControl BEGIN");

		YFCDocument yfcInputDoc = YFCDocument.getDocumentFor(inputDoc);
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("\nyfcInputDoc CopyDoc" + YFCDocument.getDocumentFor(yfcInputDoc.getDocument()));
		}

		YFCElement rootElement = yfcInputDoc.getDocumentElement();
		sNode = rootElement.getAttribute(GCConstants.SHIP_NODE);
		YFCNodeList<YFCElement> nlItem = rootElement.getElementsByTagName(GCConstants.ITEM);

		Map<String, String> itemPCMap = new HashMap<String, String>();

		getInvNodeControl(env, itemPCMap, yfcInputDoc);
		try{
			invokeManageNodeInvControl(env, nlItem, itemPCMap);
		} catch (Exception ex){
			LOGGER.error("manageInventoryNodeControl failed.. " + ex.getMessage());
			//Do Nothing
			//OMS is eating up this exception because if this fails for one record we do not want it to fail for other records
			//Raising this will not have any significance for PROD support user
		}

		LOGGER.verbose("Exiting GCManageNodeInvCntrlOnLoadMismatchAPI.manageNodeInvControl() method");
		LOGGER.endTimer("GCManageNodeInvCntrlOnLoadMismatchAPI.manageNodeInvControl");
		return inputDoc;
	}

	/**
	 * Invoke manageInventoryNodeControl API to mark node as dirty
	 * 
	 * @param env
	 * @param nlItem
	 * @param itemPCMap
	 * @throws SQLException
	 */
	private void invokeManageNodeInvControl(YFSEnvironment env,
			YFCNodeList<YFCElement> nlItem, Map<String, String> itemPCMap) throws SQLException {
		YFCDocument manageInvNodeCtrlInDoc = YFCDocument.createDocument(GCConstants.INVENTORY_NODE_CONTROL);
		YFCElement eleManageInvNodeCtrl = manageInvNodeCtrlInDoc.getDocumentElement();
		eleManageInvNodeCtrl.setAttribute(GCConstants.NODE, sNode);
		eleManageInvNodeCtrl.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GCI);
		eleManageInvNodeCtrl.setAttribute(GCConstants.UNIT_OF_MEASURE, GCConstants.EACH);

		String inCorrectTill = getIncorrectTillDate(env);
		eleManageInvNodeCtrl.setAttribute(GCConstants.INV_PICTURE_INCORRECT_TILL_DATE, inCorrectTill);

		String sItemID = "";
		String sProdClass = "";

		for(YFCElement eleNlItem : nlItem){
			sItemID = eleNlItem.getAttribute(GCConstants.ITEM_ID);
			sProdClass = eleNlItem.getAttribute(GCConstants.PRODUCT_CLASS);

			if(itemPCMap.containsKey(sItemID + "#" + sProdClass)){
				eleManageInvNodeCtrl.setAttribute(GCConstants.ITEM_ID, sItemID);
				eleManageInvNodeCtrl.setAttribute(GCConstants.PRODUCT_CLASS, sProdClass);

				GCCommonUtil.invokeAPI(env, GCConstants.MANAGE_INVENTORY_NODE_CONTROL, manageInvNodeCtrlInDoc, null);
				((YFSContext)env).commit();

			}
		}
	}

	/**
	 * Add configured no. of minutes i.e. 30 to the current system date
	 * 
	 * @param env
	 * @return
	 */
	private String getIncorrectTillDate(YFSEnvironment env) {
		String timeStamp = "";
		String codeType = GCConstants.ADDITIONAL_MINS_FOR_NODE_CTRL;
		Integer additionalMins = callCommonCodeToGetAdditionalMins(env, codeType);

		if(!YFCCommon.isVoid(additionalMins)){
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			c.add(Calendar.MINUTE, additionalMins);
			Date date = c.getTime();
			timeStamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").format(date); 
		}
		return timeStamp;
	}

	/**
	 * Invoke common code to fetch the configured value which needs to be added to InvPicIncorrectTillDate to mark node as dirty
	 * 
	 * @param env
	 * @param codeType
	 * @return
	 */
	private Integer callCommonCodeToGetAdditionalMins(YFSEnvironment env, String codeType) {
		String sItemType = "";
		Integer additionalMins = 0;
		YFCDocument additionalMinsDoc = GCCommonUtil
				.getCommonCodeListByTypeAndValue(
						env,codeType,GCConstants.GC, sItemType);
		if(!YFCCommon.isVoid(additionalMinsDoc)){
			Document additionalMinsOutDoc = additionalMinsDoc.getDocument();
			Element eleCommonCode = GCXMLUtil.getElementByXPath(additionalMinsOutDoc,"/CommonCodeList/CommonCode");
			if(!YFCCommon.isVoid(eleCommonCode)){
				additionalMins = Integer.parseInt(eleCommonCode.getAttribute(GCConstants.CODE_VALUE));
			}
		}
		return additionalMins; 
	}


	/**
	 * Invoke getInventoryNodeControlList API
	 * 
	 * @param env
	 * @param itemPCMap
	 * @param yfcInputDoc
	 */
	private void getInvNodeControl(YFSEnvironment env, Map<String, String> itemPCMap, YFCDocument yfcInputDoc) {

		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		Date date = cal.getTime();
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").format(date);

		YFCDocument getInvNodeCtrlListinDoc = YFCDocument.createDocument(GCConstants.INVENTORY_NODE_CONTROL);
		YFCElement eleGetInvNodeCtrl = getInvNodeCtrlListinDoc.getDocumentElement();
		eleGetInvNodeCtrl.setAttribute(GCConstants.NODE, sNode);
		eleGetInvNodeCtrl.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GCI);
		eleGetInvNodeCtrl.setAttribute(GCConstants.NODE_CONTROL_TYPE, GCConstants.ON_HOLD);
		eleGetInvNodeCtrl.setAttribute(GCConstants.INV_PICTURE_INCORRECT_TILL_DATE, timeStamp);
		eleGetInvNodeCtrl.setAttribute(GCConstants.INV_PICTURE_INCORRECT_TILL_DATE_QRY_TYPE, GCConstants.GREATER_THAN_OR_EQUAL); 

		YFCDocument outDoc =
				GCCommonUtil.invokeAPI(env, GCConstants.GET_INV_NODE_CTRL_LIST, getInvNodeCtrlListinDoc, null);

		YFCElement rootOutEle = outDoc.getDocumentElement();
		YFCNodeList<YFCElement> nlInvNodeCtrl = rootOutEle.getElementsByTagName(GCConstants.INVENTORY_NODE_CONTROL);

		for(YFCElement eleNlInvNodeCtrl : nlInvNodeCtrl){
			itemPCMap.put(eleNlInvNodeCtrl.getAttribute(GCConstants.ITEM_ID) + "#" + eleNlInvNodeCtrl.getAttribute(GCConstants.PRODUCT_CLASS), "");			
		}
	}



	@Override
	public void setProperties(Properties props) {
	}
}

