/** 
 * Copyright © 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               22/02/2014        Gowda, Naveen		        OMS-232: As and OMS, I want to receive web inventory receipt messages so that I can 
                                                                                     adjust available inventory and open reservations to provide accurate inventory 
                                                                                     levels to customers 
 *#################################################################################################################################################################
 */
package com.gc.api.inventory;

import java.util.Properties;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:naveen.s@expicient.com">Naveen</a>
 */
public class GCReceivePurchaseOrder implements YIFCustomApi {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCReceivePurchaseOrder.class.getName());

    /**
     * 
     * Description of receivePO - This method is used to receive the purchase
     * order. Before invoking receiveOrder API startReceipt API is invoked.
     * 
     * @param env
     *            YFSEnviroment
     * @param inDoc
     *            Input Document
     * @throws Exception
     *             Exception
     * 
     */

    public void receivePO(YFSEnvironment env, Document inDoc)
	    throws GCException {

	LOGGER.debug("Class: GCReceivePurchaseOrder Method: receivePO - BEGIN");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug("Incoming XML Doc" + GCXMLUtil.getXMLString(inDoc));
	}
	try {
	    Element eleRoot = inDoc.getDocumentElement();
	    String sDAXOrderNo = GCXMLUtil.getAttributeFromXPath(inDoc,
		    "//Extn/@ExtnDAXOrderNo");

	    GCCreateAndManagePO obj = new GCCreateAndManagePO();
	    String sOrderNo = obj.fetchOrderNoUsingDAXOrderNo(env, sDAXOrderNo);
	    Element eleShip = GCXMLUtil.getElementByXPath(inDoc,
		    GCConstants.RECEIPT_SHIPMENT_XPATH);
	    // Fetching mandatory attributes for API calls.
	    eleShip.setAttribute(GCConstants.ORDER_NO, sOrderNo);
	    String sDocType = eleRoot.getAttribute(GCConstants.DOCUMENT_TYPE);
	    String sEntCode = eleShip.getAttribute(GCConstants.ENTERPRISE_CODE);

	    Element eleReceiptLines = GCXMLUtil.getElementByXPath(inDoc,
		    GCConstants.RECEIPT_RECEIPTLINES_XPATH);

	    //Bug-4324 Starts:Finding ShipmentNo if one exist already. will be using that in the startReceipt input.
	    Document docGetShipmentListIP = GCXMLUtil
	            .createDocument(GCConstants.SHIPMENT);
	    Element eleShipment=docGetShipmentListIP.getDocumentElement();
	    eleShipment.setAttribute("OrderNo", sOrderNo);
	    Document getShipmentListOpTmp = GCXMLUtil
                .getDocument("<Shipments TotalNumberOfRecords=''><Shipment ShipmentNo=''/></Shipments>");
	    Document docGetShipmentListOp = GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST,docGetShipmentListIP, getShipmentListOpTmp);
	    NodeList nlShipment = XPathAPI.selectNodeList(docGetShipmentListOp,
                "Shipments/Shipment");
	    int shipmentCount=nlShipment.getLength();
	    String sShipmentNo=null;
	    if(shipmentCount!=0){
	        Element elemShipment=(Element)nlShipment.item(0);
	        sShipmentNo=elemShipment.getAttribute("ShipmentNo");
	    }
	    //Bug-4324 Ends
	 // Creating receipt no by calling startReceipt API.
	    Document docStartRecptOut = callStartReceipt(env, eleRoot, eleShip,sShipmentNo);
	    String sRcptHeaderKey = GCXMLUtil.getAttributeFromXPath(
		    docStartRecptOut,
		    GCConstants.RECEIPT_RECEIPT_HEADER_KEY_ATTR);
	    LOGGER.debug("RecptHeaderKey" + sRcptHeaderKey);
	    eleRoot.setAttribute(GCConstants.RECEIPT_HEADER_KEY, sRcptHeaderKey);

	    NodeList nodeLstRcptLn = GCXMLUtil.getNodeListByXpath(inDoc,
		    GCConstants.RECEIPT_RECEIPTLINES_RECEIPTLINE_XPATH);
	    for (int i = 0; i < nodeLstRcptLn.getLength(); i++) {
		Element eleRcptLine = (Element) nodeLstRcptLn.item(i);
		String sItemID = eleRcptLine.getAttribute(GCConstants.ITEM_ID);
		String sPrimeLineNo = eleRcptLine
			.getAttribute(GCConstants.PRIME_LINE_NO);
		String sSubLineNo = eleRcptLine
			.getAttribute(GCConstants.SUB_LINE_NO);
		String sRcptLnQty = eleRcptLine
			.getAttribute(GCConstants.QUANTITY);
		// Calling getOrderLineList API for the particular item.
		Document docOut = callGetOrderLineListAPI(env, sDocType,
			sEntCode, sItemID, sOrderNo, sPrimeLineNo, sSubLineNo);
		String sOrderedQty = GCXMLUtil.getAttributeFromXPath(docOut,
			GCConstants.ORDERLINELIST_ORDERLINE_ORDEREDQTY_ATTR);
		String sOrdrLnKey = GCXMLUtil.getAttributeFromXPath(docOut,
			GCConstants.ORDERLINELIST_ORDERLINE_ORDERLINEKEY_ATTR);

		String sKitCode = GCXMLUtil.getAttributeFromXPath(docOut,
			GCConstants.ORDERLINELIST_ORDERLINE_KITCODE_ATTR);
		if ((GCConstants.BUNDLE).equalsIgnoreCase(sKitCode)) {
		    // If item is a bundle remove the bundle item from the
		    // receipt
		    // lines and replace with components
		    Document docGetOrdrLnLstOut = callGetOrderLineListForBundle(
			    env, sOrdrLnKey);
		    GCXMLUtil.removeChild(eleReceiptLines, eleRcptLine);

		    NodeList nLOrdrLine = GCXMLUtil.getNodeListByXpath(
			    docGetOrdrLnLstOut,
			    GCConstants.ORDERLINELIST_ORDERLINE);
		    for (int j = 0; j < nLOrdrLine.getLength(); j++) {
			Element eleOrderLine = (Element) nLOrdrLine.item(j);

			String sOrderLineKey = eleOrderLine
				.getAttribute(GCConstants.ORDER_LINE_KEY);

			String sComQuanity = eleOrderLine
				.getAttribute(GCConstants.ORDERED_QUANITY);
			Double dFinalQty = (Double.parseDouble(sComQuanity) * Double
				.parseDouble(sRcptLnQty))
				/ Double.parseDouble(sOrderedQty);
			String sFinalQty = dFinalQty.toString();
			LOGGER.debug("Component Qty is " + sFinalQty);
			Element eleReceiptLine = inDoc
				.createElement(GCConstants.RECEIPT_LINE);
			eleReceiptLine.setAttribute(GCConstants.ORDER_LINE_KEY,
				sOrderLineKey);
			eleReceiptLine.setAttribute(GCConstants.QUANTITY,
				sFinalQty);
			eleReceiptLines.appendChild(eleReceiptLine);
		    }
		}

	    }
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("Input doc to Receive Order API call"
			+ GCXMLUtil.getXMLString(inDoc));
	    }
	    GCCommonUtil.invokeAPI(env, GCConstants.API_RECEIVE_ORDER, inDoc);
	    callCloseReceipt(env, eleRoot, eleShip, sRcptHeaderKey);
	} catch (Exception e) {
	    LOGGER.error("Exception in GCReceivePurchaseOrder:receivePO ", e);
	    throw new GCException(e);
	}
	LOGGER.debug("Class: GCReceivePurchaseOrder Method: receivePO - END");
    }

    /**
     * 
     * Description of callCloseReceipt - This method is used to close receipt
     * which was created in the beginning.
     * 
     * @param env
     *            YFSEnvironment
     * @param eleRoot
     *            Root Element of Input Doc.
     * @param eleShip
     *            Shipment Element of Input Doc.
     * @param sRcptHeaderKey
     *            ReceiptHeaderKey obtained from startReceipt API call.
     * @throws Exception
     *             Exception
     * 
     */
    private void callCloseReceipt(YFSEnvironment env, Element eleRoot,
	    Element eleShip, String sRcptHeaderKey) throws GCException {
	LOGGER.debug("Class: GCReceivePurchaseOrder Method: callCloseReceipt - BEGIN");
	try {
	    Document docStrtRcptIn = GCXMLUtil
		    .createDocument(GCConstants.RECEIPT);
	    Element eleDocRoot = docStrtRcptIn.getDocumentElement();
	    Element eleShipment = docStrtRcptIn
		    .createElement(GCConstants.SHIPMENT);
	    GCXMLUtil.copyAttributes(eleRoot, eleDocRoot);
	    GCXMLUtil.copyAttributes(eleShip, eleShipment);
	    eleDocRoot.setAttribute(GCConstants.RECEIPT_HEADER_KEY,
		    sRcptHeaderKey);
	    eleDocRoot.appendChild(eleShipment);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("Input Doc to closeReceipt API"
			+ GCXMLUtil.getXMLString(docStrtRcptIn));
	    }
	    GCCommonUtil.invokeAPI(env, GCConstants.API_CLOSE_RECEIPT,
		    docStrtRcptIn);
	} catch (Exception e) {
	    throw new GCException(e);
	}
	LOGGER.debug("Class: GCReceivePurchaseOrder Method: callCloseReceipt - END");
    }

    /**
     * 
     * Description of callGetOrderLineListForBundle - This method is used to
     * call getOrderLineList API for Bundle parent item.
     * 
     * @param env
     *            YFSEnvironment
     * @param sOrdrLnKey
     *            OrderLineKey
     * @return Document
     * @throws Exception
     *             Exception
     * 
     */
    private Document callGetOrderLineListForBundle(YFSEnvironment env,
	    String sOrdrLnKey) throws GCException {
	LOGGER.debug("Class: GCReceivePurchaseOrder Method: callGetOrderLineListForBundle - BEGIN");
	Document docOut = null;
	try {
	    Document docIn = GCXMLUtil.createDocument(GCConstants.ORDER_LINE);
	    Element eleRoot = docIn.getDocumentElement();
	    Element eleBundleParLine = docIn
		    .createElement(GCConstants.BUNDLE_PARENT_LINE);
	    eleBundleParLine.setAttribute(GCConstants.ORDER_LINE_KEY,
		    sOrdrLnKey);
	    eleRoot.appendChild(eleBundleParLine);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("Input API call" + GCXMLUtil.getXMLString(docIn));
	    }
	    docOut = GCCommonUtil.invokeAPI(env,
		    GCConstants.API_GET_ORDER_LINE_LIST, docIn);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("getOrderlneList for bundle out Doc"
			+ GCXMLUtil.getXMLString(docOut));
	    }
	} catch (Exception e) {
	    throw new GCException(e);
	}
	LOGGER.debug("Class: GCReceivePurchaseOrder Method: callGetOrderLineListForBundle - END");
	return docOut;
    }

    /**
     * 
     * Description of callGetOrderLineListAPI - This method is used to call
     * getOrderLineList for a particular item.
     * 
     * @param env
     *            YFSEnvironment
     * @param sDocType
     *            DocumentType
     * @param sEntCode
     *            EnterpriseCode
     * @param sItemID
     *            ItemID
     * @param sOrderNo
     *            OrderNo
     * @param sSubLineNo
     * @param sPrimeLineNo
     * @return
     * @throws Exception
     * 
     */
    private Document callGetOrderLineListAPI(YFSEnvironment env,
	    String sDocType, String sEntCode, String sItemID, String sOrderNo,
	    String sPrimeLineNo, String sSubLineNo) throws GCException {
	LOGGER.debug("Class: GCReceivePurchaseOrder Method: callGetOrderListAPI - BEGIN");
	Document docOut = null;
	try {
	    Document docIn = GCXMLUtil.createDocument(GCConstants.ORDER_LINE);
	    Element eleRoot = docIn.getDocumentElement();
	    eleRoot.setAttribute(GCConstants.DOCUMENT_TYPE, sDocType);
	    eleRoot.setAttribute(GCConstants.ENTERPRISE_CODE, sEntCode);
	    eleRoot.setAttribute(GCConstants.PRIME_LINE_NO, sPrimeLineNo);
	    eleRoot.setAttribute(GCConstants.SUB_LINE_NO, sSubLineNo);

	    Element eleItem = docIn.createElement(GCConstants.ITEM);
	    eleItem.setAttribute(GCConstants.ITEM_ID, sItemID);

	    Element eleOrder = docIn.createElement(GCConstants.ORDER);
	    eleOrder.setAttribute(GCConstants.ORDER_NO, sOrderNo);
	    eleRoot.appendChild(eleItem);
	    eleRoot.appendChild(eleOrder);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("getOrderlineList In Doc"
			+ GCXMLUtil.getXMLString(docIn));
	    }
	    docOut = GCCommonUtil.invokeAPI(env,
		    GCConstants.API_GET_ORDER_LINE_LIST, docIn);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("getOrderlneList out Doc"
			+ GCXMLUtil.getXMLString(docOut));
	    }
	} catch (Exception e) {
	    throw new GCException(e);
	}
	LOGGER.debug("Class: GCReceivePurchaseOrder Method: callGetOrderListAPI - END");
	return docOut;
    }

    /**
     * 
     * Description of callStartReceipt - This method is used to invoke
     * startReceipt API.
     * 
     * @param env
     *            YFSEnvironment
     * @param eleRoot
     *            Root element of input Document
     * @param eleShip
     *            Shipment Element
     * @return Document
     * @throws Exception
     * 
     */
    private Document callStartReceipt(YFSEnvironment env, Element eleRoot,
	    Element eleShip,String sShipmentNo) throws GCException {
	LOGGER.debug("Class: GCReceivePurchaseOrder Method: callStartReceipt - BEGIN");
	Document docOut = null;
	try {
	    Document docStrtRcptIn = GCXMLUtil
		    .createDocument(GCConstants.RECEIPT);
	    Element eleDocRoot = docStrtRcptIn.getDocumentElement();
	    Element eleShipment = docStrtRcptIn
		    .createElement(GCConstants.SHIPMENT);
	    GCXMLUtil.copyAttributes(eleRoot, eleDocRoot);
	    GCXMLUtil.copyAttributes(eleShip, eleShipment);
	    eleDocRoot.appendChild(eleShipment);
	    if(sShipmentNo!=null){
	        eleShipment.setAttribute("ShipmentNo",sShipmentNo);
	    }
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("Input Doc to startReceipt API"
			+ GCXMLUtil.getXMLString(docStrtRcptIn));
	    }
	    docOut = GCCommonUtil.invokeAPI(env, GCConstants.API_START_RECEIPT,
		    docStrtRcptIn);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("API OutDoc " + GCXMLUtil.getXMLString(docOut));
	    }
	} catch (Exception e) {
	    throw new GCException(e);
	}
	LOGGER.debug("Class: GCReceivePurchaseOrder Method: callStartReceipt - END");
	return docOut;
    }

    @Override
    public void setProperties(Properties arg0) {

    }
}