/** 
 * Copyright © 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               20/02/2014        Last Name, First Name		OMS-238: As an OMS, I want to reserve inventory for Non-OMS orders placed outside of OMS
                                                                                     so that I won't oversell / over-promise items to other customers 
                                                                            OMS-236  As an OMS, I want to reserve inventory for OMS related orders placed outside of
                                                                                     OMS so that I won't oversell / over-promise items to other customers 
 *#################################################################################################################################################################
 */
package com.gc.api.inventory;

import java.util.Properties;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:naveen.s@expicient.com">Naveen</a>
 */
public class GCSyncInventoryDemand implements YIFCustomApi {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCSyncInventoryDemand.class.getName());

    @Override
    public void setProperties(Properties arg0) {

    }

    /**
     * 
     * Description of syncInventoryDemand
     * 
     * @param env
     * @param inDoc
     * @throws Exception
     * 
     */
    public void syncInventoryDemand(YFSEnvironment env, Document inDoc)
	    throws GCException {
	LOGGER.verbose("Class: GCSyncInventoryDemand Method: syncInventoryDemand BEGIN");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug("Incoming Document" + GCXMLUtil.getXMLString(inDoc));
	}
	try {
	    Element eleItemFirst = GCXMLUtil.getElementByXPath(inDoc,
		    GCConstants.SYNC_INVENTORY_DEMAND_ITEMS);
	    NodeList nodeListItems = GCXMLUtil.getNodeListByXpath(inDoc,
		    GCConstants.SYNC_INVENTORY_DEMAND_ITEMS_ITEM);
	    LOGGER.debug("nodeListItems Length=" + nodeListItems.getLength());
	    for (int i = 0; i < nodeListItems.getLength(); i++) {
		Element eleItem = (Element) nodeListItems.item(i);

		GCAdjustInventory obj = new GCAdjustInventory();
		Document docGtItmDtOt = obj.fetchingItemDetails(env, eleItem);

		// Fetching Kit Code and Extn Set Code
		String sKitCode = GCXMLUtil.getAttributeFromXPath(docGtItmDtOt,
			GCConstants.ITEMLIST_ITEM_PRIMARYINFO_KIT_CODE_XPATH);
		String sSetCode = GCXMLUtil.getAttributeFromXPath(docGtItmDtOt,
			GCConstants.ITEMLIST_ITEM_EXTN_SET_CODE_XPATH);
		// If Item is a bundle them exploding components.
		if ((GCConstants.BUNDLE).equalsIgnoreCase(sKitCode)) {
		    getComponentDetails(docGtItmDtOt, eleItem, inDoc, sSetCode,
			    eleItemFirst);

		}
		if (LOGGER.isDebugEnabled()) {
		    LOGGER.debug("INPUT DOC to API CALL"
			    + GCXMLUtil.getXMLString(inDoc));
		}
		GCCommonUtil.invokeAPI(env, "syncInventoryDemand", inDoc);
	    }
	} catch (Exception e) {
	    LOGGER.error(
		    "Exception in GCSyncInventoryDemand:syncInventoryDemand ",
		    e);
	    throw new GCException(e);
	}
	LOGGER.verbose("Class: GCSyncInventoryDemand Method: syncInventoryDemand END");
    }

    /**
     * 
     * Description of getComponentDetails - This method is used to replace
     * bundle item in the input xml with it components before the API call.
     * 
     * @param docGtItmDtOt
     *            Output document of getItemList API call.
     * @param eleItem
     *            Item Element
     * @param inDoc
     *            Input XML Document.
     * @param eleRoot
     *            Root element of Input Document.
     * @param sSetCode
     *            ExtnSetCode value.
     * @throws ParserConfigurationException
     *             ParserConfigurationException
     * @throws TransformerException
     *             TransformerException
     * 
     */
    private void getComponentDetails(Document docGtItmDtOt, Element eleItem,
	    Document inDoc, String sSetCode, Element eleItemFirst)
	    throws GCException {
	LOGGER.verbose("Class: GCSyncInventoryDemand Method: getComponentDetails BEGIN");
	try {
	    NodeList nodeListComponents = GCXMLUtil.getNodeListByXpath(
		    docGtItmDtOt,
		    GCConstants.ITEMLIST_ITEM_COMPONENTS_COMPONENT_XPATH);
	    LOGGER.debug("nodeListComponents Length="
		    + nodeListComponents.getLength());
	    Boolean booIsTrue = true;
	    for (int j = 0; j < nodeListComponents.getLength(); j++) {

		Element eleCmpnt = (Element) nodeListComponents.item(j);

		Element eleCompItem = inDoc.createElement(GCConstants.ITEM);
		Element eleCompDemand = inDoc.createElement(GCConstants.DEMAND);

		// Copying attributes of parent item to component item element
		GCXMLUtil.copyAttributes(eleItem, eleCompItem);
		if (booIsTrue) {

		    // Removing bundle item from the XML.
		    GCXMLUtil.removeChild(eleItemFirst, eleItem);
		    booIsTrue = false;

		}
		String strComItmID = eleCmpnt
			.getAttribute(GCConstants.COMPONENT_ITEM_ID);
		String strKitQty = eleCmpnt
			.getAttribute(GCConstants.KIT_QUANTITY);
		Element eleDemand = (Element) eleItem.getElementsByTagName(
			GCConstants.DEMAND).item(0);
		String sQty = eleDemand.getAttribute(GCConstants.QUANTITY);
		String sDemandShpDate = eleDemand
			.getAttribute(GCConstants.DEMAND_SHIP_DATE);
		String sDemandType = eleDemand
			.getAttribute(GCConstants.DEMAND_TYPE);
		String sOrgCode = eleDemand
			.getAttribute(GCConstants.ORGANIZATION_CODE);
		String sShipNode = eleDemand
			.getAttribute(GCConstants.SHIP_NODE);

		// Component ratio is multiplied against the quantity.
		Double dFinalQty = Double.parseDouble(strKitQty)
			* Double.parseDouble(sQty);
		String sfinalQty = dFinalQty.toString();

		eleCompDemand.setAttribute(GCConstants.QUANTITY, sfinalQty);
		eleCompDemand.setAttribute(GCConstants.DEMAND_SHIP_DATE,
			sDemandShpDate);
		eleCompDemand
			.setAttribute(GCConstants.DEMAND_TYPE, sDemandType);
		eleCompDemand.setAttribute(GCConstants.SHIP_NODE, sShipNode);
		eleCompItem.setAttribute(GCConstants.ITEM_ID, strComItmID);
		eleCompDemand.setAttribute(GCConstants.ORGANIZATION_CODE,
			sOrgCode);

		// If ExtnSetCode is Y then component's product class is changed
		// to
		// SET.
		if ((GCConstants.INT_ONE).equalsIgnoreCase(sSetCode)) {
		    eleCompItem.setAttribute(GCConstants.PRODUCT_CLASS,
			    GCConstants.SET);
		}
		eleCompItem.appendChild(eleCompDemand);
		eleItemFirst.appendChild(eleCompItem);
	    }
	} catch (Exception e) {
	    throw new GCException(e);
	}
	LOGGER.verbose("Class: GCSyncInventoryDemand Method: getComponentDetails END");
    }
}
