/** 
 * Copyright © 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
 *            1.0               11/02/2014        Gowda, Naveen	            OMS-242: As an OMS, I want to sychronize web inventory levels with HighJump so that 
 *                                                                                   I can provide accurate inventory availability to customers 
 *#################################################################################################################################################################
 */
package com.gc.api.inventory;

import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:naveen.s@expicient.com">Naveen</a>
 */
public class GCSyncLoadedInventory implements YIFCustomApi {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCSyncLoadedInventory.class.getName());

    /**
     * 
     * Description of syncLoadedInventory - This method is used to call
     * syncLoadedInventory API after validating MsgID's existence in DB.
     * 
     * @param env
     *            YFSEnvironment
     * @param inDoc
     *            Input XML Document
     * @throws Exception
     *             Exception
     * 
     */
    public void syncLoadedInventory(YFSEnvironment env, Document inDoc)
	    throws GCException {

	LOGGER.debug("Class: GCSyncLoadedInventory Method: syncLoadedInventory BEGIN");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug("Incoming Document" + GCXMLUtil.getXMLString(inDoc));
	}
	try {
	    Element eleRoot = inDoc.getDocumentElement();

	    String shipNode = eleRoot.getAttribute(GCConstants.SHIP_NODE);
	    String yantraGrpMsgID = eleRoot
		    .getAttribute(GCConstants.YANTRA_GROUP_MESSAGE_ID);

	    // Preparing input document for API call.
	    Document inputDoc = GCXMLUtil.createDocument(GCConstants.INVENTORY);
	    Element eleInputDocRoot = inputDoc.getDocumentElement();

	    eleInputDocRoot.setAttribute(GCConstants.APPLY_DIFFERENCES,
		    GCConstants.FLAG_N);
	    // Setting mandatory attributes for API call.
	    eleInputDocRoot.setAttribute(GCConstants.SHIP_NODE, shipNode);
	    eleInputDocRoot.setAttribute(GCConstants.YANTRA_GROUP_MESSAGE_ID,
		    yantraGrpMsgID);
	    eleInputDocRoot.setAttribute("CompleteInventoryFlag",
		    GCConstants.FLAG_Y);
	    eleInputDocRoot.setAttribute("SynchNonOnHandInventory",
		    GCConstants.FLAG_N);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("Sync Loaded Inventory API Input"
			+ GCXMLUtil.getXMLString(inputDoc));
	    }
	    // Call syncLoadedInventory with this input doc.
	    GCCommonUtil.invokeAPI(env, GCConstants.API_SYNC_LOADED_INVENTORY,
		    inputDoc);
	} catch (Exception e) {
	    LOGGER.error(
		    "Exception GCSyncLoadedInventory:syncLoadedInventory ", e);
	    throw new GCException(e);
	}
	LOGGER.debug("Class: GCSyncLoadedInventory Method: syncLoadedInventory END");
    }

    @Override
    public void setProperties(Properties arg0) {

    }
}
