/** 
 * Copyright © 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
 *            1.0               11/02/2014        Gowda, Naveen             OMS-242: As an OMS, I want to sychronize web inventory levels with HighJump so that 
 *                                                                                   I can provide accurate inventory availability to customers 
 *            1.1               25/02/2015        Kumar, Rakesh             OMS-4958: Production: Inventory sync messages not being consumed by OMS                                  
 *#################################################################################################################################################################
 */
package com.gc.api.inventory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.shared.ycp.YFSContext;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:naveen.s@expicient.com">Naveen</a>
 */
public class GCSyncLoadedInventoryV1 implements YIFCustomApi {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
            .instance(GCSyncLoadedInventoryV1.class);

  /**
   *
   * Description of syncLoadedInventory - This method is used to call
   * syncLoadedInventory API after validating MsgID's existence in DB.
   *
   * @param env
   *            YFSEnvironment
   * @param inDoc
   *            Input XML Document
   * @throws Exception
   *             Exception
   *
   */
  public void syncLoadedInventory(YFSEnvironment env, Document inDoc)
      throws GCException {
    LOGGER.debug("Class: GCSyncLoadedInventory Method: syncLoadedInventory BEGIN");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Incoming Document" + GCXMLUtil.getXMLString(inDoc));
    }
    PreparedStatement objStatement = null;
    ResultSet objResultSet = null;
    Connection con = null;

        try {
            Element eleRoot = inDoc.getDocumentElement();
            boolean isCompAvailable = false;
            String shipNode = eleRoot.getAttribute(GCConstants.SHIP_NODE);
            String yantraGrpMsgID = eleRoot
                    .getAttribute(GCConstants.YANTRA_GROUP_MESSAGE_ID);

      // setting up the connection with database
      con = ((YFSContext) env)
          .getConnectionForTableType(GCConstants.GUITAR);
      String sStatement="select SUM(quantity) Quantity , item_id , product_class , uom , "
              + "availability_type , supply_type from GC_COMP_ITEMS_FOR_INV_SYNC "
              + "where shipnode_key= ? and yantra_message_group_id= ? "
              + "GROUP BY item_id , product_class , uom , availability_type , supply_type";
      objStatement = prepareStatement(con, sStatement);
      objStatement.setString(1, shipNode);
      objStatement.setString(2, yantraGrpMsgID);
      objResultSet = objStatement.executeQuery();
      if (objResultSet!=null) {
        isCompAvailable = true;
        Document inDocItms=createDocument(shipNode,yantraGrpMsgID);
        Element eleInputDocRoot = inDocItms
            .getDocumentElement();
        int counter=0;
        boolean bAPIExecutionPending=true;
        while(objResultSet.next()){
          Element eleItem = inDocItms
              .createElement(GCConstants.ITEM);
          eleItem.setAttribute("InventoryOrganizationCode",
              "GCI");
          eleItem.setAttribute("ItemID",
              objResultSet.getString("item_id"));
          eleItem.setAttribute("ProductClass",
              objResultSet.getString("product_class"));
          eleItem.setAttribute("UnitOfMeasure",
              objResultSet.getString("uom") );

                    Element eleCompSupplies = inDocItms
                            .createElement(GCConstants.SUPPLIES);
                    Element eleCompSupply = inDocItms
                            .createElement(GCConstants.SUPPLY);
                    eleCompSupply.setAttribute("Quantity",
                            objResultSet.getString("Quantity"));
                    eleCompSupply.setAttribute("AvailabilityType",
                            objResultSet.getString("availability_type"));
                    eleCompSupply.setAttribute("SupplyType",
                            objResultSet.getString("supply_type"));
                    eleCompSupplies.appendChild(eleCompSupply);
                    eleItem.appendChild(eleCompSupplies);
                    eleInputDocRoot.appendChild(eleItem);
                    counter++;
                    bAPIExecutionPending=true;
                    if(counter%1500==0){
                        if (LOGGER.isDebugEnabled()) {
                            LOGGER.debug("*****Load Inventory  Mismatch Input*****"
                                    + GCXMLUtil.getXMLString(inDocItms));
                        }
                        GCCommonUtil.invokeAPI(env,
                                GCConstants.API_LOAD_INVENTORY_MISMATCH,
                                inDocItms);
                        bAPIExecutionPending=false;
                        inDocItms=null;
                        inDocItms=createDocument(shipNode,yantraGrpMsgID);
                        eleInputDocRoot = inDocItms
                                .getDocumentElement();

                    }
                }
                if(bAPIExecutionPending){
                    if (LOGGER.isDebugEnabled()) {
                        LOGGER.debug("*****Load Inventory  Mismatch Input*****"
                                + GCXMLUtil.getXMLString(inDocItms));
                    }
                    GCCommonUtil.invokeAPI(env,
                            GCConstants.API_LOAD_INVENTORY_MISMATCH,
                            inDocItms);

                }
            }
            // Preparing input document for API call.
            Document inputDoc = GCXMLUtil.createDocument(GCConstants.INVENTORY);
            Element eleInputDocRoot = inputDoc.getDocumentElement();

            eleInputDocRoot.setAttribute(GCConstants.APPLY_DIFFERENCES,
                    GCConstants.FLAG_Y);
            // Setting mandatory attributes for API call.
            eleInputDocRoot.setAttribute(GCConstants.SHIP_NODE, shipNode);
            eleInputDocRoot.setAttribute(GCConstants.YANTRA_GROUP_MESSAGE_ID,
                    yantraGrpMsgID);
            eleInputDocRoot.setAttribute("CompleteInventoryFlag",
                    GCConstants.FLAG_Y);
            eleInputDocRoot.setAttribute("SynchNonOnHandInventory",
                    GCConstants.FLAG_N);
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Sync Loaded Inventory API Input"
                        + GCXMLUtil.getXMLString(inputDoc));
            }
            if (isCompAvailable) {
                GCCommonUtil.invokeService(env, "PostMsgForInv", inputDoc);
                LOGGER.debug("Class: GCSyncLoadedInventory Method: syncLoadedInventory END --Posted to a Queue");
            } else {
                GCCommonUtil.invokeAPI(env,
                        GCConstants.API_SYNC_LOADED_INVENTORY, inputDoc);
                LOGGER.debug("Class: GCSyncLoadedInventory Method: syncLoadedInventory END --Did not post to queue");
            }
        } catch (Exception e) {
            LOGGER.error(
                    "Exception in GCSyncLoadedInventory:syncLoadedInventory ",
                    e);
            throw new GCException(e);
        }finally {
            try {
                if(objResultSet!=null){
                    objResultSet.close();
                }
                if(objStatement!=null){
                    objStatement.close();
                }
            } catch (SQLException e) {
                LOGGER.error(
                        "SQLException in GCSyncLoadedInventory:syncLoadedInventory ",
                        e);
                throw new GCException(e);
            }

        }

    }

	private PreparedStatement prepareStatement(Connection con, String sStatement)
			throws SQLException {
		PreparedStatement objStatement;
		objStatement = con.prepareStatement(sStatement);
		return objStatement;
	}
	
    private Document createDocument(String shipNode,String yantraGrpMsgID){
        LOGGER.debug("Entering inside GCSyncLoadedInventoryV1.createDocument(): ShipNode="+shipNode+" yantraGrpMsgID="+yantraGrpMsgID);
        Document inDocItms = GCXMLUtil.createDocument("Items");
        Element eleInputDocRoot = inDocItms
                .getDocumentElement();
        eleInputDocRoot.setAttribute(
                GCConstants.VALIDATE_ITEMS, GCConstants.FLAG_N);
        eleInputDocRoot.setAttribute(
                GCConstants.APPLY_DIFFERENCES,
                GCConstants.FLAG_Y);
        eleInputDocRoot.setAttribute(GCConstants.SHIP_NODE,
                shipNode);
        eleInputDocRoot.setAttribute(
                GCConstants.YANTRA_GROUP_MESSAGE_ID,
                yantraGrpMsgID);
        LOGGER.debug("Exiting from GCSyncLoadedInventoryV1.createDocument()");
        return inDocItms;
    }
    @Override
    public void setProperties(Properties arg0) {

    }
}
