package com.gc.api.inventory;

import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * GCSTORE-73 - POS Inventory Sync. This class is used to receive EOF during POS inventory sync and
 * update Supply in OMS
 *
 * This class gets EOF message for Inventory sync update from POS and call OOB API
 * syncLoadedInventory to udpate YFS_INVENTORY_SUPPLY table in OMS
 *
 * @author gunjankumar
 *
 */

public class GCSyncPOSInventoryAPI implements YIFCustomApi {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCSyncPOSInventoryAPI.class.getName());

  /**
   * This method receives EOF message from POS, wait for configurable no. of seconds and calls OOB
   * API syncLoadedInventory
   *
   * @param env Environment variable
   * @param inputDoc Input document coming from POS i.e. EOF message
   * @throws InterruptedException Exception thrown if interruption during wait time
   */
  public void syncPOSInventory(YFSEnvironment env, Document inputDoc) {

    LOGGER.beginTimer("GCSyncPOSInventoryAPI.syncPOSInventory");
    LOGGER.verbose("Class: GCSyncPOSInventoryAPI Method: syncPOSInventory BEGIN");

    Element inDocEle = inputDoc.getDocumentElement();

    YFCDocument yfcSyncLoadedInDoc = YFCDocument.createDocument(GCConstants.INVENTORY);
    YFCElement yfcSyncLoadedInEle = yfcSyncLoadedInDoc.getDocumentElement();
    yfcSyncLoadedInEle
    .setAttribute(GCConstants.APPLY_DIFFERENCES, GCConstants.FLAG_N);
    yfcSyncLoadedInEle.setAttribute(GCConstants.COMPLETE_INVENTORY,
        inDocEle.getAttribute(GCConstants.COMPLETE_INVENTORY));
    yfcSyncLoadedInEle.setAttribute(GCConstants.SHIP_NODE, inDocEle.getAttribute(GCConstants.SHIP_NODE));
    yfcSyncLoadedInEle.setAttribute(GCConstants.YANTRA_GROUP_MESSAGE_ID,
        inDocEle.getAttribute(GCConstants.YANTRA_GROUP_MESSAGE_ID));    
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("\n API_SYNC_INVENTORY_MISMATCH Input Doc"
          + YFCDocument.getDocumentFor(yfcSyncLoadedInDoc.getDocument()));
    }
    GCCommonUtil.invokeAPI(env, GCConstants.API_SYNC_LOADED_INVENTORY, yfcSyncLoadedInDoc.getDocument());
    LOGGER.verbose("Exiting GCSyncPOSInventoryAPI.syncPOSInventory() method");
    LOGGER.endTimer("GCSyncPOSInventoryAPI.syncPOSInventory");
  }


  @Override
  public void setProperties(Properties arg0) {
  }
}

