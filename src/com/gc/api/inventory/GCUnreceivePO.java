/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               06/26/2014        Mittal, Yashu		JIRA No: Description of issue.
 *#################################################################################################################################################################
 */
package com.gc.api.inventory;

import java.util.List;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:yashu.mittal@expicient.com">Yashu Mittal</a>
 */
public class GCUnreceivePO implements YIFCustomApi {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCUnreceivePO.class.getName());

    public void unreceivePO(YFSEnvironment env, Document inDoc)
	    throws GCException {
	try {
	    String sExtnDAXOrderNo = GCXMLUtil.getAttributeFromXPath(inDoc,
		    "Receipt/Extn/@ExtnDAXOrderNo");
	    // Finding orderheaderkey from ExtnDAXOrderNo
	    Document getOrderList = GCCommonUtil
		    .invokeAPI(
			    env,
			    GCConstants.GET_ORDER_LIST,
			    GCXMLUtil
				    .getDocument("<Order><Extn ExtnDAXOrderNo='"
					    + sExtnDAXOrderNo + "'/></Order>"),
			    GCXMLUtil
				    .getDocument("<OrderList><Order OrderHeaderKey=''/></OrderList>"));
	    String sOrderHeaderKey = GCXMLUtil.getAttributeFromXPath(
		    getOrderList,
		    GCConstants.ORDERLIST_ORDER_ORDERHEADERKEY_XPATH);
	    LOGGER.debug("OrderHeaderKey is : " + sOrderHeaderKey);
	    // Creating getOrderlineList I/P
	    Document getOrderLineListIn = GCXMLUtil
		    .createDocument(GCConstants.ORDER_LINE);
	    Element getOrderLineListInRootElement = getOrderLineListIn
		    .getDocumentElement();
	    Element eleItem = getOrderLineListIn
		    .createElement(GCConstants.ITEM);
	    getOrderLineListInRootElement.appendChild(eleItem);
	    Element eleOrder = getOrderLineListIn
		    .createElement(GCConstants.ORDER);
	    getOrderLineListInRootElement.appendChild(eleOrder);
	    eleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
	    Element eleReceiptLines = GCXMLUtil.getElementByXPath(inDoc,
		    GCConstants.RECEIPT_RECEIPTLINES_XPATH);
	    // Looping on inDoc Receipt Lines, finding orderline key and
	    // appending
	    // each to docReceiptLine for further processing.
	    List<Element> inDocReceiptLineList = GCXMLUtil
		    .getElementListByXpath(inDoc,
			    GCConstants.RECEIPT_RECEIPTLINES_RECEIPTLINE_XPATH);
	    for (Element inDocReceiptLine : inDocReceiptLineList) {
		String sItemID = inDocReceiptLine
			.getAttribute(GCConstants.ITEM_ID);
		String sPrimeLineNo = inDocReceiptLine
			.getAttribute(GCConstants.PRIME_LINE_NO);
		String sSubLineNo = inDocReceiptLine
			.getAttribute(GCConstants.SUB_LINE_NO);
		String sUnreceiveQty = inDocReceiptLine
			.getAttribute(GCConstants.UNRECEIVE_QTY);
		// Calling getOrderLineList API for the particular item.
		Document docOut = callGetOrderLineListAPI(env, sItemID,
			sOrderHeaderKey, sPrimeLineNo, sSubLineNo);
		String sOrderedQty = GCXMLUtil.getAttributeFromXPath(docOut,
			GCConstants.ORDERLINELIST_ORDERLINE_ORDEREDQTY_ATTR);
		String sOrdrLnKey = GCXMLUtil.getAttributeFromXPath(docOut,
			GCConstants.ORDERLINELIST_ORDERLINE_ORDERLINEKEY_ATTR);
		String sKitCode = GCXMLUtil.getAttributeFromXPath(docOut,
			GCConstants.ORDERLINELIST_ORDERLINE_KITCODE_ATTR);
		if ((GCConstants.BUNDLE).equalsIgnoreCase(sKitCode)) {
		    // If item is a bundle remove the bundle item from the
		    // receipt
		    // lines and replace with components
		    Document docGetOrdrLnLstOut = callGetOrderLineListForBundle(
			    env, sOrdrLnKey, sOrderHeaderKey);
		    GCXMLUtil.removeChild(eleReceiptLines, inDocReceiptLine);
		    List<Element> orderLineList = GCXMLUtil
			    .getElementListByXpath(docGetOrdrLnLstOut,
				    GCConstants.ORDERLINELIST_ORDERLINE);
		    for (Element eleOrderLine : orderLineList) {
			String sOrderLineKey = eleOrderLine
				.getAttribute(GCConstants.ORDER_LINE_KEY);
			String sComQuanity = eleOrderLine
				.getAttribute(GCConstants.ORDERED_QUANITY);
			Double dFinalQty = (Double.parseDouble(sComQuanity) * Double
				.parseDouble(sUnreceiveQty))
				/ Double.parseDouble(sOrderedQty);
			String sFinalUnreceiveQty = dFinalQty.toString();
			LOGGER.debug("Component Qty is " + sFinalUnreceiveQty);
			Element eleReceiptLine = inDoc
				.createElement(GCConstants.RECEIPT_LINE);
			eleReceiptLine.setAttribute(GCConstants.ORDER_LINE_KEY,
				sOrderLineKey);
			eleReceiptLine.setAttribute(GCConstants.UNRECEIVE_QTY,
				sFinalUnreceiveQty);
			eleReceiptLines.appendChild(eleReceiptLine);
		    }
		} else {
		    inDocReceiptLine.setAttribute(GCConstants.ORDER_LINE_KEY,
			    sOrdrLnKey);
		}
	    }
	    getReceiptLineKey(env, inDoc, sOrderHeaderKey);
	} catch (Exception e) {
	    LOGGER.error("Exception in GCUnreceivePO.unreceivePO ", e);
	    throw new GCException(e);
	}
    }

    /**
     * Description of getReceiptLineKey : This method get the receipt line key
     * of each orderline and append them in unreceiveOrder i/p.
     * 
     * @param arg0
     * @throws Exception
     * @see com.yantra.interop.japi.YIFCustomApi#setProperties(java.util.Properties)
     */

    public void getReceiptLineKey(YFSEnvironment env, Document inDoc,
	    String sOrderHeaderKey) throws GCException {
	// Creating I/P for unreceiveOrder
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug("Input to getReceiptLine key method : "
		    + GCXMLUtil.getXMLString(inDoc));
	}
	try {
	    Document unreceiveOrderIn = GCXMLUtil
		    .createDocument(GCConstants.RECEIPT);
	    Element unreceiveOrderRootEle = unreceiveOrderIn
		    .getDocumentElement();
	    Element eleShipment = unreceiveOrderIn
		    .createElement(GCConstants.SHIPMENT);
	    eleShipment.setAttribute(GCConstants.ORDER_HEADER_KEY,
		    sOrderHeaderKey);
	    unreceiveOrderRootEle.appendChild(eleShipment);
	    Element eleReceiptLines = unreceiveOrderIn
		    .createElement(GCConstants.RECEIPT_LINES);
	    unreceiveOrderRootEle.appendChild(eleReceiptLines);

	    // Looping on inDoc to append receipt line key on unreceive order
	    // i/p.
	    List<Element> inDocReceiptLineList = GCXMLUtil
		    .getElementListByXpath(inDoc,
			    GCConstants.RECEIPT_RECEIPTLINES_RECEIPTLINE_XPATH);
	    for (Element inDocReceiptLine : inDocReceiptLineList) {
		String sOrderLineKey = inDocReceiptLine
			.getAttribute(GCConstants.ORDER_LINE_KEY);
		String sUnreceiveQty = inDocReceiptLine
			.getAttribute(GCConstants.UNRECEIVE_QTY);
		Document getReceiptLineListIn = GCXMLUtil
			.getDocument("<ReceiptLine OrderLineKey='"
				+ sOrderLineKey + "' OrderHeaderKey='"
				+ sOrderHeaderKey + "'/>");
		Document getReceiptLineListOut = GCCommonUtil.invokeAPI(env,
			GCConstants.API_GET_RECEIPT_LINE_LIST,
			getReceiptLineListIn);
		if (LOGGER.isDebugEnabled()) {
		    LOGGER.debug("getReceiptLineList Output"
			    + GCXMLUtil.getXMLString(getReceiptLineListOut));
		}
		// Looping on all receipts for the line and unreceiving from the
		// line having sufficient qty.
		List<Element> eleReceiptLineList = GCXMLUtil
			.getElementListByXpath(getReceiptLineListOut,
				"/ReceiptLineList/ReceiptLine");
		for (Element eleReceiptLineIn : eleReceiptLineList) {
		    String sQty = eleReceiptLineIn
			    .getAttribute(GCConstants.QUANTITY);
		    LOGGER.debug("Quantity is :" + sQty);
		    LOGGER.debug("UnreceiveQuantity is :" + sUnreceiveQty);
		    if (Double.parseDouble(sQty) >= Double
			    .parseDouble(sUnreceiveQty)) {
			String sReceiptHeaderKey = eleReceiptLineIn
				.getAttribute(GCConstants.RECEIPT_HEADER_KEY);
			String sReceiptLineKey = eleReceiptLineIn
				.getAttribute(GCConstants.RECEIPT_LINE_KEY);
			Element eleReceiptLine = unreceiveOrderIn
				.createElement(GCConstants.RECEIPT_LINE);
			eleReceiptLine.setAttribute(
				GCConstants.RECEIPT_LINE_KEY, sReceiptLineKey);
			eleReceiptLine.setAttribute(GCConstants.UNRECEIVE_QTY,
				sUnreceiveQty);
			unreceiveOrderRootEle.setAttribute(
				GCConstants.RECEIPT_HEADER_KEY,
				sReceiptHeaderKey);
			eleReceiptLines.appendChild(eleReceiptLine);
			break;
		    }
		}
	    }
	    // Calling unreceive order
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("Input to unreceiveOrder"
			+ GCXMLUtil.getXMLString(unreceiveOrderIn));
	    }
	    GCCommonUtil.invokeAPI(env, GCConstants.API_UNRECEIVE_ORDER,
		    unreceiveOrderIn);
	} catch (Exception e) {
	    throw new GCException(e);
	}
    }

    /**
     * 
     * Description of callGetOrderLineListForBundle - This method is used to
     * call getOrderLineList API for Bundle parent item.
     * 
     * @param env
     *            YFSEnvironment
     * @param sOrdrLnKey
     *            OrderLineKey
     * @return Document
     * @throws Exception
     *             Exception
     * 
     */
    private Document callGetOrderLineListForBundle(YFSEnvironment env,
	    String sOrdrLnKey, String sOrderHeaderKey) throws GCException {
	LOGGER.verbose("Class: GCReceivePurchaseOrder Method: callGetOrderLineListForBundle - BEGIN");
	Document docOut = null;
	try {
	    Document docIn = GCXMLUtil.createDocument(GCConstants.ORDER_LINE);
	    Element eleRoot = docIn.getDocumentElement();
	    Element eleBundleParLine = docIn
		    .createElement(GCConstants.BUNDLE_PARENT_LINE);
	    eleBundleParLine.setAttribute(GCConstants.ORDER_LINE_KEY,
		    sOrdrLnKey);
	    eleRoot.appendChild(eleBundleParLine);
	    Element eleOrder = docIn.createElement(GCConstants.ORDER);
	    eleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
	    eleRoot.appendChild(eleOrder);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("Input API call" + GCXMLUtil.getXMLString(docIn));
	    }
	    docOut = GCCommonUtil.invokeAPI(env,
		    GCConstants.API_GET_ORDER_LINE_LIST, docIn);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("getOrderlneList for bundle out Doc"
			+ GCXMLUtil.getXMLString(docOut));
	    }
	} catch (Exception e) {
	    throw new GCException(e);
	}
	LOGGER.verbose("Class: GCReceivePurchaseOrder Method: callGetOrderLineListForBundle - END");
	return docOut;
    }

    /**
     * 
     * Description of callGetOrderLineListAPI - This method is used to call
     * getOrderLineList for a particular item.
     * 
     * @param env
     *            YFSEnvironment
     * @param sDocType
     *            DocumentType
     * @param sEntCode
     *            EnterpriseCode
     * @param sItemID
     *            ItemID
     * @param sOrderNo
     *            OrderNo
     * @param sSubLineNo
     * @param sPrimeLineNo
     * @return
     * @throws Exception
     * 
     */
    private Document callGetOrderLineListAPI(YFSEnvironment env,
	    String sItemID, String sOrderHeaderKey, String sPrimeLineNo,
	    String sSubLineNo) throws GCException {
	LOGGER.verbose("Class: GCReceivePurchaseOrder Method: callGetOrderListAPI - BEGIN");
	Document docOut = null;
	try {
	    Document docIn = GCXMLUtil.createDocument(GCConstants.ORDER_LINE);
	    Element eleRoot = docIn.getDocumentElement();
	    eleRoot.setAttribute(GCConstants.PRIME_LINE_NO, sPrimeLineNo);
	    eleRoot.setAttribute(GCConstants.SUB_LINE_NO, sSubLineNo);

	    Element eleItem = docIn.createElement(GCConstants.ITEM);
	    eleItem.setAttribute(GCConstants.ITEM_ID, sItemID);

	    Element eleOrder = docIn.createElement(GCConstants.ORDER);
	    eleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
	    eleRoot.appendChild(eleItem);
	    eleRoot.appendChild(eleOrder);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("getOrderlineList In Doc"
			+ GCXMLUtil.getXMLString(docIn));
	    }
	    docOut = GCCommonUtil.invokeAPI(env,
		    GCConstants.API_GET_ORDER_LINE_LIST, docIn);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("getOrderlneList out Doc"
			+ GCXMLUtil.getXMLString(docOut));
	    }
	} catch (Exception e) {
	    throw new GCException(e);
	}
	LOGGER.verbose("Class: GCReceivePurchaseOrder Method: callGetOrderListAPI - END");
	return docOut;
    }

    /**
     * Description of setProperties
     * 
     * @param arg0
     * @throws Exception
     * @see com.yantra.interop.japi.YIFCustomApi#setProperties(java.util.Properties)
     */
    @Override
    public void setProperties(Properties arg0) {
    }

}
