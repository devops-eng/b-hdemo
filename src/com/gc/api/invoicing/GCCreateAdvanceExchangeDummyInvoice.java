package com.gc.api.invoicing;

import org.w3c.dom.Document;
import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCCreateAdvanceExchangeDummyInvoice {
  private static final YFCLogCategory LOGGER =
      YFCLogCategory.instance(GCCreateAdvanceExchangeDummyInvoice.class.getName());

  public void exchangeOrderInvoice(YFSEnvironment env, Document inDoc) {
    LOGGER.verbose("GCCreateAdvanceExchangeInvoice - exchangeOrderInvoice() : Start");
    LOGGER.verbose("GCCreateAdvanceExchangeInvoice - exchangeOrderInvoice() inDoc" + GCXMLUtil.getXMLString(inDoc));

    YFCDocument shipmentInvoice = YFCDocument.getDocumentFor(inDoc);
    String sOrderHeaderKey =
        shipmentInvoice.getElementsByTagName("OrderInvoice").item(0).getAttribute(GCConstants.ORDER_HEADER_KEY);
    YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "'/>");
    YFCDocument getOrderListTemplate = YFCDocument.getDocumentFor(
        "<OrderList><Order TotalAmount='' OrderPurpose=''><OrderLines><OrderLine OrderLineKey='' BundleParentOrderLineKey='' KitCode='' ><Schedules><Schedule ShipNode=''/></Schedules></OrderLine></OrderLines><ReturnOrdersForExchange><ReturnOrderForExchange OrderHeaderKey=''/>"
            + "</ReturnOrdersForExchange></Order> </OrderList>");
    YFCDocument getOrderList =
        GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST, getOrderListInput, getOrderListTemplate);
    
    LOGGER.verbose("GCCreateAdvanceExchangeInvoice - exchangeOrderInvoice() getOrderList Output" + GCXMLUtil.getXMLString(getOrderList.getDocument()));
    
    YFCElement eleOrder = getOrderList.getElementsByTagName(GCConstants.ORDER).item(0);
    String sOrderPurpose = eleOrder.getAttribute("OrderPurpose");

    if (YFCCommon.equalsIgnoreCase(sOrderPurpose, "EXCHANGE")) {
      LOGGER.verbose("Shipment Invoice is for Exchange orders");

      String sInvoiceNo = shipmentInvoice.getDocumentElement().getAttribute(GCConstants.INVOICE_NO);
      YFCDocument getOrderInvoiceDetailsInput =
          YFCDocument.getDocumentFor("<GetOrderInvoiceDetails EnterpriseCode='GC' InvoiceNo='" + sInvoiceNo + "'/>");
      YFCDocument getOrderInvoiceDetailsTemplate = YFCDocument.getDocumentFor(
          GCCommonUtil.getDocumentFromPath("/global/template/api/getOrderInvoiceDetails_ExchangeOrder.xml"));

      YFCDocument getOrderInvoiceDetails = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_INVOICE_DETAILS,
          getOrderInvoiceDetailsInput, getOrderInvoiceDetailsTemplate);
      
      LOGGER.verbose("GCCreateAdvanceExchangeInvoice - exchangeOrderInvoice() getOrderInvoiceDetails Output" + GCXMLUtil.getXMLString(getOrderInvoiceDetails.getDocument()));
      
      String orderLineKey = "" ;
      String shipNode = "";
      YFCElement  eleInput = getOrderInvoiceDetails.getDocumentElement();
      YFCElement eleInvoiceHeader = eleInput.getChildElement(GCConstants.INVOICE_HEADER);
      YFCElement eleLineDetails = eleInvoiceHeader.getChildElement(GCConstants.LINE_DETAILS);
      YFCNodeList<YFCElement> nlLineDetail = eleLineDetails.getElementsByTagName(GCConstants.LINE_DETAIL);
      for(YFCElement eleLineDetail : nlLineDetail){
        orderLineKey = eleLineDetail.getAttribute(GCConstants.ORDER_LINE_KEY);
          shipNode =
              GCXMLUtil.getAttributeFromXPath(getOrderInvoiceDetails.getDocument(),
                  "InvoiceDetail/InvoiceHeader /Order/OrderLines/OrderLine[@OrderLineKey='" + orderLineKey
                  + "']/Schedules/Schedule/@ShipNode");
          
          LOGGER.verbose("ShipNode Stamped from the Output of InvoiceDetails :" +shipNode);
          
          if (!YFCCommon.isVoid(shipNode)) {
            break;
          }
      }
      if (YFCCommon.isVoid(shipNode)) {
        String parentOrderLineKey =
            GCXMLUtil.getAttributeFromXPath(getOrderInvoiceDetails.getDocument(),
                "InvoiceDetail/InvoiceHeader/LineDetails/LineDetail/OrderLine[@KitCode='BUNDLE']/@OrderLineKey");

        LOGGER.verbose("parentOrderLineKey Stamped " +parentOrderLineKey);
        
        shipNode =
            GCXMLUtil.getAttributeFromXPath(getOrderList.getDocument(),
                "OrderList/Order/OrderLines/OrderLine[@BundleParentOrderLineKey='"
                    + parentOrderLineKey
                    + "']/Schedules/Schedule/@ShipNode");
        
        LOGGER.verbose("ShipNode Stamped in case of SET Items " +shipNode);

        if(YFCCommon.isVoid(shipNode)){

        	String sChargeCategoryAdvanceCredit = GCXMLUtil.getAttributeFromXPath(getOrderInvoiceDetails.getDocument(), "InvoiceDetail/InvoiceHeader/HeaderCharges/HeaderCharge/@ChargeCategory");
        	if(!YFCCommon.isVoid(sChargeCategoryAdvanceCredit) && YFCCommon.equals(sChargeCategoryAdvanceCredit, "ADVANCE_CREDIT")){
        		shipNode = "MFI";
        	} else {
        		shipNode = GCXMLUtil.getAttributeFromXPath(getOrderInvoiceDetails.getDocument(), "//@ShipNode");
        	}
        }
      }


      if((!(YFCCommon.isVoid(shipNode)) && YFCCommon.equals(shipNode, "MFI"))){
          
          getOrderInvoiceDetails.getDocumentElement().setAttribute("ExchangeInvoiceType", "Dummy");

          LOGGER.verbose(
              "Dummy invoice being published to DAX is:- " + GCXMLUtil.getXMLString(getOrderInvoiceDetails.getDocument()));
    	  
          GCCommonUtil.invokeService(env, "GCDummyExcInvMsgPostToDAXService", getOrderInvoiceDetails.getDocument());

      }
      
    }
  }
}
