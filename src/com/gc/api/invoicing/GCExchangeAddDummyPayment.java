package com.gc.api.invoicing;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCExchangeAddDummyPayment {
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCExchangeAddDummyPayment.class.getName());

  public void addDummyPayment(YFSEnvironment env, Document inDoc) {
    LOGGER.verbose("GCCreateAdvanceExchangeInvoice - exchangeOrderInvoice() : Start");
    LOGGER.verbose("GCCreateAdvanceExchangeInvoice - exchangeOrderInvoice() inDoc" + GCXMLUtil.getXMLString(inDoc));

    YFCDocument holdDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleRoot = holdDoc.getDocumentElement();
    String sOrderHeaderKey = eleRoot.getAttribute(GCConstants.ORDER_HEADER_KEY);

    YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey ='" + sOrderHeaderKey + "'/>");
    YFCDocument getOrderListTemplate = YFCDocument.getDocumentFor(
        "<OrderList><Order OrderNo=''><Extn ExtnRetRecDummyAmount=''/><OrderHoldTypes><OrderHoldType HoldType='' Status=''/></OrderHoldTypes></Order></OrderList>");
    YFCDocument getOrderList =
        GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST, getOrderListInput, getOrderListTemplate);

    YFCNodeList<YFCElement> nlOrderHoldType = getOrderList.getElementsByTagName("OrderHoldType");
    for (int t = 0; t < nlOrderHoldType.getLength(); t++) {
      YFCElement eleOrderHoldType = nlOrderHoldType.item(t);
      String sStatus = eleOrderHoldType.getAttribute("Status");
      String sHoldType = eleOrderHoldType.getAttribute("HoldType");
      if (YFCCommon.equalsIgnoreCase(sHoldType, "Apply_Dummy_Hold") && YFCCommon.equals(sStatus, "1300")) {
        YFCElement eleExtn = getOrderList.getDocumentElement().getElementsByTagName("Extn").item(0);
        String sDummyAmount = eleExtn.getAttribute("ExtnRetRecDummyAmount");

    LOGGER.verbose("Final dummy amount=" + sDummyAmount);

        YFCDocument capturePaymentInput = YFCDocument.getDocumentFor("<CapturePayment OrderHeaderKey='"
            + sOrderHeaderKey + "'><PaymentMethods><PaymentMethod Operation='Manage' "
            + "PaymentType='Dummy_Ref_Exc' PaymentReference1='DUMMY' RequestedAmount='" + String.valueOf(sDummyAmount)
            + "'></PaymentMethod></PaymentMethods></CapturePayment>");

    LOGGER.verbose("Calling capturePayment API for dummy settlement");
    GCCommonUtil.invokeAPI(env, "capturePayment", capturePaymentInput.getDocument());

        // Calling request and execute collection API
        YFCDocument req_exe_Collection =
            YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "'/>");

    LOGGER.verbose("Calling request collection and execute APIs");
    GCCommonUtil.invokeAPI(env, "requestCollection", req_exe_Collection.getDocument());
    GCCommonUtil.invokeAPI(env, "executeCollection", req_exe_Collection.getDocument());
    GCCommonUtil.invokeAPI(env, "requestCollection", req_exe_Collection.getDocument());
      }
    }



  }
}
