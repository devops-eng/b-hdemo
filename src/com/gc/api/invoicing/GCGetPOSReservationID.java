/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 * *###########################################
 * ######################################################
 * ################################################################ OBJECTIVE: This class will
 * prepare input for POS reservation ID call, will invoke the call and get reservation ID from POS.
 * #################################################################################################
 * ################################################################ Version Date Modified By
 * Description
 * ######################################################################################
 * ########################################################################### 1.0 03/03/2015 Pande,
 * Anuj POS Reservation ID on backroom pick up
 * ######################################################
 * 2.0   14/06/2017  Gupta, Abhishek   GCSTORE - 6814
 * ############################################
 * ###############################################################
 */
package com.gc.api.invoicing;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class GCGetPOSReservationID {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCGetPOSReservationID.class.getName());
  boolean bAnyBundleLineExists = true;

  /**
   * This method will prepare input request message and invoke web service call to POS to fetch
   * reservation ID This is called from Exstore on backroom pick up
   *
   * @param env
   * @param inputXML
   * @return
   * @throws TransformerException
   */
  public Document invokeAndGetPOSReserveID(YFSEnvironment env, Document inputXML) throws TransformerException {
    LOGGER.beginTimer("GCGetPOSReservationID.invokeAndGetPOSReserveID()");
    LOGGER.verbose("GCGetPOSReservationID.invokeAndGetPOSReserveID() : Start");

    YFCDocument yfcDocInputXML = YFCDocument.getDocumentFor(inputXML);
    YFCElement yfcEleRootInput = yfcDocInputXML.getDocumentElement();

    String sShipmentKey = yfcEleRootInput.getAttribute(GCConstants.SHIPMENT_KEY);
    YFCDocument yfcDocGetShipmentListInput =
        YFCDocument.getDocumentFor("<Shipment ShipmentKey='" + sShipmentKey + "' />");
    YFCDocument yfcDocGetShipmentListOutput =
        YFCDocument.getDocumentFor(GCCommonUtil.invokeAPI(env, yfcDocGetShipmentListInput.getDocument(),
            GCConstants.GET_SHIPMENT_LIST, "global/template/api/getShipmentList_For_POSReservationID.xml"));
    /* Check if any line is fulfilled from MFI - Start */
    String setSplitOrderFlag = GCConstants.FALSE_STRING;
    if (!YFCCommon
        .isVoid(GCXMLUtil.getElementByXPath(
            yfcDocGetShipmentListOutput.getDocument(),
            "Shipments/Shipment/ShipmentLines/ShipmentLine/OrderLine/Order/OrderLines/OrderLine/Schedules/Schedule[@ShipNode='MFI']"))) {
      setSplitOrderFlag = GCConstants.TRUE_STRING;
    }
    yfcEleRootInput.setAttribute(GCXmlLiterals.SPLIT_ORDER_FLAG, setSplitOrderFlag);
    /* Check if any line is fulfilled from MFI - End */

    // Get order date and system date
    YFCElement yfcEleShipments = yfcDocGetShipmentListOutput.getDocumentElement();
    YFCElement yfcEleShipmentLine = yfcEleShipments.getElementsByTagName(GCConstants.SHIPMENT_LINE).item(0);
    YFCElement yfcEleOrderLine = yfcEleShipmentLine.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
    YFCElement yfcEleOrder = yfcEleOrderLine.getElementsByTagName(GCConstants.ORDER).item(0);

    Date dOrderDateTime = yfcEleOrder.getDateAttribute(GCConstants.ORDER_DATE);
    GCDateUtils.getDateTimeForPOS(yfcEleRootInput, dOrderDateTime);

    // Invoke service to form input XML for web service call
    yfcEleShipments.importNode(yfcEleRootInput);
    YFCElement eleShipmentLines = yfcEleShipments.getElementsByTagName(GCConstants.SHIPMENT_LINES).item(0);
    YFCNodeList<YFCElement> nlShipmentLine = eleShipmentLines.getElementsByTagName(GCConstants.SHIPMENT_LINE);
    for (int i = 0; i < nlShipmentLine.getLength(); i++) {
      YFCElement eleShipmentLine = nlShipmentLine.item(i);
      YFCElement eleOrderLine = eleShipmentLine.getChildElement(GCConstants.ORDER_LINE);
      String maxLineStatus = eleOrderLine.getAttribute(GCConstants.MAX_LINE_STATUS);
      if (YFCUtils.equals(maxLineStatus, "9000")
          || YFCUtils.equals(GCConstants.ZERO, eleShipmentLine.getAttribute(GCConstants.QUANTITY))) {
        eleShipmentLines.removeChild(eleShipmentLine);
        i--;
        continue;
      }
      YFCNodeList<YFCElement> nlLineTax = eleOrderLine.getElementsByTagName(GCConstants.LINE_TAX);
      YFCNodeList<YFCElement> nlLineTax1 = eleOrderLine.getElementsByTagName(GCConstants.LINE_TAX);
      for (YFCElement eleLineTax : nlLineTax) {
        Double taxPercentage = eleLineTax.getDoubleAttribute(GCConstants.TAX_PERCENTAGE);
        if (taxPercentage == 0.00) {
          for (YFCElement eleLineTax1 : nlLineTax1) {
            Double taxPer = eleLineTax1.getDoubleAttribute(GCConstants.TAX_PERCENTAGE);
            if (taxPer > 0.00) {
              eleLineTax.setAttribute(GCConstants.TAX_PERCENTAGE, taxPer);
              break;
            }
          }
        }
      }
    }
    // pro-rate bundle line price , Charges & taxes
    prorateBundleCharges(yfcDocGetShipmentListOutput.getDocument());

    /*
     * logic to stamp taxPercentage in case if there are multiple LineTaxes & tax percentage is zero
     * for some but not all. In this case we'll stamp any non-zero taxPercentage value from the
     * other LineTax elements
     */
    handleZeroTaxPercentage(yfcDocGetShipmentListOutput);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("InDoc After bundle Proration to components: " + yfcDocGetShipmentListOutput);
    }
    LOGGER.verbose("Any bundle line exsits " + bAnyBundleLineExists);
    if (bAnyBundleLineExists) {
      clubOrderLinesWithSimilarComps(yfcDocGetShipmentListOutput.getDocument());
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("InDoc after combining same components: " + yfcDocGetShipmentListOutput);
      }
    }

    // Fix for GCSTORE#3225 -- Starts
    getAndStampCommissionDetails(yfcDocGetShipmentListOutput);
    // Fix for GCSTORE#3225 -- Ends

    YFCDocument yfcDocInvokeWebServiceInput =
        GCCommonUtil.invokeService(env, GCXmlLiterals.GC_PREPARE_POS_WEB_SERVICE_INPUT, yfcDocGetShipmentListOutput);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("docInvokeWebServiceInput : " + yfcDocInvokeWebServiceInput);
    }
    // Invoke web service with POS input XML
    YFCDocument yfcDocInvokeWebServiceOutput = null;

    try {
      yfcDocInvokeWebServiceOutput =
          GCCommonUtil.invokeService(env, GCXmlLiterals.GC_INVOKE_POS_WEB_SERVICE, yfcDocInvokeWebServiceInput);
    } catch (YFSException yfsExcep) {
      LOGGER.error("Inside catch block of GCGetPOSReservationID.invokeAndGetPOSReserveID(): ", yfsExcep);
      LOGGER.endTimer("GCGetPOSReservationID.invokeAndGetPOSReserveID()");
      LOGGER.verbose("GCGetPOSReservationID - invokeAndGetPOSReserveID() : End With Error - Timeout");
      YFCException yfce = new YFCException(GCErrorConstants.WEB_SERVICE_RESERVATION_CALL_TIMEOUT_CODE);
      throw yfce;
    }

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("yfcDocInvokeWebServiceOutput : " + yfcDocInvokeWebServiceOutput);
    }
    Node nReserverRequestDetail =
        yfcDocInvokeWebServiceOutput.getDocument().getElementsByTagName(GCXmlLiterals.RESERVE_REQUEST_RESULT).item(0);
    String sStatus = GCConstants.FALSE_STR;
    String sRequestDetail = GCConstants.BLANK;

    boolean bHasCallFailed = false;
    if (YFCCommon.isVoid(nReserverRequestDetail)) {
      bHasCallFailed = true;
    } else {
      sRequestDetail = nReserverRequestDetail.getTextContent();
      if (YFCCommon.isStringVoid(sRequestDetail)) {
        bHasCallFailed = true;
      }
    }

    if (bHasCallFailed) {
    	LOGGER.endTimer("GCGetPOSReservationID.invokeAndGetPOSReserveID()");
    	LOGGER.verbose("GCGetPOSReservationID - invokeAndGetPOSReserveID() : End With Error - POS Rejected");
    	//start of GCSTORE-6814
    	Document docResponse = yfcDocInvokeWebServiceOutput.getDocument();
    	Element eleRoot = docResponse.getDocumentElement();
    	if(!YFCCommon.isVoid(eleRoot)){
    		Node nReserverErrorMsg = eleRoot.getElementsByTagName(GCConstants.MESSAGE_SMALL).item(0);
    		//Node nReserverErrorMsg = yfcDocInvokeWebServiceOutput.getDocument().getElementsByTagName(GCConstants.MESSAGE_SMALL).item(0);
    		if(!YFCCommon.isVoid(nReserverErrorMsg))
    		{
    			YFCException yfce = new YFCException(GCErrorConstants.WEB_SERVICE_RESERVATION_CALL_TIMEOUT_CODE);
    			yfce.setAttribute(GCConstants.ERROR_CODE, "ERROR_MESSAGE");
    			String sErrorMsg = nReserverErrorMsg.getTextContent();
    			LOGGER.verbose("Error Message to be presented:" +sErrorMsg);
    			sErrorMsg = sErrorMsg.concat(GCConstants.POS_CONNECT_FAILURE_ERROR_DESC);
    			LOGGER.verbose("Error Message modified:" +sErrorMsg);
    			yfce.setAttribute(GCConstants.ERROR_DESCRIPTION, sErrorMsg);
    			//end of GCSTORE-6814
    			LOGGER.verbose("Modified Error Message:" +yfce);
    			throw yfce;
    		}
    	}
    }

    YFCDocument yfcDocResponse = YFCDocument.getDocumentFor(sRequestDetail);
    YFCElement yfcEleResponse = yfcDocResponse.getDocumentElement();
    YFCElement yfcEleSuccess = yfcEleResponse.getElementsByTagName(GCConstants.SUCCESS).item(0);
    if (!YFCCommon.isVoid(yfcEleSuccess)) {
      sStatus = yfcEleSuccess.getNodeValue();
    }

    if (YFCCommon.equals(GCConstants.FALSE_STR, sStatus)) {
      YFCElement yfcEleErrorInfo = yfcEleResponse.getElementsByTagName(GCXmlLiterals.ERROR_INFO).item(0);
      String sErrorInfo = GCConstants.BLANK;
      if (!YFCCommon.isVoid(yfcEleErrorInfo)) {
        sErrorInfo = yfcEleErrorInfo.getNodeValue();
      }

      LOGGER.endTimer("GCGetPOSReservationID.invokeAndGetPOSReserveID()");
      LOGGER.verbose("GCGetPOSReservationID - invokeAndGetPOSReserveID() : End With Error - POS Rejected");
      YFCException yfce = new YFCException(GCErrorConstants.WEB_SERVICE_RESERVATION_CALL_ERROR_FROM_POS);
      yfce.setErrorDescription(sErrorInfo);
      throw yfce;
    }

    YFCDocument yfcDocPOSResponse = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement yfcElePOSSalesTicketNo = yfcEleResponse.getElementsByTagName(GCXmlLiterals.POS_SALES_TICKET).item(0);
    String sReservationId = GCConstants.BLANK;
    if (!YFCCommon.isVoid(yfcElePOSSalesTicketNo)) {
      sReservationId = yfcElePOSSalesTicketNo.getNodeValue();
    }

    // Throwing exception in the scenario where status is success but POSResevationID is blank
    if (YFCCommon.isVoid(sReservationId)) {
      LOGGER.endTimer("GCGetPOSReservationID.invokeAndGetPOSReserveID()");
      LOGGER.verbose("GCGetPOSReservationID - invokeAndGetPOSReserveID() : End With Error - POS Error");
      YFCException yfce = new YFCException(GCErrorConstants.INVALID_RESERVATION_ID_FROM_POS);
      throw yfce;
    }

    yfcDocPOSResponse.getDocumentElement().setAttribute(GCXmlLiterals.RESERVATION_ID, sReservationId);

    LOGGER.endTimer("GCGetPOSReservationID.invokeAndGetPOSReserveID()");
    LOGGER.verbose("GCGetPOSReservationID - invokeAndGetPOSReserveID() : End");
    return yfcDocPOSResponse.getDocument();
  }


  /**
   * This method is used to stamp taxPercentage in case if there are multiple LineTaxes & tax
   * percentage is zero for some but not all. In this case we'll stamp any non-zero taxPercentage
   * value from the other LineTax elements
   *
   * @param document
   */
  private void handleZeroTaxPercentage(YFCDocument yfcDocGetShipmentListOutput) {
    LOGGER.beginTimer("GCGetPOSReservationID.handleZeroTaxPercentage()");
    LOGGER.verbose("GCGetPOSReservationID - handleZeroTaxPercentage() : Start");

    YFCElement yfcEleShipments = yfcDocGetShipmentListOutput.getDocumentElement();

    YFCElement eleShipmentLines = yfcEleShipments.getElementsByTagName(GCConstants.SHIPMENT_LINES).item(0);
    YFCNodeList<YFCElement> nlShipmentLine = eleShipmentLines.getElementsByTagName(GCConstants.SHIPMENT_LINE);
    for (int i = 0; i < nlShipmentLine.getLength(); i++) {
      YFCElement eleShipmentLine = nlShipmentLine.item(i);
      YFCElement eleOrderLine = eleShipmentLine.getChildElement(GCConstants.ORDER_LINE);
      YFCNodeList<YFCElement> nlLineTax = eleOrderLine.getElementsByTagName(GCConstants.LINE_TAX);
      YFCNodeList<YFCElement> nlLineTax1 = eleOrderLine.getElementsByTagName(GCConstants.LINE_TAX);
      for (YFCElement eleLineTax : nlLineTax) {
        Double taxPercentage = eleLineTax.getDoubleAttribute(GCConstants.TAX_PERCENTAGE);
        if (taxPercentage == 0.00) {
          for (YFCElement eleLineTax1 : nlLineTax1) {
            Double taxPer = eleLineTax1.getDoubleAttribute(GCConstants.TAX_PERCENTAGE);
            if (taxPer > 0.00) {
              eleLineTax.setAttribute(GCConstants.TAX_PERCENTAGE, taxPer);
              break;
            }
          }
        }
      }
    }
    LOGGER.verbose("GCGetPOSReservationID - handleZeroTaxPercentage() : Ends");
    LOGGER.endTimer("GCGetPOSReservationID.handleZeroTaxPercentage()");
  }

  /**
   * This method will prorate bundle parent line price, charges and taxes to components
   *
   * @param inDoc
   * @return
   * @throws TransformerException
   */
  private Document prorateBundleCharges(Document inDoc) throws TransformerException {
    LOGGER.beginTimer("GCGetPOSReservationID.prorateBundleCharges()");
    LOGGER.verbose("GCGetPOSReservationID - prorateBundleCharges() : Start");

    Boolean isPrimaryNegative = false;
   	Double dPrimaryFinalTotalAfterDiscount = 0.0;
    List<Element> listBundleParentLineDetail =
        GCXMLUtil.getElementListByXpath(inDoc,
            "Shipments/Shipment/ShipmentLines/ShipmentLine/OrderLine[@IsBundleParent='Y']/..");
    /*BigDecimal dTotalTax = new BigDecimal(0);
    String sGrandTax =
        GCXMLUtil.getAttributeFromXPath(inDoc,
            "Shipments/Shipment/ShipmentLines/ShipmentLine/OrderLine/Order/OverallTotals/@GrandTax");
    if(!YFCCommon.isVoid(sGrandTax)){
      dTotalTax = BigDecimal.valueOf(Double.valueOf(sGrandTax));
    }
    dTotalTax = BigDecimal.valueOf(209.21);*/
    if (listBundleParentLineDetail.isEmpty()) {
      bAnyBundleLineExists = false;
    } else {
      Element eleShipmentLines = (Element) inDoc.getElementsByTagName(GCConstants.SHIPMENT_LINES).item(0);
      Iterator<Element> iteratorListBundleLineDetail = listBundleParentLineDetail.iterator();
      while (iteratorListBundleLineDetail.hasNext()) {
        BigDecimal dTaxProrated = new BigDecimal(0);
        Element eleBundleParentLineDetail = iteratorListBundleLineDetail.next();
        Element eleOrderBundleParentLine =
            (Element) eleBundleParentLineDetail.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
        String sOrderLineKey = eleOrderBundleParentLine.getAttribute(GCConstants.ORDER_LINE_KEY);
        Element eleLineOverallTotals =
            (Element) eleBundleParentLineDetail.getElementsByTagName("LineOverallTotals").item(0);
        String sBundleUnitPrice = eleLineOverallTotals.getAttribute(GCConstants.UNIT_PRICE);
        boolean isSecondaryPriceMore = false;
        boolean isSecondaryComponent = true;
        String mainSecondaryComponent = null;
        Element eleSecondaryComponent = null;
        List<String> listChargeNameForSecondary = new ArrayList<String>();

        Element eleBundleItem =
            (Element) eleOrderBundleParentLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
        Element eleBundleItemExtn = (Element) eleBundleItem.getElementsByTagName(GCConstants.EXTN).item(0);
        String sPrimaryComponent = eleBundleItemExtn.getAttribute(GCXmlLiterals.EXTN_PRIMARY_COMPONENT_ITEM);
        Double dSecondaryComponentPrice = 0.00;
        Double dComponentLineTax = 0.00;

        NodeList nlLineCharge = eleBundleParentLineDetail.getElementsByTagName(GCConstants.LINE_CHARGE);
        NodeList nlLineTax = eleBundleParentLineDetail.getElementsByTagName(GCConstants.LINE_TAX);

        Map<String, Element> mapPrimaryComponent = new HashMap<String, Element>();

        List<Element> listChildShipmentLine =
            GCXMLUtil.getElementListByXpath(inDoc,
                "Shipments/Shipment/ShipmentLines/ShipmentLine/OrderLine[@BundleParentOrderLineKey='" + sOrderLineKey
                + "']/..");

        // logic : Prorating charges when component unitPrice is greater than parent
        // UnitPrice.-----starts
        for (Element eleChldShipLine : listChildShipmentLine) {

          Element eleChldOrderLine = (Element) eleChldShipLine.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
          Element eleChldItemDetails =
              (Element) eleChldOrderLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
          String sItemId = eleChldItemDetails.getAttribute(GCConstants.ITEM_ID);
          if (!YFCUtils.equals(sPrimaryComponent, sItemId)) {
            Element eleChldLinePriceInfo =
                (Element) eleChldOrderLine.getElementsByTagName(GCConstants.LINE_PRICE_INFO).item(0);
            Double listprice = Double.parseDouble(eleChldLinePriceInfo.getAttribute(GCConstants.LIST_PRICE));
            Double bundleUnitPrice = Double.parseDouble(sBundleUnitPrice);
            if (listprice > bundleUnitPrice) {
              isSecondaryPriceMore = true;
              mainSecondaryComponent = eleChldOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
              break;
            }
          }

        }
        // logic : Prorating charges when component unitPrice is greater than parent
        // UnitPrice.-----ends

        String primaryOdrLineKey = null;
        
        for (Element eleChildShipmentLine : listChildShipmentLine) {
          Element eleChildOrderLine =
              (Element) eleChildShipmentLine.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
          Element eleChildItemDetails =
              (Element) eleChildOrderLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
          Element eleChildLinePriceInfo =
              (Element) eleChildOrderLine.getElementsByTagName(GCConstants.LINE_PRICE_INFO).item(0);
          Element eleChildLineOverallTotals =
              (Element) eleChildOrderLine.getElementsByTagName("LineOverallTotals").item(0);

          String sItemId = eleChildItemDetails.getAttribute(GCConstants.ITEM_ID);
          String sListprice = eleChildLinePriceInfo.getAttribute(GCConstants.LIST_PRICE);

          // logic : Prorating charges when component unitPrice is greater than parent
          // UnitPrice.-----starts
          if (isSecondaryPriceMore) {
            if (YFCUtils.equals(mainSecondaryComponent, eleChildOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY))) {
              sListprice = sBundleUnitPrice;
            } else {
              sListprice = "0.00";
            }
          }
          // logic : Prorating charges when component unitPrice is greater than parent
          // UnitPrice.-----ends

          String sShipmentLineQty = eleChildShipmentLine.getAttribute(GCConstants.QUANTITY);
          Double dExtendedPrice = Double.parseDouble(sShipmentLineQty) * Double.parseDouble(sListprice);

          eleChildLineOverallTotals.setAttribute(GCConstants.UNIT_PRICE, sListprice);
          eleChildLineOverallTotals.setAttribute(GCXmlLiterals.EXTENDED_PRICE, dExtendedPrice.toString());
          eleChildLinePriceInfo.setAttribute(GCConstants.UNIT_PRICE, sListprice);

          NodeList nlChildLineTaxes =
              eleChildShipmentLine.getElementsByTagName(GCConstants.LINE_TAX);
          Element eleChildLineTaxes = (Element) eleChildShipmentLine.getElementsByTagName(GCConstants.LINE_TAXES).item(0);



          if (YFCCommon.equals(sItemId, sPrimaryComponent)) {
            // Item is a primary item
            mapPrimaryComponent.put(sPrimaryComponent, eleChildShipmentLine);
            //GCSTORE-4687 starts
            primaryOdrLineKey = eleChildShipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY);
            //GCSTORE-4687 ends
          } else {
            // Item is a secondary item

            dSecondaryComponentPrice += Double.parseDouble(sListprice);
            Double dSecondaryComponentRatio = Double.parseDouble(sListprice) / Double.parseDouble(sBundleUnitPrice);
            if (isSecondaryComponent && (0 != Double.parseDouble(sListprice))) {
              eleSecondaryComponent = eleChildShipmentLine;
              isSecondaryComponent = false;
            }

            // Loop Over line taxes
            for (int i = 0; i < nlLineTax.getLength(); i++) {
              Element eleBundleLineTax = (Element) nlLineTax.item(i);
              String sTaxCategory = eleBundleLineTax.getAttribute(GCConstants.CHARGE_NAME);
              if (YFCCommon.equals(GCConstants.LINE_PRICE_TAX, sTaxCategory)) {

                Element eleChildLineTax = null;

                for (int counter = 0; counter < nlChildLineTaxes.getLength(); counter++) {
                  Element eleTemp = (Element) nlChildLineTaxes.item(counter);
                  String sChargeName = eleTemp.getAttribute("ChargeName");
                  if(YFCCommon.equals(sChargeName, sTaxCategory)){
                    eleChildLineTax = eleTemp;
                  }
                }

                if (YFCCommon.isVoid(eleChildLineTax)) {
                  eleChildLineTax = inDoc.createElement(GCConstants.LINE_TAX);
                  eleChildLineTaxes.appendChild(eleChildLineTax);
                  GCXMLUtil.copyElement(inDoc, eleBundleLineTax, eleChildLineTax);
                }
                // Calculate component charge value
                String sTaxAmount = eleBundleLineTax.getAttribute(GCConstants.TAX);
                dComponentLineTax = Double.parseDouble(sTaxAmount) * dSecondaryComponentRatio;
                BigDecimal bdComponentTaxAmount = GCCommonUtil.formatNumberForDecimalPrecision(dComponentLineTax,2);
                dTaxProrated = dTaxProrated.add(bdComponentTaxAmount);
                eleChildLineTax.setAttribute(GCConstants.TAX, bdComponentTaxAmount.toString());
                break;
              }
            }
          }
          // End of if secondary Item
        }
        // End of for loop on child items

        /* Append Primary Component price, charges and taxes - Start */
        Element elePrimaryItemLine = mapPrimaryComponent.get(sPrimaryComponent);
        if (!YFCCommon.isVoid(elePrimaryItemLine)) {
          Double dFinalPrimaryLineTotal = 0.00;

          Double dPrimaryUnitPrice = Double.parseDouble(sBundleUnitPrice) - dSecondaryComponentPrice;
          String sShippedQty = elePrimaryItemLine.getAttribute(GCConstants.QUANTITY);
          Double dExtendedPrice = Double.parseDouble(sShippedQty) * dPrimaryUnitPrice;
          Element eleChildLineOverallTotals =
              (Element) elePrimaryItemLine.getElementsByTagName("LineOverallTotals").item(0);
          Element eleChildLinePriceInfo =
              (Element) elePrimaryItemLine.getElementsByTagName(GCConstants.LINE_PRICE_INFO).item(0);
          eleChildLineOverallTotals.setAttribute(GCConstants.UNIT_PRICE, dPrimaryUnitPrice.toString());
          eleChildLineOverallTotals.setAttribute(GCXmlLiterals.EXTENDED_PRICE, dExtendedPrice.toString());
          eleChildLinePriceInfo.setAttribute(GCConstants.UNIT_PRICE, dPrimaryUnitPrice.toString());
          dFinalPrimaryLineTotal += dExtendedPrice;

          Element elePrimaryLineCharges =
              (Element) elePrimaryItemLine.getElementsByTagName(GCConstants.LINE_CHARGES).item(0);
          Element elePrimaryLineTaxes =
              (Element) elePrimaryItemLine.getElementsByTagName(GCConstants.LINE_TAXES).item(0);

          // Copy all bundle line charges to primary component
          for (int i = 0; i < nlLineCharge.getLength(); i++) {
            Element eleBundleLineCharge = (Element) nlLineCharge.item(i);
            if (YFCCommon.equals(GCConstants.PROMOTION, eleBundleLineCharge.getAttribute(GCConstants.CHARGE_CATEGORY))
                || YFCCommon.equals(GCConstants.SHIPPING_PROMOTION,
                    eleBundleLineCharge.getAttribute(GCConstants.CHARGE_CATEGORY))) {
              dFinalPrimaryLineTotal -= Double.parseDouble(eleBundleLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT));
            } 
            //else {
             // dFinalPrimaryLineTotal += Double.parseDouble(eleBundleLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT));
            //}
            // Check if the primary component line total is negative after adding looped in charge
            if (dFinalPrimaryLineTotal < 0) {
            	 //GCSTORE-4687 - STARTS
          	  isPrimaryNegative = true;
  			dPrimaryFinalTotalAfterDiscount = dFinalPrimaryLineTotal;
          	  //GCSTORE-4687 - ENDS
              dFinalPrimaryLineTotal += Double.parseDouble(eleBundleLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT));
              listChargeNameForSecondary.add(eleBundleLineCharge.getAttribute(GCConstants.CHARGE_NAME));
              Element eleSecondaryLineCharges =
                  (Element) eleSecondaryComponent.getElementsByTagName(GCConstants.LINE_CHARGES).item(0);
              if (!YFCCommon.isVoid(eleSecondaryLineCharges)) {
                Element eleChildLineCharge = inDoc.createElement(GCConstants.LINE_CHARGE);
                eleSecondaryLineCharges.appendChild(eleChildLineCharge);
                GCXMLUtil.copyElement(inDoc, eleBundleLineCharge, eleChildLineCharge);
              }
            } else {
              Element eleChildLineCharge = inDoc.createElement(GCConstants.LINE_CHARGE);
              elePrimaryLineCharges.appendChild(eleChildLineCharge);
              GCXMLUtil.copyElement(inDoc, eleBundleLineCharge, eleChildLineCharge);
            }
          }

          // Loop Over line taxes
          for (int i = 0; i < nlLineTax.getLength(); i++) {
            Element eleBundleLineTax = (Element) nlLineTax.item(i);
            String sTaxCategory = eleBundleLineTax.getAttribute(GCConstants.CHARGE_NAME);
            // If charge is copied to secondary copy corresponding taxes to secondary component
            if (listChargeNameForSecondary.contains(sTaxCategory)) {
              Element eleSecondaryLineTaxes =
                  (Element) eleSecondaryComponent.getElementsByTagName(GCConstants.LINE_TAXES).item(0);
              if (!YFCCommon.isVoid(eleSecondaryLineTaxes)) {
                Element eleChildLineTax =
                    (Element) XPathAPI.selectSingleNode(eleSecondaryLineTaxes, "LineTaxes/LineTax[@ChargeName='"
                        + sTaxCategory + "']");
                if(YFCCommon.isVoid(eleChildLineTax)){
                  eleChildLineTax = inDoc.createElement(GCConstants.LINE_TAX);
                  eleSecondaryLineTaxes.appendChild(eleChildLineTax);
                }
                String sTax = eleBundleLineTax.getAttribute(GCConstants.TAX);
                dTaxProrated = dTaxProrated.add(BigDecimal.valueOf(Double.valueOf(sTax)));
                GCXMLUtil.copyElement(inDoc, eleBundleLineTax, eleChildLineTax);
              }
            } else {
              Element eleChildLineTax =
                  (Element) XPathAPI.selectSingleNode(elePrimaryLineTaxes, "LineTax[@ChargeName='"
                      + sTaxCategory + "']");
              if(YFCCommon.isVoid(eleChildLineTax)){
                eleChildLineTax = inDoc.createElement(GCConstants.LINE_TAX);
                elePrimaryLineTaxes.appendChild(eleChildLineTax);
                GCXMLUtil.copyElement(inDoc, eleBundleLineTax, eleChildLineTax);
              }
              // Calculate remaining Tax value for line price tax
              if (YFCCommon.equals(GCConstants.LINE_PRICE_TAX, sTaxCategory)) {
                String sTaxAmount = eleBundleLineTax.getAttribute(GCConstants.TAX);
                Double dComponentTaxAmount = Double.parseDouble(sTaxAmount) - dComponentLineTax;
                BigDecimal bdComponentTaxAmount = GCCommonUtil.formatNumberForDecimalPrecision(dComponentTaxAmount,2);
                dTaxProrated = dTaxProrated.add(bdComponentTaxAmount);
                if(!iteratorListBundleLineDetail.hasNext() && (dTaxProrated != BigDecimal.valueOf(Double.parseDouble(sTaxAmount)))){
                  BigDecimal bdVariableTax = BigDecimal.valueOf(Double.parseDouble(sTaxAmount)).subtract(dTaxProrated);
                  bdComponentTaxAmount = bdComponentTaxAmount.subtract(bdVariableTax);
                }
                eleChildLineTax.setAttribute(GCConstants.TAX, bdComponentTaxAmount.toString());
              }
            }
          }
          /* Append Primary Component price, charges and taxes - End */
        }
        // Remove bundle parent line
        eleShipmentLines.removeChild(eleBundleParentLineDetail);
     // inside loop for bundle parent
        //GCSTORE-4687 - STARTS
		if (isPrimaryNegative && !isSecondaryPriceMore) {
			ProrateChargesAndTax(inDoc, dPrimaryFinalTotalAfterDiscount, eleBundleParentLineDetail,
					listChildShipmentLine, primaryOdrLineKey);
		}
		//GCSTORE-4687 ENDS
      }
    }

    LOGGER.endTimer("GCGetPOSReservationID.prorateBundleCharges()");
    LOGGER.verbose("GCGetPOSReservationID - prorateBundleCharges() : End");
    return inDoc;
  }

  /**
   * This method is used to prorate line taxes and charges in case discount is greater than primary line total
   *
   * @param inDoc
   * @param dPrimaryFinalTotalAfterDiscount
   * @param eleBundleParentLineDetail
   * @param listChildLineDetail
   * @param primaryOdrLineKey
   */
  
  private void ProrateChargesAndTax(Document inDoc, Double dPrimaryFinalTotalAfterDiscount,
			Element eleBundleParentLineDetail, List<Element> listChildLineDetail, String primaryOdrLineKey)
			throws TransformerException{

	  LOGGER.verbose("ProrateChargesAndTax(): Start" + GCXMLUtil.getXMLString(inDoc));
		BigDecimal dTaxProrated = new BigDecimal(0);
		NodeList nlLineCharge = eleBundleParentLineDetail.getElementsByTagName(GCConstants.LINE_CHARGE);
		Element elePrimaryLineDetail = GCXMLUtil.getElementByXPath(inDoc,
				"Shipments/Shipment/ShipmentLines/ShipmentLine/OrderLine[@OrderLineKey='" + primaryOdrLineKey
						+ "']/..");
		
		Element eleChildLinePriceInfo =
	              (Element) elePrimaryLineDetail.getElementsByTagName(GCConstants.LINE_PRICE_INFO).item(0);
		Double dPrimaryUnitPrice = Double.parseDouble(eleChildLinePriceInfo.getAttribute(GCConstants.UNIT_PRICE));
		Double dPrimaryUnitPriceAfterProration = dPrimaryUnitPrice;
		NodeList nlPrimaryLineTaxes = elePrimaryLineDetail.getElementsByTagName(GCConstants.LINE_TAX);
		for (int j = 0; j < nlPrimaryLineTaxes.getLength(); j++) {
			Element elePrimaryLineTax = (Element) nlPrimaryLineTaxes.item(j);
			if (YFCCommon.equals(GCConstants.LINE_PRICE,
					elePrimaryLineTax.getAttribute(GCConstants.CHARGE_CATEGORY))) {
				elePrimaryLineTax.setAttribute(GCConstants.TAX, "0.0");
				elePrimaryLineDetail.setAttribute(GCConstants.TAX, "0.0");
				
			}
		}
		NodeList nlLineTax = eleBundleParentLineDetail.getElementsByTagName(GCConstants.LINE_TAX);
		for (int j = 0; j < nlLineTax.getLength(); j++) {
			Element eleBundleLineTax = (Element) nlLineTax.item(j);
				if (YFCCommon.equals(GCConstants.LINE_PRICE,
						eleBundleLineTax.getAttribute(GCConstants.CHARGE_CATEGORY))) {
					dTaxProrated = dTaxProrated.add(GCCommonUtil.formatNumberForDecimalPrecision(Double.parseDouble(eleBundleLineTax.getAttribute(GCConstants.TAX)),2));
							
				}
			}
		for (int i = 0; i < nlLineCharge.getLength(); i++) {
			Element eleBundleLineCharge = (Element) nlLineCharge.item(i);
			if (YFCCommon.equals(GCConstants.PROMOTION, eleBundleLineCharge.getAttribute(GCConstants.CHARGE_CATEGORY))) {
				Element eleOrderBundleParentLine = (Element) eleBundleParentLineDetail.getElementsByTagName(
						GCConstants.ORDER_LINE).item(0);
				Element eleBundleItem = (Element) eleOrderBundleParentLine.getElementsByTagName(
						GCConstants.ITEM_DETAILS).item(0);
				Element eleBundleItemExtn = (Element) eleBundleItem.getElementsByTagName(GCConstants.EXTN).item(0);
				String sPrimaryComponent = eleBundleItemExtn.getAttribute(GCXmlLiterals.EXTN_PRIMARY_COMPONENT_ITEM);
				String sChargeName = eleBundleLineCharge.getAttribute(GCConstants.CHARGE_NAME);
				Double dChargeAmount = Double.parseDouble(eleBundleLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT));
				Double dTotalDiscount = Double.parseDouble(eleBundleLineCharge
						.getAttribute(GCConstants.CHARGE_AMOUNT));
				Double dRemainingDiscountAfterProration = 0.0;
				Element nlPrimaryLineCharges = (Element) elePrimaryLineDetail.getElementsByTagName(
						GCConstants.LINE_CHARGES).item(0);
				
				Element elePrimaryLineCharge = (Element)XPathAPI.selectSingleNode(nlPrimaryLineCharges, "LineCharge[@ChargeName='"+sChargeName+"']"
						+ "[@ChargeCategory='"+GCConstants.PROMOTION+"']");
				if(YFCCommon.isVoid(elePrimaryLineCharge)){
					elePrimaryLineCharge = inDoc.createElement(GCConstants.LINE_CHARGE);
					nlPrimaryLineCharges.appendChild(elePrimaryLineCharge);

					GCXMLUtil.copyAttributes(eleBundleLineCharge, elePrimaryLineCharge);
				}
					if(dPrimaryUnitPriceAfterProration > 0){
						if(dChargeAmount > dPrimaryUnitPrice){
							elePrimaryLineCharge.setAttribute(GCConstants.CHARGE_AMOUNT,
									dPrimaryUnitPriceAfterProration.toString());
							dRemainingDiscountAfterProration = dChargeAmount - dPrimaryUnitPriceAfterProration;
							dPrimaryUnitPriceAfterProration = 0.0;
						}
						else{
							elePrimaryLineCharge.setAttribute(GCConstants.CHARGE_AMOUNT,
									dChargeAmount.toString());
							dPrimaryUnitPriceAfterProration = dPrimaryUnitPrice - dChargeAmount; 
						}
					}
					else{
						elePrimaryLineCharge.setAttribute(GCConstants.CHARGE_AMOUNT,
								"0.0");
						dPrimaryUnitPrice = 0.0;
					}
																						
				Double Secondarylistprice = 0.0;
				Double dSecodaryDiscountAfterProration = 0.0;
								
				// looping over secondary lines
				for (Element eleChildLineDetail : listChildLineDetail) {
					Element eleChldOrderLine = (Element) eleChildLineDetail
							.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
					Element eleChldItemDetails = (Element) eleChldOrderLine.getElementsByTagName(
							GCConstants.ITEM_DETAILS).item(0);
					String sItemId = eleChldItemDetails.getAttribute(GCConstants.ITEM_ID);
					Double dItemQty = Double.parseDouble(eleChildLineDetail.getAttribute(GCConstants.QUANTITY));
					if (!YFCUtils.equals(sPrimaryComponent, sItemId)) {
						Element eleChldLinePriceInfo = (Element) eleChldOrderLine.getElementsByTagName(
								GCConstants.LINE_PRICE_INFO).item(0);
						
						// multiply with quantity
						Secondarylistprice = Double.parseDouble(eleChldLinePriceInfo
								.getAttribute(GCConstants.LIST_PRICE)) * dItemQty;

						dSecodaryDiscountAfterProration = Secondarylistprice - dRemainingDiscountAfterProration;

						if (dSecodaryDiscountAfterProration >= 0) {
							Element eleSecondaryLineChargesList = (Element)eleChildLineDetail
									.getElementsByTagName(GCConstants.LINE_CHARGES).item(0); 
													
							Element eleSecondaryLineCharge = (Element)XPathAPI.selectSingleNode(eleSecondaryLineChargesList, "LineCharge[@ChargeName='"+sChargeName+"']"
										+ "[@ChargeCategory='"+GCConstants.PROMOTION+"']");
								
								if(!YFCCommon.isVoid(eleSecondaryLineCharge)){
									eleSecondaryLineCharge.setAttribute(GCConstants.CHARGE_AMOUNT,
											dRemainingDiscountAfterProration.toString());
								}
								else{
									Element eleSecondaryLineCharges = inDoc.createElement(GCConstants.LINE_CHARGE);
									GCXMLUtil.copyAttributes(eleBundleLineCharge, eleSecondaryLineCharges);
									eleSecondaryLineCharges.setAttribute(GCConstants.CHARGE_AMOUNT,
											dRemainingDiscountAfterProration.toString());
									eleSecondaryLineChargesList.appendChild(eleSecondaryLineCharges);
								}
									
							NodeList nlChildLineTaxes = eleChildLineDetail.getElementsByTagName(GCConstants.LINE_TAX);
							Element eleChildLineTaxes = (Element)eleChildLineDetail.getElementsByTagName(GCConstants.LINE_TAXES).item(0);
							if(nlChildLineTaxes.getLength()>0){
								for (int j = 0; j < nlChildLineTaxes.getLength(); j++) {
									Element eleChildLineTax = (Element) nlChildLineTaxes.item(j);
									if (!YFCCommon.isVoid(eleChildLineTax)
											&& eleChildLineTax.getAttribute(GCConstants.CHARGE_NAME).equalsIgnoreCase(
													GCConstants.LINE_PRICE_TAX)) {
										
										//dTaxProrated = dTaxProrated.add(GCCommonUtil.formatNumberForDecimalPrecision(Double.parseDouble(eleChildLineTax.getAttribute(GCConstants.TAX)),2));
										eleChildLineTax.setAttribute(GCConstants.TAX, dTaxProrated.toString());
										eleChildLineDetail.setAttribute(GCConstants.TAX, dTaxProrated.toString());
									} 
								}
							}
							else{
								
									Element eleChildLineTax = inDoc.createElement(GCConstants.LINE_TAX);
									GCXMLUtil.copyAttributes(eleBundleLineCharge, eleChildLineTax);
									eleChildLineTax.setAttribute(GCConstants.TAX, dTaxProrated.toString());
									eleChildLineTaxes.appendChild(eleChildLineTax);
									eleChildLineDetail.setAttribute(GCConstants.TAX, dTaxProrated.toString());
													
							}
						break;

						} else {
							dRemainingDiscountAfterProration = dRemainingDiscountAfterProration - Secondarylistprice;
							// updating line charge and tax in case of negative
							// value
							NodeList nlSecondaryLineCharges = eleChildLineDetail
									.getElementsByTagName(GCConstants.LINE_CHARGE);
							for (int j = 0; j < nlSecondaryLineCharges.getLength(); j++) {
								Element eleSecondaryLineCharges = (Element) nlSecondaryLineCharges.item(j);
								if (!YFCCommon.isVoid(eleSecondaryLineCharges)
										&& YFCCommon.equals(GCConstants.PROMOTION,
												eleSecondaryLineCharges.getAttribute(GCConstants.CHARGE_CATEGORY))
										|| YFCCommon.equals(GCConstants.SHIPPING_PROMOTION,
												eleSecondaryLineCharges.getAttribute(GCConstants.CHARGE_CATEGORY))) {
									eleSecondaryLineCharges.setAttribute(GCConstants.CHARGE_AMOUNT,
											Secondarylistprice.toString());
								}
							}

							NodeList nlChildLineTaxes = eleChildLineDetail.getElementsByTagName(GCConstants.LINE_TAX);
							for (int j = 0; j < nlChildLineTaxes.getLength(); j++) {
								Element eleChildLineTax = (Element) nlChildLineTaxes.item(j);
								if (!YFCCommon.isVoid(eleChildLineTax)
										&& eleChildLineTax.getAttribute(GCConstants.CHARGE_NAME).equalsIgnoreCase(
												GCConstants.LINE_PRICE_TAX)) {
									//dTaxProrated = dTaxProrated.add(GCCommonUtil.formatNumberForDecimalPrecision(Double.parseDouble(eleChildLineTax.getAttribute(GCConstants.TAX)),2));
									eleChildLineTax.setAttribute(GCConstants.TAX, "0.0");
								}
							}

						}
					}

				}

			}
		}

		 LOGGER.verbose("ProrateChargesAndTax(): END" + GCXMLUtil.getXMLString(inDoc));

	
}
  /**
   * This method will club items with same components in the shipment
   *
   * @param inDoc
   */
  private Document clubOrderLinesWithSimilarComps(Document inDoc) {
    LOGGER.beginTimer("GCPOSInvoicing.clubOrderLinesWithSimilarComps()");
    LOGGER.verbose("GCPOSInvoicing - clubOrderLinesWithSimilarComps() : Start");

    List<Element> listShipmentLine =
        GCXMLUtil.getElementListByXpath(inDoc, "Shipments/Shipment/ShipmentLines/ShipmentLine");
    Element eleShipmentLines = (Element) inDoc.getElementsByTagName(GCConstants.SHIPMENT_LINES).item(0);
    Iterator<Element> iteratorListShipmentLine = listShipmentLine.iterator();
    List<String> listItemId = new ArrayList<String>();
    while (iteratorListShipmentLine.hasNext()) {
      Element eleShipmentLine = iteratorListShipmentLine.next();
      String sExtnIsWarranty = GCXMLUtil.getAttributeFromXPath(eleShipmentLine, "OrderLine/Extn/@ExtnIsWarrantyItem");
      if (!GCConstants.YES.equals(sExtnIsWarranty)) {
        String sItemId = eleShipmentLine.getAttribute(GCConstants.ITEM_ID);
        String sPrimeLineNo = eleShipmentLine.getAttribute(GCConstants.PRIME_LINE_NO);
        String sSKUId =
            GCXMLUtil.getAttributeFromXPath(inDoc, "Shipments/Order/Products/Product[@LineNumber='" + sPrimeLineNo
                + "']/@SKU.Number");
        String sOrderLineKey = eleShipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY);
        Double dItemQty = Double.parseDouble(eleShipmentLine.getAttribute(GCConstants.QUANTITY));
        Element eleChildLineOverallTotals = (Element) eleShipmentLine.getElementsByTagName("LineOverallTotals").item(0);
        Double dUnitPrice =
            Double.parseDouble(eleChildLineOverallTotals.getAttribute(GCXmlLiterals.EXTENDED_PRICE)) / dItemQty;

        Map<String, Element> mapSameItemCharges = new HashMap<String, Element>();
        Map<String, Element> mapSameItemTax = new HashMap<String, Element>();

        if (listItemId.contains(sSKUId)) {
          // Remove Order line from document
          eleShipmentLines.removeChild(eleShipmentLine);
        } else {
          List<Element> listSameSKUItemShipmentLine =
              GCXMLUtil.getElementListByXpath(inDoc, "Shipments/Order/Products/Product[@LineNumber!='" + sPrimeLineNo
                  + "' and @SKU.Number='" + sSKUId + "']");
          List<Element> listSameItemShipmentLine = new ArrayList();
          if (listSameSKUItemShipmentLine.size() > 0) {
            listSameItemShipmentLine =
                GCXMLUtil.getElementListByXpath(inDoc, "Shipments/Shipment/ShipmentLines/ShipmentLine[@OrderLineKey!='"
                    + sOrderLineKey + "' and @ItemID='" + sItemId + "']");
          }
          if (!listSameItemShipmentLine.isEmpty()) {
            listItemId.add(sSKUId);
            Iterator<Element> iteratorSameItem = listSameItemShipmentLine.iterator();
            while (iteratorSameItem.hasNext()) {
              Element eleSameItemShipmentLine = iteratorSameItem.next();
              String sOrderedQty = eleSameItemShipmentLine.getAttribute(GCConstants.QUANTITY);
              dItemQty += Double.parseDouble(sOrderedQty);

              NodeList nlSameItemLineCharge = eleSameItemShipmentLine.getElementsByTagName(GCConstants.LINE_CHARGE);
              NodeList nlSameItemLineTax = eleSameItemShipmentLine.getElementsByTagName(GCConstants.LINE_TAX);

              // Loop Over line charges
              for (int i = 0; i < nlSameItemLineCharge.getLength(); i++) {
                Element eleSameLineCharge = (Element) nlSameItemLineCharge.item(i);
                String sChargeName = eleSameLineCharge.getAttribute(GCConstants.CHARGE_NAME);
                mapSameItemCharges.put(sChargeName, eleSameLineCharge);
              }

              // Loop Over line taxes
              for (int j = 0; j < nlSameItemLineTax.getLength(); j++) {
                Element eleSameLineTax = (Element) nlSameItemLineTax.item(j);
                String sChargeName = eleSameLineTax.getAttribute(GCConstants.CHARGE_NAME);
                mapSameItemTax.put(sChargeName, eleSameLineTax);
              }
            }

            // Update Extended and Shipped qty for line
            Double dFinalExtendedPrice = dUnitPrice * dItemQty;
            eleShipmentLine.setAttribute(GCConstants.QUANTITY, dItemQty.toString());
            eleChildLineOverallTotals.setAttribute(GCXmlLiterals.EXTENDED_PRICE, dFinalExtendedPrice.toString());
            NodeList nlLineCharge = eleShipmentLine.getElementsByTagName(GCConstants.LINE_CHARGE);
            NodeList nlLineTax = eleShipmentLine.getElementsByTagName(GCConstants.LINE_TAX);

            // Loop Over line charges
            for (int i = 0; i < nlLineCharge.getLength(); i++) {
              Element eleSameLineCharge = (Element) nlLineCharge.item(i);
              String sChargeName = eleSameLineCharge.getAttribute(GCConstants.CHARGE_NAME);
              String sChargeAmount = eleSameLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT);
              if (mapSameItemCharges.containsKey(sChargeName)) {
                String sEarlierChargeAmount = mapSameItemCharges.get(sChargeName).getAttribute(GCConstants.CHARGE_AMOUNT);
                Double dSumChargeAmount = Double.parseDouble(sChargeAmount) + Double.parseDouble(sEarlierChargeAmount);
                eleSameLineCharge.setAttribute(GCConstants.CHARGE_AMOUNT, dSumChargeAmount.toString());
              }
            }

            // Loop Over line taxes
            for (int j = 0; j < nlLineTax.getLength(); j++) {
              Element eleSameLineTax = (Element) nlLineTax.item(j);
              String sChargeName = eleSameLineTax.getAttribute(GCConstants.CHARGE_NAME);
              String sTaxAmount = eleSameLineTax.getAttribute(GCConstants.TAX);
              if (mapSameItemTax.containsKey(sChargeName)) {
                String sEarlierTaxAmount = mapSameItemTax.get(sChargeName).getAttribute(GCConstants.TAX);
                Double dSumTaxAmount = Double.parseDouble(sTaxAmount) + Double.parseDouble(sEarlierTaxAmount);
                eleSameLineTax.setAttribute(GCConstants.TAX, dSumTaxAmount.toString());
              }
            }
          }
        }
      }
    }
    // End of for loop on line detail
    LOGGER.endTimer("GCPOSInvoicing.clubOrderLinesWithSimilarComps()");
    LOGGER.verbose("GCPOSInvoicing - clubOrderLinesWithSimilarComps() : End");
    return inDoc;
  }

  // Fix for GCSTORE#3225 -- Starts
  /**
   * Method to get and stamp updated commission details
   *
   * @param yfcEleRootInDoc
   * @param yfcEleOrderLine
   * @param yfcEleCommissions
   * @return
   */
  private YFCElement getAndStampCommissionDetails(YFCDocument yfcDocInDoc) {
    LOGGER.beginTimer("GCGetPOSReservationID.getAndStampCommissionDetails()");
    LOGGER.verbose("GCGetPOSReservationID - getAndStampCommissionDetails() : Start");

    LOGGER.verbose("yfcDocInDoc before processing :" + yfcDocInDoc.toString());
    YFCElement yfcEleRootInDoc = yfcDocInDoc.getDocumentElement();
    Double dTotalAmount = 0.00;
    Integer intSequenceNo = 0;

    Map<String, Double> uniqueUsersMap = new HashMap<String, Double>();
    // Loop Over OrderLines
    YFCNodeList<YFCElement> yfcNLShipmentLine = yfcEleRootInDoc.getElementsByTagName(GCConstants.SHIPMENT_LINE);
    int lengthShipLine = yfcNLShipmentLine.getLength();

    // Calculate Total Amount
    for (int i = 0; i < lengthShipLine; i++) {
      YFCElement yfcEleShipLine = yfcNLShipmentLine.item(i);
      YFCElement yfcEleLineDetailOrderLine = yfcEleShipLine.getChildElement("OrderLine");
      String sBundleParentOrderLineKey = yfcEleLineDetailOrderLine.getAttribute("BundleParentOrderLineKey");
      Double dLineTotal;
      YFCElement eleLineOverAllTotals = yfcEleLineDetailOrderLine.getChildElement("LineOverallTotals");
      if (YFCCommon.isVoid(sBundleParentOrderLineKey)) {
        dLineTotal = eleLineOverAllTotals.getDoubleAttribute(GCConstants.LINE_TOTAL);
      } else {
        Double dExtendedPrice = eleLineOverAllTotals.getDoubleAttribute("ExtendedPrice");
        Double dTax = eleLineOverAllTotals.getDoubleAttribute("Tax");
        dLineTotal = dExtendedPrice + dTax;
      }
      dTotalAmount = dTotalAmount + dLineTotal;
    }

    for (int i = 0; i < lengthShipLine; i++) {
      YFCElement yfcEleShipLine = yfcNLShipmentLine.item(i);
      YFCElement yfcEleLineDetailOrderLine = yfcEleShipLine.getChildElement("OrderLine");
      String sBundleParentOrderLineKey = yfcEleLineDetailOrderLine.getAttribute("BundleParentOrderLineKey");
      Double dLineTotal;
      YFCElement eleLineOverAllTotals = yfcEleLineDetailOrderLine.getChildElement("LineOverallTotals");
      if (YFCCommon.isVoid(sBundleParentOrderLineKey)) {
        dLineTotal = eleLineOverAllTotals.getDoubleAttribute(GCConstants.LINE_TOTAL);
      } else {
        Double dExtendedPrice = eleLineOverAllTotals.getDoubleAttribute("ExtendedPrice");
        Double dTax = eleLineOverAllTotals.getDoubleAttribute("Tax");
        dLineTotal = dExtendedPrice + dTax;
      }

      YFCElement eleCommissions = yfcDocInDoc.getElementsByTagName(GCConstants.COMMISSIONS).item(0);

      if (YFCCommon.isVoid(eleCommissions)) {
        YFCNodeList<YFCElement> yfcNLCommission =
            yfcEleShipLine.getElementsByTagName(GCXmlLiterals.GC_ORDER_LINE_COMMISSION);
        for (YFCElement yfcEleOrderLineCommission : yfcNLCommission) {
          String sEmployeeID = yfcEleOrderLineCommission.getAttribute(GCConstants.USER_ID);
          Double dAllocationPercentage =
              yfcEleOrderLineCommission.getDoubleAttribute(GCConstants.ALLOCATION_PERCENTAGE);
          if (uniqueUsersMap.containsKey(sEmployeeID)) {
            if (dLineTotal != 0.00) {
              Double dNetUserAmount = uniqueUsersMap.get(sEmployeeID) + (dAllocationPercentage * dLineTotal) / 100;
              uniqueUsersMap.put(sEmployeeID, dNetUserAmount);
            } else {
              Double dNetUserAmount = 0.00;
              uniqueUsersMap.put(sEmployeeID, dNetUserAmount);
            }
          } else {
            if (dLineTotal != 0.00) {
              Double dNetUserAmount = (dAllocationPercentage * dLineTotal) / 100;
              uniqueUsersMap.put(sEmployeeID, dNetUserAmount);
            } else {
              Double dNetUserAmount = 0.00;
              uniqueUsersMap.put(sEmployeeID, dNetUserAmount);
            }
          }
        }
      } else {
        YFCIterable<YFCElement> yfcNLCommission = eleCommissions.getChildren();
        for (YFCElement yfcEleOrderLineCommission : yfcNLCommission) {
          String sEmployeeID = yfcEleOrderLineCommission.getAttribute(GCConstants.SALES_PERSON);
          Double dAllocationPercentage = yfcEleOrderLineCommission.getDoubleAttribute(GCConstants.SALES_PERCENT);
          if (uniqueUsersMap.containsKey(sEmployeeID)) {
            if (dLineTotal != 0.00) {
              Double dNetUserAmount = uniqueUsersMap.get(sEmployeeID) + (dAllocationPercentage * dLineTotal) / 100;
              uniqueUsersMap.put(sEmployeeID, dNetUserAmount);
            } else {
              Double dNetUserAmount = 0.00;
              uniqueUsersMap.put(sEmployeeID, dNetUserAmount);
            }
          } else {
            if (dLineTotal != 0.00) {
              Double dNetUserAmount = (dAllocationPercentage * dLineTotal) / 100;
              uniqueUsersMap.put(sEmployeeID, dNetUserAmount);
            } else {
              Double dNetUserAmount = 0.00;
              uniqueUsersMap.put(sEmployeeID, dNetUserAmount);
            }
          }
        }
      }
    }

    YFCElement yfcEleCommissions = yfcEleRootInDoc.createChild(GCConstants.COMMISSIONS);
    yfcEleRootInDoc.appendChild(yfcEleCommissions);
    Iterator<Map.Entry<String, Double>> entries = uniqueUsersMap.entrySet().iterator();

    Double dTotalPercentage = 0.00;
    while (entries.hasNext()) {
      Map.Entry<String, Double> entry = entries.next();
      /* Append new commission information on input XML - Start */
      intSequenceNo++;

      BigDecimal bNetUserPercentage;

      if (dTotalAmount != 0.00) {
        bNetUserPercentage = GCCommonUtil.formatNumberForDecimalPrecision((entry.getValue() * 100) / dTotalAmount, 2);
      } else {
        bNetUserPercentage = GCCommonUtil.formatNumberForDecimalPrecision(0.00, 2);
      }
      Double dNetUserPercentage = bNetUserPercentage.doubleValue();
      dTotalPercentage = dTotalPercentage + dNetUserPercentage;

      if (!entries.hasNext() && dTotalPercentage != 100.00) {
        Double dRemainingAmount = dTotalPercentage - 100.00;
        dNetUserPercentage = dNetUserPercentage - dRemainingAmount;
      }

      String percent = String.valueOf(GCCommonUtil.formatNumberForDecimalPrecision(dNetUserPercentage, 2));

      // Fix for GCSTORE#2921 -- Avoiding the addition of commission details whose percentage is 0.
      if (!(YFCUtils.equals(percent, "0.00") || dNetUserPercentage == 0.00)) {

        YFCElement yfcEleCommission = yfcEleRootInDoc.createChild(GCConstants.COMMISSION);
        yfcEleCommissions.appendChild(yfcEleCommission);
        yfcEleCommission.setAttribute(GCConstants.LINE_NUMBER, intSequenceNo.toString());
        yfcEleCommission.setAttribute(GCXmlLiterals.EMPLOYEE_ID, entry.getKey());
        yfcEleCommission.setAttribute(GCXmlLiterals.PERCENTAGE, percent);
      }
      /* Append new commission information on input XML - End */
    }

    if (!yfcEleCommissions.hasChildNodes()) {
      YFCElement yfcEleCommission = yfcEleRootInDoc.createChild(GCConstants.COMMISSION);
      yfcEleCommissions.appendChild(yfcEleCommission);
      String sShipNode = GCXMLUtil.getAttributeFromXPath(yfcDocInDoc.getDocument(), "//Shipment/@ShipNode");
      yfcEleCommission.setAttribute(GCConstants.LINE_NUMBER, GCConstants.ONE);
      yfcEleCommission.setAttribute(GCXmlLiterals.EMPLOYEE_ID, sShipNode.concat("999"));
      yfcEleCommission.setAttribute(GCXmlLiterals.PERCENTAGE, "100");
    }



    // End of for loop over commissions

    LOGGER.verbose("yfcDocInDoc after processing :" + yfcDocInDoc.toString());
    LOGGER.endTimer("GCGetPOSReservationID.getAndStampCommissionDetails()");
    LOGGER.verbose("GCGetPOSReservationID - getAndStampCommissionDetails() : End");
    return yfcEleRootInDoc;
  }
  // Fix for GCSTORE#3225 -- Ends
}
