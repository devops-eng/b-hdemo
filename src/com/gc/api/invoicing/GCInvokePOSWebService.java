/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE:
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0              03/03/2015         Pande, Anuj                Invoke POS Web Service Utill
 *#################################################################################################################################################################
 */
package com.gc.api.invoicing;

import java.io.IOException;
import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCWebServiceUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class GCInvokePOSWebService implements YIFCustomApi {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCInvokePOSWebService.class.getName());
  Properties prop = new Properties();

  /**
   * This method is a common Web service method for POS web service calls It will fetch URL & Soap
   * action details from service
   * 
   * @param env
   * @param inDoc
   * @return
   * @throws Exception
   */
  public Document invokeWebService(YFSEnvironment env, Document inDoc){
    LOGGER.beginTimer("GCInvokePOSWebService.invokeWebService()");
    LOGGER.verbose("GCInvokePOSWebService - invokeWebService() : Start");

    String sURL = prop.getProperty(GCXmlLiterals.URL);
    String sAction = prop.getProperty(GCXmlLiterals.ACTION);

    GCWebServiceUtil webServiceObj = new GCWebServiceUtil();
    Document docInvokeWebServiceOutput = null;
    try{
      docInvokeWebServiceOutput = webServiceObj.invokeSOAPWebService(inDoc, sURL, sAction);  
    }catch(IOException excep){
      YFSException yfsExcep = new YFSException();
      LOGGER.error("Inside catch block of GCInvokePOSWebService.invokeWebService():", excep);
      LOGGER.endTimer("GCInvokePOSWebService.invokeWebService()");
      LOGGER.verbose("GCInvokePOSWebService - invokeWebService() : End");
      throw yfsExcep;
    }
    
    LOGGER.endTimer("GCInvokePOSWebService.invokeWebService()");
    LOGGER.verbose("GCInvokePOSWebService - invokeWebService() : End");

    return docInvokeWebServiceOutput;
  }

  @Override
  public void setProperties(Properties paramProperties){
    prop = paramProperties;
  }
}
