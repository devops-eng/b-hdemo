/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 * *###########################################
 * ######################################################
 * ################################################################ OBJECTIVE: This class will
 * prepare input for publish invoice to POS.
 * ########################################################
 * ##########################################
 * ############################################################### Version Date Modified By
 * Description
 * ######################################################################################
 * ########################################################################### 1.0 03/03/2015 Pande,
 * Anuj POS Invoicing
 * ###############################################################################
 * ##################################################################################
 */
package com.gc.api.invoicing;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.gc.api.returns.GCPublishReturnInvoice;
import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class GCPOSInvoicing {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCPOSInvoicing.class.getName());

  Document docGetOrderListOutput = null;
  boolean bAnyBundleLineExists = true;
  List<YFCElement> bundleParentLinesList = new ArrayList<YFCElement>();
  // GCSTORE 5337 START
  Document removedLineDetailsDoc = GCXMLUtil.createDocument(GCConstants.LINE_DETAILS);
  Element removedEleLineDetails = removedLineDetailsDoc.getDocumentElement();

  // GCSTORE 5337 END



  /**
   * This method will prepare POS request and call POS to get POS sales ticket number
   *
   * @param env
   * @param inDoc
   * @return
   * @throws GCException
   * @throws TransformerException
   * @throws Exception
   */
  public void createAndPublishPOSInvoice(YFSEnvironment env, Document inDoc) throws GCException, TransformerException {
    LOGGER.beginTimer("GCPOSInvoicing.createAndPublishPOSInvoice()");
    LOGGER.verbose("GCPOSInvoicing - createAndPublishPOSInvoice() : Start");

    Element inDocRootEle = inDoc.getDocumentElement();
    YFCDocument yfcDocInDoc = YFCDocument.getDocumentFor(inDoc);
    // pro-rate bundle line price , Charges & taxes
    Map<String, String> mapBundleParentComponent = new HashMap<String, String>();
    prorateBundleCharges(inDoc , mapBundleParentComponent );

    handleZeroTaxPercentage(YFCDocument.getDocumentFor(inDoc));

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("InDoc After bundle Proration to components: " + yfcDocInDoc);
    }
    LOGGER.verbose("Any bundle line exsits " + bAnyBundleLineExists);
    if (bAnyBundleLineExists) {
      clubOrderLinesWithSimilarComps(inDoc , mapBundleParentComponent, removedLineDetailsDoc, removedEleLineDetails);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("InDoc after combining same components: " + yfcDocInDoc);
      }
    }

    YFCElement yfcEleRootInDoc = yfcDocInDoc.getDocumentElement();
    boolean bIfOnlyDependentLines = true;

    // If TaxExemptFlag is Y, then fetch and stamp Tax exemption id under Resale number
    YFCElement yfcEleOrder = yfcEleRootInDoc.getElementsByTagName(GCConstants.ORDER).item(0);
    String sTaxExemptFlag = yfcEleOrder.getAttribute(GCConstants.TAX_EXEMPT_FLAG);
    if (YFCCommon.equalsIgnoreCase(GCConstants.YES, sTaxExemptFlag)) {
      // invoke get Customer list API
      String sBillToId = yfcEleOrder.getAttribute(GCConstants.BILL_TO_ID);
      YFCDocument yfcDocGetCustomerListInp = YFCDocument.createDocument(GCConstants.CUSTOMER);
      yfcDocGetCustomerListInp.getDocumentElement().setAttribute(GCConstants.CUSTOMER_ID, sBillToId);
      YFCDocument yfcDocGetCustomerListTemp =
          YFCDocument
          .getDocumentFor("<CustomerList><Customer CustomerID='' ><Extn ExtnTaxExemptId='' /></Customer></CustomerList>");
      YFCDocument yfcDocGetCustomerListOup =
          GCCommonUtil.invokeAPI(env, GCConstants.GET_CUSTOMER_LIST, yfcDocGetCustomerListInp,
              yfcDocGetCustomerListTemp);
      YFCElement yfcEleCustomer = yfcDocGetCustomerListOup.getDocumentElement().getChildElement(GCConstants.CUSTOMER);
      if (!YFCCommon.isVoid(yfcEleCustomer)) {
        String sTaxExemptionId =
            yfcEleCustomer.getChildElement(GCConstants.EXTN).getAttribute(GCXmlLiterals.EXTN_TAX_EXEMPT_ID);
        yfcEleRootInDoc.setAttribute(GCXmlLiterals.RESALE_NUMBER, sTaxExemptionId);
      }
    }

    // Fix for GCSTORE-5470 : Adding payment type as COA for collection detail with ChargeType as
    // TRANSFER_IN.
    String sOrderPurpose = yfcEleOrder.getAttribute("OrderPurpose");
    if (YFCCommon.equals(sOrderPurpose, GCConstants.EXCHANGE)) {
      YFCNodeList<YFCElement> yfcNLCollectionDetail = yfcEleRootInDoc.getElementsByTagName("CollectionDetail");
      for (int i = 0; i < yfcNLCollectionDetail.getLength(); i++) {
        YFCElement yfcEleCollectionDetail = yfcNLCollectionDetail.item(i);
        String sChargeType = yfcEleCollectionDetail.getAttribute("ChargeType");
        if (YFCCommon.equals(sChargeType, "TRANSFER_IN")) {
          YFCElement yfcElePaymentMethod = yfcEleCollectionDetail.createChild("PaymentMethod");
          yfcElePaymentMethod.setAttribute(GCConstants.PAYMENT_TYPE, GCConstants.CREDIT_ON_ACCOUNT);
          yfcElePaymentMethod.setAttribute("TotalCharged",
              yfcEleCollectionDetail.getAttribute(GCConstants.AMOUNT_COLLECTED));
        }
      }
    }

    // If Payment Type is COA or GIFT_CARD(GCSTORE#6526), send async call to DAX
    YFCNodeList<YFCElement> yfcNLPaymentMethod = yfcEleRootInDoc.getElementsByTagName(GCConstants.PAYMENT_METHOD);
    for (int i = 0; i < yfcNLPaymentMethod.getLength(); i++) {
      YFCElement yfcElePaymentMethod = yfcNLPaymentMethod.item(i);
      if (!YFCCommon.isVoid(yfcElePaymentMethod)
          && (YFCCommon.equals(GCConstants.CREDIT_ON_ACCOUNT,
              yfcElePaymentMethod.getAttribute(GCConstants.PAYMENT_TYPE))  || YFCCommon.equals(GCConstants.GIFT_CARD,
                  yfcElePaymentMethod.getAttribute(GCConstants.PAYMENT_TYPE)))) {
        GCCommonUtil.invokeService(env, GCConstants.GC_DAX_TO_POS_COA_TRANSFER_SERVICE, inDoc);
        break;
      }
    }


    /* Check if any line is fulfilled from MFI - Start */
    String setSplitOrderFlag = GCConstants.FALSE_STRING;
    if (!YFCCommon
        .isVoid(GCXMLUtil.getElementByXPath(
            yfcDocInDoc.getDocument(),
            "Shipments/Shipment/ShipmentLines/ShipmentLine/OrderLine/Order/OrderLines/OrderLine/Schedules/Schedule[@ShipNode='MFI']"))) {
      setSplitOrderFlag = GCConstants.TRUE_STRING;
    }
    yfcEleRootInDoc.setAttribute(GCXmlLiterals.SPLIT_ORDER_FLAG, setSplitOrderFlag);
    /* Check if any line is fulfilled from MFI - End */

    // Check if Invoice type is Shipment or Credit memo
    YFCElement yfcEleInvoiceHeader = yfcEleRootInDoc.getElementsByTagName(GCConstants.INVOICE_HEADER).item(0);
    if (!YFCCommon.isVoid(yfcEleInvoiceHeader)
        && GCConstants.SHIPMENT.equalsIgnoreCase(yfcEleInvoiceHeader.getAttribute(GCXmlLiterals.INVOICE_TYPE))) {
      // Set Message Type as CONFIRM on root node
      LOGGER.verbose("Invoice Type: Shipment");
      yfcEleRootInDoc.setAttribute(GCConstants.MESSAGE_TYPE, GCXmlLiterals.CONFIRM);

      YFCElement yfcEleLineDetails = yfcEleInvoiceHeader.getElementsByTagName(GCConstants.LINE_DETAILS).item(0);
      YFCNodeList<YFCElement> yfcNLOrderLine = yfcEleLineDetails.getElementsByTagName(GCConstants.ORDER_LINE);

      getManagerCashierApproveId(yfcEleRootInDoc, yfcEleInvoiceHeader);
      // Stamp Commissions on invoice header
      getAndStampCommissionDetails(yfcDocInDoc);

      for (int i = 0; i < yfcNLOrderLine.getLength(); i++) {
        YFCElement yfcEleOrderLine = yfcNLOrderLine.item(i);

        // Invoke getAndStampCommissionDetails to get updated commissions


        // Check condition if only warranty lines are processed
        if (bIfOnlyDependentLines) {
          String sDependentLineKey = yfcEleOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
          if (YFCCommon.isStringVoid(sDependentLineKey)) {
            bIfOnlyDependentLines = false;
          } else {
            onlyDependentLinesInvoiced(env, yfcEleRootInDoc, yfcEleOrderLine, sDependentLineKey);
          }
        }
      }
      // End of for loop over order line
    }
    // End of if to check for invoice type

    // If all lines are warranty lines
    if (bIfOnlyDependentLines) {
      yfcEleRootInDoc.setAttribute(GCXmlLiterals.IS_WARRANTY_LINE_ONLY, GCConstants.TRUE);
      yfcEleRootInDoc.setAttribute(GCConstants.MESSAGE_TYPE,"SALE");

    }
    if (YFCUtils.equals(yfcEleRootInDoc.getAttribute(GCConstants.MESSAGE_TYPE), "SALE")
        || YFCUtils.equals(yfcEleRootInDoc.getAttribute(GCConstants.MESSAGE_TYPE), "CONFIRM")) {
      stampLatestPriceOverrideNote(yfcDocInDoc);
    }

    // Invoke getAndStampDates method to get order and XML creation date and time
    getAndStampDates(yfcEleRootInDoc);

    // Invoke service to form input XML for web service call
    YFCDocument yfcDocInvokeWebServiceInput =
        GCCommonUtil.invokeService(env, GCXmlLiterals.GC_PREPARE_POS_WEB_SERVICE_INPUT, yfcDocInDoc);

    //GCSTORE-3197 : Start
    double dTotalDMDiscountAmount = 0.00;
    YFCNodeList<YFCElement> nlDMDiscountAmount =
        yfcDocInvokeWebServiceInput.getElementsByTagName("DMDiscountAmount");
    for (YFCElement eleDMDiscountAmount : nlDMDiscountAmount) {
      String sDMDiscountAmount = eleDMDiscountAmount.getNodeValue();
      if(!YFCCommon.isStringVoid(sDMDiscountAmount)){
        dTotalDMDiscountAmount = dTotalDMDiscountAmount + Double.parseDouble(sDMDiscountAmount);
      }
    }

    YFCNodeList<YFCElement> nlTotalDiscounts =
        yfcDocInvokeWebServiceInput.getElementsByTagName("TotalDiscounts");
    YFCElement eleTotaDiscounts = nlTotalDiscounts.item(0);
    double dTotalDiscounts = Double.parseDouble(eleTotaDiscounts.getNodeValue());
    double dTempFinalDiscount = dTotalDiscounts + dTotalDMDiscountAmount;
    String sTempFinalDiscount = String.format("%.2f", dTempFinalDiscount);
    eleTotaDiscounts.setNodeValue(sTempFinalDiscount);

    // GCSTORE-3197 : End
    // GCSTORE-2837 : Start
    YFCNodeList<YFCElement> nlPOSAuthNumber =
        yfcDocInvokeWebServiceInput.getElementsByTagName(GCXmlLiterals.POS_AUTH_NUMBER);
    for (YFCElement elePOSAuthNumber : nlPOSAuthNumber) {
      String sAuthNumber = elePOSAuthNumber.getNodeValue();
      int iAuthNumberLength = sAuthNumber.length();
      if (iAuthNumberLength < GCXmlLiterals.FIVE_INT) {
        iAuthNumberLength = GCXmlLiterals.FIVE_INT - iAuthNumberLength;
        for (int iLength = 1; iLength <= iAuthNumberLength; iLength++) {
          sAuthNumber = sAuthNumber + GCXmlLiterals.SPACE_STRING;
        }
        elePOSAuthNumber.setNodeValue(sAuthNumber);
      }
    }
    // GCSTORE-2837 : End

    // Invoke web service with POS input XML

    // Decrypt CreditCard No from OMS private key and encrypt using POS public key.
    GCPublishReturnInvoice.encryptDecryptCCForPOS(yfcDocInvokeWebServiceInput);

    YFCDocument yfcDocInvokeWebServiceOutput = null;
    boolean bIsStandaloneWarranty = false;
    try {
      YFCElement eleMessageType = yfcDocInvokeWebServiceInput.getElementsByTagName("MessageType").item(0);
      String sMessageType = eleMessageType.getNodeValue();
      if(YFCCommon.equalsIgnoreCase(sMessageType, "CONFIRM")){
        yfcDocInvokeWebServiceOutput =
            GCCommonUtil.invokeService(env, GCXmlLiterals.GC_INVOKE_POS_INVOICE_WEB_SERVICE, yfcDocInvokeWebServiceInput);
      } else {
        bIsStandaloneWarranty = true;
        yfcDocInvokeWebServiceOutput =
            GCCommonUtil.invokeService(env, GCXmlLiterals.GC_INVOKE_POS_WARRANTY_INVOICE_WEB_SERVICE, yfcDocInvokeWebServiceInput);
      }

      processWebServiceResponse(env,inDoc,yfcEleInvoiceHeader,yfcDocInvokeWebServiceOutput, bIsStandaloneWarranty, removedLineDetailsDoc);
    } catch (YFSException yfsException) {
      LOGGER.error("Inside catch block of GCPublishReturnInvoice.publishReturnInvoice(): ", yfsException);
      inDocRootEle.setAttribute("POSError", GCErrorConstants.WEB_SERVICE_POS_INVOICE_CALL_TIMEOUT_CODE);
      if(YFCCommon.equals(env.getTxnObject("IsHoldAppiled"), "True")){
        YFCException yfcexcep = new YFCException();
        yfcexcep.setAttribute(GCConstants.ERROR_CODE, GCErrorConstants.WEB_SERVICE_POS_INVOICE_CALL_TIMEOUT_CODE);
        yfcexcep.setAttribute(GCConstants.ERROR_DESCRIPTION, "Could not connect to POS. Timeout");
        throw yfcexcep;
      }else{
        GCPublishReturnInvoice.postMessageAndRollbackTransaction(env, inDoc, yfsException);
      }
    }

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("yfcDocInvokeWebServiceOutput : " + yfcDocInvokeWebServiceOutput);
    }

    LOGGER.endTimer("GCPOSInvoicing.createAndPublishPOSInvoice()");
    LOGGER.verbose("GCPOSInvoicing - createAndPublishPOSInvoice() : End");

  }

  /**
   * This method will fetch the manager id and user id from user activity information where User
   * action is 'Confirm Pick'
   *
   * @param yfcEleRootInDoc
   * @param yfcEleInvoiceHeader
   * @return
   */
  private YFCElement getManagerCashierApproveId(YFCElement yfcEleRootInDoc, YFCElement yfcEleInvoiceHeader) {
    LOGGER.beginTimer("GCPOSInvoicing.getManagerCashierApproveId()");
    LOGGER.verbose("GCPOSInvoicing.getManagerCashierApproveId() : Start");
    String sShipmentKey =
        yfcEleInvoiceHeader.getElementsByTagName(GCConstants.SHIPMENT).item(0).getAttribute(GCConstants.SHIPMENT_KEY);
    String sOrderHeaderKey =
        yfcEleInvoiceHeader.getElementsByTagName(GCConstants.ORDER).item(0).getAttribute(GCConstants.ORDER_HEADER_KEY);
    String sManagerID = GCConstants.BLANK_STRING;
    String sUserID = GCConstants.BLANK_STRING;

    /* Fetch manager approve ID and Cashier ID for the shipment from custom table - Start */
    YFCNodeList<YFCElement> yfcNLGCUserActivity =
        yfcEleInvoiceHeader.getElementsByTagName(GCConstants.GC_USER_ACTIVITY);
    for (int j = 0; j < yfcNLGCUserActivity.getLength(); j++) {
      YFCElement yfcEleGCUserActivity = yfcNLGCUserActivity.item(j);
      String sUserActivityShipmentKey = yfcEleGCUserActivity.getAttribute(GCConstants.SHIPMENT_KEY);
      String sUserActivityOrderHeaderKey = yfcEleGCUserActivity.getAttribute(GCConstants.ORDER_HEADER_KEY);
      String sUserActivityUserAction = yfcEleGCUserActivity.getAttribute(GCConstants.USER_ACTION);
      if (YFCCommon.equals(sShipmentKey, sUserActivityShipmentKey)
          && YFCCommon.equals(sOrderHeaderKey, sUserActivityOrderHeaderKey)
          && YFCCommon.equals("Ship", sUserActivityUserAction)) {
        sManagerID = yfcEleGCUserActivity.getAttribute(GCConstants.MANAGER_ID);
        sUserID = yfcEleGCUserActivity.getAttribute(GCConstants.USER_ID);
        break;
      }
    }

    if (YFCCommon.isVoid(sUserID) && YFCCommon.isVoid(sManagerID)) {
      for (int j = 0; j < yfcNLGCUserActivity.getLength(); j++) {
        YFCElement yfcEleGCUserActivity = yfcNLGCUserActivity.item(j);
        String sUserActivityShipmentKey = yfcEleGCUserActivity.getAttribute(GCConstants.SHIPMENT_KEY);
        String sUserActivityOrderHeaderKey = yfcEleGCUserActivity.getAttribute(GCConstants.ORDER_HEADER_KEY);
        String sUserActivityUserAction = yfcEleGCUserActivity.getAttribute(GCConstants.USER_ACTION);
        if (YFCCommon.equals(sShipmentKey, sUserActivityShipmentKey)
            && YFCCommon.equals(sOrderHeaderKey, sUserActivityOrderHeaderKey)
            && YFCCommon.equals("Confirm Pick", sUserActivityUserAction)) {
          sManagerID = yfcEleGCUserActivity.getAttribute(GCConstants.MANAGER_ID);
          sUserID = yfcEleGCUserActivity.getAttribute(GCConstants.USER_ID);
          break;
        }
      }
    }

    /* Fetch manager approve ID and Cashier ID for the shipment from custom table - End */
    /* Set Manager Id and Cashier ID for the shipment - Start */
    yfcEleRootInDoc.setAttribute(GCConstants.MANAGER_ID, sManagerID);
    yfcEleRootInDoc.setAttribute(GCConstants.USER_ID, sUserID);
    /* Set Manager Id and Cashier ID for the shipment - End */

    LOGGER.endTimer("GCPOSInvoicing.getManagerCashierApproveId()");
    LOGGER.verbose("GCPOSInvoicing.getManagerCashierApproveId() : End");
    return yfcEleRootInDoc;
  }

  /**
   * Method to fetch details if all lines invoiced are warranty lines
   *
   * @param env
   * @param yfcEleRootInDoc
   * @param yfcEleOrderLine
   * @param sDependentLineKey
   * @return
   */
  private YFCElement onlyDependentLinesInvoiced(YFSEnvironment env, YFCElement yfcEleRootInDoc,
      YFCElement yfcEleOrderLine, String sDependentLineKey) {
    LOGGER.beginTimer("GCPOSInvoicing.onlyDependentLinesInvoiced()");
    LOGGER.verbose("GCPOSInvoicing - onlyDependentLinesInvoiced() : Start");
    String sExtnPOSItemID = null;
    if (YFCCommon.isVoid(docGetOrderListOutput)) {
      // Invoke get order list API
      YFCDocument yfcDocGetOrderListInput = YFCDocument.createDocument(GCConstants.ORDER);
      YFCElement yfcEleOrder = yfcEleRootInDoc.getElementsByTagName(GCConstants.ORDER).item(0);
      yfcDocGetOrderListInput.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY,
          yfcEleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY));

      docGetOrderListOutput =
          GCCommonUtil.invokeAPI(env, yfcDocGetOrderListInput.getDocument(), GCConstants.GET_ORDER_LIST,
              "global/template/api/getOrderListTempForPOSInvoice.xml");
    }
    Element eleOrderLine =
        GCXMLUtil.getElementByXPath(docGetOrderListOutput, "OrderList/Order/OrderLines/OrderLine[@OrderLineKey='"
            + sDependentLineKey + "']");
    Element eleOrderLineExtn = (Element) eleOrderLine.getElementsByTagName(GCConstants.EXTN).item(0);
    Element eleItem = (Element) eleOrderLine.getElementsByTagName(GCConstants.ITEM).item(0);
    String productCLass = eleItem.getAttribute(GCConstants.PRODUCT_CLASS);
    if (!YFCCommon.isVoid(eleOrderLineExtn)) {
      String sPOSSalesTicketNo = eleOrderLineExtn.getAttribute(GCXmlLiterals.EXTN_POS_SALES_TICKET_NO);
      String sPrimeLineNo = eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
      Element eleSchedule = (Element) eleOrderLine.getElementsByTagName(GCConstants.SCHEDULE).item(0);
      String sShipNode = eleSchedule.getAttribute(GCConstants.SHIP_NODE);
      yfcEleRootInDoc.setAttribute(GCXmlLiterals.POS_SALES_TICKET_NO, sPOSSalesTicketNo);
      yfcEleRootInDoc.setAttribute(GCXmlLiterals.POS_LOCATION, sShipNode);
      if (YFCUtils.equals(GCConstants.SET, productCLass)) {
        sExtnPOSItemID = eleOrderLineExtn.getAttribute(GCXmlLiterals.EXTN_POS_SKU_NUMBER);
        if (YFCCommon.isVoid(sExtnPOSItemID)) {
          Element eleItemDetails = (Element) eleOrderLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
          Element eleItemDetailsExtn = (Element) eleItemDetails.getElementsByTagName(GCConstants.EXTN).item(0);
          if (!YFCCommon.isVoid(eleItemDetailsExtn)) {
            String primaryCompId = eleItemDetailsExtn.getAttribute(GCConstants.EXTN_PRIMARY_COMPONENT_ID);
            Element eleSetOrderLine =
                GCXMLUtil.getElementByXPath(docGetOrderListOutput,
                    "OrderList/Order/OrderLines/OrderLine/Item[@ItemID='" + primaryCompId + "']/..");
            Element extnOrdLineSet = (Element) eleSetOrderLine.getElementsByTagName(GCConstants.EXTN).item(0);
            if (!YFCCommon.isVoid(extnOrdLineSet)) {
              sExtnPOSItemID = extnOrdLineSet.getAttribute(GCXmlLiterals.EXTN_POS_SKU_NUMBER);
            }
          }
        }
      }
      else {
        sExtnPOSItemID = eleOrderLineExtn.getAttribute(GCXmlLiterals.EXTN_POS_SKU_NUMBER);
      }
      if(!YFCCommon.isVoid(sExtnPOSItemID)){
        yfcEleOrderLine.setAttribute("ParentPOSItemID", sExtnPOSItemID);
      }
    }

    LOGGER.endTimer("GCPOSInvoicing.onlyDependentLinesInvoiced()");
    LOGGER.verbose("GCPOSInvoicing - onlyDependentLinesInvoiced() : End");
    return yfcEleRootInDoc;
  }

  /**
   * Method to get and stamp Order and current system date & time
   *
   * @param yfcEleRootInDoc
   * @return
   */
  public static YFCElement getAndStampDates(YFCElement yfcEleRootInDoc) {
    LOGGER.beginTimer("GCPOSInvoicing.getAndStampDates()");
    LOGGER.verbose("GCPOSInvoicing - getAndStampDates() : Start");

    YFCElement yfcEleOrder = yfcEleRootInDoc.getElementsByTagName(GCConstants.ORDER).item(0);
    Date dOrderDateTime = yfcEleOrder.getDateAttribute(GCConstants.ORDER_DATE);
    GCDateUtils.getDateTimeForPOS(yfcEleRootInDoc, dOrderDateTime);

    LOGGER.endTimer("GCPOSInvoicing.getAndStampDates()");
    LOGGER.verbose("GCPOSInvoicing - getAndStampDates() : End");

    return yfcEleRootInDoc;
  }

  /**
   * Method to get and stamp updated commission details
   *
   * @param yfcEleRootInDoc
   * @param yfcEleOrderLine
   * @param yfcEleCommissions
   * @return
   */
  public YFCElement getAndStampCommissionDetails(YFCDocument yfcDocInDoc) {
    LOGGER.beginTimer("GCPOSInvoicing.getAndStampCommissionDetails()");
    LOGGER.verbose("GCPOSInvoicing - getAndStampCommissionDetails() : Start");

    YFCElement yfcEleRootInDoc = yfcDocInDoc.getDocumentElement();
    Double dTotalAmount = yfcEleRootInDoc.getChildElement(GCConstants.INVOICE_HEADER).getDoubleAttribute("TotalAmount");
    Integer intSequenceNo = 0;

    Map<String, Double> uniqueUsersMap = new HashMap<String, Double>();
    // Loop Over OrderLines
    YFCNodeList<YFCElement> yfcNLLineDetails = yfcEleRootInDoc.getElementsByTagName(GCConstants.LINE_DETAIL);
    for(int i=0;i<yfcNLLineDetails.getLength();i++){
      YFCElement yfcEleLineDetail = yfcNLLineDetails.item(i);

      // Double dLineTotal = yfcEleLineDetail.getDoubleAttribute(GCConstants.LINE_TOTAL);
      // GCSTORE-5661-Start
      YFCElement yfcEleLineDetailOrderLine = yfcEleLineDetail.getChildElement("OrderLine");
      String sBundleParentOrderLineKey = yfcEleLineDetailOrderLine.getAttribute("BundleParentOrderLineKey");
      Double dLineTotal;
      if (YFCCommon.isVoid(sBundleParentOrderLineKey)) {
        dLineTotal = yfcEleLineDetail.getDoubleAttribute(GCConstants.LINE_TOTAL);
      } else {
        Double dExtendedPrice = yfcEleLineDetail.getDoubleAttribute("ExtendedPrice");
        Double dTax = yfcEleLineDetail.getDoubleAttribute("Tax");
        dLineTotal = dExtendedPrice + dTax;
      }
      // GCSTORE-5661-End
      // Loop over commission lines
      YFCNodeList<YFCElement> yfcNLCommission =
          yfcEleLineDetail.getElementsByTagName(GCXmlLiterals.GC_ORDER_LINE_COMMISSION);
      for (int j = 0; j < yfcNLCommission.getLength(); j++) {
        YFCElement yfcEleOrderLineCommission = yfcNLCommission.item(j);
        String sEmployeeID = yfcEleOrderLineCommission.getAttribute(GCConstants.USER_ID);
        Double dAllocationPercentage = yfcEleOrderLineCommission.getDoubleAttribute(GCConstants.ALLOCATION_PERCENTAGE);
        if (uniqueUsersMap.containsKey(sEmployeeID)) {
          if (dLineTotal != 0.00) {
            Double dNetUserAmount = uniqueUsersMap.get(sEmployeeID) + (dAllocationPercentage * dLineTotal) / 100;
            uniqueUsersMap.put(sEmployeeID, dNetUserAmount);
          } else {
            Double dNetUserAmount = 0.00;
            uniqueUsersMap.put(sEmployeeID, dNetUserAmount);
          }
        } else {
          if (dLineTotal != 0.00) {
            Double dNetUserAmount = (dAllocationPercentage * dLineTotal) / 100;
            uniqueUsersMap.put(sEmployeeID, dNetUserAmount);
          } else {
            Double dNetUserAmount = 0.00;
            uniqueUsersMap.put(sEmployeeID, dNetUserAmount);
          }
        }
      }
    }

    YFCElement yfcEleCommissions = yfcEleRootInDoc.createChild(GCConstants.COMMISSIONS);
    yfcEleRootInDoc.appendChild(yfcEleCommissions);
    Iterator<Map.Entry<String, Double>> entries = uniqueUsersMap.entrySet().iterator();

    Double dTotalPercentage = 0.00;
    while (entries.hasNext()) {
      Map.Entry<String, Double> entry = entries.next();
      /* Append new commission information on input XML - Start */
      intSequenceNo++;

      BigDecimal bNetUserPercentage ;

      if(dTotalAmount!=0.00){
        bNetUserPercentage = GCCommonUtil.formatNumberForDecimalPrecision((entry.getValue() * 100) / dTotalAmount, 2);
      }
      else {
        bNetUserPercentage = GCCommonUtil.formatNumberForDecimalPrecision(0.00, 2);
      }
      Double dNetUserPercentage = bNetUserPercentage.doubleValue();
      dTotalPercentage =  dTotalPercentage + dNetUserPercentage;

      if(!entries.hasNext() && dTotalPercentage != 100.00){
        Double dRemainingAmount = dTotalPercentage - 100.00;
        dNetUserPercentage = dNetUserPercentage - dRemainingAmount;
      }

      String percent = String.valueOf(GCCommonUtil.formatNumberForDecimalPrecision(dNetUserPercentage, 2));

      // Fix for GCSTORE#2921 -- Avoiding the addition of commission details whose percentage is 0.
      if (!(YFCUtils.equals(percent, "0.00") || dNetUserPercentage == 0.00)) {

        YFCElement yfcEleCommission = yfcEleRootInDoc.createChild(GCConstants.COMMISSION);
        yfcEleCommissions.appendChild(yfcEleCommission);
        yfcEleCommission.setAttribute(GCConstants.LINE_NUMBER, intSequenceNo.toString());
        yfcEleCommission.setAttribute(GCXmlLiterals.EMPLOYEE_ID, entry.getKey());
        yfcEleCommission.setAttribute(GCXmlLiterals.PERCENTAGE, percent);
      }
      /* Append new commission information on input XML - End */
    }

    if(!yfcEleCommissions.hasChildNodes()){
      YFCElement yfcEleCommission = yfcEleRootInDoc.createChild(GCConstants.COMMISSION);
      yfcEleCommissions.appendChild(yfcEleCommission);
      String sShipNode = GCXMLUtil.getAttributeFromXPath(yfcDocInDoc.getDocument(), "//Shipment/@ShipNode");
      yfcEleCommission.setAttribute(GCConstants.LINE_NUMBER, GCConstants.ONE);
      yfcEleCommission.setAttribute(GCXmlLiterals.EMPLOYEE_ID, sShipNode.concat("999"));
      yfcEleCommission.setAttribute(GCXmlLiterals.PERCENTAGE,"100");
    }



    // End of for loop over commissions

    LOGGER.endTimer("GCPOSInvoicing.getAndStampCommissionDetails()");
    LOGGER.verbose("GCPOSInvoicing - getAndStampCommissionDetails() : End");
    return yfcEleRootInDoc;
  }

  /**
   * This method will pro-rate bundle price / charges / taxes to its component items
   *
   * @param inDoc
   * @throws TransformerException
   */
  public Document prorateBundleCharges(Document inDoc, Map<String, String> mapBundleParentComponent) throws TransformerException {
    LOGGER.beginTimer("GCPOSInvoicing.prorateBundleCharges()");
    LOGGER.verbose("GCPOSInvoicing - prorateBundleCharges() : Start");

    Boolean isPrimaryNegative = false;
    Double dPrimaryFinalTotalAfterDiscount = 0.0;
    List<Element> listBundleParentLineDetail =
        GCXMLUtil.getElementListByXpath(inDoc,
            "InvoiceDetail/InvoiceHeader/LineDetails/LineDetail/OrderLine[@IsBundleParent='Y']/..");
    BigDecimal dTotalTax = new BigDecimal(0);
    String sTotalTax = GCXMLUtil.getAttributeFromXPath(inDoc, "InvoiceDetail/InvoiceHeader/@TotalTax");
    if (!YFCCommon.isVoid(sTotalTax)) {
      dTotalTax = BigDecimal.valueOf(Double.valueOf(sTotalTax));
    }
    if (listBundleParentLineDetail.isEmpty()) {
      bAnyBundleLineExists = false;
    } else {
      Element eleLineDetails = (Element) inDoc.getElementsByTagName(GCConstants.LINE_DETAILS).item(0);
      Iterator<Element> iteratorListBundleLineDetail = listBundleParentLineDetail.iterator();
      while (iteratorListBundleLineDetail.hasNext()) {
        Element eleBundleParentLineDetail = iteratorListBundleLineDetail.next();
        BigDecimal dTaxProrated = new BigDecimal(0);
        Element eleOrderBundleParentLine =
            (Element) eleBundleParentLineDetail.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
        String sOrderLineKey = eleOrderBundleParentLine.getAttribute(GCConstants.ORDER_LINE_KEY);
        String sBundleUnitPrice = eleBundleParentLineDetail.getAttribute(GCConstants.UNIT_PRICE);
        boolean isSecondaryPriceMore = false;
        boolean isSecondaryComponent = true;
        String mainSecondaryComponent = null;
        Element eleSecondaryComponent = null;
        List<String> listChargeNameForSecondary = new ArrayList<String>();

        Element eleBundleItem =
            (Element) eleOrderBundleParentLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
        Element eleBundleItemExtn = (Element) eleBundleItem.getElementsByTagName(GCConstants.EXTN).item(0);
        String sPrimaryComponent = eleBundleItemExtn.getAttribute(GCXmlLiterals.EXTN_PRIMARY_COMPONENT_ITEM);
        Double dSecondaryComponentPrice = 0.00;
        Double dComponentLineTax = 0.00;

        NodeList nlLineCharge = eleBundleParentLineDetail.getElementsByTagName(GCConstants.LINE_CHARGE);
        NodeList nlLineTax = eleBundleParentLineDetail.getElementsByTagName(GCConstants.LINE_TAX);

        Map<String, Element> mapPrimaryComponent = new HashMap<String, Element>();

        // Fix for 3426 : Start : Fetching Bundle Parent Overridden price.

        boolean bIsPriceOverriden = false;

        Element eleExtn = (Element) eleOrderBundleParentLine.getElementsByTagName(GCConstants.EXTN).item(0);
        String sOverriddenPrice = eleExtn.getAttribute("ExtnOverridedPrice");
        Double dOverridenPrice = 0.00;
        if(!YFCCommon.isVoid(sOverriddenPrice)){
          dOverridenPrice = Double.parseDouble(sOverriddenPrice);
        }
        String sExtnOverridePriceEmpId = eleExtn.getAttribute("ExtnOverridePriceEmpId");

        Element eleLinePriceInfo = (Element) eleOrderBundleParentLine.getElementsByTagName(GCConstants.LINE_PRICE_INFO).item(0);

        // GCSTORE-5733 -- Starts
        String bundleListPrice = eleLinePriceInfo.getAttribute(GCConstants.LIST_PRICE);
        // GCSTORE-5733 -- Ends

        Element elePriceOverrideNote = null;
        NodeList nlNotes = eleOrderBundleParentLine.getElementsByTagName(GCConstants.NOTE);
        for (int i= 0 ; i < nlNotes.getLength() ; i++) {
          Element eleNote = (Element) nlNotes.item(i);
          String sNoteText = eleNote.getAttribute(GCConstants.NOTE_TEXT);
          if(sNoteText.contains("PriceOverrideReasonCode:")){
            elePriceOverrideNote = eleNote;
          }
        }

        if(!YFCCommon.isVoid(elePriceOverrideNote) && dOverridenPrice>0.00){
          bIsPriceOverriden = true;
        }

        // Fix for 3426 : End

        List<Element> listChildLineDetail =
            GCXMLUtil.getElementListByXpath(inDoc,
                "InvoiceDetail/InvoiceHeader/LineDetails/LineDetail/OrderLine[@BundleParentOrderLineKey='"
                    + sOrderLineKey + "']/..");

        // logic : Prorating charges when component unitPrice is greater than parent
        // UnitPrice.-----starts
        for (Element eleChldLineDetail : listChildLineDetail) {

          Element eleChldOrderLine = (Element) eleChldLineDetail.getElementsByTagName(GCConstants.ORDER_LINE).item(0);


          Element eleChldItemDetails =
              (Element) eleChldOrderLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
          String sItemId = eleChldItemDetails.getAttribute(GCConstants.ITEM_ID);
          if (!YFCUtils.equals(sPrimaryComponent, sItemId)) {
            Element eleChldLinePriceInfo =
                (Element) eleChldOrderLine.getElementsByTagName(GCConstants.LINE_PRICE_INFO).item(0);
            Double listprice = Double.parseDouble(eleChldLinePriceInfo.getAttribute(GCConstants.LIST_PRICE));
            Double bundleUnitPrice = Double.parseDouble(sBundleUnitPrice);
            if (listprice > bundleUnitPrice) {
              isSecondaryPriceMore = true;
              mainSecondaryComponent = eleChldOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
              break;
            }
          }
        }
        // logic : Prorating charges when component unitPrice is greater than parent
        // UnitPrice.-----ends

        // Fix for GCSTORE#3392
        String primaryOdrLineKey = null;

        for (Element eleChildLineDetail : listChildLineDetail) {
          Element eleChildOrderLine = (Element) eleChildLineDetail.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
          Element eleChildItemDetails =
              (Element) eleChildOrderLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
          Element eleChildLinePriceInfo =
              (Element) eleChildOrderLine.getElementsByTagName(GCConstants.LINE_PRICE_INFO).item(0);

          // Stamping BundleUnitPrice on each Component LineDetail element.
          eleChildLineDetail.setAttribute("BundleParentUnitPrice", sBundleUnitPrice);

          String sItemId = eleChildItemDetails.getAttribute(GCConstants.ITEM_ID);
          String sListprice = eleChildLinePriceInfo.getAttribute(GCConstants.LIST_PRICE);

          // logic : Prorating charges when component unitPrice is greater than parent
          // UnitPrice.-----starts
          if (isSecondaryPriceMore && (0 != Double.parseDouble(sListprice))) {
            if (YFCUtils.equals(mainSecondaryComponent, eleChildOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY))) {
              sListprice = sBundleUnitPrice;
            } else {
              sListprice = "0.00";
            }
          }
          // logic : Prorating charges when component unitPrice is greater than parent
          // UnitPrice.-----ends

          // Fix for GCSTORE-6650 --- Starts
          String kitQty = eleChildOrderLine.getAttribute("KitQty");
          Double dListPrice = Double.parseDouble(sListprice);
          if (!YFCCommon.isVoid(kitQty)) {
            dListPrice = dListPrice / Double.parseDouble(kitQty);
          }
          // Fix for GCSTORE-6650 --- Ends

          String sShippedQty = eleChildLineDetail.getAttribute(GCConstants.SHIPPED_QUANTITY);
          Double dExtendedPrice = Double.parseDouble(sShippedQty) * dListPrice;

          eleChildLineDetail.setAttribute(GCConstants.UNIT_PRICE, dListPrice.toString());
          eleChildLineDetail.setAttribute(GCXmlLiterals.EXTENDED_PRICE, dExtendedPrice.toString());


          Element eleChildLineTaxes = (Element) eleChildLineDetail.getElementsByTagName(GCConstants.LINE_TAXES).item(0);

          if (YFCCommon.equals(sItemId, sPrimaryComponent)) {
            // Item is a primary item
            mapPrimaryComponent.put(sPrimaryComponent, eleChildLineDetail);
            primaryOdrLineKey = eleChildLineDetail.getAttribute(GCConstants.ORDER_LINE_KEY);
          } else {
            // Item is a secondary item
            dSecondaryComponentPrice += Double.parseDouble(sListprice);
            Double dSecondaryComponentRatio = Double.parseDouble(sListprice) / Double.parseDouble(sBundleUnitPrice);
            if (isSecondaryComponent) {
              eleSecondaryComponent = eleChildLineDetail;
              isSecondaryComponent = false;
            }

            // Loop Over line taxes
            for (int i = 0; i < nlLineTax.getLength(); i++) {
              Element eleBundleLineTax = (Element) nlLineTax.item(i);
              String sTaxCategory = eleBundleLineTax.getAttribute(GCConstants.CHARGE_NAME);
              if (YFCCommon.equals(GCConstants.LINE_PRICE_TAX, sTaxCategory)) {
                Element eleChildLineTax =
                    (Element) XPathAPI.selectSingleNode(eleChildLineTaxes, "LineTax[@ChargeName='" + sTaxCategory
                        + "']");
                if (YFCCommon.isVoid(eleChildLineTax)) {
                  eleChildLineTax = inDoc.createElement(GCConstants.LINE_TAX);
                  eleChildLineTaxes.appendChild(eleChildLineTax);
                  GCXMLUtil.copyElement(inDoc, eleBundleLineTax, eleChildLineTax);
                }

                // Calculate component charge value
                String sTaxAmount = eleBundleLineTax.getAttribute(GCConstants.TAX);
                dComponentLineTax = Double.parseDouble(sTaxAmount) * dSecondaryComponentRatio;
                BigDecimal bdComponentTaxAmount = GCCommonUtil.formatNumberForDecimalPrecision(dComponentLineTax, 2);
                dTaxProrated = dTaxProrated.add(bdComponentTaxAmount);
                eleChildLineTax.setAttribute(GCConstants.TAX, bdComponentTaxAmount.toString());
                eleChildLineDetail.setAttribute(GCConstants.TAX, bdComponentTaxAmount.toString());
                break;
              }
            }

          }
          // End of if secondary Item
        }
        // End of for loop on child items

        // Fix for GCSTORE#3392--Begin
        manageSetWarranty(sOrderLineKey, primaryOdrLineKey, eleLineDetails);
        // Fix for GCSTORE#3392--End

        /* Append Primary Component price, charges and taxes - Start */
        Element elePrimaryItemLine = mapPrimaryComponent.get(sPrimaryComponent);
        Double dFinalPrimaryLineTotal = 0.00;

        Double dPrimaryUnitPrice = Double.parseDouble(sBundleUnitPrice) - dSecondaryComponentPrice;

        // GCSTORE-5733 -- Starts
        Double dPrimaryListPrice = 0.00;
        Double dBundleListPrice = Double.parseDouble(bundleListPrice);
        if (dBundleListPrice > 0.00) {
          dPrimaryListPrice = dBundleListPrice - dSecondaryComponentPrice;
        }
        // GCSTORE-5733 -- Ends

        // Fix for GCSTORE-6650 --- Starts
        Element eleChildOdrLine = (Element) elePrimaryItemLine.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
        String kitQty = eleChildOdrLine.getAttribute("KitQty");
        if (!YFCCommon.isVoid(kitQty)) {
          dPrimaryUnitPrice = dPrimaryUnitPrice / Double.parseDouble(kitQty);
          dPrimaryListPrice = dPrimaryListPrice / Double.parseDouble(kitQty);
          
          //MPOS-2333: Rounding off the prices: Start.        
          dPrimaryUnitPrice = GCXMLUtil.roundOffPrice(dPrimaryUnitPrice);
          dPrimaryListPrice = GCXMLUtil.roundOffPrice(dPrimaryListPrice);
          //MPOS-2333: Rounding off the prices: End.
          
        }
        // Fix for GCSTORE-6650 --- Ends

        String sShippedQty = elePrimaryItemLine.getAttribute(GCConstants.SHIPPED_QUANTITY);
        Double dExtendedPrice = Double.parseDouble(sShippedQty) * dPrimaryUnitPrice;
        elePrimaryItemLine.setAttribute(GCConstants.UNIT_PRICE, dPrimaryUnitPrice.toString());
        elePrimaryItemLine.setAttribute(GCXmlLiterals.EXTENDED_PRICE, dExtendedPrice.toString());

        // GCSTORE-5733 -- Starts
        Element elePmryOrdrLine = (Element) elePrimaryItemLine.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
        Element elePmryLinePriceInfo =
            (Element) elePmryOrdrLine.getElementsByTagName(GCConstants.LINE_PRICE_INFO).item(0);
        elePmryLinePriceInfo.setAttribute(GCConstants.LIST_PRICE, dPrimaryListPrice.toString());
        // GCSTORE-5733 -- Ends
        dFinalPrimaryLineTotal += dExtendedPrice;

        // Fix for 3426 : Resume : Adding overridden price of Bundle Parent on Primary Component.
        if(bIsPriceOverriden){
          Element elePrimaryOrderLine = (Element) elePrimaryItemLine.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
          Element eleNotes = (Element) elePrimaryOrderLine.getElementsByTagName(GCConstants.NOTES).item(0);
          Element eleChildExtn = (Element) elePrimaryOrderLine.getElementsByTagName(GCConstants.EXTN).item(0);
          eleChildExtn.setAttribute("ExtnOverridePriceEmpId", sExtnOverridePriceEmpId);
          if(YFCCommon.isVoid(eleNotes)){
            eleNotes = inDoc.createElement(GCConstants.NOTES);
            elePrimaryOrderLine.appendChild(eleNotes);
          }
          Element eleTemp =  (Element) elePriceOverrideNote.cloneNode(true);
          eleNotes.appendChild(eleTemp);
          elePrimaryItemLine.setAttribute("OverridenPrice", String.valueOf(dOverridenPrice));
          elePrimaryItemLine.setAttribute("IsPriceOverriden", "True");
        }
        // Fix for 3426 : Stop

        Element elePrimaryLineCharges =
            (Element) elePrimaryItemLine.getElementsByTagName(GCConstants.LINE_CHARGES).item(0);
        Element elePrimaryLineTaxes = (Element) elePrimaryItemLine.getElementsByTagName(GCConstants.LINE_TAXES).item(0);

        // Copy all bundle line charges to primary component
        
        Double dTotalDiscountAmount = 0.0;
        for (int i = 0; i < nlLineCharge.getLength(); i++) {
          Element eleBundleLineCharge = (Element) nlLineCharge.item(i);
          if (YFCCommon.equals(GCConstants.PROMOTION, eleBundleLineCharge.getAttribute(GCConstants.CHARGE_CATEGORY))
              || YFCCommon.equals(GCConstants.SHIPPING_PROMOTION,
                  eleBundleLineCharge.getAttribute(GCConstants.CHARGE_CATEGORY))) {
            dFinalPrimaryLineTotal -= Double.parseDouble(eleBundleLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT));
            dTotalDiscountAmount += Double.parseDouble(eleBundleLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT));	//MPOS-2333: fix.
          } 
          //else {
            //dFinalPrimaryLineTotal += Double.parseDouble(eleBundleLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT));
          //}
          // Check if the primary component line total is negative after adding looped in charge
          if (dFinalPrimaryLineTotal < 0) {
            //GCSTORE-4687 - STARTS
            isPrimaryNegative = true;
            dPrimaryFinalTotalAfterDiscount = dFinalPrimaryLineTotal;
            //GCSTORE-4687 - ENDS
            dFinalPrimaryLineTotal += Double.parseDouble(eleBundleLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT));
            listChargeNameForSecondary.add(eleBundleLineCharge.getAttribute(GCConstants.CHARGE_NAME));
            Element eleSecondaryLineCharges =
                (Element) eleSecondaryComponent.getElementsByTagName(GCConstants.LINE_CHARGES).item(0);
            if (!YFCCommon.isVoid(eleSecondaryLineCharges)) {
              Element eleChildLineCharge = inDoc.createElement(GCConstants.LINE_CHARGE);
              eleSecondaryLineCharges.appendChild(eleChildLineCharge);
              GCXMLUtil.copyElement(inDoc, eleBundleLineCharge, eleChildLineCharge);
            }
          } else {
            Element eleChildLineCharge = inDoc.createElement(GCConstants.LINE_CHARGE);
            elePrimaryLineCharges.appendChild(eleChildLineCharge);
            GCXMLUtil.copyElement(inDoc, eleBundleLineCharge, eleChildLineCharge);
          }
        }

        // Loop Over line taxes
        for (int i = 0; i < nlLineTax.getLength(); i++) {
          Element eleBundleLineTax = (Element) nlLineTax.item(i);
          String sTaxCategory = eleBundleLineTax.getAttribute(GCConstants.CHARGE_NAME);
          // If charge is copied to secondary copy corresponding taxes to secondary component
          if (listChargeNameForSecondary.contains(sTaxCategory)) {
            Element eleSecondaryLineTaxes =
                (Element) eleSecondaryComponent.getElementsByTagName(GCConstants.LINE_TAXES).item(0);
            if (!YFCCommon.isVoid(eleSecondaryLineTaxes)) {
              Element eleChildLineTax =
                  (Element) XPathAPI.selectSingleNode(eleSecondaryLineTaxes, "LineTax[@ChargeName='" + sTaxCategory
                      + "']");
              if (YFCCommon.isVoid(eleChildLineTax)) {
                eleChildLineTax = inDoc.createElement(GCConstants.LINE_TAX);
                eleSecondaryLineTaxes.appendChild(eleChildLineTax);
              }
              String sTax = eleBundleLineTax.getAttribute(GCConstants.TAX);
              dTaxProrated = dTaxProrated.add(BigDecimal.valueOf(Double.valueOf(sTax)));
              GCXMLUtil.copyElement(inDoc, eleBundleLineTax, eleChildLineTax);
            }
          } else {
            Element eleChildLineTax =
                (Element) XPathAPI.selectSingleNode(elePrimaryLineTaxes, "LineTax[@ChargeName='" + sTaxCategory + "']");
            if (YFCCommon.isVoid(eleChildLineTax)) {
              eleChildLineTax = inDoc.createElement(GCConstants.LINE_TAX);
              elePrimaryLineTaxes.appendChild(eleChildLineTax);
              GCXMLUtil.copyElement(inDoc, eleBundleLineTax, eleChildLineTax);
            }

            // Calculate remaining Tax value for line price tax
            if (YFCCommon.equals(GCConstants.LINE_PRICE_TAX, sTaxCategory)) {
              String sTaxAmount = eleBundleLineTax.getAttribute(GCConstants.TAX);
              Double dComponentTaxAmount = Double.parseDouble(sTaxAmount) - dComponentLineTax;
              BigDecimal bdComponentTaxAmount = GCCommonUtil.formatNumberForDecimalPrecision(dComponentTaxAmount, 2);
              BigDecimal bdPriceTaxAmount = GCCommonUtil.formatNumberForDecimalPrecision(Double.parseDouble(sTaxAmount), 2);
              dTaxProrated = dTaxProrated.add(bdComponentTaxAmount);
              if (!iteratorListBundleLineDetail.hasNext() && dTaxProrated != bdPriceTaxAmount) {
                BigDecimal bdVariableTax = bdPriceTaxAmount.subtract(dTaxProrated);
                bdComponentTaxAmount = bdComponentTaxAmount.subtract(bdVariableTax);
              }
              eleChildLineTax.setAttribute(GCConstants.TAX, bdComponentTaxAmount.toString());
              elePrimaryItemLine.setAttribute(GCConstants.TAX, bdComponentTaxAmount.toString());
            }
          }
        }
        /* Append Primary Component price, charges and taxes - End */
        // Remove bundle parent line
        String sBundleParentLineDetail = GCXMLUtil.getElementString(eleBundleParentLineDetail);
        YFCDocument yfcBundleParentLineDoc = YFCDocument.getDocumentFor(sBundleParentLineDetail);
        YFCElement yfcEleBundleParentLineDetail = yfcBundleParentLineDoc.getDocumentElement();
        bundleParentLinesList.add(yfcEleBundleParentLineDetail);
        mapBundleParentComponent.put(sOrderLineKey, primaryOdrLineKey);
        eleLineDetails.removeChild(eleBundleParentLineDetail);

        // inside loop for bundle parent
        //GCSTORE-4687 - STARTS
        if (isPrimaryNegative && !isSecondaryPriceMore) {
          ProrateChargesAndTax(inDoc, dPrimaryFinalTotalAfterDiscount, eleBundleParentLineDetail,
              listChildLineDetail, primaryOdrLineKey, dTotalDiscountAmount);
        }
        //GCSTORE-4687 ENDS
      }
    }

    // Stamping Overriden Price on Regular Lines - Fix for 3426 : Start
    NodeList listRegularLineDetail = inDoc.getElementsByTagName(GCConstants.LINE_DETAIL);

    for (int i= 0 ; i < listRegularLineDetail.getLength() ; i++) {
      Element eleLineDetail = (Element) listRegularLineDetail.item(i);
      Element eleChildLineDetail = (Element) eleLineDetail.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
      String sBundleParentOrderLineKey = eleChildLineDetail.getAttribute(GCConstants.BUNDLE_PARENT_ORDERLINEKEY);
      String sKitCode = eleChildLineDetail.getAttribute(GCConstants.KIT_CODE);
      if(YFCCommon.isVoid(sBundleParentOrderLineKey) && YFCCommon.isVoid(sKitCode)){
        Element eleExtn = (Element) eleChildLineDetail.getElementsByTagName(GCConstants.EXTN).item(0);
        String sOverriddenPrice = eleExtn.getAttribute("ExtnOverridedPrice");
        if(!YFCCommon.isVoid(sOverriddenPrice)){
          Double dOverridenPrice = Double.parseDouble(sOverriddenPrice);
          if(dOverridenPrice>0.00){
            eleLineDetail.setAttribute("OverridenPrice", String.valueOf(dOverridenPrice));
            eleLineDetail.setAttribute("IsPriceOverriden", "True");
          }
        }

      }
    }

    //Fix for 3426 : End

    //
    LOGGER.endTimer("GCPOSInvoicing.prorateBundleCharges()");
    LOGGER.verbose("GCPOSInvoicing - prorateBundleCharges() : End");
    return inDoc;
  }

  /**
   * This method is used to prorate line taxes and charges in case discount is greater than primary line total
   *
   * @param inDoc
   * @param dPrimaryFinalTotalAfterDiscount
   * @param eleBundleParentLineDetail
   * @param listChildLineDetail
   * @param primaryOdrLineKey
   */

  public void ProrateChargesAndTax(Document inDoc, Double dPrimaryFinalTotalAfterDiscount,
      Element eleBundleParentLineDetail, List<Element> listChildLineDetail, String primaryOdrLineKey, Double dTotalDiscountAmount)
          throws TransformerException{

	  LOGGER.verbose("ProrateChargesAndTax(): Start" + GCXMLUtil.getXMLString(inDoc));
		BigDecimal dTaxProrated = new BigDecimal(0);
		NodeList nlLineCharge = eleBundleParentLineDetail.getElementsByTagName(GCConstants.LINE_CHARGE);
		Element elePrimaryLineDetail = GCXMLUtil.getElementByXPath(inDoc,
				"InvoiceDetail/InvoiceHeader/LineDetails/LineDetail/OrderLine[@OrderLineKey='" + primaryOdrLineKey
						+ "']/..");
		
		Double dPrimaryUnitPrice = Double.parseDouble(elePrimaryLineDetail.getAttribute(GCConstants.UNIT_PRICE));
		Double dPrimaryUnitPriceAfterProration = dPrimaryUnitPrice;
		
		
		Double dPrimaryUnitPriceAfterDiscount = dPrimaryUnitPriceAfterProration;	//MPOS-2333: fix.
		// tax should be zero at primary line
		NodeList nlPrimaryLineTaxes = elePrimaryLineDetail.getElementsByTagName(GCConstants.LINE_TAX);
		for (int j = 0; j < nlPrimaryLineTaxes.getLength(); j++) {
			Element elePrimaryLineTax = (Element) nlPrimaryLineTaxes.item(j);
				if (YFCCommon.equals(GCConstants.LINE_PRICE,
						elePrimaryLineTax.getAttribute(GCConstants.CHARGE_CATEGORY))) {
							elePrimaryLineTax.setAttribute(GCConstants.TAX, "0.0");
							elePrimaryLineDetail.setAttribute(GCConstants.TAX, "0.0");
							
						}
					}
					
		NodeList nlLineTax = eleBundleParentLineDetail.getElementsByTagName(GCConstants.LINE_TAX);
					for (int j = 0; j < nlLineTax.getLength(); j++) {
						Element eleBundleLineTax = (Element) nlLineTax.item(j);
						if (YFCCommon.equals(GCConstants.LINE_PRICE,
								eleBundleLineTax.getAttribute(GCConstants.CHARGE_CATEGORY))) {
							dTaxProrated = dTaxProrated.add(GCCommonUtil.formatNumberForDecimalPrecision(Double.parseDouble(eleBundleLineTax.getAttribute(GCConstants.TAX)),2));
							
							
						}
					}
		
		Double dRemainingDiscountAfterProration = dTotalDiscountAmount;			
		for (int i = 0; i < nlLineCharge.getLength(); i++) {
			Element eleBundleLineCharge = (Element) nlLineCharge.item(i);
			if (YFCCommon.equals(GCConstants.PROMOTION, eleBundleLineCharge.getAttribute(GCConstants.CHARGE_CATEGORY))) {
				Element eleOrderBundleParentLine = (Element) eleBundleParentLineDetail.getElementsByTagName(
						GCConstants.ORDER_LINE).item(0);
				Element eleBundleItem = (Element) eleOrderBundleParentLine.getElementsByTagName(
						GCConstants.ITEM_DETAILS).item(0);
				Element eleBundleItemExtn = (Element) eleBundleItem.getElementsByTagName(GCConstants.EXTN).item(0);
				String sPrimaryComponent = eleBundleItemExtn.getAttribute(GCXmlLiterals.EXTN_PRIMARY_COMPONENT_ITEM);
				String sChargeName = eleBundleLineCharge.getAttribute(GCConstants.CHARGE_NAME);
				Double dChargeAmount = Double.parseDouble(eleBundleLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT));
				Double dTotalDiscount = Double.parseDouble(eleBundleLineCharge
						.getAttribute(GCConstants.CHARGE_AMOUNT));
				
				Element nlPrimaryLineCharges = (Element) elePrimaryLineDetail.getElementsByTagName(
						GCConstants.LINE_CHARGES).item(0);
				
				Element elePrimaryLineCharge = (Element)XPathAPI.selectSingleNode(nlPrimaryLineCharges, "LineCharge[@ChargeName='"+sChargeName+"']"
						+ "[@ChargeCategory='"+GCConstants.PROMOTION+"']");
				if(YFCCommon.isVoid(elePrimaryLineCharge)){
					elePrimaryLineCharge = inDoc.createElement(GCConstants.LINE_CHARGE);
					nlPrimaryLineCharges.appendChild(elePrimaryLineCharge);

					GCXMLUtil.copyElement(inDoc, eleBundleLineCharge, elePrimaryLineCharge);
				}
					if(dPrimaryUnitPriceAfterProration > 0){
						if(dChargeAmount > dPrimaryUnitPriceAfterProration){
							elePrimaryLineCharge.setAttribute(GCConstants.CHARGE_AMOUNT,
									dPrimaryUnitPriceAfterProration.toString());
							dRemainingDiscountAfterProration = dChargeAmount - dPrimaryUnitPriceAfterProration;
							dPrimaryUnitPriceAfterProration = 0.0;
						}
						else{
							elePrimaryLineCharge.setAttribute(GCConstants.CHARGE_AMOUNT,
									dChargeAmount.toString());
							dPrimaryUnitPriceAfterProration = dPrimaryUnitPrice - dChargeAmount; 
							dRemainingDiscountAfterProration -= dChargeAmount;
						}
					}
					else{
						elePrimaryLineCharge.setAttribute(GCConstants.CHARGE_AMOUNT,
								"0.0");
						dPrimaryUnitPrice = 0.0;
					}
																						
				Double Secondarylistprice = 0.0;
				Double dSecodaryDiscountAfterProration = 0.0;
				
				// looping over secondary lines
				for (Element eleChildLineDetail : listChildLineDetail) {
					Element eleChldOrderLine = (Element) eleChildLineDetail
							.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
					Element eleChldItemDetails = (Element) eleChldOrderLine.getElementsByTagName(
							GCConstants.ITEM_DETAILS).item(0);
					String sItemId = eleChldItemDetails.getAttribute(GCConstants.ITEM_ID);
					Double dItemQty = Double.parseDouble(eleChildLineDetail.getAttribute(GCConstants.SHIPPED_QUANTITY));
					if (!YFCUtils.equals(sPrimaryComponent, sItemId)) {
						Element eleChldLinePriceInfo = (Element) eleChldOrderLine.getElementsByTagName(
								GCConstants.LINE_PRICE_INFO).item(0);
						
						// multiply with quantity
						Secondarylistprice = Double.parseDouble(eleChldLinePriceInfo
								.getAttribute(GCConstants.LIST_PRICE)) * dItemQty;

						dSecodaryDiscountAfterProration = Secondarylistprice - dRemainingDiscountAfterProration;

						if (dSecodaryDiscountAfterProration >= 0) {
							Element eleSecondaryLineChargesList = (Element)eleChildLineDetail
									.getElementsByTagName(GCConstants.LINE_CHARGES).item(0); 
													
							Element eleSecondaryLineCharge = (Element)XPathAPI.selectSingleNode(eleSecondaryLineChargesList, "LineCharge[@ChargeName='"+sChargeName+"']"
										+ "[@ChargeCategory='"+GCConstants.PROMOTION+"']");
								
								if(!YFCCommon.isVoid(eleSecondaryLineCharge)){
									eleSecondaryLineCharge.setAttribute(GCConstants.CHARGE_AMOUNT,
											dRemainingDiscountAfterProration.toString());
								}
								else{
									Element eleSecondaryLineCharges = inDoc.createElement(GCConstants.LINE_CHARGE);
									GCXMLUtil.copyElement(inDoc, eleBundleLineCharge, eleSecondaryLineCharges);
									eleSecondaryLineCharges.setAttribute(GCConstants.CHARGE_AMOUNT,
											dRemainingDiscountAfterProration.toString());
									eleSecondaryLineChargesList.appendChild(eleSecondaryLineCharges);
								}
									
							NodeList nlChildLineTaxes = eleChildLineDetail.getElementsByTagName(GCConstants.LINE_TAX);
							Element eleChildLineTaxes = (Element)eleChildLineDetail.getElementsByTagName(GCConstants.LINE_TAXES).item(0);
							if(nlChildLineTaxes.getLength()>0){
								for (int j = 0; j < nlChildLineTaxes.getLength(); j++) {
									Element eleChildLineTax = (Element) nlChildLineTaxes.item(j);
									if (!YFCCommon.isVoid(eleChildLineTax)
											&& eleChildLineTax.getAttribute(GCConstants.CHARGE_NAME).equalsIgnoreCase(
													GCConstants.LINE_PRICE_TAX)) {
										
										//dTaxProrated = dTaxProrated.add(GCCommonUtil.formatNumberForDecimalPrecision(Double.parseDouble(eleChildLineTax.getAttribute(GCConstants.TAX)),2));
										eleChildLineTax.setAttribute(GCConstants.TAX, dTaxProrated.toString());
										eleChildLineDetail.setAttribute(GCConstants.TAX, dTaxProrated.toString());
									} 
								}
							}
							else{
								
									Element eleChildLineTax = inDoc.createElement(GCConstants.LINE_TAX);
									GCXMLUtil.copyAttributes(eleBundleLineCharge, eleChildLineTax);
									eleChildLineTax.setAttribute(GCConstants.TAX, dTaxProrated.toString());
									eleChildLineTaxes.appendChild(eleChildLineTax);
									eleChildLineDetail.setAttribute(GCConstants.TAX, dTaxProrated.toString());
													
							}
						break;

						} else {
							dRemainingDiscountAfterProration = dRemainingDiscountAfterProration - Secondarylistprice;
							// updating line charge and tax in case of negative
							// value
							NodeList nlSecondaryLineCharges = eleChildLineDetail
									.getElementsByTagName(GCConstants.LINE_CHARGE);
							for (int j = 0; j < nlSecondaryLineCharges.getLength(); j++) {
								Element eleSecondaryLineCharges = (Element) nlSecondaryLineCharges.item(j);
								if (!YFCCommon.isVoid(eleSecondaryLineCharges)
										&& YFCCommon.equals(GCConstants.PROMOTION,
												eleSecondaryLineCharges.getAttribute(GCConstants.CHARGE_CATEGORY))
										|| YFCCommon.equals(GCConstants.SHIPPING_PROMOTION,
												eleSecondaryLineCharges.getAttribute(GCConstants.CHARGE_CATEGORY))) {
									eleSecondaryLineCharges.setAttribute(GCConstants.CHARGE_AMOUNT,
											Secondarylistprice.toString());
								}
							}

							NodeList nlChildLineTaxes = eleChildLineDetail.getElementsByTagName(GCConstants.LINE_TAX);
							for (int j = 0; j < nlChildLineTaxes.getLength(); j++) {
								Element eleChildLineTax = (Element) nlChildLineTaxes.item(j);
								if (!YFCCommon.isVoid(eleChildLineTax)
										&& eleChildLineTax.getAttribute(GCConstants.CHARGE_NAME).equalsIgnoreCase(
												GCConstants.LINE_PRICE_TAX)) {
									//dTaxProrated = dTaxProrated.add(GCCommonUtil.formatNumberForDecimalPrecision(Double.parseDouble(eleChildLineTax.getAttribute(GCConstants.TAX)),2));
									eleChildLineTax.setAttribute(GCConstants.TAX, "0.0");
								}
							}

						}
					}

				}

			}
		}

		 LOGGER.verbose("ProrateChargesAndTax(): END" + GCXMLUtil.getXMLString(inDoc));

	
}

  /**
   * This method is used to stamp OrderLineKey of primary Component in case of SET as
   * dependentOnLineKey value of associated warranty.
   *
   * @param sOrderLineKey
   * @param primaryOdrLineKey
   * @param eleLineDetails
   */
  public void manageSetWarranty(String sOrderLineKey, String primaryOdrLineKey, Element eleLineDetails) {
    LOGGER.beginTimer("GCGetPOSReservationID.manageSetWarranty()");
    LOGGER.verbose("GCGetPOSReservationID - manageSetWarranty() : Start");
    NodeList nlLineDetail = eleLineDetails.getElementsByTagName(GCConstants.LINE_DETAIL);
    int length = nlLineDetail.getLength();
    for (int i = 0; i < length; i++) {
      Element eleLineDetail = (Element) nlLineDetail.item(i);
      Element eleOdrLine = (Element) eleLineDetail.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
      String dependentOnLineKey = eleOdrLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
      if (YFCUtils.equals(sOrderLineKey, dependentOnLineKey)) {
        eleOdrLine.setAttribute(GCConstants.DEPENDENT_ON_LINE_KEY, primaryOdrLineKey);
      }
    }
    LOGGER.verbose("GCGetPOSReservationID - manageSetWarranty() : End");
    LOGGER.endTimer("GCGetPOSReservationID.manageSetWarranty()");
  }

  /**
   * This method will club items with same components in the shipment
   *
   * @param inDoc
   * @throws TransformerException
   */
  public Document clubOrderLinesWithSimilarComps(Document inDoc , Map<String, String> mapBundleParentComponent, Document removedLineDetailsDoc, Element removedEleLineDetails) throws TransformerException {
    LOGGER.beginTimer("GCPOSInvoicing.clubOrderLinesWithSimilarComps()");
    LOGGER.verbose("GCPOSInvoicing - clubOrderLinesWithSimilarComps() : Start");

    List<Element> listLineDetail =
        GCXMLUtil.getElementListByXpath(inDoc, "InvoiceDetail/InvoiceHeader/LineDetails/LineDetail");
    Element eleLineDetails = (Element) inDoc.getElementsByTagName(GCConstants.LINE_DETAILS).item(0);
    Iterator<Element> iteratorListLineDetail = listLineDetail.iterator();
    List<String> listItemId = new ArrayList();

    Map<String, Double> mapPrimaryLineChangedTaxes = new HashMap<String, Double>();
    while (iteratorListLineDetail.hasNext()) {
      Element eleLineDetail = iteratorListLineDetail.next();

      String sItemId = eleLineDetail.getAttribute(GCConstants.ITEM_ID);
      Element eleOrderLine = (Element) eleLineDetail.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
      Element eleOrderLineExtn = (Element) eleOrderLine.getElementsByTagName(GCConstants.EXTN).item(0);
      String sSKUNumber = eleOrderLineExtn.getAttribute(GCConstants.EXTN_POS_SKU_NO);
      String sOrderLineKey = eleLineDetail.getAttribute(GCConstants.ORDER_LINE_KEY);
      Double dItemQty = Double.parseDouble(eleLineDetail.getAttribute(GCConstants.SHIPPED_QUANTITY));
      Double dUnitPrice = Double.parseDouble(eleLineDetail.getAttribute(GCXmlLiterals.EXTENDED_PRICE)) / dItemQty;
      NodeList nlLineTaxes = eleLineDetail.getElementsByTagName(GCConstants.LINE_TAX);
      Double dLinePriceTax = 0.00;
      Double dTotalSharedCompLineTax = 0.00;
      for (int j = 0; j < nlLineTaxes.getLength(); j++) {
        Element eleLineTax = (Element) nlLineTaxes.item(j);
        System.out.println(GCXMLUtil.getElementXMLString(eleLineTax));
        String sChargeName = eleLineTax.getAttribute("ChargeName");
        if (YFCCommon.equals(sChargeName, GCConstants.LINE_PRICE_TAX)) {
          dLinePriceTax = Double.parseDouble(eleLineTax.getAttribute(GCConstants.TAX));
          dTotalSharedCompLineTax = dLinePriceTax;
        }
      }

      Map<String, Element> mapSameItemCharges = new HashMap<String, Element>();
      Map<String, Element> mapSameItemTax = new HashMap<String, Element>();

      if (listItemId.contains(sSKUNumber)) {
        // GCSTORE 5337 START
        Element elenewLineDetailItem = removedLineDetailsDoc.createElement(GCConstants.LINE_DETAIL);
        GCXMLUtil.copyElement(removedLineDetailsDoc, eleLineDetail, elenewLineDetailItem);
        removedEleLineDetails.appendChild(elenewLineDetailItem);
        // GCSTORE 5337 END

        // Remove Order line from document
        eleLineDetails.removeChild(eleLineDetail);

      } else {
        List<Element> listSameItemLineDetail =
            GCXMLUtil.getElementListByXpath(inDoc,
                "InvoiceDetail/InvoiceHeader/LineDetails/LineDetail[@OrderLineKey!='" + sOrderLineKey+ "']/OrderLine/Extn[@ExtnPOSSKUNumber='" + sSKUNumber+ "' and @ExtnIsWarrantyItem='N']/../..");
        if (!listSameItemLineDetail.isEmpty()) {
          listItemId.add(sSKUNumber);
          Iterator<Element> iteratorSameItem = listSameItemLineDetail.iterator();
          while (iteratorSameItem.hasNext()) {
            Element eleSameItemLineDetail = iteratorSameItem.next();
            String sOrderedQty = eleSameItemLineDetail.getAttribute(GCConstants.SHIPPED_QUANTITY);
            dItemQty += Double.parseDouble(sOrderedQty);

            NodeList nlSameItemLineCharge = eleSameItemLineDetail.getElementsByTagName(GCConstants.LINE_CHARGE);
            NodeList nlSameItemLineTax = eleSameItemLineDetail.getElementsByTagName(GCConstants.LINE_TAX);

            // Loop Over line charges
            for (int i = 0; i < nlSameItemLineCharge.getLength(); i++) {
              Element eleSameLineCharge = (Element) nlSameItemLineCharge.item(i);
              String sChargeName = eleSameLineCharge.getAttribute(GCConstants.CHARGE_NAME);
              mapSameItemCharges.put(sChargeName, eleSameLineCharge);
            }

            // Loop Over line taxes
            for (int j = 0; j < nlSameItemLineTax.getLength(); j++) {
              Element eleSameLineTax = (Element) nlSameItemLineTax.item(j);
              String sChargeName = eleSameLineTax.getAttribute(GCConstants.CHARGE_NAME);
              String sTaxAmount = eleSameLineTax.getAttribute(GCConstants.TAX);
              Double dTaxAmount = Double.parseDouble(sTaxAmount);
              if (YFCCommon.equals(sChargeName, GCConstants.LINE_PRICE_TAX)) {
                //GCSTORE- 4687 STARTS
                Double dMinTaxAmount = Math.min(dTaxAmount, dLinePriceTax);
                //dTotalSharedCompLineTax = 2* dMinTaxAmount;
                dTotalSharedCompLineTax = dTotalSharedCompLineTax + dMinTaxAmount;
                // dTotalSharedCompLineTax = dTotalSharedCompLineTax + dLinePriceTax;
                if (dTaxAmount != dLinePriceTax) {
                  Double dChangeInTax=0.0;
                  if(dTaxAmount > dLinePriceTax){
                    // changeInTax needs to be negative
                    dChangeInTax =  dLinePriceTax -dTaxAmount;
                  }
                  else{
                    dChangeInTax = dTaxAmount - dLinePriceTax ;
                  }

                  //GCSTORE- 4687 ENDS

                  //  Double dChangeInTax = dLinePriceTax - dTaxAmount;
                  Element eleSameItemOrderLine =
                      (Element) eleSameItemLineDetail.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
                  String sBundleParentLineKey =
                      eleSameItemOrderLine.getAttribute(GCConstants.BUNDLE_PARENT_ORDERLINEKEY);
                  String sPrimaryOrderLineKey = mapBundleParentComponent.get(sBundleParentLineKey);
                  //    mapPrimaryLineChangedTaxes.put(sPrimaryOrderLineKey, dChangeInTax);
                  //GCSTORE- 4687 STARTS
                  Element elePrimaryLineDetail = GCXMLUtil.getElementByXPath(inDoc,
                      "InvoiceDetail/InvoiceHeader/LineDetails/LineDetail/OrderLine[@OrderLineKey='" + sPrimaryOrderLineKey
                      + "']/..");
                  Element elePrimaryLineTaxes = (Element)elePrimaryLineDetail.getElementsByTagName(GCConstants.LINE_TAXES).item(0);

                  Element elePrimaryZeroTax =
                      (Element) XPathAPI.selectSingleNode(elePrimaryLineTaxes, "LineTax[@ChargeCategory='LinePrice'][@Tax = '0.0']");
                  if(!YFCCommon.isVoid(elePrimaryZeroTax)){
                    //primary component is zero
                    dTotalSharedCompLineTax = dTotalSharedCompLineTax  - dChangeInTax;
                  }
                  else{
                    mapPrimaryLineChangedTaxes.put(sPrimaryOrderLineKey, dChangeInTax);
                  }
                  //GCSTORE- 4687 ENDS
                  mapSameItemTax.put(sChargeName, eleSameLineTax);
                }
              } else {
                mapSameItemTax.put(sChargeName, eleSameLineTax);
              }
            }
          }

          // Update Extended and Shipped qty for line
          Double dFinalExtendedPrice = dUnitPrice * dItemQty;
          eleLineDetail.setAttribute(GCConstants.SHIPPED_QUANTITY, dItemQty.toString());
          eleLineDetail.setAttribute(GCXmlLiterals.EXTENDED_PRICE, dFinalExtendedPrice.toString());
          NodeList nlLineCharge = eleLineDetail.getElementsByTagName(GCConstants.LINE_CHARGE);
          NodeList nlLineTax = eleLineDetail.getElementsByTagName(GCConstants.LINE_TAX);

          // Loop Over line charges
          for (int i = 0; i < nlLineCharge.getLength(); i++) {
            Element eleSameLineCharge = (Element) nlLineCharge.item(i);
            String sChargeName = eleSameLineCharge.getAttribute(GCConstants.CHARGE_NAME);
            String sChargeAmount = eleSameLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT);
            if (mapSameItemCharges.containsKey(sChargeName)) {
              String sEarlierChargeAmount = mapSameItemCharges.get(sChargeName).getAttribute(GCConstants.CHARGE_AMOUNT);
              Double dSumChargeAmount = Double.parseDouble(sChargeAmount) + Double.parseDouble(sEarlierChargeAmount);
              eleSameLineCharge.setAttribute(GCConstants.CHARGE_AMOUNT, dSumChargeAmount.toString());
            }
          }

          // Loop Over line taxes
          for (int j = 0; j < nlLineTax.getLength(); j++) {
            Element eleSameLineTax = (Element) nlLineTax.item(j);
            String sChargeName = eleSameLineTax.getAttribute(GCConstants.CHARGE_NAME);
            String sTaxAmount = eleSameLineTax.getAttribute(GCConstants.TAX);
            if (mapSameItemTax.containsKey(sChargeName)) {
              if (YFCCommon.equals(sChargeName, GCConstants.LINE_PRICE_TAX)) {
                Element eleSameItemTax = mapSameItemTax.get(sChargeName);
                eleSameLineTax.setAttribute(GCConstants.TAX, dTotalSharedCompLineTax.toString());
              } else {
                Element eleSameItemTax = mapSameItemTax.get(sChargeName);
                String sEarlierTaxAmount = eleSameItemTax.getAttribute(GCConstants.TAX);
                Double dSumTaxAmount = Double.parseDouble(sTaxAmount) + Double.parseDouble(sEarlierTaxAmount);
                eleSameLineTax.setAttribute(GCConstants.TAX, dSumTaxAmount.toString());
              }
            }
          }
        }
      }
    }
    // End of for loop on line detail

    // Stamping taxes on primary component.

    if(!mapPrimaryLineChangedTaxes.isEmpty()){
      NodeList nlLineDetails = inDoc.getElementsByTagName(GCConstants.LINE_DETAIL);
      for (int j = 0; j < nlLineDetails.getLength(); j++) {
        Element eleLineDetail = (Element) nlLineDetails.item(j);
        String sOrderLineKey = eleLineDetail.getAttribute(GCConstants.ORDER_LINE_KEY);
        if (mapPrimaryLineChangedTaxes.containsKey(sOrderLineKey)) {
          Double dChangedTax = mapPrimaryLineChangedTaxes.get(sOrderLineKey);
          NodeList nlLineTax = eleLineDetail.getElementsByTagName("LineTax");
          for (int i = 0; i < nlLineTax.getLength(); i++) {
            Element eleLineTax = (Element) nlLineTax.item(i);
            String sChargeName = eleLineTax.getAttribute(GCConstants.CHARGE_NAME);
            if (YFCCommon.equals(sChargeName, GCConstants.LINE_PRICE_TAX)) {
              String sTax = eleLineTax.getAttribute(GCConstants.TAX);
              Double dFinalTax = Double.parseDouble(sTax) - dChangedTax;
              eleLineTax.setAttribute(GCConstants.TAX, dFinalTax.toString());
            }
          }
        }
      }
    }


    LOGGER.endTimer("GCPOSInvoicing.clubOrderLinesWithSimilarComps()");
    LOGGER.verbose("GCPOSInvoicing - clubOrderLinesWithSimilarComps() : End");
    return inDoc;
  }

  /**
   * This method will invoke change order API to update POS sales ticket number and pro-rated
   * component price on order line
   *
   * @param env
   * @param yfcEleInvoiceHeader
   * @param sPOSSalesTicketNo
   */
  private void invokeChangeOrder(YFSEnvironment env, YFCElement yfcEleInvoiceHeader, String sPOSSalesTicketNo, Document removedLineDetailsDoc) {
    LOGGER.beginTimer("GCPOSInvoicing.invokeChangeOrder()");
    LOGGER.verbose("GCPOSInvoicing - invokeChangeOrder() : Start");
    YFCElement yfcEleLineDetails = yfcEleInvoiceHeader.getElementsByTagName(GCConstants.LINE_DETAILS).item(0);

    // Code changes for GCSTORE-6872 which will handle GCSTORE 5337  as well
    YFCDocument yfcRemovedLineDetailsDoc = YFCDocument.getDocumentFor( GCXMLUtil.getElementString(removedEleLineDetails));
    YFCNodeList<YFCElement> yfcRemovedLineDetails = yfcRemovedLineDetailsDoc.getElementsByTagName(GCConstants.LINE_DETAIL);
    if (yfcRemovedLineDetails.getLength() > 0) {
      for (int j = 0; j < yfcRemovedLineDetails.getLength(); j++) {
        YFCNode yfcremovedLineDetailNode = yfcRemovedLineDetails.item(j);
        yfcEleLineDetails.importNode(yfcremovedLineDetailNode);
      }

    }


    YFCElement yfcEleOrder = yfcEleInvoiceHeader.getElementsByTagName(GCConstants.ORDER).item(0);
    String sOrderHeaderkey = yfcEleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);

    YFCDocument yfcDocChangeOrder = YFCDocument.createDocument(GCConstants.ORDER);
    yfcDocChangeOrder.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderkey);
    yfcDocChangeOrder.getDocumentElement().setAttribute("SelectMethod", "WAIT");
    yfcDocChangeOrder.getDocumentElement().setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
    YFCElement yfcEleChangeOrderLines = yfcDocChangeOrder.createElement(GCConstants.ORDER_LINES);
    yfcDocChangeOrder.getDocumentElement().appendChild(yfcEleChangeOrderLines);

    YFCNodeList<YFCElement> yfcNLLineDetails = yfcEleLineDetails.getElementsByTagName(GCConstants.LINE_DETAIL);
    for (int i = 0; i < yfcNLLineDetails.getLength(); i++) {
      YFCElement yfcEleLineDetail = yfcNLLineDetails.item(i);
      YFCElement yfcEleChangeOrderLine = yfcDocChangeOrder.createElement(GCConstants.ORDER_LINE);
      yfcEleChangeOrderLines.appendChild(yfcEleChangeOrderLine);

      String sOrderLineKey = yfcEleLineDetail.getAttribute(GCConstants.ORDER_LINE_KEY);
      String sItemID = yfcEleLineDetail.getAttribute(GCConstants.ITEM_ID);
      yfcEleChangeOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, sOrderLineKey);

      YFCElement yfcEleChangeOrderLineExtn = yfcDocChangeOrder.createElement(GCConstants.EXTN);
      yfcEleChangeOrderLine.appendChild(yfcEleChangeOrderLineExtn);
      yfcEleChangeOrderLineExtn.setAttribute(GCXmlLiterals.EXTN_POS_SALES_TICKET_NO, sPOSSalesTicketNo);

      /*  Commenting  GCSTORE 5337   as  changes for  GCSTORE-6872  will handle this particular scenario as well
      // GCSTORE 5337 BEGIN
      Element removedEleLineDetails = removedLineDetailsDoc.getDocumentElement();
      NodeList removedLineDetails = removedEleLineDetails.getElementsByTagName(GCConstants.LINE_DETAIL);
      if (removedLineDetails.getLength() > 0) {
        LOGGER.debug("removedLineDetailsDoc is " + GCXMLUtil.getXMLString(removedLineDetailsDoc));
        for (int j = 0; j < removedLineDetails.getLength(); j++) {
          LOGGER.debug("Entering 5337 fix");
          Element removedLineDetailEle = (Element) removedLineDetails.item(j);
          String sOrderLineKeyOfRemovedLine = removedLineDetailEle.getAttribute(GCConstants.ORDER_LINE_KEY);

          YFCElement yfcEleChangeOrderLineForRemovedLine = yfcDocChangeOrder.createElement(GCConstants.ORDER_LINE);
          yfcEleChangeOrderLineForRemovedLine.setAttribute(GCConstants.ORDER_LINE_KEY, sOrderLineKeyOfRemovedLine);

          YFCElement yfcEleChangeOrderLineExtnForRemLine = yfcDocChangeOrder.createElement(GCConstants.EXTN);
          yfcEleChangeOrderLineExtnForRemLine.setAttribute(GCXmlLiterals.EXTN_POS_SALES_TICKET_NO, sPOSSalesTicketNo);

          yfcEleChangeOrderLineForRemovedLine.appendChild(yfcEleChangeOrderLineExtnForRemLine);
          yfcEleChangeOrderLines.appendChild(yfcEleChangeOrderLineForRemovedLine);

        }
      }
      // GCSTORE 5337 END
       */

      // GCSTORE-516 - adding flag
      yfcEleChangeOrderLineExtn.setAttribute(GCXmlLiterals.EXTN_IS_STORE_FULFILLED, GCConstants.YES);
      // GCSTORE-516 - adding flag

      String sBundleParentLineKey= (yfcEleLineDetail.getElementsByTagName(GCConstants.ORDER_LINE).item(0)).getAttribute(GCConstants.BUNDLE_PARENT_ORDERLINEKEY);

      if (bAnyBundleLineExists && !YFCCommon.isVoid(sBundleParentLineKey)) {
        YFCElement yfcEleComponentPriceList =
            yfcDocChangeOrder.createElement(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS_LIST);
        yfcEleChangeOrderLineExtn.appendChild(yfcEleComponentPriceList);

        // Fix for 5021 : Making all amount to zero on GCOrderLineComponents for secondary components
        Set<String> lOrderLineCompToBePersisted = new HashSet<String>();
        // Add unit price as an individual entry

        String sUnitPrice = yfcEleLineDetail.getAttribute(GCConstants.UNIT_PRICE);
        String sQuantity = yfcEleLineDetail.getAttribute(GCConstants.SHIPPED_QUANTITY);

        // Check if GCOrderLineComp with ChargeCategory=Price exist. If yes update the same, if no create new.
        YFCNodeList<YFCElement> nlGCOrderLineComponents = yfcEleLineDetail.getElementsByTagName(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
        YFCElement yfceleGCOrderLineUnitPriceComponent = null;
        for (int j = 0; j < nlGCOrderLineComponents.getLength(); j++) {
          yfceleGCOrderLineUnitPriceComponent = nlGCOrderLineComponents.item(j);
          String sChargeCategory = yfceleGCOrderLineUnitPriceComponent.getAttribute(GCConstants.CHARGE_CATEGORY);
          String sChargeName = yfceleGCOrderLineUnitPriceComponent.getAttribute(GCConstants.CHARGE_NAME);
          if (YFCCommon.equals(sChargeCategory, GCConstants.UNIT_PRICE)
              && YFCCommon.equals(sChargeName, GCConstants.UNIT_PRICE)) {
            break;
          }
        }

        if(YFCCommon.isVoid(yfceleGCOrderLineUnitPriceComponent)){
          LOGGER.verbose("Component with UnitPrice ChargeCategory does not exist. Create new");
          YFCElement yfcEleComponentUnitPrice = yfcDocChangeOrder.createElement(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
          yfcEleComponentPriceList.appendChild(yfcEleComponentUnitPrice);
          yfcEleComponentUnitPrice.setAttribute(GCConstants.CHARGE_CATEGORY, GCConstants.UNIT_PRICE);
          yfcEleComponentUnitPrice.setAttribute(GCConstants.CHARGE_NAME, GCConstants.UNIT_PRICE);
          yfcEleComponentUnitPrice.setAttribute(GCXmlLiterals.AMOUNT, sUnitPrice);
          yfcEleComponentUnitPrice.setAttribute(GCConstants.ITEM_ID, sItemID);
          yfcEleComponentUnitPrice.setAttribute(GCConstants.QUANTITY, sQuantity);
        } else{
          LOGGER.verbose("Component with Unit Price ChargeCategory exists. Update the same, stamping updated amount");
          YFCElement yfcEleComponentUnitPrice = yfcDocChangeOrder.createElement(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
          yfcEleComponentPriceList.appendChild(yfcEleComponentUnitPrice);
          yfcEleComponentUnitPrice.setAttribute("OrderLineComponentKey",
              yfceleGCOrderLineUnitPriceComponent.getAttribute("OrderLineComponentKey"));
          yfcEleComponentUnitPrice.setAttribute(GCXmlLiterals.AMOUNT, sUnitPrice);
          lOrderLineCompToBePersisted.add(yfceleGCOrderLineUnitPriceComponent.getAttribute("OrderLineComponentKey"));
        }


        // Fetch BundlePrice and stamp on GCOrderLineComponents

        YFCElement yfcEleBundlePriceComponent = yfcDocChangeOrder.createElement(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
        yfcEleComponentPriceList.appendChild(yfcEleBundlePriceComponent);
        yfcEleBundlePriceComponent.setAttribute(GCConstants.CHARGE_CATEGORY, "BundlePrice");
        String sBundleUnitPrice = yfcEleLineDetail.getAttribute("BundleParentUnitPrice");
        yfcEleBundlePriceComponent.setAttribute(GCXmlLiterals.AMOUNT, sBundleUnitPrice);
        yfcEleBundlePriceComponent.setAttribute(GCConstants.QUANTITY, sQuantity);

        // Loop over line charges
        YFCNodeList<YFCElement> yfcNLLineCharge = yfcEleLineDetail.getElementsByTagName(GCConstants.LINE_CHARGE);
        for (int j = 0; j < yfcNLLineCharge.getLength(); j++) {
          YFCElement yfcEleLineCharge = yfcNLLineCharge.item(j);
          String sChargeCategory = yfcEleLineCharge.getAttribute(GCConstants.CHARGE_CATEGORY);
          String sChargeName = yfcEleLineCharge.getAttribute(GCConstants.CHARGE_NAME);
          String sIsDiscount = yfcEleLineCharge.getAttribute(GCXmlLiterals.IS_DISCOUNT);
          String sChargeAmount = yfcEleLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT);

          // Check if GCOrderLineComp with current looped ChargeCategory exist. If yes update the same, if no create new.
          YFCElement yfceleGCOrderLineLineChargeComponent = null;
          for (int k = 0; k < nlGCOrderLineComponents.getLength(); k++) {
            yfceleGCOrderLineLineChargeComponent = nlGCOrderLineComponents.item(k);
            String sCompChargeCategory = yfceleGCOrderLineLineChargeComponent.getAttribute(GCConstants.CHARGE_CATEGORY);
            String sCompChargeName = yfceleGCOrderLineLineChargeComponent.getAttribute(GCConstants.CHARGE_NAME);
            if (YFCCommon.equals(sCompChargeCategory, sChargeCategory)
                && YFCCommon.equals(sCompChargeName, sChargeName)) {
              break;
            }
          }

          if(YFCCommon.isVoid(yfceleGCOrderLineLineChargeComponent)){
            LOGGER.verbose("Component with " + sChargeCategory + " ChargeCategory does not exist. Create new");
            YFCElement yfcEleComponentPrice = yfcDocChangeOrder.createElement(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
            yfcEleComponentPriceList.appendChild(yfcEleComponentPrice);

            yfcEleComponentPrice.setAttribute(GCConstants.CHARGE_CATEGORY, sChargeCategory);
            yfcEleComponentPrice.setAttribute(GCConstants.CHARGE_NAME, sChargeName);
            yfcEleComponentPrice.setAttribute(GCXmlLiterals.IS_DISCOUNT, sIsDiscount);
            yfcEleComponentPrice.setAttribute(GCXmlLiterals.AMOUNT, sChargeAmount);
            yfcEleComponentPrice.setAttribute(GCConstants.ITEM_ID, sItemID);
            yfcEleComponentPrice.setAttribute(GCConstants.QUANTITY, sQuantity);
          } else{
            LOGGER.verbose("Component with " + sChargeCategory
                + "ChargeCategory exists. Update the same, stamping updated amount");
            YFCElement yfcEleComponentUnitPrice =
                yfcDocChangeOrder.createElement(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
            yfcEleComponentPriceList.appendChild(yfcEleComponentUnitPrice);
            yfcEleComponentUnitPrice.setAttribute("OrderLineComponentKey",
                yfceleGCOrderLineLineChargeComponent.getAttribute("OrderLineComponentKey"));
            yfcEleComponentUnitPrice.setAttribute(GCXmlLiterals.AMOUNT, sChargeAmount);
            lOrderLineCompToBePersisted.add(yfceleGCOrderLineLineChargeComponent.getAttribute("OrderLineComponentKey"));
          }

        }

        // Loop over line Taxes
        YFCNodeList<YFCElement> yfcNLLineTax = yfcEleLineDetail.getElementsByTagName(GCConstants.LINE_TAX);
        for (int k = 0; k < yfcNLLineTax.getLength(); k++) {
          YFCElement yfcEleLineTax = yfcNLLineTax.item(k);
          String sChargeCategory = yfcEleLineTax.getAttribute(GCConstants.CHARGE_CATEGORY);
          String sChargeName = yfcEleLineTax.getAttribute(GCConstants.CHARGE_NAME);
          if(YFCCommon.equals(sChargeName, GCConstants.SHIPPING_CHARGE)){
            sChargeName = yfcEleLineTax.getAttribute(GCConstants.TAX_NAME);
          }
          String sIsDiscount = GCConstants.BLANK;
          String sChargeAmount = yfcEleLineTax.getAttribute(GCConstants.TAX);

          if (YFCCommon.equals(GCConstants.SHIPPING_PROMOTION, sChargeCategory)
              || YFCCommon.equals(GCConstants.PROMOTION, sChargeCategory)) {
            sIsDiscount = GCConstants.YES;
          }

          YFCElement yfceleGCOrderLineLineTaxComponent = null;
          for (int l = 0; l < nlGCOrderLineComponents.getLength(); l++) {
            yfceleGCOrderLineLineTaxComponent = nlGCOrderLineComponents.item(l);
            String sCompChargeCategory = yfceleGCOrderLineLineTaxComponent.getAttribute(GCConstants.CHARGE_CATEGORY);
            String sCompChargeName = yfceleGCOrderLineLineTaxComponent.getAttribute(GCConstants.CHARGE_NAME);
            if (YFCCommon.equals(sCompChargeCategory, sChargeCategory)
                && YFCCommon.equals(sCompChargeName, sChargeName)) {
              break;
            }
          }

          if(YFCCommon.isVoid(yfceleGCOrderLineLineTaxComponent)){
            LOGGER.verbose("Component with " + sChargeCategory + " ChargeCategory does not exist. Create new");
            YFCElement yfcEleComponentPrice = yfcDocChangeOrder.createElement(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
            yfcEleComponentPriceList.appendChild(yfcEleComponentPrice);
            yfcEleComponentPrice.setAttribute(GCConstants.CHARGE_CATEGORY, sChargeCategory);
            yfcEleComponentPrice.setAttribute(GCConstants.CHARGE_NAME, sChargeName);
            yfcEleComponentPrice.setAttribute(GCXmlLiterals.IS_DISCOUNT, sIsDiscount);
            yfcEleComponentPrice.setAttribute(GCXmlLiterals.AMOUNT, sChargeAmount);
            yfcEleComponentPrice.setAttribute(GCConstants.ITEM_ID, sItemID);
            yfcEleComponentPrice.setAttribute(GCConstants.QUANTITY, sQuantity);
          } else{
            LOGGER.verbose("Component with " + sChargeCategory
                + "ChargeCategory exists. Update the same, stamping updated amount");
            YFCElement yfcEleComponentUnitPrice =
                yfcDocChangeOrder.createElement(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
            yfcEleComponentPriceList.appendChild(yfcEleComponentUnitPrice);
            yfcEleComponentUnitPrice.setAttribute("OrderLineComponentKey",
                yfceleGCOrderLineLineTaxComponent.getAttribute("OrderLineComponentKey"));
            yfcEleComponentUnitPrice.setAttribute(GCXmlLiterals.AMOUNT, sChargeAmount);

            // Set updated quantity  for GCSTORE-6872 --Begin
            yfcEleComponentUnitPrice.setAttribute("Quantity", sQuantity);
            // Set updated quantity  for GCSTORE-6872 --End

            lOrderLineCompToBePersisted.add(yfceleGCOrderLineLineTaxComponent.getAttribute("OrderLineComponentKey"));
          }
        }

        // Set Amount=0.0 on OrderLineComponents for which no charge category exists.

        YFCNodeList<YFCElement> nlAllGCOrderLineComponentsOnLine = yfcEleLineDetail.getElementsByTagName(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
        for (int j = 0; j < nlAllGCOrderLineComponentsOnLine.getLength(); j++) {
          YFCElement yfceleGCOrderLineComponent = nlAllGCOrderLineComponentsOnLine.item(j);
          String sGCOrderLineComponentsKey = yfceleGCOrderLineComponent.getAttribute("OrderLineComponentKey");
          if (!YFCCommon.isVoid(sGCOrderLineComponentsKey)
              && !lOrderLineCompToBePersisted.contains(sGCOrderLineComponentsKey)) {
            YFCElement yfceleTempGCOrderLineComponent =
                yfcDocChangeOrder.createElement(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
            yfcEleComponentPriceList.appendChild(yfceleTempGCOrderLineComponent);
            yfceleTempGCOrderLineComponent.setAttribute(GCXmlLiterals.AMOUNT, "0.00");
            yfceleTempGCOrderLineComponent.setAttribute("OrderLineComponentKey",
                yfceleGCOrderLineComponent.getAttribute("OrderLineComponentKey"));
          }
        }

      }
    }

    for (int i = 0; i < bundleParentLinesList.size(); i++) {
      YFCElement yfcEleLineDetail = bundleParentLinesList.get(i);
      YFCElement yfcEleChangeOrderLine = yfcDocChangeOrder.createElement(GCConstants.ORDER_LINE);
      yfcEleChangeOrderLines.appendChild(yfcEleChangeOrderLine);
      String sOrderLineKey = yfcEleLineDetail.getAttribute(GCConstants.ORDER_LINE_KEY);
      yfcEleChangeOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, sOrderLineKey);
      YFCElement yfcEleChangeOrderLineExtn = yfcDocChangeOrder.createElement(GCConstants.EXTN);
      yfcEleChangeOrderLine.appendChild(yfcEleChangeOrderLineExtn);
      yfcEleChangeOrderLineExtn.setAttribute(GCXmlLiterals.EXTN_POS_SALES_TICKET_NO, sPOSSalesTicketNo);
      yfcEleChangeOrderLineExtn.setAttribute(GCXmlLiterals.EXTN_IS_STORE_FULFILLED, GCConstants.YES);
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Change Order input XML :" + yfcDocChangeOrder);
    }
    GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, yfcDocChangeOrder.getDocument());
    LOGGER.endTimer("GCPOSInvoicing.invokeChangeOrder()");
    LOGGER.verbose("GCPOSInvoicing - invokeChangeOrder() : End");
  }

  /**
   * This method will process Web service response from POS
   *
   * @param yfcDocInvokeWebServiceOutput
   * @return
   */
  private void processWebServiceResponse(YFSEnvironment env, Document inDoc, YFCElement yfcEleInvoiceHeader, YFCDocument yfcDocInvokeWebServiceOutput , boolean bIsStandaloneWarranty, Document removedLineDetailsDoc) {
    LOGGER.beginTimer("GCPOSInvoicing.processWebServiceResponse()");
    LOGGER.verbose("GCPOSInvoicing - processWebServiceResponse() : Start");

    // Fix for 3042 : Start
    String sOrderInvoiceKey = yfcEleInvoiceHeader.getAttribute("OrderInvoiceKey");
    YFCDocument changeOrderInvoice = YFCDocument.createDocument(GCConstants.ORDER_INVOICE);
    YFCElement eleOrderInvoice = changeOrderInvoice.getDocumentElement();
    eleOrderInvoice.setAttribute("StampTickets", "True");
    eleOrderInvoice.setAttribute(GCConstants.ORDER_INVOICE_KEY, sOrderInvoiceKey);
    YFCElement eleExtn = eleOrderInvoice.createChild(GCConstants.EXTN);

    // Fix for 3042 : Stop

    String sConfirmDetail = GCConstants.BLANK;
    String sPOSSalesTicketNo = GCConstants.BLANK;
    boolean bHasCallFailed = false;

    NodeList nConfirmResultDetailList = null;
    if(bIsStandaloneWarranty){
      nConfirmResultDetailList = yfcDocInvokeWebServiceOutput.getDocument().getElementsByTagName("SaleRequestResult");
    } else{
      nConfirmResultDetailList =
          yfcDocInvokeWebServiceOutput.getDocument().getElementsByTagName(GCXmlLiterals.CONFIRM_RESULT);
    }
    NodeList nlFaultString = yfcDocInvokeWebServiceOutput.getDocument().getElementsByTagName("faultstring");
    if (nConfirmResultDetailList.getLength() > 0) {
      Node nConfirmResultDetail = nConfirmResultDetailList.item(0);
      if (YFCCommon.isVoid(nConfirmResultDetail)) {
        bHasCallFailed = true;
      } else {
        sConfirmDetail = nConfirmResultDetail.getTextContent();
        if (YFCCommon.isStringVoid(sConfirmDetail)) {
          bHasCallFailed = true;
        } else{
          YFCDocument yfcDocResponse = YFCDocument.getDocumentFor(sConfirmDetail);
          YFCElement yfcEleResponse = yfcDocResponse.getDocumentElement();
          YFCElement yfcEleSuccess = yfcEleResponse.getElementsByTagName(GCConstants.SUCCESS).item(0);
          if (!YFCCommon.isVoid(yfcEleSuccess)) {
            String sStatus = yfcEleSuccess.getNodeValue();
            if(YFCCommon.equals(sStatus, GCConstants.TRUE)){
              YFCElement yfcElePOSSalesTicketNo = yfcEleResponse.getElementsByTagName(GCXmlLiterals.POS_SALES_TICKET).item(0);
              if (!YFCCommon.isVoid(yfcElePOSSalesTicketNo)) {
                sPOSSalesTicketNo = yfcElePOSSalesTicketNo.getNodeValue();
                eleExtn.setAttribute(GCXmlLiterals.EXTN_POS_SALES_TICKET_NO, sPOSSalesTicketNo);
                eleExtn.setAttribute("ExtnTargetSystem", "POS");
                invokeChangeOrder(env, yfcEleInvoiceHeader, sPOSSalesTicketNo, removedLineDetailsDoc );

                // Fix for 3042 : Resume
                GCCommonUtil.invokeService(env, GCConstants.GC_PROCESS_POS_RESPONSE_SERVICE,changeOrderInvoice.getDocument());
                // Fix for 3042 : End
              }
            } else if (YFCCommon.equals(sStatus, GCConstants.FALSE_STR)) {
              YFCNodeList<YFCElement> nlErrorInfo = yfcEleResponse.getElementsByTagName("Errorinfo");
              YFCElement eleErrorInfo = nlErrorInfo.item(0);
              String sErrorInfo = "";
              if(!YFCCommon.isVoid(eleErrorInfo)){
                sErrorInfo =  eleErrorInfo.getNodeValue();
              }
              if(YFCCommon.equals(env.getTxnObject("IsHoldAppiled"), "True")){
                YFCException yfcException = new YFCException();
                yfcException.setAttribute(GCConstants.ERROR_CODE, sErrorInfo);
                yfcException.setAttribute(GCConstants.ERROR_DESCRIPTION, sErrorInfo);
                throw yfcException;
              } else {
                Element inDocRootEle = inDoc.getDocumentElement();
                inDocRootEle.setAttribute("POSError", sErrorInfo);
                inDocRootEle.setAttribute("FailedResponse", "True");
                GCCommonUtil.invokeService(env, GCConstants.GC_PROCESS_POS_RESPONSE_SERVICE, inDoc);
              }
            }
          }else{
            bHasCallFailed = true;
          }
        }
      }
    } else if (nlFaultString.getLength()>0){
      Element eleFaultString = (Element) nlFaultString.item(0);
      String sFaultString = eleFaultString.getTextContent();
      Element inDocRootEle = inDoc.getDocumentElement();
      inDocRootEle.setAttribute("POSError", sFaultString);
      inDocRootEle.setAttribute("FailedResponse", "True");
      GCCommonUtil.invokeService(env, GCConstants.GC_PROCESS_POS_RESPONSE_SERVICE, inDoc);
    }
    else {
      bHasCallFailed = true;
    }
    // Invoke Change order API to append POS sales ticket no at order line / extn level and update
    // component price if bundle line is shipped
    if (bHasCallFailed) {
      YFCException yfcException = new YFCException();
      LOGGER.endTimer("GCPOSInvoicing.processWebServiceResponse()");
      LOGGER.verbose("GCPOSInvoicing - processWebServiceResponse() : End With Error - POS Rejected");
      Element inDocRootEle = inDoc.getDocumentElement();
      inDocRootEle.setAttribute("POSError", GCErrorConstants.WEB_SERVICE_POS_INVOICE_CALL_TIMEOUT_CODE);
      if(YFCCommon.equals(env.getTxnObject("IsHoldAppiled"), "True")){
        yfcException.setAttribute(GCConstants.ERROR_CODE, GCErrorConstants.WEB_SERVICE_POS_INVOICE_CALL_TIMEOUT_CODE);
        yfcException.setAttribute(GCConstants.ERROR_DESCRIPTION, "Could not connect to POS. Timeout");
        throw yfcException;
      } else{
        GCPublishReturnInvoice.postMessageAndRollbackTransaction(env, inDoc, yfcException);
      }
    }
    LOGGER.endTimer("GCPOSInvoicing.processWebServiceResponse()");
    LOGGER.verbose("GCPOSInvoicing - processWebServiceResponse() : End");
  }


  /**
   * This method is used to stamp taxPercentage in case if there are multiple LineTaxes & tax
   * percentage is zero for some but not all. In this case we'll stamp any non-zero taxPercentage
   * value from the other LineTax elements
   *
   * @param document
   */
  public void handleZeroTaxPercentage(YFCDocument yfcDocGetShipmentListOutput) {
    LOGGER.beginTimer("GCGetPOSReservationID.handleZeroTaxPercentage()");
    LOGGER.verbose("GCGetPOSReservationID - handleZeroTaxPercentage() : Start");

    YFCElement yfcEleShipments = yfcDocGetShipmentListOutput.getDocumentElement();

    YFCElement eleLineDetails = yfcEleShipments.getElementsByTagName(GCConstants.LINE_DETAILS).item(0);
    YFCNodeList<YFCElement> nlLineDetail = eleLineDetails.getElementsByTagName(GCConstants.LINE_DETAIL);
    for (int i = 0; i < nlLineDetail.getLength(); i++) {
      YFCElement eleLineDetail = nlLineDetail.item(i);
      YFCNodeList<YFCElement> nlLineTax = eleLineDetail.getElementsByTagName(GCConstants.LINE_TAX);
      YFCNodeList<YFCElement> nlLineTax1 = eleLineDetail.getElementsByTagName(GCConstants.LINE_TAX);
      for (YFCElement eleLineTax : nlLineTax) {
        Double taxPercentage = eleLineTax.getDoubleAttribute(GCConstants.TAX_PERCENTAGE);
        if (taxPercentage == 0.00) {
          for (YFCElement eleLineTax1 : nlLineTax1) {
            Double taxPer = eleLineTax1.getDoubleAttribute(GCConstants.TAX_PERCENTAGE);
            if (taxPer > 0.00) {
              eleLineTax.setAttribute(GCConstants.TAX_PERCENTAGE, taxPer);
              break;
            }
          }
        }
      }
    }
    LOGGER.verbose("GCGetPOSReservationID - handleZeroTaxPercentage() : Ends");
    LOGGER.endTimer("GCGetPOSReservationID.handleZeroTaxPercentage()");
  }


  public void stampLatestPriceOverrideNote(YFCDocument inDoc) {
    YFCNodeList<YFCElement> nlLineDetail = inDoc.getElementsByTagName(GCConstants.LINE_DETAIL);
    for (YFCElement eleLineDetail : nlLineDetail) {
      YFCElement eleOrderLine = eleLineDetail.getChildElement(GCConstants.ORDER_LINE);
      YFCNodeList<YFCElement> nlNote = eleOrderLine.getElementsByTagName(GCConstants.NOTE);
      int count = nlNote.getLength();
      if (count == 1) {
        YFCElement eleNote = nlNote.item(0);
        String noteText = eleNote.getAttribute(GCConstants.NOTE_TEXT);
        if (noteText.contains("PriceOverrideReasonCode:")) {
          eleNote.setAttribute("IsLatestNote", GCConstants.YES);
        }
      } else {
        long maxNoteskey = 0;
        for (YFCElement eleNote : nlNote) {
          String noteText = eleNote.getAttribute(GCConstants.NOTE_TEXT);
          if (noteText.contains("PriceOverrideReasonCode:")) {
            String sNotesKey = eleNote.getAttribute("NotesKey");
            if (sNotesKey.length() > 13) {
              String subNoteKey = sNotesKey.substring(0, 13);
              long notesKey = Long.parseLong(subNoteKey);
              maxNoteskey = Math.max(maxNoteskey, notesKey);
            }
          }
        }
        if (maxNoteskey != 0) {
          String NotesKeyMax = Long.toString(maxNoteskey);
          for (YFCElement eleNote : nlNote) {
            String sNotesKey = eleNote.getAttribute("NotesKey");
            String noteText = eleNote.getAttribute(GCConstants.NOTE_TEXT);
            String subNote = sNotesKey.substring(0,13);
            //GCSTORE 4901 START
            long subNoteInt = Long.parseLong(subNote);
            String sSubNote = Long.toString(subNoteInt);
            //GCSTORE 4901 END
            if (noteText.contains("PriceOverrideReasonCode:") && YFCUtils.equals(NotesKeyMax, sSubNote)) {
              eleNote.setAttribute("IsLatestNote", GCConstants.YES);
              break;
            }
          }
        }
      }
    }

  }
}