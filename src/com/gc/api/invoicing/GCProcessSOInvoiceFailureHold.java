/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 * ####################################################################################################################
 *	OBJECTIVE: Write what this class is about
 *######################################################################################################################
 *	Version     Date       Modified By              Description
 *######################################################################################################################
    1.0      01/04/2015   Mittal, Yashu		JIRA No: Description of issue.
 *######################################################################################################################
 */
package com.gc.api.invoicing;

import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:yashu.mittal@expicient.com">Yashu Mittal</a>
 */
public class GCProcessSOInvoiceFailureHold {

	private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCProcessSOInvoiceFailureHold.class.getName());

	/**
	 * 
	 * Description of processPOSInvoiceFailureHold
	 *
	 * @param env
	 * @param inDoc
	 * @throws GCException 
	 * @throws TransformerException 
	 *
	 */
	public void processPOSInvoiceFailureHold(YFSEnvironment env, Document inDoc) throws GCException, TransformerException {


		LOGGER.beginTimer("GCProcessPOSInvoiceFailureHold.processPOSInvoiceFailureHold");
		LOGGER.verbose("Class: GCProcessPOSInvoiceFailureHold Method: processPOSInvoiceFailureHold START");

		YFCDocument yfcDoc = YFCDocument.getDocumentFor(inDoc);
		YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
		if(LOGGER.isDebugEnabled()){
			LOGGER.verbose("Input doc to GCProcessPOSInvoiceFailureHold.processPOSInvoiceFailureHold method is:--> " + yfcInDoc.toString());
		}
		YFCElement rootEle = yfcDoc.getDocumentElement();
		String sHoldType = rootEle.getAttribute(GCConstants.HOLD_TYPE);
		String sStatus = rootEle.getAttribute(GCConstants.STATUS);

		LOGGER.verbose("HoldType is :"  + sHoldType);
		LOGGER.verbose("HoldStatus is :" + sStatus );
		if (YFCCommon.equals(GCConstants.GC_POS_INVOICE_FAILURE_HOLD, sHoldType) && YFCCommon.equals(GCConstants.RESOLVE_HOLD_CODE, sStatus)) {

			String sOrderHeaderKey = rootEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
			YFCDocument getOrderInvoiceListIn =
					YFCDocument.getDocumentFor("<OrderInvoice OrderHeaderKey='" + sOrderHeaderKey + "' Status='" + GCConstants.ZERO_ONE + "'><Extn ExtnPOSRetryCount='"
							+ GCConstants.GC_POS_CONNECT_MAX_RETRY_COUNT + "'/></OrderInvoice>");

			YFCDocument getOrderInvoiceListOut = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_INVOICE_LIST, getOrderInvoiceListIn, null);

			YFCElement eleInvoiceList = getOrderInvoiceListOut.getDocumentElement();
			YFCNodeList<YFCElement> nlOrderInvoice = eleInvoiceList.getElementsByTagName(GCConstants.ORDER_INVOICE);
			for (YFCElement eleOrderInvoice : nlOrderInvoice) {
				String sOrderInvoiceKey = eleOrderInvoice.getAttribute(GCConstants.ORDER_INVOICE_KEY);
				YFCDocument getOrderInvoiceDetailsIn = YFCDocument.getDocumentFor("<GetOrderInvoiceDetails InvoiceKey='" + sOrderInvoiceKey + "'/>");
				Document getOrderInvoiceDetailTemp = GCCommonUtil.getDocumentFromPath("/global/template/event/SEND_INVOICE.PUBLISH_INVOICE_DETAIL.xml");
				YFCDocument getOrderInvoiceDetailsOut =
						GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_INVOICE_DETAILS, getOrderInvoiceDetailsIn, YFCDocument.getDocumentFor(getOrderInvoiceDetailTemp));
				GCPOSInvoicing ojbGCPOSInvoicing = new GCPOSInvoicing();
				env.setTxnObject("IsHoldAppiled", "True");
				ojbGCPOSInvoicing.createAndPublishPOSInvoice(env, getOrderInvoiceDetailsOut.getDocument());

			}
		}
		LOGGER.endTimer("GCProcessPOSInvoiceFailureHold.processPOSInvoiceFailureHold");
		LOGGER.verbose("Class: GCProcessPOSInvoiceFailureHold Method: processPOSInvoiceFailureHold END");
	}
}
