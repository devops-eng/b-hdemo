/**
 * Copyright 2014 Guitar Center. All rights reserved. Guitar Center PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 *
 * Change Log
 * #######################################################################################
 * ###########
 * #######################################################################################
 * ############################ OBJECTIVE: This class is used to publish the Invoice to POS for
 * Appeasement.
 * #####################################################################################
 * #############
 * #####################################################################################
 * ############################### Version CR/ User Story Date Modified By Description
 * ##############
 * ####################################################################################
 * ##############
 * ####################################################################################
 * ################## 1.0 GCSTORE-800 03/30/2015 Jain,Shubham This class is used to publish the
 * Invoice to POS for Appeasement.
 * ##################################################################
 * ################################
 * ##################################################################
 * ##################################################
 */

package com.gc.api.invoicing;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.gc.api.returns.GCPublishReturnInvoice;
import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * This class is used to publish the Invoice to POS for Appeasement.
 * @author shubham.jain
 * <a href="mailto:shubham.jain@expicient.com">Shubham</a>
 *
 */
public class GCPublishInvoiceToPOS {
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCPublishInvoiceToPOS.class.getName());

  /**
   * This method is used to create the input and publish the appeasement invoice invoking
   * synchronous web service.
   *
   * @param env
   * @param inDoc
   * @return
   * @throws GCException
   */
  public Document createAndPublishAppeasementInvoice(YFSEnvironment env, Document inDoc) throws GCException {

    LOGGER.beginTimer("GCPublishInvoiceToPOS.createAndPublishAppeasementInvoice");
    LOGGER.verbose("Class: GCPublishInvoiceToPOS Method: createAndPublishAppeasementInvoice BEGIN");

    replaceBundleParentWithPrimaryComponent(env,inDoc);
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input Document :" + inputDoc.getString());
    }

    YFCElement eleRoot = inputDoc.getDocumentElement();
    YFCElement eleInvoiceHeader = eleRoot.getChildElement(GCConstants.INVOICE_HEADER);
    String sCreditMemo = eleInvoiceHeader.getAttribute("InvoiceType");
    if (YFCCommon.equals(sCreditMemo, "CREDIT_MEMO")) {
      getAndStampDates(eleRoot);
      String eleOrderInvoiceKey = eleInvoiceHeader.getAttribute("OrderInvoiceKey");
      YFCElement eleLineDetails = eleInvoiceHeader.getChildElement(GCConstants.LINE_DETAILS);
      YFCElement eleLineDetail = eleLineDetails.getChildElement(GCConstants.LINE_DETAIL);
      YFCElement eleLineCharges = eleLineDetail.getChildElement(GCConstants.LINE_CHARGES);
      YFCElement eleLineCharge = eleLineCharges.getChildElement(GCConstants.LINE_CHARGE);
      String sChargeName = eleLineCharge.getAttribute(GCConstants.CHARGE_NAME);

      if (YFCCommon.equals(sChargeName, GCConstants.CUSTOMER_APPEASEMENT)) {
        eleInvoiceHeader.setAttribute("AppeasementType", "ORDER");
      }

      if (YFCCommon.equals(sChargeName, "SHIPPING_APPEASEMENT")) {
        eleInvoiceHeader.setAttribute("AppeasementType", "SHIP");
      }

      // Invoke service to form input XML for web service call
      YFCDocument yfcDocInvokeWebServiceInput =
          GCCommonUtil.invokeService(env, "GCPrepareAppeasementInvoiceToPOS", inputDoc);

      // Decrypt CreditCard No from OMS private key and encrypt using POS public key.
      GCPublishReturnInvoice.encryptDecryptCCForPOS(yfcDocInvokeWebServiceInput);

      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("yfcDocInvokeWebServiceInput:" + yfcDocInvokeWebServiceInput.getString());
      }
      // Invoke web service with POS input XML
      YFCDocument yfcDocInvokeWebServiceOutput = null;

      
      try {
        yfcDocInvokeWebServiceOutput =
            GCCommonUtil.invokeService(env, "GCInvokePOSAppeasementWebService", yfcDocInvokeWebServiceInput);

        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("yfcDocInvokeWebServiceOutput:" + yfcDocInvokeWebServiceOutput.getString());
        }
      } catch (YFSException yfsExcep) {
        LOGGER.error("Inside catch block of GCPublishInvoiceToPOS.createAndPublishAppeasementInvoice(): ", yfsExcep);
        LOGGER.endTimer("GCPublishInvoiceToPOS.createAndPublishAppeasementInvoice()");
        LOGGER.verbose("GCPublishInvoiceToPOS - createAndPublishAppeasementInvoice() : End With Error - Timeout");
        yfsExcep.setErrorCode(GCErrorConstants.WEB_SERVICE_POS_INVOICE_CALL_TIMEOUT_CODE);
        throw yfsExcep;
      }
      
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("yfcDocInvokeWebServiceOutput : " + yfcDocInvokeWebServiceOutput);
      }

      String sStatus = GCConstants.FALSE_STR;
      YFCElement yfcEleResponse = null;
      try {
        yfcEleResponse = processWebServiceResponse(env,inDoc,yfcDocInvokeWebServiceOutput);
        if(!YFCCommon.isVoid(yfcEleResponse)){
        	 YFCNodeList yfcNodeListSuccess = yfcEleResponse.getElementsByTagName(GCConstants.SUCCESS);
             YFCNode yfcEleSuccess = null;
             if(!YFCCommon.isVoid(yfcNodeListSuccess)){
             	yfcEleSuccess =  yfcNodeListSuccess.item(0);
             }
             		
             if (!YFCCommon.isVoid(yfcEleSuccess)) {
               sStatus = yfcEleSuccess.getNodeValue();
             }
        	
        }
       
      } catch (YFSException yfsExcep) {
        LOGGER.error("Inside catch block of GCPublishInvoiceToPOS.createAndPublishAppeasementInvoice(): ", yfsExcep);
        LOGGER.endTimer("GCPublishInvoiceToPOS.createAndPublishAppeasementInvoice");
        LOGGER.verbose("GCPublishInvoiceToPOS - createAndPublishAppeasementInvoice() : End With Error - Timeout");
        yfsExcep.setErrorCode(GCErrorConstants.WEB_SERVICE_POS_INVOICE_CALL_TIMEOUT_CODE);
        throw yfsExcep;
      }


      if (YFCCommon.equalsIgnoreCase(sStatus, "true")) {
        YFCElement yfcPOSSalesTicket = yfcEleResponse.getElementsByTagName("POSSalesTicket").item(0);
        YFCElement yfcPOSCOATicket = yfcEleResponse.getElementsByTagName("POSCOATicket").item(0);


        YFCDocument changeOrderInvoice = YFCDocument.createDocument(GCConstants.ORDER_INVOICE);
        YFCElement eleOrderInvoice = changeOrderInvoice.getDocumentElement();
        eleOrderInvoice.setAttribute(GCConstants.ORDER_INVOICE_KEY, eleOrderInvoiceKey);

        YFCElement eleExtn = eleOrderInvoice.createChild(GCConstants.EXTN);
        eleExtn.setAttribute("ExtnTargetSystem", "POS");


        if (!YFCCommon.isVoid(yfcPOSCOATicket)) {
          String sPOSCOATicket = yfcPOSCOATicket.getNodeValue();
          eleExtn.setAttribute("ExtnPOS70TicketNo", sPOSCOATicket);
        }

        if (!YFCCommon.isVoid(yfcPOSSalesTicket)) {
          String sPOSSalesTicket = yfcPOSSalesTicket.getNodeValue();
          eleExtn.setAttribute("ExtnPOSReturn11TicketNo", sPOSSalesTicket);
        }

        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("ChangeOrderInvoice Input :" + changeOrderInvoice.getString());
        }
        GCCommonUtil.invokeService(env, "GCAppeasementIntegService", changeOrderInvoice);
        return inputDoc.getDocument();
      }
      
      // GCSTORE -- 6539 . Following has been handled in GC_PROCESS_POS_RESPONSE_SERVICE
     /* if (YFCCommon.equalsIgnoreCase(GCConstants.FALSE_STR, sStatus)) {
        // Raise Alert to production support
        GCCommonUtil.invokeService(env, "POSAppeaseInvoiceFailureAlertService", inDoc);
        LOGGER.endTimer("GCPublishInvoiceToPOS.createAndPublishAppeasementInvoice");
        LOGGER.verbose("GCPublishInvoiceToPOS - createAndPublishAppeasementInvoice() : End");
        return inputDoc.getDocument();
      }
      */
    }
    LOGGER.endTimer("GCPublishInvoiceToPOS.createAndPublishAppeasementInvoice");
    return inputDoc.getDocument();
  }

  /**
   * This method will process Web service response from POS.
 * @param env 
 * @param inDoc 
   *
   * @param yfcDocInvokeWebServiceOutput
   * @return
   */
  YFCElement processWebServiceResponse(YFSEnvironment env, Document inDoc, YFCDocument yfcDocInvokeWebServiceOutput) {
    LOGGER.beginTimer("GCPublishInvoiceToPOS.processWebServiceResponse()");
    LOGGER.verbose("GCPublishInvoiceToPOS - processWebServiceResponse() : Start");

    String sConfirmDetail = GCConstants.BLANK;
    NodeList nConfirmResultDetailList = null;
    NodeList nlFaultString = null;
    
    nConfirmResultDetailList = yfcDocInvokeWebServiceOutput.getDocument().
    		getElementsByTagName("AppeasementResult");
    nlFaultString = yfcDocInvokeWebServiceOutput.
    		getDocument().getElementsByTagName("faultstring");
    
  
    boolean bHasCallFailed = false;
    if (nConfirmResultDetailList.getLength() > 0) {
		Node nConfirmResultDetail = nConfirmResultDetailList.item(0);
		if (YFCCommon.isVoid(nConfirmResultDetail)) {
			bHasCallFailed = true;
		}else {
	      sConfirmDetail = nConfirmResultDetail.getTextContent();
	      if (YFCCommon.isStringVoid(sConfirmDetail)) {
	        bHasCallFailed = true;
	      }else{
	          YFCDocument yfcDocResponse = YFCDocument.getDocumentFor(sConfirmDetail);
	          YFCElement yfcEleResponse = yfcDocResponse.getDocumentElement();
	          YFCElement yfcEleSuccess = yfcEleResponse.getElementsByTagName(GCConstants.SUCCESS).item(0);
	          LOGGER.debug("Calling GC POS response service");
	          // Changes for GCSTORE--6539 START
	          if (!YFCCommon.isVoid(yfcEleSuccess)) {
	            String sStatus = yfcEleSuccess.getNodeValue();
	            if (YFCCommon.equals(sStatus, GCConstants.FALSE_STR)) {
	              YFCNodeList<YFCElement> nlErrorInfo = yfcEleResponse.getElementsByTagName("Errorinfo");
	              YFCElement eleErrorInfo = nlErrorInfo.item(0);
	              String sErrorInfo = "";
	              if(!YFCCommon.isVoid(eleErrorInfo)){
	                sErrorInfo =  eleErrorInfo.getNodeValue();
	              }
	              if(YFCCommon.equals(env.getTxnObject("IsHoldAppiled"), "True")){
	                YFCException yfcException = new YFCException();
	                yfcException.setAttribute(GCConstants.ERROR_CODE, sErrorInfo);
	                yfcException.setAttribute(GCConstants.ERROR_DESCRIPTION, sErrorInfo);
	                throw yfcException;
	              } else {
	                Element inDocRootEle = inDoc.getDocumentElement();
	                inDocRootEle.setAttribute("POSError", sErrorInfo);
	                inDocRootEle.setAttribute("FailedResponse", "True");
	                GCCommonUtil.invokeService(env, GCConstants.GC_PROCESS_POS_RESPONSE_SERVICE, inDoc);
	              }
	            }
	          }else{
	            bHasCallFailed = true;
		      }
	      }
		}
    }	else if (nlFaultString.getLength() > 0){
    	Element eleFaultString = (Element) nlFaultString.item(0);
        String sFaultString = eleFaultString.getTextContent();
        Element inDocRootEle = inDoc.getDocumentElement();
        inDocRootEle.setAttribute("POSError", sFaultString);
        inDocRootEle.setAttribute("FailedResponse", "False");
        GCCommonUtil.invokeService(env, GCConstants.GC_PROCESS_POS_RESPONSE_SERVICE, inDoc);
    }
        	  // CHanges for GCSTORE--6539 END
    if (bHasCallFailed) {
      LOGGER.endTimer("GCPublishInvoiceToPOS.processWebServiceResponse()");
      LOGGER.verbose("GCPublishInvoiceToPOS - processWebServiceResponse() : End With Error - POS Rejected");
  
      YFSException yfsExcep = new YFSException();
      yfsExcep.setErrorCode(GCErrorConstants.WEB_SERVICE_POS_INVOICE_CALL_TIMEOUT_CODE);
      throw yfsExcep;
    }


    YFCDocument yfcDocResponse = YFCDocument.getDocumentFor(sConfirmDetail);
    YFCElement yfcEleResponse = yfcDocResponse.getDocumentElement();

    LOGGER.endTimer("GCPublishInvoiceToPOS.processWebServiceResponse()");
    LOGGER.verbose("GCPublishInvoiceToPOS - processWebServiceResponse() : End");
    return yfcEleResponse;

  }

  /**
   * Method to get and stamp Order and current system date & time
   *
   * @param yfcEleRootInDoc
   * @return
   */
  private YFCElement getAndStampDates(YFCElement yfcEleRootInDoc) {
    LOGGER.beginTimer("GCPublishInvoiceToPOS.getAndStampDates()");
    LOGGER.verbose("GCPublishInvoiceToPOS - getAndStampDates() : Start");

    YFCElement yfcEleOrder = yfcEleRootInDoc.getElementsByTagName(GCConstants.ORDER).item(0);
    Date dOrderDateTime = yfcEleOrder.getDateAttribute(GCConstants.ORDER_DATE);
    GCDateUtils.getDateTimeForPOS(yfcEleRootInDoc, dOrderDateTime);

    LOGGER.endTimer("GCPublishInvoiceToPOS.getAndStampDates()");
    LOGGER.verbose("GCPublishInvoiceToPOS - getAndStampDates() : End");

    return yfcEleRootInDoc;
  }

  // Fix to stampPrimaryComponent in on POS input in case of SET items

  private void replaceBundleParentWithPrimaryComponent(YFSEnvironment env, Document inputDoc){

    Element eleRoot = inputDoc.getDocumentElement();
    Element eleInvoiceHeader =  (Element) eleRoot.getElementsByTagName(GCConstants.ORDER).item(0);
    String sOrderHeaderKey = eleInvoiceHeader.getAttribute(GCConstants.ORDER_HEADER_KEY);

    List<Element> listBundleParentLineDetail =
        GCXMLUtil.getElementListByXpath(inputDoc,
            "InvoiceDetail/InvoiceHeader/LineDetails/LineDetail/OrderLine[@KitCode='BUNDLE']/..");

    if (!listBundleParentLineDetail.isEmpty()) {

      LOGGER.verbose("Appeasement is applied on BundleParent Line. Replace BundleParent Orderline with PrimaryComp orderline ");
      Document getOrderListIp = GCXMLUtil.getDocument("<Order OrderHeaderKey='"+sOrderHeaderKey+"'/>");
      Document getOrderListTemp =  GCXMLUtil.getDocument("<OrderList><Order><OrderLines><OrderLine><Item/><ItemDetails><PrimaryInformation/>"
          + "<Extn/></ItemDetails><Extn/><OrderStatuses><OrderStatus ShipNode=''/></OrderStatuses><Schedules><Schedule ShipNode=''/></Schedules></OrderLine></OrderLines></Order></OrderList>");
      Document getOrderListOut = GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST, getOrderListIp,getOrderListTemp);
      Iterator<Element> iteratorListBundleLineDetail = listBundleParentLineDetail.iterator();
      while (iteratorListBundleLineDetail.hasNext()) {
        Element eleBundleParentLineDetail = iteratorListBundleLineDetail.next();
        Element eleBundleParentOrderLine = (Element) eleBundleParentLineDetail.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
        Element eleParentItemDetails = (Element) eleBundleParentOrderLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
        Element eleParentExtn = (Element) eleParentItemDetails.getElementsByTagName(GCConstants.EXTN).item(0);
        String sExtnPrimaryComponentItemID = eleParentExtn.getAttribute(GCConstants.EXTN_PRIMARY_COMPONENT_ID);
        if(!YFCCommon.isVoid(sExtnPrimaryComponentItemID)){
          // Fetch Primary Component OrderLine Details from getOrderList Output and replace with BundleParent
          Element elePrimaryCompOrderLine =
              GCXMLUtil.getElementByXPath(getOrderListOut,
                  "OrderList/Order/OrderLines/OrderLine[@OrderedQty!='0']/ItemDetails[@ItemID='"
                      + sExtnPrimaryComponentItemID + "']/..");
          if(!YFCCommon.isVoid(elePrimaryCompOrderLine)){
            Element eleCloneLine = inputDoc.createElement("OrderLine");
            GCXMLUtil.copyElement(inputDoc, elePrimaryCompOrderLine, eleCloneLine);
            eleBundleParentLineDetail.removeChild(eleBundleParentOrderLine);
            eleBundleParentLineDetail.appendChild(eleCloneLine);
          }
        }
      }
    }
  }





}