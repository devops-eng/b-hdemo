/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: To raise alert for the increased amount
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0               05/28/2015        Singh, Gurpreet            GCSTORE-2514: Line Level Taxes (CR-022)
 *#################################################################################################################################################################
 */
package com.gc.api.invoicing;

import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author yashu.mittal
 *
 */
public class GCStampChargesTaxesOnOrderLineComponentsOnInvoice implements YIFCustomApi {
	// initialize logger
	private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCStampChargesTaxesOnOrderLineComponentsOnInvoice.class);
	public void stampChargesTaxesOnOrderLineComponents(YFSEnvironment env, Document inDoc) {

		LOGGER.beginTimer("GCStampChargesTaxesOnOrderLineComponents.stampChargesTaxesOnOrderLineComponents()");
	    LOGGER.verbose("GCStampChargesTaxesOnOrderLineComponents - stampChargesTaxesOnOrderLineComponents() : Start");
	    
	    Element eleInvoiceHeader = (Element) inDoc.getDocumentElement().getElementsByTagName(GCConstants.INVOICE_HEADER).item(0);
	    String sDocumentType = eleInvoiceHeader.getAttribute(GCConstants.DOCUMENT_TYPE);
		boolean bAnyBundleLineExists = true;
	    boolean bIsSalesOrder = true;
	    if(YFCCommon.equals(sDocumentType, GCConstants.RETURN_ORDER_DOCUMENT_TYPE)){
	    	bIsSalesOrder = false;
	    }
		List<Element> listBundleParentLineDetail =
				GCXMLUtil.getElementListByXpath(inDoc,
						"InvoiceDetail/InvoiceHeader/LineDetails/LineDetail/OrderLine[@KitCode='BUNDLE']");

		
		if (listBundleParentLineDetail.isEmpty()) {
			bAnyBundleLineExists = false;
		}

		if(bAnyBundleLineExists && bIsSalesOrder){
			LOGGER.verbose("BundleLine Exists on the order.");
			Iterator<Element> iteratorListBundleLineDetail = listBundleParentLineDetail.iterator();
			YFCDocument changeOrderDoc = YFCDocument.createDocument("Order");
			YFCElement eleChangeOrderRoot = changeOrderDoc.getDocumentElement();
			YFCElement eleChangeOrderOrderLines = eleChangeOrderRoot.createChild(GCConstants.ORDER_LINES);
			YFCDocument yfcDocument = YFCDocument.getDocumentFor(inDoc);
			YFCElement eleRoot = yfcDocument.getDocumentElement();
			YFCElement eleOrder = eleRoot.getElementsByTagName(GCConstants.ORDER).item(0);
			String sOrderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
			eleChangeOrderRoot.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
			eleChangeOrderRoot.setAttribute(GCConstants.SELECT_METHOD, GCConstants.WAIT);
			
			// Code to stamp attribute ExtnIsAmountCollected='Y' on each orderline : Fix for 5067
			NodeList listRegularLineDetail = inDoc.getElementsByTagName(GCConstants.LINE_DETAIL);
			for (int i= 0 ; i < listRegularLineDetail.getLength() ; i++) {
				Element eleLineDetail = (Element) listRegularLineDetail.item(i);
				Element eleChildLineDetail = (Element) eleLineDetail.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
				String sBundleParentOrderLineKey = eleChildLineDetail.getAttribute(GCConstants.BUNDLE_PARENT_ORDERLINEKEY);
				if(YFCCommon.isVoid(sBundleParentOrderLineKey)){
					YFCElement eleChangeOrderOrderLine = eleChangeOrderOrderLines.createChild(GCConstants.ORDER_LINE);
					eleChangeOrderOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, eleChildLineDetail.getAttribute(GCConstants.ORDER_LINE_KEY));
					YFCElement eleChangeOrderExtn = eleChangeOrderOrderLine.createChild(GCConstants.EXTN);
					eleChangeOrderExtn.setAttribute("ExtnIsAmountCollected", "Y");
					
				}
			
			while (iteratorListBundleLineDetail.hasNext()) {
				Element eleBundleParentLineDetail = iteratorListBundleLineDetail.next();
				String sOrderLineKey = eleBundleParentLineDetail.getAttribute(GCConstants.ORDER_LINE_KEY);
				LOGGER.verbose("BundleParentOrderLineKey : " + sOrderLineKey);
				List<Element> listChildLineDetail =
						GCXMLUtil.getElementListByXpath(inDoc,
								"InvoiceDetail/InvoiceHeader/LineDetails/LineDetail/OrderLine[@BundleParentOrderLineKey='"
										+ sOrderLineKey + "']/..");

				for (Element eleOrderLine : listChildLineDetail) {
					String sCompOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
					YFCElement eleChangeOrderOrderLine = eleChangeOrderOrderLines.createChild(GCConstants.ORDER_LINE);
					eleChangeOrderOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, sCompOrderLineKey);

					YFCElement eleExtn = eleChangeOrderOrderLine.createChild(GCConstants.EXTN);
					
					// Code to stamp attribute ExtnIsAmountCollected='Y' on each orderline : Fix for 5067
					eleExtn.setAttribute("ExtnIsAmountCollected", "Y");
					YFCElement eleGCOrderLineComponentsList = eleExtn.createChild(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS_LIST);
					Element eleLinePriceInfo = (Element) eleOrderLine.getElementsByTagName(GCConstants.LINE_PRICE_INFO).item(0);
					String sUnitPrice = eleLinePriceInfo.getAttribute(GCConstants.UNIT_PRICE);

					String sItemID = ((Element) eleOrderLine.getElementsByTagName(GCConstants.ITEM).item(0)).getAttribute(GCConstants.ITEM_ID);
					String sQuantity = eleOrderLine.getAttribute(GCConstants.ORDERED_QTY);

					// Check if GCOrderLineComp with ChargeCategory=Price exist. If yes update the same, if no create new.
			        NodeList nlGCOrderLineComponents = eleOrderLine.getElementsByTagName(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
			        Element eleGCOrderLineUnitPriceComponent = null;
			        for (int j = 0; j < nlGCOrderLineComponents.getLength(); j++) {
			        	 eleGCOrderLineUnitPriceComponent = (Element) nlGCOrderLineComponents.item(j);
			        	 String sChargeCategory =eleGCOrderLineUnitPriceComponent.getAttribute(GCConstants.CHARGE_CATEGORY);
			        	 String sChargeName =eleGCOrderLineUnitPriceComponent.getAttribute(GCConstants.CHARGE_NAME);
			        	 if(YFCCommon.equals(sChargeCategory, GCConstants.UNIT_PRICE) && YFCCommon.equals(sChargeName, GCConstants.UNIT_PRICE)){
			        		 break;
			        	 }
			        }
			        
			        if(YFCCommon.isVoid(eleGCOrderLineUnitPriceComponent)){
			        	LOGGER.verbose("Component with UnitPrice ChargeCategory does not exist. Create new");
			        	YFCElement eGCOrderLineComp = eleGCOrderLineComponentsList.createChild(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
						eGCOrderLineComp.setAttribute(GCConstants.CHARGE_CATEGORY, GCConstants.UNIT_PRICE);
						eGCOrderLineComp.setAttribute(GCConstants.CHARGE_NAME, GCConstants.UNIT_PRICE);
						eGCOrderLineComp.setAttribute(GCXmlLiterals.AMOUNT, sUnitPrice);
						eGCOrderLineComp.setAttribute(GCConstants.ITEM_ID, sItemID);
						eGCOrderLineComp.setAttribute(GCConstants.QUANTITY, sQuantity);
			        } else{
			        	LOGGER.verbose("Component with Unit Price ChargeCategory exists. Update the same, stamping updated amount");
			        	YFCElement eGCOrderLineComp = eleGCOrderLineComponentsList.createChild(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
			        	eGCOrderLineComp.setAttribute("OrderLineComponentKey", eleGCOrderLineUnitPriceComponent.getAttribute("OrderLineComponentKey"));
			        	eGCOrderLineComp.setAttribute(GCXmlLiterals.AMOUNT,sUnitPrice);
			        }
			        
			        
					NodeList nlLineCharge = eleOrderLine.getElementsByTagName(GCConstants.LINE_CHARGE);
					for (int j = 0; j < nlLineCharge.getLength(); j++) {
						Element eleLineCharge = (Element) nlLineCharge.item(j);
						String sChargeCategory = eleLineCharge.getAttribute(GCConstants.CHARGE_CATEGORY);
						String sChargeName = eleLineCharge.getAttribute(GCConstants.CHARGE_NAME);
						String sIsDiscount = eleLineCharge.getAttribute(GCXmlLiterals.IS_DISCOUNT);
						String sChargeAmount = eleLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT);
						if(YFCCommon.isVoid(sChargeAmount)){
							sChargeAmount = eleLineCharge.getAttribute(GCConstants.CHARGE_PER_LINE);
						}
						if(YFCCommon.isVoid(sChargeAmount)){
							String sChargePerUnit = eleLineCharge.getAttribute(GCConstants.CHARGE_PER_UNIT);
							if(!YFCCommon.isVoid(sChargePerUnit)){
								Double dChargeAmount = Double.parseDouble(sChargePerUnit)*Double.parseDouble(sQuantity);
								sChargeAmount = dChargeAmount.toString();
							} else{
								sChargeAmount = "0.00";
							}
							
						}
						
						// Check if GCOrderLineComp with current looped ChargeCategory exist. If yes update the same, if no create new.
				          Element eleGCOrderLineLineChargeComponent = null;
				          for (int k = 0; k < nlGCOrderLineComponents.getLength(); k++) {
				        	  eleGCOrderLineLineChargeComponent = (Element) nlGCOrderLineComponents.item(k);
				          	 String sCompChargeCategory =eleGCOrderLineLineChargeComponent.getAttribute(GCConstants.CHARGE_CATEGORY);
				          	 String sCompChargeName =eleGCOrderLineLineChargeComponent.getAttribute(GCConstants.CHARGE_NAME);
				          	 if(YFCCommon.equals(sCompChargeCategory, sChargeCategory) && YFCCommon.equals(sCompChargeName, sChargeName)){
				          		 break;
				          	 }
				          }
				         
				          if(YFCCommon.isVoid(eleGCOrderLineLineChargeComponent)){
				        	  LOGGER.verbose("Component with " + sChargeCategory + " ChargeCategory does not exist. Create new");
				        	  YFCElement yfcEleGCOrderLineComponent = eleGCOrderLineComponentsList.createChild(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
				        	  yfcEleGCOrderLineComponent.setAttribute(GCConstants.CHARGE_CATEGORY, sChargeCategory);
				        	  yfcEleGCOrderLineComponent.setAttribute(GCConstants.CHARGE_NAME, sChargeName);
				        	  yfcEleGCOrderLineComponent.setAttribute(GCXmlLiterals.IS_DISCOUNT, sIsDiscount);
				        	  yfcEleGCOrderLineComponent.setAttribute(GCXmlLiterals.AMOUNT, sChargeAmount);
				        	  yfcEleGCOrderLineComponent.setAttribute(GCConstants.ITEM_ID, sItemID);
				        	  yfcEleGCOrderLineComponent.setAttribute(GCConstants.QUANTITY, sQuantity);
				          } else{
				        	  LOGGER.verbose("Component with " + sChargeCategory + "ChargeCategory exists. Update the same, stamping updated amount");
				        	  YFCElement yfcEleComponentUnitPrice = eleGCOrderLineComponentsList.createChild(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
				        	  yfcEleComponentUnitPrice.setAttribute("OrderLineComponentKey", eleGCOrderLineLineChargeComponent.getAttribute("OrderLineComponentKey"));
				        	  yfcEleComponentUnitPrice.setAttribute(GCXmlLiterals.AMOUNT,sChargeAmount);
				          }
					}

					// Loop over line Taxes
					NodeList nlLineTax = eleOrderLine.getElementsByTagName(GCConstants.LINE_TAX);
					for (int k = 0; k < nlLineTax.getLength(); k++) {
						Element eleLineTax = (Element) nlLineTax.item(k);
						String sChargeCategory = eleLineTax.getAttribute(GCConstants.CHARGE_CATEGORY);
						String sChargeName = eleLineTax.getAttribute(GCConstants.TAX_NAME);
						String sIsDiscount = GCConstants.BLANK;
						String sChargeAmount = eleLineTax.getAttribute(GCConstants.TAX);
						if(YFCCommon.isVoid(sChargeAmount)){
							sChargeAmount ="0.00";
						}
						if (YFCCommon.equals(GCConstants.SHIPPING_PROMOTION, sChargeCategory)
								|| YFCCommon.equals(GCConstants.PROMOTION, sChargeCategory)) {
							sIsDiscount = GCConstants.YES;
						}
						
						Element eleGCOrderLineLineTaxComponent = null;
				          for (int l = 0; l < nlGCOrderLineComponents.getLength(); l++) {
				        	  eleGCOrderLineLineTaxComponent = (Element) nlGCOrderLineComponents.item(l);
				          	 String sCompChargeCategory =eleGCOrderLineLineTaxComponent.getAttribute(GCConstants.CHARGE_CATEGORY);
				          	 String sCompChargeName =eleGCOrderLineLineTaxComponent.getAttribute(GCConstants.CHARGE_NAME);
				          	 if(YFCCommon.equals(sCompChargeCategory, sChargeCategory) && YFCCommon.equals(sCompChargeName, sChargeName)){
				          		 break;
				          	 }
				          }
				          
				          if(YFCCommon.isVoid(eleGCOrderLineLineTaxComponent)){
				        	  LOGGER.verbose("Component with " + sChargeCategory + " ChargeCategory does not exist. Create new");
				        	  YFCElement yfcEleGCOrderLineComponent = eleGCOrderLineComponentsList.createChild(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
				        	  yfcEleGCOrderLineComponent.setAttribute(GCConstants.CHARGE_CATEGORY, sChargeCategory);
				        	  yfcEleGCOrderLineComponent.setAttribute(GCConstants.CHARGE_NAME, sChargeName);
				        	  yfcEleGCOrderLineComponent.setAttribute(GCXmlLiterals.IS_DISCOUNT, sIsDiscount);
				        	  yfcEleGCOrderLineComponent.setAttribute(GCXmlLiterals.AMOUNT, sChargeAmount);
				        	  yfcEleGCOrderLineComponent.setAttribute(GCConstants.ITEM_ID, sItemID);
				        	  yfcEleGCOrderLineComponent.setAttribute(GCConstants.QUANTITY, sQuantity);
				          } else{
				        	  LOGGER.verbose("Component with " + sChargeCategory + "ChargeCategory exists. Update the same, stamping updated amount");
				        	  YFCElement yfcEleComponentUnitPrice = eleGCOrderLineComponentsList.createChild(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
				        	  yfcEleComponentUnitPrice.setAttribute("OrderLineComponentKey", eleGCOrderLineLineTaxComponent.getAttribute("OrderLineComponentKey"));
				        	  yfcEleComponentUnitPrice.setAttribute(GCXmlLiterals.AMOUNT,sChargeAmount);
				          }
					}
				}
			}
			 if (LOGGER.isVerboseEnabled()) {
			      LOGGER.verbose("ChangeOrderInput to stamp orderline components : " + changeOrderDoc.toString());
			    }
			GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, changeOrderDoc.getDocument());
			} 

		} else if(bIsSalesOrder){
			
			YFCDocument changeOrderDoc = YFCDocument.createDocument("Order");
			YFCElement eleChangeOrderRoot = changeOrderDoc.getDocumentElement();
			YFCElement eleChangeOrderOrderLines = eleChangeOrderRoot.createChild(GCConstants.ORDER_LINES);
			YFCDocument yfcDocument = YFCDocument.getDocumentFor(inDoc);
			YFCElement eleRoot = yfcDocument.getDocumentElement();
			YFCElement eleOrder = eleRoot.getElementsByTagName(GCConstants.ORDER).item(0);
			String sOrderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
			eleChangeOrderRoot.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
			eleChangeOrderRoot.setAttribute(GCConstants.SELECT_METHOD, GCConstants.WAIT);

			// Code to stamp attribute ExtnIsAmountCollected='Y' on each orderline : Fix for 5067
			NodeList listRegularLineDetail = inDoc.getElementsByTagName(GCConstants.LINE_DETAIL);
			for (int i= 0 ; i < listRegularLineDetail.getLength() ; i++) {
				Element eleLineDetail = (Element) listRegularLineDetail.item(i);
				Element eleChildLineDetail = (Element) eleLineDetail.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
				YFCElement eleChangeOrderOrderLine = eleChangeOrderOrderLines.createChild(GCConstants.ORDER_LINE);
				eleChangeOrderOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, eleChildLineDetail.getAttribute(GCConstants.ORDER_LINE_KEY));
				YFCElement eleChangeOrderExtn = eleChangeOrderOrderLine.createChild(GCConstants.EXTN);
				eleChangeOrderExtn.setAttribute("ExtnIsAmountCollected", "Y");

			}
			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("ChangeOrderInput to stamp orderline components : " + changeOrderDoc.toString());
			}
			GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, changeOrderDoc.getDocument());

		}

		 LOGGER.endTimer("GCStampChargesTaxesOnOrderLineComponents.stampChargesTaxesOnOrderLineComponents()");
		 LOGGER.verbose("GCStampChargesTaxesOnOrderLineComponents - stampChargesTaxesOnOrderLineComponents() : End");
	}

	@Override
	public void setProperties(Properties paramProperties) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
