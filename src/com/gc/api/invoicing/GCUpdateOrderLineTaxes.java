/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 * ####################################################################################################################
 *   OBJECTIVE: Reducing the extra tax amount from order total after invoicing
 *######################################################################################################################
 *   Version     Date         Modified By              Description
 *######################################################################################################################
    1.0         10/06/2015   Singh, Gurpreet          GCSTORE-1909: Reducing the extra tax amount from order total after invoicing
 *######################################################################################################################
 */
package com.gc.api.invoicing;

import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author gurpreet.singh
 *
 */
public class GCUpdateOrderLineTaxes implements YIFCustomApi{
  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCUpdateOrderLineTaxes.class);

  /**
   * Check if tax has been reduced for any charge category and order line is fully invoiced, if yes
   * then invoke change order to reduce the value of increased tax at order line for that charge
   * category
   *
   * @param env
   * @param inDoc
   */
  public void updateOrderLineTaxes(YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("GCUpdateOrderLineTaxes.updateOrderLineTaxes");
    LOGGER.verbose("Class: GCUpdateOrderLineTaxes Method: updateOrderLineTaxes START");
    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" updateOrderLineTaxes() method, yfcInDoc is" + yfcInDoc.toString());
    }
    YFCElement eleRootInDoc = yfcInDoc.getDocumentElement();
    String sOrderHeaderKey = eleRootInDoc.getAttribute(GCConstants.ORDER_HEADER_KEY);
    LOGGER.verbose("sOrderHeaderKey : " + sOrderHeaderKey);
    YFCDocument docGetOrderListIP = YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "'/>");
    YFCDocument docGetOrderListTemp =
        YFCDocument
        .getDocumentFor("<OrderList><Order OrderHeaderKey='' ><OrderLines><OrderLine OrderLineKey='' OrderedQty=''>"
            + "<LineTaxes><LineTax /></LineTaxes><LineRemainingTotals /></OrderLine"
            + "></OrderLines></Order></OrderList>");
    YFCDocument docGetOrderListOP =
        GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST, docGetOrderListIP, docGetOrderListTemp);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" updateOrderLineTaxes() method, docGetOrderListOP is" + docGetOrderListOP.toString());
    }
    YFCElement eleGetOrderList = docGetOrderListOP.getDocumentElement();

    YFCDocument docChangeOrder =
        YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "' Override='Y'/>");
    YFCElement eleChangeOrder = docChangeOrder.getDocumentElement();
    YFCElement eleChangeOrderLines = eleChangeOrder.createChild(GCConstants.ORDER_LINES);
    eleChangeOrder.appendChild(eleChangeOrderLines);

    YFCElement eleLineDetails = eleRootInDoc.getChildElement(GCConstants.LINE_DETAILS);
    YFCNodeList<YFCElement> nlLineDetail = eleLineDetails.getElementsByTagName(GCConstants.LINE_DETAIL);
    for (YFCElement eleLineDetail : nlLineDetail) {
      LOGGER.verbose(" Looping over line detail elements ");
      String sOrderLineKey = eleLineDetail.getAttribute(GCConstants.ORDER_LINE_KEY);
      String sOrderInvoiceDetailKey = eleLineDetail.getAttribute(GCXmlLiterals.ORDER_INVOICE_DETAIL_KEY);
      double dShippedQty = eleLineDetail.getDoubleAttribute(GCConstants.SHIPPED_QUANTITY);
      LOGGER.verbose("sOrderLineKey : " + sOrderLineKey + " sOrderInvoiceDetailKey : " + sOrderInvoiceDetailKey
          + " dShippedQty : " + dShippedQty);
      YFCNodeList<YFCElement> nlOrderLine = eleGetOrderList.getElementsByTagName(GCConstants.ORDER_LINE);
      for (YFCElement eleOrderLine : nlOrderLine) {
        LOGGER.verbose(" Looping over order lines ");
        String strOLKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
        double dOrderedQty = eleOrderLine.getDoubleAttribute(GCConstants.ORDERED_QTY);
        YFCElement eleLineRemainingTotals = eleOrderLine.getChildElement(GCXmlLiterals.LINE_REMAINING_TOTALS);
        double dRemainingTax = eleLineRemainingTotals.getDoubleAttribute(GCConstants.TAX);
        LOGGER
        .verbose("strOLKey : " + strOLKey + " dOrderedQty : " + dOrderedQty + " dRemainingTax : " + dRemainingTax);
        if (YFCCommon.equals(sOrderLineKey, strOLKey) && YFCCommon.equals(dShippedQty, dOrderedQty)
            && (!YFCCommon.isVoid(dRemainingTax) && dRemainingTax > 0.00)) {
          LOGGER.verbose(" Inside if tax amount has been changed for the particaluar charge category ");
          YFCElement eleChangeOrderLine = eleChangeOrderLines.createChild(GCConstants.ORDER_LINE);
          eleChangeOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, sOrderLineKey);
          eleChangeOrderLines.appendChild(eleChangeOrderLine);
          YFCElement eleChangeOrderLineTaxes = eleChangeOrderLine.createChild(GCConstants.LINE_TAXES);
          eleChangeOrderLine.appendChild(eleChangeOrderLineTaxes);
          YFCNodeList<YFCElement> nlOrderLineTax = eleOrderLine.getElementsByTagName(GCConstants.LINE_TAX);
          for (YFCElement eleOrderLineTax : nlOrderLineTax) {
            LOGGER.verbose(" Looping over order line tax elements ");
            YFCElement eleChangeOrderLineTax = eleChangeOrderLineTaxes.createChild(GCConstants.LINE_TAX);
            String sChargeCategory = eleOrderLineTax.getAttribute(GCConstants.CHARGE_CATEGORY);
            String sChargeName = eleOrderLineTax.getAttribute(GCConstants.CHARGE_NAME);
            String sTaxName = eleOrderLineTax.getAttribute(GCConstants.TAX_NAME);
            eleChangeOrderLineTax.setAttribute(GCConstants.CHARGE_CATEGORY, sChargeCategory);
            eleChangeOrderLineTax.setAttribute(GCConstants.CHARGE_NAME, sChargeName);
            eleChangeOrderLineTax.setAttribute(GCConstants.TAX_NAME, sTaxName);
            LOGGER.verbose("sChargeCategory : " + sChargeCategory + " sChargeName : " + sChargeName + " sTaxName : "
                + sTaxName);
            double dTax =
                fetchInvoicedTaxAmount(eleRootInDoc, sOrderInvoiceDetailKey, sChargeCategory, sChargeName, sTaxName);
            LOGGER.verbose("dTax : " + dTax);
            eleChangeOrderLineTax.setAttribute(GCConstants.TAX, String.valueOf(dTax));
            eleChangeOrderLineTaxes.appendChild(eleChangeOrderLineTax);
          }
        }
      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" updateOrderLineTaxes() method, docChangeOrder is" + docChangeOrder.toString());
    }
    GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, docChangeOrder, null);
    LOGGER.endTimer("GCUpdateOrderLineTaxes.publishReturnInvoice");
    LOGGER.verbose("Class: GCUpdateOrderLineTaxes Method: publishReturnInvoice END");
  }

  /**
   * Fetch tax amount from the input document for a particular charge
   *
   * @param eleRootInDoc
   * @param sOrderInvoiceDetailKey
   * @param sChargeCategory
   * @param sChargeName
   * @param sTaxName
   * @return
   */
  private double fetchInvoicedTaxAmount(YFCElement eleRootInDoc, String sOrderInvoiceDetailKey, String sChargeCategory,
      String sChargeName, String sTaxName) {
    LOGGER.beginTimer("GCUpdateOrderLineTaxes.fetchInvoicedTaxAmount");
    LOGGER.verbose("Class: GCUpdateOrderLineTaxes Method: fetchInvoicedTaxAmount START");
    YFCElement eleTaxBreakupList = eleRootInDoc.getChildElement(GCXmlLiterals.TAX_BREAKUP_LIST);
    YFCNodeList<YFCElement> nlTaxBreakup = eleTaxBreakupList.getElementsByTagName(GCXmlLiterals.TAX_BREAKUP);
    for (YFCElement eleTaxBreakup : nlTaxBreakup) {
      LOGGER.verbose(" Inside TaxBreakup List loop ");
      String sLineKey = eleTaxBreakup.getAttribute(GCXmlLiterals.LINE_KEY);
      String strChargeCategory = eleTaxBreakup.getAttribute(GCConstants.CHARGE_CATEGORY);
      String strChargeName = eleTaxBreakup.getAttribute(GCConstants.CHARGE_NAME);
      String strTaxName = eleTaxBreakup.getAttribute(GCConstants.TAX_NAME);
      LOGGER.verbose("sLineKey : " + sLineKey + " strChargeCategory : " + strChargeCategory + " strChargeName : "
          + strChargeName + " strTaxName : " + strTaxName);
      if (YFCCommon.equals(sLineKey, sOrderInvoiceDetailKey) && YFCCommon.equals(strChargeCategory, sChargeCategory)
          && YFCCommon.equals(strChargeName, sChargeName) && YFCCommon.equals(strTaxName, sTaxName)) {
        double dTax = eleTaxBreakup.getDoubleAttribute(GCConstants.TAX);
        LOGGER.endTimer("GCUpdateOrderLineTaxes.fetchInvoicedTaxAmount");
        LOGGER.verbose("Class: GCUpdateOrderLineTaxes Method: fetchInvoicedTaxAmount END");
        LOGGER.verbose("returning dTax" + dTax);
        return dTax;
      }
    }
    LOGGER.endTimer("GCUpdateOrderLineTaxes.fetchInvoicedTaxAmount");
    LOGGER.verbose("Class: GCUpdateOrderLineTaxes Method: fetchInvoicedTaxAmount END");
    return 0;
  }

  @Override
  public void setProperties(Properties arg0) {

  }

}
