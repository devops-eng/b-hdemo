/**
 * Copyright © 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: This class is used to resolve the holds on the warranty lines when their parent line is released.
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0			 08/06/2017			  Expicient			 GCSTORE-4380: Rounding for sets can cause tax and payment discrepancy
 #################################################################################################################################################################
 */

package com.gc.api.order;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCOnDraftOrderConfirm {

	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCOnDraftOrderConfirm.class);
	public Document stampConfirmDraftOrderFlag(YFSEnvironment env,
			Document inDoc) {

		LOGGER.beginTimer("GCOnDraftOrderConfirm.stampConfirmDraftOrderFlag");
		
		env.setTxnObject(GCConstants.ENV_CONFIRM_DRAFT_ORDER_INVOKED, GCConstants.N);
		LOGGER.verbose(" Env ConfirmDraftOrderInvoked: "+env.getTxnObject(GCConstants.ENV_CONFIRM_DRAFT_ORDER_INVOKED));
		
		LOGGER.endTimer("GCOnDraftOrderConfirm.stampConfirmDraftOrderFlag");
		return inDoc;
	}
}
