package com.gc.api.order;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;

import com.gc.api.order.split.GCOrderLineSplitter;
import com.gc.api.order.split.GCSplitOrderLineHelper;
import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCLogUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * GCSTORE-54 This class Split OrderLines if item is unique,set or multiple OrderLine reservation
 * exist. GCSTORE-718: Call Center Order Processing
 *
 * @author Gunjan Kumar
 *
 */
public class GCProcessATGOrders {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCProcessATGOrders.class.getName());

  /**
   * This method Split OrderLines if item is unique,set or multiple OrderLine reservation exist
   *
   * @param env
   * @param cloneOrderDoc
   * @return orderDoc : Splitted OrderDoc if split is required
   */
  public Document splitOrderLines(YFSEnvironment env, Document cloneOrderDoc) {
    LOGGER.beginTimer("GCProcessATGOrders.processOrderLines");
    LOGGER.verbose("Class: GCProcessATGOrders Method: processOrderLines ::BEGIN");
    YFCDocument yfcOrderDoc = YFCDocument.getDocumentFor(cloneOrderDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("\n inDoc to splitOrderLines" + yfcOrderDoc.toString());
    }
    YFCElement orderElem = yfcOrderDoc.getDocumentElement();
    YFCElement orderLinesElem = orderElem.getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> orderLineList = orderLinesElem.getElementsByTagName(GCConstants.ORDER_LINE);
    Double orderedQty=0.0;
    Map<String, Double> allOrderLineOrderedQty = new HashMap<String, Double>();
    Map<String, YFCElement> allOrderLinesMap = new HashMap<String, YFCElement>();
    Map<String, List<String>> dependentOrderLineMap = new HashMap<String, List<String>>();
    for (YFCElement orderLineElem : orderLineList) {
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("\n orderLine Element to check if Order is Parent or dependent" + orderLineElem.toString());
      }
      String sPrimeLineNo = orderLineElem.getAttribute(GCConstants.PRIME_LINE_NO);

      String sOrderLineKey=orderLineElem.getAttribute(GCConstants.ORDER_LINE_KEY);
      orderedQty = orderLineElem.getDoubleAttribute("OrderedQty");
      if (!YFCCommon.isVoid(sOrderLineKey)) {
        allOrderLineOrderedQty.put(sOrderLineKey, orderedQty);
      }
      allOrderLinesMap.put(sPrimeLineNo, orderLineElem);
      YFCElement dependencyElem = orderLineElem.getChildElement(GCConstants.DEPENDENCY);
      if (!YFCCommon.isVoid(dependencyElem)) {
        LOGGER.verbose("OrderLine is dependent");
        String sParentLineKey = dependencyElem.getAttribute(GCXmlLiterals.DEPENDENT_ON_PRIME_LINE_NO);
        dependentOrderLineMap = GCCommonUtil.updateMap(dependentOrderLineMap, sPrimeLineNo, sParentLineKey);
      } else {
        LOGGER.verbose("OrderLine is Parent");
        if (!dependentOrderLineMap.containsKey(sPrimeLineNo)) {
          dependentOrderLineMap.put(sPrimeLineNo, new ArrayList<String>());
        }
      }
    }
    GCLogUtils.verboseLog(LOGGER,  "\n dependentOrderLineMap" , dependentOrderLineMap);
    LOGGER
    .verbose("All OrderLines are populated in map,Now identify which combination need splitting and identify split ratio");

    List<GCSplitOrderLineHelper> helperList = new ArrayList<GCSplitOrderLineHelper>();
    for (Map.Entry<String, List<String>> entry : dependentOrderLineMap.entrySet()) {
      String parentPrimeLineNo = entry.getKey();
      LOGGER.verbose("parentPrimeLineNo : " + parentPrimeLineNo);
      List<String> dependentPrimeLineNoList = entry.getValue();
      GCLogUtils
      .verboseLog(LOGGER, "dependentPrimeLineNoList for parentPrimeLineNo key :\n ", dependentPrimeLineNoList);
      GCSplitOrderLineHelper object =
          new GCSplitOrderLineHelper(env, parentPrimeLineNo, dependentPrimeLineNoList, allOrderLinesMap, null);
      helperList.add(object);
    }

    for(GCSplitOrderLineHelper helper : helperList){
      if (!helper.getSplitRatio().isEmpty()) {
        LOGGER.verbose("start splitting orderline and get new lines");
        GCOrderLineSplitter object = new GCOrderLineSplitter(helper);
        List<YFCElement> splittedLines = object.getSplittedOrderLines();
        LOGGER.verbose("removing lines which are splitted");
        String parentLine = helper.getPrimeLineNo();
        List<String> dependentLines = helper.getDependentPrimeLines();
        orderLinesElem.removeChild(allOrderLinesMap.get(parentLine));
        for (String dependentLine : dependentLines) {
          orderLinesElem.removeChild(allOrderLinesMap.get(dependentLine));
        }

        // Fix for Defect-3021
        Double changeInQty = 0.00;
        Double OrderedQty = 0.00;
        YFCElement orderLineAfterSplit = splittedLines.get(0);
        String sOrderLineKeyAfterSplit = orderLineAfterSplit.getAttribute(GCConstants.ORDER_LINE_KEY);

        if (allOrderLineOrderedQty.containsKey(sOrderLineKeyAfterSplit)) {
          OrderedQty = allOrderLineOrderedQty.get(sOrderLineKeyAfterSplit);
          Double orderedQtyAfterSplit = orderLineAfterSplit.getDoubleAttribute("OrderedQty");
          YFCElement extnOrderLineAfterSplit = orderLineAfterSplit.getChildElement(GCConstants.EXTN);
          Double extnQtyCancelledInSplit = extnOrderLineAfterSplit.getDoubleAttribute("ExtnQtyCancelledInSplit");
          changeInQty = OrderedQty - orderedQtyAfterSplit;
          extnQtyCancelledInSplit = extnQtyCancelledInSplit + changeInQty;
          extnOrderLineAfterSplit.setAttribute("ExtnQtyCancelledInSplit", extnQtyCancelledInSplit);
        }
        // Defect-3021 Ends Here
        LOGGER.verbose("adding splitted lines");
        for (YFCElement elem : splittedLines) {
          orderLinesElem.importNode(elem);
        }
      }
    }

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Final Splitted OrderLines to return from method splitOrderLines\n" + yfcOrderDoc.toString());
    }
    LOGGER.verbose("GCProcessATGOrders.processOrderLines method :: END");
    LOGGER.endTimer("GCProcessATGOrders.processOrderLines");
    return yfcOrderDoc.getDocument();
  }

}

