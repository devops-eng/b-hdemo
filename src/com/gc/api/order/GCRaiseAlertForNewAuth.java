/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: To raise alert for the increased amount
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0               05/28/2015        Singh, Gurpreet            GCSTORE-2514: Line Level Taxes (CR-022)
 *#################################################################################################################################################################
 */
package com.gc.api.order;

import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author gurpreet.singh
 *
 */
public class GCRaiseAlertForNewAuth implements YIFCustomApi {
  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCRaiseAlertForNewAuth.class);
  public void raiseAlert(YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("GCRaiseAlertForNewAuth.raiseAlert");
    LOGGER.verbose("Class: GCRaiseAlertForNewAuth Method: raiseAlert START");

    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" raiseAlert() method, yfcInDoc is" + yfcInDoc.toString());
    }
    YFCElement eleRootInDoc = yfcInDoc.getDocumentElement();

    YFCDocument yfcCreateExceptionDoc = YFCDocument.createDocument(GCConstants.INBOX);
    YFCElement eleCreateExceptionDoc = yfcCreateExceptionDoc.getDocumentElement();
    eleCreateExceptionDoc.setAttribute(GCConstants.ENTERPRISE_KEY,
        eleRootInDoc.getAttribute(GCConstants.ENTERPRISE_CODE));
    eleCreateExceptionDoc.setAttribute(GCConstants.EXCEPTION_TYPE, "GCNewAuthRequiredAlert");
    eleCreateExceptionDoc.setAttribute(GCConstants.ORDER_NO, eleRootInDoc.getAttribute(GCConstants.ORDER_NO));
    eleCreateExceptionDoc.setAttribute(GCConstants.ORDER_HEADER_KEY,
        eleRootInDoc.getAttribute(GCConstants.ORDER_HEADER_KEY));
    eleCreateExceptionDoc.setAttribute(GCConstants.PRIORITY, GCConstants.ONE);
    eleCreateExceptionDoc.setAttribute(GCConstants.QUEUE_ID, GCConstants.GC_NEW_AUTH_ALERT_QUEUE);

    double dIncreasedTax = eleRootInDoc.getDoubleAttribute("IncreasedTax");
    String sExtnSourceOrderNo = GCXMLUtil.getAttributeFromXPath(inDoc, "Order/Extn/@ExtnSourceOrderNo");
    eleCreateExceptionDoc.setAttribute(GCConstants.DESCRIPTION, "New Auth Required For Increased Amount");
    eleCreateExceptionDoc.setAttribute(GCXmlLiterals.DETAIL_DESCRIPTION, "Source Order No is::" + sExtnSourceOrderNo
        + " Order Total Amount Increased by :" + dIncreasedTax + " Capture New Payment Type for the authorization");
    eleCreateExceptionDoc.setAttribute("AssignedToUserId", "admin");
    GCCommonUtil.invokeAPI(env, GCConstants.API_CREATE_EXCEPTION, yfcCreateExceptionDoc, null);
    LOGGER.verbose("Class: GCRaiseAlertForNewAuth Method: raiseAlert END");
    LOGGER.endTimer("GCRaiseAlertForNewAuth.raiseAlert");
  }
  @Override
  public void setProperties(Properties arg0) {

  }
}
