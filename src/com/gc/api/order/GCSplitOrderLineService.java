package com.gc.api.order;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * GCSTORE-718: Call Center Order Processing
 *
 * @author Gunjan Kumar
 *
 */
public class GCSplitOrderLineService implements YIFCustomApi {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCSplitOrderLineService.class.getName());

  /**
   * This call logic to split OrderLines on basis of Item type and multi node fulfillment
   * scenario.Method contains logic to call changeOrder API for Updating Splitted OrderLine.
   *
   * @param env
   * @param inDoc
   */
  public void splitOrderLinesService(YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("GCSplitOrderLineService.splitOrderLinesService");
    LOGGER.verbose("Class: GCSplitOrderLineService Method: splitOrderLinesService ::BEGIN");
    // Add Dependent Element if on basis of DependentOnLineKey
    Document cloneInDoc = (Document) inDoc.cloneNode(true);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("\n cloneInDoc to splitOrderLinesService\n" + YFCDocument.getDocumentFor(inDoc));
    }
    YFCNodeList<YFCElement> nlInDocOrdLine =
        YFCDocument.getDocumentFor(cloneInDoc).getDocumentElement().getChildElement(GCConstants.ORDER_LINES, true)
        .getElementsByTagName(GCConstants.ORDER_LINE);
    LOGGER.verbose("Map for OrderLineKey and primeLineNo\n");
    Map<String, String> ordLineKeyPrimeLineNoMap = new HashMap<String, String>();
    ordLineKeyPrimeLineNoMap = prepareMapForOrderLineKeyNPrimeLineNo(ordLineKeyPrimeLineNoMap, nlInDocOrdLine);
    LOGGER.verbose("Map to contain OrderLineKey and OrderLineReservation\n");
    Map<String, List<YFCElement>> orderLineReservationsMap = new HashMap<String, List<YFCElement>>();
    orderLineReservationsMap = prepareMapForOrdLineKeyNOrdLineReservations(orderLineReservationsMap, nlInDocOrdLine);
    // Check And Update cloneInDoc with Dependency to Dependent OrderLine and OrderLineReservation
    // to Bundle Parent OrderLine
    for (int i = 0; i < nlInDocOrdLine.getLength(); i++) {
      YFCElement orderLine = nlInDocOrdLine.item(i);
      LOGGER.debug("Add Dependency Element if DependentOnLineKey matches OrderLineKey of orderLineReservationsMap");
      String sDependentOnLineKey = orderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
      if (!YFCCommon.isVoid(sDependentOnLineKey) && ordLineKeyPrimeLineNoMap.containsKey(sDependentOnLineKey)) {
        YFCElement eDependency = orderLine.createChild(GCConstants.DEPENDENCY);
        LOGGER.debug("Add DependentOnPrimeLineNo as Parent PrimeLineNo from orderLineReservationsMap");
        eDependency
        .setAttribute(GCConstants.DEPENDENT_PRIME_LINE_NO, ordLineKeyPrimeLineNoMap.get(sDependentOnLineKey));
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("OrderLine which has Dependency :\n" + orderLine.toString());
        }
      }
      LOGGER.debug("Add Component OrderLine Reservations to Parent Bundle OrderLine");
      String ordLineKey = orderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      if (orderLineReservationsMap.containsKey(ordLineKey)) {
        // <OrderLineReservations/> will come for parent Bundle From COM
        YFCElement bundleOrdLineResvs = orderLine.getChildElement(GCConstants.ORDER_LINE_RESERVATIONS);
        if (!YFCCommon.isVoid(bundleOrdLineResvs)) {
          List<YFCElement> ordLineResvListFromMap = orderLineReservationsMap.get(ordLineKey);
          for (YFCElement olrEle : ordLineResvListFromMap) {
            bundleOrdLineResvs.importNode(olrEle);
          }
          if (LOGGER.isVerboseEnabled()) {
            LOGGER
            .verbose(" Importing all OrderLine Reservations to Bundle Parent OrderLine :: ordLineReservations::\n"
                + bundleOrdLineResvs.toString());
          }
        }
      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("\n cloneInDoc After Checking and updating Dependency and Reservations"
          + YFCDocument.getDocumentFor(cloneInDoc).toString());
    }
    LOGGER.verbose("Call splitOrderLines method of GCProcessATGOrders for OrderLine Splitting if required");
    GCProcessATGOrders gcProcessATGOrdObj = new GCProcessATGOrders();
    Document splitOrderLinesOp = gcProcessATGOrdObj.splitOrderLines(env, cloneInDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("\n splitted OrderLinesOp" + YFCDocument.getDocumentFor(splitOrderLinesOp).toString());
    }
    int ordLineListSizeBeforeSplit =
        YFCDocument.getDocumentFor(inDoc).getDocumentElement().getChildElement(GCConstants.ORDER_LINES, true)
        .getElementsByTagName(GCConstants.ORDER_LINE).getLength();
    int ordLineListSizeAfterSplit =
        YFCDocument.getDocumentFor(splitOrderLinesOp).getDocumentElement().getChildElement(GCConstants.ORDER_LINES, true)
        .getElementsByTagName(GCConstants.ORDER_LINE).getLength();
    LOGGER.verbose("ordLineListSizeBeforeSplit : " + ordLineListSizeBeforeSplit);
    LOGGER.verbose("ordLineListSizeAfterSplit : " + ordLineListSizeAfterSplit);

    if (ordLineListSizeBeforeSplit != ordLineListSizeAfterSplit && !YFCCommon.equalsIgnoreCase(inDoc.getDocumentElement().getAttribute("DoNotCallChangeOrder"), GCConstants.YES)) {
      LOGGER
      .verbose("Splitt happens in any of the OrderLine if ordLineListSizeBeforeSplit is not equal to ordLineListSizeAfterSplit");
      callChangeOrderApi(env, splitOrderLinesOp);
    }
    LOGGER.verbose("GCSplitOrderLineService.splitOrderLinesService method :: END");
    LOGGER.endTimer("GCSplitOrderLineService.splitOrderLinesService");
  }


  /**
   * This Method Prepare Map for OrderLineKey And PrimeLineNo.
   *
   * @param ordLineKeyPrimeLineNoMap
   * @param nlInDocOrdLine
   * @return
   */
  private Map<String, String> prepareMapForOrderLineKeyNPrimeLineNo(Map<String, String> ordLineKeyPrimeLineNoMap,
      YFCNodeList<YFCElement> nlInDocOrdLine) {
    for (YFCElement inDocOrdLine : nlInDocOrdLine) {
      ordLineKeyPrimeLineNoMap.put(inDocOrdLine.getAttribute(GCConstants.ORDER_LINE_KEY),
          inDocOrdLine.getAttribute(GCConstants.PRIME_LINE_NO));
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("\n ordLineKeyPrimeLineNoMap" + ordLineKeyPrimeLineNoMap.toString());
    }
    return ordLineKeyPrimeLineNoMap;
  }


  /**
   * This method prepare Map for OrderLineKey and OrderLineReservation Element.
   *
   * @param orderLineReservationsMap
   * @param nlInDocOrdLine
   * @return orderLineReservationsMap
   */
  private Map<String, List<YFCElement>> prepareMapForOrdLineKeyNOrdLineReservations(
      Map<String, List<YFCElement>> orderLineReservationsMap, YFCNodeList<YFCElement> nlInDocOrdLine) {
    LOGGER.beginTimer("GCSplitOrderLineService.prepareMapForOrdLineKeyNOrdLineReservations");
    LOGGER.verbose("Class: GCSplitOrderLineService Method: prepareMapForOrdLineKeyNOrdLineReservations ::BEGIN");
    for (YFCElement inDocOrdLine : nlInDocOrdLine) {
      YFCElement cloneOrderLine = (YFCElement) inDocOrdLine.cloneNode(true);
      LOGGER.verbose("Check Bundle Parent Line to get Parent OrderLineKey\n");
      YFCElement bundleParentLine = cloneOrderLine.getChildElement(GCConstants.BUNDLE_PARENT_LINE);
      if (!YFCCommon.isVoid(bundleParentLine)) {
        LOGGER.verbose("get Parent OrderLineKey\n");
        String orderLineKey = bundleParentLine.getAttribute(GCConstants.ORDER_LINE_KEY);
        YFCElement ordLineReservations = cloneOrderLine.getChildElement(GCConstants.ORDER_LINE_RESERVATIONS);
        if (ordLineReservations.hasChildNodes()) {
          LOGGER.verbose("Store OrderLine Reservation in list\n");
          List<YFCElement> ordLineResvList = new ArrayList<YFCElement>();
          YFCNodeList<YFCElement> nlOrderLineResv =
              ordLineReservations.getElementsByTagName(GCConstants.ORDER_LINE_RESERVATION);
          for (YFCElement orderLineResv : nlOrderLineResv) {
            ordLineResvList.add(orderLineResv);
          }
          LOGGER.verbose("Store all OrderLine Reservation as list value in map for OrderLine Key\n");
          if (!orderLineReservationsMap.containsKey(orderLineKey)) {
            LOGGER.verbose("Add OrderLine Reservations List in map if Map OrderLineKey is not in Map");
            orderLineReservationsMap.put(orderLineKey, ordLineResvList);
          } else {
            LOGGER.verbose("OrderLineKey is in Map.Get OrderLine List from Map and add new Reservations to it");
            List<YFCElement> ordLineResvListFromMap = orderLineReservationsMap.get(orderLineKey);
            ordLineResvListFromMap.addAll(ordLineResvList);
            if (LOGGER.isVerboseEnabled()) {
              LOGGER.debug("ordLineResvListFromMap::\n" + ordLineResvListFromMap.toString());
            }
            orderLineReservationsMap.put(orderLineKey, ordLineResvListFromMap);
          }
        }
      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("\n orderLineReservationsMap" + orderLineReservationsMap.toString());
    }
    LOGGER.verbose("GCSplitOrderLineService.prepareMapForOrdLineKeyNOrdLineReservations method :: END");
    LOGGER.endTimer("GCSplitOrderLineService.prepareMapForOrdLineKeyNOrdLineReservations");
    return orderLineReservationsMap;
  }


  /**
   * This Method calls changeOrder API for OrderLines Which are Splitted and first Bundle Component
   * OrderLine
   *
   * @param env
   * @param splitOrderLinesOp
   */
  private void callChangeOrderApi(YFSEnvironment env, Document splitOrderLinesOp) {
    LOGGER.beginTimer("GCSplitOrderLineService.callChangeOrderApi");
    LOGGER.verbose("Class: GCSplitOrderLineService Method: callChangeOrderApi ::BEGIN");
    YFCDocument changeOrdIp = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement orderEleForChangeOrder = changeOrdIp.getDocumentElement();
    orderEleForChangeOrder.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
    orderEleForChangeOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, YFCDocument.getDocumentFor(splitOrderLinesOp)
        .getDocumentElement().getAttribute(GCConstants.ORDER_HEADER_KEY));
    orderEleForChangeOrder.setAttribute(GCConstants.ENTERPRISE_CODE, YFCDocument.getDocumentFor(splitOrderLinesOp)
        .getDocumentElement().getAttribute(GCConstants.ENTERPRISE_CODE));

    YFCElement ordLinesForChangeOrder = orderEleForChangeOrder.createChild(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> nlOrdLineAfterSplitCall =
        YFCDocument.getDocumentFor(splitOrderLinesOp).getDocumentElement()
        .getChildElement(GCConstants.ORDER_LINES, true).getElementsByTagName(GCConstants.ORDER_LINE);
    LOGGER.verbose("find OrderLine which are splitted");
    for (YFCElement eOrderLine : nlOrdLineAfterSplitCall) {
      if (YFCCommon.isVoid(eOrderLine.getAttribute(GCConstants.PRIME_LINE_NO))) {
        LOGGER.verbose("OrderLine with PrimeLineNo as Blank is splitted OrderLine");
        YFCElement splittedOrdLineClone = (YFCElement) eOrderLine.cloneNode(true);
        YFCElement bundleParentLine = splittedOrdLineClone.getChildElement(GCConstants.BUNDLE_PARENT_LINE);
        LOGGER.verbose("OrderLine with BundleParentLine Element are component OrderLine of Parent Bundle");
        if (YFCCommon.equals(GCConstants.CREATE, splittedOrdLineClone.getAttribute(GCConstants.ACTION))
            && !YFCCommon.isVoid(bundleParentLine)) {
          LOGGER.verbose("first splitted Bundle Component OrderLine is included in ChangeOrder API call");
          continue;
        }
        removeAttNElementsForChangeOrderCall(splittedOrdLineClone);
        ordLinesForChangeOrder.importNode(splittedOrdLineClone);
      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("\n changeOrdIp" + changeOrdIp.toString());
    }
    env.setTxnObject("Publish", "N");
    env.setTxnObject("DoNotSendCancellationEMail", "Y");
    Document docChangeOrderOP = GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, changeOrdIp.getDocument());
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("\n docChangeOrderOP output Doc" + YFCDocument.getDocumentFor(docChangeOrderOP).toString());
    }
    LOGGER.verbose("GCSplitOrderLineService.callChangeOrderApi method :: END");
    LOGGER.endTimer("GCSplitOrderLineService.callChangeOrderApi");
  }


  /**
   * This method removes attributes and Elements from OrderLine for ChangeOrder API Call.
   *
   * @param cloneOrderLine
   * @param cloneItem
   */
  private void removeAttNElementsForChangeOrderCall(YFCElement cloneOrderLine) {
    LOGGER.beginTimer("GCSplitOrderLineService.removeAttNElementsForChangeOrderCall");
    LOGGER.verbose("Class: GCSplitOrderLineService Method: removeAttNElementsForChangeOrderCall ::BEGIN");
    cloneOrderLine.removeAttribute(GCXmlLiterals.ALLOCATION_DATE);
    cloneOrderLine.removeAttribute(GCXmlLiterals.REMAINING_QTY);
    cloneOrderLine.removeAttribute(GCConstants.STATUS);
    cloneOrderLine.removeAttribute(GCXmlLiterals.MAX_LINE_STATUS);
    cloneOrderLine.removeAttribute(GCXmlLiterals.MAX_LINE_STATUS_DESC);
    cloneOrderLine.removeAttribute(GCXmlLiterals.MIN_LINE_STATUS);
    cloneOrderLine.removeAttribute(GCXmlLiterals.MIN_LINE_STATUS_DESC);
    cloneOrderLine.removeAttribute(GCXmlLiterals.OPEN_QTY);
    cloneOrderLine.removeAttribute(GCXmlLiterals.OTHER_CHARGES);
    cloneOrderLine.removeAttribute(GCXmlLiterals.ORG_ORD_QTY);
    cloneOrderLine.removeAttribute(GCConstants.STATUS_QUANTITY);
    cloneOrderLine.removeAttribute(GCXmlLiterals.INVOICE_QTY);
    cloneOrderLine.removeAttribute(GCXmlLiterals.LINE_SEQ_NO);
    // Handling Bundle Items
    YFCElement cloneItem = cloneOrderLine.getChildElement(GCConstants.ITEM);
    if (!YFCCommon.isVoid(cloneItem)
        && YFCCommon.equals(GCConstants.BUNDLE, cloneOrderLine.getAttribute(GCConstants.KIT_CODE))
        && !YFCCommon.isVoid(GCXmlLiterals.BUNDLE_FULLFILLMENT_MODE)) {
      cloneItem.removeAttribute(GCXmlLiterals.BUNDLE_FULLFILLMENT_MODE);
    }
    YFCElement cloneLineTaxes = cloneOrderLine.getChildElement(GCConstants.LINE_TAXES);
    if (!YFCCommon.isVoid(cloneLineTaxes)) {
      YFCElement sTaxSummary = cloneLineTaxes.getChildElement(GCXmlLiterals.TAX_SUMMARY);
      if (!YFCCommon.isVoid(sTaxSummary)) {
        cloneLineTaxes.removeChild(sTaxSummary);
      }
      YFCNodeList<YFCElement> nlLineTax = cloneLineTaxes.getElementsByTagName(GCConstants.LINE_TAX);
      for (int i = 0; i < nlLineTax.getLength(); i++) {
        YFCElement lineTax = nlLineTax.item(i);
        lineTax.removeAttribute(GCXmlLiterals.INVOICE_TAX);
        lineTax.removeAttribute(GCXmlLiterals.REF_1);
        lineTax.removeAttribute(GCXmlLiterals.REF_2);
        lineTax.removeAttribute(GCXmlLiterals.REF_3);
        lineTax.removeAttribute(GCXmlLiterals.REMAINING_TAX);
        // GCSTORE-3786::Begin
        // lineTax.removeAttribute(GCXmlLiterals.TAX_PERCENTAGE);
        // GCSTORE-3786::End
      }
    }
    YFCElement cloneLineCharges = cloneOrderLine.getChildElement(GCConstants.LINE_CHARGES);
    if (!YFCCommon.isVoid(cloneLineCharges)) {
      YFCNodeList<YFCElement> nlLineCharge = cloneLineCharges.getElementsByTagName(GCConstants.LINE_CHARGE);
      for (int j = 0; j < nlLineCharge.getLength(); j++) {
        YFCElement lineCharge = nlLineCharge.item(j);
        lineCharge.removeAttribute(GCConstants.CHARGE_AMOUNT);
        lineCharge.removeAttribute(GCXmlLiterals.INVOICE_CHRGE_AMOUNT);
        lineCharge.removeAttribute(GCXmlLiterals.INVOICE_CHRGE_PER_LINE);
        lineCharge.removeAttribute(GCXmlLiterals.INVOICE_CHRGE_PER_UNIT);
        lineCharge.removeAttribute(GCXmlLiterals.REMAINING_CHARGE_AMOUNT);
        lineCharge.removeAttribute(GCXmlLiterals.REMAINING_CHARGE_PER_LINE);
        lineCharge.removeAttribute(GCXmlLiterals.REMAINING_CHARGE_PER_UNIT);
        lineCharge.removeAttribute(GCConstants.REFERENCE);
      }
    }
    YFCElement clonePersonInfoShipTo = cloneOrderLine.getChildElement(GCConstants.PERSON_INFO_SHIP_TO);
    if (!YFCCommon.isVoid(clonePersonInfoShipTo)) {
      cloneOrderLine.removeChild(clonePersonInfoShipTo);
    }
    YFCElement linePriceInfo = cloneOrderLine.getChildElement(GCConstants.LINE_PRICE_INFO);
    if (!YFCCommon.isVoid(linePriceInfo)) {
      linePriceInfo.removeAttribute(GCConstants.LINE_TOTAL);
      linePriceInfo.removeAttribute(GCConstants.LINE_TOTAL_WITHOUT_TAX);
      linePriceInfo.setAttribute(GCConstants.IS_PRICE_LOCKED, GCConstants.YES);
    }
    YFCElement cloneOrderlineSchedules = cloneOrderLine.getChildElement(GCConstants.SCHEDULES);
    if (!YFCCommon.isVoid(cloneOrderlineSchedules)) {
      cloneOrderLine.removeChild(cloneOrderlineSchedules);
    }
    YFCElement cloneOrderlineStatuses = cloneOrderLine.getChildElement(GCConstants.ORDER_STATUSES);
    if (!YFCCommon.isVoid(cloneOrderlineStatuses)) {
      cloneOrderLine.removeChild(cloneOrderlineStatuses);
    }
    YFCElement cloneParentOrdLineRelationships =
        cloneOrderLine.getChildElement(GCXmlLiterals.PARENT_ORD_LINE_RELATIONSHIP);
    if (!YFCCommon.isVoid(cloneParentOrdLineRelationships)) {
      cloneOrderLine.removeChild(cloneParentOrdLineRelationships);
    }
    YFCElement cloneLineOverallTotals = cloneOrderLine.getChildElement(GCXmlLiterals.LINE_OVERALL_TOTALS);
    if (!YFCCommon.isVoid(cloneLineOverallTotals)) {
      cloneOrderLine.removeChild(cloneLineOverallTotals);
    }
    YFCElement cloneLineRemainingTotals = cloneOrderLine.getChildElement(GCXmlLiterals.LINE_REMAINING_TOTALS);
    if (!YFCCommon.isVoid(cloneLineRemainingTotals)) {
      cloneOrderLine.removeChild(cloneLineRemainingTotals);
    }
    YFCElement cloneLineInvoicedTotals = cloneOrderLine.getChildElement(GCXmlLiterals.LINE_INVOICE_TOTALS);
    if (!YFCCommon.isVoid(cloneLineInvoicedTotals)) {
      cloneOrderLine.removeChild(cloneLineInvoicedTotals);
    }
    YFCElement cloneAdditionalAddresses = cloneOrderLine.getChildElement(GCXmlLiterals.ADDITIONAL_ADDRESSES);
    if (!YFCCommon.isVoid(cloneAdditionalAddresses)) {
      cloneOrderLine.removeChild(cloneAdditionalAddresses);
    }
    YFCElement cloneKitLines = cloneOrderLine.getChildElement(GCXmlLiterals.KIT_LINES);
    if (!YFCCommon.isVoid(cloneKitLines)) {
      cloneOrderLine.removeChild(cloneKitLines);
    }
    YFCElement cloneReferences = cloneOrderLine.getChildElement(GCConstants.REFERENCES);
    if (!YFCCommon.isVoid(cloneReferences)) {
      cloneOrderLine.removeChild(cloneReferences);
    }
    YFCElement cloneInstructions = cloneOrderLine.getChildElement(GCXmlLiterals.INSTRUCTIONS);
    if (!YFCCommon.isVoid(cloneInstructions)) {
      cloneOrderLine.removeChild(cloneInstructions);
    }
    YFCElement cloneOrderLineReservations = cloneOrderLine.getChildElement(GCXmlLiterals.ORD_LINE_RESERVATIONS);
    if (!YFCCommon.isVoid(cloneOrderLineReservations) && cloneOrderLineReservations.hasChildNodes()) {
      cloneOrderLineReservations.setAttribute(GCConstants.RESET, GCConstants.YES);
    }
    LOGGER.verbose("GCSplitOrderLineService.removeAttFromOrderLineAndItemEle method :: END");
    LOGGER.endTimer("GCSplitOrderLineService.removeAttFromOrderLineAndItemEle");
  }

  @Override
  public void setProperties(Properties arg0) {
  }

}
