/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: To raise alert for the increased amount
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0               05/28/2015        Singh, Gurpreet            GCSTORE-2514: Line Level Taxes (CR-022)
 *#################################################################################################################################################################
 */
package com.gc.api.order;

import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author yashu.mittal
 *
 */
public class GCStampChargesTaxesOnOrderLineComponents implements YIFCustomApi {
	// initialize logger
	private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCStampChargesTaxesOnOrderLineComponents.class);
	public void stampChargesTaxesOnOrderLineComponents(YFSEnvironment env, Document inDoc) {

		LOGGER.beginTimer("GCStampChargesTaxesOnOrderLineComponents.stampChargesTaxesOnOrderLineComponents()");
	    LOGGER.verbose("GCStampChargesTaxesOnOrderLineComponents - stampChargesTaxesOnOrderLineComponents() : Start");
	    
		boolean bAnyBundleLineExists = true;
		List<Element> listBundleParentLineDetail =
				GCXMLUtil.getElementListByXpath(inDoc,
						"Order/OrderLines/OrderLine[@KitCode='BUNDLE']");

		if (listBundleParentLineDetail.isEmpty()) {
			bAnyBundleLineExists = false;
		}

		if(bAnyBundleLineExists){
			LOGGER.verbose("BundleLine Exists on the order.");
			Iterator<Element> iteratorListBundleLineDetail = listBundleParentLineDetail.iterator();
			YFCDocument changeOrderDoc = YFCDocument.createDocument("Order");
			YFCElement eleChangeOrderRoot = changeOrderDoc.getDocumentElement();
			YFCElement eleChangeOrderOrderLines = eleChangeOrderRoot.createChild(GCConstants.ORDER_LINES);
			YFCDocument yfcDocument = YFCDocument.getDocumentFor(inDoc);
			YFCElement eleRoot = yfcDocument.getDocumentElement();
			String sOrderHeaderKey = eleRoot.getAttribute(GCConstants.ORDER_HEADER_KEY);
			eleChangeOrderRoot.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
			eleChangeOrderRoot.setAttribute(GCConstants.SELECT_METHOD, GCConstants.WAIT);
			
			while (iteratorListBundleLineDetail.hasNext()) {
				Element eleBundleParentLineDetail = iteratorListBundleLineDetail.next();
				String sOrderLineKey = eleBundleParentLineDetail.getAttribute(GCConstants.ORDER_LINE_KEY);
				LOGGER.verbose("BundleParentOrderLineKey : " + sOrderLineKey);
				List<Element> listChildLineDetail =
						GCXMLUtil.getElementListByXpath(inDoc,
								"Order/OrderLines/OrderLine[@BundleParentOrderLineKey='"
										+ sOrderLineKey + "']");

				for (Element eleOrderLine : listChildLineDetail) {
					String sCompOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
					Element eleItem = (Element) eleOrderLine.getElementsByTagName(GCConstants.ITEM).item(0);
                    String productClass = eleItem.getAttribute(GCConstants.PRODUCT_CLASS);
                    String shipNode = eleOrderLine.getAttribute(GCConstants.SHIP_NODE);
					YFCElement eleChangeOrderOrderLine = eleChangeOrderOrderLines.createChild(GCConstants.ORDER_LINE);
					eleChangeOrderOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, sCompOrderLineKey);
                    if (!YFCCommon.isVoid(shipNode) && YFCUtils.equals(GCConstants.REGULAR, productClass)) {
                       eleChangeOrderOrderLine.setAttribute(GCConstants.SHIP_NODE, GCConstants.BLANK);
                    }
					YFCElement eleExtn = eleChangeOrderOrderLine.createChild(GCConstants.EXTN);
					YFCElement eleGCOrderLineComponentsList = eleExtn.createChild(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS_LIST);
					Element eleLinePriceInfo = (Element) eleOrderLine.getElementsByTagName(GCConstants.LINE_PRICE_INFO).item(0);
					String sUnitPrice = eleLinePriceInfo.getAttribute(GCConstants.UNIT_PRICE);

					String sItemID = ((Element) eleOrderLine.getElementsByTagName(GCConstants.ITEM).item(0)).getAttribute(GCConstants.ITEM_ID);
					String sQuantity = eleOrderLine.getAttribute(GCConstants.ORDERED_QTY);

					YFCElement eGCOrderLineComp = eleGCOrderLineComponentsList.createChild(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
					eGCOrderLineComp.setAttribute(GCConstants.CHARGE_CATEGORY, GCConstants.UNIT_PRICE);
					eGCOrderLineComp.setAttribute(GCConstants.CHARGE_NAME, GCConstants.UNIT_PRICE);
					eGCOrderLineComp.setAttribute(GCXmlLiterals.AMOUNT, sUnitPrice);
					eGCOrderLineComp.setAttribute(GCConstants.ITEM_ID, sItemID);
					eGCOrderLineComp.setAttribute(GCConstants.QUANTITY, sQuantity);

					NodeList nlLineCharge = eleOrderLine.getElementsByTagName(GCConstants.LINE_CHARGE);
					for (int j = 0; j < nlLineCharge.getLength(); j++) {
						Element eleLineCharge = (Element) nlLineCharge.item(j);
						String sChargeCategory = eleLineCharge.getAttribute(GCConstants.CHARGE_CATEGORY);
						String sChargeName = eleLineCharge.getAttribute(GCConstants.CHARGE_NAME);
						String sIsDiscount = eleLineCharge.getAttribute(GCXmlLiterals.IS_DISCOUNT);
						String sChargeAmount = eleLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT);
						if(YFCCommon.isVoid(sChargeAmount)){
							sChargeAmount = eleLineCharge.getAttribute(GCConstants.CHARGE_PER_LINE);
						}
						if(YFCCommon.isVoid(sChargeAmount)){
							String sChargePerUnit = eleLineCharge.getAttribute(GCConstants.CHARGE_PER_UNIT);
							if(!YFCCommon.isVoid(sChargePerUnit)){
								Double dChargeAmount = Double.parseDouble(sChargePerUnit)*Double.parseDouble(sQuantity);
								sChargeAmount = dChargeAmount.toString();
							} else{
								sChargeAmount = "0.00";
							}
							
						}
						
						YFCElement yfcEleGCOrderLineComponent = eleGCOrderLineComponentsList.createChild(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
						yfcEleGCOrderLineComponent.setAttribute(GCConstants.CHARGE_CATEGORY, sChargeCategory);
						yfcEleGCOrderLineComponent.setAttribute(GCConstants.CHARGE_NAME, sChargeName);
						yfcEleGCOrderLineComponent.setAttribute(GCXmlLiterals.IS_DISCOUNT, sIsDiscount);
						yfcEleGCOrderLineComponent.setAttribute(GCXmlLiterals.AMOUNT, sChargeAmount);
						yfcEleGCOrderLineComponent.setAttribute(GCConstants.ITEM_ID, sItemID);
						yfcEleGCOrderLineComponent.setAttribute(GCConstants.QUANTITY, sQuantity);
					}

					// Loop over line Taxes
					NodeList nlLineTax = eleOrderLine.getElementsByTagName(GCConstants.LINE_TAX);
					for (int k = 0; k < nlLineTax.getLength(); k++) {
						Element eleLineTax = (Element) nlLineTax.item(k);
						String sChargeCategory = eleLineTax.getAttribute(GCConstants.CHARGE_CATEGORY);
						String sChargeName = eleLineTax.getAttribute(GCConstants.TAX_NAME);
						String sIsDiscount = GCConstants.BLANK;
						String sChargeAmount = eleLineTax.getAttribute(GCConstants.TAX);
						if(YFCCommon.isVoid(sChargeAmount)){
							sChargeAmount ="0.00";
						}
						if (YFCCommon.equals(GCConstants.SHIPPING_PROMOTION, sChargeCategory)
								|| YFCCommon.equals(GCConstants.PROMOTION, sChargeCategory)) {
							sIsDiscount = GCConstants.YES;
						}
						YFCElement yfcEleGCOrderLineComponent = eleGCOrderLineComponentsList.createChild(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
						yfcEleGCOrderLineComponent.setAttribute(GCConstants.CHARGE_CATEGORY, sChargeCategory);
						yfcEleGCOrderLineComponent.setAttribute(GCConstants.CHARGE_NAME, sChargeName);
						yfcEleGCOrderLineComponent.setAttribute(GCXmlLiterals.IS_DISCOUNT, sIsDiscount);
						yfcEleGCOrderLineComponent.setAttribute(GCXmlLiterals.AMOUNT, sChargeAmount);
						yfcEleGCOrderLineComponent.setAttribute(GCConstants.ITEM_ID, sItemID);
						yfcEleGCOrderLineComponent.setAttribute(GCConstants.QUANTITY, sQuantity);
					}
				}
			}
			 if (LOGGER.isVerboseEnabled()) {
			      LOGGER.verbose("ChangeOrderInput to stamp orderline components : " + changeOrderDoc.toString());
			    }
			GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, changeOrderDoc.getDocument());
		}
		
		 LOGGER.endTimer("GCStampChargesTaxesOnOrderLineComponents.stampChargesTaxesOnOrderLineComponents()");
		 LOGGER.verbose("GCStampChargesTaxesOnOrderLineComponents - stampChargesTaxesOnOrderLineComponents() : End");
	}

	@Override
	public void setProperties(Properties paramProperties) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
