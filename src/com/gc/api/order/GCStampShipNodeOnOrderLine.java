/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: This class is used to resolve the holds on the warranty lines when their parent line is released.
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0             15/05/2015          Saxena, Ekansh      This class stamps ShipNode on order line for Pickup In Store Orders from orderLineReservation.
             2.0			 08/06/2017			  Expicient			 GCSTORE-4380: Rounding for sets can cause tax and payment discrepancy
 #################################################################################################################################################################
 */
package com.gc.api.order;
import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class stamps ShipNode on order line for Pickup In Store Orders from
 * orderLineReservation.
 * 
 * @author Ekansh Saxena
 *
 */

public class GCStampShipNodeOnOrderLine {


	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCStampShipNodeOnOrderLine.class.getName());

	/**
	 * This method stamps ShipNode on order line for Pickup In Store Orders from
	 * orderLineReservation.
	 * 
	 * @param env
	 * @param inDoc
	 */

	public Document stampShipNodeForPickupStoreOrder(YFSEnvironment env,
			Document inDoc) {

		LOGGER.beginTimer("GCStampShipNodeOnOrderLine.stampShipNodeForPickupStoreOrder");
		LOGGER.verbose("Class: GCStampShipNodeOnOrderLine Method: stampShipNodeForPickupStoreOrder ::BEGIN");

		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("\n Input Doc is\n"
					+ YFCDocument.getDocumentFor(inDoc));
		}

		YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
		YFCElement eleRoot = inputDoc.getDocumentElement();
		String sOrderHeaderKey = eleRoot
				.getAttribute(GCConstants.ORDER_HEADER_KEY);
		YFCDocument getOrderListIp = YFCDocument
				.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey
						+ "'/>");
		YFCDocument getOrderListTemp = YFCDocument
				.getDocumentFor("<OrderList><Order OrderHeaderKey='' OrderNo='' SourcingClassification=''>"
						+ "<Extn ExtnPickupStoreID='' ExtnOrderCaptureChannel=''/>"
						+ "<OrderLines><OrderLine FulfillmentType='' OrderLineKey='' ShipNode='' BundleParentOrderLineKey=''><ItemDetails ><PrimaryInformation PrimarySupplier=''/></ItemDetails><Item ProductClass='' ProductLine=''/><BundleParentLine/>"
						+ "<OrderLineReservations><OrderLineReservation ItemID='' Node='' ProductClass='' UnitOfMeasure=''/>"
						+ "</OrderLineReservations></OrderLine></OrderLines><PersonInfoShipTo/></Order></OrderList>");
		YFCDocument getOrderListOp = GCCommonUtil.invokeAPI(env,
				GCConstants.API_GET_ORDER_LIST, getOrderListIp,
				getOrderListTemp);

		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("\n GetOrderList output\n"
					+ getOrderListOp.toString());
		}

		String sExtnPickupStoreID = "";

		YFCElement eleOrderList = getOrderListOp.getDocumentElement();
		YFCElement eleOrder = eleOrderList.getChildElement(GCConstants.ORDER);
		String sSourcingClassification = eleOrder
				.getAttribute("SourcingClassification");
		YFCElement eleExtn = eleOrder.getChildElement(GCConstants.EXTN);
		if (!YFCCommon.isVoid(eleExtn)) {
			sExtnPickupStoreID = eleExtn
					.getAttribute(GCConstants.EXTN_PICKUP_STORE_ID);
			LOGGER.verbose("ExtnPickupStoreID is" + sExtnPickupStoreID);
		}

		YFCDocument docChangeOrder = YFCDocument
				.createDocument(GCConstants.ORDER);
		YFCElement eleChangeOrder = docChangeOrder.getDocumentElement();
		eleChangeOrder.setAttribute(GCConstants.ORDER_HEADER_KEY,
				sOrderHeaderKey);
		eleChangeOrder.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
		YFCElement eleChangeOrderLines = eleChangeOrder
				.createChild(GCConstants.ORDER_LINES);

		YFCNodeList<YFCElement> nlOrderLine = eleOrder.getChildElement(
				GCConstants.ORDER_LINES, true).getElementsByTagName(
						GCConstants.ORDER_LINE);
		if (YFCCommon.isStringVoid(sExtnPickupStoreID)) {
			//Begin Jira-5128 - update the shipNode of software item's orderLine with primary supplier.
			boolean chngOrdCallToUpdateSoftwareLine = copyPrimySuplrToShipNodeForSoftwareLN(nlOrderLine, docChangeOrder);
			if(chngOrdCallToUpdateSoftwareLine){
				GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER,
						docChangeOrder.getDocument());
			}
			//End Jira-5128
			return inDoc;
		}

		boolean isChangeOrderReqd = false;
		String sFulfillmentType = "";
		String productClass = "" ;

		for (YFCElement eleOrderLine : nlOrderLine) {
			sFulfillmentType = eleOrderLine
					.getAttribute(GCConstants.FULFILLMENT_TYPE);
			String sShipNode = eleOrderLine.getAttribute(GCConstants.SHIP_NODE);
			YFCElement eleItem = eleOrderLine.getChildElement(GCConstants.ITEM);
			if(!YFCCommon.isVoid(eleItem)){
				productClass = eleItem.getAttribute(GCConstants.PRODUCT_CLASS);
			}
			YFCElement eleBundleParentLine = eleOrderLine.getChildElement(GCConstants.BUNDLE_PARENT_LINE);
			if (sFulfillmentType.indexOf(GCConstants.PICKUP_IN_STORE) >= 0) {
				if (YFCCommon.isStringVoid(sShipNode)) {
					YFCElement eleOrderLineReservations = eleOrderLine
							.getChildElement(GCConstants.ORDER_LINE_RESERVATIONS);
					if (!YFCCommon.isVoid(eleOrderLineReservations)
							&& (!(!YFCCommon.isVoid(eleBundleParentLine) && YFCUtils.equals(GCConstants.REGULAR, productClass)))) {
						YFCNodeList<YFCElement> nlOrderLineReservation = eleOrderLineReservations
								.getElementsByTagName(GCConstants.ORDER_LINE_RESERVATION);
						int nlLength = nlOrderLineReservation.getLength();
						for (int i = 0; i < nlLength; i++) {

							YFCElement eleOrderLineReservation = nlOrderLineReservation
									.item(i);
							String sNode = eleOrderLineReservation
									.getAttribute(GCConstants.NODE);
							if (YFCCommon.equals(sSourcingClassification,
									GCConstants.PREPAID_SOURCING)
									|| YFCCommon.equals(
											sSourcingClassification,
											GCConstants.DC_SOURCED_SOURCING)) {
								boolean	bIsNodeAStore=isNodeAStore(env,sNode);
								if(bIsNodeAStore){
									if (LOGGER.isVerboseEnabled()) {
										LOGGER.verbose("\n Node Type is Store\n");
									}
									eleOrderLineReservation.getParentElement().removeChild(eleOrderLineReservation);
									nlLength--;
									i--;
								}
							}
							if (YFCCommon.equalsIgnoreCase(sExtnPickupStoreID,
									sNode)) {
								YFCElement eleChangeOrderLine = eleChangeOrderLines
										.createChild(GCConstants.ORDER_LINE);
								eleChangeOrderLine
								.setAttribute(
										GCConstants.ORDER_LINE_KEY,
										eleOrderLine
										.getAttribute(GCConstants.ORDER_LINE_KEY));
								eleChangeOrderLine.setAttribute(
										GCConstants.SHIP_NODE, sNode);
								eleChangeOrderLine.setAttribute(
										GCConstants.IS_FIRM_PREDEFINED_NODE,
										GCConstants.YES);
								isChangeOrderReqd = true;
								break;
							}
						}
					}
				}
			}
		}

		boolean isUpdateShipToInfo = false;

		if (sFulfillmentType.indexOf(GCConstants.PICKUP_IN_STORE) >= 0) {
			isUpdateShipToInfo = updateShipToInfoForPIS(env, getOrderListOp,
					docChangeOrder);
			LOGGER.verbose("isUpdateShipToInfo Flag " + isUpdateShipToInfo);
		}

		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("\n Change Order Input\n"
					+ docChangeOrder.toString());
		}
		//Begin Jira-5128 - update the shipNode of software item's orderLine with primary supplier.
		boolean chngOrdCallToUpdateSoftwareLine = copyPrimySuplrToShipNodeForSoftwareLN(nlOrderLine, docChangeOrder);
		//End Jira-5128

		// If Change Order needed
		if (isChangeOrderReqd || isUpdateShipToInfo || chngOrdCallToUpdateSoftwareLine) {
			GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER,
					docChangeOrder.getDocument());
		}
		return inDoc;
	}

	// Fix For GCStore-3174
	/**
	 * This method will update the company attribute of PersonInfoShipTo
	 * Element.
	 * @param env 
	 * 
	 * @param getOrderListOp
	 * @param docChangeOrder
	 * @return
	 */

	private boolean updateShipToInfoForPIS(YFSEnvironment env, YFCDocument getOrderListOp,
			YFCDocument docChangeOrder) {

		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("\n GetOrderList in updateShipToInfoForPIS Input\n"
					+ getOrderListOp.toString());
		}

		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("\n docChangeOrder in updateShipToInfoForPIS Input\n"
					+ getOrderListOp.toString());
		}

		YFCElement eleOrderList = getOrderListOp.getDocumentElement();
		YFCElement eleOrder = eleOrderList.getChildElement(GCConstants.ORDER);
		YFCElement eleExtn = eleOrder.getChildElement(GCConstants.EXTN);
		String sExtnOrderCaptureChannel = "";
		String sExtnPickupStoreID = "";
		if (!YFCCommon.isVoid(eleExtn)) {
			sExtnPickupStoreID = eleExtn
					.getAttribute(GCConstants.EXTN_PICKUP_STORE_ID);
			LOGGER.verbose("ExtnPickupStoreID is" + sExtnPickupStoreID);
			sExtnOrderCaptureChannel = eleExtn
					.getAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL);
			LOGGER.verbose("ExtnOrderCaptureChannel is"
					+ sExtnOrderCaptureChannel);
		}

		if (YFCCommon.equals(sExtnOrderCaptureChannel,
				GCConstants.CALL_CENTER_ORDER_CHANNEL)
				|| YFCCommon.equals(sExtnOrderCaptureChannel,
						GCConstants.GCSTORE)) {
			if (!YFCCommon.isVoid(sExtnPickupStoreID)) {

				YFCElement getOrderPersonInfoShipTo = eleOrder
						.getChildElement(GCConstants.PERSON_INFO_SHIP_TO);

				YFCElement changeOrderRootElement = docChangeOrder
						.getDocumentElement();
				YFCElement changeOrderPersonInfoShipTo = changeOrderRootElement
						.createChild(GCConstants.PERSON_INFO_SHIP_TO);

				GCXMLUtil.copyElement(docChangeOrder, getOrderPersonInfoShipTo,
						changeOrderPersonInfoShipTo);

				if (LOGGER.isVerboseEnabled()) {
					LOGGER.verbose("\n docChangeOrder after the copyElement\n"
							+ docChangeOrder.toString());
				}

				String sCompanyWithStoreID = "Guitar Center "
						+ sExtnPickupStoreID;
				changeOrderPersonInfoShipTo.setAttribute(GCConstants.COMPANY,
						sCompanyWithStoreID);

				if (LOGGER.isVerboseEnabled()) {
					LOGGER.verbose("\n docChangeOrder after updating the attribute \n"
							+ docChangeOrder.toString());
				}
				
				/*GCSTORE-4380 - Begin */
				String sConfirmDraftOrderInvoked = (String) env.getTxnObject(GCConstants.ENV_CONFIRM_DRAFT_ORDER_INVOKED);
				LOGGER.verbose("\n Env ConfirmDraftOrderInvoked: " + sConfirmDraftOrderInvoked);
				if(!YFCCommon.isVoid(sConfirmDraftOrderInvoked)){
					if(YFCUtils.equals(sConfirmDraftOrderInvoked, GCConstants.FLAG_N)){
						env.setTxnObject(GCConstants.ENV_CONFIRM_DRAFT_ORDER_INVOKED, GCConstants.FLAG_Y);
						LOGGER.verbose("\n Env ConfirmDraftOrderInvoked: " + (String) env.getTxnObject(GCConstants.ENV_CONFIRM_DRAFT_ORDER_INVOKED));
					}
				}
				/*GCSTORE-4380 - End */
				return true;
			}
		}
		return false;
	}
	// Ends Here 3174
	public static boolean isNodeAStore(YFSEnvironment env, String sOrderlineNode) {
		LOGGER.beginTimer("GCStampShipNodeOnOrderLine.isNodeAStore");
		LOGGER.verbose("Class: GCStampShipNodeOnOrderLine Method: isNodeAStore ::BEGIN");
		// Fix for getOrganizationList going in infinite loop. Changed Template creation.
		YFCDocument templOrgList = YFCDocument.createDocument("OrganizationList");
		YFCElement eleOrgList = templOrgList.getDocumentElement();
		YFCElement eleOrganization = eleOrgList.createChild("Organization");
		eleOrganization.createChild("Node").setAttribute("NodeType", "");

		YFCDocument inDocOrgList = YFCDocument.createDocument("Organization");
		inDocOrgList.getDocumentElement().setAttribute("OrganizationCode",
				sOrderlineNode);
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("\n getOrganizationList Input \n" + inDocOrgList.toString());
		}
		YFCDocument ourDocOrgList = GCCommonUtil.invokeAPI(env,
				GCConstants.GET_ORGANIZATION_LIST, inDocOrgList, templOrgList);
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("\n getOrganizationList Output \n" + ourDocOrgList.toString());
		}
		YFCElement eleRoot = ourDocOrgList.getDocumentElement();
		YFCElement eleOrganizationOut = eleRoot.getChildElement("Organization");
		if (!YFCCommon.isVoid(eleOrganizationOut)) {
			String sNodeType = eleOrganizationOut.getChildElement("Node")
					.getAttribute("NodeType");
			if (YFCCommon.equals(sNodeType, GCConstants.STORE)) {
				LOGGER.verbose("GCStampShipNodeOnOrderLine.isNodeAStore--End");
				LOGGER.endTimer("GCStampShipNodeOnOrderLine.isNodeAStore");
				return true;
			}
		}
		LOGGER.verbose("GCStampShipNodeOnOrderLine.isNodeAStore--End");
		LOGGER.endTimer("GCStampShipNodeOnOrderLine.isNodeAStore");
		return false;
	}
	/**
	 * This method will get the PrimarySupplier from ItemDetail element of software order line only
	 * and stamp it to shipNode at line level of change order.
	 * @param inDoc
	 * @param docChangeOrd
	 * @return
	 */

	private boolean copyPrimySuplrToShipNodeForSoftwareLN(YFCNodeList<YFCElement> nlOrderLine,YFCDocument docChangeOrd){
		LOGGER.debug(" Begin copyPrimySuplrToShipNodeForSoftwareLN changeOrderDoc " + docChangeOrd);
		YFCElement eleChngOrdLines = docChangeOrd.getElementsByTagName(GCConstants.ORDER_LINES).item(0);
		boolean chngOrderApiCallReq = false;
		//NodeList nlOrderLine = GCXMLUtil.getNodeListByXpath(inDoc, "Order/OrderLines/OrderLine");
		int iOrderLineLen = nlOrderLine.getLength();
		YFCElement chngeOrdRootEle = docChangeOrd.getDocumentElement();
		YFCNodeList<YFCElement> nlChngeOrderLine = chngeOrdRootEle.getElementsByTagName(GCConstants.ORDER_LINE);
		for(int orderLine = 0; orderLine < iOrderLineLen; orderLine++){
			YFCElement eleOrderLine = nlOrderLine.item(orderLine);
			YFCElement eleItem = eleOrderLine.getElementsByTagName(GCConstants.ITEM).item(0);
			String strProductLine = eleItem.getAttribute("ProductLine");
			String strBundleParentOLKey = eleOrderLine.getAttribute(GCConstants.BUNDLE_PARENT_ORDERLINEKEY);
			//If order line contains software item and also this line is a component then only stamp primarySupplier to shipNode.
			if("PV".equalsIgnoreCase(strProductLine) && null != strBundleParentOLKey 
					&& !GCConstants.BLANK.equalsIgnoreCase(strBundleParentOLKey) ){
				String strOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
				YFCElement eleItemDetail =  eleOrderLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
				YFCElement elePrimaryInfo = eleItemDetail.getElementsByTagName(GCConstants.PRIMARY_INFORMATION).item(0);
				String strPrimarySupplier = elePrimaryInfo.getAttribute("PrimarySupplier");
				
				String strShipNode = eleOrderLine.getAttribute(GCConstants.SHIP_NODE);
				
				if (YFCCommon.isVoid(strShipNode) || !strShipNode.equalsIgnoreCase(strPrimarySupplier)) {
				boolean findOrdLineInChngOrd = false;
				for(YFCElement eleChngeOrderLine :nlChngeOrderLine){
					String strChngOrdLineKey = eleChngeOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
					if(strOrderLineKey.equalsIgnoreCase(strChngOrdLineKey)){
						eleChngeOrderLine.setAttribute(GCConstants.SHIP_NODE, strPrimarySupplier);
						eleChngeOrderLine.setAttribute("IsFirmPredefinedNode", "Y");
						findOrdLineInChngOrd = true;
						break;
					}
				}
				if(!findOrdLineInChngOrd){
					YFCElement eleChngOrdLine = docChangeOrd.createElement(GCConstants.ORDER_LINE);
					eleChngOrdLine.setAttribute(GCConstants.ORDER_LINE_KEY,strOrderLineKey);
					eleChngOrdLine.setAttribute(GCConstants.SHIP_NODE, strPrimarySupplier);
					eleChngOrdLine.setAttribute("IsFirmPredefinedNode", "Y");
					eleChngOrdLines.appendChild(eleChngOrdLine);
					//chngeOrdRootEle.appendChild(eleChngOrdLines);
				}
				chngOrderApiCallReq = true;
			}
			}
		}
		LOGGER.debug(" End copyPrimySuplrToShipNodeForSoftwareLN changeOrderDoc " + docChangeOrd);
		return chngOrderApiCallReq;
	}
}
