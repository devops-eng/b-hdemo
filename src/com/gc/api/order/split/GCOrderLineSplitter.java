package com.gc.api.order.split;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;

/**
 * GCSTORE-54 This class contains logic to split
 * OrderLine,Taxes,LineCharge,Awards and Reservations at OrderLine Level.
 * GCSTORE-718: Call Center Order Processing
 *
 * @author Gunjan Kumar
 *
 */
public class GCOrderLineSplitter {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCOrderLineSplitter.class.getName());

  private List<List<YFCElement>> splittedOrderLineReservations;
  private GCSplitOrderLineHelper helperObject ;
  private Map<String, YFCElement> allOrderLines;
  private String primeLineNo;
  private List<String> dependentPrimeLineNo;
  private List<Integer> splitRatio;
  private List<Long> transactionLineIdList;
  private Map<String, Integer> bundleComponentRatio = new HashMap<String, Integer>();

  /**
   * This Constructor method initialize the class variable.
   *
   * @param helperObject
   */
  public GCOrderLineSplitter(GCSplitOrderLineHelper helperObject) {
    this.helperObject = helperObject ;
    this.bundleComponentRatio = helperObject.getBundleComponentRatio();
    this.primeLineNo = helperObject.getPrimeLineNo();
    this.dependentPrimeLineNo = helperObject.getDependentPrimeLines();
    this.splitRatio = helperObject.getSplitRatio() ;
    this.allOrderLines = helperObject.getAllOrderLines();
    this.transactionLineIdList = getTransactionLineIdList();
    this.splittedOrderLineReservations = getSplittedOrderLineReservations();
  }

  /**
   * This method contains logic to split OrderLine reservations if required
   * and returns it.
   *
   * @return splittedOrderLineReservations
   */
  public final List<List<YFCElement>> getSplittedOrderLineReservations() {
    LOGGER.beginTimer("GCOrderLineObject.getSplittedOrderLineReservations");
    LOGGER.verbose("Class: GCOrderLineObject Method: getSplittedOrderLineReservations ::BEGIN");
    splittedOrderLineReservations = new ArrayList<List<YFCElement>>();
    YFCElement orderLine = allOrderLines.get(primeLineNo);
    Integer dOrderedQty = GCCommonUtil.getIntAttribute(orderLine, GCConstants.ORDERED_QUANITY);

    // handle bundle reservation
    String sKitCode = orderLine.getAttribute(GCConstants.KIT_CODE);
    boolean isBundle = false;
    if (YFCUtils.equals(GCConstants.BUNDLE, sKitCode)) {
      LOGGER.verbose("sKitCode is Bundle");
      isBundle = true;
    }
    YFCElement orderLineReservations = orderLine.getChildElement(GCConstants.ORDER_LINE_RESERVATIONS);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("\n orderLineReservations" + orderLineReservations);
    }
    if (!YFCCommon.isVoid(orderLineReservations) && orderLineReservations.hasChildNodes()) {
      LOGGER.verbose("orderLineReservations is not null");
      YFCNodeList<YFCElement> orderLineReservationList = orderLineReservations
          .getElementsByTagName(GCConstants.ORDER_LINE_RESERVATION);
      // if single element - easy split in split ratio
      int totalOLRs = orderLineReservationList.getLength();
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("\n totalOLRs" + totalOLRs);
      }
      if (isBundle) {
        LOGGER.verbose("isBundle is true");
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("\n bundleComponentRatio.size()" + bundleComponentRatio.size());
        }
        if (YFCCommon.equals(totalOLRs, bundleComponentRatio.size())) {
          for (int i = 0; i < splitRatio.size(); i++) {
            List<YFCElement> olrs = new ArrayList<YFCElement>();
            for (YFCElement orderLineReservation : orderLineReservationList) {
              YFCElement cloneOLR = (YFCElement) orderLineReservation.cloneNode(true);
              int qty = GCCommonUtil.getIntAttribute(cloneOLR, GCConstants.QUANTITY);
              int splittedQty = (splitRatio.get(i) * qty) / dOrderedQty;
              cloneOLR.setIntAttribute(GCConstants.QUANTITY, splittedQty);
              olrs.add(cloneOLR);
            }
            if (LOGGER.isVerboseEnabled()) {
              LOGGER.verbose("\n adding OLRS" + olrs);
            }
            splittedOrderLineReservations.add(olrs);
            LOGGER.verbose("splittedOrderLineReservations : " + splittedOrderLineReservations);
          }
        } else {
          // this split is because of reservation from multiple nodes.
          List<Integer> olrRatio = new ArrayList<Integer>();
          // get first component from bundleComponentList, get ratio
          // and component name
          // find all reservation for that component and divide by
          // ratio to get split ratio
          // bundleComponentRatio can never be null since it is inside
          // bundle case
          Map.Entry<String, Integer> entry = bundleComponentRatio.entrySet().iterator().next();
          String sComponentItemId = entry.getKey();
          Integer componentQty = entry.getValue();
          if (LOGGER.isVerboseEnabled()) {
            LOGGER.verbose("\n sComponentItemId" + sComponentItemId);
            LOGGER.verbose("\n componentQty" + componentQty);
          }
          Map<String, List<YFCElement>> olrGrps = new LinkedHashMap<String, List<YFCElement>>();

          for (YFCElement olrElem : orderLineReservationList) {
            String sNode = olrElem.getAttribute(GCXmlLiterals.NODE);
            String productAvailabilityDate = olrElem.getAttribute(GCConstants.PRODUCT_AVAILABILITY_DATE);
            String sRequestedReservationDate = olrElem.getAttribute("RequestedReservationDate");

            String key = sNode + ":" + sRequestedReservationDate + ":" + productAvailabilityDate;

            // this will be used lated when we want to have all
            // components together
            if (olrGrps.containsKey(key)) {
              olrGrps.get(key).add((YFCElement) olrElem.cloneNode(true));
            } else {
              List<YFCElement> values = new ArrayList<YFCElement>();
              values.add((YFCElement) olrElem.cloneNode(true));
              olrGrps.put(key, values);
            }
            String sItemID = olrElem.getAttribute(GCXmlLiterals.ITEM_ID);
            if (YFCUtils.equals(sItemID, sComponentItemId)) {
              olrRatio.add(GCCommonUtil.getIntAttribute(olrElem, GCConstants.QUANTITY) / componentQty);
              LOGGER.verbose("olrRatio :" + olrRatio);
            }
          }
          boolean isSplitOnlyBecauseofOLR = true;
          if (YFCCommon.equals(olrRatio.size(), splitRatio.size())) {
            int reservationsForBundle = totalOLRs / bundleComponentRatio.size();
            for (int i = 0; i < reservationsForBundle; i++) {
              if (!YFCCommon.equals(olrRatio.get(i), splitRatio.get(i))) {
                isSplitOnlyBecauseofOLR = false;
                LOGGER.verbose("isSplitOnlyBecauseofOLR :" + isSplitOnlyBecauseofOLR);
                break;
              }
            }
          } else {
            isSplitOnlyBecauseofOLR = false;
          }

          if (isSplitOnlyBecauseofOLR) {
            for (Map.Entry<String, List<YFCElement>> olrGrp : olrGrps.entrySet()) {
              splittedOrderLineReservations.add(olrGrp.getValue());
              LOGGER.verbose("splittedOrderLineReservations" + splittedOrderLineReservations);
            }
          } else {
            // it will be even split
            for (Map.Entry<String, List<YFCElement>> olrGrp : olrGrps.entrySet()) {
              // FIND OUT how many splits possible for this that
              // can be done for get one component
              // and find ratio qty/component qty in bundle ratio
              // for loop for number of splits required
              evenSplitForOrdLineReservation(olrGrp);
            }
          }
        }
      } else {
        // non bundle starts here
        if (YFCCommon.equals(totalOLRs, 1)) {
          LOGGER.verbose("totalOLRs is 1");
          YFCNode orderLineReservation = orderLineReservationList.item(0);
          for (int i = 0; i < splitRatio.size(); i++) {
            List<YFCElement> olrs = new ArrayList<YFCElement>();
            YFCElement cloneOLR = (YFCElement) orderLineReservation.cloneNode(true);
            int qty = GCCommonUtil.getIntAttribute(cloneOLR, GCConstants.QUANTITY);
            int splittedQty = (splitRatio.get(i) * qty) / dOrderedQty;
            cloneOLR.setIntAttribute(GCConstants.QUANTITY, splittedQty);
            olrs.add(cloneOLR);
            splittedOrderLineReservations.add(olrs);
            LOGGER.verbose("splittedOrderLineReservations \n" + splittedOrderLineReservations);
          }
        } else {
          LOGGER.verbose("totalOLRs is not 1");
          List<Integer> olrRatio = new ArrayList<Integer>();
          for (YFCElement olrElem : orderLineReservationList) {
            olrRatio.add(GCCommonUtil.getIntAttribute(olrElem, GCConstants.QUANTITY));
          }
          boolean isSplitOnlyBecauseofOLR = true;
          if (YFCCommon.equals(olrRatio.size(), splitRatio.size())) {
            for (int i = 0; i < totalOLRs; i++) {
              if (!YFCCommon.equals(olrRatio.get(i), splitRatio.get(i))) {
                isSplitOnlyBecauseofOLR = false;
                LOGGER.verbose("isSplitOnlyBecauseofOLR :" + isSplitOnlyBecauseofOLR);
                break;
              }
            }
          } else {
            isSplitOnlyBecauseofOLR = false;
          }
          if (isSplitOnlyBecauseofOLR) {
            for (YFCElement olrElem : orderLineReservationList) {
              List<YFCElement> olrs = new ArrayList<YFCElement>();
              YFCElement cloneOLR = (YFCElement) olrElem.cloneNode(true);
              olrs.add(cloneOLR);
              splittedOrderLineReservations.add(olrs);
            }
          } else {
            // it will be even split
            for (YFCElement olrElem : orderLineReservationList) {
              int qty = GCCommonUtil.getIntAttribute(olrElem, GCConstants.QUANTITY);
              for (int i = 0; i < qty; i++) {
                List<YFCElement> olrs = new ArrayList<YFCElement>();
                YFCElement cloneOLR = (YFCElement) olrElem.cloneNode(true);
                cloneOLR.setIntAttribute(GCConstants.QUANTITY, 1);
                olrs.add(cloneOLR);
                splittedOrderLineReservations.add(olrs);
              }
            }
          }
        }
      }
    }
    LOGGER.verbose("splittedOrderLineReservations:\n" + splittedOrderLineReservations);
    LOGGER.verbose("Class: GCOrderLineObject Method: getSplittedOrderLineReservations ::END");
    LOGGER.endTimer("GCOrderLineObject.getSplittedOrderLineReservations");
    return splittedOrderLineReservations;
  }

  /**
   * This method split OrderLineReservation of Orderline based on split ratio.
   *
   * @param olrGrp
   */
  private void evenSplitForOrdLineReservation(Map.Entry<String, List<YFCElement>> olrGrp) {
    // Refactored code for Sonar Fix.
    List<YFCElement> olr = olrGrp.getValue();
    if (!olr.isEmpty()) {
      LOGGER.verbose("OLR size is greater than 0");
      YFCElement firstComponent = olr.get(0);
      String sCompoId = firstComponent.getAttribute(GCXmlLiterals.ITEM_ID);
      int qty = GCCommonUtil.getIntAttribute(firstComponent, GCConstants.QUANTITY);
      int componentQtyInBundle = bundleComponentRatio.get(sCompoId);
      int loopCount = qty / componentQtyInBundle;

      for (int i = 0; i < loopCount; i++) {
        List<YFCElement> olrs = new ArrayList<YFCElement>();
        for (YFCElement orderLineReservation : olr) {
          YFCElement cloneOLR = (YFCElement) orderLineReservation.cloneNode(true);
          int lineQty = GCCommonUtil.getIntAttribute(cloneOLR, GCConstants.QUANTITY);
          int splittedQty = lineQty / loopCount;
          cloneOLR.setIntAttribute(GCConstants.QUANTITY, splittedQty);
          olrs.add(cloneOLR);
          LOGGER.verbose("olrs: " + olrs);

        }
        splittedOrderLineReservations.add(olrs);
        LOGGER.verbose("splittedOrderLineReservations:" + splittedOrderLineReservations);
      }
    }
  }

  /**
   * This method contains logic to split OrderLines if required and returns
   * splitted OrderLines.
   *
   * @return splittedOrderLines
   */
  public List<YFCElement> getSplittedOrderLines() {
    LOGGER.beginTimer("GCOrderLineObject.getSplittedOrderLines");
    LOGGER.verbose("Class: GCOrderLineObject Method: getSplittedOrderLines ::BEGIN");
    List<YFCElement> splittedOrderLines;
    splittedOrderLines = new ArrayList<YFCElement>();
    int splitRatioSize = splitRatio.size();
    for (int splitRatioCount = 0; splitRatioCount < splitRatioSize; splitRatioCount++) {

      boolean isLast = false ;
      if(YFCCommon.equals(splitRatioCount + 1, splitRatioSize)){
        isLast = true ;
      }

      YFCElement orderLine = allOrderLines.get(primeLineNo);
      YFCElement cloneOrderLine = (YFCElement) orderLine.cloneNode(true);
      cloneOrderLine.setAttribute(GCConstants.PRIME_LINE_NO, "");
      cloneOrderLine
      .setAttribute(GCXmlLiterals.TRANSACTIONAL_LINE_ID, transactionLineIdList.get(splitRatioCount));

      Integer dOrderedQty = GCCommonUtil.getIntAttribute(cloneOrderLine, GCConstants.ORDERED_QUANITY);
      Integer splittedOrderedQty = splitRatio.get(splitRatioCount);

      cloneOrderLine.setAttribute(GCConstants.ORDERED_QUANITY, splittedOrderedQty);

      // handle line charges
      YFCElement eLineCharges = cloneOrderLine.getChildElement(GCConstants.LINE_CHARGES);
      if (!YFCCommon.isVoid(eLineCharges)) {
        YFCNodeList<YFCElement> lineCharges = eLineCharges.getElementsByTagName(GCConstants.LINE_CHARGE);
        for (YFCElement lineCharge : lineCharges) {
          double dChargePerLine = lineCharge.getDoubleAttribute(GCConstants.CHARGE_PER_LINE);
          if(!isLast){
            BigDecimal splittedCharge =
                new BigDecimal(dChargePerLine).multiply(new BigDecimal(splittedOrderedQty)).divide(
                    new BigDecimal(dOrderedQty), 2, BigDecimal.ROUND_DOWN);
            lineCharge.setAttribute(GCConstants.CHARGE_PER_LINE, splittedCharge.doubleValue());
          }else{
            double totalCharge = 0.0 ;
            for (int k = 0; k < splitRatioSize-1; k++) {
              Integer valueK = splitRatio.get(k);
              BigDecimal value =
                  new BigDecimal(dChargePerLine).multiply(new BigDecimal(valueK)).divide(new BigDecimal(dOrderedQty),
                      2, BigDecimal.ROUND_DOWN);
              totalCharge = totalCharge + value.doubleValue();
            }
            lineCharge.setAttribute(GCConstants.CHARGE_PER_LINE, (dChargePerLine - totalCharge ));
          }
        }
        LOGGER.verbose("Splitted eLineCharges:\n" + eLineCharges);
      }
      LOGGER.verbose("handle taxes");
      YFCElement eLineTaxes = cloneOrderLine.getChildElement(GCConstants.LINE_TAXES);
      if (!YFCCommon.isVoid(eLineTaxes)) {
        YFCNodeList<YFCElement> lineTaxs = eLineTaxes.getElementsByTagName(GCConstants.LINE_TAX);
        for (YFCElement lineTax : lineTaxs) {
          double taxPerLine = lineTax.getDoubleAttribute(GCConstants.TAX);
          if(!isLast){
            BigDecimal splittedTax =
                new BigDecimal(taxPerLine).multiply(new BigDecimal(splittedOrderedQty)).divide(
                    new BigDecimal(dOrderedQty), 2, BigDecimal.ROUND_DOWN);
            
            lineTax.setAttribute(GCConstants.TAX, splittedTax.doubleValue());
          }else{
            double totalTax = 0.0 ;
            for (int k = 0; k < splitRatioSize-1; k++) {
              Integer valueK = splitRatio.get(k);
              BigDecimal value =
                  new BigDecimal(taxPerLine).multiply(new BigDecimal(valueK)).divide(new BigDecimal(dOrderedQty), 2,
                      BigDecimal.ROUND_DOWN);
              totalTax = totalTax + value.doubleValue();
            }
            lineTax.setAttribute(GCConstants.TAX, (taxPerLine - totalTax ));
          }
        }
        LOGGER.verbose("Splitted eLineTaxes:\n" + eLineTaxes);
      }
      LOGGER.verbose("handle promotions/awards");
      YFCElement eLineAwards = cloneOrderLine.getChildElement(GCXmlLiterals.AWARDS);
      if (!YFCCommon.isVoid(eLineAwards)) {
        YFCNodeList<YFCElement> lineAwardList = eLineAwards.getElementsByTagName(GCXmlLiterals.AWARD);
        for (YFCElement award : lineAwardList) {
          double awardAmount = award.getDoubleAttribute(GCXmlLiterals.AWARD_AMOUNT);
          Integer awardQuantity = GCCommonUtil.getIntAttribute(award, GCXmlLiterals.AWARD_QTY);

          if(!isLast){
            BigDecimal splittedAwardAmount =
                new BigDecimal(awardAmount).multiply(new BigDecimal(splittedOrderedQty)).divide(
                    new BigDecimal(dOrderedQty), 2, BigDecimal.ROUND_DOWN);
            Integer splittedAwardQuantity = (awardQuantity * splittedOrderedQty) / dOrderedQty;
            award.setDoubleAttribute(GCXmlLiterals.AWARD_AMOUNT, splittedAwardAmount.doubleValue());
            award.setIntAttribute(GCXmlLiterals.AWARD_QTY, splittedAwardQuantity);
          }else{
            Integer splittedAwardQuantity = 0 ;
            double splittedAwardAmount = 0.0 ;
            for (int k = 0; k < splitRatioSize-1; k++) {
              Integer valueK = splitRatio.get(k);

              BigDecimal valueAwardAmount =
                  new BigDecimal(awardAmount).multiply(new BigDecimal(valueK)).divide(new BigDecimal(dOrderedQty), 2,
                      BigDecimal.ROUND_DOWN);
              Integer valueAwardQuantity = (awardQuantity * valueK) / dOrderedQty;

              splittedAwardAmount = splittedAwardAmount + valueAwardAmount.doubleValue();
              splittedAwardQuantity = splittedAwardQuantity + valueAwardQuantity ;
            }
            award.setDoubleAttribute(GCXmlLiterals.AWARD_AMOUNT, awardAmount - splittedAwardAmount);
            award.setIntAttribute(GCXmlLiterals.AWARD_QTY, awardQuantity - splittedAwardQuantity);

          }
        }
      }
      LOGGER.verbose("handle reservations");
      YFCElement orderLineReservations = cloneOrderLine.getChildElement(GCConstants.ORDER_LINE_RESERVATIONS);
      if (!YFCCommon.isVoid(orderLineReservations) && orderLineReservations.hasChildNodes() && splittedOrderLineReservations.size()>0) {
        cloneOrderLine.removeChild(orderLineReservations);
        YFCElement newOrderLineReservations = cloneOrderLine.getChildElement(
            GCConstants.ORDER_LINE_RESERVATIONS, true);
        List<YFCElement> olrs = splittedOrderLineReservations.get(splitRatioCount);
        for (YFCElement elem : olrs) {
          newOrderLineReservations.appendChild(elem);
        }
      }
      // COM OrderProcessing: Start
      String sOrderLineKey = cloneOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      // Remove Keys if Order is splitted except first splitted orderLine
      if (!YFCCommon.isVoid(sOrderLineKey)) {
        if (splitRatioCount == 0) {
          YFCElement cloneOrdLineReservations = cloneOrderLine
              .getChildElement(GCConstants.ORDER_LINE_RESERVATIONS);
          if (!YFCCommon.isVoid(cloneOrdLineReservations)) {
            cloneOrdLineReservations.setAttribute(GCConstants.RESET, GCConstants.YES);
            if (!YFCCommon.equals(GCConstants.BUNDLE, cloneOrderLine.getAttribute(GCXmlLiterals.KIT_CODE))) {
              YFCElement bundleParentLine = cloneOrderLine
                  .getChildElement(GCConstants.BUNDLE_PARENT_LINE);
              if (!YFCCommon.isVoid(bundleParentLine)) {
                cloneOrdLineReservations.setAttribute(GCConstants.RESET, GCConstants.YES);
              }
            } else {
              YFCNodeList<YFCElement> nlOrderLineResv = cloneOrdLineReservations
                  .getElementsByTagName(GCConstants.ORDER_LINE_RESERVATION);
              if (nlOrderLineResv.getLength() > 0) {
                removeOrdLnReservation(cloneOrdLineReservations,
                    nlOrderLineResv);
              }
            }
          }
        } else {
          cloneOrderLine.setAttribute(GCConstants.ACTION, GCConstants.CREATE);
          cloneOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, "");
          YFCElement cloneOrderLineReservations = cloneOrderLine
              .getChildElement(GCConstants.ORDER_LINE_RESERVATIONS);
          if (!YFCCommon.isVoid(cloneOrderLineReservations)) {
            setOrdLnReservAttr(cloneOrderLine, cloneOrderLineReservations);
          }
          YFCElement cloneLineCharges = cloneOrderLine.getChildElement(GCConstants.LINE_CHARGES);
          if (!YFCCommon.isVoid(cloneLineCharges)) {
            cloneLineCharges.setAttribute(GCConstants.RESET, GCConstants.YES);
            YFCNodeList<YFCElement> nlLineCharge = cloneLineCharges
                .getElementsByTagName(GCConstants.LINE_CHARGE);
            for (int j = 0; j < nlLineCharge.getLength(); j++) {
              YFCElement lineCharge = nlLineCharge.item(j);
              lineCharge.setAttribute(GCXmlLiterals.CHARGE_NAME_KEY, "");
            }
          }
          YFCElement cloneLineTaxes = cloneOrderLine.getChildElement(GCConstants.LINE_TAXES);
          if (!YFCCommon.isVoid(cloneLineTaxes)) {
            YFCNodeList<YFCElement> nlLineTax = cloneLineTaxes.getElementsByTagName(GCConstants.LINE_TAX);
            for (int countLineTax = 0; countLineTax < nlLineTax.getLength(); countLineTax++) {
              YFCElement lineCharge = nlLineTax.item(countLineTax);
              lineCharge.setAttribute(GCXmlLiterals.CHARGE_NAME_KEY, "");
            }
          }
        }
      }
      // OrderProcessing: End
      splittedOrderLines.add(cloneOrderLine);
    }
    for (GCSplitOrderLineHelper dependentOrderLine : helperObject.getDependentLineObjects()) {
      LOGGER.verbose("Split Logic for Dependent Line");
      GCOrderLineSplitter dependentOrderLineObject = new GCOrderLineSplitter(dependentOrderLine);
      List<YFCElement> splittedDependentLines = dependentOrderLineObject.getSplittedOrderLines();
      for (int splitDepndntLineCount = 0; splitDepndntLineCount < splittedDependentLines.size(); splitDepndntLineCount++) {
        YFCElement splittedDependentLine = splittedDependentLines.get(splitDepndntLineCount);
        // set parent line id in dependency
        Long dependentTransactionLineId = transactionLineIdList.get(0);
        if (transactionLineIdList.size() > splitDepndntLineCount) {
          dependentTransactionLineId = transactionLineIdList.get(splitDepndntLineCount) ;
        }
        YFCElement splittedDependentLineDependencyElem = splittedDependentLine.getChildElement(GCConstants.DEPENDENCY);
        splittedDependentLineDependencyElem.setAttribute(GCConstants.DEPENDENT_PRIME_LINE_NO, "");
        splittedDependentLineDependencyElem.setAttribute(GCConstants.DEPENDENT_SUB_LINE_NO, "");
        splittedDependentLineDependencyElem.setAttribute(GCXmlLiterals.DEPENDENT_ON_TRAN_LINE_ID, dependentTransactionLineId);
        splittedOrderLines.add(splittedDependentLine);
      }
    }

    LOGGER.verbose("Dependent splittedOrderLines:\n" + splittedOrderLines);
    LOGGER.verbose("Class: GCOrderLineObject Method: getSplittedOrderLines ::END");
    LOGGER.endTimer("GCOrderLineObject.getSplittedOrderLines");
    return splittedOrderLines;
  }

  private void setOrdLnReservAttr(YFCElement cloneOrderLine,
      YFCElement cloneOrderLineReservations) {
    if (!YFCCommon.equals(GCConstants.BUNDLE, cloneOrderLine.getAttribute(GCXmlLiterals.KIT_CODE))) {
      cloneOrderLineReservations.setAttribute(GCConstants.RESET, GCConstants.YES);
      YFCNodeList<YFCElement> nlOrderLineResv = cloneOrderLineReservations
          .getElementsByTagName(GCConstants.ORDER_LINE_RESERVATION);
      if (nlOrderLineResv.getLength() > 0) {
        for (int j = 0; j < nlOrderLineResv.getLength(); j++) {
          YFCElement orderLineReservation = nlOrderLineResv.item(j);
          orderLineReservation.setAttribute(GCXmlLiterals.ORD_LINE_RESV_KEY, "");
          orderLineReservation.setAttribute(GCConstants.ORDER_LINE_KEY, "");
        }
      }
    } else {
      cloneOrderLine.removeChild(cloneOrderLineReservations);
    }
  }

  private void removeOrdLnReservation(YFCElement cloneOrdLineReservations,
      YFCNodeList<YFCElement> nlOrderLineResv) {
    for (int j = 0; j < nlOrderLineResv.getLength(); j++) {
      YFCElement orderLineReservation = nlOrderLineResv.item(j);
      cloneOrdLineReservations.removeChild(orderLineReservation);
      j--;
    }
  }

  public Map<String, YFCElement> getAllOrderLines() {
    return allOrderLines;
  }

  public String getPrimeLineNo() {
    return primeLineNo;
  }

  public List<String> getDependentPrimeLineNo() {
    return dependentPrimeLineNo;
  }

  public List<Integer> getSplitRatio() {
    return splitRatio;
  }

  /**
   * This method returns TransactionLineIdList.
   *
   * @return transactionLineIdList
   */

  public final List<Long> getTransactionLineIdList() {
    LOGGER.beginTimer("GCOrderLineObject.getTransactionLineIdList");
    LOGGER.verbose("Class: GCOrderLineObject Method: getTransactionLineIdList ::BEGIN");
    transactionLineIdList = new ArrayList<Long>();
    for (int i = 0; i < getSplitRatio().size(); i++) {
      // Integer transactionLineID = (Integer.valueOf(primeLineNo) * 1000) + i;

      transactionLineIdList.add(System.nanoTime() + i);
      LOGGER.verbose("transactionLineIdList[" + i + "]=" + transactionLineIdList.get(i));
    }
    LOGGER.verbose("transactionLineIdList:\n" + transactionLineIdList);
    LOGGER.verbose("Class: GCOrderLineObject Method: getTransactionLineIdList ::END");
    LOGGER.endTimer("GCOrderLineObject.getTransactionLineIdList");
    return transactionLineIdList;
  }

}
