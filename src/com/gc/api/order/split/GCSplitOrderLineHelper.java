package com.gc.api.order.split;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCLogUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * GCSTORE-54 Helper POJO to split order lines.
 *
 * @author himanshu.gupta
 *
 */
public class GCSplitOrderLineHelper {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCSplitOrderLineHelper.class);
  private String primeLineNo;
  private List<String> dependentPrimeLines;
  private boolean isUniqueItem;
  private String serialComponent;
  private List<Integer> splitRatio = new ArrayList<Integer>();
  private boolean isBundleItem;
  private boolean isSetItem;
  private boolean isBundleReservationHandlingRequired;
  private boolean isWarrenty;
  private boolean isGift;
  private int lineQty;
  private Map<String, Integer> bundleComponentRatio = new HashMap<String, Integer>();
  private List<GCSplitOrderLineHelper> dependentLineObjects = new ArrayList<GCSplitOrderLineHelper>();
  private GCSplitOrderLineHelper parentLineObject ;
  private Map<String, YFCElement> allOrderLines = new HashMap<String, YFCElement>();
  private YFCElement orderLine;
  private boolean hasGiftItem;
  
  /**
   * This method act as helper for splitting OrderLine based on calculated Split Ratio.
   *
   * @param env
   * @param primeLineNo
   * @param dependentPrimeLines
   * @param allOrderLines
   * @param parentObject
   */
  public GCSplitOrderLineHelper(YFSEnvironment env, String primeLineNo, List<String> dependentPrimeLines,
      Map<String, YFCElement> allOrderLines, GCSplitOrderLineHelper parentObject) {
    LOGGER.beginTimer("GCSplitOrderLineHelper.GCSplitOrderLineHelper");
    LOGGER.verbose("Class: GCSplitOrderLineHelper Method: GCSplitOrderLineHelper ::BEGIN");
    this.primeLineNo = primeLineNo;
    this.dependentPrimeLines = dependentPrimeLines;
    this.allOrderLines = allOrderLines;
    this.parentLineObject = parentObject ;
    orderLine = allOrderLines.get(primeLineNo);

    YFCElement item = orderLine.getChildElement(GCConstants.ITEM, true);
    YFCElement extnElem = orderLine.getChildElement(GCConstants.EXTN, true);
    String sItemId = item.getAttribute(GCConstants.ITEM_ID);
    String sUOM = item.getAttribute(GCConstants.UNIT_OF_MEASURE);
    String productClass = item.getAttribute(GCXmlLiterals.PRODUCT_CLASS);
    String kitCode = orderLine.getAttribute(GCXmlLiterals.KIT_CODE);

    lineQty = GCCommonUtil.getIntAttribute(orderLine, GCConstants.ORDERED_QUANITY);
    isBundleItem = YFCCommon.equals(kitCode, GCConstants.BUNDLE) ? true : false;
    isSetItem = YFCCommon.equals(productClass, GCConstants.SET) ? true : false;
    isWarrenty = extnElem.getBooleanAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
    isGift = extnElem.getBooleanAttribute(GCConstants.EXTN_IS_FREE_GIFT_ITEM);
    boolean preReserved = orderLine.hasAttribute("PreReserved");
    isBundleReservationHandlingRequired = isBundleItem && preReserved;

    populateObject(env, sItemId, sUOM);

    for(String dependentLinePL : dependentPrimeLines){
      GCSplitOrderLineHelper dependent = new GCSplitOrderLineHelper(env, dependentLinePL, new ArrayList<String>(), allOrderLines, this);
      dependentLineObjects.add(dependent);
    }

    if(hasGiftItem){
      splitRatio = populateSplitRatio(lineQty);
      for(GCSplitOrderLineHelper dependentLine : dependentLineObjects){
        dependentLine.splitRatio = populateSplitRatio(dependentLine.lineQty);
      }
    }
    LOGGER.verbose("Class: GCSplitOrderLineHelper Method: GCSplitOrderLineHelper ::END");
    LOGGER.endTimer("GCSplitOrderLineHelper.GCSplitOrderLineHelper");
  }


  public List<Integer> getSplitRatio() {
    return splitRatio;
  }

  public String getPrimeLineNo() {
    return primeLineNo;
  }

  public List<String> getDependentPrimeLines() {
    return dependentPrimeLines;
  }

  public void setDependentPrimeLines(List<String> dependentPrimeLines) {
    this.dependentPrimeLines = dependentPrimeLines;
  }

  public boolean isUniqueItem() {
    return isUniqueItem;
  }

  public boolean isBundleItem() {
    return isBundleItem;
  }

  public boolean isSetItem() {
    return isSetItem;
  }

  public boolean isWarrenty() {
    return isWarrenty;
  }

  public boolean isGift() {
    return isGift;
  }

  public Map<String, Integer> getBundleComponentRatio() {
    return bundleComponentRatio;
  }

  public List<GCSplitOrderLineHelper> getDependentLineObjects() {
    return dependentLineObjects;
  }

  public void setDependentLineObjects(List<GCSplitOrderLineHelper> dependentLineObjects) {
    this.dependentLineObjects = dependentLineObjects;
  }

  public Integer getLineQty() {
    return lineQty;
  }

  public Map<String, YFCElement> getAllOrderLines() {
    return allOrderLines;
  }


 
  /**
   * This method consider OrderLine as object and calculates split ratio for splitting the
   * OrderLine.
   *
   * @param env
   * @param sItemId
   * @param sUOM
   */
  private void populateObject(YFSEnvironment env, String sItemId, String sUOM) {
    LOGGER.beginTimer("GCSplitOrderLineHelper.populateObject");
    LOGGER.verbose("Class: GCSplitOrderLineHelper Method: populateObject ::BEGIN");
    YFCElement itemDetail = null;
    if ((isBundleItem && (isBundleReservationHandlingRequired || lineQty > 1)) || (!isBundleItem && lineQty > 1)) {
      YFCDocument itemDetailDoc = GCCommonUtil.getItemList(env, sItemId, sUOM, GCConstants.GCI,true,
          YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LST_API_TMP_FOR_UNQ_ITEM));
      GCLogUtils.verboseLog(LOGGER, "output of getItemList api : ", itemDetailDoc);
      itemDetail = itemDetailDoc.getDocumentElement().getChildElement(GCConstants.ITEM);
      YFCElement itemExtn = itemDetail.getChildElement(GCConstants.EXTN, true);
      isUniqueItem = itemExtn.getBooleanAttribute(GCXmlLiterals.EXTN_IS_UNIQUE_ITEM);
      serialComponent = itemExtn.getAttribute(GCXmlLiterals.EXTN_SERIAL_COMPONENT);

      YFCElement componentsElem = itemDetail.getChildElement(GCXmlLiterals.COMPONENTS, true);
      YFCIterable<YFCElement> componentList = componentsElem.getChildren();
      for (YFCElement componentItem : componentList) {
        String sCompItemId = componentItem.getAttribute(GCConstants.COMPONENT_ITEM_ID);
        int kitQty = GCCommonUtil.getIntAttribute(componentItem, GCConstants.KIT_QUANTITY);
        bundleComponentRatio.put(sCompItemId, kitQty);
      }
    }
    // has to happen here because we need to use bundleComponentRatio
    if(isBundleReservationHandlingRequired){
      handleBundleReservations(orderLine);
    }

    if (YFCCommon.isVoid(parentLineObject)) {
      splitRatio = setSplitRatio();
    }else if(isWarrenty && !parentLineObject.splitRatio.isEmpty()){
      splitRatio = parentLineObject.splitRatio ;
    }else if(isWarrenty && parentLineObject.splitRatio.isEmpty()){
      splitRatio = setSplitRatio();
      parentLineObject.splitRatio = splitRatio;
    }else if(isGift){
      splitRatio = setSplitRatio();
      if (!(splitRatio.isEmpty() && parentLineObject.splitRatio.isEmpty())) {
        parentLineObject.hasGiftItem = true;
      }
    }else {
      splitRatio = parentLineObject.splitRatio ;
    }
    LOGGER.verbose("Class: GCSplitOrderLineHelper Method: populateObject ::END");
    LOGGER.endTimer("GCSplitOrderLineHelper.populateObject");
  }

  /**
   * This method handles bundle reservations.
   *
   * @param orderLine
   */
  private void handleBundleReservations(YFCElement orderLine) {
    LOGGER.beginTimer("GCSplitOrderLineHelper.handleBundleReservations");
    LOGGER.verbose("Class: GCSplitOrderLineHelper Method: handleBundleReservations ::BEGIN");
    YFCElement orderLineReservations = orderLine.getChildElement(GCConstants.ORDER_LINE_RESERVATIONS);
    if (!YFCCommon.isVoid(orderLineReservations)) {
      YFCIterable<YFCElement> reservationList = orderLineReservations.getChildren();
      YFCElement newOrderLineReservationElem = orderLine.createChild(GCConstants.ORDER_LINE_RESERVATIONS);
      for (YFCElement reservation : reservationList) {
        // remove each reservation and replace with components
        for (Map.Entry<String, Integer> entry : bundleComponentRatio.entrySet()) {
          YFCElement cloneReservation = (YFCElement) reservation.cloneNode(true);
          double bundleQty = cloneReservation.getDoubleAttribute(GCConstants.QUANTITY);
          cloneReservation.setAttribute(GCXmlLiterals.ITEM_ID, entry.getKey());
          cloneReservation.setDoubleAttribute(GCConstants.QUANTITY, (bundleQty * entry.getValue()));
          // check batch no for non serial
          if (!YFCCommon.equals(entry.getKey(), serialComponent)) {
            cloneReservation.setAttribute(GCXmlLiterals.BATCH_NO, "");
          }
          newOrderLineReservationElem.appendChild(cloneReservation);
        }
      }
      orderLine.removeChild(orderLineReservations);
    }
    LOGGER.verbose("Class: GCSplitOrderLineHelper Method: handleBundleReservations ::END");
    LOGGER.endTimer("GCSplitOrderLineHelper.handleBundleReservations");
  }

  /**
   * This method returns split ratio for an OrderLine.
   *
   * @return splitRatio
   */
  public final List<Integer> setSplitRatio() {
    LOGGER.beginTimer("GCSplitOrderLineHelper.setSplitRatio");
    LOGGER.verbose("Class: GCSplitOrderLineHelper Method: setSplitRatio ::BEGIN");
    splitRatio = new ArrayList<Integer>();

    // Schedule N Release Split Ratio--> Assuming CompositionRatio ratio will come all the time
    String sCompositionRatio = orderLine.getAttribute("CompositionRatio");
    String bundleParentLineKey = orderLine.getAttribute(GCConstants.BUNDLE_PARENT_ORDERLINEKEY);
    if (!YFCCommon.isVoid(sCompositionRatio)) {
      List<String> compositionRatioList = new ArrayList<String>(Arrays.asList(sCompositionRatio.split(":")));
      for (int i = compositionRatioList.size() - 1; i >= 0; i--) {
        splitRatio.add(Integer.parseInt(compositionRatioList.get(i)));
      }
      return splitRatio;
    }
    // if lineQty = 1 then split not required
    if (YFCCommon.equals(lineQty, 1) || (!YFCCommon.isVoid(bundleParentLineKey) && !isSetItem)) {
      // for 1 qty no split
      return splitRatio;
    } else if (isUniqueItem || isSetItem) {
      // for unique and serialize item each line with 1 quantity only
      return populateSplitRatio(lineQty);
    } else {
      // now think about multi olrs scenario
      YFCElement orderLineReservations = orderLine.getChildElement(GCConstants.ORDER_LINE_RESERVATIONS);
      if(YFCCommon.isVoid(orderLineReservations)){
        return splitRatio ;
      }
      YFCNodeList<YFCElement> reservationList = orderLineReservations.getElementsByTagName(GCConstants.ORDER_LINE_RESERVATION);
      int totalReservation = reservationList.getLength();
      if(YFCCommon.equals(totalReservation ,  1) || (isBundleItem && YFCCommon.equals(totalReservation, bundleComponentRatio.size()))){
        return splitRatio ;
      }else if (isBundleItem && !YFCCommon.equals(totalReservation, bundleComponentRatio.size())) {
        Map<String, List<Integer>> map = new HashMap<String, List<Integer>>();
        for (YFCElement reservation : reservationList) {
          String sCompItem = reservation.getAttribute(GCXmlLiterals.ITEM_ID);
          int qty = GCCommonUtil.getIntAttribute(reservation, GCConstants.QUANTITY);
          map = GCCommonUtil.updateMap(map, sCompItem, qty);
        }
        for (java.util.Map.Entry<String, List<Integer>> entry : map.entrySet()) {
          String sComponent = entry.getKey();
          List<Integer> values = entry.getValue();
          int componentKitQty = bundleComponentRatio.get(sComponent);
          if (componentKitQty == 1) {
            return values;
          } else {
            for (Integer j : values) {
              splitRatio.add(j / componentKitQty);
            }
          }
        }
      } else{
        for (YFCElement ordLineResEle : reservationList) {
          splitRatio.add(GCCommonUtil.getIntAttribute(ordLineResEle, GCConstants.QUANTITY));
        }
      }
    }
    LOGGER.verbose("Class: GCSplitOrderLineHelper Method: setSplitRatio ::END");
    LOGGER.endTimer("GCSplitOrderLineHelper.setSplitRatio");
    return splitRatio;
  }


  /**
   * This method returns even split ratio for an OrderLine.
   *
   * @param qty
   * @return ratioList
   */
  private List<Integer> populateSplitRatio(int qty) {
    LOGGER.beginTimer("GCSplitOrderLineHelper.populateSplitRatio");
    LOGGER.verbose("Class: GCSplitOrderLineHelper Method: populateSplitRatio ::BEGIN");
    List<Integer> ratioList = new ArrayList<Integer>();
    for (int ratioCount = 1; ratioCount <= qty; ratioCount++) {
      ratioList.add(1);
    }
    LOGGER.verbose("Class: GCSplitOrderLineHelper Method: populateSplitRatio ::END");
    LOGGER.endTimer("GCSplitOrderLineHelper.populateSplitRatio");
    return ratioList;
  }


  public boolean isBundleReservationHandlingRequired() {
    return isBundleReservationHandlingRequired;
  }


}
