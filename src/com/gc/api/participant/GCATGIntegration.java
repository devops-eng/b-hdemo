/**Copyright 2013 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.

    Change Log
######################################################################################################################################
     OBJECTIVE: Foundation Class to call ATG Web-Service for the customer creation/Update. This class creates a Soap request based on
                the input Document (in case of new customer) and getCustomerList API output(in case of customer update) and fetches
                the response Parameters and stamp it on Input Document and returns the Input Document.
######################################################################################################################################
        Version    CR                 Date                   Modified By                   Description
######################################################################################################################################
        1.0    Initial Version      30/01/2015           Singh, Zuzar Inder        GCSTORE-218 - ATGIntegration for Customer Management
######################################################################################################################################
 */

package com.gc.api.participant;

import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.utils.GCCustomerUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class is standalone code which can get called whenver a customer sync is required from OMS
 * to ATG.
 *
 * @author rashmataj
 *
 */
public class GCATGIntegration implements YIFCustomApi{
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCATGIntegration.class.getName());


  /**
   * This method sets the system arguments values.
   */



  private String sTimeOut;
  @Override
  public void setProperties(Properties prop) {

    sTimeOut = prop.getProperty(GCConstants.ATG_WS_TIME_OUT);
  }
  /**
   * This method is standalone code which can get called whenver a customer sync is required from
   * OMS to ATG.
   *
   * @param env
   * @param inDoc
   * @return
   */
  public Document manageATGCustomer(YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("manageATGCustomer");
    YFCDocument yfcDoc = YFCDocument.getDocumentFor(inDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input XML to manageATGCustomer:: " + yfcDoc.toString());
    }
    YFCElement custEle = yfcDoc.getDocumentElement();
    String custId = custEle.getAttribute("CustomerID");
    String orgCode = custEle.getAttribute("OrganizationCode");
    YFCDocument getCustListOutDoc = null;
    if (YFCCommon.isVoid(custId)) {
      YFCException yfcExcep = new YFCException(GCErrorConstants.EXTN_GCE0017, "CustomerID is mandatory");
      throw yfcExcep;
    }
    getCustListOutDoc = GCCustomerUtil.callGetCustomerList(env, custId, orgCode);

    GCCustomerUtil.manageATGCustomer(env, yfcDoc, getCustListOutDoc, sTimeOut);
    LOGGER.endTimer("manageATGCustomer");
    return yfcDoc.getDocument();
  }


}

