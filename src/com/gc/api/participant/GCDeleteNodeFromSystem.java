/** 
 * Copyright � 2015, Guitar Center,  All rights reserved.
 * ####################################################################################################################
 *	OBJECTIVE: Updating the Tracking No in Order Line Table.
 *######################################################################################################################
 *	Version     Date       Modified By              Description
 *######################################################################################################################
    1.0      06/19/2015   Jain, Shubham		Defect-2886 Delete Node from the system
 *######################################################################################################################
 */
package com.gc.api.participant;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCDeleteNodeFromSystem {

	private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCDeleteNodeFromSystem.class.getName());
/**
 * This method delete the node first from all the distribution group & then from the system.
 * @param env
 * @param inDoc
 */
	public void deleteNode(YFSEnvironment env, Document inDoc){
		
		LOGGER.beginTimer("GCDeleteNodeFromSystem.deleteNode");
		LOGGER.verbose("Class: GCDeleteNodeFromSystem Method: deleteNode START");
		
		YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
		
		if(LOGGER.isVerboseEnabled()){
			LOGGER.verbose("InputDoc " + inputDoc.toString());
		}
		YFCElement eleRoot = inputDoc.getDocumentElement();

		//List of DistributionRule
		YFCNodeList<YFCElement> listDistributionRule = eleRoot.getElementsByTagName("DistributionRule");

		//List of Organization
		YFCNodeList<YFCElement> listOrganization = eleRoot.getElementsByTagName(GCConstants.ORGANIZATION);

		//Creating a Input Document for manageOrganizationHierarchy API
		YFCDocument manageOrgHierarchyIp=YFCDocument.createDocument(GCConstants.ORGANIZATION);
		YFCElement  eleManageOrganization=manageOrgHierarchyIp.getDocumentElement();
		
		//Creating a Input Document for deleteDistribution API
		YFCDocument deleteDistributionIp=YFCDocument.createDocument("ItemShipNode");
		YFCElement  eleItemShipNode=deleteDistributionIp.getDocumentElement();
		eleItemShipNode.setAttribute("ItemId", "ALL");

		for(YFCElement eleOrganization : listOrganization ){
			
			String sOrgCode=eleOrganization.getAttribute(GCConstants.ORGANIZATION_CODE);
			YFCElement eleShipNodeKey=eleOrganization.getChildElement(GCConstants.NODE);
			String sShipNodeKey=eleShipNodeKey.getAttribute("ShipnodeKey");
			
			for(YFCElement eleDistributionRule : listDistributionRule){
				String sDistributionRuleId=eleDistributionRule.getAttribute("DistributionRuleId");
				String sOwnerKey=eleDistributionRule.getAttribute("OwnerKey");
				eleItemShipNode.setAttribute("DistributionRuleId",sDistributionRuleId);
				eleItemShipNode.setAttribute("OwnerKey",sOwnerKey);
				eleItemShipNode.setAttribute("ShipnodeKey",sShipNodeKey);
				
				if(LOGGER.isVerboseEnabled()){
					LOGGER.verbose("InputDoc for deleteDistribution " + deleteDistributionIp.toString());
				}
				GCCommonUtil.invokeAPI(env, "deleteDistribution", deleteDistributionIp.getDocument());
			}
			
			eleManageOrganization.setAttribute(GCConstants.OPERATION, "Delete");
			eleManageOrganization.setAttribute(GCConstants.ORGANIZATION_CODE, sOrgCode);
			if(LOGGER.isVerboseEnabled()){
				LOGGER.verbose("InputDoc for manageOrganizationHierarchy" + manageOrgHierarchyIp.toString());
			}
			GCCommonUtil.invokeAPI(env, "manageOrganizationHierarchy", manageOrgHierarchyIp.getDocument());
		}
		LOGGER.verbose("Class: GCDeleteNodeFromSystem Method: deleteNode END");
	}
}
