/**Copyright 2013 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.

	Change Log
##########################################################################################################################################################################################
     OBJECTIVE: Foundation Class
###########################################################################################################################################################################################
        Version    CR/ User Story                 Date                       Modified By                   Description
###########################################################################################################################################################################################
        1.0         GCSTORE-241                   02/04/2015                  Jain,Shubham          publish the updated availability for stores whenever the Store's Active flag is changed.
###########################################################################################################################################################################################
 */

package com.gc.api.participant;

import java.sql.SQLException;
import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.shared.ycp.YFSContext;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**This method is used to publish the updated availability for stores whenever the Store's
 * Active flag is changed.
 * 
 * @author shubham.jain
 * <a href="mailto:shubham.jain@expicient.com">Shubham</a>
 */

public class GCGenerateUpdatedAvailabilityAPI implements YIFCustomApi {
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCGenerateUpdatedAvailabilityAPI.class.getName());

	/**
	 * 
	 */
	String sMaxNumberOfItems;

	public String getsMaxNumberOfItems() {
		return sMaxNumberOfItems;
	}

	public void setsMaxNumberOfItems(String sMaxNumberOfItems) {
		this.sMaxNumberOfItems = sMaxNumberOfItems;
	}

	@Override
	public void setProperties(Properties prop) {
		LOGGER.beginTimer("GCGenerateUpdatedAvailabilityAPI.setProperties");
		LOGGER.verbose("Class: GCGenerateUpdatedAvailabilityAPI Method: setProperties BEGIN");

		setsMaxNumberOfItems(prop.getProperty("NumberOfItemsInABatch"));	

		LOGGER.endTimer("GCGenerateUpdatedAvailabilityAPI.setProperties");

	}

	/**This method is used to publish the updated availability for stores whenever the Store's
	 * Active flag is changed.
	 * @param env
	 * @param inDoc
	 * @return
	 * @throws SQLException
	 */
	public Document generateUpdatedAvailability(YFSEnvironment env, Document inDoc) {

		LOGGER.beginTimer("GCGenerateUpdatedAvailabilityAPI.generateUpdatedAvailability");
		LOGGER.verbose("Class: GCGenerateUpdatedAvailabilityAPI Method: generateUpdatedAvailability BEGIN");

		YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
		YFCElement eleRoot = inputDoc.getDocumentElement();
		String sExtnIsSourcingAllowedFromNode=eleRoot.getAttribute("ExtnIsSourcingAllowedFromNode");
		String sNode=eleRoot.getAttribute(GCConstants.NODE);

		if(YFCCommon.equals(sExtnIsSourcingAllowedFromNode,GCConstants.FLAG_Y)){
			manageInventoryNodeControlWhenEnabled(env, inputDoc);
		}else{
			manageInventoryNodeControlWhenDisbled(env, inputDoc);
			disableNodeFromRTAMDG(env,sNode);
		}

		LOGGER.endTimer("GCGenerateUpdatedAvailabilityAPI.generateUpdatedAvailability");
		return inDoc;
	}

	/**This method disable the Node from RTAM distribution group if the store is Inactive.
	 * 
	 * @param env
	 * @param sShipNode
	 */
	private void disableNodeFromRTAMDG(YFSEnvironment env,String sShipNode){   

		LOGGER.beginTimer("GCGenerateUpdatedAvailabilityAPI.disableNodeFromRTAMDG");
		LOGGER.verbose("Class: GCGenerateUpdatedAvailabilityAPI Method: disableNodeFromRTAMDG BEGIN");

		YFCNodeList<YFCElement> listDG=fetchDGListForRTAM(env);
		for(YFCElement eleOutDocCommonCode:listDG){
			String sCodeValue=eleOutDocCommonCode.getAttribute(GCConstants.CODE_VALUE);
			String sPriority=eleOutDocCommonCode.getAttribute(GCConstants.CODE_SHORT_DESCRIPTION);

			YFCDocument manageDistributionRuleinDoc = YFCDocument.createDocument("DistributionRule");
			YFCElement eleDistributionRule = manageDistributionRuleinDoc.getDocumentElement();
			eleDistributionRule.setAttribute(GCXmlLiterals.DISTRIBUTION_RULE_ID,sCodeValue);
			eleDistributionRule.setAttribute(GCXmlLiterals.OWNER_KEY,GCConstants.GCI);
			eleDistributionRule.setAttribute(GCXmlLiterals.PURPOSE,GCConstants.SOURCING);
			eleDistributionRule.setAttribute(GCXmlLiterals.ITEM_GROUP_CODE,GCConstants.PROD);

			YFCElement eleItemShipNodes=eleDistributionRule.createChild(GCXmlLiterals.ITEM_SHIP_NODES);
			eleItemShipNodes.setAttribute(GCConstants.RESET,GCConstants.FLAG_N);

			YFCElement eleItemShipNode=eleItemShipNodes.createChild(GCXmlLiterals.ITEM_SHIP_NODE);
			eleItemShipNode.setAttribute(GCXmlLiterals.ACTIVE_FLAG,GCConstants.FLAG_N);
			eleItemShipNode.setAttribute("ItemId","ALL");
			eleItemShipNode.setAttribute(GCXmlLiterals.SHIP_NODE_KEY,sShipNode);    

			eleItemShipNode.setAttribute(GCConstants.ITEM_TYPE,"ALL");
			eleItemShipNode.setAttribute(GCConstants.PRIORITY,sPriority);


			//Invoking API
			GCCommonUtil.invokeAPI(env, GCConstants.MANAGE_DISTRIBUTION_RULE,manageDistributionRuleinDoc,null);

		}
		LOGGER.endTimer("GCGenerateUpdatedAvailabilityAPI.disableNodeFromRTAMDG");
	}
	/**This method is used to fetch the all Sourcing Distribution Group list for RTAM by invoking getCommonCodeList API.
	 * 
	 * @param env
	 * @return
	 */
	private YFCNodeList<YFCElement> fetchDGListForRTAM(YFSEnvironment env){
		LOGGER.beginTimer("GCStoreLoadAPI.fetchDGListForRTAM");
		LOGGER.verbose("Class: GCStoreLoadlAPI Method: fetchDGListForRTAM BEGIN");

		Document commonCodeOutDocument=GCCommonUtil.getCommonCodeListByType(env,GCConstants.RTAM_DG,GCConstants.GCI);
		YFCDocument commonCodeOutDoc = YFCDocument.getDocumentFor(commonCodeOutDocument);
		YFCElement eleOutDocCommonCodeList=commonCodeOutDoc.getDocumentElement();
		LOGGER.endTimer("GCStoreLoadAPI.fetchDGListForRTAM");
		return eleOutDocCommonCodeList.getElementsByTagName(GCConstants.COMMON_CODE);
	}
	/**This method is invoking the manageInventoryNodeControl API when Node is Enable.
	 * 
	 * @param env
	 * @param inputDoc
	 */
	private void manageInventoryNodeControlWhenEnabled(YFSEnvironment env, YFCDocument inputDoc){

		LOGGER.beginTimer("GCGenerateUpdatedAvailabilityAPI.manageInventoryNodeControlWhenEnabled");
		LOGGER.verbose("Class: GCGenerateUpdatedAvailabilityAPI Method: manageInventoryNodeControlWhenEnabled BEGIN");

		YFCElement eleRoot = inputDoc.getDocumentElement(); 
		eleRoot.removeAttribute("ExtnIsSourcingAllowedFromNode"); 
		eleRoot.setAttribute("InventoryPictureCorrectForAllItemsAtNode",GCConstants.FLAG_Y);

		GCCommonUtil.invokeAPI(env,GCConstants.MANAGE_INVENTORY_NODE_CONTROL,inputDoc,null);
		LOGGER.endTimer("GCGenerateUpdatedAvailabilityAPI.manageInventoryNodeControlWhenEnabled");

	}

	/**This method is invoking the manageInventoryNodeControl API when Node is Disable.
	 * 
	 * @param env
	 * @param inputDoc
	 * @throws SQLException
	 */
	private void manageInventoryNodeControlWhenDisbled(YFSEnvironment env, YFCDocument inputDoc) {

		LOGGER.beginTimer("GCGenerateUpdatedAvailabilityAPI.manageInventoryNodeControlWhenDisbled");
		LOGGER.verbose("Class: GCGenerateUpdatedAvailabilityAPI Method: manageInventoryNodeControlWhenDisbled BEGIN");

		YFCElement eleRoot = inputDoc.getDocumentElement(); 
		String sNode=eleRoot.getAttribute(GCConstants.NODE);
		String orgCode=eleRoot.getAttribute(GCConstants.ORGANIZATION_CODE);
		YFCNodeList <YFCElement> listItem = null;
		do{
			String lastInventoryItemKey="";
			YFCElement eleOutDocShipNode=getInventorySnapShots(env, lastInventoryItemKey, sNode, orgCode);
			lastInventoryItemKey=eleOutDocShipNode.getAttribute("LastInventoryItemKey");
			listItem=eleOutDocShipNode.getElementsByTagName(GCConstants.ITEM);

			for(YFCElement eleOutDocItem:listItem){
				String sItemID=eleOutDocItem.getAttribute(GCConstants.ITEM_ID);
				String sProductClass=eleOutDocItem.getAttribute(GCConstants.PRODUCT_CLASS);
				String sUnitOfMeasure=eleOutDocItem.getAttribute(GCConstants.UNIT_OF_MEASURE); 
				callManageInventoryNodeControl(env, inputDoc, sItemID, sProductClass, sUnitOfMeasure); 


			}
			if(env instanceof YFSContext){
				try {
					((YFSContext)env).commit();
				} catch (SQLException e) {
					LOGGER.error("Exception while publishing zero availability for node having sourcing disabled",e);
					YFSException yfsEx = new YFSException();
					yfsEx.setErrorCode("EXTN_GCE00100");
					throw yfsEx;

				}
			}
		}while(listItem.getLength()==Integer.parseInt(getsMaxNumberOfItems()));

		LOGGER.endTimer("GCGenerateUpdatedAvailabilityAPI.manageInventoryNodeControlWhenDisbled");
	}
	/**This method is used to invoking the getInventorySnapShot API.
	 * 
	 * @param env
	 * @param lastInventoryItemKey
	 * @param sNode
	 * @param orgCode
	 * @return
	 */
	private YFCElement getInventorySnapShots(YFSEnvironment env, String lastInventoryItemKey,String sNode, String orgCode){

		LOGGER.beginTimer("GCGenerateUpdatedAvailabilityAPI.getInventorySnapShots");
		LOGGER.verbose("Class: GCGenerateUpdatedAvailabilityAPI Method: getInventorySnapShots BEGIN");

		YFCDocument getInventorySnapshotsinDoc = YFCDocument.createDocument("GetInventorySnapShot");
		YFCElement eleGetInventorySnapShot = getInventorySnapshotsinDoc.getDocumentElement();
		eleGetInventorySnapShot.setAttribute("LastInventoryItemKey", lastInventoryItemKey); 
		eleGetInventorySnapShot.setAttribute("MaximumNumberOfItems",sMaxNumberOfItems);
		eleGetInventorySnapShot.setAttribute(GCConstants.ORGANIZATION_CODE,orgCode);
		eleGetInventorySnapShot.setAttribute(GCConstants.SHIP_NODE,sNode);
		YFCDocument getInventorySnapShotTemplate = YFCDocument.getDocumentFor(GCConstants.GET_INVENTORY_SNAPSHOT_TEMPLATE);  
		//Invoking API
		YFCDocument outDocGetInventorySnapShot=GCCommonUtil.invokeAPI(env, GCConstants.GET_INVENTORY_SNAPSHOT,getInventorySnapshotsinDoc,getInventorySnapShotTemplate);
		YFCElement eleOutDocInventorySnapShot=outDocGetInventorySnapShot.getDocumentElement();
		YFCElement eleOutDocShipNode=eleOutDocInventorySnapShot.getChildElement(GCConstants.SHIP_NODE);

		LOGGER.endTimer("GCGenerateUpdatedAvailabilityAPI.getInventorySnapShots");
		return eleOutDocShipNode;
	}

	/**This method is invoking manageInventoryNodeControl API.
	 * 
	 * @param env
	 * @param inputDoc
	 * @param sItemID
	 * @param sProductClass
	 * @param sUnitOfMeasure
	 */

	private void callManageInventoryNodeControl(YFSEnvironment env, YFCDocument inputDoc, String sItemID, String sProductClass, String sUnitOfMeasure){

		LOGGER.beginTimer("GCGenerateUpdatedAvailabilityAPI.callManageInventoryNodeControl");
		LOGGER.verbose("Class: GCGenerateUpdatedAvailabilityAPI Method: callManageInventoryNodeControl BEGIN");

		YFCElement eleRoot = inputDoc.getDocumentElement(); 
		eleRoot.removeAttribute("ExtnIsSourcingAllowedFromNode");
		eleRoot.setAttribute("InventoryPictureCorrectForAllItemsAtNode",GCConstants.FLAG_N);
		eleRoot.setAttribute("InvPictureIncorrectTillDate","2500-01-01");
		eleRoot.setAttribute("ItemID",sItemID);
		eleRoot.setAttribute("ProductClass",sProductClass);
		eleRoot.setAttribute("UnitOfMeasure",sUnitOfMeasure);

		//Invoking API
		GCCommonUtil.invokeAPI(env, GCConstants.MANAGE_INVENTORY_NODE_CONTROL,inputDoc,null);

		LOGGER.endTimer("GCGenerateUpdatedAvailabilityAPI.callManageInventoryNodeControl");
	}
}
