/**Copyright 2013 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.

    Change Log
######################################################################################################################################
     OBJECTIVE: This class is standalone code which will get called whenever POS Customer Id is required.
######################################################################################################################################
        Version    CR                 Date                   Modified By                   Description
######################################################################################################################################
        1.0    Initial Version      02/02/2015                 Taj, Rashma       GCStore213- POS Customer Creation
                                                                                 GCStore326- Tax Exemption Approval Updates

######################################################################################################################################
 */
package com.gc.api.participant;

import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCCustomerUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCGetPOSCustomerID implements YIFCustomApi {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCGetPOSCustomerID.class.getName());

  public Document getPOSCustomerId(YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("getPOSCustomerId");
    YFCDocument yfcDoc = YFCDocument.getDocumentFor(inDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input XML to manageATGCustomer:: " + yfcDoc.toString());
    }
    YFCElement custEle = yfcDoc.getDocumentElement();
    String custId = custEle.getAttribute("CustomerID");
    String orgCode = custEle.getAttribute("OrganizationCode");
    YFCDocument getCustListOutDoc = null;
    if (YFCCommon.isVoid(custId)) {
      YFCException yfcExcep = new YFCException(GCErrorConstants.EXTN_GCE0017, "CustomerID is mandatory");
      throw yfcExcep;
    }
    getCustListOutDoc = GCCustomerUtil.callGetCustomerList(env, custId, orgCode);
    YFCElement custListEle = getCustListOutDoc.getDocumentElement();
    YFCElement customerAPIEle = custListEle.getChildElement("Customer");
    YFCElement eleExtn = customerAPIEle.getChildElement("Extn");
    String extnPOSCustomerId = eleExtn.getAttribute("ExtnPOSCustomerID");
    if (!YFCCommon.isVoid(extnPOSCustomerId)) {
      LOGGER.verbose("POSCustomerID already exits in OMS::" + extnPOSCustomerId);
      YFCElement eleExtnIn = yfcDoc.createElement("Extn");
      custEle.appendChild(eleExtnIn);
      eleExtnIn.setAttribute("ExtnPOSCustomerID", extnPOSCustomerId);
    } else {
      LOGGER.verbose("Calling POS System to get POSCustomerId");
      GCCustomerUtil.handlePOSIntegForTaxExempt(env, yfcDoc, getCustListOutDoc);
      GCCommonUtil.invokeAPI(env, GCConstants.MANAGE_CUSTOMER_API, yfcDoc, YFCDocument.createDocument("Customer"));
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Output from getPOSCustomerId::" + yfcDoc.toString());
    }
    LOGGER.endTimer("getPOSCustomerId");
    return yfcDoc.getDocument();
  }

  @Override
  public void setProperties(Properties prop) {
  }

}
