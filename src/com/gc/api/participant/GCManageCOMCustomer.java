/**Copyright 2013 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.

    Change Log
######################################################################################################################################
     OBJECTIVE: This class will get called whenever a customer is created or updated in call center. This class will will interact with
     ATG and and POS on customer creation or updation in call center.
######################################################################################################################################
        Version    CR                 Date                   Modified By                   Description
######################################################################################################################################
        1.0    Initial Version      02/02/2015                 Taj, Rashma       GCStore213- POS Customer Creation
                                                                                 GCStore326- Tax Exemption Approval Updates
                                                                                 GCSTORE-218 - ATGIntegration for Customer Management
######################################################################################################################################
 */
package com.gc.api.participant;

import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCustomerUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class will get called whenever a customer is created or updated in call center. This class
 * will will interact with ATG and and POS on customer creation or updation in call center.
 *
 * @author rashmataj
 *
 */
public class GCManageCOMCustomer implements YIFCustomApi {
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCManageCOMCustomer.class.getName());

  /**
   * This method sets the system arguments values.
   */

  String sTimeOut;

  @Override
  public void setProperties(Properties prop) {

    sTimeOut = prop.getProperty(GCConstants.ATG_WS_TIME_OUT);

  }

  /**
   * This method will get called whenever a customer is created or updated in call center. This
   * method will check for duplicate email id and integrate with POS and ATG.
   *
   *
   * @param env
   * @param inDoc
   * @return
   */
  public Document manageCOMCustomer(YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("manageCOMCustomer");
    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input to manageCOMCustomer method::" + yfcInDoc.toString());
    }
    YFCElement eleCustomer = yfcInDoc.getDocumentElement();
    String custId = eleCustomer.getAttribute("CustomerID");
    YFCDocument getCustListOutDoc = null;
    if (!YFCCommon.isVoid(custId)) {
      getCustListOutDoc = GCCustomerUtil.callGetCustomerList(env, custId, eleCustomer.getAttribute("OrganizationCode"));
    }
    GCCustomerUtil.checkCustomerEmailID(env, yfcInDoc);
    boolean isPOSIntegrationRequired = GCCustomerUtil.isPOSIntegrationRequired(env, yfcInDoc, getCustListOutDoc);
    if (isPOSIntegrationRequired) {
      GCCustomerUtil.handlePOSIntegForTaxExempt(env, yfcInDoc, getCustListOutDoc);
    }

    // boolean isATGInteractionRequired = true;
    boolean isATGInteractionRequired = isATGInteractionNeeded(env, yfcInDoc.getDocument());

    if (isATGInteractionRequired) {
      GCCustomerUtil.manageATGCustomer(env, yfcInDoc, getCustListOutDoc, sTimeOut);
    }
    LOGGER.endTimer("manageCOMCustomer");
    return yfcInDoc.getDocument();
  }

  public static boolean isATGInteractionNeeded(YFSEnvironment env, Document inDoc) {
    YFCDocument customerDoc = YFCDocument.getDocumentFor(inDoc);
	YFCElement eleCustomerForATG = customerDoc.getDocumentElement();
    String taxValueChanged = eleCustomerForATG.getAttribute("TaxExemptValueChanged");
    boolean isATGInteractionRequired = true;
    String sAddressLine1 = "";
    String sFirstName = "";
    String sLastName = "";
    String sEmailID = "";
    YFCElement elePersonInfo = customerDoc.getElementsByTagName(GCConstants.PERSON_INFO).item(0);
    if (!YFCCommon.isVoid(elePersonInfo)) {
      sAddressLine1 = elePersonInfo.getAttribute(GCConstants.ADDRESS_LINE1);
    }
    if (YFCCommon.isVoid(elePersonInfo) || YFCCommon.isVoid(sAddressLine1)) {
      isATGInteractionRequired = false;
    }
    YFCElement eleCustomerContact = customerDoc.getElementsByTagName(GCConstants.CUSTOMER_CONTACT).item(0);
    if (!YFCCommon.isVoid(eleCustomerContact)) {
      sFirstName = eleCustomerContact.getAttribute(GCConstants.FIRST_NAME);
      sLastName = eleCustomerContact.getAttribute(GCConstants.LAST_NAME);
      sEmailID = eleCustomerContact.getAttribute("EmailID");
    }
    if (YFCCommon.isVoid(sFirstName) && YFCCommon.isVoid(sLastName)) {
      isATGInteractionRequired = false;
    }
    if (YFCCommon.isVoid(sEmailID)) {
      isATGInteractionRequired = false;
    }
	if(!(YFCCommon.isVoid(sEmailID)) && YFCCommon.equalsIgnoreCase(taxValueChanged, "Y")){
    	isATGInteractionRequired = true;
    }
    return isATGInteractionRequired;
  }
}
