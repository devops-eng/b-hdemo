/**Copyright 2013 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.

    Change Log
######################################################################################################################################
     OBJECTIVE: This class checks if the POS Inetgration is required during Tax Exemption.
######################################################################################################################################
        Version    CR                 Date                   Modified By                   Description
######################################################################################################################################
        1.0    Initial Version      02/02/2015                 Taj, Rashma       GCStore213- POS Customer Creation
                                                                                 GCStore326- Tax Exemption Approval Updates
######################################################################################################################################
 */
package com.gc.api.participant;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCWebServiceUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class GCPOSIntegrationService implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCPOSIntegrationService.class);
  String sTimeOut;

  @Override
  public void setProperties(Properties prop) {

    sTimeOut = prop.getProperty(GCConstants.POS_WS_TIME_OUT);
  }



  /**
   * This method interacts with POS System and returns the webservice response.
   *
   * @param env
   * @param inDoc
   * @return
   */
  public Document managePOSCustomer(YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("managePOSCustomer");
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input to managePOSCustomer methd::" + inputDoc.toString());
    }
    Document docPOSManageCustomerOutput = null;
    String soapURL = YFSSystem.getProperty(GCConstants.POS_WEBSERVICE_URL);
    String soapAction = YFSSystem.getProperty(GCConstants.POS_WEBSERVICE_ACTION);
    YFCElement inDocAddress = getDefaultBillToAddress(inputDoc);
    YFCDocument docPOSManageCustomerInput = formSoapRequest(inDocAddress, inputDoc);
    GCWebServiceUtil webServiceObj = new GCWebServiceUtil();

    if (LOGGER.isVerboseEnabled() && !YFCCommon.isVoid(docPOSManageCustomerInput)) {
      LOGGER.verbose("SoapRequest formed for POS Integration::"
          + docPOSManageCustomerInput.toString() + "SoapURL:"
          + soapURL + "SoapAction:" + soapAction);
    }
    try {
      // invoke webservice
      docPOSManageCustomerOutput =
          webServiceObj.invokeSOAPWebService(docPOSManageCustomerInput.getDocument(), soapURL, soapAction, sTimeOut);

    } catch (ConnectTimeoutException timeoutEx) {
      LOGGER.error("The host did not accept the connection::", timeoutEx);
      YFCException yfcExcep = new YFCException("EXTNPOSDown");
      yfcExcep.setErrorDescription("POS Inetgration Failed due to connection timeout::" + timeoutEx.getMessage());
      throw yfcExcep;
    }catch (YFSException yfsExcep) {
      LOGGER.error("POS Inetgration Failed", yfsExcep);
      YFCException yfcExcep = new YFCException("EXTNPOSDown");
      yfcExcep.setErrorDescription("POS Inetgration Failed due to connection timeout::" + yfsExcep.getMessage());
      throw yfcExcep;
    } catch (IOException excep) {
      LOGGER.error("POS Inetgration Failed" , excep);
      YFCException yfcExcep = new YFCException("EXTNPOSDown");
      yfcExcep.setErrorDescription("POS Inetgration Failed::" + excep.getMessage());
      throw yfcExcep;
    } catch (Exception excep) {
      // Added for occurance of random technical exceptions.
      LOGGER.error("POS Inetgration Failed", excep);
      YFCException yfcExcep = new YFCException("EXTNPOSDown");
      yfcExcep.setErrorDescription("POS Inetgration Failed::"
          + excep.getMessage());
      throw yfcExcep;
    }
    if (LOGGER.isVerboseEnabled() && !YFCCommon.isVoid(docPOSManageCustomerOutput)) {
      LOGGER.verbose("Webservice Response from POS::" + docPOSManageCustomerOutput.toString());
    }
    LOGGER.endTimer("managePOSCustomer");
    return docPOSManageCustomerOutput;

  }

  /**
   * This method forms the POS Customer Creation soap request with the given input.
   *
   * @param inDocAddress
   * @param inDoc
   * @return
   */
  private YFCDocument formSoapRequest(YFCElement inDocAddress, YFCDocument inDoc) {
    LOGGER.beginTimer("formSoapRequest");
    YFCDocument docPOSManageCustomerInput =
        YFCDocument.getDocumentFor(GCCommonUtil
            .getDocumentFromPath("/global/template/api/extn_POSCustomerSyncRequest.xml"));

    YFCElement requestAddress = docPOSManageCustomerInput.getElementsByTagName("Address").item(0);
    String resaleExpiry = null;
    String omsCustId = inDoc.getDocumentElement().getAttribute("CustomerID");
    YFCElement personInfo = inDocAddress.getElementsByTagName("PersonInfo").item(0);
    String firstName = personInfo.getAttribute("FirstName");
    String lastName = personInfo.getAttribute("LastName");
    String middleName = personInfo.getAttribute("MiddleName");
    String email = personInfo.getAttribute("EMailID");
    String companyName = personInfo.getAttribute("Company");

    // Fix for GCSTORE#4997 -- Starts
    //Reverting the changes made for 4997
  /*  if (!YFCCommon.isVoid(companyName)) {
      String newFirstName = "";
      String[] companyNameList = companyName.split(" ");
      final int noOfCompanyName = companyNameList.length;
      LOGGER.verbose("noOfCompanyName :" + noOfCompanyName);
      if (noOfCompanyName == 1) {
        newFirstName = companyName;
        lastName = companyName;
      } else {
        newFirstName = companyNameList[0];
        for (int i = 1; i < noOfCompanyName - 1; i++) {
          newFirstName = newFirstName + " " + companyNameList[i];
        }
        lastName = companyNameList[noOfCompanyName - 1];
      }
      firstName = newFirstName;
      middleName = "";

      LOGGER.verbose("firstName :" + firstName);
      LOGGER.verbose("lastName :" + lastName);
    }*/
    // Fix for GCSTORE#4997 -- Ends
    String street1 =
        personInfo.getAttribute("AddressLine1") + "," + personInfo.getAttribute("AddressLine2") + ","
            + personInfo.getAttribute("AddressLine3");
    String city = personInfo.getAttribute("City");
    String state = personInfo.getAttribute("State");
    String zipCode = personInfo.getAttribute("ZipCode");
    String country = personInfo.getAttribute("Country");
    String dayPhone = personInfo.getAttribute("DayPhone");
    String nightPhone = personInfo.getAttribute("EveningPhone");
    YFCElement eleCustExtn = inDoc.getDocumentElement().getChildElement("Extn");
    String resaleNo = eleCustExtn.getAttribute("ExtnTaxExemptId");
    String taxExemptStatus = eleCustExtn.getAttribute("ExtnTaxExemptReqStatus");
    Date resaleExpiryDate = eleCustExtn.getDateAttribute("ExtnTaxExemptReqValidUntil");
    String posCustId = eleCustExtn.getAttribute("ExtnPOSCustomerID");
    int intIndexDiff = 0;
    if (YFCCommon.equals(taxExemptStatus, "Declined", false)) {
      resaleExpiry = GCDateUtils.getPreviousDate(GCXmlLiterals.POS_DATE_TIME_FORMAT, 1) + "";
    } else if (!YFCCommon.isVoid(resaleExpiryDate)) {
      resaleExpiry = new SimpleDateFormat(GCXmlLiterals.POS_DATE_TIME_FORMAT).format(resaleExpiryDate);
      intIndexDiff = resaleExpiry.indexOf('T');
    }
    String resaleState = eleCustExtn.getAttribute("ExtnTaxExemptState");
    requestAddress.getElementsByTagName("POSCustID").item(0).setNodeValue(posCustId);
    requestAddress.getElementsByTagName("OMScustID").item(0).setNodeValue(omsCustId);
    requestAddress.getElementsByTagName("FirstName").item(0).setNodeValue(firstName);
    requestAddress.getElementsByTagName("LastName").item(0).setNodeValue(lastName);
    requestAddress.getElementsByTagName("MiddleInitial").item(0).setNodeValue(middleName);
    requestAddress.getElementsByTagName("Email").item(0).setNodeValue(email);
    requestAddress.getElementsByTagName("CompanyName").item(0).setNodeValue(companyName);
    requestAddress.getElementsByTagName("Street1").item(0).setNodeValue(street1);
    requestAddress.getElementsByTagName("City").item(0).setNodeValue(city);
    requestAddress.getElementsByTagName("State").item(0).setNodeValue(state);
    requestAddress.getElementsByTagName("ZipCode").item(0).setNodeValue(zipCode);
    requestAddress.getElementsByTagName("Country").item(0).setNodeValue(country);
    requestAddress.getElementsByTagName("DayPhone").item(0).setNodeValue(dayPhone);
    requestAddress.getElementsByTagName("NightPhone").item(0).setNodeValue(nightPhone);
    requestAddress.getElementsByTagName("ResaleNumber").item(0).setNodeValue(resaleNo);
    if (!YFCCommon.isVoid(resaleExpiry)) {
      requestAddress.getElementsByTagName("ResaleExpire").item(0).setNodeValue(resaleExpiry.substring(0, intIndexDiff));
    } else {
      requestAddress.getElementsByTagName("ResaleExpire").item(0).setNodeValue("");
    }
    requestAddress.getElementsByTagName("ResaleState").item(0).setNodeValue(resaleState);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Soap request formed for POS webservice call::" + docPOSManageCustomerInput.toString());
    }
    LOGGER.endTimer("formSoapRequest");
    return docPOSManageCustomerInput;
  }

  /**
   * this method gets the DefaultBillTo Address from the input.
   *
   * @param inDoc
   * @return
   */
  private YFCElement getDefaultBillToAddress(YFCDocument inDoc) {

    LOGGER.beginTimer("getDefaultBillToAddress");
    YFCNodeList<YFCElement> customerAdditionalAddList = inDoc.getElementsByTagName("CustomerAdditionalAddress");
    for (YFCElement custAdditionalAddrs : customerAdditionalAddList) {
      String isDefaultBillTo = custAdditionalAddrs.getAttribute("IsDefaultBillTo");
      if (YFCCommon.equals(isDefaultBillTo, GCConstants.YES)) {
        LOGGER.endTimer("getDefaultBillToAddress");
        return custAdditionalAddrs;
      }

    }
    LOGGER.endTimer("getDefaultBillToAddress");
    return null;
  }
}
