/**Copyright 2013 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.

	Change Log
#####################################################################################################################################################################################################################
     OBJECTIVE: Foundation Class
######################################################################################################################################################################################################################
        Version    CR/ User Story                 Date                       Modified By                   Description
######################################################################################################################################################################################################################
        1.0         GCSTORE-241                   02/04/2015                 Jain,Shubham          Used to manage the Organization in OMS &And publish the messages to the internal queue for latest availability
        1.1         GCSTORE-794                   03/19/2015                 Ravi, Divya           Stamping shipping calendar key in the new store getting created - Phase 2 Order Fulfillment changes
        1.2         MPOS-223                      04/05/2016                 Mittal, Yashu         Creating pricelist for each new store created in OMS.
######################################################################################################################################################################################################################
 */
package com.gc.api.participant;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCDate;
import com.yantra.yfs.japi.YFSEnvironment;

/** This class is used to manage the Organization in OMS.It also manage the Distribution Group for sourcing.
 * It checks whether Store is Active or Inactive from the "ExtnIsSourceFromStoreAllowed" Flag.
 * And publish the messages to the internal queue for latest availability.
 * 
 * @author Shubham Jain
 * <a href="mailto:shubham.jain@expicient.com">Shubham</a>
 *
 */

public class GCStoreLoadAPI implements YIFCustomApi {
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCStoreLoadAPI.class.getName());

	@Override
	public void setProperties(Properties arg0) {
	}

	/**This method is used to manage the Organization in OMS. 
	 * It checks whether Store is Active or Inactive from the "ExtnIsSourceFromStoreAllowed" Flag.
	 * 
	 * 	
	 * @param env
	 * @param inDoc
	 * @return
	 */
	public Document loadStoreFeed(YFSEnvironment env, Document inDoc) {
		LOGGER.beginTimer("GCStoreLoadAPI.loadStoreFeed");
		LOGGER.verbose("Class: GCStoreLoadlAPI Method: loadStoreFeed BEGIN");
		
		String sExtnSourceFromStoreInOMS="";
		String sExtnSourceFromStoreInFeed="";
		YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
		YFCElement eleRoot = inputDoc.getDocumentElement();
		YFCElement eleNode = eleRoot.getChildElement(GCConstants.NODE);
		String sShipNode = eleNode.getAttribute(GCConstants.SHIP_NODE); 
		
		//Phase 2 - Order Fulfilment design changes
		//stamping shipping calendar on every node
		String parentOrgCode = eleRoot.getAttribute(GCConstants.PARENT_ORG_CODE);
		stampShippingCalendar(env, eleNode, parentOrgCode);
				
		if (LOGGER.isVerboseEnabled()){
			LOGGER.verbose("Input to Queue :" + inputDoc.getString());
		}
		if (!YFCCommon.isVoid(sShipNode)) { 
			YFCElement eleoutDocOrganization=fetchOrganization(env,inputDoc);
			if(!YFCCommon.isVoid(eleoutDocOrganization)){	
				YFCElement eleoutDocExtn = eleoutDocOrganization.getChildElement(GCConstants.EXTN);
				if(!YFCCommon.isVoid(eleoutDocExtn)){
					sExtnSourceFromStoreInOMS = eleoutDocExtn.getAttribute(GCXmlLiterals.SOURCE_FROM_STORE_IN_OMS);
				}
			}
		}
    // To stamp payment types and payment rule on each store for mPOS.
    stampPaymentTypeOnStore(env, inputDoc);
    GCCommonUtil.invokeAPI(env,GCConstants.MANAGE_ORGANIZATION_HIERARCHY, inputDoc,null);

    // MPOS-223 Price Feed.
    String sOrganizationCode = eleRoot.getAttribute(GCConstants.ORGANIZATION_CODE);
    String sNodeType = eleNode.getAttribute(GCConstants.NODE_TYPE);
    if (!YFCCommon.isVoid(sOrganizationCode) && YFCCommon.equals(sNodeType, GCConstants.STORE)) {
      createPriceListHeaderForStore(env, sOrganizationCode);
    }

		if(YFCCommon.isVoid(sShipNode)){
			return inDoc;
		}

		YFCElement eleExtn = eleRoot.getChildElement(GCConstants.EXTN);
		sExtnSourceFromStoreInFeed = eleExtn.getAttribute(GCXmlLiterals.SOURCE_FROM_STORE_IN_FEED);

		if (!YFCCommon.equals(sExtnSourceFromStoreInOMS,sExtnSourceFromStoreInFeed)) {

			manageSourcingDistributionGroup(env,sShipNode,sExtnSourceFromStoreInFeed);
			manageRTAMDistributionGroup(env, sShipNode,sExtnSourceFromStoreInFeed);
			publishMessageForLatestAvailability(env,sShipNode,sExtnSourceFromStoreInFeed);

		}
		LOGGER.endTimer("GCStoreLoadAPI.loadStoreFeed");
		return inputDoc.getDocument();
	}

	/**
	 * Phase 2 Order Fulfillment change - This method is used to stamp shipping calendar on the Node
	 * 
	 * @param env
	 * @param eleNode
	 * @param parentOrgCode
	 */

	private void stampShippingCalendar(YFSEnvironment env, YFCElement eleNode, String parentOrgCode) {
		
		// Creating a New Input Doc for getCalendarList API
		YFCDocument getCalendarListInDoc = YFCDocument.getDocumentFor("<Calendar  CalendarId='"+ GCConstants.GC_STORE_CALENDAR+"'  OrganizationCode='"+ parentOrgCode +"' />");
		// Creating a template for getCalendarList API
		YFCDocument getCalendarListTemplate = YFCDocument.getDocumentFor(GCConstants.GET_CALENDAR_LIST_TEMP_XML);
		// Invoking getCalendarList API
		YFCDocument getCalendarListOutDoc = GCCommonUtil.invokeAPI(env, GCConstants.GET_CALENDAR_LIST_API, getCalendarListInDoc, getCalendarListTemplate);
		
		YFCElement eleCalendars = getCalendarListOutDoc.getDocumentElement();
		YFCElement eleCalendar = eleCalendars.getChildElement(GCConstants.CALENDAR);
		String calKey = eleCalendar.getAttribute(GCConstants.CALENDAR_KEY);
			
		eleNode.setAttribute(GCConstants.SHIPPING_CAL_KEY, calKey);
		
	}

	/**This method is used to fetch the Organization by invoking getOrganizationList API.
	 * 
	 * @param env
	 * @param inputDoc
	 * @return
	 */
	private YFCElement fetchOrganization(YFSEnvironment env, YFCDocument inputDoc){
		LOGGER.beginTimer("GCStoreLoadAPI.fetchOrganization");
		LOGGER.verbose("Class: GCStoreLoadlAPI Method: fetchOrganization BEGIN");


		YFCElement eleRoot = inputDoc.getDocumentElement();
		String sOrganizationCode = eleRoot.getAttribute(GCConstants.ORGANIZATION_CODE);

		// Creating a New Input Doc for getOrganizationList API
		YFCDocument getOrganizationListinDoc = YFCDocument.createDocument(GCConstants.ORGANIZATION);
		YFCElement eleOrganization = getOrganizationListinDoc.getDocumentElement();
		eleOrganization.setAttribute(GCConstants.ORGANIZATION_CODE,sOrganizationCode);

		// Creating a template for getOrganizationList API
		YFCDocument getOrganizationListTemplate = YFCDocument.getDocumentFor(GCConstants.GET_ORGANIZATION_LIST_TEMPLATE);

		// Invoking getOrganizationList API
		YFCDocument outDoc = GCCommonUtil.invokeAPI(env,GCConstants.GET_ORGANIZATION_LIST,getOrganizationListinDoc, getOrganizationListTemplate);

		YFCElement eleoutDocOrganizationList = outDoc.getDocumentElement();
		YFCElement eleoutDocOrganization = eleoutDocOrganizationList.getChildElement(GCConstants.ORGANIZATION);

		LOGGER.endTimer("GCStoreLoadAPI.fetchOrganization");

		return eleoutDocOrganization;
	}

	/** This method is used to manage the Distribution group for RTAM by invoking manageDistributionRule API.
	 * 
	 * @param env
	 * @param sShipNode
	 * @param sExtnSourceFromStoreInFeed
	 */
	private void manageRTAMDistributionGroup(YFSEnvironment env,String sShipNode,String sExtnSourceFromStoreInFeed){   
		LOGGER.beginTimer("GCStoreLoadAPI.manageRTAMDistributionGroup");
		LOGGER.verbose("Class: GCStoreLoadlAPI Method: manageRTAMDistributionGroup BEGIN");

		if(YFCCommon.equals(sExtnSourceFromStoreInFeed,GCConstants.FLAG_Y)){
			YFCNodeList<YFCElement> listDG=fetchDGListForRTAM(env);
			for(YFCElement eleOutDocCommonCode:listDG){
				String sCodeValue=eleOutDocCommonCode.getAttribute(GCConstants.CODE_VALUE);
				String sPriority=eleOutDocCommonCode.getAttribute(GCConstants.CODE_SHORT_DESCRIPTION);

				YFCDocument manageDistributionRuleinDoc = YFCDocument.createDocument("DistributionRule");
				YFCElement eleDistributionRule = manageDistributionRuleinDoc.getDocumentElement();
				eleDistributionRule.setAttribute(GCXmlLiterals.DISTRIBUTION_RULE_ID,sCodeValue);
				eleDistributionRule.setAttribute(GCXmlLiterals.OWNER_KEY,GCConstants.GCI);
				eleDistributionRule.setAttribute(GCXmlLiterals.PURPOSE,GCConstants.SOURCING);
				eleDistributionRule.setAttribute(GCXmlLiterals.ITEM_GROUP_CODE,GCConstants.PROD);

				YFCElement eleItemShipNodes=eleDistributionRule.createChild(GCXmlLiterals.ITEM_SHIP_NODES);
				eleItemShipNodes.setAttribute(GCConstants.RESET,GCConstants.FLAG_N);

				YFCElement eleItemShipNode=eleItemShipNodes.createChild(GCXmlLiterals.ITEM_SHIP_NODE);
				eleItemShipNode.setAttribute(GCXmlLiterals.ACTIVE_FLAG,sExtnSourceFromStoreInFeed);
				eleItemShipNode.setAttribute("ItemId","ALL");
				eleItemShipNode.setAttribute(GCXmlLiterals.SHIP_NODE_KEY,sShipNode);    

				eleItemShipNode.setAttribute(GCConstants.ITEM_TYPE,"ALL");
				eleItemShipNode.setAttribute(GCConstants.PRIORITY,sPriority);

				if (LOGGER.isVerboseEnabled()){
					LOGGER.verbose("Input to manageDistributionRuleAPI for RTAM_DG :" + manageDistributionRuleinDoc.getString());
				}

				//Invoking API 
				GCCommonUtil.invokeAPI(env, GCConstants.MANAGE_DISTRIBUTION_RULE,manageDistributionRuleinDoc,null);

			}
		}
		LOGGER.endTimer("GCStoreLoadAPI.manageRTAMDistributionGroup");
	}

	/**This method is used to fetch the all Sourcing Distribution Group list for RTAM by invoking getCommonCodeList API.
	 * 
	 * @param env
	 * @return
	 */
	private YFCNodeList<YFCElement> fetchDGListForRTAM(YFSEnvironment env){
		LOGGER.beginTimer("GCStoreLoadAPI.fetchDGListForRTAM");
		LOGGER.verbose("Class: GCStoreLoadlAPI Method: fetchDGListForRTAM BEGIN");

		Document commonCodeOutDocument=GCCommonUtil.getCommonCodeListByType(env,GCConstants.RTAM_DG,GCConstants.GCI);
		YFCDocument commonCodeOutDoc = YFCDocument.getDocumentFor(commonCodeOutDocument);		
		YFCElement eleOutDocCommonCodeList=commonCodeOutDoc.getDocumentElement();
		LOGGER.endTimer("GCStoreLoadAPI.fetchDGListForRTAM");
		return eleOutDocCommonCodeList.getElementsByTagName(GCConstants.COMMON_CODE);
	}

	/** This method is used to manage the Sourcing Distribution Group by invoking manageDistributionRule API.
	 * 
	 * @param env
	 * @param sShipNode
	 * @param sExtnSourceFromStoreInFeed
	 */
	private void manageSourcingDistributionGroup(YFSEnvironment env,String sShipNode, String sExtnSourceFromStoreInFeed){
		LOGGER.beginTimer("GCStoreLoadAPI.manageSourcingDistributionGroup");
		LOGGER.verbose("Class: GCStoreLoadlAPI Method: manageSourcingDistributionGroup BEGIN");

		YFCNodeList<YFCElement> listDG=fetchDGList(env);
		for(YFCElement eleOutDocCommonCode:listDG){
			String sCodeValue=eleOutDocCommonCode.getAttribute(GCConstants.CODE_VALUE);
			String sPriority=eleOutDocCommonCode.getAttribute(GCConstants.CODE_SHORT_DESCRIPTION);

			//Creating a Input Doc for manageDistributionRule API
			YFCDocument manageDistributionRuleinDoc = YFCDocument.createDocument("DistributionRule");
			YFCElement eleDistributionRule = manageDistributionRuleinDoc.getDocumentElement();
			eleDistributionRule.setAttribute(GCXmlLiterals.DISTRIBUTION_RULE_ID,sCodeValue);
			eleDistributionRule.setAttribute(GCXmlLiterals.OWNER_KEY,GCConstants.GC);
			eleDistributionRule.setAttribute(GCXmlLiterals.PURPOSE,GCConstants.SOURCING);
			eleDistributionRule.setAttribute(GCXmlLiterals.ITEM_GROUP_CODE,GCConstants.PROD);

			YFCElement eleItemShipNodes=eleDistributionRule.createChild(GCXmlLiterals.ITEM_SHIP_NODES);
			eleItemShipNodes.setAttribute(GCConstants.RESET,GCConstants.FLAG_N);

			YFCElement eleItemShipNode=eleItemShipNodes.createChild(GCXmlLiterals.ITEM_SHIP_NODE);
			eleItemShipNode.setAttribute(GCXmlLiterals.ACTIVE_FLAG,sExtnSourceFromStoreInFeed);
			eleItemShipNode.setAttribute("ItemId","ALL");
			eleItemShipNode.setAttribute(GCXmlLiterals.SHIP_NODE_KEY,sShipNode);    

			eleItemShipNode.setAttribute(GCConstants.ITEM_TYPE,"ALL");
			eleItemShipNode.setAttribute(GCConstants.PRIORITY,sPriority);

			if (LOGGER.isVerboseEnabled()){
				LOGGER.verbose("Input to manageDistributionRuleAPI :" + manageDistributionRuleinDoc.getString());
			}

			//Invoking API
			GCCommonUtil.invokeAPI(env, GCConstants.MANAGE_DISTRIBUTION_RULE,manageDistributionRuleinDoc,null);

		}
		LOGGER.endTimer("GCStoreLoadAPI.manageSourcingDistributionGroup");
	}


	/** This method is used to fetch the all Sourcing Distribution Group list by invoking getCommonCodeList API.
	 * 
	 * @param env
	 * @return
	 */
	private YFCNodeList<YFCElement> fetchDGList(YFSEnvironment env){
		LOGGER.beginTimer("GCStoreLoadAPI.fetchDGList");
		LOGGER.verbose("Class: GCStoreLoadlAPI Method: fetchDGList BEGIN");


		Document commonCodeOutDocument=GCCommonUtil.getCommonCodeListByType(env,GCConstants.STORE_DG,GCConstants.GC);
		YFCDocument commonCodeOutDoc = YFCDocument.getDocumentFor(commonCodeOutDocument);

		//Iterate through each DG Entry!!
		YFCElement eleOutDocCommonCodeList=commonCodeOutDoc.getDocumentElement();

		LOGGER.endTimer("GCStoreLoadAPI.fetchDGList");
		return eleOutDocCommonCodeList.getElementsByTagName(GCConstants.COMMON_CODE);
	}

	/** This method is used to publish message to internal queue for latest availability 
	 * by invoking GCPublishMsgToInternalQForAvailabilityService.
	 * 
	 * @param env
	 * @param sShipNode
	 * @param sExtnSourceFromStoreInFeed
	 */
	private void publishMessageForLatestAvailability(YFSEnvironment env, String sShipNode, String sExtnSourceFromStoreInFeed){

		LOGGER.beginTimer("GCStoreLoadAPI.publishMessageForLatestAvailability");
		LOGGER.verbose("Class: GCStoreLoadlAPI Method: publishMessageForLatestAvailability BEGIN");
		YFCDocument publishMessageForLatestAvailabilityinDoc = YFCDocument.createDocument("InventoryNodeControl");  
		YFCElement eleInventoryNodeControl = publishMessageForLatestAvailabilityinDoc.getDocumentElement(); 
		eleInventoryNodeControl.setAttribute(GCConstants.ORGANIZATION_CODE,GCConstants.GCI);
		eleInventoryNodeControl.setAttribute(GCConstants.NODE,sShipNode);
		eleInventoryNodeControl.setAttribute("ExtnIsSourcingAllowedFromNode",sExtnSourceFromStoreInFeed);   

		if (LOGGER.isVerboseEnabled()){
			LOGGER.verbose("Input to Internal Queue :" + publishMessageForLatestAvailabilityinDoc.getString());
		}

		//Invoking API
		GCCommonUtil.invokeService(env,GCConstants.GC_PUBLISH_MSG_TO_INTERNAL_Q_FOR_AVAILABILITY_SERVICE,publishMessageForLatestAvailabilityinDoc);
		LOGGER.endTimer("GCStoreLoadAPI.publishMessageForLatestAvailability");	
	}
  //
  /**
   * This method checks if a price list header exist for the store being created/updated. If not,
   * price list header is created.
   *
   * @param env
   * @param sOrganizationCode
   */
  private void createPriceListHeaderForStore(YFSEnvironment env, String sOrganizationCode) {

    LOGGER.beginTimer("GCStoreLoadAPI.createPriceListHeaderForStore");
    LOGGER.verbose("Class: GCStoreLoadlAPI Method: createPriceListHeaderForStore BEGIN");

    YFCDocument getPriceListHeaderListInDoc =
        YFCDocument.getDocumentFor("<PricelistHeader PricelistName='' SellerOrganizationCode=''/>");
    String sPriceListName = GCConstants.STORE_PRICELIST_NAME;
    getPriceListHeaderListInDoc.getDocumentElement().setAttribute("PricelistName", sPriceListName);
    getPriceListHeaderListInDoc.getDocumentElement().setAttribute(GCConstants.SELLER_ORGANIZATION_CODE,
        sOrganizationCode);

    YFCDocument getPriceListHeaderListOutDoc =
        GCCommonUtil.invokeAPI(env, "getPricelistHeaderList",
            getPriceListHeaderListInDoc, YFCDocument.getDocumentFor(""));

    if(!getPriceListHeaderListOutDoc.getDocumentElement().hasChildNodes()){
      LOGGER.verbose("No price list found. A new pricelist is to be created");

      YFCDocument managePriceListHeaderInDoc = YFCDocument.getDocumentFor("<PricelistHeader/>");
      String sDescription = "STORE " + sOrganizationCode + " PRICES";

      Calendar calDate = Calendar.getInstance();
      SimpleDateFormat dateFormat = new SimpleDateFormat(GCConstants.DATE_TIME_FORMAT);
      String sStartDateActive = dateFormat.format(calDate.getTime());

      String sEndDateActive = YFCDate.HIGH_DATE.getString(GCConstants.DATE_TIME_FORMAT);
      YFCElement eleRoot = managePriceListHeaderInDoc.getDocumentElement();
      eleRoot.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GC);
      eleRoot.setAttribute(GCConstants.OPERATION, "Manage");
      eleRoot.setAttribute("PricingStatus", "ACTIVE");
      eleRoot.setAttribute(GCConstants.CURRENCY, GCConstants.USD);
      eleRoot.setAttribute("PricelistName", sPriceListName);
      eleRoot.setAttribute(GCConstants.DESCRIPTION, sDescription);
      eleRoot.setAttribute(GCConstants.START_DATE_ACTIVE, sStartDateActive);
      eleRoot.setAttribute(GCConstants.END_DATE_ACTIVE, sEndDateActive);
      eleRoot.setAttribute(GCConstants.SELLER_ORGANIZATION_CODE, sOrganizationCode);

      GCCommonUtil.invokeAPI(env, "managePricelistHeader", managePriceListHeaderInDoc, YFCDocument.getDocumentFor(""));

      LOGGER.info(
          "Creating input to assign pricelist to GC enterprise and assign the store as seller on each specific pricelist");

      YFCDocument managePricelistAssignmentInDoc =
          YFCDocument.getDocumentFor("<PricelistAssignment/>");
      YFCElement eleManagePricelistAssignmentRootDoc = managePricelistAssignmentInDoc.getDocumentElement();
      eleManagePricelistAssignmentRootDoc.setAttribute(GCConstants.ENTERPRISE_CODE, GCConstants.GC);
      eleManagePricelistAssignmentRootDoc.setAttribute(GCConstants.SELLER_ORGANIZATION_CODE, sOrganizationCode);
      YFCElement elePricelistHeader =
          eleManagePricelistAssignmentRootDoc.createChild("PricelistHeader");
      elePricelistHeader.setAttribute("PricelistName", sPriceListName);
      elePricelistHeader.setAttribute(GCConstants.SELLER_ORGANIZATION_CODE, sOrganizationCode);

      YFCDocument managePricelistSellerAssignmentInDoc = YFCDocument.getDocumentFor("<PricelistSellerAssignment/>");
      YFCElement elemanagePricelistSellerAssignmentRootDoc = managePricelistSellerAssignmentInDoc.getDocumentElement();
      elemanagePricelistSellerAssignmentRootDoc.setAttribute(GCConstants.ENTERPRISE_CODE, GCConstants.GC);
      elemanagePricelistSellerAssignmentRootDoc.setAttribute(GCConstants.SELLER_ORGANIZATION_CODE, sOrganizationCode);
      YFCElement elePricelistHeaderForSeller = elemanagePricelistSellerAssignmentRootDoc.createChild("PricelistHeader");
      elePricelistHeaderForSeller.setAttribute("PricelistName", sPriceListName);
      elePricelistHeaderForSeller.setAttribute(GCConstants.SELLER_ORGANIZATION_CODE, sOrganizationCode);

      GCCommonUtil.invokeAPI(env, GCConstants.MANAGE_PRICELIST_ASSIGNMENT, managePricelistAssignmentInDoc,
          YFCDocument.getDocumentFor(""));
      GCCommonUtil.invokeAPI(env, GCConstants.MANAGE_PRICELIST_SELLER_ASSIGNMENT, managePricelistSellerAssignmentInDoc,
          YFCDocument.getDocumentFor(""));

      LOGGER.endTimer("GCStoreLoadAPI.createPriceListHeaderForStore");
    }
  }

  public static void stampPaymentTypeOnStore(YFSEnvironment env, YFCDocument inputDoc) {

    YFCElement eleRoot = inputDoc.getDocumentElement();
    eleRoot.setAttribute("DefaultPaymentRuleId", "GCDefaultPayRule");
    eleRoot.setAttribute("PaymentProcessingReqd", GCConstants.YES);


    String sOrganizationCode = eleRoot.getAttribute(GCConstants.ORGANIZATION_CODE);
    YFCElement elePaymentTypeList = eleRoot.createChild("PaymentTypeList");
    Document docMPOSPaymentTypes = GCCommonUtil.getCommonCodeListByType(env, "GCMPOSPaymentTypes", "Default");
    YFCDocument yfcCommonCodeDoc = YFCDocument.getDocumentFor(docMPOSPaymentTypes);
    YFCElement eleOutDocCommonCodeList = yfcCommonCodeDoc.getDocumentElement();
    YFCNodeList<YFCElement> paymentTypeList = eleOutDocCommonCodeList.getElementsByTagName(GCConstants.COMMON_CODE);


    Set<String> mPOSPaymentTypes = new HashSet<String>();
    for (YFCElement paymentTpe : paymentTypeList) {
      String sCodeValue = paymentTpe.getAttribute(GCConstants.CODE_VALUE);
      mPOSPaymentTypes.add(sCodeValue);
    }

    LOGGER.verbose("List of PaymentTypes in SET:" + mPOSPaymentTypes);
    YFCDocument getPaymentTypeList = YFCDocument.getDocumentFor("<Payment OrganizationCode='GC_WEB'/>");
    YFCDocument getPaymentTypeListTemp = YFCDocument.getDocumentFor(
        "<PaymentTypeList><PaymentType AllowAuthorizationsToExceedSettlementRequest='' AuthorizationExpirationHours='' "
            + "AuthorizationReversalHours='' AuthorizationReversalStrategy='' CashBackLimit='' ChargeConsolAllowed='' ChargeInsteadOfAuth='' ChargeSequence='' ChargeUpToAvailable='' "
            + "DefaultForReturn='' HasAltRefundConstraint='' MerchantId='' MinWaitTimeForConsol='' NewRefundPaymentType='' NoProcessingRequired='' PartialReversalSupported='' "
            + "PaymentTemplateType='' PaymentType='' PaymentTypeDescription='' PaymentTypeGroup='' RefundConstraintOperator='' RefundConstraintPaymentType='' "
            + "RefundConstraintValue='' RefundItemID='' RefundItemUom='' RefundRequiresFulfillment='' RefundSameAccount='' RefundSequence='' ValidForReturn='' Voidable='' "
            + "ZeroAmountAuthorizationSupported=''></PaymentType></PaymentTypeList>");

    // Stamping payment types which are required to be configured for stores.
    YFCDocument getPaymentListOutput =
        GCCommonUtil.invokeAPI(env, "getPaymentTypeList", getPaymentTypeList, getPaymentTypeListTemp);
    YFCElement eleRootPaymentList = getPaymentListOutput.getDocumentElement();
    YFCNodeList<YFCElement> nlPaymentTypeList = eleRootPaymentList.getElementsByTagName(GCConstants.PAYMENT_TYPE);
    for (YFCElement elePaymentType : nlPaymentTypeList) {
      String sPaymentType = elePaymentType.getAttribute(GCConstants.PAYMENT_TYPE);
      if (mPOSPaymentTypes.contains(sPaymentType)) {
        YFCElement paymentType = (YFCElement) elePaymentTypeList.importNode(elePaymentType);
        paymentType.setAttribute(GCConstants.ORGANIZATION_CODE, sOrganizationCode);
      }
    }

    YFCDocument getPaymentRuleList =
        YFCDocument.getDocumentFor("<PaymentRule OrganizationCode='" + sOrganizationCode + "'/>");
    YFCDocument getPaymentRuleListOutput =
        GCCommonUtil.invokeAPI(env, "getPaymentRuleList", getPaymentRuleList, YFCDocument.getDocumentFor(""));

    if (!getPaymentRuleListOutput.getDocumentElement().hasChildNodes()) {

      YFCElement elePaymentRuleList = eleRoot.createChild("PaymentRuleList");
      YFCElement elePaymentRule = elePaymentRuleList.createChild("PaymentRule");
      elePaymentRule.setAttribute("InterfaceTime", "AT_COLLECT");
      elePaymentRule.setAttribute("OrganizationCode", sOrganizationCode);
      elePaymentRule.setAttribute("PaymentRuleId", "GCDefaultPayRule");

    }
  }

}

