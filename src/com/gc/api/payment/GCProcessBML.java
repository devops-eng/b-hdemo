/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: This class will handle processing of BML payment type.
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0               03/03/2015        Mittal, Yashu      GCSTORE-88: Design - Design - PayPal Credit (previously BML) Re-Authorization Processing
 *#################################################################################################################################################################
 */
package com.gc.api.payment;

import java.io.IOException;
import java.util.Date;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCPaymentUtils;
import com.gc.common.utils.GCWebServiceUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSExtnPaymentCollectionInputStruct;
import com.yantra.yfs.japi.YFSExtnPaymentCollectionOutputStruct;

/**
 * @author <a href="mailto:yashu.mittal@expicient.com">Yashu Mittal</a>
 */
public class GCProcessBML {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCProcessBML.class);

  /**
   * 
   * This method will be invoked from GCPayementExecutionOthersUE for orders wih BML as payment type.
   *
   * @param env : YFSEnvironement variable
   * @param inStruct : instruct
   * @param outstruct : outstruct to be updated.
   * @param getOrderListOpDoc : order details of order for which payment is to be processed.
   * @return
   * @throws IOException
   *
   */
  public YFSExtnPaymentCollectionOutputStruct processBMLAuthorization(YFSEnvironment env, YFSExtnPaymentCollectionInputStruct inStruct,
      YFSExtnPaymentCollectionOutputStruct outstruct, YFCDocument getOrderListOpDoc) throws IOException {
    LOGGER.beginTimer("GCProcessBML.processBMLAuthorization");
    LOGGER.verbose("Class: GCProcessBML Method: processBMLAuthorization START");
    

    /* COA PAYMENT PROCESSING Start */
    // Add logic to update for dummy auth for reverse auth call
    if (inStruct.requestAmount < 0) {
      outstruct = GCPaymentUtils.updateOutStructForAuthRev(inStruct, outstruct);
    } else {
    YFCDocument docBMLPaymentResponse = invokeBMLAuth(inStruct, getOrderListOpDoc);
    outstruct = processOutputForBMLAuth(env, docBMLPaymentResponse, outstruct, inStruct, getOrderListOpDoc);
    }
    /* COA PAYMENT PROCESSING End */
    LOGGER.verbose("Class: GCProcessBML Method: processBMLAuthorization END");
    LOGGER.endTimer("GCProcessBML.processBMLAuthorization");
    return outstruct;
  }

  /**
   * 
   * This method will invoke web-service for re-authrozation with Paymentech.
   * 
   * @param inStruct : instruct
   * @param getOrderListOpDoc : order details of order for which payment is to be processed.
   * @return
   * @throws IOException
   *
   */
  private YFCDocument invokeBMLAuth(YFSExtnPaymentCollectionInputStruct inStruct, YFCDocument getOrderListOpDoc) throws IOException {
    YFCDocument inDocBML = prepareIPDocForBMLAuth(inStruct, getOrderListOpDoc);
    GCWebServiceUtil obj = new GCWebServiceUtil();
    Document docBMLResponse = obj.invokeSOAPWebService(inDocBML.getDocument(), YFSSystem.getProperty(GCConstants.BML_WEB_SERVICE_URL), null);
    return YFCDocument.getDocumentFor(docBMLResponse);
  }

  /**
   * 
   * This method will prepare input doc for re-auth.
   *
   * @param inStruct : instruct.
   * @param getOrderListOpDoc : order details of order for which payment is to be processed.
   * @return
   *
   */
  private YFCDocument prepareIPDocForBMLAuth(YFSExtnPaymentCollectionInputStruct inStruct, YFCDocument getOrderListOpDoc) {
    LOGGER.beginTimer("GCProcessBML.prepareIPDocForBMLAuth");
    LOGGER.verbose("Class: GCProcessBML Method: prepareIPDocForBMLAuth START");

    Document docBML = GCCommonUtil.getDocumentFromPath("/global/template/api/extn_BMLRequestTemplate.xml");

    if(LOGGER.isDebugEnabled()){
      LOGGER.debug(" getOrderListOut is "
          + GCXMLUtil.getXMLString(getOrderListOpDoc.getDocument()));
    }
    YFCDocument inDocBML = YFCDocument.getDocumentFor(docBML);
    YFCElement eleOrder = getOrderListOpDoc.getElementsByTagName(GCConstants.ORDER).item(0);
    YFCElement eleExtn = eleOrder.getChildElement(GCConstants.EXTN);
    String sExtnSourceOrderNo = eleExtn.getAttribute(GCConstants.EXTN_SOURCE_ORDER_NO);
    LOGGER.verbose("ExtnSourceOrderNo is :" + sExtnSourceOrderNo);
    YFCElement eleRootBML = inDocBML.getDocumentElement();
    YFCElement eleNewOrderRequest = eleRootBML.getElementsByTagName("pay:newOrderRequest").item(0);
    if (!YFCCommon.isVoid(eleNewOrderRequest)) {
      updateAuthReqDoc(inStruct, inDocBML, sExtnSourceOrderNo, eleNewOrderRequest);
      appendShippingDetails(inStruct, inDocBML, eleNewOrderRequest);
    }
    LOGGER.verbose("Class: GCProcessBML Method: prepareIPDocForBMLAuth END");
    LOGGER.endTimer("GCProcessBML.prepareIPDocForBMLAuth");
    return inDocBML;
  }

  /**
   * 
   * This method will stamp the the required attributes on re-auth request xml to be sent to paymentech.
   *
   * @param inStruct : instruct
   * @param inDocBML : re-auth request doc sent to paymentech.
   * @param sExtnSourceOrderNo : ATG orderno received in order details.
   * @param eleNewOrderRequest : newOrderRequest element in request doc sent to paymentech.
   *
   */
  private void updateAuthReqDoc(YFSExtnPaymentCollectionInputStruct inStruct, YFCDocument inDocBML, String sExtnSourceOrderNo, YFCElement eleNewOrderRequest) {
    LOGGER.beginTimer("GCProcessBML.updateAuthReqDoc");
    LOGGER.verbose("Class: GCProcessBML Method: updateAuthReqDoc START");

    YFCElement eleorbitalConnectionUsername = inDocBML.createElement("pay:orbitalConnectionUsername");
    eleorbitalConnectionUsername.setNodeValue(YFSSystem.getProperty(GCConstants.ORBITAL_CONNECTION_USERNAME));
    eleNewOrderRequest.appendChild(eleorbitalConnectionUsername);

    YFCElement eleorbitalConnectionPassword = inDocBML.createElement("pay:orbitalConnectionPassword");
    eleorbitalConnectionPassword.setNodeValue(YFSSystem.getProperty(GCConstants.ORBITAL_CONNECTION_PASSWORD));
    eleNewOrderRequest.appendChild(eleorbitalConnectionPassword);

    YFCElement eleIndustryType = inDocBML.createElement("pay:industryType");
    eleIndustryType.setNodeValue(GCConstants.INDUSTRY_TYPE);
    eleNewOrderRequest.appendChild(eleIndustryType);

    YFCElement eleTransType = inDocBML.createElement("pay:transType");
    eleTransType.setNodeValue(GCConstants.AUTHORIZATION_TRANSACTION_TYPE_CODE);
    eleNewOrderRequest.appendChild(eleTransType);

    YFCElement eleBin = inDocBML.createElement("pay:bin");
    eleBin.setNodeValue(GCConstants.BIN);
    eleNewOrderRequest.appendChild(eleBin);

    YFCElement eleMerchantID = inDocBML.createElement("pay:merchantID");
    eleMerchantID.setNodeValue(GCConstants.MERCHANT_ID);
    eleNewOrderRequest.appendChild(eleMerchantID);

    YFCElement eleCCAccountNum = inDocBML.createElement("pay:ccAccountNum");
    eleCCAccountNum.setNodeValue(inStruct.paymentReference1);
    eleNewOrderRequest.appendChild(eleCCAccountNum);

    YFCElement eleTerminalID = inDocBML.createElement("pay:terminalID");
    eleTerminalID.setNodeValue(GCConstants.TERMINAL_ID);
    eleNewOrderRequest.appendChild(eleTerminalID);

    YFCElement eleCardBrand = inDocBML.createElement("pay:cardBrand");
    eleCardBrand.setNodeValue(GCConstants.CARD_BRAND);
    eleNewOrderRequest.appendChild(eleCardBrand);

    YFCElement eleOrderID = inDocBML.createElement("pay:orderID");
    eleOrderID.setNodeValue(sExtnSourceOrderNo);
    eleNewOrderRequest.appendChild(eleOrderID);

    YFCElement eleAmount = inDocBML.createElement("pay:amount");
    eleAmount.setNodeValue(Math.round(inStruct.requestAmount * 100));
    eleNewOrderRequest.appendChild(eleAmount);

    YFCElement eletaxInd = inDocBML.createElement("pay:taxInd");
    eletaxInd.setNodeValue(GCConstants.TAX_ID);
    eleNewOrderRequest.appendChild(eletaxInd);

    YFCElement elebmlItemCategory = inDocBML.createElement("pay:bmlItemCategory");
    elebmlItemCategory.setNodeValue(GCConstants.BML_ITEM_CATEGORY);
    eleNewOrderRequest.appendChild(elebmlItemCategory);
    
    YFCElement bmlTNCVersion = inDocBML.createElement("pay:bmlTNCVersion");
    bmlTNCVersion.setNodeValue(GCConstants.BML_TNC_VERSION);
    eleNewOrderRequest.appendChild(bmlTNCVersion);
    
    YFCElement bmlProductDeliveryType = inDocBML.createElement("pay:bmlProductDeliveryType");
    bmlProductDeliveryType.setNodeValue(GCConstants.BML_PRODUCT_DELIVERY_TYPE);
    eleNewOrderRequest.appendChild(bmlProductDeliveryType);


    YFCElement eleRetryTrace = inDocBML.createElement("pay:retryTrace");
    String sOrderNo = inStruct.orderNo;
    // GCSTORE-2952
    String sRetryTrace = sOrderNo.replaceAll(GCConstants.AUTH_RETRY_TRACE_CODE, GCConstants.ONE);
    eleRetryTrace.setNodeValue(sRetryTrace);
    eleNewOrderRequest.appendChild(eleRetryTrace);


    LOGGER.verbose("Class: GCProcessBML Method: updateAuthReqDoc END");
    LOGGER.endTimer("GCProcessBML.updateAuthReqDoc");
  }

  /**
   * 
   * This method will stamp the shipping details on re-auth request xml to be sent to paymentech.
   *
   * @param inStruct : instruct.
   * @param inDocBML : re-auth request doc sent to paymentech.
   * @param eleNewOrderRequest : newOrderRequest element in request doc sent to paymentech.
   *
   */
  private void appendShippingDetails(YFSExtnPaymentCollectionInputStruct inStruct, YFCDocument inDocBML, YFCElement eleNewOrderRequest) {
    LOGGER.beginTimer("GCProcessBML.appendShippingDetails");
    LOGGER.verbose("Class: GCProcessBML Method: appendShippingDetails START");

    YFCElement eleAvsZip = inDocBML.createElement("pay:avsZip");
    eleAvsZip.setNodeValue(inStruct.billToZipCode);
    eleNewOrderRequest.appendChild(eleAvsZip);

    YFCElement eleAvsAddress1 = inDocBML.createElement("pay:avsAddress1");
    eleAvsAddress1.setNodeValue(inStruct.billToAddressLine1);
    eleNewOrderRequest.appendChild(eleAvsAddress1);

    YFCElement eleAvsCity = inDocBML.createElement("pay:avsCity");
    eleAvsCity.setNodeValue(inStruct.billToCity);
    eleNewOrderRequest.appendChild(eleAvsCity);

    YFCElement eleAvsState = inDocBML.createElement("pay:avsState");
    eleAvsState.setNodeValue(inStruct.billToState);
    eleNewOrderRequest.appendChild(eleAvsState);

    YFCElement eleAvsPhoneNum = inDocBML.createElement("pay:avsPhoneNum");
    eleAvsPhoneNum.setNodeValue(inStruct.billToDayPhone);
    eleNewOrderRequest.appendChild(eleAvsPhoneNum);

    YFCElement elebmlCustomerEmail = inDocBML.createElement("pay:bmlCustomerEmail");
    elebmlCustomerEmail.setNodeValue(inStruct.billToEmailId);
    eleNewOrderRequest.appendChild(elebmlCustomerEmail);

    YFCElement eleAvsName = inDocBML.createElement("pay:avsName");
    eleAvsName.setNodeValue(inStruct.billToFirstName + " " + inStruct.billToLastName);
    eleNewOrderRequest.appendChild(eleAvsName);

    YFCElement eleAvsCountryCode = inDocBML.createElement("pay:avsCountryCode");
    eleAvsCountryCode.setNodeValue(inStruct.billToCountry);
    eleNewOrderRequest.appendChild(eleAvsCountryCode);


    LOGGER.verbose("Class: GCProcessBML Method: appendShippingDetails END");
    LOGGER.endTimer("GCProcessBML.appendShippingDetails");
  }

  /**
   * 
   * This method will process the response received from Paymentech and will re-authorize the order is succcess or raise
   * alert for failure response.
   *
   * @param env : YFSEnvironment variable.
   * @param docPaymentResponse : Re-authorization response sent by Paymentech.
   * @param outstruct : outstrcut to be updated.
   * @param inStruct : instruct
   * @param getOrderListOpDoc : order details of order for which payment is processed.
   * @return
   *
   */
  private YFSExtnPaymentCollectionOutputStruct processOutputForBMLAuth(YFSEnvironment env, YFCDocument docPaymentResponse,
      YFSExtnPaymentCollectionOutputStruct outstruct, YFSExtnPaymentCollectionInputStruct inStruct, YFCDocument getOrderListOpDoc) {
    LOGGER.beginTimer("GCProcessBML.processOutputForBMLAuth");
    LOGGER.verbose("Class: GCProcessBML Method: processOutputForBMLAuth START");

    if(LOGGER.isDebugEnabled()){
      LOGGER.debug(" BML Response  is "
          + GCXMLUtil.getXMLString(docPaymentResponse.getDocument()));
    }
    
    double dRequestAmount = inStruct.requestAmount;
    String sAuthExpDate = GCPaymentUtils.getAuthExpiryDate(env, inStruct);
    
    YFCElement eleFaultCode = docPaymentResponse.getElementsByTagName("faultCode").item(0);
    
    if (YFCCommon.isVoid(docPaymentResponse)) {
      GCPaymentUtils.raiseException(GCErrorConstants.GCE00088, GCErrorConstants.BML_COMMUNICATION_FAILURE, inStruct.orderNo);
      
    } else if (!YFCCommon.isVoid(eleFaultCode)){
      LOGGER.verbose("Fault String is received in BML response. This is a hard decline");
      GCPaymentUtils.processHardDeclinedResponse(env, inStruct, outstruct, getOrderListOpDoc);
      
    } else {
      String sApprovalStatus = docPaymentResponse.getElementsByTagName(GCConstants.BML_APPROVAL_STATUS).item(0).getNodeValue(); 

      if (GCConstants.ZERO.equals(sApprovalStatus)) {
        LOGGER.verbose("AprrovalStatus is :" + sApprovalStatus + " .This is hard decline. Raising hard decline alert and suspend payment");
        GCPaymentUtils.processHardDeclinedResponse(env, inStruct, outstruct, getOrderListOpDoc);
      } else if (GCConstants.ONE.equals(sApprovalStatus)) {
        LOGGER.verbose("AprrovalStatus is :" + sApprovalStatus + " . This is success");
        updateBMLOutStruct(dRequestAmount, sAuthExpDate, outstruct, docPaymentResponse);
      } else if (GCConstants.TWO.equals(sApprovalStatus)) {
        LOGGER.verbose("AprrovalStatus is :" + sApprovalStatus + " . This is retry.");
        GCPaymentUtils.updateAuthRetryCount(env, inStruct, getOrderListOpDoc);
        outstruct.retryFlag = GCConstants.YES;
        outstruct.collectionDate = GCDateUtils.getCollectionDate();

      } else if (YFCCommon.isVoid(sApprovalStatus)) {

        String sProcStatus = docPaymentResponse.getElementsByTagName(GCConstants.BML_PROC_STATUS).item(0).getNodeValue();
        YFCDocument getCommonCodeListOut = GCCommonUtil.getCommonCodeListByTypeAndValue(env, GCConstants.BML_COMMON_CODE, GCConstants.GC, sProcStatus);
       
        if (GCConstants.ZERO.equals(sProcStatus)) {
          LOGGER.verbose("ProcStatus is :" + sProcStatus + " . This is success.");
          updateBMLOutStruct(dRequestAmount, sAuthExpDate, outstruct, docPaymentResponse);
        } else if (getCommonCodeListOut.getDocumentElement().hasChildNodes()) {
          GCPaymentUtils.updateAuthRetryCount(env, inStruct, getOrderListOpDoc);
          outstruct.retryFlag = GCConstants.YES;
          outstruct.collectionDate = GCDateUtils.getCollectionDate();
        } else {
          GCPaymentUtils.processHardDeclinedResponse(env, inStruct, outstruct, getOrderListOpDoc);
        }
      }
    }
    LOGGER.verbose("Class: GCProcessBML Method: processOutputForBMLAuth END");
    LOGGER.endTimer("GCProcessBML.processOutputForBMLAuth");
    return outstruct;
  }

  /**
   * 
   * This method will update the outstruct in case Re-authorization is successful.
   *
   * @param dRequestAmount : Amount for which re-authorization is requested.
   * @param sAuthExpDate : New Authorization expiration date to be stamped on outstruct.
   * @param outstruct : outstrcut to be updated.
   * @param docPaymentResponse : Re-authorization response sent by Paymentech.
   *
   */
  private void updateBMLOutStruct(double dRequestAmount, String sAuthExpDate, YFSExtnPaymentCollectionOutputStruct outstruct, YFCDocument docPaymentResponse) {
    LOGGER.beginTimer("GCProcessBML.updateBMLOutStruct");
    LOGGER.verbose("Class: GCProcessBML Method: updateBMLOutStruct START");
    Date currentDate = new Date();
    outstruct.tranRequestTime = GCDateUtils.convertDate(currentDate);
    outstruct.authTime = "";
    outstruct.authorizationId = docPaymentResponse.getElementsByTagName(GCConstants.BML_RESPONSE_AUTHORIZATION_CODE).item(0).getNodeValue();
    outstruct.tranType = GCConstants.AUTHORIZATION;
    outstruct.authorizationExpirationDate = sAuthExpDate;
    outstruct.authReturnCode = docPaymentResponse.getElementsByTagName(GCConstants.BML_APPROVAL_STATUS).item(0).getNodeValue();
    outstruct.authReturnMessage = "";
    outstruct.authorizationAmount = dRequestAmount;
    outstruct.holdOrderAndRaiseEvent = false;
    outstruct.suspendPayment = GCConstants.NO;
    outstruct.tranAmount = dRequestAmount;
    outstruct.retryFlag = GCConstants.NO;
    outstruct.PaymentReference2 = docPaymentResponse.getElementsByTagName("pay:txRefNum").item(0).getNodeValue();
    LOGGER.verbose("Class: GCProcessBML Method: updateBMLOutStruct END");
    LOGGER.endTimer("GCProcessBML.updateBMLOutStruct");
  }
  
  
 //BML Charging Code Starts
  
  /**
	 * 
	 * This method will be invoked from GCPayementExecutionOthersUE for charging the orders with BML as payment type.
	 *
	 * @param env : YFSEnvironement variable
	 * @param inStruct : instruct
	 * @param outstruct : outstruct to be updated.
	 * @param getOrderListOpDoc : order details of order for which payment is to be processed.
	 * @return
	 * @throws IOException
	 *
	 */
  public YFSExtnPaymentCollectionOutputStruct processBMLCharge(
			YFSEnvironment env, YFSExtnPaymentCollectionInputStruct inStruct,
			YFSExtnPaymentCollectionOutputStruct outstruct,
			YFCDocument getOrderListOpDoc) throws IOException {
		LOGGER.beginTimer("GCProcessBML.processBMLCharge");
		LOGGER.verbose("Class: GCProcessBML Method: processBMLCharge START");

		double dRequestAmount = inStruct.requestAmount;
	    // check request amount to find if this entry is for charging or refund.
	    if (dRequestAmount > 0) {
		YFCDocument docBMLPaymentResponse = invokeBMLCharge(inStruct, getOrderListOpDoc);
		processBMLChargeResponse(env, docBMLPaymentResponse, outstruct, inStruct, getOrderListOpDoc);
		
	    } else if (dRequestAmount < 0) {
		      processBMLRefund(env, inStruct, outstruct, getOrderListOpDoc);
		    }
	    
		LOGGER.verbose("Class: GCProcessBML Method: processCharge END");
		LOGGER.endTimer("GCProcessBML.processBMLCharge");
		return outstruct;
	}

	/**
	 * 
	 * This method will invoke web-service for capture with Paymentech.
	 * 
	 * @param inStruct : instruct
	 * @param getOrderListOpDoc : order details of order for which payment is to be processed.
	 * @return
	 * @throws IOException
	 *
	 */
	private YFCDocument invokeBMLCharge(
			YFSExtnPaymentCollectionInputStruct inStruct,
			YFCDocument getOrderListOpDoc) throws IOException {
		YFCDocument inDocBML = prepareIPDocForBMLCharge(inStruct,
				getOrderListOpDoc);
		GCWebServiceUtil obj = new GCWebServiceUtil();
		Document docBMLResponse = obj.invokeSOAPWebService(inDocBML.getDocument(), YFSSystem.getProperty(GCConstants.BML_WEB_SERVICE_URL), null);
		return YFCDocument.getDocumentFor(docBMLResponse);
	}

	/**
	 * 
	 * This method will prepare input doc for charge request.
	 *
	 * @param inStruct : instruct.
	 * @param getOrderListOpDoc : order details of order for which payment is to be processed.
	 * @return
	 *
	 */
	private YFCDocument prepareIPDocForBMLCharge(
			YFSExtnPaymentCollectionInputStruct inStruct,
			YFCDocument getOrderListOpDoc) {
		LOGGER.beginTimer("GCProcessBML.prepareIPDocForBMLCharge");
		LOGGER
				.verbose("Class: GCProcessBML Method: prepareIPDocForBMLCharge START");

		Document docBML = GCCommonUtil.getDocumentFromPath("/global/template/api/extn_BMLChargeRequestTemplate.xml");

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(" getOrderListOut is "
					+ GCXMLUtil.getXMLString(getOrderListOpDoc.getDocument()));
		}
		YFCDocument inDocBML = YFCDocument.getDocumentFor(docBML);
		YFCElement eleOrder = getOrderListOpDoc.getElementsByTagName(
				GCConstants.ORDER).item(0);
		YFCElement eleExtn = eleOrder.getChildElement(GCConstants.EXTN);
		String sExtnSourceOrderNo = eleExtn.getAttribute(GCConstants.EXTN_SOURCE_ORDER_NO);
		LOGGER.verbose("ExtnSourceOrderNo is :" + sExtnSourceOrderNo);

		String txRefNum = inStruct.paymentReference2;
		YFCElement eleRootBML = inDocBML.getDocumentElement();
		YFCElement eleMFCRequest = eleRootBML.getElementsByTagName("pay:mfcRequest").item(0);
		if (!YFCCommon.isVoid(eleMFCRequest)) {
			updateChargeReqDoc(inStruct, inDocBML, sExtnSourceOrderNo,eleMFCRequest, txRefNum);

		}
		LOGGER
				.verbose("Class: GCProcessBML Method: prepareIPDocForBMLCharge END");
		LOGGER.endTimer("GCProcessBML.prepareIPDocForBMLCharge");
		return inDocBML;
	}

	/**
	 * 
	 * This method will stamp the the required attributes on charge request xml to be sent to paymentech.
	 *
	 * @param inStruct : instruct
	 * @param inDocBML : charge request doc sent to paymentech.
	 * @param sExtnSourceOrderNo : ATG orderno received in order details.
	 * @param eleNewOrderRequest : newOrderRequest element in request doc sent to paymentech.
	 *
	 */
	private void updateChargeReqDoc(
			YFSExtnPaymentCollectionInputStruct inStruct, YFCDocument inDocBML,
			String sExtnSourceOrderNo, YFCElement eleMFCRequest, String txRefNum) {
		LOGGER.beginTimer("GCProcessBML.updateAuthReqDoc");
		LOGGER.verbose("Class: GCProcessBML Method: updateAuthReqDoc START");

		YFCElement eleorbitalConnectionUsername = inDocBML
				.createElement("pay:orbitalConnectionUsername");
		eleorbitalConnectionUsername.setNodeValue(YFSSystem
				.getProperty(GCConstants.ORBITAL_CONNECTION_USERNAME));
		eleMFCRequest.appendChild(eleorbitalConnectionUsername);

		YFCElement eleorbitalConnectionPassword = inDocBML
				.createElement("pay:orbitalConnectionPassword");
		eleorbitalConnectionPassword.setNodeValue(YFSSystem
				.getProperty(GCConstants.ORBITAL_CONNECTION_PASSWORD));
		eleMFCRequest.appendChild(eleorbitalConnectionPassword);

		YFCElement eleOrderID = inDocBML.createElement("pay:orderID");
		eleOrderID.setNodeValue(sExtnSourceOrderNo);
		eleMFCRequest.appendChild(eleOrderID);

		YFCElement eleAmount = inDocBML.createElement("pay:amount");
		eleAmount.setNodeValue(Math.round(inStruct.requestAmount * 100));
		eleMFCRequest.appendChild(eleAmount);

		YFCElement eleBin = inDocBML.createElement("pay:bin");
		eleBin.setNodeValue(GCConstants.BIN);
		eleMFCRequest.appendChild(eleBin);

		YFCElement eleMerchantID = inDocBML.createElement("pay:merchantID");
		eleMerchantID.setNodeValue(GCConstants.MERCHANT_ID);
		eleMFCRequest.appendChild(eleMerchantID);

		YFCElement eleTerminalID = inDocBML.createElement("pay:terminalID");
		eleTerminalID.setNodeValue(GCConstants.TERMINAL_ID);
		eleMFCRequest.appendChild(eleTerminalID);

		YFCElement eletxRefNum = inDocBML.createElement("pay:txRefNum");
		eletxRefNum.setNodeValue(txRefNum);
		eleMFCRequest.appendChild(eletxRefNum);
		
		// Fix for GCSTORE-4933
		YFCElement eleVersion = inDocBML.createElement("pay:version");
		eleVersion.setNodeValue("2.8");
		eleMFCRequest.appendChild(eleVersion);

		LOGGER.verbose("Class: GCProcessBML Method: updateAuthReqDoc END");
		LOGGER.endTimer("GCProcessBML.updateAuthReqDoc");
	}

	/**
	 *
	 * This method process response received from Paymentech API.
	 *
	 * @param env : YFSEnvironment variable.
	 * @param doCaptureResponse : Response received from Paymentech.
	 * @param outStruct : outStruct containing details of charge.
	 * @param inStruct : inStruct containing details of the payment method being processed.
	 * @param getOrderListOpDoc : Details of the order for which payment processing is being done.
	 *
	 */
	public void processBMLChargeResponse(YFSEnvironment env,
			YFCDocument bmlResponse,
			YFSExtnPaymentCollectionOutputStruct outStruct,
			YFSExtnPaymentCollectionInputStruct inStruct,
			YFCDocument getOrderListOpDoc) {

		LOGGER.beginTimer("GCProcessBML.processBMLChargeResponse");
		LOGGER
				.verbose("Class: GCProcessBML Method: processBMLChargeResponse START");

		YFCElement eleFaultCode = bmlResponse.getElementsByTagName("faultcode")
				.item(0);
		
		

		if (YFCCommon.isVoid(bmlResponse)) {
			// throw exception
			GCPaymentUtils.raiseException(GCErrorConstants.GCE00088,
					GCErrorConstants.BML_COMMUNICATION_FAILURE,
					inStruct.orderNo);

			outStruct.authorizationAmount = inStruct.requestAmount;
			outStruct.holdOrderAndRaiseEvent = false;
			outStruct.retryFlag = GCConstants.YES;
			outStruct.tranAmount = inStruct.requestAmount;
			outStruct.tranType = GCConstants.PAYMENT_STATUS_CHARGE;

			GCPaymentUtils.updateAuthRetryCount(env, inStruct,
					getOrderListOpDoc);

		} else if (!YFCCommon.isVoid(eleFaultCode)) {
			YFCElement eleFaultString = bmlResponse.getElementsByTagName("faultstring").item(0);
			String faultStringDesc=eleFaultString.getNodeValue();
			String faultCodeDesc=eleFaultCode.getNodeValue();
			
			outStruct.authTime = GCConstants.BLANK;
		    outStruct.authorizationId = GCConstants.DECLINED;
		    outStruct.tranType = GCConstants.PAYMENT_STATUS_AUTHORIZATION;
		    outStruct.authReturnMessage = faultStringDesc;
		    outStruct.authorizationAmount = GCConstants.ZEROAMOUNT;
		    outStruct.holdOrderAndRaiseEvent = GCConstants.FALSE;
		    outStruct.suspendPayment = GCConstants.YES;
		    outStruct.tranAmount = GCConstants.ZEROAMOUNT;
		    outStruct.retryFlag = GCConstants.NO;
		    
			GCPaymentUtils.raiseException(faultCodeDesc,
					faultStringDesc,
					inStruct.orderNo);
		    

		} else {
			outStruct.authorizationAmount = inStruct.requestAmount;
			outStruct.holdOrderAndRaiseEvent = false;
			outStruct.retryFlag = GCConstants.NO;
			outStruct.tranAmount = inStruct.requestAmount;
			outStruct.tranType = GCConstants.PAYMENT_STATUS_CHARGE;
		}

		LOGGER
				.verbose("Class: GCProcessBML Method: processBMLChargeResponse END");
		LOGGER.endTimer("GCProcessBML.processBMLChargeResponse");
	}
	
	// BML Refund Changes
	  /**
	 * 
	 * This method processes the refunded amount. Fetches the list of all
	 * the negative charged entries from the charge transaction table and
	 * validates the amount being refunded. Raises an alert in case of refund
	 * call failure.
	 * 
	 * @param env
	 * @param inStruct
	 * @param outStruct
	 * @param getOrderListOpDoc
	 * @return
	 * @throws IOException
	 * 
	 */
private YFSExtnPaymentCollectionOutputStruct processBMLRefund(YFSEnvironment env,
    YFSExtnPaymentCollectionInputStruct inStruct,
    YFSExtnPaymentCollectionOutputStruct outStruct, YFCDocument getOrderListOpDoc) throws IOException {
  LOGGER.beginTimer("processBMLRefund.processRefund");
  LOGGER.verbose("Class: GCProcessBML Method: processRefund START");

  
  String sPaymentKey = inStruct.paymentKey;
  String ordHdrKey=inStruct.orderHeaderKey;
  
  double dRefundedAmnt = 0.0;
  double settledAmnt = 0.0;
  
  //getChargeTransactionList call to get the settled amount and the refunded amount
  YFCDocument getChargeTransactionListIn =
      YFCDocument.getDocumentFor("<ChargeTransactionDetail ChargeType='CHARGE' Status='CHECKED' OrderHeaderKey = '"+ordHdrKey+"'  PaymentKey = '"+sPaymentKey+"' />");
  YFCDocument getChargeTransactionListTemplate = YFCDocument.getDocumentFor("<ChargeTransactionDetails><ChargeTransactionDetail RequestAmount=''/></ChargeTransactionDetails>");
  
  YFCDocument getChargeTransactionListOut =
      GCCommonUtil.invokeAPI(env, GCConstants.GET_CHARGE_TRANSACTION_LIST_API, getChargeTransactionListIn, getChargeTransactionListTemplate);
  YFCNodeList<YFCElement> listChargeTransactionDetailList = getChargeTransactionListOut.getElementsByTagName(GCXmlLiterals.CHARGE_TRANSACTION_DETAIL);
       
    int chargeListLength = listChargeTransactionDetailList.getLength();
    
  for (int k = 0; k < chargeListLength; k++){
  	YFCElement eleChrgTranDet = listChargeTransactionDetailList.item(k);
      String strReqAmout = eleChrgTranDet.getAttribute("RequestAmount");
      double dReqAmnt = Double.parseDouble(strReqAmout);
      if(dReqAmnt < 0)
      {
      	dRefundedAmnt += (-1*dReqAmnt);
      }
      else {
      	settledAmnt += dReqAmnt;
      }

  }
      double dAmountEligibleForRefund = settledAmnt - dRefundedAmnt;
      double dBMLRefundAmount = inStruct.requestAmount;
      LOGGER.verbose("dAmountEligibleForRefund : " + dAmountEligibleForRefund + " dAmountToRefund : "
          + dBMLRefundAmount);
   
      if (dBMLRefundAmount > dAmountEligibleForRefund) {
      	dBMLRefundAmount = dAmountEligibleForRefund;
      } 
      LOGGER.verbose("dBMLRefundAmount : " + dBMLRefundAmount);
      
      // Invoke BML webservice url to refund the amount
      
      YFCDocument docBMLPaymentResponse = invokeBMLRefund(inStruct,
				getOrderListOpDoc, dBMLRefundAmount);
     
      
      YFCElement eleFaultCode = docBMLPaymentResponse.getElementsByTagName("faultcode").item(0);



if (YFCCommon.isVoid(docBMLPaymentResponse)) {
	// throw exception
	GCPaymentUtils.raiseException(GCErrorConstants.GCE00088,
			GCErrorConstants.BML_COMMUNICATION_FAILURE,
			inStruct.orderNo);

	outStruct.authorizationAmount = inStruct.requestAmount;
	outStruct.holdOrderAndRaiseEvent = false;
	outStruct.retryFlag = GCConstants.YES;
	outStruct.tranAmount = inStruct.requestAmount;
	outStruct.tranType = GCConstants.PAYMENT_STATUS_CHARGE;

	GCPaymentUtils.updateAuthRetryCount(env, inStruct,
			getOrderListOpDoc);

} else if (!YFCCommon.isVoid(eleFaultCode)) {
	YFCElement eleFaultString = docBMLPaymentResponse.getElementsByTagName("faultstring").item(0);
	String faultStringDesc=eleFaultString.getNodeValue();
	String faultCodeDesc=eleFaultCode.getNodeValue();
	
	outStruct.authTime = GCConstants.BLANK;
  outStruct.authorizationId = GCConstants.DECLINED;
  outStruct.tranType = GCConstants.PAYMENT_STATUS_AUTHORIZATION;
  outStruct.authReturnMessage = faultStringDesc;
  outStruct.authorizationAmount = GCConstants.ZEROAMOUNT;
  outStruct.holdOrderAndRaiseEvent = GCConstants.FALSE;
  outStruct.suspendPayment = GCConstants.YES;
  outStruct.tranAmount = GCConstants.ZEROAMOUNT;
  outStruct.retryFlag = GCConstants.NO;
  
	GCPaymentUtils.raiseException(faultCodeDesc,
			faultStringDesc,
			inStruct.orderNo);
  

} else {
	outStruct.authorizationAmount = inStruct.requestAmount;
	outStruct.holdOrderAndRaiseEvent = false;
	outStruct.retryFlag = GCConstants.NO;
	outStruct.tranAmount = inStruct.requestAmount;
	outStruct.tranType = GCConstants.PAYMENT_STATUS_CHARGE;

}
LOGGER.verbose("Class: GCProcessPaypal Method: processRefund END");
LOGGER.endTimer("GCProcessPaypal.processRefund");
return outStruct;
}

/**
	 * 
	 * This method will invoke web-service for capture with Paymentech.
	 * 
	 * @param inStruct :
	 *            instruct
	 * @param getOrderListOpDoc :
	 *            order details of order for which payment is to be
	 *            processed.
	 * @return
	 * @throws IOException
	 * 
	 */
	private YFCDocument invokeBMLRefund(
			YFSExtnPaymentCollectionInputStruct inStruct,
			YFCDocument getOrderListOpDoc, double refundAmnt) throws IOException {
		YFCDocument inDocBML = prepareIPDocForBMLRefund(inStruct,
				getOrderListOpDoc,  refundAmnt);
		GCWebServiceUtil obj = new GCWebServiceUtil();
		Document docBMLResponse = obj.invokeSOAPWebService(inDocBML.getDocument(), YFSSystem.getProperty(GCConstants.BML_WEB_SERVICE_URL), null);
		return YFCDocument.getDocumentFor(docBMLResponse);
	}

	/**
	 * 
	 * This method will prepare input doc for charge request.
	 * 
	 * @param inStruct :
	 *            instruct.
	 * @param getOrderListOpDoc :
	 *            order details of order for which payment is to be
	 *            processed.
	 * @return
	 * 
	 */
	private YFCDocument prepareIPDocForBMLRefund(
			YFSExtnPaymentCollectionInputStruct inStruct,
			YFCDocument getOrderListOpDoc, double refundAmnt) {
		LOGGER.beginTimer("GCProcessBML.prepareIPDocForBMLRefund");
		LOGGER
				.verbose("Class: GCProcessBML Method: prepareIPDocForBMLRefund START");

		Document docBML = GCCommonUtil.getDocumentFromPath("/global/template/api/extn_BMLRequestTemplate.xml");

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(" getOrderListOut is "
					+ GCXMLUtil.getXMLString(getOrderListOpDoc.getDocument()));
		}
		YFCDocument inDocBML = YFCDocument.getDocumentFor(docBML);
		YFCElement eleOrder = getOrderListOpDoc.getElementsByTagName(
				GCConstants.ORDER).item(0);
		YFCElement eleExtn = eleOrder.getChildElement(GCConstants.EXTN);
		String sExtnSourceOrderNo = eleExtn.getAttribute(GCConstants.EXTN_SOURCE_ORDER_NO);
		LOGGER.verbose("ExtnSourceOrderNo is :" + sExtnSourceOrderNo);

		String txRefNum = inStruct.paymentReference2;
		YFCElement eleRootBML = inDocBML.getDocumentElement();
		YFCElement eleNewOrdRequest = eleRootBML.getElementsByTagName("pay:newOrderRequest").item(0);
		if (!YFCCommon.isVoid(eleNewOrdRequest)) {
			updateRefundReqDoc(refundAmnt, inDocBML, sExtnSourceOrderNo,eleNewOrdRequest, txRefNum);

		}
		LOGGER
				.verbose("Class: GCProcessBML Method: prepareIPDocForBMLRefund END");
		LOGGER.endTimer("GCProcessBML.prepareIPDocForBMLRefund");
		return inDocBML;
	}

	/**
	 * 
	 * This method will stamp the required attributes on charge request
	 * xml to be sent to paymentech.
	 * 
	 * @param inStruct :
	 *            instruct
	 * @param inDocBML :
	 *            charge request doc sent to paymentech.
	 * @param sExtnSourceOrderNo :
	 *            ATG orderno received in order details.
	 * @param eleNewOrderRequest :
	 *            newOrderRequest element in request doc sent to paymentech.
	 * 
	 */
	private void updateRefundReqDoc(
			double refundAmnt, YFCDocument inDocBML,
			String sExtnSourceOrderNo, YFCElement eleNewOrdRequest, String txRefNum) {
		LOGGER.beginTimer("GCProcessBML.updateRefundReqDoc");
		LOGGER.verbose("Class: GCProcessBML Method: updateRefundReqDoc START");

		YFCElement eleorbitalConnectionUsername = inDocBML
				.createElement("pay:orbitalConnectionUsername");
		eleorbitalConnectionUsername.setNodeValue(YFSSystem
				.getProperty(GCConstants.ORBITAL_CONNECTION_USERNAME));
		eleNewOrdRequest.appendChild(eleorbitalConnectionUsername);

		YFCElement eleorbitalConnectionPassword = inDocBML
				.createElement("pay:orbitalConnectionPassword");
		eleorbitalConnectionPassword.setNodeValue(YFSSystem
				.getProperty(GCConstants.ORBITAL_CONNECTION_PASSWORD));
		eleNewOrdRequest.appendChild(eleorbitalConnectionPassword);
		
		 YFCElement eleIndustryType = inDocBML.createElement("pay:industryType");
		 eleIndustryType.setNodeValue(GCConstants.INDUSTRY_TYPE);
		 eleNewOrdRequest.appendChild(eleIndustryType);

		 YFCElement eleTransType = inDocBML.createElement("pay:transType");
		 eleTransType.setNodeValue("R");
		 eleNewOrdRequest.appendChild(eleTransType);


		YFCElement eleOrderID = inDocBML.createElement("pay:orderID");
		eleOrderID.setNodeValue(sExtnSourceOrderNo);
		eleNewOrdRequest.appendChild(eleOrderID);

		YFCElement eleAmount = inDocBML.createElement("pay:amount");
		eleAmount.setNodeValue(Math.round(refundAmnt * 100 * -1));
		eleNewOrdRequest.appendChild(eleAmount);

		YFCElement eleBin = inDocBML.createElement("pay:bin");
		eleBin.setNodeValue(GCConstants.BIN);
		eleNewOrdRequest.appendChild(eleBin);

		YFCElement eleMerchantID = inDocBML.createElement("pay:merchantID");
		eleMerchantID.setNodeValue(GCConstants.MERCHANT_ID);
		eleNewOrdRequest.appendChild(eleMerchantID);

		YFCElement eleTerminalID = inDocBML.createElement("pay:terminalID");
		eleTerminalID.setNodeValue(GCConstants.TERMINAL_ID);
		eleNewOrdRequest.appendChild(eleTerminalID);

		YFCElement eletxRefNum = inDocBML.createElement("pay:txRefNum");
		eletxRefNum.setNodeValue(txRefNum);
		eleNewOrdRequest.appendChild(eletxRefNum);

		LOGGER.verbose("Class: GCProcessBML Method: updateRefundReqDoc END");
		LOGGER.endTimer("GCProcessBML.updateRefundReqDoc");
	}


}