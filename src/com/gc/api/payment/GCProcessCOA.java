/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *##############################################################################################################################################################
 *          OBJECTIVE: This class will process COA payment method authorization
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0               03/30/2015        Pande, Anuj                GCSTORE- 752: Design- COA Payment processing
 *#################################################################################################################################################################
 */

package com.gc.api.payment;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCPaymentUtils;
import com.gc.common.utils.GCWebServiceUtil;
import com.yantra.yfc.date.YTimestamp;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSExtnPaymentCollectionInputStruct;
import com.yantra.yfs.japi.YFSExtnPaymentCollectionOutputStruct;
/**
 * This class is invoked from Payment execution others UE when payment type is CREDIT_ON_ACCOUNT
 * and ChargeType is AUTHORIZATION
 * @author anuj.pande
 *
 */
public class GCProcessCOA {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCProcessCOA.class);

  /**
   * This method will process COA authorization process
   *
   * @param env
   * @param inStruct
   * @param outStruct
   * @param yfcDocGetOrderList
   * @return
   * @throws IOException
   */
  public YFSExtnPaymentCollectionOutputStruct processCOAAuthorization(YFSEnvironment env,
      YFSExtnPaymentCollectionInputStruct inStruct, YFSExtnPaymentCollectionOutputStruct outStruct,
      YFCDocument yfcDocGetOrderList) throws IOException {
    LOGGER.beginTimer("GCProcessCOA.processCOAAuthorization()");
    LOGGER.verbose("GCProcessCOA.processCOAAuthorization() - Start");

    String sProgId = env.getProgId();

    boolean bIsCallCenterProgId = false;
    boolean bIsAuthRev = false;

    LOGGER.verbose("inStruct RequestAmount: " +inStruct.requestAmount);
    LOGGER.verbose("inStruct PaymentType:" +inStruct.paymentType);
    LOGGER.verbose("sProg ID: "+sProgId);

    if (inStruct.requestAmount < 0) {
      bIsAuthRev = true;
    } else {
      env.setTxnObject(GCConstants.ENV_IS_REAUTH_REQUEST, GCConstants.YES);
    }

    if (YFCCommon.equals(sProgId, GCConstants.PAYMENT_CALL_CALL_CENTER)) {
      bIsCallCenterProgId = true;
    }

    LOGGER.verbose("bIsCallCenterProgId" + bIsCallCenterProgId);
    LOGGER.verbose("bIsAuthRev" + bIsAuthRev);

    if ((!bIsAuthRev) || (bIsAuthRev && YFCCommon.isVoid(env.getTxnObject(GCConstants.ENV_IS_REAUTH_REQUEST)))) {
      YFCDocument yfcDocCOAResponse = invokeCOAAuth(inStruct, bIsAuthRev, yfcDocGetOrderList);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("yfcDocCOAResponse" + yfcDocCOAResponse);
      }
      processOutputForCOAAuth(env, yfcDocCOAResponse, outStruct, inStruct, bIsCallCenterProgId, bIsAuthRev,
          yfcDocGetOrderList);
    } else {
      // Update outstruct with dummy data for reverse auth transaction for expiry case
      GCPaymentUtils.updateOutStructForAuthRev(inStruct, outStruct);
    }

    LOGGER.verbose("outStruct authorizationAmount: " +outStruct.authorizationAmount);
    LOGGER.verbose("outStruct authorizationExpirationDate:" +outStruct.authorizationExpirationDate);

    LOGGER.verbose("GCProcessCOA.processCOAAuthorization() - End");
    LOGGER.endTimer("GCProcessCOA.processCOAAuthorization()");
    return outStruct;
  }

  /**
   * This method will call method to create input message and invoke and return web service call
   * response for COA authorization
   *
   * @param env
   * @param inStruct
   * @param bIsAuthRev
   * @return
   */
  private YFCDocument invokeCOAAuth(YFSExtnPaymentCollectionInputStruct inStruct,
      boolean bIsAuthRev, YFCDocument yfcDocGetOrderList) {
    LOGGER.beginTimer("GCProcessCOA.invokeCOAAuth()");
    LOGGER.verbose("GCProcessCOA.invokeCOAAuth() - Start");
    YFCDocument yfcDocCOARequest = prepareInputForCOAAuth(inStruct, yfcDocGetOrderList, bIsAuthRev);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("yfcDocCOARequest: " + yfcDocCOARequest);
    }
    Document docCOAResponse = null;
    try {
      GCWebServiceUtil webServiceCall = new GCWebServiceUtil();
      String sSoapURL = YFSSystem.getProperty(GCConstants.GET_OMS_DAX_COA_PROCESSING_URL);
      String sSoapAction = YFSSystem.getProperty(GCConstants.GET_OMS_DAX_COA_PROCESSING_ACTION);
      docCOAResponse = webServiceCall.invokeSOAPWebService(yfcDocCOARequest.getDocument(), sSoapURL, sSoapAction);
    } catch (IOException excp) {
      LOGGER.error("Time out exception on web service call", excp);
    }

    if (YFCCommon.isVoid(docCOAResponse)) {
      LOGGER.verbose("docCOAResponse is Null");
      LOGGER.verbose("GCProcessCOA.invokeCOAAuth() - End");
      LOGGER.endTimer("GCProcessCOA.invokeCOAAuth()");
      return null;
    }

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("docCOAResponse :" + YFCDocument.getDocumentFor(docCOAResponse));
    }


    NodeList nSendXMLResult = docCOAResponse.getElementsByTagName(GCConstants.SEND_XML_TO_DAX_RESULT);
    if(YFCCommon.isVoid(nSendXMLResult)){
      LOGGER.verbose("docCOAResponse is Null");
      LOGGER.verbose("GCProcessCOA.invokeCOAAuth() - End");
      LOGGER.endTimer("GCProcessCOA.invokeCOAAuth()");
      return null;
    }
    Element eleSendXMLResult = (Element) nSendXMLResult.item(0);
    Node child = eleSendXMLResult.getFirstChild();
    if (child instanceof CharacterData) {
      CharacterData cd = (CharacterData) child;
      String sSendXMLResult = cd.getData();
      if (YFCCommon.isStringVoid(sSendXMLResult)) {
        LOGGER.verbose("docCOAResponse is Null");
        LOGGER.verbose("GCProcessCOA.invokeCOAAuth() - End");
        LOGGER.endTimer("GCProcessCOA.invokeCOAAuth()");
        return null;
      }
      return YFCDocument.getDocumentFor(sSendXMLResult);
    }
    return null;
  }

  /**
   * This method will prepare input request message for COA auth web service call
   *
   * @param inStruct
   * @param yfcDocGetOrderListOp
   * @param bIsAuthRev
   * @return
   */
  private YFCDocument prepareInputForCOAAuth(YFSExtnPaymentCollectionInputStruct inStruct,
      YFCDocument yfcDocGetOrderListOp, boolean bIsAuthRev) {
    LOGGER.beginTimer("GCProcessCOA.prepareInputForCOAAuth()");
    LOGGER.verbose("GCProcessCOA.prepareInputForCOAAuth() - Start");

    String sTransactionType = GCConstants.AUTHORIZE;
    if (bIsAuthRev) {
      sTransactionType = GCConstants.REVERSE;
    }

    YFCElement yfcEleOrder = yfcDocGetOrderListOp.getDocumentElement().getChildElement(GCConstants.ORDER);

    YFCDocument yfcDocCOARequest = YFCDocument.createDocument(GCXmlLiterals.PAYMENT_TRANSACTION_REQUEST);
    YFCElement yfcEleRequestRoot = yfcDocCOARequest.getDocumentElement();
    yfcEleRequestRoot.setAttribute("xmlns", "http://www.guitarcenter.com/schemas/DAX/MF/4/PaymentTransaction");
    yfcEleRequestRoot.setAttribute("xmlns:c", "http://www.guitarcenter.com/schemas/CDM/CDMV3.0/Commerce");

    YFCElement yfcEleTransactionType = yfcEleRequestRoot.createChild(GCXmlLiterals.TRANSACTION_TYPE);
    yfcEleTransactionType.setNodeValue(sTransactionType);

    YFCElement yfcEleTransactionID = yfcEleRequestRoot.createChild(GCXmlLiterals.TRANSACTION_ID);
    yfcEleTransactionID.setNodeValue(inStruct.chargeTransactionKey);

    YFCElement yfcEleSystemID = yfcEleRequestRoot.createChild(GCXmlLiterals.SYSTEM_ID);
    yfcEleSystemID.setNodeValue(GCConstants.OMS);

    String sAPIToken = YFSSystem.getProperty(GCConstants.GET_OMS_DAX_COA_API_TOKEN);
    YFCElement yfcEleAPIToken = yfcEleRequestRoot.createChild(GCXmlLiterals.API_TOKEN);
    yfcEleAPIToken.setNodeValue(sAPIToken);

    YFCElement yfcEleSellingBrand = yfcEleRequestRoot.createChild(GCXmlLiterals.SELLING_BRAND);
    yfcEleSellingBrand.setNodeValue(GCConstants.GC);

    YFCElement yfcEleSellingChannel = yfcEleRequestRoot.createChild(GCXmlLiterals.SELLING_CHANNEL);
    yfcEleSellingChannel.setNodeValue(GCConstants.CC);

    YFCElement yfcEleBillToParty = yfcEleRequestRoot.createChild(GCXmlLiterals.BILL_TO_PARTY);
    YFCElement yfcEleCustomerID = yfcEleBillToParty.createChild(GCXmlLiterals.CUSTOMER_ID);
    yfcEleCustomerID.setAttribute(GCXmlLiterals.SYSTEM_ID_ATT, GCConstants.OMS);
    yfcEleCustomerID.setNodeValue(inStruct.billToId);

    // Fetch create user ID from get order list Doc
    String sCreateUserId = yfcEleOrder.getAttribute(GCConstants.CREATE_USER_ID);
    YFCElement yfcEleSalesAgent = yfcEleRequestRoot.createChild(GCXmlLiterals.SALES_AGENT);
    yfcEleSalesAgent.setNodeValue(sCreateUserId);

    YFCElement yfcEleOrderReference = yfcEleRequestRoot.createChild(GCXmlLiterals.ORDER_REFERENCE);
    YFCElement yfcEleOrderID = yfcEleOrderReference.createChild(GCXmlLiterals.ORDER_ID);
    yfcEleOrderID.setAttribute(GCConstants.SYSTEM_ID_TAG, GCConstants.OMS);
    yfcEleOrderID.setNodeValue(inStruct.orderNo);

    // Fetch order date from get order list doc
    String sOrderDate = yfcEleOrder.getAttribute(GCXmlLiterals.ORDER_DATE);

    YFCElement yfcEleOrderDate = yfcEleOrderReference.createChild(GCXmlLiterals.ORDER_DATE);
    yfcEleOrderDate.setNodeValue(sOrderDate);

    YFCElement yfcEleAllowPartialAuth = yfcEleRequestRoot.createChild(GCXmlLiterals.ALLOW_PARTIAL_AUTH);
    yfcEleAllowPartialAuth.setNodeValue(GCConstants.FALSE_STR);

    YFCElement yfcEleCurrencyCode = yfcEleRequestRoot.createChild(GCXmlLiterals.CURRENCY_CODE);
    yfcEleCurrencyCode.setNodeValue(inStruct.currency);

    // Add only in case of reverse auth & re-auth
    if (bIsAuthRev) {
      YFCElement yfcElePriorAuthCode = yfcEleRequestRoot.createChild(GCXmlLiterals.PRIOR_AUTH_CODE);
      yfcElePriorAuthCode.setNodeValue(inStruct.authorizationId);
    }

    YFCElement yfcEleRequestAmount = yfcEleRequestRoot.createChild(GCXmlLiterals.REQUEST_AMOUNT);
    yfcEleRequestAmount.setNodeValue(inStruct.requestAmount);

    //Changes added after request message changes
    YFCDocument yfcDocCOARequestSoapEnv = YFCDocument.getDocumentFor(GCConstants.COA_REQUEST_XML_HEADER);
    YFCElement yfcEleCOASoapEnv = yfcDocCOARequestSoapEnv.getDocumentElement();
    YFCElement yfcEleIncomingXML = yfcEleCOASoapEnv.getElementsByTagName(GCConstants.MFW_INCOMING_XML).item(0);
    yfcEleIncomingXML.setNodeValue(yfcDocCOARequest.toString());

    LOGGER.verbose("GCProcessCOA.prepareInputForCOAAuth() - End");
    LOGGER.endTimer("GCProcessCOA.prepareInputForCOAAuth()");

    return yfcDocCOARequestSoapEnv;
  }

  /**
   * This method will update the outStruct as per response from web service call to DAX
   * @param env
   * @param yfcDocCOAResponse
   * @param outStruct
   * @param instruct
   * @param bIsCallCenterProgId
   * @param bIsAuthRev
   * @return
   */
  private YFSExtnPaymentCollectionOutputStruct processOutputForCOAAuth(YFSEnvironment env,
      YFCDocument yfcDocCOAResponse, YFSExtnPaymentCollectionOutputStruct outStruct,
      YFSExtnPaymentCollectionInputStruct instruct, boolean bIsCallCenterProgId, boolean bIsAuthRev,
      YFCDocument yfcDocGetOrderList) {
    LOGGER.beginTimer("GCProcessCOA.processOutputForCOAAuth()");
    LOGGER.verbose("GCProcessCOA.processOutputForCOAAuth() - Start");

    if (!YFCCommon.isVoid(yfcDocCOAResponse)) {
      // If response is not null
      LOGGER.verbose("Response doc is not null");
      YFCElement yfcEleCOAResponse = yfcDocCOAResponse.getDocumentElement();
      YFCElement yfcEleResponseCode = yfcEleCOAResponse.getChildElement(GCXmlLiterals.RESPONSE_CODE);
      String sResponseCode = yfcEleResponseCode.getNodeValue();
      LOGGER.verbose("sResponseCode : " + sResponseCode);

      if (YFCCommon.equalsIgnoreCase(GCConstants.SUCCESS, sResponseCode)) {
        // If response code is success
        LOGGER.verbose("Response code is Success");
        outStruct.authorizationAmount =
            yfcEleCOAResponse.getChildElement(GCConstants.AUTHORIZED_AMOUNT).getDoubleNodeValue();
        outStruct.authorizationId = yfcEleCOAResponse.getChildElement(GCConstants.AUTH_CODE).getNodeValue();
        outStruct.holdOrderAndRaiseEvent = false;
        outStruct.tranAmount = instruct.requestAmount;
        outStruct.tranType = GCConstants.PAYMENT_STATUS_AUTHORIZATION;

        // set auth exp date as high date for re-auth case
        if (bIsAuthRev) {
          LOGGER.verbose("Is a Auth reversal request");
          outStruct.authorizationExpirationDate = YTimestamp.HIGH_DATE.toString();
        } else {
          String sAuthExpDate = yfcEleCOAResponse.getChildElement(GCConstants.AUTH_EXPIRATION_DATE).getNodeValue();
          Date date = GCDateUtils.convertDate(sAuthExpDate);
          sAuthExpDate = new SimpleDateFormat(GCConstants.PAYMENT_DATE_TIME_FORMAT).format(date);
          outStruct.authorizationExpirationDate = sAuthExpDate;
        }

      } else {
        // if response code is failure - INSUFFICIENT_FUNDS
        LOGGER.verbose("Response code is not Success");
        Date currentDate = new Date();
        outStruct.tranRequestTime = GCDateUtils.convertDate(currentDate);
        outStruct.authTime = GCConstants.BLANK;
        outStruct.authorizationId = GCConstants.DECLINED;
        outStruct.tranType = GCConstants.PAYMENT_STATUS_AUTHORIZATION;
        outStruct.authReturnMessage = GCConstants.DECLINED;
        outStruct.authorizationAmount = GCConstants.ZEROAMOUNT;
        outStruct.holdOrderAndRaiseEvent = false;
        outStruct.tranAmount = GCConstants.ZEROAMOUNT;
        YFCDocument yfcDocPaymentTransactionErrorList =
            YFCDocument.createDocument(GCXmlLiterals.PAYMENT_TRANSACTION_ERROR_LIST);
        YFCElement yfcElePaymentTransactionErrorList = yfcDocPaymentTransactionErrorList.getDocumentElement();
        YFCElement yfcElePaymentTransactionError =
            yfcElePaymentTransactionErrorList.createChild(GCXmlLiterals.PAYMENT_TRANSACTION_ERROR);
        yfcElePaymentTransactionError.setAttribute(GCConstants.MESSAGE_TYPE, GCXmlLiterals.PAYMENT_FAILURE);
        String sErrorMessage = GCConstants.BLANK;

        if (bIsAuthRev) {
          LOGGER.verbose("Is a Auth reversal request");
          outStruct.authReturnMessage = GCConstants.REV_FAILURE;
          outStruct.tranAmount = instruct.requestAmount;
          outStruct.authorizationAmount = instruct.requestAmount;
          outStruct.collectionDate = YTimestamp.HIGH_DATE;
          sErrorMessage = GCConstants.COA_AUTH_REVERSAL_FAILED_ERROR_MSG;
        } else {
          outStruct.suspendPayment = GCConstants.FLAG_Y;
          sErrorMessage = GCConstants.COA_AUTH_DECLINED_ERROR_MSG;
        }
        yfcElePaymentTransactionError.setAttribute(GCXmlLiterals.MESSAGE_ATTRIBUTE, sErrorMessage);
        outStruct.PaymentTransactionError = yfcDocPaymentTransactionErrorList.getDocument();

        if (!bIsCallCenterProgId || bIsAuthRev) {
          // Invoke method raise alert
          raiseAlert(env, instruct.paymentType, GCConstants.PAYMENT_STATUS_AUTHORIZATION, yfcDocGetOrderList);
        } else {
          GCPaymentUtils.raiseHardDeclineAlert(env, instruct.paymentType, instruct.chargeType, instruct.requestAmount,
              yfcDocGetOrderList);
        }
      }
    } else {
      // If response is null
      LOGGER.verbose("Response doc is null");
      outStruct.authorizationAmount = GCConstants.ZEROAMOUNT;
      outStruct.authorizationId = GCConstants.DECLINED;
      outStruct.holdOrderAndRaiseEvent = false;
      outStruct.retryFlag = GCConstants.FLAG_Y;
      outStruct.tranAmount = GCConstants.ZEROAMOUNT;
      outStruct.tranType = GCConstants.PAYMENT_STATUS_AUTHORIZATION;
      outStruct.suspendPayment = GCConstants.FLAG_N;
      YFCDocument yfcDocPaymentTransactionErrorList =
          YFCDocument.createDocument(GCXmlLiterals.PAYMENT_TRANSACTION_ERROR_LIST);
      YFCElement yfcElePaymentTransactionErrorList = yfcDocPaymentTransactionErrorList.getDocumentElement();
      YFCElement yfcElePaymentTransactionError =
          yfcElePaymentTransactionErrorList.createChild(GCXmlLiterals.PAYMENT_TRANSACTION_ERROR);
      yfcElePaymentTransactionError.setAttribute(GCConstants.MESSAGE_TYPE, GCXmlLiterals.PAYMENT_FAILURE);
      String sErrorMessage = GCConstants.BLANK;

      if (bIsAuthRev) {
        //no retries are required for auth reversals.
        outStruct.authReturnMessage = GCConstants.REV_FAILURE;
        outStruct.tranAmount = instruct.requestAmount;
        outStruct.authorizationAmount = instruct.requestAmount;
        outStruct.collectionDate = YTimestamp.HIGH_DATE;
        sErrorMessage = GCConstants.COA_COM_PAYMENT_GATEWAY_ERROR_ORDER_UPDATED_WITHOUT_COA_REVERSAL;
      } else {
        sErrorMessage = GCConstants.COA_COM_PAYMENT_GATEWAY_ERROR_CREATE_ORDER_WITHOUT_AUTH;
      }
      yfcElePaymentTransactionError.setAttribute(GCXmlLiterals.MESSAGE_ATTRIBUTE, sErrorMessage);
      outStruct.PaymentTransactionError = yfcDocPaymentTransactionErrorList.getDocument();

      if (bIsAuthRev) {
        // Invoke method raise alert
        raiseAlert(env, instruct.paymentType, GCConstants.PAYMENT_STATUS_AUTHORIZATION, yfcDocGetOrderList);
      } else if (!bIsCallCenterProgId) {
        // If request is not made from Call center - table 3
        GCPaymentUtils.updateAuthRetryCount(env, instruct, yfcDocGetOrderList);
      }
    }
    LOGGER.verbose("GCProcessCOA.processOutputForCOAAuth() - End");
    LOGGER.endTimer("GCProcessCOA.processOutputForCOAAuth()");
    return outStruct;
  }

  /**
   * This method will raise an alert for COA authorization failure
   * @param env
   * @param sPaymentType
   * @param sChargeType
   */
  private void raiseAlert(YFSEnvironment env, String sPaymentType, String sChargeType, YFCDocument yfcDocGetOrderList) {
    LOGGER.beginTimer("GCProcessCOA.raiseAlert()");
    LOGGER.verbose("GCProcessCOA.raiseAlert() - Start");
    if (YFCCommon.equals(GCConstants.PAYMENT_STATUS_AUTHORIZATION, sChargeType)
        && YFCCommon.equals(GCConstants.CREDIT_ON_ACCOUNT, sPaymentType)) {
      // Raise an alert - Invoke service component
      LOGGER.verbose("Raise alert for COA Authorization Failure");
      YFCElement eleOrder = yfcDocGetOrderList.getDocumentElement().getElementsByTagName(GCConstants.ORDER).item(0);
      String sDraftOrderFlag = eleOrder.getAttribute(GCConstants.DRAFT_ORDER_FLAG);
      if(!YFCCommon.equals(sDraftOrderFlag, GCConstants.YES)){
    	  GCCommonUtil.invokeService(env, GCConstants.COA_AUTH_FAILURE_ALERT_SERVICE, yfcDocGetOrderList);
      }
    }
    LOGGER.verbose("GCProcessCOA.raiseAlert() - End");
    LOGGER.endTimer("GCProcessCOA.raiseAlert()");
  }
}
