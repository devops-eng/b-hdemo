/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE:
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               12/01/2014        Mittal, Yashu 		GCSTORE-84: Design - PayPal Re-Authorization Processing.
 *#################################################################################################################################################################
 *			 2.0  			   21/04/2017		 Expicient			GCSTORE-7181: To prevent authorization from getting voided when order has lines in 
 *																	Included In Shipment or Shipment and invoice is not yet collected.
 *																	
 *##################################################################################################################################################################
 */
package com.gc.api.payment;

import java.io.IOException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import urn.ebay.api.PayPalAPI.DoVoidResponseType;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * GCSTORE-85: Design - PayPal Re-Authorization Processing: This class will be used to void the remaining/total
 * authorized amount when the order is completely cancelled or partial order is invoiced and partial is cancelled. /**
 *
 * @author <a href="mailto:yashu.mittal@expicient.com">Yashu Mittal</a>
 */
public class GCProcessDoVoidOnCancel {

	// initialize logger
	private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCProcessDoVoidOnCancel.class);

	/**
	 *
	 * This method void all the pending authorization once the order is
	 * cancelled completely or if partial order is invoiced and the remaining is
	 * cancelled.
	 *
	 * @param env
	 *            : YFSEnvironment variable.
	 * @param inDoc
	 *            : input document to the CHANGE_ORDER.ON_CANCEL event.
	 * @throws IOException
	 *
	 */
	public void processDoVoidOnCancel(YFSEnvironment env, Document inDoc)
			throws IOException {
		LOGGER.beginTimer("GCProcessDoVoidOnCancel.processDoVoidOnCancel");
		LOGGER.verbose("Class: GCProcessDoVoidOnCancel Method: processDoVoidOnCancel BEGIN");
		boolean isVoidCallReqd = true;
		GCProcessPaypal objProcessPaypal = new GCProcessPaypal();
		String sOrderHeaderKey = inDoc.getDocumentElement().getAttribute(GCConstants.ORDER_HEADER_KEY);
		Document getOrderListIn = GCXMLUtil.getDocument("<Order OrderHeaderKey ='" + sOrderHeaderKey + "'/>");
		Document getOrderListTemplate = GCCommonUtil.getDocumentFromPath("/global/template/api/extn_PaypalGetOrderList.xml");  
		//made an addition to the template of MinLineStatus on OrderLine level as part of GCSTORE-7181 fix
		Document getOrderListOut = GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST, getOrderListIn, getOrderListTemplate);
		NodeList nlOrderLine = GCXMLUtil.getNodeListByXpath(getOrderListOut, "OrderList/Order/OrderLines/OrderLine");

		//start of GCSTORE-7181 Fix
		boolean boolInvokeInvoiceListApi = false;
		int comparStatus;
														
		for (int i=0;i<nlOrderLine.getLength();i++){
			Element eleOrderLine = (Element) nlOrderLine.item(i);

			String sOpenQty = eleOrderLine.getAttribute(GCXmlLiterals.OPEN_QTY);										
			if(Double.parseDouble(sOpenQty)>0.00){
				isVoidCallReqd = false;
				break;
			}	
			else 																										
			{	
				LOGGER.verbose("Entering the condition when open qty is zero:");
				String sMinLineStatus = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_MIN_STATUS);					  
				LOGGER.verbose("sMinLineStatus:" +sMinLineStatus);
				comparStatus =  (sMinLineStatus).compareTo(GCConstants.STATUS_INCLUDED_IN_SHIPMENT);		

				if (comparStatus==0)																		
				{
					//Status is IncludedinShipment
					isVoidCallReqd = false;
					break;					
				}

				if( !(YFCUtils.equals(GCConstants.CANCELLED_ORDER_STATUS_CODE, sMinLineStatus)) && comparStatus>=0)			
				{
					// comparStatus>0 true indicates the order line status is in Included in Shipment or above
					boolInvokeInvoiceListApi=true;
					break;
				}
			}
		}
		if(boolInvokeInvoiceListApi)	
		{
			YFCDocument getOrderInvoiceDetailListInput = YFCDocument.createDocument(GCConstants.ORDER_INVOICE_DETAIL);
			YFCElement eleOrderInvoiceDetail = getOrderInvoiceDetailListInput.getDocumentElement();
			YFCElement eInvoiceHeader = eleOrderInvoiceDetail.createChild(GCConstants.INVOICE_HEADER);
			eInvoiceHeader.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
			YFCElement eOrder = eleOrderInvoiceDetail.createChild(GCConstants.ORDER);
			eOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
			LOGGER.verbose("getOrderInvoiceDetailList Input xml:" +getOrderInvoiceDetailListInput);
			YFCDocument getOrderInvoiceDetailList = GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_INVOICE_DETAIL_LIST,getOrderInvoiceDetailListInput, null);
			LOGGER.verbose("Output of getOrderInvoiceDetailList:" +getOrderInvoiceDetailList);
			YFCElement eleGetOrderInvoiceDetailList = getOrderInvoiceDetailList.getDocumentElement();
			
			YFCNodeList<YFCElement> nlOrderInvoiceDetail = eleGetOrderInvoiceDetailList.getElementsByTagName(GCConstants.ORDER_INVOICE_DETAIL);

			if(YFCCommon.isVoid(nlOrderInvoiceDetail) || nlOrderInvoiceDetail.getLength()==0)   
			{	LOGGER.verbose("Shipment is not yet invoiced");
			isVoidCallReqd = false;
			}
			
			
			for(YFCElement nlOrderInvoiceDetailList : nlOrderInvoiceDetail)
			{	LOGGER.verbose("Invoice is created and checking if the total amount has been collected");
			YFCElement eInvoiceHeader1 = nlOrderInvoiceDetailList.getChildElement(GCConstants.INVOICE_HEADER);
			String sInvoiceType = eInvoiceHeader1.getAttribute("InvoiceType");
			if ((GCConstants.SHIPMENT).equalsIgnoreCase(sInvoiceType))
			{
			String sAmountCollected = eInvoiceHeader1.getAttribute(GCConstants.AMOUNT_COLLECTED);
			String sTotalAmount = eInvoiceHeader1.getAttribute("TotalAmount");

			if (!YFCCommon.isVoid(sAmountCollected) && !YFCCommon.isVoid(sTotalAmount)){
				double dAmountCollected = Double.parseDouble(sAmountCollected);
				double dTotalAmount = Double.parseDouble(sTotalAmount); 
				if (dAmountCollected != dTotalAmount)
				{	LOGGER.verbose("Amount Collected is not equal to Total Amount");
				isVoidCallReqd = false;
				break;
				}
			}
			}
			}
		}
		//end of fix for GCSTORE-7181  
		
		if(isVoidCallReqd){
			// Getting transactionID from payment reference1 if payment method is Paypal and calling paypal doVoid API to void
			// the transaction.
			String sPaymentReference1 =
					GCXMLUtil.getAttributeFromXPath(getOrderListOut,
							"OrderList/Order/ChargeTransactionDetails/ChargeTransactionDetail/PaymentMethod[@PaymentType = '"
									+ GCConstants.PAYPAL + "']/@PaymentReference1");
			if (!YFCCommon.isVoid(sPaymentReference1)) {
				LOGGER.verbose("Loading PayPal properties");
				objProcessPaypal.loadPayPalProperties();
				DoVoidResponseType doVoidResponse = objProcessPaypal.doVoidPaypal(sPaymentReference1);
				if (!YFCCommon.isVoid(doVoidResponse)) {
					LOGGER.verbose("DoVoid Response :"
							+ doVoidResponse.getAck().getValue());
				}
			}
		}
		LOGGER.verbose("Class: GCProcessDoVoidOnCancel Method: processDoVoidOnCancel END");
		LOGGER.endTimer("GCProcessDoVoidOnCancel.processDoVoidOnCancel");
	}

}
