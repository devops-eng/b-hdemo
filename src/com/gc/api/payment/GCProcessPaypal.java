/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *##############################################################################################################################################################
 *	        OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               12/01/2014        Mittal, Yashu		GCSTORE-85: Design - PayPal Re-Authorization Processing.
             2.0               20/01/2014        Mittal, Yashu      GCSTORE- 350: PayPal Funds Capture
                                                                    GCSTORE- 353: Design- PayPal Refund Processing
             3.0               20/02/2015        Singh, Gurpreet    CR-16: Paypal Refund Processing
 *#################################################################################################################################################################
 */


package com.gc.api.payment;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import urn.ebay.api.PayPalAPI.DoAuthorizationReq;
import urn.ebay.api.PayPalAPI.DoAuthorizationRequestType;
import urn.ebay.api.PayPalAPI.DoAuthorizationResponseType;
import urn.ebay.api.PayPalAPI.DoCaptureReq;
import urn.ebay.api.PayPalAPI.DoCaptureRequestType;
import urn.ebay.api.PayPalAPI.DoCaptureResponseType;
import urn.ebay.api.PayPalAPI.DoVoidReq;
import urn.ebay.api.PayPalAPI.DoVoidRequestType;
import urn.ebay.api.PayPalAPI.DoVoidResponseType;
import urn.ebay.api.PayPalAPI.PayPalAPIInterfaceServiceService;
import urn.ebay.api.PayPalAPI.RefundTransactionReq;
import urn.ebay.api.PayPalAPI.RefundTransactionRequestType;
import urn.ebay.api.PayPalAPI.RefundTransactionResponseType;
import urn.ebay.apis.CoreComponentTypes.BasicAmountType;
import urn.ebay.apis.eBLBaseComponents.CompleteCodeType;
import urn.ebay.apis.eBLBaseComponents.CurrencyCodeType;
import urn.ebay.apis.eBLBaseComponents.ErrorType;
import urn.ebay.apis.eBLBaseComponents.RefundType;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCPaymentUtils;
import com.yantra.yfc.date.YTimestamp;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSExtnPaymentCollectionInputStruct;
import com.yantra.yfs.japi.YFSExtnPaymentCollectionOutputStruct;

/**
 * GCSTORE-85: Design - PayPal Re-Authorization Processing. : This class will handle all the PayPal Authorization
 * Voiding, Re-Authorization and Charging Logic.
 */
/**
 * @author <a href="mailto:yashu.mittal@expicient.com">Yashu Mittal</a>
 */
public class GCProcessPaypal {


  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCProcessPaypal.class);

  private static final Properties PAYPAL_PROP = new Properties();

  /**
   *
   * This method loads all PayPal properties in a properties object.
   *
   *
   */
  public void loadPayPalProperties(){

      PAYPAL_PROP.put("acct1.UserName",YFSSystem.getProperty("acct1.UserName"));
      PAYPAL_PROP.put("acct1.Password",YFSSystem.getProperty("acct1.Password"));
      PAYPAL_PROP.put("acct1.CertKey", YFSSystem.getProperty("acct1.CertKey"));
      PAYPAL_PROP.put("acct1.CertPath", YFSSystem.getProperty("acct1.CertPath"));
      PAYPAL_PROP.put("service.EndPoint.PayPalAPI", YFSSystem.getProperty("service.EndPoint.PayPalAPI"));
      PAYPAL_PROP.put("service.EndPoint.PayPalAPIAA", YFSSystem.getProperty("service.EndPoint.PayPalAPI"));
      PAYPAL_PROP.put("service.EndPoint.Permissions", YFSSystem.getProperty("service.EndPoint.Permissions"));
      PAYPAL_PROP.put("http.ConnectionTimeOut", YFSSystem.getProperty("http.ConnectionTimeOut"));
      PAYPAL_PROP.put("service.EndPoint.Invoice", YFSSystem.getProperty("service.EndPoint.Invoice"));
      
    


  }

  /**
   *
   * This method voids all the expired authorizations and if void is successful then get a new authorization from PayPal
   * and stamp the same on the order.
   *
   * @param env : YFSEnvironment variable.
   * @param outStruct : outStruct containing details of re-authorization.
   * @param inStruct : inStruct containing details of the payment method being processed.
   * @param getOrderListOpDoc : Details of the order for which payment processing is being done.
   * @return
   * @throws IOException
   *
   */
  public YFSExtnPaymentCollectionOutputStruct processPaypalAuthorization(YFSEnvironment env, YFSExtnPaymentCollectionOutputStruct outStruct,
      YFSExtnPaymentCollectionInputStruct inStruct, YFCDocument getOrderListOpDoc) throws IOException {
    LOGGER.beginTimer("GCProcessPaypal.processPaypalAuthorization");
    LOGGER.verbose("Class: GCProcessPaypal Method: processPaypalAuthorization START");
    
    /* COA PAYMENT PROCESSING Start */
    // Add logic to update for dummy auth for reverse auth call
    if (inStruct.requestAmount < 0) {
      outStruct = GCPaymentUtils.updateOutStructForAuthRev(inStruct, outStruct);
    } else {
      // Void all the expired authorization and set bIsVoidSuccessful to true if all are voided
      // successfully.

      Boolean bIsVoidSuccessful = voidPaypalAuths(env, inStruct);
      // if all expired authorization are voided successfully, get new authorizationID from Paypal.
      if (bIsVoidSuccessful) {
        DoAuthorizationResponseType doAuthorizationResponse = doPaypalAuthorization(inStruct);
        processPaypalAuthResponse(env, doAuthorizationResponse, outStruct, inStruct, getOrderListOpDoc);
      } else {
        // if Authorization is not voided successfully, update AuthRetryCount to try voiding the
        // authorization again or raise Alert.
        GCPaymentUtils.updateAuthRetryCount(env, inStruct, getOrderListOpDoc);
        outStruct.retryFlag = GCConstants.YES;
        outStruct.collectionDate = GCDateUtils.getCollectionDate();
      }
    }
    /* COA PAYMENT PROCESSING End*/
    
    LOGGER.verbose("Class: GCProcessPaypal Method: processPaypalAuthorization END");
    LOGGER.endTimer("GCProcessPaypal.processPaypalAuthorization");
    return outStruct;
  }


  /**
   *
   * This method process response received from Paypal after Authorization and update the ourStruct as per response.
   *
   * @param env : YFSEnvironment variable.
   * @param doAuthorizationResponse : Authorization Response Variable received from Paypal.
   * @param outStruct : outStruct containing details of re-authorization.
   * @param inStruct : inStruct containing details of the payment method being processed.
   * @param getOrderListOpDoc : Details of the order for which payment processing is being done.
   * @throws GCException
   *
   */
  private void processPaypalAuthResponse(YFSEnvironment env, DoAuthorizationResponseType doAuthorizationResponse,
      YFSExtnPaymentCollectionOutputStruct outStruct, YFSExtnPaymentCollectionInputStruct inStruct, YFCDocument getOrderListOpDoc) {
    LOGGER.beginTimer("GCProcessPaypal.processPaypalAuthResponse");
    LOGGER.verbose("Class: GCProcessPaypal Method: processPaypalAuthResponse START");
    if (YFCCommon.isVoid(doAuthorizationResponse)) {
      // throw exception
      GCPaymentUtils.raiseException(GCErrorConstants.GCE00006,
          GCErrorConstants.PAYPAL_COMMUNICATION_FAILURE, inStruct.orderNo);
    }
    String sResponse = doAuthorizationResponse.getAck().getValue();
    // if Authorization response is success, update the outstruct with authorizaionID received and reset the
    // AuthRetryCount.
    if (YFCCommon.equals(sResponse, GCConstants.PAYPAL_RESPONSE_SUCCESS, true)) {
      String sAuthorizationId = doAuthorizationResponse.getTransactionID();
      String sAuthExpiryDate = GCPaymentUtils.getAuthExpiryDate(env, inStruct);
      outStruct.authorizationAmount = inStruct.requestAmount;
      outStruct.authorizationId = sAuthorizationId;
      outStruct.holdOrderAndRaiseEvent = false;
      outStruct.retryFlag = GCConstants.NO;
      outStruct.tranAmount = inStruct.requestAmount;
      outStruct.authorizationExpirationDate = sAuthExpiryDate;
      outStruct.tranType = GCConstants.AUTHORIZATION;
      GCPaymentUtils.resetAuthRetryCount(env, inStruct, getOrderListOpDoc);
    } else if (YFCCommon.equals(sResponse, GCConstants.PAYPAL_RESPONSE_FAILURE, true) || YFCCommon.equals(sResponse, GCConstants.BLANK, true)) {
      // if response is failure or Blank, getting the errorCode from response. As per the error code
      // received, either the payment method will be hard declined (suspended) and alert is raise OR
      // soft declined and
      // AuthRetryCount is increased to retry the authorization.
      List<ErrorType> errorList = doAuthorizationResponse.getErrors();
      if (!errorList.isEmpty()) {
        String sErrorCode = errorList.get(0).getErrorCode() + "_AUTH";
        YFCDocument getCommonCodeListOut = paypalCommonCode(env, GCConstants.PAYPAL_HARD_DECLNE, sErrorCode, GCConstants.PAYPAL_DO_AUTHORIZATION_API);
        if (getCommonCodeListOut.getDocumentElement().hasChildNodes()) {
          GCPaymentUtils.processHardDeclinedResponse(env, inStruct, outStruct, getOrderListOpDoc);
        } else {
          boolean alertRaised = GCPaymentUtils.updateAuthRetryCount(env, inStruct, getOrderListOpDoc);
          if (alertRaised) {
            GCPaymentUtils.processHardDeclinedResponse(env, inStruct, outStruct, getOrderListOpDoc);
          } else {
            outStruct.retryFlag = GCConstants.YES;
            outStruct.collectionDate = GCDateUtils.getCollectionDate();
          }
        }
      }
    }
    LOGGER.verbose("Class: GCProcessPaypal Method: processPaypalAuthResponse END");
    LOGGER.endTimer("GCProcessPaypal.processPaypalAuthResponse");
  }

  /**
   *
   * This method void all Paypal auths. which are expired in the last 3 days.
   *
   * @param env : YFSEnvironment variable.
   * @param inStruct : inStruct containing details of the payment method being processed.
   * @return
   * @throws IOException
   *
   */
  private Boolean voidPaypalAuths(YFSEnvironment env, YFSExtnPaymentCollectionInputStruct inStruct) throws IOException {
    LOGGER.beginTimer("GCProcessPaypal.voidPaypalAuths");
    LOGGER.verbose("Class: GCProcessPaypal Method: voidPaypalAuths START");
    String sPaymentKey = inStruct.paymentKey;
    Calendar calendarInstance = Calendar.getInstance();
    SimpleDateFormat dateFormat = new SimpleDateFormat(GCConstants.DATE_TIME_FORMAT);
    String calSysDate = dateFormat.format(calendarInstance.getTime());
    calendarInstance.add(Calendar.DAY_OF_MONTH, -Integer.parseInt(GCConstants.PAYPAL_AUTH_VOID_PERIOD));
    String daysPreviousDate = dateFormat.format(calendarInstance.getTime());
    Set<String> uniqueAuthorizationIDList = new HashSet<String>();
    // getting all the authorization which are expired in the last x days.
    YFCDocument getChargeTransactionListIn =
        YFCDocument.getDocumentFor("<ChargeTransactionDetail ChargeType='AUTHORIZATION' Status='CHECKED' PaymentKey = '" + sPaymentKey
            + "' AuthorizationExpirationDateQryType='DATERANGE' FromAuthorizationExpirationDate=' " + daysPreviousDate + " ' ToAuthorizationExpirationDate=' "
            + calSysDate + "'/>");
    YFCDocument getChargeTransactionListTemplate = YFCDocument.getDocumentFor(GCConstants.GET_CHARGE_TRANSACTION_LIST_TEMP);
    YFCDocument getChargeTransactionListOut =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_CHARGE_TRANSACTION_LIST_API, getChargeTransactionListIn, getChargeTransactionListTemplate);
    YFCNodeList<YFCElement> listChargeTransactionDetailList = getChargeTransactionListOut.getElementsByTagName(GCXmlLiterals.CHARGE_TRANSACTION_DETAIL);
    // voiding all the unique authorizaionID received.
    for (YFCElement chargeTransactionDetail : listChargeTransactionDetailList) {
      String sAuthorizationID = chargeTransactionDetail.getAttribute(GCConstants.AUTH_ID);
      if (!uniqueAuthorizationIDList.contains(sAuthorizationID)) {
        uniqueAuthorizationIDList.add(sAuthorizationID);
        // invoking do void for authorization
        DoVoidResponseType doVoidResponse = doVoidPaypal(sAuthorizationID);
        if (YFCCommon.isVoid(doVoidResponse)) {
          // throw exception
          GCPaymentUtils.raiseException(GCErrorConstants.GCE00006,
              GCErrorConstants.PAYPAL_COMMUNICATION_FAILURE,
              inStruct.orderNo);
        }
        String sResponse = doVoidResponse.getAck().getValue();
        List<ErrorType> errorList = doVoidResponse.getErrors();
        if (!errorList.isEmpty()) {
          String sErrorCode = errorList.get(0).getErrorCode() + "_VOID";
          // getting the error code from commonCode list for which the void will be considered successfull, even if the
          // response is failed.
          YFCDocument getCommonCodeListOut = paypalCommonCode(env, GCConstants.PAYPAL_RESPONSE_SUCCESS, sErrorCode, GCConstants.PAYPAL_DO_VOID_API);
          // if response is failed and error code is not the successful one, return false.
          if (YFCCommon.equals(sResponse, GCConstants.PAYPAL_RESPONSE_FAILURE, true) && !getCommonCodeListOut.getDocumentElement().hasChildNodes()) {
            return false;
          }
        }
      }
    }
    LOGGER.verbose("Class: GCProcessPaypal Method: voidPaypalAuths END");
    LOGGER.endTimer("GCProcessPaypal.voidPaypalAuths");
    return true;
  }

  /**
   *
   * This method do reauthorization by connecting to Paypal and calling doAuthorization API..
   *
   * @param inStruct : inStruct containing details of the payment method being processed.
   * @return
   * @throws GCException
   *
   */
  private DoAuthorizationResponseType doPaypalAuthorization(YFSExtnPaymentCollectionInputStruct inStruct) throws IOException {
    LOGGER.beginTimer("GCProcessPaypal.doPaypalAuthorization");
    LOGGER.verbose("Class: GCProcessPaypal Method: doPaypalAuthorization START");
    DoAuthorizationReq doAuthorizationReq = new DoAuthorizationReq();
    double dAmount = inStruct.requestAmount;
    BasicAmountType amount = new BasicAmountType(CurrencyCodeType.USD, String.valueOf(dAmount));
    String sTransactionId = inStruct.paymentReference1;
    DoAuthorizationRequestType doAuthorizationRequest = new DoAuthorizationRequestType(sTransactionId, amount);
    doAuthorizationReq.setDoAuthorizationRequest(doAuthorizationRequest);
    PayPalAPIInterfaceServiceService service = null;
    service = new PayPalAPIInterfaceServiceService(PAYPAL_PROP);
    DoAuthorizationResponseType doAuthorizationResponse = null;
    try {
      doAuthorizationResponse = service.doAuthorization(doAuthorizationReq);
    } catch (Exception e) {
      LOGGER.error("Error while doing authorization, Could not load the paypal configuration file. : ", e);
    }
    LOGGER.verbose("Class: GCProcessPaypal Method: doPaypalAuthorization END");
    LOGGER.endTimer("GCProcessPaypal.doPaypalAuthorization");
    return doAuthorizationResponse;
  }

  /**
   *
   * This method voids all the expired auths by connecting to Paypal calling doVoid API.
   *
   * @param sAuthorizationId : Expired AuthorizationId which is to be voided.
   * @return
   * @throws GCException
   *
   */
  public DoVoidResponseType doVoidPaypal(String sAuthorizationId) throws IOException {
    LOGGER.beginTimer("GCProcessPaypal.doVoidPaypal");
    LOGGER.verbose("Class: GCProcessPaypal Method: doVoidPaypal START");
    DoVoidReq doVoidReq = new DoVoidReq();
    DoVoidRequestType doVoidRequest = new DoVoidRequestType(sAuthorizationId);
    doVoidReq.setDoVoidRequest(doVoidRequest);
    PayPalAPIInterfaceServiceService service = null;
    service = new PayPalAPIInterfaceServiceService(PAYPAL_PROP);
    DoVoidResponseType doVoidResponse = null;
    try {
      doVoidResponse = service.doVoid(doVoidReq);
    } catch (Exception e) {
      LOGGER.error("Error while doing doVoid, Could not load the paypal configuration file.", e);
    }
    LOGGER.verbose("Class: GCProcessPaypal Method: doVoidPaypal END");
    LOGGER.endTimer("GCProcessPaypal.doVoidPaypal");
    return doVoidResponse;
  }

  /**
   *
   * This method returns different PapaylErrorCodes corresponding to authorization void (success/fail) and
   * authorization(success/fail).
   *
   * @param env : YFSEnvironment variable.
   * @param sCodeLongDescription : HardDecline / Success
   * @param sErrorCode : ErrorCode received from Paypal.
   * @param sAPIName : API name for which the error is to be checked.
   * @return
   * @throws GCException
   *
   */
  private YFCDocument paypalCommonCode(YFSEnvironment env, String sCodeLongDescription, String sErrorCode, String sAPIName) {
    LOGGER.beginTimer("GCProcessPaypal.paypalCommonCode");
    LOGGER.verbose("Class: GCProcessPaypal Method: paypalCommonCode START");
    YFCDocument getCommonCodeListIn =
        YFCDocument.getDocumentFor("<CommonCode CodeType='" + GCConstants.PAYPAL_ERROR_CODES_COMMON_CODE + "' CodeValue='" + sErrorCode
            + " ' CodeShortDescription=' " + sAPIName + "' CodeLongDescription = '" + sCodeLongDescription + "' />");
    YFCDocument getCommonCodeListTemplate = YFCDocument.getDocumentFor("<CommonCodeList><CommonCode/></CommonCodeList>");
    YFCDocument getCommonCodeListOut = GCCommonUtil.invokeAPI(env, GCConstants.API_GET_COMMON_CODE_LIST, getCommonCodeListIn, getCommonCodeListTemplate);
    LOGGER.verbose("Class: GCProcessPaypal Method: paypalCommonCode END");
    LOGGER.endTimer("GCProcessPaypal.paypalCommonCode");
    return getCommonCodeListOut;
  }

  // GCSTORE- 350: PayPal Funds Capture : Start

  /**
   *
   * This method is called for Paypal Charging and Refunds.
   *
   * @param env : YFSEnvironment variable
   * @param outStruct : outStruct containing details of re-authorization.
   * @param inStruct : inStruct containing details of the payment method being processed.
   * @param getOrderListOpDoc : Details of the order for which payment processing is being done.
   * @return
   * @throws IOException
   *
   */
  public YFSExtnPaymentCollectionOutputStruct processPaypalCharge(YFSEnvironment env, YFSExtnPaymentCollectionOutputStruct outStruct,
      YFSExtnPaymentCollectionInputStruct inStruct, YFCDocument getOrderListOpDoc) throws IOException {

    LOGGER.beginTimer("GCProcessPaypal.processPaypalCharge");
    LOGGER.verbose("Class: GCProcessPaypal Method: processPaypalCharge START");
    double dRequestAmount = inStruct.requestAmount;
    // check request amount to find if this entry is for charging or refund.
    if (dRequestAmount > 0) {
      processCapture(env, inStruct, outStruct, getOrderListOpDoc);
    } else if (dRequestAmount < 0) {
      processPaypalRefund(env, inStruct, outStruct, getOrderListOpDoc);
    }
    LOGGER.verbose("Class: GCProcessPaypal Method: processPaypalCharge END");
    LOGGER.endTimer("GCProcessPaypal.processPaypalCharge");
    return outStruct;
  }

  /**
   *
   * This method contains the logic for capturing PayPal fund.
   *
   * @param env : YFSEnvironment variable
   * @param outStruct : outStruct containing details of re-authorization.
   * @param inStruct : inStruct containing details of the payment method being processed.
   * @param getOrderListOpDoc : Details of the order for which payment processing is being done.
   * @return
   * @throws IOException
   *
   */
  private YFSExtnPaymentCollectionOutputStruct processCapture(YFSEnvironment env, YFSExtnPaymentCollectionInputStruct inStruct,
      YFSExtnPaymentCollectionOutputStruct outStruct, YFCDocument getOrderListOpDoc) throws IOException {

    LOGGER.beginTimer("GCProcessPaypal.processCapture");
    LOGGER.verbose("Class: GCProcessPaypal Method: processCapture START");
    // Check if this is partial invoice or final, by finding the value of grand total attribute.
    String sPaymentKey = inStruct.paymentKey;
    YFCElement eleRemainingTotals = getOrderListOpDoc.getElementsByTagName(GCXmlLiterals.REMAINING_TOTALS).item(0);

    String sGrandTotal = eleRemainingTotals.getAttribute(GCXmlLiterals.GRAND_TOTAL);
    LOGGER.verbose("Grand Total is : " + sGrandTotal);
    DoCaptureResponseType doCaptureResponse = null;

    if (YFCCommon.equals(sGrandTotal, GCConstants.ZERO_AMOUNT)) {
      YFCDocument getChargeTransactionListIn =
          YFCDocument.getDocumentFor("<ChargeTransactionDetail ChargeType='CHARGE' Status='OPEN' PaymentKey = '" + sPaymentKey + "'/>");
      YFCDocument getChargeTransactionListTemplate = YFCDocument.getDocumentFor(GCConstants.GET_CHARGE_TRANSACTION_LIST_TEMP);
      YFCDocument getChargeTransactionListOut =
          GCCommonUtil.invokeAPI(env, GCConstants.GET_CHARGE_TRANSACTION_LIST_API, getChargeTransactionListIn, getChargeTransactionListTemplate);

      YFCNodeList<YFCElement> listChargeTransactionDetailList = getChargeTransactionListOut.getElementsByTagName(GCXmlLiterals.CHARGE_TRANSACTION_DETAIL);
      if (listChargeTransactionDetailList.getLength() > 1) {
        doCaptureResponse = doPaypalCharge(env, inStruct, GCConstants.PAYPAL_CAPTURE_NOTCOMPLETE);
      } else {
        doCaptureResponse = doPaypalCharge(env, inStruct, GCConstants.PAYPAL_CAPTURE_COMPLETE);
      }
    } else {
      doCaptureResponse = doPaypalCharge(env, inStruct, GCConstants.PAYPAL_CAPTURE_NOTCOMPLETE);
    }
    processPaypalChargeResponse(env, doCaptureResponse, outStruct, inStruct, getOrderListOpDoc);
    LOGGER.verbose("Class: GCProcessPaypal Method: processCapture END");
    LOGGER.endTimer("GCProcessPaypal.processCapture");
    return outStruct;

  }

  /**
   *
   * This method calls Paypal doCapture API for charging orders in OMS.
   *
   * @param inStruct : inStruct containing details of the payment method being processed.
   * @param sCompleteType : Parameter to be passed in paypal capture API to signify if the charge is complete or
   *        partial.
   * @return
   * @throws IOException
   *
   */
  private static DoCaptureResponseType doPaypalCharge(YFSEnvironment env, YFSExtnPaymentCollectionInputStruct inStruct, String sCompleteType) throws IOException {
    LOGGER.beginTimer("GCProcessPaypal.doPaypalCharge");
    LOGGER.verbose("Class: GCProcessPaypal Method: doPaypalCharge START");
    DoCaptureReq doCaptureReq = new DoCaptureReq();
    double dAmount = inStruct.requestAmount;
    BasicAmountType amount = new BasicAmountType(CurrencyCodeType.USD, String.valueOf(dAmount));
    String sTransactionId = inStruct.paymentReference1;
    String sAuthId = inStruct.authorizationId;
	// Fix for GCSTORE-5143
    int counter = 0;
    // Setting Authid as transactionid (o-number) in case auth is expired.
    if(YFCCommon.equals(env.getTxnObject("AuthExpired"), "Yes")){
    	sAuthId = sTransactionId;
    	counter++;
    }
    DoCaptureRequestType doCaptureRequest = null;
    // setting complete code type in paypal doCapture API based on the sCompleteType attribute.
    if (YFCCommon.equals(sCompleteType, GCConstants.PAYPAL_CAPTURE_COMPLETE)) {
      LOGGER.verbose("Complete PayPal capture");
      doCaptureRequest = new DoCaptureRequestType(sAuthId, amount, CompleteCodeType.COMPLETE);
    } else if (YFCCommon.equals(sCompleteType, GCConstants.PAYPAL_CAPTURE_NOTCOMPLETE)) {
      LOGGER.verbose("Partial PayPal capture");
      doCaptureRequest = new DoCaptureRequestType(sAuthId, amount, CompleteCodeType.NOTCOMPLETE);
    }
    
    //GCSTORE-6307-adding paypal idempotency feature for charging only
    String strChargeTxnKey=inStruct.chargeTransactionKey;
    LOGGER.debug("GCSTORE-6307 :: Charge Txn Key ::"+strChargeTxnKey );
    if(!YFCCommon.isVoid(strChargeTxnKey))
    {
    doCaptureRequest.setMsgSubID(strChargeTxnKey);
    }
    //GCSTORE-6307-ends
    
    doCaptureReq.setDoCaptureRequest(doCaptureRequest);
    PayPalAPIInterfaceServiceService service = null;
    service = new PayPalAPIInterfaceServiceService(PAYPAL_PROP);
    DoCaptureResponseType doCaptureResponse = null;
    try {
      doCaptureResponse = service.doCapture(doCaptureReq);
      LOGGER.debug("Capture successful for transaction id: "
          + sTransactionId);
      // Fix for GCSTORE-5143 : If authorization expired error code = 10601 is received, try capture again with o-number instead of authid.
      List<ErrorType> errorList = doCaptureResponse.getErrors();
      if (!errorList.isEmpty()) {
    	  String sErrorCode = errorList.get(0).getErrorCode();
    	  if(YFCCommon.equals(sErrorCode, "10601") && counter == 0){
    		  LOGGER.verbose("Auth is expired and could not be re-authorized before capture. So capturing using o-number insted of authid");
    		  env.setTxnObject("AuthExpired", "Yes");
    		  doPaypalCharge(env, inStruct, sCompleteType);
    	  }
      }
    } catch (Exception e) {
      LOGGER.error("doPaypalCharge Error Message : ", e);
    }
    LOGGER.verbose("Class: GCProcessPaypal Method: doPaypalCharge END");
    LOGGER.endTimer("GCProcessPaypal.doPaypalCharge");
    return doCaptureResponse;

  }

  /**
   *
   * This method process response received from Paypal doCapture API.
   *
   * @param env : YFSEnvironment variable.
   * @param doCaptureResponse : Response received from Paypal doCaputure API.
   * @param outStruct : outStruct containing details of re-authorization.
   * @param inStruct : inStruct containing details of the payment method being processed.
   * @param getOrderListOpDoc : Details of the order for which payment processing is being done.
   *
   */
  public void processPaypalChargeResponse(YFSEnvironment env, DoCaptureResponseType doCaptureResponse, YFSExtnPaymentCollectionOutputStruct outStruct,
      YFSExtnPaymentCollectionInputStruct inStruct, YFCDocument getOrderListOpDoc) {

    LOGGER.beginTimer("GCProcessPaypal.processPaypalChargeResponse");
    LOGGER.verbose("Class: GCProcessPaypal Method: processPaypalChargeResponse START");
    if (YFCCommon.isVoid(doCaptureResponse)) {
      // throw exception
      GCPaymentUtils.raiseException(GCErrorConstants.GCE00006,
          GCErrorConstants.PAYPAL_COMMUNICATION_FAILURE, inStruct.orderNo);
    }
    String sResponse = doCaptureResponse.getAck().getValue();
    //GCSTORE-6307-added constant-PAYPAL_RESPONSE_SUCCESS_WITH_WARNING, in if condition
    if (YFCCommon.equals(sResponse, GCConstants.PAYPAL_RESPONSE_SUCCESS,
        true) || YFCCommon.equals(sResponse, GCConstants.PAYPAL_RESPONSE_SUCCESS_WITH_WARNING,
                true)) {
      outStruct.authorizationAmount = inStruct.requestAmount;
      outStruct.holdOrderAndRaiseEvent = false;
      outStruct.retryFlag = GCConstants.NO;
      outStruct.tranAmount = inStruct.requestAmount;
      outStruct.tranType = GCConstants.PAYMENT_STATUS_CHARGE;
      String sCaptureTransactionId =
          doCaptureResponse.getDoCaptureResponseDetails().getPaymentInfo().getTransactionID();
      updatePaypalRefundDistTable(env, sCaptureTransactionId, inStruct);
      GCPaymentUtils
      .resetAuthRetryCount(env, inStruct, getOrderListOpDoc);

    } else if (YFCCommon.equals(sResponse,
        GCConstants.PAYPAL_RESPONSE_FAILURE, true)
        || YFCCommon.equals(sResponse, GCConstants.BLANK, true)) {
      List<ErrorType> errorList = doCaptureResponse.getErrors();
      if (!errorList.isEmpty()) {
        String sErrorCode = errorList.get(0).getErrorCode() + "_CAPTURE";
        YFCDocument getCommonCodeListOut = paypalCommonCode(env,
            GCConstants.PAYPAL_HARD_DECLNE, sErrorCode,
            GCConstants.PAYPAL_DO_CAPTURE_API);
        if (getCommonCodeListOut.getDocumentElement().hasChildNodes()) {
          GCPaymentUtils.processHardDeclinedResponse(env, inStruct, outStruct,
              getOrderListOpDoc);
        } else {
          GCPaymentUtils.updateAuthRetryCount(env, inStruct,
              getOrderListOpDoc);
          outStruct.retryFlag = GCConstants.YES;
          outStruct.collectionDate = GCDateUtils.getCollectionDate();
        }
      }
    }

    LOGGER.verbose("Class: GCProcessPaypal Method: processPaypalChargeResponse END");
    LOGGER.endTimer("GCProcessPaypal.processPaypalChargeResponse");
  }

  // GCSTORE- 350: PayPal Funds Capture : End


  // CR-16 Start
  /**
   *
   * This method processes the refunded amount. Fetches the list of all the charged entries from the
   * hang-off table GCPaypalRefundDist and accordingly adjusts the refunded amount and update the
   * table. Raises an alert in case of refund call failure.
   *
   * @param env
   * @param inStruct
   * @param outStruct
   * @param getOrderListOpDoc
   * @return
   * @throws IOException
   *
   */
  private YFSExtnPaymentCollectionOutputStruct processPaypalRefund(YFSEnvironment env,
      YFSExtnPaymentCollectionInputStruct inStruct,
      YFSExtnPaymentCollectionOutputStruct outStruct, YFCDocument getOrderListOpDoc) throws IOException {
    LOGGER.beginTimer("GCProcessPaypal.processRefund");
    LOGGER.verbose("Class: GCProcessPaypal Method: processRefund START");

    YFCDocument docGCChangePaypalRefundDist = YFCDocument.getDocumentFor("<MultiApi/>");
    boolean bIsRefundRetryReq = false;
    double dAmountToRefund = -(inStruct.requestAmount);
    double dTotalRefundedAmount = 0.00;
    YFCDocument docGetPaypalRefundDistIP =
        YFCDocument.getDocumentFor("<GCPaypalRefundDist OrderHeaderKey='" + inStruct.orderHeaderKey + "' />");
    YFCDocument docGetPaypalRefundDistOP =
        GCCommonUtil.invokeService(env, GCConstants.GC_GET_PP_REFUND_DIST_LIST_SERVICE, docGetPaypalRefundDistIP);
    YFCNodeList<YFCElement> nlPaypalRefundDist =
        docGetPaypalRefundDistOP.getElementsByTagName(GCConstants.GC_PAYPAL_REFUND_DIST);
    Iterator<YFCElement> itrPaypalRefundDist = nlPaypalRefundDist.iterator();
    while (itrPaypalRefundDist.hasNext()) {
      LOGGER.verbose(" Iterating on the charge trans records");
      YFCElement eleGCPaypalRefundDist = itrPaypalRefundDist.next();
      double dOriginalSettledAmount = eleGCPaypalRefundDist.getDoubleAttribute(GCXmlLiterals.ORIGINAL_SETTLED_AMT);
      double dRefundedAmount = eleGCPaypalRefundDist.getDoubleAttribute(GCXmlLiterals.REFUNDED_AMT);
      String sGCPaypalRefundDistributionKey = eleGCPaypalRefundDist.getAttribute(GCXmlLiterals.GC_PP_REFUND_DIST_KEY);
      String sChargeTransactionID = eleGCPaypalRefundDist.getAttribute(GCXmlLiterals.CHARGE_TRANSACTION_ID);
      String sDeclineErrorCode = eleGCPaypalRefundDist.getAttribute(GCXmlLiterals.DECLINE_ERROR_CODE);
      if (dRefundedAmount >= dOriginalSettledAmount || !YFCCommon.isVoid(sDeclineErrorCode)) {
        LOGGER.verbose(" Move to next record ");
        continue;
      }else {
        double dAmountEligibleForRefund = GCCommonUtil.formatNumberForDecimalPrecision(dOriginalSettledAmount - dRefundedAmount, 2).doubleValue();
        double dPaypalRefundAmount = 0.00;
        LOGGER.verbose("dAmountEligibleForRefund : " + dAmountEligibleForRefund + " dAmountToRefund : "
            + dAmountToRefund);
        if (dAmountToRefund > dAmountEligibleForRefund) {
          dPaypalRefundAmount = dAmountEligibleForRefund;
        } else{
          dPaypalRefundAmount = dAmountToRefund;
        }
        LOGGER.verbose("dPaypalRefundAmount : " + dPaypalRefundAmount);
        // Invoke paypal api to refund the amount
        RefundTransactionResponseType refundTransactionResponse = doPaypalRefund(dPaypalRefundAmount, sChargeTransactionID);
        if (YFCCommon.isVoid(refundTransactionResponse)) {
          // throw exception
          GCPaymentUtils.raiseException(GCErrorConstants.GCE00006, GCErrorConstants.PAYPAL_COMMUNICATION_FAILURE,
              inStruct.orderNo);
        }
        // Check if the response from paypal is successful
        String sResponse = refundTransactionResponse.getAck().getValue();
        LOGGER.verbose(" sResponse is :" + sResponse);
        if (YFCCommon.equalsIgnoreCase(GCConstants.PAYPAL_RESPONSE_SUCCESS, sResponse)) {
          LOGGER.verbose(" Amount refunded successfully by Paypal ");
          updatePaypalRefundDistDoc(docGCChangePaypalRefundDist, dPaypalRefundAmount + dRefundedAmount, sGCPaypalRefundDistributionKey,
              null);
          dAmountToRefund = GCCommonUtil.formatNumberForDecimalPrecision(dAmountToRefund - dPaypalRefundAmount, 2).doubleValue();
          dTotalRefundedAmount = dTotalRefundedAmount + dPaypalRefundAmount;
          if(dAmountToRefund==0.00){
            LOGGER.verbose(" If whole amount is refunded, then exit the loop ");
            break;
          }
        } else if (YFCCommon.equalsIgnoreCase(GCConstants.PAYPAL_RESPONSE_FAILURE, sResponse)
            || YFCCommon.equals(sResponse, GCConstants.BLANK, true)) {
          LOGGER.verbose(" If response is failure or blank ");
          List<ErrorType> errorList = refundTransactionResponse.getErrors();
          if (!errorList.isEmpty()) {
            LOGGER.verbose(" If error exist in the response ");
            String sErrorCode = errorList.get(0).getErrorCode();
            YFCDocument getCommonCodeListOut =
                paypalCommonCode(env, GCXmlLiterals.RETRY, sErrorCode + "_REFUND",
                    GCConstants.PAYPAL_REFUND_TRANSACTION_API);
            if (!(getCommonCodeListOut.getDocumentElement().hasChildNodes())) {
              LOGGER.verbose(" If error code is hard decline");
              updatePaypalRefundDistDoc(docGCChangePaypalRefundDist, dPaypalRefundAmount,
                  sGCPaypalRefundDistributionKey, sErrorCode);
            } else {
              LOGGER.verbose(" Set retry flag as true ");
              bIsRefundRetryReq = true;
            }
          }
        }
      }
    }
    // Invoke multiAPI to update GCPaypalRefundDist table with a single API call
    GCCommonUtil.invokeAPI(env, GCConstants.API_MULTIAPI, docGCChangePaypalRefundDist, null);
    updatePaypalRefundOutStruct(env, outStruct, inStruct, getOrderListOpDoc, dTotalRefundedAmount, bIsRefundRetryReq);
    LOGGER.verbose("Class: GCProcessPaypal Method: processRefund END");
    LOGGER.endTimer("GCProcessPaypal.processRefund");
    return outStruct;
  }

  private void updatePaypalRefundOutStruct(YFSEnvironment env, YFSExtnPaymentCollectionOutputStruct outStruct,
      YFSExtnPaymentCollectionInputStruct inStruct, YFCDocument getOrderListOpDoc, double dTotalRefundedAmount,
      boolean bIsRefundRetryReq) {
    LOGGER.beginTimer("GCProcessPaypal.updatePaypalRefundOutStruct");
    LOGGER.verbose("Class: GCProcessPaypal Method: updatePaypalRefundOutStruct START");
    if (dTotalRefundedAmount > 0.00) {
      if (bIsRefundRetryReq) {
        LOGGER
        .verbose(" If even one of the refund is unsuccessful, then update retry count as well as update the refund amount in outstruct ");
        GCPaymentUtils.updateAuthRetryCount(env, inStruct, getOrderListOpDoc);
      }
      outStruct.authorizationAmount = -dTotalRefundedAmount;
      outStruct.holdOrderAndRaiseEvent = false;
      outStruct.retryFlag = GCConstants.NO;
      outStruct.tranType = GCConstants.PAYMENT_STATUS_CHARGE;
    } else if (dTotalRefundedAmount == 0.00) {
      if (bIsRefundRetryReq) {
        LOGGER.verbose(" If all the refund calls are unsuccessful, then update retry count");
        GCPaymentUtils.updateAuthRetryCount(env, inStruct, getOrderListOpDoc);
        outStruct.retryFlag = GCConstants.YES;
        outStruct.tranType = GCConstants.PAYMENT_STATUS_CHARGE;
        outStruct.collectionDate = GCDateUtils.getCollectionDate();
      } else {
        LOGGER.verbose(" If all the refund calls are hard declined, then raise an alert");
        GCCommonUtil.invokeService(env, GCConstants.PAYPAL_REFUND_HD_ALERT_SERVICE, getOrderListOpDoc);
        outStruct.retryFlag = GCConstants.YES;
        outStruct.tranType = GCConstants.PAYMENT_STATUS_CHARGE;
        // setting collection date as high to to avoid any further processing in case of decline
        outStruct.collectionDate = YTimestamp.getHighTimestamp();
      }
    } else {
      outStruct.authorizationAmount = -dTotalRefundedAmount;
      outStruct.holdOrderAndRaiseEvent = false;
      outStruct.retryFlag = GCConstants.NO;
      outStruct.tranType = GCConstants.PAYMENT_STATUS_CHARGE;
    }
    LOGGER.verbose("Class: GCProcessPaypal Method: updatePaypalRefundOutStruct END");
    LOGGER.endTimer("GCProcessPaypal.updatePaypalRefundOutStruct");
  }

  /**
   *
   * This method call Paypal refundTransaction API for refunds.
   *
   * @param inStruct : inStruct containing details of the payment method being processed.
   * @param dAmountRefunded
   * @param sChargeTransactionId
   * @return
   * @throws IOException
   *
   */
  private RefundTransactionResponseType doPaypalRefund(double dAmountRefunded, String sChargeTransactionId)
      throws IOException {
    LOGGER.beginTimer("GCProcessPaypal.doPaypalRefund");
    LOGGER.verbose("Class: GCProcessPaypal Method: doPaypalRefund START");
    RefundTransactionReq refundTransactionReq = new RefundTransactionReq();
    RefundTransactionRequestType refundTransactionRequest = new RefundTransactionRequestType();
    double dAmount = dAmountRefunded;
    String sTransactionId = sChargeTransactionId;
    refundTransactionRequest.setTransactionID(sTransactionId);
    refundTransactionRequest.setRefundType(RefundType.PARTIAL);
    BasicAmountType amount = new BasicAmountType(CurrencyCodeType.USD, String.valueOf(dAmount));
    refundTransactionRequest.setAmount(amount);
    refundTransactionReq.setRefundTransactionRequest(refundTransactionRequest);
    PayPalAPIInterfaceServiceService service = null;
    service = new PayPalAPIInterfaceServiceService(PAYPAL_PROP);
    RefundTransactionResponseType refundTransactionResponse = null;
    try {
      refundTransactionResponse = service.refundTransaction(refundTransactionReq);
      LOGGER.verbose("Refund done to transaction id: " + sTransactionId
          + " for amount: " + dAmount);
    } catch (Exception e) {
      LOGGER.error("doPaypalRefund Error Message : ", e);
    }

    LOGGER.verbose("Class: GCProcessPaypal Method: doPaypalRefund END");
    LOGGER.endTimer("GCProcessPaypal.doPaypalRefund");
    return refundTransactionResponse;
  }


  /**
   *
   * This method updates the Paypal Refund Dist Doc
   *
   * @param docGCChangePaypalRefundDist
   * @param dPaypalRefundAmount
   * @param sGCPaypalRefundDistributionKey
   * @param sDeclineErrorCode
   *
   */
  private void updatePaypalRefundDistDoc(YFCDocument docGCChangePaypalRefundDist, double dPaypalRefundAmount,
      String sGCPaypalRefundDistributionKey, String sDeclineErrorCode) {
    LOGGER.beginTimer("GCProcessPaypal.updatePaypalRefundDistDoc");
    LOGGER.verbose("Class: GCProcessPaypal Method: updatePaypalRefundDistDoc START");
    YFCElement eleAPI = docGCChangePaypalRefundDist.createElement(GCXmlLiterals.API);
    eleAPI.setAttribute(GCXmlLiterals.FLOW_NAME, GCConstants.GC_CHANGE_PP_REFUND_DIST_SERVICE);
    docGCChangePaypalRefundDist.getDocumentElement().appendChild(eleAPI);
    YFCElement eleInput = docGCChangePaypalRefundDist.createElement(GCXmlLiterals.INPUT);
    eleAPI.appendChild(eleInput);
    YFCElement elePaypalRefundDist = docGCChangePaypalRefundDist.createElement(GCConstants.GC_PAYPAL_REFUND_DIST);
    elePaypalRefundDist.setAttribute(GCXmlLiterals.GC_PP_REFUND_DIST_KEY, sGCPaypalRefundDistributionKey);
    if (!YFCCommon.isVoid(sDeclineErrorCode)) {
      elePaypalRefundDist.setAttribute(GCXmlLiterals.DECLINE_ERROR_CODE, sDeclineErrorCode);
    } else {
      elePaypalRefundDist.setAttribute(GCXmlLiterals.REFUNDED_AMT, dPaypalRefundAmount);
    }
    eleInput.appendChild(elePaypalRefundDist);
    LOGGER.verbose("Class: GCProcessPaypal Method: updatePaypalRefundDistDoc END");
    LOGGER.endTimer("GCProcessPaypal.updatePaypalRefundDistDoc");
  }

  /**
   *
   * This method makes an entry in GCPaypalRefundDist Table for the charge transaction id
   *
   * @param env
   * @param sCaptureTransactionId
   * @param inStruct
   *
   */
  private void updatePaypalRefundDistTable(YFSEnvironment env, String sCaptureTransactionId,
      YFSExtnPaymentCollectionInputStruct inStruct) {
    LOGGER.beginTimer("GCProcessPaypal.updatePaypalRefundDistTable");
    LOGGER.verbose("Class: GCProcessPaypal Method: updatePaypalRefundDistTable START");
    YFCDocument docPaypalRefundDist =
        YFCDocument.getDocumentFor("<GCPaypalRefundDist OrderHeaderKey='" + inStruct.orderHeaderKey
            + "' ChargeTransactionKey='" + inStruct.chargeTransactionKey + "' ChargeTransactionID = '"
            + sCaptureTransactionId + "' OriginalSettledAmount='" + inStruct.requestAmount + "' RefundedAmount='0'/>");

    GCCommonUtil.invokeService(env, GCConstants.GC_CREATE_PP_REFUND_DIST_SERVICE, docPaypalRefundDist);
    LOGGER.verbose("Class: GCProcessPaypal Method: updatePaypalRefundDistTable END");
    LOGGER.endTimer("GCProcessPaypal.updatePaypalRefundDistTable");
  }

  // CR-16 End
}
