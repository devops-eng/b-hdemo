/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: This class is used to get TokenID and AuthorizationID for Paypal Payments
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0             23/02/2015        Saxena, Ekansh           Payment Utility Class for Paypal
 *#################################################################################################################################################################
 */
package com.gc.api.payment;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import urn.ebay.api.PayPalAPI.DoAuthorizationReq;
import urn.ebay.api.PayPalAPI.DoAuthorizationRequestType;
import urn.ebay.api.PayPalAPI.DoAuthorizationResponseType;
import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentReq;
import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentRequestType;
import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentResponseType;
import urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsReq;
import urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsRequestType;
import urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsResponseType;
import urn.ebay.api.PayPalAPI.PayPalAPIInterfaceServiceService;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutReq;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutRequestType;
import urn.ebay.apis.CoreComponentTypes.BasicAmountType;
import urn.ebay.apis.eBLBaseComponents.AddressType;
import urn.ebay.apis.eBLBaseComponents.CountryCodeType;
import urn.ebay.apis.eBLBaseComponents.CurrencyCodeType;
import urn.ebay.apis.eBLBaseComponents.DoExpressCheckoutPaymentRequestDetailsType;
import urn.ebay.apis.eBLBaseComponents.PaymentActionCodeType;
import urn.ebay.apis.eBLBaseComponents.PaymentDetailsType;
import urn.ebay.apis.eBLBaseComponents.PaymentInfoType;
import urn.ebay.apis.eBLBaseComponents.SellerDetailsType;
import urn.ebay.apis.eBLBaseComponents.SetExpressCheckoutRequestDetailsType;

import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class TestPaypalService {
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(TestPaypalService.class);

  public Document processToken(YFSEnvironment env, Document inDoc) {
    LOGGER.verbose("Entering the processToken method");
    Document outDoc = null;
    String sTokenId;
    PayPalAPIInterfaceServiceService service = null;
    List<PaymentDetailsType> paymentDetailsList = null;
    Properties paypal_prop = new Properties();
	paypal_prop.put("acct1.UserName", "bemail_api1.guitarcenter.com");
	paypal_prop.put("acct1.Password", "G5Y8G8U6QUS4QNZS");
	paypal_prop.put("acct1.CertKey", "Welcome1");
	paypal_prop
		.put("acct1.CertPath",
			"/opt/ibm/Sterling93GA/PayPal_Cert/paypal_cert.p12");
	// app id we tried removing and passing but same response.
	paypal_prop.put("acct1.AppId", "APP-80W284485P519543T");
	paypal_prop.put("service.EndPoint.PayPalAPI",
		"https://api.sandbox.paypal.com/2.0/");
	paypal_prop.put("service.EndPoint.PayPalAPIAA",
		"https://api.sandbox.paypal.com/2.0");

    // SetExpressCheckoutReq
    SetExpressCheckoutRequestDetailsType setExpressCheckoutRequestDetails = new SetExpressCheckoutRequestDetailsType();

    setExpressCheckoutRequestDetails.setReturnURL("http://localhost/return");

    setExpressCheckoutRequestDetails.setCancelURL("http://localhost/cancel");

    // ### Payment Information
    // list of information about the payment
    paymentDetailsList = new ArrayList<PaymentDetailsType>();

    // information about the first payment
    PaymentDetailsType paymentDetails1 = new PaymentDetailsType();

    Element eleRootOfInDoc = inDoc.getDocumentElement();
    String sAmount = eleRootOfInDoc.getAttribute("Amount");

    // * `Amount`
    if (YFCCommon.equals(sAmount, null, false) || Double.parseDouble(sAmount)<=0.0) {
      YFSException excep = new YFSException("Null or negative Amount not allowed");
      throw excep;
    }

    BasicAmountType orderTotal1 = new BasicAmountType(CurrencyCodeType.USD, sAmount);
    paymentDetails1.setOrderTotal(orderTotal1);

    paymentDetails1.setPaymentAction(PaymentActionCodeType.ORDER);

    SellerDetailsType sellerDetails1 = new SellerDetailsType();
    paymentDetails1.setSellerDetails(sellerDetails1);

    paymentDetails1.setPaymentRequestID("PaymentRequest1");

    AddressType shipToAddress1 = new AddressType();
    shipToAddress1.setStreet1("Ape Way");
    shipToAddress1.setCityName("Austin");
    shipToAddress1.setStateOrProvince("TX");
    shipToAddress1.setCountry(CountryCodeType.US);
    shipToAddress1.setPostalCode("78750");

    paymentDetails1.setNotifyURL("http://localhost/ipn");

    paymentDetails1.setShipToAddress(shipToAddress1);

    paymentDetailsList.add(paymentDetails1);

    setExpressCheckoutRequestDetails.setPaymentDetails(paymentDetailsList);

    SetExpressCheckoutReq setExpressCheckoutReq = new SetExpressCheckoutReq();
    SetExpressCheckoutRequestType setExpressCheckoutRequest =
        new SetExpressCheckoutRequestType(setExpressCheckoutRequestDetails);

    setExpressCheckoutReq.setSetExpressCheckoutRequest(setExpressCheckoutRequest);

    service = new PayPalAPIInterfaceServiceService(paypal_prop);

    urn.ebay.api.PayPalAPI.SetExpressCheckoutResponseType setExpressCheckoutResponse = null;
    try {
      setExpressCheckoutResponse = service.setExpressCheckout(setExpressCheckoutReq);
    } catch (Exception e) {
      LOGGER.error("Error while fetching Token : ", e);
    }

    if ("success".equalsIgnoreCase(setExpressCheckoutResponse.getAck().getValue())) {
      sTokenId = setExpressCheckoutResponse.getToken();
      outDoc = GCXMLUtil.getDocument("<PayPal Amount='" + sAmount + "' TokenID='" + sTokenId + "'/>");
    }
    LOGGER.verbose("Exiting the processToken method");
    return outDoc;
  }

  public Document processAuthorization(YFSEnvironment env, Document inDoc) {
    LOGGER.verbose("Entering the processAuthorization method");
    Properties paypal_prop = new Properties();
	paypal_prop.put("acct1.UserName", "bemail_api1.guitarcenter.com");
	paypal_prop.put("acct1.Password", "G5Y8G8U6QUS4QNZS");
	paypal_prop.put("acct1.CertKey", "Welcome1");
	paypal_prop
		.put("acct1.CertPath",
			"/opt/ibm/Sterling93GA/PayPal_Cert/paypal_cert.p12");
	// app id we tried removing and passing but same response.
	paypal_prop.put("acct1.AppId", "APP-80W284485P519543T");
	paypal_prop.put("service.EndPoint.PayPalAPI",
		"https://api.sandbox.paypal.com/2.0/");
	paypal_prop.put("service.EndPoint.PayPalAPIAA",
		"https://api.sandbox.paypal.com/2.0");

    PayPalAPIInterfaceServiceService service = null;
    List<PaymentDetailsType> paymentDetailsList = null;
    Document outDoc = null;
    // ### Payment Information
    // list of information about the payment
    paymentDetailsList = new ArrayList<PaymentDetailsType>();

    // information about the first payment
    PaymentDetailsType paymentDetails1 = new PaymentDetailsType();

    Element eleRootOfInDoc = inDoc.getDocumentElement();
    String sAmount = eleRootOfInDoc.getAttribute("Amount");
    if (YFCCommon.equals(sAmount, null, false) || Double.parseDouble(sAmount)<=0.0) {
      YFSException excep = new YFSException("Null or negative Authorization Amount not allowed");
      throw excep;
    }

    // * `Amount`
    BasicAmountType orderTotal1 = new BasicAmountType(CurrencyCodeType.USD, sAmount);
    paymentDetails1.setOrderTotal(orderTotal1);

    paymentDetails1.setPaymentAction(PaymentActionCodeType.ORDER);

    SellerDetailsType sellerDetails1 = new SellerDetailsType();
    paymentDetails1.setSellerDetails(sellerDetails1);

    paymentDetails1.setPaymentRequestID("PaymentRequest1");

    AddressType shipToAddress1 = new AddressType();
    shipToAddress1.setStreet1("Ape Way");
    shipToAddress1.setCityName("Austin");
    shipToAddress1.setStateOrProvince("TX");
    shipToAddress1.setCountry(CountryCodeType.US);
    shipToAddress1.setPostalCode("78750");

    paymentDetails1.setNotifyURL("http://localhost/ipn");

    paymentDetails1.setShipToAddress(shipToAddress1);

    paymentDetailsList.add(paymentDetails1);

    String sTokenId = eleRootOfInDoc.getAttribute("TokenID");

    GetExpressCheckoutDetailsRequestType getExpressCheckoutDetailsRequest =
        new GetExpressCheckoutDetailsRequestType(sTokenId);

    // ## GetExpressCheckoutDetailsReq
    GetExpressCheckoutDetailsReq getExpressCheckoutDetailsReq = new GetExpressCheckoutDetailsReq();
    getExpressCheckoutDetailsReq.setGetExpressCheckoutDetailsRequest(getExpressCheckoutDetailsRequest);
    GetExpressCheckoutDetailsResponseType getExpressCheckoutDetailsResponse = null;
    service = new PayPalAPIInterfaceServiceService(paypal_prop);
    try {

      getExpressCheckoutDetailsResponse = service.getExpressCheckoutDetails(getExpressCheckoutDetailsReq);
    } catch (Exception e) {
      LOGGER.error("Error while getting PayerID : ", e);
    }
    String sPayerId = null;
    if ("success".equalsIgnoreCase(getExpressCheckoutDetailsResponse.getAck().getValue())) {
      sPayerId =
          getExpressCheckoutDetailsResponse.getGetExpressCheckoutDetailsResponseDetails().getPayerInfo().getPayerID();
    }

    DoExpressCheckoutPaymentReq doExpressCheckoutPaymentReq = new DoExpressCheckoutPaymentReq();
    DoExpressCheckoutPaymentRequestDetailsType doExpressCheckoutPaymentRequestDetails =
        new DoExpressCheckoutPaymentRequestDetailsType();
    doExpressCheckoutPaymentRequestDetails.setToken(sTokenId);
    doExpressCheckoutPaymentRequestDetails.setPayerID(sPayerId);
    doExpressCheckoutPaymentRequestDetails.setPaymentDetails(paymentDetailsList);
    DoExpressCheckoutPaymentRequestType doExpressCheckoutPaymentRequest =
        new DoExpressCheckoutPaymentRequestType(doExpressCheckoutPaymentRequestDetails);

    doExpressCheckoutPaymentReq.setDoExpressCheckoutPaymentRequest(doExpressCheckoutPaymentRequest);
    DoExpressCheckoutPaymentResponseType doExpressCheckoutPaymentResponse = null;
    try {
      doExpressCheckoutPaymentResponse = service.doExpressCheckoutPayment(doExpressCheckoutPaymentReq);
    } catch (Exception e) {
      LOGGER.error("Error while getting TransactionID : ", e);
    }
    String sTransId = null;
    if (doExpressCheckoutPaymentResponse.getDoExpressCheckoutPaymentResponseDetails().getPaymentInfo() != null) {
      Iterator<PaymentInfoType> paymentInfoIterator =
          doExpressCheckoutPaymentResponse.getDoExpressCheckoutPaymentResponseDetails().getPaymentInfo().iterator();
      while (paymentInfoIterator.hasNext()) {
        PaymentInfoType paymentInfo = paymentInfoIterator.next();
        sTransId = paymentInfo.getTransactionID();
      }
    }

    DoAuthorizationReq doAuthorizationReq = new DoAuthorizationReq();
    BasicAmountType amount = new BasicAmountType(CurrencyCodeType.USD, sAmount);
    DoAuthorizationRequestType doAuthorizationRequest = new DoAuthorizationRequestType(sTransId, amount);
    doAuthorizationReq.setDoAuthorizationRequest(doAuthorizationRequest);

    DoAuthorizationResponseType doAuthorizationResponse = null;
    try {
      doAuthorizationResponse = service.doAuthorization(doAuthorizationReq);
    } catch (Exception e) {
      LOGGER.error("Error while getting AuthorizationID : ", e);
    }
    String sAuthId = null;
    if ("success".equalsIgnoreCase(doAuthorizationResponse.getAck().getValue())) {
      sAuthId = doAuthorizationResponse.getTransactionID();
    }
    outDoc =
        GCXMLUtil.getDocument("<PayPal PayerID='" + sPayerId + "' TransactionID='" + sTransId + "' AuthorizationID='"
            + sAuthId + "' />");

    LOGGER.verbose("Exiting the processAuthorization method");
    return outDoc;
  }
}
