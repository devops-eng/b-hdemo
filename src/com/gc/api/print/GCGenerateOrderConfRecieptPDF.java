package com.gc.api.print;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.Properties;
import java.util.UUID;

import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCGenerateOrderConfRecieptPDF implements YIFCustomApi {

	  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCGenerateOrderConfRecieptPDF.class.getName());

	  String barcode = getUniquePdfFileName();
	  String yfsPdfDir = YFSSystem.getProperty(GCConstants.PDF_DIR);

	  public static String getUniquePdfFileName() {
	    return UUID.randomUUID().toString() + GCConstants.PDF_EXTENSION;
	  }

	  public Document convertToPDF(YFSEnvironment env, Document inputDoc) throws IOException, FOPException,
	  TransformerException {
	    LOGGER.beginTimer("GCGenerateOrderConfRecieptPDF.ConvertToPDF");
	    LOGGER.verbose("GCGenerateOrderConfRecieptPDF.ConvertToPDF--Begin");
	    if (LOGGER.isVerboseEnabled()) {
	      LOGGER.verbose("yfsPdfDir: " + yfsPdfDir);
	    }
	    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
	    String xmlSource = inDoc.toString();
	    if (LOGGER.isVerboseEnabled()) {
	      LOGGER.verbose("inDoc Document: " + inDoc.toString());
	    }
	    String pdfFileName = barcode;
	    File file = new File(yfsPdfDir, pdfFileName);
	    OutputStream out = new FileOutputStream(file);
	    // creation of transform source
	    // create an instance of fop factory
	    FopFactory fopFactory = FopFactory.newInstance();
	    // a user agent is needed for transformation
	    FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
	    // to store output
	    ByteArrayOutputStream pdfoutStream = new ByteArrayOutputStream();
	    StreamSource source = new StreamSource(new StringReader(xmlSource));
	    Transformer xslfoTransformer;

	    TransformerFactory transfact = TransformerFactory.newInstance();

	    xslfoTransformer = transfact.newTransformer();
	    // Construct fop with desired output format
	    Fop fop;

	    fop = fopFactory.newFop(GCConstants.MIME_PDF, foUserAgent, pdfoutStream);
	    // Resulting SAX events (the generated FO)
	    // must be piped through to FOP
	    Result res = new SAXResult(fop.getDefaultHandler());

	    // Start XSLT transformation and FOP processing

	    // everything will happen here..
	    xslfoTransformer.transform(source, res);

	    // if you want to save PDF file use the following code
	    out.write(pdfoutStream.toByteArray());
	    if (LOGGER.isVerboseEnabled()) {
	      LOGGER.verbose("Done ");
	    }

	    YFCDocument pdfDoc = YFCDocument.createDocument(GCConstants.PDF);
	    YFCElement pdfDocRootEle = pdfDoc.getDocumentElement();
	    pdfDocRootEle.setAttribute(GCConstants.PDF_NAME, pdfFileName);

	    LOGGER.verbose("GCGenerateOrderConfRecieptPDF.ConvertToPDF--End");
	    LOGGER.endTimer("GCGenerateOrderConfRecieptPDF.ConvertToPDF");
	    return pdfDoc.getDocument();

	  }

	  @Override
	  public void setProperties(Properties arg0) {

	  }

}
