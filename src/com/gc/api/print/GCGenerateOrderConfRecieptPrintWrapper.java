package com.gc.api.print;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCGenerateOrderConfRecieptPrintWrapper {

	  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCGenerateOrderConfRecieptPrintWrapper.class.getName());



	  public Document stampShipNode(YFSEnvironment env, Document inputDoc) {
	    LOGGER.beginTimer("GCGenerateOrderConfRecieptPrintWrapper.stampShipNode");
	    LOGGER.verbose("GCGenerateOrderConfRecieptPrintWrapper.stampShipNode--Begin");

	    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
	    if (LOGGER.isVerboseEnabled()) {
	      LOGGER.verbose("inDoc Document: " + inDoc.toString());
	    }
	    
	    YFCElement inDocRootEle = inDoc.getDocumentElement();
	   String orderHeaderKey = inDocRootEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
	    YFCDocument getOrderDetailInDoc = YFCDocument.createDocument(GCConstants.ORDER);
	    YFCElement getOrderDetailInDocRootEle = getOrderDetailInDoc.getDocumentElement();
	    getOrderDetailInDocRootEle.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);


	    YFCDocument getOrderDetailOutDoc =
	        GCCommonUtil.invokeService(env, "GCGenerateOrderConfReciept_getCompleteOrderDetails_svc", getOrderDetailInDoc);//Configure the template

	    YFCDocument sGCOrderConfRecieptserviceDoc =
	        GCCommonUtil.invokeService(env, "GCOrderConfReciept", getOrderDetailOutDoc);//GCConstants.GC_CUST_RETURN_RECIEPT 

	    if (LOGGER.isVerboseEnabled()) {
	      LOGGER.verbose("serviceOneDoc Document: " + sGCOrderConfRecieptserviceDoc.toString());
	    }

	    YFCElement serviceOneDocRootEle = sGCOrderConfRecieptserviceDoc.getDocumentElement();
	    String pdfGCOrdConfReciept = serviceOneDocRootEle.getAttribute(GCConstants.PDF_NAME);
	    String yfsServerUrl = YFSSystem.getProperty(GCConstants.SERVER_URL);

	    YFCDocument orderDoc = YFCDocument.createDocument(GCConstants.ORDER);
	    YFCElement orderDocRootEle = orderDoc.getDocumentElement();
	    orderDocRootEle.setAttribute(GCConstants.SERVER_URL_ATTR, yfsServerUrl);

	    YFCElement recieptListEle = orderDoc.createElement(GCConstants.RECEIPT_LIST);
	    orderDocRootEle.appendChild(recieptListEle);

	    YFCElement recieptEle = orderDoc.createElement(GCConstants.RECEIPT);
	    recieptListEle.appendChild(recieptEle);

	    recieptEle.setAttribute(GCConstants.FILE_NAME, pdfGCOrdConfReciept);


	   
	    LOGGER.verbose("GCGenerateOrderConfRecieptPrintWrapper.stampShipNode--End");
	    LOGGER.endTimer("GCGenerateOrderConfRecieptPrintWrapper.stampShipNode");
	    return orderDoc.getDocument();

	  }

}
