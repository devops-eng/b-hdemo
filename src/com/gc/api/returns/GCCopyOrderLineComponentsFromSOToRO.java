/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class will post 70Ticket details to POS, fetch the response from POS and stamp POSDAXCOATicket on orderlines.
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               7/11/2015        Mittal, Yashu		
 *#################################################################################################################################################################
 */
package com.gc.api.returns;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCLogUtils;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:yashu.mittal@expicient.com">Yashu Mittal</a>
 */
public class GCCopyOrderLineComponentsFromSOToRO {
	// initialize logger
	private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCCopyOrderLineComponentsFromSOToRO.class.getName());

  /**
   * 
   * This class will copy GCOrderLineComponents from SO to RO and unit price will be updated by subtracting any promotions/per unit present on the order line.
   *
   * @param env
   * @param inputDoc
   * @throws IOException
   *
   */
  public void stampOrderLineComponentsOnRO(YFSEnvironment env, Document inDoc){
    Document getOrderListOp = null;

    List<Element> listSetBundles =
        GCXMLUtil.getElementListByXpath(inDoc,
            "Order/OrderLines/OrderLine[@KitCode='BUNDLE']");
    String sDerivedOrdrHdrKey = GCXMLUtil.getAttributeFromXPath(inDoc,
        "//OrderLine/@DerivedFromOrderHeaderKey");
    if (!YFCCommon.isVoid(sDerivedOrdrHdrKey)) {
      Document getOrderListIp = GCXMLUtil.getDocument("<Order OrderHeaderKey='" + sDerivedOrdrHdrKey + "'/>");
      Document getOrderListTemp =
          GCXMLUtil
              .getDocument("<OrderList><Order OrderHeaderKey=''><OrderLines><OrderLine OrderLineKey='' IsBundleParent='' OrderedQty=''><LinePriceInfo/>"
                  + "<Item ProductClass='' ItemID=''/><ItemDetails><Extn/></ItemDetails><Extn ExtnActualSerialNumber='' ExtnPOSSKUNumber=''>"
                  + "<GCOrderLineComponentsList><GCOrderLineComponents Amount='' ChargeCategory='' ChargeName='' IsDiscount='' ItemID='' Quantity=''/>"
                  + "</GCOrderLineComponentsList></Extn><BundleParentLine OrderLineKey=''/><LineTaxes><LineTax/></LineTaxes></OrderLine>"
                  + "</OrderLines><PaymentMethods><PaymentMethod/></PaymentMethods></Order></OrderList>");

      getOrderListOp = GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST, getOrderListIp, getOrderListTemp);    
      GCLogUtils.verboseLog(LOGGER, "getOrderListOpDoc : ", getOrderListOp.toString());
      stampFraudHoldStampOnSalesOrd(env, inDoc, getOrderListOp);
    }
    
    if(!listSetBundles.isEmpty() && !YFCCommon.isVoid(sDerivedOrdrHdrKey)){

      String sROOrderHeaderKey= inDoc.getDocumentElement().getAttribute(GCConstants.ORDER_HEADER_KEY);
      Document changeOrderInDoc = GCXMLUtil.createDocument("Order");
      Element eleRootChangeOrder = changeOrderInDoc.getDocumentElement();
      eleRootChangeOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sROOrderHeaderKey);
      eleRootChangeOrder.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
      Element eleChangeOrderOrderLines = changeOrderInDoc.createElement(GCConstants.ORDER_LINES);
      eleRootChangeOrder.appendChild(eleChangeOrderOrderLines);

      Map<String, Element> oLKeyOrderLineMap = new HashMap<String, Element>();

      Element eleSalesOrderList = getOrderListOp.getDocumentElement();
      Element eleSalesOrder = (Element) eleSalesOrderList.getElementsByTagName(GCConstants.ORDER).item(0);
      NodeList nlSalesOrderLine = eleSalesOrder.getElementsByTagName(GCConstants.ORDER_LINE);

      for (int i= 0 ; i < nlSalesOrderLine.getLength() ; i++) {
        Element eleSalesOrderLine = (Element) nlSalesOrderLine.item(i);
        String sOrderLineKey = eleSalesOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
        String sIsBundleParent = eleSalesOrderLine.getAttribute(GCConstants.IS_BUNDLE_PARENT);
        Element eleSalesBundleParentLine = (Element) eleSalesOrderLine.getElementsByTagName(GCConstants.BUNDLE_PARENT_LINE).item(0);
        if (!YFCCommon.isVoid(eleSalesBundleParentLine) || YFCCommon.equals(sIsBundleParent, GCConstants.YES)) {
          oLKeyOrderLineMap.put(sOrderLineKey, eleSalesOrderLine);
        }
      }

      List<Element> listBundleParentLineDetail =
          GCXMLUtil.getElementListByXpath(inDoc,
              "Order/OrderLines/OrderLine[@KitCode='BUNDLE']");

      if (!listBundleParentLineDetail.isEmpty()) {
        for (int i = 0; i < listBundleParentLineDetail.size(); i++) {
          Element eleROBundleParentLine = listBundleParentLineDetail.get(i);
          String sROBundleParentLineKey = eleROBundleParentLine.getAttribute(GCConstants.ORDER_LINE_KEY);
          String sReturnReason = eleROBundleParentLine.getAttribute(GCConstants.RETURN_REASON);
          String sDerivedOrderLineKey = eleROBundleParentLine.getAttribute(GCConstants.DERIVED_FROM_ORDER_LINE_KEY);
          Element eleSOBundleParentOrderLine = oLKeyOrderLineMap.get(sDerivedOrderLineKey);
          String sTaxPercentage = GCXMLUtil.getAttributeFromXPath(eleSOBundleParentOrderLine,"LineTaxes/LineTax[@ChargeCategory='LinePrice' and @ChargeName='LinePriceTax']/@TaxPercentage");
          //	  String sPrimaryComponentId = GCXMLUtil.getAttributeFromXPath(eleSOBundleParentOrderLine,"ItemDetails/Extn/@ExtnPrimaryComponentItemID");

          List<Element> listChildLineDetail =
              GCXMLUtil.getElementListByXpath(inDoc,
                  "Order/OrderLines/OrderLine/BundleParentLine[@OrderLineKey='" + sROBundleParentLineKey + "']/..");

          for (int j = 0; j < listChildLineDetail.size(); j++) {
            Element eleReturnChildLine = listChildLineDetail.get(j);
            Element eleChangeOrderOrderLine = changeOrderInDoc.createElement(GCConstants.ORDER_LINE);
            eleChangeOrderOrderLines.appendChild(eleChangeOrderOrderLine);
            eleChangeOrderOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, eleReturnChildLine.getAttribute(GCConstants.ORDER_LINE_KEY));
            eleChangeOrderOrderLine.setAttribute(GCConstants.RETURN_REASON, sReturnReason);
            String sChildDerivedOrderLineKey = eleReturnChildLine.getAttribute(GCConstants.DERIVED_FROM_ORDER_LINE_KEY);
            Element eleSalesChildLine = oLKeyOrderLineMap.get(sChildDerivedOrderLineKey);

            // Create Extn and append values from salesOrder
            Element eleSalesLineExtn = (Element) eleSalesChildLine.getElementsByTagName(GCConstants.EXTN).item(0);
            Element eleOrderLineExtn = changeOrderInDoc.createElement(GCConstants.EXTN);
            eleChangeOrderOrderLine.appendChild(eleOrderLineExtn);
            eleOrderLineExtn.setAttribute("ExtnActualSerialNumber", eleSalesLineExtn.getAttribute("ExtnActualSerialNumber"));
            String sPOSSKUNumber = eleSalesLineExtn.getAttribute("ExtnPOSSKUNumber");
            eleOrderLineExtn.setAttribute("ExtnPOSSKUNumber", sPOSSKUNumber);

            Element eleReturnChildLineGCCompList = changeOrderInDoc.createElement("GCOrderLineComponentsList");
            eleOrderLineExtn.appendChild(eleReturnChildLineGCCompList);
            NodeList nlSalesLineGCComp = eleSalesLineExtn.getElementsByTagName("GCOrderLineComponents");

            // Logic to Check if line does not have GCOrderLineComponents stamp. If yes, copy GCOrderLineComponets from an orderline with same SKU Id.
            if(nlSalesLineGCComp.getLength()==0){
              List<Element> listSameItemLineDetail =
                  GCXMLUtil.getElementListByXpath(getOrderListOp,
                      "OrderList/Order/OrderLines/OrderLine[@OrderLineKey!='" + sChildDerivedOrderLineKey+ "']/Extn[@ExtnPOSSKUNumber='" + sPOSSKUNumber+ "']/..");
              if (!listSameItemLineDetail.isEmpty()) {
                Iterator<Element> iteratorSameItem = listSameItemLineDetail.iterator();
                while (iteratorSameItem.hasNext()) {
                  Element eleSameItemLineDetail = iteratorSameItem.next();
                  NodeList nlSameItemGCOrderLineComp = eleSameItemLineDetail.getElementsByTagName("GCOrderLineComponents");
                  if(nlSameItemGCOrderLineComp.getLength()>0){
                    nlSalesLineGCComp = nlSameItemGCOrderLineComp;
                    break;
                  }
                }
              }
              Element eleSalesOrderLine = GCXMLUtil.getElementByXPath(getOrderListOp, "OrderList/Order/OrderLines/OrderLine[@OrderLineKey='" + sChildDerivedOrderLineKey+ "']");
              Element eleBundleParentLine = (Element) eleSalesOrderLine.getElementsByTagName(GCConstants.BUNDLE_PARENT_LINE).item(0);
              String sBundleParentLineKey = eleBundleParentLine.getAttribute(GCConstants.ORDER_LINE_KEY);
              String sBundleParentUnitPrice = GCXMLUtil.getAttributeFromXPath(getOrderListOp, "OrderList/Order/OrderLines/OrderLine[@OrderLineKey='" + sBundleParentLineKey+ "']/LinePriceInfo/@UnitPrice");

              for(int k=0;k<nlSalesLineGCComp.getLength();k++){
                Element eleSalesLineGCComp = (Element) nlSalesLineGCComp.item(k);
                String sChargeCategory = eleSalesLineGCComp.getAttribute(GCConstants.CHARGE_CATEGORY);
                if(YFCCommon.equals(sChargeCategory, "BundlePrice")){
                  eleSalesLineGCComp.setAttribute("Amount", sBundleParentUnitPrice);
                  break;
                }
              }
            }

            // End

            //Loop to find Total PromotionPresent on OrderLine.
            Double dChargeAmount = 0.00;
            for(int k=0;k<nlSalesLineGCComp.getLength();k++){
              Element eleSalesLineGCComp = (Element) nlSalesLineGCComp.item(k);
              String sChargeCategory = eleSalesLineGCComp.getAttribute("ChargeCategory");
              String sIsDiscount = eleSalesLineGCComp.getAttribute("IsDiscount");
              if(YFCCommon.equals(sIsDiscount, GCConstants.YES) && YFCCommon.equals(sChargeCategory, GCConstants.PROMOTION)){
                String sAmount =   eleSalesLineGCComp.getAttribute("Amount");
                dChargeAmount = dChargeAmount + Double.parseDouble(sAmount);
              }
            }

            for(int k=0;k<nlSalesLineGCComp.getLength();k++){

              Element eleSalesLineGCComp = (Element) nlSalesLineGCComp.item(k);
              String sChargeCategory = eleSalesLineGCComp.getAttribute("ChargeCategory");
              String sChargeName = eleSalesLineGCComp.getAttribute("ChargeName");
              if(YFCCommon.equals(sChargeCategory, GCConstants.UNIT_PRICE) || YFCCommon.equals(sChargeCategory, "BundlePrice") 
            		  || YFCCommon.equals(sChargeName, GCConstants.LINE_PRICE_TAX) || YFCCommon.equals(sChargeCategory, GCConstants.CUSTOMER_APPEASEMENT) ||
            		  YFCCommon.equals(sChargeCategory,"SHIPPING_APPEASEMENT")){
                Element eleReturnChildLineGCComp = changeOrderInDoc.createElement("GCOrderLineComponents");
                eleReturnChildLineGCComp.setAttribute("ChargeCategory", eleSalesLineGCComp.getAttribute("ChargeCategory"));
                eleReturnChildLineGCComp.setAttribute("ChargeName", eleSalesLineGCComp.getAttribute("ChargeName"));
                eleReturnChildLineGCComp.setAttribute("IsDiscount", eleSalesLineGCComp.getAttribute("IsDiscount"));
                eleReturnChildLineGCComp.setAttribute("ItemID", eleSalesLineGCComp.getAttribute("ItemID"));
                eleReturnChildLineGCComp.setAttribute("Quantity", eleSalesLineGCComp.getAttribute("Quantity"));

                if(YFCCommon.equals(sChargeCategory, GCConstants.UNIT_PRICE)){
                  String sPrimaryCompAmount = eleSalesLineGCComp.getAttribute("Amount");
                  String sOrderedQtySO = eleSalesChildLine.getAttribute("OrderedQty");
                  if (!YFCCommon.isVoid(sOrderedQtySO) && dChargeAmount!=0.00) {
                    Double sPrimaryCompAmountPerUnit = Double.parseDouble(sPrimaryCompAmount) - (dChargeAmount) / Double.parseDouble(sOrderedQtySO);
                    String sROPromotionPerUnit = sPrimaryCompAmountPerUnit.toString();
                    BigDecimal a = new BigDecimal(sROPromotionPerUnit);
                    BigDecimal bROUnitPrice = a.setScale(2, BigDecimal.ROUND_HALF_EVEN);
                    sROPromotionPerUnit = bROUnitPrice.toString();
                    eleReturnChildLineGCComp.setAttribute("Amount", sROPromotionPerUnit);
                    LOGGER.debug("sROUnitPrice" + sROPromotionPerUnit);
                  } else{
                    eleReturnChildLineGCComp.setAttribute("Amount", sPrimaryCompAmount);
                  }

                } else {
                  eleReturnChildLineGCComp.setAttribute("Amount", eleSalesLineGCComp.getAttribute("Amount"));
                }
                eleReturnChildLineGCCompList.appendChild(eleReturnChildLineGCComp);
              }
            }
            // Stamping TaxRate in GCCOrderLineComponents.
            Element eleReturnChildLineGCComp = changeOrderInDoc.createElement("GCOrderLineComponents");
            eleReturnChildLineGCComp.setAttribute("ChargeCategory", "TaxRate");
            eleReturnChildLineGCComp.setAttribute("ChargeName", "TaxRate");
            eleReturnChildLineGCComp.setAttribute("Amount", sTaxPercentage);
            eleReturnChildLineGCCompList.appendChild(eleReturnChildLineGCComp);
          }
        }
      }
      YFCDocument.getDocumentFor(changeOrderInDoc);
      GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, changeOrderInDoc);
    }
  }

  private void stampFraudHoldStampOnSalesOrd(YFSEnvironment env, Document inDoc, Document getOrderListOp) {
    boolean setFraudAttribute = false;
    Element eleRetExtn = (Element) inDoc.getElementsByTagName(GCConstants.EXTN).item(0);
    String isRefundOnAcct = eleRetExtn.getAttribute(GCConstants.EXTN_IS_REFUND_ON_ACCOUNT);
    String refPaymentKey = eleRetExtn.getAttribute(GCConstants.EXTN_REFUND_PAYMENT_KEY);
    if (!YFCCommon.isVoid(refPaymentKey)) {
      setFraudAttribute = true;
    } else if (YFCUtils.equals(GCConstants.YES, isRefundOnAcct)) {
      Element elePaymentMethod =
          GCXMLUtil.getElementByXPath(getOrderListOp,
              "OrderList/Order/PaymentMethods/PaymentMethod[@PaymentType='STORE_CREDIT']");
      if (YFCCommon.isVoid(elePaymentMethod)) {
        setFraudAttribute = true;
      }
    }
    if (setFraudAttribute) {
      Element eleRoot = getOrderListOp.getDocumentElement();
      String orderHeaderKey = GCXMLUtil.getAttributeFromXPath(eleRoot, "Order/@OrderHeaderKey");
      Document changeOrderInDoc = GCXMLUtil.createDocument("Order");
      Element eleRootChangeOrder = changeOrderInDoc.getDocumentElement();
      eleRootChangeOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
      eleRootChangeOrder.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
      Element eleExtn = changeOrderInDoc.createElement(GCConstants.EXTN);
      eleExtn.setAttribute(GCConstants.EXTN_IS_REF_PAYMENT_CHNG, GCConstants.YES);
      eleRootChangeOrder.appendChild(eleExtn);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(" evaluateCondition() method, Input document is" + GCXMLUtil.getXMLString(changeOrderInDoc));
      }
      GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, changeOrderInDoc);
    }
  }
}
