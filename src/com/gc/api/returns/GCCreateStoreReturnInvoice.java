/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Clubs all the order lines for each POSSalesTicketNo and creates separate invoice
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               03/17/2015        Singh, Gurpreet            GCSTORE-556
 *#################################################################################################################################################################
 */
package com.gc.api.returns;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.shared.ycp.YFSContext;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * @author <a href="gurpreet.singh@expicient.com">Gurpreet</a>
 * This service is invoked from service GCCreeateStoreReturnInvoiceService. This class will Club all the order lines for each POSSalesTicketNo and creates separate invoice
 */
public class GCCreateStoreReturnInvoice implements YIFCustomApi {


  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCCreateStoreReturnInvoice.class);

  /**
   *
   * Create a separate invoice for the order lines on the basis of POSSalesTicketNo
   *
   * @param env
   * @param inputDoc
   * @throws SQLException
   *
   */
  public void createStoreReturnInvoices(YFSEnvironment env, Document inputDoc) throws SQLException {
    LOGGER.beginTimer("GCCreateStoreReturnInvoice.createStoreReturnInvoices");
    LOGGER.verbose("Class: GCCreateStoreReturnInvoice Method: createStoreReturnInvoices START");
    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    YFCElement rootEle = inDoc.getDocumentElement();
    String sOrderHeaderKey = rootEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
    YFCNodeList<YFCElement> nlOrderLines = rootEle.getElementsByTagName(GCConstants.ORDER_LINE);

    /*// Invoke getOrderList to find SO OrderHeaderKey
    YFCDocument getOrderListIn = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderHeaderKey+"'/>");
    YFCDocument getOrderListTemp = YFCDocument.getDocumentFor("<OrderList><Order><OrderLines><OrderLine DerivedFromOrderHeaderKey=''/></OrderLines></Order></OrderList>");
    YFCDocument getOrderListOut = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListIn,getOrderListTemp );
    
    YFCElement eleROOrderLine = getOrderListOut.getDocumentElement().getElementsByTagName(GCConstants.ORDER_LINE).item(0);
    String sSOOrderHeaderKey = eleROOrderLine.getAttribute(GCConstants.DERIVED_FROM_ORDER_HEADER_KEY);*/
    
    // Looping over all the order lines and append POSSalesTicketNo to a set
    Map<String, List<YFCElement>> mOrderLines = new HashMap<String, List<YFCElement>>();

    for (YFCElement eleOrderLine : nlOrderLines) {
      LOGGER.verbose(" Looping over all the order lines ");
      YFCElement eleExtn = eleOrderLine.getElementsByTagName(GCConstants.EXTN).item(0);
      String sExtnPOSSalesTicketNo = eleExtn.getAttribute(GCConstants.EXTN_POS_SALESTKT_NO);
      LOGGER.verbose(" sExtnPOSSalesTicketNo is " + sExtnPOSSalesTicketNo);
      if(mOrderLines.containsKey(sExtnPOSSalesTicketNo)){
        List<YFCElement> eleTempList = mOrderLines.get(sExtnPOSSalesTicketNo);
        eleTempList.add(eleOrderLine);
        mOrderLines.put(sExtnPOSSalesTicketNo, eleTempList);
      } else{
        List<YFCElement> eleTempList = new ArrayList<YFCElement>();
        eleTempList.add(eleOrderLine);
        mOrderLines.put(sExtnPOSSalesTicketNo, eleTempList);
      }
    }

    // Loop over the map and create a separate invoice for the order lines on the basis of
    // POSSalesTicketNo
    for (Map.Entry<String, List<YFCElement>> entry : mOrderLines.entrySet()){
      LOGGER.verbose(" Looping on the entries of map ");
      YFCDocument docCreateInvoice =
          YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey
              + "' TransactionId='"+ GCConstants.CREATE_ORDER_INVOICE_0003 +"' IgnoreStatusCheck='Y' />");
      YFCElement eleInvoiceOrderLines = docCreateInvoice.createElement(GCConstants.ORDER_LINES);
      docCreateInvoice.getDocumentElement().appendChild(eleInvoiceOrderLines);

      List<YFCElement> eleTempOrderLineList =  entry.getValue();

      for(YFCElement eleOrderLine : eleTempOrderLineList){
        YFCElement eleInvoiceOrderLine = docCreateInvoice.createElement(GCConstants.ORDER_LINE);
        eleInvoiceOrderLine.setAttribute(GCConstants.PRIME_LINE_NO,eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO));
        eleInvoiceOrderLine.setAttribute(GCConstants.SUB_LINE_NO, eleOrderLine.getAttribute(GCConstants.SUB_LINE_NO));
        eleInvoiceOrderLine.setAttribute(GCConstants.QUANTITY, eleOrderLine.getAttribute(GCConstants.ORDERED_QTY));
        eleInvoiceOrderLines.appendChild(eleInvoiceOrderLine);
      }

      GCCommonUtil.invokeAPI(env, GCConstants.CREATE_ORDER_INVOICE, docCreateInvoice, null);
      invokeChangeOrderStatus(env, eleTempOrderLineList, GCConstants.CREATE_ORDER_INVOICE_0003, GCConstants.RETURN_INVOICED_STATUS, inDoc);
      LOGGER.verbose("Invoice Created Successfully");
    }
    
    // Fix to invoke processorder payments after creating invoice and changing order status.
    
    /*if(env instanceof YFSContext){
		try {
			((YFSContext)env).commit();
		} catch (SQLException e) {
			LOGGER.error("Exception while creating store returns",e);
			YFSException yfsEx = new YFSException();
			yfsEx.setErrorCode("EXTN_GCE00100");
			throw yfsEx;

		}
	}
    
    if(!YFCCommon.isVoid(sSOOrderHeaderKey)){
    	LOGGER.verbose("SalesOrderHeaderKey is fetched");
    	Document processOrderPaymentIn = GCXMLUtil.getDocument("<Order OrderHeaderKey='"+sSOOrderHeaderKey+"'/>");
    	GCCommonUtil.invokeAPI(env, "processOrderPayments", processOrderPaymentIn, null);
    	LOGGER.verbose("ProcessOrderPayments is successfull.");
    }
    */
    LOGGER.verbose("Class: GCCreateStoreReturnInvoice Method: createStoreReturnInvoices END");
    LOGGER.endTimer("GCCreateStoreReturnInvoice.createStoreReturnInvoices");
  }


  /**
   *
   * Description of invokeChangeOrderStatus
   *
   * @param env
   * @param nlOrderLines
   * @param sTransactionId
   * @param sDropStatus
   * @param yfcInDoc
   *
   */
  private void invokeChangeOrderStatus(YFSEnvironment env, List<YFCElement> nlOrderLines, String sTransactionId,
      String sDropStatus, YFCDocument yfcInDoc) {
    YFCElement eleInDoc = yfcInDoc.getDocumentElement();
    String sOrderHeaderKey = eleInDoc.getAttribute(GCConstants.ORDER_HEADER_KEY);
    YFCDocument docChangeOrderStatus = GCCommonUtil.prepareChangeOrderStatusHeaderDoc(sOrderHeaderKey, sTransactionId);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" processReturnedToStoreReturns() method, inDoc is" + yfcInDoc.toString());
    }

    YFCElement changeOrderStatusOrderLines = docChangeOrderStatus.getDocumentElement().getChildElement("OrderLines");
    for (YFCElement eleOrderLine : nlOrderLines) {
      String sQuantity = eleOrderLine.getAttribute(GCConstants.ORDERED_QTY);
      LOGGER.verbose(" sQuantity " + sQuantity);
      GCCommonUtil.addLinesToChangeOdrStatusDoc(changeOrderStatusOrderLines, eleOrderLine, sQuantity, sDropStatus);
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" processReturnedToStoreReturns() method, docChangeOrderStatus is"
          + docChangeOrderStatus.toString());
    }
    GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER_STATUS, docChangeOrderStatus, null);

  }
  @Override
  public void setProperties(Properties arg0) {

  }
}
