package com.gc.api.returns;

import org.w3c.dom.Document;

import com.gc.api.borderfree.GCProcessBorderfreeOrders;
import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCGenerateReturnRecieptPrintWrapper {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCProcessBorderfreeOrders.class.getName());



  public Document stampShipNode(YFSEnvironment env, Document inputDoc) {
    LOGGER.beginTimer("GCGenerateReturnRecieptPrintWrapper.stampShipNode");
    LOGGER.verbose("GCGenerateReturnRecieptPrintWrapper.stampShipNode--Begin");

    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("inDoc Document: " + inDoc.toString());
    }
    YFCElement inDocRootEle = inDoc.getDocumentElement();
    String failedReceipt = inDocRootEle.getAttribute("FailedReceipt");
    String isReprint = inDocRootEle.getAttribute(GCConstants.IS_REPRINT);
    String orderHeaderKey = inDocRootEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
    YFCDocument getOrderDetailInDoc = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement getOrderDetailInDocRootEle = getOrderDetailInDoc.getDocumentElement();
    getOrderDetailInDocRootEle.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);



    Document getOrderDetailTempDoc =
        GCCommonUtil.getDocumentFromPath("/global/template/api/Get_Order_Detail_Customer_Return_reciept.xml");
    YFCDocument getOrderDetailTemDoc = YFCDocument.getDocumentFor(getOrderDetailTempDoc);

    YFCDocument getOrderDetailOutDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_DETAILS, getOrderDetailInDoc, getOrderDetailTemDoc);

    //removeComponentLine(getOrderDetailOutDoc);
    YFCDocument sGCCustReturnRecieptserviceDoc =
        GCCommonUtil.invokeService(env, GCConstants.GC_CUST_RETURN_RECIEPT, getOrderDetailOutDoc);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("serviceOneDoc Document: " + sGCCustReturnRecieptserviceDoc.toString());
    }

    YFCElement serviceOneDocRootEle = sGCCustReturnRecieptserviceDoc.getDocumentElement();
    String pdfGCCustomerReturnReciept = serviceOneDocRootEle.getAttribute(GCConstants.PDF_NAME);
    String yfsServerUrl = YFSSystem.getProperty(GCConstants.SERVER_URL);



    YFCDocument orderDoc = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement orderDocRootEle = orderDoc.getDocumentElement();
    orderDocRootEle.setAttribute(GCConstants.SERVER_URL_ATTR, yfsServerUrl);

    YFCElement recieptListEle = orderDoc.createElement(GCConstants.RECEIPT_LIST);
    orderDocRootEle.appendChild(recieptListEle);

    YFCElement recieptEle = orderDoc.createElement(GCConstants.RECEIPT);
    recieptListEle.appendChild(recieptEle);

    recieptEle.setAttribute(GCConstants.FILE_NAME, pdfGCCustomerReturnReciept);


    if (!YFCUtils.equals(isReprint, GCConstants.YES)) {
      YFCDocument sGCInvReturnSlipserviceDoc =
          GCCommonUtil.invokeService(env, GCConstants.GC_INV_RETURN_SLIP, getOrderDetailOutDoc);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("serviceTwoDoc Document: " + sGCInvReturnSlipserviceDoc.toString());
      }

      YFCElement serviceTwoDocRootEle = sGCInvReturnSlipserviceDoc.getDocumentElement();
      String pdfGCInventoryReturnSlip = serviceTwoDocRootEle.getAttribute(GCConstants.PDF_NAME);
      YFCElement recieptEleOne = orderDoc.createElement(GCConstants.RECEIPT);
      recieptListEle.appendChild(recieptEleOne);

      recieptEleOne.setAttribute(GCConstants.FILE_NAME, pdfGCInventoryReturnSlip);
    }
    if(YFCCommon.equals(failedReceipt, GCConstants.YES)){
      YFCDocument yfcFailedReturnSlipDoc = GCCommonUtil.invokeService(env, GCConstants.GC_FAILED_RETURN_SLIP, getOrderDetailOutDoc);
      if(LOGGER.isVerboseEnabled()){
        LOGGER.verbose("failed Receipt Document: "+ yfcFailedReturnSlipDoc.toString());
      }
      YFCElement elefailedReturnSlip = yfcFailedReturnSlipDoc.getDocumentElement();
      String pdfGCFailedInventorySlip = elefailedReturnSlip.getAttribute(GCConstants.PDF_NAME);
      YFCElement receiptEleOne = orderDoc.createElement(GCConstants.RECEIPT);
      recieptListEle.appendChild(receiptEleOne);
      receiptEleOne.setAttribute(GCConstants.FILE_NAME, pdfGCFailedInventorySlip);
    }
    LOGGER.verbose("GCGenerateReturnRecieptPrintWrapper.stampShipNode--End");
    LOGGER.endTimer("GCGenerateReturnRecieptPrintWrapper.stampShipNode");
    return orderDoc.getDocument();

  }


  /**
   * This method is used to remove component lines as they are not required for pdf.
   *
   * @param getOrderDetailOutDoc
   */
  private void removeComponentLine(YFCDocument getOrderDetailOutDoc) {
    YFCElement eleOrder = getOrderDetailOutDoc.getDocumentElement();
    YFCElement eleOrderLines = eleOrder.getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> nlOrderLine = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
    for (int i = 0; i < nlOrderLine.getLength(); i++) {
      YFCElement eleOrderLine = nlOrderLine.item(i);
      if (!YFCCommon.isVoid(eleOrderLine.getAttribute(GCConstants.BUNDLE_PARENT_ORDERLINEKEY))) {
        eleOrderLines.removeChild(eleOrderLine);
        i--;
      }
    }

  }

}
