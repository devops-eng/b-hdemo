package com.gc.api.returns;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.api.borderfree.GCProcessBorderfreeOrders;
import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCGenerateReturnRecieptShipNodeAddress implements YIFCustomApi {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCProcessBorderfreeOrders.class.getName());



  public Document stampShipNode(YFSEnvironment env, Document inputDoc) {
    LOGGER.beginTimer("GCGenerateReturnRecieptShipNodeAddress.stampShipNode");
    LOGGER.verbose("GCGenerateReturnRecieptShipNodeAddress.stampShipNode--Begin");


    // fetching the attributes from the inputDoc that is returned from the queue
    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("inDoc Document: " + inDoc.toString());
    }
    YFCElement inDocRootEle = inDoc.getDocumentElement();
    String orderNo = inDocRootEle.getAttribute(GCConstants.ORDER_NO);
    String enterpriseCode = inDocRootEle.getAttribute(GCConstants.ENTERPRISE_CODE);
    Map<String, String> commonmap = new HashMap<String, String>();
    Document commonCodeOutDocOne =
        GCCommonUtil.getCommonCodeListByTypeSortBy(env, GCConstants.GC_EXSTORE_LOS, enterpriseCode);
    YFCDocument commonCodeOutDoc = YFCDocument.getDocumentFor(commonCodeOutDocOne);
    YFCElement commonCodeOutDocrootElement = commonCodeOutDoc.getDocumentElement();
    YFCNodeList<YFCElement> listCommonCode = commonCodeOutDocrootElement.getElementsByTagName(GCConstants.COMMON_CODE);
    int k = listCommonCode.getLength();
    for (int l = 0; l < k; l++) {
      YFCElement eleCommonCode = listCommonCode.item(l);
      String strCodeValue = eleCommonCode.getAttribute(GCConstants.CODE_VALUE);
      String strCodeShortDescription = eleCommonCode.getAttribute(GCConstants.CODE_SHORT_DESCRIPTION);
      commonmap.put(strCodeValue, strCodeShortDescription);

    }


    String orderDate = inDocRootEle.getAttribute(GCConstants.ORDER_DATE);
    int orderDate2 = orderDate.lastIndexOf('T');
    if (orderDate2 > 0) {
      String orderDate1 = orderDate.substring(0, orderDate2);
      inDocRootEle.setAttribute(GCConstants.ORDER_DATE, orderDate1);
    }
    YFCElement extnEle = inDocRootEle.getChildElement(GCConstants.EXTN);
    YFCElement activityListsEle = extnEle.getChildElement(GCConstants.GC_USER_ACTIVITY_LIST);
    YFCElement userActivityEle = activityListsEle.getChildElement(GCConstants.GC_SHIPMENT_ACTICITY);
    if (!YFCCommon.isVoid(userActivityEle)) {
      String userId = userActivityEle.getAttribute(GCConstants.USER_ID);
      YFCDocument getUserListInDoc = YFCDocument.createDocument(GCConstants.USERLIST);
      YFCElement getUserListInDocRootEle = getUserListInDoc.getDocumentElement();
      getUserListInDocRootEle.setAttribute(GCConstants.LOGINID, userId);
      YFCDocument getUserListOutDoc = GCCommonUtil.invokeAPI(env, GCConstants.GET_USER_LIST, getUserListInDoc, null);
      YFCElement getUserListOutDocRootEle = getUserListOutDoc.getDocumentElement();
      String userName = getUserListOutDocRootEle.getAttribute(GCConstants.USRNAME);
      userActivityEle.setAttribute(GCConstants.USRNAME, userName);
    }
    YFCElement orderLinesEle = inDocRootEle.getChildElement(GCConstants.ORDER_LINES);
    YFCElement orderLineEle = orderLinesEle.getChildElement(GCConstants.ORDER_LINE);
    YFCElement devrivedOrderLine = orderLineEle.getChildElement(GCConstants.DERIVED_FROM_ORDER);
    String levelOfService = devrivedOrderLine.getAttribute(GCConstants.LEVEL_OF_SERVICE);
    String strMapLevelOfService = commonmap.get(levelOfService);
    devrivedOrderLine.setAttribute(GCConstants.LEVEL_OF_SERVICE, strMapLevelOfService);
    YFCElement itemDetailsEle = orderLineEle.getChildElement(GCConstants.ITEM_DETAILS);
    String webItemID = itemDetailsEle.getAttribute(GCConstants.ITEM_ID);
    YFCElement itemDetailsExtnEle = itemDetailsEle.getChildElement(GCConstants.EXTN);
    String posItemID = itemDetailsExtnEle.getAttribute(GCConstants.EXTN_POSITEM_ID);
    if (YFCCommon.isVoid(posItemID)) {
      itemDetailsExtnEle.setAttribute(GCConstants.EXTN_POSITEM_ID, webItemID);
    }

    String isStoreCredit = "N";
    String storeCreditNumber = GCConstants.BLANK;
    YFCNodeList<YFCElement> listOrderLine = orderLinesEle.getElementsByTagName(GCConstants.ORDER_LINE);
    YFCElement eleStoreCreditList = inDocRootEle.createChild("CustomStoreCredits");
    for (int i = 0; i < listOrderLine.getLength(); i++) {
      YFCElement eleOrderLine = listOrderLine.item(i);
      eleOrderLine.setAttribute(GCConstants.ORDER, orderNo);

      // Fetching StoreCredit
      YFCElement eleLineExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
      YFCElement eleLineOverallTotals = eleOrderLine.getChildElement("LineOverallTotals");
      String lineTotal = eleLineOverallTotals.getAttribute(GCConstants.LINE_TOTAL);
      if (!YFCCommon.isVoid(eleLineExtn)) {
        storeCreditNumber = eleLineExtn.getAttribute("ExtnPOS70TicketNo");
        if (!YFCCommon.isVoid(storeCreditNumber)) {
          YFCElement eleStoreCredit = eleStoreCreditList.createChild("CustomStoreCredit");
          eleStoreCredit.setAttribute("StoreCredit", storeCreditNumber + " (" + lineTotal + ")");
          isStoreCredit = "Y";
        }
      }
    }

    // Stamping IsStoreCredit
    inDocRootEle.setAttribute("IsStoreCredit", isStoreCredit);


    YFCElement lineCharges = orderLineEle.getChildElement(GCConstants.LINE_CHARGES);
    YFCElement lineCharge = lineCharges.getChildElement(GCConstants.LINE_CHARGE);
    if (!YFCCommon.isVoid(lineCharge)) {
      if (YFCUtils.equals("RestockingFee", lineCharge.getAttribute(GCConstants.CHARGE_CATEGORY))) {
        lineCharge.setAttribute("Attr1", lineCharge.getAttribute(GCConstants.CHARGE_AMOUNT));
      }
      if (YFCUtils.equals("ShippingRefund", lineCharge.getAttribute(GCConstants.CHARGE_CATEGORY))) {
        lineCharge.setAttribute("Attr2", lineCharge.getAttribute(GCConstants.CHARGE_AMOUNT));
      }
    }


    String receivingNode = orderLineEle.getAttribute(GCConstants.RECEIVING_NODE);

    YFCDocument orgListIp = YFCDocument.createDocument(GCConstants.ORGANIZATION);
    YFCElement orgEle = orgListIp.getDocumentElement();
    orgEle.setAttribute(GCConstants.ORGANIZATION_CODE, receivingNode);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("orgListIp Document: " + orgListIp.toString());
    }
    YFCDocument orgListOpDoc = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORGANIZATION_LIST, orgListIp, null);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("orgListOpDoc Document: " + orgListOpDoc.toString());
    }
    YFCElement orgListEle = orgListOpDoc.getDocumentElement();
    YFCElement eleOrganization = orgListEle.getChildElement(GCConstants.ORGANIZATION);
    YFCElement eleNode = eleOrganization.getChildElement(GCConstants.NODE);
    YFCElement shipNodePersonInfoEle = eleNode.getChildElement(GCConstants.SHIP_NODE_PERSON_INFO);

    String company = shipNodePersonInfoEle.getAttribute(GCConstants.COMPANY);
    String addressLineOne = shipNodePersonInfoEle.getAttribute(GCConstants.ADDRESS_LINE1);
    String addressLineTwo = shipNodePersonInfoEle.getAttribute(GCConstants.ADDRESS_LINE2);
    String city = shipNodePersonInfoEle.getAttribute(GCConstants.CITY);
    String state = shipNodePersonInfoEle.getAttribute(GCConstants.STATE);
    String zipCode = shipNodePersonInfoEle.getAttribute(GCConstants.ZIP_CODE);
    String country = shipNodePersonInfoEle.getAttribute(GCConstants.COUNTRY);
    String dayPhone = shipNodePersonInfoEle.getAttribute(GCConstants.DAY_PHONE);
    String emailId = shipNodePersonInfoEle.getAttribute(GCConstants.EMAIL_ID);

    YFCElement eleShipNode = inDocRootEle.createChild(GCConstants.RECEIVING_NODE);
    eleShipNode.setAttribute(GCConstants.SHIP_NODE, receivingNode);
    eleShipNode.setAttribute(GCConstants.COMPANY, company);
    eleShipNode.setAttribute(GCConstants.ADDRESS_LINE1, addressLineOne);
    eleShipNode.setAttribute(GCConstants.ADDRESS_LINE2, addressLineTwo);
    eleShipNode.setAttribute(GCConstants.CITY, city);
    eleShipNode.setAttribute(GCConstants.STATE, state);
    eleShipNode.setAttribute(GCConstants.ZIP_CODE, zipCode);
    eleShipNode.setAttribute(GCConstants.COUNTRY, country);
    eleShipNode.setAttribute(GCConstants.DAY_PHONE, dayPhone);
    eleShipNode.setAttribute(GCConstants.EMAIL_ID, emailId);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("inDoc Document: " + inDoc.toString());
    }
    LOGGER.verbose("GCGenerateReturnRecieptShipNodeAddress.stampShipNode--End");
    LOGGER.endTimer("GCGenerateReturnRecieptShipNodeAddress.stampShipNode");
    return inDoc.getDocument();

  }

  @Override
  public void setProperties(Properties arg0) {

  }

}
