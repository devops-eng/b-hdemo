package com.gc.api.returns;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class is used to call getCompleteOrderLineList API and stamp ShipNode Element as a child
 * element of OrderStatus with ShipNode & NodeType as attribute.
 *
 * @author shanil.sharma
 *
 */
public class GCGetCompleteOrderLineList implements YIFCustomApi {

  @Override
  public void setProperties(Properties arg0) throws Exception {

  }

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCGetCompleteOrderLineList.class.getName());

  public Document getCompleteOrderLineList(final YFSEnvironment env, final Document docInput) {

    LOGGER.beginTimer("GCGetCompleteOrderLineList.getCompleteOrderLineList");
    LOGGER.verbose("GCGetCompleteOrderLineList.getCompleteOrderLineList--Begin");
    YFCDocument inDoc = YFCDocument.getDocumentFor(docInput);

    LOGGER.verbose("inDoc : " + inDoc.toString());

    // Call getCompleteOrderLineList API
    Document docGetCompleteOrderLineListTmp =
        GCCommonUtil.getDocumentFromPath("/global/template/api/getCompleteOrderLineList.xml");
    YFCDocument getCompOdrLineTemp = YFCDocument.getDocumentFor(docGetCompleteOrderLineListTmp);
    YFCDocument docGetCompleteOrderLineListOP =
        GCCommonUtil.invokeAPI(env, "getCompleteOrderLineList", inDoc, getCompOdrLineTemp);
    LOGGER.verbose("docGetCompleteOrderLineListOP : " + docGetCompleteOrderLineListOP.toString());

    // This list is used to store different shipNodes present in the order.
    List<String> shipNodeList = new ArrayList<String>();
    YFCElement eleOrderLineList = docGetCompleteOrderLineListOP.getDocumentElement();
    YFCNodeList<YFCElement> nlOrderLine = eleOrderLineList.getElementsByTagName(GCConstants.ORDER_LINE);

    // Looping through all the orderLines to fetch shipNodes.
    for (YFCElement eleOrderLine : nlOrderLine) {
      YFCElement eleOrderStatuses = eleOrderLine.getChildElement(GCConstants.ORDER_STATUSES);
      if (!YFCCommon.isVoid(eleOrderStatuses)) {
        YFCNodeList<YFCElement> nlOrderStatus = eleOrderStatuses.getElementsByTagName(GCConstants.ORDER_STATUS);
        for (YFCElement eleOrderStatus : nlOrderStatus) {
          String shipNode = eleOrderStatus.getAttribute(GCConstants.SHIP_NODE);
          if (!YFCCommon.isVoid(shipNode) && !shipNodeList.contains(shipNode)) {
            shipNodeList.add(shipNode);
          }
        }
      }
    }
    LOGGER.verbose("shipNodeList : " + shipNodeList);
    // This map Stores ShipNode as key and its NodeType as value.
    Map<String, String> shipNodeTypeMap = new HashMap<String, String>();
    for (String shipNode : shipNodeList) {
      shipNodeTypeMap.put(shipNode, GCCommonUtil.getOrganizationNodeType(env, shipNode));
    }

    LOGGER.verbose("shipNodeTypeMap : " + shipNodeTypeMap);
    // Stamping ShipNode Element in inputDoc
    for (YFCElement eleOrderLine : nlOrderLine) {
      YFCElement eleOrderStatuses = eleOrderLine.getChildElement(GCConstants.ORDER_STATUSES);
      if (!YFCCommon.isVoid(eleOrderStatuses)) {
        YFCNodeList<YFCElement> nlOrderStatus = eleOrderStatuses.getElementsByTagName(GCConstants.ORDER_STATUS);
        for (YFCElement eleOrderStatus : nlOrderStatus) {
          String shipNode = eleOrderStatus.getAttribute(GCConstants.SHIP_NODE);
          YFCElement eleShipNode = eleOrderStatus.createChild(GCConstants.SHIP_NODE);
          eleShipNode.setAttribute(GCConstants.SHIP_NODE, shipNode);
          eleShipNode.setAttribute(GCConstants.NODE_TYPE, shipNodeTypeMap.get(shipNode));
        }
      }
    }
    LOGGER.verbose("Final Doc : " + docGetCompleteOrderLineListOP.toString());
    LOGGER.verbose("GCGetCompleteOrderLineList.getCompleteOrderLineList--End");
    LOGGER.endTimer("GCGetCompleteOrderLineList.getCompleteOrderLineList");
    return docGetCompleteOrderLineListOP.getDocument();

  }
}
