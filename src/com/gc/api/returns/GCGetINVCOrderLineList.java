package com.gc.api.returns;
/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: This class will get list of order lines which are in Return INVC status
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0              03/13/2015         Pande, Anuj                INVC Processing
 *#################################################################################################################################################################
 */
import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCGetINVCOrderLineList {
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCProcessINVCItems.class.getName());

  /**
   * This method is invoked from COM to get all order lines in INVC status
   * 
   * @param envIn
   * @param inputXML
   * @return
   */
  public Document getINVCOrderLineList(YFSEnvironment envIn, Document inputXML) {
    LOGGER.beginTimer("GCGetINVCOrderLineList.getINVCOrderLineList()");
    LOGGER.verbose("GCGetINVCOrderLineList.getINVCOrderLineList() - Start");
    YFCDocument yfcDocGetOrderLineListTemp =
        YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LINE_LIST_TEMPLATE_FOR_INVC);
    YFCDocument yfcDocGetOrderLineListOup =
        GCCommonUtil.invokeAPI(envIn, GCConstants.API_GET_ORDER_LINE_LIST, YFCDocument.getDocumentFor(inputXML),
            yfcDocGetOrderLineListTemp);
    YFCDocument yfcDocOrderLineListFinal = YFCDocument.createDocument(GCXmlLiterals.ORDER_LINE_LIST);
    YFCElement yfcEleOrderLineListRoot = yfcDocOrderLineListFinal.getDocumentElement();

    YFCNodeList<YFCElement> yfcNLOrderLine = yfcDocGetOrderLineListOup.getElementsByTagName(GCConstants.ORDER_LINE);

    for (int i = 0; i < yfcNLOrderLine.getLength(); i++) {
      YFCElement yfcEleOrderLine = yfcNLOrderLine.item(i);
      YFCElement yfcEleItem = yfcEleOrderLine.getChildElement(GCConstants.ITEM);

      String sIsBundleParent = yfcEleOrderLine.getAttribute(GCConstants.IS_BUNDLE_PARENT);
      String sProductClass = yfcEleItem.getAttribute(GCConstants.PRODUCT_CLASS);

      if ((YFCCommon.equals(sIsBundleParent, GCConstants.YES) && YFCCommon.equals(sProductClass, GCConstants.SET))
          || (YFCCommon.equals(sIsBundleParent, GCConstants.NO) && YFCCommon.equals(sProductClass, GCConstants.REGULAR))) {
        yfcEleOrderLineListRoot.importNode(yfcEleOrderLine);
      }
    }

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("yfcDocOrderLineListFinal" + yfcDocOrderLineListFinal);
    }

    LOGGER.endTimer("GCGetINVCOrderLineList.getINVCOrderLineList()");
    LOGGER.verbose("GCGetINVCOrderLineList.getINVCOrderLineList() - End");

    return yfcDocOrderLineListFinal.getDocument();
  }
}
