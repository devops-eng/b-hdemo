/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 * ####################################################################################################################
 *	OBJECTIVE: Write what this class is about
 *######################################################################################################################
 *	Version     Date       Modified By              Description
 *######################################################################################################################
    1.0      10/03/2015   Mittal, Yashu		GCSTORE-516: Design - DC Returns
 *######################################################################################################################
 */
package com.gc.api.returns;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCWebServiceUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:yashu.mittal@expicient.com">Yashu Mittal</a>
 */
public class GCGetReturnableSKU {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCGetReturnableSKU.class.getName());

  /**
   * 
   * This method will check for which orderlines SKUNumber and serial number is to be checked, and make Call to DAX for the eligible lines.
   *
   * @param env
   * @param inDoc
   * @return
   * @throws IOException
   *
   */
  public Document getReturnableSKU(YFSEnvironment env, Document inputDoc) throws IOException {

    LOGGER.beginTimer("GCGetReturnableSKU.getReturnableSKU");
    LOGGER.verbose("Class: GCGetReturnableSKU Method: getReturnableSKU START");
    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    if (LOGGER.isDebugEnabled()) {
      LOGGER.verbose("Input Doc is " + inDoc);
    }
    Document docGetReturnableSKU = GCCommonUtil.getDocumentFromPath("/global/template/api/extn_GetReturnableSKUTemplate.xml");
    YFCDocument inReqGetReturnableSKU = YFCDocument.getDocumentFor(docGetReturnableSKU);
    YFCElement inReqRootEle = inReqGetReturnableSKU.getDocumentElement();
    YFCElement eleProducts = inReqRootEle.getElementsByTagName("Products").item(0);

    //This map will store each eligible orderline number and a map containing orderlinekey, SKUNumber, SerialNumber
    Map<String, Map> listOLS = new HashMap<String, Map>();

    // Stamping required attribute on SOAP request message.
    updateGetReturnableSKUInDoc(inReqGetReturnableSKU, inDoc, eleProducts, listOLS);
    GCWebServiceUtil obj = new GCWebServiceUtil();

    LOGGER.endTimer("GCGetReturnableSKU.getReturnableSKU");
    LOGGER.verbose("Class: GCGetReturnableSKU Method: getReturnableSKU END");
    Document DocResponse = null;

    YFCNodeList<YFCElement> nlProduct = inReqRootEle.getElementsByTagName("Product");
    YFCDocument blankDoc = YFCDocument.createDocument(GCConstants.ORDER);
    if(nlProduct.getLength()>0){
    	try{
    		String sWebServiceURL = YFSSystem.getProperty(GCConstants.GET_RETURNABLE_SKU_WEBSERVICE_URL);
    		String sWebServiceAction = YFSSystem.getProperty(GCConstants.GET_RETURNABLE_SKU_WEBSERVICE_ACTION);
    		Document webServiceGetReturnableSKURes = obj.invokeSOAPWebService(inReqGetReturnableSKU.getDocument(), sWebServiceURL,sWebServiceAction);
    		NodeList nlGetReturnableSKUsResult = webServiceGetReturnableSKURes.getDocumentElement().getElementsByTagName("GetReturnableSKUsResult");
    		if(nlGetReturnableSKUsResult.getLength()>0){
    			Node nGetReturnableSKUsResult = nlGetReturnableSKUsResult.item(0);
    			String sGetReturnableSKUsResult = nGetReturnableSKUsResult.getTextContent();
    			if(!YFCCommon.isVoid(sGetReturnableSKUsResult)){
    				DocResponse = GCXMLUtil.getDocument(sGetReturnableSKUsResult);
    			}
    		}
    	} catch(Exception e){
    		LOGGER.error("Exception is :" , e);
    		return blankDoc.getDocument();
    	}
    }
    
    if(!YFCCommon.isVoid(DocResponse) && !listOLS.isEmpty()){
      LOGGER.verbose("Lines are eligible to be checked for changed SKUNumber or Serial Number");
      return changeOrderInput(DocResponse, listOLS, inDoc);
    }
    
    LOGGER.verbose("No lines eligible for change order.");
    return blankDoc.getDocument();

  }

  /**
   * 
   * This method will check which lines in the order are eligible to be checked for change in SKUNumber and Serial Number, 
   * and prepare input for the web service call.
   *
   * @param inReqGetReturnableSKU
   * @param inDoc
   * @param eleProducts
   *
   */
  private void updateGetReturnableSKUInDoc(YFCDocument inReqGetReturnableSKU, YFCDocument inDoc, YFCElement eleProducts, Map<String, Map> listOLS) {

    LOGGER.beginTimer("GCGetReturnableSKU.updateGetReturnableSKUInDoc");
    LOGGER.verbose("Class: GCGetReturnableSKU Method: updateGetReturnableSKUInDoc START");

    YFCElement inDocRootEle = inDoc.getDocumentElement();
    YFCElement eleOrderLines = inDocRootEle.getChildElement(GCConstants.ORDER_LINES);

    YFCIterable<YFCElement> listOrderLineList = eleOrderLines.getChildren(GCConstants.ORDER_LINE);

    for (YFCElement eleOrderLine : listOrderLineList) {

      YFCNodeList<YFCElement> eleOrderStatusesList = eleOrderLine.getElementsByTagName(GCConstants.ORDER_STATUS);
      String sICheckRequired = "N";
      String sPOSLocation = "";
      Double statusQty = 0.0;
      YFCElement eleOrderLineExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
      String sExtnIsStoreFulfilled = eleOrderLineExtn.getAttribute(GCXmlLiterals.EXTN_IS_STORE_FULFILLED);

      LOGGER.verbose("For each orderline, chech if any of the order status is 3700.50 or 3700.20");
      for(YFCElement eleOrderStatus : eleOrderStatusesList){
        String sStatus = eleOrderStatus.getAttribute(GCConstants.STATUS);
        String sShipNode = eleOrderStatus.getAttribute(GCConstants.SHIP_NODE);
        Double dStatusQty = eleOrderStatus.getDoubleAttribute("StatusQty");
        if(YFCCommon.equals(sStatus, GCConstants.SHIPPED_TO_CUSTOMER_ORDER_LINE_STATUS) || YFCCommon.equals(sStatus, GCConstants.CUSTOMER_PICKED_UP_ORDER_LINE_STATUS)){
          LOGGER.verbose("Line is eligible to be checked for changed SKU or Serial Number.");
          LOGGER.verbose("ShipNode is :" + sShipNode + "Status is :" + sStatus);
          sICheckRequired = "Y";
          sPOSLocation = sShipNode;
          statusQty = statusQty + dStatusQty;
          break;
        }
      }

      if (YFCCommon.equals(sExtnIsStoreFulfilled, GCConstants.YES) && YFCCommon.equals(sICheckRequired, GCConstants.YES)) {

        Map<String, String> ols = new HashMap<String, String>();
        ols.put("OrderLineKey", eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
        ols.put("SKUNumber", eleOrderLineExtn.getAttribute(GCXmlLiterals.EXTN_POS_SKU_NUMBER));
        ols.put("SerialNumber", eleOrderLineExtn.getAttribute(GCXmlLiterals.EXTN_ACTUAL_SERIAL_NUMBER));

        listOLS.put(eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO), ols);

        YFCElement eleProduct = inReqGetReturnableSKU.createElement("Product");
        eleProduct.setAttribute("type", "Regular");
        eleProducts.appendChild(eleProduct);
        YFCElement eleItem = eleOrderLine.getChildElement(GCConstants.ITEM_DETAILS);
        YFCElement eleItemExtn = eleItem.getChildElement(GCConstants.EXTN);

        YFCElement eleLineNumber = inReqGetReturnableSKU.createElement("LineNumber");
        eleLineNumber.setNodeValue(eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO));
        eleProduct.appendChild(eleLineNumber);

        YFCElement elePOSSalesTicket = inReqGetReturnableSKU.createElement("POSSalesTicket");
        elePOSSalesTicket.setNodeValue(eleOrderLineExtn.getAttribute(GCXmlLiterals.EXTN_POS_SALES_TICKET_NO));
        eleProduct.appendChild(elePOSSalesTicket);

        YFCElement elePOSLocation = inReqGetReturnableSKU.createElement("POSLocation");
        elePOSLocation.setNodeValue(sPOSLocation);
        eleProduct.appendChild(elePOSLocation);

        YFCElement eleSKU = inReqGetReturnableSKU.createElement("SKU");
        eleSKU.setNodeValue(eleItemExtn.getAttribute(GCXmlLiterals.EXTN_POS_ITEM_ID)); 
        eleProduct.appendChild(eleSKU);

        YFCElement eleSKUNumber = inReqGetReturnableSKU.createElement("SKUNumber");
        eleSKUNumber.setNodeValue(eleOrderLineExtn.getAttribute(GCXmlLiterals.EXTN_POS_SKU_NUMBER));
        eleProduct.appendChild(eleSKUNumber);

        YFCElement eleSerialNumber = inReqGetReturnableSKU.createElement("SerialNumber");
        eleSerialNumber.setNodeValue(eleOrderLineExtn.getAttribute(GCXmlLiterals.EXTN_ACTUAL_SERIAL_NUMBER));
        eleProduct.appendChild(eleSerialNumber);

        YFCElement eleQuantity = inReqGetReturnableSKU.createElement("Quantity");
        eleQuantity.setNodeValue(statusQty.intValue());
        eleProduct.appendChild(eleQuantity);

      }
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.verbose("Final webServiceReqDoc" + inReqGetReturnableSKU);
    }
    LOGGER.endTimer("GCGetReturnableSKU.updateGetReturnableSKUInDoc");
    LOGGER.verbose("Class: GCGetReturnableSKU Method: updateGetReturnableSKUInDoc END");
  }

  /**
   * 
   * This method will prepare change order document for the lines, whose SKUNumber or Serial Number is changed.
   *
   * @param webServiceGetReturnableSKURes
   * @param listOLS
   * @return 
   *
   */
  private Document changeOrderInput(Document DocResponse, Map<String, Map> listOLS, YFCDocument inDoc) {

    LOGGER.beginTimer("GCGetReturnableSKU.changeOrderInput");
    LOGGER.verbose("Class: GCGetReturnableSKU Method: changeOrderInput START");

    YFCDocument changeOrderInDoc = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleChangeOrderOrderLines = changeOrderInDoc.createElement(GCConstants.ORDER_LINES);
    YFCElement changeOrderRootEle = changeOrderInDoc.getDocumentElement();
    YFCElement inDocRootEle = inDoc.getDocumentElement();
    changeOrderRootEle.setAttribute(GCConstants.ORDER_HEADER_KEY, inDocRootEle.getAttribute(GCConstants.ORDER_HEADER_KEY));
    changeOrderRootEle.appendChild(eleChangeOrderOrderLines);

    // Fix for 3779 : Start
    YFCNodeList<YFCElement> listSKUNumbers = YFCDocument.getDocumentFor(DocResponse).getElementsByTagName(GCXmlLiterals.SKU_NUMBERS);
    // Fix for 3779 : End
    for (YFCElement eleSKUNumbers : listSKUNumbers) {

      YFCElement eleLineNumber = eleSKUNumbers.getElementsByTagName("LineNumber").item(0);
      YFCElement eleSKUNumber = eleSKUNumbers.getElementsByTagName("SKUNumber").item(0);
      YFCElement eleSerialNumber = eleSKUNumbers.getElementsByTagName("SerialNumber").item(0);
      String sSKUNumber = eleSKUNumber.getNodeValue();
      String sSerialNumber = eleSerialNumber.getNodeValue();
      String sLineNumber = eleLineNumber.getNodeValue();

      Map<String, String> sListLineNumber = listOLS.get(sLineNumber);
      if(!YFCCommon.isVoid(sListLineNumber)){

        String sListSKUNumber = sListLineNumber.get("SKUNumber");
        String sListSerialNumber = sListLineNumber.get("SerialNumber");
        String sListOrderLineNumber = sListLineNumber.get("OrderLineKey");
        if (!YFCCommon.equals(sListSKUNumber, sSKUNumber) || !YFCCommon.equals(sListSerialNumber, sSerialNumber)) {
          LOGGER.verbose("SKU Number or Serial Number is changed. Add line in changeorder input doc.");
          YFCElement eleChangeOrderOrderLine = changeOrderInDoc.createElement(GCConstants.ORDER_LINE);
          eleChangeOrderOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, sListOrderLineNumber);
          YFCElement eleChangeOrderOrderLineExtn = changeOrderInDoc.createElement(GCConstants.EXTN);
          eleChangeOrderOrderLineExtn.setAttribute(GCXmlLiterals.EXTN_POS_SKU_NUMBER, sSKUNumber);
          eleChangeOrderOrderLineExtn.setAttribute(GCXmlLiterals.EXTN_ACTUAL_SERIAL_NUMBER, sSerialNumber);
          eleChangeOrderOrderLine.appendChild(eleChangeOrderOrderLineExtn);
          eleChangeOrderOrderLines.appendChild(eleChangeOrderOrderLine);
        }
      }
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.verbose("ChangeOrderInput is : " + changeOrderInDoc);
    }
    return changeOrderInDoc.getDocument();

  }
}
