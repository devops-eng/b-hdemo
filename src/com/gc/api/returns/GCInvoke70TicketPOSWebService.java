/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class will post 70Ticket details to POS, fetch the response from POS and stamp POSDAXCOATicket on orderlines.
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               27/03/2015        Mittal, Yashu		GCSTORE-556, GCSTORE-754
 *#################################################################################################################################################################
 */
package com.gc.api.returns;

import java.io.IOException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCWebServiceUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:yashu.mittal@expicient.com">Yashu Mittal</a>
 */
public class GCInvoke70TicketPOSWebService {
  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCInvoke70TicketPOSWebService.class.getName());

  /**
   * 
   * Description of create70TicketWithPOS
   *
   * @param env
   * @param inputDoc
   * @throws IOException 
   *
   */
  public void create70TicketWithPOS(YFSEnvironment env, Document inputDoc) throws IOException {
    
    LOGGER.beginTimer("GCInvoke70TicketPOSWebService.create70TicketWithPOS");
    LOGGER.verbose("Class: GCInvoke70TicketPOSWebService Method: create70TicketWithPOS START");

    YFCDocument yfcDocInDoc =  YFCDocument.getDocumentFor(inputDoc);

    if(LOGGER.isDebugEnabled()){
      LOGGER.verbose("Input doc in GCInvoke70TicketPOSWebService.create70TicketWithPOS method is:-->" + yfcDocInDoc.toString());
    }
    yfcDocInDoc.getDocumentElement().setAttribute(GCConstants.MESSAGE_TYPE, GCConstants.MESSAGE_TYPE_CREATE_70_TICKET);
    YFCDocument yfcDocInvokeWebServiceInput =
        GCCommonUtil.invokeService(env, GCXmlLiterals.GC_PREPARE_POS_WEB_SERVICE_INPUT, yfcDocInDoc);
    
    if(LOGGER.isDebugEnabled()){
        LOGGER.verbose("Input Doc to 70Ticket Webservice :-->" + yfcDocInvokeWebServiceInput.toString());
      }
    GCWebServiceUtil obj = new GCWebServiceUtil();
    try{
    	Document docWebServiceOP =
    			obj.invokeSOAPWebService(yfcDocInvokeWebServiceInput.getDocument(),
    					YFSSystem.getProperty(GCConstants.GET_70TICKET_WEBSERVICE_URL), YFSSystem.getProperty(GCConstants.GET_70TICKET_WEBSERVICE_ACTION));
    	if(LOGGER.isDebugEnabled()){
    		LOGGER.verbose("Output received from 70Ticket Webservice :-->" + YFCDocument.getDocumentFor(docWebServiceOP).toString());
    	}
    	
//    	String sOrderInvoiceKey = yfcDocInDoc.getDocumentElement().getAttribute("OrderInvoiceKey");
//    	GCSTORE - 6673 Start  
    	
    	YFCElement eleInvoiceDetaiForKey = yfcDocInDoc.getDocumentElement();
    	YFCElement eleInvoiceHeaderForKey = eleInvoiceDetaiForKey.getChildElement("InvoiceHeader");
    	String sOrderInvoiceKey = eleInvoiceHeaderForKey.getAttribute("OrderInvoiceKey");
    	LOGGER.verbose("sOrderInvoiceKey Value "+sOrderInvoiceKey);
    	
//    	GCSTORE- 6673 End
    	NodeList nlReturnResultDetail = docWebServiceOP.getDocumentElement().getElementsByTagName("ReturnResult");
    	NodeList nlFaultString = docWebServiceOP.getDocumentElement().getElementsByTagName("faultstring");
    	if(nlReturnResultDetail.getLength()>0){
    		Element eleReturnResultDetail = (Element) nlReturnResultDetail.item(0);
    		String sReturnResult = eleReturnResultDetail.getTextContent();
    		if(!YFCCommon.isVoid(sReturnResult)){
    			YFCDocument yfcDocResponse = YFCDocument.getDocumentFor(sReturnResult);
    			YFCElement yfcEleResponse = yfcDocResponse.getDocumentElement();
    			YFCElement eleSuccess = yfcEleResponse.getElementsByTagName(GCConstants.SUCCESS).item(0);
  			  	String sSuccess =  eleSuccess.getNodeValue();
  			  if(YFCCommon.equals(sSuccess, GCConstants.TRUE)){
  				  
  				 // Fix to stamp 70Ticket on Invoice
  				YFCDocument changeOrderInvoice = YFCDocument.createDocument(GCConstants.ORDER_INVOICE);
  		        YFCElement eleOrderInvoice = changeOrderInvoice.getDocumentElement();
  		        eleOrderInvoice.setAttribute(GCConstants.ORDER_INVOICE_KEY, sOrderInvoiceKey);
  		        eleOrderInvoice.setAttribute("StampTickets", "True");
  		        YFCElement eleExtn = eleOrderInvoice.createChild(GCConstants.EXTN); 
  				  
  				 //
  				YFCNodeList<YFCElement> nlResponse = yfcEleResponse.getElementsByTagName(GCXmlLiterals.POS_DAX_COA_TICKET);
    			YFCElement eResponse = nlResponse.item(0);
    			if(!YFCCommon.isVoid(eResponse)){
    				String sDAXCOATicket = eResponse.getNodeValue();
    				LOGGER.verbose("DAXCOA Ticket received from POS Response is " + sDAXCOATicket);
    				if (!YFCCommon.isVoid(sDAXCOATicket)) {
    					env.setTxnObject(GCXmlLiterals.POS_DAX_COA_TICKET, sDAXCOATicket);
    					eleExtn.setAttribute(GCXmlLiterals.EXTN_POS_70_TICKET_NO, sDAXCOATicket);
    					append70TicketOrReturn11TicketAtOrderLine(env, sDAXCOATicket, null, yfcDocInDoc);
    					// Fix to stamp 70Ticket on Invoice : Start
    					GCCommonUtil.invokeService(env, GCConstants.GC_PROCESS_POS_RESPONSE_SERVICE,changeOrderInvoice.getDocument());          
    					// Fix to stamp 70Ticket on Invoice : End
    				} else {
    					LOGGER.verbose("DAXCOATicket is not received. Raising alert.");
    					inputDoc.getDocumentElement().setAttribute("POSError", "DAXCOATicket is not present in the response received.");
    					GCCommonUtil.invokeService(env, GCConstants.GC_POS_CONNECT_FAILED_ALERT_SERVICE, inputDoc);
    				}
    			} else{
    				LOGGER.verbose("No reponse is received from POS. Raising alert.");
    				inputDoc.getDocumentElement().setAttribute("POSError", "No Response is present in the response received from POS.");
    				GCCommonUtil.invokeService(env, GCConstants.GC_POS_CONNECT_FAILED_ALERT_SERVICE, inputDoc);
    			}
  				  
  			  } else{
  				LOGGER.verbose("Success is false.");
  				YFCNodeList<YFCElement> nlErrorinfo = yfcEleResponse.getElementsByTagName("Errorinfo");
				  YFCElement eleErrorinfo = nlErrorinfo.item(0);
				  if(!YFCCommon.isVoid(eleErrorinfo)){
					  String sErrorinfo =  eleErrorinfo.getNodeValue();
					  inputDoc.getDocumentElement().setAttribute("POSError", sErrorinfo);
				  } else{
					  inputDoc.getDocumentElement().setAttribute("POSError", "ReturnResult does not contain any valid response.");
				  }
    			GCCommonUtil.invokeService(env, GCConstants.GC_POS_CONNECT_FAILED_ALERT_SERVICE, inputDoc);
  			  }
    		} else {
    			LOGGER.verbose("ReturnResult Element does not contain valid output. Raising alert.");
    			inputDoc.getDocumentElement().setAttribute("POSError", "No Valid results received from POS");
    			GCCommonUtil.invokeService(env, GCConstants.GC_POS_CONNECT_FAILED_ALERT_SERVICE, inputDoc);
				}
			} else if (nlFaultString.getLength() > 0) {
				Element eleFaultString = (Element) nlFaultString.item(0);
	        	String sFaultString = eleFaultString.getTextContent();
	        	inputDoc.getDocumentElement().setAttribute("POSError", sFaultString);
	        	GCCommonUtil.invokeService(env, GCConstants.GC_POS_CONNECT_FAILED_ALERT_SERVICE, inputDoc);

			} else {
    		LOGGER.verbose("ReturnResult Element is not present in POS Output. Raising alert.");
    		inputDoc.getDocumentElement().setAttribute("POSError", "No Valid results received from POS");
    		GCCommonUtil.invokeService(env, GCConstants.GC_POS_CONNECT_FAILED_ALERT_SERVICE, inputDoc);
    	}

    } catch(IOException e){
    	LOGGER.error("Could not connect to POS. Raising alert ", e);
    	LOGGER.verbose("Could not connect to POS. Raising alert.");
    	inputDoc.getDocumentElement().setAttribute("POSError", "Connection to POS Failed");
    	GCCommonUtil.invokeService(env, GCConstants.GC_POS_CONNECT_FAILED_ALERT_SERVICE, inputDoc);
    	LOGGER.error("Could not connect to POS", e);
    }
    
    LOGGER.endTimer("GCInvoke70TicketPOSWebService.create70TicketWithPOS");
    LOGGER.verbose("Class: GCInvoke70TicketPOSWebService Method: create70TicketWithPOS END");
  }

  /**
   * 
   * This method will stamp sDAXCOATicket on all order lines. 
   *
   * @param env
   * @param sDAXCOATicket
   * @param inDoc 
   *
   */
  public static void append70TicketOrReturn11TicketAtOrderLine(YFSEnvironment env, String sDAXCOATicket, String sPOSReturn11TicketNumber, YFCDocument inDoc) {
    
    LOGGER.beginTimer("GCInvoke70TicketPOSWebService.append70TicketOrReturn11TicketAtOrderLine");
    LOGGER.verbose("Class: GCInvoke70TicketPOSWebService Method: append70TicketOrReturn11TicketAtOrderLine START");
    
    if(LOGGER.isDebugEnabled()){
      LOGGER.verbose("Input doc in GCInvoke70TicketPOSWebService.append70TicketOrReturn11TicketAtOrderLine is : " + inDoc.toString());
      LOGGER.verbose("POSReturn11TicketNumber is:-->" + sPOSReturn11TicketNumber);
      LOGGER.verbose("DAXCOATicket number is:-->" + sDAXCOATicket);
    }
    
    YFCElement eleRoot = inDoc.getDocumentElement();
    YFCElement eleOrder = eleRoot.getElementsByTagName(GCConstants.ORDER).item(0);
    String sOrderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
    YFCDocument docChangeOrder = YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "' Override='Y'/>");
    YFCElement eleOrderLines = docChangeOrder.createElement(GCConstants.ORDER_LINES);
    docChangeOrder.getDocumentElement().appendChild(eleOrderLines);
    YFCElement eleLineDetails = eleRoot.getElementsByTagName(GCConstants.LINE_DETAILS).item(0);
    
    YFCNodeList<YFCElement> nlOrderLine = eleLineDetails.getElementsByTagName(GCConstants.LINE_DETAIL);
    for (YFCElement eleLineDetail : nlOrderLine ) {
      YFCElement eleOrderLine = eleLineDetail.getChildElement(GCConstants.ORDER_LINE);
      String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      YFCElement eleChangeOrderLine = docChangeOrder.createElement(GCConstants.ORDER_LINE);
      eleChangeOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, sOrderLineKey);
      YFCElement eleChangeOrderLineExtn = docChangeOrder.createElement(GCConstants.EXTN);
      if(!YFCCommon.isVoid(sDAXCOATicket)){
        eleChangeOrderLineExtn.setAttribute(GCXmlLiterals.EXTN_POS_70_TICKET_NO, sDAXCOATicket); 
      }
      if (!YFCCommon.isVoid(sPOSReturn11TicketNumber)){
        eleChangeOrderLineExtn.setAttribute(GCXmlLiterals.EXTN_POS_RETURN_11_TICKET_NO, sPOSReturn11TicketNumber);
      }
      
      eleChangeOrderLine.appendChild(eleChangeOrderLineExtn);
      eleOrderLines.appendChild(eleChangeOrderLine);
    }
    if(LOGGER.isDebugEnabled()){
        LOGGER.verbose("ChangeOrderDoc prepared to stamp DAXCOATicket OR  POSReturn11TicketNumber: " + docChangeOrder.toString());
      }
    YFCNodeList<YFCElement> nlChangeOrderLine = docChangeOrder.getElementsByTagName(GCConstants.ORDER_LINE);
    if (nlChangeOrderLine.getLength() > 0) {
      GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, docChangeOrder, null);
    }
    
    LOGGER.endTimer("GCInvoke70TicketPOSWebService.append70TicketOrReturn11TicketAtOrderLine");
    LOGGER.verbose("Class: GCInvoke70TicketPOSWebService Method: append70TicketOrReturn11TicketAtOrderLine END");
    
  }
}
