   /** 
* Copyright � 2014, Guitar Center,  All rights reserved.
* ####################################################################################################################
*	OBJECTIVE: This class will created input document for invoice posted to POS from the invoice details published by OMS.
*######################################################################################################################
*	Version     Date       Modified By              Description
*######################################################################################################################
    1.0      27/03/2015   Mittal, Yashu		GCSTORE-556, GCSTORE-754
*######################################################################################################################
 */
package com.gc.api.returns;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:yashu.mittal@expicient.com">Yashu Mittal</a>
 */
public class GCPostInvTransferRefToPOS {
  
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCPostInvTransferRefToPOS.class.getName());

 /**
  * 
  * This method will get the invoice and prepare the inventory transfer reference from invoice to be sent to POS.
  *
  * @param env
  * @param inDoc : inDoc containing invoice details.
  * @return 
  *
  */
  public Document postInvTransferRefToPOS(YFSEnvironment env, Document inDoc) {

    LOGGER.beginTimer("GCPostInvTransferRefToPOS.postInvTransferRefToPOS");
    LOGGER.verbose("Class: GCPostInvTransferRefToPOS Method: postInvTransferRefToPOS START");
    
    YFCDocument inputDocument = YFCDocument.getDocumentFor(inDoc);
    if(LOGGER.isDebugEnabled()){
      LOGGER.verbose("Input Document to GCPostInvTransferRefToPOS.postInvTransferRefToPOS method :-->" + inputDocument.toString());
    }
    YFCElement inputDocRootEle = inputDocument.getDocumentElement();
    YFCElement eleInvoiceHeader = inputDocRootEle.getChildElement(GCConstants.INVOICE_HEADER);
    YFCElement eleOrder = eleInvoiceHeader.getChildElement(GCConstants.ORDER);
    YFCElement eleOrderExtn = eleOrder.getChildElement(GCConstants.EXTN);
    YFCElement eleGCUserActivity = eleOrderExtn.getElementsByTagName(GCConstants.GC_USER_ACTIVITY).item(0);
    
    YFCDocument outDoc = YFCDocument.createDocument(GCXmlLiterals.RETURNS);
    YFCElement outDocRoolEle = outDoc.getDocumentElement();
    
    YFCElement eleReturn = outDoc.createElement(GCXmlLiterals.RETURN);
    outDocRoolEle.appendChild(eleReturn);
    
    YFCElement eleOrderDate = outDoc.createElement("OrderDate");
    eleOrderDate.setNodeValue(eleOrder.getAttribute(GCConstants.ORDER_DATE));
    eleReturn.appendChild(eleOrderDate);
    
    YFCElement eleOrderTime = outDoc.createElement("OrderTime");
    eleOrderTime.setNodeValue(eleOrder.getAttribute(GCConstants.ORDER_DATE));
    eleReturn.appendChild(eleOrderTime);
    
    YFCElement eleOrderID = outDoc.createElement("OrderID");
    eleOrderID.setNodeValue(eleInvoiceHeader.getAttribute(GCConstants.ORDER_NO));
    eleReturn.appendChild(eleOrderID);
    
    YFCElement eleMgrAppv = outDoc.createElement("MgrAppv");
    eleMgrAppv.setNodeValue(eleGCUserActivity.getAttribute(GCConstants.MANAGER_ID));
    eleReturn.appendChild(eleMgrAppv);
    
    YFCElement eleCashierAppv = outDoc.createElement("CashierAppv");
    eleCashierAppv.setNodeValue(eleGCUserActivity.getAttribute(GCConstants.USER_ID));
    eleReturn.appendChild(eleCashierAppv);
    
    YFCElement eleSourceCode = outDoc.createElement("SourceCode");
    eleSourceCode.setNodeValue(eleOrderExtn.getAttribute(GCConstants.SOURCE_CODE));
    eleReturn.appendChild(eleSourceCode);
    
    YFCElement elePOSReturnLoc = outDoc.createElement("POSReturnLoc");
    eleReturn.appendChild(elePOSReturnLoc);   
    YFCElement eleRISNumber = outDoc.createElement("RISNumber");
    eleReturn.appendChild(eleRISNumber);
    
    YFCElement elePOSDAXCOATicket = outDoc.createElement("POSDAXCOATicket");
    eleReturn.appendChild(elePOSDAXCOATicket);
    
    YFCElement eleProducts = outDoc.createElement("Products");
    eleReturn.appendChild(eleProducts);
    
    YFCNodeList<YFCElement> nlLinDetails = eleInvoiceHeader.getElementsByTagName(GCConstants.LINE_DETAIL);
    for(YFCElement eleLineDetail : nlLinDetails){
      
      YFCElement eleOrderLine = eleLineDetail.getChildElement(GCConstants.ORDER_LINE);
      YFCElement eleOrderLineExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
      String sDispositionCode = eleOrderLine.getAttribute(GCXmlLiterals.DISPOSITION_CODE);
      
      if(YFCCommon.equals(sDispositionCode, GCConstants.RECEIVE_IN_STORE_DISPOSITION_CODE)){
        
    
        elePOSReturnLoc.setNodeValue(eleOrderLine.getAttribute(GCXmlLiterals.RECEIVING_NODE));
        eleRISNumber.setNodeValue(eleOrderLineExtn.getAttribute(GCXmlLiterals.EXTN_RETURN_RIS_NO));
        elePOSDAXCOATicket.setNodeValue(eleOrderLineExtn.getAttribute(GCXmlLiterals.EXTN_POS_70_TICKET_NO));
        
        YFCElement eleItemDetails = eleOrderLine.getChildElement(GCConstants.ITEM_DETAILS);
        YFCElement eleItemExtn = eleItemDetails.getChildElement(GCConstants.EXTN);
        
        YFCElement eleProduct = outDoc.createElement("Product");
        eleProducts.appendChild(eleProduct);
        
        YFCElement eleLineNumber = outDoc.createElement("LineNumber");
        eleLineNumber.setNodeValue(eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO));
        eleProduct.appendChild(eleLineNumber);
        
        YFCElement eleSKU = outDoc.createElement("SKU");
        eleSKU.setNodeValue(eleItemExtn.getAttribute(GCConstants.EXTN_POSITEM_ID));
        eleProduct.appendChild(eleSKU);
        
        YFCElement eleQuantity = outDoc.createElement("Quantity");
        eleQuantity.setNodeValue(eleLineDetail.getAttribute(GCConstants.QUANTITY));
        eleProduct.appendChild(eleQuantity);
        
        YFCElement eleSKUCost = outDoc.createElement("SKUCost");
        eleSKUCost.setNodeValue(eleItemExtn.getAttribute(GCXmlLiterals.EXTN_INVENTORY_COST));
        eleProduct.appendChild(eleSKUCost);
        
        YFCElement eleReasonCode = outDoc.createElement("ReasonCode");
        eleReasonCode.setNodeValue(eleOrderLine.getAttribute(GCConstants.RETURN_REASON));
        eleProduct.appendChild(eleReasonCode);
      }  
    }   

    if(LOGGER.isDebugEnabled()){
      LOGGER.verbose("Output Document posted to POS for Inventory Transfer Reference is :-->" + outDoc.toString());
    }
    LOGGER.endTimer("GCPostInvTransferRefToPOS.postInvTransferRefToPOS");
    LOGGER.verbose("Class: GCPostInvTransferRefToPOS Method: postInvTransferRefToPOS END");
    return outDoc.getDocument();
  }
}
