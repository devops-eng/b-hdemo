/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: This class will process INVC items either as Closed by DC or Returned to Store
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0              03/13/2015         Pande, Anuj                INVC Processing
 *#################################################################################################################################################################
 */
package com.gc.api.returns;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCProcessINVCItems {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCProcessINVCItems.class.getName());

  /**
   * This method will process items as either closed by INVC or returned to store for items in input
   * XML
   *
   * @param env
   * @param inDoc
   */
  public Document processINVCItems(YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("GCProcessINVCItems.processINVCItems()");
    LOGGER.verbose("GCProcessINVCItems.processINVCItems() - Start");
    YFCDocument yfcDocInDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement yfcEleRoot = yfcDocInDoc.getDocumentElement();

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input Document" + yfcDocInDoc);
    }

    String sStatus = GCConstants.BLANK;
    String sActionType = yfcEleRoot.getAttribute(GCXmlLiterals.ACTION_TYPE);
    YFCDocument yfcDocMultiAPIInput = YFCDocument.createDocument(GCXmlLiterals.MULTI_API);
    Map<String, YFCDocument> mapOrderNos = new HashMap<String, YFCDocument>();

    if (YFCCommon.equals(GCConstants.CLOSED_BY_INVC, sActionType)) {
      sStatus = GCConstants.CLOSED_BY_INVC_CODE;
    } else if (YFCCommon.equals(GCConstants.RETURNED_TO_STORE, sActionType)) {
      sStatus = GCConstants.RETURNED_TO_STORE_CODE;
    }
    LOGGER.verbose("Status Code: " + sStatus);

    YFCNodeList<YFCElement> yfcNLOrderLine = yfcEleRoot.getElementsByTagName(GCConstants.ORDER_LINE);
    for (int i = 0; i < yfcNLOrderLine.getLength(); i++) {
      YFCElement yfcEleOrderLine = yfcNLOrderLine.item(i);
      String sOrderHeaderKey = yfcEleOrderLine.getAttribute(GCConstants.ORDER_HEADER_KEY);

      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("mapOrderNos: " + mapOrderNos);
      }

      if (mapOrderNos.containsKey(sOrderHeaderKey)) {
        appendOrderStatusChange(env, yfcDocMultiAPIInput, yfcEleOrderLine, mapOrderNos, sStatus, true);
      } else {
        appendOrderStatusChange(env, yfcDocMultiAPIInput, yfcEleOrderLine, mapOrderNos, sStatus, false);
      }
      // End of order lines from input
    }

    // invoke change order status API
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Multi API Doc: " + yfcDocMultiAPIInput);
    }
    GCCommonUtil.invokeAPI(env, GCConstants.API_MULTIAPI, yfcDocMultiAPIInput, null);

    LOGGER.endTimer("GCProcessINVCItems.processINVCItems()");
    LOGGER.verbose("GCProcessINVCItems.:processINVCItems() - End");
    return inDoc;
  }

  /**
   * This method will append order status change details. If flag is passed as true, it will fetch
   * the existing order status change element
   *
   * @param env
   * @param yfcDocMultiAPIInput
   * @param yfcEleOrderLine
   * @param sStatus
   * @param isExistingLine
   * @return
   */
  private YFCDocument appendOrderStatusChange(YFSEnvironment env, YFCDocument yfcDocMultiAPIInput,
      YFCElement yfcEleOrderLine, Map<String, YFCDocument> mapOrderNos, String sStatus, boolean isExistingLine) {

    LOGGER.beginTimer("GCProcessINVCItems.appendOrderStatusChange()");
    LOGGER.verbose("GCProcessINVCItems.appendOrderStatusChange() - Start");

    YFCElement yfcEleMultiAPI = yfcDocMultiAPIInput.getDocumentElement();
    // Fetch required attributes
    String sOrderHeaderKey = yfcEleOrderLine.getAttribute(GCConstants.ORDER_HEADER_KEY);
    String sOrderLineKey = yfcEleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
    String sPrimeLineNo = yfcEleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
    String sSubLineNo = yfcEleOrderLine.getAttribute(GCConstants.SUB_LINE_NO);
    String sQuantity = yfcEleOrderLine.getAttribute(GCConstants.ORDERED_QTY);
    String sTransactionID = GCConstants.PROCESS_INVC_TRANSACTION_ID;

    YFCElement yfcEleOrderLines = yfcDocMultiAPIInput.createElement(GCConstants.ORDER_LINES);

    LOGGER.verbose("isExistingLine: " + isExistingLine);
    if (isExistingLine) {
      // If order header key exists in multi API document, append order line to existing Order
      // status changes xml
      LOGGER.verbose("Line can be appended to an existing change order status XML in multi API document");
      YFCNodeList<YFCElement> yfcNLOrderStatusChange =
          yfcDocMultiAPIInput.getElementsByTagName(GCConstants.ORDER_STATUS_CHANGE);

      for (int i = 0; i < yfcNLOrderStatusChange.getLength(); i++) {
        YFCElement yfcEleOrderStatusChange = yfcNLOrderStatusChange.item(i);
        String sExistingHeaderKey = yfcEleOrderStatusChange.getAttribute(GCConstants.ORDER_HEADER_KEY);
        if (YFCCommon.equals(sOrderHeaderKey, sExistingHeaderKey)) {
          yfcEleOrderLines = yfcEleOrderStatusChange.getChildElement(GCConstants.ORDER_LINES);
          break;
        }
      }
    } else {
      // If order header key does not exists in multi API document, create a new Order status
      // changes xml
      LOGGER.verbose("There is no existing change order status XML for this order, create and append new");
      YFCElement yfcEleAPI = yfcDocMultiAPIInput.createElement(GCXmlLiterals.API);
      yfcEleMultiAPI.appendChild(yfcEleAPI);
      yfcEleAPI.setAttribute(GCConstants.NAME, GCConstants.API_CHANGE_ORDER_STATUS);
      YFCElement yfcEleInput = yfcDocMultiAPIInput.createElement(GCXmlLiterals.INPUT);
      yfcEleAPI.appendChild(yfcEleInput);
      YFCElement yfcEleOrderStatusChanges = yfcDocMultiAPIInput.createElement(GCConstants.ORDER_STATUS_CHANGE);
      yfcEleInput.appendChild(yfcEleOrderStatusChanges);
      yfcEleOrderStatusChanges.appendChild(yfcEleOrderLines);
      yfcEleOrderStatusChanges.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
      yfcEleOrderStatusChanges.setAttribute(GCConstants.TRANSACTION_ID, sTransactionID);
    }

    // Stamp Order line required details & fields
    YFCElement yfcEleOrderLineIn = yfcDocMultiAPIInput.createElement(GCConstants.ORDER_LINE);
    yfcEleOrderLines.appendChild(yfcEleOrderLineIn);
    YFCElement yfcEleOrderLineTranQty = yfcDocMultiAPIInput.createElement(GCConstants.ORDER_LINE_TRAN_QUANTITY);
    yfcEleOrderLineIn.appendChild(yfcEleOrderLineTranQty);

    yfcEleOrderLineIn.setAttribute(GCConstants.ORDER_LINE_KEY, sOrderLineKey);
    yfcEleOrderLineIn.setAttribute(GCConstants.BASE_DROP_STATUS, sStatus);
    yfcEleOrderLineIn.setAttribute(GCConstants.PRIME_LINE_NO, sPrimeLineNo);
    yfcEleOrderLineIn.setAttribute(GCConstants.SUB_LINE_NO, sSubLineNo);
    yfcEleOrderLineTranQty.setAttribute(GCConstants.QUANTITY, sQuantity);

    // Check If Item product class is SET
    YFCElement yfcEleItem = yfcEleOrderLine.getChildElement(GCConstants.ITEM);
    String sProductClass = yfcEleItem.getAttribute(GCConstants.PRODUCT_CLASS);
    String sIsBundleParent = yfcEleOrderLine.getAttribute(GCConstants.IS_BUNDLE_PARENT);

    if (YFCCommon.equals(sProductClass, GCConstants.SET) && YFCCommon.equals(sIsBundleParent, GCConstants.YES)) {
      LOGGER.verbose("Line is a bundle parent and product class is SET");
      YFCDocument yfcDocGetOrderListOup = null;
      if (isExistingLine) {
        yfcDocGetOrderListOup = mapOrderNos.get(sOrderHeaderKey);
        if (YFCCommon.isVoid(yfcDocGetOrderListOup)) {
          YFCDocument yfcDocGetOrderListTemp =
              YFCDocument.getDocumentFor(GCConstants.GET_ORDERLIST_TEMPLATE_FOR_INVC_PROCESSING);
          yfcDocGetOrderListOup = GCCommonUtil.invokeGetOrderListAPI(env, sOrderHeaderKey, yfcDocGetOrderListTemp);
        }
      } else {
        YFCDocument yfcDocGetOrderListTemp =
            YFCDocument.getDocumentFor(GCConstants.GET_ORDERLIST_TEMPLATE_FOR_INVC_PROCESSING);
        yfcDocGetOrderListOup = GCCommonUtil.invokeGetOrderListAPI(env, sOrderHeaderKey, yfcDocGetOrderListTemp);
      }

      YFCNodeList<YFCElement> yfcNLComponentOrderLine =
          yfcDocGetOrderListOup.getElementsByTagName(GCConstants.ORDER_LINE);

      // Loop over to find and process component order lines
      for (int j = 0; j < yfcNLComponentOrderLine.getLength(); j++) {
        YFCElement yfcEleComponentOrderLine = yfcNLComponentOrderLine.item(j);
        String sChildOrderLineKey = yfcEleComponentOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
        String sBundleParentOrderLineKey =
            yfcEleComponentOrderLine.getAttribute(GCConstants.BUNDLE_PARENT_ORDERLINEKEY);
        String sCompPrimeLineNo = yfcEleComponentOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
        String sCompSubLineNo = yfcEleComponentOrderLine.getAttribute(GCConstants.SUB_LINE_NO);

        if (YFCCommon.equals(sBundleParentOrderLineKey, sOrderLineKey)) {
          YFCElement yfcEleChildOrderLine = yfcDocMultiAPIInput.createElement(GCConstants.ORDER_LINE);
          yfcEleOrderLines.appendChild(yfcEleChildOrderLine);
          YFCElement yfcEleChildOrderLineTranQty =
              yfcDocMultiAPIInput.createElement(GCConstants.ORDER_LINE_TRAN_QUANTITY);
          yfcEleChildOrderLine.appendChild(yfcEleChildOrderLineTranQty);
          Double dCompQty =
              yfcEleOrderLine.getDoubleAttribute(GCConstants.ORDERED_QTY)
              * yfcEleComponentOrderLine.getDoubleAttribute(GCXmlLiterals.KIT_QTY);
          // Stamp component line Key
          LOGGER.verbose("Final Component Qty: " + dCompQty.toString());
          yfcEleChildOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, sChildOrderLineKey);
          yfcEleChildOrderLine.setAttribute(GCConstants.BASE_DROP_STATUS, sStatus);
          yfcEleChildOrderLine.setAttribute(GCConstants.PRIME_LINE_NO, sCompPrimeLineNo);
          yfcEleChildOrderLine.setAttribute(GCConstants.SUB_LINE_NO, sCompSubLineNo);
          yfcEleChildOrderLineTranQty.setAttribute(GCConstants.QUANTITY, dCompQty.toString());
        }
      }
      // End of for loop over get order list lines-
      mapOrderNos.put(sOrderHeaderKey, yfcDocGetOrderListOup);

      // End of if condition where Product class is SET
    } else {
      mapOrderNos.put(sOrderHeaderKey, null);
    }

    LOGGER.endTimer("GCProcessINVCItems.appendOrderStatusChange()");
    LOGGER.verbose("GCProcessINVCItems:appendOrderStatusChange() - End");

    return yfcDocMultiAPIInput;
  }
}
