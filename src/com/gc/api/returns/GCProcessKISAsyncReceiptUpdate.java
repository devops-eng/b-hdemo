package com.gc.api.returns;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCProcessKISAsyncReceiptUpdate {
  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCCreateStoreReturnInvoice.class);

  /**
   *
   * Description of processKISAsyncReceiptUpdateFromPOS
   *
   * @param env
   * @param inDoc
   *
   */
  public void processKISAsyncReceiptUpdateFromPOS(YFSEnvironment env, Document inDoc) {

    LOGGER.beginTimer("GCProcessKISAsyncReceiptUpdate.processKISAsyncReceiptUpdateFromPOS");
    LOGGER.verbose("Class: GCProcessKISAsyncReceiptUpdate Method: processKISAsyncReceiptUpdateFromPOS START");

    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    if (LOGGER.isDebugEnabled()) {
      LOGGER.verbose("Input Doc is " + inDoc.toString());
    }

    YFCElement eleRoot = inputDoc.getDocumentElement();
    String sKISNumber = eleRoot.getAttribute("ExtnReturnRISNo");

    if (!YFCCommon.isVoid(sKISNumber)) {
      LOGGER.verbose("KIS number is :" + sKISNumber);
      String sReturnOrderNo = sKISNumber.substring(3);
      YFCDocument inDocGetOrderList = YFCDocument.getDocumentFor("<Order OrderNo='" + sReturnOrderNo + "'/>");
      YFCDocument inDocGetOrderLineListTemplate =
          YFCDocument
          .getDocumentFor("<OrderList><Order OrderHeaderKey=''><OrderLines><OrderLine OrderLineKey='' OrderedQty='' ><Extn/></OrderLine></OrderLines></Order></OrderList>");

      YFCDocument outDocGetOrderList = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, inDocGetOrderList, inDocGetOrderLineListTemplate);
      YFCElement rootEleOrderList = outDocGetOrderList.getDocumentElement();
      if (rootEleOrderList.hasChildNodes()) {
        YFCDocument inDocChangeOrderStatus = YFCDocument.createDocument(GCConstants.ORDER_STATUS_CHANGE);
        YFCElement rootEleChangeOrderStatus = inDocChangeOrderStatus.getDocumentElement();
        rootEleChangeOrderStatus.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.RETURNED_TRANSACTION);
        YFCElement eleOrderLines = inDocChangeOrderStatus.createElement(GCConstants.ORDER_LINES);
        rootEleChangeOrderStatus.appendChild(eleOrderLines);

        String sOrderHeaderKey = rootEleOrderList.getChildElement(GCConstants.ORDER).getAttribute(GCConstants.ORDER_HEADER_KEY);
        rootEleChangeOrderStatus.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);

        YFCNodeList<YFCElement> nlOrderLines = rootEleOrderList.getElementsByTagName(GCConstants.ORDER_LINE);
        for (YFCElement eleTempOrderLine : nlOrderLines) {
          YFCElement eleExtn = eleTempOrderLine.getChildElement(GCConstants.EXTN);
          String sExtnReturnRISNo = eleExtn.getAttribute(GCXmlLiterals.EXTN_RETURN_RIS_NO);
          if(YFCCommon.equals(sExtnReturnRISNo, sKISNumber)){
            String sOrderLineKey = eleTempOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
            String sQty = eleTempOrderLine.getAttribute(GCConstants.ORDERED_QTY);
            YFCElement eleOrderLine = eleOrderLines.createChild(GCConstants.ORDER_LINE);
            eleOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, sOrderLineKey);
            eleOrderLine.setAttribute(GCConstants.BASE_DROP_STATUS, GCConstants.RETURNED_STATUS);
            eleOrderLine.setAttribute(GCConstants.QUANTITY, sQty);
          }
        }

        if (LOGGER.isDebugEnabled()) {
          LOGGER.verbose("ChangeOrderStatus inputDocument is " + inDocChangeOrderStatus.toString());
        }
        if(eleOrderLines.hasChildNodes()){
          GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER_STATUS, inDocChangeOrderStatus, null);
        }
      }
    }
    LOGGER.endTimer("GCProcessKISAsyncReceiptUpdate.processKISAsyncReceiptUpdateFromPOS");
    LOGGER.verbose("Class: GCProcessKISAsyncReceiptUpdate Method: processKISAsyncReceiptUpdateFromPOS START");
  }

}
