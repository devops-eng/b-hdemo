/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 * ############################################
 * ######################################################################## OBJECTIVE: Write what
 * this class is about
 * ##############################################################################
 * ######################################## Version Date Modified By Description
 * ####################
 * ##############################################################################
 * #################### 1.0 01/04/2015 Mittal, Yashu JIRA No: Description of issue.
 * #################
 * #################################################################################
 * ####################
 */
package com.gc.api.returns;

import java.io.IOException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCWebServiceUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:yashu.mittal@expicient.com">Yashu Mittal</a>
 */
public class GCProcessPOSInvoiceFailureHold {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCProcessPOSInvoiceFailureHold.class.getName());

  /**
   *
   * Description of processPOSInvoiceFailureHold
   *
   * @param env
   * @param inDoc
   * @throws GCException
   *
   */
  public void processPOSInvoiceFailureHold(YFSEnvironment env, Document inDoc) throws GCException {


    LOGGER.beginTimer("GCProcessPOSInvoiceFailureHold.processPOSInvoiceFailureHold");
    LOGGER.verbose("Class: GCProcessPOSInvoiceFailureHold Method: processPOSInvoiceFailureHold START");

    YFCDocument yfcDoc = YFCDocument.getDocumentFor(inDoc);
    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    if (LOGGER.isDebugEnabled()) {
      LOGGER.verbose("Input doc to GCProcessPOSInvoiceFailureHold.processPOSInvoiceFailureHold method is:--> "
          + yfcInDoc.toString());
    }
    YFCElement rootEle = yfcDoc.getDocumentElement();
    String sHoldType = rootEle.getAttribute(GCConstants.HOLD_TYPE);
    String sStatus = rootEle.getAttribute(GCConstants.STATUS);

    LOGGER.verbose("HoldType is :" + sHoldType);
    LOGGER.verbose("HoldStatus is :" + sStatus);
    if (YFCCommon.equals(GCConstants.GC_POS_INVOICE_FAILURE_HOLD, sHoldType)
        && YFCCommon.equals(GCConstants.RESOLVE_HOLD_CODE, sStatus)) {

      String sOrderHeaderKey = rootEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
      YFCDocument getOrderInvoiceListIn =
          YFCDocument.getDocumentFor("<OrderInvoice OrderHeaderKey='" + sOrderHeaderKey + "' Status='"
              + GCConstants.ZERO_ONE + "'><Extn ExtnPOSRetryCount='" + GCConstants.GC_POS_CONNECT_MAX_RETRY_COUNT
              + "'/></OrderInvoice>");

      YFCDocument getOrderInvoiceListOut =
          GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_INVOICE_LIST, getOrderInvoiceListIn, null);

      YFCElement eleInvoiceList = getOrderInvoiceListOut.getDocumentElement();
      YFCNodeList<YFCElement> nlOrderInvoice = eleInvoiceList.getElementsByTagName(GCConstants.ORDER_INVOICE);
      for (YFCElement eleOrderInvoice : nlOrderInvoice) {
        String sOrderInvoiceKey = eleOrderInvoice.getAttribute(GCConstants.ORDER_INVOICE_KEY);
        YFCDocument getOrderInvoiceDetailsIn =
            YFCDocument.getDocumentFor("<GetOrderInvoiceDetails InvoiceKey='" + sOrderInvoiceKey + "'/>");
        Document getOrderInvoiceDetailTemp =
            GCCommonUtil.getDocumentFromPath("/global/template/event/SEND_INVOICE.PUBLISH_INVOICE_DETAIL.xml");
        YFCDocument getOrderInvoiceDetailsOut =
            GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_INVOICE_DETAILS, getOrderInvoiceDetailsIn,
                YFCDocument.getDocumentFor(getOrderInvoiceDetailTemp));

        YFCElement yfcEleRootInDoc = getOrderInvoiceDetailsOut.getDocumentElement();
        YFCElement yfcEleInvoiceHeader = yfcEleRootInDoc.getElementsByTagName(GCConstants.INVOICE_HEADER).item(0);

        /* Check if any line is fulfilled from MFI - Start */
        String setSplitOrderFlag = GCConstants.FALSE_STRING;
        String sSalesOrderHeaderKey = yfcEleInvoiceHeader.getAttribute(GCConstants.DERIVED_FROM_ORDER_HEADER_KEY);
        YFCDocument getOrderLineListInSO =
            YFCDocument.getDocumentFor("<OrderLine OrderHeaderKey='" + sSalesOrderHeaderKey
                + "'><Extn ExtnIsStoreFulfilled='N'/></OrderLine>");
        YFCDocument getOrderLineListOutSO =
            GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LINE_LIST, getOrderLineListInSO, null);
        if (getOrderLineListOutSO.getDocument().hasChildNodes()) {
          setSplitOrderFlag = GCConstants.TRUE_STRING;
        }
        yfcEleRootInDoc.setAttribute(GCXmlLiterals.SPLIT_ORDER_FLAG, setSplitOrderFlag);
        /* Check if any line is fulfilled from MFI - End */

        // fetch the manager id and user id and for ConfirmReturnAction and stamp on InvoiceDetail
        // element.
        GCPublishReturnInvoice.getManagerCashierApproveId(yfcEleRootInDoc, yfcEleInvoiceHeader);

        // Invoke getAndStampDates method to get order and XML creation date and time
        GCPublishReturnInvoice.getAndStampDates(yfcEleRootInDoc);

        getOrderInvoiceDetailsOut.getDocumentElement().setAttribute(GCConstants.MESSAGE_TYPE,
            GCConstants.MESSAGE_TYPE_RETURN);

        GCPublishReturnInvoice.removeBundleParentLineAndProrateComponentPrices(getOrderInvoiceDetailsOut.getDocument());

        // Fix for GCSTORE-5904, 5963 -- Starts
        GCPublishReturnInvoice ob = new GCPublishReturnInvoice();
        YFCElement eleOrder = getOrderInvoiceDetailsOut.getElementsByTagName(GCConstants.ORDER).item(0);
        YFCElement eleOrderExtn = eleOrder.getElementsByTagName(GCConstants.EXTN).item(0);
        if (!YFCCommon.equals(eleOrderExtn.getAttribute(GCConstants.EXTN_IS_REFUND_ON_ACCOUNT), GCConstants.YES)) {
          ob.handleExchangeReturn(getOrderInvoiceDetailsOut);
        } else {
          ob.handleExchangeReturnStoreCredit(getOrderInvoiceDetailsOut);
        }
        // Fix for GCSTORE-5904, 5963 -- Ends

        YFCDocument yfcDocInvokeWebServiceInput =
            GCCommonUtil.invokeService(env, GCXmlLiterals.GC_PREPARE_POS_WEB_SERVICE_INPUT, getOrderInvoiceDetailsOut);

        // Decrypt CreditCard No from OMS private key and encrypt using POS public key.

        GCPublishReturnInvoice.encryptDecryptCCForPOS(yfcDocInvokeWebServiceInput);

        if (LOGGER.isDebugEnabled()) {
          LOGGER.verbose("Input doc passed to POSReturn11Ticket web service is:--> "
              + yfcDocInvokeWebServiceInput.toString());
        }
        Document docWebServiceOP = null;
        GCWebServiceUtil obj = new GCWebServiceUtil();
        YFCException yfcException = new YFCException();
        try {
          docWebServiceOP =
              obj.invokeSOAPWebService(yfcDocInvokeWebServiceInput.getDocument(),
                  YFSSystem.getProperty(GCConstants.GET_RETURN_11TICKET_WEBSERVICE_URL),
                  YFSSystem.getProperty(GCConstants.GET_RETURN_11TICKET_WEBSERVICE_ACTION));
          if (LOGGER.isDebugEnabled()) {
            LOGGER.verbose("Output received from POSReturn11Ticket web service is:--> "
                + YFCDocument.getDocumentFor(docWebServiceOP).toString());
          }
          NodeList nlReturnResultDetail = docWebServiceOP.getDocumentElement().getElementsByTagName("ReturnResult");
          if (nlReturnResultDetail.getLength() > 0) {
            Element eleReturnResultDetail = (Element) nlReturnResultDetail.item(0);
            String sReturnDetail = eleReturnResultDetail.getTextContent();
            if (!YFCCommon.isVoid(sReturnDetail)) {
              YFCDocument yfcDocResponse = YFCDocument.getDocumentFor(sReturnDetail);
              YFCElement yfcEleResponse = yfcDocResponse.getDocumentElement();
              YFCElement eleSuccess = yfcEleResponse.getElementsByTagName(GCConstants.SUCCESS).item(0);
              String sSuccess = eleSuccess.getNodeValue();
              if (YFCCommon.equals(sSuccess, GCConstants.TRUE)) {
                YFCNodeList<YFCElement> nlResponse =
                    yfcEleResponse.getElementsByTagName(GCXmlLiterals.POS_SALES_TICKET);
                YFCElement elePOSSalesTicket = nlResponse.item(0);
                if (!YFCCommon.isVoid(elePOSSalesTicket)) {
                  String sExtnPOSReturn11TicketNo = elePOSSalesTicket.getNodeValue();
                  LOGGER.verbose("POSReturn11Ticket" + sExtnPOSReturn11TicketNo);
                  invoke70TicketPOSWebServ(env, getOrderInvoiceDetailsOut, yfcException, sExtnPOSReturn11TicketNo);
                } else {
                  LOGGER.verbose("Element POSSalesTicket is not present in web-service call.");
                  yfcException.setAttribute(GCConstants.ERROR_CODE,
                      "Return11 Ticket is not present in response received from POS");
                  yfcException.setAttribute(GCConstants.ERROR_DESCRIPTION, GCConstants.POS_CONNECT_FAILURE_ERROR_DESC);
                  throw yfcException;
                }
              } else {
                YFCNodeList<YFCElement> nlErrorInfo = yfcEleResponse.getElementsByTagName("Errorinfo");
                YFCElement eleErrorInfo = nlErrorInfo.item(0);
                if (!YFCCommon.isVoid(eleErrorInfo)) {
                  String sErrorInfo = eleErrorInfo.getNodeValue();
                  yfcException.setAttribute(GCConstants.ERROR_CODE, "WebService call is failed.");
                  yfcException.setAttribute(GCConstants.ERROR_DESCRIPTION, sErrorInfo);
                  throw yfcException;
                } else {
                  yfcException.setAttribute(GCConstants.ERROR_CODE, "No response received from Webservice call");
                  yfcException.setAttribute(GCConstants.ERROR_DESCRIPTION, GCConstants.POS_CONNECT_FAILURE_ERROR_DESC);
                  throw yfcException;
                }
              }
            } else {
              LOGGER.verbose("No reponse is received from POS. Throw exception to fail hold resolve");
              yfcException.setAttribute(GCConstants.ERROR_CODE, "Invalid response received from POS.");
              yfcException.setAttribute(GCConstants.ERROR_DESCRIPTION, GCConstants.POS_CONNECT_FAILURE_ERROR_DESC);
              throw yfcException;
            }

          } else {
            LOGGER.verbose("No reponse is received from POS. Throw exception to fail hold resolve");
            yfcException.setAttribute(GCConstants.ERROR_CODE, "Invalid response received from POS.");
            yfcException.setAttribute(GCConstants.ERROR_DESCRIPTION, GCConstants.POS_CONNECT_FAILURE_ERROR_DESC);
            throw yfcException;
          }
        } catch (IOException e) {
          LOGGER.verbose("Could not connect to POS.  Throw exception to fail hold resolve");
          yfcException.setAttribute(GCConstants.ERROR_CODE, "Could not connect to POS");
          yfcException.setAttribute(GCConstants.ERROR_DESCRIPTION, GCConstants.POS_CONNECT_FAILURE_ERROR_DESC);
          LOGGER.error("IOException received. Could not connect to POS", e);
          throw yfcException;
        }
      }
    }

    LOGGER.endTimer("GCProcessPOSInvoiceFailureHold.processPOSInvoiceFailureHold");
    LOGGER.verbose("Class: GCProcessPOSInvoiceFailureHold Method: processPOSInvoiceFailureHold END");
  }

  private void invoke70TicketPOSWebServ(YFSEnvironment env, YFCDocument getOrderInvoiceDetailsOut,
      YFCException yfcException, String sExtnPOSReturn11TicketNo) {
    if (!YFCCommon.isVoid(sExtnPOSReturn11TicketNo)) {
      LOGGER
      .verbose("Calling method append70TicketOrReturn11TicketAtOrderLine to stamp sExtnPOSReturn11TicketNo on orderlines");
      GCInvoke70TicketPOSWebService.append70TicketOrReturn11TicketAtOrderLine(env, null, sExtnPOSReturn11TicketNo,
          getOrderInvoiceDetailsOut);
    } else {
      LOGGER.verbose("Response is received from POS, but POSReturn11TicketNo is not present in the output.");
      yfcException
      .setAttribute(GCConstants.ERROR_CODE, "Return11 Ticket is not present in response received from POS.");
      yfcException.setAttribute(GCConstants.ERROR_DESCRIPTION, GCConstants.POS_CONNECT_FAILURE_ERROR_DESC);
      throw yfcException;
    }
  }

}
