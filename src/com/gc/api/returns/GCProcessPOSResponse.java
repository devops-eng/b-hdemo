   /** 
* Copyright � 2014, Guitar Center,  All rights reserved.
* ####################################################################################################################
*	OBJECTIVE: This class will process response received from POS.
*######################################################################################################################
*	Version     Date       Modified By              Description
*######################################################################################################################
    1.0      25/03/2015   Mittal, Yashu		GCSTORE-556, GCSTORE-754
*######################################################################################################################
 */
package com.gc.api.returns;

import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:yashu.mittal@expicient.com">Yashu Mittal</a>
 */
public class GCProcessPOSResponse implements YIFCustomApi{
  
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCPublishReturnInvoice.class.getName());
  private Properties properties;
  
  /**
   * 
   * This method will check the how many times, Send invoice web service call to POS is failed.
   *
   * @param env
   * @param inDoc 
 * @throws InterruptedException 
   *
   */
  public void processPOSResponse(YFSEnvironment env, Document inDoc) throws InterruptedException{
    
	//for mPOS orders calling GCFinalizeCoupons service: Start:
	    String sFinalizeCouponsCall = inDoc.getDocumentElement().getAttribute("FinalizeCouponsCall");
	    LOGGER.verbose("FinalizeCouponsCall--> " + sFinalizeCouponsCall);
	    if(YFCCommon.equalsIgnoreCase(sFinalizeCouponsCall, GCConstants.YES))
	    {
	    	LOGGER.verbose("Calling GCFinalizeCoupons service for mPOS orders -->");
	    	LOGGER.verbose("InDoc for GCFinalizeCoupons is-->" + GCXMLUtil.getXMLString(inDoc));
	    	GCCommonUtil.invokeService(env, "GCFinalizeCoupons", inDoc);
	    }
	    else{
	  Set setList = properties.keySet();
	  Iterator<String> listIterator = setList.iterator(); 
	  String strParamName = listIterator.next();
	  String strParamValue = "1"; // Setting default Sleep time if not found in arguments.
	  if(YFCCommon.equals("SleepTime",strParamName)){
		  strParamValue = properties.getProperty(strParamName);
      }
	  
	  long bufferTmInsec = Integer.parseInt(strParamValue) * 1000L;
	  Thread.sleep(bufferTmInsec);  

    LOGGER.beginTimer("GCProcessPOSResponse.processPOSResponse");
    LOGGER.verbose("Class: GCProcessPOSResponse Method: processPOSResponse START");
    
    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    if(LOGGER.isDebugEnabled()){
      LOGGER.verbose("Input doc to GCProcessPOSResponse.processPOSResponse method is:--> " + yfcInDoc.toString());
    }
    YFCElement eleRoot  = yfcInDoc.getDocumentElement();
    String sFailedResponse = eleRoot.getAttribute("FailedResponse");
    String sStampTickets = eleRoot.getAttribute("StampTickets");
      
	    if(YFCCommon.equals(sStampTickets, GCConstants.TRUE_STRING)){
	    	eleRoot.removeAttribute("StampTickets");
	    	GCCommonUtil.invokeAPI(env, "changeOrderInvoice", inDoc, null);
	    	
	    } else {
	        YFCElement eleInvoiceHeader = eleRoot.getChildElement(GCConstants.INVOICE_HEADER);   
	        YFCElement eleOrder = eleInvoiceHeader.getChildElement(GCConstants.ORDER);
	        String sOrderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
	        String sOrderInvoiceKey = eleInvoiceHeader.getAttribute(GCConstants.ORDER_INVOICE_KEY);
	        YFCElement eleInvoiceHeaderExtn = eleInvoiceHeader.getChildElement(GCConstants.EXTN);
	        String sExtnPOSRetryCount = eleInvoiceHeaderExtn.getAttribute(GCXmlLiterals.EXTN_POS_RETRY_COUNT);
	        LOGGER.verbose("POSRetryCount is : " + sExtnPOSRetryCount);
	        if(YFCCommon.equals(sFailedResponse, GCConstants.TRUE_STRING)){
	        	eleRoot.removeAttribute("FailedResponse");
	        	changeOrderInvoice(env, sOrderInvoiceKey, GCConstants.GC_POS_CONNECT_MAX_RETRY_COUNT);
	        	GCCommonUtil.invokeService(env, GCConstants.GC_POS_CONNECT_FAILED_ALERT_SERVICE, inDoc);
	        	applyPOSInvoiceFailureHold(env, sOrderHeaderKey);
	        } else{
	        	if (YFCCommon.isVoid(sExtnPOSRetryCount)) {
	        		changeOrderInvoice(env, sOrderInvoiceKey, GCConstants.INT_ONE);
	        	} else if (Integer.parseInt(GCConstants.GC_POS_CONNECT_MAX_RETRY_COUNT) -1  > Integer.parseInt(sExtnPOSRetryCount)) {
	        		int iRetry = Integer.parseInt(sExtnPOSRetryCount) + 1;
	        		changeOrderInvoice(env, sOrderInvoiceKey, Integer.toString(iRetry));
	        	}else{
	        		LOGGER.verbose("Raise POSConnectFailAlert if POSRetry count is reached maximum limit");
	        		changeOrderInvoice(env, sOrderInvoiceKey, GCConstants.GC_POS_CONNECT_MAX_RETRY_COUNT);
	        		GCCommonUtil.invokeService(env, GCConstants.GC_POS_CONNECT_FAILED_ALERT_SERVICE, inDoc);
	        		LOGGER.verbose("Put order on hold if POSRetry count is reached maximum limit");
	        		applyPOSInvoiceFailureHold(env, sOrderHeaderKey);
	        	}
	        }
	    }
	 }
    LOGGER.endTimer("GCProcessPOSResponse.processPOSResponse");
    LOGGER.verbose("Class: GCProcessPOSResponse Method: processPOSResponse END");
  }
  
  /**
   * 
   * This method will apply hold on order if web service call to POS for sending invoice is failed to a maximum retry limit.
   *
   * @param env
   * @param sOrderHeaderKey 
   *
   */
  public static void applyPOSInvoiceFailureHold(YFSEnvironment env, String sOrderHeaderKey){
    
    YFCDocument docChangeOrder = YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "' Override='Y' SelectMethod='WAIT'/>");
    YFCElement changeOrderRootEle = docChangeOrder.getDocumentElement();
    YFCElement eleOrderHoldTypes = changeOrderRootEle.createChild(GCConstants.ORDER_HOLD_TYPES);
    YFCElement eleOrderHoldType = eleOrderHoldTypes.createChild(GCConstants.ORDER_HOLD_TYPE);
    eleOrderHoldType.setAttribute(GCConstants.HOLD_TYPE, GCConstants.GC_POS_INVOICE_FAILURE_HOLD);
    eleOrderHoldType.setAttribute(GCConstants.STATUS, GCConstants.APPLY_HOLD_CODE);
    if(LOGGER.isDebugEnabled()){
        LOGGER.verbose("Input doc to to changeOrder to apply POSInvoiceFailureHold:--> " + docChangeOrder.toString());
      }
    GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, docChangeOrder, null); 
    
  }
  
  /**
   * 
   * This method will stamp ExtnPOSRetryCount on invoice.
   *
   * @param env
   * @param sOrderInvoiceKey
   * @param sRetryCount 
   *
   */
  public static void changeOrderInvoice(YFSEnvironment env, String sOrderInvoiceKey, String sRetryCount ){
    
    YFCDocument docChangeOrderInvoice = YFCDocument.getDocumentFor("<OrderInvoice OrderInvoiceKey='" + sOrderInvoiceKey + "'/>");
    YFCElement changeOrderInvoiceRootEle = docChangeOrderInvoice.getDocumentElement();
    YFCElement eleChangeOrderInvoiceExtn = changeOrderInvoiceRootEle.createChild(GCConstants.EXTN);
    eleChangeOrderInvoiceExtn.setAttribute("ExtnPOSRetryCount", sRetryCount);
    if(LOGGER.isDebugEnabled()){
      LOGGER.verbose("Input document for changeOrderInvoice to stamp POSRetryCount:-->" + docChangeOrderInvoice.toString());
    }
    GCCommonUtil.invokeAPI(env, "changeOrderInvoice", docChangeOrderInvoice, null);
  }

@Override
public void setProperties(Properties paramProperties) throws GCException {
	// TODO Auto-generated method stub
	properties = paramProperties;
}
  
}
