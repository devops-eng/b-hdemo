/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
 *			 1.0			   15/04/2015		Goel, Varun			 GCSTORE - 497 - Ship Back to DC
 *#################################################################################################################################################################
             2.0               29/04/2017         Expicient 	GCSTORE-6816: Modified the getShipmentList Input and Output Template and the Pipeline to fix the issue.
             2.1			   02/05/2017		  Expicient 	GCSTORE-6722: Ship back to KCDC queue displaying no results
 *#################################################################################################################################################################
 */

package com.gc.api.returns;

import java.awt.Container;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
//This Class Implements the Functionality of Ship Back To DC Scenarios

public class GCProcessShipBackToDC {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCProcessShipBackToDC.class);

  /**
   *
   * This method create and process the Transfer Order.
   *
   * @param env
   * @param inputXML
   *
   *
   */
  public void createProcessTransferOrder(final YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("GCProcessShipBackToDC.createProcessTransferOrder");
    LOGGER.verbose("GCProcessShipBackToDC.createProcessTransferOrder--Begin");
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("inpuDoc : " + inputDoc.toString());
    }

    final YFCElement eleOrderStatusChange = inputDoc.getDocumentElement();
    String sOrderHeaderKey = eleOrderStatusChange.getAttribute(GCConstants.ORDER_HEADER_KEY);
    YFCElement eleOrderLines = eleOrderStatusChange.getChildElement(GCConstants.ORDER_LINES);
    YFCElement eleOrderAudit = eleOrderStatusChange.getChildElement(GCConstants.ORDER_AUDIT);
    YFCElement eleOrder = eleOrderAudit.getChildElement(GCConstants.ORDER);
    String sEnterpriseCode = eleOrder.getAttribute(GCConstants.ENTERPRISE_CODE);
    YFCDocument createOrderDoc = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleRoot = createOrderDoc.getDocumentElement();
    YFCElement elePersonInfoBillto = eleRoot.createChild(GCConstants.PERSON_INFO_BILL_TO);
    YFCElement eleTROrderlns = eleRoot.createChild(GCConstants.ORDER_LINES);
    eleRoot.setAttribute(GCConstants.ENTERPRISE_CODE, sEnterpriseCode);
    eleRoot.setAttribute(GCConstants.DOCUMENT_TYPE, GCConstants.TRANSFER_ORDER_DOCUMENT_TYPE);
    String sExtnReturnVPONo = null;
    String sReceivingNd = null;

    final YFCNodeList<YFCElement> nlOrderLine = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);

    YFCDocument getShipNodeListIp = YFCDocument.createDocument(GCConstants.SHIP_NODE);
    YFCElement eleShipnode = getShipNodeListIp.getDocumentElement();
    eleShipnode.setAttribute(GCConstants.SHIP_NODE, GCConstants.MFI);
    YFCDocument getShipnodelistOpTmp = YFCDocument.getDocumentFor(GCConstants.GET_SHIP_NODE_LIST_TMP);
    YFCDocument getShipnodelistOpDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIP_NODE_LIST, getShipNodeListIp, getShipnodelistOpTmp);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getShipnodelistOpDoc : " + getShipnodelistOpDoc.toString());
    }


    YFCElement eleShipnodelist = getShipnodelistOpDoc.getDocumentElement();
    YFCElement eleShipNode = eleShipnodelist.getChildElement(GCConstants.SHIP_NODE);
    YFCElement eleShipNodePersonInfo = eleShipNode.getChildElement(GCXmlLiterals.SHIP_NODE_PERSON_INFO);
    GCXMLUtil.copyElement(createOrderDoc, eleShipNodePersonInfo, elePersonInfoBillto);

    YFCDocument getOrderListIp = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement elegetOrderlist = getOrderListIp.getDocumentElement();
    elegetOrderlist.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    YFCDocument getOrderlistOutTmp = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_TMP);

    YFCDocument getOrderlistOutput =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListIp, getOrderlistOutTmp);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getOrderlistOutput : " + getOrderlistOutput.toString());
    }

    YFCElement eleOrdroot = getOrderlistOutput.getDocumentElement();
    YFCElement eleOrderelement = eleOrdroot.getChildElement(GCConstants.ORDER);
    YFCElement eleOrderLinesorderdetailop = eleOrderelement.getChildElement(GCConstants.ORDER_LINES);
    final YFCNodeList<YFCElement> nlOrderLinegetOrderList =
        eleOrderLinesorderdetailop.getElementsByTagName(GCConstants.ORDER_LINE);
    eleRoot.setAttribute(GCConstants.SELLER_ORGANIZATION_CODE, sEnterpriseCode);


    YFCDocument consolidateShipmentIpDoc = YFCDocument.createDocument(GCConstants.ORDER_RELEASE);
    YFCElement eleReleaseOrderforConsolidation = consolidateShipmentIpDoc.getDocumentElement();
    YFCElement eleOrderLinesforConsolidation = eleReleaseOrderforConsolidation.createChild(GCConstants.ORDER_LINES);

    for (YFCElement eleOrderLine : nlOrderLine) {

      String sPrimeLineNo = eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
      String sSublineNo = eleOrderLine.getAttribute(GCConstants.SUB_LINE_NO);
      String sOrderedQty = eleOrderLine.getAttribute(GCConstants.QUANTITY);
      YFCElement eleOrderLineforConsolidation = eleOrderLinesforConsolidation.createChild(GCConstants.ORDER_LINE);

      for (YFCElement eleOrderLineorderdetailop : nlOrderLinegetOrderList) {
        String sPrmlinNo = eleOrderLineorderdetailop.getAttribute(GCConstants.PRIME_LINE_NO);
        String sKitCode = eleOrderLineorderdetailop.getAttribute(GCConstants.KIT_CODE);
        YFCElement eleItemgetOrderList = eleOrderLineorderdetailop.getChildElement(GCConstants.ITEM);
        String sProdcutclass = eleItemgetOrderList.getAttribute(GCConstants.PRODUCT_CLASS);
        YFCElement eleBundleParentLine = eleOrderLineorderdetailop.getChildElement(GCConstants.BUNDLE_PARENT_LINE);

        boolean isLineAllowedToAddToTransferOrder = isLineAlloedToAddToTransferOrder(sPrimeLineNo, sPrmlinNo, sKitCode, sProdcutclass,  eleBundleParentLine);

        if (isLineAllowedToAddToTransferOrder) {
          GCXMLUtil.copyElement(consolidateShipmentIpDoc, eleOrderLineorderdetailop, eleOrderLineforConsolidation);
          sReceivingNd = eleOrderLineorderdetailop.getAttribute(GCConstants.RECEIVING_NODE);
          eleOrderLineforConsolidation.removeAttribute(GCConstants.RECEIVING_NODE);
          YFCElement eleExtn = eleOrderLineorderdetailop.getChildElement(GCConstants.EXTN);
          sExtnReturnVPONo = eleExtn.getAttribute(GCConstants.EXTN_RETURN_VPO_NO);
          YFCElement eleTROrderLine = eleTROrderlns.createChild(GCConstants.ORDER_LINE);
          eleTROrderLine.setAttribute(GCConstants.PRIME_LINE_NO, sPrimeLineNo);
          eleTROrderLine.setAttribute(GCConstants.SUB_LINE_NO, sSublineNo);
          eleTROrderLine.setAttribute(GCConstants.ORDERED_QTY, sOrderedQty);
          eleTROrderLine.setAttribute(GCConstants.SHIP_NODE, sReceivingNd);
          eleTROrderLine.setAttribute(GCConstants.RECEIVING_NODE, GCConstants.MFI);

          YFCElement eleExtnTr = eleTROrderLine.createChild(GCConstants.EXTN);
          eleExtnTr.setAttribute(GCConstants.EXTN_RETURN_VPO_NO, sExtnReturnVPONo);

          YFCElement eleLinePriceInfo = eleTROrderLine.createChild(GCConstants.LINE_PRICE_INFO);
          eleLinePriceInfo.setAttribute(GCConstants.IS_PRICE_LOCKED, GCConstants.FLAG_Y);

          YFCElement eleDerivedFromTr = eleTROrderLine.createChild(GCConstants.DERIVED_FROM);
          eleDerivedFromTr.setAttribute(GCConstants.SUB_LINE_NO, sSublineNo);
          eleDerivedFromTr.setAttribute(GCConstants.PRIME_LINE_NO, sPrimeLineNo);
          eleDerivedFromTr.setAttribute(GCConstants.DOCUMENT_TYPE, GCConstants.RETURN_ORDER_DOCUMENT_TYPE);
          eleDerivedFromTr.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
        } else{
          GCXMLUtil.copyElement(consolidateShipmentIpDoc, eleOrderLineorderdetailop, eleOrderLineforConsolidation);
        }
      }


    }

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("createOrderDoc : " + createOrderDoc.toString());
    }

    Document createOrderApiOpDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.API_CREATE_ORDER, createOrderDoc.getDocument());
    YFCDocument createorderopDoc = YFCDocument.getDocumentFor(createOrderApiOpDoc);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("createorderopDoc : " + createorderopDoc.toString());
    }

    YFCElement eleOrderRoot = createorderopDoc.getDocumentElement();
    String sOrderNumber = eleOrderRoot.getAttribute(GCConstants.ORDER_NO);
    String sDocumentType = eleOrderRoot.getAttribute(GCConstants.DOCUMENT_TYPE);
    String sEnterprise = eleOrderRoot.getAttribute(GCConstants.ENTERPRISE_CODE);

    GCCommonUtil.invokeAPI(env, GCConstants.API_SCHEDULE_ORDER, createorderopDoc.getDocument());
    eleOrderRoot.setAttribute(GCConstants.IGNORE_RELEASE_DATE, GCConstants.FLAG_Y);
    GCCommonUtil.invokeAPI(env, GCConstants.API_RELEASE_ORDER, createorderopDoc.getDocument());

    eleReleaseOrderforConsolidation.setAttribute(GCConstants.ORDER_NO, sOrderNumber);
    eleReleaseOrderforConsolidation.setAttribute(GCConstants.DOCUMENT_TYPE, sDocumentType);
    eleReleaseOrderforConsolidation.setAttribute(GCConstants.ENTERPRISE_CODE, sEnterprise);
    
    //GCSTORE-6722 Begin   
    String sReleaseNo = getReleaseNo(env,eleOrderRoot);
    eleReleaseOrderforConsolidation.setAttribute(GCConstants.RELEASE_NO, sReleaseNo);
    //GCSTORE-6722 End
    
    
    YFCElement eleToAddress = eleReleaseOrderforConsolidation.createChild(GCConstants.TO_ADDRESS);
    GCXMLUtil.copyElement(consolidateShipmentIpDoc, eleShipNodePersonInfo, eleToAddress);

    YFCDocument getShipNodeListInpforfromaddress = YFCDocument.createDocument(GCConstants.SHIP_NODE);
    YFCElement eleShipnodefrom = getShipNodeListInpforfromaddress.getDocumentElement();
    eleShipnodefrom.setAttribute(GCConstants.SHIP_NODE, sReceivingNd);
    YFCDocument getShipnodelistOpDocfrom =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIP_NODE_LIST, getShipNodeListInpforfromaddress,
            getShipnodelistOpTmp);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getShipnodelistOpDocfrom : " + getShipnodelistOpDocfrom.toString());
    }


    YFCElement eleShipnodelistfrom = getShipnodelistOpDocfrom.getDocumentElement();
    YFCElement eleShipNodefrom = eleShipnodelistfrom.getChildElement(GCConstants.SHIP_NODE);
    YFCElement eleShipNodePersonInfofrom = eleShipNodefrom.getChildElement(GCXmlLiterals.SHIP_NODE_PERSON_INFO);
    YFCElement eleFromAddress = eleReleaseOrderforConsolidation.createChild(GCXmlLiterals.FROM_ADDRESS);
    GCXMLUtil.copyElement(consolidateShipmentIpDoc, eleShipNodePersonInfofrom, eleFromAddress);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("consolidateShipmentIpDoc : " + consolidateShipmentIpDoc.toString());
    }

    GCCommonUtil.invokeAPI(env, GCConstants.CONSOLIDATE_TO_SHIPMENT_API, consolidateShipmentIpDoc.getDocument());

    LOGGER.verbose("GCProcessShipBackToDC.createProcessTransferOrder--End");
    LOGGER.endTimer("GCProcessShipBackToDC.createProcessTransferOrder");
  }


/**
 * For GCSTORE-6722. To fetch ReleaseNo  
 * @param env
 * @param eleOrderRoot
 * @return
 */
  private String getReleaseNo(YFSEnvironment env, YFCElement eleOrderRoot) {

	  LOGGER.beginTimer("getReleaseNo");
	  YFCDocument getOrderReleaselistOutTmp = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_RELEASE_TMPLATE);
	  YFCDocument getOrderReleaseListInp = YFCDocument.createDocument(GCConstants.ORDER_RELEASE);
	  YFCElement eleOrderrelease = getOrderReleaseListInp.getDocumentElement();
	  eleOrderrelease.setAttribute(GCConstants.ENTERPRISE_CODE, eleOrderRoot.getAttribute(GCConstants.ENTERPRISE_CODE));
	  eleOrderrelease.setAttribute(GCConstants.DOCUMENT_TYPE, eleOrderRoot.getAttribute(GCConstants.DOCUMENT_TYPE));
	  String sOrderHeaderKey = eleOrderRoot.getAttribute(GCConstants.ORDER_HEADER_KEY);
	  String sRelsno = "1";
	  if(!YFCCommon.isVoid(sOrderHeaderKey)){
		  eleOrderrelease.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);  
		  LOGGER.debug("getOrderReleaseList Input XML: \n"+getOrderReleaseListInp);
		  
		  YFCDocument getOrderReleaselistOutput =
				  GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_RELEASE_LIST, getOrderReleaseListInp,
						  getOrderReleaselistOutTmp);
		  
		  LOGGER.debug("getOrderReleaseList Output XML: \n"+getOrderReleaselistOutput);

		  YFCElement eleOrderReleaseList = getOrderReleaselistOutput.getDocumentElement();
		  String sLastOrderReleaseKey = eleOrderReleaseList.getAttribute(GCConstants.LAST_ORDER_RELEASE_KEY);
		  final YFCNodeList<YFCElement> nlgetOrderReleaseList =
				  eleOrderReleaseList.getElementsByTagName(GCConstants.ORDER_RELEASE);
		  for (YFCElement eleOrderRelease : nlgetOrderReleaseList) {
			  String sOrderReleaseKey = eleOrderRelease.getAttribute(GCConstants.ORDER_RELEASE_KEY);
			  if (YFCUtils.equals(sLastOrderReleaseKey, sOrderReleaseKey)) {
				  sRelsno = eleOrderRelease.getAttribute(GCConstants.RELEASE_NO);
			  }
		  }
	  }
	  LOGGER.debug("Release No: "+ sRelsno);
	  LOGGER.endTimer("getReleaseNo");
	  return sRelsno;
}



/**
   *
   * This method reschedule and release the Backordered Transfer Order Quantities.
   *
   * @param env
   * @param inputXML
   *
   *
   */
  public void processTransferOrderOnBackorder(YFSEnvironment env, Document inputXML) {

    LOGGER.beginTimer("GCProcessShipBackToDC.processTransferOrderOnBackorder");
    LOGGER.verbose("GCProcessShipBackToDC.processTransferOrderOnBackorder--Begin");
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inputXML);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("inputDoc : " + inputDoc.toString());
    }
    String sShipNd = null;
    YFCElement eleOrderlist = inputDoc.getDocumentElement();
    final YFCNodeList<YFCElement> nlOrder = eleOrderlist.getElementsByTagName(GCConstants.ORDER);
    for (YFCElement eleOrder : nlOrder) {
      String sOrderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);

      YFCDocument getOrderListIp = YFCDocument.createDocument(GCConstants.ORDER);
      YFCElement elegetOrderlist = getOrderListIp.getDocumentElement();
      elegetOrderlist.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
      YFCDocument getOrderlistOutTmp = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_TMPLATE);

      YFCDocument getOrderlistOutput =
          GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListIp, getOrderlistOutTmp);

      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("getOrderlistOutput : " + getOrderlistOutput.toString());
      }

      YFCElement eleOrdroot = getOrderlistOutput.getDocumentElement();
      YFCElement eleOrderelement = eleOrdroot.getChildElement(GCConstants.ORDER);
      String sOrderNo = eleOrderelement.getAttribute(GCConstants.ORDER_NO);
      String sDocumentType = eleOrderelement.getAttribute(GCConstants.DOCUMENT_TYPE);
      String sEnterprise = eleOrderelement.getAttribute(GCConstants.ENTERPRISE_CODE);
      YFCElement eleOrderLinesorderdetailop = eleOrderelement.getChildElement(GCConstants.ORDER_LINES);
      final YFCNodeList<YFCElement> nlOrderLinegetOrderList =
          eleOrderLinesorderdetailop.getElementsByTagName(GCConstants.ORDER_LINE);
      YFCDocument consolidateShipmentIpDoc = YFCDocument.createDocument(GCConstants.ORDER_RELEASE);
      YFCElement eleReleaseOrderforConsolidation = consolidateShipmentIpDoc.getDocumentElement();
      YFCElement eleToAddress = eleReleaseOrderforConsolidation.createChild(GCConstants.TO_ADDRESS);
      YFCElement eleFromAddress = eleReleaseOrderforConsolidation.createChild(GCXmlLiterals.FROM_ADDRESS);
      YFCElement eleOrderLinesforConsolidation = eleReleaseOrderforConsolidation.createChild(GCConstants.ORDER_LINES);
      for (YFCElement eleOrderLineorderdetailop : nlOrderLinegetOrderList) {

        YFCElement eleOrderStatuses = eleOrderLineorderdetailop.getChildElement(GCConstants.ORDER_STATUSES);
        final YFCNodeList<YFCElement> nlOrderStatus = eleOrderStatuses.getElementsByTagName(GCConstants.ORDER_STATUS);
        for (YFCElement eleOrderStatus : nlOrderStatus) {
          String sStatus = eleOrderStatus.getAttribute(GCConstants.STATUS);
          if (YFCUtils.equals(GCConstants.STATUS_BACKORDERED, sStatus)) {
            YFCElement eleOrderLineforConsolidation = eleOrderLinesforConsolidation.createChild(GCConstants.ORDER_LINE);
            String sQuantity = eleOrderStatus.getAttribute(GCConstants.STATUS_QTY);
            String sQuantityOrder = eleOrderLineorderdetailop.getAttribute(GCConstants.ORDERED_QTY);
            sQuantityOrder = sQuantity;
            eleOrderLineorderdetailop.setAttribute(GCConstants.ORDERED_QTY, sQuantityOrder);
            sShipNd = eleOrderLineorderdetailop.getAttribute(GCConstants.SHIP_NODE);
            eleOrderLineorderdetailop.removeAttribute(GCConstants.SHIP_NODE);
            GCXMLUtil.copyElement(consolidateShipmentIpDoc, eleOrderLineorderdetailop, eleOrderLineforConsolidation);
            YFCElement eleOrderStatusesforConsolidation =
                eleOrderLineforConsolidation.getChildElement(GCConstants.ORDER_STATUSES);
            final YFCNodeList<YFCElement> nlOrderStatusforConsolidation =
                eleOrderStatusesforConsolidation.getElementsByTagName(GCConstants.ORDER_STATUS);
            for (YFCElement eleOrderStatusforConsolidation : nlOrderStatusforConsolidation) {
              eleOrderStatusforConsolidation.removeAttribute(GCConstants.STATUS);
              eleOrderStatusforConsolidation.removeAttribute(GCConstants.STATUS_QTY);
              eleOrderStatusesforConsolidation.removeChild(eleOrderStatusforConsolidation);
            }

            eleOrderLineforConsolidation.removeChild(eleOrderStatusesforConsolidation);
            break;
          }
        }
      }

      YFCDocument scheduleOrderIp = YFCDocument.createDocument(GCConstants.SCHEDULE_ORDER);
      YFCElement eleScheduleOrder = scheduleOrderIp.getDocumentElement();
      eleScheduleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
      eleScheduleOrder.setAttribute(GCConstants.ORDER_NO, sOrderNo);
      eleScheduleOrder.setAttribute(GCConstants.DOCUMENT_TYPE, sDocumentType);

      GCCommonUtil.invokeAPI(env, GCConstants.API_SCHEDULE_ORDER, scheduleOrderIp.getDocument());

      YFCDocument releaseOrderInput = YFCDocument.createDocument(GCConstants.RELEASE_ORDER);
      YFCElement eleReleaseOrder = releaseOrderInput.getDocumentElement();
      eleReleaseOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
      eleReleaseOrder.setAttribute(GCConstants.ORDER_NO, sOrderNo);
      eleReleaseOrder.setAttribute(GCConstants.IGNORE_RELEASE_DATE, GCConstants.FLAG_Y);
      eleReleaseOrder.setAttribute(GCConstants.DOCUMENT_TYPE, sDocumentType);
      GCCommonUtil.invokeAPI(env, GCConstants.API_RELEASE_ORDER, releaseOrderInput.getDocument());

      YFCDocument getShipNodeListIp = YFCDocument.createDocument(GCConstants.SHIP_NODE);
      YFCElement eleShipnode = getShipNodeListIp.getDocumentElement();
      eleShipnode.setAttribute(GCConstants.SHIP_NODE, GCConstants.MFI);
      YFCDocument getShipnodelistOpTmp = YFCDocument.getDocumentFor(GCConstants.GET_SHIP_NODE_LIST_TMP);
      YFCDocument getShipnodelistOpDoc =
          GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIP_NODE_LIST, getShipNodeListIp, getShipnodelistOpTmp);

      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("getShipnodelistOpDoc : " + getShipnodelistOpDoc.toString());
      }


      YFCElement eleShipnodelist = getShipnodelistOpDoc.getDocumentElement();
      YFCElement eleShipNode = eleShipnodelist.getChildElement(GCConstants.SHIP_NODE);
      YFCElement eleShipNodePersonInfo = eleShipNode.getChildElement(GCXmlLiterals.SHIP_NODE_PERSON_INFO);


      YFCDocument getShipNodeListInpforfromaddress = YFCDocument.createDocument(GCConstants.SHIP_NODE);
      YFCElement eleShipnodefrom = getShipNodeListInpforfromaddress.getDocumentElement();
      eleShipnodefrom.setAttribute(GCConstants.SHIP_NODE, sShipNd);
      YFCDocument getShipnodelistOpDocfrom =
          GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIP_NODE_LIST, getShipNodeListInpforfromaddress,
              getShipnodelistOpTmp);

      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("getShipnodelistOpDocfrom : " + getShipnodelistOpDocfrom.toString());
      }


      YFCElement eleShipnodelistfrom = getShipnodelistOpDocfrom.getDocumentElement();
      YFCElement eleShipNodefrom = eleShipnodelistfrom.getChildElement(GCConstants.SHIP_NODE);
      YFCElement eleShipNodePersonInfofrom = eleShipNodefrom.getChildElement(GCXmlLiterals.SHIP_NODE_PERSON_INFO);

      GCXMLUtil.copyElement(consolidateShipmentIpDoc, eleShipNodePersonInfo, eleToAddress);
      GCXMLUtil.copyElement(consolidateShipmentIpDoc, eleShipNodePersonInfofrom, eleFromAddress);

      YFCDocument getOrderReleaselistOutTmp = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_RELEASE_TMPLATE);
      YFCDocument getOrderReleaseListInp = YFCDocument.createDocument(GCConstants.ORDER_RELEASE);
      YFCElement eleOrderrelease = getOrderReleaseListInp.getDocumentElement();
      eleOrderrelease.setAttribute(GCConstants.ENTERPRISE_CODE, sEnterprise);
      eleOrderrelease.setAttribute(GCConstants.DOCUMENT_TYPE, sDocumentType);
      // Adding orderHeaderKey in getOrderReleaseList Input and removed orderNo
      eleOrderrelease.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
      YFCDocument getOrderReleaselistOutput =
          GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_RELEASE_LIST, getOrderReleaseListInp,
              getOrderReleaselistOutTmp);
      YFCElement eleOrderReleaseList = getOrderReleaselistOutput.getDocumentElement();
      String sLastOrderReleaseKey = eleOrderReleaseList.getAttribute(GCConstants.LAST_ORDER_RELEASE_KEY);
      final YFCNodeList<YFCElement> nlgetOrderReleaseList =
          eleOrderReleaseList.getElementsByTagName(GCConstants.ORDER_RELEASE);
      String sRelsno = null;
      for (YFCElement eleOrderRelease : nlgetOrderReleaseList) {
        String sOrderReleaseKey = eleOrderRelease.getAttribute(GCConstants.ORDER_RELEASE_KEY);
        if (YFCUtils.equals(sLastOrderReleaseKey, sOrderReleaseKey)) {
          sRelsno = eleOrderRelease.getAttribute(GCConstants.RELEASE_NO);
        }
      }

      //eleReleaseOrderforConsolidation.setAttribute(GCConstants.ORDER_NO, sOrderNo);
      eleReleaseOrderforConsolidation.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
      eleReleaseOrderforConsolidation.setAttribute(GCConstants.DOCUMENT_TYPE, sDocumentType);
      eleReleaseOrderforConsolidation.setAttribute(GCConstants.ENTERPRISE_CODE, sEnterprise);

      eleReleaseOrderforConsolidation.setAttribute(GCConstants.RELEASE_NO, sRelsno);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("consolidateShipmentIpDoc : " + consolidateShipmentIpDoc.toString());
      }

      GCCommonUtil.invokeAPI(env, GCConstants.CONSOLIDATE_TO_SHIPMENT_API, consolidateShipmentIpDoc.getDocument());
    }
    LOGGER.verbose("GCProcessShipBackToDC.processTransferOrderOnBackorder--End");
    LOGGER.endTimer("GCProcessShipBackToDC.processTransferOrderOnBackorder");
  }

  /**
   *
   * @param env
   * @param inDocument
   */
  public void changeStatusOnReceiptUpdate(YFSEnvironment env, Document inDocument) {
    LOGGER.beginTimer("GCProcessShipBackToDC.changeStatusOnReceiptUpdate");
    LOGGER.verbose("GCProcessShipBackToDC.changeStatusOnReceiptUpdate--Begin");
    YFCDocument yfcDocinDocument = YFCDocument.getDocumentFor(inDocument);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("inputDocument : " + yfcDocinDocument.toString());
    }

    YFCElement yfcEleOrderDcUpdate = yfcDocinDocument.getDocumentElement();
    String sReturnVPONo = yfcEleOrderDcUpdate.getAttribute(GCConstants.RETURN_VPO_NO);
    sReturnVPONo = sReturnVPONo.substring(3);

    YFCDocument yfcDocCOSInTransit = YFCDocument.getDocumentFor(GCConstants.CHANGE_ORDER_STATUS_INTRANSIT);
    YFCElement yfcEleCOSInTransitOrderLines = yfcDocCOSInTransit.getElementsByTagName(GCConstants.ORDER_LINES).item(0);
    YFCDocument yfcDocCOSReturnINVC = YFCDocument.getDocumentFor(GCConstants.CHANGE_ORDER_STATUS_RETURN_INVC);
    YFCElement yfcDocCOSReturnINVCOrderLines =
        yfcDocCOSReturnINVC.getElementsByTagName(GCConstants.ORDER_LINES).item(0);
    YFCDocument yfcDocCOSReturnedToStore =
        YFCDocument.getDocumentFor(GCConstants.CHANGE_ORDER_STATUS_RETURNED_TO_STORE);
    YFCElement yfcEleCOSRTSOrderLines = yfcDocCOSReturnedToStore.getElementsByTagName(GCConstants.ORDER_LINES).item(0);

    final YFCNodeList<YFCElement> nlOrderLineDCUpdate = yfcEleOrderDcUpdate.getElementsByTagName(GCConstants.ORDER_LINE);

    // invoke get order list for the return order
    YFCDocument getOrderlistOpTmp = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_TEMPLATE_SHIP_BACK);
    YFCDocument getOrderListIpDoc = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleOrder = getOrderListIpDoc.getDocumentElement();
    eleOrder.setAttribute(GCConstants.ORDER_NO, sReturnVPONo);
    eleOrder.setAttribute(GCConstants.DOCUMENT_TYPE, GCConstants.RETURN_ORDER_DOCUMENT_TYPE);
    YFCDocument getOrderListOpDOC =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListIpDoc, getOrderlistOpTmp);
    YFCElement eleOrderOrderListOp = getOrderListOpDOC.getDocumentElement();
    YFCElement eleOrderop = eleOrderOrderListOp.getChildElement(GCConstants.ORDER);
    String sOrderHeaderKey = eleOrderop.getAttribute(GCConstants.ORDER_HEADER_KEY);
    YFCElement yfcEleOrderLinesGetOrderList = eleOrderop.getChildElement(GCConstants.ORDER_LINES);

    // Invoke GetOrderList to fetch Transfer orders for the return order
    YFCDocument yfcGetTransferOrderListTmpDoc =
        YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_TEMPLATE_TRANSFER);
    YFCDocument yfcGetTransferOrderListInpDoc = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement yfcEleGetTransferOrder = yfcGetTransferOrderListInpDoc.getDocumentElement();
    YFCElement yfcEleGetTransferOrderLne = yfcEleGetTransferOrder.createChild(GCConstants.ORDER_LINE);
    YFCElement yfcEleDerivedFrom = yfcEleGetTransferOrderLne.createChild(GCConstants.DERIVED_FROM);
    yfcEleDerivedFrom.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    YFCDocument yfcGetTransferOrderListOupDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, yfcGetTransferOrderListInpDoc,
            yfcGetTransferOrderListTmpDoc);

    //Set order header key for all three change order status input
    yfcEleCOSInTransitOrderLines.getParentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    yfcDocCOSReturnINVCOrderLines.getParentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    yfcEleCOSRTSOrderLines.getParentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);

    // iterating over the input order lines
    for (YFCElement yfcEleOrderLine : nlOrderLineDCUpdate) {
      String sPrimeLineNo = yfcEleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
      Double dRecptQtyInput = yfcEleOrderLine.getDoubleAttribute(GCConstants.QUANTITY);

      // fetch order line list from get order list document
      final YFCNodeList<YFCElement> yfcNlOrderLineGetOrderList =
          yfcEleOrderLinesGetOrderList.getElementsByTagName(GCConstants.ORDER_LINE);

      // Loop over return order lines
      for (YFCElement yfcEleOrderLineGetOrderList : yfcNlOrderLineGetOrderList) {
        String sPrimeLineNoGetOrderList = yfcEleOrderLineGetOrderList.getAttribute(GCConstants.PRIME_LINE_NO);
        String sSubLineNoGetOrderList = yfcEleOrderLineGetOrderList.getAttribute(GCConstants.SUB_LINE_NO);

        // matching the PrimeLine number of the Input and the getOrderList
        if (YFCUtils.equals(sPrimeLineNoGetOrderList, sPrimeLineNo)) {
          String sIsBundle = yfcEleOrderLineGetOrderList.getAttribute(GCConstants.IS_BUNDLE_PARENT);
          YFCElement eleItem = yfcEleOrderLineGetOrderList.getChildElement(GCConstants.ITEM);
          String sProductClass = eleItem.getAttribute(GCConstants.PRODUCT_CLASS);

          // checking if the item is set and bundle
          boolean isSetBundleItem = false;
          if (YFCUtils.equals(GCConstants.FLAG_Y, sIsBundle) && YFCUtils.equals(GCConstants.SET, sProductClass)) {
            isSetBundleItem = true;
          }

          YFCElement yfcEleOrderStatuses = yfcEleOrderLineGetOrderList.getChildElement(GCConstants.ORDER_STATUSES);
          String[] sStatusArray = {GCConstants.STATUS};
          yfcEleOrderStatuses.sortChildren(sStatusArray, false);

          final YFCNodeList<YFCElement> nlOrderStatus =
              yfcEleOrderStatuses.getElementsByTagName(GCConstants.ORDER_STATUS);

          for (YFCElement yfcEleOrderLineStatus : nlOrderStatus) {
            String sOrderLineStatus = yfcEleOrderLineStatus.getAttribute(GCConstants.STATUS);
            Double dStatusQty = yfcEleOrderLineStatus.getDoubleAttribute(GCConstants.STATUS_QTY);
            Double dQtyTobeReceived = 0.0;

            if (dRecptQtyInput > dStatusQty) {
              dQtyTobeReceived = dStatusQty;
              dRecptQtyInput -= dStatusQty;
            } else {
              dQtyTobeReceived = dRecptQtyInput;
              dRecptQtyInput = 0.0;
            }

            if (YFCUtils.equals(GCConstants.IN_TRANSIT_STATUS_CODE, sOrderLineStatus)) {
              // Quantity are in In transit status, receive against this status qty
              updateOrderLineElement(yfcEleCOSInTransitOrderLines, sPrimeLineNoGetOrderList, sSubLineNoGetOrderList,
                  dQtyTobeReceived);
              if (isSetBundleItem) {
                updateComponentLines(yfcEleOrderLineGetOrderList, yfcEleCOSInTransitOrderLines, dQtyTobeReceived);
              }
            } else if (YFCUtils.equals(GCConstants.RETURN_INVC_STATUS_CODE, sOrderLineStatus)) {
              // Quantity are in In INVC status, receive against this status qty
              updateOrderLineElement(yfcDocCOSReturnINVCOrderLines, sPrimeLineNoGetOrderList, sSubLineNoGetOrderList,
                  dQtyTobeReceived);
              if (isSetBundleItem) {
                updateComponentLines(yfcEleOrderLineGetOrderList, yfcDocCOSReturnINVCOrderLines, dQtyTobeReceived);
              }
            } else if (YFCUtils.equals(GCConstants.SHIP_BACK_TO_DC_STATUS_CODE, sOrderLineStatus)) {
              // Quantities are in returned to store status, receive against this status qty

              updateOrderLineElement(yfcDocCOSReturnINVCOrderLines, sPrimeLineNoGetOrderList, sSubLineNoGetOrderList,
                  dQtyTobeReceived);
              cancelTransferOrderQty(env, yfcGetTransferOrderListOupDoc, sPrimeLineNoGetOrderList, dQtyTobeReceived);

              if (isSetBundleItem) {
                updateComponentLines(yfcEleOrderLineGetOrderList, yfcDocCOSReturnINVCOrderLines, dQtyTobeReceived);
              }
            } else if (YFCUtils.equals(GCConstants.RETURNED_TO_STORE_CODE, sOrderLineStatus)) {
              updateOrderLineElement(yfcEleCOSRTSOrderLines, sPrimeLineNoGetOrderList, sSubLineNoGetOrderList,
                  dQtyTobeReceived);
              if (isSetBundleItem) {
                updateComponentLines(yfcEleOrderLineGetOrderList, yfcEleCOSRTSOrderLines, dQtyTobeReceived);
              }
            } else {
              dRecptQtyInput += dQtyTobeReceived;
            }
            if (!(dRecptQtyInput > 0) && !(YFCUtils.equals(GCConstants.RETURNED_STATUS, sOrderLineStatus))) {
              //No quantity left to receive, break the status loop
              break;
            }
            // End of for loop over statuses
          }
          break;
        }
      }
    }
    // Prepare and invoke MULTIAPI for upating return order status
    YFCDocument yfcDocMultipleApi = YFCDocument.createDocument(GCXmlLiterals.MULTI_API);
    YFCElement yfcEleMultiApi = yfcDocMultipleApi.getDocumentElement();
    // Check for In transit transaction
    if (yfcEleCOSInTransitOrderLines.getElementsByTagName(GCConstants.ORDER_LINE).getLength() > 0) {
      yfcEleMultiApi.importNode(yfcDocCOSInTransit.getDocumentElement());
    }
    // Check for return invc transaction
    if (yfcDocCOSReturnINVCOrderLines.getElementsByTagName(GCConstants.ORDER_LINE).getLength() > 0) {
      yfcEleMultiApi.importNode(yfcDocCOSReturnINVC.getDocumentElement());
    }
    // Check for returned to store transaction
    if (yfcEleCOSRTSOrderLines.getElementsByTagName(GCConstants.ORDER_LINE).getLength() > 0) {
      yfcEleMultiApi.importNode(yfcDocCOSReturnedToStore.getDocumentElement());
    }

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Final yfcDocMultipleApi : " + yfcDocMultipleApi.toString());
    }
    GCCommonUtil.invokeAPI(env, GCConstants.API_MULTI, yfcDocMultipleApi, null);

    LOGGER.verbose("GCProcessShipBackToDC.changeStatusOnReceiptUpdate--End");
    LOGGER.endTimer("GCProcessShipBackToDC.changeStatusOnReceiptUpdate");
  }

  /**
   * This method will cancel corresponding transfer order qty
   * @param env
   * @param yfcGetTransferOrderListOupDoc
   * @param sPrimeLineNoGetOrderList
   * @param dQtyTobeReceived
   */
  private void cancelTransferOrderQty(YFSEnvironment env, YFCDocument yfcGetTransferOrderListOupDoc,
      String sPrimeLineNoGetOrderList, Double dQtyTobeReceived) {
    LOGGER.beginTimer("GCProcessShipBackToDC.cancelTransferOrderQty");
    LOGGER.verbose("GCProcessShipBackToDC.cancelTransferOrderQty--Begin");
    boolean bToProcess;
    YFCElement yfcEleGetTransferOrderOutDoc = yfcGetTransferOrderListOupDoc.getDocumentElement();
    YFCElement eleOrd = yfcEleGetTransferOrderOutDoc.getChildElement(GCConstants.ORDER);
    YFCElement eleOrderLinesTran = eleOrd.getChildElement(GCConstants.ORDER_LINES);

    final YFCNodeList<YFCElement> nlOrderlineTransfer = eleOrderLinesTran.getElementsByTagName(GCConstants.ORDER_LINE);

    for (YFCElement eleOrderLineTran : nlOrderlineTransfer) {
      YFCElement eleDerivedOrdrLineTran = eleOrderLineTran.getChildElement(GCConstants.DERIVED_FROM_ORDER_LINE);
      String sPrimelineNoTransferOrder = eleDerivedOrdrLineTran.getAttribute(GCConstants.PRIME_LINE_NO);
      if (YFCUtils.equals(sPrimeLineNoGetOrderList, sPrimelineNoTransferOrder)) {
        // Cancel respective qty from transfer order
        String sOrderlineKey = eleOrderLineTran.getAttribute(GCConstants.ORDER_LINE_KEY);

        // invoke get Shipment List API for order line key
        YFCDocument yfcDocGetShipmentListInp = YFCDocument.getDocumentFor(GCConstants.GET_SHIPMENT_LIST_TRANSFER_ORDER_INP);
        YFCElement yfcEleShipmentLineInp = yfcDocGetShipmentListInp.getElementsByTagName(GCConstants.SHIPMENT_LINE).item(0);
        yfcEleShipmentLineInp.setAttribute(GCConstants.ORDER_LINE_KEY, sOrderlineKey);

        YFCDocument yfcDocGetShipmentListTmp = YFCDocument.getDocumentFor(GCConstants.GET_SHIPMENT_LIST_TRANSFER_ORDER_TMP);
        YFCDocument yfcDocGetShipmentListOup = GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, yfcDocGetShipmentListInp, yfcDocGetShipmentListTmp);
        LOGGER.verbose("yfcDocGetShipmentListOup:"+yfcDocGetShipmentListOup);
        
        //Start of GCSTORE-6816
       Document docGetShipmentListOup = yfcDocGetShipmentListOup.getDocument();
       Element eleOrderLineKey = GCXMLUtil.getElementByXPath(docGetShipmentListOup,
				"//Shipments/Shipment/ShipmentLines/ShipmentLine[@OrderLineKey='"+sOrderlineKey+"']");
        if (!YFCCommon.isVoid(eleOrderLineKey))
        {	String sShipmentKey = yfcDocGetShipmentListOup.getDocumentElement().getChildElement(GCConstants.SHIPMENT).getAttribute(GCConstants.SHIPMENT_KEY);
        	LOGGER.verbose("sShipmentKey:"+sShipmentKey);
        	String sShipmentLineKey = eleOrderLineKey.getAttribute(GCConstants.SHIPMENT_LINE_KEY);
        	LOGGER.verbose("sShipmentLineKey:" +sShipmentLineKey);
        	
        	//start of code for Shipment Being Packed
        	YFCElement yfcEleGetShipListRoot = yfcDocGetShipmentListOup.getDocumentElement();
        	YFCNodeList<YFCElement> yfcEleContainerDetail = yfcEleGetShipListRoot.getElementsByTagName(GCConstants.CONTAINER_DETAIL);
        	
        	List<String> yfcEleShipmentLineKeyList = new ArrayList<String>();
     
        	for (YFCElement yfcEleShipLineKey : yfcEleContainerDetail)
        	{	
        		String sShipmentLineKeyContDetails = yfcEleShipLineKey.getAttribute(GCConstants.SHIPMENT_LINE_KEY);
        		if(YFCUtils.equals(sShipmentLineKey, sShipmentLineKeyContDetails))
        		{
        			String sShipmentContainerKey = yfcEleShipLineKey.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
        			yfcEleShipmentLineKeyList.add(sShipmentContainerKey);
        			LOGGER.verbose("sShipmentContainerKey:"+sShipmentContainerKey);
        		}
        	}
        	 //creating the input to unpackShipment API
        	for(int listCount = 0; listCount<yfcEleShipmentLineKeyList.size();listCount++ )
        	{
        	YFCDocument yfcDocUnpackShipmentInp = YFCDocument.createDocument(GCConstants.SHIPMENT);
        	YFCElement yfcEleUnpackShipment = yfcDocUnpackShipmentInp.getDocumentElement();
        	yfcEleUnpackShipment.setAttribute(GCConstants.SHIPMENT_KEY, sShipmentKey);
        	YFCElement yfcEleContainers = yfcEleUnpackShipment.createChild(GCConstants.CONTAINERS);
        	YFCElement yfcEleContainer = yfcEleContainers.createChild(GCConstants.CONTAINER);
        	String strShipmentContainerKey = yfcEleShipmentLineKeyList.get(listCount);
        	yfcEleContainer.setAttribute(GCConstants.SHIPMENT_CONTAINER_KEY, strShipmentContainerKey);
        	LOGGER.verbose("strShipmentContainerKey:"+strShipmentContainerKey);
        	LOGGER.verbose("yfcDocUnpackShipmentInp:"+yfcDocUnpackShipmentInp);
        	GCCommonUtil.invokeAPI(env, GCConstants.UNPACK_SHIPMENT, yfcDocUnpackShipmentInp, null);
        	}
        	//End of code for Shipment Being Packed
        	        	
        	String sShipmentQty = eleOrderLineKey.getAttribute(GCConstants.QUANTITY);
        	LOGGER.verbose("sShipmentQty:" +sShipmentQty);
        	Double dbShipmentQty = (double) Integer.parseInt(sShipmentQty);
        	LOGGER.verbose("dbShipmentQty:"+dbShipmentQty);
        	//End of GCSTORE-6816
        	
        YFCDocument yfcDocChangeShipmentInp = YFCDocument.createDocument(GCConstants.SHIPMENT);
        YFCElement yfcEleChangeShipment = yfcDocChangeShipmentInp.getDocumentElement();
        yfcEleChangeShipment.setAttribute(GCConstants.SHIPMENT_KEY, sShipmentKey);
        yfcEleChangeShipment.setAttribute(GCConstants.CANCEL_RMVD_QTY, GCConstants.YES);

        YFCElement yfcEleChangeShipmentLines = yfcEleChangeShipment.createChild(GCConstants.SHIPMENT_LINES);
        YFCElement yfcEleChangeShipmentLine = yfcEleChangeShipmentLines.createChild(GCConstants.SHIPMENT_LINE);
        yfcEleChangeShipmentLine.setAttribute(GCConstants.SHIPMENT_LINE_KEY, sShipmentLineKey);
        yfcEleChangeShipmentLine.setAttribute(GCConstants.QUANTITY, dbShipmentQty - dQtyTobeReceived);
        LOGGER.verbose("yfcEleChangeShipmentLine:"+yfcEleChangeShipmentLine);
        // invoke API to cancel transfer order qty
        GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_SHIPMENT, yfcDocChangeShipmentInp, null);
        break;
      }
      // End of transfer order line loop
    }
    LOGGER.verbose("GCProcessShipBackToDC.cancelTransferOrderQty--End");
    LOGGER.endTimer("GCProcessShipBackToDC.cancelTransferOrderQty");
  }
    }
  
  

  /**
   * This method will update & append order line element to OrderLine
   *
   * @param yfcDocCOSOrderLines
   * @param sPrimeLineNoGetOrderList
   * @param sSubLineNoGetOrderList
   * @param dStatusQty
   */
  private void updateOrderLineElement(YFCElement yfcDocCOSOrderLines, String sPrimeLineNoGetOrderList,
      String sSubLineNoGetOrderList, Double dStatusQty) {
    LOGGER.beginTimer("GCProcessShipBackToDC.updateOrderLineElement");
    LOGGER.verbose("GCProcessShipBackToDC.updateOrderLineElement--Begin");

    YFCElement yfcDocCOSReturnINVCOrderLine = yfcDocCOSOrderLines.createChild(GCConstants.ORDER_LINE);
    yfcDocCOSReturnINVCOrderLine.setAttribute(GCConstants.PRIME_LINE_NO, sPrimeLineNoGetOrderList);
    yfcDocCOSReturnINVCOrderLine.setAttribute(GCConstants.SUB_LINE_NO, sSubLineNoGetOrderList);
    yfcDocCOSReturnINVCOrderLine.setAttribute(GCConstants.BASE_DROP_STATUS, GCConstants.RETURNED_STATUS);
    YFCElement yfcEleCOSReturnINVCOrderLineTranQty =
        yfcDocCOSReturnINVCOrderLine.createChild(GCConstants.ORDER_LINE_TRAN_QUANTITY);
    yfcEleCOSReturnINVCOrderLineTranQty.setAttribute(GCConstants.QUANTITY, dStatusQty.toString());
    LOGGER.verbose("GCProcessShipBackToDC.updateOrderLineElement--End");
    LOGGER.endTimer("GCProcessShipBackToDC.updateOrderLineElement");
  }

  /**
   * This method will fetch and append component items for RETAIL SET items
   * @param yfcEleOrderLineGetOrderList
   * @param yfcDocCOSOrderLines
   * @param dStatusQuantity
   */
  private void updateComponentLines(YFCElement yfcEleOrderLineGetOrderList, YFCElement yfcDocCOSOrderLines,
      Double dStatusQuantity) {
    LOGGER.beginTimer("GCProcessShipBackToDC.updateComponentLines");
    LOGGER.verbose("GCProcessShipBackToDC.updateComponentLines--Begin");
    String sKitQty = null;
    String sPrimeLineNoComponent = null;
    String sSublineNoComponent = null;

    YFCNodeList<YFCElement> nlComponents = yfcEleOrderLineGetOrderList.getElementsByTagName("BundleComponent");
    for (YFCElement yfcEleComponentLine : nlComponents) {
      sPrimeLineNoComponent = yfcEleComponentLine.getAttribute(GCConstants.PRIME_LINE_NO);
      sSublineNoComponent = yfcEleComponentLine.getAttribute(GCConstants.SUB_LINE_NO);
      sKitQty = yfcEleComponentLine.getAttribute(GCXmlLiterals.KIT_QTY);
      Double dStatusQtyforCompnent = dStatusQuantity * Double.parseDouble(sKitQty);
      updateOrderLineElement(yfcDocCOSOrderLines, sPrimeLineNoComponent, sSublineNoComponent, dStatusQtyforCompnent);
    }
    LOGGER.verbose("GCProcessShipBackToDC.updateComponentLines--End");
    LOGGER.endTimer("GCProcessShipBackToDC.updateComponentLines");
  }

  private boolean isLineAlloedToAddToTransferOrder(String sPrimeLineNo,
      String sPrmlinNo, String sKitCode,
      String sProdcutclass, YFCElement eleBundleParentLine) {

    if (YFCUtils.equals(sPrimeLineNo, sPrmlinNo)
        && (YFCUtils.equals(sKitCode, GCConstants.BUNDLE) && YFCUtils.equals(sProdcutclass, GCConstants.SET) && YFCCommon
            .isVoid(eleBundleParentLine))) {
      return true;
    } else if (YFCUtils.equals(sPrimeLineNo, sPrmlinNo)
        && (!YFCUtils.equals(sKitCode, GCConstants.BUNDLE) && YFCUtils.equals(sProdcutclass, GCConstants.REGULAR) && !YFCCommon
            .isVoid(eleBundleParentLine))) {
      return true;
    } else if (YFCUtils.equals(sPrimeLineNo, sPrmlinNo)
        && (!YFCUtils.equals(sKitCode, GCConstants.BUNDLE) && YFCUtils.equals(sProdcutclass, GCConstants.REGULAR) && YFCCommon
            .isVoid(eleBundleParentLine))) {
      return true;
    }

    return false;
  }
}
