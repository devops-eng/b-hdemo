/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Change order line status on the basis of disposition code
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               03/18/2015        Singh, Gurpreet            GCSTORE-556: Store Returns
 *#################################################################################################################################################################
 */
package com.gc.api.returns;

import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:gurpreet.singh@expicient.com">Gurpreet/a> This class is invoked from
 *         service GCProcessStoreReturnsService. Prepare separate documents on the basis of
 *         disposition code and and change order line status
 */
public class GCProcessStoreReturns implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCProcessStoreReturns.class);

  /**
   *
   * Prepare separate documents for KIS, ReceiveInStore and ReturnedToStore orders, loop over the
   * order lines and add lines to the document depending upon disposition code. Invoke the
   * respective services for each document
   *
   * @param env
   * @param inDoc
   *
   */
  public void processStoreReturn(YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("GCProcessStoreReturns.processStoreReturn");
    LOGGER.verbose("Class: GCProcessStoreReturns Method: processStoreReturn START");
    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" processStoreReturn() method, yfcInDoc is" + yfcInDoc.toString());
    }
    YFCElement eleInDoc = yfcInDoc.getDocumentElement();

    YFCDocument docKeepInStore = prepareHeaderDocument(yfcInDoc);
    docKeepInStore.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, yfcInDoc.getDocumentElement().getAttribute(GCConstants.ORDER_HEADER_KEY));

    YFCElement eleKISOrderLines = appendOrderLinesElement(docKeepInStore);
    YFCDocument docReceiveInStore = prepareHeaderDocument(yfcInDoc);
    docReceiveInStore.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, yfcInDoc.getDocumentElement().getAttribute(GCConstants.ORDER_HEADER_KEY));
    YFCElement eleReceiveInStoreOrderLines = appendOrderLinesElement(docReceiveInStore);
    YFCDocument docReturnedToStore = prepareHeaderDocument(yfcInDoc);
    docReturnedToStore.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, yfcInDoc.getDocumentElement().getAttribute(GCConstants.ORDER_HEADER_KEY));
    YFCElement eleReturnedToStoreOrderLines = appendOrderLinesElement(docReturnedToStore);



    boolean bIsKeepInStore = false;
    boolean bIsReceiveInStore = false;
    boolean bIsReturnedToStore = false;
    YFCElement eleOrderLines = eleInDoc.getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> nlOrderLines = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
    // Loop over the order lines and add line to the document depending upon disposition code
    for (YFCElement eleOrderLine : nlOrderLines) {
      LOGGER.verbose(" Looping over all the order lines ");
      String sDispositionCode = eleOrderLine.getAttribute(GCXmlLiterals.DISPOSITION_CODE);
      LOGGER.verbose(" Disposition Code is :: " + sDispositionCode);
      if (YFCCommon.isVoid(sDispositionCode)
          || YFCCommon.equals(GCConstants.KEEP_IN_STORE_DISPOSITION_CODE, sDispositionCode)) {
        YFCElement eleKISOrderLine = docKeepInStore.createElement(GCConstants.ORDER_LINE);
        GCXMLUtil.copyAttributes(eleOrderLine, eleKISOrderLine);
        eleKISOrderLines.appendChild(eleKISOrderLine);
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose(" processStoreReturn() method, docKeepInStore is" + docKeepInStore.toString());
        }
        bIsKeepInStore = true;
      } else if (YFCCommon.equals(GCConstants.RECEIVE_IN_STORE_DISPOSITION_CODE, sDispositionCode)) {
        YFCElement eleReceiveInStoreOrderLine = docReceiveInStore.createElement(GCConstants.ORDER_LINE);
        GCXMLUtil.copyAttributes(eleOrderLine, eleReceiveInStoreOrderLine);
        eleReceiveInStoreOrderLines.appendChild(eleReceiveInStoreOrderLine);
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose(" processStoreReturn() method, docReceiveInStore is" + docReceiveInStore.toString());
        }
        bIsReceiveInStore = true;
      } else if (YFCCommon.equals(GCConstants.RETURNED_TO_STORE_DISPOSITION_CODE, sDispositionCode)) {
        YFCElement eleReturnedToStoreOrderLine = docReturnedToStore.createElement(GCConstants.ORDER_LINE);
        GCXMLUtil.copyAttributes(eleOrderLine, eleReturnedToStoreOrderLine);
        eleReturnedToStoreOrderLines.appendChild(eleReturnedToStoreOrderLine);
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose(" processStoreReturn() method, docReturnedToStore is" + docReturnedToStore.toString());
        }
        bIsReturnedToStore = true;
      }
    }
    LOGGER.verbose(" bIsKeepInStore :: " + bIsKeepInStore + " bIsReceiveInStore :: " + bIsReceiveInStore
        + " bIsReturnedToStore :: " + bIsReturnedToStore);
    if (bIsKeepInStore) {
      invokeChangeOrderStatus(env, docKeepInStore, GCConstants.RETURNED_TRANSACTION, GCConstants.RETURNED_STATUS);
    }
    if (bIsReceiveInStore) {
      invokeChangeOrderStatus(env, docReceiveInStore, GCConstants.RECEIVE_IN_STORE_TRANSACTION,
          GCConstants.RECEIVE_IN_STORE_STATUS);
    }
    if (bIsReturnedToStore) {
      invokeChangeOrderStatus(env, docReturnedToStore, GCConstants.RETURNED_TO_STORE_TRANSACTION,
          GCConstants.RETURNED_TO_STORE_STATUS);
    }
    LOGGER.verbose("Class: GCProcessStoreReturns Method: processStoreReturn END");
    LOGGER.endTimer("GCProcessStoreReturns.processStoreReturn");
  }

  /**
   *
   * Create a new document with root element Order and copy attributes to this doc form the input document
   *
   * @param yfcInDoc
   * @return
   *
   */
  private YFCDocument prepareHeaderDocument(YFCDocument yfcInDoc) {
    LOGGER.beginTimer("GCProcessStoreReturns.prepareHeaderDocument");
    LOGGER.verbose("Class: GCProcessStoreReturns Method: prepareHeaderDocument START");
    YFCDocument yfcNewDoc = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleNewDoc = yfcNewDoc.getDocumentElement();
    eleNewDoc.getAttributes(yfcInDoc.getDocumentElement().getAttributes());
    LOGGER.verbose("Class: GCProcessStoreReturns Method: prepareHeaderDocument END");
    LOGGER.endTimer("GCProcessStoreReturns.prepareHeaderDocument");
    return yfcNewDoc;
  }

  /**
   *
   * Append OrderLines element to the document
   *
   * @param inDoc
   * @return
   *
   */
  private YFCElement appendOrderLinesElement(YFCDocument inDoc) {
    LOGGER.beginTimer("GCProcessStoreReturns.appendOrderLinesElement");
    LOGGER.verbose("Class: GCProcessStoreReturns Method: appendOrderLinesElement START");
    YFCElement eleOrderLines = inDoc.createElement(GCConstants.ORDER_LINES);
    inDoc.getDocumentElement().appendChild(eleOrderLines);
    LOGGER.verbose("Class: GCProcessStoreReturns Method: appendOrderLinesElement END");
    LOGGER.endTimer("GCProcessStoreReturns.appendOrderLinesElement");
    return eleOrderLines;
  }


  /**
   * Invoke changeOrderStatus API to change the order line status
   *
   * @param env
   * @param yfcInDoc
   * @param sTransactionId
   * @param sDropStatus
   */
  private void invokeChangeOrderStatus(YFSEnvironment env, YFCDocument yfcInDoc, String sTransactionId,
      String sDropStatus) {
    YFCElement eleInDoc = yfcInDoc.getDocumentElement();
    String sOrderHeaderKey = eleInDoc.getAttribute(GCConstants.ORDER_HEADER_KEY);
    YFCDocument docChangeOrderStatus = GCCommonUtil.prepareChangeOrderStatusHeaderDoc(sOrderHeaderKey, sTransactionId);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" processReturnedToStoreReturns() method, inDoc is" + yfcInDoc.toString());
    }

    YFCElement changeOrderStatusOrderLines = docChangeOrderStatus.getDocumentElement().getChildElement("OrderLines");
    YFCElement eleOrderLines = eleInDoc.getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> nlOrderLines = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
    for (YFCElement eleOrderLine : nlOrderLines) {
      String sQuantity = eleOrderLine.getAttribute(GCConstants.ORDERED_QTY);
      LOGGER.verbose(" sQuantity " + sQuantity);
      GCCommonUtil.addLinesToChangeOdrStatusDoc(changeOrderStatusOrderLines, eleOrderLine, sQuantity, sDropStatus);
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" processReturnedToStoreReturns() method, docChangeOrderStatus is"
          + docChangeOrderStatus.toString());
    }
    GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER_STATUS, docChangeOrderStatus, null);

  }
  @Override
  public void setProperties(Properties paramProperties) {

  }

}
