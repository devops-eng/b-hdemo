/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 * ####################################################################################################################
 *	OBJECTIVE: This class will publish invoice to DAX and POS based on certain requirements.
 *######################################################################################################################
 *	Version     Date       Modified By              Description
 *######################################################################################################################
    1.0      25/03/2015   Mittal, Yashu		GCSTORE-556, GCSTORE-754
    2.0		 18/05/2017		Expicient		GCSTORE-6649
 *######################################################################################################################
 */
package com.gc.api.returns;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.security.KeyFactory;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCPaymentSecurity;
import com.gc.common.utils.GCWebServiceUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:yashu.mittal@expicient.com">Yashu Mittal</a>
 */
public class GCPublishReturnInvoice {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCPublishReturnInvoice.class.getName());
  private static final String CLASS_NAME = GCPublishReturnInvoice.class.getName();

  /**
   *
   *
   * This method will publish invoice to DAX and POS based on certain requirements.
   *
   * @param env
   * @param inDoc
   * @throws GCException  GCException
   * @throws Exception
   * @throws IOException
   *
   */
  public void publishReturnInvoice(YFSEnvironment env, Document inDoc) throws GCException{

    LOGGER.beginTimer("GCPublishReturnInvoice.publishReturnInvoice");
    LOGGER.verbose("Class: GCPublishReturnInvoice Method: publishReturnInvoice START");


    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    if(LOGGER.isDebugEnabled()){
      LOGGER.verbose("Input Doc to GCPublishReturnInvoice.publishReturnInvoice method is:-->" + yfcInDoc.toString());
    }

    YFCElement eleOrder = yfcInDoc.getElementsByTagName(GCConstants.ORDER).item(0);
    YFCElement eleOrderExtn = eleOrder.getElementsByTagName(GCConstants.EXTN).item(0);

    String sOrderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);

    boolean bIsStoreFulfilledLinePresent = false;
    boolean bIsRISLinePresent = false;

    LOGGER.verbose("Loop over orderlines to check if any orderline is present with disposition code as Receive in Store OR any line present which is not store fulfilled.");
    YFCNodeList<YFCElement> nlLineDetails = yfcInDoc.getElementsByTagName(GCConstants.LINE_DETAIL);
    for (YFCElement eleLineDetail : nlLineDetails) {
      YFCElement eleOrderLine = eleLineDetail.getChildElement(GCConstants.ORDER_LINE);
      YFCElement eleOrderLineExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
      String sDispositionCode = eleOrderLine.getAttribute(GCXmlLiterals.DISPOSITION_CODE);
      String sExtnIsStoreFulfilled = eleOrderLineExtn.getAttribute(GCXmlLiterals.EXTN_IS_STORE_FULFILLED);

      if (YFCCommon.equals(GCConstants.RECEIVE_IN_STORE_DISPOSITION_CODE, sDispositionCode)) {
        bIsRISLinePresent = true;
      }
      if (YFCCommon.equals(GCConstants.YES, sExtnIsStoreFulfilled)) {
        bIsStoreFulfilledLinePresent = true;
      }
    }
    LOGGER.verbose("Loop End. Value of bIsStoreFulfilledPresent:" + bIsStoreFulfilledLinePresent + "And bIsRISLinePresent:" + bIsRISLinePresent);

    YFCElement yfcEleRootInDoc = yfcInDoc.getDocumentElement();
    YFCElement yfcEleInvoiceHeader = yfcEleRootInDoc.getElementsByTagName(GCConstants.INVOICE_HEADER).item(0);
    String sOrderInvoiceKey = yfcEleInvoiceHeader.getAttribute("OrderInvoiceKey");

    /* Check if any line is fulfilled from MFI - Start */
    String setSplitOrderFlag = GCConstants.FALSE_STRING;
    String sSalesOrderHeaderKey = yfcEleInvoiceHeader.getAttribute(GCConstants.DERIVED_FROM_ORDER_HEADER_KEY);
    YFCDocument getOrderLineListInSO = YFCDocument.getDocumentFor("<OrderLine OrderHeaderKey='"+ sSalesOrderHeaderKey +"'><Extn ExtnIsStoreFulfilled='N'/></OrderLine>");
    YFCDocument getOrderLineListOutSO = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LINE_LIST, getOrderLineListInSO, null);
    if (getOrderLineListOutSO.getDocumentElement().hasChildNodes()) {
      setSplitOrderFlag = GCConstants.TRUE_STRING;
    }
    yfcEleRootInDoc.setAttribute(GCXmlLiterals.SPLIT_ORDER_FLAG, setSplitOrderFlag);
    /* Check if any line is fulfilled from MFI - End */

    //fetch the manager id and user id and for ConfirmReturnAction and stamp on InvoiceDetail element.
    getManagerCashierApproveId(yfcEleRootInDoc, yfcEleInvoiceHeader);

    // Invoke getAndStampDates method to get order and XML creation date and time
    getAndStampDates(yfcEleRootInDoc);

    // Copy Paypal Transaction Id - GCSTORE-4900
    copyPaypalTransactionIDForPOS(env, inDoc);

    if (!bIsStoreFulfilledLinePresent) {
      LOGGER.verbose("All lines are fulfilled from DC. Publish invoice to DAX");
      if(LOGGER.isDebugEnabled()){
        LOGGER.verbose("Invoice Details Published to DAX:-->" + inDoc.toString());
      }
      GCCommonUtil.invokeService(env, GCConstants.GC_PUBLISH_INVOICE, inDoc);

      // Fix for 6333 : Start

      YFCDocument changeOrderInvoice = YFCDocument.createDocument(GCConstants.ORDER_INVOICE);
      YFCElement eleOrderInvoice = changeOrderInvoice.getDocumentElement();
      eleOrderInvoice.setAttribute(GCConstants.ORDER_INVOICE_KEY, sOrderInvoiceKey);
      eleOrderInvoice.setAttribute("StampTickets", "True");
      YFCElement eleExtn = eleOrderInvoice.createChild(GCConstants.EXTN);
      eleExtn.setAttribute("ExtnTargetSystem", "DAX");

      // Invoking this service to call changeOrderInvoice for stamping ExtnTargetSystem in case of
      // posting invoice to DAX as well.
      GCCommonUtil.invokeService(env, GCConstants.GC_PROCESS_POS_RESPONSE_SERVICE, changeOrderInvoice.getDocument());

      // Fix for 6333 : End

      boolean isStoreCredit = checkForStoreCredit(yfcInDoc);
      
      // GCSTORE 6008 START
      Element invHeaderEle = GCXMLUtil.getElementByXPath(inDoc,"/InvoiceDetail/InvoiceHeader");
      String totalAmount = invHeaderEle.getAttribute("TotalAmount");
      
      if(!YFCCommon.equals(totalAmount,"0.00")){ // GCSTORE 6008 END
	      if (YFCCommon.equals(eleOrderExtn.getAttribute(GCConstants.EXTN_IS_REFUND_ON_ACCOUNT), GCConstants.YES)
	          || isStoreCredit) {
	        LOGGER.verbose("ExtnRefundOnAccount is Y. Invoke GCInvoke70TicketPOSWebService to post 70ticket to POS. ");
	        // GCSTORE 5505 START
	        checkForPOSCustomerID(env, inDoc);
	        if(LOGGER.isDebugEnabled()){
	        	LOGGER.debug("inDoc after checkForPOSCustomerID"+GCXMLUtil.getXMLString(inDoc));
	        }
	        // GCSTORE 5505 END
	        GCCommonUtil.invokeService(env, GCConstants.GC_70_TICKET_POS_SERVICE, inDoc);
	      }
      }
      if (bIsRISLinePresent) {
        LOGGER.verbose("Atleast one Receive in Store line is present which is fulfilled from DC. Publish Inventory Transfer reference to POS.");

        YFCElement inputDocRootEle = YFCDocument.getDocumentFor(inDoc).getDocumentElement();
        YFCElement eleInvoiceHeader = inputDocRootEle.getChildElement(GCConstants.INVOICE_HEADER);
        YFCNodeList<YFCElement> nlLinDetails = eleInvoiceHeader.getElementsByTagName(GCConstants.LINE_DETAIL);
        for(YFCElement eleLineDetail : nlLinDetails){
          YFCElement eleOrderLine = eleLineDetail.getChildElement(GCConstants.ORDER_LINE);
          YFCElement eleOrderLineExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
          Object oExntPOS70Ticket = env.getTxnObject(GCXmlLiterals.POS_DAX_COA_TICKET);
          if(!YFCCommon.isVoid(oExntPOS70Ticket)){
            LOGGER.verbose("ExtnPOS70TicketNumber is " + oExntPOS70Ticket.toString());
            eleOrderLineExtn.setAttribute(GCXmlLiterals.EXTN_POS_70_TICKET_NO, oExntPOS70Ticket.toString() );
          }
        }
        GCCommonUtil.invokeService(env, GCConstants.GC_POST_INV_TRANSFER_REF_TO_POS_SERVICE, inDoc);
      }
    } else {

      LOGGER.verbose("All lines are fulfilled from Store. Posting invoice details to POS.");

      Element inDocRootEle = inDoc.getDocumentElement();
      inDocRootEle.setAttribute(GCConstants.MESSAGE_TYPE, GCConstants.MESSAGE_TYPE_RETURN);

      removeBundleParentLineAndProrateComponentPrices(inDoc);
      clubOrderLinesWithSimilarComps(inDoc);

      YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
      // Fix for GCSTORE-5904, 5963 -- Starts
      if (!YFCCommon.equals(eleOrderExtn.getAttribute(GCConstants.EXTN_IS_REFUND_ON_ACCOUNT), GCConstants.YES)) {
        handleExchangeReturn(inputDoc);
      } else {
        handleExchangeReturnStoreCredit(inputDoc);
      }
      // Fix for GCSTORE-5904, 5963 -- Ends
      if(LOGGER.isDebugEnabled()){
        LOGGER.verbose("Document prepared before passing details to XSL is " + inDoc.toString());
      }
      YFCDocument yfcDocInvokeWebServiceInput =
          GCCommonUtil.invokeService(env, GCXmlLiterals.GC_PREPARE_POS_WEB_SERVICE_INPUT, YFCDocument.getDocumentFor(inDoc));
      // GCSTORE-2837 : Start
      YFCNodeList<YFCElement> nlPOSAuthNumber =
          yfcDocInvokeWebServiceInput.getElementsByTagName(GCXmlLiterals.POS_AUTH_NUMBER);
      for (YFCElement elePOSAuthNumber : nlPOSAuthNumber) {
        String sAuthNumber = elePOSAuthNumber.getNodeValue();
        int iAuthNumberLength = sAuthNumber.length();
        if (iAuthNumberLength < GCXmlLiterals.FIVE_INT) {
          iAuthNumberLength = GCXmlLiterals.FIVE_INT - iAuthNumberLength;
          for (int iLength = 1; iLength <= iAuthNumberLength; iLength++) {
            sAuthNumber = sAuthNumber + GCXmlLiterals.SPACE_STRING;
          }
          elePOSAuthNumber.setNodeValue(sAuthNumber);
        }
      }
      // GCSTORE-2837 : End
      // Decrypt CreditCard No from OMS private key and encrypt using POS public key.

      encryptDecryptCCForPOS(yfcDocInvokeWebServiceInput);

      if(LOGGER.isDebugEnabled()){
        LOGGER.verbose("Invoice Details posted to POS:-->" + yfcDocInvokeWebServiceInput.toString());
      }
      Document docWebServiceOP = null;
      try {
        GCWebServiceUtil obj = new GCWebServiceUtil();
        // Changes made for MPOS Order Returns.
        String sEntryType = eleOrder.getAttribute(GCConstants.ENTRY_TYPE);
        if (YFCCommon.equals(sEntryType, GCConstants.MPOS)) {
        	
          /*docWebServiceOP = obj.invokeSOAPWebService(yfcDocInvokeWebServiceInput.getDocument(),YFSSystem.getProperty(GCConstants.GET_MPOS_RETURN_11TICKET_WEBSERVICE_URL),
              YFSSystem.getProperty(GCConstants.GET_MPOS_RETURN_11TICKET_WEBSERVICE_ACTION));*/
        	
        	 docWebServiceOP =
                    GCCommonUtil.invokeService(env, "GCMPOSPOSReturnService", yfcDocInvokeWebServiceInput.getDocument());

        } else {
          docWebServiceOP = obj.invokeSOAPWebService(yfcDocInvokeWebServiceInput.getDocument(),YFSSystem.getProperty(GCConstants.GET_RETURN_11TICKET_WEBSERVICE_URL),
              YFSSystem.getProperty(GCConstants.GET_RETURN_11TICKET_WEBSERVICE_ACTION));
        }
        YFCException yfcException = new YFCException();

        if(LOGGER.isDebugEnabled()){
          LOGGER.verbose("Response received from POS:-->" + YFCDocument.getDocumentFor(docWebServiceOP).toString());
        }

        //Fix for 3042 : Start

        YFCDocument changeOrderInvoice = YFCDocument.createDocument(GCConstants.ORDER_INVOICE);
        YFCElement eleOrderInvoice = changeOrderInvoice.getDocumentElement();
        eleOrderInvoice.setAttribute(GCConstants.ORDER_INVOICE_KEY, sOrderInvoiceKey);
        eleOrderInvoice.setAttribute("StampTickets", "True");
        YFCElement eleExtn = eleOrderInvoice.createChild(GCConstants.EXTN);
        eleExtn.setAttribute("ExtnTargetSystem", "POS");

        // Fix for 3042 : Stop

        NodeList nlReturnResultDetail = docWebServiceOP.getDocumentElement().getElementsByTagName("ReturnResult");
        NodeList nlFaultString = docWebServiceOP.getDocumentElement().getElementsByTagName("faultstring");
        if (nlReturnResultDetail.getLength() > 0) {
          Element eleConfirmResultDetail = (Element) nlReturnResultDetail.item(0);
          String sReturnDetail = eleConfirmResultDetail.getTextContent();
          if(!YFCCommon.isVoid(sReturnDetail)){
            YFCDocument yfcDocResponse = YFCDocument.getDocumentFor(sReturnDetail);
            YFCElement yfcEleResponse = yfcDocResponse.getDocumentElement();
            YFCElement eleSuccess = yfcEleResponse.getElementsByTagName(GCConstants.SUCCESS).item(0);
            String sSuccess =  eleSuccess.getNodeValue();
            if(YFCCommon.equals(sSuccess, GCConstants.TRUE)){
              YFCNodeList<YFCElement> nlResponse = yfcEleResponse.getElementsByTagName(GCXmlLiterals.POS_SALES_TICKET);
              YFCNodeList<YFCElement> nlPOSCOATicket = yfcEleResponse.getElementsByTagName("POSCOATicket");
              String sPOSCOATicket = "";
              if(nlPOSCOATicket.getLength()>0){
                YFCElement yfcPOSCOATicket = nlPOSCOATicket.item(0);
                if (!YFCCommon.isVoid(yfcPOSCOATicket)) {
                  sPOSCOATicket = yfcPOSCOATicket.getNodeValue();
                }
              }

              if(nlResponse.getLength()>0){
                YFCElement elePOSSalesTicket = nlResponse.item(0);
                String sExtnPOSReturn11TicketNo = elePOSSalesTicket.getNodeValue();
                LOGGER.verbose("POSReturn11Ticket" + sExtnPOSReturn11TicketNo);
                if (!YFCCommon.isVoid(sExtnPOSReturn11TicketNo)) {
                  LOGGER.verbose("Calling method append70TicketOrReturn11TicketAtOrderLine to stamp sExtnPOSReturn11TicketNo on orderlines");
                  if(!YFCCommon.isVoid(sPOSCOATicket)){
                    eleExtn.setAttribute("ExtnPOS70TicketNo", sPOSCOATicket);
                  }
                  eleExtn.setAttribute("ExtnPOSReturn11TicketNo", sExtnPOSReturn11TicketNo);
                  GCInvoke70TicketPOSWebService.append70TicketOrReturn11TicketAtOrderLine(env, sPOSCOATicket, sExtnPOSReturn11TicketNo, YFCDocument.getDocumentFor(inDoc));
                }else{
                  LOGGER.verbose("Response is received from POS, but POSReturn11TicketNo is not present in the output.");
                  inDocRootEle.setAttribute("POSError", GCConstants.POS_CONNECT_FAILURE_ERROR_DESC);
                  postMessageAndRollbackTransaction(env, inDoc, yfcException );
                }
              } else{
                LOGGER.verbose("Element POSSalesTicket is not present in web-service call.");
                inDocRootEle.setAttribute("POSError", GCConstants.POS_CONNECT_FAILURE_ERROR_DESC);
                postMessageAndRollbackTransaction(env, inDoc, yfcException);
              }

              // Fix for 3042 : Resume
              GCCommonUtil.invokeService(env, GCConstants.GC_PROCESS_POS_RESPONSE_SERVICE,changeOrderInvoice.getDocument());
              // Fix for 3042 : End

            } else{
              YFCNodeList<YFCElement> nlErrorInfo = yfcEleResponse.getElementsByTagName("Errorinfo");
              YFCElement eleErrorInfo = nlErrorInfo.item(0);
              if(!YFCCommon.isVoid(eleErrorInfo)){
                String sErrorInfo =  eleErrorInfo.getNodeValue();
                inDocRootEle.setAttribute("POSError", sErrorInfo);
              } else{
                inDocRootEle.setAttribute("POSError", GCConstants.POS_CONNECT_FAILURE_ERROR_DESC);
              }
              inDocRootEle.setAttribute("FailedResponse", "True");
              GCCommonUtil.invokeService(env, GCConstants.GC_PROCESS_POS_RESPONSE_SERVICE, inDoc);
            }

          } else{
            LOGGER.verbose("Output received in webservice response is null.");
            inDocRootEle.setAttribute("POSError", "ConfirmDetail Element received from POS Response is null");
            postMessageAndRollbackTransaction(env, inDoc, yfcException);
          }
        } else if(nlFaultString.getLength()>0) {
          Element eleFaultString = (Element) nlFaultString.item(0);
          String sFaultString = eleFaultString.getTextContent();
          inDocRootEle.setAttribute("POSError", sFaultString);
          inDocRootEle.setAttribute("FailedResponse", "True");
          GCCommonUtil.invokeService(env, GCConstants.GC_PROCESS_POS_RESPONSE_SERVICE, inDoc);

        } else {
          LOGGER.verbose("Output received in webservice response is null.");
          inDocRootEle.setAttribute("POSError", GCConstants.POS_CONNECT_FAILURE_ERROR_DESC);
          postMessageAndRollbackTransaction(env, inDoc, yfcException);
        }
      } catch (IOException ioException) {
        LOGGER.error("Inside catch block of GCPublishReturnInvoice.publishReturnInvoice(): ", ioException);
        YFCException yfcException = new YFCException(ioException);
        inDocRootEle.setAttribute("POSError", GCConstants.POS_CONNECT_FAILURE_ERROR_DESC);
        postMessageAndRollbackTransaction(env, inDoc, yfcException);
      }
    }

    LOGGER.endTimer("GCPublishReturnInvoice.publishReturnInvoice");
    LOGGER.verbose("Class: GCPublishReturnInvoice Method: publishReturnInvoice END");

  }

  // This method is used to check if ExtnPOSCustomerID is stamped. GCSTORE 5505
  private void checkForPOSCustomerID(YFSEnvironment env, Document inDoc) {
	// TODO Auto-generated method stub
	LOGGER.debug("Entering checkForPOSCustomerID in GCPublishReturnInvoice");
	YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
	
	YFCElement eleInvHeader = yfcInDoc.getElementsByTagName(GCConstants.INVOICE_HEADER).item(0);
	String sDerivedOHK = eleInvHeader.getAttribute(GCConstants.DERIVED_FROM_ORDER_HEADER_KEY);

	YFCElement eleOrder = yfcInDoc.getElementsByTagName(GCConstants.ORDER).item(0);
    YFCElement eleOrderExtn = eleOrder.getElementsByTagName(GCConstants.EXTN).item(0);
    String extnPOSCustID = eleOrderExtn.getAttribute("ExtnPOSCustomerID");
    String sOHK = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
    
    if(YFCCommon.isVoid(extnPOSCustID )){
    	
    	YFCDocument inDocVerifyPOSCust  = YFCDocument.createDocument(GCConstants.ORDER);
    	YFCElement eleOrderVerifyPOS = inDocVerifyPOSCust.getDocumentElement();
    	YFCElement eleOrderExtnVerifyPOS = inDocVerifyPOSCust.createElement(GCConstants.EXTN);
    	
    	GCXMLUtil.copyElement(inDocVerifyPOSCust, eleOrderExtn, eleOrderExtnVerifyPOS);
    	eleOrderVerifyPOS.appendChild(eleOrderExtnVerifyPOS);
    	
    	YFCElement eleOrderLineVerifyPOS = inDocVerifyPOSCust.createElement(GCConstants.ORDER_LINE);
    	eleOrderLineVerifyPOS.setAttribute("DerivedFromOrderHeaderKey", sDerivedOHK);
    	
    	YFCElement eleOrderLinesVerifyPOS = inDocVerifyPOSCust.createElement(GCConstants.ORDER_LINES);
    	eleOrderLinesVerifyPOS.appendChild(eleOrderLineVerifyPOS);
    	
    	eleOrderVerifyPOS.appendChild(eleOrderLinesVerifyPOS);
    	
    	if(LOGGER.isDebugEnabled()){
    		LOGGER.debug("inDoc to GCVerifyAndUpdatePOSCustomer in checkForPOSCustomerID"+ 
    					inDocVerifyPOSCust.getString());
    	}
    	
    	Document docOut =
    	        GCCommonUtil.invokeService(env, "GCVerifyAndUpdatePOSCustomer",inDocVerifyPOSCust.getDocument());
    	
    	if(LOGGER.isDebugEnabled()){
    		LOGGER.debug("outDoc from GCVerifyAndUpdatePOSCustomer in checkForPOSCustomerID"+ 
    				GCXMLUtil.getXMLString(docOut));
    	}
    	
    	if(!YFCCommon.isVoid(docOut)){
    		YFCDocument yfcDocOut =YFCDocument.getDocumentFor(docOut);
    		YFCElement eleDocOutOrderExtn = yfcDocOut.getElementsByTagName("Extn").item(0);
    		String docOutPOSCustID = eleDocOutOrderExtn.getAttribute("ExtnPOSCustomerID");
    		
    		if(!YFCCommon.isVoid("docOutPOSCustID")){
    			eleOrderExtn.setAttribute("ExtnPOSCustomerID", docOutPOSCustID);
    			
    			/*YFCDocument inDocChangeOrder  = YFCDocument.createDocument(GCConstants.ORDER);
    			YFCElement eleOrderChangeOrder = inDocChangeOrder.getDocumentElement();
    			eleOrderChangeOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOHK);
    			
    			YFCElement eleOrderExtnChangeOrder = inDocChangeOrder.createElement(GCConstants.EXTN);
    			eleOrderExtnChangeOrder.setAttribute("ExtnPOSCustomerID", docOutPOSCustID);
    			eleOrderChangeOrder.appendChild(eleOrderExtnChangeOrder);*/
    		}
    	}

    	
    }
    
}
  /**
   * This method is used to handle return of Exchange Orders where RefundType is COA i.e.
   * ExtnIsRefundOnAccount = 'Y'. In this, we'll remove all CollectionDetail elements except one and
   * we'll update PaymnetMethod as STORE_CREDIT and AmountCollected as TotalAmount.
   *
   * @param inputDoc
   */
  public void handleExchangeReturnStoreCredit(YFCDocument inputDoc) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(CLASS_NAME + "Inside handleExchangeReturnStoreCredit--Begin");
      LOGGER.debug("inputDoc : " + inputDoc.toString());
    }
    Element eleRootEle = inputDoc.getDocument().getDocumentElement();
    Element eleCollectionDetails = (Element) eleRootEle.getElementsByTagName("CollectionDetails").item(0);
    Element eleInvoiceHdr = (Element) eleRootEle.getElementsByTagName(GCConstants.INVOICE_HEADER).item(0);
    List<Element> yfcNLCollectionDetail =
        GCXMLUtil.getElementListByXpath(inputDoc.getDocument(),
            "InvoiceDetail/InvoiceHeader/CollectionDetails/CollectionDetail");
    Iterator<Element> itr = yfcNLCollectionDetail.iterator();
    int counter = 0;
    Element elePaymentMethod = null;
    while (itr.hasNext()) {
      LOGGER.debug("inside while");
      Element eleCollectionDtl = itr.next();
      if (counter == 0) {
        LOGGER.debug("inside if");
        elePaymentMethod = (Element) eleCollectionDtl.getElementsByTagName(GCConstants.PAYMENT_METHOD).item(0);
        if (YFCCommon.isVoid(elePaymentMethod)) {
          elePaymentMethod = inputDoc.getDocument().createElement(GCConstants.PAYMENT_METHOD);
          eleCollectionDtl.appendChild(elePaymentMethod);
        }
        elePaymentMethod.setAttribute(GCConstants.PAYMENT_TYPE, GCConstants.STORE_CREDIT_PAYMENT_TYPE);
        eleCollectionDtl.setAttribute(GCConstants.AMOUNT_COLLECTED,
            eleInvoiceHdr.getAttribute(GCConstants.AMOUNT_COLLECTED));
      } else {
        LOGGER.debug("inside else");
        eleCollectionDetails.removeChild(eleCollectionDtl);
      }
      counter++;
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(CLASS_NAME + "Inside handleExchangeReturnStoreCredit--End");
    }

  }

  /**
   * Handling ExchangeReturns as there will be only ChargeType="TRANSFER_OUT" and no PaymentMethod.
   *
   * @param inputDoc
   */
  public void handleExchangeReturn(YFCDocument inputDoc) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(CLASS_NAME + "Inside handleExchangeReturn--Begin");
      LOGGER.debug("inputDoc : " + inputDoc.toString());
    }
    YFCElement eleRootEle = inputDoc.getDocumentElement();
    YFCNodeList<YFCElement> yfcNLCollectionDetail = eleRootEle.getElementsByTagName("CollectionDetail");
    for (YFCElement eleCollectionDetail : yfcNLCollectionDetail) {
      String chargeType = eleCollectionDetail.getAttribute(GCConstants.CHARGE_TYPE);
      YFCElement elePaymentMethod = eleCollectionDetail.getChildElement(GCConstants.PAYMENT_METHOD);
      if (YFCUtils.equals(chargeType, "TRANSFER_OUT") && YFCCommon.isVoid(elePaymentMethod)) {
        YFCElement yfcElePaymentMethod = eleCollectionDetail.createChild(GCConstants.PAYMENT_METHOD);
        yfcElePaymentMethod.setAttribute(GCConstants.PAYMENT_TYPE, GCConstants.CREDIT_ON_ACCOUNT);
      }
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(CLASS_NAME + "Inside handleExchangeReturn--End");
    }
  }

  /**
   * This method copies the paypal transaction ID from sales order to return order
   *
   *
   * @param inDoc
   */
  private void copyPaypalTransactionIDForPOS(YFSEnvironment env, Document inDoc) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(CLASS_NAME + "Inside copyPaypalTransactionID");
    }
    // check if paypal is one of the refund payment methods
    final Element elePaymentMethod = GCXMLUtil.getElementByXPath(inDoc, GCConstants.PAYPAL_PAYMENT_XPATH);

    if (!YFCObject.isNull(elePaymentMethod)) {

      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(CLASS_NAME + "Atleast One Payment Method is Paypal");
      }

      final String strDerivedFromOrderHeaderKey =
          GCXMLUtil.getAttributeFromXPath(inDoc, GCConstants.DERIVED_FROM_ORDERHEADERKEY_XPATH);

      if (!YFCObject.isVoid(strDerivedFromOrderHeaderKey)) {

        final Element eleOutGCPaypalDistList = getGCPaypalRefundDistListForPOS(env, strDerivedFromOrderHeaderKey);

        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug(CLASS_NAME + "GCPaypalDistList Output Element"
              + GCXMLUtil.getXMLString(GCXMLUtil.getDocumentFromElement(eleOutGCPaypalDistList)));
        }

        Element eleOrderExtn = GCXMLUtil.getElementByXPath(inDoc, GCConstants.INVOICE_ORDER_EXTN);
        final Element eleInGCPaypalRefundDistList =
            GCXMLUtil.getFirstElementByName(eleOrderExtn, GCConstants.GC_PAYPAL_REFUNDDIST_LIST);
        eleOrderExtn.removeChild(eleInGCPaypalRefundDistList);
        final Element eleGCPaypal = (Element) inDoc.importNode(eleOutGCPaypalDistList, true);
        eleOrderExtn.appendChild(eleGCPaypal);

      }

    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(CLASS_NAME + "Output from copyPaypalTransactionID" + GCXMLUtil.getXMLString(inDoc));
    }


  }

  /**
   * @
   *
   * This method calls getGCPaypalRefundDistList API to fetch the Paypal TransactionID
   *
   * @param strDerivedFromOrderHeaderKey
   * @return
   */
  private Element getGCPaypalRefundDistListForPOS(YFSEnvironment env, String strDerivedFromOrderHeaderKey) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(CLASS_NAME + "Inside getGCPaypalRefundDistList");
    }
    final Document docInGCPaypalRefundDist = GCXMLUtil.createDocument(GCConstants.GC_PAYPAL_REFUND_DIST);
    Element eleGCPaypalRefundDist = docInGCPaypalRefundDist.getDocumentElement();
    eleGCPaypalRefundDist.setAttribute(GCConstants.ORDER_HEADER_KEY, strDerivedFromOrderHeaderKey);

    Document docOutGetGCPaypalDistList =
        GCCommonUtil.invokeService(env, GCConstants.GC_GET_PP_REFUND_DIST_LIST_SERVICE, docInGCPaypalRefundDist);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(CLASS_NAME + "Service Output" + GCXMLUtil.getXMLString(docOutGetGCPaypalDistList));
    }
    final Element eleOutGetGCPaypalDistList = docOutGetGCPaypalDistList.getDocumentElement();

    return eleOutGetGCPaypalDistList;
  }

  /**
   * This method is used to check whether the invoice contains any paymentMethod as STORE_CREDIT or
   * not.
   *
   * @param yfcInDoc
   * @return
   */
  private boolean checkForStoreCredit(YFCDocument yfcInDoc) {
    LOGGER.verbose("Class: GCPublishReturnInvoice Method: checkForStoreCredit BEGIN");
    LOGGER.beginTimer("GCPublishReturnInvoice.checkForStoreCredit");

    YFCElement eleInvcDtl = yfcInDoc.getDocumentElement();
    YFCElement eleInvcHeader = eleInvcDtl.getChildElement(GCConstants.INVOICE_HEADER);
    YFCElement eleCollectionDtls = eleInvcHeader.getChildElement("CollectionDetails");
    YFCNodeList<YFCElement> nlCollectionDtl = eleCollectionDtls.getElementsByTagName("CollectionDetail");
    boolean isStoreCredit = false;
    for (YFCElement eleCollectionDtl : nlCollectionDtl) {
      YFCElement elePaymentMethod = eleCollectionDtl.getChildElement(GCConstants.PAYMENT_METHOD);
      if (!YFCCommon.isVoid(elePaymentMethod)) {
        String paymentType = elePaymentMethod.getAttribute(GCConstants.PAYMENT_TYPE);
        LOGGER.verbose("paymentType : " + paymentType);
        if (YFCUtils.equals("STORE_CREDIT", paymentType)) {
          isStoreCredit = true;
          break;
        }
      }
    }
    LOGGER.verbose("isStoreCredit : " + isStoreCredit);
    LOGGER.endTimer("GCPublishReturnInvoice.checkForStoreCredit");
    LOGGER.verbose("Class: GCPublishReturnInvoice Method: checkForStoreCredit END");
    return isStoreCredit;
  }

  /**
   *
   * This method will post in invoice details in queue for processing of retry count and throw error
   * to roll back the transactions.
   *
   * @param env
   * @param inDoc
   * @param exception
   *
   */
  public static void postMessageAndRollbackTransaction(YFSEnvironment env, Document inDoc, Exception exception){

    LOGGER.verbose("Webservice request to POS is failed. Posting the invoice detail in queue by calling GCProcessPOSResponse service and thrwoing exception to fail the invoice posting process");
    GCCommonUtil.invokeService(env, GCConstants.GC_PROCESS_POS_RESPONSE_SERVICE, inDoc);
    YFCElement eleInvoiceHeader = YFCDocument.getDocumentFor(inDoc).getDocumentElement().getChildElement(GCConstants.INVOICE_HEADER);
    YFCElement eleInvoiceHeaderExtn = eleInvoiceHeader.getChildElement(GCConstants.EXTN);
    String sExtnPOSRetryCount = eleInvoiceHeaderExtn.getAttribute(GCXmlLiterals.EXTN_POS_RETRY_COUNT);
    LOGGER.verbose("POSRetryCount" + sExtnPOSRetryCount);
    if(YFCCommon.isVoid(sExtnPOSRetryCount)){
      sExtnPOSRetryCount = GCConstants.INT_ONE;
    } else{
      sExtnPOSRetryCount = String.valueOf(Integer.parseInt(sExtnPOSRetryCount) + 1);
    }
    if(Integer.parseInt(GCConstants.GC_POS_CONNECT_MAX_RETRY_COUNT) > Integer.parseInt(sExtnPOSRetryCount)){
      YFCException e1 = new YFCException(exception);
      if(YFCCommon.equals(e1.getMessage(), null)){
        e1.setAttribute("ErrorCode", "No response received from Webservice call");
        e1.setAttribute("ErrorDescription", "Response received from webservice call is either null or does not contained required attribute value.");
      }
      throw e1;
    }
  }

  /**
   * This method will fetch the manager id and user id from user activity information where User
   * action is 'ConfirmReturn'
   *
   * @param yfcEleRootInDoc
   * @param yfcEleInvoiceHeader
   * @return
   */
  public static void getManagerCashierApproveId(YFCElement yfcEleRootInDoc, YFCElement yfcEleInvoiceHeader) {
    LOGGER.beginTimer("GCPublishReturnInvoice.getManagerCashierApproveId()");
    LOGGER.verbose("GCPublishReturnInvoice.getManagerCashierApproveId() : Start");

    String sManagerID = GCConstants.BLANK_STRING;
    String sUserID = GCConstants.BLANK_STRING;

    /* Fetch manager approve ID and Cashier ID for the shipment from custom table - Start */
    YFCNodeList<YFCElement> yfcNLGCUserActivity =
        yfcEleInvoiceHeader.getElementsByTagName(GCConstants.GC_USER_ACTIVITY);
    for (int j = 0; j < yfcNLGCUserActivity.getLength(); j++) {
      YFCElement yfcEleGCUserActivity = yfcNLGCUserActivity.item(j);
      String sUserActivityUserAction = yfcEleGCUserActivity.getAttribute(GCConstants.USER_ACTION);
      if (YFCCommon.equals("ConfirmReturn", sUserActivityUserAction)) {
        sManagerID = yfcEleGCUserActivity.getAttribute(GCConstants.MANAGER_ID);
        sUserID = yfcEleGCUserActivity.getAttribute(GCConstants.USER_ID);
        break;
      }
    }

    /* Fetch manager approve ID and Cashier ID for the invoice from custom table - End */
    /* Set Manager Id and Cashier ID for the invoice - Start */
    yfcEleRootInDoc.setAttribute(GCConstants.MANAGER_ID, sManagerID);
    yfcEleRootInDoc.setAttribute(GCConstants.USER_ID, sUserID);
    /* Set Manager Id and Cashier ID for the invoice - End */

    LOGGER.endTimer("GCPublishReturnInvoice.getManagerCashierApproveId()");
    LOGGER.verbose("GCPublishReturnInvoice.getManagerCashierApproveId() : End");
  }

  /**
   * Method to get and stamp Order and current system date & time
   *
   * @param yfcEleRootInDoc
   * @return
   */
  public static void getAndStampDates(YFCElement yfcEleRootInDoc) {
    LOGGER.beginTimer("GCPublishReturnInvoice.getAndStampDates()");
    LOGGER.verbose("GCPublishReturnInvoice - getAndStampDates() : Start");

    YFCElement yfcEleOrder = yfcEleRootInDoc.getElementsByTagName(GCConstants.ORDER).item(0);
    Date dOrderDateTime = yfcEleOrder.getDateAttribute(GCConstants.ORDER_DATE);
    GCDateUtils.getDateTimeForPOS(yfcEleRootInDoc, dOrderDateTime);

    LOGGER.endTimer("GCPublishReturnInvoice.getAndStampDates()");
    LOGGER.verbose("GCPublishReturnInvoice - getAndStampDates() : End");

  }

  public static void removeBundleParentLineAndProrateComponentPrices(Document inDoc){

    LOGGER.beginTimer("GCPublishReturnInvoice.removeBundleParentLineAndProrateComponentPrices()");
    LOGGER.verbose("GCPublishReturnInvoice - prorateBundleCharges() : Start");
    if(LOGGER.isDebugEnabled()){
      LOGGER.verbose("Input Doc to method removeBundleParentLineAndProrateComponentPrices:-->" + YFCDocument.getDocumentFor(inDoc).toString());
    }

    List<Element> listBundleParentLineDetail =
        GCXMLUtil.getElementListByXpath(inDoc,
            "InvoiceDetail/InvoiceHeader/LineDetails/LineDetail/OrderLine[@KitCode='BUNDLE']/ItemDetails/Extn[@ExtnSetCode='1']/../../..");

    if (!listBundleParentLineDetail.isEmpty()) {

      Element eleLineDetails = (Element) inDoc.getElementsByTagName(GCConstants.LINE_DETAILS).item(0);
      Iterator<Element> iteratorListBundleLineDetail = listBundleParentLineDetail.iterator();
      while (iteratorListBundleLineDetail.hasNext()) {
        Element eleBundleParentLineDetail = iteratorListBundleLineDetail.next();
        Element eleOrderBundleParentLine =
            (Element) eleBundleParentLineDetail.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
        String sOrderLineKey = eleOrderBundleParentLine.getAttribute(GCConstants.ORDER_LINE_KEY);
        Element eleBundleItem =
            (Element) eleOrderBundleParentLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
        Element eleBundleItemExtn = (Element) eleBundleItem.getElementsByTagName(GCConstants.EXTN).item(0);
        String sPrimaryComponent = eleBundleItemExtn.getAttribute(GCXmlLiterals.EXTN_PRIMARY_COMPONENT_ITEM);


        //Fetching BundleParent Line Prices and ShippingRefund Element if exist.

        NodeList nlBundleParentLineCharges = eleBundleParentLineDetail.getElementsByTagName(GCConstants.LINE_CHARGE);
        Element eleShippingChargeCategory = null;

        for(int i= 0 ; i < nlBundleParentLineCharges.getLength() ; i++){
          Element eleBundleParentLineCharge = (Element) nlBundleParentLineCharges.item(i);
          String sChargeCategory = eleBundleParentLineCharge.getAttribute(GCConstants.CHARGE_CATEGORY);
          if(YFCCommon.equals(sChargeCategory, GCConstants.SHIPPING_REFUND)){
            eleShippingChargeCategory = eleBundleParentLineCharge;
            break;
          }
        }


        //Fetching ShippingRefund tax Element if exist.

        NodeList nlBundleParentLineTaxes = eleBundleParentLineDetail.getElementsByTagName(GCConstants.LINE_TAX);
        Element eleShippingTaxCategory = null;
        Element eleBundleLinePriceTax = null;
        for(int i= 0 ; i < nlBundleParentLineTaxes.getLength() ; i++){
          Element eleBundleParentLineTax = (Element) nlBundleParentLineTaxes.item(i);
          String sChargeCategory = eleBundleParentLineTax.getAttribute(GCConstants.CHARGE_CATEGORY);
          String sTaxName = eleBundleParentLineTax.getAttribute(GCConstants.TAX_NAME);
          if(YFCCommon.equals(sChargeCategory, GCConstants.SHIPPING_REFUND) && YFCCommon.equals(sTaxName, GCConstants.SHIPPING_REFUND_TAX)){
            eleShippingTaxCategory = eleBundleParentLineTax;
          } else if(YFCCommon.equals(sChargeCategory, GCConstants.LINE_PRICE)  && YFCCommon.equals(sTaxName, GCConstants.LINE_PRICE_TAX)){
            eleBundleLinePriceTax = eleBundleParentLineTax;
          }
        }

        // Fetching any CustomerAppeasement Charge and Taxes which exist on Bundle Parent Line
        String sAppeasementCharge = GCXMLUtil.getAttributeFromXPath(eleBundleParentLineDetail,"LineCharges/LineCharge[@ChargeCategory='CUSTOMER_APPEASEMENT' and @ChargeName='CUSTOMER_APPEASEMENT']/@ChargePerUnit");
        String sAppeasementTax = GCXMLUtil.getAttributeFromXPath(eleBundleParentLineDetail,"LineTaxes/LineTax[@ChargeCategory='CUSTOMER_APPEASEMENT' and @TaxName='CUSTOMER_APPEASEMENTTax']/@Tax");
        Double dAppeasementCharge = 0.00;
        Double dAppeasementTax = 0.00;
        if(!YFCCommon.isVoid(sAppeasementCharge)){
          dAppeasementCharge = Double.parseDouble(sAppeasementCharge);
        }//GCSTORE-6649:Start
        else{
          sAppeasementCharge = GCXMLUtil.getAttributeFromXPath(eleBundleParentLineDetail,"LineCharges/LineCharge[@ChargeCategory='CUSTOMER_APPEASEMENT' and @ChargeName='CUSTOMER_APPEASEMENT']/@ChargePerLine");
          if(LOGGER.isDebugEnabled()){
              LOGGER.verbose("sAppeasementCharge for ChargePerLine:-->" +sAppeasementCharge);
            }
          if(!YFCCommon.isVoid(sAppeasementCharge)){
        	  BigDecimal bigAppesementCharge = new BigDecimal(sAppeasementCharge);
              BigDecimal bigROUAppesementCharge = bigAppesementCharge.setScale(2, BigDecimal.ROUND_HALF_DOWN);
              dAppeasementCharge = bigROUAppesementCharge.doubleValue();
              if(LOGGER.isDebugEnabled()){
                  LOGGER.verbose("Final dAppeasementCharge:-->" +dAppeasementCharge);
              }
          }
        }//GCSTORE-6649:End
        if(!YFCCommon.isVoid(sAppeasementTax)){
          dAppeasementTax = Double.parseDouble(sAppeasementTax);
        }
        // End

        // Fix for GCSTORE-5471.
        // Find Mis. Charges and Taxes on OrderLine

        String sMiscCharge =
            GCXMLUtil.getAttributeFromXPath(eleBundleParentLineDetail,
                "LineCharges/LineCharge[@ChargeCategory='MiscellaneousCharges']/@ChargeAmount");
        String sMiscTax =
            GCXMLUtil.getAttributeFromXPath(eleBundleParentLineDetail,
                "LineTaxes/LineTax[@ChargeCategory='MiscellaneousCharges']/@Tax");
        Double dMiscCharge = 0.00;
        Double dMiscTax = 0.00;
        if (!YFCCommon.isVoid(sMiscCharge)) {
          dMiscCharge = -Double.parseDouble(sMiscCharge);
        }
        if (!YFCCommon.isVoid(sMiscTax)) {
          dMiscTax = -Double.parseDouble(sMiscTax);
        }

        Double dBundleParentLinePriceTax = 0.00;
        if(!YFCCommon.isVoid(eleBundleLinePriceTax)){
          String sBundleParentLinePriceTax = eleBundleLinePriceTax.getAttribute(GCConstants.TAX);
          dBundleParentLinePriceTax = Double.parseDouble(sBundleParentLinePriceTax);
        }

        Element elePrimaryComponentLine =  null;

        List<Element> listChildLineDetail =
            GCXMLUtil.getElementListByXpath(inDoc,
                "InvoiceDetail/InvoiceHeader/LineDetails/LineDetail/OrderLine[@BundleParentOrderLineKey='"
                    + sOrderLineKey + "']/..");
      //GCSTORE-6872 START
        Double dRemainingBundlePriceTax = -dBundleParentLinePriceTax;
        //GCSTORE-6872 END
        
        for (Element eleChildLineDetail : listChildLineDetail) {

          Element eleChildOrderLine = (Element) eleChildLineDetail.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
          Element eleChildItem = (Element) eleChildOrderLine.getElementsByTagName(GCConstants.ITEM).item(0);
          String sItemID = eleChildItem.getAttribute(GCConstants.ITEM_ID);

          if(YFCCommon.equals(sItemID, sPrimaryComponent)){
            elePrimaryComponentLine = eleChildLineDetail;
          } else {
            //Processing of Secondary Components

            NodeList nlGCOrderLineComponentsList =  eleChildLineDetail.getElementsByTagName(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
            String sBundleParentUnitPrice = null;
            String sUnitPrice = "";
            String sTaxes ="";
            String sQuantity ="";
            for(int i= 0 ; i < nlGCOrderLineComponentsList.getLength() ; i++){
              Element eleGCOrderLineComponents = (Element) nlGCOrderLineComponentsList.item(i);
              String sChargeCategory = eleGCOrderLineComponents.getAttribute(GCConstants.CHARGE_CATEGORY);
              String sChargeName = eleGCOrderLineComponents.getAttribute(GCConstants.CHARGE_NAME);
              if(YFCCommon.equals(sChargeCategory, "BundlePrice")){
                sBundleParentUnitPrice =  eleGCOrderLineComponents.getAttribute(GCXmlLiterals.AMOUNT);
              } else if (YFCCommon.equals(sChargeCategory, GCConstants.UNIT_PRICE)){
                sUnitPrice = eleGCOrderLineComponents.getAttribute(GCXmlLiterals.AMOUNT);
              } else if(YFCCommon.equals(sChargeName, GCConstants.LINE_PRICE_TAX)){
                sTaxes = eleGCOrderLineComponents.getAttribute(GCXmlLiterals.AMOUNT);
                sQuantity = eleGCOrderLineComponents.getAttribute("Quantity");
              }
            }

            // Fix to handle zero tax : GCSTORE-5319
            Double dNetProratedTax = 0.0;
            if (!YFCCommon.isVoid(sTaxes) && !YFCCommon.isVoid(sQuantity)) {
              dNetProratedTax = Double.parseDouble(sTaxes) / Double.parseDouble(sQuantity);
            }
            BigDecimal bdNetProratedTax = GCCommonUtil.formatNumberForDecimalPrecision(dNetProratedTax, 2);
          //GCSTORE-6872 START
            dRemainingBundlePriceTax = dRemainingBundlePriceTax - dNetProratedTax ;
            //GCSTORE-6872 END
            Element eleOrderLine = (Element) eleChildLineDetail.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
            eleOrderLine.setAttribute("FinalLineTax", String.valueOf(bdNetProratedTax));
            BigDecimal bdUnitPrice = GCCommonUtil.formatNumberForDecimalPrecision(Double.parseDouble(sUnitPrice), 2);
            eleChildLineDetail.setAttribute(GCConstants.UNIT_PRICE, bdUnitPrice.toString());
          }
        }
        // Processing of PrimaryComponentLine

        NodeList nlGCOrderLineComponentsList =  elePrimaryComponentLine.getElementsByTagName(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
        String sBundleParentUnitPrice = null;
        String sUnitPrice ="";
        String sTaxes ="";
        String sQuantity ="";
        for(int i= 0 ; i < nlGCOrderLineComponentsList.getLength() ; i++){
          Element eleGCOrderLineComponents = (Element) nlGCOrderLineComponentsList.item(i);
          String sChargeCategory = eleGCOrderLineComponents.getAttribute(GCConstants.CHARGE_CATEGORY);
          String sChargeName = eleGCOrderLineComponents.getAttribute(GCConstants.CHARGE_NAME);
          if(YFCCommon.equals(sChargeCategory, "BundlePrice")){
            sBundleParentUnitPrice =  eleGCOrderLineComponents.getAttribute(GCXmlLiterals.AMOUNT);
          } else if (YFCCommon.equals(sChargeCategory, GCConstants.UNIT_PRICE)){
            sUnitPrice = eleGCOrderLineComponents.getAttribute(GCXmlLiterals.AMOUNT);
          } else if(YFCCommon.equals(sChargeName, GCConstants.LINE_PRICE_TAX)){
            sTaxes = eleGCOrderLineComponents.getAttribute(GCXmlLiterals.AMOUNT);
            sQuantity = eleGCOrderLineComponents.getAttribute("Quantity");
          }
        }

        // Fix to handle zero tax : GCSTORE-5319
        Double dNetProratedTax = 0.0;
        if (!YFCCommon.isVoid(sTaxes) && !YFCCommon.isVoid(sQuantity)) {
          // Fix for 5653 : Putting '-' before tax and unit prices for correct collection
          dNetProratedTax = -Double.parseDouble(sTaxes) / Double.parseDouble(sQuantity);
        }
        Double dFinalLineTax = dNetProratedTax - dAppeasementTax - dMiscTax;
      
      //GCSTORE-6872 START
        if(dRemainingBundlePriceTax != -dFinalLineTax){
        	dFinalLineTax = -dRemainingBundlePriceTax;
			LOGGER.debug("dFinalLineTax: " + dFinalLineTax);
        }
	 /*Above fix for GCSTORE-6872 has resulted in a regression issue where appeasement tax and misc tax was not getting subtracted 
         * from line price tax. So added below check so that final tax should subtract these two values*/
        if(dAppeasementTax !=0.0 || dMiscTax!=0.0)
        {
        	dFinalLineTax = dNetProratedTax - dAppeasementTax - dMiscTax;
			LOGGER.debug("dFinalLineTax Updated: " + dFinalLineTax);
        }

        //GCSTORE-6872 END
        BigDecimal bdFinalLineTax = GCCommonUtil.formatNumberForDecimalPrecision(dFinalLineTax, 2);
        Element eleOrderLine = (Element) elePrimaryComponentLine.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
        eleOrderLine.setAttribute("FinalLineTax", bdFinalLineTax.toString());

        // Fix for 5653 : Putting '-' before tax and unit prices for correct collection
        Double dUnitPrice = -Double.parseDouble(sUnitPrice) - dAppeasementCharge - dMiscCharge;
        BigDecimal bdUnitPrice = GCCommonUtil.formatNumberForDecimalPrecision(dUnitPrice, 2);
        elePrimaryComponentLine.setAttribute(GCConstants.UNIT_PRICE, bdUnitPrice.toString());

        // Appending Shipping Charge and Tax on Primary ComponentOrderLine if present on Bundle Parent Line.
        if(!YFCCommon.isVoid(eleShippingChargeCategory)){
          // Fix for GCSTORE-5275 : Stamp FinalShippingAmount and FinalShippingTax on orderline
          // instead of LineDetail
          eleOrderLine.setAttribute("FinalShippingAmount",
              eleShippingChargeCategory.getAttribute(GCConstants.CHARGE_AMOUNT));
            if( !YFCCommon.isVoid(eleShippingTaxCategory)){
          eleOrderLine.setAttribute("FinalShippingTax", eleShippingTaxCategory.getAttribute(GCConstants.TAX));
        }
          }

        /* Append Primary Component price, charges and taxes - End */
        // Remove bundle parent line
        eleLineDetails.removeChild(eleBundleParentLineDetail);
      }
    }

    // Processing of Regular Items

    NodeList listRegularLineDetail = inDoc.getElementsByTagName(GCConstants.LINE_DETAIL);

    for (int i= 0 ; i < listRegularLineDetail.getLength() ; i++) {
      Element eleLineDetail = (Element) listRegularLineDetail.item(i);

      Element eleChildLineDetail = (Element) eleLineDetail.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
      String sBundleParentOrderLineKey = eleChildLineDetail.getAttribute(GCConstants.BUNDLE_PARENT_ORDERLINEKEY);
      String sKitCode = eleChildLineDetail.getAttribute(GCConstants.KIT_CODE);

      if(YFCCommon.isVoid(sBundleParentOrderLineKey) && YFCCommon.isVoid(sKitCode)){

        // Find Appeasement Charge and taxes present on orderline
        String sAppeasementCharge = GCXMLUtil.getAttributeFromXPath(eleLineDetail,"LineCharges/LineCharge[@ChargeCategory='CUSTOMER_APPEASEMENT' and @ChargeName='CUSTOMER_APPEASEMENT']/@ChargePerUnit");
        String sAppeasementTax = GCXMLUtil.getAttributeFromXPath(eleLineDetail,"LineTaxes/LineTax[@ChargeCategory='CUSTOMER_APPEASEMENT' and @TaxName='CUSTOMER_APPEASEMENTTax']/@Tax");
        Double dAppeasementCharge = 0.00;
        Double dAppeasementTax = 0.00;
        if(!YFCCommon.isVoid(sAppeasementCharge)){
          dAppeasementCharge = Double.parseDouble(sAppeasementCharge);
        }//GCSTORE-6649:Start
        else{
            sAppeasementCharge = GCXMLUtil.getAttributeFromXPath(eleLineDetail,"LineCharges/LineCharge[@ChargeCategory='CUSTOMER_APPEASEMENT' and @ChargeName='CUSTOMER_APPEASEMENT']/@ChargePerLine");
            if(LOGGER.isDebugEnabled()){
                LOGGER.verbose("sAppeasementCharge for ChargePerLine:-->" +sAppeasementCharge);
            }
            if(!YFCCommon.isVoid(sAppeasementCharge)){
          	  BigDecimal bigAppesementCharge = new BigDecimal(sAppeasementCharge);
              BigDecimal bigROUAppesementCharge = bigAppesementCharge.setScale(2, BigDecimal.ROUND_HALF_DOWN);
              dAppeasementCharge = bigROUAppesementCharge.doubleValue();
                if(LOGGER.isDebugEnabled()){
                    LOGGER.verbose("dAppeasementCharge for ChargePerLine:-->" +dAppeasementCharge);
                }
            }
        }//GCSTORE-6649:End
        if(!YFCCommon.isVoid(sAppeasementTax)){
          dAppeasementTax = Double.parseDouble(sAppeasementTax);
        }

        // Fix for GCSTORE-5471.
        // Find Mis. Charges and Taxes on OrderLine

        String sMiscCharge =
            GCXMLUtil.getAttributeFromXPath(eleLineDetail,
                "LineCharges/LineCharge[@ChargeCategory='MiscellaneousCharges']/@ChargeAmount");
        String sMiscTax =
            GCXMLUtil.getAttributeFromXPath(eleLineDetail,
                "LineTaxes/LineTax[@ChargeCategory='MiscellaneousCharges']/@Tax");
        Double dMiscCharge = 0.00;
        Double dMiscTax = 0.00;
        if (!YFCCommon.isVoid(sMiscCharge)) {
          dMiscCharge = Double.parseDouble(sMiscCharge);
        }
        if (!YFCCommon.isVoid(sMiscTax)) {
          dMiscTax = Double.parseDouble(sMiscTax);
        }



        String sUnitPrice = eleLineDetail.getAttribute(GCConstants.UNIT_PRICE);
        Double dUpdatedUnitPrice = Double.parseDouble(sUnitPrice) - dAppeasementCharge - dMiscCharge;
        BigDecimal bdUpdatedUnitPrice = GCCommonUtil.formatNumberForDecimalPrecision(dUpdatedUnitPrice, 2);
        eleLineDetail.setAttribute(GCConstants.UNIT_PRICE, String.valueOf(bdUpdatedUnitPrice));

        // Fix for 4429 : For Regular products check if shipping refund is present. Stamp amount on OrderLine/@FinalShippingAmount

        NodeList nlLineCharges = eleLineDetail.getElementsByTagName(GCConstants.LINE_CHARGE);
        for(int counter= 0 ; counter < nlLineCharges.getLength() ; counter++){
          Element eleLineCharge = (Element) nlLineCharges.item(counter);
          String sChargeCategory = eleLineCharge.getAttribute(GCConstants.CHARGE_CATEGORY);
          if(YFCCommon.equals(sChargeCategory, GCConstants.SHIPPING_REFUND)){
            String sShippingChargeAmount = eleLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT);
            Double dFinalLineShippingCharge = Double.parseDouble(sShippingChargeAmount);
            BigDecimal bdFinalLineShippingCharge = GCCommonUtil.formatNumberForDecimalPrecision(dFinalLineShippingCharge, 2);
            eleChildLineDetail.setAttribute("FinalShippingAmount", String.valueOf(bdFinalLineShippingCharge));
            break;
          }
        }

        NodeList nlRegularLineTaxes = eleLineDetail.getElementsByTagName(GCConstants.LINE_TAX);
        String sLineTax = null;
        String sShippingTax = null;
        for(int j= 0 ; j < nlRegularLineTaxes.getLength() ; j++){
          Element eleRegularLineTax = (Element) nlRegularLineTaxes.item(j);
          String sTaxName = eleRegularLineTax.getAttribute(GCConstants.TAX_NAME);
          if(YFCCommon.equals(sTaxName, GCConstants.LINE_PRICE_TAX)){
            sLineTax = eleRegularLineTax.getAttribute(GCConstants.TAX);
            if(!YFCCommon.isVoid(sLineTax)){
              Double dFinalLineTax = Double.parseDouble(sLineTax) - dAppeasementTax - dMiscTax;
              BigDecimal bdFinalLineTax = GCCommonUtil.formatNumberForDecimalPrecision(dFinalLineTax, 2);
              eleChildLineDetail.setAttribute("FinalLineTax", String.valueOf(bdFinalLineTax));
            }
          } else if (YFCCommon.equals(sTaxName, GCConstants.SHIPPING_REFUND_TAX)){
            sShippingTax = eleRegularLineTax.getAttribute(GCConstants.TAX);
            if(!YFCCommon.isVoid(sShippingTax)){
              Double dFinalShippingTax = Double.parseDouble(sShippingTax);
              BigDecimal bdFinalShippingTax = GCCommonUtil.formatNumberForDecimalPrecision(dFinalShippingTax, 2);
              eleChildLineDetail.setAttribute("FinalShippingTax", String.valueOf(bdFinalShippingTax));
            }
          }
        }
      }

    }
    if(LOGGER.isDebugEnabled()){
      LOGGER.verbose("Output Doc from method removeBundleParentLineAndProrateComponentPrices:-->" + YFCDocument.getDocumentFor(inDoc).toString());
    }

    LOGGER.endTimer("GCPublishReturnInvoice.prorateBundleCharges()");
    LOGGER.verbose("GCPublishReturnInvoice - prorateBundleCharges() : End");
  }

  public static void encryptDecryptCCForPOS(YFCDocument yfcDocPOSInput) throws GCException {

    LOGGER.beginTimer("GCPublishReturnInvoice.encryptDecryptCCForPOS()");
    LOGGER.verbose("GCPublishReturnInvoice - encryptDecryptCCForPOS() : Start");
    if(LOGGER.isDebugEnabled()){
      LOGGER.verbose("Input Doc to method encryptDecryptCCForPOS:-->" + yfcDocPOSInput.toString());
    }

    YFCNodeList<YFCElement> nlPayment = yfcDocPOSInput.getElementsByTagName("Payment");
    for(int i=0;i<nlPayment.getLength();i++){
      YFCElement elePayment = nlPayment.item(i);
      YFCElement eleType = elePayment.getChildElement("Type");
      String sType =  eleType.getNodeValue();
      String[] sCCTypes = new String[] {
          "VI", "MC", "DI", "AX", "HS", "GC", "DB" };  //Added type DB for mPOS orders with debit card.
      Set<String> sCCTypesSet = new HashSet<String>(Arrays
          .asList(sCCTypes));
      if(!YFCCommon.isVoid(eleType) && (sCCTypesSet.contains(sType))){
        YFCElement eleIxAccount = elePayment.getChildElement("IxAccount");
        String sIxAccount = eleIxAccount.getNodeValue();
        String sDecryptedIxAccount = GCPaymentSecurity.decryptCreditCard(sIxAccount, "OMS");
        try{

          // Processing POS public key : Start
          InputStream inputStream = GCPublishReturnInvoice.class.getResourceAsStream(YFSSystem.getProperty("POS_ENCRYPTION_KEY_PATH"));
          String keyString = IOUtils.toString(inputStream);
          Base64 b64 = new Base64();
          byte [] decoded = b64.decode(keyString);
          KeyFactory kf = KeyFactory.getInstance("RSA");
          RSAPublicKey pubKey = (RSAPublicKey) kf.generatePublic(new X509EncodedKeySpec(decoded));

          // Processing POS public key : End

          /* Create a cipher for encrypting. */
          Cipher encryptCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
          encryptCipher.init(Cipher.ENCRYPT_MODE, pubKey);

          byte[] ciphertext = encryptCipher.doFinal(sDecryptedIxAccount.getBytes());
          String sEncryptedIxAccount= Base64.encodeBase64String(ciphertext);
          LOGGER.info("Encrypted CC no. using POS key: " + sEncryptedIxAccount);
          eleIxAccount.setNodeValue(sEncryptedIxAccount);
        } catch(Exception e) {
          LOGGER.error("Inside GCPublishReturnInvoice.encryptDecryptCCForPOS():",e);
          throw new GCException(e);
        }
      }
    }

    LOGGER.endTimer("GCPublishReturnInvoice.encryptDecryptCCForPOS()");
    LOGGER.verbose("GCPublishReturnInvoice - encryptDecryptCCForPOS() : End");
  }


  /**
   * This method will club items with same components in the shipment
   *
   * @param inDoc
   */
  private Document clubOrderLinesWithSimilarComps(Document inDoc) {
    LOGGER.beginTimer("GCPOSInvoicing.clubOrderLinesWithSimilarComps()");
    LOGGER.verbose("GCPOSInvoicing - clubOrderLinesWithSimilarComps() : Start");

    List<Element> listLineDetail =
        GCXMLUtil.getElementListByXpath(inDoc, "InvoiceDetail/InvoiceHeader/LineDetails/LineDetail");
    Element eleLineDetails = (Element) inDoc.getElementsByTagName(GCConstants.LINE_DETAILS).item(0);
    Iterator<Element> iteratorListLineDetail = listLineDetail.iterator();
    List<String> listItemId = new ArrayList();

    while (iteratorListLineDetail.hasNext()) {
      Element eleLineDetail = iteratorListLineDetail.next();
      Element eleOrderLine = (Element) eleLineDetail.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
      Element eleOrderLineExtn = (Element) eleOrderLine.getElementsByTagName(GCConstants.EXTN).item(0);
      String sSKUNumber = eleOrderLineExtn.getAttribute(GCConstants.EXTN_POS_SKU_NO);
      String sOrderLineKey = eleLineDetail.getAttribute(GCConstants.ORDER_LINE_KEY);
      Double dItemQty = Double.parseDouble(eleLineDetail.getAttribute(GCConstants.QUANTITY));
      Double dFinalLineTax = Double.parseDouble(!YFCCommon.isVoid(eleOrderLine.getAttribute("FinalLineTax")) ? eleOrderLine.getAttribute("FinalLineTax") : "0.00");

      Map<String, Element> mapSameItemCharges = new HashMap<String, Element>();
      Map<String, Element> mapSameItemTax = new HashMap<String, Element>();

      if (listItemId.contains(sSKUNumber)) {
        // Remove Order line from document
        eleLineDetails.removeChild(eleLineDetail);
      } else {
        List<Element> listSameItemLineDetail =
            GCXMLUtil.getElementListByXpath(inDoc,
                "InvoiceDetail/InvoiceHeader/LineDetails/LineDetail[@OrderLineKey!='" + sOrderLineKey
                    + "']/OrderLine/Extn[@ExtnPOSSKUNumber='" + sSKUNumber + "' and @ExtnIsWarrantyItem='N']/../..");
        if (!listSameItemLineDetail.isEmpty()) {
          listItemId.add(sSKUNumber);
          Iterator<Element> iteratorSameItem = listSameItemLineDetail.iterator();
          while (iteratorSameItem.hasNext()) {
            Element eleSameItemLineDetail = iteratorSameItem.next();
            String sQty = eleSameItemLineDetail.getAttribute(GCConstants.QUANTITY);
            Element eleSameItemOrderLine = (Element) eleSameItemLineDetail.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
            String sFinalLineTax = eleSameItemOrderLine.getAttribute("FinalLineTax");
            dItemQty += Double.parseDouble(sQty);
            dFinalLineTax += Double.parseDouble(!YFCCommon.isVoid(sFinalLineTax) ? sFinalLineTax : "0.00");
            //Update qty for order line
            eleLineDetail.setAttribute(GCConstants.QUANTITY, dItemQty.toString());
            eleOrderLine.setAttribute("FinalLineTax", dFinalLineTax.toString());

          }
        }
      }
    }
    // End of for loop on line detail

    LOGGER.endTimer("GCPOSInvoicing.clubOrderLinesWithSimilarComps()");
    LOGGER.verbose("GCPOSInvoicing - clubOrderLinesWithSimilarComps() : End");
    return inDoc;
  }


}
