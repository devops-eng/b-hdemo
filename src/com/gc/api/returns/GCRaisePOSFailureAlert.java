/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE:
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0               05/05/2015        Singh, Gurpreet            GCSTORE-2410
 *#################################################################################################################################################################
 */
package com.gc.api.returns;

import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author gurpreet.singh
 *
 */
public class GCRaisePOSFailureAlert implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCRaisePOSFailureAlert.class);
  Properties prop = new Properties();

  /**
   * Creates alert for the POS communication failure in case of return order invoicing
   *
   * @param env
   * @param inDoc
   */
  public void raiseAlert(YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("GCRaisePOSFailureAlert.raiseAlert");
    LOGGER.verbose("Class: GCRaisePOSFailureAlert Method: raiseAlert START");

    String sExceptionType = prop.getProperty(GCConstants.EXCEPTION_TYPE);
    String sQueueID = prop.getProperty(GCConstants.QUEUE_ID);

    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" raiseAlert() method, yfcInDoc is" + yfcInDoc.toString());
    }
    YFCElement eleRootInDoc = yfcInDoc.getDocumentElement();
    YFCElement eleInvoiceHeader = eleRootInDoc.getFirstChildElement();
    YFCElement eleOrder = eleInvoiceHeader.getChildElement(GCConstants.ORDER);
    YFCDocument yfcCreateExceptionDoc = YFCDocument.createDocument(GCConstants.INBOX);
    YFCElement eleCreateExceptionDoc = yfcCreateExceptionDoc.getDocumentElement();
    eleCreateExceptionDoc.setAttribute(GCConstants.BILL_TO_ID, eleOrder.getAttribute(GCConstants.BILL_TO_ID));
    eleCreateExceptionDoc.setAttribute(GCConstants.ENTERPRISE_KEY,
        eleInvoiceHeader.getAttribute(GCConstants.ENTERPRISE_CODE));
    eleCreateExceptionDoc.setAttribute(GCConstants.EXCEPTION_TYPE, sExceptionType);
    eleCreateExceptionDoc.setAttribute(GCConstants.ORDER_NO, eleInvoiceHeader.getAttribute(GCConstants.ORDER_NO));

    eleCreateExceptionDoc.setAttribute(GCConstants.ORDER_HEADER_KEY,
        eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY));
    eleCreateExceptionDoc.setAttribute(GCConstants.PRIORITY, GCConstants.ONE);

    eleCreateExceptionDoc.setAttribute(GCConstants.QUEUE_ID, sQueueID);

    String sDescription = eleRootInDoc.getAttribute("POSError");
    if ((!YFCCommon.isVoid(sDescription)) && (sDescription.length() < 40)) {
      eleCreateExceptionDoc.setAttribute(GCConstants.DESCRIPTION, sDescription);
    }
    eleCreateExceptionDoc.setAttribute(GCXmlLiterals.DETAIL_DESCRIPTION, eleRootInDoc.getAttribute("POSError"));
    eleCreateExceptionDoc.setAttribute("AssignedToUserId", "admin");
    YFCElement eleExtn = eleCreateExceptionDoc.createChild(GCConstants.EXTN);
    String sStoreName = fetchStoreName(env, yfcInDoc, eleCreateExceptionDoc);
    LOGGER.verbose(" sStoreName " + sStoreName);
    eleExtn.setAttribute("ExtnStoreName", sStoreName);

    YFCNodeList<YFCElement> listGCUserActivity = yfcInDoc.getElementsByTagName(GCConstants.GC_SHIPMENT_ACTICITY);
    if (listGCUserActivity.getLength() > 0) {
      LOGGER.verbose(" If user activity list exists ");
      YFCElement eleGCUserActivity = listGCUserActivity.item(0);
      YFCElement eleInboxReferencesList = eleCreateExceptionDoc.createChild(GCConstants.INBOX_REFERENCE_LIST);
      YFCElement eleInboxReferences = eleInboxReferencesList.createChild(GCConstants.INBOX_REFERENCES);
      eleInboxReferences.setAttribute(GCConstants.NAME, GCConstants.USER_ID);
      eleInboxReferences.setAttribute(GCConstants.VALUE, eleGCUserActivity.getAttribute(GCConstants.USER_ID));
      eleInboxReferences.setAttribute(GCConstants.REFERENCE_TYPE, "Text");
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" raiseAlert() method, yfcCreateExceptionDoc is" + yfcCreateExceptionDoc.toString());
    }
    GCCommonUtil.invokeAPI(env, GCConstants.API_CREATE_EXCEPTION, yfcCreateExceptionDoc, null);
    LOGGER.verbose("Class: GCRaisePOSFailureAlert Method: raiseAlert END");
    LOGGER.endTimer("GCRaisePOSFailureAlert.raiseAlert");
  }

  /**
   * Fetch organization name for the organization code
   *
   * @param env
   * @param yfcInDoc
   * @param eleCreateExceptionDoc
   * @return
   */
  private String fetchStoreName(YFSEnvironment env, YFCDocument yfcInDoc, YFCElement eleCreateExceptionDoc) {
    LOGGER.beginTimer("GCRaisePOSFailureAlert.fetchStoreName");
    LOGGER.verbose("Class: GCRaisePOSFailureAlert Method: fetchStoreName START");
    YFCNodeList<YFCElement> listSchedule = yfcInDoc.getElementsByTagName(GCConstants.SCHEDULE);
    String sStoreName = null;
    if (listSchedule.getLength() > 0) {
      YFCElement eleSchedule = listSchedule.item(0);
      String sShipNode = eleSchedule.getAttribute(GCConstants.SHIP_NODE);
      eleCreateExceptionDoc.setAttribute(GCXmlLiterals.SHIP_NODE_KEY, sShipNode);

      YFCDocument yfcGetOrgHierarchy = YFCDocument.createDocument(GCConstants.ORGANIZATION);
      YFCElement eleGetOrgHierarchy = yfcGetOrgHierarchy.getDocumentElement();
      eleGetOrgHierarchy.setAttribute(GCConstants.ORGANIZATION_CODE, sShipNode);
      YFCDocument yfcGetOrgHierarchyTemp = YFCDocument.getDocumentFor("<Organization OrganizationName=''/>");
      YFCDocument yfcGetOrgHierarchyOut =
          GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORG_HIERARCHY, yfcGetOrgHierarchy, yfcGetOrgHierarchyTemp);
      sStoreName = yfcGetOrgHierarchyOut.getDocumentElement().getAttribute("OrganizationName");
    }
    LOGGER.verbose("Class: GCRaisePOSFailureAlert Method: fetchStoreName END");
    LOGGER.endTimer("GCRaisePOSFailureAlert.fetchStoreName");
    return sStoreName;
  }
  @Override
  public void setProperties(final Properties paramProperties) {
    prop = paramProperties;
  }

}
