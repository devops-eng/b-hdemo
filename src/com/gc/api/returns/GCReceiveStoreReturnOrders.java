/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Invoke change order status API to move order line to Received status
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               03/20/2015        Singh, Gurpreet            GCSTORE-556: Store Returns
 *#################################################################################################################################################################
 */
package com.gc.api.returns;

import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:gurpreet.singh@expicient.com">Gurpreet</a>
 */
public class GCReceiveStoreReturnOrders implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCReceiveStoreReturnOrders.class);

  /**
   *
   * Invoke changeOrderStatus API to move OrderLine status to Received status
   *
   * @param env
   * @param inputDoc
   * @return
   *
   */
  public Document receiveStoreReturnOrder(YFSEnvironment env, Document inputDoc) {
    LOGGER.beginTimer("GCReceiveStoreReturnOrders.receiveStoreReturnOrder");
    LOGGER.verbose("Class: GCReceiveStoreReturnOrders Method: receiveStoreReturnOrder START");
    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" receiveStoreReturnOrder() method, inDoc is" + inDoc.toString());
    }
    YFCElement eleOrder = inDoc.getDocumentElement();
    startReceipt(env, eleOrder);

    // Calling service to create multiple invoices on the basis of POSSalesTicketNo
    GCCommonUtil.invokeService(env, GCConstants.GC_CREATE_STORE_RETURN_INVOICE_SERVICE, inputDoc);
    // Calling service to move the return order lines to the appropriate status
    GCCommonUtil.invokeService(env, GCConstants.GC_PROCESS_STORE_RETURNS_SERVICE, inputDoc);
    LOGGER.verbose("Class: GCReceiveStoreReturnOrders Method: receiveStoreReturnOrder END");
    LOGGER.endTimer("GCReceiveStoreReturnOrders.receiveStoreReturnOrder");
    return inputDoc;
  }

  public void startReceipt(YFSEnvironment env, YFCElement eleOrder) {
    LOGGER.beginTimer("GCReceiveStoreReturnOrders.startReceipt");
    LOGGER.verbose("Class: GCReceiveStoreReturnOrders Method: startReceipt START");
    YFCElement eleOrderLine = eleOrder.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
    String sReceivingNode = eleOrderLine.getAttribute(GCConstants.SHIP_NODE);

    YFCDocument docReceipt = YFCDocument.createDocument(GCConstants.RECEIPT);
    YFCElement eleReceipt = docReceipt.getDocumentElement();
    eleReceipt.setAttribute(GCConstants.RECEIVING_NODE, sReceivingNode);
    eleReceipt.setAttribute(GCConstants.DOCUMENT_TYPE, GCConstants.RETURN_ORDER_DOCUMENT_TYPE);

    YFCElement eleShipment = docReceipt.createElement(GCConstants.SHIPMENT);
    eleShipment.setAttribute(GCConstants.RECEIVING_NODE, sReceivingNode);
    eleShipment.setAttribute(GCConstants.DOCUMENT_TYPE, GCConstants.RETURN_ORDER_DOCUMENT_TYPE);
    eleShipment.setAttribute(GCConstants.SELLER_ORGANIZATION_CODE,
        eleOrder.getAttribute(GCConstants.SELLER_ORGANIZATION_CODE));
    eleShipment.setAttribute(GCConstants.ORDER_NO, eleOrder.getAttribute(GCConstants.ORDER_NO));
    eleShipment.setAttribute(GCConstants.ORDER_HEADER_KEY, eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY));
    eleReceipt.appendChild(eleShipment);
    YFCDocument docOP = GCCommonUtil.invokeAPI(env, GCConstants.API_START_RECEIPT, docReceipt, null);

    String sReceiptHeaderKey = docOP.getDocumentElement().getAttribute(GCConstants.RECEIPT_HEADER_KEY);
    // Calling receive order api to receive the return order
    receiveReturnOrder(env, eleOrder, sReceiptHeaderKey, sReceivingNode);
    LOGGER.verbose("Class: GCReceiveStoreReturnOrders Method: receiveStoreReturnOrder END");
    LOGGER.endTimer("GCReceiveStoreReturnOrders.receiveStoreReturnOrder");
  }

  private void receiveReturnOrder(YFSEnvironment env, YFCElement eleOrder, String sReceiptHeaderKey,
      String sReceivingNode) {
    LOGGER.beginTimer("GCReceiveStoreReturnOrders.receiveReturnOrder");
    LOGGER.verbose("Class: GCReceiveStoreReturnOrders Method: receiveReturnOrder START");
    YFCDocument docReceiveOrder = YFCDocument.createDocument(GCConstants.RECEIPT);
    YFCElement eleReceiveOrder = docReceiveOrder.getDocumentElement();
    eleReceiveOrder.setAttribute(GCConstants.RECEIVING_NODE, sReceivingNode);
    eleReceiveOrder.setAttribute(GCConstants.DOCUMENT_TYPE, GCConstants.RETURN_ORDER_DOCUMENT_TYPE);
    eleReceiveOrder.setAttribute(GCConstants.RECEIPT_HEADER_KEY, sReceiptHeaderKey);

    YFCElement eleReceiptLines = docReceiveOrder.createElement(GCConstants.RECEIPT_LINES);
    eleReceiveOrder.appendChild(eleReceiptLines);

    YFCElement eleOrderLines = eleOrder.getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> nlOrderLines = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
    for (YFCElement eleOrderLine : nlOrderLines) {
      String sKitCode = eleOrderLine.getAttribute(GCConstants.KIT_CODE);
      if (!GCConstants.BUNDLE.equals(sKitCode)) {
        YFCElement eleReceiptLine = docReceiveOrder.createElement(GCConstants.RECEIPT_LINE);
        YFCElement eleItem = eleOrderLine.getChildElement(GCConstants.ITEM);
        eleReceiptLine.setAttribute(GCConstants.ITEM_ID, eleItem.getAttribute(GCConstants.ITEM_ID));
        eleReceiptLine.setAttribute(GCConstants.QUANTITY, eleOrderLine.getAttribute(GCConstants.ORDERED_QTY));
        eleReceiptLine.setAttribute(GCConstants.ORDER_LINE_KEY, eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
        eleReceiptLine.setAttribute(GCConstants.UOM, eleItem.getAttribute(GCConstants.UOM));
        eleReceiptLine.setAttribute(GCConstants.ENTERPRISE_CODE, eleOrder.getAttribute(GCConstants.ENTERPRISE_CODE));
        eleReceiptLines.appendChild(eleReceiptLine);
      }
    }
    GCCommonUtil.invokeAPI(env, GCConstants.API_RECEIVE_ORDER, docReceiveOrder, null);
    LOGGER.verbose("Class: GCReceiveStoreReturnOrders Method: receiveReturnOrder END");
    LOGGER.endTimer("GCReceiveStoreReturnOrders.receiveReturnOrder");
  }

  @Override
  public void setProperties(Properties arg0) {

  }

}
