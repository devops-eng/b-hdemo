package com.gc.api.returns;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCRemoveReturns {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCRemoveReturns.class.getName());


  public void changeOrderStatus(YFSEnvironment env, Document inputDoc) {
    LOGGER.beginTimer("GCRemoveReturns.changeOrderStatus");
    LOGGER.verbose("GCRemoveReturns.changeOrderStatus--Begin");

    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("inDoc Document: " + inDoc.toString());
    }
    YFCElement inDocRootEle = inDoc.getDocumentElement();
    String orderLineKey = inDocRootEle.getAttribute(GCConstants.ORDER_LINE_KEY);
    YFCElement eleRootOrder = inDocRootEle.getChildElement(GCConstants.ORDER);
    String enterpriseCode = eleRootOrder.getAttribute(GCConstants.ENTERPRISE_CODE);


    YFCDocument getOrderLineListInDoc = YFCDocument.createDocument(GCConstants.ORDER_LINE);
    YFCElement getOrderLineListInDocRootEle = getOrderLineListInDoc.getDocumentElement();
    getOrderLineListInDocRootEle.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey);

    YFCDocument getOrderLineListTempDoc = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LINE_LIST_TEMPLATE_ONE);

    YFCDocument getOrderLineListOutDoc =
        GCCommonUtil
        .invokeAPI(env, GCConstants.API_GET_ORDER_LINE_LIST, getOrderLineListInDoc, getOrderLineListTempDoc);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getOrderLineListListOutdoc Document: " + getOrderLineListOutDoc.toString());
    }


    YFCElement getOrderLineListOutDocRootEle = getOrderLineListOutDoc.getDocumentElement();
    YFCElement eleOrderLine=getOrderLineListOutDocRootEle.getChildElement(GCConstants.ORDER_LINE);
    String fulfillmentType=eleOrderLine.getAttribute(GCConstants.FULFILLMENT_TYPE);
    YFCElement eleOrder=eleOrderLine.getChildElement(GCConstants.ORDER);
    YFCElement eleExtn=eleOrder.getChildElement(GCConstants.EXTN);
    String extnPickupStoreID=eleExtn.getAttribute(GCConstants.EXTN_PICKUP_STORE_ID);
    String extnPurchaseOrderNo = eleExtn.getAttribute(GCConstants.EXTN_PURCHASE_ORDER_NUMBERS);
    YFCElement eleOrderStatusesOrderLine = eleOrderLine.getChildElement(GCConstants.ORDER_STATUSES);
    YFCElement eleOrderStatusOrderLine = eleOrderStatusesOrderLine.getChildElement(GCConstants.ORDER_STATUS);
    String strShipNode = eleOrderLine.getAttribute(GCConstants.SHIP_NODE);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("fulfillmentType: " + fulfillmentType);
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("extnPickupStoreID: " + extnPickupStoreID);
    }

    YFCDocument commonCodeOutDocOne =
        GCCommonUtil.getCommonCodeListByTypeAndValue(env, GCConstants.GC_EXSTORE_FLMNT_TYPE, enterpriseCode,
            fulfillmentType);
    YFCElement commonCodeOutDocOneRootEle = commonCodeOutDocOne.getDocumentElement();
    YFCElement eleCommonCode = commonCodeOutDocOneRootEle.getChildElement(GCConstants.COMMON_CODE);
    fulfillmentType = eleCommonCode.getAttribute(GCConstants.CODE_SHORT_DESCRIPTION);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("fulfillmentType: " + fulfillmentType);
    }

    String DocumentType = eleRootOrder.getAttribute(GCConstants.DOCUMENT_TYPE);
    String orderHeaderKey = eleRootOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);

    if (!YFCCommon.isVoid(extnPickupStoreID) || !YFCCommon.equals(GCConstants.MFI, strShipNode))
    {
      YFCNodeList<YFCElement> listOrderStatus =
          eleOrderStatusesOrderLine.getElementsByTagName(GCConstants.ORDER_STATUS);
      for (YFCElement eleOrderStatus : listOrderStatus) {
        String status = eleOrderStatus.getAttribute(GCConstants.STATUS);
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("status: " + status);
        }
        String statusQty = eleOrderStatus.getAttribute(GCConstants.STATUS_QTY);
        String orderReleaseKey = eleOrderStatus.getAttribute(GCConstants.ORDER_RELEASE_KEY);
        if (YFCCommon.equals(GCConstants.SHIPPED_STATUS, status)
            && (!YFCCommon.isVoid(orderReleaseKey) || !YFCCommon.isVoid(extnPurchaseOrderNo))) {
          if (YFCCommon.equals(GCConstants.PICKUP_IN_STORE, fulfillmentType)) {
            YFCDocument chngOrdStatusIndoc = YFCDocument.createDocument(GCConstants.ORDER_STATUS_CHANGE);
            YFCElement ordStatusChngEle = chngOrdStatusIndoc.getDocumentElement();
            ordStatusChngEle.setAttribute(GCConstants.DOCUMENT_TYPE, DocumentType);
            ordStatusChngEle.setAttribute(GCConstants.ENTERPRISE_CODE, enterpriseCode);
            ordStatusChngEle.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
            ordStatusChngEle.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.CUSTOMER_PICKED_UP_ORD_TRANID);
            YFCElement eleOrderLinesStatus = ordStatusChngEle.createChild(GCConstants.ORDER_LINES);
            YFCElement eleOrderLineStatus = eleOrderLinesStatus.createChild(GCConstants.ORDER_LINE);
            eleOrderLineStatus.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey);
            eleOrderLineStatus.setAttribute(GCConstants.QUANTITY, statusQty);
            eleOrderLineStatus.setAttribute(GCConstants.ORDER_RELEASE_KEY, orderReleaseKey);
            eleOrderLineStatus
            .setAttribute(GCConstants.BASE_DROP_STATUS, GCConstants.CUSTOMER_PICKED_UP_ORDER_STATUS);
            if (LOGGER.isVerboseEnabled()) {
              LOGGER.verbose("chngOrdStatusIndoc Document For Pick up in Store: " + chngOrdStatusIndoc.toString());
            }

            GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER_STATUS, chngOrdStatusIndoc.getDocument());



          }

          else if (YFCCommon.equals(GCConstants.SHIP_2_CUSTOMER, fulfillmentType)) {
            YFCDocument chngOrdStatusIndoc = YFCDocument.createDocument(GCConstants.ORDER_STATUS_CHANGE);
            YFCElement ordStatusChngEle = chngOrdStatusIndoc.getDocumentElement();
            ordStatusChngEle.setAttribute(GCConstants.DOCUMENT_TYPE, DocumentType);
            ordStatusChngEle.setAttribute(GCConstants.ENTERPRISE_CODE, enterpriseCode);
            ordStatusChngEle.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
            ordStatusChngEle.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.SHIP_2_CUSTOMER_TRANID);
            YFCElement eleOrderLinesStatus = ordStatusChngEle.createChild(GCConstants.ORDER_LINES);
            YFCElement eleOrderLineStatus = eleOrderLinesStatus.createChild(GCConstants.ORDER_LINE);
            eleOrderLineStatus.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey);
            eleOrderLineStatus.setAttribute(GCConstants.QUANTITY, statusQty);
            eleOrderLineStatus.setAttribute(GCConstants.ORDER_RELEASE_KEY, orderReleaseKey);
            eleOrderLineStatus
            .setAttribute(GCConstants.BASE_DROP_STATUS, GCConstants.SHIPPED_TO_CUSTOMER_VAL);
            if (LOGGER.isVerboseEnabled()) {
              LOGGER.verbose("chngOrdStatusIndoc Document For Ship 2 customer: " + chngOrdStatusIndoc.toString());
            }
            GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER_STATUS, chngOrdStatusIndoc.getDocument());

          }

        }
      }

    }




  }
}