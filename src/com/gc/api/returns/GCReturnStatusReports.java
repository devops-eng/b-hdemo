/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 * ####################################################################################################################
 *	OBJECTIVE: Write what this class is about
 *######################################################################################################################
 *	Version     Date       Modified By              Description
 *######################################################################################################################
    1.0      16/04/2015   Mittal, Yashu		GCSTORE-858: Return Reports
 *######################################################################################################################
 */
package com.gc.api.returns;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:yashu.mittal@expicient.com">Yashu Mittal</a>
 */
public class GCReturnStatusReports {
	
	private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCReturnStatusReports.class.getName());
	
/**
 * 
 * This method will fetch all orderlines fulfilling the search criteria and return the same to UI.
 * 	
 * @param env
 * @param inDoc
 * @return
 */
	public Document getOrderLinesForReturnsReport(YFSEnvironment env, Document inDoc){
		
		LOGGER.beginTimer("GCReturnStatusReports.getOrderLinesForReturnsReport");
	    LOGGER.verbose("Class: GCReturnStatusReports Method: getOrderLinesForReturnsReport START");
	    
	    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
	    if(LOGGER.isDebugEnabled()){
	      LOGGER.verbose("Input doc to GCReturnStatusReports.getOrderLinesForReturnsReport method is:--> " + yfcInDoc.toString());
	    }
	    
		YFCDocument yfcDoc = YFCDocument.getDocumentFor(inDoc);
		YFCElement eleRoot = yfcDoc.getDocumentElement();
		String sStatus = eleRoot.getAttribute(GCConstants.STATUS);
		String sDispositionCode = eleRoot.getAttribute(GCXmlLiterals.DISPOSITION_CODE);
		String sFromOrderDate = eleRoot.getAttribute(GCConstants.FROM_ORDER_DATE);
		String sToOrderDate = eleRoot.getAttribute(GCConstants.TO_ORDER_DATE);
		
		YFCDocument getOrderLineListIn = YFCDocument.getDocumentFor("<OrderLine Status='" + sStatus +"' DispositionCode='" + sDispositionCode + "'/>");
		YFCElement elegetOrderLineListRoot = getOrderLineListIn.getDocumentElement();
		YFCElement eleComplexQuery = elegetOrderLineListRoot.createChild(GCConstants.COMPLEX_QUERY);
		eleComplexQuery.setAttribute(GCConstants.OPERATOR, "OR");
		
		YFCElement eleOr = eleComplexQuery.createChild(GCConstants.OR);
		
		YFCNodeList<YFCElement> nlShipNodeList = eleRoot.getElementsByTagName(GCConstants.SHIP_NODE);
		if(!YFCCommon.isVoid(nlShipNodeList)){
			for(YFCElement eleShipNode : nlShipNodeList){
				String sShipNode = eleShipNode.getAttribute(GCConstants.SHIP_NODE_KEY);
				YFCElement eleExp = eleOr.createChild(GCConstants.EXP);
				eleExp.setAttribute(GCConstants.NAME, GCConstants.RECEIVING_NODE);
				eleExp.setAttribute(GCConstants.QRY_TYPE, GCConstants.EQ);
				eleExp.setAttribute(GCConstants.VALUE, sShipNode);
			}
		}
		
		YFCElement eleOrder = elegetOrderLineListRoot.createChild(GCConstants.ORDER);
		eleOrder.setAttribute(GCConstants.ORDER_DATE_QRY_TYPE, GCConstants.DATERANGE);
		eleOrder.setAttribute(GCConstants.FROM_ORDER_DATE, sFromOrderDate);
		eleOrder.setAttribute(GCConstants.TO_ORDER_DATE, sToOrderDate);
		
		YFCDocument getOrderLineListOut = GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LINE_LIST, getOrderLineListIn, YFCDocument.getDocumentFor(GCConstants.FAILED_RETURN_TEMPLATE));
		
		if(LOGGER.isDebugEnabled()){
		      LOGGER.verbose("Output returned by GCReturnStatusReports.getOrderLinesForReturnsReport method is:--> " + getOrderLineListOut.toString());
		    }
		
		LOGGER.endTimer("GCReturnStatusReports.getOrderLinesForReturnsReport");
	    LOGGER.verbose("Class: GCReturnStatusReports Method: getOrderLinesForReturnsReport END");
		return getOrderLineListOut.getDocument();
		
	}

}
