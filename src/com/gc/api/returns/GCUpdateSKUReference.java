/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 * ####################################################################################################################
 *	OBJECTIVE: Write what this class is about
 *######################################################################################################################
 *	Version     Date       Modified By              Description
 *######################################################################################################################
    1.0      10/03/2015   Mittal, Yashu     GCSTORE-516: Design - DC Returns
 *######################################################################################################################
 */
package com.gc.api.returns;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:yashu.mittal@expicient.com">Yashu Mittal</a>
 */
public class GCUpdateSKUReference {

	// initialize logger
	private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCUpdateSKUReference.class);

	/**
	 * 
	 * This method will update SKUReference and Serial no by calling GCGetReturnableSKUService. 
	 *
	 * @param env 
	 * @param inDoc : inDoc sent from CC while order summary screen gets loaded. 
	 *
	 */
	public Document updateSKUReferenceId(YFSEnvironment env, Document inDoc) {

		LOGGER.beginTimer("GCUpdateSKUReference.updateSKUReferenceId");
		LOGGER.verbose("Class: GCUpdateSKUReference Method: updateSKUReferenceId START");

		if (LOGGER.isDebugEnabled()) {
			LOGGER.verbose("Input Doc is: " + YFCDocument.getDocumentFor(inDoc));
		}
		String sOrderHeaderKey = inDoc.getDocumentElement().getAttribute(GCConstants.ORDER_HEADER_KEY);
		String sExpiryQuotes = inDoc.getDocumentElement().getAttribute("ExpiryQuotes");

		//Defect-4044

		String sResolveHold =inDoc.getDocumentElement().getAttribute("ResolveHold");
		if(YFCCommon.equals(sResolveHold, "true")){
		YFCDocument getOrderListOutDocForHolds =
				GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "'/>"),
						YFCDocument.getDocumentFor(GCConstants.GC_GET_ORDER_LIST_RESOLVE_HOLD));

		YFCElement eleOrderList=getOrderListOutDocForHolds.getDocumentElement();

		YFCNodeList<YFCElement> listOrderLine = eleOrderList.getElementsByTagName(GCConstants.ORDER_LINE);

		for (YFCElement eleOrderLine : listOrderLine) {

			YFCElement eleHoldTypes = eleOrderLine.getChildElement(GCConstants.ORDER_HOLD_TYPES);

			YFCNodeList<YFCElement> listHoldType = eleHoldTypes.getElementsByTagName(GCConstants.ORDER_HOLD_TYPE);

			for (YFCElement eleHoldType : listHoldType) {

				String sHoldType=eleHoldType.getAttribute(GCConstants.HOLD_TYPE);
				String sStatus=eleHoldType.getAttribute(GCConstants.STATUS);

				if(YFCCommon.equals(sHoldType, "Warranty_Line_Hold") && YFCCommon.equals(sStatus, "1100")){

					String sPrimeLineNo=eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
					String sSubLineNo=eleOrderLine.getAttribute(GCConstants.SUB_LINE_NO);

					//Resolve Hold
					YFCDocument resolveHoldIndoc = YFCDocument.createDocument("Order");
					YFCElement eleResolveOrder = resolveHoldIndoc.getDocumentElement();

					eleResolveOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
					eleResolveOrder.setAttribute(GCConstants.OVERRIDE,"Y");

					
					YFCElement eleOrderLines=eleResolveOrder.createChild(GCConstants.ORDER_LINES);
					YFCElement eleOrderline=eleOrderLines.createChild(GCConstants.ORDER_LINE);

					eleOrderline.setAttribute(GCConstants.PRIME_LINE_NO, sPrimeLineNo);
					eleOrderline.setAttribute(GCConstants.SUB_LINE_NO, sSubLineNo);

					YFCElement eleOrderHoldTypes=eleOrderline.createChild(GCConstants.ORDER_HOLD_TYPES);
					YFCElement eleOrderHoldType=eleOrderHoldTypes.createChild(GCConstants.ORDER_HOLD_TYPE);
					eleOrderHoldType.setAttribute(GCConstants.HOLD_TYPE,"Warranty_Line_Hold");	
					eleOrderHoldType.setAttribute(GCConstants.REASON_TEXT,"force");	
					eleOrderHoldType.setAttribute("ResolverUserId","");	
					eleOrderHoldType.setAttribute(GCConstants.STATUS,"1300");	

					GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, resolveHoldIndoc, null);

				}
			}
		}
		//Ends-4044
		}
		
		YFCDocument getOrderListOut =
				GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "'/>"),
						YFCDocument.getDocumentFor(GCConstants.GET_ORDERLIST_TEMPLATE_FOR_SKU_UPDATE));

		YFCElement eleOrder = getOrderListOut.getDocumentElement().getChildElement(GCConstants.ORDER);

		YFCDocument inputDoc = GCXMLUtil.getDocument(eleOrder, true);
		LOGGER.verbose("Calling GCGetReturnableSKUService");
		Document changeOrderInputDoc = GCCommonUtil.invokeService(env, GCConstants.GC_GET_RETURNABLE_SKU_SERVICE, inputDoc.getDocument());

		Element changeOrderDocumentElement = changeOrderInputDoc.getDocumentElement();
		if (changeOrderDocumentElement.hasChildNodes() && changeOrderDocumentElement.getElementsByTagName(GCConstants.ORDER_LINES).item(0).hasChildNodes()) {
			LOGGER.verbose("Change order is required.");
			if (LOGGER.isDebugEnabled()) {
				LOGGER.verbose("ChangeOrder I/P is: " + YFCDocument.getDocumentFor(changeOrderInputDoc));
			}
			GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, changeOrderInputDoc, GCXMLUtil.getDocument("<Order/>"));

		}

		Document quoteServiceOutDoc= null;
		if(!YFCCommon.isVoid(sExpiryQuotes)){
			if(YFCCommon.equals(sExpiryQuotes, GCConstants.YES)){
				YFCDocument yfcQuoteServiceInDoc = YFCDocument.createDocument(GCConstants.ORDER);
				yfcQuoteServiceInDoc.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
				quoteServiceOutDoc = GCCommonUtil.invokeService(env, "GCQuoteExpirationService", yfcQuoteServiceInDoc.getDocument());
			}
		}
		String quoteServiceResponse="";
		if(!YFCCommon.isVoid(quoteServiceOutDoc)){
			 quoteServiceResponse = quoteServiceOutDoc.getDocumentElement().getAttribute("IsSuccess");
		}
		Document getOrderLineListOut =
				GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LINE_LIST, GCXMLUtil.getDocument("<OrderLine OrderHeaderKey='" + sOrderHeaderKey + "'/>"),
						GCCommonUtil.getDocumentFromPath("/global/template/api/getOrderLineListWithUpdatedSKU.xml"));

		if (LOGGER.isDebugEnabled()) {
			LOGGER.verbose("GetOrderLineList Output Docuement is: " + YFCDocument.getDocumentFor(getOrderLineListOut));
		}
		LOGGER.endTimer("GCUpdateSKUReference.updateSKUReferenceId");
		LOGGER.verbose("Class: GCUpdateSKUReference Method: updateSKUReferenceId END");
		if(!YFCCommon.isVoid(sExpiryQuotes)){
			getOrderLineListOut.getDocumentElement().setAttribute("IsSuccess", sExpiryQuotes);
		}
		return getOrderLineListOut;
	}
}
