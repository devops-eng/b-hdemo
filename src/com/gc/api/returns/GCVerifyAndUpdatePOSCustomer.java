/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 * ####################################################################################################################
 *	OBJECTIVE: Write what this class is about
 *######################################################################################################################
 *	Version     Date       Modified By              Description
 *######################################################################################################################
    1.0      12/03/2015   Mittal, Yashu 		GCSTORE-516: Design - DC Returns
 *######################################################################################################################
 */
package com.gc.api.returns;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:yashu.mittal@expicient.com">Yashu Mittal</a>
 */
public class GCVerifyAndUpdatePOSCustomer {

  // Initialize Logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCVerifyAndUpdatePOSCustomer.class);

  /**
   * 
   * This method will check if the order capturing channel is GCSTORE and refundonAccount is Y.
   *
   * @param env
   * @param inDoc 
   *
   */
  public Document verifyPOSCustomerID(YFSEnvironment env, Document inDoc) {

	  LOGGER.beginTimer("GCVerifyAndUpdatePOSCustomer.verifyPOSCustomerID");
	  LOGGER.verbose("Class: GCVerifyAndUpdatePOSCustomer Method: verifyPOSCustomerID START");
	  YFCElement eleOrderExtn = YFCDocument.getDocumentFor(inDoc).getDocumentElement().getChildElement(GCConstants.EXTN);
	  updatePOSCustomerID(env, inDoc, eleOrderExtn);

	  LOGGER.endTimer("GCVerifyAndUpdatePOSCustomer.verifyPOSCustomerID");
	  LOGGER.verbose("Class: GCVerifyAndUpdatePOSCustomer Method: verifyPOSCustomerID END");
	  return inDoc;
  }

  /**
   * 
   * This method will update POSCustomerId on order.
   *
   * @param env
   * @param inDoc
   * @param eleOrderExtn
   *
   */
  private void updatePOSCustomerID(YFSEnvironment env, Document inDoc, YFCElement eleOrderExtn) {

	  LOGGER.beginTimer("GCVerifyAndUpdatePOSCustomer.updatePOSCustomerID");
	  LOGGER.verbose("Class: GCVerifyAndUpdatePOSCustomer Method: updatePOSCustomerID START");
	  YFCElement eleOrder = YFCDocument.getDocumentFor(inDoc).getDocumentElement();
	  String strSOOHK = null;
	  YFCElement eleOrderLines = eleOrder.getChildElement(GCConstants.ORDER_LINES);
	  YFCIterable<YFCElement> listOrderLineList = eleOrderLines.getChildren(GCConstants.ORDER_LINE);
	  for (YFCElement eleOrderLine : listOrderLineList) {
		  strSOOHK = eleOrderLine.getAttribute(GCConstants.DERIVED_FROM_ORDER_HEADER_KEY);
		  if(!YFCCommon.isVoid(strSOOHK)){
			  break;
		  }
	  }
	  YFCDocument docGetOrderList = null;
	  if (!YFCCommon.isStringVoid(strSOOHK)) {
		  String strDocGOL = "<Order OrderHeaderKey='" + strSOOHK + "'/>";
		  YFCDocument inDocGOL = YFCDocument.getDocumentFor(strDocGOL);
		  String strGetOrderListTemp = "<OrderList><Order BillToID='' EnterpriseCode=''><Extn/></Order></OrderList>";
		  YFCDocument docGetOrderListTemp = YFCDocument.getDocumentFor(strGetOrderListTemp);
		  docGetOrderList = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, inDocGOL, docGetOrderListTemp);
		  checkPOSCustomerIDOnSO(env, docGetOrderList, eleOrderExtn);
	  }
	  LOGGER.endTimer("GCVerifyAndUpdatePOSCustomer.updatePOSCustomerID");
	  LOGGER.verbose("Class: GCVerifyAndUpdatePOSCustomer Method: updatePOSCustomerID END");
  }

  /**
   * 
   * This method will check if POS customer id is present on Sales Order and make webservice call to POS, if customer is not present.
   *
   * @param env
   * @param inDocSO
   * @param eleReturnOrderExtn
   *
   */
  private void checkPOSCustomerIDOnSO(YFSEnvironment env, YFCDocument inDocSO, YFCElement eleReturnOrderExtn) {

	  LOGGER.beginTimer("GCVerifyAndUpdatePOSCustomer.checkPOSCustomerIDOnSO");
	  LOGGER.verbose("Class: GCVerifyAndUpdatePOSCustomer Method: checkPOSCustomerIDOnSO START");
	  YFCElement eleSalesOrder = inDocSO.getDocumentElement().getChildElement(GCConstants.ORDER);
	  YFCElement eleSalesOrderExtn = eleSalesOrder.getChildElement(GCConstants.EXTN);
	  String sExtnPOSCustomerID = eleSalesOrderExtn.getAttribute(GCXmlLiterals.EXTN_POS_CUSTOMER_ID);
	  String sCustomerID = eleSalesOrder.getAttribute(GCConstants.BILL_TO_ID);
	  String sOrganizationCode = eleSalesOrder.getAttribute(GCConstants.ENTERPRISE_CODE);
	  if (!YFCCommon.isVoid(sExtnPOSCustomerID)) {
		  eleReturnOrderExtn.setAttribute(GCXmlLiterals.EXTN_POS_CUSTOMER_ID, sExtnPOSCustomerID);
	  } else {
		  if (!YFCCommon.isVoid(sCustomerID)) {
			  sExtnPOSCustomerID = checkPOSCustomerIDOnOMSCustomer(env, sCustomerID);
		  }

		  if (!YFCCommon.isVoid(sExtnPOSCustomerID)) {
			  eleReturnOrderExtn.setAttribute(GCXmlLiterals.EXTN_POS_CUSTOMER_ID, sExtnPOSCustomerID);
		  } else {
			  Document inGCGetPOSCustomerID = GCXMLUtil.getDocument("<Customer CustomerID='" + sCustomerID + "' OrganizationCode ='" + sOrganizationCode + "'/>");
			  Document outGCGetPOSCustomerID = GCCommonUtil.invokeService(env, "GCGetPOSCustomerID", inGCGetPOSCustomerID);
			  if(!YFCCommon.isVoid(outGCGetPOSCustomerID)){
				  YFCNodeList<YFCElement> nlCustomerExtn = YFCDocument.getDocumentFor(outGCGetPOSCustomerID).getElementsByTagName(GCConstants.EXTN);
				  if(nlCustomerExtn.getLength()>0){
					  YFCElement eleCustomerExtn = nlCustomerExtn.item(0);
					  if (!YFCCommon.isVoid(eleCustomerExtn)) {
						  sExtnPOSCustomerID = eleCustomerExtn.getAttribute(GCXmlLiterals.EXTN_POS_CUSTOMER_ID);
						  if (!YFCCommon.isVoid(sExtnPOSCustomerID)) {
							  eleReturnOrderExtn.setAttribute(GCXmlLiterals.EXTN_POS_CUSTOMER_ID, sExtnPOSCustomerID);
						  } else {
							  eleReturnOrderExtn.setAttribute(GCXmlLiterals.EXTN_POS_CUSTOMER_ID, "");
						  }
					  }
				  } else{
					  eleReturnOrderExtn.setAttribute(GCXmlLiterals.EXTN_POS_CUSTOMER_ID, "");
				  }
			  } else{
				  eleReturnOrderExtn.setAttribute(GCXmlLiterals.EXTN_POS_CUSTOMER_ID, "");
			  }

		  }
	  }
    LOGGER.endTimer("GCVerifyAndUpdatePOSCustomer.checkPOSCustomerIDOnSO");
    LOGGER.verbose("Class: GCVerifyAndUpdatePOSCustomer Method: checkPOSCustomerIDOnSO END");
  }

  /**
   * 
   * This method will check if POSCustomerID exists on the OMS Customer.
   *
   * @param env
   * @param sCustomerID
   * @return
   *
   */
  private String checkPOSCustomerIDOnOMSCustomer(YFSEnvironment env, String sCustomerID) {

    LOGGER.beginTimer("GCVerifyAndUpdatePOSCustomer.checkPOSCustomerIDOnOMSCustomer");
    LOGGER.verbose("Class: GCVerifyAndUpdatePOSCustomer Method: checkPOSCustomerIDOnOMSCustomer START");
    YFCDocument getCustomerListIn = YFCDocument.getDocumentFor("<Customer CustomerID='" + sCustomerID + "'/>");
    YFCDocument getCustomerListTemplate = YFCDocument.getDocumentFor("<CustomerList><Customer><Extn/></Customer></CustomerList>");
    YFCDocument getCustomerListOut = GCCommonUtil.invokeAPI(env, GCConstants.GET_CUSTOMER_LIST, getCustomerListIn, getCustomerListTemplate);
    YFCElement eleCustomerExtn = getCustomerListOut.getDocumentElement().getElementsByTagName(GCConstants.EXTN).item(0);
    if (!YFCCommon.isVoid(eleCustomerExtn)) {
      return eleCustomerExtn.getAttribute(GCXmlLiterals.EXTN_POS_CUSTOMER_ID);
    }
    return null;
  }

  // Phase 2 End : GCSTORE-556

}
