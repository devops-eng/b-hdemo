/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *#################################################################################################################################################################
 *           OBJECTIVE: Web Service client for Plcc dislosure.
 *#################################################################################################################################################################
 *           Version            Date              Modified By                Description
 *#################################################################################################################################################################
 *             1.0            08-Aug-2015       Infosys Limited            Initial Version
 *#################################################################################################################################################################
 */      
 
package com.gc.api.wsclient;

import java.io.IOException;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCPaymentSecurity;
import com.gc.common.utils.GCWebServiceUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * <h3>Description :</h3>This java class is called from 'GCPlccDisclosureWsClient_svc' Service to invoke the disclosure
 * webservice hosted by Synchrony
 *
 * @author Infosys Limited
 */
public class GCPlccDisclosureWsClient implements YIFCustomApi {

	//the class name
	private static final String CLASS_NAME =GCPlccDisclosureWsClient.class.getName();

	// Initialize Logger
	private static final YFCLogCategory LOGGER = YFCLogCategory.instance(CLASS_NAME);

	/*
	 * The configurations from configurator
	 */
	Properties props;


	/* (non-Javadoc)
	 * @see com.yantra.interop.japi.YIFCustomApi#setProperties(java.util.Properties)
	 */
	@Override
	public void setProperties(final Properties props){
		
		//Set the properties
		this.props= props;
	}

	
	/**
	 * <h3>Description:</h3>This method is used to invoke the disclosure webservice hosted by Synchrony. 
	 * @param yfsEnv The YFS Environment reference
	 * @param docInput The input document
	 * @return The response of the webservice
	 * @throws IOException TODO decide the exception handling approach
	 * @throws GCException Thrown if the account failed to decrypt
	 */
	public Document invokeDiclosureWS(final YFSEnvironment yfsEnv, final Document docInput) throws IOException, GCException{

		//logging the method input
		LOGGER.info( CLASS_NAME + ":invokeDiclosureWS:entry");
		
		//get the enterprise info
		final Element eleRoot= docInput.getDocumentElement();
		final String strEntprCode = eleRoot.getAttribute( GCConstants.ENTERPRISE_CODE);
		
		//update the input xml
		final Document docupdatedInput = updateDisclosureInput(docInput);
				
		//create an instance of the webservice util
		final GCWebServiceUtil wsutil = new GCWebServiceUtil();

		//Decrypted account no doc 
		final Document docDecryptedWSin; 
		
		//check if decryption is required
		if( GCConstants.YES.equalsIgnoreCase( getProperty( "DECRYPT_AC_NO"))){
			
			//decrypt the account number for the webservice
			docDecryptedWSin=decryptAccountNo( docupdatedInput);
		}else{
			
			docDecryptedWSin=docupdatedInput;
		}
		
		//invoke the webservice
		final Document docWebServiceResponse = wsutil.invokeSOAPWebService( docDecryptedWSin, 
				getProperty(strEntprCode+ GCConstants.UNDERSCORE+GCConstants.WS_END_POINT_URL),
				getProperty( strEntprCode+ GCConstants.UNDERSCORE+GCConstants.WS_SOAP_ACTION),
				getProperty( strEntprCode+ GCConstants.UNDERSCORE+GCConstants.WS_TIME_OUT), 
				getProperty( strEntprCode+ GCConstants.UNDERSCORE+GCConstants.WS_CONTENT_TYPE));

		//logging the method exit
		LOGGER.info( CLASS_NAME + ":invokeDiclosureWS:exit");		
		
		//return the webservice response
		return docWebServiceResponse;
	}


	/**
	 * <h3>Description:</h3>This method is used to decrypt the account number and update the xml
	 * @param docupdatedInput The document containing the encrypted input
	 * @return The decrypted document
	 * @throws GCException Thrown if the credit card is unable to be decrypted
	 */
	private Document decryptAccountNo(final Document docupdatedInput) throws GCException {
		
		//the method entry
		if( LOGGER.isDebugEnabled()){
			
			LOGGER.debug( CLASS_NAME + ":decryptAccountNo:entry");
		}
		
		//fetch the encrypted account number
		final Element eleAccNo = GCXMLUtil.getElementByXPath(docupdatedInput, "/Envelope/Body/promoDisclosure/promoDislosureParm/AccountNumber");
		final String strEncryptedAcNo = eleAccNo.getTextContent();
		
		//decrypt the data
		final String strDecryptedAcNo = GCPaymentSecurity.decryptCreditCard(strEncryptedAcNo, getProperty( GCConstants.DECRYPT_KEY));

		//set the decrypted data
		eleAccNo.setTextContent(strDecryptedAcNo);
		
		//the method exit
		if( LOGGER.isDebugEnabled()){
			
			LOGGER.debug( CLASS_NAME + ":decryptAccountNo:exit");
		}
		
		//return the decrypted document
		return docupdatedInput;
	}


	/**
	 * <h3>Description:</h3>This method is used to update the input document as required
	 * by Synchrony webservice  
	 * @param docInput The input th ewebservice client
	 * @return The input document updated
	 */
	private Document updateDisclosureInput(final Document docInput) {
		
		//log the entry
		if( LOGGER.isDebugEnabled()){
		
			LOGGER.debug( CLASS_NAME + ":updateDisclosureInput:entry");
		}

		//fetch the enterprise
		final Element eleRoot= docInput.getDocumentElement();
		final String strEntprCode = eleRoot.getAttribute( GCConstants.ENTERPRISE_CODE);
		eleRoot.removeAttribute( GCConstants.ENTERPRISE_CODE);
		
		//set merchant number
		final String strMerchNumber = getProperty( strEntprCode+GCConstants.UNDERSCORE + GCConstants.MERCHANT_NUMBER);
		final Element eleAccNum = GCXMLUtil.getElementByXPath(docInput, "/Envelope/Body/promoDisclosure/promoDislosureParm/MerchantNumber");
		eleAccNum.setTextContent(strMerchNumber);

		//set merchant number
		final String strUserName = getProperty( strEntprCode+GCConstants.UNDERSCORE + GCConstants.USER_NAME);
		final Element eleUserName = GCXMLUtil.getElementByXPath(docInput, "/Envelope/Header/Security/UsernameToken/Username");
		eleUserName.setTextContent(strUserName);
		
		//set merchant number
		final String strPasswd = getProperty( strEntprCode+GCConstants.UNDERSCORE + GCConstants.PASSWORD);
		final Element elePasswd = GCXMLUtil.getElementByXPath(docInput, "/Envelope/Header/Security/UsernameToken/Password");
		elePasswd.setTextContent(strPasswd);
				
		//log the exit
		if( LOGGER.isDebugEnabled()){
		
			LOGGER.debug( CLASS_NAME + ":updateDisclosureInput:exit");
		}
		
		//return the updated document
		return docInput;
	}


	/**
	 * <h3>Description:</h3>This method is used to get the end point URL to be used by the system
	 * @return The endpoint url configured in the customer_override.properties
	 */
	private String getProperty(final String strParamName) {
		
		//log the method entry
		if( LOGGER.isDebugEnabled()){
			
			LOGGER.debug( CLASS_NAME + ":getProperty:entry:Input="+strParamName);
		}

		//fetch the property value
		final String strParamValue = props.getProperty( strParamName);
		
		//log the method exit
		if( LOGGER.isDebugEnabled()){
			
			LOGGER.debug( CLASS_NAME + ":getProperty:exit:Property Value=" + strParamValue);
		}
		
		//return the paramValue
		return strParamValue;
	}
	
	
	public Document testEncryptOrDecrypt(final YFSEnvironment yfsEnv, final Document docInput) throws GCException{
		
		final Element eleRoot = docInput.getDocumentElement();
		
		if( GCConstants.YES.equalsIgnoreCase(eleRoot.getAttribute( "Encrypt"))){
			
			final String strEncryptedAcNo = eleRoot.getAttribute( "EncryptAcNo");
			final String strKey = eleRoot.getAttribute( "EncryptKey");  
			eleRoot.setAttribute("DeCryptedAcNo",  GCPaymentSecurity.decryptCreditCard(strEncryptedAcNo, strKey));
			

		}
		
		if( GCConstants.YES.equalsIgnoreCase(eleRoot.getAttribute( "Decrypt"))){
			
			final String strEncryptedAcNo = eleRoot.getAttribute( "DecryptAcNo");
			final String strKey = eleRoot.getAttribute( "DecryptKey");  
			eleRoot.setAttribute("EnCryptedAcNo",  GCPaymentSecurity.encryptCreditCard(strEncryptedAcNo, strKey));
			
		}
		
		return docInput;
	}
	
}