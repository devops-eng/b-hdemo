/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 * ############################################
 * ######################################################
 * ############################################################### OBJECTIVE: Value Constants class.
 * #################################################################################################
 * ################################################################ Version Date Modified By
 * Description
 * ######################################################################################
 * ########################################################################### 1.0 18/02/2014 Singh,
 * Gurpreet Initial Version 1.1 23/02/2014 Gowda, Naveen Inventory Management Constants added 1.2
 * 25/2/2014 Mittal, Yashu Added Item constants, updated API Constants 1.3 26/2/2014 Kumar, Prateek
 * Added constants for QAS webservice 1.4 27/2/2014 Mittal, Yashu Added constants related to price
 * list 1.5 4/3/2014 Gowda, Naveen Added constants for COM Create Order 1.6 11/3/2014 Kumar, Prateek
 * Added constants for Level of service 1.7 02/04/2014 Soni, Karan Added constants for Payment 1.8
 * 14/04/2013 Mitta, Yashu Added Base Config Constants 1.9 15/05/2014 Soni, Karan Added Constants
 * for Return and Exchanges 2.0 14/09/2014 Gupta, Himanshu Added Constants for Order Capture Channel
 * 2.1 19/12/2014 Jain,Shubham Added Constants for RTAM Published-GC Store 52 2.2 26/12/2014
 * Rangarajan,Shwetha Added Constants for Demand Sync to Dax-GCStore 53 2.3 06/01/2015 Kumar, Gunjan
 * Added Constants for POS Inventory Sync 2.4 12/02/2015 Sinha, Rajiv Added Constants for Sourcing
 * 2.2 10/01/2017 Mishra,Deepak Added Constants for GCSTORE-6762,GCSTORE-6916,6865/6864
 * 2.3 06/06/2017 Gupta,Abhishek Added Constant, modified GetItemListTemplate for GCSTORE-7083
 * ########################################################### #######################################
 * ###############################################################
 */

package com.gc.common.constants;

/**
 * This class maintains all the constants to be used in the GC Project
 */

public class GCConstants {

  public static final String ORDER = "Order";
  public static final String ORDER_NO = "OrderNo";
  public static final String ORDER_LINE_KEY = "OrderLineKey";
  public static final String ORDER_HEADER_KEY = "OrderHeaderKey";
  public static final String SHIP_NODE = "ShipNode";
  public static final String SELLER_ORGANIZATION_CODE = "SellerOrganizationCode";
  public static final String ORGANIZATION_CODE = "OrganizationCode";
  public static final String NODE = "Node";
  public static final String OVERRIDE = "Override";
  public static final String DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
  public static final String ORDER_DATE = "OrderDate";
  public static final String ENTERPRISE_CODE = "EnterpriseCode";
  public static final String STATUS = "Status";
  public static final String CUSTOMER_PO_NO = "CustomerPONo";
  public static final String GCI = "GCI";
  public static final String MODIFY = "MODIFY";
  public static final String SCHEDULE_TRAN_ID = "SCHEDULE.0001";
  public static final String RELEASE_TRAN_ID = "RELEASE.0001";
  public static final String TRANSACTION_ID = "TransactionId";
  public static final String ECOM = "ECOM";

  public static final String PAYMENT_STATUS = "PaymentStatus";
  public static final String AUTHORIZED = "Authorized";
  public static final String RECOMMENDATION_CODE = "RecommendationCode";

  public static final String NO = "N";
  public static final String YES = "Y";
  public static final String ACCEPT = "ACCEPT";
  public static final String REJECT = "REJECT";
  public static final String REVIEW = "REVIEW";

  public static final String PRIME_LINE_NO = "PrimeLineNo";
  public static final String SUB_LINE_NO = "SubLineNo";
  public static final String NAME = "Name";
  public static final String VALUE = "Value";
  public static final String ITEM = "Item";
  public static final String ITEM_ID = "ItemID";
  public static final String ORDERED_QTY = "OrderedQty";
  public static final String UOM = "UnitOfMeasure";
  public static final String REQUIRED_QTY = "RequiredQty";
  public static final String FULFILLMENT_TYPE = "FulfillmentType";
  public static final String RESERVATION_PARAMETERS = "ReservationParameters";
  public static final String RESERVATION_ID = "ReservationID";
  public static final String PRODUCT_CLASS = "ProductClass";
  public static final String QUANTITY = "Quantity";
  public static final String LIST_PRICE = "ListPrice";
  public static final String LINE_TOTAL = "LineTotal";
  public static final String LINE_PRICE_INFO = "LinePriceInfo";
  public static final String LINE_CHARGES = "LineCharges";
  public static final String LINE_CHARGE = "LineCharge";
  public static final String LINE_TAXES = "LineTaxes";
  public static final String LINE_TAX = "LineTax";
  public static final String CHARGE_CATEGORY = "ChargeCategory";
  public static final String CHARGE_NAME = "ChargeName";
  public static final String CHARGE_AMOUNT = "ChargeAmount";
  public static final String SHIPPING = "Shipping";
  public static final String UNIT_PRICE = "UnitPrice";
  public static final String IS_PRICE_LOCKED = "IsPriceLocked";
  public static final String CHARGE_PER_LINE = "ChargePerLine";
  public static final String QUANTITY_TO_SPLIT = "QuantityToSplit";
  public static final String SPLIT_LINES = "SplitLines";
  public static final String SPLIT_LINE = "SplitLine";
  public static final String IS_DISCOUNT = "IsDiscount";
  public static final String LINE_ID = "LineId";
  public static final String IS_BUNDLE_PARENT = "IsBundleParent";
  public static final String HEADER_CHARGE = "HeaderCharge";
  public static final String HEADER_CHARGES = "HeaderCharges";
  public static final String HEADER_TAXES = "HeaderTaxes";
  public static final String HEADER_TAX = "HeaderTax";

  public static final String IS_GUEST_CHECK_OUT = "IsGuestCheckOut";
  public static final String PROMISE = "Promise";
  public static final String PROMISE_LINES = "PromiseLines";
  public static final String PROMISE_LINE = "PromiseLine";
  public static final String ORDER_LINE_RESERVATIONS = "OrderLineReservations";
  public static final String ORDER_LINE_RESERVATION = "OrderLineReservation";

  public static final String COMMON_CODE = "CommonCode";
  public static final String CODE_TYPE = "CodeType";
  public static final String COMMON_CODE_CODE_VALUE = "CodeValue";
  public static final String SEQ_STATEMENT_ORDER = "select GC_SEQ_ORDER_NO.nextval from dual";
  public static final String GC = "GC";
  public static final String GUITAR = "GUITAR";

  public static final String BUYER_REMORSE_PERIOD = "BuyerRemorsePeriod";
  public static final String ORDER_HOLD_TYPES = "OrderHoldTypes";
  public static final String ORDER_HOLD_TYPE = "OrderHoldType";
  public static final String HOLD_TYPE = "HoldType";
  public static final String FRAUD_HOLD = "FRAUD_HOLD";
  public static final String FRAUD_REVIEW = "FRAUD_REVIEW";
  public static final String FRAUD_PENDING_REVIEW = "FRAUD_PENDING_REVIEW";
  public static final String BUYER_REMORSE_HOLD = "BUYER_REMORSE_HOLD";
  public static final String UNDERPRICED_HOLD = "UNDERPRICED_HOLD";
  public static final String RESOLVE_HOLD_CODE = "1300";
  public static final String APPLY_HOLD_CODE = "1100";
  public static final String EXTN = "Extn";
  public static final String BLANK_STRING = "";
  public static final String ACTION_CANCEL = "CANCEL";
  public static final String GUITAR_CENTER_INC = "GCI";
  public static final String TOTAL_ITEM_LIST = "TotalItemList";

  public static final String AA = "AE";
  public static final String AE = "AP";
  public static final String AP = "AA";
  public static final String EXTN_IS_APO_FPO = "ExtnIsAPOFPO";
  public static final String ADDRESS_LINE1 = "AddressLine1";
  public static final String ADDRESS_LINE2 = "AddressLine2";
  public static final String ADDRESS_LINE3 = "AddressLine3";
  public static final String EXTN_IS_PO_BOX = "ExtnIsPOBox";
  public static final String SUCCESS_CODE = "SUCCESS";

  public static final String SOMETIMES_TAG_CONTROLLED = "S";
  public static final String ALWAYS_TAG_CONTROLLED = "Y";
  public static final String EXTN_SET_CODE_VALUE = "1";

  // API Constants
  public static final String FIND_INVENTORY = "findInventory";
  public static final String API_GET_COMMON_CODE_LIST = "getCommonCodeList";
  public static final String API_CHANGE_ORDER = "changeOrder";
  public static final String API_GET_ORDER_LIST = "getOrderList";
  public static final String API_MANAGE_ITEM = "manageItem";
  public static final String API_MODIFY_CATEGORY_ITEM = "modifyCategoryItem";
  public static final String API_GET_ORDER_RELEASE_LIST = "getOrderReleaseList";
  public static final String API_GET_ORDER_RELEASE_DETAILS = "getOrderReleaseDetails";
  public static final String API_GET_ORDER_DETAILS = "getOrderDetails";
  public static final String API_RECORD_INVOICE_CREATION = "recordInvoiceCreation";
  public static final String API_MULTIAPI = "multiApi";
  public static final String MODIFY_ITEM_ASSOCIATIONS = "modifyItemAssociations";
  public static final String GET_SUPPLY_DETAILS = "getSupplyDetails";


  //GCSTORE-241
  public static final String STORE_DG="STORE_DG";
  public static final String RTAM_DG="RTAM_DG";
  public static final String SOURCING="SOURCING";
  public static final String PROD="PROD";
  public static final String GET_ORGANIZATION_LIST_INPUT="getOrganizationListInputDoc";
  public static final String MANAGE_ORGANIZATION_HIERARCHY="manageOrganizationHierarchy";
  public static final String GET_COMMON_CODE_LIST_INPUT="getCommonCodeListInputDoc";
  public static final String MANAGE_DISTRIBUTION_RULE="manageDistributionRule";
  public static final String PUBLISH_MESSAGE_FOR_LATEST_AVAILABILITY_INPUT="publishMessageForLatestAvailability";
  public static final String CREATE_INVENTORY_ACTIVITY_INPUT="createInventoryActivityInputDoc";
  public static final String CREATE_INVENTORY_ACTIVITY="createInventoryActivity";
  public static final String GET_INVENTORY_SNAPSHOT_INPUT="getInventorySnapShotInputDoc";
  public static final String GET_INVENTORY_SNAPSHOT="getInventorySnapShot";
  public static final String MANAGE_INVENTORY_ALERTS_INPUT="manageInventoryAlertsInputDoc";
  public static final String MANAGE_INVENTORY_ALERTS="manageInventoryAlerts";
  public static final String MANAGE_INVENTORY_NODE_CONTROL="manageInventoryNodeControl";


  //GCSTORE-790
  public static final String MANAGEMENT="Management";
  public static final String DOMAIN_LOGINID_SEPARATOR="@";
  public static final String LDAP_DOMAIN_LOGINID_SEPARATOR="\\";
  public static final String IS_GROUP_VALIDATION_REQUIRED="IsGroupValidationRequired";

  // Service Constants
  public static final String GC_RESERVE_INVENTORY_SERVICE = "GCReserveInventoryService";
  public static final String GC_VALIDATE_AND_RESOLVE_HOLDS_WITH_PERIOD_SERVICE = "GCValidateAndResolveHoldsWithPeriodService";
  public static final String GC_SEND_FRAUD_REQUEST_SERVICE = "GCSendFraudRequestService";
  public static final String GC_PUBLISH_TO_ATG = "GCPublishToATG";
  public static final String GC_PUBLISH_MSG_TO_INTERNAL_Q_FOR_AVAILABILITY_SERVICE="GCPublishMsgToInternalQForAvailabilityService";

  // Templates
  public static final String GET_CHANGE_ORDER_LIST_TEMPLATE="<OrderList><Order OrderHeaderKey=''><OrderLines><OrderLine OrderLineKey=''/></OrderLines></Order></OrderList>";
  //GCStore-661
  public static final String GC_GET_COMPLETE_ORDER_DETAILS_TEMPLATE="<Order><OrderLines><OrderLine OrderLineKey=''><LineOverallTotals /><LineCharges>"
          + "<LineCharge ChargeAmount='' ChargeCategory='' ChargeName='' /></LineCharges><LineTaxes/> <Extn> <GCOrderLineComponentsList> <GCOrderLineComponents/> "
          + "</GCOrderLineComponentsList> </Extn> <BundleParentLine OrderLineKey='' PrimeLineNo='' SubLineNo=''/> </OrderLine></OrderLines><ReturnOrders><ReturnOrder><OrderLines>"
          +  "<OrderLine OrderLineKey='' DerivedFromOrderLineKey=''><LineCharges><LineCharge ChargeAmount='' ChargeCategory='' ChargeName=''/>"
          + "</LineCharges><LineTaxes/><LineOverallTotals /></OrderLine></OrderLines></ReturnOrder></ReturnOrders></Order>";

  public static final String GC_GET_ORDER_INVOICE_DETAILS_LIST_API_TEMPLATE="<OrderInvoiceDetailList TotalNumberOfRecords=''>"
      + "<OrderInvoiceDetail PrimeLineNo='' OrderLineKey='' Quantity=''><InvoiceHeader InvoiceType=''></InvoiceHeader>"
      + "<LineCharges><LineCharge ChargeAmount='' ChargeCategory='' ChargeName=''/></LineCharges></OrderInvoiceDetail>"
      + "</OrderInvoiceDetailList>";


  //GCStore-661

  public static final String GC_GET_ALTERNATE_STORE_AVAILABILITY_TEMPLATE="<AlternateStores OrganizationCode=''><NodeList>"
      + "<Node ActivateFlag='' Description='' DistanceFromShipToAddress='' DistanceUOM='' ShipNode=''><ShipNodePersonInfo><Extn/></ShipNodePersonInfo>"
      + "<Organization PrimaryUrl=''><Extn ExtnIsSourceFromStoreAllowed=''/></Organization>"
      + "<Availability AvailableDate='' AvailableQty='' IsAvailable='' IsFutureAvailability='' ShipNode='' UnitOfMeasure='' /></Node></NodeList></AlternateStores>";

  public static final String GET_ORDER_LIST_FRAUD_TEMPLATE = "<OrderList><Order OrderHeaderKey='' PaymentStatus=''><OrderHoldTypes>"
      + "<OrderHoldType HoldType='' Status='' /></OrderHoldTypes><OrderLines><OrderLine OrderedQty=''"
      + " PrimeLineNo='' SubLineNo='' OrderLineKey=''></OrderLine>"
      + "</OrderLines></Order></OrderList>";
  //GCSTORE-7083 - addition of attribute ProductLine to PrimaryInformation
  public static final String GET_ITEM_LIST_TEMPLATE = "<ItemList><Item><PrimaryInformation UnitCost='' ItemType='' "
      + "PrimarySupplier='' KitCode='' ProductLine='' />"
      + "<Extn ExtnIsUsedOrVintageItem='' ExtnSetCode='' ExtnIsShipAlone='' ExtnInventoryCost='' ExtnIsPreOrderItem='' ExtnItemStatus='' ExtnBillableWeight=''/>"
      + "<Components><Component /></Components></Item></ItemList>";
  public static final String GET_ORDER_LIST_TEMPLATE = "<OrderList><Order OrderHeaderKey='' ReqShipDate='' ><OrderLines><OrderLine OrderedQty=''"
      + " PrimeLineNo='' SubLineNo='' OrderLineKey=''><LinePriceInfo /><LineCharges><LineCharge /></LineCharges></OrderLine>"
      + "</OrderLines></Order></OrderList>";

  public static final String GET_SUPPLY_DETAILS_TEMPLATE = "<Item ProductClass='' UnitOfMeasure=''>"
      + "<ShipNodes><ShipNode AvailableQty='' ShipNode='' Tracked=''><Supplies><Supply AvailableQty='' SupplyType='' TotalQty=''>"
      + "<SupplyDetails ETA='' Quantity='' SupplyType='' TagNumber='' Track=''>"
      + "<Tag BatchNo='' LotAttribute1='' TagNumber='' TotalDemand='' TotalOnhandSupply='' TotalOtherSupply=''/>"
      + "</SupplyDetails></Supply></Supplies></ShipNode></ShipNodes></Item>";


  //GCStoreLoadAPI Templates Begin:
  public static final String GET_ORGANIZATION_LIST_TEMPLATE="<OrganizationList><Organization OrganizationCode=''>"
      + "<Extn ExtnIsSourceFromStoreAllowed=''/></Organization></OrganizationList>";

  public static final String GET_COMMON_CODE_LIST_TEMPLATE="<CommonCodeList><CommonCode CodeShortDescription='' CodeType='' CodeValue='' OrganizationCode=''/></CommonCodeList>";

  public static final String GET_INVENTORY_SNAPSHOT_TEMPLATE="<InventorySnapShot><ShipNode LastInventoryItemKey='' ShipNode=''>"
      + "<Item InventoryItemKey='' InventoryOrganizationCode='' ItemID='' ProductClass='' UnitOfMeasure=''><SupplyDetails Quantity='' Track=''/>"
      + "<DemandDetails Quantity='' /></Item></ShipNode></InventorySnapShot>";

  //GCStoreLoadAPI Templates END


  //GCGetItemListTemplate:
  public static final String GET_COMPLETE_ITEM_LIST_TEMPLATE="<ItemList><Item><PrimaryInformation ItemType=''/></Item></ItemList>";


  /***************** Inventory Constants - BEGIN *********************/

  public static final String GET_ITEM_LIST_OUT_TEMP = new StringBuilder(
      "<ItemList>").append("<Item ItemID=''>")
      .append("<PrimaryInformation KitCode=''/>").append("<Components>")
      .append("<Component ComponentItemID='' KitQuantity=''/>")
	  .append("</Components>")
      .append("<Extn ExtnSetCode='' ExtnIsUsedOrVintageItem=''/>")
      .append("</Item>").append("</ItemList>").toString();

  public static final String MSG_ID = "MsgID";
  public static final String APPLY_DIFFERENCES = "ApplyDifferences";
  public static final String COMPLETE_INVENTORY = "CompleteInventoryFlag";
  public static final String VALIDATE_ITEMS = "ValidateItems";
  public static final String FLAG_N = "N";
  public static final String FLAG_Y = "Y";
  public static final String ITEMS_ITEM_XPATH = "Items/Item";
  public static final String ITEMLIST_ITEM_PRIMARYINFO_KIT_CODE_XPATH = "ItemList/Item/PrimaryInformation/@KitCode";
  public static final String ITEMLIST_ITEM_EXTN_SET_CODE_XPATH = "ItemList/Item/Extn/@ExtnSetCode";
  public static final String BUNDLE = "BUNDLE";
  public static final String API_LOAD_INVENTORY_MISMATCH = "loadInventoryMismatch";
  public static final String ITEMLIST_ITEM_COMPONENTS_COMPONENT_XPATH = "ItemList/Item/Components/Component";
  public static final String SUPPLIES = "Supplies";
  public static final String SUPPLY = "Supply";
  public static final String COMPONENT="Component";
  public static final String COMPONENT_ITEM_ID = "ComponentItemID";
  public static final String KIT_QUANTITY = "KitQuantity";
  public static final String AVAILABILITY_TYPE = "AvailabilityType";
  public static final String SUPPLY_TYPE = "SupplyType";
  public static final String SET = "SET";
  public static final String API_GET_ITEM_LIST = "getItemList";
  public static final String API_ADJUST_INVENTORY = "adjustInventory";
  public static final String INVENTORY_ORGANIZATION = "InventoryOrganization";
  public static final String YANTRA_GROUP_MESSAGE_ID = "YantraMessageGroupID";
  public static final String INVENTORY = "Inventory";
  public static final String API_SYNC_LOADED_INVENTORY = "syncLoadedInventory";
  public static final String DEMAND = "Demand";
  public static final String DEMAND_SHIP_DATE = "DemandShipDate";
  public static final String DEMAND_TYPE = "DemandType";
  public static final String SYNC_INVENTORY_DEMAND_ITEMS_ITEM = "SyncInventoryDemand/Items/Item";
  public static final String SYNC_INVENTORY_DEMAND_ITEMS = "SyncInventoryDemand/Items";

  public static final String INT_ZERO = "0";
  public static final String INT_ONE = "1";
  public static final String RECEIPT_SHIPMENT_XPATH = "/Receipt/Shipment";
  public static final String DOCUMENT_TYPE = "DocumentType";
  public static final String RECEIPT_RECEIPTLINES_XPATH = "/Receipt/ReceiptLines";
  public static final String RECEIPT_RECEIPT_HEADER_KEY_ATTR = "/Receipt/@ReceiptHeaderKey";
  public static final String RECEIPT_HEADER_KEY = "ReceiptHeaderKey";
  public static final String RECEIPT_RECEIPTLINES_RECEIPTLINE_XPATH = "/Receipt/ReceiptLines/ReceiptLine";
  public static final String ORDERLINELIST_ORDERLINE_ORDEREDQTY_ATTR = "/OrderLineList/OrderLine/@OrderedQty";
  public static final String ORDERLINELIST_ORDERLINE_ORDERLINEKEY_ATTR = "/OrderLineList/OrderLine/@OrderLineKey";
  public static final String ORDERLINELIST_ORDERLINE_KITCODE_ATTR = "/OrderLineList/OrderLine/@KitCode";
  public static final String ORDERLINELIST_ORDERLINE = "/OrderLineList/OrderLine";
  public static final String ORDERED_QUANITY = "OrderedQty";
  public static final String RECEIPT_LINE = "ReceiptLine";
  public static final String RECEIPT = "Receipt";
  public static final String SHIPMENT = "Shipment";
  public static final String API_RECEIVE_ORDER = "receiveOrder";
  public static final String API_CLOSE_RECEIPT = "closeReceipt";
  public static final String API_GET_ORDER_LINE_LIST = "getOrderLineList";
  public static final String ORDER_LINE = "OrderLine";
  public static final String BUNDLE_PARENT_LINE = "BundleParentLine";
  public static final String API_START_RECEIPT = "startReceipt";
  public static final String ORDER_ORDERLINES_ORDERLINE_XPATH = "/Order/OrderLines/OrderLine";
  public static final String ACTION = "Action";
  public static final String CREATE = "CREATE";
  public static final String API_CREATE_ORDER = "createOrder";

  public static final String CANCEL = "CANCEL";
  public static final String GET_ORDER_LIST_TEMPLATE_FOR_DAX_NO = "<OrderList><Order OrderNo=''><Extn ExtnDAXOrderNo=''/></Order></OrderList>";
  public static final String API_CANCEL_ORDER = "cancelOrder";
  public static final String EXTN_DAX_ORDER_NO = "ExtnDAXOrderNo";
  public static final String ORDER_USER_NAME = "UserName";
  public static final String ORDERLIST_ORDER_ORDERHEADERKEY_XPATH = "/OrderList/Order/@OrderHeaderKey";

  public static final String UNRECEIVE_QTY = "UnreceiveQuantity";
  public static final String RECEIPT_LINES = "ReceiptLines";
  public static final String API_GET_RECEIPT_LINE_LIST = "getReceiptLineList";
  public static final String RECEIPT_LINE_KEY = "ReceiptLineKey";

  /***************** Inventory Constants - END *********************/

  /***************** Item Constants - BEGIN *********************/
  public static final String PRIMARY_INFORMATION = "PrimaryInformation";
  public static final String ITEM_TYPE = "ItemType";
  public static final String EXTN_ITEM_STATUS = "ExtnItemStatus";
  public static final String EXTN_VENDOR_ID = "ExtnVendorId";
  public static final String ADJUSTMENT_TYPE = "AdjustmentType";
  public static final String ADJUSTMENT = "ADJUSTMENT";
  public static final String ONHAND = "ONHAND";
  public static final String UNIT_OF_MEASURE = "UnitOfMeasure";
  public static final String EACH = "EACH";
  public static final String AVAILABILITY = "Availability";
  public static final String INFINITE = "INFINITE";
  public static final String PRODUCT_CLASS_REGULAR = "REGULAR";
  public static final String PRODUCT_CLASS_SET = "SET";
  public static final String KIT_CODE = "KitCode";
  public static final String ITEM_COMPONENTS_COMPONENT_XPATH = "Item/Components/Component";
  public static final String ITEM_LIST_ITEM_XPATH = "ItemList/Item";
  public static final String EXTN_CATEGORY = "ExtnCategory";
  public static final String CATEGORY_PATH = "CategoryPath";
  public static final String WEB_ITEMS_CATEGORY_PATH = "/GCIMasterCatalog/GCWEB";
  public static final String SHORT_DESCRIPTION = "ShortDescription";
  public static final String DESCRIPTION = "Description";
  public static final String EXTN_WEB_CATEGORY = "ExtnWebCategory";
  public static final String EXTN_DCT_CATEGORY = "ExtnDCTCategory";
  public static final String CATEGORY = "Category";
  public static final String CATEGORY_ID = "CategoryID";
  public static final String API_GET_CATEGORY_LIST = "getCategoryList";
  public static final String CATEGORY_ITEM_LIST = "CategoryItemList";

  /***************** Item Constants - END *********************/

  // Webservice util constant: Begin
  public static final String GC_CONFIG = "gc_config";
  public static final String GC_WEB_SERVICE_TIME_OUT = "gc.webservice.timeout";
  public static final String SOAP_ACTION = "SOAPAction";
  public static final String TEXT_XML = "text/xml";
  public static final String UTF8 = "UTF-8";
  public static final String CONTENT_TYPE = "Content-Type";
  public static final String CONTENT_TYPE_VAL = "application/soap+xml; charset=utf-8";
  // Webservice util constant: End

  // QAS service constant: Begin
  public static final String QAS_URL = "gc.qas.service.url";
  public static final String QAS_SOAP_ACTION = "gc.qas.soapaction";
  public static final String QA_SEARCH_RESULT = "QASearchResult";
  public static final String RETURN_CODE = "ReturnCode";
  public static final String TIME_OUT = "TimeOut";
  public static final String VERIFY_LEVEL = "VerifyLevel";
  public static final String NONE = "None";
  public static final String SUGGESTED_ADDRESSESS = "SuggestedAddresses";
  public static final String STREET_PARTIAL = "StreetPartial";
  public static final String PREMISES_PARTIAL = "PremisesPartial";
  public static final String MULTIPLE = "Multiple";
  public static final String SUGGESTED_ADDRESS = "SuggestedAddress";
  public static final String PICKLIST = "Picklist";
  public static final String POSTCODE = "Postcode";
  public static final String ADDRESS_LINE_1 = "AddressLine1";
  public static final String CITY = "City";
  public static final String STATE = "State";
  public static final String ZIP_CODE = "ZipCode";
  public static final String DISPLAY_ON_UI = "DisplayOnUI";
  public static final String ADDRESS = "Address";
  public static final String ADDRESS_LINE_2 = "AddressLine2";
  public static final String SOAPENV_ENVELOPE = "soapenv:Envelope";
  public static final String XMLNS_SOAPENV = "xmlns:soapenv";
  public static final String XMLNS_SOAPENV_VALUE = "http://schemas.xmlsoap.org/soap/envelope/";
  public static final String XMLNS_OND = "xmlns:ond";
  public static final String XMLNS_OND_VALUE = "http://www.qas.com/OnDemand-2011-03";
  public static final String SOAPENV_HEADER = "soapenv:Header";
  public static final String OND_QA_QUERY_HEADER = "ond:QAQueryHeader";
  public static final String OND_QA_AUTHENTICATION = "ond:QAAuthentication";
  public static final String OND_USER_NAME = "ond:Username";
  public static final String OND_PASSWORD = "ond:Password";
  public static final String USER_NAME = "USER_NAME";
  public static final String PASSWORD = "PASSWORD";
  public static final String SOAPENV_BODY = "soapenv:Body";
  public static final String OND_QA_SEARCH = "ond:QASearch";
  public static final String OND_COUNTRY = "ond:Country";
  public static final String USA = "USA";
  public static final String OND_ENGINE = "ond:Engine";
  public static final String FLATTEN = "Flatten";
  public static final String TRUE = "true";
  public static final String VERIFICATION = "Verification";
  public static final String OND_LAYOUT = "ond:Layout";
  public static final String QAS_STANDARD_LAYOUT = "( qas standard layout )";
  public static final String OND_SEARCH = "ond:Search";
  public static final String OND_FORMATTED_ADDRESS_IN_PICKLIST = "ond:FormattedAddressInPicklist";
  public static final String COUNTRY = "Country";
  public static final String PERSON_INFO = "PersonInfo";
  public static final String PROCEED_WITH_SINGLE_AVS_RESULT = "ProceedWithSingleAVSResult";
  public static final String FAILED = "FAILED";
  public static final String AVS_RETURN_CODE = "AVSReturnCode";
  public static final String ADDRESS_VERIFIFICATION_RESPONSE_MESSAGE = "AddressVerificationResponseMessage";
  public static final String ADDRESS_VERIFIFICATION_RESPONSE_MESSAGES = "AddressVerificationResponseMessages";
  public static final String PERSON_INFO_LIST = "PersonInfoList";
  public static final String AVS_DOWN = "AVS_DOWN";
  public static final String US = "US";
  // QAS service constant: End

  /***************** PriceList Constants - BEGIN *********************/

  public static final String END_DATE_ACTIVE = "EndDateActive";
  public static final String START_DATE_ACTIVE = "StartDateActive";

  /***************** PriceList Constants - END *********************/

  /***************** COM Create Order Constants - BEGIN ************/
  public static final String API_GET_AVAILABLE_INVENTORY = "getAvailableInventory";
  public static final String IS_AVAILABLE = "IsAvailable";
  public static final String EXTN_IS_BACKORDERABLE = "ExtnIsBackorderable";
  public static final String ITEM_LIST = "ItemList";
  public static final String PROMISE_ORGANIZATION_CODE_ATTR = "/Promise/@OrganizationCode";
  public static final String PROMISE_PROMISELINES_PROMISELINE_ITEMID_ATTR = "/Promise/PromiseLines/PromiseLine/@ItemID";
  public static final String PROMISE_PROMISELINES_PROMISELINE_PRODUCT_CLASS_ATTR = "/Promise/PromiseLines/PromiseLine/@ProductClass";
  public static final String PROMISE_PROMISELINES_PROMISELINE_UOM_ATTR = "/Promise/PromiseLines/PromiseLine/@UnitOfMeasure";
  public static final String PROMISE_PROMISELINES_PROMISELINE_AVAILIBILITY_AVAILABLEINVENTORY_AVAILABLEQTY_ATTR = "Promise/PromiseLines/PromiseLine/Availability/AvailableInventory/@AvailableQuantity";
  public static final String ITEMLIST_ITEM_EXTN_EXTNISBACKORDERABLE_ATTR = "/ItemList/Item/Extn/@ExtnIsBackorderable";

  public static final String CUSTOMER_ID = "CustomerID";
  public static final String BILL_TO_ID = "BillToID";
  public static final String ORDER_ON_HOLD = "OrderOnHold";
  public static final String TAX_EXEMPT_HOLD = "TAX_EXEMPT_HOLD";
  public static final String ORDER_LIST = "OrderList";
  public static final String GET_ORDER_LIST = "getOrderList";
  public static final String GET_SHIP_NODE_LIST = "getShipNodeList";
  public static final String CHANGE_ORDER = "changeOrder";
  public static final String API_GET_COMPLETE_ITEM_LIST = "getCompleteItemList";
  public static final String IS_ITEM_AVAILABLE = "IsItemAvailable";
  public static final String AVAILABLE_QUANTIY = "AvailableQuantity";
  public static final String START_DATE = "StartDate";
  public static final String PO_AVAILABILITY_DATE = "POAvailabilityDate";
  public static final String AVAILABLE_ONHAND_QTY = "AvailableOnhandQuantity";
  public static final String AVAILABLE_FUTURE_QTY = "AvailableFutureQuantity";
  public static final String CHECK_INVENTORY = "CheckInventory";
  public static final String NEXT_AVAILABILITY_DATE = "NextAvailabilityDate";
  public static final String YYYY_MM_DD = "yyyy-MM-dd";
  public static final String ITEM_STATUS_C2 = "C2";
  public static final String PRODUCT_MAX_PRICE = "ProductMaxPrice";
  public static final String PRODUCT_MIN_PRICE = "ProductMinPrice";
  public static final String GC_WARRANTY_ITEMS = "GCWarrantyItems";
  public static final String INVENTORY_PARAMETERS = "InventoryParameters";
  public static final String EXTN_CATEGORY_CODE = "ExtnCategoryCode";
  public static final String ITEM_PRICE = "ItemPrice";
  public static final String CURRENCY = "Currency";
  public static final String USD = "USD";
  public static final String LINE_ITEMS = "LineItems";
  public static final String LINE_ITEM = "LineItem";
  public static final String API_GET_ITEM_PRICE = "getItemPrice";
  public static final String GC_GET_ITEM_WARRANTY_LIST_SERVICE = "GCGetItemWarrantyListService";
  public static final String USER_ID = "UserID";
  public static final String USRNAME = "Username";
  public static final String USERNAME = "Username";
  public static final String USER = "User";
  public static final String USERLIST = "UserList";
  public static final String API_GET_USER_LIST = "getUserList";
  public static final String COMMISSION_TYPE = "CommissionType";
  public static final String SALES_COMMISSION = "Sales Commission";
  public static final String GC_UPDATE_COMMISSION_SERVICE = "GCUpdateCommissionService";
  public static final String IS_UPDATE_COMMISSION = "IsUpdateCommission";

  public static final String STANDARD = "Standard";
  public static final String KIT = "Kit";
  public static final String NEW = "New";
  public static final String USED = "Used";
  public static final String EXTN_WARRANTY_ALL_CATEGORIES = "ExtnWarrantyAllCategories";
  public static final String CATEGORY_CODE = "CategoryCode";
  public static final String EXTN_WARRANTY_MIN_PRICE = "ExtnWarrantyMinPrice";
  public static final String EXTN_WARRRANTY_MAX_PRICE = "ExtnWarrantyMaxPrice";
  public static final String EXTN_WARRANTY_IS_ACTIVE = "ExtnWarrantyIsActive";
  public static final String EXTN_WARANTY_SKU_TYPE = "ExtnWarrantySKUType";
  public static final String EXTN_WARRANTY_CONDITION = "ExtnWarrantyCondition";
  public static final String FIRST_NAME = "FirstName";
  public static final String LAST_NAME = "LastName";
  public static final String TITLE = "Title";
  public static final String DAY_PHONE = "DayPhone";
  public static final String EVENING_PHONE = "EveningPhone";
  public static final String EMAIL_ID = "EMailID";
  public static final String ADDRESS_ID = "AddressID";
  public static final String EXTN_ADDRESS_TYPE = "ExtnAddressType";
  public static final String UNCLASSIFIED = "UnClassified";
  public static final String COMMERCIAL = "Commercial";
  public static final String RESIDENTIAL = "Residential";
  public static final String CATEGORY_TYPE = "CategoryType";
  public static final String ZERO_ONE = "01";
  public static final String ZERO_AMOUNT = "0.00";
  public static final String ECOMM = "ECOMM";
  /***************** Customer Management Constants - BEGIN *********************/

  public static final String CUSTOMER = "Customer";
  public static final String GET_CUSTOMER_LIST = "getCustomerList";
  /***************** Customer Management Constants - END *********************/

  public static final String ALASKA_STATE = "AK";
  public static final String HAWAI_STATE = "HI";
  public static final String GIFT_CERTIFICATE_CODE = "09";
  public static final String IS_HAZMAT = "IsHazmat";
  public static final String UNIT_WEIGHT = "UnitWeight";
  public static final String LEVEL_OF_SERVICE = "LevelOfService";
  public static final String ALLOWED_SHIPPING_METHOD_LIST = "AllowedShippingMethodList";
  public static final String ALLOWED_SHIPPING_METHOD = "AllowedShippingMethod";
  public static final String SHIPPING_METHOD = "ShippingMethod";
  public static final String SHIPPING_METHOD_DESCRIPTION = "ShippingMethodDescription";
  public static final String ECONOMY_GROUND = "ECONOMY_GROUND";
  public static final String DIGITAL_DELIVERY = "DIGITAL_DELIVERY";
  public static final String SHIPPING_LEVEL_FLAG = "ShippingLevelFlag";
  public static final String GC_GET_SHIPPING_LEVEL_FLAG = "GCGetShippingLevelFlag";
  public static final String CODE_VALUE = "CodeValue";
  public static final String GET_COMMON_CODE_LIST = "getCommonCodeList";
  public static final String EMPLOYEE_PICKUP = "EMPLOYEE_PICKUP";
  public static final String ORDER_LINES = "OrderLines";
  public static final String DROP_SHIP = "DROP_SHIP";
  public static final String EXTN_EXPEDITE_FLAG = "ExtnExpediteFlag";
  public static final String ITEM_V1 = "V1";
  public static final String DIGITAL_ITEM_TYPE = "05";
  public static final String STANDARD_GROUND = "STANDARD_GROUND";
  public static final String SECOND_DAY_EXPRESS = "SECOND_DAY_EXPRESS";
  public static final String NEXT_DAY_AIR = "NEXT_DAY_AIR";
  public static final String EXTN_IS_TRUCK_SHIP = "ExtnIsTruckShip";
  public static final String CARRIER_SERVICE_CODE = "CarrierServiceCode";
  public static final String GET_ALTERNATE_STORE_AVAILABILITY = "getAlternateStoreAvailability";
  public static final String ORGANIZATION = "Organization";
  public static final String EXTN_IS_SHIP_TO_STORE_ALLOWED = "ExtnIsShiptoStoreAllowed";
  public static final String GET_ORGANIZATION_LIST = "getOrganizationList";
  public static final String ITEM_DETAILS = "ItemDetails";
  public static final String APPLY_CARRIER_SERVICE_CODE = "ApplyCarrierServiceCode";
  public static final String GC_APPLY_LEVEL_OF_SERVICE_TO_ORDERLINE = "GCApplyLevelOfServiceToOrderLine";
  public static final String GC_APPLY_CARRIER_SERVICE_CODE_TO_ORDERLINE = "GCApplyCarrierServiceCodeToOrderLine";
  public static final String IS_ORDER_COMMISSION_OVERRIDEN = "IsOrderCommissionOverriden";
  public static final String IS_ORDER_LEVEL = "IsOrderLevel";
  public static final String OVERRIDE_ORDER_LINE_COMMISSION = "OverrideOrderLineCommission";
  public static final String GIFT_CARD_TYPE = "04";
  public static final String ECONOMY_GROUND_DESC = "Economy Ground";
  public static final String DIGITAL_DELIVERY_DESC = "Digital Delivery";
  public static final String STANDARD_GROUND_DESC = "Standard Ground";

  /***************** Order Schedule and Release Constants - BEGIN *********************/
  public static final String GET_ATP = "GetATP";
  public static final String CONSIDER_ALL_NODES = "ConsiderAllNodes";
  public static final String END_DATE = "EndDate";
  public static final String SCHEDULES = "Schedules";
  public static final String SCHEDULE = "Schedule";
  public static final String SCHEDULE_ID = "ScheduleId";
  public static final String REJECT_ASSIGNMENT = "RejectAssignment";
  public static final String EXTN_IS_PO_RESERVED = "ExtnIsPOReserved";
  public static final String PO_PLACED = "PO_PLACED";
  public static final String COMPONENT_UOM = "ComponentUnitOfMeasure";
  public static final String DAX_ALLOCATION_HOLD = "DAX_ALLOCATION_HOLD";
  public static final String ORDER_STATUS_CHANGE = "OrderStatusChange";
  public static final String BASE_DROP_STATUS = "BaseDropStatus";
  public static final String IGNORE_TRANSACTION_DEPENDENCIES = "IgnoreTransactionDependencies";
  public static final String CHANGE_FOR_ALL_AVAIL_QTY = "ChangeForAllAvailableQty";
  public static final String CHANGE_STATUS_TRANSACTION = "CHANGE_STATUS_BACKORDER.0001.ex";
  public static final String EXTN_PICK_REQUEST_NO = "ExtnPickRequestNo";
  public static final String ORDER_RELEASE = "OrderRelease";
  public static final String SALES_ORDER_NO = "SalesOrderNo";
  public static final String RELEASE_NO = "ReleaseNo";
  public static final String TOTAL_RESERVED_QTY = "TotalReservedQty";
  public static final String ITEM_03 = "03";
  public static final String ITEM_STATUS = "ItemStatus";
  public static final String DEPENDENT_ON_LINE_KEY = "DependentOnLineKey";
  public static final String RELEASED_STATUS = "3200";
  public static final String SHIPPED_TRAN_ID = "SHIP_ORDER";
  public static final String SHIPPED_STATUS = "3700";
  public static final String SHIP_TO_CUSTOMER = "SHIP_TO_CUSTOMER";
  public static final String CHARGE_PER_UNIT = "ChargePerUnit";
  public static final String DATE_FORMAT = "yyyy-MM-dd";
  public static final String TAX_NAME = "TaxName";
  public static final String TAX = "Tax";
  public static final String TAX_PERCENTAGE = "TaxPercentage";
  public static final String CALL_CENTER = "Call Center";

  public static final String GET_ORDER_LINE_LIST_TEMP = "<OrderLineList><OrderLine OrderLineKey='' MaxLineStatus='' MinLineStatus=''>"
      + "<OrderStatuses><OrderStatus ShipNode='' StatusDescription='' /></OrderStatuses></OrderLine></OrderLineList>";
  public static final String API_CHANGE_RELEASE = "changeRelease";
  public static final String API_GET_ATP = "getATP";
  public static final String API_CHANGE_ORDER_STATUS = "changeOrderStatus";
  public static final String GC_SEND_ORDERS_TO_DAX_SERVICE = "GCSendOrdersToDAXService";
  public static final String GC_APPLY_DAX_ALLOCATION_HOLD_SERVICE = "GCApplyDAXAllocationHoldService";
  public static final String API_SPLIT_LINE = "splitLine";
  public static final String BACKORDERED_DAX = "1300.10";
  /***************** Order Schedule and Release Constants - END *********************/

  // Payment Constants begin
  public static final String IX_ACTION_CODE = "IxActionCode";
  public static final String IX_CARD_TYPE = "IxCardType";
  public static final String IX_ACCOUNT = "IxAccount";
  public static final String IX_EXPDATE = "IxExpDate";
  public static final String IX_AMOUNT = "IxAmount";
  public static final String IX_DATE = "IxDate";
  public static final String IX_TIMEOUT = "IxTimeOut";
  public static final String IX_TIME = "IxTime";
  public static final String IX_AUTH_CODE = "IxAuthCode";
  // OMS-3814 START
  public static final String IX_ISO_RESP = "IxIsoResp";
  public static final String OFFLINE = "OFFLINE";
  public static final String PAYMENT_CALL_FOR_AUTH = "2";
  public static final String REFERRAL_CODE = "4";
  public static final String REENTER_CODE = "6";
  public static final String PAYMENT_DECLINE_MSG = "Payment Declined and Suspended.";
  public static final String PAYMENT_COMMUNICATION_FAILURE_MSG = "Payment Gateway communication error. Click Confirm again to create this order without Auth(s).";
  public static final String PAYMENT_REFERRAL_MSG = "System authorization attempt failed. Contact Verifications for manual authorization.";
  public static final String PAYMENT_REENTER_MSG = "Payment entry error. Re-enter card information.";
  // OMS- 3814 END
  public static final String PAYMENT_AUTHORIZED = "0";
  public static final String PAYMENT_DECLINED = "1";
  public static final String BANK_DOWN = "3";
  public static final String TIMED_OUT = "30";
  public static final String TRY_LATER = "8";
  public static final String AUTH_RETURN_MSG = "IxReceiptDisplay";
  public static final String CREDIT_ON_ACCOUNT = "CREDIT_ON_ACCOUNT";
  public static final String CREDIT = "credit";
  public static final String DEBIT_CARD = "DC";
  public static final String IX_DEBIT_CREDIT = "IxDebitCredit";
  public static final String CC_AJB_REQUEST = "XMLAJBFipayRequest";
  public static final String IX_CMD = "IxCmd";
  public static final String AUTHORIZATION_ID = "100";
  public static final String INVOKE_AJB_PAYMENT_SERVICE = "AJB";
  public static final String INVOKE_BML_PAYMENT_SERVICE = "BML";
  public static final String INVOKE_PAYPAL_PAYMENT_SERVICE = "PAYPAL";
  public static final String INVOKE_DAX_PAYMENT_SERVICE = "DAX";
  public static final int MAXIMUM_RETRY = 2;
  public static final double ZEROAMOUNT = 0.00;
  public static final String FALSE_FLAG = "F";
  public static final boolean FALSE = false;
  public static final String BLANK = "";
  public static final String PAYMENT_STATUS_AUTHORIZATION = "AUTHORIZATION";
  public static final String PAYMENT_STATUS_CHARGE = "CHARGE";
  public static final String PAYMENT_SYS_ERROR_ELE = "XmlFaultReply";
  public static final String PAYMENT_SYS_ERROR_RET_CODE = "SYS_ERROR";
  public static final String APPROVED = "APPROVED";
  public static final String AUTH_SUCCESS_MSG = "AUTH_SUCCESS";
  public static final String DECLINED = "Declined";
  public static final String HOLD = "Hold";
  public static final String AUTH_SUCCESS = "AuthSuccess";
  public static final String SALES_ORDER_DOCUMENT_TYPE = "0001";
  public static final String GET_ORDER_DETAILS = "getOrderDetails";
  public static final String PAYMENT_GATEWAYS_COMMON_CODE = "GCPaymentGateway";
  public static final String USA_CURRENCY = "USD";
  public static final String GET_AJB_PAYMENT_REQUEST_XML = "/global/template/api/extn_AJBPaymentRequest.xml";
  public static final String AJB_AUTH_REQUEST_CODE = "100";
  public static final String IX_STORE_NUMBER = "IxStoreNumber";
  public static final String IX_TERMINAL_NUMBER = "IxTerminalNumber";
  public static final String IX_TRAN_TYPE = "IxTranType";
  public static final String SALE = "Sale";
  public static final String ENCYPTION_KEY_PATH = "/global/resources/truststore.jks";
  public static final String DECYPTION_KEY_PATH = "/global/resources/keystore.jks";
  public static final String KEYSTORE_CERTIFICATE_KEY = "certificatekey";
  public static final String RSA = "RSA";
  public static final String REQUEST_AMOUNT = "requestAmount";
  public static final String GIFT_CARD = "GIFT_CARD";
  public static final String REDEEM_GIFT_CARD = "Redeem";
  public static final String CREDIT_CARD = "CREDIT_CARD";
  public static final String IX_INVOICE = "IxInvoice";
  public static final String IX_OPTIONS = "IxOptions";
  public static final String PRIVATE_LABEL_CARD = "PRIVATE_LABEL_CARD";
  public static final String PRIVATE_LABEL = "PrivateLabel";
  public static final String CC_STORE_NUMBER = "CC_STORE_NUMBER";
  public static final String TERMINAL_NUMBER = "OMS";
  public static final String CSV_TEMPLATE_STRING = "$#$IxCmd$#$,$#$IxMultiLayeredSTAN$#$,$#$Reserved$#$,$#$IxActionCode$#$,$#$IxTimeOut$#$,$#$IxDebitCredit$#$,$#$IxTermid$#$,$#$IxStoreNumber$#$,$#$IxTerminalNumber$#$,$#$IxTranType$#$,$#$Reserved$#$,$#$IxCardType$#$,$#$IxAccount$#$,$#$IxExpDate$#$,$#$IxSwipe$#$,$#$IxAmount$#$,$#$IxInvoice$#$,$#$IxTranLanguage$#$,$#$IxForceAuthCode$#$,$#$IxOrgAmount$#$,$#$IxOptions$#$,$#$IxBankUserData$#$,$#$IxPinPadID$#$,$#$IxOperator$#$,$#$IxIssueNumber$#$,$#$IxTotalSalesTaxAmount$#$,$#$IxChequeType$#$,$#$IxChequeProvince$#$,$#$IxChequeDriverLic$#$,$#$IxChequeDateOfBirth$#$,$#$IxMailOrderAVSData$#$,$#$IxStoreAddress$#$,$#$IxStoreAddress1$#$,$#$IxPosEchoField$#$,$#$Reserved$#$,$#$IxSpdhCode$#$,$#$IxAuthCode$#$,$#$IxReceiptDisplay$#$,$#$IxCrMerchant$#$,$#$IxDbMerchant$#$,$#$IxDefaultTimeout$#$,$#$Reserved$#$,$#$IxRetCode$#$,$#$IxPS2000$#$,$#$IxRefNumber$#$,$#$IxSeqNumber$#$,$#$IxBatchNumber$#$,$#$IxPostingDate$#$,$#$IxLanguage$#$,$#$IxDate$#$,$#$IxTime$#$,$#$IxDepositData$#$,$#$IxIsoResp$#$,$#$IxBankNodeID$#$,$#$IxAuthResponseTime$#$,$#$IxProductInfo$#$,$#$Reserved$#$,$#$IxDebitComBaudRate$#$,$#$Reserved$#$,$#$Reserved$#$,$#$IxAdditionalMesg$#$,$#$Reserved$#$,$#$IxResponseTimeMilliSeconds$#$,$#$IxResponseDateTimeMilli$#$,$#$Reserved$#$,$#$Reserved$#$,$#$Reserved$#$,$#$Reserved$#$,$#$Reserved$#$,$#$Reserved$#$,$#$Reserved$#$,$#$Reserved$#$,$#$Reserved$#$,$#$Reserved$#$,$#$Reserved$#$,$#$IxMaxTry$#$,$#$IxCurrentTry$#$,$#$Reserved$#$,$#$IxMatchTran$#$,$#$IxNeedsReversal$#$,$#$Reserved$#$,$#$IxPrnFile$#$,$#$Reserved$#$,$#$Reserved$#$,$#$IxPinBlock$#$";
  public static final String AJB_IP = "AJB_IP";
  public static final String PORT_NO = "PORT_NO";
  public static final String AJB_CC_NODE_IP = "AJB_CC_NODE_IP";
  public static final String CC_PORT_NO = "CC_PORT_NO";
  public static final String AJB_WEB_NODE_IP = "AJB_WEB_NODE_IP";
  public static final String WEB_PORT_NO = "WEB_PORT_NO";
  public static final String NODE_ID = "NODE_ID";
  public static final String RANDOM_NO = "RANDOM_NO";
  public static final String CODE_SHORT_DESCRIPTION = "CodeShortDescription";
  public static final String ORDER_LIST_EXTN_CC_AUTH_RETRY_XPATH = "Order/Extn/GCAuthRetryList/GCAuthRetry";
  public static final String GC_AUTH_RETRY_ELEMENT = "GCAuthRetry";
  public static final String PAYMENT_KEY = "PaymentKey";
  public static final String AUTH_RETRY_COUNT = "AuthRetryCount";
  public static final String STRING_ONE = "1";
  public static final String GC_CREATE_CC_AUTH_RETRY = "GCCreateCCAuthRetry";
  public static final String CC_AUTH_RETRY_KEY = "GCAuthRetryKey";
  public static final String GC_CHANGE_CC_AUTH_RETRY = "GCChangeCCAuthRetry";
  public static final String REQUEST_TYPE = "requestType";
  public static final String AUTH = "AUTH";
  public static final String BALANCE_CHECK = "BALANCECHECK";
  public static final String BALANCE_INQUIRY = "balanceInquiry";
  public static final String METHOD_PARAMETERS = "MethodParameters";
  public static final String SYSTEM_ID = "systemId";
  public static final String DAX = "DAX";
  public static final String PAYPAL_CLIENT_ID = "PAYPAL_CLIENT_ID";
  public static final String PAYPAL_CLIENT_SECRET = "PAYPAL_CLIENT_SECRET";
  public static final String PAYPAL = "PAYPAL";
  public static final String BML = "BML";
  public static final String DAX_BALANCE_CHECK_INPUT_XML_PATH = "/global/template/api/extn_DAXBalanceCheck.xml";
  public static final String OMS = "OMS";
  public static final String BML_BALANCE_CHECK_INPUT_XML_PATH = "/global/template/api/extn_BMLReAuthRequest.xml";
  public static final String REQUEST_COLLECTION = "requestCollection";
  public static final String EXECUTE_COLLECTION = "executeCollection";
  public static final String PROCESS_ORDER_PAYMENTS = "processOrderPayments";
  public static final String PAYMENT_TYPE = "PaymentType";
  public static final String CALLING_ORGANIZATION_CODE = "CallingOrganizationCode";
  public static final String NEW_REFUND_PAYMENT_TYPE = "NewRefundPaymentType";
  public static final String PAYMENT_TYPE_GROUP = "PaymentTypeGroup";
  public static final String API_GET_PAYMENT_TYPE_LIST = "getPaymentTypeList";
  public static final String OTHER = "OTHER";
  public static final String PAYMENT_HOLD = "PAYMENT_HOLD";
  public static final String CHARGE_TRANSACTION_KEY = "ChargeTransactionKey";
  public static final String RECORD_COLLECTION = "recordCollection";
  public static final String GC_PROPERTIES = "gc_config";
  public static final String PAYMENT_REQUEST_AMOUNT = "RequestAmount";
  public static final String AMOUNT_COLLECTED = "AmountCollected";
  public static final String PRICE_INFO = "PriceInfo";
  public static final String CHANGE_IN_TOTAL_AMOUNT = "ChangeInTotalAmount";
  public static final String ENTRY_TYPE = "EntryType";
  public static final String PAYMENT_METHOD = "PaymentMethod";
  public static final String EXTN_DAX_CUSTOMER_ID = "ExtnDAXCustomerID";
  public static final String EXTN_IS_REFUND_ON_ACCOUNT = "ExtnIsRefundOnAccount";
  public static final String ORDER_INVOICE_KEY = "OrderInvoiceKey";
  public static final String GET_ORDER_INVOICE_DETAILS = "getOrderInvoiceDetails";
  public static final String PAYMENT_ALERT = "GCPaymentAlerts";
  public static final String GC_PUBLISH_INVOICE = "GCPublishInvoice";
  public static final String GC_DAX_TO_POS_COA_TRANSFER_SERVICE = "GCDAXToPOSCOATransferService";
  public static final String PAYMENT_DATE_TIME_FORMAT = "yyyyMMddHHmmss";
  public static final String PAYMENT_HARD_DECLINED_ALERT = "GCPaymentHardDeclineAlerts";
  public static final String CHARGE_TYPE = "ChargeType";
  public static final String AUTH_EXPIRY_DATE = "AuthorizationExpirationDate";
  public static final String PP_TS_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
  public static final String OPEN_AUTH_AMT = "OpenAuthorizedAmount";
  public static final String CHECKED = "CHECKED";
  public static final String PAYMENT_METHPD_XPATH = "ChargeTransactionDetail/PaymentMethod";
  public static final String SUS_ANY_MORE_CHARGES = "SuspendAnyMoreCharges";
  public static final String GC_CC_AUTH_GET_ORDER_LIST_TEMPLATE = "/global/template/api/extn_CCAuthGetOrderList.xml";
  public static final String CHARGE_TRANSACTION_DETAIL_XPATH = "Order/ChargeTransactionDetails/ChargeTransactionDetail";
  public static final String PLCC = "PRIVATE_LABEL_CARD";
  public static final String ORDER_PAYMENT_METHPD_XPATH = "Order/PaymentMethods/PaymentMethod";
  public static final String CREDIT_CARD_EXPIRY_DATE = "CreditCardExpDate";
  public static final String CC_EXPIRATION_DATE_FORMAT = "MMyy";
  public static final String REQUEST_AMOUNT_PAYMENT = "RequestAmount";
  public static final String CC_TRANSACTION_XPATH = "ChargeTransactionDetail/CreditCardTransactions/CreditCardTransaction";
  public static final String AUTH_AVS = "AuthAvs";
  public static final String AUTH_CODE = "AuthCode";
  public static final String REQUEST_ID = "RequestId";
  public static final String AUTH_RETURN_CODE = "AuthReturnCode";
  public static final String AUTH_RETURN_FLAG = "AuthReturnFlag";
  public static final String AUTH_RETURN_MESSAGE = "AuthReturnMessage";
  public static final String INTERNAL_RETURN_FLAG = "InternalReturnFlag";
  public static final String AUTH_ID = "AuthorizationID";
  public static final String DUMMY_AUTH_TYPE = "Dummy Auth type";
  public static final String CVV_AUTH_CODE = "CVVAuthCode";
  public static final String TRAN_TYPE = "TranType";
  public static final String TRAN_RETURN_CODE = "TranReturnCode";
  public static final String TRAN_RETURN_MESSAGE = "TranReturnMessage";
  public static final String VOIDED = "Voided";
  public static final String PENDING = "Pending";
  public static final String PAYPAL_VOID_XPATH = "Order[@MaxOrderStatus='9000' or @MinOrderStatus>='3700']";
  public static final String PAYPAL_PROPERTIES = "PayPal_Config";
  public static final String PAYMENT_SERVICE_UNAVAILABLE = "GCPaymentCommunicationFail";
  public static final String GIFTCARD = "GIFTCARD";
  public static final String AUTHORIZED_BML = "00";
  public static final String CHARGED_BML = "1";
  public static final String IX_ISSUE_NUMBER = "IxIssueNumber";
  public static final String IX_MAIL_ORDER_AVSDATA = "IxMailOrderAVSData";
  public static final String CREDIT_CARD_TYPE = "CreditCardType";
  public static final String PAYMENT_PROCESSED_AMOUNT = "ProcessedAmount";
  public static final String WEB_STORE_NUMBER = "WEB_STORE_NUMBER";
  public static final String PAYMENT_CALL_CALL_CENTER = "ISCCSSYS001";
  public static final String PAYMENT_METHODS = "PaymentMethods";
  public static final String PAYMENT_REFERENCE_5 = "PaymentReference5";
  public static final String CC_NODE_ID = "CC_NODE_ID";
  public static final String WEB_NODE_ID = "WEB_NODE_ID";
  public static final String AUTHORIZATION = "AUTHORIZATION";
  public static final String GC_REMOVE_CVV = "GCRemoveCVV";
  public static final String IX_REFERENCE_NUMBER = "IxRefNumber";
  public static final String DEBITACKREQ = "DebitAckReq";
  public static final String CHARGE_RETRY_COUNT = "ChargeRetryCount";
  public static final String DECLINED_BML = "0";
  public static final String REFUND = "Refund";
  public static final String RECORD_EXTERNAL_CHARGES = "recordExternalCharges";
  public static final String GIFT_CARD_AJB = "GiftCard";
  public static final String FROM_STATUS = "FromStatus";
  public static final String MAX_CHARGE_LIMIT = "MaxChargeLimit";
  public static final String SUSPEND_ANY_MORE_CHARGES = "SuspendAnyMoreCharges";
  public static final String GC_CC_AUTH_GET_ORDER_LIST_TEMPLATE_WITHOUT_CHARGE_TRASACTION_DEATIL = "/global/template/api/extn_CCGetOrderList.xml";
  public static final String CARD_TYPE_DISCOVER = "DISCOVER";
  public static final String IX_PS_2000 = "IxPS2000";
  public static final String CREATE_ORDER_WITHOUT_AUTH = "CREATE_ORDER_WITHOUT_AUTH";
  public static final String PAYMENT_REFERENCE_3 = "PaymentReference3";
  // Payment Constant End

  // Base Config Constants Start
  public static final String EXTN_JOB_CODE = "ExtnJobCode";
  public static final String ACTION_CREATE = "Create";
  public static final String ACTION_MODIFY = "Modify";
  public static final String ACTION_DELETE = "Delete";
  public static final String GC_LOGINID_USER_GROUP_SERVICE = "GCLoginIDUserGroupService";
  public static final String LOGINID = "Loginid";
  public static final String USER_GROUP = "UserGroup";
  public static final String EXTN_STORE_ID = "ExtnStoreID";
  public static final String EXTN_MANAGER_LEVEL_CODE = "ExtnManagerLevelCode";
  public static final String EXTN_STORE_MENU_LEVEL = "ExtnStoreMenuLevel";
  public static final String MENU_ID = "MenuId";
  public static final String USER_GROUP_LISTS = "UserGroupLists";
  public static final String USER_GROUP_LIST = "UserGroupList";
  public static final String USER_GROUP_ID = "UsergroupId";
  public static final String API_CREATE_USER_HIERARCHY = "createUserHierarchy";
  public static final String GC_USER_GROUP_SERVICE = "GCUserGroupService";
  public static final String GC_LOGINID_USERGROUP = "GCLoginidUsergroup";
  public static final String GC_USERGROUP = "GCUsergroup";
  public static final String GET_USER_LIST = "getUserList";
  public static final String USERGROUP_ID = "UsergroupId";
  public static final String API_MODIFY_USER_HIERARCHY = "modifyUserHierarchy";
  public static final String PASSWORD_VALUE = "password";
  public static final String PASSWORD_ATTRIBUTE = "Password";
  public static final String RESET = "Reset";
  public static final String API_DELETE_USER_HIERARCHY = "deleteUserHierarchy";
  public static final String DATA_SECURITY_GROUP_ID = "DataSecurityGroupId";

  // Base Config Constants end

  /***************** UPS - Begin ****************************/
  public static final String GC_UPS_URL = "gc.ups.service.url";
  public static final String GC_UPS_SOAP_ACTION = "gc.ups.soapaction";
  /***************** UPS - Begin ****************************/
  // Mass operation Begin
  public static final String STATUS_CREATED = "1100";
  public static final String STATUS_SCHEDULED = "1500";
  public static final String STATUS_BACKORDERED = "1300";
  public static final String CREATED = "Created";
  public static final String SCHEDULED = "Scheduled";
  public static final String BACKORDERED = "Backordered";
  public static final String ORDER_DATE_QRY_TYPE = "OrderDateQryType";
  public static final String DATERANGE = "DATERANGE";
  public static final String FROM_ORDER_DATE = "FromOrderDate";
  public static final String TO_ORDER_DATE = "ToOrderDate";
  public static final String GET_ORDER_LINE_LIST_TEMP_FOR_MASS_OPERATION = "global/template/api/getOrderLineListTempForMassOperation";
  public static final String HAS_PROMOTION = "HasPromotion";
  public static final String SUBSTITUTE = "SUBSTITUTE";
  public static final String ORDER_LINE_TRAN_QUANTITY = "OrderLineTranQuantity";
  public static final String GET_ITEM_PRICE_TEMP_FOR_MASS_OPERATION = "global/template/api/getItemPricepForMassOperation";
  public static final String INT_TWO = "2";
  public static final String NOTES = "Notes";
  public static final String NOTE = "Note";
  public static final String NOTE_TEXT = "NoteText";
  public static final String PRIORITY = "Priority";
  public static final String REASON_CODE = "ReasonCode";
  public static final String VISIBLE_TO_ALL = "VisibleToAll";
  public static final String INT_TWENTY_TWO = "22";
  public static final String GC_MASS_OPERATION_POST_SERVICE = "GCMassOperationPostService";
  public static final String INBOX = "Inbox";
  public static final String EXCEPTION_TYPE = "ExceptionType";
  public static final String QUEUE_ID = "QueueId";
  public static final String INBOX_REFERENCE_LIST = "InboxReferencesList";
  public static final String INBOX_REFERENCES = "InboxReferences";
  public static final String REFERENCE_TYPE = "ReferenceType";
  public static final String RESULT_MESSAGE = "ResultMessage";
  public static final String TEXT = "TEXT";
  public static final String API_CREATE_EXCEPTION = "createException";
  public static final String MASS_ITEM_SUB_RESULT_MESSAGE = "Mass item substitution failed";
  public static final String MASS_ITEM_SUB_EXCEPTION_TYPE = "GCMassItemSubstitution";
  public static final String MASS_ITEM_SUB_QUEUE_ID = "GC_MASS_ITEM_SUBSTITUTION_ALERT_QUEUE";
  public static final String MASS_ITEM_CANCELLATION_RESULT_MESSAGE = "Mass item cancellation failed";
  public static final String MASS_ITEM_CANCELLATION_EXCEPTION_TYPE = "GCMassItemCancellation";
  public static final String MASS_ITEM_CANCELLATION_QUEUE_ID = "GC_MASS_ITEM_CANCELLATION_ALERT_QUEUE";
  public static final String FROM_UNIT_PRICE = "FromUnitPrice";
  public static final String TO_UNIT_PRICE = "ToUnitPrice";
  public static final String GET_ORDER_DETAILS_TEMP_FOR_CONFIRM_APPEASEMENT = "global/template/api/getOrderDetailsTempForConfirmAppeasement";

  // Mass operation End

  public static final String ITEM_SUB_RESULT_MESSAGE = "Item substitution failed";
  public static final String ITEM_SUB_EXCEPTION_TYPE = "GCItemSubstitution";
  public static final String ITEM_SUB_QUEUE_ID = "GC_ITEM_SUBSTITUTION_ALERT_QUEUE";
  // Item substitution End

  public static final String PRORATION_RATIO = "PromotionRatio";
  public static final String PRORATED_CHARGE = "ProratedCharge";

  // Order Sync Start

  public static final String CREDIT_CARD_NO = "CreditCardNo";
  public static final String YCD_CUSTOMER_APPEASE = "YCD_CUSTOMER_APPEASE";
  public static final String YCD_ADD_MODIFY_CHARGES = "YCD_ADD_MODIFY_CHARGES";
  public static final String MODIFICATION_FULFILLMENT_TYPE = "FULFILLMENT_TYPE";
  public static final String MODIFICATION_PRICE = "PRICE";
  public static final String MODIFICATION_SCREEN_NAME_PRICE = "Change Price";
  public static final String MODIFICATION_CANCEL = "CANCEL";
  public static final String MODIFICATION_SCREEN_NAME_CANCEL = "Cancel";
  public static final String MODIFICATION_SHIPTO = "SHIPTO";
  public static final String MODIFICATION_SCREEN_NAME_SHIPTO = "Change Ship To";
  public static final String MODIFICATION_PAYMENT_METHOD = "PAYMENT_METHOD";
  public static final String MODIFICATION_SCREEN_NAME_PAYMENT_METHOD = "Change Payment Method";
  public static final String MODIFICATION_CARRIER_SERVICE_CODE = "CARRIER_SERVICE_CODE";
  public static final String MODIFICATION_SCREEN_NAME_CARRIER_SERVICE_CODE = "Change Carrier Service Code";
  public static final String ORDER_AUDIT_MODIFICATION_TYPE_XPATH = "Order/OrderAudit/OrderAuditLevels/OrderAuditLevel/ModificationTypes/ModificationType";
  public static final String ORDER_AUDIT_ATTRIBUTE_XPATH = "Order/OrderAudit/OrderAuditLevels/OrderAuditLevel/OrderAuditDetails/OrderAuditDetail/Attributes/Attribute";
  public static final String NEW_VALUE = "NewValue";
  public static final String OLD_VALUE = "OldValue";
  public static final String ORDERLIST_ORDER_PAYMENT_METHPD_XPATH = "OrderList/Order/PaymentMethods/PaymentMethod";
  public static final String PAYMENT_REFERENCE4_CVV = "PaymentReference4";
  public static final String SVC_NO = "SvcNo";
  public static final String MODIFICATION_TYPE_NAME = "Name";
  public static final String MODIFICATION_TYPE_SCREEN_NAME = "ScreenName";
  public static final String ATTRIBUTE_NAME = "Name";

  // Order Sync End
  // Shipment Constants Start
  public static final String SHIPMENT_LINES = "ShipmentLines";
  public static final String SHIPMENT_LINE = "ShipmentLine";
  public static final String DC = "DC";
  public static final String SHIPPING_PROMOTION = "ShippingPromotion";
  public static final String EXTN_FTC_EXEMPT = "ExtnFTCExempt";
  public static final String SHIPMENT_NO = "ShipmentNo";
  public static final String BACKORDER_NON_SHIPPED_QTY = "BackOrderNonShippedQuantity";
  public static final String STATUS_QUANTITY = "StatusQuantity";
  public static final String BACKORDER = "BACKORDER";
  public static final String SHIPMENT_CANCEL_HOLD = "SHIPMENT_CANCEL_HOLD";
  public static final String PO_DOCUMENT_TYPE = "0005";
  public static final String SHORTAGE_QTY = "ShortageQty";
  public static final String SHIPPING_TAX = "ShippingTax";
  public static final String ONE = "1";
  public static final String PRODUCT_AVAIL_DATE = "ProductAvailDate";
  public static final String CONSUMED_QUANTITY = "ConsumedQuantity";
  public static final String FIRST_SHIP_DATE = "FirstShipDate";
  public static final String REGULAR = "REGULAR";
  public static final String ZERO = "0";
  public static final String SHIP_ORDER = "SHIP_ORDER";
  public static final String CHAIN_TYPE = "ChainType";
  public static final String SUCCESS = "Success";
  public static final String FAILURE = "Failure";
  public static final String CHANGE_RELEASE_STATUS_TRAN_ID = "STOP_SHIPMENT_REQUEST.0001.ex";
  public static final String STOP_SHIPMENT_TRAN_ID = "STOP_SHIPMENT.0001.ex";
  public static final String STOP_SHIPMENT_REQUESTED_STATUS = "3200.05";
  public static final String SHIPMENT_STOPPED_STATUS = "1300.06";
  public static final String STOP_SHIPMENT_FAILED_STATUS = "3200.07";
  public static final String ORDER_RELEASE_DETAIL = "OrderReleaseDetail";
  public static final String CHANGE_IN_QUANTITY = "ChangeInQuantity";
  public static final String OVERSIZE = "Oversize";
  public static final String RETURN_ORDER_DOCUMENT_TYPE = "0003";
  public static final String TAX_EXEMPT_FLAG = "TaxExemptFlag";
  public static final String REQ_SHIP_DATE = "ReqShipDate";
  public static final String IS_FIRM_PREDEFINED_NODE = "IsFirmPredefinedNode";
  public static final String MFI = "MFI";

  public static final String API_CREATE_SHIPMENT = "createShipment";
  public static final String API_CONFIRM_SHIPMENT = "confirmShipment";
  public static final String API_GET_ORGANIZATION_LIST = "getOrganizationList";
  public static final String API_GET_ORDER_INVOICE_DETAIL_LIST = "getOrderInvoiceDetailList";
  public static final String GC_PROCESS_SHPG_PRORATION_SERVICE = "GCProcessShippingProrationService";
  // Shipment Constant End

  // Customer Sync Start
  public static final String DATE_OF_BIRTH = "DateOfBirth";
  public static final String IS_DEFAULT_BILLTO = "IsDefaultBillTo";
  public static final String CUSTOMER_TYPE = "CustomerType";
  public static final String CUSTOMER_OVERRIDES = "customer_overrides";
  public static final String MANAGE_CUSTOMER_API = "manageCustomer";
  public static final String IS_BILLTO = "IsBillTo";
  public static final String CANCELLED_ORDER_STATUS_CODE = "9000";
  public static final String CONSUMER_CUSTOMER_CODE = "02";
  // Customer Sync End

  // Return Constants Start
  public static final String RETURN_WITH_ADV_CREDIT = "RTRN_WITH_ADVCREDIT";
  public static final String ORDER_TYPE = "OrderType";
  public static final String CREDIT_MEMO = "CREDIT_MEMO";
  public static final String LINE_DETAIL = "LineDetail";
  public static final String LINE_CHARGE_LIST = "LineChargeList";
  public static final String LINE_TAX_LIST = "LineTaxList";
  public static final String RECORD_INVOICE_CREATION = "recordInvoiceCreation";
  public static final String ZCR_RTN_DAYS = "ZCRReturnWindow";
  public static final String TS_FORMAT_NO_TIMEZONE = "yyyy-MM-dd'T'HH:mm:ss";
  public static final String DEFAULT_ORGANIZATION = "DEFAULT";
  public static final String COMMON_CODE_XPATH = "CommonCodeList/CommonCode";
  public static final String DEBIT_MEMO = "DEBIT_MEMO";
  public static final String STORE_RETURN = "STORE_RETURN";
  public static final String GET_ITEM_LIST = "getItemList";
  public static final String BUNDLE_PARENT_ORDERLINEKEY = "BundleParentOrderLineKey";
  public static final String GET_ORDER_LINE_LIST = "getOrderLineList";
  public static final String RECEIVED = "3900";
  public static final String CREATE_ORDER_INVOICE = "createOrderInvoice";
  public static final String ITEM_DESCRIPTION = "ItemDesc";
  public static final String ITEM_SHORT_DESCRIPTION = "ItemShortDesc";
  public static final String RETURN_WITH_ADV_EXCHANGE = "RTRN_WITH_ADVEXCHNG";
  public static final String ORDER_PURPOSE = "OrderPurpose";
  public static final String EXCHANGE = "EXCHANGE";
  public static final String SHIPPED_QUANTITY = "ShippedQty";
  public static final String MONITORCONSOLIDATION_ORDERHEADERKEY_XPATH = "MonitorConsolidation/Order/@OrderHeaderKey";
  public static final String MONITORCONSOLIDATION_ORDER_STATUS_XPATH = "MonitorConsolidation/Order/OrderStatuses/OrderStatus";
  public static final String CLOSE_RECEIPT = "closeReceipt";
  public static final String ADVANCE_CREDIT = "ADVANCE_CREDIT";
  public static final String INTL_WEB = "INTL_WEB";
  public static final String DEBIT_ON_CANCEL = "DEBIT_ON_CANCEL";
  public static final String DEBIT_ON_INVOICE = "DEBIT_ON_INVOICE";
  public static final String REFERENCES = "References";
  public static final String REFERENCE = "Reference";
  public static final String GET_ORDER_AUDIT_LIST_TEMPLATE = "/global/template/api/getOrderAuditList.xml";
  public static final String ORDER_DATES = "OrderDates";
  // Return Constants End

  // Email Component Start
  public static final String EMAIL_TYPE = "EmailType";
  public static final String GC_PUBLISH_TO_EMAIL_ORDER_CONFIRMATION_INBOUND_QUEUE = "GCPublishToEmailOrderConfirmationInboundQueue";
  public static final String GC_PUBLISH_TO_EMAIL_SHIPPING_CONFIRMATION_INBOUND_QUEUE = "GCPublishToEmailShippingConfirmationInboundQueue";
  public static final String GC_PUBLISH_TO_EMAIL_ORDER_CANCELLATION_INBOUND_QUEUE = "GCPublishToEmailOrderCancellationInboundQueue";
  public static final String GC_PUBLISH_TO_EMAIL_RETURNRECIEVED_NOTIFICATION_INBOUND_QUEUE = "GCPublishToEmailReturnRecievedNotificationInboundQueue";
  public static final String GC_PUBLISH_TO_EMAIL_BACKORDER_NOTIFICATION_INBOUND_QUEUE = "GCPublishToEmailBackOrderNotificationInboundQueue";
  public static final String GC_PUBLISH_TO_EMAIL_RETURN_AUTHORIZATION_INBOUND_QUEUE = "GCPublishToEmailReturnAuthorizationInboundQueue";
  public static final String GC_PUBLISH_TO_EMAIL_FRAUD_VERIFICATION_INBOUND_QUEUE = "GCPublishToEmailFraudVerificationInboundQueue";
  public static final String GC_ORDER_CONFIRMATION_EMAIL_SERVICE = "GCOrderConfirmationEmailService";
  public static final String GC_BACKORDER_NOTIFICATION_EMAIL_SERVICE = "GCBackOrderNotificationEmailService";
  public static final String GC_SHIPMENT_CONFIRMATION_EMAIL_SERVICE = "GCShipmentConfirmationEmailService";
  public static final String GC_FRAUD_REVIEW_HOLD_EMAIL_SERVICE = "GCFraudReviewHoldEmailService";
  public static final String GC_RETURN_ACCEPTANCE_EMAIL_SERVICE = "GCReturnAcceptanceEmailService";
  public static final String GC_PAYMENT_DECLINE_EMAIL_SERVICE = "GCPaymentDeclineEmailService";
  public static final String GC_BACKORDER_NOTIFICATION_XSLT = "GCBackOrderNotificationEmailXSLT";
  public static final String GC_ORDER_CANCELLATION_XSLT = "GCOrderCancellationEmailXSLT";
  public static final String GC_ORDER_CONFIRMATION_XSLT = "GCOrderConfirmationEmailXSLT";
  public static final String GC_RETURN_ACCEPTANCE_XSLT = "GCReturnAcceptanceEmailXSLT";
  public static final String GC_RETURN_RECEIVED_XSLT = "GCReturnReceivedEmailXSLT";
  public static final String GET_SHIPMENT_LIST_FOR_ORDER = "getShipmentListForOrder";
  public static final String GET_SHIPMENT_DTL = "getShipmentDetails";
  public static final String GC_GET_INVOICE_DETAILS_SERVICE = "GCGetOrderInvoiceDetails";
  public static final String SHIPMENT_KEY = "ShipmentKey";
  public static final String GC_CREDITCARD_DECLINED_EMAIL_NOTE = "Credit Card Declined Email Sent to the customer";
  public static final String GC_BACKORDER_NOTIFICATION_EMAIL_NOTE = "BackOrder Notification Email Sent to the customer";
  public static final String GC_FRAUD_REVIEW_HOLD_EMAIL_NOTE = "Fraud Pending Review Hold Email Sent to the customer";
  public static final String GC_ORDER_CANCELLATION_EMAIL_NOTE = "Order Cancellation Email Sent to the Customer";
  public static final String GC_ORDER_CONFIRMATION_EMAIL_NOTE = "Order Confirmation Email Sent to the Customer";
  public static final String GC_RETURN_ACCEPTANCE_EMAIL_NOTE = "Return Confirmation Email Sent to the customer";
  public static final String GC_RETURN_RECIEVED_EMAIL_NOTE = "Return Received Email Sent to the Customer";
  public static final String GC_SHIPMENT_CONFIRMATION_EMAIL_NOTE = "Shipment Confirmation Email Sent to the Customer";
  public static final String GC_RETURN_RECEIVED_FOR_NPR_XSLT = "GCReturnReceivedEmailForNPRXSLT";
  public static final String GET_ORDER_LINE_LIST_TEMP_EMAIL = "<OrderLineList><OrderLine>"
      + "<Order DocumentType='' MaxOrderStatus='' OrderHeaderKey=''/></OrderLine></OrderLineList>";
  public static final String ZERO_ZERO_ZERO_THREE = "0003";
  public static final String CREATED_STATUS = "1100";
  public static final String RECEIVED_STATUS = "3900";
  public static final String GET_RECEIPT_LIST = "getReceiptList";
   //6985 Start

  public static final String AWAIT_AUTH_AMOUNT = "AwaitingAuthInterfaceAmount";
  public static final String AWAIT_CHARGE_AMOUNT = "AwaitingChargeInterfaceAmount";
  public static final String TOTAL_AUTHORIZED = "TotalAuthorized";
  public static final String TOTAL_CHARGED = "TotalCharged";
    //6985 End
  
  // Email Component End
  public static final String COMPUTED_PRICE = "ComputedPrice";
  public static final String EXTENDED_DISPLAY_DESCRIPTION = "ExtendedDisplayDescription";

  // Backorder DAX Allocation Start
  public static final String CREATED_DAX_STATUS = "1100.10";
  public static final String CREATED_DAX_ALLOCATED_STATUS = "1100.20";
  public static final String BACKORDERED_DAX_STATUS = "1300.10";
  public static final String BACKORDERED_OMS_STATUS = "1300.10";
  public static final String BACKORDERED_DAX_ALLOCATED_STATUS = "1300.20";
  public static final String SCHEDULED_PO_DAX_STATUS = "1500.10";
  public static final String SCHEDULED_PO_DAX_ALLOCATED_STATUS = "1500.20";
  public static final String AWAITING_CHAINED_ORDER_STATUS = "1600";
  public static final String CHANGE_CREATE_STATUS = "CHANGE_STATUS_CREATE.0001.ex";
  public static final String CHANGE_SCHEDULE_STATUS = "CHANGE_STATUS_SCHEDULE.0001.ex";
  public static final String CHANGE_CONFIRM_STATUS = "CHANGE_STATUS_CONFIRM.0001.ex";
  public static final String IS_CREATED_DAX_STATUS = "IsCreatedDAXStatus";
  public static final String HOLDS_RESTRICTING_SCHEDULE_RELEASE = "HoldsRestrictSchRel";
  public static final String GC_SOURCE_CODE = "GCSourceCode";
  public static final String SOURCE_CODE = "SourceCode";
  public static final String STATUS_QTY = "StatusQty";
  public static final String RELEASE_ORDER = "ReleaseOrder";
  public static final String SCHEDULE_ORDER = "ScheduleOrder";
  public static final String UNSCHEDULE_ORDER = "unScheduleOrder";
  public static final String PERSON_INFO_BILL_TO = "PersonInfoBillTo";
  public static final String PERSON_INFO_SHIP_TO = "PersonInfoShipTo";
  public static final String PROMOTION = "Promotion";
  public static final String REQUESTED_RESERVATION_DATE = "RequestedReservationDate";
  public static final String RESERVED_QTY = "ReservedQty";
  public static final String PRODUCT_AVAILABILITY_DATE = "ProductAvailabilityDate";
  public static final String SHIPPING_REFUND = "ShippingRefund";
  public static final String SHIP_DATE = "ShipDate";

  public static final String API_RELEASE_ORDER = "releaseOrder";
  public static final String API_SCHEDULE_ORDER = "scheduleOrder";
  public static final String API_UNSCHEDULE_ORDER = "unScheduleOrder";
  public static final String GC_GET_SOURCE_CODE_LIST_SERVICE = "GCGetSourceCodeListService";
  public static final String GC_PROCESS_TAX_CALCULATIONS_SERVICE = "GCProcessTaxCalculationsService";
  //OMS-5094 starts
  public static final String GET_ORDER_LIST_STATUSES_TEMPLATE =
      "<OrderList> " +
          "<Order OrderHeaderKey='' ReqShipDate='' > " +
          "<OrderLines> " +
          "<OrderLine OrderedQty='' PrimeLineNo='' SubLineNo='' OrderLineKey='' DependentOnLineKey='' KitCode='' MaxLineStatus=''> "
          +
          "<BundleParentLine OrderLineKey='' PrimeLineNo='' SubLineNo='' /> " +
          "<Extn ExtnIsWarrantyItem=''/> " +
          "<ItemDetails ItemID=''> " +
          "<PrimaryInformation ItemType=''/> " +
          "<Extn ExtnSetCode=''/>"+
          "</ItemDetails> " +
          "<Schedules> " +
          "<Schedule OrderLineScheduleKey='' ExpectedDeliveryDate=''   ExpectedShipmentDate='' OrderHeaderKey='' OrderLineKey='' Quantity='' > " +
          "<ScheduleTranQuantity Quantity='' TransactionalUOM=''/> " +
          "</Schedule> " +
          "</Schedules> " +
          "<OrderStatuses> " +
          "<OrderStatus StatusQty='' OrderLineScheduleKey='' OrderLineKey='' ShipNode='' Status='' StatusDate='' StatusDescription='' Qty='' " +
          "TotalQuantity=''> " +
          "<OrderStatusTranQuantity StatusQty='' TotalQuantity='' TransactionalUOM=''/> " +
          "</OrderStatus> " +
          "</OrderStatuses> " +
          "</OrderLine> " +
          "</OrderLines> " +
          "</Order> " +
          "</OrderList> ";
  //OMS-5094 ends
  public static final String GET_ORDER_LINE_LIST_TEMPLATE =
      "<OrderLineList><OrderLine OrderedQty='' BundleParentOrderLineKey=''><LineCharges><LineCharge /></LineCharges><LineTaxes><LineTax /></LineTaxes></OrderLine></OrderLineList>";
  // Backorder DAX Allocation Start
  public static final String API_UNRECEIVE_ORDER = "unreceiveOrder";

  public static final String ORDER_LINE_ROOT = "OrderLineRoot";
  public static final String ORDER_LINE_XPATH = "OrderLineXpath";
  public static final String PARENT_LINE_XPATH = "ParentXPath";
  public static final String LINE_CHARGE_XPATH = "LineChargeXPath";
  public static final String LINE_TAX_XPATH = "LineTaxXPath";
  public static final String LINE_PRICE_LEVEL = "LinePriceLevel";
  public static final String VERTEX_WEB_SERVICE_URL = "VERTEX_WEB_SERVICE_URL";
  // OMS-4727 Start
  public static final String VERTEX_CALL_REQUEST_TRUSTED_ID = "VERTEX_CALL_REQUEST_TRUSTED_ID";
  // OMS-4727 End
  public static final String CUSTOMER_ACTIVE_STATUS = "10";

  // Constants added after bug fixes.
  public static final String EXTN_SOURCE_CODE = "ExtnSourceCode";
  public static final String GC_GET_SC_LIST_SERVICE = "GCGetSourceCodeListService";
  public static final String SHIPPING_CHARGE = "ShippingCharge";
  public static final String EXTN_IS_SHIPPING_PRORATED = "ExtnIsShippingChargeProrated";
  public static final String ERROR_CODE = "ErrorCode";
  public static final String ERROR_DESC = "ErrorDesc";
  public static final String EXCHANGE_TYPE = "ExchangeType";
  public static final String EXTN_SENT_FOR_FRAUD_CHECK = "ExtnSentForFraudCheck";
  public static final String ZERO_ZERO_ZERO = "000";
  public static final String ZERO_ZERO_ONE = "001";
  public static final String ZERO_ZERO_TWO = "002";
  public static final String ZERO_ZERO_THREE = "003";
  public static final String EXTN_FULFILLMENT_TYPE = "ExtnFulfillmentType";
  public static final String ACCESS_LICENSE_NUMBER = "AccessLicenseNumber";
  public static final String IS_COMMERCIAL_ADDRESS = "IsCommercialAddress";
  public static final String GC_ORDER_SHIPPING_CHARGE = "GCOrderShippingCharge";
  public static final String GC_GET_ORDER_SHIPPING_CHARGE_SERVICE = "GCGetOrderShippingCharge";
  public static final String GC_TAX_EXEMPT_DECLINE_NOTIFICATION = "GCTaxExemptDeclinedNotification";
  public static final String GC_TAX_EXEMPT_DECLINE_ALERT_QUEUE = "GC_TAX_EXEMPT_DECLINED_ALERT_QUEUE";
  public static final String GC_TAX_EXEMPT_DECLINE_NOTIFICATION_VALUE = "GC Tax Exempt Declined Notification";
  public static final String REASON_TEXT = "ReasonText";
  public static final String TAX_EXEMPT = "Tax Exempt";
  public static final String GC_TO_Q_TO_RESOLVE_TAX_EXEMPT_HOLD_SERVICE = "GCPostToQToResolveTaxExemptHold";
  public static final String IN_PROGRESS = "InProgress";
  public static final String CUSTOMER_LIST = "CustomerList";
  public static final String CUSTOMER_CONTACT = "CustomerContact";
  public static final String CUSTOMER_CONTACT_LIST = "CustomerContactList";

  public static final String GC_ORDER_COUPON = "GCOrderCoupon";
  public static final String COUPON_CODE = "CouponCode";
  public static final String TO_XSL_COMPONENT = "ToXSLComponent";
  public static final String GC_UPDATE_ORDER_SHIPPING_CHARGES_SERVICE = "GCUpdateOrderShippingCharges";
  public static final String EXTN_SET_CODE = "ExtnSetCode";
  public static final String IS_ITEM_ADDITION = "IsItemEligibleForAddition";
  public static final String GC_SHIPPING_LEVEL_FLAG = "GCShippingLevelFlag";
  public static final String ITEM_TYPE_03 = "03";
  public static final String IS_ORDER_AMENDMENT = "IsOrderAmendment";
  public static final String OVERSIZE_SURCHARGE = "OVERSIZE_SURCHARGE";
  public static final String OVERSIZE_CHARGE = "OversizeCharge";

  public static final String USERID = "userId";
  public static final String PROG_ID = "progId";
  public static final String YFS_ENVIRONMENT = "YFSEnvironment";
  public static final String CALL_CENTER_SELLER_ORG = "GC";
  public static final String EXTN_ORDER_CAPTURE_CHANNEL = "ExtnOrderCaptureChannel";
  public static final String CALL_CENTER_ORDER_CHANNEL = "CallCenter";
  public static final String OVERALL_TOTALS = "OverallTotals";
  public static final String VENDOR_DROP = "VENDOR_DROP";
  public static final String GC_ORDER_SHIPPING_CHARGE_LIST = "GCOrderShippingChargeList";
  public static final String SHIPPING_CHARGE_AMOUNT = "ShippingChargeAmount";
  public static final String ATG_SERVICE_CODE = "ATGServiceCode";
  public static final String ATG_SHIPPING_MTHD = "ATG_SHIPPING_MTHD";
  public static final String GC_SHIPPING_CHARGE = "GCShippingCharge";
  public static final String ORDER_AMOUNT_FROM = "OrderAmountFrom";
  public static final String ORDER_AMOUNT_TO = "OrderAmountTo";
  public static final String SITE_ID = "SiteId";
  public static final String SOURCE = "Source";
  public static final String GC_CHANGE_ORDER_SHIPPING_CHARGE_SERVICE = "GCChangeOrderShippingCharge";
  public static final String GC_CREATE_ORDER_SHIPPING_CHARGE_SERVICE = "GCCreateOrderShippingCharge";
  public static final String NONE_EMPLOYEE_PICKUP_ATG_SHP_CODE = "NONE";
  public static final String DEPENDENCY = "Dependency";
  public static final String DEPENDENT_PRIME_LINE_NO = "DependentOnPrimeLineNo";
  public static final String DEPENDENT_SUB_LINE_NO = "DependentOnSubLineNo";
  public static final String EXTN_IS_FREE_GIFT_ITEM = "ExtnIsFreeGiftItem";
  public static final String EXTN_PROMOTION_ID = "ExtnPromotionID";
  public static final String EXTN_PROMOTION_DESC = "ExtnPromotionDesc";
  public static final String EXTN_DISCOUNT_TYPE = "ExtnDiscountType";
  public static final String EXTN_DISCOUNT_PERCENTAGE = "ExtnDiscountPercentage";
  public static final String REMOVE = "REMOVE";
  public static final String PAYMENT_REFERENCE_8 = "PaymentReference8";
  public static final String PAYMENT_REFERENCE_9 = "PaymentReference9";
  public static final String GC_ORDER_COUPON_LIST = "GCOrderCouponList";
  public static final String IS_ATG_INTERACTION_SUCCES = "IsATGInteractSuccess";
  public static final String LINE_PRICE = "LinePrice";
  public static final String LINE_PRICE_TAX = "LinePriceTax";
  public static final String LOCK_ID = "Lockid";
  public static final String DERIVED_FROM_ORDER_LINE_KEY = "DerivedFromOrderLineKey";
  public static final String SHIPPING_REFUND_TAX = "ShippingRefundTax";
  public static final String RETURN_REASON = "ReturnReason";
  public static final String DERIVED_FROM = "DerivedFrom";
  public static final String GC_ORDER_LINE_COMMISSION_LIST = "GCOrderLineCommissionList";
  public static final String GC_ORDER_LINE_COMISSION = "GCOrderLineCommission";
  public static final String ORDER_LINE_COMMISSION_KEY = "OrderLineCommissionKey";
  public static final String CREATE_PROG_ID = "Createprogid";
  public static final String CREATE_TS = "Createts";
  public static final String CREATE_USER_ID = "Createuserid";
  public static final String MODIFY_PROG_ID = "Modifyprogid";
  public static final String MODIFY_TS = "Modifyts";
  public static final String MODIFY_USER_ID = "Modifyuserid";
  public static final String IS_HISTORY = "isHistory";
  public static final String RETURN_WINDOW_FOR_WARRANTY_ADDITION = "RetWinForAutoWarrAddition";
  public static final String API_GET_SHIPMENT_LIST_FOR_ORDER = "getShipmentListForOrder";
  public static final String GC_UPDATE_RETURN_ORDER_PRICE_SERVICE = "GCUpdateReturnOrderPrice";
  public static final String JBS_WEB_ORDER_CHANNEL = "Web";
  public static final String JBS_STORE_ORDER_CHANNEL = "Store";
  public static final String DERIVED_FROM_ORDER_HEADER_KEY = "DerivedFromOrderHeaderKey";

  public static final String API_GET_ORDER_AUDIT_LIST = "getOrderAuditList";
  public static final String ATG_URL = "gc.atg.service.url";
  public static final String ATG_SOAP_ACTION = "gc.atg.soapaction";
  public static final String ATG_FINALIZE_SOAP_ACTION = "gc.atg.finalize.soapaction";
  public static final String ATG_VOID_SOAP_ACTION = "gc.atg.void.soapaction";
  public static final String ORGANIZATION_LT = "OrganizationList";
  public static final String CUSTOMER_CONTACT_ID = "CustomerContactID";
  public static final String CUSTOMER_ADDITIONAL_ADDRESS_LIST = "CustomerAdditionalAddressList";
  public static final String CUSTOMER_ADDITIONAL_ADDRESS = "CustomerAdditionalAddress";
  public static final String CUSTOMER_ADDITIONAL_ADDRESS_ID = "CustomerAdditionalAddressID";
  public static final String IS_DEFAULT_SHIP_TO = "IsDefaultShipTo";
  public static final String ORDER_INVOICE = "OrderInvoice";
  public static final String GET_ORDER_INVOICE_LIST = "getOrderInvoiceList";
  public static final String INVOICE_KEY = "InvoiceKey";
  public static final String INVOICE_HEADER = "InvoiceHeader";
  public static final String LINE_DETAILS = "LineDetails";
  public static final String API_SUCCESS="ApiSuccess";
  public static final String ALL = "all";
  public static final String ORDER_INVOICE_DETAIL = "OrderInvoiceDetail";
  public static final String GET_ORDER_INVOICE_DETAIL_LIST = "getOrderInvoiceDetailList";
  public static final String CUSTOMER_APPEASEMENT = "CUSTOMER_APPEASEMENT";

  public static final String EXTN_IS_WARRANTY_ITEM="ExtnIsWarrantyItem";
  public static final String TRACK="Track";
  public static final String TOTAL_REQUIRED_QTY="TotalRequiredQty";
  public static final String EXTN_EXPECTED_QUANTITY = "ExtnExpectedQuantity";
  // cr67: BEGIN
  public static final String ERROR = "ERROR";
  public static final String REFERENCE3 = "Reference3";
  public static final String VERTEX_FAILURE_NOTE_TEXT = "Interaction with Vertex failed";
  public static final String GC_VERTEX_DOWN_NOTIFICATION = "GCVertexDownNotification";
  public static final String GC_VERTEX_DOWN_ALERT_QUEUE = "GC_VERTEX_DOWN_ALERT_QUEUE";
  public static final String VERTEX_DOWN_RESULT_MESSAGE = "Interaction With Vertex Failed";
  public static final String GC_RAISE_VERTEX_FAILURE_ALERT = "GCRaiseVertexFailureAlert";
  public static final String REFERENCE_3 = "Reference_3";
  public static final String MAX_ORDER_STATUS = "MaxOrderStatus";
  public static final String MAX_ORDER_STATUS_CODE = "1000";

  public static final String GC_COUPON_FAILED_ALERT_QUEUE = "GC_COUPON_FAILED_ALERT_QUEUE";
  public static final String GC_COUPON_FINALIZE_FAILED = "GCCouponFinalizeFailed";
  public static final String GC_COUPON_CANCELLATION_FAILED = "GCCouponCancellationFailed";
  public static final String GC_COUPON_CANCELATION_FAILED_ALERT_QUEUE = "GC_COUPON_CANCELATION_FAILED_ALERT_QUEUE";
  public static final String ITEM_CANNOT_BE_SUBSTITUTED_AT_CURRENT_STATUS = "Item cannot be substituted at current order status";
  public static final String GC_GET_ATG_PROMO_SERVICE = "GCGetATGPromoService";
  public static final String EXTN_IS_ATG_CALL_REQUIRED = "ExtnIsATGCallRequired";
  // cr67: END

  // OMS-4265 Start
  public static final String GC_GET_ORDER_LIST_TEMPLATE = "/global/template/api/getOrderList.xml";
  // OMS-4265 End
  // OMS-4293 start
  public static final String EXTN_WARRANTY_LINE_NUMBER = "ExtnWarrantyLineNumber";
  // OMS-4293 end
  // OMS-4324 Start
  public static final String GET_SHIPMENT_LIST = "getShipmentList";
  public static final String SHIPMENTS = "Shipments";
  // OMS-4324 End

  // catalog Index purge constants
  public static final String CATEGORY_DOMAIN = "CategoryDomain";
  public static final String PURGE_CAT_INDEX_DAYS = "PurgeIndexBeforeDays";
  public static final String SYS_DATE_FORMAT = "yyyy-MM-dd'T'HH-mm-ss";
  public static final String SEARCH_INDEX_TRIGGER = "SearchIndexTrigger";
  public static final String DELETE_ANY_TRIGGERS_BEFORE_TIME_STAMP = "DeleteAnyTriggersBeforeTimestamp";
  public static final String MANAGE_SEARCH_INDEX_TRIGGER = "manageSearchIndexTrigger";

  // OMS-4570 Start
  public static final String ALLOCATION_PERCENTAGE = "AllocationPercentage";
  public static final String USERNAME1 = "UserName";
  // OMS-4570 End
  // CR-72 Start
  public static final String REFUND_TYPE = "RefundType";
  public static final String COA = "COA";
  public static final String NEW_REFUND_PAYMENT_METHOD = "NEW";
  public static final String EXTN_REFUND_PAYMENT_KEY = "ExtnRefundPaymentKey";
  // CR-72 End


  // OMS-4508 Starts
  public static final String EXTN_WARRANTY_PARENT_LINE_NUMBER = "ExtnWarrantyParentLineNumber";
  // OMS-4508 Ends
  // OMS-4660 Start
  public static final String LAST_ORDER_RELEASE_KEY = "LastOrderReleaseKey";
  // OMS-4660 End
  // OMS-4682 End
  public static final String GET_ORDER_RELEASE_LIST = "getOrderReleaseList";
  // OMS-4682 End
  // OMS-4826 Starts
  public static final String GC_GET_USER_GROUP_QUEUE_MAPPING_LIST = "GCGetUserGroupQueueMappingList";
  public static final String QUEUE_SUBSCRIPTION_LIST = "QueueSubscriptionList";
  public static final String QUEUE_SUBSCRIPTION = "QueueSubscription";
  public static final String QUEUE_KEY = "QueueKey";
  public static final String GET_QUEUE_LIST = "getQueueList";
  // OMS-4826 Ends
  // OMS-4668 Starts
  public static final String CHAINED_FROM_ORDER_HEADER_KEY = "ChainedFromOrderHeaderKey";
  public static final String CHAINED_FROM_ORDER_LINE_KEY = "ChainedFromOrderLineKey";

  public static final String TRACKING_NO = "TrackingNo";
  public static final String EXTN_CARRIER_SERVICE_CODE = "ExtnCarrierServiceCode";
  // OMS-4668 Ends
  // OMS-4635 Starts
  public static final String IS_PRORATION_REQUIRED = "IsProrationRequired";
  // OMS-4635 Ends
  // CR74: Begins
  public static final CharSequence LOGIN_ID_SEPARATOR = "@";
  public static final CharSequence LDAP_LOGIN_ID_SEPARATOR = "\\";
  // CR74: End
  public static final String GC_PAYMENT_HOLD_NOTIFICATION = "GCPaymentHoldNotification";
  public static final String API_GET_EXCEPTION_LIST = "getExceptionList";
  public static final String RESOLUTION_DETAILS = "ResolutionDetails";
  public static final String INBOX_KEY = "InboxKey";
  public static final String API_RESOLVE_EXCEPTION = "resolveException";
  public static final String CANCELLATION_REASON_CODE = "CancellationReasonCode";
  public static final String YCD_CANCEL_REASON = "YCD_CANCEL_REASON";
  public static final String CANCELLATION_REASON_CODE_PREFIX = "CancellationReasonCode: ";
  public static final String MULTI_API = "multiApi";
  public static final String API = "API";
  public static final String INPUT = "Input";
  // CR78 : Starts
  public static final String EXTN_MEMBER_CREATE_DATE = "ExtnMemberCreateDate";
  // CR78 : Ends
  // CR-77
  public static final String EXTENDED_PRICE = "ExtendedPrice";
  public static final String OFFER_AMOUNT = "OfferAmount";
  public static final String LINE_TOTAL_WITHOUT_TAX = "LineTotalWithoutTax";
  public static final String EXTN_ADD_WARRANTY_LINE_TO_RO = "ExtnAddWarrantyLineToRO";
  public static final String ADD_WARRANTY_LINE_TO_RO = "AddWarrantyLineToRO";
  public static final String REASON_CODE_1 = "ReasonCode1";
  // CR-77 Ends
  //OMS-4986 Starts
  public static final String GC_INTEGRATION_AUDIT = "GCIntegrationAudit";
  public static final String MSG_FROM = "MsgFrom";
  public static final String MSG_TO = "MsgTo";
  public static final String MSG_DESCRIPTION = "MsgDescription";
  public static final String MSG_XML = "MsgXml";
  public static final String MSG_IDENTIFIER = "MsgIdentifier";
  public static final String GC_CREATE_INTEGRATION_AUDIT_SERVICE = "GCCreateIntegrationAuditService";
  //OMS-4986 Ends
  //OMS-4860
  public static final String GC_INACTIVE_ACCOUNT_ALERT = "GCInactiveAccountAlert";
  public static final String INACTIVE_ACCOUNT_HOLD = "INACTIVE_ACCOUNT_HOL";
  public static final String DATA_KEY = "DataKey";
  public static final String DATA_TYPE = "DataType";
  public static final String OPERATION = "Operation";
  public static final String TRANSACTION_KEY = "TransactionKey";
  public static final String AVAILABLE_DATE = "AvailableDate";
  public static final String API_MANAGE_TASK_QUEUE = "manageTaskQueue";
  public static final String GC_ORDR_AMNDMNT_ON_CSTMR_VRFCSN= "GCOrderAmendmentOnCustomerVerificationMsgService";
  public static final String CUSTOMER_DOES_NOT_EXIST= "CustomerDoesNotExist";
  public static final String ACTIVE_CUSTOMER= "ActiveCustomer";
  public static final String INACTIVE_CUSTOMER= "InactiveCustomer";
  public static final String OPERATION_IDENTIFIER= "OperationIdentifier";
  public static final String CUSTOMER_INACTIVE_STATUS="00";
  // OMS-4959-start
  public static final String ELEMENT_GC_SOURCE_CODE = "GCSourceCode";
  public static final String GET_SOURCE_CODE_LIST_SVC = "GetGCSourceCodeList_svc";
  public static final String CHANGE_GC_SOURCE_CODE_SVC = "ChangeGCSourceCode_svc";
  public static final String CREATE_GC_SOURCE_CODE_SC = "CreateGCSourceCode_svc";
  public static final String SOURCE_CODE_KEY = "SourceCodeKey";
  //OMS-4959-end
  //OMS-4875 Starts
  public static final String WAIT = "WAIT";
  //OMS - 4875 Ends

  //OMS-5060
  public static final String TAX_PRODUCT_CODE = "TaxProductCode";
  //OMS-5060

  //OMS-5148
  public static final String EXTN_IS_SHIP_ALONE  = "ExtnIsShipAlone";
  //OMS-5148

  //OMS-5154
  public static final String GET_ORDER_LIST_TEMPLATE_LIABILITY_TRFR="<OrderList> <Order OrderHeaderKey='' OrderNo='' EntryType='' BillToID=''>"
      + "<Extn ExtnDAXCustomerID=''/>"
      + "<PersonInfoBillTo AddressLine1='' EMailID='' FirstName=''> </PersonInfoBillTo><ChargeTransactionDetails><ChargeTransactionDetail><CreditCardTransactions><CreditCardTransaction /></CreditCardTransactions><PaymentMethod /></ChargeTransactionDetail></ChargeTransactionDetails> </Order> </OrderList>";
  //OMS-5154
  // OMS-5010-start
  public static final String BILLABLE_WEIGHT = "BillableWeight";
  // OMS-5010-end

  // OMS-5164 starts
  public static final String REFERENCE_2 = "Reference_2";
  public static final String IS_INVOICE_ARG = "IsInvoice";
  public static final String XPATH_ITEM_DETAILS_EXTN = "ItemDetails/Extn";
  public static final String XPATH_ONE_STEP_BACK = "/..";
  public static final String MODIFY_LINE_PRICE_LEVEL_FOR_INVOICE = "OrderLine/";
  public static final String XPATH_ORDLN_ITEM_DETAILS_PRIMARY_INFO_UNIT_COST =
      "OrderLine/ItemDetails/PrimaryInformation/@UnitCost";
  public static final String XPATH_ITEM_DETAILS_PRIMARY_INFO_UNIT_COST =
      "ItemDetails/PrimaryInformation/@UnitCost";
  // OMS-5164 ends

  //OMS-5179
  public static final String GC_SHIPMENT_CONFIRMATION_ADD_NOTE_SERVICE ="GCShipmentConfirmationAddNoteService";
  //OMS-5179

  // OMS - 5193
  public static final String ORDER_STATUS = "OrderStatus";
  public static final String MAX_LINE_STATUS_DESC = "MaxLineStatusDesc";
  public static final String ORDER_STATUSES = "OrderStatuses";
  public static final String ORDER_LINE_SCHEDULE_KEY = "OrderLineScheduleKey";
  public static final String ORDER_RELEASE_STATUS_KEY = "OrderReleaseStatusKey";
  public static final String PIPELINE_KEY = "PipelineKey";
  public static final String STATUS_DATE = "StatusDate";
  public static final String STATUS_DESCRIPTION = "StatusDescription";
  // OMS - 5193

  /* ---- OMS-5209 Starts ----- */
  public static final String FROM_SCHEDULES = "FromSchedules";
  public static final String EXPECTED_SHIPMENT_DATE = "ExpectedShipmentDate";
  public static final String FROM_EXPECTED_SHIPMENT_DATE = "FromExpectedShipDate";
  public static final String PO_DAX_UNSCHEDULE_STATUS = "1310.500";
  public static final String FROM_SCHEDULE = "FromSchedule";
  public static final String TO_SCHEDULES = "ToSchedules";
  public static final String TO_SCHEDULE = "ToSchedule";
  public static final String TO_EXPECTED_SHIP_DATE = "ToExpectedShipDate";
  public static final String CHANGE_ORDER_SCHEDULE_API = "changeOrderSchedule";
  public static final String XPATH_ORDLST_ORD_ATTR_ORDHDRKEY = "OrderList/Order/@OrderHeaderKey";
  public static final String XPATH_ORDER_ORDERLINES_ORDERLINE = "Order/OrderLines/OrderLine";
  public static final String XPATH_ORDERLIST_ORDER_ATTR_REQ_SHIP_DATE = "OrderList/Order/@ReqShipDate";
  public static final String XPATH_ORDERSTATUES_ORDERSTATUS_ATTR_STATUS_EQUALTO_1300 = "OrderStatuses/OrderStatus[@Status='1300']";
  public static final String XPATH_ORDERSTATUSCHANGE_ORDERLINES_ORDERLINE = "OrderStatusChange/OrderLines/OrderLine";
  /* ---- OMS-5209 Ends ----- */

  //OMS-5213 start
  public static final String SHIPPING_CHARGE_TAX = "ShippingChargeTax";
  //OMS-5213 End

  /*---- OMS-5275 start ------*/
  public static final String STATUS_SCHEDULE_PODAX = "1500.10";
  public static final String STATUS_RECIEVED_PODAX = "1310.500.100";
  public static final String STATUS_MOVE_FROM_RCVPDX_TO_PODAX_UNSCHD = "MOVE_FROM_RECV_PDX_TO_PDX_UNSCHD";
  public static final String GC_RECIEVED_PODAX_TRANS_ID = "GC_RECIEVED_PODAX.0001.ex";
  public static final String STRING_00 = "00";
  public static final String STRING_11 = "11";
  public static final String STRING_01 = "01";
  public static final String ORDER_STATUS_CHANGE_ORDER_LINES = "OrderStatusChange/OrderLines";
  public static final String XPATH_ORD_STATUSES_ORD_STATUS = "OrderStatuses/OrderStatus";
  public static final String GC_UNSCHEDULE_TRANS_ID = "GC_UNSCHEDULE.0001.ex";
  /*---- OMS-5275 Ends ------*/

  //OMS 5474 Start
  public static final String GC_ORDERLINE_COMMISSION = "GCOrderLineCommission";
  public static final String USRNME = "UserName";
  public static final String UserName = "UserName";
  //OMS 5474 Ends




  // GCEXStore AdvancedSearchUI

  // Shipment Reports -------------------------------Starts


  // ExStore------------------------------------------------------Starts

  // Login------------------Starts
  public static final String GET_ORG_HEIRARCHY_TEMPLATE = new StringBuilder("<Organization OrganizationCode=''>")
  .append(
      "<Node NodeType = '' /></Organization>").toString();
  public static final String LOGIN_TEMPLATE = new StringBuilder("<Login DisplayUserID='' EnterpriseCode='' LoginID='' "
      + "OrganizationCode='' UserGroupID='' UserGroup_Name='' UserName='' /> ").toString();

  public static final String GC_EXSTORE_USR_GRP = "GC_EXSTORE_USR_GRP";
  public static final String GC_EXSTORE_MGR_GRP = "GC_EXSTORE_MGR_GRP";
  public static final String USER_TYPE = "UserType";
  public static final String USR_GRP_ID = "UserGroupID";
  public static final String API_LOGIN = "login";
  public static final String DOMAIN = "Domain";
  public static final String VALUE_STORE_USER = "storeuser";
  public static final String API_GET_ORG_HIERARCHY = "getOrganizationHierarchy";
  public static final String STORE = "Store";
  public static final String STORE_ID = "StoreID";
  public static final String GET_ODR_LIST_TEMPLATE = new StringBuilder("<OrderList>").append(
      "<Order OrderDate='' OrderNo='' OrderHeaderKey='' LevelOfService=''><Extn ExtnExpediteFlag='' ExtnPickupStoreID=''/><OrderLines>"
          + "<OrderLine PrimeLineNo='' FulfillmentType = '' OrderLineKey='' DependentOnLineKey=''><Extn /> </OrderLine></OrderLines></Order></OrderList>")
          .toString();
  public static final String GET_SHIP_LIST_TEMPLATE = new StringBuilder("<Shipments>").append(
      "<Shipment ShipmentKey='' ShipNode='' ShipmentNo='' StatusDate='' Status='' LevelOfService='' ><ShipmentLines><ShipmentLine OrderHeaderKey = '' "
          + "OrderNo = '' ItemDesc = '' LevelOfService='' OrderLineKey='' >"
          + "<OrderLine FulfillmentType = '' PrimeLineNo = '' OrderLineKey=''/>"
          + "</ShipmentLine></ShipmentLines><Extn /></Shipment></Shipments>")
          .toString();
  public static final String GET_SHIP_DETAIL_TEMPLATE = new StringBuilder(
      "<Shipment BillToCustomerId='' DocumentType='' EnterpriseCode='' DeliveryMethod='' ShipmentKey ='' LevelOfService='' RequestedShipmentDate='' ExpectedDeliveryDate='' ShipDate='' ShipmentNo='' Status='' OrderHeaderKey='' ShipNode=''>"
          + "<ShipNode Description='' ShipNode=''><ShipNodePersonInfo /></ShipNode><ToAddress /><FromAddress/><BillToAddress/><Instructions/><ShipmentLines>"
          + "<ShipmentLine LevelOfService='' ItemDesc='' ItemID='' KitCode='' OrderHeaderKey='' OrderLineKey='' OrderReleaseKey='' OrderNo='' OriginalQuantity=''  PrimeLineNo='' ProductClass=''"
          + " Quantity='' ReleaseNo='' ShipmentKey='' ShipmentLineKey='' ParentShipmentLineKey='' ShipmentLineNo='' ShipmentSubLineNo=''  SubLineNo='' "
          + "UnitOfMeasure='' RequestedTagNo=''><ShipmentTagSerials><ShipmentTagSerial /></ShipmentTagSerials><Order DocumentType='' "
          + "EnterpriseCode='' OrderDate='' OrderHeaderKey='' OrderNo='' SellerOrganizationCode='' CustomerFirstName='' CustomerLastName='' CustomerEMailID=''>"
          + "<Extn /><PaymentMethods><PaymentMethod CreditCardType='' PaymentType='' /></PaymentMethods></Order><OrderLine DeliveryMethod='' FulfillmentType='' KitCode='' OrderHeaderKey='' OrderLineKey='' OrderedQty='' DependentOnLineKey='' "
          + "OriginalOrderedQty='' PrimeLineNo='' SubLineNo='' ><LinePriceInfo />"
          + "<LineOverallTotals /><ItemDetails><PrimaryInformation/><Extn ExtnIsUniqueItem='' ExtnBrandName='' ExtnBrandId='' ExtnPOSItemID='' ExtnPrimaryComponentItemID='' ExtnIsPlatinumItem='' ExtnIsUsedOrVintageItem='' />"
          + "</ItemDetails><Extn /></OrderLine></ShipmentLine></ShipmentLines><Extn /></Shipment>")
  .toString();

  public static final String STATUS_READY_FOR_BACKROOM_PICK = "1100.70.06.10";
  public static final String GET_SHP_LST_API = "getShipmentList";
  public static final String FLAG_IS_SHIP_TO_STORE = "IsShipToStore";
  public static final String AGING = "Aging";
  public static final String FLAG_IS_EXPEDITE = "IsExpedite";
  public static final String FLAG_EXTN_IS_SHIP_TO_STORE = "ExtnIsBackOrderEmailSent";
  public static final String FLAG_IS_SHIP_TO_CUSTOMER = "IsShipToCustomer";
  public static final String FLAG_IS_SAME_STORE_PICKUP = "IsSameStorePickup";
  public static final String SHIP_TO_STORE = "SHIP_TO_STORE";
  public static final String COMMON_CODE_TEMPLATE = "<CommonCodeList><CommonCode /></CommonCodeList>";
  public static final String IS_OVERRIDE_STORE = "IsOverrideStore";
  // OMS 5329 Start
  public static final String IS_LINE_PRICE_FOR_INFORMATION_ONLY = "IsLinePriceForInformationOnly";
  // OMS 5329 End
  // OMS 5580 Start
  public static final String PAYMENT_RULE_ID = "PaymentRuleId";
  // OMS 5580 End
  // OMS-5497: Starts
  public static final String COMPONENTS = "Components";
  public static final String COMPONENT_ITEM_KEY = "ComponentItemKey";
  // OMS-5497: Ends
  // Login------------------Ends

  // PickList------------------Starts
  public static final String EXTN_PICKUP_STORE_ID = "ExtnPickupStoreID";
  public static final String ORDER_COMMISSION_LIST_SERVICE = "GCOrderCommissionListService";
  public static final String REQ_DELIVERY_DATE = "ReqDeliveryDate";
  public static final String IS_STORE_SHIPMENT = "IsStoreShipment";
  public static final String CONSOLIDATE_TO_SHIPMENT_API = "consolidateToShipment";
  public static final String EXTN_ORDER_DATE = "ExtnOrderDate";
  public static final String SUB_TOTAL = "SubTotal";
  public static final String IS_WARRANTY = "ExtnIsWarrantyItem";
  public static final String ORDERING_STORE = "OrderingStore";
  public static final String EXTN_ORDERING_STORE = "ExtnOrderingStore";
  public static final String IMG_LOCATION = "ImageLocation";
  public static final String IMG_ID = "ImageID";
  public static final String IMG_URL = "ImageUrl";
  public static final String BCKROOM_PCKD_QTY = "BackroomPickedQuantity";
  public static final String SHIPMENT_LINE_KEY = "ShipmentLineKey";
  public static final String BCKODR_RMVD_QTY = "BackorderRemovedQuantity";
  public static final String BCKROOM_PICKED_STATUS = "1100.70.06.30";
  public static final String BCKROOM_PICKED_TRNSCTN = "YCD_BACKROOM_PICK";
  public static final String CHANGE_SHIPMENT_STATUS = "changeShipmentStatus";
  public static final String READY_FOR_CUSTOMER_STATUS = "1400.200";
  public static final String READY_FOR_PACKING_STATUS = "1100.70.06.50";
  public static final String READY_FOR_CUST_TRNSCTN = "GC_READY_CUST_PICK_UP.0001.ex";
  public static final String PICKUP_NOTICE_SERVICE = "GCPickupNoticeService";
  public static final String SHIPMENT_NOTICE_SERVICE = "GCShipmentNoticeService";
  public static final String GC_EXSTORE_LOS = "GC_EXSTORE_LOS";
  // PickList------------------Ends
  // DEV STORY - GCSTORE-192 and GCSTORE-190 ---Starts
  public static final String CANCEL_ALL = "cancelAll";
  public static final String RESOURCE_ALL = "resourceAll";
  public static final String PARENT_SHPMNTLINE_KEY = "ParentShipmentLineKey";
  public static final String CANCEL_RMVD_QTY = "CancelRemovedQuantity";
  public static final String CHANGE_SHIPMENT = "changeShipment";
  public static final String BACKORDER_RMVD_QTY = "BackOrderRemovedQuantity";
  public static final String ACTION_RESOURCE_LINE = "resourceLine";
  public static final String CHANGE_STATUS_CONFIRM = "CHANGE_STATUS_CONFIRM.0001.ex";
  public static final String QTY_TO_PICK = "QuantityToPick";
  public static final String IS_SUCCESS = "IsSuccess";
  public static final String GC_SHIPMENT_ACTICITY = "GCUserActivity";
  public static final String USER_ACTION = "UserAction";
  public static final String CANCEL_ALL_LINES = "Cancel All Lines";
  public static final String CNFRM_PICK_CNCLALL = "CONFIRM_PICK_CANCEL_ALL";
  public static final String AUDIT_RSNCODE = "AuditReasonCode";
  public static final String RESOURCE_ALL_LINES = "Re-Source All Lines";
  public static final String CNFRM_PICK_RSRCALL = "CONFIRM_PICK_RE-SOURCE_ALL";
  public static final String CANCEL_LINE = "Cancel Line";
  public static final String CNFRM_PICK_CNCL_LINE = "CONFIRM_PICK_CANCEL_LINE";
  public static final String RESOURCE_LINE = "Re-source Line";
  public static final String CNFRM_PICK_RSRCLINE = "CONFIRM_PICK_RE-SOURCE_LINE";
  public static final String GC_INSERT_SHPMNT_ACTIVITY = "GCInsertShipmentActivityToDB";
  public static final String CUSTOM_COMPONENTS = "CustomComponents";
  public static final String MODIFY_SHIPMENT = "Create-Modify";
  public static final String ACTION_CANCEL_SHIPMENT = "Cancel";
  public static final String GET_ORDER_LIST_TEMP =
      "<OrderList><Order OrderNo='' OrderHeaderKey='' Status='' LevelOfService='' OrderDate='' EnterpriseCode='' TaxExemptFlag='' ReqShipDate='' >"
          + "<OverallTotals GrandTotal='' /><Extn ExtnSourceCode='' ExtnFTCExempt='' ExtnSourceOrderNo='' ><GCSupportTicketList><GCSupportTicket SupportTicketNo='' OrderNo='' ShipmentNo='' /></GCSupportTicketList></Extn>"
          + "<OrderLines><OrderLine IsBundleParent='' ShipNode='' PrimeLineNo='' FulfillmentType='' Status='' ><BundleParentLine OrderLineKey='' PrimeLineNo='' SubLineNo='' /> <Extn ExtnPOSSKUNumber='' ExtnActualSerialNumber='' ExtnPOSSalesTicketNo='' />"
          + "<ItemDetails ItemID='' ><PrimaryInformation ExtendedDisplayDescription='' /></ItemDetails>"
          + "<OrderLineTranQuantity OrderedQty='' /><LinePriceInfo UnitPrice='' /><OrderStatuses><OrderStatus ShipNode=''/></OrderStatuses><Schedules><Schedule /></Schedules></OrderLine></OrderLines>"
          + "<Notes NumberOfNotes=''><Note NoteText='' ContactTime='' ContactUser='' ReasonCode='' ><User Username='' Usertype=''/></Note></Notes>"
          + "<PaymentMethods><PaymentMethod PaymentType='' CreditCardNo='' CreditCardType='' CreditCardName='' DisplayCreditCardNo='' TotalAuthorized='' TotalCharged='' TotalRefundedAmount='' MaxChargeLimit='' />"
          + "</PaymentMethods><PersonInfoBillTo /><PersonInfoShipTo /><OrderDates><OrderDate /></OrderDates></Order></OrderList>";
  public static final String ACTION_CANCEL_LINE = "cancelLine";
  public static final String GET_SHIPMENT_LIST_TEMPLATE =
      new StringBuilder("<Shipments>")
  .append(
      "<Shipment ShipmentKey='' ShipNode='' ShipmentNo='' StatusDate='' Status='' LevelOfService='' ><ShipmentLines><ShipmentLine OrderHeaderKey = '' "
          + "OrderNo = '' ItemDesc = '' LevelOfService='' OrderLineKey='' >"
          + "<OrderLine FulfillmentType = '' PrimeLineNo = '' OrderLineKey='' DependentOnLineKey='' />"
          + "</ShipmentLine></ShipmentLines><Extn /></Shipment></Shipments>").toString();
  public static final String SCAN_SKU_REQUEST =
      "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>"
          + "<soap:Body><ValidateSKU xmlns='http://tempuri.org/'><XMLDocIn><Products xmlns=''></Products></XMLDocIn></ValidateSKU></soap:Body></soap:Envelope>";
  public static final String PRODUCTS = "Products";
  public static final String PRODUCT = "Product";
  public static final String TYPE = "Type";
  public static final String LINE_NUMBER = "LineNumber";
  public static final String SHIPMENT_LINE_NO = "ShipmentLineNo";
  public static final String POS_LOCATION = "POSLocation";
  public static final String SKU = "SKU";
  public static final String POS_ITEM_ID = "POSItemID";
  public static final String INV_INPUT = "Inv.input";
  public static final String TEST_WEBSERVICE = "GCTestWebServiceForScanSKU";
  public static final String WARRANTY_COMPONENTS = "WarrantyComponents";
  public static final String XMLNS = "xmlns";
  public static final String SKU_NO_ELE = "SKU.Number";
  public static final String SERIAL_NUMBER = "SerialNumber";
  public static final String SALES_PERSONS = "Salespersons";
  public static final String SALES_PERSONS_ELE = "SalesPersons";
  public static final String SALES_PERSON_ELE = "SalesPerson";
  public static final String GC_SALES_PRSN_LIST = "GCSalesPersonList";
  public static final String GC_SALES_PRSN = "GCSalesPerson";
  public static final String SALES_PERSON_ID = "SalesPersonId";
  public static final String ACQUISITION_COMMISSION = "AcquisitionCommission";
  public static final String DISPLAY_CC_NO = "************";
  public static final String SOAP_BODY = "soap:Body";
  public static final String VALIDATE_SKU_ELE = "ValidateSKU";
  public static final String XML_DOC_IN_ELE = "XMLDocIn";
  public static final String FETCH_DUMMY_RESPONSE = "FetchDummyResponse";
  public static final String RESPONSE = "Response";
  public static final String RESPONSES = "Responses";
  public static final String UNABLE_TO_CONNECT_TO_POS = "SKU Validation Failed: Unable to communicate with POS";
  public static final String FALSE_STR = "false";
  public static final String POS_ERROR = "SKU Validation Failed: [error returned by POS]";
  public static final String ERROR_ELE = "Error";
  public static final String ATTR_LOGIN_ID = "Loginid";
  public static final String ADMIN_USER = "adminuser";
  public static final String MANAGER_USER = "manageruser";
  public static final String STORE_USER = "storeuser";
  public static final String IS_MANAGER_APPROVAL = "IsManagerApproval";
  public static final String USER_ID_DOMAIN = "UserIdDomain";
  public static final String EXTN_ACTUAL_SERIAL_NO = "ExtnActualSerialNumber";
  public static final String ORDER_INV_ATTR_REQ = "OrderLineInvAttRequest";
  public static final String BATCH_NO = "BatchNo";
  public static final String IS_WEBSERVICE_CALL = "IsWebServiceCall";
  public static final String USER_AUTHENTICATION = "UserAuthentication";
  public static final String GET_USER_GROUP_LIST = "getUserGroupList";
  public static final String SBODY = "s:Body";
  public static final String VLDT_SKU_RESPONSE = "ValidateSKUResponse";
  public static final String VLDT_SKU_RESULT = "ValidateSKUResult";
  public static final String IS_LDAP_CONNECTED = "IsLDAPConnected";
  public static final String SENVELOPE = "s:Envelope";
  public static final String SKU_NUM = "SKUNumber";
  public static final String POS_SCAN_SKU_SOAP_ACTION = "POS_SCAN_SKU_SOAP_ACTION";
  public static final String POS_SCAN_SKU_WEB_SERVICE_URL = "POS_SCAN_SKU_URL";
  public static final String DISPLAY_CREDIT_CARD_NUM = "DisplayCreditCardNo";

  public static final String SHIP_NODE_DESC = "ShipNodeDesc";
  public static final String GET_SHIP_NODE_TEMP =
      "<ShipNodeList><ShipNode ShipNode='' Description='' /></ShipNodeList>";
  public static final String DATE_TYPE_ID = "DateTypeId";
  public static final String BACKORD_NOTIFY_REF_DATE = "BACKORDER_NOTIFY_REF_DATE";
  public static final String ACTUAL_DATE = "ActualDate";
  // DEV STORY - GCSTORE-192 and GCSTORE-190 ---Ends

  // Confirm Pick--- Starts
  public static final String MANAGER_ID = "ManagerID";
  public static final String COMMISSIONS = "Commissions";
  public static final String COMMISSION = "Commission";
  public static final String SALES_PERSON = "SalesPerson";
  public static final String SALES_PERCENT = "SalePercent";
  public static final String MESSAGE_TYPE = "MessageType";
  public static final String MGR_APPV = "MgrAppv";
  public static final String CASHIER_APPV = "CashierAppv";
  public static final String RESERVE = "RESERVE";
  public static final String SALESCOMMISSION = "SalesCommission";
  public static final String ACQCOMMISSION = "AcquisitionCommission";
  public static final String PRODUCT_TYPE = "ProductType";
  public static final String SERIAL_NO = "SerialNumber";
  public static final String SKU_NO = "SKU.Number";
  public static final String CONTAINERS = "Containers";
  public static final String CONTAINER = "Container";
  public static final String CONTAINER_DETAILS = "ContainerDetails";
  public static final String CONTAINER_DETAIL = "ContainerDetail";
  public static final String PARTIAL_PICK = "PartialPick";
  public static final String CONFIRM_PICK = "ConfirmPick";
  public static final String QTY_PICKED = "QuantityPicked";
  public static final String GET_ODR_LINE_LIST_TEMP = new StringBuilder("<OrderLineList>").append(
      "<OrderLine OrderLineKey=''><Extn><GCOrderLineCommissionList><GCOrderLineCommission  /></GCOrderLineCommissionList>"
          + "</Extn></OrderLine></OrderLineList>").toString();
  public static final String CHANGE_ORDER_TEMP = "<Order OrderHeaderKey='' />";
  public static final String CHANGE_SHIP_TEMP = "<Shipment ShipmentKey='' ContainerizedQuantity='' TotalQuantity='' />";
  public static final String IS_ERROR = "IsError";
  public static final String IS_VINTAGE_ITEM = "IsUsedOrVintageItem";
  public static final String EXTN_VINTAGE_ITEM = "ExtnIsUsedOrVintageItem";
  public static final String IS_SERIALIZED = "IsSerializedItem";
  public static final String EXTN_UNIQUE_ITEM = "ExtnIsUniqueItem";
  public static final String POS_RESERVATION_ID = "POSSalesTicket";
  public static final String ACTUAL_SERIAL_NO = "ActualSerialNo";
  public static final String EXTN_POS_SKU_NO = "ExtnPOSSKUNumber";
  public static final String EXTN_POS_RESERV_ID = "ExtnPOSReservationId";
  public static final String GET_SUPP_TCKT_TEMPLATE = new StringBuilder("<OrderList><Order OrderHeaderKey = ''><Extn>"
      + "<GCSupportTicketList><GCSupportTicket SupportTicketNo='' OrderNo='' ShipmentNo='' /></GCSupportTicketList>"
      + "</Extn></Order></OrderList>").toString();
  public static final String GC_SUPP_TCKT_LST = "GCSupportTicketList";
  public static final String GC_SUPP_TCKT = "GCSupportTicket";
  public static final String SUPPORT_TICKET_NO = "SupportTicketNo";
  public static final String VIEW = "View";
  public static final String ADD = "Add";
  public static final String GC_SUPPORT_TICKET_DELETE = "GCSupportTicketDeleteService";
  public static final String GET_ODR_LIST_CUST_TEMP = new StringBuilder(
      "<OrderList><Order BillToID='' EnterpriseCode='' OrderHeaderKey=''><OrderLines><OrderLine OrderLineKey='' MaxLineStatus='' DependentOnLineKey=''><Extn/></OrderLine></OrderLines></Order></OrderList>")
  .toString();
  public static final String GET_POS_CUST_ID_SERVICE = "GCGetPOSCustomerID";
  public static final String READY_FOR_BCKRM_PCK_SERVICE = "YCD_Change_Status_Backroom_Pick_8.0";
  public static final String DELIVERY_METHOD = "DeliveryMethod";
  public static final String SHP = "SHP";
  public static final String REQ_TAG_NO = "RequestedTagNo";
  public static final String CHG_ODR_SUPP_TCKT_TEMPLATE =
      "<Order OrderHeaderKey=''><Extn><GCSupportTicketList><GCSupportTicket /></GCSupportTicketList></Extn></Order>";
  public static final String GC_SUPPORT_TICKET_LST = "GCSupportTicketListService";
  public static final String CUST_ID = "BillToCustomerId";
  // Confirm Pick--- Ends

  //PackList start
  public static final String COMPLEX_QUERY = "ComplexQuery";
  public static final String OPERATOR = "Operator";
  public static final String AND = "AND";
  public static final String ORDER_BY = "OrderBy";
  public static final String ATTRIBUTE = "Attribute";
  public static final String DESC = "Desc";
  public static final String AND1 = "And";
  public static final String OR = "Or";
  public static final String EXP = "Exp";
  // to be changed
  public static final String GET_SHIP_LIST_TEMPLATE_PACK_LIST =
      new StringBuilder("<Shipments TotalNumberOfRecords=''>")
  .append(
      "<Shipment EnterpriseCode='' LevelOfService='' "
          + "OrderHeaderKey='' OrderNo='' ShipDate='' ShipNode='' ShipmentNo='' ShipmentKey='' Status='' StatusDate=''>"
          + "<Extn ExtnOrderDate='' ExtnExpediteFlag='' ExtnPackStatusDate='' ExtnPickupStoreID='' ExtnFulfillmentType=''/><ShipmentLines><ShipmentLine ItemDesc='' OrderNo='' OrderHeaderKey='' ShipmentLineNo='' ><OrderLine><Extn/></OrderLine>"
          + "</ShipmentLine></ShipmentLines></Shipment></Shipments>").toString();
  public static final String N = "N";
  public static final String EQ = "EQ";
  public static final String GC_STORE_PACK_SPEC = "GC_STORE_PACK_SPEC";
  public static final String EXTN_PACK_STATUS_DATE = "ExtnPackStatusDate";
  public static final String GET_COMMON_CODE_TEMPLATE = new StringBuilder(
      "<CommonCodeList><CommonCode CodeShortDescription='' CodeValue='' >" + "</CommonCode></CommonCodeList>")
  .toString();
  public static final String READY_FOR_PACKING = "1100.70.06.50";
  public static final String BEING_PACKED = "1100.70.06.70";
  public static final String PACKED = "1300";
  public static final String QUEUE_DATE = "QueueDate";
  public static final String ORDERED_ON = "OrderDate";
  public static final String GC_TEST = "TEST";
  public static final String CODE_LONG_DESCRIPTION = "CodeLongDescription";
  public static final String IS_EXPEDITE="IsExpedite";
  public static final String IS_SHIP_TO_CUSTOMER="IsShipToCustomer";
  public static final String IS_SHIP_TO_STORE = "IsShipToStore";
  public static final String IS_SAME_STORE_PICKUP = "IsSameStorePickup";
  public static final String LEVEL_OF_SERVICE_DESC = "LevelOfServiceDesc";
  public static final String SAME_STORE_PICKUP = "SAME_STORE_PICKUP";
  public static final String GC_STORE_PICK_SPEC = "GC_STORE_PICK_SPEC";
  public static final String STORE_PICKUP = "STORE_PICKUP";
  public static final String STORE_PICKUP_BLANK = "STORE_PICKUP_BLANK";
  public static final String SYSTEM_DATE = "SystemDate";
  public static final String RECORD_GET_SHIP_LIST_TEMPLATE = "<Shipments><Shipment ShipmentKey = '' /></Shipments>";
  // PackList End



  //GCEXStore AdvancedSearchUI
  public static final String SHIPMENT_CONTAINER_KEY = "ShipmentContainerKey";
  public static final String GET_SHIPMENT_CONTAINER_DETALS_TMP="<Container TrackingNo='' ContainerNo=''><Extn ExtnContainerStatus=''/>"
      + "<Shipment ShipmentNo='' LevelOfService='' BillToAddressKey='' ShipDate='' ShipmentKey='' OrderNo='' ShipNode='' OrderHeaderKey=''>"
      + "<ToAddress LastName='' EMailID='' DayPhone=''/><Extn ExtnOrderDate='' ExtnFulfillmentType=''/>"
      + "</Shipment><ContainerDetails><ContainerDetail><ShipmentLine ItemDesc='' Quantity='' ItemID=''/></ContainerDetail></ContainerDetails></Container>";

  public static final String GET_SHIPMENT_CONTAINER_DETAILS="getShipmentContainerDetails";
  public static final String CONTAINER_NO = "ContainerNo";
  public static final String PHONE_NO="PhoneNo";
  public static final String EXTN_STATUS="ExtnStatus";
  public static final String GET_PERSON_INFO_LIST_TMP="<PersonInfoList><PersonInfo/></PersonInfoList>";
  public static final String PERSON_INFO_KEY="PersonInfoKey";
  public static final String GET_PERSON_INFO_LIST = "getPersonInfoList";
  public static final String EXTN_BRAND_NAME="ExtnBrandName";
  public static final String EXTN_POSITEM_ID="ExtnPOSItemID";
  public static final String GET_ITEM_LIST_TMP =
      "<ItemList><Item><Extn ExtnBrandName='' ExtnPOSItemID='' /><PrimaryInformation ImageID='' ImageLocation=''/></Item></ItemList>";
  public static final String CARTON_STATUS = "CartonStatus";
  public static final String RETURN_ORDER_NO = "ReturnOrderNo";
  public static final String QRY_TYPE = "QryType";
  public static final String CONTAINER_LIST = "ContainerList";
  public static final String CUSTOMER_FIRST_NAME = "CustomerFirstName";
  public static final String CUSTOMER_LAST_NAME = "CustomerLastName";
  public static final String IS_MULTIPLE_SHIPNODE_FLAG = "MultipleShipNode";
  public static final String GET_SHIPMENT_LIST_OP_TMP =
      "<Shipments><Shipment  ShipNode='' Status='' OrderNo='' OrderHeaderKey=''>"
          + "<Containers><Container TrackingNo='' ShipmentContainerKey=''><Extn ExtnContainerStatus=''/></Container></Containers>"
          + "<Extn ExtnOrderDate=''/>"
          + "<BillToAddress />"
          + "<ToAddress /></Shipment></Shipments>";

  // AdvanceSearch Ends

  // GC EMAIL Start
  public static final String GC_READY_FOR_CUSTOMER_PICKUP_EMAIL_SERVICE = "GCReadyForCustomerPickUpEmailService";
  public static final String GC_CUSTOMER_PICKEDUP_EMAIL_SERVICE = "GCCustomerPickedupEmailService";
  public static final String GC_GET_EMAIL_TRIGGER_ALLOWED_LIST_SERVICE = "GCGetEmailTriggerAllowedListService";
  public static final String GC_GET_EMAIL_TRIGGER_ALLOWED = "GCGetEmailTriggerAllowed";
  public static final String GC_GET_EMAIL_TRIGGER = "GCGetEmailTrigger";
  public static final String GC_EMAIL_TRIGGER = "GCEmailTrigger";
  public static final String CAPTURE_CHANNEL = "OrderCaptureChannel";
  public static final String IS_RESEND_REQUEST = "IsResendRequest";
  public static final String ORDER_CAPTURE_CHANNEL = "OrderCaptureChannel";
  public static final String GET_ORDER_LIST_TMP_FOR_IS_EMAIL_ALLOWED =
      "<OrderList><Order OrderNo ='' EnterpriseCode='' SellerOrganizationCode='' EntryType=''>"
          + "<Extn ExtnOrderCaptureChannel='' /><PersonInfoBillTo EMailID=''/></Order> </OrderList> ";

  public static final String TRIGGER_ALLOWED = "TriggerAllowed";
  public static final String RESEND_ALLOWED = "ResendAllowed";
  public static final String GET_ORDER_LIST_OUTPUT_TMP =
      "<OrderList><Order DocumentType='' EnterpriseCode='' EntryType='' Status='' OrderDate='' OrderNo='' OrderType='' SellerOrganizationCode='' PaymentStatus='' ><Extn ExtnSourceOrderNo='' ExtnSourceSystem='' ExtnDAXOrderNo='' ExtnDAXCustomerID='' ExtnSourceCustomerID='' ExtnPickupStoreID='' /> "
          + "<OrderLines TotalNumberOfRecords=''><OrderLine OrderLineKey='' FulfillmentType='' LevelOfService='' CarrierServiceCode='' GiftFlag='' KitCode='' MaxLineStatus='' MaxLineStatusDesc='' MinLineStatus='' MinLineStatusDesc='' OrderedQty='' StatusQuantity='' PrimeLineNo='' SubLineNo='' ShipNode='' DependentOnLineKey='' LineType='' ReturnReason='' BundleParentOrderLineKey='' >"
          + "<Item ItemID='' ItemShortDesc='' ItemDesc='' UnitOfMeasure='' ProductClass='' ItemWeight='' ItemType='' >"
          + "</Item><BundleParentLine OrderLineKey='' PrimeLineNo='' SubLineNo=''/>"
          + "<LinePriceInfo IsPriceLocked='' UnitPrice=''/><OrderStatuses><OrderStatus OrderHeaderKey='' OrderLineKey='' OrderLineScheduleKey='' OrderReleaseKey='' OrderReleaseStatusKey='' PipelineKey='' ReceivingNode='' ShipNode='' Status='' StatusDate='' StatusDescription='' StatusQty='' TotalQuantity=''>"
          + " </OrderStatus></OrderStatuses><DerivedFromOrderLine PrimeLineNo='' SubLineNo='' /></OrderLine></OrderLines> "
          + "<OverallTotals GrandCharges='' GrandDiscount='' GrandTax='' GrandTotal='' HdrCharges='' HdrDiscount='' HdrTax='' HdrTotal='' LineSubTotal=''/>"
          + "<PersonInfoShipTo AddressLine1=''  AddressLine2='' AddressLine3='' City='' Country='' DayPhone='' MobilePhone='' EMailID='' FirstName='' MiddleName='' LastName='' Company='' State='' ZipCode='' IsCommercialAddress=''>"
          + " <Extn ExtnIsPOBox='' ExntIsAPOFPO='' /></PersonInfoShipTo><PersonInfoBillTo AddressLine1='' AddressLine2='' AddressLine3='' City='' Country='' DayPhone='' MobilePhone='' EMailID='' FirstName='' MiddleName='' LastName='' Company='' State='' ZipCode='' > "
          + "<Extn ExtnBirthday='' ExtnIsPOBox='' ExntIsAPOFPO='' /></PersonInfoBillTo></Order></OrderList>";
  public static final String ORDER_LINE_MIN_STATUS = "MinLineStatus";
  public static final String READY_FOR_CUSTOMER_PICKUP = "3700.007";

  public static final String GET_CUSTOMER_LIST_TEMPLATE_FOR_EMAIL_ID =
      "<CustomerList><Customer CustomerID='' > <CustomerContactList TotalNumberOfRecords='' > "
          + "<CustomerContact CustomerContactID='' DayFaxNo='' DayPhone='' EmailID='' EveningFaxNo='' EveningPhone='' FirstName='' LastName='' MiddleName='' MobilePhone=''> "
          + "</CustomerContact> </CustomerContactList> </Customer> </CustomerList> " ;

  public static final String CUSTOMER_EMAIL_ID = "CustomerEMailID";
  public static final String ORDER_LINE_MAX_STATUS = "MaxLineStatus";
  public static final String GC_RETURN_RECEIVED_EMAIL_SERVICE = "GCReturnReceivedEmailService";
  // GC EMAIL End



  // Shipment Reports -------------------------------Starts

  public static final String REPORT_TYPE = "ReportType";
  public static final String RECEIVING_STORE = "ReceivingStore";
  public static final String SHIPPED_TO_CUSTOMER = "Shipped To Customer";
  public static final String BILLING_CUSTOMER_FIRST_NAME = "BillingCustomerFirstName";
  public static final String BILLING_CUSTOMER_LAST_NAME = "BillingCustomerLastName";
  public static final String BILLING_PHONE = "BillingPhone";
  public static final String SHIPPING_CUSTOMER_FIRST_NAME = "ShippingCustomerFirstName";
  public static final String SHIPPING_CUSTOMER_LAST_NAME = "ShippingCustomerLastName";
  public static final String SHIPPING_DATE = "ShippingDate";
  public static final String AGING_DAYS = "AgingDays";
  public static final String IS_MULTI_STORE = "IsMultiStore";
  public static final String STATUS_QRY_TYPE = "StatusQryType";
  public static final String EXTN_POS_SALES_TICKET = "ExtnPOSSalesTicket";
  public static final String POS_SALES_TICKET = "POSSalesTicket";
  public static final String GC_ORDER_COMMISSION_LIST_SERVICE = "GCOrderCommissionListService";
  public static final String SPO_ASSOCIATE = "SPOAssociate";
  public static final String USER_NAM = "UserName";
  public static final String FULFILLMENT_ASSOCIATE = "FulfillmentAssociate";
  public static final String SHIPPING_ASSOCIATE = "ShippingAssociate";
  public static final String GC_USER_ACTIVITY = "GCUserActivity";
  public static final String GC_FETCH_USER_ACTIVITY = "GCFetchUserActivity";
  public static final String GC_SHIPMENT_REPORTS = "GCShipmentReports";
  public static final String CONFIRM_PICK_CONFIRM = "CONFIRM_PICK_CONFIRM";
  public static final String IS_RETURN = "IsReturn";
  public static final String GET_SHIPMENT_LIST_OUTPUT_TMP =
      "<Shipments><Shipment  ShipNode='' ShipDate='' LevelOfService='' Status='' OrderNo='' ShipmentKey='' OrderHeaderKey='' DocumentType=''>"
          + "<Extn ExtnPendingReceiptDate='' ExtnPickupStoreID='' ExtnOrderDate='' ExtnPOSSalesTicket=''/><ShipmentLines><ShipmentLine OrderNo=''></ShipmentLine></ShipmentLines>"
          + "<Containers><Container TrackingNo='' ShipmentContainerKey=''><Extn ExtnContainerStatus=''/></Container></Containers>"
          + "<BillToAddress ZipCode='' LastName='' FirstName='' EMailID='' DayPhone='' Country='' AddressLine3='' AddressLine2='' AddressLine1=''/>"
          + "<ToAddress ZipCode='' LastName='' FirstName='' EMailID='' DayPhone='' Country='' AddressLine3='' AddressLine2='' AddressLine1='' /></Shipment></Shipments>";

  //Shipment reports -------------------end


  //Shipment Packing---- Create Container-----Starts
  public static final String EXTN_CONTAINER_STATUS = "ExtnContainerStatus";
  public static final String CONTAINER_PACKED = "Packed";
  public static final String EXTN_MULTIBOX_GRP_ID = "ExtnMultiBoxGroupID";
  public static final String EXTN_MULTIBOX_DESC = "ExtnMultiBoxDesc";
  public static final String CONTAINERIZED_QUANTITY = "ContainerizedQuantity";
  public static final String TOTAL_QUANTITY = "TotalQuantity";
  public static final String PCK_SHP_COMPLETE_TRNSCTN = "PACK_SHIPMENT_COMPLETE";
  public static final String SHP_PACKED_STATUS = "1300";
  public static final String SHP_BEING_PACKED_STATUS = "1100.70.06.70";
  public static final String PACK_DTL_MANAGER_SERVICE = "GCPackDetailsShipListManager";
  public static final String RDY_FOR_CUST_PICKUP = "Ready for Customer Pickup";
  public static final String EXTN_RDY_FOR_PICKUP_DATE = "ExtnReadyForPickupDate";
  public static final String EXTN_SHIP_DATE = "ExtnShipDate";
  public static final String GET_SHIP_LIST_TEMPLATE_PACK_DTL = "<Shipments><Shipment><Containers><Container><ContainerDetails><ContainerDetail/></ContainerDetails>"
      + "</Container></Containers><ShipmentLines><ShipmentLine><OrderLine><Extn/></OrderLine></ShipmentLine></ShipmentLines></Shipment></Shipments>";
  public static final String SO_BACKROOM_PICKED_STATUS = "3350.100";
  public static final String SO_BCKROOM_PICKED_TRNSCTN = "CHNG_STATUS_BKRM_PCKD.0001.ex";
  public static final String SO_READY_FOR_CUST_STATUS = "3700.007";
  public static final String SENT_TO_FULFILLMENT_CENTER_STATUS = "3350.50";
  public static final String CHNG_STATUS_BCKRM_PCK_TRNSCTN = "CHNG_STATUS_BCKRM_PCK.0001.ex";
  public static final String ORDER_RELEASE_KEY = "OrderReleaseKey";
  public static final String SELECT_METHOD = "SelectMethod";
  public static final String GC_EXSTORE_FLMNT_TYPE = "GC_EXSTR_FLMNT_TYPE";
  public static final String ASSO_LINE_NO = "AssocLineNumber";
  public static final String GET_SORTED_SHIP_DTL_TEMPLATE =
      "<Shipment><Extn /><ShipNode Description='' ShipNode=''><ShipNodePersonInfo /></ShipNode><ToAddress />"
          + "<Containers><Container><ContainerDetails><ContainerDetail><ShipmentLine><OrderLine><Extn/>"
          + "<ItemDetails><PrimaryInformation/></ItemDetails></OrderLine><Extn /></ShipmentLine>"
          + "</ContainerDetail></ContainerDetails><Extn />"
          + "</Container></Containers><ShipmentLines><ShipmentLine><OrderLine><Extn/><ItemDetails><PrimaryInformation/></ItemDetails></OrderLine><Extn /></ShipmentLine></ShipmentLines></Shipment>";
  public static final String SORTED_SHIPMENT_API = "getSortedShipmentDetails";
  public static final String TO_ADDRESS = "ToAddress";
  public static final String PACK_SLIP_SERVICE = "GCPrintPackSlipService";
  public static final String SO_READY_FOR_CUST_TRNSCTN = "GC_ORD_READY_FOR_CUST.0001.ex";
  public static final String CONFIRM_SHIPMENT_TEMP =
      "<Shipment ShipmentKey=''><ShipmentLines><ShipmentLine OrderLineKey='' Quantity='' /></ShipmentLines></Shipment>";
  public static final String GC_EXSTORE_USR_AUTH = "GC_EXSTORE_USR_AUTH";
  public static final String AUTH_USER = "storeauthuser";
  public static final String GC_HOUSE_NO_EXTN = "GC_HOUSE_NO_EXTN";
  public static final String HOUSE_NO_EXTN = "HouseNoExtn";
  public static final String ENTERPRISE_KEY = "EnterpriseKey";
  public static final String EMAIL_ERROR_DESC = "Error in Sending Email. Please try resending it";
  public static final String ITEMS_ELE = "Items";
  public static final String TAG = "Tag";
  //Shipment Packing---- Create Container-----Ends

  //ShipBackToDC
  public static final String EX_SHP_BCK_TO_DC = "ExShipBackToDC";
  public static final String GC_PRCS_TRNSFRORD_ON_BKORDER_QUEUE = "GCPublishTransferOrderQueue";

  // ExStore------------------------------------------------------Ends

  // GCStore-53 Demand Sync to DAX : Phase-2 : START
  public static final Object RESERVED = "RESERVED";
  public static final Object OPEN_ORDER = "OPEN_ORDER";
  public static final String ALLOCATED = "ALLOCATED";
  public static final String GET_DMND_SMRY_TEMP =
      "<DemandSummary ItemID='' ProductClass='' ShipNode='' UnitOfMeasure='' >"
          + "<Demand DemandType='' Quantity='' ShipNode='' /></DemandSummary>";
  public static final String GET_DMND_SMRY = "getDemandSummary";
  public static final String GET_ORDER_LINELIST_TEMP =
      "<OrderLineList><OrderLine OrderLineKey='' ><BundleParentLine OrderLineKey='' PrimeLineNo='' ><Item ItemID='' ProductClass='' UnitOfMeasure=''/></BundleParentLine>"
          + "<Item ItemID='' /></OrderLine></OrderLineList>";
  public static final String GET_ITEM_LIST_TEMP =
      "<ItemList><Item ItemID='' OrganizationCode='' UnitOfMeasure=''>"
          + "<Components><Component ComponentItemID='' ComponentUnitOfMeasure='' KitQuantity='' /></Components><Extn ExtnSerialComponent='' ExtnPrimaryComponentItemID=''/></Item></ItemList>";
  public static final String TIME_STAMP = "yyyy-MM-dd'T'HH:mm:ss";
  public static final String DATE_TIME_STAMP = "DateTimeStamp";
  public static final String GC_PUBLISH_DMNDMSG_TO_DAX = "GCPublishDemandSyncToDAXService";
  public static final String CONFIRM_SHIPMENT = "ConfirmShipment";
  public static final String RSRV_ORDER = "RSRV_ORDER";
  public static final String UNDERSCORE = "_";
  // GCStore-53 End


  // Order Capture Start

  public static final String SOURCING_CLASSIFICATION_DC_SOURCED = "DC_SOURCED";

  // Order Capture Ends

  // Order Promising::Begin

  public static final String SHIPNODE_CANNOT_BE_BLANK_FOR_SERIAL_REQ = "ShipNode is mandatory for SerialItem Request";
  public static final String GC_PICK = "GC_PICK";
  public static final String PATH_FOR_FIND_INVENTORY_ATG_INTEG_TEMPLATE =
      "/global/template/api/findInventoryATGIntegration.xml";

  public static final String STORE_CLEARANCE = "STORE_CLEARANCE";

  public static final String SHIP_TO_CUST_ONHAND = "SHIP_TO_CUST_ONHAND";
  public static final String PICKUP_IN_STORE_ONHAND = "PICKUP_IN_STORE_ONHAND";
  public static final String PICKUP_IN_STORE = "PICKUP_IN_STORE";


  public static final String EXTN_PICK_UP_STORE_ID = "ExtnPickupStoreID";
  public static final String RESERVE_AVAILABLE_INVENTORY = "reserveAvailableInventory";

  public static final String QTY_TO_BE_CANCELLED = "QtyToBeCancelled";
  public static final String API_GET_RESERVATION = "getReservation";
  public static final String API_CANCEL_RESERVATION = "cancelReservation";
  //Added RestrictShipfromStore attribute for low value item--GCSTORE-6865/6864
  public static final String GET_ITEM_LIST_TEMPLATE_ORDER_PROMISING =
      "<ItemList><Item><PrimaryInformation UnitCost='' ItemType='' DefaultProductClass='' ProductLine='' "
          + "PrimarySupplier='' KitCode='' /> <Extn ExtnSetCode='' ExtnIsUsedOrVintageItem='' ExtnIsClearanceItem='' ExtnIsPlatinumItem='' ExtnRestrictShipFromStore=''/> <Components> <Component/> </Components> </Item></ItemList>";
  public static final String ITEM_DOES_NOT_EXIST_IN_OMS = "Item does not exist in OMS";

  // Order Promising::End

  // POS Inventory Sync -- Start
  // Constants for Template XML's
  //Added ItemKey for 6934
  public static final String GET_ITEM_LIST_API_TMPL = new StringBuilder("<ItemList TotalItemList = '' >")
  .append("<Item ItemID = '' ItemKey= '' >").append("<PrimaryInformation Status = '' />")
  .append("<InventoryParameters ATPRule='' TagControlFlag='' />").append("<Extn ExtnPOSItemID = '' ExtnIsPlatinumItem='' />")
  .append("</Item>").append("</ItemList>").toString();


  public static final String GC_POS_INV_SYNC_EXCEPTION_TYPE = "GCPOSInvSyncException";
  public static final String GC_POS_INVENTORY_SYNC_ALERT_Q = "GC_POS_INV_SYNC_ALERT";
  public static final String FLOW_INV_SYNC_SERVICE_NAME = "GCLoadPOSInventoryUpdateService";
  public static final String SUPPLY_TYPE_ONHAND = "ONHAND";
  public static final String TRACK_VAL = "TRACK";

  public static final String GET_INV_SUPPLY_TMP =
      "<Item ItemID='' OrganizationCode='' ProductClass='' UnitOfMeasure=''><Supplies><InventorySupply OrganizationCode='' Quantity='' ShipNode='' >"
          + "<Tag BatchNo='' LotAttribute1=''/></InventorySupply></Supplies></Item>";
  public static final String EOF_WAIT_TIME = "yfs.eof.wait.time";

  // POS Inventory Sync -- End

  // WCC GCSTORE 131 & 132 - Begin
  public static final String LOGIN_ID = "LoginID";
  public static final String IS_STORE_USER = "IsStoreUser";
  public static final String GET_USER_HEIRARCHY_API = "getUserHierarchy";
  public static final String GET_USER_HEIRARCHY_TEMPLATE =
      "<User Username='' Loginid=''><Extn /><UserGroupLists><UserGroupList UsergroupKey=''> <UserGroup UsergroupId=''/>"
          + "</UserGroupList></UserGroupLists></User>";
  public static final String EXTN_EMPLOYEE_ID = "ExtnEmployeeID";
  public static final String GC_CSR_GROUP = "GC_CSR_GROUP";
  public static final String GC_STORE_GROUP = "GC_STORE_GROUP";
  public static final String LOGIN_API = "login";
  public static final String GET_STATE_REGION_LIST_TEMP =
      "<Regions><RegionSchema RegionSchemaKey=''><Region RegionDescription='' RegionLevelName='' RegionName=''/></RegionSchema></Regions>";

  // WCC GCSTORE 131 & 132 - End


  // Payment Processing Phase2 -- Start
  public static final String AUTH_RETRY_KEY = "GCAuthRetryKey";
  public static final String PAYPAL_AUTH_ALERT_SERVICE = "GCPaypalAuthAlertService";
  public static final String MAX_RETRY_COUNT = "2";
  public static final String GET_CHARGE_TRANSACTION_LIST_TEMP =
      "<ChargeTransactionDetails><ChargeTransactionDetail AuthorizationID=''/></ChargeTransactionDetails>";
  public static final String GET_CHARGE_TRANSACTION_LIST_API = "getChargeTransactionList";
  public static final String PAYPAL_RESPONSE_FAILURE = "Failure";
  public static final String PAYPAL_RESPONSE_SUCCESS = "Success";
  public static final String PAYPAL_HARD_DECLNE = "HardDecline";
  public static final String PAYPAL_AUTH_VOID_PERIOD = "7"; // Changed from 3 to 7 fot GCSTORE 5673
  public static final String PAYPAL_ERROR_CODES_COMMON_CODE = "GCPaypalErrorCodes";
  public static final String PAYPAL_DO_VOID_API = "doVoid";
  public static final String PAYPAL_DO_AUTHORIZATION_API = "doAuthorization";
  public static final String PAYMENT_STATUS_PAID = "PAID";
  public static final String PAYPAL_CAPTURE_COMPLETE = "COMPLETE";
  public static final String PAYPAL_CAPTURE_NOTCOMPLETE = "NOTCOMPLETE";
  public static final String PAYPAL_AUTHORIZATION_VOIDED = "Voided";
  public static final String PAYPAL_AUTHORIZATION_EXPIRED = "Expired";
  public static final String PAYPAL_DO_CAPTURE_API = "doCapture";
  public static final String PAYPAL_REFUND_TRANSACTION_API = "refundTransaction";
  public static final String PAYPAL_CAPTURE_ALERT_SERVICE = "GCPaypalCaptureAlertService";
  public static final String PAYPAL_REFUND_ALERT_SERVICE = "GCPaypalRefundAlertService";
  
  //GCSTORE-6307-starts
  public static final String PAYPAL_RESPONSE_SUCCESS_WITH_WARNING = "SuccessWithWarning";
  //GCSTORE-6307-ends

  // Payment Processing Phase2 -- End

  // Order Capture :Start
  public static final String ORDER_CATURE_SYSTEM_ATG = "ATG";
  public static final String SOURCE_SYSTEM_JBS = "JBS";
  public static final String GET_ITEM_LST_API_TMP_FOR_UNQ_ITEM =
      "<ItemList TotalNumberOfRecords=''><Item ItemID='' OrganizationCode='' UnitOfMeasure='' ><Extn ExtnIsUniqueItem='' ExtnSerialComponent='' /><Components><Component ComponentItemID='' KitQuantity=''/></Components><PrimaryInformation KitCode=''/></Item></ItemList>";
  public static final String INV_REQ_ATT_FOR_SERIAL_BUNDLE = "InvRequestAttForSerialBundle";
  // Order Capture :End


  // Low Value::Begin
  public static final String GC_LOW_VALUE_THRESHOLD = "GCLowValueThreshold";
  public static final String FIVE = "05";
  public static final String FOUR = "04";
  public static final String VERSION_ONE = "V1";
  public static final String GET_COMMON_CODE_LIST_LOW_VALUE_TEMPELATE =
      "<CommonCodeList><CommonCode CodeShortDescription='' CodeType='' CodeValue='' CommonCodeKey='' DocumentType='' OrganizationCode='' /></CommonCodeList>";
  // Low Value::End

  // GCStore-154 start
  public static final String DCEMPLOYEE = "DCEMPLOYEE";
  public static final String EMPLOYEE_PICKUP_DESC = "Employee Pickup";
  // GCStore-154 End

  // Customer Sync- Begin

  public static final String ATG_INTEGRATION_SOAP_ACTION = "ATG_SOAP_ACTION";
  public static final String ATG_CUSTOMER_CREATE_WEB_SERVICE_URL = "ATG_CUSTOMER_CREATE_WEB_SERVICE_URL";
  public static final String GC_POS_INTEGRATION_SERVICE = "GCPOSIntegrationService";
  public static final String GC_RAISE_CUSTOMER_SYN_FAILURE_ALERT_SERVICE = "GCRaiseCustomerSyncAlert";
  public static final String POS_WEBSERVICE_URL = "POS_CUSTOMER_CREATE_WEB_SERVICE_URL";
  public static final String POS_WEBSERVICE_ACTION = "POS_CUSTOMER_CREATE_WEBSERVICE_ACTION";
  public static final Object TAX_EXEMPT_APPROVED = "Approved";
  public static final Object TAX_EXEMPT_INPOGRESS = "InProgress";
  public static final Object TAX_EXEMPT_DECLINED = "Declined";

  // Customer Sync- End
  //GCStore-187 & 193- Begin
  public static final String MULTIP_API_INPUT ="<MultiApi> <API Name='getSurroundingNodeList'> <Input>" +
      "<GetSurroundingNodeList DistanceToConsider='999999' DistanceToConsiderUOM='MILE' FulfillmentType='PICKUP_IN_STORE' "+ "NodeType='DC' MaximumRecords='5000' OrganizationCode=''> " +
      "<ShipToAddress City='' State='' Country='' />"+
      "</GetSurroundingNodeList> </Input> <Template> <GetSurroundingNodeList>" +
      "<NodeList><Node DistanceFromShipToAddress='' NodeOrgCode='' DistanceUOM='' Description='' >"+
      "<ShipNodePersonInfo/><Organization><Extn/></Organization></Node></NodeList>"
      +
      "</GetSurroundingNodeList></Template> </API>" +
      "<API Name='getSurroundingNodeList'> <Input> "+
      "<GetSurroundingNodeList DistanceToConsider='50' DistanceToConsiderUOM='MILE'  NodeType='Store' " +
      "MaximumRecords='5000' OrganizationCode=''> <ShipToAddress City='' State='' Country='' /> " +
      "</GetSurroundingNodeList> </Input> <Template> <GetSurroundingNodeList>" +
      "<NodeList><Node DistanceFromShipToAddress='' NodeOrgCode='' DistanceUOM='' Description='' >"+
      "<ShipNodePersonInfo/> <Organization><Extn/></Organization></Node></NodeList>"
      +
      "</GetSurroundingNodeList></Template> </API> </MultiApi>";


  public static final String GET_SURROUNDING_NODE_LIST_ELEMENT = "GetSurroundingNodeList";
  public static final String GET_SURROUNDING_NODE_LIST = "getSurroundingNodeList";
  public static final String GC_FIND_INVENTORY_WRAPPER_SERVICE = "GCFindInventoryWrapperService";
  public static final String NODE_SEARCH = "NodeSearch";
  public static final String GET_SURROUNDING_NODE_LIST_TEMPLATE = "<GetSurroundingNodeList> <ShipToAddress/> <NodeList>"+
      " <Node DistanceFromShipToAddress='' NodeOrgCode='' DistanceUOM='' Description='' >" +
      " <ShipNodePersonInfo/> " +
      "</Node>  </NodeList> </GetSurroundingNodeList>";
  public static final String NODE_LIST = "NodeList";
  public static final String SHIP_NODES = "ShipNodes";
  public static final String SHIP_TO_ADDRESS = "ShipToAddress";
  public static final String GET_SURROUNDING_NODE_LIST_INPUT = "<GetSurroundingNodeList DistanceToConsider='50' " +
      " DistanceToConsiderUOM='MILE' FulfillmentType='PICKUP_IN_STORE' " +
      " MaximumRecords='5000' OrganizationCode=''> " +
      "  </GetSurroundingNodeList> ";
//GCSTORE - 7147
  public static final String ORDER_PROMISE_INPUT =
      "<Promise ConsiderPartialShipment='Y' OrganizationCode=''  FulfillmentType='PICKUP_IN_STORE' MaximumRecords='5'  AggregateSupplyOfNonRequestedTag='N'> "
          +
          " <PromiseLines>    <PromiseLine  ItemID='' RequiredQty='99999' UnitOfMeasure='' ProductClass='' LineId='1' > " +
          "<ShipNodes/> " +
          " <Extn ExtnIsStoreClearance=''  ExtnUnitPrice='' /> " +
          " </PromiseLine>  </PromiseLines> </Promise> " ;


  //GCStore-187 & 193 End
  //GCSTORE-209,843 Start
  public static final String POST_PICK_MESSAGE_POS_SERVICE = "GCPostPickMessageToPOSService";
  public static final String GC_RSRV_EXPIRATION = "GCRsrvExpiration";
  //GCSTORE-209,843 End

  // GCSTORE-348,235,760 CustomerOrderSync:Start
  public static final String MANAGE_CUSTOMER_API_TEMP =
      "<Customer><Extn/><CustomerContactList><CustomerContact><CustomerAdditionalAddressList><CustomerAdditionalAddress><PersonInfo>"
          + "<Extn/></PersonInfo></CustomerAdditionalAddress></CustomerAdditionalAddressList></CustomerContact></CustomerContactList></Customer>";

  // GCSTORE-348,235,760 CustomerOrderSync:End

  // GCSTORE-315, 319, 329, 332, 333 Sourcing Attributes Start
  public static final String DC_SOURCED_SOURCING = "DC_SOURCED";
  public static final String PREPAID_SOURCING = "PREPAID";
  public static final String PREPAID_PAYMENT = "GCPrepaidPayment";
  public static final String SHIP_2_CUSTOMER = "SHIP_2_CUSTOMER";
  public static final String SHIP_2_CUSTOMER_LOW_VALUE = "SHIP_2_CUSTOMER_LV";
  public static final String PICKUP_IN_STORE_LOW_VALUE = "PICKUP_IN_STORE_LV";
  public static final String SHIP_2_CUSTOMER_STORE_CLEARANCE = "SHIP_2_CUSTOMER_SC";
  public static final String PICKUP_IN_STORE_STORE_CLEARANCE = "PICKUP_IN_STORE_SC";
  public static final String SHIP_2_CUSTOMER_ONHAND = "SHIP_2_CUSTOMER_ONHAND";
  public static final String GC_SHIP_SINGLE_NODE = "GC_SHP_SGL";
  public static final String GC_DEFAULT = "GC_DEFAULT";
  // GCSTORE-1119 Start
  public static final String ATG_WS_TIME_OUT = "ATGWebServiceTimeout";
  public static final String POS_WS_TIME_OUT = "POSWebServiceTimeout";
  public static final String WEBSERVICE_TIMEOUT = "gc.webservice.defaultTimeOut";
  //GCSTORE-1119 End

  // GCSTORE-315, 319, 329, 332, 333 Sourcing Attributes End

  public static final String GET_CUSTOMER_DETAILS_TEMPLATE =
      "<Customer><CustomerSchedulingPreferences/><Extn ExtnTaxExemptReq='' ExtnTaxExemptReqDate='' ExtnTaxExemptState='' ExtnTaxExemptReqStatus='' ExtnTaxExemptReqValidUntil='' />></Customer";
  public static final String GET_CUSTOMER_DETAILS = "getCustomerDetails";
  public static final String GET_CUSTOMER_LIST_TEMPLATE =
      "<Customer><CustomerSchedulingPreferences/><Extn ExtnTaxExemptReq='' ExtnTaxExemptReqDate='' ExtnTaxExemptState='' ExtnTaxExemptReqStatus='' ExtnTaxExemptReqValidUntil='' ExtnSourceCustomerID='' ExtnIsGcProCustomer='' ExtnGcProStatus='' ExtnGcProAccManager=''/></Customer>";
  public static final String EXTN_SOURCE_CUSTOMER_ID = "ExtnSourceCustomerID";
  public static final String GCSTORE = "GCSTORE";
  public static final String INPROGRESS = "InProgress";

  // GCSTORE- Pack Detail Starts - 239,243,251,249,288,290,292
  public static final String GET_SHPMNT_DETAIL_TMP = "<Shipment DeliveryMethod='' EnterpriseCode='' LevelOfService='' Status='' ShipmentNo='' ShipNode='' SCAC='' "
      + "SellerOrganizationCode='' ShipmentType='' ExpectedShipmentDate=''"
      +" ScacAndService='' ShipmentKey='' UserName='' OrderingStore='' DocumentType='' ExpectedDeliveryDate='' FulfillmentType='' OrderHeaderKey='' RequestedShipmentDate='' ShipDate='' >"
      + "<ShipNode Description='' ShipNode=''><ShipNodePersonInfo /></ShipNode><ToAddress FirstName='' LastName='' MobilePhone='' AddressLine1='' AddressLine2='' City='' Company='' Country='' ZipCode='' State='' />"
      + "<BillToAddress FirstName='' LastName='' MobilePhone='' DayPhone='' AddressLine1='' AddressLine2='' AddressLine3='' City='' Company='' Country='' ZipCode='' State=''/>"
      +"<Extn ExtnOrderDate='' ExtnFulfillmentType='' ExtnExpediteFlag ='' ExtnOrderingStore='' /> <ShipmentLines>"
      +"<ShipmentLine BackroomPickedQuantity='' Quantity='' ProductClass='' PrimeLineNo='' ItemID='' ActualQuantity='' SubLineNo='' UnitOfMeasure='' OrderNo='' ShipmentLineKey='' ShipmentKey=''"
      +" OrderHeaderKey='' OrderLineKey='' ItemDesc='' ParentShipmentLineKey='' > <ShipmentTagSerials TotalNumberOfRecords=''> <ShipmentTagSerial BatchNo='' /> </ShipmentTagSerials>"
      +"<Order OrderDate='' CustomerFirstName='' CustomerLastName='' CustomerPhoneNo='' CustomerEMailID='' OrderNo='' DocumentType='' EnterpriseCode='' ><Extn /></Order>"
      +"<OrderLine DependentOnLineKey='' OrderLineKey='' FulfillmentType='' KitCode='' OrderedQty='' ><LinePriceInfo UnitPrice=''></LinePriceInfo><ItemDetails>"
      +"<PrimaryInformation ShortDescription='' ColorCode='' DefaultProductClass='' Description='' EffectiveStartDate='' ExtendedDescription='' ExtendedDisplayDescription='' ImageID=''"
      +" ImageLocation='' /><Extn  ExtnPOSItemID='' ExtnBrandName='' /></ItemDetails>"
      +"<LineCharges><LineCharge ChargeCategory='' ChargeName='' ChargePerLine=''/></LineCharges><Extn /></OrderLine>"
      +"<PersonInfoShipTo FirstName='' LastName='' MobilePhone='' AddressLine1='' AddressLine2='' City='' Company='' Country='' ZipCode='' /><PersonInfoBillTo FirstName='' LastName=''"
      +" MobilePhone='' AddressLine1='' AddressLine2='' AddressLine3='' City='' Company='' Country='' ZipCode='' /></ShipmentLine></ShipmentLines>"
      + "<Containers TotalNumberOfRecords=''><Container ContainerNo='' SCAC='' ShipmentContainerKey='' ShipmentKey='' TrackingNo=''><Extn /><ContainerDetails TotalNumberOfRecords=''>"
      +"<ContainerDetail ContainerDetailsKey='' ItemID='' ProductClass='' Quantity='' ShipmentContainerKey='' ShipmentKey='' ShipmentLineKey='' UnitOfMeasure='' >"
      +"<ShipmentLine DocumentType=''  ItemDesc='' OrderNo='' PrimeLineNo='' Quantity='' ReleaseNo='' ParentShipmentLineKey='' ShipmentLineNo='' ShipmentSubLineNo='' SubLineNo=''>"
      + "</ShipmentLine></ContainerDetail>"
      +"</ContainerDetails></Container></Containers></Shipment>";
  public static final String QTY_AVAIL_TO_PACK = "QtyAvailableToPack";
  public static final String IMAGE_LOCATION = "ImageLocation";
  public static final String IMAGE_ID = "ImageID";
  public static final String ITEM_BRAND = "ItemBrand";
  public static final String ITEM_IMAGE = "ItemImage";
  public static final String NO_OF_ITEMS = "NoOfItems";
  public static final String GET_SHIPMENT_LIST_TEMP =
      "<Shipments><Shipment DeliveryMethod='' EnterpriseCode='' LevelOfService='' Status='' ShipmentNo='' ShipNode='' SCAC='' SellerOrganizationCode='' ShipmentType='' "
          + "ExpectedShipmentDate='' ScacAndService='' ShipmentKey='' UserName='' OrderingStore='' DocumentType='' ExpectedDeliveryDate='' FulfillmentType='' OrderHeaderKey='' RequestedShipmentDate='' "
          + "ShipDate='' ><Extn /><ToAddress /><BillToAddress /><ShipNode /><Containers><Container ContainerNo='' SCAC='' ShipmentContainerKey='' ShipmentKey='' TrackingNo='' ><Extn /><ContainerDetails>"
          + "<ContainerDetail><ShipmentLine /></ContainerDetail></ContainerDetails></Container></Containers>"
          + "<ShipmentLines><ShipmentLine><OrderLine DerivedFromOrderHeaderKey='' DerivedFromOrderLineKey='' DependentOnLineKey='' OrderLineKey='' FulfillmentType='' KitCode='' OrderedQty='' ><Extn /><ItemDetails >"
          + "<PrimaryInformation ShortDescription='' ImageLocation='' ImageID='' /><Extn ExtnBrandName='' ExtnPOSItemID='' /></ItemDetails><LineCharges><LineCharge /></LineCharges><LinePriceInfo />"
          + "</OrderLine></ShipmentLine></ShipmentLines></Shipment></Shipments>";
  public static final String GET_ITEM_LIST_TEMP_PACK = "<ItemList TotalNumberOfRecords=''><Item ItemID='' ProductClass='' UnitOfMeasure=''>"
      + "<PrimaryInformation ShortDescription='' ImageID='' ImageLabel='' ImageLocation='' KitCode='' UnitCost='' ItemType=''/>"
      +"<Extn ExtnBrandName='' ExtnPOSItemID='' /></Item></ItemList>";
  public static final String ERROR_INFO = "ErrorInfo";
  public static final String POS_ERR_MSG = "SKU Validation Failed : ";
  public static final String GET_SHPMNT_LIST_TEMP_DEL = "<Shipments><Shipment><Containers><Container><Extn /><ContainerDetails><ContainerDetail><ShipmentLine />"
      + "</ContainerDetail></ContainerDetails></Container></Containers></Shipment></Shipments>";
  public static final String EXTN_MULTIBOX_GPID = "ExtnMultiBoxGroupID";
  public static final String UNPACK_SHIP_TEMP =
      "<Shipment ShipmentKey='' ShipmentNo='' ContainerizedQuantity='' TotalQuantity='' Status='' />";
  public static final String UNPACK_SHIPMENT = "unpackShipment";
  public static final String TOTAL_QTY = "TotalQuantity";
  public static final String CONTAINERIZED_QTY = "ContainerizedQuantity";
  public static final String UNDO_PACK_SHIP_COMPLETE = "UNDO_PACK_SHMT_COMPLETE";
  public static final String SHIPMENT_BEING_PACKED = "1100.70.06.70";
  public static final String GC_PACK_DETAIL_SHPLIST = "GCPackDetailShipList";
  public static final String GET_SHPMNT_LIST_TEMP_CANCEL = "<Shipments><Shipment><Extn /><ShipmentLines><ShipmentLine><OrderLine><Order><Extn /></Order><ItemDetails><Extn /></ItemDetails><Extn/></OrderLine><Extn /></ShipmentLine></ShipmentLines></Shipment></Shipments>";
  public static final String UNDO_PICK = "UndoPick";
  public static final String UNDO_BACKROOM_PICK_TRANS = "YCD_UNDO_BACKROOM_PICK";
  public static final String UNDO_BACKROOM_PICK = "undoBackRoomPick";
  public static final String SENT_TO_FULFILMENT_CENTER = "3350.50";
  public static final String GC_READY_FOR_BACKROOM_PICKUP = "CHNG_STATUS_BCKRM_PCK.0001.ex";
  public static final String CHANGE_ORDER_STATUS = "changeOrderStatus";
  public static final String HAS_GIFT_ITEM = "HasGiftItem";
  public static final String EMP = "EMP";
  public static final String RELATIONSHIP_TYPE = "RelationshipType";
  public static final String GC_EMPLOYEE_ORDER = "GCEMPLOYEEORDER";
  public static final String CHNG_STATUS_ON_SHIP_CNFRM = "CHANGE_ON_SHIP_CONFIRM.0001.ex";
  public static final String SHIPPED_TO_CUSTOMER_VAL = "3700.005";
  public static final String PENDING_RECEIPT_VAL = "3700.006";
  public static final String SHIP_CONTAINER_KEY = "ShipmentContainerKey";
  public static final String CONFIRM_SHIPMENT_API = "confirmShipment";
  public static final String SHIP_STATUS_UPDATE = "SHIP_STATUS_UPDATE.0001.ex";
  public static final String SHIPPED_TO_CUSTOMER_STATUS = "1400.50";
  public static final String PENDING_RECEIPT_STATUS = "1400.100";
  public static final String PENDING_RECEIPT = "Pending Receipt";
  public static final String GET_SHIP_LIST_TEMP =
      "<Shipments><Shipment ShipmentKey='' SellerOrganizationCode='' ShipNode='' ><Containers><Container ShipmentContainerKey='' /></Containers>"
          + "<ShipmentLines><ShipmentLine ShipmentLineKey='' OrderLineKey='' OrderHeaderKey=''/></ShipmentLines></Shipment></Shipments>";
  public static final String EXTN_CONTAINER_STATUS_DATE = "ExtnContainerStatusDate";
  public static final String CREATE_SHPMNT_INVC = "GC_STORE_SHPMNT_INVOICE.0001.ex";
  public static final String CREATE_SHPMNT_INVC_API = "createShipmentInvoice";
  public static final String SHIPPED_TO_CUST = "Shipped To Customer";
  public static final String POS_CNCL_RSRV_INDOC = "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>"
      + "<soap:Body><CancelRequest xmlns='http://tempuri.org/'><XMLDocIn><Orders xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns='' >"
      + "<Order><MessageType>CANCEL</MessageType><POSSalesTicket /><POSLocation /><CancelALL /><MgrAppv /><OrderDate /><OrderTime />"
      + "<XMLCreatedDate /><XMLCreatedTime /><OrderID /><IPAddress /><SourceCode /><Products /></Order></Orders></XMLDocIn></CancelRequest></soap:Body></soap:Envelope>";
  public static final String PACK_REJECT_LINE = "PACK_REJECT_LINE";
  public static final String PACK_CONFIRM = "PACK_CONFIRM";
  public static final String CANCEL_REQ_ELE = "CancelRequest";
  public static final String ORDERS = "Orders";
  public static final String POS_SALES_TCKT_ELE = "POSSalesTicket";
  public static final String EXTN_POS_SALESTKT_NO = "ExtnPOSSalesTicketNo";
  public static final String CANCEL_ALL_ELE = "CancelALL";
  public static final String MGR_APPRVL_ELE = "MgrAppv";
  public static final String ORDER_TIME = "OrderTime";
  public static final String XML_CREATED_DATE = "XMLCreatedDate";
  public static final String XML_CREATED_TIME = "XMLCreatedTime";
  public static final String ORDER_ID = "OrderID";
  public static final String IP_ADDRESS = "IPAddress";
  public static final String IMAGE_URL = "ImageUrl";
  public static final String EXTN_PRIMARY_COMPONENT_ID = "ExtnPrimaryComponentItemID";
  public static final String ASSOC_LINE_NO = "AssocLineNumber";
  public static final String SHIP = "Ship";
  public static final String REJECT_LINE = "Reject Line";
  public static final String POS_CANCEL_RSRVN_ACTION = "POS_CANCEL_RSRVN_ACTION";
  public static final String POS_CANCEL_RSRVN_URL = "POS_CANCEL_RSRVN_URL";
  public static final String EXTN_IP_ADDRESS = "ExtnIPAddress";
  public static final String EXTN_RSRVN_ID = "ExtnPOSReservationId";
  public static final String EXTN_PENDING_RECEIPT_DATE = "ExtnPendingReceiptDate";
  public static final String IS_COMPLETE_CANCEL = "IsCompleteCancel";
  public static final String GET_ORG_LIST_TEMPLATE = "<OrganizationList>"
      + "<Organization IsEnterprise='' IsHubOrganization='' IsLegalEntity='' IsNode='' OrganizationCode='' OrganizationKey='' OrganizationName='' ParentOrganizationCode='' PrimaryEnterpriseKey='' >"
      + "<Node NodeOrgCode='' NodeType='' ShipnodeKey='' ShipnodeType='' ShippingNode='' /></Organization></OrganizationList>";
  public static final String NODE_TYPE = "NodeType";
  public static final String HASH_TAG = "#";
  public static final String DIGITAL_ITEM_TYPE_10 = "10";
  public static final String CONFIRM_SHIP_TEMP = "<Shipment SellerOrganizationCode='' ShipNode='' ShipmentKey='' />";
  public static final String HASH = "#";
  public static final String BRAND_NAME = "BrandName";
  public static final String ACTUAL_SHIPMENT_DATE = "ActualShipmentDate";
  public static final String IS_BUNDLE_COMPONENT = "IsBundleComponent";
  public static final String CHNG_TO_PENDING_RCPT = "CHNG_TO_PNDNG_RCPT.0005.ex";
  public static final String IS_ORDER_SUMMARY = "IsOrderSummary";
  public static final String PO_CREATE_SHIP_INVOICE = "GC2_DROP_SHIP_INVOICE.0005.ex";
  // GCSTORE - PackDetail ends






  public static final String API_ORDER_AUDIT_LIST = "getOrderAuditList";
  public static final String ORDER_AUDIT_LEVEL = "OrderAuditLevel";
  public static final String MODIFICATION_TYPE = "ModificationType";
  public static final String ORDER_AUDIT_LIST = "OrderAuditList";
  public static final String ORDER_AUDIT = "OrderAudit";
  public static final String API_GET_COMPLETE_ORDER_DETAILS = "getCompleteOrderDetails";
  // GCSTORE-328 End

  public static final String APPROVED_LOWERCASE = "Approved";
  // GCSTORE-328 End

  // CR-16 Start
  public static final String GC_PAYPAL_REFUND_DIST = "GCPaypalRefundDist";
  public static final String GC_CHANGE_PP_REFUND_DIST_SERVICE = "GCChangePayPalRefundDistribution";
  public static final String GC_CREATE_PP_REFUND_DIST_SERVICE = "GCCreatePayPalRefundDistributionService";
  public static final String GC_GET_PP_REFUND_DIST_LIST_SERVICE = "GCGetPayPalRefundDistributionListService";
  public static final String PAYPAL_AUTH_HD_ALERT_SERVICE = "GCPaypalAuthHardDeclineAlertService";
  public static final String PAYPAL_CAPTURE_HD_ALERT_SERVICE = "GCPaypalCaptureHardDeclineAlertService";
  public static final String PAYPAL_REFUND_HD_ALERT_SERVICE = "GCPaypalRefundHardDeclineAlertService";

  // CR-16 End

  // Customer Pick Up Start
  public static final String GET_INP_SHIP_TMP = "<Shipment><Extn ExtnPickUpStore=''/><Containers><Container>"
      + "<Extn ExtnCartonStatus='ReadyForPickUp'/></Container></Containers><ShipmentLines><ShipmentLine OrderNo=''/>"
      + "</ShipmentLines></Shipment>";
  public static final String PICK_UP_STORE = "PickUpStore";
  public static final String GET_OUT_SHIP_TMP =
      "<Shipments><Shipment><Extn/><BillToAddress/><ToAddress/><Containers><Container><Extn/>"
          + "<ContainerDetails><ContainerDetail Quantity=''><ShipmentLine ItemDesc='' Quantity=''/></ContainerDetail></ContainerDetails>"
          + "</Container></Containers><Extn/><ShipmentLines><ShipmentLine/></ShipmentLines></Shipment></Shipments>";
  public static final String EXTN_PICK_UP_STORE = "ExtnPickupStoreID";
  public static final String GET_SHIPMENT_LIST_OUT_TMP =
      "<Shipments><Shipment><Extn/><BillToAddress/><ToAddress/><Containers><Container><Extn/>"
          + "<ContainerDetails><ContainerDetail OrderLineKey='' OrderReleaseKey='' ShipmentContainerKey='' ContainerDetailsKey='' ShipmentKey='' OrderHeaderKey='' Quantity='' ProductClass=''  DocumentType=''><Extn/><OrderLine><Extn/></OrderLine><ShipmentLine ItemDesc='' OrderReleaseKey='' ParentShipmentLineKey='' ItemID='' Quantity='' OrderNo='' UnitOfMeasure=''/>"
          + "</ContainerDetail></ContainerDetails></Container></Containers><ShipmentLines><ShipmentLine ItemDesc='' OrderNo='' ShipmentLineKey='' ItemID='' ShipmentKey='' OrderHeaderKey='' Quantity='' OrderLineKey='' UnitOfMeasure='' DocumentType=''><OrderLine ChainedFromOrderHeaderKey=''  ChainedFromOrderLineKey=''/></ShipmentLine></ShipmentLines></Shipment></Shipments>";
  public static final String EXTN_PICK_UP_DATE = "ExtnShipDate";
  public static final String EXTN_STATUS_DATE = "ExtnStatusDate";
  public static final String CONTAINER_DETAILS_KEY = "ContainerDetailsKey";
  public static final String GET_COMMON_CODE_OP_TMP = "<CommonCodeList><CommonCode CodeType='' CodeShortDescription='' CodeValue=''/></CommonCodeList>";
  public static final String COMMON_CODE_LIST = "CommonCodeList";
  public static final String EXTN_MULTI_BOX_GROUP_ID = "ExtnMultiBoxGroupID";
  public static final String READY_FOR_CUSTOMER_PICK_UP = "Ready for Customer Pickup";
  public static final String IS_MULTIBOX = "IsMultibox";
  public static final String PICK_UP_PERSON_NAME="PickUpPersonName";
  public static final String CHANGE_SHIPMENT_OUT_TEMP="<Shipment><Containers><Container ShipmentContainerKey=''><ContainerDetails><ContainerDetail Quantity=''><Extn ExtnPickedUpQty=''/></ContainerDetail></ContainerDetails><Extn ExtnContainerStatus=''/></Container></Containers></Shipment>";
  public static final String CUSTOMER_PICKED_UP = "Customer PickedUp";
  public static final String CUSTOMER_PICKED_UP_SHIPMENT_STATUS = "1400.300";
  public static final String CUSTOMER_PICKED_UP_SHP_TRANID = "GC_SHP_CUST_PICK_UP.0001.ex";
  public static final String CHANGE_ORDER_STATUS_TEMP = "<Order OrderHeaderKey=''/>";
  public static final String CUSTOMER_PICKED_UP_ORD_TRANID = "GC_ORD_CUST_PICK_UP.0001.ex";
  public static final String BILLING_CUSTOMER_NAME = "BillingCustomerName";
  public static final String CUSTOMER_PICKED_UP_ORDER_STATUS = "3700.008";
  public static final String GET_ORDER_LIST_OUT_TEMP =
      "<OrderList><Order OrderNo='' OrderDate='' BillToID='' OrderHeaderKey='' LevelOfService=''><Extn ExtnOrderingStore=''/><PersonInfoShipTo/><PersonInfoBillTo/><OrderLines><OrderLine><Item ItemID='' ItemDesc='' ProductClass=''/><Extn/><LinePriceInfo UnitPrice=''/><LineCharges><LineCharge ChargeCategory='' ChargeAmount='' ChargePerLine=''/></LineCharges></OrderLine></OrderLines><PaymentMethods><PaymentMethod PaymentType='' CreditCardType='' DisplayCreditCardNo=''/></PaymentMethods></Order></OrderList>";
  public static final String BILL_TO_CUSTOMER_ID = "BillToCustomerId";
  public static final String GC_PRINT_PICK_SLIP = "GCPrintPickupSlipService";
  public static final String GC_ORDER_COMMISSION = "GCOrderCommission";
  public static final String GET_COMMISSION_LIST_OUT_TEMP =
      "<GCOrderCommissionList><GCOrderCommision UserName=''/></GCOrderCommissionList>";
  public static final String GET_ORDER_COMMISSION_LIST_SERVICE = "GCOrderCommissionListService";
  public static final String EXTN_POS_SALES_TICKET_NO = "ExtnPOSSalesTicketNo";
  public static final String GET_ORDER_LIST_TMP_ORD_STORE =
      "<OrderList><Order><Extn ExtnOrderingStore=''/></Order></OrderList>";
  public static final String GET_ORGANIZATION_LIST_TEMP =
      "<OrganizationList><Organization OrganizationCode=''><Node Description=''/></Organization></OrganizationList>";
  public static final String PICKUP_USER_ACTION = "Save and Print";
  public static final String GET_ITEM_LIST_PICKUP_TMP =
      "<ItemList><Item ItemID=''><Extn ExtnPOSItemID=''/></Item></ItemList>";
  public static final String GC_ID_TYPES = "GC_ID_TYPES";
  public static final String READY_FOR_PICKUP = "Ready For Pickup";
  public static final String PICKUP_AUDIT_REASONCODE = "CUSTOMER_PICK_CONFIRM";
  public static final String GC_CUST_PICKED_UP_EMAIL_SERVICE = "GCCustomerPickedupEmailService";
  public static final String CUSTOMER_PICKED_UP_SHP_TRANID_PO = "CHNG_TO_CUST_PKDUP.0005.ex";
  public static final String CUSTOMER_PICKED_UP_ORD_TRANID_PO = "UPDT_TO_CUST_PICKED_UP.0005.ex";
  public static final String GET_ORDER_LIST_FOR_PURCHASE_ORD = "<OrderList><Order><Extn/></Order></OrderList>";
  public static final String EXTN_PURCHASE_ORDER_NUMBERS = "ExtnPurchaseOrderNumbers";
  // Customer Pick up End


  //GCSTORE-788 Start
  public static final String EXTN_ORDER_ORDERING_STORE = "ExtnOrderingStore";
  public static final String EXTN_PARENT_SCREEN = "ExtnParentScreen";
  public static final String EXTN_CUSTOMER_SCREEN = "CustomerIdentification";
  //GCSTORE-788 End

  // GCSTORE-776 - Begin
  public static final String RESOURCE_ID = "ISCUSR109";
  public static final String STORE_RESOURCE_ID = "ISCUSR104";
  // GCSTORE-776 - End

  // GCSTORE-464 - Begin
  public static final String STOREID = "storeId";
  // GCSTORE-464 - End


  //BML Constants Start

  //BML Constants Start

  public static final String BML_COMMON_CODE = "GCBMLErrorCodes";
  public static final String TWO = "2";
  public static final String BML_AUTH_HD_ALERT_SERVICE = "GCBMLAuthHardDeclineAlertService";
  public static final String BML_AUTH_ALERT_SERVICE = "GCBMLAuthAlertService";
  public static final String BML_WEB_SERVICE_URL = "BML_WEB_SERVICE_URL";
  public static final String BML_APPROVAL_STATUS = "approvalStatus";
  public static final String BML_PROC_STATUS = "procStatus";
  public static final String EXTN_SOURCE_ORDER_NO = "ExtnSourceOrderNo";
  public static final String ORBITAL_CONNECTION_USERNAME = "ORBITAL_CONNECTION_USERNAME";
  public static final String ORBITAL_CONNECTION_PASSWORD = "ORBITAL_CONNECTION_PASSWORD";
  public static final String INDUSTRY_TYPE = "EC";
  public static final String AUTHORIZATION_TRANSACTION_TYPE_CODE = "A";
  public static final String BIN = "000001";
  public static final String TERMINAL_ID = "001";
  public static final String CARD_BRAND = "BL";
  public static final String TAX_ID = "0";
  public static final String BML_ITEM_CATEGORY = "8000";
  public static final String AUTH_RETRY_TRACE_CODE = "[A-Za-z]";
  public static final String BML_RESPONSE_AUTHORIZATION_CODE = "authorizationCode";
  public static final String MERCHANT_ID = "193513";
  public static final String BML_TNC_VERSION = "32103";
  public static final String BML_PRODUCT_DELIVERY_TYPE = "PHY";


  //BML Constants End




  //BML Constants End

  // GCSTORE-516: Design - DC Returns : Start

  public static final String GET_RETURNABLE_SKU_WEBSERVICE_URL = "GET_RETURNABLE_SKU_WEBSERVICE_URL";
  public static final String GC_GET_RETURNABLE_SKU_SERVICE = "GCGetReturnableSKUService";
  public static final String GET_RETURNABLE_SKU_WEBSERVICE_ACTION = "GET_RETURNABLE_SKU_WEBSERVICE_ACTION";
  public static final String GET_ORDERLIST_TEMPLATE_FOR_SKU_UPDATE =
      "<OrderList><Order><OrderLines><OrderLine><OrderStatuses><OrderStatus ShipNode='' Status=''  StatusQty=''/></OrderStatuses><Extn/><ItemDetails><Extn/></ItemDetails></OrderLine></OrderLines></Order></OrderList>";
  public static final String SHIPPED_TO_CUSTOMER_ORDER_LINE_STATUS = "3700.005";
  public static final String CUSTOMER_PICKED_UP_ORDER_LINE_STATUS = "3700.008";
  public static final String KEEP_IN_STORE_DISPOSITION_CODE = "Keep In Store";
  public static final String RETURNED_TO_STORE_DISPOSITION_CODE = "Returned To Store";
  public static final String RECEIVE_IN_STORE_DISPOSITION_CODE = "Receive In Store";
  public static final String GET_ORDERLIST_TEMPLATE_FOR_RETURN_ORDER =
      "<OrderList><Order DraftOrderFlag='' OrderNo=''><OrderLines><OrderLine><Extn/><ItemDetails><PrimaryInformation/><Extn/></ItemDetails></OrderLine></OrderLines></Order></OrderList>";
  public static final String RETURN_INVOICED_STATUS = "3950.01";
  // GCSTORE-516: Design - DC Returns : End

  // Store Return Constants START


  //GCSTORE-241 Start
  public static final String GET_ORG_LIST_INPUT_TO_FILTER_STORE = "<Organization><Extn ExtnIsSourceFromStoreAllowed='Y'/><ComplexQuery><And><Or></Or></And></ComplexQuery></Organization>";
  //GCSTORE-241 End
  public static final String GET_ORG_LIST_TEMPLATE_TO_FILTER_STORE = "<OrganizationList><Organization OrganizationCode=''/></OrganizationList>";



  // Store Return Constants END

  // Pending Receipt starts
  public static final String GET_SHIPMENT_LIST_OP_TMP_FOR_RECEIPT_LIST =
      "<Shipments><Shipment><ShipmentLines><ShipmentLine><OrderLine ChainedFromOrderHeaderKey=''/></ShipmentLine></ShipmentLines><Extn ExtnOrderDate=''  ExtnPickupStoreID='' ExtnPendingReceiptDate=''/><Containers><Container ShipmentKey='' ShipmentContainerKey='' TrackingNo=''><Extn ExtnContainerStatus=''/><ContainerDetails><ContainerDetail><ShipmentLine ItemDesc='' OrderNo='' OrderHeaderKey=''/></ContainerDetail></ContainerDetails></Container></Containers></Shipment></Shipments>";
  public static final String PARTIALLY_READY_FOR_CUSTOMER_PICK_STATUS = "1400.150";
  public static final String READY_FOR_CUSTOMER_PICK_SHP_STATUS = "1400.200";
  public static final String GET_SHIPMENT_LIST_OP_TMP_FOR_RECEIPT_DTLS =
      "<Shipments TotalNumberOfRecords=''><Shipment  ActualShipmentDate='' ShipmentNo='' LevelOfService='' ShipNode=''><BillToAddress/><ToAddress/><Extn ExtnOrderDate='' ExtnFulfillmentType=''/><ShipmentLines><ShipmentLine DocumentType='' ItemID='' ProductClass='' UnitOfMeasure='' OrderLineKey='' Quantity='' OrderNo='' OrderHeaderKey='' ShipmentLineKey='' ItemDesc=''><OrderLine ChainedFromOrderHeaderKey=''/></ShipmentLine></ShipmentLines><Containers><Container ContainerNo='' TrackingNo='' ShipmentContainerKey=''><Extn ExtnContainerStatus=''  ExtnMultiBoxGroupID='' ExtnMultiBoxDesc=''/><ContainerDetails><ContainerDetail ItemID='' ProductClass='' UnitOfMeasure='' ShipmentLineKey='' OrderReleaseKey='' Quantity=''><ShipmentLine ParentShipmentLineKey='' Quantity='' ShipmentLineKey='' ItemDesc=''/><OrderLine><Extn/></OrderLine></ContainerDetail></ContainerDetails></Container></Containers></Shipment></Shipments>";
  public static final String GET_ITEM_LIST_RECEIPT_TMP =
      "<ItemList><Item ItemID=''><Extn ExtnBrandName='' ExtnPOSItemID='' ExtnSetCode='' /><PrimaryInformation ImageID='' ImageLocation='' ShortDescription=''/></Item></ItemList>";
  public static final String SIZE = "Size";
  public static final String OTHER_STORE_ORDERS = "OtherStoreOrders";
  public static final String MULTIBOX_RECEIVED = "Multibox Received";
  public static final String CHANGE_SHIPMENT_OUT_TEMP_RECEIVE =
      "<Shipment Status='' ShipmentKey='' DocumentType=''><Containers><Container ShipmentContainerKey=''><Extn ExtnContainerStatus=''/></Container></Containers></Shipment>";
  public static final String READY_FOR_PICK_UP_SHP_TRANID = "GC_READY_CUST_PICK_UP.0001.ex";
  public static final String READY_FOR_PICKUP_ORD_TRANID = "GC_ORD_READY_FOR_CUST.0001.ex";
  public static final String READY_FOR_CUSTOMER_PICKUP_ORD_STATUS = "3700.007";
  public static final String COMPONENT_RECEIVED = "Component Received";
  public static final String GC_READY_FOR_CUSTOMER_EMAIL_SERVICE = "GCReadyForCustomerEmailService";
  public static final String GET_SHIPMENT_LIST_FOR_RECEIVE_CARTON =
      "<Shipments><Shipment><ShipmentLines><ShipmentLine ShipmentLineKey='' ParentShipmentLineKey='' OrderReleaseKey='' OrderLineKey='' Quantity=''/></ShipmentLines><Containers><Container ShipmentContainerKey=''><Extn ExtnMultiBoxGroupID='' ExtnContainerStatus='' ExtnIsProblemWithCarton=''/><ContainerDetails><ContainerDetail><OrderLine DependentOnLineKey=''><Extn/></OrderLine><ShipmentLine ParentShipmentLineKey=''/></ContainerDetail></ContainerDetails></Container></Containers></Shipment></Shipments>";
  public static final String GC_PRINT_ORDER_PICKUP_NOTICE = "GCReceiptPickupNoticeService";
  public static final String RECEIVE_USER_ACTION = "Receive";
  public static final String RECEIVE_AUDIT_CODE = "RECEIVE_RECEIVED";
  public static final String PROBLEM_USER_ACTION = "Problem with carton";
  public static final String PROBLEM_AUDIT_CODE = "RECEIVE_PWC";
  public static final String ZERO_ZERO_ZERO_FIVE = "0005";
  public static final String READY_FOR_PICK_UP_SHP_TRANID_PO = "CHNG_RDY_FOR_CUST_PKUP.0005.ex";
  public static final String READY_FOR_PICKUP_ORD_TRANID_PO = "UPDT_RDY_FOR_CUST_PKUP.0005.ex";
  public static final String GET_ORDER_LIST_SALES_ORD_TEMP =
      "<OrderList><Order OrderNo='' OrderHeaderKey='' OrderDate=''/></OrderList>";
  public static final String PURCHASE_HEADER_KEY = "PurchaseOrderHeaderKey";
  public static final String PURCHASE_ORDER_NO = "PurchaseOrderNo";
  // Pending Receipt ends



  //Ship back to DC -- START
  public static final String GET_SHIP_LIST_INPUT = "<Shipment DocumentType='0006' ><ComplexQuery Operator='AND' ><And><Or><Exp Name='Status' Value='1100.70.10' QryType='EQ'/><Exp Name='Status' Value='1100.70.20' QryType='EQ'/><Exp Name='Status' Value='1100.70.30' QryType='EQ'/></Or></And></ComplexQuery></Shipment>";
  public static final String IS_SHIP_BACK_TO_DC = "IsShipBackToDC";
  public static final String EXTN_RTRN_ORD_NO = "ExtnReturnOrderNo";
  public static final String EXTN_RETURN_ORDER_NO = "ExtnReturnOrderNo";
  public static final String EXTN_RTRN_ORD_LINEKEY = "ExtnReturnOrderLineKey";
  public static final String EXTN_VPO_REF_NO = "ExtnVPOReferenceNo";
  public static final String GET_SHIP_LIST_TEMP_SHIPBACK_TO_DC = "<Shipments><Shipment ActualShipmentDate='' DocumentType='' SCAC='' ShipNode='' EnterpriseCode='' ShipmentKey='' OrderHeaderKey='' ><ScacAndService ScacAndService='' />"
      + "<Containers><Container ShipmentContainerKey='' ><ContainerDetails><ContainerDetail Quantity='' >"
      + "<ShipmentLine ItemID='' DocumentType='' OrderNo='' ReleaseNo='' PrimeLineNo='' SubLineNo='' ShipmentLineKey='' KitCode='' /></ContainerDetail></ContainerDetails></Container></Containers>"
      + "<ShipmentLines><ShipmentLine ItemID='' DocumentType='' OrderNo='' ReleaseNo='' PrimeLineNo='' SubLineNo='' ShipmentLineKey='' Quantity='' KitCode='' ><OrderLine OrderHeaderKey='' /></ShipmentLine></ShipmentLines></Shipment></Shipments>";
  public static final String GET_SHIP_LIST_MARK_INVC = "<Shipments><Shipment ShipmentKey='' ><ShipmentLines>"
      + "<ShipmentLine Quantity='' OrderHeaderKey='' ReleaseNo='' PrimeLineNo='' SubLineNo='' ShipmentLineKey='' /></ShipmentLines></Shipment></Shipments>";
  public static final String SHP_PACKED_STATUS_TRNSFR_ORD = "1100.70.30";
  public static final String TRNSFR_ORD_SHIP_STATUS_UPDATE = "GC_SHPMNT_STATUS_UPDATE.0006.ex";
  public static final String SHP_BEING_PACKED_STATUS_TRNSFR_ORD = "1100.70.20";
  public static final String READY_FOR_PACKING_TRNSFR_ORD = "1100.70.10";
  public static final String READY_FOR_PACK_TRANSACTION = "GC_READY_FOR_PACKING.0006.ex";
  public static final String INVOKE_SHIPMENT_UE = "InvokeShipmentUE";
  public static final String GC_PRCS_TRNSFRORD_ON_BKORDER = "GCProcessTransferOrderOnBackorder";
  public static final String CONTAINER_KEY = "ContainerKey";
  public static final String SHIP_TO_DC_SHIPPED = "SHIP_TO_DC_SHIPPED";
  public static final String SHIP_TO_DC_INVC = "SHIP_TO_DC_INVC";
  public static final String MARK_AN_INVC = "Mark an INVC";
  public static final String PACK_SLIP_FOR_SHIPBK_TO_DC = "GCPrintPackSlipForTransferOrder";

  // OMS-4727 End

  //INVC Processing Constants - Start
  public static final String CLOSED_BY_INVC = "ClosedByINVC";
  public static final String CLOSED_BY_INVC_CODE = "3950.01.60";
  public static final String RETURNED_TO_STORE_CODE = "3950.01.20";
  public static final String RETURNED_TO_STORE = "ReturnedToStore";
  public static final String GET_ORDERLIST_TEMPLATE_FOR_INVC_PROCESSING = "<OrderList><Order><OrderLines><OrderLine OrderLineKey='' KitQty='' BundleParentOrderLineKey='' PrimeLineNo='' SubLineNo=''>"
      + "<Item ProductClass='' ItemID='' /></OrderLine></OrderLines></Order></OrderList>";
  public static final String GET_ORDER_LINE_LIST_TEMPLATE_FOR_INVC = "<OrderLineList><OrderLine OrderHeaderKey='' OrderLineKey='' OrderedQty='' IsBundleParent='' ReceivingNode='' ShipNode='' Modifyts='' StatusQuantity='' "
      + "PrimeLineNo='' SubLineNo='' ReturnReasonShortDesc=''><Item ItemID='' ProductClass='' ItemShortDesc=''><PrimaryInformation UnitCost=''/></Item><ItemDetails><PrimaryInformation UnitCost=''/></ItemDetails><Order OrderDate='' OrderNo='' EnteredBy='' /><OrderStatuses><OrderStatus Status='' StatusDate='' StatusQty='' />"
      + "</OrderStatuses><Extn ExtnReturnVPONo='' ExtnInvcUpdatedDate='' /><LinePriceInfo UnitPrice='' ListPrice=''/></OrderLine></OrderLineList>";
  public static final String PROCESS_INVC_TRANSACTION_ID = "PROCESS_INVC.0003.ex";
  public static final String FALSE_STRING = "False";
  public static final String TRUE_STRING = "True";
  //INVC Processing Constants - End

  // ShipBackToDC Returns Starts
  public static final String EXTN_RETURN_VPO_NO = "ExtnReturnVPONo";
  public static final String DO_NOT_CONSOLIDATE = "DoNotConsolidate";
  public static final String GET_ORDER_LIST_TMP =
      "<OrderList><Order OrderHeaderKey='' SellerOrganizationCode=''  ><OrderLines><OrderLine OrderedQty='' ReceivingNode='' PrimeLineNo='' SubLineNo='' KitCode=''><BundleParentLine OrderLineKey='' PrimeLineNo='' SubLineNo=''/><Item ItemID='' ProductClass='' UnitOfMeasure=''/>"
          + "<Extn ExtnReturnVPONo=''/></OrderLine></OrderLines></Order></OrderList>";
  public static final String GET_ORDER_LIST_TMPLATE =
      "<OrderList><Order OrderHeaderKey='' SellerOrganizationCode='' OrderNo='' EnterpriseCode='' DocumentType='' ><OrderLines><OrderLine OrderedQty='' ShipNode='' PrimeLineNo='' SubLineNo=''><Item ItemID='' ProductClass='' UnitOfMeasure=''/>"
          + "<OrderStatuses><OrderStatus Status='' StatusQty=''/>"
          + "</OrderStatuses></OrderLine></OrderLines></Order></OrderList>";
  public static final String GET_RELEASE_ORDER_DETAILS_TMP =
      "<OrderRelease DoNotConsolidate='' DocumentType='' EnterpriseCode='' OrderNo='' ReceivingNode='' ReleaseNo='' SellerOrganizationCode='' OrderReleaseKey='' >"
          + "<OrderLines><OrderLine KitCode='' OrderedQty='' PrimeLineNo='' SubLineNo=''>"
          + "<Item ItemID='' ProductClass='' UnitOfMeasure=''/></OrderLine></OrderLines>"
          + "<PersonInfoShipTo ZipCode='' Title='' TaxGeoCode='' Suffix='' State='' PersonInfoKey='' PersonID='' OtherPhone='' MobilePhone='' MiddleName='' LastName='' JobTitle='' "
          + "IsCommercialAddress='' FirstName='' EveningPhone='' EveningFaxNo='' EMailID='' Department='' DayPhone='' DayFaxNo='' Country='' Company='' City='' Beeper='' AlternateEmailID='' "
          + "AddressLine6='' AddressLine5='' AddressLine4='' AddressLine3='' AddressLine2='' AddressLine1='' AddressID='' Longitude='' Latitude='' IsAddressVerified=''/></OrderRelease>";

  public static final String GET_SHIP_NODE_LIST_TMP =
      "<ShipNodeList><ShipNode ShipNode=''><ShipNodePersonInfo Longitude='' Latitude='' ZipCode='' VerificationStatus='' UseCount='' Title='' TaxGeoCode='' "
          + "Suffix='' State='' PreferredShipAddress='' PersonInfoKey='' PersonID='' OtherPhone='' MobilePhone='' MiddleName='' LastName='' JobTitle='' "
          + "IsCommercialAddress='' HttpUrl='' FirstName='' EveningPhone='' EveningFaxNo='' ErrorTxt='' EMailID='' Department='' DayPhone='' DayFaxNo='' "
          + "Country='' Company='' City='' Beeper='' AlternateEmailID='' AddressLine6='' AddressLine5='' AddressLine4='' AddressLine3='' AddressLine2='' "
          + "AddressLine1='' AddressID=''/></ShipNode></ShipNodeList>";
  public static final String TRANSFER_ORDER_DOCUMENT_TYPE = "0006";
  public static final String RECEIVING_NODE = "ReceivingNode";
  public static final String GET_ORDER_LIST_RELEASE_TMPLATE =
      "<OrderReleaseList LastOrderReleaseKey='' TotalOrderReleaseList='' ><OrderRelease OrderNo='' ReleaseNo='' OrderReleaseKey='' /></OrderReleaseList>";
  public static final String TOTAL_ORDER_RELEASE_LIST = "TotalOrderReleaseList";
  public static final String IGNORE_RELEASE_DATE = "IgnoreReleaseDate";
  public static final String DERIVED_FROM_ORDER_LINE = "DerivedFromOrderLine";
  public static final String GET_ORDER_LIST_TEMPLATE_SHIP_BACK =
      "<OrderList><Order OrderHeaderKey='' ><OrderLines><OrderLine  PrimeLineNo='' SubLineNo='' OrderLineKey='' IsBundleParent='' KitQty=''>"
          + "<BundleComponents><BundleComponent PrimeLineNo='' SubLineNo='' KitQty='' /></BundleComponents><Item ItemID='' ProductClass='' />"
          + "<OrderStatuses><OrderStatus Status='' StatusQty='' /></OrderStatuses></OrderLine></OrderLines></Order></OrderList>";
  public static final String API_MULTI = "multiApi";
  public static final String GET_ORDER_LIST_TEMPLATE_TRANSFER =
      "<OrderList><Order OrderHeaderKey='' ><OrderLines><OrderLine  PrimeLineNo='' SubLineNo='' OrderLineKey='' >"
          + "<DerivedFromOrderLine SubLineNo='' PrimeLineNo='' OrderLineKey=''/>"
          + "</OrderLine></OrderLines></Order></OrderList>";
  public static final String IN_TRANSIT_STATUS_CODE = "3950.01.50";
  public static final String RETURN_INVC_STATUS_CODE = "3950.01.40";
  public static final String SHIP_BACK_TO_DC_STATUS_CODE = "3950.01.30";
  public static final String RETURN_VPO_NO = "ReturnVPONo";
  public static final String CHANGE_ORDER_STATUS_INTRANSIT =
      "<API Name='changeOrderStatus'><Input><OrderStatusChange TransactionId='RETURNED.0003.ex'><OrderLines /></OrderStatusChange></Input></API>";
  public static final String CHANGE_ORDER_STATUS_RETURN_INVC =
      "<API Name='changeOrderStatus'><Input><OrderStatusChange TransactionId='PROCESS_INVC.0003.ex'><OrderLines /></OrderStatusChange></Input></API>";
  public static final String CHANGE_ORDER_STATUS_RETURNED_TO_STORE =
      "<API Name='changeOrderStatus'><Input><OrderStatusChange TransactionId='RETURNED_TO_STORE.0003.ex'><OrderLines /></OrderStatusChange></Input></API>";
//GCSTORE-6816 modified the template of  GET_SHIPMENT_LIST_TRANSFER_ORDER_INP and GET_SHIPMENT_LIST_TRANSFER_ORDER_TMP
 public static final String GET_SHIPMENT_LIST_TRANSFER_ORDER_INP = "<Shipment><ComplexQuery Operator='AND'><And><Or><Exp Name='Status' Value='1100' QryType='EQ'/>"
		  + "<Exp Name='Status' Value='1100.70.10' QryType='EQ'/><Exp Name='Status' Value='1100.70.20' QryType='EQ'/><Exp Name='Status' Value='1100.70.30' QryType='EQ'/>"
		  + "</Or></And></ComplexQuery><ShipmentLines><ShipmentLine OrderLineKey=''/></ShipmentLines></Shipment>";
  public static final String GET_SHIPMENT_LIST_TRANSFER_ORDER_TMP = "<Shipments><Shipment ShipmentKey='' ShipmentNo='' Status=''><ShipmentLines>"
      + "<ShipmentLine OrderLineKey='' ShipmentLineKey='' Quantity='' /></ShipmentLines><Containers><Container><ContainerDetails><ConatinerDetail /></ContainerDetails>"
      + "</Container></Containers></Shipment></Shipments>";


  // Pending Receipt ends

  // GCSTORE-635 Start
  public static final String GET_ORGANIZATION_HIERARCHY_OP_TMP_PICK_REQUEST =
      "<Organization OrganizationCode=''><Node NodeType=''/></Organization>";

  // GCSTORE-635 End

  //Phase 2 Order Fulfillment design changes Starts
  public static final String GET_CALENDAR_LIST_API = "getCalendarList";
  public static final String SHIPPING_CAL_KEY = "ShippingCalendarKey";
  public static final String GC_STORE_CALENDAR = "GCStoreCalendar";
  public static final String GET_CALENDAR_LIST_TEMP_XML = "<Calendars><Calendar CalendarId='' CalendarKey='' OrganizationCode=''></Calendar></Calendars>";
  public static final String CALENDAR = "Calendar";
  public static final String CALENDAR_KEY = "CalendarKey";
  public static final String ORDER_LINE_SOURCING_CNTRLS = "OrderLineSourcingControls";
  public static final String ORDER_LINE_SOURCING_CNTRL = "OrderLineSourcingCntrl";
  public static final String SUPPRESS_SOURCING = "SuppressSourcing";
  public static final String EXTN_IS_UNIQUE_ITEM = "ExtnIsUniqueItem";
  public static final String PARENT_ORG_CODE = "ParentOrganizationCode";
  //Phase 2 Order Fulfillment design changes Ends

  //GCSTORE-556 Start
  public static final String STORE_CREDIT_PAYMENT_TYPE = "STORE_CREDIT";
  public static final String RECEIVE_IN_STORE_TRANSACTION = "RECEIVE_IN_STORE.0003.ex";
  public static final String RETURNED_TO_STORE_TRANSACTION = "RETURNED_TO_STORE.0003.ex";
  public static final String RETURNED_TRANSACTION = "RETURNED.0003.ex";
  public static final String RECEIVE_RETURN_TRANSACTION = "RECEIVE_ORDER.0003";
  public static final String RECEIVE_IN_STORE_STATUS = "3950.01.10";
  public static final String RETURNED_TO_STORE_STATUS = "3950.01.20";
  public static final String RETURNED_STATUS = "3950.01.80";
  public static final String GC_PROCESS_KEEP_IN_STORE_RETURNS_SERVICE = "GCProcessKeepInStoreReturnsService";
  public static final String GC_PROCESS_RECEIVE_IN_STORE_RETURNS_SERVICE = "GCProcessReceiveInStoreReturnsService";
  public static final String GC_PROCESS_RETURNED_TO_STORE_RETURNS_SERVICE = "GCProcessReturnedToStoreReturnsService";
  public static final String GC_CREATE_STORE_RETURN_INVOICE_SERVICE = "GCCreateStoreReturnInvoiceService";
  public static final String GC_PROCESS_STORE_RETURNS_SERVICE = "GCProcessStoreReturnsService";
  public static final String CREATE_ORDER_INVOICE_0003 = "CREATE_ORDER_INVOICE.0003";
  public static final String GC_APPROVED_POS_STATUS_CODE = "GCApprovedPOSCode";

  public static final String GC_POST_INV_TRANSFER_REF_TO_POS_SERVICE = "GCPostInvTransferRefToPOSService";
  public static final String GC_70_TICKET_POS_SERVICE = "GC70TicketPOSService";
  public static final String GC_PROCESS_POS_RESPONSE_SERVICE = "GCProcessPOSResponseService";
  public static final String MESSAGE_TYPE_CREATE_70_TICKET = "CREATE70TICKET";
  public static final String MESSAGE_TYPE_RETURN = "RETURN";
  public static final String GC_POS_CONNECT_FAILED_ALERT_SERVICE = "GCPOSConnectFailedAlertService";
  public static final String GC_POS_CONNECT_MAX_RETRY_COUNT = "3";
  public static final String GC_POS_INVOICE_FAILURE_HOLD = "INVOICE_FAILURE_HOLD";

  public static final String GET_70TICKET_WEBSERVICE_URL = "GET_70TICKET_WEBSERVICE_URL";
  public static final String GET_70TICKET_WEBSERVICE_ACTION = "GET_70TICKET_WEBSERVICE_ACTION";
  public static final String GET_RETURN_11TICKET_WEBSERVICE_URL = "GET_RETURN_11TICKET_WEBSERVICE_URL";
  public static final String GET_RETURN_11TICKET_WEBSERVICE_ACTION = "GET_RETURN_11TICKET_WEBSERVICE_ACTION";

  public static final String ERROR_DESCRIPTION = "ErrorDescription";
  public static final String POS_CONNECT_FAILURE_ERROR_DESC = "POS Communication Error. Please try again or dial Store Support at 866-498-7876 for assistance.";

  // GCSTORE-556 End

  // Defect Fix:: GCSTORE-1639::Begin
  public static final String PATH_FOR_RESERVE_INVENTORY_TMPL_XML =
      "/global/template/api/reserveInventoryATGIntegration.xml";


  // Defect Fix:: GCSTORE-1639::End

  // Borderfree Orders -- Starts
  public static final String GC_BORDERFREE_WEBSERVICE_URL = "GC_BORDERFREE_WEBSERVICE_URL";
  public static final String POST_BF_RESPONSE_MSG_TO_Q_SERVICE = "GCPostBFResponseMsgToQ";
  public static final String GC_BORDERFREE_WEBSERVICE_USERNAME = "GC_BORDERFREE_WEBSERVICE_USERNAME";
  public static final String GC_BORDERFREE_WEBSERVICE_PASSWORD = "GC_BORDERFREE_WEBSERVICE_PASSWORD";
  public static final String UNSUCCESS_BF_WEBSERVICE_CALL = "UNSUCCESS_BF_WEBSERVICE_CALL";
  public static final String GET_ORDER_LIST_TEMP_BORDERFREE =
      "<OrderList><Order OrderNo='' OrderHeaderKey='' BillToID=''><Extn ExtnSourceOrderNo='' /></Order></OrderList>";
  public static final String IS_PACKING_REQUIRED = "IsPackingRequired";
  public static final String CREATE_ASYNC_REQUEST = "CreateAsyncRequest";
  public static final String CREATE_ASYNC_REQUEST_API = "createAsyncRequest";
  public static final String IS_SERVICE = "IsService";
  public static final String SOAP_ENVELOPE = "soap:Envelope";
  public static final String SOAP_HEADER = "soap:Header";
  public static final String WSSE_SECURITY = "wsse:Security";
  public static final String XMLNS_SOAP = "xmlns:soap";
  public static final String BORDERFREE_URL_XMLNS_SOAP = "http://www.w3.org/2003/05/soap-envelope";
  public static final String XMLNS_V1 = "xmlns:v1";
  public static final String BORDERFREE_URL_XMLNS_V1 = "http://services.fiftyone.com/ws/merchantAPI/v1.0";
  public static final String XMLNS_WSSE = "xmlns:wsse";
  public static final String BORDERFREE_URL_XMLNS_WSSE =
      "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
  public static final String ITEMS = "items";
  public static final String QUANTITY_SMALL = "quantity";
  public static final String SKU_SMALL = "sku";
  public static final String ORDER_ID_SMALL = "orderId";
  public static final String PARCEL_ID = "parcelId";
  public static final String PARCEL_REFERENCE = "parcelReference";
  public static final String SHIPPING_DATE_SMALL = "shippingDate";
  public static final String TRACKING_URL = "trackingURL";
  public static final String CARRIER_SERVICE = "carrierService";
  public static final String CARRIER_NAME = "carrierName";
  public static final String REQUEST_PACKING_SLIP = "requestPackingSlip";
  public static final String PARCEL = "parcel";
  public static final String PARCEL_SHIPMENT_NOTIFICATION = "v1:parcelShipmentNotification";
  public static final String USER_NAME_TOKEN = "wsse:UsernameToken";
  public static final String WSSE_USERNAME = "wsse:Username";
  public static final String WSSE_PASSWORD = "wsse:Password";
  public static final String GC_BF_WEB_SERVICE = "GCBFWebServiceCall";
  public static final String INTL_WEB_ORDER_HOLD = "INTL_WEB_ORDER_HOLD";
  public static final String APPROVE = "Approve";
  public static final String ORDER_CONFIRMATION = "v1:orderConfirmation";
  public static final String ORDER_SMALL = "order";
  public static final String MERCHANT_ORDER_ID = "merchantOrderId";
  public static final String REJECT_SMALL = "Reject";
  public static final String THROW_EXCEPTION = "ThrowException";
  public static final String ERROR_DETAIL_DESC = "ErrorDetailDescription";
  public static final String DETAILS = "details";
  public static final String MESSAGE = "message";
  public static final String ERROR_SMALL = "error";
  public static final String MESSAGES = "messages";
  public static final String S_BODY = "S:Body";
  public static final String S_FAULT = "S:Fault";
  public static final String S_DETAIL = "S:Detail";
  public static final String NS2_ERROR = "ns2:Error";
  public static final String ERROR_COUNT = "ErrorCount";
  public static final String MESSAGE_SMALL = "Message";
  public static final String ASYNC_REQ_HAS_EXCP_EVENT_MESSAGE = "Message";
  public static final String BF_NULL_RESPONSE_DESC = "Not able to connect to server or null response from server";
  public static final String MODIFICATION_REASON_CODE = "ModificationReasonCode";
  public static final String MODIFICATION_REASON_TEXT = "ModificationReasonText";
  public static final String CANCEL_CODE_TYPE = "GC_CANCEL_REASON";
  public static final String BORDERFREE_CANCEL_CODE_VALUE = "BORDERFREE";
  public static final String CODE_SMALL = "code";
  public static final String EXTN_INTL_WEB_ORDER_NO ="ExtnIntlWebOrderNo";
  public static final String MODE_OF_DELIVERY = "ModeOfDelivery";
  public static final String TRACKING_URL_BORDER_FREE = "TrackingUrl";
  public static final String SHIPPING_CARRIER_ID = "ShippingCarrierId";
  public static final String MODE_OF_DELIVERY_DESCRIPTION = "ModeDescription";
  // Borderfree Orders -- End

  //GCSTORE-752 : COA processing
  public static final String GET_OMS_DAX_COA_API_TOKEN = "GET_OMS_DAX_COA_API_TOKEN";
  public static final String GET_OMS_DAX_COA_PROCESSING_URL = "GET_OMS_DAX_COA_PROCESSING_URL";
  public static final String GET_OMS_DAX_COA_PROCESSING_ACTION = "GET_OMS_DAX_COA_PROCESSING_ACTION";
  public static final String CC = "CC";
  public static final String AUTHORIZE = "Authorize";
  public static final String REVERSE = "Reverse";
  public static final String SYSTEM_ID_TAG = "systemID";
  public static final String AUTH_EXPIRATION_DATE = "AuthExpirationDate";
  public static final String AUTHORIZED_AMOUNT = "AuthorizedAmount";
  public static final String COA_AUTH_REVERSAL_FAILED_ERROR_MSG = "COA Auth Reversal Declined. Order updated without COA Auth reversal.";
  public static final String COA_AUTH_DECLINED_ERROR_MSG = "CreditOnAccount Authorization Declined: Insufficient Funds";
  public static final String COA_COM_PAYMENT_GATEWAY_ERROR_ORDER_UPDATED_WITHOUT_COA_REVERSAL = "Payment Gateway communication error. Order updated without COA Auth reversal.";
  public static final String COA_COM_PAYMENT_GATEWAY_ERROR_CREATE_ORDER_WITHOUT_AUTH = "Payment Gateway communication error. Click Confirm again to create this order without Auth(s).";
  public static final String COA_AUTH_FAILURE_ALERT_SERVICE = "GCCOAAlertService";
  public static final String ENV_IS_REAUTH_REQUEST = "IsAuthReAuthRequest";
  public static final String COA_AUTH_HD_ALERT_SERVICE = "GCCOAAuthHardDeclineAlertService";
  public static final String REV_FAILURE = "REVERSE_FAIL";
  public static final String COA_REQUEST_XML_HEADER = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:mfw=\"http://MFWEB.musiciansfriend.com/\"><soapenv:Header/>"
      + "<soapenv:Body><mfw:SendXMLToDAX><mfw:_incomingXML /></mfw:SendXMLToDAX></soapenv:Body></soapenv:Envelope>";
  public static final String SEND_XML_TO_DAX_RESULT = "SendXMLToDAXResult";
  public static final String MFW_INCOMING_XML = "mfw:_incomingXML";

  // Standalone Warranty begins
  public static final String GET_SHIPMENT_LIST_STANDALONE_WARRANTY =
      "<Shipments><Shipment ShipmentKey='' Status=''><ShipmentLines> <ShipmentLine OrderNo='' ItemID=''  PrimeLineNo='' ProductClass='' Quantity='' ShipmentLineNo=''  SubLineNo='' UnitOfMeasure='' /></ShipmentLines></Shipment></Shipments>";
  public static final String BACKROOM_PICKED_STATUS = "1100.70.06.30";
  public static final String GET_ORDER_LIST_WARR_TEMP =
      "<OrderList><Order><OrderLines><OrderLine><Extn/><OrderStatuses><OrderStatus ShipNode='' Status=''/></OrderStatuses></OrderLine></OrderLines></Order></OrderList>";
  public static final String MAX_LINE_STATUS = "MaxLineStatus";
  public static final String ZERO_ZERO_ZERO_ONE = "0001";
  // Standalone Warranty ends

  //GCStore-502 starts
  public static final String GC_FAILED_RETURN_SLIP = "GCFailedReturnSlip";
  public static final String GC_CUST_RETURN_RECIEPT="GCCustomerReturnReciept";
  public static final String GC_INV_RETURN_SLIP="GCInventoryReturnSlip";
  public static final String PDF="Pdf";
  public static final String PDF_NAME="PdfName";
  public static final String RECEIPT_LIST="ReceiptList";
  public static final String FILE_NAME= "FileName";
  public static final String SERVER_URL="SERVER_URL";
  public static final String SERVER_URL_ATTR="ServerURL";
  public static final String PDF_DIR="PDF_DIR";
  public static final String MIME_PDF="application/pdf";
  public static final String PDF_EXTENSION=".pdf";
  public static final String SHIP_NODE_PERSON_INFO="ShipNodePersonInfo";
  public static final String COMPANY="Company";
  public static final String IS_REPRINT="IsReprint";
  public static final String GC_USER_ACTIVITY_LIST = "GCUserActivityList";
  public static final String DERIVED_FROM_ORDER = "DerivedFromOrder";
  //ENDS

  // Defect-3609
  public static final String GC_CANCEL_RESERVATION_WRAPPER_SERVICE = "GCCancelReservationWrapperService";
  // Ends 3609

  // GCSTORE-1229::Begin
  public static final String GET_ORDER_LIST_OUTPUT_TMPL_FOR_RSRVATION =
      "<OrderList><Order MaxOrderStatus='' OrderNo='' EnterpriseCode='' DocumentType='' OrderHeaderKey='' SourcingClassification='' OrderDate='' ><Extn/><OrderLines><OrderLine OrderLineKey='' IsBundleParent='' OrderedQty='' IsScheduledOrderLine='' FulfillmentType='' PrimeLineNo='' IsFirmPredefinedNode='' ShipNode='' >"
          + "<Item/><LinePriceInfo/><Extn/><OrderLineReservations><OrderLineReservation/></OrderLineReservations><BundleParentLine/><BundleComponents><BundleComponent OrderLineKey='' /></BundleComponents><OrderLineInvAttRequest/></OrderLine></OrderLines></Order></OrderList>";
  public static final String ORDER_LINE_RESERVATION_KEY = "OrderLineReservationKey";
  public static final String BUNDLE_FULFILLMENT_MODE = "BundleFulfillmentMode";
  public static final String MODIFY_FULFILLMENT_OPTIONS = "modifyFulfillmentOptions";
  public static final String NEXT_ALERT_TS = "NextAlertts";
  // GCSTORE-1229::End

  //GCSTORE-858 : Start

  public static final String FAILED_RETURN_TEMPLATE = "<OrderLineList><OrderLine OrderedQty='' ReceivingNode='' ReturnReason=''><Item ItemDesc=''/><ItemDetails><Extn ExtnPOSItemID=''/></ItemDetails><Order OrderDate='' DraftOrderFlag='' OrderHeaderKey='' OrderNo='' BillToID='' Createuserid=''/><OrderStatuses><OrderStatus Status='' StatusQty='' StatusDate='' StatusDescription=''/></OrderStatuses><Extn ExtnReturnRISNo='' ExtnTrackingNo=''/><Notes><Note NoteText=''/></Notes></OrderLine></OrderLineList>";
  public static final String SHIP_NODE_KEY = "ShipNodeKey";

  //GCSTORE-858 : End


  //Defect-2307 Template
  public static final String GET_ORDER_LINE_LIST_FOR_RETURN="<OrderLineList><OrderLine OrderLineKey=''><Extn ExtnTrackingNo=''/></OrderLine></OrderLineList>";
  public static final String GET_ORDER_LIST_FOR_RETURN="<OrderList><Order OrderNo=''><OrderLines><OrderLine OrderLineKey='' DerivedFromOrderHeaderKey='' DerivedFromOrderLineKey='' /></OrderLines></Order></OrderList>";
  public static final String GET_SHIPMET_LIST_FOR_RETURN="<Shipments><Shipment ShipmentKey='' ShipmentNo=''><ShipmentLines><ShipmentLine OrderNo=''/></ShipmentLines><Containers><Container TrackingNo=''><ContainerDetails><ContainerDetail OrderLineKey=''></ContainerDetail></ContainerDetails></Container></Containers></Shipment></Shipments>";

  //Defect-2307 Ends

  //Defect - 1862
  public static final String IS_SOLD_SEPERATELY = "IsSoldSeparately";
  //Changes - END

  //Defect - 1935
  public static final String GC_PUBLISH_TO_CUST_PICKUP_QUEUE = "GCPublishEmailToCustPickupInboundQueue";
  public static final String EMAIL_TYPE_RDY_FOR_CUST_PICKUP = "READY_FOR_CUSTOMER_PICKUP";
  public static final String EMAIL_TYPE_CUST_PICKEDUP = "CUSTOMER_PICKEDUP";
  public static final String GC_ADDNOTE_FOR_RDY_FOR_CUST_PICKUP = "GCAddNoteForReadyForCustPickupEmail";
  public static final String GC_ADDNOTE_FOR_CUST_PICKUP = "GCAddNoteForCustomerPickupEmail";
  public static final String GC_PUBLISH_TO_RDY_FOR_CUST_PICKUP_QUEUE = "GCPublishToRdyForCustPickupEmailQ";
  //End


  //Defect - 2333
  public static final String ADD_NOTE_ON_SHIP_CONFIRM = "GCShipmentConfirmationAddNoteService";

  //End
  //GCSTORE-917, 2008
  public static final String DOMAIN_LIST = "DOMAIN_LIST";
  public static final String LOGIN_DOMAIN_LIST = "LOGIN_DOMAIN_LIST";
  //END


  // GCSTORE-2330
  public static final String GET_ORDER_LINE_LIST_TEMP_FOR_SCHEDULES =
      "<OrderList><Order><OrderLines><OrderLine OrderLineKey='' MaxLineStatus='' ><Schedules><Schedule ProductAvailabilityDate='' Quantity='' /></Schedules></OrderLine></OrderLines></Order></OrderList>";

  // GCSTORE # 2268
  public static final String STORE_PHY_LOC_SCR_NR = "STORE_PHY_LOC_SCR_NR";

  //Defect - 2355
  public static final String LOCALE_CODE = "LocaleCode";
  public static final String LOCALE = "Locale";
  public static final String LOCALE_CODE_LOCALELIST = "Localecode";
  public static final String GET_LOCALE_LIST = "getLocaleList";
  public static final String TIME_ZONE = "TimeZone";
  public static final String FROM_DATE = "FromDate";
  public static final String TO_DATE = "ToDate";
  public static final String GET_CALENDAR_DAY_DTLS = "getCalendarDayDetails";
  public static final String DATES = "Dates";
  public static final String DATE = "Date";
  public static final String SHIFTS = "Shifts";
  public static final String SHIFT = "Shift";
  public static final String VALID_FOR_DAY = "ValidForDay";
  public static final String SHIFT_END_TIME = "ShiftEndTime";
  public static final String SHIPPING_CALENDAR = "ShippingCalendar";
  public static final String TIME_ZONE_IN_LOCALELIST = "Timezone";
  //Defect - 2355 Ends

  public static final String SERVICE_SCHEDULE_ORDER = "GCInvokeScheduleOrder";

  //Improvement Ticket for Resending OrderCanellation Email.
  public static final String GC_ORDER_CANCELLATION_EMAIL_SERVICE = "GCOrderCancellationEmailService";
  public static final String STAMP_FL_TYPE = "StampFLType";

  // Data Load
  public static final String GET_ORDER_LIST_TEMP_DATA_LOAD =
      "<OrderList TotalOrderList=''><Order OrderHeaderKey='' EnterpriseCode='' OrderComplete=''></Order></OrderList>";
  public static final String TOTAL_ORDER_LIST = "TotalOrderList";
  public static final String GC_DUMMY = "GC_DUMMY";
  public static final String ORDER_COMPLETE = "OrderComplete";
  public static final String IMPORT_ORDER_ELEMENT = "Import Sales&ExchangeOrderInput";
  public static final String IMPORT_RETURN_ORDER_ELEMENT = "ReturnOrderInput";
  public static final String GET_SHIPMENT_LIST_TMP_DATA_LOAD = "<Shipments><Shipment ShipmentKey=''/></Shipments>";
  public static final String GC_IMPORT = "GC_IMPORT";
  public static final String FIFTY_ONE_GROUND = "FIFTY_ONE_GROUND";

  // GCSTORE-2514 : Start
  public static final String GC_RAISE_ALERT_FOR_NEW_AUTH_SERVICE = "GCRaiseAlertForNewAuthService";
  public static final String GC_NEW_AUTH_ALERT_QUEUE = "GC_NEW_AUTH_ALERT_QUEUE";
  // GCSTORE-2514 : End
  public static final String GC_HANDLE_BUNDLE_RESERVATION_CALL_CENTER_SERV = "GCHandleBundleReservationFromCallCenter";

  // Pilot Design
  public static final String JBS = "JBS";
  public static final String EXTN_ORDER_PHASE = "ExtnOrderPhase";
  public static final String ATG = "ATG";
  public static final String SOURCE_SYSTEM = "SourceSystem";
  public static final String HYPHEN = "-";
  public static final String PHASE_TWO = "P2";
  public static final String PHASE_ONE = "P1";
  public static final String BACKORDERED_STATUS = "1300";
  public static final String MIN_LINE_STATUS = "MinLineStatus";
  public static final String GET_ORDER_LIST_SYNC =
      "<OrderList><Order><OrderLines><OrderLine OrderLineKey='' MaxLineStatus=''/></OrderLines></Order></OrderList>";
  public static final String GET_ORDER_LIST_COND = "<OrderList><Order><Extn/></Order></OrderList>";

  public static final String STATUS_BREAKUP_QTY = "StatusBreakupForCanceledQty";
  public static final String CANCELLED_FROM = "CanceledFrom";
  public static final String EXTN_USR_DOMAIN = "ExtnUserDomain";
  public static final String STR_OPERATION_USR = "STR_OPERATION_USR";
  public static final String STR_MANAGEMENT_USR = "STR_MANAGEMENT_USR";
  // GCSTORE-2206::Begin
  public static final String GC_ORDER_RESERVATION_DATE = "GC_ORDER_RESERVATION_DATE";
  public static final String DRAFT_ORDER_FLAG = "DraftOrderFlag";

  // GCSTORE-2206::End
  // GCSTORE-2879::Begin
  public static final String EXTN_SERIAL_COMPONENT = "ExtnSerialComponent";

  // GCSTORE-2384
  public static final String GET_ORDER_LINE_LIST_TEMPLATE_ONE =
      "<OrderLineList><OrderLine><OrderStatuses><OrderStatus /></OrderStatuses><Order><Extn/></Order></OrderLine></OrderLineList>";
  public static final String SHIP_2_CUSTOMER_TRANID = "CHANGE_ON_SHIPPED.0001.ex";


  // GCSTORE-2879::End

  // GCSTORE-2775 Start
  public static final String GET_ITEM_LIST_INPUT_FOR_PLATINUM_BUNDLE =
      "<Item IgnoreIsSoldSeparately='Y'><InventoryParameters TagControlFlag='S'/><ComplexQuery><And><Or></Or></And></ComplexQuery></Item>";
  public static final String GET_ITEM_LIST_TEMPLATE_FOR_PLATINUM_BUNDLE =
      "<ItemList><Item ItemID='' ><Extn ExtnIsUniqueItem='' ExtnIsPlatinumItem=''/></Item></ItemList>";

  // GCSTORE-2275 End

  //Infosys Constants
  public static final String QUANTITY_TO_CANCEL = "QuantityToCancel";
  // OMS-5457:starts
  public static final String ORDER_DTLS_ENV = "ORDER_DTLS_ENV";
  public static final String RMV_XPTH_COUNT = "RMV_XPTH_COUNT";
  public static final String ITM_RMV_XPTH = "ITM_RMV_XPTH_";
  public static final String ITM_RMV_DMD_XPTH = "ITM_RMV_DMD_XPTH_";
  public static final String COLON = ":";
  //OMS-5457:Ends

  //OMS-5588: Starts
  public static final String TO_ORDER_RELEASE_STATUSES = "ToOrderReleaseStatuses";
  public static final String TO_ORDER_RELEASE_STATUS = "ToOrderReleaseStatus";
  public static final String STATUS_INCLUDED_IN_SHIPMENT = "3350";
  public static final String CONSOLIDATE = "Consolidate";
  public static final String GC_KIT_COMP_BACKORDER_NOTIFICATION = "GCKitCompWarehouseBackOrderNotification";
  public static final String CREATE_EXCEPTION_SVC = "GCCreateExceptionSvc";
  public static final String GC_KIT_WAREHOUSE_BACKORDER_QUEUE = "GC_KIT_WAREHOUSE_BACKORDER_QUEUE";
  public static final String SHIP_NODEKEY = "ShipnodeKey";
  public static final String URL = "URL";
  
  public static final String GC_ATG_SEARCHINDEX_URL = "gc.atg.searchindex.url";
  
  
  public static final String EXTN_LINE_UNIT_PRICE = "ExtnLineUnitPrice";
  public static final String BACKORDERED_QTY = "Backordered Qty";
  public static final String TASK_QUEUE = "TaskQueue";
  public static final String CURRENT_TRANSACTION_SHIPMENT_KEY = "CURRENT_TRANSACTION_SHIPMENT_KEY";
  // OMS-5588-end
  // GCSTORE-3677 Start
  public static final String ATGNULLBILLTOIDHOLD = "BILLTOID_NULL_HOLD";
  // GCSTORE-3677 End
  public static final String GET_ORDER_LIST_OUT_TMP_RESRC =
      "<OrderList><Order OrderNo='' EnterpriseCode='' DocumentType='' OrderHeaderKey=''><Extn ExtnPickupStoreID=''/></Order></OrderList>";
  public static final String EXTN_IS_REF_PAYMENT_CHNG = "ExtnIsRefPaymentChng";
  // GCSTORE:3516
  public static final String GET_ORDER_LIST_TMPL_FOR_SCHEDULES =
      "<OrderList><Order OrderHeaderKey='' EnterpriseCode='' ><OrderLines>"
          + "<OrderLine OrderedQty='' OrderLineKey='' MaxLineStatus='' MinLineStatus=''>"
          + "<Schedules><Schedule /></Schedules></OrderLine></OrderLines></Order></OrderList>";
  public static final String WS_END_POINT_URL = "END_POINT_URL";
  public static final String WS_SOAP_ACTION = "SOAP_ACTION";
  public static final String WS_TIME_OUT = "TIME_OUT";
  public static final String WS_CONTENT_TYPE = "CONTENT_TYPE";
  public static final String MERCHANT_NUMBER = "MERCHANT_NUMBER";
  public static final String DECRYPT_KEY = "DECRYPT_KEY";
  public static final String PLCC_PROMOS = "PLCCPromos";
  public static final String PLCC_PROMO_TYPE = "PLCCPromoType";
  public static final String PLAN_CODE = "planCode";
  public static final String PROMO_TITLE = "promoTitle";
  public static final String PROMO_DESCRIPTION = "promoDescription";
  public static final String RANK = "rank";
  public static final String IS_SPECIAL_FINANCING = "isSpecialFinancing";
  public static final String PROMO_CODE_TEXT_VAL = "PromoCodeTextValue";
  public static final String PVT_LBL_CREDIT_CARD = "PVT_LBL_CREDIT_CARD";
  public static final String WS_CONNECTION_DTLS_SVC = "GCGetWSDetailsForInputParams_svc";
  public static final String LONG_DESCRIPTION = "LongDescription";
  public static final String COMMON_CODE_ATTRIBUTE = "CommonCodeAttribute";
  public static final String AJB_NODE_ID = "AJB_NODE_ID";
  public static final String AJB_NODE_IP = "AJB_NODE_IP";
  public static final String AJB_PORT_NO = "AJB_PORT_NO";
  // GCSTORE-3516

  // Defect-4044
  public static final String GC_GET_ORDER_LIST_RESOLVE_HOLD =
      "<OrderList><Order OrderNo='' EnterpriseCode='' DocumentType='' ><OrderLines>"
          + "<OrderLine PrimeLineNo='' SubLineNo=''><OrderHoldTypes>"
          + "<OrderHoldType HoldType='' ReasonText='' ResolverUserId='' Status='' ></OrderHoldType>"
          + "</OrderHoldTypes></OrderLine></OrderLines></Order></OrderList>";
  public static final String GET_EXCEPTION_LIST_TEMP = "<InboxList><Inbox InboxKey=''/></InboxList>";

  // Defect-4044
  public static final String PAYMENT_REFERENCE_6 = "PaymentReference6";
  public static final String SHIP_2_CUSTOMER_KIT_ONHAND = "SHIP_2_CUSTOMER_KIT_ONHAND";
  public static final String PICKUP_IN_STORE_KIT_ONHAND = "PICKUP_IN_STORE_KIT_ONHAND";
  public static final String SHIP_2_CUSTOMER_KIT = "SHIP_2_CUSTOMER_KIT";
  public static final String PICKUP_IN_STORE_KIT = "PICKUP_IN_STORE_KIT";


  public static final String EXTN_OVERRIDE_PRICE_EMP_ID = "ExtnOverridePriceEmpId";
  public static final String EXTN_QUOTE_MIN_QTY = "ExtnQuoteMinQty";
  public static final String EXTN_QUOTE_MAX_QTY = "ExtnQuoteMaxQty";
  public static final String CUSTOMER_NAME = "CustomerName";
  public static final String CUSTOMER_EMAIL = "CustomerEmail";
  public static final String QUOTE_SUBJECT = "QuoteSubject";
  public static final String QUOTE_MESSAGE = "QuoteMessage";
  public static final String QUOTE_EXPIRATION_PERIOD = "QuoteExpirationPeriod";
  public static final String EXTN_QUOTE_START_DATE = "ExtnQuoteStartDate";
  public static final String EXTN_QUOTE_EXPIRATION_DATE = "ExtnQuoteExpirationDate";
  public static final String QUOTE_SENT_SUCCESSFULLY = "QuoteSentSuccessfully";
  public static final String GC_CREATE_QUOTE_REQUEST_XSL_SERVICE = "GCCreateQuoteRequestXslService";
  public static final String ATG_QUOTE_URL = "gc.atg.quote.url";
  public static final String ATG_QUOTE_SOAP_ACTION = "gc.atg.quote.soapaction";
  public static final String EXTN_SUPPORT_TICKET_NO = "ExtnSupportTicketNo";
  public static final String EXTN_QUOTE_SENT = "ExtnQuoteSent";
  public static final String ATG_EXP_QUOTE_URL = "gc.atg.exp.quote.url";
  public static final String ATG_EXP_QUOTE_SOAP_ACTION = "gc.atg.exp.quote.soapaction";
  public static final String GC_EXPIRE_QUOTE_REQUEST_XSL_SERVICE = "GCExpireQuoteRequestXslService";
  public static final String EXTN_IS_CONVERTED_QUOTE = "EXTN_IS_CONVERTED_QUOTE";
  public static final String DELETE_ORDER = "deleteOrder";
  public static final String EXPIRED_QUOTE_RESPONSE = "ExpiredQuoteResponse";
  public static final String YCD_PRICEOVERRIDE = "YCD_PRICEOVERRIDE";
  public static final String QUOTE_EXPIRATION_DATE = "QuoteExpirationDate";
  public static final String YCD_APPEASEMENT_RSN = "YCD_APPEASEMENT_RSN";
  public static final String PRICE_OVERRIDE_REASON_CODE = "PriceOverrideReasonCode";
  public static final String EXTN_QUOTE_LINE_SENT = "ExtnQuoteLineSent";
  public static final String ATG_UPDATE_QUOTE_SOAP_ACTION = "gc.atg.update.quote.soapaction";
  public static final String GC_UPDATE_QUOTE_REQUEST_XSL_SERVICE = "GCUpdateQuoteRequestXslService";

  // GCSTORE-5189
  public static final String PAYPAL_PAYMENT_XPATH = "/InvoiceDetail/InvoiceHeader/CollectionDetails/CollectionDetail/PaymentMethod[@PaymentType ='PAYPAL']";
  public static final String DERIVED_FROM_ORDERHEADERKEY_XPATH = "/InvoiceDetail/InvoiceHeader/@DerivedFromOrderHeaderKey";
  public static final String INVOICE_ORDER_EXTN = "/InvoiceDetail/InvoiceHeader/Order/Extn";
  public static final String GC_PAYPAL_REFUNDDIST_LIST = "GCPaypalRefundDistList";

  public static final String MULTI_API_TAG = "MultiApi";
  public static final String TEMPLATE_TAG = "Template";
  public static final String FLOW_NAME = "FlowName";
  //PO Cancellation -- Starts
  public static final String TOTAL_NO_OF_RECORDS = "TotalNumberOfRecords";
  public static final String CANCEL_REQUESTED_STATUS = "1100.100";
  public static final String CHANGE_ORDER_STATUS_PO = "CHANGE_PO_STATUS.0005.ex";
  public static final String SCHEDULE_HOLD = "SCHEDULE_HOLD.0005";
  //PO Cancellation -- Ends

  // mPOS Create Order Constants
  public static final String MPOS = "mPOS";
  public static final String MPOS_ORDER_CHANNEL = "mPOS";
  public static final String NO_POS_ITEM_WITH_ENTERPRISE_ITEM_ID_EXIST_ERROR =
      "Item not found as entered; please try again using POS Item ID, SKU, Serial, or UPC.";
  public static final String NO_ITEM_WITH_ENTERPRISE_ITEM_ID_EXIST_ERROR = "Item not found";
  public static final String NO_ITEM_WITH_POS_ITEM_ID_EXIST_ERROR = "Error: Please process this item in POS.";
  public static final String POS_CONNECTION_ERROR =
      "Unable to Connect to POS; please try again. If this issue persists, please contact the Help Desk at (818) 735-8800 ext. 2660.";
  public static final int ENTERPRISE_ITEM_ID_LENGTH = 13;

  // Price Feed constants
  public static final String STORE_PRICELIST_NAME = "STORE_PRICES";
  public static final String MANAGE_PRICELIST_ASSIGNMENT = "managePricelistAssignment";
  public static final String MANAGE_PRICELIST_SELLER_ASSIGNMENT = "managePricelistSellerAssignment";
  public static final String GET_ITEM_PRICE_SERVICE = "GCGetItemPriceService";

  // mPOS Add Item To Cart
  public static final String NOT_ENOUGH_QUANTITY_AVAILABLE = "Not enough quantity available";

  /** ATG Catalog Search Constants */
  public static final String TERM = "Term";
  public static final String CONDITION = "Condition";
  public static final String SERVICE_TRANSFORM_ATG_RESPONSE = "GCTransformATGResponse";
  public static final String PAGE_SIZE = "PageSize";
  public static final String PAGE_NO = "PageNumber";
  public static final String TOTAL_HITS = "TotalHits";
  public static final String TOTAL_PAGES = "TotalPages";
  public static final String JSON_ATTRIB_TOTAL_ERECS_NO = "totalERecsNum";
  public static final String PRODUCT_ID = "productId";
  public static final String SKU_ID = "skuId";
  public static final String PRODUCT_NAME = "productName";
  public static final String PRODUCT_IMAGE_URL = "productImageUrl";
  public static final String ATG_CS_NAO = "Nao";
  public static final String ATG_CS_NS = "Ns";
  public static final String ATG_CS_MUST = "MUST";
  public static final String ATG_CS_NTT = "Ntt";
  public static final String ATG_CS_N = "N";
  public static final String CHILD_SKU = "ChildSku";
  public static final String GC_ATG_SEARCHINDEX_TIME_OUT = "gc.atg.searchindex.timeout";
  public static final String SKU_ENT_ID = "skuENTId";
  public static final String LOW_PRICE = "lowPrice";


  //Defect fixes 5732
  public static final String EXTN_IS_STORE_CLEARANCE = "ExtnIsStoreClearance";
  public static final String USE_UNPLANNED_INVENTORY = "UseUnplannedInventory";
  public static final String AVAILABLE_QUANTITY = "AvailableQuantity";
  public static final String SOURCING_CLASSIFICATION = "SourcingClassification";
  public static final String EXCLUDED_SHIP_NODES = "ExcludedShipNodes";
  public static final String EXCLUDED_SHIP_NODE = "ExcludedShipNode";
  public static final String SUPRESS_SOURCING = "SupressSourcing";

  //Defect fixes 5954
  public static final String DAX_FLAG_5954 = "DAX_FLAG_5954";
  public static final String QUANTITY_TO_PICK = "QuantityToPick";

  //Defect fix 5259
  public static final String ENTERED_BY = "EnteredBy";
  public static final String LOGGEDIN_USER_NAME = "loggedInUserName";
  public static final String GET_SHIPMENT_LINE_LIST = "getShipmentLineList";
  public static final String MARK_INVC_GET_SHIPMENT_LINELIST_TMP = "<ShipmentLines><ShipmentLine OrderHeaderKey='' OrderLineKey=''/></ShipmentLines>";
  public static final String MARK_INVC_GET_ORDER_LINE_LIST_TMP = "<OrderList> <Order OrderHeaderKey=''> <OrderLines> <OrderLine DerivedFromOrderHeaderKey='' DerivedFromOrderLineKey='' OrderLineKey=''/> </OrderLines> </Order> </OrderList>";
  public static final String EXTN_INVC_UPDATED_DATE = "ExtnInvcUpdatedDate";

  //Defect fix 6580
  public static final String IGNORE_IS_SOLD_SEPERATELY = "IgnoreIsSoldSeparately";
  public static final String CATEGORY_ITEM = "CategoryItem";

  // GCSTORE - 1236
  public static final String PAYMENT_COMMUNICATION_FAILURE_MSG_CANNOT_PROCEED = "Payment Gateway communication error. Please try other payment options";

  //GCSTORE-4413
  public static final String TRACKING_NUMBER = "TrackingNumber";

  //GCSTORE-3493 Start
  public static final String INVOICE_NO = "InvoiceNo";
  public static final String GC_GET_ORDER_INVOICE_DETAIL_FOR_EMAIL_SERVICE = "GCGetOrderInvoiceDetailForEmail";
  //GCSTORE-3493 End
  //Added for 4480 - Start
  public static final String TRANSACTION_LINE_ID = "TransactionalLineId";
  public static final String EXTN_WARRANTY_PARENT_ITEM_ID = "ExtnWarrantyParentItemId";
  //end
  //GCSTORE 6302 Start
  public static final String STANDARD_INTL = "STANDARD_INTL";
  public static final String STANDARD_INTL_DESC = "Standard International";

  //GCSTORE 6302 End

  // mPOS Return Service constant - Start

  public static final String GET_MPOS_RETURN_11TICKET_WEBSERVICE_URL = "GET_MPOS_RETURN_11TICKET_WEBSERVICE_URL";
  public static final String GET_MPOS_RETURN_11TICKET_WEBSERVICE_ACTION = "GET_MPOS_RETURN_11TICKET_WEBSERVICE_ACTION";

  // mPOS Return Service constant - Start
  // mPOS-2345: manageCustomer input: START.
  public static final String MANAGE_CUSTOMER_INPUT = "<Customer CustomerType='02' OrganizationCode='GC' Status='10'>"
  		+ "<CustomerContactList>"
  		+ "<CustomerContact>"
  		+ "<CustomerAdditionalAddressList>"
  		+ "</CustomerAdditionalAddressList>"
  		+ "</CustomerContact>"
  		+ "</CustomerContactList>"
  		+ "</Customer>";
  //mPOS-1267: manageCustomer input: END.
  
  //mPOS-538: GCFinalizeCoupons failed alert queue for mPOS orders: Start.
  public static final String GC_MPOS_COUPON_FAILED_ALERT_QUEUE = "GC_MPOS_COUPON_FAILED_ALERT_QUEUE";
  //mPOS-538: GCFinalizeCoupons failed alert queue for mPOS orders: End.
  
  //mPOS-1222 ATG url for getting dim ID for the store: Start.
  public static final String GC_ATG_GETSTOREDIMID_URL = "gc.atg.getstoredimid.url";
  //mPOS-1222 ATG url for getiing dim ID for the store: End.
  
  	// VDS-31 Start
	public static final String ITEM_ALIAS_LIST = "ItemAliasList";
	public static final String ITEM_ALIAS = "ItemAlias";
	public static final String ALIAS_VALUE = "AliasValue";
	public static final String UPC_CODE = "UPC";
   // VDS-31 End 
    //Fix for GCSTORE-6762
	public static final String SOFT_MATCH_DEMAND = "SoftMatchedDemands";
	//End
	//Fix For GCSTORE-6916
	public static final String EXTENDED_DAYS_FOR_FUTURE_PO = "ExtendedDaysForInv";
	public static final String SUPPLY_DETAILS = "SupplyDetails";
	public static final String ETA = "ETA";
	//End
   //Added for GCSTORE-7162
	public static final String API_MANAGE_INVENTORY_NODE_CONTROL = "manageInventoryNodeControl";
	public static final String INVENTORY_NODE_CONTROL = "InventoryNodeControl";
	public static final String INV_PICTURE_INCORRECT_TILL_DATE = "InvPictureIncorrectTillDate";
	public static final String INVENTORY_ORG_FOR_GC = "GCI";
	
	public static final String EXTN_RESTRICT_SHIP_FROM_STORE = "ExtnRestrictShipFromStore";
	public static final String GET_ITEM_LIST_API_TMPL_FOR_RESTRICT_SHIP_TO_STORE = "<ItemList><Item>"
      + "<Extn ExtnRestrictShipFromStore=''/>"
      + "</Item></ItemList>";
	//End
	//Added for GCSTORE-7162 :BEGIN
	public static final String BUNDLE_COMPONENT = "BundleComponent";	
	public static final String ADDITIONAL_MINS_FOR_NODE_CTRL = "GCAddMinsForNodeCtrl";
	public static final String REMOVE_INVENTORY_NODE_CONTROL = "RemoveInventoryNodeControl"; 
	public static final String NODE_CONTROL_TYPE = "NodeControlType"; 
	public static final String ON_HOLD = "ON_HOLD";
	public static final String INV_PICTURE_INCORRECT_TILL_DATE_QRY_TYPE = "InvPictureIncorrectTillDateQryType";
	public static final String GREATER_THAN_OR_EQUAL = "GE"; 
	public static final String GET_INV_NODE_CTRL_LIST = "getInventoryNodeControlList"; 
	public static final String GC_POST_INV_NODE_CONTRL_MSG = "GCPostInvNodeCtrlMsg"; 
	public static final String ADDITIONAL_DAYS_FOR_NODE_CTRL = "GCAddDaysForNodeCtrl";
	//END
	
	//Addition for 5130 Starts
	  
	  public static final String INV_ORGANIZATION_CODE = "GCI";
	  
	 public static final String INVENTORY_ITEM = "InventoryItem";
	//might be a change in template
	 public static final String GET_INVENTORY_ITEM_LIST_TEMP ="<InventoryList LastInventoryItemKey='' LastRecordSet='' OrganizationCode='' TotalInventoryItemList='' TotalNumberOfRecords=''>"
+ "<InventoryItem InventoryItemKey='' InventoryOrganizationCode='' ItemID='' ProductClass='' UnitOfMeasure=''>"
+ "<Item ItemID='' ItemKey='' OrganizationCode='' UnitOfMeasure=''>"
+"<PrimaryInformation Description='' ShortDescription=''/>"
+ "</Item></InventoryItem></InventoryList>";
	
	 public static final String GET_INVENTORY_ITEM_LIST = "getInventoryItemList";
	 
	 public static final String INVENTORY_ORG_CODE = "InventoryOrganizationCode";
	 
	 public static final String TAG_CONTROLLED= "TagControlFlag";
	 
	// 5130 Ends
	
	 //GC PRO start
	  public static final String GC_PRO_PENDING = "Pending";
	  public static final String GC_PRO_APPROVED = "Approved";
	  public static final String DENIED = "Denied";
	  public static final String GC_PRO_CC = "GCPROCC";
	  public static final String GC_PRO_SPO = "GCPROSTORE";
	  public static final String EXTN_IS_GCPRO_CUSTOMER="ExtnIsGcProCustomer";
	  public static final String EXTN_GCPRO_STATUS="ExtnGcProStatus";
	  //GC PRO end
      //Added for GCSTORE-3706--Start
		public static final String FIRST_FUTURE_AVAILABLE_DATE = "FirstFutureAvailableDate";
		public static final String DEFALUT_DATE = "2500-01-01";
		public static final String LEAD_TIME = "LeadTime";
		public static final String JUNK_QUANTITY = "2147483647.00";
		public static final String FUTURE_AVAILABLE_QUANTITY = "FutureAvailableQuantity";
		public static final String CODE_VAULE_FOR_INVALID_DATE = "displayForInvalidDate";
	    public static final String CODE_VALUE_FOR_VALID_DATE = "displayForValidDate";
	//End
	//Added for GCSTORE - 6726--Start
	public static final String GET_ITEM_LIST_TEMPLATE_RELEASE_RESCH =
		      "<ItemList><Item><PrimaryInformation UnitCost='' ItemType='' DefaultProductClass='' ProductLine='' "
		          + "PrimarySupplier='' KitCode='' /> "
		    	  + "<InventoryParameters UseUnplannedInventory='' /> "
		          + " <Components> <Component/> </Components> </Item></ItemList>";
	public static final String CODE_VAULE_FOR_EXTENDED_DAYS = "CodeValueForExtendedDays";
	public static final String AVAILABLE_QTY = "AvailableQty";
	//Added for GCSTORE - 6726--End	
    //Added for GCSTORE-6934
	public static final String GET_POS_ITEM_ID_LIMIT_VALUE = "getPOSItemIDLimitValue";
	public static final String GET_POS_ITEM_ID_LIMIT = "getPOSItemIDLimit";
	public static final String ITEM_KEY = "ItemKey";  
    //GCSTORE-6934 -- End
	
	//Added for GCSTORE_6877 :START
	public static final String IS_COMPLETE_LINE_CANCEL = "IsCompleteLineCancel"; 
	public static final String EXTN_PROG_ID = "ExtnProgId";
	public static final String SYSTEM_CANCELLATION = "SystemCancellation";
	public static final String BACKORDER_CANCEL_MSG = "BackOrderCancelMsg";
	public static final String GC_CANCEL_GIFT_EXCEPTION = "GCCancelGiftException"; 
	public static final String PROG_ID_EXSTORE = "ExStore";
	public static final String DETAIL_DESCRIPTION = "DetailDescription"; 
	//GCSTORE-6877 :END
	
     //Added for GCSTORE-7405 : START
	 public static final String ORDER_LINE_LIST = "OrderLineList";
	 //GCSTORE-7405 : END
	 
	 //Added for GCSTORE-7083
	 public static final String PRODUCT_LINE = "ProductLine";

	//Added for 4380 : Begin
	public static final String ENV_CONFIRM_DRAFT_ORDER_INVOKED = "ConfirmDraftOrderInvoked";
	//End
	 
  private GCConstants() {
  }
}
