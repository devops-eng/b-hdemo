/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class defines the Error constant used to invoke exception
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               14/02/2014        Soni, Karan		       Initial Version
			 1.1			   01/06/2015	     Kumar, Gunjan             Added Error Constants Used for POS Inventory Sync
			 1.2               03/02/2015        Taj, Rashma               Added Error Constants used for Customer Sync Development
 *#################################################################################################################################################################
 */
package com.gc.common.constants;

/**
 * @author Soni, Karan
 */
public class GCErrorConstants {

  // GC Error Constants
  public static final String INVALID_PAYMENT_GROUP_DESCRIPTION = "Invalid Payment Group for Payment Type";
  public static final String INVALID_PAYMENT_GROUP_CODE = "GCINV0000PYT";

  public static final String INVALID_USER_ERROR_CODE = "GCWCC0000IU";
  public static final String INVALID_USER_ERROR_DESCRIPTION = "User entered does not exist";

  public static final String INVALID_ITEM_ERROR_CODE = "GCWCC0000II";
  public static final String INVALID_ITEM_ERROR_DESCRIPTION = "Invalid Item";

  public static final String ACTION_NULL_ERROR_CODE = "GCINV0000AN";
  public static final String ACTION_NULL_ERROR_DESCRIPTION = "Action attribute is null";

  public static final String INVALID_ACTION_ERROR_CODE = "GCINV0000AN";
  public static final String INVALID_ACTION_ERROR_DESCRIPTION = "Invalid Action. Valid values are MODIFY/CANCEL/CREATE.";

  public static final String PAYMENT_SERVER_DOWN_DESCRIPTION = "Payment Server is down";
  public static final String PAYMENT_SERVER_DOWN_CODE = "GCPSD0000AJB";

  public static final String INVALID_SC_ERROR_CODE = "SCV0001";
  public static final String INVALID_SOURCE_CODE_ERROR_DESC = "Invalid Source Code";

  public static final String INVALID_SC_ERROR_CODE_FOR_ENTERPRISE = "SCV0002";
  public static final String INVALID_SOURCE_CODE_FOR_ENTERPRISE_DESC = "Source Code Not Valid For Enterprise";

  public static final String INACTIVE_SC_ERROR_CODE_FOR_ENTERPRISE = "SCV0003";
  public static final String INACTIVE_SOURCE_CODE_FOR_ENTERPRISE_DESC = "Source Code Is Not Active";

  public static final String ITEM_CANNOT_BE_ADDED_ERROR_CODE = "GCINV0000CI";
  public static final String ITEM_CANNOT_BE_ADDED_ERROR_DESC = "Order cannot be modified at the current status. Only Coverage Item can be added.";

  public static final String GIFT_ITEM_ERROR_CODE = "GCINV0000GI";
  public static final String GIFT_ITEM_ERROR_DESC = "Item is not eligible for orders with expedited shipping methods.";


  // ExStore Error Constants-----------Starts
  public static final String INVALID_STORE = "EXTN_INVAL_STR";
  public static final String INVALID_USER_GROUP_ID = "EXTN_INVAL_GRP";
  public static final String POS_CONNECTION_FAILURE = "EXTN_POS_FAIL";
  public static final String EXTN_POS_SCAN_FAILED = "EXTN_POS_ERR";
  public static final String EXTN_POS_CONNECT_TIMEOUT = "EXTN_POS_CNCT";
  public static final String EXTN_POS_CANCEL_RSRVN_FAIL = "EXTN_POS_CNCL1";
  public static final String EXTN_SERIAL_NUM_NOT_RCVD = "EXTN_SRL_ERR";
  // ExStore Error Constants-----------Ends


  //LDAP Connection
  public static final String LDAP_IS_NOT_CONNECTED="LDAP is not Connected.";


  // Order Capture Error Constants : Start
  public static final String SOURCE_CODE_MANDATORY_ERROR_CODE = "EXTN_GCE00401";

  public static final String ORDER_ALREADY_CAPTURED_ERROR_CODE = "EXTN_GCE00404";

  public static final String OFFLINE_AUTH_NOT_SUPPORTED_ERROR_CODE = "EXTN_GCE00402";

  public static final String SOURCE_SYSTEM_NOT_SUPPORTED_ERROR_CODE = "EXTN_GCE00403";

  // Order Capture Error Constants : End

  // GC Email start

  public static final String EXTN_GCE00701 = "EXTN_GCE00701";
  public static final String RESEND_NOT_ALLOWED = "EXTN_GCE0010";
  public static final String TRIGGER_NOT_ALLOWED = "EXTN_GCE0011";

  // GC Email end


  // GC POS InventorySync Constant : Start
  public static final String POS_INVALID_ITEMS_IN_INV_SYNC_MSG_DESC =
      "Invalid Item(s) found in POS inventory sync message";
  public static final String POS_INVALID_ITEMS_IN_INV_SYNC_MSG_DETL_DESC =
      "Invalid Item(s) found in POS inventory sync message with following ItemID(s): ";

  public static final String GC_POS_SYNC_ERROR_REASON = "Either Item does not exist or not Active in OMS";
  // GC POS InventorySync Constant : End

  // GCSTORE 131 & 132 Constants- Begin
  public static final String INSUFFICIENT_CREDENTIAL_ERROR_CODE = "GCMA00001";
  public static final String INSUFFICIENT_CREDENTIAL_ERROR_DESC = "Insufficient Credentials";
  public static final String INVALID_CREDENTIAL_ERROR_CODE = "GCMA00002";
  public static final String INVALID_CREDENTIAL_ERROR_DESC = "Invalid Credentials";

  // GCSTORE 131 & 132 Constants- End
  //Order Promising::Begin
  public static final String GCE00001 = "EXTN_GCE00001";
  public static final String GCE00002 = "EXTN_GCE00002";
  public static final String GCE00003 = "EXTN_GCE00003";
  public static final String GCE00004 = "EXTN_GCE00004";
  public static final String GCE00005 = "EXTN_GCE00005";
  //Order Promising::End

  // PayPal error codes Start
  public static final String GCE00006 = "EXTN_GCE00006";
  public static final String PAYPAL_COMMUNICATION_FAILURE = "Communication with PayPal is failed";

  // Paypal error codes End

  //Manager Apporval GCStore-790
  public static final String INSUFFICIENT_CREDENTIAL_ERRORCODE = "EXTN_GCMA00001";
  public static final String INSUFFICIENT_CREDENTIAL_ERRORDESC = "Insufficient Credentials";
  public static final String INVALID_CREDENTIAL_ERRORCODE = "EXTN_GCMA00002";
  public static final String INVALID_CREDENTIAL_ERRORDESC = "Invalid Credentials";





  //GCSTORE 166,168,170,396
  public static final String GCE00503 = "EXTN_GCE00503";
  public static final String GCE00504 = "EXTN_GCE00504";
  //GCSTORE 166,168,170,396 End
  // Customer Sync::Begin
  public static final String EXTN_GCE0012 = "EXTN_GCE0012 ";
  public static final String EXTN_GCE0013 = "EXTN_GCE0013";
  public static final String EXTN_GCE0014 = "EXTN_GCE0014";
  public static final String EXTN_GCE0015 = "EXTN_GCE0015";
  public static final String EXTN_GCE0016 = "EXTN_GCE0016";
  public static final String EXTN_GCE0017 = "EXTN_GCE0017";

  // Customer Sync::End

  // GCSTORE-348,235,760 CustomerOrderSync: Begin
  public static final String EMAIL_ID_MISSING = "EXTN_GCE00507";
  public static final String INVALID_CUSTOMER_SYNC_ROOT_ELE = "EXTN_GCE00508";

  // GCSTORE-348,235,760 CustomerOrderSync::End

  /* GCSTORE - 557 - Begin */
  public static final String SINGLE_SOURCE_FUTURE_DATE = "GCINV0000SS";
  public static final String SINGLE_SOURCE_FUTURE_DATE_DESC = "Future ship dates cannot be added to orders for retail " +
      "used, vintage or platinum items with a specified serial number. Please remove these items to add a " +
      "future ship date.";

  public static final String SINGLE_SOURCE_FUTURE_DATE_ADD_PROD = "GCINV0000AP";
  public static final String SINGLE_SOURCE_FUTURE_DATE_ADD_PROD_DESC = "Retail Used, Vintage and Platinum items with "
      + "specified serial numbers cannot be added to orders with future ship dates."
      + " Please remove the future ship date to add this item.";
  /* GCSTORE - 557 - End */
  // --GCSTORE-195:Start
  public static final String EXTN_GCE00101 = "EXTN_GCE00101";

  // --GCSTORE-195:End


  //BML error codes Start
  public static final String GCE00088 = "EXTN_GCE00088";
  public static final String BML_COMMUNICATION_FAILURE = "Communication with BML is failed";


  // BML error codes End

  //POS Invoicing Start
  public static final String WEB_SERVICE_RESERVATION_CALL_TIMEOUT_CODE= "EXTN_RSRVFAIL";
  public static final String WEB_SERVICE_POS_INVOICE_CALL_TIMEOUT_CODE= "EXTN_INVC_FAIL";
  public static final String WEB_SERVICE_RESERVATION_CALL_ERROR_FROM_POS = "EXTN_POS_FAIL";
  public static final String INVALID_RESERVATION_ID_FROM_POS = "EXTN_BLANK_ID";
  //POS Invoicing End

  //Store Returns error codes Start
  public static final String GCE00511 = "EXTN_GCE00511";
  public static final String POS_CUSTOMER_CREATION_FAILURE = "Unable to create POS Customer. Please try again, or choose a different refund type";
  //Store Returns error codes End


  // Pick request COM Start

  public static final String SHIPNODE_BLANK = "Shipnode is blank";

  // Pick request COM End

  //Border free error codes
  public static final String ORDER_DOESNOT_EXISTS = "EXTN_ORD_NE";
  public static final String WEB_SERVICE_BORDERFREE_TIMEOUT_CODE = "EXTN_BF_FAIL";

  // Manage Commission Error Code
  public static final String MANAGE_COMMISSION_FAILURE = "EXTN_COMM_FAIL";
  public static final String CONFIRM_SHIPMENT_FAILURE = "EXTN_SHIP_FAIL";
  
  public static final String ERROR_ON_SCHEDULING = "EXTN_SCH_01";
  
  //GCSTORE-2314 Start
  public static final String ITEM_LIST_PRICE_NOT_DEFINED_CODE = "GCWCC0000I1";	
  public static final String ITEM_LIST_PRICE_NOT_DEFINED_DESCRIPTION = "Item is missing pricing information and can not be added to the order.";
  
  public static final String SERIALIZEDITEM_APO_PO_ADDRESS_CODE = "GCINV000AP";
  public static final String SERIALIZEDITEM_APO_PO_ADDRESS_DESCRIPTION = "Order with Serialized Platinum item request can not be shipped to APO/ FPO/ PO Addresses";
  //GCSTORE-2314 End
  //GCATG
  public static final String GC_SEARCH_CATALOG_INDEX_DOWN_CODE ="EXTN_GCEATG_01";
  public static final String GC_SEARCH_CATALOG_INDEX_DOWN_DESCRIPTION = "Unable to connect to the catalog search engine, please try again. If this issue persists, Please contact the Help Desk at (818) 735-8800 ext. 2660.";
  //GCATG

  //GCSTORE-5732
  public static final String INSUFFICIENT_INVENTORY= "EXTN_INSUFINV";
  
  //GCSTORE-6301 - Start
 
  public static final String MANDATORY_BILLTO_ATTRIBUTES_MISSING = "EXTN_BILLTO_MIS";
  
  //GCSTORE-6301 - End

  private GCErrorConstants() {

  }
}
