/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               06/02/2014        Last Name, First Name		JIRA No: Description of issue.
             1.1               12/19/2014         Jain,Shubham				Adding XML Literals Used for RTAM Published-GC Store 52
             1.2               12/26/2014        Rangarajan,Shwetha			Adding XML Literals Used for Demand Sync to Dax-GC Store 53
			 1.3               01/06/2015        Kumar, Gunjan              Added XML Literals Used for POS Inventory Sync
			 1.4               01/20/2015		 Jain,Shubham				Added XML Literals Used for RTAM Published-GC Store 52
			 1.5               02/26/2015        Singh,Gurpreet             Added XMLLiterals used for Paypal Refund CR-16
 *#################################################################################################################################################################
 */
package com.gc.common.constants;

/**
 * @author <a href="mailto:email Address">Name</a>
 */
public class GCXmlLiterals {

  // CatalogManagement Start
  public static final String ONHAND_SAFETY_FACTOR_QUANTITY = "OnhandSafetyFactorQuantity";
  public static final String MULTI_API = "MultiApi";
  public static final String INVENTORY_PARAMETERS = "InventoryParameters";
  public static final String ASSOCIATION_LIST = "AssociationList";
  public static final String API = "API";
  public static final String NAME = "Name";
  public static final String INPUT = "Input";

  // CatalogManagement End



  //Used for RTAM Published-GC Store 52
  public static final String EXTN_SERIAL_COMPONENT="ExtnSerialComponent";
  public static final String SHIP_NODES="ShipNodes";
  public static final String NON_PICKABLE = "NON_PICKABLE.ex";
  public static final String SUPPLY_DETAILS="SupplyDetails";
  public static final String TAGS="Tags";
  public static final String TAG="Tag";
  public static final String TOTAL_ONHAND_SUPPLY="TotalOnhandSupply";
  public static final String TOTAL_DEMAND="TotalDemand";
  public static final String BATCH_NO="BatchNo";
  public static final String LOT_NUMBER="LotNumber";
  public static final String TAG_CONTROL_FLAG="TagControlFlag";
  public static final String ALERT_TYPE="AlertType";


  //Used for Sourcing GCStoreLoadAPI
  public static final String SOURCE_FROM_STORE_IN_FEED="ExtnIsSourceFromStoreAllowed";
  public static final String SOURCE_FROM_STORE_IN_OMS="ExtnIsSourceFromStoreAllowed";

  public static final String DISTRIBUTION_RULE_ID="DistributionRuleId";
  public static final String OWNER_KEY="OwnerKey";
  public static final String PURPOSE="Purpose";
  public static final String ITEM_GROUP_CODE="ItemGroupCode";
  public static final String ITEM_SHIP_NODES="ItemShipNodes";
  public static final String ITEM_SHIP_NODE="ItemShipNode";
  public static final String ACTIVE_FLAG="ActiveFlag";
  public static final String SHIP_NODE_KEY="ShipnodeKey";
  public static final String CREATE_FOR_INV_ITEMS_AT_NODE="CreateForInvItemsAtNode";
  public static final String INVENTORY_ITEM_KEY="InventoryItemKey";

  public static final String ALERT_LEVEL="AlertLevel";
  public static final String ALERT_QUANTITY="AlertQuantity";
  public static final String FIRST_FUTURE_AVAILABLE_DATE="FirstFutureAvailableDate";
  public static final String FUTURE_AVAILABLE_DATE="FutureAvailableDate";
  public static final String FUTURE_AVAILABLE_QUANTITY="FutureAvailableQuantity";
  public static final String ONHAND_AVAILABLE_DATE="OnhandAvailableDate";
  public static final String ONHAND_AVAILABLE_QUANTITY="OnhandAvailableQuantity";

  //End for GCStoreLoadAPI


  //Demand Sync to DAX - GCStore 53-----------------------START
  public static final String DEMAND_SUMMARY = "DemandSummary";
  public static final String STATUS_BRKUP_ELE = "StatusBreakupForCanceledQty";
  public static final String CANCELED_FROM = "CanceledFrom";
  public static final String DETAILS = "Details";
  public static final String CHANGE_IN_ORDER_QUANTITY = "ChangeInOrderedQty";
  public static final String COMPONENTS = "Components";
  public static final String COMPONENT = "Component";
  public static final String ITEMS = "Items";
  public static final String RESERVATION_CHANGES = "ReservationChanges";
  public static final String RESERVATION = "Reservation";
  public static final String RESERVATIONS = "Reservations";
  public static final String ASSUME_INFINITE_INVNTRY = "AssumeInfiniteInventory";
  public static final String GC_BUNDLE_RSRVN = "GCBundleReservation";

  // Demand Sync to DAX - End

  // ExStore------------------------------------------------------Starts

  // Login------------------Starts
  public static final String TO_ADDRESS = "ToAddress";
  public static final String RQSTD_SHIP_DATE = "RequestedShipmentDate";
  public static final String EXPCTD_DELIVERY_DATE = "ExpectedDeliveryDate";
  public static final String INSTRUCTIONS = "Instructions";
  public static final String SHIPMENTS = "Shipments";
  public static final String ELE_SHIPMENT = "Shipment";
  public static final String STATUS_DATE = "StatusDate";
  public static final String DELIVERY_METHOD = "DeliveryMethod";
  public static final String BILL_TO_ADDRESS = "BillToAddress";
  public static final String FROM_ADDRESS = "FromAddress";
  public static final String CUSTOM_COMPONENTS = "CustomComponents";
  public static final String PARENT_SHP_LINE_KEY = "ParentShipmentLineKey";
  public static final String SHP_LINE_KEY = "ShipmentLineKey";
  public static final String ORGANIZATION_KEY = "OrganizationKey";
  public static final String LOGIN_ID = "LoginID";
  public static final String PWD = "Password";
  public static final String LOGIN = "Login";
  public static final String NODE_TYPE = "NodeType";
  // Login------------------Ends
  // PickList-------------Starts
  public static final String ORDER_BY = "OrderBy";
  public static final String ATTRIBUTE = "Attribute";
  public static final String EXTN = "Extn";
  public static final String ORDER_COMMISSION = "GCOrderCommission";
  public static final String USER_NAME = "UserName";
  public static final String LINE_OVERALL_TOTALS = "LineOverallTotals";

  // PickList-------------Ends
  // PickDetail - GCSTORE-192 GCSTORE-190---Starts
  public static final String SHIPMENT_LINE_KEY = "ShipmentLineKey";
  public static final String ORDERLINE_SRCNG_CTRLS = "OrderLineSourcingControls";
  public static final String ORDERLINE_SRCNG_CTRL = "OrderLineSourcingCntrl";
  public static final String SUPPRESS_SRCNG = "SuppressSourcing";
  public static final String USER_GROUP_KEY = "UsergroupKey";
  public static final String GET_USER_LIST = "getUserList";
  public static final String SHIP_INV_RQST_ATT = "ShipmentInvAttrequest";
  public static final String ODR_LINE_COMM_LIST = "GCOrderLineCommissionList";
  public static final String GC_ODR_LINE_COMM = "GCOrderLineCommission";
  public static final String WARRANTY_COMPONENTS = "WarrantyComponents";
  public static final String ODR_LINE_COMM = "GCOrderLineCommission";

  // PickDetail - GCSTORE-192 GCSTORE-190---Ends

  // ExStore------------------------------------------------------Ends
  // Order Capture --- Start
  public static final String PROCESSED_AMOUNT = "ProcessedAmount";
  public static final String REQUESTED_AMONT = "RequestedAmount";
  public static final String PAYMENT_DETAILS = "PaymentDetails";
  public static final String ORDERLINES = "OrderLines";
  public static final String SOURCING_CLASSIFICATION = "SoucingClassification";


  // Order Capture --- End

  // Order Promising::Begin
  public static final String FULFILLMENT_TYPE = "FulfillmentType";
  public static final String FIND_INVENTORY_API = "findInventory";
  public static final String ALLOCATION_RULE_ID = "AllocationRuleID";

  public static final String EXTN_IS_ON_HAND_ONLY = "ExtnIsOnHandOnly";
  public static final String EXTN_IS_STORE_CLEARANCE = "ExtnIsStoreClearance";

  public static final String SHIP_TO_ADDRESS = "ShipToAddress";

  public static final String EXTN_SET_CODE = "ExtnSetCode";
  public static final String EXTN_BATCH_NO = "ExtnBatchNo";
  public static final String INVENTORY_TAG_KEY = "InventoryTagKey";
  public static final String GET_RESERVATION = "getReservation";
  public static final String CANCEL_RESERVATION = "CancelReservation";
  public static final String CANCEL_RESERVATION_LINE_LIST = "CancelReservationLineList";
  public static final String CANCELLED_QTY = "CancelledQty";
  public static final String EXTN_PICK_UP_STORE_ID = "ExtnPickupStoreID";
  public static final String ITEM_RESERVATION = "ItemReservation";
  public static final String TAG_NUMBER = "TagNumber";
  public static final String ITEM = "Item";
  public static final String PRODUCT_CLASS = "ProductClass";
  public static final String PRIMARY_INFORMATION = "PrimaryInformation";
  public static final String KIT_CODE = "KitCode";
  public static final String DEFAULT_PRODUCT_CLASS = "DefaultProductClass";
  public static final String TAG_KEY = "TagKey";
  // Order Promising::End

  // POS Inventory Sync -- Start
  public static final String EXTN_POS_ITEM_ID = "ExtnPOSItemID";
  public static final String DETAIL_DESCRIPTION = "DetailDescription";
  public static final String ERROR_REASON = "ErrorReason";
  public static final String FLOW_NAME = "FlowName";
  public static final String INVENTORY_SUPPLY = "InventorySupply";
  public static final String INV_ORG_CODE = "InventoryOrganizationCode";
  public static final String API_GET_INV_SUPPLY = "getInventorySupply";
  public static final String INV_MESSAGE = "InventoryMessage";
  // POS Inventory Sync -- End

  // Payment Processing Phase2 -- Start
  public static final String CHARGE_TRANSACTION_DETAIL = "ChargeTransactionDetail";
  public static final String REMAINING_TOTALS = "RemainingTotals";
  public static final String GRAND_TOTAL = "GrandTotal";

  // Order Capture -- Start
  public static final String EXTN_SOURCE_SYSTEM = "ExtnSourceSystem";
  public static final String EXTN_SOURCE_ORD_NO = "ExtnSourceOrderNo";
  public static final String DEPENDENT_ON_PRIME_LINE_NO = "DependentOnPrimeLineNo";
  public static final String TRANSACTIONAL_LINE_ID = "TransactionalLineId";
  public static final String GET_UNPUBLISH_ITEM = "GetUnpublishedItem";
  public static final String EXTN_IS_UNIQUE_ITEM = "ExtnIsUniqueItem";
  public static final String TOTAL_NO_OF_RECORDS = "TotalNumberOfRecords";
  public static final String REQUEST_AMOUNT = "RequestAmount";

  // Payment Processing Phase2 -- End


  //Low Value::Begin
  public static final String ITEM_ID = "ItemID";
  public static final String ITEM_LIST = "ItemList";
  public static final String EXTN_IS_USED_OR_VINTAGE_ITEM = "ExtnIsUsedOrVintageItem";
  public static final String SHIP_NODE = "ShipNode";
  public static final String ITEM_TYPE = "ItemType";
  public static final String EXTN_UNIT_PRICE = "ExtnUnitPrice";
  public static final String PRODUCT_LINE = "ProductLine";
  public static final String PRIMARY_SUPPLIER = "PrimarySupplier";
  public static final String NODE = "Node";
  public static final String ORGANIZATION_CODE = "OrganizationCode";
  public static final String RESERVATION_PARAMETERS = "ReservationParameters";
  //Low Value::End

  // GCSTORE-396, GCSTORE-166,168,170 Start
  public static final String PRICELIST_LINE = "PricelistLine";
  public static final String EXTN_POS_PRICE_FEED = "ExtnPOSPriceFeed";
  public static final String EXTN_IS_CLEARANCE_ITEM = "ExtnIsClearanceItem";
  public static final String API_MANAGE_PRICELIST_LINE = "managePricelistLine";
  public static final String EXTN_CLEARANCE_PRICE = "ExtnClearancePrice";
  public static final String API_CREATE_INVENTORY_ACTIVITY = "createInventoryActivity";
  //GCSTORE-396, GCSTORE-166,168,170 End

  //GCSTORE-187 & 193 -Begin
  public static final String SUGGESTED_OPTION = "SuggestedOption";
  public static final String OPTION = "Option";
  public static final String ASSIGNMENTS = "Assignments";
  public static final String ASSIGNMENT = "Assignment";
  public static final String ALTERNATE_STORES = "AlternateStores";
  public static final String OPTIONS = "Options";
  public static final String AVAILABLE_QTY = "AvailableQty";
  public static final String FIRST_AVAILABLE_DATE = "FirstAvailableDate";
  public static final String DISTANCE_TO_CONSIDER = "DistanceToConsider";
  public static final String IS_AVAILABLE = "IsAvailable";
  public static final String MULTIAPI = "multiApi";
  public static final String OUTPUT = "Output";
  public static final String TEMPLATE = "Template";
  public static final String SHIP_NODE_PERSON_INFO = "ShipNodePersonInfo";
  public static final String NODE_ORG_CODE = "NodeOrgCode";
  public static final String INV_QTY = "InvQty";
  //GCStore-187 & 193 -End
  // GCSTORE-348,235,760 CustomerOrderSync:Start
  public static final String EXTN_SRC_CSTOMER_ID = "ExtnSourceCustomerID";
  public static final String CUSTOMER = "Customer";
  public static final String CUSTOMER_CONTACT = "CustomerContact";
  public static final String CUSTOMER_CONTACT_EMAIL_ID = "EmailID";
  public static final String PERSON_INFO_BILL_TO = "PersonInfoBillTo";
  public static final String PERSON_INFO_BILL_TO_EMAIL_ID = "EMailID";
  public static final String DESC = "Desc";
  public static final String MODIFYTS = "Modifyts";
  public static final String CUSTOMER_CONTACT_LIST = "CustomerContactList";
  public static final String CUSTOMER_CONTACT_ID = "CustomerContactID";
  public static final String CUSTOMER_ADDITIONAL_ADD_LIST = "CustomerAdditionalAddressList";
  public static final String EXTN_SHIP_TO_STORE_ALLOWED = "ExtnShiptoStoreAllowed";

  // GCSTORE-348,235,760 CustomerOrderSync:End
  // GCStore-328-Start.
  public static final String EXTN_TAX_EXEMPT_REQUEST_STATUS = "ExtnTaxExemptReqStatus";
  public static final String EXTN_TAX_EXEMPT_REQ_VALID_UNTIL = "ExtnTaxExemptReqValidUntil";
  public static final String EXTN_TAX_EXEMPT_STATE = "ExtnTaxExemptState";
  public static final String EXTN_IS_SHIP_TO_STORE_ALLOWED = "ExtnIsShiptoStoreAllowed";
  public static final String IS_STORE_USER = "IsStoreUser";
  public static final String BILL_TO_ID = "BillToID";
  public static final String ENTERPRISE_CODE = "EnterpriseCode";
  public static final String PERSON_INFO_SHIP_TO = "PersonInfoShipTo";
  public static final String STATE = "State";
  public static final String EXTN_IS_TAX_EXEMPT_EDITABLE = "ExtnIsTaxExemptEditable";
  public static final String TAX_EXEMPT_FLAG = "TaxExemptFlag";
  public static final String STAMP_TAX_EXEMPT_FLAG = "StampTaxExemptFlag";

  // GCStore-328 End
  // GCStore -443 Begin
  public static final String SHIP_NODE_LIST = "ShipNodeList";
  public static final String EXTN_SHIP_NODE = "ExtnShipNode";
  public static final String ORDERLINE_INVATT_REQUEST = "OrderLineInvAttRequest";
  public static final String EXTN_PROPOGATE_SERIAL_TO_COMP = "ExtnPropogateSerialToComp";
  //GCStore -443 End

  // CustomerOrderHistory : Start
  public static final String REASON_TEXT = "ReasonText";
  public static final String REASON_CODE = "ReasonCode";
  // CustomerOrderHistory : END
  // CR-16 Start
  public static final String REFUNDED_AMT = "RefundedAmount";
  public static final String ORIGINAL_SETTLED_AMT = "OriginalSettledAmount";
  public static final String GC_PP_REFUND_DIST_KEY = "GCPaypalRefundDistributionKey";
  public static final String CHARGE_TRANSACTION_ID = "ChargeTransactionID";
  public static final String RETRY = "retry";
  public static final String DECLINE_ERROR_CODE = "DeclineErrorCode";
  // CR-16 End

  // GCSTORE-776 - Begin
  public static final String LOGINID = "Loginid";
  public static final String RESOURCE_ID = "ResourceId";
  // GCSTORE-776 - End

  // GCSTORE-718: Call Center Order Processing:Start
  public static final String ALLOCATION_DATE = "AllocationDate";
  public static final String REMAINING_QTY = "RemainingQty";
  public static final String MAX_LINE_STATUS = "MaxLineStatus";
  public static final String MAX_LINE_STATUS_DESC = "MaxLineStatusDesc";
  public static final String MIN_LINE_STATUS = "MinLineStatus";
  public static final String MIN_LINE_STATUS_DESC = "MinLineStatusDesc";
  public static final String OPEN_QTY = "OpenQty";
  public static final String OTHER_CHARGES = "OtherCharges";
  public static final String ORG_ORD_QTY = "OriginalOrderedQty";
  public static final String INVOICE_QTY = "InvoicedQty";
  public static final String LINE_SEQ_NO = "LineSeqNo";
  public static final String BUNDLE_FULLFILLMENT_MODE = "BundleFulfillmentMode";
  public static final String TAX_SUMMARY = "TaxSummary";
  public static final String INVOICE_TAX = "InvoicedTax";
  public static final String REF_1 = "Reference_1";
  public static final String REF_2 = "Reference_2";
  public static final String REF_3 = "Reference_3";
  public static final String REMAINING_TAX = "RemainingTax";
  public static final String TAX_PERCENTAGE = "TaxPercentage";
  public static final String INVOICE_CHRGE_AMOUNT = "InvoicedChargeAmount";
  public static final String INVOICE_CHRGE_PER_LINE = "InvoicedChargePerLine";
  public static final String INVOICE_CHRGE_PER_UNIT = "InvoicedChargePerUnit";
  public static final String REMAINING_CHARGE_AMOUNT = "RemainingChargeAmount";
  public static final String REMAINING_CHARGE_PER_LINE = "RemainingChargePerLine";
  public static final String REMAINING_CHARGE_PER_UNIT = "RemainingChargePerUnit";
  public static final String PARENT_ORD_LINE_RELATIONSHIP = "ParentOrderLineRelationships";
  public static final String LINE_REMAINING_TOTALS = "LineRemainingTotals";
  public static final String LINE_INVOICE_TOTALS = "LineInvoicedTotals";
  public static final String ADDITIONAL_ADDRESSES = "AdditionalAddresses";
  public static final String KIT_LINES = "KitLines";
  public static final String ORD_LINE_RESERVATIONS = "ORDER_LINE_RESERVATIONS";
  public static final String AWARDS = "Awards";
  public static final String AWARD = "Award";
  public static final String AWARD_AMOUNT = "AwardAmount";
  public static final String AWARD_QTY = "AwardQuantity";
  public static final String ORD_LINE_RESV_KEY = "OrderLineReservationKey";
  public static final String CHARGE_NAME_KEY = "ChargeNameKey";
  public static final String DEPENDENT_ON_TRAN_LINE_ID = "DependentOnTransactionalLineId";
  // GCSTORE-718: Call Center Order Processing:End

  //Pending Receipt Starts
  public static final String WARRANTY_COMPONENT = "WarrantyComponent";
  public static final String SET_COMPONENT = "SetComponent";
  public static final String INDEX_NO = "IndexNo";
  public static final String EXTN_IS_PROBLEM_WITH_CARTON = "ExtnIsProblemWithCarton";
  public static final String BUNDLE_PARENT_ITEM_DESCRIPTION = "BundleParentItemDesc";
  public static final String BUNDLE_PARENT_QUANTITY = "BundleParentQuantity";
  public static final String BUNDLE_ITEM_ID = "BundleItemID";
  public static final String QUANTITY_PICKED = "QuantityPicked";
  //Pending Receipt Ends

  //GCSTORE-454 - POS Invoicing - Start
  public static final String SPLIT_ORDER_FLAG= "SplitOrderFlag";
  public static final String INVOICE_TYPE = "InvoiceType";
  public static final String XML_DATE = "XMLDate";
  public static final String XML_TIME = "XMLTime";
  public static final String GC_PREPARE_POS_WEB_SERVICE_INPUT = "GCPreparePOSWebServiceInput";
  public static final String GC_INVOKE_POS_WEB_SERVICE = "GCInvokePOSWebService";
  public static final String GC_INVOKE_POS_INVOICE_WEB_SERVICE = "GCInvokePOSInvoiceWebService";
  public static final String POS_SALES_TICKET = "POSSalesTicket";
  public static final String RESERVATION_ID = "ReservationID";
  public static final String ERROR_INFO = "Errorinfo";
  public static final String CONFIRM = "CONFIRM";
  public static final String IS_WARRANTY_LINE_ONLY = "IsWarrantyLineOnly";
  public static final String EXTN_POS_SALES_TICKET_NO = "ExtnPOSSalesTicketNo";
  public static final String POS_SALES_TICKET_NO = "POSSalesTicketNo";
  public static final String GC_ORDER_LINE_COMMISSION = "GCOrderLineCommission";
  public static final String AQUISATION_COMMISSION = "Acquisition Commission";
  public static final String SALES_COMMISSION = "Sales Commission";
  public static final String GC_INVOICE_POS_ALERT_SERVICE = "GCInvoicePOSAlertService";
  public static final String URL = "URL";
  public static final String ACTION = "Action";
  public static final String EMPLOYEE_ID = "EmployeeID";
  public static final String PERCENTAGE = "Percentage";
  public static final String EXTENDED_PRICE = "ExtendedPrice";
  public static final String EXTN_PRIMARY_COMPONENT_ITEM = "ExtnPrimaryComponentItemID";
  public static final String GC_ORDER_LINE_COMPONENTS_LIST = "GCOrderLineComponentsList";
  public static final String GC_ORDER_LINE_COMPONENTS = "GCOrderLineComponents";
  public static final String AMOUNT = "Amount";
  public static final String IS_DISCOUNT = "IsDiscount";
  public static final String POS_LOCATION = "POSLocation";
  public static final String EXTN_IS_STORE_FULFILLED = "ExtnIsStoreFulfilled";
  public static final String RESERVE_REQUEST_RESULT = "ReserveRequestResult";
  public static final String CONFIRM_RESULT = "ConfirmResult";
  public static final String POS_DATE_TIME_FORMAT = "MMddyy'T'HHmmss";
  public static final String RESALE_NUMBER = "ResaleNumber";
  public static final String EXTN_TAX_EXEMPT_ID = "ExtnTaxExemptId";
  public static final String GC_INVOKE_POS_WARRANTY_INVOICE_WEB_SERVICE = "GCInvokePOSWarrantyInvoiceWebService";
  //GCSTORE-454 - POS Invoicing - End

  // GCStore-786 Start
  public static final String CUSTOMER_SCHEDULING_PREFERENCES = "CustomerSchedulingPreferences";
  public static final String IS_SHIP_COMPLETE = "IsShipComplete";
  public static final String EXTN_FTC_EXEMPT = "ExtnFTCExempt";

  // GCStore-786 End

  //INVC Processing Constants - Start
  public static final String ACTION_TYPE = "ActionType";
  public static final String ORDER_LINE_LIST = "OrderLineList";
  public static final String KIT_QTY = "KitQty";
  //INVC Processing Constants - End

  // Customer Pickup starts
  public static final String ID_TYPE = "IdType";
  public static final String EXTN_ID_TYPE_FOR_VERIFICATION = "ExtnIdTypeForVerification";
  public static final String EXTN_PICKED_UP_DATE = "ExtnPickedUpDate";
  public static final String EXTN_PICKED_UP_BY = "ExtnPickedUpBy";
  public static final String EXTN_IS_PICK_UP_PERSON_VERIFIED = "ExtnIsPickUpPersonVerified";
  public static final String EXTN_PICKED_UP_QTY = "ExtnPickedUpQty";
  public static final String USER_AUTHENTICATED = "UserAuthenticated";
  // Customer Pickup ends

  //GCSTORE-516: Design - DC Returns : Start
  public static final String EXTN_ACTUAL_SERIAL_NUMBER = "ExtnActualSerialNumber";
  public static final String EXTN_POS_SKU_NUMBER = "ExtnPOSSKUNumber";
  public static final String EXTN_POS_CUSTOMER_ID = "ExtnPOSCustomerID";
  public static final String SKU_NUMBERS = "SKUNumbers";
  public static final String DISPOSITION_CODE = "DispositionCode";
  public static final String EXTN_POS_STATUS_CODE = "ExtnPOSStatusCode";
  public static final String EXTN_RETURN_RIS_NO = "ExtnReturnRISNo";
  public static final String EXTN_RETURN_VPO_NO = "ExtnReturnVPONo";
  public static final String STAMP_DISPOSITION_CODE = "StampDispositionCode";

  public static final String RETURNS = "Returns";
  public static final String RETURN = "Return";
  public static final String RECEIVING_NODE = "ReceivingNode";
  public static final String EXTN_INVENTORY_COST = "ExtnInventoryCost";
  public static final String EXTN_POS_70_TICKET_NO = "ExtnPOS70TicketNo";
  public static final String EXTN_POS_RETURN_11_TICKET_NO = "ExtnPOSReturn11TicketNo";
  public static final String POS_DAX_COA_TICKET = "POSDAXCOATicket";
  public static final String EXTN_POS_RETRY_COUNT = "ExtnPOSRetryCount";


  //GCSTORE-516: Design - DC Returns : End

  //POS Inventory Management changes for Platinum Items.
  public static final String EXTN_IS_PLATINUM_ITEM = "ExtnIsPlatinumItem";
  public static final String LOT_ATTR1 = "LotAttribute1";

  //COA Authorization Process - Start
  public static final String PAYMENT_TRANSACTION_REQUEST = "PaymentTransactionRequest";
  public static final String TRANSACTION_TYPE = "TransactionType";
  public static final String TRANSACTION_ID = "TransactionID";
  public static final String SYSTEM_ID= "SystemID";
  public static final String API_TOKEN = "APIToken";
  public static final String SELLING_BRAND = "SellingBrand";
  public static final String SELLING_CHANNEL = "SellingChannel";
  public static final String BILL_TO_PARTY = "BillToParty";
  public static final String CUSTOMER_ID = "CustomerID";
  public static final String SYSTEM_ID_ATT= "systemID";
  public static final String SALES_AGENT = "SalesAgent";
  public static final String ORDER_REFERENCE = "OrderReference";
  public static final String ORDER_ID = "OrderID";
  public static final String ORDER_DATE = "OrderDate";
  public static final String ALLOW_PARTIAL_AUTH = "AllowPartialAuth";
  public static final String CURRENCY_CODE= "CurrencyCode";
  public static final String PRIOR_AUTH_CODE = "PriorAuthCode";
  public static final String PAYMENT_TRANSACTION_ERROR_LIST = "PaymentTransactionErrorList";
  public static final String PAYMENT_TRANSACTION_ERROR = "PaymentTransactionError";
  public static final String PAYMENT_FAILURE = "PAYMENT_FAILURE";
  public static final String MESSAGE_ATTRIBUTE = "Message";
  public static final String RESPONSE_CODE = "ResponseCode";
  // COA Authorization Process - End


  // GCSTORE-2514 : Start
  public static final String TOTAL_TAX = "TotalTax";
  public static final String VERTEX_CALL_FAILED = "VERTEX_CALL_FAILED";
  public static final String LAST_HOLD_TYPE_DATE = "LastHoldTypeDate";
  public static final String HOLD_TYPES_TO_PROCESS = "HoldTypesToProcess";
  public static final String VERTEX_RETRY_PERIOD = "VertexRetryPeriod";
  // GCSTORE-2514 : End

  // GCSTORE-1909 : Start
  public static final String ORDER_INVOICE_DETAIL_KEY = "OrderInvoiceDetailKey";
  public static final String LINE_KEY = "LineKey";
  public static final String TAX_BREAKUP_LIST = "TaxBreakupList";
  public static final String TAX_BREAKUP = "TaxBreakup";
  // GCSTORE-1909 : End

  // GCSTORE-2837 : Start
  public static final String POS_AUTH_NUMBER = "POSAuthNumber";
  public static final int FIVE_INT = 5;
  public static final String SPACE_STRING = " ";
  // GCSTORE-2837 : End
  private GCXmlLiterals() {

  }
}
