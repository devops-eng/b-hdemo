package com.gc.common.utils;

import com.yantra.yfs.japi.YFSException;
import com.yantra.yfs.japi.YFSUserExitException;

public class ExException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ExException(String message) {

		super(message);
	}

	public ExException(Exception ex) {

		super(ex);
	}

	public ExException(String message, Exception ex) {

		super(message, ex);
	}

	public YFSUserExitException getUserExitException() {

		return new YFSUserExitException(this.getMessage());

	}

	public YFSException getYFSException() {

		return new YFSException(this.getMessage());

	}

	public static ExException getExpicientExpection(Exception e) {

		return new ExException(e);

	}

	public static ExException getExpicientExpection(String message, Exception e) {

		return new ExException(message, e);

	}
}
