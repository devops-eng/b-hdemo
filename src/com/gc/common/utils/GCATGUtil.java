/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               06/02/2014        Naveen, Gowda		JIRA No: This class is used to determine if ATG call is required
 *#################################################################################################################################################################
 */
package com.gc.common.utils;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.yfc.util.YFCCommon;

/**
 * @author <a href="mailto:email Address">Name</a>
 */
public class GCATGUtil {

	// initialize logger
	private static final Logger LOGGER = Logger.getLogger(GCATGUtil.class
			.getName());

	private Element eleOrderExtn = null;
	private Document docEleOrder = null;

	/**
	 * 
	 * Description of checkIfATGCallRequired
	 * 
	 * @param env
	 * @param changeOrderInDoc
	 * @param docGetOrderListOut
	 * @throws GCException
	 * 
	 */
	public void checkIfATGCallRequired(Document changeOrderInDoc,
			Document docGetOrderListOut) throws GCException {

		try {
			LOGGER.debug("Incoming document"
					+ GCXMLUtil.getXMLString(docGetOrderListOut));
			Element eleRoot = changeOrderInDoc.getDocumentElement();
			Element eleOrder = GCXMLUtil.getElementByXPath(docGetOrderListOut,
					"/OrderList/Order");
			eleOrderExtn = GCXMLUtil.getElementByXPath(changeOrderInDoc,
					"//Order/Extn");
			if (YFCCommon.isVoid(eleOrderExtn)) {
				eleOrderExtn = changeOrderInDoc.createElement(GCConstants.EXTN);
				eleRoot.appendChild(eleOrderExtn);
			}
			docEleOrder = GCXMLUtil.getDocumentFromElement(eleOrder);
			String sExtnOrderChannel = GCXMLUtil.getAttributeFromXPath(
					docEleOrder, "//@ExtnOrderCaptureChannel");
			String sLevelOfService = GCXMLUtil.getAttributeFromXPath(
					docEleOrder, "//Order/@LevelOfService");

			/*
			 * In case ATG is invoked in change Order, ExtnATGCallIsRequired is
			 * set to N to avoid any infinite loops.
			 */
			eleOrderExtn.setAttribute(GCConstants.EXTN_IS_ATG_CALL_REQUIRED,
					GCConstants.FLAG_N);

			if (GCConstants.CALL_CENTER_ORDER_CHANNEL
					.equalsIgnoreCase(sExtnOrderChannel)
					&& !YFCCommon.isVoid(sLevelOfService)) {

				Double dMaxOrderStatus = fetchMaxOrderStatus(docEleOrder);
				String sExtnIsATGRequired = "";
				if (dMaxOrderStatus < 1100) {
					checkIfFullfilmentTypeHasChanged(changeOrderInDoc);

					sExtnIsATGRequired = eleOrderExtn
							.getAttribute(GCConstants.EXTN_IS_ATG_CALL_REQUIRED);
					if (!GCConstants.FLAG_Y
							.equalsIgnoreCase(sExtnIsATGRequired)) {
						checkIfAddressHasChanged(changeOrderInDoc);
					}
					if (!GCConstants.FLAG_Y
							.equalsIgnoreCase(sExtnIsATGRequired)) {
						checkIfItemIsAddedOrDeleted(changeOrderInDoc);
					}
				}
			}
			LOGGER.debug("Update Change Order document"
					+ GCXMLUtil.getXMLString(changeOrderInDoc));
		} catch (Exception e) {
			LOGGER.error("The exceptionn is in checkIfATGCallRequired method",
					e);
			throw new GCException(e);
		}

	}

	/**
	 * 
	 * Description of checkIfItemIsAddedOrDeleted
	 * 
	 * @param env
	 * @param changeOrderInDoc
	 * @param docEleOrder
	 * @throws GCException
	 * 
	 */
	private void checkIfItemIsAddedOrDeleted(Document changeOrderInDoc)
			throws GCException {

		try {
			LOGGER.debug("checkIfItemIsAddedOrDeleted - Begin");
			NodeList nlOrderLine = GCXMLUtil.getNodeListByXpath(
					changeOrderInDoc, "Order/OrderLines/OrderLine");
			for (int i = 0; i < nlOrderLine.getLength(); i++) {
				Element eleOrderLine = (Element) nlOrderLine.item(i);
				String sOrderLineKey = eleOrderLine
						.getAttribute(GCConstants.ORDER_LINE_KEY);
				String sOrderQty = eleOrderLine
						.getAttribute(GCConstants.ORDERED_QTY);
				String sAction = eleOrderLine.getAttribute(GCConstants.ACTION);
				Document docEleOrderLine = GCXMLUtil
						.getDocumentFromElement(eleOrderLine);
				String sExtnIsFreeGiftItem = GCXMLUtil.getAttributeFromXPath(
						docEleOrderLine, "//@ExtnIsFreeGiftItem");
				if (GCConstants.CREATE.equalsIgnoreCase(sAction)
						&& !GCConstants.FLAG_Y
						.equalsIgnoreCase(sExtnIsFreeGiftItem)
						|| GCConstants.REMOVE.equalsIgnoreCase(sAction)
						&& !GCConstants.FLAG_Y
						.equalsIgnoreCase(sExtnIsFreeGiftItem)) {
					eleOrderExtn.setAttribute(GCConstants.EXTN_IS_ATG_CALL_REQUIRED,
							GCConstants.YES);
				}
				if (!YFCCommon.isVoid(sOrderLineKey)
						&& !YFCCommon.isVoid(sOrderQty)) {
					String sOrginalOrderedQty = GCXMLUtil
							.getAttributeFromXPath(docEleOrder,
									"Order/OrderLines/OrderLine[@OrderLineKey='"
											+ sOrderLineKey + "']/@OrderedQty");
					if (!sOrderQty.equalsIgnoreCase(sOrginalOrderedQty)) {
						eleOrderExtn.setAttribute(GCConstants.EXTN_IS_ATG_CALL_REQUIRED,
								GCConstants.YES);
					}
				}

			}
		} catch (Exception e) {
			LOGGER.error(
					"The exceptionn is in checkIfItemIsAddedOrDeleted method",
					e);
			throw new GCException(e);
		}

	}

	/**
	 * 
	 * Description of checkIfAddressHasChanged
	 * 
	 * @param env
	 * @param changeOrderInDoc
	 * @param docEleOrder
	 * 
	 * @throws GCException
	 * 
	 */
	private void checkIfAddressHasChanged(Document changeOrderInDoc)
			throws GCException {

		try {
			Element elePersonShipTo = GCXMLUtil.getElementByXPath(
					changeOrderInDoc, "//Order/PersonInfoShipTo");
			Element elePersonBillTo = GCXMLUtil.getElementByXPath(
					changeOrderInDoc, "//Order/PersonInfoBillTo");
			NodeList nlOrderLine = GCXMLUtil.getNodeListByXpath(
					changeOrderInDoc, "Order/OrderLines/OrderLine");
			int iOrderLineLength = nlOrderLine.getLength();
			if ((!YFCCommon.isVoid(elePersonBillTo)
					|| !YFCCommon.isVoid(elePersonShipTo))&&(iOrderLineLength == 0)) {

				LOGGER.debug("Inside if Address Chnage");
				eleOrderExtn.setAttribute(GCConstants.EXTN_IS_ATG_CALL_REQUIRED,
						GCConstants.YES);

			}
		} catch (Exception e) {
			LOGGER.error(
					"The exceptionn is in checkIfAddressHasChanged method", e);
			throw new GCException(e);
		}

	}

	/**
	 * 
	 * Description of checkIfFullfilmentTypeHasChanged
	 * 
	 * @param env
	 * @param changeOrderInDoc
	 * @throws GCException
	 * 
	 */
	private void checkIfFullfilmentTypeHasChanged(Document changeOrderInDoc)
			throws GCException {

		try {
			LOGGER.debug("checkIfFullfilmentTypeHasChanged - Begin");
			String sCOExtnFulfillmentType = GCXMLUtil.getAttributeFromXPath(
					changeOrderInDoc, "//@ExtnFulfillmentType");
			String sExtnFulfillmentType = GCXMLUtil.getAttributeFromXPath(
					docEleOrder, "//@ExtnFulfillmentType");
			LOGGER.debug("sCOExtnFulfillmentType:" + sCOExtnFulfillmentType
					+ "sExtnFulfillmentType" + sExtnFulfillmentType);
			if (!YFCCommon.isVoid(sCOExtnFulfillmentType)
					&& !YFCCommon.isVoid(sExtnFulfillmentType)
					&& !sExtnFulfillmentType
					.equalsIgnoreCase(sCOExtnFulfillmentType)) {
				LOGGER.debug("Inside if FulfillmentChanged");
				eleOrderExtn.setAttribute(GCConstants.EXTN_IS_ATG_CALL_REQUIRED,
						GCConstants.YES);
			}
		} catch (Exception e) {
			LOGGER.error("The exceptionn is in callGetUserListOut method", e);
			throw new GCException(e);
		}

	}

	/**
	 * 
	 * Description of fetchMaxOrderStatus
	 * 
	 * @param docEleOrder
	 * @return
	 * @throws GCException
	 * 
	 */
	private Double fetchMaxOrderStatus(Document docEleOrder) throws GCException {

		try {
			String sMaxOrderStatus = GCXMLUtil.getAttributeFromXPath(
					docEleOrder, "Order/@MaxOrderStatus");
			LOGGER.debug("sMaxOrderStatus::" + sMaxOrderStatus);
			String[] parts = sMaxOrderStatus.split("\\.");
			String sOrderStatus = parts[0];
			LOGGER.debug("sOrderStatus::" + sOrderStatus);
			Double dMaxOrderStatus = Double.parseDouble(sOrderStatus);
			LOGGER.debug("dMaxOrderStatus:::" + dMaxOrderStatus);
			return dMaxOrderStatus;
		} catch (Exception e) {
			LOGGER.error("The exceptionn is in callGetUserListOut method", e);
			throw new GCException(e);
		}

	}
	 /**
     * Description of getATGUrlForCatalogSearch
     *
     * @param sUrl
     * @param sSearchCondition
     * @param sConditionVal
     * @return 
     **/
    public static String getATGUrlForCatalogSearch(String sUrl,String sSearchCondition, String sConditionVal) {
        LOGGER.debug("getATGUrlForCatalogSearch() -- Start");
        LOGGER.debug("URL received-->" + sUrl);
        if (!XmlUtils.isVoid(sConditionVal)) {
			if (GCConstants.ATG_CS_MUST.equals(sSearchCondition)) {
				 String sTrimmedConditionVal = sConditionVal.trim();
				 sTrimmedConditionVal= sTrimmedConditionVal.replaceAll("[^a-zA-Z0-9 ]", "");
			        String sNewCondVal="";
			       List<String> tokens = Arrays.asList(sTrimmedConditionVal.split("\\s+"));
			       Iterator<String> iterator = tokens.iterator();
			           
			            while (iterator.hasNext()) {
			            sNewCondVal+="+"+iterator.next();
			            
			     }
			            
			     sConditionVal=sNewCondVal.substring(1);
				sSearchCondition = GCConstants.ATG_CS_NTT;
				sUrl = sUrl + "&" + sSearchCondition + "=" + sConditionVal;
			} else {
					sUrl = sUrl + "&" + sSearchCondition + "=" + sConditionVal;
				}
			}
        LOGGER.debug("Url returning-->" + sUrl);
        LOGGER.debug("getATGUrlForCatalogSearch() -- end");
        return sUrl;
    }

}
