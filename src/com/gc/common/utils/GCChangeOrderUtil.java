/**Copyright 2013 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.

	Change Log
######################################################################################################################################
     OBJECTIVE: Foundation Class
######################################################################################################################################
        Version                     Date						Modified By                   Description
######################################################################################################################################
        1.0							06/02/2014					Susai, John Melvin            Initial Version     
######################################################################################################################################
 */
package com.gc.common.utils;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCChangeOrderUtil {
  
    private static final Logger LOGGER = Logger
      .getLogger(GCChangeOrderUtil.class.getName());
  
    private GCChangeOrderUtil(){
      
    }
    // initialize logger

    /**
     * 
     * Description of addNoteToOrder Add note to an existing order
     * 
     * @param env
     * @param sNoteText
     * @param sOrderHeaderKey
     * @return
     * @throws Exception
     * 
     */
    public static void addNoteToOrder(YFSEnvironment env, String sNoteText,
	    String sOrderHeaderKey) throws GCException {
	try {
	    // Adding Note
	    Document docChangeOrderIp = GCXMLUtil.createDocument("Order");
	    Element eleRoot = docChangeOrderIp.getDocumentElement();
	    eleRoot.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);

	    Element eleNotes = docChangeOrderIp.createElement("Notes");
	    eleRoot.appendChild(eleNotes);
	    Element eleNote = docChangeOrderIp.createElement("Note");
	    eleNotes.appendChild(eleNote);
	    eleNote.setAttribute("NoteText", sNoteText);
	    LOGGER.debug("*****changeOrder input*****"
		    + GCXMLUtil.getXMLString(docChangeOrderIp));
	    GCCommonUtil.invokeAPI(env, "changeOrder", docChangeOrderIp);
	} catch (Exception e) {
	    LOGGER.error("The exception is in addNoteToOrder method:", e);
	    throw new GCException(e);
	}
    }

    /**
     * 
     * Description of addNoteAndUpdateExtnIsBackOrderEmailSent Add note to an
     * existing order and update ExtnIsBackOrderEmailSent
     * 
     * @param env
     * @param sNoteText
     * @param ordEle
     * @return
     * @throws Exception
     * 
     */
    public static void addNoteAndUpdateExtnIsBackOrderEmailSent(
	    YFSEnvironment env, String sNoteText, String sOrderHeaderKey,
	    Element ordEle, String sMinLineStatus) throws GCException {
	try {
	    // Adding Note & set ExtnIsBackOrderEmailSent="Y" for the OrderLine
	    // where MinLineStatus==sMinLineStatus
	    Document docChangeOrderIp = GCXMLUtil.createDocument("Order");
	    Element eleRoot = docChangeOrderIp.getDocumentElement();
	    eleRoot.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);

	    Element eleNotes = docChangeOrderIp.createElement("Notes");
	    eleRoot.appendChild(eleNotes);
	    Element eleNote = docChangeOrderIp.createElement("Note");
	    eleNotes.appendChild(eleNote);
	    eleNote.setAttribute("NoteText",
		    GCConstants.GC_BACKORDER_NOTIFICATION_EMAIL_NOTE);

	    Element eleOrderLines = docChangeOrderIp
		    .createElement("OrderLines");
	    eleRoot.appendChild(eleOrderLines);

	    for (int j = 0; j < ordEle.getElementsByTagName("OrderLine")
		    .getLength(); j++) {
		Element orderLineEle = (Element) ordEle.getElementsByTagName(
			"OrderLine").item(j);
		String minLineStatus = orderLineEle
			.getAttribute("MinLineStatus");
		if (minLineStatus.equals(sMinLineStatus)) {
		    String orderLineKey = orderLineEle
			    .getAttribute("OrderLineKey");

		    Element eleOrderLine = docChangeOrderIp
			    .createElement("OrderLine");
		    eleOrderLines.appendChild(eleOrderLine);
		    eleOrderLine.setAttribute("OrderLineKey", orderLineKey);

		    Element eleExtn = docChangeOrderIp.createElement("Extn");
		    eleOrderLine.appendChild(eleExtn);
		    eleExtn.setAttribute("ExtnIsBackOrderEmailSent", "Y");
		}
	    }

	    LOGGER.debug("*****changeOrder input*****"
		    + GCXMLUtil.getXMLString(docChangeOrderIp));
	    GCCommonUtil.invokeAPI(env, "changeOrder", docChangeOrderIp);
	} catch (Exception e) {
	    LOGGER.error(
		    "The exception is in addNoteAndUpdateExtnIsBackOrderEmailSent method:",
		    e);
	    throw new GCException(e);
	}
    }

}
