/**Copyright 2013 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.

	Change Log
######################################################################################################################################
     OBJECTIVE: Foundation Class
######################################################################################################################################
        Version    CR                 Date                       Modified By                   Description
######################################################################################################################################
        1.0    Initial Version      14/02/2014                  Soni, Karan
        1.1    OMS-1141             14/02/2014                  Singh, Gurpreet                Development - External Order Capture
        1.2    Payments 			02/04/2014					Soni, Karan					   Payment Development
        1.3    Ex-Store             09/02/2015                  Kumaresan,Karthik              Get sorted(CodeValue) common code list
		1.4	   GCSTORE-7083			06/06/2017					Gupta, Abhishek				   Checking and modifying product line for digital item	
######################################################################################################################################
 */

package com.gc.common.utils;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.naming.Context;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;


public class GCCommonUtil {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCCommonUtil.class);

  private GCCommonUtil() {

  }

  /**
   * @param env
   * @param apiName
   * @param inXMLDoc
   * @return
   * @throws Exception
   * @description invoke API within the system
   */
  public static Document invokeAPI(YFSEnvironment env, String apiName, Document inXMLDoc, Document inTemplate) {
    try {
      LOGGER.verbose("Entering invokeAPI method");
      if (!YFCCommon.isVoid(inTemplate)) {
        env.setApiTemplate(apiName, inTemplate);
      }
      YIFApi yifApi = YIFClientFactory.getInstance().getLocalApi();
      Document outXML = yifApi.invoke(env, apiName, inXMLDoc);
      env.clearApiTemplate(apiName);
      LOGGER.verbose("Exiting invokeAPI method");
      return outXML;
    }  catch (YIFClientCreationException e) {
      LOGGER.error("yifclientcreation exception thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;
    } catch (YFSException e) {
      LOGGER.error("yfsexception exception thrown ", e);
      throw e;
    } catch (RemoteException e) {
      LOGGER.error("remote exception thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;
    }
  }

  /**
   *
   * This method is used to invoke any Sterling API.
   *
   * @param env : YFSEnvironment variable.
   * @param apiName : Name of the API to be invoked.
   * @param inputDoc : Input document to be passed to the API.
   * @param templateDoc : Template document to be passed to the API.
   * @return
   *
   */
  public static YFCDocument invokeAPI(YFSEnvironment env, String apiName, YFCDocument inputDoc, YFCDocument templateDoc) {
    Document inTemplate=null;
    Document inXMLDoc = inputDoc.getDocument();
    if (!YFCCommon.isVoid(templateDoc)) {
      inTemplate = templateDoc.getDocument();
    }
    Document outDoc = invokeAPI(env, apiName, inXMLDoc, inTemplate);
    if (!YFCCommon.isVoid(outDoc)) {
      return YFCDocument.getDocumentFor(outDoc);
    } else {
      return null;
    }
  }

  /**
   * Calls a YIF api
   *
   * @param env YFS env
   * @param input Input xml
   * @param apiName String api name
   * @param templateName String template name
   * @return Result xml on calling the sterling api
   * @throws Exception if the api can't be called
   */
  public static Document invokeAPI(YFSEnvironment env, Document input, String apiName, String templateName) {
    Document outputDoc = null;

    if (!isEmpty(templateName)) {
      env.setApiTemplate(apiName, templateName);
    }

    try{
      YIFApi api = YIFClientFactory.getInstance().getApi();
      outputDoc = api.invoke(env, apiName, input);
    } catch (YIFClientCreationException e) {
      LOGGER.error("yifclientcreation exception thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;
    } catch (YFSException e) {
      LOGGER.error("yfsexception exception thrown ", e);
      throw e;
    } catch (RemoteException e) {
      LOGGER.error("remote exception thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;
    }
    env.clearApiTemplate(apiName);

    return outputDoc;
  }

	/**
	 * @param env
	 * @param serviceName
	 * @param inXMLDoc
	 * @return
	 * @description invoke service within the system
	 */
	public static Document invokeService(YFSEnvironment env, String serviceName, Document inXMLDoc)  {
		try {
			LOGGER.verbose("Entering invokeService method");
			YIFApi oApi = YIFClientFactory.getInstance().getLocalApi();
			LOGGER.verbose("Exiting invokeService method");
			return oApi.executeFlow(env, serviceName, inXMLDoc);
		} catch (YIFClientCreationException e) {
			LOGGER.error("yifclientcreation exception thrown ", e);
			YFCException ex = new YFCException(e.getMessage());
			ex.setStackTrace(e.getStackTrace());
			throw ex;
		} catch (YFSException e) {
			LOGGER.error("yfsexception exception thrown ", e);
			throw e;
		} catch (YFCException e) {
			LOGGER.error("yfcexception exception thrown ", e);
			throw e;
		} catch (RemoteException e) {
			LOGGER.error("remote exception thrown ", e);
			YFCException ex = new YFCException(e.getMessage());
			ex.setStackTrace(e.getStackTrace());
			throw ex;
		}
	}

  /**
   *
   * This method is used to invoke any custom service.
   *
   * @param env : YFSEnvironment variable
   * @param serviceName : Name of the service to be invoked.
   * @param inXMLDoc : Input XML to be passed to the service.
   * @return : Output of the service.
   *
   */
  public static YFCDocument invokeService(YFSEnvironment env, String serviceName, YFCDocument inXMLDoc) {
    if (YFCCommon.isVoid(inXMLDoc)) {
      throw new YFSException("Input Document cannot be null.");
    }
    Document doc = invokeService(env, serviceName, inXMLDoc.getDocument());
    if(!YFCCommon.isVoid(doc)){
      return YFCDocument.getDocumentFor(doc);
    }
    return null;
  }

  /**
   * @param env
   * @param orderHeaderKey
   * @param templateDoc
   * @return
   * @throws Exception
   */
  public static Document invokeGetOrderListAPI(YFSEnvironment env, String orderHeaderKey, Document templateDoc){
    if (YFCCommon.isStringVoid(orderHeaderKey)) {
      throw new YFSException("OrderHeaderKey is mandatory.");
    }
    Document outXML = null;
    try {
      LOGGER.verbose("Entering invokeGetOrderListAPI method");
      if (templateDoc != null) {
        env.setApiTemplate("getOrderList", templateDoc);
        outXML = invokeAPI(env, "getOrderList", createOrderInputDocument(orderHeaderKey));
        env.clearApiTemplate("getOrderList");
      } else {
        outXML = invokeAPI(env, "getOrderList", createOrderInputDocument(orderHeaderKey));
      }
      LOGGER.verbose("Exiting invokeGetOrderListAPI method");
    }  catch (YFSException e) {
      LOGGER.error("yfsexception exception thrown ", e);
      throw e;
    }
    return outXML;
  }

  /**
   * @param orderHeaderKey
   * @return
   * @throws GCException
   */
  private static Document createOrderInputDocument(String orderHeaderKey) {
    LOGGER.verbose("Entering createOrderInputDocument method");
    Document orderDetailInXML = GCXMLUtil.createDocument("Order");
    Element rootElement = GCXMLUtil.getRootElement(orderDetailInXML);
    GCXMLUtil.setAttribute(rootElement, "OrderHeaderKey", orderHeaderKey);
    LOGGER.verbose("Exiting createOrderInputDocument method");
    return orderDetailInXML;
  }

  /**
   * @param env
   * @param apiName
   * @param inXMLDoc
   * @return
   * @throws Exception
   * @description invoke API within the system
   */
  public static Document invokeAPI(YFSEnvironment env, String apiName, Document inXMLDoc) {
    LOGGER.verbose("Entering invokeAPI method");
    YIFApi yifApi = null;
    Document outXML = null;
    try {
      yifApi = YIFClientFactory.getInstance().getLocalApi();
      outXML = yifApi.invoke(env, apiName, inXMLDoc);
    } catch (YIFClientCreationException e) {
      LOGGER.error("yifclientcreation exception thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;
    } catch (YFSException e) {
      LOGGER.error("yfsexception exception thrown ", e);
      throw e;
    } catch (RemoteException e) {
      LOGGER.error("remote exception thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;
    }
    LOGGER.verbose("Exiting invokeAPI method");
    return outXML;
  }

  /**
   *
   * Description of isAffirmative Return true for Y / true / 1 else false
   *
   * @param value
   * @return
   *
   */
  public static boolean isAffirmative(String value) {

    if (value == null) {
      return false;
    } else if ("Y".equalsIgnoreCase(value) || "true".equalsIgnoreCase(value) || "1".equals(value)) {
      return true;
    }
    return false;
  }

  /**
   *
   * @param str
   * @return
   */
  public static boolean isEmpty(String str) {
    return ((str == null) || (str.trim().length() == 0));
  }

  /**
   *
   * Description of getItemList Gives output for getItemList
   *
   * @param env
   * @param sItemID
   * @param sUOM
   * @param sOrganizationCode
   * @return
   * @throws Exception
   *
   */
  public static Document getItemList(YFSEnvironment env, String sItemID, String sUOM, String sOrganizationCode) {
    LOGGER.verbose(" Entering GCCommonUtil.getItemList() method ");
    YFCDocument outDoc = getItemList(env, sItemID, sUOM, sOrganizationCode, null);
    return outDoc.getDocument();
  }

  /**
  *
  * Description of getItemList Gives output for getItemList with IgnoreIsSoldSeparately="Y" 
  *
  * @param env
  * @param sItemID
  * @param sUOM
  * @param sOrganizationCode
  * @return
  * @throws Exception
  *
  */
 public static Document getItemList(YFSEnvironment env, String sItemID, String sUOM, String sOrganizationCode, boolean ignoreIsSoldSeparately) {
   LOGGER.verbose(" Entering GCCommonUtil.getItemList() method ");
   YFCDocument outDoc = getItemList(env, sItemID, sUOM, sOrganizationCode, ignoreIsSoldSeparately, null);
   return outDoc.getDocument();
 }

 
  /**
   *
   * Description of getItemList Gives output for getItemList
   *
   * @param env
   * @param sItemID
   * @param sUOM
   * @param sOrganizationCode
   * @return
   * @throws Exception
   *
   */
  public static YFCDocument getItemList(YFSEnvironment env, String sItemID, String sUOM, String sOrganizationCode,
      YFCDocument templateDocument) {

    LOGGER.beginTimer("YFCCommonUtils.getItemList");
    LOGGER.verbose(" Entering GCCommonUtil.getItemList() method ");
    LOGGER.debug(" sItemID " + sItemID + " sUOM " + sUOM + " sOrganizationCode " + sOrganizationCode);
    YFCDocument getItemListOP = getItemList(env,sItemID, sUOM, sOrganizationCode, false, templateDocument);
   
    LOGGER.endTimer("YFCCommonUtils.getItemList");
    return getItemListOP;
  }
  
  /**
  *
  * Description of getItemList Gives output for getItemList
  *
  * @param env
  * @param sItemID
  * @param sUOM
  * @param sOrganizationCode
  * @return
  * @throws Exception
  *
  */
 public static YFCDocument getItemList(YFSEnvironment env, String sItemID, String sUOM, String sOrganizationCode, boolean ignoreIsSoldSeparately, 
     YFCDocument templateDocument) {

   LOGGER.beginTimer("YFCCommonUtils.getItemList");
   LOGGER.verbose(" Entering GCCommonUtil.getItemList() method with ignoreIsSoldSeparately");
   LOGGER.debug(" sItemID " + sItemID + " sUOM " + sUOM + " sOrganizationCode " + sOrganizationCode + " ignoreIsSoldSeparately "+ignoreIsSoldSeparately );

   Document templateDoc = null;
   if (!YFCCommon.isVoid(templateDocument)) {
     templateDoc = templateDocument.getDocument();
   }

   Document docGetItemListIP = GCXMLUtil.createDocument(GCConstants.ITEM);
   Element eleGetItemListIP = docGetItemListIP.getDocumentElement();
   eleGetItemListIP.setAttribute(GCConstants.ITEM_ID, sItemID);
   eleGetItemListIP.setAttribute(GCConstants.ORGANIZATION_CODE, sOrganizationCode);
   eleGetItemListIP.setAttribute(GCConstants.UOM, sUOM);
   if(ignoreIsSoldSeparately){
	   LOGGER.verbose(" Found  ignoreIsSoldSeparately true so appending IgnoreIsSoldSeparately=Y in getItemList");
	   eleGetItemListIP.setAttribute("IgnoreIsSoldSeparately", "Y");
   }
 

   if(YFCCommon.isVoid(templateDoc)){
     templateDoc = GCXMLUtil.getDocument(GCConstants.GET_ITEM_LIST_TEMPLATE);
   }

   Document docGetItemListOP = GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ITEM_LIST, docGetItemListIP, templateDoc);

   if (LOGGER.isVerboseEnabled()) {
     LOGGER.verbose(" docGetItemListOP  is " + GCXMLUtil.getXMLString(docGetItemListOP));
   }

   LOGGER.verbose(" Exiting GCCommonUtil.getItemList() method ignoreIsSoldSeparately");
   LOGGER.endTimer("YFCCommonUtils.getItemList");
   return YFCDocument.getDocumentFor(docGetItemListOP);
 }

  /**
   * To get the document from relative path
   *
   * @param path
   * @return
   * @throws Exception
   */
  public static Document getDocumentFromPath(String path) {
    YFCDocument templateDoc;
    try {
      templateDoc = YFCDocument.parse(GCCommonUtil.class.getResourceAsStream(path));
      if (templateDoc != null) {
        return templateDoc.getDocument();
      } else {
        return null;
      }
    } catch (SAXException e) {
      LOGGER.error("SAXException exception thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;
    } catch (IOException e) {
      LOGGER.error("IOException exception thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;
    }
  }

  /**
   * this method return the array of length 2 . In the first index, it stores the rounded(Rounding
   * mode is half-Up) value In the second index, it stores the difference of original number and
   * rounded value.
   *
   * @param dbNumber
   * @param decPrecision
   * @return
   */
  public static double[] splitNumberForDecimalPrecision(double dbNumber, int decPrecision) {
    DecimalFormat twoDForm = new DecimalFormat("#.##");
    // converting to dwo
    // digit format as
    // per as the
    // assumption that
    // input value will
    // alwasy be having 2
    // digit decimal.
    // to handle NaN values. NaN is returned when 0.0/0.0
    if (Double.isNaN(dbNumber) || Double.isInfinite(dbNumber)) {
      dbNumber = 0.00;
    }
    dbNumber = Double.valueOf(twoDForm.format(dbNumber));
    double[] arraySplitNumberForDecimalPrecision = new double[2];
    BigDecimal bd = new BigDecimal(dbNumber);
    double dbNumberVal = dbNumber;
    dbNumberVal = bd.setScale(decPrecision, RoundingMode.HALF_UP).doubleValue();
    arraySplitNumberForDecimalPrecision[0] = dbNumberVal;
    arraySplitNumberForDecimalPrecision[1] = dbNumber - dbNumberVal;
    return arraySplitNumberForDecimalPrecision;
  }

  /**
   * Format the value to display amount in format ".00"
   *
   * @param dAmount
   * @param decPrecision
   * @return
   * @throws Exception
   */
  public static BigDecimal formatNumberForDecimalPrecision(Double dAmount, int decPrecision) {
    LOGGER.verbose("Inside formatChargeAmount");
    BigDecimal dTmp = new BigDecimal(dAmount).setScale(decPrecision, RoundingMode.HALF_EVEN);
    LOGGER.debug("formatted value: " + dTmp);
    LOGGER.verbose("End formatChargeAmount");
    return dTmp;
  }

  /**
   *
   * @param env
   * @param orderHeaderKey
   * @param templateName
   * @return
   * @throws GCException
   */
  public static Document invokeGetOrderListAPITemp(YFSEnvironment env, String orderHeaderKey, String templateName) {
    Document outXML = null;
    try {
      if (templateName != null) {
        YFCDocument templateDoc;
        templateDoc = YFCDocument.parse(GCCommonUtil.class.getResourceAsStream(templateName));
        env.setApiTemplate("getOrderList", templateDoc.getDocument());
        outXML = invokeGetOrderListAPI(env, orderHeaderKey);
        env.clearApiTemplate("getOrderList");
      } else {
        outXML = invokeGetOrderListAPI(env, orderHeaderKey);
      }
    } catch (SAXException e) {
      LOGGER.error("SAXException  thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;

    } catch (IOException e) {
      LOGGER.error("IOException  thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;

    }
    return outXML;
  }

  public static Document invokeGetOrderListAPI(YFSEnvironment env, String orderHeaderKey) {
    return invokeAPI(env, "getOrderList", createOrderInputDocument(orderHeaderKey));
  }

  /**
   *
   * @param codeType
   * @param orgCode
   * @return
   * @throws YIFClientCreationException
   * @throws RemoteException
   * @throws YFSException
   */
  public static Document getCommonCodeListByType(YFSEnvironment env, String codeType, String orgCode) {

    if (orgCode == null || "".equals(orgCode)) {
      orgCode = GCConstants.DEFAULT_ORGANIZATION;
    }

    LOGGER.debug("Calling Common Code Api for Code Type: " + codeType + " and Organization Code: " + orgCode);

    Document outDoc = null;
    Document inXML = null;
    YIFApi oApi;
    try {
      oApi = YIFClientFactory.getInstance().getLocalApi();
      inXML = GCXMLUtil.createDocument(GCConstants.COMMON_CODE);
      Element rootElement = GCXMLUtil.getRootElement(inXML);
      GCXMLUtil.setAttribute(rootElement, GCConstants.CODE_TYPE, codeType);
      GCXMLUtil.setAttribute(rootElement, GCConstants.ORGANIZATION_CODE, orgCode);

      outDoc = oApi.getCommonCodeList(env, inXML);
      List<Element> commonCodeList = GCXMLUtil.getElementListByXpath(outDoc, GCConstants.COMMON_CODE_XPATH);
      if (commonCodeList.isEmpty() && !GCConstants.DEFAULT_ORGANIZATION.equals(orgCode)) {
        outDoc = getCommonCodeListByType(env, codeType, GCConstants.DEFAULT_ORGANIZATION);
      }
    } catch (YFSException e) {
      LOGGER.error("YFSException  thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;
    } catch (RemoteException e) {
      LOGGER.error("RemoteException  thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;
    } catch (YIFClientCreationException e) {
      LOGGER.error("yifclientcreation exception thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;

    }
    return outDoc;
  }

  /**
   * Sorted based on CodeValue
   * @param codeType
   * @param orgCode
   * @return
   * @throws YIFClientCreationException
   * @throws RemoteException
   * @throws YFSException
   */
  public static Document getCommonCodeListByTypeSortBy(YFSEnvironment env, String codeType, String orgCode) {

    if (orgCode == null || "".equals(orgCode)) {
      orgCode = GCConstants.DEFAULT_ORGANIZATION;
    }

    LOGGER.debug("Calling Common Code Api for Code Type: " + codeType + " and Organization Code: " + orgCode);

    Document outDoc = null;
    Document inXML = null;
    YIFApi oApi;
    try {
      oApi = YIFClientFactory.getInstance().getLocalApi();
      inXML = GCXMLUtil.createDocument(GCConstants.COMMON_CODE);
      Element rootElement = GCXMLUtil.getRootElement(inXML);
      GCXMLUtil.setAttribute(rootElement, GCConstants.CODE_TYPE, codeType);
      GCXMLUtil.setAttribute(rootElement, GCConstants.ORGANIZATION_CODE, orgCode);
      Element orderBy = inXML.createElement(GCConstants.ORDER_BY);
      rootElement.appendChild(orderBy);
      Element attribute = inXML.createElement(GCConstants.ATTRIBUTE);
      orderBy.appendChild(attribute);
      GCXMLUtil.setAttribute(attribute, GCConstants.DESC, GCConstants.N);
      GCXMLUtil.setAttribute(attribute, GCConstants.NAME, GCConstants.CODE_VALUE);

      outDoc = oApi.getCommonCodeList(env, inXML);
      List<Element> commonCodeList = GCXMLUtil.getElementListByXpath(outDoc, GCConstants.COMMON_CODE_XPATH);
      if (commonCodeList.isEmpty() && !GCConstants.DEFAULT_ORGANIZATION.equals(orgCode)) {
        outDoc = getCommonCodeListByType(env, codeType, GCConstants.DEFAULT_ORGANIZATION);
      }
    } catch (YFSException e) {
      LOGGER.error("YFSException  thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;
    } catch (RemoteException e) {
      LOGGER.error("RemoteException  thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;
    } catch (YIFClientCreationException e) {
      LOGGER.error("yifclientcreation exception thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;

    }
    return outDoc;
  }


  public static Document prepareChangeOrderStatusHdrDoc(String sOrderHeaderKey, String sTransactionID) {
    LOGGER.verbose("Entering GCCommonUtil.prepareChangeOrderStatusHdrDoc");
    Document docChangeOrderStatus = GCXMLUtil.createDocument(GCConstants.ORDER_STATUS_CHANGE);
    Element eleChangeOrderStatus = docChangeOrderStatus.getDocumentElement();
    eleChangeOrderStatus.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    eleChangeOrderStatus.setAttribute(GCConstants.IGNORE_TRANSACTION_DEPENDENCIES, GCConstants.YES);
    eleChangeOrderStatus.setAttribute(GCConstants.TRANSACTION_ID, sTransactionID);
    Element eleOrderLinesChngStatus = GCXMLUtil.createElement(docChangeOrderStatus, GCConstants.ORDER_LINES, null);
    eleChangeOrderStatus.appendChild(eleOrderLinesChngStatus);
    LOGGER.verbose("Exiting GCCommonUtil.prepareChangeOrderStatusHdrDoc");
    return docChangeOrderStatus;
  }

  public static void addLinesToChangeOrderStatusDoc(Document docChangeOrderStatus, Element eleOrderLine,
      String sQuantity, String sBaseDropStatus) {
    LOGGER.verbose("Entering GCCommonUtil.addLinesToChangeOrderStatusDoc");
    Element eleOrderLinesChngStatus = GCXMLUtil.getElementByXPath(docChangeOrderStatus, "OrderStatusChange/OrderLines");
    Element eleOrderLineChngStatus = GCXMLUtil.createElement(docChangeOrderStatus, GCConstants.ORDER_LINE, null);
    eleOrderLineChngStatus.setAttribute(GCConstants.ORDER_LINE_KEY,
        eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
    eleOrderLineChngStatus
    .setAttribute(GCConstants.PRIME_LINE_NO, eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO));
    eleOrderLineChngStatus.setAttribute(GCConstants.SUB_LINE_NO, eleOrderLine.getAttribute(GCConstants.SUB_LINE_NO));
    eleOrderLineChngStatus.setAttribute(GCConstants.BASE_DROP_STATUS, sBaseDropStatus);
    eleOrderLineChngStatus.setAttribute(GCConstants.QUANTITY, sQuantity);
    eleOrderLinesChngStatus.appendChild(eleOrderLineChngStatus);
    LOGGER.verbose("Exiting GCCommonUtil.addLinesToChangeOrderStatusDoc");
  }

//Method overloading for Phase-1 Fix
public static void addLinesToChangeOrderStatusDoc(Document docChangeOrderStatus, Element eleOrderLine,
      String sQuantity, String sBaseDropStatus, String sExpectedShipmentDate) {
    LOGGER.verbose("Entering GCCommonUtil.addLinesToChangeOrderStatusDoc");
    Element eleOrderLinesChngStatus = GCXMLUtil.getElementByXPath(docChangeOrderStatus, "OrderStatusChange/OrderLines");
    Element eleOrderLineChngStatus = GCXMLUtil.createElement(docChangeOrderStatus, GCConstants.ORDER_LINE, null);
    eleOrderLineChngStatus.setAttribute(GCConstants.ORDER_LINE_KEY, eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
    eleOrderLineChngStatus.setAttribute(GCConstants.PRIME_LINE_NO, eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO));
    eleOrderLineChngStatus.setAttribute(GCConstants.SUB_LINE_NO, eleOrderLine.getAttribute(GCConstants.SUB_LINE_NO));
    eleOrderLineChngStatus.setAttribute(GCConstants.BASE_DROP_STATUS, sBaseDropStatus);
    eleOrderLineChngStatus.setAttribute(GCConstants.QUANTITY, sQuantity);
    
    if(!YFCCommon.isVoid(sExpectedShipmentDate)) {
    	eleOrderLineChngStatus.setAttribute(GCConstants.FROM_EXPECTED_SHIPMENT_DATE, sExpectedShipmentDate);	
    }
    
    eleOrderLinesChngStatus.appendChild(eleOrderLineChngStatus);
    LOGGER.verbose("Exiting GCCommonUtil.addLinesToChangeOrderStatusDoc");
  }

  /**
   * This method is used to validate store by calling getOrganizationHeirarchy API.
   *
   * @param env
   * @param storeID
   * @param loginID
   */
  public static void validateStore(YFSEnvironment env, String storeID, String loginID) {

    LOGGER.beginTimer("GCCommonUtil.validateStore");
    LOGGER.verbose("Entering GCCommonUtil.validateStore");
    try {
      // preparing input for getOrganizationHeirarchy API call
      final YFCDocument orgHeirarchyIndocYfc = YFCDocument.createDocument(GCConstants.ORGANIZATION);
      final YFCElement eleOrganization = orgHeirarchyIndocYfc.getDocumentElement();
      eleOrganization.setAttribute(GCXmlLiterals.ORGANIZATION_KEY, storeID);

      /*
       * Calling getOrganizationHeirarchy API. If there is any exception, then catch it & throw a
       * custom exception
       */
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("loginIndoc : " + orgHeirarchyIndocYfc.getString());
      }
      final YFCDocument getOrgHeirarchyTmpl = YFCDocument.getDocumentFor((GCConstants.GET_ORG_HEIRARCHY_TEMPLATE));
      final YFCDocument orgHeirarchyOutdocYfc =
          GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORG_HIERARCHY, orgHeirarchyIndocYfc, getOrgHeirarchyTmpl);


      final YFCElement eleOrganizationYfc = orgHeirarchyOutdocYfc.getDocumentElement();
      final YFCElement eleNodeYfc = eleOrganizationYfc.getChildElement(GCConstants.NODE);

      // Checking if NodeType is not equal to "Store" then throwing Exception
      if (!(GCConstants.STORE.equals(eleNodeYfc.getAttribute(GCXmlLiterals.NODE_TYPE)))) {
        YFCException yfce = new YFCException(GCErrorConstants.INVALID_STORE);
        if (!YFCCommon.isVoid(loginID)) {
          yfce.setAttribute(GCConstants.USER_ID, loginID);
        }
        yfce.setAttribute(GCConstants.STORE_ID, storeID);
        throw yfce;
      }
      LOGGER.verbose("valid store ID");

    } catch (YFSException e) {
      LOGGER.error("yifclientcreation exception thrown ", e);
      YFCException yfce = new YFCException(GCErrorConstants.INVALID_STORE);
      if (!YFCCommon.isVoid(loginID)) {
        yfce.setAttribute(GCConstants.USER_ID, loginID);
      }
      yfce.setAttribute(GCConstants.STORE_ID, storeID);
      throw yfce;

    }
    LOGGER.verbose("Exiting GCCommonUtil.validateStore");
    LOGGER.endTimer("GCCommonUtil.validateStore");
  }
  /**
   * This method is developed as a part of GC-Phase2. This Method renames the Nodes Tag Name in the
   * complete inDoc.
   *
   * @param doc
   * @param fromTag
   * @param toTag
   */
  public static void changeTagName(Document doc, String fromTag, String toTag) {
    LOGGER.beginTimer("GCCommonUtil.changeTagName");
    NodeList nodes = doc.getElementsByTagName(fromTag);
    for (int i = 0; i < nodes.getLength(); i++) {
      if (nodes.item(i) instanceof Element) {
        Element elem = (Element) nodes.item(i);
        doc.renameNode(elem, elem.getNamespaceURI(), toTag);
      }
    }
    LOGGER.endTimer("GCCommonUtil.changeTagName");
  }
  /**
   * @param env
   * @param orderHeaderKey
   * @param templateDoc
   * @return
   * @throws Exception
   */
  public static YFCDocument invokeGetOrderListAPI(YFSEnvironment env, String orderHeaderKey, YFCDocument templateDoc) {
    if (YFCCommon.isVoid(templateDoc)) {
      throw new YFSException("Template Document cannot be null.");
    }
    Document outDoc = invokeGetOrderListAPI(env, orderHeaderKey, templateDoc.getDocument());
    return YFCDocument.getDocumentFor(outDoc);
  }

  /**
   * This method returns attribute value as integer
   *
   * @param elem
   * @param attribute
   * @return
   */
  public static int getIntAttribute(YFCElement elem, String attribute) {
    Double d = elem.getDoubleAttribute(attribute);
    return d.intValue();
  }

  /**
   * This util method update Existing Map
   *
   * @param map
   * @param key
   * @param value
   * @return
   */
  public static Map<String, List<String>> updateMap(Map<String, List<String>> map, String key, String value) {
    if (!map.containsKey(value)) {
      List<String> dependentList = new ArrayList<String>();
      dependentList.add(key);
      map.put(value, dependentList);
    } else {
      List<String> dependentList = map.get(value);
      dependentList.add(key);
      map.put(value, dependentList);
    }
    return map;
  }

  /**
   * This Util method Update Existing Map
   *
   * @param map
   * @param key
   * @param value
   * @return
   */
  public static Map<String, List<Integer>> updateMap(Map<String, List<Integer>> map, String key, int value) {
    if (map.containsKey(key)) {
      map.get(key).add(value);
    } else {
      List<Integer> values = new ArrayList<Integer>();
      values.add(value);
      map.put(key, values);
    }
    return map;
  }

  /**
   *
   * Description of getCommonCodeListByTypeAndValue
   *
   * @param env
   * @param codeType
   * @param orgCode
   * @param codeValue
   * @return
   *
   */
  public static YFCDocument getCommonCodeListByTypeAndValue(YFSEnvironment env, String codeType, String orgCode, String codeValue) {

    if (orgCode == null || "".equals(orgCode)) {
      orgCode = GCConstants.DEFAULT_ORGANIZATION;
    }

    LOGGER.debug("Calling Common Code Api for Code Type: " + codeType + " and Organization Code: " + orgCode + "Code value: " + codeValue);

    YFCDocument getCommonCodeListIn =
        YFCDocument.getDocumentFor("<CommonCode CodeType='" + codeType + "' CodeValue='" + codeValue + "' OrganizationCode='" + orgCode + "'/>");
    YFCDocument getCommonCodeListTemplate = YFCDocument.getDocumentFor("<CommonCodeList><CommonCode/></CommonCodeList>");
    return GCCommonUtil.invokeAPI(env, GCConstants.API_GET_COMMON_CODE_LIST, getCommonCodeListIn, getCommonCodeListTemplate);
  }

  /**
   *
   * @param rootEle
   * @param sldapDN
   */
  public static void ldapConnection(YFCElement rootEle, String sldapDN){

    LOGGER.beginTimer("GCCommonUtil.ldapConnection");

    final String sLdapSecurityAuth =YFSSystem.getProperty("yfs.security.authenticator");

    final String sLdapFactory =YFSSystem.getProperty("yfs.security.ldap.factory");

    final String sLdapUrl =YFSSystem.getProperty("yfs.security.ldap.url");

    if(!YFCCommon.isVoid(sLdapSecurityAuth)){
      Hashtable<String, String> env = new Hashtable<String, String>();
      env.put(Context.INITIAL_CONTEXT_FACTORY, sLdapFactory);
      env.put(Context.PROVIDER_URL, sLdapUrl);
      env.put(Context.SECURITY_AUTHENTICATION, "simple");
      env.put(Context.SECURITY_PRINCIPAL, sldapDN);
      env.put(Context.SECURITY_CREDENTIALS, rootEle.getAttribute(GCConstants.PASSWORD_ATTRIBUTE));
      DirContext ctx;
      try {
        ctx = new InitialDirContext(env);
        ctx.close();
      } catch (Exception e) {
        LOGGER.error("Got exception in authenticate method", e);
        YFCException yfce = new YFCException(GCErrorConstants.INVALID_CREDENTIAL_ERRORCODE);
        throw yfce;
      }
      LOGGER.endTimer("GCCommonUtil.ldapConnection");
      LOGGER.debug("Context Created");

    }
  }


  /**
   *
   * @param yfsEnv
   * @param sldapDN
   * @return
   */

  public static YFCDocument userGroupList(YFSEnvironment yfsEnv,String sldapDN){

    LOGGER.beginTimer("GCCommonUtil.userGroupList");
    String userGrpID = GCConstants.BLANK_STRING;

    YFCDocument getUserHierarchyIndoc = YFCDocument.createDocument(GCConstants.USER);
    YFCElement userEle = getUserHierarchyIndoc.getDocumentElement();
    userEle.setAttribute(GCConstants.ATTR_LOGIN_ID, sldapDN);
    YFCDocument getUserHierarchyTemp = YFCDocument.getDocumentFor(GCConstants.GET_USER_HEIRARCHY_TEMPLATE);

    try{
      YFCDocument getUserHierarchyOut = GCCommonUtil.invokeAPI(yfsEnv, GCConstants.GET_USER_HEIRARCHY_API, getUserHierarchyIndoc, getUserHierarchyTemp);
      YFCElement user = getUserHierarchyOut.getDocumentElement();
      YFCElement userGrpLists = user.getChildElement(GCConstants.USER_GROUP_LISTS);
      if (!YFCCommon.isVoid(userGrpLists) && userGrpLists.hasChildNodes()) {
        YFCElement userGrpList = userGrpLists.getChildElement(GCConstants.USER_GROUP_LIST);
        YFCElement userGroup = userGrpList.getChildElement(GCConstants.USER_GROUP);
        userGrpID = userGroup.getAttribute(GCConstants.USER_GROUP_ID);
      }
      YFCElement eleExtn=user.getChildElement(GCConstants.EXTN);
      String sExtnEmpID=eleExtn.getAttribute(GCConstants.EXTN_EMPLOYEE_ID);
      String sUserName = user.getAttribute("Username");

      YFCDocument outDoc=YFCDocument.createDocument("User");
      YFCElement  eleUser=outDoc.getDocumentElement();
      eleUser.setAttribute(GCConstants.USER_GROUP_ID,userGrpID);
      eleUser.setAttribute(GCConstants.EXTN_EMPLOYEE_ID,sExtnEmpID);
      eleUser.setAttribute("Username", sUserName);
      LOGGER.endTimer("GCCommonUtil.userGroupList");
      return outDoc;
    }catch(Exception e){
      LOGGER.error("Got exception in authenticate method", e);
      YFCException yfce = new YFCException(GCErrorConstants.INVALID_CREDENTIAL_ERRORCODE);
      throw yfce;
    }
  }


  public static String getOrganizationNodeType(YFSEnvironment oEnv, String sShipNode) {
    // Calling getOrganizationHierarchy

    LOGGER.debug(" Entering getOrganizationNodeType() method ");
    LOGGER.beginTimer("GCCommonUtil.getOrganizationNodeType");
    if (!(sShipNode == null || "".equals(sShipNode))) {
      YFCDocument docGetOrganizationHierarchyIp = YFCDocument.createDocument(GCConstants.ORGANIZATION);
      YFCElement eleGetOrganizationHierarchyIp = docGetOrganizationHierarchyIp.getDocumentElement();
      eleGetOrganizationHierarchyIp.setAttribute(GCConstants.ORGANIZATION_CODE, sShipNode);
      YFCDocument getOrganizationHierarchyTemplate =
          YFCDocument.getDocumentFor(GCConstants.GET_ORGANIZATION_HIERARCHY_OP_TMP_PICK_REQUEST);
      YFCDocument docGetOrganizationHierarchyOp =
          GCCommonUtil.invokeAPI(oEnv, GCConstants.API_GET_ORG_HIERARCHY, docGetOrganizationHierarchyIp,
              getOrganizationHierarchyTemplate);
      YFCElement eleOrganizationHierarchy = docGetOrganizationHierarchyOp.getDocumentElement();
      YFCElement eleNode = eleOrganizationHierarchy.getChildElement(GCConstants.NODE);
      String sNodeType = eleNode.getAttribute(GCConstants.NODE_TYPE);
      LOGGER.endTimer("GCCommonUtil.getOrganizationNodeType");
      LOGGER.debug("Leaving getOrganizationNodeType() method ");
      return sNodeType;

    } else {
      YFCException yfce = new YFCException(GCErrorConstants.SHIPNODE_BLANK);
      throw yfce;
    }

  }


  // GCSTORE-556 Start
  public static YFCDocument prepareChangeOrderStatusHeaderDoc(String sOrderHeaderKey, String sTransactionID) {
    LOGGER.verbose("Entering GCCommonUtil.prepareChangeOrderStatusHeaderDoc");
    YFCDocument docChangeOrderStatus = YFCDocument.createDocument(GCConstants.ORDER_STATUS_CHANGE);
    YFCElement eleChangeOrderStatus = docChangeOrderStatus.getDocumentElement();
    eleChangeOrderStatus.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    eleChangeOrderStatus.setAttribute(GCConstants.IGNORE_TRANSACTION_DEPENDENCIES, GCConstants.YES);
    eleChangeOrderStatus.setAttribute(GCConstants.TRANSACTION_ID, sTransactionID);
    docChangeOrderStatus.getDocumentElement().createChild(GCConstants.ORDER_LINES);
    LOGGER.verbose("Exiting GCCommonUtil.prepareChangeOrderStatusHeaderDoc");
    return docChangeOrderStatus;
  }

  public static void addLinesToChangeOdrStatusDoc(YFCElement changeOrderStatusOrderLines, YFCElement eleOrderLine,
      String sQuantity, String sBaseDropStatus) {
    LOGGER.verbose("Entering GCCommonUtil.addLinesToChangeOrderStatusDoc");
    YFCElement eleOrderLineChngStatus = changeOrderStatusOrderLines.createChild(GCConstants.ORDER_LINE);
    eleOrderLineChngStatus.setAttribute(GCConstants.ORDER_LINE_KEY,
        eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
    eleOrderLineChngStatus
    .setAttribute(GCConstants.PRIME_LINE_NO, eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO));
    eleOrderLineChngStatus.setAttribute(GCConstants.SUB_LINE_NO, eleOrderLine.getAttribute(GCConstants.SUB_LINE_NO));
    eleOrderLineChngStatus.setAttribute(GCConstants.BASE_DROP_STATUS, sBaseDropStatus);
    eleOrderLineChngStatus.setAttribute(GCConstants.QUANTITY, sQuantity);
    LOGGER.verbose("Exiting GCCommonUtil.addLinesToChangeOrderStatusDoc");
  }
  // GCSTORE-556 End

  public static void setPhase(YFSEnvironment env, Document inDoc) {
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleRoot = inputDoc.getDocumentElement();
    YFCElement eleExtn = eleRoot.getChildElement(GCConstants.EXTN);
    if (!YFCCommon.isVoid(eleExtn)) {
      String sExtnOrderCaptureChannel = eleExtn.getAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL);
      if (YFCUtils.equals(GCConstants.CALL_CENTER_ORDER_CHANNEL, sExtnOrderCaptureChannel)
          || YFCUtils.equals(GCConstants.GCSTORE, sExtnOrderCaptureChannel)) {
        String codeType = GCConstants.SOURCE_SYSTEM + GCConstants.HYPHEN + GCConstants.OMS;
        YFCDocument commonCodeOp =
            GCCommonUtil.getCommonCodeListByTypeAndValue(env, codeType, GCConstants.GC, sExtnOrderCaptureChannel);
        YFCElement eleCommonCode = commonCodeOp.getElementsByTagName(GCConstants.COMMON_CODE).item(0);
        if (!YFCCommon.isVoid(eleCommonCode)) {
          String codeLongDesc = eleCommonCode.getAttribute(GCConstants.CODE_LONG_DESCRIPTION);
          YFCDocument chOrdIp = YFCDocument.createDocument(GCConstants.ORDER);
          YFCElement chOrderRootEle = chOrdIp.getDocumentElement();
          String orderHeaderKey = eleRoot.getAttribute(GCConstants.ORDER_HEADER_KEY);
          chOrderRootEle.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
          chOrderRootEle.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
          YFCElement eleChangeOrdExtn = chOrderRootEle.createChild(GCConstants.EXTN);
          eleChangeOrdExtn.setAttribute(GCConstants.EXTN_ORDER_PHASE, codeLongDesc);
          GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, chOrdIp, null);
        }
      }

    }
  }
  //GCSTORE-6005 and GCSTORE-6009
  
    @SuppressWarnings("unused")
    public static HashMap<String, String> checkforBundleParent(Element ordEle) {
        HashMap<String, String> hmMinLine = new HashMap<String, String>();
      BigDecimal dmlMinLineStatus=new BigDecimal(90000);
      BigDecimal dmlScheduleStatus=new BigDecimal(1500);
      BigDecimal tmpDmlMinOrderStatus=new BigDecimal(90000);
      String strOrderLineMinStatus=null;
      String strOrderLineKey=null;
     
        for (int i = 0; i < ordEle.getElementsByTagName("OrderLine").getLength(); i++)
        {
           
            Element eleOrderLine = (Element) ordEle
                    .getElementsByTagName("OrderLine").item(i);
            
            Element eleExtn = (Element) eleOrderLine.getElementsByTagName("Extn")
                    .item(0);
         
            
           if(!YFCCommon.isVoid(eleOrderLine.getAttribute("MinLineStatus")))
           {
               String strMinStatus = eleOrderLine.getAttribute("MinLineStatus");
               String[] minStatusSubList = strMinStatus.split("\\.");
               final int noOfSubStatus = minStatusSubList.length;
               LOGGER.verbose("noOfSubStatus :" + noOfSubStatus);
               Double dMaxOrderStatus = 0.0;
               if (noOfSubStatus > 2) {
                   strMinStatus = minStatusSubList[0] + "."
                           + minStatusSubList[1];
                  
               } 
             tmpDmlMinOrderStatus=new BigDecimal(strMinStatus);
           
             
           }
           String strKitCode=eleOrderLine.getAttribute("KitCode");
           String strIsWarrantyItem=eleExtn.getAttribute("ExtnIsWarrantyItem");
            if((!YFCCommon.isVoid(strKitCode) && "BUNDLE".equalsIgnoreCase(strKitCode)) || tmpDmlMinOrderStatus.compareTo(dmlScheduleStatus)==0 ||(!YFCCommon.isVoid(strIsWarrantyItem) && "Y".equalsIgnoreCase(strIsWarrantyItem)))
            {
                continue;
            }
         
            if(dmlMinLineStatus.compareTo(tmpDmlMinOrderStatus)>0.0d)
            {
                dmlMinLineStatus=tmpDmlMinOrderStatus;
                
                strOrderLineKey=eleOrderLine.getAttribute("OrderLineKey");
                strOrderLineMinStatus=eleOrderLine.getAttribute("MinLineStatus");
            }
         }
        if(LOGGER.isDebugEnabled())
        {
            LOGGER.debug("GCCommonUtil :: checkforBundleParent :: minLineStatus :: "+dmlMinLineStatus + "strOrderLineKey ::"+strOrderLineKey);
        }
        
        hmMinLine.put(strOrderLineKey, strOrderLineMinStatus);
        return hmMinLine;
    }
    
    /**
    *
    * Description of stampPOBox Check If (PersonInfoShipTo/@ AddressLine1 || PersonInfoShipTo/@
    * AddressLine2 || PersonInfoShipTo/@ AddressLine3 like '%P%O%%BOX%') Stamp ExtnIsPOBox=�Y�
    * Added this method in GCCommonUtil for YFCElement and YFCDocument
    *
    * @param elePersonInfoShipTo
    * @param inDoc
    * @throws GCException
    *
    */
   public static void stampPOBox(YFCElement elePersonInfoShipTo, YFCDocument inDoc) throws GCException {
	    
	    LOGGER.beginTimer("GCCommonUtil..stampPOBox()");
	    LOGGER.verbose("GCCommonUtil..stampPOBox() : Start");
     try {
       // GCSTORE-1916::begin
    	 YFCElement eleExtn = elePersonInfoShipTo.getChildElement(GCConstants.EXTN);
       if (!YFCCommon.isVoid(eleExtn) && YFCCommon.equals(eleExtn.getAttribute("ExtnIsPOBox"), "Y")) {

    	   LOGGER.verbose("GCCommonUtil..ExtnIsPOBox() : Value already Y ");
         } else {

         // //GCSTORE-1916::end
         String sAddressLine1 = elePersonInfoShipTo.getAttribute(GCConstants.ADDRESS_LINE1);
         String sAddressLine2 = elePersonInfoShipTo.getAttribute(GCConstants.ADDRESS_LINE2);
         String sAddressLine3 = elePersonInfoShipTo.getAttribute(GCConstants.ADDRESS_LINE3);
         String sAddress = sAddressLine1 + sAddressLine2 + sAddressLine3;
         String smAddress=sAddress.toUpperCase();
         // Check if the pattern %P%O%%BOX% exists in the addressLine1,
         // addressLine2 and AddressLine3
         if (smAddress.matches(".*P.*O.*BOX.*")) {
           LOGGER.debug(" if Address = %P%O%%BOX% ");

           if (YFCCommon.isVoid(eleExtn)) {
             LOGGER.debug(" Inside if(YFCCommon.isVoid(eleExtn))");
             eleExtn = GCXMLUtil.createElement(inDoc, GCConstants.EXTN, null);
             elePersonInfoShipTo.appendChild(eleExtn);
           }
           eleExtn.setAttribute(GCConstants.EXTN_IS_PO_BOX, GCConstants.YES);
         }
       }
     } catch (Exception e) {
       LOGGER.error(" Inside catch of GCCommonUtil.stampPOBox()", e);
       throw new GCException(e);
     }
     LOGGER.debug(" Exiting GCCommonUtil.stampPOBox() method ");
   }
   
   /**
   *
   * Description of stampApoFpo Check If(PersonInfoShipTo/@State =('AE'|| 'AP' || 'AA')){ Stamp
   * ExntIsAPOFPO = �Y� and returns true if it is APOFPO address
   *  Added this method in GCCommonUtil for YFCElement and YFCDocument
   *
   * @param elePersonInfoShipTo
   * @param inDoc
   * @throws GCException
   *
   */
  public static void stampApoFpo(YFCElement elePersonInfoShipTo, YFCDocument inDoc) throws GCException {
    
    LOGGER.beginTimer("GCCommonUtil..stampApoFpo()");
    LOGGER.verbose("GCCommonUtil..stampApoFpo() : Start");

    try {
      // GCSTORE-1916::begin
    	YFCElement eleExtn = elePersonInfoShipTo.getChildElement(GCConstants.EXTN);
//      Element eleExtn = (Element) XPathAPI. selectSingleNode(elePersonInfoShipTo, GCConstants.EXTN);
      if (!YFCCommon.isVoid(eleExtn) && YFCCommon.equals(eleExtn.getAttribute("ExtnIsAPOFPO"), "Y")) {
    	  
    	  LOGGER.verbose("GCCommonUtil..ExtnIsAPOFPO() : Value already Y ");
      } else {

        String sState = elePersonInfoShipTo.getAttribute(GCConstants.STATE);
        if (GCConstants.AP.equalsIgnoreCase(sState) || GCConstants.AE.equalsIgnoreCase(sState)
            || GCConstants.AA.equalsIgnoreCase(sState)) {
          LOGGER.debug(" if State =('AE'|| 'AP' || 'AA')");

          if (YFCCommon.isVoid(eleExtn)) {
            LOGGER.debug(" Inside if(YFCCommon.isVoid(eleExtn))");
            eleExtn = GCXMLUtil.createElement(inDoc, GCConstants.EXTN, null);
            elePersonInfoShipTo.appendChild(eleExtn);
          }
          eleExtn.setAttribute(GCConstants.EXTN_IS_APO_FPO, GCConstants.YES);
        }
      }
    } catch (Exception e) {
      LOGGER.error(" Inside catch of GCCommonUtil.stampApoFpo()", e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCCommonUtil.stampApoFpo() method ");
  }
      //GCSTORE - 7083 :: Begin
  /**
  *
  * Description of checkModifyProductLine Check If(Item/PrimaryInformation/ProductLine=PV (DigitalItem)
  * Check for isFirmPrdeDefinedNode is Y OR ShipNode is not empty, set these attributes as blanks
  *  Added this method in GCCommonUtil for Document
  *
  */
  public static void checkModifyProductLine (YFSEnvironment env , Document inDoc)
  {
	  LOGGER.beginTimer("GCCommonUtil..checkModifyProductLin()");
	    LOGGER.verbose("GCCommonUtil..checkModifyProductLin() : Start");
	    
	    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
	    YFCElement yfcInDocRoot = yfcInDoc.getDocumentElement();
	    YFCElement yfcOLs = yfcInDocRoot.getChildElement(GCConstants.ORDER_LINES);
	   
	    if(!YFCCommon.isVoid(yfcOLs))
	    {
	    	YFCNodeList<YFCElement> listOrderLine = yfcOLs.getElementsByTagName(GCConstants.ORDER_LINE);
	    
	    for (YFCElement eleOrderLine : listOrderLine) {
	    	YFCElement yfcItem = eleOrderLine.getChildElement(GCConstants.ITEM);
	    	if(!YFCCommon.isVoid(yfcItem))
	    	{
			String sItemId = yfcItem.getAttribute(GCConstants.ITEM_ID);
	    	LOGGER.verbose("sItemId:" +sItemId);
	    	
	    	if(!YFCCommon.isVoid(sItemId))
	    	{
	    		Document docGetItemListIP = GCXMLUtil.createDocument(GCConstants.ITEM);
	    		docGetItemListIP.getDocumentElement().setAttribute(GCConstants.ITEM_ID, sItemId);
	    		LOGGER.verbose("docGetItemListIP:" +docGetItemListIP);
	    		Document docGetItemListTemplate = GCXMLUtil.getDocument(GCConstants.GET_ITEM_LIST_TEMPLATE);
	    		Document docGetItemListOp = GCCommonUtil.invokeAPI(env, GCConstants.GET_ITEM_LIST, docGetItemListIP, docGetItemListTemplate);
	    		LOGGER.verbose("docGetItemListOp:" +docGetItemListOp);
	    		Element eledocGetItemListOpRoot = docGetItemListOp.getDocumentElement();
	    		Element eleItem = (Element) eledocGetItemListOpRoot.getElementsByTagName(GCConstants.ITEM).item(0);
	    		Element elePrimaryInfo = (Element) eleItem.getElementsByTagName(GCConstants.PRIMARY_INFORMATION).item(0);
	    		String sProdLine = elePrimaryInfo.getAttribute(GCConstants.PRODUCT_LINE);
	    		LOGGER.verbose("sProdLine:" +sProdLine);
	    		
	    	if (YFCUtils.equals("PV", sProdLine))
	    	{
	    		String sIsFirmPredefinedNode = eleOrderLine.getAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE);
	    		String sShipNode = eleOrderLine.getAttribute(GCConstants.SHIP_NODE);
	    		
	    		if (YFCUtils.equals(GCConstants.YES, sIsFirmPredefinedNode) || !YFCUtils.isVoid(sShipNode))
	    		{
	    			//blank out the shipnode and IsFirmPredefinedNode
	    			eleOrderLine.setAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE, "");
	    			eleOrderLine.setAttribute(GCConstants.SHIP_NODE, "");
	    			LOGGER.verbose("GCConstants.IS_FIRM_PREDEFINED_NODE" +GCConstants.IS_FIRM_PREDEFINED_NODE);
	    			LOGGER.verbose("GCConstants.SHIP_NODE" +GCConstants.SHIP_NODE);
	    		}
	    	}
	    }
	    	}
  }
  }
  }  
  //GCSTORE - 7083 :: End
}
