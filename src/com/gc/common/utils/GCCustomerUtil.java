/**Copyright 2013 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.

    Change Log
######################################################################################################################################
     OBJECTIVE: This class is utility class which has method which can get called to integrate with ATG and POS on customer creation or updation
######################################################################################################################################
        Version    CR                 Date                   Modified By                   Description
######################################################################################################################################
        1.0    Initial Version      02/02/2015                 Taj, Rashma       GCStore213- POS Customer Creation
                                                                                 GCStore326- Tax Exemption Approval Updates
                                                                                 GCSTORE-218 - ATGIntegration for Customer Management
######################################################################################################################################
 */
package com.gc.common.utils;

import java.io.IOException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class GCCustomerUtil {
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCCustomerUtil.class.getName());

	private GCCustomerUtil() {

	}

	/**
	 * This method will be called when the manage customer api is called from
	 * COM
	 *
	 * @param env
	 * @param inDoc
	 * @return
	 * @throws GCException
	 */
	public static YFCDocument checkCustomerEmailID(YFSEnvironment env,
			YFCDocument inDoc) {
		LOGGER.beginTimer("checkCustomerEmailID");
		YFCElement customerConatctEle = inDoc.getElementsByTagName(
				"CustomerContact").item(0);
		String strEmailId = customerConatctEle.getAttribute("EmailID");
		if (YFCCommon.isVoid(strEmailId)) {
			YFCException yfs = new YFCException(GCErrorConstants.EXTN_GCE0013,
					"The Email Id cannot be blank");
			yfs.setErrorDescription("The Email Id cannot be blank ");
			yfs.setAttribute("CustomerID", strEmailId);
			throw yfs;
		}
		String sCustomerIDInput = inDoc.getDocumentElement().getAttribute(
				"CustomerID");
		if (YFCCommon.isVoid(sCustomerIDInput) && !YFCCommon.isVoid(strEmailId)) {
			YFCDocument getCustomerListInput = YFCDocument
					.getDocumentFor("<Customer><CustomerContactList><CustomerContact EmailID='"
							+ strEmailId
							+ "' /></CustomerContactList></Customer>");
			YFCDocument getCustomerListTmpl = YFCDocument
					.getDocumentFor(GCCommonUtil
							.getDocumentFromPath("/global/template/api/getCustomerList_TemplateXML.xml"));
			YFCDocument getCustomerListOutDoc = GCCommonUtil.invokeAPI(env,
					GCConstants.GET_CUSTOMER_LIST, getCustomerListInput,
					getCustomerListTmpl);
			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("The getCustomerListOutDoc output document is"
						+ getCustomerListOutDoc.toString());
			}
			YFCNodeList<YFCElement> nlCustomer = getCustomerListOutDoc
					.getElementsByTagName("Customer");
			if (nlCustomer.getLength() > 0) {
				LOGGER.verbose("Entering the exception block  if customer with same Email Id exists");
				YFCException yfs = new YFCException(
						GCErrorConstants.EXTN_GCE0014,
						"The Email Id already exist in record");
				throw yfs;
			}
		} else if (!YFCCommon.isVoid(sCustomerIDInput)
				&& !YFCCommon.isVoid(strEmailId)) {
			YFCDocument getCustomerListInput = YFCDocument
					.getDocumentFor("<Customer><CustomerContactList><CustomerContact EmailID='"
							+ strEmailId
							+ "' /></CustomerContactList></Customer>");
			YFCDocument getCustomerListTmpl = YFCDocument
					.getDocumentFor(GCCommonUtil
							.getDocumentFromPath("/global/template/api/getCustomerList_TemplateXML.xml"));
			YFCDocument getCustomerListOutDoc = GCCommonUtil.invokeAPI(env,
					GCConstants.GET_CUSTOMER_LIST, getCustomerListInput,
					getCustomerListTmpl);
			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("The getCustomerListOutDoc output document is"
						+ getCustomerListOutDoc.toString());
			}
			YFCNodeList<YFCElement> customerNodeList = getCustomerListOutDoc
					.getElementsByTagName("Customer");
			if (customerNodeList.getLength() > 0) {
				String sCustomerIDEmail = customerNodeList.item(0)
						.getAttribute("CustomerID");
				if (!YFCCommon.equals(sCustomerIDInput, sCustomerIDEmail)) {
					YFCException yfs = new YFCException(
							GCErrorConstants.EXTN_GCE0014,
							"The Email Id already exist in record");
					throw yfs;
				}
			}

		}
		LOGGER.endTimer("checkCustomerEmailID");
		return inDoc;
	}

	/**
	 * This method checks if POS Integration is required during TaxExemption for
	 * the customer based on few conditions.
	 */
	public static boolean isPOSIntegrationRequired(YFSEnvironment env,
			YFCDocument inDoc, YFCDocument getCustomerListOutput) {
		LOGGER.beginTimer("evaluateCondition");
		boolean result = false;
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Incoming XML Doc" + inDoc.toString());
		}
		YFCElement eleNewCustomer = inDoc.getDocumentElement();
		String custId = eleNewCustomer.getAttribute("CustomerID");
		YFCElement eleExtnNew = eleNewCustomer.getChildElement("Extn");
		if (YFCCommon.isVoid(eleExtnNew)) {
			return result;
		}
		String newTaxExemptStatus = eleExtnNew
				.getAttribute("ExtnTaxExemptReqStatus");
		String newTaxExemptExpiry = eleExtnNew
				.getAttribute("ExtnTaxExemptReqValidUntil");
		String newTaxExemptState = eleExtnNew
				.getAttribute("ExtnTaxExemptState");

		if (YFCCommon.isVoid(custId)
				&& YFCCommon.equals(newTaxExemptStatus,
						GCConstants.TAX_EXEMPT_APPROVED)) {
			// throw Exception Tax Exemption cannot be approved during customer
			// creation
			YFCException excep = new YFCException(
					GCErrorConstants.EXTN_GCE0015,
					"TaxExemption cannot be approved during Customer Creation");
			excep.setAttribute("CustomerID", custId);
			throw excep;
		} else if (YFCCommon.isVoid(custId)) {
			// if a new customer is getting created then return false
			return result;
		}
		YFCElement eleCustomerList = getCustomerListOutput.getDocumentElement();
		YFCElement eleOldCustomer = eleCustomerList.getChildElement("Customer");
		YFCElement eleExtnOld = eleOldCustomer.getChildElement("Extn");
		String oldTaxExemptStatus = eleExtnOld
				.getAttribute("ExtnTaxExemptReqStatus");
		String oldTaxExemptExpiry = "";
		if (!YFCCommon.isVoid(eleExtnOld
				.getAttribute("ExtnTaxExemptReqValidUntil"))) {
			oldTaxExemptExpiry = eleExtnOld.getAttribute(
					"ExtnTaxExemptReqValidUntil").substring(0, 10);
		}
		String oldTaxExemptState = eleExtnOld
				.getAttribute("ExtnTaxExemptState");
		if (YFCCommon.equals(oldTaxExemptStatus,
				GCConstants.TAX_EXEMPT_INPOGRESS)
				&& YFCCommon.equals(newTaxExemptStatus,
						GCConstants.TAX_EXEMPT_APPROVED)) {
			LOGGER.verbose("TaxExemptStatus Change from InProgress to Approved");
			result = true;
		} else if (YFCCommon.equals(oldTaxExemptStatus,
				GCConstants.TAX_EXEMPT_DECLINED)
				&& YFCCommon.equals(newTaxExemptStatus,
						GCConstants.TAX_EXEMPT_APPROVED)) {
			LOGGER.verbose("TaxExemptStatus Change from Declined to Approved");
			result = true;
		} else if (YFCCommon.equals(oldTaxExemptStatus,
				GCConstants.TAX_EXEMPT_APPROVED)
				&& YFCCommon.equals(newTaxExemptStatus,
						GCConstants.TAX_EXEMPT_DECLINED)) {
			LOGGER.verbose("TaxExemptStatus Change from Approved to Declined");
			result = true;
		} else if (YFCCommon.equals(newTaxExemptStatus,
				GCConstants.TAX_EXEMPT_APPROVED)
				&& !YFCCommon.equals(newTaxExemptState, oldTaxExemptState)) {
			LOGGER.verbose("TaxExemptState Change");
			result = true;
		} else if (YFCCommon.equals(newTaxExemptStatus,
				GCConstants.TAX_EXEMPT_APPROVED)
				&& !YFCCommon.equals(oldTaxExemptExpiry, newTaxExemptExpiry)) {
			LOGGER.verbose("TaxExemptExpiry Date Change");
			result = true;
		}
		LOGGER.endTimer("evaluateCondition");
		return result;
	}

	/**
	 * This method calls getCustomerList API with the given customerId and
	 * Organization Code.
	 *
	 * @param env
	 * @param customerId
	 * @param orgCode
	 * @return
	 */
	public static YFCDocument callGetCustomerList(YFSEnvironment env,
			String customerId, String orgCode) {
		LOGGER.beginTimer("callGetCustomerList");
		YFCDocument inDoc = YFCDocument.createDocument("Customer");
		YFCElement rootEle = inDoc.getDocumentElement();
		rootEle.setAttribute("CustomerID", customerId);
		rootEle.setAttribute("OrganizationCode", orgCode);
		Document getCustomerListmplDoc = GCCommonUtil
				.getDocumentFromPath("/global/template/api/getCustomerList_TemplateXML.xml");
		YFCDocument getCustListOutDoc = GCCommonUtil.invokeAPI(env,
				GCConstants.GET_CUSTOMER_LIST, inDoc,
				YFCDocument.getDocumentFor(getCustomerListmplDoc));
		LOGGER.endTimer("callGetCustomerList");
		return getCustListOutDoc;

	}

	/**
	 * This method handles POS Integration during TaxExemption Changes. This
	 * Method gets all the customer information and calls GCPOSIntegration
	 * Service to interact with POS and gets the POS Customer ID and updates it
	 * in OMS DB.
	 *
	 * @param env
	 * @param inDoc
	 * @return
	 */
	public static YFCDocument handlePOSIntegForTaxExempt(YFSEnvironment env,
			YFCDocument yfceInDoc, YFCDocument getCustomerListOutput) {
		LOGGER.beginTimer("handlePOSIntegForTaxExempt");
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Input XML to handlePOSIntegForTaxExempt ::"
					+ yfceInDoc.toString());
		}
		// fetch the input elements from input.
		YFCElement eleCustomerIndoc = yfceInDoc.getDocumentElement();
		String sCustomerID = eleCustomerIndoc.getAttribute("CustomerID");
		YFCElement eleExtnIndoc = eleCustomerIndoc.getChildElement("Extn");

		// Get CustomerList Elements
		YFCElement eleCustomerListAPI = getCustomerListOutput
				.getDocumentElement();
		YFCElement eleCustomerAPI = eleCustomerListAPI
				.getChildElement("Customer");
		YFCElement eleExtnAPI = eleCustomerAPI.getChildElement("Extn");
		YFCElement eleCustomerContactListAPI = eleCustomerAPI
				.getChildElement("CustomerContactList");
		YFCElement eleCustomerContactAPI = eleCustomerContactListAPI
				.getChildElement("CustomerContact");
		// copy customer level attributes from method input to api output.
		eleCustomerAPI.setAttributes(eleCustomerIndoc.getAttributes());
		if (!YFCCommon.isVoid(eleExtnIndoc)) {
			// copy Extn level attributes from method Input to API Output
			eleExtnAPI.setAttributes(eleExtnIndoc.getAttributes());
		}
		YFCElement eleCustomerContactListIndoc = eleCustomerIndoc
				.getChildElement("CustomerContactList");
		if (!YFCCommon.isVoid(eleCustomerContactListIndoc)) {
			YFCElement eleCustomerContactIndoc = eleCustomerContactListIndoc
					.getChildElement("CustomerContact");
			// copy Contact level attributes from method Input to API Output
			eleCustomerContactAPI.setAttributes(eleCustomerContactIndoc
					.getAttributes());
		}

		YFCDocument webserviceRequest = YFCDocument.createDocument("Customer");
		// prepare merged doc.
		YFCElement webServiceRoot = webserviceRequest.getDocumentElement();
		GCXMLUtil
				.copyElement(webserviceRequest, eleCustomerAPI, webServiceRoot);
		Document webserviceResponse = null;
		try {
			webserviceResponse = GCCommonUtil.invokeService(env,
					GCConstants.GC_POS_INTEGRATION_SERVICE,
					webserviceRequest.getDocument());
			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("Webservice response::"
						+ GCXMLUtil.getXMLString(webserviceResponse));
			}
		} catch (YFCException yfs) {
			LOGGER.error("POS Web-Service call Failed", yfs);
			webserviceResponse = null;
		} catch (YFSException yfs) {
			LOGGER.error("POS Web-Service call Failed", yfs);
			webserviceResponse = null;
		}
		if (webserviceResponse == null) {
			// if webservice response is null, raise an alert and continue.
			YFCDocument alertDoc = YFCDocument.createDocument("Alert");
			GCCommonUtil.invokeService(env,
					GCConstants.GC_RAISE_CUSTOMER_SYN_FAILURE_ALERT_SERVICE,
					alertDoc);
			return yfceInDoc;
		} else {
			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("POS Webservice Response::"
						+ GCXMLUtil.getXMLString(webserviceResponse));
			}
			Node createResultEle = webserviceResponse.getElementsByTagName(
					"CreateResult").item(0);
			if (!YFCCommon.isVoid(createResultEle)) {
				String createResult = createResultEle.getTextContent();
				YFCDocument actualResponseDoc = YFCDocument
						.getDocumentFor(createResult);
				if (LOGGER.isVerboseEnabled()) {
					LOGGER.verbose("actual Response::"
							+ actualResponseDoc.toString());
				}
				String errorMsg = actualResponseDoc
						.getElementsByTagName("Errorinfo").item(0)
						.getNodeValue();
				if (!YFCCommon.isVoid(errorMsg)) {
					// if error response from POS, throw an error on UI.
					YFCException yfsExcep = new YFCException(
							GCErrorConstants.EXTN_GCE0016,
							"POS Integration failed");
					yfsExcep.setErrorDescription(errorMsg);
					yfsExcep.setAttribute("OMSCustomerID", sCustomerID);
					throw yfsExcep;
				} else {
					// if success response from POS, stamp POSCustomerId on the
					// Customer record.
					String posCustId = actualResponseDoc
							.getElementsByTagName("POSBillcustID").item(0)
							.getNodeValue();
					YFCElement extnEle = yfceInDoc.getDocumentElement()
							.getChildElement("Extn", true);
					extnEle.setAttribute("ExtnPOSCustomerID", posCustId);
					if (LOGGER.isVerboseEnabled()) {
						LOGGER.verbose("Output Document from HandlePOS Integration after stamping POS Customer ID::"
								+ yfceInDoc.toString());
					}

					LOGGER.endTimer("handlePOSIntegForTaxExempt");
					return yfceInDoc;
				}
			} else {
				YFCDocument alertDoc = YFCDocument.createDocument("Alert");
				GCCommonUtil
						.invokeService(
								env,
								GCConstants.GC_RAISE_CUSTOMER_SYN_FAILURE_ALERT_SERVICE,
								alertDoc);
				return yfceInDoc;
			}
		}

	}

	/**
	 * This method is called whenever a customer is created or updated in Call
	 * Center. This method calls ATG webservice to get the ATG Profile Id.
	 *
	 * @param env
	 * @param sTimeOut
	 * @param inDoc
	 * @return
	 */
	public static YFCDocument manageATGCustomer(YFSEnvironment env,
			YFCDocument yfceInDoc, YFCDocument getCustomerListOutDoc,
			String sTimeOut) {
		LOGGER.beginTimer("manageATGCustomer");
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Input XML to manageATGCustomer ::"
					+ yfceInDoc.toString());
		}
		// create a merged doc.
		YFCDocument mergedDoc = YFCDocument
				.getDocumentFor(yfceInDoc.toString());
		YFCElement eleCustomerIndoc = yfceInDoc.getDocumentElement();

		String sCustomerID = eleCustomerIndoc.getAttribute("CustomerID");
		if (!YFCCommon.isVoid(sCustomerID)) {
			// existing customer updation request.
			YFCElement eleExtnIndoc = eleCustomerIndoc.getChildElement("Extn");
			// Get CustomerList Elements
			YFCElement eleCustomerListAPI = getCustomerListOutDoc
					.getDocumentElement();
			YFCElement eleCustomerAPI = eleCustomerListAPI
					.getElementsByTagName("Customer").item(0);
			YFCElement eleExtnAPI = eleCustomerAPI.getChildElement("Extn");
			YFCElement eleCustomerContactListAPI = eleCustomerAPI
					.getChildElement("CustomerContactList");
			YFCElement eleCustomerContactAPI = eleCustomerContactListAPI
					.getChildElement("CustomerContact");

			copyAttributes(eleCustomerIndoc, eleCustomerAPI);
			if (!YFCCommon.isVoid(eleExtnIndoc)) {
				copyAttributes(eleExtnIndoc, eleExtnAPI);
			}
			YFCElement eleCustomerContactListIndoc = eleCustomerIndoc
					.getChildElement("CustomerContactList");
			if (!YFCCommon.isVoid(eleCustomerContactListIndoc)) {
				YFCElement eleCustomerContactIndoc = eleCustomerContactListIndoc
						.getChildElement("CustomerContact");
				copyAttributes(eleCustomerContactIndoc, eleCustomerContactAPI);
			}
			mergedDoc = null;
			mergedDoc = YFCDocument.createDocument("Customer");
			GCXMLUtil.copyElement(mergedDoc, eleCustomerAPI,
					mergedDoc.getDocumentElement());
		}
		// get the soap action and url from customer overrides property file.

		String sSoapAction = YFSSystem
				.getProperty(GCConstants.ATG_INTEGRATION_SOAP_ACTION);
		String sSoapUrlForATG = YFSSystem
				.getProperty(GCConstants.ATG_CUSTOMER_CREATE_WEB_SERVICE_URL);
		GCWebServiceUtil obj = new GCWebServiceUtil();
		YFCElement eleCustomerMerged = mergedDoc.getDocumentElement();

		Document inDocSoap = prepareSoapIndoc(eleCustomerMerged);
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Soap Request for the ATG Webservice Call::"
					+ GCXMLUtil.getXMLString(inDocSoap));
		}
		Document docATGResponse;
		try {
			// invoke ATG Customer Sync Webservice
			docATGResponse = obj.invokeSOAPWebService(inDocSoap,
					sSoapUrlForATG, sSoapAction, sTimeOut);
		} catch (IOException | YFCException yfsExcep) {
			LOGGER.error("ATG Web-Service call Failed", yfsExcep);
			docATGResponse = null;
		}

		if (!YFCCommon.isVoid(docATGResponse)) {
			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("Soap Webservice Response from ATG::"
						+ GCXMLUtil.getXMLString(docATGResponse));
			}
			YFCDocument docATGManageCustomerOutput = YFCDocument
					.getDocumentFor(docATGResponse);
			YFCElement customerEleResponse = docATGManageCustomerOutput
					.getElementsByTagName("ns1:customerID").item(0);
			if (YFCCommon.isVoid(customerEleResponse)) {
				// fi there is an error response from ATG
				docATGResponse = null;
			} else {
				String sATGCustomerID = customerEleResponse.getNodeValue();
				eleCustomerIndoc.getChildElement("Extn", true).setAttribute(
						"ExtnSourceCustomerID", sATGCustomerID);
			}
		}
		if (YFCCommon.isVoid(docATGResponse)) {
			YFCDocument inDocAlert = YFCDocument.createDocument("Alert");
			GCCommonUtil.invokeService(env,
					GCConstants.GC_RAISE_CUSTOMER_SYN_FAILURE_ALERT_SERVICE,
					inDocAlert);
		}
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Output Document from GCATGIntegrationService::"
					+ yfceInDoc.toString());
		}
		LOGGER.endTimer("manageATGCustomer");
		return yfceInDoc;
	}

	/**
	 * This method copies all the attributes from given srcElement to given
	 * destination element.
	 *
	 * @param srcElem
	 * @param destElem
	 */
	public static void copyAttributes(YFCElement srcElem, YFCElement destElem) {
		LOGGER.beginTimer("copyAttributes");
		destElem.setAttributes(srcElem.getAttributes());
		LOGGER.endTimer("copyAttributes");
	}

	/**
	 * This method fetches Soap Request Template for ATG and stamps all the
	 * values from given input element.
	 *
	 * @param eleCustomerMerged
	 * @return
	 */
	public static Document prepareSoapIndoc(YFCElement eleCustomerMerged) {
		LOGGER.beginTimer("prepareSoapIndoc");
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("prepareSoapIndoc:Input::"
					+ eleCustomerMerged.toString());
		}
		YFCElement eleCustomerContactListMerged = eleCustomerMerged
				.getChildElement("CustomerContactList");
		YFCElement eleCustomerContactMerged = eleCustomerContactListMerged
				.getElementsByTagName("CustomerContact").item(0);
		YFCElement eleCustomerAdditionalAddressListMerged = eleCustomerContactMerged
				.getChildElement("CustomerAdditionalAddressList");

		Document inDoc = GCCommonUtil
				.getDocumentFromPath("/global/template/api/extn_ATGCallRequest.xml");
		YFCDocument inDocSoap = YFCDocument.getDocumentFor(inDoc);
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Soap Input Template:" + GCXMLUtil.getXMLString(inDoc));
		}
		YFCElement eleSoapEnv = inDocSoap.getDocumentElement();
		YFCElement eleCSoapBody = eleSoapEnv.getChildElement("soapenv:Body");
		YFCElement eleCustomerSoap = eleCSoapBody
				.getChildElement("v0:Customer");
		YFCElement eleCustomerData = eleCustomerSoap
				.getChildElement("v0:CustomerData");
		eleCustomerData.getChildElement("com:ATGCustomerID").setNodeValue(
				eleCustomerMerged.getChildElement("Extn").getAttribute(
						"ExtnSourceCustomerID"));
						
		//GCPRO-301: Stamping TaxExemptID in customer sync req to ATG: Start.
		eleCustomerData.getChildElement("com:TaxExemptID").setNodeValue(
				eleCustomerMerged.getChildElement("Extn").getAttribute(
						"ExtnTaxExemptId"));
		//GCPRO-301: Stamping TaxExemptID in customer sync req to ATG: End.				

		//==================================
		eleCustomerData.getChildElement("com:TaxExemptRequested").setNodeValue(
				eleCustomerMerged.getChildElement("Extn").getAttribute(
						"ExtnTaxExemptReq"));
		eleCustomerData.getChildElement("com:TaxExemptRequestedDate").setNodeValue(
				eleCustomerMerged.getChildElement("Extn").getAttribute(
						"ExtnTaxExemptReqDate"));
		eleCustomerData.getChildElement("com:TaxExemptReqStatus").setNodeValue(
				eleCustomerMerged.getChildElement("Extn").getAttribute(
						"ExtnTaxExemptReqStatus"));
		eleCustomerData.getChildElement("com:TaxExemptReqValidUntil").setNodeValue(
				eleCustomerMerged.getChildElement("Extn").getAttribute(
						"ExtnTaxExemptReqValidUntil"));
		eleCustomerData.getChildElement("com:TaxExemptState").setNodeValue(
				eleCustomerMerged.getChildElement("Extn").getAttribute(
						"ExtnTaxExemptState"));
		//====================================
						
		eleCustomerData.getChildElement("com:FirstName").setNodeValue(
				eleCustomerContactMerged.getAttribute("FirstName"));
		eleCustomerData.getChildElement("com:LastName").setNodeValue(
				eleCustomerContactMerged.getAttribute("LastName"));
		eleCustomerData.getChildElement("com:MiddleName").setNodeValue(
				eleCustomerContactMerged.getAttribute("MiddleName"));
		eleCustomerData.getChildElement("com:Email").setNodeValue(
				eleCustomerContactMerged.getAttribute("EmailID"));
		eleCustomerData.getChildElement("com:Birthday").setNodeValue(
				eleCustomerContactMerged.getAttribute("DateOfBirth"));

		String sDayPhone = eleCustomerContactMerged.getAttribute("DayPhone");
		String sMobilePhone = eleCustomerContactMerged
				.getAttribute("MobilePhone");

		if (!YFCCommon.isVoid(sMobilePhone)) {
			YFCElement elePhone1 = eleCustomerData.getElementsByTagName(
					"com:Phone").item(0);
			elePhone1.setNodeValue(sMobilePhone);
			elePhone1.setAttribute("com:type", "mobile");
			elePhone1.setAttribute("com:isPrimary", "true");
		} else if (!YFCCommon.isVoid(sDayPhone)) {
			YFCElement elePhone1 = eleCustomerData.getElementsByTagName(
					"com:Phone").item(0);
			elePhone1.setNodeValue(sDayPhone);
			elePhone1.setAttribute("com:type", "mobile");
			elePhone1.setAttribute("com:isPrimary", "true");
		} else {
			YFCElement elePhone1 = eleCustomerData.getElementsByTagName(
					"com:Phone").item(0);
			elePhone1.setNodeValue("");
			elePhone1.setAttribute("com:type", "mobile");
			elePhone1.setAttribute("com:isPrimary", "true");
		}

		YFCNodeList<YFCElement> nlCustomerAdditionalAddressMerged = eleCustomerAdditionalAddressListMerged
				.getElementsByTagName("CustomerAdditionalAddress");
		for (int i = 0; i < nlCustomerAdditionalAddressMerged.getLength(); i++) {
			YFCElement eleCustomerAdditionalAddressMerged = nlCustomerAdditionalAddressMerged
					.item(i);

			YFCElement eleAddress = eleCustomerSoap
					.getChildElement("v0:Address");

			if (i > 0) {
				// clone the element for multiple address
				YFCElement eleAddressClone = (YFCElement) eleAddress
						.cloneNode(true);
				eleCustomerData.appendChild(eleAddressClone);
				eleAddress = eleAddressClone;
			}
			if (YFCCommon.equals(GCConstants.YES,
					eleCustomerAdditionalAddressMerged
							.getAttribute("IsDefaultShipTo"))) {
				eleAddress.setAttribute("com:isPrimaryShipping", "true");
			} else {
				eleAddress.setAttribute("com:isPrimaryShipping", "false");
			}
			if (YFCCommon.equals(GCConstants.YES,
					eleCustomerAdditionalAddressMerged
							.getAttribute("IsDefaultBillTo"))) {
				eleAddress.setAttribute("com:isPrimaryBilling", "true");
			} else {
				eleAddress.setAttribute("com:isPrimaryBilling", "false");
			}
			if (YFCCommon
					.equals(GCConstants.YES, eleCustomerAdditionalAddressMerged
							.getAttribute("IsShipTo"))) {
				eleAddress.setAttribute("com:isShipping", "true");
			} else {
				eleAddress.setAttribute("com:isShipping", "false");
			}
			if (YFCCommon
					.equals(GCConstants.YES, eleCustomerAdditionalAddressMerged
							.getAttribute("IsBillTo"))) {
				eleAddress.setAttribute("com:isBilling", "true");
			} else {
				eleAddress.setAttribute("com:isBilling", "false");
			}

			YFCElement elePersonInfoAPI = eleCustomerAdditionalAddressMerged
					.getChildElement("PersonInfo");
			if (!YFCCommon.isVoid(elePersonInfoAPI)) {
				YFCElement eleExtnAPI = elePersonInfoAPI
						.getChildElement("Extn");
				if (YFCCommon.equals(GCConstants.YES,
						eleExtnAPI.getAttribute("ExtnIsPOBox"))) {
					eleAddress.setAttribute("com:isPOBox", "true");
				} else {
					eleAddress.setAttribute("com:isPOBox", "false");
				}
				eleAddress.getChildElement("com:FirstName").setNodeValue(
						elePersonInfoAPI.getAttribute("FirstName"));
				eleAddress.getChildElement("com:LastName").setNodeValue(
						elePersonInfoAPI.getAttribute("LastName"));
				eleAddress.getChildElement("com:CompanyName").setNodeValue(
						elePersonInfoAPI.getAttribute("Company"));
				eleAddress.getChildElement("com:Street1").setNodeValue(
						elePersonInfoAPI.getAttribute("AddressLine1"));
				eleAddress.getChildElement("com:Street2").setNodeValue(
						elePersonInfoAPI.getAttribute("AddressLine2"));
				eleAddress.getChildElement("com:City").setNodeValue(
						elePersonInfoAPI.getAttribute("City"));
				eleAddress.getChildElement("com:State").setNodeValue(
						elePersonInfoAPI.getAttribute("State"));
				eleAddress.getChildElement("com:Country").setNodeValue(
						elePersonInfoAPI.getAttribute("Country"));
				eleAddress.getChildElement("com:ZipCode").setNodeValue(
						elePersonInfoAPI.getAttribute("ZipCode"));
				eleAddress.getChildElement("com:Phone").setNodeValue(
						elePersonInfoAPI.getAttribute("MobilePhone"));
			}
		}
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("prepareSoapIndoc:Output::" + inDocSoap.toString());
		}
		LOGGER.verbose("ATG Webservice Request::" + inDocSoap.toString());
		LOGGER.endTimer("prepareSoapIndoc");
		return inDocSoap.getDocument();
	}
}
