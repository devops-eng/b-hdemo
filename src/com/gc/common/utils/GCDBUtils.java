package com.gc.common.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.yantra.shared.ycp.YFSContext;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCDBUtils {
  /**
   *
   * Class to generate unique sequence number used for GiftCard Identification (GCSTORE#6526) This
   * class will be called from GCStoredValueCardUE.
   *
   * @author <a href="mailto:email Address">Shanil Sharma</a>
   */
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCDBUtils.class);

  /**
   * Generate unique number Description of generateUniqueSequence
   *
   * @param env
   * @return
   * @throws SQLException
   * @throws Exception
   *
   */
  public static String generateUniqueSequence(YFSEnvironment env, String query) throws SQLException {
    Connection con = null;
    Statement stmt = null;
    ResultSet rs = null;
    Long sequenceNumber = null;

    try {
      con = ((YFSContext) env).getConnection();
      stmt = con.createStatement();
      rs = stmt.executeQuery(query);
      LOGGER.debug("Result set ###### " + rs.next());
      sequenceNumber = rs.getLong(1);

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      if (rs != null) {
        rs.close();
      }

      if (stmt != null) {
        stmt.close();
      }
    }
    LOGGER.debug("Output from the db sequence is ######### " + sequenceNumber);
    return String.valueOf(sequenceNumber);
  }
}
