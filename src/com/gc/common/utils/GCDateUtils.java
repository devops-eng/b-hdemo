/**
 * Copyright 2014 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  ######################################################################################################################################
 *   OBJECTIVE: This class is format the dates.
 *  ######################################################################################################################################
 *        Version    Date          Modified By           CR                Description
 *  ######################################################################################################################################
 *        1.0       02/04/2014     Soni, Karan                             Initial Version Date Utils
 *
 * *  ######################################################################################################################################
 */

package com.gc.common.utils;

// Java imports
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * Contains collection of Date utilities
 */
public class GCDateUtils {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCCommonUtil.class);

  /**
   * The constant for the date format yyyyMMdd
   */
  public static final String YYYYMMDD = "yyyyMMdd";
  private GCDateUtils(){

  }
  /**
   * Returns short default date string format i.e. <code>yyyyMMdd</code>
   */
  protected static String getShortDefaultDateFormat() {
    // Yantra short default date string format
    return YYYYMMDD;
  }

  /**
   * Converts date object to date-time string
   *
   * @param inputDate
   *            Date object to be converted
   * @param outputFormat
   *            Output format. Refer to
   *            <code>java.text.SimpleDateFormat</code> for date format codes
   * @return Formatted date-time string
   * @throws IllegalArgumentException
   *             for Invalid input
   * @throws Exception
   *             for all others
   */
  public static String formatDate(java.util.Date inputDate,
      String outputFormat) {
    // Validate input date value
    if (inputDate == null) {
      throw new IllegalArgumentException("Input date cannot "
          + " be null in DateUtils.formatDate method");
    }

    // Validate output date format
    if (outputFormat == null) {
      throw new IllegalArgumentException("Output format cannot"
          + " be null in DateUtils.formatDate method");
    }

    // Apply formatting
    SimpleDateFormat formatter = new SimpleDateFormat(outputFormat);
    return formatter.format(inputDate);
  }

  /**
   * Returns default date-time string format i.e.
   * <code>yyyyMMdd'T'HH:mm:ss</code>
   *
   * @return Default date-time string i.e. yyyyMMdd'T'HH:mm:ss
   */
  protected static String getDefaultDateFormat() {
    // Yantra default date-time string format
    return "yyyyMMdd'T'HH:mm:ss";
  }

  /**
   * Returns default date-time string format i.e.
   * <code>yyyyMMdd'T'HH:mm:ss</code>
   *
   * @return Default date-time string i.e. yyyyMMdd'T'HH:mm:ss
   */
  protected static String getDefaultDateFormatISO() {
    // Yantra default date-time string format
    return "yyyy-MM-dd'T'HH:mm:ss";
  }

  /**
   * Converts date object to date-time string in default date format
   *
   * @param inputDate
   *            Date object to be converted
   * @return Date-time string in default date format
   * @throws IllegalArgumentException
   *             for Invalid input
   * @throws Exception
   *             for all others
   * @see getDefaultDateFormat
   */
  public static String convertDate(java.util.Date inputDate){
    return formatDate(inputDate, getDefaultDateFormat());
  }

  /**
   * Converts date-time string to Date object. Date-time string should be in
   * default date format
   *
   * @param inputDate
   *            Date-time string to be converted
   * @return Equivalent date object to input string
   * @throws IllegalArgumentException
   *             for Invalid input
   * @throws Exception
   *             for all others
   */
  public static java.util.Date convertDate(String inputDate){

    // Validate input date value
    if (inputDate == null) {
      throw new IllegalArgumentException("Input date cannot "
          + " be null in DateUtils.convertDate method");
    }
    if (inputDate.indexOf('T') != -1 && inputDate.indexOf('-') == -1) {
      return convertDate(inputDate, getDefaultDateFormat());
    } else if (inputDate.indexOf('T') != -1 && inputDate.indexOf('-') != -1) {

      return convertDate(inputDate, getDefaultDateFormatISO());
    } else {

      return convertDate(inputDate, getShortDefaultDateFormat());
    }
  }

  /**
   * Converts date-time string to Date object
   *
   * @param inputDate
   *            Date-time string to be converted
   * @param inputDateFormat
   *            Format of date-time string. Refer to
   *            <code>java.util.SimpleDateFormat</code> for date format codes
   * @return Equivalent date object to input string
   * @throws IllegalArgumentException
   *             for Invalid input
   * @throws Exception
   *             for all others
   */
  public static java.util.Date convertDate(String inputDate,
      String inputDateFormat) {
    // Validate Input Date value
    if (inputDate == null) {
      throw new IllegalArgumentException("Input date cannot be null"
          + " in DateUtils.convertDate method");
    }

    // Validate Input Date format
    if (inputDateFormat == null) {
      throw new IllegalArgumentException("Input date format cannot"
          + " be null in DateUtils.convertDate method");
    }

    // Apply formatting
    SimpleDateFormat formatter = new SimpleDateFormat(inputDateFormat);

    ParsePosition position = new ParsePosition(0);
    return formatter.parse(inputDate, position);
  }

  /**
   * Returns current date-time string in desired format
   *
   * @param outputFormat
   *            Desired output date-time format
   * @return Current date-time string in desired format
   * @throws IllegalArgumentException
   *             for Invalid input
   * @throws Exception
   *             for all others
   */
  public static String getCurrentTime(String outputFormat) {
    // Create current date object
    Date currentDateTime = new Date();

    // Apply formatting
    return formatDate(currentDateTime, outputFormat);
  }

  /**
   * Adds leading zeros to given value until max length is reached
   */
  private static String addLeadingZeros(long value, int maxLength) {
    String result = Long.toString(value);

    int remaining = maxLength - result.length();
    for (int index = 0; index < remaining; index++) {
      result = "0" + result;
    }
    return result;
  }

  /**
   * Returns time difference between two date objects
   *
   * @param startTime
   *            Start time
   * @param endTime
   *            End time. End time should be greater than Start time.
   * @return Time difference in HH:mm:ss.SSS format
   * @throws IllegalArgumentException
   *             for Invalid input
   * @throws Exception
   *             for all others
   */
  public static String getTimeDifference(Date startTime, Date endTime) {
    // Validate Start time
    if (startTime == null) {
      throw new IllegalArgumentException("Start time cannot be"
          + " null in DateUtils.getTimeDifference method");
    }

    // Validate End time
    if (endTime == null) {
      throw new IllegalArgumentException("End time cannot be "
          + "null in DateUtils.getTimeDifference method");
    }

    // Check whether start time is less than end time
    if (startTime.after(endTime)) {
      throw new IllegalArgumentException(
          "End time should be greater than Start time in"
              + "  DateUtils.getTimeDifference method");
    }

    long longStartTime = startTime.getTime();
    long longEndTime = endTime.getTime();

    // Get total difference in milli seconds
    long difference = longEndTime - longStartTime;

    long temp = difference;

    // Get milli seconds
    long milliseconds = temp % 1000;
    temp = temp / 1000;

    // Get seconds
    long seconds = temp % 60;
    temp = temp / 60;

    // Get Minutes
    long minutes = temp % 60;
    temp = temp / 60;

    // Get Hours
    long hours = temp;

    // Calculate result
    String result = addLeadingZeros(hours, 2) + ":"
        + addLeadingZeros(minutes, 2) + ":"
        + addLeadingZeros(seconds, 2) + "."
        + addLeadingZeros(milliseconds, 3);

    // Format result and return
    return result;
  }

  /**
   * Returns time difference between two date objects in hours
   *
   * @param startTime
   *            Start time
   * @param endTime
   *            End time. End time should be greater than Start time.
   * @return Time difference in hours
   * @throws IllegalArgumentException
   *             for Invalid input
   * @throws Exception
   *             for all others
   */
  public static long getTimeDifferenceInHours(Date startTime, Date endTime) {
    // Validate Start time
    if (startTime == null) {
      throw new IllegalArgumentException("Start time cannot be null in "
          + " DateUtils.getTimeDifferenceInHours method");
    }

    // Validate End time
    if (endTime == null) {
      throw new IllegalArgumentException("End time cannot be null in "
          + "DateUtils.getTimeDifferenceInHours method");
    }

    // Check whether start time is less than end time
    if (startTime.after(endTime)) {
      throw new IllegalArgumentException(
          "End time should be greater than Start time in"
              + "  DateUtils.getTimeDifferenceInHours method");
    }

    long differenceInHours = (endTime.getTime() - startTime.getTime())
        / (1000 * 3600);

    // Return number of hours
    return differenceInHours;
  }

  /**
   * Returns difference between two dates in days. This method does not
   * consider timings for calculation. Result can be a positive or negative or
   * zero value. This method results the difference as an integer.
   *
   * @param startDate
   *            Start date
   * @param endDate
   *            End date
   * @return Difference in dates
   * @throws IllegalArgumentException
   *             for Invalid input
   * @throws Exception
   *             for all others
   */
  public static int getDifferenceInDays(Date startDate, Date endDate) {
    // Difference in days
    int differenceInDays = 0;

    // Validate Start date
    if (startDate == null) {
      throw new IllegalArgumentException("Start date cannot be"
          + " null in DateUtils.getDifferenceInDays method");
    }

    // Validate End date
    if (endDate == null) {
      throw new IllegalArgumentException("End date cannot be "
          + "null in DateUtils.getDifferenceInDays method");
    }

    // Calculate difference in days
    long difference = (endDate.getTime() - startDate.getTime())
        / (1000 * 86400);

    // Set sign
    differenceInDays = (int) difference;

    return differenceInDays;
  }

  /**
   * Adds specified interval to input date. Valid values for Interval are
   * Calendar.YEAR, Calendar.MONTH, Calendar.DATE etc. See Calendar API for
   * more information
   *
   * @param inputDate
   *            Input Date
   * @param interval
   *            Interval
   * @param amount
   *            Amount to add(use negative numbers to subtract
   * @return Date after addition
   * @throws IllegalArgumentException
   *             for Invalid input
   * @throws Exception
   *             for all others
   */
  public static Date addToDate(Date inputDate, int interval, int amount) {
    // Validate Input date
    if (inputDate == null) {
      throw new IllegalArgumentException("Input date cannot be"
          + " null in DateUtils.addToDate method");
    }

    // Get instance of calendar
    Calendar calendar = Calendar.getInstance();

    // Set input date to calendar
    calendar.setTime(inputDate);

    // Add amount to interval
    calendar.add(interval, amount);

    // Return result date
    return calendar.getTime();
  }

  /**
   * Returns current date-time string in Sterling Datetime format
   *
   * @param outputFormat
   *            Sterling date-time format
   * @return Current date-time string in desired format
   * @throws IllegalArgumentException
   *             for Invalid input
   * @throws Exception
   *             for all others
   */
  public static String getSterlingCurrentTimes() {
    // Create current date object
    Date currentDateTime = new Date();

    // Apply the Sterling Date & Time Format
    String strDateFormat = formatDate(currentDateTime,
        "DD-MM-YYYYTHH-MM-SS");

    String strNewDateFormat = strDateFormat.substring(0, 22) + ":"
        + strDateFormat.substring(22, 24);

    // Apply formatting
    return strNewDateFormat;
  }

  /**
   * Returns short date string format i.e. <code>yyMM</code>
   */
  public static String getVeryShortDefaultDateFormat() {
    Date currentDateTime = new Date();
    return formatDate(currentDateTime, "YY-MM");
  }

  public static String getCurrentMonth() {
    Date currentDateTime = new Date();
    return formatDate(currentDateTime, "MM");
  }

  public static String getCurrentYear() {
    Date currentDateTime = new Date();
    return formatDate(currentDateTime, "yyyy");
  }

  public static String addHours(String tempDate, int iNumberOfHour, int iSign) {
    Calendar calendar = DatatypeConverter.parseDateTime(tempDate);
    calendar.add(Calendar.HOUR, (iNumberOfHour * iSign));
    return DatatypeConverter.printDateTime(calendar);
  }

  public static String addDays(String tempDate, int iNumberOfDays, int iSign) {
    Calendar calendar = DatatypeConverter.parseDateTime(tempDate);
    calendar.add(Calendar.DATE, (iNumberOfDays * iSign));
    return DatatypeConverter.printDateTime(calendar);
  }

  public static String convertDateFormat(String strDate) {
    String strMonth = strDate.substring(0, 2);
    String strDatee = strDate.substring(2, 4);
    String strYear = strDate.substring(4, strDate.length());
    strDate = strYear + strMonth + strDatee;
    return strDate;
  }

  public static String convertTimeFormat(String strTime) {
    String strHour = strTime.substring(0, 2);
    String strMin = strTime.substring(2, 4);
    String strSec = strTime.substring(4, strTime.length());
    strTime = strHour + strMin + strSec;
    return strTime;
  }

  // Done as part of code merge from Phase1 to Phase2
  public static String getPreviousDate(String outputFormat, int pastNoOfDays) {
    if (YFCCommon.isVoid(outputFormat)) {
      outputFormat = GCConstants.DATE_TIME_FORMAT;
    }
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, -pastNoOfDays);
    return formatDate(cal.getTime(), outputFormat);
  }

  /**
   * To get the Time difference in HH:MM:SS
   *
   * @param startDate
   * @param endDate
   * @return
   */
  public static String getTimeDiffrenceInHoursMinutesAndSeconds(String startDate, String endDate) {


    // Custom date format
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    Date d1 = null;
    Date d2 = null;
    String timeDiff = null;
    try {
      d1 = format.parse(startDate);
      d2 = format.parse(endDate);

      // Get msec from each, and subtract.
      long diff = d2.getTime() - d1.getTime();
      long diffHours = diff / (60 * 60 * 1000);
      long diffMinutes = diff / (60 * 1000) % 60;
      long diffSeconds = (diff / 1000 % 60);
      String strDiffHour = String.valueOf(diffHours);
      String strDiffMinutes = String.valueOf(diffMinutes);
      String strDiffSeconds = String.valueOf(diffSeconds);
      timeDiff = strDiffHour + ":" + strDiffMinutes + ":" + strDiffSeconds;
    } catch (ParseException e) {
      LOGGER.error("ParseException exception thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      throw ex;
    } catch (YFSException e) {
      LOGGER.error("yfsexception exception thrown ", e);
      throw e;
    }
    return timeDiff;
  }

  /**
   * To get the time difference in Days:HH:MM
   *
   * @param startDate
   * @param endDate
   * @return
   */
  public static String getTimeDiffrenceInDaysHoursAndMinutes(String startDate, String endDate) {


    // Custom date format
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    Date d1 = null;
    Date d2 = null;
    String timeDiff = null;
    try {
      d1 = format.parse(startDate);
      d2 = format.parse(endDate);

      // Get msec from each, and subtract.
      long diff = d2.getTime() - d1.getTime();
      long diffDays = diff / (24 * 60 * 60 * 1000);
      long diffHours = diff / (60 * 60 * 1000) % 24;
      long diffMinutes = diff / (60 * 1000) % 60;
      String strDiffDays = String.valueOf(diffDays);
      String strDiffHour = String.valueOf(diffHours);
      String strDiffMinutes = String.valueOf(diffMinutes);
      timeDiff = strDiffDays + ":" + strDiffHour + ":" + strDiffMinutes;
    } catch (ParseException e) {
      LOGGER.error("ParseException exception thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      throw ex;
    } catch (YFSException e) {
      LOGGER.error("yfsexception exception thrown ", e);
      throw e;
    }
    return timeDiff;
  }


  public static Date getCollectionDate() {
    try {
      Date collectionDate = new Date(System.currentTimeMillis() + 5 * 60 * 1000);
      String formattedCollectionDate = new SimpleDateFormat(GCConstants.DATE_TIME_FORMAT).format(collectionDate);
      SimpleDateFormat formatter = new SimpleDateFormat(GCConstants.DATE_TIME_FORMAT);
      return formatter.parse(formattedCollectionDate);
    } catch (ParseException e) {
      LOGGER.error("ParseException exception thrown ", e);
      YFSException ex = new YFSException(e.getMessage());
      throw ex;
    }
  }
  public static String getFromDate(YDate date, int days) {

    int noOfDays = days;
    date.getString("yyyy-MM-dd'T'HH:mm:ss");
    date.changeDate(-noOfDays);
    return date.getString("yyyy-MM-dd'T'HH:mm:ss");
  }


  public static YFCElement getDateTimeForPOS(YFCElement yfcEleRootInput, Date dOrderDateTime) {

    String strXMLDateTime = new SimpleDateFormat(GCXmlLiterals.POS_DATE_TIME_FORMAT).format(dOrderDateTime);
    int intIndexDiff = strXMLDateTime.indexOf('T');
    yfcEleRootInput.setAttribute(GCConstants.ORDER_DATE, strXMLDateTime.substring(0, intIndexDiff));
    yfcEleRootInput.setAttribute(GCConstants.ORDER_TIME, strXMLDateTime.substring(intIndexDiff + 1));

    Date dateXMLDateTime = new Date();
    strXMLDateTime = new SimpleDateFormat(GCXmlLiterals.POS_DATE_TIME_FORMAT).format(dateXMLDateTime);
    intIndexDiff = strXMLDateTime.indexOf('T');
    yfcEleRootInput.setAttribute(GCXmlLiterals.XML_DATE, strXMLDateTime.substring(0, intIndexDiff));
    yfcEleRootInput.setAttribute(GCXmlLiterals.XML_TIME, strXMLDateTime.substring(intIndexDiff + 1));
    return yfcEleRootInput;
  }

  /**
   *
   * @param env
   * @param organizationCode
   * @return
   * @throws ParserConfigurationException
   * @throws RemoteException
   * @throws YIFClientCreationException
   */
  public static String getTimezoneForLocale(final YFSEnvironment env, final String localeCode) throws ParserConfigurationException,
  RemoteException, YIFClientCreationException {


    // Input doc for getLocaleList API
    YFCDocument getLocaleListIndoc = YFCDocument.createDocument(GCConstants.LOCALE);
    getLocaleListIndoc.getDocumentElement().setAttribute(GCConstants.LOCALE_CODE_LOCALELIST, localeCode);
    // Call getLocaleList API
    Document getLocaleListOutput = GCCommonUtil.invokeAPI(env, GCConstants.GET_LOCALE_LIST, getLocaleListIndoc.getDocument());
    // Fetch timeZone and date time format from getLocaleList output
    YFCDocument getLocaleListOutdoc = YFCDocument.getDocumentFor(getLocaleListOutput);
    YFCElement localeListOut = getLocaleListOutdoc.getDocumentElement();
    YFCElement localeEle = localeListOut.getChildElement(GCConstants.LOCALE);
    return localeEle.getAttribute(GCConstants.TIME_ZONE_IN_LOCALELIST);

  }

  /**
   *
   * @param date
   * @param srcTimeZone
   * @param nodeTimeZone
   * @return
   * @throws ParseException
   */
  public static String getQueryDate(String date, String srcTimeZone, String nodeTimeZone) throws ParseException {

    String dateFormat = GCConstants.DATE_TIME_FORMAT;
    SimpleDateFormat dateFormatVar = new SimpleDateFormat(dateFormat);
    dateFormatVar.setTimeZone(TimeZone.getTimeZone(srcTimeZone));
    Date dateVar = dateFormatVar.parse(date);
    dateFormatVar.setTimeZone(TimeZone.getTimeZone(nodeTimeZone));
    return dateFormatVar.format(dateVar);

  }

}
