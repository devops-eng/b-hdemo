package com.gc.common.utils;

import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * <h3>Description :</h3>This java class is used as an utility to add delay a process completions
 *
 * @author Infosys Limited
 */
public class GCDelayUtil implements YIFCustomApi {

  /*
   * The class name
   */
  private static final String CLASS_NAME = GCDelayUtil.class.getName();

  /*
   * logger reference
   */
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(CLASS_NAME);

  /*
   * The arguments set for the api component
   */
  private Properties props;

  /*
   * (non-Javadoc)
   *
   * @see com.yantra.interop.japi.YIFCustomApi#setProperties(java.util.Properties)
   */
  @Override
  public void setProperties(final Properties props) {

    // set the arguments from the service
    this.props = props;

  }


  /**
   * <h3>Description:</h3>This method is used to delay the processing by the amount of millis
   * configured in the service
   *
   * @param yfsEnv The yfs environment reference
   * @param docInput The input document
   * @return The input document
   */
  public Document delay(final YFSEnvironment yfsEnv, final Document docInput) {

    // log the input
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASS_NAME + ":delay:entry:Entry Time=" + System.currentTimeMillis());
    }

    // get the argument configure
    final String strDelay = props.getProperty("DELAY_IN_MILLIS");

    // check if the configured value is numeric
    if (StringUtils.isNumeric(strDelay)) {

      // parse to numeric
      final long lDelayInMillis = Long.parseLong(strDelay);

      try {

        // put the current thread to sleep
        Thread.sleep(lDelayInMillis);

      } catch (final InterruptedException intrrptedExcep) {

        // log the interruption
        LOGGER.warn(CLASS_NAME + ":delay: The sleep process was interrupted!!!");
      }

    } else {

      // log an error
      LOGGER.error(CLASS_NAME
          + ":delay:The argument DELAY_IN_MILLIS configured for the delay component is not configured or non numeric");
    }

    // log the output
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASS_NAME + ":delay:exit:Output=" + System.currentTimeMillis());
    }

    // return the input document
    return docInput;
  }
}