/**
 * Copyright ¬© 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: Email Specific Methods which can we reused across email related classes.
 *#################################################################################################################################################################
 *          Version             Date                Modified By                 Description
 *#################################################################################################################################################################
 1.0                09/09/2014          Gupta, Himanshu                Email Specific Methods 
 *#################################################################################################################################################################
 */

package com.gc.common.utils;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class GCEmailUtils {
  
    private GCEmailUtils(){
    }
    
    public static Document returnNoRecordFoundDoc() throws ParserConfigurationException {
        Document doc = GCXMLUtil.createDocument("Response");
        Element elem = doc.getDocumentElement();
        elem.setAttribute("ResponseCode", "ERROR");
        elem.setAttribute("ResponseDescription", "No Record Found");
        return doc;
    }

    public static Document sendSuccessMessageDoc() throws ParserConfigurationException{
        Document doc = GCXMLUtil.createDocument("Response");
        Element elem = doc.getDocumentElement();
        elem.setAttribute("ResponseCode", "SUCCESS");
        elem.setAttribute("ResponseDescription", "Email Sent Successfully");
        return doc ;
    }

}
