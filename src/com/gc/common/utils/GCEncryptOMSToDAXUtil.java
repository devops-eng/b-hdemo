/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
*	        OBJECTIVE: 
*#################################################################################################################################################################
*	        Version			   Date              Modified By             	Description
*#################################################################################################################################################################
             1.0               29/08/2014        Singh, Gurpreet            
*#################################################################################################################################################################
 */
package com.gc.common.utils;

import java.util.List;
import java.util.Properties;

import com.yantra.yfc.log.YFCLogCategory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.api.GCGetOrderListAndPostProcessing;
import com.gc.common.constants.GCConstants;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:gurpreet.singh@expicient.com">Gurpreet</a>
 */
public class GCEncryptOMSToDAXUtil implements YIFCustomApi{
    
    private static final YFCLogCategory LOGGER = YFCLogCategory
            .instance(GCGetOrderListAndPostProcessing.class);
    
    public Document encryptCCAndSVC(YFSEnvironment env, Document inDoc)
            throws GCException {
        LOGGER.verbose(" Entering GCEncryptOMSToDAXUtil.encryptCCAndSVC() method ");
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(" encryptCCAndSVC() method, Input document is"
                    + GCXMLUtil.getXMLString(inDoc));
        }
        try {
            List<Element> listPaymentMethod = GCXMLUtil.getElementListByXpath(
                    inDoc, "//PaymentMethod");
            // Encrypt/Decrypt CC, PLCC or GC payment method present in order.
            for (Element elePaymentMethod : listPaymentMethod) {
                String sPaymentType = elePaymentMethod
                        .getAttribute(GCConstants.PAYMENT_TYPE);
                if (GCConstants.CREDIT_CARD.equalsIgnoreCase(sPaymentType)
                        || GCConstants.PLCC.equalsIgnoreCase(sPaymentType)
                        || GCConstants.GIFT_CARD.equalsIgnoreCase(sPaymentType)
						|| GCConstants.PVT_LBL_CREDIT_CARD.equalsIgnoreCase(sPaymentType)) {
                    LOGGER.debug("Decrypting/Encrypting Svc no in case of any payment method");
                    String sSvcNo = elePaymentMethod
                            .getAttribute(GCConstants.SVC_NO);
                    if (!YFCCommon.isVoid(sSvcNo)) {
                        String sDecryptedSvcNo = GCPaymentSecurity
                                .decryptCreditCard(sSvcNo, "OMS");
                        String sEncryptedSvcNo = GCPaymentSecurity
                                .encryptCreditCard(sDecryptedSvcNo, "DAX");
                        elePaymentMethod.setAttribute(GCConstants.SVC_NO,
                                sEncryptedSvcNo);
                    }
                    if (sPaymentType.equalsIgnoreCase(GCConstants.CREDIT_CARD)
                            || sPaymentType.equalsIgnoreCase(GCConstants.PLCC)
							|| sPaymentType.equalsIgnoreCase(GCConstants.PVT_LBL_CREDIT_CARD)) {
                        LOGGER.debug("Decrypting/Encrypting CC No. no in case of CC or PLCC Payment Method");
                        String sCreditCardNo = elePaymentMethod
                                .getAttribute(GCConstants.CREDIT_CARD_NO);
                        if (!YFCCommon.isVoid(sCreditCardNo)) {
                            String sDecryptedCCNo = GCPaymentSecurity
                                    .decryptCreditCard(sCreditCardNo, "OMS");
                            String sEncryptedCCNo = GCPaymentSecurity
                                    .encryptCreditCard(sDecryptedCCNo, "DAX");
                            elePaymentMethod.setAttribute(
                                    GCConstants.CREDIT_CARD_NO, sEncryptedCCNo);
                        }
                    }
                }
            }
        }catch (Exception e){
            LOGGER.error("Inside catch block of encryptCCAndSVC method. Error is : ", e);
            throw new GCException(e);
        }
        return inDoc;
    }
    
    public void setProperties(Properties arg0){

    }
}
