/**
 * Copyright 2014 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  ####################################################################################################################################################
 *   OBJECTIVE: This class defines the exception handling
 *  ####################################################################################################################################################
 *        Version    Date          Modified By           CR/Bug                Description
 *  ####################################################################################################################################################
 *        1.0       28/08/2014     Chopra, Inder                             Initial Version exception util
 *
 * *  ###################################################################################################################################################
 */
package com.gc.common.utils;

import com.yantra.util.YFCUtils;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSException;
import com.yantra.yfs.japi.YFSUserExitException;

public class GCException extends Exception {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  public GCException(String message) {

    super(message);
  }

  public GCException(Exception ex) {

    super(ex);
  }

  public GCException(String message, Exception ex) {

    super(message, ex);
  }

  public YFSUserExitException getYFSUserExitException() {

    return new YFSUserExitException(this.getMessage());

  }

  public YFSException getYFSException() {

    return new YFSException(this.getMessage());

  }

  public static GCException getGCExpection(Exception e) {

    return new GCException(e);

  }

  public static GCException getGCExpection(String message, Exception e) {

    return new GCException(message, e);

  }

  // this is done because we don't want to change most of existing code.
  public static YFCException getYFCException(String message, Exception e) {

    if (e instanceof YFCException) {
      return (YFCException) e;
    }
    YFCException yfc = new YFCException(e);
    if (!YFCUtils.isVoid(message)) {
      yfc.setErrorDescription(message);
    }
    return yfc;

  }

}
