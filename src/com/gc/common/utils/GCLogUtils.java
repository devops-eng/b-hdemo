package com.gc.common.utils;

import com.yantra.yfc.log.YFCLogCategory;

public class GCLogUtils {

  private GCLogUtils(){

  }

  public static void verboseLog(YFCLogCategory logger, String message, Object obj){
    if(logger.isVerboseEnabled()){
      logger.verbose(message + obj);
    }
  }

  public static void verboseLog(YFCLogCategory logger, String message){
    if(logger.isVerboseEnabled()){
      logger.verbose(message );
    }
  }


}
