/**
 * Copyright © 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: Foundation Class
 *#################################################################################################################################################################
 *          Version       CR                Date              Modified By                Description
 *#################################################################################################################################################################
             1.0     Initial Version     12/18/2014         Shinde,Abhishek              Utility Class for Order Promise.
 *#################################################################################################################################################################
             1.1     Updated Version     12/18/2014         ZuzarInder,Singh             Added methods for Low-Value Items following signature @zuzarinder.singh
 *#################################################################################################################################################################
             1.2     Updated Version     02/26/2015         Sinha, Rajiv                 Added methods for stamping sourcing classification and fulfillment type
 *#################################################################################################################################################################
             1.3     Updated Version     01/10/2017         Mishra, Deepak               Added code for GCSTORE-6864.6865 - Low value items(CR)
 *#####################################################################################################################################################################################
             1.4     Updated Version     31/03/2017         Mishra, Deepak               Added code for GCSTORE-7165 - Restrict ship from store flag needs to be visible in Call Center
 *#####################################################################################################################################################################################
 
 */


package com.gc.common.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This is class is used for Utility methods in Order Promising.
 *
 * @author abhishek.shinde
 * @author zuzarinder.singh
 */

public class GCOrderUtil {
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCOrderUtil.class.getName());

  /**
   * stampProductClass method will stamp the product class Product class at PromiseLine based on the
   * value of ExtnSetCode
   *
   * @param elePromiseLine
   */


  private GCOrderUtil(){
  }

  /**
   * This method is used to stamp Sourcing classification. This value is stamped as "DC_SOURCED", if Address is PO/ APO / FPO or Country is other than USA
   * @param env
   * @param cOrdInDoc - Input document coming from ChangeOrder UE
   * @param orderEle - Output document of getOrderList call
   */
  public static void stampSourcingClassification(YFSEnvironment env, YFCDocument cOrdInDoc, YFCElement orderEle) {
    LOGGER.beginTimer("GCOrderUtil.stampSourcingClassification");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Incoming UE Doc" + cOrdInDoc.toString());
      LOGGER.verbose("Incoming orderdetails Doc" + orderEle.toString());
    }
    YFCElement cOrdUERootEle = cOrdInDoc.getDocumentElement();
    if (isDCSourcedAddresses(cOrdUERootEle, orderEle)||isDCEmployeeOrder(orderEle)) {
      cOrdUERootEle.setAttribute("SourcingClassification", GCConstants.DC_SOURCED_SOURCING);
    } else if (!YFCCommon.isVoid(orderEle)
        && YFCCommon.equals(orderEle.getAttribute("SourcingClassification"), GCConstants.DC_SOURCED_SOURCING, false)) {
      cOrdUERootEle.setAttribute("SourcingClassification", "");
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Updated UE Doc" + cOrdInDoc.toString());
    }
    LOGGER.endTimer("GCOrderUtil.stampSourcingClassification");
  }

  /**
   * This method checks if SourceCode is DCEMPLOYEE then returns true. else false
   * @param orderEle
   * @return
   */
  private static boolean isDCEmployeeOrder(YFCElement orderEle) {
    if (YFCCommon.isVoid(orderEle)) {
      return false;
    }
    YFCElement extEle = orderEle.getChildElement("Extn");
    if (!YFCCommon.isVoid(extEle) && YFCCommon.equals(extEle.getAttribute("ExtnSourceCode"), "DCEMPLOYEE", false)) {
      return true;
    }
    return false;
  }

  /** This method is used to stamp OrderLine attributes. These attributes include FulfillmentType, ShipNode as "MFI" and Inventory Check as "INFINV"
   * depending upon if request is for Store clearance, low value or serial item
   *
   * @param env
   * @param cOrdInDoc - Input document coming from ChangeOrder UE
   * @param orderEle - Output document of getOrderList call
   */
  public static void stampOrderLineAttributes(YFSEnvironment env,YFCDocument cOrdInDoc, YFCElement orderEle) {
    LOGGER.beginTimer("GCOrderUtil.stampOrderLineAttributes");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Incoming UE Doc" + cOrdInDoc.toString());
      LOGGER.verbose("Incoming orderdetails Doc" + orderEle.toString());
    }
    YFCElement cOrdUERootEle = cOrdInDoc.getDocumentElement();
    YFCElement cOrdUEExtnEle = cOrdUERootEle.getChildElement("Extn");
    String cOSourcingClass = cOrdUERootEle.getAttribute("SourcingClassification");
    boolean doNotSourceFromStore = false;
    String extnFullfilmentType = "";
    if(!YFCCommon.isVoid(cOrdUEExtnEle)){
      extnFullfilmentType = cOrdUEExtnEle.getAttribute("ExtnFulfillmentType");
    }

    if (YFCCommon.equals(cOSourcingClass, GCConstants.DC_SOURCED_SOURCING, false)
        || YFCCommon.equals(cOSourcingClass, GCConstants.PREPAID_SOURCING, false)) {
      doNotSourceFromStore = true;
    }

    if (!YFCCommon.isVoid(orderEle)) {
      // It is being called in Change Order as part of COM, means all orderlines exist in the system
      // and no orderline added or item modification happens through ChangeOrderUE while on fulfillment summary screen or while reaching to fulfillment summary screen
      Map <String, String>oLKeyOrderLineAttrMap = new HashMap<String, String>();
      YFCElement orderLinesEle = orderEle.getChildElement("OrderLines");
      YFCNodeList<YFCElement> orderLineList = orderLinesEle.getElementsByTagName("OrderLine");
      for (YFCElement orderLineEle: orderLineList) {

        // Fix for defect 1921 Start
        YFCElement bundleParentLineEle = orderLineEle.getChildElement("BundleParentLine");

        String parentOrderLineKey = "";
        if (!YFCCommon.isVoid(bundleParentLineEle)) {

          parentOrderLineKey = bundleParentLineEle.getAttribute("OrderLineKey");
          LOGGER.verbose("Found parentOrderLineKey="
              + parentOrderLineKey);
        }
        if (!YFCCommon.isVoid(parentOrderLineKey)) {
          LOGGER.verbose("Found parentOrderLineKey. It means it is bundle component and its orderline attribute should be as its parent. So skipping the determination of order attribute for this line");
          continue;
        }
        // Fix for defect 1921 End
        YFCElement orderLineExtnEle = orderLineEle.getChildElement("Extn");
        String ffType = extnFullfilmentType;
        String isCleranceItemRequested = "";
        String isCleranceItem = "";
        //Added for GCSTORE-6864/6865
        String restrictShipfromStore = "";
        String olKey = orderLineEle.getAttribute("OrderLineKey");
        if (YFCCommon.isVoid(ffType)) {
          ffType = orderLineEle.getAttribute("FulfillmentType");
        }
        if (!YFCCommon.isVoid(orderLineExtnEle)) {
          isCleranceItemRequested = orderLineExtnEle.getAttribute("ExtnIsStoreClearance");
        }
        YFCElement itemDtlsEle = orderLineEle.getChildElement("ItemDetails");
        YFCElement linePriceInfo = orderLineEle.getChildElement("LinePriceInfo");
        String itemPrice = linePriceInfo.getAttribute("UnitPrice");
        if(YFCCommon.isVoid(itemPrice)) {
          itemPrice = linePriceInfo.getAttribute("ListPrice");
        }
        boolean isStoreOnlyItem= isItemForParticularStoreOnly(orderLineEle);
        YFCElement itemExtnEle = itemDtlsEle.getChildElement("Extn");
        if (!YFCCommon.isVoid(itemExtnEle)) {
          isCleranceItem = itemExtnEle.getAttribute("ExtnIsClearanceItem");
          //Added for GCSTORE-6864/6865
          restrictShipfromStore = itemExtnEle.getAttribute(GCConstants.EXTN_RESTRICT_SHIP_FROM_STORE);
        }
        String fixNodeMFI = "";
        String storeForSingleSourceItem = "";
        String pickUpStoreShipNodeAtOrderLine = "";
        if (YFCCommon.equals(isCleranceItemRequested, "Y", false)) {
          if (YFCCommon.equals(ffType, GCConstants.SHIP_2_CUSTOMER, false)) {
            ffType = GCConstants.SHIP_2_CUSTOMER_STORE_CLEARANCE;
          } else if (YFCCommon.equals(ffType, GCConstants.PICKUP_IN_STORE, false)) {
            ffType = GCConstants.PICKUP_IN_STORE_STORE_CLEARANCE;
          }
        } else if (YFCCommon.equals(isCleranceItem, "Y", false)) {
          fixNodeMFI="Y";
          //Updated for GCSTORE-6864/6865
        } else if (/*isLowValueItem(env, orderEle.getAttribute(GCConstants.ENTERPRISE_CODE), itemPrice)*/
        		YFCCommon.equals(restrictShipfromStore, "Y", false)) {
          // "GC" is coming as EnterpriseCode in COM order
          // GCSTORE-7165--Start
          stampExtnRestrictShipFromStoreAtOrderLine(orderLineEle);
          // GCSTORE-7165--End
          if (YFCCommon.equals(ffType, GCConstants.SHIP_2_CUSTOMER, false)) {
            ffType = GCConstants.SHIP_2_CUSTOMER_LOW_VALUE;
          } else if (YFCCommon.equals(ffType, GCConstants.PICKUP_IN_STORE, false)) {
            ffType = GCConstants.PICKUP_IN_STORE_LOW_VALUE;
          }
        }
        YFCElement itemEle = orderLineEle.getChildElement(GCConstants.ITEM);
        String sItemID = itemEle.getAttribute("ItemID");
        String sUOM = itemEle.getAttribute("UnitOfMeasure");

        YFCDocument getItemLstTmpl =
            YFCDocument.getDocumentFor(GCXMLUtil.getDocument(GCConstants.GET_ITEM_LIST_TEMPLATE_ORDER_PROMISING));

        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("GetItemList Output Template XML::" + getItemLstTmpl.toString());
        }
        // call getItemList
        YFCDocument getItemListOp = GCCommonUtil.getItemList(env, sItemID, sUOM, GCConstants.GCI, true, getItemLstTmpl);
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("GetItemList Output XML::" + getItemListOp.toString());
        }
        boolean isKit = GCOrderUtil.isRegularKit(env, getItemListOp);
        if (isKit) {

          if (YFCCommon.equals(ffType, GCConstants.SHIP_2_CUSTOMER, false)) {
            ffType = GCConstants.SHIP_2_CUSTOMER_KIT;
          }
          if (YFCCommon.equals(ffType, GCConstants.PICKUP_IN_STORE, false)) {
            ffType = GCConstants.PICKUP_IN_STORE_KIT;
          }
          if (YFCCommon.equals(ffType, GCConstants.SHIP_2_CUSTOMER_ONHAND, false)) {
            ffType = GCConstants.SHIP_2_CUSTOMER_KIT_ONHAND;
          }
          if (YFCCommon.equals(ffType, GCConstants.PICKUP_IN_STORE_ONHAND, false)) {
            ffType = GCConstants.PICKUP_IN_STORE_KIT_ONHAND;
          }
        }


        if (isStoreOnlyItem) {
          storeForSingleSourceItem = orderLineEle.getAttribute(GCConstants.SHIP_NODE);
        } else {
          pickUpStoreShipNodeAtOrderLine = orderLineEle.getAttribute(GCConstants.SHIP_NODE);
        }
        oLKeyOrderLineAttrMap.put(olKey, ffType + ":" + fixNodeMFI + ":" + storeForSingleSourceItem + ":"
            + pickUpStoreShipNodeAtOrderLine);
      }

      // Fix for defect 1921 Start
      updateOrderLineAttrForComponents(orderLineList, oLKeyOrderLineAttrMap);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("Found order line attribute map as="
            + oLKeyOrderLineAttrMap);
      }
      // Fix for defect 1921 End
      // GCSTORE-1229::Begin
      YFCElement orderExtn = orderEle.getChildElement(GCConstants.EXTN);
      String extnPickUpStore = orderExtn.getAttribute(GCConstants.EXTN_PICK_UP_STORE_ID);
      updateChangeOrderUEDoc(cOrdUERootEle, oLKeyOrderLineAttrMap, doNotSourceFromStore, extnPickUpStore);

    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Outgoing UE Doc" + cOrdInDoc.toString());
    }
    LOGGER.endTimer("GCOrderUtil.stampOrderLineAttributes");
  }
//GCSTORE-7165--Start
public static void stampExtnRestrictShipFromStoreAtOrderLine(
		YFCElement orderLineEle) {
	YFCElement extnEle = orderLineEle.getChildElement(GCConstants.EXTN);
	extnEle.setAttribute(GCConstants.EXTN_RESTRICT_SHIP_FROM_STORE, GCConstants.YES);
	
}
//GCSTORE-7165--End

  // Fix for defect 1921 Start
  /**
   * This method iterates through each orderline which are a component and check in map if it has
   * order line attributes. If not then stamp the same as its parent.
   *
   * @param orderLineList
   * @param oLKeyOrderLineAttrMap
   */
  private static void updateOrderLineAttrForComponents(YFCNodeList<YFCElement> orderLineList,
      Map<String, String> oLKeyOrderLineAttrMap) {

    for (YFCElement orderLineEle : orderLineList) {
      String olKey = orderLineEle.getAttribute("OrderLineKey");

      YFCElement bundleParentLineEle = orderLineEle.getChildElement("BundleParentLine");
      String parentOrderLineKey = "";
      if (!YFCCommon.isVoid(bundleParentLineEle)) {
        parentOrderLineKey = bundleParentLineEle.getAttribute("OrderLineKey");
      }
      if (!YFCCommon.isVoid(parentOrderLineKey)) {
        String orderLineAttr = oLKeyOrderLineAttrMap.get(parentOrderLineKey);
        oLKeyOrderLineAttrMap.put(olKey, orderLineAttr);
      }
    }
  }

  // Fix for defect 1921 Ends

  /**
   * This methods update the FullfillmentType/ MFI as fixed node/ Infinite Inventory = "Y" on Change
   * Order UE doc
   *
   * @param cOrdUERootEle - Root element of Change Order UE doc
   * @param oLKeyOrderLineAttrMap - Map that stores the OrderLineKey and changes required for that
   *        OrderLine
   * @param doNotSourceFromStore
   */
  private static void updateChangeOrderUEDoc(YFCElement cOrdUERootEle,
      Map<String, String> oLKeyOrderLineAttrMap,
      boolean doNotSourceFromStore, String extnPickupStoreId) {
    YFCElement cOrdOrderLinesEle = cOrdUERootEle.getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> cOrdOrderLineList = cOrdOrderLinesEle.getElementsByTagName(GCConstants.ORDER_LINE);
    for (YFCElement cOrdOrderLineEle:cOrdOrderLineList ) {
      String orderLineKey = cOrdOrderLineEle.getAttribute(GCConstants.ORDER_LINE_KEY);
      if(oLKeyOrderLineAttrMap.containsKey(orderLineKey)){
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("OrderLine attribute found in change order for OrderLineKey="
              + orderLineKey
              + " as "
              + oLKeyOrderLineAttrMap.get(orderLineKey));
        }
        // GCSTORE-1229::Begin
        stampMapAttributes(cOrdOrderLineEle, oLKeyOrderLineAttrMap.get(orderLineKey), doNotSourceFromStore,
            extnPickupStoreId);
        // GCSTORE-1229::End
        oLKeyOrderLineAttrMap.remove(orderLineKey);
      }
    }
    Set <Entry <String,String>> orderLineEntrySet = oLKeyOrderLineAttrMap.entrySet();

    Iterator<Entry <String,String>> orderLineEntries = orderLineEntrySet.iterator();
    while (orderLineEntries.hasNext()) {
      Entry<String, String> orderLineEntry = orderLineEntries.next();
      String orderLineKey = orderLineEntry.getKey();
      String orderLineAttrValue = orderLineEntry.getValue();
      YFCElement cOrdOrderLineEle = cOrdOrderLinesEle.createChild(GCConstants.ORDER_LINE);
      cOrdOrderLineEle.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("OrderLine attribute found for OrderLineKey="
            + orderLineKey + " as " + orderLineAttrValue);
      }
      // GCSTORE-1229::Begin
      stampMapAttributes(cOrdOrderLineEle, orderLineAttrValue, doNotSourceFromStore, extnPickupStoreId);
      // GCSTORE-1229::End
    }

  }

  /**
   * Takes item details and return true if it can be fulfilled from store only
   * @param itemDtlsEle
   * @return
   */
  public static boolean isItemForParticularStoreOnly(YFCElement orderLineEle) {
    LOGGER.beginTimer("GCOrderUtil.isItemForParticularStoreOnly");
    YFCElement itemDtlsEle = orderLineEle.getChildElement("ItemDetails");
    boolean isSerialRequest = false;
    YFCElement orderLineInvAttReq = orderLineEle.getChildElement("OrderLineInvAttRequest");
    if (!YFCCommon.isVoid(orderLineInvAttReq)) {
      isSerialRequest = !YFCCommon.isVoid(orderLineInvAttReq.getAttribute("BatchNo"));
    }
    String kitCode = itemDtlsEle.getChildElement("PrimaryInformation").getAttribute("KitCode");
    if (isSerialRequest && YFCCommon.isVoid(kitCode)) {
      LOGGER.verbose("Found a serial request without kit code");
      LOGGER.endTimer("GCOrderUtil.isItemForParticularStoreOnly");
      return true;
    }
    LOGGER.endTimer("GCOrderUtil.isItemForParticularStoreOnly");
    return false;
  }

  /**
   * This service is used to stamp different OrderLine attributes i.e. FulfillmentType,
   * OrderLineSourcingCntrl, MFI as fixed not. These values are read from a map
   *
   * @param cOrdOrderLineEle
   * @param orderLineAttrValue
   * @param doNotSourceFromStore
   */
  private static void stampMapAttributes(YFCElement cOrdOrderLineEle,
      String orderLineAttrValue,
      boolean doNotSourceFromStore, String extnPickupStoreId) {
    LOGGER.beginTimer("GCOrderUtil.stampMapAttributes");
    orderLineAttrValue = orderLineAttrValue + ":" + "DummyValue";
    String[] orderLineAttr = orderLineAttrValue.split(":");
    String ffType = orderLineAttr[0];
    String fixedNodeMFI = orderLineAttr[1];
    String storeForSingleSourceItem = orderLineAttr[2];
    String shipNodeOfPickUpStore = orderLineAttr[3];

    if (!YFCCommon.isVoid(ffType)) {
      cOrdOrderLineEle.setAttribute("FulfillmentType", ffType);
    }
    // GCSTORE-1229::Begin
    // If it is Store only item, then it will have serial number and ShipNode stamped, then only stamp INFINV="Y"

    if ((!YFCCommon.isVoid(storeForSingleSourceItem) && !doNotSourceFromStore)
        || (ffType.indexOf(GCConstants.PICKUP_IN_STORE) >= 0 && YFCCommon.equals(extnPickupStoreId,
            shipNodeOfPickUpStore))) {
      // GCSTORE:1299:End
      // Commented for defect GCStore-1937 as "INFINV" was getting stamped before OrderConfirmation. Now its moved after OrderConfirmation.
      //GCStore-1937--End
    } else if (YFCCommon.equals("Y", fixedNodeMFI, false)) {
      // if not store only item, then check if it has to be fulfilled from MFI only.
      cOrdOrderLineEle.setAttribute("ShipNode", "MFI");
      cOrdOrderLineEle.setAttribute("IsFirmPredefinedNode","Y");
    }
    LOGGER.endTimer("GCOrderUtil.stampMapAttributes");
  }


  /**
   * This method checks if Order is DCSourced i.e. it has APO/ FPO/ PO Box addresses and Ship to
   * Country is not US
   *
   * @param cOrdUERootEle
   * @param orderEle
   * @return
   */
  public static boolean isDCSourcedAddresses(YFCElement cOrdUERootEle, YFCElement orderEle) {
    LOGGER.beginTimer("GCOrderUtil.isDCSourcedAddresses");

    // Checking Change Order has PersonInfoShipTo at Order level. If not then check PersonInfoShipTo
    // at Order level of getOrderDetails
    YFCElement cOrdPersonInfoShipToEle = cOrdUERootEle.getChildElement("PersonInfoShipTo");
    String isAPOFPO="";
    String isPO="";
    String country="";
    //First check in UE doc, if any of the required attributes are present as Y or N. If not then check the order's existing details
    if(!YFCCommon.isVoid(cOrdPersonInfoShipToEle)){
      YFCElement cOrdExtnEle = cOrdPersonInfoShipToEle.getChildElement("Extn");
      if(!YFCCommon.isVoid(cOrdExtnEle)){
        isAPOFPO = cOrdExtnEle.getAttribute("ExtnIsAPOFPO");
        isPO = cOrdExtnEle.getAttribute("ExtnIsPOBox");
      }
      country = cOrdPersonInfoShipToEle.getAttribute("Country");
    }
    if(!YFCCommon.isVoid(orderEle)){
      YFCElement personInfoShipToEle = orderEle.getChildElement("PersonInfoShipTo");

      YFCElement extnEle = personInfoShipToEle.getChildElement("Extn");
      if (!YFCCommon.isVoid(extnEle)) {
        if(YFCCommon.isVoid(isAPOFPO)){
          isAPOFPO = extnEle.getAttribute("ExtnIsAPOFPO");
        }
        if(YFCCommon.isVoid(isPO)){
          isPO = extnEle.getAttribute("ExtnIsPOBox");
        }
      }
      if (YFCCommon.isVoid(country)) {
        country = personInfoShipToEle.getAttribute("Country");
      }
    }

    if ((!YFCCommon.equals(country, GCConstants.US, false)) || YFCCommon.equals(isAPOFPO, "Y", false)
        || YFCCommon.equals(isPO, "Y", false)) {
      LOGGER.endTimer("GCOrderUtil.isDCSourcedAddresses");
      return true;
    }
    LOGGER.endTimer("GCOrderUtil.isDCSourcedAddresses");
    return false;

  }


  /**
   * stampFulfillmentTypeForClearance will stamp FulfillmentType at PromiseLine as 'SHIP_2_CUSTOMER_SC' or 'PICKUP_IN_STORE_SC'
   * based on the attribute value of ExtnIsStoreClearance
   *
   * @param elePromiseLine
   */

  public static boolean stampFulfillmentTypeForClearance(YFCElement elePromiseLine, boolean isClearanceItem,
      String headerLevelFulfillmentType) {
    LOGGER.beginTimer("GCOrderUtil.stampFulfillmentTypeForClearance");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input to stampFulfillmentTypeForClearance  : " + elePromiseLine.getString());
    }
    String fulfillmentType = elePromiseLine.getAttribute(GCConstants.FULFILLMENT_TYPE);
    if (YFCCommon.isVoid(fulfillmentType)) {
      fulfillmentType = headerLevelFulfillmentType;
    }
    YFCElement eleExtn = elePromiseLine.getChildElement(GCConstants.EXTN);
    if (!YFCCommon.isVoid(eleExtn)) {
      String sExtnIsStoreClearance = eleExtn.getAttribute(GCXmlLiterals.EXTN_IS_STORE_CLEARANCE);
      if (YFCCommon.equals(sExtnIsStoreClearance, GCConstants.YES, false)) {
        LOGGER.verbose("This is a clearance Item Request");
        if (YFCCommon.equals(fulfillmentType, GCConstants.SHIP_2_CUSTOMER, false)) {
          elePromiseLine.setAttribute(GCConstants.FULFILLMENT_TYPE, GCConstants.SHIP_2_CUSTOMER_STORE_CLEARANCE);
        } else {
          elePromiseLine.setAttribute(GCConstants.FULFILLMENT_TYPE, GCConstants.PICKUP_IN_STORE_STORE_CLEARANCE);
        }
        return true;
      } else if (isClearanceItem) {
        elePromiseLine.setAttribute("ShipNode", GCConstants.MFI);
        return true;
      }
    }
    LOGGER.endTimer("GCOrderUtil.stampFulfillmentTypeForClearance");
    return false;
  }


  /**
   * stampFulfilmentTypeForOnhand will stamp the FulfillmentType as 'SHIP_TO_CUST_ON_HAND' at
   * PromiseLine if FulfillentType is SHIP_TO_CUSTOMER at Promise level & ExtnIsOnHandOnly is "Y"
   * else value will be 'PICK_IN_STORE_ONHAND' if FulfillmentType is not SHIP_TO_CUSTOMER at Promise
   * Level
   *
   * @param elePromiseLine
   * @param sFulfillmentType
   */
  public static boolean stampFulfilmentTypeForOnhand(YFCElement elePromiseLine, String sFulfillmentType) {
    LOGGER.beginTimer("GCOrderUtil.stampFulfillmentTypeForOnHand");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input to stampFulfillmentTypeForOnHand " + elePromiseLine.getString());
    }

    YFCElement eleExtn = elePromiseLine.getChildElement(GCConstants.EXTN);
    if (!YFCCommon.isVoid(eleExtn)) {
      String sExtnIsOnHandOnly = eleExtn.getAttribute(GCXmlLiterals.EXTN_IS_ON_HAND_ONLY);
      if (YFCCommon.equals(GCConstants.YES, sExtnIsOnHandOnly, false)) {
        LOGGER.verbose("This is a OhandOnly Request");
        if (YFCCommon.equals(sFulfillmentType, GCConstants.SHIP_2_CUSTOMER, false)) {
          elePromiseLine.setAttribute(GCConstants.FULFILLMENT_TYPE, GCConstants.SHIP_2_CUSTOMER_ONHAND);
        } else {
          elePromiseLine.setAttribute(GCConstants.FULFILLMENT_TYPE, GCConstants.PICKUP_IN_STORE_ONHAND);
        }
        LOGGER.verbose("Input to stampFulfillmentTypeForOnHand with onhand flag=true");
        return true;
      }
    }
    LOGGER.verbose("Input to stampFulfillmentTypeForOnHand with onhand flag=false");
    LOGGER.endTimer("GCOrderUtil.stampFulfillmentTypeForOnHand");
    return false;
  }

  /**
   * This method fetches the ProductClass of Item.
   *
   * @param inDoc
   * @throws YFS Exception
   * @return
   */
  /**
   * This method is identifies if the Item incoming is Bundle set by checking its ProductClass and
   * KitCode.
   *
   * @param inDoc
   * @throws YFS Exception
   * @return
   */
  public static String getProductClass(YFCDocument inDoc) {
    LOGGER.beginTimer("GCOrderUtil.getProductClass");
    YFCNodeList<YFCElement> nlItemList = inDoc.getElementsByTagName(GCConstants.ITEM);
    int len = nlItemList.getLength();
    if (len == 0) {
      return GCConstants.BLANK;
    } else {
      YFCElement eleItem = nlItemList.item(0);
      YFCElement elePrimaryInformation = eleItem.getChildElement(GCXmlLiterals.PRIMARY_INFORMATION);
      YFCElement eleExtn = eleItem.getChildElement(GCConstants.EXTN);
      String sExtnSetCode = eleExtn.getAttribute(GCConstants.EXTN_SET_CODE);
      String sKitCode = elePrimaryInformation.getAttribute(GCXmlLiterals.KIT_CODE);
      if (YFCCommon.equals(sExtnSetCode, GCConstants.ONE, false)
          && YFCCommon.equals(sKitCode, GCConstants.BUNDLE, false)) {
        LOGGER.verbose("The given Item is BundleSet");
        LOGGER.endTimer("GCOrderUtil.getProductClass");
        return GCConstants.SET;
      } else {
        LOGGER.endTimer("GCOrderUtil.getProductClass");
        return GCConstants.REGULAR;
      }
    }
  }

  /**
   * This method validates if shipNode is passed for serialItem Request. If not it throws an error.
   *
   * @param promiseLineEle
   * @param errCode
   */
  public static void validateShipNodeForSerialItem(YFCElement promiseLineEle, String errCode) {
    LOGGER.beginTimer("validateShipNodeForSerialItem");
    YFCElement tagEle = promiseLineEle.getChildElement(GCXmlLiterals.TAG);
    if (!YFCCommon.isVoid(tagEle) && tagEle.hasAttribute(GCXmlLiterals.BATCH_NO)) {
      String batchNo = tagEle.getAttribute(GCXmlLiterals.BATCH_NO);
      if (!YFCCommon.isVoid(batchNo)) {
        String strShipNode= promiseLineEle.getAttribute(GCConstants.SHIP_NODE);
        if(!YFCCommon.isVoid(strShipNode)){
          return;
        }

        YFCElement shipNodesEle = promiseLineEle.getChildElement(GCXmlLiterals.SHIP_NODES);
        if (YFCCommon.isVoid(shipNodesEle) || YFCCommon.isVoid(shipNodesEle.getChildElement(GCXmlLiterals.SHIP_NODE))) {
          LOGGER.error("ShipNode Missing for serial Request");
          String errDesc = GCConstants.SHIPNODE_CANNOT_BE_BLANK_FOR_SERIAL_REQ;
          raiseException(errCode, errDesc, promiseLineEle.getAttribute(GCConstants.ITEM_ID));
        } else {
          YFCElement shipNode = shipNodesEle.getChildElement(GCConstants.SHIP_NODE);
          String shipNodeStr = shipNode.getAttribute(GCConstants.NODE);
          if (YFCCommon.isVoid(shipNodeStr)) {
            LOGGER.error("ShipNode Missing for serial Request");
            String errDesc = GCConstants.SHIPNODE_CANNOT_BE_BLANK_FOR_SERIAL_REQ;
            raiseException(errCode, errDesc, promiseLineEle.getAttribute(GCConstants.ITEM_ID));
          }
        }

      }
    }
    LOGGER.endTimer("validateShipNodeForSerialItem");
  }

  /**
   * This Method raises a YFSException with the given error code and Description.
   *
   * @param errCode
   * @param errDesc
   */
  public static void raiseException(String errCode, String errDesc, String itemId) {
    LOGGER.beginTimer("raiseException");
    YFCException excep = new YFCException(errCode);
    excep.setAttribute("Exception for ItemID", itemId);
    LOGGER.endTimer("raiseException");
    throw excep;

  }

  /**
   * method contains the logic for stamping ShipNodes for Low Value items if incoming in Promise Document.
   * author @zuzarinder.singh
   * @param env
   * @param elePromiseLine
   * @param getItemListoutDoc
   * @param inDoc
   */
  public static void stampShipNodeForLowValueItems(YFSEnvironment env, YFCElement elePromiseLine,
      YFCDocument getItemListOutDoc, YFCDocument inDoc) {
    LOGGER.beginTimer("stampShipNodeForLowValueItems");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Incoming XML Doc::PromiseLine Element:"+ elePromiseLine.toString()+"::Get Item List Output:"
          +getItemListOutDoc.toString()+"::Input Document as YFCDocument:"+inDoc.toString());
    }
    YFCElement elePromise = inDoc.getDocumentElement();
    String sFulfillmentType = elePromise.getAttribute(GCXmlLiterals.FULFILLMENT_TYPE);
    String sOrgCode = elePromise.getAttribute(GCXmlLiterals.ORGANIZATION_CODE);

    boolean bShipNodeVoid = isPromiseLineShipNodeVoid(elePromiseLine, inDoc);
    // check if ShipNode in PromiseLine is Blank
    if (bShipNodeVoid) {
      // check if item is Single Sourced by calling following method
      boolean bSingleSourced = isSingleSourced(getItemListOutDoc);
      if(bSingleSourced){
        return;
      }
      // check if item is GiftCard by calling following method
      boolean bGiftCard = isGiftCard(getItemListOutDoc);
      if(bGiftCard){
        return;
      }
      // check if item is DigitalItem by calling following method
      boolean bDigital = isDigital(getItemListOutDoc);
      if(bDigital){
        return;
      }
      
      //Added for GCSTORE-6864/6865
      boolean bIsLowValue = isLowValueItem(getItemListOutDoc);
      //Commented for GCSTORE-6864/6865
      //YFCElement elePromiseLineExtn = elePromiseLine.getChildElement(GCXmlLiterals.EXTN);
      //String sExtnUnitPrice = elePromiseLineExtn.getAttribute(GCXmlLiterals.EXTN_UNIT_PRICE);
      // call method to check if item is low value
      //boolean bIsLowValue = isLowValueItem(env, sOrgCode, sExtnUnitPrice);
      if (bIsLowValue) {
        YFCElement promiseLineShipNodesElement = elePromiseLine.getChildElement(GCXmlLiterals.SHIP_NODES, true);
        YFCElement promiseLineShipNodeElement = promiseLineShipNodesElement.getChildElement(GCXmlLiterals.SHIP_NODE, true);

        promiseLineShipNodeElement.setAttribute(GCXmlLiterals.NODE, GCConstants.MFI);
        if (YFCCommon.equals(sFulfillmentType, GCConstants.PICKUP_IN_STORE, false)){
          // check if input is findInventory request(ReservationParameters element absent in
          // findInventory input)
          YFCElement eleResParam = elePromise.getChildElement(GCXmlLiterals.RESERVATION_PARAMETERS);
          if (!YFCCommon.isVoid(eleResParam)) {
            YFCElement eleExtn = elePromise.getChildElement(GCXmlLiterals.EXTN);
            // fetch pick-up storeID and append it to ShipNodes element in promise line
            String sExtnPickupStoreID = eleExtn.getAttribute(GCXmlLiterals.EXTN_PICK_UP_STORE_ID);
            YFCElement eleShipNodeSecond = promiseLineShipNodesElement.createChild(GCXmlLiterals.SHIP_NODE);
            eleShipNodeSecond.setAttribute(GCXmlLiterals.NODE, sExtnPickupStoreID);
          }
        }
        // call method check for Drop-Ship item
        boolean bDropShip = isDropShip(getItemListOutDoc);
        if (bDropShip) {
          YFCNodeList<YFCElement> nlItem = getItemListOutDoc.getElementsByTagName(GCXmlLiterals.ITEM);
          YFCElement eleItem = nlItem.item(0);
          YFCElement elePrimaryInfo = eleItem.getChildElement(GCXmlLiterals.PRIMARY_INFORMATION);
          String sPrimarySupplier = elePrimaryInfo.getAttribute(GCXmlLiterals.PRIMARY_SUPPLIER);
          YFCElement eleShipNodeSecond = promiseLineShipNodesElement.createChild(GCXmlLiterals.SHIP_NODE);
          //stamp Primary Supplier in ShipNodes
          eleShipNodeSecond.setAttribute(GCXmlLiterals.NODE, sPrimarySupplier);
        }
      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Output of stampShipNodeForLowValueItems::New PromiseLine Element:"+ elePromiseLine.toString());
    }
    LOGGER.endTimer("stampShipNodeForLowValueItems");

  }

  /** Removed for GCSTORE-6864/6865
   * This method checks is the Item existing in PromiseLine is LowValue item by
   * comparing comparing GCCommon code with ExtnUnitPrice in Promise Input
   * author @zuzarinder.singh
   * @param env
   * @param sOrgCode
   * @param sExtnUnitPrice
   * @return
   */
/*  public static boolean isLowValueItem(YFSEnvironment env, String sOrgCode, String sExtnUnitPrice) {
    // Low value check by comparing GCCommon code with unit price in Promise Input
    // Fix to avoid exception when ExtnUnitPrice is not provided in the input start
    if(YFCCommon.isVoid(sExtnUnitPrice)) {
      return false;
    }
    // Fix to avoid exception when ExtnUnitPrice is not provided in the input end
    double dExtnUnitPrice = Double.parseDouble(sExtnUnitPrice);
    double dLowValueThreshold = getCommonCodeLowValueThreshold(env, sOrgCode);
    if (dLowValueThreshold > dExtnUnitPrice) {
      return true;
    }
    return false;
  }*/

  /**
   * This method fetches the configurable CodeValue from CommonCodeList stored as string and returns as a double:
   * InputPassed CodeType,OrganisationCode.
   * author @zuzarinder.singh
   * @param env
   * @param sOrgCode
   * @return
   */
  public static double getCommonCodeLowValueThreshold(YFSEnvironment env, String sOrgCode) {
    LOGGER.beginTimer("getCommonCodeLowValueThreshold");
    YFCDocument docGetCommonCodeIn = YFCDocument.createDocument(GCConstants.COMMON_CODE);
    YFCElement eleRoot = docGetCommonCodeIn.getDocumentElement();
    eleRoot.setAttribute(GCConstants.CODE_TYPE, GCConstants.GC_LOW_VALUE_THRESHOLD);
    eleRoot.setAttribute(GCConstants.ORGANIZATION_CODE, sOrgCode);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Incoming XML Doc" + docGetCommonCodeIn.toString());
    }
    // call API getCommonCodeList
    YFCDocument outDocument = GCCommonUtil.invokeAPI(env, GCConstants.GET_COMMON_CODE_LIST, docGetCommonCodeIn, YFCDocument.getDocumentFor(GCConstants.GET_COMMON_CODE_LIST_LOW_VALUE_TEMPELATE));
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Outtput XML Doc" + outDocument.toString());
    }
    YFCElement eleCommonCodeList=outDocument.getDocumentElement();
    YFCNodeList<YFCElement> nlCommonCode = eleCommonCodeList.getElementsByTagName(GCConstants.COMMON_CODE);
    YFCElement eleCommonCode = nlCommonCode.item(0);
    double dLowValueThreshold=0;
    String sLowValueThreshold="";
    if(!YFCCommon.isVoid(eleCommonCode)){
      // Fetching CommonCode CodeValue
      sLowValueThreshold = eleCommonCode.getAttribute(GCConstants.CODE_VALUE);
      dLowValueThreshold = Double.parseDouble(sLowValueThreshold);
    }
    LOGGER.endTimer("getCommonCodeLowValueThreshold");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Return Value:" + dLowValueThreshold);
    }
    return dLowValueThreshold;

  }
  /**This method checks if the Item is single sourced by checking ExtnIsUsedOrVintageItem Flag and returns true if flag is "Y".
   * author @zuzarinder.singh
   * @param getItemListoutDoc
   * @return
   */
  public static boolean isSingleSourced(YFCDocument getItemListoutDoc) {
    boolean bSingleSourced = false;
    YFCNodeList<YFCElement> nlItem = getItemListoutDoc.getElementsByTagName(GCXmlLiterals.ITEM);
    YFCElement eleItem = nlItem.item(0);
    if(YFCCommon.isVoid(eleItem)){
      return bSingleSourced;
    }
    YFCElement eleExtn = eleItem.getChildElement(GCXmlLiterals.EXTN);
    if(eleExtn != null){
      String sExtnIsUsedOrVintageItem = eleExtn.getAttribute(GCXmlLiterals.EXTN_IS_USED_OR_VINTAGE_ITEM);

      bSingleSourced = YFCCommon.equals(sExtnIsUsedOrVintageItem, GCConstants.FLAG_Y, false);
    }
    if (LOGGER.isVerboseEnabled()){
      LOGGER.verbose(" is item single soourced : " + bSingleSourced);
    }
    return bSingleSourced;
  }

  /**This method checks if the Item is DigitalItem by checking ItemType and returns true if it is "05".
   * author @zuzarinder.singh
   * @param getItemListoutDoc
   * @return
   */
  public static boolean isDigital(YFCDocument getItemListoutDoc) {
    boolean bDigital=false;
    YFCNodeList<YFCElement> nlItem = getItemListoutDoc.getElementsByTagName(GCXmlLiterals.ITEM);
    YFCElement eleItem = nlItem.item(0);
    if(YFCCommon.isVoid(eleItem)){
      return bDigital;
    }
    YFCElement elePrimaryInfo = eleItem.getChildElement(GCXmlLiterals.PRIMARY_INFORMATION);
    if(elePrimaryInfo != null){
      String sItemType = elePrimaryInfo.getAttribute(GCXmlLiterals.ITEM_TYPE);
      bDigital = YFCCommon.equals(sItemType, GCConstants.FIVE, false);
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" is item Digital : " + bDigital);
    }
    return bDigital;
  }


  /**
   * This method checks if the Item is GiftCard by checking ItemType and returns true if it is "04".
   * author @zuzarinder.singh
   * @param getItemListoutDoc
   * @return
   */
  public static boolean isGiftCard(YFCDocument getItemListoutDoc) {
    boolean bGiftCard=false;
    YFCNodeList<YFCElement> nlItem = getItemListoutDoc.getElementsByTagName(GCXmlLiterals.ITEM);
    YFCElement eleItem = nlItem.item(0);
    if(YFCCommon.isVoid(eleItem)){
      return bGiftCard;
    }
    YFCElement elePrimaryInfo = eleItem.getChildElement(GCXmlLiterals.PRIMARY_INFORMATION);
    if(elePrimaryInfo != null){
      String sItemType = elePrimaryInfo.getAttribute(GCXmlLiterals.ITEM_TYPE);
      if(YFCCommon.isVoid(sItemType)){
        bGiftCard = YFCCommon.equals(sItemType, GCConstants.FOUR, false);
      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" is item GiftCard : " + bGiftCard);
    }
    return bGiftCard;
  }

  /**
   * * This method checks if the Item is DropShip by checking ProductLine and returns true if it is "V1".
   * author @zuzarinder.singh
   * @param getItemListoutDoc
   * @return
   */
  public static boolean isDropShip(YFCDocument getItemListoutDoc) {
    boolean bDropShipItem=false;
    YFCNodeList<YFCElement> nlItem = getItemListoutDoc.getElementsByTagName(GCXmlLiterals.ITEM);
    YFCElement eleItem = nlItem.item(0);
    if(YFCCommon.isVoid(eleItem)){
      return bDropShipItem;
    }
    YFCElement elePrimaryInfo = eleItem.getChildElement(GCXmlLiterals.PRIMARY_INFORMATION);
    if(elePrimaryInfo != null){
      String sProductLine = elePrimaryInfo.getAttribute(GCXmlLiterals.PRODUCT_LINE);
      bDropShipItem = YFCCommon.equals(sProductLine, GCConstants.VERSION_ONE, false);
    }
    if (LOGGER.isVerboseEnabled()){
      LOGGER.verbose(" is item DropShip : " + bDropShipItem);
    }
    return bDropShipItem;

  }

  /**
   * This method checks if the ShipNode Node passed is blank or ShipNode element does not exist or ShipNodes element does not exist
   *  or ShipNode exists as attribute in PromiseLine Element and in all cases creating element ShipNode in element ShipNodes without setting the attribute.
   * author @zuzarinder.singh
   * @param elePromiseLine
   * @param inDoc
   * @return
   */
  public static boolean isPromiseLineShipNodeVoid(YFCElement elePromiseLine, YFCDocument inDoc) {
    //check if ShipNode exists as attribute in PromiseLine Element.
    String strShipNode= elePromiseLine.getAttribute(GCConstants.SHIP_NODE);
    if(!YFCCommon.isVoid(strShipNode)){
      return false;
    }
    //checks if the ShipNode Node passed is blank or ShipNode element does not exist or ShipNodes element does not exist
    YFCElement eleShipNodes = elePromiseLine.getChildElement(GCXmlLiterals.SHIP_NODES);
    if (YFCCommon.isVoid(eleShipNodes)){
      return true;
    }
    YFCElement eleShipNode = eleShipNodes.getChildElement(GCXmlLiterals.SHIP_NODE);
    if (!eleShipNodes.hasChildNodes()){
      return true;
    }
    if (YFCCommon.isVoid(eleShipNode)) {
      return true;
    }
    String sNode = eleShipNode.getAttribute(GCXmlLiterals.NODE);
    if (YFCCommon.isVoid(sNode)) {
      return true;
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Output"+eleShipNodes.toString()+ eleShipNode.toString()+sNode);
    }
    return false;
  }


  /**
   * This method contains the logic to stamp fulfillmentType of low value.
   * @param env
   * @param elePromiseLine
   * @param getItemListOutDoc
   * @param inDoc
   */
  public static void stampFulfillmentTypeForLowValueItems(YFSEnvironment env,
      YFCElement elePromiseLine, YFCDocument getItemListOutDoc,
      YFCDocument inDoc) {
    LOGGER.beginTimer("stampFulfillmentTypeForLowValueItems");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Incoming XML Doc::PromiseLine Element:"+ elePromiseLine.toString()+"::Get Item List Output:"
          +getItemListOutDoc.toString()+"::Input Document as YFCDocument:"+inDoc.toString());
    }
    YFCElement elePromise = inDoc.getDocumentElement();
    String sFulfillmentType = elePromise.getAttribute(GCXmlLiterals.FULFILLMENT_TYPE);
    String sOrgCode = elePromise.getAttribute(GCXmlLiterals.ORGANIZATION_CODE);

    boolean bShipNodeVoid = isPromiseLineShipNodeVoid(elePromiseLine, inDoc);
    // check if ShipNode in PromiseLine is Blank
    if (bShipNodeVoid) {
      // check if item is Single Sourced by calling following method
      boolean bSingleSourced = isSingleSourced(getItemListOutDoc);
      if(bSingleSourced){
        return;
      }
      // check if item is GiftCard by calling following method
      boolean bGiftCard = isGiftCard(getItemListOutDoc);
      if(bGiftCard){
        return;
      }
      // check if item is DigitalItem by calling following method
      boolean bDigital = isDigital(getItemListOutDoc);
      if(bDigital){
        return;
      }
      //Removed for GCSTORE-6864/6865
      //YFCElement elePromiseLineExtn = elePromiseLine.getChildElement(GCXmlLiterals.EXTN);
      //String sExtnUnitPrice = elePromiseLineExtn.getAttribute(GCXmlLiterals.EXTN_UNIT_PRICE);
      // call method to check if item is low value
      //boolean bIsLowValue = isLowValueItem(env, sOrgCode, sExtnUnitPrice);
      //Added for GCSTORE-6864/6865
      boolean bIsLowValue = isLowValueItem(getItemListOutDoc);
      if (bIsLowValue) {
        String promiseLineFulfillmentType = elePromiseLine.getAttribute(GCXmlLiterals.FULFILLMENT_TYPE);
        if(YFCCommon.isVoid(promiseLineFulfillmentType)){
          promiseLineFulfillmentType=sFulfillmentType;
        }
        if (YFCCommon.equals(promiseLineFulfillmentType, GCConstants.SHIP_2_CUSTOMER, false)) {
          promiseLineFulfillmentType = GCConstants.SHIP_2_CUSTOMER_LOW_VALUE;
        }
        if(YFCCommon.equals(promiseLineFulfillmentType,GCConstants.PICKUP_IN_STORE, false)){
          promiseLineFulfillmentType = GCConstants.PICKUP_IN_STORE_LOW_VALUE;
        }
        elePromiseLine.setAttribute(GCXmlLiterals.FULFILLMENT_TYPE,promiseLineFulfillmentType);
      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Output of stampFulfillmentTypeForLowValueItems::New PromiseLine Element:"+ elePromiseLine.toString());
    }
    LOGGER.endTimer("stampFulfillmentTypeForLowValueItems");
  }


  /**
   * This method returns true if item is a Clearance Item
   *
   * @param getItemListOp
   * @return
   */
  public static boolean isClearanceItem(YFCDocument getItemListOp) {
    LOGGER.beginTimer("isClearanceItem");
    YFCElement itemListEle = getItemListOp.getDocumentElement();
    YFCElement itemEle = itemListEle.getChildElement(GCConstants.ITEM);
    if(YFCCommon.isVoid(itemEle)) {
      return false;
    }
    YFCElement eleExtn = itemEle.getChildElement(GCConstants.EXTN);
    if(!YFCCommon.isVoid(eleExtn)){
      String isClearanceItem = eleExtn.getAttribute("ExtnIsClearanceItem");
      if(YFCCommon.equals(isClearanceItem, "Y", false)){
        LOGGER.endTimer("isClearanceItem");
        return true;
      }

    }
    LOGGER.endTimer("isClearanceItem");
    return false;
  }
  
  /**
   * This method returns true if item is a Low Value item Item
   * //Added for low value check - GCSTORE-6864/6865
   * @param getItemListOp
   * @return
   */
  public static boolean isLowValueItem(YFCDocument getItemListOp) {
    LOGGER.beginTimer("isLowValueItem");
    YFCElement itemListEle = getItemListOp.getDocumentElement();
    YFCElement itemEle = itemListEle.getChildElement(GCConstants.ITEM);
    if(YFCCommon.isVoid(itemEle)) {
      return false;
    }
    YFCElement eleExtn = itemEle.getChildElement(GCConstants.EXTN);
    if(!YFCCommon.isVoid(eleExtn)){
      String isLowValueItem = eleExtn.getAttribute(GCConstants.EXTN_RESTRICT_SHIP_FROM_STORE);
      if(YFCCommon.equals(isLowValueItem, "Y", false)){
        LOGGER.endTimer("isLowValueItem");
        return true;
      }

    }
    LOGGER.endTimer("isLowValueItem");
    return false;
  }


  /**
   * This method updates SourcingClassification="PREPAID", if any of order's payment method is
   * pre-paid. It is called ON Success event of confirmDraftOrder when Order created by Call Center
   * or changeOrder is called during amendment.
   *
   * @param env
   * @param orderEle - Element which has current order details
   * @param changeOrderDoc - document that will be used in changeOrder API call
   * @return
   */
  public static boolean updatePrepaidSourcing(YFSEnvironment env, YFCElement orderEle, YFCDocument changeOrderDoc) {
    LOGGER.beginTimer("updatePrepaidSourcing");

    YFCElement changeOrderRootEle = changeOrderDoc.getDocumentElement();
    YFCElement paymentMethodsEle = orderEle.getChildElement("PaymentMethods");
    String orderPurpose = orderEle.getAttribute(GCConstants.ORDER_PURPOSE);
    String currentSrcClassification = orderEle.getAttribute("SourcingClassification");
    boolean isPrePaidOrder = isPrePaidOrder(env, paymentMethodsEle, orderEle.getAttribute("EnterpriseCode"));
    // Since DC_SOURCED is more restrictive than PREPAID, not stamping PREPAID, if it already has
    // DC_SOURCED
    if (!YFCCommon.equals(currentSrcClassification, GCConstants.DC_SOURCED_SOURCING, false)) {
      if (isPrePaidOrder) {
        changeOrderRootEle.setAttribute("SourcingClassification", GCConstants.PREPAID_SOURCING);
        LOGGER.endTimer("updatePrepaidSourcing");
        return true;
      } else if (!YFCCommon.isStringVoid(currentSrcClassification)&&!YFCCommon.equalsIgnoreCase(orderPurpose, GCConstants.EXCHANGE)) {
        changeOrderRootEle.setAttribute("SourcingClassification", "");
        LOGGER.endTimer("updatePrepaidSourcing");
        return true;
      }
    }
    LOGGER.endTimer("updatePrepaidSourcing");
    return false;
  }


  /**
   * This method takes PaymentMethods and return true if any of the PaymentMethod is PREPAID Type
   *
   * @param paymentMethodsEle
   * @orgCode OrganizationCode/ EnterpriseCode of the Order - Required to call getCommonCodeList
   * @return
   */
  public static boolean isPrePaidOrder(YFSEnvironment env, YFCElement paymentMethodsEle, String orgCode) {
    if (!YFCCommon.isVoid(paymentMethodsEle)) {
      YFCNodeList<YFCElement> paymentMethodListEle = paymentMethodsEle.getElementsByTagName("PaymentMethod");
      List<String> prePaidPaymentList = new ArrayList<String>();

      Document commondCodeDoc = GCCommonUtil.getCommonCodeListByType(env, GCConstants.PREPAID_PAYMENT, orgCode);
      YFCDocument yfcCommonCodeDoc = YFCDocument.getDocumentFor(commondCodeDoc);
      YFCElement eleOutDocCommonCodeList = yfcCommonCodeDoc.getDocumentElement();
      YFCNodeList<YFCElement> prepaidList = eleOutDocCommonCodeList.getElementsByTagName(GCConstants.COMMON_CODE);

      for (YFCElement prePaidPayment : prepaidList) {
        String sCodeValue = prePaidPayment.getAttribute(GCConstants.CODE_VALUE);
        prePaidPaymentList.add(sCodeValue);
      }
      for (YFCElement paymentMethodEle : paymentMethodListEle) {
        String paymentType = paymentMethodEle.getAttribute("PaymentType");
        if (prePaidPaymentList.contains(paymentType)) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * This method takes the list of ShipNode and filters out ShipNode which is not allowed to be sourced
   * @param shipNodeList
   */
  public static List<String> getStoresAllowedForSourcing(YFSEnvironment env, List<String> shipNodeList) {
    if(shipNodeList.isEmpty()) {
      return shipNodeList;
    }
    YFCDocument inputDoc = YFCDocument.getDocumentFor(GCXMLUtil.getDocument(GCConstants.GET_ORG_LIST_INPUT_TO_FILTER_STORE));
    YFCElement rootEle =inputDoc.getDocumentElement();
    YFCElement complexQryEle = rootEle.getChildElement("ComplexQuery");
    YFCElement andEle = complexQryEle.getChildElement("And");
    YFCElement orEle = andEle.getChildElement("Or");
    for(int i=0;i<shipNodeList.size();i++){
      YFCElement expEle = orEle.createChild("Exp");
      expEle.setAttribute("Name", "OrganizationCode");
      expEle.setAttribute("QryType", "EQ");
      expEle.setAttribute("Value", shipNodeList.get(i));
    }
    YFCDocument templDoc = YFCDocument.getDocumentFor(GCXMLUtil.getDocument(GCConstants.GET_ORG_LIST_TEMPLATE_TO_FILTER_STORE));
    YFCDocument outDocument = GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORGANIZATION_LIST, inputDoc, templDoc);
    YFCElement orgListEle = outDocument.getDocumentElement();

    List<String> srcingAllowedNode =  new ArrayList<String>();
    YFCNodeList<YFCElement> organizationList = orgListEle.getElementsByTagName("Organization");
    for (YFCElement orgEle: organizationList) {
      srcingAllowedNode.add(orgEle.getAttribute("OrganizationCode"));
    }
    if(shipNodeList.contains(GCConstants.MFI) && !srcingAllowedNode.contains(GCConstants.MFI) ){
      srcingAllowedNode.add(GCConstants.MFI);
    }
    return srcingAllowedNode;
  }

  /**
   * This method takes the list of shipnode and returns thoses shipnodes for which sourcing is not allowed
   * @param env
   * @param shipNodeList
   * @return
   */
  public static List<String> getStoreNotAllowedForSourcing(YFSEnvironment env, List<String> shipNodeList){
    List<String> srcingAllowedNode = getStoresAllowedForSourcing(env, shipNodeList);
    shipNodeList.removeAll(srcingAllowedNode);
    return shipNodeList;
  }

  /**
   * This method checks if the ShipNode passed is blank or ShipNode element does not exist or
   * ShipNodes element does not exist or ShipNode exists as attribute for all the promiselines in
   * inDoc.
   *
   * @param elePromiseLine
   * @param inDoc
   * @return
   */
  public static boolean isShipToStoreScenario(YFSEnvironment env,YFCDocument inDoc, String sExtnPickStoreId) {
		YFCNodeList<YFCElement> nlPromiseLine = inDoc.getElementsByTagName(GCConstants.PROMISE_LINE);
		boolean isShipToStore = false;
		for (YFCElement elePromiseLine : nlPromiseLine) {
			boolean result = false;
			// check if ShipNode exists as attribute in PromiseLine Element.
			String sItemID=elePromiseLine.getAttribute(GCConstants.ITEM_ID);
			String sUOM = elePromiseLine.getAttribute(GCConstants.UNIT_OF_MEASURE);
			String strShipNode = elePromiseLine.getAttribute(GCConstants.SHIP_NODE);
			 YFCDocument getItemListOpTmpl = YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LIST_TEMPLATE_ORDER_PROMISING);
             YFCDocument getItemListoutDoc =
             GCCommonUtil.getItemList(env, sItemID, sUOM, GCConstants.GCI, true, getItemListOpTmpl);
             boolean isKit = GCOrderUtil.isRegularKit(env, getItemListoutDoc);

			if (!YFCCommon.isVoid(strShipNode)) {
				result = false;
				continue;
			}
			// checks if the ShipNode Node passed is blank or ShipNode element does not exist or ShipNodes
			// element does not exist
			YFCElement eleShipNodes = elePromiseLine.getChildElement(GCXmlLiterals.SHIP_NODES);
			if (YFCCommon.isVoid(eleShipNodes)) {
				result = true;
			} else {
				YFCElement shipNode = eleShipNodes
						.getChildElement(GCConstants.SHIP_NODE);
				if (YFCCommon.isVoid(shipNode)) {
					result = true;
				} else {
					String shipNodeStr = shipNode
							.getAttribute(GCConstants.NODE);
					if (YFCCommon.isVoid(shipNodeStr)) {
						result = true;
					}
				}
			}
			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("Output" + result);
			}
			if (result) {
				boolean isdigitalItem = isDigitalOrSoftWareItem(env, sItemID);
				LOGGER.verbose("IsDigitalItem" + isdigitalItem);
				if(!isdigitalItem &&  !isKit){
					elePromiseLine.setAttribute(GCXmlLiterals.SHIP_NODE, sExtnPickStoreId);
				}
			}

			isShipToStore = isShipToStore || result;
		}
		return isShipToStore;
	}


	//Fix for Defect-3703
	/** This method return true if Product line is "PV" else return false.
	 * 
	 * @param env
	 * @param sItemId
	 * @return
	 */
	public static boolean isDigitalOrSoftWareItem(YFSEnvironment env, String sItemID){

		YFCDocument inDoc=YFCDocument.createDocument("Item");
		YFCElement  eleIndocItem=inDoc.getDocumentElement();
		eleIndocItem.setAttribute(GCConstants.ITEM_ID, sItemID);
		
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("inDoc for GetItemList" + inDoc.toString());
		}

		YFCDocument outDocGetItemList=GCCommonUtil.invokeAPI(env,GCConstants.GET_ITEM_LIST, inDoc,YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LIST_TEMPLATE_ORDER_PROMISING));

		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Output of GetItemList" + outDocGetItemList.toString());
		}

		String sProductLine="";
		YFCElement eleItemList = outDocGetItemList.getDocumentElement();
		YFCElement eleItem=eleItemList.getChildElement(GCConstants.ITEM);
		if(!YFCCommon.isVoid(eleItem)){
			YFCElement elePrimaryInfo=eleItem.getChildElement(GCConstants.PRIMARY_INFORMATION);
			if(!YFCCommon.isVoid(elePrimaryInfo)){
				sProductLine=elePrimaryInfo.getAttribute("ProductLine");
			}
		}
		LOGGER.verbose("ProductLine is " + sProductLine);
		if(YFCCommon.equals(sProductLine, "PV")){
			return true;
		}else{
			return false;	
		}
	}
	
	/**
   * This method return true if it is kit item else return false.
   * 
   * @param env
   * @param sItemId
   * @return
   */
  public static boolean isRegularKit(YFSEnvironment env, YFCDocument getItemListOp) {


    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("inDoc for GetItemList" + getItemListOp.toString());
    }
    String extnSetCode = "";
    String kitCode = "";
    YFCElement eleItemList = getItemListOp.getDocumentElement();
    YFCElement eleItem = eleItemList.getChildElement(GCConstants.ITEM);
    if (!YFCCommon.isVoid(eleItem)) {
      YFCElement elePrimaryInfo = eleItem.getChildElement(GCConstants.PRIMARY_INFORMATION);
      if (!YFCCommon.isVoid(elePrimaryInfo)) {
        kitCode = elePrimaryInfo.getAttribute(GCConstants.KIT_CODE);
      }

      YFCElement eleItemExtn = eleItem.getChildElement(GCConstants.EXTN);
      if (!YFCCommon.isVoid(eleItemExtn)) {
        extnSetCode = eleItemExtn.getAttribute(GCConstants.EXTN_SET_CODE);
      }
    }
    if (YFCUtils.equals(GCConstants.BUNDLE, kitCode)
        && (YFCCommon.isVoid(extnSetCode) || YFCUtils.equals(GCConstants.ZERO, extnSetCode))) {
      return true;
    } else {
      return false;
    }
  }
}

