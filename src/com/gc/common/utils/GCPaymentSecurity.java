/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE:This class is used in the case of providing security.
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               18/03/2014        Soni, Karan		This class does the encryption and decryption of the CC No.
 *#################################################################################################################################################################
 */
package com.gc.common.utils;

import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.PrivateKey;

import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Base64;

import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.core.YFSSystem;

/**
 * @author Soni Karan
 */
public class GCPaymentSecurity {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCPaymentSecurity.class);

  private GCPaymentSecurity() {
    
  }

  /**
   *
   * Method encrypt the Credit Card no
   *
   * @param strCreditCardNo
   * @return encrypted Credit Card No
   *
   */
  public static String encryptCreditCard(String strCreditCardNo,
      String strKeyUsed) throws GCException {
    try {
      LOGGER.verbose("Entering the encryptCreditCard method");
      KeyStore keystore1 = KeyStore
          .getInstance(KeyStore.getDefaultType());
      /* Information for certificate to be generated */
			String password = YFSSystem.getProperty("oms.encrypt.password");
			String alias = YFSSystem.getProperty("oms.encrypt.alias");
      /* Get certificate of public key */

      keystore1.load(GCPaymentSecurity.class
          .getResourceAsStream(YFSSystem.getProperty(strKeyUsed
              + "_ENCRYPTION_KEY_PATH")), password.toCharArray());
      java.security.cert.Certificate cert = keystore1
          .getCertificate(alias);
      Cipher cipher = Cipher.getInstance("RSA");
      cipher.init(Cipher.ENCRYPT_MODE, cert.getPublicKey());
      byte[] bytes = strCreditCardNo.getBytes();
      byte[] encryptedData = cipher.doFinal(bytes);
      String strEncryCCNo = Base64
          .encodeBase64String(encryptedData);
      LOGGER.debug("The encrypted string returned " + strEncryCCNo);
      LOGGER.verbose("Returning from encryptCreditCard method");
      return strEncryCCNo;
    } catch (Exception e) {
      LOGGER.error("Inside GCPaymentSecurity.encryptCreditCard():",e);
      throw new GCException(e);
    }
  }

  /**
   *
   * This method Decyrpt the encrypted credit card no.
   *
   * @param strCreditCardNo
   * @return Decrypted credit card no
   * @throws Exception
   *
   */
  public static String decryptCreditCard(String strCreditCardNo,
      String strKeyUsed) throws GCException {
    try {
      LOGGER.verbose("Entering the decryptCreditCard method");
      KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
      /* Information for certificate to be generated */
			String password = YFSSystem.getProperty("oms.decrypt.password");
			String alias = YFSSystem.getProperty("oms.decrypt.alias");
      /* getting the private key */
      keystore.load(GCPaymentSecurity.class.getResourceAsStream(YFSSystem
          .getProperty(strKeyUsed + "_DECRYPTION_KEY_PATH")), password
          .toCharArray());
      PrivateKey key = (PrivateKey) keystore.getKey(alias,
          password.toCharArray());
      Cipher cipher = Cipher.getInstance("RSA");
      cipher.init(Cipher.DECRYPT_MODE, key);
      byte[] descryptedData = cipher.doFinal(Base64
          .decodeBase64(strCreditCardNo));
      String strDecryptCCNo = new String(descryptedData);
      LOGGER.debug("The decrypted CC No is: " + strDecryptCCNo);
      LOGGER.verbose("Returning from decryptCreditCard method");
      return strDecryptCCNo;
    } catch (Exception e) {
      LOGGER.error("Inside GCPaymentSecurity.decryptCreditCard():",e);
      throw new GCException(e);
    }
  }

  /**
   *
   * This method do the SHA hashing of the CC No.
   *
   * @param strCCNo
   * @return strCCHashCode
   * @throws Exception
   *
   */
  public static String ccHashing(String strCCNo) throws GCException {
    try {
      MessageDigest md = MessageDigest.getInstance("SHA-256");
      md.update(strCCNo.getBytes());

      byte []byteData = md.digest();

      // convert the byte to hex format method 1
      StringBuilder strCCHashCode = new StringBuilder();
      for (int i = 0; i < byteData.length; i++) {
        strCCHashCode.append(Integer.toString(
            (byteData[i] & 0xff) + 0x100, 16).substring(1));
      }
      return strCCHashCode.toString();
    } catch (Exception e) {
      LOGGER.error("Inside GCPaymentSecurity.ccHashing():",e);
      throw new GCException(e);
    }
  }

  /**
   *
   * Description This method mask the CC No and displays only the last 4
   * digits.
   *
   * @param strCCNo
   * @return
   * @throws Exception
   *
   */
  public static String ccNoMasking(String strCreditCardNo) throws GCException {
    try {
      String strSubString = strCreditCardNo.substring(
          strCreditCardNo.length() - 4, strCreditCardNo.length());
      String strAstericks = "************";
      return strAstericks + strSubString;
    } catch (Exception e) {
      LOGGER.error("Inside GCPaymentSecurity.ccNoMasking():",e);
      throw new GCException(e);
    }
  }
}
