/**
 * Copyright 2014 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  ####################################################################################################################################################
 *   OBJECTIVE: This class defines the Payment Utils.
 *  ####################################################################################################################################################
 *        Version    Date          Modified By           CR/Bug                Description
 *  ####################################################################################################################################################
 *        1.0       14/02/2014     Soni, Karan                             Initial Version Payment utils.
 *        1.1       26/06/2014     Soni, Karan          OMS-2263           Check Balance” button is not provided to check the balance on a gift card.
 *        1.2       12/12/2014     Soni, Karan          OMS-4304           Accertify is missing Credit Card data from OMS orders
 *        1.3       22/12/2014     Soni, Karan          OMS-4396           CR-72: Display Refund information for CSR's for Returns
 *        1.4       16/01/2015     Yashu               GCSTORE-84         Paypal Re-authorization processing
 *        1.5       03/06/2015     Saxena,Ekansh       GCSTORE-2794       AJB Connection Information must be specifiable per node
 *        1.6		19/05/2017	   Kumar, Shashank	   GCSTORE-5068		  Changes added to send error message in case Web service invocation fails
          1.7       05/19/2017     Gosai, Pinky        GCSTORE-6851       Prod - Declined WEB orders not sending Decline Letters
          1.8		05/26/2017	   Kumar, Shashank     GCSTORE-5068       Changes added to truncate the AvailableAmount and CurrentBalance to 2 decimal
          																  coming in through the DAX response
 * *  ###################################################################################################################################################
 */

package com.gc.common.utils;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import AJBComm.CAFipay;
import AJBComm.CAFipayNetworkException;
import AJBComm.CAFipayTimeoutException;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.userexit.GCCreditCardUE;
import com.yantra.yfc.date.YTimestamp;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;
import com.yantra.yfs.japi.YFSExtnPaymentCollectionInputStruct;
import com.yantra.yfs.japi.YFSExtnPaymentCollectionOutputStruct;

public class GCPaymentUtils {

  /*
   * The class Name
   */
  private static final String CLASS_NAME = GCPaymentUtils.class.getName();

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(CLASS_NAME);

  /**
   *
   * Process Payment and make Payment Requests
   *
   * @param env
   * @param inStruct
   * @return docPaymentResponse
   * @throws Exception
   *
   */
  public static Document processPayment(YFSEnvironment env,
      YFSExtnPaymentCollectionInputStruct inStruct) throws GCException {
    Document docPaymentRequest = null;
    try {
      LOGGER.verbose("Entering the processPayment method");
      String strPaymentType = inStruct.paymentType;
      Document docInCommonCode = GCXMLUtil
          .getDocument("<CommonCode CodeType='"
              + GCConstants.PAYMENT_GATEWAYS_COMMON_CODE
              + "' OrganizationCode='" + inStruct.enterpriseCode
              + "'/>");
      Document docOutCommonCode = GCCommonUtil.invokeAPI(env,
          GCConstants.GET_COMMON_CODE_LIST, docInCommonCode);
      String strPaymentServ = GCXMLUtil.getAttributeFromXPath(
          docOutCommonCode, "CommonCodeList/CommonCode[@CodeValue='"
              + strPaymentType + "']/@CodeShortDescription");
      if (!YFCCommon.isVoid(strPaymentType)) {
        if (GCConstants.INVOKE_AJB_PAYMENT_SERVICE
            .equals(strPaymentServ)) {
          LOGGER.debug("Entering the if condition of Credit Card Group");
          docPaymentRequest = processAJBAuthInput(env, inStruct);
          LOGGER.debug("Exting the ifcondition of Credit Card Group");
        } else {
          LOGGER.debug("Entering the else condition for throwing Exception");
          YFSException ex = new YFSException();
          ex.setErrorDescription(GCErrorConstants.INVALID_PAYMENT_GROUP_CODE);
          ex.setErrorDescription(GCErrorConstants.INVALID_PAYMENT_GROUP_DESCRIPTION
              + strPaymentType);
          throw ex;
        }
        LOGGER.verbose("Exiting the processPayment method");
      }
    } catch (Exception e) {
      LOGGER.error("Inside GCPaymentUtils.processPayment():", e);
      throw new GCException(e);
    }
    return docPaymentRequest;
  }

  /**
   *
   * Creates the payment request for AJB payment Gateway posting the request
   * to AJB and getting response xml.
   *
   * @param inStruct
   * @return ccAuthOp
   *
   */
  public static Document processAJBAuthInput(YFSEnvironment env,
      YFSExtnPaymentCollectionInputStruct inStruct) throws GCException {
    Document ccAuthOp = null;
    try {
      LOGGER.debug("Entering the createAJBAuthInput method");
      String strPaymentType = inStruct.paymentType;
      Document docGetOrderList = GCCreditCardUE.getOrderListOp(env,
          inStruct);
      String strOrderCaptureChannel = GCXMLUtil.getAttributeFromXPath(
          docGetOrderList, "Order/Extn/@ExtnOrderCaptureChannel");
      Document docPaymentRequest = GCCommonUtil
          .getDocumentFromPath(GCConstants.GET_AJB_PAYMENT_REQUEST_XML);
      Element eleAJBRoot = docPaymentRequest.getDocumentElement();
      Element eleCmd = (Element) XPathAPI.selectSingleNode(eleAJBRoot,
          GCConstants.IX_CMD);
      eleCmd.setTextContent(GCConstants.AJB_AUTH_REQUEST_CODE);
      Element eleTimeOut = (Element) XPathAPI.selectSingleNode(
          eleAJBRoot, GCConstants.IX_TIMEOUT);
      eleTimeOut.setTextContent(GCConstants.TIMED_OUT);
      Element eleDebitCredit = (Element) XPathAPI.selectSingleNode(
          eleAJBRoot, GCConstants.IX_DEBIT_CREDIT);
      if (GCConstants.GIFT_CARD.endsWith(strPaymentType)) {
        eleDebitCredit.setTextContent("GiftCard");
      } else {
        eleDebitCredit.setTextContent(GCConstants.CREDIT);
      }
      Element eleStoreNo = (Element) XPathAPI.selectSingleNode(
          eleAJBRoot, GCConstants.IX_STORE_NUMBER);
      if (GCConstants.CALL_CENTER_ORDER_CHANNEL
          .equals(strOrderCaptureChannel)) {
        eleStoreNo.setTextContent(YFSSystem
            .getProperty(GCConstants.CC_STORE_NUMBER));
      } else {
        eleStoreNo.setTextContent(YFSSystem
            .getProperty(GCConstants.WEB_STORE_NUMBER));
      }
      Element eleTerminalNo = (Element) XPathAPI.selectSingleNode(
          eleAJBRoot, GCConstants.IX_TERMINAL_NUMBER);
      eleTerminalNo.setTextContent(GCConstants.TERMINAL_NUMBER);
      Element eleTranType = (Element) XPathAPI.selectSingleNode(
          eleAJBRoot, GCConstants.IX_TRAN_TYPE);
      eleTranType.setTextContent(GCConstants.SALE);
      Element eleCardType = (Element) XPathAPI.selectSingleNode(
          eleAJBRoot, GCConstants.IX_CARD_TYPE);
      if (!GCConstants.GIFT_CARD.equals(strPaymentType)) {
        eleCardType.setTextContent(inStruct.creditCardType);
      }

      Element eleCreditCardNo = (Element) XPathAPI.selectSingleNode(
          eleAJBRoot, GCConstants.IX_ACCOUNT);
      if (GCConstants.GIFT_CARD.equals(strPaymentType)) {
        String strDecryptedGCNo = GCPaymentSecurity.decryptCreditCard(
            inStruct.svcNo, "OMS");
        eleCreditCardNo.setTextContent(strDecryptedGCNo);
      } else {
        String strDecryptedCCNo = GCPaymentSecurity.decryptCreditCard(
            inStruct.creditCardNo, "OMS");
        eleCreditCardNo.setTextContent(strDecryptedCCNo);
      }
      Element eleCCExpDate = (Element) XPathAPI.selectSingleNode(
          eleAJBRoot, GCConstants.IX_EXPDATE);
      String strCCExpDate = inStruct.creditCardExpirationDate;
      if (!YFCCommon.isVoid(strCCExpDate)) {
        String strYY = strCCExpDate.substring(
            strCCExpDate.length() - 2, strCCExpDate.length());
        String strMM = strCCExpDate.substring(0, 2);
        String strCCExp = strYY + strMM;
        eleCCExpDate.setTextContent(strCCExp);
      }

      //hardcode the expiry date for plcc v4
      if(GCConstants.PVT_LBL_CREDIT_CARD.equals(inStruct.paymentType)){

        eleCCExpDate.setTextContent("4912");
      }

      Element eleAmount = (Element) XPathAPI.selectSingleNode(eleAJBRoot,
          GCConstants.IX_AMOUNT);
      BigDecimal bdAuthAmt = GCCommonUtil
          .formatNumberForDecimalPrecision(inStruct.requestAmount, 2);
      String strAuthAmount = convertAuthAmount(bdAuthAmt);
      eleAmount.setTextContent(strAuthAmount);
      Element eleOptions = (Element) XPathAPI.selectSingleNode(
          eleAJBRoot, GCConstants.IX_OPTIONS);
      if (GCConstants.PRIVATE_LABEL_CARD.equals(inStruct.paymentType)) {
        eleOptions.setTextContent(GCConstants.PRIVATE_LABEL);
      }else if( GCConstants.PVT_LBL_CREDIT_CARD.equals(inStruct.paymentType)){
        eleOptions.setTextContent(GCConstants.PRIVATE_LABEL);
      }

      Element eleIssueNumber = (Element) XPathAPI.selectSingleNode(
          eleAJBRoot, GCConstants.IX_ISSUE_NUMBER);
      if (GCConstants.PRIVATE_LABEL_CARD.equals(inStruct.paymentType)) {

        // payment reference 9
        final StringBuilder stbPytRef9 = new StringBuilder(inStruct.paymentReference9);

        // check if the length is greater than 2
        if (stbPytRef9.length() >= 2) {

          // remove the second character
          stbPytRef9.deleteCharAt(1);

          eleIssueNumber.setTextContent(stbPytRef9.toString());
        }

        // eleIssueNumber.setTextContent(inStruct.paymentReference9);
      }else if(GCConstants.PVT_LBL_CREDIT_CARD.equals(inStruct.paymentType)){

        // left pad the number with zeros to make it 4 character
        final String strLeftPaddedPromoCode = StringUtils.leftPad(inStruct.paymentReference9, 4, '0');

        // right pad with space for space to make it 6 character
        final String strRightPaddedPromoCode = StringUtils.rightPad(strLeftPaddedPromoCode, 6, ' ');

        // set the promocode
        eleIssueNumber.setTextContent(strRightPaddedPromoCode);
      }
      Element eleMailOrderAVSData = (Element) XPathAPI.selectSingleNode(
          eleAJBRoot, GCConstants.IX_MAIL_ORDER_AVSDATA);
      if (!GCConstants.GIFT_CARD.equals(inStruct.paymentType)) {
        String strZipCode = inStruct.billToZipCode;
        String strFirstName = inStruct.billToFirstName;
        String strLastName = inStruct.billToLastName;
        String strBillToAddress = (inStruct.billToAddressLine1)
            .replaceAll(" ", "-");
        // Removing hyphen as AJB do not permit hyphen
        if (!YFCCommon.isVoid(strZipCode)) {
          strZipCode = strZipCode.replaceAll("-", "");
        }
        String strMailOrderAVSDAta = "*ECI *NAME_" + strFirstName + "_"
            + strLastName;
        String strCVV = null;
        // placed check to handle CVV from COM
        if (!YFCCommon.isVoid(inStruct.secureAuthenticationCode)) {
          strCVV = inStruct.secureAuthenticationCode;
          strMailOrderAVSDAta = strMailOrderAVSDAta + " *CVV2_"
              + strCVV;
        } else {
          strCVV = inStruct.svcNo;
          if (!YFCCommon.isVoid(strCVV)) {
            strCVV = GCPaymentSecurity.decryptCreditCard(
                inStruct.svcNo, "OMS");
            strMailOrderAVSDAta = strMailOrderAVSDAta + " *CVV2_"
                + strCVV;
          }
        }
        strMailOrderAVSDAta = strMailOrderAVSDAta + " *AVS_"
            + strZipCode + "_" + strBillToAddress;
        // Removing hyphen as AJB do not permit hyphen
        // OMS-4632 Start
        StringBuilder strProcessedData = new StringBuilder();
        for (int ij = 0; ij < strMailOrderAVSDAta.length(); ij++) {
          int c = strMailOrderAVSDAta.charAt(ij);
          if (c == 13 || c == 10) {
            break;
          }
          strProcessedData.append(strMailOrderAVSDAta.charAt(ij));
        }
        eleMailOrderAVSData.setTextContent(strProcessedData.toString());
        // OMS-4632 End
      }
      if (GCConstants.GIFT_CARD.equals(inStruct.paymentType)) {
        String strMailOrderAVSDAta = "*ECI";
        eleMailOrderAVSData.setTextContent(strMailOrderAVSDAta);

        Element elePinBlock = (Element) XPathAPI.selectSingleNode(
            eleAJBRoot, "IxPinBlock");
        elePinBlock.setTextContent(inStruct.paymentReference5);
      }
	
      //PLCC-Start

      //get the webservice connection details
      final Map<String,String> mapWSConnDtls =getWSConnDetails( env, getOrgCode(docGetOrderList), strOrderCaptureChannel,inStruct.paymentType );

      //PLCC-End
		//GCSTORE-5945 Start
      LOGGER.debug("GCSTORE-5945 Start");
      YFCDocument yfcIndoc = YFCDocument.createDocument("CommonCode");
      YFCElement eleRoot = yfcIndoc.getDocumentElement();
      eleRoot.setAttribute(GCConstants.CODE_TYPE, "GC_AVS_Eligible");

      YFCDocument yfcgetGetCommonCodeTemp =YFCDocument.
    		  getDocumentFor("<CommonCodeList> <CommonCode/> </CommonCodeList>");
      LOGGER.debug("Calling getCommonCodeList API :Input" +yfcIndoc.toString());
      YFCDocument yfcgetCommonCodeListOutput =
    		  GCCommonUtil.invokeAPI(env, GCConstants.GET_COMMON_CODE_LIST, yfcIndoc,yfcgetGetCommonCodeTemp);
      LOGGER.debug("getCommonCodeList API :Output" +yfcgetCommonCodeListOutput.toString());
      
      NodeList eleCommonCodeList = GCXMLUtil.getNodeListByXpath(yfcgetCommonCodeListOutput.getDocument(),
    		  "CommonCodeList/CommonCode");
      int length =eleCommonCodeList.getLength();
      boolean nonAVSCountry=false;
      String strCountry=inStruct.billToCountry;

      for(int k=0;k<length;k++)
      {
    	  Element eleCommonCode=(Element)eleCommonCodeList.item(k);
    	  String country = eleCommonCode.getAttribute("CodeShortDescription");
    	  if(YFCCommon.equalsIgnoreCase(country, strCountry)){
    		  LOGGER.debug("Its a non AVS country");
    		  nonAVSCountry=true;
    		  break;
    	  }
    	  

      }

      //Check if international card then remove the IxMailOrderAVSData element
      if(!nonAVSCountry)
      {
    	  //Removing existing element which contains data
    	  Element eleAVSData=(Element)eleAJBRoot.getElementsByTagName(GCConstants.IX_MAIL_ORDER_AVSDATA).item(0);
    	  eleAJBRoot.removeChild(eleAVSData);
    	  LOGGER.debug("Removing eleAVSData element");

      }
      LOGGER.debug("GCSTORE-5945 Stop");
    //GCSTORE-5945 End
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("The final xml made to sen to AJB"
            + GCXMLUtil.getXMLString(docPaymentRequest));
      }
      // Calling AJB method to post the request to AJB
      ccAuthOp = callAJB(docPaymentRequest, strOrderCaptureChannel, mapWSConnDtls);

      LOGGER.verbose("Exiting the createAJBAuthInput method");
    } catch (Exception e) {
      LOGGER.error("Inside GCPaymentUtils.processAJBAuthInput():", e);
      throw new GCException(e);
    }
    return ccAuthOp;
  }


  /**
   * <h3>Description:</h3>This method is used to get the web service connection details
   * @author Infosys Limited
   * @param env The YFS Environment
   * @param orgCode The organisation code
   * @param strOrderCaptureChannel The order channel
   * @param paymentType The payment type
   * @return The connection details
   */
  private static Map<String,String> getWSConnDetails(final YFSEnvironment env, final String strOrgCode, final String strOrdChannel, final String strPytType) {

    // logging the method entry
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASS_NAME + ":getWSConnDetails:entry:Input:OrgCode=" + strOrgCode + ":Channel=" + strOrdChannel
          + ":PaymentType=" + strPytType);
    }

    // prepare the input to get the webservice connection details
    final Document docGetPytWSDtlsIn = getWSConnDtls(strOrgCode, strOrdChannel, strPytType);

    // invoke the GetWSConnectionDtls_svc
    final Document docWSConnDtlsOut =
        GCCommonUtil.invokeService(env, GCConstants.WS_CONNECTION_DTLS_SVC, docGetPytWSDtlsIn);

    // prepare the connection details
    final Map<String, String> mapWSConnDtls = getWSConnDtlsFromDoc(docWSConnDtlsOut);

    // logging the method exit
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASS_NAME + ":getWSConnDetails:exit:Output=" + mapWSConnDtls);
    }

    // return the updated map
    return mapWSConnDtls;
  }


  /**
   * <h3>Description:</h3>This method is used to get the WebService coonection details
   *
   * @author Infosys Limited
   * @param docWSConnDtlsOut The WS connection details in xml
   * @return The WS connection details
   */
  private static Map<String, String> getWSConnDtlsFromDoc(final Document docWSConnDtlsOut) {

    // log the method input
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASS_NAME + ":getWSConnDtlsFromDoc:entry:Input=" + GCXMLUtil.getXMLString(docWSConnDtlsOut));
    }

    // fetch the details
    final NodeList nlCommonCodeAttr = docWSConnDtlsOut.getElementsByTagName(GCConstants.COMMON_CODE_ATTRIBUTE);

    // the map with the connection values
    final Map<String, String> mapWSConnDtls = new HashMap<String, String>();

    // iterate through each of the CommonCodeAttribute
    final int iCommonCodeAttrLen = nlCommonCodeAttr.getLength();
    for (int iCommonCodeAttrIdx = 0; iCommonCodeAttrIdx < iCommonCodeAttrLen; iCommonCodeAttrIdx++) {

      // fetch and update the details in map
      final Element eleCommonCodeAttr = (Element) nlCommonCodeAttr.item(iCommonCodeAttrIdx);
      final String strName = eleCommonCodeAttr.getAttribute(GCConstants.NAME);
      final String strValue = eleCommonCodeAttr.getAttribute(GCConstants.VALUE);

      // add to the return map
      mapWSConnDtls.put(strName, strValue);
    }

    // log the method output
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASS_NAME + ":getWSConnDtlsFromDoc:entry:Output=" + mapWSConnDtls);
    }

    // return the map
    return mapWSConnDtls;
  }


  /**
   * <h3>Description:</h3> This method is used to prepare the input the service to fetch the webservice conneciton details
   * @author Infosys Limited
   * @param strOrgCode The organization code
   * @param strOrdChannel The order channnel
   * @param strPytType The payment type
   * @return The input document for the service to get the ws connection details
   */
  private static Document getWSConnDtls(final String strOrgCode, final String strOrdChannel, final String strPytType) {

    // log the method entry
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASS_NAME + ":getWSConnDtls:entry");
    }

    // prepare the document
    final Document docCommonCodeLstIn = GCXMLUtil.createDocument(GCConstants.COMMON_CODE);
    final Element eleCommonCode = docCommonCodeLstIn.getDocumentElement();

    // set the input values
    eleCommonCode.setAttribute(GCConstants.ORGANIZATION_CODE, strOrgCode);
    eleCommonCode.setAttribute(GCConstants.CODE_SHORT_DESCRIPTION, strPytType);
    eleCommonCode.setAttribute(GCConstants.CODE_LONG_DESCRIPTION, strOrdChannel);

    // log the mehtod exit
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASS_NAME + ":getWSConnDtls:exit:Output=" + GCXMLUtil.getXMLString(docCommonCodeLstIn));
    }

    // return the prepared document
    return docCommonCodeLstIn;
  }


  /**
   * <h3>Description :</h3>This method is used to get the Organization Code for the input order
   * @author Infosys Limited
   * @param docGetOrderList The order xml
   * @return The organization code
   */
  private static String getOrgCode(final Document docGetOrderList) {

    // Logging the method entry
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASS_NAME + ":getOrgCode:entry" + GCXMLUtil.getXMLString(docGetOrderList));
    }

    // fetch the root
    final Element eleOrder = docGetOrderList.getDocumentElement();
    final String strOrgCode = eleOrder.getAttribute(GCConstants.SELLER_ORGANIZATION_CODE);

    // Logging the method exit
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASS_NAME + ":getOrgCode:exit:Output=" + strOrgCode);
    }

    // return the org code
    return strOrgCode;
  }


  /**
   *
   * This method calculates the Authorization Expiration date for the different payment types
   *
   * @return strAuthExpDate
   *
   */
  public static String getAuthExpiryDate(YFSEnvironment env,
      YFSExtnPaymentCollectionInputStruct inStruct) {
    try {
      LOGGER.debug("Entering the getAuthExpiryDate method");
      Date currentDate = new Date();
      String strOrderDate = new SimpleDateFormat(GCConstants.DATE_TIME_FORMAT).format(currentDate);
      Document inDocGetCommonCode = GCXMLUtil
          .getDocument("<CommonCode CodeType='GCAuthExpiryDays'/>");
      Document outDocGetCommonCode = GCCommonUtil.invokeAPI(env,
          GCConstants.GET_COMMON_CODE_LIST, inDocGetCommonCode);
      Element eleAuthExp = GCXMLUtil.getElementByXPath(
          outDocGetCommonCode,
          "//CommonCodeList/CommonCode[@CodeName='"
              + inStruct.paymentType
              + "' and @CodeLongDescription='"
              + inStruct.creditCardType + "']");
      String strAuthExpDate = "";
      if (!YFCCommon.isVoid(eleAuthExp)) {
        String strAuthValidationDate = eleAuthExp
            .getAttribute(GCConstants.CODE_SHORT_DESCRIPTION);
        LOGGER.debug("The authorization date" + strAuthValidationDate);
        SimpleDateFormat formatter = new SimpleDateFormat(
            GCConstants.DATE_TIME_FORMAT);
        Date dtOrderDate = formatter.parse(strOrderDate);
        Calendar calOrderDate = Calendar.getInstance();
        calOrderDate.setTime(dtOrderDate);
        calOrderDate.add(Calendar.DATE,
            Integer.parseInt(strAuthValidationDate));
        SimpleDateFormat dateFormat = new SimpleDateFormat(
            GCConstants.PAYMENT_DATE_TIME_FORMAT);
        strAuthExpDate = dateFormat.format(calOrderDate.getTime());
        LOGGER.debug("Exiting the getAuthExpiryDate method");
      }
      return strAuthExpDate;
    } catch (ParseException e) {
      LOGGER.error("ParseException exception thrown ", e);
      YFSException ex = new YFSException(e.getMessage());
      throw ex;
    }
  }

  /**
   *
   * This method receives input from Call Center and process that input xml
   *
   * @param env
   * @param inDoc
   * @return docPaymentResponse
   * @throws Exception
   *
   */
  public Document processPaymentWrapper(YFSEnvironment env, Document inDoc)
      throws GCException {
    try {
      LOGGER.verbose("Entering the processPaymentWrapper method");
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("processPaymentWrapper Input"
            + GCXMLUtil.getXMLString(inDoc));
      }
      YFSExtnPaymentCollectionInputStruct inStruct = new YFSExtnPaymentCollectionInputStruct();
      Element eRootElement = inDoc.getDocumentElement();

      // create instruct from xml
      if (!YFCCommon.isVoid(eRootElement
          .getAttribute(GCConstants.REQUEST_AMOUNT))) {
        inStruct.requestAmount = Double.parseDouble(eRootElement
            .getAttribute(GCConstants.REQUEST_AMOUNT));
      }
      inStruct.chargeType = eRootElement.getAttribute("chargeType");
      inStruct.paymentType = eRootElement.getAttribute("paymentType");
      inStruct.authorizationId = eRootElement
          .getAttribute("authorizationId");
      inStruct.orderNo = eRootElement.getAttribute("orderNo");
      inStruct.currency = eRootElement.getAttribute("currency");
      inStruct.creditCardNo = eRootElement.getAttribute("creditCardNo");
      inStruct.creditCardExpirationDate = eRootElement
          .getAttribute("creditCardExpirationDate");
      inStruct.creditCardName = eRootElement
          .getAttribute("creditCardName");
      inStruct.svcNo = eRootElement.getAttribute("svcNo");
      inStruct.paymentReference1 = eRootElement
          .getAttribute("paymentReference1");
      inStruct.paymentReference2 = eRootElement
          .getAttribute("paymentReference2");
      inStruct.paymentReference3 = eRootElement
          .getAttribute("paymentReference3");
      inStruct.paymentReference4 = eRootElement
          .getAttribute("paymentReference4");
      inStruct.paymentReference5 = eRootElement
          .getAttribute("paymentReference5");
      inStruct.paymentReference6 = eRootElement
          .getAttribute("paymentReference6");
      inStruct.customerAccountNo = eRootElement
          .getAttribute("customerAccountNo");
      inStruct.chargeTransactionKey = eRootElement
          .getAttribute("chargeTransactionKey");
      inStruct.orderHeaderKey = eRootElement
          .getAttribute("orderHeaderKey");
      inStruct.enterpriseCode = eRootElement
          .getAttribute("enterpriseCode");
      inStruct.creditCardType = eRootElement
          .getAttribute("creditCardType");
      inStruct.documentType = eRootElement.getAttribute("documentType");
      inStruct.firstName = eRootElement.getAttribute("firstName");
      inStruct.middleName = eRootElement.getAttribute("middleName");
      inStruct.lastName = eRootElement.getAttribute("lastName");
      if (!YFCCommon.isVoid(eRootElement
          .getAttribute("currentAuthorizationAmount"))) {
        inStruct.currentAuthorizationAmount = Double
            .parseDouble(eRootElement
                .getAttribute("currentAuthorizationAmount"));
      }
      if (YFCCommon.isVoid(eRootElement
          .getAttribute("currentAuthorizationExpirationDate"))) {
        inStruct.currentAuthorizationExpirationDate = GCDateUtils
            .convertDate(eRootElement
                .getAttribute("currentAuthorizationExpirationDate"));
      }
      inStruct.paymentKey = eRootElement.getAttribute("paymentKey");
      // Invoking method process payment
      Document docPaymentResponse = null;
      String strRequestType = eRootElement
          .getAttribute(GCConstants.REQUEST_TYPE);
      LOGGER.debug("The Request Type is:" + strRequestType);
      if (GCConstants.AUTH.equals(strRequestType)) {
        docPaymentResponse = processPayment(env, inStruct);
      } else if (GCConstants.BALANCE_CHECK.equals(strRequestType)) {
        docPaymentResponse = processPaymentBalanceCheck(env, inStruct);
      }
      LOGGER.debug("Exiting the processPaymentWrapper method");
      return docPaymentResponse;
    } catch (Exception e) {
      LOGGER.error("The exception is in processPaymentWrapper:", e);
      throw new GCException(e).getYFSException();
    }

  }

  /**
   *
   * This method do the dummy update of outStruct in the case of CHARGE.
   *
   * @param inStruct
 * @param outstructFromAuth // Added as a part of GCSTORE 1236 ; Pass null in normal charge scenario
   * @return outStruct
   * @throws Exception
   *
   */
  public static YFSExtnPaymentCollectionOutputStruct updateDummyOutstruct(
      YFSEnvironment env, YFSExtnPaymentCollectionInputStruct inStruct, YFSExtnPaymentCollectionOutputStruct outstructFromAuth)
          throws GCException {
    try {
      LOGGER.debug("Entering the updateDummyOutstruct method");
      YFSExtnPaymentCollectionOutputStruct outStruct = new YFSExtnPaymentCollectionOutputStruct();
      String strChargeType = inStruct.chargeType;
      if (GCConstants.PAYMENT_STATUS_CHARGE.equals(strChargeType)) {
        outStruct.authorizationAmount = inStruct.requestAmount;
        outStruct.authorizationId = inStruct.authorizationId;
        Date date = new Date();
        outStruct.executionDate = date;
        outStruct.collectionDate = date;
        outStruct.holdOrderAndRaiseEvent = false;
        outStruct.retryFlag = "N";
        outStruct.tranAmount = inStruct.requestAmount;
        outStruct.authReturnCode = "0";
        outStruct.authReturnMessage = "CHARGE_SUCCESS";
        outStruct.tranType = "CHARGE";
        outStruct.tranAmount = inStruct.requestAmount;
        outStruct.PaymentReference1 = inStruct.paymentReference1;
        // Updating the IxRefNumber in Auth code to be posted to DAX on
        // Invoice, fetching it from the Authorization for specific
        // Authorization Id
        Document getOrderListOpDoc = GCCreditCardUE.getOrderListOp(env,
            inStruct);
        if (GCConstants.CREDIT_CARD.equals(inStruct.paymentType)
            || GCConstants.PRIVATE_LABEL_CARD
            .equals(inStruct.paymentType) || GCConstants.PVT_LBL_CREDIT_CARD.equalsIgnoreCase(inStruct.paymentType)) {
        	String strIxRefNumber = null;
        	
        	// Changes wrt outstructFromAuth made for GCSTORE 1236
        	if(YFCCommon.isVoid(outstructFromAuth)){
		          strIxRefNumber = GCXMLUtil
		              .getAttributeFromXPath(
		                  getOrderListOpDoc,
		                  "Order/ChargeTransactionDetails/ChargeTransactionDetail[@ChargeType='AUTHORIZATION' and @AuthorizationID='"
		                      + inStruct.authorizationId
		                      + "']/CreditCardTransactions/CreditCardTransaction/@InternalReturnMessage");
        	}else{
        		strIxRefNumber=outstructFromAuth.internalReturnMessage;
        		LOGGER.debug("Inside GCPaymentUtils, outstructFromAuth is not void. strIxRefNumber :"+strIxRefNumber);
        	}
        	
          outStruct.internalReturnMessage = strIxRefNumber;
        }

        String strIxPS2000 = null;
        //Changes wrt outstructFromAuth made for GCSTORE 1236
        if (GCConstants.CREDIT_CARD.equals(inStruct.paymentType)) {
        	if(YFCCommon.isVoid(outstructFromAuth)){
		          strIxPS2000 =
		              GCXMLUtil
		              .getAttributeFromXPath(
		                  getOrderListOpDoc,
		                  "Order/ChargeTransactionDetails/ChargeTransactionDetail[@ChargeType='AUTHORIZATION' and @PaymentKey='"
		                      + inStruct.paymentKey + "' and @AuthorizationID='"
		                      + inStruct.authorizationId
		                      + "']/CreditCardTransactions/CreditCardTransaction/@RequestId");
		
		          if (YFCCommon.isVoid(strIxPS2000) && YFCCommon.isVoid(inStruct.authorizationId)) {
		            strIxPS2000 =
		                GCXMLUtil
		                .getAttributeFromXPath(
		                    getOrderListOpDoc,
		                    "Order/ChargeTransactionDetails/ChargeTransactionDetail[@ChargeType='AUTHORIZATION' and @AuthorizationID!='Declined' and @PaymentKey='"
		                        + inStruct.paymentKey + "']/CreditCardTransactions/CreditCardTransaction/@RequestId");
		          }
        	}
        	else{
        		strIxPS2000=outstructFromAuth.requestID;
        		LOGGER.debug("Inside GCPaymentUtils, outstructFromAuth is not void. strIxPS2000 :"+strIxPS2000);
        	}
          outStruct.requestID = strIxPS2000;
        }
      }
      LOGGER.debug("Returning from the updateDummyOutstruct method");
      return outStruct;
    } catch (Exception e) {
      LOGGER.error("Inside GCPaymentUtils.updateDummyOutstruct():", e);
      throw new GCException(e);
    }

  }

  /**
   *
   * This method convert the request amount into the form accepted by AJB
   *
   *
   */
  public static String convertAuthAmount(BigDecimal dRequestAmount)
      throws GCException {
    try {
      LOGGER.debug("Entering the convertAuthAmount method");
      String[] arrAuthAmt = String.valueOf(dRequestAmount).split("\\.");
      StringBuilder buff = new StringBuilder();
      String strAuthAmount = "";
      for (int i = 0; i < arrAuthAmt.length; i++) {
        buff.append(arrAuthAmt[i]);
      }
      strAuthAmount = buff.toString();
      LOGGER.debug("Returning from the convertAuthAmount method"
          + strAuthAmount);
      return strAuthAmount;
    } catch (Exception e) {
      LOGGER.error("Inside GCPaymentUtils.convertAuthAmount():", e);
      throw new GCException(e);
    }

  }

  /**
   *
   * This method converts the xml into csv string that is taken by AJB as
   * input
   *
   * @param docPaymentRequest
   * @return CSVTemplateString
   * @throws Exception
   *
   */
  public static String getXMLToCSV(Document docPaymentRequest)
      throws GCException {
    try {
      String strCSV = GCConstants.CSV_TEMPLATE_STRING;
      NodeList nlAuthEle = XPathAPI.selectNodeList(docPaymentRequest,
          "/XMLAJBFipayRequest/*");
      for (int i = 0; i < nlAuthEle.getLength(); i++) {
        Element eleRequest = (Element) nlAuthEle.item(i);
        strCSV = strCSV.replace("$#$" + eleRequest.getNodeName()
            + "$#$", eleRequest.getTextContent());
      }

      return strCSV.replaceAll("\\$\\#\\$(.*?)\\$\\#\\$", "");
    } catch (Exception e) {
      LOGGER.error("Inside GCPaymentUtils.getXMLToCSV():", e);
      throw new GCException(e);
    }

  }

  /**
   *
   * This method is used to convert the response from the AJB into the XML
   * from.
   *
   * @param strResponse
   * @return Document
   * @throws Exception
   *
   */
  public static Document getCSVToXML(String strResponse) throws GCException {
    try {
      String xmlpattern = "[^" + "\u0009\r\n" + "\u0020-\uD7FF"
          + "\uE000-\uFFFD" + "\ud800\udc00-\udbff\udfff" + "]";
      String strFinalResponse = strResponse.replaceAll(xmlpattern, "");
      LOGGER.debug("The final Response is" + strFinalResponse);
      String[] templateArray = GCConstants.CSV_TEMPLATE_STRING.split(",");
      String[] responseArray = strFinalResponse.split(",");
      int iTempLgth = templateArray.length;
      int iResponseLgth = responseArray.length;
      if (iTempLgth > iResponseLgth) {
        iTempLgth = iResponseLgth;
      }
      Document docRefund = GCXMLUtil
          .getDocument("<XMLAJBFipayResponse/>");
      Element eleRoot = docRefund.getDocumentElement();
      for (int i = 0; i < iTempLgth; i++) {
        String strElement = templateArray[i];
        strElement = strElement.replace("$#$", "");
        Element eleTemplate = docRefund.createElement(strElement);
        eleTemplate.setTextContent(responseArray[i]);
        eleRoot.appendChild(eleTemplate);
      }
      return docRefund;
    } catch (Exception e) {
      LOGGER.error("Inside GCPaymentUtils.getCSVToXML():", e);
      throw new GCException(e);
    }
  }

  /**
   *
   * This method convert the Authorized amt received from AJB into decimal
   * form.
   *
   *
   */
  public static String convertAuthReturnedAmount(String strRequestAmount) {
    try {
      LOGGER.debug("Entering the convertAuthReturnedAmount method");
      // OMS:4874 Start
      if (strRequestAmount.length() == 1) {
        LOGGER.debug("Appending 00 before amount if amount received from AJB is in single digit");
        strRequestAmount = "00".concat(strRequestAmount);
      }
      // OMS:4874 End
      String strSubSeq2 = strRequestAmount.substring(
          strRequestAmount.length() - 2, strRequestAmount.length());
      String strSubSeq1 = strRequestAmount.substring(0,
          strRequestAmount.length() - 2);
      strRequestAmount = strSubSeq1 + "." + strSubSeq2;
      LOGGER.debug("Returning from the convertAuthReturnedAmount method");
      return strRequestAmount;
    } catch (Exception e) {
      LOGGER.error(
          "Exception GCPaymentUtils.convertAuthReturnedAmount():", e);
      return "0.00";
    }

  }

  /**
   *
   * This method checks the payment gateway to invoke for balance check of
   * payment type and call the method for that payment gateway
   *
   * @param env
   * @param inStruct
   * @return docPaymentResponse
   * @throws Exception
   *
   */
  public static Document processPaymentBalanceCheck(YFSEnvironment env,
      YFSExtnPaymentCollectionInputStruct inStruct) throws GCException {
    try {
      LOGGER.debug("Entering processPaymentBalanceCheck method");
      Document docPaymentResponse = null;
      String strPaymentType = inStruct.paymentType;
      Document docInCommonCode = GCXMLUtil
          .getDocument("<CommonCode CodeType='"
              + GCConstants.PAYMENT_GATEWAYS_COMMON_CODE
              + "' OrganizationCode='" + inStruct.enterpriseCode
              + "'/>");
      Document docOutCommonCode = GCCommonUtil.invokeAPI(env,
          GCConstants.GET_COMMON_CODE_LIST, docInCommonCode);
      String strPaymentServ = GCXMLUtil.getAttributeFromXPath(
          docOutCommonCode,
          "//CommonCodeList/CommonCode[@CodeValue='" + strPaymentType
          + "']/@CodeShortDescription");
      if (GCConstants.INVOKE_AJB_PAYMENT_SERVICE.equals(strPaymentServ)) {
        LOGGER.debug("Entering the if condition of Credit Card Group");
        docPaymentResponse = processAJBBalanceCheckInput(env, inStruct);
        LOGGER.debug("Exting the ifcondition of Credit Card Group");
      } else if (GCConstants.INVOKE_DAX_PAYMENT_SERVICE
          .equals(strPaymentServ)) {
        LOGGER.debug("Entering the if condition of DAX Group");
        docPaymentResponse = processDAXBalanceCheckInput(env, inStruct);
        LOGGER.debug("Exting the ifcondition of DAX Group");
      }
      LOGGER.debug("Entering processPaymentBalanceCheck method");
      return docPaymentResponse;
    } catch (Exception e) {
      LOGGER.error("Inside GCPaymentUtils.processPaymentBalanceCheck():",
          e);
      throw new GCException(e);
    }
  }

  /**
   *
   * This method makes the input for balance check for AJB gateway and gets
   * the response back from AJB
   *
   * @param inStruct
   * @return ccAuthOp
   * @throws TransformerException
   * @throws GCException
   * @throws CAFipayTimeoutException
   * @throws CAFipayNetworkException
   * @throws Exception
   *
   */
  public static Document processAJBBalanceCheckInput(YFSEnvironment env,
      YFSExtnPaymentCollectionInputStruct inStruct)
          throws GCException, TransformerException, CAFipayTimeoutException, CAFipayNetworkException {
    LOGGER.debug("Entering the ProcessAJBBalanceCheckInput method");
    String strChargeType = inStruct.chargeType;
    Document ccAuthOp = null;
    if (GCConstants.PAYMENT_STATUS_AUTHORIZATION.equals(strChargeType)) {
      Document docPaymentRequest = GCCommonUtil
          .getDocumentFromPath(GCConstants.GET_AJB_PAYMENT_REQUEST_XML);
      Element eleAJBRoot = docPaymentRequest.getDocumentElement();
      Element eleCmd = (Element) XPathAPI.selectSingleNode(eleAJBRoot,
          GCConstants.IX_CMD);
      eleCmd.setTextContent(GCConstants.AJB_AUTH_REQUEST_CODE);
      Element eleTimeOut = (Element) XPathAPI.selectSingleNode(
          eleAJBRoot, GCConstants.IX_TIMEOUT);
      eleTimeOut.setTextContent(GCConstants.TIMED_OUT);
      Element eleDebitCredit = (Element) XPathAPI.selectSingleNode(
          eleAJBRoot, GCConstants.IX_DEBIT_CREDIT);
      eleDebitCredit.setTextContent(GCConstants.GIFTCARD);
      Element eleStoreNo = (Element) XPathAPI.selectSingleNode(
          eleAJBRoot, GCConstants.IX_STORE_NUMBER);
      eleStoreNo.setTextContent(YFSSystem
          .getProperty(GCConstants.CC_STORE_NUMBER));
      Element eleTerminalNo = (Element) XPathAPI.selectSingleNode(
          eleAJBRoot, GCConstants.IX_TERMINAL_NUMBER);
      eleTerminalNo.setTextContent(GCConstants.TERMINAL_NUMBER);
      Element eleTranType = (Element) XPathAPI.selectSingleNode(
          eleAJBRoot, GCConstants.IX_TRAN_TYPE);
      eleTranType.setTextContent(GCConstants.BALANCE_INQUIRY);
      Element eleCreditCardNo = (Element) XPathAPI.selectSingleNode(
          eleAJBRoot, GCConstants.IX_ACCOUNT);
      if (!YFCCommon.isVoid(inStruct.svcNo)) {
        String strDecryptedCCNo = GCPaymentSecurity.decryptCreditCard(
            inStruct.svcNo, "OMS");
        eleCreditCardNo.setTextContent(strDecryptedCCNo);
      }
      Element eleCCExpDate = (Element) XPathAPI.selectSingleNode(
          eleAJBRoot, GCConstants.IX_EXPDATE);
      eleCCExpDate.setTextContent(inStruct.creditCardExpirationDate);
      Element eleAmount = (Element) XPathAPI.selectSingleNode(eleAJBRoot,
          GCConstants.IX_AMOUNT);
      BigDecimal bdAuthAmt = GCCommonUtil
          .formatNumberForDecimalPrecision(inStruct.requestAmount, 2);
      String strAuthAmount = convertAuthAmount(bdAuthAmt);
      eleAmount.setTextContent(strAuthAmount);
      Element elePinBlock = (Element) XPathAPI.selectSingleNode(
          eleAJBRoot, "IxPinBlock");
      elePinBlock.setTextContent(inStruct.paymentReference5);
      // Converting the request xml formed into the csv
      String strAJBRequest = getXMLToCSV(docPaymentRequest);
      // Sending request to AJB
      String resp = "";
      boolean result = false;

      CAFipay aCafipayObj = new CAFipay(
          YFSSystem.getProperty(GCConstants.AJB_CC_NODE_IP),
          Integer.parseInt(YFSSystem.getProperty(GCConstants.CC_PORT_NO)),
          YFSSystem.getProperty(GCConstants.CC_NODE_ID), "*FIPAY",
          Integer.parseInt(YFSSystem
              .getProperty(GCConstants.RANDOM_NO)), true, false);
      try {
        result = aCafipayObj.SEND_MSGAPI(strAJBRequest);
        LOGGER.debug("The result of SENDMSGAPI:" + result);
      } catch (CAFipayTimeoutException toExc) {
        LOGGER.error(
            "Inside GCPaymentUtils.ProcessAJBBalanceCheckInput():",
            toExc);
        throw toExc;
      } catch (CAFipayNetworkException netExc) {
        LOGGER.error(
            "Inside GCPaymentUtils.ProcessAJBBalanceCheckInput():",
            netExc);
        throw netExc;
      }

      if (result) {
        Date dateStart, dateEnd;
        long start, end, timediff;
        dateStart = new Date();
        start = dateStart.getTime();
        timediff = 0;
        do {
          try {
            resp = aCafipayObj.RECV_MSGAPI();
            LOGGER.debug("The response from AJB:" + resp);
          } catch (CAFipayNetworkException netExc) {
            LOGGER.error(
                "Inside GCPaymentUtils.ProcessAJBBalanceCheckInput():",
                netExc);
            break;
          }

          if (resp.length() > 0 && resp.startsWith("10")) {
            break;
          }

          dateEnd = new Date();
          end = dateEnd.getTime();
          // time difference in secs
          timediff = (end - start) / 1000;

        } while (timediff < Integer.parseInt(YFSSystem
            .getProperty(GCConstants.TIME_OUT)));

        if (timediff >= Integer.parseInt(YFSSystem
            .getProperty(GCConstants.TIME_OUT))) {
          LOGGER.debug("Inside GCPaymentUtils.ProcessAJBBalanceCheckInput():: ERROR: Timed out, No Response received");
        }
      }
      // Closing the socket with AJB
      aCafipayObj.Disconnect();
      // Converting the response string into XML
      if (!YFCCommon.isVoid(resp)) {
        ccAuthOp = getCSVToXML(resp);
        Element eleResponseAmount = GCXMLUtil.getElementByXPath(
            ccAuthOp, "XMLAJBFipayResponse/IxAmount");
        String strAmt = eleResponseAmount.getTextContent();
        strAmt = convertAuthReturnedAmount(strAmt);
        eleResponseAmount.setTextContent(strAmt);
        String strProcessedData = processIxDepositData(resp);
        if (!YFCCommon.isVoid(strProcessedData)) {
          // processed data
          Element eleDepositData = GCXMLUtil.getElementByXPath(
              ccAuthOp, "XMLAJBFipayResponse/IxDepositData");
          strProcessedData = convertAuthReturnedAmount(strProcessedData);
          eleDepositData.setTextContent(strProcessedData.trim());
        }
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("The payment response document:"
              + GCXMLUtil.getXMLString(ccAuthOp));
        }
      }
      LOGGER.verbose("Exiting the ProcessAJBBalanceCheckInput method");
    }
    return ccAuthOp;
  }

  /**
   *
   * This method makes the DAX balance check input and invokes the web
   * service.
   *
   * @param inStruct
   * @return
   * @throws IOException
   * @throws SAXException
   * @throws ParserConfigurationException
   * @throws Exception
   *
   */
  public static Document processDAXBalanceCheckInput(YFSEnvironment env,
      YFSExtnPaymentCollectionInputStruct inStruct) throws GCException {
    try {
      LOGGER.debug("Entering the processDAXBalanceCheckInput method");
      Document daxAuthOp = null;
      String strOMSCustomerId = inStruct.paymentReference1;
      LOGGER.debug("The customer Id" + strOMSCustomerId);
      Document inDocDAX = GCCommonUtil
          .getDocumentFromPath(GCConstants.DAX_BALANCE_CHECK_INPUT_XML_PATH);
      Element eleSystemId = GCXMLUtil.getElementByXPath(inDocDAX,
          "Envelope/Body/GetOnAccountCreditSummary/systemId");
      eleSystemId.setTextContent(GCConstants.OMS);
      Element eleCustomerId = GCXMLUtil.getElementByXPath(inDocDAX,
          "Envelope/Body/GetOnAccountCreditSummary/customerId");
      eleCustomerId.setTextContent(strOMSCustomerId);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("The inDoc for web service call"
            + GCXMLUtil.getXMLString(inDocDAX));
      }
      GCWebServiceUtil obj = new GCWebServiceUtil();
      daxAuthOp = obj.invokeSOAPWebService(inDocDAX,
          YFSSystem.getProperty("DAX_WEB_SERVICE_URL"),
          YFSSystem.getProperty("DAX_SOAP_ACTION"));
      
      
      //GCSTORE-5068: Truncate the Available Balance and Available Credit to 2 decimal places: START
      Element rootEle = daxAuthOp.getDocumentElement();
      NodeList availableAmountNL = rootEle.getElementsByTagName("AvailableAmount");

      if(availableAmountNL.getLength() > 0){
    	  Element availableAmountEle = (Element)availableAmountNL.item(0);
          String availableAmtStr = truncateTo2DecimalPlaces(availableAmountEle);
          availableAmountEle.setTextContent(availableAmtStr);
          
          if (LOGGER.isDebugEnabled()) {
              LOGGER.debug("Available Amount:: " + availableAmtStr);
          }
      }

      NodeList currentBalanceNL = daxAuthOp.getElementsByTagName("CurrentBalance");

      if(currentBalanceNL.getLength() > 0){ 
    	  Element currentBalanceEle = (Element)currentBalanceNL.item(0);
          String currentBalanceStr = truncateTo2DecimalPlaces(currentBalanceEle);
          currentBalanceEle.setTextContent(currentBalanceStr);
          
          if (LOGGER.isDebugEnabled()) {
              LOGGER.debug("Current Balance:: " + currentBalanceStr);
          }
      }
      
      //GCSTORE-5068: Truncate the Available Balance and Available Credit to 2 decimal places: END
      if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("The response from DAX"
              + GCXMLUtil.getXMLString(daxAuthOp));
        }
      
      LOGGER.verbose("Exiting the processDAXBalanceCheckInput method");
      return daxAuthOp;
    } catch (Exception e) {
      //Added ErrorResponse attribute (for GCSTORE-5068) in case invocation of the Web service fails : Start
      String errorMsg = e.getMessage();
      String strExceptionxml = "<DAXBalanceCheck DAXResponse='SystemDown' ErrorResponse='" + errorMsg + "'/>";
      //GCSTORE-5068 :End
      Document docResponse = GCXMLUtil.getDocument(strExceptionxml);
      LOGGER.error(
          "The exception invoked in DAX balance check service: the exception is ",
          e);
      return docResponse;

    }
  }

  /**
   * Truncate to 2 decimal places
   * @param amountEle
   * @return
   */
  private static String truncateTo2DecimalPlaces(Element amountEle) {
	  String amountStr = amountEle.getTextContent();
	  int decimalIndex = amountStr.indexOf(".");
	  String decimalStr = amountStr.substring(decimalIndex+1);
	  
	  if(decimalIndex > 0 && decimalStr.length() >= 3){
		  amountStr = amountStr.substring(0, decimalIndex + 3);
	  }
	return amountStr;
  }



  /**
   *
   * This method invokes the getPaymentTypeList api and returns the output for
   * the new refund type as COA and Payment group OTHER.
   *
   * @param env
   * @param strSellerOrgCode
   * @return
   * @throws Exception
   *
   */
  public static Document invokeGetPaymentTypeList(YFSEnvironment env,
      String strSellerOrgCode) throws GCException {
    try {
      LOGGER.debug("Entering the invokeGetPaymentTypeList method");
      Document docGetPaymentTypeListIP = GCXMLUtil
          .createDocument(GCConstants.PAYMENT_TYPE);
      Element eleGetPaymentTypeList = docGetPaymentTypeListIP
          .getDocumentElement();
      eleGetPaymentTypeList.setAttribute(
          GCConstants.CALLING_ORGANIZATION_CODE, strSellerOrgCode);
      eleGetPaymentTypeList.setAttribute(
          GCConstants.NEW_REFUND_PAYMENT_TYPE,
          GCConstants.CREDIT_ON_ACCOUNT);
      eleGetPaymentTypeList.setAttribute(GCConstants.PAYMENT_TYPE_GROUP,
          GCConstants.OTHER);
      Document docGetPaymentTypeListOP = GCCommonUtil.invokeAPI(env,
          GCConstants.API_GET_PAYMENT_TYPE_LIST,
          docGetPaymentTypeListIP);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("The output doc for getPaymentTypeList is-->"
            + GCXMLUtil.getXMLString(docGetPaymentTypeListOP));
      }
      LOGGER.debug("Exiting the invokeGetPaymentTypeList method");
      return docGetPaymentTypeListOP;
    } catch (Exception e) {
      LOGGER.error("Inside GCPaymentUtils.invokeGetPaymentTypeList():", e);
      throw new GCException(e);
    }
  }

  /**
   *
   * This method checks whether thr exists a charge for this open newly opened
   * charge and that needs to be auth dummy or not
   *
   * @param env
   * @param chargeTransactionDetailList
   * @param paymentMethodEle
   * @return
   * @throws Exception
   *
   */
  public static List<Element> previousAuthAndChargeElements(
      YFSEnvironment env, List chargeTransactionDetailList,
      Element paymentMethodEle) throws GCException {
    Element authEle = null, chargeEle = null;
    try {
      LOGGER.debug("[GCCreditCardUE] :previousAuthAndChargeElements() ------ Start");
      Date currentDate = new Date();
      String ccnumberPayMethod = paymentMethodEle
          .getAttribute(GCConstants.CREDIT_CARD_NO);
      String paymentKeyPayMethod = paymentMethodEle
          .getAttribute(GCConstants.PAYMENT_KEY);
      if (chargeTransactionDetailList != null) {
        LOGGER.debug("Charge transaction details found: ");
        for (int eleCounter = 0; eleCounter < chargeTransactionDetailList
            .size(); eleCounter++) {
          Element chargeTransactionDetailEle = (Element) chargeTransactionDetailList
              .get(eleCounter);
          String chargeType = chargeTransactionDetailEle
              .getAttribute(GCConstants.CHARGE_TYPE);
          if (GCConstants.PAYMENT_STATUS_CHARGE
              .equalsIgnoreCase(chargeType)
              || GCConstants.PAYMENT_STATUS_AUTHORIZATION
              .equalsIgnoreCase(chargeType)) {
            String authorizationExpirationDate = chargeTransactionDetailEle
                .getAttribute(GCConstants.AUTH_EXPIRY_DATE);
            authorizationExpirationDate = authorizationExpirationDate
                .substring(0, 18);
            SimpleDateFormat sdf1 = new SimpleDateFormat(
                GCConstants.PP_TS_FORMAT);
            Date authExpiDate = sdf1
                .parse(authorizationExpirationDate);
            String status = chargeTransactionDetailEle
                .getAttribute(GCConstants.STATUS);

            String openAuthorizedAmount = chargeTransactionDetailEle
                .getAttribute(GCConstants.OPEN_AUTH_AMT);
            String paymentKeychargeTran = chargeTransactionDetailEle
                .getAttribute(GCConstants.PAYMENT_KEY);
            String requestAmount = chargeTransactionDetailEle
                .getAttribute(GCConstants.REQUEST_AMOUNT_PAYMENT);
            double openAuthAmt = 0.0, requestAmt = 0.0;
            if (!YFCCommon.isStringVoid(openAuthorizedAmount)) {
              openAuthAmt = Double
                  .parseDouble(openAuthorizedAmount);
            }
            if (!YFCCommon.isStringVoid(requestAmount)) {
              requestAmt = Double.parseDouble(requestAmount);
            }
            if (chargeType
                .equalsIgnoreCase(GCConstants.PAYMENT_STATUS_CHARGE)) {
              LOGGER.debug("Potential prev charge case");

              if (paymentKeyPayMethod
                  .equalsIgnoreCase(paymentKeychargeTran)
                  && (openAuthAmt < 0.0)
                  && (requestAmt > 0.0)) {
                LOGGER.debug("Valid previous charge scenario. Returning previous charge transaction");
                chargeEle = chargeTransactionDetailEle;
              }
            } else if (chargeType
                .equalsIgnoreCase(GCConstants.PAYMENT_STATUS_AUTHORIZATION)
                && authExpiDate.after(currentDate)
                && status.equalsIgnoreCase(GCConstants.CHECKED)) {
              LOGGER.debug("Potential dummy auth case");
              if (paymentKeyPayMethod
                  .equalsIgnoreCase(paymentKeychargeTran)
                  && (openAuthAmt > 0.0)) {
                LOGGER.debug("Previous auth amount greater than zero");
                Document paymentMethodOfChargeTranDoc = GCXMLUtil
                    .getDocumentFromElement(chargeTransactionDetailEle);
                Element paymentMethodOfChargeTranEle = GCXMLUtil
                    .getElementByXPath(
                        paymentMethodOfChargeTranDoc,
                        GCConstants.PAYMENT_METHPD_XPATH);
                String ccnumberOfChargeTran = paymentMethodOfChargeTranEle
                    .getAttribute(GCConstants.CREDIT_CARD_NO);
                String suspendAnyMoreChargesOfChargeTran = paymentMethodOfChargeTranEle
                    .getAttribute(GCConstants.SUS_ANY_MORE_CHARGES);
                if (suspendAnyMoreChargesOfChargeTran
                    .equalsIgnoreCase(GCConstants.NO)
                    && ccnumberPayMethod
                    .equalsIgnoreCase(ccnumberOfChargeTran)) {
                  LOGGER.debug("Dummy auth scenario. Returning previous auth charge transaction");
                  authEle = chargeTransactionDetailEle;
                }
              }
            }
          }
        }
      }
    } catch (Exception e) {
      LOGGER.error(
          "Inside GCPaymentUtils.previousAuthAndChargeElements():", e);
      throw new GCException(e);
    }
    LOGGER.debug("[GCCreditCardUE] :dummyAuthElement() ------ End");
    List<Element> prevChargeTransactionEle = new ArrayList<Element>();
    prevChargeTransactionEle.add(authEle);
    prevChargeTransactionEle.add(chargeEle);
    return prevChargeTransactionEle;
  }

  /**
   *
   * This method updated the outstruct with dummy auth values
   *
   * @param env
   * @param output
   * @param chargeTransactionDetailDummyaAuthEle
   * @param inStruct
   * @return
   * @throws Exception
   *
   */
  public static YFSExtnPaymentCollectionOutputStruct dummyAuthOutputStruct(
      YFSEnvironment env, YFSExtnPaymentCollectionOutputStruct output,
      Element chargeTransactionDetailDummyaAuthEle,
      YFSExtnPaymentCollectionInputStruct inStruct) throws GCException {

    try {
      LOGGER.verbose("[GCCreditCardUE] :dummyAuthOutputStruct() ------ Start");
      Document creditCardTransactionDoc = GCXMLUtil
          .getDocumentFromElement(chargeTransactionDetailDummyaAuthEle);
      Element creditCardTransactionEle = GCXMLUtil.getElementByXPath(
          creditCardTransactionDoc, GCConstants.CC_TRANSACTION_XPATH);
      output.authCode = creditCardTransactionEle
          .getAttribute(GCConstants.AUTH_CODE);
      output.authReturnCode = creditCardTransactionEle
          .getAttribute(GCConstants.AUTH_RETURN_CODE);
      output.authorizationExpirationDate = YTimestamp.HIGH_DATE
          .toString();
      output.authorizationId = chargeTransactionDetailDummyaAuthEle
          .getAttribute(GCConstants.AUTH_ID);
      output.authorizationAmount = inStruct.requestAmount;
      output.holdOrderAndRaiseEvent = false;
      output.holdReason = GCConstants.BLANK;
      output.internalReturnCode = GCConstants.DUMMY_AUTH_TYPE;
      output.internalReturnFlag = null;
      output.internalReturnMessage = GCConstants.DUMMY_AUTH_TYPE;
      output.retryFlag = GCConstants.NO;
      output.tranType = creditCardTransactionEle
          .getAttribute(GCConstants.TRAN_TYPE);
      output.tranAmount = inStruct.requestAmount;
    } catch (Exception e) {
      LOGGER.error("Inside GCPaymentUtils.dummyAuthOutputStruct():", e);
      throw new GCException(e);
    }
    LOGGER.verbose("[GCCreditCardUE] :dummyAuthOutputStruct() ------ End");
    return output;
  }

  /**
   *
   * Remove CVV after authorization is successful. 1# Suspend the payment type
   * 2# Remove CVV 3# Make payment method active.
   *
   * @param env
   * @param inDoc
   * @throws Exception
   *
   */
  public static void removeCVV(YFSEnvironment env, Document inDoc)
      throws GCException {
    try {
      LOGGER.verbose("Entering the removeCVV method");
      Element eleOrder = inDoc.getDocumentElement();
      String strOrderHeaderKey = eleOrder
          .getAttribute(GCConstants.ORDER_HEADER_KEY);
      Element elePaymentMethod = (Element) XPathAPI.selectSingleNode(
          eleOrder, "PaymentMethods/PaymentMethod");
      String strPaymentKey = elePaymentMethod
          .getAttribute(GCConstants.PAYMENT_KEY);
      Document outDoc = null;
      LOGGER.debug("The OrderHeaderKey is--->" + strOrderHeaderKey
          + "The Payment Key is" + strPaymentKey);
      Document inDocSuspendPayment = GCXMLUtil
          .getDocument("<Order OrderHeaderKey='"
              + strOrderHeaderKey
              + "' Override='Y'><PaymentMethods><PaymentMethod PaymentKey='"
              + strPaymentKey
              + "' SuspendAnyMoreCharges='B'/></PaymentMethods></Order>");
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("The doc to suspend payment method is"
            + GCXMLUtil.getXMLString(inDocSuspendPayment));
      }
      // invoke api to suspend payment method
      outDoc = GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER,
          inDocSuspendPayment);

      Element eleOutDoc = outDoc.getDocumentElement();
      if (GCConstants.ORDER.equalsIgnoreCase(eleOutDoc.getNodeName())) {
        inDocSuspendPayment = GCXMLUtil
            .getDocument("<Order OrderHeaderKey='"
                + strOrderHeaderKey
                + "' Override='Y'><PaymentMethods><PaymentMethod PaymentKey='"
                + strPaymentKey
                + "' SvcNo=''/></PaymentMethods></Order>");
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("The doc to remove cvv"
              + GCXMLUtil.getXMLString(inDocSuspendPayment));
        }
        // invoke change order to remove CVV
        outDoc = GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER,
            inDocSuspendPayment);
      }

      eleOutDoc = outDoc.getDocumentElement();
      if (GCConstants.ORDER.equalsIgnoreCase(eleOutDoc.getNodeName())) {
        inDocSuspendPayment = GCXMLUtil
            .getDocument("<Order OrderHeaderKey='"
                + strOrderHeaderKey
                + "' Override='Y'><PaymentMethods><PaymentMethod PaymentKey='"
                + strPaymentKey
                + "' SuspendAnyMoreCharges='N'/></PaymentMethods></Order>");
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("The doc to active payment method is"
              + GCXMLUtil.getXMLString(inDocSuspendPayment));
        }
        // change order to make payment method active
        outDoc = GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER,
            inDocSuspendPayment);
      }

      eleOutDoc = outDoc.getDocumentElement();
      if (GCConstants.ORDER.equalsIgnoreCase(eleOutDoc.getNodeName())) {
        inDocSuspendPayment = GCXMLUtil
            .getDocument("<Order OrderHeaderKey='"
                + strOrderHeaderKey + "' />");
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("The doc for requestCollection api is"
              + GCXMLUtil.getXMLString(inDocSuspendPayment));
        }
      }

      LOGGER.verbose("Exiting the removeCVV method");
    } catch (Exception e) {
      LOGGER.error(" Error while removing CVV number ", e);
      throw new GCException(e);
    }
  }

  /**
   * This method post the Acknowledgement request to AJB and gets the response
   *
   * @param paymentRequestDoc
   * @param strEntryType
   * @return PaymentResponseDoc
   */
  public static void acknowledgeAJB(String strAJBRequest, String strNodeId,
      CAFipay aCafipayObj) throws GCException {
    LOGGER.verbose("Entering the acknowledgeAJB method");
    // Sending request to AJB
    boolean result = false;
    LOGGER.debug("The request is " + strAJBRequest + "The node id is"
        + strNodeId);
    try {
      result = aCafipayObj.SEND_MSGAPI(strAJBRequest);
    } catch (CAFipayTimeoutException toExc) {
      LOGGER.error("Inside GCPaymentUtils.acknowledgeAJB():", toExc);
      throw new GCException("Timeout Exception", toExc);
    } catch (CAFipayNetworkException netExc) {
      LOGGER.error("Inside GCPaymentUtils.acknowledgeAJB():", netExc);
      throw new GCException("Network Exception", netExc);
    }
    if (result) {
      LOGGER.debug("The request is successfully Acknowledge");
    }
    LOGGER.verbose("Exiting the acknowledgeAJB method");
  }

  /**
   * This method post the request to AJB and gets the response
   *
   * @param paymentRequestDoc
   * @param strSellerOrg
   * @return PaymentResponseDoc
   */
  public static Document callAJB(Document paymentRequestDoc,
      String strOrderCaptureChannel, final Map<String, String> mapWSConnDtls) {
    Document ccAuthOp = null;
    try {
      LOGGER.verbose("Entering the callAJB method");
      // Converting the request xml formed into the csv
      String strAJBRequest = getXMLToCSV(paymentRequestDoc);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("The xml converted csv" + strAJBRequest);
      }
      // Sending request to AJB
      String resp = "";
      boolean result = false;

      //PLCC-Start
      final String strNodeId = StringUtils.isBlank(YFSSystem.getProperty(mapWSConnDtls.get(GCConstants.AJB_NODE_ID)))?mapWSConnDtls.get(GCConstants.AJB_NODE_ID):YFSSystem.getProperty(mapWSConnDtls.get(GCConstants.AJB_NODE_ID));
      // GCSTORE-2794 - Start

      final String sAJBIp = StringUtils.isBlank(YFSSystem.getProperty(mapWSConnDtls.get(GCConstants.AJB_NODE_IP)))?mapWSConnDtls.get(GCConstants.AJB_NODE_IP):YFSSystem.getProperty(mapWSConnDtls.get(GCConstants.AJB_NODE_IP));
      final String sPortNo = StringUtils.isBlank(YFSSystem.getProperty(mapWSConnDtls.get(GCConstants.AJB_PORT_NO)))?mapWSConnDtls.get(GCConstants.AJB_PORT_NO):YFSSystem.getProperty(mapWSConnDtls.get(GCConstants.AJB_PORT_NO));

      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(CLASS_NAME + ":callAJB:The connection details:NodeId=" + strNodeId + ":NodeIP=" + sAJBIp
            + ":PortNo=" + sPortNo);
      }
      /*if (GCConstants.CALL_CENTER_ORDER_CHANNEL
          .equals(strOrderCaptureChannel)) {
        strNodeId = YFSSystem.getProperty(GCConstants.CC_NODE_ID);
        sAJBIp = YFSSystem.getProperty(GCConstants.AJB_CC_NODE_IP);
        sPortNo = YFSSystem.getProperty(GCConstants.CC_PORT_NO);
      } else {
        strNodeId = YFSSystem.getProperty(GCConstants.WEB_NODE_ID);
        sAJBIp = YFSSystem.getProperty(GCConstants.AJB_WEB_NODE_IP);
        sPortNo = YFSSystem.getProperty(GCConstants.WEB_PORT_NO);
      }*/
      //PLCC End

      CAFipay aCafipayObj = new CAFipay(sAJBIp,
          Integer.parseInt(sPortNo),
          strNodeId, "*FIPAY", Integer.parseInt(YFSSystem
              .getProperty(GCConstants.RANDOM_NO)), true, false);
      // GCSTORE-2794 - End
      try {
        result = aCafipayObj.SEND_MSGAPI(strAJBRequest);
        // Delay the response by 5000ms Start
        //Date start = new Date();
        //Date end = new Date();
        //while (end.getTime() - start.getTime() < 1 * 100) {
        // end = new Date();
        //}
        Thread.sleep(100);

        // Delay the response by 5000ms End
      } catch (CAFipayTimeoutException toExc) {
        LOGGER.error("Inside GCPaymentUtils.acknowledgeAJB():", toExc);
        throw toExc;
      } catch (CAFipayNetworkException netExc) {
        LOGGER.error("Inside GCPaymentUtils.acknowledgeAJB():", netExc);
        throw netExc;
      }
      if (result) {
        Date dateStart, dateEnd;
        long start, end, timediff;
        dateStart = new Date();
        start = dateStart.getTime();
        timediff = 0;
        do {
          try {
            resp = aCafipayObj.RECV_MSGAPI();
          } catch (CAFipayNetworkException netExc) {
            LOGGER.error("Inside GCPaymentUtils.acknowledgeAJB():",
                netExc);
            break;
          }
          if (resp.length() > 0 && resp.startsWith("10")) {
            break;
          }

          dateEnd = new Date();
          end = dateEnd.getTime();
          // time difference in secs
          timediff = (end - start) / 1000;
        } while (timediff < Integer.parseInt(YFSSystem
            .getProperty(GCConstants.TIME_OUT)));

        if (timediff >= Integer.parseInt(YFSSystem
            .getProperty(GCConstants.TIME_OUT))) {
          LOGGER.debug("ERROR: Timed out, No Response received");
        }
      }
      // Converting the response string into XML
      if (!YFCCommon.isVoid(resp)) {
        ccAuthOp = getCSVToXML(resp);
        // Sending the Acknowledgment back to AJB in case of Gift card
        // Charging when IxOptions has value as DEBITACKREQ
        Element eleRoot = ccAuthOp.getDocumentElement();
        Element eleOptions = (Element) XPathAPI.selectSingleNode(
            eleRoot, GCConstants.IX_OPTIONS);
        String strOptions = eleOptions.getTextContent();
        Element eleDebitCredit = (Element) XPathAPI.selectSingleNode(
            eleRoot, GCConstants.IX_DEBIT_CREDIT);
        String strPaymentType = eleDebitCredit.getTextContent();
        Element eleActionCode = (Element) XPathAPI.selectSingleNode(
            eleRoot, GCConstants.IX_ACTION_CODE);
        String strAuthResponse = eleActionCode.getTextContent();
        if (GCConstants.GIFT_CARD_AJB.equalsIgnoreCase(strPaymentType)
            && strOptions.contains(GCConstants.DEBITACKREQ)
            && GCConstants.PAYMENT_AUTHORIZED
            .equals(strAuthResponse)) {
          LOGGER.debug("Entering the acknowledge Gift card response if");
          resp = resp.replaceFirst(String.valueOf(resp.charAt(0)),
              "9");
          GCPaymentUtils.acknowledgeAJB(resp, strNodeId, aCafipayObj);
          Thread.sleep(500);
        }
      } else {
        LOGGER.verbose("Blank response");
      }
      // disconnecting the connection
      aCafipayObj.Disconnect();
      LOGGER.verbose("Exiting the callAJB method");
    } catch (Exception e) {
      LOGGER.error("Inside GCPaymentUtils.callAJB():", e);
    }
    return ccAuthOp;
  }

  /**
   *
   * Description of processIxDepositData
   *
   * @param resp
   * @return
   *
   */
  public static String processIxDepositData(String resp) {
    LOGGER.verbose("Entering the processIxDepositData method");
    String[] responseArray = resp.split(",");
    StringBuilder strProcessedData = new StringBuilder();
    if (responseArray != null && responseArray.length > 51) {
      String strIxDepositDate = responseArray[51];
      for (int ij = 0; ij < strIxDepositDate.length(); ij++) {
        int c = strIxDepositDate.charAt(ij);
        if (c == 28) {
          break;
        }
        strProcessedData.append(strIxDepositDate.charAt(ij));
      }
    }
    LOGGER.debug("The strProcessedData amount is" + strProcessedData);
    LOGGER.verbose("Exiting the processIxDepositData method");
    return strProcessedData.toString();
  }

  //OMS-4304 Start
  /**
   * This method return the Bin no
   * @param strEncryptedCCNo
   * @return
   */
  public String getBinNumber(String strEncryptedCCNo) {
    String strBinNo="";
    try{
      if(!YFCCommon.isVoid(strEncryptedCCNo)){
        String strDecryptedCCNo = GCPaymentSecurity.decryptCreditCard(
            strEncryptedCCNo, "OMS");
        strBinNo= strDecryptedCCNo.substring(0, 6);
      }
    } catch(GCException ex){
      LOGGER.error("Exception in Bin number ", ex);
    }
    return strBinNo;
  }

  // OMS-4304 End


  //OMS-4396 Start
  /**
   * This method returns the refund payment method for return
   * @param env
   * @param inDoc
   * @return
   * @throws GCException
   */
  public static Document getReturnPaymentMethod(YFSEnvironment env, Document inDoc)throws GCException{
    Document docPaymentMethod= GCXMLUtil.getDocument("<ReturnOrder RefundType='OPM'><PaymentMethods/></ReturnOrder >");
    try{
      LOGGER.debug("Entering getReturnPaymentMethod method()");
      Document tempGOD = GCXMLUtil
          .getDocument("<Order><Extn/><PaymentMethods><PaymentMethod PaymentKey='' CreditCardType='' CreditCardExpDate='' CreditCardName='' CreditCardNo=''  DisplayCreditCardNo='' PaymentType='' RequestedRefundAmount='' /></PaymentMethods><OrderLines><OrderLine DerivedFromOrderHeaderKey='' /></OrderLines></Order>");
      Document outDocGOD= GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_DETAILS, inDoc, tempGOD);
      String strRefundOnAccount= GCXMLUtil.getAttributeFromXPath(outDocGOD, "Order/Extn/@ExtnIsRefundOnAccount");
      String strPaymentKey= GCXMLUtil.getAttributeFromXPath(outDocGOD, "Order/Extn/@ExtnRefundPaymentKey");
      //If refund is on credit on account
      if(GCConstants.YES.equals(strRefundOnAccount)){
        docPaymentMethod.getDocumentElement().setAttribute(GCConstants.REFUND_TYPE, GCConstants.COA);
      }else if(!YFCCommon.isVoid(strPaymentKey)){
        //If refund is on new credit card payment type
        String strSOOHK= GCXMLUtil.getAttributeFromXPath(outDocGOD, "Order/OrderLines/OrderLine/@DerivedFromOrderHeaderKey");
        Document inDocSOGOD= GCXMLUtil.getDocument("<Order OrderHeaderKey='"+strSOOHK+"'/>");
        Document outDocSOGOD= GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_DETAILS, inDocSOGOD, tempGOD);
        docPaymentMethod.getDocumentElement().setAttribute(GCConstants.REFUND_TYPE, GCConstants.NEW_REFUND_PAYMENT_METHOD);
        Element elePaymentMethod= GCXMLUtil.getElementByXPath(outDocSOGOD, "Order/PaymentMethods/PaymentMethod[@PaymentKey='"+strPaymentKey+"']");
        Element elePaymentMethodDest= docPaymentMethod.createElement(GCConstants.PAYMENT_METHOD);
        Element elePaymentMethods= GCXMLUtil.getElementByXPath(docPaymentMethod, "ReturnOrder/PaymentMethods");
        elePaymentMethods.appendChild(elePaymentMethodDest);
        GCXMLUtil.copyElement(docPaymentMethod, elePaymentMethod, elePaymentMethodDest);
      }
      LOGGER.debug("Exiting getReturnPaymentMethod method()");
    }catch(Exception e){
      LOGGER.error(
          " Inside catch of GCPaymentUtils.getReturnPaymentMethod()",
          e);
      throw new GCException(e);
    }
    return docPaymentMethod;
  }
  //OMS-4396 End


  // GCSTORE-85: Design - PayPal Re-Authorization Processing : Start
  /**
   *
   * This method is used to update the authorization retry count in GC_AUTH_RETRY table for each
   * failed authorization. Once the authorization count is reached above the maximum defined limit,
   * an alert is raised by calling raiseAlert method.
   *
   * @param env : YFSEnvironment variable
   * @param inStruct :inSruct containing the details of the payment type being processed.
   * @param getOrderListOpDoc : Details of the order for which payment processing is being done.
   * @throws GCException
   *
   */
  public static boolean updateAuthRetryCount(YFSEnvironment env, YFSExtnPaymentCollectionInputStruct inStruct,
      YFCDocument getOrderListOpDoc)  {

    LOGGER.beginTimer("GCPaymentUtils.updateAuthRetryCount");
    LOGGER.verbose("Class: GCPaymentUtils Method: updateAuthRetryCount BEGIN");
    String sPaymentKey = inStruct.paymentKey;

    // Fetching GCAuthRetryElement from getOrderListOutput corresponding to the payment key fetched from inStruct.
    YFCElement eleGCAuthRetry = null;
    YFCNodeList<YFCElement> nlOrderList = getOrderListOpDoc.getElementsByTagName(GCConstants.ORDER);
    YFCElement eleOrder = null;
    YFCElement eleOrderExtn = null;
    if(!YFCCommon.isVoid(nlOrderList)){
      eleOrder = nlOrderList.item(0);
      eleOrderExtn = eleOrder.getElementsByTagName(GCConstants.EXTN).item(0);
      YFCNodeList<YFCElement> nlGCAuthRetryList = eleOrderExtn.getElementsByTagName(GCConstants.GC_AUTH_RETRY_ELEMENT);
      for (YFCElement dummyGCAuthRetry : nlGCAuthRetryList) {
        if (YFCCommon.equals(sPaymentKey, dummyGCAuthRetry.getAttribute(GCConstants.PAYMENT_KEY), true)) {
          eleGCAuthRetry = dummyGCAuthRetry;
          break;
        }
      }
    }
    String sRetryCount = GCConstants.ZERO;
    YFCDocument docGCAuthRetryInXML = YFCDocument.createDocument(GCConstants.GC_AUTH_RETRY_ELEMENT);
    YFCElement eleGCAuthRetryRoot = docGCAuthRetryInXML.getDocumentElement();
    eleGCAuthRetryRoot.setAttribute(GCConstants.ORDER_HEADER_KEY, inStruct.orderHeaderKey);
    eleGCAuthRetryRoot.setAttribute(GCConstants.PAYMENT_KEY, inStruct.paymentKey);

    // if GCAuthRetry Element is not null, fetching AuthRetryCount from the element.
    if (!YFCCommon.isVoid(eleGCAuthRetry)) {
      LOGGER.verbose("Entering the If eleGCCCAuthRetry not void ");
      sRetryCount = eleGCAuthRetry.getAttribute(GCConstants.AUTH_RETRY_COUNT);
      LOGGER.verbose("The Retry Count is:" + sRetryCount);
    }

    // if GCAuthRetry Element is null, setting AuthRetryCount as 1 and invoking service GCCreateCCAuthRetry to make
    // the entry in GCAuthRetryTable.
    if (YFCCommon.isVoid(eleGCAuthRetry)) {
      eleGCAuthRetryRoot.setAttribute(GCConstants.AUTH_RETRY_COUNT,
          GCConstants.INT_ONE);
      GCCommonUtil.invokeService(env, GCConstants.GC_CREATE_CC_AUTH_RETRY, docGCAuthRetryInXML);
      // if GCAuthRetry Element is not null, and AuthRetryCount is less the maximum limit, then increasing the
      // AuthRetryCount.
    } else if (Integer.parseInt(GCConstants.MAX_RETRY_COUNT) > Integer.parseInt(sRetryCount)) {
      int iRetry = Integer.parseInt(sRetryCount) + 1;
      String sAuthRetryKey = eleGCAuthRetry.getAttribute(GCConstants.AUTH_RETRY_KEY);
      eleGCAuthRetryRoot.setAttribute(GCConstants.AUTH_RETRY_KEY, sAuthRetryKey);
      eleGCAuthRetryRoot.setAttribute(GCConstants.AUTH_RETRY_COUNT, String.valueOf(iRetry));
      GCCommonUtil.invokeService(env, GCConstants.GC_CHANGE_CC_AUTH_RETRY, docGCAuthRetryInXML);
      LOGGER.verbose("Setting new retry count to " + iRetry);
      // if GCAuthRetry Element is not null, and AuthRetryCount reached the maximum retry limit, then increasing the
      // AuthRetryCount and calling method raiseAlert to raise GCPaypalAuthFailed Alert.
    } else {
      int iRetry = Integer.parseInt(sRetryCount) + 1;
      String ccAuthRetryKey = eleGCAuthRetry.getAttribute(GCConstants.AUTH_RETRY_KEY);
      eleGCAuthRetryRoot.setAttribute(GCConstants.AUTH_RETRY_KEY, ccAuthRetryKey);
      eleGCAuthRetryRoot.setAttribute(GCConstants.AUTH_RETRY_COUNT, String.valueOf(iRetry));
      GCCommonUtil.invokeService(env, GCConstants.GC_CHANGE_CC_AUTH_RETRY, docGCAuthRetryInXML);
      String sPaymentType = inStruct.paymentType;
      if (YFCCommon.equals(
          Integer.parseInt(GCConstants.MAX_RETRY_COUNT),
          Integer.parseInt(sRetryCount))) {
        LOGGER.verbose("Raising Alert only when retry count is max retry count");
        raiseAlert(env, sPaymentType, inStruct.chargeType, inStruct.requestAmount, getOrderListOpDoc);
        return true;
      }
    }
    LOGGER.verbose("Class: GCPaymentUtils Method: updateAuthRetryCount END");
    LOGGER.endTimer("GCPaymentUtils.updateAuthRetryCount");
    return false;
  }

  /**
   *
   * This method reset the authorization retry count in GC_AUTH_RETRY table to zero, once the
   * authorization is done successfully for the given payment method.
   *
   * @param env : YFSEnvironment variable
   * @param inStruct :inSruct containing the details of the payment type being processed.
   * @param getOrderListOpDoc : Details of the order for which payment processing is being done.
   * @throws GCException
   *
   */
  public static void resetAuthRetryCount(YFSEnvironment env, YFSExtnPaymentCollectionInputStruct inStruct, YFCDocument getOrderListOPDoc) {

    LOGGER.beginTimer("GCPaymentUtils.resetAuthRetryCount");
    LOGGER.verbose("Class: GCPaymentUtils Method: resetAuthRetryCount START");
    String sPaymentKey = inStruct.paymentKey;

    // Fetching GCAuthRetry Element from getOrderListOutput corresponding to the payment key fetched from inStruct.
    YFCElement eleGCAuthRetry = null;
    YFCElement eleOrder = null;
    YFCElement eleOrderExtn = null;
    YFCNodeList<YFCElement> nlOrderList = getOrderListOPDoc.getElementsByTagName(GCConstants.ORDER);
    if (!YFCCommon.isVoid(nlOrderList)) {
      eleOrder = nlOrderList.item(0);
      eleOrderExtn = eleOrder.getElementsByTagName(GCConstants.EXTN).item(0);
      YFCNodeList<YFCElement> nlGCAuthRetryList = eleOrderExtn.getElementsByTagName(GCConstants.GC_AUTH_RETRY_ELEMENT);
      for (YFCElement dummyGCAuthRetry : nlGCAuthRetryList) {
        if (YFCCommon.equals(sPaymentKey, dummyGCAuthRetry.getAttribute(GCConstants.PAYMENT_KEY), true)) {
          eleGCAuthRetry = dummyGCAuthRetry;
          break;
        }
      }
    }
    // if GCAuthRetry Element is not null then resetting the value of AuthRetryCount to zero.
    if (!YFCCommon.isVoid(eleGCAuthRetry)) {
      YFCDocument docGCAuthRetryInXML = YFCDocument.createDocument(GCConstants.GC_AUTH_RETRY_ELEMENT);
      YFCElement eleGCAuthRetryRoot = docGCAuthRetryInXML.getDocumentElement();
      String sAuthRetryKey = eleGCAuthRetry.getAttribute(GCConstants.AUTH_RETRY_KEY);
      eleGCAuthRetryRoot.setAttribute(GCConstants.ORDER_HEADER_KEY, inStruct.orderHeaderKey);
      eleGCAuthRetryRoot.setAttribute(GCConstants.PAYMENT_KEY, inStruct.paymentKey);
      eleGCAuthRetryRoot.setAttribute(GCConstants.AUTH_RETRY_KEY, sAuthRetryKey);
      eleGCAuthRetryRoot.setAttribute(GCConstants.AUTH_RETRY_COUNT, GCConstants.ZERO);
      GCCommonUtil.invokeService(env, GCConstants.GC_CHANGE_CC_AUTH_RETRY, docGCAuthRetryInXML);

    }
    LOGGER.verbose("Class: GCPaymentUtils Method: resetAuthRetryCount END");
    LOGGER.endTimer("GCPaymentUtils.resetAuthRetryCount");
  }

  /**
   *
   * This method raise the alert once the authorization count exceeds the maximum retry limit.
   *
   * @param env : YFSEnvironment variable.
   * @param sPaymentType : Payment type being processed.
   * @param getOrderListOPDoc : Details of the order for which payment processing is being done.
   * @throws GCException
   *
   */
  private static void raiseAlert(YFSEnvironment env, String sPaymentType, String sChargeType, double dRequestAmount, YFCDocument getOrderListOPDoc) {
    LOGGER.beginTimer("GCPaymentUtils.raiseAlert");
    LOGGER.verbose("Class: GCPaymentUtils Method: raiseAlert START");
    if (YFCCommon.equals(sPaymentType, GCConstants.PAYPAL, true)) {
      if (YFCCommon.equals(sChargeType, GCConstants.PAYMENT_STATUS_AUTHORIZATION)) {
        GCCommonUtil.invokeService(env, GCConstants.PAYPAL_AUTH_ALERT_SERVICE, getOrderListOPDoc);
      } else if (YFCCommon.equals(sChargeType, GCConstants.PAYMENT_STATUS_CHARGE) && dRequestAmount > 0) {
        GCCommonUtil.invokeService(env, GCConstants.PAYPAL_CAPTURE_ALERT_SERVICE, getOrderListOPDoc);
      } else if (YFCCommon.equals(sChargeType, GCConstants.PAYMENT_STATUS_CHARGE) && dRequestAmount < 0) {
        GCCommonUtil.invokeService(env, GCConstants.PAYPAL_REFUND_ALERT_SERVICE, getOrderListOPDoc);
      }
    } else if(YFCCommon.equals(sPaymentType, GCConstants.BML, true)){
      LOGGER.verbose("Alert is invoked for BML payment type.");
      if (YFCCommon.equals(sChargeType, GCConstants.PAYMENT_STATUS_AUTHORIZATION)){
        GCCommonUtil.invokeService(env, GCConstants.BML_AUTH_ALERT_SERVICE, getOrderListOPDoc);
      }
    } else if (YFCCommon.equals(sPaymentType, GCConstants.CREDIT_ON_ACCOUNT)) {
      LOGGER.verbose("Alert is invoked for COA payment type.");
      if (YFCCommon.equals(sChargeType, GCConstants.PAYMENT_STATUS_AUTHORIZATION)) {
        GCCommonUtil.invokeService(env, GCConstants.COA_AUTH_FAILURE_ALERT_SERVICE, getOrderListOPDoc);
      }

    }
    LOGGER.verbose("Class: GCPaymentUtils Method: raiseAlert END");
    LOGGER.endTimer("GCPaymentUtils.raiseAlert");
  }

  /**
   *
   * This method is used to process Hard decline, suspending the payment type and raising alert.
   *
   * @param env : YFSEnvironment variable.
   * @param inStruct
   * @param outStruct : outStruct containing details for the suspended payment method.
   * @param getOrderListOPDoc : Details of the order for which payment processing is being done.
   * @return outStruct
   * @throws GCException
   *
   */
  public static YFSExtnPaymentCollectionOutputStruct processHardDeclinedResponse(YFSEnvironment env,
      YFSExtnPaymentCollectionInputStruct inStruct, YFSExtnPaymentCollectionOutputStruct outStruct,
      YFCDocument getOrderListOPDoc) {

    LOGGER.beginTimer("GCPaymentUtils.processHardDeclinedResponse");
    LOGGER.verbose("Class: GCPaymentUtils Method: processHardDeclinedResponse START");
    LOGGER.verbose("Entering IF Response is DECLINED");
    outStruct.authTime = GCConstants.BLANK;
    outStruct.authorizationId = GCConstants.DECLINED;
    outStruct.tranType = GCConstants.PAYMENT_STATUS_AUTHORIZATION;
    outStruct.authReturnMessage = GCConstants.DECLINED;
    outStruct.authorizationAmount = GCConstants.ZEROAMOUNT;
    outStruct.holdOrderAndRaiseEvent = GCConstants.FALSE;
    outStruct.suspendPayment = GCConstants.YES;
    outStruct.tranAmount = GCConstants.ZEROAMOUNT;
    outStruct.retryFlag = GCConstants.NO;
    
    //GCSTORE-4795
    YFCNodeList<YFCElement> nlOrderList = getOrderListOPDoc.getElementsByTagName(GCConstants.ORDER);
    if(!YFCCommon.isVoid(nlOrderList)){
      YFCElement eleOrder = nlOrderList.item(0);
      String sMaxOrderStatus = eleOrder.getAttribute("MaxOrderStatus");
      if (!YFCCommon.isStringVoid(sMaxOrderStatus) && Double.parseDouble(sMaxOrderStatus) >= 3200) {
        outStruct.holdOrderAndRaiseEvent = true;
        outStruct.holdReason = "REAUTH_FAILED_HOLD";
      }
    }
    
    raiseHardDeclineAlert(env, inStruct.paymentType, inStruct.chargeType, inStruct.requestAmount, getOrderListOPDoc);
    LOGGER.verbose("Class: GCPaymentUtils Method: processHardDeclinedResponse END");
    LOGGER.endTimer("GCPaymentUtils.processHardDeclinedResponse");
    return outStruct;
  }

  /**
   *
   * This method raise the alert on getting Declined response from payment engine
   *
   * @param env
   * @param sPaymentType
   * @param sChargeType
   * @param dRequestAmount
   * @param getOrderListOPDoc
   *
   */
  public static void raiseHardDeclineAlert(YFSEnvironment env, String sPaymentType, String sChargeType,
      double dRequestAmount, YFCDocument getOrderListOPDoc) {
    LOGGER.beginTimer("GCPaymentUtils.raiseHardDeclineAlert");
    LOGGER.verbose("Class: GCPaymentUtils Method: raiseHardDeclineAlert START");
    if (YFCCommon.equals(sPaymentType, GCConstants.PAYPAL, true)) {
      if (YFCCommon.equals(sChargeType, GCConstants.PAYMENT_STATUS_AUTHORIZATION)) {
        GCCommonUtil.invokeService(env, GCConstants.PAYPAL_AUTH_HD_ALERT_SERVICE, getOrderListOPDoc);
      } else if (YFCCommon.equals(sChargeType, GCConstants.PAYMENT_STATUS_CHARGE) && dRequestAmount > 0) {
        GCCommonUtil.invokeService(env, GCConstants.PAYPAL_CAPTURE_HD_ALERT_SERVICE, getOrderListOPDoc);
      }
    } else if (YFCCommon.equals(sPaymentType, GCConstants.BML, true)){
      LOGGER.verbose("HardDeclineAlert is invoked for BML payment type.");
      if (YFCCommon.equals(sChargeType, GCConstants.PAYMENT_STATUS_AUTHORIZATION)){
        GCCommonUtil.invokeService(env, GCConstants.BML_AUTH_HD_ALERT_SERVICE, getOrderListOPDoc);
      }
    } else if (YFCCommon.equals(sPaymentType, GCConstants.CREDIT_ON_ACCOUNT, true)){
      LOGGER.verbose("HardDeclineAlert is invoked for COA payment type.");
      if (YFCCommon.equals(sChargeType, GCConstants.PAYMENT_STATUS_AUTHORIZATION)){
        GCCommonUtil.invokeService(env, GCConstants.COA_AUTH_HD_ALERT_SERVICE, getOrderListOPDoc);
      }
    }
    LOGGER.verbose("Class: GCPaymentUtils Method: raiseHardDeclineAlert END");
    LOGGER.endTimer("GCPaymentUtils.raiseHardDeclineAlert");
  }

  /**
   * Raise exceptions related to payment gateway connection failures
   *
   * @param errCode
   * @param errDesc
   * @param itemId
   */
  public static void raiseException(String errCode, String errDesc,
      String orderNo) {
    LOGGER.beginTimer("raiseException");
    YFSException excep = new YFSException();
    excep.setErrorCode(errCode);
    excep.setErrorDescription(errDesc);
    excep.setAttribute("Exception for Order", orderNo);
    LOGGER.endTimer("raiseException");
    throw excep;

  }
  // GCSTORE-85: Design - PayPal Re-Authorization Processing : End

  /**
   * GCSTORE-752: Design - COA Payment Processing
   * @param inStruct
   * @param outStruct
   * @return
   */
  public static YFSExtnPaymentCollectionOutputStruct updateOutStructForAuthRev(
      YFSExtnPaymentCollectionInputStruct inStruct, YFSExtnPaymentCollectionOutputStruct outStruct) {
    LOGGER.beginTimer("updateOutStructForAuthRev()");
    LOGGER.verbose("updateOutStructForAuthRev() - Start");
    Date currentDate = new Date();
    outStruct.tranRequestTime = GCDateUtils.convertDate(currentDate);
    outStruct.authTime = GCConstants.BLANK;
    outStruct.authorizationId = GCConstants.AUTHORIZED;
    outStruct.tranType = GCConstants.AUTHORIZATION;
    outStruct.authReturnMessage = GCConstants.AUTHORIZED;
    outStruct.tranAmount = inStruct.requestAmount;
    outStruct.authorizationAmount = inStruct.requestAmount;
    outStruct.retryFlag = GCConstants.FLAG_N;

    LOGGER.endTimer("updateOutStructForAuthRev()");
    LOGGER.verbose("updateOutStructForAuthRev() - End");
    return outStruct;
  }
  
  /**
   * GCSTORE-6851 
   * 
   * Common method which takes data from Element Order and calls GCPaymentDeclineEmailService service
   * 
   * @param env
   * @param eleOrder
   */
  public static void prepareAndSendCardDeclineEmail(YFSEnvironment env, Element eleOrder, String strAuthId, String strSuspendPayment) 
  {
	  LOGGER.info("GCPaymentUtils.prepareAndSendCardDeclineEmail() - Begin");
	  LOGGER.debug(" strAuthId" + strAuthId);
	  LOGGER.debug(" strSuspendPayment" + strSuspendPayment);
	  YFCDocument docPaymentDeclineEmailInp = YFCDocument.createDocument("Order");
	  YFCElement elePaymentDeclineEmailInp =docPaymentDeclineEmailInp.getDocumentElement();
		
	  elePaymentDeclineEmailInp.setAttribute("AllocationRuleID", eleOrder.getAttribute("AllocationRuleID"));
	  elePaymentDeclineEmailInp.setAttribute("BillToID", eleOrder.getAttribute("BillToID"));
	  elePaymentDeclineEmailInp.setAttribute("CarrierAccountNo", eleOrder.getAttribute("CarrierAccountNo"));
	  elePaymentDeclineEmailInp.setAttribute("CarrierServiceCode", eleOrder.getAttribute("CarrierServiceCode"));
	  elePaymentDeclineEmailInp.setAttribute("ChargeActualFreightFlag", eleOrder.getAttribute("ChargeActualFreightFlag"));
	  elePaymentDeclineEmailInp.setAttribute("CreatedAtNode", eleOrder.getAttribute("CreatedAtNode"));
	  elePaymentDeclineEmailInp.setAttribute("CustomerEMailID", eleOrder.getAttribute("CustomerEMailID"));
	  elePaymentDeclineEmailInp.setAttribute("CustomerPONo", eleOrder.getAttribute("CustomerPONo"));
	  elePaymentDeclineEmailInp.setAttribute("DeliveryCode", eleOrder.getAttribute("DeliveryCode"));
	  elePaymentDeclineEmailInp.setAttribute("Division", eleOrder.getAttribute("Division"));
	  elePaymentDeclineEmailInp.setAttribute("DocumentType", eleOrder.getAttribute("DocumentType"));
	  elePaymentDeclineEmailInp.setAttribute("DraftOrderFlag", eleOrder.getAttribute("DraftOrderFlag"));
	  elePaymentDeclineEmailInp.setAttribute("DriverDate", eleOrder.getAttribute("DriverDate"));
	  elePaymentDeclineEmailInp.setAttribute("EnterpriseCode", eleOrder.getAttribute("EnterpriseCode"));
	  elePaymentDeclineEmailInp.setAttribute("EntryType", eleOrder.getAttribute("EntryType"));
	  elePaymentDeclineEmailInp.setAttribute("FreightTerms", eleOrder.getAttribute("FreightTerms"));
	  elePaymentDeclineEmailInp.setAttribute("HasChainedLines", eleOrder.getAttribute("HasChainedLines"));
	  elePaymentDeclineEmailInp.setAttribute("HasDerivedChild", eleOrder.getAttribute("HasDerivedChild"));
	  elePaymentDeclineEmailInp.setAttribute("HasDerivedParent", eleOrder.getAttribute("HasDerivedParent"));
	  elePaymentDeclineEmailInp.setAttribute("HoldFlag", eleOrder.getAttribute("HoldFlag"));
	  elePaymentDeclineEmailInp.setAttribute("HoldReasonCode", eleOrder.getAttribute("HoldReasonCode"));
	  elePaymentDeclineEmailInp.setAttribute("MaxOrderStatusDesc", eleOrder.getAttribute("MaxOrderStatusDesc"));
	  elePaymentDeclineEmailInp.setAttribute("MinOrderStatus", eleOrder.getAttribute("MinOrderStatus"));
	  elePaymentDeclineEmailInp.setAttribute("MaxOrderStatus", eleOrder.getAttribute("MaxOrderStatus"));
	  elePaymentDeclineEmailInp.setAttribute("MinOrderStatusDesc", eleOrder.getAttribute("MinOrderStatusDesc"));
	  elePaymentDeclineEmailInp.setAttribute("NotifyAfterShipmentFlag", eleOrder.getAttribute("NotifyAfterShipmentFlag"));
	  elePaymentDeclineEmailInp.setAttribute("OrderDate", eleOrder.getAttribute("OrderDate"));
	  elePaymentDeclineEmailInp.setAttribute("OrderHeaderKey", eleOrder.getAttribute("OrderHeaderKey"));
	  elePaymentDeclineEmailInp.setAttribute("OrderName", eleOrder.getAttribute("OrderName"));
	  elePaymentDeclineEmailInp.setAttribute("OrderNo", eleOrder.getAttribute("OrderNo"));
	  elePaymentDeclineEmailInp.setAttribute("OrderType", eleOrder.getAttribute("OrderType"));
	  elePaymentDeclineEmailInp.setAttribute("OriginalTax", eleOrder.getAttribute("OriginalTax"));
	  elePaymentDeclineEmailInp.setAttribute("OriginalTotalAmount", eleOrder.getAttribute("OriginalTotalAmount"));
	  elePaymentDeclineEmailInp.setAttribute("OtherCharges", eleOrder.getAttribute("OtherCharges"));
	  elePaymentDeclineEmailInp.setAttribute("PaymentRuleId", eleOrder.getAttribute("PaymentRuleId"));
	  elePaymentDeclineEmailInp.setAttribute("PaymentStatus", eleOrder.getAttribute("PaymentStatus"));
	  elePaymentDeclineEmailInp.setAttribute("PersonalizeCode", eleOrder.getAttribute("PersonalizeCode"));  
	  elePaymentDeclineEmailInp.setAttribute("PriorityCode", eleOrder.getAttribute("PriorityCode"));
	  elePaymentDeclineEmailInp.setAttribute("PriorityNumber", eleOrder.getAttribute("PriorityNumber"));
	  elePaymentDeclineEmailInp.setAttribute("Purpose", eleOrder.getAttribute("Purpose"));
	  elePaymentDeclineEmailInp.setAttribute("SCAC", eleOrder.getAttribute("SCAC"));
	  elePaymentDeclineEmailInp.setAttribute("ScacAndService", eleOrder.getAttribute("ScacAndService"));
	  elePaymentDeclineEmailInp.setAttribute("ScacAndServiceKey", eleOrder.getAttribute("ScacAndServiceKey"));
	  elePaymentDeclineEmailInp.setAttribute("SearchCriteria1", eleOrder.getAttribute("SearchCriteria1"));
	  elePaymentDeclineEmailInp.setAttribute("SearchCriteria2", eleOrder.getAttribute("SearchCriteria2"));
	  elePaymentDeclineEmailInp.setAttribute("ShipToID", eleOrder.getAttribute("ShipToID"));
	  elePaymentDeclineEmailInp.setAttribute("Status", eleOrder.getAttribute("Status"));
	  elePaymentDeclineEmailInp.setAttribute("TaxExemptFlag", eleOrder.getAttribute("TaxExemptFlag"));
	  elePaymentDeclineEmailInp.setAttribute("TaxExemptionCertificate", eleOrder.getAttribute("TaxExemptionCertificate"));
	  elePaymentDeclineEmailInp.setAttribute("TaxJurisdiction", eleOrder.getAttribute("TaxJurisdiction"));
	  elePaymentDeclineEmailInp.setAttribute("TaxPayerId", eleOrder.getAttribute("TaxPayerId"));
	  elePaymentDeclineEmailInp.setAttribute("TermsCode", eleOrder.getAttribute("TermsCode"));
	  elePaymentDeclineEmailInp.setAttribute("TotalAdjustmentAmount", eleOrder.getAttribute("TotalAdjustmentAmount"));          
	  elePaymentDeclineEmailInp.setAttribute("isHistory", eleOrder.getAttribute("isHistory"));
	  elePaymentDeclineEmailInp.setAttribute("AuthId", strAuthId);
	  elePaymentDeclineEmailInp.setAttribute("SuspendPayment", strSuspendPayment);
	  if (LOGGER.isDebugEnabled()) 
	  {
		  LOGGER.debug("Input to GCPaymentDeclineEmailService is ::" +docPaymentDeclineEmailInp.toString());
	  }
		    
	  env.setTxnObject("DeclineEmail", "");
		
	  GCCommonUtil.invokeService(env, "GCPaymentDeclineEmailService", docPaymentDeclineEmailInp);
	  LOGGER.info("GCPaymentUtils.prepareAndSendCardDeclineEmail() - End");
  }
}
