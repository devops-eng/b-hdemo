/** 
 * Copyright � 2015, Expicient Inc.,  All rights reserved.
 * ###########################################################################################################################################################
*           OBJECTIVE: Utility Class that Calls ATG Web service and returns the JSON response
*#############################################################################################################################################################
*           Version            Date              Modified By                Description
*#############################################################################################################################################################
             1.0               12/04/2016        Chirag                     JIRA No: Description of issue.
*#############################################################################################################################################################
 * For questions contact ally@expicient.com
 */

package com.gc.common.utils;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.core.YFSSystem;

/**
 * @author <a href="ally@expicient.com">Expicient</a>
 */
public class GCWebServiceRestUtil {

    private static YFCLogCategory LOGGER = YFCLogCategory
            .instance(GCWebServiceRestUtil.class);

    public static String restClientService(String strUrl)
            throws ClientProtocolException, IOException {

        LOGGER.verbose("GCWebServiceRestUtil.restClientService() -- Start");
        LOGGER.debug("Input URL : " + strUrl);
        String strResponseBody = "";
        // Create HTTP Client
        String strTimeOut = YFSSystem
				.getProperty(GCConstants.GC_ATG_SEARCHINDEX_TIME_OUT);
        int timeout = new Integer(strTimeOut);
        RequestConfig config = RequestConfig.custom()
        		  .setConnectTimeout(timeout * 1000)
        		  .setConnectionRequestTimeout(timeout * 1000)
        		  .setSocketTimeout(timeout * 1000).build();
        
        try
        {
	        // Create HTTP Client
	        HttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
			
	        // Post Http request
	        HttpGet getRequest = new HttpGet(strUrl);
	
	        // Executing request
	        HttpResponse response = httpClient.execute(getRequest);
	      
	
	        // Check for HTTP response code: 200 = success
	        if (response.getStatusLine().getStatusCode() != 200) {
	        	YFCException yfce = new YFCException(GCErrorConstants.GC_SEARCH_CATALOG_INDEX_DOWN_CODE);
			      throw yfce;
	        }
	
	        // Get-Capture Complete XML body response
	
	        HttpEntity entity = response.getEntity();
	       
	
		        if (entity != null) {
		            strResponseBody = EntityUtils.toString(entity);
		        }
        }
        catch (Exception e) {
			LOGGER.error("Exception is in GCATGSearchCatalogIndex.getSearchCatalogIndex()",	e);
			 YFCException yfce = new YFCException(GCErrorConstants.GC_SEARCH_CATALOG_INDEX_DOWN_CODE);
		      throw yfce;
		}
     
        LOGGER.debug("Output Response : " + strResponseBody);
        LOGGER.verbose("GCWebServiceRestUtil.restClientService() -- END");
        return strResponseBody;

    }

}
