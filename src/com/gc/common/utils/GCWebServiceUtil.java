package com.gc.common.utils;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.core.YFSSystem;

/**
 * This class contains logic to call WebService.
 *
 * @author gunjankumar
 *
 */
public class GCWebServiceUtil {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCWebServiceUtil.class.getName());

  /**
   * This method will invoke SOAP web service using http client and http post
   * utilities.
   *
   * @param env
   *            YFSEnvironment
   * @param inDoc
   *            Document
   * @return Document
   * @throws IOException
   * @throws Exception
   *             Exception
   */
  public Document invokeSOAPWebService(Document inDoc, String sURL,
      String sSoapAction) throws IOException {

    String sTimeOut;
    sTimeOut = YFSSystem.getProperty(GCConstants.WEBSERVICE_TIMEOUT);
    return invokeSOAPWebService(inDoc, sURL, sSoapAction, sTimeOut);
  }

  /**
   * This method invokes SOAP web service using http client and post method This is an overloaded
   * method to take Timeout value in addition to other required attributes
   *
   * @param inDoc
   * @param sURL
   * @param sSoapAction
   * @param sTimeOut
   * @return
   * @throws YFCException
   * @throws IOException
   */
  public Document invokeSOAPWebService(Document inDoc, String sURL, String sSoapAction, String sTimeOut)
      throws IOException {

    return invokeSOAPWebService(inDoc, sURL, sSoapAction, sTimeOut, GCConstants.BLANK);

  }


  /**
   * This method invokes SOAP web service using http client and post method This is an overloaded
   * method to take content type value in addition to other required attributes
   *
   * @param inDoc
   * @param sURL
   * @param sSoapAction
   * @param sTimeOut
   * @return
   * @throws YFCException
   * @throws IOException
   */
  public Document invokeSOAPWebService(Document inDoc, String sURL, String sSoapAction, String sTimeOut,
      String contentType) throws IOException {

    final String logInfo = "WebServiceUtil :: invokeSOAPWebService :: ";
    LOGGER.verbose(logInfo + "Begin");
    LOGGER.debug(logInfo + "Input Doc\n" + GCXMLUtil.getXMLString(inDoc));

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input Document to WebService:" + inDoc.toString()
          + "WebService URL:" + sURL + "SoapAction:" + sSoapAction
          + "DefaultTimeout:" + sTimeOut + "ContentType:"
          + contentType);
    }

    if (YFCCommon.isVoid(sTimeOut)) {

      sTimeOut = YFSSystem.getProperty(GCConstants.WEBSERVICE_TIMEOUT);

    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("ConfiguredTimeout from Service Arguements:"
          + sTimeOut);
    }
    // Get input document as XML String
    String sXMLInDoc = GCXMLUtil.getXMLString(inDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Soap messege:" + sXMLInDoc);
    }
    // Prepare HTTP post
    PostMethod post = new PostMethod(sURL);
    // Request content will be retrieved directly from the input stream
    RequestEntity entity = new StringRequestEntity(sXMLInDoc, GCConstants.TEXT_XML, GCConstants.UTF8);
    post.setRequestEntity(entity);
    // set SOAP Action
    if (!YFCCommon.isStringVoid(contentType)) {
      post.setRequestHeader(GCConstants.CONTENT_TYPE, contentType);
    }
    // set SOAP Action
    if (!YFCCommon.isStringVoid(sSoapAction)) {
      post.setRequestHeader(GCConstants.SOAP_ACTION, sSoapAction);
    }
    // Get HTTP client
    HttpClient httpclient = new HttpClient();

    // Set the socket timeout to 'n' seconds where n corresponds to the
    // value from gc_config.properties file
    httpclient.getHttpConnectionManager().getParams().setSoTimeout(Integer.valueOf(sTimeOut) * 1000);
    // Set the connection timeout to 'n' seconds where n corresponds to the
    // value from gc_config.properties file
    httpclient.getHttpConnectionManager().getParams().setConnectionTimeout(Integer.valueOf(sTimeOut) * 1000);
    // get status code
    httpclient.executeMethod(post);
    // get response message
    String sResponse = post.getResponseBodyAsString();
    LOGGER.debug(logInfo + "Response :: " + sResponse);
    Document opDoc = GCXMLUtil.getDocument(sResponse);

    if (!YFCCommon.isVoid(opDoc)) {
      LOGGER.debug(logInfo + "Webservice Response :: " + GCXMLUtil.getXMLString(opDoc));
    }
    // Release current connection to the connection pool once done
    post.releaseConnection();
    LOGGER.verbose(logInfo + "End");
    return opDoc;


  }


}
