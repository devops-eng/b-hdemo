/**
 * Copyright 2014 Guitar Center. All rights reserved. Guitar Center PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 *
 * #################################################################################################
 * ##################################### OBJECTIVE: XML Util
 * ########################################
 * ##############################################################################################
 * Version CR Date Modified By Description
 * ##########################################################
 * ############################################################################ 1.0 Initial Version
 * 14/02/2014 Soni, Karan Initial Version of XML Util
 *
 * #################################################################################################
 * #####################################
 */

package com.gc.common.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.xerces.jaxp.DocumentBuilderFactoryImpl;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.apache.xpath.CachedXPathAPI;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;

/**
 * A helper class providing methods for XML document processing. All methods are static, object of
 * this class cannot be created.
 */

public class GCXMLUtil {
  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCXMLUtil.class);

  private GCXMLUtil() {

  }

  /**
   * Create a new blank XML Document
   */
  public static Document newDocument() {
    try {
      return DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
    } catch (ParserConfigurationException e) {
      LOGGER.error("parserconfig exception thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;
    }
  }

  /**
   * Parse an XML string or a file, to return the Document.
   *
   * @param inXML if starts with '&lt;', it is a XML string; otherwise it should be an XML file name
   *
   * @return the Document object generated
   */
  public static Document getDocument(String inXML) {
    String currentXML = null;
    if (inXML != null && inXML.length() > 0) {
      currentXML = inXML.trim();
      if (currentXML.startsWith("<")) {
        StringReader strReader = new StringReader(currentXML);
        InputSource iSource = new InputSource(strReader);
        return getDocument(iSource);
      }
      // It's a file
      FileReader inFileReader;
      try {
        inFileReader = new FileReader(currentXML);
      } catch (FileNotFoundException e) {
        LOGGER.error("FileNotFoundException thrown ", e);
        YFCException ex = new YFCException(e.getMessage());
        ex.setStackTrace(e.getStackTrace());
        throw ex;
      }
      Document retVal = null;
      try {
        InputSource iSource = new InputSource(inFileReader);
        retVal = getDocument(iSource);
      } finally {
        if (inFileReader != null) {
          try {
            inFileReader.close();
          } catch (IOException e) {
            LOGGER.error("IOException thrown ", e);
            YFCException ex = new YFCException(e.getMessage());
            ex.setStackTrace(e.getStackTrace());
            throw ex;
          }
        }
      }
      return retVal;
    } else {
      return null;
    }
  }

  /**
   * Generate a Document object according to InputSource object
   */
  public static Document getDocument(InputSource inSource) {
    DocumentBuilder dbdr;
    try {
      dbdr = DocumentBuilderFactory.newInstance().newDocumentBuilder();
      return dbdr.parse(inSource);
    } catch (ParserConfigurationException e) {
      LOGGER.error("transformation error thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;
    } catch (SAXException e) {
      LOGGER.error("transformation error thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;
    } catch (IOException e) {
      LOGGER.error("io error thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;
    }
  }

  /**
   * Generate a Document object according to InputSource object
   *
   */
  public static Document getDocument(InputStream inStream) {
    Document retDoc = null;
    try {
      retDoc = getDocument(new InputSource(new InputStreamReader(inStream)));
      inStream.close();
    } catch (IOException e) {
      LOGGER.error("io error thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;
    }
    return retDoc;
  }

  /**
   * Create a Document object with input as the name of document element
   *
   * @param docElementTag : the document element name
   */
  public static Document createDocument(String docElementTag) {
    Document doc = newDocument();
    Element ele = doc.createElement(docElementTag);
    doc.appendChild(ele);
    return doc;
  }

  /**
   * Returns a formatted XML string for the Node, using encoding 'iso-8859-1'.
   *
   * @param Node a valid document object for which XML output in String form is required.
   *
   * @return the formatted XML string.
   */
  public static String serialize(Node node) {
    return serialize(node, "iso-8859-1", true);
  }

  /**
   * Return a XML string for a Node, with specified encoding and indenting flag.
   * <p>
   * <b>Note:</b> only serialize DOCUMENT_NODE, ELEMENT_NODE, and DOCUMENT_FRAGMENT_NODE
   *
   * @param node the input node.
   * @param encoding such as "UTF-8", "iso-8859-1"
   * @param indenting indenting output or not.
   *
   * @return the XML string
   */
  public static String serialize(Node node, String encoding, boolean indenting) {
    OutputFormat outFmt = null;
    StringWriter strWriter = null;
    XMLSerializer xmlSerializer = null;
    String retVal = null;
    try {
      outFmt = new OutputFormat("xml", encoding, indenting);
      outFmt.setOmitXMLDeclaration(true);
      strWriter = new StringWriter();
      xmlSerializer = new XMLSerializer(strWriter, outFmt);
      short ntype = node.getNodeType();
      switch (ntype) {
        case 11: // '\013'
          xmlSerializer.serialize((DocumentFragment) node);
          break;

        case 9: // '\t'
          xmlSerializer.serialize((Document) node);
          break;

        case 1: // '\001'
          xmlSerializer.serialize((Element) node);
          break;

        default:
          throw new IOException("Can serialize only Document, DocumentFragment and Element type nodes");
      }
      retVal = strWriter.toString();
    } catch (IOException e) {
      LOGGER.error("error", e);
      retVal = e.getMessage();
    } finally {
      try {
        if (strWriter != null) {
          strWriter.close();
        }
      } catch (IOException ie) {
        LOGGER.error("Use the base log functionality, Exception:", ie);
      }
    }
    return retVal;
  }

  /**
   * Return a decendent of first parameter, that is the first one to match the XPath specified in
   * the second parameter.
   *
   * @param ele The element to work on.
   * @param tagName format like "CHILD/GRANDCHILD/GRANDGRANDCHILD"
   *
   * @return the first element that matched, null if nothing matches.
   */
  public static Element getFirstElementByName(Element ele, String tagName) {
    StringTokenizer st = new StringTokenizer(tagName, "/");
    Element curr = ele;
    while (st.hasMoreTokens()) {
      String tag = st.nextToken();
      Node node;
      for (node = curr.getFirstChild(); node != null; node = node.getNextSibling()) {
        if (node.getNodeType() == 1 && tag.equals(node.getNodeName())) {
          break;
        }
      }
      if (node != null) {
        curr = (Element) node;
      } else {
        return null;
      }
    }
    return curr;
  }

  /**
   *
   */
  public static Element getRootElement(Document doc) {
    return doc.getDocumentElement();
  }

  /**
   * Creates an node element with the supplied name and attributevalues
   *
   * @param doc XML Document on which to create the element
   * @param elementName the name of the node element
   * @param hashAttributes the attributes for the node element in the Hashtable
   */
  public static Element createElement(Document doc, String elementName, Object hashAttributes) {
    return createElement(doc, elementName, hashAttributes, false);
  }

  /**
   *
   * Creates an YFC node element with the supplied name and attributevalues
   *
   * @param doc : XML Document on which to create the element
   * @param elementName : elementName the name of the node element
   * @param hashAttributes : the attributes for the node element in the Hashtable
   * @return
   *
   */
  public static YFCElement createElement(YFCDocument doc, String elementName, Object hashAttributes) {
    return createElement(doc, elementName, hashAttributes, false);
  }

  /**
   * Utilty method to create both the node element and the text node element.
   *
   * @param doc the XML document on which the node has to be created
   * @param elementName the name of the element to be created
   * @param hashAttributes the value for the text node or the attributes for the node element
   * @param textNodeFlag a flag signifying whether the node to be created is the text node
   */
  private static Element createElement(Document doc, String elementName, Object hashAttributes, boolean textNodeFlag) {
    Element elem = doc.createElement(elementName);
    if (hashAttributes != null && (hashAttributes instanceof String) && textNodeFlag) {
      elem.appendChild(doc.createTextNode((String) hashAttributes));
    }
    return elem;
  }

  /**
   *
   * Utilty method to create both the node element and the text node element
   *
   * @param doc : the XML document on which the node has to be created
   * @param elementName : the name of the element to be created
   * @param hashAttributes : the value for the text node or the attributes for the node element
   * @param textNodeFlag : a flag signifying whether the node to be created is the text node
   * @return elem : created element.
   */
  private static YFCElement createElement(YFCDocument doc, String elementName, Object hashAttributes,
      boolean textNodeFlag) {
    YFCElement elem = doc.createElement(elementName);
    if (hashAttributes != null && (hashAttributes instanceof String) && textNodeFlag) {
      elem.appendChild(doc.createTextNode((String) hashAttributes));
    }
    return elem;
  }

  /**
   * This method is for adding child Nodes to parent node element, the child element has to be
   * created first.
   *
   * @param parentElement Parent Element under which the new Element should be present
   * @param ElementName Name of the element to be created
   * @param value Can be either a String ,just the element value if it is a single attribute OR
   *        Hashtable containing the name-value pair of multiple attributes.
   */
  public static Element appendChild(Document doc, Element parentElement, String elementName, Object value) {
    Element childElement = createElement(doc, elementName, value);
    parentElement.appendChild(childElement);
    return childElement;
  }

  /**
   *
   * This method is for adding child Nodes to parent YFC node element, the child element has to be
   * created first.
   *
   * @param doc : the document in which element is to be appended.
   * @param parentElement : Parent Element under which the new Element should be present
   * @param elementName : Name of the element to be created
   * @param value : Can be either a String ,just the element value if it is a single attribute OR
   *        Hashtable containing the name-value pair of multiple attributes.
   * @return : childElement
   *
   */
  public static YFCElement appendChild(YFCDocument doc, YFCElement parentElement, String elementName, Object value) {
    YFCElement childElement = createElement(doc, elementName, value);
    parentElement.appendChild(childElement);
    return childElement;
  }

  /**
   * This method is for adding child Nodes to parent node element.
   *
   * @param parentElement Parent Element under which the new Element should be present
   * @param childElement Child Element which should be added.
   */
  public static void appendChild(Element parentElement, Element childElement) {
    parentElement.appendChild(childElement);
  }

  /**
   * This method is for setting the attribute of an element
   *
   * @param objElement Element where this attribute should be set
   * @param attributeName Name of the attribute
   * @param attributeValue Value of the attribute
   */
  public static void setAttribute(Element objElement, String attributeName, String attributeValue) {
    objElement.setAttribute(attributeName, attributeValue);
  }

  /**
   * This method is for removing the child element of an element
   *
   * @param parentElement Element from where the child element should be removed.
   * @param childElement Child Element which needs to be removed from the parent
   */
  public static void removeChild(Element parentElement, Element childElement) {
    parentElement.removeChild(childElement);
  }

  /**
   * This method takes Document as input and returns the XML String.
   *
   * @param document a valid document object for which XML output in String form is required.
   */
  public static String getXMLString(Document document) {
    return serialize(document);
  }

  /**
   * This method takes Document as input and returns the XML String.
   *
   * @param document a valid document object for which XML output in String form is required.
   */
  public static String getElementXMLString(Element element) {
    return serialize(element);
  }

  /** Gets the Parser from the input XML **/
  /** Gets the attribute of a node **/
  public static String getAttribute(Object element, String attributeName) {
    if (element != null) {
      String attributeValue = ((Element) element).getAttribute(attributeName);
      if (attributeValue != null) {
        attributeValue = attributeValue.trim();
      }
      return attributeValue;
    } else {
      return null;
    }
  }

  public static void copyElement(Document destDoc, Element srcElem, Element destElem) {
    NamedNodeMap attrMap = srcElem.getAttributes();
    int attrLength = attrMap.getLength();
    for (int count = 0; count < attrLength; count++) {
      Node attr = attrMap.item(count);
      String attrName = attr.getNodeName();
      String attrValue = attr.getNodeValue();
      destElem.setAttribute(attrName, attrValue);
    }

    if (srcElem.hasChildNodes()) {
      NodeList childList = srcElem.getChildNodes();
      int numOfChildren = childList.getLength();
      for (int cnt = 0; cnt < numOfChildren; cnt++) {
        Object childSrcNode = childList.item(cnt);
        if (childSrcNode instanceof CharacterData) {
          if (childSrcNode instanceof Text) {
            String data = ((CharacterData) childSrcNode).getData();
            Node childDestNode = destDoc.createTextNode(data);
            destElem.appendChild(childDestNode);
          } else if (childSrcNode instanceof Comment) {
            String data = ((CharacterData) childSrcNode).getData();
            Node childDestNode = destDoc.createComment(data);
            destElem.appendChild(childDestNode);
          }
        } else {
          Element childSrcElem = (Element) childSrcNode;
          Element childDestElem = appendChild(destDoc, destElem, childSrcElem.getNodeName(), null);
          copyElement(destDoc, childSrcElem, childDestElem);
        }
      }

    }
  }


  /**
   *
   * This method copy one element to another element object which can be appended to a new document.
   *
   * @param destDoc : Document in which new element is to be added.
   * @param srcElem : Element to be copied.
   * @param destElem : Copy of the original element.
   *
   */
  public static void copyElement(YFCDocument destDoc, YFCElement srcElem, YFCElement destElem) {
    Map<String, String> attrMap = srcElem.getAttributes();
    for (Entry<String, String> entry : attrMap.entrySet()) {
      String attrName = entry.getKey();
      String attrValue = entry.getValue();
      destElem.setAttribute(attrName, attrValue);
    }

    if (srcElem.hasChildNodes()) {
      YFCNodeList<YFCNode> childList = srcElem.getChildNodes();
      for (YFCNode eleChild : childList) {
	  	if(eleChild instanceof YFCElement){
        YFCElement childSrcElem = (YFCElement) eleChild;
        YFCElement childDestElem = appendChild(destDoc, destElem, childSrcElem.getNodeName(), null);
        copyElement(destDoc, childSrcElem, childDestElem);
		}
      }
    }
  }

  public static List getElementListByXpath(Document inXML, String xPath) {
    NodeList nodeList = null;
    List elementList = new ArrayList();
    CachedXPathAPI aCachedXPathAPI = new CachedXPathAPI();
    try {
      nodeList = aCachedXPathAPI.selectNodeList(inXML, xPath);
      int iNodeLength = nodeList.getLength();
      for (int iCount = 0; iCount < iNodeLength; iCount++) {
        Node node = nodeList.item(iCount);
        elementList.add(node);
      }
    } catch (TransformerException e) {
      LOGGER.error("transformation error thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;

    }


    return elementList;
  }

  public static Element getElementByXPath(Document inXML, String xPath) {
    Node node = null;
    Element eleNode = null;
    CachedXPathAPI oCachedXPathAPI = new CachedXPathAPI();
    try {
      node = oCachedXPathAPI.selectSingleNode(inXML, xPath);
      eleNode = (Element) node;
    } catch (TransformerException e) {
      LOGGER.error("transformation error thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;
    }
    return eleNode;
  }

  public static Document getDocumentFromElement(Element element) {
    Document doc = null;
    Node nodeImp = null;
    DocumentBuilder dbdr = null;
    try {
      dbdr = DocumentBuilderFactory.newInstance().newDocumentBuilder();
      doc = dbdr.newDocument();
      nodeImp = doc.importNode(element, true);
      doc.appendChild(nodeImp);
    } catch (ParserConfigurationException e) {
      LOGGER.error("parsing error thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;
    }
    return doc;
  }

  public static NodeList getNodeListByXpath(Document inXML, String xPath) {
    NodeList nodeList = null;
    CachedXPathAPI aCachedXPathAPI = new CachedXPathAPI();
    try {
      nodeList = aCachedXPathAPI.selectNodeList(inXML, xPath);
    } catch (TransformerException e) {
      LOGGER.error("transaformation error thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;
    }
    return nodeList;
  }

  /**
   * Constructs XML String from given element object
   *
   * @param inputElement Input element
   * @return XML String
   * @throws IllegalArgumentException for Invalid input
   * @throws Exception incase of any other exception
   */
  public static String getElementString(Element inputElement) {
    // Validate input element
    if (inputElement == null) {
      throw new IllegalArgumentException("Input element cannot be null in " + "GCXmlUtils.getElementString method");
    }

    // Import element content and construct Document
    Document document = getDocument(inputElement, true);

    // Convert document as element string
    String xmlString = getXMLString(document);

    // Remove Processing Instruction from xml string if exists
    xmlString = removeProcessingInstruction(xmlString);

    // Return result XML string
    return xmlString;
  }

  /**
   * Create a new document object with input element as the root.
   *
   * @param inputElement Input Element object
   * @param deep Include child nodes of this element true/false
   * @return XML Document object
   * @throws IllegalArgumentException if input is invalid
   * @throws Exception incase of any other exception
   */
  public static Document getDocument(Element inputElement, boolean deep) {
    // Validate input element
    if (inputElement == null) {
      throw new IllegalArgumentException("Input element cannot be null in " + "GCXmlUtils.getDocument method");
    }

    // Create a new document
    Document outputDocument = getDocument();

    // Import data from input element and
    // set as root element for output document
    outputDocument.appendChild(outputDocument.importNode(inputElement, deep));

    // return output document
    return outputDocument;
  }

  /**
   * Removes processing instruction from input XML String. Requirement is that input XML string
   * should be a valid XML.
   *
   * @param xmlString XML String thay may contain processing instruction
   * @throws IllegalArgumentException for Invalid input
   * @return XML String
   */
  public static String removeProcessingInstruction(String xmlString) {
    // Validate input XML string
    if (xmlString == null) {
      throw new IllegalArgumentException("Input XML string cannot be null in "
          + "GCXmlUtils.removeProcessingInstruction method");
    }

    // Is input contains processing instruction
    if ((xmlString.toLowerCase().trim().startsWith("<?xml"))) {
      // Get the ending index of processing instruction
      int processInstructionEndIndex = xmlString.indexOf("?>");

      // If processing instruction ending found,
      if (processInstructionEndIndex != -1) {
        // Remove processing instruction
        xmlString = xmlString.substring(processInstructionEndIndex + 2);
      }
    }

    // Return XML string after update
    return xmlString;
  }

  /**
   * Creates and empty Document object
   *
   * @throws Exception incase of any exception
   */
  public static Document getDocument() {
    // Create a new Document Builder Factory instance
    // SCR# 2392
    DocumentBuilderFactory documentBuilderFactory = new DocumentBuilderFactoryImpl();
    // Create new document builder
    DocumentBuilder documentBuilder;
    try {
      documentBuilder = documentBuilderFactory.newDocumentBuilder();
    } catch (ParserConfigurationException e) {
      LOGGER.error("transaformation error thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;
    }
    // Create and return document object
    return documentBuilder.newDocument();
  }

  /**
   * Method to return attribute value by XPath
   *
   * @see Need to call like --> GCXMLUtil.getAttributeFromXPath(Document Name,
   *      XPath/@AttributeName")
   * @param inXML
   * @param xpathExpr
   * @return Attribute Value
   * @throws Exception
   */
  public static String getAttributeFromXPath(Object inNode, String xpathExpr) {
    Node node = null;
    CachedXPathAPI aCachedXPathAPI = new CachedXPathAPI();
    try {
      node = aCachedXPathAPI.selectSingleNode((Node) inNode, xpathExpr);
    } catch (TransformerException e) {
      LOGGER.error("transaformation error thrown ", e);
      YFCException ex = new YFCException(e.getMessage());
      ex.setStackTrace(e.getStackTrace());
      throw ex;
    }

    if (node == null) {
      return null;
    } else {
      return node.getNodeValue();
    }
  }

  /**
   * @param Source Element
   * @param Destination Element
   * @return void
   */
  public static void copyAttributes(Element srcElem, Element destElem) {
    NamedNodeMap attrMap = srcElem.getAttributes();
    int attrLength = attrMap.getLength();
    for (int count = 0; count < attrLength; count++) {
      Node attr = attrMap.item(count);
      String attrName = attr.getNodeName();
      String attrValue = attr.getNodeValue();
      destElem.setAttribute(attrName, attrValue);

    }

  }

  /**
   * This method copies all attributes from destElem to srcElem elements.
   *
   * @param srcElem
   * @param destElem
   */
  public static void copyAttributes(YFCElement srcElem, YFCElement destElem) {
    Map<String, String> attrMap = srcElem.getAttributes();
    Set<Map.Entry<String, String>> entrySet = attrMap.entrySet();
    Iterator<Map.Entry<String, String>> itr = entrySet.iterator();
    while (itr.hasNext()) {
      Map.Entry<String, String> attrEntry = itr.next();
      destElem.setAttribute(attrEntry.getKey(), attrEntry.getValue());
    }
  }

  /**
   * Loads and constructs XML Document object from input file name.
   *
   * @param inputFileName XML Filename with path
   * @return XML Document object
   * @throws ParserConfigurationException ParserConfigurationException
   * @throws IOException IOException
   * @throws SAXException SAXException
   */
  public static Document getXmlFromFile(final String inputFileName) throws ParserConfigurationException, IOException,
  SAXException {
    // Validate input file name
    if (inputFileName == null || inputFileName.trim().length() == 0) {
      throw new IllegalArgumentException("Input Filename cannot be null or empty "
          + " in GenericExXMLUtil.getXMLfromfile method");
    }

    // Create an instance of document builder factory
    DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

    // Create document builder
    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

    // load XML document from file
    Document resultDocument = documentBuilder.parse(new File(inputFileName));

    // return result document
    return resultDocument;
  }
  /**
   *
   * This method will convert a YFCElement node into YFCDocument.
   *
   * @param inputElement
   * @param deep
   * @return
   *
   */
  public static YFCDocument getDocument(YFCElement inputElement, boolean deep) {
    // Validate input element
    if (inputElement == null) {
      throw new IllegalArgumentException("Input element cannot be null in " + "GCXmlUtils.getDocument method");
    }

    // Create a new document
    Document outputDocument = getDocument();
    YFCDocument outDocumentYFC = YFCDocument.getDocumentFor(outputDocument);
    // Import data from input element and
    // set as root element for output document
    outDocumentYFC.appendChild(outDocumentYFC.importNode(inputElement, deep));

    // return output document
    return outDocumentYFC;
  }
  
   /**
	 * Returns the clone of an XML Document.
	 * 
	 * @param doc
	 *            Input document to be cloned.
	 * @throws java.lang.Exception
	 *             If unable to clone document.
	 * @return Clone of the document.
 * @throws IOException 
 * @throws SAXException 
	 */
  public static Document cloneDocument(final Document doc) throws SAXException, IOException{
		return YFCDocument.parse(GCXMLUtil.getXMLString(doc)).getDocument();
	}
  /**
   * Returns AttributeList.
   * 
   * @param doc
   *            Input document.
   * @throws ParserConfigurationException
   * @throws TransformerException
   *         
   * @return attributeList

   */
  public static List getAttributeStringListByXpath(Object inNode, String XPath) throws ParserConfigurationException, TransformerException {
      List<String> attributeList = new ArrayList<String>();
      NodeList nodeList = getNodeListByXpath(inNode, XPath);
      int iNodeLength = nodeList.getLength();
      for (int iCount = 0; iCount < iNodeLength; iCount++) {
          Node node = nodeList.item(iCount);
          attributeList.add(node.getNodeValue());
      }
      return attributeList;
  }

  /**
   * 
   * @param inXML
   * @param XPath
   * @return
   * @throws ParserConfigurationException
   * @throws TransformerException
   */
  public static NodeList getNodeListByXpath(Object inNode, String XPath) throws ParserConfigurationException, TransformerException {
      CachedXPathAPI aCachedXPathAPI = new CachedXPathAPI();
      NodeList nodeList = aCachedXPathAPI.selectNodeList((Node) inNode, XPath);
      return nodeList;
  }
  
  public static String formatPrice(String sPrice) {
	    if (!YFCCommon.isVoid(sPrice)) {
	      Double dPrice = Double.parseDouble(sPrice);
	      sPrice = String.format("%.2f", dPrice);
	    }
	    return sPrice;

	  }
  
//Added as a part of MPOS-2333: Price RoundOff Start.  
  public static double roundOffPrice(Double dPrice)
  {
	  LOGGER.verbose("Enterd to round off the price");
	  LOGGER.verbose("\nPrice to round off--> " + dPrice.toString());
	  dPrice = (double) Math.round(dPrice * 100);
	  dPrice = dPrice / 100;	
	  return dPrice;
	  
  }
 //Added as a part of MPOS-2333: Price RoundOff End.
}
