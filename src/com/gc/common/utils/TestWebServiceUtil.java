package com.gc.common.utils;

import java.io.IOException;
import java.util.Properties;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

public class TestWebServiceUtil implements YIFCustomApi {

  Properties prop = null;

  private static final Logger LOGGER = Logger.getLogger(TestWebServiceUtil.class.getName());

  /**
   * This method will invoke SOAP web service using http client and http post utilities.
   *
   * @param env YFSEnvironment
   * @param inDoc Document
   * @return Document
   * @throws IOException
   * @throws HttpException
   * @throws Exception Exception
   */
  public Document invokeSOAPWebService(YFSEnvironment env, Document inDoc) throws IOException {

    final String logInfo = "WebServiceUtil :: invokeSOAPWebService :: ";
    LOGGER.info(logInfo + "Begin");
    LOGGER.debug(logInfo + "Input Doc\n" + GCXMLUtil.getXMLString(inDoc));

    if (YFCCommon.isVoid(prop)) {
      throw new YFCException(
          "webservice url and soap action are not configured in service argument. Please retry after configuring it.");
    }

    String sTimeOut = "";

    // Get input document as XML String
    String sXMLInDoc = GCXMLUtil.getXMLString(inDoc);

    // Prepare HTTP post
    PostMethod post = new PostMethod(prop.getProperty("URL"));
    // Request content will be retrieved directly from the input stream
    RequestEntity entity = new StringRequestEntity(sXMLInDoc, GCConstants.TEXT_XML, GCConstants.UTF8);
    post.setRequestEntity(entity);
    // set SOAP Action
    post.setRequestHeader(GCConstants.SOAP_ACTION, prop.getProperty("SOAP_ACTION"));
    // Get HTTP client
    HttpClient httpclient = new HttpClient();

    // Set the socket timeout to 'n' seconds where n corresponds to the
    // value from gc_config.properties file
    if (!YFCObject.isVoid(sTimeOut)) {
      httpclient.getHttpConnectionManager().getParams().setSoTimeout(Integer.valueOf(sTimeOut) * 1000);
      long timeout = httpclient.getHttpConnectionManager().getParams().getSoTimeout();
      LOGGER.debug("Timeout set to " + timeout + " milliseconds");
    }

    // get status code
    httpclient.executeMethod(post);
    // get response message
    String sResponse = post.getResponseBodyAsString();
    LOGGER.debug(logInfo + "Response :: " + sResponse);
    LOGGER.debug("Inside TestWebServiceUtil.invokeSOAPWebService:  " + logInfo + "Webservice Response :: " + sResponse);
    Document opDoc = GCXMLUtil.getDocument(sResponse);

    LOGGER.debug(logInfo + "Webservice Response :: " + GCXMLUtil.getXMLString(opDoc));
    // Release current connection to the connection pool once done
    post.releaseConnection();
    LOGGER.info(logInfo + "End");
    return opDoc;
  }


  @Override
  public void setProperties(Properties prop) {
    this.prop = prop;

  }

}
