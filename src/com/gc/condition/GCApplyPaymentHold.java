/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Checks whether the Payment Hold should be applied or not
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               07/25/2014        Soni, Karan		This class applies Payment Hold on the basis of various conditions
 *#################################################################################################################################################################
 */
package com.gc.condition;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.ycp.japi.YCPDynamicConditionEx;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCApplyPaymentHold implements YCPDynamicConditionEx {

  //Initialize Logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCApplyPaymentHold.class);

  @Override
  public boolean evaluateCondition(YFSEnvironment env, String strInput, Map mapInput, Document inDoc) {
    boolean returnFlag = false;

    try {

      List<String> listPrepaidPaymentTypes = Arrays.asList(
          "WIRE_TRANSFER", "CHECK", "CASH", "MONEY_ORDER",
          "WESTERN_UNION");
      Element eleRoot = inDoc.getDocumentElement();
      String strSellerOrg = eleRoot
          .getAttribute(GCConstants.SELLER_ORGANIZATION_CODE);
      String strOrderHeaderKey = eleRoot
          .getAttribute(GCConstants.ORDER_HEADER_KEY);
      Document docGetOrderDetails = null;
      Element eleOrder = null;
      if (!YFCCommon.isVoid(strOrderHeaderKey)) {
        docGetOrderDetails = GCCommonUtil.invokeGetOrderListAPITemp(
            env, strOrderHeaderKey,
            GCConstants.GC_CC_AUTH_GET_ORDER_LIST_TEMPLATE);

        // Getting eleOrder element which comes null at the timeof order
        // creation
        eleOrder = GCXMLUtil.getElementByXPath(docGetOrderDetails,
            "OrderList/Order");
      }
      // This if is for any order ammendment after order creation
      if (!YFCCommon.isVoid(eleOrder)) {
        NodeList nlPaymentMethod = GCXMLUtil.getNodeListByXpath(inDoc,
            "Order/PaymentMethods/PaymentMethod");
        for (int i = 0; i < nlPaymentMethod.getLength(); i++) {
          Element elePaymentType = (Element) nlPaymentMethod.item(i);
          String strPaymentType = elePaymentType
              .getAttribute(GCConstants.PAYMENT_TYPE);
          Element elePaymentExist = GCXMLUtil
              .getElementByXPath(
                  docGetOrderDetails,
                  "OrderList/Order/ChargeTransactionDetails/ChargeTransactionDetail/PaymentMethod[@PaymentType='"
                      + strPaymentType + "']");
          if (!strSellerOrg.equals(GCConstants.INTL_WEB)
              && listPrepaidPaymentTypes.contains(strPaymentType)
              && YFCCommon.isVoid(elePaymentExist)) {
            returnFlag = true;
            break;
          }
        }
      } else {
        // The code will come into this else on when the order gets
        // created.
        NodeList nlPaymentMethodIDS = GCXMLUtil.getNodeListByXpath(
            inDoc, "Order/PaymentMethods/PaymentMethod");
        for (int i = 0; i < nlPaymentMethodIDS.getLength(); i++) {
          Element elePaymentMethod = (Element) nlPaymentMethodIDS
              .item(i);
          String strPaymentType = elePaymentMethod
              .getAttribute(GCConstants.PAYMENT_TYPE);
          if (!strSellerOrg.equals(GCConstants.INTL_WEB)
              && listPrepaidPaymentTypes.contains(strPaymentType)) {
            returnFlag = true;
            break;
          }
        }
      }
    } catch (Exception e) {
      LOGGER.error("Inside GCApplyPaymentHold.evaluateCondition():", e);
      throw new GCException(e).getYFSException();
    }
    return returnFlag;
  }

  @Override
  public void setProperties(Map arg0) {
  }

}
