package com.gc.condition;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.util.YFCUtils;
import com.yantra.ycp.japi.YCPDynamicConditionEx;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCApplyUnderpricedHold implements YCPDynamicConditionEx {

    // initiaize Logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCApplyUnderpricedHold.class);

    @Override
    public boolean evaluateCondition(YFSEnvironment env, String arg1, Map arg2,
	    Document inDoc) {
	boolean bIsUnderpricedHoldApplied = false;
	try {
		String sourceSystem = "";
	    String strOHK = inDoc.getDocumentElement().getAttribute(
		    GCConstants.ORDER_HEADER_KEY);
	    Document getOrderListOp = GCCommonUtil
		    .invokeGetOrderListAPITemp(env, strOHK,
			    GCConstants.GC_CC_AUTH_GET_ORDER_LIST_TEMPLATE);
	    Document getOrderListOpDoc = GCXMLUtil
		    .getDocumentFromElement((Element) getOrderListOp
			    .getDocumentElement().getFirstChild());
	    Element eleOrder = getOrderListOpDoc.getDocumentElement();
        Element eleExtn = (Element) eleOrder.getElementsByTagName(GCConstants.EXTN).item(0);
        if (!YFCCommon.isVoid(eleExtn)) {
        sourceSystem = eleExtn.getAttribute("ExtnSourceSystem");
        }
      String strDraftOrderFalg = eleOrder
          .getAttribute("DraftOrderFlag");
	    if (GCConstants.YES.equals(strDraftOrderFalg)) {
		bIsUnderpricedHoldApplied = false;
	    } else {

		Document getOrderAuditListIP = GCXMLUtil
			.getDocument("<OrderAudit OrderHeaderKey='" + strOHK
				+ "'/>");
		Document getOrderAuditListTmp = GCCommonUtil
			.getDocumentFromPath(GCConstants.GET_ORDER_AUDIT_LIST_TEMPLATE);
		Document getOrderAuditListOP = GCCommonUtil.invokeAPI(env,
			GCConstants.API_GET_ORDER_AUDIT_LIST,
			getOrderAuditListIP, getOrderAuditListTmp);
		NodeList nlEligibleOrderLines = XPathAPI
			.selectNodeList(
				getOrderAuditListOP,
				"OrderAuditList/OrderAudit/OrderAuditLevels/OrderAuditLevel/"
					+ "OrderAuditDetails/OrderAuditDetail/Attributes/"
					+ "Attribute[@Name='OrderedQty' and "
					+ "@OldValue='0.00' and @NewValue>'0.00']/../../../..");
		
		List<String> validOLsList = new ArrayList<String>();
		for (int i = 0; i < nlEligibleOrderLines.getLength(); i++) {
		    Element eleOL = (Element) nlEligibleOrderLines.item(i);
		    String sOLKey = eleOL
			    .getAttribute(GCConstants.ORDER_LINE_KEY);
		    validOLsList.add(sOLKey);
		}

		NodeList nlOrderLine = XPathAPI.selectNodeList(
			getOrderListOpDoc, "Order/OrderLines/OrderLine");
		for (int counter = 0; counter < nlOrderLine.getLength(); counter++) {

		    Element eleOrderLine = (Element) nlOrderLine.item(counter);
		    String sOrderLineKey = eleOrderLine
			    .getAttribute(GCConstants.ORDER_LINE_KEY);

		    String sExtnIsFreeGiftItem = GCXMLUtil
			    .getAttributeFromXPath(eleOrderLine,
				    "Extn/@ExtnIsFreeGiftItem");
		    Element eleBundleParentLine = (Element) XPathAPI
			    .selectSingleNode(eleOrderLine, "BundleParentLine");
		    if (validOLsList.contains(sOrderLineKey)
			    && YFCCommon.isVoid(eleBundleParentLine)
			    && !GCConstants.YES
				    .equalsIgnoreCase(sExtnIsFreeGiftItem)) {
			String sUnitPrice = GCXMLUtil.getAttributeFromXPath(
				eleOrderLine, "LinePriceInfo/@UnitPrice");
			Double dUnitPrice = Double.valueOf(sUnitPrice);

			String sItemID = GCXMLUtil.getAttributeFromXPath(
				eleOrderLine, "Item/@ItemID");
			String sUOM = GCXMLUtil.getAttributeFromXPath(
				eleOrderLine, "Item/@UnitOfMeasure");

			Document docGetItemListOP = GCCommonUtil.getItemList(
				env, sItemID, sUOM, GCConstants.GCI);

			String sExtnInventoryCost = GCXMLUtil
				.getAttributeFromXPath(docGetItemListOP,
					"ItemList/Item/Extn/@ExtnInventoryCost");
			double dExtnInventoryCost = (!YFCCommon
				.isVoid(sExtnInventoryCost)) ? Double
				.valueOf(sExtnInventoryCost) : 0.00;
			    LOGGER.debug(" dUnitPrice is " + dUnitPrice
				    + " dExtnInventoryCost is "
				    + dExtnInventoryCost);
			if (!bIsUnderpricedHoldApplied
				&& (dExtnInventoryCost) > dUnitPrice  && YFCUtils.equals(sourceSystem, "ATG")) {
			    LOGGER.debug(" Inside if(dExtnInventoryCost > dUnitPrice) ");
			    bIsUnderpricedHoldApplied = true;
			}
		    }
		}
	    }
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch block of GCProcessOrderOnSuccess.validateUnderpricedHold(), Exception is :",
		    e);
	}
	return bIsUnderpricedHoldApplied;
    }

    @Override
    public void setProperties(Map arg0) {

    }

}
