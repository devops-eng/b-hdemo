package com.gc.condition;

import java.util.Map;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.ycp.japi.YCPDynamicConditionEx;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCCheckChangeOrderCallReason implements YCPDynamicConditionEx {
    private static final YFCLogCategory LOGGER = YFCLogCategory
            .instance(GCCheckChangeOrderCallReason.class.getName());

    @Override
    public boolean evaluateCondition(YFSEnvironment yfsenvironment, String s,
            Map map, Document inDoc){
    	if (LOGGER.isVerboseEnabled()) {
    		 LOGGER.beginTimer("Entering GCCheckChangeOrderCallReason.evaluateCondition()");
			LOGGER.verbose("Incoming XML to evaluateCondition():" + GCXMLUtil.getXMLString(inDoc));
		}
        boolean returnFlag=true;
        try {
            Element eleOrder=(Element)XPathAPI.selectSingleNode(inDoc,"OrderInvoice");
            if(!YFCCommon.isVoid(eleOrder)){
                returnFlag=false;
                if (LOGGER.isVerboseEnabled()) {
       			LOGGER.verbose("Return from evaluateCondition:"+returnFlag);
       		 LOGGER.endTimer("Exiting GCCheckChangeOrderCallReason.evaluateCondition()");
       		}
            }
        } catch (Exception e) {
            LOGGER.error("Inside catch of GCCheckChangeOrderCallReason.evaluateCondition()",e);
            throw new GCException(e).getYFSException();
        }
        if (LOGGER.isVerboseEnabled()) {
   			LOGGER.verbose("Return from evaluateCondition():"+returnFlag);
   		 LOGGER.endTimer("Exiting GCCheckChangeOrderCallReason.evaluateCondition()");
   		}
        return returnFlag;
    }
    @Override
    public void setProperties(Map map) {
        
    }

}
