/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *##############################################################################################################################################################
 *          OBJECTIVE: Dynamic condition to fetch FulfillmentType from Sales Order and validate Phase 1 order or not
 *##################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *##################################################################################################################################################################
             1.0             27/04/2015      Rangarajan, Shwetha      GCSTORE-813 - Condition to check Phase 1 orders(Defect - 2369)
 *##################################################################################################################################################################
 */
package com.gc.condition;

import java.util.Map;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.util.YFCUtils;
import com.yantra.ycp.japi.YCPDynamicConditionEx;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCCheckIsPhaseOneReturnOrder implements YCPDynamicConditionEx {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCCheckIsPhaseOneReturnOrder.class.getName());

  /**
   * Condition to check for Phase-1 returns.
   */
  @Override
  public boolean evaluateCondition(YFSEnvironment yfsEnv, String stringArg, Map mapArg, Document inputDoc) {

    LOGGER.beginTimer("GCCheckIsPhaseOneReturnOrder :: evaluateCondition :: begin");
    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    YFCElement rootEle = inDoc.getDocumentElement();

    LOGGER.verbose("OrderHeaderKey fetched :" +rootEle.getAttribute(GCConstants.ORDER_HEADER_KEY));

    YFCDocument getOrderListTemp = YFCDocument.getDocumentFor("<OrderList><Order OrderHeaderKey='' >"
        + "<OrderLines><OrderLine OrderLineKey=''><DerivedFrom OrderHeaderKey='' /></OrderLine></OrderLines></Order></OrderList>");
    if(LOGGER.isVerboseEnabled()){
      LOGGER.verbose("getOrderList template doc for Return Order :" +getOrderListTemp.toString());
    }

    YFCDocument returnOrdDetails = GCCommonUtil.invokeAPI(yfsEnv, GCConstants.GET_ORDER_LIST, inDoc, getOrderListTemp);
    if(LOGGER.isVerboseEnabled()){
      LOGGER.verbose("Return Order Details :" +returnOrdDetails.toString());
    }

    YFCElement returnOrderListEle = returnOrdDetails.getDocumentElement();
    YFCElement returnOrderEle = returnOrderListEle.getChildElement(GCConstants.ORDER);
    YFCElement returnOrderLinesEle = returnOrderEle.getChildElement(GCConstants.ORDER_LINES);
    YFCElement returnOrderLineEle = returnOrderLinesEle.getChildElement(GCConstants.ORDER_LINE);
    if(!YFCCommon.isVoid(returnOrderLineEle)){
      YFCElement derivedFromOrderEle = returnOrderLineEle.getChildElement(GCConstants.DERIVED_FROM);
      String salesOrderHeaderKey = derivedFromOrderEle.getAttribute(GCConstants.ORDER_HEADER_KEY);

      YFCDocument getSalesOrderListIndoc = YFCDocument.createDocument(GCConstants.ORDER);
      YFCElement orderEle = getSalesOrderListIndoc.getDocumentElement();
      orderEle.setAttribute(GCConstants.ORDER_HEADER_KEY, salesOrderHeaderKey);

      YFCDocument getSalesOrderListTemp = YFCDocument.getDocumentFor("<OrderList><Order><OrderLines>"
          + "<OrderLine FulfillmentType=''/></OrderLines></Order></OrderList>");
      LOGGER.verbose("getOrderList template for Sales Order :" +getSalesOrderListTemp.toString());

      YFCDocument salesOrderDetails = GCCommonUtil.invokeAPI(yfsEnv, GCConstants.GET_ORDER_LIST, getSalesOrderListIndoc, getSalesOrderListTemp);
      if(LOGGER.isVerboseEnabled()){
        LOGGER.verbose("getOrderList Output for Sales Order :" +salesOrderDetails.toString());
      }
      YFCElement salesOrderList = salesOrderDetails.getDocumentElement();
      YFCElement salesOrderEle = salesOrderList.getChildElement(GCConstants.ORDER);
      YFCElement salesOrderLinesEle = salesOrderEle.getChildElement(GCConstants.ORDER_LINES);
      YFCElement salesOrderLineEle = salesOrderLinesEle.getChildElement(GCConstants.ORDER_LINE);
      String fulfilmentType = GCConstants.BLANK;
      if(!YFCCommon.isVoid(salesOrderLineEle)){
        fulfilmentType = salesOrderLineEle.getAttribute(GCConstants.FULFILLMENT_TYPE);
      }

      LOGGER.verbose("Fulfilment Type fecthed :" +fulfilmentType);
      if(YFCUtils.equals(fulfilmentType, GCConstants.SHIP_TO_CUSTOMER) && YFCUtils.equals(fulfilmentType, GCConstants.SHIP_TO_STORE)){
        return true;
      }
    }

    return false;
  }

  /**
   *
   */
  @Override
  public void setProperties(Map arg0) {

  }

}
