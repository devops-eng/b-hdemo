/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: Dynamic condition to evaluate the xpath and returns true or false
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0               05/07/2015        Singh, Gurpreet            GCSTORE-2504
 *#################################################################################################################################################################
 */
package com.gc.condition;

import java.util.Map;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.apache.xpath.objects.XObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.ycp.japi.YCPDynamicConditionEx;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * @author gurpreet.singh
 *
 */
public class GCDynamicConditionUtil implements YCPDynamicConditionEx {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCDynamicConditionUtil.class);
  private Map propMap;

  /**
   * Dynamic condition to evaluate the xpath and returns true or false
   *
   * @param env
   * @param name
   * @param mapData
   * @param inDoc
   * @return
   */
  @Override
  public boolean evaluateCondition(YFSEnvironment env, String name, Map mapData, Document inDoc) {
    LOGGER.beginTimer("GCDynamicConditionUtil.evaluateCondition");
    LOGGER.verbose("Class: GCDynamicConditionUtil Method: evaluateCondition START");
    boolean bRetVal = false;
    try {
      String orderLineKey = "" ;
      String shipNode = "";
      String orderHeaderKey =
          GCXMLUtil.getAttributeFromXPath(inDoc, "InvoiceDetail/InvoiceHeader /Order/@OrderHeaderKey");
      YFCDocument docInput = YFCDocument.getDocumentFor(inDoc);
      YFCElement  eleInput = docInput.getDocumentElement();
      YFCElement eleInvoiceHeader = eleInput.getChildElement(GCConstants.INVOICE_HEADER);
      YFCElement eleLineDetails = eleInvoiceHeader.getChildElement(GCConstants.LINE_DETAILS);
      YFCNodeList<YFCElement> nlLineDetail = eleLineDetails.getElementsByTagName(GCConstants.LINE_DETAIL);
      for(YFCElement eleLineDetail : nlLineDetail){
        YFCElement eleOrderLine = eleLineDetail.getChildElement(GCConstants.ORDER_LINE);
        if(!YFCCommon.isVoid(eleOrderLine)){
          orderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
          shipNode =
              GCXMLUtil.getAttributeFromXPath(inDoc,
                  "InvoiceDetail/InvoiceHeader /Order/OrderLines/OrderLine[@OrderLineKey='" + orderLineKey
                  + "']/Schedules/Schedule/@ShipNode");
          if (!YFCCommon.isVoid(shipNode)) {
            break;
          }
        }
      }
      if (YFCCommon.isVoid(shipNode)) {
        String parentOrderLineKey =
            GCXMLUtil.getAttributeFromXPath(inDoc,
                "InvoiceDetail/InvoiceHeader /LineDetails/LineDetail/OrderLine[@KitCode='BUNDLE']/@OrderLineKey");
        Document getOrderListIp = GCXMLUtil.createDocument(GCConstants.ORDER);;
        Element eleOrder = getOrderListIp.getDocumentElement();
        eleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
        Document getOrderListTemp =
            GCXMLUtil
                .getDocument("<OrderList><Order><OrderLines><OrderLine OrderLineKey='' BundleParentOrderLineKey=''><Schedules><Schedule ShipNode=''/></Schedules></OrderLine></OrderLines></Order></OrderList>");
         Document getOrderListOp =
         GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListIp,
         getOrderListTemp);

        shipNode =
            GCXMLUtil.getAttributeFromXPath(getOrderListOp,
                "OrderList/Order/OrderLines/OrderLine[@BundleParentOrderLineKey='"
                    + parentOrderLineKey
                    + "']/Schedules/Schedule/@ShipNode");
        // fix to 
        if(YFCCommon.isVoid(shipNode)){
        	// Fix for 5738:
        	String sChargeCategoryAdvanceCredit = GCXMLUtil.getAttributeFromXPath(inDoc, "InvoiceDetail/InvoiceHeader/HeaderCharges/HeaderCharge/@ChargeCategory");
        	if(!YFCCommon.isVoid(sChargeCategoryAdvanceCredit) && YFCCommon.equals(sChargeCategoryAdvanceCredit, "ADVANCE_CREDIT")){
        		shipNode = "MFI";
        	} else {
        		shipNode = GCXMLUtil.getAttributeFromXPath(inDoc, "//@ShipNode");
        	}
        }
      }



      // preparing input for getOrganizationHeirarchy API call
      final YFCDocument orgHeirarchyIndocYfc = YFCDocument.createDocument(GCConstants.ORGANIZATION);
      final YFCElement eleOrganization = orgHeirarchyIndocYfc.getDocumentElement();
      eleOrganization.setAttribute(GCXmlLiterals.ORGANIZATION_KEY, shipNode);
      final YFCDocument getOrgHeirarchyTmpl = YFCDocument.getDocumentFor((GCConstants.GET_ORG_HEIRARCHY_TEMPLATE));
      final YFCDocument orgHeirarchyOutdocYfc =
          GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORG_HIERARCHY, orgHeirarchyIndocYfc, getOrgHeirarchyTmpl);
      final YFCElement eleOrganizationYfc = orgHeirarchyOutdocYfc.getDocumentElement();
      if (!YFCCommon.isVoid(eleOrganizationYfc)) {
        final YFCElement eleNodeYfc = eleOrganizationYfc.getChildElement(GCConstants.NODE);

        // Checking if NodeType is not equal to "Store" then return false
        if (!YFCCommon.isVoid(eleNodeYfc)
            && (!(GCConstants.STORE.equals(eleNodeYfc.getAttribute(GCXmlLiterals.NODE_TYPE))))) {
          bRetVal = false;
        } else {
          bRetVal = true;
        }
      }
      LOGGER.verbose("evaluateCondition:: bRetVal:: " + bRetVal);
    } catch (Exception e) {
      YFSException ex = new YFSException();
      LOGGER.verbose("Class: GCDynamicConditionUtil Method: evaluateCondition END");
      LOGGER.endTimer("GCDynamicConditionUtil.evaluateCondition");
      LOGGER.error("Class: GCDynamicConditionUtil Method: evaluateCondition END", e);
      throw ex;
    }
    LOGGER.verbose("Class: GCDynamicConditionUtil Method: evaluateCondition END");
    LOGGER.endTimer("GCDynamicConditionUtil.evaluateCondition");
    return bRetVal;
  }

  @Override
  public void setProperties(Map props) {
    propMap = props;
  }

}
