package com.gc.condition;

import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.ycp.japi.YCPDynamicConditionEx;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCInactiveAccountHoldCondition implements YCPDynamicConditionEx {

    private static final YFCLogCategory LOGGER = YFCLogCategory
            .instance(GCInactiveAccountHoldCondition.class);
    @Override
    public boolean evaluateCondition(YFSEnvironment env, String sInput,
            Map mapInput, Document inDoc) {

        LOGGER.debug("GCInactiveAccountHoldCondition ::  evaluateCondition :: Begin");
        boolean returnFlag = false;
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("GCInactiveAccountHoldCondition ::  evaluateCondition :: Input document is"
                    + GCXMLUtil.getXMLString(inDoc));
        }

        String sExtnDAXCustomerID = GCXMLUtil.getAttributeFromXPath(inDoc,
                "Order/Extn/@ExtnDAXCustomerID");
        if (!YFCCommon.isVoid(sExtnDAXCustomerID)) {

            Document docGetCustomerListIp = GCXMLUtil
                    .getDocument("<Customer> <Extn ExtnDAXCustomerID=\""
                            + sExtnDAXCustomerID + "\"/> </Customer>");
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("GCInactiveAccountHoldCondition ::  evaluateCondition :: docGetCustomerListIp"
                        + GCXMLUtil.getXMLString(docGetCustomerListIp));
            }

            Document docGetCustomerListOp = GCCommonUtil
                    .invokeAPI(env, docGetCustomerListIp,
                            GCConstants.GET_CUSTOMER_LIST,
                            "global/template/api/getCustomerListForInactiveAccountHold");
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("GCInactiveAccountHoldCondition ::  evaluateCondition :: docGetCustomerListOp"
                        + GCXMLUtil.getXMLString(docGetCustomerListOp));
            }

            String sCustomerStatus = GCXMLUtil.getAttributeFromXPath(
                    docGetCustomerListOp, "CustomerList/Customer/@Status");
            if (!GCConstants.CUSTOMER_ACTIVE_STATUS.equals(sCustomerStatus) && sCustomerStatus!=null) {
                String sCustomerID = GCXMLUtil.getAttributeFromXPath(
                        docGetCustomerListOp,
                        "CustomerList/Customer/@CustomerID");
                returnFlag = true;
                raiseAlert(env, inDoc, sCustomerID);
            }

        }
        LOGGER.debug("GCInactiveAccountHoldCondition ::  evaluateCondition :: End");
        return returnFlag;
    }

    private void raiseAlert(YFSEnvironment env, Document inDoc,
            String sCustomerID) {
        LOGGER.debug("GCInactiveAccountHoldCondition ::  raiseAlert :: Start");
            Element eleInputRoot = inDoc.getDocumentElement();
            eleInputRoot.setAttribute(GCConstants.CUSTOMER_ID, sCustomerID);
            GCCommonUtil.invokeService(env, GCConstants.GC_INACTIVE_ACCOUNT_ALERT, inDoc);
         LOGGER.debug("GCInactiveAccountHoldCondition ::  raiseAlert :: End");  
    }

    @Override
    public void setProperties(Map arg0) {
              
    }

}
