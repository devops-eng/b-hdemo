/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 * *###########################################
 * ######################################################
 * ################################################################ OBJECTIVE: This class is used to
 * resolve the holds on the warranty lines when their parent line is released.
 * ######################
 * ############################################################################
 * ############################################################### Version Date Modified By
 * Description
 * ######################################################################################
 * ########################################################################### 1.0 11/06/2015
 * Saxena, Ekansh Condition to check if the order is in completely shipped status.
 * ##################
 * ################################################################################
 * ###############################################################
 */
package com.gc.condition;

import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.util.YFCUtils;
import com.yantra.ycp.japi.YCPDynamicConditionEx;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCIsBackordered implements YCPDynamicConditionEx {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCIsBackordered.class.getName());

  /**
   * Condition to check if the order is in completely shipped status.
   */

  @Override
  public boolean evaluateCondition(YFSEnvironment env, String s, Map map, Document inDoc) {
    LOGGER.beginTimer("GCIsBackordered :: evaluateCondition :: begin");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" evaluateCondition() method, Input document is" + GCXMLUtil.getXMLString(inDoc));
    }

    boolean isBackordered = false;
    //YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    //GC-6009-starts
    Element eleOrder= GCXMLUtil.getElementByXPath(inDoc, "//Order");
    //GC-6009-ends
   // YFCElement eleOrder = inputDoc.getDocumentElement();
    String sOrderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);

    YFCDocument docGetOrderListIP = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleOrderGetOrderList = docGetOrderListIP.getDocumentElement();
    eleOrderGetOrderList.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    YFCDocument docGetOrderListIPTemplate =
        YFCDocument.getDocumentFor("<OrderList><Order OrderNo='' OrderHeaderKey='' SellerOrganizationCode=''>"
            + "<OrderLines><OrderLine MaxLineStatus='' MinLineStatus='' OrderLineKey=''>"
            + "</OrderLine></OrderLines></Order></OrderList>");

    YFCDocument docGetOrderListOp =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, docGetOrderListIP, docGetOrderListIPTemplate);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("GetOrderList Output document is" + docGetOrderListOp.toString());
    }

    YFCElement eleOrderList = docGetOrderListOp.getDocumentElement();
    YFCElement eleGetOrder = eleOrderList.getChildElement(GCConstants.ORDER);
    String sellerOrgCode = eleGetOrder.getAttribute(GCConstants.SELLER_ORGANIZATION_CODE);
    if (YFCUtils.equals(GCConstants.INTL_WEB, sellerOrgCode)) {
      return false;
    }
    YFCElement eleOrderLines = eleGetOrder.getChildElement(GCConstants.ORDER_LINES);
    if (!YFCCommon.isVoid(eleOrderLines)) {
      YFCNodeList<YFCElement> nlOrderLine = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
      for (YFCElement eleOrderLine : nlOrderLine) {
        String maxLineStatus = eleOrderLine.getAttribute(GCConstants.MAX_LINE_STATUS);
        String minLineStatus = eleOrderLine.getAttribute(GCConstants.MIN_LINE_STATUS);
        if (YFCCommon.equalsIgnoreCase(maxLineStatus, "1500.10") || YFCCommon.equalsIgnoreCase(maxLineStatus, "1300")
            || YFCCommon.equalsIgnoreCase(maxLineStatus, "1300.10")
            || YFCCommon.equalsIgnoreCase(minLineStatus, "1500.10")
            || YFCCommon.equalsIgnoreCase(minLineStatus, "1300")
            || YFCCommon.equalsIgnoreCase(minLineStatus, "1300.10")) {
          isBackordered = true;
          break;
        }
      }
    }
    return isBackordered;
  }
  @Override
  public void setProperties(Map arg0) {}
}
