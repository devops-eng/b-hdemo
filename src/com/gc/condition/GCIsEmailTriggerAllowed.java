/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 * *#################################################################################################################################################################
 * OBJECTIVE: This class is invoked to check whether Email should be triggered or not depending on the Email type.
 * #################################################################################################################################################################
 * Version                     Date                            Modified By                              Description
 * #################################################################################################################################################################
 * 1.0                       03/25/2015                        Saxena, Ekansh                           Check whehter Email should be triggered or not
 * #################################################################################################################################################################
 * 1.1                       09/09/2016                        Mishra, Deepak                           GCSTORE - 6890
 * #################################################################################################################################################################
 */
package com.gc.condition;

/**
 * This class checks whether Email should be triggered or not depending on the Email type.
 * 
 * @author Ekansh, Saxena
 */

import java.util.Map;

import org.w3c.dom.Document;

import com.gc.api.GCOnCancelPublish;
import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.ycp.japi.YCPDynamicConditionEx;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCIsEmailTriggerAllowed implements YCPDynamicConditionEx {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCIsEmailTriggerAllowed.class.getName());

  /**
   *
   * Description of GCIsEmailTriggerAllowed is a condition which decides whether Email should be
   * triggered or not depending upon the Email type.
   *
   * @param env
   * @param inDoc
   * @return
   * @throws Exception
   *
   */

  @Override
  public void setProperties(Map arg0) {
    
  }

  @Override
  public boolean evaluateCondition(YFSEnvironment env, String arg1, Map arg2, Document inDoc) {
    
    LOGGER.beginTimer("GCIsEmailTriggerAllowed.evaluateCondition");
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    LOGGER.verbose(" Entering evaluateCondition() method ");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" IsemailTriggerallowed() method, Input document is" + inputDoc.toString());
    }
    
    String sTriggerAllowed = null;
    String sResendAllowed = null; 
    
    YFCElement eleRoot = inputDoc.getDocumentElement();
    String sOrderHeaderKey = eleRoot.getAttribute(GCConstants.ORDER_HEADER_KEY);
    //GCSTORE-6890 FIX - Start
    if(YFCCommon.isVoid(sOrderHeaderKey)){
    	YFCElement sShipmentEle = eleRoot.getElementsByTagName(GCConstants.SHIPMENT).item(0);
    	sOrderHeaderKey = sShipmentEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
    }
    //GCSTORE-6890 FIX - End
    String sIsresendRequest = eleRoot.getAttribute(GCConstants.IS_RESEND_REQUEST);

    String sEmailtype = eleRoot.getAttribute(GCConstants.EMAIL_TYPE);

    YFCDocument getOrderListIp = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleOrderList = getOrderListIp.getDocumentElement();
    eleOrderList.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    YFCDocument getOrderListopTmp = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_TMP_FOR_IS_EMAIL_ALLOWED);
    YFCDocument getOrderListOpDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListIp, getOrderListopTmp);

    YFCElement eleOrder = getOrderListOpDoc.getDocumentElement().getChildElement(GCConstants.ORDER);
    String sEnterpriseCode = eleOrder.getAttribute(GCConstants.ENTERPRISE_CODE);
    String sSellerOrganizationCode = eleOrder.getAttribute(GCConstants.SELLER_ORGANIZATION_CODE);

    YFCElement eleExtn = eleOrder.getChildElement(GCConstants.EXTN);
    String sExtnOrderCaptureChannel = eleExtn.getAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL);
    
    //Checking EntryType and EMailID present for mPOS order: Start.
    String sEntryType = eleOrder.getAttribute(GCConstants.ENTRY_TYPE);
    YFCElement elePersonInfoBillTo = eleOrder.getElementsByTagName(GCConstants.PERSON_INFO_BILL_TO).item(0);
    LOGGER.verbose("PersonInfoBillTo fetched -->" + elePersonInfoBillTo);
    String sEmailID = elePersonInfoBillTo.getAttribute(GCConstants.EMAIL_ID);
    LOGGER.verbose("EMailID fetched -->" + sEmailID);
    if(YFCCommon.equalsIgnoreCase(sEntryType, GCConstants.MPOS) && YFCElement.isVoid(sEmailID))
    {
    	LOGGER.verbose("EntryType is mPOS and no EmailId is present.");
    	LOGGER.verbose("TriggerAllowed is set to false");
    	return false;
    }
    //Checking EnrtyType and EMailID present for mPOS order: End.

    YFCDocument docGetEmailTriggerIp = YFCDocument.createDocument(GCConstants.GC_GET_EMAIL_TRIGGER);
    YFCElement eleRootofdocGetEmailTriggerIp = docGetEmailTriggerIp.getDocumentElement();
    eleRootofdocGetEmailTriggerIp.setAttribute(GCConstants.ENTERPRISE_CODE, sEnterpriseCode);
    eleRootofdocGetEmailTriggerIp.setAttribute(GCConstants.SELLER_ORGANIZATION_CODE, sSellerOrganizationCode);
    eleRootofdocGetEmailTriggerIp.setAttribute(GCConstants.ORDER_CAPTURE_CHANNEL, sExtnOrderCaptureChannel);
    eleRootofdocGetEmailTriggerIp.setAttribute(GCConstants.EMAIL_TYPE, sEmailtype);

    YFCDocument docGetEmailTriggerOp =
        GCCommonUtil.invokeService(env, GCConstants.GC_GET_EMAIL_TRIGGER_ALLOWED_LIST_SERVICE, docGetEmailTriggerIp);
    
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" IsemailTriggerallowed() method, GetEmailTrigger Output document is" + docGetEmailTriggerOp.toString());
    }
    
    YFCElement eleGetEmailTriggerList = docGetEmailTriggerOp.getDocumentElement();
    YFCElement eleGetEmailTrigger = eleGetEmailTriggerList.getChildElement(GCConstants.GC_EMAIL_TRIGGER);
    
    if(!YFCCommon.isVoid(eleGetEmailTrigger)){
    sTriggerAllowed = eleGetEmailTrigger.getAttribute(GCConstants.TRIGGER_ALLOWED);
    sResendAllowed = eleGetEmailTrigger.getAttribute(GCConstants.RESEND_ALLOWED);
    }
    
    if (YFCCommon.equalsIgnoreCase(GCConstants.YES, sIsresendRequest)) {
      if (YFCCommon.equalsIgnoreCase(GCConstants.YES, sResendAllowed)) {
        LOGGER.endTimer("GCIsEmailTriggerAllowed.evaluateCondition");
        return true;
      } else if(YFCCommon.equalsIgnoreCase(GCConstants.NO, sResendAllowed)) {
        LOGGER.endTimer("GCIsEmailTriggerAllowed.evaluateCondition");
        YFCException e = new YFCException(GCErrorConstants.RESEND_NOT_ALLOWED, "Email resend is not allowed.");
        throw e;
      }
    }
    if (YFCCommon.equalsIgnoreCase(GCConstants.YES, sTriggerAllowed)) {
      LOGGER.endTimer("GCIsEmailTriggerAllowed.evaluateCondition");
      return true;
    } else {
      LOGGER.endTimer("GCIsEmailTriggerAllowed.evaluateCondition");
      return false;
    }
  }
}