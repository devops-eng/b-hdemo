/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               06/25/2014        Mittal, Yashu		JIRA No: Description of issue.
 *#################################################################################################################################################################
 */
package com.gc.condition;

import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.ycp.japi.YCPDynamicConditionEx;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:yashu.mittal@expicient.com">Yashu Mittal</a>
 */
public class GCIsInvalidBackorderCondition implements YCPDynamicConditionEx {

  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCIsInvalidBackorderCondition.class.getName());

  /**
   * This condition checks for ExtnIsBackorderable attribute in
   * CREATE_ORDER_ON_SUCCESS and SCHEDULE_ORDER_ON_BACKORDER
   *
   * @param arg0
   * @param arg1
   * @param arg2
   * @param CREATE_ORDER_ON_SUCCESS
   *            and SCHEDULE_ORDER_ON_BACKORDER event output.
   * @return
   * @see com.yantra.ycp.japi.YCPDynamicConditionEx#evaluateCondition(com.yantra.yfs.japi.YFSEnvironment,
   *      java.lang.String, java.util.Map, org.w3c.dom.Document)
   */
  @Override
  public boolean evaluateCondition(YFSEnvironment env, String arg1, Map arg2, Document inDoc) {
    try {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(" Entering IsInvalidBackorderCondition.evaluateCondition() method ");
        LOGGER.debug(" IsInvalidBackorderCondition() method, Input document is"
            + GCXMLUtil.getXMLString(inDoc));
      }
      // Finding all orderlines of the order.
      List<Element> listOrderlines = GCXMLUtil.getElementListByXpath(
          inDoc, GCConstants.ORDER_ORDERLINES_ORDERLINE_XPATH);
      for (Element eleOrderLine : listOrderlines) {

        String sItemId = GCXMLUtil.getAttributeFromXPath(eleOrderLine,
            "Item/@ItemID");
        LOGGER.debug(sItemId);

        Document getItemListOutDoc = GCCommonUtil
            .invokeAPI(
                env,
                GCConstants.GET_ITEM_LIST,
                GCXMLUtil
                .getDocument("<Item ItemID='" + sItemId
                    + "' OrganizationCode='"
                    + GCConstants.GUITAR_CENTER_INC
                    + "'/>"),
                    GCXMLUtil
                    .getDocument("<ItemList><Item><Extn ExtnIsBackorderable=''/></Item></ItemList>"));

        if (getItemListOutDoc.getDocumentElement().hasChildNodes()) {
          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(GCXMLUtil.getXMLString(getItemListOutDoc));
          }
          String sExtnIsBackorderable = GCXMLUtil
              .getAttributeFromXPath(getItemListOutDoc,
                  "ItemList/Item/Extn/@ExtnIsBackorderable");

          LOGGER.debug(sExtnIsBackorderable);
          if (sExtnIsBackorderable.equalsIgnoreCase(GCConstants.NO)) {
            Element eleOrder = inDoc.getDocumentElement();
            String orderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);

            YFCDocument docGetExceptionListIp = YFCDocument.createDocument(GCConstants.INBOX);
            YFCElement eleInbox = docGetExceptionListIp.getDocumentElement();
            eleInbox.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
            eleInbox.setAttribute(GCConstants.STATUS, "OPEN");
            eleInbox.setAttribute(GCConstants.EXCEPTION_TYPE, "GCInvalidBackOrder");
            YFCDocument templateDoc = YFCDocument.getDocumentFor(GCConstants.GET_EXCEPTION_LIST_TEMP);
            YFCDocument yfcOutputDoc =
                GCCommonUtil.invokeAPI(env, "getExceptionListForOrder", docGetExceptionListIp, templateDoc);
            YFCElement eleOp = yfcOutputDoc.getDocumentElement();
            YFCNodeList<YFCElement> eleInboxOp = eleOp.getElementsByTagName(GCConstants.INBOX);
            int noOfAlerts = eleInboxOp.getLength();
            if (noOfAlerts == 0) {
              return true;
            }
          }
        }

      }
    } catch (Exception e) {
      LOGGER.error(
          "Inside catch block of IsInvalidBackorderCondition. Exception is : ",
          e);
      throw new GCException(e).getYFSException();

    }
    return false;
  }

  /**
   * Description of setProperties
   *
   * @param arg0
   * @see com.yantra.ycp.japi.YCPDynamicConditionEx#setProperties(java.util.Map)
   */

  @Override
  public void setProperties(Map arg0) {

  }

}
