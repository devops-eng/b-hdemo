/**Copyright 2013 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.

	Change Log
#############################################################################################################################################################################
     OBJECTIVE: Foundation Class
##############################################################################################################################################################################
        Version    CR/ User Story                 Date                       Modified By                   Description
##############################################################################################################################################################################
        1.0         GCSTORE-52                   01/14/2015                  Jain,Shubham          Checking the Dynamic Condition for ItemID and Product Class & KitCode
        1.1         GCSTORE-52 					 01/20/2015					 Jain,Shubham		   Checking the Dynamic Condition for Product Class & KitCode  & Extn_Set_Code
        1.2         GCSTORE-52  				 01/21/2015					 Jain,Shubham          Remove the Alert Type and Add the return statements.
        1.3         GCSTORE-52					 01/22/2015					 Jain,Shubham          Checking the one more Dynamic Condition for Product Class & KitCode & Extn_Set_Code
##############################################################################################################################################################################
 */

package com.gc.condition;

import java.util.Map;
import org.w3c.dom.Document;
import com.gc.common.constants.GCConstants;
import com.yantra.ycp.japi.YCPDynamicConditionEx;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * GCSTORE-52 Inventory Sync from OMS to ATG 
 * This class is used to check the Dynamic condition that Item Id is present or Not.
 * And if ProductClass is SET then KitCode is BUNDLE or Not.If its return true those messages 
 * will not getting publish from OMS to ATG.
 * @author Shubham Jain
 * <a href="mailto:shubham.jain@expicient.com">Shubham</a>
 *
 */

public class GCIsItemIDBlankRTAM implements YCPDynamicConditionEx{
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCIsItemIDBlankRTAM.class.getName());
	/**
	 * Description of evaluateCondition - This Method will return true either if Item Id
	 * is null or (if Product Class is SET & KitCode is not BUNDLE) or (if Product Class is REGULAR
	 * & KitCode is BUNDLE & Extn_Set_Code is 1) or (if Product Class is SET & KitCode is Bundle & Extn_Set_Code is not 1). 
	 * Else it will return false. 
	 */

	public boolean evaluateCondition(YFSEnvironment env, String str,
			Map map, Document inDoc) {
		LOGGER.beginTimer("GCIsItemIDBlankRTAM.evaluateCondition");
		LOGGER.verbose("Class: GCIsItemIDBlankRTAM Method: evaluateCondition BEGIN");

		YFCDocument inputDoc=YFCDocument.getDocumentFor(inDoc);
		YFCElement eleRoot= inputDoc.getDocumentElement();
		YFCElement eleItem= eleRoot.getChildElement(GCConstants.ITEM);

		boolean shouldMessageNotbePublished = false;
		String sNode = eleRoot.getAttribute(GCConstants.NODE);

		if( YFCCommon.isVoid(sNode) || YFCCommon.isVoid(eleItem)){
			shouldMessageNotbePublished= true;
			return shouldMessageNotbePublished;
		}

		if(!YFCCommon.isVoid(eleItem)){
			String sProductClass=eleItem.getAttribute(GCConstants.PRODUCT_CLASS);
			YFCElement elePrimaryInfo=eleItem.getChildElement(GCConstants.PRIMARY_INFORMATION);
			if(!YFCCommon.isVoid(elePrimaryInfo)){
				String sKitCode=elePrimaryInfo.getAttribute(GCConstants.KIT_CODE);

				YFCElement extnItem = eleItem.getChildElement(GCConstants.EXTN);
				String sSetCode=extnItem.getAttribute(GCConstants.EXTN_SET_CODE);

				if((YFCCommon.equals(sProductClass,GCConstants.SET) && !YFCCommon.equals(sKitCode,GCConstants.BUNDLE))){
					shouldMessageNotbePublished= true;
					return shouldMessageNotbePublished;
				}

				if ((YFCCommon.equals(sProductClass,GCConstants.REGULAR) && YFCCommon.equals(sKitCode,GCConstants.BUNDLE)
						&& YFCCommon.equals(sSetCode,GCConstants.EXTN_SET_CODE_VALUE)) ){	
					shouldMessageNotbePublished= true;
					return shouldMessageNotbePublished;
				}

				if ((YFCCommon.equals(sProductClass,GCConstants.SET) && YFCCommon.equals(sKitCode,GCConstants.BUNDLE)
						&& !YFCCommon.equals(sSetCode,GCConstants.EXTN_SET_CODE_VALUE)) ){	
					shouldMessageNotbePublished= true;
					return shouldMessageNotbePublished;
				}
			}
		}
		LOGGER.endTimer("GCIsItemIDBlankRTAM.evaluateCondition");
		return  shouldMessageNotbePublished;
	}
	public void setProperties(Map map) {
	}
}
