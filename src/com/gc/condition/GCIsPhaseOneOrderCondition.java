/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *##############################################################################################################################################################
 *	        OBJECTIVE: Dynamic condition to fetch FulfillmentType from Sales Order and validate Phase 1 order or not
 *##################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *##################################################################################################################################################################
             1.0              25/03/2015        Ravi, Divya		        GCSTORE-813	- Condition to check Phase 1 orders
             1.1              28/04/2015      Rangarajan,Shwetha        GCSTORE-2369 - Defect fix
 *##################################################################################################################################################################
 */
package com.gc.condition;

import java.util.Map;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.util.YFCUtils;
import com.yantra.ycp.japi.YCPDynamicConditionEx;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCIsPhaseOneOrderCondition implements YCPDynamicConditionEx {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCIsPhaseOneOrderCondition.class.getName());

  /**
   * Condition to check if the order belongs to Phase 1
   */
  @Override
  public boolean evaluateCondition(YFSEnvironment yfsenvironment, String s,
      Map map, Document document) {

    LOGGER.beginTimer("GCIsPhaseOneOrderCondition :: evaluateCondition :: begin");
    String phaseMode = null;
    String orderHeaderKey = null;
    YFCDocument inputDoc = YFCDocument.getDocumentFor(document);
    YFCElement rootEle = inputDoc.getDocumentElement();
    String tagName = rootEle.getTagName();
    if (YFCUtils.equals(GCConstants.RECEIPT, tagName)) {
      YFCElement orderLineEle = rootEle.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
      orderHeaderKey = orderLineEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
    } else {
      orderHeaderKey = rootEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
    }
    YFCElement eleExtn = rootEle.getChildElement(GCConstants.EXTN);
    if (!YFCCommon.isVoid(eleExtn)) {
      phaseMode = eleExtn.getAttribute(GCConstants.EXTN_ORDER_PHASE);
    }
    if (YFCCommon.isVoid(phaseMode)) {
      YFCDocument getOrdListIp = YFCDocument.createDocument(GCConstants.ORDER);
      YFCElement eleOrdListIp = getOrdListIp.getDocumentElement();
      eleOrdListIp.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
      YFCDocument getOrderListOp =
          GCCommonUtil.invokeAPI(yfsenvironment, GCConstants.GET_ORDER_LIST, getOrdListIp,
              YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_COND));
      YFCElement eleOrderList = getOrderListOp.getDocumentElement();
      YFCElement eleOrder = eleOrderList.getChildElement(GCConstants.ORDER);
      YFCElement eleExtnOrd = eleOrder.getChildElement(GCConstants.EXTN);
      phaseMode = eleExtnOrd.getAttribute(GCConstants.EXTN_ORDER_PHASE);
    }
    if (YFCCommon.isVoid(phaseMode) || YFCUtils.equals(GCConstants.PHASE_ONE, phaseMode)) {
      LOGGER.verbose("Phase Mode : " + phaseMode);
      return true;
    } else {
      return false;
    }
  }



  @Override
  public void setProperties(Map map) {


  }

}
