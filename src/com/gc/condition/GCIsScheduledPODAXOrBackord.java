package com.gc.condition;

import java.util.Map;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.util.YFCUtils;
import com.yantra.ycp.japi.YCPDynamicConditionEx;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCIsScheduledPODAXOrBackord implements YCPDynamicConditionEx {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCIsScheduledPODAXOrBackord.class.getName());

  /**
   * Condition to check if the order is in backordered or scheduledPODAX status
   */

  @Override
  public boolean evaluateCondition(YFSEnvironment env, String s, Map map, Document inDoc) {
    LOGGER.beginTimer("GCIsScheduledPODAXOrBackord :: evaluateCondition :: begin");

    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement rootEle = inputDoc.getDocumentElement();
    YFCElement eleOrderLines = rootEle.getChildElement(GCConstants.ORDER_LINES);
    Object obPublishFlag = env.getTxnObject("DAXAllocationProcessing");
    String sPublishFlag = "";
    if (!YFCCommon.isVoid(obPublishFlag)) {
      sPublishFlag = (String) obPublishFlag;
    }
    LOGGER.verbose("sPublishFlag" + sPublishFlag);
    if (YFCCommon.equals(sPublishFlag, "Y")) {
      return false;
    }
    if (!YFCCommon.isVoid(eleOrderLines)) {
      YFCNodeList<YFCElement> nlOrderLine = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
      for (YFCElement eleOrderLine : nlOrderLine) {
        String minLineStatus = eleOrderLine.getAttribute(GCConstants.MIN_LINE_STATUS);
        String maxLineStatus = eleOrderLine.getAttribute(GCConstants.MAX_LINE_STATUS);

        if (YFCUtils.equals(GCConstants.BACKORDERED_STATUS, minLineStatus)) {
          return true;
        } else if (YFCUtils.equals("1500", maxLineStatus)) {
          String orderHeaderKey = rootEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
          String orderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
          YFCDocument getOrdListIp = YFCDocument.createDocument(GCConstants.ORDER);
          YFCElement eleOrdListIp = getOrdListIp.getDocumentElement();
          eleOrdListIp.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
          YFCDocument getOrderListOp =
              GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrdListIp,
                  YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_SYNC));
          YFCElement eleOrderList = getOrderListOp.getDocumentElement();
          YFCElement eleOrder = eleOrderList.getChildElement(GCConstants.ORDER);
          YFCElement eleOrderLinesOp = eleOrder.getChildElement(GCConstants.ORDER_LINES);
          YFCNodeList<YFCElement> nlOrderLineOp = eleOrderLinesOp.getElementsByTagName(GCConstants.ORDER_LINE);
          for (YFCElement eleOrderLineOp : nlOrderLineOp) {
            String orderLineKeyOp = eleOrderLineOp.getAttribute(GCConstants.ORDER_LINE_KEY);
            String maxLineStatusOp = eleOrderLineOp.getAttribute(GCConstants.MAX_LINE_STATUS);
            if (YFCUtils.equals(orderLineKeyOp, orderLineKey)
                && YFCUtils.equals(GCConstants.SCHEDULED_PO_DAX_STATUS, maxLineStatusOp)) {
              return true;
            }
          }
        } else if (YFCUtils.equals(GCConstants.SCHEDULED_PO_DAX_STATUS, maxLineStatus)) {
          return true;
        } else if (YFCUtils.equals(GCConstants.CANCELLED_ORDER_STATUS_CODE, maxLineStatus)) {
          YFCElement eleStatusBreakupForCanceledQty = eleOrderLine.getChildElement(GCConstants.STATUS_BREAKUP_QTY);
          if(!YFCCommon.isVoid(eleStatusBreakupForCanceledQty)){
          YFCElement eleCanceledFrom = eleStatusBreakupForCanceledQty.getChildElement(GCConstants.CANCELLED_FROM);
          String status = eleCanceledFrom.getAttribute(GCConstants.STATUS);
          if (YFCUtils.equals(GCConstants.SCHEDULED_PO_DAX_STATUS, status)
              || YFCUtils.equals(GCConstants.BACKORDERED_STATUS, status)) {
            return true;
          }
         }
        }
      }
    }
    return false;
  }

  @Override
  public void setProperties(Map arg0) {

  }
}