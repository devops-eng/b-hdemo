/**Copyright 2013 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.

	Change Log
##############################################################################################################################################
     OBJECTIVE: Foundation Class
##############################################################################################################################################
        Version    CR/ User Story                 Date                       Modified By                   Description
##############################################################################################################################################
        1.0         GCSTORE-52                   12/22/2014                  Jain,Shubham          Checking the Dynamic Condition of Item for Serialization
##############################################################################################################################################
 */

package com.gc.condition;

import java.util.Map;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.yantra.ycp.japi.YCPDynamicConditionEx;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;


/**
 * GCSTORE-52 Inventory Sync from OMS to ATG 
 * This class is used to check the Dynamic condition that Item is Sometimes Tag Controlled or
 * Always Tag Controlled or Non Tag Controlled.
 * 
 * @author Shubham Jain
 * <a href="mailto:shubham.jain@expicient.com">Shubham</a>

 *
 */
public class GCIsTagControlledCondition implements YCPDynamicConditionEx {
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCIsTagControlledCondition.class.getName());

	/**
	 * Description of evaluateCondition - This Method will return true if Item is 
	 * Sometimes Tag Controlled or Always Tag Controlled and return false if Item 
	 * is Not Tag Controlled.	
	 */
	public boolean evaluateCondition(YFSEnvironment env, String str,
			Map map, Document inDoc) {
		LOGGER.beginTimer("GCIsTagControlledCondition.evaluateCondition");
		LOGGER.verbose("Class: GCIsTagControlledCondition Method: evaluateCondition BEGIN");

		YFCDocument inputDoc=YFCDocument.getDocumentFor(inDoc);
		YFCElement eleRoot= inputDoc.getDocumentElement();
		YFCElement eleItem= eleRoot.getChildElement(GCConstants.ITEM);

		if(!YFCCommon.isVoid(eleItem)){
			YFCElement elePriInfo=eleItem.getChildElement(GCConstants.PRIMARY_INFORMATION);
			String sKitCode=elePriInfo.getAttribute(GCConstants.KIT_CODE);

			YFCElement eleInventoryParam=eleItem.getChildElement(GCConstants.INVENTORY_PARAMETERS);

			if(!YFCCommon.isVoid(eleInventoryParam)){
				String sTagFlag=eleInventoryParam.getAttribute(GCXmlLiterals.TAG_CONTROL_FLAG);

				if(GCConstants.SOMETIMES_TAG_CONTROLLED.equals(sTagFlag)||GCConstants.ALWAYS_TAG_CONTROLLED.equals(sTagFlag)||GCConstants.BUNDLE.equals(sKitCode)) {
					LOGGER.endTimer("GCIsTagControlledCondition.evaluateCondition");
					return true;
				}
			}
		}
		LOGGER.endTimer("GCIsTagControlledCondition.evaluateCondition");
		return false;
	}

	public void setProperties(Map map) {
	}
}
