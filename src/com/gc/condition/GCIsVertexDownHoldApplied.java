/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 * *###########################################
 * ######################################################
 * ################################################################ OBJECTIVE: Dynamic condition to
 * evaluate the xpath and returns true or false. Writing this class to fix a defect GCSTORE-2964
 * #####################################################
 * #############################################
 * ############################################################### Version Date Modified By
 * Description
 * ######################################################################################
 * ########################################################################### 1.0 05/07/2015 taj,
 * Rashma GCSTORE-2964 ############################################################################
 * #####################################################################################
 */
package com.gc.condition;

import java.util.Map;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.yantra.ycp.japi.YCPDynamicConditionEx;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * @author taj,rashma
 *
 */
public class GCIsVertexDownHoldApplied implements YCPDynamicConditionEx {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCIsVertexDownHoldApplied.class);
  private Map propMap;

  /**
   * Dynamic condition to evaluate the xpath and returns true or false
   *
   * @param env
   * @param name
   * @param mapData
   * @param inDoc
   * @return
   */
  @Override
  public boolean evaluateCondition(YFSEnvironment env, String name, Map mapData, Document inDoc) {
    LOGGER.beginTimer("GCIsVertexDownHoldApplied.evaluateCondition");
    LOGGER.verbose("Class: GCIsVertexDownHoldApplied Method: evaluateCondition START");
    boolean bRetVal = false;
    StringBuilder strEvalXPath = new StringBuilder("");
    for (int iCtr = 1; iCtr < (propMap.size() + 1); iCtr++) {
      strEvalXPath = strEvalXPath.append(propMap.get(String.valueOf(iCtr)));
      LOGGER.verbose("evaluateCondition:: strEvalXPath:: " + strEvalXPath.toString());
    }
    NodeList orderHoldElementList;
    try {
      orderHoldElementList = XPathAPI.selectNodeList(inDoc, strEvalXPath.toString());
      if (orderHoldElementList.getLength() > 0) {
        bRetVal = true;
        LOGGER.verbose("evaluateCondition:: bRetVal:: " + bRetVal);
        return bRetVal;
      }
      LOGGER.verbose("evaluateCondition:: bRetVal:: " + bRetVal);
    } catch (TransformerException e) {
      YFSException ex = new YFSException();
      LOGGER.verbose("Class: GCIsVertexDownHoldApplied Method: evaluateCondition END");
      LOGGER.endTimer("GCIsVertexDownHoldApplied.evaluateCondition");
      LOGGER.error("Class: GCIsVertexDownHoldApplied Method: evaluateCondition END", e);
      throw ex;
    }
    LOGGER.verbose("Class: GCIsVertexDownHoldApplied Method: evaluateCondition END");
    LOGGER.endTimer("GCIsVertexDownHoldApplied.evaluateCondition");
    return bRetVal;
  }

  @Override
  public void setProperties(Map props) {
    propMap = props;
  }

}
