package com.gc.condition;

import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.utils.GCXMLUtil;
import com.yantra.ycp.japi.YCPDynamicConditionEx;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class is used to check whether the chained order contains only Warranty Item if yes then
 * there will be no orderLine in the document as GCChainedOrderCreateOnSuccess class removes the
 * warranty item from the document.
 *
 * @author shanil.sharma
 *
 */
public class GCIsWarrantyChainedOrder implements YCPDynamicConditionEx {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCIsVertexDownHoldApplied.class);

  @Override
  public boolean evaluateCondition(YFSEnvironment arg0, String arg1, Map arg2, Document inDoc) {

    LOGGER.beginTimer("GCIsWarrantyChainedOrder.evaluateCondition");
    LOGGER.verbose("Class: GCIsWarrantyChainedOrder Method: evaluateCondition START");
    List<Element> nlOrderLine =
        GCXMLUtil.getElementListByXpath(inDoc, "Order/ChainedOrderList/Order/OrderLines/OrderLine");
    int lengthOdrLine = nlOrderLine.size();
    if (lengthOdrLine > 0) {

      LOGGER.verbose("Class: GCIsWarrantyChainedOrder Method: evaluateCondition END");
      LOGGER.endTimer("GCIsWarrantyChainedOrder.evaluateCondition");
      return false;
    } else {

      LOGGER.verbose("Class: GCIsWarrantyChainedOrder Method: evaluateCondition END");
      LOGGER.endTimer("GCIsWarrantyChainedOrder.evaluateCondition");
      return true;
    }

  }

  @Override
  public void setProperties(Map arg0) {

  }

}
