/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Checks whether the Payment Hold should be applied or not
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               07/25/2014        Soni, Karan		This class applies Payment Hold on the basis of various conditions
 *#################################################################################################################################################################
 */
package com.gc.condition;

import java.util.Map;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCPublishSETParent {

  //Initialize Logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCPublishSETParent.class);

  public Document checkIfSETParent(YFSEnvironment env, Document inDoc) {

	  boolean publishDoc = false;
	  YFCDocument yfcDoc = YFCDocument.getDocumentFor(inDoc);
	  YFCElement eleRoot = yfcDoc.getDocumentElement();
	  String sItemID = eleRoot.getAttribute(GCConstants.ITEM_ID);
	  String sProductClass = eleRoot.getAttribute(GCConstants.PRODUCT_CLASS);
	  if(YFCCommon.equals(sProductClass, GCConstants.PRODUCT_CLASS_SET)){
		  YFCDocument getComponentItemListIn = YFCDocument.getDocumentFor("<Item ItemID='"+sItemID+"' IgnoreIsSoldSeparately='Y'/>");
		  YFCDocument getComponentItemListTemp = YFCDocument.getDocumentFor("<ItemList><Item><Extn ExtnSetCode=''/></Item></ItemList>");
		  YFCDocument getComponentItemListOut = GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ITEM_LIST, getComponentItemListIn, getComponentItemListTemp);

		  YFCElement eleItemList = getComponentItemListOut.getDocumentElement();
		  YFCElement eleItem =  eleItemList.getElementsByTagName(GCConstants.ITEM).item(0);
		  YFCElement eleExtn = eleItem.getChildElement(GCConstants.EXTN);
		  String sExtnSetCode = eleExtn.getAttribute(GCConstants.EXTN_SET_CODE);
		  if(YFCCommon.equals(sExtnSetCode, GCConstants.TWO)){
			  YFCDocument getParentItemListIn = YFCDocument.getDocumentFor("<Item IgnoreIsSoldSeparately='Y'><Extn ExtnPrimaryComponentItemID='"+sItemID+"'/></Item>");
			  YFCDocument getParentItemListTemp = YFCDocument.getDocumentFor("<ItemList><Item ItemID=''/></ItemList>");
			  YFCDocument getParentItemListOut = GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ITEM_LIST, getParentItemListIn, getParentItemListTemp);
			  YFCElement eleBundleParentRoot = getParentItemListOut.getDocumentElement();
			  if(eleBundleParentRoot.hasChildNodes()){
				  YFCElement eleParentItem = eleBundleParentRoot.getElementsByTagName(GCConstants.ITEM).item(0);
				  String sParentItemID =  eleParentItem.getAttribute(GCConstants.ITEM_ID);
				  eleRoot.setAttribute(GCConstants.ITEM_ID, sParentItemID);
				  publishDoc = true;
			  }		  		  
		  } else {
			  publishDoc = true;
		  }
	  }else {
		  publishDoc = true;
	  } 
	  
	  
	  if(publishDoc){
		  return yfcDoc.getDocument();
	  } else {
		  return GCXMLUtil.getDocument("<BlankDoc DoNotPublish='Y'/>");
	  }
  }
}

