/**
 * Copyright 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Conditions to send email
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
 *          1.0                06/05/2014        Susai, John Melvin			Initial Version
 *#################################################################################################################################################################
 */
package com.gc.condition;

import com.gc.common.constants.GCConstants;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="john.melvin@expicient.com">John Melvin</a>
 */
public class GCSendEmailConditions {

  //initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCSendEmailConditions.class);

  private GCSendEmailConditions() {
  }

  /**
   *
   * Description of isJBSDCSourced
   *
   * @param env
   * @param orderCaptureChannel
   * @return
   * @throws Exception
   *
   */
  public static boolean isJBSDCSourced(YFSEnvironment env, String orderCaptureChannel) {
    if (YFCUtils.equals(GCConstants.JBS_WEB_ORDER_CHANNEL, orderCaptureChannel)
        || YFCUtils.equals(GCConstants.JBS_STORE_ORDER_CHANNEL, orderCaptureChannel)
        || YFCUtils.equals("GCSTORE", orderCaptureChannel)) {
      LOGGER.debug("GCSendEmailConditions: isJBSSourcedOrder");
      return true;
    }
    return false;
  }

  public static boolean isCallCenterOrder(YFSEnvironment env, String orderCaptureChannel){
    if (YFCUtils.equals(GCConstants.CALL_CENTER_ORDER_CHANNEL, orderCaptureChannel)) {
      LOGGER.debug("GCSendEmailConditions: CallCenterOrder");
      return true;
    }
    return false;
  }

  public static boolean isOrderFTCExempt(YFSEnvironment env, String extnFTCExempt) {
    if ("Y".equalsIgnoreCase(extnFTCExempt)) {
      LOGGER.debug("GCSendEmailConditions: Order is FTCExempt");
      return true;
    }
    return false;
  }

  public static boolean isStoreReturnOrder(YFSEnvironment env, String orderType) {
    if ("STORE_RETURN".equalsIgnoreCase(orderType)) {
      LOGGER.debug("GCSendEmailConditions: Store Returned Order");
      return true;
    }
    return false;
  }

}
