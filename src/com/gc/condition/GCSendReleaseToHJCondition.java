package com.gc.condition;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.ycp.japi.YCPDynamicConditionEx;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCSendReleaseToHJCondition implements YCPDynamicConditionEx{
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCSendReleaseToHJCondition.class.getName());

  @Override
  public boolean evaluateCondition(YFSEnvironment env, String arg1,
      Map arg2, Document inDoc){

    LOGGER.debug(" Entering GCSendReleaseToHJCondition::evaluateCondition() method, Input document is"
        + GCXMLUtil.getXMLString(inDoc));
    boolean returnFlag=true;
    try {
      Element eleOrderRelease=(Element)XPathAPI.selectSingleNode(inDoc,"OrderRelease");
      String sReleaseNo=eleOrderRelease.getAttribute("ReleaseNo");
      String sSalesOrderNo=eleOrderRelease.getAttribute("SalesOrderNo");

      Document getOrderReleaseListTemplateDoc = GCXMLUtil.getDocument("<OrderReleaseList><OrderRelease  ReleaseNo='' OrderReleaseKey=''>"
          + "<OrderLines><OrderLine PrimeLineNo='' OrderLineKey='' DependentOnLineKey=''>"
          +"<Extn ExtnIsWarrantyItem='' ExtnNonHJOrderLine='' ExtnWarrantyParentLineNumber=''/>"
          +"</OrderLine></OrderLines></OrderRelease></OrderReleaseList>");
      Document getOrderReleaseListInputDoc=GCXMLUtil.createDocument(GCConstants.ORDER_RELEASE);
      Element eOrderRelease=getOrderReleaseListInputDoc.getDocumentElement();
      eOrderRelease.setAttribute(GCConstants.RELEASE_NO, sReleaseNo);
      eOrderRelease.setAttribute(GCConstants.SALES_ORDER_NO, sSalesOrderNo);
      Document getOrderReleaseListOutputDoc=GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_RELEASE_LIST, getOrderReleaseListInputDoc,getOrderReleaseListTemplateDoc);
      NodeList nlOrderLine=XPathAPI.selectNodeList(getOrderReleaseListOutputDoc, "OrderReleaseList/OrderRelease/OrderLines/OrderLine");
      int orderLineCount=nlOrderLine.getLength();
      List<String> odrLineKeyList = new ArrayList<String>();
      for(int i=0;i<orderLineCount;i++){
        Element eleOrderLine=(Element)nlOrderLine.item(i);
        odrLineKeyList.add(eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
        Element eleExtn = (Element) XPathAPI.selectSingleNode(
            eleOrderLine, GCConstants.EXTN);
        String sExtnIsWarrantyItem=eleExtn.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
        if("N".equals(sExtnIsWarrantyItem)){
          returnFlag=false;
        }
      }

      returnFlag = returnFlag || checkForWarrantyHold(env, sSalesOrderNo, odrLineKeyList);
    }catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCSendReleaseToHJCondition.evaluateCondition():",
          e);
      throw new GCException(e).getYFSException();
    }
    LOGGER.debug(" Exiting GCSendReleaseToHJCondition::evaluateCondition() method, returning:" + returnFlag);
    return returnFlag;
  }

  /**
   * This method is used to check whether the orderLines in the release have their dependent
   * warranty still on hold.
   *
   * @param env
   * @param sSalesOrderNo
   * @param odrLineKeyList
   * @return
   */
  private boolean checkForWarrantyHold(YFSEnvironment env, String sSalesOrderNo, List<String> odrLineKeyList) {

    LOGGER.beginTimer("GCSendReleaseToHJCondition.checkForWarrantyHold");
    LOGGER.verbose("GCSendReleaseToHJCondition.checkForWarrantyHold--Begin");
    YFCDocument docGetOrderListIP = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleOrderGetOrderList = docGetOrderListIP.getDocumentElement();
    eleOrderGetOrderList.setAttribute(GCConstants.ORDER_NO, sSalesOrderNo);
    boolean isWarrantyHold = false;
    YFCDocument docGetOrderListIPTemplate =
        YFCDocument.getDocumentFor("<OrderList><Order OrderNo='' OrderHeaderKey=''>"
            + "<OrderLines><OrderLine KitCode='' FulfillmentType='' DependentOnLineKey='' OrderLineKey=''>"
            + "<Extn ExtnIsWarrantyItem='' ExtnWarrantyLineNumber='' ExtnIsShipAlone='' ExtnIsFreeGiftItem=''/>"
            + "<OrderHoldTypes><OrderHoldType HoldType='' Status=''/></OrderHoldTypes>"
            + "<BundleParentLine OrderLineKey=''/>" + "</OrderLine></OrderLines></Order></OrderList>");

    YFCDocument docGetOrderListOp =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, docGetOrderListIP, docGetOrderListIPTemplate);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("GetOrderList Output document is" + docGetOrderListOp.toString());
    }

    YFCElement eleOrderList = docGetOrderListOp.getDocumentElement();
    YFCElement eleOrder = eleOrderList.getChildElement(GCConstants.ORDER);
    YFCElement eleOrderLines = eleOrder.getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> nlOrderLine = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);

    // Prepare a list for ShipAlone OrderLineKey
    List<String> shipAloneLineKeyList = new ArrayList<String>();
    updatedListForBundles(nlOrderLine, odrLineKeyList, shipAloneLineKeyList);

    for (YFCElement eleOrderLine : nlOrderLine) {
      String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      YFCElement eleOrderLineExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
      if (!YFCCommon.isVoid(eleOrderLineExtn.getAttribute(GCConstants.EXTN_IS_SHIP_ALONE))
          && !shipAloneLineKeyList.contains(sOrderLineKey)) {
        shipAloneLineKeyList.add(sOrderLineKey);
      }
    }

    for (YFCElement eleOrderLine : nlOrderLine) {
      YFCElement eleOrderLineExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
      String sExtnIsWarrantyItem = eleOrderLineExtn.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
      YFCElement eleOrderLineHoldTypes = eleOrderLine.getChildElement(GCConstants.ORDER_HOLD_TYPES);
      if (!YFCCommon.isVoid(eleOrderLineHoldTypes)
          && (YFCCommon.equalsIgnoreCase(sExtnIsWarrantyItem, GCConstants.YES))
          && odrLineKeyList.contains(eleOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY))) {
        YFCNodeList<YFCElement> nlOrderLineHoldType =
            eleOrderLineHoldTypes.getElementsByTagName(GCConstants.ORDER_HOLD_TYPE);
        for (YFCElement eleOrderLineHoldType : nlOrderLineHoldType) {
          String sHoldType = eleOrderLineHoldType.getAttribute(GCConstants.HOLD_TYPE);
          String sStatus = eleOrderLineHoldType.getAttribute(GCConstants.STATUS);
          if (YFCCommon.equalsIgnoreCase(sHoldType, "DEPENDENT_LINE_HOLD")
              && YFCCommon.equalsIgnoreCase(sStatus, "1100")
              && !shipAloneLineKeyList.contains(eleOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY))) {
            isWarrantyHold = true;
          }
        }
      }
    }
    LOGGER.verbose("isWarrantyHold : " + isWarrantyHold);
    LOGGER.verbose("GCSendReleaseToHJCondition.checkForWarrantyHold--End");
    LOGGER.endTimer("GCSendReleaseToHJCondition.checkForWarrantyHold");
    return isWarrantyHold;
  }

  /**
   * This method is used to update the odrLineKeyList with the bundle parent OrderLineKey if its
   * components are part of current release.
   *
   * @param nlOrderLine
   * @param odrLineKeyList
   * @param shipAloneLineKeyList
   */
  private void updatedListForBundles(YFCNodeList<YFCElement> nlOrderLine, List<String> odrLineKeyList,
      List<String> shipAloneLineKeyList) {

    LOGGER.beginTimer("GCSendReleaseToHJCondition.updatedListForBundles");
    LOGGER.verbose("GCSendReleaseToHJCondition.updatedListForBundles--Begin");
    for (YFCElement eleOrderLine : nlOrderLine) {
      String orderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);

      YFCElement eleBundleParent = eleOrderLine.getChildElement(GCConstants.BUNDLE_PARENT_LINE);
      YFCElement eleOrderLineExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
      if (odrLineKeyList.contains(orderLineKey)) {
        if (!YFCCommon.isVoid(eleBundleParent)
            && !odrLineKeyList.contains(eleBundleParent.getAttribute(GCConstants.ORDER_LINE_KEY))) {
          odrLineKeyList.add(eleBundleParent.getAttribute(GCConstants.ORDER_LINE_KEY));
        }
      }
      // adding bundleParentLineKey of shipAloneComponents to shipAloneLineKeyList
      if (!YFCCommon.isVoid(eleBundleParent)
          && !YFCCommon.isVoid(eleOrderLineExtn.getAttribute(GCConstants.EXTN_IS_SHIP_ALONE))
          && !shipAloneLineKeyList.contains(eleBundleParent.getAttribute(GCConstants.ORDER_LINE_KEY))) {
        shipAloneLineKeyList.add(eleBundleParent.getAttribute(GCConstants.ORDER_LINE_KEY));
      }
    }
    LOGGER.verbose("final odrLineKeyList : " + odrLineKeyList);
    LOGGER.verbose("GCSendReleaseToHJCondition.updatedListForBundles--End");
    LOGGER.endTimer("GCSendReleaseToHJCondition.updatedListForBundles");
  }

  @Override
  public void setProperties(Map arg0) {


  }



}
