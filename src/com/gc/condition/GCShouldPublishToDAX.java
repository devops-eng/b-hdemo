package com.gc.condition;

import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.api.GCOnCancelPublish;
import com.gc.common.constants.GCConstants;
import com.yantra.ycp.japi.YCPDynamicConditionEx;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCShouldPublishToDAX implements YCPDynamicConditionEx {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCShouldPublishToDAX.class.getName());

  @Override
  public boolean evaluateCondition(YFSEnvironment env, String arg1, Map arg2, Document arg3) {
    LOGGER.beginTimer("GCShouldPublishToDAX :: evaluateCondition :: begin");
    
    Element eleRootOrder = arg3.getDocumentElement();
    String sDraftOrderFlag = eleRootOrder.getAttribute(GCConstants.DRAFT_ORDER_FLAG);
    if(YFCCommon.equals(sDraftOrderFlag, GCConstants.YES)){
    	return false;
    }
    Object obPublishFlag = env.getTxnObject("DoNotPublishToDAX");
    String sPublishFlag = "";
    if (!YFCCommon.isVoid(obPublishFlag)) {
      sPublishFlag = (String) obPublishFlag;
    }
    LOGGER.verbose("sPublishFlag" + sPublishFlag);
    if (!YFCCommon.equals(sPublishFlag, "Y")) {
      return true;
    }
    return false;
  }

  @Override
  public void setProperties(Map arg0) {

  }

}
