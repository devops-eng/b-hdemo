/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *##############################################################################################################################################################################
 *	        OBJECTIVE: This class is invoked on Change order on success and validate Under priced hold needs to be applied on order or not when unit price is overrided
 *################################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *###########################################################################################################################################################################################
             1.0               12/08/2014        Soni, Karan		         OMS-4262: orders are not going to under price hold when price of the item is overriden below the sale price
 *############################################################################################################################################################################################
 */
package com.gc.eventhandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCChangeOrderOnSuccess implements YIFCustomApi{

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCChangeOrderOnSuccess.class);


  /**
   *
   * Description of validateUnderpricedHold Check if InventoryCost of item >
   * UnitPrice, then apply UnderPriced hold on order
   *
   * @param env
   * @param inDoc
   * @throws GCException
   *
   */
  public void validateUnderpricedHold(YFSEnvironment env, Document inDoc)
      throws GCException {
    LOGGER.debug(" Entering GCProcessOrderOnSuccess.validateUnderpricedHold() method ");
    try {
      String strOHK = inDoc.getDocumentElement().getAttribute(
          GCConstants.ORDER_HEADER_KEY);
      Document getOrderListOp = GCCommonUtil
          .invokeGetOrderListAPITemp(env, strOHK,
              GCConstants.GC_CC_AUTH_GET_ORDER_LIST_TEMPLATE);
      Document getOrderListOpDoc = GCXMLUtil
          .getDocumentFromElement((Element) getOrderListOp
              .getDocumentElement().getFirstChild());

      NodeList nlEligibleOrderLines = XPathAPI.selectNodeList(inDoc,
          "Order/OrderAudit/OrderAuditLevels/OrderAuditLevel[@ModificationLevel='ORDER_LINE']/OrderAuditDetails/OrderAuditDetail[@AuditType='OrderLine']/Attributes/Attribute[@ModificationType='PRICE' and @Name='UnitPrice' and @NewValue<@OldValue]/../../../..");
      List<String> validOLsList = new ArrayList<String>();
      for (int i = 0; i < nlEligibleOrderLines.getLength(); i++) {
        Element eleOL = (Element) nlEligibleOrderLines.item(i);
        String sOLKey = eleOL
            .getAttribute(GCConstants.ORDER_LINE_KEY);
        validOLsList.add(sOLKey);
      }

      NodeList nlOrderLine = XPathAPI.selectNodeList(
          getOrderListOpDoc, "Order/OrderLines/OrderLine");
      for (int counter = 0; counter < nlOrderLine.getLength(); counter++) {

        Element eleOrderLine = (Element) nlOrderLine.item(counter);
        String sOrderLineKey = eleOrderLine
            .getAttribute(GCConstants.ORDER_LINE_KEY);

        String sExtnIsFreeGiftItem = GCXMLUtil
            .getAttributeFromXPath(eleOrderLine,
                "Extn/@ExtnIsFreeGiftItem");
        Element eleBundleParentLine = (Element) XPathAPI
            .selectSingleNode(eleOrderLine, "BundleParentLine");
        if (validOLsList.contains(sOrderLineKey)
            && YFCCommon.isVoid(eleBundleParentLine)
            && !GCConstants.YES
            .equalsIgnoreCase(sExtnIsFreeGiftItem)) {
          String sUnitPrice = GCXMLUtil.getAttributeFromXPath(
              eleOrderLine, "LinePriceInfo/@UnitPrice");
          Double dUnitPrice = Double.valueOf(sUnitPrice);

          String sItemID = GCXMLUtil.getAttributeFromXPath(
              eleOrderLine, "Item/@ItemID");
          String sUOM = GCXMLUtil.getAttributeFromXPath(
              eleOrderLine, "Item/@UnitOfMeasure");

          Document docGetItemListOP = GCCommonUtil.getItemList(
              env, sItemID, sUOM, GCConstants.GCI);

          String sExtnInventoryCost = GCXMLUtil
              .getAttributeFromXPath(docGetItemListOP,
                  "ItemList/Item/Extn/@ExtnInventoryCost");
          double dExtnInventoryCost = (!YFCCommon
              .isVoid(sExtnInventoryCost)) ? Double
                  .valueOf(sExtnInventoryCost) : 0.00;
                  LOGGER.debug(" dUnitPrice is " + dUnitPrice
                      + " dExtnInventoryCost is "
                      + dExtnInventoryCost);
                  if ((dExtnInventoryCost) > dUnitPrice) {
                    LOGGER.debug(" Inside if(dExtnInventoryCost > dUnitPrice) ");
                    Document docChangeOrder = GCXMLUtil
                        .createDocument(GCConstants.ORDER);
                    Element eleRootOfChangeOrder = docChangeOrder
                        .getDocumentElement();
                    eleRootOfChangeOrder.setAttribute(
                        GCConstants.ORDER_HEADER_KEY,
                        inDoc.getDocumentElement().getAttribute(
                            GCConstants.ORDER_HEADER_KEY));
                    eleRootOfChangeOrder.setAttribute(GCConstants.OVERRIDE,
                        GCConstants.YES);

                    Element eleOrderHoldTypes = GCXMLUtil.createElement(
                        docChangeOrder, GCConstants.ORDER_HOLD_TYPES,
                        null);
                    eleRootOfChangeOrder.appendChild(eleOrderHoldTypes);
                    Element eleOrderHoldType = GCXMLUtil.createElement(
                        docChangeOrder, GCConstants.ORDER_HOLD_TYPE,
                        null);
                    eleOrderHoldType.setAttribute(GCConstants.HOLD_TYPE,
                        GCConstants.UNDERPRICED_HOLD);
                    eleOrderHoldType.setAttribute(GCConstants.STATUS,
                        GCConstants.APPLY_HOLD_CODE);
                    eleOrderHoldTypes.appendChild(eleOrderHoldType);
                    if (LOGGER.isDebugEnabled()) {
                      LOGGER.debug(" docChangeOrder is"
                          + GCXMLUtil.getXMLString(docChangeOrder));
                    }
                    GCCommonUtil.invokeAPI(env,
                        GCConstants.API_CHANGE_ORDER, docChangeOrder);
                  }
        }
      }

    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCProcessOrderOnSuccess.validateUnderpricedHold(), Exception is :",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCProcessOrderOnSuccess.validateUnderpricedHold() method ");
  }

  @Override
  public void setProperties(Properties arg0) {

  }

}
