/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################################
 *          OBJECTIVE: Contains the logic of processing the on success event of confirmShipment api
 *###################################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *################################################################################################################################################################################
 *            1.0           16-June-2015        Infosys Limited           Save the shipment key in the yfs environment to be used by
 *                                                                         implementation of ON_ORDER_RELEASE_STATUS_CHANGE
 *##################################################################################################################################################################################
 */
package com.gc.eventhandler;

import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * <h3>Description :</h3>This java class is invoked by the GCConfirmShipment_OnSuccess_svc service configured on the onSuccess event of the confirmShipment
 *
 * @author Infosys Limited
 */
public class GCConfirmShipmentOnSuccess implements YIFCustomApi {

  /*
   * The class name
   */
  private static final String CLASS_NAME = GCConfirmShipmentOnSuccess.class.getName();

  /*
   * The logger reference
   */
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(CLASS_NAME);

  /*
   * The configurations for the service
   */
  private Properties props;

  /* (non-Javadoc)
   * @see com.yantra.interop.japi.YIFCustomApi#setProperties(java.util.Properties)
   */
  @Override
  public void setProperties(final Properties props) {

    this.props = props;
  }


  /**
   * <h3>Description:</h3>This method is used to handle the on success event of confirmShipment api
   *
   * @param yfsEnv The yfs environment reference
   * @param docInput The input document
   * @return The input document
   */
  public Document handleOnSuccessEvent(final YFSEnvironment yfsEnv, final Document docInput) {

    // log the method entry
    LOGGER.info(CLASS_NAME + ":handleOnSuccessEvent:entry");

    // log the method input
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASS_NAME + ":handleOnSuccessEvent:Input=" + GCXMLUtil.getXMLString(docInput));
    }

    // fetch the shipment key from the input
    final Element eleDocInputRt = docInput.getDocumentElement();
    final String strShipmentKey = eleDocInputRt.getAttribute(GCConstants.SHIPMENT_KEY);


    // logging the shipment key
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASS_NAME + ":handleOnSuccessEvent:The shipment key to be saved in the yfs environment="
          + strShipmentKey);
    }

    // set the shipment key in the environment
    yfsEnv.setTxnObject(GCConstants.CURRENT_TRANSACTION_SHIPMENT_KEY, strShipmentKey);
    
    //GCSTORE-4795 Start
    String sOrderHeaderKey = GCXMLUtil.getAttributeFromXPath(docInput, "Shipment/ShipmentLines/ShipmentLine/@OrderHeaderKey");
    String sShipNode = eleDocInputRt.getAttribute(GCConstants.SHIP_NODE);
    
    if(!YFCCommon.isStringVoid(sOrderHeaderKey) && !YFCCommon.equalsIgnoreCase(sShipNode, GCConstants.MFI)){
      YFCDocument getOrderListInDoc = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderHeaderKey+"'/>");
      YFCDocument getOrderListTemp = YFCDocument.getDocumentFor("<OrderList><Order OrderHeaderKey='' OrderNo=''>"
          + "<OrderHoldTypes><OrderHoldType HoldType='' Status=''/></OrderHoldTypes></Order></OrderList>");
      YFCDocument getOrderListOutDoc = GCCommonUtil.invokeAPI(yfsEnv, GCConstants.GET_ORDER_LIST, getOrderListInDoc, getOrderListTemp);
      YFCElement eleRoot = getOrderListOutDoc.getDocumentElement();
      YFCElement eleOrder = eleRoot.getChildElement(GCConstants.ORDER);
      if(!YFCCommon.isVoid(eleOrder)){
        YFCElement eleOrderHoldTypes = eleOrder.getChildElement(GCConstants.ORDER_HOLD_TYPES);
        YFCNodeList<YFCElement> nlOrderHoldType = eleOrderHoldTypes.getElementsByTagName(GCConstants.ORDER_HOLD_TYPE);
        for (YFCElement eleOrderHoldType : nlOrderHoldType){
          String sHoldType = eleOrderHoldType.getAttribute(GCConstants.HOLD_TYPE);
          String sStatus = eleOrderHoldType.getAttribute(GCConstants.STATUS);
          if(YFCCommon.equalsIgnoreCase(sHoldType, "REAUTH_FAILED_HOLD") && YFCCommon.equalsIgnoreCase(sStatus, "1100")){
            LOGGER.debug("Order is on REAUTH_FAILED_HOLD.");
            YFCException yfce = new YFCException("EXTN_REAUTHFAIL");
            yfce.setErrorDescription("Order fulfillment is prevented when order is on hold type REAUTH_FAILED_HOLD.");
            throw yfce;            
          }
        }
      }
    }
    //GCSTORE-4795 End
    
    // log the method exit
    LOGGER.info(CLASS_NAME + ":handleOnSuccessEvent:exit");

    // return the input document
    return docInput;
  }
}