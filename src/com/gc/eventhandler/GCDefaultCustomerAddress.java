package com.gc.eventhandler;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCDefaultCustomerAddress {

	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCDefaultCustomerAddress.class.getName());

	public void defaultCustomerAddress(YFSEnvironment env, Document inDoc)
			throws GCException, TransformerException {

		final String logInfo = "GCDefaultCustomerAddress :: defaultCustomerAddress :: ";
		LOGGER.debug(logInfo + "Begin");
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(logInfo + "Input Document"
					+ GCXMLUtil.getXMLString(inDoc));
		}

		Element eleCustomerAdditionalAddressIp = (Element) XPathAPI.selectSingleNode(inDoc, "Customer/CustomerContactList/CustomerContact/CustomerAdditionalAddressList/CustomerAdditionalAddress[@IsDefaultBillTo='Y']"); 
		if (!GCConstants.NO.equals(env.getTxnObject("CallManageCustomerAPI"))&& YFCCommon.isVoid(eleCustomerAdditionalAddressIp)) {

			try{
				if (!GCConstants.NO.equals(env.getTxnObject("CallManageCustomerAPI"))) {

					Document docManageCustomerIp = GCXMLUtil
							.createDocument(GCConstants.CUSTOMER);
					Element eleCustomer = docManageCustomerIp.getDocumentElement();
					eleCustomer.setAttribute(GCConstants.CUSTOMER_ID, GCXMLUtil
							.getAttributeFromXPath(inDoc, "Customer/@CustomerID"));
					eleCustomer
					.setAttribute(GCConstants.ORGANIZATION_CODE, GCXMLUtil
							.getAttributeFromXPath(inDoc,
									"Customer/@OrganizationCode"));

					Element eleCustomerContactList = docManageCustomerIp
							.createElement(GCConstants.CUSTOMER_CONTACT_LIST);
					eleCustomer.appendChild(eleCustomerContactList);

					Element eleCustomerContact = docManageCustomerIp
							.createElement(GCConstants.CUSTOMER_CONTACT);
					eleCustomerContactList.appendChild(eleCustomerContact);
					eleCustomerContact
					.setAttribute(
							GCConstants.CUSTOMER_CONTACT_ID,
							GCXMLUtil
							.getAttributeFromXPath(inDoc,
									"Customer/CustomerContactList/CustomerContact/@CustomerContactID"));

					Element eleCustomerAdditionalAddressList = docManageCustomerIp
							.createElement(GCConstants.CUSTOMER_ADDITIONAL_ADDRESS_LIST);
					eleCustomerContact.appendChild(eleCustomerAdditionalAddressList);

					Element eleCustomerAdditionalAddress = docManageCustomerIp
							.createElement(GCConstants.CUSTOMER_ADDITIONAL_ADDRESS);
					eleCustomerAdditionalAddressList
					.appendChild(eleCustomerAdditionalAddress);
					eleCustomerAdditionalAddress
					.setAttribute(
							GCConstants.CUSTOMER_ADDITIONAL_ADDRESS_ID,
							GCXMLUtil
							.getAttributeFromXPath(
									inDoc,
									"Customer/CustomerContactList/CustomerContact/CustomerAdditionalAddressList/CustomerAdditionalAddress/@CustomerAdditionalAddressID"));
					eleCustomerAdditionalAddress.setAttribute(
							GCConstants.IS_DEFAULT_BILLTO, GCConstants.YES);
					//GCSTORE-6957 Start
					eleCustomerAdditionalAddress.setAttribute(GCConstants.IS_BILLTO, GCConstants.YES);
					//GCSTORE-6957 End
					eleCustomer.appendChild(eleCustomerContactList);
					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug(logInfo + "Manage customer Ip Doc"
								+ GCXMLUtil.getXMLString(docManageCustomerIp));
					}
					GCCommonUtil.invokeAPI(env, GCConstants.MANAGE_CUSTOMER_API,
							docManageCustomerIp);

					env.setTxnObject("CallManageCustomerAPI", GCConstants.NO);
				}

				LOGGER.debug(logInfo + "End");
			} catch (Exception e) {
				LOGGER.error("Inside catch block of GCDefaultCustomerAddress.defaultCustomerAddress method. Exception is", e);
				throw new GCException(e);
			}
		}
	}
}
