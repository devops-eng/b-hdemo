
/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#############################################################################################################################################
 *          OBJECTIVE: Update OrderHeaderKey and ChargeTransactionKey in GC_PAYPAL_REFUND_DIST table for imported orders
 *#################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################
             1.0               19/08/2015       Jethuri Bhawana             Update OrderHeaderKey and ChargeTransactionKey in GC_PAYPAL_REFUND_DIST
                                                                            table for imported orders

 *#################################################################################################################################################

 */
package com.gc.eventhandler;

import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author Gurpreet
 */
public class GCProcessImportOrderOnSuccess implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCProcessImportOrderOnSuccess.class);

  /**
   *
   * Description of processOrderOnSuccess Processes Order On Success
   *
   * @param env
   * @param inDoc
   * @return inDoc
   * @throws GCException
   *
   */
  public void processImportOrderOnSuccess(YFSEnvironment env, Document inDoc)
      throws GCException {
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleInput = inputDoc.getDocumentElement();
    String orderHeaderKey = eleInput.getAttribute(GCConstants.ORDER_HEADER_KEY);
    YFCElement eleExtn = eleInput.getChildElement(GCConstants.EXTN);
    YFCElement eleGCRefndDistList = eleExtn.getChildElement("GCPaypalRefundDistList");
    YFCNodeList<YFCElement> nlGCRefndDistList = eleGCRefndDistList.getElementsByTagName("GCPaypalRefundDist");
    YFCElement eleChargeTranDetails = eleInput.getChildElement("ChargeTransactionDetails");
    YFCNodeList<YFCElement> nlChargeTranDetails = eleChargeTranDetails.getElementsByTagName("ChargeTransactionDetail");

    for (YFCElement eleGCRefndDist : nlGCRefndDistList) {
      String chargeTranId = eleGCRefndDist.getAttribute("ChargeTransactionID");
      String key = eleGCRefndDist.getAttribute("GCPaypalRefundDistributionKey");
      for (YFCElement eleChargeTranDetail : nlChargeTranDetails) {
        String authId = eleChargeTranDetail.getAttribute("AuthorizationID");
        if (YFCUtils.equals(chargeTranId, authId)) {
          String chargeTranKey = eleChargeTranDetail.getAttribute(GCConstants.CHARGE_TRANSACTION_KEY);
          YFCDocument docUpdatePaypalRefTable = YFCDocument.createDocument(GCConstants.GC_PAYPAL_REFUND_DIST);
          YFCElement eleIpDoc = docUpdatePaypalRefTable.getDocumentElement();
          eleIpDoc.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
          eleIpDoc.setAttribute(GCConstants.CHARGE_TRANSACTION_KEY, chargeTranKey);
          eleIpDoc.setAttribute("GCPaypalRefundDistributionKey", key);
          GCCommonUtil.invokeService(env, GCConstants.GC_CHANGE_PP_REFUND_DIST_SERVICE, docUpdatePaypalRefTable);
          break;

        }
      }

    }
  }

  @Override
  public void setProperties(Properties arg0) throws Exception {
    // TODO Auto-generated method stub

  }
}
