/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#############################################################################################################################################
 *	        OBJECTIVE: Process the Bundle OrderLines and prorate its charges on component lines
 *#################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################
             1.0               12/02/2014        Singh Gurpreet		        OMS-1139: Development - Dependent Line Associations - External Orders
 *#################################################################################################################################################
 *           1.1               02/26/2015        Rajiv Sinha		        GCSTORE-332: Development - Stamping PrePaid as sourcing classification for PrePaid order
 *#################################################################################################################################################
 *           1.2               04/07/2015        Zuzar Singh		        GCSTORE-1937: Defect Fix - Stamping INFINV for serial item on Draft confirmation
 */
package com.gc.eventhandler;

import java.util.Map;
import java.util.Properties;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCLogUtils;
import com.gc.common.utils.GCOrderUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author Gurpreet
 */
public class GCProcessOrderOnSuccess implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCProcessOrderOnSuccess.class);

  /**
   *
   * Description of processOrderOnSuccess Processes Order On Success
   *
   * @param env
   * @param inDoc
   * @return inDoc
   * @throws GCException
   *
   */
  public Document processOrderOnSuccess(YFSEnvironment env, Document inDoc)
      throws GCException {
    LOGGER.debug(" Entering GCProcessOrderOnSuccess.processOrderOnSuccess() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" processOrderOnSuccess() method, Input document is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    try {
      String sExtnOrderCaptureChannel = "";
      String sExtnSourceSystem = "";
      YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
      GCCommonUtil.setPhase(env, inDoc);
      YFCElement eExtn = yfcInDoc.getDocumentElement().getChildElement(
          GCConstants.EXTN);
      if (!YFCCommon.isVoid(eExtn)) {
        sExtnOrderCaptureChannel = eExtn
            .getAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL);
        sExtnSourceSystem = eExtn.getAttribute(GCXmlLiterals.EXTN_SOURCE_SYSTEM);

      }
      boolean isSerialBundleMapEmpty = true;
      if (YFCCommon.equals(GCConstants.ORDER_CATURE_SYSTEM_ATG, sExtnSourceSystem)) {
    
        Map<String, String> mapPrimeLineNoNBatchNoForSerialBundle =
            (Map<String, String>) env.getTxnObject(GCConstants.INV_REQ_ATT_FOR_SERIAL_BUNDLE);
        if (LOGGER.isDebugEnabled() && !YFCCommon.isVoid(mapPrimeLineNoNBatchNoForSerialBundle)) {
          LOGGER.debug("mapPrimeLineNoNBatchNoForSerialBundle" + mapPrimeLineNoNBatchNoForSerialBundle.toString());
        }
        isSerialBundleMapEmpty = mapPrimeLineNoNBatchNoForSerialBundle.isEmpty();
        if (!isSerialBundleMapEmpty) {
          inDoc = updateInvAttributeForCompSerialBundle(env, inDoc, mapPrimeLineNoNBatchNoForSerialBundle);
          GCLogUtils.verboseLog(LOGGER, "inDoc after updating InvAttributesForSerialBundleItem : ", inDoc);
        }
      }

      boolean callChangeOrderForPriceHold = false;
      YFCDocument docChangeOrder = YFCDocument
          .createDocument(GCConstants.ORDER);
      YFCElement chOrderRootEle = docChangeOrder.getDocumentElement();
      if (!isSerialBundleMapEmpty) {
        yfcInDoc = YFCDocument.getDocumentFor(inDoc);
        chOrderRootEle.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
        chOrderRootEle.importNode(yfcInDoc.getDocumentElement().getChildElement(GCConstants.ORDER_LINES));
        GCLogUtils.verboseLog(LOGGER, "changeOrder Doc after updating InvAttributesForCompSerialBundleItem : ",
            docChangeOrder);
      }

      if (!YFCCommon.equals(GCConstants.CALL_CENTER_ORDER_CHANNEL,
          sExtnOrderCaptureChannel)
          && !YFCCommon.equals(GCConstants.GCSTORE, sExtnOrderCaptureChannel)) {
        callChangeOrderForPriceHold = validateUnderpricedHold(env,
            inDoc, docChangeOrder);
      }
      YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
      YFCElement inputRootEle = inputDoc.getDocumentElement();
      // GCStore-1937 DefectFix --Start
      boolean callChangeOrderForPrePaidOrder = GCOrderUtil
          .updatePrepaidSourcing(env, inputRootEle, docChangeOrder);
      boolean callChangeOrderForSerial = updateInfInvCheckOfSerialItem(
          inputRootEle, docChangeOrder);
      if (callChangeOrderForPriceHold || callChangeOrderForPrePaidOrder
          || callChangeOrderForSerial
          || !isSerialBundleMapEmpty) {
        // GCStore-1937 DefectFix --End

        chOrderRootEle.setAttribute("OrderHeaderKey",
            inputRootEle.getAttribute("OrderHeaderKey"));
        GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER,
            docChangeOrder, null);
      }
      /* checkForPreOrderItem(env, inDoc) */
      // Fix for 4091 : Moved stampUnitCost method from Start to End.
      // call stampUnitCost Method
      if (YFCCommon.equals(GCConstants.ORDER_CATURE_SYSTEM_ATG, sExtnSourceSystem)) {
    	  stampUnitCost(env, inDoc);
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCProcessOrderOnSuccess.processOrderOnSuccess(), Exception is :",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCProcessOrderOnSuccess.processOrderOnSuccess() method ");
    return inDoc;
  }


  /**
   * This method updates Inventory Attributes for Serial Bundle Item at OrderLine Level.
   *
   * @param env
   * @param inDoc
   * @param mapPrimeLineNoNBatchNoForSerialBundle
   * @return
   */
  private Document updateInvAttributeForCompSerialBundle(YFSEnvironment env, Document inDoc,
      Map<String, String> mapPrimeLineNoNBatchNoForSerialBundle) {
    LOGGER.beginTimer("GCProcessOrderOnSuccess.updateInvAttributeForCompSerialBundle");
    LOGGER.verbose("Class: GCProcessOrderOnSuccess Method: updateInvAttributeForCompSerialBundle ::BEGIN");
    Document cloneInDoc = (Document) inDoc.cloneNode(true);
    YFCDocument cloneYFCDoc = YFCDocument.getDocumentFor(cloneInDoc);
    YFCElement orderElem = cloneYFCDoc.getDocumentElement();
    YFCElement orderLines = orderElem.getChildElement(GCConstants.ORDER_LINES);
    if (!YFCCommon.isVoid(orderLines)) {
      for (YFCElement orderLine : orderLines.getElementsByTagName(GCConstants.ORDER_LINE)) {
        YFCElement item = orderLine.getChildElement(GCConstants.ITEM);
        if (!YFCCommon.isVoid(item.getAttribute(GCXmlLiterals.BUNDLE_FULLFILLMENT_MODE))) {
          item.removeAttribute(GCXmlLiterals.BUNDLE_FULLFILLMENT_MODE);
        }
        YFCElement eBundleParentLine = orderLine.getChildElement(GCConstants.BUNDLE_PARENT_LINE);
        if (!YFCCommon.isVoid(eBundleParentLine)) {
          String sItemId = item.getAttribute(GCConstants.ITEM_ID);
          String sUOM = item.getAttribute(GCConstants.UNIT_OF_MEASURE);
          YFCDocument itemDetailDoc =
              GCCommonUtil.getItemList(env, sItemId, sUOM, GCConstants.GCI, true,
                  YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LST_API_TMP_FOR_UNQ_ITEM));
          GCLogUtils.verboseLog(LOGGER, "output of getItemList api : ", itemDetailDoc);
          YFCElement itemDetail = itemDetailDoc.getDocumentElement().getChildElement(GCConstants.ITEM);
          YFCElement eExtn = itemDetail.getChildElement(GCConstants.EXTN);
          String componentExtnIsUniqueItem = "";
          if (!YFCCommon.isVoid(eExtn)) {
            componentExtnIsUniqueItem = eExtn.getAttribute(GCConstants.EXTN_UNIQUE_ITEM);
          }
          if (YFCCommon.equals(componentExtnIsUniqueItem, GCConstants.YES)) {
            String parentPrimeLineNo = eBundleParentLine.getAttribute(GCConstants.PRIME_LINE_NO);
            YFCElement eOrderLineInvAttRequest =
                orderLine.getChildElement(GCXmlLiterals.ORDERLINE_INVATT_REQUEST, true);
            if (mapPrimeLineNoNBatchNoForSerialBundle.containsKey(parentPrimeLineNo)) {
              eOrderLineInvAttRequest.setAttribute(GCConstants.BATCH_NO,
                  mapPrimeLineNoNBatchNoForSerialBundle.get(parentPrimeLineNo));
            } else {
              eOrderLineInvAttRequest.setAttribute(GCConstants.BATCH_NO, "");
            }
            GCLogUtils.verboseLog(LOGGER, "updated orderLine with InvAttributes for SerialBundle : ", orderLine);
          }
        }
      }
    }
    GCLogUtils.verboseLog(LOGGER, "cloneInDoc after updating InvAttRequest of serial bundle   : ", cloneInDoc);
    LOGGER.verbose("GCProcessOrderOnSuccess.updateInvAttributeForCompSerialBundle method :: END");
    LOGGER.endTimer("GCProcessOrderOnSuccess.updateInvAttributeForCompSerialBundle");
    return cloneInDoc;
  }


  /**
   * @author zuzarinder.singh
   * @param env
   * @param inputRootEle
   * @param docChangeOrder
   * @return
   */
  private boolean updateInfInvCheckOfSerialItem(YFCElement inputRootEle,
      YFCDocument docChangeOrder) {
    LOGGER.beginTimer(" Entering updateInfInvCheckOfSerialItem method ");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" updateInfInvCheckOfSerialItem(): Input Document"
          + inputRootEle.toString()
          + "Input Document Change Order Document"
          + docChangeOrder.toString());
    }
    YFCElement eleOrderLines = inputRootEle.getChildElement("OrderLines");
    YFCNodeList<YFCElement> nlOrderLine = eleOrderLines
        .getElementsByTagName("OrderLine");

    YFCElement eleRootChangeOrder = docChangeOrder.getDocumentElement();
    YFCElement eleOrderLinesChangeOrder = eleRootChangeOrder
        .getChildElement("OrderLines", true);
    boolean isItemForParticularStore = false;
    for (YFCElement eleOrderLine : nlOrderLine) {
      isItemForParticularStore = isItemForParticularStoreOnly(eleOrderLine);
      if (isItemForParticularStore) {
        YFCElement eleOrderLineChangeOrder = eleOrderLinesChangeOrder
            .createChild("OrderLine");
        eleOrderLineChangeOrder.setAttribute(
            GCConstants.ORDER_LINE_KEY,
            eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
        YFCElement eleOrdOrderLinSrcCtrlsChangeOrder = eleOrderLineChangeOrder
            .getChildElement("OrderLineSourcingControls", true);
        YFCElement cOrdOrderLinSrcCtrlChangeOrder = eleOrdOrderLinSrcCtrlsChangeOrder
            .createChild("OrderLineSourcingCntrl");
        cOrdOrderLinSrcCtrlChangeOrder.setAttribute("Action", "CREATE");
        cOrdOrderLinSrcCtrlChangeOrder.setAttribute(
            "InventoryCheckCode", "INFINV");
        cOrdOrderLinSrcCtrlChangeOrder.setAttribute(GCConstants.NODE,
            eleOrderLine.getAttribute(GCConstants.SHIP_NODE));
      }
    }
    YFCElement eleRootOfChangeOrder = docChangeOrder.getDocumentElement();
    eleRootOfChangeOrder.setAttribute(GCConstants.ORDER_HEADER_KEY,
        inputRootEle.getAttribute(GCConstants.ORDER_HEADER_KEY));
    eleRootOfChangeOrder
    .setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" updateInfInvCheckOfSerialItem():Updated Change Order Document"
          + docChangeOrder.toString());
      LOGGER.endTimer(" Exiting updateInfInvCheckOfSerialItem method ");
    }
    return true;
  }

  /**
   * /**
   *
   * @author zuzarinder.singh
   * @param orderLineEle
   * @return
   */
  public static boolean isItemForParticularStoreOnly(YFCElement orderLineEle) {
    LOGGER.beginTimer("Entering method isItemForParticularStoreOnly");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" isItemForParticularStoreOnly(): Input Document"
          + orderLineEle.toString());
    }
    boolean isSerialRequest = false;
    YFCElement orderLineInvAttReq = orderLineEle
        .getChildElement("OrderLineInvAttRequest");
    if (!YFCCommon.isVoid(orderLineInvAttReq)) {
      isSerialRequest = !YFCCommon.isVoid(orderLineInvAttReq
          .getAttribute("BatchNo"));
    }
    String kitCode = orderLineEle.getAttribute("KitCode");
    if (isSerialRequest && YFCCommon.isVoid(kitCode)) {
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose(" isItemForParticularStoreOnly():Found a serial request without kit code");
      }
      LOGGER.endTimer("Exiting method isItemForParticularStoreOnly");
      return true;
    }
    LOGGER.endTimer("Exiting method isItemForParticularStoreOnly:Could not find a serial request without kit code");
    return false;
  }

  /**
   *
   * Description of validateUnderpricedHold Check if InventoryCost of item >
   * UnitPrice, then apply UnderPriced hold on order
   *
   * @param env
   * @param inDoc
   * @throws GCException
   *
   */
  private boolean validateUnderpricedHold(YFSEnvironment env, Document inDoc,
      YFCDocument docChangeOrder) throws GCException {
    LOGGER.debug(" Entering GCProcessOrderOnSuccess.validateUnderpricedHold() method ");
    try {
      boolean bIsUnderpricedHoldApplied = false;
      NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc,
          "Order/OrderLines/OrderLine");
      for (int counter = 0; counter < nlOrderLine.getLength(); counter++) {

        Element eleOrderLine = (Element) nlOrderLine.item(counter);

        String sExtnIsFreeGiftItem = GCXMLUtil.getAttributeFromXPath(
            eleOrderLine, "Extn/@ExtnIsFreeGiftItem");
        Element eleBundleParentLine = (Element) XPathAPI
            .selectSingleNode(eleOrderLine, "BundleParentLine");
        if (YFCCommon.isVoid(eleBundleParentLine)
            && !GCConstants.YES
            .equalsIgnoreCase(sExtnIsFreeGiftItem)) {
          String sUnitPrice = GCXMLUtil.getAttributeFromXPath(
              eleOrderLine, "LinePriceInfo/@UnitPrice");
          Double dUnitPrice = Double.valueOf(sUnitPrice);

          String sItemID = GCXMLUtil.getAttributeFromXPath(
              eleOrderLine, "Item/@ItemID");
          String sUOM = GCXMLUtil.getAttributeFromXPath(eleOrderLine,
              "Item/@UnitOfMeasure");

          Document docGetItemListOP = GCCommonUtil.getItemList(env,
              sItemID, sUOM, GCConstants.GCI);

          String sExtnInventoryCost = GCXMLUtil
              .getAttributeFromXPath(docGetItemListOP,
                  "ItemList/Item/Extn/@ExtnInventoryCost");
          double dExtnInventoryCost = (!YFCCommon
              .isVoid(sExtnInventoryCost)) ? Double
                  .valueOf(sExtnInventoryCost) : 0.00;

                  LOGGER.debug(" dUnitPrice is " + dUnitPrice
                      + " dExtnInventoryCost is " + dExtnInventoryCost);

                  if (!bIsUnderpricedHoldApplied
                      && (dExtnInventoryCost) > dUnitPrice) {
                    LOGGER.debug(" Inside if(dExtnInventoryCost > dUnitPrice) ");

                    YFCElement eleRootOfChangeOrder = docChangeOrder
                        .getDocumentElement();
                    eleRootOfChangeOrder.setAttribute(
                        GCConstants.ORDER_HEADER_KEY,
                        inDoc.getDocumentElement().getAttribute(
                            GCConstants.ORDER_HEADER_KEY));
                    eleRootOfChangeOrder.setAttribute(GCConstants.OVERRIDE,
                        GCConstants.YES);

                    YFCElement eleOrderHoldTypes = GCXMLUtil.createElement(
                        docChangeOrder, GCConstants.ORDER_HOLD_TYPES,
                        null);
                    eleRootOfChangeOrder.appendChild(eleOrderHoldTypes);
                    YFCElement eleOrderHoldType = GCXMLUtil.createElement(
                        docChangeOrder, GCConstants.ORDER_HOLD_TYPE,
                        null);
                    eleOrderHoldType.setAttribute(GCConstants.HOLD_TYPE,
                        GCConstants.UNDERPRICED_HOLD);
                    eleOrderHoldType.setAttribute(GCConstants.STATUS,
                        GCConstants.APPLY_HOLD_CODE);
                    eleOrderHoldTypes.appendChild(eleOrderHoldType);
                    if (LOGGER.isDebugEnabled()) {
                      LOGGER.debug(" docChangeOrder is" + docChangeOrder);
                    }
                    // Since changeOrder call is required when Order is
                    // prepaid type, instead of call changeOrder here, moved
                    // to calling method
                    // this has been commented and a flag is passed if
                    // changeOrder call required, so that calling method
                    // call changeOrder
                    return true;
                  }
        }
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCProcessOrderOnSuccess.validateUnderpricedHold(), Exception is :",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCProcessOrderOnSuccess.validateUnderpricedHold() method ");
    return false;
  }

  @Override
  public void setProperties(Properties arg0) {

  }

  /**
   * GCSTORE-3266::This method calls getItemList to get the ExtnInventoryCost of an item and updates
   * it at orderLine unit cost.
   *
   * @param inDoc
   */
  private void stampUnitCost(YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("stampUnitCost::Begin");
    // create chnageOrder Input
    YFCDocument changeOrdrDoc = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement changeOrdrRoot = changeOrdrDoc.getDocumentElement();
    changeOrdrRoot.setAttribute(GCConstants.ORDER_HEADER_KEY,
        inDoc.getDocumentElement().getAttribute(GCConstants.ORDER_HEADER_KEY));
    YFCElement changeOrdrOrderLinesEle = changeOrdrRoot.createChild(GCConstants.ORDER_LINES);
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleOrderLines = inputDoc.getDocumentElement().getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> orderLineNodeList = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
    boolean callChangeOrdr = false;
    for (YFCElement eleOrderLine : orderLineNodeList) {
      callChangeOrdr = true;
      YFCElement changeOrderLine = changeOrdrOrderLinesEle.createChild(GCConstants.ORDER_LINE);
      changeOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
        
      YFCElement changeOrdrItem = changeOrderLine.createChild(GCConstants.ITEM);

      YFCElement eleItem = eleOrderLine.getChildElement(GCConstants.ITEM);
      String itemId = eleItem.getAttribute(GCConstants.ITEM_ID);
      changeOrdrItem.setAttribute(GCConstants.ITEM_ID, itemId);
      changeOrdrItem.setAttribute(GCConstants.PRODUCT_CLASS, eleItem.getAttribute(GCConstants.PRODUCT_CLASS));
      changeOrdrItem.setAttribute(GCConstants.UOM, eleItem.getAttribute(GCConstants.UNIT_OF_MEASURE));
      YFCDocument getItemListTmpl =
          YFCDocument
          .getDocumentFor("<ItemList><Item ItemID=''><PrimaryInformation UnitCost=''/><Extn ExtnInventoryCost='' /></Item></ItemList>");
      YFCDocument getItemListOutput =
          GCCommonUtil.getItemList(env, itemId, GCConstants.EACH, GCConstants.GCI, true, getItemListTmpl);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("GetItemList Output::" + getItemListOutput.toString());
      }
      YFCElement itemListEle = getItemListOutput.getDocumentElement();
      YFCElement itemEle = itemListEle.getChildElement(GCConstants.ITEM);
      if (!YFCCommon.isVoid(itemEle)) {
        YFCElement eleExtn = itemEle.getChildElement(GCConstants.EXTN);
        YFCElement elePrimaryInfo = itemEle.getChildElement(GCConstants.PRIMARY_INFORMATION);
        String sUnitCost = elePrimaryInfo.getAttribute("UnitCost");
        Double dUnitCost = elePrimaryInfo.getDoubleAttribute("UnitCost");
        String extnInvCost = eleExtn.getAttribute("ExtnInventoryCost");
        Double dInvCost = eleExtn.getDoubleAttribute("ExtnInventoryCost");
        LOGGER.verbose("ItemID::" + itemId + "::ExtnInventoryCost::" + extnInvCost);

        //Defect-4091
        YFCElement orderLineExtn = changeOrderLine.getChildElement(GCConstants.EXTN,true);
      	
        if (!YFCCommon.isVoid(extnInvCost) && dInvCost != 0.00) {
        	orderLineExtn.setAttribute("ExtnInventoryCost", extnInvCost);
          } else if (!YFCCommon.isVoid(sUnitCost) && dUnitCost != 0.00) {
        	  orderLineExtn.setAttribute("ExtnInventoryCost", sUnitCost);
          } else {
        	  orderLineExtn.setAttribute("ExtnInventoryCost", "0.00");
          }
        
        //Defect-4091
     }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input After appending UnitCost:::" + changeOrdrDoc.toString());
    }
    if (callChangeOrdr) {
      GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, changeOrdrDoc, null);
    }
    LOGGER.endTimer("stampUnitCost::End");
  }
}
