/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *##############################################################################################################################################################
 *	        OBJECTIVE: Stamp ExtnShipAlone at order line level for release consolidation
 *##################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *##################################################################################################################################################################
             1.0               10/03/2015        Jain Mansi		        OMS-4904: Development - Stamp ExtnShipAlone at order line level for release consolidation
             1.1               19/03/2015        Ravi, Divya		    GCSTORE-803: Development - Stamp ExtnShipAlone at recurring item lines for release consolidation
             1.2			   21/04/2015		 Ravi, Divya            GCSTORE-1847 Get CSC from inDoc and restamp in order lines if it doesnt match the out put of apply carrier service code
 *##################################################################################################################################################################
 */
package com.gc.eventhandler;

import java.util.ArrayList;
import java.util.List;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCProcessOrdersOnScheduleOnSuccessAPI{

  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCProcessOrdersOnScheduleOnSuccessAPI.class);

  /**
   *
   * Description: This method will stamp ExtnIsShipAlone at order line level which would help in release consolidation
   *
   * @param env
   * @param inDoc
   * @return inDoc
   * @throws GCException
   *
   */
  public Document processOrdersOnSchedule(YFSEnvironment env, Document inDoc)
      throws GCException {

    LOGGER.debug("Class: GCProcessOrdersOnScheduleOnSuccessAPI:: " + "Method: processOrdersOnSchedule:: ");

    LOGGER.debug("In Doc:"+GCXMLUtil.getXMLString(inDoc));
    try {
      //Form input xml for getOrderList
      String strOHK = inDoc.getDocumentElement().getAttribute(GCConstants.ORDER_HEADER_KEY);
      Document docGetOrderListIp = GCXMLUtil.createDocument(GCConstants.ORDER);
      docGetOrderListIp.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, strOHK);

      //Form template for getOrderList
      Document docGetOrderListTemplate = GCXMLUtil.getDocument("<OrderList><Order OrderNo='' LevelOfService=''><OrderLines><OrderLine CarrierServiceCode='' OrderLineKey='' PrimeLineNo='' MaxLineStatus='' ShipNode='' IsBundleParent='' KitCode=''>" +
          "<Item ProductClass='' />"+
 "<Extn ExtnNonHJOrderLine='' ExtnIsShipAlone='' ExtnIsWarrantyItem=''/><ItemDetails  ItemID='' ProductClass=''><PrimaryInformation ItemType='' IsHazmat=''/><Extn ExtnIsShipAlone='' ExtnIsUniqueItem='' ExtnIsTruckShip='' ExtnItemStatus=''/></ItemDetails>"
                  +
          "<BundleParentLine OrderLineKey=''/><OrderStatuses><OrderStatus ShipNode='' Status='' /></OrderStatuses><Schedules><Schedule ShipNode=''/></Schedules></OrderLine>" +
          "</OrderLines><PersonInfoShipTo Country=''><Extn ExtnIsAPOFPO='' ExtnIsPOBox=''/></PersonInfoShipTo></Order></OrderList>");

      LOGGER.debug("Get Order List Input:"+GCXMLUtil.getXMLString(docGetOrderListIp));

      //Invoke getOrderList API
      Document docGetOrderListOutput=GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST, docGetOrderListIp, docGetOrderListTemplate);

      LOGGER.debug("Get Order List Output:"+GCXMLUtil.getXMLString(docGetOrderListOutput));

      NodeList nlOrderLine = XPathAPI.selectNodeList(docGetOrderListOutput, "OrderList/Order/OrderLines/OrderLine");

      LOGGER.debug("NodeList length:"+nlOrderLine.getLength());

      //Phase 2 ORder Fulfillment changes
      Element orderEle = (Element) docGetOrderListOutput.getElementsByTagName(GCConstants.ORDER).item(0);
      Document outDocCarrierService = GCCommonUtil.invokeService(env, GCConstants.GC_APPLY_CARRIER_SERVICE_CODE_TO_ORDERLINE, GCXMLUtil.getDocumentFromElement(orderEle));
      //Phase 2 ORder Fulfillment changes
      //Form document for changeOrder xml
      Document docChangeOrderIp = GCXMLUtil.createDocument(GCConstants.ORDER);
      Element eleChangeOrder=docChangeOrderIp.getDocumentElement();
      eleChangeOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, strOHK);
      eleChangeOrder.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
      Element eleChangeOrderLines = GCXMLUtil.createElement(docChangeOrderIp, GCConstants.ORDER_LINES,null);
      eleChangeOrder.appendChild(eleChangeOrderLines);
      Element eleGOLOrderLine;
      Element eleChangeOrderLine;
      Element eleGOLOrderLineExtn;
      String strBundleParentOLKey=null;
      String strShipNode=null;
      Element eleBundleParentOrderLine;
      String strPrimeLineNo;
      //Phase 2 Order fulfillment changes
      List<String> items = new ArrayList<String>();
      String expediteFlag = GCXMLUtil.getAttributeFromXPath(outDocCarrierService, "Order/Extn/@ExtnExpediteFlag");
      if (!YFCCommon.isVoid(expediteFlag)) {
        Element orderExtnEle = docChangeOrderIp.createElement(GCConstants.EXTN);
        docChangeOrderIp.getDocumentElement().appendChild(orderExtnEle);
        orderExtnEle.setAttribute(GCConstants.EXTN_EXPEDITE_FLAG, expediteFlag);
      }
      //Phase 2 Order fulfillment changes

      for (int counter = 0; counter < nlOrderLine.getLength(); counter++) {

        LOGGER.debug("Inside For loop");

        eleGOLOrderLine = (Element) nlOrderLine.item(counter);

        //if order line status is scheduled
        if(GCConstants.STATUS_SCHEDULED.equals(eleGOLOrderLine.getAttribute("MaxLineStatus"))
            || GCConstants.SCHEDULED_PO_DAX_STATUS.equals(eleGOLOrderLine.getAttribute("MaxLineStatus"))
            || GCConstants.AWAITING_CHAINED_ORDER_STATUS.equals(eleGOLOrderLine.getAttribute("MaxLineStatus"))) {
          LOGGER.debug("Order line status is scheduled");

          //Fetch Ship Node from which Scheduling has taken place
          strShipNode=GCXMLUtil.getAttributeFromXPath(eleGOLOrderLine, "OrderStatuses/OrderStatus[@Status='1500']/@ShipNode");
          //Fetch Bundle Parent Order Line Key
          if(strShipNode==null){
            strShipNode=GCXMLUtil.getAttributeFromXPath(eleGOLOrderLine, "OrderStatuses/OrderStatus[@Status='1500.10']/@ShipNode");
          }
          strBundleParentOLKey=GCXMLUtil.getAttributeFromXPath(eleGOLOrderLine, "BundleParentLine/@OrderLineKey");

          //Phase 2 changes ORder Fulfillment
          String sKitCode = eleGOLOrderLine.getAttribute(GCConstants.KIT_CODE);
          Element itemEle = (Element) eleGOLOrderLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
          Element extnEle = (Element) itemEle.getElementsByTagName(GCConstants.EXTN).item(0);
          String sExtnIsUniqItem = extnEle.getAttribute(GCConstants.EXTN_IS_UNIQUE_ITEM);
          String orderLineKey = eleGOLOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
          String carrierServiceCode = GCXMLUtil.getAttributeFromXPath(outDocCarrierService, "//OrderLine[@OrderLineKey='"+ orderLineKey +"']/@CarrierServiceCode");
          //GCSTORE-1847 Get CSC from inDoc and restamp in order lines if it doesnt match the out put of apply carrier service code
          String cscFromInDoc = GCXMLUtil.getAttributeFromXPath(inDoc, "//OrderLine[@OrderLineKey='"+ orderLineKey +"']/@CarrierServiceCode");

          //Phase 2 changes ORder Fulfillment
          //For Bundle Components
          if(!(YFCCommon.isVoid(strBundleParentOLKey))){
            LOGGER.debug("Bundle Component");

            //Fetch Bundle Parent Order Lin-e element
            eleBundleParentOrderLine=GCXMLUtil.getElementByXPath(docGetOrderListOutput, "OrderList/Order/OrderLines/OrderLine[@OrderLineKey='"+strBundleParentOLKey+"']");
           
            if (eleBundleParentOrderLine!=null){
               
              LOGGER.debug("Bundle Parent Order Line is present");
              //GCSTORE-4659::Begin
              Element eleBundleParentFromCarrierOut = GCXMLUtil.getElementByXPath(outDocCarrierService, "Order/OrderLines/OrderLine[@OrderLineKey='" + strBundleParentOLKey +"']");
              addBundleParentLinetoChangeOrderInput(eleBundleParentFromCarrierOut, eleChangeOrderLines, docChangeOrderIp);
              //GCSTORE-4659::End
              if((GCConstants.YES.equals(GCXMLUtil.getAttributeFromXPath(eleBundleParentOrderLine, "Extn/@ExtnNonHJOrderLine"))) ||
                  (GCConstants.YES.equals(GCXMLUtil.getAttributeFromXPath(eleBundleParentOrderLine, "ItemDetails/Extn/@ExtnIsShipAlone")) && GCConstants.MFI.equals(strShipNode) && GCConstants.SET.equals(GCXMLUtil.getAttributeFromXPath(eleBundleParentOrderLine, "Item/@ProductClass")))){
                LOGGER.debug("Bundle Parent Order Line Key is present & order line requires ExtnIsShipAlone to be set");

                //If the ExtnIsShipAlone is not already stamped or the value is not correct
                if(!(strBundleParentOLKey.equals(GCXMLUtil.getAttributeFromXPath(eleGOLOrderLine, "Extn/@ExtnIsShipAlone")))){
                  //LOGGER.debug("ExtnIsShipNode is not present.Bundle Parent Order Line Key is present. Stamp ExtnIsShipAlone:"+strBundleParentOLKey);
                  //GCSTORE-5240:START
                  Element eleItem = (Element) eleBundleParentOrderLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
                  String itemId = eleItem.getAttribute(GCConstants.ITEM_ID);
                  LOGGER.debug("ExtnIsShipNode is not present.ItemId is present. Stamp ExtnIsShipAlone:"+itemId);
                  //GCSTORE-5240:END
                  eleChangeOrderLine=GCXMLUtil.createElement(docChangeOrderIp, GCConstants.ORDER_LINE,null);
                  eleGOLOrderLineExtn=GCXMLUtil.createElement(docChangeOrderIp, GCConstants.EXTN,null);
                  //Stamp OrderLine\Extn\@ExtnIsShipAlone as Bundle Parent OrderLineKey
                  //GCSTORE-5240:START
                  //eleGOLOrderLineExtn.setAttribute("ExtnIsShipAlone", strBundleParentOLKey);
                  eleGOLOrderLineExtn.setAttribute("ExtnIsShipAlone", itemId);
                  //GCSTORE-5240:END
                  eleChangeOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, eleGOLOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
                  eleChangeOrderLine.appendChild(eleGOLOrderLineExtn);
                  eleChangeOrderLines.appendChild(eleChangeOrderLine);
                }
              }
            }
          } else if((GCConstants.YES.equals(GCXMLUtil.getAttributeFromXPath(eleGOLOrderLine, "Extn/@ExtnNonHJOrderLine"))) ||
              (GCConstants.YES.equals(GCXMLUtil.getAttributeFromXPath(eleGOLOrderLine, "ItemDetails/Extn/@ExtnIsShipAlone")) && GCConstants.MFI.equals(strShipNode))){
            //For Regular Items
            LOGGER.debug("Regular Item is present");
            strPrimeLineNo=eleGOLOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
            
          //GCSTORE-5240:START
            Element eleItem = (Element) eleGOLOrderLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
            String itemId = eleItem.getAttribute(GCConstants.ITEM_ID);
          //GCSTORE-5240:END

            //If the ExtnIsShipAlone is not already stamped or the value is not correct
            if(!(itemId.equals(GCXMLUtil.getAttributeFromXPath(eleGOLOrderLine, "Extn/@ExtnIsShipAlone")))){

              eleChangeOrderLine=GCXMLUtil.createElement(docChangeOrderIp, GCConstants.ORDER_LINE,null);
              eleGOLOrderLineExtn=GCXMLUtil.createElement(docChangeOrderIp, GCConstants.EXTN,null);
              eleChangeOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, eleGOLOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
              eleChangeOrderLine.appendChild(eleGOLOrderLineExtn);
              eleChangeOrderLines.appendChild(eleChangeOrderLine);

              LOGGER.debug("Regular Item. Extn/@ExtnIsShipAlone is not present .Stamp ExtnIsShipAlone as Prime Line No:"+eleGOLOrderLine.getAttribute(GCConstants.PRIME_LINE_NO));

              //GCSTORE-5240:START
              //Stamp OrderLine\Extn\@ExtnIsShipAlone as ItemId
              //eleGOLOrderLineExtn.setAttribute("ExtnIsShipAlone", strPrimeLineNo);
              eleGOLOrderLineExtn.setAttribute("ExtnIsShipAlone", itemId);
              //GCSTORE-5240:END
            }

          } else if ((!YFCCommon.equals(GCConstants.YES, sExtnIsUniqItem)) && !YFCCommon.equals(GCConstants.BUNDLE, sKitCode)) {
            //Phase 2 changes Order Fulfillment changes
            stampShipAloneForRecurringItems(eleGOLOrderLine, docChangeOrderIp, items, eleChangeOrderLines, strShipNode);
          } else if (!(YFCCommon.isVoid(GCXMLUtil.getAttributeFromXPath(eleGOLOrderLine, "Extn/@ExtnIsShipAlone")))){
            //Phase 2 changes Order Fulfillment changes
            //Wipe out the existing OrderLine/Extn/@ExtnIsShipAlone if the above conditions are not met
            LOGGER.debug("OrderLine/Extn/@ExtnIsShipNode needs to be wiped off");
            eleChangeOrderLine=GCXMLUtil.createElement(docChangeOrderIp, GCConstants.ORDER_LINE,null);
            eleGOLOrderLineExtn=GCXMLUtil.createElement(docChangeOrderIp, GCConstants.EXTN,null);
            eleChangeOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, eleGOLOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
            eleChangeOrderLine.appendChild(eleGOLOrderLineExtn);
            eleChangeOrderLines.appendChild(eleChangeOrderLine);
            eleGOLOrderLineExtn.setAttribute("ExtnIsShipAlone", "");
          }

          //Phase 2 changes Order Fulfillment changes
          //GCSTORE-1847 Get CSC from inDoc and restamp in order lines if it doesnt match the out put of apply carrier service code
          if (!YFCCommon.equals(carrierServiceCode, cscFromInDoc)) {
            eleChangeOrderLine = GCXMLUtil.getElementByXPath(docChangeOrderIp, "//OrderLine[@OrderLineKey='"+ orderLineKey +"']");
            if (YFCCommon.isVoid(eleChangeOrderLine)) {
              eleChangeOrderLine=GCXMLUtil.createElement(docChangeOrderIp, GCConstants.ORDER_LINE,null);
              eleChangeOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey );
              eleChangeOrderLines.appendChild(eleChangeOrderLine);
            }
            eleChangeOrderLine.setAttribute(GCConstants.CARRIER_SERVICE_CODE, carrierServiceCode);
          }
          //Phase 2 changes Order Fulfillment changes
        }
      }
      modifyChangeOrderDoc(docChangeOrderIp,docGetOrderListOutput,eleChangeOrderLines);
      LOGGER.debug("Change Order API input:"+GCXMLUtil.getXMLString(docChangeOrderIp));

      if (GCXMLUtil.getElementByXPath(docChangeOrderIp, "Order/OrderLines/OrderLine")!=null
          ||!YFCCommon.isVoid(expediteFlag)){

        LOGGER.debug("Invoke Change Order API");
        //Invoke changeOrder API
        GCCommonUtil.invokeAPI(env,GCConstants.API_CHANGE_ORDER, docChangeOrderIp);
      }


    } catch (Exception e) {
      LOGGER.error("Inside catch block of GCProcessOrdersOnScheduleOnSuccessAPI.processOrdersOnSchedule method. Exception is", e);
      throw new GCException(e);
    }

    return inDoc;
  }

  /**
   * GCSTORE-4659:: To stamp carrier service code at bundle parent level as well.
   * Description of addBundleParentLinetoChangeOrderInput
   *
   * @param eleBundleParentOrderLine
   * @param eleChangeOrderLines
   * @param changeOrderInput 
   *
   */
  private void addBundleParentLinetoChangeOrderInput(
        Element eleBundleParentOrderLine, Element eleChangeOrderLines, Document changeOrderInput) {
   String ordrLneKey = eleBundleParentOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
   Element orderLineInput = GCXMLUtil.getElementByXPath(changeOrderInput, "Order/OrderLines/OrderLine[@OrderLineKey='" + ordrLneKey +"']");
   if(YFCCommon.isVoid(orderLineInput)){
       Element orderlineEle = changeOrderInput.createElement(GCConstants.ORDER_LINE);
       orderlineEle.setAttribute(GCConstants.ORDER_LINE_KEY, ordrLneKey);
       orderlineEle.setAttribute(GCConstants.CARRIER_SERVICE_CODE, eleBundleParentOrderLine.getAttribute(GCConstants.CARRIER_SERVICE_CODE));
       eleChangeOrderLines.appendChild(orderlineEle);
   }
    
}

/**
   * Phase 2 Order fulfillment changes
   * @param eleGOLOrderLine
   * @param docChangeOrderIp
   * @param items
   * @param eleChangeOrderLines
   * @param strShipNode
   */

  private void stampShipAloneForRecurringItems(Element eleGOLOrderLine, Document docChangeOrderIp, List<String> items, Element eleChangeOrderLines, String strShipNode) {

    Element eleItem = (Element) eleGOLOrderLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
    String itemId = eleItem.getAttribute(GCConstants.ITEM_ID);
    if (!items.contains(itemId)) {
      items.add(itemId);
    } else if (!YFCCommon.equals(GCConstants.MFI, strShipNode)) {
      Element eleChangeOrderLine = GCXMLUtil.createElement(docChangeOrderIp, GCConstants.ORDER_LINE,null);
      Element eleGOLOrderLineExtn = GCXMLUtil.createElement(docChangeOrderIp, GCConstants.EXTN,null);
      eleChangeOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, eleGOLOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
      eleChangeOrderLine.appendChild(eleGOLOrderLineExtn);
      eleChangeOrderLines.appendChild(eleChangeOrderLine);
      eleGOLOrderLineExtn.setAttribute("ExtnIsShipAlone", eleGOLOrderLine.getAttribute(GCConstants.PRIME_LINE_NO));
    }

  }
  /**
   *
   * Description of modifyChangeOrderDoc this method finds the components of the bundle parents and stamp ExtnIsShipAlone=Component OrderLine PrimeLineNo
   *
   * @param docChangeOrderIp
   * @param docGetOrderListOutput
   * @param eleChangeOrderLines
   * @return
   * @throws GCException
   *
   */
  public Document modifyChangeOrderDoc(Document docChangeOrderIp , Document docGetOrderListOutput,Element eleChangeOrderLines) throws GCException{
    LOGGER.debug("Entering inside  GCProcessOrdersOnScheduleOnSuccessAPI.modifyChangeOrderDoc method.");
    try {
      NodeList nlOrderLine = XPathAPI.selectNodeList(docGetOrderListOutput, "OrderList/Order/OrderLines/OrderLine[@KitCode='"+GCConstants.BUNDLE+"']");
      int length=nlOrderLine.getLength();
      for(int i=0;i<length;i++){
        Element eleGOLOrderLine = (Element) nlOrderLine.item(i);
        String sKitCode=eleGOLOrderLine.getAttribute("KitCode");
        Element eleItem=(Element)XPathAPI.selectSingleNode(eleGOLOrderLine, GCConstants.ITEM);
        String sProductClass=eleItem.getAttribute(GCConstants.PRODUCT_CLASS);
        if(!YFCCommon.isVoid(sKitCode) && !YFCCommon.isVoid(sProductClass) && sKitCode.equals(GCConstants.BUNDLE) && sProductClass.equals(GCConstants.REGULAR)){
          String sOrderLineKey=eleGOLOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
          NodeList nlOrderLineComp=XPathAPI.selectNodeList(docGetOrderListOutput,"OrderList/Order/OrderLines/OrderLine[BundleParentLine/@OrderLineKey='"+sOrderLineKey+"']");
          int len=nlOrderLineComp.getLength();
          for(int j=0;j<len;j++){
            Element eleGOLOrderLineComp = (Element) nlOrderLineComp.item(j);
            Element eleExtn=(Element)XPathAPI.selectSingleNode(eleGOLOrderLineComp, "ItemDetails/Extn");
            String sExtnIsShipAlone=eleExtn.getAttribute(GCConstants.EXTN_IS_SHIP_ALONE);
            if (!YFCCommon.isVoid(sExtnIsShipAlone) && GCConstants.FLAG_Y.equalsIgnoreCase(sExtnIsShipAlone)) {
              Element eleChangeOrderLine=GCXMLUtil.createElement(docChangeOrderIp, GCConstants.ORDER_LINE,null);
              Element eleGOLOrderLineExtn=GCXMLUtil.createElement(docChangeOrderIp, GCConstants.EXTN,null);
              eleChangeOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, eleGOLOrderLineComp.getAttribute(GCConstants.ORDER_LINE_KEY));
              eleChangeOrderLine.appendChild(eleGOLOrderLineExtn);
              eleChangeOrderLines.appendChild(eleChangeOrderLine);
              eleGOLOrderLineExtn.setAttribute(GCConstants.EXTN_IS_SHIP_ALONE, eleGOLOrderLineComp.getAttribute(GCConstants.PRIME_LINE_NO));
            }
          }
        }
      }

    } catch (Exception e) {
      LOGGER.error("Inside catch block of GCProcessOrdersOnScheduleOnSuccessAPI.modifyChangeOrderDoc method. Exception is", e);
      throw new GCException(e);
    }
    LOGGER.debug("Exiting inside  GCProcessOrdersOnScheduleOnSuccessAPI.modifyChangeOrderDoc method.");
    return docChangeOrderIp;
  }
}
