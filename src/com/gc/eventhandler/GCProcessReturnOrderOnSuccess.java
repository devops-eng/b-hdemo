package com.gc.eventhandler;

import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCProcessReturnOrderOnSuccess implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCProcessReturnOrderOnSuccess.class);


  /**
   *
   * Description of GCProcessReturnOrderOnSuccess Processes Return Order On Success
   *
   * @param env
   * @param inDoc
   * @return
   *
   */

  public void processReturnOrderOnSuccess(YFSEnvironment env, Document inDoc) {
    LOGGER.debug(" Entering GCProcessReturnOrderOnSuccess.processReturnOrderOnSuccess() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" processReturnOrderOnSuccess() method, Input document is" + GCXMLUtil.getXMLString(inDoc));
    }
    // Fix to stamp BillToID if not present on ReturnOrder
    Element eleRoot = inDoc.getDocumentElement();
    String sBillToID = eleRoot.getAttribute(GCConstants.BILL_TO_ID);
    String sSalesOrderHeaderKey = GCXMLUtil.getAttributeFromXPath(inDoc,"Order/OrderLines/OrderLine/@DerivedFromOrderHeaderKey");
    //Start: Fix for GCSTORE-4628
    if(!YFCCommon.isVoid(sSalesOrderHeaderKey)){

    	Document docGetOrderListIP = GCXMLUtil.createDocument(GCConstants.ORDER);
    	docGetOrderListIP.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, sSalesOrderHeaderKey);
    	Document docGetOrderListTmp = GCXMLUtil.getDocument("<OrderList><Order OrderNo='' OrderHeaderKey='' BillToID=''><Extn ExtnOrderingStore='' /></Order></OrderList>");
    	Document docGetOrderListOP = GCCommonUtil.invokeAPI(env,GCConstants.API_GET_ORDER_LIST, docGetOrderListIP, docGetOrderListTmp);
    	LOGGER.debug("GCProcessReturnOrderOnSuccess :: processReturnOrderOnSuccess :: docGetOrderListOP :: \n"+GCXMLUtil.getXMLString(docGetOrderListOP));
    	
    	Element eleSOOrderElement = (Element) docGetOrderListOP.getDocumentElement().getElementsByTagName(GCConstants.ORDER).item(0);
    	String strExtnOrderingStore = GCXMLUtil.getAttributeFromXPath(docGetOrderListOP, "OrderList/Order/Extn/@ExtnOrderingStore");

    	YFCDocument chOrdIp = YFCDocument.createDocument(GCConstants.ORDER);
    	YFCElement chOrderRootEle = chOrdIp.getDocumentElement();
    	String strChangeOrderFlag="N";
    	
    	//Check whether ExtnOrderingStore exists for Sales Order
    	if(!YFCCommon.isVoid(strExtnOrderingStore)){
    		LOGGER.debug("The ExtnOrderingStore is " + strExtnOrderingStore);
    		YFCElement eleROExtn = chOrdIp.createElement(GCConstants.EXTN);
    		eleROExtn.setAttribute("ExtnOrderingStore", strExtnOrderingStore);
    		chOrderRootEle.appendChild(eleROExtn);
    		strChangeOrderFlag="Y";
    	}
    	//Check if BillToID is void for return order
    	if(YFCCommon.isVoid(sBillToID)){
    		String sSOBillToID = eleSOOrderElement.getAttribute(GCConstants.BILL_TO_ID);
    		LOGGER.debug("The sSOBillToID is " + sSOBillToID);
    		if(!YFCCommon.isVoid(sSOBillToID)){
    			chOrderRootEle.setAttribute(GCConstants.BILL_TO_ID, sSOBillToID);
    			strChangeOrderFlag="Y";
    		}
    	}
    	//Invoke changeOrder API
    	if (GCConstants.YES.equalsIgnoreCase(strChangeOrderFlag)){
    		String orderHeaderKey = eleRoot.getAttribute(GCConstants.ORDER_HEADER_KEY);
    		chOrderRootEle.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
    		chOrderRootEle.setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
    		LOGGER.debug("GCProcessReturnOrderOnSuccess :: processReturnOrderOnSuccess :: chOrdIp :: \n"+chOrdIp.getString());
    		GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, chOrdIp, null);
    	}
    }
  //End: Fix for GCSTORE-4628
    GCCommonUtil.setPhase(env, inDoc);
  }

  @Override
  public void setProperties(Properties properties) {

  }
}
