/**
 * Copyright � 2016, Guitar Center,  All rights reserved.
 *  *##########################################################################################################################################################################################
 *          OBJECTIVE: Prorates Header Charges to Line and Bundle Charges to Components
 *#############################################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#############################################################################################################################################################################################
             1.0               15/06/2016        Singh, Jaipreet           GC2037: Stamping tracking number for returns which needs to be shipped back from store to DC.
*#############################################################################################################################################################################################
 */
package com.gc.eventhandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.api.GCProcessProrations;
import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:jaipreet.singh@expicient.com">Jaipreet</a>
 */
public class GCTransferOrderConfirmShipmentOnSuccess {
 // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
            .instance(GCProcessProrations.class); 
    
    /**
     * 
     * This method will stamp Tracking numbers on the return orders which are shipped back from store to DC.
     *  
     * @param env
     * @param inDoc
     */
    
    public void processConfirmShipmentOnSuccess(YFSEnvironment env, Document inDoc) {
        
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("GCTransferOrderConfirmShipmentOnSuccess :: processTransferOrderConfirmShipmentOnSuccess :: Starts");
            LOGGER.debug("GCTransferOrderConfirmShipmentOnSuccess :: processTransferOrderConfirmShipmentOnSuccess :: inDoc ::\n"+inDoc);
          }
        NodeList nlContainer=inDoc.getElementsByTagName("Container");
        HashMap<String,List <String>> hmOrderLineTrackingNo=new HashMap<String, List <String>>();
        for(int i=0;i<nlContainer.getLength();i++)
        {
           
           Element eleContainer=(Element) nlContainer.item(i);
           if (LOGGER.isDebugEnabled()) {
               LOGGER.debug("GCTransferOrderConfirmShipmentOnSuccess :: processTransferOrderConfirmShipmentOnSuccess :: Inside Outer for loop");
               LOGGER.debug("GCTransferOrderConfirmShipmentOnSuccess :: processTransferOrderConfirmShipmentOnSuccess :: eleContainer :: \n"+GCXMLUtil.getElementString(eleContainer));
           }
           NodeList nlContainerDetail=eleContainer.getElementsByTagName("ContainerDetail");
           for(int j=0;j<nlContainerDetail.getLength();j++)
           {
               if (LOGGER.isDebugEnabled()) {
                   LOGGER.debug("GCTransferOrderConfirmShipmentOnSuccess :: processTransferOrderConfirmShipmentOnSuccess :: Inside Inner for loop");
               }
               Element eleContainerDetail=(Element) nlContainerDetail.item(j);
               if (LOGGER.isDebugEnabled()) {
                   LOGGER.debug("GCTransferOrderConfirmShipmentOnSuccess :: processTransferOrderConfirmShipmentOnSuccess :: eleContainerDetail :: \n" +GCXMLUtil.getElementString(eleContainerDetail));
               }
               //fetching orderlinekey of transfer order
               String strOrderLineKey=eleContainerDetail.getAttribute("OrderLineKey");
               if(hmOrderLineTrackingNo.containsKey(strOrderLineKey))
                {
                   if (LOGGER.isDebugEnabled()) {
                       LOGGER.debug("GCTransferOrderConfirmShipmentOnSuccess :: processTransferOrderConfirmShipmentOnSuccess :: hmOrderLineTrackingNo contains :: "+strOrderLineKey);
                   }
                   List <String>  lstTrackingNo=hmOrderLineTrackingNo.get(strOrderLineKey);
                   lstTrackingNo.add(eleContainer.getAttribute("TrackingNo"));
                   hmOrderLineTrackingNo.put(strOrderLineKey, lstTrackingNo);
                   
                }
               else
               {
                   if (LOGGER.isDebugEnabled()) {
                       LOGGER.debug("GCTransferOrderConfirmShipmentOnSuccess :: processTransferOrderConfirmShipmentOnSuccess :: hmOrderLineTrackingNo does not contain :: "+strOrderLineKey);
                   }
                   List <String>  lstTrackingNo=new ArrayList<String>();
                   lstTrackingNo.add(eleContainer.getAttribute("TrackingNo"));
                   hmOrderLineTrackingNo.put(strOrderLineKey, lstTrackingNo);
               }
              
           }
               
         }
        
        //Creating Complex Query
        Document inDocForGetOrderLineList = GCXMLUtil.createDocument("OrderLine");
        Element eleInDocForGetOrderLineList=inDocForGetOrderLineList.getDocumentElement();
        Element eleComplexQuery=  inDocForGetOrderLineList.createElement("ComplexQuery");
        eleComplexQuery.setAttribute("Operator", "Or");
        eleInDocForGetOrderLineList.appendChild(eleComplexQuery);
        Element eleOr=inDocForGetOrderLineList.createElement("Or");
        eleComplexQuery.appendChild(eleOr);
           
        
        for (String key : hmOrderLineTrackingNo.keySet()) {

            Element eleExp = inDocForGetOrderLineList.createElement("Exp");
            eleExp.setAttribute(GCConstants.QRY_TYPE, GCConstants.EQ);
            eleExp.setAttribute(GCConstants.NAME, GCConstants.ORDER_LINE_KEY);
            eleExp.setAttribute(GCConstants.VALUE,  key);
            eleOr.appendChild(eleExp);
            }
        
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("GCTransferOrderConfirmShipmentOnSuccess :: processTransferOrderConfirmShipmentOnSuccess :: inDocForGetOrderLineList :: \n" +GCXMLUtil.getXMLString(inDocForGetOrderLineList));
        }  
        
        Document docGetOrderLineListTemp=GCXMLUtil.getDocument("<OrderLineList> <OrderLine OrderLineKey='' DerivedFromOrderHeaderKey='' DerivedFromOrderLineKey=''/> </OrderLineList>");
       //calling getOrderLineList API to fetch return order for respective transfer orders
        
        Document docGetOrderLineListOp=GCCommonUtil.invokeAPI(env,GCConstants.GET_ORDER_LINE_LIST,inDocForGetOrderLineList,docGetOrderLineListTemp);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("GCTransferOrderConfirmShipmentOnSuccess :: processTransferOrderConfirmShipmentOnSuccess :: docGetOrderLineListTemp :: \n" +GCXMLUtil.getXMLString(docGetOrderLineListTemp));
            LOGGER.debug("GCTransferOrderConfirmShipmentOnSuccess :: processTransferOrderConfirmShipmentOnSuccess :: docGetOrderLineListOp :: \n" +GCXMLUtil.getXMLString(docGetOrderLineListOp));
        }  
        NodeList nlOrderLineList=docGetOrderLineListOp.getElementsByTagName("OrderLine");
        
        //Creating Input for change Order API call
        for(int i=0;i<nlOrderLineList.getLength();i++)
        {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("GCTransferOrderConfirmShipmentOnSuccess :: processTransferOrderConfirmShipmentOnSuccess :: Inside for loop for change Order Input");
            }
            Element eleOrderLineList=(Element) nlOrderLineList.item(i);
            Document docChangeOrderInp=GCXMLUtil.createDocument("Order");
            
            Element eleChangeOrderInp=docChangeOrderInp.getDocumentElement();
            eleChangeOrderInp.setAttribute("Override","Y");
            eleChangeOrderInp.setAttribute("OrderHeaderKey", eleOrderLineList.getAttribute("DerivedFromOrderHeaderKey"));
            
            Element eleOrderLines=docChangeOrderInp.createElement("OrderLines");
            eleChangeOrderInp.appendChild(eleOrderLines);
            
            Element eleOrderLine=docChangeOrderInp.createElement("OrderLine");
            eleOrderLine.setAttribute("OrderLineKey", eleOrderLineList.getAttribute("DerivedFromOrderLineKey"));
            eleOrderLines.appendChild(eleOrderLine);
           
            Element eleExtn=docChangeOrderInp.createElement("Extn");
            
            List <String> lstOrderLineKey=hmOrderLineTrackingNo.get(eleOrderLineList.getAttribute("OrderLineKey"));
            String strTrackingNo=null;
            for(int j=0;j<lstOrderLineKey.size();j++)
            {
                
                if(j==0)
                {
                    strTrackingNo=lstOrderLineKey.get(j);
                }
                else{
                    strTrackingNo=strTrackingNo+";"+lstOrderLineKey.get(j); 
                }
               
             }
            eleExtn.setAttribute("ExtnTrackingNo",strTrackingNo);
            eleOrderLine.appendChild(eleExtn);
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("GCTransferOrderConfirmShipmentOnSuccess :: processTransferOrderConfirmShipmentOnSuccess :: docChangeOrderInp :: \n" +GCXMLUtil.getXMLString(docChangeOrderInp));
            }
            //call changeOrder API
           GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER,docChangeOrderInp);          
        }
             
    }

}
