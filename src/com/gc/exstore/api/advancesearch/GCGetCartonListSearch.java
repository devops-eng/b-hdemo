package com.gc.exstore.api.advancesearch;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class is used to prepare the input and call getShipmentList API to getCartonList.
 *
 * @author varun.goel
 *
 */
public class GCGetCartonListSearch {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCGetCartonListSearch.class);



  /**
   *
   * This method invokes the getShipmentList API with the created input document from inDoc and
   * returns the getCartonList document.
   *
   * @param env
   * @param inDoc return Doc
   * @throws Exception
   *
   */
  public Document getCartonList(final YFSEnvironment env, final Document inDoc) {
    LOGGER.beginTimer("GCGetCartonListSearch.getCartonList");
    LOGGER.verbose("GCGetCartonListSearch.getCartonList--Begin");
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    LOGGER.verbose("inputDoc : " + inputDoc.toString());

    final YFCElement eleRoot = inputDoc.getDocumentElement();
    String sTrackingNo = eleRoot.getAttribute(GCConstants.TRACKING_NO);
    String sCartonStatus = eleRoot.getAttribute(GCConstants.CARTON_STATUS);


    // Creating input Template for getShipmentList

    YFCDocument getShipmentListIp = YFCDocument.getDocumentFor(getShipmentListInput(inDoc));

    // Invoke getShipmentList API with getShipmentListIp document

    YFCDocument getShipmentListTemp = YFCDocument.getDocumentFor(GCConstants.GET_SHIPMENT_LIST_OP_TMP);
    YFCDocument getShipmentListOpDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, getShipmentListIp, getShipmentListTemp);

    YFCElement elertt = getShipmentListOpDoc.getDocumentElement();
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getShipmentListOpDoc : " + getShipmentListOpDoc.toString());
    }

    YFCDocument getCartonListOpDoc = YFCDocument.createDocument(GCConstants.CONTAINER_LIST);
    final YFCNodeList<YFCElement> nlShipmentList = elertt.getElementsByTagName(GCConstants.SHIPMENT);
    final int lengthShipment = nlShipmentList.getLength();
    if (lengthShipment == 0) {
      return getCartonListOpDoc.getDocument();
    }
    YFCElement elert = getCartonListOpDoc.getDocumentElement();
    YFCElement eleShipmentList = elertt.getChildElement(GCConstants.SHIPMENT);

    // Iterating over the ShipmentList

    LOGGER.verbose("LengthShipment :" + lengthShipment);
    for (int j = 0; j < lengthShipment; j++) {
      eleShipmentList = nlShipmentList.item(j);
      YFCElement eleBilToAddress = eleShipmentList.getChildElement(GCXmlLiterals.BILL_TO_ADDRESS);
      String sOrderno = eleShipmentList.getAttribute(GCConstants.ORDER_NO);
      String sOrderHeader = eleShipmentList.getAttribute(GCConstants.ORDER_HEADER_KEY);
      String sCustomerFirstName = eleBilToAddress.getAttribute(GCConstants.FIRST_NAME);
      String sCustomerLastName = eleBilToAddress.getAttribute(GCConstants.LAST_NAME);
      String sShipnode = eleShipmentList.getAttribute(GCConstants.SHIP_NODE);
      String sShipmentLineStatus=eleShipmentList.getAttribute(GCConstants.STATUS);
      //GCSTORE-6200-starts
      LOGGER.debug("Shipment Line Status ::"+sShipmentLineStatus);
      if(GCConstants.CANCELLED_ORDER_STATUS_CODE.equals(sShipmentLineStatus))
      {
          continue;
      }
      //GCSTORE-6200-ends
      YFCElement extnn = eleShipmentList.getChildElement(GCConstants.EXTN);
      String sOrderDate = extnn.getAttribute(GCConstants.EXTN_ORDER_DATE);
      YFCElement eleContainersList = eleShipmentList.getChildElement(GCConstants.CONTAINERS);
      YFCElement eleContainerList = eleContainersList.getChildElement(GCConstants.CONTAINER);
      YFCElement eleBillto = eleShipmentList.getChildElement(GCXmlLiterals.BILL_TO_ADDRESS);
      YFCElement eleToAddress = eleShipmentList.getChildElement(GCXmlLiterals.TO_ADDRESS);

      // Iterate over the ContainerList

      final YFCNodeList<YFCNode> nlContainerList = eleContainersList.getChildNodes();
      final int elelengthContainerList = nlContainerList.getLength();
      if (elelengthContainerList > 0) {
        for (int k = 0; k < elelengthContainerList; k++) {

          eleContainerList = (YFCElement) nlContainerList.item(k);
          String sTrackno = eleContainerList.getAttribute(GCConstants.TRACKING_NO);
          String sShipmentContainerKey = eleContainerList.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
          YFCElement extn1 = eleContainerList.getChildElement(GCConstants.EXTN);
          String sExtnCartonStatus1 = extn1.getAttribute(GCConstants.EXTN_CONTAINER_STATUS);


          // putting up all the if and else condition

          if ((YFCCommon.isVoid(sTrackingNo) && YFCCommon.isVoid(sCartonStatus))) {
            YFCElement eleContainerOp = elert.createChild(GCConstants.CONTAINER);
            eleContainerOp.setAttribute(GCConstants.STATUS, sExtnCartonStatus1);
            eleContainerOp.setAttribute(GCConstants.SHIP_NODE, sShipnode);
            eleContainerOp.setAttribute(GCConstants.CUSTOMER_FIRST_NAME, sCustomerFirstName);
            eleContainerOp.setAttribute(GCConstants.CUSTOMER_LAST_NAME, sCustomerLastName);
            eleContainerOp.setAttribute(GCConstants.TRACKING_NO, sTrackno);
            eleContainerOp.setAttribute(GCConstants.SHIPMENT_CONTAINER_KEY, sShipmentContainerKey);
            eleContainerOp.setAttribute(GCConstants.ORDER_NO, sOrderno);
            eleContainerOp.setAttribute(GCConstants.ORDER_DATE, sOrderDate);
            eleContainerOp.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeader);
            elert.importNode(eleContainerOp);
            eleContainerOp.importNode(eleBillto);
            eleContainerOp.importNode(eleToAddress);
          } else if (!(YFCCommon.isVoid(sTrackingNo)) && (YFCUtils.equals(sTrackingNo, sTrackno))) {
            YFCElement eleContainerOp = elert.createChild(GCConstants.CONTAINER);
            eleContainerOp.setAttribute(GCConstants.STATUS, sExtnCartonStatus1);
            eleContainerOp.setAttribute(GCConstants.SHIP_NODE, sShipnode);
            eleContainerOp.setAttribute(GCConstants.CUSTOMER_FIRST_NAME, sCustomerFirstName);
            eleContainerOp.setAttribute(GCConstants.CUSTOMER_LAST_NAME, sCustomerLastName);
            eleContainerOp.setAttribute(GCConstants.TRACKING_NO, sTrackingNo);
            eleContainerOp.setAttribute(GCConstants.SHIPMENT_CONTAINER_KEY, sShipmentContainerKey);
            eleContainerOp.setAttribute(GCConstants.ORDER_NO, sOrderno);
            eleContainerOp.setAttribute(GCConstants.ORDER_DATE, sOrderDate);
            eleContainerOp.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeader);
            elert.importNode(eleContainerOp);
            eleContainerOp.importNode(eleBillto);
            eleContainerOp.importNode(eleToAddress);
            
          } else if (!(YFCCommon.isVoid(sCartonStatus)) && (YFCUtils.equals(sExtnCartonStatus1, sCartonStatus))
              && (YFCCommon.isVoid(sTrackingNo))) {
            YFCElement eleContainerOp = elert.createChild(GCConstants.CONTAINER);
            eleContainerOp.setAttribute(GCConstants.STATUS, sExtnCartonStatus1);
            eleContainerOp.setAttribute(GCConstants.SHIP_NODE, sShipnode);
            eleContainerOp.setAttribute(GCConstants.CUSTOMER_FIRST_NAME, sCustomerFirstName);
            eleContainerOp.setAttribute(GCConstants.CUSTOMER_LAST_NAME, sCustomerLastName);
            eleContainerOp.setAttribute(GCConstants.TRACKING_NO, sTrackno);
            eleContainerOp.setAttribute(GCConstants.SHIPMENT_CONTAINER_KEY, sShipmentContainerKey);
            eleContainerOp.setAttribute(GCConstants.ORDER_NO, sOrderno);
            eleContainerOp.setAttribute(GCConstants.ORDER_DATE, sOrderDate);
            eleContainerOp.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeader);
            elert.importNode(eleContainerOp);
            eleContainerOp.importNode(eleBillto);
            eleContainerOp.importNode(eleToAddress);
          }
        }
      } else {
        YFCElement eleDummyContainer = elert.createChild(GCConstants.CONTAINER);
        eleDummyContainer.setAttribute(GCConstants.STATUS, GCConstants.CREATED);
        eleDummyContainer.setAttribute(GCConstants.SHIP_NODE, sShipnode);
        eleDummyContainer.setAttribute(GCConstants.CUSTOMER_FIRST_NAME, sCustomerFirstName);
        eleDummyContainer.setAttribute(GCConstants.CUSTOMER_LAST_NAME, sCustomerLastName);
        eleDummyContainer.setAttribute(GCConstants.TRACKING_NO, "");
		eleDummyContainer.setAttribute(GCConstants.ORDER_NO, sOrderno);
        eleDummyContainer.setAttribute(GCConstants.ORDER_DATE, sOrderDate);
        eleDummyContainer.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeader);
        elert.importNode(eleDummyContainer);
        eleDummyContainer.importNode(eleBillto);
        eleDummyContainer.importNode(eleToAddress);
      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getCartonListOpDoc : " + getCartonListOpDoc.toString());
    }
    LOGGER.verbose("GCGetCartonListSearch.getCartonList--End");
    LOGGER.endTimer("GCGetCartonListSearch.getCartonList");
    return getCartonListOpDoc.getDocument();
  }

  /**
   * Description This method is used to prepare the getShipmentListInput Document which will invoke
   * the getShipmentList API
   *
   * @param inDoc
   * @return
   */
  public Document getShipmentListInput(Document inDoc) {
    LOGGER.beginTimer("GCGetCartonListSearch.getShipmentListInput");
    LOGGER.verbose("GCGetCartonListSearch.getShipmentListInput--Begin");

    // Fetching the required attributes from inDoc
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("inputDoc : " + inputDoc.toString());
    }
    final YFCElement eleInputDoc = inputDoc.getDocumentElement();
    String sOrderNo = eleInputDoc.getAttribute(GCConstants.ORDER_NO);
    String sLastName = eleInputDoc.getAttribute(GCConstants.LAST_NAME);
    String sEmailId = eleInputDoc.getAttribute(GCConstants.EMAIL_ID);
    String sPhoneNo = eleInputDoc.getAttribute(GCConstants.PHONE_NO);
    String sZip = eleInputDoc.getAttribute(GCConstants.ZIP_CODE);
    String sTrackingNo = eleInputDoc.getAttribute(GCConstants.TRACKING_NO);
    String sFromOrderDate = eleInputDoc.getAttribute(GCConstants.FROM_ORDER_DATE);
    String sToOrderDate = eleInputDoc.getAttribute(GCConstants.TO_ORDER_DATE);
    String sCartonStatus = eleInputDoc.getAttribute(GCConstants.CARTON_STATUS);
    String sShipNode = eleInputDoc.getAttribute(GCConstants.SHIP_NODE);
    String sMultipleShipNode = eleInputDoc.getAttribute(GCConstants.IS_MULTIPLE_SHIPNODE_FLAG);

    // start preparing the Input getShipmentList Doc

    YFCDocument getShipmentListIp = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement rootEle = getShipmentListIp.getDocumentElement();
    rootEle.setAttribute(GCConstants.SHIP_NODE, sShipNode);
    YFCElement eleShipnode = rootEle.createChild(GCConstants.SHIP_NODE);
    eleShipnode.setAttribute("NodeType", GCConstants.STORE);
    

    YFCElement eleShipmentLines = rootEle.createChild(GCConstants.SHIPMENT_LINES);
    YFCElement eleShipmentLine = eleShipmentLines.createChild(GCConstants.SHIPMENT_LINE);
	YFCElement eleOrder = eleShipmentLine.createChild(GCConstants.ORDER);
    if (!(YFCCommon.isVoid(sOrderNo))) {
      eleShipmentLine.setAttribute(GCConstants.ORDER_NO, sOrderNo);
    }

    if (!(YFCCommon.isVoid(sFromOrderDate) && YFCCommon.isVoid(sToOrderDate))) {
      eleOrder.setAttribute(GCConstants.FROM_ORDER_DATE, sFromOrderDate);
      eleOrder.setAttribute(GCConstants.TO_ORDER_DATE, sToOrderDate);
    } else {
      YDate date = new YDate(false);
      eleOrder.setAttribute(GCConstants.TO_ORDER_DATE, date.getString("yyyy-MM-dd'T'HH:mm:ss"));
      eleOrder.setAttribute(GCConstants.FROM_ORDER_DATE, GCDateUtils.getFromDate(date, 90));
    }
    eleOrder.setAttribute("OrderDateQryType", "BETWEEN");

    YFCElement eleBillToAddress = rootEle.createChild(GCXmlLiterals.BILL_TO_ADDRESS);

    if (!(YFCCommon.isVoid(sLastName) && YFCCommon.isVoid(sEmailId) && YFCCommon.isVoid(sPhoneNo) && YFCCommon
        .isVoid(sZip))) {
      eleBillToAddress.setAttribute(GCConstants.LAST_NAME, sLastName);
      eleBillToAddress.setAttribute(GCConstants.EMAIL_ID, sEmailId);
      eleBillToAddress.setAttribute(GCConstants.ZIP_CODE, sZip);
      eleBillToAddress.setAttribute(GCConstants.DAY_PHONE, sPhoneNo);
    }

    
    if (!(YFCCommon.isVoid(sTrackingNo) && YFCCommon.isVoid(sCartonStatus))) {
	YFCElement eleContainers = rootEle.createChild(GCConstants.CONTAINERS);
    YFCElement eleContainer = eleContainers.createChild(GCConstants.CONTAINER);
    YFCElement eleExtn = eleContainer.createChild(GCConstants.EXTN);
      eleContainer.setAttribute(GCConstants.TRACKING_NO, sTrackingNo);
      eleExtn.setAttribute(GCConstants.EXTN_CONTAINER_STATUS, sCartonStatus);
    }


    if (!(YFCCommon.isVoid(sShipNode))) {
      rootEle.setAttribute(GCConstants.SHIP_NODE, sShipNode);
    } else if (YFCUtils.equals(GCConstants.FLAG_Y, sMultipleShipNode)) {
      final YFCNodeList<YFCElement> nlShipNode = eleInputDoc.getElementsByTagName(GCConstants.SHIP_NODE);
      final int lengthShipnl = nlShipNode.getLength();
      YFCElement eleComplexQuery = rootEle.createChild("ComplexQuery");
      eleComplexQuery.setAttribute("Operator", "AND");
      YFCElement eleAnd = eleComplexQuery.createChild("And");
      YFCElement eleOr = eleAnd.createChild("Or");
      LOGGER.verbose("lengthShipnl :" + lengthShipnl);
      for (int j = 0; j < lengthShipnl; j++) {
        final YFCElement eleShipNode = nlShipNode.item(j);
        String sShipnodevalue = eleShipNode.getAttribute(GCConstants.SHIP_NODE);
        YFCElement eleExp = eleOr.createChild("Exp");
        eleExp.setAttribute(GCConstants.NAME, "ShipNode");
        eleExp.setAttribute(GCConstants.VALUE, sShipnodevalue);
        eleExp.setAttribute(GCConstants.QRY_TYPE, "EQ");
      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getShipmentListIp : " + getShipmentListIp.toString());
    }
    LOGGER.verbose("GCGetCartonListSearch.getShipmentListInput--End");
    LOGGER.endTimer("GCGetCartonListSearch.getShipmentListInput");

    return getShipmentListIp.getDocument();

  }
}