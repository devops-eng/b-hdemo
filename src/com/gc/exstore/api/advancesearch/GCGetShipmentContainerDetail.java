package com.gc.exstore.api.advancesearch;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.exstore.api.picking.GCPickDetailAPI;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;


public class GCGetShipmentContainerDetail {
  // Initialize LOGGER
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCGetShipmentContainerDetail.class.getName());

  /**
   * This method call the getShipmentContainerDetail with ShipmentContainerKey and return the Output
   * Document
   *
   * @param env
   * @param inpDoc
   * @return
   */


  public Document getShipmentContainerDetail(YFSEnvironment env, Document inpDoc) {
    LOGGER.beginTimer("GCgetShipmentContainerDetail.getShipmentContainerDetail");
    LOGGER.verbose("GCgetShipmentContainerDetail.getShipmentContainerDetail--Begin");
    YFCDocument inpuDoc = YFCDocument.getDocumentFor(inpDoc);
    YFCElement elerootinput = inpuDoc.getDocumentElement();
    String sShpContainerKey = elerootinput.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
    String sTrackingNO = elerootinput.getAttribute(GCConstants.TRACKING_NO);
    elerootinput.removeAttribute(GCConstants.TRACKING_NO);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("inpuDoc : " + inpuDoc.toString());
    }

    String sParentItemID = null;
    String ssItemid = null;
    String sQuantity = null;
    // calling getShipmentContainerDetailsAPI

    YFCDocument getShipmentCntDtltmp = YFCDocument.getDocumentFor(GCConstants.GET_SHIPMENT_CONTAINER_DETALS_TMP);


    YFCDocument getShipmentCntDtlOpDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_CONTAINER_DETAILS, inpuDoc, getShipmentCntDtltmp);

    // Fetching the required attributes from the getShipmentCntDtlOpDoc
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getShipmentCntDtlOpDoc : " + getShipmentCntDtlOpDoc.toString());
    }

    final YFCElement eleShipmntCntOp = getShipmentCntDtlOpDoc.getDocumentElement();
    String sTrackingNo = eleShipmntCntOp.getAttribute(GCConstants.TRACKING_NO);
    String sContainerNo = eleShipmntCntOp.getAttribute(GCConstants.CONTAINER_NO);
    YFCElement eleShipment = eleShipmntCntOp.getChildElement(GCConstants.SHIPMENT);
    YFCElement eleContainerDetails = eleShipmntCntOp.getChildElement(GCConstants.CONTAINER_DETAILS);
    YFCElement eleContainerDetail = eleContainerDetails.getChildElement(GCConstants.CONTAINER_DETAIL);
    String sShipmentNo = eleShipment.getAttribute(GCConstants.SHIPMENT_NO);
    String sOrderHeader = eleShipment.getAttribute(GCConstants.ORDER_HEADER_KEY);
    String sShipNode = eleShipment.getAttribute(GCConstants.SHIP_NODE);
    String sShipmentKey = eleShipment.getAttribute(GCConstants.SHIPMENT_KEY);
    YFCElement eleExtn = eleShipment.getChildElement(GCConstants.EXTN);
    String sOrderNo = eleShipment.getAttribute(GCConstants.ORDER_NO);
    String sShipDate = eleShipment.getAttribute(GCConstants.SHIP_DATE);
    YFCElement eleExtn1 = eleShipmntCntOp.getChildElement(GCConstants.EXTN);
    String sStatus = eleExtn1.getAttribute(GCConstants.EXTN_CONTAINER_STATUS);
    String sOrderDate = eleExtn.getAttribute(GCConstants.EXTN_ORDER_DATE);

    String sFulfillmentType = eleExtn.getAttribute(GCConstants.EXTN_FULFILLMENT_TYPE);
    String sShipMethod = eleShipment.getAttribute(GCConstants.LEVEL_OF_SERVICE);
    GCPickDetailAPI obj = new GCPickDetailAPI();
    final String levelOfServiceDesc = obj.callCommonCodeList(env, sShipMethod);

    // Invoking getShipmentList Api for the Set Item.
    YFCDocument getShipmentListIp = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement elegetShipmentListIp = getShipmentListIp.getDocumentElement();
    elegetShipmentListIp.setAttribute(GCConstants.SHIPMENT_KEY, sShipmentKey);
    elegetShipmentListIp.setAttribute(GCConstants.TRACKING_NO, sTrackingNO);
    YFCDocument getShipmentListopTemp = YFCDocument.getDocumentFor(GCConstants.GET_SHIPMENT_LIST_OUT_TMP);
    YFCDocument getShipmentListOutputDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, getShipmentListIp, getShipmentListopTemp);

    YFCElement eleGetShipmentListOutput = getShipmentListOutputDoc.getDocumentElement();
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getShipmentListOutputDoc : " + getShipmentListOutputDoc.toString());
    }
    YFCElement eleGetShipmentListShip = eleGetShipmentListOutput.getChildElement(GCConstants.SHIPMENT);
    YFCElement eleBillTo = eleGetShipmentListShip.getChildElement(GCXmlLiterals.BILL_TO_ADDRESS);
    YFCElement eletoadds = eleGetShipmentListShip.getChildElement(GCXmlLiterals.TO_ADDRESS);
    YFCElement eleContainers = eleGetShipmentListShip.getChildElement(GCConstants.CONTAINERS);

    String bLastName = eleBillTo.getAttribute(GCConstants.LAST_NAME);
    String bFirstName = eleBillTo.getAttribute(GCConstants.FIRST_NAME);
    String bEmailId = eleBillTo.getAttribute(GCConstants.EMAIL_ID);
    String bPhoneNo = eleBillTo.getAttribute(GCConstants.DAY_PHONE);


    final YFCNodeList<YFCElement> nlContainerList = eleContainers.getElementsByTagName(GCConstants.CONTAINER);
    nlContainerList.getLength();

    for (YFCElement eleContainer : nlContainerList) {

      String sShipcontkey = eleContainer.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
      if (YFCUtils.equals(sShipcontkey, sShpContainerKey)) {
        YFCElement eleContainerDetailsShoutput = eleContainer.getChildElement(GCConstants.CONTAINER_DETAILS);
        final YFCNodeList<YFCElement> nlContainerDetailList =
            eleContainerDetailsShoutput.getElementsByTagName(GCConstants.CONTAINER_DETAIL);
        for (YFCElement eleContainerDetailhoutput : nlContainerDetailList) {
          YFCElement eleShipmentline = eleContainerDetailhoutput.getChildElement(GCConstants.SHIPMENT_LINE);
          String sParentShipmentLineKey = eleShipmentline.getAttribute(GCConstants.PARENT_SHPMNTLINE_KEY);
          if (!(YFCCommon.isVoid(sParentShipmentLineKey))) {

            String sQuantitycomp = eleShipmentline.getAttribute(GCConstants.QUANTITY);
            YFCElement eleShipmentLines = eleGetShipmentListShip.getChildElement(GCConstants.SHIPMENT_LINES);

            final YFCNodeList<YFCElement> nlShipmentLine1 =
                eleShipmentLines.getElementsByTagName(GCConstants.SHIPMENT_LINE);

            for (YFCElement eleShipmentLineOfShipment : nlShipmentLine1) {
              String sShipmentLkey1 = eleShipmentLineOfShipment.getAttribute(GCConstants.SHIPMENT_LINE_KEY);
              if ((YFCCommon.equals(sParentShipmentLineKey, sShipmentLkey1))) {
                String sQuantityParent = eleShipmentLineOfShipment.getAttribute(GCConstants.QUANTITY);
                Double qty1 = (Double.parseDouble(sQuantitycomp) / Double.parseDouble(sQuantityParent));
                String sQuantitycompcd = eleContainerDetailhoutput.getAttribute(GCConstants.QUANTITY);
                Double qtyParent = Double.parseDouble(sQuantitycompcd) / qty1;
                sQuantity = String.valueOf(qtyParent);
                sParentItemID = eleShipmentLineOfShipment.getAttribute(GCConstants.ITEM_ID);
              }

            }
          }
        }
      }
    }

    // Creating new document for creating new Container Element.

    YFCDocument getShipmentContainDetail = YFCDocument.createDocument(GCConstants.CONTAINER);
    YFCElement eleShipmntCntdtl = getShipmentContainDetail.getDocumentElement();
    YFCElement eleShipment1 = eleShipmntCntdtl.createChild(GCConstants.SHIPMENT);
    eleShipment1.setAttribute(GCConstants.SHIP_DATE, sShipDate);
    eleShipment1.setAttribute(GCConstants.SHIPMENT_KEY, sShipmentKey);
    eleShipment1.setAttribute(GCConstants.SHIP_NODE, sShipNode);


    // appending all the elements and attributes to the new container element

    eleShipmntCntdtl.setAttribute(GCConstants.TRACKING_NO, sTrackingNo);
    eleShipmntCntdtl.setAttribute(GCConstants.CONTAINER_NO, sContainerNo);
    eleShipmntCntdtl.setAttribute(GCConstants.FIRST_NAME, bFirstName);
    eleShipmntCntdtl.setAttribute(GCConstants.LAST_NAME, bLastName);
    eleShipmntCntdtl.setAttribute(GCConstants.EMAIL_ID, bEmailId);
    eleShipmntCntdtl.setAttribute(GCConstants.DAY_PHONE, bPhoneNo);
    eleShipmntCntdtl.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeader);
    eleShipmntCntdtl.setAttribute(GCConstants.SHIPMENT_NO, sShipmentNo);
    eleShipmntCntdtl.setAttribute(GCConstants.LEVEL_OF_SERVICE, levelOfServiceDesc);
    eleShipmntCntdtl.setAttribute(GCConstants.EXTN_STATUS, sStatus);
    eleShipmntCntdtl.setAttribute(GCConstants.EXTN_ORDER_DATE, sOrderDate);
    eleShipmntCntdtl.setAttribute(GCConstants.EXTN_FULFILLMENT_TYPE, sFulfillmentType);
    eleShipmntCntdtl.importNode(eleBillTo);
    eleShipmntCntdtl.importNode(eletoadds);
    // iterate through shipment lines in the getShipmentContainerDetailsopt

    final YFCNodeList<YFCNode> nlShipmentLine = eleContainerDetail.getChildNodes();
    final int lengthShipmntnl = nlShipmentLine.getLength();
    LOGGER.verbose("lengthShipmntnl :" + lengthShipmntnl);
    for (int j = 0; j < lengthShipmntnl; j++) {
      final YFCElement eleShipmentLine = (YFCElement) nlShipmentLine.item(j);
      String sShippedQty = eleShipmentLine.getAttribute(GCConstants.QUANTITY);
      if (YFCCommon.isVoid(sParentItemID)) {
        ssItemid = eleShipmentLine.getAttribute(GCConstants.ITEM_ID);
      } else {
        ssItemid = sParentItemID;
        sShippedQty = sQuantity;
      }
      YFCDocument getitemlistindoc = YFCDocument.createDocument(GCConstants.ITEM);
      YFCElement eleItemList = getitemlistindoc.getDocumentElement();
      eleItemList.setAttribute(GCConstants.ITEM_ID, ssItemid);
      eleItemList.setAttribute("IgnoreIsSoldSeparately", "Y");
      YFCDocument getItemLisoptmp = YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LIST_TMP);
      YFCDocument getItemListodoc =
          GCCommonUtil.invokeAPI(env, GCConstants.GET_ITEM_LIST, getitemlistindoc, getItemLisoptmp);

      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("getItemListodoc : " + getItemListodoc.toString());
      }

      String sItemDescription = eleShipmentLine.getAttribute(GCConstants.ITEM_DESCRIPTION);
      final YFCElement elertIl = getItemListodoc.getDocumentElement();
      YFCElement eleItem = elertIl.getChildElement(GCConstants.ITEM);
      YFCElement elePrimaryInformation = eleItem.getChildElement(GCConstants.PRIMARY_INFORMATION);
      String sImageID = elePrimaryInformation.getAttribute(GCConstants.IMG_ID);
      String sImageLocation = elePrimaryInformation.getAttribute(GCConstants.IMG_LOCATION);
      String sImageIDloc = sImageLocation + "/" + sImageID;
      YFCElement extn4 = eleItem.getChildElement(GCConstants.EXTN);
      String sbrand = extn4.getAttribute(GCConstants.EXTN_BRAND_NAME);
      String sItemId = extn4.getAttribute(GCConstants.EXTN_POSITEM_ID);
      YFCElement eleShipmentLineNew = eleShipmntCntdtl.createChild(GCConstants.SHIPMENT_LINE);
      eleShipmentLineNew.setAttribute(GCConstants.ITEM_DESCRIPTION, sItemDescription);
      eleShipmentLineNew.setAttribute(GCConstants.EXTN_BRAND_NAME, sbrand);
      eleShipmentLineNew.setAttribute(GCConstants.EXTN_POSITEM_ID, sItemId);
      eleShipmentLineNew.setAttribute(GCConstants.TRACKING_NO, sTrackingNo);
      eleShipmentLineNew.setAttribute(GCConstants.CONTAINER_NO, sContainerNo);
      eleShipmentLineNew.setAttribute(GCConstants.QUANTITY, sShippedQty);
      eleShipmentLineNew.setAttribute(GCConstants.ORDER_NO, sOrderNo);
      eleShipmentLineNew.setAttribute("ImageURL", sImageIDloc);
      eleShipmntCntdtl.importNode(eleShipmentLineNew);
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getShipmentContainDetail : " + getShipmentContainDetail.toString());
    }
    LOGGER.verbose("GCgetShipmentContainerDetail.getShipmentContainerDetail--End");
    LOGGER.endTimer("GCgetShipmentContainerDetail.getShipmentContainerDetail");
    return getShipmentContainDetail.getDocument();
  }
}