package com.gc.exstore.api.customerpick;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCLogUtils;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;


public class GCGetReadyForPickUpListAndDetails {
  // Initialise LOGGER
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCGetReadyForPickUpListAndDetails.class
      .getName());

  /**
   * This method is used for getting the Ready for Pick up List group by OrderNo.
   *
   * @param env
   * @param inpDoc
   * @return
   */
  public Document getReadyForPickUpList(YFSEnvironment env, Document inpDoc) {
    LOGGER.beginTimer("GCGetReadyForPickUpListAndDetails.getReadyForPickUpList");
    GCLogUtils.verboseLog(LOGGER, "GCGetReadyForPickUpListAndDetails.getReadyForPickUpList");
    String aging = null;
    String orderHeaderKey = null;
    String orderNo = null;
    String purchaseOrdNo = null;
    String purchaseOrderHeaderKey = null;
    YFCNodeList<YFCElement> nlShipmentList;
    YFCDocument inpuDoc = YFCDocument.getDocumentFor(inpDoc);
    GCLogUtils.verboseLog(LOGGER, "inpuDoc : ", inpuDoc.toString());
    YFCElement eleInput = inpuDoc.getDocumentElement();
    String orderNoIp = eleInput.getAttribute(GCConstants.ORDER_NO);
    String pickUpStoreIp = eleInput.getAttribute(GCConstants.PICK_UP_STORE);

    List<String> listOrderNo = new ArrayList<String>();


    // Creating Input Template for getShipmentList

    YFCDocument getShipmentListIp = getShipmentListInput(inpDoc);

    YFCDocument getShipmentListopTemp = YFCDocument.getDocumentFor(GCConstants.GET_SHIPMENT_LIST_OUT_TMP);


    YFCDocument getShipmentListOpDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, getShipmentListIp, getShipmentListopTemp);

    YFCElement eleOutputDocument = getShipmentListOpDoc.getDocumentElement();
    GCLogUtils.verboseLog(LOGGER, "getShipmentListOpDoc : ", getShipmentListOpDoc.toString());

    YFCDocument getReadyForPickUpListOpDoc = YFCDocument.createDocument(GCConstants.ORDER_LIST);
    YFCElement eleOrderList = getReadyForPickUpListOpDoc.getDocumentElement();

    nlShipmentList = eleOutputDocument.getElementsByTagName(GCConstants.SHIPMENT);
    if (!YFCCommon.isVoid(orderNoIp) && nlShipmentList.getLength() == 0) {
      nlShipmentList = getShipmentListFromPurchaseOrders(env, orderNoIp);
    }
    if (!YFCCommon.isVoid(nlShipmentList)) {
      for (YFCElement eleShipment : nlShipmentList) {
        YFCElement eleShipmentLines = eleShipment.getChildElement(GCConstants.SHIPMENT_LINES);
        YFCElement eleShipmentLine = eleShipmentLines.getChildElement(GCConstants.SHIPMENT_LINE);
        String docType = eleShipmentLine.getAttribute(GCConstants.DOCUMENT_TYPE);
        String sItemDesc = eleShipmentLine.getAttribute(GCConstants.ITEM_DESCRIPTION);
        if (YFCUtils.equals(GCConstants.ZERO_ZERO_ZERO_FIVE, docType)) {
          YFCElement eleOrderLine = eleShipmentLine.getChildElement(GCConstants.ORDER_LINE);
          orderHeaderKey = eleOrderLine.getAttribute(GCConstants.CHAINED_FROM_ORDER_HEADER_KEY);
          YFCDocument getOrderListIp = YFCDocument.createDocument(GCConstants.ORDER);
          YFCElement eleOrder = getOrderListIp.getDocumentElement();
          eleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
          YFCDocument getOrderListTemp = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_SALES_ORD_TEMP);
          YFCDocument getOrderListDoc =
              GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListIp, getOrderListTemp);
          YFCElement eleOrderListOp = getOrderListDoc.getDocumentElement();
          YFCElement eleOrderIp = eleOrderListOp.getChildElement(GCConstants.ORDER);
          orderNo = eleOrderIp.getAttribute(GCConstants.ORDER_NO);
          purchaseOrdNo = eleShipmentLine.getAttribute(GCConstants.ORDER_NO);
          purchaseOrderHeaderKey = eleShipmentLine.getAttribute(GCConstants.ORDER_HEADER_KEY);
        } else {
          orderNo = eleShipmentLine.getAttribute(GCConstants.ORDER_NO);
          orderHeaderKey = eleShipmentLine.getAttribute(GCConstants.ORDER_HEADER_KEY);
        }
        YFCElement eleExtnShipment = eleShipment.getChildElement(GCConstants.EXTN);
        String pickUpStore = eleExtnShipment.getAttribute(GCConstants.EXTN_PICK_UP_STORE_ID);
        if (YFCCommon.isVoid(orderNoIp) && !listOrderNo.isEmpty() && listOrderNo.contains(orderNo)) {
          continue;
        }


        YFCElement eleToAddress = eleShipment.getChildElement(GCXmlLiterals.TO_ADDRESS);
        String sFirstName = eleToAddress.getAttribute(GCConstants.FIRST_NAME);
        String sLastName = eleToAddress.getAttribute(GCConstants.LAST_NAME);
        YFCElement eleContainers = eleShipment.getChildElement(GCConstants.CONTAINERS);

        final YFCNodeList<YFCElement> nlContainerList = eleContainers.getElementsByTagName(GCConstants.CONTAINER);


        for (YFCElement eleContainer : nlContainerList) {
          YFCElement eleExtn = eleContainer.getChildElement(GCConstants.EXTN);
          String sStatus = eleExtn.getAttribute(GCConstants.EXTN_CONTAINER_STATUS);
          if (YFCUtils.equals(sStatus, GCConstants.READY_FOR_CUSTOMER_PICK_UP)) {
            YFCElement eleOrder = eleOrderList.createChild(GCConstants.ORDER);

            YFCElement eleExtn1 = eleShipment.getChildElement(GCConstants.EXTN);

            String sQueueDate = eleExtn1.getAttribute(GCConstants.EXTN_PICK_UP_DATE);
            if (!YFCCommon.isVoid(sQueueDate)) {
              aging =
                  GCDateUtils.getTimeDiffrenceInDaysHoursAndMinutes(sQueueDate,
                      GCDateUtils.getCurrentTime(GCConstants.DATE_TIME_FORMAT));
            }
            eleOrder.setAttribute(GCConstants.ORDER_NO, orderNo);
            eleOrder.setAttribute(GCConstants.FIRST_NAME, sFirstName);
            eleOrder.setAttribute(GCConstants.LAST_NAME, sLastName);
            eleOrder.setAttribute(GCConstants.AGING, aging);
            eleOrder.setAttribute(GCConstants.QUEUE_DATE, sQueueDate);
            eleOrder.setAttribute(GCConstants.ITEM_DESCRIPTION, sItemDesc);
            eleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
            if (!YFCCommon.isVoid(purchaseOrdNo)) {
              eleOrder.setAttribute(GCConstants.PURCHASE_ORDER_NO, purchaseOrdNo);
              eleOrder.setAttribute(GCConstants.PURCHASE_HEADER_KEY, purchaseOrderHeaderKey);
            } else {
              eleOrder.setAttribute(GCConstants.PURCHASE_ORDER_NO, GCConstants.BLANK_STRING);
              eleOrder.setAttribute(GCConstants.PURCHASE_HEADER_KEY, GCConstants.BLANK_STRING);
            }
            eleOrderList.importNode(eleOrder);

            if (!YFCCommon.isVoid(orderNoIp)) {
              YFCElement eleOrderRes = eleOrderList.getChildElement(GCConstants.ORDER);
              if (!YFCCommon.isVoid(eleOrderRes) && !YFCUtils.equals(pickUpStore, pickUpStoreIp)) {
                eleOrderList.removeChild(eleOrderRes);
                eleOrderList.setAttribute(GCConstants.OTHER_STORE_ORDERS, GCConstants.YES);
              }
              return getReadyForPickUpListOpDoc.getDocument();
            } else {
              listOrderNo.add(orderNo);
              break;
            }
          }

        }
      }
    }
    GCLogUtils.verboseLog(LOGGER, "Ready for pick up list output document : ", getReadyForPickUpListOpDoc.toString());
    GCLogUtils.verboseLog(LOGGER, "GCGetReadyForPickUpListAndDetails.getReadyForPickUpList--End");
    LOGGER.endTimer("GCGetReadyForPickUpListAndDetails.getReadyForPickUpList");
    return getReadyForPickUpListOpDoc.getDocument();



  }

  private YFCNodeList<YFCElement> getShipmentListFromPurchaseOrders(YFSEnvironment env, String orderNoIp) {
    YFCNodeList<YFCElement> nlShipment = null;
    YFCDocument getOrderListIp = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleOrderIp = getOrderListIp.getDocumentElement();
    eleOrderIp.setAttribute(GCConstants.ORDER_NO, orderNoIp);
    YFCDocument getOrderListTemp = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_FOR_PURCHASE_ORD);
    YFCDocument getOrderListOp =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListIp, getOrderListTemp);
    YFCElement eleOrder = getOrderListOp.getElementsByTagName(GCConstants.ORDER).item(0);
    if (!YFCCommon.isVoid(eleOrder)) {
      YFCElement eleOrdExtn = eleOrder.getChildElement(GCConstants.EXTN);
      String purchaseOrderString = eleOrdExtn.getAttribute(GCConstants.EXTN_PURCHASE_ORDER_NUMBERS);
      if (!YFCCommon.isVoid(purchaseOrderString)) {
        String[] purchaseOrders = purchaseOrderString.split(GCConstants.UNDERSCORE);
        for (String purchaseOrderNo : purchaseOrders) {
          Document tempDoc = GCXMLUtil.createDocument(GCConstants.ORDER);
          Element eleTemp = tempDoc.getDocumentElement();
          eleTemp.setAttribute(GCConstants.ORDER_NO, purchaseOrderNo);
          YFCDocument getShipmentListIp = getShipmentListInput(tempDoc);
          YFCDocument getShipmentListopTemp = YFCDocument.getDocumentFor(GCConstants.GET_SHIPMENT_LIST_OUT_TMP);
          YFCDocument getShipmentListOpDoc =
              GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, getShipmentListIp, getShipmentListopTemp);
          YFCElement eleOutputDocument = getShipmentListOpDoc.getDocumentElement();
          nlShipment = eleOutputDocument.getElementsByTagName(GCConstants.SHIPMENT);
          if (nlShipment.getLength() > 0) {

            return nlShipment;

          }
        }
      }
    }
    return nlShipment;
  }


  private YFCElement getShipmentListPurchaseOrdDetPage(YFSEnvironment env, String orderNoIp) {
    YFCNodeList<YFCElement> nlShipment;
    YFCElement eleShipments = null;
    int count = 0;
    YFCDocument getOrderListIp = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleOrderIp = getOrderListIp.getDocumentElement();
    eleOrderIp.setAttribute(GCConstants.ORDER_NO, orderNoIp);
    YFCDocument getOrderListTemp = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_FOR_PURCHASE_ORD);
    YFCDocument getOrderListOp =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListIp, getOrderListTemp);
    YFCElement eleOrder = getOrderListOp.getElementsByTagName(GCConstants.ORDER).item(0);
    YFCElement eleOrdExtn = eleOrder.getChildElement(GCConstants.EXTN);
    String purchaseOrderString = eleOrdExtn.getAttribute(GCConstants.EXTN_PURCHASE_ORDER_NUMBERS);
    if (!YFCCommon.isVoid(purchaseOrderString)) {
      String[] purchaseOrders = purchaseOrderString.split(GCConstants.UNDERSCORE);

      for (String purchaseOrderNo : purchaseOrders) {
        Document tempDoc = GCXMLUtil.createDocument(GCConstants.ORDER);
        Element eleTemp = tempDoc.getDocumentElement();
        eleTemp.setAttribute(GCConstants.ORDER_NO, purchaseOrderNo);
        YFCDocument getShipmentListIp = getShipmentListInput(tempDoc);
        YFCDocument getShipmentListopTemp = YFCDocument.getDocumentFor(GCConstants.GET_SHIPMENT_LIST_OUT_TMP);
        YFCDocument getShipmentListOpDoc =
            GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, getShipmentListIp, getShipmentListopTemp);
        YFCElement eleOutputDocument = getShipmentListOpDoc.getDocumentElement();
        nlShipment = eleOutputDocument.getElementsByTagName(GCConstants.SHIPMENT);
        if (nlShipment.getLength() > 0) {
          if (count == 0) {
            eleShipments = getShipmentListOpDoc.getDocumentElement();
            count = 1;
          } else {
            for (YFCElement eleShipment : nlShipment) {
              eleShipments.importNode(eleShipment);
            }
          }
        }
      }
    }
    return eleShipments;
  }

  /**
   * Description This method is used to prepare the getShipmentListInput Document which will invoke
   * the getShipmentList API
   *
   * @param inDoc
   * @return
   */
  public YFCDocument getShipmentListInput(Document inDoc) {
    LOGGER.beginTimer("GCGetReadyForPickUpListAndDetails.getShipmentListInput");
    GCLogUtils.verboseLog(LOGGER, "GCGetReadyForPickUpListAndDetails.getShipmentListInput--Begin");

    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);

    final YFCElement eleInputDoc = inputDoc.getDocumentElement();
    // Fetching the required attributes from inDoc
    String sOrderNo = eleInputDoc.getAttribute(GCConstants.ORDER_NO);
    String sPickUpStore = eleInputDoc.getAttribute(GCConstants.PICK_UP_STORE);

    // Preparing Input For getShipmentList API call
    YFCDocument getShipmentListIp = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement rootEle = getShipmentListIp.getDocumentElement();
    if (YFCCommon.isVoid(sOrderNo)) {
      YFCElement eleExtn = rootEle.createChild(GCConstants.EXTN);
      eleExtn.setAttribute(GCConstants.EXTN_PICK_UP_STORE, sPickUpStore);
    }
    YFCElement eleContainers = rootEle.createChild(GCConstants.CONTAINERS);
    YFCElement eleContainer = eleContainers.createChild(GCConstants.CONTAINER);
    YFCElement eleExtns1 = eleContainer.createChild(GCConstants.EXTN);
    eleExtns1.setAttribute(GCConstants.EXTN_CONTAINER_STATUS, GCConstants.READY_FOR_CUSTOMER_PICK_UP);
    YFCElement eleShipmentLines = rootEle.createChild(GCConstants.SHIPMENT_LINES);
    YFCElement eleShipmentLine = eleShipmentLines.createChild(GCConstants.SHIPMENT_LINE);
    eleShipmentLine.setAttribute(GCConstants.ORDER_NO, sOrderNo);

    GCLogUtils.verboseLog(LOGGER, "getShipmentList input  : ", getShipmentListIp.toString());
    GCLogUtils.verboseLog(LOGGER, "GCGetReadyForPickUpListAndDetails.getShipmentListInput--End");
    LOGGER.endTimer("GCGetReadyForPickUpListAndDetails.getShipmentListInput");
    return getShipmentListIp;
  }

  /**
   * This method is used for getting the Ready for Pick up Details for the Order selected on List
   * Page.
   *
   * @param inputDoc
   * @return
   */


  public Document getReadyForPickUpDetails(YFSEnvironment env, Document inputDoc) {
    LOGGER.beginTimer("GCGetReadyForPickUpListAndDetails.getReadyForPickUpDetails");
    GCLogUtils.verboseLog(LOGGER, "GCGetReadyForPickUpListAndDetails.getReadyForPickUpDetails--Begin");
    GCLogUtils.verboseLog(LOGGER, "Ready For pick up details input document : ", inputDoc.toString());
    String orderHeaderKey = null;
    // Preparing Input For getShipmentList API call
    Element eleRoot = inputDoc.getDocumentElement();
    String ordeNoIp = eleRoot.getAttribute(GCConstants.ORDER_NO);
    YFCDocument getShipmentListIp = getShipmentListInput(inputDoc);
    YFCDocument getShipmentListopTemp = YFCDocument.getDocumentFor(GCConstants.GET_SHIPMENT_LIST_OUT_TMP);

    // Invoking getShipmentList API
    YFCDocument getShipmentListOutputDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, getShipmentListIp, getShipmentListopTemp);

    YFCElement eleGetShipmentListOutput = getShipmentListOutputDoc.getDocumentElement();
    GCLogUtils.verboseLog(LOGGER, "getShipmentListOutputDoc : ", getShipmentListOutputDoc.toString());

    YFCElement elePurchaseShipments = getShipmentListPurchaseOrdDetPage(env, ordeNoIp);

    YFCDocument getReadyForPickUpDetailsOpDoc = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleOrder = getReadyForPickUpDetailsOpDoc.getDocumentElement();
    YFCElement eleContainerDetailsOut = eleOrder.createChild(GCConstants.CONTAINER_DETAILS);

    final YFCNodeList<YFCElement> nlShipmentList = eleGetShipmentListOutput.getElementsByTagName(GCConstants.SHIPMENT);
    YFCElement eleShipmentForAttr = nlShipmentList.item(0);
    if (!YFCCommon.isVoid(eleShipmentForAttr)) {
      YFCElement eleShipmentLinesForAttr = eleShipmentForAttr.getChildElement(GCConstants.SHIPMENT_LINES);
      YFCElement eleShipmentLineForAttr = eleShipmentLinesForAttr.getChildElement(GCConstants.SHIPMENT_LINE);

      orderHeaderKey = eleShipmentLineForAttr.getAttribute(GCConstants.ORDER_HEADER_KEY);
    } else {
      eleShipmentForAttr = elePurchaseShipments.getChildElement(GCConstants.SHIPMENT);
      YFCElement eleShipmentLinesForAttr = eleShipmentForAttr.getChildElement(GCConstants.SHIPMENT_LINES);
      YFCElement eleShipmentLineForAttr = eleShipmentLinesForAttr.getChildElement(GCConstants.SHIPMENT_LINE);
      YFCElement eleOrderLineForAttr = eleShipmentLineForAttr.getChildElement(GCConstants.ORDER_LINE);
      orderHeaderKey = eleOrderLineForAttr.getAttribute(GCConstants.CHAINED_FROM_ORDER_HEADER_KEY);
    }


    String spoAssociate = getSPOAssociate(orderHeaderKey, env);
    eleOrder.setAttribute(GCConstants.ORDER_USER_NAME, spoAssociate);

    YFCDocument getOrderListTemp = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_OUT_TEMP);
    YFCDocument ordListOpDoc = GCCommonUtil.invokeGetOrderListAPI(env, orderHeaderKey, getOrderListTemp);
    GCLogUtils.verboseLog(LOGGER, "getOrderListOutputDoc : ", ordListOpDoc.toString());
    YFCElement eleOrderList = ordListOpDoc.getDocumentElement();
    YFCElement eleOrderOp = eleOrderList.getChildElement(GCConstants.ORDER);
    // Stamp order level attributes
    stampHeaderAttributes(eleShipmentForAttr, eleOrder, eleOrderOp);
    for (YFCElement eleShipment : nlShipmentList) {
      updateContainerDetails(env, eleContainerDetailsOut, eleOrderOp, eleShipment);
    }

    // Append containers from Purchase Order Shipments

    if (!YFCCommon.isVoid(elePurchaseShipments)) {
      YFCNodeList<YFCElement> nlPurchaseShipment = elePurchaseShipments.getElementsByTagName(GCConstants.SHIPMENT);
      for (YFCElement eleShipment : nlPurchaseShipment) {
        updateContainerDetails(env, eleContainerDetailsOut, eleOrderOp, eleShipment);
      }
    }

    eleOrder.importNode(eleContainerDetailsOut);
    Document getCommonCodeopDoc = GCCommonUtil.getCommonCodeListByType(env, GCConstants.GC_ID_TYPES, GCConstants.GC);
    YFCDocument commonCodeOp = YFCDocument.getDocumentFor(getCommonCodeopDoc);
    YFCElement eleCommonCodeList = commonCodeOp.getDocumentElement();
    eleOrder.importNode(eleCommonCodeList);
    GCLogUtils.verboseLog(LOGGER, "Ready For pick up details output document : ",
        getReadyForPickUpDetailsOpDoc.toString());
    GCLogUtils.verboseLog(LOGGER, "GCGetReadyForPickUpListAndDetails.getReadyForPickUpDetails--End");
    LOGGER.endTimer("GCGetReadyForPickUpListAndDetails.getReadyForPickUpDetails");
    return getReadyForPickUpDetailsOpDoc.getDocument();
  }

  /**
   *
   * @param env
   * @param eleContainerDetailsOut
   * @param eleOrderOp
   * @param eleShipment
   */
  private void updateContainerDetails(YFSEnvironment env, YFCElement eleContainerDetailsOut, YFCElement eleOrderOp,
      YFCElement eleShipment) {

    //Code refactored for Sonar Fixes.
    YFCElement eleContainers = eleShipment.getChildElement(GCConstants.CONTAINERS);
    YFCNodeList<YFCElement> nlContainerList = eleContainers.getElementsByTagName(GCConstants.CONTAINER);
    Map<String, YFCElement> bundleMap = new HashMap<String, YFCElement>();
    Map<String, YFCElement> multiBoxMap = new HashMap<String, YFCElement>();
    for (YFCElement eleContainer : nlContainerList) {
      stampContainerAttributes(env, eleContainerDetailsOut, eleOrderOp, eleShipment, bundleMap, multiBoxMap,
          eleContainer);
    }
  }

  /**
   *
   * @param env
   * @param eleContainerDetailsOut
   * @param eleOrderOp
   * @param eleShipment
   * @param bundleMap
   * @param multiBoxMap
   * @param eleContainer
   */
  private void stampContainerAttributes(YFSEnvironment env, YFCElement eleContainerDetailsOut, YFCElement eleOrderOp,
      YFCElement eleShipment, Map<String, YFCElement> bundleMap, Map<String, YFCElement> multiBoxMap,
      YFCElement eleContainer) {

    //Refactored code for Sonar Violation fixes.
    String shipmentKey = eleShipment.getAttribute(GCConstants.SHIPMENT_KEY);
    String shipmentContainerKey = eleContainer.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
    YFCElement eleContainerExtn = eleContainer.getChildElement(GCConstants.EXTN);
    String sCartonStatus = eleContainerExtn.getAttribute(GCConstants.EXTN_CONTAINER_STATUS);
    String sMultiBoxId = eleContainerExtn.getAttribute(GCConstants.EXTN_MULTI_BOX_GROUP_ID);
    if (YFCCommon.equals(sCartonStatus, GCConstants.READY_FOR_CUSTOMER_PICK_UP)) {
      String sTrackingNo = eleContainer.getAttribute(GCConstants.TRACKING_NO);
      YFCElement eleContainerDetails = eleContainer.getChildElement(GCConstants.CONTAINER_DETAILS);
      YFCNodeList<YFCElement> nlContainerDetailList =
          eleContainerDetails.getElementsByTagName(GCConstants.CONTAINER_DETAIL);
      Map<String, YFCElement> dependentKeyMap = getDependentKeyMapForWarrantyLines(nlContainerDetailList);
      Map<String, YFCElement> setMap = new HashMap<String, YFCElement>();
      for (YFCElement eleContainerDetail : nlContainerDetailList) {
        YFCElement eleContainerDetailExtn = eleContainerDetail.getChildElement(GCConstants.EXTN);
        YFCElement eleOrderLine = eleContainerDetail.getChildElement(GCConstants.ORDER_LINE);
        String chainedFromOrderLineKey = eleOrderLine.getAttribute(GCConstants.CHAINED_FROM_ORDER_LINE_KEY);
        YFCElement eleOrderLineExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
        String sIsWarrantyItem = eleOrderLineExtn.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
        Double extnPickedUpQty = eleContainerDetailExtn.getDoubleAttribute(GCXmlLiterals.EXTN_PICKED_UP_QTY);
        String sContainerDetailQty = eleContainerDetail.getAttribute(GCConstants.QUANTITY);
        YFCElement eleShipmentLine = eleContainerDetail.getChildElement(GCConstants.SHIPMENT_LINE);
        String sItemID = eleShipmentLine.getAttribute(GCConstants.ITEM_ID);
        String sItemDesc = eleShipmentLine.getAttribute(GCConstants.ITEM_DESCRIPTION);
        String sUom = eleShipmentLine.getAttribute(GCConstants.UNIT_OF_MEASURE);
        String sParentShipmentLineKey = eleShipmentLine.getAttribute(GCConstants.PARENT_SHPMNTLINE_KEY);
        String orderLineKey = eleContainerDetail.getAttribute(GCConstants.ORDER_LINE_KEY);
        String orderReleaseKey = eleContainerDetail.getAttribute(GCConstants.ORDER_RELEASE_KEY);
        String containerDetailsKey = eleContainerDetail.getAttribute(GCConstants.CONTAINER_DETAILS_KEY);
        String orderHeaderKey = eleContainerDetail.getAttribute(GCConstants.ORDER_HEADER_KEY);
        String docType = eleContainerDetail.getAttribute(GCConstants.DOCUMENT_TYPE);
        boolean isMultiBoxQty = false;
        if (0 == Double.parseDouble(sContainerDetailQty)
            && YFCUtils.equals(sContainerDetailQty,
                eleContainerDetailExtn.getAttribute(GCXmlLiterals.EXTN_PICKED_UP_QTY))) {
          isMultiBoxQty = true;
        }
        if (YFCCommon.equals(sIsWarrantyItem, GCConstants.NO)
            && (!(extnPickedUpQty >= Double.parseDouble(sContainerDetailQty)) || isMultiBoxQty)) {
          if (!(YFCCommon.isVoid(sParentShipmentLineKey))) {
            if (!setMap.containsKey(sParentShipmentLineKey) && !bundleMap.containsKey(sParentShipmentLineKey)) {
              String productClass = eleContainerDetail.getAttribute(GCConstants.PRODUCT_CLASS);
              Double lineQty = eleShipmentLine.getDoubleAttribute(GCConstants.QUANTITY);
              YFCElement eleContainerDetailOut = eleContainerDetailsOut.createChild(GCConstants.CONTAINER_DETAIL);
              YFCElement eleShipmentLines = eleShipment.getChildElement(GCConstants.SHIPMENT_LINES);
              YFCNodeList<YFCElement> nlShipmentLine =
                  eleShipmentLines.getElementsByTagName(GCConstants.SHIPMENT_LINE);
              YFCElement eleParentShipmentLine = getParentShipmentLine(nlShipmentLine, sParentShipmentLineKey);
              if (!YFCCommon.isVoid(eleParentShipmentLine)) {
                String itemIDParent = eleParentShipmentLine.getAttribute(GCConstants.ITEM_ID);
                String uomParent = eleParentShipmentLine.getAttribute(GCConstants.UNIT_OF_MEASURE);
                String itemDescParent = eleParentShipmentLine.getAttribute(GCConstants.ITEM_DESCRIPTION);
                String orderLineKeyParent = eleParentShipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY);
                String posItemId = getPosItemID(env, itemIDParent, uomParent);
                String parentQty = eleParentShipmentLine.getAttribute(GCConstants.QUANTITY);
                String orderReleaseKeyParent = eleParentShipmentLine.getAttribute(GCConstants.ORDER_RELEASE_KEY);
                YFCElement eleParentOrderLine = eleParentShipmentLine.getChildElement(GCConstants.ORDER_LINE);
                String chainedFromOrderLineKeyParent =
                    eleParentOrderLine.getAttribute(GCConstants.CHAINED_FROM_ORDER_LINE_KEY);
                setPOSItemId(eleContainerDetailOut, itemIDParent, posItemId);
                eleContainerDetailOut.setAttribute(GCConstants.ITEM_DESCRIPTION, itemDescParent);
                eleContainerDetailOut.setAttribute(GCConstants.ORDER_RELEASE_KEY, orderReleaseKeyParent);
                eleContainerDetailOut.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKeyParent);
                eleContainerDetailOut.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
                eleContainerDetailOut.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
                eleContainerDetailOut.setAttribute(GCConstants.DOCUMENT_TYPE, docType);
                stampWarrantyComponent(env, dependentKeyMap, orderLineKeyParent, eleContainerDetailOut, eleOrderOp);
                setContainerDetails(eleOrderOp, eleContainerDetailOut, orderLineKeyParent,
                    chainedFromOrderLineKeyParent);
                if (YFCUtils.equals(productClass, GCConstants.SET)) {
                  Double temp = Double.parseDouble(sContainerDetailQty) * Double.parseDouble(parentQty);
                  Double actualQty = temp / lineQty;
                  eleContainerDetailOut.setAttribute(GCConstants.QUANTITY, actualQty.toString());
                  eleContainerDetailOut.setAttribute(GCConstants.SHIPMENT_CONTAINER_KEY, shipmentContainerKey);
                  eleContainerDetailOut.setAttribute(GCConstants.TRACKING_NO, sTrackingNo);
                  eleContainerDetailOut.setAttribute(GCConstants.PRODUCT_CLASS, productClass);
                  setMap.put(sParentShipmentLineKey, eleContainerDetailOut);
                } else {
                  eleContainerDetailOut.setAttribute(GCConstants.QUANTITY, parentQty);
                  eleContainerDetailOut.setAttribute(GCConstants.IS_BUNDLE_PARENT, GCConstants.YES);
                  bundleMap.put(sParentShipmentLineKey, eleContainerDetailOut);

                }
              }
              eleContainerDetailsOut.importNode(eleContainerDetailOut);
            }
            if (setMap.containsKey(sParentShipmentLineKey)) {
              YFCElement eleParent = setMap.get(sParentShipmentLineKey);
              YFCElement eleComponent = eleParent.createChild(GCXmlLiterals.COMPONENT);
              eleComponent.setAttribute(GCConstants.CONTAINER_DETAILS_KEY, containerDetailsKey);
              eleComponent.setAttribute(GCConstants.QUANTITY, sContainerDetailQty);
              eleComponent.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey);
              eleComponent.setAttribute(GCConstants.ORDER_RELEASE_KEY, orderReleaseKey);
              eleComponent.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
              eleComponent.setAttribute(GCConstants.DOCUMENT_TYPE, docType);
              eleParent.importNode(eleComponent);
            } else {
              YFCElement eleParent = bundleMap.get(sParentShipmentLineKey);
              YFCElement eleComponent = eleParent.createChild(GCXmlLiterals.COMPONENT);
              eleComponent.setAttribute(GCConstants.CONTAINER_DETAILS_KEY, containerDetailsKey);
              eleComponent.setAttribute(GCConstants.QUANTITY, sContainerDetailQty);
              eleComponent.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey);
              eleComponent.setAttribute(GCConstants.SHIPMENT_CONTAINER_KEY, shipmentContainerKey);
              eleComponent.setAttribute(GCConstants.ORDER_RELEASE_KEY, orderReleaseKey);
              eleComponent.setAttribute(GCConstants.TRACKING_NO, sTrackingNo);
              eleComponent.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
              eleComponent.setAttribute(GCConstants.DOCUMENT_TYPE, docType);
              eleParent.importNode(eleComponent);
            }
          } else if (!(YFCCommon.isVoid(sMultiBoxId))) {
            if (!multiBoxMap.containsKey(sMultiBoxId)) {
              YFCElement eleContainerDetailOut = eleContainerDetailsOut.createChild(GCConstants.CONTAINER_DETAIL);
              String posItemID = getPosItemID(env, sItemID, sUom);
              eleContainerDetailOut.setAttribute(GCConstants.ITEM_ID, posItemID);
              eleContainerDetailOut.setAttribute(GCConstants.ITEM_DESCRIPTION, sItemDesc);
              eleContainerDetailOut.setAttribute(GCConstants.QUANTITY, GCConstants.ONE);
              eleContainerDetailOut.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
              eleContainerDetailOut.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey);
              eleContainerDetailOut.setAttribute(GCConstants.TRACKING_NO, sTrackingNo);
              eleContainerDetailOut.setAttribute(GCConstants.ORDER_RELEASE_KEY, orderReleaseKey);
              eleContainerDetailOut.setAttribute(GCConstants.SHIPMENT_CONTAINER_KEY, shipmentContainerKey);
              eleContainerDetailOut.setAttribute(GCConstants.CONTAINER_DETAILS_KEY, containerDetailsKey);
              eleContainerDetailOut.setAttribute(GCConstants.IS_MULTIBOX, GCConstants.YES);
              eleContainerDetailOut.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
              eleContainerDetailOut.setAttribute(GCConstants.DOCUMENT_TYPE, docType);
              stampWarrantyComponent(env, dependentKeyMap, orderLineKey, eleContainerDetailOut, eleOrderOp);
              setContainerDetails(eleOrderOp, eleContainerDetailOut, orderLineKey, chainedFromOrderLineKey);
              eleContainerDetailsOut.importNode(eleContainerDetailOut);
              multiBoxMap.put(sMultiBoxId, eleContainerDetailOut);
            } else {
              YFCElement eleMultiBoxParent = multiBoxMap.get(sMultiBoxId);
              YFCElement eleComponent = eleMultiBoxParent.createChild(GCXmlLiterals.COMPONENT);
              eleComponent.setAttribute(GCConstants.CONTAINER_DETAILS_KEY, containerDetailsKey);
              eleComponent.setAttribute(GCConstants.TRACKING_NO, sTrackingNo);
              eleComponent.setAttribute(GCConstants.SHIPMENT_CONTAINER_KEY, shipmentContainerKey);
              eleComponent.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
              eleComponent.setAttribute(GCConstants.DOCUMENT_TYPE, docType);
              eleMultiBoxParent.importNode(eleComponent);
            }
          } else {
            YFCElement eleContainerDetailOut = eleContainerDetailsOut.createChild(GCConstants.CONTAINER_DETAIL);
            String posItemID = getPosItemID(env, sItemID, sUom);
            setPOSItemId(eleContainerDetailOut, sItemID, posItemID);
            eleContainerDetailOut.setAttribute(GCConstants.ITEM_DESCRIPTION, sItemDesc);
            eleContainerDetailOut.setAttribute(GCConstants.QUANTITY, sContainerDetailQty);
            eleContainerDetailOut.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
            eleContainerDetailOut.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey);
            eleContainerDetailOut.setAttribute(GCConstants.TRACKING_NO, sTrackingNo);
            eleContainerDetailOut.setAttribute(GCConstants.ORDER_RELEASE_KEY, orderReleaseKey);
            eleContainerDetailOut.setAttribute(GCConstants.SHIPMENT_CONTAINER_KEY, shipmentContainerKey);
            eleContainerDetailOut.setAttribute(GCConstants.CONTAINER_DETAILS_KEY, containerDetailsKey);
            eleContainerDetailOut.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
            eleContainerDetailOut.setAttribute(GCConstants.DOCUMENT_TYPE, docType);
            stampWarrantyComponent(env, dependentKeyMap, orderLineKey, eleContainerDetailOut, eleOrderOp);
            setContainerDetails(eleOrderOp, eleContainerDetailOut, orderLineKey, chainedFromOrderLineKey);
            eleContainerDetailsOut.importNode(eleContainerDetailOut);
          }
        }
      }
    }
  }

  private void setContainerDetails(YFCElement eleOrderOp, YFCElement eleContainerDetailOut, String orderLineKeyParent,
      String chainedFromOrderLineKeyParent) {
    if (!YFCCommon.isVoid(chainedFromOrderLineKeyParent)) {
      setOrderLineAttributes(eleOrderOp, chainedFromOrderLineKeyParent, eleContainerDetailOut);
    } else {
      setOrderLineAttributes(eleOrderOp, orderLineKeyParent, eleContainerDetailOut);
    }
  }

  private void setPOSItemId(YFCElement eleContainerDetailOut, String itemIDParent, String posItemId) {
    if (!YFCCommon.isVoid(posItemId)) {
      eleContainerDetailOut.setAttribute(GCConstants.ITEM_ID, posItemId);
    } else {
      eleContainerDetailOut.setAttribute(GCConstants.ITEM_ID, itemIDParent);
    }
  }


  public Map<String, YFCElement> getDependentKeyMapForWarrantyLines(YFCNodeList<YFCElement> nlContainerDetailList) {
    LOGGER.beginTimer("GCGetReadyForPickUpListAndDetails.getDependentKeyMapForWarrantyLines");
    GCLogUtils.verboseLog(LOGGER, "GCGetReadyForPickUpListAndDetails.getDependentKeyMapForWarrantyLines--Begin");
    Map<String, YFCElement> dependentKeyMap = new HashMap<String, YFCElement>();


    for (YFCElement eleContainerDetail : nlContainerDetailList) {

      YFCElement eleOrderLine = eleContainerDetail.getChildElement(GCConstants.ORDER_LINE);
      YFCElement eleOrderLineExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
      String sIsWarrantyItem = eleOrderLineExtn.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
      if (YFCCommon.equals(sIsWarrantyItem, GCConstants.YES)) {
        String sDependentOnLineKey = eleOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
        dependentKeyMap.put(sDependentOnLineKey, eleContainerDetail);
      }

    }

    GCLogUtils.verboseLog(LOGGER, "GCGetReadyForPickUpListAndDetails.getDependentKeyMapForWarrantyLines--End");
    LOGGER.endTimer("GCGetReadyForPickUpListAndDetails.getDependentKeyMapForWarrantyLines");
    return dependentKeyMap;
  }



  /**
   * This method is used for getting the POS Item ID
   *
   * @param env
   * @param sItemID
   * @param sUom
   * @return posItemId
   */
  private String getPosItemID(YFSEnvironment env, String sItemID, String sUom) {
    LOGGER.beginTimer("GCGetReadyForPickUpListAndDetails.getPosItemID");
    GCLogUtils.verboseLog(LOGGER, "GCGetReadyForPickUpListAndDetails.getPosItemID--Begin");
    YFCDocument getItemListTemp = YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LIST_PICKUP_TMP);
    YFCDocument getItemListOp = GCCommonUtil.getItemList(env, sItemID, sUom, GCConstants.GCI, true, getItemListTemp);
    GCLogUtils.verboseLog(LOGGER, "getItemList output for", getItemListOp.toString());
    YFCElement eleRoot = getItemListOp.getDocumentElement();
    YFCElement eleItem = eleRoot.getChildElement(GCConstants.ITEM);
    YFCElement eleExtnItem = eleItem.getChildElement(GCConstants.EXTN);
    String posItemId = eleExtnItem.getAttribute(GCConstants.EXTN_POSITEM_ID);
    GCLogUtils.verboseLog(LOGGER, "GCGetReadyForPickUpListAndDetails.getPosItemID--End");
    LOGGER.endTimer("GCGetReadyForPickUpListAndDetails.getPosItemID");
    return posItemId;

  }


  /**
   * This method is used for getting the shipmentline element for the bundle or set parent
   *
   * @param nlShipmentLine
   * @param sParentShipmentLineKey
   * @return
   */
  private YFCElement getParentShipmentLine(YFCNodeList<YFCElement> nlShipmentLine, String sParentShipmentLineKey) {
    LOGGER.beginTimer("GCGetReadyForPickUpListAndDetails.getParentShipmentLine");
    GCLogUtils.verboseLog(LOGGER, "GCGetReadyForPickUpListAndDetails.getParentShipmentLine--Begin");

    for (YFCElement eleShipmentLine : nlShipmentLine) {
      String shipmentLineKey = eleShipmentLine.getAttribute(GCConstants.SHIPMENT_LINE_KEY);
      if (YFCUtils.equals(shipmentLineKey, sParentShipmentLineKey)) {
        return eleShipmentLine;
      }
    }
    GCLogUtils.verboseLog(LOGGER, "GCGetReadyForPickUpListAndDetails.getParentShipmentLine--End");
    LOGGER.endTimer("GCGetReadyForPickUpListAndDetails.getParentShipmentLine");
    return null;
  }



  /**
   * This method is stamps the warranty component associated with the product line
   *
   * @param env
   * @param dependentKeyMap
   * @param sOrderLineKey
   * @param eleContainerDetailOut
   * @param eleOrder
   * @return
   */
  public void stampWarrantyComponent(YFSEnvironment env, Map<String, YFCElement> dependentKeyMap, String sOrderLineKey,
      YFCElement eleContainerDetailOut, YFCElement eleOrder) {
    LOGGER.beginTimer("GCGetReadyForPickUpListAndDetails.stampWarrantyComponent");
    GCLogUtils.verboseLog(LOGGER, "GCGetReadyForPickUpListAndDetails.stampWarrantyComponent--Begin");
    if (dependentKeyMap.containsKey(sOrderLineKey)) {
      YFCElement eleWarrantyComp = dependentKeyMap.get(sOrderLineKey);
      String detailsKey = eleWarrantyComp.getAttribute(GCConstants.CONTAINER_DETAILS_KEY);
      String qty = eleWarrantyComp.getAttribute(GCConstants.QUANTITY);
      String lineKey = eleWarrantyComp.getAttribute(GCConstants.ORDER_LINE_KEY);
      String containerKey = eleWarrantyComp.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
      String releaseKey = eleWarrantyComp.getAttribute(GCConstants.ORDER_RELEASE_KEY);
      YFCElement eleWarrShipmentLine = eleWarrantyComp.getChildElement(GCConstants.SHIPMENT_LINE);
      String sItemID = eleWarrShipmentLine.getAttribute(GCConstants.ITEM_ID);
      String sItemDesc = eleWarrShipmentLine.getAttribute(GCConstants.ITEM_DESCRIPTION);
      String sUom = eleWarrShipmentLine.getAttribute(GCConstants.UNIT_OF_MEASURE);
      String posItemID = getPosItemID(env, sItemID, sUom);
      YFCElement eleWarranty = eleContainerDetailOut.createChild(GCXmlLiterals.WARRANTY_COMPONENT);
      eleWarranty.setAttribute(GCConstants.CONTAINER_DETAILS_KEY, detailsKey);
      eleWarranty.setAttribute(GCConstants.ORDER_LINE_KEY, lineKey);
      eleWarranty.setAttribute(GCConstants.QUANTITY, qty);
      eleWarranty.setAttribute(GCConstants.SHIPMENT_CONTAINER_KEY, containerKey);
      eleWarranty.setAttribute(GCConstants.ORDER_RELEASE_KEY, releaseKey);
      eleWarranty.setAttribute(GCConstants.ITEM_ID, posItemID);
      eleWarranty.setAttribute(GCConstants.ITEM_DESCRIPTION, sItemDesc);
      setOrderLineAttributes(eleOrder, lineKey, eleWarranty);
      eleContainerDetailOut.importNode(eleWarranty);
    }

    GCLogUtils.verboseLog(LOGGER, "GCGetReadyForPickUpListAndDetails.stampWarrantyComponent--End");
    LOGGER.endTimer("GCGetReadyForPickUpListAndDetails.stampWarrantyComponent");
  }



  /**
   * This method invokes getOrderCommissionList API and fetches SPOAssociate
   *
   * @param env
   * @param dependentKeyMap
   * @param sOrderLineKey
   * @param eleContainerDetailOut
   * @param eleOrder
   * @return
   */
  private String getSPOAssociate(String orderHeaderKey, YFSEnvironment env) {
    LOGGER.beginTimer("GCGetReadyForPickUpListAndDetails.getSPOAssociate");
    GCLogUtils.verboseLog(LOGGER, "GCGetReadyForPickUpListAndDetails.getSPOAssociate--Begin");
    String spoAssociate = null;
    YFCDocument commisionListIp = YFCDocument.createDocument(GCConstants.GC_ORDER_COMMISSION);
    YFCElement rootEle = commisionListIp.getDocumentElement();
    rootEle.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
    YFCDocument commListOpDoc =
        GCCommonUtil.invokeService(env, GCConstants.GET_ORDER_COMMISSION_LIST_SERVICE, commisionListIp);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getCommissionListOpDoc : " + commListOpDoc.toString());
    }
    YFCElement commListEle = commListOpDoc.getDocumentElement();
    YFCElement eleCommission = commListEle.getElementsByTagName(GCConstants.GC_ORDER_COMMISSION).item(0);
    if (!YFCCommon.isVoid(eleCommission)) {
      spoAssociate = eleCommission.getAttribute(GCConstants.ORDER_USER_NAME);
    }
    GCLogUtils.verboseLog(LOGGER, "GCGetReadyForPickUpListAndDetails.getSPOAssociate--End");
    LOGGER.endTimer("GCGetReadyForPickUpListAndDetails.getSPOAssociate");
    return spoAssociate;
  }


  /**
   * This method fetches header level attributes and stamps it to order level
   *
   * @param eleShipment
   * @param eleOrder
   * @param eleOrderOp
   * @return
   */
  private void stampHeaderAttributes(YFCElement eleShipment, YFCElement eleOrder, YFCElement eleOrderOp) {
    LOGGER.beginTimer("GCGetReadyForPickUpListAndDetails.stampHeaderAttributes");
    GCLogUtils.verboseLog(LOGGER, "GCGetReadyForPickUpListAndDetails.stampHeaderAttributes--Begin");
    YFCElement eleBillToAddress = eleShipment.getChildElement(GCXmlLiterals.BILL_TO_ADDRESS);
    YFCElement eleExtn = eleShipment.getChildElement(GCXmlLiterals.EXTN);
    String sFirstName = eleBillToAddress.getAttribute(GCConstants.FIRST_NAME);
    String sLastName = eleBillToAddress.getAttribute(GCConstants.LAST_NAME);
    String billingCustomerName = sFirstName + " " + sLastName;
    String sEMailID = eleBillToAddress.getAttribute(GCConstants.EMAIL_ID);
    String sPhone = eleBillToAddress.getAttribute(GCConstants.DAY_PHONE);
    YFCElement eleToAddress = eleShipment.getChildElement(GCXmlLiterals.TO_ADDRESS);
    String sFirstNameShipTo = eleToAddress.getAttribute(GCConstants.FIRST_NAME);
    String sLastNameShipTo = eleToAddress.getAttribute(GCConstants.LAST_NAME);
    String sPickUpPersonName = sFirstNameShipTo + " " + sLastNameShipTo;
    String sOrderDate = eleOrderOp.getAttribute(GCConstants.ORDER_DATE);
    String sFulfillmentType = eleExtn.getAttribute(GCConstants.EXTN_FULFILLMENT_TYPE);
    String orderNo = eleOrderOp.getAttribute(GCConstants.ORDER_NO);
    String orderHeaderKey = eleOrderOp.getAttribute(GCConstants.ORDER_HEADER_KEY);
    YFCElement eleOrderExtn = eleOrderOp.getChildElement(GCConstants.EXTN);
    String orderingStore = eleOrderExtn.getAttribute(GCConstants.EXTN_ORDERING_STORE);
    YFCElement elePaymentMethods = eleOrderOp.getChildElement(GCConstants.PAYMENT_METHODS);
    String billToId = eleOrderOp.getAttribute(GCConstants.BILL_TO_ID);
    eleOrder.setAttribute(GCConstants.BILL_TO_ID, billToId);
    eleOrder.setAttribute(GCConstants.BILLING_CUSTOMER_NAME, billingCustomerName);
    eleOrder.setAttribute(GCConstants.EMAIL_ID, sEMailID);
    eleOrder.setAttribute(GCConstants.DAY_PHONE, sPhone);
    eleOrder.setAttribute(GCConstants.FULFILLMENT_TYPE, sFulfillmentType);
    eleOrder.setAttribute(GCConstants.STATUS, GCConstants.READY_FOR_CUSTOMER_PICK_UP);
    eleOrder.setAttribute(GCConstants.ORDERED_ON, sOrderDate);
    eleOrder.setAttribute(GCConstants.PICK_UP_PERSON_NAME, sPickUpPersonName);
    eleOrder.setAttribute(GCConstants.ORDER_NO, orderNo);
    eleOrder.setAttribute(GCConstants.ORDERING_STORE, orderingStore);
    eleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
    eleOrder.importNode(elePaymentMethods);
    eleOrder.importNode(eleToAddress);
    eleOrder.importNode(eleBillToAddress);
    GCLogUtils.verboseLog(LOGGER, "GCGetReadyForPickUpListAndDetails.stampHeaderAttributes--End");
    LOGGER.endTimer("GCGetReadyForPickUpListAndDetails.stampHeaderAttributes");
  }

  /**
   *
   * This method takes orderlinekey and retrieves price and serial no
   *
   * @param eleOrder
   * @param orderLineKey
   * @param eleContainerElement
   * @return
   */
  private void setOrderLineAttributes(YFCElement eleOrder, String orderLineKey, YFCElement eleContainerElement) {
    LOGGER.beginTimer("GCGetReadyForPickUpListAndDetails.setOrderLineAttributes");
    GCLogUtils.verboseLog(LOGGER, "GCGetReadyForPickUpListAndDetails.setOrderLineAttributes--Begin");
    YFCElement eleOrderLines = eleOrder.getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> nlOrderLine = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
    for (YFCElement eleOrderLine : nlOrderLine) {
      if (YFCUtils.equals(eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY), orderLineKey)) {
        String unitPrice = calculateUnitPrice(eleOrderLine).toString();
        eleContainerElement.setAttribute(GCConstants.UNIT_PRICE, unitPrice);
        YFCElement eleExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
        String actualSerialNo = eleExtn.getAttribute(GCConstants.EXTN_ACTUAL_SERIAL_NO);
        String salesTicketNo = eleExtn.getAttribute(GCConstants.EXTN_POS_SALES_TICKET_NO);
        eleContainerElement.setAttribute(GCConstants.SERIAL_NUMBER, actualSerialNo);
        eleContainerElement.setAttribute(GCConstants.EXTN_POS_SALES_TICKET_NO, salesTicketNo);
        break;
      }
    }
    GCLogUtils.verboseLog(LOGGER, "GCGetReadyForPickUpListAndDetails.setOrderLineAttributes--End");
    LOGGER.endTimer("GCGetReadyForPickUpListAndDetails.setOrderLineAttributes");
  }

  // This method calculates the unit price at order line to be displayed in customer pick up slip
  private BigDecimal calculateUnitPrice(YFCElement eleOrderLine) {
    LOGGER.beginTimer("GCGetReadyForPickUpListAndDetails.calculateUnitPrice");
    GCLogUtils.verboseLog(LOGGER, "GCGetReadyForPickUpListAndDetails.calculateUnitPrice--Begin");

    final YFCElement eleLinePriceInfo = eleOrderLine.getChildElement(GCConstants.LINE_PRICE_INFO);
    BigDecimal totalPromotion = new BigDecimal(GCConstants.ZERO_AMOUNT);
    BigDecimal unitPromotion = new BigDecimal(GCConstants.ZERO_AMOUNT);
    final YFCElement eleLineCharges = eleOrderLine.getChildElement(GCConstants.LINE_CHARGES);
    if (!YFCCommon.isVoid(eleLineCharges)) {
      final YFCNodeList<YFCElement> nlLineCharge = eleLineCharges.getElementsByTagName(GCConstants.LINE_CHARGE);
      final int lengthLineChg = nlLineCharge.getLength();
      for (int i = 0; i < lengthLineChg; i++) {
        final YFCElement eleLineChg = nlLineCharge.item(i);
        if (YFCUtils.equals(GCConstants.PROMOTION, eleLineChg.getAttribute(GCConstants.CHARGE_CATEGORY))) {
          String chargePerLine = eleLineChg.getAttribute(GCConstants.CHARGE_PER_LINE);
          BigDecimal chgAmt = new BigDecimal(chargePerLine);
          totalPromotion = totalPromotion.add(chgAmt);
        }
      }
    }
    BigDecimal quantity = new BigDecimal(eleOrderLine.getAttribute(GCConstants.ORDERED_QTY));
    if (quantity.intValue() != 0) {
      unitPromotion = totalPromotion.divide(quantity, 1);
    }
    BigDecimal unitPrice = new BigDecimal(eleLinePriceInfo.getAttribute(GCConstants.UNIT_PRICE));
    GCLogUtils.verboseLog(LOGGER, "GCGetReadyForPickUpListAndDetails.calculateUnitPrice--End");
    LOGGER.endTimer("GCGetReadyForPickUpListAndDetails.calculateUnitPrice");
    return unitPrice.subtract(unitPromotion);
  }
}

