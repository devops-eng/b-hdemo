package com.gc.exstore.api.customerpick;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.exstore.utils.GCShipmentUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCManageCustomerPickup {
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCManageCustomerPickup.class.getName());

  /**
   * This method is used for changing the shipment and line status on Pick up.
   *
   * @param env
   * @param inpDoc
   * @return
   */
  public Document saveChangesOnPickUp(YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("GCManageCustomerPickup.saveChangesOnPickUp");
    LOGGER.verbose("GCManageCustomerPickup.saveChangesOnPickUp--Begin");
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("inputDoc : " + inputDoc.toString());
    }

    YFCElement eleInput = inputDoc.getDocumentElement();
    String orderHeaderKey = eleInput.getAttribute(GCConstants.ORDER_HEADER_KEY);
    String loginId = eleInput.getAttribute(GCXmlLiterals.USER_AUTHENTICATED);
    Map<String, Double> orderLineKeyMap = new HashMap<String, Double>();
    Map<String, HashMap<String, Double>> map1 = new HashMap<String, HashMap<String, Double>>();
    Map<String, HashSet<String>> map2 = new HashMap<String, HashSet<String>>();
    List<String> orderLineList = new ArrayList<String>();
    setMaps(map1, map2, eleInput);
    for (Map.Entry<String, HashSet<String>> entry : map2.entrySet()) {
      String shipmentKey = entry.getKey();
      Set<String> shipmentContainerList = entry.getValue();
      // Prepare input for changeShipment API to update picked up quantity at container detail
      YFCDocument changeShipmentIp =
          getInputForChangeShipmentForPickedUpQty(shipmentKey, shipmentContainerList, map1, eleInput);
      YFCDocument changeShipmentOpTemp = YFCDocument.getDocumentFor(GCConstants.CHANGE_SHIPMENT_OUT_TEMP);

      // Invoke changeShipment API to update picked up quantity at container detail
      YFCDocument changeShipmentOpDoc =
          GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_SHIPMENT, changeShipmentIp, changeShipmentOpTemp);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("Change Shipment Output Document after updating Picked Up quantity : "
            + changeShipmentOpDoc.toString());
      }
      YFCElement eleChangeShipmentOutDoc = changeShipmentOpDoc.getDocumentElement();


      // Update User Activity Table
      String employeeId = GCShipmentUtil.getEmployeeID(env, loginId);

      // Prepare input for changeShipment API to update carton status
      YFCDocument chngShpmntForCartonStatusIp =
          getInputForChngeShpmntForCartonStatus(eleChangeShipmentOutDoc, shipmentContainerList, orderHeaderKey, env,
              employeeId);
      YFCDocument chngShpmntForCartonStatusOpTemp = YFCDocument.getDocumentFor(GCConstants.CHANGE_SHIPMENT_OUT_TEMP);

      // Invoke changeShipment API to update carton status
      YFCDocument chngShpmntForCartonStatusOpDoc =
          GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_SHIPMENT, chngShpmntForCartonStatusIp,
              chngShpmntForCartonStatusOpTemp);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("Change Shipment Output Document after updating Carton Status : "
            + chngShpmntForCartonStatusOpDoc.toString());
      }
      YFCElement eleChangeShipmentForCartonStatusOutDoc = chngShpmntForCartonStatusOpDoc.getDocumentElement();
      changeShipmentStatus(shipmentKey, eleChangeShipmentForCartonStatusOutDoc, env);
    }

    // Invoke changeOrderStatus to update order lines with picked up quantity

    updateOrderLinesWithPickedUpQty(eleInput, env, orderLineKeyMap, orderLineList);



    // Prepare input for pick up slip
    YFCDocument pickUpSlipDoc = printPickSlip(eleInput, env);

    LOGGER.verbose("GCManageCustomerPickup.saveChangesOnPickUp--End");
    LOGGER.endTimer("GCManageCustomerPickup.saveChangesOnPickUp");
    return pickUpSlipDoc.getDocument();
  }

  /**
   * This method is used for preparing input for customer pick up slip and invoke service to
   * generate pdf input
   *
   * @param eleInput
   * @param env
   * @return
   */

  private YFCDocument printPickSlip(YFCElement eleInput, YFSEnvironment env) {
    LOGGER.beginTimer("GCManageCustomerPickup.printPickSlip");
    LOGGER.verbose("GCManageCustomerPickup.printPickSlip--Begin");

    String spoAssociate = eleInput.getAttribute(GCConstants.ORDER_USER_NAME);
    String shipNode = eleInput.getAttribute(GCConstants.STORE_ID);
    String orderNo = eleInput.getAttribute(GCConstants.ORDER_NO);
    String orderDate = eleInput.getAttribute(GCConstants.ORDER_DATE);
    String orderDate1 = orderDate.substring(0, orderDate.lastIndexOf('T'));
    String orderDate2 = orderDate.substring(orderDate.lastIndexOf('T') + 1);
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    Date sysDate = new Date();
    String strDate = sdf.format(sysDate);
    String sysDate1 = strDate.substring(0, strDate.lastIndexOf('T'));
    String sysDate2 = strDate.substring(strDate.lastIndexOf('T') + 1);
    String billToID = eleInput.getAttribute(GCConstants.BILL_TO_ID);
    YFCElement eleShipTo = eleInput.getChildElement(GCXmlLiterals.TO_ADDRESS);
    YFCElement eleBillTo = eleInput.getChildElement(GCXmlLiterals.BILL_TO_ADDRESS);
    YFCDocument opDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement opEle = opDoc.getDocumentElement();
    opEle.setAttribute(GCConstants.ORDER_NO, orderNo);
    opEle.setAttribute(GCConstants.ORDER_DATE, orderDate1);
    opEle.setAttribute("OrderTime", orderDate2);
    opEle.setAttribute("SystemDate", sysDate1);
    opEle.setAttribute("SystemTime", sysDate2);
    opEle.setAttribute(GCConstants.BILL_TO_CUSTOMER_ID, billToID);
    opEle.setAttribute(GCConstants.ORDER_USER_NAME, spoAssociate);
    YFCDocument orgListIp = YFCDocument.createDocument(GCConstants.ORGANIZATION);
    YFCElement orgEle = orgListIp.getDocumentElement();
    orgEle.setAttribute(GCConstants.ORGANIZATION_CODE, shipNode);
    YFCDocument orgListOpTemp = YFCDocument.getDocumentFor(GCConstants.GET_ORGANIZATION_LIST_TEMP);
    YFCDocument orgListOpDoc = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORGANIZATION_LIST, orgListIp, orgListOpTemp);
    YFCElement orgListEle = orgListOpDoc.getDocumentElement();
    YFCElement eleOrganization = orgListEle.getChildElement(GCConstants.ORGANIZATION);
    YFCElement eleNode = eleOrganization.getChildElement(GCConstants.NODE);
    String description = eleNode.getAttribute(GCConstants.DESCRIPTION);
    YFCElement eleShipNode = opEle.createChild(GCConstants.SHIP_NODE);
    eleShipNode.setAttribute(GCConstants.SHIP_NODE, shipNode);
    eleShipNode.setAttribute(GCConstants.DESCRIPTION, description);
    YFCElement elePaymentMethods = eleInput.getChildElement(GCConstants.PAYMENT_METHODS);
    if (!YFCCommon.isVoid(elePaymentMethods)) {
      YFCElement elePaymentMethod = elePaymentMethods.getElementsByTagName(GCConstants.PAYMENT_METHOD).item(0);
      if (!YFCCommon.isVoid(elePaymentMethod)) {
        String sPaymentType = elePaymentMethod.getAttribute(GCConstants.CREDIT_CARD_TYPE);
        if (!YFCCommon.isVoid(sPaymentType)) {
          if (sPaymentType.contains("_")) {
            sPaymentType = sPaymentType.replace("_", " ");
          }
          String sCreditCardNo = elePaymentMethod.getAttribute(GCConstants.DISPLAY_CREDIT_CARD_NUM);
          opEle.setAttribute(GCConstants.PAYMENT_TYPE, sPaymentType + "  " + sCreditCardNo);
        } else {
          String paymentType = elePaymentMethod.getAttribute(GCConstants.PAYMENT_TYPE);
          if (paymentType.contains("_")) {
            paymentType = paymentType.replace("_", " ");
          }
          opEle.setAttribute(GCConstants.PAYMENT_TYPE, paymentType);
        }
      }
      YFCNodeList<YFCElement> nlPaymentMethod = elePaymentMethods.getElementsByTagName(GCConstants.PAYMENT_METHOD);
      stampMultiplePaymentMethods(opEle, nlPaymentMethod);
    }
    YFCElement eleShipmentLines = opEle.createChild(GCConstants.SHIPMENT_LINES);
    YFCNodeList<YFCElement> nlContainerDetail = eleInput.getElementsByTagName(GCConstants.CONTAINER_DETAIL);
    for (YFCElement eleContainerDetail : nlContainerDetail) {
      addLineDetailForReceipt(env, eleContainerDetail, eleShipmentLines);
      YFCElement eleWarrantyComponent = eleContainerDetail.getChildElement(GCXmlLiterals.WARRANTY_COMPONENT);
      if (!YFCCommon.isVoid(eleWarrantyComponent)) {
        addLineDetailForReceipt(env, eleWarrantyComponent, eleShipmentLines);
      }
    }

    opEle.importNode(eleShipmentLines);
    opEle.importNode(eleBillTo);
    opEle.importNode(eleShipTo);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("input to print pick up slip service : " + opDoc.toString());
    }
    
    //GCSTORE-4769 - Start
    //YFCElement eleContainerDetail = eleInput.getChildElement(GCConstants.CONTAINER_DETAIL);
  //GCSTORE-6955--Start
    ArrayList<String> arrList = new ArrayList<String>();
  //GCSTORE-6955--End
    double subTotal = 0.00;
    double adjustments = 0.00;
    double shipping = 0.00;
    double taxes = 0.00;
    double total = 0.00;
    //GCSTORE-6955--Start
    for (YFCElement eleContainerDetail : nlContainerDetail) {
    //if (!YFCCommon.isVoid(eleContainerDetail)) {
    //GCSTORE-6955--End
      String sShipmentKey = eleContainerDetail.getAttribute(GCConstants.SHIPMENT_KEY);
    //GCSTORE-6955--Start
      if (!YFCCommon.isStringVoid(sShipmentKey) && !arrList.contains(sShipmentKey)) {
    	arrList.add(sShipmentKey);
    	//GCSTORE-6955--End
        YFCDocument getSortedShipmentDoc = YFCDocument.getDocumentFor("<Shipment ShipmentKey='" + sShipmentKey + "'/>");
        YFCDocument getSortedShipmentTemp =
            YFCDocument
                .getDocumentFor("<Shipment ShipmentKey='' ShipmentNo=''><ShipmentLines>"
                    + "<ShipmentLine OrderHeaderKey='' OrderLineKey='' OrderNo=''><OrderLine OrderedQty=''>"
                    + "<BundleParentLine/>"
                    + "<ComputedPrice Charges='' Discount='' LineTotal='' Tax='' UnitPrice=''/></OrderLine></ShipmentLine></ShipmentLines></Shipment>");
        YFCDocument getSortedShipmentOutDoc =
            GCCommonUtil.invokeAPI(env, GCConstants.SORTED_SHIPMENT_API, getSortedShipmentDoc, getSortedShipmentTemp);
        YFCElement eleSortedShipment = getSortedShipmentOutDoc.getDocumentElement();
        YFCElement eleSortedShipmentLines = eleSortedShipment.getChildElement(GCConstants.SHIPMENT_LINES);
        YFCNodeList<YFCElement> nlSortedShipmentLine =
            eleSortedShipmentLines.getElementsByTagName(GCConstants.SHIPMENT_LINE);
        for(YFCElement eleSortedShipmentLine : nlSortedShipmentLine){
          YFCNodeList<YFCElement> nlOrderLine = eleSortedShipmentLine.getElementsByTagName(GCConstants.ORDER_LINE);
          for(YFCElement eleOrderLine : nlOrderLine){
        	//GCSTORE-7092/6955--Start
        	YFCElement eleBundleParentLine = eleOrderLine.getElementsByTagName(GCConstants.BUNDLE_PARENT_LINE).item(0);
            if(YFCCommon.isVoid(eleBundleParentLine)){
            //GCSTORE-7092/6955--End
            YFCElement eleComputedPrice = eleOrderLine.getChildElement(GCConstants.COMPUTED_PRICE);
            Double unitPrice = Double.parseDouble(eleComputedPrice.getAttribute(GCConstants.UNIT_PRICE));
            Double qty = Double.parseDouble(eleOrderLine.getAttribute(GCConstants.ORDERED_QTY));
            subTotal = subTotal + (unitPrice*qty);
            adjustments = adjustments + Double.parseDouble(eleComputedPrice.getAttribute("Discount"));
            shipping = shipping + Double.parseDouble(eleComputedPrice.getAttribute("Charges"));
            taxes = taxes + Double.parseDouble(eleComputedPrice.getAttribute(GCConstants.TAX));
            total = total + Double.parseDouble(eleComputedPrice.getAttribute(GCConstants.LINE_TOTAL));
            }
          }
        }
      }
    }    
    YFCElement opPriceInfo = opEle.getChildElement(GCConstants.SHIPMENT_LINES).createChild(GCConstants.PRICE_INFO);
    opPriceInfo.setAttribute(GCConstants.SUB_TOTAL, Double.toString(subTotal));
    opPriceInfo.setAttribute("Adjustments", Double.toString(adjustments));
    opPriceInfo.setAttribute(GCConstants.SHIPPING, Double.toString(shipping));
    opPriceInfo.setAttribute("Taxes", Double.toString(taxes));
    opPriceInfo.setAttribute("Total", Double.toString(total));
    //GCSTORE-4769 - End    
    
    // Invoke service to print pick slip
    YFCDocument pickUpSlipDoc = GCCommonUtil.invokeService(env, GCConstants.GC_PRINT_PICK_SLIP, opDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("output of  print pick up slip service : " + pickUpSlipDoc.toString());
    }


    LOGGER.verbose("GCManageCustomerPickup.printPickSlip--End");
    LOGGER.endTimer("GCManageCustomerPickup.printPickSlip");
    return pickUpSlipDoc;
  }

  /**
   * This method is used to create multiplePaymentMethods element for PDF.
   * 
   * @param opEle
   * @param nlPaymentMethod
   */
  private void stampMultiplePaymentMethods(YFCElement opEle, YFCNodeList<YFCElement> nlPaymentMethod) {

    final int lengthPymntMthd = nlPaymentMethod.getLength();
    YFCElement elePaymentMethodList = opEle.createChild("PaymentMethodList");
    for (int i = 1; i < lengthPymntMthd; i++) {
      final YFCElement elePaymentMethod = nlPaymentMethod.item(i);
      if (!YFCCommon.isVoid(elePaymentMethod)) {
        YFCElement eleCustomPaymentMethod = elePaymentMethodList.createChild("CustomPaymentMethod");
        String sPaymentType = elePaymentMethod.getAttribute(GCConstants.CREDIT_CARD_TYPE);
        if (!YFCCommon.isVoid(sPaymentType)) {
          if (sPaymentType.contains("_")) {
            sPaymentType = sPaymentType.replace("_", " ");
          }
          String sCreditCardNo = elePaymentMethod.getAttribute(GCConstants.DISPLAY_CREDIT_CARD_NUM);
          eleCustomPaymentMethod.setAttribute(GCConstants.PAYMENT_TYPE, sPaymentType + "  " + sCreditCardNo);
        } else {
          String paymentType = elePaymentMethod.getAttribute(GCConstants.PAYMENT_TYPE);
          if (paymentType.contains("_")) {
            paymentType = paymentType.replace("_", " ");
          }
          eleCustomPaymentMethod.setAttribute(GCConstants.PAYMENT_TYPE, paymentType);
        }
      }
    }
  }

  /**
   * This method is used for fetching the Shipment & container details for changeShipment API input.
   *
   * @param map1
   * @param map2
   * @param eleInput
   * @return
   */

  private void setMaps(Map<String, HashMap<String, Double>> map1, Map<String, HashSet<String>> map2,
      YFCElement eleInput) {
    LOGGER.beginTimer("GCManageCustomerPickup.setMaps");
    LOGGER.verbose("GCManageCustomerPickup.setMaps--Begin");

    String shipmentContainerKey = null;
    String shipmentKey = null;
    String containerDetailKey = null;
    Double quantity = 0.00;

    YFCNodeList<YFCElement> nlContainerDetailList = eleInput.getElementsByTagName(GCConstants.CONTAINER_DETAIL);
    for (YFCElement eleContainerDetail : nlContainerDetailList) {
      shipmentKey = eleContainerDetail.getAttribute(GCConstants.SHIPMENT_KEY);
      String isMultiBox = eleContainerDetail.getAttribute(GCConstants.IS_MULTIBOX);
      String isBundle = eleContainerDetail.getAttribute(GCConstants.IS_BUNDLE_PARENT);
      YFCElement eleWarrantyComp = eleContainerDetail.getChildElement(GCXmlLiterals.WARRANTY_COMPONENT);
      YFCNodeList<YFCElement> nlComponents = eleContainerDetail.getElementsByTagName(GCXmlLiterals.COMPONENT);
      int lengthComponent = nlComponents.getLength();
      if (lengthComponent == 0) {
        shipmentContainerKey = eleContainerDetail.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
        quantity = eleContainerDetail.getDoubleAttribute(GCConstants.QUANTITY);
        containerDetailKey = eleContainerDetail.getAttribute(GCConstants.CONTAINER_DETAILS_KEY);
        addContainerKeyToMap(map1, shipmentContainerKey);
        map1.get(shipmentContainerKey).put(containerDetailKey, quantity);
        if (!YFCCommon.isVoid(eleWarrantyComp)) {
          Double quantityW = eleWarrantyComp.getDoubleAttribute(GCConstants.QUANTITY);
          String containerDetailKeyW = eleWarrantyComp.getAttribute(GCConstants.CONTAINER_DETAILS_KEY);
          map1.get(shipmentContainerKey).put(containerDetailKeyW, quantityW);
        }
        addShipmentKeyToMap(map2, shipmentKey);
        map2.get(shipmentKey).add(shipmentContainerKey);
      } else if (YFCUtils.equals(isMultiBox, GCConstants.YES)) {
        Double multiboxQty = 1.00;
        addShipmentKeyToMap(map2, shipmentKey);
        shipmentContainerKey = eleContainerDetail.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
        containerDetailKey = eleContainerDetail.getAttribute(GCConstants.CONTAINER_DETAILS_KEY);
        addContainerKeyToMap(map1, shipmentContainerKey);
        map1.get(shipmentContainerKey).put(containerDetailKey, multiboxQty);
        map2.get(shipmentKey).add(shipmentContainerKey);
        for (YFCElement eleComponent : nlComponents) {
          String shipmentContainerKeyComp = eleComponent.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
          map2.get(shipmentKey).add(shipmentContainerKeyComp);
          String containerDetailKeyComp = eleComponent.getAttribute(GCConstants.CONTAINER_DETAILS_KEY);
          addContainerKeyToMap(map1, shipmentContainerKeyComp);
          map1.get(shipmentContainerKeyComp).put(containerDetailKeyComp, multiboxQty);
        }
        if (!YFCCommon.isVoid(eleWarrantyComp)) {
          String shipmentContainerKeyW = eleWarrantyComp.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
          map2.get(shipmentKey).add(shipmentContainerKeyW);
          Double quantityW = eleWarrantyComp.getDoubleAttribute(GCConstants.QUANTITY);
          String containerDetailKeyW = eleWarrantyComp.getAttribute(GCConstants.CONTAINER_DETAILS_KEY);
          addContainerKeyToMap(map1, shipmentContainerKeyW);
          map1.get(shipmentContainerKeyW).put(containerDetailKeyW, quantityW);
        }
      } else if (YFCUtils.equals(isBundle, GCConstants.YES)) {
        addShipmentKeyToMap(map2, shipmentKey);
        for (YFCElement eleComponent : nlComponents) {
          String shipmentContainerKeyComp = eleComponent.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
          map2.get(shipmentKey).add(shipmentContainerKeyComp);
          String containerDetailKeyComp = eleComponent.getAttribute(GCConstants.CONTAINER_DETAILS_KEY);
          Double quantityComp = eleComponent.getDoubleAttribute(GCConstants.QUANTITY);
          addContainerKeyToMap(map1, shipmentContainerKeyComp);
          map1.get(shipmentContainerKeyComp).put(containerDetailKeyComp, quantityComp);
        }
        if (!YFCCommon.isVoid(eleWarrantyComp)) {
          String shipmentContainerKeyW = eleWarrantyComp.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
          map2.get(shipmentKey).add(shipmentContainerKeyW);
          Double quantityW = eleWarrantyComp.getDoubleAttribute(GCConstants.QUANTITY);
          String containerDetailKeyW = eleWarrantyComp.getAttribute(GCConstants.CONTAINER_DETAILS_KEY);
          addContainerKeyToMap(map1, shipmentContainerKeyW);
          map1.get(shipmentContainerKeyW).put(containerDetailKeyW, quantityW);
        }
      } else {
        addShipmentKeyToMap(map2, shipmentKey);
        shipmentContainerKey = eleContainerDetail.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
        addContainerKeyToMap(map1, shipmentContainerKey);
        map2.get(shipmentKey).add(shipmentContainerKey);
        for (YFCElement eleComponent : nlComponents) {
          quantity = eleComponent.getDoubleAttribute(GCConstants.QUANTITY);
          containerDetailKey = eleComponent.getAttribute(GCConstants.CONTAINER_DETAILS_KEY);
          map1.get(shipmentContainerKey).put(containerDetailKey, quantity);
        }


        if (!YFCCommon.isVoid(eleWarrantyComp)) {
          Double quantityW = eleWarrantyComp.getDoubleAttribute(GCConstants.QUANTITY);
          String containerDetailKeyW = eleWarrantyComp.getAttribute(GCConstants.CONTAINER_DETAILS_KEY);
          map1.get(shipmentContainerKey).put(containerDetailKeyW, quantityW);
        }
      }

    }


    LOGGER.verbose("GCManageCustomerPickup.setMaps--End");
    LOGGER.endTimer("GCManageCustomerPickup.setMaps");
  }



  /**
   *
   * This method is used to add ShipmentConatinerkey to map
   *
   * @param map1
   * @param shipmentContainerKey
   * @return
   *
   */
  private void addContainerKeyToMap(Map<String, HashMap<String, Double>> map1, String shipmentContainerKey) {
    LOGGER.beginTimer("GCManageCustomerPickup.addContainerKeyToMap");
    LOGGER.verbose("GCManageCustomerPickup.addContainerKeyToMap--Begin");

    if (!map1.containsKey(shipmentContainerKey)) {
      map1.put(shipmentContainerKey, new HashMap<String, Double>());
    }

    LOGGER.verbose("GCManageCustomerPickup.addContainerKeyToMap--End");
    LOGGER.endTimer("GCManageCustomerPickup.addContainerKeyToMap");
  }


  /**
   *
   * This method is used to add Shipmentkey to map
   *
   * @param map2
   * @param shipmentKey
   * @return
   *
   */
  private void addShipmentKeyToMap(Map<String, HashSet<String>> map2, String shipmentKey) {
    LOGGER.beginTimer("GCManageCustomerPickup.addShipmentKeyToMap");
    LOGGER.verbose("GCManageCustomerPickup.addShipmentKeyToMap--Begin");
    if (!map2.containsKey(shipmentKey)) {
      map2.put(shipmentKey, new HashSet<String>());
    }

    LOGGER.verbose("GCManageCustomerPickup.addShipmentKeyToMap--End");
    LOGGER.endTimer("GCManageCustomerPickup.addShipmentKeyToMap");
  }


  /**
   *
   * This method is used to add OrderLinekey to map
   *
   * @param orderLineKeyMap
   * @param orderLineKey
   * @param quantity
   * @return
   *
   */
  private void addOrderLineKeyToMap(Map<String, Double> orderLineKeyMap, String orderLineKey, Double quantity) {
    LOGGER.beginTimer("GCManageCustomerPickup.addOrderLineKeyToMap");
    LOGGER.verbose("GCManageCustomerPickup.addOrderLineKeyToMap--Begin");
    if (!orderLineKeyMap.containsKey(orderLineKey)) {
      orderLineKeyMap.put(orderLineKey, quantity);
    } else {
      orderLineKeyMap.put(orderLineKey, orderLineKeyMap.get(orderLineKey) + quantity);
    }

    LOGGER.verbose("GCManageCustomerPickup.addOrderLineKeyToMap--End");
    LOGGER.endTimer("GCManageCustomerPickup.addOrderLineKeyToMap");
  }

  /**
   *
   * This method prepares input for updating conatiner detail picked up quantity
   *
   * @param shipmentKey
   * @param shipmentContainerList
   * @param map1
   * @param eleInput
   * @return
   *
   */
  private YFCDocument getInputForChangeShipmentForPickedUpQty(String shipmentKey,
      Set<String> shipmentContainerList,
      Map<String, HashMap<String, Double>> map1, YFCElement eleInput) {
    LOGGER.beginTimer("GCManageCustomerPickup.getInputForChangeShipmentForPickedUpQty");
    LOGGER.verbose("GCManageCustomerPickup.getInputForChangeShipmentForPickedUpQty--Begin");

    // Preparing Input For changeShipment API call to update the Picked up quantity at container
    // detail
    String pickedUpBy = eleInput.getAttribute(GCXmlLiterals.EXTN_PICKED_UP_BY);
    String verificationFlag = eleInput.getAttribute(GCXmlLiterals.EXTN_IS_PICK_UP_PERSON_VERIFIED);
    String idType = eleInput.getAttribute(GCXmlLiterals.ID_TYPE);
    YFCDocument changeShipmentIp = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement rootEle = changeShipmentIp.getDocumentElement();
    rootEle.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
    YFCElement eleContainers = rootEle.createChild(GCConstants.CONTAINERS);
    for (String shipmentContainerKey : shipmentContainerList) {
      YFCElement eleContainer = eleContainers.createChild(GCConstants.CONTAINER);
      eleContainer.setAttribute(GCConstants.SHIPMENT_CONTAINER_KEY, shipmentContainerKey);
      Map<String, Double> containerDetailMap = map1.get(shipmentContainerKey);
      YFCElement eleContainerDetails = eleContainer.createChild(GCConstants.CONTAINER_DETAILS);
      for (Map.Entry<String, Double> entry : containerDetailMap.entrySet()) {
        String containerDetailKey = entry.getKey();
        Double quantity = entry.getValue();
        if (quantity == 0.0) {
          quantity = 1.00;
        }
        YFCElement eleContainerDetail = eleContainerDetails.createChild(GCConstants.CONTAINER_DETAIL);
        eleContainerDetail.setAttribute(GCConstants.CONTAINER_DETAILS_KEY, containerDetailKey);
        YFCElement eleExtn = eleContainerDetail.createChild(GCConstants.EXTN);
        eleExtn.setAttribute(GCXmlLiterals.EXTN_PICKED_UP_QTY, quantity);
        eleExtn.setAttribute(GCXmlLiterals.EXTN_PICKED_UP_BY, pickedUpBy);
        eleExtn.setAttribute(GCXmlLiterals.EXTN_IS_PICK_UP_PERSON_VERIFIED, verificationFlag);
        eleExtn.setAttribute(GCXmlLiterals.EXTN_ID_TYPE_FOR_VERIFICATION, idType);
        YDate date = new YDate(false);
        eleExtn.setAttribute(GCXmlLiterals.EXTN_PICKED_UP_DATE, date.getString("yyyy-MM-dd'T'HH:mm:ss"));
      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input for changeShipment call to update picked up Quantity at container detail: "
          + changeShipmentIp.toString());
    }

    LOGGER.verbose("GCManageCustomerPickup.getInputForChangeShipmentForPickedUpQty--End");
    LOGGER.endTimer("GCManageCustomerPickup.getInputForChangeShipmentForPickedUpQty");
    return changeShipmentIp;

  }


  /**
   *
   * This method prepares input for updating carton status
   *
   * @param eleChangeShipmentOutDoc
   * @param shipmentContainerList
   * @param orderHeaderKey
   * @param env
   * @param employeeId
   * @return
   *
   */
  private YFCDocument getInputForChngeShpmntForCartonStatus(YFCElement eleChangeShipmentOutDoc,
      Set<String> shipmentContainerList, String orderHeaderKey, YFSEnvironment env, String employeeId) {
    LOGGER.beginTimer("GCManageCustomerPickup.getInputForChngeShpmntForCartonStatus");
    LOGGER.verbose("GCManageCustomerPickup.getInputForChngeShpmntForCartonStatus--Begin");
    String shipmentKey = eleChangeShipmentOutDoc.getAttribute(GCConstants.SHIPMENT_KEY);
    YFCDocument changeShipmentIp = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement rootEle = changeShipmentIp.getDocumentElement();
    rootEle.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
    YFCElement eleContainersIp = rootEle.createChild(GCConstants.CONTAINERS);
    YFCElement eleContainers = eleChangeShipmentOutDoc.getChildElement(GCConstants.CONTAINERS);
    YFCNodeList<YFCElement> nlContainerList = eleContainers.getElementsByTagName(GCConstants.CONTAINER);
    for (YFCElement eleContainer : nlContainerList) {
      String shipmentContainerKey = eleContainer.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
      if (shipmentContainerList.contains(shipmentContainerKey)) {
        YFCElement eleContainerDetails = eleContainer.getChildElement(GCConstants.CONTAINER_DETAILS);
        YFCNodeList<YFCElement> nlContainerDetail =
            eleContainerDetails.getElementsByTagName(GCConstants.CONTAINER_DETAIL);
        boolean pickedUpFlag = true;
        for (YFCElement eleContainerDetail : nlContainerDetail) {
          String quantity = eleContainerDetail.getAttribute(GCConstants.QUANTITY);
          YFCElement eleExtn = eleContainerDetail.getChildElement(GCConstants.EXTN);
          String pickedUpQty = eleExtn.getAttribute(GCXmlLiterals.EXTN_PICKED_UP_QTY);
          if (Double.parseDouble(pickedUpQty) < Double.parseDouble(quantity)) {
            pickedUpFlag = false;
            break;
          }
        }

        // if all the container details have been picked up , add container to changeShipment input
        // to update carton status
        if (pickedUpFlag) {
          YFCDocument gcShpmntActvIndoc = YFCDocument.createDocument(GCConstants.GC_SHIPMENT_ACTICITY);
          YFCElement gcShpmntActivity = gcShpmntActvIndoc.getDocumentElement();
          gcShpmntActivity.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
          gcShpmntActivity.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
          gcShpmntActivity.setAttribute(GCConstants.SHIPMENT_CONTAINER_KEY, shipmentContainerKey);
          gcShpmntActivity.setAttribute(GCConstants.USER_ACTION, GCConstants.PICKUP_USER_ACTION);
          gcShpmntActivity.setAttribute(GCConstants.AUDIT_RSNCODE, GCConstants.PICKUP_AUDIT_REASONCODE);
          gcShpmntActivity.setAttribute(GCConstants.USER_ID, employeeId);
          GCCommonUtil.invokeService(env, GCConstants.GC_INSERT_SHPMNT_ACTIVITY, gcShpmntActvIndoc.getDocument());
          YFCElement eleContainerIp = eleContainersIp.createChild(GCConstants.CONTAINER);
          eleContainerIp.setAttribute(GCConstants.SHIPMENT_CONTAINER_KEY, shipmentContainerKey);
          YFCElement eleExtnIp = eleContainerIp.createChild(GCConstants.EXTN);
          eleExtnIp.setAttribute(GCConstants.EXTN_CONTAINER_STATUS, GCConstants.CUSTOMER_PICKED_UP);
        }
      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input for changeShipment call to update carton status: " + changeShipmentIp.toString());
    }

    LOGGER.verbose("GCManageCustomerPickup.getInputForChngeShpmntForCartonStatus--End");
    LOGGER.endTimer("GCManageCustomerPickup.getInputForChngeShpmntForCartonStatus");
    return changeShipmentIp;
  }


  /**
   *
   * This method invokes changeShipmentStatus to update shipment status
   *
   * @param shipmentKey
   * @param eleInput
   * @param env
   * @return
   *
   */
  private void changeShipmentStatus(String shipmentKey, YFCElement eleInput, YFSEnvironment env) {
    LOGGER.beginTimer("GCManageCustomerPickup.changeShipmentStatus");
    LOGGER.verbose("GCManageCustomerPickup.changeShipmentStatus--Begin");
    String docType = eleInput.getAttribute(GCConstants.DOCUMENT_TYPE);
    YFCElement eleContainers = eleInput.getChildElement(GCConstants.CONTAINERS);
    YFCNodeList<YFCElement> nlContainerList = eleContainers.getElementsByTagName(GCConstants.CONTAINER);
    boolean pickedUpFlag = true;
    for (YFCElement eleContainer : nlContainerList) {
      YFCElement eleExtn = eleContainer.getChildElement(GCConstants.EXTN);
      String containerStatus = eleExtn.getAttribute(GCConstants.EXTN_CONTAINER_STATUS);
      if (!containerStatus.equals(GCConstants.CUSTOMER_PICKED_UP)) {
        pickedUpFlag = false;
        break;
      }
    }
    if (pickedUpFlag) {
      YFCDocument changeShipmentStatusIp = YFCDocument.createDocument(GCConstants.SHIPMENT);
      YFCElement rootEle = changeShipmentStatusIp.getDocumentElement();
      rootEle.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
      rootEle.setAttribute(GCConstants.BASE_DROP_STATUS, GCConstants.CUSTOMER_PICKED_UP_SHIPMENT_STATUS);
      if (YFCUtils.equals(docType, GCConstants.ZERO_ZERO_ZERO_FIVE)) {
        rootEle.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.CUSTOMER_PICKED_UP_SHP_TRANID_PO);
      } else {
        rootEle.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.CUSTOMER_PICKED_UP_SHP_TRANID);
      }
      YFCDocument tempdoc = YFCDocument.getDocumentFor(GCConstants.CHANGE_SHIP_TEMP);

      // Invoke changeShipmentStatus API to change the status of the shipment if all the containers
      // are in customer picked up status

      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("Input for changeShipmentStatus API: " + changeShipmentStatusIp.toString());
      }
      GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_SHIPMENT_STATUS, changeShipmentStatusIp, tempdoc);
    }

    LOGGER.verbose("GCManageCustomerPickup.changeShipmentStatus--End");
    LOGGER.endTimer("GCManageCustomerPickup.changeShipmentStatus");
  }

  /**
   *
   * This method prepares invokes changeOrderStatus to update orderline status
   *
   * @param eleInput
   * @param env
   * @param orderHeaderKey
   * @param orderLineKeyMap
   * @param orderLineList
   * @param docType
   * @return
   *
   */

  private void updateOrderLinesWithPickedUpQty(YFCElement eleInput, YFSEnvironment env,
      Map<String, Double> orderLineKeyMap, List<String> orderLineList) {
    LOGGER.beginTimer("GCManageCustomerPickup.updateOrderLinesWithPickedUpQty");
    LOGGER.verbose("GCManageCustomerPickup.updateOrderLinesWithPickedUpQty--Begin");
    String orderLineKey = null;
    Double quantity = 0.00;
    String multiBoxFlag = null;
    String orderReleaseKey = null;
    String headerKeyWithDocType = null;
    String orderHeaderKey = null;
    String docType = null;
    Map<String, String> orderReleaseKeyMap = new HashMap<String, String>();
    Map<String, Set<String>> orderHeaderKeyMap = new HashMap<String, Set<String>>();
    YFCNodeList<YFCElement> nlContainerDetail = eleInput.getElementsByTagName(GCConstants.CONTAINER_DETAIL);
    for (YFCElement eleContainerDetail : nlContainerDetail) {
      multiBoxFlag = eleContainerDetail.getAttribute(GCConstants.IS_MULTIBOX);
      String isBundle = eleContainerDetail.getAttribute(GCConstants.IS_BUNDLE_PARENT);
      String productClass = eleContainerDetail.getAttribute(GCConstants.PRODUCT_CLASS);
      YFCNodeList<YFCElement> nlComponents = eleContainerDetail.getElementsByTagName(GCXmlLiterals.COMPONENT);


      orderLineKey = eleContainerDetail.getAttribute(GCConstants.ORDER_LINE_KEY);
      orderReleaseKey = eleContainerDetail.getAttribute(GCConstants.ORDER_RELEASE_KEY);
      orderHeaderKey = eleContainerDetail.getAttribute(GCConstants.ORDER_HEADER_KEY);
      quantity = eleContainerDetail.getDoubleAttribute(GCConstants.QUANTITY);
      docType = eleContainerDetail.getAttribute(GCConstants.DOCUMENT_TYPE);
      headerKeyWithDocType = orderHeaderKey + GCConstants.UNDERSCORE + docType;
      if (!YFCUtils.equals(isBundle, GCConstants.YES) && !YFCUtils.equals(productClass, GCConstants.SET)) {
        addOrderLineKeyToMap(orderLineKeyMap, orderLineKey, quantity);
        orderReleaseKeyMap.put(orderLineKey, orderReleaseKey);
        if (!orderHeaderKeyMap.containsKey(headerKeyWithDocType)) {
          orderHeaderKeyMap.put(headerKeyWithDocType, new HashSet<String>());
        }
        orderHeaderKeyMap.get(headerKeyWithDocType).add(orderLineKey);
      }
      YFCElement eleWarr = eleContainerDetail.getChildElement(GCXmlLiterals.WARRANTY_COMPONENT);
      if (!YFCCommon.isVoid(eleWarr)) {
        String orderLineKeyW = eleWarr.getAttribute(GCConstants.ORDER_LINE_KEY);
        Double quantityW = eleWarr.getDoubleAttribute(GCConstants.QUANTITY);
        String orderReleaseKeyW = eleWarr.getAttribute(GCConstants.ORDER_RELEASE_KEY);
        String orderHeaderKeyW = eleContainerDetail.getAttribute(GCConstants.ORDER_HEADER_KEY);
        String docTypeW = eleContainerDetail.getAttribute(GCConstants.DOCUMENT_TYPE);
        String headerKeyWithDocTypeW = orderHeaderKeyW + GCConstants.UNDERSCORE + docTypeW;
        addOrderLineKeyToMap(orderLineKeyMap, orderLineKeyW, quantityW);
        orderReleaseKeyMap.put(orderLineKeyW, orderReleaseKeyW);
        if (!orderHeaderKeyMap.containsKey(headerKeyWithDocTypeW)) {
          orderHeaderKeyMap.put(headerKeyWithDocTypeW, new HashSet<String>());
        }
        orderHeaderKeyMap.get(headerKeyWithDocTypeW).add(orderLineKeyW);
      }
      if (YFCCommon.isVoid(multiBoxFlag)) {
        for (YFCElement eleComponent : nlComponents) {
          orderLineKey = eleComponent.getAttribute(GCConstants.ORDER_LINE_KEY);
          quantity = eleComponent.getDoubleAttribute(GCConstants.QUANTITY);
          orderReleaseKey = eleComponent.getAttribute(GCConstants.ORDER_RELEASE_KEY);
          addOrderLineKeyToMap(orderLineKeyMap, orderLineKey, quantity);
          orderReleaseKeyMap.put(orderLineKey, orderReleaseKey);
          orderHeaderKey = eleComponent.getAttribute(GCConstants.ORDER_HEADER_KEY);
          docType = eleComponent.getAttribute(GCConstants.DOCUMENT_TYPE);
          headerKeyWithDocType = orderHeaderKey + GCConstants.UNDERSCORE + docType;
          if (!orderHeaderKeyMap.containsKey(headerKeyWithDocType)) {
            orderHeaderKeyMap.put(headerKeyWithDocType, new HashSet<String>());
          }
          orderHeaderKeyMap.get(headerKeyWithDocType).add(orderLineKey);
          orderLineList.add(orderLineKey);
        }

      }
    }

    for (Entry<String, Set<String>> orderLineEntry : orderHeaderKeyMap.entrySet()) {
      String orderHeaderKeyDocIp = orderLineEntry.getKey();
      String[] headerkeyDoc = orderHeaderKeyDocIp.split(GCConstants.UNDERSCORE);
      String orderHeaderKeyIp = headerkeyDoc[0];
      String docTypeIp = headerkeyDoc[1];
      Set<String> orderLinesIpSet = orderLineEntry.getValue();
      YFCDocument changeOrderStatusIp = YFCDocument.createDocument(GCConstants.ORDER_STATUS_CHANGE);
      YFCElement rootEle = changeOrderStatusIp.getDocumentElement();
      rootEle.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKeyIp);
      if (YFCUtils.equals(GCConstants.ZERO_ZERO_ZERO_FIVE, docTypeIp)) {
        rootEle.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.CUSTOMER_PICKED_UP_ORD_TRANID_PO);
      } else {
        rootEle.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.CUSTOMER_PICKED_UP_ORD_TRANID);
      }
      YFCElement eleOrderLines = rootEle.createChild(GCConstants.ORDER_LINES);
      for (String orderLineKeyInHeaderMap : orderLinesIpSet) {
        Double quantityIp = orderLineKeyMap.get(orderLineKeyInHeaderMap);
        String orderReleaseKeyIp = orderReleaseKeyMap.get(orderLineKeyInHeaderMap);
        YFCElement eleOrderLine = eleOrderLines.createChild(GCConstants.ORDER_LINE);
        eleOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKeyInHeaderMap);
        eleOrderLine.setAttribute(GCConstants.QUANTITY, quantityIp);
        eleOrderLine.setAttribute(GCConstants.ORDER_RELEASE_KEY, orderReleaseKeyIp);
        eleOrderLine.setAttribute(GCConstants.BASE_DROP_STATUS, GCConstants.CUSTOMER_PICKED_UP_ORDER_STATUS);
      }
      YFCDocument chngOrderStatusOpTemp = YFCDocument.getDocumentFor(GCConstants.CHANGE_ORDER_STATUS_TEMP);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("Input for changeOrderStatus API: " + changeOrderStatusIp.toString());
      }
      YFCDocument changeOrderStatusOp =
          GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER_STATUS, changeOrderStatusIp, chngOrderStatusOpTemp);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("Output of  changeOrderStatus API: " + changeOrderStatusOp.toString());
      }
    }
    LOGGER.verbose("GCManageCustomerPickup.updateOrderLinesWithPickedUpQty--End");
    LOGGER.endTimer("GCManageCustomerPickup.updateOrderLinesWithPickedUpQty");
  }



  /**
   *
   * This method adds order line attributes to the element service
   *
   * @param eleInput
   * @param eleShipmentLines
   * @return
   *
   */
  private void addLineDetailForReceipt(YFSEnvironment env, YFCElement eleInput, YFCElement eleShipmentLines) {
    LOGGER.beginTimer("GCManageCustomerPickup.addLineDetailForReceipt");
    LOGGER.verbose("GCManageCustomerPickup.addLineDetailForReceipt--Begin");

    String itemID = eleInput.getAttribute(GCConstants.ITEM_ID);
    String itemDesc = eleInput.getAttribute(GCConstants.ITEM_DESCRIPTION);
    String salesTicketNo = eleInput.getAttribute(GCConstants.EXTN_POS_SALES_TICKET_NO);
    String serialNo = eleInput.getAttribute(GCConstants.SERIAL_NUMBER);
    Double quantity = eleInput.getDoubleAttribute(GCConstants.QUANTITY);
    String unitPrice = eleInput.getAttribute(GCConstants.UNIT_PRICE);
    
    //GCSTORE-4769 - Start
    String sOrderLineKey = eleInput.getAttribute(GCConstants.ORDER_LINE_KEY);
    if(!YFCCommon.isStringVoid(sOrderLineKey)){
      YFCDocument getOrdLineListDoc = YFCDocument.getDocumentFor("<OrderLine OrderLineKey='"+sOrderLineKey+"'/>");
      YFCDocument getOrdLineListTemp = YFCDocument.getDocumentFor("<OrderLineList><OrderLine OrderHeaderKey='' OrderLineKey=''>"
          + "<ComputedPrice LineTotal=''/></OrderLine></OrderLineList>");
      YFCDocument getOrdLineListOutDoc = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LINE_LIST, getOrdLineListDoc, getOrdLineListTemp);
      YFCElement eleRoot = getOrdLineListOutDoc.getDocumentElement();
      YFCElement eleOrderLine = eleRoot.getChildElement(GCConstants.ORDER_LINE);
      if(!YFCCommon.isVoid(eleOrderLine)){
        YFCElement eleComputedPrice = eleOrderLine.getChildElement(GCConstants.COMPUTED_PRICE);
        unitPrice = eleComputedPrice.getAttribute(GCConstants.LINE_TOTAL);
      }
    }
    //GCSTORE-4769 - End
    
    YFCElement eleShipmentLine = eleShipmentLines.createChild(GCConstants.SHIPMENT_LINE);
    eleShipmentLine.setAttribute(GCConstants.ITEM_ID, itemID);
    eleShipmentLine.setAttribute(GCConstants.ITEM_DESCRIPTION, itemDesc);
    eleShipmentLine.setAttribute(GCConstants.QUANTITY, String.format("%.0f", quantity));
    eleShipmentLine.setAttribute(GCConstants.EXTN_POS_SALES_TICKET_NO, salesTicketNo);
    eleShipmentLine.setAttribute(GCConstants.SERIAL_NUMBER, serialNo);
    eleShipmentLine.setAttribute(GCConstants.UNIT_PRICE, unitPrice);
    eleShipmentLines.importNode(eleShipmentLine);
    LOGGER.verbose("GCManageCustomerPickup.addLineDetailForReceipt--End");
    LOGGER.endTimer("GCManageCustomerPickup.addLineDetailForReceipt");
  }

}
