/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 * *###########################################
 * ######################################################
 * ################################################################ OBJECTIVE: This class is written
 * to implement user authentication using LDAP.
 * #####################################################
 * #############################################
 * ############################################################### Version Date Modified By
 * Description
 * ######################################################################################
 * ########################################################################### 1.0 20/03/2014
 * Mittal, Yashu JIRA No: Description of issue. 1.1 11/08/2014 Mittal, Yashu OMS-3032 1.2 23/12/2014
 * Mitta, Yashu Removed unused methods(findDN, encryptPassword, decryptPassword) and variables.
 * #####
 * #############################################################################################
 * ###############################################################
 */
package com.gc.exstore.api.login;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:email Address">Shwetha Rangarajan</a>
 */
public class GCValidateLDAPLogin implements YIFCustomApi {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCValidateLDAPLogin.class.getName());

  /**
   * Description of authenticate
   *
   * @param sLoginID
   * @param sPassword
   * @return
   * @throws Exception
   * @see com.yantra.yfs.japi.util.YFSAuthenticator#authenticate(java.lang.String, java.lang.String)
   */

  public Document validateLDAPLogin(YFSEnvironment yfsEnv, Document inDoc) {

    LOGGER.debug(" Entering GCValidateLDAPLogin.validateLDAPLogin() method ");

    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement rootEle = inputDoc.getDocumentElement();
    String loginID = rootEle.getAttribute(GCConstants.LOGIN_ID);
    String userIdDomain = rootEle.getAttribute(GCConstants.USER_ID_DOMAIN);
    String sldapDN = userIdDomain + "\\" + loginID;

    GCCommonUtil.ldapConnection(rootEle, sldapDN);
    String userGrpID = GCConstants.BLANK_STRING;
    boolean isAuthSuccess = false;
    List<String> listUserGroup = new ArrayList<String>();
    List<String> listUserType = new ArrayList<String>();
    YFCDocument getUserHierarchyIndoc = YFCDocument.createDocument(GCConstants.USER);
    YFCElement userEle = getUserHierarchyIndoc.getDocumentElement();
    String sldapD = userIdDomain + "@" + loginID;
    userEle.setAttribute(GCConstants.ATTR_LOGIN_ID, sldapD);

    YFCDocument getUserHierarchyTemp = YFCDocument.getDocumentFor(GCConstants.GET_USER_HEIRARCHY_TEMPLATE);

    YFCDocument getUserHierarchyOut = GCCommonUtil.invokeAPI(yfsEnv, GCConstants.GET_USER_HEIRARCHY_API, getUserHierarchyIndoc, getUserHierarchyTemp);
    YFCElement user = getUserHierarchyOut.getDocumentElement();
    YFCElement userGrpLists = user.getChildElement(GCConstants.USER_GROUP_LISTS);
    if (!YFCCommon.isVoid(userGrpLists) && userGrpLists.hasChildNodes()) {
      final YFCNodeList<YFCElement> listgroup = userGrpLists.getElementsByTagName(GCConstants.USER_GROUP);
      for (YFCElement userGroupEle : listgroup) {
        userGrpID = userGroupEle.getAttribute(GCConstants.USER_GROUP_ID);
        listUserGroup.add(userGrpID);
      }
    }

    YFCDocument getCommonCodeListIndoc = YFCDocument.createDocument(GCConstants.COMMON_CODE);
    YFCElement commonCode = getCommonCodeListIndoc.getDocumentElement();
    boolean isManagerApp = rootEle.getBooleanAttribute(GCConstants.IS_MANAGER_APPROVAL);

    if (isManagerApp) {
      commonCode.setAttribute(GCConstants.CODE_TYPE, GCConstants.GC_EXSTORE_MGR_GRP);
      commonCode.setAttribute(GCConstants.CODE_SHORT_DESCRIPTION, GCConstants.MANAGER_USER);
    } else {
      commonCode.setAttribute(GCConstants.CODE_TYPE, GCConstants.GC_EXSTORE_USR_AUTH);
      commonCode.setAttribute(GCConstants.CODE_SHORT_DESCRIPTION, GCConstants.AUTH_USER);
    }

    Document getCommonCodeListOutput = GCCommonUtil.invokeAPI(yfsEnv, GCConstants.GET_COMMON_CODE_LIST, getCommonCodeListIndoc.getDocument());
    YFCDocument getCommonCodeListOutdoc = YFCDocument.getDocumentFor(getCommonCodeListOutput);
    YFCElement commonCodeList = getCommonCodeListOutdoc.getDocumentElement();
    YFCIterable<YFCElement> commonCodeEleList = commonCodeList.getChildren();
    for(YFCElement commonCodeEle : commonCodeEleList){
      String commonCodevalue = commonCodeEle.getAttribute(GCConstants.CODE_VALUE);
      listUserType.add(commonCodevalue);
    }
    for (String userGroup : listUserType) {
      if (listUserGroup.contains(userGroup)) {
        isAuthSuccess = true;
        break;
      }
    }

    if (!isAuthSuccess) {
      YFCException yfce = new YFCException(GCErrorConstants.INVALID_USER_GROUP_ID);
      yfce.setAttribute(GCConstants.USER_ID, loginID);
      throw yfce;
    }
    return inputDoc.getDocument();
  }



  @Override
  public void setProperties(Properties arg0) {
  }
}
