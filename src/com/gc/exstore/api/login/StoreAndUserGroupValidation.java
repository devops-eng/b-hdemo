package com.gc.exstore.api.login;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;


/**
 * This class is used to validate StoreID & access permission of the user(UserStory# GCSTORE-46,
 * GCSTORE-48).
 *
 * @author shanil.sharma
 *
 */
public class StoreAndUserGroupValidation {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCCommonUtil.class);

  // to store list of Eligible UserTypes
  List<String> listUserType = new ArrayList<String>();

  /**
   * This method is used to check validity of StoreID by calling getOrganizationHeirarchy API in
   * case of store user.
   *
   * @param env
   * @param inDoc
   * @return response of login API call.
   */
  public Document validateStoreID(YFSEnvironment env, Document inputDoc) {

    LOGGER.beginTimer("StoreAndUserGroupValidation.validateStoreID");
    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    LOGGER.verbose("Class: StoreAndUserGroupValidation Method: validateStoreID BEGIN");
    final YFCElement rootEle = inDoc.getDocumentElement();

    // Checking StoreID only if the user is Store User i.e. UserType = "storeuser"
    if (GCConstants.VALUE_STORE_USER.equals(rootEle.getAttribute(GCConstants.USER_TYPE))
        || GCConstants.FLAG_Y.equals(rootEle.getAttribute(GCConstants.IS_OVERRIDE_STORE))) {

      final String storeID = rootEle.getAttribute(GCConstants.STORE_ID);
      final String loginID = rootEle.getAttribute(GCXmlLiterals.LOGIN_ID);

      GCCommonUtil.validateStore(env, storeID, loginID);

    }
    // Call validateUserGroup() method
    if (GCConstants.FLAG_N.equals(rootEle.getAttribute(GCConstants.IS_OVERRIDE_STORE))) {
      validateUserGroup(env, rootEle);
    }

    // setting UserGroupID back in input xml
    if (YFCUtils.equals(GCConstants.VALUE_STORE_USER, rootEle.getAttribute(GCConstants.USER_TYPE))) {
      rootEle.setAttribute(GCConstants.USR_GRP_ID, "StoreUserGroup");
    } else {
      rootEle.setAttribute(GCConstants.USR_GRP_ID, "StoreAdminGroup");
    }

    LOGGER.verbose("Class: StoreAndUserGroupValidation Method: validateStoreID END");
    LOGGER.endTimer("StoreAndUserGroupValidation.validateStoreID");
    return inputDoc;

  }

  /**
   * This method is used to prepare the input xml & call login api.
   *
   * @param env
   * @param rootEle
   * @return login API response Doc.
   */
  private void validateUserGroup(YFSEnvironment env, YFCElement rootEle) {

    LOGGER.beginTimer("StoreAndUserGroupValidation.validateUserGroup");
    LOGGER.verbose("Class: StoreAndUserGroupValidation Method: validateUserGroup BEGIN");

    callGetCommonCodeListAPI(env, rootEle);

    boolean isAvailable = false;

    // to store list of UserGroups assigned to user
    List<String> listUserGroup = prepareUserGroupList(env, rootEle);

    // Iterating through each group in common code and checking whether it is assigned to user or
    // not.
    for (String userGroup : listUserType) {
      if (listUserGroup.contains(userGroup)) {
        isAvailable = true;
        break;
      }
    }

    if (!isAvailable) {

      YFCException yfce = new YFCException(GCErrorConstants.INVALID_USER_GROUP_ID);
      yfce.setAttribute(GCConstants.USER_ID, rootEle.getAttribute(GCXmlLiterals.LOGIN_ID));
      throw yfce;

    }
    LOGGER.endTimer("StoreAndUserGroupValidation.validateUserGroup");
    LOGGER.verbose("Class: StoreAndUserGroupValidation Method: validateUserGroup END");
  }

  /**
   * This method is used to prepare the list of usergroups assigned to a particular user.
   *
   * @param env
   * @param rootEle
   * @return
   */
  private List<String> prepareUserGroupList(YFSEnvironment env, YFCElement rootEle) {

    LOGGER.beginTimer("StoreAndUserGroupValidation.prepareUserGroupList");
    LOGGER.verbose("Class: StoreAndUserGroupValidation Method: prepareUserGroupList BEGIN");
    // to store list of UserGroups assigned to user
    List<String> listUserGroup = new ArrayList<String>();
    YFCDocument getUserHierarchyIndoc = YFCDocument.createDocument(GCConstants.USER);
    YFCElement userEle = getUserHierarchyIndoc.getDocumentElement();
    userEle.setAttribute(GCConstants.ATTR_LOGIN_ID, rootEle.getAttribute(GCXmlLiterals.LOGIN_ID));

    YFCDocument getUserHierarchyTemp = YFCDocument.getDocumentFor(GCConstants.GET_USER_HEIRARCHY_TEMPLATE);

    YFCDocument getUserHierarchyOut =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_USER_HEIRARCHY_API, getUserHierarchyIndoc, getUserHierarchyTemp);
    YFCElement user = getUserHierarchyOut.getDocumentElement();
    YFCElement userGrpLists = user.getChildElement(GCConstants.USER_GROUP_LISTS);
    if (!YFCCommon.isVoid(userGrpLists) && userGrpLists.hasChildNodes()) {
      YFCNodeList<YFCElement> userGroupList = userGrpLists.getElementsByTagName(GCConstants.USER_GROUP);

      if (0 == userGroupList.getLength()) {
        YFCException yfce = new YFCException(GCErrorConstants.INVALID_USER_GROUP_ID);
        yfce.setAttribute(GCConstants.USER_ID, rootEle.getAttribute(GCXmlLiterals.LOGIN_ID));
        throw yfce;
      }
      for (YFCElement eleUserGroup : userGroupList) {
        listUserGroup.add(eleUserGroup.getAttribute(GCConstants.USER_GROUP_ID));
      }
    } else {
      YFCException yfce = new YFCException(GCErrorConstants.INVALID_USER_GROUP_ID);
      yfce.setAttribute(GCConstants.USER_ID, rootEle.getAttribute(GCXmlLiterals.LOGIN_ID));
      throw yfce;
    }

    LOGGER.verbose("Class: StoreAndUserGroupValidation Method: prepareUserGroupList END");
    LOGGER.endTimer("StoreAndUserGroupValidation.prepareUserGroupList");
    return listUserGroup;
  }

  /**
   * This method prepares input & call getCommonCodeList API
   *
   * @param env
   * @param rootEle
   * @return userType
   */
  private void callGetCommonCodeListAPI(YFSEnvironment env, YFCElement rootEle) {

    LOGGER.beginTimer("StoreAndUserGroupValidation.callGetCommonCodeListAPI");
    LOGGER.verbose("Class: StoreAndUserGroupValidation Method: callGetCommonCodeListAPI BEGIN");
    final String userID = rootEle.getAttribute(GCXmlLiterals.LOGIN_ID);

    // preparing input for API call
    final YFCDocument commonCodeinXMLYfc = YFCDocument.createDocument(GCConstants.COMMON_CODE);
    final YFCElement commonCodeEle = commonCodeinXMLYfc.getDocumentElement();
    commonCodeEle.setAttribute(GCConstants.CODE_TYPE, GCConstants.GC_EXSTORE_USR_GRP);
    commonCodeEle.setAttribute(GCConstants.CODE_SHORT_DESCRIPTION, rootEle.getAttribute(GCConstants.USER_TYPE));

    LOGGER.verbose("commonCodeinXML : " + commonCodeinXMLYfc.getString());
    final YFCDocument commonCodeTemplate = YFCDocument.getDocumentFor(GCConstants.COMMON_CODE_TEMPLATE);

    // calling getCommonCodeList API
    final YFCDocument outXMLYfc =
        GCCommonUtil.invokeAPI(env, GCConstants.API_GET_COMMON_CODE_LIST, commonCodeinXMLYfc, commonCodeTemplate);


    YFCElement eleOutXml = outXMLYfc.getDocumentElement();
    LOGGER.verbose("outXML : " + outXMLYfc.getString());
    final YFCNodeList<YFCElement> listCommonCode = eleOutXml.getElementsByTagName(GCConstants.COMMON_CODE);

    final int lengthCommonCode = listCommonCode.getLength();
    LOGGER.verbose("lengthCommonCode : " + lengthCommonCode);
    if (0 == lengthCommonCode) {
      YFCException yfce = new YFCException(GCErrorConstants.INVALID_USER_GROUP_ID);
      yfce.setAttribute(GCConstants.USER_ID, userID);
      throw yfce;
    }

    // Prepare List of userTypes
    for (int i = 0; i < lengthCommonCode; i++) {
      final YFCElement eleCommonCode = listCommonCode.item(i);
      final String userType = eleCommonCode.getAttribute(GCConstants.COMMON_CODE_CODE_VALUE);
      listUserType.add(userType);
    }
    LOGGER.verbose("Class: StoreAndUserGroupValidation Method: callGetCommonCodeListAPI END");
    LOGGER.endTimer("StoreAndUserGroupValidation.callGetCommonCodeListAPI");
  }
}
