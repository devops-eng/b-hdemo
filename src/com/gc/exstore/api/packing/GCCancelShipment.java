/**
 * Copyright 2014 Guitar Center. All rights reserved. Guitar Center PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 *
 * Change Log
 * #######################################################################################
 * ###############################################################################################
 * OBJECTIVE: Foundation Class
 * ######################################################################
 * ############################
 * #################################################################################### Version Date
 * Modified By Description
 * ##########################################################################
 * ########################
 * #################################################################################### 1.0
 * 01/16/2015 Rangarajan,Shwetha The class contains logic to cancel a particular shipment line from
 * Exstore Pack Detail page.
 * ########################################################################
 * ##########################
 * ##############################################################################################################################################
 * 1.1		05/16/2017		 Kumar,Shashank		Changes made for (GCSTROE-6877) cancellation of free gift item when the parent is being cancelled
 * ############################################################################################################################################## 
 */
package com.gc.exstore.api.packing;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCWebServiceUtil;
import com.gc.exstore.utils.GCShipmentUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * The class handles logic to cancel a particular shipment line from the ExStore Pack Detail page.
 * @author <a href="mailto:shwetha.rangarajan@expicient.com">Shwetha</a>
 *
 */
public class GCCancelShipment implements YIFCustomApi{

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCCancelShipment.class);

  /**
   * The method calls undoBackroomPick API to undo the backroom picked quantities. Also a POS call
   * is made to cancel the reservation previously made. A changeShipment API call is made to cancel
   * the specified quantity of the Shipment line. Finally the GCUserActivity table is updated with
   * the shipment details.
   *
   * @param env
   * @param inputDoc
   * @return
   * @throws UnknownHostException
   */
  public Document cancelShipment(YFSEnvironment env, Document inputDoc) throws UnknownHostException {

    LOGGER.beginTimer("GCCancelShipment.cancelShipment");
    LOGGER.verbose("GCCancelShipment.cancelShipment -- Begin");
    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Indoc :" + inDoc.toString());
    }
    YFCElement shipInEle = inDoc.getDocumentElement();
    String shipmentKey = shipInEle.getAttribute(GCConstants.SHIPMENT_KEY);
    String isCompleteCancel = shipInEle.getAttribute(GCConstants.IS_COMPLETE_CANCEL);

    YFCDocument getShpListIndoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCDocument getShipmentListOutdoc = callGetShipmentList(env, shipmentKey, getShpListIndoc);

    YFCElement shipmentLinesInEle  = shipInEle.getChildElement(GCConstants.SHIPMENT_LINES);
    YFCElement shipmentLineEle = shipmentLinesInEle.getChildElement(GCConstants.SHIPMENT_LINE);
    String shipLineKeyInput = shipmentLineEle.getAttribute(GCConstants.SHIPMENT_LINE_KEY);
    String qtyToBeCancelled = shipmentLineEle.getAttribute(GCConstants.QUANTITY);
    LOGGER.verbose("ShipmentLineKey fetched :" + shipLineKeyInput);
    LOGGER.verbose("Quantity to be cancelled :" + qtyToBeCancelled);

    YFCElement shipmentsOutEle = getShipmentListOutdoc.getDocumentElement();
    YFCElement shipmentInList = shipmentsOutEle.getChildElement(GCConstants.SHIPMENT);
    YFCElement shipmentLinesInList = shipmentInList.getChildElement(GCConstants.SHIPMENT_LINES);
    YFCIterable<YFCElement> shipmentLineList = shipmentLinesInList.getChildren();

    Map<String, String> orderLineKeyMap = new HashMap<String, String>();
    Map<String, String> shipLineKeyMap = new HashMap<String, String>();
    List<String> shipLineKeyList = new ArrayList<String>();
    List<String> bundleCompList = new ArrayList<String>();
    Map<String, String> associatedLineMap = new HashMap<String, String>();

    String shipNode = shipmentInList.getAttribute(GCConstants.SHIP_NODE);

    // Creating Document for Adjust Inventory API call
    YFCDocument adjustInvDoc = YFCDocument.createDocument(GCConstants.ITEMS_ELE);
    YFCElement eleItems = adjustInvDoc.getDocumentElement();

    if (!YFCUtils.equals(GCConstants.READY_FOR_PACKING_STATUS, shipmentInList.getAttribute(GCConstants.STATUS))) {
      callChangeShipmentStatus(env, shipmentKey, GCConstants.READY_FOR_PACKING_STATUS,
          GCConstants.UNDO_PACK_SHIP_COMPLETE);
    }

    for(YFCElement shipmentLine : shipmentLineList){

      YFCElement orderLine = shipmentLine.getChildElement(GCConstants.ORDER_LINE);
      String shipmentLineKey = shipmentLine.getAttribute(GCConstants.SHIPMENT_LINE_KEY);
      String orderLineKey = orderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      String shipmentLineNo = orderLine.getAttribute(GCConstants.SHIPMENT_LINE_NO);


      if(YFCUtils.equals(shipLineKeyInput, shipmentLineKey)){

        YFCElement extnShipmentLine = shipmentLine.getChildElement(GCConstants.EXTN);
        final String actualSerialNo = extnShipmentLine.getAttribute(GCConstants.EXTN_ACTUAL_SERIAL_NO);

        boolean isSerialAvailable = true;
        if (!YFCCommon.isVoid(actualSerialNo)) {
          isSerialAvailable = checkActualSrialNo(env, shipmentLine, shipNode, actualSerialNo);
        }
        String requestedTagNo = shipmentLine.getAttribute(GCConstants.REQ_TAG_NO);
        if (!YFCCommon.isVoid(actualSerialNo) && !YFCUtils.equals(actualSerialNo, requestedTagNo)) {
          if (!(!isSerialAvailable && YFCCommon.isVoid(requestedTagNo))) {
            stampAttrForAdjInv(eleItems, shipmentLine, shipNode, isSerialAvailable);
          }
        }
        callUndoBackroomPickAPI(env, shipLineKeyInput, shipmentInList);

        shipLineKeyMap.put(shipmentLineKey, qtyToBeCancelled);
        LOGGER.verbose("Updated ShipLineKey Map :" + shipLineKeyMap.toString());

        LOGGER.verbose("Updated OrderLineKey Map :" + orderLineKeyMap.toString());
        shipLineKeyList.add(shipmentLineKey);
        if(YFCUtils.equals(GCConstants.BUNDLE, shipmentLine.getAttribute(GCConstants.KIT_CODE))){
          YFCElement itemDtlsEle = orderLine.getChildElement(GCConstants.ITEM_DETAILS);
          YFCElement extnInItem = itemDtlsEle.getChildElement(GCConstants.EXTN);
          String extnPrimaryComponentId = extnInItem.getAttribute(GCConstants.EXTN_PRIMARY_COMPONENT_ID);
          addBundleComponentstoMap(env, shipmentLinesInList, shipmentLineKey, orderLineKey, shipLineKeyMap,
              orderLineKeyMap, bundleCompList, eleItems, shipNode);
          prepareAssociatedLineMap(shipmentLinesInList, extnPrimaryComponentId, associatedLineMap, shipmentLineKey,
              orderLineKey);
        } else{
          stampRegularAssociatedMap(shipmentLinesInList, associatedLineMap, orderLineKey, shipmentLineNo);
          orderLineKeyMap.put(orderLineKey, qtyToBeCancelled);
        }
        checkWarrantyAssociation(shipmentLinesInList, orderLineKey, shipLineKeyMap, orderLineKeyMap);

      }

    }

    callChangeOrderStatusAPI(env, shipmentInList, orderLineKeyMap);
    callChangeOrderAPI(env, shipmentInList, shipmentLineEle, orderLineKeyMap);
    callPOSForCancelReservation(env, shipmentInList, shipInEle, orderLineKeyMap, associatedLineMap);
    YFCDocument outEle = callChangeShipmentAPI(env, shipmentInList, shipLineKeyMap, isCompleteCancel);
    callChangeShipmentStatus(env, outEle, shipmentKey);
    updateUserActivityTable(env, shipInEle, GCConstants.PACK_REJECT_LINE, GCConstants.REJECT_LINE);

    // Calling adjustInventory API
    if (eleItems.hasChildNodes()) {
      LOGGER.verbose("adjustInvDoc : " + adjustInvDoc.toString());
      GCCommonUtil.invokeAPI(env, GCConstants.API_ADJUST_INVENTORY, adjustInvDoc.getDocument());
    }

    LOGGER.verbose("GCCancelShipment.cancelShipment -- END");
    LOGGER.endTimer("GCCancelShipment.cancelShipment");
    return inputDoc;

  }

  /**
   * This method is used to check whether the ActualSrialNo is available in oms or not by calling
   * getSupplyDetails API.
   *
   * @param env
   * @param eleShipLine
   * @param shipNode
   * @param actualSerialNo
   * @return
   */
  private boolean checkActualSrialNo(YFSEnvironment env, YFCElement eleShipLine, String shipNode, String actualSerialNo) {

    LOGGER.beginTimer("GCConfirmPickAPI.checkActualSrialNo");
    LOGGER.verbose("GCConfirmPickAPI.checkActualSrialNo--Begin");
    // Creating a New Document of getSupplyDetails
    YFCDocument getSupplyDetailsinDoc = YFCDocument.createDocument(GCConstants.GET_SUPPLY_DETAILS);
    YFCElement eleGetSupplyDetails = getSupplyDetailsinDoc.getDocumentElement();
    eleGetSupplyDetails.setAttribute(GCConstants.ITEM_ID, eleShipLine.getAttribute(GCConstants.ITEM_ID));
    eleGetSupplyDetails.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GCI);
    eleGetSupplyDetails.setAttribute(GCConstants.PRODUCT_CLASS, eleShipLine.getAttribute(GCConstants.PRODUCT_CLASS));
    eleGetSupplyDetails.setAttribute(GCConstants.SHIP_NODE, shipNode);
    eleGetSupplyDetails.setAttribute(GCConstants.UNIT_OF_MEASURE, eleShipLine.getAttribute(GCConstants.UOM));
    YFCElement eleTag = eleGetSupplyDetails.createChild(GCConstants.TAG);
    eleTag.setAttribute(GCConstants.BATCH_NO, actualSerialNo);


    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input to getSupplyDetails API : " + getSupplyDetailsinDoc.getString());
    }

    YFCDocument getSupplyDetailsTemplate = YFCDocument.getDocumentFor(GCConstants.GET_SUPPLY_DETAILS_TEMPLATE);

    // calling getSupplyDetails to get the Tag supply of actualSerialNo available in OMS for this
    // item
    YFCDocument outDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_SUPPLY_DETAILS, getSupplyDetailsinDoc, getSupplyDetailsTemplate);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Output from getSupplyDetails :" + outDoc.getString());
    }

    YFCElement eleOutDocItem = outDoc.getDocumentElement();
    YFCElement eleOutDocShipNodes = eleOutDocItem.getChildElement(GCXmlLiterals.SHIP_NODES);
    if (eleOutDocShipNodes.hasChildNodes()) {
      LOGGER.verbose("GCConfirmPickAPI.checkActualSrialNo--end");
      LOGGER.endTimer("GCConfirmPickAPI.checkActualSrialNo");
      return true;
    } else {
      LOGGER.verbose("GCConfirmPickAPI.checkActualSrialNo--end");
      LOGGER.endTimer("GCConfirmPickAPI.checkActualSrialNo");
      return false;
    }
  }

  /**
   * Stamp AssociatedLineNo in associatedLineMap for Regular items.
   *
   * @param shipmentLinesInList
   *
   * @param associatedLineMap
   * @param shipmentLineNo
   * @param shipmentLine
   * @param shipmentLineKey
   */
  private void stampRegularAssociatedMap(YFCElement shipmentLinesInList, Map<String, String> associatedLineMap,
      String orderLineKey, String shipmentLineNo) {


    YFCIterable<YFCElement> shipmentLineList = shipmentLinesInList.getChildren();
    for (YFCElement shipmentLine : shipmentLineList) {

      YFCElement warrantyOrderLine = shipmentLine.getChildElement(GCConstants.ORDER_LINE);
      String dependentOnLineKey = warrantyOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
      if (YFCUtils.equals(orderLineKey, dependentOnLineKey)) {
        associatedLineMap.put(shipmentLine.getAttribute(GCConstants.SHIPMENT_LINE_KEY), shipmentLineNo);
      }
    }
  }

  /**
   * The method prepares a map with OrderLine details of all associated warranties of the incoming
   * ShipmentLine.
   *
   * @param shipmentLinesInList
   * @param extnPrimaryComponentId
   * @param associatedLineMap
   * @param shipmentLineKey
   * @param orderLineKey
   */
  private void prepareAssociatedLineMap(YFCElement shipmentLinesInList, String extnPrimaryComponentId,
      Map<String, String> associatedLineMap, String shipmentLineKey, String orderLineKey) {

    LOGGER.beginTimer("GCCancelShipment.prepareAssociatedLineMap");
    LOGGER.verbose("GCCancelShipment.prepareAssociatedLineMap -- Begin");
    LOGGER.verbose("PrimaryComponentID :" + extnPrimaryComponentId);
    String shipLineNo = GCConstants.BLANK_STRING;
    YFCIterable<YFCElement> shipmentLineList = shipmentLinesInList.getChildren();
    for (YFCElement shipmentLine : shipmentLineList) {
      String parentShipLineKey = shipmentLine.getAttribute(GCConstants.PARENT_SHPMNTLINE_KEY);
      if (YFCUtils.equals(shipmentLineKey, parentShipLineKey)
          && YFCUtils.equals(extnPrimaryComponentId, shipmentLine.getAttribute(GCConstants.ITEM_ID))) {
        shipLineNo = shipmentLine.getAttribute(GCConstants.SHIPMENT_LINE_NO);
        LOGGER.verbose("shipLineNo for Primary Component :" + shipLineNo);
      }
    }

    for (YFCElement shipmentLine : shipmentLineList) {

      YFCElement warrantyOrderLine = shipmentLine.getChildElement(GCConstants.ORDER_LINE);
      String dependentOnLineKey = warrantyOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
      if (YFCUtils.equals(orderLineKey, dependentOnLineKey)) {
        associatedLineMap.put(shipmentLine.getAttribute(GCConstants.SHIPMENT_LINE_KEY), shipLineNo);
      }
    }

    LOGGER.verbose("WarrantyLine Map with associated line number :" + associatedLineMap.toString());
    LOGGER.endTimer("GCCancelShipment.prepareAssociatedLineMap");
    LOGGER.verbose("GCCancelShipment.prepareAssociatedLineMap -- END");
  }

  /**
   * The method calls the POS for cancelling the reservation against an item.
   *
   * @param env
   * @param shipmentInList
   * @param shipInEle
   * @param orderLineKeyMap
   * @param associatedLineMap
   * @throws UnknownHostException
   */
  private void callPOSForCancelReservation(YFSEnvironment env, YFCElement shipmentInList, YFCElement shipInEle,
      Map<String, String> orderLineKeyMap, Map<String, String> associatedLineMap) throws UnknownHostException {

    LOGGER.beginTimer("GCCancelShipment.callPOSForCancelReservation");
    LOGGER.verbose("GCCancelShipment.callPOSForCancelReservation -- END");
    YFCDocument posRequest = YFCDocument.getDocumentFor(GCConstants.POS_CNCL_RSRV_INDOC);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Initial SOAP Request :" + posRequest.toString());
    }
    YFCElement soapEnvEle = posRequest.getDocumentElement();
    YFCElement soapBodyEle = soapEnvEle.getChildElement(GCConstants.SOAP_BODY);
    YFCElement cancelReqEle = soapBodyEle.getChildElement(GCConstants.CANCEL_REQ_ELE);
    YFCElement xmlDocInEle = cancelReqEle.getChildElement(GCConstants.XML_DOC_IN_ELE);
    YFCElement ordersEle = xmlDocInEle.getChildElement(GCConstants.ORDERS);
    YFCElement orderInReqEle = ordersEle.getChildElement(GCConstants.ORDER);

    YFCElement shipmentLinesInList = shipmentInList.getChildElement(GCConstants.SHIPMENT_LINES);
    YFCNodeList<YFCElement> nlShipmentLine = shipmentLinesInList.getElementsByTagName(GCConstants.SHIPMENT_LINE);
    YFCElement extnInShipLine = null;
    YFCElement orderLineEle = null;
    for (YFCElement shipmentLineEle : nlShipmentLine) {
      extnInShipLine = shipmentLineEle.getChildElement(GCConstants.EXTN);
      orderLineEle = shipmentLineEle.getChildElement(GCConstants.ORDER_LINE);
      if (!YFCCommon.isVoid(extnInShipLine.getAttribute(GCConstants.EXTN_RSRVN_ID))) {
        break;
      }
    }

    YFCElement orderEle = orderLineEle.getChildElement(GCConstants.ORDER);
    YFCElement extnInOrderEle = orderEle.getChildElement(GCConstants.EXTN);

    YFCElement posSalesTicketNo = orderInReqEle.getChildElement(GCConstants.POS_SALES_TCKT_ELE);
    posSalesTicketNo.setNodeValue(extnInShipLine.getAttribute(GCConstants.EXTN_RSRVN_ID));

    YFCElement posLocation = orderInReqEle.getChildElement(GCConstants.POS_LOCATION);
    posLocation.setNodeValue(shipmentInList.getAttribute(GCConstants.SHIP_NODE));

    YFCElement cancelAll = orderInReqEle.getChildElement(GCConstants.CANCEL_ALL_ELE);
    cancelAll.setNodeValue(GCConstants.FALSE_STR);

    YFCElement mgrAppr = orderInReqEle.getChildElement(GCConstants.MGR_APPRVL_ELE);
    // Call getEmployeeeID util to fetch ExtnEmployeeID for the logged in user.
    String empID = GCShipmentUtil.getEmployeeID(env, shipInEle.getAttribute(GCConstants.USER_ID));
    // Changes -- End
    mgrAppr.setNodeValue(empID);

    // Fetch Order Date in proper format
    YFCDocument tempDoc = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement tempEle = tempDoc.getDocumentElement();
    Date orderDateAttr = orderEle.getDateAttribute(GCConstants.ORDER_DATE);

    GCDateUtils.getDateTimeForPOS(tempEle, orderDateAttr);

    YFCElement orderDate = orderInReqEle.getChildElement(GCConstants.ORDER_DATE);
    orderDate.setNodeValue(tempEle.getAttribute(GCConstants.ORDER_DATE));

    YFCElement orderTime = orderInReqEle.getChildElement(GCConstants.ORDER_TIME);
    orderTime.setNodeValue(tempEle.getAttribute(GCConstants.ORDER_TIME));

    YFCElement xmlCreatedDate = orderInReqEle.getChildElement(GCConstants.XML_CREATED_DATE);
    YFCElement xmlCreatedTime = orderInReqEle.getChildElement(GCConstants.XML_CREATED_TIME);

    xmlCreatedDate.setNodeValue(tempEle.getAttribute(GCXmlLiterals.XML_DATE));
    xmlCreatedTime.setNodeValue(tempEle.getAttribute(GCXmlLiterals.XML_TIME));

    YFCElement orderId = orderInReqEle.getChildElement(GCConstants.ORDER_ID);
    orderId.setNodeValue(orderEle.getAttribute(GCConstants.ORDER_NO));

    YFCElement ipAddress = orderInReqEle.getChildElement(GCConstants.IP_ADDRESS);
    ipAddress.setNodeValue(extnInOrderEle.getAttribute(GCConstants.EXTN_IP_ADDRESS));

    YFCElement srcCode = orderInReqEle.getChildElement(GCConstants.SOURCE_CODE);
    srcCode.setNodeValue(extnInOrderEle.getAttribute(GCConstants.EXTN_SOURCE_CODE));

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("POS Requst after stampong header attributes :" + posRequest.toString());
    }
    YFCElement productsEle = orderInReqEle.getChildElement(GCConstants.PRODUCTS);
    YFCIterable<YFCElement> shipmentLineList = shipmentLinesInList.getChildren();

    LOGGER.verbose("OrderLineKeyMap :" + orderLineKeyMap.toString());
    LOGGER.verbose("AssociatedLineKey Map :" + associatedLineMap.toString());
    Set<String> orderLineKeyList = orderLineKeyMap.keySet();
    for (String orderLineKey : orderLineKeyList) {

      for (YFCElement shipmentLine : shipmentLineList) {

        YFCElement extnInShipmentLine = shipmentLine.getChildElement(GCConstants.EXTN);
        YFCElement orderLine = shipmentLine.getChildElement(GCConstants.ORDER_LINE);
        YFCElement itemDetails = orderLine.getChildElement(GCConstants.ITEM_DETAILS);
        YFCElement extnInItem = itemDetails.getChildElement(GCConstants.EXTN);
        if (YFCUtils.equals(orderLineKey, orderLine.getAttribute(GCConstants.ORDER_LINE_KEY))) {

          YFCElement productEle = productsEle.createChild(GCConstants.PRODUCT);
          productEle.setAttribute("type", shipmentLine.getAttribute(GCConstants.PRODUCT_CLASS));

          YFCElement lineNumber = productEle.createChild(GCConstants.LINE_NUMBER);
          lineNumber.setNodeValue(orderLine.getAttribute(GCConstants.PRIME_LINE_NO));

          YFCElement skuEle = productEle.createChild(GCConstants.SKU);
          skuEle.setNodeValue(extnInItem.getAttribute(GCConstants.EXTN_POSITEM_ID));

          YFCElement skuNumber = productEle.createChild(GCConstants.SKU_NO);
          skuNumber.setNodeValue(extnInShipmentLine.getAttribute(GCConstants.EXTN_POS_SKU_NO));

          YFCElement serialNo = productEle.createChild(GCConstants.SERIAL_NO);
          serialNo.setNodeValue(extnInShipmentLine.getAttribute(GCConstants.EXTN_ACTUAL_SERIAL_NO));

          YFCElement quantity = productEle.createChild(GCConstants.QUANTITY);
          quantity.setNodeValue(shipmentLine.getAttribute(GCConstants.QUANTITY));

          YFCElement assocLineNo = productEle.createChild(GCConstants.ASSOC_LINE_NO);
          if (associatedLineMap.containsKey(shipmentLine.getAttribute(GCConstants.SHIPMENT_LINE_KEY))) {
            LOGGER.verbose("Match for Associated Line!!");
            assocLineNo.setNodeValue(associatedLineMap.get(shipmentLine.getAttribute(GCConstants.SHIPMENT_LINE_KEY)));
          }
        }
      }
    }

    String sSoapAction = YFSSystem.getProperty(GCConstants.POS_CANCEL_RSRVN_ACTION);
    LOGGER.verbose("SOAP Action :" + sSoapAction);
    String sSoapUrl = YFSSystem.getProperty(GCConstants.POS_CANCEL_RSRVN_URL);
    LOGGER.verbose("SOAP URL :" + sSoapUrl);
    GCWebServiceUtil obj = new GCWebServiceUtil();
    YFCDocument posResponse;
    Document posResponseDoc = null;

    try {

      posResponseDoc = obj.invokeSOAPWebService(posRequest.getDocument(), sSoapUrl, sSoapAction);

    } catch (Exception e) {

      LOGGER.error("Got exception in cancel reservation call", e);
      YFCException yfce = new YFCException(GCErrorConstants.WEB_SERVICE_RESERVATION_CALL_TIMEOUT_CODE);
      throw yfce;

    }

    boolean bHasCallFailed = false;
    String response = GCConstants.BLANK_STRING;
    posResponse = YFCDocument.getDocumentFor(posResponseDoc);
    YFCElement soapEnv = posResponse.getDocumentElement();
    YFCElement soapBody = soapEnv.getChildElement(GCConstants.SBODY);
    YFCElement cancelReqResp = soapBody.getChildElement("CancelRequestResponse");
    YFCElement cancelReqResult = cancelReqResp.getChildElement("CancelRequestResult");
    if (YFCCommon.isVoid(cancelReqResult)) {
      LOGGER.verbose("CancelRequestResult -- VOID");
      bHasCallFailed = true;
    } else {
      response = cancelReqResult.getNodeValue();
      if (YFCCommon.isStringVoid(response)) {
        LOGGER.verbose("Response is void");
        bHasCallFailed = true;
      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("SOAP Response obtained :" + posResponse.toString());
    }

    if (bHasCallFailed) {
      YFCException yfce = new YFCException(GCErrorConstants.WEB_SERVICE_RESERVATION_CALL_TIMEOUT_CODE);
      throw yfce;
    }

    YFCDocument cancelRespDoc = YFCDocument.getDocumentFor(response);
    YFCElement responseEle = cancelRespDoc.getDocumentElement();
    YFCElement successEle = responseEle.getChildElement(GCConstants.SUCCESS);
    if (YFCUtils.equals(GCConstants.FALSE_STR, successEle.getNodeValue())) {
      YFCElement errorInfo = responseEle.getChildElement("Errorinfo");
      LOGGER.verbose("Error from POS :" + errorInfo.getNodeValue());
      YFCException yfce = new YFCException(GCErrorConstants.WEB_SERVICE_RESERVATION_CALL_ERROR_FROM_POS);
      yfce.setErrorDescription(errorInfo.getNodeValue());
      throw yfce;
    }


    LOGGER.endTimer("GCCancelShipment.callPOSForCancelReservation");
    LOGGER.verbose("GCCancelShipment.callPOSForCancelReservation -- END");
  }


  /**
   * The method calls
   * @param env
   * @param shipInEle
   */
  public static void updateUserActivityTable(YFSEnvironment env, YFCElement shipInEle, String auditReason, String action) {

    YFCDocument gcUserActvIndoc = YFCDocument.createDocument(GCConstants.GC_SHIPMENT_ACTICITY);
    YFCElement gcUserActivity = gcUserActvIndoc.getDocumentElement();
    gcUserActivity.setAttribute(GCConstants.SHIPMENT_KEY, shipInEle.getAttribute(GCConstants.SHIPMENT_KEY));
    gcUserActivity.setAttribute(GCConstants.ORDER_HEADER_KEY, shipInEle.getAttribute(GCConstants.ORDER_HEADER_KEY));
    gcUserActivity.setAttribute(GCConstants.USER_ACTION, action);
    gcUserActivity.setAttribute(GCConstants.AUDIT_RSNCODE, auditReason);

    // Call getEmployeeeID util to fetch ExtnEmployeeID for the logged in user.
    String employeeID = GCShipmentUtil.getEmployeeID(env, shipInEle.getAttribute(GCConstants.USER_ID));
    // Changes -- End

    String isManagerApproval = shipInEle.getAttribute(GCConstants.IS_MANAGER_APPROVAL);
    if (YFCUtils.equals(GCConstants.YES, isManagerApproval)) {
      gcUserActivity.setAttribute(GCConstants.MANAGER_ID, employeeID);
    } else {
      gcUserActivity.setAttribute(GCConstants.USER_ID, employeeID);
    }
    if (shipInEle.hasAttribute(GCConstants.CONTAINER_KEY)) {
      gcUserActivity.setAttribute(GCConstants.CONTAINER_KEY, shipInEle.getAttribute(GCConstants.CONTAINER_KEY));
    }

    GCCommonUtil.invokeService(env, GCConstants.GC_INSERT_SHPMNT_ACTIVITY, gcUserActvIndoc.getDocument());

  }

  /**
   *
   * @param env
   * @param shipLineKeyInput
   * @param shipmentInList
   */
  private void callUndoBackroomPickAPI(YFSEnvironment env, String shipLineKeyInput, YFCElement shipmentInList) {

    LOGGER.beginTimer("GCCancelShipment.callUndoBackroomPickAPI");
    LOGGER.verbose("GCCancelShipment.callUndoBackroomPickAPI -- Begin");
    YFCDocument inDoc = YFCDocument.createDocument(GCConstants.UNDO_PICK);
    YFCElement undoPickEle = inDoc.getDocumentElement();
    YFCElement shipmentEle = undoPickEle.createChild(GCConstants.SHIPMENT);
    shipmentEle.setAttribute(GCConstants.SELLER_ORGANIZATION_CODE,
        shipmentInList.getAttribute(GCConstants.SELLER_ORGANIZATION_CODE));
    shipmentEle.setAttribute(GCConstants.SHIP_NODE, shipmentInList.getAttribute(GCConstants.SHIP_NODE));
    shipmentEle.setAttribute(GCConstants.SHIPMENT_KEY, shipmentInList.getAttribute(GCConstants.SHIPMENT_KEY));
    shipmentEle.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.UNDO_BACKROOM_PICK_TRANS);

    YFCElement shipmentLinesEle = shipmentEle.createChild(GCConstants.SHIPMENT_LINES);
    YFCElement shipmentLineEle = shipmentLinesEle.createChild(GCConstants.SHIPMENT_LINE);
    shipmentLineEle.setAttribute(GCConstants.SHIPMENT_LINE_KEY, shipLineKeyInput);

    LOGGER.verbose("Undo Indoc :" + inDoc.toString());
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("undoBackroomPick API Indoc :" + inDoc.toString());
    }
    YFCDocument outDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.UNDO_BACKROOM_PICK, inDoc,
            YFCDocument.createDocument(GCConstants.SHIPMENT));
    LOGGER.verbose("Undo Outdoc :" + outDoc.toString());
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("undoBackroomPick API Output :" + outDoc.toString());
    }
    LOGGER.verbose("GCCancelShipment.callUndoBackroomPickAPI -- END");
    LOGGER.endTimer("GCCancelShipment.callUndoBackroomPickAPI");

  }

  /**
   * The method calls changeShipmentStatus API to move the
   * @param env
   * @param outEle
   * @param shipmentKey
   */
  private void callChangeShipmentStatus(YFSEnvironment env, YFCDocument outEle, String shipmentKey) {

    YFCElement shipEle = outEle.getDocumentElement();
    double containerizedQty = shipEle.getDoubleAttribute(GCConstants.CONTAINERIZED_QUANTITY);
    LOGGER.verbose("containerized Quabtity :" + containerizedQty);
    double totalQty = shipEle.getDoubleAttribute(GCConstants.TOTAL_QTY);
    LOGGER.verbose("Total Qty :" + totalQty);

    if (totalQty != 0) {
      LOGGER.verbose("Complete Cancel!");
      callChangeShipmentStatus(env, shipmentKey, GCConstants.READY_FOR_PACKING_STATUS,
          GCConstants.BCKROOM_PICKED_TRNSCTN);
    }
    if (totalQty != 0 && (containerizedQty == totalQty)) {
      LOGGER.verbose("Completely Packed!");
      callChangeShipmentStatus(env, shipmentKey, GCConstants.SHP_PACKED_STATUS, GCConstants.PCK_SHP_COMPLETE_TRNSCTN);
    } else if (containerizedQty != 0) {
      LOGGER.verbose("Containers are present!");
      callChangeShipmentStatus(env, shipmentKey, GCConstants.SHIPMENT_BEING_PACKED,
          GCConstants.PCK_SHP_COMPLETE_TRNSCTN);
    }

  }

  /**
   * The generic method to call changeShipmentStatus API.
   * @param env
   * @param shipmentKey
   * @param dropStatus
   * @param transactionID
   */
  private void callChangeShipmentStatus(YFSEnvironment env, String shipmentKey, String dropStatus, String transactionID) {

    YFCDocument changeShipStatusIndoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement rootEle = changeShipStatusIndoc.getDocumentElement();
    rootEle.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
    rootEle.setAttribute(GCConstants.BASE_DROP_STATUS, dropStatus);
    rootEle.setAttribute(GCConstants.TRANSACTION_ID, transactionID);
    GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_SHIPMENT_STATUS, changeShipStatusIndoc.getDocument());
    LOGGER.verbose("Status Change Done!");

  }

  /**
   * The method calls the changeOrder API to stamp the CancelReasonCode and ReasonText against the
   * OrderLine being cancelled.
   *
   * @param env
   * @param shipmentInList
   * @param shipmentLineEle
   * @param orderLineKeyMap
   */
  private void callChangeOrderAPI(YFSEnvironment env, YFCElement shipmentInList, YFCElement shipmentLineEle, Map<String, String> orderLineKeyMap) {

    LOGGER.beginTimer("GCCancelShipment.callChangeOrderAPI");
    LOGGER.verbose("GCCancelShipment.callChangeOrderAPI -- Begin");
    YFCDocument changeOrderIndoc = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement orderEle = changeOrderIndoc.getDocumentElement();
    orderEle.setAttribute(GCConstants.ORDER_HEADER_KEY, shipmentInList.getAttribute(GCConstants.ORDER_HEADER_KEY));
    orderEle.setAttribute(GCConstants.ENTERPRISE_CODE, shipmentInList.getAttribute(GCConstants.ENTERPRISE_CODE));
    //Added for GCSTORE-6877--Start
  	orderEle.setAttribute(GCConstants.EXTN_PROG_ID, GCConstants.PROG_ID_EXSTORE); 
  	//End
    YFCElement orderLinesEle = orderEle.createChild(GCConstants.ORDER_LINES);
    
    Set<String> orderLineKeyList = orderLineKeyMap.keySet();
    for(String orderLineKey : orderLineKeyList) {

      YFCElement orderLineEle = orderLinesEle.createChild(GCConstants.ORDER_LINE);
      orderLineEle.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey);
      orderLineEle.setAttribute(GCConstants.REASON_CODE, shipmentLineEle.getAttribute(GCConstants.REASON_CODE));
      orderLineEle.setAttribute(GCConstants.REASON_TEXT, shipmentLineEle.getAttribute(GCConstants.REASON_TEXT));
      //Added for GCSTORE-6877--START
	  orderLineEle.setAttribute(GCConstants.IS_COMPLETE_LINE_CANCEL, GCConstants.YES);
	  //END
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("changeOrder Input :" + changeOrderIndoc.toString());
    }
    GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, changeOrderIndoc.getDocument());
    LOGGER.verbose("GCCancelShipment.callChangeOrderAPI -- END");
    LOGGER.endTimer("GCCancelShipment.callChangeOrderAPI");

  }

  /**
   * The method calls the changeShipment API to cancel the particular ShipmentLine.
   * @param env
   * @param shipmentInList
   * @param shipLineKeyMap
   * @param isCompleteCancel
   * @return
   */
  private YFCDocument callChangeShipmentAPI(YFSEnvironment env, YFCElement shipmentInList,
      Map<String, String> shipLineKeyMap, String isCompleteCancel) {

    LOGGER.beginTimer("GCCancelShipment.callChangeShipmentAPI");
    LOGGER.verbose("GCCancelShipment.callChangeShipmentAPI -- Begin");
    YFCDocument inDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement shipmentEle = inDoc.getDocumentElement();
    shipmentEle.setAttribute(GCConstants.SHIPMENT_KEY, shipmentInList.getAttribute(GCConstants.SHIPMENT_KEY));
    shipmentEle.setAttribute(GCConstants.CANCEL_RMVD_QTY, GCConstants.YES);

    if (YFCUtils.equals(GCConstants.YES, isCompleteCancel)) {
      shipmentEle.setAttribute(GCConstants.ACTION, GCConstants.ACTION_CANCEL_SHIPMENT);
    } else {
      YFCElement shipmentLinesEle = shipmentEle.createChild(GCConstants.SHIPMENT_LINES);
      Set<String> shipmentLineKeyList = shipLineKeyMap.keySet();
      for (String shipmentLineKey : shipmentLineKeyList) {

        YFCElement shipmentLineEle = shipmentLinesEle.createChild(GCConstants.SHIPMENT_LINE);
        YFCElement shipmentLinesInList = shipmentInList.getChildElement(GCConstants.SHIPMENT_LINES);
        YFCIterable<YFCElement> shipmentLineList = shipmentLinesInList.getChildren();
        for (YFCElement shipmentLine : shipmentLineList) {

          if (YFCUtils.equals(shipmentLineKey, shipmentLine.getAttribute(GCConstants.SHIPMENT_LINE_KEY))) {
            shipmentLineEle.setAttribute(GCConstants.ACTION, GCConstants.ACTION_CANCEL_SHIPMENT);
            shipmentLineEle.setAttribute(GCConstants.SHIPMENT_LINE_KEY, shipmentLineKey);
            shipmentLineEle.setAttribute(GCConstants.ORDER_HEADER_KEY,
                shipmentLine.getAttribute(GCConstants.ORDER_HEADER_KEY));
            shipmentLineEle.setAttribute(GCConstants.ORDER_LINE_KEY,
                shipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY));
          }

        }

      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("changeShipmentAPI inDoc :" + inDoc.toString());
    }
    YFCDocument changeShipOutdoc =
        GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_SHIPMENT, inDoc,
            YFCDocument.getDocumentFor(GCConstants.CHANGE_SHIP_TEMP));
    LOGGER.verbose("GCCancelShipment.callChangeShipmentAPI -- END");
    LOGGER.endTimer("GCCancelShipment.callChangeShipmentAPI");
    return changeShipOutdoc;

  }

  /**
   * The method calls changeOrderStatus API to move the order to Sent To Fulfillment Center status
   * before canceling the ShipmentLine to account for proper inventory changes.
   * @param env
   * @param shipmentInList
   * @param orderLineKeyMap
   */
  private void callChangeOrderStatusAPI(YFSEnvironment env, YFCElement shipmentInList,
      Map<String, String> orderLineKeyMap) {

    LOGGER.beginTimer("GCCancelShipment.callChangeOrderStatusAPI");
    LOGGER.verbose("GCCancelShipment.callChangeOrderStatusAPI -- Begin");
    YFCDocument inDoc = YFCDocument.createDocument(GCConstants.ORDER_STATUS_CHANGE);
    YFCElement orderStatusChange = inDoc.getDocumentElement();
    orderStatusChange.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.GC_READY_FOR_BACKROOM_PICKUP);
    orderStatusChange.setAttribute(GCConstants.ORDER_HEADER_KEY, shipmentInList.getAttribute(GCConstants.ORDER_HEADER_KEY));
    orderStatusChange.setAttribute(GCConstants.CHANGE_FOR_ALL_AVAIL_QTY, GCConstants.NO);

    YFCElement orderLinesEle = orderStatusChange.createChild(GCConstants.ORDER_LINES);
    for (Entry<String, String> orderLineKeyList : orderLineKeyMap.entrySet()) {
      String orderLineKey = orderLineKeyList.getKey();
      String quantity = orderLineKeyList.getValue();
      YFCElement orderLineEle = orderLinesEle.createChild(GCConstants.ORDER_LINE);
      YFCElement shipmentLinesInList = shipmentInList.getChildElement(GCConstants.SHIPMENT_LINES);
      YFCIterable<YFCElement> shipmentLineList = shipmentLinesInList.getChildren();
      for (YFCElement shipmentLine : shipmentLineList) {

        if (YFCUtils.equals(orderLineKey, shipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY))) {
          orderLineEle.setAttribute(GCConstants.ORDER_RELEASE_KEY,
              shipmentLine.getAttribute(GCConstants.ORDER_RELEASE_KEY));
          orderLineEle.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey);
          orderLineEle.setAttribute(GCConstants.QUANTITY, quantity);
          orderLineEle.setAttribute(GCConstants.BASE_DROP_STATUS, GCConstants.SENT_TO_FULFILMENT_CENTER);
        }

      }

    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("changeOrderStatus Indoc :" + inDoc.toString());
    }
    Document outDoc = GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER_STATUS, inDoc.getDocument());
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("changeOrderStatus output :" + YFCDocument.getDocumentFor(outDoc).toString());
    }
    LOGGER.verbose("GCCancelShipment.callChangeOrderStatusAPI -- END");
    LOGGER.endTimer("GCCancelShipment.callChangeOrderStatusAPI");
  }


  /**
   * The method fetches the warranty association details for the input ShipmentLine and adds this to
   * the ShipmentLineKey map and OrderLineKey map.
   *
   * @param shipmentLinesInList
   * @param orderLineKey
   * @param shipLineKeyMap
   * @param orderLineKeyMap
   */
  private void checkWarrantyAssociation(YFCElement shipmentLinesInList,
      String orderLineKey, Map<String, String> shipLineKeyMap,
      Map<String, String> orderLineKeyMap) {

    LOGGER.beginTimer("GCCancelShipment.checkWarrantyAssociation");
    LOGGER.verbose("GCCancelShipment.checkWarrantyAssociation -- Begin");
    YFCIterable<YFCElement> shipmentLineList = shipmentLinesInList.getChildren();
    for(YFCElement shipmentLine : shipmentLineList){

      String compShipLineKey = shipmentLine.getAttribute(GCConstants.SHIPMENT_LINE_KEY);
      YFCElement compOrderLine = shipmentLine.getChildElement(GCConstants.ORDER_LINE);
      YFCElement extnInOrderLine = compOrderLine.getChildElement(GCConstants.EXTN);
      String extnIsWarrantyItem = extnInOrderLine.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
      String compOrderLineKey = compOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      String dependentOnLineKey = compOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);

      if(YFCUtils.equals(orderLineKey, dependentOnLineKey) && YFCUtils.equals(GCConstants.YES, extnIsWarrantyItem)){
        LOGGER.verbose("Warranty associated with regular item :" + compOrderLine.getAttribute(GCConstants.ITEM_ID));
        shipLineKeyMap.put(compShipLineKey, shipmentLine.getAttribute(GCConstants.QUANTITY));
        orderLineKeyMap.put(compOrderLineKey, shipmentLine.getAttribute(GCConstants.QUANTITY));
      }

    }
    LOGGER.verbose("Updated Map with warranty details :" + shipLineKeyMap.toString());
    LOGGER.verbose(orderLineKeyMap.toString());
    LOGGER.verbose("GCCancelShipment.checkWarrantyAssociation -- END");
    LOGGER.endTimer("GCCancelShipment.checkWarrantyAssociation");


  }

  /**
   * The method adds the bundle component and warranty associations details to the ShipmentLineKey
   * map and OrderLineKey map.
   *
   * @param env
   *
   * @param shipmentLinesInList
   * @param shipmentLineKey
   * @param orderLineKey
   * @param shipLineKeyMap
   * @param orderLineKeyMap
   * @param bundleCompList
   * @param eleItems
   * @param shipNode
   */
  private void addBundleComponentstoMap(YFSEnvironment env, YFCElement shipmentLinesInList, String shipmentLineKey,
      String orderLineKey, Map<String, String> shipLineKeyMap,
      Map<String, String> orderLineKeyMap, List<String> bundleCompList,
      YFCElement eleItems, String shipNode) {

    LOGGER.beginTimer("GCCancelShipment.addBundleComponentstoMap");
    LOGGER.verbose("GCCancelShipment.addBundleComponentstoMap -- Begin");
    YFCIterable<YFCElement> shipmentLineList = shipmentLinesInList.getChildren();
    for(YFCElement shipmentLine : shipmentLineList){

      YFCElement extnShipmentLine = shipmentLine.getChildElement(GCConstants.EXTN);
      String compShipLineKey = shipmentLine.getAttribute(GCConstants.SHIPMENT_LINE_KEY);
      String parentShipLineKey = shipmentLine.getAttribute(GCConstants.PARENT_SHPMNTLINE_KEY);
      YFCElement compOrderLine = shipmentLine.getChildElement(GCConstants.ORDER_LINE);
      YFCElement extnInOrderLine = compOrderLine.getChildElement(GCConstants.EXTN);
      String extnIsWarrantyItem = extnInOrderLine.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
      String compOrderLineKey = compOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      String dependentOnLineKey = compOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
      if(YFCUtils.equals(shipmentLineKey, parentShipLineKey)){
        LOGGER.verbose("Is a bundle component :" + compOrderLine.getAttribute(GCConstants.ITEM_ID));
        shipLineKeyMap.put(compShipLineKey, shipmentLine.getAttribute(GCConstants.QUANTITY));
        orderLineKeyMap.put(compOrderLineKey, shipmentLine.getAttribute(GCConstants.QUANTITY));
        bundleCompList.add(compShipLineKey);

        // logic for AdjustInventory call
        final String actualSerialNo = extnShipmentLine.getAttribute(GCConstants.EXTN_ACTUAL_SERIAL_NO);
        boolean isSerialAvailable = true;
        if (!YFCCommon.isVoid(actualSerialNo)) {
          isSerialAvailable = checkActualSrialNo(env, shipmentLine, shipNode, actualSerialNo);
        }
        String requestedTagNo = shipmentLine.getAttribute(GCConstants.REQ_TAG_NO);
        if (!YFCCommon.isVoid(actualSerialNo) && !YFCUtils.equals(actualSerialNo, requestedTagNo)) {
          if (!(!isSerialAvailable && YFCCommon.isVoid(requestedTagNo))) {
            stampAttrForAdjInv(eleItems, shipmentLine, shipNode, isSerialAvailable);
          }
        }

      } else if(YFCUtils.equals(orderLineKey, dependentOnLineKey) && YFCUtils.equals(GCConstants.YES, extnIsWarrantyItem)){
        LOGGER.verbose("Is a warranty child :" + compOrderLine.getAttribute(GCConstants.ITEM_ID));
        shipLineKeyMap.put(compShipLineKey, shipmentLine.getAttribute(GCConstants.QUANTITY));
        orderLineKeyMap.put(compOrderLineKey, shipmentLine.getAttribute(GCConstants.QUANTITY));
      }

      LOGGER.verbose("Updated map with bundle component and warranty associations :" + shipLineKeyMap.toString());
      LOGGER.verbose(orderLineKeyMap.toString());
      LOGGER.verbose("GCCancelShipment.addBundleComponentstoMap -- END");
      LOGGER.endTimer("GCCancelShipment.addBundleComponentstoMap");
    }

  }

  /**
   * The method calls getShipmentList API with the input.
   * @param env
   * @param shipmentKey
   * @param getShpListIndoc
   * @return
   */
  public static YFCDocument callGetShipmentList(YFSEnvironment env, String shipmentKey, YFCDocument getShpListIndoc) {

    LOGGER.beginTimer("GCCancelShipment.callGetShipmentList");
    LOGGER.verbose("GCCancelShipment.callGetShipmentList -- Begin");
    YFCElement shpmntEle = getShpListIndoc.getDocumentElement();
    shpmntEle.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getShipmentList Indoc :" + getShpListIndoc.toString());
    }
    YFCDocument getShpListTemp = YFCDocument.getDocumentFor(GCConstants.GET_SHPMNT_LIST_TEMP_CANCEL);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Template :" + getShpListTemp.toString());
    }
    YFCDocument getShipmentList = GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, getShpListIndoc, getShpListTemp);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getShipmentDetails Output :" + getShipmentList.toString());
    }
    LOGGER.verbose("GCCancelShipment.callGetShipmentList -- END");
    LOGGER.endTimer("GCCancelShipment.callGetShipmentList");
    return getShipmentList;
  }

  /**
   * This method is used to stamp mandatory attributes for adjustInventory call
   *
   * @param eleItems
   * @param eleShipLine
   * @param shipNode
   * @param isSerialAvailable
   */
  private void stampAttrForAdjInv(YFCElement eleItems, YFCElement eleShipLine, String shipNode,
      boolean isSerialAvailable) {

    final YFCElement eleItem = eleItems.createChild(GCConstants.ITEM);
    final YFCElement eleTag = eleItem.createChild(GCConstants.TAG);

    YFCElement extnShipLine = eleShipLine.getChildElement(GCConstants.EXTN);
    String actualSerialNo = extnShipLine.getAttribute(GCConstants.EXTN_ACTUAL_SERIAL_NO);
    if (!isSerialAvailable) {
      actualSerialNo = "";
    }
    eleTag.setAttribute(GCConstants.BATCH_NO, actualSerialNo);
    eleItem.setAttribute(GCConstants.ITEM_ID, eleShipLine.getAttribute(GCConstants.ITEM_ID));
    eleItem.setAttribute(GCConstants.PRODUCT_CLASS, eleShipLine.getAttribute(GCConstants.PRODUCT_CLASS));
    eleItem.setAttribute(GCConstants.UOM, eleShipLine.getAttribute(GCConstants.UOM));
    eleItem.setAttribute(GCConstants.QUANTITY, "+" + eleShipLine.getAttribute(GCConstants.QUANTITY));

    eleItem.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GCI);
    eleItem.setAttribute(GCConstants.ADJUSTMENT_TYPE, GCConstants.ADJUSTMENT);
    eleItem.setAttribute(GCConstants.SUPPLY_TYPE, GCConstants.ONHAND);
    eleItem.setAttribute(GCConstants.SHIP_NODE, shipNode);

    final YFCElement eleItem1 = eleItems.createChild(GCConstants.ITEM);
    final YFCElement eleTag1 = eleItem1.createChild(GCConstants.TAG);
    eleTag1.setAttribute(GCConstants.BATCH_NO, eleShipLine.getAttribute(GCConstants.REQ_TAG_NO));
    eleItem1.setAttribute(GCConstants.ITEM_ID, eleShipLine.getAttribute(GCConstants.ITEM_ID));
    eleItem1.setAttribute(GCConstants.PRODUCT_CLASS, eleShipLine.getAttribute(GCConstants.PRODUCT_CLASS));
    eleItem1.setAttribute(GCConstants.UOM, eleShipLine.getAttribute(GCConstants.UOM));
    eleItem1.setAttribute(GCConstants.QUANTITY, "-" + eleShipLine.getAttribute(GCConstants.QUANTITY));

    eleItem1.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GCI);
    eleItem1.setAttribute(GCConstants.ADJUSTMENT_TYPE, GCConstants.ADJUSTMENT);
    eleItem1.setAttribute(GCConstants.SUPPLY_TYPE, GCConstants.ONHAND);
    eleItem1.setAttribute(GCConstants.SHIP_NODE, shipNode);

  }

  /**
   *
   */
  @Override
  public void setProperties(Properties arg0) {


  }

}
