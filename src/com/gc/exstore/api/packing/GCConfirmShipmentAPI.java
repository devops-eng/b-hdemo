package com.gc.exstore.api.packing;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCLogUtils;
import com.gc.exstore.utils.GCShipmentUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCConfirmShipmentAPI implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCConfirmShipmentAPI.class);

  /**
   *
   * @param env
   * @param inputDoc
   */
  public Document confirmShipment(YFSEnvironment env, Document inputDoc) {

    LOGGER.beginTimer("GCConfirmShipmentAPI.confirmShipment");
    LOGGER.verbose("GCConfirmShipmentAPI.confirmShipment -- Begin");

    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    YFCElement rootEle = inDoc.getDocumentElement();
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input to Confirm Ship class :" + inDoc.toString());
    }
    YFCDocument getShipListTemp = YFCDocument.getDocumentFor(GCConstants.GET_SHIP_LIST_TEMP);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getShipmentList template :" + getShipListTemp.toString());
    }
    YFCDocument getShipListOutdoc = GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, inDoc, getShipListTemp);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getShipmentList output :" + getShipListOutdoc.toString());
    }
    YFCElement shipmentsEle = getShipListOutdoc.getDocumentElement();
    YFCElement shipmentEle = shipmentsEle.getChildElement(GCConstants.SHIPMENT);
    YFCElement extninShipEle = shipmentEle.createChild(GCConstants.EXTN);
    YDate date = new YDate(false);
    if (!YFCUtils.equals(GCConstants.SHIP_2_CUSTOMER, rootEle.getAttribute(GCConstants.FULFILLMENT_TYPE))) {
      extninShipEle.setAttribute(GCConstants.EXTN_PENDING_RECEIPT_DATE, date.getString(GCConstants.DATE_TIME_FORMAT));
    }

    YFCElement containersEle = shipmentEle.getChildElement(GCConstants.CONTAINERS);
    YFCIterable<YFCElement> containerList = containersEle.getChildren();

    for (YFCElement containerEle : containerList) {

      YFCElement extnEle = containerEle.createChild(GCConstants.EXTN);
      if (YFCUtils.equals(GCConstants.SHIP_2_CUSTOMER, rootEle.getAttribute(GCConstants.FULFILLMENT_TYPE))) {
        extnEle.setAttribute(GCConstants.EXTN_CONTAINER_STATUS, GCConstants.SHIPPED_TO_CUST);
      } else {
        extnEle.setAttribute(GCConstants.EXTN_CONTAINER_STATUS, GCConstants.PENDING_RECEIPT);
      }

    }

    YFCDocument confirmShipIndoc = YFCDocument.getDocumentFor(shipmentEle.toString());
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("confirmPick Indoc :" + confirmShipIndoc.toString());
    }
    GCCommonUtil.invokeAPI(env, GCConstants.CONFIRM_SHIPMENT_API, confirmShipIndoc.getDocument());

    callCreateShipInvoiceAPI(env, shipmentEle);

    YFCDocument changeShipStatusIndoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement shipment = changeShipStatusIndoc.getDocumentElement();
    shipment.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.SHIP_STATUS_UPDATE);
    shipment.setAttribute(GCConstants.SHIPMENT_KEY, rootEle.getAttribute(GCConstants.SHIPMENT_KEY));

    if (YFCUtils.equals(GCConstants.SHIP_2_CUSTOMER, rootEle.getAttribute(GCConstants.FULFILLMENT_TYPE))) {
      shipment.setAttribute(GCConstants.BASE_DROP_STATUS, GCConstants.SHIPPED_TO_CUSTOMER_STATUS);
    } else {
      shipment.setAttribute(GCConstants.BASE_DROP_STATUS, GCConstants.PENDING_RECEIPT_STATUS);
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("changeShipmentStatus Indoc :" + changeShipStatusIndoc.toString());
    }
    GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_SHIPMENT_STATUS, changeShipStatusIndoc.getDocument());

    //To stamp shipmentcontainerkey in GCUserActivity table
    YFCElement container = containersEle.getChildElement(GCConstants.CONTAINER);
    String shipContainerKey = container.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
    rootEle.setAttribute(GCConstants.CONTAINER_KEY, shipContainerKey);
    //End

    GCCancelShipment.updateUserActivityTable(env, rootEle, GCConstants.PACK_CONFIRM, GCConstants.SHIP);
    updateWarrantyLines(env, shipmentEle);
    // Calling pdf Service
    Document finalPdfDoc = GCCommonUtil.invokeService(env, GCConstants.PACK_SLIP_SERVICE, inputDoc);
    LOGGER.verbose("GCConfirmShipmentAPI.confirmShipment -- END");
    LOGGER.endTimer("GCConfirmShipmentAPI.confirmShipment");

    return finalPdfDoc;
  }

  /**
   *
   * @param env
   * @param shipmentEle
   */
  public void callCreateShipInvoiceAPI(YFSEnvironment env, YFCElement shipmentEle) {
    YFCDocument createShipInvoiceIndoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement shipInvoiceEle = createShipInvoiceIndoc.getDocumentElement();
    shipInvoiceEle.setAttribute(GCConstants.SELLER_ORGANIZATION_CODE,
        shipmentEle.getAttribute(GCConstants.SELLER_ORGANIZATION_CODE));
    shipInvoiceEle.setAttribute(GCConstants.SHIP_NODE, shipmentEle.getAttribute(GCConstants.SHIP_NODE));
    shipInvoiceEle.setAttribute(GCConstants.SHIPMENT_KEY, shipmentEle.getAttribute(GCConstants.SHIPMENT_KEY));
    shipInvoiceEle.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.CREATE_SHPMNT_INVC);

    if(LOGGER.isVerboseEnabled()){
      LOGGER.verbose("createShipInvoice Indoc :" +createShipInvoiceIndoc.toString());
    }

    GCCommonUtil.invokeAPI(env, GCConstants.CREATE_SHPMNT_INVC_API, createShipInvoiceIndoc.getDocument());
  }
  /**
   *
   */

  private void updateWarrantyLines(YFSEnvironment env , YFCElement rootEle) {
    YFCElement eleShipmentLines = rootEle.getChildElement(GCConstants.SHIPMENT_LINES);
    YFCNodeList<YFCElement> nlShipmentLine = eleShipmentLines.getElementsByTagName(GCConstants.SHIPMENT_LINE);
    YFCElement eleShipmentLine = nlShipmentLine.item(0);
    String orderHeaderKey = eleShipmentLine.getAttribute(GCConstants.ORDER_HEADER_KEY);
    YFCDocument orderListOpTemp = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_WARR_TEMP);
    YFCDocument orderListOp = GCCommonUtil.invokeGetOrderListAPI(env, orderHeaderKey, orderListOpTemp);
    YFCElement ordEle = orderListOp.getDocumentElement();
    YFCElement eleOrder = ordEle.getChildElement(GCConstants.ORDER);


    List<String> orderLineKeyList = GCShipmentUtil.getOrderLineKeyList(nlShipmentLine);
    YFCElement eleOrderLines = eleOrder.getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> nlOrderLine = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
    List<String> shipmentKeyList = new ArrayList<String>();
    for (YFCElement eleOrderLine : nlOrderLine) {
      YFCElement eleExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
      String isWarranty = eleExtn.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
      String ordLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      String parentLineKey = eleOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
      if (YFCUtils.equals(isWarranty, GCConstants.YES) && orderLineKeyList.contains(parentLineKey)
          && !orderLineKeyList.contains(ordLineKey)) {
        YFCDocument getShipmentListIp = YFCDocument.createDocument(GCConstants.SHIPMENT);
        YFCElement eleIp = getShipmentListIp.getDocumentElement();
        YFCElement eleShipmentLinesIp = eleIp.createChild(GCConstants.SHIPMENT_LINES);
        YFCElement eleShipmentLineIp = eleShipmentLinesIp.createChild(GCConstants.SHIPMENT_LINE);
        eleShipmentLineIp.setAttribute(GCConstants.ORDER_LINE_KEY, ordLineKey);
        YFCDocument opTemp = YFCDocument.getDocumentFor(GCConstants.GET_SHIPMENT_LIST_STANDALONE_WARRANTY);
        GCLogUtils.verboseLog(LOGGER, "getShipmentList Input", getShipmentListIp.toString());
        // Invoke getShipmentList API
        YFCDocument opDoc = GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, getShipmentListIp, opTemp);
        GCLogUtils.verboseLog(LOGGER, "getShipmentList Output", opDoc.toString());
        if (!YFCCommon.isVoid(opDoc)) {
          YFCElement eleRootShip = opDoc.getDocumentElement();
          YFCNodeList<YFCElement> nlShipment = eleRootShip.getElementsByTagName(GCConstants.SHIPMENT);
          for (YFCElement eleShipmentOp : nlShipment) {
            String shipmentKeyWarr = eleShipmentOp.getAttribute(GCConstants.SHIPMENT_KEY);
            String shipNode = eleShipmentOp.getAttribute(GCConstants.SHIP_NODE);
            YFCElement eleShipmentLinesOp = eleShipmentOp.getChildElement(GCConstants.SHIPMENT_LINES);
            String status = eleShipmentOp.getAttribute(GCConstants.STATUS);
            YFCDocument confirmShipmentDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
            YFCElement eleShipmentIp = confirmShipmentDoc.getDocumentElement();
            eleShipmentIp.setAttribute(GCConstants.DOCUMENT_TYPE, GCConstants.ZERO_ZERO_ZERO_ONE);
            eleShipmentIp.setAttribute(GCConstants.ENTERPRISE_CODE, GCConstants.GC);
            eleShipmentIp.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKeyWarr);
            eleShipmentIp.setAttribute(GCConstants.SHIP_NODE, shipNode);
            eleShipmentIp.importNode(eleShipmentLinesOp);
            YFCDocument opTempConfShp = YFCDocument.getDocumentFor(GCConstants.CONFIRM_SHIP_TEMP);
            if ((shipmentKeyList.isEmpty() || !shipmentKeyList.contains(shipmentKeyWarr))
                && !YFCUtils.equals(status, GCConstants.CANCELLED_ORDER_STATUS_CODE)) {
              YFCDocument confirmShpOp =
                  GCCommonUtil.invokeAPI(env, GCConstants.CONFIRM_SHIPMENT_API, confirmShipmentDoc, opTempConfShp);
              YFCElement eleShpInvoiceIp = confirmShpOp.getDocumentElement();
              eleShpInvoiceIp.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.CREATE_SHPMNT_INVC);
              YFCDocument createShpInvpiceOpTemp = YFCDocument.createDocument(GCConstants.API_SUCCESS);
              GCCommonUtil.invokeAPI(env, GCConstants.CREATE_SHPMNT_INVC_API, confirmShpOp, createShpInvpiceOpTemp);
              shipmentKeyList.add(shipmentKeyWarr);
            }
          }
        }
      }
    }


  }

  @Override
  public void setProperties(Properties arg0) {


  }



}
