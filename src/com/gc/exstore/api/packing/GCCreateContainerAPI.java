package com.gc.exstore.api.packing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
/**
 * This class is used to create container by calling changeShipment API and moves the shipment status to Shipment Packed status or Shipment Being Packed status
 * @author shanil.sharma
 *
 */
public class GCCreateContainerAPI {

  //initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCCreateContainerAPI.class.getName());

  //preparing 2 array list to store ShipmentLineKeys of all warrantyLines & bundleComponents
  final List<String> warrantyLineKeyList = new ArrayList<String>();
  final List<String> compLineKeyList = new ArrayList<String>();

  /**
   * This is the main method which is used to create container by calling getShipmentList and
   * perform logic for normal shipments, Shipments with warraty lines & multibox shipments and calls
   * ChangeShipmentStatus on the basis of quantities packed.
   *
   * @param env
   * @param inputDoc
   * @return
   */
  public Document createContainer(YFSEnvironment env, Document inputDoc){
    LOGGER.beginTimer("GCCreateContainerAPI.createContainer");
    LOGGER.verbose("GCCreateContainerAPI.createContainer--Begin");
    final YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("inDoc : " + inDoc.toString());
    }

    final YFCElement eleShipment = inDoc.getDocumentElement();

    // Shipback to DC
    String isShipBackToDC = eleShipment.getAttribute(GCConstants.IS_SHIP_BACK_TO_DC);
    // End

    final String shipmentKey = eleShipment.getAttribute(GCConstants.SHIPMENT_KEY);
    YFCDocument getshipmentOutDoc = callGetShipmentList(env, shipmentKey);

    Map<String, String> lineKeyQtyMap = prepareQtyMap(getshipmentOutDoc);

    //This map stores ShipmentLineKey as key & Quantity as value
    Map<String, String> inputLineKeyQtyMap = new HashMap<String, String>();

    //Preparing document for changeShipment call
    YFCDocument changeShipmentDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    final YFCElement eleShipmentIndoc = changeShipmentDoc.getDocumentElement();
    eleShipmentIndoc.setAttribute(GCConstants.ACTION, GCConstants.ACTION_MODIFY);
    eleShipmentIndoc.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
    final YFCElement eleContainersIndoc = eleShipmentIndoc.createChild(GCConstants.CONTAINERS);

    final YFCElement eleContainers = eleShipment.getChildElement(GCConstants.CONTAINERS);
    final YFCElement eleContainer = eleContainers.getChildElement(GCConstants.CONTAINER);
    final YFCElement eleContainerDetails = eleContainer.getChildElement(GCConstants.CONTAINER_DETAILS);
    final YFCNodeList<YFCElement> nlContainerDetail = eleContainerDetails.getElementsByTagName(GCConstants.CONTAINER_DETAIL);
    for(YFCElement eleContainerDtl : nlContainerDetail){
      final String inShipLineKey = eleContainerDtl.getAttribute(GCConstants.SHIPMENT_LINE_KEY);
      final String inOdrLineKey = eleContainerDtl.getAttribute(GCConstants.ORDER_LINE_KEY);
      final String inKitCode = eleContainerDtl.getAttribute(GCConstants.KIT_CODE);
      final int inQuantity = Float.valueOf(eleContainerDtl.getAttribute(GCConstants.QUANTITY)).intValue();

      if(YFCUtils.equals(GCConstants.BUNDLE, inKitCode)){
        LOGGER.verbose("Its bundle");

        //Check for Bundle & Warranty
        handleComponents(getshipmentOutDoc, inputLineKeyQtyMap, inShipLineKey, inQuantity, inOdrLineKey, lineKeyQtyMap);

      } else {
        inputLineKeyQtyMap.put(inShipLineKey, eleContainerDtl.getAttribute(GCConstants.QUANTITY));
        LOGGER.verbose("InputLineKey QtyMap :" + inputLineKeyQtyMap);
        handleComponents(getshipmentOutDoc, inputLineKeyQtyMap, inShipLineKey, inQuantity, inOdrLineKey, lineKeyQtyMap);
      }

    }

    //Fetching TrackingNo
    final String trackingNo = eleContainer.getAttribute(GCConstants.TRACKING_NO).toUpperCase();
    String[] trackingNoList = trackingNo.split(",");
    final int noOfTrackingNo = trackingNoList.length;
    LOGGER.verbose("noOfTrackingNo :" + noOfTrackingNo);

    if(noOfTrackingNo == 1){
      final YFCElement eleContainerIndoc = eleContainersIndoc.createChild(GCConstants.CONTAINER);
      eleContainerIndoc.setAttribute(GCConstants.TRACKING_NO, trackingNo);
      final YFCElement eleContainerExtn = eleContainerIndoc.createChild(GCConstants.EXTN);
      eleContainerExtn.setAttribute(GCConstants.EXTN_CONTAINER_STATUS, GCConstants.CONTAINER_PACKED);
      prepareChangeShipDoc(eleContainerIndoc, inputLineKeyQtyMap);
    } else {
      LOGGER.verbose("Its multibox");
      handleMultiBox(eleContainersIndoc, inputLineKeyQtyMap, trackingNoList, noOfTrackingNo);

    }
    // Call changeShipment API
    YFCDocument tempdoc = YFCDocument.getDocumentFor(GCConstants.CHANGE_SHIP_TEMP);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("changeShipment InDoc : " + changeShipmentDoc.toString());
    }
    YFCDocument changeShipOutDoc = GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_SHIPMENT, changeShipmentDoc, tempdoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("changeShip OutDoc : " + changeShipOutDoc.toString());
    }
    final YFCElement eleShipmentOutDoc = changeShipOutDoc.getDocumentElement();
    final String containerizedQTy = eleShipmentOutDoc.getAttribute(GCConstants.CONTAINERIZED_QUANTITY);
    LOGGER.verbose("Containerized Qty :" + containerizedQTy);
    final String totalQTy = eleShipmentOutDoc.getAttribute(GCConstants.TOTAL_QUANTITY);
    LOGGER.verbose("Total Quantity :" + totalQTy);
    if(YFCUtils.equals(containerizedQTy, totalQTy)){
      if (YFCUtils.equals(GCConstants.YES, isShipBackToDC)) {
        callChangeShipmentStatusApi(env, shipmentKey, GCConstants.SHP_PACKED_STATUS_TRNSFR_ORD,
            GCConstants.TRNSFR_ORD_SHIP_STATUS_UPDATE);
      } else {
        callChangeShipmentStatusApi(env, shipmentKey, GCConstants.SHP_PACKED_STATUS,
            GCConstants.PCK_SHP_COMPLETE_TRNSCTN);
      }

    } else {
      if (YFCUtils.equals(GCConstants.YES, isShipBackToDC)) {
        callChangeShipmentStatusApi(env, shipmentKey, GCConstants.SHP_BEING_PACKED_STATUS_TRNSFR_ORD,
            GCConstants.TRNSFR_ORD_SHIP_STATUS_UPDATE);
      } else {
        callChangeShipmentStatusApi(env, shipmentKey, GCConstants.SHP_BEING_PACKED_STATUS,
            GCConstants.PCK_SHP_COMPLETE_TRNSCTN);
      }
    }

    //Invoke GCPackDetailsShipListManager Service
    YFCDocument serviceDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement eleShpServiceDoc = serviceDoc.getDocumentElement();
    final YFCElement eleShipList = getshipmentOutDoc.getDocumentElement();
    final YFCElement eleShipmentOut = eleShipList.getChildElement(GCConstants.SHIPMENT);
    eleShpServiceDoc.setAttribute(GCConstants.SHIP_NODE, eleShipmentOut.getAttribute(GCConstants.SHIP_NODE));
    eleShpServiceDoc.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);

    if (YFCUtils.equals(GCConstants.YES, isShipBackToDC)) {
      eleShpServiceDoc.setAttribute(GCConstants.IS_SHIP_BACK_TO_DC, GCConstants.FLAG_Y);
    }
    final YFCDocument outputDoc = GCCommonUtil.invokeService(env, GCConstants.GC_PACK_DETAIL_SHPLIST, serviceDoc);

    LOGGER.verbose("GCCreateContainerAPI.createContainer--End");
    LOGGER.endTimer("GCCreateContainerAPI.createContainer");
    return outputDoc.getDocument();

  }

  /**
   * This method is used to create containers in multiBox scenario.
   * @param eleContainersIndoc
   * @param inputLineKeyQtyMap
   * @param trackingNoList
   * @param noOfTrackingNo
   */
  private void handleMultiBox(YFCElement eleContainersIndoc, Map<String, String> inputLineKeyQtyMap,
      String[] trackingNoList, int noOfTrackingNo) {

    LOGGER.beginTimer("GCCreateContainerAPI.handleMultiBox");
    LOGGER.verbose("GCCreateContainerAPI.handleMultiBox--Begin");
    final YFCElement eleContainerIndocRoot = eleContainersIndoc.createChild(GCConstants.CONTAINER);
    eleContainerIndocRoot.setAttribute(GCConstants.TRACKING_NO, trackingNoList[0]);
    final YFCElement eleContainerExtnRoot = eleContainerIndocRoot.createChild(GCConstants.EXTN);
    eleContainerExtnRoot.setAttribute(GCConstants.EXTN_CONTAINER_STATUS, GCConstants.CONTAINER_PACKED);
    eleContainerExtnRoot.setAttribute(GCConstants.EXTN_MULTIBOX_GRP_ID, trackingNoList[0]);
    eleContainerExtnRoot.setAttribute(GCConstants.EXTN_MULTIBOX_DESC, "Box "+ 1 + " of "+noOfTrackingNo);
    final YFCElement eleContainerDetailsRoot = eleContainerIndocRoot.createChild(GCConstants.CONTAINER_DETAILS);
    Set<String> inputShipLineKeyList = inputLineKeyQtyMap.keySet();
    for(String inputShipLineKey : inputShipLineKeyList){
      final YFCElement eleContainerDetail = eleContainerDetailsRoot.createChild(GCConstants.CONTAINER_DETAIL);
      eleContainerDetail.setAttribute(GCConstants.SHIPMENT_LINE_KEY, inputShipLineKey);
      eleContainerDetail.setAttribute(GCConstants.QUANTITY, "1");

    }
    LOGGER.verbose("Container Details appended :" + eleContainersIndoc.toString());
    for(int i = 1; i < noOfTrackingNo; i++){

      LOGGER.verbose("inside for--diff tracking no");
      for(String inputShipLineKey : inputShipLineKeyList){
        if(!warrantyLineKeyList.contains(inputShipLineKey)){
          LOGGER.verbose("inside if--warranty");
          final YFCElement eleContainerIndoc = eleContainersIndoc.createChild(GCConstants.CONTAINER);
          eleContainerIndoc.setAttribute(GCConstants.TRACKING_NO, trackingNoList[i]);
          final YFCElement eleContainerExtn = eleContainerIndoc.createChild(GCConstants.EXTN);
          eleContainerExtn.setAttribute(GCConstants.EXTN_CONTAINER_STATUS, GCConstants.CONTAINER_PACKED);
          eleContainerExtn.setAttribute(GCConstants.EXTN_MULTIBOX_GRP_ID, trackingNoList[0]);
          int j = i + 1;
          eleContainerExtn.setAttribute(GCConstants.EXTN_MULTIBOX_DESC, "Box " + j + " of " + noOfTrackingNo);
          final YFCElement eleContainerDetails = eleContainerIndoc.createChild(GCConstants.CONTAINER_DETAILS);
          final YFCElement eleContainerDetail = eleContainerDetails.createChild(GCConstants.CONTAINER_DETAIL);
          eleContainerDetail.setAttribute(GCConstants.SHIPMENT_LINE_KEY, inputShipLineKey);
          eleContainerDetail.setAttribute(GCConstants.QUANTITY, "0");
        }
      }

    }
    LOGGER.verbose("finalContainer Details appended :" + eleContainersIndoc.toString());
    LOGGER.verbose("GCCreateContainerAPI.handleMultiBox--End");
    LOGGER.endTimer("GCCreateContainerAPI.handleMultiBox");
  }
  /**
   * This method is used to prepare changeShipment Input Document
   * @param eleContainersIndoc
   * @param inputLineKeyQtyMap
   * @param trackingNo
   */
  private void prepareChangeShipDoc(YFCElement eleContainerIndoc, Map<String, String> inputLineKeyQtyMap) {
    LOGGER.beginTimer("GCCreateContainerAPI.prepareChangeShipDoc");
    LOGGER.verbose("GCCreateContainerAPI.prepareChangeShipDoc--Begin");
    final YFCElement eleContainerDetails = eleContainerIndoc.createChild(GCConstants.CONTAINER_DETAILS);
    for (Entry<String, String> inputShipLineEntrySet : inputLineKeyQtyMap.entrySet()) {
      final YFCElement eleContainerDetail = eleContainerDetails.createChild(GCConstants.CONTAINER_DETAIL);
      eleContainerDetail.setAttribute(GCConstants.SHIPMENT_LINE_KEY, inputShipLineEntrySet.getKey());
      eleContainerDetail.setAttribute(GCConstants.QUANTITY, inputShipLineEntrySet.getValue());
    }
    LOGGER.verbose("Container element formed :" + eleContainerIndoc.toString());
    LOGGER.verbose("GCCreateContainerAPI.prepareChangeShipDoc--End");
    LOGGER.endTimer("GCCreateContainerAPI.prepareChangeShipDoc");
  }
  /**
   * This method is used to calculate quantity for bundle components & warranty components and put them into inputLineKeyQtyMap map
   * @param getshipmentOutDoc
   * @param inputLineKeyQtyMap
   * @param inShipLineKey
   * @param inQuantity
   * @param inOdrLineKey
   * @param lineKeyQtyMap
   */
  private void handleComponents(YFCDocument getshipmentOutDoc, Map<String, String> inputLineKeyQtyMap, String inShipLineKey, int inQuantity, String inOdrLineKey, Map<String, String> lineKeyQtyMap) {

    LOGGER.beginTimer("GCCreateContainerAPI.handleComponents");
    LOGGER.verbose("GCCreateContainerAPI.handleComponents--Begin");
    LOGGER.verbose("lineKeyQtyMap :" + lineKeyQtyMap);
    final YFCElement eleShipments = getshipmentOutDoc.getDocumentElement();
    final YFCElement eleShipment = eleShipments.getChildElement(GCConstants.SHIPMENT);
    final YFCElement eleShipLines = eleShipment.getChildElement(GCConstants.SHIPMENT_LINES);
    final YFCNodeList<YFCElement> nlShipLine = eleShipLines.getElementsByTagName(GCConstants.SHIPMENT_LINE);
    int parentQty = 0;
    for(YFCElement eleShipmentLine : nlShipLine){
      if(YFCUtils.equals(inShipLineKey, eleShipmentLine.getAttribute(GCConstants.SHIPMENT_LINE_KEY))){
        parentQty = Float.valueOf(eleShipmentLine.getAttribute(GCConstants.QUANTITY)).intValue();
        break;
      }
    }
    for(YFCElement eleShipmentLine : nlShipLine){
      final String parentShipLineKey = eleShipmentLine.getAttribute(GCConstants.PARENT_SHPMNTLINE_KEY);
      final String shipLineKey = eleShipmentLine.getAttribute(GCConstants.SHIPMENT_LINE_KEY);
      if(YFCUtils.equals(inShipLineKey, parentShipLineKey) && parentQty!=0){
        final int compQty = Float.valueOf(eleShipmentLine.getAttribute(GCConstants.QUANTITY)).intValue();
        int updatedQty = (compQty/parentQty) * inQuantity;
        inputLineKeyQtyMap.put(shipLineKey, String.valueOf(updatedQty));
        compLineKeyList.add(shipLineKey);
        LOGGER.verbose("Component LineKey List :" + compLineKeyList.toString());
      }
      int packedQty = 0;
      final YFCElement  eleShpOdrLine = eleShipmentLine.getChildElement(GCConstants.ORDER_LINE);
      final String dependentLineKey = eleShpOdrLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
      final YFCElement eleShipExtnOdrLine = eleShpOdrLine.getChildElement(GCConstants.EXTN);
      if(YFCUtils.equals(inOdrLineKey, dependentLineKey) && YFCUtils.equals(GCConstants.FLAG_Y, eleShipExtnOdrLine.getAttribute(GCConstants.IS_WARRANTY))){
        if(!YFCCommon.isVoid(lineKeyQtyMap) && lineKeyQtyMap.containsKey(shipLineKey)){
          packedQty = Float.valueOf(lineKeyQtyMap.get(shipLineKey)).intValue();
        }
        final int originalQty = Float.valueOf(eleShipmentLine.getAttribute(GCConstants.QUANTITY)).intValue();
        int warrantyQty = calculateWarrantyQty(originalQty, inQuantity, packedQty);
        inputLineKeyQtyMap.put(shipLineKey, String.valueOf(warrantyQty));
        warrantyLineKeyList.add(shipLineKey);
        LOGGER.verbose("Warranty Line List :" + warrantyLineKeyList.toString());
      }
    }
    LOGGER.verbose("GCCreateContainerAPI.handleComponents--End");
    LOGGER.endTimer("GCCreateContainerAPI.handleComponents");
  }
  /**
   * This method is used to calculate warranty quantity.
   * @param originalQty
   * @param parentQty
   * @param packedQty
   * @return
   */
  private int calculateWarrantyQty(int originalQty, int parentQty, int packedQty) {

    final int diffQty = originalQty - packedQty;
    final int qtyToPack = parentQty - diffQty;
    if(qtyToPack >= 0){
      LOGGER.verbose("Warranty Qty :" + diffQty);
      return diffQty;
    } else{
      LOGGER.verbose("Warranty Qty :" + parentQty);
      return parentQty;
    }



  }
  /**
   * This method is used to prepare ShipmentLineKey & quantity map for already packed shipment lines.
   * @param getshipmentOutDoc
   * @return
   */
  private Map<String, String> prepareQtyMap(YFCDocument getshipmentOutDoc) {

    LOGGER.beginTimer("GCCreateContainerAPI.prepareQtyMap");
    LOGGER.verbose("GCCreateContainerAPI.prepareQtyMap--Begin");
    //This map stores ShipmentLineKey as key & Quantity as value
    Map<String, String> lineKeyQtyMap = new HashMap<String, String>();
    final YFCElement eleShipments = getshipmentOutDoc.getDocumentElement();
    final YFCElement eleShipment = eleShipments.getChildElement(GCConstants.SHIPMENT);
    final YFCElement eleContainers = eleShipment.getChildElement(GCConstants.CONTAINERS);
    final YFCNodeList<YFCElement> nlContainer = eleContainers.getElementsByTagName(GCConstants.CONTAINER);
    for(YFCElement eleContainer : nlContainer){
      final YFCElement eleContainerDetails = eleContainer.getChildElement(GCConstants.CONTAINER_DETAILS);
      final YFCNodeList<YFCElement> nlContainerDetail = eleContainerDetails.getElementsByTagName(GCConstants.CONTAINER_DETAIL);
      for(YFCElement eleContainerDtl : nlContainerDetail){
        final String shipmentLineKey = eleContainerDtl.getAttribute(GCConstants.SHIPMENT_LINE_KEY);
        float quantity = Float.parseFloat(eleContainerDtl.getAttribute(GCConstants.QUANTITY));
        if(lineKeyQtyMap.containsKey(shipmentLineKey)){
          quantity = quantity + Float.parseFloat(lineKeyQtyMap.get(shipmentLineKey));
        }
        lineKeyQtyMap.put(shipmentLineKey, String.valueOf(quantity));
      }
    }
    LOGGER.verbose("*******lineKeyQtyMap : " + lineKeyQtyMap);
    LOGGER.verbose("GCCreateContainerAPI.prepareQtyMap--End");
    LOGGER.endTimer("GCCreateContainerAPI.prepareQtyMap");
    return lineKeyQtyMap;
  }

  /**
   * This method is used to call getShipmentList API.
   * @param env
   * @param shipmentKey
   * @return
   */
  private YFCDocument callGetShipmentList(YFSEnvironment env, String shipmentKey) {

    LOGGER.beginTimer("GCCreateContainerAPI.callGetShipmentList");
    LOGGER.verbose("GCCreateContainerAPI.callGetShipmentList--Begin");
    //getShipmentList inDoc preparation
    YFCDocument getshipmentInDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement getshipmentInDocRootEle = getshipmentInDoc.getDocumentElement();
    getshipmentInDocRootEle.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
    YFCDocument getshipmentTempDoc = YFCDocument.getDocumentFor(GCConstants.GET_SHIP_LIST_TEMPLATE_PACK_DTL);


    LOGGER.verbose("getShipList Indoc Document: " + getshipmentInDoc.toString());

    YFCDocument getshipmentOutDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, getshipmentInDoc, getshipmentTempDoc);
    LOGGER.verbose("getShipList Outdoc :" + getshipmentOutDoc.toString());
    LOGGER.verbose("GCCreateContainerAPI.callGetShipmentList--End");
    LOGGER.endTimer("GCCreateContainerAPI.callGetShipmentList");
    return getshipmentOutDoc;
  }
  /**
   * This method is used to call changeShipmentStatus.
   *
   * @param env
   * @param shipmentKey
   * @param dropStatus
   * @param trnsactionID
   */
  private void callChangeShipmentStatusApi(YFSEnvironment env, String shipmentKey, String dropStatus,
      String trnsactionID) {

    LOGGER.beginTimer("GCCreateContainerAPI.callChangeShipmentStatusApi");
    LOGGER.verbose("GCCreateContainerAPI.callChangeShipmentStatusApi--Begin");

    YFCDocument changeShipmentDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement eleShipment = changeShipmentDoc.getDocumentElement();
    eleShipment.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
    eleShipment.setAttribute(GCConstants.BASE_DROP_STATUS, dropStatus);
    eleShipment.setAttribute(GCConstants.TRANSACTION_ID, trnsactionID);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("changeShipment Input :" + changeShipmentDoc.toString());
    }
    // call changeShipmentStatusAPI
    YFCDocument tempdoc = YFCDocument.getDocumentFor(GCConstants.CHANGE_SHIP_TEMP);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("changeShipment Temp :" + tempdoc.toString());
    }
    YFCDocument changeShipOutdoc = GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_SHIPMENT_STATUS, changeShipmentDoc, tempdoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("changeShipOutDoc :" + changeShipOutdoc.toString());
    }
    LOGGER.verbose("GCCreateContainerAPI.callChangeShipmentStatusApi--end");
    LOGGER.endTimer("GCCreateContainerAPI.callChangeShipmentStatusApi");
  }

}
