package com.gc.exstore.api.packing;

import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCDeleteContainerAPI implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCDeleteContainerAPI.class);

  public Document deleteContainer (YFSEnvironment env, Document inputDoc) {

    LOGGER.beginTimer("GCDeleteContainerAPI.callChangeShipmentStatus");
    LOGGER.verbose("GCDeleteContainerAPI.callChangeShipmentStatus -- Begin");
    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Indoc :" + inDoc.toString());
    }
    YFCElement shipInEle = inDoc.getDocumentElement();
    // Shipback to DC
    String isShipBackToDC = shipInEle.getAttribute(GCConstants.IS_SHIP_BACK_TO_DC);
    String shipmentKey = shipInEle.getAttribute(GCConstants.SHIPMENT_KEY);
    YFCElement containersInEle = shipInEle.getChildElement(GCConstants.CONTAINERS);
    YFCElement containerInEle = containersInEle.getChildElement(GCConstants.CONTAINER);
    YFCElement extnInContainerEle = containerInEle.getChildElement(GCConstants.EXTN);
    String extnMultiBoxId = GCConstants.BLANK_STRING;
    if(!YFCCommon.isVoid(extnInContainerEle)){
      extnMultiBoxId = extnInContainerEle.getAttribute(GCConstants.EXTN_MULTIBOX_GPID);
    }

    YFCDocument unpackShipTemp = YFCDocument.getDocumentFor(GCConstants.UNPACK_SHIP_TEMP);
    YFCDocument unpackShipOutdoc = YFCDocument.createDocument(GCConstants.SHIPMENT);

    YFCDocument getShpListIndoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCDocument getShipmentList = callGetShipmentList(env, shipmentKey, getShpListIndoc);

    YFCElement shipmentsEle = getShipmentList.getDocumentElement();
    YFCElement shipmentEle = shipmentsEle.getChildElement(GCConstants.SHIPMENT);
    YFCElement containers = shipmentEle.getChildElement(GCConstants.CONTAINERS);
    YFCIterable<YFCElement> containerList = containers.getChildren();
    for(YFCElement containerEle : containerList){

      LOGGER.verbose("COntainer :" + containerEle.toString());
      YFCDocument unpackShpmntIndoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
      YFCElement unpackShipRootEle = unpackShpmntIndoc.getDocumentElement();
      unpackShipRootEle.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
      YFCElement containersInUnpackShip = unpackShipRootEle.createChild(GCConstants.CONTAINERS);
      YFCElement extnEle = containerEle.getChildElement(GCConstants.EXTN);
      YFCElement containerInUnpackShip = containersInUnpackShip.createChild(GCConstants.CONTAINER);
      if(!YFCCommon.isVoid(extnMultiBoxId) && YFCUtils.equals(extnMultiBoxId, extnEle.getAttribute(GCConstants.EXTN_MULTIBOX_GPID))){
        containerInUnpackShip.setAttribute(GCConstants.SHIPMENT_CONTAINER_KEY,
            containerEle.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY));
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("Unpack Ship Indoc :" + unpackShpmntIndoc.toString());
        }
      } else if (YFCUtils.equals(containerInEle.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY), containerEle.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY))){
        containerInUnpackShip.setAttribute(GCConstants.SHIPMENT_CONTAINER_KEY,
            containerEle.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY));
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("Unpack Ship Indoc :" + unpackShpmntIndoc.toString());
        }
      }

      if(containerInUnpackShip.hasAttribute(GCConstants.SHIP_CONTAINER_KEY)){
        LOGGER.verbose("Match!");
        unpackShipOutdoc = GCCommonUtil.invokeAPI(env, GCConstants.UNPACK_SHIPMENT, unpackShpmntIndoc, unpackShipTemp);
      }
    }

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Unpack Ship Outdoc :" + unpackShipOutdoc.toString());
    }
    YFCElement shpmntOutEle = unpackShipOutdoc.getDocumentElement();
    Double containerizedQty = shpmntOutEle.getDoubleAttribute(GCConstants.CONTAINERIZED_QTY);
    LOGGER.verbose("Containerized Quantity :" + containerizedQty);

    LOGGER.verbose("Status -- " + shpmntOutEle.getAttribute(GCConstants.STATUS));

    if(containerizedQty == 0){
      if (YFCUtils.equals(GCConstants.YES, isShipBackToDC)) {
        callChangeShipmentStatus(env, shipmentKey, GCConstants.READY_FOR_PACKING_TRNSFR_ORD,
            GCConstants.READY_FOR_PACK_TRANSACTION);
      } else {
        callChangeShipmentStatus(env, shipmentKey, GCConstants.READY_FOR_PACKING, GCConstants.UNDO_PACK_SHIP_COMPLETE);
      }

    } else if (YFCUtils.equals(GCConstants.YES, isShipBackToDC)
        && !YFCUtils.equals(GCConstants.SHP_BEING_PACKED_STATUS_TRNSFR_ORD,
            shpmntOutEle.getAttribute(GCConstants.STATUS))) {
      LOGGER.verbose("ShipBackToDC -- ");
      callChangeShipmentStatus(env, shipmentKey, GCConstants.SHP_BEING_PACKED_STATUS_TRNSFR_ORD,
          GCConstants.TRNSFR_ORD_SHIP_STATUS_UPDATE);
    } else if (YFCUtils.equals(GCConstants.NO, isShipBackToDC)
        && !YFCUtils.equals(GCConstants.SHIPMENT_BEING_PACKED, shpmntOutEle.getAttribute(GCConstants.STATUS))) {
      LOGGER.verbose("Pack Detail -- ");
      callChangeShipmentStatus(env, shipmentKey, GCConstants.SHIPMENT_BEING_PACKED, GCConstants.UNDO_PACK_SHIP_COMPLETE);
    }


    if (YFCUtils.equals(GCConstants.YES, isShipBackToDC)) {
      getShpListIndoc.getDocumentElement().setAttribute(GCConstants.IS_SHIP_BACK_TO_DC, GCConstants.YES);
      getShpListIndoc.getDocumentElement().setAttribute(GCConstants.SHIP_NODE,
          shipInEle.getAttribute(GCConstants.SHIP_NODE));
    }
    Document outDoc = GCCommonUtil.invokeService(env, GCConstants.GC_PACK_DETAIL_SHPLIST, getShpListIndoc.getDocument());
    LOGGER.verbose("GCDeleteContainerAPI.callChangeShipmentStatus -- END");
    LOGGER.endTimer("GCDeleteContainerAPI.callChangeShipmentStatus");
    return outDoc;

  }

  /**
   *
   * @param env
   * @param shipmentKey
   * @param getShpListIndoc
   * @return
   */
  private YFCDocument callGetShipmentList(YFSEnvironment env, String shipmentKey, YFCDocument getShpListIndoc) {

    LOGGER.beginTimer("GCDeleteContainerAPI.callChangeShipmentStatus");
    LOGGER.verbose("GCDeleteContainerAPI.callChangeShipmentStatus -- Begin");
    YFCElement shpmntEle = getShpListIndoc.getDocumentElement();
    shpmntEle.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);

    YFCDocument getShpListTemp = YFCDocument.getDocumentFor(GCConstants.GET_SHPMNT_LIST_TEMP_DEL);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Template :" + getShpListTemp.toString());
    }

    YFCDocument getShipmentList = GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, getShpListIndoc, getShpListTemp);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getShipmentDetails Output :" + getShipmentList.toString());
    }
    LOGGER.verbose("GCDeleteContainerAPI.callChangeShipmentStatus -- END");
    LOGGER.endTimer("GCDeleteContainerAPI.callChangeShipmentStatus");
    return getShipmentList;
  }

  /**
   *
   * @param env
   * @param shipmentKey
   * @param baseDrpStatus
   * @param transactionID
   */
  private void callChangeShipmentStatus(YFSEnvironment env, String shipmentKey, String baseDrpStatus,
      String transactionID) {

    LOGGER.beginTimer("GCDeleteContainerAPI.callChangeShipmentStatus");
    LOGGER.verbose("GCDeleteContainerAPI.callChangeShipmentStatus -- Begin");
    YFCDocument changeShipmentIndoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement shipmentInEle = changeShipmentIndoc.getDocumentElement();
    shipmentInEle.setAttribute(GCConstants.BASE_DROP_STATUS, baseDrpStatus);
    shipmentInEle.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
    shipmentInEle.setAttribute(GCConstants.TRANSACTION_ID, transactionID);

    GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_SHIPMENT_STATUS, changeShipmentIndoc.getDocument());
    LOGGER.verbose("GCDeleteContainerAPI.callChangeShipmentStatus -- END");
    LOGGER.endTimer("GCDeleteContainerAPI.callChangeShipmentStatus");
  }
  /**
   *
   */

  @Override
  public void setProperties(Properties arg0) {


  }
}
