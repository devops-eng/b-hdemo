/**
 * Copyright 2014 Guitar Center. All rights reserved. Guitar Center PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 *
 * Change Log
 * ############################################################################################################################################################################
 *       OBJECTIVE: Foundation Class
 * ############################################################################################################################################################################
 *          Version                 Date                        Modified By                                         Description
 * ############################################################################################################################################################################
 *           1.0                 01/16/2015                  Rangarajan,Shwetha             The class contains logic to display the required shipment details
 *                                                                                          in case of both store picked and DC fulfilled store return orders.
 * ############################################################################################################################################################################
 */
package com.gc.exstore.api.packing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * The class contains logic to display the required shipment details in case of both store picked and DC fulfilled store return orders.
 * @author <a href="mailto:shwetha.rangarajan@expicient.com">Shwetha</a>
 *
 */
public class GCPackDetailsManager implements YIFCustomApi{

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCPackDetailsManager.class);

  /**
   * The main method which invokes getShipmentDetail API to fetch shipment details and
   * modify it for required additional details to be displayed on the exstore UI.
   * @param env
   * @param inputDoc
   * @return
   */
  public Document preparePackDetailDocument(YFSEnvironment env, Document inputDoc) {

    LOGGER.beginTimer("GCPackDetailsManager.preparePackDetailDocument");
    LOGGER.verbose("GCPackDetailsManager.preparePackDetailDocument -- Begin");

    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Indoc :" + inDoc.toString());
    }

    YFCElement rootInEle = inDoc.getDocumentElement();
    String isShipBackToDC = rootInEle.getAttribute(GCConstants.IS_SHIP_BACK_TO_DC);
    LOGGER.verbose("IsShipBackToDC Input boolean :" +isShipBackToDC);

    YFCElement shipmentEle;

    if(YFCUtils.equals(GCConstants.YES, isShipBackToDC)){

      YFCDocument getShipListIndoc = YFCDocument.getDocumentFor(GCConstants.GET_SHIP_LIST_INPUT);

      //Append the logged in Store ID to fetch the respective shipments only.
      YFCElement shipmentInEle = getShipListIndoc.getDocumentElement();
      shipmentInEle.setAttribute(GCConstants.SHIP_NODE, rootInEle.getAttribute(GCConstants.SHIP_NODE));

      if(LOGGER.isVerboseEnabled()){
        LOGGER.verbose("Call getShipmentList to fetch transfer order details" +getShipListIndoc.toString());
      }
      YFCDocument getShipListTemp = YFCDocument.getDocumentFor(GCConstants.GET_SHIPMENT_LIST_TEMP);
      if(LOGGER.isVerboseEnabled()){
        LOGGER.verbose("getShipmentList template :" +getShipListTemp.toString());
      }
      YFCDocument getShipList = GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, getShipListIndoc, getShipListTemp);
      if(LOGGER.isVerboseEnabled()){
        LOGGER.verbose("getShipmentList Outdoc :" +getShipList.toString());
      }

      YFCElement shipmentsEle = getShipList.getDocumentElement();
      shipmentEle = shipmentsEle.getChildElement(GCConstants.SHIPMENT);

      if (YFCCommon.isVoid(shipmentEle)) {
        final YFCDocument shipmentDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
        final YFCElement shipOutEle = shipmentDoc.getDocumentElement();
        shipOutEle.setAttribute("IsShipmentListEmpty", "Y");
        return shipmentDoc.getDocument();
      }

    } else {

      YFCDocument getShpDtlsTemp = YFCDocument.getDocumentFor(GCConstants.GET_SHPMNT_DETAIL_TMP);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("Template :" + getShpDtlsTemp.toString());
      }
      YFCDocument getShpDetails = GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_DTL, inDoc, getShpDtlsTemp);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("getShipmentDetails Output :" + getShpDetails.toString());
      }
      shipmentEle = getShpDetails.getDocumentElement();

    }


    // Started preparing a custom doc
    final YFCDocument finalDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    final YFCElement shipOutEle = finalDoc.getDocumentElement();

    final String levelOfServiceDesc = callCommonCodeList(env, shipmentEle.getAttribute(GCConstants.LEVEL_OF_SERVICE));
    // Stamping Attributes
    stampAttributes(shipOutEle, shipmentEle, levelOfServiceDesc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("After stamping attributes :" + shipOutEle.toString());
    }
    //Fetch OrderHeaderKey and call getGCOrderCommissionList Service.
    YFCElement shipmentLinesEle = shipmentEle.getChildElement(GCConstants.SHIPMENT_LINES);
    String orderHeaderKey = shipmentEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
    if(YFCUtils.equals(GCConstants.YES, isShipBackToDC)){
      YFCElement shipmentLine = shipmentLinesEle.getChildElement(GCConstants.SHIPMENT_LINE);
      orderHeaderKey = shipmentLine.getAttribute(GCConstants.ORDER_HEADER_KEY);
    }
    // calling getGCOrderCommissionListService and stamping userName in the custom doc
    final String userName = getGCOrderCommissionListService(env, orderHeaderKey);
    LOGGER.verbose("UserName fetched :" + userName);
    shipOutEle.setAttribute(GCXmlLiterals.USER_NAME, userName);

    //Create List for storing ShipmentLineKey for Bundle and Warranty Components.
    List<String> warrantyLineList = new ArrayList<String>();
    Map<String, String> bundleItemList = new HashMap<String, String>();
    Map<String, Double> quantityMap = new HashMap<String, Double>();
    Map<String, List<String>> containerMap = new HashMap<String, List<String>>();
    List<String> giftItemParentList = new ArrayList<String>();

    YFCElement shipmentLinesOutEle = shipOutEle.createChild(GCConstants.SHIPMENT_LINES);

    if (!shipmentLinesEle.hasChildNodes() && YFCUtils.equals(GCConstants.YES, isShipBackToDC)) {
      final YFCDocument shipmentDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
      final YFCElement shipmentOutEle = shipmentDoc.getDocumentElement();
      shipmentOutEle.setAttribute("IsShipmentListEmpty", "Y");
      return shipmentDoc.getDocument();
    }
    calculatePackedQty(shipmentEle, quantityMap);
    LOGGER.verbose("PackedQty caalculated :" + quantityMap.toString());

    //Iterate over ShipmentLines
    YFCIterable<YFCElement> shipmentLineList = shipmentLinesEle.getChildren();

    for(YFCElement shipmentLineEle : shipmentLineList){


      String shipmentLineKey = shipmentLineEle.getAttribute(GCConstants.SHIPMENT_LINE_KEY);
      LOGGER.verbose("ShipmentLineKey fecthed :" + shipmentLineKey);
      YFCElement orderLineEle = shipmentLineEle.getChildElement(GCConstants.ORDER_LINE);
      YFCElement orderLineExtnEle = orderLineEle.getChildElement(GCConstants.EXTN);

      //Prepare ContainerMap with item details
      prepareContainerMap(env, containerMap, shipmentLineEle, shipmentLineKey, isShipBackToDC);
      LOGGER.verbose("Image URL fetched :" +shipmentLineEle.getAttribute(GCConstants.IMAGE_URL));
      LOGGER.verbose("Item details Map :" + containerMap.toString());

      //Price Calculation for UI
      YFCElement lineChargesEle = null;
      YFCElement linePriceInfoEle = null;
      double quantity = shipmentLineEle.getDoubleAttribute(GCConstants.QUANTITY);
      LOGGER.verbose("Shipped Quantity :" +quantity);
      double orderedQty  = orderLineEle.getDoubleAttribute(GCConstants.ORDERED_QTY);
      LOGGER.verbose("Ordered Quantity :" +orderedQty);

      if(YFCUtils.equals(GCConstants.YES, isShipBackToDC)){
        YFCDocument getOrderListTemp = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_OUT_TEMP);
        String returnOrderHeaderKey = orderLineEle.getAttribute(GCConstants.DERIVED_FROM_ORDER_HEADER_KEY);
        YFCDocument ordListOpDoc = GCCommonUtil.invokeGetOrderListAPI(env, returnOrderHeaderKey, getOrderListTemp);
        YFCElement eleReturnOrderList = ordListOpDoc.getDocumentElement();
        YFCElement eleReturnOrder = eleReturnOrderList.getChildElement(GCConstants.ORDER);
        YFCElement returnOrderLines = eleReturnOrder.getChildElement(GCConstants.ORDER_LINES);
        YFCIterable<YFCElement> returnOrderLineList = returnOrderLines.getChildren();
        for(YFCElement returnOrderLine : returnOrderLineList){
          if(YFCUtils.equals(orderLineEle.getAttribute(GCConstants.DERIVED_FROM_ORDER_LINE_KEY), returnOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY))){
            linePriceInfoEle = returnOrderLine.getChildElement(GCConstants.LINE_PRICE_INFO);
            LOGGER.verbose("LinePriceInfo fetched from getOrderList Ouput :" +linePriceInfoEle.toString());
            lineChargesEle = returnOrderLine.getChildElement(GCConstants.LINE_CHARGES);
            LOGGER.verbose("LineCharges fetched from getOrderList output :" +lineChargesEle.toString());
          }
        }
        shipmentLineEle.setAttribute(GCConstants.EXTN_RETURN_ORDER_NO, eleReturnOrder.getAttribute(GCConstants.ORDER_NO));
        shipmentLineEle.setAttribute(GCConstants.EXTN_VPO_REF_NO, orderLineExtnEle.getAttribute(GCConstants.EXTN_RETURN_VPO_NO));

      } else {
        linePriceInfoEle = orderLineEle.getChildElement(GCConstants.LINE_PRICE_INFO);
        LOGGER.verbose("LinePriceInfo fetched from getShipmentList Ouput :" +linePriceInfoEle.toString());
        lineChargesEle = orderLineEle.getChildElement(GCConstants.LINE_CHARGES);
        LOGGER.verbose("LineCharges fetched from getShipmentList output :" +lineChargesEle.toString());
      }

      //Unit Price
      double unitPrice = linePriceInfoEle.getDoubleAttribute(GCConstants.UNIT_PRICE);
      LOGGER.verbose("UnitPrice :" + unitPrice);
      //Calculate Unit Promotion
      double totalPromotion = 0;
      double unitPromotion = 0;
      unitPromotion = calculateUnitPromotion(lineChargesEle, orderedQty, totalPromotion, unitPromotion);
      unitPrice = unitPrice - unitPromotion;
      LOGGER.verbose("Final Unit Price :" + unitPrice);

      //Calculate qtyAvailToPack
      double qtyAvailToPack = 0;
      if(quantityMap.containsKey(shipmentLineKey)){

        Double packedQty = quantityMap.get(shipmentLineKey);
        LOGGER.verbose("Packed Quantity :" + packedQty);
        double shippedQty = shipmentLineEle.getDoubleAttribute(GCConstants.QUANTITY);
        LOGGER.verbose("Shipped Quantity :" + shippedQty);
        qtyAvailToPack = shippedQty - packedQty;
        LOGGER.verbose("Quantity Avail to pack " + qtyAvailToPack);

      } else {
        qtyAvailToPack = shipmentLineEle.getDoubleAttribute(GCConstants.QUANTITY);
      }

      //Check if ShipmentLine is to be shown on the UI
      String extnActualSerialNo = orderLineExtnEle.getAttribute(GCConstants.EXTN_ACTUAL_SERIAL_NO);
      LOGGER.verbose("ActualSerialNo fetched :" +extnActualSerialNo);
      String productClass = shipmentLineEle.getAttribute(GCConstants.PRODUCT_CLASS);
      LOGGER.verbose("Product Class fetched :" +productClass);

      if(YFCUtils.equals(GCConstants.BUNDLE, orderLineEle.getAttribute(GCConstants.KIT_CODE)) &&
          YFCUtils.equals(GCConstants.SET, productClass)&& quantity!=0 ){
        LOGGER.verbose("Import the shipment line and stamp attributes for Retail Set parent");
        shipmentLinesOutEle.importNode(shipmentLineEle);
        setShipmentLineAttr(shipmentLinesOutEle, shipmentLineKey, qtyAvailToPack, unitPrice, extnActualSerialNo, true);
        bundleItemList.put(shipmentLineKey, shipmentLineEle.getAttribute(GCConstants.QUANTITY));
        LOGGER.verbose("Bundle Parent Map :" + bundleItemList.toString());
      } else if(YFCUtils.equals(GCConstants.YES, orderLineExtnEle.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM))){
        warrantyLineList.add(shipmentLineKey);
      } else if(!YFCUtils.equals(GCConstants.BUNDLE, orderLineEle.getAttribute(GCConstants.KIT_CODE))
          && (YFCUtils.equals(GCConstants.REGULAR, productClass)) && quantity != 0) {
        LOGGER.verbose("Import Regular shipment lines and DC Bundle Components in case of ShipBack to DC");
        shipmentLinesOutEle.importNode(shipmentLineEle);
        setShipmentLineAttr(shipmentLinesOutEle, shipmentLineKey, qtyAvailToPack, unitPrice, extnActualSerialNo, false);
        if(YFCUtils.equals(GCConstants.YES, orderLineExtnEle.getAttribute(GCConstants.EXTN_IS_FREE_GIFT_ITEM))){
          giftItemParentList.add(orderLineEle.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY));
          LOGGER.verbose("GiftParent List :" + giftItemParentList.toString());
        }
      }
    }

    if (!shipmentLinesOutEle.hasChildNodes() && YFCUtils.equals(GCConstants.YES, isShipBackToDC)) {
      final YFCDocument shipmentDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
      final YFCElement shipmentOutEle = shipmentDoc.getDocumentElement();
      shipmentOutEle.setAttribute("IsShipmentListEmpty", "Y");
      return shipmentDoc.getDocument();
    }
    appendContainerDetails(shipmentEle, shipOutEle, warrantyLineList, bundleItemList, containerMap, giftItemParentList, isShipBackToDC);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Final Output :" + finalDoc.toString());
    }

    LOGGER.verbose("GCPackDetailsManager.preparePackDetailDocument -- END");
    LOGGER.endTimer("GCPackDetailsManager.preparePackDetailDocument");
    return finalDoc.getDocument();
  }

  /**
   * Set custom attributes at the ShipmentLine level.
   * @param shipmentLinesOutEle
   * @param shipmentLineKey
   * @param qtyAvailToPack
   * @param unitPrice
   * @param extnActualSerialNo
   * @param isBundle
   * @param isShipBackToDC
   */
  private void setShipmentLineAttr(YFCElement shipmentLinesOutEle, String shipmentLineKey, double qtyAvailToPack, double unitPrice,
      String extnActualSerialNo, boolean isBundle) {

    LOGGER.beginTimer("GCPackDetailsManager.setShipmentLineAttr");
    LOGGER.verbose("GCPackDetailsManager.setShipmentLineAttr -- START");
    YFCIterable<YFCElement> shipmentLineList = shipmentLinesOutEle.getChildren();
    for(YFCElement shipmentLine : shipmentLineList){
      if(YFCUtils.equals(shipmentLineKey, shipmentLine.getAttribute(GCConstants.SHIPMENT_LINE_KEY))){

        shipmentLine.setAttribute(GCConstants.QTY_AVAIL_TO_PACK, qtyAvailToPack);
        shipmentLine.setAttribute(GCConstants.UNIT_PRICE, unitPrice);
        shipmentLine.setAttribute(GCConstants.SERIAL_NO, extnActualSerialNo);
        if(isBundle){
          shipmentLine.setAttribute(GCConstants.KIT_CODE, GCConstants.BUNDLE);
        }

        YFCElement orderLineEle = shipmentLine.getChildElement(GCConstants.ORDER_LINE);
        YFCElement itemDtlsEle = orderLineEle.getChildElement(GCConstants.ITEM_DETAILS);
        YFCElement extnEle = itemDtlsEle.getChildElement(GCConstants.EXTN);
        if (!YFCCommon.isVoid(extnEle)) {
          String extnPOSItemId = extnEle.getAttribute(GCConstants.EXTN_POSITEM_ID);
          if (YFCCommon.isVoid(extnPOSItemId)) {
            extnEle.setAttribute(GCConstants.EXTN_POSITEM_ID, itemDtlsEle.getAttribute(GCConstants.ITEM_ID));
          }
        }
      }
    }
    LOGGER.verbose("GCPackDetailsManager.setShipmentLineAttr -- END");
    LOGGER.endTimer("GCPackDetailsManager.setShipmentLineAttr");
  }

  /**
   * Append the required details to be displayed on the UI.
   * @param shipmentEle
   * @param shipOutEle
   * @param warrantyLineList
   * @param bundleItemList
   * @param containerMap
   * @param giftItemParentList
   * @param isShipBackToDC
   */
  private void appendContainerDetails(YFCElement shipmentEle, final YFCElement shipOutEle,
      List<String> warrantyLineList, Map<String, String> bundleItemList, Map<String, List<String>> containerMap, List<String> giftItemParentList, String isShipBackToDC) {

    LOGGER.beginTimer("GCPackDetailsManager.appendContainerDetails");
    LOGGER.verbose("GCPackDetailsManager.appendContainerDetails -- START");

    //Create a temporary map to store Return Order No and ExtnReturnVPO number in case of transfer orders.
    Map<String, List<String>> returnOrdDtlsMap = new HashMap<String, List<String>>();
    YFCElement shipmentLinesEle = shipOutEle.getChildElement(GCConstants.SHIPMENT_LINES);
    YFCIterable<YFCElement> shipmentLineList = shipmentLinesEle.getChildren();
    for(YFCElement shipmentLineEle : shipmentLineList){
      if(YFCUtils.equals(GCConstants.YES, isShipBackToDC)){
        List<String> returnOrdDetailsList = new ArrayList<String>();
        returnOrdDetailsList.add(shipmentLineEle.getAttribute(GCConstants.EXTN_RETURN_ORDER_NO));
        returnOrdDetailsList.add(shipmentLineEle.getAttribute(GCConstants.EXTN_VPO_REF_NO));
        returnOrdDtlsMap.put(shipmentLineEle.getAttribute(GCConstants.SHIPMENT_LINE_KEY), returnOrdDetailsList);
      }

      YFCElement orderLineEle = shipmentLineEle.getChildElement(GCConstants.ORDER_LINE);
      String orderLineKey = orderLineEle.getAttribute(GCConstants.ORDER_LINE_KEY);
      if(giftItemParentList.contains(orderLineKey)){
        shipmentLineEle.setAttribute(GCConstants.HAS_GIFT_ITEM, GCConstants.YES);
      }
    }

    YFCElement containersOutEle = shipOutEle.createChild(GCConstants.CONTAINERS);
    YFCElement containersEle = shipmentEle.getChildElement(GCConstants.CONTAINERS);
    YFCIterable<YFCElement> containerList = containersEle.getChildren();
    for(YFCElement containerEle : containerList){

      YFCElement containerOutEle = containersOutEle.createChild(GCConstants.CONTAINER);
      containerOutEle.setAttribute(GCConstants.SHIPMENT_CONTAINER_KEY, containerEle.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY));
      containerOutEle.setAttribute(GCConstants.TRACKING_NO, containerEle.getAttribute(GCConstants.TRACKING_NO));
      containerOutEle.setAttribute(GCConstants.SHIPMENT_KEY, containerEle.getAttribute(GCConstants.SHIPMENT_KEY));
      YFCElement extnInContainerEle = containerEle.getChildElement(GCConstants.EXTN);
      YFCElement extnInContainerOutEle = containerOutEle.createChild(GCConstants.EXTN);
      if(!YFCCommon.isVoid(extnInContainerEle) && !extnInContainerEle.hasAttribute(GCConstants.EXTN_MULTIBOX_GRP_ID)){
        extnInContainerOutEle.setAttribute(GCConstants.EXTN_MULTIBOX_GRP_ID, GCConstants.BLANK_STRING);
      } else {
        extnInContainerOutEle.setAttribute(GCConstants.EXTN_MULTIBOX_GRP_ID, extnInContainerEle.getAttribute(GCConstants.EXTN_MULTIBOX_GRP_ID));
      }
      if(!YFCCommon.isVoid(extnInContainerEle) && !extnInContainerEle.hasAttribute(GCConstants.EXTN_MULTIBOX_DESC)){
        extnInContainerOutEle.setAttribute(GCConstants.EXTN_MULTIBOX_DESC, GCConstants.BLANK_STRING);
      } else {
        extnInContainerOutEle.setAttribute(GCConstants.EXTN_MULTIBOX_DESC, extnInContainerEle.getAttribute(GCConstants.EXTN_MULTIBOX_DESC));
      }
      YFCElement containerDtlsOutEle = containerOutEle.createChild(GCConstants.CONTAINER_DETAILS);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("After appending container details :" + shipOutEle.toString());
      }
      int noOfContainers = 0;
      List<String> parentShipLineKeyList = new ArrayList<String>();
      YFCElement containerDetailsEle = containerEle.getChildElement(GCConstants.CONTAINER_DETAILS);
      YFCIterable<YFCElement> containerDtlList = containerDetailsEle.getChildren();
      for(YFCElement containerDetailEle : containerDtlList){

        String shipmentLineKey = containerDetailEle.getAttribute(GCConstants.SHIPMENT_LINE_KEY);
        if(returnOrdDtlsMap.containsKey(shipmentLineKey)){
          stampReturnDetails(returnOrdDtlsMap, shipmentLineKey, containerDetailEle);
        }
        YFCElement shipmentLineChildEle = containerDetailEle.getChildElement(GCConstants.SHIPMENT_LINE);
        if(!warrantyLineList.contains(shipmentLineKey) && !shipmentLineChildEle.hasAttribute(GCConstants.PARENT_SHPMNTLINE_KEY)){

          containerDtlsOutEle.importNode(containerDetailEle);
          setContainerDetailAttr(containerDtlsOutEle, shipmentLineKey, containerMap);
          noOfContainers++;
          if (LOGGER.isVerboseEnabled()) {
            LOGGER.verbose("After appending details for regular items :" + shipOutEle.toString());
          }
        } else if (shipmentLineChildEle.hasAttribute(GCConstants.PARENT_SHPMNTLINE_KEY)){

          if(YFCUtils.equals(GCConstants.REGULAR, containerDetailEle.getAttribute(GCConstants.PRODUCT_CLASS))){

            containerDtlsOutEle.importNode(containerDetailEle);
            setContainerDetailAttr(containerDtlsOutEle, shipmentLineKey, containerMap);
            noOfContainers++;
            if (LOGGER.isVerboseEnabled()) {
              LOGGER.verbose("After appending details for DC bundle component items :" + shipOutEle.toString());
            }

          } else if(YFCUtils.equals(GCConstants.SET, containerDetailEle.getAttribute(GCConstants.PRODUCT_CLASS))) {

            String shipLineKeyForParent = shipmentLineChildEle.getAttribute(GCConstants.PARENT_SHPMNTLINE_KEY);
            if(!parentShipLineKeyList.contains(shipLineKeyForParent)){
              parentShipLineKeyList.add(shipLineKeyForParent);
              LOGGER.verbose("ParentShipmentLineKey :" + shipLineKeyForParent);
              YFCElement containerDtlOutEle = containerDtlsOutEle.createChild(GCConstants.CONTAINER_DETAIL);
              containerDtlOutEle.setAttribute(GCConstants.SHIPMENT_LINE_KEY, shipLineKeyForParent);
              List<String> tempItemList = containerMap.get(shipLineKeyForParent);
              containerDtlOutEle.setAttribute(GCConstants.ITEM_ID, tempItemList.get(0));
              containerDtlOutEle.setAttribute(GCConstants.ITEM_BRAND, tempItemList.get(1));
              containerDtlOutEle.setAttribute(GCConstants.ITEM_DESCRIPTION, tempItemList.get(2));
              containerDtlOutEle.setAttribute(GCConstants.IMAGE_URL, tempItemList.get(3));
              noOfContainers++;
              LOGGER.verbose("bundleItemList map :" + bundleItemList.toString());
              String shipQuantityAttr = bundleItemList.get(shipLineKeyForParent);
              double parentQty = Double.parseDouble(shipQuantityAttr);
              double compQty = Double.parseDouble(shipmentLineChildEle.getAttribute(GCConstants.QUANTITY));
              double packQty = Double.parseDouble(containerDetailEle.getAttribute(GCConstants.QUANTITY));

              double quantity = parentQty * packQty / compQty;
              containerDtlOutEle.setAttribute(GCConstants.QUANTITY, ""+quantity);

              //Stamp return details for transfer orders
              if(returnOrdDtlsMap.containsKey(shipLineKeyForParent)){
                stampReturnDetails(returnOrdDtlsMap, shipLineKeyForParent, containerDtlOutEle);
              }
              if (LOGGER.isVerboseEnabled()) {
                LOGGER.verbose("After appending retail set parent details :" + shipOutEle.toString());
              }
            }

          }

        }

      }

      if(noOfContainers!=0){
        containerOutEle.setAttribute(GCConstants.NO_OF_ITEMS, noOfContainers);
      } else {
        containersOutEle.removeChild(containerOutEle);
      }
    }

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Final containetr details :" + shipOutEle.toString());
    }
    LOGGER.verbose("GCPackDetailsManager.appendContainerDetails -- END");
    LOGGER.endTimer("GCPackDetailsManager.appendContainerDetails");
  }

  /**
   *
   * @param returnOrdDtlsMap
   * @param shipLineKeyForParent
   * @param containerDtlOutEle
   */
  private void stampReturnDetails(Map<String, List<String>> returnOrdDtlsMap, String shipLineKeyForParent,
      YFCElement containerDtlOutEle) {

    //Refactored code for Sonar Fixes.
    List<String> tempList = returnOrdDtlsMap.get(shipLineKeyForParent);
    containerDtlOutEle.setAttribute(GCConstants.EXTN_RETURN_ORDER_NO, tempList.get(0));
    if(!YFCCommon.isVoid(tempList.get(1))){
      containerDtlOutEle.setAttribute(GCConstants.EXTN_VPO_REF_NO, tempList.get(1));
    }
  }

  /**
   * Stamp Item details at container level to be displayed on the UI
   * @param containerDtlsOutEle
   * @param shipmentLineKey
   * @param containerMap
   */
  private void setContainerDetailAttr(YFCElement containerDtlsOutEle, String shipmentLineKey,
      Map<String, List<String>> containerMap) {

    YFCIterable<YFCElement> containerDtlOutList = containerDtlsOutEle.getChildren();
    for (YFCElement containerDtlOutEle : containerDtlOutList) {
      containerDtlOutEle = containerDtlsOutEle.getChildElement(GCConstants.CONTAINER_DETAIL);
      if (YFCUtils.equals(shipmentLineKey, containerDtlOutEle.getAttribute(GCConstants.SHIPMENT_LINE_KEY))) {
        List<String> tempItemList = containerMap.get(shipmentLineKey);
        containerDtlOutEle.setAttribute(GCConstants.ITEM_ID, tempItemList.get(0));
        containerDtlOutEle.setAttribute(GCConstants.ITEM_BRAND, tempItemList.get(1));
        containerDtlOutEle.setAttribute(GCConstants.ITEM_DESCRIPTION, tempItemList.get(2));
        containerDtlOutEle.setAttribute(GCConstants.IMAGE_URL, tempItemList.get(3));
      }
    }
  }

  /**
   * Prepare a map with ShipmentLineKey as the key and List with item details as the value.
   * @param containerMap
   * @param orderLineEle
   * @param shipmentLineKey
   * @param isShipBackToDC
   */
  private void prepareContainerMap(YFSEnvironment env, Map<String, List<String>> containerMap, YFCElement shipmentLineEle,
      String shipmentLineKey, String isShipBackToDC) {

    LOGGER.beginTimer("GCPackDetailsManager.prepareContainerMap");
    LOGGER.verbose("GCPackDetailsManager.prepareContainerMap -- START");
    YFCElement primaryInfoEle;
    YFCElement itemExtnEle;

    if(YFCUtils.equals(GCConstants.YES, isShipBackToDC)){
      String itemID = shipmentLineEle.getAttribute(GCConstants.ITEM_ID);
      String uom = shipmentLineEle.getAttribute(GCConstants.UNIT_OF_MEASURE);
      YFCDocument getItemListTemp = YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LIST_TEMP_PACK);
      if(LOGGER.isVerboseEnabled()){
        LOGGER.verbose("getItemList Temp:" +getItemListTemp.toString());
      }
      YFCDocument getItemListOutdoc =
          GCCommonUtil.getItemList(env, itemID, uom, GCConstants.GCI, true, getItemListTemp);
      YFCElement itemListEle = getItemListOutdoc.getDocumentElement();
      YFCElement itemEle = itemListEle.getChildElement(GCConstants.ITEM);
      primaryInfoEle = itemEle.getChildElement(GCConstants.PRIMARY_INFORMATION);
      itemExtnEle = itemEle.getChildElement(GCConstants.EXTN);

      shipmentLineEle.setAttribute(GCConstants.EXTN_BRAND_NAME, itemExtnEle.getAttribute(GCConstants.EXTN_BRAND_NAME));
      if (!YFCCommon.isVoid(itemExtnEle.getAttribute(GCConstants.EXTN_POSITEM_ID))) {
        shipmentLineEle
        .setAttribute(GCConstants.EXTN_POSITEM_ID, itemExtnEle.getAttribute(GCConstants.EXTN_POSITEM_ID));
      } else {
        shipmentLineEle.setAttribute(GCConstants.EXTN_POSITEM_ID, itemEle.getAttribute(GCConstants.ITEM_ID));
      }

    } else {
      YFCElement orderLineEle = shipmentLineEle.getChildElement(GCConstants.ORDER_LINE);
      YFCElement itemDtlsEle = orderLineEle.getChildElement(GCConstants.ITEM_DETAILS);
      primaryInfoEle = itemDtlsEle.getChildElement(GCConstants.PRIMARY_INFORMATION);
      itemExtnEle = itemDtlsEle.getChildElement(GCConstants.EXTN);

    }

    List<String> itemDetailsList = new ArrayList<String>();

    if (!YFCCommon.isVoid(itemExtnEle.getAttribute(GCConstants.EXTN_POSITEM_ID))) {
      itemDetailsList.add(itemExtnEle.getAttribute(GCConstants.EXTN_POSITEM_ID));
    } else {
      itemDetailsList.add(shipmentLineEle.getAttribute(GCConstants.EXTN_POSITEM_ID));
    }
    itemDetailsList.add(itemExtnEle.getAttribute(GCConstants.EXTN_BRAND_NAME));
    itemDetailsList.add(primaryInfoEle.getAttribute(GCConstants.SHORT_DESCRIPTION));
    String imageURL = primaryInfoEle.getAttribute(GCConstants.IMAGE_LOCATION) + "/" + primaryInfoEle.getAttribute(GCConstants.IMAGE_ID);
    itemDetailsList.add(imageURL);
    shipmentLineEle.setAttribute(GCConstants.IMAGE_URL, imageURL);
    containerMap.put(shipmentLineKey, itemDetailsList);
    LOGGER.verbose("Item Details Map formed :" + containerMap.toString());
    LOGGER.verbose("GCPackDetailsManager.prepareContainerMap -- END");
    LOGGER.endTimer("GCPackDetailsManager.prepareContainerMap");
  }

  /**
   * Loop through the containers and calculate the containerized quantity for a ShipmentLine
   * @param shipmentEle
   * @param quantityMap
   */
  public static void calculatePackedQty(YFCElement shipmentEle, Map<String, Double> quantityMap) {

    LOGGER.beginTimer("GCPackDetailsManager.calculatePackedQty");
    LOGGER.verbose("GCPackDetailsManager.calculatePackedQty -- START");
    //Iterate over ContainerDetail to calculate quantity against each ShipmentLine
    YFCElement containersEle = shipmentEle.getChildElement(GCConstants.CONTAINERS);
    YFCIterable<YFCElement> containerList = containersEle.getChildren();
    List<String> shipContainerKeyList = new ArrayList<String>();
    for(YFCElement containerEle : containerList){
      final String containerKey = containerEle.getAttribute(GCConstants.SHIP_CONTAINER_KEY);
      YFCElement containerDetailsEle = containerEle.getChildElement(GCConstants.CONTAINER_DETAILS);
      YFCIterable<YFCElement> containerDtlList = containerDetailsEle.getChildren();
      boolean isMultipleContainer = true;
      for(YFCElement containerDetailEle : containerDtlList){
        isMultipleContainer =
            getQtyToPack(shipmentEle, quantityMap, containerDetailEle, isMultipleContainer, shipContainerKeyList,
                containerKey);
        LOGGER.verbose("QuantityMap :" + quantityMap.toString());
      }
    }
    LOGGER.verbose("Final packed quantity Map " + quantityMap.toString());
    LOGGER.verbose("GCPackDetailsManager.calculatePackedQty -- END");
    LOGGER.endTimer("GCPackDetailsManager.calculatePackedQty");
  }

  /**
   * Calculation of containerized quantity for bundle parent using component details
   * @param shipmentEle
   * @param quantityMap
   * @param containerDetailEle
   * @param isMultipleContainer
   * @param shipContainerKeyList
   * @param containerKey
   * @return
   */
  private static boolean getQtyToPack(YFCElement shipmentEle, Map<String, Double> quantityMap,
      YFCElement containerDetailEle, boolean isMultipleContainer, List<String> shipContainerKeyList, String containerKey) {

    String shipmentLineKey = containerDetailEle.getAttribute(GCConstants.SHIPMENT_LINE_KEY);
    YFCElement shipmentLineChildEle = containerDetailEle.getChildElement(GCConstants.SHIPMENT_LINE);
    String parentShipLineKey = GCConstants.BLANK_STRING;
    double parentQty = 0;
    if (shipmentLineChildEle.hasAttribute(GCConstants.PARENT_SHPMNTLINE_KEY)) {
      parentShipLineKey = shipmentLineChildEle.getAttribute(GCConstants.PARENT_SHPMNTLINE_KEY);
      LOGGER.verbose("ParentShipLineKey :" + parentShipLineKey);
    }

    if (!YFCCommon.isVoid(parentShipLineKey)) {

      YFCElement shipmentLinesEle = shipmentEle.getChildElement(GCConstants.SHIPMENT_LINES);
      YFCIterable<YFCElement> shipmentLineList = shipmentLinesEle.getChildren();
      for (YFCElement shipmentLine : shipmentLineList) {

        if (YFCUtils.equals(parentShipLineKey, shipmentLine.getAttribute(GCConstants.SHIPMENT_LINE_KEY))) {
          parentQty = shipmentLine.getDoubleAttribute(GCConstants.QUANTITY);
        }

      }

      double compPackQty = containerDetailEle.getDoubleAttribute(GCConstants.QUANTITY);
      double compQty = shipmentLineChildEle.getDoubleAttribute(GCConstants.QUANTITY);
      double quantity = parentQty * compPackQty / compQty;
      LOGGER.verbose("Parent Qty :" + parentQty);
      LOGGER.verbose("Comp Qty :" + compQty);
      LOGGER.verbose("CompPackQty :" + compPackQty);
      LOGGER.verbose("Quantity :" + quantity);
      if (quantityMap.containsKey(parentShipLineKey) && isMultipleContainer
          && !shipContainerKeyList.contains(containerKey)) {
        double sum = quantityMap.get(parentShipLineKey);
        sum += quantity;
        quantityMap.put(parentShipLineKey, sum);
        shipContainerKeyList.add(containerKey);
      } else {
        quantityMap.put(parentShipLineKey, quantity);
        isMultipleContainer = false;
        if (!shipContainerKeyList.contains(containerKey)) {
          shipContainerKeyList.add(containerKey);
        }
      }

    }

    double quantity = containerDetailEle.getDoubleAttribute(GCConstants.QUANTITY);
    LOGGER.verbose("Quantity :" + quantity);
    if (quantityMap.containsKey(shipmentLineKey)) {
      double sum = quantityMap.get(shipmentLineKey);
      sum += quantity;
      quantityMap.put(shipmentLineKey, sum);
    } else {
      quantityMap.put(shipmentLineKey, quantity);
    }


    return isMultipleContainer;

  }

  /**
   * Calculate Unit Promotion on a item.
   * @param orderLineEle
   * @param quantity
   * @param totalPromotion
   * @param unitPromotion
   * @return
   */
  public static double calculateUnitPromotion(YFCElement lineChargesEle, double quantity, double totalPromotion,
      double unitPromotion) {

    LOGGER.beginTimer("GCPackDetailsManager.calculateUnitPromotion");
    LOGGER.verbose("GCPackDetailsManager.calculateUnitPromotion -- START");
    YFCIterable<YFCElement> lineChargeList = lineChargesEle.getChildren();
    for(YFCElement lineChargeEle : lineChargeList){
      String chargeCategory = lineChargeEle.getAttribute(GCConstants.CHARGE_CATEGORY);
      LOGGER.verbose("ChargeCategory :" + chargeCategory);
      if(YFCUtils.equals(GCConstants.PROMOTION, chargeCategory)){
        String chargePerLineAttr = lineChargeEle.getAttribute(GCConstants.CHARGE_PER_LINE);
        double chargePerLine = Double.parseDouble(chargePerLineAttr);
        LOGGER.verbose("Line Promotion:" + chargePerLine);
        totalPromotion += chargePerLine;
      }
      LOGGER.verbose("Total Promotion :" + totalPromotion);
      unitPromotion = totalPromotion/quantity ;
      LOGGER.verbose("Unit promotion calculated :" + unitPromotion);
    }
    LOGGER.verbose("GCPackDetailsManager.calculateUnitPromotion -- END");
    LOGGER.endTimer("GCPackDetailsManager.calculateUnitPromotion");
    return unitPromotion;

  }


  /**
   * This method is used to call GCOrderCommissionListService and fetch the User Name from the first
   * GCOrderCommission Element
   *
   * @param env
   * @param orderHeaderKey
   * @return userName
   */
  public String getGCOrderCommissionListService(YFSEnvironment env, String orderHeaderKey) {

    LOGGER.beginTimer("GCPackDetailsManager.getGCOrderCommissionListService");
    LOGGER.verbose("GCPackDetailsManager.getGCOrderCommissionListService--Begin");
    YFCDocument odrCommInputDoc = YFCDocument.createDocument(GCXmlLiterals.ORDER_COMMISSION);
    odrCommInputDoc.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);

    String userName = GCConstants.BLANK_STRING;
    if(LOGGER.isVerboseEnabled()){
      LOGGER.verbose("OrderCommissionList Indoc :" +odrCommInputDoc.toString());
    }
    YFCDocument getODRCommOutdoc =
        GCCommonUtil.invokeService(env, GCConstants.ORDER_COMMISSION_LIST_SERVICE, odrCommInputDoc);
    final YFCElement eleCommissionList = getODRCommOutdoc.getDocumentElement();
    final YFCElement eleOdrCommission = eleCommissionList.getElementsByTagName(GCXmlLiterals.ORDER_COMMISSION).item(0);
    if (!YFCCommon.isVoid(eleOdrCommission)) {
      userName = eleOdrCommission.getAttribute(GCXmlLiterals.USER_NAME);
    }
    LOGGER.verbose("GCPackDetailsManager.getGCOrderCommissionListService--End");
    LOGGER.endTimer("GCPackDetailsManager.getGCOrderCommissionListService");
    return userName;
  }

  /**
   * Calling getCommonCodeList to fetch LevelOfService value
   *
   * @param env
   * @param attribute
   * @return
   */
  public static String callCommonCodeList(YFSEnvironment env, String levelOfService) {

    LOGGER.beginTimer("GCPackDetailsManager.callCommonCodeList");
    LOGGER.verbose("GCPackDetailsManager.callCommonCodeList -- START");
    // preparing input for API call
    final YFCDocument commonCodeinXMLYfc = YFCDocument.createDocument(GCConstants.COMMON_CODE);
    final YFCElement commonCodeEle = commonCodeinXMLYfc.getDocumentElement();
    commonCodeEle.setAttribute(GCConstants.CODE_TYPE, GCConstants.GC_EXSTORE_LOS);
    commonCodeEle.setAttribute(GCConstants.CODE_VALUE, levelOfService);

    LOGGER.verbose("commonCodeinXML : " + commonCodeinXMLYfc.getString());
    final YFCDocument commonCodeTemplate = YFCDocument.getDocumentFor(GCConstants.COMMON_CODE_TEMPLATE);

    // calling getCommonCodeList API
    final YFCDocument outXMLYfc =
        GCCommonUtil.invokeAPI(env, GCConstants.API_GET_COMMON_CODE_LIST, commonCodeinXMLYfc, commonCodeTemplate);


    YFCElement eleOutXml = outXMLYfc.getDocumentElement();
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("outXML : " + outXMLYfc.getString());
    }
    final YFCElement eleCommonCode = eleOutXml.getElementsByTagName(GCConstants.COMMON_CODE).item(0);
    String levelOfServiceDesc = GCConstants.BLANK;
    if (!YFCCommon.isVoid(eleCommonCode)) {
      levelOfServiceDesc = eleCommonCode.getAttribute(GCConstants.CODE_SHORT_DESCRIPTION);
    }
    LOGGER.verbose("GCPackDetailsManager.callCommonCodeList -- END");
    LOGGER.endTimer("GCPackDetailsManager.callCommonCodeList");
    return levelOfServiceDesc;
  }

  /**
   * This method is used to stamp attributes in the custom document.
   *
   * @param eleShipmentOutdoc
   * @param eleShipment
   * @param levelOfServiceDesc
   */
  public static void stampAttributes(YFCElement eleShipmentOutdoc, YFCElement eleShipment, String levelOfServiceDesc) {

    LOGGER.beginTimer("GCPackDetailsManager.stampAttributes");
    LOGGER.verbose("GCPackDetailsManager.stampAttributes--Begin");

    final YFCElement eleShipExtn = eleShipment.getChildElement(GCConstants.EXTN);
    eleShipmentOutdoc.setAttribute(GCXmlLiterals.DELIVERY_METHOD,
        eleShipment.getAttribute(GCXmlLiterals.DELIVERY_METHOD));
    eleShipmentOutdoc.setAttribute(GCConstants.SHIPMENT_KEY, eleShipment.getAttribute(GCConstants.SHIPMENT_KEY));
    eleShipmentOutdoc.setAttribute(GCConstants.SHIPMENT_NO, eleShipment.getAttribute(GCConstants.SHIPMENT_NO));

    eleShipmentOutdoc.setAttribute(GCConstants.FULFILLMENT_TYPE, eleShipExtn.getAttribute(GCConstants.EXTN_FULFILLMENT_TYPE));

    eleShipmentOutdoc.setAttribute(GCConstants.ENTERPRISE_CODE, eleShipment.getAttribute(GCConstants.ENTERPRISE_CODE));
    eleShipmentOutdoc.setAttribute(GCConstants.DOCUMENT_TYPE, eleShipment.getAttribute(GCConstants.DOCUMENT_TYPE));
    eleShipmentOutdoc.setAttribute(GCConstants.LEVEL_OF_SERVICE, levelOfServiceDesc);

    eleShipmentOutdoc
    .setAttribute(GCConstants.ORDER_HEADER_KEY, eleShipment.getAttribute(GCConstants.ORDER_HEADER_KEY));
    eleShipmentOutdoc.setAttribute(GCConstants.STATUS, eleShipment.getAttribute(GCConstants.STATUS));
    eleShipmentOutdoc.setAttribute(GCConstants.SHIP_DATE, eleShipment.getAttribute(GCConstants.SHIP_DATE));
    eleShipmentOutdoc.setAttribute(GCXmlLiterals.RQSTD_SHIP_DATE,
        eleShipment.getAttribute(GCXmlLiterals.RQSTD_SHIP_DATE));
    eleShipmentOutdoc.setAttribute(GCXmlLiterals.EXPCTD_DELIVERY_DATE,
        eleShipment.getAttribute(GCXmlLiterals.EXPCTD_DELIVERY_DATE));
    eleShipmentOutdoc.importNode(eleShipment.getChildElement(GCXmlLiterals.TO_ADDRESS));
    eleShipmentOutdoc.importNode(eleShipment.getChildElement(GCXmlLiterals.BILL_TO_ADDRESS));
    eleShipmentOutdoc.importNode(eleShipment.getChildElement(GCConstants.EXTN));
    eleShipmentOutdoc.importNode(eleShipment.getChildElement(GCConstants.SHIP_NODE));

    final YFCElement eleShipLines = eleShipment.getChildElement(GCConstants.SHIPMENT_LINES);
    final YFCNodeList<YFCElement> listShipmentLine = eleShipLines.getElementsByTagName(GCConstants.SHIPMENT_LINE);
    // Fetching Order/Extn Element to stamp orderingStore value
    final YFCElement eleShipLineOrder = listShipmentLine.item(0).getChildElement(GCConstants.ORDER);
    if(!YFCCommon.isVoid(eleShipLineOrder)){
      final YFCElement eleShipLineOrderExtn = eleShipLineOrder.getChildElement(GCConstants.EXTN);
      eleShipmentOutdoc.setAttribute(GCConstants.ORDERING_STORE,
          eleShipLineOrderExtn.getAttribute(GCConstants.EXTN_ORDERING_STORE));
    }

    LOGGER.verbose("GCPackDetailsManager.stampAttributes--End");
    LOGGER.endTimer("GCPackDetailsManager.stampAttributes");
  }

  /**
   *
   */
  @Override
  public void setProperties(Properties arg0) {


  }



}
