/**
 * Copyright 2014 Guitar Center. All rights reserved. Guitar Center PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 *
 * Change Log
 * ############################################################################################################################################################################
 *       OBJECTIVE: Foundation Class
 * ############################################################################################################################################################################
 *          Version                 Date                        Modified By                                         Description
 * ############################################################################################################################################################################
 *           1.0                 01/16/2015                  Rangarajan,Shwetha             The class contains logic to display the required shipment details
 *                                                                                          in case of both store picked and DC fulfilled store return orders.
 * ############################################################################################################################################################################
 */
package com.gc.exstore.api.packing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * The class contains logic to display the required shipment details in case of both store picked and DC fulfilled store return orders.
 * @author <a href="mailto:shwetha.rangarajan@expicient.com">Shwetha</a>
 *
 */
public class GCPackDetailsShipListManager implements YIFCustomApi{

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCPackDetailsShipListManager.class);

  /**
   * The main method which invokes getShipmentList API to fetch shipment details and
   * modify it for required additional details to be displayed on the exstore UI.
   * @param env
   * @param inputDoc
   * @return
   */
  public static Document preparePackListDoc (YFSEnvironment env, Document inputDoc) {

    LOGGER.beginTimer("GCPackDetailsShipListManager.preparePackListDoc");
    LOGGER.verbose("GCPackDetailsShipListManager.preparePackListDoc -- Begin");
    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    YFCElement rootInEle = inDoc.getDocumentElement();
    String isShipBackToDC = rootInEle.getAttribute(GCConstants.IS_SHIP_BACK_TO_DC);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Indoc :" + inDoc.toString());
    }
    YFCDocument getShpListTemp = YFCDocument.getDocumentFor(GCConstants.GET_SHIPMENT_LIST_TEMP);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Template :" + getShpListTemp.toString());
    }

    YFCDocument getShipmentList;
    if(YFCUtils.equals(GCConstants.YES, isShipBackToDC)){
      YFCDocument getShipListIndoc = YFCDocument.getDocumentFor(GCConstants.GET_SHIP_LIST_INPUT);
      YFCElement shipInEle = getShipListIndoc.getDocumentElement();
      shipInEle.setAttribute(GCConstants.SHIP_NODE, rootInEle.getAttribute(GCConstants.SHIP_NODE));
      getShipmentList = GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, getShipListIndoc, getShpListTemp);
    } else {
      getShipmentList = GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, inDoc, getShpListTemp);
    }

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getShipmentList Output :" + getShipmentList.toString());
    }
    YFCElement shipmentsEle = getShipmentList.getDocumentElement();
    YFCElement shipmentEle = shipmentsEle.getChildElement(GCConstants.SHIPMENT);

    // Started preparing a custom doc
    final YFCDocument finalDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    final YFCElement shipOutEle = finalDoc.getDocumentElement();

    final String levelOfServiceDesc = GCPackDetailsManager.callCommonCodeList(env, shipmentEle.getAttribute(GCConstants.LEVEL_OF_SERVICE));
    // Stamping Attributes
    GCPackDetailsManager.stampAttributes(shipOutEle, shipmentEle, levelOfServiceDesc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("After stamping attributes :" + finalDoc.toString());
    }
    List<String> warrantyLineList = new ArrayList<String>();
    Map<String, List<String>> itemDetailsMap = new HashMap<String, List<String>>();
    List<String> giftItemParentList = new ArrayList<String>();
    Map<String, String> bundleItemList = new HashMap<String, String>();
    Map<String, Double> quantityMap = new HashMap<String, Double>();
    GCPackDetailsManager.calculatePackedQty(shipmentEle, quantityMap);
    LOGGER.verbose("QuantityMap :" + quantityMap.toString());

    YFCElement shipmentLinesOutEle = shipOutEle.createChild(GCConstants.SHIPMENT_LINES);
    YFCElement shipmentLinesEle = shipmentEle.getChildElement(GCConstants.SHIPMENT_LINES);
    YFCIterable<YFCElement> shipmentLineList = shipmentLinesEle.getChildren();

    for(YFCElement shipmentLineEle : shipmentLineList){

      prepareItemDetailsMap(env, itemDetailsMap, shipmentLineEle);
      LOGGER.verbose("Test ImageURL :" +shipmentLineEle.getAttribute(GCConstants.IMAGE_URL));
      //Calculate qtyAvailToPack
      double qtyAvailToPack = 0;
      String shipmentLineKey = shipmentLineEle.getAttribute(GCConstants.SHIPMENT_LINE_KEY);
      YFCElement orderLineEle = shipmentLineEle.getChildElement(GCConstants.ORDER_LINE);
      double quantity = shipmentLineEle.getDoubleAttribute(GCConstants.QUANTITY);

      if (quantityMap.containsKey(shipmentLineKey)) {

        Double packedQty = quantityMap.get(shipmentLineKey);
        LOGGER.verbose("Packed Quantity :" + packedQty);
        double shippedQty = shipmentLineEle.getDoubleAttribute(GCConstants.QUANTITY);
        LOGGER.verbose("Shipped Quantity :" + shippedQty);
        qtyAvailToPack = shippedQty - packedQty;
        LOGGER.verbose("Quantity Avail to pack " + qtyAvailToPack);

      } else {
        qtyAvailToPack = shipmentLineEle.getDoubleAttribute(GCConstants.QUANTITY);
      }

      YFCElement extnEle = orderLineEle.getChildElement(GCConstants.EXTN);

      // To stamp return order number and Return VPO Number in case of Transfer Orders
      if(YFCUtils.equals(GCConstants.YES, isShipBackToDC)){
        YFCDocument getOrderListTemp = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_OUT_TEMP);
        String returnOrderHeaderKey = orderLineEle.getAttribute(GCConstants.DERIVED_FROM_ORDER_HEADER_KEY);
        YFCDocument ordListOpDoc = GCCommonUtil.invokeGetOrderListAPI(env, returnOrderHeaderKey, getOrderListTemp);
        YFCElement eleReturnOrderList = ordListOpDoc.getDocumentElement();
        YFCElement eleReturnOrder = eleReturnOrderList.getChildElement(GCConstants.ORDER);
        shipmentLineEle.setAttribute(GCConstants.EXTN_RETURN_ORDER_NO, eleReturnOrder.getAttribute(GCConstants.ORDER_NO));
        shipmentLineEle.setAttribute(GCConstants.EXTN_VPO_REF_NO, extnEle.getAttribute(GCConstants.EXTN_RETURN_VPO_NO));
      }

      String extnActualSerialNo = extnEle.getAttribute(GCConstants.EXTN_ACTUAL_SERIAL_NO);
      String productClass = shipmentLineEle.getAttribute(GCConstants.PRODUCT_CLASS);

      if(YFCUtils.equals(GCConstants.BUNDLE, shipmentLineEle.getAttribute(GCConstants.KIT_CODE)) &&
          YFCUtils.equals(GCConstants.SET, productClass) && quantity!=0){
        LOGGER.verbose("Import the shipment line and stamp attributes for Retail Set parent");
        shipmentLinesOutEle.importNode(shipmentLineEle);
        setShipmentLineAttr(shipmentLinesOutEle, shipmentLineKey, qtyAvailToPack, extnActualSerialNo, true);
        bundleItemList.put(shipmentLineKey, shipmentLineEle.getAttribute(GCConstants.QUANTITY));
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("After appending retail set parent :" + finalDoc.toString());
        }
      } else if(YFCUtils.equals(GCConstants.YES, extnEle.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM))){
        warrantyLineList.add(shipmentLineKey);
      } else if (!YFCUtils.equals(GCConstants.BUNDLE, orderLineEle.getAttribute(GCConstants.KIT_CODE))
          && (YFCUtils.equals(GCConstants.REGULAR, productClass)) && quantity != 0) {
        LOGGER.verbose("Import Regular shipment lines and DC Bundle Components in case of ShipBack to DC");
        shipmentLinesOutEle.importNode(shipmentLineEle);
        setShipmentLineAttr(shipmentLinesOutEle, shipmentLineKey, qtyAvailToPack, extnActualSerialNo, false);
        if(YFCUtils.equals(GCConstants.YES, extnEle.getAttribute(GCConstants.EXTN_IS_FREE_GIFT_ITEM))){
          giftItemParentList.add(orderLineEle.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY));
        }
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("After appending regular and DC bundle component Line :" + finalDoc.toString());
        }
      }

    }

    if (!shipmentLinesOutEle.hasChildNodes() && YFCUtils.equals(GCConstants.YES, isShipBackToDC)) {
      final YFCDocument shipmentDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
      final YFCElement shipmentOutEle = shipmentDoc.getDocumentElement();
      shipmentOutEle.setAttribute("IsShipmentListEmpty", "Y");
      return shipmentDoc.getDocument();
    }
    appendContainerDtls(shipmentEle, shipOutEle, warrantyLineList, itemDetailsMap, giftItemParentList, bundleItemList, isShipBackToDC);

    LOGGER.verbose("GCPackDetailsShipListManager.preparePackListDoc -- END");
    LOGGER.endTimer("GCPackDetailsShipListManager.preparePackListDoc");
    return finalDoc.getDocument();
  }

  /**
   * Set custom attributes to be displayed on the UI at ShipmentLine level.
   * @param shipmentLinesOutEle
   * @param shipmentLineKey
   * @param qtyAvailToPack
   * @param extnActualSerialNo
   * @param isBundle
   */
  private static void setShipmentLineAttr(YFCElement shipmentLinesOutEle, String shipmentLineKey,
      double qtyAvailToPack, String extnActualSerialNo, boolean isBundle) {

    LOGGER.beginTimer("GCPackDetailsShipListManager.setShipmentLineAttr");
    LOGGER.verbose("GCPackDetailsShipListManager.setShipmentLineAttr -- Begin");
    YFCIterable<YFCElement> shipmentLineList = shipmentLinesOutEle.getChildren();
    for(YFCElement shipmentLine : shipmentLineList){
      if(YFCUtils.equals(shipmentLineKey, shipmentLine.getAttribute(GCConstants.SHIPMENT_LINE_KEY))){

        shipmentLine.setAttribute(GCConstants.QTY_AVAIL_TO_PACK, qtyAvailToPack);
        shipmentLine.setAttribute(GCConstants.SERIAL_NO, extnActualSerialNo);
        if(isBundle){
          shipmentLine.setAttribute(GCConstants.KIT_CODE, GCConstants.BUNDLE);
        }
      }

      // To stamp ItemID incase of Shipback to DC
      YFCElement orderLineEle = shipmentLine.getChildElement(GCConstants.ORDER_LINE);
      YFCElement itemDtlsEle = orderLineEle.getChildElement(GCConstants.ITEM_DETAILS);
      YFCElement extnEle = itemDtlsEle.getChildElement(GCConstants.EXTN);
      if (!YFCCommon.isVoid(extnEle)) {
        String extnPOSItemId = extnEle.getAttribute(GCConstants.EXTN_POSITEM_ID);
        if (YFCCommon.isVoid(extnPOSItemId)) {
          extnEle.setAttribute(GCConstants.EXTN_POSITEM_ID, itemDtlsEle.getAttribute(GCConstants.ITEM_ID));
        }
      }
    }
    LOGGER.verbose("GCPackDetailsShipListManager.setShipmentLineAttr -- END");
    LOGGER.endTimer("GCPackDetailsShipListManager.setShipmentLineAttr");
  }

  /**
   *
   *
   * @param shipmentEle
   * @param shipOutEle
   * @param warrantyLineList
   * @param itemDetailsMap
   * @param giftItemParentList
   * @param bundleItemList
   * @param isShipBackToDC
   */
  private static void appendContainerDtls(YFCElement shipmentEle, final YFCElement shipOutEle, List<String> warrantyLineList,
      Map<String, List<String>> itemDetailsMap, List<String> giftItemParentList,
      Map<String, String> bundleItemList, String isShipBackToDC) {

    LOGGER.beginTimer("GCPackDetailsShipListManager.appendContainerDtls");
    LOGGER.verbose("GCPackDetailsShipListManager.appendContainerDtls -- Begin");

    LOGGER.verbose("Stamp Flag for Gift Item");
    LOGGER.verbose("Map formed :" +giftItemParentList.toString());

    //Create a temporary map to store Return Order No and ExtnReturnVPO number in case of transfer orders.
    Map<String, List<String>> returnOrdDtlsMap = new HashMap<String, List<String>>();
    YFCElement shipmentLinesEle = shipOutEle.getChildElement(GCConstants.SHIPMENT_LINES);
    YFCIterable<YFCElement> shipmentLineList = shipmentLinesEle.getChildren();

    for(YFCElement shipmentLineEle : shipmentLineList){

      if(YFCUtils.equals(GCConstants.YES, isShipBackToDC)){
        List<String> returnOrdDetailsList = new ArrayList<String>();
        returnOrdDetailsList.add(shipmentLineEle.getAttribute(GCConstants.EXTN_RETURN_ORDER_NO));
        returnOrdDetailsList.add(shipmentLineEle.getAttribute(GCConstants.EXTN_VPO_REF_NO));
        returnOrdDtlsMap.put(shipmentLineEle.getAttribute(GCConstants.SHIPMENT_LINE_KEY), returnOrdDetailsList);
      }
      YFCElement orderLineEle = shipmentLineEle.getChildElement(GCConstants.ORDER_LINE);
      String orderLineKey = orderLineEle.getAttribute(GCConstants.ORDER_LINE_KEY);
      LOGGER.verbose("OrderLineKey fetched :" +orderLineKey);
      if(giftItemParentList.contains(orderLineKey)){
        LOGGER.verbose("Match found!");
        shipmentLineEle.setAttribute(GCConstants.HAS_GIFT_ITEM, GCConstants.YES);
      }
    }

    YFCElement containersOutEle = shipOutEle.createChild(GCConstants.CONTAINERS);
    YFCElement containersEle = shipmentEle.getChildElement(GCConstants.CONTAINERS);
    LOGGER.verbose("Containers Ele :" + containersEle.toString());
    YFCIterable<YFCElement> containerList = containersEle.getChildren();
    for(YFCElement containerEle : containerList){

      LOGGER.verbose("Container :" + containerEle.toString());
      YFCElement containerOutEle = containersOutEle.createChild(GCConstants.CONTAINER);
      containerOutEle.setAttribute(GCConstants.SHIPMENT_CONTAINER_KEY, containerEle.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY));
      containerOutEle.setAttribute(GCConstants.TRACKING_NO, containerEle.getAttribute(GCConstants.TRACKING_NO));
      containerOutEle.setAttribute(GCConstants.SHIPMENT_KEY, containerEle.getAttribute(GCConstants.SHIPMENT_KEY));
      YFCElement extnInContainerEle = containerEle.getChildElement(GCConstants.EXTN);
      YFCElement extnInContainerOutEle = containerOutEle.createChild(GCConstants.EXTN);
      if (!YFCCommon.isVoid(extnInContainerEle) && !extnInContainerEle.hasAttribute(GCConstants.EXTN_MULTIBOX_GRP_ID)) {
        extnInContainerOutEle.setAttribute(GCConstants.EXTN_MULTIBOX_GRP_ID, GCConstants.BLANK_STRING);
      } else {
        extnInContainerOutEle.setAttribute(GCConstants.EXTN_MULTIBOX_GRP_ID,
            extnInContainerEle.getAttribute(GCConstants.EXTN_MULTIBOX_GRP_ID));
      }
      if (!YFCCommon.isVoid(extnInContainerEle) && !extnInContainerEle.hasAttribute(GCConstants.EXTN_MULTIBOX_DESC)) {
        extnInContainerOutEle.setAttribute(GCConstants.EXTN_MULTIBOX_DESC, GCConstants.BLANK_STRING);
      } else {
        extnInContainerOutEle.setAttribute(GCConstants.EXTN_MULTIBOX_DESC,
            extnInContainerEle.getAttribute(GCConstants.EXTN_MULTIBOX_DESC));
      }
      YFCElement containerDtlsOutEle = containerOutEle.createChild(GCConstants.CONTAINER_DETAILS);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("After appending container details :" + shipOutEle.toString());
      }
      LOGGER.verbose("After appending container details :" +shipOutEle.toString());
      int noOfContainers = 0;
      YFCElement containerDetailsEle = containerEle.getChildElement(GCConstants.CONTAINER_DETAILS);
      YFCIterable<YFCElement> containerDtlList = containerDetailsEle.getChildren();
      List<String> parentShipLineKeyList = new ArrayList<String>();
      for(YFCElement containerDetailEle : containerDtlList){

        String shipmentLineKey = containerDetailEle.getAttribute(GCConstants.SHIPMENT_LINE_KEY);
        if(returnOrdDtlsMap.containsKey(shipmentLineKey)){
          stampReturnDetails(returnOrdDtlsMap, shipmentLineKey, containerDetailEle);
        }
        YFCElement shipmentLineChildEle = containerDetailEle.getChildElement(GCConstants.SHIPMENT_LINE);
        if(!warrantyLineList.contains(shipmentLineKey) && !shipmentLineChildEle.hasAttribute(GCConstants.PARENT_SHPMNTLINE_KEY)){
          LOGGER.verbose("Regular Item!");
          containerDtlsOutEle.importNode(containerDetailEle);
          setContainerDetailAttr(containerDtlsOutEle, shipmentLineKey, itemDetailsMap);
          noOfContainers++;
          if (LOGGER.isVerboseEnabled()) {
            LOGGER.verbose("After appending details for regular items :" + shipOutEle.toString());
          }
        } else if (shipmentLineChildEle.hasAttribute(GCConstants.PARENT_SHPMNTLINE_KEY)){

          if(YFCUtils.equals(GCConstants.REGULAR, shipmentLineChildEle.getAttribute(GCConstants.PRODUCT_CLASS))){

            LOGGER.verbose("Regular Item!");
            containerDtlsOutEle.importNode(containerDetailEle);
            setContainerDetailAttr(containerDtlsOutEle, shipmentLineKey, itemDetailsMap);
            noOfContainers++;
            if (LOGGER.isVerboseEnabled()) {
              LOGGER.verbose("After appending details for regular items :" + shipOutEle.toString());
            }

          } else if(YFCUtils.equals(GCConstants.SET, shipmentLineChildEle.getAttribute(GCConstants.PRODUCT_CLASS))) {

            String shipLineKeyForParent = shipmentLineChildEle.getAttribute(GCConstants.PARENT_SHPMNTLINE_KEY);
            LOGGER.verbose("ParentShipLineKey :" + shipLineKeyForParent);
            LOGGER.verbose("ParentShipmentLineKey :" +shipLineKeyForParent);
            if(!parentShipLineKeyList.contains(shipLineKeyForParent)){
              parentShipLineKeyList.add(shipLineKeyForParent);
              YFCElement containerDtlOutEle = containerDtlsOutEle.createChild(GCConstants.CONTAINER_DETAIL);
              String itemID = containerDetailEle.getAttribute(GCConstants.ITEM_ID);
              String unitOfMeasure = containerDetailEle.getAttribute(GCConstants.UNIT_OF_MEASURE);
              String itemDtl = itemID+GCConstants.HASH_TAG+unitOfMeasure;
              List<String> tempItemList = itemDetailsMap.get(itemDtl);
              LOGGER.verbose("ItemDetails Map :" + itemDetailsMap.toString());
              LOGGER.verbose("ItemList :" + tempItemList.toString());
              containerDtlOutEle.setAttribute(GCConstants.ITEM_ID, tempItemList.get(0));
              containerDtlOutEle.setAttribute(GCConstants.ITEM_BRAND, tempItemList.get(1));
              containerDtlOutEle.setAttribute(GCConstants.ITEM_DESCRIPTION, tempItemList.get(2));
              containerDtlOutEle.setAttribute(GCConstants.IMAGE_URL, tempItemList.get(3));
              noOfContainers++;
              LOGGER.verbose("bundleItemList map :" + bundleItemList.toString());
              String shipQuantityAttr = bundleItemList.get(shipLineKeyForParent);
              double parentQty = Double.parseDouble(shipQuantityAttr);
              double compQty = Double.parseDouble(shipmentLineChildEle.getAttribute(GCConstants.QUANTITY));
              double packQty = Double.parseDouble(containerDetailEle.getAttribute(GCConstants.QUANTITY));

              double quantity = parentQty * packQty / compQty;

              containerDtlOutEle.setAttribute(GCConstants.QUANTITY, "" + quantity);

              //Stamp return details for transfer orders
              if(returnOrdDtlsMap.containsKey(shipLineKeyForParent)){
                stampReturnDetails(returnOrdDtlsMap, shipLineKeyForParent, containerDtlOutEle);
              }
              LOGGER.verbose("Bundle Parent :" + containerDtlOutEle.toString());
              LOGGER.verbose("NoOfContainers :" + noOfContainers);
              if (LOGGER.isVerboseEnabled()) {
                LOGGER.verbose("After appending bundle parent details :" + shipOutEle.toString());
              }
            }

          }

        }

      }

      LOGGER.verbose("NoOfContainers :" + noOfContainers);
      if(noOfContainers!=0){
        LOGGER.verbose("Works fine!");
        containerOutEle.setAttribute(GCConstants.NO_OF_ITEMS, noOfContainers);
      } else {
        LOGGER.verbose("Problem!");
        containersOutEle.removeChild(containerOutEle);
      }

    }

    LOGGER.verbose("GCPackDetailsShipListManager.appendContainerDtls -- END");
    LOGGER.endTimer("GCPackDetailsShipListManager.appendContainerDtls");
  }

  /**
   *
   * @param returnOrdDtlsMap
   * @param shipLineKeyForParent
   * @param containerDtlOutEle
   */
  private static void stampReturnDetails(Map<String, List<String>> returnOrdDtlsMap, String shipLineKeyForParent,
      YFCElement containerDtlOutEle) {
    //Refactored code for Sonar Fix.
    List<String> tempList = returnOrdDtlsMap.get(shipLineKeyForParent);
    containerDtlOutEle.setAttribute(GCConstants.EXTN_RETURN_ORDER_NO, tempList.get(0));
    if(!YFCCommon.isVoid(tempList.get(1))){
      containerDtlOutEle.setAttribute(GCConstants.EXTN_VPO_REF_NO, tempList.get(1));
    }
  }

  private static void setContainerDetailAttr(YFCElement containerDtlsOutEle, String shipmentLineKey,
      Map<String, List<String>> itemDetailsMap) {

    YFCIterable<YFCElement> containerDtlOutList = containerDtlsOutEle.getChildren();
    for (YFCElement containerDtlOutEle : containerDtlOutList) {
      if (YFCUtils.equals(shipmentLineKey, containerDtlOutEle.getAttribute(GCConstants.SHIPMENT_LINE_KEY))) {
        String itemID = containerDtlOutEle.getAttribute(GCConstants.ITEM_ID);
        String unitOfMeasure = containerDtlOutEle.getAttribute(GCConstants.UNIT_OF_MEASURE);
        String itemDtl = itemID + GCConstants.HASH_TAG + unitOfMeasure;
        List<String> tempItemList = itemDetailsMap.get(itemDtl);
        containerDtlOutEle.setAttribute(GCConstants.ITEM_ID, tempItemList.get(0));
        containerDtlOutEle.setAttribute(GCConstants.ITEM_BRAND, tempItemList.get(1));
        containerDtlOutEle.setAttribute(GCConstants.ITEM_DESCRIPTION, tempItemList.get(2));
        containerDtlOutEle.setAttribute(GCConstants.IMAGE_URL, tempItemList.get(3));

      }
      LOGGER.verbose("Regular Item :" + containerDtlOutEle.toString());
    }


  }

  /**
   *
   * @param env
   * @param itemDetailsMap
   * @param shipmentLineEle
   */
  private static void prepareItemDetailsMap(YFSEnvironment env, Map<String, List<String>> itemDetailsMap,
      YFCElement shipmentLineEle) {

    LOGGER.beginTimer("GCPackDetailsShipListManager.prepareItemDetailsMap");
    LOGGER.verbose("GCPackDetailsShipListManager.prepareItemDetailsMap -- Begin");
    String itemID = shipmentLineEle.getAttribute(GCConstants.ITEM_ID);
    String unitOfMeasure = shipmentLineEle.getAttribute(GCConstants.UNIT_OF_MEASURE);

    YFCDocument getItemListTemp = YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LIST_TEMP_PACK);
    if (LOGGER.isVerboseEnabled()){
      LOGGER.verbose("getItemList Temp:" +getItemListTemp.toString());
    }
    YFCDocument getItemListOutdoc =
        GCCommonUtil.getItemList(env, itemID, unitOfMeasure, GCConstants.GCI, true, getItemListTemp);
    LOGGER.verbose("getItemList Outdoc :" + getItemListOutdoc.toString());
    YFCElement itemListEle = getItemListOutdoc.getDocumentElement();
    YFCElement itemEle = itemListEle.getChildElement(GCConstants.ITEM);
    YFCElement primaryInfoEle = itemEle.getChildElement(GCConstants.PRIMARY_INFORMATION);
    YFCElement itemExtnEle = itemEle.getChildElement(GCConstants.EXTN);

    String itemDesc = itemID+GCConstants.HASH_TAG+unitOfMeasure;
    List<String>itemDetailsList = new ArrayList<String>();
    // Changes to stamp ItemID for Shipback to DC
    if (!YFCCommon.isVoid(itemExtnEle.getAttribute(GCConstants.EXTN_POSITEM_ID))) {
      itemDetailsList.add(itemExtnEle.getAttribute(GCConstants.EXTN_POSITEM_ID));
    } else {
      itemDetailsList.add(itemEle.getAttribute(GCConstants.ITEM_ID));
    }
    itemDetailsList.add(itemExtnEle.getAttribute(GCConstants.EXTN_BRAND_NAME));
    itemDetailsList.add(primaryInfoEle.getAttribute(GCConstants.SHORT_DESCRIPTION));
    itemDetailsList.add(primaryInfoEle.getAttribute(GCConstants.IMAGE_LOCATION)+ "/" +primaryInfoEle.getAttribute(GCConstants.IMAGE_ID));

    shipmentLineEle.setAttribute(GCConstants.IMAGE_URL, primaryInfoEle.getAttribute(GCConstants.IMAGE_LOCATION)+ "/" +primaryInfoEle.getAttribute(GCConstants.IMAGE_ID));
    itemDetailsMap.put(itemDesc, itemDetailsList);
    LOGGER.verbose("Item Details Map :" +itemDetailsMap);
    LOGGER.verbose("GCPackDetailsShipListManager.prepareItemDetailsMap -- END");
    LOGGER.endTimer("GCPackDetailsShipListManager.prepareItemDetailsMap");
  }

  /**
   *
   */
  @Override
  public void setProperties(Properties arg0) {


  }



}
