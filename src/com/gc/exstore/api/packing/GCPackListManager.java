package com.gc.exstore.api.packing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * This class is used prepare a document containing sorted Shipment Lines for the Packing list
 * screen based Level Of Service
 *
 * @author karthik.kumaresan
 *
 */
public class GCPackListManager {


  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCPackListManager.class);


  private List<YFCElement> sortedShipmentList = new ArrayList<YFCElement>();
  private List<YFCElement> newLOSShipmentList = new ArrayList<YFCElement>();

  public List<YFCElement> getSortedShipmentList() {
    return sortedShipmentList;
  }

  public void setSortedShipmentList(List<YFCElement> sortedShipmentList) {
    this.sortedShipmentList = sortedShipmentList;
  }

  /**
   * This method invokes the LevelOfService method to return a list of shipment sorted based on
   * level of service
   *
   * @param env YFSEnvironment
   * @param inputDoc Document - The input XML received from the ExStore UI
   * @return Document - The list of Shipments with Status '', ShipNode in sorted order
   * @throws SAXException
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws YFSException
   *
   */
  public Document getPackListByShipmentStatus(YFSEnvironment env, Document inputDoc) {

    LOGGER.beginTimer("PackListManager.getPackListByShipmentStatus");
    LOGGER.verbose("PackListManager.getPackListByShipmentStatus--Begin");

    final YFCDocument finalDoc = YFCDocument.createDocument(GCXmlLiterals.SHIPMENTS);

    // Invoke getShipmentList API to fetch list of shipments
    YFCNodeList<YFCElement> listShipment = getListOfShipments(env, inputDoc);

    int listShipmentlength = listShipment.getLength();
    if (listShipmentlength == 0) {
      return finalDoc.getDocument();
    }

    // create a map that maps level of service in readable form
    Map<String, String> commonmap = readableLosMap(env, listShipment);

    // convert the shipment list into a map with Level Of Service as the key
    Map<String, List<YFCElement>> levelOfServiceMap = createLOSShipmentMap(listShipment, commonmap);

    // Fetch common code 'GC_STORE_PACK_SPEC' and prepare listed sorted on priority
    YFCNodeList<YFCElement> listLevelOfService = levelOfServiceList(env, listShipment);

    // sort shipment based on level of service
    sortedShipmentList = sortShipmentBasedOnLevelOfService(levelOfServiceMap, listLevelOfService);

    final YFCElement shipmentsIndocRootEle = finalDoc.getDocumentElement();
    for (int f = 0; f < sortedShipmentList.size(); f++) {
      shipmentsIndocRootEle.importNode(sortedShipmentList.get(f));
    }

    LOGGER.verbose("PackListManager.getPackListByShipmentStatus--End");
    LOGGER.endTimer("PackListManager.getPackListByShipmentStatus");
    return finalDoc.getDocument();

  }

  /**
   * create a map that maps level of service in readable form
   *
   * @param env
   * @param listShipment
   * @return
   */
  private Map<String, String> readableLosMap(YFSEnvironment env, YFCNodeList<YFCElement> listShipment) {
    Map<String, String> commonmap = new HashMap<String, String>();
    YFCElement shipmentEle = listShipment.item(0);
    String organizationCode = shipmentEle.getAttribute(GCConstants.ENTERPRISE_CODE);
    Document commonCodeOutDocOne =
        GCCommonUtil.getCommonCodeListByTypeSortBy(env, GCConstants.GC_EXSTORE_LOS, organizationCode);
    YFCDocument commonCodeOutDoc = YFCDocument.getDocumentFor(commonCodeOutDocOne);
    YFCElement commonCodeOutDocrootElement = commonCodeOutDoc.getDocumentElement();
    YFCNodeList<YFCElement> listCommonCode = commonCodeOutDocrootElement.getElementsByTagName(GCConstants.COMMON_CODE);
    int k = listCommonCode.getLength();
    for (int l = 0; l < k; l++) {
      YFCElement eleCommonCode = listCommonCode.item(l);
      String strCodeValue = eleCommonCode.getAttribute(GCConstants.CODE_VALUE);
      String strCodeShortDescription = eleCommonCode.getAttribute(GCConstants.CODE_SHORT_DESCRIPTION);
      commonmap.put(strCodeValue, strCodeShortDescription);

    }
    return commonmap;
  }



  /**
   * This method is used to sort the shipment list based on Level Of Service
   *
   * @param levelOfServiceMap NodeList -
   * @param levelOfServicemap
   * @return
   */


  private List<YFCElement> sortShipmentBasedOnLevelOfService(Map<String, List<YFCElement>> levelOfServiceMap,
      YFCNodeList<YFCElement> levelOfServiceList) {

    LOGGER.beginTimer("PackListManager.levelOfServiceSort");
    LOGGER.verbose("PackListManager.levelOfServiceSort--Begin");

    for (YFCElement levelOfService : levelOfServiceList) {

      String strLevelOfService = levelOfService.getAttribute(GCConstants.CODE_LONG_DESCRIPTION);
      if(levelOfServiceMap.containsKey(strLevelOfService)){
        List<YFCElement> shipmentList = levelOfServiceMap.get(strLevelOfService);

        if (!shipmentList.isEmpty()) {
          sortedShipmentList.addAll(shipmentList);
        }
      }
    }

    if (!newLOSShipmentList.isEmpty()) {
      sortedShipmentList.addAll(newLOSShipmentList);
    }
    LOGGER.verbose("PackListManager.levelOfServiceSort--End");
    LOGGER.endTimer("PackListManager.levelOfServiceSort");
    return sortedShipmentList;

  }



  /**
   * this method is to prepare a map of shipments with LOS as the key and value in the Map is List
   * of corresponding Shipment elements and stamp the aging and item description of the first
   * shipment line at the shipment level
   *
   * @param listShipment
   * @param commonmap
   * @return levelOfServiceMap
   */
  private Map<String, List<YFCElement>> createLOSShipmentMap(YFCNodeList<YFCElement> listShipment,
      Map<String, String> commonmap) {


    LOGGER.beginTimer("PackListManager.shipmentMap");
    LOGGER.verbose("PackListManager.shipmentMap--Begin");


    Map<String, List<YFCElement>> levelOfServiceMap = new HashMap<String, List<YFCElement>>();

    for (int j = 0; j < listShipment.getLength(); j++) {

      LOGGER.verbose("Value Of  j" + j);
      YFCElement shipmentEle = listShipment.item(j);
      YFCElement shipLinesEle = shipmentEle.getChildElement(GCConstants.SHIPMENT_LINES);
      YFCElement extnEle = shipmentEle.getChildElement(GCConstants.EXTN);
      YFCElement shipLineEle = shipLinesEle.getElementsByTagName(GCConstants.SHIPMENT_LINE).item(0);

      // Stamping Same Store Pick Up level of service at shipment level
      String shipmentLevelOfService = shipmentEle.getAttribute(GCConstants.LEVEL_OF_SERVICE);

      // Stamping item description at shipment level
      String itemDesc = shipLineEle.getAttribute(GCConstants.ITEM_DESCRIPTION);
      shipmentEle.setAttribute(GCConstants.ITEM_DESCRIPTION, itemDesc);
      // Stamping orderno and orderheaderkey at shipment level(to be removed)
      String orderNumber = shipLineEle.getAttribute(GCConstants.ORDER_NO);
      shipmentEle.setAttribute(GCConstants.ORDER_NO, orderNumber);
      String orderHeaderKey = shipLineEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
      shipmentEle.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
      // Stamping Order creation date
      String extnOrderDate = extnEle.getAttribute(GCConstants.EXTN_ORDER_DATE);
      shipmentEle.setAttribute(GCConstants.ORDERED_ON, extnOrderDate);
      // Stamping ready for pack date
      String extnPackStatusDate = extnEle.getAttribute(GCConstants.EXTN_PACK_STATUS_DATE);
      shipmentEle.setAttribute(GCConstants.QUEUE_DATE, extnPackStatusDate);
      // Setting the IsExpedite flag
      String isExpedite = extnEle.getAttribute(GCConstants.EXTN_EXPEDITE_FLAG);
      shipmentEle.setAttribute(GCConstants.IS_EXPEDITE, isExpedite);
      // Setting IsShipToCustomer flag
      String isShipToCustomer = extnEle.getAttribute(GCConstants.EXTN_FULFILLMENT_TYPE);
      if (YFCUtils.equals(isShipToCustomer, GCConstants.SHIP_2_CUSTOMER)) {
        shipmentEle.setAttribute(GCConstants.IS_SHIP_TO_CUSTOMER, GCConstants.YES);
      } else {
        shipmentEle.setAttribute(GCConstants.IS_SHIP_TO_CUSTOMER, GCConstants.NO);
      }
      // Setting IsShipToStore flag
      String isShipToStore = extnEle.getAttribute(GCConstants.EXTN_PICKUP_STORE_ID);
      String shipNode = shipmentEle.getAttribute(GCConstants.SHIP_NODE);
      if (!YFCCommon.isVoid(isShipToStore) && !YFCUtils.equals(isShipToStore, shipNode)) {
        shipmentEle.setAttribute(GCConstants.IS_SHIP_TO_STORE, GCConstants.YES);
      } else if (YFCUtils.equals(isShipToStore, shipNode)) {
        shipmentEle.setAttribute(GCConstants.IS_SHIP_TO_STORE, GCConstants.NO);
      }
      // Stamping Aging
      String aging =
          GCDateUtils.getTimeDiffrenceInDaysHoursAndMinutes(extnPackStatusDate,
              GCDateUtils.getCurrentTime(GCConstants.DATE_TIME_FORMAT));
      LOGGER.verbose("Value Of  aging" + aging);
      shipmentEle.setAttribute(GCConstants.AGING, aging);

      List<YFCElement> shipmentEleList = levelOfServiceMap.get(shipmentLevelOfService);
      String strMapLevelOfService = commonmap.get(shipmentLevelOfService);

      // Adding logic for consideration of LOS which are not listed in common-code
      if (YFCCommon.isVoid(strMapLevelOfService) || YFCCommon.isVoid(shipmentLevelOfService)) {
        newLOSShipmentList.add(shipmentEle);
        continue;
      }
      // Renaming the LOS in readable form
      shipmentEle.setAttribute(GCConstants.LEVEL_OF_SERVICE, strMapLevelOfService);

      if (shipmentEleList == null) {
        shipmentEleList = new ArrayList<YFCElement>();
        shipmentEleList.add(shipmentEle);
      } else {
        shipmentEleList.add(shipmentEle);
      }
      levelOfServiceMap.put(shipmentLevelOfService, shipmentEleList);

    }

    LOGGER.verbose("PackListManager.shipmentMap--End");
    LOGGER.endTimer("PackListManager.shipmentMap");
    return levelOfServiceMap;
  }



  /**
   * This method is used fetch the Level Of Service from the common codes
   *
   * @param env
   * @param listShipment
   * @return
   * @return Map with Level Of Service in serialized order based on priority
   */

  private YFCNodeList<YFCElement> levelOfServiceList(YFSEnvironment env, YFCNodeList<YFCElement> listShipment) {
    LOGGER.beginTimer("PackListManager.levelOfServiceListCreation");
    LOGGER.verbose("PackListManager.levelOfServiceListCreation--Begin");


    YFCElement shipmentEle = listShipment.item(0);
    String organizationCode = shipmentEle.getAttribute(GCConstants.ENTERPRISE_CODE);
    Document commonCodeOutDocOne =
        GCCommonUtil.getCommonCodeListByTypeSortBy(env, GCConstants.GC_STORE_PACK_SPEC, organizationCode);
    YFCDocument commonCodeOutDoc = YFCDocument.getDocumentFor(commonCodeOutDocOne);


    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("commonCodeOutDoc Document: " + commonCodeOutDoc.toString());
    }

    YFCElement getcommonCodeOutDocRootele = commonCodeOutDoc.getDocumentElement();
    YFCNodeList<YFCElement> listLevelOfService =
        getcommonCodeOutDocRootele.getElementsByTagName(GCConstants.COMMON_CODE);



    LOGGER.verbose("PackListManager.levelOfServiceListCreation--End");
    LOGGER.endTimer("PackListManager.levelOfServiceListCreation");

    return listLevelOfService;
  }



  /**
   * This method is used to get the list of shipments in time(OrderHeaderKey) sorted manner which
   * are in the following statuses a. Ready for packing b. Being packed c. Packed
   *
   * @param env YFSEnvironment
   * @param inputDoc Document - The input XML received from the ExStore UI
   * @return Shipment list
   */

  private YFCNodeList<YFCElement> getListOfShipments(YFSEnvironment env, Document inputDoc) {

    LOGGER.beginTimer("PackListManager.getShipmentList");
    LOGGER.verbose("PackListManager.getShipmentList--Begin");

    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    YFCElement inDocRootEle = inDoc.getDocumentElement();
    String shipNode = inDocRootEle.getAttribute(GCConstants.SHIP_NODE);

    // getShipmentList inDoc preparation
    YFCDocument getshipmentInDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement getshipmentInDocRootEle = getshipmentInDoc.getDocumentElement();

    getshipmentInDocRootEle.setAttribute(GCConstants.SHIP_NODE, shipNode);
    YFCElement complexQueryEle = getshipmentInDoc.createElement(GCConstants.COMPLEX_QUERY);
    getshipmentInDocRootEle.appendChild(complexQueryEle);
    complexQueryEle.setAttribute(GCConstants.OPERATOR, GCConstants.AND);

    YFCElement orderByEle = getshipmentInDoc.createElement(GCConstants.ORDER_BY);
    getshipmentInDocRootEle.appendChild(orderByEle);
    YFCElement attributeEle = getshipmentInDoc.createElement(GCConstants.ATTRIBUTE);
    orderByEle.appendChild(attributeEle);
    attributeEle.setAttribute(GCConstants.DESC, GCConstants.N);
    attributeEle.setAttribute(GCConstants.NAME, GCConstants.ORDER_HEADER_KEY);

    YFCElement andEle = getshipmentInDoc.createElement(GCConstants.AND1);
    complexQueryEle.appendChild(andEle);
    YFCElement orEle = getshipmentInDoc.createElement(GCConstants.OR);
    andEle.appendChild(orEle);
    YFCElement expOneEle = getshipmentInDoc.createElement(GCConstants.EXP);
    orEle.appendChild(expOneEle);

    expOneEle.setAttribute(GCConstants.NAME, GCConstants.STATUS);
    expOneEle.setAttribute(GCConstants.VALUE, GCConstants.READY_FOR_PACKING);
    expOneEle.setAttribute(GCConstants.QRY_TYPE, GCConstants.EQ);
    YFCElement expTwoEle = getshipmentInDoc.createElement(GCConstants.EXP);
    orEle.appendChild(expTwoEle);

    expTwoEle.setAttribute(GCConstants.NAME, GCConstants.STATUS);
    expTwoEle.setAttribute(GCConstants.VALUE, GCConstants.BEING_PACKED);
    expTwoEle.setAttribute(GCConstants.QRY_TYPE, GCConstants.EQ);
    YFCElement expThreeEle = getshipmentInDoc.createElement(GCConstants.EXP);
    orEle.appendChild(expThreeEle);

    expThreeEle.setAttribute(GCConstants.NAME, GCConstants.STATUS);
    expThreeEle.setAttribute(GCConstants.VALUE, GCConstants.PACKED);
    expThreeEle.setAttribute(GCConstants.QRY_TYPE, GCConstants.EQ);



    YFCDocument getshipmentTempDoc = YFCDocument.getDocumentFor(GCConstants.GET_SHIP_LIST_TEMPLATE_PACK_LIST);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getShipListOutdoc Document: " + getshipmentInDoc.toString());
    }

    YFCDocument getshipmentOutDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, getshipmentInDoc, getshipmentTempDoc);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getShipListOutdoc Document: " + getshipmentOutDoc.toString());
    }

    YFCElement getshipmentOutDocRootele = getshipmentOutDoc.getDocumentElement();
    YFCNodeList<YFCElement> listShipment = getshipmentOutDocRootele.getElementsByTagName(GCConstants.SHIPMENT);

    LOGGER.verbose("PackListManager.getShipmentList--End");
    LOGGER.endTimer("PackListManager.getShipmentList");

    return listShipment;
  }



}
