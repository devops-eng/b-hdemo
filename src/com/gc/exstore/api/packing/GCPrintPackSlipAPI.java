package com.gc.exstore.api.packing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.exstore.api.picking.GCPickDetailAPI;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class is used to prepare the input Document for Pack Slip XSL.
 *
 * @author shanil.sharma
 *
 */
public class GCPrintPackSlipAPI {

  /**
   * This is the main method which calls different methods to prapare a final input doc for Pack
   * Slip XSL.
   *
   * @param env
   * @param inDocument
   * @return
   */

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCPrintPackSlipAPI.class.getName());

  public Document printPackSlip(YFSEnvironment env, Document inDocument) {

    LOGGER.beginTimer("GCPrintPackSlipAPI.printPackSlip");
    LOGGER.verbose("GCPrintPackSlipAPI.printPackSlip--Begin");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("inDocument : " + YFCDocument.getDocumentFor(inDocument).toString());
    }

    final String indocShipmentKey = inDocument.getDocumentElement().getAttribute(GCConstants.SHIPMENT_KEY);
    final YFCDocument sortedShipmentOutDoc = callSortedShipmentDetailAPI(env, indocShipmentKey);
    final YFCElement eleSortedShipment = sortedShipmentOutDoc.getDocumentElement();

    // Fetch DocumentType to check for Purchase Orders.
    final String documentType = eleSortedShipment.getAttribute(GCConstants.DOCUMENT_TYPE);
    final String orderNo = eleSortedShipment.getAttribute(GCConstants.ORDER_NO);
    final YFCElement eleSortedContainers = eleSortedShipment.getChildElement(GCConstants.CONTAINERS);
    final YFCNodeList<YFCElement> nlSortedContainer = eleSortedContainers.getElementsByTagName(GCConstants.CONTAINER);

    final YFCElement eleSortedShipmentLines = eleSortedShipment.getChildElement(GCConstants.SHIPMENT_LINES);
    final YFCNodeList<YFCElement> nlSortedShipmentLine =
        eleSortedShipmentLines.getElementsByTagName(GCConstants.SHIPMENT_LINE);

    // multiBox map, Key : ExtnMultiBoxID Value : List of ContainerDetail Elements.
    Map<String, List<YFCElement>> multiBoxMap = new HashMap<String, List<YFCElement>>();

    // bundleParentMap, Key : ShipmentLineKey Value : List of Quantity, ItemID, ItemDesc.
    Map<String, List<String>> bundleParentMap = new HashMap<String, List<String>>();

    // returnDetailsMap, Key : ShipmentLineKey Value : ExtnReturnVPONo
    Map<String, String> returnDetailsMap = new HashMap<String, String>();

    prepareMultiBoxMap(multiBoxMap, nlSortedContainer);
    prepareBundleParentMap(bundleParentMap, returnDetailsMap, documentType, nlSortedShipmentLine);
    LOGGER.verbose("bundleParentMap after: " + bundleParentMap.toString());
    final YFCDocument pdfDoc =
        preparePDFDoc(env, nlSortedContainer, multiBoxMap, bundleParentMap, orderNo, eleSortedShipment,
            returnDetailsMap);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("pdfDoc : " + pdfDoc.toString());
    }
    if (YFCUtils.equals(GCConstants.FLAG_Y, inDocument.getDocumentElement().getAttribute("IsReportPrintPackSLip"))) {
      LOGGER.verbose("GCReportprintPackSlip--Start");
      final YFCDocument reportpdfDoc = YFCDocument.createDocument(GCConstants.CONTAINERS);
      final YFCElement reportOutDocContainers = reportpdfDoc.getDocumentElement();
      final YFCElement elePDF = pdfDoc.getDocumentElement();
      final YFCNodeList<YFCElement> listContainer = elePDF.getElementsByTagName(GCConstants.CONTAINER);
      for (YFCElement elemContainer : listContainer) {
        String trackingNo = elemContainer.getAttribute(GCConstants.TRACKING_NO);
        if (YFCUtils.equals(trackingNo, inDocument.getDocumentElement().getAttribute(GCConstants.TRACKING_NO))) {
          reportOutDocContainers.importNode(elemContainer);
          break;
        }

      }
      return reportpdfDoc.getDocument();
    }
    LOGGER.verbose("GCPrintPackSlipAPI.printPackSlip--End");
    LOGGER.endTimer("GCPrintPackSlipAPI.printPackSlip");

    return pdfDoc.getDocument();
  }

  /**
   * This method is used to prepare input Document for Pack Slip XSL.
   *
   * @param env
   *
   * @param nlSortedContainer
   * @param multiBoxMap
   * @param bundleParentMap
   * @param orderNo
   * @param eleShipNode
   * @return
   */
  private YFCDocument preparePDFDoc(YFSEnvironment env, YFCNodeList<YFCElement> nlSortedContainer,
      Map<String, List<YFCElement>> multiBoxMap, Map<String, List<String>> bundleParentMap, String orderNo,
      YFCElement eleSortedShipment, Map<String, String> returnDetailsMap) {

    LOGGER.beginTimer("GCPrintPackSlipAPI.preparePDFDoc");
    LOGGER.verbose("GCPrintPackSlipAPI.preparePDFDoc--Begin");
    final YFCDocument pdfDoc = YFCDocument.createDocument(GCConstants.CONTAINERS);
    final YFCElement outDocContainers = pdfDoc.getDocumentElement();

    for (YFCElement eleSortedContainer : nlSortedContainer) {
      final YFCElement eleSortedContainerExtn = eleSortedContainer.getChildElement(GCConstants.EXTN);
      final String multiBoxID = eleSortedContainerExtn.getAttribute(GCConstants.EXTN_MULTI_BOX_GROUP_ID);
      List<String> finalShipLineKeyList = new ArrayList<String>();
      final YFCElement eleSortedCntrDtls = eleSortedContainer.getChildElement(GCConstants.CONTAINER_DETAILS);
      final YFCNodeList<YFCElement> nlSortedCntrDtl =
          eleSortedCntrDtls.getElementsByTagName(GCConstants.CONTAINER_DETAIL);

      final YFCElement outDocContainer = outDocContainers.createChild(GCConstants.CONTAINER);
      final YFCElement outDocContainerDtls = outDocContainer.createChild(GCConstants.CONTAINER_DETAILS);
      for (YFCElement eleSortedCntrDtl : nlSortedCntrDtl) {

        final YFCElement eleSortedShipLine = eleSortedCntrDtl.getChildElement(GCConstants.SHIPMENT_LINE);
        final String parentShipLineKey = eleSortedShipLine.getAttribute(GCConstants.PARENT_SHPMNTLINE_KEY);
        String shipmentLineKey = eleSortedShipLine.getAttribute(GCConstants.SHIPMENT_LINE_KEY);
        // Stamp ExtnReturnVPO# at ContainerDetail level
        if (returnDetailsMap.containsKey(shipmentLineKey)) {
          eleSortedCntrDtl.setAttribute(GCConstants.EXTN_RETURN_VPO_NO, returnDetailsMap.get(shipmentLineKey));
        }

        // Bundle Parent Logic
        if (!YFCCommon.isVoid(parentShipLineKey) && !finalShipLineKeyList.contains(parentShipLineKey)) {

          stampPdfAttributes(env, eleSortedShipment, outDocContainer);
          outDocContainer.setAttribute(GCConstants.TRACKING_NO,
              eleSortedContainer.getAttribute(GCConstants.TRACKING_NO));
          outDocContainer.setAttribute(GCConstants.ORDER_NO, orderNo);
          outDocContainer.setAttribute(GCConstants.EXTN_MULTIBOX_DESC,
              eleSortedContainerExtn.getAttribute(GCConstants.EXTN_MULTIBOX_DESC));

          final YFCElement outDocContainerDtl = outDocContainerDtls.createChild(GCConstants.CONTAINER_DETAIL);
          final List<String> itemDtlList = bundleParentMap.get(parentShipLineKey);
          int parentQty = Float.valueOf(itemDtlList.get(0)).intValue();
          int compOriginalQty = Float.valueOf(eleSortedShipLine.getAttribute(GCConstants.QUANTITY)).intValue();
          int compPackedQty = Float.valueOf(eleSortedCntrDtl.getAttribute(GCConstants.QUANTITY)).intValue();
          int finalQty = (compPackedQty * parentQty) / compOriginalQty;
          outDocContainerDtl.setAttribute(GCConstants.QUANTITY, finalQty);
          outDocContainerDtl.setAttribute(GCConstants.ITEM_ID, itemDtlList.get(1));

          // Stamping ItemDescription at ShipmentLine Level
          final YFCElement outDocCtnrDtlShipLine = outDocContainerDtl.createChild(GCConstants.SHIPMENT_LINE);
          outDocCtnrDtlShipLine.setAttribute(GCConstants.ITEM_DESCRIPTION, itemDtlList.get(2));

          // Stamp ExtnReturnVPO# on Bundle Parent Line.
          if (returnDetailsMap.containsKey(parentShipLineKey)) {
            outDocContainerDtl.setAttribute(GCConstants.EXTN_RETURN_VPO_NO, returnDetailsMap.get(parentShipLineKey));
          }
          finalShipLineKeyList.add(parentShipLineKey);
        } else if (!YFCCommon.isVoid(multiBoxID)
            && 0 == Float.valueOf(eleSortedCntrDtl.getAttribute(GCConstants.QUANTITY)).intValue()) {

          eleSortedCntrDtl.setAttribute(GCConstants.QUANTITY, "1");
          List<YFCElement> containerDtlList = multiBoxMap.get(multiBoxID);
          stampPdfAttributes(env, eleSortedShipment, outDocContainer);
          outDocContainer.setAttribute(GCConstants.EXTN_MULTIBOX_DESC,
              eleSortedContainerExtn.getAttribute(GCConstants.EXTN_MULTIBOX_DESC));

          outDocContainer.setAttribute(GCConstants.TRACKING_NO,
              eleSortedContainer.getAttribute(GCConstants.TRACKING_NO));
          outDocContainer.setAttribute(GCConstants.ORDER_NO, orderNo);
          for (YFCElement eleContainerDtl : containerDtlList) {
            outDocContainerDtls.importNode(eleContainerDtl);
          }

          // fetching ItemDescription from ShipmentLine/OrderLine/ItemDetails/PrimaryInformation
          final YFCElement eleSortedShipmentLine = eleSortedCntrDtl.getChildElement(GCConstants.SHIPMENT_LINE);
          final YFCElement eleSortedOdrLine = eleSortedShipmentLine.getChildElement(GCConstants.ORDER_LINE);
          final YFCElement eleSortedItemDetails = eleSortedOdrLine.getChildElement(GCConstants.ITEM_DETAILS);
          final YFCElement eleSortedPrimaryInfo = eleSortedItemDetails.getChildElement(GCConstants.PRIMARY_INFORMATION);
          eleSortedShipmentLine.setAttribute(GCConstants.ITEM_DESCRIPTION,
              eleSortedPrimaryInfo.getAttribute(GCConstants.SHORT_DESCRIPTION));

          outDocContainerDtls.importNode(eleSortedCntrDtl);
        } else if (YFCCommon.isVoid(parentShipLineKey)) {
          stampPdfAttributes(env, eleSortedShipment, outDocContainer);
          outDocContainer.setAttribute(GCConstants.EXTN_MULTIBOX_DESC,
              eleSortedContainerExtn.getAttribute(GCConstants.EXTN_MULTIBOX_DESC));

          outDocContainer.setAttribute(GCConstants.TRACKING_NO,
              eleSortedContainer.getAttribute(GCConstants.TRACKING_NO));
          outDocContainer.setAttribute(GCConstants.ORDER_NO, orderNo);

          // fetching ItemDescription from ShipmentLine/OrderLine/ItemDetails/PrimaryInformation
          final YFCElement eleSortedShipmentLine = eleSortedCntrDtl.getChildElement(GCConstants.SHIPMENT_LINE);
          final YFCElement eleSortedOdrLine = eleSortedShipmentLine.getChildElement(GCConstants.ORDER_LINE);
          final YFCElement eleSortedItemDetails = eleSortedOdrLine.getChildElement(GCConstants.ITEM_DETAILS);
          final YFCElement eleSortedPrimaryInfo = eleSortedItemDetails.getChildElement(GCConstants.PRIMARY_INFORMATION);
          eleSortedShipmentLine.setAttribute(GCConstants.ITEM_DESCRIPTION,
              eleSortedPrimaryInfo.getAttribute(GCConstants.SHORT_DESCRIPTION));

          outDocContainerDtls.importNode(eleSortedCntrDtl);
        }
      }
    }
    LOGGER.verbose("GCPrintPackSlipAPI.preparePDFDoc--End");
    LOGGER.endTimer("GCPrintPackSlipAPI.preparePDFDoc");
    return pdfDoc;
  }

  /**
   * To stamp few attributes for Pack Slip PDF.
   *
   * @param env
   * @param eleSortedShipment
   * @param outDocContainer
   */
  private void stampPdfAttributes(YFSEnvironment env, YFCElement eleSortedShipment, YFCElement outDocContainer) {

    LOGGER.beginTimer("GCPrintPackSlipAPI.stampPdfAttributes");
    LOGGER.verbose("GCPrintPackSlipAPI.stampPdfAttributes--Begin");
    // Fetching attributes from Shipment level & shipment Extn Level
    final YFCElement eleShipNode = eleSortedShipment.getChildElement(GCConstants.SHIP_NODE);
    final YFCElement eleToAddress = eleSortedShipment.getChildElement(GCConstants.TO_ADDRESS);
    final YFCElement eleSortedShipExtn = eleSortedShipment.getChildElement(GCConstants.EXTN);
    outDocContainer.importNode(eleShipNode);
    outDocContainer.importNode(eleToAddress);
    String orderDate = eleSortedShipExtn.getAttribute(GCConstants.EXTN_ORDER_DATE);
    String shipDate = eleSortedShipment.getAttribute(GCConstants.ACTUAL_SHIPMENT_DATE);
    String orderDate1 = orderDate.substring(0, orderDate.lastIndexOf('T'));
    String shipDate1 = shipDate.substring(0, shipDate.lastIndexOf('T'));


    outDocContainer.setAttribute(GCConstants.ORDER_DATE, orderDate1);
    outDocContainer.setAttribute(GCConstants.ACTUAL_SHIPMENT_DATE, shipDate1);
    outDocContainer.setAttribute(GCConstants.BILL_TO_CUSTOMER_ID,
        eleSortedShipment.getAttribute(GCConstants.BILL_TO_CUSTOMER_ID));

    // Fetching LevelOfService Description from CommonCode & stamp it in order element
    GCPickDetailAPI obj = new GCPickDetailAPI();
    final String levelOfServiceDesc =
        obj.callCommonCodeList(env, eleSortedShipment.getAttribute(GCConstants.LEVEL_OF_SERVICE));
    outDocContainer.setAttribute(GCConstants.LEVEL_OF_SERVICE, levelOfServiceDesc);

    LOGGER.verbose("GCPrintPackSlipAPI.stampPdfAttributes--End");
    LOGGER.endTimer("GCPrintPackSlipAPI.stampPdfAttributes");
  }

  /**
   * This method is used to prepare a Bundle Parent Map.
   *
   * @param bundleParentMap
   * @param documentType
   * @param returnDetailsMap
   * @param nlSortedShipmentLine
   */
  private void prepareBundleParentMap(Map<String, List<String>> bundleParentMap,
      Map<String, String> returnDetailsMap,
      String documentType, YFCNodeList<YFCElement> nlSortedShipmentLine) {
    LOGGER.beginTimer("GCPrintPackSlipAPI.prepareBundleParentMap");
    LOGGER.verbose("GCPrintPackSlipAPI.prepareBundleParentMap--Begin");
    for (YFCElement eleSortedShipmentLine : nlSortedShipmentLine) {
      LOGGER.verbose("eleSortedShipmentLine : " + eleSortedShipmentLine.toString());
      List<String> itemDetailList = new ArrayList<String>();
      if (YFCUtils.equals(GCConstants.BUNDLE, eleSortedShipmentLine.getAttribute(GCConstants.KIT_CODE))) {

        itemDetailList.add(eleSortedShipmentLine.getAttribute(GCConstants.QUANTITY));
        itemDetailList.add(eleSortedShipmentLine.getAttribute(GCConstants.ITEM_ID));

        // fetching ItemDescription from ShipmentLine/OrderLine/ItemDetails/PrimaryInformation
        final YFCElement eleSortedOdrLine = eleSortedShipmentLine.getChildElement(GCConstants.ORDER_LINE);
        final YFCElement eleSortedItemDetails = eleSortedOdrLine.getChildElement(GCConstants.ITEM_DETAILS);
        final YFCElement eleSortedPrimaryInfo = eleSortedItemDetails.getChildElement(GCConstants.PRIMARY_INFORMATION);

        itemDetailList.add(eleSortedPrimaryInfo.getAttribute(GCConstants.SHORT_DESCRIPTION));

        bundleParentMap.put(eleSortedShipmentLine.getAttribute(GCConstants.SHIPMENT_LINE_KEY), itemDetailList);
      }

      // Stamp ExtnReturnVPONo for transfer Orders.
      if (YFCUtils.equals(GCConstants.TRANSFER_ORDER_DOCUMENT_TYPE, documentType)) {
        YFCElement orderLineEle = eleSortedShipmentLine.getChildElement(GCConstants.ORDER_LINE);
        YFCElement extnInOrderEle = orderLineEle.getChildElement(GCConstants.EXTN);
        returnDetailsMap.put(eleSortedShipmentLine.getAttribute(GCConstants.SHIPMENT_LINE_KEY),
            extnInOrderEle.getAttribute(GCConstants.EXTN_RETURN_VPO_NO));
      }
    }
    LOGGER.verbose("bundleParentMap : " + bundleParentMap.toString());
    LOGGER.verbose("GCPrintPackSlipAPI.prepareBundleParentMap--End");
    LOGGER.endTimer("GCPrintPackSlipAPI.prepareBundleParentMap");
  }

  /**
   * This method is used to prepare the Multi Box Map
   *
   * @param multiBoxMap
   * @param nlSortedContainer
   */
  private void prepareMultiBoxMap(Map<String, List<YFCElement>> multiBoxMap,
      YFCNodeList<YFCElement> nlSortedContainer) {
    LOGGER.beginTimer("GCPrintPackSlipAPI.prepareMultiBoxMap");
    LOGGER.verbose("GCPrintPackSlipAPI.prepareMultiBoxMap--Begin");
    List<YFCElement> multiBoxList = new ArrayList<YFCElement>();
    for (YFCElement eleSortedContainer : nlSortedContainer) {
      final YFCElement eleSortedContainerExtn = eleSortedContainer.getChildElement(GCConstants.EXTN);
      final String multiBoxID = eleSortedContainerExtn.getAttribute(GCConstants.EXTN_MULTI_BOX_GROUP_ID);
      if (!YFCCommon.isVoid(multiBoxID)) {

        final YFCElement eleSortedCntrDtls = eleSortedContainer.getChildElement(GCConstants.CONTAINER_DETAILS);
        final YFCNodeList<YFCElement> nlSortedCntrDtl =
            eleSortedCntrDtls.getElementsByTagName(GCConstants.CONTAINER_DETAIL);
        for (YFCElement eleSortedCntrDtl : nlSortedCntrDtl) {

          // fetching ItemDescription from ShipmentLine/OrderLine/ItemDetails/PrimaryInformation
          final YFCElement eleSortedShipmentLine = eleSortedCntrDtl.getChildElement(GCConstants.SHIPMENT_LINE);
          final YFCElement eleSortedOdrLine = eleSortedShipmentLine.getChildElement(GCConstants.ORDER_LINE);
          final YFCElement eleSortedItemDetails = eleSortedOdrLine.getChildElement(GCConstants.ITEM_DETAILS);
          final YFCElement eleSortedPrimaryInfo = eleSortedItemDetails.getChildElement(GCConstants.PRIMARY_INFORMATION);

          final int quantity = Float.valueOf(eleSortedCntrDtl.getAttribute(GCConstants.QUANTITY)).intValue();
          if (quantity == 1) {
            final YFCElement eleSortedShipLine = eleSortedCntrDtl.getChildElement(GCConstants.SHIPMENT_LINE);

            // Setting ItemDesc
            eleSortedShipLine.setAttribute(GCConstants.ITEM_DESCRIPTION,
                eleSortedPrimaryInfo.getAttribute(GCConstants.SHORT_DESCRIPTION));
            final YFCElement eleSortedOrderLine = eleSortedShipLine.getChildElement(GCConstants.ORDER_LINE);
            final YFCElement eleSortedOdrLineExtn = eleSortedOrderLine.getChildElement(GCConstants.EXTN);
            if (YFCUtils.equals(GCConstants.FLAG_Y, eleSortedOdrLineExtn.getAttribute(GCConstants.IS_WARRANTY))) {
              multiBoxList.add(eleSortedCntrDtl);
            }
          }
        }
        multiBoxMap.put(multiBoxID, multiBoxList);
      }
    }
    LOGGER.verbose("GCPrintPackSlipAPI.prepareMultiBoxMap--End");
    LOGGER.endTimer("GCPrintPackSlipAPI.prepareMultiBoxMap");
  }

  /**
   * This method is used to call getSortedShipmentDetails API.
   *
   * @param env
   * @param indocShipmentKey
   * @return
   */
  private YFCDocument callSortedShipmentDetailAPI(YFSEnvironment env, String indocShipmentKey) {
    LOGGER.beginTimer("GCPrintPackSlipAPI.callSortedShipmentDetailAPI");
    LOGGER.verbose("GCPrintPackSlipAPI.callSortedShipmentDetailAPI--Begin");
    final YFCDocument sortedShipmentInDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    final YFCElement eleSortedIndocShipment = sortedShipmentInDoc.getDocumentElement();
    eleSortedIndocShipment.setAttribute(GCConstants.SHIPMENT_KEY, indocShipmentKey);

    YFCDocument tempdoc = YFCDocument.getDocumentFor(GCConstants.GET_SORTED_SHIP_DTL_TEMPLATE);
    final YFCDocument sortedShipmentOutDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.SORTED_SHIPMENT_API, sortedShipmentInDoc, tempdoc);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("sortedShipmentOutDoc : " + sortedShipmentOutDoc);
    }
    LOGGER.verbose("GCPrintPackSlipAPI.callSortedShipmentDetailAPI--End");
    LOGGER.endTimer("GCPrintPackSlipAPI.callSortedShipmentDetailAPI");
    return sortedShipmentOutDoc;
  }
}
