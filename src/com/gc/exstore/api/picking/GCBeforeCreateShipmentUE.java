package com.gc.exstore.api.picking;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.util.YFCUtils;
import com.yantra.ydm.japi.ue.YDMBeforeCreateShipment;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSUserExitException;

/**
 * This class is used to stamp few Extn attributes before the shipment creation
 *
 * @author shanil.sharma
 *
 */
public class GCBeforeCreateShipmentUE implements YDMBeforeCreateShipment {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCBeforeCreateShipmentUE.class);

  @Override
  public Document beforeCreateShipment(YFSEnvironment env, Document inDoc) throws YFSUserExitException {

    LOGGER.beginTimer("BeforeCreateShipmentUE.beforeCreateShipment");
    LOGGER.verbose("BeforeCreateShipmentUE.beforeCreateShipment--Begin");

    LOGGER.verbose("IsStoreShipment :" + env.getTxnObject(GCConstants.IS_STORE_SHIPMENT));

    // Stamping Extn attributes only if the shipment is a store shipment
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("initial Indoc : " + YFCDocument.getDocumentFor(inDoc).toString());
    }
    final YFCElement eleShipment = YFCDocument.getDocumentFor(inDoc).getDocumentElement();
    final YFCElement eleShpLine =
        YFCDocument.getDocumentFor(inDoc).getElementsByTagName(GCConstants.SHIPMENT_LINE).item(0);
    String doctype = null;
    String odrHdrKey = null;
    String odrLineKey = null;
    String primeLineNo = null;
    // if ShipmentLine element is not present then avoiding the whole logic.
    if (!YFCCommon.isVoid(eleShpLine)) {
      doctype = eleShipment.getAttribute(GCConstants.DOCUMENT_TYPE);
      odrHdrKey = eleShpLine.getAttribute(GCConstants.ORDER_HEADER_KEY);

      odrLineKey = eleShpLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      primeLineNo = eleShpLine.getAttribute(GCConstants.PRIME_LINE_NO);

      final YFCDocument getOdrListTmpl = YFCDocument.getDocumentFor((GCConstants.GET_ODR_LIST_TEMPLATE));

      YFCDocument getOdrListOutdoc = null;
      // Calling getOrderList
      if (!YFCCommon.isVoid(odrHdrKey)) {
        getOdrListOutdoc = GCCommonUtil.invokeGetOrderListAPI(env, odrHdrKey, getOdrListTmpl);

      } else if (!YFCCommon.isVoid(eleShpLine.getAttribute(GCConstants.ORDER_NO))) {
        final YFCDocument getOdrLstIndoc = YFCDocument.createDocument(GCConstants.ORDER);
        getOdrLstIndoc.getDocumentElement().setAttribute(GCConstants.ORDER_NO,
            eleShpLine.getAttribute(GCConstants.ORDER_NO));
        getOdrListOutdoc = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOdrLstIndoc, getOdrListTmpl);
      }
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("getOrderList output :" + getOdrListOutdoc.toString());
      }
      if (!YFCCommon.isVoid(getOdrListOutdoc)) {
        final YFCElement eleOrderList = getOdrListOutdoc.getDocumentElement();
        final YFCElement eleOrder = eleOrderList.getChildElement(GCConstants.ORDER);
        if (YFCCommon.isVoid(odrHdrKey)) {
          odrHdrKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
        }

        if (!(YFCUtils.equals(GCConstants.TRANSFER_ORDER_DOCUMENT_TYPE, doctype))) {
          eleShipment.setAttribute(GCConstants.ORDER_HEADER_KEY, odrHdrKey);
        }

        final YFCElement eleOrderExtn = eleOrder.getChildElement(GCConstants.EXTN);
        String pickUpStoreID = eleOrderExtn.getAttribute(GCConstants.EXTN_PICKUP_STORE_ID);

        YFCElement eleExtnShipment = null;
        eleExtnShipment = eleShipment.getChildElement(GCConstants.EXTN);
        if (YFCCommon.isVoid(eleExtnShipment)) {
          // Creating Shipment Extn Element
          eleExtnShipment = eleShipment.createChild(GCConstants.EXTN);
        }
        eleExtnShipment.setAttribute(GCConstants.EXTN_ORDER_DATE, eleOrder.getAttribute(GCConstants.ORDER_DATE));
        eleExtnShipment.setAttribute(GCConstants.EXTN_EXPEDITE_FLAG,
            eleOrderExtn.getAttribute(GCConstants.EXTN_EXPEDITE_FLAG));
        eleExtnShipment.setAttribute(GCConstants.EXTN_PICKUP_STORE_ID, pickUpStoreID);

        final YFCNodeList<YFCElement> nlOrderLine =
            getOdrListOutdoc.getDocumentElement().getElementsByTagName(GCConstants.ORDER_LINE);
        final int lengthOdrLine = nlOrderLine.getLength();
        for (int i = 0; i < lengthOdrLine; i++) {
          final YFCElement eleOrderLine = nlOrderLine.item(i);

          if (YFCUtils.equals(odrLineKey, eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY))
              || YFCUtils.equals(primeLineNo, eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO))) {

            final String fulfillmentType =
                getFulfillmentType(env, eleOrderLine.getAttribute(GCConstants.FULFILLMENT_TYPE));
            eleExtnShipment.setAttribute(GCConstants.EXTN_FULFILLMENT_TYPE, fulfillmentType);
            break;
          }
        }

        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("final Indoc : " + YFCDocument.getDocumentFor(inDoc).toString());
        }
      }
    }
    LOGGER.verbose("BeforeCreateShipmentUE.beforeCreateShipment--End");
    LOGGER.endTimer("BeforeCreateShipmentUE.beforeCreateShipment");
    return inDoc;
  }

  public String getFulfillmentType(YFSEnvironment env, String fulfillmentType) {

    // preparing input for API call
    final YFCDocument commonCodeinXMLYfc = YFCDocument.createDocument(GCConstants.COMMON_CODE);
    final YFCElement commonCodeEle = commonCodeinXMLYfc.getDocumentElement();
    commonCodeEle.setAttribute(GCConstants.CODE_TYPE, GCConstants.GC_EXSTORE_FLMNT_TYPE);
    commonCodeEle.setAttribute(GCConstants.CODE_VALUE, fulfillmentType);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("commonCodeinXML : " + commonCodeinXMLYfc.getString());
    }
    final YFCDocument commonCodeTemplate = YFCDocument.getDocumentFor(GCConstants.COMMON_CODE_TEMPLATE);

    // calling getCommonCodeList API
    final YFCDocument outXMLYfc =
        GCCommonUtil.invokeAPI(env, GCConstants.API_GET_COMMON_CODE_LIST, commonCodeinXMLYfc, commonCodeTemplate);


    YFCElement eleOutXml = outXMLYfc.getDocumentElement();
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("outXML : " + outXMLYfc.getString());
    }
    final YFCElement eleCommonCode = eleOutXml.getElementsByTagName(GCConstants.COMMON_CODE).item(0);
    String fulfillmentTypedesc = GCConstants.BLANK;
    if (!YFCCommon.isVoid(eleCommonCode)) {
      fulfillmentTypedesc = eleCommonCode.getAttribute(GCConstants.CODE_SHORT_DESCRIPTION);
    }

    return fulfillmentTypedesc;
  }
}
