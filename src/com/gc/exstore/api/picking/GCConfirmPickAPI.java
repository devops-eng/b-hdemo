package com.gc.exstore.api.picking;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.exstore.api.packing.GCConfirmShipmentAPI;
import com.gc.exstore.utils.GCShipmentUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * This class is used to perform confirm pick operation once the user clicks on confirm pick button.
 * 1) ChangeShipment call to do backroom pick operation 2) Alternate sourcing in case of partial
 * pick 3) If it is a same store pickup then update the commision table after recalculating it, POS
 * Reservation call confirmShipment & recordConfirmpick 4) Else update the commision table after
 * recalculating it, POS Reservation call changeShipmentStatus & recordConfirmpick
 *
 * @author shanil.sharma
 *
 */
public class GCConfirmPickAPI {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCConfirmPickAPI.class.getName());

  YFCDocument confirmShipOutdoc = null;

  /**
   * This method is used to manage operation on the factor whether it is fullConfirmPick or
   * PartialPick.
   *
   * @param env
   * @param inputDoc
   * @return
   */

  public Document manageBackRoomPickCompletion(YFSEnvironment env, Document inputDoc) {

    LOGGER.beginTimer("GCConfirmPickAPI.manageBackRoomPickCompletion");
    LOGGER.verbose("GCConfirmPickAPI.manageBackRoomPickCompletion--Begin");
    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    final YFCElement eleShipment = inDoc.getDocumentElement();
    eleShipment.setAttribute(GCConstants.DELIVERY_METHOD, GCConstants.SHP);
    final YFCElement eleShipmentExtn = eleShipment.getChildElement(GCConstants.EXTN);
    String orderDate = eleShipmentExtn.getAttribute(GCConstants.EXTN_ORDER_DATE);
    Date ordDate = GCDateUtils.convertDate(orderDate);
    SimpleDateFormat sdf1 = new SimpleDateFormat("MM/dd/yyyy");
    String strOrdDate = sdf1.format(ordDate);
    eleShipment.setAttribute(GCConstants.ORDER_DATE, strOrdDate);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("inDoc#####1 : " + inDoc.toString());
    }
    // Started preparing a input doc for changeShipment call
    final YFCDocument finalDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    final YFCElement eleShipmentIndoc = finalDoc.getDocumentElement();
    eleShipmentIndoc.setAttribute(GCConstants.ACTION, GCConstants.ACTION_MODIFY);
    final String shipmentKey = eleShipment.getAttribute(GCConstants.SHIPMENT_KEY);
    eleShipmentIndoc.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);

    // Started preparing final ChangeShipment & ChangeOrder Doc
    final YFCDocument finalChangeShipDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    final YFCElement elefinalChangeShipment = finalChangeShipDoc.getDocumentElement();
    elefinalChangeShipment.setAttribute(GCConstants.ACTION, GCConstants.ACTION_MODIFY);
    elefinalChangeShipment.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
    final YFCElement elefinalChangeShipLines = elefinalChangeShipment.createChild(GCConstants.SHIPMENT_LINES);

    final YFCDocument finalChgOdrDoc = YFCDocument.createDocument(GCConstants.ORDER);
    final YFCElement eleFinalChgOdr = finalChgOdrDoc.getDocumentElement();
    eleFinalChgOdr.setAttribute(GCConstants.ACTION, GCConstants.ACTION_MODIFY);
    final String orderHeaderKey = eleShipment.getAttribute(GCConstants.ORDER_HEADER_KEY);
    eleFinalChgOdr.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
    final YFCElement elefinalChangeOdrLines = eleFinalChgOdr.createChild(GCConstants.ORDER_LINES);

    YFCElement orderLinesEle = null;
    YFCElement eleOrderLinesStatus = null;
    YFCDocument changeOrderIndoc = null;
    YFCDocument chngOrdStatusIndoc = null;

    boolean isFullConfirm = true;
    boolean isSameStore = false;
    if (YFCUtils.equals(GCConstants.PARTIAL_PICK, eleShipment.getAttribute(GCConstants.ACTION))) {
      eleShipmentIndoc.setAttribute(GCConstants.BACKORDER_RMVD_QTY, GCConstants.FLAG_Y);
      isFullConfirm = false;

      // Start preparing Input Doc for changeOrder call to suppress sourcing
      changeOrderIndoc = YFCDocument.createDocument(GCConstants.ORDER);
      YFCElement orderEle = changeOrderIndoc.getDocumentElement();
      orderEle.setAttribute(GCConstants.ORDER_HEADER_KEY, eleShipment.getAttribute(GCConstants.ORDER_HEADER_KEY));
      orderEle.setAttribute(GCConstants.ENTERPRISE_CODE, eleShipment.getAttribute(GCConstants.ENTERPRISE_CODE));
      orderEle.setAttribute(GCConstants.ACTION, GCConstants.ACTION_MODIFY);
      orderLinesEle = orderEle.createChild(GCConstants.ORDER_LINES);

      // Create changeOrderStatus input document to move partial pick request/pick lines to
      // BackOrderedDAX status.
      chngOrdStatusIndoc = YFCDocument.createDocument(GCConstants.ORDER_STATUS_CHANGE);
      YFCElement ordStatusChngEle = chngOrdStatusIndoc.getDocumentElement();
      ordStatusChngEle.setAttribute(GCConstants.DOCUMENT_TYPE, GCConstants.SALES_ORDER_DOCUMENT_TYPE);
      ordStatusChngEle.setAttribute(GCConstants.ENTERPRISE_CODE, eleShipment.getAttribute(GCConstants.ENTERPRISE_CODE));
      ordStatusChngEle.setAttribute(GCConstants.ORDER_HEADER_KEY,
          eleShipment.getAttribute(GCConstants.ORDER_HEADER_KEY));
      ordStatusChngEle.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.CHANGE_STATUS_CONFIRM);
      eleOrderLinesStatus = ordStatusChngEle.createChild(GCConstants.ORDER_LINES);
    }

    LOGGER.verbose("isFullConfirm######2" + isFullConfirm);
    final YFCElement eleShipLinesIndoc = eleShipmentIndoc.createChild(GCConstants.SHIPMENT_LINES);
    final YFCElement eleShipExtn = eleShipment.getChildElement(GCConstants.EXTN);
    final YFCElement eleShipNode = eleShipment.getChildElement(GCConstants.SHIP_NODE);
    final String shipNode = eleShipNode.getAttribute(GCConstants.SHIP_NODE);
    final String orderNo = eleShipment.getAttribute(GCConstants.ORDER_NO);

    final YFCElement eleShipLines = eleShipment.getChildElement(GCConstants.SHIPMENT_LINES);
    YFCIterable<YFCElement> nlShipmentLine = eleShipLines.getChildren();
    int index = 1;

    // Creating Document for Adjust Inventory API call
    YFCDocument adjustInvDoc = YFCDocument.createDocument(GCConstants.ITEMS_ELE);
    YFCElement eleItems = adjustInvDoc.getDocumentElement();

    for (YFCElement eleShipLine : nlShipmentLine) {

      // stamping current date
      SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
      Date sysDate = new Date();
      String strDate = sdf.format(sysDate);
      eleShipLine.setAttribute(GCConstants.SYSTEM_DATE, strDate);

      // converting Quantity into integer value
      final int qty = Float.valueOf(eleShipLine.getAttribute(GCConstants.QTY_PICKED)).intValue();
      eleShipLine.setAttribute(GCConstants.QTY_PICKED, qty);
      eleShipLine.setAttribute("IndexNo", String.valueOf(index));
      index++;

      //Stamping SerialNo
      final String actualSerialNo = eleShipLine.getAttribute(GCConstants.ACTUAL_SERIAL_NO);
      final String requestedTagNo = eleShipLine.getAttribute(GCConstants.REQ_TAG_NO);
      if(YFCCommon.isVoid(actualSerialNo)){
        eleShipLine.setAttribute(GCConstants.SERIAL_NO, requestedTagNo);
      } else {
        eleShipLine.setAttribute(GCConstants.SERIAL_NO, actualSerialNo);
      }

      if (YFCCommon.isVoid(orderNo)) {
        eleShipment.setAttribute(GCConstants.ORDER_NO, eleShipLine.getAttribute(GCConstants.ORDER_NO));
      }
      // Call prepareInputForbackroompickedQty()
      prepareInputForbackroompickedQty(env, eleShipLine, eleShipLinesIndoc, shipNode, eleItems);

      // stamp attributes for alternate sourcing
      if (!isFullConfirm) {
        prepareChangeOrderIndoc(orderLinesEle, eleShipLine, shipNode, eleOrderLinesStatus);
      }
    }

    // Calling adjustInventory API
    if (eleItems.hasChildNodes()) {
      LOGGER.verbose("adjustInvDoc : " + adjustInvDoc.toString());
      GCCommonUtil.invokeAPI(env, GCConstants.API_ADJUST_INVENTORY, adjustInvDoc.getDocument());
    }
    LOGGER.verbose("finalDoc : " + finalDoc.toString());
    // Call changeShipment API
    YFCDocument tempdoc = YFCDocument.getDocumentFor(GCConstants.CHANGE_SHIP_TEMP);
    GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_SHIPMENT, finalDoc, tempdoc);

    LOGGER.verbose("changeShipment invoked");
    // call manage alternate sourcing
    if (!isFullConfirm) {

      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("changeOrderIndoc############4" + changeOrderIndoc.toString());
        LOGGER.verbose("changeOrderStatusIndoc############" + chngOrdStatusIndoc.toString());
      }
      GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER_STATUS, chngOrdStatusIndoc.getDocument());
      GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, changeOrderIndoc.getDocument());

      // calling Schedule Order for splitiing the orderLine
      YFCDocument scheduleInputDoc = YFCDocument.createDocument(GCConstants.SCHEDULE_ORDER);
      scheduleInputDoc.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
      GCCommonUtil.invokeService(env, GCConstants.SERVICE_SCHEDULE_ORDER, scheduleInputDoc.getDocument());
      LOGGER.verbose("changeOrder Invoked");
    }

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("finalDoc###################3" + finalDoc.toString());
    }

    LOGGER.verbose("API Invoked");
    String posReservID = null;
    if (YFCUtils.equals(eleShipExtn.getAttribute(GCConstants.EXTN_PICKUP_STORE_ID), shipNode)) {
      LOGGER.verbose("Its a same store pickup");
      isSameStore = true;
      posReservID = manageSameStorePickup(env, eleShipment, shipNode);
    } else {
      LOGGER.verbose("Not a same store pickup");
      callChangeShipmentStatusApi(env, shipmentKey, GCConstants.READY_FOR_PACKING_STATUS,
          GCConstants.BCKROOM_PICKED_TRNSCTN);

      callChangeOrderStatus(env, eleShipment, GCConstants.SO_BACKROOM_PICKED_STATUS,
          GCConstants.SO_BCKROOM_PICKED_TRNSCTN);
      GCRecordConfirmPickAction obj = new GCRecordConfirmPickAction();
      final YFCDocument commissionsDoc = obj.updateUserIdAndCommission(env, eleShipment);

      // webservice call for reserving inventory with POS
      YFCDocument requestDocPOS = reserveInventoryPOS(env, commissionsDoc, eleShipment);
      LOGGER.verbose("requestDocPOS : " + requestDocPOS.toString());
      YFCDocument responsePOS = GCCommonUtil.invokeService(env, "GCGetPOSReservationID", requestDocPOS);
      LOGGER.verbose("responsePOS : " + responsePOS.toString());
      YFCElement eleResponse = responsePOS.getDocumentElement();
      if (!YFCCommon.isVoid(eleResponse)) {
        posReservID = eleResponse.getAttribute(GCConstants.RESERVATION_ID);
        LOGGER.verbose("posReservID : " + posReservID);
      } else {
        throw new YFSException("Error connecting POS");
      }

    }

    // Stamping attributes for ChangeShipment call
    for (YFCElement eleShipLine : nlShipmentLine) {
      final YFCElement eleFinalChangeShipLine = elefinalChangeShipLines.createChild(GCConstants.SHIPMENT_LINE);
      final YFCElement eleFinalChgOdrLine = elefinalChangeOdrLines.createChild(GCConstants.ORDER_LINE);
      stampMandatoryAttributes(eleFinalChangeShipLine, eleShipLine, eleFinalChgOdrLine, posReservID);

      YFCElement customComponents = eleShipLine.getChildElement(GCConstants.CUSTOM_COMPONENTS);
      if (!YFCCommon.isVoid(customComponents) && customComponents.hasChildNodes()) {
        YFCIterable<YFCElement> bundleCompList = customComponents.getChildren();
        for (YFCElement shipmentLine : bundleCompList) {
          final YFCElement eleFinalCompChangeShipLine = elefinalChangeShipLines.createChild(GCConstants.SHIPMENT_LINE);
          final YFCElement eleFinalCompChgOdrLine = elefinalChangeOdrLines.createChild(GCConstants.ORDER_LINE);
          stampMandatoryAttributes(eleFinalCompChangeShipLine, shipmentLine, eleFinalCompChgOdrLine, posReservID);
        }
      }

      YFCElement warrantyComponents = eleShipLine.getChildElement(GCConstants.WARRANTY_COMPONENTS);
      if (!YFCCommon.isVoid(warrantyComponents) && warrantyComponents.hasChildNodes()) {
        YFCIterable<YFCElement> warrantyCompList = warrantyComponents.getChildren();
        for (YFCElement warrantyShipmentLine : warrantyCompList) {
          final YFCElement eleFinalWarrantyChangeShipLine =
              elefinalChangeShipLines.createChild(GCConstants.SHIPMENT_LINE);
          final YFCElement eleFinalWarrantyChgOdrLine = elefinalChangeOdrLines.createChild(GCConstants.ORDER_LINE);
          stampMandatoryAttributes(eleFinalWarrantyChangeShipLine, warrantyShipmentLine, eleFinalWarrantyChgOdrLine,
              posReservID);
        }
      }
    }
    //Setting ExtnPackDate
    final YFCElement eleExtnFinalChgShip = elefinalChangeShipment.createChild(GCConstants.EXTN);
    eleExtnFinalChgShip.setAttribute(GCConstants.EXTN_PACK_STATUS_DATE, GCDateUtils.getCurrentTime(GCConstants.TIME_STAMP));

    //Setting ExtnShipDate in case of Same store pickup
    if(YFCUtils.equals(eleShipExtn.getAttribute(GCConstants.EXTN_PICKUP_STORE_ID), shipNode)){
      eleExtnFinalChgShip.setAttribute(GCConstants.EXTN_SHIP_DATE, GCDateUtils.getCurrentTime(GCConstants.TIME_STAMP));
    }

    // call changeShipment API
    YFCDocument templatedoc = YFCDocument.getDocumentFor(GCConstants.CHANGE_SHIP_TEMP);
    GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_SHIPMENT, finalChangeShipDoc, templatedoc);

    // call changeOrder API
    YFCDocument templateChgOdrdoc = YFCDocument.getDocumentFor(GCConstants.CHANGE_ORDER_TEMP);
    GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, finalChgOdrDoc, templateChgOdrdoc);

    if (YFCUtils.equals(eleShipExtn.getAttribute(GCConstants.EXTN_PICKUP_STORE_ID), shipNode)) {
      GCConfirmShipmentAPI invObj = new GCConfirmShipmentAPI();
      invObj.callCreateShipInvoiceAPI(env, eleShipment);
      callChangeShipmentStatusApi(env, shipmentKey, GCConstants.READY_FOR_CUSTOMER_STATUS,
          GCConstants.READY_FOR_CUST_TRNSCTN);
      callChangeOrderStatus(env, eleShipment, GCConstants.SO_READY_FOR_CUST_STATUS,
          GCConstants.SO_READY_FOR_CUST_TRNSCTN);

      // Sending Mail

      try {
        GCCommonUtil.invokeService(env, GCConstants.GC_READY_FOR_CUSTOMER_PICKUP_EMAIL_SERVICE, confirmShipOutdoc);
      } catch (YFCException e) {
        LOGGER.error("creating exception", e);
        final YFCDocument createExceptionDoc = YFCDocument.createDocument(GCConstants.INBOX);
        final YFCElement eleInbox = createExceptionDoc.getDocumentElement();
        eleInbox.setAttribute(GCConstants.ORDER_NO, eleShipment.getAttribute(GCConstants.ORDER_NO));
        eleInbox.setAttribute(GCConstants.SHIPMENT_KEY, eleShipment.getAttribute(GCConstants.SHIPMENT_KEY));
        eleInbox.setAttribute(GCConstants.ENTERPRISE_KEY, eleShipment.getAttribute(GCConstants.ENTERPRISE_CODE));
        eleInbox.setAttribute(GCConstants.DESCRIPTION, GCConstants.EMAIL_ERROR_DESC);
        eleInbox.setAttribute(GCConstants.EXCEPTION_TYPE, "GCEmailError");

        GCCommonUtil.invokeAPI(env, GCConstants.API_CREATE_EXCEPTION, createExceptionDoc.getDocument());
      }
    }
    Document fopDoc = callPdfService(env, inDoc, isSameStore);

    LOGGER.verbose("GCConfirmPickAPI.manageBackRoomPickCompletion--end");
    LOGGER.endTimer("GCConfirmPickAPI.manageBackRoomPickCompletion");
    return fopDoc;
  }

  /**
   * This method is used to stamp mandatory attributes for changeShipment & changeOrder call.
   *
   * @param eleFinalChangeShipLine
   * @param eleShipLine
   * @param elefinalChangeOdrLines
   * @param posReservID
   */
  private void stampMandatoryAttributes(YFCElement eleFinalChangeShipLine, YFCElement eleShipLine,
      YFCElement eleFinalChgOdrLine, String posReservID) {

    eleFinalChangeShipLine.setAttribute(GCConstants.ACTION, GCConstants.ACTION_MODIFY);
    eleFinalChangeShipLine.setAttribute(GCConstants.SHIPMENT_LINE_KEY,
        eleShipLine.getAttribute(GCConstants.SHIPMENT_LINE_KEY));
    final YFCElement eleFinalChangeShipLineExtn = eleFinalChangeShipLine.createChild(GCConstants.EXTN);
    eleFinalChangeShipLineExtn.setAttribute(GCConstants.EXTN_ACTUAL_SERIAL_NO,
        eleShipLine.getAttribute(GCConstants.ACTUAL_SERIAL_NO));
    eleFinalChangeShipLineExtn.setAttribute(GCConstants.EXTN_POS_SKU_NO, eleShipLine.getAttribute(GCConstants.SKU_NUM));
    eleFinalChangeShipLineExtn.setAttribute(GCConstants.EXTN_POS_RESERV_ID, posReservID);

    eleFinalChgOdrLine.setAttribute(GCConstants.ACTION, GCConstants.ACTION_MODIFY);
    eleFinalChgOdrLine.setAttribute(GCConstants.ORDER_LINE_KEY, eleShipLine.getAttribute(GCConstants.ORDER_LINE_KEY));
    final YFCElement eleFinalChangeOdrLineExtn = eleFinalChgOdrLine.createChild(GCConstants.EXTN);
    eleFinalChangeOdrLineExtn.setAttribute(GCConstants.EXTN_ACTUAL_SERIAL_NO,
        eleShipLine.getAttribute(GCConstants.ACTUAL_SERIAL_NO));
    eleFinalChangeOdrLineExtn.setAttribute(GCConstants.EXTN_POS_SKU_NO, eleShipLine.getAttribute(GCConstants.SKU_NUM));
    eleFinalChangeOdrLineExtn.setAttribute("ExtnIsStoreFulfilled", GCConstants.FLAG_Y);

  }

  /**
   * This method is used to call changeOrderStatus API
   *
   * @param env
   * @param eleShipment
   * @param baseDropStatus
   * @param transactionID
   */
  private void callChangeOrderStatus(YFSEnvironment env, YFCElement eleShipment, String baseDropStatus,
      String transactionID) {
    LOGGER.beginTimer("GCConfirmPickAPI.callChangeOrderStatus");
    LOGGER.verbose("GCConfirmPickAPI.callChangeOrderStatus--Begin");
    // Create changeOrderStatus input document
    YFCDocument chngOrdStatusIndoc = YFCDocument.createDocument(GCConstants.ORDER_STATUS_CHANGE);
    YFCElement ordStatusChngEle = chngOrdStatusIndoc.getDocumentElement();
    ordStatusChngEle.setAttribute(GCConstants.DOCUMENT_TYPE, GCConstants.SALES_ORDER_DOCUMENT_TYPE);
    ordStatusChngEle.setAttribute(GCConstants.ENTERPRISE_CODE, eleShipment.getAttribute(GCConstants.ENTERPRISE_CODE));
    ordStatusChngEle.setAttribute(GCConstants.ORDER_HEADER_KEY, eleShipment.getAttribute(GCConstants.ORDER_HEADER_KEY));
    ordStatusChngEle.setAttribute(GCConstants.TRANSACTION_ID, transactionID);
    YFCElement eleOrderLinesStatus = ordStatusChngEle.createChild(GCConstants.ORDER_LINES);

    final YFCElement eleShipLines = eleShipment.getChildElement(GCConstants.SHIPMENT_LINES);
    YFCIterable<YFCElement> nlShipmentLine = eleShipLines.getChildren();
    for (YFCElement eleShipLine : nlShipmentLine) {

      if (YFCUtils.equals(GCConstants.BUNDLE, eleShipLine.getAttribute(GCConstants.KIT_CODE))) {
        YFCElement customComponents = eleShipLine.getChildElement(GCConstants.CUSTOM_COMPONENTS);
        if (!YFCCommon.isVoid(customComponents) && customComponents.hasChildNodes()) {
          YFCIterable<YFCElement> bundleCompList = customComponents.getChildren();
          for (YFCElement shipmentLine : bundleCompList) {
            YFCElement bundleCompLine = eleOrderLinesStatus.createChild(GCConstants.ORDER_LINE);
            bundleCompLine.setAttribute(GCConstants.ORDER_LINE_KEY,
                shipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY));
            bundleCompLine.setAttribute(GCConstants.QUANTITY, shipmentLine.getAttribute(GCConstants.QTY_PICKED));
            bundleCompLine.setAttribute(GCConstants.ORDER_RELEASE_KEY,
                shipmentLine.getAttribute(GCConstants.ORDER_RELEASE_KEY));
            bundleCompLine.setAttribute(GCConstants.BASE_DROP_STATUS, baseDropStatus);
          }
        }
      } else {
        YFCElement eleOrderLineStatus = eleOrderLinesStatus.createChild(GCConstants.ORDER_LINE);
        eleOrderLineStatus.setAttribute(GCConstants.ORDER_LINE_KEY,
            eleShipLine.getAttribute(GCConstants.ORDER_LINE_KEY));
        eleOrderLineStatus.setAttribute(GCConstants.ORDER_RELEASE_KEY,
            eleShipLine.getAttribute(GCConstants.ORDER_RELEASE_KEY));
        eleOrderLineStatus.setAttribute(GCConstants.QUANTITY, eleShipLine.getAttribute(GCConstants.QTY_PICKED));
        eleOrderLineStatus.setAttribute(GCConstants.BASE_DROP_STATUS, baseDropStatus);
      }

      YFCElement warrantyComponents = eleShipLine.getChildElement(GCConstants.WARRANTY_COMPONENTS);
      final double originalQty = Double.parseDouble(eleShipLine.getAttribute(GCConstants.QUANTITY));
      if (!YFCCommon.isVoid(warrantyComponents) && warrantyComponents.hasChildNodes()) {
        YFCIterable<YFCElement> warrantyCompList = warrantyComponents.getChildren();
        for (YFCElement shipmentLine : warrantyCompList) {
          YFCElement warrantyLine = eleOrderLinesStatus.createChild(GCConstants.ORDER_LINE);
          final double warrantyQty = Double.parseDouble(shipmentLine.getAttribute(GCConstants.QUANTITY));
          String finalQty = "";
          final double pickedQty = Double.parseDouble(eleShipLine.getAttribute(GCConstants.QTY_PICKED));
          if (warrantyQty == originalQty) {
            finalQty = String.valueOf(pickedQty);
          } else {
            if (warrantyQty > pickedQty) {
              finalQty = String.valueOf(pickedQty);
            } else {
              finalQty = String.valueOf(warrantyQty);
            }
          }
          warrantyLine.setAttribute(GCConstants.ORDER_LINE_KEY, shipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY));
          warrantyLine.setAttribute(GCConstants.ORDER_RELEASE_KEY,
              shipmentLine.getAttribute(GCConstants.ORDER_RELEASE_KEY));
          warrantyLine.setAttribute(GCConstants.QUANTITY, finalQty);
          warrantyLine.setAttribute(GCConstants.BASE_DROP_STATUS, baseDropStatus);
        }
      }
    }

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("chngOrdStatusIndoc : " + chngOrdStatusIndoc.toString());
    }
    GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER_STATUS, chngOrdStatusIndoc.getDocument());
    LOGGER.verbose("GCConfirmPickAPI.callChangeOrderStatus--End");
    LOGGER.endTimer("GCConfirmPickAPI.callChangeOrderStatus");
  }

  private Document callPdfService(YFSEnvironment env, YFCDocument inDoc, boolean isSameStore) {

    YFCDocument fopDoc = null;
    if (isSameStore) {
      fopDoc = GCCommonUtil.invokeService(env, GCConstants.PICKUP_NOTICE_SERVICE, inDoc);
    } else {
      fopDoc = GCCommonUtil.invokeService(env, GCConstants.SHIPMENT_NOTICE_SERVICE, inDoc);
    }

    return fopDoc.getDocument();
  }

  /**
   * This method is used to prepare changeOrderIndoc by updating the quantity for alternate sourcing
   *
   * @param orderLinesEle
   * @param eleShipLine
   * @param shipNode
   * @param eleOrderLinesStatus
   */
  private void prepareChangeOrderIndoc(YFCElement orderLinesEle, YFCElement eleShipLine, String shipNode,
      YFCElement eleOrderLinesStatus) {

    LOGGER.beginTimer("GCConfirmPickAPI.prepareChangeOrderIndoc");
    LOGGER.verbose("GCConfirmPickAPI.prepareChangeOrderIndoc--Begin");
    String qtyToPick = "";
    String pickedQty = "";
    if (YFCUtils.equals(GCConstants.BUNDLE, eleShipLine.getAttribute(GCConstants.KIT_CODE))) {
      final String calculatedParentQty = calculateParentQty(eleShipLine);
      qtyToPick = eleShipLine.getAttribute(GCConstants.QUANTITY);
      pickedQty = calculatedParentQty;
    } else {
      qtyToPick = eleShipLine.getAttribute(GCConstants.QUANTITY);
      pickedQty = eleShipLine.getAttribute(GCConstants.QTY_PICKED);
    }
    if (!YFCUtils.equals(qtyToPick, pickedQty)) {
      BigDecimal bdQtyToPick = new BigDecimal(qtyToPick);
      BigDecimal bdPickedQty = new BigDecimal(pickedQty);
      BigDecimal diffQty = bdQtyToPick.subtract(bdPickedQty);
      GCPickDetailActionsManager ob = new GCPickDetailActionsManager();
      ob.prepareResourceDoc(eleShipLine, orderLinesEle, shipNode);
      YFCElement warrantyComponents = eleShipLine.getChildElement(GCConstants.WARRANTY_COMPONENTS);
      if (YFCUtils.equals(GCConstants.BUNDLE, eleShipLine.getAttribute(GCConstants.KIT_CODE))) {
        YFCElement customComponents = eleShipLine.getChildElement(GCConstants.CUSTOM_COMPONENTS);
        if (!YFCCommon.isVoid(customComponents) && customComponents.hasChildNodes()) {
          YFCIterable<YFCElement> bundleCompList = customComponents.getChildren();
          for (YFCElement shipmentLine : bundleCompList) {
            qtyToPick = shipmentLine.getAttribute(GCConstants.QUANTITY);
            pickedQty = shipmentLine.getAttribute(GCConstants.QTY_PICKED);
            BigDecimal bdQtyToPickBundle = new BigDecimal(qtyToPick);
            BigDecimal bdPickedQtyBundle = new BigDecimal(pickedQty);
            BigDecimal diffQtyBundle = bdQtyToPickBundle.subtract(bdPickedQtyBundle);
            ob.prepareResourceStatusDoc(shipmentLine, eleOrderLinesStatus, diffQtyBundle.toString());
          }
        }
      } else if (!YFCCommon.isVoid(warrantyComponents) && warrantyComponents.hasChildNodes()) {
        YFCIterable<YFCElement> warrantyCompList = warrantyComponents.getChildren();
        for (YFCElement shipmentLine : warrantyCompList) {
          final double warrantyQty = Double.parseDouble(shipmentLine.getAttribute(GCConstants.QUANTITY));
          String finalQty = "";
          if (warrantyQty > Double.parseDouble(diffQty.toString())) {
            finalQty = diffQty.toString();
          } else {
            finalQty = String.valueOf(warrantyQty);
          }

          // Setting back the updated warranty Qty in input xml
          shipmentLine.setAttribute(GCConstants.QUANTITY, String.valueOf(warrantyQty - Double.parseDouble(finalQty)));


          ob.prepareResourceStatusDoc(shipmentLine, eleOrderLinesStatus, finalQty);
        }
        if (!YFCUtils.equals(GCConstants.BUNDLE, eleShipLine.getAttribute(GCConstants.KIT_CODE))) {
          YFCElement orderLine = eleOrderLinesStatus.createChild(GCConstants.ORDER_LINE);
          orderLine.setAttribute(GCConstants.ORDER_LINE_KEY, eleShipLine.getAttribute(GCConstants.ORDER_LINE_KEY));
          orderLine.setAttribute(GCConstants.QUANTITY, diffQty.toString());
          orderLine.setAttribute(GCConstants.BASE_DROP_STATUS, GCConstants.BACKORDERED_DAX);
        }
      } else {
        ob.prepareResourceStatusDoc(eleShipLine, eleOrderLinesStatus, diffQty.toString());
      }
    }
    LOGGER.verbose("GCConfirmPickAPI.prepareChangeOrderIndoc--end");
    LOGGER.endTimer("GCConfirmPickAPI.prepareChangeOrderIndoc");
  }

  /**
   * This method is used to prepare input for POS reservation web service call.
   *
   * @param env
   *
   * @param env
   * @param commissionsDoc
   * @param eleShipment
   */
  private YFCDocument reserveInventoryPOS(YFSEnvironment env, YFCDocument commissionsDoc, YFCElement eleShipment) {

    LOGGER.beginTimer("GCConfirmPickAPI.reserveInventoryPOS");
    LOGGER.verbose("GCConfirmPickAPI.reserveInventoryPOS--Begin");

    final YFCDocument docPOS = YFCDocument.createDocument(GCConstants.ORDER);
    final YFCElement elePOSOrder = docPOS.getDocumentElement();
    elePOSOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, eleShipment.getAttribute(GCConstants.ORDER_HEADER_KEY));
    elePOSOrder.setAttribute(GCConstants.SHIPMENT_KEY, eleShipment.getAttribute(GCConstants.SHIPMENT_KEY));
    elePOSOrder.setAttribute(GCConstants.SHIPMENT_NO, eleShipment.getAttribute(GCConstants.SHIPMENT_NO));
    elePOSOrder.setAttribute(GCConstants.MESSAGE_TYPE, GCConstants.RESERVE);

    final String userID = GCShipmentUtil.getEmployeeID(env, eleShipment.getAttribute(GCConstants.USER_ID));
    elePOSOrder.setAttribute(GCConstants.MGR_APPV, userID);
    elePOSOrder.setAttribute(GCConstants.CASHIER_APPV, userID);

    if (!YFCCommon.isVoid(commissionsDoc)) {
      final YFCElement elePOSCommissions = elePOSOrder.createChild(GCConstants.COMMISSIONS);
      final YFCElement eleCommissions = commissionsDoc.getDocumentElement();
      YFCIterable<YFCElement> nlEleCommision = eleCommissions.getChildren();

      // Looping through the commission list
      for (YFCElement eleCommission : nlEleCommision) {

        if (YFCUtils.equals(GCConstants.SALES_COMMISSION, eleCommission.getAttribute(GCConstants.COMMISSION_TYPE))) {
          final YFCElement elePOSSalesComm = elePOSCommissions.createChild(GCConstants.SALESCOMMISSION);
          elePOSSalesComm.setAttribute(GCConstants.SALES_PERSON, eleCommission.getAttribute(GCConstants.SALES_PERSON));
          elePOSSalesComm.setAttribute(GCConstants.SALES_PERCENT, eleCommission.getAttribute(GCConstants.SALES_PERCENT));
          elePOSSalesComm.setAttribute(GCConstants.LINE_NUMBER, eleCommission.getAttribute(GCConstants.LINE_NUMBER));
        } else {
          final YFCElement elePOSAcqComm = elePOSCommissions.createChild(GCConstants.ACQCOMMISSION);
          elePOSAcqComm.setAttribute(GCConstants.SALES_PERSON, eleCommission.getAttribute(GCConstants.SALES_PERSON));
          elePOSAcqComm.setAttribute(GCConstants.SALES_PERCENT, eleCommission.getAttribute(GCConstants.SALES_PERCENT));
          elePOSAcqComm.setAttribute(GCConstants.LINE_NUMBER, eleCommission.getAttribute(GCConstants.LINE_NUMBER));
        }
      }
    }
    final YFCElement elePOSProducts = elePOSOrder.createChild(GCConstants.PRODUCTS);
    final YFCElement eleShipLines = eleShipment.getChildElement(GCConstants.SHIPMENT_LINES);
    YFCIterable<YFCElement> nlEleShipLine = eleShipLines.getChildren();

    // prepare AssociateLineNo map
    Map<String, String> bundlePrimaryCompMap = new HashMap<String, String>();
    bundlePrimaryCompMap = prepareBundlePrimaryCompMap(nlEleShipLine, bundlePrimaryCompMap);

    for (YFCElement eleShipLine : nlEleShipLine) {
      if (YFCUtils.equals(GCConstants.BUNDLE, eleShipLine.getAttribute(GCConstants.KIT_CODE))) {
        final YFCElement eleCustomComp = eleShipLine.getChildElement(GCConstants.CUSTOM_COMPONENTS);
        YFCIterable<YFCElement> nlCompShipLine = eleCustomComp.getChildren();

        for (YFCElement eleProduct : nlCompShipLine) {
          final YFCElement elePOSProduct = elePOSProducts.createChild(GCConstants.PRODUCT);
          stampProductAttributes(elePOSProduct, eleProduct);
        }
      } else {
        final YFCElement elePOSProduct = elePOSProducts.createChild(GCConstants.PRODUCT);
        stampProductAttributes(elePOSProduct, eleShipLine);
      }

      // Warranty Logic
      final YFCElement eleWarrantyComponents = eleShipLine.getChildElement(GCXmlLiterals.WARRANTY_COMPONENTS);
      if (!YFCCommon.isVoid(eleWarrantyComponents)) {

        YFCIterable<YFCElement> nlWarrantyShpLine = eleWarrantyComponents.getChildren();
        for (YFCElement eleWarrantyShipLine : nlWarrantyShpLine) {
          final YFCElement elePOSProduct = elePOSProducts.createChild(GCConstants.PRODUCT);
          stampProductAttributes(elePOSProduct, eleWarrantyShipLine);
          elePOSProduct.setAttribute(GCConstants.QUANTITY, eleWarrantyShipLine.getAttribute(GCConstants.QUANTITY));
          elePOSProduct.setAttribute(GCConstants.ASSO_LINE_NO, eleShipLine.getAttribute(GCConstants.PRIME_LINE_NO));
          if (YFCUtils.equals(GCConstants.BUNDLE, eleShipLine.getAttribute(GCConstants.KIT_CODE))) {
            elePOSProduct.setAttribute(GCConstants.ASSO_LINE_NO,
                bundlePrimaryCompMap.get(eleShipLine.getAttribute(GCConstants.PRIME_LINE_NO)));
          }
        }
      }
    }
    LOGGER.verbose("GCConfirmPickAPI.reserveInventoryPOS--end");
    LOGGER.endTimer("GCConfirmPickAPI.reserveInventoryPOS");
    return docPOS;
  }

  /**
   * This method is used to find the ShipmentLineNo of primary component in case of bundles.
   *
   * @param nlEleShipLine
   * @param bundlePrimaryCompMap
   * @return
   */
  private Map<String, String> prepareBundlePrimaryCompMap(YFCIterable<YFCElement> nlEleShipLine,
      Map<String, String> bundlePrimaryCompMap) {

    for (YFCElement eleShipLine : nlEleShipLine) {
      if (YFCUtils.equals(GCConstants.BUNDLE, eleShipLine.getAttribute(GCConstants.KIT_CODE))) {
        final YFCElement eleShipOrderLine = eleShipLine.getChildElement(GCConstants.ORDER_LINE);
        final YFCElement eleShipOdrLineItem = eleShipOrderLine.getChildElement(GCConstants.ITEM_DETAILS);
        final YFCElement eleShipOdrLineItemExtn = eleShipOdrLineItem.getChildElement(GCConstants.EXTN);
        final String primaryComp = eleShipOdrLineItemExtn.getAttribute(GCConstants.EXTN_PRIMARY_COMPONENT_ID);
        final YFCElement eleCustomComp = eleShipLine.getChildElement(GCConstants.CUSTOM_COMPONENTS);
        YFCIterable<YFCElement> nlCompShipLine = eleCustomComp.getChildren();

        for (YFCElement eleProduct : nlCompShipLine) {
          if (YFCUtils.equals(primaryComp, eleProduct.getAttribute(GCConstants.ITEM_ID))) {
            bundlePrimaryCompMap.put(eleShipLine.getAttribute(GCConstants.PRIME_LINE_NO),
                eleProduct.getAttribute(GCConstants.PRIME_LINE_NO));
          }

        }
      }
    }
    return bundlePrimaryCompMap;
  }

  /**
   * This method is used to stamp few Product attributes
   *
   * @param elePOSProduct
   * @param eleShipLine
   */
  private void stampProductAttributes(YFCElement elePOSProduct, YFCElement eleShipLine) {

    LOGGER.beginTimer("GCConfirmPickAPI.stampProductAttributes");
    LOGGER.verbose("GCConfirmPickAPI.stampProductAttributes--Begin");

    elePOSProduct.setAttribute(GCConstants.PRODUCT_TYPE, eleShipLine.getAttribute(GCConstants.PRODUCT_CLASS));
    elePOSProduct.setAttribute(GCConstants.LINE_NUMBER, eleShipLine.getAttribute(GCConstants.PRIME_LINE_NO));
    elePOSProduct.setAttribute(GCConstants.SERIAL_NO, eleShipLine.getAttribute(GCConstants.ACTUAL_SERIAL_NO));
    final YFCElement eleShipOrderLine = eleShipLine.getChildElement(GCConstants.ORDER_LINE);
    final YFCElement eleShipOdrLineItem = eleShipOrderLine.getChildElement(GCConstants.ITEM_DETAILS);
    final YFCElement eleShipOdrLineItemExtn = eleShipOdrLineItem.getChildElement(GCConstants.EXTN);
    final String posItemID = eleShipOdrLineItemExtn.getAttribute(GCConstants.EXTN_POSITEM_ID);
    elePOSProduct.setAttribute(GCConstants.SKU, posItemID);
    elePOSProduct.setAttribute(GCConstants.SKU_NO, eleShipLine.getAttribute(GCConstants.SKU_NUM));
    elePOSProduct.setAttribute(GCConstants.QUANTITY, eleShipLine.getAttribute(GCConstants.QTY_PICKED));

    LOGGER.verbose("GCConfirmPickAPI.stampProductAttributes--end");
    LOGGER.endTimer("GCConfirmPickAPI.stampProductAttributes");
  }

  /**
   * This method manages same store pickup.
   *
   * @param env
   * @param eleShipment
   * @param shipNode
   */
  private String manageSameStorePickup(YFSEnvironment env, YFCElement eleShipment, String shipNode) {

    LOGGER.beginTimer("GCConfirmPickAPI.manageSameStorePickup");
    LOGGER.verbose("GCConfirmPickAPI.manageSameStorePickup--Begin");

    final String shipmentKey = eleShipment.getAttribute(GCConstants.SHIPMENT_KEY);
    callChangeShipmentStatusApi(env, shipmentKey, GCConstants.BCKROOM_PICKED_STATUS, GCConstants.BCKROOM_PICKED_TRNSCTN);
    callChangeOrderStatus(env, eleShipment, GCConstants.SO_BACKROOM_PICKED_STATUS,
        GCConstants.SO_BCKROOM_PICKED_TRNSCTN);

    YFCDocument confirmShipDoc = prepareConfirmShipmentInput(eleShipment, shipNode);
    GCRecordConfirmPickAction obj = new GCRecordConfirmPickAction();
    final YFCDocument commissionsDoc = obj.updateUserIdAndCommission(env, eleShipment);
    String reservationID = null;
    // webservice call for reserving inventory with POS
    YFCDocument requestDocPOS = reserveInventoryPOS(env, commissionsDoc, eleShipment);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("requestDocPOS : " + requestDocPOS.toString());
    }
    YFCDocument responsePOS = GCCommonUtil.invokeService(env, "GCGetPOSReservationID", requestDocPOS);
    YFCElement eleResponse = responsePOS.getDocumentElement();
    LOGGER.verbose("requestDocPOS : " + requestDocPOS.toString());

    if (!YFCCommon.isVoid(eleResponse)) {
      reservationID = eleResponse.getAttribute(GCConstants.RESERVATION_ID);
    }

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("confirmShipDoc : " + confirmShipDoc.toString());
    }
    // calling confirmShipment API
    YFCDocument tempConfirmShipDoc = YFCDocument.getDocumentFor(GCConstants.CONFIRM_SHIPMENT_TEMP);
    confirmShipOutdoc =
        GCCommonUtil.invokeAPI(env, GCConstants.API_CONFIRM_SHIPMENT, confirmShipDoc, tempConfirmShipDoc);

    YFCElement eleConfirmShipment = confirmShipOutdoc.getDocumentElement();
    eleConfirmShipment.setAttribute(GCConstants.ORDER_NO, eleShipment.getAttribute(GCConstants.ORDER_NO));
    eleConfirmShipment.setAttribute(GCConstants.ORDER_HEADER_KEY,
        eleShipment.getAttribute(GCConstants.ORDER_HEADER_KEY));

    LOGGER.verbose("GCConfirmPickAPI.manageSameStorePickup--end");
    LOGGER.endTimer("GCConfirmPickAPI.manageSameStorePickup");
    return reservationID;
  }


  /**
   * This method is used to prepare input for confirmShipment API call
   *
   * @param env
   * @param eleShipment
   * @param shipNode
   * @return
   */
  private YFCDocument prepareConfirmShipmentInput(YFCElement eleShipment, String shipNode) {

    LOGGER.beginTimer("GCConfirmPickAPI.prepareConfirmShipmentInput");
    LOGGER.verbose("GCConfirmPickAPI.prepareConfirmShipmentInput--Begin");

    final YFCDocument confirmShipDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    final YFCElement eleConfirmShipment = confirmShipDoc.getDocumentElement();
    eleConfirmShipment.setAttribute(GCConstants.ACTION, GCConstants.MODIFY);
    eleConfirmShipment.setAttribute(GCConstants.SHIPMENT_KEY, eleShipment.getAttribute(GCConstants.SHIPMENT_KEY));
    eleConfirmShipment.setAttribute(GCConstants.DOCUMENT_TYPE, eleShipment.getAttribute(GCConstants.DOCUMENT_TYPE));
    eleConfirmShipment.setAttribute(GCConstants.ENTERPRISE_CODE, eleShipment.getAttribute(GCConstants.ENTERPRISE_CODE));
    eleConfirmShipment.setAttribute(GCConstants.SHIP_NODE, shipNode);
    final YFCElement eleContainers = eleConfirmShipment.createChild(GCConstants.CONTAINERS);
    final YFCElement eleConfirmShpLines = eleConfirmShipment.createChild(GCConstants.SHIPMENT_LINES);
    final YFCElement eleShipLines = eleShipment.getChildElement(GCConstants.SHIPMENT_LINES);
    YFCIterable<YFCElement> nlEleShipLine = eleShipLines.getChildren();

    for (YFCElement eleShipLine : nlEleShipLine) {

      // Fix For GCSTORE# 2270
      final YFCElement eleContainer = eleContainers.createChild(GCConstants.CONTAINER);
      final YFCElement eleContainerExtn = eleContainer.createChild(GCConstants.EXTN);
      eleContainerExtn.setAttribute(GCConstants.EXTN_CONTAINER_STATUS, GCConstants.RDY_FOR_CUST_PICKUP);
      final YFCElement eleContainerDtls = eleContainer.createChild(GCConstants.CONTAINER_DETAILS);

      final String kitCode = eleShipLine.getAttribute(GCConstants.KIT_CODE);
      if(!YFCUtils.equals(GCConstants.BUNDLE, kitCode)){
        stampContainerDetails(eleContainerDtls, eleShipLine);
      }

      final YFCElement eleConfirmShipLine = eleConfirmShpLines.createChild(GCConstants.SHIPMENT_LINE);
      stampShipLineAttributes(eleShipLine, eleConfirmShipLine);
      eleConfirmShipLine.setAttribute(GCConstants.QUANTITY, eleShipLine.getAttribute(GCConstants.QTY_PICKED));
      eleConfirmShipLine.setAttribute(GCConstants.BCKROOM_PCKD_QTY, eleShipLine.getAttribute(GCConstants.QTY_PICKED));
      if (YFCUtils.equals(GCConstants.BUNDLE, kitCode)) {
        final String newParentQty = calculateParentQty(eleShipLine);
        eleConfirmShipLine.setAttribute(GCConstants.QUANTITY, newParentQty);
        eleConfirmShipLine.setAttribute(GCConstants.BCKROOM_PCKD_QTY, newParentQty);
        final YFCElement eleCustomComp = eleShipLine.getChildElement(GCConstants.CUSTOM_COMPONENTS);
        YFCIterable<YFCElement> nlCompShipLine = eleCustomComp.getChildren();

        for (YFCElement eleCompShipLine : nlCompShipLine) {

          stampContainerDetails(eleContainerDtls, eleCompShipLine);
          final YFCElement eleConfirmCompLine = eleConfirmShpLines.createChild(GCConstants.SHIPMENT_LINE);
          stampShipLineAttributes(eleCompShipLine, eleConfirmCompLine);
          eleConfirmCompLine.setAttribute(GCConstants.QUANTITY, eleCompShipLine.getAttribute(GCConstants.QTY_PICKED));
          eleConfirmCompLine.setAttribute(GCConstants.BCKROOM_PCKD_QTY,
              eleCompShipLine.getAttribute(GCConstants.QTY_PICKED));
        }
      }

      // Warranty Lines
      final YFCElement eleWarrantyComponents = eleShipLine.getChildElement(GCXmlLiterals.WARRANTY_COMPONENTS);
      if (!YFCCommon.isVoid(eleWarrantyComponents)) {

        YFCIterable<YFCElement> nlWarrantyShpLine = eleWarrantyComponents.getChildren();
        for (YFCElement eleWarrantyShipLine : nlWarrantyShpLine) {
          final YFCElement eleConfirmWarrantyLine = eleConfirmShpLines.createChild(GCConstants.SHIPMENT_LINE);
          stampShipLineAttributes(eleWarrantyShipLine, eleConfirmWarrantyLine);
          final double warrantyQty = Double.parseDouble(eleWarrantyShipLine.getAttribute(GCConstants.QUANTITY));
          String finalQty = "0.0";
          final double pickedQty = Double.parseDouble(eleShipLine.getAttribute(GCConstants.QTY_PICKED));
          if (warrantyQty >= pickedQty) {
            finalQty = String.valueOf(pickedQty);
          } else {
            finalQty = String.valueOf(warrantyQty);
          }
          eleConfirmWarrantyLine.setAttribute(GCConstants.QUANTITY, finalQty);
          eleConfirmWarrantyLine.setAttribute(GCConstants.BCKROOM_PCKD_QTY, finalQty);
        }
      }
    }
    LOGGER.verbose("GCConfirmPickAPI.prepareConfirmShipmentInput--end");
    LOGGER.endTimer("GCConfirmPickAPI.prepareConfirmShipmentInput");
    return confirmShipDoc;
  }

  /**
   * This method is used to stamp containerDetail attributes
   *
   * @param eleContainers
   * @param eleShipLine
   * @param kitCode
   */
  private void stampContainerDetails(YFCElement eleContainerDtls, YFCElement eleShipLine) {

    final YFCElement eleContainerDtl = eleContainerDtls.createChild(GCConstants.CONTAINER_DETAIL);
    eleContainerDtl.setAttribute(GCConstants.QUANTITY, eleShipLine.getAttribute(GCConstants.QTY_PICKED));

    final YFCElement eleContainerShipLine = eleContainerDtl.createChild(GCConstants.SHIPMENT_LINE);
    stampShipLineAttributes(eleShipLine, eleContainerShipLine);

    // Warranty Components
    final YFCElement eleWarrantyComponents = eleShipLine.getChildElement(GCXmlLiterals.WARRANTY_COMPONENTS);
    if (!YFCCommon.isVoid(eleWarrantyComponents)) {

      YFCIterable<YFCElement> nlWarrantyShpLine = eleWarrantyComponents.getChildren();
      for (YFCElement eleWarrantyShipLine : nlWarrantyShpLine) {
        final YFCElement eleWarrantyContainerDtl = eleContainerDtls.createChild(GCConstants.CONTAINER_DETAIL);
        final double warrantyQty = Double.parseDouble(eleWarrantyShipLine.getAttribute(GCConstants.QUANTITY));
        String finalQty = "0.0";
        final double pickedQty = Double.parseDouble(eleShipLine.getAttribute(GCConstants.QTY_PICKED));
        if (warrantyQty >= pickedQty) {
          finalQty = String.valueOf(pickedQty);
        } else {
          finalQty = String.valueOf(warrantyQty);
        }
        eleWarrantyContainerDtl.setAttribute(GCConstants.QUANTITY, finalQty);
        final YFCElement eleWarrantyContainerShipLine = eleWarrantyContainerDtl.createChild(GCConstants.SHIPMENT_LINE);
        stampShipLineAttributes(eleWarrantyShipLine, eleWarrantyContainerShipLine);
      }
    }
  }

  /**
   * This method is used to stamp ShipmentLine attributes for confirmShipment.
   *
   * @param eleShipLine
   * @param eleConfirmShipLine
   */
  private void stampShipLineAttributes(YFCElement eleShipLine, YFCElement eleConfirmShipLine) {

    LOGGER.beginTimer("GCConfirmPickAPI.stampShipLineAttributes");
    LOGGER.verbose("GCConfirmPickAPI.stampShipLineAttributes--Begin");

    eleConfirmShipLine.setAttribute(GCConstants.ITEM_ID, eleShipLine.getAttribute(GCConstants.ITEM_ID));
    eleConfirmShipLine.setAttribute(GCConstants.ORDER_HEADER_KEY,
        eleShipLine.getAttribute(GCConstants.ORDER_HEADER_KEY));
    eleConfirmShipLine.setAttribute(GCConstants.SHIPMENT_LINE_KEY,
        eleShipLine.getAttribute(GCConstants.SHIPMENT_LINE_KEY));
    eleConfirmShipLine.setAttribute(GCConstants.ORDER_LINE_KEY, eleShipLine.getAttribute(GCConstants.ORDER_LINE_KEY));
    eleConfirmShipLine.setAttribute(GCConstants.PRODUCT_CLASS, eleShipLine.getAttribute(GCConstants.PRODUCT_CLASS));
    eleConfirmShipLine.setAttribute(GCConstants.UOM, eleShipLine.getAttribute(GCConstants.UOM));
    LOGGER.verbose("GCConfirmPickAPI.stampShipLineAttributes--end");
    LOGGER.endTimer("GCConfirmPickAPI.stampShipLineAttributes");
  }

  /**
   * This method is used to call changeShipmentStatus.
   *
   * @param env
   * @param shipmentKey
   * @param dropStatus
   * @param trnsactionID
   */
  private void callChangeShipmentStatusApi(YFSEnvironment env, String shipmentKey, String dropStatus,
      String trnsactionID) {

    LOGGER.beginTimer("GCConfirmPickAPI.callChangeShipmentStatusApi");
    LOGGER.verbose("GCConfirmPickAPI.callChangeShipmentStatusApi--Begin");

    YFCDocument changeShipmentDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement eleShipment = changeShipmentDoc.getDocumentElement();
    eleShipment.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
    eleShipment.setAttribute(GCConstants.BASE_DROP_STATUS, dropStatus);
    eleShipment.setAttribute(GCConstants.TRANSACTION_ID, trnsactionID);

    // call changeShipmentStatusAPI
    YFCDocument tempdoc = YFCDocument.getDocumentFor(GCConstants.CHANGE_SHIP_TEMP);
    GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_SHIPMENT_STATUS, changeShipmentDoc, tempdoc);

    LOGGER.verbose("GCConfirmPickAPI.callChangeShipmentStatusApi--end");
    LOGGER.endTimer("GCConfirmPickAPI.callChangeShipmentStatusApi");
  }

  /**
   * This method is used to prepare changeShipment input to backroom pick the qty.
   *
   * @param env
   *
   * @param eleShipLine
   * @param eleShipLinesIndoc
   * @param shipNode
   * @param eleItems
   * @return
   */

  private void prepareInputForbackroompickedQty(YFSEnvironment env, YFCElement eleShipLine,
      YFCElement eleShipLinesIndoc, String shipNode,
      YFCElement eleItems) {

    LOGGER.beginTimer("GCConfirmPickAPI.prepareInputForbackroompickedQty");
    LOGGER.verbose("GCConfirmPickAPI.prepareInputForbackroompickedQty--Begin");

    final YFCElement eleShipLineIndoc = eleShipLinesIndoc.createChild(GCConstants.SHIPMENT_LINE);

    final String actualSerialNo = eleShipLine.getAttribute(GCConstants.ACTUAL_SERIAL_NO);

    String requestedTagNo = eleShipLine.getAttribute(GCConstants.REQ_TAG_NO);
    boolean isSerialAvailable = true;
    if (!YFCCommon.isVoid(actualSerialNo)) {
      isSerialAvailable = checkActualSrialNo(env, eleShipLine, shipNode, actualSerialNo);
    }

    // If isSerialAvailable is false then adjusting inventory in the non-serialized bucket when a
    // serial no. is requested, if requested serial no. is also null then there is no need to adjust
    // the inventory.

    if (!YFCUtils.equals(GCConstants.BUNDLE, eleShipLine.getAttribute(GCConstants.KIT_CODE))) {
      if (!YFCCommon.isVoid(actualSerialNo) && !YFCUtils.equals(actualSerialNo, requestedTagNo)) {
        if (!(!isSerialAvailable && YFCCommon.isVoid(requestedTagNo))) {
          stampAttrForAdjInv(eleItems, eleShipLine, shipNode, isSerialAvailable);
        }
      }
    } else {
      final YFCElement eleCustomComponents = eleShipLine.getChildElement(GCXmlLiterals.CUSTOM_COMPONENTS);
      YFCIterable<YFCElement> nlCustomShipmentLine = eleCustomComponents.getChildren();

      for (YFCElement eleCustomShipLine : nlCustomShipmentLine) {
        final String actualSerialNoComp = eleCustomShipLine.getAttribute(GCConstants.ACTUAL_SERIAL_NO);

        if (!YFCCommon.isVoid(actualSerialNoComp)) {
          isSerialAvailable = checkActualSrialNo(env, eleShipLine, shipNode, actualSerialNoComp);
        }

        if (!YFCCommon.isVoid(actualSerialNoComp) && !YFCUtils.equals(actualSerialNoComp, requestedTagNo)) {
          if (!(!isSerialAvailable && YFCCommon.isVoid(requestedTagNo))) {
            stampAttrForAdjInv(eleItems, eleCustomShipLine, shipNode, isSerialAvailable);
          }
        }
      }
    }
    eleShipLineIndoc.setAttribute(GCConstants.ACTION, GCConstants.ACTION_MODIFY);
    eleShipLineIndoc.setAttribute(GCConstants.ORDER_HEADER_KEY, eleShipLine.getAttribute(GCConstants.ORDER_HEADER_KEY));
    eleShipLineIndoc.setAttribute(GCConstants.SHIPMENT_LINE_NO, eleShipLine.getAttribute(GCConstants.SHIPMENT_LINE_NO));
    final String odrLineKey = eleShipLine.getAttribute(GCConstants.ORDER_LINE_KEY);
    eleShipLineIndoc.setAttribute(GCConstants.ORDER_LINE_KEY, odrLineKey);

    String finalPickedQty = eleShipLine.getAttribute(GCConstants.QTY_PICKED);
    if (YFCUtils.equals(GCConstants.BUNDLE, eleShipLine.getAttribute(GCConstants.KIT_CODE))) {
      finalPickedQty = calculateParentQty(eleShipLine);
    }
    eleShipLineIndoc.setAttribute(GCConstants.BCKROOM_PCKD_QTY, finalPickedQty);
    eleShipLineIndoc.setAttribute(GCConstants.QUANTITY, finalPickedQty);

    final double originalQty = Double.parseDouble(eleShipLine.getAttribute(GCConstants.QUANTITY));
    YFCElement eleShipOdrline = eleShipLine.getChildElement(GCConstants.ORDER_LINE);

    // Warranty Logic
    final String dependentOnLineKey = eleShipOdrline.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
    if (YFCCommon.isVoid(dependentOnLineKey)) {
      final YFCElement eleWarrantyComponents = eleShipLine.getChildElement(GCXmlLiterals.WARRANTY_COMPONENTS);
      if (!YFCCommon.isVoid(eleWarrantyComponents)) {

        YFCIterable<YFCElement> nlWarrantyShpLine = eleWarrantyComponents.getChildren();
        for (YFCElement eleWarrantyShipLine : nlWarrantyShpLine) {
          final YFCElement eleWarrantyShipLineIndoc = eleShipLinesIndoc.createChild(GCConstants.SHIPMENT_LINE);
          final double warrantyQty = Double.parseDouble(eleWarrantyShipLine.getAttribute(GCConstants.QUANTITY));
          String finalQty = "";
          final double pickedQty = Double.parseDouble(eleShipLine.getAttribute(GCConstants.QTY_PICKED));
          if (warrantyQty == originalQty) {
            finalQty = String.valueOf(pickedQty);
          } else {
            if (warrantyQty > pickedQty) {
              finalQty = String.valueOf(pickedQty);
            } else {
              finalQty = String.valueOf(warrantyQty);
            }
          }
          eleWarrantyShipLineIndoc.setAttribute(GCConstants.BCKROOM_PCKD_QTY, finalQty);
          eleWarrantyShipLineIndoc.setAttribute(GCConstants.QUANTITY, finalQty);
          eleWarrantyShipLineIndoc.setAttribute(GCConstants.ACTION, GCConstants.ACTION_MODIFY);
          eleWarrantyShipLineIndoc.setAttribute(GCConstants.ORDER_HEADER_KEY,
              eleWarrantyShipLine.getAttribute(GCConstants.ORDER_HEADER_KEY));
          eleWarrantyShipLineIndoc.setAttribute(GCConstants.SHIPMENT_LINE_NO,
              eleWarrantyShipLine.getAttribute(GCConstants.SHIPMENT_LINE_NO));
          eleWarrantyShipLineIndoc.setAttribute(GCConstants.ORDER_LINE_KEY,
              eleWarrantyShipLine.getAttribute(GCConstants.ORDER_LINE_KEY));
        }
      }
    }
    LOGGER.verbose("GCConfirmPickAPI.prepareInputForbackroompickedQty--end");
    LOGGER.endTimer("GCConfirmPickAPI.prepareInputForbackroompickedQty");
  }

  /**
   * This method is used to check whether the ActualSrialNo is available in oms or not by calling
   * getSupplyDetails API.
   *
   * @param env
   * @param eleShipLine
   * @param shipNode
   * @param actualSerialNo
   * @return
   */
  private boolean checkActualSrialNo(YFSEnvironment env, YFCElement eleShipLine, String shipNode, String actualSerialNo) {

    LOGGER.beginTimer("GCConfirmPickAPI.checkActualSrialNo");
    LOGGER.verbose("GCConfirmPickAPI.checkActualSrialNo--Begin");
    // Creating a New Document of getSupplyDetails
    YFCDocument getSupplyDetailsinDoc = YFCDocument.createDocument(GCConstants.GET_SUPPLY_DETAILS);
    YFCElement eleGetSupplyDetails = getSupplyDetailsinDoc.getDocumentElement();
    eleGetSupplyDetails.setAttribute(GCConstants.ITEM_ID, eleShipLine.getAttribute(GCConstants.ITEM_ID));
    eleGetSupplyDetails.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GCI);
    eleGetSupplyDetails.setAttribute(GCConstants.PRODUCT_CLASS, eleShipLine.getAttribute(GCConstants.PRODUCT_CLASS));
    eleGetSupplyDetails.setAttribute(GCConstants.SHIP_NODE, shipNode);
    eleGetSupplyDetails.setAttribute(GCConstants.UNIT_OF_MEASURE, eleShipLine.getAttribute(GCConstants.UOM));
    YFCElement eleTag = eleGetSupplyDetails.createChild(GCConstants.TAG);
    eleTag.setAttribute(GCConstants.BATCH_NO, actualSerialNo);


    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input to getSupplyDetails API : " + getSupplyDetailsinDoc.getString());
    }

    YFCDocument getSupplyDetailsTemplate = YFCDocument.getDocumentFor(GCConstants.GET_SUPPLY_DETAILS_TEMPLATE);

    // calling getSupplyDetails to get the Tag supply of actualSerialNo available in OMS for this
    // item
    YFCDocument outDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_SUPPLY_DETAILS, getSupplyDetailsinDoc, getSupplyDetailsTemplate);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Output from getSupplyDetails :" + outDoc.getString());
    }

    YFCElement eleOutDocItem = outDoc.getDocumentElement();
    YFCElement eleOutDocShipNodes = eleOutDocItem.getChildElement(GCXmlLiterals.SHIP_NODES);
    if (eleOutDocShipNodes.hasChildNodes()) {
      LOGGER.verbose("GCConfirmPickAPI.checkActualSrialNo--end");
      LOGGER.endTimer("GCConfirmPickAPI.checkActualSrialNo");
      return true;
    } else {
      LOGGER.verbose("GCConfirmPickAPI.checkActualSrialNo--end");
      LOGGER.endTimer("GCConfirmPickAPI.checkActualSrialNo");
      return false;
    }
  }

  /**
   * This method is used to stamp mandatory attributes for adjustInventory call
   *
   * @param eleItems
   * @param eleShipLine
   * @param shipNode
   * @param isSerialAvailable
   */
  private void stampAttrForAdjInv(YFCElement eleItems, YFCElement eleShipLine, String shipNode,
      boolean isSerialAvailable) {

    final YFCElement eleItem = eleItems.createChild(GCConstants.ITEM);
    final YFCElement eleTag = eleItem.createChild(GCConstants.TAG);
    String actualSerialNo = eleShipLine.getAttribute(GCConstants.ACTUAL_SERIAL_NO);
    if (!isSerialAvailable) {
      actualSerialNo = "";
    }
    eleTag.setAttribute(GCConstants.BATCH_NO, actualSerialNo);
    eleItem.setAttribute(GCConstants.ITEM_ID, eleShipLine.getAttribute(GCConstants.ITEM_ID));
    eleItem.setAttribute(GCConstants.PRODUCT_CLASS, eleShipLine.getAttribute(GCConstants.PRODUCT_CLASS));
    eleItem.setAttribute(GCConstants.UOM, eleShipLine.getAttribute(GCConstants.UOM));
    eleItem.setAttribute(GCConstants.QUANTITY, "-" + eleShipLine.getAttribute(GCConstants.QTY_PICKED));

    eleItem.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GCI);
    eleItem.setAttribute(GCConstants.ADJUSTMENT_TYPE, GCConstants.ADJUSTMENT);
    eleItem.setAttribute(GCConstants.SUPPLY_TYPE, GCConstants.ONHAND);
    eleItem.setAttribute(GCConstants.SHIP_NODE, shipNode);

    final YFCElement eleItem1 = eleItems.createChild(GCConstants.ITEM);
    final YFCElement eleTag1 = eleItem1.createChild(GCConstants.TAG);
    eleTag1.setAttribute(GCConstants.BATCH_NO, eleShipLine.getAttribute(GCConstants.REQ_TAG_NO));
    eleItem1.setAttribute(GCConstants.ITEM_ID, eleShipLine.getAttribute(GCConstants.ITEM_ID));
    eleItem1.setAttribute(GCConstants.PRODUCT_CLASS, eleShipLine.getAttribute(GCConstants.PRODUCT_CLASS));
    eleItem1.setAttribute(GCConstants.UOM, eleShipLine.getAttribute(GCConstants.UOM));
    eleItem1.setAttribute(GCConstants.QUANTITY, "+" + eleShipLine.getAttribute(GCConstants.QTY_PICKED));

    eleItem1.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GCI);
    eleItem1.setAttribute(GCConstants.ADJUSTMENT_TYPE, GCConstants.ADJUSTMENT);
    eleItem1.setAttribute(GCConstants.SUPPLY_TYPE, GCConstants.ONHAND);
    eleItem1.setAttribute(GCConstants.SHIP_NODE, shipNode);

  }

  /**
   * This method is used to calculate updated bundle parent quantity
   *
   * @param eleShipLine
   * @return
   */
  private String calculateParentQty(YFCElement eleShipLine) {

    LOGGER.beginTimer("GCConfirmPickAPI.calculateParentQty");
    LOGGER.verbose("GCConfirmPickAPI.calculateParentQty--Begin");

    final BigDecimal parentQty = new BigDecimal(eleShipLine.getAttribute(GCConstants.QUANTITY));
    final YFCElement eleCustomComponents = eleShipLine.getChildElement(GCXmlLiterals.CUSTOM_COMPONENTS);
    final YFCElement eleCompShipLine = eleCustomComponents.getElementsByTagName(GCConstants.SHIPMENT_LINE).item(0);
    final BigDecimal compPickedQty = new BigDecimal(eleCompShipLine.getAttribute(GCConstants.QTY_PICKED));
    final BigDecimal originalCompQty = new BigDecimal(eleCompShipLine.getAttribute(GCConstants.QUANTITY));
    final BigDecimal ratio = originalCompQty.divide(parentQty);
    final BigDecimal newParentQty = compPickedQty.divide(ratio);

    LOGGER.verbose("GCConfirmPickAPI.calculateParentQty--end");
    LOGGER.endTimer("GCConfirmPickAPI.calculateParentQty");
    return newParentQty.toString();
  }

  /**
   * This method is used to calculate the partialPick of set items whether the components are picked
   * in correct ratio or not.
   *
   * @param env
   * @param inputDoc
   * @return
   */
  public Document validatePartialSetPickUp(YFSEnvironment env, Document inputDoc) {

    LOGGER.beginTimer("GCConfirmPickAPI.validatePartialSetPickUp");
    LOGGER.verbose("GCConfirmPickAPI.validatePartialSetPickUp--Begin");

    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("inputDoc :" + inDoc.toString());
    }
    final YFCElement eleShipment = inDoc.getDocumentElement();
    final YFCElement eleShipLines = eleShipment.getChildElement(GCConstants.SHIPMENT_LINES);
    YFCIterable<YFCElement> nlShipmentLine = eleShipLines.getChildren();

    final YFCDocument errorDoc = YFCDocument.createDocument(GCConstants.ERROR);
    final YFCElement eleError = errorDoc.getDocumentElement();
    eleError.setAttribute(GCConstants.IS_ERROR, GCConstants.FLAG_N);
    for (YFCElement eleShipLine : nlShipmentLine) {
      if (YFCUtils.equals(GCConstants.BUNDLE, eleShipLine.getAttribute(GCConstants.KIT_CODE))) {
        final int bundleQty = Float.valueOf(eleShipLine.getAttribute(GCConstants.QUANTITY)).intValue();
        LOGGER.verbose("bundleQty : " + bundleQty);
        int intParentQty = 0;
        int intOriginalQtyRatio = 0;
        final YFCElement eleCustomComponents = eleShipLine.getChildElement(GCXmlLiterals.CUSTOM_COMPONENTS);
        YFCIterable<YFCElement> nlCustomShipmentLine = eleCustomComponents.getChildren();

        for (YFCElement eleCustomShipLine : nlCustomShipmentLine) {
          final int compQtyToPick = Float.valueOf(eleCustomShipLine.getAttribute(GCConstants.QUANTITY)).intValue();
          final int compPickedQty = Float.valueOf(eleCustomShipLine.getIntAttribute(GCConstants.QTY_PICKED)).intValue();
          LOGGER.verbose("compQtyToPick : " + compQtyToPick);
          LOGGER.verbose("compPickedQty : " + compPickedQty);
          intOriginalQtyRatio = compQtyToPick / bundleQty;
          LOGGER.verbose("intOriginalQty : " + intOriginalQtyRatio);
          if (compPickedQty % intOriginalQtyRatio == 0) {

            if (intParentQty == 0) {
              intParentQty = compPickedQty / intOriginalQtyRatio;
              LOGGER.verbose("intParentQty : " + intParentQty);
            } else {
              int tempParentQty = compPickedQty / intOriginalQtyRatio;
              LOGGER.verbose("tempParentQty : " + tempParentQty);
              if (tempParentQty != intParentQty) {
                YFCElement eleErrorShipLine = eleError.createChild(GCConstants.SHIPMENT_LINE);
                eleErrorShipLine.setAttribute(GCConstants.SHIPMENT_LINE_KEY,
                    eleCustomShipLine.getAttribute(GCConstants.SHIPMENT_LINE_KEY));
                eleError.setAttribute(GCConstants.IS_ERROR, GCConstants.FLAG_Y);
              }
            }
          } else {
            YFCElement eleErrorShipLine = eleError.createChild(GCConstants.SHIPMENT_LINE);
            eleErrorShipLine.setAttribute(GCConstants.SHIPMENT_LINE_KEY,
                eleCustomShipLine.getAttribute(GCConstants.SHIPMENT_LINE_KEY));
            eleError.setAttribute(GCConstants.IS_ERROR, GCConstants.FLAG_Y);
          }
        }
      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("errorDoc : " + errorDoc.toString());
    }
    LOGGER.verbose("GCConfirmPickAPI.validatePartialSetPickUp--end");
    LOGGER.endTimer("GCConfirmPickAPI.validatePartialSetPickUp");
    return errorDoc.getDocument();

  }

}
