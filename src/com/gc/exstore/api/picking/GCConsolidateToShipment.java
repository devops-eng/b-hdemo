package com.gc.exstore.api.picking;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class is used to prepare the input and call consolidateToShipment API to create Shipment.
 *
 * @author shanil.sharma
 *
 */
public class GCConsolidateToShipment {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCPickListManager.class);

  /**
   * This method is used to prepare the consolidateToShipment API Input & call the same.
   *
   * @param env
   * @param inputDoc
   * @return
   */
  public Document callConsolidateToShipment(YFSEnvironment env, Document inputDoc) {

    LOGGER.beginTimer("GCConsolidateToShipment.callConsolidateToShipment");
    LOGGER.verbose("GCConsolidateToShipment.callConsolidateToShipment--Begin");
    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    LOGGER.verbose("inDoc : " + inDoc.toString());
    YFCDocument consolidateIndoc = YFCDocument.createDocument(GCConstants.ORDER_RELEASE);
    YFCElement eleConOdrRelease = consolidateIndoc.getDocumentElement();
    YFCElement eleOdrRelease = inDoc.getDocumentElement();

    eleConOdrRelease.setAttribute(GCConstants.ENTERPRISE_CODE, eleOdrRelease.getAttribute(GCConstants.ENTERPRISE_CODE));
    eleConOdrRelease.setAttribute(GCConstants.REQ_DELIVERY_DATE, eleOdrRelease.getAttribute(GCConstants.REQ_DELIVERY_DATE));
    eleConOdrRelease.setAttribute(GCConstants.REQ_SHIP_DATE, eleOdrRelease.getAttribute(GCConstants.REQ_SHIP_DATE));
    eleConOdrRelease.setAttribute(GCConstants.SALES_ORDER_NO, eleOdrRelease.getAttribute(GCConstants.SALES_ORDER_NO));
    eleConOdrRelease.setAttribute(GCConstants.SHIP_NODE, eleOdrRelease.getAttribute(GCConstants.SHIP_NODE));
    eleConOdrRelease.setAttribute(GCConstants.ORDER_HEADER_KEY,
        eleOdrRelease.getAttribute(GCConstants.ORDER_HEADER_KEY));
    eleConOdrRelease.setAttribute(GCConstants.RELEASE_NO, eleOdrRelease.getAttribute(GCConstants.RELEASE_NO));

    YFCNodeList<YFCElement> nlOrderLine = inDoc.getElementsByTagName(GCConstants.ORDER_LINE);
    final int lengthOdrLine = nlOrderLine.getLength();
    for(int i = 0; i < lengthOdrLine; i++){
      YFCElement eleOdrLine = nlOrderLine.item(i);
      YFCElement eleConOdrLine = eleConOdrRelease.createChild(GCConstants.ORDER_LINE);
      eleConOdrLine.setAttribute(GCConstants.FULFILLMENT_TYPE, eleOdrLine.getAttribute(GCConstants.FULFILLMENT_TYPE));
      eleConOdrLine.setAttribute(GCConstants.ORDER_LINE_KEY, eleOdrLine.getAttribute(GCConstants.ORDER_LINE_KEY));
      eleConOdrLine.setAttribute(GCConstants.ORDERED_QTY, eleOdrLine.getAttribute(GCConstants.ORDERED_QTY));
      eleConOdrLine.setAttribute(GCConstants.PRIME_LINE_NO, eleOdrLine.getAttribute(GCConstants.PRIME_LINE_NO));
      eleConOdrLine.setAttribute(GCConstants.STATUS_QUANTITY, eleOdrLine.getAttribute(GCConstants.STATUS_QUANTITY));
      eleConOdrLine.setAttribute(GCConstants.SUB_LINE_NO, eleOdrLine.getAttribute(GCConstants.SUB_LINE_NO));
    }
    LOGGER.verbose("consolidateIndoc : " + consolidateIndoc.toString());
    env.setTxnObject(GCConstants.IS_STORE_SHIPMENT, GCConstants.FLAG_Y);
    final YFCDocument consolidateToShipmentOutdoc =
        GCCommonUtil.invokeAPI(env, GCConstants.CONSOLIDATE_TO_SHIPMENT_API, consolidateIndoc,
            YFCDocument.getDocumentFor(GCConstants.GET_SHIP_DETAIL_TEMPLATE));

    LOGGER.verbose("GCConsolidateToShipment.callConsolidateToShipment--End");
    LOGGER.endTimer("GCConsolidateToShipment.callConsolidateToShipment");
    return consolidateToShipmentOutdoc.getDocument();
  }

}
