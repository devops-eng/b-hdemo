package com.gc.exstore.api.picking;

import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCFetchScanSKUResponse implements YIFCustomApi {

  public Document validateScanSKU(YFSEnvironment yfsEnv, Document inputDoc) {

    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    YFCElement envelopeEle = inDoc.getDocumentElement();
    YFCElement bodyEle = envelopeEle.getChildElement(GCConstants.SOAP_BODY);
    YFCElement validateSKUEle = bodyEle.getChildElement(GCConstants.VALIDATE_SKU_ELE);
    YFCElement xmlDocInELe = validateSKUEle.getChildElement(GCConstants.XML_DOC_IN_ELE);
    YFCElement productsEle = xmlDocInELe.getChildElement(GCConstants.PRODUCTS);
    String action = productsEle.getAttribute(GCConstants.ACTION);

    YFCDocument responseDoc = YFCDocument.createDocument(GCConstants.SENVELOPE);
    YFCElement sEnvelopeEle = responseDoc.getDocumentElement();
    YFCElement sBodyEle = sEnvelopeEle.createChild(GCConstants.SBODY);
    YFCElement validateSKUResp = sBodyEle.createChild(GCConstants.VLDT_SKU_RESPONSE);
    YFCElement validateSKUResult = validateSKUResp.createChild(GCConstants.VLDT_SKU_RESULT);
    YFCElement responseEle = validateSKUResult.createChild(GCConstants.RESPONSE);

    if (YFCUtils.equals(action, "TimeOut")) {
      return responseDoc.getDocument();
    } else if (YFCUtils.equals(action, "False")) {
      YFCElement responsesEle = responseEle.createChild(GCConstants.RESPONSES);
      YFCElement lineNoEle = responsesEle.createChild(GCConstants.LINE_NUMBER);
      YFCElement productEle = productsEle.getChildElement(GCConstants.PRODUCT);
      lineNoEle.setNodeValue(productEle.getChildElement(GCConstants.LINE_NUMBER).getNodeValue());
      YFCElement success = responsesEle.createChild(GCConstants.SUCCESS);
      success.setNodeValue(GCConstants.FALSE_STR);
    } else {
      YFCIterable<YFCElement> productList = productsEle.getChildren();
      for (YFCElement productEle : productList) {

        YFCElement responsesEle = responseEle.createChild(GCConstants.RESPONSES);

        YFCElement lineNoEle = responsesEle.createChild(GCConstants.LINE_NUMBER);
        lineNoEle.setNodeValue(productEle.getChildElement(GCConstants.LINE_NUMBER).getNodeValue());
        YFCElement success = responsesEle.createChild(GCConstants.SUCCESS);
        success.setNodeValue(GCConstants.TRUE);
        YFCElement skuNo = responsesEle.createChild(GCConstants.SKU_NO_ELE);
        skuNo.setNodeValue(productEle.getChildElement(GCConstants.SKU).getNodeValue());
        YFCElement serialNo = responsesEle.createChild(GCConstants.SERIAL_NUMBER);
        serialNo.setNodeValue(productEle.getChildElement(GCConstants.INV_INPUT).getNodeValue());
        YFCElement salespersonsEle = responsesEle.createChild(GCConstants.SALES_PERSONS);
        YFCElement salesPersonEle = salespersonsEle.createChild(GCConstants.SALES_PERSON_ELE);
        salesPersonEle.setNodeValue("834798");

      }
    }
    return responseDoc.getDocument();

  }

  @Override
  public void setProperties(Properties arg0) {

  }



}
