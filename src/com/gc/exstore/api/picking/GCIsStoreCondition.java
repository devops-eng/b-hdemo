package com.gc.exstore.api.picking;

import java.util.Map;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.ycp.japi.YCPDynamicConditionEx;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCIsStoreCondition implements YCPDynamicConditionEx {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCIsStoreCondition.class.getName());

  @Override
  public boolean evaluateCondition(YFSEnvironment env, String arg1, Map arg2, Document inDoc) {

    LOGGER.beginTimer("GCIsStoreCondition.evaluateCondition");
    LOGGER.verbose("Class: GCIsStoreCondition Method: evaluateCondition BEGIN");

    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("initial Indoc : " + inputDoc.toString());
    }
    YFCElement eleOdrRelease = inputDoc.getDocumentElement();
    final String shipNode = eleOdrRelease.getAttribute(GCConstants.SHIP_NODE);

    boolean isStore = true;
    // preparing input for getOrganizationHeirarchy API call
    final YFCDocument orgHeirarchyIndocYfc = YFCDocument.createDocument(GCConstants.ORGANIZATION);
    final YFCElement eleOrganization = orgHeirarchyIndocYfc.getDocumentElement();
    eleOrganization.setAttribute(GCXmlLiterals.ORGANIZATION_KEY, shipNode);

    /*
     * Calling getOrganizationHeirarchy API. If there is any exception, then catch it & throw a
     * custom exception
     */
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("loginIndoc : " + orgHeirarchyIndocYfc.getString());
    }
    final YFCDocument getOrgHeirarchyTmpl = YFCDocument.getDocumentFor((GCConstants.GET_ORG_HEIRARCHY_TEMPLATE));
    final YFCDocument orgHeirarchyOutdocYfc =
        GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORG_HIERARCHY, orgHeirarchyIndocYfc, getOrgHeirarchyTmpl);


    final YFCElement eleOrganizationYfc = orgHeirarchyOutdocYfc.getDocumentElement();
    if (!YFCCommon.isVoid(eleOrganizationYfc)) {
      final YFCElement eleNodeYfc = eleOrganizationYfc.getChildElement(GCConstants.NODE);

      // Checking if NodeType is not equal to "Store" then return false
      if (!YFCCommon.isVoid(eleNodeYfc)
          && (!(GCConstants.STORE.equals(eleNodeYfc.getAttribute(GCXmlLiterals.NODE_TYPE))))) {
        isStore = false;
      } else {
        isStore = true;
      }
    }
    LOGGER.verbose("isStore : " + isStore);
    LOGGER.endTimer("GCIsStoreCondition.evaluateCondition");
    return isStore;
  }

  @Override
  public void setProperties(Map arg0) {

  }

}
