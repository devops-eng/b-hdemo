package com.gc.exstore.api.picking;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCWebServiceUtil;
import com.gc.exstore.utils.GCShipmentUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class is used to call GCGetPOSCustomerID service for POS webservice call. This class posts
 * an open pick message to POS.
 *
 * @author shanil.sharma
 *
 */
public class GCManagePOSCustomerDetails {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCManagePOSCustomerDetails.class.getName());

  /**
   * This method is used to call getOrderList to fetch BillToID and call GCGetPOSCustomerID service
   * for POS webservice call. And update the POSCustomerID into the database.
   *
   * @param env
   * @param inDoc
   */
  public void managePOSCustomerID(YFSEnvironment env, Document inDoc) {

    LOGGER.beginTimer("GCManagePOSCustomerDetails.managePOSCustomerID");
    LOGGER.verbose("GCManagePOSCustomerDetails.managePOSCustomerID--Begin");
    final YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    final YFCElement eleShipment = inputDoc.getDocumentElement();
    final YFCElement eleShipLines = eleShipment.getChildElement(GCConstants.SHIPMENT_LINES);
    final YFCElement eleShipLine = eleShipLines.getElementsByTagName(GCConstants.SHIPMENT_LINE).item(0);
    final String orderHeaderKey = eleShipLine.getAttribute(GCConstants.ORDER_HEADER_KEY);

    // calling getOrderList
    YFCDocument templatedoc = YFCDocument.getDocumentFor(GCConstants.GET_ODR_LIST_CUST_TEMP);
    final YFCDocument getOdrLstOutDoc = GCCommonUtil.invokeGetOrderListAPI(env, orderHeaderKey, templatedoc);
    final YFCElement eleOdrList = getOdrLstOutDoc.getDocumentElement();
    final YFCElement eleOrder = eleOdrList.getChildElement(GCConstants.ORDER);
    final String billToID = eleOrder.getAttribute(GCConstants.BILL_TO_ID);
    final String orgCode = eleOrder.getAttribute(GCConstants.ENTERPRISE_CODE);

    // Prepare doc for Customer Validation
    YFCDocument customerDoc = YFCDocument.createDocument(GCConstants.CUSTOMER);
    final YFCElement eleCustomer = customerDoc.getDocumentElement();
    eleCustomer.setAttribute(GCConstants.ORGANIZATION_CODE, orgCode);
    eleCustomer.setAttribute(GCConstants.CUSTOMER_ID, billToID);

    // Calling GCGetPOSCustomerID Service
    YFCDocument posCustIDDoc = GCCommonUtil.invokeService(env, GCConstants.GET_POS_CUST_ID_SERVICE, customerDoc);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("posCustIDDoc : " + posCustIDDoc.toString());
    }

    final YFCElement eleCustomerOutDoc = posCustIDDoc.getDocumentElement();
    final YFCElement eleCustomerExtn = eleCustomerOutDoc.getChildElement(GCConstants.EXTN);
    if (!YFCCommon.isVoid(eleCustomerExtn)) {
      YFCDocument chgOrderDoc = YFCDocument.createDocument(GCConstants.ORDER);
      final YFCElement eleChgOrder = chgOrderDoc.getDocumentElement();
      eleChgOrder.setAttribute(GCConstants.ACTION, GCConstants.ACTION_MODIFY);
      eleChgOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
      eleChgOrder.setAttribute(GCConstants.SELECT_METHOD, "WAIT");
      eleChgOrder.importNode(eleCustomerExtn);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("chgOrderDoc : " + chgOrderDoc.toString());
      }
      GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, chgOrderDoc.getDocument());
      // calling service to change Shipment Status to Ready for Backroom Pick
      GCCommonUtil.invokeService(env, GCConstants.READY_FOR_BCKRM_PCK_SERVICE, inDoc);

      // call changeOrderStatus
      callChangeOrderStatus(env, eleShipment, orderHeaderKey);
      updateWarrantyLines(env, inputDoc, eleOrder);
    } else {
      YFCException yfce = new YFCException(GCErrorConstants.POS_CONNECTION_FAILURE);
      yfce.setAttribute(GCConstants.CUSTOMER_ID, billToID);
      throw yfce;
    }


    // Invoking service to post open pick message to POS
    final String orderNo = eleShipLine.getAttribute(GCConstants.ORDER_NO);
    final String shipNode = eleShipment.getAttribute(GCConstants.SHIP_NODE);
    postOpenPickMessagePOS(env, orderNo, shipNode);

    LOGGER.verbose("GCManagePOSCustomerDetails.managePOSCustomerID--end");
    LOGGER.endTimer("GCManagePOSCustomerDetails.managePOSCustomerID");
  }

  /**
   * This method is used to call changeorderStatus API
   *
   * @param env
   * @param eleShipment
   * @param orderHeaderKey
   * @param orderReleaseKey
   */
  private void callChangeOrderStatus(YFSEnvironment env, YFCElement eleShipment, String orderHeaderKey) {

    LOGGER.beginTimer("GCManagePOSCustomerDetails.callChangeOrderStatus");
    LOGGER.verbose("GCManagePOSCustomerDetails.callChangeOrderStatus--Starts");
    // Create changeOrderStatus input document
    YFCDocument chngOrdStatusIndoc = YFCDocument.createDocument(GCConstants.ORDER_STATUS_CHANGE);
    YFCElement ordStatusChngEle = chngOrdStatusIndoc.getDocumentElement();
    ordStatusChngEle.setAttribute(GCConstants.DOCUMENT_TYPE, GCConstants.SALES_ORDER_DOCUMENT_TYPE);
    ordStatusChngEle.setAttribute(GCConstants.ENTERPRISE_CODE, eleShipment.getAttribute(GCConstants.ENTERPRISE_CODE));
    ordStatusChngEle.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
    ordStatusChngEle.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.CHNG_STATUS_BCKRM_PCK_TRNSCTN);
    YFCElement eleOrderLinesStatus = ordStatusChngEle.createChild(GCConstants.ORDER_LINES);

    final YFCElement eleShipLines = eleShipment.getChildElement(GCConstants.SHIPMENT_LINES);
    YFCIterable<YFCElement> nlShipmentLine = eleShipLines.getChildren();
    for (YFCElement eleShipLine : nlShipmentLine) {

      if (!YFCCommon.isVoid(eleShipLine.getAttribute(GCConstants.ORDER_RELEASE_KEY))) {
        YFCElement eleOrderLineStatus = eleOrderLinesStatus.createChild(GCConstants.ORDER_LINE);
        eleOrderLineStatus.setAttribute(GCConstants.ORDER_LINE_KEY,
            eleShipLine.getAttribute(GCConstants.ORDER_LINE_KEY));
        eleOrderLineStatus.setAttribute(GCConstants.QUANTITY, eleShipLine.getAttribute(GCConstants.QUANTITY));
        eleOrderLineStatus.setAttribute(GCConstants.ORDER_RELEASE_KEY,
            eleShipLine.getAttribute(GCConstants.ORDER_RELEASE_KEY));
        eleOrderLineStatus.setAttribute(GCConstants.BASE_DROP_STATUS, GCConstants.SENT_TO_FULFILLMENT_CENTER_STATUS);
      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("chngOrdStatusIndoc : " + chngOrdStatusIndoc.toString());
    }
    GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER_STATUS, chngOrdStatusIndoc.getDocument());
    LOGGER.verbose("GCManagePOSCustomerDetails.callChangeOrderStatus--end");
    LOGGER.endTimer("GCManagePOSCustomerDetails.callChangeOrderStatus");
  }

  /**
   * This method is used to post an open pick message to POS when a shipment moves to ready for
   * backroom pick up status-GCSTORE-209,843
   *
   * @param env
   * @param sOrderNo
   * @param sShipNode
   */
  private void postOpenPickMessagePOS(YFSEnvironment env, String sOrderNo, String sShipNode) {
    LOGGER.beginTimer("GCManagePOSCustomerDetails.postOpenPickMessagePOS");
    LOGGER.verbose("GCManagePOSCustomerDetails.postOpenPickMessagePOS--Begin");

    YFCDocument posOpenPickDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    final YFCElement elePostOpenPick = posOpenPickDoc.getDocumentElement();
    elePostOpenPick.setAttribute(GCConstants.ORDER_NO, sOrderNo);
    elePostOpenPick.setAttribute(GCConstants.SHIP_NODE, sShipNode);
    GCCommonUtil.invokeService(env, GCConstants.POST_PICK_MESSAGE_POS_SERVICE, posOpenPickDoc.getDocument());

    LOGGER.verbose("GCManagePOSCustomerDetails.postOpenPickMessagePOS--end");
    LOGGER.endTimer("GCManagePOSCustomerDetails.postOpenPickMessagePOS");
  }

  public void updateWarrantyLines(YFSEnvironment env, YFCDocument inDoc, YFCElement eleOrder) {
    boolean isWarrShipment = true;
    boolean parentShipped = false;
    String parentLineKey = null;
    YFCElement eleShipment = inDoc.getDocumentElement();
    YFCElement eleShipmentLines = eleShipment.getChildElement(GCConstants.SHIPMENT_LINES);
    YFCNodeList<YFCElement> nlShipmentLine = eleShipmentLines.getElementsByTagName(GCConstants.SHIPMENT_LINE);
    List<String> orderLineKeyList = GCShipmentUtil.getOrderLineKeyList(nlShipmentLine);
    YFCElement eleOrderLines = eleOrder.getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> nlOrderLine = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
    String orderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
    for (YFCElement eleOrderLine : nlOrderLine) {
      YFCElement eleExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
      String isWarranty = eleExtn.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
      String ordLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      parentLineKey = eleOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
      if (YFCUtils.equals(isWarranty, GCConstants.NO) && orderLineKeyList.contains(ordLineKey)) {
        isWarrShipment=false;
      }
    }
    if(isWarrShipment){
      validateSKU(env, eleShipment , orderHeaderKey);
      for (YFCElement eleOrderLine : nlOrderLine) {
        String orderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
        if(YFCUtils.equals(orderLineKey, parentLineKey)){
          String status = eleOrderLine.getAttribute(GCConstants.MAX_LINE_STATUS);
          status = GCShipmentUtil.checkAndConvertOrderStatus(status);
          Double dStatus = Double.parseDouble(status);
          Double shippedStatus = Double.parseDouble(GCConstants.SHIPPED_STATUS);
          if (dStatus >= shippedStatus) {
            parentShipped = true;
          }
          break;
        }
      }
    }




    if (parentShipped) {
      YFCElement eleInput = inDoc.getDocumentElement();
      String shipmentKey = eleInput.getAttribute(GCConstants.SHIPMENT_KEY);
      String shipNode = eleInput.getAttribute(GCConstants.SHIP_NODE);
      YFCDocument confirmShpIp = YFCDocument.createDocument(GCConstants.SHIPMENT);
      YFCElement eleRoot = confirmShpIp.getDocumentElement();
      eleRoot.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
      eleRoot.setAttribute(GCConstants.DOCUMENT_TYPE, GCConstants.ZERO_ZERO_ZERO_ONE);
      eleRoot.setAttribute(GCConstants.ENTERPRISE_CODE, GCConstants.GC);
      eleRoot.setAttribute(GCConstants.SHIP_NODE, shipNode);
      eleRoot.setAttribute(GCConstants.BACKORDER_NON_SHIPPED_QTY, GCConstants.YES);
      YFCElement eleShipmentlinesConfShp = eleRoot.createChild(GCConstants.SHIPMENT_LINES);
      for (YFCElement eleShipmentLineIp : nlShipmentLine) {
        String shipmentLineNo = eleShipmentLineIp.getAttribute(GCConstants.SHIPMENT_LINE_NO);
        String primeLineNo = eleShipmentLineIp.getAttribute(GCConstants.PRIME_LINE_NO);
        String subLineNo = eleShipmentLineIp.getAttribute(GCConstants.SUB_LINE_NO);
        String orderNo = eleShipmentLineIp.getAttribute(GCConstants.ORDER_NO);
        String qty = eleShipmentLineIp.getAttribute(GCConstants.QUANTITY);
        String itemId = eleShipmentLineIp.getAttribute(GCConstants.ITEM_ID);
        String productClass = eleShipmentLineIp.getAttribute(GCConstants.PRODUCT_CLASS);
        String uom = eleShipmentLineIp.getAttribute(GCConstants.UNIT_OF_MEASURE);
        YFCElement eleShipmentLine = eleShipmentlinesConfShp.createChild(GCConstants.SHIPMENT_LINE);
        eleShipmentLine.setAttribute(GCConstants.SHIPMENT_LINE_NO, shipmentLineNo);
        eleShipmentLine.setAttribute(GCConstants.PRIME_LINE_NO, primeLineNo);
        eleShipmentLine.setAttribute(GCConstants.ORDER_NO, orderNo);
        eleShipmentLine.setAttribute(GCConstants.QUANTITY, qty);
        eleShipmentLine.setAttribute(GCConstants.ITEM_ID, itemId);
        eleShipmentLine.setAttribute(GCConstants.PRODUCT_CLASS, productClass);
        eleShipmentLine.setAttribute(GCConstants.SUB_LINE_NO, subLineNo);
        eleShipmentLine.setAttribute(GCConstants.UNIT_OF_MEASURE, uom);
      }
      YFCDocument opTemp = YFCDocument.getDocumentFor(GCConstants.CONFIRM_SHIP_TEMP);
      YFCDocument confirmShpOp = GCCommonUtil.invokeAPI(env, GCConstants.CONFIRM_SHIPMENT_API, confirmShpIp, opTemp);
      YFCElement eleShpInvoiceIp = confirmShpOp.getDocumentElement();
      eleShpInvoiceIp.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.CREATE_SHPMNT_INVC);
      YFCDocument createShpInvpiceOpTemp = YFCDocument.createDocument(GCConstants.API_SUCCESS);

      GCCommonUtil.invokeAPI(env, GCConstants.CREATE_SHPMNT_INVC_API, confirmShpOp, createShpInvpiceOpTemp);
    }
  }



  private void validateSKU(YFSEnvironment env, YFCElement eleShipment, String orderHeaderKey) {

    Document scanSKUResponse = null;
    YFCDocument soapResponse;
    Map<String, String> lineMap = new HashMap<String, String>();

    YFCDocument soapRequest = YFCDocument.getDocumentFor(GCConstants.SCAN_SKU_REQUEST);
    YFCElement envelopeEle = soapRequest.getDocumentElement();
    YFCElement bodyEle = envelopeEle.getChildElement(GCConstants.SOAP_BODY);
    YFCElement validateSKUEle = bodyEle.getChildElement(GCConstants.VALIDATE_SKU_ELE);
    YFCElement xmlDocInELe = validateSKUEle.getChildElement(GCConstants.XML_DOC_IN_ELE);
    YFCElement productsEle = xmlDocInELe.getChildElement(GCConstants.PRODUCTS);
    productsEle.setAttribute(GCConstants.XMLNS, GCConstants.BLANK_STRING);
    YFCElement eleShipmentLines = eleShipment.getChildElement(GCConstants.SHIPMENT_LINES);
    YFCNodeList<YFCElement> nlShipmentLine = eleShipmentLines.getElementsByTagName(GCConstants.SHIPMENT_LINE);
    for (YFCElement eleShipmentLine : nlShipmentLine) {
      YFCElement productEle = productsEle.createChild(GCConstants.PRODUCT);

      YFCElement typeEle = productEle.createChild(GCConstants.TYPE);
      typeEle.setNodeValue(GCConstants.REGULAR);

      String lineNo = eleShipmentLine.getAttribute(GCConstants.SHIPMENT_LINE_NO);
      String orderLineKey = eleShipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      lineMap.put(lineNo, orderLineKey);
      YFCElement shipmentLineNumber = productEle.createChild(GCConstants.LINE_NUMBER);
      shipmentLineNumber.setNodeValue(lineNo);

      String shipNode = eleShipment.getAttribute(GCConstants.SHIP_NODE);
      YFCElement posLocationEle = productEle.createChild(GCConstants.POS_LOCATION);
      posLocationEle.setNodeValue(shipNode);

      String itemID = eleShipmentLine.getAttribute(GCConstants.ITEM_ID);
      String unitOfMeasure = eleShipmentLine.getAttribute(GCConstants.UNIT_OF_MEASURE);
      YFCDocument itemListOptemp = YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LIST_PICKUP_TMP);
      YFCDocument getItemListOp =
          GCCommonUtil.getItemList(env, itemID, unitOfMeasure, GCConstants.GCI, true, itemListOptemp);
      YFCElement eleRoot = getItemListOp.getDocumentElement();
      YFCElement eleItem = eleRoot.getElementsByTagName(GCConstants.ITEM).item(0);
      YFCElement eleExtnItem = eleItem.getChildElement(GCConstants.EXTN);
      String posItemID = eleExtnItem.getAttribute(GCConstants.EXTN_POSITEM_ID);
      YFCElement skuEle = productEle.createChild(GCConstants.SKU);
      skuEle.setNodeValue(posItemID);

      YFCElement invInputEle = productEle.createChild(GCConstants.INV_INPUT);
      invInputEle.setNodeValue(posItemID);

      String quantity = eleShipmentLine.getAttribute(GCConstants.QUANTITY);
      YFCElement qtyEle = productEle.createChild(GCConstants.QUANTITY);
      qtyEle.setNodeValue(quantity);



      String sSoapAction = YFSSystem.getProperty(GCConstants.POS_SCAN_SKU_SOAP_ACTION);
      LOGGER.verbose("SOAP Action :" + sSoapAction);
      String sSoapUrl = YFSSystem.getProperty(GCConstants.POS_SCAN_SKU_WEB_SERVICE_URL);
      LOGGER.verbose("SOAP URL :" + sSoapUrl);
      GCWebServiceUtil obj = new GCWebServiceUtil();

      try {

        scanSKUResponse = obj.invokeSOAPWebService(soapRequest.getDocument(), sSoapUrl, sSoapAction);

        if (!YFCCommon.isVoid(scanSKUResponse)) {

          soapResponse = YFCDocument.getDocumentFor(scanSKUResponse);
          LOGGER.verbose("SOAP Response obtained :" + soapResponse.toString());
          YFCElement sEnvelopeEle = soapResponse.getDocumentElement();
          YFCElement sBodyEle = sEnvelopeEle.getChildElement(GCConstants.SBODY);
          YFCElement validateSKUResp = sBodyEle.getChildElement(GCConstants.VLDT_SKU_RESPONSE);

          if (!YFCCommon.isVoid(validateSKUResp)) {
            stampSkuNumber(env, validateSKUResp, lineMap, orderHeaderKey);

          } else {

            YFCException yfce = new YFCException(GCErrorConstants.EXTN_POS_SCAN_FAILED);
            throw yfce;

          }
        }

      } catch (Exception e) {
        LOGGER.error("Got exception in authenticate method", e);
        if (!YFCCommon.isVoid(scanSKUResponse)) {
          YFCException yfce = new YFCException(GCErrorConstants.EXTN_POS_SCAN_FAILED);
          throw yfce;
        } else {
          YFCException yfce = new YFCException(GCErrorConstants.EXTN_POS_CONNECT_TIMEOUT);
          throw yfce;
        }

      }


    }

  }


  private void stampSkuNumber(YFSEnvironment env, YFCElement validateSKUResp, Map<String, String> lineMap,
      String orderHeaderKey) {

    YFCDocument changeOrderDoc = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleRoot = changeOrderDoc.getDocumentElement();
    eleRoot.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
    YFCElement eleOrderLines = eleRoot.createChild(GCConstants.ORDER_LINES);
    YFCElement validateSKUResult = validateSKUResp.getChildElement(GCConstants.VLDT_SKU_RESULT);
    String response = validateSKUResult.getNodeValue();
    YFCDocument responseDoc = YFCDocument.getDocumentFor(response);
    LOGGER.verbose("Response Doc fetched :" +responseDoc.toString());
    YFCElement responseRootEle = responseDoc.getDocumentElement();
    YFCElement responseEle = responseRootEle.getChildElement(GCConstants.RESPONSES);
    YFCElement success = responseEle.getChildElement(GCConstants.SUCCESS);
    if (YFCUtils.equals(GCConstants.FALSE_STR, success.getNodeValue())) {
      YFCElement errorInfo = responseEle.getChildElement(GCConstants.ERROR_INFO);
      YFCException yfce = new YFCException(GCErrorConstants.EXTN_POS_SCAN_FAILED);
      yfce.setErrorDescription(GCConstants.POS_ERR_MSG + errorInfo.getNodeValue());
      throw yfce;
    } else{
      YFCIterable<YFCElement> responsesList = responseRootEle.getChildren();
      for (YFCElement responsesEle : responsesList) {
        YFCElement lineNoEle = responsesEle.getChildElement(GCConstants.LINE_NUMBER);
        String lineNo = lineNoEle.getNodeValue();
        String orderLineKey = lineMap.get(lineNo);
        YFCElement skuNoEle = responsesEle.getChildElement(GCConstants.SKU_NO_ELE);
        String skuNumber = skuNoEle.getNodeValue();
        YFCElement eleOrderLine = eleOrderLines.createChild(GCConstants.ORDER_LINE);
        eleOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey);
        YFCElement eleExtn = eleOrderLine.createChild(GCConstants.EXTN);
        eleExtn.setAttribute(GCConstants.EXTN_POS_SKU_NO, skuNumber);
      }
      YFCDocument opTemp = YFCDocument.getDocumentFor(GCConstants.CHANGE_ORDER_TEMP);
      GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, changeOrderDoc, opTemp);

    }
  }
}

