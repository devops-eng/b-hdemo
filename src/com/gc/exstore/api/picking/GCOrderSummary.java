/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 * *###########################################
 * ######################################################
 * ################################################################ OBJECTIVE: The class provides
 * the Order Details to be displayed on the UI
 * ######################################################
 * ############################################
 * ############################################################### Version Date Modified By
 * Description
 * ######################################################################################
 * ########################################################################### 1.0 06/02/2014
 * Rangarajan, Shwetha The class provides the necessary Order Details to be published on the screen
 * #################################################################################################
 * ################################################################
 */
package com.gc.exstore.api.picking;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * The class contains logic to fetch the Order Details for the incoming request.
 *
 * @author <a href="mailto:shwetha.rangarajan@expicient.com">Shwetha</a>
 */
public class GCOrderSummary implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCOrderSummary.class.getName());

  /**
   * The method calls getOrderList to fetch Order details and modifies the output as required.
   *
   * @param yfsEnv
   * @param inputDoc
   * @return
   */
  public Document getOrderSummary(YFSEnvironment yfsEnv, Document inputDoc) {

    LOGGER.beginTimer("GCOrderSummary.getOrderSummary");
    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    YFCDocument getOrderListTemp = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_TEMP);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input to the method : " + inDoc.toString());
      LOGGER.verbose("getOrderList API template :" + getOrderListTemp.toString());
    }
    YFCDocument getOrderListOutDoc = GCCommonUtil.invokeAPI(yfsEnv, GCConstants.GET_ORDER_LIST, inDoc, getOrderListTemp);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getOrderList API output :" + getOrderListOutDoc.toString());
    }
    YFCElement orderListEle = getOrderListOutDoc.getDocumentElement();
    YFCElement orderEle = orderListEle.getChildElement(GCConstants.ORDER);
    orderEle.setAttribute(GCConstants.IS_ORDER_SUMMARY, GCConstants.YES);

    // Fix for Displaying FTCStartDate
    YFCElement orderDates = orderEle.getChildElement(GCConstants.ORDER_DATES);
    YFCIterable<YFCElement> orderDateList = orderDates.getChildren();
    for (YFCElement ordDate : orderDateList) {
      String dateTypeId = ordDate.getAttribute(GCConstants.DATE_TYPE_ID);
      if(YFCUtils.equals(GCConstants.BACKORD_NOTIFY_REF_DATE, dateTypeId)) {
        String actualDate = ordDate.getAttribute(GCConstants.ACTUAL_DATE);
        orderEle.setAttribute(GCConstants.ACTUAL_DATE, actualDate);
      }
    }
    //FTCStartDate changes End

    // Fetching LevelOfService Description from CommonCode & stamp it in order element
    GCPickDetailAPI obj = new GCPickDetailAPI();
    final String levelOfServiceDesc =
        obj.callCommonCodeList(yfsEnv, orderEle.getAttribute(GCConstants.LEVEL_OF_SERVICE));
    orderEle.setAttribute(GCConstants.LEVEL_OF_SERVICE, levelOfServiceDesc);

    Map<String, String> shipNodeMap = new HashMap<String, String>();

    YFCElement orderLinesEle = orderEle.getChildElement(GCConstants.ORDER_LINES);

    if (!YFCCommon.isVoid(orderLinesEle) && orderLinesEle.hasChildNodes()) {
      YFCElement orderLineEle = orderLinesEle.getChildElement(GCConstants.ORDER_LINE);

      // Fetching fulfillmentType from common code
      GCBeforeCreateShipmentUE ftObj = new GCBeforeCreateShipmentUE();
      String fulfilmentType = ftObj.getFulfillmentType(yfsEnv, orderLineEle.getAttribute(GCConstants.FULFILLMENT_TYPE));
      orderEle.setAttribute(GCConstants.FULFILLMENT_TYPE, fulfilmentType);

      YFCIterable<YFCElement> orderLineList = orderLinesEle.getChildren();
      for (YFCElement orderLine : orderLineList) {

        YFCElement extnEle = orderLine.getChildElement(GCConstants.EXTN);
        YFCElement orderInvAttReq = orderLine.getChildElement(GCConstants.ORDER_INV_ATTR_REQ);
        String batchNo = GCConstants.BLANK_STRING;
        if (!YFCCommon.isVoid(orderInvAttReq)) {
          batchNo = orderInvAttReq.getAttribute(GCConstants.BATCH_NO);
        }
        String extnActualSerialNo = extnEle.getAttribute(GCConstants.EXTN_ACTUAL_SERIAL_NO);
        if (YFCCommon.isVoid(extnActualSerialNo) && !YFCCommon.isVoid(batchNo)) {
          extnEle.setAttribute(GCConstants.EXTN_ACTUAL_SERIAL_NO, batchNo);
        }
        //GCTSTORE-6200-starts
        String shipNode=null;
        String strIsBundleParent=orderLine.getAttribute("IsBundleParent");
        LOGGER.debug("strIsBundleParent :: "+strIsBundleParent);
        if(!YFCCommon.isVoid(strIsBundleParent) && GCConstants.YES.equals(strIsBundleParent))
        {
            shipNode=GCXMLUtil.getAttributeFromXPath(getOrderListOutDoc.getDocument(), "//BundleParentLine[@PrimeLineNo='"+orderLine.getAttribute("PrimeLineNo")+"']/../@ShipNode");
            LOGGER.debug("Inside if condition :: ShipNode ::"+shipNode);
        }
        else
        {
            shipNode=orderLine.getAttribute("ShipNode");
            LOGGER.debug("Inside else condition :: ShipNode ::"+shipNode);
        }
        //GCTSTORE-6200-ends 

        //YFCElement orderStatusesEle = orderLine.getChildElement(GCConstants.ORDER_STATUSES);
       // YFCElement orderStatusEle = orderStatusesEle.getChildElement(GCConstants.ORDER_STATUS);
      //  String shipNode = orderStatusEle.getAttribute(GCConstants.SHIP_NODE);
        if (shipNodeMap.containsKey(shipNode)) {
          if(YFCUtils.equals(GCConstants.MFI, shipNode)){
            orderLine.setAttribute(GCConstants.SHIP_NODE_DESC, "KCDC");
          } else {
            orderLine.setAttribute(GCConstants.SHIP_NODE_DESC, shipNode + "-" + shipNodeMap.get(shipNode));
          }
        } else {

          YFCDocument getShipNodeListIndoc = YFCDocument.createDocument(GCConstants.SHIP_NODE);
          YFCElement shipNodeEle = getShipNodeListIndoc.getDocumentElement();
          shipNodeEle.setAttribute(GCConstants.SHIP_NODE, shipNode);
          YFCDocument getShipNodeListTemp = YFCDocument.getDocumentFor(GCConstants.GET_SHIP_NODE_TEMP);
          YFCDocument getShipNodeListOutdoc =
              GCCommonUtil.invokeAPI(yfsEnv, GCConstants.GET_SHIP_NODE_LIST, getShipNodeListIndoc, getShipNodeListTemp);
          YFCElement shipNodeList = getShipNodeListOutdoc.getDocumentElement();
          YFCElement shipNodeOutEle = shipNodeList.getChildElement(GCConstants.SHIP_NODE);
          if(YFCUtils.equals(GCConstants.MFI, shipNode)){
            orderLine.setAttribute(GCConstants.SHIP_NODE_DESC, "KCDC");
          } else {
            orderLine.setAttribute(GCConstants.SHIP_NODE_DESC,
                shipNode + "-" + shipNodeOutEle.getAttribute(GCConstants.DESCRIPTION));
          }
          shipNodeMap.put(shipNodeOutEle.getAttribute(GCConstants.SHIP_NODE),
              shipNodeOutEle.getAttribute(GCConstants.DESCRIPTION));
        }
      }
    }


    YFCElement paymentMthdsEle = orderEle.getChildElement(GCConstants.PAYMENT_METHODS);
    if (!YFCCommon.isVoid(paymentMthdsEle) && paymentMthdsEle.hasChildNodes()) {

      YFCIterable<YFCElement> paymentMthdsList = paymentMthdsEle.getChildren();
      for (YFCElement paymentMthdEle : paymentMthdsList) {
        String creditCardNo = paymentMthdEle.getAttribute(GCConstants.DISPLAY_CREDIT_CARD_NUM);
        paymentMthdEle.setAttribute(GCConstants.DISPLAY_CREDIT_CARD_NUM, GCConstants.DISPLAY_CC_NO + creditCardNo);
      }

    }

    YFCElement notesEle = orderEle.getChildElement(GCConstants.NOTES);
    if (YFCCommon.isVoid(notesEle)) {
      orderEle.createChild(GCConstants.NOTES);
      notesEle = orderEle.getChildElement(GCConstants.NOTES);
      notesEle.createChild(GCConstants.NOTE);
    } else if (!notesEle.hasChildNodes()) {
      notesEle.createChild(GCConstants.NOTE);
    }

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getOrderList API output after modifying Credit Card No and "
          + "Stamping fulfilment type at Order level :" + getOrderListOutDoc.toString());
    }
    LOGGER.verbose("GCOrderSummary.getOrderSummary : END");
    LOGGER.endTimer("GCOrderSummary.getOrderSummary");
    return getOrderListOutDoc.getDocument();
  }

  @Override
  public void setProperties(Properties arg0) {
    // Used to fetch property variables from customer_overrides file.

  }

}
