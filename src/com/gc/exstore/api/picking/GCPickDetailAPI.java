package com.gc.exstore.api.picking;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class prepares a custom doc by appending all the components shipment lines to its
 * corresponding bundle parent.
 *
 * @author shanil.sharma
 *
 */
public class GCPickDetailAPI {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCPickDetailAPI.class);

  /**
   * This method is used to prepare the custom document for UI by calling getShipmentDetails API.
   *
   * @param env
   * @param inputDoc
   * @return
   */
  public Document preparePickDetailDocument(YFSEnvironment env, Document inputDoc) {

    LOGGER.beginTimer("PickDetailAPI.preparePickDetailDocument");
    LOGGER.verbose("PickDetailAPI.preparePickDetailDocument--Begin");
    // Calling getShipemntDetail api
    YFCDocument tempdoc = YFCDocument.getDocumentFor(GCConstants.GET_SHIP_DETAIL_TEMPLATE);
    YFCDocument getShipDtlOutdoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_DTL, YFCDocument.getDocumentFor(inputDoc), tempdoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getShipDtlOutdoc:" + getShipDtlOutdoc.toString());
    }

    // This Map is used to store the parentShipmentLineKey as key & the ShipmentLine Element as the
    // value of Bundle components.
    Map<String, YFCElement> componentsMap = new HashMap<String, YFCElement>();

    final YFCElement eleShipment = getShipDtlOutdoc.getDocumentElement();

    final YFCElement eleShipLines = eleShipment.getChildElement(GCConstants.SHIPMENT_LINES);
    final YFCNodeList<YFCElement> listShipmentLine = eleShipLines.getElementsByTagName(GCConstants.SHIPMENT_LINE);

    // Started preparing a custom doc
    final YFCDocument finalDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    final YFCElement eleShipmentOutdoc = finalDoc.getDocumentElement();

    // This map is used to store DependentOnLineKey & warranty line
    Map<String, YFCElement> warrantylistMap = fetchWarrantyItems(env, eleShipment, finalDoc);

    final String levelOfServiceDesc = callCommonCodeList(env, eleShipment.getAttribute(GCConstants.LEVEL_OF_SERVICE));

    // Stamping Attributes
    stampAttributes(eleShipmentOutdoc, eleShipment, levelOfServiceDesc);

    final String orderHeaderKey = eleShipment.getAttribute(GCConstants.ORDER_HEADER_KEY);
    // calling getGCOrderCommissionListService and stamping userName in the custom doc
    final String userName = getGCOrderCommissionListService(env, orderHeaderKey);
    eleShipmentOutdoc.setAttribute(GCXmlLiterals.USER_NAME, userName);

    eleShipmentOutdoc.importNode(eleShipment.getChildElement(GCXmlLiterals.INSTRUCTIONS));
    final YFCElement eleShipmentLinesOutdoc = eleShipmentOutdoc.createChild(GCConstants.SHIPMENT_LINES);
    final int lengthShipLine = listShipmentLine.getLength();

    // Fetching Order/Extn Element to stamp orderingStore value
    final YFCElement eleShipLineOrder = listShipmentLine.item(0).getChildElement(GCConstants.ORDER);
    final YFCElement eleShipLineOrderExtn = eleShipLineOrder.getChildElement(GCConstants.EXTN);
    eleShipmentOutdoc.setAttribute(GCConstants.ORDERING_STORE,
        eleShipLineOrderExtn.getAttribute(GCConstants.EXTN_ORDERING_STORE));

    //Fetch PaymentMethods
    final String paymentMethod = getPaymentMethod(eleShipLineOrder);
    eleShipmentOutdoc.setAttribute(GCConstants.PAYMENT_TYPE, paymentMethod);

    // Importing all those shipment lines from the getShipmentDetail output doc which are not a
    // Bundle Component & saving those Bundle component shipmentLines in the map.
    LOGGER.verbose("lengthShipLine: " + lengthShipLine);
    BigDecimal totalAmt = new BigDecimal(GCConstants.ZERO_AMOUNT);
    for (int i = 0; i < lengthShipLine; i++) {
      final YFCElement eleShipLine = listShipmentLine.item(i);
      eleShipLine.setAttribute(GCConstants.LEVEL_OF_SERVICE, levelOfServiceDesc);

      // converting Quantity into integer value
      final int qty = Float.valueOf(eleShipLine.getAttribute(GCConstants.QUANTITY)).intValue();
      eleShipLine.setAttribute(GCConstants.QUANTITY, qty);


      final YFCElement eleShipOdrLine = eleShipLine.getChildElement(GCConstants.ORDER_LINE);
      final YFCElement eleShipItemDetails = eleShipOdrLine.getChildElement(GCConstants.ITEM_DETAILS);
      final YFCElement eleShipItemPrimaryInfo = eleShipItemDetails.getChildElement(GCConstants.PRIMARY_INFORMATION);
      final String imgLocation = eleShipItemPrimaryInfo.getAttribute(GCConstants.IMG_LOCATION);
      final String imgID = eleShipItemPrimaryInfo.getAttribute(GCConstants.IMG_ID);
      eleShipLine.setAttribute(GCConstants.IMG_URL, imgLocation + "/" + imgID);
      final YFCElement eleShipExtnOdrLine = eleShipOdrLine.getChildElement(GCConstants.EXTN);
      final String quantity = eleShipLine.getAttribute(GCConstants.QUANTITY);
      final YFCElement eleShipItemDetailsExtn = eleShipItemDetails.getChildElement(GCConstants.EXTN);

      // Setting Item Flags
      eleShipLine.setAttribute(GCConstants.IS_VINTAGE_ITEM,
          eleShipItemDetailsExtn.getAttribute(GCConstants.EXTN_VINTAGE_ITEM));
      if (!YFCCommon.isVoid(eleShipLine.getAttribute(GCConstants.REQ_TAG_NO))) {
        eleShipLine.setAttribute(GCConstants.IS_SERIALIZED, eleShipItemDetailsExtn.getAttribute("ExtnIsPlatinumItem"));
      } else {
        eleShipLine.setAttribute(GCConstants.IS_SERIALIZED, GCConstants.N);
      }

      // calculating unit price
      BigDecimal bdUnitPrice = calculateUnitPrice(eleShipLine);
      LOGGER.verbose("bdUnitPrice : " + bdUnitPrice.toString());
      eleShipLine.setAttribute(GCConstants.UNIT_PRICE, bdUnitPrice.toString());
      eleShipLine.setAttribute(GCConstants.LINE_TOTAL, calculateShipmentSubTotal(eleShipLine).toString());

      // Avoiding WarrantyLine from getting displayed on the UI & the lines with quantity 0
      if (YFCUtils.equals(GCConstants.FLAG_N, eleShipExtnOdrLine.getAttribute(GCConstants.IS_WARRANTY))
          && (!YFCUtils.equals(GCConstants.ZERO, quantity))) {
        final String parentShipLineKey = eleShipLine.getAttribute(GCXmlLiterals.PARENT_SHP_LINE_KEY);
        final String productClass = eleShipLine.getAttribute(GCConstants.PRODUCT_CLASS);
        LOGGER.verbose("productClass: " + productClass);
        LOGGER.verbose("parentShipLineKey : " + parentShipLineKey);
        if (YFCCommon.isVoid(parentShipLineKey)) {
          LOGGER.verbose("its regular or parent");
          eleShipmentLinesOutdoc.importNode(eleShipLine);
        } else {
          eleShipLine.setAttribute(GCConstants.LINE_TOTAL, GCConstants.ZERO_AMOUNT);
          LOGGER.verbose("its component");
          if (componentsMap.containsKey(parentShipLineKey)) {
            LOGGER.verbose("already in map");
            componentsMap.get(parentShipLineKey).importNode(eleShipLine);
          } else {
            final YFCElement eleComponentsOutdoc = finalDoc.createElement(GCXmlLiterals.CUSTOM_COMPONENTS);
            eleComponentsOutdoc.importNode(eleShipLine);
            LOGGER.verbose("new in map");
            componentsMap.put(parentShipLineKey, eleComponentsOutdoc);
          }
        }

        totalAmt = totalAmt.add(calculateShipmentSubTotal(eleShipLine));
      }
    }
    // Stamping shipment sub total in custom doc
    eleShipmentOutdoc.setAttribute(GCConstants.SUB_TOTAL, totalAmt.toString());

    YFCIterable<YFCElement> nlShipmentLineOutdoc = eleShipmentLinesOutdoc.getChildren();
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Pre finalDoc : " + finalDoc.toString());
    }
    // Now, iterating through the custom doc & appending the Component Elements by fetching it from
    // map to its Bundle parent & warranty Lines.
    for (YFCElement eleShipLineOutdoc : nlShipmentLineOutdoc) {
      final String kitCode = eleShipLineOutdoc.getAttribute(GCConstants.KIT_CODE);
      if (YFCUtils.equals(GCConstants.BUNDLE, kitCode)) {
        eleShipLineOutdoc.appendChild(componentsMap.get(eleShipLineOutdoc.getAttribute(GCXmlLiterals.SHP_LINE_KEY)));
      }
      final String orderLineKey = eleShipLineOutdoc.getAttribute(GCConstants.ORDER_LINE_KEY);
      if (warrantylistMap.containsKey(orderLineKey)) {
        eleShipLineOutdoc.appendChild(warrantylistMap.get(orderLineKey));
        if (YFCUtils.equals(GCConstants.BUNDLE, kitCode)) {
          final YFCElement eleCustComp = eleShipLineOutdoc.getChildElement(GCConstants.CUSTOM_COMPONENTS);
          final YFCElement eleCompLine = (YFCElement) eleCustComp.getChildNodes().item(0);
          eleCompLine.appendChild(warrantylistMap.get(orderLineKey));
        }

      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("finalDoc : " + finalDoc.toString());
    }
    LOGGER.verbose("PickDetailAPI.preparePickDetailDocument--End");
    LOGGER.endTimer("PickDetailAPI.preparePickDetailDocument");
    return finalDoc.getDocument();

  }

  private String getPaymentMethod(YFCElement eleShipLineOrder) {

    final YFCElement elePaymentMthds = eleShipLineOrder.getChildElement(GCConstants.PAYMENT_METHODS);
    final YFCNodeList<YFCElement> nlPaymentMethod = elePaymentMthds.getElementsByTagName(GCConstants.PAYMENT_METHOD);
    final int lengthPymntMthd = nlPaymentMethod.getLength();
    String paymentMethod = GCConstants.BLANK;
    for(int i = 0; i < lengthPymntMthd; i++){
      final YFCElement elePymntMthd = nlPaymentMethod.item(i);
      String paymentMode = elePymntMthd.getAttribute(GCConstants.CREDIT_CARD_TYPE);
      if (YFCCommon.isVoid(paymentMode)) {
        paymentMode = elePymntMthd.getAttribute(GCConstants.PAYMENT_TYPE);
      }
      if (paymentMode.contains("_")) {
        paymentMode = paymentMode.replace("_", " ");
      }
      if(i == 0){
        paymentMethod = paymentMode;
      } else{
        paymentMethod = paymentMethod + "  " + paymentMode;
      }
    }
    return paymentMethod;
  }

  /**
   * Calling getCommonCodeList to fetch LevelOfService value
   *
   * @param env
   * @param attribute
   * @return
   */
  public String callCommonCodeList(YFSEnvironment env, String levelOfService) {
    // preparing input for API call
    final YFCDocument commonCodeinXMLYfc = YFCDocument.createDocument(GCConstants.COMMON_CODE);
    final YFCElement commonCodeEle = commonCodeinXMLYfc.getDocumentElement();
    commonCodeEle.setAttribute(GCConstants.CODE_TYPE, GCConstants.GC_EXSTORE_LOS);
    commonCodeEle.setAttribute(GCConstants.CODE_VALUE, levelOfService);

    LOGGER.verbose("commonCodeinXML : " + commonCodeinXMLYfc.getString());
    final YFCDocument commonCodeTemplate = YFCDocument.getDocumentFor(GCConstants.COMMON_CODE_TEMPLATE);

    // calling getCommonCodeList API
    final YFCDocument outXMLYfc =
        GCCommonUtil.invokeAPI(env, GCConstants.API_GET_COMMON_CODE_LIST, commonCodeinXMLYfc, commonCodeTemplate);


    YFCElement eleOutXml = outXMLYfc.getDocumentElement();
    LOGGER.verbose("outXML : " + outXMLYfc.getString());
    final YFCElement eleCommonCode = eleOutXml.getElementsByTagName(GCConstants.COMMON_CODE).item(0);
    String levelOfServiceDesc = GCConstants.BLANK;
    if (!YFCCommon.isVoid(eleCommonCode)) {
      levelOfServiceDesc = eleCommonCode.getAttribute(GCConstants.CODE_SHORT_DESCRIPTION);
    }

    return levelOfServiceDesc;
  }

  private BigDecimal calculateUnitPrice(YFCElement eleShipLine) {

    final YFCElement eleOdrLine = eleShipLine.getChildElement(GCConstants.ORDER_LINE);
    final YFCElement eleLinePriceInfo = eleOdrLine.getChildElement(GCConstants.LINE_PRICE_INFO);
    BigDecimal totalPromotion = new BigDecimal(GCConstants.ZERO_AMOUNT);
    BigDecimal unitPromotion = new BigDecimal(GCConstants.ZERO_AMOUNT);
    final YFCElement eleLineCharges = eleOdrLine.getChildElement(GCConstants.LINE_CHARGES);
    if (!YFCCommon.isVoid(eleLineCharges)) {
      final YFCNodeList<YFCElement> nlLineCharge = eleLineCharges.getElementsByTagName(GCConstants.LINE_CHARGE);
      final int lengthLineChg = nlLineCharge.getLength();
      for (int i = 0; i < lengthLineChg; i++) {
        final YFCElement eleLineChg = nlLineCharge.item(i);
        if (YFCUtils.equals(GCConstants.PROMOTION, eleLineChg.getAttribute(GCConstants.CHARGE_CATEGORY))) {
          BigDecimal chgAmt = new BigDecimal(eleLineChg.getAttribute(GCConstants.CHARGE_PER_LINE));
          totalPromotion = totalPromotion.add(chgAmt);
        }
      }
    }
    BigDecimal quantity = new BigDecimal(eleShipLine.getAttribute(GCConstants.QUANTITY));
    if (quantity.intValue() != 0) {
      unitPromotion = totalPromotion.divide(quantity, 1);
    }
    BigDecimal unitPrice = new BigDecimal(eleLinePriceInfo.getAttribute(GCConstants.UNIT_PRICE));
    return unitPrice.subtract(unitPromotion);
  }

  /**
   * This method is used to stamp attributes in the custom document.
   *
   * @param eleShipmentOutdoc
   * @param eleShipment
   * @param levelOfServiceDesc
   */
  private void stampAttributes(YFCElement eleShipmentOutdoc, YFCElement eleShipment, String levelOfServiceDesc) {

    LOGGER.beginTimer("PickDetailAPI.stampAttributes");
    LOGGER.verbose("PickDetailAPI.stampAttributes--Begin");

    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    Date sysDate = new Date();
    String strDate = sdf.format(sysDate);

    final YFCElement eleShipExtn = eleShipment.getChildElement(GCConstants.EXTN);
    eleShipmentOutdoc.setAttribute(GCXmlLiterals.DELIVERY_METHOD,
        eleShipment.getAttribute(GCXmlLiterals.DELIVERY_METHOD));
    eleShipmentOutdoc.setAttribute(GCConstants.SHIPMENT_KEY, eleShipment.getAttribute(GCConstants.SHIPMENT_KEY));
    eleShipmentOutdoc.setAttribute(GCConstants.SHIPMENT_NO, eleShipment.getAttribute(GCConstants.SHIPMENT_NO));
    eleShipmentOutdoc.setAttribute(GCConstants.FULFILLMENT_TYPE, eleShipExtn.getAttribute(GCConstants.EXTN_FULFILLMENT_TYPE));
    eleShipmentOutdoc.setAttribute(GCConstants.SYSTEM_DATE, strDate);

    eleShipmentOutdoc.setAttribute(GCConstants.ENTERPRISE_CODE, eleShipment.getAttribute(GCConstants.ENTERPRISE_CODE));
    eleShipmentOutdoc.setAttribute(GCConstants.DOCUMENT_TYPE, eleShipment.getAttribute(GCConstants.DOCUMENT_TYPE));
    eleShipmentOutdoc.setAttribute(GCConstants.LEVEL_OF_SERVICE, levelOfServiceDesc);


    eleShipmentOutdoc
    .setAttribute(GCConstants.ORDER_HEADER_KEY, eleShipment.getAttribute(GCConstants.ORDER_HEADER_KEY));

    eleShipmentOutdoc.setAttribute(GCConstants.CUST_ID, eleShipment.getAttribute(GCConstants.CUST_ID));
    eleShipmentOutdoc.setAttribute(GCConstants.STATUS, eleShipment.getAttribute(GCConstants.STATUS));
    eleShipmentOutdoc.setAttribute(GCConstants.SHIP_DATE, eleShipment.getAttribute(GCConstants.SHIP_DATE));
    eleShipmentOutdoc.setAttribute(GCXmlLiterals.RQSTD_SHIP_DATE,
        eleShipment.getAttribute(GCXmlLiterals.RQSTD_SHIP_DATE));
    eleShipmentOutdoc.setAttribute(GCXmlLiterals.EXPCTD_DELIVERY_DATE,
        eleShipment.getAttribute(GCXmlLiterals.EXPCTD_DELIVERY_DATE));
    eleShipmentOutdoc.importNode(eleShipment.getChildElement(GCXmlLiterals.TO_ADDRESS));
    eleShipmentOutdoc.importNode(eleShipment.getChildElement(GCXmlLiterals.BILL_TO_ADDRESS));
    eleShipmentOutdoc.importNode(eleShipment.getChildElement(GCConstants.SHIP_NODE));
    eleShipmentOutdoc.importNode(eleShipment.getChildElement(GCConstants.EXTN));


    LOGGER.verbose("PickDetailAPI.stampAttributes--End");
    LOGGER.endTimer("PickDetailAPI.stampAttributes");
  }

  /**
   * This function fetches the lineTotal for the ShipmentLine
   *
   * @param eleShipLine
   * @return lineTotal
   */
  private BigDecimal calculateShipmentSubTotal(YFCElement eleShipLine) {

    LOGGER.beginTimer("PickDetailAPI.calculateShipmentSubTotal");
    LOGGER.verbose("PickDetailAPI.calculateShipmentSubTotal--Begin");
    final String parentShipLineKey = eleShipLine.getAttribute(GCXmlLiterals.PARENT_SHP_LINE_KEY);
    YFCElement eleShpOdrLine = eleShipLine.getChildElement(GCConstants.ORDER_LINE);
    YFCElement eleLineOverAllTotals = eleShpOdrLine.getChildElement(GCXmlLiterals.LINE_OVERALL_TOTALS);
    BigDecimal lineTotal = new BigDecimal(GCConstants.ZERO_AMOUNT);
    if (!YFCCommon.isVoid(eleLineOverAllTotals) && YFCCommon.isVoid(parentShipLineKey)) {
      lineTotal = new BigDecimal(eleLineOverAllTotals.getAttribute(GCConstants.LINE_TOTAL));
    }

    LOGGER.verbose("PickDetailAPI.calculateShipmentSubTotal--End");
    LOGGER.endTimer("PickDetailAPI.calculateShipmentSubTotal");
    return lineTotal;
  }

  /**
   * This method is used to call GCOrderCommissionListService and fetch the User Name from the first
   * GCOrderCommission Element
   *
   * @param env
   * @param orderHeaderKey
   * @return userName
   */
  private String getGCOrderCommissionListService(YFSEnvironment env, String orderHeaderKey) {

    LOGGER.beginTimer("PickDetailAPI.getGCOrderCommissionListService");
    LOGGER.verbose("PickDetailAPI.getGCOrderCommissionListService--Begin");
    YFCDocument odrCommInputDoc = YFCDocument.createDocument(GCXmlLiterals.ORDER_COMMISSION);
    odrCommInputDoc.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);

    String userName = GCConstants.BLANK_STRING;
    YFCDocument getODRCommOutdoc =
        GCCommonUtil.invokeService(env, GCConstants.ORDER_COMMISSION_LIST_SERVICE, odrCommInputDoc);
    final YFCElement eleCommissionList = getODRCommOutdoc.getDocumentElement();
    final YFCElement eleOdrCommission = eleCommissionList.getElementsByTagName(GCXmlLiterals.ORDER_COMMISSION).item(0);
    if (!YFCCommon.isVoid(eleOdrCommission)) {
      userName = eleOdrCommission.getAttribute(GCXmlLiterals.USER_NAME);
    }
    LOGGER.verbose("PickDetailAPI.getGCOrderCommissionListService--End");
    LOGGER.endTimer("PickDetailAPI.getGCOrderCommissionListService");
    return userName;
  }

  /**
   * Prepare a map of all OrderLines with associated warranties.
   *
   * @param rootInEle
   * @param finalDoc
   * @return
   */
  public Map<String, YFCElement> fetchWarrantyItems(YFSEnvironment yfsEnv, YFCElement rootInEle, YFCDocument finalDoc) {

    LOGGER.verbose("Mthod : fetchWarrantyItems : START ");
    Map<String, YFCElement> warrantyItems = new HashMap<String, YFCElement>();

    // Call getShipmentList to fetch WarrantyLines
    String shipmentKey = rootInEle.getAttribute(GCConstants.SHIPMENT_KEY);

    YFCDocument getShpmntDtlIndoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement rootEle = getShpmntDtlIndoc.getDocumentElement();
    rootEle.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);

    YFCDocument getShpmntDtlTemp = YFCDocument.getDocumentFor(GCConstants.GET_SHIP_DETAIL_TEMPLATE);
    YFCDocument getShpmntListOutdoc =
        GCCommonUtil.invokeAPI(yfsEnv, GCConstants.GET_SHIPMENT_DTL, getShpmntDtlIndoc, getShpmntDtlTemp);

    YFCElement shipmentEle = getShpmntListOutdoc.getDocumentElement();
    YFCElement shipmentLinesEle = shipmentEle.getChildElement(GCConstants.SHIPMENT_LINES);

    YFCIterable<YFCElement> shipmntLineLst = shipmentLinesEle.getChildren();
    for (YFCElement shpmntLineEle : shipmntLineLst) {

      YFCElement orderLineEle = shpmntLineEle.getChildElement(GCConstants.ORDER_LINE);
      // Check for Warranty Items only. Exclude Gift Items.
      YFCElement extnInOrderLineEle = orderLineEle.getChildElement(GCConstants.EXTN);
      String extnIsWarratyItem = extnInOrderLineEle.getAttribute(GCConstants.IS_WARRANTY);
      String dependentOnLineKey = orderLineEle.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
      LOGGER.verbose("DependentOnLineKey fetched :" + dependentOnLineKey);
      if (!YFCCommon.isVoid(dependentOnLineKey) && YFCUtils.equals(GCConstants.YES, extnIsWarratyItem)) {
        if (warrantyItems.containsKey(dependentOnLineKey)) {
          LOGGER.verbose("already in map");
          warrantyItems.get(dependentOnLineKey).importNode(shpmntLineEle);
        } else {
          final YFCElement eleWarrantyCompOutdoc = finalDoc.createElement(GCXmlLiterals.WARRANTY_COMPONENTS);
          eleWarrantyCompOutdoc.importNode(shpmntLineEle);
          LOGGER.verbose("new in map");
          warrantyItems.put(dependentOnLineKey, eleWarrantyCompOutdoc);
        }
      }

      LOGGER.verbose("Map formed :" + warrantyItems.toString());
    }
    LOGGER.verbose("Mthod : fetchWarrantyItems : END ");
    return warrantyItems;
  }

}
