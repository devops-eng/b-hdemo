/**
 * Copyright 2014 Guitar Center. All rights reserved. Guitar Center PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 *
 * Change Log
 * #######################################################################################
 * ################################################################# OBJECTIVE: Foundation Class
 * ####
 * ##############################################################################################
 * ###################################################### Version CR Date Modified By Description
 * ###
 * ###############################################################################################
 * ###################################################### 1.0 Initial Version 01/16/2015
 * Rangarajan,Shwetha The class manages complete and partial pick request cancellation and
 * re-sourcing.
 * #####################################################################################
 * ################################################################################################################################################################
 * 1.1		05/16/2017		 Kumar,Shashank		Changes made for (GCSTROE-6877) cancellation of free gift item when the parent is being cancelled
 * 1.2      06/07/2017		 Kumar,Shashank     Changes added (for GCSTORE-7405) to throw an exception if the the parent associated with the 
 * 												free gift item has already been cancelled
 * 1.3      06/07/2017       Kumar,Shashank     Changes added for GCSTORE-7400 to mark the primary component of a set dirty when it is resourced
 * ################################################################################################################################################################
 */
package com.gc.exstore.api.picking;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.api.GCProcessBackordersAPI;
import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCLogUtils;
import com.gc.common.utils.GCXMLUtil;
import com.gc.exstore.utils.GCShipmentUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCDate;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * GCSTORE-190 - Cancel a Pick Request / Pick Line GCSTORE-192 - Re-Source Pick Request / Pick Line
 * <p>
 * The class contains logic to cancel or re-source a complete pick request or selected pick lines.
 *
 * @author <a href="mailto:shwetha.rangarajan@expicient.com">Shwetha</a>
 */
public class GCPickDetailActionsManager implements YIFCustomApi {

	// initialize logger
	private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCPickDetailActionsManager.class.getName());
	private String shipNode = "";
	private String sParentItemID = null;
	
	private String sGiftItemID = null;

	/**
	 * The method includes logic that allows customers to cancel or alternately source a pick request
	 * or selected pick lines.
	 *
	 * @param yfsEnv
	 * @param inDoc
	 * @return
	 */
	public Document managePickDetailActions(YFSEnvironment yfsEnv, Document inDocument) {

		// Fetch the root element from inDoc and action to check for operation to be performed.
		LOGGER.beginTimer("GCPickDetailActionsManager.managePickDetailActions");
		YFCDocument inDoc = YFCDocument.getDocumentFor(inDocument);
		Document outputDoc = null;
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("GCPickDetailActionsManager.managePickDetailActions : START");
			LOGGER.verbose("Indoc to the method :" + inDoc.toString());
		}
		YFCElement rootInEle = inDoc.getDocumentElement();
		shipNode = rootInEle.getAttribute(GCConstants.SHIP_NODE);
		String action = rootInEle.getAttribute(GCConstants.ACTION);
		boolean isConfirmPick = false;
		if (YFCUtils.equals(action, GCConstants.PARTIAL_PICK) || YFCUtils.equals(action, GCConstants.CONFIRM_PICK)) {
			isConfirmPick = true;
		}

		LOGGER.verbose("isConfirmPick : " + isConfirmPick);
		LOGGER.verbose("Enter Method : managePickDetailActions");
		LOGGER.verbose("Action fetched : " + action);

		// Document to be returned to the front end.
		YFCDocument outDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
		YFCElement rootOutEle = outDoc.getDocumentElement();
		rootOutEle.setAttribute(GCConstants.IS_SUCCESS, GCConstants.YES);
		rootOutEle.setAttribute(GCConstants.ACTION, action);

		boolean isPartialCancel = false;
		boolean isPartialDecline = false;
		String pickUpStore = null;

		// Create changeShipment input document to
		// 1. Cancel a pick request/pick lines
		// 2. BackOrder a pick request/selected pick lines in case of re-sourcing.
		YFCDocument changeShpmntIndoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
		YFCElement shpmntInEle = changeShpmntIndoc.getDocumentElement();
		String shipmentKey = rootInEle.getAttribute(GCConstants.SHIPMENT_KEY);
		shpmntInEle.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
		
		// if complete pick request is cancelled then
		// Call changeShipment API to cancel the pick request
		// Update the ReasonCode/ReasonText against the pick request
		if (YFCUtils.equals(GCConstants.CANCEL_ALL, action)) {
			LOGGER.verbose("Enter action - cancelAll. Call changeShipment API to cancel the shipment and changeOrder API to update ReasonCode");
			rootOutEle.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
			cancelOrDeclineCompletePickRequest(yfsEnv, changeShpmntIndoc, GCConstants.YES);
			updateReasonCodeAndText(yfsEnv, inDoc, isPartialCancel);
		} else if (YFCUtils.equals(GCConstants.RESOURCE_ALL, action)) {
			// If complete order is to be re-sourced then
			// Call changeShipment API to BackOrder the items
			String ordHeaderkey = null;
			
			
			//Added for GCSTORE-7405 : START
			LOGGER.verbose("Enter action - resourceAll. Call popUpIfParentOfGiftHasBeenCancelled"
							+ " to display a pop-up if the parent item associated with the free gift item has already been cancelled.");
			popUpIfParentOfGiftHasBeenCancelled(yfsEnv, inDoc);
			//GCSTORE-7405 : END
			
			LOGGER.verbose("Enter action - resourceAll. Call changeShipment API to cancle the shipment. "
					+ "Call changeOrder API to suppress soourcing at the node and changeOrderStatus API to move the order to BackOrderedDAX");
			rootOutEle.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
			
			try {
				checkInventoryForResourcing(yfsEnv,inDoc);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
			cancelOrDeclineCompletePickRequest(yfsEnv, changeShpmntIndoc, GCConstants.NO);
			// and call changeOrder API to suppress sourcing against the selected node and
			// changeOrderStatus API to move the order status to BackOrderedDAX.
			YFCDocument chngOrdStatusIndoc = prepareChngOrdStatusIndoc(rootInEle, isPartialDecline);
			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("changeOrderStatus API input : " + chngOrdStatusIndoc.toString());
			}
			manageAlternateSourcing(yfsEnv, inDoc, chngOrdStatusIndoc, isPartialDecline);
			YFCElement shipmentLinesEle = rootInEle.getChildElement(GCConstants.SHIPMENT_LINES);
			YFCElement shipmentChildEle = shipmentLinesEle.getChildElement(GCConstants.SHIPMENT_LINE);
			if (!YFCCommon.isVoid(shipmentChildEle)) {
				ordHeaderkey = shipmentChildEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
			}
			YFCDocument getOrdeListIp = YFCDocument.createDocument(GCConstants.ORDER);
			YFCElement eleOrder = getOrdeListIp.getDocumentElement();
			ordHeaderkey = shipmentChildEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
			eleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, ordHeaderkey);
			YFCDocument getOrderListopTemp = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_OUT_TMP_RESRC);
			YFCDocument getOrderListOpDoc =
					GCCommonUtil.invokeAPI(yfsEnv, GCConstants.GET_ORDER_LIST, getOrdeListIp, getOrderListopTemp);
			GCLogUtils.verboseLog(LOGGER, "getOrderListOpDoc : ", getOrderListOpDoc.toString());
			YFCElement eleOutputDocument = getOrderListOpDoc.getDocumentElement();
			YFCElement eleOrd = eleOutputDocument.getChildElement(GCConstants.ORDER);
			YFCElement eleExtn = eleOrd.getChildElement(GCConstants.EXTN);
			if (!YFCCommon.isVoid(eleExtn)) {
				pickUpStore = eleExtn.getAttribute(GCConstants.EXTN_PICK_UP_STORE_ID);
			}
			
			if (!YFCCommon.isVoid(pickUpStore) && YFCUtils.equals(pickUpStore, shipNode)) {
				LOGGER
				.verbose("Going to raise alert. Because Pick up in store has been backordered, moving the ordered to Ship to store");
				GCCommonUtil.invokeService(yfsEnv, "GCRaiseAlertForShipToStoreService", GCXMLUtil.getDocument(eleOrd, true));
				GCProcessBackordersAPI processBackOrder = new GCProcessBackordersAPI();
				processBackOrder.callChangeOrderToUpdateLOS(yfsEnv, eleOrder, GCConstants.STANDARD_GROUND);
			}
		} else if (isConfirmPick) {
			//Added for GCSTORE-7405 : START
			popUpIfParentOfGiftHasBeenCancelled(yfsEnv, inDoc);
			//GCSTORE-7405 : END
			GCConfirmPickAPI ob = new GCConfirmPickAPI();
			outputDoc = ob.manageBackRoomPickCompletion(yfsEnv, inDocument);
		} else {

			YFCElement shipmentLinesEle = rootInEle.getChildElement(GCConstants.SHIPMENT_LINES);
			YFCElement shipmentChildEle = shipmentLinesEle.getChildElement(GCConstants.SHIPMENT_LINE);
			String orderHeaderKey = shipmentChildEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
			LOGGER.verbose("OrderHeaderKey fetched : " + orderHeaderKey);

			rootOutEle.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
			rootOutEle.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
			rootOutEle.setAttribute(GCXmlLiterals.SHIPMENT_LINE_KEY, shipmentChildEle.getAttribute(GCXmlLiterals.SHIPMENT_LINE_KEY));

			shpmntInEle.setAttribute(GCConstants.ACTION, GCConstants.ACTION_MODIFY);
			YFCElement shpmntLinesInEle = shpmntInEle.createChild(GCConstants.SHIPMENT_LINES);

			//if the action is cancelLine then loop through the ShipmentLines and check for Bundle/Warranty items.
			if (YFCUtils.equals(GCConstants.ACTION_CANCEL_LINE, action)) {

				LOGGER.verbose("Enter action - cancelLine. Call changeShipment API to cancel the lines "
						+ "and changeOrder API to update ReasonCode against required lines");
				isPartialCancel = true;
				prepareChangeShpmntIndoc(changeShpmntIndoc, shpmntInEle, shipmentLinesEle, orderHeaderKey, shpmntLinesInEle, true);
			} else if (YFCUtils.equals(GCConstants.ACTION_RESOURCE_LINE, action)) {
				
				LOGGER.verbose("Enter action - resourceLine. Call popUpIfParentOfGiftHasBeenCancelled"
						+ " to display a pop-up if the parent item associated with the free gift item has already been cancelled.");
				//Added for GCSTORE-7405 : START
				popUpIfParentOfGiftHasBeenCancelled(yfsEnv, inDoc);
				//GCSTORE-7405 : END
				
				LOGGER.verbose("Enter action - resourceLine. Call changeShipment API to cancle the shipment line. "
						+ "Call changeOrder API to suppress soourcing at the node and changeOrderStatus API to move the orderline to BackOrderedDAX");
				isPartialDecline = true;
				
				try {
					checkInventoryForResourcing(yfsEnv,inDoc);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				prepareChangeShpmntIndoc(changeShpmntIndoc, shpmntInEle, shipmentLinesEle, orderHeaderKey, shpmntLinesInEle, false);

			}

			if(LOGGER.isVerboseEnabled()){
				LOGGER.verbose("Complete changeShipment input API :" +changeShpmntIndoc.toString());
			}

			GCCommonUtil.invokeAPI(yfsEnv, GCConstants.CHANGE_SHIPMENT, changeShpmntIndoc.getDocument());
		}

		if (isPartialCancel) {
			YFCDocument changeOrderIndoc = prepareChngOrdIndoc(rootInEle);
			updateReasonCodeAndText(yfsEnv, changeOrderIndoc, isPartialCancel);
		}
		if (isPartialDecline) {
			YFCDocument changeOrderIndoc = prepareChngOrdIndoc(rootInEle);
			YFCDocument chngOrdStatusIndoc = prepareChngOrdStatusIndoc(rootInEle, isPartialDecline);
			manageAlternateSourcing(yfsEnv, changeOrderIndoc, chngOrdStatusIndoc, isPartialDecline);
		}

		if (!isConfirmPick) {
			updateGCShipmentActivity(yfsEnv, rootInEle);
		}
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Output to UI : " + outDoc);
			LOGGER.verbose("GCPickDetailActionsManager.managePickDetailActions : END");
		}
		LOGGER.endTimer("GCPickDetailActionsManager.managePickDetailActions");
		if (isConfirmPick) {
			return outputDoc;
		} else {
			return outDoc.getDocument();
		}

	}
	
	//GCSTORE-7405 : START
	/**
	 * Method to display a pop-up if parent line associated with Free Gift Item has been cancelled already
	 * @param yfsEnv
	 * @param inDoc
	 */
	private void popUpIfParentOfGiftHasBeenCancelled(
			YFSEnvironment yfsEnv, YFCDocument inDoc) {
		LOGGER.info("Entering inside GCPickDetailActionsManager.popUpIfParentOfGiftHasBeenCancelled()");
	
		List<Element> nlShipmentLine = GCXMLUtil.getElementListByXpath(inDoc.getDocument(), "Shipment/ShipmentLines/ShipmentLine");
		Element shipmentLineEle  = nlShipmentLine.get(0);

		if(!YFCCommon.isVoid(shipmentLineEle)){
			YFCDocument inDocForGetOrderList = YFCDocument.createDocument(GCConstants.ORDER);
			YFCElement eleOrder = inDocForGetOrderList.getDocumentElement();
			String sOrderHeaderKey = shipmentLineEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
			eleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
			YFCDocument getOrderListTemp = prepareGetOrderListTemplate();
			if(!YFCCommon.isVoid(sOrderHeaderKey)){
				YFCDocument outDocGetOrderList = GCCommonUtil.invokeAPI(yfsEnv, GCConstants.GET_ORDER_LIST, inDocForGetOrderList, getOrderListTemp);
				LOGGER.info("getOrderList output Doc :" + outDocGetOrderList.toString());

				for(Element eleShipmentLine : nlShipmentLine){

					String sOrderLineKey = eleShipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY);
					String sDependentOnLineKeyOnGift = fetchDependentLineKeyOnGift(outDocGetOrderList, sOrderLineKey);
					if(!YFCCommon.isVoid(sDependentOnLineKeyOnGift)){
						
						//Fetching the status of the parent OrderLine
						String sParentLineStatus = GCXMLUtil.getAttributeFromXPath(outDocGetOrderList.getDocument(),
								"//Order/OrderLines/OrderLine[@OrderLineKey='" + sDependentOnLineKeyOnGift + "']/@MinLineStatus");
						if(!YFCCommon.isVoid(sDependentOnLineKeyOnGift) && YFCUtils.equals(sParentLineStatus, GCConstants.CANCELLED_ORDER_STATUS_CODE)){
							//throw exception
							YFCException yfce = new YFCException();
							yfce.setAttribute(GCConstants.ERROR_CODE, "GIFT_LINE_CANCEL");
							yfce.setAttribute(GCConstants.ERROR_DESCRIPTION, "Parent Item: " + sParentItemID + " associated with the Gift item: " + sGiftItemID + " has already been cancelled. Please cancel the Gift item: "+ sGiftItemID + " manually.");
							throw yfce;
						}
					}
				}  
			}



		}
		LOGGER.info("Exiting from GCPickDetailActionsManager.popUpIfParentOfGiftHasBeenCancelled()");
	}
	
	/**
	 * Method to fetch the DependentLineKey from Gift line
	 * @param outDocGetOrderList
	 * @param sOrderLineKey
	 * @return
	 */
	private String fetchDependentLineKeyOnGift(YFCDocument outDocGetOrderList,
			String sOrderLineKey) {
		LOGGER.info("Entering inside GCPickDetailActionsManager.fetchDependentLineKeyOnGift()");
		String sDependentOnLineKeyOfGift = null;
		
		Element eleGiftOrderLine = GCXMLUtil.getElementByXPath(outDocGetOrderList.getDocument(), 
				"//Order/OrderLines/OrderLine[@OrderLineKey='" + sOrderLineKey + "' and Extn[@ExtnIsFreeGiftItem='Y']]");
		
		if(!YFCCommon.isVoid(eleGiftOrderLine)){
			sDependentOnLineKeyOfGift = eleGiftOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
			Element eleGiftItemDetails = (Element)eleGiftOrderLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
			sGiftItemID = eleGiftItemDetails.getAttribute(GCConstants.ITEM_ID);	
			
			if(!YFCCommon.isVoid(sDependentOnLineKeyOfGift)){
				Element eleParentLine = GCXMLUtil.getElementByXPath(outDocGetOrderList.getDocument(), 
						"//Order/OrderLines/OrderLine[@OrderLineKey='" + sDependentOnLineKeyOfGift + "']");
				
				if(!YFCCommon.isVoid(eleParentLine)){
					Element eleParentItemDetails = (Element)eleParentLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
					sParentItemID = eleParentItemDetails.getAttribute(GCConstants.ITEM_ID);
					
				}
			}
		}
		LOGGER.info("Exiting from GCPickDetailActionsManager.fetchDependentLineKeyOnGift()");
		LOGGER.info("DependentOnLineKey of Gift item: " + sDependentOnLineKeyOfGift);
		return sDependentOnLineKeyOfGift;
	}
	
	/**
	 * Method to form output template for getOrderList
	 * @return
	 */
	private YFCDocument prepareGetOrderListTemplate() {

		YFCDocument getOrderListTemplateDoc = YFCDocument.createDocument(GCConstants.ORDER_LIST);
		YFCElement eleOrderList = getOrderListTemplateDoc.getDocumentElement();
		YFCElement eleOrdr = eleOrderList.createChild(GCConstants.ORDER);
		eleOrdr.setAttribute(GCConstants.DOCUMENT_TYPE, "");
		eleOrdr.setAttribute(GCConstants.ORDER_HEADER_KEY, "");
		eleOrdr.setAttribute(GCConstants.ENTERPRISE_CODE, "");
		eleOrdr.setAttribute(GCConstants.ORDER_NO, "");
		YFCElement eleOrdrLines = eleOrdr.createChild(GCConstants.ORDER_LINES);
		eleOrdrLines.setAttribute(GCConstants.TOTAL_NO_OF_RECORDS, "");
		YFCElement eleOrdrLine = eleOrdrLines.createChild(GCConstants.ORDER_LINE);
		eleOrdrLine.setAttribute(GCConstants.ORDER_LINE_KEY, "");
		eleOrdrLine.setAttribute(GCConstants.MAX_LINE_STATUS, "");
		eleOrdrLine.setAttribute(GCConstants.MIN_LINE_STATUS, "");
		eleOrdrLine.setAttribute(GCConstants.PRIME_LINE_NO, "");
		eleOrdrLine.setAttribute(GCConstants.SUB_LINE_NO, "");
		eleOrdrLine.setAttribute(GCConstants.SHIP_NODE, "");
		eleOrdrLine.setAttribute(GCConstants.DEPENDENT_ON_LINE_KEY, "");
		YFCElement eleItemDetails = eleOrdrLine.createChild(GCConstants.ITEM_DETAILS);
		eleItemDetails.setAttribute(GCConstants.ITEM_ID, "");
		YFCElement eleExtn = eleOrdrLine.createChild(GCConstants.EXTN);
		eleExtn.setAttribute(GCConstants.EXTN_IS_FREE_GIFT_ITEM, "");
		return getOrderListTemplateDoc;
	}
	//GCSTORE-7405 : END
	
	//GCSTORE-5732 - Start
	private void checkInventoryForResourcing(YFSEnvironment yfsEnv, YFCDocument inDoc) throws ParseException {
		//Creating getOrderList input passing the order header key
		LOGGER.beginTimer("GCPickDetailActionsManager.checkInventoryForResourcing");
		YFCDocument getOrdeListIp = YFCDocument.createDocument(GCConstants.ORDER);

		YFCElement eleOrder = getOrdeListIp.getDocumentElement();  
		String ordHeaderkey = null;

		YFCElement rootInEle=inDoc.getDocumentElement();
		YFCElement shipmentLinesEle = rootInEle.getChildElement(GCConstants.SHIPMENT_LINES);
		YFCElement shipmentChildEle = shipmentLinesEle.getChildElement(GCConstants.SHIPMENT_LINE);


		if (!YFCCommon.isVoid(shipmentChildEle)) {
			ordHeaderkey = shipmentChildEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
		}
		eleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, ordHeaderkey);

		//Invoking getOrderList API
		if(LOGGER.isVerboseEnabled()){
			LOGGER.verbose("Invoking getOrderList API :" +getOrdeListIp.toString());
		}
		
        Document getOrderListTempDoc = GCCommonUtil.getDocumentFromPath(GCConstants.GC_GET_ORDER_LIST_TEMPLATE);  
	    YFCDocument yfcgetOrderListTempDoc = YFCDocument.getDocumentFor(getOrderListTempDoc);

		YFCDocument orderDoc =
				GCCommonUtil.invokeAPI(yfsEnv, GCConstants.GET_ORDER_LIST, getOrdeListIp,yfcgetOrderListTempDoc);

		YFCElement orderRootEle=orderDoc.getDocumentElement();
		YFCElement orderEle=orderRootEle.getChildElement(GCConstants.ORDER);
		//Create input to invoke GCFindInventoryWrapperService 
		YFCDocument findInventoryInDoc = YFCDocument.createDocument(GCConstants.PROMISE);
		YFCElement rootInvenEle = findInventoryInDoc.getDocumentElement();
		rootInvenEle.setAttribute(GCConstants.ORGANIZATION_CODE, orderEle.getAttribute(GCConstants.ENTERPRISE_CODE));
		if(orderEle.hasAttribute(GCConstants.SOURCING_CLASSIFICATION))
		{
			rootInvenEle.setAttribute(GCConstants.SOURCING_CLASSIFICATION, 
					orderEle.getAttribute(GCConstants.SOURCING_CLASSIFICATION));
		}

		rootInvenEle.setAttribute(GCConstants.USE_UNPLANNED_INVENTORY, "N");
		rootInvenEle.setAttribute("MaximumRecords","5");
		rootInvenEle.setAttribute("AggregateSupplyOfNonRequestedTag","N");
		//Adding the present ship node to the exclude node list
		      YFCElement excludedShipNodesEle=findInventoryInDoc.createElement(GCConstants.EXCLUDED_SHIP_NODES);
		      rootInvenEle.appendChild(excludedShipNodesEle);
		      YFCElement excludedShipNodeEle=findInventoryInDoc.createElement(GCConstants.EXCLUDED_SHIP_NODE);
		      excludedShipNodeEle.setAttribute(GCConstants.NODE,rootInEle.getAttribute(GCConstants.SHIP_NODE));
		      
		      excludedShipNodeEle.setAttribute(GCConstants.SUPRESS_SOURCING,GCConstants.YES);
		      excludedShipNodesEle.appendChild(excludedShipNodeEle);

		YFCElement PromiseLinesEle=findInventoryInDoc.createElement(GCConstants.PROMISE_LINES);
		rootInvenEle.appendChild(PromiseLinesEle);

		//Create a map containing Item ID from getOrderList Output and matching ShipmentLine input
		Map<String, YFCElement> itemShipmentLineMap = new HashMap<String,YFCElement>();

		//Iterating through input shipment lines
		YFCIterable<YFCElement> nlShipmentChildEle = shipmentLinesEle.getChildren(GCConstants.SHIPMENT_LINE);
		
		//Fix for GCSTORE-7162, Create a list of OrderLine element
		List<YFCElement> orderLineList = new ArrayList<YFCElement>();
		
		int count =0;
		for (YFCElement eleShipLine : nlShipmentChildEle) {

			//Iterating through getOrderList output order lines
			YFCNodeList<YFCElement> nlOdrLine = orderDoc.getElementsByTagName(GCConstants.ORDER_LINE);
			
			String orderLineKey = eleShipLine.getAttribute(GCConstants.ORDER_LINE_KEY);
			
			
			for (YFCElement eleOdrLine : nlOdrLine) {
				if (YFCCommon.equalsIgnoreCase(orderLineKey, eleOdrLine.getAttribute(GCConstants.ORDER_LINE_KEY)))
				{
					//Fix of GCSTORE-7162, Add the orderline elements to the list
					orderLineList.add(eleOdrLine);
					
					count++;
					YFCElement PromiseLineEle=findInventoryInDoc.createElement(GCConstants.PROMISE_LINE);
					PromiseLinesEle.appendChild(PromiseLineEle);
					if(count==1)
					{
						rootInvenEle.setAttribute(GCConstants.FULFILLMENT_TYPE, eleOdrLine.getAttribute(GCConstants.FULFILLMENT_TYPE));
					}
					YFCElement itemEle =eleOdrLine.getChildElement(GCConstants.ITEM);
					PromiseLineEle.setAttribute(GCConstants.ITEM_ID, itemEle.getAttribute(GCConstants.ITEM_ID));

					itemShipmentLineMap.put(itemEle.getAttribute(GCConstants.ITEM_ID), eleShipLine);
					if(eleShipLine.hasAttribute(GCConstants.QUANTITY)){
					 PromiseLineEle.setAttribute(GCConstants.REQUIRED_QTY, eleShipLine.getAttribute(GCConstants.QUANTITY));
					}
					else
					{
						PromiseLineEle.setAttribute(GCConstants.REQUIRED_QTY, eleShipLine.getAttribute(GCConstants.QUANTITY_TO_PICK));
					}
					 PromiseLineEle.setAttribute(GCConstants.PRODUCT_CLASS, eleShipLine.getAttribute(GCConstants.PRODUCT_CLASS));
					PromiseLineEle.setAttribute(GCConstants.UOM, itemEle.getAttribute(GCConstants.UOM));
					PromiseLineEle.setAttribute(GCConstants.LINE_ID, count);
					PromiseLineEle.setAttribute(GCConstants.KIT_CODE, eleShipLine.getAttribute(GCConstants.KIT_CODE));
					YFCElement PromiseLineExtnEle=findInventoryInDoc.createElement(GCConstants.EXTN);
					PromiseLineEle.appendChild(PromiseLineExtnEle);
					YFCElement extnEle =eleOdrLine.getChildElement(GCConstants.EXTN);
					if(extnEle.hasAttribute(GCConstants.EXTN_IS_STORE_CLEARANCE))
					{
						PromiseLineExtnEle.setAttribute(GCConstants.EXTN_IS_STORE_CLEARANCE, 
								extnEle.getAttribute(GCConstants.EXTN_IS_STORE_CLEARANCE));
					}

					YFCElement invEle=eleOdrLine.getChildElement("OrderLineInvAttRequest");
					if(!YFCCommon.isVoid(invEle))
					{
						YFCElement promiseLineTagEle=findInventoryInDoc.createElement(GCConstants.TAG);
						PromiseLineEle.appendChild(promiseLineTagEle);
						promiseLineTagEle.setAttribute(GCConstants.BATCH_NO, invEle.getAttribute(GCConstants.BATCH_NO));
					}
					
					break;
				}
			}
			//Adding component lines to the order line and quantity map
			YFCElement eleCustomComp = (YFCElement)eleShipLine.getElementsByTagName(GCConstants.CUSTOM_COMPONENTS).item(0);
			if(!YFCCommon.isVoid(eleCustomComp))
			{
			YFCNodeList<YFCElement> nlCustomLine = eleCustomComp.getElementsByTagName(GCConstants.SHIPMENT_LINE);

			for (YFCElement eleCustomLine : nlCustomLine) {
				orderLineKey = eleCustomLine.getAttribute(GCConstants.ORDER_LINE_KEY);
				for (YFCElement eleOdrLine : nlOdrLine) {
					if (YFCCommon.equalsIgnoreCase(orderLineKey, eleOdrLine.getAttribute(GCConstants.ORDER_LINE_KEY)))
					{
						YFCElement itemEle =eleOdrLine.getChildElement(GCConstants.ITEM);
						itemShipmentLineMap.put(itemEle.getAttribute(GCConstants.ITEM_ID), eleCustomLine);
					}
				}
			 }
			}
		}
		//Invoke Find inventory wrapper service
		if(LOGGER.isVerboseEnabled()){
			LOGGER.verbose("Invoking Find inventory wrapper service :" +findInventoryInDoc.toString());
		}
		YFCDocument findInvOpDoc= GCCommonUtil.invokeService(yfsEnv, GCConstants.GC_FIND_INVENTORY_WRAPPER_SERVICE , findInventoryInDoc);
		if(LOGGER.isVerboseEnabled()){
			LOGGER.verbose("Invoking Find inventory wrapper service outDoc :" +findInvOpDoc.toString());
		}
		//Iterating through the promise lines to check for the supply availability
		YFCNodeList<YFCElement> nlPromiseLine = findInvOpDoc.getElementsByTagName(GCConstants.PROMISE_LINE);
		Date todayDate = new Date();
		boolean bAvailable= true;
		if(nlPromiseLine.getLength()==0)
		{
			bAvailable=false;
		}
		for (YFCElement elePromiseLine : nlPromiseLine) {
			String itemID = elePromiseLine.getAttribute(GCConstants.ITEM_ID);
			//Check is bundle parent,then continue
			if(YFCCommon.equalsIgnoreCase(GCConstants.YES, elePromiseLine.getAttribute(GCConstants.IS_BUNDLE_PARENT)))
			{
				continue;
			}
			else if(itemShipmentLineMap.containsKey(itemID)){
				YFCElement shipmentLine = itemShipmentLineMap.get(itemID);
				int reqQty=0;
				if(shipmentLine.hasAttribute(GCConstants.QUANTITY)){
					reqQty=Integer.parseInt(shipmentLine.getAttribute(GCConstants.QUANTITY));
					}
					else
					{
						reqQty=Integer.parseInt(shipmentLine.getAttribute(GCConstants.QUANTITY_TO_PICK));
					}
				
				
				int availableQty=0;
				DateFormat formatter ; 
				Date dateFirstShip;
				YFCNodeList<YFCElement> nlSupplyLine = elePromiseLine.getElementsByTagName(GCConstants.SUPPLY);

				for (YFCElement eleSupplyLine : nlSupplyLine) {

					String strFirstShip = eleSupplyLine.getAttribute("FirstShipDate");

					formatter = new SimpleDateFormat("yyyy-MM-dd");
					dateFirstShip = formatter.parse(strFirstShip);


					if(dateFirstShip.before(todayDate)){
						availableQty=availableQty+Integer.parseInt(eleSupplyLine.getAttribute(GCConstants.AVAILABLE_QUANTITY));

					}    
				}


				if(reqQty>availableQty)
				{
					bAvailable=false;
					break;
				}

			}

		}
		
		//Fix of GCSTORE-7162, call manageInventoryNodecontrol to mark node as dirty
		markDirtyNode(yfsEnv, orderLineList);


		if(!bAvailable)
		{
			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("Insufficient inventory");

			}
			YFCException yfce = new YFCException(GCErrorConstants.INSUFFICIENT_INVENTORY);
			throw yfce;
		}
		
		LOGGER.verbose("GCPickDetailActionsManager.checkInventoryForResourcing : END");
		LOGGER.endTimer("GCPickDetailActionsManager.checkInventoryForResourcing");
	}	
	
	
	/**
	 * This method will mark the ship node as dirty by invoking manageInventoryNodeControl API
	 * Fix of GCSTORE- 7162
	 * 
	 * @param yfsEnv
	 * @param orderLineList
	 */
	
	private void markDirtyNode(YFSEnvironment yfsEnv, List<YFCElement> orderLineList)
	{
		YFCElement orderLineEle = null;
		YFCElement itemEle = null;
		
		
		String itemID = "";
		String productClass="";
		String unitOfMeasure = "";
		
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("OrderLineList :" + orderLineList.toString());
		}
		
		for(int i=0; i<orderLineList.size(); i++){
			orderLineEle = orderLineList.get(i);
			
			String sKitCode = orderLineEle.getAttribute(GCConstants.KIT_CODE);
			//Added for GCSTORE-7400 : Start
			YFCElement eleItemDetails = orderLineEle.getChildElement(GCConstants.ITEM_DETAILS);
			YFCElement eleItemDetailsExtn = eleItemDetails.getChildElement(GCConstants.EXTN);
			String sPrimaryCompItemID = eleItemDetailsExtn.getAttribute(GCConstants.EXTN_PRIMARY_COMPONENT_ID);
			//GCSTORE-7400 : End
			
			YFCNodeList<YFCElement> nlBundleComp = orderLineEle.getElementsByTagName(GCConstants.BUNDLE_COMPONENT);			
			
			if(YFCUtils.equals(sKitCode, GCConstants.BUNDLE)){
				
				for (YFCElement eleBundleComp : nlBundleComp){
					
					itemEle =eleBundleComp.getChildElement(GCConstants.ITEM);
					String sBundleCompItemID = itemEle.getAttribute(GCConstants.ITEM_ID);			
					//Condition added for GCSTORE-7400 : to mark the primary component of the bundle alone as dirty
					if(YFCUtils.equals(sBundleCompItemID, sPrimaryCompItemID)){
						itemID = itemEle.getAttribute(GCConstants.ITEM_ID);
						productClass = itemEle.getAttribute(GCConstants.PRODUCT_CLASS);
						unitOfMeasure = itemEle.getAttribute(GCConstants.UNIT_OF_MEASURE);
						
						invokeManageInventoryNodeControl(yfsEnv, itemID,
								productClass, unitOfMeasure);
						break;
					}			
				}
				
			} else {
				itemEle =orderLineEle.getChildElement(GCConstants.ITEM);
				itemID = itemEle.getAttribute(GCConstants.ITEM_ID);
				productClass = itemEle.getAttribute(GCConstants.PRODUCT_CLASS);
				unitOfMeasure = itemEle.getAttribute(GCConstants.UNIT_OF_MEASURE);
				
				invokeManageInventoryNodeControl(yfsEnv, itemID, productClass,
						unitOfMeasure);	
			}
		}
				
	}
	
	/**
	 * Method to invoke manageInventoryNodeControl API
	 * 
	 * @param yfsEnv
	 * @param itemID
	 * @param productClass
	 * @param unitOfMeasure
	 */
	private void invokeManageInventoryNodeControl(YFSEnvironment yfsEnv,
			String itemID, String productClass, String unitOfMeasure) {
		YFCDocument nodeCtrlInDoc = YFCDocument.createDocument(GCConstants.INVENTORY_NODE_CONTROL);
		YFCElement rootNodeCtrlEle = nodeCtrlInDoc.getDocumentElement();	
		//InvPictureIncorrectTillDate attribute set to some future date
		
		String inCorrectTill = getIncorrectTillDate(yfsEnv);
		
		rootNodeCtrlEle.setAttribute(GCConstants.INV_PICTURE_INCORRECT_TILL_DATE, inCorrectTill);
		
		//organizationCode value for GC Inventory hard-coded
		rootNodeCtrlEle.setAttribute(GCConstants.ORGANIZATION_CODE, (GCConstants.INVENTORY_ORG_FOR_GC));
		rootNodeCtrlEle.setAttribute(GCConstants.ITEM_ID, itemID);
		rootNodeCtrlEle.setAttribute(GCConstants.NODE, shipNode);
		rootNodeCtrlEle.setAttribute(GCConstants.PRODUCT_CLASS, productClass);
		rootNodeCtrlEle.setAttribute(GCConstants.UNIT_OF_MEASURE, unitOfMeasure);
		
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("ManageInventoryNodeControl Input XML" + nodeCtrlInDoc.toString());
		}
		
		GCCommonUtil.invokeAPI(yfsEnv, GCConstants.API_MANAGE_INVENTORY_NODE_CONTROL, nodeCtrlInDoc, null);
	}
	
	private String getIncorrectTillDate(YFSEnvironment yfsEnv) {
		String timeStamp = "";
		String codeType = GCConstants.ADDITIONAL_DAYS_FOR_NODE_CTRL;
	  	Integer additionalDays = callCommonCodeToGetAdditionalDays(yfsEnv, codeType);
	  	
	  	if(!YFCCommon.isVoid(additionalDays)){
	  	Calendar c = Calendar.getInstance();
		c.setTime(new Date());
	  	c.add(Calendar.DATE, additionalDays);
	  	Date date = c.getTime();
	    timeStamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").format(date); 
	  	}
	  	return timeStamp;
	}
	
	 private Integer callCommonCodeToGetAdditionalDays(YFSEnvironment env, String codeType) {
		  	String sItemType = "";
		  	Integer additionalDays = 0;
		      YFCDocument AdditionalDaysDoc = GCCommonUtil
		              .getCommonCodeListByTypeAndValue(
		              		env,codeType,GCConstants.GC, sItemType);
		      if(!YFCCommon.isVoid(AdditionalDaysDoc)){
		      Document AdditionalDaysOutDoc = AdditionalDaysDoc.getDocument();
		      Element eleCommonCode = GCXMLUtil.getElementByXPath(AdditionalDaysOutDoc,"/CommonCodeList/CommonCode");
		      if(!YFCCommon.isVoid(eleCommonCode)){
		    	  additionalDays = Integer.parseInt(eleCommonCode.getAttribute(GCConstants.CODE_VALUE));
		      }
		      }
		     return additionalDays; 
		  }
	
	//GCSTORE-5732 - End
	private void prepareChangeShpmntIndoc(YFCDocument changeShpmntIndoc, YFCElement shpmntInEle,
			YFCElement shipmentLinesEle, String orderHeaderKey, YFCElement shpmntLinesInEle, boolean isCancelLine) {

		// if Action is Cancel on the ShipmentLine
		// call changeShipment API to Cancel the ShipmentLine else BackOrder the ShipmentLine
		if (isCancelLine) {
			shpmntInEle.setAttribute(GCConstants.CANCEL_RMVD_QTY, GCConstants.YES);
		} else {
			shpmntInEle.setAttribute(GCConstants.BACKORDER_RMVD_QTY, GCConstants.YES);
		}
		YFCIterable<YFCElement> shipmntLineLst = shipmentLinesEle.getChildren();
		for (YFCElement shpmntLineEle : shipmntLineLst) {

			String orderLineKey = shpmntLineEle.getAttribute(GCConstants.ORDER_LINE_KEY);

			YFCElement shpmntLineInEle = shpmntLinesInEle.createChild(GCConstants.SHIPMENT_LINE);
			shpmntLineInEle.setAttribute(GCConstants.ACTION, GCConstants.ACTION_CANCEL_SHIPMENT);
			shpmntLineInEle.setAttribute(GCXmlLiterals.SHIPMENT_LINE_KEY,
					shpmntLineEle.getAttribute(GCXmlLiterals.SHIPMENT_LINE_KEY));
			shpmntLineInEle.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
			shpmntLineInEle.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey);

			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("changeShipment API input after appending line details : " + changeShpmntIndoc.toString());
			}
			// If the item is a bundle parent call changeShipment API to cancel bundle components.
			if (YFCUtils.equals(GCConstants.BUNDLE, shpmntLineEle.getAttribute(GCConstants.KIT_CODE))) {
				YFCElement customComponents = shpmntLineEle.getChildElement(GCConstants.CUSTOM_COMPONENTS);
				if (!YFCCommon.isVoid(customComponents) && customComponents.hasChildNodes()) {
					YFCIterable<YFCElement> bundleCompList = customComponents.getChildren();
					for (YFCElement shipmentLine : bundleCompList) {
						YFCElement bundleCompShipLine = shpmntLinesInEle.createChild(GCConstants.SHIPMENT_LINE);
						bundleCompShipLine.setAttribute(GCConstants.ACTION, GCConstants.ACTION_CANCEL_SHIPMENT);
						bundleCompShipLine.setAttribute(GCXmlLiterals.SHIPMENT_LINE_KEY,
								shipmentLine.getAttribute(GCXmlLiterals.SHIPMENT_LINE_KEY));
						bundleCompShipLine.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
						bundleCompShipLine.setAttribute(GCConstants.ORDER_LINE_KEY,
								shipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY));
					}
				}
				if(LOGGER.isVerboseEnabled()){
					LOGGER.verbose("changeShipment API input after appending bundle component lines :" +changeShpmntIndoc.toString());
				}
			}

			YFCElement warrantyComponents = shpmntLineEle.getChildElement(GCConstants.WARRANTY_COMPONENTS);
			if (!YFCCommon.isVoid(warrantyComponents) && warrantyComponents.hasChildNodes()) {
				YFCIterable<YFCElement> warrantyCompList = warrantyComponents.getChildren();
				for (YFCElement shipmentLine : warrantyCompList) {
					YFCElement warrantyShipLine = shpmntLinesInEle.createChild(GCConstants.SHIPMENT_LINE);
					warrantyShipLine.setAttribute(GCConstants.ACTION, GCConstants.ACTION_CANCEL_SHIPMENT);
					warrantyShipLine.setAttribute(GCXmlLiterals.SHIPMENT_LINE_KEY,
							shipmentLine.getAttribute(GCXmlLiterals.SHIPMENT_LINE_KEY));
					warrantyShipLine.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
					warrantyShipLine.setAttribute(GCConstants.ORDER_LINE_KEY,
							shipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY));
				}
				if(LOGGER.isVerboseEnabled()){
					LOGGER.verbose("changeShipment API input after appending warranty lines :" +changeShpmntIndoc.toString());
				}
			}
		}
	}

	/**
	 * This method inserts the shipment activity details to the GCShipmentActivity custom table.
	 *
	 * @param yfsEnv
	 * @param rootInEle
	 */
	private void updateGCShipmentActivity(YFSEnvironment yfsEnv, YFCElement rootInEle) {

		LOGGER.beginTimer("GCPickDetailActionsManager.updateGCShipmentActivity");
		LOGGER.verbose("GCPickDetailActionsManager.updateGCShipmentActivity : START");
		String action = rootInEle.getAttribute(GCConstants.ACTION);
		YFCElement shipmentLinesEle = rootInEle.getChildElement(GCConstants.SHIPMENT_LINES);
		YFCElement shipmentChildEle = shipmentLinesEle.getChildElement(GCConstants.SHIPMENT_LINE);

		YFCDocument gcShpmntActvIndoc = YFCDocument.createDocument(GCConstants.GC_SHIPMENT_ACTICITY);
		YFCElement gcShpmntActivity = gcShpmntActvIndoc.getDocumentElement();
		gcShpmntActivity.setAttribute(GCConstants.SHIPMENT_KEY, rootInEle.getAttribute(GCConstants.SHIPMENT_KEY));
		gcShpmntActivity.setAttribute(GCConstants.ORDER_HEADER_KEY,
				shipmentChildEle.getAttribute(GCConstants.ORDER_HEADER_KEY));

		//Call getEmployeeeID util to fetch ExtnEmployeeID for the logged in user.
		String employeeID = GCShipmentUtil.getEmployeeID(yfsEnv, rootInEle.getAttribute(GCConstants.USER_ID));
		// Changes -- End

		String isManagerApproval = rootInEle.getAttribute(GCConstants.IS_MANAGER_APPROVAL);
		if (YFCUtils.equals(GCConstants.YES, isManagerApproval)) {
			gcShpmntActivity.setAttribute(GCConstants.MANAGER_ID, employeeID);
		} else {
			gcShpmntActivity.setAttribute(GCConstants.USER_ID, employeeID);
		}

		if (YFCUtils.equals(GCConstants.CANCEL_ALL, action)) {
			gcShpmntActivity.setAttribute(GCConstants.USER_ACTION, GCConstants.CANCEL_ALL_LINES);
			gcShpmntActivity.setAttribute(GCConstants.AUDIT_RSNCODE, GCConstants.CNFRM_PICK_CNCLALL);
			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("Input to GCShipmentActivity table on cancelAll " + gcShpmntActvIndoc);
			}
		} else if (YFCUtils.equals(GCConstants.RESOURCE_ALL, action)) {
			gcShpmntActivity.setAttribute(GCConstants.USER_ACTION, GCConstants.RESOURCE_ALL_LINES);
			gcShpmntActivity.setAttribute(GCConstants.AUDIT_RSNCODE, GCConstants.CNFRM_PICK_RSRCALL);
			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("Input to GCShipmentActivity table on resourceAll " + gcShpmntActvIndoc);
			}
		} else if (!YFCCommon.isVoid(shipmentLinesEle) && shipmentLinesEle.hasChildNodes()) {

			if (YFCUtils.equals(GCConstants.ACTION_CANCEL_LINE, action)) {

				YFCIterable<YFCElement> shipmntLineLst = shipmentLinesEle.getChildren();
				for (YFCElement shpmntLineEle : shipmntLineLst) {

					gcShpmntActivity.setAttribute(GCConstants.SHIPMENT_KEY, shpmntLineEle.getAttribute(GCConstants.SHIPMENT_KEY));
					gcShpmntActivity.setAttribute(GCConstants.ORDER_LINE_KEY,
							shpmntLineEle.getAttribute(GCConstants.ORDER_LINE_KEY));
					gcShpmntActivity.setAttribute(GCConstants.USER_ACTION, GCConstants.CANCEL_LINE);
					gcShpmntActivity.setAttribute(GCConstants.AUDIT_RSNCODE, GCConstants.CNFRM_PICK_CNCL_LINE);
				}
				if (LOGGER.isVerboseEnabled()) {
					LOGGER.verbose("Input to GCShipmentActivity table on cancelLine " + gcShpmntActvIndoc);
				}

			} else if (YFCUtils.equals(GCConstants.ACTION_RESOURCE_LINE, action)) {

				YFCIterable<YFCElement> shipmntLineLst = shipmentLinesEle.getChildren();
				for (YFCElement shpmntLineEle : shipmntLineLst) {

					gcShpmntActivity.setAttribute(GCConstants.SHIPMENT_KEY, shpmntLineEle.getAttribute(GCConstants.SHIPMENT_KEY));
					gcShpmntActivity.setAttribute(GCConstants.ORDER_LINE_KEY,
							shpmntLineEle.getAttribute(GCConstants.ORDER_LINE_KEY));
					gcShpmntActivity.setAttribute(GCConstants.USER_ACTION, GCConstants.RESOURCE_LINE);
					gcShpmntActivity.setAttribute(GCConstants.AUDIT_RSNCODE, GCConstants.CNFRM_PICK_RSRCLINE);
				}
				if (LOGGER.isVerboseEnabled()) {
					LOGGER.verbose("Input to GCShipmentActivity table on resourceLine " + gcShpmntActvIndoc);
				}
			}
		}

		GCCommonUtil.invokeService(yfsEnv, GCConstants.GC_INSERT_SHPMNT_ACTIVITY, gcShpmntActvIndoc.getDocument());
		LOGGER.verbose("GCPickDetailActionsManager.updateGCShipmentActivity : END");
		LOGGER.endTimer("GCPickDetailActionsManager.updateGCShipmentActivity");

	}

	/**
	 * The class prepares the necessary input for calling changeOrder API.
	 *
	 * @param rootEle - Root element of the input document coming to the class.
	 * @return
	 */
	private YFCDocument prepareChngOrdIndoc(YFCElement rootEle) {

		LOGGER.beginTimer("GCPickDetailActionsManager.prepareChngOrdIndoc");
		LOGGER.verbose("GCPickDetailActionsManager.prepareChngOrdIndoc : END");
		String action = rootEle.getAttribute(GCConstants.ACTION);
		YFCElement shipmentLinesEle = rootEle.getChildElement(GCConstants.SHIPMENT_LINES);
		YFCElement shipmentChildEle = shipmentLinesEle.getChildElement(GCConstants.SHIPMENT_LINE);

		// Create changeOrder input document to
		// 1. Update ReasonCode/ReasonText incase of pick request cancellation.
		// 2. Suppress sourcing from selected store in case of re-sourcing
		YFCDocument changeOrderIndoc = YFCDocument.createDocument(GCConstants.ORDER);
		YFCElement orderEle = changeOrderIndoc.getDocumentElement();
		orderEle.setAttribute(GCConstants.ORDER_HEADER_KEY, shipmentChildEle.getAttribute(GCConstants.ORDER_HEADER_KEY));
		orderEle.setAttribute(GCConstants.ENTERPRISE_CODE, rootEle.getAttribute(GCConstants.ENTERPRISE_CODE));
		
		YFCElement orderLinesEle = orderEle.createChild(GCConstants.ORDER_LINES);

		String shipNode = rootEle.getAttribute(GCConstants.SHIP_NODE);

		if (!YFCCommon.isVoid(shipmentLinesEle) && shipmentLinesEle.hasChildNodes()) {

			if (YFCUtils.equals(GCConstants.ACTION_CANCEL_LINE, action)) {

				LOGGER.verbose("Enter Action - CancelLine. Update ReasonCode and ReasonText for the respective OrderLine.");
				YFCIterable<YFCElement> shipmntLineLst = shipmentLinesEle.getChildren();
				for (YFCElement shpmntLineEle : shipmntLineLst) {

					String orderLineKey = shpmntLineEle.getAttribute(GCConstants.ORDER_LINE_KEY);
					String reasonCode = shpmntLineEle.getAttribute(GCConstants.REASON_CODE);
					String reasonText = shpmntLineEle.getAttribute(GCConstants.REASON_TEXT);

					YFCElement orderLineEle = orderLinesEle.createChild(GCConstants.ORDER_LINE);
					orderLineEle.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey);
					orderLineEle.setAttribute(GCConstants.REASON_CODE, reasonCode);
					orderLineEle.setAttribute(GCConstants.REASON_TEXT, reasonText);
					
					if(LOGGER.isVerboseEnabled()){
						LOGGER.verbose("changeOrder Indoc after appending line details :" +changeOrderIndoc.toString());
					}
					if (YFCUtils.equals(GCConstants.BUNDLE, shpmntLineEle.getAttribute(GCConstants.KIT_CODE))) {
						YFCElement customComponents = shpmntLineEle.getChildElement(GCConstants.CUSTOM_COMPONENTS);
						if (!YFCCommon.isVoid(customComponents) && customComponents.hasChildNodes()) {
							YFCIterable<YFCElement> bundleCompList = customComponents.getChildren();
							for (YFCElement shipmentLine : bundleCompList) {
								YFCElement bundleCompLine = orderLinesEle.createChild(GCConstants.ORDER_LINE);
								bundleCompLine.setAttribute(GCConstants.ORDER_LINE_KEY,
										shipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY));
								bundleCompLine.setAttribute(GCConstants.REASON_CODE, reasonCode);
								bundleCompLine.setAttribute(GCConstants.REASON_TEXT, reasonText);
							}
						}
						if (LOGGER.isVerboseEnabled()) {
							LOGGER.verbose("changeOrder Indoc after appending bundle components :" + changeOrderIndoc.toString());
						}
					}

					YFCElement warrantyComponents = shpmntLineEle.getChildElement(GCConstants.WARRANTY_COMPONENTS);
					if (!YFCCommon.isVoid(warrantyComponents) && warrantyComponents.hasChildNodes()) {
						YFCIterable<YFCElement> warrantyCompList = warrantyComponents.getChildren();
						for (YFCElement shipmentLine : warrantyCompList) {
							YFCElement warrantyLineEle = orderLinesEle.createChild(GCConstants.SHIPMENT_LINE);
							warrantyLineEle.setAttribute(GCConstants.ORDER_LINE_KEY,
									shipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY));
							warrantyLineEle.setAttribute(GCConstants.REASON_CODE, reasonCode);
							warrantyLineEle.setAttribute(GCConstants.REASON_TEXT, reasonText);
						}
						if (LOGGER.isVerboseEnabled()) {
							LOGGER.verbose("changeOrder Indoc after appending warranty items :" + changeOrderIndoc.toString());
						}
					}
				}
			} else if (YFCUtils.equals(GCConstants.ACTION_RESOURCE_LINE, action)) {

				LOGGER.verbose("Suppress sourcing for the OrderLine");
				YFCIterable<YFCElement> shipmntLineLst = shipmentLinesEle.getChildren();
				for (YFCElement shpmntLineEle : shipmntLineLst) {
					prepareResourceDoc(shpmntLineEle, orderLinesEle, shipNode);
				}
			}
		}

		LOGGER.verbose("GCPickDetailActionsManager.prepareChngOrdIndoc : END ");
		LOGGER.endTimer("GCPickDetailActionsManager.prepareChngOrdIndoc");
		return changeOrderIndoc;

	}

	/**
	 * Append line details to changeOrder input.
	 *
	 * @param shpmntLineEle
	 * @param orderLinesEle
	 * @param shipNode
	 */
	public void prepareResourceDoc(YFCElement shpmntLineEle, YFCElement orderLinesEle, String shipNode) {

		LOGGER.beginTimer("GCPickDetailActionsManager.prepareResourceDoc");
		LOGGER.verbose("GCPickDetailActionsManager.prepareResourceDoc : START");
		String orderLineKey = shpmntLineEle.getAttribute(GCConstants.ORDER_LINE_KEY);

		if (YFCUtils.equals(GCConstants.BUNDLE, shpmntLineEle.getAttribute(GCConstants.KIT_CODE))) {
			YFCElement customComponents = shpmntLineEle.getChildElement(GCConstants.CUSTOM_COMPONENTS);
			if (!YFCCommon.isVoid(customComponents) && customComponents.hasChildNodes()) {
				YFCIterable<YFCElement> bundleCompList = customComponents.getChildren();
				for (YFCElement shipmentLine : bundleCompList) {
					YFCElement bundleCompLine = orderLinesEle.createChild(GCConstants.ORDER_LINE);
					bundleCompLine
					.setAttribute(GCConstants.ORDER_LINE_KEY, shipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY));
					bundleCompLine.setAttribute(GCConstants.SHIP_NODE, GCConstants.BLANK);
					bundleCompLine.setAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE, GCConstants.FLAG_N);

					YFCElement compLineSrcngCtrls = bundleCompLine.createChild(GCXmlLiterals.ORDERLINE_SRCNG_CTRLS);
					YFCElement compLineSrcngCtrl = compLineSrcngCtrls.createChild(GCXmlLiterals.ORDERLINE_SRCNG_CTRL);
					compLineSrcngCtrl.setAttribute(GCConstants.ACTION, GCConstants.CREATE);
					compLineSrcngCtrl.setAttribute(GCXmlLiterals.SUPPRESS_SRCNG, GCConstants.YES);
					compLineSrcngCtrl.setAttribute(GCConstants.NODE, shipNode);
				}
			}
		} else {

			YFCElement orderLineEle = orderLinesEle.createChild(GCConstants.ORDER_LINE);
			orderLineEle.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey);
			orderLineEle.setAttribute(GCConstants.SHIP_NODE, GCConstants.BLANK);
			orderLineEle.setAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE, GCConstants.FLAG_N);

			YFCElement orderLineSrcngCtrls = orderLineEle.createChild(GCXmlLiterals.ORDERLINE_SRCNG_CTRLS);
			YFCElement orderLineSrcngCtrlEle = orderLineSrcngCtrls.createChild(GCXmlLiterals.ORDERLINE_SRCNG_CTRL);
			orderLineSrcngCtrlEle.setAttribute(GCConstants.ACTION, GCConstants.CREATE);
			orderLineSrcngCtrlEle.setAttribute(GCXmlLiterals.SUPPRESS_SRCNG, GCConstants.YES);
			orderLineSrcngCtrlEle.setAttribute(GCConstants.NODE, shipNode);

		}

		YFCElement warrantyComponents = shpmntLineEle.getChildElement(GCConstants.WARRANTY_COMPONENTS);
		if (!YFCCommon.isVoid(warrantyComponents) && warrantyComponents.hasChildNodes()) {
			YFCIterable<YFCElement> warrantyCompList = warrantyComponents.getChildren();
			for (YFCElement shipmentLine : warrantyCompList) {
				YFCElement warrantyLineEle = orderLinesEle.createChild(GCConstants.ORDER_LINE);
				warrantyLineEle.setAttribute(GCConstants.ORDER_LINE_KEY, shipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY));
				warrantyLineEle.setAttribute(GCXmlLiterals.SHIP_NODE, GCConstants.BLANK);
				warrantyLineEle.setAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE, GCConstants.FLAG_N);

				YFCElement warrantyLineSrcngCtrls = warrantyLineEle.createChild(GCXmlLiterals.ORDERLINE_SRCNG_CTRLS);
				YFCElement warrantyLineSrcngCtrl = warrantyLineSrcngCtrls.createChild(GCXmlLiterals.ORDERLINE_SRCNG_CTRL);
				warrantyLineSrcngCtrl.setAttribute(GCConstants.ACTION, GCConstants.CREATE);
				warrantyLineSrcngCtrl.setAttribute(GCXmlLiterals.SUPPRESS_SRCNG, GCConstants.YES);
				warrantyLineSrcngCtrl.setAttribute(GCConstants.NODE, shipNode);
			}
		}

		LOGGER.verbose("GCPickDetailActionsManager.prepareResourceDoc : END");
		LOGGER.endTimer("GCPickDetailActionsManager.prepareResourceDoc");
	}

	/**
	 * The class prepares the necessary input to call changeOrderStatus API.
	 *
	 * @param rootInEle
	 * @param isPartialDecline
	 * @return
	 */
	private YFCDocument prepareChngOrdStatusIndoc(YFCElement rootInEle, boolean isPartialDecline) {

		LOGGER.beginTimer("GCPickDetailActionsManager.prepareChngOrdStatusIndoc");
		LOGGER.verbose("GCPickDetailActionsManager.prepareChngOrdStatusIndoc - START");
		YFCElement shipmentLinesEle = rootInEle.getChildElement(GCConstants.SHIPMENT_LINES);
		YFCElement shipmentChildEle = shipmentLinesEle.getChildElement(GCConstants.SHIPMENT_LINE);

		// Create changeOrderStatus input document to move re-sourced pick request/pick lines to
		// BackOrderedDAX status.
		YFCDocument chngOrdStatusIndoc = YFCDocument.createDocument(GCConstants.ORDER_STATUS_CHANGE);
		YFCElement ordStatusChngEle = chngOrdStatusIndoc.getDocumentElement();
		ordStatusChngEle.setAttribute(GCConstants.DOCUMENT_TYPE, GCConstants.SALES_ORDER_DOCUMENT_TYPE);
		ordStatusChngEle.setAttribute(GCConstants.ENTERPRISE_CODE, rootInEle.getAttribute(GCConstants.ENTERPRISE_CODE));
		ordStatusChngEle.setAttribute(GCConstants.ORDER_HEADER_KEY,
				shipmentChildEle.getAttribute(GCConstants.ORDER_HEADER_KEY));
		ordStatusChngEle.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.CHANGE_STATUS_CONFIRM);

		if (isPartialDecline) {
			YFCElement orderLines = ordStatusChngEle.createChild(GCConstants.ORDER_LINES);
			YFCIterable<YFCElement> shipmntLineLst = shipmentLinesEle.getChildren();
			for (YFCElement shpmntLineEle : shipmntLineLst) {
				final String quantity = shpmntLineEle.getAttribute(GCConstants.QUANTITY);
				prepareResourceStatusDoc(shpmntLineEle, orderLines, quantity);
				if (LOGGER.isVerboseEnabled()) {
					LOGGER.verbose("changeOrderStatus indoc after appending line details :" + chngOrdStatusIndoc.toString());
				}
			}
		} else {
			ordStatusChngEle.setAttribute(GCConstants.BASE_DROP_STATUS, GCConstants.BACKORDERED_DAX);
			ordStatusChngEle.setAttribute(GCConstants.CHANGE_FOR_ALL_AVAIL_QTY, GCConstants.YES);
			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("changeOrderStatus indoc for Order resourcing:" + chngOrdStatusIndoc.toString());
			}
		}

		LOGGER.verbose("GCPickDetailActionsManager.prepareChngOrdStatusIndoc : END ");
		LOGGER.endTimer("GCPickDetailActionsManager.prepareChngOrdStatusIndoc");
		return chngOrdStatusIndoc;

	}

	/**
	 * The method appends line level details for changeOrderStatus input document.
	 *
	 * @param shpmntLineEle
	 * @param orderLines
	 * @param quantity
	 */
	public void prepareResourceStatusDoc(YFCElement shpmntLineEle, YFCElement orderLines, String quantity) {

		LOGGER.beginTimer("GCPickDetailActionsManager.prepareResourceStatusDoc");
		LOGGER.verbose("GCPickDetailActionsManager.prepareResourceStatusDoc : START");
		String orderLineKey = shpmntLineEle.getAttribute(GCConstants.ORDER_LINE_KEY);

		if (YFCUtils.equals(GCConstants.BUNDLE, shpmntLineEle.getAttribute(GCConstants.KIT_CODE))) {
			YFCElement customComponents = shpmntLineEle.getChildElement(GCConstants.CUSTOM_COMPONENTS);
			if (!YFCCommon.isVoid(customComponents) && customComponents.hasChildNodes()) {
				YFCIterable<YFCElement> bundleCompList = customComponents.getChildren();
				for (YFCElement shipmentLine : bundleCompList) {
					YFCElement bundleCompLine = orderLines.createChild(GCConstants.ORDER_LINE);
					bundleCompLine
					.setAttribute(GCConstants.ORDER_LINE_KEY, shipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY));
					bundleCompLine.setAttribute(GCConstants.QUANTITY, shipmentLine.getAttribute(GCConstants.QUANTITY));
					bundleCompLine.setAttribute(GCConstants.BASE_DROP_STATUS, GCConstants.BACKORDERED_DAX);
				}
			}
		} else {
			YFCElement orderLine = orderLines.createChild(GCConstants.ORDER_LINE);
			orderLine.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey);
			orderLine.setAttribute(GCConstants.QUANTITY, quantity);
			orderLine.setAttribute(GCConstants.BASE_DROP_STATUS, GCConstants.BACKORDERED_DAX);
		}
		YFCElement warrantyComponents = shpmntLineEle.getChildElement(GCConstants.WARRANTY_COMPONENTS);
		if (!YFCCommon.isVoid(warrantyComponents) && warrantyComponents.hasChildNodes()) {
			YFCIterable<YFCElement> warrantyCompList = warrantyComponents.getChildren();
			for (YFCElement shipmentLine : warrantyCompList) {
				YFCElement warrantyLine = orderLines.createChild(GCConstants.ORDER_LINE);
				warrantyLine.setAttribute(GCConstants.ORDER_LINE_KEY, shipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY));
				warrantyLine.setAttribute(GCConstants.QUANTITY, shipmentLine.getAttribute(GCConstants.QUANTITY));
				warrantyLine.setAttribute(GCConstants.BASE_DROP_STATUS, GCConstants.BACKORDERED_DAX);
			}
		}

		LOGGER.verbose("GCPickDetailActionsManager.prepareResourceStatusDoc : END");
		LOGGER.endTimer("GCPickDetailActionsManager.prepareResourceStatusDoc");
	}

	/**
	 * The method handles complete cancellation and complete back-order of a pick request.
	 *
	 * @param yfsEnv
	 * @param inDoc
	 * @param isCompleteCancel
	 */
	private void cancelOrDeclineCompletePickRequest(YFSEnvironment yfsEnv, YFCDocument inDoc, String isCompleteCancel) {

		LOGGER.beginTimer("GCPickDetailActionsManager.cancelOrDeclineCompletePickRequest");
		LOGGER.verbose("GCPickDetailActionsManager.cancelOrDeclineCompletePickRequest : START ");
		YFCElement shipmentEle = inDoc.getDocumentElement();
		shipmentEle.setAttribute(GCConstants.ACTION, GCConstants.ACTION_CANCEL_SHIPMENT);
		if (YFCUtils.equals(GCConstants.YES, isCompleteCancel)) {
			shipmentEle.setAttribute(GCConstants.CANCEL_RMVD_QTY, GCConstants.YES);

		} else {
			shipmentEle.setAttribute(GCConstants.BACKORDER_RMVD_QTY, GCConstants.YES);
		}

		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("changeShipment input : " + inDoc.toString());
		}
		GCCommonUtil.invokeAPI(yfsEnv, GCConstants.CHANGE_SHIPMENT, inDoc.getDocument());
		LOGGER.verbose("changeShipment Successful");
		LOGGER.verbose("GCPickDetailActionsManager.cancelOrDeclineCompletePickRequest : END ");
		LOGGER.endTimer("GCPickDetailActionsManager.cancelOrDeclineCompletePickRequest");

	}

	/**
	 * The method implements logic to suppress sourcing from a selected node and moving the
	 * order/selected order lines to BackOrderDAX status.
	 *
	 * @param yfsEnv
	 * @param inDoc
	 * @param chngOrdStatusIndoc
	 * @param isPartialDecline
	 */
	private void manageAlternateSourcing(YFSEnvironment yfsEnv, YFCDocument inDoc, YFCDocument chngOrdStatusIndoc,
			boolean isPartialDecline) {

		LOGGER.beginTimer("GCPickDetailActionsManager.manageAlternateSourcing");
		LOGGER.verbose("GCPickDetailActionsManager.manageAlternateSourcing : START ");

		// Calling ChangeOrderStatus API before changeOrder call to fix GCSTORE-1192
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("Calling changeOrderStatus to move to BackOrderDAX :" + chngOrdStatusIndoc.toString());
		}

		GCCommonUtil.invokeAPI(yfsEnv, GCConstants.API_CHANGE_ORDER_STATUS, chngOrdStatusIndoc.getDocument());
		// The declined ShipmentLine cannot be sourced from the same node again. Call changeOrder API to
		// achieve the same.
		if (isPartialDecline) {

			LOGGER.verbose("Incase of Decline line : changeOrder Indoc for suppressing sourcing" + inDoc.toString());
			GCCommonUtil.invokeAPI(yfsEnv, GCConstants.CHANGE_ORDER, inDoc.getDocument());

		} else {

			YFCElement rootEle = inDoc.getDocumentElement();
			YFCElement shipmentLinesEle = rootEle.getChildElement(GCConstants.SHIPMENT_LINES);
			YFCElement shipmentChildEle = shipmentLinesEle.getChildElement(GCConstants.SHIPMENT_LINE);

			YFCDocument changeOrderIndoc = YFCDocument.createDocument(GCConstants.ORDER);
			YFCElement orderEle = changeOrderIndoc.getDocumentElement();
			orderEle.setAttribute(GCConstants.ENTERPRISE_CODE, rootEle.getAttribute(GCConstants.ENTERPRISE_CODE));
			orderEle.setAttribute(GCConstants.ORDER_HEADER_KEY, shipmentChildEle.getAttribute(GCConstants.ORDER_HEADER_KEY));
			YFCElement orderLinesEle = orderEle.createChild(GCConstants.ORDER_LINES);

			if (!YFCCommon.isVoid(shipmentLinesEle) && shipmentLinesEle.hasChildNodes()) {
				YFCIterable<YFCElement> shipmntLineLst = shipmentLinesEle.getChildren();
				for (YFCElement shpmntLineEle : shipmntLineLst) {

					String shipNode = rootEle.getAttribute(GCConstants.SHIP_NODE);

					if (YFCUtils.equals(GCConstants.BUNDLE, shpmntLineEle.getAttribute(GCConstants.KIT_CODE))) {
						YFCElement customComponents = shpmntLineEle.getChildElement(GCConstants.CUSTOM_COMPONENTS);
						if (!YFCCommon.isVoid(customComponents) && customComponents.hasChildNodes()) {
							YFCIterable<YFCElement> bundleCompList = customComponents.getChildren();
							for (YFCElement shipmentLine : bundleCompList) {
								YFCElement bundleCompLine = orderLinesEle.createChild(GCConstants.ORDER_LINE);
								bundleCompLine.setAttribute(GCConstants.ORDER_LINE_KEY,
										shipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY));
								bundleCompLine.setAttribute(GCConstants.SHIP_NODE, GCConstants.BLANK);
								bundleCompLine.setAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE, GCConstants.FLAG_N);

								YFCElement bundleLineSrcngCtrls = bundleCompLine.createChild(GCXmlLiterals.ORDERLINE_SRCNG_CTRLS);
								YFCElement bundleLineSrcngCtrlEle =
										bundleLineSrcngCtrls.createChild(GCXmlLiterals.ORDERLINE_SRCNG_CTRL);
								bundleLineSrcngCtrlEle.setAttribute(GCConstants.ACTION, GCConstants.CREATE);
								bundleLineSrcngCtrlEle.setAttribute(GCXmlLiterals.SUPPRESS_SRCNG, GCConstants.YES);
								bundleLineSrcngCtrlEle.setAttribute(GCConstants.NODE, shipNode);
							}
						}
					} else {

						YFCElement orderLineEle = orderLinesEle.createChild(GCConstants.ORDER_LINE);
						orderLineEle.setAttribute(GCConstants.ORDER_LINE_KEY,
								shpmntLineEle.getAttribute(GCConstants.ORDER_LINE_KEY));
						orderLineEle.setAttribute(GCConstants.SHIP_NODE, GCConstants.BLANK);
						orderLineEle.setAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE, GCConstants.FLAG_N);

						YFCElement orderLineSrcngCtrls = orderLineEle.createChild(GCXmlLiterals.ORDERLINE_SRCNG_CTRLS);
						YFCElement orderLineSrcngCtrlEle = orderLineSrcngCtrls.createChild(GCXmlLiterals.ORDERLINE_SRCNG_CTRL);
						orderLineSrcngCtrlEle.setAttribute(GCConstants.ACTION, GCConstants.CREATE);
						orderLineSrcngCtrlEle.setAttribute(GCXmlLiterals.SUPPRESS_SRCNG, GCConstants.YES);
						orderLineSrcngCtrlEle.setAttribute(GCConstants.NODE, shipNode);

					}
					YFCElement warrantyComponents = shpmntLineEle.getChildElement(GCConstants.WARRANTY_COMPONENTS);
					if (!YFCCommon.isVoid(warrantyComponents) && warrantyComponents.hasChildNodes()) {
						YFCIterable<YFCElement> warrantyCompList = warrantyComponents.getChildren();
						for (YFCElement shipmentLine : warrantyCompList) {
							YFCElement warrantyLine = orderLinesEle.createChild(GCConstants.ORDER_LINE);
							warrantyLine.setAttribute(GCConstants.ORDER_LINE_KEY,
									shipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY));
							warrantyLine.setAttribute(GCConstants.SHIP_NODE, GCConstants.BLANK);
							warrantyLine.setAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE, GCConstants.FLAG_N);

							YFCElement warrantyLineSrcngCtrls = warrantyLine.createChild(GCXmlLiterals.ORDERLINE_SRCNG_CTRLS);
							YFCElement warrantyLineSrcngCtrlEle =
									warrantyLineSrcngCtrls.createChild(GCXmlLiterals.ORDERLINE_SRCNG_CTRL);
							warrantyLineSrcngCtrlEle.setAttribute(GCConstants.ACTION, GCConstants.CREATE);
							warrantyLineSrcngCtrlEle.setAttribute(GCXmlLiterals.SUPPRESS_SRCNG, GCConstants.YES);
							warrantyLineSrcngCtrlEle.setAttribute(GCConstants.NODE, shipNode);
						}
					}


				}
			}
			if(LOGGER.isVerboseEnabled()){
				LOGGER.verbose("changeOrder Indoc for suppressing sourcing on complete decline :"
						+ changeOrderIndoc.toString());
			}

			GCCommonUtil.invokeAPI(yfsEnv, GCConstants.CHANGE_ORDER, changeOrderIndoc.getDocument());
		}
		LOGGER.verbose("GCPickDetailActionsManager.manageAlternateSourcing : END ");
		LOGGER.endTimer("GCPickDetailActionsManager.manageAlternateSourcing");
	}

	/**
	 * The method implements logic to update ReasonCode & ReasonText against an order/orderline on
	 * pick request cancellation.
	 *
	 * @param yfsEnv
	 * @param inDoc
	 * @param isPartialCancel
	 */
	private void updateReasonCodeAndText(YFSEnvironment yfsEnv, YFCDocument inDoc, boolean isPartialCancel) {

		LOGGER.beginTimer("GCPickDetailActionsManager.updateReasonCodeAndText");
		LOGGER.verbose("GCPickDetailActionsManager.updateReasonCodeAndText : START ");
		if (LOGGER.isVerboseEnabled()) {
			LOGGER.verbose("On line cancel - changeOrder Indoc :" + inDoc.toString());
		}
		if (isPartialCancel) {
			// GCSTORE - 7102 - Add notes of Cancel via ExStore
			YFCElement eleOrderLine = inDoc.getDocumentElement().getElementsByTagName(GCConstants.ORDER_LINE).item(0);
			String reasonCode = eleOrderLine.getAttribute(GCConstants.REASON_CODE);
			String reasonText = eleOrderLine.getAttribute(GCConstants.REASON_TEXT);
			YFCElement eleNotes = eleOrderLine.createChild(GCConstants.NOTES);
			YFCElement eleNote = eleNotes.createChild(GCConstants.NOTE);
			eleNote.setAttribute(GCConstants.NOTE_TEXT, reasonText);
			eleNote.setAttribute(GCConstants.REASON_CODE, reasonCode);
			
			GCCommonUtil.invokeAPI(yfsEnv, GCConstants.CHANGE_ORDER, inDoc.getDocument());
		} else {

			YFCElement rootEle = inDoc.getDocumentElement();
			YFCElement shipmentLinesEle = rootEle.getChildElement(GCConstants.SHIPMENT_LINES);
			YFCElement shipmentChildEle = shipmentLinesEle.getChildElement(GCConstants.SHIPMENT_LINE);
			String reasonCode = rootEle.getAttribute(GCConstants.REASON_CODE);
			String reasonText = rootEle.getAttribute(GCConstants.REASON_TEXT);
			// Form changeOrder API input to update ReasonCode and ReasonText
			YFCDocument changeOrderIndoc = YFCDocument.createDocument(GCConstants.ORDER);
			YFCElement orderEle = changeOrderIndoc.getDocumentElement();
			orderEle.setAttribute(GCConstants.ENTERPRISE_CODE, rootEle.getAttribute(GCConstants.ENTERPRISE_CODE));
			orderEle.setAttribute(GCConstants.ORDER_HEADER_KEY, shipmentChildEle.getAttribute(GCConstants.ORDER_HEADER_KEY));
			//Added for GCSTORE-6877: Start
			orderEle.setAttribute(GCConstants.EXTN_PROG_ID, GCConstants.PROG_ID_EXSTORE); 
			//End
			YFCElement orderLinesEle = orderEle.createChild(GCConstants.ORDER_LINES);

			if (!YFCCommon.isVoid(shipmentLinesEle) && shipmentLinesEle.hasChildNodes()) {
				YFCIterable<YFCElement> shipmntLineLst = shipmentLinesEle.getChildren();
				for (YFCElement shpmntLineEle : shipmntLineLst) {

					YFCElement orderLineEle = orderLinesEle.createChild(GCConstants.ORDER_LINE);
					orderLineEle.setAttribute(GCConstants.ORDER_LINE_KEY, shpmntLineEle.getAttribute(GCConstants.ORDER_LINE_KEY));
					orderLineEle.setAttribute(GCConstants.REASON_CODE, reasonCode);
					orderLineEle.setAttribute(GCConstants.REASON_TEXT, reasonText);
					//Added for GCSTORE-6877: Start
					orderLineEle.setAttribute(GCConstants.IS_COMPLETE_LINE_CANCEL, GCConstants.YES);
					//End
					
					YFCElement eleNotes = orderLineEle.createChild(GCConstants.NOTES);
					YFCElement eleNote = eleNotes.createChild(GCConstants.NOTE);
					eleNote.setAttribute(GCConstants.NOTE_TEXT, reasonText);
					eleNote.setAttribute(GCConstants.REASON_CODE, reasonCode);

					if (YFCUtils.equals(GCConstants.BUNDLE, shpmntLineEle.getAttribute(GCConstants.KIT_CODE))) {
						YFCElement customComponents = shpmntLineEle.getChildElement(GCConstants.CUSTOM_COMPONENTS);
						if (!YFCCommon.isVoid(customComponents) && customComponents.hasChildNodes()) {
							YFCIterable<YFCElement> bundleCompList = customComponents.getChildren();
							for (YFCElement shipmentLine : bundleCompList) {
								YFCElement bundleCompLine = orderLinesEle.createChild(GCConstants.ORDER_LINE);
								bundleCompLine.setAttribute(GCConstants.ORDER_LINE_KEY,
										shipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY));
								bundleCompLine.setAttribute(GCConstants.REASON_CODE, reasonCode);
								bundleCompLine.setAttribute(GCConstants.REASON_TEXT, reasonText);
							}
						}
					}
					YFCElement warrantyComponents = shpmntLineEle.getChildElement(GCConstants.WARRANTY_COMPONENTS);
					if (!YFCCommon.isVoid(warrantyComponents) && warrantyComponents.hasChildNodes()) {
						YFCIterable<YFCElement> warrantyCompList = warrantyComponents.getChildren();
						for (YFCElement shipmentLine : warrantyCompList) {
							YFCElement warrantyLine = orderLinesEle.createChild(GCConstants.ORDER_LINE);
							warrantyLine.setAttribute(GCConstants.ORDER_LINE_KEY,
									shipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY));
							warrantyLine.setAttribute(GCConstants.REASON_CODE, reasonCode);
							warrantyLine.setAttribute(GCConstants.REASON_TEXT, reasonText);
						}
					}
				}
			}
			if (LOGGER.isVerboseEnabled()) {
				LOGGER.verbose("Calling changeOrder on complete cancel :" + changeOrderIndoc.toString());
			}
			GCCommonUtil.invokeAPI(yfsEnv, GCConstants.CHANGE_ORDER, changeOrderIndoc.getDocument());

		}
		LOGGER.verbose("GCPickDetailActionsManager.updateReasonCodeAndText : END ");
		LOGGER.endTimer("GCPickDetailActionsManager.updateReasonCodeAndText");
	}

	@Override
	public void setProperties(Properties arg0) {


	}


}
