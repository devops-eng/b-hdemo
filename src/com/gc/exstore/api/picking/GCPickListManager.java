package com.gc.exstore.api.picking;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * This class is used prepare a document containing sorted Shipment Lines for the Picking list
 * screen based Level Of Service
 *
 * @author karthik.kumaresan
 *
 */
public class GCPickListManager {


  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCPickListManager.class);


  private List<YFCElement> sortedShipmentList = new ArrayList<YFCElement>();
  private List<YFCElement> newLOSShipmentList = new ArrayList<YFCElement>();

  public List<YFCElement> getSortedShipmentList() {
    return sortedShipmentList;
  }

  public void setSortedShipmentList(List<YFCElement> sortedShipmentList) {
    this.sortedShipmentList = sortedShipmentList;
  }

  /**
   * This method invokes the LevelOfService method to return a list of shipment sorted based on
   * level of service
   *
   * @param env YFSEnvironment
   * @param inputDoc Document - The input XML received from the ExStore UI
   * @return Document - The list of Shipments with Status '', ShipNode in sorted order
   * @throws SAXException
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws YFSException
   *
   */
  public Document sortShipment(YFSEnvironment env, Document inputDoc) {

    LOGGER.beginTimer("PickListManager.getPickListByShipmentStatus");
    LOGGER.verbose("PickListManager.getPickListByShipmentStatus--Begin");

    final YFCDocument finalDoc = YFCDocument.createDocument(GCXmlLiterals.SHIPMENTS);

    // Invoke getShipmentList API to fetch list of shipments
    List<YFCElement> listShipment = new ArrayList<YFCElement>();
    getListOfShipments(env, inputDoc, listShipment);

    removeStandAloneWarrantyShipments(listShipment);
    
    int listShipmentlength = listShipment.size();
    if (listShipmentlength == 0) {
      return finalDoc.getDocument();
    }

    // create a map that maps level of service in readable form
    Map<String, String> commonmap = readableLosMap(env, listShipment);

    // convert the shipment list into a map with Level Of Service as the key
    Map<String, List<YFCElement>> levelOfServiceMap = createLOSShipmentMap(listShipment, commonmap);

    // Fetch common code 'GC_STORE_PICK_SPEC' and prepare listed sorted on priority
    YFCNodeList<YFCElement> listLevelOfService = levelOfServiceList(env, listShipment);

    // sort shipment based on level of service
    sortedShipmentList = sortShipmentBasedOnLevelOfService(levelOfServiceMap, listLevelOfService);

    final YFCElement shipmentsIndocRootEle = finalDoc.getDocumentElement();
    for (int f = 0; f < sortedShipmentList.size(); f++) {
      shipmentsIndocRootEle.importNode(sortedShipmentList.get(f));
    }


    LOGGER.verbose("PickListManager.getPickListByShipmentStatus--End");
    LOGGER.endTimer("PickListManager.getPickListByShipmentStatus");
    return finalDoc.getDocument();

  }

  private void removeStandAloneWarrantyShipments(List<YFCElement> listShipment) {
    for (int i = 0; i < listShipment.size(); i++) {
      YFCElement eleShipment = listShipment.get(i);
      String isWarranty = "";
      boolean isStandaloneWarranty = true;
      YFCElement eleShipmentLines = eleShipment.getChildElement(GCConstants.SHIPMENT_LINES);

      YFCNodeList<YFCElement> nlShipmentLines = eleShipmentLines.getElementsByTagName(GCConstants.SHIPMENT_LINE);
      for (YFCElement eleShipmentLine : nlShipmentLines) {
        YFCElement eleOrderLine = eleShipmentLine.getChildElement(GCConstants.ORDER_LINE);
        if (!YFCCommon.isVoid(eleOrderLine)) {
          YFCElement eleOrderLineExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
          if (!YFCCommon.isVoid(eleOrderLineExtn)) {
            isWarranty = eleOrderLineExtn.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
          }
          if (!YFCUtils.equals(GCConstants.YES, isWarranty)) {
            isStandaloneWarranty = false;
            break;
          }
        }
      }

      if (isStandaloneWarranty) {
        listShipment.remove(eleShipment);
        --i;
      }
    }

  }

  /**
   * create a map that maps level of service in readable form
   *
   * @param env
   * @param listShipment
   * @return
   */
  private Map<String, String> readableLosMap(YFSEnvironment env, List<YFCElement> listShipment) {
    Map<String, String> commonmap = new HashMap<String, String>();
    YFCElement shipmentEle = listShipment.get(0);
    String organizationCode = shipmentEle.getAttribute(GCConstants.ENTERPRISE_CODE);
    Document commonCodeOutDocOne =
        GCCommonUtil.getCommonCodeListByTypeSortBy(env, GCConstants.GC_EXSTORE_LOS, organizationCode);
    YFCDocument commonCodeOutDoc = YFCDocument.getDocumentFor(commonCodeOutDocOne);
    YFCElement commonCodeOutDocrootEle = commonCodeOutDoc.getDocumentElement();
    YFCNodeList<YFCElement> listCommonCode = commonCodeOutDocrootEle.getElementsByTagName(GCConstants.COMMON_CODE);
    int k = listCommonCode.getLength();
    for (int l = 0; l < k; l++) {
      YFCElement eleCommonCode = listCommonCode.item(l);
      String strCodeValue = eleCommonCode.getAttribute(GCConstants.CODE_VALUE);
      String strCodeShortDescription = eleCommonCode.getAttribute(GCConstants.CODE_SHORT_DESCRIPTION);
      commonmap.put(strCodeValue, strCodeShortDescription);

    }
    return commonmap;
  }



  /**
   * This method is used to sort the shipment list based on Level Of Service
   *
   * @param levelOfServiceMap NodeList -
   * @param levelOfServicemap
   * @return
   */


  private List<YFCElement> sortShipmentBasedOnLevelOfService(Map<String, List<YFCElement>> levelOfServiceMap,
      YFCNodeList<YFCElement> levelOfServiceList) {

    LOGGER.beginTimer("PickListManager.levelOfServiceSort");
    LOGGER.verbose("PickListManager.levelOfServiceSort--Begin");

    for (YFCElement levelOfService : levelOfServiceList) {

      String strLevelOfService = levelOfService.getAttribute(GCConstants.CODE_LONG_DESCRIPTION);
      if (levelOfServiceMap.containsKey(strLevelOfService)) {
        List<YFCElement> shipmentList = levelOfServiceMap.get(strLevelOfService);

        if (!shipmentList.isEmpty()) {
          sortedShipmentList.addAll(shipmentList);
        }

      }
    }

    // adding Shipments which have LOS other than mentioned in CommonCode
    if (!newLOSShipmentList.isEmpty()) {
      sortedShipmentList.addAll(newLOSShipmentList);
    }

    LOGGER.verbose("PickListManager.levelOfServiceSort--End");
    LOGGER.endTimer("PickListManager.levelOfServiceSort");
    return sortedShipmentList;

  }



  /**
   * this method is to prepare a map of shipments with LOS as the key and value in the Map is List
   * of corresponding Shipment elements and stamp the aging and item description of the first
   * shipment line at the shipment level
   *
   * @param listShipment
   * @param commonmap
   * @return levelOfServiceMap
   */
  private Map<String, List<YFCElement>> createLOSShipmentMap(List<YFCElement> listShipment,
      Map<String, String> commonmap) {


    LOGGER.beginTimer("PickListManager.shipmentMap");
    LOGGER.verbose("PickListManager.shipmentMap--Begin");


    Map<String, List<YFCElement>> levelOfServiceMap = new HashMap<String, List<YFCElement>>();

    for (int j = 0; j < listShipment.size(); j++) {

      LOGGER.verbose("Value Of  j" + j);
      YFCElement shipmentEle = listShipment.get(j);
      YFCElement shipLinesEle = shipmentEle.getChildElement(GCConstants.SHIPMENT_LINES);
      YFCElement extnEle = shipmentEle.getChildElement(GCConstants.EXTN);
      YFCElement shipLineEle = shipLinesEle.getElementsByTagName(GCConstants.SHIPMENT_LINE).item(0);

      // Stamping Same Store Pick Up level of service at shipment level
      String shipmentLevelOfService = shipmentEle.getAttribute(GCConstants.LEVEL_OF_SERVICE);
      if (YFCCommon.isVoid(shipmentLevelOfService)) {
        shipmentLevelOfService = GCConstants.STORE_PICKUP_BLANK;
      }
      // Stamping item description at shipment level
      String itemDesc = shipLineEle.getAttribute(GCConstants.ITEM_DESCRIPTION);
      shipmentEle.setAttribute(GCConstants.ITEM_DESCRIPTION, itemDesc);
      // Stamping orderno and orderheaderkey at shipment level(to be removed)
      String orderNumber = shipLineEle.getAttribute(GCConstants.ORDER_NO);
      shipmentEle.setAttribute(GCConstants.ORDER_NO, orderNumber);
      String orderHeaderKey = shipLineEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
      shipmentEle.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
      // Stamping Order creation date
      String extnOrderDate = extnEle.getAttribute(GCConstants.EXTN_ORDER_DATE);
      shipmentEle.setAttribute(GCConstants.ORDERED_ON, extnOrderDate);
      // Setting the IsExpedite flag
      String isExpedite = extnEle.getAttribute(GCConstants.EXTN_EXPEDITE_FLAG);
      shipmentEle.setAttribute(GCConstants.IS_EXPEDITE, isExpedite);
      // Setting IsShipToCustomer flag
      String isShipToCustomer = extnEle.getAttribute(GCConstants.EXTN_FULFILLMENT_TYPE);
      if (YFCUtils.equals(isShipToCustomer, GCConstants.SHIP_2_CUSTOMER)) {
        shipmentEle.setAttribute(GCConstants.IS_SHIP_TO_CUSTOMER, GCConstants.YES);
      }
      // Setting IsShipToStore flag and same store pick up flags
      String isShipToStore = extnEle.getAttribute(GCConstants.EXTN_PICKUP_STORE_ID);
      String shipNode = shipmentEle.getAttribute(GCConstants.SHIP_NODE);
      if (!YFCCommon.isVoid(isShipToStore) && !YFCUtils.equals(shipNode, isShipToStore)) {
        shipmentEle.setAttribute(GCConstants.IS_SHIP_TO_STORE, GCConstants.YES);
      } else if (YFCUtils.equals(shipNode, isShipToStore)) {
        shipmentEle.setAttribute(GCConstants.IS_SAME_STORE_PICKUP, GCConstants.YES);
      }
      // Stamping Aging
      String extnStatusDate = shipmentEle.getAttribute(GCXmlLiterals.STATUS_DATE);
      String aging =
          GCDateUtils.getTimeDiffrenceInDaysHoursAndMinutes(extnStatusDate,
              GCDateUtils.getCurrentTime(GCConstants.DATE_TIME_FORMAT));
      LOGGER.verbose("Value Of  aging" + aging);
      shipmentEle.setAttribute(GCConstants.AGING, aging);
      List<YFCElement> shipmentEleList = levelOfServiceMap.get(shipmentLevelOfService);

      // Converting Level Of Service to readable form
      String mapLevelOfService = commonmap.get(shipmentLevelOfService);

      // Adding logic for consideration of LOS which are not listed in common-code
      if (YFCCommon.isVoid(mapLevelOfService)) {
        newLOSShipmentList.add(shipmentEle);
        continue;
      }

      shipmentEle.setAttribute(GCConstants.LEVEL_OF_SERVICE, mapLevelOfService);
      // Removing Same Store Pick Up
      if (YFCUtils.equals(shipmentLevelOfService, GCConstants.STORE_PICKUP_BLANK)) {
        shipmentEle.setAttribute(GCConstants.LEVEL_OF_SERVICE, GCConstants.BLANK);
      }
      if (shipmentEleList == null) {
        shipmentEleList = new ArrayList<YFCElement>();
        shipmentEleList.add(shipmentEle);
      } else {
        shipmentEleList.add(shipmentEle);
      }
      levelOfServiceMap.put(shipmentLevelOfService, shipmentEleList);

    }

    LOGGER.verbose("PickListManager.shipmentMap--End");
    LOGGER.endTimer("PickListManager.shipmentMap");
    return levelOfServiceMap;
  }



  /**
   * This method is used fetch the Level Of Service from the common codes
   *
   * @param env
   * @param listShipment
   * @return
   * @return Map with Level Of Service in serialized order based on priority
   */

  private YFCNodeList<YFCElement> levelOfServiceList(YFSEnvironment env, List<YFCElement> listShipment) {
    LOGGER.beginTimer("PickListManager.levelOfServiceListCreation");
    LOGGER.verbose("PickListManager.levelOfServiceListCreation--Begin");


    YFCElement shipmentEle = listShipment.get(0);
    String organizationCode = shipmentEle.getAttribute(GCConstants.ENTERPRISE_CODE);
    Document commonCodeOutDocOne =
        GCCommonUtil.getCommonCodeListByTypeSortBy(env, GCConstants.GC_STORE_PICK_SPEC, organizationCode);
    YFCDocument commonCodeOutDoc = YFCDocument.getDocumentFor(commonCodeOutDocOne);


    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("commonCodeOutDoc Document: " + commonCodeOutDoc.toString());
    }

    YFCElement getcommonCodeOutDocRootele = commonCodeOutDoc.getDocumentElement();
    YFCNodeList<YFCElement> listLevelOfService =
        getcommonCodeOutDocRootele.getElementsByTagName(GCConstants.COMMON_CODE);



    LOGGER.verbose("PickListManager.levelOfServiceListCreation--End");
    LOGGER.endTimer("PickListManager.levelOfServiceListCreation");

    return listLevelOfService;
  }



  /**
   * This method is used to get the list of shipments in time(OrderHeaderKey) sorted manner which
   * are in the following status ready for back room pick
   *
   * @param env YFSEnvironment
   * @param inputDoc Document - The input XML received from the ExStore UI
   * @param listShipment
   * @return Shipment list
   */

  private void getListOfShipments(YFSEnvironment env, Document inputDoc, List<YFCElement> listShipment) {

    LOGGER.beginTimer("PickListManager.getShipmentList");
    LOGGER.verbose("PickListManager.getShipmentList--Begin");

    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    YFCElement inDocRootEle = inDoc.getDocumentElement();
    String shipNode = inDocRootEle.getAttribute(GCConstants.SHIP_NODE);

    // getShipmentList inDoc preparation
    YFCDocument getshipmentInDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement getshipmentInDocRootEle = getshipmentInDoc.getDocumentElement();

    getshipmentInDocRootEle.setAttribute(GCConstants.SHIP_NODE, shipNode);
    getshipmentInDocRootEle.setAttribute(GCConstants.STATUS, GCConstants.STATUS_READY_FOR_BACKROOM_PICK);

    YFCElement orderByEle = getshipmentInDoc.createElement(GCConstants.ORDER_BY);
    getshipmentInDocRootEle.appendChild(orderByEle);
    YFCElement attributeEle = getshipmentInDoc.createElement(GCConstants.ATTRIBUTE);
    orderByEle.appendChild(attributeEle);
    attributeEle.setAttribute(GCConstants.DESC, GCConstants.N);
    attributeEle.setAttribute(GCConstants.NAME, GCConstants.ORDER_HEADER_KEY);
    YFCDocument getshipmentTempDoc = YFCDocument.getDocumentFor(GCConstants.GET_SHIP_LIST_TEMPLATE_PACK_LIST);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getShipListOutdoc Document: " + getshipmentInDoc.toString());
    }

    YFCDocument getshipmentOutDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, getshipmentInDoc, getshipmentTempDoc);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getShipListOutdoc Document: " + getshipmentOutDoc.toString());
    }

    YFCElement getshipmentOutDocRootele = getshipmentOutDoc.getDocumentElement();
    YFCNodeList<YFCElement> listShipmentOut = getshipmentOutDocRootele.getElementsByTagName(GCConstants.SHIPMENT);
    for (YFCElement shipmentEle : listShipmentOut) {
      YFCElement eleShipmentLines = shipmentEle.getChildElement(GCConstants.SHIPMENT_LINES);
      if (eleShipmentLines.hasChildNodes()) {
        listShipment.add(shipmentEle);
      }
    }

    LOGGER.verbose("PickListManager.getShipmentList--End");
    LOGGER.endTimer("PickListManager.getShipmentList");

  }



}