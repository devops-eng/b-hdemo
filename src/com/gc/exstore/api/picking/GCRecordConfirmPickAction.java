package com.gc.exstore.api.picking;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.exstore.utils.GCShipmentUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class is responsible for recording the user id of person confirm pick in custom activity
 * table and also it updates commission data in order line commission table
 *
 * @author shanil.sharma
 *
 */
public class GCRecordConfirmPickAction {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCRecordConfirmPickAction.class.getName());

  /**
   * This method is used to prepare lineKeyMap, update Order after recalculating the commission &
   * call recordUserID() to record userID
   *
   * @param env
   * @param inputDoc
   * @return
   */
  public YFCDocument updateUserIdAndCommission(YFSEnvironment env, YFCElement eleShipment) {

    LOGGER.beginTimer("GCRecordConfirmPickAction.updateUserIdAndCommission");
    LOGGER.verbose("GCRecordConfirmPickAction.updateUserIdAndCommission--Begin");

    final String orderHeaderKey = eleShipment.getAttribute(GCConstants.ORDER_HEADER_KEY);
    final String shipmentKey = eleShipment.getAttribute(GCConstants.SHIPMENT_KEY);
    YFCElement eleShipNode = eleShipment.getChildElement(GCConstants.SHIP_NODE);
    final String shipNodeExtn = getShipNodeExtn(env, eleShipment);
    final String shipNode = eleShipNode.getAttribute(GCConstants.SHIP_NODE) + shipNodeExtn;
    final String userID = GCShipmentUtil.getEmployeeID(env, eleShipment.getAttribute(GCConstants.USER_ID));

    String managerID = eleShipment.getAttribute(GCConstants.MANAGER_ID);
    final String action = eleShipment.getAttribute(GCConstants.ACTION);
    if (YFCUtils.equals(action, GCConstants.PARTIAL_PICK)) {
      managerID = userID;
    }

    // to store OrderLineKey as key & Shipment Element as value.
    Map<String, YFCElement> lineKeyMap = prepareLineKeyMap(eleShipment);

    // Start preparing InputDoc for changeOrder call
    YFCDocument changeOrderDoc = YFCDocument.createDocument(GCConstants.ORDER);
    final YFCElement eleOdrIndoc = changeOrderDoc.getDocumentElement();
    eleOdrIndoc.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
    eleOdrIndoc.setAttribute(GCConstants.ACTION, GCConstants.ACTION_MODIFY);
    YFCElement eleOdrLinesIndoc = eleOdrIndoc.createChild(GCConstants.ORDER_LINES);

    YFCDocument outputDoc = null;
    // Call getOrderLineList api
    final YFCDocument getOdrLineListOutdoc = callGetOrderLineListApi(env, orderHeaderKey);

    final YFCElement eleOdrLineList = getOdrLineListOutdoc.getDocumentElement();
    final YFCNodeList<YFCElement> nlOdrLine = eleOdrLineList.getElementsByTagName(GCConstants.ORDER_LINE);

    boolean isChangeOrder = false;
    for (YFCElement eleOrderLine : nlOdrLine) {
      final String orderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      if (!lineKeyMap.containsKey(orderLineKey)) {
        continue;
      }
      final YFCElement eleOdrLineIndoc = eleOdrLinesIndoc.createChild(GCConstants.ORDER_LINE);
      eleOdrLineIndoc.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey);
      eleOdrLineIndoc.setAttribute(GCConstants.ACTION, GCConstants.ACTION_MODIFY);
      final YFCElement eleOdrLineExtnIndoc = eleOdrLineIndoc.createChild(GCConstants.EXTN);
      final YFCElement eleOdrLineCommLstIndoc = eleOdrLineExtnIndoc.createChild(GCXmlLiterals.ODR_LINE_COMM_LIST);
      final YFCElement eleOdrLineExtn = eleOrderLine.getChildElement(GCConstants.EXTN);

      YFCNodeList<YFCElement> nlOdrLineComm = null;
      int lengthLineComm = 0;
      final YFCElement eleOdrLineCommList = eleOdrLineExtn.getChildElement(GCXmlLiterals.ODR_LINE_COMM_LIST);
      if (!YFCCommon.isVoid(eleOdrLineCommList)) {
        nlOdrLineComm = eleOdrLineCommList.getElementsByTagName(GCXmlLiterals.ODR_LINE_COMM);
        lengthLineComm = nlOdrLineComm.getLength();
      }
      if (lineKeyMap.containsKey(orderLineKey)) {
        LOGGER.verbose("Line key in map");
        isChangeOrder =
            prepareChangeOrderInput(lengthLineComm, orderLineKey, eleOdrLineCommLstIndoc, shipNode, nlOdrLineComm,
                lineKeyMap);
        LOGGER.verbose("isChangeOrder :" + isChangeOrder);

      }
    }
    if (isChangeOrder) {
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("changeOrderDoc---- : " + changeOrderDoc.toString());
      }
      YFCDocument tempdoc = YFCDocument.getDocumentFor(GCConstants.CHANGE_ORDER_TEMP);
      GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, changeOrderDoc, tempdoc);
      outputDoc = prepareOutDoc(changeOrderDoc, lineKeyMap);
    }

    recordUserID(env, userID, managerID, orderHeaderKey, shipmentKey, action);

    if (LOGGER.isVerboseEnabled() && !YFCCommon.isVoid(outputDoc)) {
      LOGGER.verbose("outputDoc : " + outputDoc.toString());
    }
    LOGGER.verbose("GCRecordConfirmPickAction.updateUserIdAndCommission--end");
    LOGGER.endTimer("GCRecordConfirmPickAction.updateUserIdAndCommission");

    return outputDoc;

  }

  /**
   * Get Extension for ShipNode to create House Number.
   *
   * @param env
   * @param eleShipment
   * @return
   */
  private String getShipNodeExtn(YFSEnvironment env, YFCElement eleShipment) {

    final String orgCode = eleShipment.getAttribute(GCConstants.ENTERPRISE_CODE);
    final YFCDocument commonCodeOutDoc =
        GCCommonUtil.getCommonCodeListByTypeAndValue(env, GCConstants.GC_HOUSE_NO_EXTN, orgCode,
            GCConstants.HOUSE_NO_EXTN);
    YFCElement eleOutXml = commonCodeOutDoc.getDocumentElement();
    final YFCElement eleCommonCode = eleOutXml.getElementsByTagName(GCConstants.COMMON_CODE).item(0);

    return eleCommonCode.getAttribute(GCConstants.CODE_SHORT_DESCRIPTION);
  }

  /**
   * This method is used to prepare output doc which contains commissions information
   *
   * @param changeOrderDoc
   * @param lineKeyMap
   * @return
   */
  private YFCDocument prepareOutDoc(YFCDocument changeOrderDoc, Map<String, YFCElement> lineKeyMap) {


    LOGGER.beginTimer("GCRecordConfirmPickAction.prepareOutDoc");
    LOGGER.verbose("GCRecordConfirmPickAction.prepareOutDoc--Begin");
    YFCDocument outputDoc = YFCDocument.createDocument(GCConstants.COMMISSIONS);
    YFCElement eleCommissions = outputDoc.getDocumentElement();
    YFCElement eleOrderOut = changeOrderDoc.getDocumentElement();
    YFCElement eleOrderLinesOut = eleOrderOut.getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> nlOdrLine = eleOrderLinesOut.getElementsByTagName(GCConstants.ORDER_LINE);
    final int lengthOdrLine = nlOdrLine.getLength();
    for (int i = 0; i < lengthOdrLine; i++) {
      final YFCElement eleOdrLine = nlOdrLine.item(i);
      final YFCElement eleCorresShipLine = lineKeyMap.get(eleOdrLine.getAttribute(GCConstants.ORDER_LINE_KEY));
      final String primeLineNo = eleCorresShipLine.getAttribute(GCConstants.PRIME_LINE_NO);
      final YFCElement eleOdrLineExtn = eleOdrLine.getChildElement(GCConstants.EXTN);
      final YFCElement eleOdrCommList = eleOdrLineExtn.getChildElement(GCXmlLiterals.ODR_LINE_COMM_LIST);
      YFCIterable<YFCElement> nlOdrLineComm = eleOdrCommList.getChildren();

      for (YFCElement eleOdrLineComm : nlOdrLineComm) {
        final YFCElement eleCommission = eleCommissions.createChild(GCConstants.COMMISSION);
        eleCommission.setAttribute(GCConstants.LINE_NUMBER, primeLineNo);
        eleCommission.setAttribute(GCConstants.SALES_PERSON, eleOdrLineComm.getAttribute(GCConstants.USER_ID));
        eleCommission.setAttribute(GCConstants.SALES_PERCENT,
            eleOdrLineComm.getAttribute(GCConstants.ALLOCATION_PERCENTAGE));
        eleCommission.setAttribute(GCConstants.COMMISSION_TYPE,
            eleOdrLineComm.getAttribute(GCConstants.COMMISSION_TYPE));
      }

    }
    LOGGER.verbose("GCRecordConfirmPickAction.prepareOutDoc--end");
    LOGGER.endTimer("GCRecordConfirmPickAction.prepareOutDoc");
    return outputDoc;
  }

  /**
   * This method is used to prepare a map which stores OrderLineKey as key & Shipment Element as
   * value.
   *
   * @param eleShipment
   * @return
   */
  private Map<String, YFCElement> prepareLineKeyMap(YFCElement eleShipment) {

    LOGGER.beginTimer("GCRecordConfirmPickAction.prepareLineKeyMap");
    LOGGER.verbose("GCRecordConfirmPickAction.prepareLineKeyMap--Begin");

    Map<String, YFCElement> prepareLineKeyMap = new HashMap<String, YFCElement>();
    final YFCElement eleShipmentLines = eleShipment.getChildElement(GCConstants.SHIPMENT_LINES);

    YFCIterable<YFCElement> nlShipmentLine = eleShipmentLines.getChildren();

    for (YFCElement eleShipLine : nlShipmentLine) {
      prepareLineKeyMap.put(eleShipLine.getAttribute(GCConstants.ORDER_LINE_KEY), eleShipLine);
    }
    LOGGER.verbose("lineKeyMap : " + prepareLineKeyMap);
    LOGGER.verbose("GCRecordConfirmPickAction.prepareLineKeyMap--end");
    LOGGER.endTimer("GCRecordConfirmPickAction.prepareLineKeyMap");
    return prepareLineKeyMap;
  }

  /**
   * This method is used to record UserID.
   *
   * @param env
   * @param userID
   * @param managerID
   * @param orderHeaderKey
   * @param shipmentKey
   * @param action
   */
  private void recordUserID(YFSEnvironment env, String userID, String managerID, String orderHeaderKey,
      String shipmentKey, String action) {

    LOGGER.beginTimer("GCRecordConfirmPickAction.recordUserID");
    LOGGER.verbose("GCRecordConfirmPickAction.recordUserID--Begin");

    YFCDocument gcShpmntActvIndoc = YFCDocument.createDocument(GCConstants.GC_SHIPMENT_ACTICITY);
    YFCElement gcShpmntActivity = gcShpmntActvIndoc.getDocumentElement();
    gcShpmntActivity.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
    gcShpmntActivity.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
    gcShpmntActivity.setAttribute(GCConstants.AUDIT_RSNCODE, "CONFIRM_PICK_CONFIRM");
    gcShpmntActivity.setAttribute(GCConstants.USER_ACTION, "Confirm Pick");

    if (YFCUtils.equals(GCConstants.PARTIAL_PICK, action)) {
      gcShpmntActivity.setAttribute(GCConstants.USER_ID, managerID);
      gcShpmntActivity.setAttribute(GCConstants.MANAGER_ID, managerID);
    } else {
      gcShpmntActivity.setAttribute(GCConstants.USER_ID, userID);
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input to update DB :" + gcShpmntActvIndoc.toString());
    }
    GCCommonUtil.invokeService(env, GCConstants.GC_INSERT_SHPMNT_ACTIVITY, gcShpmntActvIndoc.getDocument());
    LOGGER.verbose("GCRecordConfirmPickAction.recordUserID--end");
    LOGGER.endTimer("GCRecordConfirmPickAction.recordUserID");
  }

  /**
   * This method is used to prepare ChangeOrder Input doc after recalculating commissions.
   *
   * @param env
   * @param lengthLineComm
   * @param orderLineKey
   * @param eleOdrLineCommLstIndoc
   * @param shipNode
   * @param nlOdrLineComm
   * @param lineKeyMap
   * @return
   */
  private boolean prepareChangeOrderInput(int lengthLineComm, String orderLineKey,
      YFCElement eleOdrLineCommLstIndoc, String shipNode, YFCNodeList<YFCElement> nlOdrLineComm,
      Map<String, YFCElement> lineKeyMap) {

    LOGGER.beginTimer("GCRecordConfirmPickAction.prepareChangeOrderInput");
    LOGGER.verbose("GCRecordConfirmPickAction.prepareChangeOrderInput--Begin");

    LOGGER.verbose("lengthLineComm : " + lengthLineComm);
    if (lengthLineComm == 0) {
      LOGGER.verbose("lengthLineComm is zero");
      // calling getGCOdrLineCommListService to fetch aquisition commission list for the
      // orderLineKey

      final YFCElement eleShipmentLine = lineKeyMap.get(orderLineKey);
      final YFCElement eleSalesPersonList = eleShipmentLine.getChildElement(GCConstants.GC_SALES_PRSN_LIST);
      YFCNodeList<YFCElement> nlOdrLineAqComm = null;
      int lengthAqComm = 0;
      if(!YFCCommon.isVoid(eleSalesPersonList)){
        nlOdrLineAqComm = eleSalesPersonList.getElementsByTagName(GCConstants.GC_SALES_PRSN);
        lengthAqComm = nlOdrLineAqComm.getLength();
      }

      LOGGER.verbose("lengthAqComm : " + lengthAqComm);

      if (lengthAqComm == 0) {
        stampAttributes(eleOdrLineCommLstIndoc, orderLineKey, shipNode, shipNode, GCConstants.SALES_COMMISSION, "100",
            GCConstants.BLANK);
        return true;
      } else {
        calculateAcquisitionCommission(eleOdrLineCommLstIndoc, orderLineKey, lengthAqComm, nlOdrLineAqComm);
        stampAttributes(eleOdrLineCommLstIndoc, orderLineKey, shipNode, shipNode, GCConstants.SALES_COMMISSION, "50",
            GCConstants.BLANK);
        return true;
      }
    } else {
      LOGGER.verbose("lengthLineComm not zero");
      // calling getGCOdrLineCommListService to fetch aquisition commission list for the
      // orderLineKey

      final YFCElement eleShipmentLine = lineKeyMap.get(orderLineKey);
      final YFCElement eleSalesPersonList = eleShipmentLine.getChildElement(GCConstants.GC_SALES_PRSN_LIST);
      YFCNodeList<YFCElement> nlOdrLineAqComm = null;
      int lengthAqComm = 0;
      if(!YFCCommon.isVoid(eleSalesPersonList)){
        nlOdrLineAqComm = eleSalesPersonList.getElementsByTagName(GCConstants.GC_SALES_PRSN);
        lengthAqComm = nlOdrLineAqComm.getLength();
      }
      if (lengthAqComm == 0) {
        return false;
      } else {
        calculateAcquisitionCommission(eleOdrLineCommLstIndoc, orderLineKey, lengthAqComm, nlOdrLineAqComm);
        for (int k = 0; k < lengthLineComm; k++) {
          final YFCElement eleOdrLineComm = nlOdrLineComm.item(k);
          BigDecimal allocPercent = new BigDecimal(eleOdrLineComm.getAttribute(GCConstants.ALLOCATION_PERCENTAGE));
          BigDecimal fifty = new BigDecimal("50.00");
          BigDecimal hundred = new BigDecimal("100.00");
          BigDecimal newAllocPercent = allocPercent.multiply(fifty).divide(hundred);
          stampAttributes(eleOdrLineCommLstIndoc, orderLineKey, eleOdrLineComm.getAttribute(GCConstants.USER_NAM),
              eleOdrLineComm.getAttribute(GCConstants.USER_ID), GCConstants.SALES_COMMISSION,
              newAllocPercent.toString(), eleOdrLineComm.getAttribute(GCConstants.ORDER_LINE_COMMISSION_KEY));
        }
        LOGGER.verbose("GCRecordConfirmPickAction.prepareChangeOrderInput--end");
        LOGGER.endTimer("GCRecordConfirmPickAction.prepareChangeOrderInput");
        return true;
      }
    }
  }

  /**
   * This method is used to calculate acquisition commission.
   *
   * @param eleOdrLineCommIndoc
   * @param orderLineKey
   * @param lengthAqComm
   * @param nlOdrLineAqComm
   */
  private void calculateAcquisitionCommission(YFCElement eleOdrLineCommLstIndoc, String orderLineKey, int lengthAqComm,
      YFCNodeList<YFCElement> nlOdrLineAqComm) {

    LOGGER.beginTimer("GCRecordConfirmPickAction.calculateAcquisitionCommission");
    LOGGER.verbose("GCRecordConfirmPickAction.calculateAcquisitionCommission--Begin");

    BigDecimal fifty = new BigDecimal("50.00");
    BigDecimal bgLengthAqComm = new BigDecimal(String.valueOf(lengthAqComm));
    BigDecimal amt = fifty.divide(bgLengthAqComm, 1);
    int tempLength = lengthAqComm;
    BigDecimal totalAmt = new BigDecimal(GCConstants.ZERO_AMOUNT);
    while (tempLength > 0) {
      totalAmt = totalAmt.add(amt);
      tempLength--;
    }
    BigDecimal diff = fifty.subtract(totalAmt);
    for (int j = 0; j < lengthAqComm; j++) {
      final YFCElement eleOdrLineAqComm = nlOdrLineAqComm.item(j);
      String userName = eleOdrLineAqComm.getAttribute(GCConstants.SALES_PERSON_ID);
      String userID = eleOdrLineAqComm.getAttribute(GCConstants.SALES_PERSON_ID);
      if (j == 0) {
        stampAttributes(eleOdrLineCommLstIndoc, orderLineKey, userName, userID, "Acquisition Commission", amt.add(diff)
            .toString(), GCConstants.BLANK);
      } else {
        stampAttributes(eleOdrLineCommLstIndoc, orderLineKey, userName, userID, "Acquisition Commission",
            amt.toString(), GCConstants.BLANK);
      }
    }
    LOGGER.verbose("GCRecordConfirmPickAction.calculateAcquisitionCommission--end");
    LOGGER.endTimer("GCRecordConfirmPickAction.calculateAcquisitionCommission");
  }

  /**
   * This method is used to stamp commissions information in the OrderLine
   *
   * @param eleOdrLineCommIndoc
   * @param orderLineKey
   * @param userName
   * @param userID
   * @param commissionType
   * @param allocationPercentage
   * @param odrLineCommKey
   */
  private void stampAttributes(YFCElement eleOdrLineCommLstIndoc, String orderLineKey, String userName, String userID,
      String commissionType, String allocationPercentage, String odrLineCommKey) {

    LOGGER.beginTimer("GCRecordConfirmPickAction.stampAttributes");
    LOGGER.verbose("GCRecordConfirmPickAction.stampAttributes--Begin");

    final YFCElement eleOdrLineCommIndoc = eleOdrLineCommLstIndoc.createChild(GCXmlLiterals.GC_ODR_LINE_COMM);
    eleOdrLineCommIndoc.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey);
    eleOdrLineCommIndoc.setAttribute(GCConstants.USER_NAME, userName);
    eleOdrLineCommIndoc.setAttribute(GCConstants.USER_ID, userID);
    eleOdrLineCommIndoc.setAttribute(GCConstants.COMMISSION_TYPE, commissionType);
    eleOdrLineCommIndoc.setAttribute(GCConstants.IS_ORDER_COMMISSION_OVERRIDEN, GCConstants.FLAG_Y);
    eleOdrLineCommIndoc.setAttribute(GCConstants.ALLOCATION_PERCENTAGE, allocationPercentage);

    if (!YFCCommon.isVoid(odrLineCommKey)) {
      eleOdrLineCommIndoc.setAttribute(GCConstants.ORDER_LINE_COMMISSION_KEY, odrLineCommKey);
    }
    LOGGER.verbose("GCRecordConfirmPickAction.stampAttributes--end");
    LOGGER.endTimer("GCRecordConfirmPickAction.stampAttributes");
  }

  /**
   * This method is used to call getOrderLineList API
   *
   * @param env
   * @param orderHeaderKey
   * @return
   */
  private YFCDocument callGetOrderLineListApi(YFSEnvironment env, String orderHeaderKey) {

    LOGGER.beginTimer("GCRecordConfirmPickAction.callGetOrderLineListApi");
    LOGGER.verbose("GCRecordConfirmPickAction.callGetOrderLineListApi--Begin");

    final YFCDocument odrLineListIndoc = YFCDocument.createDocument(GCConstants.ORDER_LINE);
    odrLineListIndoc.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
    YFCDocument tempdoc = YFCDocument.getDocumentFor(GCConstants.GET_ODR_LINE_LIST_TEMP);
    YFCDocument getOdrLineListOutdoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LINE_LIST, odrLineListIndoc, tempdoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getOdrLineListOutdoc : " + getOdrLineListOutdoc.toString());
    }
    LOGGER.verbose("GCRecordConfirmPickAction.callGetOrderLineListApi--end");
    LOGGER.endTimer("GCRecordConfirmPickAction.callGetOrderLineListApi");
    return getOdrLineListOutdoc;
  }

}
