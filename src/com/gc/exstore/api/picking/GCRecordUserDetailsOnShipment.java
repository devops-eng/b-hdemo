package com.gc.exstore.api.picking;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class is used to record user activity for dependent lines whose shipment is different from
 * its parent.
 *
 * @author shanil.sharma
 *
 */
public class GCRecordUserDetailsOnShipment implements YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCRecordUserDetailsOnShipment.class);
  @Override
  public void setProperties(Properties arg0) {

  }

  public Document recordUserDetails(YFSEnvironment env, Document inDoc) {

    LOGGER.beginTimer("GCRecordUserDetailsOnShipment.recordUserDetails");
    LOGGER.verbose("GCRecordUserDetailsOnShipment.recordUserDetails--Begin");

    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input to update DB :" + inputDoc.toString());
    }
    YFCDocument getShipListInputDoc1 = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement eleInShipment1 = getShipListInputDoc1.getDocumentElement();
    eleInShipment1.setAttribute(GCConstants.SHIPMENT_KEY,
        inputDoc.getDocumentElement().getAttribute(GCConstants.SHIPMENT_KEY));
    YFCDocument tempdoc1 = YFCDocument.getDocumentFor(GCConstants.GET_SHIPMENT_LIST_TEMPLATE);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input getShipListInputDoc :" + getShipListInputDoc1.toString());
    }

    YFCDocument getShipDtlOutdoc1 =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, getShipListInputDoc1, tempdoc1);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input getShipDtlOutdoc1 :" + getShipDtlOutdoc1.toString());
    }

    YFCElement eleRoot = getShipDtlOutdoc1.getDocumentElement();
    YFCElement eleShipment = eleRoot.getChildElement(GCConstants.SHIPMENT);
    YFCElement eleShipLines = eleShipment.getChildElement(GCConstants.SHIPMENT_LINES);
    String currentOrderHeaderKey = null;
    String currentShipmentKey = eleShipment.getAttribute(GCConstants.SHIPMENT_KEY);
    YFCNodeList<YFCElement> nlShipLine = eleShipLines.getElementsByTagName(GCConstants.SHIPMENT_LINE);
    boolean isStandaloneWarranty = true;
    List<String> dependentKeyList = new ArrayList<String>();
    for (YFCElement eleShipLine : nlShipLine) {
      YFCElement eleOrderLine = eleShipLine.getChildElement(GCConstants.ORDER_LINE);
      currentOrderHeaderKey = eleShipLine.getAttribute(GCConstants.ORDER_HEADER_KEY);
      String dependentOnLineKey = eleOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);

      if (YFCCommon.isVoid(dependentOnLineKey)) {
        isStandaloneWarranty = false;
        break;
      } else if (!dependentKeyList.contains(dependentOnLineKey)) {
        dependentKeyList.add(dependentOnLineKey);
      }
    }

    LOGGER.verbose("dependentKeyList :" + dependentKeyList);

    List<String> shipmentKeyList = new ArrayList<String>();
    LOGGER.verbose("isStandaloneWarranty :" + isStandaloneWarranty);
    if (isStandaloneWarranty) {
      for (String orderLineKey : dependentKeyList) {

        YFCDocument getShipListInputDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
        YFCElement eleInShipment = getShipListInputDoc.getDocumentElement();
        YFCElement eleInShipLines = eleInShipment.createChild(GCConstants.SHIPMENT_LINES);
        YFCElement eleInShipLine = eleInShipLines.createChild(GCConstants.SHIPMENT_LINE);
        eleInShipLine.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey);
        YFCDocument tempdoc = YFCDocument.getDocumentFor(GCConstants.RECORD_GET_SHIP_LIST_TEMPLATE);

        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("Input getShipListInputDoc :" + getShipListInputDoc.toString());
        }

        YFCDocument getShipDtlOutdoc =
            GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, getShipListInputDoc, tempdoc);

        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("Input getShipDtlOutdoc :" + getShipDtlOutdoc.toString());
        }

        YFCElement eleOutShipments = getShipDtlOutdoc.getDocumentElement();
        YFCElement eleOutShipment = eleOutShipments.getChildElement(GCConstants.SHIPMENT);
        String shipmentKey = eleOutShipment.getAttribute(GCConstants.SHIPMENT_KEY);
        if (!shipmentKeyList.contains(shipmentKey)) {
          shipmentKeyList.add(shipmentKey);
          recordUserActivity(env, shipmentKey, currentShipmentKey, currentOrderHeaderKey);
        }
      }
      LOGGER.verbose("shipmentKeyList :" + shipmentKeyList);
    }
    LOGGER.verbose("GCRecordUserDetailsOnShipment.recordUserDetails--End");
    LOGGER.endTimer("GCRecordUserDetailsOnShipment.recordUserDetails");
    return inDoc;

  }

  /**
   * This method is used to record User Activity for dependent line.
   *
   * @param env
   * @param shipmentKey
   * @param currentOrderHeaderKey
   * @param currentShipmentKey
   */
  private void recordUserActivity(YFSEnvironment env, String shipmentKey, String currentShipmentKey,
      String currentOrderHeaderKey) {

    LOGGER.beginTimer("GCRecordUserDetailsOnShipment.recordUserActivity");
    LOGGER.verbose("GCRecordUserDetailsOnShipment.recordUserActivity--Begin");
    YFCDocument userActivityInDoc = YFCDocument.createDocument(GCConstants.GC_USER_ACTIVITY);
    userActivityInDoc.getDocumentElement().setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
    YFCDocument getUsrActivityOutdoc =
        GCCommonUtil.invokeService(env, GCConstants.GC_FETCH_USER_ACTIVITY, userActivityInDoc);
    YFCElement eleGCUserActivityList = getUsrActivityOutdoc.getDocumentElement();
    YFCNodeList<YFCElement> nlUserActivity = eleGCUserActivityList.getElementsByTagName(GCConstants.GC_USER_ACTIVITY);
    YFCDocument gcShpmntActvIndoc = null;
    for(YFCElement eleUserActivity : nlUserActivity){
      String action = eleUserActivity.getAttribute(GCConstants.USER_ACTION);
      if (YFCCommon.equals("Confirm Pick", action)
          && (YFCCommon.equals(shipmentKey, eleUserActivity.getAttribute(GCConstants.SHIPMENT_KEY)))) {

        gcShpmntActvIndoc = YFCDocument.createDocument(GCConstants.GC_SHIPMENT_ACTICITY);
        YFCElement gcShpmntActivity = gcShpmntActvIndoc.getDocumentElement();
        gcShpmntActivity.setAttribute(GCConstants.SHIPMENT_KEY, currentShipmentKey);
        gcShpmntActivity.setAttribute(GCConstants.ORDER_HEADER_KEY, currentOrderHeaderKey);
        gcShpmntActivity.setAttribute(GCConstants.AUDIT_RSNCODE, "CONFIRM_PICK_CONFIRM");
        gcShpmntActivity.setAttribute(GCConstants.USER_ACTION, "Confirm Pick");
        gcShpmntActivity.setAttribute(GCConstants.USER_ID, eleUserActivity.getAttribute(GCConstants.USER_ID));
        gcShpmntActivity.setAttribute(GCConstants.MANAGER_ID, eleUserActivity.getAttribute(GCConstants.MANAGER_ID));

        break;
      }
    }

    if (!YFCCommon.isVoid(gcShpmntActvIndoc)) {
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("Input to Fetch from DB :" + gcShpmntActvIndoc.toString());
      }

      GCCommonUtil.invokeService(env, GCConstants.GC_INSERT_SHPMNT_ACTIVITY, gcShpmntActvIndoc.getDocument());
    }
    LOGGER.verbose("GCRecordUserDetailsOnShipment.recordUserActivity--End");
    LOGGER.endTimer("GCRecordUserDetailsOnShipment.recordUserActivity");
  }
}
