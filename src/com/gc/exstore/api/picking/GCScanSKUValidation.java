/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: The class provides the Order Details to be displayed on the UI
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0               06/02/2014        Rangarajan, Shwetha      The class provides the necessary Order Details
                                                                           to be published on the screen
             1.1               07/06/2017        Shashank, Kumar          Changes added (for GCSTORE-7405) to throw an exception on SKU validation if the the parent
              															  associated with the free gift item has already been cancelled                           
 *#################################################################################################################################################################
 */
package com.gc.exstore.api.picking;

import java.util.List;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCWebServiceUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * The class makes a web-service call to validate the scanned SKU.
 *
 * @author <a href="mailto:shwetha.rangarajan@expicient.com">Shwetha</a>
 */
public class GCScanSKUValidation implements YIFCustomApi {
	
	private String sParentItemID = null;
	private String sGiftItemID = null;

  //initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCScanSKUValidation.class.getName());
  String callWebSerivce;

  /**
   *
   * @param yfsEnv
   * @param inputDoc
   * @return
   */
  public Document validateSKU(YFSEnvironment yfsEnv, Document inputDoc) {


    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    LOGGER.verbose("Input document :" + inDoc.toString());
    YFCElement rootEle = inDoc.getDocumentElement();

    YFCDocument soapRequest = YFCDocument.getDocumentFor(GCConstants.SCAN_SKU_REQUEST);
    YFCElement envelopeEle = soapRequest.getDocumentElement();
    YFCElement bodyEle = envelopeEle.getChildElement(GCConstants.SOAP_BODY);
    YFCElement validateSKUEle = bodyEle.getChildElement(GCConstants.VALIDATE_SKU_ELE);
    YFCElement xmlDocInELe = validateSKUEle.getChildElement(GCConstants.XML_DOC_IN_ELE);
    YFCElement productsEle = xmlDocInELe.getChildElement(GCConstants.PRODUCTS);
    productsEle.setAttribute(GCConstants.XMLNS, GCConstants.BLANK_STRING);
    
    //Added for GCSTORE-7405 : START
    popUpIfParentOfGiftHasBeenCancelled(yfsEnv, inDoc);
    //GCSTORE-7405 : END
    
    YFCElement shipmentLinesEle = rootEle.getChildElement(GCConstants.SHIPMENT_LINES);

    if (!YFCCommon.isVoid(shipmentLinesEle) && shipmentLinesEle.hasChildNodes()) {
      YFCIterable<YFCElement> shipmntLineLst = shipmentLinesEle.getChildren();
      for (YFCElement shpmntLineEle : shipmntLineLst) {

        YFCElement productEle = productsEle.createChild(GCConstants.PRODUCT);

        YFCElement typeEle = productEle.createChild(GCConstants.TYPE);
        typeEle.setNodeValue(shpmntLineEle.getAttribute(GCConstants.PRODUCT_CLASS));

        YFCElement lineNumberEle = productEle.createChild(GCConstants.LINE_NUMBER);
        lineNumberEle.setNodeValue(shpmntLineEle.getAttribute(GCConstants.SHIPMENT_LINE_NO));

        YFCElement posLocationEle = productEle.createChild(GCConstants.POS_LOCATION);
        posLocationEle.setNodeValue(rootEle.getAttribute(GCConstants.SHIP_NODE));

        YFCElement skuEle = productEle.createChild(GCConstants.SKU);
        if(!YFCCommon.isVoid(shpmntLineEle.getAttribute(GCConstants.POS_ITEM_ID))){
          skuEle.setNodeValue(shpmntLineEle.getAttribute(GCConstants.POS_ITEM_ID));
        }


        YFCElement invInputEle = productEle.createChild(GCConstants.INV_INPUT);
        invInputEle.setNodeValue(shpmntLineEle.getAttribute(GCConstants.INV_INPUT));
        String parentQty = shpmntLineEle.getAttribute(GCConstants.QTY_PICKED);
        double dblParentQty = 0.0;
        if (!YFCCommon.isVoid(parentQty)) {
          dblParentQty = Double.parseDouble(parentQty);
        }
        YFCElement qtyEle = productEle.createChild(GCConstants.QUANTITY);
        qtyEle.setNodeValue(shpmntLineEle.getAttribute(GCConstants.QTY_PICKED));

        YFCElement warrantyComponents = shpmntLineEle.getChildElement(GCConstants.WARRANTY_COMPONENTS);
        if (!YFCCommon.isVoid(warrantyComponents) && warrantyComponents.hasChildNodes()) {
          YFCIterable<YFCElement> warrantyCompList = warrantyComponents.getChildren();
          for (YFCElement shipmentLine : warrantyCompList) {
            YFCElement warrantyProduct = productsEle.createChild(GCConstants.PRODUCT);

            YFCElement warrantyType = warrantyProduct.createChild(GCConstants.TYPE);
            warrantyType.setNodeValue(shipmentLine.getAttribute(GCConstants.PRODUCT_CLASS));

            YFCElement warrantyLineNumber = warrantyProduct.createChild(GCConstants.LINE_NUMBER);
            warrantyLineNumber.setNodeValue(shipmentLine.getAttribute(GCConstants.SHIPMENT_LINE_NO));

            YFCElement warrantyPOSLocation = warrantyProduct.createChild(GCConstants.POS_LOCATION);
            warrantyPOSLocation.setNodeValue(rootEle.getAttribute(GCConstants.SHIP_NODE));

            YFCElement warrantySKU = warrantyProduct.createChild(GCConstants.SKU);
            if(!YFCCommon.isVoid(shipmentLine.getAttribute(GCConstants.POS_ITEM_ID))){
              warrantySKU.setNodeValue(shipmentLine.getAttribute(GCConstants.POS_ITEM_ID));
            }

            YFCElement warrantyInvInput = warrantyProduct.createChild(GCConstants.INV_INPUT);
            warrantyInvInput.setNodeValue(shipmentLine.getAttribute(GCConstants.INV_INPUT));

            String strWarrantyQty = shipmentLine.getAttribute(GCConstants.QUANTITY);
            double dblWarrantyQty = 0.0;
            if (!YFCCommon.isVoid(strWarrantyQty)) {
              dblWarrantyQty = Double.parseDouble(strWarrantyQty);
            }
            if (dblWarrantyQty == dblParentQty || dblParentQty > dblWarrantyQty) {
              YFCElement warrantyQty = warrantyProduct.createChild(GCConstants.QUANTITY);
              warrantyQty.setNodeValue(shipmentLine.getAttribute(GCConstants.QUANTITY));
            } else if (dblParentQty < dblWarrantyQty) {
              YFCElement warrantyQty = warrantyProduct.createChild(GCConstants.QUANTITY);
              warrantyQty.setNodeValue(parentQty);
            }
          }
        }

      }
    }

    LOGGER.verbose("SOAP Request formed :" + soapRequest.toString());
    YFCDocument soapResponse;
    Document scanSKUResponse = null ;
    YFCDocument outDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);

    if (YFCUtils.equals(callWebSerivce, GCConstants.YES)) {

      String sSoapAction = YFSSystem.getProperty(GCConstants.POS_SCAN_SKU_SOAP_ACTION);
      LOGGER.verbose("SOAP Action :" +sSoapAction);
      String sSoapUrl = YFSSystem.getProperty(GCConstants.POS_SCAN_SKU_WEB_SERVICE_URL);
      LOGGER.verbose("SOAP URL :" + sSoapUrl);
      GCWebServiceUtil obj = new GCWebServiceUtil();

      try{

        scanSKUResponse = obj.invokeSOAPWebService(soapRequest.getDocument(), sSoapUrl, sSoapAction);
      } catch (Exception e) {
        LOGGER.error("Got exception in authenticate method", e);
        if (!YFCCommon.isVoid(scanSKUResponse)) {
          YFCException yfce = new YFCException(GCErrorConstants.EXTN_POS_SCAN_FAILED);
          throw yfce;
        } else {
          YFCException yfce = new YFCException(GCErrorConstants.EXTN_POS_CONNECT_TIMEOUT);
          throw yfce;
        }

      }
      if(!YFCCommon.isVoid(scanSKUResponse)){

        soapResponse = YFCDocument.getDocumentFor(scanSKUResponse);
        LOGGER.verbose("SOAP Response obtained :" + soapResponse.toString());
        YFCElement sEnvelopeEle = soapResponse.getDocumentElement();
        YFCElement sBodyEle = sEnvelopeEle.getChildElement(GCConstants.SBODY);
        YFCElement validateSKUResp = sBodyEle.getChildElement(GCConstants.VLDT_SKU_RESPONSE);

        if (!YFCCommon.isVoid(validateSKUResp)) {
          validateSKUResponse(shipmentLinesEle, outDoc, validateSKUResp);

        } else {

          YFCException yfce = new YFCException(GCErrorConstants.EXTN_POS_SCAN_FAILED);
          throw yfce;

        }
      }
    }

    LOGGER.verbose("Output document to UI :" + outDoc);
    return outDoc.getDocument();

  }

  //Added for GCSTORE-7405 : START
  /**
   * Method to display a pop-up if parent line associated with Free Gift Item has been cancelled already
   * @param yfsEnv
   * @param inDoc
   */
  private void popUpIfParentOfGiftHasBeenCancelled(YFSEnvironment yfsEnv,
		  YFCDocument inDoc) {

	  LOGGER.info("Entering inside GCScanSKUValidation.popUpIfParentOfGiftHasBeenCancelled()");
	  List<Element> nlShipmentLine = GCXMLUtil.getElementListByXpath(inDoc.getDocument(), "Shipment/ShipmentLines/ShipmentLine");
	  Element shipmentLineEle  = nlShipmentLine.get(0);

	  if(!YFCCommon.isVoid(shipmentLineEle)){
		  
		  //Invoke getOrderLineList api
		  YFCDocument inDocForGetOrderLineList = YFCDocument.createDocument(GCConstants.ORDER_LINE);
		  YFCElement eleOL = inDocForGetOrderLineList.getDocumentElement();
		  String sOLKey = shipmentLineEle.getAttribute(GCConstants.ORDER_LINE_KEY);
		  eleOL.setAttribute(GCConstants.ORDER_LINE_KEY, sOLKey);
		  YFCDocument getOrderLineListTemp = prepareGetOrderLineListTemplate();
		  if(!YFCCommon.isVoid(sOLKey)){
			  YFCDocument outDocGetOrderLineList = GCCommonUtil.invokeAPI(yfsEnv, GCConstants.GET_ORDER_LINE_LIST, inDocForGetOrderLineList, getOrderLineListTemp);
			  LOGGER.verbose("getOrderLineList output Doc :" + outDocGetOrderLineList.toString());

			  for(Element eleShipmentLine : nlShipmentLine){

				  String sOrderLineKey = eleShipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY);
				  String sDependentOnLineKeyOnGift = fetchDependentLineKeyOnGift(outDocGetOrderLineList, sOrderLineKey);
				  if(!YFCCommon.isVoid(sDependentOnLineKeyOnGift)){

					  //Fetching the status of the parent OrderLine
					  String sParentLineStatus = GCXMLUtil.getAttributeFromXPath(outDocGetOrderLineList.getDocument(),
							  "OrderLineList/OrderLine/Order/OrderLines/OrderLine[@OrderLineKey='" + sDependentOnLineKeyOnGift + "']/@MinLineStatus");
					  if(!YFCCommon.isVoid(sDependentOnLineKeyOnGift) && YFCUtils.equals(sParentLineStatus, GCConstants.CANCELLED_ORDER_STATUS_CODE)){
						  //throw exception
						  YFCException yfce = new YFCException();
						  yfce.setAttribute(GCConstants.ERROR_CODE, "GIFT_LINE_CANCEL");
						  yfce.setAttribute(GCConstants.ERROR_DESCRIPTION, "Parent Item: " + sParentItemID + " associated with the Gift item: " + 
						  sGiftItemID + " has already been cancelled. Please cancel the Gift item: "+ sGiftItemID + " manually.");
						  throw yfce;
					  }
				  }
			  }  
		  }
	  }
	  LOGGER.info("Exiting from GCScanSKUValidation.popUpIfParentOfGiftHasBeenCancelled()");
  }

  /**
   * Method to fetch the DependentLineKey from Gift line
   * @param outDocGetOrderLineList
   * @param sOrderLineKey
   * @return
   */
  private String fetchDependentLineKeyOnGift(YFCDocument outDocGetOrderLineList,
		  String sOrderLineKey) {

	  LOGGER.info("Entering inside GCScanSKUValidation.fetchDependentLineKeyOnGift()");
	  String sDependentOnLineKeyOfGift = null;

	  Element eleGiftOrderLine = GCXMLUtil.getElementByXPath(outDocGetOrderLineList.getDocument(), 
			  "OrderLineList/OrderLine[@OrderLineKey='" + sOrderLineKey + "' and Extn[@ExtnIsFreeGiftItem='Y']]");

	  if(!YFCCommon.isVoid(eleGiftOrderLine)){	  
		  Element eleGiftOL = GCXMLUtil.getElementByXPath(outDocGetOrderLineList.getDocument(), 
				  "OrderLineList/OrderLine/Order/OrderLines/OrderLine[@OrderLineKey='" + sOrderLineKey + "']");

		  sDependentOnLineKeyOfGift = eleGiftOL.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
		  Element eleGiftItemDetails = (Element)eleGiftOL.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
		  sGiftItemID = eleGiftItemDetails.getAttribute(GCConstants.ITEM_ID);	

		  if(!YFCCommon.isVoid(sDependentOnLineKeyOfGift)){
			  Element eleParentLine = GCXMLUtil.getElementByXPath(outDocGetOrderLineList.getDocument(), 
					  "OrderLineList/OrderLine/Order/OrderLines/OrderLine[@OrderLineKey='" + sDependentOnLineKeyOfGift + "']");

			  if(!YFCCommon.isVoid(eleParentLine)){
				  Element eleParentItemDetails = (Element)eleParentLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
				  sParentItemID = eleParentItemDetails.getAttribute(GCConstants.ITEM_ID);
			  }
		  }
	  }
	  LOGGER.info("Exiting from GCScanSKUValidation.fetchDependentLineKeyOnGift()");
	  LOGGER.info("DependentOnLineKey of Gift item: " + sDependentOnLineKeyOfGift);
	  return sDependentOnLineKeyOfGift;
  }

  /**
   * Method to form output template for getOrderLineList
   * @return
   */
  private YFCDocument prepareGetOrderLineListTemplate() {

	  YFCDocument getOrderLineListTemplateDoc = YFCDocument.createDocument(GCConstants.ORDER_LINE_LIST);
	  YFCElement eleOrderLineList = getOrderLineListTemplateDoc.getDocumentElement();
	  YFCElement eleOrdrLine = eleOrderLineList.createChild(GCConstants.ORDER_LINE);
	  YFCElement eleExtn = eleOrdrLine.createChild(GCConstants.EXTN);
	  eleExtn.setAttribute(GCConstants.EXTN_IS_FREE_GIFT_ITEM, "");
	  YFCElement eleOrdr = eleOrdrLine.createChild(GCConstants.ORDER);
	  eleOrdr.setAttribute(GCConstants.ORDER_HEADER_KEY, "");
	  YFCElement eleOrderLines = eleOrdr.createChild(GCConstants.ORDER_LINES);
	  YFCElement eleOrderLine = eleOrderLines.createChild(GCConstants.ORDER_LINE);
	  eleOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, "");
	  eleOrderLine.setAttribute(GCConstants.MAX_LINE_STATUS, "");
	  eleOrderLine.setAttribute(GCConstants.MIN_LINE_STATUS, "");
	  eleOrderLine.setAttribute(GCConstants.PRIME_LINE_NO, "");
	  eleOrderLine.setAttribute(GCConstants.SUB_LINE_NO, "");
	  eleOrderLine.setAttribute(GCConstants.SHIP_NODE, "");
	  eleOrderLine.setAttribute(GCConstants.DEPENDENT_ON_LINE_KEY, "");
	  YFCElement eleItemDetails = eleOrderLine.createChild(GCConstants.ITEM_DETAILS);
	  eleItemDetails.setAttribute(GCConstants.ITEM_ID, "");

	  return getOrderLineListTemplateDoc;
  }

  //GCSTORE-7405 : END

  public void validateSKUResponse(YFCElement shipmentLinesEle, YFCDocument outDoc, YFCElement validateSKUResp) {

    YFCElement validateSKUResult = validateSKUResp.getChildElement(GCConstants.VLDT_SKU_RESULT);
    String response = validateSKUResult.getNodeValue();
    boolean isCustomError = false;
    String strErrorInfo = "";
    YFCDocument responseDoc = YFCDocument.getDocumentFor(response);
    LOGGER.verbose("Response Doc fetched :" +responseDoc.toString());
    YFCElement responseRootEle = responseDoc.getDocumentElement();
    YFCIterable<YFCElement> responsesPOSList = responseRootEle.getChildren();
    for (YFCElement responsesPOSEle : responsesPOSList) {
      YFCElement success = responsesPOSEle.getChildElement(GCConstants.SUCCESS);
      if (YFCUtils.equals(GCConstants.FALSE_STR, success.getNodeValue())) {
        isCustomError = true;
        YFCElement errorInfo = responsesPOSEle.getChildElement(GCConstants.ERROR_INFO);
        strErrorInfo = errorInfo.getNodeValue();
        break;
      }
    }
    if (isCustomError) {
      YFCException yfce = new YFCException(GCErrorConstants.EXTN_POS_SCAN_FAILED);
      yfce.setErrorDescription(GCConstants.POS_ERR_MSG + strErrorInfo);
      throw yfce;
    } else {

      YFCElement shpmntOutEle = outDoc.getDocumentElement();
      shpmntOutEle.setAttribute(GCConstants.IS_SUCCESS, GCConstants.YES);
      YFCElement shpmntLinesOutEle = shpmntOutEle.createChild(GCConstants.SHIPMENT_LINES);

      YFCIterable<YFCElement> shpmntLineList = shipmentLinesEle.getChildren();
      for (YFCElement shipmentInLine : shpmntLineList) {

        YFCElement shipmentLineOutEle = shpmntLinesOutEle.createChild(GCConstants.SHIPMENT_LINE);

        String shipmentLineNo = shipmentInLine.getAttribute(GCConstants.SHIPMENT_LINE_NO);
        YFCIterable<YFCElement> responsesList = responseRootEle.getChildren();
        for (YFCElement responsesEle : responsesList) {
          prepareScanResult(responseRootEle, shipmentInLine, shipmentLineOutEle, shipmentLineNo, responsesEle);

        }

        YFCElement warrantyCompInEle = shipmentInLine.getChildElement(GCConstants.WARRANTY_COMPONENTS);
        if (!YFCCommon.isVoid(warrantyCompInEle) && warrantyCompInEle.hasChildNodes()) {
          YFCIterable<YFCElement> warrantyCompInList = warrantyCompInEle.getChildren();
          YFCElement warrantyComponents = shipmentLineOutEle.createChild(GCConstants.WARRANTY_COMPONENTS);
          for (YFCElement warrantyInShipLine : warrantyCompInList) {
            YFCElement warrantyOutLine = warrantyComponents.createChild(GCConstants.SHIPMENT_LINE);
            String warrantyLineNo = warrantyInShipLine.getAttribute(GCConstants.SHIPMENT_LINE_NO);
            YFCIterable<YFCElement> responses = responseRootEle.getChildren();
            for (YFCElement responsesTag : responses) {
              prepareScanResult(responseRootEle, warrantyInShipLine, warrantyOutLine, warrantyLineNo, responsesTag);
            }
          }
        }
      }
    }
  }


  private void prepareScanResult(YFCElement responseRootEle, YFCElement shipmentInLine, YFCElement shipmentLineOutEle,
      String shipmentLineNo, YFCElement responsesEle) {

    YFCElement lineNoEle = responsesEle.getChildElement(GCConstants.LINE_NUMBER);
    String lineNo = lineNoEle.getNodeValue();
    if (YFCUtils.equals(shipmentLineNo, lineNo)) {
      shipmentLineOutEle.setAttribute(GCXmlLiterals.SHIPMENT_LINE_KEY,
          shipmentInLine.getAttribute(GCXmlLiterals.SHIPMENT_LINE_KEY));

      YFCElement skuNoEle = responsesEle.getChildElement(GCConstants.SKU_NO_ELE);
      String skuNumber = skuNoEle.getNodeValue();
      shipmentLineOutEle.setAttribute(GCConstants.SKU_NUM, skuNumber);

      YFCElement serialNoEle = responsesEle.getChildElement(GCConstants.SERIAL_NUMBER);
      String serialNumber = serialNoEle.getNodeValue();
      shipmentLineOutEle.setAttribute(GCConstants.SERIAL_NUMBER, serialNumber);

      // Check for Serial Items -- START
      YFCElement extnInEle = shipmentInLine.getChildElement(GCConstants.EXTN);
      String extnIsPlatinumItem = GCConstants.BLANK_STRING;
      if (!YFCCommon.isVoid(extnInEle)) {
        extnIsPlatinumItem = extnInEle.getAttribute("ExtnIsPlatinumItem");
        if (YFCUtils.equals(GCConstants.YES, extnIsPlatinumItem) && YFCCommon.isVoid(serialNumber)
            && YFCCommon.isVoid(shipmentLineOutEle.getAttribute(GCConstants.REQ_TAG_NO))) {
          YFCException yfce = new YFCException(GCErrorConstants.EXTN_SERIAL_NUM_NOT_RCVD);
          throw yfce;
        }
      }
      // Check for Serial Items -- END


      YFCElement salesPersons = responsesEle.getChildElement(GCConstants.SALES_PERSONS);
      if (!YFCCommon.isVoid(salesPersons) && salesPersons.hasChildNodes()) {
        YFCElement salesPersonsEle = shipmentLineOutEle.createChild(GCConstants.GC_SALES_PRSN_LIST);
        YFCIterable<YFCElement> salesPersonList = salesPersons.getChildren();
        for (YFCElement salesPerson : salesPersonList) {
          YFCElement salesPersonEle = salesPersonsEle.createChild(GCConstants.GC_SALES_PRSN);
          salesPersonEle.setAttribute(GCConstants.SALES_PERSON_ID, salesPerson.getNodeValue());
          salesPersonEle.setAttribute(GCConstants.COMMISSION_TYPE, GCConstants.ACQUISITION_COMMISSION);
        }
      }

      responseRootEle.removeChild(responsesEle);
    }
  }


  @Override
  public void setProperties(Properties arg0) {
    // Used to fetch property variables from customer_overrides.
    callWebSerivce = arg0.getProperty(GCConstants.IS_WEBSERVICE_CALL);


  }



}
