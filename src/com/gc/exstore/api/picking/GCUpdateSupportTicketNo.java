package com.gc.exstore.api.picking;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class is used to view, add, delete the support tickets.
 *
 * @author shanil.sharma
 *
 */
public class GCUpdateSupportTicketNo {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCUpdateSupportTicketNo.class.getName());

  /**
   * This method is used to update the support ticket information in the hang off table.
   *
   * @param env
   * @param indoc
   * @return
   */
  public Document updateSupportTicketNo(YFSEnvironment env, Document indoc) {

    LOGGER.beginTimer("GCUpdateSupportTicketNo.updateSupportTicketNo");
    LOGGER.verbose("GCUpdateSupportTicketNo.updateSupportTicketNo--Begin");
    final YFCDocument inputDoc = YFCDocument.getDocumentFor(indoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("inputDoc : " + inputDoc.toString());
    }

    final YFCElement eleRoot = inputDoc.getDocumentElement();
    final String action = eleRoot.getAttribute(GCConstants.ACTION);
    final String shipmentNo = eleRoot.getAttribute(GCConstants.SHIPMENT_NO);
    final String orderNo = eleRoot.getAttribute(GCConstants.ORDER_NO);
    final String orderHeaderKey = eleRoot.getAttribute(GCConstants.ORDER_HEADER_KEY);
    if (YFCUtils.equals(action, GCConstants.VIEW)) {
      if(!YFCCommon.isVoid(shipmentNo)){
        final YFCDocument supportTcktDoc = YFCDocument.createDocument(GCConstants.GC_SUPP_TCKT_LST);
        final YFCElement eleSupportTcktLstOutDoc = supportTcktDoc.getDocumentElement();

        final YFCElement eleGCSupportTicketNoList = getSupportTcktLst(env, orderNo);
        final YFCNodeList<YFCElement> nlGCSupportTicketNo =
            eleGCSupportTicketNoList.getElementsByTagName(GCConstants.GC_SUPP_TCKT);
        final int lengthTcktNo = nlGCSupportTicketNo.getLength();
        for (int i = 0; i < lengthTcktNo; i++) {
          final YFCElement eleGCSupportTicketNo = nlGCSupportTicketNo.item(i);
          if (YFCUtils.equals(shipmentNo, eleGCSupportTicketNo.getAttribute(GCConstants.SHIPMENT_NO))) {
            eleSupportTcktLstOutDoc.importNode(eleGCSupportTicketNo);
          }
        }

        if(lengthTcktNo == 0){
          return indoc;
        } else {
          final YFCElement eleSuppTcktDocResponse = supportTcktDoc.getDocumentElement();
          eleSuppTcktDocResponse.setAttribute(GCConstants.SHIPMENT_NO, shipmentNo);
          eleSuppTcktDocResponse.setAttribute(GCConstants.ORDER_NO, orderNo);
          eleSuppTcktDocResponse.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);

          if (LOGGER.isVerboseEnabled()) {
            LOGGER.verbose("supportTcktDoc : "+supportTcktDoc.toString());
          }
          return supportTcktDoc.getDocument();
        }
      } else if(!YFCCommon.isVoid(orderNo)){
        final YFCElement eleGCSupportTicketNoList = getSupportTcktLst(env, orderNo);

        final YFCNodeList<YFCElement> nlGCSupportTicketNo = eleGCSupportTicketNoList.getElementsByTagName(GCConstants.GC_SUPP_TCKT);
        final int lengthTckt = nlGCSupportTicketNo.getLength();

        if(lengthTckt == 0){
          return indoc;
        } else {
          final YFCDocument responseDoc = YFCDocument.getDocumentFor(eleGCSupportTicketNoList.toString());
          final YFCElement eleSuppTcktDocResponse = responseDoc.getDocumentElement();
          eleSuppTcktDocResponse.setAttribute(GCConstants.SHIPMENT_NO, shipmentNo);
          eleSuppTcktDocResponse.setAttribute(GCConstants.ORDER_NO, orderNo);
          eleSuppTcktDocResponse.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);

          return responseDoc.getDocument();
        }
      }
    } else if (YFCUtils.equals(action, GCConstants.ACTION_DELETE)) {

      if(!YFCCommon.isVoid(shipmentNo)){
        YFCDocument suppTcktLstDoc = YFCDocument.createDocument(GCConstants.GC_SUPP_TCKT);
        final YFCElement eleSuppTckt = suppTcktLstDoc.getDocumentElement();
        eleSuppTckt.setAttribute(GCConstants.SHIPMENT_NO, shipmentNo);
        final YFCNodeList<YFCElement> nlGCSupportTcktIndoc = eleRoot.getElementsByTagName(GCConstants.GC_SUPP_TCKT);
        final int lengthSuppTckt = nlGCSupportTcktIndoc.getLength();
        for (int i = 0; i < lengthSuppTckt; i++) {

          YFCElement eleGCSuppTcktDoc = nlGCSupportTcktIndoc.item(i);
          final YFCDocument supportTcktDelDoc = YFCDocument.createDocument(GCConstants.GC_SUPP_TCKT);
          final YFCElement eleSuuportTcktDel = supportTcktDelDoc.getDocumentElement();
          eleSuuportTcktDel.setAttribute(GCConstants.SUPPORT_TICKET_NO, eleGCSuppTcktDoc.getAttribute(GCConstants.SUPPORT_TICKET_NO));
          eleSuuportTcktDel.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
          // call delete service
          GCCommonUtil.invokeService(env, GCConstants.GC_SUPPORT_TICKET_DELETE, supportTcktDelDoc.getDocument());

        }
        // call list service
        final Document responseDoc =
            GCCommonUtil.invokeService(env, GCConstants.GC_SUPPORT_TICKET_LST, suppTcktLstDoc.getDocument());
        YFCDocument respDoc = YFCDocument.getDocumentFor(responseDoc);
        final YFCElement eleSuppTcktDocResponse = respDoc.getDocumentElement();
        eleSuppTcktDocResponse.setAttribute(GCConstants.SHIPMENT_NO, shipmentNo);
        eleSuppTcktDocResponse.setAttribute(GCConstants.ORDER_NO, orderNo);
        eleSuppTcktDocResponse.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);

        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("respDoc : "+respDoc.toString());
        }
        return respDoc.getDocument();
      } else if (YFCCommon.isVoid(shipmentNo) && (!YFCCommon.isVoid(orderNo))) {

        YFCDocument suppTcktLstDoc = YFCDocument.createDocument(GCConstants.GC_SUPP_TCKT);
        final YFCElement eleSuppTckt = suppTcktLstDoc.getDocumentElement();
        eleSuppTckt.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
        final YFCNodeList<YFCElement> nlGCSupportTcktIndoc = eleRoot.getElementsByTagName(GCConstants.GC_SUPP_TCKT);
        final int lengthSuppTckt = nlGCSupportTcktIndoc.getLength();
        for (int i = 0; i < lengthSuppTckt; i++) {

          YFCElement eleGCSuppTcktDoc = nlGCSupportTcktIndoc.item(i);
          final YFCDocument supportTcktDelDoc = YFCDocument.createDocument(GCConstants.GC_SUPP_TCKT);
          final YFCElement eleSuuportTcktDel = supportTcktDelDoc.getDocumentElement();
          eleSuuportTcktDel.setAttribute(GCConstants.SUPPORT_TICKET_NO, eleGCSuppTcktDoc.getAttribute(GCConstants.SUPPORT_TICKET_NO));
          eleSuuportTcktDel.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
          // call delete service
          GCCommonUtil.invokeService(env, GCConstants.GC_SUPPORT_TICKET_DELETE, supportTcktDelDoc.getDocument());

        }
        // call list service
        final Document responseDoc =
            GCCommonUtil.invokeService(env, GCConstants.GC_SUPPORT_TICKET_LST, suppTcktLstDoc.getDocument());
        YFCDocument respDoc = YFCDocument.getDocumentFor(responseDoc);
        final YFCElement eleSuppTcktDocResponse = respDoc.getDocumentElement();
        eleSuppTcktDocResponse.setAttribute(GCConstants.SHIPMENT_NO, shipmentNo);
        eleSuppTcktDocResponse.setAttribute(GCConstants.ORDER_NO, orderNo);
        eleSuppTcktDocResponse.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);

        return respDoc.getDocument();

      }
    } else if (YFCUtils.equals(action, GCConstants.ADD)) {

      YFCDocument responseDoc = manageAddAction(env, eleRoot, orderHeaderKey, shipmentNo, orderNo);

      return responseDoc.getDocument();
    }
    LOGGER.verbose("GCUpdateSupportTicketNo.updateSupportTicketNo--end");
    LOGGER.endTimer("GCUpdateSupportTicketNo.updateSupportTicketNo");
    return indoc;

  }

  private YFCDocument manageAddAction(YFSEnvironment env, YFCElement eleRoot, String orderHeaderKey, String shipmentNo, String orderNo) {

    LOGGER.beginTimer("GCUpdateSupportTicketNo.manageAddAction");
    LOGGER.verbose("GCUpdateSupportTicketNo.manageAddAction--Begin");
    final YFCDocument changeOrderDoc = YFCDocument.createDocument(GCConstants.ORDER);
    final YFCElement eleChangeOrder = changeOrderDoc.getDocumentElement();
    eleChangeOrder.setAttribute(GCConstants.ACTION, GCConstants.ACTION_MODIFY);
    eleChangeOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
    final YFCElement eleChangeOdrExtn = eleChangeOrder.createChild(GCConstants.EXTN);
    final YFCElement eleChgGCSupportTicketList = eleChangeOdrExtn.createChild(GCConstants.GC_SUPP_TCKT_LST);
    final YFCNodeList<YFCElement> nlGCSupportTcktIndoc = eleRoot.getElementsByTagName(GCConstants.GC_SUPP_TCKT);
    final int lengthSuppTckt = nlGCSupportTcktIndoc.getLength();
    for (int i = 0; i < lengthSuppTckt; i++) {
      final YFCElement eleGCSupportTcktIndoc = nlGCSupportTcktIndoc.item(i);
      final YFCElement eleChgGCSupportTicket = eleChgGCSupportTicketList.createChild(GCConstants.GC_SUPP_TCKT);
      eleChgGCSupportTicket.setAttribute(GCConstants.ORDER_HEADER_KEY,
          eleGCSupportTcktIndoc.getAttribute(GCConstants.ORDER_HEADER_KEY));
      eleChgGCSupportTicket.setAttribute(GCConstants.SHIPMENT_NO,
          eleGCSupportTcktIndoc.getAttribute(GCConstants.SHIPMENT_NO));
      eleChgGCSupportTicket.setAttribute(GCConstants.SUPPORT_TICKET_NO,
          eleGCSupportTcktIndoc.getAttribute(GCConstants.SUPPORT_TICKET_NO));
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("changeOrderDoc : " + changeOrderDoc.toString());
    }

    final YFCDocument getChgOdrTemp = YFCDocument.getDocumentFor(GCConstants.CHG_ODR_SUPP_TCKT_TEMPLATE);
    final YFCDocument getChgOdrOutDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, changeOrderDoc, getChgOdrTemp);
    final YFCElement eleChgOrder = getChgOdrOutDoc.getDocumentElement();
    final YFCElement eleChgOdrExtn = eleChgOrder.getChildElement(GCConstants.EXTN);
    final YFCElement eleSupportTcktLst = eleChgOdrExtn.getChildElement(GCConstants.GC_SUPP_TCKT_LST);

    final YFCDocument responseDoc = YFCDocument.getDocumentFor(eleSupportTcktLst.toString());

    final YFCElement eleSuppTcktDocResponse = responseDoc.getDocumentElement();
    eleSuppTcktDocResponse.setAttribute(GCConstants.SHIPMENT_NO, shipmentNo);
    eleSuppTcktDocResponse.setAttribute(GCConstants.ORDER_NO, orderNo);
    eleSuppTcktDocResponse.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("responseDoc : "+responseDoc.toString());
    }
    LOGGER.verbose("GCUpdateSupportTicketNo.manageAddAction--End");
    LOGGER.endTimer("GCUpdateSupportTicketNo.manageAddAction");
    return responseDoc;
  }

  /**
   * This method is used to fetch the support ticket list by calling getOrderList API.
   *
   * @param env
   * @param orderNo
   * @return
   */
  private YFCElement getSupportTcktLst(YFSEnvironment env, String orderNo) {

    LOGGER.beginTimer("GCUpdateSupportTicketNo.getSupportTcktLst");
    LOGGER.verbose("GCUpdateSupportTicketNo.getSupportTcktLst--Begin");
    final YFCDocument odrListInDoc = YFCDocument.createDocument(GCConstants.ORDER);
    odrListInDoc.getDocumentElement().setAttribute(GCConstants.ORDER_NO, orderNo);
    YFCDocument getOdrListTemp = YFCDocument.getDocumentFor(GCConstants.GET_SUPP_TCKT_TEMPLATE);
    YFCDocument getOdrLstOutDoc = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, odrListInDoc, getOdrListTemp);
    final YFCElement eleOdrList = getOdrLstOutDoc.getDocumentElement();
    final YFCElement eleOdr = eleOdrList.getChildElement(GCConstants.ORDER);
    final YFCElement eleOdrExtn = eleOdr.getChildElement(GCConstants.EXTN);
    final YFCElement eleGCSupportTicketNoList = eleOdrExtn.getChildElement(GCConstants.GC_SUPP_TCKT_LST);

    LOGGER.verbose("GCUpdateSupportTicketNo.getSupportTcktLst--end");
    LOGGER.endTimer("GCUpdateSupportTicketNo.getSupportTcktLst");
    return eleGCSupportTicketNoList;
  }
}
