package com.gc.exstore.api.receipt;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.gc.exstore.utils.GCShipmentUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class gets invoked when a container received or marked as Problematic.updates the Shipment
 * status, updates the Container status and updates the order line status. This also additionally
 * checks for Problem with Carton and stamps the flag for the carton when it is marked Problem with
 * Carton. Email is triggered conditionally only for �Received� cartons. In case of multi box and DC
 * Bundles cartons the email is triggered only when associated cartons are received
 *
 * @author bhawana.jethuri
 *
 */

public class GCReceiveOrMarkProblemWithContainerAPI {
  // initialise logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCReceiveOrMarkProblemWithContainerAPI.class
      .getName());


  /**
   *
   * This method updates the Shipment status, updates the Container status and updates the order
   * line status.
   *
   * @param env
   * @param inDoc
   * @return Doc
   *
   *
   *
   */

  public Document receiveOrMarkCarton(YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("GCReceiveOrMarkProblemWithContainerAPI.receiveOrMarkCarton");
    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.receiveOrMarkCarton--Begin");
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("inputDoc : " + inputDoc.toString());
    }
    YFCDocument outputDoc = null;
    YFCElement eleInput = inputDoc.getDocumentElement();
    String shipmentKey = eleInput.getAttribute(GCConstants.SHIPMENT_KEY);
    String shipmentContainerKeyIp = eleInput.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
    String isBundleComponent = eleInput.getAttribute(GCConstants.IS_BUNDLE_COMPONENT);
    String loginDetail = eleInput.getAttribute(GCConstants.LOGIN_ID);
    String problemWithCarton = eleInput.getAttribute(GCXmlLiterals.EXTN_IS_PROBLEM_WITH_CARTON);
    String employeeId = GCShipmentUtil.getEmployeeID(env, loginDetail);
    updateUserActivity(env, eleInput, employeeId);
    Map<String, Double> orderLineKeyMap = new HashMap<String, Double>();
    // prepare input for getShipmentListAPI call
    YFCDocument getShipmentListIp = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement eleGetShpListIp = getShipmentListIp.getDocumentElement();
    eleGetShpListIp.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
    YFCDocument getShipmentListTemp = YFCDocument.getDocumentFor(GCConstants.GET_SHIPMENT_LIST_FOR_RECEIVE_CARTON);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input to getShipmentList API  : " + getShipmentListIp.toString());
    }
    // Invoke getShipmentList API
    YFCDocument getShipmentListOp =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, getShipmentListIp, getShipmentListTemp);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Output of  getShipmentList API  : " + getShipmentListOp.toString());
    }
    YFCElement eleRoot = getShipmentListOp.getDocumentElement();
    YFCElement eleShipment = eleRoot.getChildElement(GCConstants.SHIPMENT);
    String docType = eleShipment.getAttribute(GCConstants.DOCUMENT_TYPE);
    YFCElement eleContainers = eleShipment.getChildElement(GCConstants.CONTAINERS);
    YFCNodeList<YFCElement> nlContainer = eleContainers.getElementsByTagName(GCConstants.CONTAINER);

    // prepare a map for OrderLineKey - ReleaseKey
    Map<String, String> orderReleaseKeyMap = new HashMap<String, String>();
    prepareReleaseKeyMap(orderReleaseKeyMap, eleShipment);


    String multiBoxId = eleInput.getAttribute(GCConstants.EXTN_MULTI_BOX_GROUP_ID);
    if (!YFCCommon.isVoid(multiBoxId)) {
      // Call method to update multibox container status and fetch orderlinekey , quantity for
      // changeOrderStatus API
      for (YFCElement eleContainer : nlContainer) {
        YFCElement eleExtnContainer = eleContainer.getChildElement(GCConstants.EXTN);
        if (!YFCCommon.isVoid(eleExtnContainer)) {
          String multiCoxIdNew = eleExtnContainer.getAttribute(GCConstants.EXTN_MULTI_BOX_GROUP_ID);
          if (YFCUtils.equals(multiBoxId, multiCoxIdNew)) {
            YFCElement eleContainerDetails = eleContainer.getChildElement(GCConstants.CONTAINER_DETAILS);
            YFCNodeList<YFCElement> nlContainerDetail =
                eleContainerDetails.getElementsByTagName(GCConstants.CONTAINER_DETAIL);
            for (YFCElement eleContainerDetail : nlContainerDetail) {
              YFCElement eleOrderLineCont = eleContainerDetail.getChildElement(GCConstants.ORDER_LINE);
              YFCElement eleLineExtn = eleOrderLineCont.getChildElement(GCConstants.EXTN);
              String isWarr = eleLineExtn.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
              String dependentOnLinekey = eleOrderLineCont.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
              if (YFCUtils.equals(GCConstants.YES, isWarr) && !YFCCommon.isVoid(dependentOnLinekey)) {
                Double qty = eleContainerDetail.getDoubleAttribute(GCConstants.QUANTITY);
                String olkey = eleContainerDetail.getAttribute(GCConstants.ORDER_LINE_KEY);
                addOrderLineKeyToMap(orderLineKeyMap, olkey, qty);
                break;
              }
            }

          }
        }

      }
      updateMultiBoxContainer(env, nlContainer, orderLineKeyMap, eleInput, orderReleaseKeyMap);
    } else if (YFCUtils.equals(isBundleComponent, GCConstants.YES)) {

      // Check if the container contains bundle component
      updateBundleContainer(env, eleShipment, orderLineKeyMap, inputDoc, orderReleaseKeyMap);
    } else {
      List<String> shipmentContainerKeyIpList = new ArrayList<String>();
      shipmentContainerKeyIpList.add(shipmentContainerKeyIp);
      YFCDocument changeShipmentOp =
          changeShipment(env, shipmentContainerKeyIpList, eleInput, GCConstants.READY_FOR_CUSTOMER_PICK_UP, true);
      updateShipmentStatus(env, changeShipmentOp);
      addOrderLineDetail(eleInput, orderLineKeyMap);
      updateOrderLineStatus(env, eleInput, orderLineKeyMap, docType, orderReleaseKeyMap);
      if (!YFCUtils.equals(problemWithCarton, GCConstants.YES)) {
        sendEmail(env, eleInput, orderLineKeyMap);
      }

    }
    outputDoc = printPickUpNotice(orderLineKeyMap, eleInput, env);


    return outputDoc.getDocument();
  }


  /**
   * This method is used to prepare OrderLineKey - releaseKey map.
   *
   * @param orderReleaseKeyMap
   * @param eleShipment
   */
  private void prepareReleaseKeyMap(Map<String, String> orderReleaseKeyMap, YFCElement eleShipment) {
    LOGGER.beginTimer("GCReceiveOrMarkProblemWithContainerAPI.prepareReleaseKeyMap");
    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.prepareReleaseKeyMap--Begin");

    YFCNodeList<YFCElement> nlShipmentLine = eleShipment.getElementsByTagName(GCConstants.SHIPMENT_LINE);
    for (YFCElement eleShipmentLine : nlShipmentLine) {

      String orderReleaseKey = eleShipmentLine.getAttribute(GCConstants.ORDER_RELEASE_KEY);
      String orderLineKey = eleShipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      if (!YFCCommon.isVoid(orderReleaseKey)) {
        orderReleaseKeyMap.put(orderLineKey, orderReleaseKey);
      }
    }

    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.prepareReleaseKeyMap--End");
    LOGGER.endTimer("GCReceiveOrMarkProblemWithContainerAPI.prepareReleaseKeyMap");
  }



  /**
   *
   * This method invokes changeOrderStatus API to update the status of orderlines
   *
   * @param env
   * @param docType
   * @param orderReleaseKeyMap
   * @param changeShipmentOp
   * @return
   *
   */
  private void updateOrderLineStatus(YFSEnvironment env, YFCElement eleInput, Map<String, Double> orderLineKeyMap,
      String docType, Map<String, String> orderReleaseKeyMap) {
    LOGGER.beginTimer("GCReceiveOrMarkProblemWithContainerAPI.updateOrderLineStatus");
    LOGGER
    .verbose("GCReceiveOrMap<String, String> orderReleaseKeyMapMarkProblemWithContainerAPI.updateOrderLineStatus--Begin");
    String orderHeaderKey = eleInput.getAttribute(GCConstants.ORDER_HEADER_KEY);
    String orderReleaseKey = eleInput.getAttribute(GCConstants.ORDER_RELEASE_KEY);
    YFCDocument changeOrderStatusIp = YFCDocument.createDocument(GCConstants.ORDER_STATUS_CHANGE);
    YFCElement rootEle = changeOrderStatusIp.getDocumentElement();
    if (YFCUtils.equals(GCConstants.ZERO_ZERO_ZERO_FIVE, docType)) {
      rootEle.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.READY_FOR_PICKUP_ORD_TRANID_PO);
      rootEle.setAttribute(GCConstants.ORDER_HEADER_KEY, eleInput.getAttribute(GCConstants.PURCHASE_HEADER_KEY));
    } else {
      rootEle.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.READY_FOR_PICKUP_ORD_TRANID);
      rootEle.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
    }
    YFCElement eleOrderLines = rootEle.createChild(GCConstants.ORDER_LINES);
    for (Entry<String, Double> orderLineEntry : orderLineKeyMap.entrySet()) {
      String orderLineKeyIp = orderLineEntry.getKey();
      Double quantityIp = orderLineEntry.getValue();
      YFCElement eleOrderLine = eleOrderLines.createChild(GCConstants.ORDER_LINE);
      eleOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKeyIp);
      eleOrderLine.setAttribute(GCConstants.QUANTITY, quantityIp);
      eleOrderLine.setAttribute(GCConstants.ORDER_RELEASE_KEY, orderReleaseKey);

      if (!orderReleaseKeyMap.isEmpty() && orderReleaseKeyMap.containsKey(orderLineKeyIp)) {
        eleOrderLine.setAttribute(GCConstants.ORDER_RELEASE_KEY, orderReleaseKeyMap.get(orderLineKeyIp));
      }
      eleOrderLine.setAttribute(GCConstants.BASE_DROP_STATUS, GCConstants.READY_FOR_CUSTOMER_PICKUP_ORD_STATUS);
    }

    YFCDocument chngOrderStatusOpTemp = YFCDocument.getDocumentFor(GCConstants.CHANGE_ORDER_STATUS_TEMP);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input to changeOrderStatus API: " + changeOrderStatusIp.toString());
    }
    YFCDocument changeOrderStatusOp =
        GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER_STATUS, changeOrderStatusIp, chngOrderStatusOpTemp);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Output of changeOrderStatus API: " + changeOrderStatusOp.toString());
    }
    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.updateOrderLineStatus--End");
    LOGGER.endTimer("GCReceiveOrMarkProblemWithContainerAPI.updateOrderLineStatus");

  }



  /**
   *
   * This method checks the status of all the containers in the shipment and invokes
   * changeShipmentStatus API to update shipment status
   *
   * @param env
   * @param changeShipmentOp
   * @return
   *
   */
  private void updateShipmentStatus(YFSEnvironment env, YFCDocument changeShipmentOp) {
    LOGGER.beginTimer("GCReceiveOrMarkProblemWithContainerAPI.updateShipmentStatus");
    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.updateShipmentStatus--Begin");
    YFCElement eleRoot = changeShipmentOp.getDocumentElement();
    YFCElement eleContainers = eleRoot.getChildElement(GCConstants.CONTAINERS);
    boolean isReadyForPickUp = true;
    YFCNodeList<YFCElement> nlContainer = eleContainers.getElementsByTagName(GCConstants.CONTAINER);
    // Check if all the containers are in Ready For Customer Pick up status.
    for (YFCElement eleContainer : nlContainer) {
      YFCElement eleExtnContainer = eleContainer.getChildElement(GCConstants.EXTN);
      String containerStatus = eleExtnContainer.getAttribute(GCConstants.EXTN_CONTAINER_STATUS);
      if (!YFCUtils.equals(containerStatus, GCConstants.READY_FOR_CUSTOMER_PICK_UP)) {
        isReadyForPickUp = false;
        break;
      }
    }
    if (isReadyForPickUp) {

      changeShipmentStatus(env, changeShipmentOp, GCConstants.READY_FOR_CUSTOMER_PICK_SHP_STATUS);

    } else if (!YFCUtils.equals(eleRoot.getAttribute(GCConstants.STATUS),
        GCConstants.PARTIALLY_READY_FOR_CUSTOMER_PICK_STATUS)) {

      changeShipmentStatus(env, changeShipmentOp, GCConstants.PARTIALLY_READY_FOR_CUSTOMER_PICK_STATUS);

    }
    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.updateShipmentStatus--End");
    LOGGER.endTimer("GCReceiveOrMarkProblemWithContainerAPI.updateShipmentStatus");
  }

  /**
   *
   * This method invokes the changeShipmentStatus API
   *
   * @param env
   * @param changeShipmentOp
   * @param status
   * @return
   *
   */
  private void changeShipmentStatus(YFSEnvironment env, YFCDocument changeShipmentOp, String status) {
    LOGGER.beginTimer("GCReceiveOrMarkProblemWithContainerAPI.changeShipmentStatus");
    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.changeShipmentStatus--Begin");
    YFCElement eleRoot = changeShipmentOp.getDocumentElement();
    String docType = eleRoot.getAttribute(GCConstants.DOCUMENT_TYPE);
    YFCDocument changeShipmentStatusIp = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement eleShipment = changeShipmentStatusIp.getDocumentElement();
    String shipmentKey = eleRoot.getAttribute(GCConstants.SHIPMENT_KEY);
    eleShipment.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
    eleShipment.setAttribute(GCConstants.BASE_DROP_STATUS, status);
    if (YFCUtils.equals(docType, GCConstants.ZERO_ZERO_ZERO_FIVE)) {
      eleShipment.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.READY_FOR_PICK_UP_SHP_TRANID_PO);
    } else {
      eleShipment.setAttribute(GCConstants.TRANSACTION_ID, GCConstants.READY_FOR_PICK_UP_SHP_TRANID);
    }
    YFCDocument tempdoc = YFCDocument.getDocumentFor(GCConstants.CHANGE_SHIP_TEMP);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input to changeShipmentStatus API  : " + changeShipmentStatusIp.toString());
    }
    YFCDocument changeShipmentStatusOp =
        GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_SHIPMENT_STATUS, changeShipmentStatusIp, tempdoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Output of changeShipmentStatus API  : " + changeShipmentStatusOp.toString());
    }
    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.changeShipmentStatus--End");
    LOGGER.endTimer("GCReceiveOrMarkProblemWithContainerAPI.changeShipmentStatus");
  }



  /**
   *
   * This method is called if the container selected is a multibox container.It updates the status
   * of multibox container after checking if all the corresponding containers have been received
   *
   * @param env
   *
   * @param eleContainerIp
   * @param nlContainer
   * @param qtyMap
   * @param eleInput
   * @param orderReleaseKeyMap
   * @return
   *
   */
  private void updateMultiBoxContainer(YFSEnvironment env, YFCNodeList<YFCElement> nlContainer,
      Map<String, Double> orderLineKeyMap, YFCElement eleInput, Map<String, String> orderReleaseKeyMap) {
    LOGGER.beginTimer("GCReceiveOrMarkProblemWithContainerAPI.updateMultiBoxContainer");
    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.updateMultiBoxContainer--Begin");
    List<String> listContainerKey = new ArrayList<String>();
    boolean multiBoxReceived = true;
    boolean updateShipDate = true;
    boolean isProblem = false;
    YFCDocument changeShipmentOp = null;
    String shipmentContainerKeyIp = eleInput.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
    String multiBoxIdIp = eleInput.getAttribute(GCConstants.EXTN_MULTI_BOX_GROUP_ID);
    listContainerKey.add(shipmentContainerKeyIp);
    addOrderLineDetail(eleInput, orderLineKeyMap);
    for (YFCElement eleContainer : nlContainer) {
      YFCElement eleExtnContainer = eleContainer.getChildElement(GCConstants.EXTN);
      String multiBoxId = eleExtnContainer.getAttribute(GCConstants.EXTN_MULTI_BOX_GROUP_ID);
      String problemWithCarton = eleExtnContainer.getAttribute(GCXmlLiterals.EXTN_IS_PROBLEM_WITH_CARTON);
      String shipmentContainerKey = eleContainer.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
      if (YFCUtils.equals(multiBoxId, multiBoxIdIp) && !YFCUtils.equals(shipmentContainerKey, shipmentContainerKeyIp)) {
        String containerStatus = eleExtnContainer.getAttribute(GCConstants.EXTN_CONTAINER_STATUS);
        if (YFCUtils.equals(problemWithCarton, GCConstants.YES)) {
          isProblem = true;
        }
        // check if all the multibox containers corresponding to the input container are received
        if (YFCUtils.equals(containerStatus, GCConstants.MULTIBOX_RECEIVED)) {

          listContainerKey.add(shipmentContainerKey);
        } else if (YFCUtils.equals(containerStatus, GCConstants.PENDING_RECEIPT)) {
          multiBoxReceived = false;
          break;
        }

      }
    }
    // if all the multibox containers have been received , update the container status to Ready For
    // Customer Pick up
    if (multiBoxReceived) {
      changeShipmentOp =
          changeShipment(env, listContainerKey, eleInput, GCConstants.READY_FOR_CUSTOMER_PICK_UP, updateShipDate);
      YFCElement eleChangeShp = changeShipmentOp.getDocumentElement();
      String docType = eleChangeShp.getAttribute(GCConstants.DOCUMENT_TYPE);
      updateShipmentStatus(env, changeShipmentOp);
      updateOrderLineStatus(env, eleInput, orderLineKeyMap, docType, orderReleaseKeyMap);
      if (!isProblem) {
        sendEmail(env, eleInput, orderLineKeyMap);
      }
    } else {
      List<String> shipmentContainerKeyIpList = new ArrayList<String>();
      shipmentContainerKeyIpList.add(shipmentContainerKeyIp);
      updateShipDate = false;
      changeShipmentOp =
          changeShipment(env, shipmentContainerKeyIpList, eleInput, GCConstants.MULTIBOX_RECEIVED, updateShipDate);
      if (LOGGER.isVerboseEnabled() && !YFCCommon.isVoid(changeShipmentOp)) {
        LOGGER.verbose("Output of changeShipment API  : " + changeShipmentOp.toString());
      }
    }

    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.updateMultiBoxContainer--End");
    LOGGER.endTimer("GCReceiveOrMarkProblemWithContainerAPI.updateMultiBoxContainer");
  }

  /**
   *
   * This method fetches the warranty line details for changeOrderStatus API call
   *
   * @param eleInput
   * @param qtyMap
   * @param orderReleaseKeyMap
   * @return
   *
   */

  private void addOrderLineDetail(YFCElement eleInput, Map<String, Double> orderLineKeyMap) {
    LOGGER.beginTimer("GCReceiveOrMarkProblemWithContainerAPI.addOrderLineDetail");
    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.addOrderLineDetail--Begin");
    YFCNodeList<YFCElement> nlContaineDetail = eleInput.getElementsByTagName(GCConstants.CONTAINER_DETAIL);
    // Iterate over container details to check for warranty component. If warranty component is
    // present , add its details to the map
    for (YFCElement eleContainerDetail : nlContaineDetail) {
      YFCElement eleWarrantyComp = eleContainerDetail.getChildElement(GCXmlLiterals.WARRANTY_COMPONENT);
      YFCNodeList<YFCElement> nlSetComponent = eleContainerDetail.getElementsByTagName(GCXmlLiterals.SET_COMPONENT);
      int lengthSetComp = nlSetComponent.getLength();
      String orderLineKey = null;
      String isBundle = eleInput.getAttribute(GCConstants.IS_BUNDLE_COMPONENT);
      if (!YFCCommon.isVoid(eleWarrantyComp)) {
        orderLineKey = eleWarrantyComp.getAttribute(GCConstants.ORDER_LINE_KEY);
        Double warrantyQty = eleWarrantyComp.getDoubleAttribute(GCConstants.QUANTITY);

        // not adding warranty to orderLineKeyMap in case of multibox as it is already added in the
        // beginning
        if (YFCCommon.isVoid(eleInput.getAttribute(GCConstants.EXTN_MULTI_BOX_GROUP_ID))) {
          addOrderLineKeyToMap(orderLineKeyMap, orderLineKey, warrantyQty);
        }
      } else if (lengthSetComp == 0 && !YFCUtils.equals(isBundle, GCConstants.YES)) {
        Double quantity = 0.00;
        orderLineKey = eleContainerDetail.getAttribute(GCConstants.ORDER_LINE_KEY);

        // If container is multibox , set quantity as 1
        String multiBoxId = eleInput.getAttribute(GCConstants.EXTN_MULTI_BOX_GROUP_ID);
        if (!YFCCommon.isVoid(multiBoxId)) {
          quantity = 1.00;
        } else {
          quantity = eleContainerDetail.getDoubleAttribute(GCConstants.QUANTITY);
        }
        addOrderLineKeyToMap(orderLineKeyMap, orderLineKey, quantity);

      }

      for (YFCElement eleSetComponent : nlSetComponent) {
        orderLineKey = eleSetComponent.getAttribute(GCConstants.ORDER_LINE_KEY);
        Double warrantyQty = eleSetComponent.getDoubleAttribute(GCConstants.QUANTITY);
        addOrderLineKeyToMap(orderLineKeyMap, orderLineKey, warrantyQty);
      }
    }
    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.addOrderLineDetail--End");
    LOGGER.endTimer("GCReceiveOrMarkProblemWithContainerAPI.addOrderLineDetail");
  }


  /**
   *
   * This method is called if the container selected is a multibox container.It updates the status
   * of multibox container after checking if all the corresponding containers have been received
   *
   * @param env
   *
   * @param eleContainerIp
   * @param nlContainer
   * @param orderReleaseKeyMap
   * @param qtyMap
   * @param eleInput
   * @param orderReleaseKeyMap
   * @return
   *
   */
  private void updateBundleContainer(YFSEnvironment env, YFCElement eleShipment, Map<String, Double> orderLineKeyMap,
      YFCDocument inDoc, Map<String, String> orderReleaseKeyMap) {
    LOGGER.beginTimer("GCReceiveOrMarkProblemWithContainerAPI.updateBundleContainer");
    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.updateBundleContainer--Begin");

    YFCElement eleInput = inDoc.getDocumentElement();
    // List of shipmentLineKeys part of the container
    List<String> listShipmentLineKey = new ArrayList<String>();
    // List of parentShipmentLineKeys part of the container
    List<String> listParentShipmentLineKey = new ArrayList<String>();
    prepareListShipmentLineKey(listShipmentLineKey, listParentShipmentLineKey, eleInput);

    List<String> listContainerKey = new ArrayList<String>();
    boolean componentReceived = true;
    boolean isProblem = false;
    boolean updateShipeDate = true;
    YFCDocument changeShipmentOp = null;

    YFCElement eleContainers = eleShipment.getChildElement(GCConstants.CONTAINERS);
    YFCNodeList<YFCElement> nlContainer = eleContainers.getElementsByTagName(GCConstants.CONTAINER);
    String shipmentContainerKeyIp = eleInput.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);

    // List of parentShipmentLineKeys part of the container which are in pending receipt status
    List<String> listParentShipLineKeyPndgRcpt = new ArrayList<String>();

    // List of containerKeys that contains components which are not yet received i.e. the components
    // are part of container which is in pending receipt status.
    List<String> listContainerKeyPndgRcpt = new ArrayList<String>();

    preparePndgRcptParentShpLineKeyList(listParentShipLineKeyPndgRcpt, nlContainer, shipmentContainerKeyIp);
    prepareContainerKeyList(listContainerKeyPndgRcpt, listContainerKey, nlContainer, orderLineKeyMap,
        shipmentContainerKeyIp, listParentShipLineKeyPndgRcpt, listShipmentLineKey);

    // This map is used to store OrderLineKey as key & quantity as value to store the orderLineKeys
    // of the regular lines which are in same container as of bundle components. So that we can move
    // the status of that regular line to ReadyForCustomerPickup unlike component lines whose line
    // status gets changed once all the components are received.
    Map<String, Double> regularItemInBundleCntr = new HashMap<String, Double>();

    YFCElement eleShipmentLines = eleShipment.getChildElement(GCConstants.SHIPMENT_LINES);
    YFCNodeList<YFCElement> nlShipmentLine = eleShipmentLines.getElementsByTagName(GCConstants.SHIPMENT_LINE);
    for (YFCElement eleShipmentLine : nlShipmentLine) {
      Double quantityComp = 0.0;
      String parenKey = eleShipmentLine.getAttribute(GCConstants.PARENT_SHPMNTLINE_KEY);
      String shipmentLKey = eleShipmentLine.getAttribute(GCConstants.SHIPMENT_LINE_KEY);
      if (!YFCCommon.isVoid(parenKey)) {
        quantityComp = eleShipmentLine.getDoubleAttribute(GCConstants.QUANTITY);
      } else {
        String qty =
            GCXMLUtil.getAttributeFromXPath(inDoc.getDocument(), "ContainerDetails/ContainerDetail[@ShipmentLineKey='"
                + shipmentLKey
                + "']/@Quantity");
        if (!YFCCommon.isVoid(qty)) {
          quantityComp = Double.parseDouble(qty);
        }
      }
      if (listShipmentLineKey.contains(eleShipmentLine.getAttribute(GCConstants.SHIPMENT_LINE_KEY))) {
        addOrderLineKeyToMap(orderLineKeyMap, eleShipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY), quantityComp);
        if (YFCCommon.isVoid(parenKey)){
          regularItemInBundleCntr.put(eleShipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY), quantityComp);
        }
      }
    }
    addOrderLineDetail(eleInput, orderLineKeyMap);
    listContainerKey.add(shipmentContainerKeyIp);
    boolean isBreakReq = false;
    for (YFCElement eleContainer : nlContainer) {
      YFCElement eleExtnContainer = eleContainer.getChildElement(GCConstants.EXTN);
      String problemWithCarton = eleExtnContainer.getAttribute(GCXmlLiterals.EXTN_IS_PROBLEM_WITH_CARTON);
      String shipmentContainerKey = eleContainer.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
      YFCElement eleContainerDetails = eleContainer.getChildElement(GCConstants.CONTAINER_DETAILS);

      if (YFCUtils.equals(problemWithCarton, GCConstants.YES)) {
        isProblem = true;
      }


      if (!YFCUtils.equals(shipmentContainerKey, shipmentContainerKeyIp)) {
        YFCNodeList<YFCElement> nlContainerDtl = eleContainerDetails.getElementsByTagName(GCConstants.CONTAINER_DETAIL);

        for (YFCElement eleContainerDtl : nlContainerDtl) {
          YFCElement eleShipmentLineCont = eleContainerDtl.getChildElement(GCConstants.SHIPMENT_LINE);
          String parentShipmentLineKeyCont = eleShipmentLineCont.getAttribute(GCConstants.PARENT_SHPMNTLINE_KEY);
          if (!YFCCommon.isVoid(parentShipmentLineKeyCont)
              && listParentShipmentLineKey.contains(parentShipmentLineKeyCont)
              && !YFCCommon.isVoid(listContainerKeyPndgRcpt) && listContainerKeyPndgRcpt.contains(shipmentContainerKey)) {

            componentReceived = false;
            isBreakReq = true;
            break;
          }
        }
        LOGGER.verbose("isBreakReq : " + isBreakReq);
      }
      if (isBreakReq) {
        break;
      }

    }
    // if all the bundle components have been received , update the container status to Ready For
    // Customer Pick up
    if (componentReceived) {
      changeShipmentOp =
          changeShipment(env, listContainerKey, eleInput, GCConstants.READY_FOR_CUSTOMER_PICK_UP, updateShipeDate);
      YFCElement eleChangeShip = changeShipmentOp.getDocumentElement();
      String docType = eleChangeShip.getAttribute(GCConstants.DOCUMENT_TYPE);
      updateShipmentStatus(env, changeShipmentOp);
      updateOrderLineStatus(env, eleInput, orderLineKeyMap, docType, orderReleaseKeyMap);
      if (!isProblem) {
        sendEmail(env, eleInput, orderLineKeyMap);
      }


    } else {
      updateShipeDate = false;
      List<String> shipmentContainerKeyIpList = new ArrayList<String>();
      shipmentContainerKeyIpList.add(shipmentContainerKeyIp);
      changeShipmentOp =
          changeShipment(env, shipmentContainerKeyIpList, eleInput, GCConstants.COMPONENT_RECEIVED, updateShipeDate);

      // Moving the status of that regular line to ReadyForCustomerPickup unlike component lines
      // whose line status gets changed once all the components are received.
      if (!regularItemInBundleCntr.isEmpty()) {
        YFCElement eleChangeShip = changeShipmentOp.getDocumentElement();
        String docType = eleChangeShip.getAttribute(GCConstants.DOCUMENT_TYPE);
        updateOrderLineStatus(env, eleInput, regularItemInBundleCntr, docType, orderReleaseKeyMap);
      }
      if (LOGGER.isVerboseEnabled() && !YFCCommon.isVoid(changeShipmentOp)) {
        LOGGER.verbose("Output of changeShipment API  : " + changeShipmentOp.toString());
      }
    }
    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.updateBundleContainer--End");
    LOGGER.endTimer("GCReceiveOrMarkProblemWithContainerAPI.updateBundleContainer");
  }

  /**
   * This method is used to prepare listContainerKeyPndgRcpt & listContainerKeyReceived.
   *
   * @param listContainerKeyPndgRcpt
   * @param listContainerKeyReceived
   * @param nlContainer
   * @param orderLineKeyMap
   * @param shipmentContainerKeyIp
   * @param listParentShipLineKeyPndgRcpt
   * @param listShipmentLineKey
   */
  private void prepareContainerKeyList(List<String> listContainerKeyPndgRcpt, List<String> listContainerKey,
      YFCNodeList<YFCElement> nlContainer, Map<String, Double> orderLineKeyMap, String shipmentContainerKeyIp,
      List<String> listParentShipLineKeyPndgRcpt, List<String> listShipmentLineKey) {
    LOGGER.beginTimer("GCReceiveOrMarkProblemWithContainerAPI.prepareContainerKeyList");
    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.prepareContainerKeyList--Begin");
    for (YFCElement eleContainer : nlContainer) {
      YFCElement eleExtnContainer = eleContainer.getChildElement(GCConstants.EXTN);
      String containerStatus = eleExtnContainer.getAttribute(GCConstants.EXTN_CONTAINER_STATUS);
      String shipmentContainerKey = eleContainer.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);

      if (!YFCUtils.equals(shipmentContainerKeyIp, shipmentContainerKey)){

        YFCElement eleContainerDetails = eleContainer.getChildElement(GCConstants.CONTAINER_DETAILS);
        YFCNodeList<YFCElement> nlContainerDtl = eleContainerDetails.getElementsByTagName(GCConstants.CONTAINER_DETAIL);
        for (YFCElement eleContainerDtl : nlContainerDtl) {
          YFCElement eleShipmentLineCont = eleContainerDtl.getChildElement(GCConstants.SHIPMENT_LINE);
          String parentShipmentLineKeyCont = eleShipmentLineCont.getAttribute(GCConstants.PARENT_SHPMNTLINE_KEY);
          
          //Adding fix for including warranty items present in the container which are already received.
          YFCElement eleOdrLineCont = eleContainerDtl.getChildElement(GCConstants.ORDER_LINE);
          String dependentOnLineKeyCont = eleOdrLineCont.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
          if (!YFCCommon.isVoid(parentShipmentLineKeyCont) && !YFCCommon.isVoid(listParentShipLineKeyPndgRcpt)
              && listParentShipLineKeyPndgRcpt.contains(parentShipmentLineKeyCont)
              && !listContainerKeyPndgRcpt.contains(shipmentContainerKey)) {

            listContainerKeyPndgRcpt.add(shipmentContainerKey);
          }

          // if container is received and the shipmentline is not part of the current container
          // being received then updating orderLineKeyMap. if shipmentLine is part of the current
          // container being received then orderLineKeyMap will be updated later in the code.
          if (YFCUtils.equals(containerStatus, GCConstants.COMPONENT_RECEIVED)
              && (YFCCommon.isVoid(listContainerKeyPndgRcpt) || !listContainerKeyPndgRcpt
                  .contains(shipmentContainerKey))
                  && !listShipmentLineKey.contains(eleContainerDtl.getAttribute(GCConstants.SHIPMENT_LINE_KEY))
                  && (!YFCCommon.isVoid(parentShipmentLineKeyCont) || !YFCCommon.isVoid(dependentOnLineKeyCont))) {
            Double quantityComp = eleContainerDtl.getDoubleAttribute(GCConstants.QUANTITY);
            addOrderLineKeyToMap(orderLineKeyMap, eleContainerDtl.getAttribute(GCConstants.ORDER_LINE_KEY),
                quantityComp);
          }
        }
        if (YFCUtils.equals(containerStatus, GCConstants.COMPONENT_RECEIVED)
            && (YFCCommon.isVoid(listContainerKeyPndgRcpt) || !listContainerKeyPndgRcpt.contains(shipmentContainerKey))) {

          listContainerKey.add(shipmentContainerKey);
        }
      }
    }

    LOGGER.verbose("listContainerKeyPndgRcpt : " + listContainerKeyPndgRcpt);
    LOGGER.verbose("listContainerKey : " + listContainerKey);
    LOGGER.verbose("orderLineKeyMap : " + orderLineKeyMap);
    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.prepareContainerKeyList--End");
    LOGGER.endTimer("GCReceiveOrMarkProblemWithContainerAPI.prepareContainerKeyList");
  }

  /**
   * This method is used to prepare List of parentShipmentLineKeys part of the container which are
   * in pending receipt status.
   *
   * @param listParentShipLineKeyPndgRcpt
   * @param nlContainer
   * @param shipmentContainerKeyIp
   */
  private void preparePndgRcptParentShpLineKeyList(List<String> listParentShipLineKeyPndgRcpt,
      YFCNodeList<YFCElement> nlContainer, String shipmentContainerKeyIp) {

    LOGGER.beginTimer("GCReceiveOrMarkProblemWithContainerAPI.preparePndgRcptParentShpLineKeyList");
    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.preparePndgRcptParentShpLineKeyList--Begin");
    for (YFCElement eleContainer : nlContainer) {
      YFCElement eleExtnContainer = eleContainer.getChildElement(GCConstants.EXTN);
      String containerStatus = eleExtnContainer.getAttribute(GCConstants.EXTN_CONTAINER_STATUS);
      String shipmentContainerKey = eleContainer.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
      if (!YFCUtils.equals(shipmentContainerKeyIp, shipmentContainerKey)
          && YFCUtils.equals(containerStatus, GCConstants.PENDING_RECEIPT)) {

        YFCElement eleContainerDetails = eleContainer.getChildElement(GCConstants.CONTAINER_DETAILS);
        YFCNodeList<YFCElement> nlContainerDtl = eleContainerDetails.getElementsByTagName(GCConstants.CONTAINER_DETAIL);
        for (YFCElement eleContainerDtl : nlContainerDtl) {
          YFCElement eleShipmentLineCont = eleContainerDtl.getChildElement(GCConstants.SHIPMENT_LINE);
          String parentShipmentLineKeyCont = eleShipmentLineCont.getAttribute(GCConstants.PARENT_SHPMNTLINE_KEY);
          if (!YFCCommon.isVoid(parentShipmentLineKeyCont)
              && !(YFCCommon.isVoid(listParentShipLineKeyPndgRcpt) || listParentShipLineKeyPndgRcpt
                  .contains(parentShipmentLineKeyCont))) {

            listParentShipLineKeyPndgRcpt.add(parentShipmentLineKeyCont);
          }
        }
      }
    }
    LOGGER.verbose("listParentShipLineKeyPndgRcpt : " + listParentShipLineKeyPndgRcpt);
    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.preparePndgRcptParentShpLineKeyList--End");
    LOGGER.endTimer("GCReceiveOrMarkProblemWithContainerAPI.preparePndgRcptParentShpLineKeyList");
  }


  /**
   * This method is used to prepare the list of shipmentLineKeys which are the part of container
   * being received.
   *
   * @param listShipmentLineKey
   * @param listParentShipmentLineKey
   * @param eleInput
   */
  private void prepareListShipmentLineKey(List<String> listShipmentLineKey, List<String> listParentShipmentLineKey,
      YFCElement eleInput) {

    LOGGER.beginTimer("GCReceiveOrMarkProblemWithContainerAPI.prepareListShipmentLineKey");
    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.prepareListShipmentLineKey--Begin");
    YFCNodeList<YFCElement> nlContainerDetail = eleInput.getElementsByTagName(GCConstants.CONTAINER_DETAIL);
    for (YFCElement eleContainerDetail : nlContainerDetail) {
      listShipmentLineKey.add(eleContainerDetail.getAttribute(GCConstants.SHIPMENT_LINE_KEY));
      String parentShipLineKey = eleContainerDetail.getAttribute(GCConstants.PARENT_SHPMNTLINE_KEY);
      if (!YFCCommon.isVoid(parentShipLineKey)) {
        listParentShipmentLineKey.add(parentShipLineKey);
      }
    }
    LOGGER.verbose("listShipmentLineKey : " + listShipmentLineKey);
    LOGGER.verbose("listParentShipmentLineKey : " + listParentShipmentLineKey);
    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.prepareListShipmentLineKey--Begin");
    LOGGER.endTimer("GCReceiveOrMarkProblemWithContainerAPI.prepareListShipmentLineKey");
  }


  /**
   *
   * This method takes as input the list of ShipmentContainerKey and invokes the changeShipment API
   * to update the container status
   *
   * @param env
   * @param listContainerKey
   * @param eleInput
   * @param status
   * @param updateShipDate
   *
   */
  private YFCDocument changeShipment(YFSEnvironment env, List<String> listContainerKey, YFCElement eleInput,
      String status, boolean updateShipDate) {
    LOGGER.beginTimer("GCReceiveOrMarkProblemWithContainerAPI.changeShipment");
    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.changeShipment--Begin");
    String containerKeyIp = eleInput.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
    String problemWithCarton = eleInput.getAttribute(GCXmlLiterals.EXTN_IS_PROBLEM_WITH_CARTON);
    YFCDocument changeShipmentIp = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement rootEle = changeShipmentIp.getDocumentElement();
    rootEle.setAttribute(GCConstants.SHIPMENT_KEY, eleInput.getAttribute(GCConstants.SHIPMENT_KEY));
    if (updateShipDate) {
      YFCElement eleExtnShipment = rootEle.createChild(GCConstants.EXTN);
      YDate date = new YDate(false);
      eleExtnShipment.setAttribute(GCConstants.EXTN_SHIP_DATE, date.getString("yyyy-MM-dd'T'HH:mm:ss"));
    }
    YFCElement eleContainersIp = rootEle.createChild(GCConstants.CONTAINERS);
    // fetch the shipmentcontainerkey from the list and set to the changeShipment API Input
    for (String containerKey : listContainerKey) {
      YFCElement eleContainerIp = eleContainersIp.createChild(GCConstants.CONTAINER);
      eleContainerIp.setAttribute(GCConstants.SHIPMENT_CONTAINER_KEY, containerKey);
      YFCElement eleExtnContainerIp = eleContainerIp.createChild(GCConstants.EXTN);
      eleExtnContainerIp.setAttribute(GCConstants.EXTN_CONTAINER_STATUS, status);
      // If problem with carton is selected for the input container , update the
      // ExtnIsProblemWithCarton flag as Y
      if (YFCUtils.equals(containerKey, containerKeyIp) && YFCUtils.equals(problemWithCarton, GCConstants.YES)) {
        eleExtnContainerIp.setAttribute(GCXmlLiterals.EXTN_IS_PROBLEM_WITH_CARTON, problemWithCarton);
      }
    }
    YFCDocument chngShipmentTemp = YFCDocument.getDocumentFor(GCConstants.CHANGE_SHIPMENT_OUT_TEMP_RECEIVE);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input to changeShipment  : " + changeShipmentIp.toString());
    }
    // Invoke changeShipment API to update container status
    YFCDocument chngShipmentOpDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_SHIPMENT, changeShipmentIp, chngShipmentTemp);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Output of  changeShipment  : " + chngShipmentOpDoc.toString());
    }
    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.changeShipment--End");
    LOGGER.endTimer("GCReceiveOrMarkProblemWithContainerAPI.changeShipment");
    return chngShipmentOpDoc;
  }


  /**
   *
   * This method adds the orderlinekey and quantity to the map for changeOrderStatus API call
   *
   * @param orderLineKeyMap
   * @param orderLineKey
   * @param quantity
   * @return
   *
   */
  private void addOrderLineKeyToMap(Map<String, Double> orderLineKeyMap, String orderLineKey, Double quantity) {
    LOGGER.beginTimer("GCReceiveOrMarkProblemWithContainerAPI.addOrderLineKeyToMap");
    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.addOrderLineKeyToMap--Begin");
    if (!orderLineKeyMap.containsKey(orderLineKey)) {
      orderLineKeyMap.put(orderLineKey, quantity);
    } else {
      orderLineKeyMap.put(orderLineKey, orderLineKeyMap.get(orderLineKey) + quantity);
    }

    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.addOrderLineKeyToMap--End");
    LOGGER.endTimer("GCReceiveOrMarkProblemWithContainerAPI.addOrderLineKeyToMap");
  }


  /**
   *
   * This method prepares the input for sending ReadyForCustomerPickUp Email and calling the email
   * service
   *
   * @param env
   * @param eleInput
   * @param orderLineKeyMap
   * @return
   *
   */
  private void sendEmail(YFSEnvironment env, YFCElement eleInput, Map<String, Double> orderLineKeyMap) {
    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.sendEmail--Begin");
    LOGGER.beginTimer("GCReceiveOrMarkProblemWithContainerAPI.sendEmail");
    Map<String, Double> bundleMap = new HashMap<String, Double>();
    YFCDocument emailDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement eleShipment = emailDoc.getDocumentElement();
    eleShipment.setAttribute(GCConstants.SHIPMENT_KEY, eleInput.getAttribute(GCConstants.SHIPMENT_KEY));
    String purchaseOrderHeaderKey = eleInput.getAttribute(GCConstants.PURCHASE_HEADER_KEY);
    String billToId = eleInput.getAttribute(GCConstants.BILL_TO_ID);
    eleShipment.setAttribute(GCConstants.BILL_TO_ID, billToId);
    if (!YFCCommon.isVoid(purchaseOrderHeaderKey)) {
      eleShipment.setAttribute(GCConstants.ORDER_NO, eleInput.getAttribute(GCConstants.PURCHASE_ORDER_NO));
      eleShipment.setAttribute(GCConstants.ORDER_HEADER_KEY, purchaseOrderHeaderKey);
    } else {
      eleShipment.setAttribute(GCConstants.ORDER_NO, eleInput.getAttribute(GCConstants.ORDER_NO));
      eleShipment.setAttribute(GCConstants.ORDER_HEADER_KEY, eleInput.getAttribute(GCConstants.ORDER_HEADER_KEY));
    }
    if (YFCCommon.isVoid(purchaseOrderHeaderKey)) {
      String sOrderHeaderKey = eleInput.getAttribute(GCConstants.ORDER_HEADER_KEY);
      Document getOrderListInDoc = GCXMLUtil.getDocument("<Order OrderHeaderKey=\"" + sOrderHeaderKey + "\"></Order>");

      Document docGetOrderListIPTemplate =
          GCXMLUtil
          .getDocument("<OrderList><Order OrderNo='' OrderHeaderKey=''>"
              + "<OrderLines><OrderLine MaxLineStatus='' MinLineStatus='' OrderLineKey='' OrderedQty=''><BundleParentLine OrderLineKey=''/>"
              + "</OrderLine></OrderLines></Order></OrderList>");

      Document docGetOrderListOp =
          GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListInDoc, docGetOrderListIPTemplate);

      for (Entry<String, Double> orderLineEntry : orderLineKeyMap.entrySet()) {
        String orderLineKeyIp = orderLineEntry.getKey();
        Element eleBundleParent =GCXMLUtil.getElementByXPath(
            docGetOrderListOp, "OrderList/Order/OrderLines/OrderLine[@OrderLineKey='"
                + orderLineKeyIp + "']/BundleParentLine");
        if (!YFCCommon.isVoid(eleBundleParent)) {
          String orderLineyKeyParent = eleBundleParent.getAttribute(GCConstants.ORDER_LINE_KEY);
          if (!bundleMap.containsKey(orderLineyKeyParent)) {
            String qtyParent =
                GCXMLUtil.getAttributeFromXPath(docGetOrderListOp,
                    "OrderList/Order/OrderLines/OrderLine[@OrderLineKey='" + orderLineyKeyParent + "']/@OrderedQty");
            Double dQtyParent = Double.parseDouble(qtyParent);
            bundleMap.put(orderLineyKeyParent, dQtyParent);
          }
        }
      }

      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("GetOrderList Output document is" + docGetOrderListOp.toString());
      }
    }
    YFCElement eleShipmentLines = eleShipment.createChild(GCConstants.SHIPMENT_LINES);
    for (Entry<String, Double> orderLineEntry : orderLineKeyMap.entrySet()) {
      String orderLineKeyIp = orderLineEntry.getKey();
      Double quantityIp = orderLineEntry.getValue();
      YFCElement eleShipmentLine = eleShipmentLines.createChild(GCConstants.SHIPMENT_LINE);
      eleShipmentLine.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKeyIp);
      eleShipmentLine.setAttribute(GCConstants.QUANTITY, quantityIp);
    }
    for (Entry<String, Double> bundleOrderLineEntry : bundleMap.entrySet()) {
      String orderLineKeyIp = bundleOrderLineEntry.getKey();
      Double quantityIp = bundleOrderLineEntry.getValue();
      YFCElement eleShipmentLine = eleShipmentLines.createChild(GCConstants.SHIPMENT_LINE);
      eleShipmentLine.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKeyIp);
      eleShipmentLine.setAttribute(GCConstants.QUANTITY, quantityIp);
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input to email service  : " + emailDoc.toString());
    }
    try {
      GCCommonUtil.invokeService(env, GCConstants.GC_READY_FOR_CUSTOMER_PICKUP_EMAIL_SERVICE, emailDoc);
    } catch (YFCException e) {
      LOGGER.error("creating exception", e);
      final YFCDocument createExceptionDoc = YFCDocument.createDocument(GCConstants.INBOX);
      final YFCElement eleInbox = createExceptionDoc.getDocumentElement();
      eleInbox.setAttribute(GCConstants.ORDER_NO, eleInput.getAttribute(GCConstants.ORDER_NO));
      eleInbox.setAttribute(GCConstants.SHIPMENT_KEY, eleInput.getAttribute(GCConstants.SHIPMENT_KEY));
      eleInbox.setAttribute(GCConstants.ENTERPRISE_KEY, GCConstants.GC);
      eleInbox.setAttribute(GCConstants.DESCRIPTION, GCConstants.EMAIL_ERROR_DESC);
      eleInbox.setAttribute(GCConstants.EXCEPTION_TYPE, "GCEmailError");

      GCCommonUtil.invokeAPI(env, GCConstants.API_CREATE_EXCEPTION, createExceptionDoc.getDocument());
    }
    LOGGER.verbose("GCReceiveOrMarkProblemWithContainerAPI.sendEmail--End");
    LOGGER.endTimer("GCReceiveOrMarkProblemWithContainerAPI.sendEmail");
  }

  private YFCDocument printPickUpNotice(Map<String, Double> orderLineKeyMap, YFCElement eleInput, YFSEnvironment env) {
    LOGGER.beginTimer("GCManageCustomerPickup.printPickUpNotice");
    LOGGER.verbose("GCManageCustomerPickup.printPickUpNotice--Begin");
    int index = 0;

    YFCDocument opDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement opEle = opDoc.getDocumentElement();
    opEle.setAttribute(GCConstants.DELIVERY_METHOD, GCConstants.SHP);
    YFCElement eleShipmentLines = opEle.createChild(GCConstants.SHIPMENT_LINES);
    String shipNode = eleInput.getAttribute(GCConstants.STORE_ID);
    String multiBoxDesc = eleInput.getAttribute(GCConstants.EXTN_MULTIBOX_DESC);
    YFCDocument orgListIp = YFCDocument.createDocument(GCConstants.ORGANIZATION);
    YFCElement orgEle = orgListIp.getDocumentElement();
    orgEle.setAttribute(GCConstants.ORGANIZATION_CODE, shipNode);
    YFCDocument orgListOpTemp = YFCDocument.getDocumentFor(GCConstants.GET_ORGANIZATION_LIST_TEMP);
    YFCDocument orgListOpDoc = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORGANIZATION_LIST, orgListIp, orgListOpTemp);
    YFCElement orgListEle = orgListOpDoc.getDocumentElement();
    YFCElement eleOrganization = orgListEle.getChildElement(GCConstants.ORGANIZATION);
    YFCElement eleNode = eleOrganization.getChildElement(GCConstants.NODE);
    String description = eleNode.getAttribute(GCConstants.DESCRIPTION);
    YFCElement eleShipNode = opEle.createChild(GCConstants.SHIP_NODE);
    eleShipNode.setAttribute(GCConstants.SHIP_NODE, shipNode);
    eleShipNode.setAttribute(GCConstants.DESCRIPTION, description);
    opEle.setAttribute(GCConstants.ORDER_NO, eleInput.getAttribute(GCConstants.ORDER_NO));
    opEle.setAttribute(GCConstants.BILL_TO_CUSTOMER_ID, eleInput.getAttribute(GCConstants.BILL_TO_ID));
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    Date sysDate = new Date();
    String strDate = sdf.format(sysDate);
    String orderDate = eleInput.getAttribute(GCConstants.ORDER_DATE);
    String orderDate1 = orderDate.substring(0, orderDate.lastIndexOf('T'));
    opEle.setAttribute(GCConstants.ORDER_DATE, orderDate1);
    if (!YFCCommon.isVoid(multiBoxDesc)) {
      opEle.setAttribute(GCConstants.EXTN_MULTIBOX_DESC, multiBoxDesc);
    }
    opEle.importNode(eleInput.getChildElement(GCConstants.TO_ADDRESS));
    YFCElement elePaymentMethods = eleInput.getChildElement(GCConstants.PAYMENT_METHODS);
    if (!YFCCommon.isVoid(elePaymentMethods)) {
      YFCElement elePaymentMethod = elePaymentMethods.getChildElement(GCConstants.PAYMENT_METHOD);
      if(!YFCCommon.isVoid(elePaymentMethod)){
        String sCreditCardType = elePaymentMethod.getAttribute(GCConstants.CREDIT_CARD_TYPE);
        opEle.setAttribute(GCConstants.PAYMENT_TYPE, sCreditCardType);
      }
    }
    String isBundleComponent = eleInput.getAttribute(GCConstants.IS_BUNDLE_COMPONENT);
    if (YFCUtils.equals(isBundleComponent, GCConstants.YES)) {
      YFCElement eleShipmentLine = eleShipmentLines.createChild(GCConstants.SHIPMENT_LINE);
      eleShipmentLine.setAttribute(GCConstants.ITEM_ID, eleInput.getAttribute(GCXmlLiterals.BUNDLE_ITEM_ID));
      eleShipmentLine.setAttribute(GCConstants.ITEM_DESCRIPTION,
          eleInput.getAttribute(GCXmlLiterals.BUNDLE_PARENT_ITEM_DESCRIPTION));
      eleShipmentLine.setAttribute(GCConstants.SERIAL_NUMBER, eleInput.getAttribute(GCConstants.SERIAL_NO));
      eleShipmentLine.setAttribute(GCConstants.UNIT_PRICE, eleInput.getAttribute(GCConstants.UNIT_PRICE));
      eleShipmentLine.setAttribute(GCConstants.SYSTEM_DATE, strDate);
      eleShipmentLine.setAttribute(GCXmlLiterals.INDEX_NO, GCConstants.ONE);
      eleShipmentLine.setAttribute(GCXmlLiterals.QUANTITY_PICKED, GCConstants.BLANK_STRING);
      eleShipmentLine.setAttribute(GCConstants.KIT_CODE, GCConstants.BLANK_STRING);
      eleShipmentLines.importNode(eleShipmentLine);
    }
    YFCNodeList<YFCElement> nlContainerDetail = eleInput.getElementsByTagName(GCConstants.CONTAINER_DETAIL);
    for (YFCElement eleContainerDetail : nlContainerDetail) {
      YFCElement eleWarrantyComp = eleContainerDetail.getChildElement(GCXmlLiterals.WARRANTY_COMPONENT);
      if (!YFCCommon.isVoid(eleWarrantyComp)) {
        continue;
      }
      String ordLineKeyCont = eleContainerDetail.getAttribute(GCConstants.ORDER_LINE_KEY);
      Double quantity = orderLineKeyMap.get(ordLineKeyCont);
      String sQuantity = String.format("%.0f", quantity);
      String itemIdCont = eleContainerDetail.getAttribute(GCConstants.ITEM_ID);
      String itemDescContDet = eleContainerDetail.getAttribute(GCConstants.ITEM_DESCRIPTION);
      String serialNoCont = eleContainerDetail.getAttribute(GCConstants.SERIAL_NO);
      String unitPriceCont = eleContainerDetail.getAttribute(GCConstants.UNIT_PRICE);
      YFCElement eleShipmentLine = eleShipmentLines.createChild(GCConstants.SHIPMENT_LINE);
      eleShipmentLine.setAttribute(GCConstants.ITEM_ID, itemIdCont);
      eleShipmentLine.setAttribute(GCConstants.ITEM_DESCRIPTION, itemDescContDet);
      eleShipmentLine.setAttribute(GCConstants.SERIAL_NUMBER, serialNoCont);
      eleShipmentLine.setAttribute(GCXmlLiterals.QUANTITY_PICKED, sQuantity);
      eleShipmentLine.setAttribute(GCConstants.SYSTEM_DATE, strDate);
      if (YFCCommon.isVoid(isBundleComponent)) {
        eleShipmentLine.setAttribute(GCXmlLiterals.INDEX_NO, ++index);
        eleShipmentLine.setAttribute(GCConstants.UNIT_PRICE, unitPriceCont);
      } else {
        eleShipmentLine.setAttribute(GCXmlLiterals.INDEX_NO, GCConstants.BLANK_STRING);
        eleShipmentLine.setAttribute(GCConstants.UNIT_PRICE, GCConstants.BLANK_STRING);
      }
      eleShipmentLine.setAttribute(GCConstants.KIT_CODE, GCConstants.BLANK_STRING);
      eleShipmentLines.importNode(eleShipmentLine);
    }
    opEle.importNode(eleShipmentLines);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("input to print pick up slip service : " + opDoc.toString());
    }

    // Invoke service to print pick up notice
    YFCDocument pickUpSlipDoc = GCCommonUtil.invokeService(env, GCConstants.GC_PRINT_ORDER_PICKUP_NOTICE, opDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("output of  print pick up slip service : " + pickUpSlipDoc.toString());
    }


    LOGGER.verbose("GCManageCustomerPickup.printPickUpNotice--End");
    LOGGER.endTimer("GCManageCustomerPickup.printPickUpNotice");
    return pickUpSlipDoc;
  }

  private void updateUserActivity(YFSEnvironment env, YFCElement eleInput, String employeeId) {
    YFCDocument gcShpmntActvIndoc = YFCDocument.createDocument(GCConstants.GC_SHIPMENT_ACTICITY);
    YFCElement gcShpmntActivity = gcShpmntActvIndoc.getDocumentElement();
    String shipmentKey = eleInput.getAttribute(GCConstants.SHIPMENT_KEY);
    String orderHeaderKey = eleInput.getAttribute(GCConstants.ORDER_HEADER_KEY);
    String shipmentContainerKey = eleInput.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
    String problemWithCarton = eleInput.getAttribute(GCXmlLiterals.EXTN_IS_PROBLEM_WITH_CARTON);
    gcShpmntActivity.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
    gcShpmntActivity.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
    gcShpmntActivity.setAttribute(GCConstants.SHIPMENT_CONTAINER_KEY, shipmentContainerKey);
    gcShpmntActivity.setAttribute(GCConstants.USER_ID, employeeId);
    if (!YFCUtils.equals(problemWithCarton, GCConstants.YES)) {
      gcShpmntActivity.setAttribute(GCConstants.USER_ACTION, GCConstants.RECEIVE_USER_ACTION);
      gcShpmntActivity.setAttribute(GCConstants.AUDIT_RSNCODE, GCConstants.RECEIVE_AUDIT_CODE);
    } else {
      gcShpmntActivity.setAttribute(GCConstants.USER_ACTION, GCConstants.PROBLEM_USER_ACTION);
      gcShpmntActivity.setAttribute(GCConstants.AUDIT_RSNCODE, GCConstants.PROBLEM_AUDIT_CODE);
    }
    GCCommonUtil.invokeService(env, GCConstants.GC_INSERT_SHPMNT_ACTIVITY, gcShpmntActvIndoc.getDocument());
  }
}
