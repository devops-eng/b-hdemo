package com.gc.exstore.api.receipt;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCShipmentReceiptDetailManager {
  // initialise logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCShipmentReceiptDetailManager.class.getName());


  /**
   *
   * This method invokes the getShipmentList API to fetch the container details which can be
   * received or marked as problematic.
   *
   * @param env
   * @param inDoc
   * @return Doc
   *
   *
   *
   */

  public Document getReceiptDetail(YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("GCShipmentReceiptDetailManager.getReceiptDetail");
    LOGGER.verbose("GCShipmentReceiptDetailManager.getReceiptDetail--Begin");
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("inputDoc : " + inputDoc.toString());
    }
    String trackingNo = null;
    String containerNo = null;
    String orderHeaderKey = null;
    // prepare output document
    YFCDocument receiptDetailDoc = YFCDocument.createDocument(GCConstants.CONTAINER_DETAILS);
    YFCElement eleOutput = receiptDetailDoc.getDocumentElement();
    YFCElement eleInput = inputDoc.getDocumentElement();

    // fetch attributes from input document
    String shipmentKey = eleInput.getAttribute(GCConstants.SHIPMENT_KEY);
    String shipmentContainerKeyIp = eleInput.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);


    // Call getOrderList API


    // prepare input for getShipmentList API call
    YFCDocument getShipmentListIpDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement eleShipmentIp = getShipmentListIpDoc.getDocumentElement();
    eleShipmentIp.setAttribute(GCConstants.SHIPMENT_KEY, shipmentKey);
    YFCDocument getShipmentListOpTemp =
        YFCDocument.getDocumentFor(GCConstants.GET_SHIPMENT_LIST_OP_TMP_FOR_RECEIPT_DTLS);

    // Invoke getShipmentList API
    YFCDocument getShpmentListOp =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, getShipmentListIpDoc, getShipmentListOpTemp);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getShipmentList Output : " + getShpmentListOp.toString());
    }
    YFCElement eleroot = getShpmentListOp.getDocumentElement();
    YFCElement eleShipment = eleroot.getElementsByTagName(GCConstants.SHIPMENT).item(0);
    YFCElement eleContainers = eleShipment.getChildElement(GCConstants.CONTAINERS);
    YFCNodeList<YFCElement> nlContainer = eleContainers.getElementsByTagName(GCConstants.CONTAINER);
    List<String> listItemDetails = new ArrayList<String>();
    Set<String> parentKeySet = new HashSet<String>();
    getItemDetailsListAndParentKeySet(nlContainer, eleOutput, listItemDetails, parentKeySet, shipmentContainerKeyIp,
        trackingNo, containerNo);
    YFCElement eleShipmentLines = eleShipment.getChildElement(GCConstants.SHIPMENT_LINES);
    YFCNodeList<YFCElement> nlShipmentLine = eleShipmentLines.getElementsByTagName(GCConstants.SHIPMENT_LINE);
    YFCElement eleShipmentLine = nlShipmentLine.item(0);
    String docType = eleShipmentLine.getAttribute(GCConstants.DOCUMENT_TYPE);
    if (YFCUtils.equals(GCConstants.ZERO_ZERO_ZERO_FIVE, docType)) {
      YFCElement eleOrderLine = eleShipmentLine.getChildElement(GCConstants.ORDER_LINE);
      orderHeaderKey = eleOrderLine.getAttribute(GCConstants.CHAINED_FROM_ORDER_HEADER_KEY);
      eleOutput.setAttribute(GCConstants.PURCHASE_HEADER_KEY,
          eleShipmentLine.getAttribute(GCConstants.ORDER_HEADER_KEY));
      eleOutput.setAttribute(GCConstants.PURCHASE_ORDER_NO, eleShipmentLine.getAttribute(GCConstants.ORDER_NO));
    } else {
      orderHeaderKey = eleShipmentLine.getAttribute(GCConstants.ORDER_HEADER_KEY);
    }
    YFCDocument getOrderListTemp = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_OUT_TEMP);
    YFCDocument ordListOpDoc = GCCommonUtil.invokeGetOrderListAPI(env, orderHeaderKey, getOrderListTemp);
    YFCElement eleOrderList = ordListOpDoc.getDocumentElement();
    YFCElement eleOrder = eleOrderList.getChildElement(GCConstants.ORDER);
    // call method to stamp header level attributes
    stampHeaderAttributes(eleShipment, eleOutput, env, eleOrder);
    Map<String, List<String>> setParentMap = getSetParentDetailsMap(nlShipmentLine, listItemDetails, parentKeySet);
    Map<String, List<String>> itemMap = getItemDetailsMap(listItemDetails, env);

    // set container detail attributes
    setContainerDetails(env, eleShipment, eleOutput, shipmentContainerKeyIp, itemMap, setParentMap, eleOrder);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("receiptDetailDoc : " + receiptDetailDoc.toString());
    }

    LOGGER.verbose("GCShipmentReceiptDetailManager.getReceiptDetail--End");
    LOGGER.endTimer("GCShipmentReceiptDetailManager.getReceiptDetail");
    return receiptDetailDoc.getDocument();
  }






  /**
   *
   * This method sets the container detail attributes to be displayed in receipt detail page
   *
   * @param eleShipment
   * @param eleOutput
   * @param shipmentContainerKeyIp
   * @param itemMap
   * @param setParentMap
   * @return
   *
   *
   */
  private void setContainerDetails(YFSEnvironment env, YFCElement eleShipment, YFCElement eleOutput,
      String shipmentContainerKeyIp, Map<String, List<String>> itemMap, Map<String, List<String>> setParentMap,
      YFCElement eleOrder) {
    LOGGER.beginTimer("GCShipmentReceiptDetailManager.setContainerDetails");
    LOGGER.verbose("GCShipmentReceiptDetailManager.setContainerDetails--Begin");
    YFCElement eleContainers = eleShipment.getChildElement(GCConstants.CONTAINERS);
    YFCNodeList<YFCElement> nlContainer = eleContainers.getElementsByTagName(GCConstants.CONTAINER);
    for (YFCElement eleContainer : nlContainer) {
      String shipmentContainerKey = eleContainer.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
      boolean bundleCompTraversed = false;
      // check if the container is the same as provided in the input by comparing the
      // shipmentcontainerkey
      if (YFCUtils.equals(shipmentContainerKey, shipmentContainerKeyIp)) {
        YFCElement eleContainerDetails = eleContainer.getChildElement(GCConstants.CONTAINER_DETAILS);
        YFCNodeList<YFCElement> nlContainerDetail =
            eleContainerDetails.getElementsByTagName(GCConstants.CONTAINER_DETAIL);
        Map<String, YFCElement> setMap = new HashMap<String, YFCElement>();
        YFCElement eleContainerDetailFirst = nlContainerDetail.item(0);
        String orderReleaseKey = eleContainerDetailFirst.getAttribute(GCConstants.ORDER_RELEASE_KEY);
        eleOutput.setAttribute(GCConstants.ORDER_RELEASE_KEY, orderReleaseKey);
        // Iterate over container details to add container detail for each line item in container
        for (YFCElement eleContainerDetail : nlContainerDetail) {
          YFCElement eleShipmentLine = eleContainerDetail.getChildElement(GCConstants.SHIPMENT_LINE);
          String parentShipmentLineKey = eleShipmentLine.getAttribute(GCConstants.PARENT_SHPMNTLINE_KEY);
          String shipmentLineKey = eleShipmentLine.getAttribute(GCConstants.SHIPMENT_LINE_KEY);
          String itemDescription = eleShipmentLine.getAttribute(GCConstants.ITEM_DESCRIPTION);
          String lineQty = eleShipmentLine.getAttribute(GCConstants.QUANTITY);

          String quantity = eleContainerDetail.getAttribute(GCConstants.QUANTITY);
          String itemId = eleContainerDetail.getAttribute(GCConstants.ITEM_ID);
          String uom = eleContainerDetail.getAttribute(GCConstants.UNIT_OF_MEASURE);
          String itemDetail = itemId + GCConstants.HASH + uom;
          List<String> listItemAttri = itemMap.get(itemDetail);
          String setCode = listItemAttri.get(0);
          String posItemId = listItemAttri.get(1);
          String brandName = listItemAttri.get(2);
          String shortDesc = listItemAttri.get(3);
          String imgUrl = listItemAttri.get(4);
          YFCElement eleOrderLine = eleContainerDetail.getChildElement(GCConstants.ORDER_LINE);
          String chainedFromOrderLineKey = eleOrderLine.getAttribute(GCConstants.CHAINED_FROM_ORDER_LINE_KEY);
          YFCElement eleExtnOrdLine = eleOrderLine.getChildElement(GCConstants.EXTN);
          String isWarrantyItem = eleExtnOrdLine.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
          String orderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);

          // check if it is a warranty item
          if (YFCUtils.equals(GCConstants.YES, isWarrantyItem)) {
            YFCElement eleContainerDetailOp = eleOutput.createChild(GCConstants.CONTAINER_DETAIL);
            YFCElement eleWarrantyComp = eleContainerDetailOp.createChild(GCXmlLiterals.WARRANTY_COMPONENT);
            eleWarrantyComp.setAttribute(GCConstants.ORDER_LINE_KEY,
                eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
            eleWarrantyComp.setAttribute(GCConstants.QUANTITY, quantity);
            eleWarrantyComp.setAttribute(GCConstants.ORDER_RELEASE_KEY, orderReleaseKey);
            eleWarrantyComp.setAttribute(GCConstants.SHIPMENT_LINE_KEY, shipmentLineKey);
            eleContainerDetailOp.importNode(eleWarrantyComp);
          } else if (YFCUtils.equals(GCConstants.TWO, setCode)
              && YFCUtils.equals(GCConstants.SET, eleContainerDetail.getAttribute(GCConstants.PRODUCT_CLASS))) {
            // If it is a retail set item , check if its parent details have already been added
            if (!setMap.containsKey(parentShipmentLineKey)) {
              YFCElement eleContainerDetailOp = eleOutput.createChild(GCConstants.CONTAINER_DETAIL);
              List<String> parentAttributes = setParentMap.get(parentShipmentLineKey);
              String parentQuantity = parentAttributes.get(0);
              String parentItemDet = parentAttributes.get(2);
              eleContainerDetailOp.setAttribute(GCConstants.ORDER_LINE_KEY, parentAttributes.get(1));
              eleContainerDetailOp.setAttribute(GCConstants.ITEM_DESCRIPTION, parentAttributes.get(3));
              List<String> listParentItemAttri = itemMap.get(parentItemDet);
              eleContainerDetailOp.setAttribute(GCConstants.ITEM_ID, listParentItemAttri.get(1));
              eleContainerDetailOp.setAttribute(GCConstants.BRAND_NAME, listParentItemAttri.get(2));
              eleContainerDetailOp.setAttribute(GCConstants.SHORT_DESCRIPTION, listParentItemAttri.get(3));
              eleContainerDetailOp.setAttribute(GCConstants.IMAGE_URL, listParentItemAttri.get(4));
              setOrderLineAttributes(eleOrder, parentAttributes.get(1), eleContainerDetailOp);
              Double temp = Double.parseDouble(quantity) * Double.parseDouble(parentQuantity);
              Double actualQty = temp / Double.parseDouble(lineQty);
              eleContainerDetailOp.setAttribute(GCConstants.QUANTITY, actualQty.toString());
              addSetComponents(eleContainerDetailOp, parentShipmentLineKey, eleContainerDetail);
              setMap.put(parentShipmentLineKey, eleContainerDetailOp);
            } else {
              YFCElement eleParentContainerDetail = setMap.get(parentShipmentLineKey);
              addSetComponents(eleParentContainerDetail, parentShipmentLineKey, eleContainerDetail);
            }

          } else {
            YFCElement eleContainerDetailOp = eleOutput.createChild(GCConstants.CONTAINER_DETAIL);
            eleContainerDetailOp.setAttribute(GCConstants.ORDER_RELEASE_KEY, orderReleaseKey);
            eleContainerDetailOp.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey);
            eleContainerDetailOp.setAttribute(GCConstants.ITEM_ID, posItemId);
            eleContainerDetailOp.setAttribute(GCConstants.BRAND_NAME, brandName);
            eleContainerDetailOp.setAttribute(GCConstants.SHORT_DESCRIPTION, shortDesc);
            eleContainerDetailOp.setAttribute(GCConstants.IMAGE_URL, imgUrl);
            eleContainerDetailOp.setAttribute(GCConstants.QUANTITY, quantity);
            eleContainerDetailOp.setAttribute(GCConstants.SHIPMENT_LINE_KEY, shipmentLineKey);
            eleContainerDetailOp.setAttribute(GCConstants.ITEM_DESCRIPTION, itemDescription);
            if (!YFCCommon.isVoid(chainedFromOrderLineKey)) {
              LOGGER.verbose("ChaineFromOrderLineKey" + chainedFromOrderLineKey);
              setOrderLineAttributes(eleOrder, chainedFromOrderLineKey, eleContainerDetailOp);
            } else {
              setOrderLineAttributes(eleOrder, orderLineKey, eleContainerDetailOp);
            }

            if (!YFCCommon.isVoid(parentShipmentLineKey) && !bundleCompTraversed) {
              eleOutput.setAttribute(GCConstants.IS_BUNDLE_COMPONENT, GCConstants.YES);
              eleOutput.setAttribute(GCConstants.PARENT_SHPMNTLINE_KEY, parentShipmentLineKey);
              eleContainerDetailOp.setAttribute(GCConstants.PARENT_SHPMNTLINE_KEY, parentShipmentLineKey);
              YFCElement eleShipmentLines = eleShipment.getChildElement(GCConstants.SHIPMENT_LINES);
              YFCNodeList<YFCElement> nlShipmentLine = eleShipmentLines.getElementsByTagName(GCConstants.SHIPMENT_LINE);
              for (YFCElement eleShipmentLineParent : nlShipmentLine) {
                setShpmntLnParentAttr(env, eleOutput, eleOrder, parentShipmentLineKey, eleShipmentLineParent);
              }

            }
          }
        }
        break;
      }
    }

    LOGGER.verbose("GCShipmentReceiptDetailManager.setContainerDetails--End");
    LOGGER.endTimer("GCShipmentReceiptDetailManager.setContainerDetails");
  }



  private void setShpmntLnParentAttr(YFSEnvironment env, YFCElement eleOutput, YFCElement eleOrder,
      String parentShipmentLineKey, YFCElement eleShipmentLineParent) {
    if (YFCUtils.equals(eleShipmentLineParent.getAttribute(GCConstants.SHIPMENT_LINE_KEY), parentShipmentLineKey)) {
      String parentOrderLineKey = eleShipmentLineParent.getAttribute(GCConstants.ORDER_LINE_KEY);
      setOrderLineAttributes(eleOrder, parentOrderLineKey, eleOutput);
      eleOutput.setAttribute(GCXmlLiterals.BUNDLE_PARENT_ITEM_DESCRIPTION,
          eleShipmentLineParent.getAttribute(GCConstants.ITEM_DESCRIPTION));
      eleOutput.setAttribute(GCXmlLiterals.BUNDLE_PARENT_QUANTITY,
          eleShipmentLineParent.getAttribute(GCConstants.QUANTITY));
      YFCDocument getItemListTemp = YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LIST_RECEIPT_TMP);
      String bundleItemId = eleShipmentLineParent.getAttribute(GCConstants.ITEM_ID);
      YFCDocument getItemListOp =
          GCCommonUtil.getItemList(env, bundleItemId, eleShipmentLineParent.getAttribute(GCConstants.UNIT_OF_MEASURE),
              GCConstants.GCI, true, getItemListTemp);
      YFCElement eleRoot = getItemListOp.getDocumentElement();
      YFCElement eleItem = eleRoot.getElementsByTagName(GCConstants.ITEM).item(0);
      YFCElement eleExtnItem = eleItem.getChildElement(GCConstants.EXTN);
      String itemUniqueId = eleExtnItem.getAttribute(GCConstants.EXTN_POSITEM_ID);
      if (!YFCCommon.isVoid(itemUniqueId)) {
        eleOutput.setAttribute(GCXmlLiterals.BUNDLE_ITEM_ID, itemUniqueId);
      } else {
        eleOutput.setAttribute(GCXmlLiterals.BUNDLE_ITEM_ID, bundleItemId);
      }
      eleOutput.setAttribute(GCConstants.BUNDLE_PARENT_ORDERLINEKEY, parentOrderLineKey);
    }
  }

  /**
   *
   * This method fetches the ItemId and UnitOfMeasure combination from the listItemDetails and
   * invokes getItemList for each combination to fetch the item attributes to be displayed for each
   * container detail on receipt detail page
   *
   * @param env
   * @param listItemDetails
   * @return itemMap
   *
   *
   */
  private Map<String, List<String>> getItemDetailsMap(List<String> listItemDetails, YFSEnvironment env) {
    LOGGER.beginTimer("GCShipmentReceiptDetailManager.getItemDetailsMap");
    LOGGER.verbose("GCShipmentReceiptDetailManager.getItemDetailsMap--Begin");
    Map<String, List<String>> itemMap = new HashMap<String, List<String>>();



    for (String itemDetail : listItemDetails) {
      String[] item = itemDetail.split(GCConstants.HASH);
      List<String> listItemAttributes = new ArrayList<String>();
      YFCDocument getItemListTemp = YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LIST_RECEIPT_TMP);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("ItemID : " + item[0]);
        LOGGER.verbose("UOM : " + item[1]);
      }
      YFCDocument getItemListOp =
          GCCommonUtil.getItemList(env, item[0], item[1], GCConstants.GCI, true, getItemListTemp);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("getItemList Output : " + getItemListOp.toString());
      }
      YFCElement eleRoot = getItemListOp.getDocumentElement();
      YFCElement eleItem = eleRoot.getElementsByTagName(GCConstants.ITEM).item(0);
      YFCElement eleExtnItem = eleItem.getChildElement(GCConstants.EXTN);
      String setCode = eleExtnItem.getAttribute(GCConstants.EXTN_SET_CODE);
      String posItemId = eleExtnItem.getAttribute(GCConstants.EXTN_POSITEM_ID);
      String brandName = eleExtnItem.getAttribute(GCConstants.EXTN_BRAND_NAME);
      YFCElement elePrimaryInfo = eleItem.getChildElement(GCConstants.PRIMARY_INFORMATION);
      String shortDesc = elePrimaryInfo.getAttribute(GCConstants.SHORT_DESCRIPTION);
      String imageId = elePrimaryInfo.getAttribute(GCConstants.IMAGE_ID);
      String imageLocation = elePrimaryInfo.getAttribute(GCConstants.IMAGE_LOCATION);
      // Add Item attributes to the list
      listItemAttributes.add(setCode);
      if (!YFCCommon.isVoid(posItemId)) {
        listItemAttributes.add(posItemId);
      } else {
        listItemAttributes.add(item[0]);
      }
      listItemAttributes.add(brandName);
      listItemAttributes.add(shortDesc);
      listItemAttributes.add(imageLocation + "/" + imageId);
      itemMap.put(itemDetail, listItemAttributes);
    }

    LOGGER.verbose("GCShipmentReceiptDetailManager.getItemDetailsMap--End");
    LOGGER.endTimer("GCShipmentReceiptDetailManager.getItemDetailsMap");
    return itemMap;
  }



  /**
   *
   * This method prepares a Map with parentShipmentLiney as key and list of attributes of set parent
   * as value
   *
   * @param nlShipmentLine
   * @param listItemDetails
   * @param parentKeySet
   * @return setParentMap
   *
   *
   */
  private Map<String, List<String>> getSetParentDetailsMap(YFCNodeList<YFCElement> nlShipmentLine,
      List<String> listItemDetails, Set<String> parentKeySet) {
    LOGGER.beginTimer("GCShipmentReceiptDetailManager.getSetParentDetailsMap");
    LOGGER.verbose("GCShipmentReceiptDetailManager.getSetParentDetailsMap--Begin");
    Map<String, List<String>> setParentMap = new HashMap<String, List<String>>();
    for (YFCElement eleShipmentLine : nlShipmentLine) {
      String shipmentLineKey = eleShipmentLine.getAttribute(GCConstants.SHIPMENT_LINE_KEY);
      if (parentKeySet.contains(shipmentLineKey)) {
        String quantity = eleShipmentLine.getAttribute(GCConstants.QUANTITY);
        String orderLineKey = eleShipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY);
        String itemId = eleShipmentLine.getAttribute(GCConstants.ITEM_ID);
        String uom = eleShipmentLine.getAttribute(GCConstants.UNIT_OF_MEASURE);
        String itemDesc = eleShipmentLine.getAttribute(GCConstants.ITEM_DESCRIPTION);
        String itemDetail = itemId + GCConstants.HASH + uom;
        listItemDetails.add(itemDetail);
        List<String> listParentDetails = new ArrayList<String>();
        listParentDetails.add(quantity);
        listParentDetails.add(orderLineKey);
        listParentDetails.add(itemDetail);
        listParentDetails.add(itemDesc);
        setParentMap.put(shipmentLineKey, listParentDetails);
      }
    }

    LOGGER.verbose("GCShipmentReceiptDetailManager.getSetParentDetailsMap--End");
    LOGGER.endTimer("GCShipmentReceiptDetailManager.getSetParentDetailsMap");
    return setParentMap;
  }


  /**
   *
   * This method prepares a list of ItemID#UOM and a set of parentShipmentLineKey as value
   *
   * @param nlContainer
   * @param eleOutput
   * @param listItemDetails
   * @param parentKeySet
   * @param shipmentContainerKeyIp
   * @param trackingNo
   * @param containerNo
   * @return
   */
  private void getItemDetailsListAndParentKeySet(YFCNodeList<YFCElement> nlContainer, YFCElement eleOutput,
      List<String> listItemDetails, Set<String> parentKeySet, String shipmentContainerKeyIp, String trackingNo,
      String containerNo) {
    LOGGER.beginTimer("GCShipmentReceiptDetailManager.getItemDetailsListAndParentKeySet");
    LOGGER.verbose("GCShipmentReceiptDetailManager.getItemDetailsListAndParentKeySet--Begin");
    for (YFCElement eleContainer : nlContainer) {
      String shipmentContainerKey = eleContainer.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
      if (YFCUtils.equals(shipmentContainerKey, shipmentContainerKeyIp)) {
        containerNo = eleContainer.getAttribute(GCConstants.CONTAINER_NO);
        trackingNo = eleContainer.getAttribute(GCConstants.TRACKING_NO);
        YFCElement eleExtnContainer = eleContainer.getChildElement(GCConstants.EXTN);
        String multiBoxId = eleExtnContainer.getAttribute(GCConstants.EXTN_MULTI_BOX_GROUP_ID);
        String multiBoxDesc = eleExtnContainer.getAttribute(GCConstants.EXTN_MULTIBOX_DESC);
        String containerStatus = eleExtnContainer.getAttribute(GCConstants.EXTN_CONTAINER_STATUS);
        eleOutput.setAttribute(GCConstants.EXTN_MULTI_BOX_GROUP_ID, multiBoxId);
        eleOutput.setAttribute(GCConstants.EXTN_MULTIBOX_DESC, multiBoxDesc);
        eleOutput.setAttribute(GCConstants.CONTAINER_NO, containerNo);
        eleOutput.setAttribute(GCConstants.TRACKING_NO, trackingNo);
        eleOutput.setAttribute(GCConstants.STATUS, containerStatus);
        YFCElement eleContainerDetails = eleContainer.getChildElement(GCConstants.CONTAINER_DETAILS);
        YFCNodeList<YFCElement> nlContainerDetail =
            eleContainerDetails.getElementsByTagName(GCConstants.CONTAINER_DETAIL);
        for (YFCElement eleContainerDetail : nlContainerDetail) {
          String itemId = eleContainerDetail.getAttribute(GCConstants.ITEM_ID);
          String productClass = eleContainerDetail.getAttribute(GCConstants.PRODUCT_CLASS);
          String uom = eleContainerDetail.getAttribute(GCConstants.UNIT_OF_MEASURE);
          String itemDetail = itemId + GCConstants.HASH + uom;
          listItemDetails.add(itemDetail);
          YFCElement eleShipmentLine = eleContainerDetail.getChildElement(GCConstants.SHIPMENT_LINE);
          String parentShipmentLinekey = eleShipmentLine.getAttribute(GCConstants.PARENT_SHPMNTLINE_KEY);
          if (!YFCCommon.isVoid(parentShipmentLinekey) && YFCUtils.equals(productClass, GCConstants.SET)) {
            parentKeySet.add(parentShipmentLinekey);
          }
        }
        break;
      }
    }

    LOGGER.verbose("GCShipmentReceiptDetailManager.getItemDetailsListAndParentKeySet--End");
    LOGGER.endTimer("GCShipmentReceiptDetailManager.getItemDetailsListAndParentKeySet");
  }


  /**
   *
   * This method invokes getOrderCommissionList API to get SPOAssociate
   *
   * @param orderHeaderKey
   * @param env
   * @return
   */
  private String getSPOAssociate(String orderHeaderKey, YFSEnvironment env) {
    LOGGER.beginTimer("GCShipmentReceiptDetailManager.getSPOAssociate");
    LOGGER.verbose("GCShipmentReceiptDetailManager.getSPOAssociate--Begin");
    String spoAssociate = null;
    YFCDocument commisionListIp = YFCDocument.createDocument(GCConstants.GC_ORDER_COMMISSION);
    YFCElement rootEle = commisionListIp.getDocumentElement();
    rootEle.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
    YFCDocument commListOpDoc =
        GCCommonUtil.invokeService(env, GCConstants.GET_ORDER_COMMISSION_LIST_SERVICE, commisionListIp);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getCommissionListOpDoc : " + commListOpDoc.toString());
    }
    YFCElement commListEle = commListOpDoc.getDocumentElement();
    YFCElement eleCommission = commListEle.getElementsByTagName(GCConstants.GC_ORDER_COMMISSION).item(0);
    if (!YFCCommon.isVoid(eleCommission)) {
      spoAssociate = eleCommission.getAttribute(GCConstants.ORDER_USER_NAME);
    }

    LOGGER.verbose("GCShipmentReceiptDetailManager.getSPOAssociate--End");
    LOGGER.endTimer("GCShipmentReceiptDetailManager.getSPOAssociate");
    return spoAssociate;
  }


  /**
   *
   * This method adds the set components to parent conatiner detail
   *
   * @param eleContainerDetailOp
   * @param eleContainerDetail
   * @param parentShipmentLineKey
   * @return
   */
  public void addSetComponents(YFCElement eleContainerDetailOp, String parentShipmentLineKey,
      YFCElement eleContainerDetail) {
    LOGGER.beginTimer("GCShipmentReceiptDetailManager.addSetComponents");
    LOGGER.verbose("GCShipmentReceiptDetailManager.addSetComponents--Begin");


    YFCElement eleOrderLineComp = eleContainerDetail.getChildElement(GCConstants.ORDER_LINE);
    String orderLineKey = eleOrderLineComp.getAttribute(GCConstants.ORDER_LINE_KEY);
    YFCElement eleShipmentLineComp = eleContainerDetail.getChildElement(GCConstants.SHIPMENT_LINE);
    String orderReleaseKey = eleContainerDetail.getAttribute(GCConstants.ORDER_RELEASE_KEY);
    String sParentShipmentLineKeyComp = eleShipmentLineComp.getAttribute(GCConstants.PARENT_SHPMNTLINE_KEY);
    if (YFCUtils.equals(sParentShipmentLineKeyComp, parentShipmentLineKey)) {
      String sCompQty = eleContainerDetail.getAttribute(GCConstants.QUANTITY);
      YFCElement eleComponent = eleContainerDetailOp.createChild(GCXmlLiterals.SET_COMPONENT);
      eleComponent.setAttribute(GCConstants.QUANTITY, sCompQty);
      eleComponent.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey);
      eleComponent.setAttribute(GCConstants.ORDER_RELEASE_KEY, orderReleaseKey);
      eleContainerDetailOp.importNode(eleComponent);

    }

    LOGGER.verbose("GCShipmentReceiptDetailManager.addSetComponents--End");
    LOGGER.endTimer("GCShipmentReceiptDetailManager.addSetComponents");
  }

  /**
   *
   * This method invoke getCommonCOdeList API to fetch LevelOfService without spaces
   *
   * @param env
   * @param levelOfService
   * @return
   */

  public static String callCommonCodeList(YFSEnvironment env, String levelOfService) {

    LOGGER.beginTimer("GCShipmentReceiptDetailManager.callCommonCodeList");
    LOGGER.verbose("GCShipmentReceiptDetailManager.callCommonCodeList -- START");


    // calling getCommonCodeList API
    final YFCDocument outXMLYfc =
        GCCommonUtil.getCommonCodeListByTypeAndValue(env, GCConstants.GC_EXSTORE_LOS, GCConstants.GC, levelOfService);


    YFCElement eleOutXml = outXMLYfc.getDocumentElement();
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("outXML : " + outXMLYfc.getString());
    }
    final YFCElement eleCommonCode = eleOutXml.getElementsByTagName(GCConstants.COMMON_CODE).item(0);
    String levelOfServiceDesc = GCConstants.BLANK;
    if (!YFCCommon.isVoid(eleCommonCode)) {
      levelOfServiceDesc = eleCommonCode.getAttribute(GCConstants.CODE_SHORT_DESCRIPTION);
    }
    LOGGER.verbose("GCShipmentReceiptDetailManager.callCommonCodeList -- END");
    LOGGER.endTimer("GCShipmentReceiptDetailManager.callCommonCodeList");
    return levelOfServiceDesc;
  }


  /**
   *
   * This method stamps header level attributes at container details
   *
   * @param eleShipment
   * @param eleShipmentLine
   * @param eleOutput
   * @param env
   * @return
   */
  private void stampHeaderAttributes(YFCElement eleShipment, YFCElement eleOutput,
      YFSEnvironment env, YFCElement eleOrder) {
    String orderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
    String orderNo = eleOrder.getAttribute(GCConstants.ORDER_NO);
    String userName = getSPOAssociate(orderHeaderKey, env);
    String shipDate = eleShipment.getAttribute(GCConstants.ACTUAL_SHIPMENT_DATE);
    String shipmentNo = eleShipment.getAttribute(GCConstants.SHIPMENT_NO);
    String shipNode = eleShipment.getAttribute(GCConstants.SHIP_NODE);
    YFCElement eleExtnOrder = eleOrder.getChildElement(GCConstants.EXTN);
    String orderingStore = eleExtnOrder.getAttribute(GCConstants.EXTN_ORDERING_STORE);
    YFCElement elePaymentMethods = eleOrder.getChildElement(GCConstants.PAYMENT_METHODS);
    String levelOfServiceDesc = callCommonCodeList(env, eleOrder.getAttribute(GCConstants.LEVEL_OF_SERVICE));
    YFCElement eleExtnShipment = eleShipment.getChildElement(GCConstants.EXTN);
    String fulfillmentType = eleExtnShipment.getAttribute(GCConstants.EXTN_FULFILLMENT_TYPE);
    String orderDate = eleExtnShipment.getAttribute(GCConstants.EXTN_ORDER_DATE);
    YFCElement eleToAddress = eleShipment.getChildElement(GCXmlLiterals.TO_ADDRESS);
    YFCElement eleBillTo = eleShipment.getChildElement(GCXmlLiterals.BILL_TO_ADDRESS);
    eleOutput.setAttribute(GCConstants.FIRST_NAME, eleBillTo.getAttribute(GCConstants.FIRST_NAME));
    eleOutput.setAttribute(GCConstants.LAST_NAME, eleBillTo.getAttribute(GCConstants.LAST_NAME));
    eleOutput.setAttribute(GCConstants.PHONE_NO, eleBillTo.getAttribute(GCConstants.DAY_PHONE));
    eleOutput.setAttribute(GCConstants.EMAIL_ID, eleBillTo.getAttribute(GCConstants.EMAIL_ID));
    eleOutput.importNode(eleToAddress);
    eleOutput.importNode(eleBillTo);
    eleOutput.importNode(elePaymentMethods);
    eleOutput.setAttribute(GCConstants.BILL_TO_ID, eleOrder.getAttribute(GCConstants.BILL_TO_ID));
    eleOutput.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
    eleOutput.setAttribute(GCConstants.ORDER_USER_NAME, userName);
    eleOutput.setAttribute(GCConstants.ORDERING_STORE, orderingStore);
    eleOutput.setAttribute(GCConstants.FULFILLMENT_TYPE, fulfillmentType);
    eleOutput.setAttribute(GCConstants.ORDER_DATE, orderDate);
    eleOutput.setAttribute(GCConstants.SHIP_DATE, shipDate);
    eleOutput.setAttribute(GCConstants.SHIPMENT_NO, shipmentNo);
    eleOutput.setAttribute(GCConstants.DELIVERY_METHOD, levelOfServiceDesc);
    eleOutput.setAttribute(GCConstants.SHIP_NODE, shipNode);
    eleOutput.setAttribute(GCConstants.ORDER_NO, orderNo);
  }

  /**
   *
   * This method takes orderlinekey and retrieves price and serial no
   *
   * @param eleOrder
   * @param orderLineKey
   * @return
   */
  private void setOrderLineAttributes(YFCElement eleOrder, String orderLineKey, YFCElement eleContainerElement) {
    YFCElement eleOrderLines = eleOrder.getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> nlOrderLine = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
    for (YFCElement eleOrderLine : nlOrderLine) {
      if (YFCUtils.equals(eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY), orderLineKey)) {
        LOGGER.verbose("Line Charges are calculated from this OrderLine" + orderLineKey);
        YFCElement eleExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
        eleContainerElement.setAttribute(GCConstants.UNIT_PRICE, calculateUnitPrice(eleOrderLine).toString());
        eleContainerElement.setAttribute(GCConstants.SERIAL_NUMBER,
            eleExtn.getAttribute(GCConstants.EXTN_ACTUAL_SERIAL_NO));
        break;
      }
    }
  }

  // This method calculates the unit price at order line to be displayed in customer pick up slip
  private BigDecimal calculateUnitPrice(YFCElement eleOrderLine) {
    LOGGER.beginTimer("GCManageCustomerPickup.calculateUnitPrice");
    LOGGER.verbose("GCManageCustomerPickup.calculateUnitPrice--Begin");

    final YFCElement eleLinePriceInfo = eleOrderLine.getChildElement(GCConstants.LINE_PRICE_INFO);
    BigDecimal totalPromotion = new BigDecimal(GCConstants.ZERO_AMOUNT);
    BigDecimal unitPromotion = new BigDecimal(GCConstants.ZERO_AMOUNT);
    final YFCElement eleLineCharges = eleOrderLine.getChildElement(GCConstants.LINE_CHARGES);
    if (!YFCCommon.isVoid(eleLineCharges)) {
      final YFCNodeList<YFCElement> nlLineCharge = eleLineCharges.getElementsByTagName(GCConstants.LINE_CHARGE);
      final int lengthLineChg = nlLineCharge.getLength();
      for (int i = 0; i < lengthLineChg; i++) {
        final YFCElement eleLineChg = nlLineCharge.item(i);
        if (YFCUtils.equals(GCConstants.PROMOTION, eleLineChg.getAttribute(GCConstants.CHARGE_CATEGORY))) {
          String chargePerLine = eleLineChg.getAttribute(GCConstants.CHARGE_PER_LINE);
          BigDecimal chgAmt = new BigDecimal(chargePerLine);
          totalPromotion = totalPromotion.add(chgAmt);
        }
      }
    }
    BigDecimal quantity = new BigDecimal(eleOrderLine.getAttribute(GCConstants.ORDERED_QTY));
    if (quantity.intValue() != 0) {
      unitPromotion = totalPromotion.divide(quantity, 1);
    }
    BigDecimal unitPrice = new BigDecimal(eleLinePriceInfo.getAttribute(GCConstants.UNIT_PRICE));
    return unitPrice.subtract(unitPromotion);
  }
}