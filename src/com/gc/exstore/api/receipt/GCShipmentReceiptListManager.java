package com.gc.exstore.api.receipt;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * This class gets invoked on loading the Pending Receipt list screen, a custom service will be
 * invoked to get the list of all cartons in Pending Receipt or Problem with Carton with
 * ExtnPickupStoreID same as the logged in store.
 *
 * @author bhawana.jethuri
 *
 */

public class GCShipmentReceiptListManager {

  // initialise logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCShipmentReceiptListManager.class.getName());


  /**
   *
   * This method invokes the getShipmentList API with the created input document from inDoc and
   * returns the list of cartons in Pending Receipt and Problem With Carton.
   *
   * @param env
   * @param inDoc
   * @return Doc
   *
   *
   *
   */

  public Document getReceiptList(YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("GCShipmentReceiptListManager.getReceiptList");
    LOGGER.verbose("GCShipmentReceiptListManager.getReceiptList--Begin");
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("inputDoc : " + inputDoc.toString());
    }
    String trackingNo = null;
    String pickUpStoreID = null;
    Document receiptListOpDoc = null;

    final YFCElement eleRoot = inputDoc.getDocumentElement();
    trackingNo = eleRoot.getAttribute(GCConstants.TRACKING_NO).toUpperCase();
    pickUpStoreID = eleRoot.getAttribute(GCConstants.EXTN_PICK_UP_STORE_ID);

    // Prepare input template for getShipmentList API
    YFCDocument getShipmentListIp = YFCDocument.getDocumentFor(getShipmentListInput(trackingNo, pickUpStoreID));

    // Prepare output template for getShipmentList API
    YFCDocument getShipmentListTemp = YFCDocument.getDocumentFor(GCConstants.GET_SHIPMENT_LIST_OP_TMP_FOR_RECEIPT_LIST);

    // Invoke getShipmentListAPI
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input for getShipmentList API: " + getShipmentListIp.toString());
    }
    YFCDocument getShipmentListOp =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, getShipmentListIp, getShipmentListTemp);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Output of getShipmentList API: " + getShipmentListOp.toString());
    }
    YFCElement shpOpEle = getShipmentListOp.getDocumentElement();

    if (!YFCCommon.isVoid(trackingNo)) {
      receiptListOpDoc = getListWithTrackingNo(env, trackingNo, pickUpStoreID, shpOpEle);
    } else {
      receiptListOpDoc = getListWithStoreID(env, shpOpEle);
    }

    LOGGER.verbose("GCShipmentReceiptListManager.getReceiptList--End");
    LOGGER.endTimer("GCShipmentReceiptListManager.getReceiptList");
    return receiptListOpDoc;
  }

  /**
   *
   * This method provides the available containers if Tracking No is provided in UI input
   *
   * @param trackingNo
   * @param nlShipment
   * @param pickUpStoreID
   * @param shpOpEle
   * @return Doc
   *
   *
   *
   */

  private Document getListWithTrackingNo(YFSEnvironment env, String trackingNo, String pickUpStoreID,
      YFCElement shpOpEle) {
    LOGGER.beginTimer("GCShipmentReceiptListManager.getListWithTrackingNo");
    LOGGER.verbose("GCShipmentReceiptListManager.getListWithTrackingNo--Begin");
    String aging = null;
    String orderHeaderKey = null;
    String orderNo = null;
    String orderDate = null;
    List<YFCElement> listContainers = new ArrayList<YFCElement>();
    YFCDocument outputDoc = YFCDocument.createDocument(GCConstants.CONTAINERS);
    YFCElement eleOp = outputDoc.getDocumentElement();

    int countDiffStoreContainers = 0;
    YFCNodeList<YFCElement> nlShipment = shpOpEle.getElementsByTagName(GCConstants.SHIPMENT);
    for (YFCElement eleShipment : nlShipment) {
      YFCElement eleShipmentLines = eleShipment.getChildElement(GCConstants.SHIPMENT_LINES);
      YFCElement eleShipmentLine = eleShipmentLines.getChildElement(GCConstants.SHIPMENT_LINE);
      YFCElement eleOrderLine = eleShipmentLine.getChildElement(GCConstants.ORDER_LINE);
      String docType = eleShipment.getAttribute(GCConstants.DOCUMENT_TYPE);
      YFCElement eleShpExtn = eleShipment.getChildElement(GCConstants.EXTN);
      if (YFCUtils.equals(GCConstants.ZERO_ZERO_ZERO_FIVE, docType)) {
        orderHeaderKey = eleOrderLine.getAttribute(GCConstants.CHAINED_FROM_ORDER_HEADER_KEY);
        YFCDocument getOrderListIp = YFCDocument.createDocument(GCConstants.ORDER);
        YFCElement eleOrder = getOrderListIp.getDocumentElement();
        eleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
        YFCDocument getOrderListTemp = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_SALES_ORD_TEMP);
        YFCDocument getOrderListDoc =
            GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListIp, getOrderListTemp);
        YFCElement eleOrderListOp = getOrderListDoc.getDocumentElement();
        YFCElement eleOrderIp = eleOrderListOp.getChildElement(GCConstants.ORDER);
        orderNo = eleOrderIp.getAttribute(GCConstants.ORDER_NO);
        orderDate = eleOrderIp.getAttribute(GCConstants.ORDER_DATE);
      } else {
        orderNo = eleShipmentLine.getAttribute(GCConstants.ORDER_NO);
        orderHeaderKey = eleShipmentLine.getAttribute(GCConstants.ORDER_HEADER_KEY);
        orderDate = eleShpExtn.getAttribute(GCConstants.EXTN_ORDER_DATE);
      }
      String pickUpStoreOp = eleShpExtn.getAttribute(GCConstants.EXTN_PICK_UP_STORE_ID);
      String receiptDate = eleShpExtn.getAttribute(GCConstants.EXTN_PENDING_RECEIPT_DATE);
      if (!YFCCommon.isVoid(receiptDate)) {
        aging =
            GCDateUtils.getTimeDiffrenceInDaysHoursAndMinutes(receiptDate,
                GCDateUtils.getCurrentTime(GCConstants.DATE_TIME_FORMAT));
      }
      YFCElement eleContainers = eleShipment.getChildElement(GCConstants.CONTAINERS);
      YFCNodeList<YFCElement> nlContainer = eleContainers.getElementsByTagName(GCConstants.CONTAINER);
      for (YFCElement eleContainer : nlContainer) {
        String opTrackingNo = eleContainer.getAttribute(GCConstants.TRACKING_NO);
        if (YFCUtils.equals(opTrackingNo, trackingNo)) {
          YFCElement eleContExtn = eleContainer.getChildElement(GCConstants.EXTN);
          String status = eleContExtn.getAttribute(GCConstants.EXTN_CONTAINER_STATUS);
          if (YFCUtils.equals(status, GCConstants.PENDING_RECEIPT)) {
            if (YFCUtils.equals(pickUpStoreOp, pickUpStoreID)) {


              eleContainer.setAttribute(GCConstants.STATUS, status);
              eleContainer.setAttribute(GCConstants.ORDER_DATE, orderDate);
              eleContainer.setAttribute(GCConstants.QUEUE_DATE, receiptDate);
              eleContainer.setAttribute(GCConstants.AGING, aging);
              eleContainer.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
              eleContainer.setAttribute(GCConstants.ORDER_NO, orderNo);
              listContainers.add(eleContainer);
            } else {
              countDiffStoreContainers = countDiffStoreContainers + 1;
            }
          }
          break;
        }
      }
    }
    int lengthSameStoreContainerList = listContainers.size();
    if (lengthSameStoreContainerList > 0) {
      setContainers(listContainers, eleOp);

    } else if (countDiffStoreContainers > 0) {
      eleOp.setAttribute(GCConstants.OTHER_STORE_ORDERS, "Y");
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("OutputDoc by TrackingNo: " + outputDoc.toString());
    }

    LOGGER.verbose("GCShipmentReceiptListManager.getListWithTrackingNo--End");
    LOGGER.endTimer("GCShipmentReceiptListManager.getListWithTrackingNo");
    return outputDoc.getDocument();


  }



  /**
   * Description This method is used to prepare the getShipmentListInput Document
   *
   * @param sTrackingNo
   * @param sPickUpStoreID
   * @return
   */



  private Document getShipmentListInput(String trackingNo, String pickUpStoreID) {
    LOGGER.beginTimer("GCShipmentReceiptListManager.getShipmentListInput");
    LOGGER.verbose("GCShipmentReceiptListManager.getShipmentListInput--Begin");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Tracking No : " + trackingNo);
      LOGGER.verbose("Pick Up Store : " + pickUpStoreID);
    }
    YFCDocument getShipmentListIp = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement rootEle = getShipmentListIp.getDocumentElement();
    if (!YFCCommon.isVoid(trackingNo)) {
      YFCElement eleContainers = rootEle.createChild(GCConstants.CONTAINERS);
      YFCElement eleContainer = eleContainers.createChild(GCConstants.CONTAINER);
      eleContainer.setAttribute(GCConstants.TRACKING_NO, trackingNo);
    } else {
      YFCElement eleExtn = rootEle.createChild(GCConstants.EXTN);
      eleExtn.setAttribute(GCConstants.EXTN_PICK_UP_STORE_ID, pickUpStoreID);
    }
    YFCElement eleComplexQuery = rootEle.createChild(GCConstants.COMPLEX_QUERY);
    eleComplexQuery.setAttribute(GCConstants.OPERATOR, GCConstants.AND);
    YFCElement eleAnd = eleComplexQuery.createChild(GCConstants.AND1);
    YFCElement eleOr = eleAnd.createChild(GCConstants.OR);
    YFCElement eleExp1 = eleOr.createChild(GCConstants.EXP);
    eleExp1.setAttribute(GCConstants.NAME, GCConstants.STATUS);
    eleExp1.setAttribute(GCConstants.VALUE, GCConstants.PARTIALLY_READY_FOR_CUSTOMER_PICK_STATUS);
    eleExp1.setAttribute(GCConstants.QRY_TYPE, GCConstants.EQ);
    YFCElement eleExp2 = eleOr.createChild(GCConstants.EXP);
    eleExp2.setAttribute(GCConstants.NAME, GCConstants.STATUS);
    eleExp2.setAttribute(GCConstants.VALUE, GCConstants.PENDING_RECEIPT_STATUS);
    eleExp2.setAttribute(GCConstants.QRY_TYPE, GCConstants.EQ);
    YFCElement eleOrderBy = rootEle.createChild(GCConstants.ORDER_BY);
    YFCElement eleExtnOrderBy = eleOrderBy.createChild(GCConstants.EXTN);
    YFCElement eleAttribute = eleExtnOrderBy.createChild(GCConstants.ATTRIBUTE);
    eleAttribute.setAttribute(GCConstants.DESC, GCConstants.N);
    eleAttribute.setAttribute(GCConstants.NAME, GCConstants.EXTN_PENDING_RECEIPT_DATE);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("getShipmentListIp : " + getShipmentListIp.toString());
    }
    LOGGER.verbose("GCShipmentReceiptListManager.getShipmentListInput--End");
    LOGGER.endTimer("GCShipmentReceiptListManager.getShipmentListInput");
    return getShipmentListIp.getDocument();
  }

  /**
   * Description This method is used to get the list of containers in Pending Receipt status
   * arriving at the logged in store
   *
   * @param nlShipment
   * @param shpOpEle
   * @return Document
   */

  private Document getListWithStoreID(YFSEnvironment env, YFCElement shpOpEle) {

    LOGGER.beginTimer("GCShipmentReceiptListManager.getListWithStoreID");
    LOGGER.verbose("GCShipmentReceiptListManager.getListWithStoreID--Begin");
    String aging = null;
    String orderHeaderKey = null;
    String orderDate = null;
    String orderNo = null;
    YFCDocument outputDoc = YFCDocument.createDocument(GCConstants.CONTAINERS);
    YFCElement eleOp = outputDoc.getDocumentElement();
    List<YFCElement> listContainers = new ArrayList<YFCElement>();
    YFCNodeList<YFCElement> nlShipment = shpOpEle.getElementsByTagName(GCConstants.SHIPMENT);
    for (YFCElement eleShipment : nlShipment) {
      YFCElement eleShipmentLines = eleShipment.getChildElement(GCConstants.SHIPMENT_LINES);
      YFCElement eleShipmentLine = eleShipmentLines.getChildElement(GCConstants.SHIPMENT_LINE);
      YFCElement eleOrderLine = eleShipmentLine.getChildElement(GCConstants.ORDER_LINE);
      String docType = eleShipment.getAttribute(GCConstants.DOCUMENT_TYPE);
      YFCElement eleShpExtn = eleShipment.getChildElement(GCConstants.EXTN);
      if (YFCUtils.equals(GCConstants.ZERO_ZERO_ZERO_FIVE, docType)) {
        orderHeaderKey = eleOrderLine.getAttribute(GCConstants.CHAINED_FROM_ORDER_HEADER_KEY);
        YFCDocument getOrderListIp = YFCDocument.createDocument(GCConstants.ORDER);
        YFCElement eleOrder = getOrderListIp.getDocumentElement();
        eleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
        YFCDocument getOrderListTemp = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_SALES_ORD_TEMP);
        YFCDocument getOrderListDoc =
            GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListIp, getOrderListTemp);
        YFCElement eleOrderListOp = getOrderListDoc.getDocumentElement();
        YFCElement eleOrderIp = eleOrderListOp.getChildElement(GCConstants.ORDER);
        orderNo = eleOrderIp.getAttribute(GCConstants.ORDER_NO);
        orderDate = eleOrderIp.getAttribute(GCConstants.ORDER_DATE);
      } else {
        orderNo = eleShipmentLine.getAttribute(GCConstants.ORDER_NO);
        orderHeaderKey = eleShipmentLine.getAttribute(GCConstants.ORDER_HEADER_KEY);
        orderDate = eleShpExtn.getAttribute(GCConstants.EXTN_ORDER_DATE);
      }
      String receiptDate = eleShpExtn.getAttribute(GCConstants.EXTN_PENDING_RECEIPT_DATE);
      if (!YFCCommon.isVoid(receiptDate)) {
        aging =
            GCDateUtils.getTimeDiffrenceInDaysHoursAndMinutes(receiptDate,
                GCDateUtils.getCurrentTime(GCConstants.DATE_TIME_FORMAT));
      }
      YFCElement eleContainers = eleShipment.getChildElement(GCConstants.CONTAINERS);
      YFCNodeList<YFCElement> nlContainer = eleContainers.getElementsByTagName(GCConstants.CONTAINER);
      for (YFCElement eleContainer : nlContainer) {

        YFCElement eleContExtn = eleContainer.getChildElement(GCConstants.EXTN);
        String status = eleContExtn.getAttribute(GCConstants.EXTN_CONTAINER_STATUS);
        if (YFCUtils.equals(status, GCConstants.PENDING_RECEIPT)) {


          eleContainer.setAttribute(GCConstants.STATUS, status);
          eleContainer.setAttribute(GCConstants.ORDER_DATE, orderDate);
          eleContainer.setAttribute(GCConstants.QUEUE_DATE, receiptDate);
          eleContainer.setAttribute(GCConstants.AGING, aging);
          eleContainer.setAttribute(GCConstants.ORDER_NO, orderNo);
          eleContainer.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
          listContainers.add(eleContainer);
        }
      }
    }


    int lengthContainerList = listContainers.size();
    if (lengthContainerList > 0) {
      setContainers(listContainers, eleOp);

    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("OutputDoc by StoreID: " + outputDoc.toString());
    }
    LOGGER.verbose("GCShipmentReceiptListManager.getListWithStoreID--End");
    LOGGER.endTimer("GCShipmentReceiptListManager.getListWithStoreID");
    return outputDoc.getDocument();
  }


  /**
   * Description This method is used to set the container attributes to be displayed in receipt list
   *
   * @param listContainers
   * @param shpOpEle
   * @param eleOutput
   */


  private void setContainers(List<YFCElement> listContainers, YFCElement eleOutput) {
    LOGGER.beginTimer("GCShipmentReceiptListManager.setContainers");
    LOGGER.verbose("GCShipmentReceiptListManager.setContainers--Begin");



    for (YFCElement eleContainer : listContainers) {


      YFCElement eleContainerOp = eleOutput.createChild(GCConstants.CONTAINER);
      YFCElement eleContainerDetails = eleContainer.getChildElement(GCConstants.CONTAINER_DETAILS);
      YFCElement eleContainerDetail = eleContainerDetails.getChildElement(GCConstants.CONTAINER_DETAIL);
      YFCElement eleShipmentLine = eleContainerDetail.getChildElement(GCConstants.SHIPMENT_LINE);
      String itemDesc = eleShipmentLine.getAttribute(GCConstants.ITEM_DESCRIPTION);
      eleContainerOp.setAttribute(GCConstants.ITEM_DESCRIPTION, itemDesc);
      eleContainerOp.setAttribute(GCConstants.ORDER_NO, eleContainer.getAttribute(GCConstants.ORDER_NO));
      eleContainerOp
      .setAttribute(GCConstants.ORDER_HEADER_KEY, eleContainer.getAttribute(GCConstants.ORDER_HEADER_KEY));
      eleContainerOp.setAttribute(GCConstants.STATUS, eleContainer.getAttribute(GCConstants.STATUS));
      eleContainerOp.setAttribute(GCConstants.SHIPMENT_CONTAINER_KEY,
          eleContainer.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY));
      eleContainerOp.setAttribute(GCConstants.TRACKING_NO, eleContainer.getAttribute(GCConstants.TRACKING_NO));
      eleContainerOp.setAttribute(GCConstants.SHIPMENT_KEY, eleContainer.getAttribute(GCConstants.SHIPMENT_KEY));
      eleContainerOp.setAttribute(GCConstants.ORDER_DATE, eleContainer.getAttribute(GCConstants.ORDER_DATE));
      eleContainerOp.setAttribute(GCConstants.QUEUE_DATE, eleContainer.getAttribute(GCConstants.QUEUE_DATE));
      eleContainerOp.setAttribute(GCConstants.AGING, eleContainer.getAttribute(GCConstants.AGING));
      eleOutput.importNode(eleContainerOp);
    }


    if (listContainers.size() == 1) {
      eleOutput.setAttribute(GCConstants.SIZE, GCConstants.ONE);
    }
    LOGGER.verbose("GCShipmentReceiptListManager.setContainers--End");
    LOGGER.endTimer("GCShipmentReceiptListManager.setContainers");
  }


}