package com.gc.exstore.api.reports;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCLogUtils;
import com.gc.exstore.api.picking.GCPickDetailAPI;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCGetShipmentReport {
  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCGetShipmentReport.class);

  /**
   *
   * This method invokes the getShipmentList API with the created input document from inDoc and
   * returns the list of Shipment Reports document.
   *
   * @param env
   * @param inDoc return Doc
   * @throws Exception
   *
   */
  public Document getShipmentReport(final YFSEnvironment env, final Document inDoc) {
    LOGGER.beginTimer("GCGetShipmentReport.getShipmentReport");
    LOGGER.verbose("GCGetShipmentReport.getShipmentReport--Begin");
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    GCLogUtils.verboseLog(LOGGER, "inputDoc:", inputDoc);

    String sStartDate;
    String sshippingDate;
    String sReceiptDate;
    String sBillingCustomerFirstName = null;
    String sBillingCustomerLastName = null;
    String sBillingPhone = null;
    String shippingCustomerFirstName = null;
    String shippingCustomerLastName = null;
    String sOrderNo;
    String levelOfServiceDesc;
    String sFulfilmntAssociate = GCConstants.BLANK_STRING;
    String sShipngAssociate = GCConstants.BLANK_STRING;
    String sDocType = GCConstants.BLANK_STRING;

    final YFCElement eleInput = inputDoc.getDocumentElement();
    String sTrackingNo = eleInput.getAttribute(GCConstants.TRACKING_NO);
    String sCartonStatus = eleInput.getAttribute(GCConstants.CARTON_STATUS);
    String sReportType = eleInput.getAttribute(GCConstants.REPORT_TYPE);
    String sIsReturn = eleInput.getAttribute(GCConstants.IS_RETURN);

    // Creating input Template for getShipmentList
    YFCDocument getShipmentListIp = YFCDocument.getDocumentFor(getShipmentListInput(inDoc));
    // Invoke getShipmentList API with getShipmentListIp document

    YFCDocument getShipmentListTemp = YFCDocument.getDocumentFor(GCConstants.GET_SHIPMENT_LIST_OUTPUT_TMP);
    YFCDocument getShipmentListOpDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, getShipmentListIp, getShipmentListTemp);
    YFCElement eleshipoutput = getShipmentListOpDoc.getDocumentElement();
    GCLogUtils.verboseLog(LOGGER, "getShipmentListOpDoc:", getShipmentListOpDoc);
    YFCDocument getShipmentReportOpDoc = YFCDocument.createDocument(GCConstants.CONTAINERS);
    final YFCNodeList<YFCElement> nlShipmentList = eleshipoutput.getElementsByTagName(GCConstants.SHIPMENT);
    final int lengthShipment = nlShipmentList.getLength();
    if (lengthShipment == 0) {
      return getShipmentReportOpDoc.getDocument();
    }
    YFCElement eleoutput = getShipmentReportOpDoc.getDocumentElement();
    YFCElement eleShipmentList = eleshipoutput.getChildElement(GCConstants.SHIPMENT);

    // Iterating over the ShipmentList

    LOGGER.verbose("LengthShipment :" + lengthShipment);
    for (int j = 0; j < lengthShipment; j++) {
      eleShipmentList = nlShipmentList.item(j);
      String sSource = eleShipmentList.getAttribute(GCConstants.SHIP_NODE);

      YFCElement eleExtnsh = eleShipmentList.getChildElement(GCConstants.EXTN);
      String sRecievingNode = eleExtnsh.getAttribute(GCConstants.EXTN_PICKUP_STORE_ID);
      String sExtnPOSSalesTicketNo = eleExtnsh.getAttribute(GCConstants.EXTN_POS_SALES_TICKET);
      String sOrderDate = eleExtnsh.getAttribute(GCConstants.EXTN_ORDER_DATE);
      String sOrderHeader = eleShipmentList.getAttribute(GCConstants.ORDER_HEADER_KEY);
      String sShipmentKey = eleShipmentList.getAttribute(GCConstants.SHIPMENT_KEY);
      String sDocumentType = eleShipmentList.getAttribute(GCConstants.DOCUMENT_TYPE);
      if (((YFCUtils.equals(sReportType, "Incoming")) && (YFCCommon.isVoid(sRecievingNode)))
          || (YFCUtils.equals(sSource, sRecievingNode))) {
        continue;
      } else {
        YFCElement eleBilToAddress = eleShipmentList.getChildElement(GCXmlLiterals.BILL_TO_ADDRESS);
        if (!YFCCommon.isVoid(eleBilToAddress)) {
          sBillingCustomerFirstName = eleBilToAddress.getAttribute(GCConstants.FIRST_NAME);
          sBillingCustomerLastName = eleBilToAddress.getAttribute(GCConstants.LAST_NAME);
          sBillingPhone = eleBilToAddress.getAttribute(GCConstants.DAY_PHONE);
        }
        YFCElement eleToAddress = eleShipmentList.getChildElement(GCXmlLiterals.TO_ADDRESS);
        if (!YFCCommon.isVoid(eleToAddress)) {
          shippingCustomerFirstName = eleToAddress.getAttribute(GCConstants.FIRST_NAME);
          shippingCustomerLastName = eleToAddress.getAttribute(GCConstants.LAST_NAME);
        }
        sshippingDate = eleShipmentList.getAttribute(GCConstants.SHIP_DATE);
        String sshippingMethod = eleShipmentList.getAttribute(GCConstants.LEVEL_OF_SERVICE);
        GCPickDetailAPI obj = new GCPickDetailAPI();
        levelOfServiceDesc = obj.callCommonCodeList(env, sshippingMethod);
        YFCElement eleExtn = eleShipmentList.getChildElement(GCXmlLiterals.EXTN);
        sReceiptDate = eleExtn.getAttribute(GCConstants.EXTN_PENDING_RECEIPT_DATE);

        YFCElement eleShipLines = eleShipmentList.getChildElement(GCConstants.SHIPMENT_LINES);
        YFCElement eleShipLine = eleShipLines.getChildElement(GCConstants.SHIPMENT_LINE);
        sOrderNo = eleShipLine.getAttribute(GCConstants.ORDER_NO);
      }

      // Invoking GCOrderCommissionListService to fetch the value of UserName

      YFCDocument getOrderCommissionInputDoc = YFCDocument.createDocument(GCConstants.GC_ORDER_COMMISSION);
      YFCElement elegcordCommission = getOrderCommissionInputDoc.getDocumentElement();
      elegcordCommission.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeader);
      YFCDocument getOrderCommissionListOpDoc =
          GCCommonUtil.invokeService(env, GCConstants.GC_ORDER_COMMISSION_LIST_SERVICE, getOrderCommissionInputDoc);
      YFCElement elegcordCommissionop = getOrderCommissionListOpDoc.getDocumentElement();
      final YFCNodeList<YFCElement> nlOrderCommissionList =
          elegcordCommissionop.getElementsByTagName(GCConstants.GC_ORDER_COMMISSION);
      final int lengthComm = nlOrderCommissionList.getLength();
      String sUserName = "";
      if (lengthComm > 0) {
        YFCElement eleGCOrderCommission = nlOrderCommissionList.item(0);
        sUserName = eleGCOrderCommission.getAttribute(GCConstants.USER_NAM);
      }

      // Invoking GCFetchUserActivityService to fetch UserID

      YFCDocument getUserActivityInputDoc = YFCDocument.createDocument(GCConstants.GC_USER_ACTIVITY);
      YFCElement eleUserActivity = getUserActivityInputDoc.getDocumentElement();
      eleUserActivity.setAttribute(GCConstants.SHIPMENT_KEY, sShipmentKey);
      YFCDocument getUserActivityOpDoc =
          GCCommonUtil.invokeService(env, GCConstants.GC_FETCH_USER_ACTIVITY, getUserActivityInputDoc);
      YFCElement eleUserActivitylistop = getUserActivityOpDoc.getDocumentElement();
      GCLogUtils.verboseLog(LOGGER, "getUserActivityOpDoc:", getUserActivityOpDoc);
      final YFCNodeList<YFCElement> nlUserActivity =
          eleUserActivitylistop.getElementsByTagName(GCConstants.GC_USER_ACTIVITY);
      final int lengthUserActivity = nlUserActivity.getLength();
      LOGGER.verbose("lengthUserActivity :" + lengthShipment);
      for (int l = 0; l < lengthUserActivity; l++) {
        YFCElement eleUserActivityop = nlUserActivity.item(l);
        String sAuditReasonCode = eleUserActivityop.getAttribute(GCConstants.AUDIT_RSNCODE);
        if (YFCUtils.equals(GCConstants.CONFIRM_PICK_CONFIRM, sAuditReasonCode)) {
          sFulfilmntAssociate = eleUserActivityop.getAttribute(GCConstants.USER_ID);
        }
        if (YFCUtils.equals(GCConstants.PACK_CONFIRM, sAuditReasonCode)) {
          sShipngAssociate = eleUserActivityop.getAttribute(GCConstants.USER_ID);
        }
      }
      YFCElement eleContainersList = eleShipmentList.getChildElement(GCConstants.CONTAINERS);
      YFCElement eleContainerList = eleContainersList.getChildElement(GCConstants.CONTAINER);
      YFCElement eleBillto = eleShipmentList.getChildElement(GCXmlLiterals.BILL_TO_ADDRESS);
      YFCElement eleToAddress = eleShipmentList.getChildElement(GCXmlLiterals.TO_ADDRESS);

      // Iterate over the ContainerList

      final YFCNodeList<YFCNode> nlContainerList = eleContainersList.getChildNodes();
      final int elelengthContainerList = nlContainerList.getLength();
      if (elelengthContainerList > 0) {
        for (int k = 0; k < elelengthContainerList; k++) {
          eleContainerList = (YFCElement) nlContainerList.item(k);
          String sShipmentContainerKey = eleContainerList.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
          String sTrackno = eleContainerList.getAttribute(GCConstants.TRACKING_NO);
          YFCElement extn1 = eleContainerList.getChildElement(GCConstants.EXTN);
          String sStatus1 = extn1.getAttribute(GCConstants.EXTN_CONTAINER_STATUS);
          if (YFCUtils.equals(sReportType, "Outgoing")) {
            sStartDate = sshippingDate;
          } else {
            sStartDate = sReceiptDate;
          }
          String aging = "";
          if(!YFCCommon.isVoid(sStartDate)){
            aging = GCDateUtils.getTimeDiffrenceInDaysHoursAndMinutes(sStartDate,
                GCDateUtils.getCurrentTime(GCConstants.DATE_TIME_FORMAT));
          }

          if (YFCUtils.equals(sDocumentType, "0006")) {
            sDocType = sDocumentType;
          }

          if ((YFCCommon.isVoid(sTrackingNo) && YFCCommon.isVoid(sCartonStatus))) {
            if ((YFCUtils.equals(GCConstants.SHIPPED_TO_CUSTOMER, sStatus1))
                || (YFCUtils.equals(GCConstants.PENDING_RECEIPT, sStatus1))) {
              YFCElement eleContainer = eleoutput.createChild(GCConstants.CONTAINER);
              eleContainer.setAttribute(GCConstants.SOURCE, sSource);
              eleContainer.setAttribute(GCConstants.SPO_ASSOCIATE, sUserName);
              eleContainer.setAttribute(GCConstants.BILLING_CUSTOMER_FIRST_NAME, sBillingCustomerFirstName);
              eleContainer.setAttribute(GCConstants.BILLING_CUSTOMER_LAST_NAME, sBillingCustomerLastName);
              eleContainer.setAttribute(GCConstants.ORDER_NO, sOrderNo);
              eleContainer.setAttribute(GCConstants.BILLING_PHONE, sBillingPhone);
              eleContainer.setAttribute(GCConstants.SHIPPING_CUSTOMER_FIRST_NAME, shippingCustomerFirstName);
              eleContainer.setAttribute(GCConstants.SHIPPING_CUSTOMER_LAST_NAME, shippingCustomerLastName);
              eleContainer.setAttribute(GCConstants.SHIPPING_DATE, sshippingDate);
              eleContainer.setAttribute(GCConstants.ORDER_DATE, sOrderDate);
              eleContainer.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeader);
              eleContainer.setAttribute(GCConstants.RECEIVING_STORE, sRecievingNode);
              eleContainer.setAttribute(GCConstants.SHIPMENT_CONTAINER_KEY, sShipmentContainerKey);
              eleContainer.setAttribute(GCConstants.POS_SALES_TICKET, sExtnPOSSalesTicketNo);
              eleContainer.setAttribute(GCConstants.SHIPPING_METHOD, levelOfServiceDesc);
              eleContainer.setAttribute(GCConstants.FULFILLMENT_ASSOCIATE, sFulfilmntAssociate);
              eleContainer.setAttribute(GCConstants.SHIPPING_ASSOCIATE, sShipngAssociate);
              eleContainer.setAttribute(GCConstants.AGING_DAYS, aging);
              eleContainer.setAttribute(GCConstants.TRACKING_NO, sTrackno);
              eleContainer.setAttribute(GCConstants.STATUS, sStatus1);
              eleContainer.setAttribute(GCConstants.DOCUMENT_TYPE, sDocType);
              eleContainer.setAttribute(GCConstants.SHIPMENT_KEY, sShipmentKey);
              eleoutput.importNode(eleContainer);
              eleContainer.importNode(eleBillto);
              eleContainer.importNode(eleToAddress);
            }
          } else if (!(YFCCommon.isVoid(sTrackingNo)) && YFCUtils.equals(sTrackingNo, sTrackno)) {
            YFCElement eleContainer = eleoutput.createChild(GCConstants.CONTAINER);
            eleContainer.setAttribute(GCConstants.SOURCE, sSource);
            eleContainer.setAttribute(GCConstants.SPO_ASSOCIATE, sUserName);
            eleContainer.setAttribute(GCConstants.BILLING_CUSTOMER_FIRST_NAME, sBillingCustomerFirstName);
            eleContainer.setAttribute(GCConstants.BILLING_CUSTOMER_LAST_NAME, sBillingCustomerLastName);
            eleContainer.setAttribute(GCConstants.ORDER_NO, sOrderNo);
            eleContainer.setAttribute(GCConstants.BILLING_PHONE, sBillingPhone);
            eleContainer.setAttribute(GCConstants.SHIPPING_CUSTOMER_FIRST_NAME, shippingCustomerFirstName);
            eleContainer.setAttribute(GCConstants.SHIPPING_CUSTOMER_LAST_NAME, shippingCustomerLastName);
            eleContainer.setAttribute(GCConstants.SHIPPING_DATE, sshippingDate);
            eleContainer.setAttribute(GCConstants.ORDER_DATE, sOrderDate);
            eleContainer.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeader);
            eleContainer.setAttribute(GCConstants.RECEIVING_STORE, sRecievingNode);
            eleContainer.setAttribute(GCConstants.SHIPMENT_CONTAINER_KEY, sShipmentContainerKey);
            eleContainer.setAttribute(GCConstants.POS_SALES_TICKET, sExtnPOSSalesTicketNo);
            eleContainer.setAttribute(GCConstants.SHIPPING_METHOD, levelOfServiceDesc);
            eleContainer.setAttribute(GCConstants.FULFILLMENT_ASSOCIATE, sFulfilmntAssociate);
            eleContainer.setAttribute(GCConstants.SHIPPING_ASSOCIATE, sShipngAssociate);
            eleContainer.setAttribute(GCConstants.AGING_DAYS, aging);
            eleContainer.setAttribute(GCConstants.TRACKING_NO, sTrackingNo);
            eleContainer.setAttribute(GCConstants.STATUS, sStatus1);
            eleContainer.setAttribute(GCConstants.DOCUMENT_TYPE, sDocType);
            eleContainer.setAttribute(GCConstants.SHIPMENT_KEY, sShipmentKey);
            eleoutput.importNode(eleContainer);
            eleContainer.importNode(eleBillto);
            eleContainer.importNode(eleToAddress);

          } else if (!(YFCCommon.isVoid(sCartonStatus)) && (YFCUtils.equals(sCartonStatus, sStatus1))
              && (YFCCommon.isVoid(sTrackingNo))) {
            YFCElement eleContainer = eleoutput.createChild(GCConstants.CONTAINER);
            eleContainer.setAttribute(GCConstants.SOURCE, sSource);
            eleContainer.setAttribute(GCConstants.SPO_ASSOCIATE, sUserName);
            eleContainer.setAttribute(GCConstants.BILLING_CUSTOMER_FIRST_NAME, sBillingCustomerFirstName);
            eleContainer.setAttribute(GCConstants.BILLING_CUSTOMER_LAST_NAME, sBillingCustomerLastName);
            eleContainer.setAttribute(GCConstants.ORDER_NO, sOrderNo);
            eleContainer.setAttribute(GCConstants.BILLING_PHONE, sBillingPhone);
            eleContainer.setAttribute(GCConstants.SHIPPING_CUSTOMER_FIRST_NAME, shippingCustomerFirstName);
            eleContainer.setAttribute(GCConstants.SHIPPING_CUSTOMER_LAST_NAME, shippingCustomerLastName);
            eleContainer.setAttribute(GCConstants.SHIPPING_DATE, sshippingDate);
            eleContainer.setAttribute(GCConstants.ORDER_DATE, sOrderDate);
            eleContainer.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeader);
            eleContainer.setAttribute(GCConstants.RECEIVING_STORE, sRecievingNode);
            eleContainer.setAttribute(GCConstants.SHIPMENT_CONTAINER_KEY, sShipmentContainerKey);
            eleContainer.setAttribute(GCConstants.POS_SALES_TICKET, sExtnPOSSalesTicketNo);
            eleContainer.setAttribute(GCConstants.SHIPPING_METHOD, levelOfServiceDesc);
            eleContainer.setAttribute(GCConstants.FULFILLMENT_ASSOCIATE, sFulfilmntAssociate);
            eleContainer.setAttribute(GCConstants.SHIPPING_ASSOCIATE, sShipngAssociate);
            eleContainer.setAttribute(GCConstants.AGING_DAYS, aging);
            eleContainer.setAttribute(GCConstants.TRACKING_NO, sTrackno);
            eleContainer.setAttribute(GCConstants.STATUS, sStatus1);
            eleContainer.setAttribute(GCConstants.DOCUMENT_TYPE, sDocType);
            eleContainer.setAttribute(GCConstants.SHIPMENT_KEY, sShipmentKey);
            eleoutput.importNode(eleContainer);
            eleContainer.importNode(eleBillto);
            eleContainer.importNode(eleToAddress);
          }
        }
      }
    }
    if (YFCUtils.equals(sIsReturn, GCConstants.FLAG_Y)) {
      return GCCommonUtil.invokeService(env, GCConstants.GC_SHIPMENT_REPORTS, getShipmentReportOpDoc).getDocument();
    }
    return getShipmentReportOpDoc.getDocument();
  }

  /**
   * Description This method is used to prepare the getShipmentListInput Document which will invoke
   * the getShipmentList API
   *
   * @param inDoc
   * @return
   */
  public Document getShipmentListInput(Document inDoc) {
    LOGGER.beginTimer("GCGetShipmentReport.getShipmentListInput");
    LOGGER.verbose("GCGetShipmentReport.getShipmentListInput--Begin");

    // Fetching the required attributes from inDoc
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    GCLogUtils.verboseLog(LOGGER, "inputDoc:", inputDoc);
    final YFCElement eleInputDoc = inputDoc.getDocumentElement();
    String sOrderNo = eleInputDoc.getAttribute(GCConstants.ORDER_NO);
    String sTrackingNo = eleInputDoc.getAttribute(GCConstants.TRACKING_NO);
    String sFromOrderDate = eleInputDoc.getAttribute(GCConstants.FROM_ORDER_DATE);
    String sToOrderDate = eleInputDoc.getAttribute(GCConstants.TO_ORDER_DATE);
    String sCartonStatus = eleInputDoc.getAttribute(GCConstants.CARTON_STATUS);
    String sStoreID = eleInputDoc.getAttribute(GCConstants.STORE_ID);
    String sReportType = eleInputDoc.getAttribute(GCConstants.REPORT_TYPE);
    String sMultipleStoreFlag = eleInputDoc.getAttribute(GCConstants.IS_MULTI_STORE);

    // start preparing the Input getShipmentList Doc

    YFCDocument getShipmentListIp = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement eleShipIp = getShipmentListIp.getDocumentElement();
    eleShipIp.setAttribute(GCConstants.STATUS, "1400");
    eleShipIp.setAttribute(GCConstants.STATUS_QRY_TYPE, "GT");
    YFCElement eleShipnode = eleShipIp.createChild(GCConstants.SHIP_NODE);
    eleShipnode.setAttribute("NodeType", GCConstants.STORE);


    YFCElement eleShipmentLines = eleShipIp.createChild(GCConstants.SHIPMENT_LINES);
    YFCElement eleShipmentLine = eleShipmentLines.createChild(GCConstants.SHIPMENT_LINE);
    YFCElement eleOrder = eleShipmentLine.createChild(GCConstants.ORDER);
    if (!(YFCCommon.isVoid(sOrderNo))) {
      eleShipmentLine.setAttribute(GCConstants.ORDER_NO, sOrderNo);
    }

    if (!(YFCCommon.isVoid(sFromOrderDate) && YFCCommon.isVoid(sToOrderDate))) {
      eleOrder.setAttribute(GCConstants.FROM_ORDER_DATE, sFromOrderDate);
      eleOrder.setAttribute(GCConstants.TO_ORDER_DATE, sToOrderDate);
      eleOrder.setAttribute("OrderDateQryType", "BETWEEN");
    }
    if (!(YFCCommon.isVoid(sTrackingNo) && YFCCommon.isVoid(sCartonStatus))) {
      YFCElement eleContainers = eleShipIp.createChild(GCConstants.CONTAINERS);
      YFCElement eleContainer = eleContainers.createChild(GCConstants.CONTAINER);
      YFCElement eleExtncont = eleContainer.createChild(GCConstants.EXTN);
      eleContainer.setAttribute(GCConstants.TRACKING_NO, sTrackingNo);
      eleExtncont.setAttribute(GCConstants.EXTN_CONTAINER_STATUS, sCartonStatus);
    }
    if (!(YFCCommon.isVoid(sStoreID))) {
      if (YFCUtils.equals(sReportType, "Outgoing")) {
        eleShipIp.setAttribute(GCConstants.SHIP_NODE, sStoreID);
      }else {
        YFCElement eleExtnShipment = eleShipIp.createChild(GCConstants.EXTN);
        eleExtnShipment.setAttribute(GCConstants.EXTN_PICKUP_STORE_ID, sStoreID);
      }
    } else if (YFCUtils.equals(GCConstants.FLAG_Y, sMultipleStoreFlag)) {
      final YFCNodeList<YFCElement> nlStore = eleInputDoc.getElementsByTagName(GCConstants.STORE);
      final int lengthStore = nlStore.getLength();
      if (YFCUtils.equals(sReportType, "Outgoing")) {
        YFCElement eleComplexQuery = eleShipIp.createChild("ComplexQuery");
        eleComplexQuery.setAttribute("Operator", "AND");
        YFCElement eleAnd = eleComplexQuery.createChild("And");
        YFCElement eleOr = eleAnd.createChild("Or");
        LOGGER.verbose("lengthStoreid :" + lengthStore);
        for (int j = 0; j < lengthStore; j++) {
          final YFCElement eleStoreID = nlStore.item(j);
          String sStoreId = eleStoreID.getAttribute(GCConstants.STORE_ID);
          YFCElement eleExp = eleOr.createChild("Exp");
          eleExp.setAttribute(GCConstants.NAME, "ShipNode");
          eleExp.setAttribute(GCConstants.VALUE, sStoreId);
          eleExp.setAttribute(GCConstants.QRY_TYPE, "EQ");
        }
      } else {
        YFCElement eleComplexQuery = eleShipIp.createChild("ComplexQuery");
        eleComplexQuery.setAttribute("Operator", "AND");
        YFCElement eleAnd = eleComplexQuery.createChild("And");
        YFCElement eleOr = eleAnd.createChild("Or");
        LOGGER.verbose("lengthStoreid :" + lengthStore);
        for (int j = 0; j < lengthStore; j++) {
          final YFCElement eleStoreID = nlStore.item(j);
          String sStoreId = eleStoreID.getAttribute(GCConstants.STORE_ID);
          YFCElement eleExp = eleOr.createChild("Exp");
          eleExp.setAttribute(GCConstants.NAME, GCConstants.EXTN_PICKUP_STORE_ID);
          eleExp.setAttribute(GCConstants.VALUE, sStoreId);
          eleExp.setAttribute(GCConstants.QRY_TYPE, "EQ");
        }
      }
    }

    GCLogUtils.verboseLog(LOGGER, "getShipmentListIp:", getShipmentListIp);
    LOGGER.verbose("GCGetShipmentReport.getShipmentListInput--End");
    LOGGER.endTimer("GCGetShipmentReport.getShipmentListInput");
    return getShipmentListIp.getDocument();
  }
}
