package com.gc.exstore.api.shipbacktodc;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.exstore.api.packing.GCCancelShipment;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCMarkINVCShipmentAPI implements YIFCustomApi{
	
	private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCMarkINVCShipmentAPI.class);
	
  public Document markINVCShipment(YFSEnvironment env, Document inputDoc) {
	LOGGER.verbose("GCMarkINVCShipmentAPI.markINVCShipment -- Begin");
	LOGGER.verbose("Input Document  :" + inputDoc.toString());
    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    YFCElement rootEle = inDoc.getDocumentElement();
    YFCElement shipLinesInEle = rootEle.getChildElement(GCConstants.SHIPMENT_LINES);
    YFCElement shipLineInEle = shipLinesInEle.getChildElement(GCConstants.SHIPMENT_LINE);
    String shipLineKeyInput = shipLineInEle.getAttribute(GCConstants.SHIPMENT_LINE_KEY);
    double qtyToCancel = shipLineInEle.getDoubleAttribute(GCConstants.QUANTITY);
    
    String userName = rootEle.getAttribute(GCConstants.LOGGEDIN_USER_NAME);
    String domain = rootEle.getAttribute(GCConstants.USER_ID_DOMAIN);
    String enteredBy = domain + GCConstants.DOMAIN_LOGINID_SEPARATOR + userName; 
  
    YFCDocument shipListInputDoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    shipListInputDoc.getDocumentElement().setAttribute(GCConstants.SHIPMENT_KEY,
        rootEle.getAttribute(GCConstants.SHIPMENT_KEY));

    YFCDocument getShipListTemp = YFCDocument.getDocumentFor(GCConstants.GET_SHIP_LIST_MARK_INVC);
    YFCDocument getShipListOutdoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, shipListInputDoc, getShipListTemp);

    YFCDocument changeShipIndoc = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement shipment = changeShipIndoc.getDocumentElement();
    shipment.setAttribute(GCConstants.SHIPMENT_KEY, rootEle.getAttribute(GCConstants.SHIPMENT_KEY));
    YFCElement shipmentLines = shipment.createChild(GCConstants.SHIPMENT_LINES);

    YFCElement shipmentsEle = getShipListOutdoc.getDocumentElement();
    YFCElement shipmentEle = shipmentsEle.getChildElement(GCConstants.SHIPMENT);
    YFCElement shipmentLinesEle = shipmentEle.getChildElement(GCConstants.SHIPMENT_LINES);
    YFCIterable<YFCElement> shipmentLineList = shipmentLinesEle.getChildren();

    // Boolean used to check whether the complete shipment is getting cancelled or not.
    boolean completeCancelShipment = true;
    for(YFCElement shipmentLineEle : shipmentLineList){
      if(YFCUtils.equals(GCConstants.SET, shipmentLineEle.getAttribute(GCConstants.PRODUCT_CLASS)) && YFCUtils.equals(shipLineKeyInput, shipmentLineEle.getAttribute(GCConstants.PARENT_SHPMNTLINE_KEY))){
        shipmentLines.importNode(shipmentLineEle);
      } else if(YFCUtils.equals(shipLineKeyInput, shipmentLineEle.getAttribute(GCConstants.SHIPMENT_LINE_KEY))){
        shipmentLines.importNode(shipmentLineEle);
      } else if (0 != Float.valueOf(shipmentLineEle.getAttribute(GCConstants.QUANTITY)).intValue()) {
        completeCancelShipment = false;
      }

    }

    YFCIterable<YFCElement> shipmentLineOutList = shipmentLines.getChildren();

    for(YFCElement shipmentLine : shipmentLineOutList){

      double shippedQty = shipmentLine.getDoubleAttribute(GCConstants.QUANTITY);
      if (qtyToCancel == shippedQty) {
        shipmentLine.setAttribute(GCConstants.ACTION, GCConstants.ACTION_CANCEL_SHIPMENT);
      } else {
        completeCancelShipment = false;
        shipmentLine.setAttribute(GCConstants.ACTION, GCConstants.ACTION_MODIFY);
        if (!YFCUtils.equals(GCConstants.BUNDLE, shipmentLine.getAttribute(GCConstants.KIT_CODE))
            && YFCUtils.equals(GCConstants.SET, shipmentLine.getAttribute(GCConstants.PRODUCT_CLASS))) {
          double kitQty = shipmentLine.getDoubleAttribute(GCConstants.KIT_QUANTITY);
          double cancelledQty = kitQty * qtyToCancel;
          shipmentLine.setAttribute(GCConstants.QUANTITY, shippedQty - cancelledQty);
        } else {
          shipmentLine.setAttribute(GCConstants.QUANTITY, shippedQty - qtyToCancel);
        }
      }


    }

    if (completeCancelShipment) {
      shipment.setAttribute(GCConstants.ACTION, GCConstants.ACTION_CANCEL_SHIPMENT);
    } else {
      shipment.setAttribute(GCConstants.ACTION, GCConstants.ACTION_MODIFY);
    }
    shipment.setAttribute(GCConstants.CANCEL_RMVD_QTY, GCConstants.YES);

    // call getShipmentLineList to find the OHK of transferOrder
    YFCDocument shipLineListInputDoc = YFCDocument.createDocument(GCConstants.SHIPMENT_LINE);
    shipLineListInputDoc.getDocumentElement().setAttribute(GCConstants.SHIPMENT_LINE_KEY, shipLineKeyInput);

    YFCDocument getShipLineListTemp = YFCDocument.getDocumentFor(GCConstants.MARK_INVC_GET_SHIPMENT_LINELIST_TMP);
    
    YFCDocument getShipLineListOutdoc =
            GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LINE_LIST, shipLineListInputDoc, getShipLineListTemp);
    YFCElement eleshipmentLines = getShipLineListOutdoc.getDocumentElement();
    YFCElement eleShipmentLine = eleshipmentLines.getChildElement(GCConstants.SHIPMENT_LINE);
    String orderLineKey1 = eleShipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY);
   
    
    //call getOrderLineList to find the OHK and OLK of returnOrder with the transfer OHK
    YFCDocument getOrderLineListInputDoc = YFCDocument.createDocument(GCConstants.ORDER_LINE);
    getOrderLineListInputDoc.getDocumentElement().setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey1);

    YFCDocument getOrderLineListTemp = YFCDocument.getDocumentFor(GCConstants.MARK_INVC_GET_ORDER_LINE_LIST_TMP);
    
    YFCDocument getOrderLineListOutdoc =
            GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LINE_LIST, getOrderLineListInputDoc, getOrderLineListTemp);
    YFCElement eleOrderLineList = getOrderLineListOutdoc.getDocumentElement();
   
    YFCElement eleOrderLine = eleOrderLineList.getChildElement(GCConstants.ORDER_LINE);
    String orderHeaderKey = eleOrderLine.getAttribute(GCConstants.DERIVED_FROM_ORDER_HEADER_KEY);
    String orderLineKey = eleOrderLine.getAttribute(GCConstants.DERIVED_FROM_ORDER_LINE_KEY);
    
    // Call change Order to set the EnteredBy for the return Order
    YFCDocument changeOrderIndoc = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement eleOrder = changeOrderIndoc.getDocumentElement();
    eleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
    eleOrder.setAttribute(GCConstants.OVERRIDE, GCConstants.FLAG_Y);
    eleOrder.setAttribute(GCConstants.ENTERED_BY, enteredBy);
    YFCElement ordLines = eleOrder.createChild(GCConstants.ORDER_LINES);
    YFCElement ordLine = ordLines.createChild(GCConstants.ORDER_LINE);
    ordLine.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKey);
    YFCElement eleExtn = ordLine.createChild(GCConstants.EXTN);
    Date currentDate = new Date();
	String strOrderDate = new SimpleDateFormat(GCConstants.DATE_TIME_FORMAT).format(currentDate);
    eleExtn.setAttribute(GCConstants.EXTN_INVC_UPDATED_DATE, strOrderDate );
    LOGGER.verbose("Input Document to changeOrder :" + changeOrderIndoc.toString());
    GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, changeOrderIndoc.getDocument());
    
    GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_SHIPMENT, changeShipIndoc.getDocument());
    
    GCCancelShipment.updateUserActivityTable(env, rootEle, GCConstants.SHIP_TO_DC_INVC, GCConstants.MARK_AN_INVC);

    rootEle.setAttribute(GCConstants.SHIPMENT_LINE_KEY, shipLineKeyInput);
    return inputDoc;


  }

  /**
   *
   */
  @Override
  public void setProperties(Properties arg0) {


  }



}
