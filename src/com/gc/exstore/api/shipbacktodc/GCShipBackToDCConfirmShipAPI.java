package com.gc.exstore.api.shipbacktodc;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.exstore.api.packing.GCCancelShipment;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCShipBackToDCConfirmShipAPI implements YIFCustomApi{

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCShipBackToDCConfirmShipAPI.class);

  public Document confirmShipment(YFSEnvironment env, Document inputDoc) {

    LOGGER.beginTimer("GCShipBackToDCConfirmShipAPI.confirmShipment");
    LOGGER.verbose("GCPackDetailsShipListManager.confirmShipment -- Begin");
    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    YFCElement rootInEle = inDoc.getDocumentElement();

    //Call getShipmentList
    YFCDocument getShipListTemp = YFCDocument.getDocumentFor(GCConstants.GET_SHIP_LIST_TEMP_SHIPBACK_TO_DC);
    if(LOGGER.isVerboseEnabled()){
      LOGGER.verbose("Template Doc :" +getShipListTemp.toString());
    }
    YFCDocument getShipListOutdoc = GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, inDoc, getShipListTemp);
    if(LOGGER.isVerboseEnabled()){
      LOGGER.verbose("Shipment List Outdoc :" +getShipListOutdoc.toString());
    }

    YFCElement shipmentsEle = getShipListOutdoc.getDocumentElement();
    YFCElement shipmentEle = shipmentsEle.getChildElement(GCConstants.SHIPMENT);

    YFCDocument confirmShipIndoc = YFCDocument.getDocumentFor(shipmentEle.toString());
    YFCElement confirmShipRootEle = confirmShipIndoc.getDocumentElement();
    confirmShipRootEle.setAttribute(GCConstants.BACKORDER_NON_SHIPPED_QTY, GCConstants.YES);

    Map<String, String> packedLines = new HashMap<String, String>();
    //Remove unpacked shipment lines
    YFCElement containersEle = confirmShipRootEle.getChildElement(GCConstants.CONTAINERS);
    YFCIterable<YFCElement> containerList = containersEle.getChildren();

    for(YFCElement containerEle : containerList){
      YFCElement containerDtlsEle = containerEle.getChildElement(GCConstants.CONTAINER_DETAILS);
      YFCIterable<YFCElement> containerDtlList = containerDtlsEle.getChildren();
      for(YFCElement containerDtlEle : containerDtlList){
        String containerQty = containerDtlEle.getAttribute(GCConstants.QUANTITY);
        YFCElement shipmentLineChild = containerDtlEle.getChildElement(GCConstants.SHIPMENT_LINE);
        if (0 != Float.valueOf(containerQty).intValue()) {
          packedLines.put(shipmentLineChild.getAttribute(GCConstants.SHIPMENT_LINE_KEY), containerQty);
        }
      }
    }

    //Define a set to store the backorderable OrderHeaderKeys
    Set<String> orderHeaderKeySet = new HashSet<String>();

    YFCElement shipmentLinesEle = confirmShipRootEle.getChildElement(GCConstants.SHIPMENT_LINES);
    YFCIterable<YFCElement> shipmentLineList = shipmentLinesEle.getChildren();
    for(YFCElement shipmentLineEle : shipmentLineList){

      double shippedQty = shipmentLineEle.getDoubleAttribute(GCConstants.QUANTITY);
      if (packedLines.containsKey(shipmentLineEle.getAttribute(GCConstants.SHIPMENT_LINE_KEY)) && shippedQty != 0) {
        shipmentLineEle.setAttribute(GCConstants.QUANTITY, packedLines.get(shipmentLineEle.getAttribute(GCConstants.SHIPMENT_LINE_KEY)));
      } else if (!YFCUtils.equals(GCConstants.BUNDLE, shipmentLineEle.getAttribute(GCConstants.KIT_CODE))
          && shippedQty != 0) {
        shipmentLineEle.setAttribute(GCConstants.QUANTITY, GCConstants.ZERO_AMOUNT);
        YFCElement orderLineEle = shipmentLineEle.getChildElement(GCConstants.ORDER_LINE);
        orderHeaderKeySet.add(orderLineEle.getAttribute(GCConstants.ORDER_HEADER_KEY));
        // Fix for 4958:  As per PMR : 20916,227,000, adding Action=DELETE for lines to be backordered (Both Regular + Bundle Components).
        shipmentLineEle.setAttribute(GCConstants.ACTION, "Delete");
      } else {
        shipmentLinesEle.removeChild(shipmentLineEle);
      }
    }

    if(LOGGER.isVerboseEnabled()){
      LOGGER.verbose("confirmShip Indoc :" +confirmShipIndoc.toString());
    }
    GCCommonUtil.invokeAPI(env, GCConstants.CONFIRM_SHIPMENT_API, confirmShipIndoc.getDocument());

    //Stamp ShipmentContainerKey to root element.
    YFCElement container = containersEle.getChildElement(GCConstants.CONTAINER);
    String shipContainerKey = container.getAttribute(GCConstants.SHIPMENT_CONTAINER_KEY);
    rootInEle.setAttribute(GCConstants.CONTAINER_KEY, shipContainerKey);
    GCCancelShipment.updateUserActivityTable(env, rootInEle, GCConstants.SHIP_TO_DC_SHIPPED, GCConstants.SHIP);

    YFCDocument outDoc = YFCDocument.createDocument(GCConstants.ORDER_LIST);
    YFCElement orderListEle = outDoc.getDocumentElement();
    orderListEle.setAttribute(GCConstants.EX_SHP_BCK_TO_DC, GCConstants.FLAG_Y);
    for(String orderHeaderKey : orderHeaderKeySet){
      YFCElement orderEle = orderListEle.createChild(GCConstants.ORDER);
      orderEle.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
    }

    if(orderListEle.hasChildNodes()){
			String shipmentKey = confirmShipIndoc.getDocumentElement().getAttribute("ShipmentKey");
			outDoc.getDocumentElement().setAttribute("ShipmentKey", shipmentKey);
      GCCommonUtil.invokeService(env, GCConstants.GC_PRCS_TRNSFRORD_ON_BKORDER_QUEUE, outDoc);
    }

    Document finalPdfDoc = GCCommonUtil.invokeService(env, GCConstants.PACK_SLIP_FOR_SHIPBK_TO_DC, inputDoc);
    LOGGER.verbose("GCConfirmShipmentAPI.confirmShipment -- END");
    LOGGER.endTimer("GCConfirmShipmentAPI.confirmShipment");

    return finalPdfDoc;


  }
  /**
   *
   */
  @Override
  public void setProperties(Properties arg0) {

  }



}
