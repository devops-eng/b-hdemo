package com.gc.exstore.utils;

import java.util.ArrayList;
import java.util.List;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCLogUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCShipmentUtil {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCShipmentUtil.class.getName());

  private GCShipmentUtil() {

  }

  public static String getEmployeeID(YFSEnvironment env, String loginId) {

    LOGGER.beginTimer("GCShipmentUtil.getEmployeeID");
    LOGGER.verbose("GCShipmentUtil.getEmployeeID--Begin");

    LOGGER.verbose("loginId : " + loginId);
    final YFCDocument employeeIdDoc = YFCDocument.createDocument(GCConstants.USER);
    final YFCElement eleUserIndoc = employeeIdDoc.getDocumentElement();
    eleUserIndoc.setAttribute(GCConstants.LOGINID, loginId);

    // Call changeShipment API
    YFCDocument tempdoc = YFCDocument.getDocumentFor(GCConstants.GET_USER_HEIRARCHY_TEMPLATE);
    final YFCDocument employeeIdOutdoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_USER_HEIRARCHY_API, employeeIdDoc, tempdoc);
    final YFCElement eleUserOutDoc = employeeIdOutdoc.getDocumentElement();
    final YFCElement eleUserExtn = eleUserOutDoc.getChildElement(GCConstants.EXTN);

    LOGGER.verbose("GCShipmentUtil.getEmployeeID--End");
    LOGGER.endTimer("GCShipmentUtil.getEmployeeID");
    return eleUserExtn.getAttribute(GCConstants.EXTN_EMPLOYEE_ID);

  }

  public static List<String> getOrderLineKeyList(YFCNodeList<YFCElement> nlShipmentLine) {
    List<String> orderLineKeyList = new ArrayList<String>();
    for (YFCElement eleShipmentLine : nlShipmentLine) {
      String orderLineKey = eleShipmentLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      orderLineKeyList.add(orderLineKey);
    }
    return orderLineKeyList;
  }

  /**
   * Method to get order status as valid string, check if it has more than one decimal and convert
   * it into string that is valid for conversion into Double/Integer.
   *
   * @param strStatus
   * @return
   * */
  public static String checkAndConvertOrderStatus(String sStatus) {
    LOGGER.beginTimer("GCBeforeConsolidateToShipmentUE.checkAndConvertOrderStatus");
    GCLogUtils.verboseLog(LOGGER, "GCBeforeConsolidateToShipmentUE.checkAndConvertOrderStatus--Begin");
    int index = sStatus.lastIndexOf('.');
    while (index > 5) {
      StringBuilder sbStatus = new StringBuilder(sStatus);
      sbStatus.deleteCharAt(index);
      sStatus = sbStatus.toString();
      index = sStatus.lastIndexOf('.');
    }
    GCLogUtils.verboseLog(LOGGER, "Status after conversion", sStatus);
    GCLogUtils.verboseLog(LOGGER, "GCBeforeConsolidateToShipmentUE.checkAndConvertOrderStatus--End");
    LOGGER.endTimer("GCBeforeConsolidateToShipmentUE.checkAndConvertOrderStatus");
    return sStatus;
  }
}
