package com.gc.isccs.print;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gc.common.constants.GCConstants;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.core.YFSSystem;

public class GCDisplayPDFServlet extends HttpServlet {

	
	/**
	 * Default serial version UID.
	 */
	private static final long serialVersionUID = 1L;
	
	private YFCLogCategory logger = YFCLogCategory.instance(GCDisplayPDFServlet.class);

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		returnPDF(request, response);
	}

	private void returnPDF(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.beginTimer("DisplayPDFServlet.returnPDF");
		File pdfFile = null;
		try{
			
			String servletPath = request.getServletPath();
			String yfsPdfDir = YFSSystem.getProperty(GCConstants.PDF_DIR);
			String pdfFileName = yfsPdfDir + servletPath;
			logger.debug("Reading pdf file : " + pdfFileName);
			pdfFile = new File(pdfFileName);

			response.setContentType("application/pdf");

			response.addHeader("Content-Disposition", "inline; filename=" + pdfFileName);
			response.setContentLength((int) pdfFile.length());

			FileInputStream inputStream = new FileInputStream(pdfFile);

			OutputStream responseOutputStream = response.getOutputStream();
			int bytes = inputStream.read();
			while (bytes != -1) {
				responseOutputStream.write(bytes);
				bytes = inputStream.read();
			}
			inputStream.close();
			
		}finally{
			logger.endTimer("DisplayPDFServlet.returnPDF");
		}
	}
}
