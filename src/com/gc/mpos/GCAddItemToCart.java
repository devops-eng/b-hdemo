/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 ** #################################################################################################
 * ################################################################ OBJECTIVE: This class is called
 * when GCAddItemTomPOSCartService is invoked from mPOS. This class will check the item id
 * entered/scanned and connect with POS to validate it.
 * #################################################################################################
 * ################################################################ Version Date Modified By
 * Description
 * #################################################################################################
 * ################################################################ 1.0 03/24/2016 Mittal, Yashu
 * JIRA No:MPOS-27, MPOS-28, MPOS-198, MPOS-201, MPOS-202
 * #################################################################################################
 * ################################################################
 */
package com.gc.mpos;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.gc.api.GCGetItemAvailability;
import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:yashu.mittal@expicient.com">Yashu Mittal</a>
 */
public class GCAddItemToCart {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCAddItemToCart.class.getName());

  String storeId = "";
  boolean IsCompleteInventoryValidationFlag = false;
  boolean IsWebOrder = false;

  /**
   * This method will be called from mPOS to validate SKU of scanned/keyed in items.
   *
   * @param env
   * @param inDoc
   * @return docPOSResponse
   * @throws TransformerException
   *
   */
  public Document addItemToCart(YFSEnvironment env, Document inDoc) throws GCException {

    LOGGER.beginTimer("GCAddItemToCart.addItemToCart()");
    LOGGER.verbose("GCAddItemToCart - addItemToCart() : Start");

    if (LOGGER.isDebugEnabled()) {
      LOGGER.verbose("Input Doc to method addItemToCart:-->" + GCXMLUtil.getXMLString(inDoc));
    }
    
    
    // <!-----------------------------IsSPODisable="Y": for SPO DISABLING------------------------------->

    YFCDocument mPOSRequestInDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement rootElement = mPOSRequestInDoc.getDocumentElement();
    String sItemID = rootElement.getAttribute(GCConstants.ITEM_ID);
    storeId = rootElement.getAttribute(GCConstants.STORE_ID);
    String sIsSPODisable = rootElement.getAttribute("IsSPODisable");

    // Check inDoc root --> If "Order" --> then call is for CompleteInventoryValidation
    String sRootNodeName = rootElement.getNodeName();
    YFCDocument requestDocToPOS = YFCDocument.getDocumentFor("<ItemList/>");
    YFCDocument getItemDetails = YFCDocument.getDocumentFor("<ItemList/>");
    YFCDocument getOrderList = YFCDocument.getDocumentFor("<OrderList/>");
    if (YFCCommon.equalsIgnoreCase(sRootNodeName, GCConstants.ORDER)) {
      IsCompleteInventoryValidationFlag = true;
      String sOrderHeaderKey = rootElement.getAttribute(GCConstants.ORDER_HEADER_KEY);
      requestDocToPOS = getOrderList(env, sOrderHeaderKey, storeId);

      Document cloneDoc = (Document) requestDocToPOS.getDocument().cloneNode(true);
      Element eleOrderList = (Element) cloneDoc.getElementsByTagName(GCConstants.ORDER).item(0);
      Document cloneGetOrderList = GCXMLUtil.getDocumentFromElement(eleOrderList);
      getOrderList = YFCDocument.getDocumentFor(cloneGetOrderList);

      requestDocToPOS.getDocumentElement()
      .removeChild(requestDocToPOS.getDocumentElement().getElementsByTagName(GCConstants.ORDER_LIST).item(0));


      if (!requestDocToPOS.getDocumentElement().hasChildNodes()) {

        LOGGER.verbose("No G&G lines. SPO Order -->");
        boolean hasSPOInvShortHap = false;

        Document networkAvailabilityPresent = networkAvailability(env, getOrderList);
        LOGGER.verbose("Network availability is:-" + GCXMLUtil.getXMLString(networkAvailabilityPresent));

        Document cloningNADoc = (Document) networkAvailabilityPresent.cloneNode(true);
        Element elePromise = (Element) cloningNADoc.getElementsByTagName(GCConstants.PROMISE).item(0);
        Element elePromiseChild = (Element) elePromise.getElementsByTagName(GCConstants.PROMISE).item(0);
        Document clonePromiseDoc = GCXMLUtil.getDocumentFromElement(elePromiseChild);
        YFCDocument findInventory = YFCDocument.getDocumentFor(clonePromiseDoc);

        YFCDocument availableInventory = YFCDocument.getDocumentFor(networkAvailabilityPresent);
        availableInventory.getDocumentElement()
        .removeChild(availableInventory.getDocumentElement().getElementsByTagName(GCConstants.PROMISE).item(0));

        YFCNodeList<YFCElement> nlOrderLines = getOrderList.getElementsByTagName(GCConstants.ORDER_LINE);
        for (int d = 0; d < nlOrderLines.getLength(); d++) {
          YFCElement eleOrderLine = nlOrderLines.item(d);
          String sPrimeLineNo = eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
          String sProductClass = eleOrderLine.getChildElement(GCConstants.ITEM).getAttribute(GCConstants.PRODUCT_CLASS);
          String sOrderLineKitCode = eleOrderLine.getAttribute(GCConstants.KIT_CODE);

          Element eleItem = GCXMLUtil.getElementByXPath(availableInventory.getDocument(),
              "ItemList/Item[@LineId='" + sPrimeLineNo + "']");



          if (YFCCommon.equalsIgnoreCase(sProductClass, GCConstants.REGULAR)
              || (YFCCommon.equalsIgnoreCase(sProductClass, GCConstants.SET)
                  && YFCCommon.equalsIgnoreCase(sOrderLineKitCode, GCConstants.BUNDLE))) {
            String sOrderedQty = eleOrderLine.getAttribute(GCConstants.ORDERED_QTY);

            String sNetworkOnHandAvailability = "0";
            String sNetworkFutureAvailability = "0";
            String sFirstAvailableDate = "";
            if (!YFCCommon.isVoid(eleItem)) {
              sNetworkOnHandAvailability = eleItem.getAttribute("NetworkOnHandAvailability");
              sNetworkFutureAvailability = eleItem.getAttribute("NetworkFutureAvailability");
              sFirstAvailableDate = eleItem.getAttribute("FirstAvailableDate");
            }
            if (Double.parseDouble(sNetworkOnHandAvailability) < Double.parseDouble(sOrderedQty)) {
              hasSPOInvShortHap = true;
              eleOrderLine.setAttribute("StoreAvailability", "0");
              eleOrderLine.setAttribute("NetworkOnHandAvailability", sNetworkOnHandAvailability);
              eleOrderLine.setAttribute("NetworkFutureAvailability", sNetworkFutureAvailability);
              eleOrderLine.setAttribute("FirstAvailableDate", sFirstAvailableDate);
            } else {
              eleOrderLine.setAttribute("StoreAvailability", "0");
              eleOrderLine.setAttribute("NetworkOnHandAvailability", sNetworkOnHandAvailability);
              eleOrderLine.setAttribute("NetworkFutureAvailability", sNetworkFutureAvailability);
              eleOrderLine.setAttribute("FirstAvailableDate", sFirstAvailableDate);
            }
          }
        }

        if (hasSPOInvShortHap) {
          getOrderList.getDocumentElement().setAttribute(GCConstants.SUCCESS, "false");
          return getOrderList.getDocument();
        } else {
          getOrderList.getDocumentElement().setAttribute(GCConstants.SUCCESS, "true");
          YFCElement eleImportPromiseDoc = getOrderList.importNode(findInventory.getDocumentElement(), true);
          getOrderList.getDocumentElement().appendChild(eleImportPromiseDoc);
          return getOrderList.getDocument();
        }

      }
    }

    else {
      if (YFCCommon.equals(sItemID.length(), GCConstants.ENTERPRISE_ITEM_ID_LENGTH)) {
        boolean bDoesItemExists = false;
        LOGGER.verbose("Enterprise Item ID is " + sItemID);
        String sIsProCoverageItem = rootElement.getAttribute("IsProCoverageItem");
        LOGGER.verbose("Is Item ProCoverage " + sIsProCoverageItem);
        getItemDetails = getCompleteOMSItemList(env, sItemID, true);

        // If getPOSItemDetails is blank --> EnterpriseItemID not present --> POS Call
        if (!getItemDetails.getDocumentElement().hasChildNodes()) {
          LOGGER.verbose("Value entered is UPC and not EnterpriseItemID");
        } else {
          YFCElement eleExtn = getItemDetails.getDocumentElement().getElementsByTagName(GCConstants.EXTN).item(0);
        YFCElement elePrimaryInfo =
              getItemDetails.getDocumentElement().getElementsByTagName(GCConstants.PRIMARY_INFORMATION).item(0);
        String sPOSItemID = "";
          YFCElement elePrimItemExtn = checkIfSetParent(getItemDetails, rootElement, env);
        if (!YFCCommon.isVoid(elePrimItemExtn)) {
          GCXMLUtil.copyAttributes(elePrimItemExtn, eleExtn);
        }
        if (!YFCCommon.isVoid(elePrimaryInfo)) {
          String itemType = elePrimaryInfo.getAttribute(GCConstants.ITEM_TYPE);
          sPOSItemID = eleExtn.getAttribute(GCConstants.EXTN_POSITEM_ID);

            // <!------------------------------------------------------------------------------------------------------------------------------->

            if (!YFCCommon.equalsIgnoreCase(sIsSPODisable, "Y")) {
            if (YFCCommon.isVoid(sPOSItemID)) {
              LOGGER.verbose("POSItemID is not present --> WebOrder");
              IsWebOrder = true;
            }
            }
          String sBrandId = eleExtn.getAttribute("ExtnBrandId");
            String sPOSStatusCode = eleExtn.getAttribute("ExtnPOSStatusCode");

          YFCDocument docPOSResponse =
                checkIfRestrictedItems(env, sPOSItemID, itemType, sBrandId, storeId, sIsProCoverageItem,
                    sPOSStatusCode);
          if (YFCCommon.equalsIgnoreCase(docPOSResponse.getDocumentElement().getAttribute("Success"), "false")) {
              LOGGER.verbose("############# RESTRICTED ITEM ##############");
            return docPOSResponse.getDocument();
          }
        }

        if (!YFCCommon.isVoid(eleExtn)) {
          sPOSItemID = eleExtn.getAttribute(GCConstants.EXTN_POSITEM_ID);
          bDoesItemExists = true;
        }
        LOGGER.verbose("POS Item ID is " + sPOSItemID);

          if (!IsWebOrder) {
        if (YFCCommon.equals(sPOSItemID, GCConstants.BLANK) || !bDoesItemExists) {
          LOGGER.verbose(
              "No POSItem ID Exist in OMS for the provided enterprise item id. OR Enterprise item id does not exist. Send Error to mPOS");
          YFCDocument docPOSResponse =
              YFCDocument.getDocumentFor("<Response Success=''><Errorinfo Error=''/></Response>");
          docPOSResponse.getDocumentElement().setAttribute(GCConstants.SUCCESS, GCConstants.FALSE_STR);
          if (!bDoesItemExists) {
            docPOSResponse.getDocumentElement().getElementsByTagName("Errorinfo").item(0).setAttribute("Error",
                GCConstants.NO_ITEM_WITH_ENTERPRISE_ITEM_ID_EXIST_ERROR);
          } else {
            docPOSResponse.getDocumentElement().getElementsByTagName("Errorinfo").item(0).setAttribute("Error",
                GCConstants.NO_POS_ITEM_WITH_ENTERPRISE_ITEM_ID_EXIST_ERROR);
          }
          if (LOGGER.isDebugEnabled()) {
            LOGGER.verbose("Response Returned to mPOS from addItemToCart method-->" + docPOSResponse.toString());
          }
          return docPOSResponse.getDocument();
        } else {
          rootElement.setAttribute(GCConstants.ITEM_ID, sPOSItemID);
        }
      }
      }
      }
      else {
        getItemDetails = getPOSItemID(env, sItemID, false);
        YFCElement eleItemList = getItemDetails.getDocumentElement();
        YFCElement eleItem = eleItemList.getChildElement(GCConstants.ITEM);
        if (!YFCCommon.isVoid(eleItem)) {
          YFCElement elePrimItemExtn = checkIfSetParent(getItemDetails, rootElement, env);
          if (!YFCCommon.isVoid(elePrimItemExtn)) {
            String primItemId = elePrimItemExtn.getAttribute(GCConstants.EXTN_POSITEM_ID);
            rootElement.setAttribute(GCConstants.ITEM_ID, primItemId);
          }
        }

        if (YFCCommon.isVoid(eleItem)) {
          LOGGER.verbose("ItemID passed is OMS ItemID");
          getItemDetails = getCompleteOMSItemList(env, sItemID, false);
          if (getItemDetails.getDocumentElement().hasChildNodes()) {
            String sPOSItemID =
                getItemDetails.getElementsByTagName(GCConstants.EXTN).item(0).getAttribute(GCConstants.EXTN_POSITEM_ID);

            // <!--------------------------------------------------------------------------------------------------------------->
            if (!YFCCommon.equalsIgnoreCase(sIsSPODisable, "Y")) {
            if (YFCCommon.isVoid(sPOSItemID)) {
              IsWebOrder = true;

              }
            }
          }
        }
      }

      YFCElement eleInDocImportToRequestDoc = requestDocToPOS.importNode(mPOSRequestInDoc.getDocumentElement(), true);
      requestDocToPOS.getDocumentElement().appendChild(eleInDocImportToRequestDoc);
    }

    YFCDocument docPOSResponse = null;
    if (!IsWebOrder) {
      docPOSResponse = validatePOSSKU(env, requestDocToPOS, getOrderList, sIsSPODisable);
    }
    if (IsWebOrder) {
      LOGGER.verbose("Processing WebOrders requirements");
      if (getItemDetails.hasChildNodes()) {
        Document networkAvailaibilityInput = GCXMLUtil.createDocument(GCConstants.ITEM);
        Element eleInput = networkAvailaibilityInput.getDocumentElement();
        
        YFCElement eleItemTag = getItemDetails.getElementsByTagName(GCConstants.ITEM).item(0);
        sItemID = eleItemTag.getAttribute(GCConstants.ITEM_ID);
        eleInput.setAttribute(GCConstants.ITEM_ID, sItemID);

        YFCElement elePrimaryInfo = getItemDetails.getElementsByTagName(GCConstants.PRIMARY_INFORMATION).item(0);
        String sUOM = elePrimaryInfo.getAttribute(GCConstants.UNIT_OF_MEASURE);
        String sDefaultProductClass = elePrimaryInfo.getAttribute(GCXmlLiterals.DEFAULT_PRODUCT_CLASS);
        String sKitCode = elePrimaryInfo.getAttribute(GCConstants.KIT_CODE);
        YFCElement eleComputedPrice = getItemDetails.getElementsByTagName(GCConstants.COMPUTED_PRICE).item(0);
        if (YFCCommon.isVoid(eleComputedPrice)) {
          YFCDocument priceNotSet =
              YFCDocument.getDocumentFor("<Response Success='false'><Errorinfo Error=''/></Response>");
          priceNotSet.getDocumentElement().getElementsByTagName("Errorinfo").item(0).setAttribute("Error",
              "Please process this item in POS.");
          return priceNotSet.getDocument();
        }
        String sUnitPrice = eleComputedPrice.getAttribute(GCConstants.UNIT_PRICE);
		
		
		YFCElement eleItem = getItemDetails.getElementsByTagName(GCConstants.ITEM).item(0);
        YFCElement eleExtn = eleItem.getElementsByTagName(GCConstants.EXTN).item(0);
        if(!YFCCommon.isVoid(eleExtn))
        { 
        	String sExtnIsClearanceItem = eleExtn.getAttribute("ExtnIsClearanceItem");
        	if(YFCCommon.equalsIgnoreCase(sExtnIsClearanceItem, GCConstants.YES))
        	{
        		eleInput.setAttribute("ExtnIsStoreClearance", GCConstants.YES);
        	}
        }
		
		
        eleInput.setAttribute("UnitOfMeasure", GCConstants.EACH);
        eleInput.setAttribute("ProductClass", sDefaultProductClass);
        eleInput.setAttribute("UnitPrice", sUnitPrice);
        eleInput.setAttribute("StoreID", storeId);
        eleInput.setAttribute(GCConstants.KIT_CODE, sKitCode);

        Document networkAvailaibility =
            GCCommonUtil.invokeService(env, "GCGetNetworkAvailabilityService", networkAvailaibilityInput);

        getItemDetails.getDocumentElement().getChildElement(GCConstants.ITEM).setAttribute("NetworkOnHandAvailability",
            networkAvailaibility.getDocumentElement().getAttribute("NetworkOnHandAvailability"));
        getItemDetails.getDocumentElement().getChildElement(GCConstants.ITEM).setAttribute("NetworkFutureAvailability",
            networkAvailaibility.getDocumentElement().getAttribute("NetworkFutureAvailability"));
        getItemDetails.getDocumentElement().getChildElement(GCConstants.ITEM).setAttribute("FirstAvailableDate",
            networkAvailaibility.getDocumentElement().getAttribute("FirstAvailableDate"));
        getItemDetails.getDocumentElement().getChildElement(GCConstants.ITEM).setAttribute("StoreAvailability",
            GCConstants.ZERO);

        docPOSResponse = YFCDocument.getDocumentFor("<Response Success=''><Errorinfo Error=''/></Response>");
        docPOSResponse.getDocumentElement().setAttribute(GCConstants.SUCCESS, "true");
        YFCElement eleImportItemDetails = docPOSResponse.importNode(getItemDetails.getDocumentElement(), true);
        docPOSResponse.getDocumentElement().appendChild(eleImportItemDetails);
      }

    }

    // List Price check not for Inventory validation
    if (!IsCompleteInventoryValidationFlag) {
      YFCElement eleOutput = docPOSResponse.getDocumentElement();
      String response = eleOutput.getAttribute(GCConstants.SUCCESS);
      if (YFCUtils.equals(response, GCConstants.TRUE)) {
        YFCElement eleItemList = eleOutput.getChildElement(GCConstants.ITEM_LIST);
        YFCElement eleItem = eleItemList.getElementsByTagName(GCConstants.ITEM).item(0);
        if (!YFCCommon.isVoid(eleItem)) {
          YFCElement eleComputedPrice = eleItem.getChildElement("ComputedPrice");
          YFCElement eleItemExtn = eleItem.getChildElement("Extn");
          String listPrice = null;
          if (!YFCCommon.isVoid(eleComputedPrice)) {
            listPrice = eleComputedPrice.getAttribute(GCConstants.LIST_PRICE);
            String unitPrice = eleComputedPrice.getAttribute(GCConstants.UNIT_PRICE);
            if (YFCCommon.isVoid(listPrice)) {
              LOGGER.verbose("Price not defined for item in OMS");
              YFCDocument docPOSResponsePrice =
                  YFCDocument.getDocumentFor("<Response Success='false'><Errorinfo Error=''/></Response>");
              docPOSResponsePrice.getDocumentElement().getElementsByTagName("Errorinfo").item(0).setAttribute("Error",
                  GCConstants.NO_ITEM_WITH_POS_ITEM_ID_EXIST_ERROR);
              return docPOSResponsePrice.getDocument();
            } else {
              String sListPrice = formatPrice(listPrice);
              eleComputedPrice.setAttribute(GCConstants.LIST_PRICE, sListPrice);
            }
            String sUnittPrice = formatPrice(unitPrice);
            eleComputedPrice.setAttribute(GCConstants.UNIT_PRICE, sUnittPrice);
          }
          if (!YFCCommon.isVoid(eleItemExtn)) {
            String inventoryCost = eleItemExtn.getAttribute("ExtnInventoryCost");
            String sInventoryCost = formatPrice(inventoryCost);
            eleItemExtn.setAttribute("ExtnInventoryCost", sInventoryCost);

            String minSellPrice = eleItemExtn.getAttribute("ExtnMinSellPrice");
            String sMinSellPrice = formatPrice(minSellPrice);
            eleItemExtn.setAttribute("ExtnMinSellPrice", sMinSellPrice);
          }
        }
      }
      LOGGER.endTimer("GCAddItemToCart.addItemToCart()");
      LOGGER.verbose("GCAddItemToCart - addItemToCart() : End");

      if (LOGGER.isDebugEnabled()) {
        LOGGER.verbose("Response Returned to mPOS from addItemToCart method-->" + docPOSResponse.toString());
      }
    }
    return docPOSResponse.getDocument();

  }

  private YFCElement checkIfSetParent(YFCDocument getPOSItemDetails, YFCElement rootElement, YFSEnvironment env) {
    YFCElement eleItemExtn = null;
    YFCElement elePrimaryInfo =
        getPOSItemDetails.getDocumentElement().getElementsByTagName(GCConstants.PRIMARY_INFORMATION).item(0);
    if (!YFCCommon.isVoid(elePrimaryInfo)) {
      String productClass = elePrimaryInfo.getAttribute("DefaultProductClass");
      String kitCode = elePrimaryInfo.getAttribute(GCConstants.KIT_CODE);
      if (YFCUtils.equals(productClass, GCConstants.SET) && YFCUtils.equals(kitCode, GCConstants.BUNDLE)) {
        YFCElement eleExtn = getPOSItemDetails.getDocumentElement().getElementsByTagName(GCConstants.EXTN).item(0);
        String primaryComp = eleExtn.getAttribute(GCConstants.EXTN_PRIMARY_COMPONENT_ID);
        YFCDocument docGetItemListIP = YFCDocument.createDocument(GCConstants.ITEM);
        YFCElement eleGetItemListIP = docGetItemListIP.getDocumentElement();
        eleGetItemListIP.setAttribute(GCConstants.ITEM_ID, primaryComp);
        eleGetItemListIP.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GCI);
        eleGetItemListIP.setAttribute(GCConstants.UOM, GCConstants.EACH);
        eleGetItemListIP.setAttribute(GCConstants.IGNORE_IS_SOLD_SEPERATELY, GCConstants.YES);   //mPOS-1723: Fix.
        YFCDocument getItemListTemp = YFCDocument
            .getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_getItemListTemplate.xml"));
        YFCDocument getItemListOut =
            GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ITEM_LIST, docGetItemListIP, getItemListTemp);
        eleItemExtn = getItemListOut.getDocumentElement().getElementsByTagName(GCConstants.EXTN).item(0);
        String posItemId = eleItemExtn.getAttribute(GCConstants.EXTN_POSITEM_ID);
        rootElement.setAttribute(GCConstants.ITEM_ID, posItemId);

      }
    }
    return eleItemExtn;
  }

  /**
   * This method will prepare POS SKU validation input and make a web service call to POS to get the
   * response.
   *
   * @param mPOSRequesInDoc
   * @return docPOSResponse
   * @throws TransformerException
   * @throws GCException
   */
  public YFCDocument validatePOSSKU(YFSEnvironment env, YFCDocument mPOSRequesInDoc, YFCDocument getOrderList,
      String sIsSPODisable)
      throws GCException {

    LOGGER.beginTimer("GCAddItemToCart.addItemToCart()");
    LOGGER.verbose("GCAddItemToCart - addItemToCart() : Start");
    boolean bHasPOSCallFailed = false;
    YFCElement eleRequest = mPOSRequesInDoc.getDocumentElement();
    if (YFCCommon.isVoid(storeId)) {
      storeId = eleRequest.getAttribute(GCConstants.STORE_ID);
    }

    YFCDocument docPOSSKUValidateRequest = YFCDocument.getDocumentFor(
        GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_POS_SKU_Validation_Request_Template.xml"));

    // deleting Product Element from template imported
    YFCElement eleProducts = docPOSSKUValidateRequest.getElementsByTagName("Products").item(0);
    YFCNodeList nlItemElements = mPOSRequesInDoc.getElementsByTagName(GCConstants.ITEM);
    for (int y = 0; y < nlItemElements.getLength(); y++) {
      String sLineNumberCounter = Integer.toString(y + 1);

      YFCElement eleProduct = docPOSSKUValidateRequest.createElement("Product");
      eleProduct.setAttribute("type", "Regular");
      YFCElement eleItem = (YFCElement) nlItemElements.item(y);
      String sItemID = eleItem.getAttribute(GCConstants.ITEM_ID);

      YFCElement eleLineNumber = eleProducts.createChild("LineNumber");
      YFCElement elePOSLocation = eleProducts.createChild("POSLocation");
      YFCElement eleInvInput = eleProducts.createChild("Inv.input");

      eleLineNumber.setNodeValue(sLineNumberCounter);
      elePOSLocation.setNodeValue(storeId);
      eleInvInput.setNodeValue(sItemID);

      eleProduct.appendChild(eleLineNumber);
      eleProduct.appendChild(eleInvInput);
      eleProduct.appendChild(elePOSLocation);
      eleProducts.appendChild(eleProduct);

    }



    if (LOGGER.isDebugEnabled()) {
      LOGGER.verbose("Request Doc sent to POS-->" + docPOSSKUValidateRequest.toString());
    }


    YFCDocument docPOSSKUValidationRespose =
        GCCommonUtil.invokeService(env, "GCValidatePOSSKUService", docPOSSKUValidateRequest);



    if (LOGGER.isDebugEnabled()) {
      LOGGER.verbose("Resonse received from POS-->" + docPOSSKUValidationRespose.toString());
    }

    String sValidateResult = GCConstants.BLANK;
    if (!YFCCommon.isVoid(docPOSSKUValidationRespose)) {
      NodeList nlValidateSKUResultList =
          docPOSSKUValidationRespose.getDocument().getElementsByTagName("ValidateSKUResult");
      NodeList nlFaultString =
          docPOSSKUValidationRespose.getDocument().getElementsByTagName("faultstring");

      if (nlValidateSKUResultList.getLength() > 0) {
        Node nValidateSKUResult = nlValidateSKUResultList.item(0);
        if (YFCCommon.isVoid(nValidateSKUResult)) {
          bHasPOSCallFailed = true;
        } else {
          sValidateResult = nValidateSKUResult.getTextContent();
          if (YFCCommon.isStringVoid(sValidateResult)) {
            bHasPOSCallFailed = true;
          }
        }
      } else if (nlFaultString.getLength() > 0) {
        bHasPOSCallFailed = true;
      }
    } else {
      bHasPOSCallFailed = true;
    }

    if (bHasPOSCallFailed) {
      LOGGER.verbose("OMS Could not connect to POS. or Response recevied from POS is fault string");
      YFCDocument docPOSResponse = YFCDocument.getDocumentFor("<Response Success=''><Errorinfo Error=''/></Response>");
      docPOSResponse.getDocumentElement().setAttribute(GCConstants.SUCCESS, GCConstants.FALSE_STR);
      docPOSResponse.getDocumentElement().getElementsByTagName("Errorinfo").item(0).setAttribute("Error",
          GCConstants.POS_CONNECTION_ERROR);
      return docPOSResponse;
    }

    YFCDocument docPOSValidationResponse = YFCDocument.getDocumentFor(sValidateResult);
    if (!YFCCommon.isVoid(eleRequest.getAttribute("TotalAvailability"))) {
      return docPOSValidationResponse;
    }
    YFCDocument docPOSResponse = null;
    if (IsCompleteInventoryValidationFlag) {
      docPOSResponse =
          processPOSInventoryValidateResponse(env, docPOSValidationResponse, mPOSRequesInDoc, getOrderList, sIsSPODisable);
    } else {
      docPOSResponse = processPOSSKUValidateResponse(env, docPOSValidationResponse, mPOSRequesInDoc, sIsSPODisable);
    }

    LOGGER.endTimer("GCAddItemToCart.validatePOSSKU()");
    LOGGER.verbose("GCAddItemToCart - validatePOSSKU() : End");

    return docPOSResponse;

  }

  /**
   * This method will process the validation response from POS and return response to mPOS.
   *
   * @param docPOSSKUValidationRespose
   * @return docPOSResponse
   *
   */

  private YFCDocument processPOSSKUValidateResponse(YFSEnvironment env, YFCDocument docPOSSKUValidationRespose,
      YFCDocument mPOSRequesInDoc, String sIsSPODisable) {

    LOGGER.beginTimer("GCAddItemToCart.addItemToCart()");
    LOGGER.verbose("GCAddItemToCart - addItemToCart() : Start");

    YFCDocument docPOSResponse = YFCDocument.getDocumentFor("<Response Success=''><Errorinfo Error=''/></Response>");
    YFCElement elePOSSKUValidationRoot = docPOSSKUValidationRespose.getDocumentElement();
    YFCElement eleSuccess = elePOSSKUValidationRoot.getElementsByTagName(GCConstants.SUCCESS).item(0);
    String sSuccess = eleSuccess.getNodeValue();
    YFCElement elePOSResponseRoot = docPOSResponse.getDocumentElement();
    // Adding flag for Success = false && ErrorInfo = Quantity not available
    boolean bIsQtyNotAvailable = false;
    YFCElement eleErrorReason = docPOSSKUValidationRespose.getElementsByTagName("ErrorInfo").item(0);
    String sErrorReason = "";
    if (!YFCCommon.isVoid(eleErrorReason)) {
      sErrorReason = eleErrorReason.getNodeValue();
    }
    if (YFCCommon.equalsIgnoreCase(sErrorReason, "Quantity is not available.")
        && YFCCommon.equalsIgnoreCase(sSuccess, "false")) {
      bIsQtyNotAvailable = true;
    }

    // <!------------------------------------------------------------------------------------------------------------------------->

    if (YFCCommon.equalsIgnoreCase(sIsSPODisable, "Y")) {
      bIsQtyNotAvailable = false;
    }

    if (YFCCommon.equals(sSuccess, GCConstants.TRUE) || bIsQtyNotAvailable) {
      LOGGER.verbose("Successful response is received from POS.");
      elePOSResponseRoot.setAttribute(GCConstants.SUCCESS, GCConstants.TRUE);
      String sSKU = elePOSSKUValidationRoot.getElementsByTagName(GCConstants.SKU).item(0).getNodeValue();
      String sQuantity = "";
      String sSKUNumber = "";
      String sSerialNumber = "";
      if (!bIsQtyNotAvailable) {
        sQuantity = elePOSSKUValidationRoot.getElementsByTagName(GCConstants.QUANTITY).item(0).getNodeValue();
        sSKUNumber = elePOSSKUValidationRoot.getElementsByTagName("SKU.Number").item(0).getNodeValue();
        sSerialNumber = elePOSSKUValidationRoot.getElementsByTagName("SerialNumber").item(0).getNodeValue();
      }
      YFCDocument getItemDetails = getItemDetails(env, sSKU);
      YFCElement eleItemElement = getItemDetails.getElementsByTagName(GCConstants.ITEM).item(0);
      if (YFCCommon.isVoid(eleItemElement)) {
        LOGGER.verbose("ItemID received from POS does not exist in OMS. Return error to POS");
        docPOSResponse.getDocumentElement().setAttribute(GCConstants.SUCCESS, GCConstants.FALSE_STR);
        docPOSResponse.getDocumentElement().getElementsByTagName("Errorinfo").item(0)
            .setAttribute("Error", GCConstants.NO_ITEM_WITH_POS_ITEM_ID_EXIST_ERROR);
        return docPOSResponse;
      }
      String sPrimCompItemID = eleItemElement.getAttribute(GCConstants.ITEM_ID);
      String sPrimCompItemKey = eleItemElement.getAttribute("ItemKey");
      YFCDocument dSETParentItemDetails =
          getSETParentItemDetails(env, sPrimCompItemID, sPrimCompItemKey);
      YFCElement eleParentItemList = dSETParentItemDetails.getDocumentElement();

      YFCElement eleComponent = docPOSSKUValidationRespose.getElementsByTagName("Component").item(0);
      if (!YFCCommon.isVoid(eleComponent)){
        LOGGER.verbose("Scanned item is primary Comp of a set item. Fetch SET Parent");

        // get primary component details using OMS ItemID to get SET parent details
        YFCElement eleItem = eleParentItemList.getElementsByTagName(GCConstants.ITEM).item(0);
        if (YFCCommon.isVoid(eleItem)) {
          LOGGER.verbose("ItemID received from POS does not exist in OMS. Return error to POS");
          docPOSResponse.getDocumentElement().setAttribute(GCConstants.SUCCESS, GCConstants.FALSE_STR);
          docPOSResponse.getDocumentElement().getElementsByTagName("Errorinfo").item(0).setAttribute("Error",
              GCConstants.NO_ITEM_WITH_POS_ITEM_ID_EXIST_ERROR);
          return docPOSResponse;
        }
        String parentItemId = eleItem.getAttribute(GCConstants.ITEM_ID);
        Document networkAvailIpDoc = GCXMLUtil.createDocument(GCConstants.ITEM);
        Element eleInput = networkAvailIpDoc.getDocumentElement();
        eleInput.setAttribute(GCConstants.ITEM_ID, parentItemId);
        if (bIsQtyNotAvailable) {
          YFCElement elePrimaryInfo = eleItem.getElementsByTagName(GCConstants.PRIMARY_INFORMATION).item(0);
          String sUOM = eleItem.getAttribute(GCConstants.UNIT_OF_MEASURE);
          String kitCode = "";
          kitCode = elePrimaryInfo.getAttribute(GCConstants.KIT_CODE);
          String sDefaultProductClass = elePrimaryInfo.getAttribute(GCXmlLiterals.DEFAULT_PRODUCT_CLASS);
          YFCElement eleComputedPrice = eleItem.getElementsByTagName(GCConstants.COMPUTED_PRICE).item(0);
          String sUnitPrice = eleComputedPrice.getAttribute(GCConstants.UNIT_PRICE);


          YFCElement eleExtn = eleParentItemList.getElementsByTagName(GCConstants.EXTN).item(0);
          if(!YFCCommon.isVoid(eleExtn))
          {
        	  String sExtnIsClearanceItem = eleExtn.getAttribute("ExtnIsClearanceItem");
        	  if(YFCCommon.equalsIgnoreCase(sExtnIsClearanceItem, GCConstants.YES))
              {
                eleInput.setAttribute("ExtnIsStoreClearance", GCConstants.YES);
              }
          }
          


          eleInput.setAttribute("UnitOfMeasure", sUOM);
          eleInput.setAttribute("ProductClass", sDefaultProductClass);
          eleInput.setAttribute("UnitPrice", sUnitPrice);
          eleInput.setAttribute("StoreID", storeId);
          eleInput.setAttribute("KitCode", kitCode);
          Document networkAvDoc = GCCommonUtil.invokeService(env, "GCGetNetworkAvailabilityService", networkAvailIpDoc);
          Element eleNetworkAvDoc = networkAvDoc.getDocumentElement();
          eleItem.setAttribute("NetworkOnHandAvailability",
              eleNetworkAvDoc.getAttribute("NetworkOnHandAvailability"));
          eleItem.setAttribute("NetworkFutureAvailability",
              eleNetworkAvDoc.getAttribute("NetworkFutureAvailability"));
          eleItem.setAttribute("FirstAvailableDate", eleNetworkAvDoc.getAttribute("FirstAvailableDate"));
          eleItem.setAttribute("StoreAvailability", GCConstants.ZERO);

        }
        if (!bIsQtyNotAvailable) {
        //mPOS-1431 Start.
          int nPrimaryAvailQty = 0, nSecondaryAvailQty = 0;
          int nPrimaryPOSQty = 0, nSecondaryPOSQty = 0; 
          int nPrimaryKitQty = 0, nSecondaryKitQty = 0;
          
          String sPrimaryPOSQty = sQuantity;
          String sSecondaryPOSQty = eleComponent.getElementsByTagName("cQuantity").item(0).getNodeValue();
          YFCElement eleSetRoot = dSETParentItemDetails.getDocumentElement();
          YFCElement eleSetExtn = eleSetRoot.getElementsByTagName(GCConstants.EXTN).item(0);
          String ExtnPrimaryComponentItemID = eleSetExtn.getAttribute("ExtnPrimaryComponentItemID");
          
          if(!YFCCommon.isVoid(sPrimaryPOSQty))
          {
        	  nPrimaryPOSQty = Integer.parseInt(sPrimaryPOSQty);
          }
          
          if(!YFCCommon.isVoid(sSecondaryPOSQty))
          {
        	  nSecondaryPOSQty = Integer.parseInt(sSecondaryPOSQty);
          }
          
          YFCNodeList<YFCElement> nlComponent = eleParentItemList.getElementsByTagName(GCConstants.COMPONENT);
          for(YFCElement eleComponentItems : nlComponent)
          {
        	 String sComponentItemID = eleComponentItems.getAttribute(GCConstants.COMPONENT_ITEM_ID);
        	 if(!YFCCommon.isVoid(sComponentItemID))
        	 {
        		 if(YFCCommon.equalsIgnoreCase(sComponentItemID, ExtnPrimaryComponentItemID))
        		 {
        			 nPrimaryKitQty = Integer.parseInt(eleComponentItems.getAttribute(GCConstants.KIT_QUANTITY));
        			 nPrimaryAvailQty = nPrimaryPOSQty/nPrimaryKitQty; 
        		 }
        		 
        		 else
        		 {
        			 nSecondaryKitQty = Integer.parseInt(eleComponentItems.getAttribute(GCConstants.KIT_QUANTITY));
        			 nSecondaryAvailQty = nSecondaryPOSQty/nSecondaryKitQty;
        		 }
        	 }
          }
          
          int nSetStoreQty = Math.min(nPrimaryAvailQty, nSecondaryAvailQty);
          String sSetStoreAvailability = String.valueOf(nSetStoreQty);
          eleItem.setAttribute("StoreAvailability", sSetStoreAvailability);
          
        //mPOS-1431 End.
        }
        eleItem.setAttribute("IsBundleItem", GCConstants.YES);

        YFCNodeList<YFCElement> nlComponents = eleParentItemList.getElementsByTagName(GCConstants.COMPONENT);
        YFCNodeList<YFCElement> nlResponseComponents =
            docPOSSKUValidationRespose.getElementsByTagName(GCConstants.COMPONENT);

        // Declaring map which contains ExtnPOSItemID as key and sSKU.Number + cQuantity as value
        HashMap<String, String> responseComponents = new HashMap();

        for (int y = 0; y < nlResponseComponents.getLength(); y++) {
          YFCElement eleResponseElement = nlResponseComponents.item(y);
          YFCElement eleCSKU = eleResponseElement.getElementsByTagName("cSKU").item(0);
          YFCElement eleCSKU_Number = eleResponseElement.getElementsByTagName("cSKU.NUMBER").item(0);
          YFCElement eleCQuantity = eleResponseElement.getElementsByTagName("cQuantity").item(0);

          String sCKSU = eleCSKU.getNodeValue();
          String sCSKU_Number = eleCSKU_Number.getNodeValue();
          String sCQuantity = eleCQuantity.getNodeValue();
          String sCSKUNum_Quan = sCSKU_Number + "|" + sCQuantity;
          responseComponents.put(sCKSU, sCSKUNum_Quan);
        }


        // Looping over Parent components to set attributes and append component item details
        for (int i = 0; i < nlComponents.getLength(); i++) {
          YFCElement eleComponents = nlComponents.item(i);

          String sComponentItemID = eleComponents.getAttribute(GCConstants.COMPONENT_ITEM_ID);
          YFCDocument componentDetails = getComponentPOSItemID(env, sComponentItemID);
          YFCElement eleComponentItemDetails = componentDetails.getElementsByTagName("Item").item(0);
          String sComponentPOSItemID =
              componentDetails.getElementsByTagName("Extn").item(0).getAttribute("ExtnPOSItemID");

          if (responseComponents.containsKey(sComponentPOSItemID)) {
            String sValuesToSet = responseComponents.get(sComponentPOSItemID);
            eleComponents.setAttribute("SKUNumber", sValuesToSet.substring(0, sValuesToSet.indexOf("|")));
            eleComponents.setAttribute("StoreAvailability", sValuesToSet.substring(sValuesToSet.indexOf("|") + 1));

            YFCElement eleImportComponentItemDetails = dSETParentItemDetails.importNode(eleComponentItemDetails, true);
            eleComponents.appendChild(eleImportComponentItemDetails);
          }
          else if (YFCCommon.equalsIgnoreCase(sComponentPOSItemID, sSKU)) {
            eleComponents.setAttribute("IsPrimaryComponent", GCConstants.YES);
            eleComponents.setAttribute("SKUNumber", sSKUNumber);
            eleComponents.setAttribute("SerialNumber", sSerialNumber);
            
            if (bIsQtyNotAvailable)
            {
            	eleComponents.setAttribute("StoreAvailability", GCConstants.ZERO);
            }
            
            else
            {
            	eleComponents.setAttribute("StoreAvailability", sQuantity);
            }
           

            YFCElement eleImportComponentItemDetails = dSETParentItemDetails.importNode(eleItemElement, true);
            eleComponents.appendChild(eleImportComponentItemDetails);
          } 
          
          else if (bIsQtyNotAvailable)
          {        	
        	eleComponents.setAttribute("StoreAvailability", GCConstants.ZERO);
        	YFCElement eleImportComponentItemDetails = dSETParentItemDetails.importNode(eleComponentItemDetails, true);
            eleComponents.appendChild(eleImportComponentItemDetails);
          }
          
          else {
            LOGGER.verbose("Extn POS ItemID not present in OMS");
          }

        }
        YFCElement eleImportNode = docPOSResponse.importNode(eleParentItemList, true);
        elePOSResponseRoot.appendChild(eleImportNode);

      } else {

        LOGGER.verbose("Scanned Item is a regular item");
        YFCElement eleItemList = getItemDetails.getDocumentElement();
        YFCElement eleItem = eleItemList.getElementsByTagName(GCConstants.ITEM).item(0);
        if (!YFCCommon.isVoid(eleItem)) {
          YFCElement elePrimaryInfo = eleItem.getElementsByTagName(GCConstants.PRIMARY_INFORMATION).item(0);
          if (!YFCCommon.isVoid(elePrimaryInfo)) {
            Document networkAvailIpDoc = GCXMLUtil.createDocument(GCConstants.ITEM);
            Element eleInput = networkAvailIpDoc.getDocumentElement();
            String itemID = eleItem.getAttribute(GCConstants.ITEM_ID);
            eleInput.setAttribute(GCConstants.ITEM_ID, itemID);
            if (bIsQtyNotAvailable) {
              String sDefaultProductClass = elePrimaryInfo.getAttribute(GCXmlLiterals.DEFAULT_PRODUCT_CLASS);
              String kitCode = "";
              kitCode = elePrimaryInfo.getAttribute(GCConstants.KIT_CODE);
              YFCElement eleComputedPrice = eleItem.getElementsByTagName(GCConstants.COMPUTED_PRICE).item(0);
              String sUnitPrice = eleComputedPrice.getAttribute(GCConstants.UNIT_PRICE);
              eleInput.setAttribute("UnitOfMeasure", GCConstants.EACH);
              eleInput.setAttribute("ProductClass", sDefaultProductClass);
              eleInput.setAttribute("UnitPrice", sUnitPrice);
              eleInput.setAttribute("StoreID", storeId);
              eleInput.setAttribute("KitCode", kitCode);


              YFCElement eleExtn = eleItem.getElementsByTagName(GCConstants.EXTN).item(0);
              if(!YFCCommon.isVoid(eleExtn))
              {
            	  String sExtnIsClearanceItem = eleExtn.getAttribute("ExtnIsClearanceItem");
                  if(YFCCommon.equalsIgnoreCase(sExtnIsClearanceItem, GCConstants.YES))
                  {
                    eleInput.setAttribute("ExtnIsStoreClearance", GCConstants.YES);
                  }
  
              }
              

              Document networkAvDoc =
                  GCCommonUtil.invokeService(env, "GCGetNetworkAvailabilityService", networkAvailIpDoc);
              Element eleNetworkAvDoc = networkAvDoc.getDocumentElement();

              eleItem.setAttribute("NetworkOnHandAvailability",
                  eleNetworkAvDoc.getAttribute("NetworkOnHandAvailability"));
              eleItem.setAttribute("NetworkFutureAvailability",
                  eleNetworkAvDoc.getAttribute("NetworkFutureAvailability"));
              eleItem.setAttribute("FirstAvailableDate", eleNetworkAvDoc.getAttribute("FirstAvailableDate"));
              eleItem.setAttribute("StoreAvailability", GCConstants.ZERO);
            }
            String itemType = elePrimaryInfo.getAttribute(GCConstants.ITEM_TYPE);
            String sBrandId = eleItemList.getElementsByTagName(GCConstants.EXTN).item(0).getAttribute("ExtnBrandId");
            String sPOSStatusCode =
                eleItemList.getElementsByTagName(GCConstants.EXTN).item(0).getAttribute("ExtnPOSStatusCode");
            String sPOSItemId =
                eleItemList.getElementsByTagName(GCConstants.EXTN).item(0).getAttribute(GCConstants.EXTN_POSITEM_ID);
            String sIsProCoverageItem =
                mPOSRequesInDoc.getElementsByTagName("Item").item(0).getAttribute("IsProCoverageItem");
            YFCDocument docToReturn =
                checkIfRestrictedItems(env, sPOSItemId, itemType, sBrandId, storeId, sIsProCoverageItem,
                    sPOSStatusCode);
            if (YFCCommon.equalsIgnoreCase(docToReturn.getDocumentElement().getAttribute("Success"), "false")) {
              LOGGER.verbose("############# RESTRICTED ITEM ##############");
              return docToReturn;
            }
          }
          if (!bIsQtyNotAvailable) {
            eleItem.setAttribute("StoreAvailability", sQuantity);
          }
          eleItem.setAttribute("IsBundleItem", GCConstants.NO);
          eleItem.setAttribute("SKUNumber", sSKUNumber);
          eleItem.setAttribute("SerialNumber", sSerialNumber);
          YFCElement eleImportNode = docPOSResponse.importNode(eleItemList, true);
          elePOSResponseRoot.appendChild(eleImportNode);
        } else {
          LOGGER.verbose("ItemID received from POS does not exist in OMS. Return error to POS");
          docPOSResponse.getDocumentElement().setAttribute(GCConstants.SUCCESS, GCConstants.FALSE_STR);
          docPOSResponse.getDocumentElement().getElementsByTagName("Errorinfo").item(0).setAttribute("Error",
              GCConstants.NO_ITEM_WITH_POS_ITEM_ID_EXIST_ERROR);
          return docPOSResponse;
        }
      }
    } else {
      LOGGER.verbose("Error response received from POS");
      elePOSResponseRoot.setAttribute(GCConstants.SUCCESS, GCConstants.FALSE_STR);
      String sErrorInfo = elePOSSKUValidationRoot.getElementsByTagName("ErrorInfo").item(0).getNodeValue();
      elePOSResponseRoot.getElementsByTagName("Errorinfo").item(0).setAttribute("Error",
          sErrorInfo);
    }


    LOGGER.endTimer("GCAddItemToCart.processPOSSKUValidateResponse()");
    LOGGER.verbose("GCAddItemToCart - processPOSSKUValidateResponse() : End");

    return docPOSResponse;

  }

  /**
   *
   * This method will get the Enterprise item id and returned corresponding POS Item ID.
   *
   * @param sEnterpriseItemID
   * @return sPOSItemID
   *
   */
  public static YFCDocument getPOSItemID(YFSEnvironment env, String sEnterpriseItemID, boolean isEnterpriseId) {

    LOGGER.beginTimer("GCAddItemToCart.addItemToCart()");
    LOGGER.verbose("GCAddItemToCart - addItemToCart() : Start");
    YFCDocument getItemListInputDoc;
    if (isEnterpriseId) {
      getItemListInputDoc =
          YFCDocument.getDocumentFor("<Item IgnoreIsSoldSeparately='Y'><Extn ExtnEnterpriseItemID='" + sEnterpriseItemID + "'/></Item>");
    } else {
      getItemListInputDoc =
          YFCDocument.getDocumentFor("<Item IgnoreIsSoldSeparately='Y'><Extn ExtnPOSItemID='" + sEnterpriseItemID + "'/></Item>");
    }
    YFCDocument getItemListTemp = YFCDocument
        .getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_getItemListTemplate.xml"));

    YFCDocument getItemListOut =
        GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ITEM_LIST, getItemListInputDoc, getItemListTemp);
    LOGGER.endTimer("GCAddItemToCart.getPOSItemID()");
    LOGGER.verbose("GCAddItemToCart - getPOSItemID() : End");

    return getItemListOut;

  }


  public static YFCDocument getComponentPOSItemID(YFSEnvironment env, String sItemID) {

    LOGGER.beginTimer("GCAddItemToCart.addItemToCart()");
    LOGGER.verbose("GCAddItemToCart - addItemToCart() : Start");
    YFCDocument getItemListInputDoc = YFCDocument.getDocumentFor("<Item IgnoreIsSoldSeparately='Y' ItemID='" + sItemID + "'/>");

    YFCDocument getItemListTemp = YFCDocument
        .getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_getItemListTemplate.xml"));
    YFCDocument getItemListOut =
        GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ITEM_LIST, getItemListInputDoc, getItemListTemp);
    LOGGER.endTimer("GCAddItemToCart.getPOSItemID()");
    LOGGER.verbose("GCAddItemToCart - getPOSItemID() : End");

    return getItemListOut;

  }
  /**
   * This method will return the item details of the SET item.
   *
   * @param sPrimaryComponentItemID
   * @return getItemListOut
   */
  private YFCDocument getSETParentItemDetails(YFSEnvironment env, String sPrimaryComponentItemID, String sPrimaryComponentItemKey) {

    LOGGER.beginTimer("GCAddItemToCart.getSETParentItemDetails()");
    LOGGER.verbose("GCAddItemToCart - getSETParentItemDetails() : Start");

    YFCDocument getItemListInputDoc =
        YFCDocument
        .getDocumentFor(
            "<Item IgnoreIsSoldSeparately='Y' CallingOrganizationCode='GC' EntitlementDate='2500-12-12T10:25:33-07:00' GetAvailabilityFromCache='Y' SellerOrganizationCode='"
                + storeId + "'><Extn ExtnPrimaryComponentItemID='" + sPrimaryComponentItemID + "'/><Components><Component ComponentItemKey='" + sPrimaryComponentItemKey + "'/></Components></Item>");

    YFCDocument getItemListTemp = YFCDocument
        .getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_getItemListTemplate.xml"));
    YFCDocument getItemListOut =
        GCCommonUtil.invokeAPI(env, GCConstants.API_GET_COMPLETE_ITEM_LIST, getItemListInputDoc, getItemListTemp);
    LOGGER.endTimer("GCAddItemToCart.getSETParentItemDetails()");
    LOGGER.verbose("GCAddItemToCart - getSETParentItemDetails() : End");

    return getItemListOut;
  }

  public YFCDocument getCompleteOMSItemList(YFSEnvironment env, String sOMSItemID, boolean IsEnterpriseID) {

    LOGGER.beginTimer("GCAddItemToCart.getCompleteOMSItemList()");
    LOGGER.verbose("GCAddItemToCart - getCompleteOMSItemList() : Start");

    YFCDocument getItemListInputDoc = null;
    if (IsEnterpriseID) {
      getItemListInputDoc = YFCDocument.getDocumentFor(
          "<Item IgnoreIsSoldSeparately='Y' CallingOrganizationCode='GC' EntitlementDate='2500-12-12T10:25:33-07:00' GetAvailabilityFromCache='Y' SellerOrganizationCode='"
          + storeId + "'><Extn ExtnEnterpriseItemID='" + sOMSItemID + "'/></Item>");
    } else {
      getItemListInputDoc =
        YFCDocument.getDocumentFor(
              "<Item IgnoreIsSoldSeparately='Y' CallingOrganizationCode='GC' EntitlementDate='2500-12-12T10:25:33-07:00' GetAvailabilityFromCache='Y' SellerOrganizationCode='"
                  + storeId + "' ItemID='" + sOMSItemID + "'/>");
    }
    YFCDocument getItemListTemp = YFCDocument
        .getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_getItemListTemplate.xml"));
    YFCDocument getItemListOut =
        GCCommonUtil.invokeAPI(env, GCConstants.API_GET_COMPLETE_ITEM_LIST, getItemListInputDoc, getItemListTemp);
    LOGGER.endTimer("GCAddItemToCart.getSETParentItemDetails()");
    LOGGER.verbose("GCAddItemToCart - getSETParentItemDetails() : End");

    return getItemListOut;
  }

  /**
   *
   * This method will return the item details for a particular item.
   *
   * @param sPOSItemID
   * @return
   * @throws GCException
   */
  private YFCDocument getItemDetails(YFSEnvironment env, String sPOSItemID) {

    LOGGER.beginTimer("GCAddItemToCart.getSETParentItemID()");
    LOGGER.verbose("GCAddItemToCart - getSETParentItemID() : Start");

    YFCDocument getItemListInputDoc =
        YFCDocument.getDocumentFor(
            "<Item IgnoreIsSoldSeparately='Y' CallingOrganizationCode='GC' EntitlementDate='2500-12-12T10:25:33-07:00' GetAvailabilityFromCache='Y' SellerOrganizationCode='"
                + storeId
            + "'><Extn ExtnPOSItemID='" + sPOSItemID + "'/></Item>");
    YFCDocument getItemListTemp = YFCDocument
        .getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_getItemListTemplate.xml"));
    YFCDocument getItemListOut =
        GCCommonUtil.invokeAPI(env, GCConstants.API_GET_COMPLETE_ITEM_LIST, getItemListInputDoc, getItemListTemp);
    LOGGER.endTimer("GCAddItemToCart.getSETParentItemID()");
    LOGGER.verbose("GCAddItemToCart - getSETParentItemID() : End");

    return getItemListOut;
  }


  private static String formatPrice(String sPrice) {
    if (!YFCCommon.isVoid(sPrice)) {
      Double dPrice = Double.parseDouble(sPrice);
      sPrice = String.format("%.2f", dPrice);
    }
    return sPrice;

  }


  /**
   *
   * This method will return the item details for a particular item.
   *
   * @param sOrderHeaderKey
   * @return
   * @throws GCException
   */
  private YFCDocument getOrderList(YFSEnvironment env, String sOrderHeaderKey, String sStoreID) {

    LOGGER.beginTimer("GCAddItemToCart.getOrderList()");
    LOGGER.verbose("GCAddItemToCart - getOrderList() : Start");

    YFCDocument getOrderListInputDoc = YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "'/>");
    YFCDocument getOrderListTemp = YFCDocument.getDocumentFor(
        GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_getOrderList.xml"));

    YFCDocument getOrderListOut =
        GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST, getOrderListInputDoc, getOrderListTemp);


    // Sort OrderLines in ascending OrderLines
    List<YFCElement> orderLines = new ArrayList<YFCElement>();
    YFCNodeList<YFCElement> nlOrderLines = getOrderListOut.getElementsByTagName(GCConstants.ORDER_LINE);
    for (int j = 0; j < nlOrderLines.getLength(); j++) {
      YFCElement eleOrderLine = nlOrderLines.item(j);
      orderLines.add(eleOrderLine);
    }

    Collections.sort(orderLines, new orderLinesPrimeLineNo());
    YFCElement eleOrderLines = getOrderListOut.getElementsByTagName(GCConstants.ORDER_LINES).item(0);
    eleOrderLines.getParentElement().removeChild(eleOrderLines);

    YFCElement eleOrderLinesNew = getOrderListOut.createElement(GCConstants.ORDER_LINES);
    getOrderListOut.getDocumentElement().getChildElement(GCConstants.ORDER).appendChild(eleOrderLinesNew);
    eleOrderLinesNew.setAttribute("TotalNumberOfRecords", orderLines.size());
    for (int k = 0; k < orderLines.size(); k++) {
      YFCElement eleOrderLineSorted = orderLines.get(k);
      YFCElement sortedOrderLineToAdd = getOrderListOut.importNode(eleOrderLineSorted, true);
      eleOrderLinesNew.appendChild(sortedOrderLineToAdd);
    }


    // Map for combining all OrderLines containing same ItemID as one
    Map<String, String> uniqueItemMap = new HashMap<String, String>();
    // Map for getting info of SET parent so as to only add primary component
    Map<String, String> setComponentsMap = new HashMap<String, String>();
    // Map to to get total quantity of an item which can be present on diff OrderLines
    Map<String, Double> totalItemQuantityMap = new HashMap<String, Double>();
    // List containing ExtnPOSSKUNumber, ExtnSerialNo & ExtnPOSItemID of each item to restrict
    // duplicacy of uniqueMap
    List<String> itemExtnAttributeDetails = new ArrayList<String>();

    YFCNodeList<YFCElement> nlOrderLine = getOrderListOut.getElementsByTagName(GCConstants.ORDER_LINE);
    for (int i = 0; i < nlOrderLine.getLength(); i++) {
      YFCElement eleOrderLine = nlOrderLine.item(i);
      String sDeliveryType = eleOrderLine.getAttribute(GCConstants.DELIVERY_METHOD);
      String sKitCode = eleOrderLine.getAttribute(GCConstants.KIT_CODE);
      if (YFCCommon.equals(sDeliveryType, "CARRY")) {

        // conditions for getting total quantity of items
        YFCElement eleOrderLineExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
        String sExtnPOSSKUNumber = eleOrderLineExtn.getAttribute(GCConstants.EXTN_POS_SKU_NO);
        String sExtnActualSerialNumber = eleOrderLineExtn.getAttribute(GCConstants.EXTN_ACTUAL_SERIAL_NO);

        YFCElement eleItemDetails = eleOrderLine.getChildElement(GCConstants.ITEM_DETAILS);
        YFCElement eleItemDetailsExtn = eleItemDetails.getChildElement(GCConstants.EXTN);
        String sExtnPOSItemID = eleItemDetailsExtn.getAttribute(GCConstants.EXTN_POSITEM_ID);

        String sPrimeLineNo = eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
        String sOrderedQty = eleOrderLine.getAttribute(GCConstants.ORDERED_QTY);

        eleOrderLine.setAttribute("ExtnPOSItemID", sExtnPOSItemID);

        if (!YFCCommon.isVoid(sExtnPOSSKUNumber)) {
          if (totalItemQuantityMap.containsKey(sExtnPOSSKUNumber)) {
            Double intialQuantity = totalItemQuantityMap.get(sExtnPOSSKUNumber);
            Double changedQuantity = intialQuantity + Double.parseDouble(sOrderedQty);
            totalItemQuantityMap.put(sExtnPOSSKUNumber, changedQuantity);
          } else {
            totalItemQuantityMap.put(sExtnPOSSKUNumber, Double.parseDouble(sOrderedQty));
          }
        } else if (!YFCCommon.isVoid(sExtnActualSerialNumber)) {
          if (totalItemQuantityMap.containsKey(sExtnActualSerialNumber)) {
            Double intialQuantity = totalItemQuantityMap.get(sExtnPOSSKUNumber);
            Double changedQuantity = intialQuantity + Double.parseDouble(sOrderedQty);
            totalItemQuantityMap.put(sExtnActualSerialNumber, changedQuantity);
          } else {
            totalItemQuantityMap.put(sExtnActualSerialNumber, Double.parseDouble(sOrderedQty));
          }
        } else {
          if (totalItemQuantityMap.containsKey(sExtnPOSItemID)) {
            Double intialQuantity = totalItemQuantityMap.get(sExtnPOSItemID);
            Double changedQuantity = intialQuantity + Double.parseDouble(sOrderedQty);
            totalItemQuantityMap.put(sExtnPOSItemID, changedQuantity);
          } else {
            totalItemQuantityMap.put(sExtnPOSItemID, Double.parseDouble(sOrderedQty));
          }
        }
        if (!YFCCommon.equalsIgnoreCase(sKitCode, GCConstants.BUNDLE)) {
          YFCElement eleBundleParentLine = eleOrderLine.getElementsByTagName(GCConstants.BUNDLE_PARENT_LINE).item(0);
          if(YFCCommon.isVoid(eleBundleParentLine)){

            String sMapValue = sPrimeLineNo + "|" + sExtnPOSItemID;


            // conditions for getting item present over OrderLines once
            if (!YFCCommon.isVoid(sExtnPOSSKUNumber) && !uniqueItemMap.containsKey(sExtnPOSSKUNumber)
                && !itemExtnAttributeDetails.contains(sExtnPOSSKUNumber)) {
              uniqueItemMap.put(sExtnPOSSKUNumber, sMapValue);
            } else if (!YFCCommon.isVoid(sExtnActualSerialNumber) && !uniqueItemMap.containsKey(sExtnActualSerialNumber)
                && !itemExtnAttributeDetails.contains(sExtnActualSerialNumber)) {
              uniqueItemMap.put(sExtnActualSerialNumber, sMapValue);
            } else {

              if (!YFCCommon.isVoid(sExtnPOSItemID) && !uniqueItemMap.containsKey(sExtnPOSItemID)
                  && !itemExtnAttributeDetails.contains(sExtnPOSItemID)) {
                uniqueItemMap.put(sExtnPOSItemID, sMapValue);
              }
            }
          } else {
            YFCElement eleItem = eleOrderLine.getElementsByTagName(GCConstants.ITEM).item(0);
            String sComponentID = eleItem.getAttribute(GCConstants.ITEM_ID);

            String sParentOrderLineKey = eleBundleParentLine.getAttribute(GCConstants.ORDER_LINE_KEY);
            String sParentComponentID = setComponentsMap.get(sParentOrderLineKey);
            if (YFCCommon.equalsIgnoreCase(sComponentID, sParentComponentID)) {

              String sMapValue = sPrimeLineNo + "|" + sExtnPOSItemID;

              if (!YFCCommon.isVoid(sExtnPOSSKUNumber) && !uniqueItemMap.containsKey(sExtnPOSSKUNumber)
                  && !itemExtnAttributeDetails.contains(sExtnPOSSKUNumber)) {
                uniqueItemMap.put(sExtnPOSSKUNumber, sMapValue);
              } else if (!YFCCommon.isVoid(sExtnActualSerialNumber)
                  && !uniqueItemMap.containsKey(sExtnActualSerialNumber)
                  && !itemExtnAttributeDetails.contains(sExtnActualSerialNumber)) {
                uniqueItemMap.put(sExtnActualSerialNumber, sMapValue);
              } else {

                if (!YFCCommon.isVoid(sExtnPOSItemID) && !uniqueItemMap.containsKey(sExtnPOSItemID)
                    && !itemExtnAttributeDetails.contains(sExtnPOSItemID)) {
                  uniqueItemMap.put(sExtnPOSItemID, sMapValue);
                }
              }
            }
          }
        } else {
          String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
          YFCElement eleItemDetails1 = eleOrderLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
          YFCElement eleExtn = eleItemDetails1.getElementsByTagName(GCConstants.EXTN).item(0);
          String sPrimaryComponentItemID = eleExtn.getAttribute(GCConstants.EXTN_PRIMARY_COMPONENT_ID);
          setComponentsMap.put(sOrderLineKey, sPrimaryComponentItemID);
        }
        if (!YFCCommon.isVoid(sExtnPOSSKUNumber)) {
        itemExtnAttributeDetails.add(sExtnPOSSKUNumber);
        }
        if (!YFCCommon.isVoid(sExtnActualSerialNumber) && YFCCommon.isVoid(sExtnPOSSKUNumber)) {
        itemExtnAttributeDetails.add(sExtnActualSerialNumber);
        }
        if (!YFCCommon.isVoid(sExtnPOSItemID) && YFCCommon.isVoid(sExtnPOSSKUNumber)
            && YFCCommon.isVoid(sExtnActualSerialNumber)) {
        itemExtnAttributeDetails.add(sExtnPOSItemID);
      }
      }

    }

    YFCDocument cartInventoryValidationInDoc = YFCDocument.getDocumentFor("<ItemList/>");

    YFCElement eleRoot = cartInventoryValidationInDoc.getDocumentElement();
    for (Map.Entry<String, String> uniqueItemEntry : uniqueItemMap.entrySet()) {
      YFCElement eleItem = eleRoot.createChild(GCConstants.ITEM);
      eleItem.setAttribute(GCConstants.ITEM_ID, uniqueItemEntry.getKey());
      eleItem.setAttribute("LineNumber",
          uniqueItemEntry.getValue().substring(0, uniqueItemEntry.getValue().indexOf("|")));
      eleItem.setAttribute(GCConstants.EXTN_POSITEM_ID,
          uniqueItemEntry.getValue().substring(uniqueItemEntry.getValue().indexOf("|") + 1));
      String sKey = uniqueItemEntry.getKey();
      eleItem.setAttribute(GCConstants.ORDERED_QTY, totalItemQuantityMap.get(sKey));
      eleItem.setAttribute(GCConstants.STORE_ID, storeId);
    }


    YFCElement importOrderLineDetails =
        cartInventoryValidationInDoc.importNode(getOrderListOut.getDocumentElement(), true);
    cartInventoryValidationInDoc.getDocumentElement().appendChild(importOrderLineDetails);

    LOGGER.verbose("Doc returning is:-" + cartInventoryValidationInDoc.toString());
    LOGGER.endTimer("GCAddItemToCart.getOrderList()");
    LOGGER.verbose("GCAddItemToCart - getOrderList() : End");

    return cartInventoryValidationInDoc;
  }

  static class orderLinesPrimeLineNo implements Comparator<YFCElement>, Serializable {

    @Override
    public int compare(YFCElement elem1, YFCElement elem2) {
      Double value1 = Double.parseDouble(elem1.getAttribute("PrimeLineNo"));
      Double value2 = Double.parseDouble(elem2.getAttribute("PrimeLineNo"));
      int compare = value1.compareTo(value2);
      return compare;
    }

  }

  private YFCDocument processPOSInventoryValidateResponse(YFSEnvironment env, YFCDocument docPOSSKUValidationRespose,
      YFCDocument getOrderItemDetails, YFCDocument getOrderList, String sIsSPODisable) throws GCException {

    LOGGER.beginTimer("GCAddItemToCart.processPOSInventoryValidateResponse()");
    LOGGER.verbose("GCAddItemToCart - processPOSInventoryValidateResponse() : Start");

    Map<String, String> SKUNumberItemAvailaibility = new HashMap<String, String>();
    // Map<String, String> SerialNumberItemAvailaibility = new HashMap<String, String>();
    Map<String, String> POSItemAvailaibility = new HashMap<String, String>();
    // Map to store SKU number and Serial Number against a POSID
    Map<String, String> ValueAgainstPOSID = new HashMap<String, String>();
    // Map to store POSItemID against ErrorInfo from POS
    Map<String, String> errorInfoFromPOSResponse = new HashMap<String, String>();
    List<String> unavailableItems = new ArrayList<String>();
    boolean hasPOSInvShortageHappened = false;
    boolean hasNetworkAvaiInvShortageHappened = false;

    YFCNodeList nlResponse = docPOSSKUValidationRespose.getElementsByTagName("Response");
    for (int x = 0; x < nlResponse.getLength(); x++) {
      YFCElement eleResponse = (YFCElement) nlResponse.item(x);
      String sSuccess = eleResponse.getElementsByTagName(GCConstants.SUCCESS).item(0).getNodeValue();
      String sPOSItemID = eleResponse.getElementsByTagName("SKU").item(0).getNodeValue();
      if (YFCCommon.equalsIgnoreCase(sSuccess, "true")) {
      String sSKUNumberItemID = eleResponse.getElementsByTagName("SKU.Number").item(0).getNodeValue();
      String sSerialNumberItemID = eleResponse.getElementsByTagName("SerialNumber").item(0).getNodeValue();
      String sOrderedQty = eleResponse.getElementsByTagName("Quantity").item(0).getNodeValue();
        String sSKUandSerialNumber = sSKUNumberItemID + "|" + sSerialNumberItemID;
        ValueAgainstPOSID.put(sPOSItemID, sSKUandSerialNumber);
      if (!YFCCommon.isVoid(sSKUNumberItemID)) {
        SKUNumberItemAvailaibility.put(sSKUNumberItemID, sOrderedQty);
        } // else if (!YFCCommon.isVoid(sSerialNumberItemID)) {
        // SerialNumberItemAvailaibility.put(sSerialNumberItemID, sOrderedQty);
        // }
        else {
      POSItemAvailaibility.put(sPOSItemID, sOrderedQty);
    }
      } else {
        POSItemAvailaibility.put(sPOSItemID, "0");
        unavailableItems.add(sPOSItemID);
        String sErrorInfo = eleResponse.getElementsByTagName(GCConstants.ERROR_INFO).item(0).getNodeValue();
        errorInfoFromPOSResponse.put(sPOSItemID, sErrorInfo);
      }
    }
    LOGGER.verbose("Looping over OrderLines to determine if Inv shortage");

    // Call Network Availability Service for all Lines present
    YFCNodeList<YFCElement> nlOrderLines = getOrderList.getElementsByTagName(GCConstants.ORDER_LINE);

    YFCDocument networkAvailability = YFCDocument.getDocumentFor("<ItemList/>");
    YFCDocument findInventory = YFCDocument.getDocumentFor("<ItemList/>");

    // <!--------------------------------------------------------------------------------------------------------->
    YFCDocument availableInventory = YFCDocument.getDocumentFor("<Order/>");

    if (!YFCCommon.equalsIgnoreCase(sIsSPODisable, "Y")) {
    for (int c = 0; c < nlOrderLines.getLength(); c++) {
      YFCElement eleOrderLine = nlOrderLines.item(c);
      String sOrderedQty = eleOrderLine.getAttribute(GCConstants.ORDERED_QTY);
      String sPrimeLineNo = eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
      YFCElement eleItem = networkAvailability.createElement(GCConstants.ITEM);
      eleItem.setAttribute(GCConstants.ITEM_ID,
          eleOrderLine.getChildElement(GCConstants.ITEM).getAttribute(GCConstants.ITEM_ID));
      YFCElement elePrimaryInfo = eleOrderLine.getElementsByTagName(GCConstants.PRIMARY_INFORMATION).item(0);
      String sDefaultProductClass = elePrimaryInfo.getAttribute(GCXmlLiterals.DEFAULT_PRODUCT_CLASS);
      String sKitCode = elePrimaryInfo.getAttribute(GCConstants.KIT_CODE);
      YFCElement eleComputedPrice = eleOrderLine.getElementsByTagName(GCConstants.COMPUTED_PRICE).item(0);
      YFCElement eleItemDetails = eleOrderLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
      YFCElement eleExtn = eleItemDetails.getChildElement(GCConstants.EXTN);
      String sExtnPOSItemID = "";
      if (!YFCCommon.isVoid(eleExtn)) {
        String sExtnIsClearanceItem = eleExtn.getAttribute("ExtnIsClearanceItem");
        sExtnPOSItemID = eleExtn.getAttribute(GCConstants.EXTN_POSITEM_ID);
        if (YFCCommon.equalsIgnoreCase(sExtnIsClearanceItem, GCConstants.YES)) {
          eleItem.setAttribute("ExtnIsStoreClearance", GCConstants.YES);
        }
      }
      String sUnitPrice = eleComputedPrice.getAttribute(GCConstants.UNIT_PRICE);
      eleItem.setAttribute("UnitOfMeasure", GCConstants.EACH);
      eleItem.setAttribute("ProductClass", sDefaultProductClass);
      eleItem.setAttribute("UnitPrice", sUnitPrice);
      eleItem.setAttribute("StoreID", storeId);
      eleItem.setAttribute(GCConstants.KIT_CODE, sKitCode);
      eleItem.setAttribute(GCConstants.LINE_ID, sPrimeLineNo);
      eleItem.setAttribute("ExtnPOSItemID", sExtnPOSItemID);
      eleItem.setAttribute(GCConstants.ORDERED_QTY, sOrderedQty);

      networkAvailability.getDocumentElement().appendChild(eleItem);
    }

    LOGGER.verbose("Network availability inDoc is:-" + GCXMLUtil.getXMLString(networkAvailability.getDocument()));
    Document networkAvailabilityPresent =
        GCCommonUtil.invokeService(env, "GCGetNetwokAvailabilityForOrderService", networkAvailability.getDocument());
      availableInventory = YFCDocument.getDocumentFor(networkAvailabilityPresent);

    // Clone Doc to separate <ItemList> & <Promise> Documents
    Document cloneDoc = (Document) networkAvailabilityPresent.cloneNode(true);
    Element elePromise = (Element) cloneDoc.getElementsByTagName(GCConstants.PROMISE).item(0);
    Element elePromiseChild = (Element) elePromise.getElementsByTagName(GCConstants.PROMISE).item(0);
    Document clonePromiseDoc = GCXMLUtil.getDocumentFromElement(elePromiseChild);
      findInventory = YFCDocument.getDocumentFor(clonePromiseDoc);

    availableInventory.getDocumentElement()
    .removeChild(availableInventory.getDocumentElement().getElementsByTagName(GCConstants.PROMISE).item(0));

    LOGGER.verbose("Network availability response is:-" + GCXMLUtil.getXMLString(availableInventory.getDocument()));
    }
    // <!---------------------------------------------------------------------------------------------------------------------->

    boolean isDistributionApplicable = false;
    String sLineIdFromNA = "";
    Map<String, Integer> presentSETItem = new HashMap<String, Integer>();
    Map<String, String> naForSET = new HashMap<String, String>();
    List<String> primaryComponents = new ArrayList<String>();

    int orderLineToSetIn;
    String counter = "";
    String sRemainingOtherVariation = "";

    nlOrderLines = getOrderList.getElementsByTagName(GCConstants.ORDER_LINE);
    List<Integer> processedLineNo = new ArrayList<Integer>();

    // First loop to make sure in 1st go only SKU No. considered, in 2nd only SKU
    for (int itemIdentifier = 1; itemIdentifier <= 2; itemIdentifier++) {
      // Second loop to actually distribute inventory
    for (int z = 0; z < nlOrderLines.getLength(); z++) {

        // Below condition to not process same lines again
        if (!processedLineNo.contains(z)) {

      YFCElement eleOrderLine = nlOrderLines.item(z);
      String sPrimeLineNo = eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
      String sItemID = eleOrderLine.getChildElement(GCConstants.ITEM).getAttribute(GCConstants.ITEM_ID);
      String sProductClass = eleOrderLine.getChildElement(GCConstants.ITEM).getAttribute(GCConstants.PRODUCT_CLASS);
      String sOrderLineKitCode = eleOrderLine.getAttribute(GCConstants.KIT_CODE);

      // <!----------------------------------------------------------------------------------------------------------------->
      Element eleItem = null;
      if (!YFCCommon.equalsIgnoreCase(sIsSPODisable, "Y")) {
        eleItem = GCXMLUtil.getElementByXPath(availableInventory.getDocument(),
          "ItemList/Item[@LineId='" + sPrimeLineNo + "']");
      }

      String sDeliveryMethod = eleOrderLine.getAttribute(GCConstants.DELIVERY_METHOD);
      if (YFCCommon.equalsIgnoreCase(sDeliveryMethod, "CARRY")) {

        // Below 2 attributes to track REGULAR and Primary attributes
        String sPrimaryComp = "";
        YFCElement eleItemDetails = eleOrderLine.getChildElement(GCConstants.ITEM_DETAILS);
        if (!YFCCommon.isVoid(eleItemDetails)) {
          sPrimaryComp =
              eleItemDetails.getChildElement(GCConstants.EXTN).getAttribute(GCConstants.EXTN_PRIMARY_COMPONENT_ID);
        }
        if (!YFCCommon.isVoid(sPrimaryComp)) {
          primaryComponents.add(sPrimaryComp);
        }

        //MPOS-1590 fix: KitCode check applied to consider secondary component inventory as well in case of SET.
        if (YFCCommon.equalsIgnoreCase(sProductClass, "REGULAR") || primaryComponents.contains(sItemID) || !YFCCommon.equalsIgnoreCase(sOrderLineKitCode, GCConstants.BUNDLE)) 
        {
          isDistributionApplicable = true;
        } else {
          isDistributionApplicable = false;
        }

        String sSKU_Number = eleOrderLine.getChildElement(GCConstants.EXTN).getAttribute("ExtnPOSSKUNumber");
            String sSerial_Number =
                eleOrderLine.getChildElement(GCConstants.EXTN).getAttribute("ExtnActualSerialNumber");
      String sPOSItemID = eleOrderLine.getAttribute("ExtnPOSItemID");
      String sOrderedQty = eleOrderLine.getAttribute(GCConstants.ORDERED_QTY);
            String sAvailableQuantity = "";
        String sValueAgainstPOSID = ValueAgainstPOSID.get(sPOSItemID);
        
            /*
             * if(YFCCommon.isVoid(sSKU_Number) && !YFCCommon.isVoid(sValueAgainstPOSID)) { String
             * sSKUValueToSet = sValueAgainstPOSID.substring(0, sValueAgainstPOSID.indexOf("|")); if
             * (!YFCCommon.isVoid(sSKUValueToSet)) {
             * eleOrderLine.getChildElement(GCConstants.EXTN).setAttribute("ExtnPOSSKUNumber",
             * sSKUValueToSet); } }
             *
             * if(YFCCommon.isVoid(sSerial_Number) && !YFCCommon.isVoid(sValueAgainstPOSID)) {
             * String sSerialValueToSet =
             * sValueAgainstPOSID.substring(sValueAgainstPOSID.indexOf("|") + 1); if
             * (!YFCCommon.isVoid(sSerialValueToSet)) {
             * eleOrderLine.getChildElement(GCConstants.EXTN).setAttribute("ExtnActualSerialNumber",
             * sSerialValueToSet); } }
             */
        String sNetworkOnHandAvailability = "";
        String sNetworkFutureAvailability = "";
        String sFirstAvailableDate = "";


          if (unavailableItems.contains(sPOSItemID)) {
            eleOrderLine.setAttribute(GCConstants.ERROR_INFO, errorInfoFromPOSResponse.get(sPOSItemID));
          }


            if (itemIdentifier == 1) {
              sAvailableQuantity = SKUNumberItemAvailaibility.get(sSKU_Number);
            } else {
              sAvailableQuantity = POSItemAvailaibility.get(sPOSItemID);
        }
      String sOrderLineKey = "";
      String sStoreAvailability = "";

      if (YFCCommon.equalsIgnoreCase(sOrderLineKitCode, GCConstants.BUNDLE)) {
        LOGGER.verbose("SET Item present");

        sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
        presentSETItem.put(sOrderLineKey, z);

          // <!--------------------------------------------------------------------------------------------------------->
          if (!YFCCommon.equalsIgnoreCase(sIsSPODisable, "Y")) {
          if (!YFCCommon.isVoid(eleItem)) {
            sNetworkOnHandAvailability = eleItem.getAttribute("NetworkOnHandAvailability");
            sNetworkFutureAvailability = eleItem.getAttribute("NetworkFutureAvailability");
            sFirstAvailableDate = eleItem.getAttribute("FirstAvailableDate");
          }
          String sCombination =
              sNetworkOnHandAvailability + "_" + sNetworkFutureAvailability + "/" + sFirstAvailableDate;
          naForSET.put(sOrderLineKey, sCombination);
          }
          // <!--------------------------------------------------------------------------------------------------------------->
      } else {
              if (!YFCCommon.isVoid(sAvailableQuantity) && !YFCCommon.isVoid(sOrderedQty)) {
                YFCElement eleBundleParentLine =
                    eleOrderLine.getElementsByTagName(GCConstants.BUNDLE_PARENT_LINE).item(0);
          if (!YFCCommon.isVoid(eleBundleParentLine)) {
            String sBundleParentOLKey = eleBundleParentLine.getAttribute(GCConstants.ORDER_LINE_KEY);
            orderLineToSetIn = presentSETItem.get(sBundleParentOLKey);
              String sGetCombination = naForSET.get(sBundleParentOLKey);
              // <!----------------------------------------------------------------------------------------------------------->
              if (!YFCCommon.equalsIgnoreCase(sIsSPODisable, "Y")) {
              sNetworkOnHandAvailability = sGetCombination.substring(0, sGetCombination.indexOf("_"));
              sNetworkFutureAvailability =
                  sGetCombination.substring(sGetCombination.indexOf("_") + 1, sGetCombination.indexOf("/"));
              sFirstAvailableDate = sGetCombination.substring(sGetCombination.indexOf("/") + 1);
              }
              // <!----------------------------------------------------------------------------------------------------------------->
          } else {
            orderLineToSetIn = z;
              // <!----------------------------------------------------------------------------------------------------------->
              if (!YFCCommon.equalsIgnoreCase(sIsSPODisable, "Y")) {
              if (!YFCCommon.isVoid(eleItem)) {
                sNetworkOnHandAvailability = eleItem.getAttribute("NetworkOnHandAvailability");
                sNetworkFutureAvailability = eleItem.getAttribute("NetworkFutureAvailability");
                sFirstAvailableDate = eleItem.getAttribute("FirstAvailableDate");
              }
      }
              // <!---------------------------------------------------------------------------------------------------------->
            }

            if (isDistributionApplicable) {
                  processedLineNo.add(z);

                  if (Double.parseDouble(sAvailableQuantity) >= Double.parseDouble(sOrderedQty)) {

                	  //MPOS-1590 fix: Start.
                	  sStoreAvailability = nlOrderLines.item(orderLineToSetIn).getAttribute("StoreAvailability");
                	  if(YFCCommon.isVoid(sStoreAvailability)){
                		  nlOrderLines.item(orderLineToSetIn).setAttribute("StoreAvailability", sOrderedQty);
                	  }
                	  
                	  else{
                		  sStoreAvailability = String.valueOf(Math.min(Double.parseDouble(sStoreAvailability), Double.parseDouble(sOrderedQty)));
                		  nlOrderLines.item(orderLineToSetIn).setAttribute("StoreAvailability", sStoreAvailability);
                	  }
                	  //MPOS-1590 fix: End.
                // <!---------------------------------------------------------------------------------------------------------->
                if (!YFCCommon.equalsIgnoreCase(sIsSPODisable, "Y")) {
                  nlOrderLines.item(orderLineToSetIn).setAttribute("NetworkOnHandAvailability",
                      sNetworkOnHandAvailability);
                  nlOrderLines.item(orderLineToSetIn).setAttribute("NetworkFutureAvailability",
                      sNetworkFutureAvailability);
                nlOrderLines.item(orderLineToSetIn).setAttribute("FirstAvailableDate", sFirstAvailableDate);
                }
                // <!---------------------------------------------------------------------------------------------------------->

            String sRemainingPOSQuantity =
                        Double.toString((Double.parseDouble(sAvailableQuantity) - Double.parseDouble(sOrderedQty)));

                    if (itemIdentifier == 1) {
                SKUNumberItemAvailaibility.put(sSKU_Number, sRemainingPOSQuantity);
                      if (POSItemAvailaibility.containsKey(sPOSItemID)) {
                        sRemainingOtherVariation = Double.toString(
                            Double.parseDouble(POSItemAvailaibility.get(sPOSItemID)) - Double.parseDouble(sOrderedQty));
                        POSItemAvailaibility.put(sPOSItemID, sRemainingOtherVariation);
                      }
              } else {
            POSItemAvailaibility.put(sPOSItemID, sRemainingPOSQuantity);
              }


                  } else if (Double.parseDouble(sAvailableQuantity) < Double.parseDouble(sOrderedQty)) {
                	  //MPOS-1590 fix: Start.
                	  sStoreAvailability = nlOrderLines.item(orderLineToSetIn).getAttribute("StoreAvailability");
                	  if(YFCCommon.isVoid(sStoreAvailability)){

                    nlOrderLines.item(orderLineToSetIn).setAttribute("StoreAvailability", sAvailableQuantity);
                	  }
                	  else{
                		  sStoreAvailability = String.valueOf(Math.min((Double.parseDouble(sStoreAvailability)),Double.parseDouble(sAvailableQuantity)));
                		  nlOrderLines.item(orderLineToSetIn).setAttribute("StoreAvailability", sStoreAvailability);
                	  }
                	  //MPOS-1590 fix: End.
                // <!---------------------------------------------------------------------------------------------------------->
                if (!YFCCommon.equalsIgnoreCase(sIsSPODisable, "Y")) {
                  nlOrderLines.item(orderLineToSetIn).setAttribute("NetworkOnHandAvailability",
                      sNetworkOnHandAvailability);
                  nlOrderLines.item(orderLineToSetIn).setAttribute("NetworkFutureAvailability",
                      sNetworkFutureAvailability);
                nlOrderLines.item(orderLineToSetIn).setAttribute("FirstAvailableDate", sFirstAvailableDate);
                }
                // <!---------------------------------------------------------------------------------------------------------->

                    if (itemIdentifier == 1) {
                SKUNumberItemAvailaibility.put(sSKU_Number, "0");
                      if (POSItemAvailaibility.containsKey(sPOSItemID)) {
                        sRemainingOtherVariation = Double.toString(
                            Double.parseDouble(POSItemAvailaibility.get(sPOSItemID)) - Double.parseDouble(sOrderedQty));
                        POSItemAvailaibility.put(sPOSItemID, sRemainingOtherVariation);
                      }
              } else {
                POSItemAvailaibility.put(sPOSItemID, "0");
              }

                hasPOSInvShortageHappened = true;


              }
            }
          }

              }
          } else {
        if (YFCCommon.equalsIgnoreCase(sProductClass, GCConstants.REGULAR)
            || (YFCCommon.equalsIgnoreCase(sProductClass, GCConstants.SET)
                && YFCCommon.equalsIgnoreCase(sOrderLineKitCode, GCConstants.BUNDLE))) {
          String sOrderedQty = eleOrderLine.getAttribute(GCConstants.ORDERED_QTY);

          // <!-------------------------------------------------------------------------------------------------->
          String sNetworkOnHandAvailability = "";
          String sNetworkFutureAvailability = "";
          String sFirstAvailableDate = "";
          if (!YFCCommon.isVoid(eleItem)) {
            sNetworkOnHandAvailability = eleItem.getAttribute("NetworkOnHandAvailability");
            sNetworkFutureAvailability = eleItem.getAttribute("NetworkFutureAvailability");
            sFirstAvailableDate = eleItem.getAttribute("FirstAvailableDate");
          }
          if (Double.parseDouble(sNetworkOnHandAvailability) < Double.parseDouble(sOrderedQty)) {
            hasNetworkAvaiInvShortageHappened = true;
            eleOrderLine.setAttribute("StoreAvailability", "0");
            eleOrderLine.setAttribute("NetworkOnHandAvailability", sNetworkOnHandAvailability);
            eleOrderLine.setAttribute("NetworkFutureAvailability", sNetworkFutureAvailability);
            eleOrderLine.setAttribute("FirstAvailableDate", sFirstAvailableDate);
              } else {
            eleOrderLine.setAttribute("StoreAvailability", "0");
            eleOrderLine.setAttribute("NetworkOnHandAvailability", sNetworkOnHandAvailability);
            eleOrderLine.setAttribute("NetworkFutureAvailability", sNetworkFutureAvailability);
            eleOrderLine.setAttribute("FirstAvailableDate", sFirstAvailableDate);
        }

      }
    }
    }
      }
    }

    if (hasPOSInvShortageHappened || hasNetworkAvaiInvShortageHappened) {
      getOrderList.getDocumentElement().setAttribute("Success", "false");
    } else {
      getOrderList.getDocumentElement().setAttribute("Success", "true");

      // <!--------------------------------------------------------------------------------------------------------------------------->

      if (!YFCCommon.equalsIgnoreCase(sIsSPODisable, "Y")) {
      YFCElement eleImportPromiseDoc = getOrderList.importNode(findInventory.getDocumentElement(), true);
      getOrderList.getDocumentElement().appendChild(eleImportPromiseDoc);
    }
    }



    LOGGER.verbose("Doc sending to Front End " + getOrderList.toString());
    return getOrderList;


  }

  /**
   *
   * This method checks for possible Item Restrictions
   *
   * @param sPOSItemID
   * @return
   * @throws GCException
   */
  public static YFCDocument checkIfRestrictedItems(YFSEnvironment env, String sItemID, String sItemType, String sBrandId,
      String StoreId, String sIsProCoverageItem, String sPOSStatusCode) {

    LOGGER.verbose("GCAddItemToCart - checkIfRestrictedItems() : Start");
    YFCDocument toReturn = YFCDocument.getDocumentFor("<Response Success='true'/>");

    if (YFCCommon.equalsIgnoreCase(sPOSStatusCode, "F") || YFCCommon.equalsIgnoreCase(sPOSStatusCode, "I")
        || YFCCommon.equalsIgnoreCase(sPOSStatusCode, "S") || YFCCommon.equalsIgnoreCase(sPOSStatusCode, "Z")) {
      toReturn = YFCDocument.getDocumentFor(
          "<Response Success='false'><Errorinfo Error='Please process this item in POS.'/></Response>");
      return toReturn;
    }

    if (YFCCommon.equalsIgnoreCase(sItemType, "03") && !YFCCommon.equalsIgnoreCase(sIsProCoverageItem, "Y")) {
      LOGGER.verbose("Warranty cannot be added separately. Send Error to mPOS");
      toReturn = YFCDocument.getDocumentFor(
          "<Response Success='false'><Errorinfo Error='This item may not be independently added'/></Response>");
      return toReturn;
    }

    YFCDocument getCommonCodeInput =
        YFCDocument.getDocumentFor("<CommonCode CodeType='mPOSRestrictedItems'  CodeValue= '" + sItemType + "'/>");

    YFCDocument templateDoc =
        YFCDocument.getDocumentFor("<CommonCodeList><CommonCode CodeLongDescription=''/></CommonCodeList> ");

    YFCDocument getCommonCodeOutput =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_COMMON_CODE_LIST, getCommonCodeInput, templateDoc);

    if (getCommonCodeOutput.getDocumentElement().hasChildNodes() && !YFCCommon.isVoid(sItemType)) {

      toReturn = YFCDocument.getDocumentFor(
          "<Response Success='false'><Errorinfo Error='Please process this item in POS.'/></Response>");
      return toReturn;
    }

    try {
      if (!YFCCommon.isVoid(sBrandId)) {
        GCGetItemAvailability.isItemSellableFromLoggedInStore(env, StoreId, sBrandId, sItemID);
      }
    } catch (Exception YFSException) {
      toReturn = YFCDocument.getDocumentFor("<Response Success='false'><Errorinfo Error=''/></Response>");
      toReturn.getDocumentElement().getElementsByTagName("Errorinfo").item(0).setAttribute("Error", sItemID
          + " belongs to brand '" + sBrandId + "' which is not allowed to be sold from Store '" + StoreId + "'");
    }

    LOGGER.verbose("GCAddItemToCart - checkIfRestrictedItems() : Returning");
    return toReturn;

  }

  public Document networkAvailability(YFSEnvironment env, YFCDocument inDoc) {
    LOGGER.verbose("GCAddItemToCart - networkAvailability() : Start");

    YFCNodeList<YFCElement> nlOrderLines = inDoc.getElementsByTagName(GCConstants.ORDER_LINE);

    YFCDocument networkAvailability = YFCDocument.getDocumentFor("<ItemList/>");

    for (int c = 0; c < nlOrderLines.getLength(); c++) {
      YFCElement eleOrderLine = nlOrderLines.item(c);
      String sOrderedQty = eleOrderLine.getAttribute(GCConstants.ORDERED_QTY);
      String sPrimeLineNo = eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
      YFCElement eleItem = networkAvailability.createElement(GCConstants.ITEM);
      eleItem.setAttribute(GCConstants.ITEM_ID,
          eleOrderLine.getChildElement(GCConstants.ITEM).getAttribute(GCConstants.ITEM_ID));
      YFCElement elePrimaryInfo = eleOrderLine.getElementsByTagName(GCConstants.PRIMARY_INFORMATION).item(0);
      String sDefaultProductClass = elePrimaryInfo.getAttribute(GCXmlLiterals.DEFAULT_PRODUCT_CLASS);
      String sKitCode = elePrimaryInfo.getAttribute(GCConstants.KIT_CODE);
      YFCElement eleComputedPrice = eleOrderLine.getElementsByTagName(GCConstants.COMPUTED_PRICE).item(0);
      YFCElement eleItemDetails = eleOrderLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
      YFCElement eleExtn = eleItemDetails.getChildElement(GCConstants.EXTN);
      String sExtnPOSItemID = eleExtn.getAttribute(GCConstants.EXTN_POSITEM_ID);
      if (!YFCCommon.isVoid(eleExtn)) {
        String sExtnIsClearanceItem = eleExtn.getAttribute("ExtnIsClearanceItem");
        if (YFCCommon.equalsIgnoreCase(sExtnIsClearanceItem, GCConstants.YES)) {
          eleItem.setAttribute("ExtnIsStoreClearance", GCConstants.YES);
        }
      }
      String sUnitPrice = eleComputedPrice.getAttribute(GCConstants.UNIT_PRICE);
      eleItem.setAttribute("UnitOfMeasure", GCConstants.EACH);
      eleItem.setAttribute("ProductClass", sDefaultProductClass);
      eleItem.setAttribute("UnitPrice", sUnitPrice);
      eleItem.setAttribute("StoreID", storeId);
      eleItem.setAttribute(GCConstants.KIT_CODE, sKitCode);
      eleItem.setAttribute(GCConstants.LINE_ID, sPrimeLineNo);
      eleItem.setAttribute("ExtnPOSItemID", sExtnPOSItemID);
      eleItem.setAttribute(GCConstants.ORDERED_QTY, sOrderedQty);

      networkAvailability.getDocumentElement().appendChild(eleItem);
    }

    LOGGER.verbose("Network availability inDoc is:-" + GCXMLUtil.getXMLString(networkAvailability.getDocument()));
    Document networkAvailabilityPresent =
        GCCommonUtil.invokeService(env, "GCGetNetwokAvailabilityForOrderService", networkAvailability.getDocument());

    return networkAvailabilityPresent;
  }

}


