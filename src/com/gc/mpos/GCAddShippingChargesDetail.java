/**
 * File Name : GCAddShippingChargesDetail.java
 *
 * Description : This class has logic for Adding Shipping Charges
 *
 * Modification Log :
 * -------------------------------------------------------------------------------------------------------------------------------
 * Version #                    Date                    Author                              Modification
 * 0.1                      25/05/2016              Singh, Amit Mohan               Initial Draft Adding Shipping Charges
 * -------------------------------------------------------------------------------------------------------------------------------
 */
package com.gc.mpos;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCAddShippingChargesDetail {
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCAddShippingChargesDetail.class.getName());

  public Document addShippingCharges(YFSEnvironment env, Document inDoc){

    LOGGER.verbose("GCAddShippingChargesDetail - addShippingCharges() : Start");
    LOGGER.verbose("GCAddShippingChargesDetail - addShippingCharges() inDoc" + GCXMLUtil.getXMLString(inDoc));

    YFCDocument inDocToAdd = YFCDocument.getDocumentFor(inDoc);
    YFCDocument changeOrderTemplate =
        YFCDocument.getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_getOrderDetails.xml"));

    YFCDocument docWithAdditionalUIInfo =
        GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, inDocToAdd, changeOrderTemplate);
    docWithAdditionalUIInfo.getDocumentElement().setAttribute("PromotionOrPLCCCall", "Y");

    LOGGER.verbose("Doc sending to GCGetATGPromoService" + docWithAdditionalUIInfo.toString());

    YFCDocument responseAfterAddingShippingCharges =
        GCCommonUtil.invokeService(env, "GCGetATGPromoService", docWithAdditionalUIInfo);


    LOGGER.verbose("Doc returning after adding Shipping Charges" + responseAfterAddingShippingCharges.toString());
    return responseAfterAddingShippingCharges.getDocument();
  }


}
