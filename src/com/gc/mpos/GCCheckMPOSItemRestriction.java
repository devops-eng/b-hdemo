/**
 * File Name : GCCheckMPOSItemRestriction.java
 *
 * Description : This class has logic for Adding Shipping Charges
 *
 * Modification Log :
 * -------------------------------------------------------------------------------------------------------------------------------
 * Version #                    Date                    Author                              Modification
 * 0.1                      26/08/2016              Singh, Amit Mohan               Initial Draft - checks whether restricted Item
 * -------------------------------------------------------------------------------------------------------------------------------
 */
package com.gc.mpos;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCCheckMPOSItemRestriction {
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCCheckMPOSItemRestriction.class.getName());

  public Document checkItemRestrictions(YFSEnvironment env, Document inDoc) {
    LOGGER.verbose("GCCheckMPOSItemRestriction - checkItemRestrictions() : Start");
    LOGGER.verbose("GCCheckMPOSItemRestriction - checkItemRestrictions() inDoc is" + GCXMLUtil.getXMLString(inDoc));

    YFCDocument restrictCheck = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleRoot = restrictCheck.getDocumentElement();
    String sItemID = eleRoot.getAttribute(GCConstants.ITEM_ID);
    String sStoreID = eleRoot.getAttribute(GCConstants.STORE_ID);
    String sIsProCoverageItem = eleRoot.getAttribute("IsProCoverageItem");

    YFCDocument getItemList = GCAddItemToCart.getComponentPOSItemID(env, sItemID);

    YFCElement elePrimaryInfo = getItemList.getElementsByTagName(GCConstants.PRIMARY_INFORMATION).item(0);
    YFCElement eleExtn = getItemList.getDocumentElement().getElementsByTagName(GCConstants.EXTN).item(0);

    YFCDocument toReturn = YFCDocument.getDocumentFor("<Response Success='true'/>");

    if (!YFCCommon.isVoid(elePrimaryInfo)) {
      String itemType = elePrimaryInfo.getAttribute(GCConstants.ITEM_TYPE);
      String sBrandId = eleExtn.getAttribute("ExtnBrandId");
      String sPOSStatusCode = eleExtn.getAttribute("ExtnPOSStatusCode");

      toReturn = GCAddItemToCart.checkIfRestrictedItems(env, sItemID, itemType, sBrandId, sStoreID,
          sIsProCoverageItem, sPOSStatusCode);

      if (YFCCommon.equalsIgnoreCase(toReturn.getDocumentElement().getAttribute("Success"), "false")) {
        LOGGER.verbose("############# RESTRICTED ITEM ##############");
        return toReturn.getDocument();
      }
    }

    return toReturn.getDocument();
  }
}
