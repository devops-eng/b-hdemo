package com.gc.mpos;

import java.sql.SQLException;

/**
 * File Name : GCUpdateMPOSOrder.java
 *
 *OBJECTIVE: This class is called when GCCreateMPOSOrderInvoice is invoked from on order confirmation from mPOS.
 *This class will create invoice for the mPOS Orders and process the payment for the order.
 *
 *Modification Log :
 * -------------------------------------------------------------------------------------------------------------------------------
 * Version #                    Date                    Author                              Modification
 * 0.1                      05/18/2016              Singh, Amit Mohan               Initial Draft UpdateMPOSOrder MPOS - 428
 * 0.2                      06/01/2016              Singh, Amit Mohan               Added logic for Order Sourcing Controls
 * -------------------------------------------------------------------------------------------------------------------------------
 */






import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.shared.ycp.YFSContext;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class GCCreateMPOSOrderInvoice {
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCCreateMPOSOrderInvoice.class.getName());

  public void createMPOSOrderInvoice(YFSEnvironment env, Document inDoc) {

    LOGGER.verbose("GCCreateMPOSOrderInvoice - createMPOSOrderInvoice() : Start");
    LOGGER.verbose("GCCreateMPOSOrderInvoice inDoc is :-" + GCXMLUtil.getXMLString(inDoc));

    env.setTxnObject("EntryType", "mPOS");

    //MPOS-2416: Start.
    String sExtnPOSCustomerID = "";
    if(!YFCCommon.isVoid(env.getTxnObject("ExtnPOSCustomerIDFromConfirmOrder")))
    {
    	sExtnPOSCustomerID = env.getTxnObject("ExtnPOSCustomerIDFromConfirmOrder").toString();
    }
	//MPOS-2416: End.
    
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    String sOrderHeaderKey = inputDoc.getDocumentElement().getAttribute(GCConstants.ORDER_HEADER_KEY);

    LOGGER.verbose("Calling changeOrderStatus : Start");
    YFCDocument changeOrderStatus = YFCDocument.getDocumentFor("<OrderStatusChange/>");
    changeOrderStatus.getDocumentElement().setAttribute(GCConstants.BASE_DROP_STATUS, "1100.30.10");
    changeOrderStatus.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    changeOrderStatus.getDocumentElement().setAttribute(GCConstants.TRANSACTION_ID, "UpdateSupply.0001.ex");
    GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER_STATUS, changeOrderStatus.getDocument());
    LOGGER.verbose("Calling changeOrderStatus : End");

    LOGGER.verbose("Calling createOrderInvoice : Start");
    YFCDocument createOrderInvoice = YFCDocument.getDocumentFor("<Order/>");
    createOrderInvoice.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    createOrderInvoice.getDocumentElement().setAttribute(GCConstants.TRANSACTION_ID, "TenderOrder.0001.ex");
    YFCDocument orderInvoice = GCCommonUtil.invokeAPI(env, "createOrderInvoice", createOrderInvoice, YFCDocument.getDocumentFor(""));
    LOGGER.verbose("Calling createOrderInvoice : End");

    YFCDocument getCommonCodeListInput = YFCDocument.getDocumentFor("<CommonCode/>");
    getCommonCodeListInput.getDocumentElement().setAttribute(GCConstants.CODE_TYPE, "GCUseSendInvoice");
    YFCDocument getCommonCodeListTemplate = YFCDocument.getDocumentFor("<CommonCodeList> <CommonCode/></CommonCodeList>");
    YFCDocument getCommonCodeListOutput = GCCommonUtil.invokeAPI(env, GCConstants.API_GET_COMMON_CODE_LIST, getCommonCodeListInput, getCommonCodeListTemplate);

    String sCodeValue = getCommonCodeListOutput.getElementsByTagName(GCConstants.COMMON_CODE).item(0).getAttribute(GCConstants.CODE_VALUE);

    if (YFCCommon.equalsIgnoreCase(sCodeValue, "N")) {

      YFCElement eleRoot = inputDoc.getDocumentElement();
      String sOrderNo = eleRoot.getAttribute(GCConstants.ORDER_NO);
      YFCDocument inDocFinalizeCoupons = YFCDocument.getDocumentFor("<Order EntryType='mPOS' FinalizeCouponsCall='Y' OrderNo='" + sOrderNo + "'><Extn><GCOrderCouponList/></Extn></Order>");
      YFCElement eleFinalizeDocRoot = inDocFinalizeCoupons.getDocumentElement();
      YFCElement eleGCCouponList = eleFinalizeDocRoot.getElementsByTagName(GCConstants.GC_ORDER_COUPON_LIST).item(0);
      LOGGER.verbose("Looping over GCOrderCoupon to prepare inDoc for GCFinalizeCoupons-->");
      YFCNodeList<YFCElement> nlGCOrderCoupon = eleRoot.getElementsByTagName(GCConstants.GC_ORDER_COUPON);
      for(int j = 0; j < nlGCOrderCoupon.getLength(); j++)
      {

        YFCElement eleGCCoupon = nlGCOrderCoupon.item(j);
        eleGCCouponList.importNode(eleGCCoupon);
      }

      if(nlGCOrderCoupon.getLength() > 0)
      {
        GCCommonUtil.invokeService(env, GCConstants.GC_PROCESS_POS_RESPONSE_SERVICE,
            inDocFinalizeCoupons.getDocument());

      }

      LOGGER.verbose("Calling getOrderList API to get coupons applied on the order : End");

      LOGGER.verbose("Calling request/execute/request API to process order payments : Start");
      YFCDocument processOrderPayments = YFCDocument.getDocumentFor("<Order/>");
      processOrderPayments.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
      GCCommonUtil.invokeAPI(env, GCConstants.REQUEST_COLLECTION, processOrderPayments.getDocument());
      LOGGER.verbose("Request Collection API : End");


      LOGGER.verbose("Committing transaction : Start");
      if (env instanceof YFSContext) {
        try {
          ((YFSContext) env).commit();
          ((YFSContext) env).close();
        } catch (SQLException e) {
          LOGGER.error("Exception while publishing zero availability for node having sourcing disabled", e);
          YFSException yfsEx = new YFSException();
          yfsEx.setErrorCode("EXTN_GCE00100");
          throw yfsEx;

        }
      }
      LOGGER.verbose("Committing transaction : End");
      
      //MPOS-2416: Start.
      LOGGER.verbose("Class GCCreateMPOSOrderInvoice : Transaction object set as ExtnPOSCustomerID and the value is--> " + sExtnPOSCustomerID);
      env.setTxnObject("ExtnPOSCustomerIDFromCreateInvoice", sExtnPOSCustomerID);
      //MPOS-2416: End.

      GCCommonUtil.invokeAPI(env, GCConstants.EXECUTE_COLLECTION, processOrderPayments.getDocument());
      GCCommonUtil.invokeAPI(env, GCConstants.REQUEST_COLLECTION, processOrderPayments.getDocument());
      LOGGER.verbose("Calling request/execute/request API to process order payments : End");

      LOGGER.verbose("Calling getOrderInvoiceDetails : Start");
      String sOrderInvoiceKey = orderInvoice.getElementsByTagName(GCConstants.ORDER_INVOICE).item(0)
          .getAttribute(GCConstants.ORDER_INVOICE_KEY);
      YFCDocument getOrderInvoiceDetailsInput =
          YFCDocument.getDocumentFor("<GetOrderInvoiceDetails InvoiceKey='" + sOrderInvoiceKey + "'/>");
      YFCDocument getOrderInvoiceDetailsTemp = YFCDocument.getDocumentFor(
          GCCommonUtil.getDocumentFromPath("/global/template/event/SEND_INVOICE.PUBLISH_INVOICE_DETAIL.xml"));

      YFCDocument getOrderInvoiceDetails = GCCommonUtil.invokeAPI(env, "getOrderInvoiceDetails",
          getOrderInvoiceDetailsInput, getOrderInvoiceDetailsTemp);
      LOGGER.verbose("Calling getOrderInvoiceDetails : End");

      LOGGER.verbose("Calling publish invoice to POS to retrieve POS Sales ticket : Start");
      GCCommonUtil.invokeService(env, "GCPublishMPOSInvoiceToPOS", getOrderInvoiceDetails);
      LOGGER.verbose("Calling publish invoice to POS to retrieve POS Sales ticket : End");

      LOGGER.verbose("GCCreateMPOSOrderInvoice - createMPOSOrderInvoice() : End");

    }
  }
}
