package com.gc.mpos;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCDeleteMPOSOrder {
	
	private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCDeleteMPOSOrder.class.getName());
	
	public void deleteOrder(YFSEnvironment env, Document inDoc) throws GCException {
		
		 LOGGER.beginTimer("GCDeleteMPOSOrder.deleteOrder()");
		 LOGGER.verbose("GCDeleteMPOSOrder - deleteOrder() : Start");
		 LOGGER.verbose("Input Doc to method deleteOrder:-->" + GCXMLUtil.getXMLString(inDoc));
	     
		 YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
		 YFCElement eleRoot = inputDoc.getDocumentElement();
		 String sOrderHeaderKey = "";
		
		 YFCNodeList<YFCElement> nlOrder = eleRoot.getElementsByTagName(GCConstants.ORDER);
		 int nLength = nlOrder.getLength();
		 for(int i = 0; i < nLength; i++)
		 {
			 YFCElement eleOrder = nlOrder.item(i);
			 sOrderHeaderKey = eleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
			 
			 YFCDocument docToService = YFCDocument.createDocument(GCConstants.ORDER);
			 docToService.getDocumentElement().setAttribute(GCConstants.ACTION, "DELETE");
			 docToService.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
			 
			 GCCommonUtil.invokeAPI(env, GCConstants.DELETE_ORDER, docToService.getDocument());
		 }		 
		
	}

}
