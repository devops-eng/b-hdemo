package com.gc.mpos;


import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCGetItemDetailAtMPOS {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCGetItemDetailAtMPOS.class.getName());

  public Document getItemDetail(YFSEnvironment env, Document inDoc) throws GCException {

    LOGGER.beginTimer("GCGetItemDetailAtMPOS.getItemDetail()");
    LOGGER.verbose("GCGetItemDetailAtMPOS - getItemDetail() : Start");

    if (LOGGER.isDebugEnabled())
    {
      LOGGER.verbose("Input Doc to method getItemDetail:-->" + GCXMLUtil.getXMLString(inDoc));
    }


    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleRoot = inputDoc.getDocumentElement();

    String sItemID = eleRoot.getAttribute(GCConstants.ITEM_ID);
    String sStoreID = eleRoot.getAttribute(GCConstants.STORE_ID);
    String sExtnIsStoreClearance = eleRoot.getAttribute("ExtnIsStoreClearance");

    YFCDocument getItemListOutDoc = itemDetails(env, sItemID, sStoreID);


    String sUnitPrice="";
    String sProductClass = "";
    String sKitCode = "";
    String sExtnIsClearanceItem = "";

    YFCElement itemElement = getItemListOutDoc.getElementsByTagName(GCConstants.ITEM).item(0);

    if(YFCCommon.isVoid(itemElement))
    {
      return getItemListOutDoc.getDocument();
    }


    YFCElement eleComputedPrice = getItemListOutDoc.getElementsByTagName(GCConstants.COMPUTED_PRICE).item(0);
    if(!YFCCommon.isVoid(eleComputedPrice))
    {
      sUnitPrice = eleComputedPrice.getAttribute(GCConstants.UNIT_PRICE);
    
    
      if(YFCCommon.isVoid(sUnitPrice))
      {
	        YFCDocument priceNotSet =
	            YFCDocument.getDocumentFor("<Response Success='false'><Errorinfo Error=''/></Response>");
	        priceNotSet.getDocumentElement().getElementsByTagName("Errorinfo").item(0).setAttribute("Error",
	            "Please process this item in POS.");
	        return priceNotSet.getDocument();
      }
    }


    YFCElement elePrimaryInformation = getItemListOutDoc.getElementsByTagName(GCConstants.PRIMARY_INFORMATION).item(0);
    if(!YFCCommon.isVoid(elePrimaryInformation))
    {
      sProductClass = elePrimaryInformation.getAttribute("DefaultProductClass");
      sKitCode = elePrimaryInformation.getAttribute(GCConstants.KIT_CODE);
    }


    YFCElement eleExtn = getItemListOutDoc.getElementsByTagName(GCConstants.EXTN).item(0);
    String sExtnPrimaryComponentItemID = eleExtn.getAttribute(GCConstants.EXTN_PRIMARY_COMPONENT_ID);
    String sExtnPOSItemID = eleExtn.getAttribute(GCConstants.EXTN_POSITEM_ID);

    if(!YFCCommon.isVoid(eleExtn))
    {
      if(YFCCommon.isVoid(sExtnPrimaryComponentItemID))
      {
        sExtnPrimaryComponentItemID = "";
      }
      
      if(YFCCommon.isVoid(sExtnPOSItemID))
      {
    	  sExtnPOSItemID = "";
      }
      sExtnIsClearanceItem = eleExtn.getAttribute("ExtnIsClearanceItem");
    }


    YFCElement componentElement = getItemListOutDoc.getElementsByTagName(GCConstants.COMPONENT).item(0);

    if(!YFCCommon.isVoid(componentElement))
    {
      YFCNodeList<YFCElement> nlComponent = getItemListOutDoc.getElementsByTagName(GCConstants.COMPONENT);
      for (int i = 0; i < nlComponent.getLength(); i++)
      {
        YFCElement eleComponent = nlComponent.item(i);

        String sComponentItemID = eleComponent.getAttribute(GCConstants.COMPONENT_ITEM_ID);
        YFCDocument componentDetails = itemDetails(env, sComponentItemID, sStoreID);
        YFCElement eleComponentItem = componentDetails.getElementsByTagName("Item").item(0);

        YFCElement eleComponentItemDetails = getItemListOutDoc.importNode(eleComponentItem, true);
        eleComponent.appendChild(eleComponentItemDetails);
      }
    }


    YFCDocument networkAvailabilityInputDoc = YFCDocument.getDocumentFor("<Item ItemID='" + sItemID + "' StoreID='" +  sStoreID + "' ProductClass='" + sProductClass +"' TotalAvailability='Y' UnitOfMeasure='EACH' ExtnIsStoreClearance='" + sExtnIsStoreClearance
        + "' UnitPrice='" + sUnitPrice + "' ExtnPOSItemID='" + sExtnPOSItemID + "' KitCode='" + sKitCode +"' ExtnPrimaryComponentItemID='" + sExtnPrimaryComponentItemID + "' ExtnIsClearanceItem='" + sExtnIsClearanceItem +"'/>");

    YFCDocument networkAvailaibility = GCCommonUtil.invokeService(env, "GCGetNetworkAvailabilityService", networkAvailabilityInputDoc);


    YFCElement eleItem = networkAvailaibility.getElementsByTagName(GCConstants.ITEM).item(0);
    String sNetworkOnHandAvailability = "";
    String sStoreAvailability = "";
    String sSKUNumber = "";
    String sSerialNumber = "";
    if(!YFCCommon.isVoid(eleItem))
    {
      sNetworkOnHandAvailability = eleItem.getAttribute("NetworkOnHandAvailability");
      sStoreAvailability = eleItem.getAttribute("StoreAvailability");
      sSKUNumber = eleItem.getAttribute("SKUNumber");
      sSerialNumber = eleItem.getAttribute("SerialNumber");
      String sPrice  = eleItem.getAttribute(GCConstants.UNIT_PRICE);
      if(!YFCCommon.isVoid(sPrice))
      {
        itemElement.setAttribute(GCConstants.UNIT_PRICE, sPrice);
      }
    }

    itemElement.setAttribute("NetworkOnHandAvailability", sNetworkOnHandAvailability);
    itemElement.setAttribute("StoreAvailability", sStoreAvailability);
    itemElement.setAttribute("SKUNumber", sSKUNumber);
    itemElement.setAttribute("SerialNumber", sSerialNumber);
		 
		 return getItemListOutDoc.getDocument();
		
	}
	

	public YFCDocument itemDetails(YFSEnvironment env, String sItemID, String sStoreID)
	{
		YFCDocument getItemListInputDoc = YFCDocument.getDocumentFor("<Item IgnoreIsSoldSeparately='Y' CallingOrganizationCode='GC' EntitlementDate='2500-12-12T10:25:33-07:00' GetAvailabilityFromCache='Y' SellerOrganizationCode='"
				 + sStoreID + "' ItemID='" + sItemID + "'/>");
		
		 YFCDocument getItemListTemp = YFCDocument.getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_getItemListTemplate.xml"));
		 
		 if (LOGGER.isDebugEnabled()) 
		 {
		      LOGGER.verbose("Input Doc to getCompleteItemList:-->" + getItemListInputDoc.toString());
	     }
		 YFCDocument outDoc = GCCommonUtil.invokeAPI(env, GCConstants.API_GET_COMPLETE_ITEM_LIST, getItemListInputDoc, getItemListTemp);
		 
		 if (LOGGER.isDebugEnabled()) 
		 {
		      LOGGER.verbose("Output Doc of getCompleteItemList:-->" + outDoc.toString());
	     }
		 
		 return outDoc;
	}

}
