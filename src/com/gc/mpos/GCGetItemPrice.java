package com.gc.mpos;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCLogUtils;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:bhawana.jethuri@expicient.com">Bhawana Jethuri</a>
 */

public class GCGetItemPrice {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCGetItemPrice.class.getName());


  /**
   * This method will be called from GCAddItemToCart to fetch list price of line items added to cart
   * in mPOS.
   * 
   * @param env
   * @param inDoc
   * @return sListPrice
   * 
   */
  public Document getItemPrice(YFSEnvironment env, Document inDoc) throws GCException {
    LOGGER.beginTimer("GCGetItemPrice.getItemPrice()");
    LOGGER.verbose("GCGetItemPrice - getItemPrice() : BEGIN");

    if (LOGGER.isDebugEnabled()) {
      LOGGER.verbose("Input Doc to method getItemPrice:-->" + GCXMLUtil.getXMLString(inDoc));

    }
    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleRoot = yfcInDoc.getDocumentElement();
    String itemID = eleRoot.getAttribute(GCConstants.ITEM_ID);
    String storeId = eleRoot.getAttribute(GCConstants.STORE_ID);
    YFCDocument getItemPriceInputDoc =
        YFCDocument.getDocumentFor("<ItemPrice Currency='USD' OrganizationCode='GC' SellerOrganizationCode='" + storeId
            + "'><LineItems><LineItem ItemID='" + itemID + "' Quantity='1' UnitOfMeasure='" + GCConstants.EACH
            + "'/></LineItems></ItemPrice>");
    YFCDocument getItemPriceTemplateDoc =
        YFCDocument.getDocumentFor(GCCommonUtil
            .getDocumentFromPath("/global/template/api/mPOS_Get_Item_Price_Template.xml"));
    GCLogUtils.verboseLog(LOGGER, "getItemPrice Input document-->" + getItemPriceInputDoc.toString());
    YFCDocument getItemPriceOutDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ITEM_PRICE, getItemPriceInputDoc, getItemPriceTemplateDoc);
    GCLogUtils.verboseLog(LOGGER, "getItemPrice Output document-->" + getItemPriceOutDoc.toString());
    GCLogUtils.verboseLog(LOGGER, "GCGetItemPrice.getItemPrice--End");
    LOGGER.endTimer("GCGetItemPrice.getItemPrice()");
    return getItemPriceOutDoc.getDocument();
  }
}
