/**
 * File Name : GCGetMPOSPaginationCustomerList.java
 *
 * Description : This class has logic for returning list of Customers
 *
 * Modification Log :
 * -------------------------------------------------------------------------------------------------------------------------------
 * Version #                    Date                    Author                              Modification
 * 0.1                      25/05/2016              Singh, Amit Mohan               Initial Draft for getCustomerList
 * 0.2                      31/05/2016              Singh, Amit Mohan               Changed logic to return single customer
 * -------------------------------------------------------------------------------------------------------------------------------
 */
package com.gc.mpos;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCGetMPOSPaginationCustomerList {
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCGetMPOSPaginationCustomerList.class.getName());

  public Document getCustomerList(YFSEnvironment env, Document inDoc) {
    LOGGER.verbose("GCGetMPOSPaginationCustomerList - getCustomerList() : Start");
    LOGGER.verbose("GCGetMPOSPaginationCustomerList - getCustomerList() inDoc is" + GCXMLUtil.getXMLString(inDoc));

    YFCDocument pageInput = YFCDocument.getDocumentFor(inDoc);
    YFCDocument pageOutput = YFCDocument.getDocumentFor("<CustomerList/>");

    YFCElement eleInput = pageInput.getElementsByTagName(GCConstants.INPUT).item(0);
    YFCElement eleCustContact = eleInput.getElementsByTagName(GCConstants.CUSTOMER_CONTACT).item(0);

    YFCDocument getCustListTemplate =
        YFCDocument.getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_getCustomerList.xml"));

    YFCElement eleAPI = pageInput.getElementsByTagName(GCConstants.API).item(0);
    YFCElement eleTemplate = pageInput.createElement("Template");
    eleAPI.appendChild(eleTemplate);

    YFCElement eleToImportToPageInput = pageInput.importNode(getCustListTemplate.getDocumentElement(), true);
    eleTemplate.appendChild(eleToImportToPageInput);

    LOGGER.verbose("Input XML for getPage API is" + pageInput.toString());

    pageOutput = GCCommonUtil.invokeAPI(env, "getPage", pageInput, null);

    LOGGER.verbose("Output XML for getPage API is" + pageOutput.toString());
    YFCElement eleCustomerList = pageOutput.getElementsByTagName(GCConstants.CUSTOMER_LIST).item(0);

    if (eleCustomerList.hasChildNodes()) {
      YFCNodeList<YFCElement> nlCustomer = pageOutput.getElementsByTagName(GCConstants.CUSTOMER);

      for (int x = 0; x < nlCustomer.getLength(); x++) {
    	  
    	  String sModifyTs = "";
        YFCElement eleCustomer = nlCustomer.item(x);
        YFCElement eleCustomerContact = eleCustomer.getElementsByTagName(GCConstants.CUSTOMER_CONTACT).item(0);
      YFCElement eleCustomerAdditionalAddressList =
            eleCustomer.getElementsByTagName(GCConstants.CUSTOMER_ADDITIONAL_ADDRESS_LIST).item(0);
        YFCElement eleCustomerAdditionalAddressListNew =
            pageOutput.createElement(GCConstants.CUSTOMER_ADDITIONAL_ADDRESS_LIST);

        YFCNodeList<YFCElement> nlCustomerAdditionalAddress = eleCustomer.getElementsByTagName(GCConstants.CUSTOMER_ADDITIONAL_ADDRESS);

        // Dummy entry in Map --> this will be compared to modifyts to get latest entry
      Map<String, YFCElement> modifyTs = new HashMap<String, YFCElement>();
      modifyTs.put("1950-05-27T17:44:26-05:00", eleTemplate);

      boolean IsDefaultBillToPresent = false;


        for (int y = 0; y < nlCustomerAdditionalAddress.getLength(); y++) {
          YFCElement eleCustomerAdditionalAddress = nlCustomerAdditionalAddress.item(y);

          if (YFCCommon.equalsIgnoreCase(eleCustomerAdditionalAddress.getAttribute(GCConstants.IS_DEFAULT_BILLTO),
              "Y")) {
        IsDefaultBillToPresent = true;
            eleCustomerContact.removeChild(eleCustomerAdditionalAddressList);
        eleCustomerContact.appendChild(eleCustomerAdditionalAddressListNew);
            eleCustomerAdditionalAddressListNew.appendChild(eleCustomerAdditionalAddress);
            break;
      }

          else{
           sModifyTs =
              eleCustomerAdditionalAddress.getChildElement(GCConstants.PERSON_INFO).getAttribute(GCConstants.MODIFY_TS);
          SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
          try {
	        	  if(!YFCCommon.isVoid(sModifyTs))
	        	  {
		            Date date1 = sdf.parse(sModifyTs);
		            Map.Entry<String, YFCElement> entry = modifyTs.entrySet().iterator().next();
		            String key = entry.getKey();
		            Date date2 = sdf.parse(key);
		
		            if (date1.after(date2)) {
		              modifyTs.remove(key);
		              modifyTs.put(sModifyTs, eleCustomerAdditionalAddress);
		            }
	        	  }

          } catch (ParseException e) {
            LOGGER.verbose("Invalid time format");
            // TODO Auto-generated catch block
            e.printStackTrace();
          }
          }

      }

        if(eleCustomerAdditionalAddressList.hasChildNodes())
        {
        	if (!IsDefaultBillToPresent && !YFCCommon.isVoid(sModifyTs)) //empty CustomerAdditionalAddress fix.
          	{
      	        eleCustomerContact.removeChild(eleCustomerAdditionalAddressList);
      	        eleCustomerContact.appendChild(eleCustomerAdditionalAddressListNew);
      	        Map.Entry<String, YFCElement> entry = modifyTs.entrySet().iterator().next();
      	        YFCElement eleMapValue = entry.getValue();
      	        YFCElement eleAddressToReturn = pageOutput.importNode(eleMapValue, true);
      	        eleCustomerAdditionalAddressListNew.appendChild(eleAddressToReturn);
          	}
        }
        
        // Removing as part of MPOS-1881 fix.
        //        else
        //        {
        //          YFCNodeList<YFCElement> nlTempCustomer =
        //              eleCustomerList.getElementsByTagName(GCConstants.CUSTOMER);
        //          eleCustomerList.removeChild(eleCustomer);
        //
        //        }
        //
        //      }
        LOGGER.verbose("Doc returning to UI is" + pageOutput.toString());
      }
    }
        	
    removeEmptyCustomerAdditionalAddress(pageOutput); // Fix for MPOS-1881
    return pageOutput.getDocument();
        }
      
  private void removeEmptyCustomerAdditionalAddress(YFCDocument pageOutput) {

    YFCElement eleCustomerList = pageOutput.getElementsByTagName(GCConstants.CUSTOMER_LIST).item(0);
    YFCNodeList<YFCElement> nlCustomer = eleCustomerList.getElementsByTagName(GCConstants.CUSTOMER);
    for (YFCElement eleCustomer : nlCustomer) {
      YFCNodeList<YFCElement> nlCustomerAdditionalList =
          eleCustomer.getElementsByTagName(GCConstants.CUSTOMER_ADDITIONAL_ADDRESS_LIST);
      for (YFCElement eleCustomerAdditionaAddressList : nlCustomerAdditionalList) {
        if (!eleCustomerAdditionaAddressList.hasChildNodes()) {
          eleCustomerList.removeChild(eleCustomer);
    }
    }
  }

  }
}
