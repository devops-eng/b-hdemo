package com.gc.mpos;

import java.util.Date;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCGetNetwokAvailabilityForAllOrderLines {
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCGetNetwokAvailabilityForAllOrderLines.class.getName());

  public Document getNetworkAvailability(YFSEnvironment env, Document inDoc) throws GCException {

    LOGGER.beginTimer("GCGetNetwokAvailabilityForAllOrderLines.getNetworkAvailability()");
    LOGGER.verbose("GCGetNetwokAvailabilityForAllOrderLines - getNetworkAvailability() : Start");

    if (LOGGER.isDebugEnabled())
    {
      LOGGER.verbose("Input Doc to method getNetworkAvailability:-->" + inDoc.toString());
    }

    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleInput = inputDoc.getDocumentElement();

    YFCDocument outputDoc = YFCDocument.createDocument(GCConstants.ITEM_LIST);
    YFCElement outDocRoot = outputDoc.getDocumentElement();

    getNetworkAvailability(env, eleInput, outDocRoot);

    if (LOGGER.isDebugEnabled())
    {
      LOGGER.verbose("final output:-->" + outputDoc.toString());
    }

    return outputDoc.getDocument();
  }



  private void getNetworkAvailability(YFSEnvironment env, YFCElement eleInput, YFCElement eleOutput)
  {
    String sItemID = "";
    String sUnitPrice="";
    String sUnitOfMeasure = "";
    String sProductClass = "";
    String skitCode = "";
    String sExtnIsStoreClearance = "";
    String sLineId = "";
    String storeId = eleInput.getAttribute(GCConstants.STORE_ID);
    String sRequiredQty = "";
    if(!YFCCommon.isVoid(eleInput))
    {
      YFCNodeList<YFCElement> nlItem = eleInput.getElementsByTagName(GCConstants.ITEM);

      YFCDocument docPromise = YFCDocument.createDocument(GCConstants.PROMISE);
      YFCElement elePromise = docPromise.getDocumentElement();
      elePromise.setAttribute("AggregateSupplyOfNonRequestedTag", GCConstants.N);
      elePromise.setAttribute(GCConstants.FULFILLMENT_TYPE,"PICKUP_IN_STORE");
     
      elePromise.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GC);

      YFCElement eleExtn = elePromise.createChild(GCConstants.EXTN);
      eleExtn.setAttribute(GCConstants.EXTN_PICK_UP_STORE_ID, storeId);
      elePromise.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GC);

      YFCElement elePromiseLines = elePromise.createChild(GCConstants.PROMISE_LINES);

      for(int i = 0; i< nlItem.getLength(); i++)
      {
        YFCElement eleItem = nlItem.item(i);
        sItemID = eleItem.getAttribute(GCConstants.ITEM_ID);
        storeId = eleItem.getAttribute(GCConstants.STORE_ID);
        sProductClass = eleItem.getAttribute(GCConstants.PRODUCT_CLASS);
        sUnitOfMeasure = eleItem.getAttribute(GCConstants.UNIT_OF_MEASURE);
        sExtnIsStoreClearance = eleItem.getAttribute(GCXmlLiterals.EXTN_IS_STORE_CLEARANCE);
        sUnitPrice = eleItem.getAttribute(GCConstants.UNIT_PRICE);
        skitCode = eleItem.getAttribute(GCConstants.KIT_CODE);
        sLineId = eleItem.getAttribute(GCConstants.LINE_ID);
        sRequiredQty = eleItem.getAttribute(GCConstants.ORDERED_QTY);
        YFCElement eleTag = eleItem.getElementsByTagName(GCConstants.TAG).item(0);

        YFCElement elePromiseLine = elePromiseLines.createChild(GCConstants.PROMISE_LINE);
        elePromiseLine.setAttribute(GCConstants.ITEM_ID, sItemID);
        elePromiseLine.setAttribute(GCConstants.PRODUCT_CLASS, sProductClass);
        elePromiseLine.setAttribute(GCConstants.UNIT_OF_MEASURE, sUnitOfMeasure);
        elePromiseLine.setAttribute(GCConstants.REQUIRED_QTY, sRequiredQty);
        elePromiseLine.setAttribute(GCConstants.LINE_ID, sLineId);

        YFCElement eleExcludedNodes = elePromiseLine.createChild("ExcludedShipNodes");
        YFCElement eleExcludedNode = eleExcludedNodes.createChild("ExcludedShipNode");
        String sNode = eleItem.getAttribute(GCConstants.STORE_ID);
        eleExcludedNode.setAttribute("Node",sNode);
        eleExcludedNode.setAttribute("SuppressNodeCapacity", "N");
        eleExcludedNode.setAttribute("SupressProcurement", "Y");
        eleExcludedNode.setAttribute("SupressSourcing", "Y");

        YFCElement eleExtnin = elePromiseLine.createChild(GCConstants.EXTN);
        eleExtnin.setAttribute(GCXmlLiterals.EXTN_IS_STORE_CLEARANCE, sExtnIsStoreClearance);
        eleExtnin.setAttribute(GCXmlLiterals.EXTN_UNIT_PRICE, sUnitPrice);

        if (!YFCCommon.isVoid(eleTag))
        {
          String sBatchNo = eleTag.getAttribute(GCXmlLiterals.BATCH_NO);
          YFCElement eleTagin = docPromise.createElement(GCXmlLiterals.TAG);
          eleTagin.setAttribute(GCXmlLiterals.BATCH_NO, sBatchNo);
        }

        elePromiseLines.appendChild(elePromiseLine);


      }

      if (LOGGER.isVerboseEnabled())
      {

        LOGGER.verbose("Input to Inventory " + (docPromise.toString()));

      }

      YFCDocument promiseOutDoc = GCCommonUtil.invokeService(env, GCConstants.GC_FIND_INVENTORY_WRAPPER_SERVICE, docPromise);

      if (LOGGER.isDebugEnabled())
      {
        LOGGER.verbose("GCFindInventoryWrapperService output:-->" + promiseOutDoc.toString());
      }


      String futureInventory = "0";
      String sProdAvailDate = "";

      YFCElement elePromiseOutput = promiseOutDoc.getDocumentElement();

      YFCElement eleSuggestedOption = elePromiseOutput.getElementsByTagName("SuggestedOption").item(0);
      YFCNodeList<YFCElement> nlPromiseLine = eleSuggestedOption.getElementsByTagName(GCConstants.PROMISE_LINE);
      YFCNodeList<YFCElement> nlAssignment = null;

      for (YFCElement elePromiseLineOp : nlPromiseLine)
      {
        sItemID = elePromiseLineOp.getAttribute(GCConstants.ITEM_ID);
        skitCode = elePromiseLineOp.getAttribute(GCConstants.KIT_CODE);
        sProductClass = elePromiseLineOp.getAttribute(GCConstants.PRODUCT_CLASS);
        sLineId = elePromiseLineOp.getAttribute(GCConstants.LINE_ID);
        YFCElement eleAssignments  = elePromiseLineOp.getElementsByTagName(GCXmlLiterals.ASSIGNMENTS).item(0);

        if(YFCCommon.equalsIgnoreCase(sProductClass, "REGULAR")){
          nlAssignment = eleAssignments.getElementsByTagName(GCXmlLiterals.ASSIGNMENT);

        }
        else if(YFCCommon.equalsIgnoreCase(sProductClass, "SET"))
        {
          if(YFCCommon.equalsIgnoreCase(skitCode, "BUNDLE"))
          {

            String kitCode = elePromiseLineOp.getAttribute(GCConstants.KIT_CODE);
            if (YFCUtils.equals(kitCode, GCConstants.BUNDLE))
            {
              nlAssignment = eleAssignments.getElementsByTagName(GCXmlLiterals.ASSIGNMENT);

            }
          }

          else{
            nlAssignment = null;
          }
        }



        double dOnhandQty = 0.0;
        double dAvailableQuantity = 0.0;
        String sEmptyAssignmentReason = "";
        String sInvQTy = "";

        if (!YFCCommon.isVoid(nlAssignment))
        {
          for (YFCElement eleAssignment : nlAssignment)
          {
            Date sysDate = new Date();

            sInvQTy = eleAssignment.getAttribute(GCXmlLiterals.INV_QTY);
            sEmptyAssignmentReason = eleAssignment.getAttribute("EmptyAssignmentReason");
            if(!YFCCommon.isVoid(sEmptyAssignmentReason))
            {

              sInvQTy = "0.0";

            }


            if(!YFCCommon.isVoid(sInvQTy))
            {
              dAvailableQuantity = Double.parseDouble(sInvQTy);
            }

            String sShipNode = eleAssignment.getAttribute(GCConstants.SHIP_NODE);
            sProdAvailDate = eleAssignment.getAttribute(GCConstants.PRODUCT_AVAIL_DATE);
            Date dProdAvailDate = eleAssignment.getDateAttribute(GCConstants.PRODUCT_AVAIL_DATE);

            if (!YFCCommon.isVoid(dProdAvailDate) && !YFCCommon.isVoid(sInvQTy) )
            {
              if (sysDate.compareTo(dProdAvailDate) < 0 && YFCUtils.equals(sShipNode, GCConstants.MFI))
              {
                futureInventory = sInvQTy;
              }
              else
              {
                dOnhandQty = dOnhandQty + dAvailableQuantity;
              }

            }

            if(YFCCommon.isVoid(dProdAvailDate))
            {
              dOnhandQty = dOnhandQty + dAvailableQuantity;
              futureInventory = sInvQTy;
            }
          }


          YFCElement eItem = eleOutput.createChild(GCConstants.ITEM);
          eItem.setAttribute(GCConstants.ITEM_ID, sItemID);
          eItem.setAttribute("NetworkOnHandAvailability", YFCUtils.stringfromDouble(dOnhandQty));
          eItem.setAttribute("NetworkFutureAvailability", futureInventory);
          eItem.setAttribute("FirstAvailableDate", sProdAvailDate);
          eItem.setAttribute(GCConstants.LINE_ID, sLineId );

          eleOutput.appendChild(eItem);
        }
      }

      YFCElement promiseElement = eleOutput.createChild(GCConstants.PROMISE);
      YFCElement importEle = (YFCElement) promiseElement.importNode(elePromiseOutput);
      promiseElement.appendChild(importEle);
    }

  }

}
