package com.gc.mpos;

import java.util.Date;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCGetNetworkAvailability {
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCGetNetworkAvailability.class.getName());

  /**
   * getNetworkAvailability method invokes GCfindInventoryWrapperService and returns store and
   * network availability to mPOS
   *
   * @param env
   * @param inDoc
   * @return
   * @throws GCException
   */
  public Document getNetworkAvailability(YFSEnvironment env, Document inDoc) throws GCException {

    Element eleInput = inDoc.getDocumentElement();
    String totalAvailability = eleInput.getAttribute("TotalAvailability");
    Document outputDoc = GCXMLUtil.createDocument(GCConstants.ITEM);
    Element eleOutput = outputDoc.getDocumentElement();
    getNetworkAvailability(env, eleInput, eleOutput);
    String primaryComponentId = eleInput.getAttribute(GCConstants.EXTN_PRIMARY_COMPONENT_ID);
    if (YFCUtils.equals(totalAvailability, GCConstants.YES)) {
      String storeClearance = eleInput.getAttribute(GCXmlLiterals.EXTN_IS_STORE_CLEARANCE);
      String itemId = eleInput.getAttribute(GCConstants.ITEM_ID);
      String storeId = eleInput.getAttribute(GCConstants.STORE_ID);

      Document inDocCompleteItemList = GCXMLUtil.getDocument(
          "<Item IgnoreIsSoldSeparately='Y' CallingOrganizationCode='GC' EntitlementDate='2500-12-12T10:25:33-07:00' GetAvailabilityFromCache='Y' SellerOrganizationCode='"
              + storeId + "' ItemID='" + itemId + "'/>");

      Document tempCompleteitemList = GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_completeItemListTemplate.xml");

      Document outCompleteItemList =
          GCCommonUtil.invokeAPI(env, GCConstants.API_GET_COMPLETE_ITEM_LIST, inDocCompleteItemList, tempCompleteitemList);

      Element eleRoot = outCompleteItemList.getDocumentElement();
      Element eleExtn = (Element) eleRoot.getElementsByTagName(GCConstants.EXTN).item(0);
      String sExtnIsClearanceItem = eleExtn.getAttribute("ExtnIsClearanceItem");

      String unitPrice = "";
      Element eleComputedPrice = (Element) eleRoot.getElementsByTagName("ComputedPrice").item(0);
      if (!YFCCommon.isVoid(eleComputedPrice)) {
        unitPrice = eleComputedPrice.getAttribute(GCConstants.UNIT_PRICE);
      }
      Element eleAvailability = (Element) eleRoot.getElementsByTagName(GCConstants.AVAILABILITY).item(0);
      Element eleAvailabilityOp = outputDoc.createElement(GCConstants.AVAILABILITY);

      if(!YFCCommon.isVoid(eleAvailability))
      {
        GCXMLUtil.copyElement(outputDoc, eleAvailability, eleAvailabilityOp);
      }
      eleOutput.appendChild(eleAvailabilityOp);
      if (YFCUtils.equals(storeClearance, GCConstants.N) && YFCCommon.equalsIgnoreCase(sExtnIsClearanceItem, GCConstants.YES))
      {

        YFCDocument getItemPriceInput = YFCDocument.getDocumentFor("<ItemPrice/>");
        getItemPriceInput.getDocumentElement().setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GC);
        getItemPriceInput.getDocumentElement().setAttribute(GCConstants.CURRENCY, GCConstants.USD);
        YFCElement eleLineItems = getItemPriceInput.createElement(GCConstants.LINE_ITEMS);
        getItemPriceInput.getDocumentElement().appendChild(eleLineItems);
        YFCElement eleLineItem = getItemPriceInput.createElement(GCConstants.LINE_ITEM);
        eleLineItem.setAttribute(GCConstants.ITEM_ID, itemId);
        eleLineItem.setAttribute(GCConstants.UNIT_OF_MEASURE, GCConstants.EACH);
        eleLineItem.setAttribute(GCConstants.QUANTITY, "1");
        eleLineItems.appendChild(eleLineItem);

        Document getItemPrice =
            GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ITEM_PRICE, getItemPriceInput.getDocument());
        Element eleLineItemOutput =
            (Element) getItemPrice.getDocumentElement().getElementsByTagName(GCConstants.LINE_ITEM).item(0);

        if (!YFCCommon.isVoid(eleLineItemOutput)) {
          String sListPrice = eleLineItemOutput.getAttribute(GCConstants.LIST_PRICE);
          eleOutput.setAttribute("UnitPrice", sListPrice);
        }
      }


      if (YFCUtils.equals(storeClearance, GCConstants.YES)|| YFCCommon.equalsIgnoreCase(sExtnIsClearanceItem, GCConstants.NO)) {
        eleOutput.setAttribute("UnitPrice", unitPrice);
      }
      if (!YFCCommon.isVoid(primaryComponentId)) {
        eleInput.setAttribute(GCConstants.ITEM_ID, primaryComponentId);
      } else {
        eleInput.setAttribute(GCConstants.ITEM_ID, eleInput.getAttribute(GCConstants.EXTN_POSITEM_ID));
      }

      getStoreAvailabilty(env, inDoc, eleOutput);
    }
    return outputDoc;
  }

  private void getStoreAvailabilty(YFSEnvironment env, Document inDoc, Element eleOutput) throws GCException {
    Element eleInput = inDoc.getDocumentElement();
    String posItemId = eleInput.getAttribute(GCConstants.EXTN_POSITEM_ID);
    String kitCode = eleInput.getAttribute(GCConstants.KIT_CODE);
    String productClass = eleInput.getAttribute(GCConstants.PRODUCT_CLASS);
    String sExtnIsStoreClearance = eleInput.getAttribute("ExtnIsStoreClearance");

    String sExtnIsClearanceItem = eleInput.getAttribute("ExtnIsClearanceItem");

    if (YFCCommon.isVoid(posItemId)
        || (YFCUtils.equals(kitCode, GCConstants.BUNDLE) && YFCUtils.equals(productClass, GCConstants.REGULAR))) {
      eleOutput.setAttribute("StoreAvailability", "0");
    } else {
      YFCDocument dummyDoc = YFCDocument.getDocumentFor("<OrderList/>");
      String sDummyCodeValue = "";
      GCAddItemToCart addToCart = new GCAddItemToCart();
      YFCDocument docPOSSKUValidationRespose =
          addToCart.validatePOSSKU(env, YFCDocument.getDocumentFor(inDoc), dummyDoc, sDummyCodeValue);
      YFCElement elePOSSKUValidationRoot = docPOSSKUValidationRespose.getDocumentElement();
      String rootTag = elePOSSKUValidationRoot.getTagName();
      if (YFCUtils.equals(rootTag, "Response")) {
        eleOutput.setAttribute("StoreAvailability", GCConstants.N);
      }

      else {
        YFCElement eleSuccess = elePOSSKUValidationRoot.getElementsByTagName(GCConstants.SUCCESS).item(0);
        String sSuccess = eleSuccess.getNodeValue();
        if (YFCCommon.equals(sSuccess, GCConstants.TRUE))
        {
          LOGGER.verbose("Successful response is received from POS.");
          String sSKUNumber = "";
          String sSerialNumber = "";
          YFCElement eleSKUNumber =
              docPOSSKUValidationRespose.getDocumentElement().getElementsByTagName("SKU.Number").item(0);
          YFCElement eleSerialNumber =
              docPOSSKUValidationRespose.getDocumentElement().getElementsByTagName("SerialNumber").item(0);
          if (!YFCCommon.isVoid(eleSKUNumber)) {
            sSKUNumber = eleSKUNumber.getNodeValue();
          }
          if (!YFCCommon.isVoid(eleSerialNumber)) {
            sSerialNumber = eleSerialNumber.getNodeValue();
          }

          eleOutput.setAttribute("SKUNumber", sSKUNumber);
          eleOutput.setAttribute("SerialNumber", sSerialNumber);

          if(YFCCommon.equalsIgnoreCase(sExtnIsStoreClearance, GCConstants.N) && YFCCommon.equalsIgnoreCase(sExtnIsClearanceItem, GCConstants.YES))
          {
            eleOutput.setAttribute("StoreAvailability", "0");
          }

          else if(YFCCommon.isVoid(sExtnIsClearanceItem) && (YFCCommon.equalsIgnoreCase(sExtnIsStoreClearance, GCConstants.NO)))
          {
            eleOutput.setAttribute("StoreAvailability", "0");
          }

          else {
            String sQuantity = elePOSSKUValidationRoot.getElementsByTagName(GCConstants.QUANTITY).item(0).getNodeValue();
            eleOutput.setAttribute("StoreAvailability", sQuantity);
          }


        } else {
          YFCElement eleErrorCode = docPOSSKUValidationRespose.getElementsByTagName("ErrorInfo").item(0);

          if (!YFCCommon.isVoid(eleErrorCode)) {
            String sErrorCode = eleErrorCode.getNodeValue();
            if (YFCCommon.equalsIgnoreCase(sErrorCode, "Quantity is not available.")) {
              eleOutput.setAttribute("StoreAvailability", "0");
            } else {
              eleOutput.setAttribute("StoreAvailability", "0");
            }
          }
        }
      }
    }
  }

  private void getNetworkAvailability(YFSEnvironment env, Element eleInput, Element eleOutput) {
    String sItemID = eleInput.getAttribute(GCConstants.ITEM_ID);
    String storeId = eleInput.getAttribute(GCConstants.STORE_ID);
    String sProductClass = eleInput.getAttribute(GCConstants.PRODUCT_CLASS);
    String sUnitOfMeasure = eleInput.getAttribute(GCConstants.UNIT_OF_MEASURE);
    String sExtnIsStoreClearance = eleInput.getAttribute(GCXmlLiterals.EXTN_IS_STORE_CLEARANCE);
    String sUnitPrice = eleInput.getAttribute(GCConstants.UNIT_PRICE);
    String skitCode = eleInput.getAttribute(GCConstants.KIT_CODE);
    Element eleTag = (Element) eleInput.getElementsByTagName(GCConstants.TAG).item(0);
    YFCDocument docPromise = YFCDocument.getDocumentFor(GCConstants.ORDER_PROMISE_INPUT);
    YFCElement elePromise = docPromise.getDocumentElement();
    elePromise.removeAttribute("ConsiderPartialShipment");
    YFCElement eleExtn = elePromise.createChild(GCConstants.EXTN);
    eleExtn.setAttribute(GCConstants.EXTN_PICK_UP_STORE_ID, storeId);
    elePromise.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GC);
    YFCElement elePromiseLines = elePromise.getChildElement(GCConstants.PROMISE_LINES);
    YFCElement elePromiseLine = elePromiseLines.getChildElement(GCConstants.PROMISE_LINE);
    elePromiseLine.setAttribute(GCConstants.ITEM_ID, sItemID);
    elePromiseLine.setAttribute(GCConstants.PRODUCT_CLASS, sProductClass);
    elePromiseLine.setAttribute(GCConstants.UNIT_OF_MEASURE, sUnitOfMeasure);
    YFCElement eleExcludedNodes = elePromiseLine.createChild("ExcludedShipNodes");
    YFCElement eleExcludedNode = eleExcludedNodes.createChild("ExcludedShipNode");
    eleExcludedNode.setAttribute("Node", eleInput.getAttribute(GCConstants.STORE_ID));
    eleExcludedNode.setAttribute("SuppressNodeCapacity", "N");
    eleExcludedNode.setAttribute("SupressProcurement", "Y");
    eleExcludedNode.setAttribute("SupressSourcing", "Y");
    YFCElement eleExtnin = elePromiseLine.getChildElement(GCConstants.EXTN);

    String sExtnIsClearanceItem = eleInput.getAttribute("ExtnIsClearanceItem");
    if(YFCCommon.equalsIgnoreCase(sExtnIsClearanceItem, GCConstants.YES))    {
      eleExtnin.setAttribute(GCXmlLiterals.EXTN_IS_STORE_CLEARANCE, sExtnIsStoreClearance);
    }

    if(YFCCommon.isVoid(sExtnIsClearanceItem))
    {
      eleExtnin.setAttribute(GCXmlLiterals.EXTN_IS_STORE_CLEARANCE, sExtnIsStoreClearance);
    }

    eleExtnin.setAttribute(GCXmlLiterals.EXTN_UNIT_PRICE, sUnitPrice);
    String futureInventory = "0";
    String sProdAvailDate = "";
    if (!YFCCommon.isVoid(eleTag)) {
      String sBatchNo = eleTag.getAttribute(GCXmlLiterals.BATCH_NO);
      YFCElement eleTagin = docPromise.createElement(GCXmlLiterals.TAG);
      eleTagin.setAttribute(GCXmlLiterals.BATCH_NO, sBatchNo);
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input to Inventory " + (docPromise.toString()));
    }

    YFCDocument promiseOutDoc =
        GCCommonUtil.invokeService(env, GCConstants.GC_FIND_INVENTORY_WRAPPER_SERVICE, docPromise);

    YFCElement elePromiseOutput = promiseOutDoc.getDocumentElement();
    YFCNodeList<YFCElement> nlAssignment = null;
    if (YFCUtils.equals(skitCode, GCConstants.BUNDLE)) {
      YFCNodeList<YFCElement> nlPromiseLine =
          elePromiseOutput.getChildElement("SuggestedOption").getElementsByTagName(GCConstants.PROMISE_LINE);
      for (YFCElement elePromiseLineOp : nlPromiseLine) {
        String kitCode = elePromiseLineOp.getAttribute(GCConstants.KIT_CODE);
        if (YFCUtils.equals(kitCode, GCConstants.BUNDLE)) {
          nlAssignment = elePromiseLineOp.getElementsByTagName(GCXmlLiterals.ASSIGNMENT);
          break;
        }

      }
    } else {
      nlAssignment = elePromiseOutput.getChildElement("SuggestedOption").getElementsByTagName(GCXmlLiterals.ASSIGNMENT);
    }

    double dOnhandQty = 0.0;
    if (!YFCCommon.isVoid(nlAssignment)) {
      for (YFCElement eleAssignment : nlAssignment) {
        Date sysDate = new Date();

        double dAvailableQuantity = eleAssignment.getDoubleAttribute(GCXmlLiterals.INV_QTY);
        String sInvQTy = eleAssignment.getAttribute(GCXmlLiterals.INV_QTY);
        String sShipNode = eleAssignment.getAttribute(GCConstants.SHIP_NODE);
        sProdAvailDate = eleAssignment.getAttribute(GCConstants.PRODUCT_AVAIL_DATE);
        Date dProdAvailDate = eleAssignment.getDateAttribute(GCConstants.PRODUCT_AVAIL_DATE);
        if (!YFCCommon.isVoid(dProdAvailDate) && !YFCCommon.isVoid(sInvQTy)) {
          if (sysDate.compareTo(dProdAvailDate) < 0 && YFCUtils.equals(sShipNode, GCConstants.MFI)) {
            futureInventory = sInvQTy;
          } else {
            dOnhandQty = dOnhandQty + dAvailableQuantity;
          }
        }
      }
    }

    eleOutput.setAttribute("NetworkOnHandAvailability", YFCUtils.stringfromDouble(dOnhandQty));
    eleOutput.setAttribute("NetworkFutureAvailability", futureInventory);
    eleOutput.setAttribute("FirstAvailableDate", sProdAvailDate);
  }
}
