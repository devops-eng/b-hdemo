package com.gc.mpos;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCMPOSConfirmDraftOrder {
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCAddShippingChargesDetail.class.getName());

  public void confirmDraftOrder(YFSEnvironment env, Document inDoc) throws GCException {
    LOGGER.verbose("GCMPOSConfirmDraftOrder - confirmDraftOrder() : Start");
    LOGGER.verbose("GCMPOSConfirmDraftOrder inDoc is :-" + GCXMLUtil.getXMLString(inDoc));

    Element eleRoot = inDoc.getDocumentElement();
    
    //mPOS-2183 GCValidateManagerApprovalService merging with GCMPOSConfirmDraftOrderService: Start.
    Element eleLoginInput = (Element) eleRoot.getElementsByTagName("Login").item(0);
    Document docLoginInput = GCXMLUtil.getDocumentFromElement((Element) eleLoginInput);
    LOGGER.verbose("Login document prepared for GCValidateManagerApprovalService -->" + GCXMLUtil.getXMLString(docLoginInput));
    LOGGER.verbose("Invoking GCValidateManagerApprovalService ::");
    GCCommonUtil.invokeService(env, "GCValidateManagerApprovalService", docLoginInput);
    LOGGER.verbose("Back to GCMPOSConfirmDraftOrder class after Manager Approval validation");
    //mPOS-2183 GCValidateManagerApprovalService merging with GCMPOSConfirmDraftOrderService: End.
    
    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement yfcEleRoot = yfcInDoc.getDocumentElement();
    YFCElement eleConfirmDraftOrder = yfcEleRoot.getElementsByTagName("ConfirmDraftOrder").item(0);
    String sIsEmailSentReq = eleConfirmDraftOrder.getAttribute("IsEmailSentRequired");
    String sOrderHeaderKey = eleConfirmDraftOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
    String sEMailID = eleConfirmDraftOrder.getAttribute("EMailID");
    String sAddressVerificationDone = eleConfirmDraftOrder.getAttribute("AddressVerificationDone");	//MPOS-2209: Attribute added to check if address got validated.
    //MPOS-1617: To create customer on submit click: Start.
    YFCDocument orderListTemp = YFCDocument
            .getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_getOrderList.xml"));
    YFCDocument orderListInDoc = YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "'/>");
    YFCDocument orderListOutDoc = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, orderListInDoc, orderListTemp);
    YFCElement orderListRootEle = orderListOutDoc.getDocumentElement();
    YFCElement eleOrder = orderListRootEle.getElementsByTagName(GCConstants.ORDER).item(0);
    YFCElement elePersonInfoBillTo = eleOrder.getElementsByTagName(GCConstants.PERSON_INFO_BILL_TO).item(0);
    String sExtnSourceOrderNo = eleOrder.getAttribute(GCConstants.ORDER_NO);
  
    YFCDocument manageCustomerInDoc = YFCDocument.getDocumentFor("<Order/>");
    YFCElement manageCustomerDocRoot = manageCustomerInDoc.getDocumentElement();
    manageCustomerDocRoot.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    manageCustomerDocRoot.setAttribute("AddressVerificationDone", sAddressVerificationDone);
    YFCElement eleOrderExtn = manageCustomerDocRoot.createChild(GCConstants.EXTN);
    eleOrderExtn.setAttribute(GCConstants.EXTN_SOURCE_ORDER_NO, sExtnSourceOrderNo);
    YFCElement eleImportPersonInfo = manageCustomerInDoc.importNode(elePersonInfoBillTo, true);
    manageCustomerDocRoot.appendChild(eleImportPersonInfo);
    
    LOGGER.verbose("Input prepared for GCManageCustomerService is-->" + manageCustomerInDoc.toString());
    GCCommonUtil.invokeService(env, "GCManageMPOSCustomerService", manageCustomerInDoc);
    //MPOS-1617: End.

    String sExtnPOSCustomerID = "";
    if(!YFCCommon.isVoid(env.getTxnObject("ExtnPOSCustomerIDFromManageCustomer")))
    {
    	sExtnPOSCustomerID = env.getTxnObject("ExtnPOSCustomerIDFromManageCustomer").toString();
    }
	LOGGER.verbose("Class GCMPOSConfirmDraftOrder : Transaction object set as ExtnPOSCustomerID and the value is--> " + sExtnPOSCustomerID);
	env.setTxnObject("ExtnPOSCustomerIDFromConfirmOrder", sExtnPOSCustomerID);
    
    // calling confirmDraftOrder API
    LOGGER.verbose("Calling confirmDraftOrder API: Start:");
    YFCDocument confirmDraftOrderAPI =
        YFCDocument.getDocumentFor("<ConfirmDraftOrder EnterpriseCode='GC' OrderHeaderKey='" + sOrderHeaderKey + "'/>");
    GCCommonUtil.invokeAPI(env, "confirmDraftOrder", confirmDraftOrderAPI.getDocument());

    // Handling Email Receipt
    if (YFCCommon.equalsIgnoreCase("Y", sIsEmailSentReq)) {
      YFCDocument emailReceiptDoc =
          YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "' EMailID='" + sEMailID + "'/>");
      GCCommonUtil.invokeService(env, "GCMPOSEmailReceiptService", emailReceiptDoc.getDocument());
    }
  }
}
