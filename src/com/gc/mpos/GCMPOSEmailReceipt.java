package com.gc.mpos;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCMPOSEmailReceipt {
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCAddShippingChargesDetail.class.getName());

  public void emailReceipt(YFSEnvironment env, Document inDoc) {
    LOGGER.verbose("GCMPOSEmailReceipt - emailReceipt() : Start");
    LOGGER.verbose("GCMPOSEmailReceipt inDoc is :-" + GCXMLUtil.getXMLString(inDoc));

    YFCDocument mailReceipt = YFCDocument.getDocumentFor(inDoc);
    String sOrderHeaderKey = mailReceipt.getDocumentElement().getAttribute(GCConstants.ORDER_HEADER_KEY);
    String sReceiptEmail = mailReceipt.getDocumentElement().getAttribute("EMailID");
    String sCustomerEmail = "";

    YFCDocument orderDetailsInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "'/>");
    YFCDocument orderDetailsTemplate =
        YFCDocument.getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_getOrderList_EmailReceipt.xml"));  
    YFCDocument orderList = GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST, orderDetailsInput, orderDetailsTemplate);

    //MPOS-1546 fix: Start.
    YFCElement elePersonInfoBillTo = orderList.getElementsByTagName(GCConstants.PERSON_INFO_BILL_TO).item(0);
    if (!YFCCommon.isVoid(elePersonInfoBillTo)) {
    	sCustomerEmail = elePersonInfoBillTo.getAttribute(GCConstants.EMAIL_ID);
    	if(!YFCCommon.isVoid(sReceiptEmail))
    	{
    		elePersonInfoBillTo.setAttribute("AlternateEmailID", sReceiptEmail);
    	}
    	else
    	{
    		elePersonInfoBillTo.setAttribute("AlternateEmailID", sCustomerEmail);
    	}
    }
    //MPOS-1546 fix: End.

    YFCElement eleOrder = orderList.getElementsByTagName(GCConstants.ORDER).item(0);
    YFCElement eleOrderExtn = eleOrder.getElementsByTagName(GCConstants.EXTN).item(0);
   

    String sExtnOrderingStore = "";
    if (!YFCCommon.isVoid(eleOrderExtn)) {
      sExtnOrderingStore = eleOrderExtn.getAttribute(GCConstants.EXTN_ORDERING_STORE);
    }


    YFCElement eleCorporatePersonInfo = null;
    if (!YFCCommon.isVoid(sExtnOrderingStore)) {
      YFCDocument getOrgListInput =
          YFCDocument.getDocumentFor("<Organization  OrganizationCode='" + sExtnOrderingStore + "'/>");
      YFCDocument getOrgListTemp =
          YFCDocument
          .getDocumentFor(
              GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_getOrganizationListTemplate.xml"));
      YFCDocument getOrganizationList =
          GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORGANIZATION_LIST, getOrgListInput, getOrgListTemp);

      YFCElement eleOrganization = getOrganizationList.getElementsByTagName(GCConstants.ORGANIZATION).item(0);
      eleCorporatePersonInfo = eleOrganization.getElementsByTagName("CorporatePersonInfo").item(0);
    }
    
  //MPOS-1644 fix: Start.
    YFCDocument changeOrderInDoc = YFCDocument.getDocumentFor("<Order/>");
    YFCElement eleRoot = changeOrderInDoc.getDocumentElement();
    eleRoot.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    YFCElement eleImportPersonInfo = changeOrderInDoc.importNode(elePersonInfoBillTo, true);
    eleRoot.appendChild(eleImportPersonInfo);
    LOGGER.verbose("Input document to changeOrder API in GCMPOSEmailReceipt-->" + changeOrderInDoc.toString());
   //MPOS-1644 fix: End.
    
    GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, changeOrderInDoc.getDocument());	//MPOS-1546 fix.
    
    //MPOS-2378 Start.
    LOGGER.verbose("Proration logic begins::");
    YFCElement orderElement = orderList.getDocumentElement().getElementsByTagName(GCConstants.ORDER).item(0);
    YFCDocument docOrder = GCXMLUtil.getDocument(orderElement, true);
    docOrder = GCPushMPOSOrderToPOS.stampMPOSLatestOverrideNote(env, docOrder.getDocument());
    docOrder = GCPushMPOSOrderToPOS.overridePrice(env, docOrder.getDocument());
    String setParentLines = "";
    if(!YFCCommon.isVoid(env.getTxnObject("setParentLines")))
    {
    	setParentLines = env.getTxnObject("setParentLines").toString();
    	env.setTxnObject("setParentLines", setParentLines);
    }
    docOrder = GCMPOSReceiptInformation.bundleLineChargeProration(env, docOrder);
    //MPOS-2378: End.
    
    YFCDocument emailReceiptSent = YFCDocument.getDocumentFor(docOrder.getDocument());
    YFCElement eleStoreAddress = emailReceiptSent.createElement("StoreAddress");
    emailReceiptSent.getDocumentElement().appendChild(eleStoreAddress);
    GCXMLUtil.copyElement(emailReceiptSent, eleCorporatePersonInfo, eleStoreAddress);

    LOGGER.verbose("Doc going to Queue" + GCXMLUtil.getXMLString(emailReceiptSent.getDocument()));
    GCCommonUtil.invokeService(env, "GCMPOSPublishEmailReceiptService", emailReceiptSent.getDocument());
  }
}
