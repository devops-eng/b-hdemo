package com.gc.mpos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCMPOSOrderListSort {
	
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCMPOSOrderListSort.class.getName());
	
	public Document getSortedOrderList(YFSEnvironment env, Document inDoc) {
		LOGGER.verbose("Class: GCMPOSOrderListSort Method: getSortedOrderList BEGIN");
		LOGGER.verbose("Incoming Doc to the method-->:" + GCXMLUtil.getXMLString(inDoc));
		
		YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
		YFCElement eleRoot = inputDoc.getDocumentElement();
		YFCElement eleAttribute = eleRoot.getElementsByTagName("Attribute").item(0);
		String sName = eleAttribute.getAttribute("Name");
		String sDesc = eleAttribute.getAttribute("Desc");
		String sFirstName = "";
		String sLastName = "";
		
		Document getPageDocOut = GCCommonUtil.invokeAPI(env, "getPage", inDoc);
		YFCDocument docOut = YFCDocument.getDocumentFor(getPageDocOut);
		YFCElement eleDocOutRoot = docOut.getDocumentElement();
		YFCElement eleOutput = eleDocOutRoot.getElementsByTagName("Output").item(0);
		YFCElement eleOrderList = eleDocOutRoot.getElementsByTagName(GCConstants.ORDER_LIST).item(0);
		String sTotalOrderList = eleOrderList.getAttribute("TotalOrderList");
		YFCDocument orderListOutdoc = YFCDocument.createDocument(GCConstants.ORDER_LIST);
		YFCElement eleOrderListOutDocRoot = orderListOutdoc.getDocumentElement();
		eleOrderListOutDocRoot.setAttribute("TotalOrderList", sTotalOrderList);
		
		if(YFCCommon.equalsIgnoreCase(sName, GCConstants.CUSTOMER_LAST_NAME) || YFCCommon.equalsIgnoreCase(sName, GCConstants.CUSTOMER_FIRST_NAME))
		{
			LOGGER.verbose("Search on the basis of customer name:");
			YFCNodeList<YFCElement> nlOrder = eleOrderList.getElementsByTagName(GCConstants.ORDER);
			HashMap<String, List<YFCElement>> customerOrderMap = new HashMap<String, List<YFCElement>>();
			List orderList = new ArrayList<>();
			
			if(YFCCommon.equalsIgnoreCase(sName, GCConstants.CUSTOMER_FIRST_NAME))
			{
				LOGGER.verbose("Sorting for customer first name:");
				for (YFCElement eleOrder : nlOrder)
				{
					sFirstName = eleOrder.getAttribute(GCConstants.CUSTOMER_FIRST_NAME);
					if(customerOrderMap.containsKey(sFirstName))
					{
						List orderListRepeat = (List) customerOrderMap.get(sFirstName);
						orderListRepeat.add(eleOrder);
					}
					else
					{
						List orderListReg = new ArrayList<>();
						orderListReg.add(eleOrder);
						customerOrderMap.put(sFirstName, orderListReg);
					}
				}
			}
			else
			{
				LOGGER.verbose("Sorting for customer last name:");
				for (YFCElement eleOrder : nlOrder)
				{
					sLastName = eleOrder.getAttribute(GCConstants.CUSTOMER_LAST_NAME);
					if(customerOrderMap.containsKey(sLastName))
					{
						List orderListRepeat = (List) customerOrderMap.get(sLastName);
						orderListRepeat.add(eleOrder);
					}
					else
					{
						List orderListReg = new ArrayList<>();
						orderListReg.add(eleOrder);
						customerOrderMap.put(sLastName, orderListReg);					
					}
				}
			}
			
			LOGGER.verbose("Size of customerOrderMap: " + customerOrderMap.size());
			List customerList = new ArrayList(customerOrderMap.keySet());
			YFCElement eleOrderOutDoc = null;
			
			LOGGER.verbose("Case insensitive sorting begins:");
			Collections.sort(customerList, String.CASE_INSENSITIVE_ORDER);
			
			if(YFCCommon.equalsIgnoreCase(sDesc, GCConstants.YES))
			{
				LOGGER.verbose("Sorted list reversed:");
				Collections.reverse(customerList);
			
			}
			Iterator customerName = customerList.iterator();
			LOGGER.verbose("Preparing result doc begins:");
			while(customerName.hasNext())
			{
				Object name = customerName.next();
				List<YFCElement> outDocOrderList = (List)customerOrderMap.get(name);
				Iterator order = outDocOrderList.iterator();
				while(order.hasNext())
				{
					LOGGER.verbose("Inside multivalued key list loop:");
					Object next = order.next();
					eleOrderOutDoc = (YFCElement) next;
					orderListOutdoc.getDocumentElement().importNode(eleOrderOutDoc);
					eleOrderOutDoc = null;
				}
				
			}
	
			LOGGER.verbose("Final sorted orderList on the basis of customer name is:-->" + orderListOutdoc.toString());
			eleOutput.removeChild(eleOrderList);
			eleOutput.importNode(orderListOutdoc.getDocumentElement());
			return docOut.getDocument();
		}
		
		LOGGER.verbose("Final sorted orderList is:-->" + docOut.toString());
		return docOut.getDocument();	 
	}

}
