/**
 * File Name : GCMPOSReceiptInformation.java
 *
 *OBJECTIVE: This class is called when GCGetOrderSummaryDetails is invoked from mPOS. This class will prorate BUNDLE charges of SET parent to primary & secondary components.
 *
 */
package com.gc.mpos;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;

import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCMPOSReceiptInformation {
	private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCMPOSReceiptInformation.class.getName());

	  public Document receiptInfo(YFSEnvironment env, Document inDoc) throws GCException {
	    LOGGER.verbose("GCMPOSReceiptInformation - receiptInfo() : Start");
	    LOGGER.verbose("GCMPOSReceiptInformation inDoc is :-" + GCXMLUtil.getXMLString(inDoc));
	    
	  //mPOS-1527 : Proration work till invoicing is done : Start.
	    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
	    YFCElement eleRoot = inputDoc.getDocumentElement();
	    String sOrderHeaderKey = eleRoot.getAttribute(GCConstants.ORDER_HEADER_KEY);
	    String sEntryType="";
	    String sFirstName="";
	    String sLastName="";
	    String sCity="";
	    
	    YFCDocument getOrderDetailsInDoc = YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "'/>");
	    YFCDocument orderDetailsTemplate = YFCDocument.getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_getOrderDetails.xml"));
	    YFCDocument orderDetails = GCCommonUtil.invokeAPI(env, GCConstants.API_GET_COMPLETE_ORDER_DETAILS, getOrderDetailsInDoc, orderDetailsTemplate);
	    YFCElement eleOrderRoot = orderDetails.getDocumentElement();
	    sEntryType = eleOrderRoot.getAttribute(GCConstants.ENTRY_TYPE);
	    
	    //mPOS-1695 getOrganizationList, getUserList API merging: Start.
	    YFCElement eleExtn = eleOrderRoot.getElementsByTagName(GCConstants.EXTN).item(0);
	    String sExtnOrderingStore = eleExtn.getAttribute("ExtnOrderingStore");
	    if (!YFCCommon.isVoid(sExtnOrderingStore)) 
	    {
	        YFCDocument inDocOrganizationList = YFCDocument.getDocumentFor("<Organization  OrganizationCode='" + sExtnOrderingStore + "'/>");
	        YFCDocument tempOrganizationList = YFCDocument.getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_getOrganizationList.xml"));
	        YFCDocument outDocOrganizationList = GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORGANIZATION_LIST, inDocOrganizationList, tempOrganizationList);
	        
	        LOGGER.verbose("getOrganizationList outDoc -->" + outDocOrganizationList.toString());
	        
	        YFCElement rootOrganizationList = outDocOrganizationList.getDocumentElement();
	        YFCElement eleOrganization = rootOrganizationList.getElementsByTagName(GCConstants.ORGANIZATION).item(0);
	        YFCElement eleNode = eleOrganization.getElementsByTagName(GCConstants.NODE).item(0);
	        YFCElement eleShipNodePersonInfo = eleNode.getElementsByTagName(GCConstants.SHIP_NODE_PERSON_INFO).item(0);
	        sCity = eleShipNodePersonInfo.getAttribute(GCConstants.CITY);
	    }
	    
	    LOGGER.verbose("Stamping City and User FirstName, LastName -->");
	    YFCElement eleOrderExtn = eleOrderRoot.getElementsByTagName(GCConstants.EXTN).item(0);
	    String sExtnOrderCaptureChannel = eleOrderExtn.getAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL);
	    String sLoginid = eleOrderRoot.getAttribute(GCConstants.CREATE_USER_ID);
	    
	    if(YFCCommon.equalsIgnoreCase(sExtnOrderCaptureChannel, GCConstants.CALL_CENTER_ORDER_CHANNEL) || YFCCommon.equalsIgnoreCase(sExtnOrderCaptureChannel, GCConstants.GCSTORE))
	    {
	    	YFCDocument tempUserList = YFCDocument.getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_getUserList.xml"));
	    	YFCDocument inDocUserList = YFCDocument.createDocument(GCConstants.USER);
	    	inDocUserList.getDocumentElement().setAttribute("Loginid", sLoginid);
	    	YFCDocument outDocUserList = GCCommonUtil.invokeAPI(env, GCConstants.GET_USER_LIST, inDocUserList, tempUserList );
	    	
	    	LOGGER.verbose("getUserList outDoc -->" + outDocUserList.toString());
	    	
	    	YFCElement eleUserListRoot = outDocUserList.getDocumentElement();
	    	YFCElement eleContactPersonInfo = eleUserListRoot.getElementsByTagName("ContactPersonInfo").item(0);
	    	if(!YFCCommon.isVoid(eleContactPersonInfo))
	    	{
		    	sFirstName = eleContactPersonInfo.getAttribute(GCConstants.FIRST_NAME);
		    	sLastName = eleContactPersonInfo.getAttribute(GCConstants.LAST_NAME);
	    	}
	    }
	    
	    eleOrderRoot.setAttribute(GCConstants.FIRST_NAME, sFirstName);
	    eleOrderRoot.setAttribute(GCConstants.LAST_NAME, sLastName);
	    eleOrderExtn.setAttribute("ExtnOrderCity", sCity);
	  //mPOS-1695 getOrganizationList, getUserList API merging: End.
	    
	    
	    if(YFCCommon.equalsIgnoreCase(sEntryType, GCConstants.MPOS))
	    {
	    	LOGGER.verbose("Proration logic begins::");
	    	orderDetails = GCPushMPOSOrderToPOS.stampMPOSLatestOverrideNote(env, orderDetails.getDocument());
	    	orderDetails = GCPushMPOSOrderToPOS.overridePrice(env, orderDetails.getDocument());
	    	orderDetails = bundleLineChargeProration(env, orderDetails);
	    	LOGGER.verbose("Order Document After proration-->" + orderDetails.toString());
	  }
	    LOGGER.verbose("OutDoc from receiptInfo:- " + orderDetails.toString());
	    return orderDetails.getDocument();
	  
}
	  //MPOS-2378: Adjusting BundleParent line charges to components:
	  public static YFCDocument bundleLineChargeProration(YFSEnvironment env, YFCDocument orderDetails )
	  {
		  LOGGER.verbose("Input to bundleLineChargeProration-->\n" + orderDetails.toString());
		  Document proratedDoc = orderDetails.getDocument();
		  Element proratedDocRoot = proratedDoc.getDocumentElement();
		  
		  Map<String, Element> mapSecondaryComponents = new HashMap<String, Element>();
		  
	    	NodeList nlOrderLine = proratedDocRoot.getElementsByTagName(GCConstants.ORDER_LINE);
		    String sKitCode = "";
		    String setParentLines = "";
		    String sBundleParentUnitPrice = "";
		    if(!YFCCommon.isVoid(env.getTxnObject("setParentLines")))
		    {
		    	setParentLines = env.getTxnObject("setParentLines").toString();
		    }
		    
		    
		    int nLength = nlOrderLine.getLength();
		    
		    //MPOS-2378: Start.
		    for(int i = 0; i < nLength;  i++)
		    {
		    	//If even one SET item is present in the order
		    	if(!YFCCommon.equalsIgnoreCase(setParentLines, "0")) 
			    {
			   
			    	Element eleOrderLine = (Element) nlOrderLine.item(i);
			    	sKitCode = eleOrderLine.getAttribute(GCConstants.KIT_CODE);
			    	sBundleParentUnitPrice = eleOrderLine.getAttribute("BundleParentUnitPrice");
			    	
			    	if((YFCCommon.isVoid(sBundleParentUnitPrice)) && (!YFCCommon.equalsIgnoreCase(sKitCode, GCConstants.BUNDLE)))
			    	{
			    		Element eleLinePriceInfo = (Element) eleOrderLine.getElementsByTagName(GCConstants.LINE_PRICE_INFO).item(0);
			    		String sUnitPrice = eleLinePriceInfo.getAttribute(GCConstants.UNIT_PRICE);
			    		eleOrderLine.setAttribute(GCConstants.UNIT_PRICE, sUnitPrice);
			    		
			    	}
			    	
			    	
			    	if(YFCCommon.equalsIgnoreCase(sKitCode, GCConstants.BUNDLE))
			    	{
			    		 Double dTotalDiscountAmount = 0.0;
				    	 Double dPrimaryLineTotal = 0.0;
				    	 Double dSecondaryLineTotal = 0.0;
				    	 Double dAmountOnPrimary = 0.0;
				    	 Double dAmountOnSecondary = 0.0;
				    	 Double dPrimaryLineAfterDiscount = 0.0;
				    	 Element elePrimaryOrderLine = null;
				    	 Element eleSecondaryLineElement = null;
				    	 Element eleSecondaryOrderLine = null;
				    	 Double dChargeAmount = 0.0;
				    	 Double dremainingDiscountOnSecondary=0.0;
				    	 Element elePrimaryLineCharges = null;
				    	 Element eleSecondaryLineCharges = null;
				    	 List<Element> setComponentsList = new ArrayList<Element>(); 
				    	 List<String> secondaryOlkList = new ArrayList<String>();
			    		LOGGER.verbose("Bundle parent line");
			    		String sBundleParentOLK	= eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
			    		Element eleBundleItemDetails = (Element) eleOrderLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
			    		Element eleBundleItemDetailsExtn = (Element) eleBundleItemDetails.getElementsByTagName(GCConstants.EXTN).item(0);
			    		String sPrimaryItemID = eleBundleItemDetailsExtn.getAttribute(GCConstants.EXTN_PRIMARY_COMPONENT_ID);
			    		NodeList nlLineCharge = eleOrderLine.getElementsByTagName(GCConstants.LINE_CHARGE);
			    		
			    		setComponentsList = GCXMLUtil.getElementListByXpath(orderDetails.getDocument(),
			    	                    "Order/OrderLines/OrderLine[@BundleParentOrderLineKey='"
			    	                        + sBundleParentOLK + "']");
			    		if(!setComponentsList.isEmpty())
			    		{
			    			String sChidItemID = "";
			    			for (int z = 0; z < setComponentsList.size(); z++) {
			    			    Element eleChildElement = setComponentsList.get(z);
			    			    Element eleChildItem = (Element) eleChildElement.getElementsByTagName(GCConstants.ITEM).item(0);
			    				if(!YFCCommon.isVoid(eleChildItem))
			    				{
			    					sChidItemID = eleChildItem.getAttribute(GCConstants.ITEM_ID);
			    				}
			    				
			    				if(YFCCommon.equalsIgnoreCase(sChidItemID, sPrimaryItemID))
			    				{
			    					LOGGER.verbose("Order line is primary orderline");
			    					dPrimaryLineTotal += Double.parseDouble(eleChildElement.getAttribute(GCConstants.UNIT_PRICE));
			    					dPrimaryLineAfterDiscount = Double.parseDouble(eleChildElement.getAttribute(GCConstants.UNIT_PRICE));
			    					elePrimaryOrderLine = eleChildElement;
			    				}
			    				else
			    				{
			    					LOGGER.verbose("Order line is secondary orderline");
			    					String sSecondaryOlk = eleChildElement.getAttribute(GCConstants.ORDER_LINE_KEY);
			    					dSecondaryLineTotal += Double.parseDouble(eleChildElement.getAttribute(GCConstants.UNIT_PRICE));	
			    					mapSecondaryComponents.put(sSecondaryOlk, eleChildElement);
			    					secondaryOlkList.add(sSecondaryOlk);
			    				}
			    			}
			    		}
			    		
			    		// Adjust bundle line charges to components
			            for (int x = 0; x < nlLineCharge.getLength(); x++) {
			              Element eleBundleLineCharge = (Element) nlLineCharge.item(x);
			              if (YFCCommon.equals(GCConstants.PROMOTION, eleBundleLineCharge.getAttribute(GCConstants.CHARGE_CATEGORY))
			                  || YFCCommon.equals(GCConstants.SHIPPING_PROMOTION, eleBundleLineCharge.getAttribute(GCConstants.CHARGE_CATEGORY))) {
			            	 
			            	  dChargeAmount = Double.parseDouble(eleBundleLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT));
			                  dTotalDiscountAmount = Double.parseDouble(eleBundleLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT));
			                  dPrimaryLineTotal -= dChargeAmount;
			              } 
			              
			              // Check if the primary component line total is negative after discount
			              if (dPrimaryLineTotal < 0) {
			            	LOGGER.verbose("After discount calculation primary is negative");			                           
			                dPrimaryLineTotal += Double.parseDouble(eleBundleLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT));
			                dAmountOnPrimary = dPrimaryLineTotal;
			                dAmountOnSecondary = dChargeAmount - dPrimaryLineTotal;
			                dAmountOnSecondary = GCXMLUtil.roundOffPrice(dAmountOnSecondary);
			                String sNewUnitPrice = "";
			                //If more than one secondary component is present.
			                if(!secondaryOlkList.isEmpty())
			                {
			                	for(int s = 0; s < secondaryOlkList.size(); s++){

			                	String sSecondaryOlk = secondaryOlkList.get(s);
			                	eleSecondaryLineElement = mapSecondaryComponents.get(sSecondaryOlk);
			                	String sSecondaryLineAmount = eleSecondaryLineElement.getAttribute(GCConstants.UNIT_PRICE);
			                	sNewUnitPrice = eleSecondaryLineElement.getAttribute("NewUnitPrice");
			                	Double dSecondaryLineAmount = 0.0;
			              
			                	if(!YFCCommon.isVoid(sSecondaryLineAmount)){
			                	dSecondaryLineAmount= Double.parseDouble(sSecondaryLineAmount);
			                	}
			                	eleSecondaryLineCharges = (Element) eleSecondaryLineElement.getElementsByTagName(GCConstants.LINE_CHARGES).item(0);
			                	if(dAmountOnSecondary > dSecondaryLineAmount && (!YFCCommon.equalsIgnoreCase(sNewUnitPrice, "0.0")))
			                	{
			                		if (!YFCCommon.isVoid(eleSecondaryLineCharges)) {
			                			Element eleChildLineCharge = orderDetails.getDocument().createElement(GCConstants.LINE_CHARGE);
			                			eleSecondaryLineCharges.appendChild(eleChildLineCharge);
			                			GCXMLUtil.copyElement(orderDetails.getDocument(), eleBundleLineCharge, eleChildLineCharge);
			                			eleChildLineCharge.setAttribute(GCConstants.CHARGE_AMOUNT,dSecondaryLineAmount.toString());
			                			dAmountOnSecondary = dAmountOnSecondary - dSecondaryLineAmount;
			                			eleSecondaryLineElement.setAttribute("NewUnitPrice", "0.0");
			                		}
			                	}
			                	else{
			                			if(!YFCCommon.equalsIgnoreCase(sNewUnitPrice, "0.0")){
			                		if (!YFCCommon.isVoid(eleSecondaryLineCharges)) {
			                			Element eleChildLineCharge = orderDetails.getDocument().createElement(GCConstants.LINE_CHARGE);
			                			eleSecondaryLineCharges.appendChild(eleChildLineCharge);
			                			GCXMLUtil.copyElement(orderDetails.getDocument(), eleBundleLineCharge, eleChildLineCharge);
			                			eleChildLineCharge.setAttribute(GCConstants.CHARGE_AMOUNT,dAmountOnSecondary.toString());
			                			Double dpriceLeft = Double.parseDouble(sSecondaryLineAmount) - dAmountOnSecondary;
			                			eleSecondaryLineElement.setAttribute("NewUnitPrice", dpriceLeft.toString());
			                			dAmountOnSecondary = 0.0;
			                		}
			                			
			                	}
				               }
			                	
			      
			                	}
			               }
			                
			            elePrimaryLineCharges = (Element) elePrimaryOrderLine.getElementsByTagName(GCConstants.LINE_CHARGES).item(0);
			            if (!YFCCommon.isVoid(elePrimaryLineCharges) && (dAmountOnPrimary != 0.0 || dAmountOnPrimary != 0.00)) {
		                Element eleChildLineCharge = orderDetails.getDocument().createElement(GCConstants.LINE_CHARGE);
		                elePrimaryLineCharges.appendChild(eleChildLineCharge);
		                GCXMLUtil.copyElement(orderDetails.getDocument(), eleBundleLineCharge, eleChildLineCharge);
		                eleChildLineCharge.setAttribute(GCConstants.CHARGE_AMOUNT,dAmountOnPrimary.toString());
		                if(YFCCommon.equals(dPrimaryLineAfterDiscount, dAmountOnPrimary))
		                {
		                	dPrimaryLineTotal = 0.0;
		                }
		                
			            }
			              }
			              
			            else {
			            	elePrimaryLineCharges =
					                    (Element) elePrimaryOrderLine.getElementsByTagName(GCConstants.LINE_CHARGES).item(0);
			                Element eleChildLineCharge = orderDetails.getDocument().createElement(GCConstants.LINE_CHARGE);
			                elePrimaryLineCharges.appendChild(eleChildLineCharge);
			                GCXMLUtil.copyElement(orderDetails.getDocument(), eleBundleLineCharge, eleChildLineCharge);
			                dPrimaryLineAfterDiscount = dPrimaryLineAfterDiscount - dChargeAmount;
			                dPrimaryLineTotal = dPrimaryLineAfterDiscount;
			              }
			            }
			    }
			   }
		    	
		    	//No set item present in the order
		    	else 
		    	{
		    		Element eleOrderLine = (Element) nlOrderLine.item(i);
		    		Element eleLinePriceInfo = (Element) eleOrderLine.getElementsByTagName(GCConstants.LINE_PRICE_INFO).item(0);
		    		String sUnitPrice = eleLinePriceInfo.getAttribute(GCConstants.UNIT_PRICE);
		    		eleOrderLine.setAttribute(GCConstants.UNIT_PRICE, sUnitPrice);
		    		
		    	}
		    }
			return orderDetails;    
	    }
	  
}
		  
