/**
 * File Name : GCManageMPOSCustomer.java
 *
 * Description : This class has logic for Adding Shipping Charges
 *
 * Modification Log :
 * -------------------------------------------------------------------------------------------------------------------------------
 * Version #                    Date                    Author                              Modification
 * 0.1                      28/05/2016              Singh, Amit Mohan               Initial Draft for Managing Customers MPOS-305
 * -------------------------------------------------------------------------------------------------------------------------------
 */
package com.gc.mpos;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.api.participant.GCManageCOMCustomer;
import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCCustomerUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class GCManageMPOSCustomer {
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCUpdateMPOSOrder.class.getName());

  //MPOS-1333 Start.
  String sTimeOut;
  boolean IsCustomerPresent = false;

  public void setProperties(Properties prop) {
    sTimeOut = prop.getProperty(GCConstants.ATG_WS_TIME_OUT);
  }
  //MPOS-1333 End.
  
  public void manageCustomer(YFSEnvironment env, Document inDoc) {
    LOGGER.verbose("GCManageMPOSCustomer - manageCustomer() : Start");
    LOGGER.verbose("GCManageMPOSCustomer - manageCustomer() inDoc is" + GCXMLUtil.getXMLString(inDoc));

    YFCDocument manageCustomerInDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleRoot = manageCustomerInDoc.getDocumentElement();
    String sNodeName = eleRoot.getNodeName();
    String sFulfillmentType = "";
    String sCustomerID = "";
    
    env.setTxnObject("EntryType", "mPOS"); //MPOS-1553 fix.

    YFCElement eleOrderExtn = manageCustomerInDoc.getDocumentElement().getChildElement(GCConstants.EXTN);

    if (!YFCCommon.isVoid(eleOrderExtn)) {
      sFulfillmentType = manageCustomerInDoc.getDocumentElement().getChildElement(GCConstants.EXTN)
          .getAttribute(GCConstants.EXTN_FULFILLMENT_TYPE);
    }

    YFCNodeList<YFCElement> nlCustomer = manageCustomerInDoc.getElementsByTagName(GCConstants.CUSTOMER);
    if (!YFCCommon.isVoid(nlCustomer) && nlCustomer.getLength() > 0) {
      sNodeName = "Customer";
    }

    String sEmailId_ShipTo = "";
    String sDayPhone_ShipTo = "";
    String sPOSCustomerID = "";

    YFCDocument getCustomerList = YFCDocument.getDocumentFor("<CustomerList/>");

    // InDoc NodeName= Order :- Search: EmailID --> DayPhone
    if (YFCCommon.equalsIgnoreCase(sNodeName, "Order")) {
      sEmailId_ShipTo = manageCustomerInDoc.getElementsByTagName(GCConstants.PERSON_INFO_BILL_TO).item(0)
          .getAttribute(GCConstants.EMAIL_ID);
      sDayPhone_ShipTo = manageCustomerInDoc.getElementsByTagName(GCConstants.PERSON_INFO_BILL_TO).item(0)
          .getAttribute(GCConstants.DAY_PHONE);

      // If no email, dayphone on BillToInfo --> Return
      boolean emailDayphonePresent = true;
      if (YFCCommon.isVoid(sEmailId_ShipTo) && YFCCommon.isVoid(sDayPhone_ShipTo)) {
        emailDayphonePresent = false;
      }

      if (emailDayphonePresent) {
        boolean customerPresent = false;
        getCustomerList = isCustomerPresent(env, sEmailId_ShipTo, sDayPhone_ShipTo);

        if (getCustomerList.getDocumentElement().hasChildNodes()) {
          IsCustomerPresent = true;
        }
        updateCustomer(env, manageCustomerInDoc, sFulfillmentType, sNodeName, IsCustomerPresent, "", "",
            getCustomerList);
      }
      else {

        if(YFCCommon.equalsIgnoreCase(sNodeName, GCConstants.ORDER)){
          customerNameUpdate(env, manageCustomerInDoc, getCustomerList);
          GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, manageCustomerInDoc.getDocument());

        }
      }
      
    //MPOS-2416: Stamping ExtnSourceCustomerID: Start.
      YFCDocument inputChangeOrder = YFCDocument.getDocumentFor("<Order><Extn/></Order>");
      YFCElement inputChangeOrderRoot = inputChangeOrder.getDocumentElement();
      String sOHK = eleRoot.getAttribute(GCConstants.ORDER_HEADER_KEY);
      inputChangeOrderRoot.setAttribute(GCConstants.ORDER_HEADER_KEY, sOHK);
      YFCElement eleCustomerRoot = getCustomerList.getDocumentElement();
      String sSourceCustomerID = "";
      if(!IsCustomerPresent)
      {
    	  if(!YFCCommon.isVoid(env.getTxnObject("ExtnSourceCustomerID")))
    	  {
    		  
    		  sSourceCustomerID = env.getTxnObject("ExtnSourceCustomerID").toString();
    		  inputChangeOrderRoot.getElementsByTagName(GCConstants.EXTN).item(0).setAttribute(GCConstants.EXTN_SOURCE_CUSTOMER_ID, sSourceCustomerID);
    	  }
    	  
    	  LOGGER.verbose("Input to change order for ExtnSourceCustomerID -->" + inputChangeOrder.toString());
    	  GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, inputChangeOrder, null);
    	  LOGGER.verbose("Order after change order call --> " + inputChangeOrder.toString());
      }
  	//MPOS-2416 : Stamping ExtnSourceCustomerID: End.
    }

    // InDoc NodeName= Customer :- Search: CustomerID --> ExtnPOSCustomerID --> EmailID --> DayPhone
    else {
      boolean customerListPresent = true;
      YFCElement eleCustomer = manageCustomerInDoc.getDocumentElement().getChildElement(GCConstants.CUSTOMER);
      sCustomerID = eleCustomer.getAttribute(GCConstants.CUSTOMER_ID);

      if (YFCCommon.isVoid(sCustomerID)) {
        customerListPresent = false;
      }

      if (!customerListPresent) {
        YFCElement eleExtn = manageCustomerInDoc.getDocumentElement().getElementsByTagName(GCConstants.EXTN).item(0);
        if (!YFCCommon.isVoid(eleExtn)) {
          sPOSCustomerID = eleExtn.getAttribute("ExtnPOSCustomerID");
        }
      }

      if (YFCCommon.isVoid(sPOSCustomerID) && YFCCommon.isVoid(sCustomerID)) {
        customerListPresent = false;
      }

      if (!customerListPresent) {
        YFCElement eleCustomerContact =
            manageCustomerInDoc.getDocumentElement().getElementsByTagName(GCConstants.CUSTOMER_CONTACT).item(0);
        sEmailId_ShipTo = eleCustomerContact.getAttribute("EmailID");
        sDayPhone_ShipTo = eleCustomerContact.getAttribute(GCConstants.DAY_PHONE);

        getCustomerList = isCustomerPresent(env, sEmailId_ShipTo, sDayPhone_ShipTo);
        if (getCustomerList.getDocumentElement().hasChildNodes()) {
          customerListPresent = true;
        }
      }

      if (customerListPresent) {
        IsCustomerPresent = true;
      }
      updateCustomer(env, manageCustomerInDoc, sFulfillmentType, sNodeName, IsCustomerPresent, sCustomerID,
          sPOSCustomerID, getCustomerList);
    }
  }


  /**
   * This function sends required elements, attributes to manageCustomer function
   */
  public void updateCustomer(YFSEnvironment env, YFCDocument manageCustomerInDoc, String sFulfillmentType,
      String sNodeName, boolean CustomerPresent, String sCustomerID, String sPOSCustomerID,
      YFCDocument getCustomerList) {

    LOGGER.verbose("GCManageMPOSCustomer - updateCustomer() inDoc is");

    YFCElement eleDummy = manageCustomerInDoc.createElement("Dummy");

    if (!CustomerPresent) {
      LOGGER.verbose("Customer not present. Create a new Customer.");
      YFCElement elePersonInfoBillTo =
          manageCustomerInDoc.getDocumentElement().getElementsByTagName(GCConstants.PERSON_INFO_BILL_TO).item(0);

      YFCElement elePersonInfo = manageCustomerInDoc.getDocumentElement().getElementsByTagName(GCConstants.PERSON_INFO).item(0);

      if(YFCCommon.equalsIgnoreCase(sNodeName, GCConstants.ORDER)){

        manageCustomerAPICall(env, elePersonInfoBillTo, eleDummy, manageCustomerInDoc, "", "", sNodeName, getCustomerList);
      }

      else{

        manageCustomerAPICall(env, eleDummy, elePersonInfo, manageCustomerInDoc, "", "", sNodeName, getCustomerList);
      }

    }
    else {
      LOGGER.verbose("Customer already present. Check if entered address is already present.");
      boolean IsPOSCustomerIDPresent = false;
      CustomerPresent = true;
      String sOrderAddressLine1Present = "";
      YFCElement elePersonInfoBillTo = null;


      if (YFCCommon.equalsIgnoreCase(sNodeName, GCConstants.ORDER)){

        elePersonInfoBillTo = manageCustomerInDoc.getDocumentElement().getElementsByTagName(GCConstants.PERSON_INFO_BILL_TO).item(0);
        sOrderAddressLine1Present = elePersonInfoBillTo.getAttribute(GCConstants.ADDRESS_LINE1);
      }


      if (!YFCCommon.isVoid(sOrderAddressLine1Present)
          || YFCCommon.equalsIgnoreCase(GCConstants.CUSTOMER, sNodeName)) {

        YFCDocument getCustomerListTemplate = YFCDocument
            .getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/getCustomerList_TemplateXML.xml"));

        String sOrganizationCode = manageCustomerInDoc.getDocumentElement().getAttribute(GCConstants.ENTERPRISE_CODE);
        if (YFCCommon.equalsIgnoreCase(sNodeName, "Order")) {
          sCustomerID = getCustomerList.getDocumentElement().getChildElement(GCConstants.CUSTOMER)
              .getAttribute(GCConstants.CUSTOMER_ID);
        }


        if (!YFCCommon.isVoid(sPOSCustomerID) && YFCCommon.equalsIgnoreCase(sNodeName, GCConstants.CUSTOMER)) {
          YFCDocument getCustomerListInDoc = YFCDocument.getDocumentFor(
              "<Customer OrganizationCode='GC'><Extn ExtnPOSCustomerID='" + sPOSCustomerID + "'/></Customer>");

          getCustomerList =
              GCCommonUtil.invokeAPI(env, GCConstants.GET_CUSTOMER_LIST, getCustomerListInDoc, getCustomerListTemplate);
        }

        if(!YFCCommon.isVoid(sCustomerID) && YFCCommon.equalsIgnoreCase(sNodeName, GCConstants.CUSTOMER)){
          YFCDocument getCustomerListInDoc = YFCDocument.getDocumentFor(
              "<Customer OrganizationCode='GC' CustomerID='" + sCustomerID + "'/>");

          getCustomerList =
              GCCommonUtil.invokeAPI(env, GCConstants.GET_CUSTOMER_LIST, getCustomerListInDoc, getCustomerListTemplate);

        }
        String sCustomerContactID = getCustomerList.getElementsByTagName(GCConstants.CUSTOMER_CONTACT).item(0)
            .getAttribute("CustomerContactID");

        String sAddressInput = "";
        //MPOS-2345: NodeList created in case of multiple addresses: Start.
        YFCNodeList<YFCElement> nlPersonInfoInput =
            manageCustomerInDoc.getDocumentElement().getElementsByTagName(GCConstants.PERSON_INFO);
        YFCElement elePersonInfoInput = null;
       //MPOS-2345: NodeList created in case of multiple addresses: End.
        
        // PersonShipToInfo address combined in a string
        if (YFCCommon.equalsIgnoreCase(sNodeName, GCConstants.ORDER)) {

          String sOrderAddressLine1 = "";
          String sOrderAddressLine2 = "";
          String sOrderAddressLine3 = "";

          if(!YFCCommon.isVoid(elePersonInfoBillTo.getAttribute(GCConstants.ADDRESS_LINE1))){
            String sAddressLine1 = elePersonInfoBillTo.getAttribute(GCConstants.ADDRESS_LINE1);
            sOrderAddressLine1 = sAddressLine1.toUpperCase().trim();
          }

          if(!YFCCommon.isVoid(elePersonInfoBillTo.getAttribute(GCConstants.ADDRESS_LINE2))){
            String sAddressLine2 = elePersonInfoBillTo.getAttribute(GCConstants.ADDRESS_LINE2);
            sOrderAddressLine2 = sAddressLine2.toUpperCase().trim();
          }

          if(!YFCCommon.isVoid(elePersonInfoBillTo.getAttribute(GCConstants.ADDRESS_LINE3))){
            String sAddressLine3 = elePersonInfoBillTo.getAttribute(GCConstants.ADDRESS_LINE3);
            sOrderAddressLine3 = sAddressLine3.toUpperCase().trim();
          }

          String sOrderDayPhone = elePersonInfoBillTo.getAttribute(GCConstants.DAY_PHONE);

          sAddressInput = sOrderAddressLine1 + "|" + sOrderAddressLine2
              + "|" + sOrderAddressLine3 + "|" + sOrderDayPhone;
        }
        
        
        // This map contains all addresses present in Customer Details
        Map<String, YFCElement> uniqueaddressInGetCustomerDetails = new HashMap<String, YFCElement>();

        YFCNodeList<YFCElement> nlCustomerAdditionalAddress =
            getCustomerList.getElementsByTagName(GCConstants.CUSTOMER_ADDITIONAL_ADDRESS);
        for (int x = 0; x < nlCustomerAdditionalAddress.getLength(); x++) {
          YFCElement eleCustomerAdditionalAddress = nlCustomerAdditionalAddress.item(x);
          YFCElement elePersonInfo = eleCustomerAdditionalAddress.getChildElement(GCConstants.PERSON_INFO);
          
          String sAddressLine1 = "";
          String sAddressLine2 = "";
          String sAddressLine3 = "";
          String sDayPhone ="";
          if(!YFCCommon.isVoid(elePersonInfo)){
        	   sAddressLine1 = elePersonInfo.getAttribute(GCConstants.ADDRESS_LINE1);
               sAddressLine2 = elePersonInfo.getAttribute(GCConstants.ADDRESS_LINE2);
               sAddressLine3 = elePersonInfo.getAttribute(GCConstants.ADDRESS_LINE3);
               sDayPhone = elePersonInfo.getAttribute(GCConstants.DAY_PHONE);
          }

          String sPresentAddress = sAddressLine1.toUpperCase().trim() + "|" + sAddressLine2.toUpperCase().trim() + "|"
              + sAddressLine3.toUpperCase().trim() + "|" + sDayPhone;
          uniqueaddressInGetCustomerDetails.put(sPresentAddress, eleCustomerAdditionalAddress);
        }
        
        //MPOS-2345: Fetching the personInfo added/modified: Start.
        LOGGER.verbose("Check if new/modified address present");
    	Map<String, YFCElement> mapNewOrModifiedPersonInfo = new HashMap<String, YFCElement>();
    	String sAddressStringPrepared = "";
    	if(YFCCommon.equalsIgnoreCase(sNodeName, GCConstants.CUSTOMER))
    	{
    		int countAddressToValidate = 0;
    		for(int z = 0; z < nlPersonInfoInput.getLength(); z++)
    		{
    			elePersonInfoInput = nlPersonInfoInput.item(z);
    			String sAddressLine1 = elePersonInfoInput.getAttribute(GCConstants.ADDRESS_LINE1);
    	        String sAddressLine2 = elePersonInfoInput.getAttribute(GCConstants.ADDRESS_LINE2);
    	        String sAddressLine3 = elePersonInfoInput.getAttribute(GCConstants.ADDRESS_LINE3);
    	        String sDayPhone = elePersonInfoInput.getAttribute(GCConstants.DAY_PHONE);

    	         sAddressStringPrepared= sAddressLine1.toUpperCase().trim() + "|" + sAddressLine2.toUpperCase().trim() + "|"
    	              + sAddressLine3.toUpperCase().trim() + "|" + sDayPhone;
    	         
    	         if(!uniqueaddressInGetCustomerDetails.containsKey(sAddressStringPrepared))
    	         {
    	        	 LOGGER.verbose("New/Modified address found --> " + sAddressInput);
    	        	 countAddressToValidate ++;
    	        	 sAddressInput = sAddressStringPrepared;
    	         }
    		}
    	        	 
    	        if(countAddressToValidate > 0)
    	        {
    	        	 manageCustomerAPICall(env, eleDummy, elePersonInfoInput, manageCustomerInDoc, sCustomerID, sCustomerContactID, sNodeName, getCustomerList);
    	        }          	         
    		}
    	
        if (!uniqueaddressInGetCustomerDetails.containsKey(sAddressInput)) {
          if (YFCCommon.equalsIgnoreCase(sNodeName, GCConstants.ORDER)) {
            manageCustomerAPICall(env, elePersonInfoBillTo, eleDummy, manageCustomerInDoc, sCustomerID,
                sCustomerContactID,
                sNodeName, getCustomerList);
          }
         
        }
        else {

          if(YFCCommon.equalsIgnoreCase(sNodeName, GCConstants.ORDER)){
            YFCDocument inDocToChangeOrder = customerNameUpdate(env, manageCustomerInDoc,getCustomerList);
            GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, inDocToChangeOrder.getDocument());
          }
        }
      }
      if (YFCCommon.isVoid(sOrderAddressLine1Present)) {

        if(YFCCommon.equalsIgnoreCase(sNodeName, GCConstants.ORDER)){
          customerNameUpdate(env, manageCustomerInDoc,getCustomerList);
          GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, manageCustomerInDoc.getDocument());
        }
      }
    }
  }


  /**
   * This function calls manageCustomer API to create or update customer
   */
  public void manageCustomerAPICall(YFSEnvironment env, YFCElement elePersonInfoShipTo, YFCElement elePersonInfoInput,
      YFCDocument manageCustomerInDoc, String sCustomerID, String sCustomerContactID, String sNodeName, YFCDocument docCustomerList) {

    LOGGER.verbose("GCManageMPOSCustomer - manageCustomer() : Start");
    String sFirstName = "";
    String sLastName = "";
    String sDayPhone = "";
    String sEmailID = "";
    String sAddressVerificationDone = "";
    YFCDocument manageCustomerTemp =
            YFCDocument.getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_manageCustomer.xml"));
    Document manageCutomerOutput = null;
    if (YFCCommon.equalsIgnoreCase(GCConstants.ORDER, sNodeName)) {
      sFirstName = elePersonInfoShipTo.getAttribute(GCConstants.FIRST_NAME);
      sLastName = elePersonInfoShipTo.getAttribute(GCConstants.LAST_NAME);
      sDayPhone = elePersonInfoShipTo.getAttribute(GCConstants.DAY_PHONE);
      sEmailID = elePersonInfoShipTo.getAttribute(GCConstants.EMAIL_ID);
    }
    else {
      sFirstName = elePersonInfoInput.getAttribute(GCConstants.FIRST_NAME);
      sLastName = elePersonInfoInput.getAttribute(GCConstants.LAST_NAME);
      sDayPhone = elePersonInfoInput.getAttribute(GCConstants.DAY_PHONE);
      sEmailID = elePersonInfoInput.getAttribute("EmailID");
    }
    YFCDocument manageCustomerAPIInput = YFCDocument.createDocument(GCConstants.CUSTOMER);
    manageCustomerAPIInput.getDocumentElement().setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GC);
    manageCustomerAPIInput.getDocumentElement().setAttribute(GCConstants.CUSTOMER_TYPE,
        GCConstants.CONSUMER_CUSTOMER_CODE);
    manageCustomerAPIInput.getDocumentElement().setAttribute("Status", GCConstants.CUSTOMER_ACTIVE_STATUS);
    if (IsCustomerPresent) {
      manageCustomerAPIInput.getDocumentElement().setAttribute(GCConstants.CUSTOMER_ID, sCustomerID);
    }
    YFCElement eleCustomerContactList = manageCustomerAPIInput.createElement(GCConstants.CUSTOMER_CONTACT_LIST);
    manageCustomerAPIInput.getDocumentElement().appendChild(eleCustomerContactList);
    YFCElement eleCustomerContact = manageCustomerAPIInput.createElement(GCConstants.CUSTOMER_CONTACT);
    if (IsCustomerPresent) {
      eleCustomerContact.setAttribute("CustomerContactID", sCustomerContactID);
    } else {
      eleCustomerContact.setAttribute(GCConstants.FIRST_NAME, sFirstName);
      eleCustomerContact.setAttribute(GCConstants.LAST_NAME, sLastName);
      eleCustomerContact.setAttribute(GCConstants.DAY_PHONE, sDayPhone);
      eleCustomerContact.setAttribute("EmailID", sEmailID);
    }
    eleCustomerContactList.appendChild(eleCustomerContact);
    YFCElement eleCustomerAdditionalAddressList =
        manageCustomerAPIInput.createElement(GCConstants.CUSTOMER_ADDITIONAL_ADDRESS_LIST);
    eleCustomerContact.appendChild(eleCustomerAdditionalAddressList);
    YFCElement eleCustomerAdditionalAddress =
        manageCustomerAPIInput.createElement(GCConstants.CUSTOMER_ADDITIONAL_ADDRESS);
    eleCustomerAdditionalAddress.setAttribute(GCConstants.IS_BILLTO, "Y");
    eleCustomerAdditionalAddress.setAttribute(GCConstants.IS_DEFAULT_BILLTO, "Y");
    eleCustomerAdditionalAddress.setAttribute("IsShipTo", "Y");
    eleCustomerAdditionalAddressList.appendChild(eleCustomerAdditionalAddress);
    YFCElement elePersonInfo = manageCustomerAPIInput.createElement(GCConstants.PERSON_INFO);
    if(YFCCommon.equalsIgnoreCase(sNodeName, GCConstants.ORDER))
    {
    	sAddressVerificationDone = manageCustomerInDoc.getDocumentElement().getAttribute("AddressVerificationDone");
    	if(YFCCommon.equalsIgnoreCase(sAddressVerificationDone, "Y"))
    	{
    		GCXMLUtil.copyAttributes(elePersonInfoShipTo, elePersonInfo);
    	}
    
    eleCustomerAdditionalAddress.appendChild(elePersonInfo);

    LOGGER.verbose("Input for manageCustomer API is:-" + manageCustomerAPIInput.toString());
    
    if(YFCCommon.equalsIgnoreCase(sNodeName, GCConstants.ORDER))
    {
    	if(YFCCommon.equalsIgnoreCase(sAddressVerificationDone, GCConstants.YES) || !docCustomerList.getDocumentElement().hasChildNodes() )
    	manageCutomerOutput = GCCommonUtil.invokeAPI(env, GCConstants.MANAGE_CUSTOMER_API, manageCustomerAPIInput.getDocument(),
                manageCustomerTemp.getDocument());
    }
   }
    else
    {
    	//MPOS-2345: Customer address validation: Start.
        if(YFCCommon.equalsIgnoreCase(sNodeName, GCConstants.CUSTOMER) && !IsCustomerPresent)
        {
        	LOGGER.verbose("Customer imported is new. Address validation needed to persist the address:");
        	LOGGER.verbose("Doc going for address validation --> \n" + manageCustomerInDoc.toString());
        	try {
        		manageCutomerOutput = addressValidation(env, manageCustomerInDoc, null);
    			} catch(Exception e){
    					LOGGER.error(
    					    " Inside GCManageMPOSCustomer catch block of manageCustomerAPICall. Exception occured while validating address of the imported new customer. Exception is :",e);
    			}
        	}
        
        if(YFCCommon.equalsIgnoreCase(sNodeName, GCConstants.CUSTOMER) && IsCustomerPresent)
        {
        	LOGGER.verbose("Customer imported is existing customer with new/modified address. Address validation needed to persist the address:");
        	LOGGER.verbose("Doc going for address validation --> \n" + manageCustomerInDoc.toString());
        	try {
        		manageCutomerOutput = addressValidation(env, manageCustomerInDoc, docCustomerList);
    			} catch(Exception e){
    					LOGGER.error(
    					    " Inside GCManageMPOSCustomer catch block of manageCustomerAPICall. Exception occured while validating new/modified address of the existing imported customer. Exception is :",e);
    			}
        	}
      //MPOS-2345: Customer address validation: End.
    }
    
    YFCDocument manageCustATGInteraction = null;
    if(!YFCCommon.isVoid(manageCutomerOutput))
    {
    	manageCustATGInteraction = YFCDocument.getDocumentFor(manageCutomerOutput); //MPOS-1333 fix
    }

    String sCustomerIDObtained ="";
    if (!IsCustomerPresent) {
      sCustomerIDObtained = manageCutomerOutput.getDocumentElement().getAttribute(GCConstants.CUSTOMER_ID);
      if(YFCCommon.equalsIgnoreCase(sNodeName, GCConstants.ORDER))
      {
    	  manageCustomerInDoc.getDocumentElement().setAttribute(GCConstants.BILL_TO_ID, sCustomerIDObtained);
      }
      else
      {
    	  manageCustomerInDoc.getDocumentElement().getElementsByTagName(GCConstants.CUSTOMER).item(0).setAttribute(GCConstants.CUSTOMER_ID, sCustomerIDObtained);
      }

      if(YFCCommon.equalsIgnoreCase(sNodeName, GCConstants.ORDER)){
        customerNameUpdate(env, manageCustomerInDoc,null);
        GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, manageCustomerInDoc.getDocument());
      }
    }
    else {

      if(YFCCommon.equalsIgnoreCase(sNodeName, GCConstants.ORDER)){
        customerNameUpdate(env, manageCustomerInDoc,docCustomerList);	//Added docCustomerList to get BillToID for existing customer in case address is modified
        GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, manageCustomerInDoc.getDocument());
      }

    }
  
    //MPOS-1333 Start.
    // ATG Interaction
    Document getCustListOutDoc = docCustomerList.getDocument();
    if(!YFCCommon.isVoid(manageCustATGInteraction))
	{
	    Document docATG = manageCustATGInteraction.getDocument();
		Element eleRoot = docATG.getDocumentElement();
		String sCusID = eleRoot.getAttribute(GCConstants.CUSTOMER_ID);
	    if(YFCCommon.isVoid(docCustomerList.getDocumentElement()) || !docCustomerList.getDocumentElement().hasChildNodes())
	    {
	    	getCustListOutDoc = (GCCustomerUtil.callGetCustomerList(env, sCusID, eleRoot.getAttribute("OrganizationCode"))).getDocument();
	    }
	    else
	    {
	    	if(YFCCommon.equalsIgnoreCase(sNodeName, GCConstants.ORDER)){
	    	LOGGER.verbose("Existing customer updated.");
	    	YFCDocument inDocToValidateInteraction = 
	        		YFCDocument.getDocumentFor("<CustomerList>"
	        				+ "<Customer>"
	        				+ "<CustomerContactList>"
	    		    		+ "<CustomerContact>"
	    		    		+ "<CustomerAdditionalAddressList>"
	    		    		+ "<CustomerAdditionalAddress>"
	    		    		+ "<PersonInfo/>"
	    		    		+ "</CustomerAdditionalAddress>"
	    		    		+ "</CustomerAdditionalAddressList>"
	    		    		+ "</CustomerContact>"
	    		    		+ "</CustomerContactList>"
	    		    		+ "</Customer>"
	    		    		+ "</CustomerList>");
	    	LOGGER.verbose("Indoc to Validate if ATG interaction needed.");
	        YFCElement elePersonInfoToInteraction = inDocToValidateInteraction.getElementsByTagName(GCConstants.PERSON_INFO).item(0);
	        YFCElement eleSrcPersonInfo = manageCustomerInDoc.getElementsByTagName(GCConstants.PERSON_INFO_BILL_TO).item(0);
	        GCXMLUtil.copyAttributes(eleSrcPersonInfo, elePersonInfoToInteraction);
	        YFCElement eleCustomerToInteract = inDocToValidateInteraction.getElementsByTagName(GCConstants.CUSTOMER).item(0);
	        YFCElement eleSrcCustomer = docCustomerList.getElementsByTagName(GCConstants.CUSTOMER).item(0);
	        GCXMLUtil.copyAttributes(eleSrcCustomer, eleCustomerToInteract);
	        YFCElement eleCustomerContactToInteract = inDocToValidateInteraction.getElementsByTagName(GCConstants.CUSTOMER_CONTACT).item(0);
	        YFCElement eleSrcCustomerContact = docCustomerList.getElementsByTagName(GCConstants.CUSTOMER_CONTACT).item(0);
	        GCXMLUtil.copyAttributes(eleSrcCustomerContact, eleCustomerContactToInteract);
	        getCustListOutDoc = inDocToValidateInteraction.getDocument();
	    	}
	    	
	    	else{
	    		NodeList nlAdditonalAddress = manageCutomerOutput.getElementsByTagName(GCConstants.CUSTOMER_ADDITIONAL_ADDRESS);
	    		int additionalAddressLen = nlAdditonalAddress.getLength();
	    		if(additionalAddressLen > 1)
	    		{
		    		for(int p = 0; p < additionalAddressLen; p++)
		    		{
		    			Element eleCustomerAdditonalAddressCheck = (Element) nlAdditonalAddress.item(p);
		    			Element elePersonInfoAddressCheck = (Element) eleCustomerAdditonalAddressCheck.getElementsByTagName(GCConstants.PERSON_INFO).item(0);
		    			if(!YFCCommon.isVoid(elePersonInfoAddressCheck))
		    			{
		    				String sAddressline1Check = elePersonInfoAddressCheck.getAttribute(GCConstants.ADDRESS_LINE1);
		    				if(YFCCommon.isVoid(sAddressline1Check))
		    				{
		    					eleCustomerAdditonalAddressCheck.getParentNode().removeChild(eleCustomerAdditonalAddressCheck);
		    					additionalAddressLen --;
		    					p--;
		    				}
		    			}
		    		}
	    		}
	    		Document manageCustomerOpPrepared = GCXMLUtil.createDocument(GCConstants.CUSTOMER_LIST);
	    		Element rootOpPrepared = manageCustomerOpPrepared.getDocumentElement();
	    		Element eleImportcustomer = (Element) manageCustomerOpPrepared.importNode(manageCutomerOutput.getDocumentElement(), true);
	    		rootOpPrepared.appendChild(eleImportcustomer);
	    		getCustListOutDoc = manageCustomerOpPrepared;
	    	}
	    }
	    LOGGER.verbose("Indoc to isATGInteractionNeeded --> " + GCXMLUtil.getXMLString(getCustListOutDoc));
	    boolean isATGInteractionRequired = GCManageCOMCustomer.isATGInteractionNeeded(env, getCustListOutDoc);
	    if (isATGInteractionRequired) {
	      try 
	      {
	    	  YFCDocument yfcCustomerList = YFCDocument.getDocumentFor(getCustListOutDoc); 
		    	if(YFCCommon.equalsIgnoreCase(sNodeName, GCConstants.ORDER))
		    	{
		    		LOGGER.verbose("Root node is Order\n");
		    		if(!docCustomerList.getDocumentElement().hasChildNodes() || YFCCommon.equalsIgnoreCase(sAddressVerificationDone, GCConstants.YES))   //Added these checks as a fix for MPOS-2209  		
		    		{
		    			if(docCustomerList.getDocumentElement().hasChildNodes())
		    			{
		    				LOGGER.verbose("Calling getCustomerList to get all the address after customer creation/updation");
		    				YFCDocument inDoc = YFCDocument.createDocument("Customer");
		    				YFCElement rootEle = inDoc.getDocumentElement();
		    				rootEle.setAttribute("CustomerID", sCusID);
		    				rootEle.setAttribute("OrganizationCode", GCConstants.GC);
		    				Document getCustomerListmplDoc = GCCommonUtil
		    						.getDocumentFromPath("/global/template/api/getCustomerList_TemplateXML.xml");
		    				yfcCustomerList = GCCommonUtil.invokeAPI(env,
		    						GCConstants.GET_CUSTOMER_LIST, inDoc,
		    						YFCDocument.getDocumentFor(getCustomerListmplDoc));
		    				LOGGER.verbose("CustomerList obtained --> " + yfcCustomerList.toString() );
		    			}
		    			LOGGER.verbose("Inside ATG interaction try block: Customer is either created or updated.\n");
		    			YFCDocument manageATGoutDoc = GCCustomerUtil.manageATGCustomer(env, manageCustATGInteraction, yfcCustomerList, sTimeOut);
		    			GCCommonUtil.invokeAPI(env, GCConstants.MANAGE_CUSTOMER_API, manageATGoutDoc, manageCustomerTemp);
		    		}
			    } 
		    	
		    	else
		    	{
		    		LOGGER.verbose("Inside ATG interaction try block for Root Node Customer: Customer is imported");
		    		YFCDocument manageATGoutDoc = GCCustomerUtil.manageATGCustomer(env, manageCustATGInteraction, yfcCustomerList, sTimeOut);
					GCCommonUtil.invokeAPI(env, GCConstants.MANAGE_CUSTOMER_API, manageATGoutDoc, manageCustomerTemp);
		    	}
	    	}catch (YFSException expUnused) {
	    		LOGGER.verbose("ATG Interaction has been unsuccessful.");
	      }
	    }
	}
   //MPOS-1333 End.
  }

  /**
   * This function calls getCustomerList using EmailID or DayPhone
   */
  public YFCDocument getCustomerListEmail_DayPhone(YFSEnvironment env, String sEmailId, String sDayPhone) {

    LOGGER.verbose("GCManageMPOSCustomer - getCustomerListEmail_DayPhone() : Start");

    YFCDocument getCustomerListIp = YFCDocument.createDocument(GCConstants.CUSTOMER);
    YFCElement eleCustomer = getCustomerListIp.getDocumentElement();
    eleCustomer.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GC);
    YFCElement eleOrderBy = eleCustomer.createChild(GCXmlLiterals.ORDER_BY);
    YFCElement eleAttribute = eleOrderBy.createChild(GCXmlLiterals.ATTRIBUTE);
    eleAttribute.setAttribute(GCXmlLiterals.DESC, GCConstants.YES);
    eleAttribute.setAttribute(GCXmlLiterals.NAME, GCXmlLiterals.MODIFYTS);
    YFCElement eleCustomerContactList = eleCustomer.createChild(GCXmlLiterals.CUSTOMER_CONTACT_LIST);
    YFCElement eleCustomerContact = eleCustomerContactList.createChild(GCXmlLiterals.CUSTOMER_CONTACT);
    if(YFCCommon.equalsIgnoreCase(sDayPhone, "")){
      eleCustomerContact.setAttribute("EmailID", sEmailId);
    }
    if(YFCCommon.equalsIgnoreCase(sEmailId, "")){
      eleCustomerContact.setAttribute(GCConstants.DAY_PHONE, sDayPhone);
    }
    YFCDocument getCustomerListTemp = YFCDocument
        .getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/getCustomerList_TemplateXML.xml"));

    YFCDocument getCustomerList =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_CUSTOMER_LIST, getCustomerListIp, getCustomerListTemp);

    return getCustomerList;
  }

  /**
   * This function checks whether Customer exists using EmailID & DayPhone from BillTo Address
   */
  public YFCDocument isCustomerPresent(YFSEnvironment env, String sEmailID, String sDayPhone) {

    LOGGER.verbose("GCManageMPOSCustomer - isCustomerPresent() inDoc is");

    YFCDocument getCustomerList = YFCDocument.getDocumentFor("<CustomerList/>");
    boolean customerPresent = false;

    if (!YFCCommon.isVoid(sEmailID)) {
      getCustomerList = getCustomerListEmail_DayPhone(env, sEmailID, "");
    }

    if (getCustomerList.getDocumentElement().hasChildNodes()) {
      return getCustomerList;
    } 
    if (!YFCCommon.isVoid(sDayPhone) && YFCCommon.isVoid(sEmailID)) //mPOS-1558 fix.
    {
      getCustomerList = getCustomerListEmail_DayPhone(env, "", sDayPhone);
      if (getCustomerList.getDocumentElement().hasChildNodes()) {
        return getCustomerList;
      }
    }

    return getCustomerList;
  }



  public YFCDocument customerNameUpdate(YFSEnvironment env, YFCDocument inDoc, YFCDocument customerListDoc)
  {
	LOGGER.verbose("GCManageMPOSCustomer.customerNameUpdate() start -->" + inDoc.toString());
    YFCElement eleDocRoot = inDoc.getDocumentElement();
    YFCElement elePersonInfo = eleDocRoot.getElementsByTagName(GCConstants.PERSON_INFO_BILL_TO).item(0);
    String sCustomerFirstName = elePersonInfo.getAttribute(GCConstants.FIRST_NAME);
    String sCustomerLastName = elePersonInfo.getAttribute(GCConstants.LAST_NAME);
    YFCElement eleOrderExtn = eleDocRoot.getElementsByTagName(GCConstants.EXTN).item(0);
    if(!YFCCommon.isVoid(customerListDoc)){

      LOGGER.verbose("getCustomerList document-->" + customerListDoc.toString());
      YFCElement eleCustomerListRoot = customerListDoc.getDocumentElement();
      YFCElement eleCustomer = eleCustomerListRoot.getChildElement(GCConstants.CUSTOMER);
      YFCElement eleCustomerExtn = eleCustomerListRoot.getElementsByTagName(GCConstants.EXTN).item(0);

      if(!YFCCommon.isVoid(eleCustomer))
      {
        String sCustomerID = eleCustomer.getAttribute(GCConstants.CUSTOMER_ID);
        eleDocRoot.setAttribute(GCConstants.BILL_TO_ID, sCustomerID);
        LOGGER.verbose("BillToID fetched-->" + sCustomerID);
        
        if(!YFCCommon.isVoid(eleCustomerExtn))
        {
        	String sExtnPOSCustomerID = eleCustomerExtn.getAttribute("ExtnPOSCustomerID");
        	LOGGER.verbose("ExtnPOSCustomerID fetched-->" + sExtnPOSCustomerID);
        	eleOrderExtn.setAttribute("ExtnPOSCustomerID", sExtnPOSCustomerID);
        	LOGGER.verbose("Setting transaction object to stamp ExtnPOSCustomerID on the customer if the object is empty");
        	env.setTxnObject("ExtnPOSCustomerIDFromManageCustomer", sExtnPOSCustomerID);
        	LOGGER.verbose("Class GCManageMPOSCustomer : Transaction object set as ExtnPOSCustomerID and the value is--> " + sExtnPOSCustomerID);
        }
        
      }
      
      if(IsCustomerPresent)
      {
     
      		if(!YFCCommon.isVoid(eleCustomerExtn))
      		{
      			String sSourceCustomerID = eleCustomerExtn.getAttribute(GCConstants.EXTN_SOURCE_CUSTOMER_ID);
      			LOGGER.verbose("ExtnSourceCustomerID fetched-->" + sSourceCustomerID);
      			eleOrderExtn.setAttribute(GCConstants.EXTN_SOURCE_CUSTOMER_ID, sSourceCustomerID);
      		}
      }

    }

    eleDocRoot.setAttribute("CustomerFirstName", sCustomerFirstName);
    eleDocRoot.setAttribute("CustomerLastName", sCustomerLastName);

    LOGGER.verbose("Document customerNameUpdate method returning--> " + inDoc.toString());
    return inDoc;

  }
  
  //MPOS-2345: This method is called for UPS address validation once the customer is imported: Start.
  public Document addressValidation(YFSEnvironment env, YFCDocument yfcInputDocManageCustomer, YFCDocument getCustomerList) throws GCException
  {
	  try
	  {
		  // This map contains all addresses present in Customer Details.
	        Map<String, YFCElement> uniqueaddressInGetCustomerList = new HashMap<String, YFCElement>();
	        Document manageCustomerOutDoc = null;
	        Document manageCustomerAPIinDoc = GCXMLUtil.getDocument(GCConstants.MANAGE_CUSTOMER_INPUT);
	        Element eleAPIinDocRoot = manageCustomerAPIinDoc.getDocumentElement();
	        Element eleAPIinDocCustomerContact = (Element) eleAPIinDocRoot.getElementsByTagName(GCConstants.CUSTOMER_CONTACT).item(0);
	        boolean bValidationFailed = false;
		      boolean bValidationDone = false;
	        if(!YFCCommon.isVoid(getCustomerList)){
		        YFCNodeList<YFCElement> nlCustomerAdditionalAddress =
		            getCustomerList.getElementsByTagName(GCConstants.CUSTOMER_ADDITIONAL_ADDRESS);
		        for (int x = 0; x < nlCustomerAdditionalAddress.getLength(); x++) {
		          YFCElement eleCustomerAdditionalAddress = nlCustomerAdditionalAddress.item(x);
		          YFCElement elePersonInfo = eleCustomerAdditionalAddress.getElementsByTagName(GCConstants.PERSON_INFO).item(0);
		          
		          String sAddressLine1 = "";
		          String sAddressLine2 = "";
		          String sAddressLine3 = "";
		          String sDayPhone ="";
		          
		          if(!YFCCommon.isVoid(elePersonInfo)){
		        	   sAddressLine1 = elePersonInfo.getAttribute(GCConstants.ADDRESS_LINE1);
		               sAddressLine2 = elePersonInfo.getAttribute(GCConstants.ADDRESS_LINE2);
		               sAddressLine3 = elePersonInfo.getAttribute(GCConstants.ADDRESS_LINE3);
		               sDayPhone = elePersonInfo.getAttribute(GCConstants.DAY_PHONE);
		          }
	
		          String sPresentAddress = sAddressLine1.toUpperCase().trim() + "|" + sAddressLine2.toUpperCase().trim() + "|"
		              + sAddressLine3.toUpperCase().trim() + "|" + sDayPhone;
		          uniqueaddressInGetCustomerList.put(sPresentAddress, eleCustomerAdditionalAddress);
		        }
	        }
	        
		  Document inDocManageCustomer = yfcInputDocManageCustomer.getDocument();
		  Element eleInDocCustomerAdditionalAddressList = (Element) inDocManageCustomer.getDocumentElement().getElementsByTagName("CustomerAdditionalAddressList").item(0);
		  if(eleInDocCustomerAdditionalAddressList.hasChildNodes())
		  {
			  NodeList nlInDocCustomerAdditionalAddress = eleInDocCustomerAdditionalAddressList.getElementsByTagName("CustomerAdditionalAddress");
			  //Looping over CuatomerAdditionalAddress for each address verification.
			  for(int i = 0; i < nlInDocCustomerAdditionalAddress.getLength(); i++)
			  {
				  bValidationFailed = false;
				  bValidationDone = false;
				  Element eleInDocCustomerAdditionalAddress = (Element) nlInDocCustomerAdditionalAddress.item(i);
				  Element eleInDocPersonInfo = (Element)eleInDocCustomerAdditionalAddress.getElementsByTagName(GCConstants.PERSON_INFO).item(0);
				  Document personInfoDoc = GCXMLUtil.getDocumentFromElement(eleInDocPersonInfo);
				  
				  YFCDocument yfcPersonInfoDoc = YFCDocument.getDocumentFor(personInfoDoc);
				  YFCElement yfcElePersonInfoRoot = yfcPersonInfoDoc.getDocumentElement();
				  String sAddressLine1 = yfcElePersonInfoRoot.getAttribute(GCConstants.ADDRESS_LINE1); 
				  String sAddressLine2 = yfcElePersonInfoRoot.getAttribute(GCConstants.ADDRESS_LINE2);
				  String sAddressLine3 = yfcElePersonInfoRoot.getAttribute(GCConstants.ADDRESS_LINE3);
			      String sDayPhone = yfcElePersonInfoRoot.getAttribute(GCConstants.DAY_PHONE);
			      
			      String sInDocPersonInfoAddress = sAddressLine1.toUpperCase().trim() + "|" + sAddressLine2.toUpperCase().trim() + "|"
			              + sAddressLine3.toUpperCase().trim() + "|" + sDayPhone;
			      
			      Document addressValidationOutDoc = null;
			   			      
			      if (uniqueaddressInGetCustomerList.containsKey(sInDocPersonInfoAddress))
			      {
			    	  LOGGER.verbose("Address is already present. No Validation needed for the address --> "  + yfcPersonInfoDoc.toString());
			    	  bValidationDone = false;
			      }
			      else
			      {
			    	  LOGGER.verbose("Validation needed for the address --> " + yfcPersonInfoDoc.toString());
			    	  String sCountry = "";
			    	  String sCheckAddressLine1 = "";
			    	  YFCElement eleyfcPersonInfoDocRoot = yfcPersonInfoDoc.getDocumentElement();
			    	  sCountry = eleyfcPersonInfoDocRoot.getAttribute(GCConstants.COUNTRY);
			    	  if(YFCCommon.isVoid(sCountry))
			    	  {
			    		  sCountry = "US";
			    		  eleyfcPersonInfoDocRoot.setAttribute(GCConstants.COUNTRY, sCountry);
			    	  }
			    	  sCheckAddressLine1 = eleyfcPersonInfoDocRoot.getAttribute(GCConstants.ADDRESS_LINE1);
			    	  if(!YFCCommon.isVoid(sCheckAddressLine1))
			    	  {
						  LOGGER.verbose("Input to addressValidation method to call GCUPSAddressValidationStreetLevelService --> " + GCXMLUtil.getXMLString(personInfoDoc));
						  addressValidationOutDoc = GCCommonUtil.invokeService(env, "GCUPSAddressValidationStreetLevelService", personInfoDoc);
						  LOGGER.verbose("Document received after address validation --> " + GCXMLUtil.getXMLString(addressValidationOutDoc));
						  bValidationDone = true;
			    	  }
			      	
			    	  if(bValidationDone){
					  YFCDocument yfcUPSPersonInfoList = YFCDocument.getDocumentFor(addressValidationOutDoc);
					  YFCElement eleUPSoutDocRoot = yfcUPSPersonInfoList.getDocumentElement();
					  String sAddresssIndicator = eleUPSoutDocRoot.getAttribute("AddressIndicator");
					  
					  if(YFCCommon.equalsIgnoreCase(sAddresssIndicator, "ValidAddressIndicator"))
					  {
						  LOGGER.verbose("Successful UPS response with verified address::");
						  
					  }
					  
					  else if(YFCCommon.equalsIgnoreCase(sAddresssIndicator, "AmbiguousAddressIndicator"))
					  {
						  LOGGER.verbose("Multiple suggestions received, consider address not validated.");
						  bValidationFailed = true;
					  }
					  
					  else if(YFCCommon.equalsIgnoreCase(sAddresssIndicator, "NoCandidatesIndicator"))
					  {
						  LOGGER.verbose("Failed AVSReturnCode: Couldn't verify the address::");
						  bValidationFailed = true;
					  }
					  
					  else if(YFCCommon.isVoid(sAddresssIndicator))
					  {
						  Element UPSPersonInfo = (Element) eleUPSoutDocRoot.getElementsByTagName(GCConstants.PERSON_INFO).item(0);
						  String sAVSReturnCode = UPSPersonInfo.getAttribute(GCConstants.AVS_RETURN_CODE);
						  if(YFCCommon.equalsIgnoreCase(sAVSReturnCode, GCConstants.AVS_DOWN))
						  {
							  LOGGER.verbose("AVS Down. UPS didn't give back the expected response. Cannot validate the address::");
							  bValidationFailed = true;
						  }
					  }
					 
			        String sCustomerID = "";
			        String sCustomerContactID = "";
			        if(!YFCCommon.isVoid(getCustomerList))
			        {
				        //Customer is already present. Fetch info needed to update the customer.
						YFCElement elegetCustomerListRoot = getCustomerList.getDocumentElement();
						LOGGER.verbose("Fetching and stamping customer contact ID");
						YFCElement eleCustomerContact = elegetCustomerListRoot.getElementsByTagName(GCConstants.CUSTOMER_CONTACT).item(0);
						sCustomerContactID = eleCustomerContact.getAttribute(GCConstants.CUSTOMER_CONTACT_ID);
						Element inputCustomerContact = (Element) eleAPIinDocRoot.getElementsByTagName(GCConstants.CUSTOMER_CONTACT).item(0);
						inputCustomerContact.setAttribute(GCConstants.CUSTOMER_CONTACT_ID, sCustomerContactID);
						
						LOGGER.verbose("Fetching and stamping customer ID\n");
						YFCElement eleCustomer = elegetCustomerListRoot.getElementsByTagName(GCConstants.CUSTOMER).item(0);
						sCustomerID = eleCustomer.getAttribute(GCConstants.CUSTOMER_ID);
						eleAPIinDocRoot.setAttribute(GCConstants.CUSTOMER_ID, sCustomerID);
			        }
					
					//Creating manage customer input 	
						if(!bValidationFailed)
						{
							Element eleAPIinDocCustomerAdditionalAddressList = (Element) eleAPIinDocRoot.getElementsByTagName(GCConstants.CUSTOMER_ADDITIONAL_ADDRESS_LIST).item(0);
							Element eleAPIinDocCustomerAdditionalAddress = GCXMLUtil.createElement(manageCustomerAPIinDoc, GCConstants.CUSTOMER_ADDITIONAL_ADDRESS, "");
							GCXMLUtil.copyElement(manageCustomerAPIinDoc, eleInDocCustomerAdditionalAddress, eleAPIinDocCustomerAdditionalAddress);
							eleAPIinDocCustomerAdditionalAddressList.appendChild(eleAPIinDocCustomerAdditionalAddress);
						}
						if(!eleAPIinDocCustomerContact.hasAttributes())
						{
							Element eleInDocCustomerContact = (Element) inDocManageCustomer.getElementsByTagName(GCConstants.CUSTOMER_CONTACT).item(0);
							GCXMLUtil.copyAttributes(eleInDocCustomerContact, eleAPIinDocCustomerContact);
						}
			    	  }
			      }
		  }
			  if(!bValidationFailed || bValidationDone)
			    {
					Document getCustomerListTemp = GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_manageCustomer.xml");
					LOGGER.verbose("Input to manageCustomerAPI:: -->"+ GCXMLUtil.getXMLString(manageCustomerAPIinDoc));
					Element eleCustomerAdditionalAddressCheck = (Element) manageCustomerAPIinDoc.getElementsByTagName(GCConstants.CUSTOMER_ADDITIONAL_ADDRESS).item(0);
					if(!YFCCommon.isVoid(eleCustomerAdditionalAddressCheck))
					{
						manageCustomerOutDoc = GCCommonUtil.invokeAPI(env, GCConstants.MANAGE_CUSTOMER_API, manageCustomerAPIinDoc, getCustomerListTemp);
						LOGGER.verbose("After manageCustomer API output is-->" + GCXMLUtil.getXMLString(manageCustomerOutDoc));
					}
					else if(YFCCommon.isVoid(getCustomerList))
					{
						LOGGER.verbose("Customer is new with no valid address. Create the Customer");
						Element eleCustomerAdditionalAddressPrepared = manageCustomerAPIinDoc.createElement(GCConstants.CUSTOMER_ADDITIONAL_ADDRESS);
						eleAPIinDocRoot.getElementsByTagName(GCConstants.CUSTOMER_ADDITIONAL_ADDRESS_LIST).item(0).appendChild(eleCustomerAdditionalAddressPrepared);
						Element elePersonInfoPrepared = manageCustomerAPIinDoc.createElement(GCConstants.PERSON_INFO);
						eleAPIinDocRoot.getElementsByTagName(GCConstants.CUSTOMER_ADDITIONAL_ADDRESS).item(0).appendChild(elePersonInfoPrepared);
						LOGGER.verbose("Input to manageCustomer when new customer imported with no address--> " + GCXMLUtil.getXMLString(manageCustomerAPIinDoc));
						if(!eleAPIinDocCustomerContact.hasAttributes())
						{
							Element eleInDocCustomerContact = (Element) inDocManageCustomer.getElementsByTagName(GCConstants.CUSTOMER_CONTACT).item(0);
							GCXMLUtil.copyAttributes(eleInDocCustomerContact, eleAPIinDocCustomerContact);
						}
						manageCustomerOutDoc = GCCommonUtil.invokeAPI(env, GCConstants.MANAGE_CUSTOMER_API, manageCustomerAPIinDoc, getCustomerListTemp);
						LOGGER.verbose("After manageCustomer API, new customer created as ->" + GCXMLUtil.getXMLString(manageCustomerOutDoc));
					}

			    }
			   
		}
		  return manageCustomerOutDoc;
	  }catch (Exception e) {
		    LOGGER.error(
				    " Inside GCManageMPOSCustomer catch block of addressValidation, Exception is :",
				    e);
			    throw new GCException(e);
			}
	  
  }
  //MPOS-2345: This method is called for UPS address validation once the customer is imported: End.

}

