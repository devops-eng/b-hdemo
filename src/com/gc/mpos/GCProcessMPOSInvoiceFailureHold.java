package com.gc.mpos;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCProcessMPOSInvoiceFailureHold {
	
	 private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCProcessMPOSInvoiceFailureHold.class.getName());

	  public void processMPOSInvoiceFailureHold (YFSEnvironment env, Document inDoc) throws GCException {

	    LOGGER.beginTimer("GCProcessMPOSInvoiceFailureHold.processMPOSInvoiceFailureHold()");
	    LOGGER.verbose("GCProcessMPOSInvoiceFailureHold - processMPOSInvoiceFailureHold() : Start");	    
	    
		YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
		if (LOGGER.isDebugEnabled())
		{
		      LOGGER.verbose("Input doc to GCProcessMPOSInvoiceFailureHold.processMPOSInvoiceFailureHold method is:--> "
		          + yfcInDoc.toString());
		}
		
		env.setTxnObject("IsHoldAppiled", "True");
	    
		YFCElement rootEle = yfcInDoc.getDocumentElement();
		String sHoldType = rootEle.getAttribute(GCConstants.HOLD_TYPE);
		String sStatus = rootEle.getAttribute(GCConstants.STATUS);
		
		LOGGER.verbose("HoldType is :"  + sHoldType);
	    LOGGER.verbose("HoldStatus is :" + sStatus);
	    
	    if (YFCCommon.equalsIgnoreCase(sHoldType, "MPOS_INVCE_FAIL_HOLD") && YFCCommon.equalsIgnoreCase(sStatus, "1300"))
	    {
	    	String sOrderHeaderKey = rootEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
	    	
	    	YFCDocument getOrderInvoiceListIn =
	    	          YFCDocument.getDocumentFor("<OrderInvoice OrderHeaderKey='" + sOrderHeaderKey + "'/>");
	    	
	    	YFCDocument getOrderInvoiceListOut =
	    	          GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_INVOICE_LIST, getOrderInvoiceListIn, null);
	    	
	    	YFCElement eleInvoiceList = getOrderInvoiceListOut.getDocumentElement();
			YFCNodeList<YFCElement> nlOrderInvoice = eleInvoiceList.getElementsByTagName(GCConstants.ORDER_INVOICE);
			for (YFCElement eleOrderInvoice : nlOrderInvoice) 
			{
				String sOrderInvoiceKey = eleOrderInvoice.getAttribute(GCConstants.ORDER_INVOICE_KEY);
       
				YFCDocument getOrderInvoiceDetailsIn =
            YFCDocument.getDocumentFor("<GetOrderInvoiceDetails InvoiceKey='" + sOrderInvoiceKey + "'/>");
       
				YFCDocument getOrderInvoiceDetailTemp =
           YFCDocument.getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/event/SEND_INVOICE.PUBLISH_INVOICE_DETAIL.xml"));
				YFCDocument getOrderInvoiceDetailsOut =
            GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_INVOICE_DETAILS, getOrderInvoiceDetailsIn, getOrderInvoiceDetailTemp);
				
	    	
	    	YFCElement rootOrderInvoiceList = getOrderInvoiceDetailsOut.getDocumentElement();
	    	YFCElement eleOrder = rootOrderInvoiceList.getElementsByTagName(GCConstants.ORDER).item(0);
	    	YFCElement eleOrderLines = eleOrder.getElementsByTagName(GCConstants.ORDER_LINES).item(0);
	    	String sDeliveryMethod = eleOrderLines.getChildElement(GCConstants.ORDER_LINE).getAttribute(GCConstants.DELIVERY_METHOD);

	    	if(YFCCommon.equalsIgnoreCase(sDeliveryMethod, "CARRY"))
	    	{
	    		YFCElement eleInvoiceHeader = rootOrderInvoiceList.getElementsByTagName(GCConstants.INVOICE_HEADER).item(0);
	    		
	    		
	    		LOGGER.verbose("Doc for GCPublishMPOSInvoiceToPOS service-->" + getOrderInvoiceDetailsOut.toString());
	    		GCCommonUtil.invokeService(env, "GCPublishMPOSInvoiceToPOS", getOrderInvoiceDetailsOut);
	    		
	    	}
			
		}
	    
	  }
	 
  }
	  
}
