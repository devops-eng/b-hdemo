/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 * *###########################################
 * ######################################################
 * ################################################################ OBJECTIVE: This class will
 * prepare input to publish MPOS invoice to POS.
 * ########################################################
 * ##########################################
 * ############################################################### Version Date Modified By
 * Description
 * ######################################################################################
 * ########################################################################### 1.0 11/30/2016 Thapa,
 * Sweta MPOS Invoicing
 * ###############################################################################
 * ##################################################################################
 */
package com.gc.mpos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.gc.api.invoicing.GCPOSInvoicing;
import com.gc.api.returns.GCPublishReturnInvoice;
import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class GCPublishMPOSOrderInvoice {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCPublishMPOSOrderInvoice.class.getName());

  Document docGetOrderListOutput = null;
  boolean bAnyBundleLineExists = true;
  List<Element> bundleParentLinesList = new ArrayList<Element>();
  GCPOSInvoicing oPOSInvoice = new GCPOSInvoicing();
  //MPOS-2382 and 2410: Getting the orderline removed after clubbing: Starts.
  Document removedLineDetailsDoc = GCXMLUtil.createDocument(GCConstants.LINE_DETAILS);
  Element removedEleLineDetails = removedLineDetailsDoc.getDocumentElement();
  //MPOS-2382 and 2410: Getting the orderline removed after clubbing: Ends.

  /**
   * This method will prepare POS request and call POS to get POS sales ticket number
   *
   * @param env
   * @param inDoc
   * @return
   * @throws GCException
   * @throws TransformerException
   * @throws Exception
   */
  public void createAndPublishPOSInvoice(YFSEnvironment env, Document inDoc) throws GCException, TransformerException {
    LOGGER.beginTimer("GCPublishMPOSOrderInvoice.createAndPublishPOSInvoice()");
    LOGGER.verbose("GCPublishMPOSOrderInvoice - createAndPublishPOSInvoice() : Start");

    Element eleRoot = inDoc.getDocumentElement();
    YFCDocument publishMPOSInvoice = YFCDocument.getDocumentFor(inDoc);
    YFCElement yfcEleRootInDoc = publishMPOSInvoice.getDocumentElement();
    YFCElement yfcEleInvoiceHeader = yfcEleRootInDoc.getElementsByTagName(GCConstants.INVOICE_HEADER).item(0);
    
    // Stamp Order Date & Time
    getAndStampDates(publishMPOSInvoice.getDocumentElement());

    //MPOS-2365: getting parent line details: Start.
    bundleParentLinesList = GCXMLUtil.getElementListByXpath(inDoc,
            "InvoiceDetail/InvoiceHeader/LineDetails/LineDetail/OrderLine[@IsBundleParent='Y']/..");
   //MPOS-2365: getting parent line details: End.
    
    // stamp manager approval id
    getManagerCashierApproveId(yfcEleRootInDoc, yfcEleInvoiceHeader);

    // pro-rate bundle line price , Charges & taxes
    Map<String, String> mapBundleParentComponent = new HashMap<String, String>();
    oPOSInvoice.prorateBundleCharges(publishMPOSInvoice.getDocument(), mapBundleParentComponent);
    oPOSInvoice.handleZeroTaxPercentage(YFCDocument.getDocumentFor(publishMPOSInvoice.getDocument()));
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("InDoc After bundle Proration to components: " + publishMPOSInvoice.toString());
    }

    // CLub OrderLines with same POSSKUNumber
    LOGGER.verbose("Any bundle line exsits " + bAnyBundleLineExists);
    if (bAnyBundleLineExists) {
      oPOSInvoice.clubOrderLinesWithSimilarComps(publishMPOSInvoice.getDocument(), mapBundleParentComponent, removedLineDetailsDoc, removedEleLineDetails);
      
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("InDoc after combining same components: " + publishMPOSInvoice.toString());
      }
    }

    // Stamp latest Override Note
    oPOSInvoice.stampLatestPriceOverrideNote(publishMPOSInvoice);

    // Stamp MessageType = mSALE at header level
    publishMPOSInvoice.getDocumentElement().setAttribute("MessageType", "mSALE");

    // Call GCPrepareMSALERequestService to get required XML
    Document docToPOS =
        GCCommonUtil.invokeService(env, "GCPrepareMSALERequestService", publishMPOSInvoice.getDocument());
    YFCDocument xslDocRequestToPOS = YFCDocument.getDocumentFor(docToPOS);

    // Of PaymentType = GC || PLCC --> Encrypt / Decrypt Payment Info
    boolean isEncryptDecryptNeeded = false;
    YFCNodeList<YFCElement> nlPaymentMethod = publishMPOSInvoice.getElementsByTagName(GCConstants.PAYMENT_METHOD);
    for (int j = 0; j < nlPaymentMethod.getLength(); j++) {
      YFCElement elePaymentMethod = nlPaymentMethod.item(j);
      String sPaymentType = elePaymentMethod.getAttribute(GCConstants.PAYMENT_TYPE);
      if (!YFCCommon.isVoid(sPaymentType)) {
        isEncryptDecryptNeeded = true;
      }
    }
    if (isEncryptDecryptNeeded) {
    	GCPublishReturnInvoice.encryptDecryptCCForPOS(xslDocRequestToPOS);
    }

    // Invoke Web Service
    YFCDocument yfcDocInvokeWebServiceOutput = null;

    try {
      yfcDocInvokeWebServiceOutput = GCCommonUtil.invokeService(env, "GCInvokeMPOSInvoiceWebService", xslDocRequestToPOS);
    }

    catch (YFSException yfsException) {
      LOGGER.error("Inside catch block of GCPublishMPOSOrderInvoice.createAndPublishPOSInvoice(): ", yfsException);
      eleRoot.setAttribute("POSError", GCErrorConstants.WEB_SERVICE_POS_INVOICE_CALL_TIMEOUT_CODE);
      if (YFCCommon.equals(env.getTxnObject("IsHoldAppiled"), "True")) {
        YFCException yfcexcep = new YFCException();
        yfcexcep.setAttribute(GCConstants.ERROR_CODE, GCErrorConstants.WEB_SERVICE_POS_INVOICE_CALL_TIMEOUT_CODE);
        yfcexcep.setAttribute(GCConstants.ERROR_DESCRIPTION, "Could not connect to POS. Timeout");
        throw yfcexcep;
      } else {
        YFCElement eleInvoiceOrder = yfcEleInvoiceHeader.getElementsByTagName(GCConstants.ORDER).item(0);
        String sOrderHeaderKey = eleInvoiceOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
        // Applying hold on order
        applyMPOSInvoiceFailureHold(env, sOrderHeaderKey);
        // Raising alert on hold
        GCCommonUtil.invokeService(env, "GCPOSConnectFailedMPOSAlertService", inDoc);
      }

    }

    processWebServiceResponse(env, inDoc, yfcEleInvoiceHeader, yfcDocInvokeWebServiceOutput);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("yfcDocInvokeWebServiceOutput : " + yfcDocInvokeWebServiceOutput);
    }

    LOGGER.endTimer("GCPublishMPOSOrderInvoice.createAndPublishPOSInvoice()");
    LOGGER.verbose("GCPublishMPOSOrderInvoice - createAndPublishPOSInvoice() : End");

  }


  /**
   * Method to get and stamp Order and current system date & time
   *
   * @param yfcEleRootInDoc
   * @return
   */
  public static YFCElement getAndStampDates(YFCElement yfcEleRootInDoc) {
    LOGGER.beginTimer("GCPublishMPOSOrderInvoice.getAndStampDates()");
    LOGGER.verbose("GCPublishMPOSOrderInvoice - getAndStampDates() : Start");

    YFCElement yfcEleOrder = yfcEleRootInDoc.getElementsByTagName(GCConstants.ORDER).item(0);
    Date dOrderDateTime = yfcEleOrder.getDateAttribute(GCConstants.ORDER_DATE);
    GCDateUtils.getDateTimeForPOS(yfcEleRootInDoc, dOrderDateTime);

    LOGGER.endTimer("GCPublishMPOSOrderInvoice.getAndStampDates()");
    LOGGER.verbose("GCPublishMPOSOrderInvoice - getAndStampDates() : End");

    return yfcEleRootInDoc;
  }



  
  /**
   * This method will invoke change order API to update POS sales ticket number and pro-rated
   * component price on order line
   *
   * @param env
   * @param yfcEleInvoiceHeader
   * @param sPOSSalesTicketNo
   */
  private void invokeChangeOrder(YFSEnvironment env, YFCElement yfcEleInvoiceHeader,YFCDocument changeOrderInvoice, String sPOSSalesTicketNo, String sPOSBillcustID) {
    LOGGER.beginTimer("GCPublishMPOSOrderInvoice.invokeChangeOrder()");
    LOGGER.verbose("GCPublishMPOSOrderInvoice - invokeChangeOrder() : Start");
    YFCElement yfcEleLineDetails = yfcEleInvoiceHeader.getElementsByTagName(GCConstants.LINE_DETAILS).item(0);
    YFCElement yfcEleOrder = yfcEleInvoiceHeader.getElementsByTagName(GCConstants.ORDER).item(0);
  //mPOS-1948: to stamp POSCustomerID on order.
    YFCElement yfcEleOrderExtn = yfcEleOrder.getElementsByTagName(GCConstants.EXTN).item(0);
    yfcEleOrderExtn.setAttribute("ExtnPOSCustomerID", sPOSBillcustID);
    changeOrderInvoice.getDocumentElement().setAttribute(GCConstants.STATUS, "01");
  
    String sOrderHeaderkey = yfcEleOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);
    

    GCCommonUtil.invokeAPI(env, "changeOrderInvoice", changeOrderInvoice.getDocument(), null);

    YFCDocument yfcDocChangeOrder = YFCDocument.createDocument(GCConstants.ORDER);
    yfcDocChangeOrder.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderkey);
    yfcDocChangeOrder.getDocumentElement().setAttribute("SelectMethod", "WAIT");
    yfcDocChangeOrder.getDocumentElement().setAttribute(GCConstants.OVERRIDE, GCConstants.YES);
    YFCElement yfcEleChangeOrderExtn = yfcDocChangeOrder.importNode(yfcEleOrderExtn,false);
    yfcDocChangeOrder.getDocumentElement().appendChild(yfcEleChangeOrderExtn);
    
    YFCElement yfcEleChangeOrderLines = yfcDocChangeOrder.createElement(GCConstants.ORDER_LINES);
    yfcDocChangeOrder.getDocumentElement().appendChild(yfcEleChangeOrderLines);

    YFCNodeList<YFCElement> yfcNLLineDetails = yfcEleLineDetails.getElementsByTagName(GCConstants.LINE_DETAIL);
    for (int i = 0; i < yfcNLLineDetails.getLength(); i++) {
      YFCElement yfcEleLineDetail = yfcNLLineDetails.item(i);
      YFCElement yfcEleChangeOrderLine = yfcDocChangeOrder.createElement(GCConstants.ORDER_LINE);
      yfcEleChangeOrderLines.appendChild(yfcEleChangeOrderLine);

      String sOrderLineKey = yfcEleLineDetail.getAttribute(GCConstants.ORDER_LINE_KEY);
      String sItemID = yfcEleLineDetail.getAttribute(GCConstants.ITEM_ID);
      yfcEleChangeOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, sOrderLineKey);

      YFCElement yfcEleChangeOrderLineExtn = yfcDocChangeOrder.createElement(GCConstants.EXTN);
      yfcEleChangeOrderLine.appendChild(yfcEleChangeOrderLineExtn);
      yfcEleChangeOrderLineExtn.setAttribute(GCXmlLiterals.EXTN_POS_SALES_TICKET_NO, sPOSSalesTicketNo);
      
      
   // MPOS-2382 and 2410: Stamping POSSalesTicket on removed orderline: Starts.
      NodeList removedLineDetails = removedEleLineDetails.getElementsByTagName(GCConstants.LINE_DETAIL);
      if (removedLineDetails.getLength() > 0) {
        LOGGER.debug("removedLineDetailsDoc is " + GCXMLUtil.getXMLString(removedLineDetailsDoc));
        int nlLength = removedLineDetails.getLength();
        for (int j = 0; j < nlLength; j++) {
          LOGGER.debug("Entering 2382 and 2410 fix");
          Element removedLineDetailEle = (Element) removedLineDetails.item(j);
          LOGGER.verbose("Removed orderine fetched at index [" + j +"] is -->" + GCXMLUtil.getElementString(removedLineDetailEle));
          String sOrderLineKeyOfRemovedLine = removedLineDetailEle.getAttribute(GCConstants.ORDER_LINE_KEY);

          YFCElement yfcEleChangeOrderLineForRemovedLine = yfcDocChangeOrder.createElement(GCConstants.ORDER_LINE);
          yfcEleChangeOrderLineForRemovedLine.setAttribute(GCConstants.ORDER_LINE_KEY, sOrderLineKeyOfRemovedLine);

          YFCElement yfcEleChangeOrderLineExtnForRemLine = yfcDocChangeOrder.createElement(GCConstants.EXTN);
          yfcEleChangeOrderLineExtnForRemLine.setAttribute(GCXmlLiterals.EXTN_POS_SALES_TICKET_NO, sPOSSalesTicketNo);
          yfcEleChangeOrderLineExtnForRemLine.setAttribute(GCXmlLiterals.EXTN_IS_STORE_FULFILLED, GCConstants.YES);

          yfcEleChangeOrderLineForRemovedLine.appendChild(yfcEleChangeOrderLineExtnForRemLine);
          yfcEleChangeOrderLines.appendChild(yfcEleChangeOrderLineForRemovedLine);
          
          removedEleLineDetails.removeChild(removedLineDetailEle);
          nlLength--;
          j--;
        }
      }
      // MPOS-2382 and 2410: Stamping POSSalesTicket on removed orderline: Ends.
      
      
      // GCSTORE-516 - adding flag
      yfcEleChangeOrderLineExtn.setAttribute(GCXmlLiterals.EXTN_IS_STORE_FULFILLED, GCConstants.YES);
      // GCSTORE-516 - adding flag

      String sBundleParentLineKey = (yfcEleLineDetail.getElementsByTagName(GCConstants.ORDER_LINE).item(0))
          .getAttribute(GCConstants.BUNDLE_PARENT_ORDERLINEKEY);

      if (bAnyBundleLineExists && !YFCCommon.isVoid(sBundleParentLineKey)) {
        YFCElement yfcEleComponentPriceList =
            yfcDocChangeOrder.createElement(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS_LIST);
        yfcEleChangeOrderLineExtn.appendChild(yfcEleComponentPriceList);

        // Fix for 5021 : Making all amount to zero on GCOrderLineComponents for secondary
        // components
        Set<String> lOrderLineCompToBePersisted = new HashSet<String>();
        // Add unit price as an individual entry

        String sUnitPrice = yfcEleLineDetail.getAttribute(GCConstants.UNIT_PRICE);
        String sQuantity = yfcEleLineDetail.getAttribute(GCConstants.SHIPPED_QUANTITY);

        // Check if GCOrderLineComp with ChargeCategory=Price exist. If yes update the same, if no
        // create new.
        YFCNodeList<YFCElement> nlGCOrderLineComponents =
            yfcEleLineDetail.getElementsByTagName(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
        YFCElement yfceleGCOrderLineUnitPriceComponent = null;
        for (int j = 0; j < nlGCOrderLineComponents.getLength(); j++) {
          yfceleGCOrderLineUnitPriceComponent = nlGCOrderLineComponents.item(j);
          String sChargeCategory = yfceleGCOrderLineUnitPriceComponent.getAttribute(GCConstants.CHARGE_CATEGORY);
          String sChargeName = yfceleGCOrderLineUnitPriceComponent.getAttribute(GCConstants.CHARGE_NAME);
          if (YFCCommon.equals(sChargeCategory, GCConstants.UNIT_PRICE)
              && YFCCommon.equals(sChargeName, GCConstants.UNIT_PRICE)) {
            break;
          }
        }

        if (YFCCommon.isVoid(yfceleGCOrderLineUnitPriceComponent)) {
          LOGGER.verbose("Component with UnitPrice ChargeCategory does not exist. Create new");
          YFCElement yfcEleComponentUnitPrice = yfcDocChangeOrder.createElement(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
          yfcEleComponentPriceList.appendChild(yfcEleComponentUnitPrice);
          yfcEleComponentUnitPrice.setAttribute(GCConstants.CHARGE_CATEGORY, GCConstants.UNIT_PRICE);
          yfcEleComponentUnitPrice.setAttribute(GCConstants.CHARGE_NAME, GCConstants.UNIT_PRICE);
          yfcEleComponentUnitPrice.setAttribute(GCXmlLiterals.AMOUNT, sUnitPrice);
          yfcEleComponentUnitPrice.setAttribute(GCConstants.ITEM_ID, sItemID);
          yfcEleComponentUnitPrice.setAttribute(GCConstants.QUANTITY, sQuantity);
        } else {
          LOGGER.verbose("Component with Unit Price ChargeCategory exists. Update the same, stamping updated amount");
          YFCElement yfcEleComponentUnitPrice = yfcDocChangeOrder.createElement(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
          yfcEleComponentPriceList.appendChild(yfcEleComponentUnitPrice);
          yfcEleComponentUnitPrice.setAttribute("OrderLineComponentKey",
              yfceleGCOrderLineUnitPriceComponent.getAttribute("OrderLineComponentKey"));
          yfcEleComponentUnitPrice.setAttribute(GCXmlLiterals.AMOUNT, sUnitPrice);
          lOrderLineCompToBePersisted.add(yfceleGCOrderLineUnitPriceComponent.getAttribute("OrderLineComponentKey"));
        }


        // Fetch BundlePrice and stamp on GCOrderLineComponents

        YFCElement yfcEleBundlePriceComponent = yfcDocChangeOrder.createElement(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
        yfcEleComponentPriceList.appendChild(yfcEleBundlePriceComponent);
        yfcEleBundlePriceComponent.setAttribute(GCConstants.CHARGE_CATEGORY, "BundlePrice");
        String sBundleUnitPrice = yfcEleLineDetail.getAttribute("BundleParentUnitPrice");
        yfcEleBundlePriceComponent.setAttribute(GCXmlLiterals.AMOUNT, sBundleUnitPrice);
        yfcEleBundlePriceComponent.setAttribute(GCConstants.QUANTITY, sQuantity);

        // Loop over line charges
        YFCNodeList<YFCElement> yfcNLLineCharge = yfcEleLineDetail.getElementsByTagName(GCConstants.LINE_CHARGE);
        for (int j = 0; j < yfcNLLineCharge.getLength(); j++) {
          YFCElement yfcEleLineCharge = yfcNLLineCharge.item(j);
          String sChargeCategory = yfcEleLineCharge.getAttribute(GCConstants.CHARGE_CATEGORY);
          String sChargeName = yfcEleLineCharge.getAttribute(GCConstants.CHARGE_NAME);
          String sIsDiscount = yfcEleLineCharge.getAttribute(GCXmlLiterals.IS_DISCOUNT);
          String sChargeAmount = yfcEleLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT);

          // Check if GCOrderLineComp with current looped ChargeCategory exist. If yes update the
          // same, if no create new.
          YFCElement yfceleGCOrderLineLineChargeComponent = null;
          for (int k = 0; k < nlGCOrderLineComponents.getLength(); k++) {
            yfceleGCOrderLineLineChargeComponent = nlGCOrderLineComponents.item(k);
            String sCompChargeCategory = yfceleGCOrderLineLineChargeComponent.getAttribute(GCConstants.CHARGE_CATEGORY);
            String sCompChargeName = yfceleGCOrderLineLineChargeComponent.getAttribute(GCConstants.CHARGE_NAME);
            if (YFCCommon.equals(sCompChargeCategory, sChargeCategory)
                && YFCCommon.equals(sCompChargeName, sChargeName)) {
              break;
            }
          }

          if (YFCCommon.isVoid(yfceleGCOrderLineLineChargeComponent)) {
            LOGGER.verbose("Component with " + sChargeCategory + " ChargeCategory does not exist. Create new");
            YFCElement yfcEleComponentPrice = yfcDocChangeOrder.createElement(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
            yfcEleComponentPriceList.appendChild(yfcEleComponentPrice);

            yfcEleComponentPrice.setAttribute(GCConstants.CHARGE_CATEGORY, sChargeCategory);
            yfcEleComponentPrice.setAttribute(GCConstants.CHARGE_NAME, sChargeName);
            yfcEleComponentPrice.setAttribute(GCXmlLiterals.IS_DISCOUNT, sIsDiscount);
            yfcEleComponentPrice.setAttribute(GCXmlLiterals.AMOUNT, sChargeAmount);
            yfcEleComponentPrice.setAttribute(GCConstants.ITEM_ID, sItemID);
            yfcEleComponentPrice.setAttribute(GCConstants.QUANTITY, sQuantity);
          } else {
            LOGGER.verbose("Component with " + sChargeCategory
                + "ChargeCategory exists. Update the same, stamping updated amount");
            YFCElement yfcEleComponentUnitPrice =
                yfcDocChangeOrder.createElement(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
            yfcEleComponentPriceList.appendChild(yfcEleComponentUnitPrice);
            yfcEleComponentUnitPrice.setAttribute("OrderLineComponentKey",
                yfceleGCOrderLineLineChargeComponent.getAttribute("OrderLineComponentKey"));
            yfcEleComponentUnitPrice.setAttribute(GCXmlLiterals.AMOUNT, sChargeAmount);
            lOrderLineCompToBePersisted.add(yfceleGCOrderLineLineChargeComponent.getAttribute("OrderLineComponentKey"));
          }

        }

        // Loop over line Taxes
        YFCNodeList<YFCElement> yfcNLLineTax = yfcEleLineDetail.getElementsByTagName(GCConstants.LINE_TAX);
        for (int k = 0; k < yfcNLLineTax.getLength(); k++) {
          YFCElement yfcEleLineTax = yfcNLLineTax.item(k);
          String sChargeCategory = yfcEleLineTax.getAttribute(GCConstants.CHARGE_CATEGORY);
          String sChargeName = yfcEleLineTax.getAttribute(GCConstants.CHARGE_NAME);
          if (YFCCommon.equals(sChargeName, GCConstants.SHIPPING_CHARGE)) {
            sChargeName = yfcEleLineTax.getAttribute(GCConstants.TAX_NAME);
          }
          String sIsDiscount = GCConstants.BLANK;
          String sChargeAmount = yfcEleLineTax.getAttribute(GCConstants.TAX);

          if (YFCCommon.equals(GCConstants.SHIPPING_PROMOTION, sChargeCategory)
              || YFCCommon.equals(GCConstants.PROMOTION, sChargeCategory)) {
            sIsDiscount = GCConstants.YES;
          }

          YFCElement yfceleGCOrderLineLineTaxComponent = null;
          for (int l = 0; l < nlGCOrderLineComponents.getLength(); l++) {
            yfceleGCOrderLineLineTaxComponent = nlGCOrderLineComponents.item(l);
            String sCompChargeCategory = yfceleGCOrderLineLineTaxComponent.getAttribute(GCConstants.CHARGE_CATEGORY);
            String sCompChargeName = yfceleGCOrderLineLineTaxComponent.getAttribute(GCConstants.CHARGE_NAME);
            if (YFCCommon.equals(sCompChargeCategory, sChargeCategory)
                && YFCCommon.equals(sCompChargeName, sChargeName)) {
              break;
            }
          }

          if (YFCCommon.isVoid(yfceleGCOrderLineLineTaxComponent)) {
            LOGGER.verbose("Component with " + sChargeCategory + " ChargeCategory does not exist. Create new");
            YFCElement yfcEleComponentPrice = yfcDocChangeOrder.createElement(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
            yfcEleComponentPriceList.appendChild(yfcEleComponentPrice);
            yfcEleComponentPrice.setAttribute(GCConstants.CHARGE_CATEGORY, sChargeCategory);
            yfcEleComponentPrice.setAttribute(GCConstants.CHARGE_NAME, sChargeName);
            yfcEleComponentPrice.setAttribute(GCXmlLiterals.IS_DISCOUNT, sIsDiscount);
            yfcEleComponentPrice.setAttribute(GCXmlLiterals.AMOUNT, sChargeAmount);
            yfcEleComponentPrice.setAttribute(GCConstants.ITEM_ID, sItemID);
            yfcEleComponentPrice.setAttribute(GCConstants.QUANTITY, sQuantity);
          } else {
            LOGGER.verbose("Component with " + sChargeCategory
                + "ChargeCategory exists. Update the same, stamping updated amount");
            YFCElement yfcEleComponentUnitPrice =
                yfcDocChangeOrder.createElement(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
            yfcEleComponentPriceList.appendChild(yfcEleComponentUnitPrice);
            yfcEleComponentUnitPrice.setAttribute("OrderLineComponentKey",
                yfceleGCOrderLineLineTaxComponent.getAttribute("OrderLineComponentKey"));
            yfcEleComponentUnitPrice.setAttribute(GCXmlLiterals.AMOUNT, sChargeAmount);
            lOrderLineCompToBePersisted.add(yfceleGCOrderLineLineTaxComponent.getAttribute("OrderLineComponentKey"));
          }
        }

        // Set Amount=0.0 on OrderLineComponents for which no charge category exists.

        YFCNodeList<YFCElement> nlAllGCOrderLineComponentsOnLine =
            yfcEleLineDetail.getElementsByTagName(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
        for (int j = 0; j < nlAllGCOrderLineComponentsOnLine.getLength(); j++) {
          YFCElement yfceleGCOrderLineComponent = nlAllGCOrderLineComponentsOnLine.item(j);
          String sGCOrderLineComponentsKey = yfceleGCOrderLineComponent.getAttribute("OrderLineComponentKey");
          if (!YFCCommon.isVoid(sGCOrderLineComponentsKey)
              && !lOrderLineCompToBePersisted.contains(sGCOrderLineComponentsKey)) {
            YFCElement yfceleTempGCOrderLineComponent =
                yfcDocChangeOrder.createElement(GCXmlLiterals.GC_ORDER_LINE_COMPONENTS);
            yfcEleComponentPriceList.appendChild(yfceleTempGCOrderLineComponent);
            yfceleTempGCOrderLineComponent.setAttribute(GCXmlLiterals.AMOUNT, "0.00");
            yfceleTempGCOrderLineComponent.setAttribute("OrderLineComponentKey",
                yfceleGCOrderLineComponent.getAttribute("OrderLineComponentKey"));
          }
        }

      }
    }

    for (int i = 0; i < bundleParentLinesList.size(); i++) {
      Element yfcEleLineDetail = bundleParentLinesList.get(i);
      YFCElement yfcEleChangeOrderLine = yfcDocChangeOrder.createElement(GCConstants.ORDER_LINE);
      yfcEleChangeOrderLines.appendChild(yfcEleChangeOrderLine);
      String sOrderLineKey = yfcEleLineDetail.getAttribute(GCConstants.ORDER_LINE_KEY);
      yfcEleChangeOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, sOrderLineKey);
      YFCElement yfcEleChangeOrderLineExtn = yfcDocChangeOrder.createElement(GCConstants.EXTN);
      yfcEleChangeOrderLine.appendChild(yfcEleChangeOrderLineExtn);
      yfcEleChangeOrderLineExtn.setAttribute(GCXmlLiterals.EXTN_POS_SALES_TICKET_NO, sPOSSalesTicketNo);
      yfcEleChangeOrderLineExtn.setAttribute(GCXmlLiterals.EXTN_IS_STORE_FULFILLED, GCConstants.YES);
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Change Order input XML :" + yfcDocChangeOrder);
    }
    

    GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, yfcDocChangeOrder.getDocument());
    
    //mPOS-1948: To persist ExtnPOSCustomerID received in POS response: Start.
    String sCustomerID = yfcEleOrder.getAttribute(GCConstants.BILL_TO_ID);    
    	
    //MPOS-2416: Start.
    String sTxnExtnPOSCustomerID = "";
    if(!YFCCommon.isVoid(env.getTxnObject("ExtnPOSCustomerIDFromCreateInvoice")))
    {
    	sTxnExtnPOSCustomerID = env.getTxnObject("ExtnPOSCustomerIDFromCreateInvoice").toString();
	}
	LOGGER.verbose("Class GCMPOSConfirmDraftOrder : Transaction object for ExtnPOSCustomerID fetched --> " + sTxnExtnPOSCustomerID);
	//MPOS-2416: End.
		
	
		if(YFCCommon.isVoid(sTxnExtnPOSCustomerID) && !YFCCommon.isVoid(sCustomerID))
		{
	    	YFCDocument yfcManageCustomerInDoc = YFCDocument.createDocument(GCConstants.CUSTOMER);
	    	YFCElement yfcManageCustomerInDocRoot = yfcManageCustomerInDoc.getDocumentElement();
	    	yfcManageCustomerInDocRoot.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GC);
	    	yfcManageCustomerInDocRoot.setAttribute(GCConstants.CUSTOMER_ID, sCustomerID);
	    	YFCElement yfcManageCustomerExtn = yfcManageCustomerInDocRoot.createChild(GCConstants.EXTN);
	    	yfcManageCustomerExtn.setAttribute("ExtnPOSCustomerID", sPOSBillcustID);
	
	        YFCDocument yfcCustomerListTemp = YFCDocument.getDocumentFor("<CustomerList><Customer CustomerID='' CustomerType='' OrganizationCode='' Status=''><Extn/></Customer></CustomerList>");
	    	LOGGER.verbose("MAnageCustomer indoc in Publish MPOS invoice class-->" + yfcManageCustomerInDoc.toString());
	    	YFCDocument manageCustomerOutDoc = GCCommonUtil.invokeAPI(env, GCConstants.MANAGE_CUSTOMER_API, yfcManageCustomerInDoc, yfcCustomerListTemp);
	    	LOGGER.verbose("manageCustomer API outDoc-->" + manageCustomerOutDoc.toString());	
		}
    //mPOS-1948- To persist ExtnPOSCustomerID received in POS response: End.
    
    
    LOGGER.endTimer("GCPublishMPOSOrderInvoice.invokeChangeOrder()");
    LOGGER.verbose("GCPublishMPOSOrderInvoice - invokeChangeOrder() : End");
  }

  /**
   * This method will process Web service response from POS
   *
   * @param yfcDocInvokeWebServiceOutput
   * @return
   */
  private void processWebServiceResponse(YFSEnvironment env, Document inDoc, YFCElement yfcEleInvoiceHeader,
      YFCDocument yfcDocInvokeWebServiceOutput) {
    LOGGER.beginTimer("GCPublishMPOSOrderInvoice.processWebServiceResponse()");
    LOGGER.verbose("GCPublishMPOSOrderInvoice - processWebServiceResponse() : Start");

    // Fix for 3042 : Start
    String sOrderInvoiceKey = yfcEleInvoiceHeader.getAttribute("OrderInvoiceKey");
    YFCDocument changeOrderInvoice = YFCDocument.createDocument(GCConstants.ORDER_INVOICE);
    YFCElement eleOrderInvoice = changeOrderInvoice.getDocumentElement();
    // eleOrderInvoice.setAttribute("StampTickets", "True");
    eleOrderInvoice.setAttribute(GCConstants.ORDER_INVOICE_KEY, sOrderInvoiceKey);
    YFCElement eleExtn = eleOrderInvoice.createChild(GCConstants.EXTN);

    YFCElement eleInvoiceOrder = yfcEleInvoiceHeader.getElementsByTagName(GCConstants.ORDER).item(0);
    String sOrderHeaderKey = eleInvoiceOrder.getAttribute(GCConstants.ORDER_HEADER_KEY);

    // Fix for 3042 : Stop

    String sSaleRequestResult = GCConstants.BLANK;
    String sPOSSalesTicketNo = GCConstants.BLANK;
    String sPOSBillcustID = GCConstants.BLANK;
    boolean bHasCallFailed = false;

    NodeList nSaleRequestResultList = yfcDocInvokeWebServiceOutput.getDocument().getElementsByTagName("SaleRequestResult");
    NodeList nlFaultString = yfcDocInvokeWebServiceOutput.getDocument().getElementsByTagName("faultstring");
    String sErrorInfo = "";
    if (nSaleRequestResultList.getLength() > 0) {
      Node nSaleRequestResult = nSaleRequestResultList.item(0);
      if (YFCCommon.isVoid(nSaleRequestResult)) {
        bHasCallFailed = true;
      } else {
        sSaleRequestResult = nSaleRequestResult.getTextContent();
        if (YFCCommon.isStringVoid(sSaleRequestResult)) {
          bHasCallFailed = true;
        } else {
          YFCDocument yfcDocResponse = YFCDocument.getDocumentFor(sSaleRequestResult);
          YFCElement yfcEleResponse = yfcDocResponse.getDocumentElement();
          YFCElement yfcEleSuccess = yfcEleResponse.getElementsByTagName(GCConstants.SUCCESS).item(0);
          if (!YFCCommon.isVoid(yfcEleSuccess)) {
            String sStatus = yfcEleSuccess.getNodeValue();
            if (YFCCommon.equals(sStatus, GCConstants.TRUE)) {
              YFCElement yfcElePOSSalesTicketNo =
                  yfcEleResponse.getElementsByTagName(GCXmlLiterals.POS_SALES_TICKET).item(0);
              YFCElement yfcElePOSBillcustID = yfcEleResponse.getElementsByTagName("POSBillcustID").item(0);
              if(!YFCCommon.isVoid(yfcElePOSBillcustID))
              {
            	  sPOSBillcustID = yfcElePOSBillcustID.getNodeValue();
              }
              if (!YFCCommon.isVoid(yfcElePOSSalesTicketNo)) {
                sPOSSalesTicketNo = yfcElePOSSalesTicketNo.getNodeValue();
                eleExtn.setAttribute(GCXmlLiterals.EXTN_POS_SALES_TICKET_NO, sPOSSalesTicketNo);
                eleExtn.setAttribute("ExtnTargetSystem", "POS");

                invokeChangeOrder(env, yfcEleInvoiceHeader,changeOrderInvoice, sPOSSalesTicketNo, sPOSBillcustID);

              }
            } else if (YFCCommon.equals(sStatus, GCConstants.FALSE_STR)) {
              YFCNodeList<YFCElement> nlErrorInfo = yfcEleResponse.getElementsByTagName("Errorinfo");
              YFCElement eleErrorInfo = nlErrorInfo.item(0);
         
              if (!YFCCommon.isVoid(eleErrorInfo)) {
                sErrorInfo = eleErrorInfo.getNodeValue();
              }
              if (YFCCommon.equals(env.getTxnObject("IsHoldAppiled"), "True")) {
                YFCException yfcException = new YFCException();
                yfcException.setAttribute(GCConstants.ERROR_CODE, sErrorInfo);
                yfcException.setAttribute(GCConstants.ERROR_DESCRIPTION, sErrorInfo);
                throw yfcException;
              } 
              else {
                Element inDocRootEle = inDoc.getDocumentElement();
                inDocRootEle.setAttribute("POSError", sErrorInfo);
                inDocRootEle.setAttribute("FailedResponse", "True");

                //Applying hold on order
                applyMPOSInvoiceFailureHold(env,sOrderHeaderKey);

                //Raising alert on hold
                GCCommonUtil.invokeService(env,"GCPOSConnectFailedMPOSAlertService", inDoc);
              }
            }
          } else {
            bHasCallFailed = true;
          }
        }
      }
    } else if (nlFaultString.getLength() > 0) {
    	
    	if (YFCCommon.equals(env.getTxnObject("IsHoldAppiled"), "True")) {
            YFCException yfcException = new YFCException();
           Element eleFaultString = (Element) nlFaultString.item(0);
  	      String sFaultString = eleFaultString.getTextContent();
            yfcException.setAttribute(GCConstants.ERROR_CODE, sFaultString);
            yfcException.setAttribute(GCConstants.ERROR_DESCRIPTION, sFaultString);
            throw yfcException;
    	}
    	else{
    		
    		Element eleFaultString = (Element) nlFaultString.item(0);
    	      String sFaultString = eleFaultString.getTextContent();
    	      Element inDocRootEle = inDoc.getDocumentElement();
    	      inDocRootEle.setAttribute("POSError", sFaultString);
    	      inDocRootEle.setAttribute("FailedResponse", "True");

    	      //Applying hold on order
    	      applyMPOSInvoiceFailureHold(env,sOrderHeaderKey);

    	      //Raising alert on hold
    	      GCCommonUtil.invokeService(env,"GCPOSConnectFailedMPOSAlertService", inDoc); 	    
    		
    	}
      } else {
      bHasCallFailed = true;
    }
    // Invoke Change order API to append POS sales ticket no at order line / extn level and update
    // component price if bundle line is shipped
    if (bHasCallFailed) {
      YFCException yfcException = new YFCException();
      LOGGER.endTimer("GCPublishMPOSOrderInvoice.processWebServiceResponse()");
      LOGGER.verbose("GCPublishMPOSOrderInvoice - processWebServiceResponse() : End With Error - POS Rejected");
      Element inDocRootEle = inDoc.getDocumentElement();
      inDocRootEle.setAttribute("POSError", GCErrorConstants.WEB_SERVICE_POS_INVOICE_CALL_TIMEOUT_CODE);
      if (YFCCommon.equals(env.getTxnObject("IsHoldAppiled"), "True")) {
        yfcException.setAttribute(GCConstants.ERROR_CODE, GCErrorConstants.WEB_SERVICE_POS_INVOICE_CALL_TIMEOUT_CODE);
        yfcException.setAttribute(GCConstants.ERROR_DESCRIPTION, "Could not connect to POS. Timeout");
        throw yfcException;
      }
      else
      {

        //Applying hold on order
        applyMPOSInvoiceFailureHold(env,sOrderHeaderKey);

        //Raising alert on hold
        GCCommonUtil.invokeService(env,"GCPOSConnectFailedMPOSAlertService", inDoc);
      }
    }
    LOGGER.endTimer("GCPublishMPOSOrderInvoice.processWebServiceResponse()");
    LOGGER.verbose("GCPublishMPOSOrderInvoice - processWebServiceResponse() : End");
  }


  public static void applyMPOSInvoiceFailureHold(YFSEnvironment env, String sOrderHeaderKey){

    YFCDocument docChangeOrder = YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "' Override='Y' SelectMethod='WAIT'/>");
    YFCElement changeOrderRootEle = docChangeOrder.getDocumentElement();
    YFCElement eleOrderHoldTypes = changeOrderRootEle.createChild(GCConstants.ORDER_HOLD_TYPES);
    YFCElement eleOrderHoldType = eleOrderHoldTypes.createChild(GCConstants.ORDER_HOLD_TYPE);
    eleOrderHoldType.setAttribute(GCConstants.HOLD_TYPE, "MPOS_INVCE_FAIL_HOLD");
    eleOrderHoldType.setAttribute(GCConstants.STATUS, "1100");
    if(LOGGER.isDebugEnabled()){
      LOGGER.verbose("Input doc to to changeOrder to apply POSInvoiceFailureHold:--> " + docChangeOrder.toString());
    }
    GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, docChangeOrder, null);

  }

  private YFCElement getManagerCashierApproveId(YFCElement yfcEleRootInDoc, YFCElement yfcEleInvoiceHeader) {
    LOGGER.beginTimer("GCPublishMPOSOrderInvoice.getManagerCashierApproveId()");
    LOGGER.verbose("GCPublishMPOSOrderInvoice.getManagerCashierApproveId() : Start");
    String sManagerID = GCConstants.BLANK_STRING;
    String sUserID = GCConstants.BLANK_STRING;

    /* Fetch manager approve ID and Cashier ID for the shipment from custom table - Start */
    YFCNodeList<YFCElement> yfcNLGCUserActivity =
        yfcEleInvoiceHeader.getElementsByTagName(GCConstants.GC_USER_ACTIVITY);
    for (int j = 0; j < yfcNLGCUserActivity.getLength(); j++) {
      YFCElement yfcEleGCUserActivity = yfcNLGCUserActivity.item(j);
      String sUserActivityUserAction = yfcEleGCUserActivity.getAttribute(GCConstants.USER_ACTION);
      if (YFCCommon.equals("ConfirmOrder", sUserActivityUserAction)) {
        sManagerID = yfcEleGCUserActivity.getAttribute(GCConstants.MANAGER_ID);
        sUserID = yfcEleGCUserActivity.getAttribute(GCConstants.USER_ID);
        break;
      }
    }

    /* Fetch manager approve ID and Cashier ID for the shipment from custom table - End */
    /* Set Manager Id and Cashier ID for the shipment - Start */
    yfcEleRootInDoc.setAttribute(GCConstants.MANAGER_ID, sManagerID);
    yfcEleRootInDoc.setAttribute(GCConstants.USER_ID, sUserID);
    /* Set Manager Id and Cashier ID for the shipment - End */

    LOGGER.endTimer("GCPublishMPOSOrderInvoice.getManagerCashierApproveId()");
    LOGGER.verbose("GCPublishMPOSOrderInvoice.getManagerCashierApproveId() : End");
    return yfcEleRootInDoc;
  }
}