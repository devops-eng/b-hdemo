/**
 * File Name : GCPushMPOSOrderToPOS.java
 *
 *OBJECTIVE: This class is called when GCPushMPOSOrderToPOS is invoked from mPOS. This class will push all CARRY lines to POS.
 *This will remove all CARRY Lines from order and send SHP / PICK lines to front end
 *
 *Modification Log :
 * -------------------------------------------------------------------------------------------------------------------------------
 * Version #                    Date                    Author                              Modification
 * 0.1                      06/28/2016              Singh, Amit Mohan               Initial Draft GCPushMPOSOrderToPOS
 * -------------------------------------------------------------------------------------------------------------------------------
 */
package com.gc.mpos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.gc.api.invoicing.GCPOSInvoicing;
import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCPushMPOSOrderToPOS {
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCAddShippingChargesDetail.class.getName());

  public Document pushToPOS(YFSEnvironment env, Document inDoc) {

    LOGGER.beginTimer("GCPushMPOSOrderToPOS.pushToPOS()");
    LOGGER.verbose("GCPushMPOSOrderToPOS - pushToPOS() : Start");

    YFCDocument pushToPOSInDoc = YFCDocument.getDocumentFor(inDoc);

    YFCDocument orderDetails = YFCDocument.getDocumentFor("<Order/>");
    String sOrderHeaderKey = pushToPOSInDoc.getDocumentElement().getAttribute(GCConstants.ORDER_HEADER_KEY);
    String sUserID = pushToPOSInDoc.getDocumentElement().getAttribute(GCConstants.USER_ID);

    YFCDocument changeOrderTemplate =
        YFCDocument.getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_getOrderDetails.xml"));
    YFCDocument getOrderListTemplate =
        YFCDocument.getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_getOrderList.xml"));

    if (!pushToPOSInDoc.getDocumentElement().hasChildNodes()) {
      YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "'/>");
      YFCDocument getOrderListOutput =
          GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST, getOrderListInput, getOrderListTemplate);
      YFCElement eleOrderLevel =
          getOrderListOutput.getDocumentElement().getElementsByTagName(GCConstants.ORDER).item(0);
      orderDetails = GCXMLUtil.getDocument(eleOrderLevel, true);
    } else {
      orderDetails = GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER, pushToPOSInDoc, changeOrderTemplate);
    }

    // Clone orderDetails doc for later to remove GnG lines
    Document cloneDoc = (Document) orderDetails.getDocument().cloneNode(true);
    YFCDocument orderDetailsToRemoveGnG = YFCDocument.getDocumentFor(cloneDoc);
    YFCDocument ifFalsePOSResponse = YFCDocument.getDocumentFor(cloneDoc);

    String sDeliveryMethod = "";

    // Stamping Date & Time
    orderDetails = mPOSDateTimeStamp(env, orderDetails.getDocument());
    orderDetails.getDocumentElement().setAttribute("UserID", sUserID);

    // Sort OrderLines in ascending OrderLines
    List<YFCElement> orderLines = new ArrayList<YFCElement>();
    YFCNodeList<YFCElement> nlOrderLines = orderDetails.getElementsByTagName(GCConstants.ORDER_LINE);
    for (int j = 0; j < nlOrderLines.getLength(); j++) {
      YFCElement eleOrderLineAddingToList = nlOrderLines.item(j);
      orderLines.add(eleOrderLineAddingToList);
    }

    Collections.sort(orderLines, new orderLinesPrimeLineNo());
    YFCElement eleOrderLinesToRemove = orderDetails.getElementsByTagName(GCConstants.ORDER_LINES).item(0);
    eleOrderLinesToRemove.getParentElement().removeChild(eleOrderLinesToRemove);

    YFCElement eleOrderLinesNew = orderDetails.createElement(GCConstants.ORDER_LINES);
    orderDetails.getDocumentElement().appendChild(eleOrderLinesNew);
    eleOrderLinesNew.setAttribute("TotalNumberOfRecords", orderLines.size());
    for (int k = 0; k < orderLines.size(); k++) {
      YFCElement eleOrderLineSorted = orderLines.get(k);
      YFCElement sortedOrderLineToAdd = orderDetails.importNode(eleOrderLineSorted, true);
      eleOrderLinesNew.appendChild(sortedOrderLineToAdd);
    }

    LOGGER.verbose("Message going to logics" + orderDetails.toString());

    // Associating warranties from SET Parent to Primary Component
    Map<String, String> setComponentsMap = new HashMap<String, String>();
    setComponentsMap = setCompParentMap(env, orderDetails.getDocument());


    // Stamping Latest Price Override Note
    orderDetails = stampMPOSLatestOverrideNote(env, orderDetails.getDocument());

    // Applying PriceOverride in POS Sales Ticket
    orderDetails = overridePrice(env, orderDetails.getDocument());
    //MPOS-2391: Start.
    YFCNodeList<YFCElement> nlOrderLine = orderDetails.getElementsByTagName(GCConstants.ORDER_LINE);
    for(int l = 0; l < nlOrderLine.getLength(); l++)
    {
    	LOGGER.verbose("Inside the loop to stamp unit price of all the orderlines to OrderLine.UnitPrice");
    	YFCElement eleOrderLine = nlOrderLine.item(l);
    	YFCElement eleOrderLinePriceInfo = eleOrderLine.getElementsByTagName(GCConstants.LINE_PRICE_INFO).item(0);
    	String sOrderlineUnitPrice = eleOrderLine.getAttribute(GCConstants.UNIT_PRICE);
    	String sOrderlineKitCode = eleOrderLine.getAttribute(GCConstants.KIT_CODE);
    	if(YFCCommon.isVoid(sOrderlineUnitPrice) && !YFCCommon.equalsIgnoreCase(sOrderlineKitCode, GCConstants.BUNDLE)){
    	String sLinePriceInfoUnitPrice = eleOrderLinePriceInfo.getAttribute(GCConstants.UNIT_PRICE);
    	eleOrderLine.setAttribute(GCConstants.UNIT_PRICE, sLinePriceInfoUnitPrice);
    	}
    }
    //MPOS-2391: End.
    
    
    // Clubbing all OrderLines with same POSSKUNumber
    orderDetails = clubMPOSOrderLines(env, orderDetails.getDocument(), setComponentsMap);


    orderDetails.getDocumentElement().setAttribute("MessageType", "SUSPEND");
    LOGGER.verbose("Message pushed to XSL" + orderDetails.toString());

    Document messageToPOS = GCCommonUtil.invokeService(env, "GCPreparePushToPOSInput", orderDetails.getDocument());
    LOGGER.verbose("Message pushed to POS" + GCXMLUtil.getXMLString(messageToPOS));

    Document messageFromPOS = GCCommonUtil.invokeService(env, "GCPushMPOSOrderToPOSService", messageToPOS);
    YFCDocument docFromPOS = YFCDocument.getDocumentFor(messageFromPOS);
    LOGGER.verbose("Message received from POS" + GCXMLUtil.getXMLString(messageFromPOS));

    boolean bHasPOSCallFailed = false;
    String sSuspendRequestResult = GCConstants.BLANK;
    if (!YFCCommon.isVoid(docFromPOS)) {
      NodeList nlSuspendRequestResult = docFromPOS.getDocument().getElementsByTagName("SuspendRequestResult");
      NodeList nlFaultString = docFromPOS.getDocument().getElementsByTagName("faultstring");

      if (nlSuspendRequestResult.getLength() > 0) {
        Node nSuspendRequestResult = nlSuspendRequestResult.item(0);
        if (YFCCommon.isVoid(nSuspendRequestResult)) {
          bHasPOSCallFailed = true;
        } else {
          sSuspendRequestResult = nSuspendRequestResult.getTextContent();
          if (YFCCommon.isStringVoid(sSuspendRequestResult)) {
            bHasPOSCallFailed = true;
          }
        }
      } else if (nlFaultString.getLength() > 0) {
        bHasPOSCallFailed = true;
      }
    } else {
      bHasPOSCallFailed = true;
    }

    if (bHasPOSCallFailed) {
      LOGGER.verbose("TimeOut Exception Occured");
      orderDetails.getDocumentElement().setAttribute("ErrorInfo", "Push to POS failed. Please try again.");
      return orderDetails.getDocument();
    }

    YFCDocument textFromPOS = YFCDocument.getDocumentFor(sSuspendRequestResult);
    String sSuccess = textFromPOS.getDocumentElement().getElementsByTagName(GCConstants.SUCCESS).item(0).getNodeValue();

    if (YFCCommon.equalsIgnoreCase(sSuccess, "true")) {
      LOGGER.verbose("True Response");
      List<String> carryLines = new ArrayList<String>();
      YFCNodeList<YFCElement> nlOrderLineRemoveGnG =
          orderDetailsToRemoveGnG.getElementsByTagName(GCConstants.ORDER_LINE);
      for (int d = 0; d < nlOrderLineRemoveGnG.getLength(); d++) {
        YFCElement eleOrderLine = nlOrderLineRemoveGnG.item(d);
        sDeliveryMethod = eleOrderLine.getAttribute(GCConstants.DELIVERY_METHOD);
        if (YFCCommon.equalsIgnoreCase(sDeliveryMethod, "CARRY")) {
          String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
          carryLines.add(sOrderLineKey);
        }
      }

      // Prepare doc for changed order details
      YFCDocument changeOrderInput = YFCDocument.getDocumentFor("<Order/>");
      changeOrderInput.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
      YFCElement eleOrderLines = changeOrderInput.createElement(GCConstants.ORDER_LINES);
      changeOrderInput.getDocumentElement().appendChild(eleOrderLines);

      for (int l = 0; l < carryLines.size(); l++) {
        YFCElement eleOrderLine = changeOrderInput.createElement(GCConstants.ORDER_LINE);
        eleOrderLine.setAttribute(GCConstants.ACTION, GCConstants.REMOVE);
        eleOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, carryLines.get(l));
        eleOrderLines.appendChild(eleOrderLine);
      }

      Document changedOrderDetails = GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER,
          changeOrderInput.getDocument(), changeOrderTemplate.getDocument());

      // delete Draft Order if all OrderLines are Pushed to POS
      YFCDocument currentOrderDetails = YFCDocument.getDocumentFor(changedOrderDetails);
      YFCElement eleOrderLinesPresent =
          currentOrderDetails.getDocumentElement().getElementsByTagName(GCConstants.ORDER_LINES).item(0);
      if (!eleOrderLinesPresent.hasChildNodes()) {
        YFCDocument deleteOrder =
            YFCDocument.getDocumentFor("<Order Action='DELETE' OrderHeaderKey='" + sOrderHeaderKey + "'/>");
        GCCommonUtil.invokeAPI(env, "deleteOrder", deleteOrder.getDocument());
      }

      LOGGER.verbose("Message going to Front End" + GCXMLUtil.getXMLString(changedOrderDetails));

      return changedOrderDetails;
    } else {
      ifFalsePOSResponse.getDocumentElement().setAttribute("ErrorInfo",
          "Push to POS cannot be completed at this time. Please create a new sale in POS.");

      return ifFalsePOSResponse.getDocument();
    }


  }

  static class orderLinesPrimeLineNo implements Comparator<YFCElement>, Serializable {

    @Override
    public int compare(YFCElement elem1, YFCElement elem2) {
      Double value1 = Double.parseDouble(elem1.getAttribute("PrimeLineNo"));
      Double value2 = Double.parseDouble(elem2.getAttribute("PrimeLineNo"));
      int compare = value1.compareTo(value2);
      return compare;
    }

  }

  // this method prorates BUNDLE charges of SET parent to primary & secondary components
  public static YFCDocument overridePrice(YFSEnvironment env, Document inDoc) {

    LOGGER.verbose("GCPushMPOSOrderToPOS - overridePrice() : Start");
    LOGGER.verbose("Input Doc to overridePrice method is:-" + GCXMLUtil.getXMLString(inDoc));

    boolean isPriceOverridden = false;
    boolean isSecondaryPriceMore = false;
    String mainSecondaryComponent = "";
    Map<String, Element> primaryComponents = new HashMap<String, Element>();
    String sPrimaryOrderLineKey = null;
    Double secondaryComponentPrice = 0.00;

    YFCDocument addOverrideDetails = YFCDocument.getDocumentFor(inDoc);
    YFCNodeList<YFCElement> nlOrderLine = addOverrideDetails.getElementsByTagName(GCConstants.ORDER_LINE);

    List<Element> setParentLines =
        GCXMLUtil.getElementListByXpath(inDoc, "Order/OrderLines/OrderLine[@IsBundleParent='Y']");

    if (setParentLines.isEmpty()) {
      LOGGER.verbose("No SET present");
      env.setTxnObject("setParentLines", "0");
      
    }

    else {
      for (int i = 0; i < setParentLines.size(); i++) {
        Element eleParentOrderLine = setParentLines.get(i);
        String sParentOrderLineKey = eleParentOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
        Element eleParentLinePriceInfo =
            (Element) eleParentOrderLine.getElementsByTagName(GCConstants.LINE_PRICE_INFO).item(0);
        String sParentUnitPrice = eleParentLinePriceInfo.getAttribute(GCConstants.UNIT_PRICE);
        String sParentListPrice = eleParentLinePriceInfo.getAttribute(GCConstants.LIST_PRICE);
        Double parentUnitPrice = Double.parseDouble(sParentUnitPrice);
        Double parentListPrice = Double.parseDouble(sParentListPrice);

        Element eleParentItemDetails =
            (Element) eleParentOrderLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
        Element eleParentItemExtn = (Element) eleParentItemDetails.getElementsByTagName(GCConstants.EXTN).item(0);
        String sPrimaryComponent = eleParentItemExtn.getAttribute(GCConstants.EXTN_PRIMARY_COMPONENT_ID);

        Element eleExtn = (Element) eleParentOrderLine.getElementsByTagName(GCConstants.EXTN).item(0);
        String sOverriddenPrice = eleExtn.getAttribute("ExtnOverridedPrice");
        Double overridenPrice = 0.00;
        if (!YFCCommon.isVoid(sOverriddenPrice)) {
          overridenPrice = Double.parseDouble(sOverriddenPrice);
        }
        String sExtnOverridePriceEmpId = eleExtn.getAttribute("ExtnOverridePriceEmpId");

        Element elePriceOverrideNote = null;
        NodeList nlNotes = eleParentOrderLine.getElementsByTagName(GCConstants.NOTE);
        for (int z = 0; z < nlNotes.getLength(); z++) {
          Element eleNote = (Element) nlNotes.item(z);
          String sNoteText = eleNote.getAttribute(GCConstants.NOTE_TEXT);
          if (sNoteText.contains("PriceOverrideReasonCode:")) {
            elePriceOverrideNote = eleNote;
          }
        }

        if (!YFCCommon.isVoid(elePriceOverrideNote) && overridenPrice > 0.00) {
          isPriceOverridden = true;
        }

        // Prorating charges when component unitPrice is greater than parent
        List<Element> setComponents = GCXMLUtil.getElementListByXpath(inDoc,
            "Order/OrderLines/OrderLine[@BundleParentOrderLineKey='" + sParentOrderLineKey + "']");
        for (int y = 0; y < setComponents.size(); y++) {
          Element eleComponentOrderLine = setComponents.get(y);
          Element eleComponentItem = (Element) eleComponentOrderLine.getElementsByTagName(GCConstants.ITEM).item(0);
          String sItemId = eleComponentItem.getAttribute(GCConstants.ITEM_ID);
          if (!YFCUtils.equals(sPrimaryComponent, sItemId)) {
            Element eleComponentLinePriceInfo =
                (Element) eleComponentOrderLine.getElementsByTagName(GCConstants.LINE_PRICE_INFO).item(0);
            Double componenetListPrice =
                Double.parseDouble(eleComponentLinePriceInfo.getAttribute(GCConstants.LIST_PRICE));
            if (componenetListPrice > parentUnitPrice) {
              isSecondaryPriceMore = true;
              mainSecondaryComponent = eleComponentOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
              break;
            }
          }
        }

        for (int y = 0; y < setComponents.size(); y++) {
          Element eleComponentOrderLine = setComponents.get(y);
          Element eleComponentLinePriceInfo =
              (Element) eleComponentOrderLine.getElementsByTagName(GCConstants.LINE_PRICE_INFO).item(0);
          Element eleComponentItem = (Element) eleComponentOrderLine.getElementsByTagName(GCConstants.ITEM).item(0);
          String sItemId = eleComponentItem.getAttribute(GCConstants.ITEM_ID);

          // Stamping BundleUnitPrice on each Component LineDetail element.
          eleComponentOrderLine.setAttribute("BundleParentUnitPrice", sParentUnitPrice);
          String sListprice = eleComponentLinePriceInfo.getAttribute(GCConstants.LIST_PRICE);

          // logic : Prorating charges when component unitPrice is greater than parent
          // UnitPrice.-----starts
          if (isSecondaryPriceMore && (0 != Double.parseDouble(sListprice))) {
            if (YFCUtils.equals(mainSecondaryComponent,
                eleComponentOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY))) {
              sListprice = sParentUnitPrice;
            } else {
              sListprice = "0.00";
            }
          }
          // logic : Prorating charges when component unitPrice is greater than parent
          // UnitPrice.-----ends

          eleComponentOrderLine.setAttribute(GCConstants.UNIT_PRICE, sListprice);

          if (YFCCommon.equals(sItemId, sPrimaryComponent)) {
            // Item is a primary item
            primaryComponents.put(sPrimaryComponent, eleComponentOrderLine);
            sPrimaryOrderLineKey = eleComponentOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
          } else {
            // Item is a secondary item
            secondaryComponentPrice += Double.parseDouble(sListprice);

            /*
             * if (isSecondaryComponent) { eleSecondaryComponent = eleChildLineDetail;
             * isSecondaryComponent = false; }
             */

            // Loop Over line taxes


          }
          // End of if secondary Item
        }

        Element elePrimaryComponentLine = primaryComponents.get(sPrimaryComponent);
        Double primaryUnitPrice = Double.parseDouble(sParentUnitPrice) - secondaryComponentPrice;
        Double primaryListPrice = Double.parseDouble(sParentListPrice) - secondaryComponentPrice;
        elePrimaryComponentLine.setAttribute(GCConstants.UNIT_PRICE, primaryUnitPrice.toString());
        Element elePrimCompLinePriceInfo =
            (Element) elePrimaryComponentLine.getElementsByTagName(GCConstants.LINE_PRICE_INFO).item(0);
        if (!YFCCommon.isVoid(elePrimCompLinePriceInfo)) {
          elePrimCompLinePriceInfo.setAttribute(GCConstants.UNIT_PRICE, primaryUnitPrice.toString());
          elePrimCompLinePriceInfo.setAttribute(GCConstants.LIST_PRICE, primaryListPrice.toString());
        }

        if (isPriceOverridden) {
          Element elePCNotes = (Element) elePrimaryComponentLine.getElementsByTagName(GCConstants.NOTES).item(0);
          Element elePCExtn = (Element) elePrimaryComponentLine.getElementsByTagName(GCConstants.EXTN).item(0);
          elePCExtn.setAttribute("ExtnOverridePriceEmpId", sExtnOverridePriceEmpId);
          elePCExtn.setAttribute("ExtnOverridedPrice", sOverriddenPrice);

          if (YFCCommon.isVoid(elePCNotes)) {
            elePCNotes = (Element) addOverrideDetails.createElement(GCConstants.NOTES);
            elePrimaryComponentLine.appendChild(elePCNotes);
          }
          Element eleNote = addOverrideDetails.getDocument().createElement(GCConstants.NOTE);
          elePCNotes.appendChild(eleNote);
          GCXMLUtil.copyElement(addOverrideDetails.getDocument(), elePriceOverrideNote, eleNote);
          elePrimaryComponentLine.setAttribute("OverridenPrice", String.valueOf(overridenPrice));
          elePrimaryComponentLine.setAttribute("IsPriceOverriden", "True");
        }
        isPriceOverridden = false;
        secondaryComponentPrice = 0.00;
        isSecondaryPriceMore = false;
      }
    }


    // Stamping Overriden Price on Regular Lines
    for (int n = 0; n < nlOrderLine.getLength(); n++) {
      YFCElement eleOrderLine = nlOrderLine.item(n);
      String sKitCode = eleOrderLine.getAttribute(GCConstants.KIT_CODE);
      String sBundleParentOrderLineKey = eleOrderLine.getAttribute(GCConstants.BUNDLE_PARENT_ORDERLINEKEY);

      if (YFCCommon.isVoid(sBundleParentOrderLineKey) && YFCCommon.isVoid(sKitCode)) {
        YFCElement eleExtn = eleOrderLine.getElementsByTagName(GCConstants.EXTN).item(0);
        String sOverriddenPrice = eleExtn.getAttribute("ExtnOverridedPrice");

        if (!YFCCommon.isVoid(sOverriddenPrice)) {
          Double overridenPrice = Double.parseDouble(sOverriddenPrice);
          if (overridenPrice > 0.00) {
            eleOrderLine.setAttribute("OverridenPrice", String.valueOf(overridenPrice));
            eleOrderLine.setAttribute("IsPriceOverriden", "True");
          }
        }

      }
    }

    return addOverrideDetails;
  }

  public static YFCDocument mPOSDateTimeStamp(YFSEnvironment env, Document mPOSTimeStamp) {
    // Stamp Order Date and Time & XML Date and Time
    LOGGER.verbose("Stamping XML and Order --> Date and Time");

    YFCDocument stampDateAndTime = YFCDocument.getDocumentFor(mPOSTimeStamp);
    YFCDocument docToStampDateTimeAtttr = YFCDocument.getDocumentFor("<OrderList/>");
    YFCElement eleToImport = docToStampDateTimeAtttr.importNode(stampDateAndTime.getDocumentElement(), true);
    docToStampDateTimeAtttr.getDocumentElement().appendChild(eleToImport);
    GCPOSInvoicing.getAndStampDates(docToStampDateTimeAtttr.getDocumentElement());
    String sOrderDate = docToStampDateTimeAtttr.getDocumentElement().getAttribute(GCConstants.ORDER_DATE);
    String sOrderTime = docToStampDateTimeAtttr.getDocumentElement().getAttribute(GCConstants.ORDER_TIME);
    String sXMLDate = docToStampDateTimeAtttr.getDocumentElement().getAttribute("XMLDate");
    String sXMLTime = docToStampDateTimeAtttr.getDocumentElement().getAttribute("XMLTime");

    stampDateAndTime.getDocumentElement().setAttribute(GCConstants.ORDER_DATE, sOrderDate);
    stampDateAndTime.getDocumentElement().setAttribute(GCConstants.ORDER_TIME, sOrderTime);
    stampDateAndTime.getDocumentElement().setAttribute("XMLDate", sXMLDate);
    stampDateAndTime.getDocumentElement().setAttribute("XMLTime", sXMLTime);

    return stampDateAndTime;
  }

  public static YFCDocument stampMPOSLatestOverrideNote(YFSEnvironment env, Document inDoc) {
    LOGGER.verbose("GCPushMPOSOrderToPOS - stampMPOSLatestOverrideNote() : Start");
    LOGGER.verbose("inDoc to stampMPOSLatestOverrideNote:- " + GCXMLUtil.getXMLString(inDoc));

    String sDeliveryMethod = "";
    YFCDocument stampOverride = YFCDocument.getDocumentFor(inDoc);
    YFCNodeList<YFCElement> nlOrderLine =
        stampOverride.getDocumentElement().getElementsByTagName(GCConstants.ORDER_LINE);

    for(int i=0; i<nlOrderLine.getLength(); i++){
      YFCElement eleOrderLine = nlOrderLine.item(i);

      sDeliveryMethod = eleOrderLine.getAttribute(GCConstants.DELIVERY_METHOD);
      if(YFCCommon.equalsIgnoreCase(sDeliveryMethod,"CARRY")){

        // Handle PriceOverride Notes
        YFCNodeList<YFCElement> nlNote = eleOrderLine.getElementsByTagName(GCConstants.NOTE);
        int count = nlNote.getLength();
        if (count == 1) {
          YFCElement eleNote = nlNote.item(0);
          String noteText = eleNote.getAttribute(GCConstants.NOTE_TEXT);
          if (noteText.contains("PriceOverrideReasonCode:")) {
            eleNote.setAttribute("IsLatestNote", GCConstants.YES);
          }
        } else {
          int maxNoteskey = '0';
          for (YFCElement eleNote : nlNote) {
            String noteText = eleNote.getAttribute(GCConstants.NOTE_TEXT);
            if (noteText.contains("PriceOverrideReasonCode:")) {
              String sNotesKey = eleNote.getAttribute("NotesKey");
              if (sNotesKey.length() > 4) {
                String subNoteKey = sNotesKey.substring(sNotesKey.length() - 4);
                int notesKey = Integer.parseInt(subNoteKey);
                maxNoteskey = Math.max(maxNoteskey, notesKey);
              }
            }
          }
          if (maxNoteskey != 0) {
            String NotesKeyMax = Integer.toString(maxNoteskey);
            for (YFCElement eleNote : nlNote) {
              String sNotesKey = eleNote.getAttribute("NotesKey");
              String noteText = eleNote.getAttribute(GCConstants.NOTE_TEXT);
              String subNote = sNotesKey.substring(sNotesKey.length() - 4);
              if (noteText.contains("PriceOverrideReasonCode:") && YFCUtils.equals(NotesKeyMax, subNote)) {
                eleNote.setAttribute("IsLatestNote", GCConstants.YES);
                break;
              }
            }
          }
        }

      }
    }

    LOGGER.verbose("OutDoc from stampMPOSLatestOverrideNote" + GCXMLUtil.getXMLString(stampOverride.getDocument()));
    return stampOverride;
  }

  public static YFCDocument clubMPOSOrderLines(YFSEnvironment env, Document inDoc,
      Map<String, String> setComponentsMap) {
    LOGGER.verbose("GCPushMPOSOrderToPOS - clubMPOSOrderLines() : Start");
    LOGGER.verbose("In Doc to clubMPOSOrderLines:- " + GCXMLUtil.getXMLString(inDoc));

    YFCDocument clubOrderLines = YFCDocument.getDocumentFor(inDoc);
    YFCNodeList<YFCElement> nlOrderLine = clubOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);

    // Combine quantities of all OrderLines with same POSSKUNumber
    String sPOSSKUNumber = "";
    String sQuantity = "";
    String sDeliveryMethod = "";
    Map<String, String> quantity_SKUNumber = new HashMap<String, String>();


    for (int i = 0; i < nlOrderLine.getLength(); i++) {
      YFCElement eleOrderLine = nlOrderLine.item(i);

      sDeliveryMethod = eleOrderLine.getAttribute(GCConstants.DELIVERY_METHOD);
      if (YFCCommon.equalsIgnoreCase(sDeliveryMethod, "CARRY")) {
    	  
    	  //MPOS- 2368: looping over line charges: Start.
    	  YFCNodeList<YFCElement> nlLineCharge = eleOrderLine.getElementsByTagName(GCConstants.LINE_CHARGE);
    	  LOGGER.verbose("nlLineCharge --> " + nlLineCharge.getLength());
    	  for(int x = 0; x < nlLineCharge.getLength(); x++)
    	  {
	    	  //MPOS-1523 fix: Start.
	    	  YFCElement eleLineCharge = nlLineCharge.item(x);
	    	    String sChargeAmount = "";
	    	    String sCouponID="";
	    	    if(!YFCCommon.isVoid(eleLineCharge))
	    	    {
	    	    	sChargeAmount = eleLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT);
	    	    	if(YFCCommon.equalsIgnoreCase(sChargeAmount, "0.00") || YFCCommon.equalsIgnoreCase(sChargeAmount, "0.0"))
	    	    	{
	    	    		if(eleLineCharge.hasChildNodes()){
	    	        	    YFCElement eleExtn = eleLineCharge.getChildElement(GCConstants.EXTN);
	    	        	    eleExtn.setAttribute("ExtnCouponID", "");
	    	        	    eleExtn.setAttribute("ExtnPromotionID", "");
	    	    		}
	    	    		
	    	    	}
	    	    }
	    	    //MPOS-1523 fix: End.
    	  }
    	//MPOS- 2368: looping over line charges: End.
        sPOSSKUNumber = eleOrderLine.getChildElement(GCConstants.EXTN).getAttribute("ExtnPOSSKUNumber");
        String sKitCode = eleOrderLine.getAttribute(GCConstants.KIT_CODE);
        if (!YFCCommon.equalsIgnoreCase(sKitCode, GCConstants.BUNDLE)) {
          sQuantity = eleOrderLine.getAttribute(GCConstants.ORDERED_QTY);
          String sPosition = String.valueOf(i);

          String sDependentONKey = eleOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
          if (setComponentsMap.containsKey(sDependentONKey)) {
            eleOrderLine.setAttribute(GCConstants.DEPENDENT_ON_LINE_KEY, setComponentsMap.get(sDependentONKey));
          }

          if (!YFCCommon.isVoid(sPOSSKUNumber) && YFCCommon.isVoid(sDependentONKey)) {
        if (quantity_SKUNumber.containsKey(sPOSSKUNumber)) {
          // Combine quantities of all OrderLines with same POSSKUNumber
          String intialCombPresent = quantity_SKUNumber.get(sPOSSKUNumber);
          String sLineNumberToUpdate = intialCombPresent.substring(0, intialCombPresent.indexOf("/"));
          String sInitialQuantity = intialCombPresent.substring(intialCombPresent.indexOf("/") + 1);
              String sFinalQuantity =
                  String.valueOf(Double.parseDouble(sInitialQuantity) + Double.parseDouble(sQuantity));
              String sFinalComb = sLineNumberToUpdate + "/" + sFinalQuantity;
              quantity_SKUNumber.put(sPOSSKUNumber, sFinalComb);
              nlOrderLine.item(Integer.parseInt(sLineNumberToUpdate)).setAttribute(GCConstants.ORDERED_QTY,
                  sFinalQuantity);

              // Delete OrderLines
              eleOrderLine.getParentNode().removeChild(eleOrderLine);
              i--;
            } else {
              String sComb_QuanPrimeLine = sPosition + "/" + sQuantity;
              quantity_SKUNumber.put(sPOSSKUNumber, sComb_QuanPrimeLine);
            }
          }
        } else {
        	
        	//mPOS- 2171: For SET Parent LineCharges with coupons details is prorated to primary component: Start.
        	YFCElement eleParentLineCharges = eleOrderLine.getElementsByTagName(GCConstants.LINE_CHARGES).item(0);
        	if(YFCCommon.equalsIgnoreCase(sKitCode, GCConstants.BUNDLE) && eleParentLineCharges.hasChildNodes())
        	{
        		String sParentOLK = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
        		String sPrimaryOLK = setComponentsMap.get(sParentOLK);
            	YFCElement elePrimaryOrderLine = null;
            	for(int s = 0; s<nlOrderLine.getLength(); s++)
            	{
            		YFCElement eleOL = nlOrderLine.item(s);
            		String sOLK = eleOL.getAttribute(GCConstants.ORDER_LINE_KEY);
            		if(YFCCommon.equals(sOLK, sPrimaryOLK))
            		{
            			elePrimaryOrderLine = eleOL;
            		}
            	}
            	YFCElement elePrimaryLineCharges = elePrimaryOrderLine.getElementsByTagName(GCConstants.LINE_CHARGES).item(0);
            	elePrimaryLineCharges.getParentElement().removeChild(elePrimaryLineCharges);
            	YFCElement eleImportParentLineCharges = (YFCElement) elePrimaryLineCharges.importNode(eleParentLineCharges);
            	elePrimaryOrderLine.appendChild(eleImportParentLineCharges);
            	LOGGER.verbose("Primary OrderLine with LineCharges -->" + elePrimaryOrderLine);
        	}
        	//mPOS- 2171: For SET Parent LineCharges with coupons details is prorated to primary component: End.
          eleOrderLine.getParentNode().removeChild(eleOrderLine);
          i--;
        }
      }
    }

    LOGGER.verbose("OutDoc from clubMPOSOrderLines:- " + GCXMLUtil.getXMLString(clubOrderLines.getDocument()));
    return clubOrderLines;
  }

  public static Map<String, String> setCompParentMap(YFSEnvironment env, Document inDoc) {
    Map<String, String> setAssociationParComp = new HashMap<String, String>();


    // Map to handle Warrant association in case of mutiple same SETs
    Map<String, Integer> sameSETPrimCompNumber = new HashMap<String, Integer>();
    int counter = 0;
    int primCompOLToFetch = 0;
    int updatePrimCompOL = 0;

    YFCDocument orderDetails = YFCDocument.getDocumentFor(inDoc);

    // Map all set Parent OLKey with OLKey of Primary Components, MPOS - 1209
    YFCNodeList<YFCElement> nlOrderLine = orderDetails.getElementsByTagName(GCConstants.ORDER_LINE);
    String sDeliveryMethod = "";
    String sPOSSKUNumber = "";
    String sOrderLineKey = "";

    for (int t = 0; t < nlOrderLine.getLength(); t++) {
      YFCElement eleOrderLine = nlOrderLine.item(t);
      sDeliveryMethod = eleOrderLine.getAttribute(GCConstants.DELIVERY_METHOD);
      if (YFCCommon.equalsIgnoreCase(sDeliveryMethod, "CARRY")) {
        String sKitCode = eleOrderLine.getAttribute(GCConstants.KIT_CODE);
        if (YFCCommon.equalsIgnoreCase(sKitCode, GCConstants.BUNDLE)) {
          sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
          YFCElement eleItemDetails = eleOrderLine.getElementsByTagName(GCConstants.ITEM_DETAILS).item(0);
          YFCElement eleExtn = eleItemDetails.getElementsByTagName(GCConstants.EXTN).item(0);
          String sPrimaryComponentItemID = eleExtn.getAttribute(GCConstants.EXTN_PRIMARY_COMPONENT_ID);

          if (sameSETPrimCompNumber.containsKey(sPrimaryComponentItemID)) {
            primCompOLToFetch = sameSETPrimCompNumber.get(sPrimaryComponentItemID);
            updatePrimCompOL = primCompOLToFetch + 1;
            sameSETPrimCompNumber.put(sPrimaryComponentItemID, updatePrimCompOL);
          } else {
            primCompOLToFetch = 0;
            sameSETPrimCompNumber.put(sPrimaryComponentItemID, 1);
          }

          // Element elePrimComp = GCXMLUtil.getElementByXPath(orderDetails.getDocument(),
          // "Order/OrderLines/OrderLine/Item[@ItemID='" + sPrimaryComponentItemID + "']/..");
          Element elePrimComp = (Element) GCXMLUtil
              .getElementListByXpath(orderDetails.getDocument(),
                  "Order/OrderLines/OrderLine/Item[@ItemID='" + sPrimaryComponentItemID + "']/..")
              .get(primCompOLToFetch);


          String sDependedOLKey = elePrimComp.getAttribute(GCConstants.ORDER_LINE_KEY);
          setAssociationParComp.put(sOrderLineKey, sDependedOLKey);
        }
        else {
          YFCElement eleBundleParentLine = eleOrderLine.getChildElement(GCConstants.BUNDLE_PARENT_LINE);
          String sDependentONKey = eleOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
          if (YFCCommon.isVoid(eleBundleParentLine) && YFCCommon.isVoid(sDependentONKey)) {
            sPOSSKUNumber = eleOrderLine.getChildElement(GCConstants.EXTN).getAttribute(GCConstants.EXTN_POS_SKU_NO);
            sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
            if (!setAssociationParComp.containsKey(sPOSSKUNumber) && !YFCCommon.isVoid(sPOSSKUNumber)) {
              setAssociationParComp.put(sPOSSKUNumber, sOrderLineKey);
            }
          }
          if (YFCCommon.isVoid(eleBundleParentLine) && !YFCCommon.isVoid(sDependentONKey)) {
            Element eleRegularPCParent = GCXMLUtil.getElementByXPath(orderDetails.getDocument(),
                "Order/OrderLines/OrderLine[@OrderLineKey='" + sDependentONKey + "']");
            Element eleRegularParentExtn = (Element) eleRegularPCParent.getElementsByTagName(GCConstants.EXTN).item(0);
            String sRegularParentPOSSKU = eleRegularParentExtn.getAttribute(GCConstants.EXTN_POS_SKU_NO);
            if (setAssociationParComp.containsKey(sRegularParentPOSSKU)) {
              eleOrderLine.setAttribute(GCConstants.DEPENDENT_ON_LINE_KEY,
                  setAssociationParComp.get(sRegularParentPOSSKU));
            }
          }
        }
      }
    }
    // MPOS - 1209

    return setAssociationParComp;
  }
}

