package com.gc.mpos;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.api.GCReserveCOMOrder;
import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class GCReserveMPOSOrder {
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCAddItemToCart.class.getName());

  public Document reserveOrder(YFSEnvironment env, Document inDoc) {

    LOGGER.beginTimer("GCReserveMPOSOrder.reserveOrder()");
    LOGGER.verbose("GCReserveMPOSOrder - reserveOrder() : Start");

    YFCDocument orderDetailsDoc = YFCDocument.getDocumentFor(inDoc);
    LOGGER.verbose("Reserve Order Class inDoc is" + orderDetailsDoc.toString());

    Document cloneDoc = (Document) orderDetailsDoc.getDocument().cloneNode(true);
    Element elePromise = (Element) cloneDoc.getElementsByTagName(GCConstants.PROMISE).item(0);
    Document cloneGetOrderList = GCXMLUtil.getDocumentFromElement(elePromise);
    YFCDocument findInventory = YFCDocument.getDocumentFor(cloneGetOrderList);
    LOGGER.verbose("Find Inventory doc is" + findInventory.toString());

    orderDetailsDoc.getDocumentElement()
    .removeChild(orderDetailsDoc.getDocumentElement().getElementsByTagName(GCConstants.PROMISE).item(0));

    String sOrderHeaderKey = orderDetailsDoc.getDocumentElement().getAttribute(GCConstants.ORDER_HEADER_KEY);

    LOGGER.verbose("Resetting OrderLine Reservations");
    GCReserveCOMOrder.resetOrderLineReservations(env, orderDetailsDoc.getDocumentElement());

    YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "'/>");
    YFCDocument getOrderListTemplate = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_OUTPUT_TMPL_FOR_RSRVATION);
    YFCDocument getOrderList =
        GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST, getOrderListInput, getOrderListTemplate);

    YFCElement eleOrderLevel = getOrderList.getDocumentElement().getChildElement(GCConstants.ORDER);
    YFCDocument reserveInvServInput = YFCDocument.getDocumentFor(eleOrderLevel.toString());
    YFCElement eleFindInventory = reserveInvServInput.importNode(findInventory.getDocumentElement(), true);
    reserveInvServInput.getDocumentElement().appendChild(eleFindInventory);

    LOGGER.verbose("Input going to GCReserveInventoryService is" + reserveInvServInput.toString());
    Document reserveInvServOutput =
        GCCommonUtil.invokeService(env, GCConstants.GC_RESERVE_INVENTORY_SERVICE, reserveInvServInput.getDocument());

    LOGGER.verbose("Input going to GCHandleBundleReservation is" + GCXMLUtil.getXMLString(reserveInvServOutput));
    Document bundleReservation = GCCommonUtil.invokeService(env,
        GCConstants.GC_HANDLE_BUNDLE_RESERVATION_CALL_CENTER_SERV, reserveInvServOutput);

    YFCDocument changeOrderWithReser = GCCommonUtil.invokeAPI(env, GCConstants.API_CHANGE_ORDER,
        YFCDocument.getDocumentFor(bundleReservation),
        YFCDocument
            .getDocumentFor(getOrderListTemplate.getDocumentElement().getChildElement(GCConstants.ORDER).toString()));

    return changeOrderWithReser.getDocument();

  }
}
