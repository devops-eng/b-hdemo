/**
 * File Name : GCUpdateMPOSOrder.java
 *
 *OBJECTIVE: This class is called when GCUpdateMPOSOrder is invoked from mPOS. This class will adds OrderLines to order and
 * performs complete inventory validation & adds tax info to the order
 *
 *Modification Log :
 * -------------------------------------------------------------------------------------------------------------------------------
 * Version #                    Date                    Author                              Modification
 * 0.1                      05/18/2016              Singh, Amit Mohan               Initial Draft UpdateMPOSOrder MPOS - 428
 * 0.2                      06/01/2016              Singh, Amit Mohan               Added logic for Order Sourcing Controls
 * -------------------------------------------------------------------------------------------------------------------------------
 */
package com.gc.mpos;

import java.util.HashSet;
import java.util.Set;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author <a href="mailto:amitsingh.mohan@expicient.com">Amit Mohan Singh</a>
 */

public class GCUpdateMPOSOrder {
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCUpdateMPOSOrder.class.getName());

  public Document updateMPOSOrder(YFSEnvironment env, Document inDoc) {

    LOGGER.verbose("GCUpdateMPOSOrder - updateMPOSOrder() : Start");
    LOGGER.verbose("GCUpdateMPOSOrder - updateMPOSOrder() inDoc is" + inDoc.toString());

    env.setTxnObject("EntryType", "mPOS");
    
    
    YFCDocument updateInDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleInputRoot = updateInDoc.getDocumentElement();
    String sIsCompleteCartValidation = eleInputRoot.getAttribute("IsCompleteCartValidation");
    String sIsPISAvailableNow = eleInputRoot.getAttribute("IsPISAvailableNow");
    String sStoreID = eleInputRoot.getAttribute(GCConstants.STORE_ID);
    String sOrderHeaderKey = eleInputRoot.getAttribute(GCConstants.ORDER_HEADER_KEY);
    String sIsSPODisable = eleInputRoot.getAttribute("IsSPODisable");

    LOGGER.verbose("Call is for Complete Inventory Validation" + sIsCompleteCartValidation);
    
    
    YFCDocument changeOrderTemp =
            YFCDocument.getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_getOrderDetails.xml"));

    // If N --> changeOrder to add OrderLines
    if (YFCCommon.equalsIgnoreCase(sIsCompleteCartValidation, "N")) {


      if (YFCCommon.equalsIgnoreCase(sIsPISAvailableNow, "Y")) {
        YFCDocument getOrderListTemp =
            YFCDocument.getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_getOrderList.xml"));
        YFCDocument orderDetailsInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "'/>");



        YFCDocument orderDetailsList =
            GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST, orderDetailsInput, getOrderListTemp);

        NodeList nlOrderLineExisting = orderDetailsList.getDocument().getElementsByTagName(GCConstants.ORDER_LINE);


        Element eleOrderLinesSent = null;
        eleOrderLinesSent = (Element) updateInDoc.getDocument().getElementsByTagName(GCConstants.ORDER_LINES).item(0);
        if (YFCCommon.isVoid(eleOrderLinesSent)) {
          eleOrderLinesSent = updateInDoc.getDocument().createElement(GCConstants.ORDER_LINES);
          updateInDoc.getDocument().getDocumentElement().appendChild(eleOrderLinesSent);
        }

        boolean isShippingChargePresent = false;

        for (int j = 0; j < nlOrderLineExisting.getLength(); j++) {
          Element eleOrderLine = (Element) nlOrderLineExisting.item(j);
          NodeList nlLineCharge = eleOrderLine.getElementsByTagName(GCConstants.LINE_CHARGE);

          for (int p = 0; p < nlLineCharge.getLength(); p++) {
            Element eleLineCharge = (Element) nlLineCharge.item(p);
            String sChargeCategory = eleLineCharge.getAttribute(GCConstants.CHARGE_CATEGORY);
            if (YFCCommon.equalsIgnoreCase(sChargeCategory, GCConstants.SHIPPING)) {
              eleLineCharge.getParentNode().removeChild(eleLineCharge);
              p--;
              isShippingChargePresent = true;
            }
          }

          if (isShippingChargePresent) {
            String sOrderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
            Element eleOrderLineToUpdate = null;

            if (!YFCCommon.isVoid(updateInDoc.getDocument().getElementsByTagName(GCConstants.ORDER_LINES).item(0))) {
              eleOrderLineToUpdate = GCXMLUtil.getElementByXPath(updateInDoc.getDocument(),
                  "Order/OrderLines/OrderLine[@OrderLineKey='" + sOrderLineKey + "']");
            }
            Element eleLineChargesImport = (Element) updateInDoc.getDocument()
                .importNode(eleOrderLine.getElementsByTagName(GCConstants.LINE_CHARGES).item(0), true);
            if (YFCCommon.isVoid(eleOrderLineToUpdate)) {
              Element eleOrderLineNew = updateInDoc.getDocument().createElement(GCConstants.ORDER_LINE);
              eleOrderLinesSent.appendChild(eleOrderLineNew);
              eleOrderLineNew.setAttribute(GCConstants.ORDER_LINE_KEY, sOrderLineKey);
              eleOrderLineNew.appendChild(eleLineChargesImport);
              eleLineChargesImport.setAttribute(GCConstants.RESET, "Y");
            } else {
              eleOrderLineToUpdate.appendChild(eleLineChargesImport);
              eleLineChargesImport.setAttribute(GCConstants.RESET, "Y");
        }
      }
          isShippingChargePresent = false;

        }


        updateInDoc.getDocumentElement().setAttribute(GCConstants.SELECT_METHOD, "NO_WAIT");

      }



      YFCNodeList<YFCElement> nlOrderLines = updateInDoc.getElementsByTagName(GCConstants.ORDER_LINE);
      for (int x = 0; x < nlOrderLines.getLength(); x++) {
        YFCElement eleOrderLine = nlOrderLines.item(x);

        String sDeliveryMethod = eleOrderLine.getAttribute(GCConstants.DELIVERY_METHOD);
        String sKitCode = eleOrderLine.getAttribute(GCConstants.KIT_CODE);
        YFCElement eleOrderLineSourcingControl =
            eleOrderLine.getElementsByTagName(GCConstants.ORDER_LINE_SOURCING_CNTRL).item(0);

        if (!YFCCommon.isVoid(eleOrderLineSourcingControl)) {
          if (YFCCommon.equalsIgnoreCase(sDeliveryMethod, "CARRY")) {
            // To add OrderLineSourcingControls if not present
            eleOrderLineSourcingControl.setAttribute(GCConstants.ACTION, GCConstants.REMOVE);
          }
        } else {
          if (!YFCCommon.equalsIgnoreCase(sDeliveryMethod, "CARRY")
              && !YFCCommon.equalsIgnoreCase(sKitCode, "BUNDLE")) {
            YFCElement eleOrderLineSourcingControls =
                updateInDoc.createElement(GCConstants.ORDER_LINE_SOURCING_CNTRLS);
            eleOrderLine.appendChild(eleOrderLineSourcingControls);
            YFCElement eleOrderLineSourcingControlCreate =
                updateInDoc.createElement(GCConstants.ORDER_LINE_SOURCING_CNTRL);
            eleOrderLineSourcingControlCreate.setAttribute(GCConstants.NODE, sStoreID);
            eleOrderLineSourcingControlCreate.setAttribute(GCConstants.SUPPRESS_SOURCING, GCConstants.YES);
            eleOrderLineSourcingControls.appendChild(eleOrderLineSourcingControlCreate);
          }
        }
      }

      LOGGER.verbose("Doc going for changeOrder API is" + updateInDoc.toString());

      YFCDocument changeOrderToUpdateDraftOrder =
          GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, updateInDoc, changeOrderTemp);
      
      //mPOS-1067 : Start : Formatting ExtnInventoryCost upto two decimal places
      
      YFCElement eleRoot = changeOrderToUpdateDraftOrder.getDocumentElement();
      YFCNodeList<YFCElement> nlOrderLine = eleRoot.getElementsByTagName(GCConstants.ORDER_LINE);
      for (YFCElement eleOrderLine : nlOrderLine)
      {

        YFCElement eleItemDetails = eleOrderLine.getElementsByTagName("ItemDetails").item(0);
        YFCElement eleItemDetailsExtn = eleItemDetails.getElementsByTagName(GCConstants.EXTN).item(0);
        if(!YFCCommon.isVoid(eleItemDetailsExtn))
        {
          String sExtnInventoryCost = eleItemDetailsExtn.getAttribute("ExtnInventoryCost"); 
          if(!YFCCommon.isVoid(sExtnInventoryCost)){
        	  
        	  String sFormattedExtnInventoryCost = GCXMLUtil.formatPrice(sExtnInventoryCost);
              eleItemDetailsExtn.setAttribute("ExtnInventoryCost", sFormattedExtnInventoryCost); 
          }
         
          String sExtnMinSellPrice = eleItemDetailsExtn.getAttribute("ExtnMinSellPrice");
          if(!YFCCommon.isVoid(sExtnMinSellPrice)){
        	  
        	  String sFormattedExtnMinSellPrice = GCXMLUtil.formatPrice(sExtnMinSellPrice);
        	  eleItemDetailsExtn.setAttribute("ExtnMinSellPrice", sFormattedExtnMinSellPrice);
          }
        }

      }
      
      //mPOS-1067 : End
      

      LOGGER.verbose("Doc returning for adding OrderLine info" + changeOrderToUpdateDraftOrder.toString());


      return changeOrderToUpdateDraftOrder.getDocument();
    }
    // If Y --> Get CompleteInventoryValidation --> If Success = true --> changeOrder to add Taxes
    else {
    //mPOS-1091 : Start
    	GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, updateInDoc, changeOrderTemp);
    	
    	YFCDocument docToAddToCart  =  YFCDocument.createDocument(GCConstants.ORDER);
    	YFCElement rootEle = updateInDoc.getDocumentElement();
    	String sOrderHeaderKeyValue = rootEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
    	String sIsCompleteCartValidationValue = rootEle.getAttribute("IsCompleteCartValidation");
    	String sStoreIDValue = rootEle.getAttribute(GCConstants.STORE_ID);
    	
    	YFCElement eleRootToDocAddToCart = docToAddToCart.getDocumentElement();
    	eleRootToDocAddToCart.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKeyValue);
    	eleRootToDocAddToCart.setAttribute("IsCompleteCartValidation", sIsCompleteCartValidationValue);
    	eleRootToDocAddToCart.setAttribute(GCConstants.STORE_ID, sStoreIDValue);
    	eleRootToDocAddToCart.setAttribute("IsSPODisable", sIsSPODisable);
    	
    //mPOS-1091 : End
    	
    	YFCDocument invValidationResponse = GCCommonUtil.invokeService(env, "GCAddItemToMPOSCartService", docToAddToCart);


      String sResponse = invValidationResponse.getDocumentElement().getAttribute("Success");

      if (YFCCommon.equalsIgnoreCase(sResponse, "true")) {

        //<!--------------------------------------------------------------------------------->
    	  if(!YFCCommon.equalsIgnoreCase(sIsSPODisable, "Y"))
          {
      		  Document reservationDoc = GCCommonUtil.invokeService(env, "GCMPOSReserveOrderService", invValidationResponse.getDocument());
        LOGGER.verbose("Doc after reservations is" + GCXMLUtil.getXMLString(reservationDoc));
      		  
          }
    	  
    	  GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, invValidationResponse, changeOrderTemp);

        YFCDocument changeOrderToStampOrderTaxes = stampOrderTaxes(env, updateInDoc);
        return changeOrderToStampOrderTaxes.getDocument();
      } else {
        LOGGER.verbose("Complete Cart Inventory Validation failed. Returning with appropriate error message");

        return invValidationResponse.getDocument();
      }
    }

  }

  public YFCDocument stampOrderTaxes(YFSEnvironment env, YFCDocument updateInDoc) {

    LOGGER.verbose("GCUpdateMPOSOrder - stampOrderTaxes() : Start");
    LOGGER.verbose("GCUpdateMPOSOrder - stampOrderTaxes() inDoc is" + updateInDoc.toString());

    String sOrderHeaderKey = updateInDoc.getDocumentElement().getAttribute(GCConstants.ORDER_HEADER_KEY);

    YFCDocument getOrderListInputDoc = YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "'/>");
    YFCDocument getOrderListTemp =
        YFCDocument
        .getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/getOrderListForVertexCall.xml"));

    YFCDocument getOrderListOut =
        GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST, getOrderListInputDoc, getOrderListTemp);

    YFCElement nOrderHeaderLevel = getOrderListOut.getElementsByTagName(GCConstants.ORDER).item(0);
    YFCDocument docToTaxService = GCXMLUtil.getDocument(nOrderHeaderLevel, true);

    LOGGER.verbose("Doc going to TaxCalculationService is" + docToTaxService.toString());

    // call TaxCalculationService to receive doc containing all tax info
    Document processTaxCalculationsServiceResponseDoc =
        GCCommonUtil.invokeService(env, "GCProcessTaxCalculationsService", docToTaxService.getDocument());
    YFCDocument processTaxInfoDoc = YFCDocument.getDocumentFor(processTaxCalculationsServiceResponseDoc);

    LOGGER
    .verbose("Document returning from TaxCalculation class"
        + GCXMLUtil.getXMLString(processTaxCalculationsServiceResponseDoc));

    String sIsVertexCallFailed = (String) env.getTxnObject("IsVertexCallFailed");

    if (YFCCommon.equalsIgnoreCase(sIsVertexCallFailed, "Y")) {
      processTaxInfoDoc.getDocumentElement().setAttribute("IsVertexCallFailed", "Y");
      processTaxInfoDoc.getDocumentElement().setAttribute("ErrorInfo",
          "mPOS currently unavailable, please try again. If this issue persists, please process transaction on POS and/or SPO, as applicable.");
      return processTaxInfoDoc;
    }
    else {
    YFCDocument processTaxCalc = YFCDocument.getDocumentFor(processTaxCalculationsServiceResponseDoc);
    // call changeOrder to add Tax info in Order
    YFCDocument changeOrderTemp =
        YFCDocument.getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_getOrderDetails.xml"));

    YFCDocument changeOrderToAddTaxInfo = GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER,
        processTaxCalc, changeOrderTemp);

      //MPOS-1389 fix: Start.
      YFCElement finalDocRoot = changeOrderToAddTaxInfo.getDocumentElement();
      YFCElement eleGCOrderCouponList = finalDocRoot.getElementsByTagName(GCConstants.GC_ORDER_COUPON_LIST).item(0);
      if(!YFCCommon.isVoid(eleGCOrderCouponList))
      {
	      if(eleGCOrderCouponList.hasChildNodes())
	      {
	    	  YFCNodeList<YFCElement> nlGCOrderCoupon = finalDocRoot.getElementsByTagName(GCConstants.GC_ORDER_COUPON);
	    	  int nlLength = nlGCOrderCoupon.getLength();
	    	  if(nlLength > 1)
	    	  {
	    		  String sCouponCode = "";
	    		  String sCouponKey = "";
	              Set<String> couponsSet = new HashSet<>();
	              for(int i = 0; i<nlLength; i++)
	              {
	            	  YFCElement eleOrderCoupon = nlGCOrderCoupon.item(i);
	            	  if(!YFCCommon.isVoid(eleOrderCoupon))
	            	  {
	            		  sCouponCode = eleOrderCoupon.getAttribute(GCConstants.COUPON_CODE);
	            		  sCouponKey = eleOrderCoupon.getAttribute("GCOrderCouponKey");
	            		  if(!couponsSet.contains(sCouponCode))
	            		  {
	            			  couponsSet.add(sCouponCode);
	            		  }
	            		  else
	            		  {
	            			 
	            		        Document inputDoc = GCXMLUtil.getDocument("<GCOrderCoupon GCOrderCouponKey='" + sCouponKey +"' OrderHeaderKey='" + sOrderHeaderKey + "'/>");
	            		        LOGGER.debug("Input for GCDeleteOrderCoupon service"
	            		            + GCXMLUtil.getXMLString(inputDoc));
	            		        GCCommonUtil.invokeService(env, "GCDeleteOrderCoupon", inputDoc);

	            		      }
	            		  }
	            	  }
	              }
	              
	    	  } 
	      }  
  
      YFCDocument inDocOrderDetails = YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "'/>");
      YFCDocument docWithAddedTax = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_DETAILS, inDocOrderDetails, changeOrderTemp);
      LOGGER.verbose("Doc returning with adding Tax info is" + docWithAddedTax.toString());
      docWithAddedTax.getDocumentElement().setAttribute("Success", "true");    
    //MPOS-1389 fix: End.
      
      return docWithAddedTax;
    }
  }

}
