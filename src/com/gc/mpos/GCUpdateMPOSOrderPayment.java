package com.gc.mpos;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCPaymentSecurity;
import com.gc.common.utils.GCPaymentUtils;
import com.gc.common.utils.GCXMLUtil;
import com.gc.userexit.GCCreditCardUE;
import com.yantra.yfc.date.YTimestamp;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSExtnPaymentCollectionInputStruct;
import com.yantra.yfs.japi.YFSExtnPaymentCollectionOutputStruct;

public class GCUpdateMPOSOrderPayment {
	
	private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCUpdateMPOSOrderPayment.class.getName());
	
	public Document orderPaymentInfo(YFSEnvironment env, Document inputDoc) throws GCException, TransformerException, ParseException {
	    LOGGER.verbose("GCUpdateMPOSOrderPayment - orderPayemntInfo() : Start");
	    LOGGER.verbose("GCUpdateMPOSOrderPayment inDoc is :-" + GCXMLUtil.getXMLString(inputDoc));
	    
		env.setTxnObject("EntryType", "mPOS");
		
	    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
	    YFCElement eleRoot = inDoc.getDocumentElement();
	    YFCElement eleOrderExtn = eleRoot.getElementsByTagName(GCConstants.EXTN).item(0);
	    String sOrderHeaderKey = eleRoot.getAttribute(GCConstants.ORDER_HEADER_KEY);
	    String sString101 = "";
	    String sPaymentType = "";
	    String sCustomerSignature = "";
	    String sSvcNo = "";
	    String sPlanCode="";
	    String sField42="";
	    String sPlanName = "";
	    YFSExtnPaymentCollectionInputStruct inStruct = new YFSExtnPaymentCollectionInputStruct();
	    YFSExtnPaymentCollectionOutputStruct outStruct = new YFSExtnPaymentCollectionOutputStruct();
	    
	    YFCDocument docOrderList = YFCDocument.createDocument(GCConstants.ORDER_LIST);
	    YFCElement eleOrderListRoot = docOrderList.getDocumentElement();
	    YFCElement eleOrder = eleOrderListRoot.createChild(GCConstants.ORDER);
	    eleOrder.setAttribute(GCConstants.ENTRY_TYPE, GCConstants.MPOS);
    	
	    YFCElement eleInDocPaymentMethods = eleRoot.getElementsByTagName(GCConstants.PAYMENT_METHODS).item(0);
	    YFCNodeList<YFCElement> nlPaymentMethod = eleInDocPaymentMethods.getElementsByTagName(GCConstants.PAYMENT_METHOD);
	    int nlLength = nlPaymentMethod.getLength();
	    for(int i = 0; i< nlLength; i++)
	    {
	    	YFCElement elePaymentMethod = nlPaymentMethod.item(i);
	    	sString101 = elePaymentMethod.getAttribute("String101");
      // MPOS-2050, Encoding issue
      LOGGER.verbose("Decoding text in String101 response\n\n\n");
      sString101 = sString101.replaceAll("amp;", "");
      sString101 = sString101.replace("amp;", "");
      LOGGER.verbose("Decoding complete\n\n\n");
      LOGGER.verbose("Response after decoding\n\n" + sString101 + "\n\n\n");
	    	sPaymentType = elePaymentMethod.getAttribute(GCConstants.PAYMENT_TYPE);
	    	sCustomerSignature = elePaymentMethod.getAttribute("CustomerSignature");
	    	sPlanCode = elePaymentMethod.getAttribute("PlanCode");
	    	sField42 = elePaymentMethod.getAttribute("Field42");
	    	sPlanName = elePaymentMethod.getAttribute("PlanName");
	    
	    	Document docVerifoneResponse = GCPaymentUtils.getCSVToXML(sString101);
	    	LOGGER.verbose("Verifone response in xml format-->" + GCXMLUtil.getXMLString(docVerifoneResponse));
	    	Element eleResponseRoot = docVerifoneResponse.getDocumentElement();
	    
      Element eleIxDebitCredit = (Element) XPathAPI.selectSingleNode(eleResponseRoot, GCConstants.IX_DEBIT_CREDIT);
      String sIxDebitCredit = eleIxDebitCredit.getTextContent();
      if(YFCCommon.equalsIgnoreCase(sIxDebitCredit, "Debit"))
      {
    	  sPaymentType = "DEBIT_CARD";
      }
	    	Element eleExpDate = (Element) XPathAPI.selectSingleNode(eleResponseRoot, GCConstants.IX_EXPDATE);
	        String sExpDate = eleExpDate.getTextContent();
	        if(!YFCCommon.isVoid(sExpDate))
	        {
	        	String sYear = sExpDate.substring(0,2);
		      	String sMonth = sExpDate.substring(2);
		      	sExpDate = sMonth + "/" + sYear;
		      	LOGGER.verbose("Expiration date is: "+ sExpDate);
	        }
      	
	        Element eleCardType = (Element) XPathAPI.selectSingleNode(eleResponseRoot, GCConstants.IX_CARD_TYPE);
	        String sCreditCardType = "";
	        if(!YFCCommon.isVoid(eleCardType))
	        {
	        	String sCCType = eleCardType.getTextContent();
	        	if(!YFCCommon.isVoid(sCCType)){
	        	sCreditCardType = eleCardType.getTextContent().toUpperCase();}
	        }
	        if(YFCCommon.equalsIgnoreCase(sPaymentType, GCConstants.PVT_LBL_CREDIT_CARD))
	        {
	        	sCreditCardType = "GEAR_CARD";
	        	
	        	YFCElement eleOrderGCDisclosureList =  eleRoot.getElementsByTagName("GCDisclosureList").item(0);
	        	if(YFCCommon.isVoid(eleOrderGCDisclosureList))
	        	{
		        	LOGGER.verbose("GCDisclosure doc is creation begins in case of PLCC-->");
		        	YFCDocument docGCDisclosureList = YFCDocument.createDocument("GCDisclosureList");
			    	YFCElement eleGCDisclosureDocRoot = docGCDisclosureList.getDocumentElement();
			    	YFCElement eleGCDisclosure = eleGCDisclosureDocRoot.createChild("GCDisclosure");
			    	
			        int stringLength = sField42.length();
			        
			        if(stringLength > 0)
			        {
			        	LOGGER.verbose("Setting attributes in GCDisclosure in case indoc has field42 length > 0-->");
				    	String sAcceptanceDate = GCDateUtils.getCurrentTime("");
			        	eleGCDisclosure.setAttribute("Disclosure", sField42);
			        	eleGCDisclosure.setAttribute("AcceptanceDate", sAcceptanceDate);
			        	eleGCDisclosure.setAttribute("DisclosureAccepted", GCConstants.YES);
			        	eleGCDisclosure.setAttribute("EConsentAccepted", GCConstants.YES);
			        	eleGCDisclosure.setAttribute("IsPromotionalPlan", GCConstants.YES);
			        	eleGCDisclosure.setAttribute("IsCustomerInStore", GCConstants.YES);	
			        	
			        	if(!YFCCommon.isVoid(eleOrderExtn))
					    {
					    	eleOrderExtn.importNode(eleGCDisclosureDocRoot);
					    }
					    
					    else
					    {
					    	LOGGER.verbose("Extn element created in case indoc doesn't have at Order level-->");
					    	eleOrderExtn = eleRoot.createChild(GCConstants.EXTN);
					    	eleOrderExtn.importNode(eleGCDisclosureDocRoot);
					    }
					
			        }
	        	}
	        	else
	        	{
	        		
			    	YFCElement eleGCDisclosure = eleOrderGCDisclosureList.createChild("GCDisclosure");
			    	
			        int stringLength = sField42.length();
			        
			        if(stringLength > 0)
			        {
			        	LOGGER.verbose("Setting attributes in GCDisclosure in case indoc has field42 length > 0-->");
				    	String sAcceptanceDate = GCDateUtils.getCurrentTime("");
			        	eleGCDisclosure.setAttribute("Disclosure", sField42);
			        	eleGCDisclosure.setAttribute("AcceptanceDate", sAcceptanceDate);
			        	eleGCDisclosure.setAttribute("DisclosureAccepted", GCConstants.YES);
			        	eleGCDisclosure.setAttribute("EConsentAccepted", GCConstants.YES);
			        	eleGCDisclosure.setAttribute("IsPromotionalPlan", GCConstants.YES);
			        	eleGCDisclosure.setAttribute("IsCustomerInStore", GCConstants.YES);	
			        }
			        
	        	}
	        }
	         
	        Element eleCreditCardNo = (Element) XPathAPI.selectSingleNode(eleResponseRoot, GCConstants.IX_ACCOUNT);
	        String sCreditCardNo = eleCreditCardNo.getTextContent();
	        String sBinNo = "";
		    String sDisplayCreditCardNo = "";
		    String sDisplaySvcNo = "";
        
	        if(!YFCCommon.isVoid(sCreditCardNo) && !YFCCommon.equalsIgnoreCase(sPaymentType,GCConstants.GIFT_CARD))
	        {
		        sBinNo = sCreditCardNo.substring(0, 6);
		        LOGGER.verbose("Bin number extracted from CC No: " + sBinNo);
		        sDisplayCreditCardNo = sCreditCardNo.substring(sCreditCardNo.length()-4, sCreditCardNo.length());
		        LOGGER.verbose("DisplayCreditCardNo is: " + sDisplayCreditCardNo);
		      //Encrypting Credit Card number
		       sCreditCardNo = GCPaymentSecurity.encryptCreditCard(sCreditCardNo, "OMS");
		     }
      //MPOS-2258: Gift Card last 4 validation: Start.
      else {
	    	Element eleIxSwipe = (Element) XPathAPI.selectSingleNode(eleResponseRoot,"IxSwipe");
	        String sIxSwipe = eleIxSwipe.getTextContent();
	        LOGGER.verbose("IxSwipe fetched -->" + sIxSwipe);
	        String[] parts = sIxSwipe.split("=");
	        String part1 = parts[0]; 
	        String part2 = parts[1];
	        sCreditCardNo = "";
	        String a1 = part1.substring(6,7);
	        String a2 = part1.substring(7,15);
	        String b1 = part2.substring(12,13);
	        String b2 = part2.substring(14,16);
	        String b3 = part2.substring(16,20);
	        
	        sSvcNo = b1+b2+a1+a2+b3;
	        LOGGER.verbose("SVC no fetched -->" + sSvcNo);
	        sDisplaySvcNo = sSvcNo.substring(sSvcNo.length() - 4, sSvcNo.length());
	        LOGGER.verbose("DisplaySvcNo is: " + sDisplaySvcNo);
	        // Encrypting SVCNo.
	        sSvcNo = GCPaymentSecurity.encryptCreditCard(sSvcNo, "OMS");
      }
	        
	        Element elePaymentReference1 = (Element) XPathAPI.selectSingleNode(eleResponseRoot, GCConstants.IX_INVOICE);
	        String sPaymentReference1 = elePaymentReference1.getTextContent();
	        Element eleAmount = (Element) XPathAPI.selectSingleNode(eleResponseRoot, GCConstants.IX_AMOUNT);
	        String sRequestAmount = eleAmount.getTextContent();
	        Double dRequestAmount = Double.parseDouble(sRequestAmount);
	        
	        inStruct.requestAmount = dRequestAmount;
	        inStruct.creditCardType = sCreditCardType;
	        inStruct.paymentType = sPaymentType;
	        inStruct.orderHeaderKey = sOrderHeaderKey;
	        inStruct.svcNo = sSvcNo;
	        inStruct.creditCardExpirationDate = sExpDate;
	        String sCollectionDate = "";
	        LOGGER.verbose("Payment Response and outStruct updation: Begins ");
	        outStruct = GCCreditCardUE.processCCAuthOutput(env,docVerifoneResponse, outStruct, inStruct, docOrderList.getDocument());
	        String sAuthReturnCode = outStruct.authReturnCode;
	        String sAuthorizationID = outStruct.authorizationId;
	        String sAuthorizationExpirationDate = "";
	        if(!YFCCommon.equalsIgnoreCase(sPaymentType, GCConstants.GIFT_CARD))
	        {
	    		sAuthorizationExpirationDate = outStruct.authorizationExpirationDate;     
	        }
	        else
	        {
	        	Date date = new Date();
	        	sCollectionDate = GCDateUtils.formatDate(date, "yyyy-MM-dd'T'HH:mm:ss");
	        	sAuthorizationExpirationDate = YTimestamp.HIGH_DATE.toString();
	        	
	        }
	        
	        LOGGER.verbose("Preparing Change Order input document to persist payment information on order");
        	
	        elePaymentMethod.removeAttribute("String101");
	        elePaymentMethod.removeAttribute(GCConstants.PAYMENT_TYPE);
	        elePaymentMethod.removeAttribute("CustomerSignature");
	        elePaymentMethod.removeAttribute("Field42");
	        elePaymentMethod.removeAttribute("PlanCode");
	        elePaymentMethod.removeAttribute("PlanName");
	        YFCElement elePaymentDetails = elePaymentMethod.createChild("PaymentDetails");
        
	        elePaymentMethod.setAttribute(GCConstants.PAYMENT_TYPE, sPaymentType);
	    	elePaymentMethod.setAttribute(GCConstants.CREDIT_CARD_NO, sCreditCardNo);
	    	elePaymentMethod.setAttribute(GCConstants.CREDIT_CARD_TYPE, sCreditCardType);
	    	elePaymentMethod.setAttribute(GCConstants.CREDIT_CARD_EXPIRY_DATE, sExpDate); 
     
	    	elePaymentMethod.setAttribute("PaymentReference1", sPaymentReference1);
    	
	    	if(YFCCommon.equalsIgnoreCase(sPaymentType, GCConstants.GIFT_CARD))
	    	{
	    		String sPinBlock = "";
	    		Element elePinBlock = (Element) XPathAPI.selectSingleNode(eleResponseRoot, "IxPinBlock");
	    		if(!YFCCommon.isVoid(elePinBlock))
	    		{
	    			sPinBlock = elePinBlock.getTextContent();
	    		}
	    		elePaymentMethod.setAttribute(GCConstants.SVC_NO, sSvcNo);
	    		elePaymentMethod.setAttribute(GCConstants.PAYMENT_REFERENCE_5, sPinBlock);
	    		elePaymentMethod.setAttribute(GCConstants.PAYMENT_REFERENCE_6, "");
	    		elePaymentMethod.setAttribute("DisplaySvcNo", sDisplaySvcNo);
	    		elePaymentDetails.setAttribute(GCConstants.CHARGE_TYPE, "CHARGE");
	    		elePaymentDetails.setAttribute("CollectionDate", sCollectionDate);
	    	}
	    	else
	    	{
	    		elePaymentMethod.setAttribute(GCConstants.PAYMENT_REFERENCE_3, sBinNo);
	    		elePaymentMethod.setAttribute(GCConstants.PAYMENT_REFERENCE_6, GCConstants.NO);
	    		elePaymentMethod.setAttribute(GCConstants.PAYMENT_REFERENCE_8, sPlanCode);
	    		elePaymentDetails.setAttribute(GCConstants.CHARGE_TYPE, GCConstants.AUTHORIZATION);
	    	}
	    	    	
	    	String sAuthExpDate = "";
	        LOGGER.verbose("Authorization expiraton date before conversion: " + sAuthorizationExpirationDate);
		    Date dateParse = new SimpleDateFormat(GCConstants.PAYMENT_DATE_TIME_FORMAT).parse(sAuthorizationExpirationDate);
            sAuthExpDate = GCDateUtils.formatDate(dateParse, "yyyy-MM-dd'T'HH:mm:ss");
            LOGGER.verbose("Authorization expiration after formatting: " + sAuthExpDate);
	        
	    	Double dMaxChargeLimit = outStruct.authorizationAmount;
	    	String sMaxChargeLimit = Double.toString(dMaxChargeLimit);
	    	elePaymentMethod.setAttribute(GCConstants.MAX_CHARGE_LIMIT, sMaxChargeLimit);
	    	elePaymentMethod.setAttribute("UnlimitedCharges", GCConstants.NO);   	
	    	elePaymentMethod.setAttribute(GCConstants.DISPLAY_CREDIT_CARD_NUM, sDisplayCreditCardNo);
    			         
	        elePaymentDetails.setAttribute(GCConstants.AUTH_RETURN_CODE, sAuthReturnCode); 
	        elePaymentDetails.setAttribute("AuthorizationID", sAuthorizationID);	 
			elePaymentDetails.setAttribute(GCConstants.PAYMENT_REQUEST_AMOUNT, sMaxChargeLimit);	 
			elePaymentDetails.setAttribute(GCConstants.PAYMENT_PROCESSED_AMOUNT, sMaxChargeLimit); 
			elePaymentDetails.setAttribute("AuthorizationExpirationDate", sAuthExpDate);
	        
	        YFCElement eleExtn = elePaymentMethod.createChild(GCConstants.EXTN);

      // MPOS-2050, Encoding issue
      LOGGER.verbose("Decoding text in String101 response\n\n\n");
      sString101 = sString101.replaceAll("amp;", "");
      sString101 = sString101.replace("amp;", "");
      LOGGER.verbose("Decoding complete\n\n\n");
      LOGGER.verbose("Response after decoding\n\n" + sString101 + "\n\n\n");

	        eleExtn.setAttribute("ExtnString101", sString101);
	        eleExtn.setAttribute("ExtnCustomerSignature", sCustomerSignature);
	        eleExtn.setAttribute("ExtnPlanName", sPlanName);
	      
	    }
	   
	    
	    LOGGER.verbose("changeOrder doc created--> " + inDoc.toString());
	        YFCDocument changeOrderTemp = YFCDocument.getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/mPOS_getOrderDetails.xml"));
	        YFCDocument paymentOutDoc = GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, inDoc, changeOrderTemp);
	    
	        LOGGER.verbose("Order retured after stamping Payment details-->" + paymentOutDoc.toString());
	        return paymentOutDoc.getDocument();
	}
	

}
