package com.gc.mpos;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class anotherExp {
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCUpdateMPOSOrderPayment.class.getName());

  public Document trythis(YFSEnvironment env, Document inDoc1) throws TransformerException {
    LOGGER.verbose("Input anotherExp:-->" + GCXMLUtil.getXMLString(inDoc1));
    // YFCDocument inDoc =
    // YFCDocument.getDocumentFor(GCCommonUtil.getDocumentFromPath("/global/template/api/douEncOrder.xml"));
    // Document pubInv = inDoc.getDocument();
    Element eleRoot = inDoc1.getDocumentElement();
    Element eleOrder = (Element) XPathAPI.selectSingleNode(eleRoot, "Order");
    NodeList nlString101 = eleRoot.getElementsByTagName("String101");

    for (int n = 0; n < nlString101.getLength(); n++) {
      Element eleString101 = (Element) nlString101.item(n);
      String sString101 = eleString101.getTextContent();

      sString101 = sString101.replaceAll("&amp;gt;", "&gt;");
      sString101 = sString101.replaceAll("&amp;lt;", "&lt;");
      sString101 = sString101.replaceAll("&gt;", ">");
      sString101 = sString101.replaceAll("&lt;", "<");

      LOGGER.verbose("Final String101-->" + sString101);

      eleString101.setTextContent(sString101);
    }

    // System.out.println("Did it worked");
    // System.out.println("Did it worked" + GCXMLUtil.getXMLString(inDoc1));
    LOGGER.verbose("Final Doc:-->" + GCXMLUtil.getXMLString(inDoc1));

    return inDoc1;
  }
}
