/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *          OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0               06/02/2014        Last Name, First Name      JIRA No: Description of issue.
             1.1               27/01/2015        Soni, Karan                OMS-4711: Unable to cancel the order placed using warranty item
             1.2               13/02/2015        Kumar Rakesh               OMS-4635 Adjustments zeroe'd out by addition of Preferred Player's Card
             1.3               19/03/2015        Ravi, Divya                GCSTORE-811 : ORder Fulfillment changes Phase 2
             1.4               01/04/2015        Infosys Ltd.               OMS-5156 Production: OMS is sending Pick Releases with blank shiptoname, shiptophone, & address line 2
             1.5               07/04/2015        Kumar Rakesh               Production: OMS sent Shipping discount at header level instead of prorating to the line level
             1.6               06/05/2015        Infosys Ltd.               OMS 5537 OMS stamping incorrect ENTRY_TYPE on some orders causing DAX to incorrectly assign Order Manager
             1.7			   16/05/2017		 Kumar Shashank             GCSTORE-6877 : logic to automatically Cancel Free Gift Item When the Parent ID is Canceled on an Order
             
 *#################################################################################################################################################################
 */
package com.gc.userexit;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.gc.api.GCDependentLineHold;
import com.gc.api.GCManageCommission;
import com.gc.api.GCProcessTaxCalculationsAPI;
import com.gc.api.order.GCSplitOrderLineService;
import com.gc.api.order.GCStampShipNodeOnOrderLine;
import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCOrderUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;
import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.yfs.japi.ue.YFSBeforeChangeOrderUE;

/**
 * @author <a href="mailto:email Address">Name</a>
 */
public class GCBeforeChangeOrderUE implements YFSBeforeChangeOrderUE {


  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCBeforeChangeOrderUE.class.getName());
  private static final String CLASS_NAME = GCBeforeChangeOrderUE.class.getName();
  Document docGetOrderListOP = null;
  private Properties properties = null;
  
  
  @Override
  public Document beforeChangeOrder(YFSEnvironment env, Document inDoc)
      throws YFSUserExitException {

    final String logInfo = "GCBeforeChangeOrderUE :: beforeChangeOrder :: ";
    LOGGER.verbose(logInfo + "Begin");
    LOGGER.debug(logInfo + "Incoming Document"
        + GCXMLUtil.getXMLString(inDoc));


    String sOrderHeaderKey = null;
    Element eleRoot = inDoc.getDocumentElement();

    // Fix for 1026: Start
    String sTranObjectValue = (String) env.getTxnObject("EntryType");
    if (YFCCommon.equals(sTranObjectValue, "mPOS")) {
      LOGGER.verbose("Transaction object received from GCATGPromoService to stamp entryType on inDoc");
      eleRoot.setAttribute(GCConstants.ENTRY_TYPE, sTranObjectValue);
    }
    // Fix for 1026: End

    // Fix for GCSTORE-4850 : Start
    String sExtnIsNonDependentWarranty = eleRoot.getAttribute("ExtnIsNonDependentWarranty");
    //GCSTORE-5985
    if(!YFCCommon.isVoid(sExtnIsNonDependentWarranty)){
      applyAssociationHold(env, inDoc,sExtnIsNonDependentWarranty);
    }
    /*else
    {
        checkAndRemoveAssoicationHold(env,inDoc);
    }*/
        

    // GCSTORE#3413--- Begin
    String isOdrAmendAftrPymnt = eleRoot.getAttribute("IsOrderAmendAfterPayment");
    // GCSTORE#3413--- End
    // --GCStore- Defect Fix Start
    removeAdditionalDeletedOrderLines(inDoc);
    // --GCStore- Defect Fix End

    Document modifyfulfillmentDoc = (Document) env
        .getApiTemplate("modifyFulfillmentOptions");
    // GCSTORE-3147 Start
    String sSkipShippingChargeForPLCC = eleRoot
        .getAttribute("SkipShippingChargeForPLCC");
    // GCSTORE-3147 End
    String sDocumentType = eleRoot.getAttribute(GCConstants.DOCUMENT_TYPE);
    sOrderHeaderKey = eleRoot
        .getAttribute(GCConstants.ORDER_HEADER_KEY);
    if (YFCCommon.isVoid(sOrderHeaderKey)) {
      String orderNo = eleRoot.getAttribute(GCConstants.ORDER_NO);
      YFCDocument getOrderListIp = YFCDocument.createDocument(GCConstants.ORDER);
      YFCElement eleGetOrderListIp = getOrderListIp.getDocumentElement();
      eleGetOrderListIp.setAttribute(GCConstants.ORDER_NO, orderNo);
      YFCDocument getOrderListTemp = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_TEMP_DATA_LOAD);
      YFCDocument getOrderListOpDoc =
          GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListIp, getOrderListTemp);

      YFCElement eleOrderOp = getOrderListOpDoc.getElementsByTagName(GCConstants.ORDER).item(0);
      sOrderHeaderKey = eleOrderOp.getAttribute(GCConstants.ORDER_HEADER_KEY);

    }
    String sIsProrationRequired=eleRoot
        .getAttribute(GCConstants.IS_PRORATION_REQUIRED);
    // WCC commission: Begin
    String sIsUpdateCommission = eleRoot
        .getAttribute(GCConstants.IS_UPDATE_COMMISSION);

    if (GCConstants.FLAG_Y.equalsIgnoreCase(sIsUpdateCommission)) {
      try {
        GCCommonUtil.invokeService(env,
            GCConstants.GC_UPDATE_COMMISSION_SERVICE, inDoc);
      } catch (YFSException ex) {
        LOGGER.error(logInfo, ex);
        YFSUserExitException e = new YFSUserExitException();
        e.setErrorCode(ex.getErrorCode());
        e.setErrorDescription(ex.getErrorDescription());
        throw e;
      } catch (Exception ex) {
        LOGGER.error(logInfo, ex);
        YFSUserExitException e = new YFSUserExitException();
        e.setErrorDescription(ex.getMessage());
        throw e;
      }
    }
    // WCC commission: End

    /* Stamp fulfillment for OrderLine: begin */
    try {

      String sStampFulfillment = GCXMLUtil.getAttributeFromXPath(inDoc,
          "Order/@StampFulfillmentType");
      String sExtnFulfillmentType = "";
      if (!YFCCommon.isVoid(sOrderHeaderKey)) {
        sExtnFulfillmentType = fetchExtnFulfillmentType(env,inDoc,
            sOrderHeaderKey);
      }
      if (GCConstants.FLAG_Y.equalsIgnoreCase(sStampFulfillment)) {

        LOGGER.debug("sExtnFulfillmentType" + sExtnFulfillmentType);

        if (!YFCCommon.isVoid(sExtnFulfillmentType)) {
          NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc,
              "Order/OrderLines/OrderLine");
          if(!YFCCommon.isVoid(nlOrderLine)){
            for (int iCounter = 0; iCounter < nlOrderLine.getLength(); iCounter++) {
              Element eleOrderLine = (Element) nlOrderLine
                  .item(iCounter);

              eleOrderLine.setAttribute(GCConstants.FULFILLMENT_TYPE,
                  sExtnFulfillmentType);
            }
          }
        }
      }
      eleRoot.removeAttribute("StampFulfillmentType");
    } catch (GCException ex) {
      LOGGER.error(logInfo, ex);
      throw ex.getYFSUserExitException();
    } catch (Exception ex) {
      LOGGER.error(logInfo, ex);
      YFSUserExitException e = new YFSUserExitException();
      e.setErrorDescription(ex.getMessage());
      throw e;
    }
    //GCSTORE-2629 : Start

    String sStampFLType = eleRoot.getAttribute(GCConstants.STAMP_FL_TYPE);
    if(YFCCommon.equals(sStampFLType, "Y")) {
      YFCDocument orderDetailsDoc = YFCDocument.getDocumentFor(inDoc);
      YFCElement eleOrder = orderDetailsDoc.getDocumentElement();
      YFCElement eleExtn = eleOrder.getChildElement(GCConstants.EXTN);
      String sExtnFulfillmentType = eleExtn.getAttribute(GCConstants.EXTN_FULFILLMENT_TYPE);
      YFCNodeList<YFCElement> listOrderLine = orderDetailsDoc.getElementsByTagName(GCConstants.ORDER_LINE);
      if(!YFCCommon.isVoid(listOrderLine)){
        int lengthOrderLine = listOrderLine.getLength();
        for (int iCounter = 0; iCounter < lengthOrderLine; iCounter++) {
          YFCElement eleOrderLine = listOrderLine.item(iCounter);
          eleOrderLine.setAttribute(GCConstants.FULFILLMENT_TYPE,sExtnFulfillmentType);
        }
      }
      eleRoot.removeAttribute(GCConstants.STAMP_FL_TYPE);
    }
    // GCSTORE-2629 : End


    /* Stamp fulfillment for OrderLine: begin */

    // Stamp Product Class: Start
    try {
      NodeList nlItem = XPathAPI.selectNodeList(inDoc,
          "Order/OrderLines/OrderLine/Item");
      for (int iCounter = 0; iCounter < nlItem.getLength(); iCounter++) {
        Element eleItem = (Element) nlItem.item(iCounter);
        String sProductClass = eleItem
            .getAttribute(GCConstants.PRODUCT_CLASS);
        if (YFCCommon.isVoid(sProductClass)) {
          String sExtnSetCode = GCBeforeCreateOrderUE
              .checkForTrackedAsSet(env, eleItem, GCConstants.GCI);
          if (GCConstants.ONE.equals(sExtnSetCode)) {
            eleItem.setAttribute(GCConstants.PRODUCT_CLASS,
                GCConstants.SET);
          } else if (YFCCommon.isVoid(sExtnSetCode)
              || GCConstants.ZERO.equals(sExtnSetCode)) {
            eleItem.setAttribute(GCConstants.PRODUCT_CLASS,
                GCConstants.REGULAR);
          }
        }
      }
    } catch (Exception ex) {
      LOGGER.error(logInfo, ex);
      YFSUserExitException e = new YFSUserExitException();
      e.setErrorDescription(ex.getMessage());
      throw e;
    }
    // Stamp Product Class: End
    // GC Store- 443
    YFCDocument orderDetailsDoc = YFCDocument
    		.getDocumentFor(docGetOrderListOP);
    //InDoc Order
    YFCDocument newInDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement yfcEleOrder = newInDoc.getDocumentElement();
    //GetOrderList o/p document
    YFCElement elenewOrderList = orderDetailsDoc.getDocumentElement();
    YFCElement elenewOrder = elenewOrderList
    		.getChildElement(GCConstants.ORDER);
    String docType=elenewOrder.getAttribute(GCConstants.DOCUMENT_TYPE);
    // GCSTORE - 6301 Start
    LOGGER.debug("6301 and 4459 start");

    YFCElement elenewExtn = elenewOrder.getChildElement(GCConstants.EXTN);
    if(!YFCCommon.isVoid(elenewExtn))
    {
    	if(elenewExtn.hasAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL))
    	{String sExtnOrderCaptureChannel=elenewExtn
    	.getAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL);
    	YFCElement elePersonBillTo = yfcEleOrder
    			.getChildElement(GCConstants.PERSON_INFO_BILL_TO);
    	

    	// GCSTORE-2517 Start

    	String strBillToMissingParameters ="";
    if(YFCCommon.equals(GCConstants.SALES_ORDER_DOCUMENT_TYPE, docType)){	
    	if(YFCCommon.equals(sExtnOrderCaptureChannel,GCConstants.CALL_CENTER_ORDER_CHANNEL) || YFCCommon.equals(sExtnOrderCaptureChannel,GCConstants.GCSTORE))
    	{
      	  YFCElement elePersonInfoShipTo = yfcEleOrder.getChildElement(GCConstants.PERSON_INFO_SHIP_TO);
      	  LOGGER.verbose("elePersonInfoShipTo\n" + elePersonInfoShipTo);
      	  if(!YFCCommon.isVoid(elePersonInfoShipTo)){
      		  
  			try {
  				GCCommonUtil.stampApoFpo(elePersonInfoShipTo, newInDoc);
  				GCCommonUtil.stampPOBox(elePersonInfoShipTo, newInDoc);
  			} catch (GCException e) {
  				LOGGER.error(" Inside catch block of the value for StampApoFpo and StampPOBox, Exception is :", e);
  				e.printStackTrace();
  			}
      	  }
    	  //GCSTORE-6137 Start
    	 /* YFCElement elePersonInfoShipTo = yfcEleOrder.getChildElement(GCConstants.PERSON_INFO_SHIP_TO);
    	  if(!YFCCommon.isVoid(elePersonInfoShipTo)){
    	      String sCommercialAddress = elePersonInfoShipTo.getAttribute(GCConstants.IS_COMMERCIAL_ADDRESS);
    	      if(YFCCommon.isStringVoid(sCommercialAddress)){
    	        elePersonInfoShipTo.setAttribute(GCConstants.IS_COMMERCIAL_ADDRESS, GCConstants.N);
    	      }
    	      LOGGER.verbose("\n sExtnFulfillmentTypeFromGetOrderList" + sExtnFulfillmentTypeFromGetOrdList);
              if (!YFCCommon.isStringVoid(sExtnFulfillmentTypeFromGetOrdList)) {
                if (!YFCCommon.isStringVoid(elePersonInfoShipTo.getAttribute(GCConstants.COMPANY))
                    || sExtnFulfillmentTypeFromGetOrdList.indexOf(GCConstants.PICKUP_IN_STORE) >= 0) {
                  elePersonInfoShipTo.setAttribute(GCConstants.IS_COMMERCIAL_ADDRESS, GCConstants.YES);
                }
              }
            } */
    	  //GCSTORE-6137 End
    		if (!YFCCommon.isVoid(elePersonBillTo)) {
    			//GCSTORE-6945: GC Enchancement Phase 2
    			List<String> countryWithoutStates = new ArrayList<String> ();
    			YFCDocument commonCodeOutput =
    			        YFCDocument.getDocumentFor(GCCommonUtil.getCommonCodeListByType(env, "COUNTRY_WO_STATES", GCConstants.GC));
    			    YFCElement eleCommonCodeList = commonCodeOutput.getDocumentElement();
    		     if (eleCommonCodeList.hasChildNodes()) {
    		         YFCNodeList<YFCElement> nlCommonCode = eleCommonCodeList.getElementsByTagName(GCConstants.COMMON_CODE);
    		         for (YFCElement eleCommonCode : nlCommonCode) {
    		        	 countryWithoutStates.add(eleCommonCode.getAttribute(GCConstants.CODE_VALUE));
    		         }
    		       }
    		     String strAdditionalNote = "";
    		    //GCSTORE-6945: GC Enchancement Phase 2
    			if (YFCCommon.isVoid(elePersonBillTo
    					.getAttribute(GCConstants.FIRST_NAME))) {
    				strBillToMissingParameters= strBillToMissingParameters+" First name";

    			} if (YFCCommon.isVoid(elePersonBillTo
    					.getAttribute(GCConstants.LAST_NAME))) {
    				if(YFCCommon.isStringVoid(strBillToMissingParameters))
    				{
    					strBillToMissingParameters= strBillToMissingParameters+" Last name";
    				}
    				else
    				{
    					strBillToMissingParameters= strBillToMissingParameters+", Last name";
    				}
    			}
    			if (YFCCommon.isVoid(elePersonBillTo
    					.getAttribute(GCConstants.EMAIL_ID))) {
    				if(YFCCommon.isStringVoid(strBillToMissingParameters))
    				{
    					strBillToMissingParameters= strBillToMissingParameters+" Email address";
    				}
    				else
    				{
    					strBillToMissingParameters= strBillToMissingParameters+", Email address";
    				}
    			}
    			if (YFCCommon.isVoid(elePersonBillTo
    					.getAttribute(GCConstants.ADDRESS_LINE_1))) {
    				if(YFCCommon.isStringVoid(strBillToMissingParameters))
    				{
    					strBillToMissingParameters= strBillToMissingParameters+" Address Line 1";
    				}
    				else
    				{
    					strBillToMissingParameters= strBillToMissingParameters+", Address Line 1";
    				}
    			}
    			if (YFCCommon.isVoid(elePersonBillTo
    					.getAttribute(GCConstants.CITY))) {
    				if(YFCCommon.isStringVoid(strBillToMissingParameters))
    				{
    					strBillToMissingParameters= strBillToMissingParameters+" City";
    				}
    				else
    				{
    					strBillToMissingParameters= strBillToMissingParameters+", City";
    				}
    			}
    			
    			//GCSTORE-6945
    			if(!(countryWithoutStates.contains(elePersonBillTo.getAttribute(GCConstants.COUNTRY)))){
	    			if (YFCCommon.isVoid(elePersonBillTo
	    					.getAttribute(GCConstants.STATE))) {
	    				if(YFCCommon.isStringVoid(strBillToMissingParameters))
	    				{
	    					strBillToMissingParameters= strBillToMissingParameters+" State";
	    					strAdditionalNote = strAdditionalNote + "If the selected country does not have State, please enter one of the following County/Province/Prefecture/Republic/Union Territory";
	    				}
	    				else
	    				{
	    					strBillToMissingParameters= strBillToMissingParameters+", State";
	    					strAdditionalNote = strAdditionalNote + "If the selected country does not have State, please enter one of the following County/Province/Prefecture/Republic/Union Territory";	    					
	    				}
	    			}
    			}
    			if (YFCCommon.isVoid(elePersonBillTo
    					.getAttribute(GCConstants.ZIP_CODE))) {
    				if(YFCCommon.isStringVoid(strBillToMissingParameters))
    				{
    					strBillToMissingParameters= strBillToMissingParameters+" ZipCode";
    				}
    				else
    				{
    					strBillToMissingParameters= strBillToMissingParameters+", ZipCode";
    				}
    			}
    			if (YFCCommon.isVoid(elePersonBillTo
    					.getAttribute(GCConstants.COUNTRY))) {
    				if(YFCCommon.isStringVoid(strBillToMissingParameters))
    				{
    					strBillToMissingParameters= strBillToMissingParameters+" Country";
    				}
    				else
    				{
    					strBillToMissingParameters= strBillToMissingParameters+", Country";
    				}
    			}
    			if(!YFCCommon.isStringVoid(strBillToMissingParameters)){
    				YFCException yfs = new YFCException(
    						GCErrorConstants.MANDATORY_BILLTO_ATTRIBUTES_MISSING);
    				LOGGER.debug("6301 Bill to mandatory attributes are missing");
    				yfs.setErrorDescription("Mandatory attributes "+strBillToMissingParameters+" are missing in Bill To address."+strAdditionalNote);
    				throw yfs;
    			}
    		}
    	}
       }
      }
    }
    LOGGER.debug("6301 end");
    // GCSTORE - 6301 End

        
    YFCNodeList<YFCElement> listOrderLine = orderDetailsDoc.getElementsByTagName(GCConstants.ORDER_LINE);
    int lengthOrderLine = listOrderLine.getLength();
    // Commenting below line since this attribute is not required. Fix as part of code merge.
    // YFCElement elenewExtn = elenewOrder.getChildElement(GCConstants.EXTN);
    String sOldFulfillmentType = elenewExtn
        .getAttribute(GCConstants.EXTN_FULFILLMENT_TYPE);
    String sExtnOrderCaptureChannel=elenewExtn
        .getAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL);
    String sExtnPickupStoreID = yfcEleOrder
        .getAttribute(GCConstants.EXTN_PICK_UP_STORE_ID);

    String newLine1 = "Guitar Center " + sExtnPickupStoreID;

    String sIsStoreUser = yfcEleOrder
        .getAttribute(GCConstants.IS_STORE_USER);
    YFCElement eleExtn = yfcEleOrder.getChildElement(GCConstants.EXTN, true);

    String sEntryType = yfcEleOrder.getAttribute(GCConstants.ENTRY_TYPE);
    if ((YFCCommon.equals(GCConstants.FLAG_Y, sIsStoreUser) || YFCCommon.equals("true", sIsStoreUser))
        && lengthOrderLine == 0) {
      if (YFCCommon.isVoid(sOldFulfillmentType)) {
        eleExtn.setAttribute(GCConstants.EXTN_FULFILLMENT_TYPE,
            GCConstants.PICKUP_IN_STORE);
      }

      YFCDocument outDoc = null;

      YFCDocument inputDoc = YFCDocument
          .createDocument(GCConstants.SHIP_NODE);
      YFCElement eleShipNode = inputDoc.getDocumentElement();
      YFCElement eleOrganization = eleShipNode
          .createChild(GCConstants.ORGANIZATION);
      eleOrganization.setAttribute(GCConstants.ORGANIZATION_CODE,
          sExtnPickupStoreID);
      YFCDocument templateDoc = YFCDocument
          .createDocument(GCXmlLiterals.SHIP_NODE_LIST);
      YFCElement eletemproot = templateDoc.getDocumentElement();
      YFCElement eletempShipNode = eletemproot
          .createChild(GCConstants.SHIP_NODE);
      YFCElement eletempPersonInfo = eletempShipNode
          .createChild(GCXmlLiterals.SHIP_NODE_PERSON_INFO);
      eletempShipNode.appendChild(eletempPersonInfo);
      outDoc = GCCommonUtil.invokeAPI(env,
          GCConstants.GET_SHIP_NODE_LIST, inputDoc, templateDoc);

      YFCElement eleShipNodelist = outDoc.getDocumentElement();
      YFCElement eleoutShipNode = eleShipNodelist
          .getChildElement(GCConstants.SHIP_NODE);

      YFCElement eleShipNodePersonInfo = eleoutShipNode
          .getChildElement(GCXmlLiterals.SHIP_NODE_PERSON_INFO);
      String sAddress1 = eleShipNodePersonInfo
          .getAttribute(GCConstants.ADDRESS_LINE1);
      String sAddress2 = eleShipNodePersonInfo
          .getAttribute(GCConstants.ADDRESS_LINE2);
      String sAddress3 = eleShipNodePersonInfo
          .getAttribute(GCConstants.ADDRESS_LINE3);
      String sCity = eleShipNodePersonInfo.getAttribute(GCConstants.CITY);
      String sState = eleShipNodePersonInfo
          .getAttribute(GCConstants.STATE);
      String sZipCode = eleShipNodePersonInfo
          .getAttribute(GCConstants.ZIP_CODE);
      String sCountry = eleShipNodePersonInfo
          .getAttribute(GCConstants.COUNTRY);
      YFCElement elePersonInfoShipTo = yfcEleOrder
          .getChildElement(GCConstants.PERSON_INFO_SHIP_TO);
      if (YFCCommon.isVoid(elePersonInfoShipTo)) {
        elePersonInfoShipTo = yfcEleOrder
            .createChild(GCConstants.PERSON_INFO_SHIP_TO);
      }
      // fix for Defect-3174
      elePersonInfoShipTo.setAttribute(GCConstants.ADDRESS_LINE1,
          sAddress1);
      elePersonInfoShipTo.setAttribute(GCConstants.ADDRESS_LINE2,
          sAddress2);
      elePersonInfoShipTo.setAttribute(GCConstants.ADDRESS_LINE3,
          sAddress3);
      elePersonInfoShipTo.setAttribute(GCConstants.COMPANY, newLine1);
      elePersonInfoShipTo.setAttribute(GCConstants.CITY, sCity);
      elePersonInfoShipTo.setAttribute(GCConstants.STATE, sState);
      elePersonInfoShipTo.setAttribute(GCConstants.ZIP_CODE, sZipCode);
      elePersonInfoShipTo.setAttribute(GCConstants.COUNTRY, sCountry);
      YFCElement elePersonInfoShipToExtn = elePersonInfoShipTo.getChildElement(GCConstants.EXTN);
      if(!YFCCommon.isVoid(elePersonInfoShipToExtn)){
        elePersonInfoShipTo.setAttribute(GCConstants.EXTN_IS_APO_FPO, GCConstants.NO);
        elePersonInfoShipTo.setAttribute(GCConstants.EXTN_IS_PO_BOX, GCConstants.NO);
      }
      YFCElement elePersonInfoBillTo = yfcEleOrder
          .getChildElement(GCConstants.PERSON_INFO_BILL_TO);
      // GCSTORE-2517 Start
      if (!YFCCommon.isVoid(elePersonInfoBillTo)) {
        String sFirstName = elePersonInfoBillTo
            .getAttribute(GCConstants.FIRST_NAME);
        String sLastname = elePersonInfoBillTo
            .getAttribute(GCConstants.LAST_NAME);
        elePersonInfoShipTo
        .setAttribute(GCConstants.FIRST_NAME, sFirstName);
        elePersonInfoShipTo.setAttribute(GCConstants.LAST_NAME, sLastname);
      }
      // GCSTORE-2517 End
    } else if (YFCCommon.isVoid(sOldFulfillmentType) && !YFCCommon.equals(sEntryType, GCConstants.MPOS)) {
      eleExtn.setAttribute(GCConstants.EXTN_FULFILLMENT_TYPE,
          GCConstants.SHIP_2_CUSTOMER);

    }

    if (!YFCCommon.isVoid(eleExtn)) {
      String extnShipNode = eleExtn
          .getAttribute(GCXmlLiterals.EXTN_SHIP_NODE);
      if (!YFCCommon.isVoid(extnShipNode)) {
        eleExtn.setAttribute(GCConstants.EXTN_PICK_UP_STORE_ID, extnShipNode);
      }
    }
    // GC-Store -443

    //GCSTORE-4281 - Begin
    String stampShipToAddress = yfcEleOrder.getAttribute("stampShipToAddress");
    if((YFCCommon.equals(GCConstants.FLAG_Y, sIsStoreUser) || YFCCommon.equals("true", sIsStoreUser)) && YFCCommon.equals(stampShipToAddress, "Y")){

      if (YFCCommon.isVoid(sOldFulfillmentType)) {
        eleExtn.setAttribute(GCConstants.EXTN_FULFILLMENT_TYPE,
            GCConstants.PICKUP_IN_STORE);
      }

      YFCDocument outDoc = null;

      YFCDocument inputDoc = YFCDocument
          .createDocument(GCConstants.SHIP_NODE);
      YFCElement eleShipNode = inputDoc.getDocumentElement();
      YFCElement eleOrganization = eleShipNode
          .createChild(GCConstants.ORGANIZATION);
      eleOrganization.setAttribute(GCConstants.ORGANIZATION_CODE,
          sExtnPickupStoreID);
      YFCDocument templateDoc = YFCDocument
          .createDocument(GCXmlLiterals.SHIP_NODE_LIST);
      YFCElement eletemproot = templateDoc.getDocumentElement();
      YFCElement eletempShipNode = eletemproot
          .createChild(GCConstants.SHIP_NODE);
      YFCElement eletempPersonInfo = eletempShipNode
          .createChild(GCXmlLiterals.SHIP_NODE_PERSON_INFO);
      eletempShipNode.appendChild(eletempPersonInfo);
      outDoc = GCCommonUtil.invokeAPI(env,
          GCConstants.GET_SHIP_NODE_LIST, inputDoc, templateDoc);

      YFCElement eleShipNodelist = outDoc.getDocumentElement();
      YFCElement eleoutShipNode = eleShipNodelist
          .getChildElement(GCConstants.SHIP_NODE);

      YFCElement eleShipNodePersonInfo = eleoutShipNode
          .getChildElement(GCXmlLiterals.SHIP_NODE_PERSON_INFO);
      String sAddress1 = eleShipNodePersonInfo
          .getAttribute(GCConstants.ADDRESS_LINE1);
      String sAddress2 = eleShipNodePersonInfo
          .getAttribute(GCConstants.ADDRESS_LINE2);
      String sAddress3 = eleShipNodePersonInfo
          .getAttribute(GCConstants.ADDRESS_LINE3);
      String sCity = eleShipNodePersonInfo.getAttribute(GCConstants.CITY);
      String sState = eleShipNodePersonInfo
          .getAttribute(GCConstants.STATE);
      String sZipCode = eleShipNodePersonInfo
          .getAttribute(GCConstants.ZIP_CODE);
      String sCountry = eleShipNodePersonInfo
          .getAttribute(GCConstants.COUNTRY);
      YFCElement elePersonInfoShipTo = yfcEleOrder
          .getChildElement(GCConstants.PERSON_INFO_SHIP_TO);
      if (YFCCommon.isVoid(elePersonInfoShipTo)) {
        elePersonInfoShipTo = yfcEleOrder
            .createChild(GCConstants.PERSON_INFO_SHIP_TO);
      }
      // fix for Defect-3174
      elePersonInfoShipTo.setAttribute(GCConstants.ADDRESS_LINE1,
          sAddress1);
      elePersonInfoShipTo.setAttribute(GCConstants.ADDRESS_LINE2,
          sAddress2);
      elePersonInfoShipTo.setAttribute(GCConstants.ADDRESS_LINE3,
          sAddress3);

      elePersonInfoShipTo.setAttribute(GCConstants.CITY, sCity);
      elePersonInfoShipTo.setAttribute(GCConstants.STATE, sState);
      elePersonInfoShipTo.setAttribute(GCConstants.ZIP_CODE, sZipCode);
      elePersonInfoShipTo.setAttribute(GCConstants.COUNTRY, sCountry);
      YFCElement elePersonInfoShipToExtn = elePersonInfoShipTo.getChildElement(GCConstants.EXTN);
      if(!YFCCommon.isVoid(elePersonInfoShipToExtn)){
        elePersonInfoShipTo.setAttribute(GCConstants.EXTN_IS_APO_FPO, GCConstants.NO);
        elePersonInfoShipTo.setAttribute(GCConstants.EXTN_IS_PO_BOX, GCConstants.NO);
      }
      YFCElement elePersonInfoBillTo = yfcEleOrder
          .getChildElement(GCConstants.PERSON_INFO_BILL_TO);
      // GCSTORE-2517 Start
      if (!YFCCommon.isVoid(elePersonInfoBillTo)) {
        String sFirstName = elePersonInfoBillTo
            .getAttribute(GCConstants.FIRST_NAME);
        String sLastname = elePersonInfoBillTo
            .getAttribute(GCConstants.LAST_NAME);
        elePersonInfoShipTo
        .setAttribute(GCConstants.FIRST_NAME, sFirstName);
        elePersonInfoShipTo.setAttribute(GCConstants.LAST_NAME, sLastname);
      }
      // GCSTORE-2517 End

    }
    //GCSTORE-4281 - End


    // GCSTORE-788 Start
    String sOldOrderingStore = elenewExtn
        .getAttribute(GCConstants.EXTN_ORDER_ORDERING_STORE);
    // New ordering store
    String sNewOrderingStore = eleExtn
        .getAttribute(GCConstants.EXTN_ORDER_ORDERING_STORE);
    // Check if parent screen is Customer Identification screen
    String sParentScreen = eleExtn
        .getAttribute(GCConstants.EXTN_PARENT_SCREEN);
    // Check if the order does not have an existing ordering store and
    // parent screen is customer identification
    if (YFCCommon.isVoid(sOldOrderingStore)
        && YFCCommon.equals(GCConstants.EXTN_CUSTOMER_SCREEN,
            sParentScreen)) {
      eleExtn.setAttribute(GCConstants.EXTN_ORDER_ORDERING_STORE,
          sNewOrderingStore);
    } else if (!YFCCommon.isVoid(sOldOrderingStore)
        && YFCCommon.equals(GCConstants.EXTN_CUSTOMER_SCREEN,
            sParentScreen)) {
      eleExtn.setAttribute(GCConstants.EXTN_ORDER_ORDERING_STORE,
          sOldOrderingStore);
    }
    // GCSTORE-788 End
    // --GCSTORE-328 & 513 Start
    String sStampTaxExemptFlag = eleRoot
        .getAttribute(GCXmlLiterals.STAMP_TAX_EXEMPT_FLAG);
    if (!YFCCommon.isVoid(sStampTaxExemptFlag)
        && YFCCommon.equals(sStampTaxExemptFlag, GCConstants.FLAG_Y)) {
      stampOrderHeaderAttributes(env, inDoc,orderDetailsDoc);
      eleRoot.removeAttribute(sStampTaxExemptFlag);
    }
    // --GCSTORE-328 & 513 End

    /* Stamp fulfillment for OrderLine: begin */
    // Stamp Product Class: Start
    try {
      NodeList nlItem = XPathAPI.selectNodeList(inDoc,
          "Order/OrderLines/OrderLine/Item");
      for (int iCounter = 0; iCounter < nlItem.getLength(); iCounter++) {
        Element eleItem = (Element) nlItem.item(iCounter);
        String sProductClass = eleItem
            .getAttribute(GCConstants.PRODUCT_CLASS);
        if (YFCCommon.isVoid(sProductClass)) {
          String sExtnSetCode = GCBeforeCreateOrderUE
              .checkForTrackedAsSet(env, eleItem, GCConstants.GCI);
          if (GCConstants.ONE.equals(sExtnSetCode)) {
            eleItem.setAttribute(GCConstants.PRODUCT_CLASS,
                GCConstants.SET);
          } else if (YFCCommon.isVoid(sExtnSetCode)
              || GCConstants.ZERO.equals(sExtnSetCode)) {
            eleItem.setAttribute(GCConstants.PRODUCT_CLASS,
                GCConstants.REGULAR);
          }
        }
      }
    } catch (Exception ex) {
      LOGGER.error(logInfo, ex);
      YFSUserExitException e = new YFSUserExitException();
      e.setErrorDescription(ex.getMessage());
      throw e;
    }
    // Stamp Product Class: End

    // Update fulfillment type, predefined node and sourcing classification
    // starts
    YFCDocument changeOrderUEDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement changeOrderRootEle = changeOrderUEDoc.getDocumentElement();
    if (YFCCommon.equals(
        changeOrderRootEle.getAttribute("UpdateSourcingDetails"), "Y",
        false)) {
      YFCElement orderDtlsEle = fetchOrderDetails(env,
          changeOrderRootEle.getAttribute("OrderHeaderKey"));
      GCOrderUtil.stampSourcingClassification(env, changeOrderUEDoc,
          orderDtlsEle);
      GCOrderUtil.stampOrderLineAttributes(env, changeOrderUEDoc,
          orderDtlsEle);
      changeOrderRootEle.removeAttribute("UpdateSourcingDetails");
    }

    if (YFCCommon
        .equals(changeOrderRootEle
            .getAttribute("UpdateSourcingClassification"), "Y",
            false)) {
      YFCElement orderDtlsEle = fetchOrderDetails(env,
          changeOrderRootEle.getAttribute("OrderHeaderKey"));
      GCOrderUtil.stampSourcingClassification(env, changeOrderUEDoc,
          orderDtlsEle);
      changeOrderRootEle.removeAttribute("UpdateSourcingClassification");
    }

    // Udpate fulfillment type, predefined node and sourcing classification
    // ends
    // Update EarliestScheduleDate starts
    if (YFCCommon.equals(
        changeOrderRootEle.getAttribute("UpdateEarliestScheduleDate"),
        "Y", false)) {
      updateEarliestScheduleDate(env,changeOrderUEDoc);
      changeOrderRootEle.removeAttribute("UpdateEarliestScheduleDate");
    }
    // Update EarliestScheduleDate ends
    // Update SourcingClassification="PREPAID" when order amendment is done
    // to add Prepaid type - starts
    if (YFCCommon.equals(changeOrderRootEle
        .getAttribute("UpdatePrepaidSourcingClassification"), "Y",
        false)) {
      YFCElement orderDtlsEle = fetchOrderDetails(env,
          changeOrderRootEle.getAttribute("OrderHeaderKey"));
      GCOrderUtil.updatePrepaidSourcing(env, orderDtlsEle,
          changeOrderUEDoc);
      changeOrderRootEle
      .removeAttribute("UpdatePrepaidSourcingClassification");
    }
    // Update SourcingClassification="PREPAID" when order amendment is done
    // to add Prepaid type - ends


    // GC-Phase2::End


    // WCC carrier service code: Begin
    String sApplyCarrierServiceCode = eleRoot
        .getAttribute(GCConstants.APPLY_CARRIER_SERVICE_CODE);
    if (GCConstants.FLAG_Y.equalsIgnoreCase(sApplyCarrierServiceCode)) {
      try {
        // remove item details element from indoc as its only used in
        // carrier service code module and its not part of standard
        // change order
        String sLevelOfService = eleRoot
            .getAttribute(GCConstants.LEVEL_OF_SERVICE);
        Document inDocGetCommonCode = GCXMLUtil.getDocument("<CommonCode CodeType='GCExpediteLOS'/>");
        Document outDocGetCommonCode =
            GCCommonUtil.invokeAPI(env, GCConstants.GET_COMMON_CODE_LIST, inDocGetCommonCode);
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("The output of getComonCodeList" + GCXMLUtil.getXMLString(outDocGetCommonCode));
        }
        Element eleCommonCode =
            GCXMLUtil.getElementByXPath(outDocGetCommonCode, "//CommonCodeList/CommonCode[@CodeLongDescription='"
                + sLevelOfService + "']");
        if (!YFCCommon.isVoid(eleCommonCode))
        {

          YFCElement eleExtnInOrder=yfcEleOrder.getChildElement(GCConstants.EXTN);
          eleExtnInOrder.setAttribute(GCConstants.EXTN_EXPEDITE_FLAG, GCConstants.YES);

        }
        NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc,
            "//OrderLine");
        for (int i = 0; i < nlOrderLine.getLength(); i++) {
          Element eleOrderLine = (Element) nlOrderLine.item(i);
          Element eleItemDetails = (Element) eleOrderLine
              .getElementsByTagName(GCConstants.ITEM_DETAILS)
              .item(0);
          eleOrderLine.removeChild(eleItemDetails);
        }
      } catch (Exception ex) {
        LOGGER.error(logInfo , ex);
        YFSUserExitException e = new YFSUserExitException();
        e.setErrorDescription(ex.getMessage());
        throw e;
      }
    }
    //Phase 2 changes Order Fulfillment
    /*
     * Removed code to stamp Carrier service code*/

    try {
      String sSellerOrgCode = inDoc.getDocumentElement().getAttribute(
          GCConstants.SELLER_ORGANIZATION_CODE);
      if (!GCConstants.GC.equalsIgnoreCase(sSellerOrgCode)) {
        Element elePersonInfoShipTo = GCXMLUtil.getElementByXPath(
            inDoc, "Order/OrderLines/OrderLine/PersonInfoShipTo");
        env.setTxnObject("PersonInfoShipTo", elePersonInfoShipTo);
      }

      // Shipping Logic
      String sExtnIsShippingChargeProrated = GCXMLUtil
          .getAttributeFromXPath(inDoc, "Order/Extn"
              + "/@ExtnIsShippingChargeProrated");
      //OMS-4635 Starts
      if(GCConstants.YES.equalsIgnoreCase(sIsProrationRequired)){
        addCharges(inDoc);
      }
      if (GCConstants.NO.equalsIgnoreCase(sExtnIsShippingChargeProrated)){
        if(!YFCCommon.equals(sSkipShippingChargeForPLCC,
            GCConstants.FLAG_Y)) {
          GCCommonUtil.invokeService(env,
              GCConstants.GC_PROCESS_SHPG_PRORATION_SERVICE, inDoc);
        }
        if (!YFCCommon.equalsIgnoreCase(sEntryType, "mPOS")) {
          LOGGER.verbose("Tax calculation for Shipping");
          GCProcessTaxCalculationsAPI gcProcessTaxCalculationsAPI = new GCProcessTaxCalculationsAPI();
          inDoc = gcProcessTaxCalculationsAPI.processTaxCalculations(env,
              inDoc);
        }
        Element extnEle = GCXMLUtil.getElementByXPath(
            inDoc, "Order/Extn");
        if(!YFCCommon.isVoid(extnEle)){
          extnEle.setAttribute("ExtnIsShippingChargeProrated", "Y");
        }
      }

      if (GCConstants.RETURN_ORDER_DOCUMENT_TYPE.equals(sDocumentType)) {
        String sOrderHeaderKeyEnv = sOrderHeaderKey + "_"
            + sDocumentType;
        String sSalesOrderHeaderKey = GCXMLUtil
            .getAttributeFromXPath(inDoc,
                "Order/OrderLines/OrderLine/DerivedFrom/@OrderHeaderKey");
        if (YFCCommon.isVoid(sSalesOrderHeaderKey)) {
          sSalesOrderHeaderKey = GCXMLUtil
              .getAttributeFromXPath(inDoc,
                  "Order/OrderLines/OrderLine/@DerivedFromOrderHeaderKey");
        }
        if (!YFCCommon.isVoid(sSalesOrderHeaderKey)
            && YFCCommon.isVoid(env
                .getTxnObject(sOrderHeaderKeyEnv))) {
          Document docGetOrderListIP = GCXMLUtil
              .createDocument(GCConstants.ORDER);
          docGetOrderListIP.getDocumentElement().setAttribute(
              GCConstants.ORDER_HEADER_KEY, sSalesOrderHeaderKey);
          Document docGetOrderListTmp = GCCommonUtil
              .getDocumentFromPath("/global/template/api/getOrderList.xml");
          Document docGetOrderListOutput = GCCommonUtil.invokeAPI(
              env, GCConstants.API_GET_ORDER_LIST,
              docGetOrderListIP, docGetOrderListTmp);
          env.setTxnObject(sOrderHeaderKeyEnv, docGetOrderListOutput);
        }
      }
    } catch (Exception ex) {
      LOGGER.error(logInfo, ex);
      YFSUserExitException e = new YFSUserExitException();
      e.setErrorDescription(ex.getMessage());
    }
    // WCC carrier service code: End

    // Check if ATG call is required
    /* GCATGUtil obj = new GCATGUtil();
    try {
      if (!YFCCommon.isVoid(sOrderHeaderKey)) {
        obj.checkIfATGCallRequired(inDoc, docGetOrderListOP);
      }
    } catch (Exception ex) {
      LOGGER.error(logInfo, ex);
      YFSUserExitException e = new YFSUserExitException();
      e.setErrorDescription(ex.getMessage());
      throw e;
    }*/
    // ATG call - End
    /* GCSTORE - 557 - Begin */
    String sIsReqShipDateChange = eleRoot.getAttribute("IsReqShipDateChange");
    String sIsPlatinumAdded = eleRoot.getAttribute("IsPlatinumAdded");
    if (YFCCommon.equals(GCConstants.FLAG_Y, sIsReqShipDateChange)
        || YFCCommon.equals(GCConstants.FLAG_Y, sIsPlatinumAdded)) {
      singleSourceItemValidationOnOrder(inDoc, sIsReqShipDateChange);
    }
    /* GCSTORE - 557 - End */

    //GCSTORE-2805 START
    String sPlatinumSerializedCheck = eleRoot.getAttribute("PlatinumSerializedCheck");
    if(YFCCommon.equals(GCConstants.FLAG_Y, sPlatinumSerializedCheck))
    {
      addressCheckForPlatinumItem(env,inDoc);
      eleRoot.removeAttribute(sPlatinumSerializedCheck);
    }
    //GCSTORE-2805 END

    // OMS - 4026 - Cancel warranty items when parent items are cancelled.
    try {
      cancelWarrantyItemIfParentIsDeleted(inDoc);

      LOGGER.debug(logInfo + "Output Doc\n"
          + GCXMLUtil.getXMLString(inDoc));

      eleRoot.removeAttribute(GCConstants.IS_UPDATE_COMMISSION);
      eleRoot.removeAttribute("CallReservationService");
      eleRoot.removeAttribute(GCConstants.IS_ORDER_LEVEL);
      eleRoot.removeAttribute(GCConstants.OVERRIDE_ORDER_LINE_COMMISSION);
      env.clearApiTemplate("modifyFulfillmentOptions");
      env.setApiTemplate("modifyFulfillmentOptions", modifyfulfillmentDoc);
      LOGGER.verbose(logInfo + "End");

      // Enabling parent to add warranty again
      inDoc = modifyInDoc(env, inDoc);
      // Apply Hold on Warranty Line
      // Fixes for GCSTORE-2569 & 2405 start
      YFCElement orderEle = YFCDocument.getDocumentFor(inDoc).getDocumentElement();
      String isChangeOrderForAmendAfterPymnt = orderEle.getAttribute("IsOrderAmendAfterPayment");
      String sOrderHeaderKeyForOrderSplit = orderEle.getAttribute(GCConstants.ORDER_HEADER_KEY);
      if (YFCCommon.equalsIgnoreCase(GCConstants.YES, isChangeOrderForAmendAfterPymnt)) {
        YFCDocument orderDetailsDocOnOrderAmend =
            getOrderDetailsForSplitOnOrderAmend(env, sOrderHeaderKeyForOrderSplit);
        inDoc = orderDetailsDocOnOrderAmend.getDocument();
        orderDetailsDocOnOrderAmend.getDocumentElement().setAttribute("DoNotCallChangeOrder", "Y");
        // call SplitOrderLineService.. update SplitOrderLineService to not call ChangeOrder when
        // above mentioned flag is already there. Return split document
        new GCSplitOrderLineService().splitOrderLinesService(env, orderDetailsDocOnOrderAmend.getDocument());
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("Order details after split\n" + orderDetailsDocOnOrderAmend);
        }
        YFCElement orderDetailsEle = orderDetailsDocOnOrderAmend.getDocumentElement();
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("Order details output\n" + orderDetailsDocOnOrderAmend);
        }

        new GCDependentLineHold().processOrderToStampDependentLineHold(env, orderDetailsEle, orderDetailsEle);
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("Order details after stamping dependent line hold\n" + orderDetailsDocOnOrderAmend);
        }
        stampShipNodeForPISOrder(env,orderDetailsEle);

        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("Order details after stamping ShipNode for PIS order\n" + orderDetailsDocOnOrderAmend);
        }
      }
      // Fixes for GCSTORE-2569 & 2405 end
      // Code for OMS-4570 begins:
      GCManageCommission gcManageCommission = new GCManageCommission();
      gcManageCommission.updateOrderLineCommissions(inDoc);
      // Code for OMS-4570 ends
      // OMS-4977: Begin
      stampPriceOverrideReasonCode(inDoc);
      // OMS-4977: End
      //GCSTORE-6877: Begin
      freeGiftItemCancellation(env, inDoc);
      //GCSTORE-6877: End
    } catch (Exception ex) {
      LOGGER.error(logInfo, ex);
      YFSUserExitException e = new YFSUserExitException();
      e.setErrorDescription(ex.getMessage());
      throw e;
    }
    // GCSTORE-2909 & 2910::Begin
    removeBundleFulfillmentMode(inDoc);
    // GCSTORE-2909 & 2910::End

    // GCSTORE-2895 Begin
    inDoc.getDocumentElement().setAttribute(GCConstants.OVERRIDE, GCConstants.FLAG_Y);
    // GCSTORE-2895 End
    // GCSTORE-3413- Begin
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleOrder = inputDoc.getDocumentElement();
    if (YFCCommon.equalsIgnoreCase(GCConstants.YES, isOdrAmendAftrPymnt)) {
      removeReservationElement(eleOrder);
    }
    // GCSTORE-3413- End

    //GCSTORE-4091
    YFCDocument yfcInDocument = YFCDocument.getDocumentFor(inDoc);
    YFCElement yfcEleRoot = yfcInDocument.getDocumentElement();
    YFCElement eleRootOls = yfcEleRoot.getChildElement("OrderLines", true);

    String sStampUnitCost = yfcEleRoot.getAttribute("StampUnitCost");
    if(YFCCommon.equals(sStampUnitCost, "Y")){
      YFCDocument docGOL = YFCDocument.getDocumentFor(docGetOrderListOP);
      YFCNodeList<YFCElement> nlOrderLines = docGOL.getElementsByTagName("OrderLine");
      for(YFCElement eleGOLs : nlOrderLines){
        String sOLK = eleGOLs.getAttribute("OrderLineKey");
        YFCElement eleItemDetails = eleGOLs.getChildElement(GCConstants.ITEM_DETAILS);
        YFCElement eleItemExtn = eleItemDetails.getChildElement(GCConstants.EXTN);
        String sInventoryCost = eleItemExtn.getAttribute("ExtnInventoryCost");
        if(YFCCommon.isVoid(sInventoryCost)){
          YFCElement elePrimaryInformation = eleItemDetails.getChildElement("PrimaryInformation");
          sInventoryCost = elePrimaryInformation.getAttribute("UnitCost");
        }
        YFCElement eleRootOl = eleRootOls.createChild(GCConstants.ORDER_LINE);
        eleRootOl.setAttribute(GCConstants.ORDER_LINE_KEY, sOLK);
        YFCElement eleRootOlExtn = eleRootOl.createChild("Extn");
        eleRootOlExtn.setAttribute("ExtnInventoryCost", sInventoryCost);
      }
    }
    //GCSTORE_4091
    //GCSTORE-7165--Start
    String toStampItemFlag = eleRoot.getAttribute("StampRestrictShipFromStore");
    if(GCConstants.YES.equalsIgnoreCase(toStampItemFlag)){
    setExtnRestrictShipToStoreAtOrderLine(env,inDoc,logInfo);
    }
    //GCSTORE-7165--End

    LOGGER.debug(logInfo +"Outgoing doc \n"
        + GCXMLUtil.getXMLString(inDoc));
    LOGGER.verbose(logInfo + "End");

    return inDoc;

  }




//GCSTORE-7165--Start
private void setExtnRestrictShipToStoreAtOrderLine(YFSEnvironment env,Document inDoc,String logInfo) {
    try {
    	
		NodeList nOrderLinesForFlag = XPathAPI.selectNodeList(inDoc, "Order/OrderLines/OrderLine");
		for(int count = 0;count<nOrderLinesForFlag.getLength();count++){
			Element eleOrderLine = (Element) nOrderLinesForFlag.item(count);
			Element eleItem = (Element) eleOrderLine.getElementsByTagName(GCConstants.ITEM).item(0);
			if(!YFCCommon.isVoid(eleItem)){
			String itemID = eleItem.getAttribute(GCConstants.ITEM_ID);
			YFCDocument inDocForItem = YFCDocument.createDocument(GCConstants.ITEM);
			inDocForItem.getDocumentElement().setAttribute(GCConstants.ITEM_ID, itemID);
			 LOGGER.debug(logInfo + "Input document for getItemList"
				        + GCXMLUtil.getXMLString(inDocForItem.getDocument()));
			YFCDocument getItemTemplate = YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LIST_API_TMPL_FOR_RESTRICT_SHIP_TO_STORE);
		      YFCDocument getItemListOpDoc =
			          GCCommonUtil.invokeAPI(env, GCConstants.GET_ITEM_LIST, inDocForItem, getItemTemplate);
			LOGGER.debug(logInfo + "Output document for getItemList"
			        + GCXMLUtil.getXMLString(getItemListOpDoc.getDocument()));
			if(!YFCDocument.isVoid(getItemListOpDoc)){
			YFCElement eleItemExtn = (YFCElement) getItemListOpDoc.getDocumentElement().getElementsByTagName(GCConstants.EXTN).item(0);
			if(!YFCCommon.isVoid(eleItemExtn)){
			String restShipFromStr = eleItemExtn.getAttribute(GCConstants.EXTN_RESTRICT_SHIP_FROM_STORE);
		    Element eleOrderLineExtn = (Element) eleOrderLine.getElementsByTagName(GCConstants.EXTN).item(0);
			if(YFCCommon.isVoid(eleOrderLineExtn)){
				eleOrderLineExtn = inDoc.createElement(GCConstants.EXTN);
				eleOrderLine.appendChild(eleOrderLineExtn);
			}
			eleOrderLineExtn.setAttribute(GCConstants.EXTN_RESTRICT_SHIP_FROM_STORE, restShipFromStr);
			}
		  }
	     }
	   }
	} catch (TransformerException e1) {
		LOGGER.error(logInfo, e1);
	}
	
}
//GCSTORE-7165--End





/**
   * This method is used to remove reservation element.
   *
   * @param eleOrder
   */
  private void removeReservationElement(YFCElement eleOrder) {

    LOGGER.debug("removeReservationElement -- Begin");
    YFCNodeList<YFCElement> nlOdrLine = eleOrder.getElementsByTagName(GCConstants.ORDER_LINE);
    for (YFCElement eleOdrLine : nlOdrLine) {

      String orderLineKey = eleOdrLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      YFCElement eleOdrLinereservations = eleOdrLine.getChildElement(GCConstants.ORDER_LINE_RESERVATIONS);
      if (!YFCCommon.isVoid(orderLineKey) && !YFCCommon.isVoid(eleOdrLinereservations)) {
        eleOdrLine.removeChild(eleOdrLinereservations);
      }
    }
    LOGGER.debug("removeReservationElement -- End");
  }

  /**
   * GCSTORE-2909 & 2910::This method removes BundleFulfillmentMode Attribute from item level if
   * present.
   *
   * @param inputDoc
   */
  private void removeBundleFulfillmentMode(Document inputDoc) {
    YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
    YFCElement root = inDoc.getDocumentElement();
    YFCElement orderLines = root.getChildElement(GCConstants.ORDER_LINES);
    if (!YFCCommon.isVoid(orderLines)) {
      YFCNodeList<YFCElement> orderLineNodeList = orderLines.getElementsByTagName(GCConstants.ORDER_LINE);
      for (YFCElement orderLine : orderLineNodeList) {
        YFCElement item = orderLine.getChildElement(GCConstants.ITEM);
        if (!YFCCommon.isVoid(item)) {

          if (item.hasAttribute(GCConstants.BUNDLE_FULFILLMENT_MODE)) {
            item.removeAttribute(GCConstants.BUNDLE_FULFILLMENT_MODE);
          }
        }
      }
    }
    return;
  }
  // Fixes for GCSTORE-2569 & 2405 start
  /**
   * This method stamps ShipNode on order line for Pickup In Store Orders from orderLineReservation.
   *
   * @param env
   * @param inDoc
   */

  private void stampShipNodeForPISOrder(YFSEnvironment env,YFCElement orderDetailsEle) {

    LOGGER.beginTimer("GCBeforeChangeOrderUE.stampShipNodeForPISOrder");

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input Doc is\n" + orderDetailsEle);
    }

    String sExtnPickupStoreID = "";
    String sFulfillmentType="";
    String sExtnOrderCaptureChannel="";
    String sSourcingClassification=orderDetailsEle.getAttribute("SourcingClassification");
    YFCElement eleExtn = orderDetailsEle.getChildElement(GCConstants.EXTN);
    if (!YFCCommon.isVoid(eleExtn)) {
      sExtnPickupStoreID = eleExtn.getAttribute(GCConstants.EXTN_PICKUP_STORE_ID);
      LOGGER.verbose("ExtnPickupStoreID is" + sExtnPickupStoreID);
    }


    YFCNodeList<YFCElement> nlOrderLine =
        orderDetailsEle.getChildElement(GCConstants.ORDER_LINES, true).getElementsByTagName(GCConstants.ORDER_LINE);
    for (YFCElement eleOrderLine : nlOrderLine) {
      sFulfillmentType = eleOrderLine.getAttribute(GCConstants.FULFILLMENT_TYPE);
      String sShipNode = eleOrderLine.getAttribute(GCConstants.SHIP_NODE);
      if (sFulfillmentType.indexOf(GCConstants.PICKUP_IN_STORE) >= 0 && YFCCommon.isStringVoid(sShipNode)) {
        YFCElement eleOrderLineReservations = eleOrderLine.getChildElement(GCConstants.ORDER_LINE_RESERVATIONS);
        if (!YFCCommon.isVoid(eleOrderLineReservations)) {
          YFCNodeList<YFCElement> nlOrderLineReservation =
              eleOrderLineReservations.getElementsByTagName(GCConstants.ORDER_LINE_RESERVATION);
          int nlLength = nlOrderLineReservation.getLength();
          for (int i = 0; i < nlLength; i++) {
            YFCElement eleOrderLineReservation = nlOrderLineReservation
                .item(i);
            String sNode = eleOrderLineReservation
                .getAttribute(GCConstants.NODE);
            if (YFCCommon.equals(sSourcingClassification,
                GCConstants.PREPAID_SOURCING)
                || YFCCommon.equals(
                    sSourcingClassification,
                    GCConstants.DC_SOURCED_SOURCING)) {
              boolean bIsNodeAStore=GCStampShipNodeOnOrderLine.isNodeAStore(env,sNode);
              if(bIsNodeAStore){
                eleOrderLineReservation.getParentElement().removeChild(eleOrderLineReservation);
                nlLength--;
                i--;
              }
              if (YFCCommon.equalsIgnoreCase(sExtnPickupStoreID, sNode)) {
                eleOrderLine.setAttribute(GCConstants.SHIP_NODE, sNode);
                eleOrderLine.setAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE, GCConstants.YES);
                break;
              }
            }
          }
          eleOrderLineReservations.setAttribute(GCConstants.RESET, GCConstants.YES);
        }
      } else {
        // GCSTORE-2707::Begin
        YFCElement eleOrderLineReservations = eleOrderLine.getChildElement(GCConstants.ORDER_LINE_RESERVATIONS);
        if (!YFCCommon.isVoid(eleOrderLineReservations)) {
          eleOrderLineReservations.setAttribute(GCConstants.RESET, GCConstants.YES);
        }
        // GCSTORE-2707::End
      }


      YFCElement eleOrderLineReservations = eleOrderLine.getChildElement(GCConstants.ORDER_LINE_RESERVATIONS);
      if (!YFCCommon.isVoid(eleOrderLineReservations)) {
        YFCNodeList<YFCElement> nlOrderLineReservation =
            eleOrderLineReservations.getElementsByTagName(GCConstants.ORDER_LINE_RESERVATION);
        for (YFCElement eleOrderLineReservation : nlOrderLineReservation) {

          if (eleOrderLineReservation.hasAttribute("ReservationKey")) {
            eleOrderLineReservation.setAttribute("ReservationKey", "");

          }
        }
      }
    }

    //Defect-3174
    if(sFulfillmentType.indexOf(GCConstants.PICKUP_IN_STORE) >= 0){

      LOGGER.verbose("sExtnOrderCaptureChannel " + sExtnOrderCaptureChannel);

      YFCElement elePersonInfoShipTo = orderDetailsEle.getChildElement(GCConstants.PERSON_INFO_SHIP_TO);
      LOGGER.verbose("elePersonInfoShipTo" + elePersonInfoShipTo);
      if(YFCCommon.equals(sExtnOrderCaptureChannel,GCConstants.CALL_CENTER_ORDER_CHANNEL) || YFCCommon.equals(sExtnOrderCaptureChannel,GCConstants.GCSTORE) ||YFCCommon.equalsIgnoreCase(sExtnOrderCaptureChannel,"Web")){
        if(!YFCCommon.isVoid(sExtnPickupStoreID)  && !YFCCommon.isVoid(elePersonInfoShipTo)){


          String sCompanyWithStoreID = "Guitar Center " + sExtnPickupStoreID;
          elePersonInfoShipTo.setAttribute(GCConstants.COMPANY, sCompanyWithStoreID);

          if (LOGGER.isVerboseEnabled()) {
            LOGGER.verbose("Updated  Doc is\n" + orderDetailsEle);
          }
        }
      }

    }
    //Ends Here 3174



  }



  /**
   * This method calls getOrderList and get the order details required for order line split,
   * dependent line hold and stamping of shipnode for PIS order (when it is being fulfilled from
   * fulfilling store)
   *
   * @param env
   * @param sOrderHeaderKey
   * @return
   */

  private YFCDocument getOrderDetailsForSplitOnOrderAmend(YFSEnvironment env, String sOrderHeaderKey) {
    LOGGER.beginTimer("getOrderDetailsForSplitOnOrderAmend");

    YFCDocument docGetOrderListInput = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement orderEle = docGetOrderListInput.getDocumentElement();
    orderEle.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    Document orderListOutputDoc =
        GCCommonUtil.invokeAPI(env, docGetOrderListInput.getDocument(), GCConstants.API_GET_ORDER_LIST,
            "/global/template/api/getOrderListForSplitOnOrderAmend.xml");
    Element eleOrder = GCXMLUtil.getElementByXPath(orderListOutputDoc, "/OrderList/Order");
    Document docEleOrder = GCXMLUtil.getDocumentFromElement(eleOrder);
    YFCDocument yfcOrderDoc = YFCDocument.getDocumentFor(docEleOrder);
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Order details in getOrderDetailsForSplitOnOrderAmend " + yfcOrderDoc);
    }
    LOGGER.endTimer("getOrderDetailsForSplitOnOrderAmend");
    return yfcOrderDoc;
  }

  // Fixes for GCSTORE-2569 & 2405 end

  private void stampPriceOverrideReasonCode(Document inDoc)
      throws TransformerException {

    NodeList nlNotes = XPathAPI.selectNodeList(inDoc,
        "Order/OrderLines/OrderLine/Notes/Note[@ReasonCode1!='']");
    for (int i = 0; i < nlNotes.getLength(); i++) {

      Element eleNote = (Element) nlNotes.item(i);
      String sReasonCode1 = eleNote
          .getAttribute(GCConstants.REASON_CODE_1);
      String sReasonCode2 = eleNote.getAttribute("ReasonCode2");
      Element eleNote1 = inDoc.createElement(GCConstants.NOTE);
      String sOverridedPrice = eleNote
          .getAttribute("OverridedPrice");
      eleNote1.setAttribute(GCConstants.NOTE_TEXT, sReasonCode1);
      eleNote1.setAttribute("ReasonCode", sReasonCode2);
      eleNote.getParentNode().appendChild(eleNote1);
      Element eleOl = (Element) eleNote.getParentNode().getParentNode();
      Element eleExtn = (Element) eleOl.getElementsByTagName("Extn").item(0);
      eleExtn.setAttribute("ExtnOverridedPrice", sOverridedPrice);
      eleNote.removeAttribute("OverridedPrice");
    }
  }
  /**
   * This methods checks for single source items on order and throws an error when user tries to add
   * future ship date on such order
   *
   * @param inDoc
   * @param sIsReqShipDateChange
   * @param sIsPlatinumAdded
   */
  private void singleSourceItemValidationOnOrder(Document inDoc, String sIsReqShipDateChange) {

    YFCDocument docInput = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleRoot = docInput.getDocumentElement();
    YFCDocument docGetOrderList = YFCDocument.getDocumentFor(docGetOrderListOP);
    String sReqShipDate = eleRoot.getAttribute(GCConstants.REQ_SHIP_DATE);
    SimpleDateFormat sdf = new SimpleDateFormat(GCConstants.DATE_FORMAT);

    Calendar cCurrentDate = Calendar.getInstance();
    cCurrentDate.setTime(new Date());
    String sCurrentDate = sdf.format(cCurrentDate.getTime());
    Date dReqShipdate;
    Date dCurrentDate;
    try {
      dReqShipdate = sdf.parse(sReqShipDate);
      dCurrentDate = sdf.parse(sCurrentDate);
    } catch (ParseException yfsEx) {
      YFSException e = new YFSException(yfsEx.getMessage());
      LOGGER.error("Date parsing failed in singleSourceItemValidationOnOrder method", yfsEx);
      throw e;
    }
    YFCNodeList<YFCElement> listOrderLine = docGetOrderList.getElementsByTagName(GCConstants.ORDER_LINE);
    int ilengthOrderLine = listOrderLine.getLength();

    if (YFCCommon.equals(GCConstants.FLAG_Y, sIsReqShipDateChange)) {
      for (int iCounterOrderLine = 0; iCounterOrderLine < ilengthOrderLine; iCounterOrderLine++) {
        YFCElement eleOrderLine = listOrderLine.item(iCounterOrderLine);
        YFCElement eleOrderInvAttr = eleOrderLine.getChildElement("OrderLineInvAttRequest");
        YFCElement eleItemDetails = eleOrderLine.getChildElement(GCConstants.ITEM_DETAILS);
        YFCElement eleItemExtn = eleItemDetails.getChildElement(GCConstants.EXTN);
        String sBatchNo = "";
        if (!YFCCommon.isVoid(eleOrderInvAttr)) {
          sBatchNo = eleOrderInvAttr.getAttribute(GCConstants.BATCH_NO);
        }
        String sExtnIsUsedOrVintage = eleItemExtn.getAttribute("ExtnIsUsedOrVintageItem");
        String sMaxLineStatus = eleOrderLine.getAttribute("MaxLineStatus");
        if ((YFCCommon.equals(GCConstants.FLAG_Y, sExtnIsUsedOrVintage) && !YFCCommon.equals(sMaxLineStatus, "9000") && dReqShipdate
            .after(dCurrentDate))
            || (!YFCCommon.isVoid(sBatchNo) && dReqShipdate.after(dCurrentDate) && !YFCCommon.equals(sMaxLineStatus,
                "9000"))) {
          YFCException yfs = new YFCException(GCErrorConstants.SINGLE_SOURCE_FUTURE_DATE);
          yfs.setErrorDescription(GCErrorConstants.SINGLE_SOURCE_FUTURE_DATE_DESC);
          throw yfs;
        }
      }

    } else {

      Date dReqShipDateOnOrder;

      try {
        //GCSTORE-1482 - Start
        dReqShipDateOnOrder = sdf.parse(sReqShipDate);
        //GCSTORE-1482 - End
      } catch (ParseException yfsEx) {
        YFSException e = new YFSException(yfsEx.getMessage());
        LOGGER.error("Date parsing failed in singleSourceItemValidationOnOrder method", yfsEx);
        throw e;
      }
      if (dReqShipDateOnOrder.after(dCurrentDate)) {
        YFCException yfs = new YFCException(GCErrorConstants.SINGLE_SOURCE_FUTURE_DATE_ADD_PROD);
        yfs.setErrorDescription(GCErrorConstants.SINGLE_SOURCE_FUTURE_DATE_ADD_PROD_DESC);
        throw yfs;
      }
    }
  }

  /**
   * This method checks If the Ship to Address is APOFPO/PO and
   * if item is serialized platinum. if so, then throws error
   *
   *
   * @param YFSEnvironment
   * @param inDoc
   */
  private void addressCheckForPlatinumItem(YFSEnvironment env,Document inDoc){
    YFCDocument docInput = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleRoot = docInput.getDocumentElement();
    String sOHK = eleRoot.getAttribute(GCConstants.ORDER_HEADER_KEY);
    String sIsPlatinumAdded = eleRoot.getAttribute("IsPlatinumAdded");
    YFCElement eleOrderList  = fetchOrderDetails(env, sOHK);
    YFCElement elePersonInfoShipTo;
    YFCNodeList<YFCElement> orderLineList;
    if(YFCCommon.equals(GCConstants.FLAG_Y, sIsPlatinumAdded)){
      elePersonInfoShipTo = eleOrderList.getChildElement(GCConstants.PERSON_INFO_SHIP_TO);
      orderLineList= eleRoot.getElementsByTagName("OrderLine");
    }   else{
      elePersonInfoShipTo = eleRoot.getChildElement(GCConstants.PERSON_INFO_SHIP_TO);
      orderLineList = eleOrderList.getElementsByTagName("OrderLine");
    }if(!YFCCommon.isVoid(elePersonInfoShipTo)) {
      YFCElement eleExtn = elePersonInfoShipTo.getChildElement(GCConstants.EXTN);
      if(!YFCCommon.isVoid(eleExtn)){
        String sIsAPOFPO = eleExtn.getAttribute(GCConstants.EXTN_IS_APO_FPO);
        String sIsPOBox = eleExtn.getAttribute(GCConstants.EXTN_IS_PO_BOX);
        if(YFCCommon.equals(sIsAPOFPO,"Y") || YFCCommon.equals(sIsPOBox,"Y")){
          for (YFCElement orderLineEle: orderLineList) {
            YFCElement eleOrderLineInvAttRequest = orderLineEle.getChildElement("OrderLineInvAttRequest");
            if(!YFCCommon.isVoid(eleOrderLineInvAttRequest)){
              String sBatchNo = eleOrderLineInvAttRequest.getAttribute(GCConstants.BATCH_NO);
              if(!YFCCommon.isVoid(sBatchNo)){
                YFCException yfs = new YFCException(GCErrorConstants.SERIALIZEDITEM_APO_PO_ADDRESS_CODE);
                yfs.setErrorDescription(GCErrorConstants.SERIALIZEDITEM_APO_PO_ADDRESS_DESCRIPTION);
                throw yfs;
              }
            }
          }
        }
      }
    }
  }
  /**
   * This method updates the EarliestScheduleDate of each OrderLine as
   * ReqShipDate of Order
   *
   * @param changeOrderUEDoc
   */

  //Fix for defect-2255
  private void updateEarliestScheduleDate(YFSEnvironment env, YFCDocument changeOrderUEDoc) {
    YFCElement cOrdUERootEle = changeOrderUEDoc.getDocumentElement();

    String sOrderHeaderKey= cOrdUERootEle.getAttribute("OrderHeaderKey");
    String reqShipDate = cOrdUERootEle.getAttribute("ReqShipDate");

    //Node List of changeOrder
    YFCElement cOrdOrderLinesEle = cOrdUERootEle.getChildElement("OrderLines");
    YFCNodeList<YFCElement> cOrdOrderLineList = cOrdOrderLinesEle.getElementsByTagName("OrderLine");


    // invoking getOrderList
    YFCDocument getOrderListinDoc=YFCDocument.createDocument("Order");
    YFCElement eleOrder=getOrderListinDoc.getDocumentElement();
    eleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);


    YFCDocument getOrderListTemplate=YFCDocument.getDocumentFor(GCConstants.GET_CHANGE_ORDER_LIST_TEMPLATE);

    YFCDocument outDoc= GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListinDoc,getOrderListTemplate);

    YFCElement eleoutDocOrderList=outDoc.getDocumentElement();
    YFCElement eleoutDocOrder=eleoutDocOrderList.getChildElement(GCConstants.ORDER);
    YFCElement eleoutDocOrderLines=eleoutDocOrder.getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> listoutDocOrderLine = eleoutDocOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);

    List<String> orderLineKeyList = new ArrayList<String>(listoutDocOrderLine.getLength());
    for (YFCElement eleoutDocOrderLine : listoutDocOrderLine) {
      String sOrderLineKey=eleoutDocOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      orderLineKeyList.add(sOrderLineKey);
    }

    //Comparing
    for(YFCElement eleChangeOrdeLine : cOrdOrderLineList ){
      String sChangeOrderLineKey=eleChangeOrdeLine.getAttribute(GCConstants.ORDER_LINE_KEY);

      if(YFCCommon.isVoid(sChangeOrderLineKey)){
        eleChangeOrdeLine.setAttribute("EarliestScheduleDate",
            reqShipDate);
      } else if(orderLineKeyList.contains(sChangeOrderLineKey)){
        eleChangeOrdeLine.setAttribute("EarliestScheduleDate",
            reqShipDate);
        orderLineKeyList.remove(sChangeOrderLineKey);
      }

    }

    for (int i = 0; i < orderLineKeyList.size(); i++) {
      YFCElement eleNewOrderLine=cOrdOrderLinesEle.createChild("OrderLine");
      eleNewOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY, orderLineKeyList.get(i));
      eleNewOrderLine.setAttribute("EarliestScheduleDate",reqShipDate);
    }

  }

  /**
   * This method takes OrderHeaderKey and calls getOrderList to get the order
   * details which is further used to stamp SourcingClassification and
   * FulfillmentType
   *
   * @param env
   * @param sOrderHeaderKey
   * @return
   * @throws GCException
   */
  private YFCElement fetchOrderDetails(YFSEnvironment env,
      String sOrderHeaderKey) {

    YFCDocument docGetOrderListIP = YFCDocument
        .createDocument(GCConstants.ORDER);
    YFCElement orderEle = docGetOrderListIP.getDocumentElement();
    orderEle.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
    Document docGetOrderListTmp = GCCommonUtil
        .getDocumentFromPath("/global/template/api/getOrderList.xml");
    YFCDocument orderListOutput = GCCommonUtil.invokeAPI(env,
        GCConstants.API_GET_ORDER_LIST, docGetOrderListIP,
        YFCDocument.getDocumentFor(docGetOrderListTmp));
    YFCElement orderListEle = orderListOutput.getDocumentElement();
    return orderListEle.getChildElement(GCConstants.ORDER);
  }

  /**
   *
   * Description of cancelWarrantyItemIfParentIsDeleted
   *
   * @param inDoc
   *
   *
   */
  private void cancelWarrantyItemIfParentIsDeleted(Document inDoc)
      throws GCException {

    try {
      LOGGER.debug("Entering cancelWarrantyItemIfParentIsDeleted method()");
      NodeList nlOrderLine = GCXMLUtil.getNodeListByXpath(inDoc,
          "Order/OrderLines/OrderLine");

      int iOrderLineLen = nlOrderLine.getLength();
      String sOrderedQtyModified = null;
      String sAction = "";
      for (int iCounter = 0; iCounter < iOrderLineLen; iCounter++) {

        Element eleOrderLine = (Element) nlOrderLine.item(iCounter);
        Element eleOrderLineTranQuantity =
            (Element) eleOrderLine.getElementsByTagName(GCConstants.ORDER_LINE_TRAN_QUANTITY).item(0);
        if(!YFCCommon.isVoid(eleOrderLineTranQuantity)) {
          sOrderedQtyModified = eleOrderLineTranQuantity.getAttribute(GCConstants.ORDERED_QTY);
          sAction = eleOrderLine.getAttribute(GCConstants.ACTION);
        }
        String sQtyToCancel = eleOrderLine
            .getAttribute("QuantityToCancel");
        LOGGER.debug("ssQtyToCancel::" + sQtyToCancel);
        String sOrderLineKey = eleOrderLine
            .getAttribute(GCConstants.ORDER_LINE_KEY);
        if (!YFCCommon.isVoid(sQtyToCancel) || !YFCCommon.isVoid(eleOrderLineTranQuantity)) {
          LOGGER.debug("Qty to cancel is not null");
          Element eleWarrOrderLine = GCXMLUtil
              .getElementByXPath(
                  docGetOrderListOP,
                  "//Order/OrderLines/OrderLine[@DependentOnLineKey='"
                      + sOrderLineKey
                      + "' and @MaxLineStatus!='9000' and ItemDetails/PrimaryInformation[@ItemType='03']]");
          if (!YFCCommon.isVoid(eleWarrOrderLine)) {
            LOGGER.debug("Warranty Line exists");
            String sWarrantyOLKey = eleWarrOrderLine
                .getAttribute(GCConstants.ORDER_LINE_KEY);
            String sWarrOrderedQty = eleWarrOrderLine
                .getAttribute(GCConstants.ORDERED_QTY);

            if(!YFCCommon.isVoid(eleOrderLineTranQuantity)) {
              Element eleWarrantyLineToModify = inDoc
                  .createElement(GCConstants.ORDER_LINE);
              eleWarrantyLineToModify.setAttribute(
                  GCConstants.ORDER_LINE_KEY, sWarrantyOLKey);
              eleWarrantyLineToModify.setAttribute(GCConstants.ACTION, sAction);
              Element eleWarrantyLineTranQty = inDoc
                  .createElement(GCConstants.ORDER_LINE_TRAN_QUANTITY);
              eleWarrantyLineTranQty.setAttribute(
                  GCConstants.ORDERED_QTY, sOrderedQtyModified);
              Element eleOrderLines = GCXMLUtil
                  .getElementByXPath(inDoc,
                      "/Order/OrderLines");
              eleWarrantyLineToModify.appendChild(eleWarrantyLineTranQty);
              if(!YFCCommon.isVoid(sAction)){
                eleOrderLines.appendChild(eleWarrantyLineToModify);
              }
            }
            /*
             * Check if warranty line is already there in change order
             */
            Element eleWarrOnCOXml = GCXMLUtil.getElementByXPath(
                inDoc,
                "//Order/OrderLines/OrderLine[@OrderLineKey='"
                    + sWarrantyOLKey + "']");
            if (YFCCommon.isVoid(eleWarrOnCOXml) && !YFCCommon.isVoid(sQtyToCancel)) {
              Double dWarrOrderedQty = Double.parseDouble(sWarrOrderedQty) - Double.parseDouble(sQtyToCancel);

              sWarrOrderedQty = dWarrOrderedQty.toString();

              Element eleWarrantyLineToCancel = inDoc
                  .createElement(GCConstants.ORDER_LINE);
              eleWarrantyLineToCancel.setAttribute(
                  GCConstants.ORDER_LINE_KEY, sWarrantyOLKey);
              eleWarrantyLineToCancel.setAttribute(
                  "QuantityToCancel", sQtyToCancel);
              eleWarrantyLineToCancel.setAttribute(
                  GCConstants.ORDERED_QTY, sWarrOrderedQty);
              Element eleOrderLines = GCXMLUtil
                  .getElementByXPath(inDoc,
                      "/Order/OrderLines");
              eleOrderLines.appendChild(eleWarrantyLineToCancel);
            }
          }
        }
      }

      LOGGER.debug("Exiting cancelWarrantyItemIfParentIsDeleted method()");
    } catch (Exception e) {
      LOGGER.error("Error in cancelWarrantyItemIfParentIsDeleted method",
          e);
      throw new GCException(e);
    }

  }
  
  //Free-Gift item cancellation
  //6877-BEGIN:
  private void freeGiftItemCancellation(YFSEnvironment env, Document inDoc) throws ParserConfigurationException, IOException, SAXException{

	  LOGGER.info("Entering inside GCBeforeChangeOrderUE.freeGiftItemCancellation()");
	  YFCDocument yfcInputDoc = YFCDocument.getDocumentFor(inDoc);

	  YFCDocument docCloneOfInDoc = createCloneOfInDoc(yfcInputDoc);
	  YFCElement eleChangeOrderUERoot = docCloneOfInDoc.getDocumentElement();
	  YFCNodeList<YFCElement> nlOrderLine = eleChangeOrderUERoot.getElementsByTagName(GCConstants.ORDER_LINE);

	  for(YFCElement eleOrderLine : nlOrderLine){

		  String sQtyToCancel = eleOrderLine.getAttribute(GCConstants.QUANTITY_TO_CANCEL);
		  String sIsCompleteLineCancel = eleOrderLine.getAttribute(GCConstants.IS_COMPLETE_LINE_CANCEL);

		  if(((!YFCCommon.isVoid(sQtyToCancel) && !sQtyToCancel.equals("0"))) || YFCCommon.equals(sIsCompleteLineCancel, GCConstants.YES)){

			  computeGiftQtyToCancel(env, eleOrderLine, inDoc);
		  }	  
	  }	  

	  LOGGER.info("Exiting from GCBeforeChangeOrderUE.freeGiftItemCancellation()");
  }
  
  private YFCDocument createCloneOfInDoc(YFCDocument yfcInputDoc) {

	  LOGGER.info("Entering inside GCBeforeChangeOrderUE.createCloneOfInDoc()");
	  YFCDocument docCreateCloneChangeOrder = YFCDocument.createDocument();
	  YFCElement eleCloneWO = docCreateCloneChangeOrder.importNode(
			  yfcInputDoc.getDocumentElement(), true);
	  docCreateCloneChangeOrder.appendChild(eleCloneWO);

	  LOGGER.info("Exiting from GCBeforeChangeOrderUE.createCloneOfInDoc()");
	  return docCreateCloneChangeOrder;
  }

  private void computeGiftQtyToCancel(YFSEnvironment env, YFCElement eleOrderLine, Document inDoc){

	  LOGGER.info("Entering inside GCBeforeChangeOrderUE.computeGiftQtyToCancel()");
	  //the orderLine over which we are iterating in the above method
	  String sOLKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
	  
	  if(YFCCommon.isVoid(sOLKey)){
		  String sPrimeLineNo = eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
		  String sSubLineNo = eleOrderLine.getAttribute(GCConstants.SUB_LINE_NO);

		  if((!YFCCommon.isVoid(sPrimeLineNo)) && (!YFCCommon.isVoid(sSubLineNo))){
			  sOLKey = GCXMLUtil.getAttributeFromXPath(docGetOrderListOP,
					  "//Order/OrderLines/" + "OrderLine[@PrimeLineNo='" + sPrimeLineNo + "' and @SubLineNo='"
							  + sSubLineNo + "']/@OrderLineKey");
		  }		  
	  }
	  
	  String sOrderedQty =  eleOrderLine.getAttribute(GCConstants.ORDERED_QTY);
	  String sIsCompleteLineCancel = eleOrderLine.getAttribute(GCConstants.IS_COMPLETE_LINE_CANCEL);
	  String sQuantityToCancel =  eleOrderLine.getAttribute(GCConstants.QUANTITY_TO_CANCEL);

	  List<Element> giftOrderLineList = 
			  GCXMLUtil.getElementListByXpath(docGetOrderListOP, "//Order/OrderLines/OrderLine[@DependentOnLineKey='"
					  + sOLKey
					  + "' and Extn[@ExtnIsFreeGiftItem='Y']]");  

	  int iGiftLinesCount = giftOrderLineList.size();
	  if(iGiftLinesCount > 1){
		  LOGGER.debug("Parent line has more than one gift line");
		  return;
	  }
	  else if(iGiftLinesCount == 0){
		  LOGGER.debug("This order line does not have associated gift line" );
		  return;
	  }
	  else{
		  //Parent line has 1 free gift line
		  Element eleGiftOrderLine = giftOrderLineList.get(0);
		  String sStatus = eleGiftOrderLine.getAttribute(GCConstants.ORDER_LINE_MAX_STATUS);

		  YFCDocument yfcInputDoc = YFCDocument.getDocumentFor(inDoc);
		  YFCElement eleRootInDoc = yfcInputDoc.getDocumentElement();
		  String sUpdatedGLQty = eleGiftOrderLine.getAttribute(GCConstants.ORDERED_QUANITY);

		  if("3200".compareTo(sStatus) > 0){
			  //Gift line is less than release

			  //Condition to check if the free gift item is already present in change order input  
			  String sGiftOrderLineKey = eleGiftOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);  
			  Element   eleGiftLinePresentInChangeOrderIp = GCXMLUtil.getElementByXPath(inDoc, "//Order/OrderLines/OrderLine[@OrderLineKey='"
					  + sGiftOrderLineKey
					  + "']");	

			  if(!YFCCommon.isVoid(eleGiftLinePresentInChangeOrderIp)){
				  LOGGER.debug("Gift Line is already getting cancelled and present in changeOrder input");
			  }

			  else{

				  if(YFCCommon.equals(sIsCompleteLineCancel, GCConstants.YES))
				  {
					  //Parent line is completely cancelled. Setting Gift Line Quantity to 0 
					  sUpdatedGLQty = "0";
				  }	

				  else if(!YFCCommon.isVoid(sOrderedQty)){
					  double  dOrderdQty =Double.parseDouble(sOrderedQty);
					  if(dOrderdQty==0){
						  sUpdatedGLQty = "0"; 
					  }
					  else{
						  //partial cancellation
						  if(!YFCCommon.isVoid(sQuantityToCancel)){
							  double dQuantityToCancel = Double.parseDouble(sQuantityToCancel);
							  String sGiftLineOrderedQty =eleGiftOrderLine.getAttribute(GCConstants.ORDERED_QUANITY);
							  double dGiftLineOrderedQty = Double.parseDouble(sGiftLineOrderedQty);
							  double dUpdatedGLQty = dGiftLineOrderedQty - dQuantityToCancel;
							  if(dUpdatedGLQty < 0){
								  sUpdatedGLQty = "0";
							  }else{
								  sUpdatedGLQty = Double.toString(dUpdatedGLQty);
							  }

						  }
					  }
				  }

				  YFCElement eleOrderLines =  eleRootInDoc.getChildElement(GCConstants.ORDER_LINES);
				  YFCElement eleGiftLineToCancel =eleOrderLines.createChild(GCConstants.ORDER_LINE);
				  eleGiftLineToCancel.setAttribute(GCConstants.ORDER_LINE_KEY, sGiftOrderLineKey);
				  eleGiftLineToCancel.setAttribute(GCConstants.ORDERED_QTY, sUpdatedGLQty); 
			  }
		  }
		  else{
			  //release or greater.

			  String isSystemCancellation = eleRootInDoc.getAttribute(GCConstants.SYSTEM_CANCELLATION);
			  String isBackOrderCancel = eleRootInDoc.getAttribute(GCConstants.BACKORDER_CANCEL_MSG);

			  if((!YFCCommon.isVoid(isSystemCancellation) && isSystemCancellation.equals(GCConstants.YES)) || 
					  (!YFCCommon.isVoid(isBackOrderCancel) && isBackOrderCancel.equals(GCConstants.YES))){

				  // raising alert
				  YFCDocument createExceptionDoc = YFCDocument.createDocument(GCConstants.INBOX);
				  YFCElement eleInbox = createExceptionDoc.getDocumentElement();	

				  Element eleGetOrderList = docGetOrderListOP.getDocumentElement();
				  Element eleOrder = (Element) eleGetOrderList.getElementsByTagName(GCConstants.ORDER).item(0);
				  String sOrderNo = eleOrder.getAttribute(GCConstants.ORDER_NO);
				  Element eleGiftItem = (Element) eleGiftOrderLine.getElementsByTagName(GCConstants.ITEM).item(0);
				  String sGiftID = eleGiftItem.getAttribute(GCConstants.ITEM_ID);
				  //fetching OrderNo from getOrderListOP		

				  eleInbox.setAttribute(GCConstants.ORDER_NO, sOrderNo);				  
				  eleInbox.setAttribute(GCConstants.ORDER_HEADER_KEY, eleRootInDoc.getAttribute(GCConstants.ORDER_HEADER_KEY));				  
				  eleInbox.setAttribute(GCConstants.ORDER_LINE_KEY, eleGiftOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
				  eleInbox.setAttribute(GCConstants.ITEM_ID, sGiftID);				  
				  eleInbox.setAttribute(GCConstants.DETAIL_DESCRIPTION, "Parent item associated with the free gift Item ID: " + sGiftID + " has been cancelled");				  
				  eleInbox.setAttribute(GCConstants.EXCEPTION_TYPE, GCConstants.GC_CANCEL_GIFT_EXCEPTION);

				  GCCommonUtil.invokeAPI(env, GCConstants.API_CREATE_EXCEPTION, createExceptionDoc.getDocument());
			  }

			  else{
				  //This is manual cancellation. Pop up will be displayed at the UI.
				  return;
			  }  
		  }  
	  }  
  }
  
  //6877-END

  /**
   *
   * @param env
   * @param sOrderHeaderKey
   * @return
   * @throws GCException
   */
  private String fetchExtnFulfillmentType(YFSEnvironment env,
      Document inDoc, String sOrderHeaderKey) throws GCException {

    Document docGetOrderListIP;
    String sExtnFulfillmentType = null;
    try {
      docGetOrderListIP = GCXMLUtil.createDocument(GCConstants.ORDER);
      docGetOrderListIP.getDocumentElement().setAttribute(
          GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
      Document docGetOrderListTmp = GCCommonUtil
          .getDocumentFromPath("/global/template/api/getOrderList.xml");
      docGetOrderListOP = GCCommonUtil.invokeAPI(env,
          GCConstants.API_GET_ORDER_LIST, docGetOrderListIP,
          docGetOrderListTmp);
      sExtnFulfillmentType = GCXMLUtil.getAttributeFromXPath(
          docGetOrderListOP, "//Order/Extn/@ExtnFulfillmentType");

      //Defect-4044 Applying Hold For Warranty Item
      //  YFCElement eleOrder = (YFCElement) docGetOrderListOP.getDocumentElement();
      //  String sDraftOrderFlag=  GCXMLUtil.getAttributeFromXPath(
      //    docGetOrderListOP, "//Order/@DraftOrderFlag");


      String sMaxOrderStatus=  GCXMLUtil.getAttributeFromXPath(
          docGetOrderListOP, "//Order/@MaxOrderStatus");
      if(!YFCCommon.isVoid(sMaxOrderStatus)){

        // Fix for GCSTORE-6477 -- Starts - Handling the scenario where maxStatus is of the format
        // 1.2.3
        String maxStatus = sMaxOrderStatus;
        String[] maxStatusSubList = maxStatus.split("\\.");
        final int noOfSubStatus = maxStatusSubList.length;
        LOGGER.verbose("noOfSubStatus :" + noOfSubStatus);
        Double dMaxOrderStatus = 0.0;
        if (noOfSubStatus > 2) {
          String newSubStatus = maxStatusSubList[0] + "." + maxStatusSubList[1];
          dMaxOrderStatus = Double.parseDouble(newSubStatus);
        } else {
          dMaxOrderStatus = Double.parseDouble(sMaxOrderStatus);
        }
        // Fix for GCSTORE-6477 -- Ends
        //String sDraftOrderFlag=eleOrder.getAttribute(GCConstants.DRAFT_ORDER_FLAG);
        if(dMaxOrderStatus >= 1100){
          YFCDocument inputDocument = YFCDocument.getDocumentFor(inDoc);
          YFCElement eleInputDocRoot = inputDocument.getDocumentElement();
          String sApplyHoldFlag = eleInputDocRoot.getAttribute("ApplyHoldOnNewLines");
          if(YFCCommon.equals(sApplyHoldFlag, "Y")){
            YFCNodeList<YFCElement> listInputDocOrderLine = eleInputDocRoot.getElementsByTagName(GCConstants.ORDER_LINE);

            for (YFCElement eleOrderLine : listInputDocOrderLine ) {
              //apply Hold
              YFCElement eleOrderHoldTypes = eleOrderLine.getChildElement(GCConstants.ORDER_HOLD_TYPES, true);
              YFCElement eleOrderHoldType = eleOrderHoldTypes.createChild(GCConstants.ORDER_HOLD_TYPE);
              eleOrderHoldType.setAttribute(GCConstants.HOLD_TYPE,"Warranty_Line_Hold");
              eleOrderHoldType.setAttribute(GCConstants.REASON_TEXT,"AvoidMaxChargeLimit");
              eleOrderHoldType.setAttribute(GCConstants.STATUS,"1100");
            }

          }
          //End Defect-4044
        }

      }
    } catch (Exception e) {
      LOGGER.error("Error in fetchExtnFulfillmentType method", e);
      throw new GCException(e);
    }
    LOGGER.debug("Exting from GCBeforeChangeOrderUE.fetchExtnFulfillmentType() "
        + "with docGetOrderListOP:"+GCXMLUtil.getXMLString(docGetOrderListOP));
    return sExtnFulfillmentType;
  }

  private Document modifyInDoc(YFSEnvironment env, Document inDoc)
      throws GCException {
    try {
      LOGGER.debug("Entering inside GCBeforeChangeOrderUE.modifyInDoc() inDoc:"+GCXMLUtil.getXMLString(inDoc));
      Element eleOrder = inDoc.getDocumentElement();
      String sOrderHeaderKey = eleOrder
          .getAttribute(GCConstants.ORDER_HEADER_KEY);
      Document docGetOrderListIP = GCXMLUtil
          .createDocument(GCConstants.ORDER);
      Element eleOrderGetOrderList = docGetOrderListIP
          .getDocumentElement();
      eleOrderGetOrderList.setAttribute(GCConstants.ORDER_HEADER_KEY,
          sOrderHeaderKey);
      Document docGetOrderListIPTemplate = GCXMLUtil
          .getDocument("<OrderList><Order OrderNo='' OrderHeaderKey='' EntryType='' OrderPurpose='' ReturnOrderHeaderKeyForExchange=''>"
              + "<OrderLines><OrderLine DependentOnLineKey='' OrderLineKey=''>"
              + "<Extn ExtnIsWarrantyItem='' ExtnWarrantyLineNumber=''/>"
              + "</OrderLine></OrderLines>"
              + "<Extn ExtnSourceCode=''/>"
              + "</Order></OrderList>");
      Document docGetOrderListOp = GCCommonUtil.invokeAPI(env,
          GCConstants.GET_ORDER_LIST, docGetOrderListIP,
          docGetOrderListIPTemplate);
      // OMS 5537 Start
      final String sOrderPurpose = GCXMLUtil.getAttributeFromXPath(docGetOrderListOp, "OrderList/Order/@OrderPurpose");

      if ((!YFCCommon.isVoid(sOrderPurpose)) && (GCConstants.EXCHANGE.equalsIgnoreCase(sOrderPurpose))) {
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug(CLASS_NAME + "Order purpose is exchange Start " + sOrderPurpose);
        }
        modifyEntryTypeForExchange(env, inDoc, docGetOrderListOp);

      }
      // OMS 5537 End

      NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc,
          "Order/OrderLines/OrderLine");
      int nlOrderLineCount = nlOrderLine.getLength();
      String sOrderLineKeyCancelled = null, sQuantityToCancel = null;
      for (int count = 0; count < nlOrderLineCount; count++) {
        Element eleOrderLine = (Element) nlOrderLine.item(count);
        sOrderLineKeyCancelled = eleOrderLine
            .getAttribute(GCConstants.ORDER_LINE_KEY);
        sQuantityToCancel = eleOrderLine
            .getAttribute("QuantityToCancel");
        if (!sOrderLineKeyCancelled.isEmpty()
            && !sQuantityToCancel.isEmpty()) {
          String sParentOrderLineKey = findParentOrderLineKey(
              docGetOrderListOp, sOrderLineKeyCancelled);
          if (!YFCCommon.isVoid(sParentOrderLineKey)) {
            Element eleOrderLineParent = (Element) XPathAPI
                .selectSingleNode(inDoc,
                    "Order/OrderLines/OrderLine[@OrderLineKey='"
                        + sParentOrderLineKey + "']");
            if (eleOrderLineParent != null) {
              Element eleExtnParent = (Element) XPathAPI
                  .selectSingleNode(eleOrderLineParent,
                      "Extn");
              //OMS-4711 Start
              if(!YFCCommon.isVoid(eleExtnParent)){
                eleExtnParent.setAttribute(
                    GCConstants.EXTN_WARRANTY_LINE_NUMBER, "");
              }
              //OMS-4711 End
            } else {
              Element eleNewOrderLine = GCXMLUtil.createElement(
                  inDoc, "OrderLine", null);
              eleNewOrderLine.setAttribute(
                  GCConstants.ORDER_LINE_KEY,
                  sParentOrderLineKey);
              Element eleNewExtn = GCXMLUtil.createElement(inDoc,
                  "Extn", null);
              eleNewExtn.setAttribute(
                  GCConstants.EXTN_WARRANTY_LINE_NUMBER, "");
              eleNewOrderLine.appendChild(eleNewExtn);
              Element eleOrderLines = (Element) XPathAPI
                  .selectSingleNode(inDoc, "Order/OrderLines");
              eleOrderLines.appendChild(eleNewOrderLine);
            }
          }
        }

      }
      LOGGER.debug("Exiting From GCBeforeChangeOrderUE.modifyInDoc() outputDoc:"+GCXMLUtil.getXMLString(inDoc));

    } catch (Exception e) {
      LOGGER.error(
          "Error,Inside catch block of GCBeforeChangeOrderUE.modifyInDoc()",
          e);
      throw new GCException(e);
    }
    return inDoc;
  }

  private String findParentOrderLineKey(Document docGetOrderListOP,
      String sOrderLineKeyCancelled) throws GCException {
    String sOrderLineKeyCandidate = null, sDependentOnLineKey = null;
    LOGGER.info("Entering Inside GCBeforeChangeOrderUE.findParentOrderLineKey() inDoc:"+GCXMLUtil.getXMLString(docGetOrderListOP));

    try {
      NodeList nListOrderLine = XPathAPI.selectNodeList(
          docGetOrderListOP, "OrderList/Order/OrderLines/OrderLine");
      int nListOrderLineCount = nListOrderLine.getLength();
      for (int c = 0; c < nListOrderLineCount; c++) {
        Element elemOrderLine = (Element) nListOrderLine.item(c);
        Element eleExtn = (Element) XPathAPI.selectSingleNode(
            elemOrderLine, "Extn");
        String sExtnIsWarrantyItem = eleExtn
            .getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
        if ("Y".equals(sExtnIsWarrantyItem)) {
          sOrderLineKeyCandidate = elemOrderLine
              .getAttribute(GCConstants.ORDER_LINE_KEY);
          if (sOrderLineKeyCancelled.equals(sOrderLineKeyCandidate)) {
            sDependentOnLineKey = elemOrderLine
                .getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
            break;
          }
        }
      }
      LOGGER.info("Exiting From GCBeforeChangeOrderUE.findParentOrderLineKey()");
    } catch (Exception e) {
      LOGGER.error(
          "Error,Inside catch block of GCBeforeChangeOrderUE.findParentOrderLineKey()",
          e);
      throw new GCException(e);
    }
    return sDependentOnLineKey;
  }
  /**
   *
   * Description of addCharges this method adds all the LineCharges where ChargeCategory='Shipping'
   * and HeaderCharge ChargeAmount where ChargeCategory=Shipping and append the total in inDoc at HeaderCharge ChargeAmount
   *
   * @param inDoc
   * @throws GCException
   *
   */
  private void addCharges(Document inDoc) throws GCException{

    LOGGER.debug("Entering inside GCBeforeChangeOrderUE.addCharges Incoming Document"
        + GCXMLUtil.getXMLString(inDoc));
    try {
      Element eleOrderOrderDetails=GCXMLUtil.getElementByXPath(docGetOrderListOP, "OrderList/Order");
      Element eleOrderinDoc=GCXMLUtil.getElementByXPath(inDoc, "Order");
      eleOrderinDoc.setAttribute(GCConstants.ORDER_NO, eleOrderOrderDetails.getAttribute(GCConstants.ORDER_NO));
      eleOrderinDoc.setAttribute(GCConstants.ORDER_DATE, eleOrderOrderDetails.getAttribute(GCConstants.ORDER_DATE));
      eleOrderinDoc.setAttribute(GCConstants.TAX_EXEMPT_FLAG, eleOrderOrderDetails.getAttribute(GCConstants.TAX_EXEMPT_FLAG));

      Element elePersonInfoShipToOrderDetails=GCXMLUtil.getElementByXPath(docGetOrderListOP, "OrderList/Order/PersonInfoShipTo");
      // Fix for OMS -5156 Start

      if (elePersonInfoShipToOrderDetails != null) {

        final Element elePersonInfoShipTo = (Element) inDoc.importNode(elePersonInfoShipToOrderDetails, true);
        eleOrderinDoc.appendChild(elePersonInfoShipTo);
      }

      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(CLASS_NAME + "Inside addCharges Method, after adding the person info"
            + GCXMLUtil.getXMLString(inDoc));
      }

      // Fix for OMS -5156 End
      appendOrderLines(inDoc);
      Double dTotalCharges=0.0;
      String sShipping="Shipping";
      //Adding charges coming in inDoc
      NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc, "Order/OrderLines/OrderLine");
      int countOrderLines=nlOrderLine.getLength();
      if(countOrderLines!=0){
        for(int j=0;j<countOrderLines;j++){
          Element eleOrderLine= (Element)nlOrderLine.item(j);
          String sQuantity=eleOrderLine.getAttribute(GCConstants.ORDERED_QTY);
          int iQuantity= Integer.parseInt(sQuantity);
          NodeList nlLineCharge = XPathAPI.selectNodeList(eleOrderLine, "LineCharges/LineCharge[@ChargeCategory='"+sShipping+"']");
          int count=nlLineCharge.getLength();
          if(count!=0){
            for(int i=0;i<count;i++){
              Element eleLineCharge= (Element)nlLineCharge.item(i);
              String sChargePerUnit=eleLineCharge.getAttribute(GCConstants.CHARGE_PER_UNIT);
              Double dChargePerUnit=(!sChargePerUnit.isEmpty()) ? Double.parseDouble(sChargePerUnit) : 0.00 ;

              String sChargePerLine=eleLineCharge.getAttribute(GCConstants.CHARGE_PER_LINE);
              Double dChargePerLine=(!sChargePerLine.isEmpty()) ? Double.parseDouble(sChargePerLine) : 0.00 ;
              dTotalCharges=dTotalCharges+dChargePerUnit*iQuantity+dChargePerLine;
            }
          }
        }
      }
      Element eleHeaderCharge=GCXMLUtil.getElementByXPath(inDoc, "Order/HeaderCharges/HeaderCharge[@ChargeCategory='"+sShipping+"']");
      if(!YFCCommon.isVoid(eleHeaderCharge)){
        String sChargeAmount=eleHeaderCharge.getAttribute(GCConstants.CHARGE_AMOUNT);
        Double dChargeAmount=(!sChargeAmount.isEmpty()) ? Double.parseDouble(sChargeAmount) : 0.00 ;
        dTotalCharges=dTotalCharges+dChargeAmount;
      }
      Element elemHeaderCharge=GCXMLUtil.getElementByXPath(docGetOrderListOP, "OrderList/Order/HeaderCharges/HeaderCharge[@ChargeCategory='"+sShipping+"']");
      Element elemHeaderChargesExtn=null;
      if(!YFCCommon.isVoid(elemHeaderCharge)){
        String stChargeAmount=elemHeaderCharge.getAttribute(GCConstants.CHARGE_AMOUNT);
        Double dbChargeAmount=(!stChargeAmount.isEmpty()) ? Double.parseDouble(stChargeAmount) : 0.00 ;
        dTotalCharges=dTotalCharges+dbChargeAmount;
        elemHeaderChargesExtn = (Element) XPathAPI.selectSingleNode(
            elemHeaderCharge, GCConstants.EXTN);
      }
      //if HeaderCharges element is not present in the inDoc then append it
      Element elHeaderCharges=GCXMLUtil.getElementByXPath(inDoc, "Order/HeaderCharges");
      if(YFCCommon.isVoid(elHeaderCharges)){
        elHeaderCharges=GCXMLUtil.createElement(inDoc, "HeaderCharges", null);
        Element eleOrder=GCXMLUtil.getElementByXPath(inDoc, "Order");
        eleOrder.appendChild(elHeaderCharges);
      }
      //if HeaderCharge with ChargeCategory="Shipping" element is not present in the inDoc then append it
      if(YFCCommon.isVoid(eleHeaderCharge)){
        eleHeaderCharge=GCXMLUtil.createElement(inDoc, "HeaderCharge", null);
        elHeaderCharges.appendChild(eleHeaderCharge);
        eleHeaderCharge.setAttribute(GCConstants.CHARGE_CATEGORY, sShipping);
        eleHeaderCharge.setAttribute(GCConstants.CHARGE_NAME,
            GCConstants.SHIPPING_CHARGE);
        if(!YFCCommon.isVoid(elemHeaderChargesExtn)){
          Element eleHeaderChargeExtn = GCXMLUtil.createElement(inDoc,GCConstants.EXTN , null);
          eleHeaderCharge.appendChild(eleHeaderChargeExtn);
          eleHeaderChargeExtn.setAttribute(GCConstants.EXTN_PROMOTION_ID,
              elemHeaderChargesExtn.getAttribute(GCConstants.EXTN_PROMOTION_ID));
          eleHeaderChargeExtn.setAttribute(GCConstants.EXTN_PROMOTION_DESC,
              elemHeaderChargesExtn.getAttribute(GCConstants.EXTN_PROMOTION_DESC));
        }
      }
      eleHeaderCharge.setAttribute(GCConstants.CHARGE_AMOUNT, dTotalCharges+"");

      LOGGER.debug("Exiting from GCBeforeChangeOrderUE.addCharges Output Document"
          + GCXMLUtil.getXMLString(inDoc));

    }catch (Exception e) {
      LOGGER.error(
          "Error,Inside catch block of GCBeforeChangeOrderUE.addCharges()",
          e);
      throw new GCException(e);
    }
  }

  /**
   * @author Infosys Ltd.
   *
   *OMS 5537- Modify the entry type and source code based on return order
   *
   * @param env
   * @param inDoc
   * @param GetOrderListOp
   */


  private void modifyEntryTypeForExchange(YFSEnvironment env, Document inDoc, Document GetOrderListOp) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(CLASS_NAME + "modifyEntryTypeForExchange Start " + GCXMLUtil.getXMLString(GetOrderListOp));
    }

    final String sROHkeyForExchange =
        GCXMLUtil.getAttributeFromXPath(GetOrderListOp, "OrderList/Order/@ReturnOrderHeaderKeyForExchange");
    final String sEntryType = GCXMLUtil.getAttributeFromXPath(GetOrderListOp, "OrderList/Order/@EntryType");

    final Element eleExtn = GCXMLUtil.getElementByXPath(GetOrderListOp, "OrderList/Order/Extn");
    final String sSourceCode = eleExtn.getAttribute(GCConstants.EXTN_SOURCE_CODE);
    String sReturnSourceCode = "";
    String sReturnOrderEntryType = "";
    Element eleReturnExtn = null;
    Element inDocExtn = GCXMLUtil.getElementByXPath(inDoc, "Order/Extn");
    Document docGetOrdListIp = GCXMLUtil.createDocument(GCConstants.ORDER);
    Element eleGetOrdlistIp = docGetOrdListIp.getDocumentElement();
    eleGetOrdlistIp.setAttribute(GCConstants.ORDER_HEADER_KEY, sROHkeyForExchange);
    eleGetOrdlistIp.setAttribute(GCConstants.DOCUMENT_TYPE, GCConstants.RETURN_ORDER_DOCUMENT_TYPE);

    Document docGetOrderListTemp =
        GCXMLUtil.getDocument("<OrderList><Order OrderNo='' OrderHeaderKey='' EntryType='' DocumentType=''>"
            + "<Extn ExtnSourceCode=''/>" + "</Order></OrderList>");



    if (!YFCCommon.isVoid(sROHkeyForExchange)) {
      Document docGetOrdListOp =
          GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST, docGetOrdListIp, docGetOrderListTemp);

      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(CLASS_NAME + "Get Return Order O/p is " + GCXMLUtil.getXMLString(docGetOrderListTemp));
      }

      sReturnOrderEntryType = GCXMLUtil.getAttributeFromXPath(docGetOrdListOp, "OrderList/Order/@EntryType");
      eleReturnExtn = GCXMLUtil.getElementByXPath(docGetOrdListOp, "OrderList/Order/Extn");
      sReturnSourceCode = eleReturnExtn.getAttribute(GCConstants.EXTN_SOURCE_CODE);

    }
    Element eleOrdRoot = inDoc.getDocumentElement();
    if (!YFCCommon.isVoid(sReturnOrderEntryType)
        && ((YFCCommon.isVoid(sEntryType)) || (GCConstants.CALL_CENTER.equalsIgnoreCase(sEntryType)))) {

      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(CLASS_NAME + "Inside Check for Exchange Order Entry Type " + sReturnOrderEntryType + sEntryType);
      }
      eleOrdRoot.setAttribute(GCConstants.ENTRY_TYPE, sReturnOrderEntryType);
    }
    if (YFCCommon.isVoid(sSourceCode)) {

      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(CLASS_NAME + "Inside Check for Source Code Type " + sSourceCode);
      }
      if (YFCCommon.isVoid(inDocExtn)) {

        Element eleOutExtn = inDoc.createElement(GCConstants.EXTN);
        eleOutExtn.setAttribute(GCConstants.EXTN_SOURCE_CODE, sReturnSourceCode);
        eleOrdRoot.appendChild(eleOutExtn);
      } else {
        inDocExtn.setAttribute(GCConstants.EXTN_SOURCE_CODE, sReturnSourceCode);
      }

    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(CLASS_NAME + "modifyEntryTypeForExchange End " + GCXMLUtil.getXMLString(inDoc));
    }
  }
  /**
   *
   * Description of appendOrderLines this method append the all the missing orderlines in the inDoc
   *
   * @param inDoc
   * @throws GCException
   * @throws Exception
   *
   */
  private void appendOrderLines(Document inDoc) throws GCException {
    LOGGER.debug("Entering inside GCBeforeChangeOrderUE.appendOrderLines Incoming Document"
        + GCXMLUtil.getXMLString(inDoc));

    try {
      NodeList nlgetOrderListOrderLine = XPathAPI.selectNodeList(docGetOrderListOP, "OrderList/Order/OrderLines/OrderLine");
      int c=nlgetOrderListOrderLine.getLength();
      if(c!=0){
        for(int j=0;j<c;j++){
          Element eleOrderLineOrderDoc= (Element)nlgetOrderListOrderLine.item(j);
          Element eleBundleParentLine=(Element)XPathAPI.selectSingleNode(eleOrderLineOrderDoc,"BundleParentLine");
          if(!YFCCommon.isVoid(eleBundleParentLine)){
            continue;
          }
          String sOrderLineKey=eleOrderLineOrderDoc.getAttribute(GCConstants.ORDER_LINE_KEY);
          Element eleOrderLinesinDoc=GCXMLUtil.getElementByXPath(inDoc,"Order/OrderLines");
          if(YFCCommon.isVoid(eleOrderLinesinDoc)){
            eleOrderLinesinDoc=GCXMLUtil.createElement(inDoc, "OrderLines", null);
            Element eleOrder=GCXMLUtil.getRootElement(inDoc);
            eleOrder.appendChild(eleOrderLinesinDoc);
          }
          Element eleOrderLineinDoc=GCXMLUtil.getElementByXPath(inDoc, "Order/OrderLines/OrderLine[@OrderLineKey='"+sOrderLineKey+"']");
          Element eleLinePriceInfo=(Element)XPathAPI.selectSingleNode(eleOrderLineOrderDoc,"LinePriceInfo");
          if(!YFCCommon.isVoid(eleOrderLineinDoc)){
            eleOrderLineinDoc.setAttribute(GCConstants.ORDERED_QTY, eleOrderLineOrderDoc.getAttribute(GCConstants.ORDERED_QTY));
            Element eleExtn=GCXMLUtil.createElement(inDoc, "Extn", null);
            eleOrderLineinDoc.appendChild(eleExtn);
            eleExtn.setAttribute(GCConstants.EXTN_IS_FREE_GIFT_ITEM, GCXMLUtil.getAttributeFromXPath(eleOrderLineOrderDoc,"Extn/@ExtnIsFreeGiftItem"));
            Element eleItem=GCXMLUtil.createElement(inDoc, "Item", null);
            eleOrderLineinDoc.appendChild(eleItem);
            eleItem.setAttribute(GCConstants.ITEM_ID, GCXMLUtil.getAttributeFromXPath(eleOrderLineOrderDoc,"Item/@ItemID"));
            eleItem.setAttribute(GCConstants.UNIT_OF_MEASURE, GCXMLUtil.getAttributeFromXPath(eleOrderLineOrderDoc,"Item/@UnitOfMeasure"));

            //Appending LinePriceInfo element
            Element eleLinePriceInfoinDoc=GCXMLUtil.createElement(inDoc, "LinePriceInfo", null);
            eleOrderLineinDoc.appendChild(eleLinePriceInfoinDoc);
            eleLinePriceInfoinDoc.setAttribute(GCConstants.IS_PRICE_LOCKED, eleLinePriceInfo.getAttribute(GCConstants.IS_PRICE_LOCKED));
            eleLinePriceInfoinDoc.setAttribute(GCConstants.LIST_PRICE, eleLinePriceInfo.getAttribute(GCConstants.LIST_PRICE));
            eleLinePriceInfoinDoc.setAttribute(GCConstants.UNIT_PRICE, eleLinePriceInfo.getAttribute(GCConstants.UNIT_PRICE));
            Element eleLineChargesinDoc=GCXMLUtil.getElementByXPath(inDoc,"Order/OrderLines/OrderLine/LineCharges");
            NodeList nlLineCharge=XPathAPI.selectNodeList(eleOrderLineOrderDoc,"LineCharges/LineCharge");
            int len=nlLineCharge.getLength();
            for(int i=0;i<len;i++){
              Element eleLineChargeOrderDoc= (Element)nlLineCharge.item(i);
              String sChargeCategory=eleLineChargeOrderDoc.getAttribute(GCConstants.CHARGE_CATEGORY);
              Element eleLineChargeinDoc=GCXMLUtil.getElementByXPath(inDoc, "Order/OrderLines/OrderLine/LineCharges/LineCharge[@ChargeCategory='"+sChargeCategory+"']");
              if(YFCCommon.isVoid(eleLineChargeinDoc)){
                //Appending LineCharge elements
                eleLineChargeinDoc=GCXMLUtil.createElement(inDoc, "LineCharge", null);
                eleLineChargesinDoc.appendChild(eleLineChargeinDoc);
                eleLineChargeinDoc.setAttribute(GCConstants.CHARGE_CATEGORY, eleLineChargeOrderDoc.getAttribute(GCConstants.CHARGE_CATEGORY));
                eleLineChargeinDoc.setAttribute(GCConstants.CHARGE_NAME, eleLineChargeOrderDoc.getAttribute(GCConstants.CHARGE_NAME));
                eleLineChargeinDoc.setAttribute(GCConstants.CHARGE_PER_LINE, eleLineChargeOrderDoc.getAttribute(GCConstants.CHARGE_PER_LINE));
                eleLineChargeinDoc.setAttribute(GCConstants.CHARGE_PER_UNIT, eleLineChargeOrderDoc.getAttribute(GCConstants.CHARGE_PER_UNIT));
              }
            }
          }else{
            eleOrderLineinDoc=GCXMLUtil.createElement(inDoc, "OrderLine", null);
            eleOrderLinesinDoc.appendChild(eleOrderLineinDoc);
            eleOrderLineinDoc.setAttribute(GCConstants.ORDER_LINE_KEY, eleOrderLineOrderDoc.getAttribute(GCConstants.ORDER_LINE_KEY));
            eleOrderLineinDoc.setAttribute(GCConstants.ORDERED_QTY, eleOrderLineOrderDoc.getAttribute(GCConstants.ORDERED_QTY));
            Element eleExtn=GCXMLUtil.createElement(inDoc, "Extn", null);
            eleOrderLineinDoc.appendChild(eleExtn);
            eleExtn.setAttribute(GCConstants.EXTN_IS_FREE_GIFT_ITEM, GCXMLUtil.getAttributeFromXPath(eleOrderLineOrderDoc,"Extn/@ExtnIsFreeGiftItem"));
            Element eleItem=GCXMLUtil.createElement(inDoc, "Item", null);
            eleOrderLineinDoc.appendChild(eleItem);
            eleItem.setAttribute(GCConstants.ITEM_ID, GCXMLUtil.getAttributeFromXPath(eleOrderLineOrderDoc,"Item/@ItemID"));
            eleItem.setAttribute(GCConstants.UNIT_OF_MEASURE, GCXMLUtil.getAttributeFromXPath(eleOrderLineOrderDoc,"Item/@UnitOfMeasure"));
            // Appending LinePriceInfo element
            Element eleLinePriceInfoinDoc=GCXMLUtil.createElement(inDoc, "LinePriceInfo", null);
            eleOrderLineinDoc.appendChild(eleLinePriceInfoinDoc);
            eleLinePriceInfoinDoc.setAttribute(GCConstants.IS_PRICE_LOCKED, eleLinePriceInfo.getAttribute(GCConstants.IS_PRICE_LOCKED));
            eleLinePriceInfoinDoc.setAttribute(GCConstants.LIST_PRICE, eleLinePriceInfo.getAttribute(GCConstants.LIST_PRICE));
            eleLinePriceInfoinDoc.setAttribute(GCConstants.UNIT_PRICE, eleLinePriceInfo.getAttribute(GCConstants.UNIT_PRICE));
            Element eleLineCharges=GCXMLUtil.createElement(inDoc, "LineCharges", null);
            eleOrderLineinDoc.appendChild(eleLineCharges);
            NodeList nlLineCharge=XPathAPI.selectNodeList(eleOrderLineOrderDoc,"LineCharges/LineCharge");
            int len=nlLineCharge.getLength();
            for(int i=0;i<len;i++){
              //Appending LineCharge elements
              Element eleLineChargeOrderDoc= (Element)nlLineCharge.item(i);
              Element eleLineCharge=GCXMLUtil.createElement(inDoc, "LineCharge", null);
              eleLineCharges.appendChild(eleLineCharge);
              eleLineCharge.setAttribute(GCConstants.CHARGE_CATEGORY, eleLineChargeOrderDoc.getAttribute(GCConstants.CHARGE_CATEGORY));
              eleLineCharge.setAttribute(GCConstants.CHARGE_NAME, eleLineChargeOrderDoc.getAttribute(GCConstants.CHARGE_NAME));
              eleLineCharge.setAttribute(GCConstants.CHARGE_PER_LINE, eleLineChargeOrderDoc.getAttribute(GCConstants.CHARGE_PER_LINE));
              eleLineCharge.setAttribute(GCConstants.CHARGE_PER_UNIT, eleLineChargeOrderDoc.getAttribute(GCConstants.CHARGE_PER_UNIT));
            }
          }
        }
      }
      LOGGER.debug("Exiting From GCBeforeChangeOrderUE.appendOrderLines Output Document"
          + GCXMLUtil.getXMLString(inDoc));

    } catch (Exception e) {

      LOGGER.error(
          "Error,Inside catch block of GCBeforeChangeOrderUE.addCharges()",
          e);
      throw new GCException(e); }
  }
  // Phase-2--GCStore-328-Start.
  private void stampOrderHeaderAttributes(YFSEnvironment env, Document inDoc,
      YFCDocument orderDetailsDoc) {
    String sSetTaxExemptAttributesAs = GCConstants.FLAG_Y;
    YFCDocument orderInDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleOrder = orderInDoc.getDocumentElement();
    String sIsStoreUser = eleOrder
        .getAttribute(GCXmlLiterals.IS_STORE_USER);
    YFCElement eleOrderExtn = eleOrder.getChildElement(GCConstants.EXTN,
        true);

    String sCustomerID = eleOrder.getAttribute(GCXmlLiterals.BILL_TO_ID);
    String sEnterpriseCode = eleOrder
        .getAttribute(GCXmlLiterals.ENTERPRISE_CODE);
    YFCDocument docCustomerDetailIp = YFCDocument
        .createDocument(GCConstants.CUSTOMER);
    YFCElement eleRootAPIInput = docCustomerDetailIp.getDocumentElement();
    eleRootAPIInput.setAttribute(GCConstants.CUSTOMER_ID, sCustomerID);
    eleRootAPIInput.setAttribute(GCConstants.ORGANIZATION_CODE,
        sEnterpriseCode);
    YFCDocument sInputTmplAPI = YFCDocument
        .getDocumentFor(GCConstants.GET_CUSTOMER_LIST_TEMPLATE);
    YFCDocument outDocCustomerList = GCCommonUtil.invokeAPI(env,
        GCConstants.GET_CUSTOMER_LIST, docCustomerDetailIp,
        sInputTmplAPI);
    YFCElement eleCustomerList = outDocCustomerList.getDocumentElement();
    YFCElement eleCustomer = eleCustomerList
        .getChildElement(GCConstants.CUSTOMER);
    YFCElement eleExtnCustomer = eleCustomer.getChildElement(GCConstants.EXTN);
    String sExtnSourceCustomerID = eleExtnCustomer.getAttribute(GCConstants.EXTN_SOURCE_CUSTOMER_ID);
    if (!YFCCommon.isStringVoid(sExtnSourceCustomerID)){
      eleOrderExtn.setAttribute(GCConstants.EXTN_SOURCE_CUSTOMER_ID, sExtnSourceCustomerID);
    }

    /* GCSTORE 513 - Begin */
    YFCElement eleRootGetOrderList = orderDetailsDoc.getDocumentElement();
    YFCElement eleExtnGetOrderList = eleRootGetOrderList
        .getChildElement(GCConstants.ORDER);
    YFCElement eleOrderExtnOfGetOrderList = eleExtnGetOrderList
        .getChildElement(GCConstants.EXTN);
    String sSourceCode = eleOrderExtnOfGetOrderList
        .getAttribute(GCConstants.EXTN_SOURCE_CODE);

    String sRelationshipType = eleCustomer
        .getAttribute(GCConstants.RELATIONSHIP_TYPE);
    if (YFCCommon.equals(sRelationshipType, GCConstants.EMP)
        && YFCCommon.isVoid(sSourceCode)) {
      eleOrderExtn.setAttribute(GCConstants.EXTN_SOURCE_CODE,
          GCConstants.GC_EMPLOYEE_ORDER);
      eleOrderExtn.setAttribute("ExtnIsEmployeeOrder",
          GCConstants.FLAG_Y);
      /* GCSTORE 513 - End */
    }
    /* GCSTORE 786 - Start */
	
	 YFCElement eleExtnCustomer1 = eleCustomer.getChildElement(GCConstants.EXTN);
    String sExtnIsGcProCustomer = eleExtnCustomer1.getAttribute(GCConstants.EXTN_IS_GCPRO_CUSTOMER);
	String sExtnGcProStatus = eleExtnCustomer.getAttribute(GCConstants.EXTN_GCPRO_STATUS);
	

    if (YFCCommon.equals(GCConstants.FLAG_Y, sIsStoreUser)) {
      eleOrderExtn.setAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL,
          GCConstants.GCSTORE);
      sSetTaxExemptAttributesAs = GCConstants.FLAG_N;

      if (YFCCommon.equals(sRelationshipType, GCConstants.EMP)) {
        eleOrderExtn.setAttribute(GCConstants.EXTN_SOURCE_CODE,
            GCConstants.GC_EMPLOYEE_ORDER);
        eleOrderExtn.setAttribute("ExtnIsEmployeeOrder",
            GCConstants.FLAG_Y);
        eleOrder.setAttribute(GCConstants.ENTRY_TYPE, "CC");
      } else {
        eleOrderExtn.setAttribute(GCConstants.EXTN_SOURCE_CODE,
            GCConstants.GCSTORE);
        eleOrder.setAttribute(GCConstants.ENTRY_TYPE, "SPO Web");
      }
      // code fix for GCPRO-170
      //if (YFCCommon.equals(sExtnIsGcProCustomer, GCConstants.FLAG_Y) && YFCCommon.isVoid(sSourceCode)) {
       if (YFCCommon.equals(sExtnIsGcProCustomer, GCConstants.FLAG_Y) ) {	  
			if(YFCCommon.equals(sExtnGcProStatus, GCConstants.GC_PRO_PENDING) || (YFCCommon.equals(sExtnGcProStatus, GCConstants.GC_PRO_APPROVED))){
				eleOrderExtn.setAttribute(GCConstants.EXTN_SOURCE_CODE, GCConstants.GC_PRO_SPO);
				// code fix for GCPRO-287
				eleOrder.setAttribute(GCConstants.ENTRY_TYPE, "ProSPOWeb");
		}
	 // code fix for GCPRO-170		
	  }
	 
      YFCElement eleCustomerSchedulingPreferences = eleCustomer
          .getChildElement(GCXmlLiterals.CUSTOMER_SCHEDULING_PREFERENCES);

      if(!YFCCommon.isVoid(eleCustomerSchedulingPreferences)){
        String fShipComplete = eleCustomerSchedulingPreferences
            .getAttribute(GCXmlLiterals.IS_SHIP_COMPLETE);
        eleOrder.setAttribute(GCXmlLiterals.IS_SHIP_COMPLETE, fShipComplete);
      }

      eleOrderExtn.setAttribute(GCXmlLiterals.EXTN_FTC_EXEMPT,
          GCConstants.FLAG_Y);
      eleOrderExtn.setAttribute(
          GCXmlLiterals.EXTN_TAX_EXEMPT_REQUEST_STATUS,
          GCConstants.FLAG_N);
      /* GCSTORE 786 - End */
    } else {

	 YFCElement eleCustomerExtn = eleCustomer
          .getChildElement(GCXmlLiterals.EXTN);

      String sStateAPIOutput = eleCustomerExtn
          .getAttribute(GCXmlLiterals.EXTN_TAX_EXEMPT_STATE);

		 

      String sTaxExemptRequestStatus = eleCustomerExtn
          .getAttribute(GCXmlLiterals.EXTN_TAX_EXEMPT_REQUEST_STATUS);
      String sExpiryDate = eleCustomerExtn
          .getAttribute(GCXmlLiterals.EXTN_TAX_EXEMPT_REQ_VALID_UNTIL);
      boolean bIsDateValid = checkValidity(sExpiryDate);
      if (YFCCommon.equals(sTaxExemptRequestStatus,
          GCConstants.APPROVED_LOWERCASE) && (bIsDateValid)) {
        YFCElement elePersonInfoShipTo = eleOrder
            .getChildElement(GCXmlLiterals.PERSON_INFO_SHIP_TO);
        if(!YFCCommon.isVoid(elePersonInfoShipTo)){
          String sOrderStateCode = elePersonInfoShipTo
              .getAttribute(GCXmlLiterals.STATE);
          if (YFCCommon.equals(sOrderStateCode, sStateAPIOutput)) {
            sSetTaxExemptAttributesAs = GCConstants.FLAG_Y;
          } else {
            sSetTaxExemptAttributesAs = GCConstants.FLAG_N;
          }
        } else {
          sSetTaxExemptAttributesAs = GCConstants.FLAG_N;
        }
      }else {
        sSetTaxExemptAttributesAs = GCConstants.FLAG_N;
      }
    }
    if (YFCCommon.equals(sSetTaxExemptAttributesAs, GCConstants.FLAG_Y)) {
      eleOrder.setAttribute(GCConstants.TAX_EXEMPT_FLAG, GCConstants.YES);
      eleOrderExtn.setAttribute(
          GCXmlLiterals.EXTN_IS_TAX_EXEMPT_EDITABLE, GCConstants.YES);
    } else {
      eleOrder.setAttribute(GCXmlLiterals.TAX_EXEMPT_FLAG, GCConstants.NO);
      eleOrderExtn.setAttribute(
          GCXmlLiterals.EXTN_IS_TAX_EXEMPT_EDITABLE, GCConstants.NO);
    }
    eleOrder.removeAttribute(GCXmlLiterals.IS_STORE_USER);
    orderInDoc.getDocument();
  }

  private boolean checkValidity(String sExpiryDate) {
    boolean response = false;
    try {
      if (!YFCCommon.isVoid(sExpiryDate)) {
        DateFormat formatter = new SimpleDateFormat(
            "yyyy-MM-dd'T'HH:mm:ss");
        Date expDate = formatter.parse(sExpiryDate);
        Date currentDate = new Date();
        if (expDate.after(currentDate)) {
          response = true;
        } else {
          response = false;
        }
      }
    } catch (ParseException yfsEx) {
      YFSException e = new YFSException(yfsEx.getMessage());
      LOGGER.error(
          "Date parsing failed for Tax Exempt date validity check",
          yfsEx);
      throw e;
    }
    return response;
  }
  // //Phase-2--GCStore-328-End.
  private void removeAdditionalDeletedOrderLines(Document inDoc) {
    LOGGER.beginTimer("Entering removeAdditionalDeletedOrderLines()");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input Document to removeAdditionalDeletedOrderLines:"
          + inDoc.toString());
    }
    List<String> orderLineKeyList = new ArrayList<String>();
    YFCDocument inputDoc=YFCDocument.getDocumentFor(inDoc);
    YFCElement eleOrder=inputDoc.getDocumentElement();
    YFCElement eleOrderlines=eleOrder.getChildElement("OrderLines");
    if (!YFCCommon.isVoid(eleOrderlines)) {
      YFCNodeList<YFCElement> nlOrderLine=eleOrderlines.getElementsByTagName("OrderLine");
      int nodeListLength = nlOrderLine.getLength();
      for (int k = 0; k < nodeListLength; k++) {
        YFCElement eleOrderLine = nlOrderLine.item(k);
        String sOrderLineKey = eleOrderLine.getAttribute("OrderLineKey");
        boolean bAction = YFCCommon.equalsIgnoreCase(
            eleOrderLine.getAttribute("Action"), "REMOVE");
        boolean isUnitqueOrderLine = true;
        if (bAction) {
          if (orderLineKeyList.size() != 0) {
            isUnitqueOrderLine = !orderLineKeyList
                .contains(sOrderLineKey);
          }
          if (isUnitqueOrderLine) {
            orderLineKeyList.add(sOrderLineKey);
          }else{
            eleOrderLine.getParentNode().removeChild(eleOrderLine);
            k--;
            nodeListLength--;
          }
        }
      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Output Document from removeAdditionalDeletedOrderLines:"
          + inDoc.toString());
    }
  }


  // Fix for GCSTORE-4850
  public void applyAssociationHold(YFSEnvironment env, Document inDoc, String sExtnIsNonDependentWarranty){

      if(LOGGER.isDebugEnabled())
      {
      LOGGER.debug("sExtnIsNonDependentWarranty ::"+sExtnIsNonDependentWarranty);
      }
    boolean bholdAlreadyApplied = false;
    //  boolean bIsIndependentWarrantyLinePresent = false;

    Element eleRoot = inDoc.getDocumentElement();

    // Check if hold is already present on the order.
    String sOrderNo = eleRoot.getAttribute(GCConstants.ORDER_NO);
    String sOrderHeaderKey = eleRoot.getAttribute(GCConstants.ORDER_HEADER_KEY);

    if(!YFCCommon.isVoid(sOrderNo) || !YFCCommon.isVoid(sOrderHeaderKey)){

      YFCDocument getOrderListIp = YFCDocument.createDocument(GCConstants.ORDER);
      YFCElement eleGetOrderListIp = getOrderListIp.getDocumentElement();
      eleGetOrderListIp.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
      eleGetOrderListIp.setAttribute(GCConstants.ORDER_NO, sOrderNo);
      YFCDocument getOrderListTemp = YFCDocument.getDocumentFor("<OrderList><Order OrderNo=''><OrderHoldTypes><OrderHoldType/></OrderHoldTypes></Order></OrderList>");
      YFCDocument getOrderListOpDoc =
          GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, getOrderListIp, getOrderListTemp);

      YFCElement eleOrderOp = getOrderListOpDoc.getElementsByTagName(GCConstants.ORDER).item(0);

      YFCNodeList<YFCElement> nlOrderHoldType = eleOrderOp.getElementsByTagName(GCConstants.ORDER_HOLD_TYPE);
      if(!YFCCommon.isVoid(nlOrderHoldType) && nlOrderHoldType.getLength()>0){
        for(int i=0;i<nlOrderHoldType.getLength();i++){
          YFCElement eleOrderHoldType = nlOrderHoldType.item(i);
          String sHoldType = eleOrderHoldType.getAttribute(GCConstants.HOLD_TYPE);
          String sStatus =  eleOrderHoldType.getAttribute(GCConstants.STATUS);
          if(YFCCommon.equals(sHoldType, "ASSOCIATION_HOLD") && YFCCommon.equals(sStatus, "1100")){
            bholdAlreadyApplied =  true;
             break;
            }
           
          }
        }
      }

      /*    // Check if independent warranty line is present.
      NodeList nlOrderLines = eleRoot.getElementsByTagName(GCConstants.ORDER_LINE);
      for(int i=0;i<nlOrderLines.getLength();i++){
          Element eleOrderLine = (Element) nlOrderLines.item(i);

          NodeList nlExtn = eleOrderLine.getElementsByTagName(GCConstants.EXTN);
          if(nlExtn.getLength()>0){
              Element eleExtn = (Element) nlExtn.item(0);
              String sExtnIsWarrantyItem = eleExtn.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
              String sDependentOnLineKey = eleOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
              if(YFCCommon.isVoid(sDependentOnLineKey) && YFCCommon.equals(sExtnIsWarrantyItem, GCConstants.YES)){
                  bIsIndependentWarrantyLinePresent = true;
                  break;
              }
          }
      }*/
        if(LOGGER.isDebugEnabled())
        {
            LOGGER.debug("bholdAlreadyApplied ::" +bholdAlreadyApplied);
        }
      if(!bholdAlreadyApplied && GCConstants.YES.equals(sExtnIsNonDependentWarranty)){
          if(LOGGER.isDebugEnabled())
          {
          LOGGER.debug("Creating ASSOCIATION_HOLD");
          }
        Element eleOrderHoldTypes = (Element) eleRoot.getElementsByTagName(GCConstants.ORDER_HOLD_TYPES).item(0);
        if(YFCCommon.isVoid(eleOrderHoldTypes)){
          eleOrderHoldTypes = GCXMLUtil.createElement(inDoc, GCConstants.ORDER_HOLD_TYPES, false);
          eleRoot.appendChild(eleOrderHoldTypes);
        }

        Element eleOrderHoldType =  GCXMLUtil.createElement(inDoc, GCConstants.ORDER_HOLD_TYPE, true);
        eleOrderHoldTypes.appendChild(eleOrderHoldType);
        eleOrderHoldType.setAttribute(GCConstants.HOLD_TYPE, "ASSOCIATION_HOLD");
        eleOrderHoldType.setAttribute(GCConstants.STATUS, "1100");
        //  GCCommonUtil.invokeService(env, "GCWarrantyWithoutAssociationAlertService", GCXMLUtil.getDocument("<Order OrderNo='"+sOrderNo+"' OrderHeaderKey='"+sOrderHeaderKey+"'/>"));
      }
      else if (bholdAlreadyApplied && GCConstants.NO.equals(sExtnIsNonDependentWarranty))
      {
          if(LOGGER.isDebugEnabled())
          {
          LOGGER.debug("Resolving ASSOCIATION_HOLD");
          }
          Element eleOrderHoldTypes = (Element) eleRoot.getElementsByTagName(GCConstants.ORDER_HOLD_TYPES).item(0);
          if(YFCCommon.isVoid(eleOrderHoldTypes)){
            eleOrderHoldTypes = GCXMLUtil.createElement(inDoc, GCConstants.ORDER_HOLD_TYPES, false);
            eleRoot.appendChild(eleOrderHoldTypes);
          }

          Element eleOrderHoldType =  GCXMLUtil.createElement(inDoc, GCConstants.ORDER_HOLD_TYPE, true);
          eleOrderHoldTypes.appendChild(eleOrderHoldType);
          eleOrderHoldType.setAttribute(GCConstants.HOLD_TYPE, "ASSOCIATION_HOLD");
          eleOrderHoldType.setAttribute(GCConstants.STATUS, "1300");
      }
     
    }

public void setProperties(Properties properties) throws Exception{
	this.properties = properties;
}
  }
  

    




