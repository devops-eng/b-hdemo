/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               06/02/2014        Last Name, First Name		JIRA No: Description of issue.
			 1.1              06/05/2015        Infosys Ltd.               OMS 5537 OMS stamping incorrect ENTRY_TYPE on some orders causing DAX to incorrectly assign Order Manager
		     2.0               12/03/2015        Mittal, Yashu            GCSTORE-556: Design � Store Returns (pt. 2) � Payment Confirmation UI through completion
		     2.1               14/05/2015        Singh, Gurpreet          GCSTORE-2475: Return Addresses are blank when return is created for Borderfree orders in COM UI.
			 1.2				18/05/2015		 Infosys Ltd.				OMS 5329:Refund amount is greater than the original order amount for an order containing kit item.
			 1.2				21/05/2015		 Infosys Ltd.				OMS 5580:Unable to setup return on GCSTORE web order
 *#################################################################################################################################################################
 */
package com.gc.userexit;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.api.GCUpdateRefundShippingTax;
import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.yfs.japi.ue.YFSBeforeChangeOrderUE;

/**
 * @author <a href="mailto:email Address">Name</a>
 */
public class GCBeforeChangeReturnOrderUE implements YFSBeforeChangeOrderUE {

  private static final Logger LOGGER = Logger
  .getLogger(GCBeforeChangeReturnOrderUE.class.getName());
  final private String CLASS_NAME = GCBeforeChangeReturnOrderUE.class.getName();

  /**
   *
   * Description of beforeChangeOrder
   *
   * @param env
   * @param inDoc
   * @return
   * @throws YFSUserExitException
   * @see com.yantra.yfs.japi.ue.YFSBeforeChangeOrderUE#beforeChangeOrder(com.yantra.yfs.japi.YFSEnvironment,
   *      org.w3c.dom.Document)
   */
  @Override
  public Document beforeChangeOrder(YFSEnvironment env, Document inDoc)
      throws YFSUserExitException {
    final String logInfo = "GCBeforeChangeReturnOrderUE :: beforeChangeOrder :: ";
    Element eleRoot = inDoc.getDocumentElement();
    String sDocumentType = eleRoot.getAttribute(GCConstants.DOCUMENT_TYPE);
    String sOrderHeaderKey = eleRoot
        .getAttribute(GCConstants.ORDER_HEADER_KEY);
    String sDraftOrder = eleRoot.getAttribute("DraftOrderFlag");
    Document salesOrderDetails = null;
    String sSalesOrderHeaderKey = GCXMLUtil.getAttributeFromXPath(inDoc,
        "Order/OrderLines/OrderLine/@DerivedFromOrderHeaderKey");
    String sCheckIfReturnable = GCXMLUtil.getAttributeFromXPath(inDoc, "Order/@CheckIfReturnable");
    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
	YFCElement eleOrder = yfcInDoc.getDocumentElement();
	YFCNodeList<YFCElement> nlOrderLines = eleOrder.getElementsByTagName("OrderLine");
    try {
      if(!YFCCommon.isVoid(sSalesOrderHeaderKey)){
        salesOrderDetails = getSalesOrderDetails(env, sSalesOrderHeaderKey);

      }
    } catch (GCException ex) {
      LOGGER
      .error(
          "The error is in getting sales order details for store returns",
          ex);
      throw new GCException(ex).getYFSUserExitException();
    }

    if(YFCCommon.equals(GCConstants.YES, sCheckIfReturnable) && !YFCCommon.isVoid(salesOrderDetails)){
    	YFCNodeList<YFCElement> nlSOOrderLines = YFCDocument.getDocumentFor(salesOrderDetails).getElementsByTagName("OrderLine");
    	Map<String , YFCElement> assignMap= new HashMap<String, YFCElement>();
    	for(YFCElement eleSOOrderLine : nlSOOrderLines){
    		String sOrderLineKey = eleSOOrderLine.getAttribute("OrderLineKey");
    		YFCElement eleExtn = eleSOOrderLine.getChildElement("Extn");
    		assignMap.put(sOrderLineKey, eleExtn);
    		
    	}
    	for(YFCElement eleOrderLine : nlOrderLines){
    		YFCElement eleDerivedFrom = eleOrderLine.getChildElement("DerivedFrom");
    		LOGGER.debug("OrderLine is : "+ eleOrderLine.toString());
    		String sOrderLineKey = eleOrderLine.getAttribute("DerivedFromOrderLineKey");
    		if(YFCCommon.isVoid(sOrderLineKey)){
    			sOrderLineKey = eleDerivedFrom.getAttribute("OrderLineKey");
    		}
    		YFCElement eleSOOrderLineExtn = assignMap.get(sOrderLineKey);
    		LOGGER.debug("OrderLine is : "+ eleOrderLine.toString());
    		LOGGER.debug("Sales Order Extn is : "+ eleSOOrderLineExtn.toString());
    		if(!YFCCommon.isVoid(eleSOOrderLineExtn)){
    			String sExtnIsAmountCollected = eleSOOrderLineExtn.getAttribute("ExtnIsAmountCollected");
    			String sExtnIsStoreFulfilled = eleSOOrderLineExtn.getAttribute("ExtnIsStoreFulfilled");
    			String sExtnPOSSalesTicketNo = eleSOOrderLineExtn.getAttribute("ExtnPOSSalesTicketNo");
    			if(YFCCommon.equals(GCConstants.YES, sExtnIsStoreFulfilled)){
    				if(YFCCommon.isVoid(sExtnPOSSalesTicketNo)){
    					 YFCException yfs = new YFCException("GCRET00001");
		                 yfs.setErrorDescription("Sales order not invoiced. Please allow system to complete the order before processing return.");
		                 throw yfs;
    				}
    			}
    			else if (YFCCommon.equals(sExtnIsAmountCollected, GCConstants.NO)){
    				 	YFCException yfs = new YFCException("GCRET00001");
    	                yfs.setErrorDescription("Sales order not invoiced. Please allow system to complete the order before processing return.");
    	                throw yfs;
    			}
    		}
    	}
    }
    
    
    // Phase2 Starts
    String sStampDispositionCode = eleRoot
        .getAttribute(GCXmlLiterals.STAMP_DISPOSITION_CODE);
    if (!YFCCommon.isVoid(sStampDispositionCode) && YFCCommon.equals(sStampDispositionCode, GCConstants.YES)) {
      stampingDispositionCode(env, inDoc);
	  if(!YFCCommon.isVoid(salesOrderDetails)){
			inDoc = copyAddressFromSalesToReturn(salesOrderDetails, inDoc);
		}
    }
    
   
    // Phase2 Ends

    // Start Code to handle Regular return as NPR when reaason change
    try {
      String strGetOrderDetails = "<Order OrderHeaderKey='"
          + sOrderHeaderKey + "'/>";
      Document inDocGOD = GCXMLUtil.getDocument(strGetOrderDetails);
      Document outDocGOD = GCCommonUtil.invokeAPI(env,
          GCConstants.GET_ORDER_DETAILS, inDocGOD);
      String sellerOrgCode = GCXMLUtil.getAttributeFromXPath(outDocGOD,
          "//Order/@SellerOrganizationCode");
      // GCSTORE-2475: Start
      if (!YFCCommon.isVoid(salesOrderDetails)) {
        inDoc =  updateBorderfreeROAdd(inDoc,outDocGOD,salesOrderDetails);
      }

      // GCSTORE-2475: End
      if (!YFCCommon.isVoid(sellerOrgCode)
          && GCConstants.FLAG_Y.equals(sDraftOrder)) {
       // eleRoot.setAttribute(GCConstants.SELLER_ORGANIZATION_CODE,            sellerOrgCode);
      }
      Element eleOrderLn = GCXMLUtil.getElementByXPath(outDocGOD,
          "Order/OrderLines/OrderLine");
      if (!YFCCommon.isVoid(eleOrderLn)) {
        NodeList nlOrderLine = GCXMLUtil.getNodeListByXpath(inDoc,
            "Order/OrderLines/OrderLine");
        // Fix for 5377 : Change Return Reason code to new three digit code.
        String[] arrNPRCode = new String[] { "20", "24", "28", "32",
            "36", "37", "38", "45", "46", "47", "48", "49", "50",
            "51", "57", "82", "83", "84" , "200", "201", "202", "203", "204"};
        Set<String> stNPPRCode = new HashSet<String>(Arrays
            .asList(arrNPRCode));
        Document docChangeOrderStatus = GCXMLUtil
            .createDocument(GCConstants.ORDER_STATUS_CHANGE);
        Element eleChangeOrderStatus = docChangeOrderStatus
            .getDocumentElement();
        eleChangeOrderStatus.setAttribute(GCConstants.ORDER_HEADER_KEY,
            sOrderHeaderKey);
        eleChangeOrderStatus.setAttribute(
            GCConstants.IGNORE_TRANSACTION_DEPENDENCIES,
            GCConstants.YES);
        eleChangeOrderStatus.setAttribute(GCConstants.TRANSACTION_ID,
            "ChangeNPROrderStatus.0003.ex");
        Element eleOrderLinesChngStatus = GCXMLUtil.createElement(
            docChangeOrderStatus, GCConstants.ORDER_LINES, null);
        eleChangeOrderStatus.appendChild(eleOrderLinesChngStatus);
        for (int i = 0; i < nlOrderLine.getLength(); i++) {
          Element eleorderLine = (Element) nlOrderLine.item(i);
          String sAction = eleorderLine
              .getAttribute(GCConstants.ACTION);
          if (!GCConstants.CREATE.equalsIgnoreCase(sAction)) {
            String strOLK = eleorderLine
                .getAttribute(GCConstants.ORDER_LINE_KEY);
            String strReturnReasonChange = eleorderLine
                .getAttribute("ReturnReason");
            if (!YFCCommon.isVoid(strReturnReasonChange)
                && stNPPRCode.contains(strReturnReasonChange)) {
              eleorderLine.setAttribute("LineType", "NPR");
              Element eleOrderLineChngStatus = GCXMLUtil
                  .createElement(docChangeOrderStatus,
                      GCConstants.ORDER_LINE, null);
              eleOrderLinesChngStatus
              .appendChild(eleOrderLineChngStatus);
              eleOrderLineChngStatus.setAttribute(
                  GCConstants.BASE_DROP_STATUS, "1100.100");
              eleOrderLineChngStatus.setAttribute(
                  GCConstants.ORDER_LINE_KEY, strOLK);
              eleOrderLineChngStatus.setAttribute(
                  GCConstants.CHANGE_FOR_ALL_AVAIL_QTY,
                  GCConstants.YES);
            }
          }
          Element eleOrderLine = GCXMLUtil.getElementByXPath(
              docChangeOrderStatus,
              "OrderStatusChange/OrderLines/OrderLine");
          if (!YFCCommon.isVoid(eleOrderLine)) {
            LOGGER.debug("input"
                + GCXMLUtil.getXMLString(docChangeOrderStatus));
            GCCommonUtil.invokeAPI(env,
                GCConstants.API_CHANGE_ORDER_STATUS,
                docChangeOrderStatus);
          }
        }
      }
    } catch (Exception ex) {
      LOGGER.error("The error is in Return Reason Fetching", ex);
      throw new GCException(ex).getYFSUserExitException();
    }
    // End Code to handle Regular return as NPR when reaason change

    Element eleHeaderCharge = null;
    String sExtnIsShipCharProrFlag = "";
    try {
      sExtnIsShipCharProrFlag = GCXMLUtil.getAttributeFromXPath(inDoc,
          "Order/Extn" + "/@ExtnIsShippingChargeProrated");
      eleHeaderCharge = GCXMLUtil
          .getElementByXPath(
              inDoc,
              "Order/HeaderCharges/HeaderCharge[@ChargeName='ShippingRefund' and @ChargeAmount!='0.00']");

    } catch (Exception ex) {
      LOGGER.error(logInfo, ex);
      YFSUserExitException e = new YFSUserExitException();
      e.setErrorDescription(ex.getMessage());
      throw e;
    }
    Object obj = env.getTxnObject("isProration");

    if (YFCCommon.isVoid(obj)
        && !YFCCommon.isVoid(eleHeaderCharge)
        && !GCConstants.FLAG_N
        .equalsIgnoreCase(sExtnIsShipCharProrFlag)) {

      GCUpdateRefundShippingTax ob = new GCUpdateRefundShippingTax();
      try {
        Document docChangeOrder = ob.updateRefundShippingTax(env,
            inDoc, eleHeaderCharge, obj);
        return docChangeOrder;
      } catch (Exception ex) {
        LOGGER.error(logInfo, ex);
        YFSUserExitException e = new YFSUserExitException();
        e.setErrorDescription(ex.getMessage());
        throw e;
      }

    }
    try {
      // Shipping Proration Logic
      String sExtnIsShippingChargeProrated = GCXMLUtil
          .getAttributeFromXPath(inDoc, "Order/Extn"
              + "/@ExtnIsShippingChargeProrated");
      if (GCConstants.NO.equalsIgnoreCase(sExtnIsShippingChargeProrated)) {
        GCCommonUtil.invokeService(env,
            GCConstants.GC_PROCESS_SHPG_PRORATION_SERVICE, inDoc);
      }

      if (GCConstants.RETURN_ORDER_DOCUMENT_TYPE.equals(sDocumentType)) {
        String sOrderHeaderKeyEnv = sOrderHeaderKey + "_"
            + sDocumentType;

        if (YFCCommon.isVoid(sSalesOrderHeaderKey)) {
          sSalesOrderHeaderKey = GCXMLUtil
              .getAttributeFromXPath(inDoc,
                  "Order/OrderLines/OrderLine/@DerivedFromOrderHeaderKey");
        }
        if (!YFCCommon.isVoid(sSalesOrderHeaderKey)
            && YFCCommon.isVoid(env
                .getTxnObject(sOrderHeaderKeyEnv))) {
          /*
           * This method is used to copy sales order attributes like
           * ExtnDaxOrderNo to return order
           */

          copyingSalesOrderAttributes(env, salesOrderDetails,
              sOrderHeaderKeyEnv, inDoc, eleRoot);
        }
      }
    } catch (Exception e) {
      LOGGER.error(logInfo, e);
      YFSUserExitException ex = new YFSUserExitException();
      ex.setErrorDescription(e.getMessage());
    }

    if (GCConstants.YES.equals(eleRoot
        .getAttribute(GCConstants.EXTN_ADD_WARRANTY_LINE_TO_RO))) {
      LOGGER.debug("GCBeforeChangeReturnOrderUE: Inside warranty to RO");
      env.setTxnObject(GCConstants.ADD_WARRANTY_LINE_TO_RO,
          GCConstants.YES);
      eleRoot.removeAttribute(GCConstants.EXTN_ADD_WARRANTY_LINE_TO_RO);
    }
    // OMS-5329: Begins
     stampIsLinePriceForInformationOnlyflag(inDoc);
    // OMS-5329: Ends
    return inDoc;
  }

/**
   * @author Infosys Ltd.
   *
   *
   *
   * @param inDoc
   * @throws GCException
   */
  private void stampIsLinePriceForInformationOnlyflag(Document inDoc) {
    try {
      LOGGER.debug("Entering GCBeforeChangeOrderUE.stampIsLinePriceForInformationOnlyflag() method ");
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("stampIsLinePriceForInformationOnlyflag() method, Input document is"
            + GCXMLUtil.getXMLString(inDoc));
      }
      Element eleOrder = inDoc.getDocumentElement();
      String sDocumentType = eleOrder.getAttribute(GCConstants.DOCUMENT_TYPE);

      if (GCConstants.RETURN_ORDER_DOCUMENT_TYPE.equalsIgnoreCase(sDocumentType)) {
        NodeList nlOrderLine = GCXMLUtil.getNodeListByXpath(inDoc, "Order/OrderLines/OrderLine");
        int iOrderLine = nlOrderLine.getLength();
        for (int iCounter = 0; iCounter < iOrderLine; iCounter++) {
          Element eleOrderLine = (Element) nlOrderLine.item(iCounter);
          if (!YFCCommon.isVoid(eleOrderLine)) {
            String sAction = eleOrderLine.getAttribute(GCConstants.ACTION);
            Element eleBundleParentLine = GCXMLUtil.getFirstElementByName(eleOrderLine, GCConstants.BUNDLE_PARENT_LINE);

            if (!YFCCommon.isVoid(eleBundleParentLine) && (GCConstants.CREATE.equalsIgnoreCase(sAction))) {
              Element eleLinePriceInfo = GCXMLUtil.getFirstElementByName(eleOrderLine, GCConstants.LINE_PRICE_INFO);
              if (!YFCCommon.isVoid(eleLinePriceInfo)) {

                if (LOGGER.isDebugEnabled()) {
                  LOGGER.debug(CLASS_NAME + "Stamping the flag for child Line" + iCounter);
                }

                eleLinePriceInfo.setAttribute(GCConstants.IS_LINE_PRICE_FOR_INFORMATION_ONLY, "Y");
              }
            }
          }
        }

      }

      LOGGER
      .debug("stampIsLinePriceForInformationOnlyflag() method,Output document is" + GCXMLUtil.getXMLString(inDoc));
      LOGGER.debug("Exiting GCBeforeChangeOrderUE.stampIsLinePriceForInformationOnlyflag() method ");
    }

    catch (Exception ex) {
      YFSUserExitException e = new YFSUserExitException();
      e.setErrorDescription(ex.getMessage());
      throw ex;
    }

  }
  private void copyingSalesOrderAttributes(YFSEnvironment env,
      Document docGetOrderListOP, String sOrderHeaderKeyEnv,
      Document inDoc, Element eleRoot) throws GCException {

    try {

      env.setTxnObject(sOrderHeaderKeyEnv, docGetOrderListOP);
      // to correct the seller on return
      String lineAction = GCXMLUtil.getAttributeFromXPath(inDoc,
          "Order/OrderLines/OrderLine/@Action");
      String sellerOrgCode = GCXMLUtil.getAttributeFromXPath(
          docGetOrderListOP,
          "OrderList/Order/@SellerOrganizationCode");
 //OMS-5580: Starts
      String strPaymentRuleID =
    	        GCXMLUtil.getAttributeFromXPath(docGetOrderListOP, "OrderList/Order/@PaymentRuleId");
      //OMS-5580: Ends
       if (GCConstants.CREATE.equals(lineAction) && !YFCCommon.isVoid(sellerOrgCode) && !YFCCommon.isVoid(strPaymentRuleID)) {
        //eleRoot.setAttribute(GCConstants.SELLER_ORGANIZATION_CODE, sellerOrgCode);
 		eleRoot.setAttribute(GCConstants.PAYMENT_RULE_ID, strPaymentRuleID);
        //GCSTORE-2259 Fix: Start

        if(YFCCommon.equals(sellerOrgCode, GCConstants.INTL_WEB)){
          eleRoot.setAttribute("PaymentRuleId", "GCBorderfreePayRule");
        }

        //GCSTORE-2259 Fix: End

        String strSourceCode = GCXMLUtil.getAttributeFromXPath(
            docGetOrderListOP,
            "OrderList/Order/Extn/@ExtnSourceCode");
        String strDaxCustomerId = GCXMLUtil.getAttributeFromXPath(
            docGetOrderListOP,
            "OrderList/Order/Extn/@ExtnDAXCustomerID");
        Element extnEle = GCXMLUtil.getElementByXPath(inDoc,
            "Order/Extn");

        if (YFCCommon.isVoid(extnEle)) {
          extnEle = inDoc.createElement(GCConstants.EXTN);
          eleRoot.appendChild(extnEle);
        }

        if (!YFCCommon.isVoid(strSourceCode)) {
          extnEle.setAttribute("ExtnSourceCode", strSourceCode);
        }

        if (!YFCCommon.isVoid(strDaxCustomerId)) {
          extnEle.setAttribute("ExtnDAXCustomerID", strDaxCustomerId);
        }
        //Start: 5537::To stamp entry type on the RO by fetching it from SO

  final String sReturnOrderEntryType= inDoc.getDocumentElement().getAttribute(GCConstants.ENTRY_TYPE);
        String strSOEntryType =
            GCXMLUtil.getAttributeFromXPath(docGetOrderListOP, "OrderList/Order/@EntryType");

        Element eleOrder=  inDoc.getDocumentElement();
        if (!YFCCommon.isVoid(strSOEntryType)) {
          eleOrder.setAttribute(GCConstants.ENTRY_TYPE, strSOEntryType);
        }
        LOGGER.debug("Exiting From GCBeforeChangeReturnOrderUE.copyingSalesOrderAttributes() outputDoc:"+GCXMLUtil.getXMLString(inDoc));
        //End:5537
      }
    } catch (Exception e) {
      LOGGER.error("Error in fetchExtnFulfillmentType method", e);
      throw new GCException(e);
    }

  }


  /**
   *
   * @param salesOrderDetails
   * @param inDoc
   * @return
   * @throws GCException
   */
  private Document copyAddressFromSalesToReturn(Document salesOrderDetails,
      Document inDoc) {

	  	
		YFCDocument getOrderListYfc = YFCDocument
				.getDocumentFor(salesOrderDetails);
		YFCDocument inputDocYfc = YFCDocument.getDocumentFor(inDoc);

		YFCElement eleSalesOrder = getOrderListYfc.getDocumentElement()
				.getChildElement(GCConstants.ORDER);
		
		// Copy Sourcing Classification from SO to RO : Start
		String sSourcingClassification = eleSalesOrder.getAttribute("SourcingClassification");
		if(!YFCCommon.isVoid(sSourcingClassification)){
			inputDocYfc.getDocumentElement().setAttribute("SourcingClassification", sSourcingClassification);
		}
		
		// Copy Sourcing Classification from SO to RO : End
		
		YFCElement elePersonInfoBillTo = eleSalesOrder
				.getChildElement(GCConstants.PERSON_INFO_BILL_TO);
		YFCElement elePersonInfoShipTo = eleSalesOrder
				.getChildElement(GCConstants.PERSON_INFO_SHIP_TO);

		YFCElement elePersonInfoBillToRO = inputDocYfc.getDocumentElement()
				.getChildElement(GCConstants.PERSON_INFO_BILL_TO);
		YFCElement elePersonInfoShipToRO = inputDocYfc.getDocumentElement()
				.getChildElement(GCConstants.PERSON_INFO_SHIP_TO);

		if (YFCCommon.isVoid(elePersonInfoBillToRO)) {

			elePersonInfoBillToRO = inputDocYfc.getDocumentElement()
					.createChild(GCConstants.PERSON_INFO_BILL_TO);

		}

		if (YFCCommon.isVoid(elePersonInfoShipToRO)) {

			elePersonInfoShipToRO = inputDocYfc.getDocumentElement()
					.createChild(GCConstants.PERSON_INFO_SHIP_TO);

		}
		// Fix to copy address Extn element on ReturnOrder : Start

		YFCElement elePersonInfoBillToExtn = elePersonInfoBillTo
				.getChildElement(GCConstants.EXTN);
		YFCElement elePersonInfoShipToExtn = elePersonInfoShipTo
				.getChildElement(GCConstants.EXTN);

		YFCElement elePersonInfoBillToROExtn = elePersonInfoBillToRO
				.getChildElement(GCConstants.EXTN);
		YFCElement elePersonInfoShipToROExtn = elePersonInfoShipToRO
				.getChildElement(GCConstants.EXTN);

		if (YFCCommon.isVoid(elePersonInfoBillToROExtn)) {

			elePersonInfoBillToROExtn = elePersonInfoBillToRO
					.createChild(GCConstants.EXTN);
		}

		if (YFCCommon.isVoid(elePersonInfoShipToROExtn)) {
			elePersonInfoShipToROExtn = elePersonInfoShipToRO
					.createChild(GCConstants.EXTN);
		}

		// Fix to copy address Extn element on ReturnOrder : End

		GCXMLUtil.copyAttributes(elePersonInfoBillTo, elePersonInfoBillToRO);
		GCXMLUtil.copyAttributes(elePersonInfoShipTo, elePersonInfoShipToRO);

		GCXMLUtil.copyAttributes(elePersonInfoBillToExtn,elePersonInfoBillToROExtn);
		GCXMLUtil.copyAttributes(elePersonInfoShipToExtn,elePersonInfoShipToROExtn);

    return inputDocYfc.getDocument();
  }

  /**
   *
   *
   *
   * @param env
   * @param sSalesOrderHeaderKey
   * @return
   * @throws GCException
   */

  private Document getSalesOrderDetails(YFSEnvironment env,
      String sSalesOrderHeaderKey) throws GCException {

    Document docGetOrderListIP = GCXMLUtil
        .createDocument(GCConstants.ORDER);
    docGetOrderListIP.getDocumentElement().setAttribute(
        GCConstants.ORDER_HEADER_KEY, sSalesOrderHeaderKey);
    Document docGetOrderListTmp = GCCommonUtil
        .getDocumentFromPath("/global/template/api/getOrderList.xml");
    Document docGetOrderListOP = GCCommonUtil.invokeAPI(env,
        GCConstants.API_GET_ORDER_LIST, docGetOrderListIP,
        docGetOrderListTmp);

    return docGetOrderListOP;
  }

  /**
   *
   * This method will stamp disposition code on orderline depending upon
   * multiple conditions.
   *
   * @param env
   * @param inDoc
   *
   */
  private void stampingDispositionCode(YFSEnvironment env, Document inDoc) {

    YFCElement eleRoot = YFCDocument.getDocumentFor(inDoc)
        .getDocumentElement();

    String sDraftOrder = eleRoot.getAttribute("DraftOrderFlag");
    String sOrderNo = eleRoot.getAttribute(GCConstants.ORDER_NO);
    if (YFCCommon.equals(sDraftOrder, GCConstants.YES)) {
      YFCElement eleOrderLines = eleRoot
          .getChildElement(GCConstants.ORDER_LINES);
      YFCIterable<YFCElement> nlOrderLines = eleOrderLines
          .getChildren(GCConstants.ORDER_LINE);

      for (YFCElement eleOrderLine : nlOrderLines) {

        String sDispositionCode = eleOrderLine
            .getAttribute(GCXmlLiterals.DISPOSITION_CODE);

        if (YFCCommon.isVoid(sDispositionCode)) {
          eleOrderLine.setAttribute("ConditionVariable1",
              GCConstants.STORE);
          YFCElement eleOrderLineExtn = eleOrderLine
              .getChildElement(GCConstants.EXTN);
          YFCElement eleItemDetails = eleOrderLine
              .getChildElement(GCConstants.ITEM_DETAILS);
          YFCElement eleItemPrimaryInformation = eleItemDetails
              .getChildElement(GCConstants.PRIMARY_INFORMATION);
          YFCElement eleItemExtn = eleItemDetails
              .getChildElement(GCConstants.EXTN);

          String sItemType = eleItemPrimaryInformation
              .getAttribute(GCConstants.ITEM_TYPE);
          String sExtnIsStoreFulfilled = eleOrderLineExtn
              .getAttribute(GCXmlLiterals.EXTN_IS_STORE_FULFILLED);
          String sExtnSalesFulfillLocation = eleOrderLineExtn.getAttribute("ExtnSalesFulfillLocation");
          String sNodeType = "";
          if (!YFCCommon.isVoid(sExtnSalesFulfillLocation)) {
            sNodeType = fetchNodeType(env, sExtnSalesFulfillLocation);
          }
          YFCDocument getCommonCodeListOutForItemType = GCCommonUtil
              .getCommonCodeListByTypeAndValue(
                  env,"GCApprovedItemType",GCConstants.GC, sItemType);

          if (getCommonCodeListOutForItemType.getDocumentElement().hasChildNodes()) {
            if (YFCCommon.equals(GCConstants.YES, sExtnIsStoreFulfilled)
                || YFCCommon.equals(GCConstants.STORE, sNodeType)) {
              eleOrderLine.setAttribute(
                  GCXmlLiterals.DISPOSITION_CODE,
                  GCConstants.KEEP_IN_STORE_DISPOSITION_CODE);
            } else if (!YFCCommon.equals(eleItemDetails.getAttribute(GCConstants.ITEM_ID), eleItemExtn.getAttribute(GCConstants.EXTN_POSITEM_ID))) {
              String sExtnPOSStatusCode = eleItemExtn
                  .getAttribute(GCXmlLiterals.EXTN_POS_STATUS_CODE);
              setPOSAttributes(env, sOrderNo, eleOrderLine, eleOrderLineExtn,
                  sExtnPOSStatusCode);
            } else {
              eleOrderLine.setAttribute(
                  GCXmlLiterals.DISPOSITION_CODE,
                  GCConstants.RETURNED_TO_STORE_DISPOSITION_CODE);
              eleOrderLineExtn.setAttribute(
                  GCXmlLiterals.EXTN_RETURN_VPO_NO, "VPO"
                  .concat(sOrderNo));
            }
          } else{
            eleOrderLine.setAttribute(
                GCXmlLiterals.DISPOSITION_CODE,
                GCConstants.BLANK);
          }
        }
      }
    }
  }

  public String fetchNodeType(YFSEnvironment env, String sExtnSalesFulfillLocation) {
    YFCDocument orgHeirarchyIndocYfc = YFCDocument.createDocument(GCConstants.ORGANIZATION);
    YFCElement eleOrganization = orgHeirarchyIndocYfc.getDocumentElement();
    eleOrganization.setAttribute(GCXmlLiterals.ORGANIZATION_KEY, sExtnSalesFulfillLocation);
    YFCDocument getOrgHeirarchyTmpl = YFCDocument.getDocumentFor((GCConstants.GET_ORG_HEIRARCHY_TEMPLATE));
    YFCDocument orgHeirarchyOutdocYfc =
        GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORG_HIERARCHY, orgHeirarchyIndocYfc, getOrgHeirarchyTmpl);
    YFCElement eleOrganizationYfc = orgHeirarchyOutdocYfc.getDocumentElement();
    String sNodeType = "";
    if (!YFCCommon.isVoid(eleOrganizationYfc)) {
      YFCElement eleNodeYfc = eleOrganizationYfc.getChildElement(GCConstants.NODE);
      // Fetching NodeType
      if (!YFCCommon.isVoid(eleNodeYfc)) {
        sNodeType = eleNodeYfc.getAttribute(GCXmlLiterals.NODE_TYPE);
      }
    }
    return sNodeType;
  }

  private void setPOSAttributes(YFSEnvironment env, String sOrderNo,
      YFCElement eleOrderLine, YFCElement eleOrderLineExtn,
      String sExtnPOSStatusCode) {
    if (!YFCCommon.isVoid(sExtnPOSStatusCode)) {
      YFCDocument getCommonCodeListOutForPOSStatusCode = GCCommonUtil
          .getCommonCodeListByTypeAndValue(
              env,
              GCConstants.GC_APPROVED_POS_STATUS_CODE,
              GCConstants.GC, sExtnPOSStatusCode);
      if (getCommonCodeListOutForPOSStatusCode.getDocumentElement().hasChildNodes()) {
        eleOrderLine
        .setAttribute(
            GCXmlLiterals.DISPOSITION_CODE,
            GCConstants.RECEIVE_IN_STORE_DISPOSITION_CODE);
        eleOrderLineExtn.setAttribute(
            GCXmlLiterals.EXTN_RETURN_RIS_NO, "RIS"
            .concat(sOrderNo));
      } else {
        eleOrderLine
        .setAttribute(
            GCXmlLiterals.DISPOSITION_CODE,
            GCConstants.RETURNED_TO_STORE_DISPOSITION_CODE);
        eleOrderLineExtn.setAttribute(
            GCXmlLiterals.EXTN_RETURN_VPO_NO, "VPO"
            .concat(sOrderNo));
      }
    } else {
      eleOrderLine
      .setAttribute(
          GCXmlLiterals.DISPOSITION_CODE,
          GCConstants.RETURNED_TO_STORE_DISPOSITION_CODE);
      eleOrderLineExtn.setAttribute(
          GCXmlLiterals.EXTN_RETURN_VPO_NO, "VPO"
          .concat(sOrderNo));
    }
  }

  // GCSTORE-2475: Start
  /**
   * Update the address on Return Order for Borderfree orders if it doesn't already exist on the
   * return order
   *
   * @param inDoc
   * @param outDocGOD
   * @param salesOrderDetails
   * @return
   */
  private Document updateBorderfreeROAdd(Document inDoc, Document outDocGOD, Document salesOrderDetails) {
    LOGGER.debug("GCBeforeChangeReturnOrderUE.updateBorderfreeROAdd--Begin");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("inDoc is " + GCXMLUtil.getXMLString(inDoc));
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("outDocGOD is " + GCXMLUtil.getXMLString(outDocGOD));
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("salesOrderDetails is " + GCXMLUtil.getXMLString(salesOrderDetails));
    }
    YFCDocument yfcSalesOrderDetails = YFCDocument.getDocumentFor(salesOrderDetails);
    YFCElement eleSalesOrder = yfcSalesOrderDetails.getDocumentElement().getChildElement(GCConstants.ORDER);
    YFCDocument yfcOutDocGOD = YFCDocument.getDocumentFor(outDocGOD);
    YFCDocument inputDocYfc = YFCDocument.getDocumentFor(inDoc);

    YFCElement elePersonInfoBillToRO = inputDocYfc.getDocumentElement().getChildElement(GCConstants.PERSON_INFO_BILL_TO);
    YFCElement elePersonInfoShipToRO = inputDocYfc.getDocumentElement().getChildElement(GCConstants.PERSON_INFO_SHIP_TO);
    if (YFCCommon.isVoid(elePersonInfoBillToRO)) {
      elePersonInfoBillToRO = yfcOutDocGOD.getDocumentElement().getChildElement(GCConstants.PERSON_INFO_BILL_TO);
      if(YFCCommon.isVoid(elePersonInfoBillToRO)){
        YFCElement elePersonInfoBillTo = eleSalesOrder.getChildElement(GCConstants.PERSON_INFO_BILL_TO);
        elePersonInfoBillToRO = inputDocYfc.getDocumentElement().createChild(GCConstants.PERSON_INFO_BILL_TO);
        GCXMLUtil.copyAttributes(elePersonInfoBillTo, elePersonInfoBillToRO);
      }
    }
    if (YFCCommon.isVoid(elePersonInfoShipToRO)) {
      elePersonInfoShipToRO = yfcOutDocGOD.getDocumentElement().getChildElement(GCConstants.PERSON_INFO_SHIP_TO);
      if(YFCCommon.isVoid(elePersonInfoShipToRO)){
        YFCElement elePersonInfoShipTo = eleSalesOrder.getChildElement(GCConstants.PERSON_INFO_SHIP_TO);
        elePersonInfoShipToRO = inputDocYfc.getDocumentElement().createChild(GCConstants.PERSON_INFO_SHIP_TO);
        GCXMLUtil.copyAttributes(elePersonInfoShipTo, elePersonInfoShipToRO);
      }
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" updated inDoc is " + GCXMLUtil.getXMLString(inDoc));
    }
    LOGGER.debug("GCBeforeChangeReturnOrderUE.updateBorderfreeROAdd--End");
    return inputDocYfc.getDocument();
  }
  // GCSTORE-2475: End
}