package com.gc.userexit;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCLogUtils;
import com.gc.exstore.utils.GCShipmentUtil;
import com.yantra.util.YFCUtils;
import com.yantra.ydm.japi.ue.YDMBeforeConsolidateToShipment;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSUserExitException;

public class GCBeforeConsolidateToShipmentUE implements YDMBeforeConsolidateToShipment {
  // Initialise LOGGER
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCBeforeConsolidateToShipmentUE.class.getName());

  /**
   * This method finds out if the shipment contains just the warranty lines. If yes then find out
   * the parent line shipment and include the warranty lines in it.
   *
   * @param env
   * @param inDoc
   * @return
   */

  @Override
  public Document beforeConsolidateToShipment(YFSEnvironment env, Document inDoc) throws YFSUserExitException {
    LOGGER.beginTimer(".beforeConsolidateToShipment");
    GCLogUtils.verboseLog(LOGGER, "GCBeforeConsolidateToShipmentUE.beforeConsolidateToShipment--Begin");
    List<String> dependentOnLineKeyList = new ArrayList<String>();
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);

    GCLogUtils.verboseLog(LOGGER, "inputDoc : ", inputDoc.toString());
    YFCElement rootEle = inputDoc.getDocumentElement();
    String documentType = rootEle.getAttribute(GCConstants.DOCUMENT_TYPE);
    if (YFCUtils.equals(documentType, GCConstants.ZERO_ZERO_ZERO_ONE)) {
      YFCElement eleShipmentLines = rootEle.getChildElement(GCConstants.SHIPMENT_LINES);
      YFCNodeList<YFCElement> nlShipmentLine = eleShipmentLines.getElementsByTagName(GCConstants.SHIPMENT_LINE);
      YFCElement eleShipmentLine = nlShipmentLine.item(0);
      String orderHeaderKey = eleShipmentLine.getAttribute(GCConstants.ORDER_HEADER_KEY);
      YFCDocument orderListOpTemp = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_WARR_TEMP);
      YFCDocument orderListOp = GCCommonUtil.invokeGetOrderListAPI(env, orderHeaderKey, orderListOpTemp);
      YFCElement rootOrderEle = orderListOp.getDocumentElement();
      YFCElement eleOrder = rootOrderEle.getChildElement(GCConstants.ORDER);
      List<String> orderLineKeyList = GCShipmentUtil.getOrderLineKeyList(nlShipmentLine);
      boolean isStandaloneWarrShipment = checkIfWarrShipment(orderLineKeyList, dependentOnLineKeyList, eleOrder);
      boolean isGiftShipment = checkIfGiftShipment(orderLineKeyList, dependentOnLineKeyList, eleOrder);
      // Check if shipment contains only standalone warranty lines

      if (isStandaloneWarrShipment || isGiftShipment) {

        // Get shipment key for parent line
        String parentShipmentKey = getParentShipmentKey(env, dependentOnLineKeyList);
        GCLogUtils.verboseLog(LOGGER, "Parent Shipment Key", parentShipmentKey);
        if (!YFCCommon.isVoid(parentShipmentKey)) {

          rootEle.setAttribute(GCConstants.SHIPMENT_KEY, parentShipmentKey);
          rootEle.setAttribute("OverrideModificationRules", "Y");
          rootEle.setAttribute(GCConstants.ACTION, GCConstants.ACTION_MODIFY);
        }
      }
    }
    GCLogUtils.verboseLog(LOGGER, "GCBeforeConsolidateToShipmentUE.beforeConsolidateToShipment--End");
    LOGGER.endTimer("GCBeforeConsolidateToShipmentUE.beforeConsolidateToShipment");
    GCLogUtils.verboseLog(LOGGER, "outputDoc : ", inputDoc.toString());
    return inputDoc.getDocument();
  }



  /**
   * This method invokes getShipmentList API with DependentOnLineKey and fetches the shipment key of
   * parent line
   *
   * @param env
   * @param dependentOnLineKeyList.get(0)
   * @return
   */

  private String getParentShipmentKey(YFSEnvironment env, List<String> dependentOnLineKeyList) {
    LOGGER.beginTimer("GCBeforeConsolidateToShipmentUE.getParentShipmentKey");
    GCLogUtils.verboseLog(LOGGER, "GCBeforeConsolidateToShipmentUE.getParentShipmentKey--Begin");
    String shipmentKey = null;
    YFCDocument getShipmentListIp = YFCDocument.createDocument(GCConstants.SHIPMENT);
    YFCElement eleIp = getShipmentListIp.getDocumentElement();
    YFCElement eleShipmentLines = eleIp.createChild(GCConstants.SHIPMENT_LINES);
    YFCElement eleShipmentLine = eleShipmentLines.createChild(GCConstants.SHIPMENT_LINE);
    eleShipmentLine.setAttribute(GCConstants.ORDER_LINE_KEY, dependentOnLineKeyList.get(0));
    YFCDocument opTemp = YFCDocument.getDocumentFor(GCConstants.GET_SHIPMENT_LIST_STANDALONE_WARRANTY);
    GCLogUtils.verboseLog(LOGGER, "getShipmentList Input", getShipmentListIp.toString());
    // Invoke getShipmentList API
    YFCDocument opDoc = GCCommonUtil.invokeAPI(env, GCConstants.GET_SHIPMENT_LIST, getShipmentListIp, opTemp);
    if (!YFCCommon.isVoid(opDoc)) {
      GCLogUtils.verboseLog(LOGGER, "getShipmentList Output", opDoc.toString());
      YFCElement eleOpRoot = opDoc.getDocumentElement();
      YFCElement eleShipment = eleOpRoot.getChildElement(GCConstants.SHIPMENT);
      if (!YFCCommon.isVoid(eleShipment)) {
        String status = eleShipment.getAttribute(GCConstants.STATUS);
        status = GCShipmentUtil.checkAndConvertOrderStatus(status);
        Double dStatus = Double.parseDouble(status);
        String backroomPickedStatus = GCConstants.BACKROOM_PICKED_STATUS;
        backroomPickedStatus = GCShipmentUtil.checkAndConvertOrderStatus(backroomPickedStatus);
        Double dBackroomStatus = Double.parseDouble(backroomPickedStatus);
        if (dStatus < dBackroomStatus) {
          shipmentKey = eleShipment.getAttribute(GCConstants.SHIPMENT_KEY);
        }
      }
    }
    GCLogUtils.verboseLog(LOGGER, "GCBeforeConsolidateToShipmentUE.getParentShipmentKey--End");
    LOGGER.endTimer("GCBeforeConsolidateToShipmentUE.getParentShipmentKey");
    return shipmentKey;
  }




  /**
   * This method checks if incoming shipment contain s only standalone warranty lines
   *
   * @param shipmentLineMap
   * @return
   */
  private boolean checkIfWarrShipment(List<String> orderLineKeyList, List<String> parentLineKeyList, YFCElement eleOrder) {
    LOGGER.beginTimer("GCBeforeConsolidateToShipment.checkIfWarrShipment");
    GCLogUtils.verboseLog(LOGGER, "GCBeforeConsolidateToShipment.checkIfWarrShipment--Begin");
    YFCElement eleOrderLines = eleOrder.getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> nlOrderLine = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
    for (YFCElement eleOrderLine : nlOrderLine) {
      String orderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      if(orderLineKeyList.contains(orderLineKey)){
        YFCElement eleExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
        String isWarranty = eleExtn.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
        // Check if order line is a warrant line , if not break the loop
        if (!YFCUtils.equals(isWarranty, GCConstants.YES)) {
          GCLogUtils.verboseLog(LOGGER, "Is Standalone Warranty Shipment", GCConstants.NO);

          GCLogUtils.verboseLog(LOGGER, "GCBeforeConsolidateToShipment.checkIfWarrShipment--End");
          LOGGER.endTimer("GCBeforeConsolidateToShipment.checkIfWarrShipment");
          return false;
        } else {
          String parentLineKey = eleOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
          parentLineKeyList.add(parentLineKey);
        }
      }
    }
    GCLogUtils.verboseLog(LOGGER, "Is Standalone Warranty Shipment", GCConstants.YES);
    GCLogUtils.verboseLog(LOGGER, "GCBeforeConsolidateToShipment.checkIfWarrShipment--End");
    LOGGER.endTimer("GCBeforeConsolidateToShipment.checkIfWarrShipment");
    return true;
  }

  private boolean checkIfGiftShipment(List<String> orderLineKeyList, List<String> parentLineKeyList, YFCElement eleOrder) {
    LOGGER.beginTimer("GCBeforeConsolidateToShipment.checkIfWarrShipment");
    GCLogUtils.verboseLog(LOGGER, "GCBeforeConsolidateToShipment.checkIfWarrShipment--Begin");
    YFCElement eleOrderLines = eleOrder.getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> nlOrderLine = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
    for (YFCElement eleOrderLine : nlOrderLine) {
      String orderLineKey = eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      if (orderLineKeyList.contains(orderLineKey)) {
        YFCElement eleExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
        String isGift = eleExtn.getAttribute(GCConstants.EXTN_IS_FREE_GIFT_ITEM);
        // Check if order line is a warrant line , if not break the loop
        if (!YFCUtils.equals(isGift, GCConstants.YES)) {
          GCLogUtils.verboseLog(LOGGER, "Is Standalone Warranty Shipment", GCConstants.NO);

          GCLogUtils.verboseLog(LOGGER, "GCBeforeConsolidateToShipment.checkIfWarrShipment--End");
          LOGGER.endTimer("GCBeforeConsolidateToShipment.checkIfWarrShipment");
          return false;
        } else {

          String parentLineKey = eleOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
          String shipNodeForGiftItem = "";
          String shipNodeForParentItem = "";
          YFCElement nlOrderStatusElement = eleOrderLine.getElementsByTagName(GCConstants.ORDER_STATUSES).item(0);
          YFCNodeList<YFCElement> nlStatusList = nlOrderStatusElement.getElementsByTagName(GCConstants.ORDER_STATUS);
          for (YFCElement eleStatus : nlStatusList) {
            String status = eleStatus.getAttribute(GCConstants.STATUS);
            if (GCConstants.RELEASED_STATUS.equals(status)) {
              shipNodeForGiftItem = eleStatus.getAttribute(GCConstants.SHIP_NODE);
              break;
            }
          }
          shipNodeForParentItem = getShipNodeForParentItem(eleOrder,
				parentLineKey, shipNodeForParentItem);
          if (!YFCCommon.isVoid(shipNodeForGiftItem) && YFCUtils.equals(shipNodeForGiftItem, shipNodeForParentItem)) {
            parentLineKeyList.add(parentLineKey);
          } else {
            return false;
          }
        }
      }
    }
    GCLogUtils.verboseLog(LOGGER, "Is Standalone Warranty Shipment", GCConstants.YES);
    GCLogUtils.verboseLog(LOGGER, "GCBeforeConsolidateToShipment.checkIfWarrShipment--End");
    LOGGER.endTimer("GCBeforeConsolidateToShipment.checkIfWarrShipment");
    return true;
  }



	private String getShipNodeForParentItem(YFCElement eleOrder,
			String parentLineKey, String shipNodeForParentItem) {
		YFCElement eleOrderLinesFoParent = eleOrder
				.getChildElement(GCConstants.ORDER_LINES);
		YFCNodeList<YFCElement> nlOrderLineForParent = eleOrderLinesFoParent
				.getElementsByTagName(GCConstants.ORDER_LINE);
		for (YFCElement elemOrderLine : nlOrderLineForParent) {
			String orderLineKeyForParent = elemOrderLine
					.getAttribute(GCConstants.ORDER_LINE_KEY);
			if (orderLineKeyForParent.equals(parentLineKey)) {
				YFCElement nlOrderStatElement = elemOrderLine
						.getElementsByTagName(GCConstants.ORDER_STATUSES).item(
								0);
				YFCNodeList<YFCElement> nlStatList = nlOrderStatElement
						.getElementsByTagName(GCConstants.ORDER_STATUS);
				for (YFCElement eleStatusForParent : nlStatList) {
					String statusForParent = eleStatusForParent
							.getAttribute(GCConstants.STATUS);
					if (YFCUtils.equals(GCConstants.RELEASED_STATUS,
							statusForParent)
							|| YFCUtils.equals("3350", statusForParent)) {
						shipNodeForParentItem = eleStatusForParent
								.getAttribute(GCConstants.SHIP_NODE);
						break;
					}
				}
			}
		}
		return shipNodeForParentItem;
	}
}