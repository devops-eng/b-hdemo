/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 * *#################################################################################################################################################################
 *                                          OBJECTIVE: Handle the functionalities required for before order creation
 * #################################################################################################################################################################
 * Version          Date                Modified By                 Description
 * #################################################################################################################################################################
 * 1.0              14/02/2014          Singh,Gurpreet              OMS-1141: Development - External Order Capture
 * 1.1              21/11/2014          Soni, Karan                 CR-53 : Display order level �level of service� (LOS) on order summary
 * 1.1              19/03/2015          Ravi, Divya                 GCSTORE-811 : ORder Fulfillment changes Phase 2
 * 1.2              25/03/2015          Ravi, Divya                 GCSTORE-1594 : Copy ship to address from OrderLine to Order
 * 1.3              19/05/2015          Singh, Gurpreet             GCSTORE-2514: Line Level Taxes (CR-022)
 * 1.2              28/04/2015          Infosys Ltd.                OMS-5463 :OMS won't accept new source code until start date + 1 day
 * 1.4              18/05/2015          Infosy Ltd.                 OMS-5491 : Warranty-only Web order failed to capture in OMS
 * 1.5              10/01/2017          Mishra, Deepak              GCSTORE-6865/6864 : Low value item Change(CR) 
 * 1.6              31/03/2017          Mishra, Deepak              GCSTORE-7165 : Restrict ship from store flag needs to be visible in Call Center
 * 1.7				06/06/2017			Gupta, Abhishek				GCSTORE-7083 : Digital download items are getting sourced from Store and not from MFI	 #################################################################################################################################################################
 */
package com.gc.userexit;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.gc.api.GCValidateAndResolveHoldsWithPeriod;
import com.gc.api.order.GCProcessATGOrders;
import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.constants.GCXmlLiterals;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCLogUtils;
import com.gc.common.utils.GCOrderUtil;
import com.gc.common.utils.GCPaymentUtils;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.yfs.japi.ue.YFSBeforeCreateOrderUE;

/**
 * @author Gurpreet
 */
public class GCBeforeCreateOrderUE implements YFSBeforeCreateOrderUE, YIFCustomApi {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCBeforeCreateOrderUE.class);
  private static Date currentDate = new Date();
  Properties prop = new Properties();

  /**
   *
   * Description of beforeCreateOrder Handle the functionalities required for before order creation
   * a) stamp ProductClass = 'SET' if ExtnSetCode='1' for bundle item b) check for APO/FPO and POBOX
   * in the address c) do inventory reservation by invoking GCReserveInventory Service d) Validate
   * Order on the basis of ExtnSourceOrderNo and ExtnDAXOrderNo e) Check for the digitalItems
   *
   * @param env
   * @param inDoc
   * @return
   * @throws
   * @see com.yantra.yfs.japi.ue.YFSBeforeCreateOrderUE#beforeCreateOrder(com.yantra.yfs.japi.YFSEnvironment,
   *      org.w3c.dom.Document)
   */
  @Override
  public Document beforeCreateOrder(YFSEnvironment env, Document inDoc) throws YFSUserExitException {

    LOGGER.debug(" Entering GCBeforeCreateOrderUE.beforeCreateOrder() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" beforeCreateOrder() method, Input document is" + GCXMLUtil.getXMLString(inDoc));
    }
    Element eleRootOfInDoc = inDoc.getDocumentElement();
    String sEntryType = eleRootOfInDoc.getAttribute(GCConstants.ENTRY_TYPE);
    String sGuestCheckOut = eleRootOfInDoc.getAttribute(GCConstants.IS_GUEST_CHECK_OUT);
    Element eleReferences = GCXMLUtil.createElement(inDoc, "References", null);
    Element eleReference = GCXMLUtil.createElement(inDoc, "Reference", null);
    eleReference.setAttribute("Name", GCConstants.IS_GUEST_CHECK_OUT);
    eleReference.setAttribute("Value", sGuestCheckOut);
    eleReferences.appendChild(eleReference);
    eleRootOfInDoc.appendChild(eleReferences);
    Document copyOrderDoc = (Document) env.getApiTemplate("copyOrder");
    String sUserId = env.getUserId();
    String sEmployeeID="";
    String sUserName= "";
    if (!YFCCommon.isVoid(sUserId) && GCConstants.CALL_CENTER.equals(sEntryType)) {

        Document getUserListOutDoc =
            GCCommonUtil.invokeAPI(env, GCConstants.GET_USER_LIST,
                GCXMLUtil.getDocument("<User Loginid='" + sUserId + "'/>"),
                GCXMLUtil.getDocument("<UserList><User Username=''><Extn/></User></UserList>"));
        Element eleUserList = getUserListOutDoc.getDocumentElement();
        if(eleUserList.hasChildNodes()){
        Element eleUser = (Element) eleUserList.getElementsByTagName("User").item(0);
        Element eleExtn =(Element) eleUser.getElementsByTagName("Extn").item(0);
        sEmployeeID  = eleExtn.getAttribute(GCConstants.EXTN_EMPLOYEE_ID);
        sUserName = eleUser.getAttribute(GCConstants.USERNAME);
    }
    }
    LOGGER.debug(" Going to call  updateSourcingAttrForExchangeOrder");
    updateSourcingAttrForExchangeOrder(env, inDoc);
    LOGGER.debug(" After  updateSourcingAttrForExchangeOrder call");

    try {
        
    //GCSTORE-5305: Begin
        //check if the quote is converted
        Element eleExtn1 = (Element) XPathAPI.selectSingleNode(inDoc, "//Order/Extn");
        
        if(!YFCCommon.isVoid(eleExtn1)){
            String sExtnSupportTicketNo = eleExtn1.getAttribute(GCConstants.EXTN_SUPPORT_TICKET_NO);
            if(!YFCCommon.isVoid(sExtnSupportTicketNo)){
                String sExtnSourceOrderNo = eleExtn1.getAttribute(GCConstants.EXTN_SOURCE_ORDER_NO);
                
                Document docGetOrderListIp =  GCXMLUtil.createDocument(GCConstants.ORDER);
                Element eleRoot = docGetOrderListIp.getDocumentElement();

                Element eleExtn = docGetOrderListIp.createElement(GCConstants.EXTN);
                eleRoot.appendChild(eleExtn);
                eleExtn.setAttribute(GCConstants.EXTN_SOURCE_ORDER_NO, sExtnSourceOrderNo);   
                Document docGetOrderListOp = GCCommonUtil.invokeAPI(env, docGetOrderListIp, GCConstants.GET_ORDER_LIST, "/global/template/api/getOrderListTempForQuoteRequest.xml");
                YFCDocument yfcGetOrderList = YFCDocument.getDocumentFor(docGetOrderListOp);
                YFCElement yfcEleOrderList = yfcGetOrderList.getDocumentElement();
                YFCElement yfcEleOrder = yfcEleOrderList.getChildElement("Order");
                if(!YFCCommon.isVoid(yfcEleOrder)){                
                    YFCElement yfcEleExtn = yfcEleOrder.getChildElement("Extn");
                    String syfcExtnOrderCaptureChannel = yfcEleExtn.getAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL);
                    eleExtn1.setAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL, syfcExtnOrderCaptureChannel);
                    String sExtnSourceCode = yfcEleExtn.getAttribute(GCConstants.EXTN_SOURCE_CODE);
                    eleExtn1.setAttribute(GCConstants.EXTN_SOURCE_CODE, sExtnSourceCode);
                    String sExtnOrderingStore = yfcEleExtn.getAttribute(GCConstants.EXTN_ORDERING_STORE);
                    if(!YFCCommon.isVoid(sExtnOrderingStore)){
                    	eleExtn1.setAttribute(GCConstants.EXTN_ORDERING_STORE, sExtnOrderingStore);
                    }
                    deleteCallCenterQuote(env, inDoc, docGetOrderListOp);
                    appendQuoteNotes(inDoc, sExtnSourceOrderNo, docGetOrderListOp);
                }
                appendPriceOverrideNotes(env, inDoc);
                appendUserNameInCommission(env, inDoc);
            }
        // Fix to stamp user name on commission when mPOS order is created.
        if (YFCCommon.equals(sEntryType, GCConstants.MPOS_ORDER_CHANNEL)) {
          appendUserNameInCommissionForMpos(env, inDoc);
        }
      }
        
      // GCSTORE-2532::Begin
      stampProductAvailabilityDate(inDoc);
      // GCSTORE-2532::Ends
      stampAttributesForExchangeOrders(env, inDoc);
      // Stamp Phase mode
      setOrderPhase(env, inDoc);

      String sAPIName = eleRootOfInDoc.getAttribute("APIName");

      inDoc = getAuthExpiryDate(env, inDoc);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("The document returned after Auth exp date append--->" + GCXMLUtil.getXMLString(inDoc));
      }
      // Phase 2 Changes - ORder Fulfillment
      String sLevelOfService = eleRootOfInDoc.getAttribute(GCConstants.LEVEL_OF_SERVICE);
      // CR-53 Start
      // Phase 2 changes Order Fulfillment - check level of service exists or not
      if ((YFCCommon.isVoid(sEntryType) || !GCConstants.CALL_CENTER.equals(sEntryType))
          && YFCCommon.isVoid(sLevelOfService)) {
        NodeList nlOrderLine = GCXMLUtil.getNodeListByXpath(inDoc, "Order/OrderLines/OrderLine");
        for (int i = 0; i < nlOrderLine.getLength(); i++) {
          Element eleOL = (Element) nlOrderLine.item(i);
          String strCarrierServiceCode = eleOL.getAttribute(GCConstants.CARRIER_SERVICE_CODE);
          if ("03".equals(strCarrierServiceCode) || "52".equals(strCarrierServiceCode)) {
            eleRootOfInDoc.setAttribute(GCConstants.LEVEL_OF_SERVICE, GCConstants.NEXT_DAY_AIR);
            break;
          } else if ("01".equals(strCarrierServiceCode)) {
            eleRootOfInDoc.setAttribute(GCConstants.LEVEL_OF_SERVICE, GCConstants.SECOND_DAY_EXPRESS);
            break;
          } else if ("37".equals(strCarrierServiceCode)) {
            eleRootOfInDoc.setAttribute(GCConstants.LEVEL_OF_SERVICE, GCConstants.STANDARD_GROUND);
            break;
          } else if ("02".equals(strCarrierServiceCode)) {
            eleRootOfInDoc.setAttribute(GCConstants.LEVEL_OF_SERVICE, GCConstants.ECONOMY_GROUND);
            break;
          } else if ("81".equals(strCarrierServiceCode)) {
            eleRootOfInDoc.setAttribute(GCConstants.LEVEL_OF_SERVICE, GCConstants.ECONOMY_GROUND);
          } else if ("99".equals(strCarrierServiceCode)) {
            String strIsEconomy = eleRootOfInDoc.getAttribute(GCConstants.LEVEL_OF_SERVICE);
            if (!"ECONOMY_GROUND".equals(strIsEconomy)) {
              eleRootOfInDoc.setAttribute(GCConstants.LEVEL_OF_SERVICE, GCConstants.DIGITAL_DELIVERY);
            }
          }
        }
      }
      /**
       * Stamp AllocationRuleID on Order Level for COM Order
       *
       * Begin
       */
      if (GCConstants.CALL_CENTER.equalsIgnoreCase(sEntryType)) {
        eleRootOfInDoc.setAttribute(GCXmlLiterals.ALLOCATION_RULE_ID, GCConstants.GC_DEFAULT);
      }

      // End
      // /CR-53 End
      /*
       * Stamping EXTN_ORDER_CAPTURE_CHANNEL as Call Center for COM Orders - Begin
       */
      if (GCConstants.CALL_CENTER.equalsIgnoreCase(sEntryType)) {
        Element eleOrderExtn = inDoc.createElement(GCConstants.EXTN);
        eleOrderExtn.setAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL, GCConstants.CALL_CENTER_ORDER_CHANNEL);
        eleRootOfInDoc.appendChild(eleOrderExtn);
      }

	 // MPOS-27 : Create Order : Stamp Order Capture Channel for mPOS Orders
	/*Commentting out as these are getting stamped on order from UI	
     * if (GCConstants.MPOS.equalsIgnoreCase(sEntryType)) {
     *  Element eleOrderExtn = inDoc.createElement(GCConstants.EXTN);
     *  eleOrderExtn.setAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL, GCConstants.MPOS_ORDER_CHANNEL);
     *  eleOrderExtn.setAttribute(GCConstants.EXTN_ORDER_PHASE, GCConstants.PHASE_TWO);
     *  eleRootOfInDoc.appendChild(eleOrderExtn);
     *}
	 */

      if (GCConstants.CALL_CENTER.equalsIgnoreCase(sEntryType)) {
        YFCDocument yfcInDoc=YFCDocument.getDocumentFor(inDoc);
        YFCElement yfcEleRoot = yfcInDoc.getDocumentElement();
        YFCElement eleExtn = yfcEleRoot.getChildElement("Extn");
        YFCElement eleGCOrderCommissionList = eleExtn.getChildElement("GCOrderCommissionList", true);
        YFCElement eleGCOrderCommision = eleGCOrderCommissionList.getChildElement("GCOrderCommission", true);
        eleGCOrderCommision.setAttribute("UserID", sEmployeeID);
        eleGCOrderCommision.setAttribute("UserName", sUserName);
        eleGCOrderCommision.setAttribute("AllocationPercentage", "100");
        eleGCOrderCommision.setAttribute("CommissionType", "Sales Commission");
        
      }


      /*
       * Stamping EXTN_ORDER_CAPTURE_CHANNEL as Call Center for COM Orders - END
       */
      // Skipping method calls for mPOS Orders
      if (!GCConstants.CALL_CENTER.equalsIgnoreCase(sEntryType) && !"copyOrder".equals(sAPIName)
          && !YFCCommon.equals(GCConstants.MPOS, sEntryType)) {
        validateOrder(env, inDoc);
        stampEntryType(env, inDoc);
      }

      setSentForFraudCheck(inDoc);

      inDoc = checkDigitalItems(env, inDoc);

      stampUserName(env, inDoc);

      checkBundleOrderLine(env, inDoc);
      /*
       * Handle below code later when customer management service is available. Invoke
       * getCustomerList API to check if customer exist in OMS or not. If doesn�t exist create new
       * customer.
       */

      YFCElement orderElem = YFCDocument.getDocumentFor(inDoc).getDocumentElement();
      YFCElement extnEle = orderElem.getChildElement(GCConstants.EXTN, true);
      String sExtnSourceSystem = extnEle.getAttribute(GCXmlLiterals.EXTN_SOURCE_SYSTEM);
      String sellerOrganizationCode = orderElem.getAttribute(GCConstants.SELLER_ORGANIZATION_CODE);
      if (YFCCommon.equals("INTL_WEB", sellerOrganizationCode)) {
        String paymentRuleId = orderElem.getAttribute("PaymentRuleId");
        if (YFCCommon.isVoid(paymentRuleId)) {
          orderElem.setAttribute("PaymentRuleId", "GCBorderfreePayRule");
        }
      }

      stampSetProductClass(env, inDoc);

      // Invoke reserveAvailableInventory API for reservation.
      // MPOS-27 : Skipping for mPOS Orders
      if (!GCConstants.CALL_CENTER.equalsIgnoreCase(sEntryType) && !"copyOrder".equals(sAPIName)
          && !YFCCommon.equals(GCConstants.MPOS, sEntryType)) {
        LOGGER.verbose("Invoke reserveAvailableInventory API for reservation");
        // Method added as part of fix for GCSTORE-1594
        copyShipToFromLineToOrder(YFCDocument.getDocumentFor(inDoc));
        boolean isDCSourcedOrder = stampApoFpoPOBoxAddress(inDoc);
        stampSourcingClassification(env, inDoc, isDCSourcedOrder);

        // Defect-3609
        String cancelATGReservationFlag = prop.getProperty("CancelATGReservation");
        if (YFCCommon.equals(cancelATGReservationFlag, "Y")) {
          cancelEarlierReservation(env, inDoc);
        }
        // Ends 3609

        inDoc = GCCommonUtil.invokeService(env, GCConstants.GC_RESERVE_INVENTORY_SERVICE, inDoc);
        stampOrderLineAttributes(env, inDoc);
      }
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("Indoc After Invoking reserveAvailableInventory API for reservation :\n"
            + YFCDocument.getDocumentFor(inDoc));
      }
      // Phase 2: Start
      if (YFCUtils.equals(GCConstants.ORDER_CATURE_SYSTEM_ATG, sExtnSourceSystem)) {
        //GCSTORE-6137
        updateCommercialAddressFlag(inDoc);
        inDoc = removeOrderLineInvAttForSerialBundle(env, inDoc);
        LOGGER.verbose("call method splitOrderLines\n");
        GCProcessATGOrders gcProcessATGOrders = new GCProcessATGOrders();
        Document cloneDoc = (Document) inDoc.cloneNode(true);
        inDoc = gcProcessATGOrders.splitOrderLines(env, cloneDoc);
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("inDOC After OrderLine Split:\n" + YFCDocument.getDocumentFor(inDoc));
        }
        
        // Fix for GCSTORE-4132 : Stamping Bin number in payment reference2
        List<Element> listPaymentMethod = GCXMLUtil.getElementListByXpath(inDoc,
                    "Order/PaymentMethods/PaymentMethod[@PaymentType='CREDIT_CARD']");

            for (Element elePaymentMethod : listPaymentMethod) {
                
                String sEncryptedCCNo = elePaymentMethod.getAttribute(GCConstants.CREDIT_CARD_NO);
                GCPaymentUtils objPaymentUtils =  new GCPaymentUtils();
                String sBinNumber = objPaymentUtils.getBinNumber(sEncryptedCCNo);
                elePaymentMethod.setAttribute(GCConstants.PAYMENT_REFERENCE_3, sBinNumber);
            } 
      }
      // Phase 2 : End

      if (GCConstants.CALL_CENTER.equalsIgnoreCase(sEntryType)) {
        String sOrderPurpose = eleRootOfInDoc.getAttribute(GCConstants.ORDER_PURPOSE);
        NodeList nlOrderLine = XPathAPI.selectNodeList(eleRootOfInDoc, "OrderLines/OrderLine");
        if (GCConstants.EXCHANGE.equals(sOrderPurpose) && nlOrderLine.getLength() > 0) {
          LOGGER.debug(" If Entry Type is call center and Order Purpose is EXCHANGE ");
          Document docGetOrderPrice = prepareGetOrderPriceIP(eleRootOfInDoc);
          Document docGetOrderPriceTmp =
              GCXMLUtil
              .getDocument("<Order><OrderLines><OrderLine ItemID='' UnitPrice='' LineID='' /></OrderLines></Order>");
          Document docGetOrderPriceOP =
              GCCommonUtil.invokeAPI(env, "getOrderPrice", docGetOrderPrice, docGetOrderPriceTmp);
          updateInDocUnitPrice(inDoc, docGetOrderPriceOP);
          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(" updated inDoc is" + GCXMLUtil.getXMLString(inDoc));
          }
          GCCommonUtil.invokeService(env, GCConstants.GC_PROCESS_TAX_CALCULATIONS_SERVICE, inDoc);
        } else if (!(nlOrderLine.getLength() > 0)) {
          env.setTxnObject("IsNewExchangeOrder_0001", GCConstants.YES);
        }
      }

      NodeList nlOrderLine = XPathAPI.selectNodeList(eleRootOfInDoc, "OrderLines/OrderLine");
      if ("copyOrder".equals(sAPIName) && nlOrderLine.getLength() > 0) {
        LOGGER.debug(" If APIName is copyOrder ");
        GCCommonUtil.invokeService(env, GCConstants.GC_PROCESS_TAX_CALCULATIONS_SERVICE, inDoc);
      }

      // Fix for GCSTORE# 4758-- Starts
      double initaialTotalTax = fetchTotalTaxAmount(inDoc);
      // Fix for GCSTORE# 4758-- Ends
      // Shipping Logic
      String sExtnIsShippingChargeProrated =
          GCXMLUtil.getAttributeFromXPath(inDoc, "Order/Extn" + "/@ExtnIsShippingChargeProrated");
      if (GCConstants.NO.equalsIgnoreCase(sExtnIsShippingChargeProrated)
          && !GCConstants.CALL_CENTER.equalsIgnoreCase(sEntryType)) {
        GCCommonUtil.invokeService(env, GCConstants.GC_PROCESS_SHPG_PRORATION_SERVICE, inDoc);
      }
      inDoc = modifyInDoc(inDoc);
      // Apply Hold on Warranty Line
      applyHoldOnDependentLineAndStampShipNodeForPIS(inDoc);
      // GCSTORE-2514 : Start
      Element eExtn = GCXMLUtil.getElementByXPath(inDoc, "Order/Extn");
      String sSellerOrgCode = eleRootOfInDoc.getAttribute(GCConstants.SELLER_ORGANIZATION_CODE);
      if (!GCConstants.CALL_CENTER.equalsIgnoreCase(sEntryType)
          && ((!YFCCommon.isVoid(eExtn)) && YFCCommon.equals(GCConstants.ORDER_CATURE_SYSTEM_ATG,
              eExtn.getAttribute(GCXmlLiterals.EXTN_SOURCE_SYSTEM)))
              && !YFCCommon.equals(GCConstants.INTL_WEB, sSellerOrgCode)) {
        inDoc = GCCommonUtil.invokeService(env, GCConstants.GC_PROCESS_TAX_CALCULATIONS_SERVICE, inDoc);
        processUpdatedDocWithTaxes(env, inDoc, initaialTotalTax);
      }
      // GCSTORE-2514 : End
    } catch (GCException e) {
      LOGGER.error(" Inside catch block of GCBeforeCreateOrderUE.beforeCreateOrder(), Exception is :", e);
      throw e.getYFSUserExitException();
    } catch (Exception ex) {
      LOGGER.error("Inside catch block of GCBeforeCreateOrderUE.beforeCreateOrder(), YFS UE Exception is :", ex);
      throw (new GCException(ex).getYFSUserExitException());
    }
	
	//GCSTORE - 7083 Begin
    GCCommonUtil.checkModifyProductLine(env,inDoc);
    //GCSTORE - 7083 End
	
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" Updated Input document is" + GCXMLUtil.getXMLString(inDoc));
    }
    LOGGER.debug(" Exiting GCBeforeCreateOrderUE.beforeCreateOrder() method ");
    env.setApiTemplate("copyOrder", copyOrderDoc);

    return inDoc;
  }

  private void appendUserNameInCommission(YFSEnvironment env, Document inDoc) throws TransformerException {
    // order level commission
    NodeList nlOrderCommission = XPathAPI.selectNodeList(inDoc, "//GCOrderCommission");
    for(int iOrderCommission=0; iOrderCommission<nlOrderCommission.getLength();iOrderCommission++){
      Element eleOrderCommission = (Element) nlOrderCommission.item(iOrderCommission);
      String sUserName = eleOrderCommission.getAttribute(GCConstants.UserName);
      if(StringUtils.isNotBlank(sUserName)){
        String sExtnEmployeeID = eleOrderCommission.getAttribute(GCConstants.USER_ID);
              if(StringUtils.isNotBlank(sExtnEmployeeID)) {
                sUserName = getUserNameByExtnEmployeeID(env, sExtnEmployeeID);
              }
        eleOrderCommission.setAttribute(GCConstants.UserName, sUserName);
      }
    }

    //Line level commission
    NodeList nlOrderLineCommission = XPathAPI.selectNodeList(inDoc, "//GCOrderLineCommission");
    for(int iOrderLineCommission=0; iOrderLineCommission<nlOrderLineCommission.getLength();iOrderLineCommission++){
      Element eleOrderLineCommission = (Element) nlOrderLineCommission.item(iOrderLineCommission);
      String sUserName = eleOrderLineCommission.getAttribute(GCConstants.UserName);
      if(StringUtils.isNotBlank(sUserName)){
        String sExtnEmployeeID = eleOrderLineCommission.getAttribute(GCConstants.USER_ID);
              if(StringUtils.isNotBlank(sExtnEmployeeID)) {
                sUserName = getUserNameByExtnEmployeeID(env, sExtnEmployeeID);
              }
        eleOrderLineCommission.setAttribute(GCConstants.UserName, sUserName);
      }
    }


    

  }

  private String getUserNameByExtnEmployeeID(YFSEnvironment env,
      String sExtnEmployeeID) {

    Document getUserListOutDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_USER_LIST,
            GCXMLUtil.getDocument("<User> <Extn ExtnEmployeeID='" + sExtnEmployeeID + "'/> </User>"),
            GCXMLUtil.getDocument("<UserList><User Username=''></User></UserList>"));

    Element eleUser = (Element) getUserListOutDoc.getElementsByTagName(GCConstants.USER).item(0);
    String sUserName = "";
    if(!YFCCommon.isVoid(eleUser)){
      sUserName = eleUser.getAttribute(GCConstants.USERNAME);
    }
    return sUserName;
  }

// Fix for GCSTORE#4758 -- Start
  /**
   * This method is used fetch Total Tax amount from header level before prorating it.
   *
   * @param inDoc
   * @return
   */
  private double fetchTotalTaxAmount(Document inDoc) {

    LOGGER.debug(" Entering GCBeforeCreateOrderUE.fetchTotalTaxAmount() method ");
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    double totalTaxAmount = 0.0;
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" fetchTotalTaxAmount() method, inputDoc is" + inputDoc.toString());
    }
    YFCElement eleOrder = inputDoc.getDocumentElement();
    YFCNodeList<YFCElement> nlHeaderTax = eleOrder.getElementsByTagName(GCConstants.HEADER_TAX);
    for (YFCElement eleHeaderTax : nlHeaderTax) {
      LOGGER.verbose(" Looping on header tax elements ");
      String sChargeCategory = eleHeaderTax.getAttribute(GCConstants.CHARGE_CATEGORY);
      if (GCXmlLiterals.TOTAL_TAX.equalsIgnoreCase(sChargeCategory)) {
        LOGGER.verbose(" Inside if charge category is TotalTax ");
        totalTaxAmount = eleHeaderTax.getDoubleAttribute(GCConstants.TAX);
        break;
      }
    }
    LOGGER.verbose("totalTaxAmount : " + totalTaxAmount);
    LOGGER.debug(" Exiting GCBeforeCreateOrderUE.fetchTotalTaxAmount() method ");
    return totalTaxAmount;
  }

  // Fix for GCSTORE#4758 -- End

  private void stampAttributesForExchangeOrders(YFSEnvironment env, Document inDoc) {
    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleOrder = yfcInDoc.getDocumentElement();
    String sOrderPurpose = eleOrder.getAttribute("OrderPurpose");
    if (YFCCommon.equals(sOrderPurpose, "EXCHANGE")) {
      YFCElement eleExtn = eleOrder.getChildElement("Extn", true);
      eleExtn.setAttribute("ExtnFulfillmentType", GCConstants.SHIP_2_CUSTOMER);
      eleExtn.setAttribute("ExtnStampUnitCost", "Y");
      String sCustomerID = eleOrder.getAttribute(GCXmlLiterals.BILL_TO_ID);
      String sEnterpriseCode = eleOrder.getAttribute(GCXmlLiterals.ENTERPRISE_CODE);
      YFCDocument docCustomerDetailIp = YFCDocument.createDocument(GCConstants.CUSTOMER);
      YFCElement eleRootAPIInput = docCustomerDetailIp.getDocumentElement();
      eleRootAPIInput.setAttribute(GCConstants.CUSTOMER_ID, sCustomerID);
      eleRootAPIInput.setAttribute(GCConstants.ORGANIZATION_CODE, sEnterpriseCode);
      YFCDocument sInputTmplAPI = YFCDocument.getDocumentFor(GCConstants.GET_CUSTOMER_LIST_TEMPLATE);
      YFCDocument outDocCustomerList =
          GCCommonUtil.invokeAPI(env, GCConstants.GET_CUSTOMER_LIST, docCustomerDetailIp, sInputTmplAPI);
      YFCElement eleCustomerList = outDocCustomerList.getDocumentElement();
      YFCElement eleCustomer = eleCustomerList.getChildElement(GCConstants.CUSTOMER);
      YFCElement eleExtnCustomer = eleCustomer.getChildElement(GCConstants.EXTN);
      String sExtnSourceCustomerID = eleExtnCustomer.getAttribute(GCConstants.EXTN_SOURCE_CUSTOMER_ID);
      if (!YFCCommon.isStringVoid(sExtnSourceCustomerID)) {
        eleExtn.setAttribute(GCConstants.EXTN_SOURCE_CUSTOMER_ID, sExtnSourceCustomerID);
      }

    }
  }


  /**
   * This methods update sourcing related attribute like Fulfillment Type which may not be coming
   * for Exchange order
   *
   * @param env
   * @param inDoc
   */
  private void updateSourcingAttrForExchangeOrder(YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("updateSourcingAttrForExchangeOrder");
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Input to updateSourcingAttrForExchangeOrder=\n" + inputDoc);
    }

    YFCElement orderEle = inputDoc.getDocumentElement();
    String orderPurpose = orderEle.getAttribute(GCConstants.ORDER_PURPOSE);
    if (YFCCommon.isVoid(orderPurpose)) {
      LOGGER.debug("Orderpurpose is void. So not making any changes");
      return;
    }
    if (YFCCommon.equalsIgnoreCase(orderPurpose, "EXCHANGE")) {
      LOGGER.debug("Orderpurpose is Exchange. So going to make the changes");
      YFCElement orderLinesEle = orderEle.getChildElement(GCConstants.ORDER_LINES);
      if (YFCCommon.isVoid(orderLinesEle)) {
        LOGGER.debug("OrderLines element is void. So returning without any changes");
        return;
      }
      YFCNodeList<YFCElement> nlOrderLine = orderLinesEle.getElementsByTagName(GCConstants.ORDER_LINE);

      for (YFCElement eleOrderLine : nlOrderLine) {
        eleOrderLine.setAttribute(GCConstants.FULFILLMENT_TYPE, GCConstants.SHIP_2_CUSTOMER);
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("Updated the fulfillment type to " + GCConstants.SHIP_2_CUSTOMER + " for orderline element \n"
              + eleOrderLine);
        }
      }
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Exiting from updateSourcingAttrForExchangeOrder=\n" + inputDoc);
    }
    LOGGER.endTimer("updateSourcingAttrForExchangeOrder");
  }

  /**
   * GCSTORE-2532::This methods copy's Requested reservation date to product availability at
   * orderline reservation(if present) elements for all orderlines.
   *
   * @param inDoc
   */
  private void stampProductAvailabilityDate(Document inDoc) {
    NodeList orderLineNodeList = inDoc.getElementsByTagName(GCConstants.ORDER_LINE);
    int noOfOrderLine = orderLineNodeList.getLength();
    for (int orderLineCount = 0; orderLineCount < noOfOrderLine; orderLineCount++) {
      Element orderLineEle = (Element) orderLineNodeList.item(orderLineCount);
      // Fix for GCSTORE-2981 : Start
      orderLineEle.setAttribute(GCConstants.DELIVERY_METHOD, GCConstants.SHP);
      // Fix for GCSTORE-2981 : End
      NodeList orderLineReservationNodeList = orderLineEle.getElementsByTagName(GCConstants.ORDER_LINE_RESERVATION);
      int orderLineReservationLength = orderLineReservationNodeList.getLength();
      for (int orderLineReservationCount = 0; orderLineReservationCount < orderLineReservationLength; orderLineReservationCount++) {
        Element orderLineReservationEle = (Element) orderLineReservationNodeList.item(orderLineReservationCount);
        String requestedReservationDate = orderLineReservationEle.getAttribute(GCConstants.REQUESTED_RESERVATION_DATE);
        orderLineReservationEle.setAttribute(GCConstants.PRODUCT_AVAILABILITY_DATE, requestedReservationDate);
      }

    }


  }

  /**
   * This method removes OrderLine OrderLineInvAttRequest Attributes if item is serial bundle.
   *
   * @param env
   * @param inDoc
   * @return
   */
  private Document removeOrderLineInvAttForSerialBundle(YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("GCBeforeCreateOrderUE.removeOrderLineInvAttForSerialBundle");
    LOGGER.verbose("Class: GCBeforeCreateOrderUE Method: removeOrderLineInvAttForSerialBundle ::BEGIN");
    Document cloneInDoc = (Document) inDoc.cloneNode(true);
    YFCDocument cloneYFCDoc = YFCDocument.getDocumentFor(cloneInDoc);
    YFCElement orderElem = cloneYFCDoc.getDocumentElement();
    YFCElement orderLines = orderElem.getChildElement(GCConstants.ORDER_LINES);
    if (!YFCCommon.isVoid(orderLines)) {
      Map<String, String> mapPrimeLineNoNBatchNoForSerialBundle = new HashMap<String, String>();
      for (YFCElement orderLine : orderLines.getElementsByTagName(GCConstants.ORDER_LINE)) {
        if (YFCCommon.equals(GCConstants.BUNDLE, orderLine.getAttribute(GCConstants.KIT_CODE))) {
          YFCElement item = orderLine.getChildElement(GCConstants.ITEM);
          String sItemId = item.getAttribute(GCConstants.ITEM_ID);
          String sUOM = item.getAttribute(GCConstants.UNIT_OF_MEASURE);
          YFCDocument itemDetailDoc =
              GCCommonUtil.getItemList(env, sItemId, sUOM, GCConstants.GCI,
                  YFCDocument.getDocumentFor(GCConstants.GET_ITEM_LST_API_TMP_FOR_UNQ_ITEM));
          GCLogUtils.verboseLog(LOGGER, "output of getItemList api : ", itemDetailDoc);
          YFCElement itemDetail = itemDetailDoc.getDocumentElement().getChildElement(GCConstants.ITEM);
          YFCElement eExtn = itemDetail.getChildElement(GCConstants.EXTN);
          String parentExtnIsUniqueItem = "";
          if (!YFCCommon.isVoid(eExtn)) {
            parentExtnIsUniqueItem = eExtn.getAttribute(GCConstants.EXTN_UNIQUE_ITEM);
          }
          String primeLineNo = orderLine.getAttribute(GCConstants.PRIME_LINE_NO);
          if (YFCCommon.equals(parentExtnIsUniqueItem, GCConstants.YES)) {
            YFCElement eOrderLineInvAttRequest = orderLine.getChildElement(GCXmlLiterals.ORDERLINE_INVATT_REQUEST);
            if (!YFCCommon.isVoid(eOrderLineInvAttRequest)) {
              mapPrimeLineNoNBatchNoForSerialBundle.put(primeLineNo,
                  eOrderLineInvAttRequest.getAttribute(GCConstants.BATCH_NO));
              eOrderLineInvAttRequest.setAttribute(GCConstants.BATCH_NO, "");
            } else {
              mapPrimeLineNoNBatchNoForSerialBundle.put(primeLineNo, "");
            }
          }
        }
      }
      env.setTxnObject(GCConstants.INV_REQ_ATT_FOR_SERIAL_BUNDLE, mapPrimeLineNoNBatchNoForSerialBundle);
      GCLogUtils
      .verboseLog(LOGGER, "InvRequestAttForSerialBundle : ", env.getTxnObject("InvRequestAttForSerialBundle"));
    }
    LOGGER.verbose("GCBeforeCreateOrderUE.removeOrderLineInvAttForSerialBundle method :: END");
    LOGGER.endTimer("GCBeforeCreateOrderUE.removeOrderLineInvAttForSerialBundle");
    return cloneInDoc;
  }

  /**
   * GCSTORE-1594 Copy shipToAddress from OrderLine level to Order
   *
   * @param inDoc
   */
  private void copyShipToFromLineToOrder(YFCDocument inDoc) {
    LOGGER.beginTimer("GCBeforeCreateOrderUE.copyShipToFromLineToOrder");
    YFCElement orderEle = inDoc.getDocumentElement();
    if (YFCCommon.isVoid(orderEle.getChildElement(GCConstants.PERSON_INFO_SHIP_TO))) {
      YFCElement orderLineEle = orderEle.getElementsByTagName(GCConstants.ORDER_LINE).item(0);
      YFCElement shipToElement = orderLineEle.getChildElement(GCConstants.PERSON_INFO_SHIP_TO);
      YFCElement orderShipToEle = (YFCElement) shipToElement.cloneNode(true);
      orderEle.importNode(orderShipToEle);
    }
    LOGGER.beginTimer("GCBeforeCreateOrderUE.copyShipToFromLineToOrder");
  }

  /**
   * This method takes Order details coming in BeforeCreateOrderUE and does following If Serial
   * request then stamp sourcing control as INFINV and Node as coming in order If Clearance request
   * then stamp Store Clearance Fulfillment Type, else if item is clearance then MFI as Firm
   * predefined node If Low value item then stamp Low value fulfillment type
   *
   * @param env
   * @param inDoc
   */
  private void stampOrderLineAttributes(YFSEnvironment env, Document inDoc) {
    LOGGER.beginTimer("GCBeforeCreateOrderUE.stampOrderLineAttributes");
    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Incoming BeforeCreateOrderUE Doc" + yfcInDoc.toString());
    }
    YFCElement orderEle = yfcInDoc.getDocumentElement();
    YFCElement orderLinesEle = orderEle.getChildElement("OrderLines");
    YFCNodeList<YFCElement> orderLineList = orderLinesEle.getElementsByTagName("OrderLine");
    for (YFCElement orderLineEle : orderLineList) {
      String fulfillmentType = orderLineEle.getAttribute("FulfillmentType");
      YFCElement itemEle = orderLineEle.getChildElement("Item");
      String sItemID = itemEle.getAttribute("ItemID");
      String sUOM = itemEle.getAttribute("UnitOfMeasure");

      YFCDocument getItemLstTmpl =
          YFCDocument.getDocumentFor(GCXMLUtil.getDocument(GCConstants.GET_ITEM_LIST_TEMPLATE_ORDER_PROMISING));

      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("GetItemList Output Template XML::" + getItemLstTmpl.toString());
      }
      // call getItemList to check for clearance request or clearance item
      YFCDocument getItemListOp = GCCommonUtil.getItemList(env, sItemID, sUOM, GCConstants.GCI, true, getItemLstTmpl);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose("GetItemList Output XML::" + getItemListOp.toString());
      }
      // GCSTORE - 6650 START
      String kitCode = orderLineEle.getAttribute(GCConstants.KIT_CODE);
      String productClass = ""; 
      YFCElement orderLineItemEle = orderLineEle.getChildElement(GCConstants.ITEM);
      if(!YFCCommon.isVoid(orderLineItemEle)){
    	  productClass = orderLineItemEle.getAttribute(GCConstants.PRODUCT_CLASS);
      }
      if(LOGGER.isDebugEnabled()){
    	  LOGGER.debug("orderLineItemEle is "+orderLineItemEle.toString());
    	  LOGGER.debug("kitCode ="+kitCode+" productClass ="+productClass);
    	  LOGGER.debug("IS_FIRM_PREDEFINED_NODE is "+orderLineEle.getAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE));
    	  LOGGER.debug("SHIP_NODE ="+orderLineEle.getAttribute(GCConstants.SHIP_NODE));
      }
      if(YFCCommon.equals(kitCode, GCConstants.BUNDLE) && 
    		  YFCCommon.equals(productClass, GCConstants.REGULAR)){
    	  orderLineEle.setAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE, "N");
    	  orderLineEle.setAttribute(GCConstants.SHIP_NODE, "");
      }

      if(LOGGER.isDebugEnabled()){
    	  LOGGER.debug("After:::");
    	  LOGGER.debug("IS_FIRM_PREDEFINED_NODE is "+orderLineEle.getAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE));
    	  LOGGER.debug("SHIP_NODE ="+orderLineEle.getAttribute(GCConstants.SHIP_NODE));
      }
      // GCSTORE - 6650 END

      // Handling Serial Request first. i.e. stamping INFINV if it is serial request and not a
      // bundle (bundle does not support INFINV, component will take care of INFINV)
      // Serial request is identified as ShipNode and BatchNo is not blank, KitCode is blank (means
      // do not consider bundle) and
      String shipNode = orderLineEle.getAttribute("ShipNode");
      String isFirmPreDefine = orderLineEle.getAttribute("IsFirmPredefinedNode");
      String batchNo = "";
      
      YFCElement orderLineInvAttReq = orderLineEle.getChildElement("OrderLineInvAttRequest");
      if (!YFCCommon.isVoid(orderLineInvAttReq)) {
        batchNo = orderLineInvAttReq.getAttribute("BatchNo");
      }

      if (!YFCCommon.isVoid(shipNode) && YFCCommon.equals(isFirmPreDefine, GCConstants.YES, false)
          && YFCCommon.isVoid(kitCode) && !YFCCommon.isVoid(batchNo)) {
        YFCElement orderLinSrcCtrls = orderLineEle.getChildElement("OrderLineSourcingControls");
        if (YFCCommon.isVoid(orderLinSrcCtrls)) {
          orderLinSrcCtrls = orderLineEle.createChild("OrderLineSourcingControls");
        }
        YFCElement orderLinSrcCtrl = orderLinSrcCtrls.createChild("OrderLineSourcingCntrl");
        orderLinSrcCtrl.setAttribute("Action", "CREATE");
        orderLinSrcCtrl.setAttribute("InventoryCheckCode", "INFINV");
        orderLinSrcCtrl.setAttribute("Node", shipNode);
        if (LOGGER.isVerboseEnabled()) {
          LOGGER.verbose("Added infinite inventory check with node=" + shipNode);
          LOGGER.verbose("Now document is updated with orderLine element as " + orderLineEle.toString());
        }

      } else {
        // Handling clearance Request or if not request then clearance item in orderline
        // get getItemList Template

        boolean isClearanceItem = GCOrderUtil.isClearanceItem(getItemListOp);
        //Added for low value check - GCSTORE-6864/6865
        boolean isLowValueItem = GCOrderUtil.isLowValueItem(getItemListOp);
        boolean isReqRelatedToClearanceItem =
            stampFulfillmentTypeForClearance(orderLineEle, isClearanceItem, fulfillmentType);
        if (!isReqRelatedToClearanceItem) {
          boolean bSingleSourced = GCOrderUtil.isSingleSourced(getItemListOp);
          boolean bGiftCard = GCOrderUtil.isGiftCard(getItemListOp);
          boolean bDigital = GCOrderUtil.isDigital(getItemListOp);
          //Removed for GCSTORE-6864/6865
         // YFCElement linePriceInfo = orderLineEle.getChildElement("LinePriceInfo");
          //String itemPrice = linePriceInfo.getAttribute("UnitPrice");
          //if (YFCCommon.isVoid(itemPrice)) {
           // itemPrice = linePriceInfo.getAttribute("ListPrice");
          //}
          if (/*GCOrderUtil.isLowValueItem(env, "GC", itemPrice)*/ 
              ////Added for low value check - GCSTORE-6864/6865
        	  isLowValueItem && !bSingleSourced && !bGiftCard && !bDigital) {
            // EnterpriseCode could have been used. But "GC" is used to check low value, because low
            // value thresold is defined at GC but EnterpriseCode can be non GC (as per interface
            // map)
            // GCSTORE-7165--Start
        	GCOrderUtil.stampExtnRestrictShipFromStoreAtOrderLine(orderLineEle);
        	// GCSTORE-7165--End
            if (YFCCommon.equals(fulfillmentType, GCConstants.SHIP_2_CUSTOMER, false)) {
              orderLineEle.setAttribute(GCConstants.FULFILLMENT_TYPE, GCConstants.SHIP_2_CUSTOMER_LOW_VALUE);
            } else if (YFCCommon.equals(fulfillmentType, GCConstants.PICKUP_IN_STORE, false)) {
              orderLineEle.setAttribute(GCConstants.FULFILLMENT_TYPE, GCConstants.PICKUP_IN_STORE_LOW_VALUE);
            }
            if (LOGGER.isVerboseEnabled()) {
              //LOGGER.verbose("Found low value item with itemPrice=" + itemPrice);
              LOGGER.verbose("Now document is updated with orderLine element as " + orderLineEle.toString());
            }
          }
        }
      }
      String sFulfillmentType = orderLineEle.getAttribute(GCXmlLiterals.FULFILLMENT_TYPE);
      boolean isKit = GCOrderUtil.isRegularKit(env, getItemListOp);
      if (isKit) {

        if (YFCCommon.equals(sFulfillmentType, GCConstants.SHIP_2_CUSTOMER, false) || 
        		YFCCommon.equals(sFulfillmentType, GCConstants.SHIP_2_CUSTOMER_LOW_VALUE, false)) { // Added condition as a part of GCSTORE 6650
          sFulfillmentType = GCConstants.SHIP_2_CUSTOMER_KIT;
        }
        if (YFCCommon.equals(sFulfillmentType, GCConstants.PICKUP_IN_STORE, false) ||
        		YFCCommon.equals(sFulfillmentType, GCConstants.PICKUP_IN_STORE_LOW_VALUE, false)) { // Added condition as a part of GCSTORE 6650
          sFulfillmentType = GCConstants.PICKUP_IN_STORE_KIT;
        }
        if (YFCCommon.equals(sFulfillmentType, GCConstants.SHIP_2_CUSTOMER_ONHAND, false)) {
          sFulfillmentType = GCConstants.SHIP_2_CUSTOMER_KIT_ONHAND;
        }
        if (YFCCommon.equals(sFulfillmentType, GCConstants.PICKUP_IN_STORE_ONHAND, false)) {
          sFulfillmentType = GCConstants.PICKUP_IN_STORE_KIT_ONHAND;
        }
        orderLineEle.setAttribute(GCConstants.FULFILLMENT_TYPE, sFulfillmentType);
      }
    }

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Outgoing BeforeCreateOrderUE Doc" + yfcInDoc.toString());
    }
    LOGGER.endTimer("GCBeforeCreateOrderUE.stampOrderLineAttributes");
  }

  /**
   * This method stamps fulfilment type for clearance. If request is for clearance item, it stamps
   * SHIP_2_CUSTOMER_SC/PICKUP_IN_STORE_SC for SHIP_2_CUSTOMER/PICKUP_IN_STORE respectively If
   * request is not for clearance, but item is clearance. then Stamp MFI as fixed node.
   *
   * @param orderLineEle
   * @param isClearanceItem
   * @param fulfillmentType
   * @return
   */
  private boolean stampFulfillmentTypeForClearance(YFCElement orderLineEle, boolean isClearanceItem,
      String fulfillmentType) {
    LOGGER.beginTimer("GCBeforeCreateOrderUE.stampFulfillmentTypeForClearance");
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Input to stampFulfillmentTypeForClearance  : " + orderLineEle.getString());
    }

    YFCElement eleExtn = orderLineEle.getChildElement(GCConstants.EXTN);
    if (!YFCCommon.isVoid(eleExtn)) {
      String sExtnIsStoreClearance = eleExtn.getAttribute(GCXmlLiterals.EXTN_IS_STORE_CLEARANCE);
      if (YFCCommon.equals(sExtnIsStoreClearance, GCConstants.YES, false)) {
        LOGGER.verbose("This is a clearance Item Request");
        if (YFCCommon.equals(fulfillmentType, GCConstants.SHIP_2_CUSTOMER, false)) {
          orderLineEle.setAttribute(GCConstants.FULFILLMENT_TYPE, GCConstants.SHIP_2_CUSTOMER_STORE_CLEARANCE);
        } else {
          orderLineEle.setAttribute(GCConstants.FULFILLMENT_TYPE, GCConstants.PICKUP_IN_STORE_STORE_CLEARANCE);
        }
        return true;
      } else if (isClearanceItem) {
        orderLineEle.setAttribute("ShipNode", GCConstants.MFI);
        orderLineEle.setAttribute("IsFirmPredefinedNode", GCConstants.YES);
        return true;
      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("Output from stampFulfillmentTypeForClearance  : " + orderLineEle.getString());
    }
    LOGGER.endTimer("GCBeforeCreateOrderUE.stampFulfillmentTypeForClearance");
    return false;
  }

  /**
   * This method takes stamps sourcing classification on order if it is APO/FPO/PO Box address,
   * DCEmployee adddress or prepaid orders
   *
   * @param inDoc
   * @param isAPOFPOPOAddress
   */
  private void stampSourcingClassification(YFSEnvironment env, Document inDoc, boolean isDCSourcedOrder) {
    YFCDocument orderDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement orderEle = orderDoc.getDocumentElement();
    String currentSourcingClassification = orderEle.getAttribute("SourcingClassification");
    if (!YFCCommon.equals(currentSourcingClassification, GCConstants.DC_SOURCED_SOURCING, false)) {
      if (isDCSourcedOrder || isDCEmployeeOrder(orderEle)) {
        orderEle.setAttribute("SourcingClassification", GCConstants.DC_SOURCED_SOURCING);
      } else if (!YFCCommon.equals(currentSourcingClassification, GCConstants.PREPAID_SOURCING, false)
          && GCOrderUtil.isPrePaidOrder(env, orderEle.getChildElement("PaymentMethods"),
              orderEle.getAttribute("EnterpriseCode"))) {
        orderEle.setAttribute("SourcingClassification", GCConstants.PREPAID_SOURCING);
      }
    }

  }

  /**
   * This method checks if SourceCode is DCEMPLOYEE then returns true. else false
   *
   * @param orderEle
   * @return
   */
  private boolean isDCEmployeeOrder(YFCElement orderEle) {
    if (YFCCommon.isVoid(orderEle)) {
      return false;
    }
    YFCElement extEle = orderEle.getChildElement("Extn");
    if (!YFCCommon.isVoid(extEle) && YFCCommon.equals(extEle.getAttribute("ExtnSourceCode"), "DCEMPLOYEE", false)) {
      return true;
    }
    return false;
  }

  /**
   *
   * Description of checkBundleOrderLine
   *
   * @param env
   * @param inDoc
   * @throws GCException
   *
   */
  private void checkBundleOrderLine(YFSEnvironment env, Document inDoc) throws GCException {
    try {
      NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc, "Order/OrderLines/OrderLine");
      for (int counter = 0; counter < nlOrderLine.getLength(); counter++) {
        Element eleOrderLine = (Element) nlOrderLine.item(counter);
        if (GCConstants.BUNDLE.equalsIgnoreCase(eleOrderLine.getAttribute(GCConstants.KIT_CODE))) {
          env.setTxnObject("isBundleLineCreateOrder", GCConstants.YES);
          break;
        }
      }
    } catch (Exception e) {
      LOGGER.error(" Inside catch block of GCBeforeCreateOrderUE.checkBundleOrderLine(), Exception is :", e);
      throw new GCException(e);
    }
  }


  /**
   *
   * Description of validateOrder Check if DAXOrder number being passed already exist, if yes do not
   * create the order and create an exception for order number to the exception queue If DAX Order
   * number or Source order number both are missing, create an exception to the queue, saying
   * �Source Order No Missing�
   *
   * @param env
   * @param inDoc
   * @throws GCException
   *
   */
  private void validateOrder(YFSEnvironment env, Document inDoc) throws GCException {
    LOGGER.debug(" Entering GCBeforeCreateOrderUE.validateOrder() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" Input document is" + GCXMLUtil.getXMLString(inDoc));
    }
    try {
      // Phase -2 :Start
      YFCDocument yfcCreateOrdIpDoc = YFCDocument.getDocumentFor(inDoc);
      YFCElement orderEle = yfcCreateOrdIpDoc.getDocumentElement();
      YFCElement extnEle = orderEle.getChildElement(GCConstants.EXTN);
      if (!YFCCommon.isVoid(extnEle)) {
        String sExtnSourceSystem = extnEle.getAttribute(GCXmlLiterals.EXTN_SOURCE_SYSTEM);
        String sExtnSourceOrdNo = extnEle.getAttribute(GCXmlLiterals.EXTN_SOURCE_ORD_NO);

        // SourceOrderNo and SourceSystem is required for both ATG and
        // JBS orders
        if (!YFCCommon.isVoid(sExtnSourceOrdNo) && !YFCCommon.isVoid(sExtnSourceSystem)) {
          if (YFCUtils.equals(GCConstants.SOURCE_SYSTEM_JBS, sExtnSourceSystem)) {
            // Phase -2 : End
            String sExtnDAXOrderNo = GCXMLUtil.getAttributeFromXPath(inDoc, "Order/Extn/@ExtnDAXOrderNo");
            if (YFCCommon.isVoid(sExtnDAXOrderNo)) {
              LOGGER.debug(" Throwing DAX Order No is Missing' exception ");
              GCException gcException = new GCException("DAX Order No is Missing ");
              throw gcException;
            } else {
              Document docGetOrderListIP = GCXMLUtil.createDocument(GCConstants.ORDER);
              Element eleGetOrderList = docGetOrderListIP.getDocumentElement();
              Element eleExtn = GCXMLUtil.createElement(docGetOrderListIP, GCConstants.EXTN, null);
              eleExtn.setAttribute(GCConstants.EXTN_DAX_ORDER_NO, sExtnDAXOrderNo);
              eleGetOrderList.appendChild(eleExtn);
              Document docGetOrderListOP =
                  GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST, docGetOrderListIP);
              NodeList nlOrder = XPathAPI.selectNodeList(docGetOrderListOP, "OrderList/Order");
              if ((!YFCCommon.isVoid(nlOrder)) && nlOrder.getLength() > 0) {
                LOGGER.debug(" Throwing 'DAX Order No already exists' exception ");
                GCException gcException = new GCException(" DAX Order No already exists");
                throw gcException;
              }
            }
          } else if (YFCUtils.equals("ATG", sExtnSourceSystem)) {
            // Phase -2 :Start
            checkATGOrders(env, yfcCreateOrdIpDoc, sExtnSourceSystem, sExtnSourceOrdNo);
          } else {
            YFCException excep = new YFCException(GCErrorConstants.SOURCE_SYSTEM_NOT_SUPPORTED_ERROR_CODE);
            excep.setAttribute(GCXmlLiterals.EXTN_SOURCE_SYSTEM, sExtnSourceSystem);
            excep.setAttribute(GCXmlLiterals.EXTN_SOURCE_ORD_NO, sExtnSourceOrdNo);
            LOGGER.endTimer("raiseException");
            throw excep;
          }
        } else {
          // throw exception source order no And Source system is
          // mandatory
          YFCException excep = new YFCException(GCErrorConstants.SOURCE_CODE_MANDATORY_ERROR_CODE);
          excep.setAttribute(GCXmlLiterals.EXTN_SOURCE_SYSTEM, sExtnSourceSystem);
          excep.setAttribute(GCXmlLiterals.EXTN_SOURCE_ORD_NO, sExtnSourceOrdNo);
          LOGGER.endTimer("raiseException");
          throw excep;
        }
      }
    } catch (GCException yfs) {
      LOGGER.error("Inside catch block of GCBeforeCreateOrderUE.validateOrder(),YFS UE Exception is Exception is :",
          yfs);
      throw new GCException(yfs);
    } catch (Exception e) {
      LOGGER.error(" Inside catch block of GCBeforeCreateOrderUE.validateOrder(), Exception is :", e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCBeforeCreateOrderUE.validateOrder() method ");
  }

  /**
   * This method calls methods to validate ATGOrder and PaymentMethod.
   *
   * @param env
   * @param yfcCreateOrdIpDoc
   * @param sExtnSourceSystem
   * @param sExtnSourceOrdNo
   */
  public static void checkATGOrders(YFSEnvironment env, YFCDocument yfcCreateOrdIpDoc, String sExtnSourceSystem,
      String sExtnSourceOrdNo) {
    LOGGER.beginTimer("GCATGOrder.atgOrders");
    LOGGER.verbose("Class: GCATGOrder Method: atgOrders ::BEGIN");
    // validate ATG Order and raise Exception if order exist in OMS
    validateATGOrder(env, sExtnSourceSystem, sExtnSourceOrdNo);
    validatePaymentMethod(yfcCreateOrdIpDoc);
    LOGGER.verbose("GCATGOrder.atgOrders() method :: END");
    LOGGER.endTimer("GCATGOrder.atgOrders");
  }

  /**
   * This method throws exception if order is not ATG order.
   *
   * @param env
   * @param sExtnSourceSystem
   * @param sExtnSourceOrdNo
   */
  private static void validateATGOrder(YFSEnvironment env, String sExtnSourceSystem, String sExtnSourceOrdNo) {
    LOGGER.beginTimer("GCATGOrder.validateATGOrder");
    LOGGER.verbose("Class: GCATGOrder Method: validateATGOrder ::BEGIN");
    YFCDocument getOrderListIp = YFCDocument.createDocument(GCConstants.ORDER);
    YFCElement orderEle = getOrderListIp.getDocumentElement();
    YFCElement extnEle = orderEle.createChild(GCConstants.EXTN);
    extnEle.setAttribute(GCXmlLiterals.EXTN_SOURCE_SYSTEM, sExtnSourceSystem);
    extnEle.setAttribute(GCXmlLiterals.EXTN_SOURCE_ORD_NO, sExtnSourceOrdNo);

    YFCDocument getOrdListTemp = YFCDocument.getDocumentFor(GCConstants.GET_ORDER_LIST_TEMPLATE);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("\n getOrderListOp Input Doc" + getOrderListIp);
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("\n getOrderListOp Template Doc" + getOrdListTemp);
    }

    YFCDocument getOrderListOp =
        GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST, getOrderListIp, getOrdListTemp);

    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("\n getOrderListOp Output Doc" + getOrderListOp);
    }
    YFCElement ordListEle = getOrderListOp.getDocumentElement();
    YFCNodeList<YFCElement> orderList = ordListEle.getElementsByTagName(GCConstants.ORDER);

    if (orderList.getLength() > 0) {
      LOGGER.verbose("Order Already captured in OMS");
      YFCException excep = new YFCException(GCErrorConstants.ORDER_ALREADY_CAPTURED_ERROR_CODE);
      excep.setAttribute(GCXmlLiterals.EXTN_SOURCE_SYSTEM, sExtnSourceSystem);
      excep.setAttribute(GCXmlLiterals.EXTN_SOURCE_ORD_NO, sExtnSourceOrdNo);
      LOGGER.endTimer("raiseException");
      throw excep;
    }
    LOGGER.verbose("GCATGOrder.validateATGOrder() method :: END");
    LOGGER.endTimer("GCATGOrder.validateATGOrder");
  }

  /**
   * This method validate Payment Method for ATG orders.
   *
   * @param env
   * @param yfcCreateOrdIpDoc
   */
  private static void validatePaymentMethod(YFCDocument yfcCreateOrdIpDoc) {
    LOGGER.beginTimer("GCATGOrder.validatePaymentMethod");
    LOGGER.verbose("Class: GCATGOrder Method: validatePaymentMethod ::BEGIN");
    YFCElement orderEle = yfcCreateOrdIpDoc.getDocumentElement();
    YFCElement paymentMethodsEle = orderEle.getChildElement(GCConstants.PAYMENT_METHODS);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("paymentMethodsEle:\n" + paymentMethodsEle);
    }

    if (!YFCCommon.isVoid(paymentMethodsEle)) {
      YFCNodeList<YFCElement> paymentMethodList = paymentMethodsEle.getElementsByTagName(GCConstants.PAYMENT_METHOD);
      for (YFCElement paymentMethodEle : paymentMethodList) {
        String paymentType = paymentMethodEle.getAttribute(GCConstants.PAYMENT_TYPE);
        if (!(YFCUtils.equals(GCConstants.PAYPAL, paymentType) || YFCUtils.equals(GCConstants.BML, paymentType))) {
          continue;
        } else {
          YFCElement paymentDetailsEle = paymentMethodEle.getChildElement(GCXmlLiterals.PAYMENT_DETAILS);
          if (!YFCCommon.isVoid(paymentDetailsEle)) {
            String sChargeType = paymentDetailsEle.getAttribute(GCConstants.CHARGE_TYPE);
            double sRequestAmount = paymentDetailsEle.getDoubleAttribute(GCXmlLiterals.REQUEST_AMOUNT);
            double sProcessedAmount = paymentDetailsEle.getDoubleAttribute(GCConstants.PAYMENT_PROCESSED_AMOUNT);

            if (YFCUtils.equals(GCConstants.AUTHORIZATION, sChargeType)
                && YFCCommon.equals(sProcessedAmount, sRequestAmount)) {
              continue;
            } else {
              LOGGER.verbose("Raise exception as Offline Auth not Supported");
              YFCException excep = new YFCException(GCErrorConstants.OFFLINE_AUTH_NOT_SUPPORTED_ERROR_CODE);
              excep.setAttribute(GCConstants.PAYMENT_TYPE, paymentType);
              excep.setAttribute(GCConstants.CHARGE_TYPE, sChargeType);
              excep.setAttribute(GCXmlLiterals.REQUEST_AMOUNT,
                  paymentDetailsEle.getAttribute(GCXmlLiterals.REQUEST_AMOUNT));
              excep.setAttribute(GCConstants.PAYMENT_PROCESSED_AMOUNT,
                  paymentDetailsEle.getAttribute(GCConstants.PAYMENT_PROCESSED_AMOUNT));
              LOGGER.endTimer("raiseException");
              throw excep;
            }
          } else {
            YFCException excep = new YFCException(GCErrorConstants.OFFLINE_AUTH_NOT_SUPPORTED_ERROR_CODE);
            excep.setAttribute(GCConstants.PAYMENT_TYPE, paymentType);
            LOGGER.endTimer("raiseException");
            throw excep;
          }
        }
      }
    }
    LOGGER.verbose("GCATGOrder.validatePaymentMethod() method :: END");
    LOGGER.endTimer("GCATGOrder.validatePaymentMethod");
  }

  /**
   *
   * Description of checkDigitalItems See if this is digital item, set the FulfilmentType as
   * �SHIP_TO_CUSTOMER� on the order
   *
   * @param env
   * @param inDoc
   * @return
   * @throws GCException
   *
   */
  private Document checkDigitalItems(YFSEnvironment env, Document inDoc) throws GCException {
    LOGGER.debug(" Entering GCBeforeCreateOrderUE.checkDigitalItems() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" Input document is" + GCXMLUtil.getXMLString(inDoc));
    }
    try {
      NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc, "Order/OrderLines/OrderLine");
      for (int iCounter = 0; iCounter < nlOrderLine.getLength(); iCounter++) {
        Element eleOrderLine = (Element) nlOrderLine.item(iCounter);
        Element eleItem = (Element) XPathAPI.selectSingleNode(eleOrderLine, GCConstants.ITEM);
        String sItemID = eleItem.getAttribute(GCConstants.ITEM_ID);
        String sUOM = eleItem.getAttribute(GCConstants.UOM);
        Document docGetItemListOP = GCCommonUtil.getItemList(env, sItemID, sUOM, GCConstants.GCI);
        String sItemType =
            GCXMLUtil.getAttributeFromXPath(docGetItemListOP, "ItemList/Item/PrimaryInformation/@ItemType");
        LOGGER.debug(" sItemType is " + sItemType);

        if (GCConstants.DIGITAL_ITEM_TYPE.equalsIgnoreCase(sItemType)) {
          String fulfillmentType = eleOrderLine.getAttribute(GCConstants.FULFILLMENT_TYPE);
          // Added SHIP_2_CUSTOMER for phase-2 keeping phase-1 untouched
          if (YFCCommon.equals(fulfillmentType, GCConstants.SHIP_2_CUSTOMER, false)
              || YFCCommon.equals(fulfillmentType, GCConstants.PICKUP_IN_STORE, false)) {
            eleOrderLine.setAttribute(GCConstants.FULFILLMENT_TYPE, GCConstants.SHIP_2_CUSTOMER);
          } else {
            eleOrderLine.setAttribute(GCConstants.FULFILLMENT_TYPE, GCConstants.SHIP_TO_CUSTOMER);
          }
        }
      }
    } catch (Exception e) {
      LOGGER.error(" Inside catch block of GCBeforeCreateOrderUE.validateOrder(), Exception is :", e);
      throw new GCException(e);
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" Updated Input document is" + GCXMLUtil.getXMLString(inDoc));
    }
    LOGGER.debug(" Exiting GCBeforeCreateOrderUE.checkDigitalItems() method ");
    return inDoc;
  }

  /**
   *
   * Description of stampSetProductClass Stamp ProductClass = 'SET' if ExtnSetCode='1' for bundle
   * item
   *
   * @param env
   * @param inDoc
   * @throws GCException
   *
   */
  private void stampSetProductClass(YFSEnvironment env, Document inDoc) throws GCException {
    LOGGER.debug(" Entering GCBeforeCreateOrderUE.stampSetProductClass() method ");
    try {
      String sOrganizationCode = inDoc.getDocumentElement().getAttribute(GCConstants.ENTERPRISE_CODE);
      NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc, "Order/OrderLines/OrderLine");
      for (int iOrderLineCounter = 0; iOrderLineCounter < nlOrderLine.getLength(); iOrderLineCounter++) {
        LOGGER.debug(" Inside Loop on parent lines, iParentLineCounter is :" + iOrderLineCounter);

        Element eleOrderLine = (Element) nlOrderLine.item(iOrderLineCounter);
        Element eleItem = (Element) XPathAPI.selectSingleNode(eleOrderLine, GCConstants.ITEM);
        if (!YFCCommon.isVoid(eleItem)) {
          String sExtnSetCode = checkForTrackedAsSet(env, eleItem, sOrganizationCode);
          LOGGER.debug(" sExtnSetCode is :" + sExtnSetCode);

          if ((!YFCCommon.isVoid(sExtnSetCode)) && GCCommonUtil.isAffirmative(sExtnSetCode)) {
            eleItem.setAttribute(GCConstants.PRODUCT_CLASS, GCConstants.SET);
          }
        }
      }
    } catch (Exception e) {
      LOGGER.error(" Inside catch block of GCBeforeCreateOrderUE.stampSetProductClass(), Exception is :", e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCBeforeCreateOrderUE.stampSetProductClass() method ");
  }


  // Defect-3609
  /**
   * This method is used to cancel the ATG reservations.
   *
   * @param env
   * @param inDoc
   */
  private void cancelEarlierReservation(YFSEnvironment env, Document inDoc) {

    LOGGER.beginTimer("GCBeforeCreateOrderUE.cancelEarlierReservation");
    LOGGER.verbose("Class: GCBeforeCreateOrderUE Method: cancelEarlierReservation BEGIN");

    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleRoot = inputDoc.getDocumentElement();
    YFCElement eleOrderLines = eleRoot.getChildElement(GCConstants.ORDER_LINES);
    YFCNodeList<YFCElement> listOrderLine = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
    for (YFCElement eleOrderLine : listOrderLine) {
      String kitCode = eleOrderLine.getAttribute(GCConstants.KIT_CODE);
      YFCElement eleOrderLineReservations = eleOrderLine.getChildElement(GCConstants.ORDER_LINE_RESERVATIONS);
      if (!YFCCommon.isVoid(eleOrderLineReservations)) {
        YFCNodeList<YFCElement> listReservations =
            eleOrderLineReservations.getElementsByTagName(GCConstants.ORDER_LINE_RESERVATION);
        for (int i = 0; i < listReservations.getLength(); i++) {
          YFCElement eleOrderLineReservation = listReservations.item(i);
          String sBatchNo = eleOrderLineReservation.getAttribute(GCConstants.BATCH_NO);
          String sItemID = eleOrderLineReservation.getAttribute(GCConstants.ITEM_ID);
          String sNode = eleOrderLineReservation.getAttribute(GCConstants.NODE);
          String sProductClass = eleOrderLineReservation.getAttribute(GCConstants.PRODUCT_CLASS);
          String sQuantity = eleOrderLineReservation.getAttribute(GCConstants.QUANTITY);
          String sReservationID = eleOrderLineReservation.getAttribute(GCConstants.RESERVATION_ID);
          String sUnitOfMeasure = eleOrderLineReservation.getAttribute(GCConstants.UNIT_OF_MEASURE);
          String sRequestedReservationDate =
              eleOrderLineReservation.getAttribute(GCConstants.REQUESTED_RESERVATION_DATE);

          // Creating a Doc for CancelReservationWrapperService
          YFCDocument cancelReservationWrapperIndoc = YFCDocument.createDocument("CancelReservationLineList");
          YFCElement eleCancelReservationLineList = cancelReservationWrapperIndoc.getDocumentElement();
          YFCElement eleCancelReservation = eleCancelReservationLineList.createChild("CancelReservation");
          eleCancelReservation.setAttribute(GCConstants.ITEM_ID, sItemID);
          eleCancelReservation.setAttribute(GCConstants.PRODUCT_CLASS, sProductClass);
          eleCancelReservation.setAttribute(GCConstants.QTY_TO_BE_CANCELLED, sQuantity);
          eleCancelReservation.setAttribute(GCConstants.RESERVATION_ID, sReservationID);
          eleCancelReservation.setAttribute(GCConstants.SHIP_NODE, sNode);
          eleCancelReservation.setAttribute(GCConstants.UNIT_OF_MEASURE, sUnitOfMeasure);
          eleCancelReservation.setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GCI);
          eleCancelReservation.setAttribute(GCConstants.SHIP_DATE, sRequestedReservationDate);
          YFCElement eleExtn = eleCancelReservation.createChild(GCConstants.EXTN);
          eleExtn.setAttribute("ExtnBatchNo", sBatchNo);


          if (LOGGER.isVerboseEnabled()) {
            LOGGER.verbose("cancelReservationWrapperIndoc" + cancelReservationWrapperIndoc.getString());
          }

          GCCommonUtil.invokeService(env, GCConstants.GC_CANCEL_RESERVATION_WRAPPER_SERVICE,
              cancelReservationWrapperIndoc);

          YFCElement eleOdrLineExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
          updateReservationDateForStoreSourced(env, sBatchNo, eleOdrLineExtn, eleOrderLineReservation, sItemID,
              sUnitOfMeasure, sNode);
          if (YFCUtils.equals(kitCode, GCConstants.BUNDLE)
              && YFCUtils.equals(sProductClass, GCConstants.REGULAR)) {
            eleOrderLineReservations.removeChild(eleOrderLineReservation);
            i--;
          }


        }
      }
    }
    LOGGER.endTimer("GCBeforeCreateOrderUE.cancelEarlierReservation");
  }

  /**
   * This method is used to stamp current date as reservationRequestDate if the line is getting
   * fulfilled from store.
   *
   * @param env
   *
   * @param sBatchNo
   * @param eleOdrLineExtn
   * @param eleOrderLineReservation
   * @param sUnitOfMeasure
   * @param sItemID
   * @param sNode
   */
  private void updateReservationDateForStoreSourced(YFSEnvironment env, String sBatchNo, YFCElement eleOdrLineExtn,
      YFCElement eleOrderLineReservation, String sItemID, String sUnitOfMeasure, String sNode) {

    LOGGER.debug(" Entering GCBeforeCreateOrderUE.updateReservationDateForStoreSourced() method ");
    YFCDocument docGetItemListOP = GCCommonUtil.getItemList(env, sItemID, sUnitOfMeasure, GCConstants.GCI, true, null);

    if (LOGGER.isDebugEnabled()) {
      if (!YFCCommon.isVoid(eleOdrLineExtn)) {
        LOGGER.debug(" eleOdrLineExtn is " + eleOdrLineExtn.toString());
      }
      LOGGER.debug(" docGetItemListOP is " + docGetItemListOP.toString());
    }
    String isUsedOrVintage =
        GCXMLUtil.getAttributeFromXPath(docGetItemListOP.getDocument(), "ItemList/Item/Extn/@ExtnIsUsedOrVintageItem");
    LOGGER.debug(" isUsedOrVintage is " + isUsedOrVintage);
    String nodeType = GCCommonUtil.getOrganizationNodeType(env, sNode);
    if (YFCUtils.equals(GCConstants.STORE, nodeType)) {

      eleOrderLineReservation.setAttribute(GCConstants.REQUESTED_RESERVATION_DATE,
          GCDateUtils.getCurrentTime(GCConstants.DATE_FORMAT) + "T00:00:00-07:00");
      eleOrderLineReservation.setAttribute("ProductAvailabilityDate",
          GCDateUtils.getCurrentTime(GCConstants.DATE_FORMAT) + "T00:00:00-07:00");
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" eleOrderLineReservation is " + eleOrderLineReservation.toString());
    }
    LOGGER.debug(" Exiting GCBeforeCreateOrderUE.updateReservationDateForStoreSourced() method ");
  }

  /**
   *
   * Description of stampApoFpoPOBoxAddress Check for the APO/FPO and POBOX at Order level and
   * OrderLine level
   *
   * @param inDoc
   * @throws GCException
   *
   */
  private boolean stampApoFpoPOBoxAddress(Document inDoc) throws GCException {
    LOGGER.debug(" Entering GCBeforeCreateOrderUE.stampApoFpoPOBoxAddress() method ");
    boolean isDCSourcedOrder = false;
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" stampApoFpoPOBoxAddress() method, Input document is" + GCXMLUtil.getXMLString(inDoc));
    }
    try {
      Element eleOrderPersonInfoShipTo = (Element) XPathAPI.selectSingleNode(inDoc, "Order/PersonInfoShipTo");
      if (!YFCCommon.isVoid(eleOrderPersonInfoShipTo)) {
        boolean isAPOFPO = stampApoFpo(eleOrderPersonInfoShipTo, inDoc);
        boolean isPO = stampPOBox(eleOrderPersonInfoShipTo, inDoc);
        boolean isCountryNotUS =
            !YFCCommon.equals(eleOrderPersonInfoShipTo.getAttribute("Country"), GCConstants.US, false);
        isDCSourcedOrder = isDCSourcedOrder || isAPOFPO || isPO || isCountryNotUS;
      }
      NodeList nlPersonInfoShipTo = XPathAPI.selectNodeList(inDoc, "Order/OrderLines/OrderLine/PersonInfoShipTo");
      for (int iCounter = 0; iCounter < nlPersonInfoShipTo.getLength(); iCounter++) {
        Element elePersonInfoShipTo = (Element) nlPersonInfoShipTo.item(iCounter);
        if (!YFCCommon.isVoid(elePersonInfoShipTo)) {
          boolean isAPOFPO = stampApoFpo(elePersonInfoShipTo, inDoc);
          boolean isPO = stampPOBox(elePersonInfoShipTo, inDoc);
          boolean isCountryNotUS =
              !YFCCommon.equals(elePersonInfoShipTo.getAttribute("Country"), GCConstants.US, false);
          isDCSourcedOrder = isDCSourcedOrder || isAPOFPO || isPO || isCountryNotUS;
        }
      }
    } catch (Exception e) {
      LOGGER.error(" Inside catch of GCBeforeCreateOrderUE.stampApoFpoPOBoxAddress()", e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCBeforeCreateOrderUE.stampApoFpoPOBoxAddress() method with isDCSourcedOrder"
        + isDCSourcedOrder);
    return isDCSourcedOrder;
  }

  /**
   *
   * Description of stampApoFpo Check If(PersonInfoShipTo/@State =('AE'|| 'AP' || 'AA')){ Stamp
   * ExntIsAPOFPO = �Y� and returns true if it is APOFPO address
   *
   * @param elePersonInfoShipTo
   * @param inDoc
   * @throws GCException
   *
   */
  private boolean stampApoFpo(Element elePersonInfoShipTo, Document inDoc) throws GCException {
    LOGGER.debug(" Entering GCBeforeCreateOrderUE.stampApoFpo() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" stampApoFpo() method, Input document is" + GCXMLUtil.getXMLString(inDoc));
      LOGGER.debug(" stampApoFpo() method, Input document is" + GCXMLUtil.getElementXMLString(elePersonInfoShipTo));
    }
    boolean isAPOFPAddress = false;
    try {
      // GCSTORE-1916::begin
      Element eleExtn = (Element) XPathAPI.selectSingleNode(elePersonInfoShipTo, GCConstants.EXTN);
      if (!YFCCommon.isVoid(eleExtn) && YFCCommon.equals(eleExtn.getAttribute("ExtnIsAPOFPO"), "Y")) {
        isAPOFPAddress = true;
      } else {
        // //GCSTORE-1916::end

        String sState = elePersonInfoShipTo.getAttribute(GCConstants.STATE);
        if (GCConstants.AP.equalsIgnoreCase(sState) || GCConstants.AE.equalsIgnoreCase(sState)
            || GCConstants.AA.equalsIgnoreCase(sState)) {
          LOGGER.debug(" if State =('AE'|| 'AP' || 'AA')");

          if (YFCCommon.isVoid(eleExtn)) {
            LOGGER.debug(" Inside if(YFCCommon.isVoid(eleExtn))");
            eleExtn = GCXMLUtil.createElement(inDoc, GCConstants.EXTN, null);
            elePersonInfoShipTo.appendChild(eleExtn);
          }
          eleExtn.setAttribute(GCConstants.EXTN_IS_APO_FPO, GCConstants.YES);
          isAPOFPAddress = true;
        }
      }
    } catch (Exception e) {
      LOGGER.error(" Inside catch of GCBeforeCreateOrderUE.stampApoFpo()", e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCBeforeCreateOrderUE.stampApoFpo() method ");
    return isAPOFPAddress;
  }

  /**
   *
   * Description of stampPOBox Check If (PersonInfoShipTo/@ AddressLine1 || PersonInfoShipTo/@
   * AddressLine2 || PersonInfoShipTo/@ AddressLine3 like '%P%O%%BOX%') Stamp ExtnIsPOBox=�Y�
   *
   * @param elePersonInfoShipTo
   * @param inDoc
   * @throws GCException
   *
   */
  private boolean stampPOBox(Element elePersonInfoShipTo, Document inDoc) throws GCException {
    LOGGER.debug(" Entering GCBeforeCreateOrderUE.stampPOBox() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" stampPOBox() method, Input document is" + GCXMLUtil.getXMLString(inDoc));
      LOGGER.debug(" stampPOBox() method, Input document is" + GCXMLUtil.getElementXMLString(elePersonInfoShipTo));
    }
    boolean isPOAddress = false;
    try {
      // GCSTORE-1916::begin
      Element eleExtn = (Element) XPathAPI.selectSingleNode(elePersonInfoShipTo, GCConstants.EXTN);
      if (!YFCCommon.isVoid(eleExtn) && YFCCommon.equals(eleExtn.getAttribute("ExtnIsPOBox"), "Y")) {
        isPOAddress = true;
      } else {
        // //GCSTORE-1916::end
        String sAddressLine1 = elePersonInfoShipTo.getAttribute(GCConstants.ADDRESS_LINE1);
        String sAddressLine2 = elePersonInfoShipTo.getAttribute(GCConstants.ADDRESS_LINE2);
        String sAddressLine3 = elePersonInfoShipTo.getAttribute(GCConstants.ADDRESS_LINE3);
        String sAddress = sAddressLine1 + sAddressLine2 + sAddressLine3;
        String smAddress=sAddress.toUpperCase();
        // Check if the pattern %P%O%%BOX% exists in the addressLine1,
        // addressLine2 and AddressLine3
        if (smAddress.matches(".*P.*O.*BOX.*")) {
          LOGGER.debug(" if Address = %P%O%%BOX% ");

          if (YFCCommon.isVoid(eleExtn)) {
            LOGGER.debug(" Inside if(YFCCommon.isVoid(eleExtn))");
            eleExtn = GCXMLUtil.createElement(inDoc, GCConstants.EXTN, null);
            elePersonInfoShipTo.appendChild(eleExtn);
          }
          eleExtn.setAttribute(GCConstants.EXTN_IS_PO_BOX, GCConstants.YES);
          isPOAddress = true;
        }
      }
    } catch (Exception e) {
      LOGGER.error(" Inside catch of GCBeforeCreateOrderUE.stampPOBox()", e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCBeforeCreateOrderUE.stampPOBox() method ");
    return isPOAddress;
  }

  /**
   *
   * Description of checkForTrackedAsSet Fetch ExtnSetCode for the ItemID by getItemList API
   *
   * @param env
   * @param eleItem
   * @param sOrganizationCode
   * @return
   * @throws GCException
   *
   */
  public static String checkForTrackedAsSet(YFSEnvironment env, Element eleItem, String sOrganizationCode)
      throws GCException {
    LOGGER.debug(" Entering GCBeforeCreateOrderUE.checkForTrackedAsSet() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" validateUnderpricedHold() method, Input document is" + GCXMLUtil.getElementXMLString(eleItem));
    }
    try {
      String sExtnSetCode = null;
      String sItemID = eleItem.getAttribute(GCConstants.ITEM_ID);
      String sUOM = eleItem.getAttribute(GCConstants.UOM);
      LOGGER.debug(" sItemID is " + sItemID + " sUOM is " + sUOM);

      Document docGetItemListIP = GCXMLUtil.createDocument(GCConstants.ITEM);
      docGetItemListIP.getDocumentElement().setAttribute(GCConstants.ITEM_ID, sItemID);
      docGetItemListIP.getDocumentElement().setAttribute(GCConstants.UOM, sUOM);
      docGetItemListIP.getDocumentElement().setAttribute(GCConstants.ORGANIZATION_CODE, GCConstants.GCI);

      Document docGetItemListTemplate = GCXMLUtil.getDocument(GCConstants.GET_ITEM_LIST_TEMPLATE);

      Document docGetItemListOP =
          GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ITEM_LIST, docGetItemListIP, docGetItemListTemplate);
      sExtnSetCode = GCXMLUtil.getAttributeFromXPath(docGetItemListOP, "ItemList/Item/Extn/@ExtnSetCode");
      LOGGER.debug(" sExtnSetCode is " + sExtnSetCode);
      LOGGER.debug(" Exiting GCBeforeCreateOrderUE.checkForTrackedAsSet() method ");
      return sExtnSetCode;
    } catch (Exception e) {
      LOGGER.error(" Inside catch of GCBeforeCreateOrderUE.checkForTrackedAsSet()", e);
      throw new GCException(e);
    }
  }

  /**
   *
   * Description of stampUserName getUserId from each GCOrderLineCommission, get corresponding
   * username and stamp on GCOrderLineCommission.
   *
   * @param env
   * @param inDoc
   * @throws GCException
   *
   */

  public void stampUserName(YFSEnvironment env, Document inDoc) throws GCException {

    LOGGER.debug(" Entering GCBeforeCreateOrderUE.stampUserName() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" inDoc is" + GCXMLUtil.getXMLString(inDoc));
    }
    try {
      List<Element> listGCOrderLineCommissionList =
          GCXMLUtil.getElementListByXpath(inDoc,
              "Order/OrderLines/OrderLine/Extn/GCOrderLineCommissionList/GCOrderLineCommission");
      for (Element eleGCOrderLineCommission : listGCOrderLineCommissionList) {
        String sUserId = eleGCOrderLineCommission.getAttribute(GCConstants.USER_ID);

        if (!YFCCommon.isVoid(sUserId)) {

          Document getUserListOutDoc =
              GCCommonUtil.invokeAPI(env, GCConstants.GET_USER_LIST,
                  GCXMLUtil.getDocument("<User Loginid='" + sUserId + "'/>"),
                  GCXMLUtil.getDocument("<UserList><User Username=''/></UserList>"));

          if (getUserListOutDoc.hasChildNodes()) {
            String sUserName = GCXMLUtil.getAttributeFromXPath(getUserListOutDoc, "/UserList/User/@Username");
            eleGCOrderLineCommission.setAttribute(GCConstants.ORDER_USER_NAME, sUserName);
          }
        }
      }
    } catch (Exception e) {
      LOGGER.error(" Inside catch block of GCBeforeCreateOrderUE.stampUserName(), Exception is :", e);
      throw new GCException(e);
    }
  }

  /**
   *
   * This method calculates the Authorization Expiration date for the different payment types
   *
   * @param env
   * @param inDoc
   * @return
   * @throws GCException
   *
   */
  public static Document getAuthExpiryDate(YFSEnvironment env, Document inDoc) throws GCException {
    try {
      LOGGER.debug("Entering the getAuthExpiryDate method");
      String strOrderDate = inDoc.getDocumentElement().getAttribute(GCConstants.ORDER_DATE);
      if (YFCCommon.isVoid(strOrderDate)) {
        strOrderDate = new SimpleDateFormat(GCConstants.DATE_TIME_FORMAT).format(currentDate);
      }
      Document inDocGetCommonCode = GCXMLUtil.getDocument("<CommonCode CodeType='GCAuthExpiryDays'/>");
      Document outDocGetCommonCode = GCCommonUtil.invokeAPI(env, GCConstants.GET_COMMON_CODE_LIST, inDocGetCommonCode);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("The output of getComonCodeList" + GCXMLUtil.getXMLString(outDocGetCommonCode));
      }
      NodeList nlPaymentMethod = GCXMLUtil.getNodeListByXpath(inDoc, "Order/PaymentMethods/PaymentMethod");
      for (int i = 0; i < nlPaymentMethod.getLength(); i++) {
        Element elePaymentMethod = (Element) nlPaymentMethod.item(i);
        LOGGER.debug("The payment method parsed is" + GCXMLUtil.getElementString(elePaymentMethod));
        String strPaymentType = elePaymentMethod.getAttribute(GCConstants.PAYMENT_TYPE);
        String strCardType = elePaymentMethod.getAttribute(GCConstants.CREDIT_CARD_TYPE);
        LOGGER.debug("The payment type is " + strPaymentType + "The Card Type is " + strCardType);
        Element eleAuthExp =
            GCXMLUtil.getElementByXPath(outDocGetCommonCode, "//CommonCodeList/CommonCode[@CodeName='" + strPaymentType
                + "' and @CodeLongDescription='" + strCardType + "']");
        Element elePaymentDetails = (Element) XPathAPI.selectSingleNode(elePaymentMethod, "PaymentDetails");
        String strProcessedAmount = "";
        if (!YFCCommon.isVoid(elePaymentDetails)) {
          strProcessedAmount = elePaymentDetails.getAttribute(GCConstants.PAYMENT_PROCESSED_AMOUNT);
        }
        String strAuthExpDate = "";
        if (!YFCCommon.isVoid(eleAuthExp) && !YFCCommon.isVoid(strProcessedAmount)) {

          String strAuthValidationDate = eleAuthExp.getAttribute(GCConstants.CODE_SHORT_DESCRIPTION);
          LOGGER.debug("The authorization date to add " + strAuthValidationDate);
          SimpleDateFormat formatter = new SimpleDateFormat(GCConstants.DATE_TIME_FORMAT);
          Date dtOrderDate = formatter.parse(strOrderDate);
          Calendar calOrderDate = Calendar.getInstance();
          calOrderDate.setTime(dtOrderDate);
          calOrderDate.add(Calendar.DATE, Integer.parseInt(strAuthValidationDate));
          SimpleDateFormat dateFormat = new SimpleDateFormat(GCConstants.DATE_TIME_FORMAT);
          strAuthExpDate = dateFormat.format(calOrderDate.getTime());
          LOGGER.debug("The atrAuthExpDate is --->>>>" + strAuthExpDate);
          elePaymentDetails.setAttribute(GCConstants.AUTH_EXPIRY_DATE, strAuthExpDate);
          LOGGER.debug("Exiting the getAuthExpiryDate method");
        }
      }
      return inDoc;
    } catch (Exception e) {
      LOGGER.error("Inside GCBeforeCreateOrderUE.getAuthExpiryDate() :", e);
      throw new GCException(e);
    }
  }

  /**
   *
   * Description of stampEntryType Fetch the Entry Type from GC_SOURCE_CODE table on the basis of
   * Source Code and Order Date and set it in the inDoc
   *
   * @param env
   * @param inDoc
   * @throws GCException
   *
   */
  private void stampEntryType(YFSEnvironment env, Document inDoc) throws GCException {
    LOGGER.debug(" Entering GCBeforeCreateOrderUE.stampEntryType() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" stampEntryType() method, Input document is" + GCXMLUtil.getXMLString(inDoc));
    }
    try {
      String sSourceCode = GCXMLUtil.getAttributeFromXPath(inDoc, "Order/Extn/@ExtnSourceCode");
      String sOrderDate = inDoc.getDocumentElement().getAttribute(GCConstants.ORDER_DATE);
      if (YFCCommon.isVoid(sOrderDate)) {
        Calendar calRemorseDate = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat(GCConstants.DATE_TIME_FORMAT);
        sOrderDate = dateFormat.format(calRemorseDate.getTime());
        LOGGER.debug(" sOrderDate " + sOrderDate);
      }
      Date strOrderDate = new SimpleDateFormat(GCConstants.DATE_FORMAT).parse(sOrderDate);
      Calendar calOrderDate = Calendar.getInstance();
      calOrderDate.setTime(strOrderDate);
      LOGGER.debug(" sSourceCode " + sSourceCode + " strOrderDate " + strOrderDate);

      Document docGCSourceCodeIP = GCXMLUtil.createDocument(GCConstants.GC_SOURCE_CODE);
      docGCSourceCodeIP.getDocumentElement().setAttribute(GCConstants.SOURCE_CODE, sSourceCode);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(" stampEntryType() method, docGetSourceCodeOP is" + GCXMLUtil.getXMLString(docGCSourceCodeIP));
      }

      Document docGetSourceCodeOP =
          GCCommonUtil.invokeService(env, GCConstants.GC_GET_SOURCE_CODE_LIST_SERVICE, docGCSourceCodeIP);
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(" stampEntryType() method, docGetSourceCodeOP is" + GCXMLUtil.getXMLString(docGetSourceCodeOP));
      }

      NodeList nlGCSourceCode = XPathAPI.selectNodeList(docGetSourceCodeOP, "GCSourceCodeList/GCSourceCode");
      for (int iCounter = 0; iCounter < nlGCSourceCode.getLength(); iCounter++) {
        LOGGER.debug(" Looping on the source code list ");

        Element eleSourceCode = (Element) nlGCSourceCode.item(iCounter);
        String sEndDate = eleSourceCode.getAttribute(GCConstants.END_DATE);
        Date strEndDate = new SimpleDateFormat(GCConstants.DATE_FORMAT).parse(sEndDate);
        Calendar calEndDate = Calendar.getInstance();
        calEndDate.setTime(strEndDate);
        LOGGER.debug(" strEndDate " + strEndDate);

        String sStartDate = eleSourceCode.getAttribute(GCConstants.START_DATE);
        Date strStartDate = new SimpleDateFormat(GCConstants.DATE_FORMAT).parse(sStartDate);
        Calendar calStartDate = Calendar.getInstance();
        calStartDate.setTime(strStartDate);
        LOGGER.debug(" strStartDate " + strStartDate);

        // OMS-5463- Start
        if (calOrderDate.getTime().compareTo(calStartDate.getTime()) >= 0
            && calEndDate.getTime().compareTo(calOrderDate.getTime()) >= 0) {
          // OMS-5463-end
          LOGGER.debug(" Inside if date is in between the interval ");
          String sEntryType = eleSourceCode.getAttribute(GCConstants.ENTRY_TYPE);
          inDoc.getDocumentElement().setAttribute(GCConstants.ENTRY_TYPE, sEntryType);
          LOGGER.debug(" sEntryType " + sEntryType);
        }
      }
    } catch (Exception e) {
      LOGGER.error(" Inside catch block of GCBeforeCreateOrderUE.stampEntryType(), Exception is :", e);
      throw new GCException(e);
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" stampEntryType() method, updated input document is" + GCXMLUtil.getXMLString(inDoc));
    }
    LOGGER.debug(" Exiting GCBeforeCreateOrderUE.stampEntryType() method ");
  }

  /**
   *
   * Description of prepareGetOrderPriceIP Prepare Input for GetOrderPrice XML
   *
   * @param eleRootOfInDoc
   * @return
   * @throws ParserConfigurationException
   * @throws Exception
   *
   */
  private Document prepareGetOrderPriceIP(Element eleRootOfInDoc) throws GCException, ParserConfigurationException {
    LOGGER.debug(" Entering GCBeforeCreateOrderUE.prepareGetOrderPriceIP() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" prepareGetOrderPriceIP() method, eleRootOfInDoc is"
          + GCXMLUtil.getElementXMLString(eleRootOfInDoc));
    }
    Document docGetOrderPrice = GCXMLUtil.createDocument(GCConstants.ORDER);
    try {
      String sCurrency = GCXMLUtil.getAttributeFromXPath(eleRootOfInDoc, "PriceInfo/@Currency");
      Element eleRootGetOrderPrice = docGetOrderPrice.getDocumentElement();
      eleRootGetOrderPrice.setAttribute(GCConstants.ORGANIZATION_CODE,
          eleRootOfInDoc.getAttribute(GCConstants.ENTERPRISE_CODE));
      eleRootGetOrderPrice.setAttribute(GCConstants.CURRENCY, sCurrency);

      Element eleOrderPriceOLs = GCXMLUtil.createElement(docGetOrderPrice, GCConstants.ORDER_LINES, null);
      eleRootGetOrderPrice.appendChild(eleOrderPriceOLs);

      NodeList nlOrderLine = XPathAPI.selectNodeList(eleRootOfInDoc, "OrderLines/OrderLine");
      for (int iLineCount = 0; iLineCount < nlOrderLine.getLength(); iLineCount++) {
        Element eleOrderLine = (Element) nlOrderLine.item(iLineCount);
        String sOrderedQty = eleOrderLine.getAttribute(GCConstants.ORDERED_QTY);
        Element eleItem = (Element) XPathAPI.selectSingleNode(eleOrderLine, GCConstants.ITEM);
        String sItemID = eleItem.getAttribute(GCConstants.ITEM_ID);

        String sLineID = sItemID + "_" + sOrderedQty;
        LOGGER.debug(" LineID is " + sLineID);
        Element eleOrderPriceOL = GCXMLUtil.createElement(docGetOrderPrice, GCConstants.ORDER_LINE, null);
        eleOrderPriceOL.setAttribute(GCConstants.QUANTITY, sOrderedQty);
        eleOrderPriceOL.setAttribute(GCConstants.ITEM_ID, sItemID);
        eleOrderPriceOL.setAttribute(GCConstants.UOM, eleItem.getAttribute(GCConstants.UOM));
        eleOrderPriceOL.setAttribute(GCConstants.PRODUCT_CLASS, eleItem.getAttribute(GCConstants.PRODUCT_CLASS));
        eleOrderPriceOL.setAttribute(GCConstants.LINE_ID, sLineID);
        eleOrderPriceOLs.appendChild(eleOrderPriceOL);
      }
    } catch (Exception e) {
      LOGGER.error(" Inside catch block of GCBeforeCreateOrderUE.prepareGetOrderPriceIP(), Exception is :", e);
      throw new GCException(e);
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" prepareGetOrderPriceIP() method, docGetOrderPrice document is"
          + GCXMLUtil.getXMLString(docGetOrderPrice));
    }
    LOGGER.debug(" Exiting GCBeforeCreateOrderUE.prepareGetOrderPriceIP() method ");
    return docGetOrderPrice;
  }

  /**
   *
   * Description of updateInDocUnitPrice Update UnitPrice in inDoc
   *
   * @param inDoc
   * @param docGetOrderPriceOP
   * @throws Exception
   *
   */
  private void updateInDocUnitPrice(Document inDoc, Document docGetOrderPriceOP) throws GCException {
    LOGGER.debug(" Entering GCBeforeCreateOrderUE.updateInDocUnitPrice() method ");
    try {
      NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc, "Order/OrderLines/OrderLine");
      for (int iLineCount = 0; iLineCount < nlOrderLine.getLength(); iLineCount++) {
        Element eleOrderLine = (Element) nlOrderLine.item(iLineCount);
        String sOrderedQty = eleOrderLine.getAttribute(GCConstants.ORDERED_QTY);
        String sItemID = GCXMLUtil.getAttributeFromXPath(eleOrderLine, "Item/@ItemID");
        String sLineID = sItemID + "_" + sOrderedQty;
        eleOrderLine.setAttribute(GCConstants.LINE_ID, sLineID);

        String sUnitPrice =
            GCXMLUtil
            .getAttributeFromXPath(docGetOrderPriceOP, "Order/OrderLines/OrderLine[@LineID='" + sLineID + "']");
        if (!YFCCommon.isVoid(sUnitPrice)) {
          Element eleLinePriceInfo = GCXMLUtil.createElement(inDoc, GCConstants.LINE_PRICE_INFO, null);
          eleLinePriceInfo.setAttribute(GCConstants.UNIT_PRICE, sUnitPrice);
          eleLinePriceInfo.setAttribute(GCConstants.IS_PRICE_LOCKED, GCConstants.YES);
          eleOrderLine.appendChild(eleLinePriceInfo);
        }
      }
    } catch (Exception e) {
      LOGGER.error(" Inside catch block of GCBeforeCreateOrderUE.updateInDocUnitPrice(), Exception is :", e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCBeforeCreateOrderUE.updateInDocUnitPrice() method ");
  }

  private void setSentForFraudCheck(Document inDoc) throws GCException {
    LOGGER.debug(" Entering GCBeforeCreateOrderUE.setSentForFraudFlag() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" inDoc is" + GCXMLUtil.getXMLString(inDoc));
    }
    try {
      String sExtnSentForFraudCheck = GCXMLUtil.getAttributeFromXPath(inDoc, "Order/Extn/@ExtnSentForFraudCheck");
      if (YFCCommon.isVoid(sExtnSentForFraudCheck)) {
        Element eleExtn = GCXMLUtil.getElementByXPath(inDoc, "Order/Extn");
        eleExtn.setAttribute("ExtnSentForFraudCheck", GCConstants.NO);
      }
    } catch (Exception e) {
      LOGGER.error(" Inside catch block of GCBeforeCreateOrderUE.setSentForFraudFlag(), Exception is :", e);
      throw new GCException(e);
    }

  }

  @Override
  public String beforeCreateOrder(YFSEnvironment env, String arg1) {
    return null;
  }

  /**
   *
   * Description of modifyInDoc this method modifies the incoming document in the method. Here it
   * finds the WarrantyOrderLine and adds Extn/ExtnWarrantyParentLineNumber attribute Also It finds
   * the corresponding parent orderline and adds Extn/ExtnWarrantyLineNumber
   *
   * @param inDoc
   * @return
   * @throws GCException
   *
   */
  private Document modifyInDoc(Document inDoc) throws GCException {
    LOGGER.debug("Entering from GCBeforeCreateOrderUE.modifyInDoc()");
    try {
      NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc, "Order/OrderLines/OrderLine");
      int count = nlOrderLine.getLength();
      for (int i = 0; i < count; i++) {
        Element eleOrderLine = (Element) nlOrderLine.item(i);
        Element eleOrderLineExtn = (Element) XPathAPI.selectSingleNode(eleOrderLine, GCConstants.EXTN);
        if (!YFCCommon.isVoid(eleOrderLineExtn)) {
          String sExtnIsWarrantyItem = eleOrderLineExtn.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
          if ("Y".equals(sExtnIsWarrantyItem)) {
            String sPrimeLineNo = eleOrderLine.getAttribute(GCConstants.PRIME_LINE_NO);
            Element eleOrderLineDependency = (Element) XPathAPI.selectSingleNode(eleOrderLine, GCConstants.DEPENDENCY);
            // Added null check for GCSTORE-3013 - Start
            if (YFCCommon.isVoid(eleOrderLineDependency)) {
              continue;
            }
            // GCSTORE-3013 - End
            String sDependentOnPrimeLineNo = eleOrderLineDependency.getAttribute(GCConstants.DEPENDENT_PRIME_LINE_NO);
            eleOrderLineExtn.setAttribute(GCConstants.EXTN_WARRANTY_PARENT_LINE_NUMBER, sDependentOnPrimeLineNo);
            Element eleParentOrderLine =
                (Element) XPathAPI.selectSingleNode(inDoc, "Order/OrderLines/OrderLine[@PrimeLineNo='"
                    + sDependentOnPrimeLineNo + "']");
            Element eleParentOrderLineExtn = (Element) XPathAPI.selectSingleNode(eleParentOrderLine, GCConstants.EXTN);
            if (YFCCommon.isVoid(eleParentOrderLineExtn)) {
              eleParentOrderLineExtn = GCXMLUtil.createElement(inDoc, GCConstants.EXTN, null);
              eleParentOrderLine.appendChild(eleParentOrderLineExtn);
            }
            eleParentOrderLineExtn.setAttribute(GCConstants.EXTN_WARRANTY_LINE_NUMBER, sPrimeLineNo);
          }
        }
      }
      LOGGER.debug("Exiting from GCBeforeCreateOrderUE.modifyInDoc()");

    } catch (Exception e) {
      LOGGER.error(" Inside catch block of GCBeforeCreateOrderUE.modifyInDoc(), Exception is :", e);
      throw new GCException(e);
    }

    return inDoc;
  }

  /**
   *
   * This method modifies the incoming document in the method. Here it finds the WarrantyOrderLine
   * and applies hold on the warranty orderLine At the same time if order is PIS order then stamp
   * pick up store as shipnode with firm predefined for the orderlines (split has already happened)
   * which are getting fulfilled from pick up store)
   *
   * @param inDoc
   * @return
   * @throws GCException
   *
   */
  private void applyHoldOnDependentLineAndStampShipNodeForPIS(Document inDoc) {
    LOGGER.debug("Entering GCBeforeCreateOrderUE.applyHoldOnWarrantyLine()");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" inDoc is" + GCXMLUtil.getXMLString(inDoc));
    }
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleOrder = inputDoc.getDocumentElement();
    String sDependentOnPrimeLineNo = null;
    String sDependentOnSubLineNo = null;
    String sDependentOnTransactionalLineId = null;
    String sExtnPickupStoreID = "";
    YFCElement eleExtn = eleOrder.getChildElement(GCConstants.EXTN);
    if (!YFCCommon.isVoid(eleExtn)) {
      sExtnPickupStoreID = eleExtn.getAttribute(GCConstants.EXTN_PICKUP_STORE_ID);
      LOGGER.verbose("ExtnPickupStoreID is" + sExtnPickupStoreID);
    }
    YFCElement eleOrderLines = eleOrder.getChildElement(GCConstants.ORDER_LINES);
    if (!YFCCommon.isVoid(eleOrderLines)) {
      YFCNodeList<YFCElement> nlOrderLine = eleOrderLines.getElementsByTagName(GCConstants.ORDER_LINE);
      // Look for dependent line and apply hold on that.
      for (YFCElement eleOrderLine : nlOrderLine) {
        YFCElement eleOrderLineExtn = eleOrderLine.getChildElement(GCConstants.EXTN);
        if (!YFCCommon.isVoid(eleOrderLineExtn)) {
          String sExtnIsWarrantyItem = eleOrderLineExtn.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
          String sExtnIsFreeGiftItem = eleOrderLineExtn.getAttribute(GCConstants.EXTN_IS_FREE_GIFT_ITEM);
          if ("Y".equals(sExtnIsWarrantyItem) || "Y".equals(sExtnIsFreeGiftItem)) {
            YFCElement eleOrderLineDependency = eleOrderLine.getChildElement(GCConstants.DEPENDENCY);
            if (!YFCCommon.isVoid(eleOrderLineDependency)) {
              sDependentOnPrimeLineNo = eleOrderLineDependency.getAttribute(GCConstants.DEPENDENT_PRIME_LINE_NO);
              sDependentOnSubLineNo = eleOrderLineDependency.getAttribute(GCConstants.DEPENDENT_SUB_LINE_NO);
              sDependentOnTransactionalLineId =
                  eleOrderLineDependency.getAttribute(GCXmlLiterals.DEPENDENT_ON_TRAN_LINE_ID);
            }
            if ((!YFCCommon.isStringVoid(sDependentOnPrimeLineNo) && !YFCCommon.isStringVoid(sDependentOnSubLineNo))
                || (!YFCCommon.isVoid(sDependentOnTransactionalLineId))) {
              YFCElement eleOrderHoldTypes = eleOrderLine.createChild(GCConstants.ORDER_HOLD_TYPES);
              YFCElement eleOrderHoldType = eleOrderHoldTypes.createChild(GCConstants.ORDER_HOLD_TYPE);
              eleOrderHoldType.setAttribute(GCConstants.HOLD_TYPE, "DEPENDENT_LINE_HOLD");
              eleOrderHoldType.setAttribute(GCConstants.STATUS, "1100");
            }
          }
        }
        // Look for PIS fulfillment type with fulfillment from Pick up store and stamp the pick up
        // store as firm predefined node. orderline is already split in this scenario.
        String sFulfillmentType = eleOrderLine.getAttribute(GCConstants.FULFILLMENT_TYPE);
        String sShipNode = eleOrderLine.getAttribute(GCConstants.SHIP_NODE);
        if (!YFCCommon.isVoid(sFulfillmentType) && sFulfillmentType.indexOf(GCConstants.PICKUP_IN_STORE) >= 0
            && YFCCommon.isVoid(sShipNode)) {
          YFCElement eleOrderLineReservations = eleOrderLine.getChildElement(GCConstants.ORDER_LINE_RESERVATIONS);
          if (!YFCCommon.isVoid(eleOrderLineReservations)) {
            YFCNodeList<YFCElement> nlOrderLineReservation =
                eleOrderLineReservations.getElementsByTagName(GCConstants.ORDER_LINE_RESERVATION);
            for (YFCElement eleOrderLineReservation : nlOrderLineReservation) {
              String sNode = eleOrderLineReservation.getAttribute(GCConstants.NODE);
              if (YFCCommon.equalsIgnoreCase(sExtnPickupStoreID, sNode)) {
                eleOrderLine.setAttribute(GCConstants.ORDER_LINE_KEY,
                    eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
                eleOrderLine.setAttribute(GCConstants.SHIP_NODE, sNode);
                eleOrderLine.setAttribute(GCConstants.IS_FIRM_PREDEFINED_NODE, GCConstants.YES);
                break;
              }
            }
          }
        }
      }
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" inDoc is" + GCXMLUtil.getXMLString(inDoc));
    }
    LOGGER.debug("Exiting from GCBeforeCreateOrderUE.applyHoldOnWarrantyLine()");
  }

  // GCSTORE-2514 : Start
  /**
   * This method will check if the tax is calculated successfully, if not then apply an hold on the
   * order If yes, then check if tax has increased and payment type is Paypal or BML, raise an alert
   * for this
   *
   * @param env
   * @param inDoc
   * @param initaialTotalTax
   */
  private void processUpdatedDocWithTaxes(YFSEnvironment env, Document inDoc, double initaialTotalTax) {
    LOGGER.beginTimer("GCBeforeCreateOrderUE.processUpdatedDocWithTaxes");
    LOGGER.verbose("Class: GCBeforeCreateOrderUE Method: processUpdatedDocWithTaxes START");
    Element eOrder = inDoc.getDocumentElement();
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" processUpdatedDocWithTaxes() method, inputDoc is" + inputDoc.toString());
    }
    YFCElement eleOrder = inputDoc.getDocumentElement();
    YFCElement eleLineTax = eleOrder.getElementsByTagName(GCConstants.LINE_TAX).item(0);
    String sReference3 = eleLineTax.getAttribute(GCConstants.REFERENCE3);
    LOGGER.verbose("sReference3 is" + sReference3);
    YFCNodeList<YFCElement> nlHeaderTax = eleOrder.getElementsByTagName(GCConstants.HEADER_TAX);
    YFCElement eleTotalTax = null;
    for (YFCElement eleHeaderTax : nlHeaderTax) {
      LOGGER.verbose(" Looping on header tax elements ");
      String sChargeCategory = eleHeaderTax.getAttribute(GCConstants.CHARGE_CATEGORY);
      if (GCXmlLiterals.TOTAL_TAX.equalsIgnoreCase(sChargeCategory)) {
        LOGGER.verbose(" Inside if charge category is TotalTax ");
        eleTotalTax = eleHeaderTax;
        break;
      }
    }
    if (YFCCommon.equals(GCConstants.ERROR, sReference3)) {
      LOGGER.verbose(" Inside if Reference3 is Error ");
      if (!YFCCommon.isVoid(eleTotalTax)) {
        applyHoldforATGOrder(inDoc);
      }
    } else {
      double dTotalTax = eleTotalTax.getDoubleAttribute(GCConstants.TAX);
      GCValidateAndResolveHoldsWithPeriod obj = new GCValidateAndResolveHoldsWithPeriod();
      double dCalculatedTax = obj.calculateOrderTotalTax(eleOrder);
      LOGGER.verbose(" dTotalTax is " + dTotalTax + " dCalculatedTax is " + dCalculatedTax + "initaialTotalTax is "
          + initaialTotalTax);
      obj.removeHeaderTaxElement(inDoc);
      if (dCalculatedTax > initaialTotalTax) {
        LOGGER.verbose(" Inside if dCalculatedTax is greater than dTotalTax ");
        YFCElement elePaymentMethod = eleOrder.getElementsByTagName(GCConstants.PAYMENT_METHOD).item(0);
        String sPaymentType = elePaymentMethod.getAttribute(GCConstants.PAYMENT_TYPE);
        if (YFCCommon.equals(GCConstants.BML, sPaymentType) || YFCCommon.equals(GCConstants.PAYPAL, sPaymentType)) {
          LOGGER.verbose(" If PaymentType is PAYPAL or BML, then raise an alert ");
          double dIncreasedTax = dCalculatedTax - initaialTotalTax;
          LOGGER.verbose(" dIncreasedTax is " + dIncreasedTax);
          eOrder.setAttribute("IncreasedTax", String.valueOf(dIncreasedTax));
          GCCommonUtil.invokeService(env, GCConstants.GC_RAISE_ALERT_FOR_NEW_AUTH_SERVICE, inDoc);
          eOrder.removeAttribute("IncreasedTax");
        } else {
          LOGGER.verbose("Inside else, increase request amount");
          double dMaxChargeLimit = elePaymentMethod.getDoubleAttribute(GCConstants.MAX_CHARGE_LIMIT);
          dMaxChargeLimit = dMaxChargeLimit + (dCalculatedTax - initaialTotalTax);
          elePaymentMethod.setAttribute(GCConstants.MAX_CHARGE_LIMIT, dMaxChargeLimit);
          YFCElement elePaymentDetails = elePaymentMethod.getFirstChildElement();
          elePaymentDetails.setAttribute(GCConstants.REQUEST_AMOUNT, dMaxChargeLimit);
        }
      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" processUpdatedDocWithTaxes() method, updated inputDoc is" + inputDoc.toString());
    }
    LOGGER.verbose("Class: GCBeforeCreateOrderUE Method: processUpdatedDocWithTaxes END");
    LOGGER.endTimer("GCBeforeCreateOrderUE.processUpdatedDocWithTaxes");
  }

  // GCSTORE-2514 : End

  // GCSTORE-2514 : Start
  /**
   * Apply VERTEX CALL FAILED Hold on the order
   *
   * @param inDoc
   * @return
   */
  private Document applyHoldforATGOrder(Document inDoc) {
    LOGGER.beginTimer("GCBeforeCreateOrderUE.applyHoldforATGOrder");
    LOGGER.verbose("Class: GCBeforeCreateOrderUE Method: applyHoldforATGOrder START");
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleOrder = inputDoc.getDocumentElement();
    YFCElement eleOrderHoldTypes = eleOrder.createChild(GCConstants.ORDER_HOLD_TYPES);
    eleOrder.appendChild(eleOrderHoldTypes);
    YFCElement eleOrderHoldType = eleOrder.createChild(GCConstants.ORDER_HOLD_TYPE);
    eleOrderHoldType.setAttribute(GCConstants.HOLD_TYPE, GCXmlLiterals.VERTEX_CALL_FAILED);
    eleOrderHoldType.setAttribute(GCConstants.STATUS, GCConstants.APPLY_HOLD_CODE);
    eleOrderHoldTypes.appendChild(eleOrderHoldType);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" applyHoldforATGOrder() method, updated inputDoc is" + inputDoc.toString());
    }
    LOGGER.verbose("Class: GCBeforeCreateOrderUE Method: applyHoldforATGOrder END");
    LOGGER.endTimer("GCBeforeCreateOrderUE.applyHoldforATGOrder");
    return inDoc;
  }

  // GCSTORE-2514 : End

  private Document setOrderPhase(YFSEnvironment env, Document inDoc) {
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleOrder = inputDoc.getDocumentElement();
    YFCElement eleOrdExtn = eleOrder.getChildElement(GCConstants.EXTN);
    if (!YFCCommon.isVoid(eleOrdExtn)) {
      String orderCaptureChannel = eleOrdExtn.getAttribute(GCConstants.EXTN_ORDER_CAPTURE_CHANNEL);
      String sourceSystem = eleOrdExtn.getAttribute(GCXmlLiterals.EXTN_SOURCE_SYSTEM);
      if (YFCCommon.equals(orderCaptureChannel, GCConstants.CALL_CENTER_ORDER_CHANNEL)
          || YFCCommon.equals(orderCaptureChannel, GCConstants.GCSTORE)) {
        sourceSystem = GCConstants.OMS;
      }

      if (YFCUtils.equals("JBS", sourceSystem)) {
        eleOrdExtn.setAttribute(GCConstants.EXTN_ORDER_PHASE, GCConstants.PHASE_ONE);
      } else if (YFCUtils.equals(GCConstants.ATG, sourceSystem) || YFCUtils.equals(GCConstants.OMS, sourceSystem)) {
        String codeType = GCConstants.SOURCE_SYSTEM + GCConstants.HYPHEN + sourceSystem;
        YFCDocument commonCodeOp =
            GCCommonUtil.getCommonCodeListByTypeAndValue(env, codeType, GCConstants.GC, orderCaptureChannel);
        YFCElement eleCommonCode = commonCodeOp.getElementsByTagName(GCConstants.COMMON_CODE).item(0);
        String codeLongDesc = eleCommonCode.getAttribute(GCConstants.CODE_LONG_DESCRIPTION);
        eleOrdExtn.setAttribute(GCConstants.EXTN_ORDER_PHASE, codeLongDesc);
      }
    }
    return inDoc;

  }

 /* private void appendQuoteNotes(Document inDoc, String sExtnSourceOrderNo) {

      Element eleOrder = inDoc.getDocumentElement();
      Element eleNotes = (Element) eleOrder.getElementsByTagName(GCConstants.NOTES).item(0);
      if(YFCCommon.isVoid(eleNotes)){
          eleNotes = inDoc.createElement(GCConstants.NOTES);
          eleOrder.appendChild(eleNotes);
      }
      Element eleNote = inDoc.createElement(GCConstants.NOTE);
      eleNotes.appendChild(eleNote);
      eleNote.setAttribute(GCConstants.NOTE_TEXT, "This is a conversion of quote# "+sExtnSourceOrderNo);
  }

  private void deleteCallCenterQuote(YFSEnvironment env, Document inDoc, String sExtnSourceOrderNo) {

      Document docGetOrderListIp =  GCXMLUtil.createDocument(GCConstants.ORDER);
      Element eleRoot = docGetOrderListIp.getDocumentElement();

      Element eleExtn = docGetOrderListIp.createElement(GCConstants.EXTN);
      eleRoot.appendChild(eleExtn);
      eleExtn.setAttribute(GCConstants.EXTN_SOURCE_ORDER_NO, sExtnSourceOrderNo);     
      Document docGetOrderListOp = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, docGetOrderListIp);

      Element eleRootGetOrderListOp = docGetOrderListOp.getDocumentElement();
      Element eleOrderGetOrderListOp = (Element) eleRootGetOrderListOp.getElementsByTagName(GCConstants.ORDER).item(0);
      String sOrderHeaderKey = eleOrderGetOrderListOp.getAttribute(GCConstants.ORDER_HEADER_KEY);

      Document docDeleteOrderIp = GCXMLUtil.createDocument(GCConstants.ORDER);
      Element eleRootDeteleOrderIp = docDeleteOrderIp.getDocumentElement();
      eleRootDeteleOrderIp.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);

      GCCommonUtil.invokeAPI(env, GCConstants.DELETE_ORDER, docDeleteOrderIp);
  }*/
  
  /**
   * 
   * @param inDoc
   * @param sExtnSourceOrderNo
   * @param docGetOrderListOp
   * @throws TransformerException
   */
  private void appendQuoteNotes(Document inDoc, String sExtnSourceOrderNo, Document docGetOrderListOp) throws TransformerException {

      Element eleOrder = inDoc.getDocumentElement();
      //      Element eleNotes = (Element) eleOrder.getElementsByTagName(GCConstants.NOTES).item(0);
      Element eleNotes = (Element) XPathAPI.selectSingleNode(inDoc, "//Order/Notes");
      if(YFCCommon.isVoid(eleNotes)){
          eleNotes = inDoc.createElement(GCConstants.NOTES);
          eleOrder.appendChild(eleNotes);
      }
      Element eleNote = inDoc.createElement(GCConstants.NOTE);
      eleNotes.appendChild(eleNote);
      eleNote.setAttribute(GCConstants.NOTE_TEXT, "This is a conversion of quote# "+sExtnSourceOrderNo);

      NodeList nlNote = XPathAPI.selectNodeList(docGetOrderListOp, "//Order/Notes/Note");
      for(int i=0;i<nlNote.getLength();i++){
          Node nNote = inDoc.importNode(nlNote.item(i), true);
          eleNotes.appendChild(nNote);
      }
  }

  /**
   * 
   * @param env
   * @param inDoc
   * @param docGetOrderListOp
   */
  
  private void deleteCallCenterQuote(YFSEnvironment env, Document inDoc, Document docGetOrderListOp) {

      Element eleRootGetOrderListOp = docGetOrderListOp.getDocumentElement();
      Element eleOrderGetOrderListOp = (Element) eleRootGetOrderListOp.getElementsByTagName(GCConstants.ORDER).item(0);
      String sOrderHeaderKey = eleOrderGetOrderListOp.getAttribute(GCConstants.ORDER_HEADER_KEY);

      Document docDeleteOrderIp = GCXMLUtil.createDocument(GCConstants.ORDER);
      Element eleRootDeteleOrderIp = docDeleteOrderIp.getDocumentElement();
      eleRootDeteleOrderIp.setAttribute(GCConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
      eleRootDeteleOrderIp.setAttribute(GCConstants.ACTION, "DELETE");

      GCCommonUtil.invokeAPI(env, GCConstants.DELETE_ORDER, docDeleteOrderIp);
  }
  
  /**
   * 
   * @param env
   * @param inDoc
   * @throws TransformerException
   */
  private void appendPriceOverrideNotes(YFSEnvironment env, Document inDoc) throws TransformerException {

      Document docGetCommonCodeList = GCXMLUtil.createDocument(GCConstants.COMMON_CODE);
      Element eleCommonCode = docGetCommonCodeList.getDocumentElement();
      eleCommonCode.setAttribute(GCConstants.CODE_TYPE, GCConstants.YCD_APPEASEMENT_RSN);
      Document docGetCommonCodeOp = GCCommonUtil.invokeAPI(env, GCConstants.GET_COMMON_CODE_LIST, docGetCommonCodeList);
      Map<String, String> mapReasonCodeDesc = new HashMap<String, String>();
      NodeList nlCommonCode = XPathAPI.selectNodeList(docGetCommonCodeOp, "//CommonCode");
      for(int i=0; i<nlCommonCode.getLength();i++){
          Element eleCommonCodeOp=(Element) nlCommonCode.item(i);
          String sCodeValue=eleCommonCodeOp.getAttribute(GCConstants.CODE_VALUE);
          if(!YFCCommon.isVoid(sCodeValue)){
              mapReasonCodeDesc.put(sCodeValue, eleCommonCodeOp.getAttribute(GCConstants.CODE_SHORT_DESCRIPTION));
          }
      }

      NodeList nlPriceOverrideNote = XPathAPI.selectNodeList(inDoc, "//Note[@NoteText ='PriceOverrideReasonCode']");
      for(int i=0;i<nlPriceOverrideNote.getLength();i++){
          Element elePriceOverrideNote = (Element) nlPriceOverrideNote.item(i);
          String sReasonCode=elePriceOverrideNote.getAttribute(GCConstants.REASON_CODE);
          if(!YFCCommon.isVoid(sReasonCode)){
              if(!YFCCommon.isVoid(mapReasonCodeDesc.get(sReasonCode))){
                  elePriceOverrideNote.setAttribute(GCConstants.NOTE_TEXT, "PriceOverrideReasonCode: "+mapReasonCodeDesc.get(sReasonCode));
              }
              else{
                  elePriceOverrideNote.setAttribute(GCConstants.NOTE_TEXT, "PriceOverrideReasonCode: "+sReasonCode);
              }
              elePriceOverrideNote.setAttribute(GCConstants.REASON_CODE, sReasonCode);
          }
      }   
  }
  
  /**
   * This method stamp the IsCommercialAddress Flag on the order.
   * @param inDoc
   */
  
  private void updateCommercialAddressFlag(Document inDoc){
    LOGGER.beginTimer("GCBeforeCreateOrderUE.updateCommercialAddressFlag");
    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement eleOrder = inputDoc.getDocumentElement();
    YFCElement elePersonInfoShipTo = eleOrder.getChildElement(GCConstants.PERSON_INFO_SHIP_TO);
    if(!YFCCommon.isVoid(elePersonInfoShipTo)){
      String sCommercialAddress = elePersonInfoShipTo.getAttribute(GCConstants.IS_COMMERCIAL_ADDRESS);
      if(YFCCommon.isStringVoid(sCommercialAddress)){
        elePersonInfoShipTo.setAttribute(GCConstants.IS_COMMERCIAL_ADDRESS, GCConstants.N);
      }
      String sFulfillmentType = GCXMLUtil.getAttributeFromXPath(inDoc, "Order/OrderLines/OrderLine/@FulfillmentType");
      if (!YFCCommon.isStringVoid(sFulfillmentType)) {
        if (!YFCCommon.isStringVoid(elePersonInfoShipTo.getAttribute(GCConstants.COMPANY))
            || sFulfillmentType.indexOf(GCConstants.PICKUP_IN_STORE) >= 0) {
          elePersonInfoShipTo.setAttribute(GCConstants.IS_COMMERCIAL_ADDRESS, GCConstants.YES);
        }
      }
    }
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose(" updateCommercialAddressFlag() method, updated inputDoc is" + inputDoc.toString());
    }
    LOGGER.endTimer("GCBeforeCreateOrderUE.updateCommercialAddressFlag");
  }
 



  private void appendUserNameInCommissionForMpos(YFSEnvironment env, Document inDoc) throws TransformerException {
    // order level commission
    NodeList nlOrderCommission = XPathAPI.selectNodeList(inDoc, "//GCOrderCommission");
    for (int iOrderCommission = 0; iOrderCommission < nlOrderCommission.getLength(); iOrderCommission++) {
      Element eleOrderCommission = (Element) nlOrderCommission.item(iOrderCommission);
      String sUserName = "";
      String sExtnEmployeeID = eleOrderCommission.getAttribute(GCConstants.USER_ID);
      if (StringUtils.isNotBlank(sExtnEmployeeID)) {
        sUserName = getUserNameByExtnEmployeeID(env, sExtnEmployeeID);
      }
      eleOrderCommission.setAttribute(GCConstants.UserName, sUserName);
    }
  }

  @Override
  public void setProperties(Properties paramProperties) throws Exception {
    prop = paramProperties;
  }

}
