/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Handle the functionalities required for before order creation for purchase order
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               11/05/2014        Singh, Gurpreet            Shipment Development
 *#################################################################################################################################################################
 */
package com.gc.userexit;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;
import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.yfs.japi.ue.YFSBeforeCreateOrderUE;

/**
 * @author <a href="mailto:gurpreet.singh@expicient.com">Gurpreet</a>
 */
public class GCBeforePurchaseOrderCreateUE implements YFSBeforeCreateOrderUE {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCBeforePurchaseOrderCreateUE.class);

  /**
   *
   * Description of beforeCreateOrder For the purchase order, stamp ShipNode
   * at the OrderLine, stamp PersonInfoShipTo and PersonInfoBillTo at Order
   * level
   *
   * @param env
   * @param inDoc
   * @return
   * @throws YFSUserExitException
   * @see com.yantra.yfs.japi.ue.YFSBeforeCreateOrderUE#beforeCreateOrder(com.yantra.yfs.japi.YFSEnvironment,
   *      org.w3c.dom.Document)
   */
  @Override
  public Document beforeCreateOrder(YFSEnvironment env, Document inDoc)
      throws YFSUserExitException {
    LOGGER.debug(" Entering GCBeforePurchaseOrderCreateUE.beforeCreateOrder() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" beforeCreateOrder() method, Input document is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    try {
      Element eleRootOfInDoc = inDoc.getDocumentElement();
      String sChainType = eleRootOfInDoc
          .getAttribute(GCConstants.CHAIN_TYPE);
      if (GCConstants.DROP_SHIP.equals(sChainType)) {
        LOGGER.debug(" Inside if ChainType is DROP_SHIP ");
        stampOrderLineShipNode(env, inDoc);
        stampShipToAndBillToInfoAndPrice(env, inDoc);
        if(LOGGER.isVerboseEnabled()){
          LOGGER.verbose("Method Outdoc :" +YFCDocument.getDocumentFor(inDoc).toString());
        }
      }
    } catch (YFSException e) {
      LOGGER.error(
          " Inside catch block of GCBeforePurchaseOrderCreateUE.beforeCreateOrder(), Exception is :",
          e);
      throw (new GCException(e)).getYFSUserExitException();
    } catch (Exception ex) {
      LOGGER.error(
          " Inside catch block of GCBeforePurchaseOrderCreateUE.beforeCreateOrder(), Exception is :",
          ex);
      throw (new GCException(ex)).getYFSUserExitException();
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" Updated Input document is"
          + GCXMLUtil.getXMLString(inDoc));
    }
    LOGGER.debug(" Exiting GCBeforePurchaseOrderCreateUE.beforeCreateOrder() method ");
    return inDoc;
  }

  /**
   *
   * Description of stampOrderLineShipNode For the purchase order, stamp
   * ShipNode at the OrderLine Value of ShipNode will be PrimarySupplier of
   * the item
   *
   * @param env
   * @param inDoc
   * @throws Exception
   *
   */
  private void stampOrderLineShipNode(YFSEnvironment env, Document inDoc)
      throws GCException {
    LOGGER.debug(" Entering GCBeforePurchaseOrderCreateUE.stampOrderLineShipNode() method ");
    try {
      NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc,
          "Order/OrderLines/OrderLine");
      for (int iCounter = 0; iCounter < nlOrderLine.getLength(); iCounter++) {
        Element eleOrderLine = (Element) nlOrderLine.item(iCounter);
        
        //Changes pertaining to VDS_31
        String sGCVendorShipNode =  GCXMLUtil.getAttributeFromXPath(eleOrderLine, "Schedules/Schedule/@ShipNode");
        LOGGER.debug(" sGCVendorShipNode is " + sGCVendorShipNode);

        String sItemID = GCXMLUtil.getAttributeFromXPath(eleOrderLine,
            "Item/@ItemID");
        String sUOM = GCXMLUtil.getAttributeFromXPath(eleOrderLine,
            "Item/@UnitOfMeasure");
        Document docGetItemListOP = GCCommonUtil.getItemList(env,
            sItemID, sUOM, GCConstants.GCI);
        String sPrimarySupplier = GCXMLUtil.getAttributeFromXPath(
            docGetItemListOP, "ItemList/Item/"
                + "PrimaryInformation/@PrimarySupplier");
        LOGGER.debug(" sPrimarySupplier is " + sPrimarySupplier);
        
        
      //Changes pertaining to VDS_31
        if(null!=sGCVendorShipNode && sGCVendorShipNode.equals("GCVendorNode")){
        	eleOrderLine.setAttribute(GCConstants.SHIP_NODE,sGCVendorShipNode);
        	}
        else{
        	eleOrderLine.setAttribute(GCConstants.SHIP_NODE, sPrimarySupplier);
        }
        
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCBeforePurchaseOrderCreateUE.stampOrderLineShipNode(), Exception is :",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCBeforePurchaseOrderCreateUE.stampOrderLineShipNode() method ");
  }

  /**
   *
   * Description of stampShipToAndBillToInfo Stamp PersonInfoShipTo and
   * PersonInfoBillTo on the Purchase Order from Sales Order
   *
   * @param env
   * @param inDoc
   * @throws Exception
   *
   */
  private void stampShipToAndBillToInfoAndPrice(YFSEnvironment env,
      Document inDoc) throws GCException {
    LOGGER.debug(" Entering GCBeforePurchaseOrderCreateUE.stampShipToAndBillToInfo() method ");
    try {
      Element eleRoot = inDoc.getDocumentElement();
      String sSalesOrderHeaderKey = GCXMLUtil.getAttributeFromXPath(
          eleRoot, "OrderLines/OrderLine/@ChainedFromOrderHeaderKey");
      Document docGetOrderListIP = GCXMLUtil
          .getDocument("<Order OrderHeaderKey='"
              + sSalesOrderHeaderKey + "' />");
      Document docGetOrderListTmp = GCXMLUtil
          .getDocument("<OrderList><Order OrderHeaderKey='' LevelOfService=''><Extn ExtnPickupStoreID='' ExtnOrderPhase=''/><OrderLines><OrderLine OrderLineKey=''>"
              + "<LinePriceInfo UnitPrice=''/><PersonInfoShipTo><Extn /></PersonInfoShipTo><LineCharges><LineCharge/></LineCharges></OrderLine></OrderLines><PersonInfoShipTo><Extn /></PersonInfoShipTo><PersonInfoBillTo /></Order></OrderList>");
      Document docGetOrderListOP = GCCommonUtil.invokeAPI(env,
          GCConstants.API_GET_ORDER_LIST, docGetOrderListIP,
          docGetOrderListTmp);
      if (LOGGER.isVerboseEnabled()) {
        LOGGER.verbose(" docGetOrderListOP is"
            + GCXMLUtil.getXMLString(docGetOrderListOP));
      }

      // Changes for stamping ExtnPickupStoreID from parent sales order to chained order -- START
      //11-03-2015

      String extnPickupStoreID = GCXMLUtil.getAttributeFromXPath(docGetOrderListOP, "OrderList/Order/Extn/@ExtnPickupStoreID");
      String slevelOfService = GCXMLUtil.getAttributeFromXPath(docGetOrderListOP, "OrderList/Order/@LevelOfService");
      LOGGER.verbose("Pickup Store ID :" +extnPickupStoreID);
      LOGGER.verbose("level Of Service :" +slevelOfService);
      String extnOrderPhase =
          GCXMLUtil.getAttributeFromXPath(docGetOrderListOP, "OrderList/Order/Extn/@ExtnOrderPhase");
      LOGGER.verbose("Order Phase :" + extnOrderPhase);
      Element extnInPOEle = inDoc.createElement(GCConstants.EXTN);
      eleRoot.appendChild(extnInPOEle);
      eleRoot.setAttribute(GCConstants.LEVEL_OF_SERVICE, slevelOfService);
      extnInPOEle.setAttribute(GCConstants.EXTN_PICKUP_STORE_ID, extnPickupStoreID);
      extnInPOEle.setAttribute(GCConstants.EXTN_ORDER_PHASE, extnOrderPhase);

      // Changes for stamping ExtnPickupStoreID from parent sales order to chained order -- END

      Element elePersonInfoShipTo = (Element) XPathAPI.selectSingleNode(
          docGetOrderListOP, "OrderList/Order/PersonInfoShipTo");
      if (!YFCCommon.isVoid(elePersonInfoShipTo)) {
        LOGGER.debug(" Inside if PersonInfoShipTo is not null ");
        Element elePOPersonInfoShipTo = GCXMLUtil.createElement(inDoc,
            GCConstants.PERSON_INFO_SHIP_TO, null);
        GCXMLUtil.copyElement(inDoc, elePersonInfoShipTo,
            elePOPersonInfoShipTo);
        eleRoot.appendChild(elePOPersonInfoShipTo);
      }
      Element elePersonInfoBillTo = (Element) XPathAPI.selectSingleNode(
          docGetOrderListOP, "OrderList/Order/PersonInfoBillTo");
      if (!YFCCommon.isVoid(elePersonInfoBillTo)) {
        LOGGER.debug(" Inside if PersonInfoBillTo is not null ");
        Element elePOPersonInfoBillTo = GCXMLUtil.createElement(inDoc,
            GCConstants.PERSON_INFO_BILL_TO, null);
        GCXMLUtil.copyElement(inDoc, elePersonInfoBillTo,
            elePOPersonInfoBillTo);
        eleRoot.appendChild(elePOPersonInfoBillTo);
      }

      NodeList nlOrderLine = XPathAPI.selectNodeList(docGetOrderListOP,
          "OrderList/Order/OrderLines/OrderLine");
      for (int iCount = 0; iCount < nlOrderLine.getLength(); iCount++) {
        LOGGER.debug(" Inside looping on the orderlines ");
        Element eleOrderLine = (Element) nlOrderLine.item(iCount);
        String sOrderLineKey = eleOrderLine
            .getAttribute(GCConstants.ORDER_LINE_KEY);
        LOGGER.debug(" sOrderLineKey " + sOrderLineKey);
        Element elePOOrderLine = (Element) XPathAPI.selectSingleNode(
            inDoc,
            "Order/OrderLines/OrderLine[@ChainedFromOrderLineKey='"
                + sOrderLineKey + "']");
        if (!YFCCommon.isVoid(elePOOrderLine)) {
          Element eleLinePriceInfo = (Element) XPathAPI
              .selectSingleNode(eleOrderLine,
                  GCConstants.LINE_PRICE_INFO);
          Element elePOLinePriceInfo = GCXMLUtil.createElement(inDoc,
              GCConstants.LINE_PRICE_INFO, null);
          GCXMLUtil.copyElement(inDoc, eleLinePriceInfo,
              elePOLinePriceInfo);
          elePOOrderLine.appendChild(elePOLinePriceInfo);
          
//        VDS -106  Changes Start
          Element eleLinePersonInfoShipTo = (Element) XPathAPI
                  .selectSingleNode(eleOrderLine,
                      GCConstants.PERSON_INFO_SHIP_TO);
          if (!YFCCommon.isVoid(eleLinePersonInfoShipTo)) {

                  Element elePOLinePersonInfoShipTo = GCXMLUtil.createElement(inDoc,
                      GCConstants.PERSON_INFO_SHIP_TO, null);
                  GCXMLUtil.copyElement(inDoc, eleLinePersonInfoShipTo,
                		  elePOLinePersonInfoShipTo);
                  elePOOrderLine.appendChild(elePOLinePersonInfoShipTo);
          }      
//              VDS - 106 Changes End

        }
      }

    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCBeforePurchaseOrderCreateUE.stampShipToAndBillToInfo(), Exception is :",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCBeforePurchaseOrderCreateUE.stampShipToAndBillToInfo() method ");
  }

  @Override
  public String beforeCreateOrder(YFSEnvironment arg0, String arg1)
      throws YFSUserExitException {

    return null;
  }

}
