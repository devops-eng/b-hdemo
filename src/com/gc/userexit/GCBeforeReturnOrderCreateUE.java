/**
 * Copyright � 2014, Guitar Center, All rights reserved.
 * ############################################
 * ######################################################
 * ########################################################### OBJECTIVE: This class is called at
 * return creation.
 * #################################################################################
 * ################################################################################ Version Date
 * Modified By Description
 * ##########################################################################
 * ####################################################################################### 1.0
 * 05/05/2014 Soni, Karan Initial Version
 * ###########################################################
 * #######################################
 * ###############################################################
 **/

package com.gc.userexit;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.yfs.japi.ue.YFSBeforeCreateOrderUE;

/**
 * @author karan
 */
public class GCBeforeReturnOrderCreateUE implements YFSBeforeCreateOrderUE {

  // Initialize Logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCBeforeReturnOrderCreateUE.class);

  @Override
  public String beforeCreateOrder(YFSEnvironment arg0, String arg1)
      throws YFSUserExitException {
    return null;
  }

  @Override
  public Document beforeCreateOrder(YFSEnvironment env, Document inDoc)
      throws YFSUserExitException {

    try {
      LOGGER.verbose("Entering beforeCreateOrder method");
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("The input xml to beforeCreateOrder method is"+ GCXMLUtil.getXMLString(inDoc));
      }
      Element eleOrder = inDoc.getDocumentElement();
      String strOrderType = eleOrder.getAttribute(GCConstants.ORDER_TYPE);
      String strSOOHK = GCXMLUtil.getAttributeFromXPath(eleOrder,
          "OrderLines/OrderLine/@DerivedFromOrderHeaderKey");
      Document docGetOrderList = null;
      Document docGetOrderListTemp =null;
      if(!YFCCommon.isStringVoid(strSOOHK)){
        String strDocGOL= "<Order OrderHeaderKey='"+strSOOHK+"'/>";
        Document inDocGOL= GCXMLUtil.getDocument(strDocGOL);
        //OMS-5299: Added EnterpriseCode and OrderHeaderKey attributes
        String strGetOrderListTemp =
            "<OrderList><Order EnterpriseCode='' OrderHeaderKey='' BillToID='' EntryType='' TaxExemptFlag=''><OrderLines><OrderLine><Item><PrimaryInformation/><Extn/></Item></OrderLine></OrderLines><PaymentMethods><PaymentMethod/></PaymentMethods></Order></OrderList>";
        //OMS-5299
        docGetOrderListTemp = GCXMLUtil
            .getDocument(strGetOrderListTemp);
        docGetOrderList = GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST,inDocGOL , docGetOrderListTemp);
        LOGGER.debug("GCBeforeReturnOrderCreateUE :: beforeCreateOrder :: docGetOrderList :: \n"+GCXMLUtil.getXMLString(docGetOrderList));

        // Fix to stamp BillToID
        Element eleSO = GCXMLUtil.getElementByXPath(
            docGetOrderList, "OrderList/Order");
        String strBillToId=eleSO.getAttribute("BillToID");
        LOGGER.debug("GCBeforeReturnOrderCreateUE :: beforeCreateOrder :: BillToID :: "+strBillToId);
        eleOrder.setAttribute("BillToID", strBillToId);

        // Fix to stamp TaxExemptFlag
        eleOrder.setAttribute("TaxExemptFlag", eleSO.getAttribute("TaxExemptFlag"));

      }
      if (GCConstants.STORE_RETURN.equalsIgnoreCase(strOrderType) && !YFCCommon.isStringVoid(strSOOHK)) {

        String strStoreCredit = GCXMLUtil.getAttributeFromXPath(
            eleOrder, "Extn/@ExtnStoreCredit");
        LOGGER.debug("The strStoreCredit is " + strStoreCredit);
        Element eleOrderSO = GCXMLUtil.getElementByXPath(
            docGetOrderList, "OrderList/Order");
        //OMS 5537 Start
        String strSalesEntryType=eleOrderSO.getAttribute(GCConstants.ENTRY_TYPE);
        //OMS 5537 End
        String strEnterpriseCode = eleOrderSO
            .getAttribute(GCConstants.ENTERPRISE_CODE);
        String strOrderHeaderKey = eleOrderSO
            .getAttribute(GCConstants.ORDER_HEADER_KEY);
        LOGGER.debug("The strEnterpriseCode is" + strEnterpriseCode
            + "The strOrderHeaderKey is " + strOrderHeaderKey);

        //OMS 5537 Start
        if(LOGGER.isDebugEnabled()){
          LOGGER.debug("Stamping entry type on store return from sales order::"+strSalesEntryType);
        }
        if(!YFCCommon.isVoid(strSalesEntryType)){
          eleOrder.setAttribute(GCConstants.ENTRY_TYPE, strSalesEntryType);
        }
        //OMS 5537 End

        //OMS-5134
        String strBillToId=eleOrderSO.getAttribute("BillToID");
        LOGGER.debug("GCBeforeReturnOrderCreateUE :: beforeCreateOrder :: BillToID :: "+strBillToId);
        eleOrder.setAttribute("BillToID", strBillToId);
        //OMS-5134
        // If refund type is store credit
        if (GCConstants.YES.equalsIgnoreCase(strStoreCredit)) {
          LOGGER.debug("Entering if strStoreCredit equals Yes");
          Element eleStoreCredit = GCXMLUtil
              .getElementByXPath(docGetOrderList,
                  "OrderList/Order/PaymentMethods/PaymentMethod[@PaymentType='STORE_CREDIT']");

          if (YFCCommon.isVoid(eleStoreCredit)) {
            Document capturePaymentDoc = GCXMLUtil
                .getDocument("<CapturePayment EnterpriseCode=\""
                    + strEnterpriseCode
                    + "\" OrderHeaderKey=\""
                    + strOrderHeaderKey
                    + "\" ProceedOnZeroAmountFailure=\"Y\" />");
            Element eCaptuePayment = capturePaymentDoc
                .getDocumentElement();
            Element eCapturePaymentMethods = capturePaymentDoc
                .createElement("PaymentMethods");
            Element eCapturePaymentMethod = capturePaymentDoc
                .createElement("PaymentMethod");
            eCapturePaymentMethod.setAttribute("PaymentType",
                "STORE_CREDIT");
            eCapturePaymentMethod.setAttribute("Operation",
                "Manage");
            eCapturePaymentMethod.setAttribute("PaymentReference1",
                "store_credit");
            eCapturePaymentMethod.setAttribute("UnlimitedCharges",
                "Y");
            GCXMLUtil.appendChild(eCapturePaymentMethods,
                eCapturePaymentMethod);
            GCXMLUtil.appendChild(eCaptuePayment,
                eCapturePaymentMethods);
            if (LOGGER.isDebugEnabled()) {
              LOGGER.debug("The capture payment method input"
                  + GCXMLUtil.getXMLString(capturePaymentDoc));
            }
            // invoke API
            YIFApi yifApi = YIFClientFactory.getInstance()
                .getLocalApi();
            yifApi.invoke(env, "capturePayment",
                capturePaymentDoc);
          }
        }
      }




    } catch (Exception e) {
      LOGGER.error("Inside GCBeforeReturnOrderCreateUE.beforeCreateOrder()", e);
      throw new GCException(e).getYFSUserExitException();
    }



    return inDoc;
  }

}
