/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: 
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               10/03/2014        Singh, Gurpreet            OMS-1468: Order Scheduling
 *#################################################################################################################################################################
 */
package com.gc.userexit;

import java.util.HashMap;
import java.util.Map;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.yfs.japi.ue.YFSConfirmAssignmentsUE;

/**
 * @author <a href="mailto:gurpreet.singh@expicient.com">Gurpreet</a>
 */
public class GCConfirmAssignmentsUE implements YFSConfirmAssignmentsUE {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCConfirmAssignmentsUE.class);

    /**
     * 
     * Description of confirmAssignments a)For Transaction ID is SCHEDULE.0001,
     * looks for all the lines check ExtnIsPOReserved='Y' and move lines to
     * SchduledPO status for PO supply allocation b)For Transaction ID is
     * RELEASE.0001, check for ExtnIsShipAlone=�Y� and ExtnNonHJOrderLine =�Y�,
     * if yes, then stamp RejectAssignment='true'
     * 
     * @param env
     * @param inDoc
     * @return
     * @throws YFSUserExitException
     * @see com.yantra.yfs.japi.ue.YFSConfirmAssignmentsUE#confirmAssignments(com.yantra.yfs.japi.YFSEnvironment,
     *      org.w3c.dom.Document)
     */
    public Document confirmAssignments(YFSEnvironment env, Document inDoc)
	    throws YFSUserExitException {
	LOGGER.debug(" Entering GCConfirmAssignmentsUE.confirmAssignments() method ");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" Input document is" + GCXMLUtil.getXMLString(inDoc));
	}
	Document docConfirmAssOP = null;
	try {
	    Element eleRootOfInDoc = inDoc.getDocumentElement();
	    String sTransactionId = eleRootOfInDoc
		    .getAttribute(GCConstants.TRANSACTION_ID);
	    LOGGER.debug(" sTransactionId is" + sTransactionId);

	    docConfirmAssOP = GCXMLUtil.createDocument(GCConstants.ORDER);
	    Element eleConfirmAss = docConfirmAssOP.getDocumentElement();
	    Element eleOrderLinesConfirmAss = GCXMLUtil.createElement(
		    docConfirmAssOP, GCConstants.ORDER_LINES, null);
	    eleConfirmAss.appendChild(eleOrderLinesConfirmAss);

	    if (GCConstants.RELEASE_TRAN_ID.equalsIgnoreCase(sTransactionId)) {
		LOGGER.debug(" Inside Transaction ID is RELEASE.0001 ");
		docConfirmAssOP = processReleaseTran(env, inDoc,
			docConfirmAssOP, eleOrderLinesConfirmAss);
	    }
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCConfirmAssignmentsUE.confirmAssignments()",
		    e);
	    throw (new GCException(e)).getYFSUserExitException();
	}
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" docConfirmAssOP is "
		    + GCXMLUtil.getXMLString(docConfirmAssOP));
	}
	LOGGER.debug(" Exiting GCConfirmAssignmentsUE.confirmAssignments() method ");
	return docConfirmAssOP;
    }

    /**
     * 
     * Description of processReleaseTran Iterate over the orderLines and do not
     * release the lines having ExtnNonHJOrderLine='Y' and ExtnIsShipALone='Y'
     * with the regular orderlines Order Lines with these flags will be released
     * one by one
     * 
     * @param env
     * @param inDoc
     * @param docConfirmAssOP
     * @param eleOrderLinesConfirmAss
     * @return
     * @throws GCException
     * 
     */
    private Document processReleaseTran(YFSEnvironment env, Document inDoc,
	    Document docConfirmAssOP, Element eleOrderLinesConfirmAss)
	    throws GCException {
	LOGGER.debug(" Entering GCConfirmAssignmentsUE.processReleaseTran() method ");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" Input document is" + GCXMLUtil.getXMLString(inDoc));
	    LOGGER.debug(" docConfirmAssOP is"
		    + GCXMLUtil.getXMLString(docConfirmAssOP));
	    LOGGER.debug(" eleOrderLinesConfirmAss is"
		    + GCXMLUtil.getElementXMLString(eleOrderLinesConfirmAss));
	}
	try {
	    NodeList nlOrderLine = XPathAPI.selectNodeList(inDoc,
		    "Order/OrderLines/OrderLine");
	    NodeList nlNonHJOrderLine = XPathAPI.selectNodeList(inDoc,
		    "Order/OrderLines/OrderLine[Extn/@ExtnNonHJOrderLine='Y']");
	    Map<String, String> mShipAlone = getNoOfShipAloneLines(env,
		    nlOrderLine);

	    for (int iCounter = 0; iCounter < nlOrderLine.getLength(); iCounter++) {
		LOGGER.debug(" Looping over the OrderLines  ");
		Element eleOrderLine = (Element) nlOrderLine.item(iCounter);
		String sOrderLineKey = eleOrderLine
			.getAttribute(GCConstants.ORDER_LINE_KEY);
		Element eleOrderLineConfirmAss = GCXMLUtil.createElement(
			docConfirmAssOP, GCConstants.ORDER_LINE, null);
		eleOrderLineConfirmAss.setAttribute(GCConstants.ORDER_LINE_KEY,
			eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY));
		eleOrderLinesConfirmAss.appendChild(eleOrderLineConfirmAss);

		Element eleSchedules = GCXMLUtil.createElement(docConfirmAssOP,
			GCConstants.SCHEDULES, null);
		eleOrderLineConfirmAss.appendChild(eleSchedules);

		Element eleSchedule = GCXMLUtil.createElement(docConfirmAssOP,
			GCConstants.SCHEDULE, null);
		eleSchedule.setAttribute(GCConstants.SCHEDULE_ID, GCXMLUtil
			.getAttributeFromXPath(eleOrderLine,
				"Schedules/Schedule/@ScheduleId"));

		// Check if this is a ship alone line and it is not the only
		// line on the order
		// stamp RejectAssignment = true
		if ((nlOrderLine.getLength() > mShipAlone.size())) {
		    String sIsShipAloneLine = mShipAlone.get(sOrderLineKey);
		    if (GCConstants.YES.equalsIgnoreCase(sIsShipAloneLine)) {
			LOGGER.debug(" Inside IsShipAlone line ");
			eleSchedule.setAttribute(GCConstants.REJECT_ASSIGNMENT,
				GCConstants.TRUE);
		    }
		} else if ((nlOrderLine.getLength() == mShipAlone.size())
			&& iCounter < (nlOrderLine.getLength() - 1)) {
		    // If all the Orderlines on the order are ship alone lines,
		    // then
		    // Release only 1 OrderLine at a time
		    // and stamp RejectAssignment='True' for the other
		    // OrderLines
		    LOGGER.debug(" Until last shipAlone line, stamp RejectAssignment='Y'");
		    eleSchedule.setAttribute(GCConstants.REJECT_ASSIGNMENT,
			    GCConstants.TRUE);
		}

		// Check if this is a ExtnNonHJOrderLine Line and it is not the
		// only line on the order
		// stamp RejectAssignment = true
		if ((nlOrderLine.getLength() > nlNonHJOrderLine.getLength())) {
		    String sNonHJOrderLine = GCXMLUtil.getAttributeFromXPath(
			    eleOrderLine, "Extn/@ExtnNonHJOrderLine");
		    if (GCConstants.YES.equalsIgnoreCase(sNonHJOrderLine)) {
			LOGGER.debug(" Inside ExtnNonHJOrderLine line ");
			eleSchedule.setAttribute(GCConstants.REJECT_ASSIGNMENT,
				GCConstants.TRUE);
		    }
		} else if ((nlOrderLine.getLength() == nlNonHJOrderLine
			.getLength())
			&& iCounter < (nlOrderLine.getLength() - 1)) {
		    // If all the Orderlines on the order are
		    // ExtnNonHJOrderLine='Y'
		    // lines, then Release only 1 OrderLine at a time
		    // and stamp RejectAssignment='True' for the other
		    // OrderLines
		    LOGGER.debug(" Until last NonHJOrderLine line, stamp RejectAssignment='Y'");
		    eleSchedule.setAttribute(GCConstants.REJECT_ASSIGNMENT,
			    GCConstants.TRUE);
		}
		eleSchedules.appendChild(eleSchedule);
	    }
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCConfirmAssignmentsUE.processReleaseTran()",
		    e);
	    throw new GCException(e);
	}
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" docConfirmAssOP is "
		    + GCXMLUtil.getXMLString(docConfirmAssOP));
	}
	LOGGER.debug(" Exiting GCConfirmAssignmentsUE.processReleaseTran() method ");
	return docConfirmAssOP;
    }

    /**
     * 
     * Description of getNoOfShipAloneLines Returns the NoOfShipAloneLines on
     * the basis of ExtnIsShipAlone='Y'
     * 
     * @param env
     * @param nlOrderLine
     * @return
     * @throws GCException
     * 
     */
    private Map<String, String> getNoOfShipAloneLines(YFSEnvironment env,
	    NodeList nlOrderLine) throws GCException {
	LOGGER.debug(" Entering GCConfirmAssignmentsUE.getNoOfShipAloneLines() method ");
	Map<String, String> mShipAlone = new HashMap<String, String>();
	try {
	    for (int iCounter = 0; iCounter < nlOrderLine.getLength(); iCounter++) {
		Element eleOrderLine = (Element) nlOrderLine.item(iCounter);

		Element eleItem = (Element) XPathAPI.selectSingleNode(
			eleOrderLine, GCConstants.ITEM);
		String sItemID = eleItem.getAttribute(GCConstants.ITEM_ID);
		String sUOM = eleItem.getAttribute(GCConstants.UOM);
		LOGGER.debug(" sItemID is " + sItemID + " sUOM is " + sUOM);

		Document docGetItemListOP = GCCommonUtil.getItemList(env,
			sItemID, sUOM, GCConstants.GCI);
		String sExtnIsShipAlone = GCXMLUtil
			.getAttributeFromXPath(docGetItemListOP,
				"ItemList/Item/Extn/@ExtnIsShipAlone");
		LOGGER.debug(" sExtnIsShipAlone is " + sExtnIsShipAlone);
		// OMS:4920 Start
		Element eleBundleParentLine = (Element) eleOrderLine.getElementsByTagName(GCConstants.BUNDLE_PARENT_LINE).item(0);
		  // OMS:4920 End
		if ((!YFCCommon.isVoid(sExtnIsShipAlone))
			&& GCConstants.YES.equalsIgnoreCase(sExtnIsShipAlone) && YFCCommon.isVoid(eleBundleParentLine) ) {
		    mShipAlone.put(eleOrderLine
			    .getAttribute(GCConstants.ORDER_LINE_KEY),
			    sExtnIsShipAlone);
		}
	    }
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCConfirmAssignmentsUE.getNoOfShipAloneLines()",
		    e);
	    throw new GCException(e);
	}
	LOGGER.debug(" Exiting GCConfirmAssignmentsUE.getNoOfShipAloneLines() method ");
	return mShipAlone;
    }

}
