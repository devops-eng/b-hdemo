/**
 * Copyright 2014 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  ######################################################################################################################################
 *   OBJECTIVE: This class is is used in the case of Refunds..
 *  ######################################################################################################################################
 *        Version    Date          Modified By           CR                Description
 *  ######################################################################################################################################
 *        1.0       04/07/2014     Soni, Karan                             Initial Version GCConfirmRefundDistribution UE.
 *        1.1       12/05/2014     Soni, Karan                             CR-65: Allow CSR's to specify the refund card
 *        1.2       18/03/2014     Singh, Gurpreet                         GCSTORE-556: Store Returns
 * *  ######################################################################################################################################
 */

package com.gc.userexit;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.yfs.japi.ue.OMPConfirmRefundDistributionUE;

public class GCConfirmRefundDistributionUE implements
OMPConfirmRefundDistributionUE {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCConfirmRefundDistributionUE.class);

  @Override
  public Document confirmRefundDistribution(YFSEnvironment env,
      Document inDocument) throws YFSUserExitException {
    Element ePaymentMethod = null;
    Element eRefundPaymentMethods = null;
    Element eRootElement = inDocument.getDocumentElement();
    // GCSTORE-556 Start
    YFCDocument yfcInDocument = YFCDocument.getDocumentFor(inDocument);
    YFCElement eleYFCRefundPaymentMethods = yfcInDocument.getElementsByTagName("RefundPaymentMethod").item(0);
    // GCSTORE-556 End

    // GCSTORE#4630 -- Starts
    List<String> paymentKeyList = new ArrayList<String>();
    boolean removeRefundType = false;
    // GCSTORE#4630 -- Ends
    try {
      LOGGER.verbose("Entering the confirmRefundDistribution method");
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("The UE is called on Cancel"
            + GCXMLUtil.getXMLString(inDocument));
      }
      Element eleRoot = inDocument.getDocumentElement();
      String strOrderHeaderKey = eleRoot
          .getAttribute(GCConstants.ORDER_HEADER_KEY);
      Element eleRefundMethod = (Element) XPathAPI
          .selectSingleNode(eleRoot,
              "RefundPaymentMethods/RefundPaymentMethod[@PaymentType='CREDIT_ON_ACCOUNT']");
      Element eleStoreCreditRefundMethod =
          (Element) XPathAPI.selectSingleNode(eleRoot,
              "RefundPaymentMethods/RefundPaymentMethod[@PaymentType='STORE_CREDIT']");
      String strEnterpriseCode = eleRoot
          .getAttribute(GCConstants.ENTERPRISE_CODE);
      // getting info in the case of multiple return order and which
      // return order needs to be mapped
      String strAmountToBeRefunded = "-"
          + GCXMLUtil
          .getAttributeFromXPath(inDocument,
              "Order/RefundPaymentMethods/@TotalAmountToBeRefunded");
      String strAmountToBeMaxCharge = GCXMLUtil
          .getAttributeFromXPath(inDocument,
              "Order/RefundPaymentMethods/@TotalAmountToBeRefunded");
      LOGGER.debug("The amount to be refunded" + strAmountToBeRefunded);
      Element eleOpenReturnCharge = GCXMLUtil
          .getElementByXPath(
              inDocument,
              "Order/ChargeTransactionDetails/ChargeTransactionDetail[@ChargeType='RETURN' and @Status='OPEN' and @DebitAmount='"
                  + strAmountToBeRefunded + "']");
      Element eleOpenMemoCharge = GCXMLUtil
          .getElementByXPath(
              inDocument,
              "Order/ChargeTransactionDetails/ChargeTransactionDetail[@ChargeType='ADJUSTMENTS' and @Status='OPEN' and @DebitAmount='"
                  + strAmountToBeRefunded + "']");
      Element eleOpenTransferOutCharge = GCXMLUtil
          .getElementByXPath(
              inDocument,
              "Order/ChargeTransactionDetails/ChargeTransactionDetail[@ChargeType='RETURN' and @Status='OPEN' and @DistributedAmount!='0.0']");
      String strIsRefundAccount = "";
      String strRefundPaymentKey="";
      String strIsStoreCredit = "";
      String sOrderCaptureChannel = "";
      if (!YFCCommon.isVoid(eleOpenReturnCharge)
          || !YFCCommon.isVoid(eleOpenMemoCharge) || !YFCCommon.isVoid(eleOpenTransferOutCharge)) {
        String strInvoiceKey = "";
        if (!YFCCommon.isVoid(eleOpenReturnCharge)) {
          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("The return element Charge Transaction Details"
                + GCXMLUtil.getElementString(eleOpenReturnCharge));
          }
          strInvoiceKey = eleOpenReturnCharge
              .getAttribute(GCConstants.ORDER_INVOICE_KEY);
        }
        if (!YFCCommon.isVoid(eleOpenMemoCharge)) {
          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("The Adjustment element Charge Transaction Details"
                + GCXMLUtil.getElementString(eleOpenMemoCharge));
          }
          strInvoiceKey = eleOpenMemoCharge
              .getAttribute(GCConstants.ORDER_INVOICE_KEY);
        }
        if (!YFCCommon.isVoid(eleOpenTransferOutCharge)) {
          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("The TransferOut element Charge Transaction Details"
                + GCXMLUtil.getElementString(eleOpenTransferOutCharge));
          }
          strInvoiceKey = eleOpenTransferOutCharge
              .getAttribute(GCConstants.ORDER_INVOICE_KEY);
        }
        String strIpGetOrderInvoiceDetais = "<GetOrderInvoiceDetails EnterpriseCode='"
            + strEnterpriseCode
            + "' InvoiceKey='"
            + strInvoiceKey
            + "' />";
        Document inDoc = GCXMLUtil
            .getDocument(strIpGetOrderInvoiceDetais);
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("The getOrderInvoiceDetails input doc"
              + GCXMLUtil.getXMLString(inDoc));
        }
        String strGOIDTemplate = "<InvoiceDetail><InvoiceHeader><Extn/><Order><Extn/></Order></InvoiceHeader></InvoiceDetail>";
        Document inDocTemplate = GCXMLUtil.getDocument(strGOIDTemplate);
        Document outDocGOID = GCCommonUtil.invokeAPI(env,
            GCConstants.GET_ORDER_INVOICE_DETAILS, inDoc,
            inDocTemplate);
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("The getOrderInvoiceDetails outDoc"
              + GCXMLUtil.getXMLString(outDocGOID));
        }
        //CR-65 Start
        if (!YFCCommon.isVoid(eleOpenMemoCharge)) {
          strIsRefundAccount = GCXMLUtil
              .getAttributeFromXPath(outDocGOID,
                  "InvoiceDetail/InvoiceHeader/Extn/@ExtnIsRefundOnAccount");
          strRefundPaymentKey = GCXMLUtil
              .getAttributeFromXPath(outDocGOID,
                  "InvoiceDetail/InvoiceHeader/Extn/@ExtnRefundPaymentKey");
        } else {
          strIsRefundAccount = GCXMLUtil
              .getAttributeFromXPath(outDocGOID,
                  "InvoiceDetail/InvoiceHeader/Order/Extn/@ExtnIsRefundOnAccount");
          strRefundPaymentKey = GCXMLUtil
              .getAttributeFromXPath(outDocGOID,
                  "InvoiceDetail/InvoiceHeader/Order/Extn/@ExtnRefundPaymentKey");
        }
        sOrderCaptureChannel =
            GCXMLUtil.getAttributeFromXPath(outDocGOID,
                "InvoiceDetail/InvoiceHeader/Order/Extn/@ExtnOrderCaptureChannel");
        LOGGER.debug("The strRefundPaymentKey is ---->>>>"+ strRefundPaymentKey);
        //CR-65 End
        strIsStoreCredit = GCXMLUtil
            .getAttributeFromXPath(outDocGOID,
                "InvoiceDetail/InvoiceHeader/Order/Extn/@ExtnStoreCredit");
        LOGGER.debug("The strIsRefundAccount value is"
            + strIsRefundAccount);
      }
      // Return info fetched end
      Element orderExtn = (Element) XPathAPI.selectSingleNode(eleRoot,
          "Extn");
      String strDaxCustomerId = orderExtn
          .getAttribute(GCConstants.EXTN_DAX_CUSTOMER_ID);
      // Fix for GCSTORE-4748
      if(YFCCommon.isVoid(strDaxCustomerId)){
          strDaxCustomerId = orderExtn.getAttribute("ExtnSourceCustomerID");
      }
      LOGGER.debug("The strEnterpriseCode-->" + strEnterpriseCode
          + "strDaxCustomerId-->" + strDaxCustomerId);
      boolean isRefundOnAccount = false;
      boolean isStoreCredit = false;
      boolean isStoreReturn = false;
      if (GCConstants.YES.equals(strIsRefundAccount)) {
        LOGGER.debug("Entering the strIsRefundAccount equals YES if");
        isRefundOnAccount = true;
        LOGGER.debug("Exiting the strIsRefundAccount equals YES if");
      }

      if (GCConstants.YES.equals(strIsStoreCredit)) {
        LOGGER.debug("Entering the strStoreCredit if");
        isStoreCredit = true;
        LOGGER.debug("Exiting the strStoreCredit if");
      }
      if ("GCSTORE".equalsIgnoreCase(sOrderCaptureChannel)) {
        isStoreReturn = true;
      }
      // Processing All other order types except STORE_RETURN
      Element eleCOA = GCXMLUtil
          .getElementByXPath(inDocument,
              "Order/PaymentMethods/PaymentMethod[@PaymentType='CREDIT_ON_ACCOUNT']");
      if (YFCCommon.isVoid(eleCOA)) {
        LOGGER.debug("Entering the COA payment type not exists If block ");
        if (!YFCCommon.isVoid(eleRefundMethod) || (isRefundOnAccount && !isStoreReturn)) {
          LOGGER.debug("Entering the COA append IF");
          Document capturePaymentDoc = GCXMLUtil
              .getDocument("<CapturePayment EnterpriseCode=\""
                  + strEnterpriseCode
                  + "\" OrderHeaderKey=\""
                  + strOrderHeaderKey
                  + "\" ProceedOnZeroAmountFailure=\"Y\" />");
          Element eCaptuePayment = capturePaymentDoc
              .getDocumentElement();
          Element eCapturePaymentMethods = capturePaymentDoc
              .createElement("PaymentMethods");
          Element eCapturePaymentMethod = capturePaymentDoc
              .createElement("PaymentMethod");
          eCapturePaymentMethod.setAttribute("PaymentType", "CREDIT_ON_ACCOUNT");
          eCapturePaymentMethod.setAttribute("RequestedAmount", strAmountToBeMaxCharge);
          eCapturePaymentMethod.setAttribute("Operation", "Manage");
          eCapturePaymentMethod.setAttribute("PaymentReference1",
              strDaxCustomerId);
          GCXMLUtil.appendChild(eCapturePaymentMethods,
              eCapturePaymentMethod);
          GCXMLUtil.appendChild(eCaptuePayment,
              eCapturePaymentMethods);
          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("The capture payment method input"
                + GCXMLUtil.getXMLString(capturePaymentDoc));
          }
          // invoke API
          YIFApi yifApi = YIFClientFactory.getInstance()
              .getLocalApi();
          Document outXML = yifApi.invoke(env, "capturePayment",
              capturePaymentDoc);
          ePaymentMethod = (Element) XPathAPI
              .selectSingleNode(outXML,
                  "//PaymentMethods/PaymentMethod[@PaymentType='CREDIT_ON_ACCOUNT']");
          String paymentKey = ePaymentMethod
              .getAttribute("PaymentKey");
          LOGGER.debug("The payment key is --> " + paymentKey);
          eRefundPaymentMethods = (Element) XPathAPI
              .selectSingleNode(eRootElement,
                  "RefundPaymentMethods/RefundPaymentMethod");
          // change the refund method
          if (eRefundPaymentMethods != null) {
            eRefundPaymentMethods.setAttribute("PaymentKey",
                paymentKey);
            eRefundPaymentMethods.setAttribute(
                GCConstants.PAYMENT_TYPE, "CREDIT_ON_ACCOUNT");
            eRefundPaymentMethods.setAttribute("PaymentReference1", strDaxCustomerId);
            // Fix for GCSTORE-4748
            if(YFCCommon.isVoid(strDaxCustomerId)){
                String sSourceCustomerID = orderExtn.getAttribute("ExtnSourceCustomerID");
                eRefundPaymentMethods.setAttribute("PaymentReference1", sSourceCustomerID);
            }

            // GCSTORE#4630 -- Starts (stamping refundAmt if isRefundOnAccount is true i.e. COA is
            // chosen as refund type)
            if (isRefundOnAccount && !isStoreReturn) {
              eRefundPaymentMethods.setAttribute("RefundAmount", strAmountToBeMaxCharge);
              paymentKeyList.add(paymentKey);
              removeRefundType = true;
              LOGGER.debug("paymentKeyList @265 : " + paymentKeyList);
            }
            // GCSTORE#4630 -- End
          }
          LOGGER.debug("Exiting the COA append IF");
        }
      } else {
        LOGGER.debug("Entering the Else block");
        if (!YFCCommon.isVoid(eleRefundMethod) || (isRefundOnAccount && !isStoreReturn)) {
          LOGGER.debug("Entering the elePrepaidPaymentType exists If block");
          String strMaxChargeLimit= GCXMLUtil.getAttributeFromXPath(inDocument, "Order/PaymentMethods/PaymentMethod[@PaymentType='CREDIT_ON_ACCOUNT']/@MaxChargeLimit");
          String strPaymentKey = GCXMLUtil
              .getAttributeFromXPath(
                  inDocument,
                  "Order/PaymentMethods/PaymentMethod[@PaymentType='CREDIT_ON_ACCOUNT']/@PaymentKey");
          Double dNewMaxChargeLimit= Double.valueOf(strMaxChargeLimit)+ Double.valueOf(strAmountToBeMaxCharge);
          String strChangeOdr= "<Order Override='Y' Action='Modify' OrderHeaderKey='"+strOrderHeaderKey+"'><PaymentMethods><PaymentMethod PaymentKey='"+strPaymentKey+"' MaxChargeLimit='"+Double.toString(dNewMaxChargeLimit)+"'/></PaymentMethods></Order>";
          Document inDocCO= GCXMLUtil.getDocument(strChangeOdr);
          GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, inDocCO);
          LOGGER.debug("The payment key is--> " + strPaymentKey);
          eRefundPaymentMethods = (Element) XPathAPI
              .selectSingleNode(eRootElement,
                  "RefundPaymentMethods/RefundPaymentMethod");
          // change the refund method
          if (eRefundPaymentMethods != null) {
            eRefundPaymentMethods.setAttribute("PaymentKey",
                strPaymentKey);
            eRefundPaymentMethods.setAttribute(
                GCConstants.PAYMENT_TYPE, "CREDIT_ON_ACCOUNT");
            eRefundPaymentMethods.setAttribute("PaymentReference1",
                strDaxCustomerId);

            // GCSTORE#4630 -- Starts (stamping refundAmt if isRefundOnAccount is true i.e. COA is
            // chosen as refund type)
            if (isRefundOnAccount && !isStoreReturn) {
              eRefundPaymentMethods.setAttribute("RefundAmount", strAmountToBeMaxCharge);
              paymentKeyList.add(strPaymentKey);
              removeRefundType = true;
              LOGGER.debug("paymentKeyList @302 : " + paymentKeyList);
            }
            // GCSTORE#4630 -- End
          }
          LOGGER.debug("Exiting the elePrepaidPaymentType exists If block");
        }
      }

      // GCSTORE-556 Start
      Element eleStoreCredit =
          GCXMLUtil.getElementByXPath(inDocument, "Order/PaymentMethods/PaymentMethod[@PaymentType='STORE_CREDIT']");
      // Check if Store Credit payment does not already exist on order
      if (YFCCommon.isVoid(eleStoreCredit)) {
        LOGGER.verbose("Entering the Store Credit payment type not exists If block ");
        if (!YFCCommon.isVoid(eleStoreCreditRefundMethod) || (isRefundOnAccount && isStoreReturn)) {
          LOGGER.verbose("Entering the Store Credit append IF");
          YFCDocument capturePaymentDoc =
              YFCDocument.getDocumentFor("<CapturePayment EnterpriseCode=\"" + strEnterpriseCode
                  + "\" OrderHeaderKey=\"" + strOrderHeaderKey + "\" ProceedOnZeroAmountFailure=\"Y\" />");
          YFCElement eCapturePayment = capturePaymentDoc.getDocumentElement();
          YFCElement eCapturePaymentMethods = capturePaymentDoc.createElement(GCConstants.PAYMENT_METHODS);
          eCapturePayment.appendChild(eCapturePaymentMethods);
          YFCElement eCapturePaymentMethod = capturePaymentDoc.createElement(GCConstants.PAYMENT_METHOD);
          eCapturePaymentMethod.setAttribute(GCConstants.PAYMENT_TYPE, "STORE_CREDIT");
          eCapturePaymentMethod.setAttribute("RequestedAmount", strAmountToBeMaxCharge);
          eCapturePaymentMethod.setAttribute("Operation", "Manage");
          eCapturePaymentMethod.setAttribute("PaymentReference1", strDaxCustomerId);
          eCapturePaymentMethods.appendChild(eCapturePaymentMethod);
          if (LOGGER.isDebugEnabled()) {
            LOGGER.verbose("The capture payment method input" + capturePaymentDoc.toString());
          }
          // invoke capture payment API
          YFCDocument outXML = GCCommonUtil.invokeAPI(env, "capturePayment", capturePaymentDoc, null);
          Element elePaymentMethod =
              (Element) XPathAPI.selectSingleNode(outXML.getDocument(),
                  "//PaymentMethods/PaymentMethod[@PaymentType='STORE_CREDIT']");
          String paymentKey = elePaymentMethod.getAttribute(GCConstants.PAYMENT_KEY);
          LOGGER.verbose("The payment key is --> " + paymentKey);

          // change the refund method
          if (eleYFCRefundPaymentMethods != null) {
            eleYFCRefundPaymentMethods.setAttribute(GCConstants.PAYMENT_KEY, paymentKey);
            eleYFCRefundPaymentMethods.setAttribute(GCConstants.PAYMENT_TYPE, GCConstants.STORE_CREDIT_PAYMENT_TYPE);
            eleYFCRefundPaymentMethods.setAttribute("PaymentReference1", strDaxCustomerId);

            // GCSTORE#4630 -- Starts (stamping refundAmt if isRefundOnAccount is true i.e. COA is
            // chosen as refund type)
            if (isRefundOnAccount && isStoreReturn) {
              eleYFCRefundPaymentMethods.setAttribute("RefundAmount", strAmountToBeMaxCharge);
              paymentKeyList.add(paymentKey);
              removeRefundType = true;
              LOGGER.debug("paymentKeyList @352 : " + paymentKeyList);
            }
            // GCSTORE#4630 -- End
          }
          LOGGER.debug("Exiting the Store Credit append IF");
        }
      } else if (!YFCCommon.isVoid(eleStoreCreditRefundMethod) || (isRefundOnAccount && isStoreReturn)) {
        LOGGER.verbose("Entering the eleStoreCreditRefundMethod exists If block");
        String strMaxChargeLimit =
            GCXMLUtil.getAttributeFromXPath(inDocument,
                "Order/PaymentMethods/PaymentMethod[@PaymentType='STORE_CREDIT']/@MaxChargeLimit");
        String strPaymentKey =
            GCXMLUtil.getAttributeFromXPath(inDocument,
                "Order/PaymentMethods/PaymentMethod[@PaymentType='STORE_CREDIT']/@PaymentKey");
        Double dNewMaxChargeLimit = Double.valueOf(strMaxChargeLimit) + Double.valueOf(strAmountToBeMaxCharge);
        String strChangeOdr =
            "<Order Override='Y' Action='Modify' OrderHeaderKey='" + strOrderHeaderKey
            + "'><PaymentMethods><PaymentMethod PaymentKey='" + strPaymentKey + "' MaxChargeLimit='"
            + Double.toString(dNewMaxChargeLimit) + "'/></PaymentMethods></Order>";
        Document inDocCO = GCXMLUtil.getDocument(strChangeOdr);
        GCCommonUtil.invokeAPI(env, GCConstants.CHANGE_ORDER, inDocCO);
        LOGGER.verbose("The payment key is--> " + strPaymentKey);
        eRefundPaymentMethods =
            (Element) XPathAPI.selectSingleNode(eRootElement, "RefundPaymentMethods/RefundPaymentMethod");
        // change the refund method
        if (eRefundPaymentMethods != null) {
          eRefundPaymentMethods.setAttribute("PaymentKey", strPaymentKey);
          eRefundPaymentMethods.setAttribute(GCConstants.PAYMENT_TYPE, "STORE_CREDIT");
          eRefundPaymentMethods.setAttribute("PaymentReference1", strDaxCustomerId);

          // GCSTORE#4630 -- Starts (stamping refundAmt if isRefundOnAccount is true i.e. COA is
          // chosen as refund type)
          if (isRefundOnAccount && isStoreReturn) {
            eRefundPaymentMethods.setAttribute("RefundAmount", strAmountToBeMaxCharge);
            paymentKeyList.add(strPaymentKey);
            removeRefundType = true;
            LOGGER.debug("paymentKeyList @387 : " + paymentKeyList);
          }
          // GCSTORE#4630 -- End
        }
        LOGGER.verbose("Exiting the elePrepaidPaymentType exists If block");
      }
      // GCSTORE-556 End

      if (isStoreCredit) {
        LOGGER.debug("Entering the Store Credit Refund type exists If block");
        String strPaymentKey = GCXMLUtil
            .getAttributeFromXPath(inDocument,
                "Order/PaymentMethods/PaymentMethod[@PaymentType='STORE_CREDIT']/@PaymentKey");
        LOGGER.debug("The payment key is--> " + strPaymentKey);
        eRefundPaymentMethods = (Element) XPathAPI.selectSingleNode(
            eRootElement,
            "RefundPaymentMethods/RefundPaymentMethod");
        // change the refund method
        if (eRefundPaymentMethods != null) {
          eRefundPaymentMethods.setAttribute("PaymentKey",
              strPaymentKey);
          eRefundPaymentMethods.setAttribute(
              GCConstants.PAYMENT_TYPE, "STORE_CREDIT");
        }
        LOGGER.debug("Exiting the elePrepaidPaymentType exists If block");
      } else {
        String strInDocGetOrderList = "<Order  DocumentType='"
            + GCConstants.RETURN_ORDER_DOCUMENT_TYPE
            + "'><OrderLine DerivedFromOrderHeaderKey='"
            + strOrderHeaderKey + "'/></Order>";
        Document inDocGetOrderList = GCXMLUtil
            .getDocument(strInDocGetOrderList);
        Document indocTemplate = GCXMLUtil
            .getDocument("<OrderList><Order></Order><References><Reference Name='' Value=''/></References></OrderList>");
        Document outDocGetOrderList = GCCommonUtil.invokeAPI(env,
            GCConstants.GET_ORDER_LIST, inDocGetOrderList,
            indocTemplate);
        Element eleReference = GCXMLUtil.getElementByXPath(
            outDocGetOrderList,
            "OrderList/Order/References/Reference");
        if (!YFCCommon.isVoid(eleOpenReturnCharge)
            && !YFCCommon.isVoid(eleReference)) {
          LOGGER.debug("Entering the else if Refund Payment type was Other payment method usinf reference");
          String strPaymenthod = GCXMLUtil
              .getAttributeFromXPath(outDocGetOrderList,
                  "Order/References/Reference[@Name='payMethod']/@Value");
          String strCardLastFour = GCXMLUtil
              .getAttributeFromXPath(outDocGetOrderList,
                  "Order/References/Reference[@Name='cardLastFour']/@Value");
          LOGGER.debug("The strPaymenthod is::" + strPaymenthod
              + "The strCardFourDigit is::" + strCardLastFour);
          String strPaymentKey = GCXMLUtil.getAttributeFromXPath(
              inDocument,
              "Order/PaymentMethods/PaymentMethod[@PaymentType='"
                  + strPaymenthod
                  + "' and @DisplayCreditCardNo='"
                  + strCardLastFour + "']/@PaymentKey");
          LOGGER.debug("The PaymentKey is>>>" + strPaymentKey);
          eRefundPaymentMethods = (Element) XPathAPI
              .selectSingleNode(eRootElement,
                  "RefundPaymentMethods/RefundPaymentMethod");
          // change the refund method
          if (eRefundPaymentMethods != null) {
            eRefundPaymentMethods.setAttribute("PaymentKey",
                strPaymentKey);
            eRefundPaymentMethods.setAttribute(
                GCConstants.PAYMENT_TYPE, "CREDIT_CARD");
          }
        }
      }
      //CR-65 Start
      if(!isRefundOnAccount && !YFCCommon.isVoid(strRefundPaymentKey)){
        eRefundPaymentMethods = (Element) XPathAPI
            .selectSingleNode(eRootElement,
                "RefundPaymentMethods/RefundPaymentMethod");
        if (eRefundPaymentMethods != null) {
          eRefundPaymentMethods.setAttribute("PaymentKey",
              strRefundPaymentKey);
        }
      }
      //CR-65 End

      // GCSTORE#4630 -- Starts (Removing unwanted RefundTypes if RefundType is COA i.e.
      // isRefundOnAccount is true)
      if (removeRefundType) {
        removeExtraRefundType(inDocument, paymentKeyList);
      }
      // GCSTORE#4630 -- Ends
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("The indoc returned is"
            + GCXMLUtil.getXMLString(inDocument));
      }
      LOGGER.verbose("Exiting the confirmRefundDistribution method");
    } catch (Exception e) {
      LOGGER.error("inside GCConfirmRefundDistributionUE.confirmRefundDistribution():",e);
      throw new GCException(e).getYFSUserExitException();
    }
    return inDocument;
  }

  // GCSTORE#4630 -- Starts
  /**
   * This method is used to remove unwanted RefundTypes if RefundType is COA i.e. isRefundOnAccount
   * is true from inDocument.
   *
   * @param inDocument
   * @param paymentKeyList
   */
  private void removeExtraRefundType(Document inDocument, List<String> paymentKeyList) {
    LOGGER.verbose("Entering the removeExtraRefundType method");
    LOGGER.debug("The indoc before removeExtraRefundType is" + GCXMLUtil.getXMLString(inDocument));

    LOGGER.debug("paymentKeyList : " + paymentKeyList);
    // Fix for 4808 : Used List and iterator instead of For Each to fetch RefundPaymentMethod Element.
    List<Element> nlRefundPaymentMethod = GCXMLUtil.getElementListByXpath(inDocument,"Order/RefundPaymentMethods/RefundPaymentMethod");
    Element eleRefundPaymentMethods = (Element) inDocument.getDocumentElement().getElementsByTagName("RefundPaymentMethods").item(0);

    Iterator<Element> itr = nlRefundPaymentMethod.iterator();
    int counter = 0;
    while(itr.hasNext()){
      LOGGER.debug("inside while");
      Element eleRefundPaymentMethod = itr.next();
      if (counter != 0) {
        LOGGER.debug("inside if");
        eleRefundPaymentMethods.removeChild(eleRefundPaymentMethod);
      }
      counter++;
    }

    LOGGER.debug("The indoc after removeExtraRefundType is" + GCXMLUtil.getXMLString(inDocument));
    LOGGER.verbose("Exiting the removeExtraRefundType method");
  }
  // GCSTORE#4630 -- Ends

}
