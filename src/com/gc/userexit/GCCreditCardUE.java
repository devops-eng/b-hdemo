/**
 * Copyright 2014 Guitar Center. All rights reserved. Guitar Center PROPRIETARY/CONFIDENTIAL. Use is
 * subject to license terms.
 * ########################################################################
 * ################################################################################## OBJECTIVE:
 * This class defines the Credit Card payment group processing.
 * #####################################
 * #############################################################
 * ######################################################## Version Date Modified By CR Description
 * #################################################################################################
 * ######################################################### 1.0 02/04/2014 Soni, Karan Initial
 * Version Credit Card UE.. 1.1 06/24/2014 Soni, Karan OMS-2500/CR-38 to remove CVV no after the
 * success authorization 1.2 10/01/2014 Chopra, Inderpreet OMS-3814 Offline Authorizations Show as
 * Declined instead of "Call for Auth" 1.3 10/06/2014 Soni, Karan OMS-3849 OMS to retrieve and store
 * the Discover Network Transaction ID returned during authorization response from AJB and pass onto
 * DAX during invoicing for it to use in the corresponding settlement record 1.4 03/10/2014 Chopra,
 * Inderpreet OMS-3946 Present the correct message for payment processing issues *
 * 
 *  1.1               05/16/2017        Gosai, Pinky      GCSTORE-6851: Prod - Declined WEB orders not sending Decline Letters
 *  
 * ##################
 * ################################################################################
 * ######################################################
 */

package com.gc.userexit;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.constants.GCErrorConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCPaymentUtils;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.date.YTimestamp;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;
import com.yantra.yfs.japi.YFSExtnPaymentCollectionInputStruct;
import com.yantra.yfs.japi.YFSExtnPaymentCollectionOutputStruct;
import com.yantra.yfs.japi.YFSUserExitException;

public class GCCreditCardUE implements
com.yantra.yfs.japi.ue.YFSCollectionCreditCardUE {

 // static Date currentDate = new Date();

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCCreditCardUE.class);

  @Override
  public YFSExtnPaymentCollectionOutputStruct collectionCreditCard(YFSEnvironment env,
      YFSExtnPaymentCollectionInputStruct inStruct) throws YFSUserExitException {
  

    YFSExtnPaymentCollectionOutputStruct outStruct = new YFSExtnPaymentCollectionOutputStruct();
    try {
      LOGGER.verbose("Entering the collectionCreditCard method");
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("com.gc.userexit.GCCreditCardUE.SecureAuthCode" + inStruct.secureAuthenticationCode);
        printInStruct(inStruct);
      }
      
      
      
     
      String strChargeTxnKey=inStruct.chargeTransactionKey;
      
      
      String strChargeType = inStruct.chargeType;
      if (GCConstants.PAYMENT_STATUS_AUTHORIZATION.equals(strChargeType)) {
        LOGGER.debug("Entering If in the case of Authorization charge.");
        Document paymentResponseDoc = null;
        Document getOrderListOpDoc = getOrderListOpCCAuth(env, inStruct);
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("The output of getOrderList API" + GCXMLUtil.getXMLString(getOrderListOpDoc));
        }
        // To handle the new auth generated in case of Partial shipment
        LOGGER.debug("Entering the actual authorization else");
        // Begin: Manual authorization logic
        String sPaymentReference4 = inStruct.paymentReference4;
        if (!YFCCommon.isVoid(sPaymentReference4)
            && ("CREDIT_CARD".equals(inStruct.paymentType) || "PRIVATE_LABEL_CARD".equals(inStruct.paymentType) 
                    || GCConstants.PVT_LBL_CREDIT_CARD.equals(inStruct.paymentType))) {
          /* COA PAYMENT PROCESSING Start*/
          //Add logic to update for dummy auth for reverse auth call
          if(inStruct.requestAmount < 0){
            outStruct = GCPaymentUtils.updateOutStructForAuthRev(inStruct, outStruct);
          }else{
            outStruct = processManualAuth(env, outStruct, inStruct);
          }
          /* COA PAYMENT PROCESSING End*/
        } else {
          // End: Manual authorization logic
            if (inStruct.requestAmount < 0) {
                outStruct = GCPaymentUtils.updateOutStructForAuthRev(inStruct, outStruct);
            } else {
                paymentResponseDoc = GCPaymentUtils.processPayment(env,inStruct);
                if (YFCCommon.isVoid(paymentResponseDoc)) {
                    outStruct = processAJBVoidResponse(env, outStruct,paymentResponseDoc, getOrderListOpDoc,inStruct);
                        
                } else {
                    LOGGER.debug("Entering the Payment response received IF");
                    if (LOGGER.isDebugEnabled()) {
                        LOGGER.debug("The response doc -->"
                                + GCXMLUtil
                                .getXMLString(paymentResponseDoc));
                    }
                    outStruct = processCCAuthOutput(env,paymentResponseDoc, outStruct, inStruct,getOrderListOpDoc);
                    
                    
                    
                    
                    //GCSTORE-5516-starts
                    
                    String strAuthId=outStruct.authorizationId;
                    String strPaymentKey=inStruct.paymentKey;
                    String strSuspendPayment=outStruct.suspendPayment;
                    if (LOGGER.isDebugEnabled()) {
                        LOGGER.debug("Outstruct Authorization ID::"+strAuthId);
                    }
                    
                    /**
                     * GCSTORE-6851 : Calling GCPaymentDeclineEmailService when response from 
                     * 				  AJB is declined.
                     * start
                     */
                    String strProgId = env.getProgId();
                    LOGGER.debug("strProgId " + strProgId);
                    
                    if(!YFCCommon.isVoid(strAuthId) && "DECLINED".equalsIgnoreCase(strAuthId) && !YFCCommon.isVoid(strPaymentKey) && !GCConstants.PAYMENT_CALL_CALL_CENTER.equalsIgnoreCase(strProgId))
                    {
                    	LOGGER.debug("Declined scenario not via call center");
                    	
                    	Document docGetOrderListOp=getOrderListOp(env, inStruct);
                    	Element eleOrder=docGetOrderListOp.getDocumentElement();
                    	//add for current payment type 
                
                    
                    	if (LOGGER.isDebugEnabled()) 
                    	{
                    		LOGGER.debug("Instruct Payment Key::"+strPaymentKey);
                    		LOGGER.debug("docGetOrderListOp :: \n"+GCXMLUtil.getXMLString(docGetOrderListOp));
                    	}
                    	
                    	// Sending CC Decline message to customer
                    	GCPaymentUtils.prepareAndSendCardDeclineEmail(env,eleOrder,strAuthId, strSuspendPayment);
                        LOGGER.debug("After sending decline message");
                        
                        // Handling Reauth scenario
                        {
                        	LOGGER.debug("Checking for Reauth");
                        	
                        	NodeList nlhargeTypeAuth=GCXMLUtil.getNodeListByXpath(docGetOrderListOp, "//Order/ChargeTransactionDetails/ChargeTransactionDetail[@PaymentKey='"+strPaymentKey +"'][@ChargeType='AUTHORIZATION']");
                        	LOGGER.debug("Node List nlhargeTypeAuth size:: "+nlhargeTypeAuth.getLength());
                        	if(nlhargeTypeAuth.getLength()>1)
                    		{
                        		for (int i = 0; i < nlhargeTypeAuth.getLength(); i++) 
                    		    {
                        			Element eleChargeTypeAuth=(Element) nlhargeTypeAuth.item(i);
                        			LOGGER.debug(" eleChargeTypeAuth " + GCXMLUtil.getElementString(eleChargeTypeAuth));
                        			LOGGER.debug(" strChargeTxnKey " + strChargeTxnKey);
                        			if(!YFCCommon.isVoid(eleChargeTypeAuth.getAttribute("AuthorizationID")) && !strChargeTxnKey.equals(eleChargeTypeAuth.getAttribute("ChargeTransactionKey")))                      
                    		        {
                    		    		env.setTxnObject("ReauthEmail", strPaymentKey);
                    		    		LOGGER.debug("This is a Reauth Case");
                		    			LOGGER.debug("Reauth Env Object is ::"+(String)env.getTxnObject("ReauthEmail"));
                    		    		
                		    			LOGGER.debug("MaxOrderStatus is "+ eleOrder.getAttribute("MaxOrderStatus"));
                    		    		
                    		    		//GCSTORE-4795
                    		            if(Double.parseDouble(eleOrder.getAttribute("MaxOrderStatus"))>=3200)
                    		            {
                    		            	LOGGER.debug("Inside if where MaxOrderStatus>=3200");
                    		            			
                    		            	outStruct.holdOrderAndRaiseEvent = true;
                    		                outStruct.holdReason = "REAUTH_FAILED_HOLD";
                    		            } // End of if (Double.parseDouble(eleOrder.getAttribute("MaxOrderStatus"))>=3200)
                    		            
                    		            break;
                    		    	} // End of if(!YFCCommon.isVoid(eleChargeTypeAuth.getAttribute("AuthorizationID"))
                    		    }// End of for nlhargeTypeAuth
                    		} // End of if if(nlhargeTypeAuth.getLength()>1)
                        } // End of Rauth scenario
                        
                        
                    } // End of outer  if(!YFCCommon.isVoid(strAuthId) && "DECLINED".equalsIgnoreCase(strAuthId) 
                    //GSTORE-5516-ends
                    
                    // GCSTOE-6851 ends
                    
                    LOGGER.debug("Exiting the Payment response received IF");
                }
                LOGGER.debug("Exiting the actual authorization else");
            }
        }
        LOGGER.debug("Exiting the Auth main IF block");
      } else if (GCConstants.PAYMENT_STATUS_CHARGE.equals(strChargeType)) {
    	LOGGER.debug("Entering If in case of Charge Type as CHARGE");
    	
    	if(LOGGER.isDebugEnabled()){
    	   	printInStruct(inStruct);
    	}
		
		boolean enable_1236=getCommonCode(env);
    	LOGGER.debug("boolean enable_new_code is"+enable_1236); 
    	YFSExtnPaymentCollectionOutputStruct outstructFromAuth = null;
    	if(enable_1236 && inStruct.requestAmount > 0){
    	// GCSTORE 1236 Fix -- BEGIN
    		LOGGER.debug("Entering GCSTORE 1236 FIX");  
	    	  
    		boolean proceedForCharge= true;
	          	          
	        if((YFCCommon.equals(inStruct.authorizationId, GCConstants.DECLINED) || YFCCommon.isVoid(inStruct.authorizationId)) && (
          		YFCCommon.equals(inStruct.paymentType, GCConstants.CREDIT_CARD) || YFCCommon.equals(inStruct.paymentType, GCConstants.PRIVATE_LABEL_CARD) ||
          		YFCCommon.equals(GCConstants.PVT_LBL_CREDIT_CARD,inStruct.paymentType))){
	        	outstructFromAuth =  processAuthBeforeCharge(env, inStruct);
	  	          
	        	if(YFCCommon.equals(outstructFromAuth.authReturnMessage,GCConstants.AUTH_SUCCESS_MSG)){
	  	        	// In case of AUTH success  
	  	        	inStruct.authorizationId = outstructFromAuth.authorizationId;
	        	}else {
	  	        	// In case of Hard decline and soft decline
	        		Date currentDate= new Date();
	  	        	  
	  	        	proceedForCharge = false;  
	  	        	  
	  	        	outStruct.authorizationId = outstructFromAuth.authorizationId;
  		            outStruct.authReturnMessage = outstructFromAuth.authReturnMessage;
  		            outStruct.authReturnCode = outstructFromAuth.authReturnCode;
	  	        	outStruct.PaymentTransactionError = outstructFromAuth.PaymentTransactionError;
	  	        	outStruct.suspendPayment = outstructFromAuth.suspendPayment;
	  	        	outStruct.tranAmount = GCConstants.ZEROAMOUNT;
	  	        	outStruct.tranReturnMessage = outstructFromAuth.tranReturnMessage;
	  	        	outStruct.tranType = GCConstants.PAYMENT_STATUS_CHARGE;
	  	        	outStruct.authorizationAmount = GCConstants.ZEROAMOUNT;
	  	        	outStruct.retryFlag = GCConstants.NO;
	  	        	outStruct.holdOrderAndRaiseEvent = GCConstants.FALSE;
	  	        	outStruct.tranRequestTime = GCDateUtils.convertDate(currentDate);
	  	          }
	  	        	  
	        }
	          LOGGER.debug("boolean proceedForCharge is"+proceedForCharge); 
	          if(proceedForCharge){
	            // update this method to pass values following values in list: internalReturnMessage, requestID - in blow method i.e updateDummyOutstruct, add condition to fetch these attributes from List or Order details
	            outStruct = GCPaymentUtils.updateDummyOutstruct(env, inStruct,outstructFromAuth);
	          }
    	}else{
    		outStruct = GCPaymentUtils.updateDummyOutstruct(env, inStruct,outstructFromAuth);
    	}
          // GCSTORE 1236 Fix -- END
          LOGGER.debug("Exiting If in case of Charge Type as CHARGE");
      }
      LOGGER.verbose("Exiting the collectionCreditCard method");
      LOGGER.verbose("The outstruct of collectionCreditCard method");
      if(LOGGER.isDebugEnabled()){
    	  printOutStruct(outStruct);
      }
    } catch (Exception e) {

      LOGGER.error("Inside GCCreditCardUE.collectionCreditCard():", e);
      throw new GCException(e).getYFSUserExitException();
    }
    
    return outStruct;
  }
  
  

  

private boolean getCommonCode(YFSEnvironment env) {
	// TODO Auto-generated method stub
	  YFCDocument yfcIndoc = YFCDocument.createDocument("CommonCode");
	    YFCElement eleRoot = yfcIndoc.getDocumentElement();
	    eleRoot.setAttribute(GCConstants.CODE_TYPE, "EXTN_1236_CODE");
	    eleRoot.setAttribute(GCConstants.CODE_VALUE, "EXTN_1236_CODE_ENABLE");
	    
	    YFCDocument yfcgetGetCommonCodeTemp =YFCDocument.
	    		getDocumentFor("<CommonCodeList> <CommonCode/> </CommonCodeList>");
	    
	    YFCDocument yfcgetCommonCodeListOutput =
	            GCCommonUtil.invokeAPI(env, GCConstants.GET_COMMON_CODE_LIST, yfcIndoc,yfcgetGetCommonCodeTemp);
	    
	    Element eleCommonCode =
	            GCXMLUtil.getElementByXPath(yfcgetCommonCodeListOutput.getDocument(),
	                "CommonCodeList/CommonCode");
	    
	    String enable_new_code = eleCommonCode.getAttribute("CodeShortDescription");
	    if(YFCCommon.equals(enable_new_code, "Y")){
	    	return true;
	    }
	    else{
	    	return false;
	    }
	  
	
}


// This method is written for GCSTORE -- 1236
public YFSExtnPaymentCollectionOutputStruct processAuthBeforeCharge(YFSEnvironment env,
	      YFSExtnPaymentCollectionInputStruct inStruct) throws GCException {

	    LOGGER.debug("Entering processAuthBeforeCharge");
	    if(LOGGER.isDebugEnabled()){
	    	printInStruct(inStruct);
	    }
	    Document paymentResponseDoc = null;
	    Document getOrderListOpDoc = getOrderListOpCCAuth(env, inStruct);
	    if (LOGGER.isDebugEnabled()) {
	      LOGGER.debug("The output of getOrderList API" + GCXMLUtil.getXMLString(getOrderListOpDoc));
	    }
	    
	    paymentResponseDoc = GCPaymentUtils.processPayment(env,inStruct);
	    if (LOGGER.isDebugEnabled()) {
		      LOGGER.debug("The payment response doc is" + GCXMLUtil.getXMLString(paymentResponseDoc));
		}
	    
	    YFSExtnPaymentCollectionOutputStruct outStruct = new YFSExtnPaymentCollectionOutputStruct();
	    
	    /*if (YFCCommon.isVoid(paymentResponseDoc)) {
	      outStruct = processAJBVoidResponse(env, outStruct, paymentResponseDoc, getOrderListOpDoc, inStruct);
	    }*/ 
	    LOGGER.debug("Entering the Payment response received IF");
	    if (LOGGER.isDebugEnabled()) {
	    	LOGGER.debug("The response doc -->"+ GCXMLUtil.getXMLString(paymentResponseDoc));
	    }
	      
	      
	    outStruct = processCCAuthOutput(env,paymentResponseDoc, outStruct, inStruct,getOrderListOpDoc);
	    LOGGER.debug("Exiting the Payment response received IF");
	    
	    
	    
	    LOGGER.debug("Exiting processAuthBeforeCharge");
	   
	    if(LOGGER.isDebugEnabled()){
	    	printOutStruct(outStruct);
	    }
	    return outStruct;

	  }

/**
   *
   * @param env
   * @param outStruct
   * @param inStruct
   * @return
   * @throws Exception
   */
  private YFSExtnPaymentCollectionOutputStruct processManualAuth(YFSEnvironment env,
      YFSExtnPaymentCollectionOutputStruct outStruct, YFSExtnPaymentCollectionInputStruct inStruct) throws GCException {

    outStruct.tranAmount = inStruct.requestAmount;
    outStruct.authorizationId = inStruct.paymentReference4;
    outStruct.authorizationAmount = inStruct.requestAmount;
    outStruct.holdOrderAndRaiseEvent = false;
    outStruct.holdReason = GCConstants.BLANK;
    outStruct.retryFlag = GCConstants.NO;
    outStruct.authorizationExpirationDate = GCPaymentUtils.getAuthExpiryDate(env, inStruct);
    return outStruct;
  }

  /**
   *
   * @param env
   * @param inStruct
   * @return
   * @throws Exception
   */
  public static Document getOrderListOp(YFSEnvironment env, YFSExtnPaymentCollectionInputStruct inStruct)
      throws GCException {
    LOGGER.verbose("Entering the getOrderListOpmethod");
    Document getOrderListOp;
    Document getOrderListOpDoc = null;
    try {
      getOrderListOp = GCCommonUtil.invokeGetOrderListAPITemp(env,
          inStruct.orderHeaderKey,
          GCConstants.GC_CC_AUTH_GET_ORDER_LIST_TEMPLATE);
      getOrderListOpDoc = GCXMLUtil
          .getDocumentFromElement((Element) getOrderListOp.getDocumentElement().getFirstChild());
    }catch (Exception e) {
      throw new GCException(e);
    }
    LOGGER.verbose("Exiting the getOrderListOpmethod");
    return getOrderListOpDoc;
  }

  /**
   *
   * This method proceses the payment response and updates the outstruct.
   *
   * @param env
   * @param paymentOutputDoc
   * @param outStruct
   * @param inStruct
   * @param getOrderListOpDoc
   * @return
   * @throws Exception
   *
   */
  public static YFSExtnPaymentCollectionOutputStruct processCCAuthOutput(YFSEnvironment env, Document paymentOutputDoc,
      YFSExtnPaymentCollectionOutputStruct outStruct, YFSExtnPaymentCollectionInputStruct inStruct,
      Document getOrderListOpDoc) throws GCException {
    try {
      Date currentDate = new Date();
      LOGGER.verbose("Entering the processCCAuthOutput method");
      String strProgId = env.getProgId();
      boolean callcenterProgId = false;
      if (GCConstants.PAYMENT_CALL_CALL_CENTER.equalsIgnoreCase(strProgId)) {
        callcenterProgId = true;
      }
      if (!YFCCommon.isVoid(paymentOutputDoc)) {
        Element eleRoot = paymentOutputDoc.getDocumentElement();
        Element eleActionCode = (Element) XPathAPI.selectSingleNode(eleRoot, GCConstants.IX_ACTION_CODE);
        String strAuthResponse = eleActionCode.getTextContent();
        // OMS-3814 START
        Element eleRespCode = (Element) XPathAPI.selectSingleNode(
            eleRoot, GCConstants.IX_ISO_RESP);
        String strIsoResp = eleRespCode!=null ? eleRespCode.getTextContent():"";
        //OMS-3814 END
        String strCCExpDate = GCPaymentUtils.getAuthExpiryDate(env, inStruct);
        LOGGER.debug("The auth exp dat is" + strCCExpDate);
        Element eleAuthorizationId = (Element) XPathAPI.selectSingleNode(eleRoot, GCConstants.IX_AUTH_CODE);
        String strAuthorizationId = eleAuthorizationId.getTextContent();
        Element eleAuthAmt = (Element) XPathAPI.selectSingleNode(eleRoot, GCConstants.IX_AMOUNT);
        String strAuthAmt = eleAuthAmt.getTextContent();
        strAuthAmt = GCPaymentUtils.convertAuthReturnedAmount(strAuthAmt);
        Element eleDate = (Element) XPathAPI.selectSingleNode(eleRoot, GCConstants.IX_DATE);
        String strDate = eleDate.getTextContent();
        Element eleTime = (Element) XPathAPI.selectSingleNode(eleRoot, GCConstants.IX_TIME);
        String strTime = eleTime.getTextContent();
        strDate = GCDateUtils.convertDateFormat(strDate);
        strTime = GCDateUtils.convertTimeFormat(strTime);
        String strAuthDateTime = strDate + strTime;
        LOGGER.debug("The auth Time is " + strAuthDateTime);
        Element eleOptions = (Element) XPathAPI.selectSingleNode(eleRoot, GCConstants.IX_OPTIONS);
        String strOptions = eleOptions.getTextContent();
        String strRefNumber = "";
        // Fetching IxReference No in Credit Card and Private Label Card
        // payment type and stamping it in outStruct AuthCode, which
        // will be published to DAX for charging the customer
        if (GCConstants.CREDIT_CARD.equals(inStruct.paymentType)
            || GCConstants.PRIVATE_LABEL_CARD.equals(inStruct.paymentType) || GCConstants.PVT_LBL_CREDIT_CARD.equals(inStruct.paymentType)) {
          Element eleRefNumber = (Element) XPathAPI.selectSingleNode(eleRoot, GCConstants.IX_REFERENCE_NUMBER);
          strRefNumber = eleRefNumber.getTextContent();
        }
        // OMS-3849 Start
        String strIxPS2000 = "";
        // Fix for GCSTORE-5975 : Removing check for CreditCard Type as Discover Card
        if (GCConstants.CREDIT_CARD.equals(inStruct.paymentType)) {
          Element elePS2000 = (Element) XPathAPI.selectSingleNode(eleRoot, GCConstants.IX_PS_2000);
          if(!YFCCommon.isVoid(elePS2000)){
              strIxPS2000 = elePS2000.getTextContent();
          }
        }
        // OMS-3849 End
        // placed null check
        int iAVS = -1;
        int iCVV = -1;
        if (!YFCCommon.isStringVoid(strOptions)) {
          String strAVS = "AvsResp-";
          String strCVV = "CvvResp-";
          iAVS = strOptions.lastIndexOf(strAVS);
          iCVV = strOptions.lastIndexOf(strCVV);
        }
        Element eleAuthReturnMsg = (Element) XPathAPI.selectSingleNode(eleRoot, GCConstants.AUTH_RETURN_MSG);
        String strAuthReturnMsg = eleAuthReturnMsg.getTextContent();
        if (GCConstants.PAYMENT_AUTHORIZED.equals(strAuthResponse)) {
          LOGGER.debug("Entering If Response is PAYMENT AUTHORIZED");
          outStruct.tranRequestTime = GCDateUtils.convertDate(currentDate);
          outStruct.authTime = strAuthDateTime;
          outStruct.authorizationId = strAuthorizationId;
          // If for payment type common code entry is blank for auth
          // expiry then strCCExpDate will come blank and we will
          // stamp High date as Auth Exp Date
          if (!inStruct.paymentType.equals(GCConstants.GIFT_CARD)) {
            if (!YFCCommon.isVoid(strCCExpDate)) {
              outStruct.authorizationExpirationDate = strCCExpDate;
            } else {
              outStruct.authorizationExpirationDate = YTimestamp.HIGH_DATE.toString();
            }
          }
          outStruct.authReturnCode = strAuthResponse;
          outStruct.authReturnMessage = GCConstants.AUTH_SUCCESS_MSG;
          outStruct.tranReturnMessage = strAuthReturnMsg;
          outStruct.authorizationAmount = Double.parseDouble(strAuthAmt);
          outStruct.holdOrderAndRaiseEvent = GCConstants.FALSE;
          outStruct.suspendPayment = GCConstants.NO;
          outStruct.tranType = inStruct.chargeType;
          outStruct.tranAmount = inStruct.requestAmount;
          // Updating the IxRefNumber to be posted to DAX on Invoice
          
          outStruct.internalReturnMessage = strRefNumber;
          // OMS-3849 Start
          outStruct.requestID = strIxPS2000;
          // OMS-3849 End
          // if response for AVS and CVV received
          if (iAVS >= 0) {
            outStruct.authAVS = String.valueOf(strOptions.charAt(iAVS + 8));
          }
          if (iCVV >= 0) {
            outStruct.sCVVAuthCode = String.valueOf(strOptions.charAt(iCVV + 8));
          }
          // Removing the CVV code after the successful authorization
          Document inDocRemoveCVV = removeCVVData(inStruct);
          Element elePaymentMethod = GCXMLUtil.getElementByXPath(inDocRemoveCVV, "Order/PaymentMethods/PaymentMethod");
          if (!YFCCommon.isVoid(elePaymentMethod)) {
            if (LOGGER.isDebugEnabled()) {
              LOGGER.debug("The changeOrder xml is" + GCXMLUtil.getXMLString(inDocRemoveCVV));
            }
            GCCommonUtil.invokeService(env, GCConstants.GC_REMOVE_CVV, inDocRemoveCVV);
          }
          LOGGER.debug("Exiting If Response is PAYMENT AUTHORIZED");
          return outStruct;
        } else if (GCConstants.PAYMENT_DECLINED.equals(strAuthResponse) || callcenterProgId) {
          LOGGER.debug("Entering IF Response is PAYMENT HARD DECLINED");
          outStruct.tranRequestTime = GCDateUtils.convertDate(currentDate);
          outStruct.authTime = GCConstants.BLANK;
          outStruct.authorizationId = GCConstants.DECLINED;
          outStruct.tranReturnMessage = strAuthReturnMsg;
          outStruct.tranType = GCConstants.PAYMENT_STATUS_AUTHORIZATION;
          if (!GCConstants.GIFT_CARD.equals(inStruct.paymentType)) {
            outStruct.authorizationExpirationDate = YTimestamp.HIGH_DATE.toString();
          }
          outStruct.authReturnCode = strAuthResponse;
          outStruct.authReturnMessage = GCConstants.DECLINED;
          outStruct.authorizationAmount = GCConstants.ZEROAMOUNT;
          outStruct.holdOrderAndRaiseEvent = GCConstants.FALSE;

          outStruct.tranAmount = GCConstants.ZEROAMOUNT;

          // OMS-3814 Start
          //OMS-4704 Start
          boolean isReferral= false;
          if (GCConstants.OFFLINE.equalsIgnoreCase(strIsoResp)
              && GCConstants.PAYMENT_CALL_FOR_AUTH.equals(strAuthResponse)) {
            LOGGER.debug("Offline scenario");
            outStruct.PaymentTransactionError =
                setPayTrnxError("PAYMENT_FAILURE", GCConstants.PAYMENT_COMMUNICATION_FAILURE_MSG);

          } else if ((GCConstants.REFERRAL_CODE.equalsIgnoreCase(strIsoResp) && GCConstants.PAYMENT_CALL_FOR_AUTH
              .equals(strAuthResponse))) {
            LOGGER.debug("Referral scenario");
            outStruct.PaymentTransactionError = setPayTrnxError(
                "PAYMENT_FAILURE",
                GCConstants.PAYMENT_REFERRAL_MSG);
            isReferral= true;
            outStruct.suspendPayment = GCConstants.YES;
          } else if ((GCConstants.REENTER_CODE.equalsIgnoreCase(strIsoResp) && GCConstants.PAYMENT_CALL_FOR_AUTH
              .equals(strAuthResponse))) {
            LOGGER.debug("Reenter scenario");
            outStruct.PaymentTransactionError = setPayTrnxError(
                "PAYMENT_FAILURE",
                GCConstants.PAYMENT_REENTER_MSG);
            isReferral= true;
            outStruct.suspendPayment = GCConstants.YES;
          } else {
            outStruct.PaymentTransactionError = setPayTrnxError("PAYMENT_FAILURE", GCConstants.PAYMENT_DECLINE_MSG);
          }
          Date currentDate1 = new Date(System.currentTimeMillis()+5*60*1000);
          String strOrderDate = new SimpleDateFormat(
              GCConstants.DATE_TIME_FORMAT).format(currentDate1);
          SimpleDateFormat formatter = new SimpleDateFormat(
              GCConstants.DATE_TIME_FORMAT);
          Date dtOrderDate = formatter.parse(strOrderDate);
          outStruct.collectionDate= dtOrderDate;
          // raise alert only in case of hard decline
       // Fix for 4925 : Draft Orders should not generate payment alerts
          String sDraftOrderFlag = getOrderListOpDoc.getDocumentElement().getAttribute(GCConstants.DRAFT_ORDER_FLAG);
          if (GCConstants.PAYMENT_DECLINED.equals(strAuthResponse)) {
            outStruct.suspendPayment = GCConstants.YES;
            if(!YFCCommon.equals(sDraftOrderFlag, GCConstants.YES)){            // Fix for 4925
                GCCommonUtil.invokeService(env, GCConstants.PAYMENT_HARD_DECLINED_ALERT, getOrderListOpDoc);
            }
          }
          if(isReferral){
              if(!YFCCommon.equals(sDraftOrderFlag, GCConstants.YES)){     // Fix for 4925
                  GCCommonUtil.invokeService(env, GCConstants.PAYMENT_ALERT, getOrderListOpDoc);
              }
          }
          //OMS-4704 End
          // OMS- 3814 End
          return outStruct;
        } else {
          LOGGER.debug("Entering the If in the else case");
          outStruct= processSoftDecline(env, inStruct, outStruct, getOrderListOpDoc, strAuthDateTime, strAuthReturnMsg, strAuthResponse, strIsoResp, GCConstants.FALSE);
          return outStruct;
        }
      } else {
        YFSException ex = new YFSException();
        ex.setErrorCode(GCErrorConstants.PAYMENT_SERVER_DOWN_CODE);
        ex.setErrorDescription(GCErrorConstants.PAYMENT_SERVER_DOWN_DESCRIPTION);
        throw ex;
      }
    } catch (Exception e) {
      LOGGER.error("Inside GCCreditCardUE.processCCAuthOutput():", e);
      throw new GCException(e);
    }
  }

 
  
  
  
  /**
   * Returning the Payment method Element from the output of getOrderList API.
   *
   * @param env
   * @param getOrderListOpDoc
   * @return
   * @throws Exception
   */
  public Element getpaymentMethodEle(YFSEnvironment env, Document getOrderListOpDoc,
      YFSExtnPaymentCollectionInputStruct inStruct) throws GCException {
    Element paymentMethodEle = null;
    boolean notNullEle = false;
    Date actualCurrentDate = new Date();
    try {
      LOGGER.verbose("[GCCreditCardUE] :getpaymentMethodEle() ------ Start");
      if (LOGGER.isDebugEnabled()) {
        LOGGER
        .debug("[GCCreditCardUE] :getpaymentMethodEle() input doc is" + GCXMLUtil.getXMLString(getOrderListOpDoc));
      }
      String strPaymentKey = inStruct.paymentKey;
      paymentMethodEle =
          GCXMLUtil.getElementByXPath(getOrderListOpDoc, "Order/PaymentMethods/PaymentMethod[@PaymentKey='"
              + strPaymentKey + "']");
      String paymenType = paymentMethodEle.getAttribute(GCConstants.PAYMENT_TYPE);
      String suspendAnyMoreCharges = paymentMethodEle.getAttribute(GCConstants.SUS_ANY_MORE_CHARGES);
      LOGGER.debug("Payment Type" + paymenType + "   Suspend Charges Flag" + suspendAnyMoreCharges);

      if ((paymenType.equalsIgnoreCase(GCConstants.CREDIT_CARD) || paymenType.equalsIgnoreCase(GCConstants.PLCC) || paymenType
          .equalsIgnoreCase(GCConstants.GIFT_CARD)) && suspendAnyMoreCharges.equalsIgnoreCase(GCConstants.NO)) {
        String cardExpiryDate = paymentMethodEle.getAttribute(GCConstants.CREDIT_CARD_EXPIRY_DATE);

        if (!YFCCommon.isVoid(cardExpiryDate)) {
          String strYY = cardExpiryDate.substring(3, 5);
          String strMM = cardExpiryDate.substring(0, 2);
          cardExpiryDate = strMM + strYY;
          SimpleDateFormat sdf2 = new SimpleDateFormat(GCConstants.CC_EXPIRATION_DATE_FORMAT);
          String ccExpiDate = sdf2.format(sdf2.parse(cardExpiryDate));
          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Expiration date" + ccExpiDate);
          }
          String currentDateString = sdf2.format(actualCurrentDate);
          if (sdf2.parse(ccExpiDate).after(sdf2.parse(currentDateString))
              || sdf2.parse(ccExpiDate).equals(sdf2.parse(currentDateString))) {
            LOGGER.debug("Checking for credit card expiry");
            notNullEle = true;
          }
        }
      }
    } catch (Exception e) {
      LOGGER.error("Inside GCCreditCardUE.processCCAuthOutput():", e);
      throw new GCException(e);
    }
    if (notNullEle) {
      return paymentMethodEle;
    } else {
      return null;
    }
  }

  private static Document setPayTrnxError(final String strMsgType, final String strMsg) throws GCException {
    Document docPayTrnErrorLst = null;
    try {
      docPayTrnErrorLst = GCXMLUtil.createDocument("PaymentTransactionErrorList");
      final Element elePayTrnxErrLst = docPayTrnErrorLst.getDocumentElement();
      final Element elePayTrnxErr = GCXMLUtil.createElement(docPayTrnErrorLst, "PaymentTransactionError", "");
      elePayTrnxErrLst.appendChild(elePayTrnxErr);
      elePayTrnxErr.setAttribute("MessageType", strMsgType);
      elePayTrnxErr.setAttribute("Message", strMsg);
    } catch (Exception ex) {
      LOGGER.error("Inside GCCreditCardUE.setPayTrnxError():", ex);
      throw new GCException(ex);
    }
    
    if (LOGGER.isDebugEnabled()) {
    	LOGGER.debug("Exiting setPayTrnxError");
        LOGGER.debug("The docPayTrnErrorLst in setPayTrnxError is :" + GCXMLUtil.getXMLString(docPayTrnErrorLst));
      }
    
    return docPayTrnErrorLst;
    
  }

  private static Document removeCVVData(YFSExtnPaymentCollectionInputStruct inStruct) throws GCException {
    LOGGER.verbose("Entering the removeCVVData method");
    Document inDoc = null;
    try {
      inDoc = GCXMLUtil.getDocument("<Order OrderHeaderKey='" + inStruct.orderHeaderKey + "' />");
      Element eleOrder = inDoc.getDocumentElement();
      Element elePaymentMethods = inDoc.createElement(GCConstants.PAYMENT_METHODS);
      eleOrder.appendChild(elePaymentMethods);
      String strPaymentType = inStruct.paymentType;
      if ((GCConstants.CREDIT_CARD.equals(strPaymentType) || GCConstants.PRIVATE_LABEL_CARD.equals(strPaymentType) || GCConstants.PVT_LBL_CREDIT_CARD.equalsIgnoreCase(strPaymentType))
          && (!YFCCommon.isVoid(inStruct.svcNo))) {
        LOGGER.debug("Entering the if of Credit card or Private Label Card");
        Element elePaymentMethod = inDoc.createElement(GCConstants.PAYMENT_METHOD);
        elePaymentMethods.appendChild(elePaymentMethod);
        elePaymentMethod.setAttribute(GCConstants.PAYMENT_KEY, inStruct.paymentKey);
      }
      LOGGER.verbose("Exiting the removeCVVData method");
    } catch (Exception e) {
      LOGGER.error("Inside GCCreditCardUE.removeCVVData():", e);
      throw new GCException(e);
    }
    return inDoc;
  }

  /**
   * This method invokes getOrderList and return xml with Order as parent element
   *
   * @param env
   * @param inStruct
   * @return
   * @throws Exception
   */
  public static Document getOrderListOpCCAuth(YFSEnvironment env, YFSExtnPaymentCollectionInputStruct inStruct)
      throws GCException {
    LOGGER.verbose("Entering the getOrderListOpmethod");
    Document getOrderListOp;
    Document getOrderListOpDoc = null;
    try {
      getOrderListOp =
          GCCommonUtil.invokeGetOrderListAPITemp(env, inStruct.orderHeaderKey,
              GCConstants.GC_CC_AUTH_GET_ORDER_LIST_TEMPLATE_WITHOUT_CHARGE_TRASACTION_DEATIL);
      getOrderListOpDoc =
          GCXMLUtil.getDocumentFromElement((Element) getOrderListOp.getDocumentElement().getFirstChild());
    } catch (Exception e) {
      throw new GCException(e);
    }
    LOGGER.verbose("Exiting the getOrderListOpmethod");
    return getOrderListOpDoc;
  }

  /**
   * The method does the processing when the payment response document is null(AJB is down).
   * @param env
   * @param outStruct
   * @param docPaymentResponse
   * @param getOrderListOpDoc
   * @return
   */
  public static YFSExtnPaymentCollectionOutputStruct processAJBVoidResponse(YFSEnvironment env, YFSExtnPaymentCollectionOutputStruct outStruct, Document docPaymentResponse, Document getOrderListOpDoc, YFSExtnPaymentCollectionInputStruct inStruct)throws GCException{
    try{
      LOGGER.verbose("Entering the paymnt response doc null if");
      outStruct= processSoftDecline(env, inStruct, outStruct, getOrderListOpDoc, null, null, null, null, true);
      LOGGER.debug("Exiting the payment response doc null if");
    } catch(Exception e){
      LOGGER.error("Inside GCCreditCardUE.processCCAuthOutput():", e);
      throw new GCException(e);
    }
    return outStruct;
  }

  /**
   * This method basically handle the soft decline and manage the retry count increment
   * @param env
   * @param inStruct
   * @param outStruct
   * @param getOrderListOpDoc
   * @param strAuthDateTime
   * @param strAuthReturnMsg
   * @param strAuthResponse
   * @param strIsoResp
   * @return
   * @throws GCException
   */
  public static YFSExtnPaymentCollectionOutputStruct processSoftDecline(YFSEnvironment env, YFSExtnPaymentCollectionInputStruct inStruct, YFSExtnPaymentCollectionOutputStruct outStruct, Document getOrderListOpDoc, String strAuthDateTime, String strAuthReturnMsg, String strAuthResponse, String strIsoResp, boolean isAJBDown) throws GCException{
    try{
      LOGGER.debug("Entering the If in the else case");
      Date currentDate = new Date(System.currentTimeMillis()+5*60*1000);
      String strOrderDate = new SimpleDateFormat(
          GCConstants.DATE_TIME_FORMAT).format(currentDate);
      SimpleDateFormat formatter = new SimpleDateFormat(
          GCConstants.DATE_TIME_FORMAT);
      Date dtOrderDate = formatter.parse(strOrderDate);
      //To display message fro AJB orders when AJB or First DAta is down
      boolean bIsPaymentGatewayDown=false;
      boolean bIsSuspendPayment= false;
      if(GCConstants.OFFLINE.equalsIgnoreCase(strIsoResp) || isAJBDown){
        bIsPaymentGatewayDown= true;
        outStruct.PaymentTransactionError =
            setPayTrnxError("PAYMENT_FAILURE", GCConstants.PAYMENT_COMMUNICATION_FAILURE_MSG);
      } else if ((GCConstants.REFERRAL_CODE.equalsIgnoreCase(strIsoResp) && GCConstants.PAYMENT_CALL_FOR_AUTH
          .equals(strAuthResponse))) {
        LOGGER.debug("Referral scenario");
        outStruct.PaymentTransactionError = setPayTrnxError(
            "PAYMENT_FAILURE",
            GCConstants.PAYMENT_REFERRAL_MSG);
        outStruct.suspendPayment = GCConstants.YES;
        bIsSuspendPayment= true;
        GCCommonUtil.invokeService(env, GCConstants.PAYMENT_ALERT, getOrderListOpDoc);
      } else if ((GCConstants.REENTER_CODE.equalsIgnoreCase(strIsoResp) && GCConstants.PAYMENT_CALL_FOR_AUTH
          .equals(strAuthResponse))) {
        LOGGER.debug("Reenter scenario");
        outStruct.PaymentTransactionError = setPayTrnxError(
            "PAYMENT_FAILURE",
            GCConstants.PAYMENT_REENTER_MSG);
        outStruct.suspendPayment = GCConstants.YES;
        bIsSuspendPayment= true;
        GCCommonUtil.invokeService(env, GCConstants.PAYMENT_ALERT, getOrderListOpDoc);
      }
      Element eleGCCCAuthRetry =
          GCXMLUtil.getElementByXPath(getOrderListOpDoc, GCConstants.ORDER_LIST_EXTN_CC_AUTH_RETRY_XPATH
              + "[@PaymentKey='" + inStruct.paymentKey + "']");
      String strRetryCount = "0";
      if (!YFCCommon.isVoid(eleGCCCAuthRetry)) {
        LOGGER.debug("Entering the If eleGCCCAuthRetry not void ");
        strRetryCount = eleGCCCAuthRetry.getAttribute(GCConstants.AUTH_RETRY_COUNT);
        LOGGER.debug("The Retry Count is:" + strRetryCount);
        LOGGER.debug("Exiting the If eleGCCCAuthRetry not void ");
      }
      if (YFCCommon.isVoid(eleGCCCAuthRetry) || GCConstants.MAXIMUM_RETRY > Integer.parseInt(strRetryCount)) {
        LOGGER.debug("Entering the If Retry count is less than Maximu Retry");
        if(bIsPaymentGatewayDown){
          outStruct.authorizationId = GCConstants.DECLINED;
          outStruct.authReturnMessage = "Payment Gateway Down";
          outStruct.authReturnCode = "Payment Gateway Down";
          outStruct.tranReturnMessage = "Payment Gateway Down";
        } else{
          outStruct.authorizationId = GCConstants.DECLINED;
          outStruct.authReturnMessage = GCConstants.FAILED;
          outStruct.authReturnCode = strAuthResponse;
          outStruct.tranReturnMessage = strAuthReturnMsg;
        }
        outStruct.authTime = strAuthDateTime;
        if (!GCConstants.GIFT_CARD.equals(inStruct.paymentType)) {
          outStruct.authorizationExpirationDate = YTimestamp.HIGH_DATE.toString();
        }
        outStruct.authorizationAmount = GCConstants.ZEROAMOUNT;
        outStruct.holdOrderAndRaiseEvent = GCConstants.FALSE;
        if(!bIsSuspendPayment){
          outStruct.suspendPayment = GCConstants.NO;
        }
        outStruct.tranRequestTime = GCDateUtils.convertDate(currentDate);
        outStruct.tranType = GCConstants.PAYMENT_STATUS_AUTHORIZATION;
        outStruct.tranAmount = GCConstants.ZEROAMOUNT;
        outStruct.retryFlag = "N";
        outStruct.collectionDate=dtOrderDate;
        // Updating the Auth Retry Count
        Document opDocCCAuthRetry = null;
        Document inXMLDoc = GCXMLUtil.createDocument(GCConstants.GC_AUTH_RETRY_ELEMENT);
        Element rootInXMLEle = GCXMLUtil.getRootElement(inXMLDoc);
        rootInXMLEle.setAttribute(GCConstants.ORDER_HEADER_KEY, inStruct.orderHeaderKey);
        rootInXMLEle.setAttribute(GCConstants.PAYMENT_KEY, inStruct.paymentKey);
        if (null == eleGCCCAuthRetry) {
          LOGGER.debug("Entering the If, eleGCCCAuthRetry is null");
          rootInXMLEle.setAttribute(GCConstants.AUTH_RETRY_COUNT, GCConstants.STRING_ONE);
          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("The inDoc for GCCreateCCEntry is:" + GCXMLUtil.getXMLString(inXMLDoc));
          }
          opDocCCAuthRetry = GCCommonUtil.invokeService(env, GCConstants.GC_CREATE_CC_AUTH_RETRY, inXMLDoc);
          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("The output doc of GChangeCCAuthRetry" + GCXMLUtil.getXMLString(opDocCCAuthRetry));
          }
          LOGGER.debug("Exiting the If, eleGCCCAuthRetry is null");
        } else {
          LOGGER.debug("Entering the else when the retry hasbeen done before");
          int iRetry = Integer.parseInt(strRetryCount) + 1;
          String ccAuthRetryKey = eleGCCCAuthRetry.getAttribute(GCConstants.CC_AUTH_RETRY_KEY);
          rootInXMLEle.setAttribute(GCConstants.CC_AUTH_RETRY_KEY, ccAuthRetryKey);
          rootInXMLEle.setAttribute(GCConstants.AUTH_RETRY_COUNT, String.valueOf(iRetry));
          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("The inDoc for GCCreateCCEntry is:" + GCXMLUtil.getXMLString(inXMLDoc));
          }
          opDocCCAuthRetry = GCCommonUtil.invokeService(env, GCConstants.GC_CHANGE_CC_AUTH_RETRY, inXMLDoc);
          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("The output doc of GChangeCCAuthRetry" + GCXMLUtil.getXMLString(opDocCCAuthRetry));
          }
          LOGGER.debug("Exiting the else when the retry hasbeen done before");
        }
        LOGGER.debug("Exiting the If Retry count is less than Maximu Retry");
        return outStruct;
      } else {
        LOGGER.debug("Entering IF Response is DECLINED");

        if(bIsPaymentGatewayDown){
          outStruct.authorizationId = GCConstants.DECLINED;
          outStruct.authReturnMessage = "Payment Gateway Down";
          outStruct.authReturnCode = "Payment Gateway Down";
          outStruct.tranReturnMessage = "Payment Gateway Down";
        } else{
          outStruct.authorizationId = GCConstants.DECLINED;
          outStruct.authReturnMessage = GCConstants.FAILED;
          outStruct.authReturnCode = strAuthResponse;
          outStruct.tranReturnMessage = strAuthReturnMsg;
        }
        outStruct.tranRequestTime = GCDateUtils.convertDate(currentDate);
        outStruct.authTime = GCConstants.BLANK;
        outStruct.tranType = GCConstants.PAYMENT_STATUS_AUTHORIZATION;
        if (!GCConstants.GIFT_CARD.equals(inStruct.paymentType)) {
          outStruct.authorizationExpirationDate = YTimestamp.HIGH_DATE.toString();
        }
        outStruct.authorizationAmount = GCConstants.ZEROAMOUNT;
        outStruct.holdOrderAndRaiseEvent = GCConstants.FALSE;
        outStruct.suspendPayment = GCConstants.NO;
        outStruct.tranAmount = GCConstants.ZEROAMOUNT;
        outStruct.retryFlag = "N";

        Document inXMLDoc = GCXMLUtil.createDocument(GCConstants.GC_AUTH_RETRY_ELEMENT);
        Element rootInXMLEle = GCXMLUtil.getRootElement(inXMLDoc);
        int iRetry = Integer.parseInt(strRetryCount) + 1;
        String ccAuthRetryKey = eleGCCCAuthRetry.getAttribute(GCConstants.CC_AUTH_RETRY_KEY);
        rootInXMLEle.setAttribute(GCConstants.CC_AUTH_RETRY_KEY, ccAuthRetryKey);
        rootInXMLEle.setAttribute(GCConstants.AUTH_RETRY_COUNT, String.valueOf(iRetry));
        GCCommonUtil.invokeService(env, GCConstants.GC_CHANGE_CC_AUTH_RETRY, inXMLDoc);
        outStruct.collectionDate=dtOrderDate;
        //OMS-4371 Start
        boolean isOfflineResp= false;
        if(GCConstants.OFFLINE.equalsIgnoreCase(strIsoResp) && GCConstants.MAXIMUM_RETRY == Integer.parseInt(strRetryCount)) {
          GCCommonUtil.invokeService(env, GCConstants.PAYMENT_SERVICE_UNAVAILABLE, getOrderListOpDoc);
          isOfflineResp= true;
        }
        if(isAJBDown && GCConstants.MAXIMUM_RETRY == Integer.parseInt(strRetryCount)){
          GCCommonUtil.invokeService(env, GCConstants.PAYMENT_SERVICE_UNAVAILABLE, getOrderListOpDoc);
          isOfflineResp= true;
        }
        if(GCConstants.MAXIMUM_RETRY == Integer.parseInt(strRetryCount) && !isOfflineResp){
          GCCommonUtil.invokeService(env, GCConstants.PAYMENT_ALERT, getOrderListOpDoc);
        }
        //OMS-4371 End
        LOGGER.debug("Entering IF Response is DECLINED");
        return outStruct;
      }
    } catch(Exception e){
      LOGGER.error("Inside GCCreditCardUE.processCCAuthOutput():", e);
      throw new GCException(e);
    }
  }
  
  private void printInStruct(YFSExtnPaymentCollectionInputStruct inStruct) {
		// TODO Auto-generated method stub
	  if(!YFCCommon.isVoid(inStruct)){
		  LOGGER.debug("inStruct.bPreviouslyInvoked"+inStruct.bPreviouslyInvoked);
		  LOGGER.debug("inStruct.requestAmount"+inStruct.requestAmount);
		  LOGGER.debug("inStruct.chargeType"+inStruct.chargeType);
		  LOGGER.debug("inStruct.paymentType"+inStruct.paymentType);
		  LOGGER.debug("inStruct.authorizationId"+inStruct.authorizationId);
		  LOGGER.debug("inStruct.orderNo"+inStruct.orderNo);
		  LOGGER.debug("inStruct.shipTokey"+inStruct.shipTokey);
		  LOGGER.debug("inStruct.billTokey"+inStruct.billTokey);
		  LOGGER.debug("inStruct.currency"+inStruct.currency);
		  LOGGER.debug("inStruct.creditCardNo"+inStruct.creditCardNo);
		  LOGGER.debug("inStruct.creditCardExpirationDate"+inStruct.creditCardExpirationDate);
		  LOGGER.debug("inStruct.creditCardName"+inStruct.creditCardName);
		  LOGGER.debug("inStruct.svcNo"+inStruct.svcNo);
		  LOGGER.debug("inStruct.debitCardNo"+inStruct.debitCardNo);
		  LOGGER.debug("inStruct.paymentReference1"+inStruct.paymentReference1);
		  LOGGER.debug("inStruct.paymentReference2"+inStruct.paymentReference2);
		  LOGGER.debug("inStruct.paymentReference3"+inStruct.paymentReference3);
		  LOGGER.debug("inStruct.paymentReference4"+inStruct.paymentReference4);
		  LOGGER.debug("inStruct.paymentReference5"+inStruct.paymentReference5);
		  LOGGER.debug("inStruct.paymentReference6"+inStruct.paymentReference6);
		  LOGGER.debug("inStruct.paymentReference7"+inStruct.paymentReference7);
		  LOGGER.debug("inStruct.paymentReference8"+inStruct.paymentReference8);
		  LOGGER.debug("inStruct.paymentReference9"+inStruct.paymentReference9);
		  LOGGER.debug("inStruct.eleExtendedFields"+inStruct.eleExtendedFields);
		  LOGGER.debug("inStruct.customerAccountNo"+inStruct.customerAccountNo);
		  LOGGER.debug("inStruct.customerPONo"+inStruct.customerPONo);
		  LOGGER.debug("inStruct.merchantId"+inStruct.merchantId);
		  LOGGER.debug("inStruct.shipToFirstName"+inStruct.shipToFirstName);
		  LOGGER.debug("inStruct.shipToLastName"+inStruct.shipToLastName);
		  LOGGER.debug("inStruct.shipToAddressLine1"+inStruct.shipToAddressLine1);
		  LOGGER.debug("inStruct.shipToCity"+inStruct.shipToCity);
		  LOGGER.debug("inStruct.shipToState"+inStruct.shipToState);
		  LOGGER.debug("inStruct.shipToZipCode"+inStruct.shipToZipCode);
		  LOGGER.debug("inStruct.shipToCountry"+inStruct.shipToCountry);
		  LOGGER.debug("inStruct.shipToEmailId"+inStruct.shipToEmailId);
		  LOGGER.debug("inStruct.shipToDayPhone"+inStruct.shipToDayPhone);
		  LOGGER.debug("inStruct.shipToId"+inStruct.shipToId);
		  LOGGER.debug("inStruct.billToFirstName"+inStruct.billToFirstName);
		  LOGGER.debug("inStruct.billToLastName"+inStruct.billToLastName);
		  LOGGER.debug("inStruct.billToAddressLine1"+inStruct.billToAddressLine1);
		  LOGGER.debug("inStruct.billToCity"+inStruct.billToCity);
		  LOGGER.debug("inStruct.billToState"+inStruct.billToState);
		  LOGGER.debug("inStruct.billToZipCode"+inStruct.billToZipCode);
		  LOGGER.debug("inStruct.billToCountry"+inStruct.billToCountry);
		  LOGGER.debug("inStruct.billToEmailId"+inStruct.billToEmailId);
		  LOGGER.debug("inStruct.billToDayPhone"+inStruct.billToDayPhone);
		  LOGGER.debug("inStruct.billToId"+inStruct.billToId);
		  LOGGER.debug("inStruct.chargeTransactionKey"+inStruct.chargeTransactionKey);
		  LOGGER.debug("inStruct.orderHeaderKey"+inStruct.orderHeaderKey);
		  LOGGER.debug("inStruct.enterpriseCode"+inStruct.enterpriseCode);
		  LOGGER.debug("inStruct.paymentConfigOrganizationCode"+inStruct.paymentConfigOrganizationCode);
		  LOGGER.debug("inStruct.creditCardType"+inStruct.creditCardType);
		  LOGGER.debug("inStruct.documentType"+inStruct.documentType);
		  LOGGER.debug("inStruct.firstName"+inStruct.firstName);
		  LOGGER.debug("inStruct.middleName"+inStruct.middleName);
		  LOGGER.debug("inStruct.lastName"+inStruct.lastName);
		  LOGGER.debug("inStruct.secureAuthenticationCode"+inStruct.secureAuthenticationCode);
		  LOGGER.debug("inStruct.currentAuthorizationAmount"+inStruct.currentAuthorizationAmount);
		  LOGGER.debug("inStruct.currentAuthorizationExpirationDate"+inStruct.currentAuthorizationExpirationDate);
		  LOGGER.debug("inStruct.currentAuthorizationCreditCardTransactions"+inStruct.currentAuthorizationCreditCardTransactions);
		  LOGGER.debug("inStruct.paymentKey"+inStruct.paymentKey);
		  LOGGER.debug("inStruct.bVoidTransaction"+inStruct.bVoidTransaction);
		  LOGGER.debug("inStruct.cashBackAmount"+inStruct.cashBackAmount);
		  LOGGER.debug("inStruct.chequeNo"+inStruct.chequeNo);
		  LOGGER.debug("inStruct.chequeReference"+inStruct.chequeReference);
		  LOGGER.debug("inStruct.entryType"+inStruct.entryType);
		  LOGGER.debug("inStruct.voidTransactionStatus"+inStruct.voidTransactionStatus);
		  LOGGER.debug("inStruct.callForAuthorizationStatus"+inStruct.callForAuthorizationStatus);

	  }  
		
	}

	  
	private void printOutStruct(YFSExtnPaymentCollectionOutputStruct outStruct){
		
		if(!YFCCommon.isVoid(outStruct))
		{
		
			LOGGER.debug("outStruct.authorizationAmount"+outStruct.authorizationAmount);
			LOGGER.debug("outStruct.authorizationId"+outStruct.authorizationId);
			LOGGER.debug("outStruct.authorizationExpirationDate"+outStruct.authorizationExpirationDate);
			LOGGER.debug("outStruct.tranType"+outStruct.tranType);
			LOGGER.debug("outStruct.tranAmount"+outStruct.tranAmount);
			LOGGER.debug("outStruct.bPreviousInvocationSuccessful"+outStruct.bPreviousInvocationSuccessful);
			LOGGER.debug("outStruct.tranRequestTime"+outStruct.tranRequestTime);
			LOGGER.debug("outStruct.tranReturnCode"+outStruct.tranReturnCode);
			LOGGER.debug("outStruct.tranReturnMessage"+outStruct.tranReturnMessage);
			LOGGER.debug("outStruct.tranReturnFlag"+outStruct.tranReturnFlag);
			LOGGER.debug("outStruct.requestID"+outStruct.requestID);
			LOGGER.debug("outStruct.internalReturnCode"+outStruct.internalReturnCode);
			LOGGER.debug("outStruct.internalReturnFlag"+outStruct.internalReturnFlag);
			LOGGER.debug("outStruct.internalReturnMessage"+outStruct.internalReturnMessage);
			LOGGER.debug("outStruct.authCode"+outStruct.authCode);
			LOGGER.debug("outStruct.authAVS"+outStruct.authAVS);
			LOGGER.debug("outStruct.sCVVAuthCode"+outStruct.sCVVAuthCode);
			LOGGER.debug("outStruct.collectionDate"+outStruct.collectionDate);
			LOGGER.debug("outStruct.executionDate"+outStruct.executionDate);
			LOGGER.debug("outStruct.authReturnCode"+outStruct.authReturnCode);
			LOGGER.debug("outStruct.authReturnFlag"+outStruct.authReturnFlag);
			LOGGER.debug("outStruct.authReturnMessage"+outStruct.authReturnMessage);
			LOGGER.debug("outStruct.authTime"+outStruct.authTime);
			LOGGER.debug("outStruct.retryFlag"+outStruct.retryFlag);
			LOGGER.debug("outStruct.holdOrderAndRaiseEvent"+outStruct.holdOrderAndRaiseEvent);
			LOGGER.debug("outStruct.asynchRequestProcess"+outStruct.asynchRequestProcess);
			LOGGER.debug("outStruct.holdReason"+outStruct.holdReason);
			LOGGER.debug("outStruct.suspendPayment"+outStruct.suspendPayment);
			LOGGER.debug("outStruct.SvcNo"+outStruct.SvcNo);
			LOGGER.debug("outStruct.DisplaySvcNo"+outStruct.DisplaySvcNo);
			LOGGER.debug("outStruct.PaymentReference1"+outStruct.PaymentReference1);
			LOGGER.debug("outStruct.DisplayPaymentReference1"+outStruct.DisplayPaymentReference1);
			LOGGER.debug("outStruct.PaymentReference2"+outStruct.PaymentReference2);
			LOGGER.debug("outStruct.PaymentReference3"+outStruct.PaymentReference3);
			LOGGER.debug("outStruct.PaymentReference4"+outStruct.PaymentReference4);
			LOGGER.debug("outStruct.PaymentReference5"+outStruct.PaymentReference5);
			LOGGER.debug("outStruct.PaymentReference6"+outStruct.PaymentReference6);
			LOGGER.debug("outStruct.PaymentReference7"+outStruct.PaymentReference7);
			LOGGER.debug("outStruct.PaymentReference8"+outStruct.PaymentReference8);
			LOGGER.debug("outStruct.PaymentReference9"+outStruct.PaymentReference9);
			LOGGER.debug("outStruct.eleExtendedFields"+outStruct.eleExtendedFields);
			LOGGER.debug("outStruct.RequiresCallForAuthorization"+outStruct.RequiresCallForAuthorization);
			LOGGER.debug("outStruct.ConditionalCallForAuthorization"+outStruct.ConditionalCallForAuthorization);
			LOGGER.debug("outStruct.OfflineStatus"+outStruct.OfflineStatus);
			LOGGER.debug("outStruct.recordAdditionalTransactions"+outStruct.recordAdditionalTransactions);
			LOGGER.debug("outStruct.PaymentTransactionError"+outStruct.PaymentTransactionError);
			LOGGER.debug("outStruct.bRetryVoidAsRefund"+outStruct.bRetryVoidAsRefund);
			LOGGER.debug("outStruct.HoldAgainstBook"+outStruct.HoldAgainstBook);
			LOGGER.debug("outStruct.bChargeMayHaveOccurred"+outStruct.bChargeMayHaveOccurred);
		}	
	}

  
}
