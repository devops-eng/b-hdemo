/**
 * Copyright 2012 SIGMA Inc. All rights reserved.
 * SIGMA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 **/

package com.gc.userexit;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.pca.ycd.japi.ue.YCDGetAppeasementOffersUE;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSUserExitException;

/**
 * PRIMARY PURPOSE: This class is used to set Offer type as Variable Amount
 * Order
 * 
 * It calls the following methods getAppeasementOffers
 **/
public class GCGetAppeasementOffersUE implements YCDGetAppeasementOffersUE {

  private static final YFCLogCategory LOGGER =YFCLogCategory
  .instance(GCGetAppeasementOffersUE.class);
  /**
   * PURPOSE: This method is used to set Offer type as Variable Amount Order
   * 
   * @param YFSEnvironment
   *            env
   * @param Document
   *            inDoc contains AppeasementOffers xml
   * 
   */
  @Override
  public Document getAppeasementOffers(YFSEnvironment env, Document inputDoc)
  throws YFSUserExitException {

    Document responseDoc = null;

    Element appeasementOffers = inputDoc.getDocumentElement();
    Element eleAppeasementReason = (Element) inputDoc.getElementsByTagName("AppeasementReason").item(0);
    String sAppeaseType = eleAppeasementReason.getAttribute("GCAppeaseType");

    try {
      responseDoc = GCXMLUtil.createDocument("AppeasementOffers");
    } catch (IllegalArgumentException e1) {
      LOGGER.error("Inside GCGetAppeasementOffersUE.getAppeasementOffers():",e1);
    }
    Element outDocEle = responseDoc.getDocumentElement();
    String orderHeaderKey = ((Element) appeasementOffers
        .getElementsByTagName(GCConstants.ORDER).item(0))
        .getAttribute(GCConstants.ORDER_HEADER_KEY);
    outDocEle.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);

    outDocEle.setAttribute(GCConstants.CHARGE_CATEGORY,
    "CUSTOMER_APPEASEMENT");
    if (YFCCommon.equals("SHIPPING", sAppeaseType)) {
      outDocEle.setAttribute(GCConstants.CHARGE_NAME, "SHIPPING_APPEASEMENT");
    } else {
      outDocEle.setAttribute(GCConstants.CHARGE_NAME, "CUSTOMER_APPEASEMENT");
    }
    Element appOffer = responseDoc.createElement("AppeasementOffer");
    appOffer.setAttribute("OfferType", "VARIABLE_AMOUNT_ORDER");
    appOffer.setAttribute("Preferred", GCConstants.YES);
    Element orderEle = responseDoc.createElement(GCConstants.ORDER);
    orderEle.setAttribute(GCConstants.CHARGE_CATEGORY,
    "CUSTOMER_APPEASEMENT");

    if (YFCCommon.equals("SHIPPING", sAppeaseType)) {
      orderEle.setAttribute(GCConstants.CHARGE_NAME, "SHIPPING_APPEASEMENT");
    } else {
      orderEle.setAttribute(GCConstants.CHARGE_NAME, "CUSTOMER_APPEASEMENT");
    }

    Element orderLinesEle = responseDoc
    .createElement(GCConstants.ORDER_LINES);
    Element orderLineEle = responseDoc
    .createElement(GCConstants.ORDER_LINE);
    orderLineEle.setAttribute(GCConstants.CHARGE_CATEGORY,
    "CUSTOMER_APPEASEMENT");

    orderLinesEle.appendChild(orderLineEle);
    orderEle.appendChild(orderLinesEle);
    appOffer.appendChild(orderEle);
    outDocEle.appendChild(appOffer);
    return responseDoc;
  }

}
