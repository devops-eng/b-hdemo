package com.gc.userexit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.yfs.japi.ue.YFSGetAvailabilityCorrectionsForItemListUE;
/**
 * @author <a href="mailto:rakesh.kumar@expicient.com">Rakesh</a>
 */
public class GCGetAvailabilityCorrectionsForItemListUE implements YFSGetAvailabilityCorrectionsForItemListUE {

  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCGetAvailabilityCorrectionsForItemListUE.class);

  @Override
  /**
   *
   * Description of getAvailabilityCorrectionsForItemList If there is a warranty
   * associated with a regular item parent or bundle item parent then this UE
   * will handle the simultaneous Release/Partial Release of the Parent and warranty line
   *
   * @param env
   * @param inDoc
   * @return
   * @throws YFSUserExitException
   * @see com.yantra.yfs.japi.ue.YFSGetAvailabilityCorrectionsForItemListUE#getAvailabilityCorrectionsForItemList(com.yantra.yfs.japi.YFSEnvironment, org.w3c.dom.Document)
   */
  public Document getAvailabilityCorrectionsForItemList(YFSEnvironment env,
      Document inDoc) throws YFSUserExitException {
    LOGGER.info("Entering GCGetAvailabilityCorrectionsForItemListUE.getAvailabilityCorrectionsForItemList() inputDoc:"+GCXMLUtil.getXMLString(inDoc));
    try {
      String orderHeaderKey=GCXMLUtil.getAttributeFromXPath(inDoc, "Items/@OrderReference");
      String transactionId=GCXMLUtil.getAttributeFromXPath(inDoc, "Items/@TransactionId");
      if(transactionId.equals(GCConstants.RELEASE_TRAN_ID)){
        Document docGetOrderListIP=GCXMLUtil.createDocument(GCConstants.ORDER);
        Element eleOrder = docGetOrderListIP.getDocumentElement();
        eleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
        Document docGetOrderListIPTemplate = GCXMLUtil.getDocument(" <OrderList><Order OrderHeaderKey=''><OrderLines><OrderLine DependentOnLineKey=''"
            + " OrderLineKey='' OrderedQty='' KitCode=''><Extn ExtnIsWarrantyItem=''/><Item ItemID=''/><BundleParentLine OrderLineKey=''/><OrderStatuses><OrderStatus ShipNode='' Status='' StatusDescription=''"
            + " StatusQty='' TotalQuantity=''/></OrderStatuses></OrderLine></OrderLines></Order></OrderList>");
        Document docGetOrderListOP=GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_LIST, docGetOrderListIP,docGetOrderListIPTemplate);
        NodeList nlOrderLine = XPathAPI.selectNodeList(docGetOrderListOP,
            "OrderList/Order/OrderLines/OrderLine");
        int noOfOrderLines=nlOrderLine.getLength();
        for(int counter=0; counter<noOfOrderLines; counter++){
          Element eleOrderLine = (Element) nlOrderLine.item(counter);
          Element eleExtn = (Element) XPathAPI.selectSingleNode(
              eleOrderLine, GCConstants.EXTN);
          String extnIsWarrantyItem = eleExtn.getAttribute(GCConstants.EXTN_IS_WARRANTY_ITEM);
          if("Y".equalsIgnoreCase(extnIsWarrantyItem)){
            Element eleItem = (Element) XPathAPI.selectSingleNode(
                eleOrderLine, GCConstants.ITEM);
            String itemIdWarranty=eleItem.getAttribute(GCConstants.ITEM_ID);
            String dependentOnLineKey=eleOrderLine.getAttribute(GCConstants.DEPENDENT_ON_LINE_KEY);
            Map<String,String> mParentAttributes=findParentOrderLineDetails(docGetOrderListOP,dependentOnLineKey);
            String itemIdParent=mParentAttributes.get(GCConstants.ITEM_ID).toString();
            String sOrderLineKeyParent=mParentAttributes.get(GCConstants.ORDER_LINE_KEY).toString();
            Map<String,String> mparentStatusQuantity= findStatusQuantityParent(sOrderLineKeyParent,docGetOrderListOP);
            if(!mparentStatusQuantity.isEmpty()){
              String releaseQty=mparentStatusQuantity.get(GCConstants.RELEASED_STATUS);
              Element eleWarrantySupply=(Element)XPathAPI.selectSingleNode(inDoc,"Items/Item[@ItemID='"+itemIdWarranty + "']/Supplies/Supply");
              if(releaseQty!=null){
                eleWarrantySupply.setAttribute(GCConstants.QUANTITY,releaseQty);
              }
            } else {
              String kitCodeParent=mParentAttributes.get(GCConstants.KIT_CODE).toString();
              String totalScheduledQuantityofParent=findTotalScheduledQuantityofParent(docGetOrderListOP,dependentOnLineKey);
              if(!GCConstants.BUNDLE.equals(kitCodeParent)){
                updateInputDoc(inDoc,itemIdWarranty,itemIdParent,totalScheduledQuantityofParent,kitCodeParent);
              }else if(GCConstants.BUNDLE.equals(kitCodeParent)){
                String bundleParentOrderLineKey=mParentAttributes.get(GCConstants.ORDER_LINE_KEY).toString();
                List<String> itemIdsList=new ArrayList<String>();
                NodeList nlOrderLineInner=XPathAPI.selectNodeList(docGetOrderListOP,"OrderList/Order/OrderLines/OrderLine");
                int nlOrderLineCount=nlOrderLineInner.getLength();
                for(int count=0; count<nlOrderLineCount;count++){
                  addParentItemIDToList(bundleParentOrderLineKey, itemIdsList, nlOrderLineInner, count);
                }
                String itemIdComponent=itemIdsList.get(0);
                Element eleItemInner=(Element)XPathAPI.selectSingleNode(inDoc,"Items/Item[@ItemID='"+itemIdComponent +"']");
                if(eleItemInner==null){
                  Element eleSupply=(Element)XPathAPI.selectSingleNode(inDoc,"Items/Item[@ItemID='"+itemIdWarranty + "']/Supplies/Supply");
                  eleSupply.setAttribute(GCConstants.QUANTITY, GCConstants.ZERO_AMOUNT);
                  break;
                }
              }
            }
            LOGGER.info("Exiting GCGetAvailabilityCorrectionsForItemListUE.getAvailabilityCorrectionsForItemList() outputDoc:"+GCXMLUtil.getXMLString(inDoc));
          }
        }
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCGetAvailabilityCorrectionsForItemListUE.getAvailabilityCorrectionsForItemList():",
          e);
      throw (new GCException(e)).getYFSUserExitException();
    }
    return inDoc;
  }

  /**
   *
   * @param bundleParentOrderLineKey
   * @param itemIdsList
   * @param nlOrderLineInner
   * @param count
   * @throws TransformerException
   */
  private void addParentItemIDToList(String bundleParentOrderLineKey, List<String> itemIdsList,
      NodeList nlOrderLineInner, int count) throws TransformerException {

    //Code refactored for Sonar Violations.
    String componentOrderLineKey;
    String sItemId;
    Element eleOrderLineInner=(Element)nlOrderLineInner.item(count);
    Element eleBundleParentLine=(Element)XPathAPI.selectSingleNode(eleOrderLineInner, "BundleParentLine");
    if(eleBundleParentLine!=null){
      componentOrderLineKey=eleBundleParentLine.getAttribute(GCConstants.ORDER_LINE_KEY);
      if(bundleParentOrderLineKey!=null && bundleParentOrderLineKey.equals(componentOrderLineKey)){
        Element eleItem1=(Element)XPathAPI.selectSingleNode(eleOrderLineInner,"Item" );
        sItemId=eleItem1.getAttribute(GCConstants.ITEM_ID);
        itemIdsList.add(sItemId);
      }
    }
  }
  /**
   *
   * Description of findTotalScheduledQuantityofParent  This method Returns the total scheduled quantity of the parent orderline
   *
   * @param docGetOrderListOP
   * @param orderLineKey
   * @return
   * @throws GCException
   *
   */
  private String findTotalScheduledQuantityofParent(Document docGetOrderListOP, String orderLineKey) throws GCException{
    LOGGER.info("Entering GCGetAvailabilityCorrectionsForItemListUE.findTotalScheduledQuantityofParent() ");
    try {
      NodeList nlOrderLine= XPathAPI.selectNodeList(docGetOrderListOP, "OrderList/Order/OrderLines/OrderLine");
      int nlOrderLineCount=nlOrderLine.getLength();
      double quantity=0;
      for(int counter=0;counter<nlOrderLineCount;counter++){
        Element eleOrderLine = (Element) nlOrderLine.item(counter);
        String candidateOrderLineKey=eleOrderLine.getAttribute(GCConstants.ORDER_LINE_KEY);
        if(orderLineKey.equals(candidateOrderLineKey)){
          NodeList nlOrderStatus= XPathAPI.selectNodeList(eleOrderLine, "OrderStatuses/OrderStatus");
          int nlScheduleCount=nlOrderStatus.getLength();
          for(int innerCounter=0;innerCounter<nlScheduleCount;innerCounter++){
            Element eleOrderStatus = (Element) nlOrderStatus.item(innerCounter);
            String status=eleOrderStatus.getAttribute(GCConstants.STATUS);
            if("1500".equals(status)){
              quantity= quantity+Double.parseDouble(eleOrderStatus.getAttribute(GCConstants.STATUS_QTY));
            }
          }
          break;
        }
      }
      LOGGER.info("Exiting GCGetAvailabilityCorrectionsForItemListUE.findTotalScheduledQuantityofParent() ");
      return quantity+"";
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCGetAvailabilityCorrectionsForItemListUE.findTotalScheduledQuantityofParent():",
          e);
      throw new GCException(e);
    }
  }
  /**
   *
   * Description of findParentOrderLineDetails This method returns a map of ParentOrderLine attributes extracted from getOrderList API Output
   *
   * @param docGetOrderListOP
   * @param dependentOnLineKey
   * @return
   * @throws GCException
   *
   */
  private Map<String,String> findParentOrderLineDetails(Document docGetOrderListOP,String dependentOnLineKey) throws GCException{
    LOGGER.info("Entering GCGetAvailabilityCorrectionsForItemListUE.findParentOrderLineItemId() ");
    try {
      String orderLineKeyParent=null,itemIdParent=null,kitCodeParent=null,orderedQtyParent=null,orderLineKey=null;
      Map<String,String> mParentAttributes=new HashMap<String,String>();
      NodeList nlOrderLine=XPathAPI.selectNodeList(docGetOrderListOP, "OrderList/Order/OrderLines/OrderLine");
      int orderLineCount=nlOrderLine.getLength();
      for (int counter = 0; counter < orderLineCount; counter++) {
        Element orderLineEle = (Element)nlOrderLine.item(counter);
        orderLineKeyParent=orderLineEle.getAttribute(GCConstants.ORDER_LINE_KEY);
        if(orderLineKeyParent.equals(dependentOnLineKey)){
          Element eleItem = (Element) XPathAPI.selectSingleNode(
              orderLineEle, GCConstants.ITEM);
          itemIdParent=eleItem.getAttribute(GCConstants.ITEM_ID);
          kitCodeParent=orderLineEle.getAttribute(GCConstants.KIT_CODE);
          orderedQtyParent=orderLineEle.getAttribute(GCConstants.ORDERED_QTY);
          orderLineKey=orderLineEle.getAttribute(GCConstants.ORDER_LINE_KEY);
          mParentAttributes.put(GCConstants.ITEM_ID, itemIdParent);
          mParentAttributes.put(GCConstants.KIT_CODE, kitCodeParent);
          mParentAttributes.put(GCConstants.ORDERED_QTY, orderedQtyParent);
          mParentAttributes.put(GCConstants.ORDER_LINE_KEY, orderLineKey);
          break;
        }
      }
      LOGGER.info("Exiting GCGetAvailabilityCorrectionsForItemListUE.findParentOrderLineItemId() ");
      return mParentAttributes;
    } catch (Exception e) {
      LOGGER.error("inside catch of GCGetAvailabilityCorrectionsForItemListUE.findParentOrderLineItemId():", e);
      throw new GCException(e);
    }
  }
  /**
   *
   * Description of updateInputDoc this method update the final inDoc.
   * It will update the supply quantity and other supply details of
   * the warranty according to its associated parent
   *
   * @param inDoc
   * @param itemIdWarranty
   * @param itemIdParent
   * @param totalScheduledQuantityofParent
   * @param kitCodeParent
   * @return
   * @throws GCException
   *
   */
  private Document updateInputDoc(Document inDoc,String itemIdWarranty,String itemIdParent, String totalScheduledQuantityofParent,String kitCodeParent) throws GCException{
    try {
      LOGGER.info("Entering GCGetAvailabilityCorrectionsForItemListUE.updateInputDoc() ");
      NodeList nlItemLine=XPathAPI.selectNodeList(inDoc,"Items/Item");
      String itemId=null;
      Element eleSuppliesSource=null;
      Element eleSuppliesDest=null;
      Element eleSuppliesDestChild=null;
      String totalRequiredQuantityForWarranty=null;
      int itemLinesCount=nlItemLine.getLength();
      for (int counter = 0; counter < itemLinesCount; counter++) {
        Element orderLineEle=(Element)nlItemLine.item(counter);
        itemId=orderLineEle.getAttribute(GCConstants.ITEM_ID);
        totalRequiredQuantityForWarranty=orderLineEle.getAttribute("TotalRequiredQty");
        Double totalRequiredQuantityForWarrantyDouble=Double.parseDouble(totalRequiredQuantityForWarranty);
        Double totalScheduledQuantityofParentDouble=Double.parseDouble(totalScheduledQuantityofParent);
        if(itemId.equals(itemIdParent)){
          eleSuppliesSource=(Element)XPathAPI.selectSingleNode(orderLineEle,"Supplies");
        }else if(itemId.equals(itemIdWarranty)){
          eleSuppliesDest=(Element)XPathAPI.selectSingleNode(orderLineEle,"Supplies");
          eleSuppliesDestChild=(Element)XPathAPI.selectSingleNode(orderLineEle,"Supplies/Supply");
        }
        if(eleSuppliesSource!=null && eleSuppliesDest!=null && eleSuppliesDestChild!=null){
          eleSuppliesDest.removeChild(eleSuppliesDestChild);
          GCXMLUtil.copyElement(inDoc, eleSuppliesSource, eleSuppliesDest);
          Element eleSupply= (Element)XPathAPI.selectSingleNode(eleSuppliesDest, "Supply");
          if(totalRequiredQuantityForWarrantyDouble<totalScheduledQuantityofParentDouble){
            eleSupply.setAttribute(GCConstants.QUANTITY, totalRequiredQuantityForWarranty);
          }else{
            eleSupply.setAttribute(GCConstants.QUANTITY, totalScheduledQuantityofParent);
          }
          break;
        }
      }
      Element eleSupply= (Element)XPathAPI.selectSingleNode(eleSuppliesDest, "Supply");
      if(eleSuppliesSource==null && !(GCConstants.BUNDLE.equals(kitCodeParent))){
        eleSupply.setAttribute(GCConstants.QUANTITY, totalScheduledQuantityofParent);
      }
      LOGGER.info("Exiting GCGetAvailabilityCorrectionsForItemListUE.updateInputDoc() ");
    }catch (Exception e) {
      LOGGER.error("inside catch of GCGetAvailabilityCorrectionsForItemListUE.updateInputDoc()",e);
      throw new GCException(e);
    }
    return inDoc;
  }
  /**
   *
   * Description of findStatusQuantityParent this method returns a map of the release and shipped quantity of the parent
   *
   * @param sOrderLineKeyParent
   * @param docGetOrderListOP
   * @return
   * @throws GCException
   *
   */
  private Map<String,String> findStatusQuantityParent(String sOrderLineKeyParent,Document docGetOrderListOP) throws GCException{
    LOGGER.info("Entering GCGetAvailabilityCorrectionsForItemListUE.findStatusQuantityParent() ");
    try {
      NodeList nlOrderStatus = XPathAPI.selectNodeList(docGetOrderListOP, "OrderList/Order/OrderLines/OrderLine[@OrderLineKey='"+sOrderLineKeyParent+"']/OrderStatuses/OrderStatus");
      int orderStatusCount=nlOrderStatus.getLength();
      Map<String,String> mparentStatusQuantity=new HashMap<String,String>();
      String status=null,quantity=null;
      for(int c=0;c<orderStatusCount;c++){
        Element eleOrderStatus= (Element)nlOrderStatus.item(c);
        status=eleOrderStatus.getAttribute(GCConstants.STATUS);
        if("3200".equals(status) || "3700".equals(status)){
          quantity = GCXMLUtil.getAttributeFromXPath(docGetOrderListOP, "OrderList/Order/OrderLines/OrderLine[@OrderLineKey='"+sOrderLineKeyParent+"']/OrderStatuses/OrderStatus/@StatusQty");
          mparentStatusQuantity.put(status,quantity);
        }
      }
      LOGGER.info("Exiting GCGetAvailabilityCorrectionsForItemListUE.findStatusQuantityParent() ");
      return mparentStatusQuantity;
    } catch (Exception e) {
      LOGGER.error("inside catch of GCGetAvailabilityCorrectionsForItemListUE.findStatusQuantityParent()",e);
      throw new GCException(e);
    }

  }
}

