/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Make the supply 0 for the items with  ExtnItemStatus is (A1, C1, F1, G3, S1)
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               24/03/2014        Singh, Gurpreet            OMS-1468: Order Scheduling
 *#################################################################################################################################################################
 */
package com.gc.userexit;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.yfs.japi.ue.YFSGetAvailabilityCorrectionsUE;

/**
 * @author <a href="mailto:gurpreet.singh@expicient.com">Gurpreet</a>
 */
public class GCGetAvailabilityCorrectionsUE implements
	YFSGetAvailabilityCorrectionsUE {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCGetAvailabilityCorrectionsUE.class);

    /**
     * 
     * Description of getAvailabilityCorrections If ExtnItemStatus is (A1, C1,
     * F1, G3, S1)// This should be maintained in a common code; Do not send all
     * the supplies coming for the item to outputDoc;
     * 
     * @param env
     * @param inDoc
     * @return
     * @throws YFSUserExitException
     * @see com.yantra.yfs.japi.ue.YFSGetAvailabilityCorrectionsUE#getAvailabilityCorrections(com.yantra.yfs.japi.YFSEnvironment,
     *      org.w3c.dom.Document)
     */
    public Document getAvailabilityCorrections(YFSEnvironment env,
	    Document inDoc) throws YFSUserExitException {
	LOGGER.debug(" Entering GCGetAvailabilityCorrectionsUE.getAvailabilityCorrections() method ");
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" getAvailabilityCorrections() method, Input document is"
		    + GCXMLUtil.getXMLString(inDoc));
	}
	try {
	    Element eleRootOfInDoc = inDoc.getDocumentElement();
	    String sItemID = eleRootOfInDoc.getAttribute(GCConstants.ITEM_ID);
	    String sUOM = eleRootOfInDoc.getAttribute(GCConstants.UOM);
	    String sOrganizationCode = eleRootOfInDoc
		    .getAttribute(GCConstants.ENTERPRISE_CODE);
	    Document docGetItemListOP = GCCommonUtil.getItemList(env, sItemID,
		    sUOM, GCConstants.GCI);
	    String sExtnItemStatus = GCXMLUtil.getAttributeFromXPath(
		    docGetItemListOP, "ItemList/Item/Extn/@ExtnItemStatus");
	    LOGGER.debug("sExtnItemStatus :" + sExtnItemStatus);

	    // Create XML for getCommonCodeList and Enter Attributes Values
	    Document docGetCommonCodeListInput = GCXMLUtil
		    .createDocument(GCConstants.COMMON_CODE);
	    docGetCommonCodeListInput.getDocumentElement().setAttribute(
		    GCConstants.CODE_TYPE, GCConstants.ITEM_STATUS);
	    docGetCommonCodeListInput.getDocumentElement().setAttribute(
		    GCConstants.ORGANIZATION_CODE, sOrganizationCode);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("docGetCommonCodeListInput:"
			+ GCXMLUtil.getXMLString(docGetCommonCodeListInput));
	    }

	    // Invoke getCommonCodeList
	    Document docGetCommonCodeListOutput = GCCommonUtil.invokeAPI(env,
		    GCConstants.API_GET_COMMON_CODE_LIST,
		    docGetCommonCodeListInput, null);
	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("docGetCommonCodeListOutput: "
			+ GCXMLUtil.getXMLString(docGetCommonCodeListOutput));
	    }

	    if ((!YFCCommon.isVoid(sExtnItemStatus))) {
		Element eleCommonCode = (Element) XPathAPI.selectSingleNode(
			docGetCommonCodeListOutput,
			"CommonCodeList/CommonCode[@CodeValue='"
				+ sExtnItemStatus + "']");
		if (!YFCCommon.isVoid(eleCommonCode)) {
		    NodeList nlSupply = XPathAPI.selectNodeList(inDoc,
			    "Item/Supplies/Supply");
		    for (int iCounter = 0; iCounter < nlSupply.getLength(); iCounter++) {
			LOGGER.debug(" Inside the loop over supply lines ");
			Element eleSupply = (Element) nlSupply.item(iCounter);
			eleSupply.setAttribute(GCConstants.QUANTITY, "0.00");
		    }
		}
	    }
	   // GC-Phase2::Begin
      // Fix for find/reserve Inventory calls of serial Items.
      GCCommonUtil.changeTagName(inDoc, "InventoryTag", "Tag");

      // GC-Phase2::End
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCGetAvailabilityCorrectionsUE.getAvailabilityCorrections()",
		    e);
	    throw (new GCException(e)).getYFSUserExitException();
	}
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug(" Updated Input document is"
		    + GCXMLUtil.getXMLString(inDoc));
	}
	LOGGER.debug(" Exiting GCGetAvailabilityCorrectionsUE.getAvailabilityCorrections() method ");
	return inDoc;
    }
}
