/** 
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Prorate the charges at time of Invoice depending on the quantity
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               02/07/2014        Singh, Gurpreet            Charges proration at Invoice
 *#################################################################################################################################################################
 */
package com.gc.userexit;

import java.math.BigDecimal;
import java.util.List;

import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSExtnInputLineChargesShipment;
import com.yantra.yfs.japi.YFSExtnLineChargeStruct;
import com.yantra.yfs.japi.YFSExtnOutputLineChargesShipment;
import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.yfs.japi.ue.YFSGetLineChargesForShipmentUE;

/**
 * @author <a href="mailto:gurpreet.singh@expicient.com">Gurpreet</a>
 */
public class GCGetLineChargesForShipmentUE implements
	YFSGetLineChargesForShipmentUE {

    // initialize logger
    private static final YFCLogCategory LOGGER = YFCLogCategory
	    .instance(GCGetLineChargesForShipmentUE.class);

    private Double pricingQty;
    private Boolean bLastInvoiceForOrderLine;
    private Double orderLineOrderedQty;
    private List<YFSExtnLineChargeStruct> orderLineCharges;

    /**
     * 
     * Description of getLineChargesForShipment Prorate the charges depending on
     * the quantity
     * 
     * @param env
     * @param inStruct
     * @return
     * @throws YFSUserExitException
     * @see com.yantra.yfs.japi.ue.YFSGetLineChargesForShipmentUE#getLineChargesForShipment(com.yantra.yfs.japi.YFSEnvironment,
     *      com.yantra.yfs.japi.YFSExtnInputLineChargesShipment)
     */
    public YFSExtnOutputLineChargesShipment getLineChargesForShipment(
	    YFSEnvironment env, YFSExtnInputLineChargesShipment inStruct)
	    throws YFSUserExitException {
	LOGGER.debug(" Entering GCGetLineChargesForShipmentUE.getLineChargesForShipment() method ");
	YFSExtnOutputLineChargesShipment outStruct = new YFSExtnOutputLineChargesShipment();
	try {
	    // Parse input struct
	    parseInputStruct(inStruct);
	    prorateCharges();
	    outStruct.newLineCharges = inStruct.orderLineCharges;
	} catch (Exception e) {
	    LOGGER.error(
		    "Inside GCGetLineChargesForShipmentUE.getLineChargesForShipment():",
		    e);
	    throw (new GCException(e).getYFSUserExitException());
	}
	LOGGER.debug(" Exiting GCGetLineChargesForShipmentUE.getLineChargesForShipment() method ");
	return outStruct;
    }

    /**
     * 
     * Description of parseInputStruct Initialize the fields
     * 
     * @param inStruct
     * @throws GCException
     * 
     */
    private void parseInputStruct(YFSExtnInputLineChargesShipment inStruct)
	    throws GCException {
	LOGGER.debug(" Entering GCGetLineChargesForShipmentUE.parseInputStruct() method ");

	pricingQty = inStruct.pricingQty;
	bLastInvoiceForOrderLine = inStruct.bLastInvoiceForOrderLine;
	orderLineCharges = inStruct.orderLineCharges;
	orderLineOrderedQty = inStruct.orderLineOrderedQty;

	LOGGER.debug(" Exiting GCGetLineChargesForShipmentUE.parseInputStruct() method ");
    }

    /**
     * 
     * Description of prorateCharges Prorate the charges depending on the
     * quantity
     * 
     * @throws GCException
     * 
     */
    private void prorateCharges() throws GCException {
	LOGGER.debug(" Entering GCGetLineChargesForShipmentUE.prorateCharges() method ");
	try {
	    for (int iCounter = 0; iCounter < orderLineCharges.size(); iCounter++) {
		LOGGER.debug(" Looping on orderLineCharges ");
		YFSExtnLineChargeStruct inputStructlineCharge = orderLineCharges
			.get(iCounter);
		double dChargePerLine = inputStructlineCharge.chargePerLine;
		double dInvoicedChargePerLine = inputStructlineCharge.invoicedPerLine;
		double dProratedAmount = dChargePerLine * pricingQty
			/ orderLineOrderedQty;
		LOGGER.debug(" dChargePerLine " + dChargePerLine
			+ " dInvoicedChargePerLine " + dInvoicedChargePerLine
			+ " dProratedAmount " + dProratedAmount);
		if (bLastInvoiceForOrderLine) {
		    LOGGER.debug(" Inside if it Last line for Invoice ");
		    double dVarianceInCharge = dChargePerLine
			    - (dInvoicedChargePerLine + dProratedAmount);
		    LOGGER.debug(" dVarianceInCharge " + dVarianceInCharge);
		    if (dVarianceInCharge != 0.00) {
			dProratedAmount = dProratedAmount + dVarianceInCharge;
			LOGGER.debug(" Inside if dVarianceInCharge is not zero, dProratedAmount"
				+ dProratedAmount);
		    }
		}
		BigDecimal bdProratedAmount = GCCommonUtil
			.formatNumberForDecimalPrecision(dProratedAmount, 2);
		LOGGER.debug(" bdProratedAmount " + bdProratedAmount);
		inputStructlineCharge.chargePerLine = bdProratedAmount
			.doubleValue();
	    }
	} catch (Exception e) {
	    LOGGER.error(
		    " Inside catch of GCGetLineChargesForShipmentUE.prorateCharges() ",
		    e);
	    throw new GCException(e);
	}
	LOGGER.debug(" Exiting GCGetLineChargesForShipmentUE.prorateCharges() method ");
    }
}
