/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: Generating the orderno. of 6 digits and prefix a constant to that
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               10/02/2014        Singh, Gurpreet            OMS-1141 : Development - External Order Capture
 *#################################################################################################################################################################
 */
package com.gc.userexit;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.Set;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCException;
import com.yantra.shared.ycp.YFSContext;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.yfs.japi.ue.YFSGetOrderNoUE;

/**
 * @author Gurpreet
 */
public class GCGetOrderNoUE implements YFSGetOrderNoUE {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCGetOrderNoUE.class);

  @Override
  /**
   *
   * Description of getOrderNo
   * This method will check and generate the valid OrderNo and will return that
   * @param env
   * @param inMap
   * @return
   * @throws YFSUserExitException
   * @see com.yantra.yfs.japi.ue.YFSGetOrderNoUE#getOrderNo(com.yantra.yfs.japi.YFSEnvironment, java.util.Map)
   */
  public String getOrderNo(YFSEnvironment env, Map inMap)
      throws YFSUserExitException {
    LOGGER.debug(" Entering GCGetOrderNoUE.getOrderNo() method ");
    String sSequenceNumber = null;
    // Do the operations only of the order number coming form XML is empty
    // just a double check.
    try {
      // Check if the order number coming form XML is empty
      if (checkOrderNumber(inMap)) {
        LOGGER.debug(" Inside if(checkOrderNumber(inMap)) ");
        // Generate the order number by fetching sequence number from DB
        sSequenceNumber = getSequenceNumber(env);
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCGetOrderNoUE.getOrderNo(), Exception is :",
          e);
    }
    LOGGER.debug(" sSequenceNumber is : " + sSequenceNumber);
    LOGGER.debug(" Exiting GCGetOrderNoUE.getOrderNo() method ");
    return sSequenceNumber;
  }

  /**
   *
   * Description of getSequenceNumber This method will fetch the sequence
   * number from database and prefix a constant to it to generate OrderNo
   *
   * @param env
   * @return
   * @throws SQLException
   * @throws Exception
   *
   */
  private String getSequenceNumber(YFSEnvironment env) throws GCException,
  SQLException {
    LOGGER.debug(" Entering GCGetOrderNoUE.getSequenceNumber() method ");
    Statement objStatement = null;
    ResultSet objResultSet = null;
    Long lIntSequenceNumber = -1L;
    Connection con = null;
    String seqNum = null;

    try {
      // setting up the connection with database
      con = ((YFSContext) env)
          .getConnectionForTableType(GCConstants.GUITAR);
      objStatement = getPrepareStatement(con);
      objResultSet = objStatement
          .executeQuery(GCConstants.SEQ_STATEMENT_ORDER);
      while (objResultSet.next()) {
        LOGGER.debug(" Inside while(objResultSet.next()) ");

        // fetching the sequence number from result set
        lIntSequenceNumber = objResultSet.getLong(1);
        LOGGER.debug(" lIntSequenceNumber is : " + lIntSequenceNumber);

        // generating the order number by prefixing constant to sequence
        // number
        seqNum = GCConstants.GC + String.valueOf(lIntSequenceNumber);
        LOGGER.debug(" seqNum is : " + seqNum);
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCGetOrderNoUE.getSequenceNumber(), Exception is :",
          e);
      throw new GCException(e);
    } finally {
      if(objResultSet!=null){
        objResultSet.close();
      }
      if(objStatement!=null){
        objStatement.close();
      }
    }
   
    LOGGER.debug(" Exiting GCGetOrderNoUE.getSequenceNumber() method ");
    return seqNum;
  }

private Statement getPrepareStatement(Connection con) throws SQLException {
	Statement objStatement;
	objStatement = con.createStatement();
	return objStatement;
}

  /**
   *
   * Description of checkOrderNumber This method will check if OrderNo already
   * exists in XML
   *
   * @param inMap
   * @return
   * @throws Exception
   *
   */
  private boolean checkOrderNumber(Map inMap) throws GCException {
    LOGGER.debug(" Entering GCGetOrderNoUE.checkOrderNumber() method ");
    try {
      if (inMap != null) {
        LOGGER.debug(" Inside if(inMap != null) ");
        Set lObjKeysSet = inMap.keySet();
        // check if OrderNo exists in XML
        if (lObjKeysSet.contains(GCConstants.ORDER_NO)) {
          LOGGER.debug(" Inside if (lObjKeysSet.contains(GCConstants.ORDER_NO)) ");
          String ordNum = (String) inMap.get(GCConstants.ORDER_NO);
          if (GCConstants.BLANK_STRING.equals(ordNum)) {
            LOGGER.debug(" Inside if (ordNum.equals('')) ");
            return true;
          } else {
            LOGGER.debug(" Inside else (ordNum.equals('')) ");
            return false;
          }
        } else {
          // if OrderNo does not exist in XML, return true
          LOGGER.debug(" Inside else (lObjKeysSet.contains(GCConstants.ORDER_NO)) ");
          return true;
        }
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch block of GCGetOrderNoUE.checkOrderNumber(), Exception is :",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCGetOrderNoUE.checkOrderNumber() method ");
    return false;
  }
}
