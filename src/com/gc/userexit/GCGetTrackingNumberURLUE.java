package com.gc.userexit;

import java.util.Properties;

import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.pca.ycd.japi.ue.YCDGetTrackingNumberURLUE;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSUserExitException;

public class GCGetTrackingNumberURLUE implements YCDGetTrackingNumberURLUE, YIFCustomApi{

	
	 private static final YFCLogCategory LOGGER = YFCLogCategory
		      .instance(GCGetTrackingNumberURLUE.class.getName());
		  Properties prop = new Properties();
		  public Document getTrackingNumberURL(YFSEnvironment env, Document inDoc)
		      throws YFSUserExitException {
			  
			  final String logInfo = "GCGetTrackingNumberURLUE :: getTrackingNumberURL :: ";
			  LOGGER.verbose(logInfo + "Begin");
			  if (LOGGER.isDebugEnabled()) {
			  LOGGER.debug(logInfo + "Incoming Document"+ GCXMLUtil.getXMLString(inDoc));
			  }
			  YFCDocument trackingDetailsDoc = YFCDocument.getDocumentFor(inDoc);
			  YFCElement eleTrackingNumbers = trackingDetailsDoc.getDocumentElement();
			  String sConstantUrl = prop.getProperty(GCConstants.TRACKING_URL);
			  YFCNodeList<YFCElement> listTrackingNumber = eleTrackingNumbers.getElementsByTagName(GCConstants.TRACKING_NUMBER);
			  int lengthOrderLine = listTrackingNumber.getLength();
		        for (int iCounter = 0; iCounter < lengthOrderLine; iCounter++) {
		        	 YFCElement eleTrackingNumber = listTrackingNumber.item(iCounter);
		        	  String sTrackingNumber = eleTrackingNumber.getAttribute(GCConstants.TRACKING_NO);
		        	  String sUrl = sConstantUrl.concat(sTrackingNumber);
					  eleTrackingNumber.setAttribute(GCConstants.URL, sUrl);
		        }
			  if (LOGGER.isDebugEnabled()) {
			  LOGGER.debug(logInfo + "OutDoc: "+ GCXMLUtil.getXMLString(inDoc));
			  }
			  return inDoc;
		  }

		@Override
		public void setProperties(Properties arg0) throws Exception {
			 prop = arg0;
		}
}
