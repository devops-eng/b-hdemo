/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *#################################################################################################################################################################
 *          OBJECTIVE: This file contains the custom api implementation of the userexit
 *           com.yantra.yfs.japi.ue.INVGetDemandCorrectionsUE#getDemandCorrections(com.yantra.yfs.japi.YFSEnvironment,org.w3c.dom.Document)
 *#################################################################################################################################################################
 *           Version            Date              Modified By                Description
 *#################################################################################################################################################################
 *             1.0               28-May-2015       Infosys Limited            Initial Version
 *#################################################################################################################################################################
 */
package com.gc.userexit;

// imports
import java.util.List;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.ue.INVGetDemandCorrectionsUE;

/**
 * <h3>Description :</h3>This java class is invoked from 'GCINVGetDemandCorrectionsUE_svc' Service
 * which is implemented for the INVGetDemandCorrectionsUE userexit.
 *
 * @author Infosys Limited
 */
public class GCINVGetDemandCorrectionsUEService implements INVGetDemandCorrectionsUE, YIFCustomApi {

  // the class name
  private static final String CLASS_NAME = GCINVGetDemandCorrectionsUEService.class.getName();

  // the logger reference
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(CLASS_NAME);

  // the arguments from the configurator
  Properties props;


  /* (non-Javadoc)
   * @see com.yantra.yfs.japi.ue.INVGetDemandCorrectionsUE#getDemandCorrections(com.yantra.yfs.japi.YFSEnvironment, org.w3c.dom.Document)
   */
  @Override
  public Document getDemandCorrections(final YFSEnvironment yfsEnv, final Document docInput) {

    // log the method entry
    LOGGER.info(CLASS_NAME + ":getDemandCorrections:entry");

    // logging the method input
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASS_NAME + ":getDemandCorrections:Input=" + GCXMLUtil.getXMLString(docInput));
    }

    // fetch the order details from env
    final Document docOrder = (Document) yfsEnv.getTxnObject(GCConstants.ORDER_DTLS_ENV);

    // the output document
    final Document docOutput;
    String sDonotIgnoreDAXDemand="";
    Object oDonotIgnoreDAXDemand = yfsEnv.getTxnObject("DoNotIgnoreDAXDemand");
    if(!YFCCommon.isVoid(oDonotIgnoreDAXDemand)){
    	sDonotIgnoreDAXDemand = (String)oDonotIgnoreDAXDemand;
    }
    // check if the order details exist
    if (YFCCommon.isVoid(docOrder)||YFCCommon.equals(sDonotIgnoreDAXDemand, "Y")) {

      LOGGER.info(CLASS_NAME + ":getDemandCorrections:The order xml is empty in the environment");

      // set the input as the output document
      docOutput = docInput;
    } else {

      // update the input document
      docOutput = correctDemands(docInput, docOrder);
    }

    // log the output
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASS_NAME + ":getDemandCorrections:Output=" + GCXMLUtil.getXMLString(docOutput));
    }

    // log the method exit
    LOGGER.info(CLASS_NAME + ":getDemandCorrections:exit");

    // return the output document
    return docOutput;
  }


  /**
   * <h3>Description:</h3>This method is used to correct the demands of the input document
   *
   * @param docInput The input xml for which the demands needs to be corrected
   * @param docOrder The order xml
   * @return The updated demand xml
   */
  private Document correctDemands(final Document docInput, final Document docOrder) {

    // log the input
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASS_NAME + ":correctDemands:entry:Input=" + GCXMLUtil.getXMLString(docOrder));
    }

    // Execute the remove conditions
    final Document docAfterRemoveDemands = removeDemandCorrections(docInput, docOrder);

    // log the output
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASS_NAME + ":correctDemands:Exit:Output=" + GCXMLUtil.getXMLString(docAfterRemoveDemands));
    }

    // return the updated document
    return docAfterRemoveDemands;
  }


  /**
   * <h3>Description:</h3> This method is used to execute the remove conditions configured
   *
   * @param docInput The input document to the ue
   * @param docOrder The order xml
   * @return The document after the demands are filtered
   */
  private Document removeDemandCorrections(final Document docInput, final Document docOrder) {

    // logging the method entry
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASS_NAME + ":removeDemandCorrections:Entry:Input=" + GCXMLUtil.getXMLString(docInput)
          + ":Order XML=" + GCXMLUtil.getXMLString(docOrder));
    }

    // get the list of item ids
    final String stRmvXpthCount = (String) props.get(GCConstants.RMV_XPTH_COUNT);

    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASS_NAME + ":removeDemandCorrections:RMV_XPTH_COUNT value=" + stRmvXpthCount);
    }

    final int iRmvXpthCount = Integer.parseInt(stRmvXpthCount);


    // check if the xpath count is greater than zero
    if (iRmvXpthCount > 0) {

      String strRmvXpth;
      String strRmvXpthVal;
      String strRmvDmdXpth;
      String strRmvDmdXpthVal;

      // iterate and fetch the xpaths
      for (int iRmXpthPropIndex = 1; iRmXpthPropIndex <= iRmvXpthCount; iRmXpthPropIndex++) {

        // fetch the remove xpaths
        strRmvXpth = (new StringBuilder(GCConstants.ITM_RMV_XPTH).append(iRmXpthPropIndex)).toString();
        strRmvDmdXpth = (new StringBuilder(GCConstants.ITM_RMV_DMD_XPTH).append(iRmXpthPropIndex)).toString();

        if (LOGGER.isDebugEnabled()) {

          LOGGER.debug(CLASS_NAME + ":removeDemandCorrections:XPath keys:ITM_RMV_XPTH=" + strRmvXpth
              + ":ITM_RMV_DMD_XPTH=" + strRmvDmdXpth);
        }

        // fetch the xpath values configured
        strRmvXpthVal = props.getProperty(strRmvXpth);
        strRmvDmdXpthVal = props.getProperty(strRmvDmdXpth);

        if (LOGGER.isDebugEnabled()) {

          LOGGER.debug(CLASS_NAME + ":removeDemandCorrections:XPath values:ITM_RMV_XPTH=" + strRmvXpthVal
              + ":ITM_RMV_DMD_XPTH=" + strRmvDmdXpthVal);
        }

        // apply the xpaths on the input xml
        filterInput(docInput, docOrder, strRmvXpthVal, strRmvDmdXpthVal);
      }
    }

    if (LOGGER.isDebugEnabled()) {

      // log the output
      LOGGER.debug(CLASS_NAME + ":removeDemandCorrections:Exit:Output=" + GCXMLUtil.getXMLString(docInput));
    }

    // updated input document
    return docInput;
  }


  /**
   * <h3>Description:</h3>This method is used to apply the filter on the input document based on the
   * input xpaths
   *
   * @param docInput The input the user exit
   * @param docOrder The order xml
   * @param strRmvXpth The xpath to get the item element
   * @param strRmvDmdXpth The xpath for Demands to be removed
   */
  private void filterInput(final Document docInput, final Document docOrder, final String strRmvXpth,
      final String strRmvDmdXpth) {

    // log the entry
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASS_NAME + ":filterInput:Entry");
    }

    // get the item id string to be updated in the remove demand xpath
    final String strItemIds = getItemIdString(docOrder, strRmvXpth);

    // check if any items exist
    if (YFCCommon.isStringVoid(strItemIds)) {

      // log the logical block entry
      if (LOGGER.isDebugEnabled()) {

        LOGGER.debug(CLASS_NAME + ":filterInput:No items present matching the input condition");
      }

    } else {

      // update the remove demand xpath
      final String strDmdXpath = strRmvDmdXpth.replaceFirst("#ITEMIDLIST#", strItemIds);

      if (LOGGER.isDebugEnabled()) {

        LOGGER.debug(CLASS_NAME + ":filterInput:Demand Xpath=" + strDmdXpath);
      }

      // apply the xpath
      final List listDemandEle = GCXMLUtil.getElementListByXpath(docInput, strDmdXpath);

      Element eleDemand;
      Node ndParent;

      // iterate through each of the Demand element
      for (Object objDemandEle : listDemandEle) {

        // remove the element
        eleDemand = (Element) objDemandEle;

        if (LOGGER.isDebugEnabled()) {

          LOGGER.debug(CLASS_NAME + ":filterInput:Element to be deleted=" + GCXMLUtil.getElementXMLString(eleDemand));
        }

        ndParent = eleDemand.getParentNode();
        ndParent.removeChild(eleDemand);
      }
    }
    // log the exit
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASS_NAME + ":filterInput:exit:Output=" + GCXMLUtil.getXMLString(docInput));
    }

  }


  /**
   * <h3>Description:</h3> This method is used to prepare the string of item ids to be used in the
   * xpath
   *
   * @param docOrder The order xml
   * @param strRmvXpth The removeItemId xpath
   * @return The string of item ids
   */
  private String getItemIdString(final Document docOrder, final String strRmvXpth) {

    // log the input
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASS_NAME + ":getItemIdString:entry:Input:Xpath=" + strRmvXpth);
    }

    // fetch the list of item elements
    final List listItms = GCXMLUtil.getElementListByXpath(docOrder, strRmvXpth);

    final StringBuilder stbItemList = new StringBuilder();

    Element eleItem;
    String strItemID;
    boolean bIsNotFirst = false;

    // iterate through each of the Item element
    for (Object objItem : listItms) {

      // fetch the item Id
      eleItem = (Element) objItem;
      strItemID = eleItem.getAttribute(GCConstants.ITEM_ID);

      // append to the xpath
      if (bIsNotFirst) {

        stbItemList.append(" and ");
      } else {

        bIsNotFirst = true;
      }

      stbItemList.append(" @ItemID='");
      stbItemList.append(strItemID);
      stbItemList.append("' ");
    }

    final String strItemList = stbItemList.toString();

    // log the output
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASS_NAME + ":getItemIdString:exit:Output=" + strItemList);
    }

    // return the updated string
    return strItemList;
  }


  @Override
  public void setProperties(final Properties props) {

    this.props = props;
  }
}