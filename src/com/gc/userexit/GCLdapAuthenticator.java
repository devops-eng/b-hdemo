/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class is written to implement user authentication using LDAP.
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               20/03/2014        Mittal, Yashu		JIRA No: Description of issue.
             1.1               11/08/2014        Mittal, Yashu      OMS-3032
             1.2               23/12/2014        Mitta, Yashu       Removed unused methods(findDN, encryptPassword, decryptPassword) and variables.
 *#################################################################################################################################################################
 */
package com.gc.userexit;

import java.util.Hashtable;
import java.util.Map;

import javax.naming.Context;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCException;
import com.yantra.shared.plt.PLTErrorCodes;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.util.YFSAuthenticator;

import org.apache.commons.lang3.StringUtils;

/**
 * @author <a href="mailto:email Address">Yashu Mittal</a>
 */
public class GCLdapAuthenticator implements YFSAuthenticator {

  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCLdapAuthenticator.class.getName());

  public static final String LDAP_FACTORY = YFSSystem.getProperty("yfs.security.ldap.factory");

  public static final String LDAP_URL = YFSSystem.getProperty("yfs.security.ldap.url");


  /**
   * Description of authenticate
   *
   * @param sLoginID
   * @param sPassword
   * @return
   * @throws Exception
   * @see com.yantra.yfs.japi.util.YFSAuthenticator#authenticate(java.lang.String, java.lang.String)
   */
  @Override
  public Map authenticate(String sLoginID, String sPassword) throws GCException {
    try {
      LOGGER.debug(" Entering YFSLdapAuthenticator.authenticate() method ");

      LOGGER.debug(LDAP_FACTORY);
      LOGGER.debug(LDAP_URL);

      if (YFCCommon.isVoid(LDAP_FACTORY) || YFCCommon.isVoid(LDAP_URL)) {
        YFCException ex = new YFCException(PLTErrorCodes.YCP_INVALID_LDAP_AUTHENTICATOR_CONFIGURATION);
        throw ex;
      } else {
		if (StringUtils.isEmpty(sPassword))
		{
			YFCException ex = new YFCException("Invalid login attempt: blank password");
			throw ex;
		}
		String sldapDN = sLoginID.replace(
				GCConstants.LOGIN_ID_SEPARATOR,
				GCConstants.LDAP_LOGIN_ID_SEPARATOR);
		sldapDN = StringUtils.substringBefore(sldapDN,"+"); // strip any loginid suffix in support of user aliases

        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, LDAP_FACTORY);
        env.put(Context.PROVIDER_URL, LDAP_URL);
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, sldapDN);
        env.put(Context.SECURITY_CREDENTIALS, sPassword);

        DirContext ctx = new InitialDirContext(env);

        LOGGER.debug("Context Created");

        ctx.close();

        return null;
      }
    } catch (Exception e) {
      LOGGER.error("Got exception in authenticate method", e);
      throw GCException.getYFCException(e.getMessage(), e);
    }
  }
}
