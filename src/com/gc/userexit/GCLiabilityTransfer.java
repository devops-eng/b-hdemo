/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: This class is invoked at Request Collection Liability Transfer event and publish the Data to DAX
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               19/05/2014        Soni, Karan		      Initial Version
 *#################################################################################################################################################################
 */
package com.gc.userexit;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * @author karan
 */
public class GCLiabilityTransfer {
  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCLiabilityTransfer.class);

  public Document updateDAX(YFSEnvironment env, Document inDoc) {
    try {
      LOGGER.verbose("Entering the updateDAX method");
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("The indoc is" + GCXMLUtil.getXMLString(inDoc));
      }
      Element eleFromChargeTransaction = GCXMLUtil
          .getElementByXPath(
              inDoc,
              "Order/ChargeTranDistributions/ChargeTranDistribution/DistributedFromChargeTransaction/PaymentMethod");
      Element eleToChargeTransaction = GCXMLUtil
          .getElementByXPath(
              inDoc,
              "Order/ChargeTranDistributions/ChargeTranDistribution/DistributedToChargeTransaction/PaymentMethod");
      //Code fix Begin for 5154 - to update the dax customer id in case of exchange
      Element eleForExchangeOrd = GCXMLUtil
          .getElementByXPath(
              inDoc,
              "Order/ChargeTranDistributions/ChargeTranDistribution/DistributedFromChargeTransaction");
      //Code fix End for 5154 - to update the dax customer id in case of exchange
      if (!YFCCommon.isVoid(eleToChargeTransaction)
          || !YFCCommon.isVoid(eleFromChargeTransaction)) {
        LOGGER.debug("Entering the if to publish Data to DAX");
        //OMS-5154
        modifyInputDocument(inDoc, env);
        //OMS-5154
        GCCommonUtil.invokeService(env, "GCPublishLiabilityTransferService",
            inDoc);
        LOGGER.debug("Exiting the if to publish Data to DAX");
      } else if (!YFCCommon.isVoid(eleForExchangeOrd)) {
        // Code fix Begin for 5154 - to update the dax customer id in case of exchange
        modifyInputDocument(inDoc, env);
        GCCommonUtil.invokeService(env, "GCPublishLiabilityTransferService",
            inDoc);
      }
      //Code fix End for 5154 - to update the dax customer id in case of exchange

      LOGGER.verbose("Exiting the updateDAX method");
    } catch (Exception e) {
      LOGGER.error("Inside GCLiabilityTransfer.updateDAX(): ",e);
    }
    return inDoc;
  }

  /**
   * Description: This method will modify the input document, by adding entry type and email id to input document
   *
   * @param yfsEnv
   * @param inDoc
   * @throws GCException
   * @throws Exception
   */
  public static void modifyInputDocument(Document inDoc, YFSEnvironment yfsEnv) throws GCException {
    try {
      LOGGER.debug("Entering the modifyInputDocument method");

      String strOrderHeaderKey=GCXMLUtil.getAttributeFromXPath(inDoc,"/Order/@OrderHeaderKey");

      if (!GCCommonUtil.isEmpty(strOrderHeaderKey)) {
        LOGGER.debug("GCLiabilityTransfer :: modifyInputDocument :: OrdeHeaderKey ::"+ strOrderHeaderKey);
        Document docGetOrderListTemplate=GCXMLUtil.getDocument(GCConstants.GET_ORDER_LIST_TEMPLATE_LIABILITY_TRFR);
        Document docGetOrderListIP=GCXMLUtil.createDocument(GCConstants.ORDER);

        docGetOrderListIP.getDocumentElement().setAttribute(GCConstants.ORDER_HEADER_KEY, strOrderHeaderKey);
        Document docGetOrderListOp=GCCommonUtil.invokeAPI(yfsEnv, GCConstants.API_GET_ORDER_LIST, docGetOrderListIP, docGetOrderListTemplate);

        LOGGER.debug("GCLiabilityTransfer :: modifyInputDocument :: GetOrder List Output Document :: \n"+GCXMLUtil.getXMLString(docGetOrderListOp));

        String strEntryType="";
        String strEmailID="";
        String strBillToID="";
        String strExtnDAXCustomerID="";

        strEntryType=GCXMLUtil.getAttributeFromXPath(docGetOrderListOp, "/OrderList/Order/@EntryType");
        strEmailID=GCXMLUtil.getAttributeFromXPath(docGetOrderListOp, "/OrderList/Order/PersonInfoBillTo/@EMailID");
        strBillToID=GCXMLUtil.getAttributeFromXPath(docGetOrderListOp, "/OrderList/Order/@BillToID");
        strExtnDAXCustomerID=GCXMLUtil.getAttributeFromXPath(docGetOrderListOp, "/OrderList/Order/Extn/@ExtnDAXCustomerID");

        Element eleInDoc=inDoc.getDocumentElement();
        eleInDoc.setAttribute("EntryType", strEntryType);
        eleInDoc.setAttribute("BillToID", strBillToID);

        Element eleExtn=inDoc.createElement("Extn");
        eleExtn.setAttribute("ExtnDAXCustomerID", strExtnDAXCustomerID);
        LOGGER.debug("GCLiabilityTransfer :: modifyInputDocument :: Extn Element :: \n"+GCXMLUtil.getElementString(eleExtn));
        eleInDoc.appendChild(eleExtn);

        Element elePersonInfoBillTo=inDoc.createElement("PersonInfoBillTo");
        elePersonInfoBillTo.setAttribute("EMailID", strEmailID);
        LOGGER.debug("GCLiabilityTransfer :: modifyInputDocument :: Person Info Bill To Element :: \n"+GCXMLUtil.getElementString(elePersonInfoBillTo));

        Element elePaymentMethod=GCXMLUtil.getElementByXPath(inDoc, "/Order/ChargeTranDistributions/ChargeTranDistribution/DistributedToChargeTransaction/PaymentMethod");
        elePaymentMethod.appendChild(elePersonInfoBillTo);

        
        // Fix for GCSTORE#6526 -- Starts

        handleGiftCardUpdation(docGetOrderListOp, inDoc);

        // Fix for GCSTORE#6526 -- Ends
          
         
      }
      LOGGER.debug("GCLiabilityTransfer :: modifyInputDocument :: Modified Input Document :: \n"+GCXMLUtil.getXMLString(inDoc));
      LOGGER.debug("Exiting the modifyInputDocument method");
    } catch (Exception e) {
      LOGGER.error("Inside catch of GCProcessDAXAllocation.addBundleComponentToUnScheduleOrderDoc()", e);
      throw new GCException(e);
    }
  }

  /**
   * This method is used to append chargeTransactionDetail of GiftCard as a fix for GCSTORE#6526
   * @param docGetOrderListOp
   * @param inDoc
   */
  private static void handleGiftCardUpdation(Document docGetOrderListOp, Document inDoc) {

    NodeList nlChargeTranDistribution = inDoc.getElementsByTagName("ChargeTranDistribution");
    int length = nlChargeTranDistribution.getLength();
    for (int i = 0; i < length; i++) {
      Element eleChargeTranDistribution = (Element) nlChargeTranDistribution.item(i);
      Element eleDistributedFromCT =
          (Element) eleChargeTranDistribution.getElementsByTagName("DistributedFromChargeTransaction").item(0);
      Element eleDistributedFromPymntMthd =
          (Element) eleDistributedFromCT.getElementsByTagName("PaymentMethod").item(0);
      String fromPymntType = eleDistributedFromPymntMthd.getAttribute("PaymentType");


      if (YFCUtils.equals(GCConstants.GIFT_CARD, fromPymntType)) {

        String fromChargeTrnsctnKey = eleDistributedFromCT.getAttribute("ChargeTransactionKey");
        Element eleCreditCardTrsctn =
            GCXMLUtil.getElementByXPath(docGetOrderListOp,
                "/OrderList/Order/ChargeTransactionDetails/ChargeTransactionDetail[@ChargeTransactionKey='"
                    + fromChargeTrnsctnKey + "']/CreditCardTransactions/CreditCardTransaction");

        Element inCreditCardTransaction = inDoc.createElement("CreditCardTransaction");
        GCXMLUtil.copyElement(inDoc, eleCreditCardTrsctn, inCreditCardTransaction);
        eleDistributedFromCT.appendChild(inCreditCardTransaction);
      }
    }
  }
}
