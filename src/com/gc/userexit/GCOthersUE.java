/**
 * Copyright 2014 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  ######################################################################################################################################
 *   OBJECTIVE: This class defines the Others payment group processing.
 *  ######################################################################################################################################
 *        Version    Date          Modified By           CR                Description
 *  ######################################################################################################################################
 *        1.0       02/04/2014     Soni, Karan                             Initial Version Others UE..
 *   
 * *  ######################################################################################################################################
 */

package com.gc.userexit;

import java.util.Date;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCPaymentUtils;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.date.YTimestamp;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSExtnPaymentCollectionInputStruct;
import com.yantra.yfs.japi.YFSExtnPaymentCollectionOutputStruct;
import com.yantra.yfs.japi.YFSUserExitException;

public class GCOthersUE implements com.yantra.yfs.japi.ue.YFSCollectionOthersUE {

	// initialize logger
	private static final YFCLogCategory LOGGER = YFCLogCategory
			.instance(GCOthersUE.class);
	public static final Date CURRENT_DATE = new Date();

	public YFSExtnPaymentCollectionOutputStruct collectionOthers(
			YFSEnvironment env, YFSExtnPaymentCollectionInputStruct inStruct)
			throws YFSUserExitException {
		YFSExtnPaymentCollectionOutputStruct outStruct = new YFSExtnPaymentCollectionOutputStruct();
		try {
			LOGGER.verbose("Entering the GCOthers method");
			String strChargeType = inStruct.chargeType;
			String strSellerOrg = "";
			if (GCConstants.PAYMENT_STATUS_AUTHORIZATION.equals(strChargeType)) {
				LOGGER.debug("Entering If in the case of Authorization charge.");
				Document docGetPaymentTypeList = GCPaymentUtils
						.invokeGetPaymentTypeList(env, strSellerOrg);
				Element elePaymentType = GCXMLUtil.getElementByXPath(
						docGetPaymentTypeList,
						"PaymentTypeList/PaymentType[@PaymentType='"
								+ inStruct.paymentType + "']");
				if (GCConstants.CREDIT_ON_ACCOUNT.equals(inStruct.paymentType)
						|| !YFCCommon.isVoid(elePaymentType)) {
					outStruct = dummyAuthProcessCOA(inStruct);
				}
				LOGGER.debug("Exiting the If of the Authorization Charge");
			} else if (GCConstants.PAYMENT_STATUS_CHARGE.equals(strChargeType)) {
				LOGGER.debug("Entering If in case of Charge Type as CHARGE");
				outStruct = GCPaymentUtils.updateDummyOutstruct(env, inStruct, null);
				LOGGER.debug("Exiting If in case of Charge Type as CHARGE");
			}
			LOGGER.verbose("Exiting the GCOthers method");
		} catch (Exception e) {
			LOGGER.error("Inside GCOthersUE.collectionOthers:", e);
			throw new GCException(e).getYFSUserExitException();
		}
		return outStruct;
	}


	/**
	 * 
	 * Description of dummyAuthProcessCOA
	 *
	 * @param inStruct
	 * @return 
	 *
	 */
	public static YFSExtnPaymentCollectionOutputStruct dummyAuthProcessCOA(
			YFSExtnPaymentCollectionInputStruct inStruct) {
		YFSExtnPaymentCollectionOutputStruct outStruct = new YFSExtnPaymentCollectionOutputStruct();
		try {
			outStruct.authCode = GCConstants.AUTHORIZED;
			outStruct.authReturnCode = GCConstants.AUTHORIZED;
			outStruct.authorizationExpirationDate = YTimestamp.HIGH_DATE
					.toString();
			outStruct.authorizationId = inStruct.authorizationId;
			outStruct.authorizationAmount = inStruct.requestAmount;
			outStruct.holdOrderAndRaiseEvent = false;
			outStruct.holdReason = GCConstants.BLANK;
			outStruct.internalReturnCode = GCConstants.DUMMY_AUTH_TYPE;
			outStruct.internalReturnFlag = null;
			outStruct.internalReturnMessage = GCConstants.DUMMY_AUTH_TYPE;
			outStruct.retryFlag = GCConstants.NO;
			outStruct.tranType = "AUTHORIZATION";
			outStruct.tranAmount = inStruct.requestAmount;
		} catch (Exception e) {
			LOGGER.error("Inside GCOthersUE.dummyAuthProcessCOA():", e);
		}
		return outStruct;
	}

}
