/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *##############################################################################################################################################################
 *          OBJECTIVE: Write what this class is about
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0               02/04/2014        Soni, Karan      Initial Version Others UE..
             2.0               12/01/2015        Mittal, Yashu    Changed name to GCPaymentExecutionOthersUE,
                                                                  GCSTORE-85: Design - PayPal Re-Authorization Processing
             3.0               20/01/2015        Mittal, Yashu    GCSTORE- 351: Design- PayPal Funds Capture
                                                                  GCSTORE- 353: Design- PayPal Refund Processing
             3.1               03/30/2015        Pande, Anuj      GCSTORE- 752: Design- COA Payment processing
 *#################################################################################################################################################################
 */


package com.gc.userexit;

import java.util.Date;

import org.w3c.dom.Document;

import com.gc.api.payment.GCProcessBML;
import com.gc.api.payment.GCProcessCOA;
import com.gc.api.payment.GCProcessPaypal;
import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDateUtils;
import com.gc.common.utils.GCPaymentUtils;
import com.yantra.yfc.date.YTimestamp;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSExtnPaymentCollectionInputStruct;
import com.yantra.yfs.japi.YFSExtnPaymentCollectionOutputStruct;
import com.yantra.yfs.japi.YFSUserExitException;

public class GCPaymentExecutionOthersUE implements com.yantra.yfs.japi.ue.YFSCollectionOthersUE {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCPaymentExecutionOthersUE.class);
  public static final Date CURRENT_DATE = new Date();

  @Override
  public YFSExtnPaymentCollectionOutputStruct collectionOthers(YFSEnvironment env,
    YFSExtnPaymentCollectionInputStruct inStruct) throws YFSUserExitException {
	//if(LOGGER.isVerboseEnabled()){
		LOGGER.debug("Entered GCPaymentExecutionOthersUE - printing inStruct");
		printInStruct(inStruct);
	//}
    YFSExtnPaymentCollectionOutputStruct outStruct = new YFSExtnPaymentCollectionOutputStruct();
    YFSExtnPaymentCollectionOutputStruct outStructAuthBeforeCharge = new YFSExtnPaymentCollectionOutputStruct();
    try {
      LOGGER.verbose("Entering the GCPaymentExecutionOthersUE method");
      String strChargeType = inStruct.chargeType;
      String sPaymentType = inStruct.paymentType;
      String sOrderHeaderKey = inStruct.orderHeaderKey;

      Document getOrderListTemplate = GCCommonUtil.getDocumentFromPath("/global/template/api/extn_PaymentOthersGetOrderList.xml");
      YFCDocument getOrderListOut =
          GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LIST, YFCDocument.getDocumentFor("<Order OrderHeaderKey ='" + sOrderHeaderKey + "'/>"),
              YFCDocument.getDocumentFor(getOrderListTemplate));

      GCProcessPaypal objProcessPaypal = new GCProcessPaypal();
      GCProcessBML objProcessBML = new GCProcessBML();
      if (YFCCommon.equals(strChargeType, GCConstants.PAYMENT_STATUS_AUTHORIZATION)) {
        LOGGER.debug("Entering If in the case of Authorization charge.");
        /* COA PAYMENT PROCESSING Start */
        if (YFCCommon.equals(inStruct.paymentType, GCConstants.CREDIT_ON_ACCOUNT)) {
          GCProcessCOA processCOAAuth = new GCProcessCOA();
          outStruct = processCOAAuth.processCOAAuthorization(env, inStruct, outStruct, getOrderListOut);
          
          //if(LOGGER.isVerboseEnabled()){
      		LOGGER.debug("After COA Auth");
      		printOutStruct(outStruct);
      	  //}
          
          /* COA PAYMENT PROCESSING End */
        } else if (YFCCommon.equals(sPaymentType, GCConstants.PAYPAL)) {
          LOGGER.verbose("Loading PayPal properties");
          objProcessPaypal.loadPayPalProperties();
          // GCSTORE-85: Design - PayPal Re-Authorization Processing : Start
          objProcessPaypal.processPaypalAuthorization(env, outStruct, inStruct, getOrderListOut);
        } else if(YFCCommon.equals(sPaymentType, GCConstants.BML)){
          objProcessBML.processBMLAuthorization(env, inStruct, outStruct, getOrderListOut);
        } else {
          outStruct = dummyAuthProcessCOA(inStruct);
        }
        // GCSTORE-85: Design - PayPal Re-Authorization Processing : End
        LOGGER.debug("Exiting the If of the Authorization Charge");
      } else if (YFCCommon.equals(strChargeType, GCConstants.PAYMENT_STATUS_CHARGE)) {
        LOGGER.debug("Entering If in case of Charge Type as CHARGE");
        // GCSTORE- 351: Design- PayPal Funds Capture : Start
        if (YFCCommon.equals(sPaymentType, GCConstants.PAYPAL)) {
          LOGGER.verbose("Loading PayPal properties");
          objProcessPaypal.loadPayPalProperties();
          objProcessPaypal.processPaypalCharge(env, outStruct, inStruct, getOrderListOut);
          // GCSTORE- 351: Design- PayPal Funds Capture : End
        } else if(YFCCommon.equals(sPaymentType, GCConstants.BML)){
        	objProcessBML.processBMLCharge(env, inStruct, outStruct, getOrderListOut);
        }else if(YFCCommon.equals(sPaymentType, GCConstants.CREDIT_ON_ACCOUNT)){ // GCSTORE 6614 START
        	String sAuthID = inStruct.authorizationId; 
        	if(YFCCommon.isVoid(sAuthID) && inStruct.requestAmount > 0 ){
        		GCProcessCOA processCOAAuth = new GCProcessCOA();
        		outStructAuthBeforeCharge = processCOAAuth.processCOAAuthorization(env, inStruct, outStructAuthBeforeCharge, getOrderListOut);

        		//if(LOGGER.isVerboseEnabled()){
        			LOGGER.debug("Entered 6614 fix - printing outStructAuthBeforeCharge");
        			printOutStruct(outStructAuthBeforeCharge);
        		//}

        		if(!YFCCommon.equals(outStructAuthBeforeCharge.authorizationId,GCConstants.DECLINED)){
	  	        	// In case of AUTH success  
	  	        	inStruct.authorizationId = outStructAuthBeforeCharge.authorizationId;
	  	        	outStruct = GCPaymentUtils.updateDummyOutstruct(env, inStruct, outStructAuthBeforeCharge);
	        	}else {
	        		Date currentDate= new Date();
	        		outStruct.authorizationId = outStructAuthBeforeCharge.authorizationId;
  		            outStruct.authReturnMessage = outStructAuthBeforeCharge.authReturnMessage;
  		            outStruct.authorizationAmount = GCConstants.ZEROAMOUNT;
  		            outStruct.holdOrderAndRaiseEvent = false;
  		            outStruct.tranAmount = GCConstants.ZEROAMOUNT;
  		            outStruct.PaymentTransactionError = outStructAuthBeforeCharge.PaymentTransactionError;
  		            outStruct.suspendPayment = outStructAuthBeforeCharge.suspendPayment;
	  	        	outStruct.tranType = GCConstants.PAYMENT_STATUS_CHARGE;
	  	        	outStruct.retryFlag = GCConstants.NO;
	  	        	outStruct.holdOrderAndRaiseEvent = GCConstants.FALSE;
	  	        	outStruct.tranRequestTime = GCDateUtils.convertDate(currentDate);
	        	}
        		
        	}else{
        		outStruct = GCPaymentUtils.updateDummyOutstruct(env, inStruct, null);
        	}
        	
        }
        // GCSTORE 6614 END
        else {
          outStruct = GCPaymentUtils.updateDummyOutstruct(env, inStruct, null);
          LOGGER.debug("Exiting If in case of Charge Type as CHARGE");
        }
      }
      LOGGER.verbose("Exiting the GCPaymentExecutionOthersUE method");
    } catch (Exception e) {
      LOGGER.error("Inside catch block of GCPaymentExecutionOthersUE.collectionOthers:", e);
      outStruct.retryFlag = GCConstants.YES;
      outStruct.collectionDate = GCDateUtils.getCollectionDate();
    }
    return outStruct;
  }


  /**
   *
   * Description of dummyAuthProcessCOA
   *
   * @param inStruct
   * @return
   *
   */
  public static YFSExtnPaymentCollectionOutputStruct dummyAuthProcessCOA(YFSExtnPaymentCollectionInputStruct inStruct) {
    YFSExtnPaymentCollectionOutputStruct outStruct = new YFSExtnPaymentCollectionOutputStruct();
    try {
      outStruct.authCode = GCConstants.AUTHORIZED;
      outStruct.authReturnCode = GCConstants.AUTHORIZED;
      outStruct.authorizationExpirationDate = YTimestamp.HIGH_DATE.toString();
      outStruct.authorizationId = inStruct.authorizationId;
      outStruct.authorizationAmount = inStruct.requestAmount;
      outStruct.holdOrderAndRaiseEvent = false;
      outStruct.holdReason = GCConstants.BLANK;
      outStruct.internalReturnCode = GCConstants.DUMMY_AUTH_TYPE;
      outStruct.internalReturnFlag = null;
      outStruct.internalReturnMessage = GCConstants.DUMMY_AUTH_TYPE;
      outStruct.retryFlag = GCConstants.NO;
      outStruct.tranType = "AUTHORIZATION";
      outStruct.tranAmount = inStruct.requestAmount;
    } catch (Exception e) {
      LOGGER.error("Inside GCOthersUE.dummyAuthProcessCOA():", e);
    }
    return outStruct;
  }
  
  private void printInStruct(YFSExtnPaymentCollectionInputStruct inStruct) {
		// TODO Auto-generated method stub
	  if(!YFCCommon.isVoid(inStruct)){
		  LOGGER.debug("inStruct.bPreviouslyInvoked"+inStruct.bPreviouslyInvoked);
		  LOGGER.debug("inStruct.requestAmount"+inStruct.requestAmount);
		  LOGGER.debug("inStruct.chargeType"+inStruct.chargeType);
		  LOGGER.debug("inStruct.paymentType"+inStruct.paymentType);
		  LOGGER.debug("inStruct.authorizationId"+inStruct.authorizationId);
		  LOGGER.debug("inStruct.orderNo"+inStruct.orderNo);
		  LOGGER.debug("inStruct.shipTokey"+inStruct.shipTokey);
		  LOGGER.debug("inStruct.billTokey"+inStruct.billTokey);
		  LOGGER.debug("inStruct.currency"+inStruct.currency);
		  LOGGER.debug("inStruct.creditCardNo"+inStruct.creditCardNo);
		  LOGGER.debug("inStruct.creditCardExpirationDate"+inStruct.creditCardExpirationDate);
		  LOGGER.debug("inStruct.creditCardName"+inStruct.creditCardName);
		  LOGGER.debug("inStruct.svcNo"+inStruct.svcNo);
		  LOGGER.debug("inStruct.debitCardNo"+inStruct.debitCardNo);
		  LOGGER.debug("inStruct.paymentReference1"+inStruct.paymentReference1);
		  LOGGER.debug("inStruct.paymentReference2"+inStruct.paymentReference2);
		  LOGGER.debug("inStruct.paymentReference3"+inStruct.paymentReference3);
		  LOGGER.debug("inStruct.paymentReference4"+inStruct.paymentReference4);
		  LOGGER.debug("inStruct.paymentReference5"+inStruct.paymentReference5);
		  LOGGER.debug("inStruct.paymentReference6"+inStruct.paymentReference6);
		  LOGGER.debug("inStruct.paymentReference7"+inStruct.paymentReference7);
		  LOGGER.debug("inStruct.paymentReference8"+inStruct.paymentReference8);
		  LOGGER.debug("inStruct.paymentReference9"+inStruct.paymentReference9);
		  LOGGER.debug("inStruct.eleExtendedFields"+inStruct.eleExtendedFields);
		  LOGGER.debug("inStruct.customerAccountNo"+inStruct.customerAccountNo);
		  LOGGER.debug("inStruct.customerPONo"+inStruct.customerPONo);
		  LOGGER.debug("inStruct.merchantId"+inStruct.merchantId);
		  LOGGER.debug("inStruct.shipToFirstName"+inStruct.shipToFirstName);
		  LOGGER.debug("inStruct.shipToLastName"+inStruct.shipToLastName);
		  LOGGER.debug("inStruct.shipToAddressLine1"+inStruct.shipToAddressLine1);
		  LOGGER.debug("inStruct.shipToCity"+inStruct.shipToCity);
		  LOGGER.debug("inStruct.shipToState"+inStruct.shipToState);
		  LOGGER.debug("inStruct.shipToZipCode"+inStruct.shipToZipCode);
		  LOGGER.debug("inStruct.shipToCountry"+inStruct.shipToCountry);
		  LOGGER.debug("inStruct.shipToEmailId"+inStruct.shipToEmailId);
		  LOGGER.debug("inStruct.shipToDayPhone"+inStruct.shipToDayPhone);
		  LOGGER.debug("inStruct.shipToId"+inStruct.shipToId);
		  LOGGER.debug("inStruct.billToFirstName"+inStruct.billToFirstName);
		  LOGGER.debug("inStruct.billToLastName"+inStruct.billToLastName);
		  LOGGER.debug("inStruct.billToAddressLine1"+inStruct.billToAddressLine1);
		  LOGGER.debug("inStruct.billToCity"+inStruct.billToCity);
		  LOGGER.debug("inStruct.billToState"+inStruct.billToState);
		  LOGGER.debug("inStruct.billToZipCode"+inStruct.billToZipCode);
		  LOGGER.debug("inStruct.billToCountry"+inStruct.billToCountry);
		  LOGGER.debug("inStruct.billToEmailId"+inStruct.billToEmailId);
		  LOGGER.debug("inStruct.billToDayPhone"+inStruct.billToDayPhone);
		  LOGGER.debug("inStruct.billToId"+inStruct.billToId);
		  LOGGER.debug("inStruct.chargeTransactionKey"+inStruct.chargeTransactionKey);
		  LOGGER.debug("inStruct.orderHeaderKey"+inStruct.orderHeaderKey);
		  LOGGER.debug("inStruct.enterpriseCode"+inStruct.enterpriseCode);
		  LOGGER.debug("inStruct.paymentConfigOrganizationCode"+inStruct.paymentConfigOrganizationCode);
		  LOGGER.debug("inStruct.creditCardType"+inStruct.creditCardType);
		  LOGGER.debug("inStruct.documentType"+inStruct.documentType);
		  LOGGER.debug("inStruct.firstName"+inStruct.firstName);
		  LOGGER.debug("inStruct.middleName"+inStruct.middleName);
		  LOGGER.debug("inStruct.lastName"+inStruct.lastName);
		  LOGGER.debug("inStruct.secureAuthenticationCode"+inStruct.secureAuthenticationCode);
		  LOGGER.debug("inStruct.currentAuthorizationAmount"+inStruct.currentAuthorizationAmount);
		  LOGGER.debug("inStruct.currentAuthorizationExpirationDate"+inStruct.currentAuthorizationExpirationDate);
		  LOGGER.debug("inStruct.currentAuthorizationCreditCardTransactions"+inStruct.currentAuthorizationCreditCardTransactions);
		  LOGGER.debug("inStruct.paymentKey"+inStruct.paymentKey);
		  LOGGER.debug("inStruct.bVoidTransaction"+inStruct.bVoidTransaction);
		  LOGGER.debug("inStruct.cashBackAmount"+inStruct.cashBackAmount);
		  LOGGER.debug("inStruct.chequeNo"+inStruct.chequeNo);
		  LOGGER.debug("inStruct.chequeReference"+inStruct.chequeReference);
		  LOGGER.debug("inStruct.entryType"+inStruct.entryType);
		  LOGGER.debug("inStruct.voidTransactionStatus"+inStruct.voidTransactionStatus);
		  LOGGER.debug("inStruct.callForAuthorizationStatus"+inStruct.callForAuthorizationStatus);

	  }  
		
	}

	  
	private void printOutStruct(YFSExtnPaymentCollectionOutputStruct outStruct){
		
		if(YFCCommon.isVoid(outStruct))
		{
		
			LOGGER.debug("outStruct.authorizationAmount"+outStruct.authorizationAmount);
			LOGGER.debug("outStruct.authorizationId"+outStruct.authorizationId);
			LOGGER.debug("outStruct.authorizationExpirationDate"+outStruct.authorizationExpirationDate);
			LOGGER.debug("outStruct.tranType"+outStruct.tranType);
			LOGGER.debug("outStruct.tranAmount"+outStruct.tranAmount);
			LOGGER.debug("outStruct.bPreviousInvocationSuccessful"+outStruct.bPreviousInvocationSuccessful);
			LOGGER.debug("outStruct.tranRequestTime"+outStruct.tranRequestTime);
			LOGGER.debug("outStruct.tranReturnCode"+outStruct.tranReturnCode);
			LOGGER.debug("outStruct.tranReturnMessage"+outStruct.tranReturnMessage);
			LOGGER.debug("outStruct.tranReturnFlag"+outStruct.tranReturnFlag);
			LOGGER.debug("outStruct.requestID"+outStruct.requestID);
			LOGGER.debug("outStruct.internalReturnCode"+outStruct.internalReturnCode);
			LOGGER.debug("outStruct.internalReturnFlag"+outStruct.internalReturnFlag);
			LOGGER.debug("outStruct.internalReturnMessage"+outStruct.internalReturnMessage);
			LOGGER.debug("outStruct.authCode"+outStruct.authCode);
			LOGGER.debug("outStruct.authAVS"+outStruct.authAVS);
			LOGGER.debug("outStruct.sCVVAuthCode"+outStruct.sCVVAuthCode);
			LOGGER.debug("outStruct.collectionDate"+outStruct.collectionDate);
			LOGGER.debug("outStruct.executionDate"+outStruct.executionDate);
			LOGGER.debug("outStruct.authReturnCode"+outStruct.authReturnCode);
			LOGGER.debug("outStruct.authReturnFlag"+outStruct.authReturnFlag);
			LOGGER.debug("outStruct.authReturnMessage"+outStruct.authReturnMessage);
			LOGGER.debug("outStruct.authTime"+outStruct.authTime);
			LOGGER.debug("outStruct.retryFlag"+outStruct.retryFlag);
			LOGGER.debug("outStruct.holdOrderAndRaiseEvent"+outStruct.holdOrderAndRaiseEvent);
			LOGGER.debug("outStruct.asynchRequestProcess"+outStruct.asynchRequestProcess);
			LOGGER.debug("outStruct.holdReason"+outStruct.holdReason);
			LOGGER.debug("outStruct.suspendPayment"+outStruct.suspendPayment);
			LOGGER.debug("outStruct.SvcNo"+outStruct.SvcNo);
			LOGGER.debug("outStruct.DisplaySvcNo"+outStruct.DisplaySvcNo);
			LOGGER.debug("outStruct.PaymentReference1"+outStruct.PaymentReference1);
			LOGGER.debug("outStruct.DisplayPaymentReference1"+outStruct.DisplayPaymentReference1);
			LOGGER.debug("outStruct.PaymentReference2"+outStruct.PaymentReference2);
			LOGGER.debug("outStruct.PaymentReference3"+outStruct.PaymentReference3);
			LOGGER.debug("outStruct.PaymentReference4"+outStruct.PaymentReference4);
			LOGGER.debug("outStruct.PaymentReference5"+outStruct.PaymentReference5);
			LOGGER.debug("outStruct.PaymentReference6"+outStruct.PaymentReference6);
			LOGGER.debug("outStruct.PaymentReference7"+outStruct.PaymentReference7);
			LOGGER.debug("outStruct.PaymentReference8"+outStruct.PaymentReference8);
			LOGGER.debug("outStruct.PaymentReference9"+outStruct.PaymentReference9);
			LOGGER.debug("outStruct.eleExtendedFields"+outStruct.eleExtendedFields);
			LOGGER.debug("outStruct.RequiresCallForAuthorization"+outStruct.RequiresCallForAuthorization);
			LOGGER.debug("outStruct.ConditionalCallForAuthorization"+outStruct.ConditionalCallForAuthorization);
			LOGGER.debug("outStruct.OfflineStatus"+outStruct.OfflineStatus);
			LOGGER.debug("outStruct.recordAdditionalTransactions"+outStruct.recordAdditionalTransactions);
			LOGGER.debug("outStruct.PaymentTransactionError"+outStruct.PaymentTransactionError);
			LOGGER.debug("outStruct.bRetryVoidAsRefund"+outStruct.bRetryVoidAsRefund);
			LOGGER.debug("outStruct.HoldAgainstBook"+outStruct.HoldAgainstBook);
			LOGGER.debug("outStruct.bChargeMayHaveOccurred"+outStruct.bChargeMayHaveOccurred);
		}	
	}

  

}
