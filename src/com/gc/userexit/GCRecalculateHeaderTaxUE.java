/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: The class invokes Vertex and recalculate taxes on the order.
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               22/05/2014        Singh, Gurpreet            Tax Calculations

             1.1              02/02/2015          Kumar, Rakesh              OMS:4727 Production OMS: No sales tax was applied to a production order in OMS

             1.2               03/09/2015        Pande, Anuj                POS Invoicing changes

 *#################################################################################################################################################################
 */
package com.gc.userexit;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.api.GCProcessTaxCalculationsAPI;
import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCWebServiceUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSExtnHeaderChargeStruct;
import com.yantra.yfs.japi.YFSExtnHeaderTaxCalculationInputStruct;
import com.yantra.yfs.japi.YFSExtnTaxBreakup;
import com.yantra.yfs.japi.YFSExtnTaxCalculationOutStruct;
import com.yantra.yfs.japi.ue.YFSRecalculateHeaderTaxUE;

/**
 * @author <a href="mailto:gurpreet.singh@expicient.com">Gurpreet</a>
 */
public class GCRecalculateHeaderTaxUE implements YFSRecalculateHeaderTaxUE {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCRecalculateHeaderTaxUE.class);

  private String orderHeaderKey;
  private String enterpriseCode;
  private String documentType;
  private Boolean bForInvoice;
  private List<YFSExtnTaxBreakup> colTax;
  private List<YFSExtnHeaderChargeStruct> colCharge;
  private Boolean bForPacklistPrice;
  private String shipToCity;
  private String shipToCountry;
  private String shipToState;
  private String shipToZipCode;
  int iLineItemNumber = 0;
  String sDraftOrderFlag = "";

  @Override
  public YFSExtnTaxCalculationOutStruct recalculateHeaderTax(
      YFSEnvironment env,
      YFSExtnHeaderTaxCalculationInputStruct inputStruct) {
    LOGGER.debug(" Entering GCRecalculateHeaderTaxUE.recalculateHeaderTax() method ");
    YFSExtnTaxCalculationOutStruct outStruct = new YFSExtnTaxCalculationOutStruct();
    try {

      Document docVertexWebServiceOP = null;
      parseInputStruct(inputStruct);

      if (!colCharge.isEmpty()
          && !(bForPacklistPrice)
          && !(GCConstants.YES
              .equals(env.getTxnObject("isProration")))
              && !(GCConstants.YES.equals(env
                  .getTxnObject("isVertexDown")))
                  && !(GCConstants.PO_DOCUMENT_TYPE.equals(documentType))) {

        LOGGER.debug(" Inside if sales order document and orderHeaderKey is not in env ");

        Document docGetOrderListIP = GCXMLUtil
            .createDocument(GCConstants.ORDER);
        Element eleGetOrderListIP = docGetOrderListIP
            .getDocumentElement();
        eleGetOrderListIP.setAttribute(GCConstants.ORDER_HEADER_KEY,
            orderHeaderKey);
        Document docGetOrderListTmp = GCCommonUtil
            .getDocumentFromPath("/global/template/api/getOrderList.xml");
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug(" processBackOrders() method, docGetOrderListOP is"
              + GCXMLUtil.getXMLString(docGetOrderListTmp));
        }

        Document docGetOrderListOP = GCCommonUtil.invokeAPI(env,
            GCConstants.API_GET_ORDER_LIST, docGetOrderListIP,
            docGetOrderListTmp);
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug(" processBackOrders() method, docGetOrderListOP is"
              + GCXMLUtil.getXMLString(docGetOrderListOP));
        }

        sDraftOrderFlag = GCXMLUtil.getAttributeFromXPath(
            docGetOrderListOP, "OrderList/Order/@DraftOrderFlag");
        /* POS Invoicing - Start */
        Element eleOrder = (Element)docGetOrderListOP.getDocumentElement().getElementsByTagName(GCConstants.ORDER).item(0);
        String sSellerOrganizationCode = eleOrder.getAttribute(GCConstants.SELLER_ORGANIZATION_CODE);
        if(!YFCCommon.isStringVoid(sSellerOrganizationCode) &&
            GCConstants.INTL_WEB.equalsIgnoreCase(sSellerOrganizationCode)){
          createEmptyOutStruct(outStruct);
          return outStruct;
        }
        /* POS Invoicing - End */

        Element elePersonInfoShipTo = (Element) env
            .getTxnObject("PersonInfoShipTo");
        if (YFCCommon.isVoid(elePersonInfoShipTo)) {
          elePersonInfoShipTo = GCXMLUtil.getElementByXPath(
              docGetOrderListOP,
              "OrderList/Order/PersonInfoShipTo");
        }

        Document docGetOrderInvoiceDetailListOP = fecthOrderInvoiceDetailList(env);

        Document docVertexCallIP = prepareVertexWebServiceIP(
            docGetOrderListOP, docGetOrderInvoiceDetailListOP,
            elePersonInfoShipTo);
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug(" processBackOrders() method, docVertexCallIP is"
              + GCXMLUtil.getXMLString(docVertexCallIP));
        }

        GCWebServiceUtil obj = new GCWebServiceUtil();
        docVertexWebServiceOP = obj
            .invokeSOAPWebService(
                docVertexCallIP,
                YFSSystem
                .getProperty(GCConstants.VERTEX_WEB_SERVICE_URL),
                null);
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug(" processBackOrders() method, docVertexWebServiceOP is"
              + GCXMLUtil.getXMLString(docVertexWebServiceOP));
        }

        if (YFCCommon.isVoid(docVertexWebServiceOP)) {
          env.setTxnObject("isVertexDown", GCConstants.YES);
          outStruct.colTax = colTax;
          return outStruct;
        }

        env.setTxnObject(orderHeaderKey, docVertexWebServiceOP);

        prepareOutStruct(docVertexWebServiceOP, outStruct);

      } else {
        outStruct.colTax = colTax;
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateLineTaxUE.recalculateHeaderTax()",
          e);
      updateColTax(outStruct);
    }

    LOGGER.debug(" Exiting GCRecalculateHeaderTaxUE.recalculateHeaderTax() method ");

    return outStruct;
  }

  private YFSExtnTaxCalculationOutStruct prepareOutStruct(
      Document docVertexWebServiceOP,
      YFSExtnTaxCalculationOutStruct outStruct) throws GCException {
    LOGGER.debug(" Entering GCRecalculateHeaderTaxUE.prepareOutStruct() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" prepareOutStruct() method, docVertexWebServiceOP is"
          + GCXMLUtil.getXMLString(docVertexWebServiceOP));
    }
    try {
      List<YFSExtnTaxBreakup> proratedColTax = new ArrayList<YFSExtnTaxBreakup>();
      for (int iCounter = 0; iCounter < colCharge.size(); iCounter++) {

        YFSExtnHeaderChargeStruct inputStructlineCharge = colCharge
            .get(iCounter);
        String strChargeCategory = "";
        String strChargeName = "";
        String sLineItemIdPrefix = "";
        Element eleLineItem = null;
        strChargeCategory = inputStructlineCharge.chargeCategory;
        if (!YFCCommon.isVoid(strChargeCategory)) {
          strChargeCategory = strChargeCategory.trim();
        } else {
          strChargeCategory = GCConstants.BLANK;
        }

        strChargeName = inputStructlineCharge.chargeName;
        if (!YFCCommon.isVoid(strChargeName)) {
          strChargeName = strChargeName.trim();
        } else {
          strChargeName = GCConstants.BLANK;
        }

        if (strChargeCategory.contains(GCConstants.SHIPPING)
            || strChargeCategory.contains("MiscellaneousCharges")) {
          sLineItemIdPrefix = "M-";
        } else {
          sLineItemIdPrefix = "L-";
        }
        eleLineItem = (Element) XPathAPI.selectSingleNode(
            docVertexWebServiceOP, "Envelope/Body/VertexEnvelope/"
                + "QuotationResponse/LineItem[@lineItemId='"
                + sLineItemIdPrefix + "" + strChargeCategory
                + "_" + strChargeName + "']");

        double dResponseTax = 0.00;
        double dTaxPercentage = 0.00;
        if (!YFCCommon.isVoid(eleLineItem)) {
          String sResponseTax = XPathAPI.selectSingleNode(
              eleLineItem, "TotalTax").getTextContent();
          dResponseTax = Double.parseDouble(sResponseTax);
          /* POS Invoicing - Start */
          GCProcessTaxCalculationsAPI effectiverateTax = new GCProcessTaxCalculationsAPI();
          dTaxPercentage = effectiverateTax.sumEffectiveTaxRate(eleLineItem, dTaxPercentage);
          /* POS Invoicing - End */
        }
        double dOriginaltax = 0.00;

        updateProratedColTax(proratedColTax, strChargeCategory,
            strChargeName, dResponseTax, dOriginaltax, dTaxPercentage);
      }
      outStruct.colTax = proratedColTax;

    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateHeaderTaxUE.prepareOutStruct() ",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCRecalculateHeaderTaxUE.prepareOutStruct() method ");
    return outStruct;
  }

  private void updateProratedColTax(List<YFSExtnTaxBreakup> proratedColTax,
      String strChargeCategory, String strChargeName,
      double dResponseTax, double dOriginaltax, double dTaxPercentage) throws GCException {
    LOGGER.debug(" Entering GCRecalculateHeaderTaxUE.updateProratedColTax() method ");
    LOGGER.debug(" strChargeCategory " + strChargeCategory
        + " strChargeName" + strChargeName + " dResponseTax"
        + dResponseTax + "dOriginaltax" + dOriginaltax);
    try {
      if (bForInvoice && colTax != null) {
        for (int i = 0; i < colTax.size(); i++) {
          YFSExtnTaxBreakup taxBreakup = colTax.get(i);
          if (StringUtils.equalsIgnoreCase(strChargeCategory,
              taxBreakup.chargeCategory)
              && StringUtils.equalsIgnoreCase(strChargeName,
                  taxBreakup.chargeName)) {
            dOriginaltax = taxBreakup.tax;
            double dInvoicedTax = taxBreakup.invoicedTax;
            if ((dResponseTax + dInvoicedTax) > dOriginaltax) {
              String sReference2 = taxBreakup.reference2;
              double dReference2 = (!YFCCommon
                  .isVoid(sReference2)) ? Double
                      .valueOf(sReference2) : 0.00;
                      dReference2 = dReference2
                          + (dResponseTax + dInvoicedTax)
                          - dOriginaltax;
                      taxBreakup.reference2 = String.valueOf(dReference2);
                      taxBreakup.tax = dOriginaltax - dInvoicedTax;
                      taxBreakup.taxPercentage = dTaxPercentage;
                      proratedColTax.add(taxBreakup);
            } else {
              YFSExtnTaxBreakup extnTaxBreakup = new YFSExtnTaxBreakup();
              extnTaxBreakup.chargeCategory = strChargeCategory;
              extnTaxBreakup.chargeName = strChargeName;
              extnTaxBreakup.taxName = ("LinePriceTax"
                  .equalsIgnoreCase(strChargeName)) ? strChargeName
                      : strChargeName + "Tax";
              extnTaxBreakup.taxPercentage = dTaxPercentage;
              extnTaxBreakup.tax = dResponseTax;
              proratedColTax.add(extnTaxBreakup);
            }
            break;
            // END IF - ChargeCat and ChargeName
          }
          // END LOOP - colTax
        }
      } else if (colTax != null) {
        for (int i = 0; i < colTax.size(); i++) {
          YFSExtnTaxBreakup taxBreakup = colTax.get(i);
          if (StringUtils.equalsIgnoreCase(strChargeCategory,
              taxBreakup.chargeCategory)
              && StringUtils.equalsIgnoreCase(strChargeName,
                  taxBreakup.chargeName)) {
            break;
          }
        }
        YFSExtnTaxBreakup extnTaxBreakup = new YFSExtnTaxBreakup();
        extnTaxBreakup.chargeCategory = strChargeCategory;
        extnTaxBreakup.chargeName = strChargeName;
        extnTaxBreakup.taxName = ("LinePriceTax"
            .equalsIgnoreCase(strChargeName)) ? strChargeName
                : strChargeName + "Tax";
        extnTaxBreakup.taxPercentage = dTaxPercentage;
        extnTaxBreakup.tax = dResponseTax;
        proratedColTax.add(extnTaxBreakup);
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateHeaderTaxUE.updateProratedColTax() ",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCRecalculateHeaderTaxUE.updateProratedColTax() method ");
  }

  private void parseInputStruct(
      YFSExtnHeaderTaxCalculationInputStruct inputStruct)
          throws GCException {
    LOGGER.debug(" Entering GCRecalculateHeaderTaxUE.parseInputStruct() method ");
    try {
      orderHeaderKey = inputStruct.orderHeaderKey;
      bForInvoice = inputStruct.bForInvoice;
      documentType = inputStruct.documentType;
      bForPacklistPrice = inputStruct.bForPacklistPrice;
      colTax = inputStruct.colTax;
      colCharge = inputStruct.colCharge;
      enterpriseCode = inputStruct.enterpriseCode;
      shipToCity = inputStruct.shipToCity;
      shipToCountry = inputStruct.shipToCountry;
      shipToState = inputStruct.shipToState;
      shipToZipCode = inputStruct.shipToZipCode;
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateHeaderTaxUE.parseInputStruct() ",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCRecalculateHeaderTaxUE.parseInputStruct() method ");
  }

  private Document fecthOrderInvoiceDetailList(YFSEnvironment env)
      throws GCException {
    LOGGER.debug(" Entering GCRecalculateHeaderTaxUE.fecthOrderInvoiceDetailList() method ");
    Document docGetOrderInvoiceDetailListOP = null;
    try {
      Document docGetOrderInvoiceDetailListIP = GCXMLUtil
          .createDocument("OrderInvoiceDetail");
      Element eleGetOrderInvoiceDetailListIP = docGetOrderInvoiceDetailListIP
          .getDocumentElement();
      Element eleInvoiceHeader = GCXMLUtil.createElement(
          docGetOrderInvoiceDetailListIP, "InvoiceHeader", null);
      eleInvoiceHeader.setAttribute(GCConstants.ORDER_HEADER_KEY,
          orderHeaderKey);
      eleGetOrderInvoiceDetailListIP.appendChild(eleInvoiceHeader);

      docGetOrderInvoiceDetailListOP = GCCommonUtil.invokeAPI(env,
          GCConstants.API_GET_ORDER_INVOICE_DETAIL_LIST,
          docGetOrderInvoiceDetailListIP, null);
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateHeaderTaxUE.fecthOrderInvoiceDetailList() ",
          e);
      throw new GCException(e);
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" docGetOrderInvoiceDetailListOP is"
          + GCXMLUtil.getXMLString(docGetOrderInvoiceDetailListOP));
    }
    LOGGER.debug(" Exiting GCRecalculateHeaderTaxUE.fecthOrderInvoiceDetailList() method ");
    return docGetOrderInvoiceDetailListOP;
  }

  private Document prepareVertexWebServiceIP(Document docGetOrderListOP,
      Document docGetOrderInvoiceDetailListOP, Element elePersonInfoShipTo)
          throws GCException {
    LOGGER.debug(" Entering GCRecalculateHeaderTaxUE.prepareVertexWebServiceIP() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" prepareVertexWebServiceIP() method, docGetOrderListOP is"
          + GCXMLUtil.getXMLString(docGetOrderListOP));
      LOGGER.debug(" prepareVertexWebServiceIP() method, docGetOrderInvoiceDetailListOP is"
          + GCXMLUtil.getXMLString(docGetOrderInvoiceDetailListOP));
    }
    Document docVertexCallIP = null;
    try {

      docVertexCallIP = GCCommonUtil
          .getDocumentFromPath("/global/template/api/extn_VertexCallRequest.xml");
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(" prepareVertexWebServiceIP() method, docVertexCallIP is"
            + GCXMLUtil.getXMLString(docVertexCallIP));
      }

      Element eleOrder = (Element) XPathAPI.selectSingleNode(
          docGetOrderListOP, "OrderList/Order");
      String sOrderNo = eleOrder.getAttribute(GCConstants.ORDER_NO);
      String sTaxExemptFlag = eleOrder
          .getAttribute(GCConstants.TAX_EXEMPT_FLAG);
      sTaxExemptFlag = (GCConstants.YES.equalsIgnoreCase(sTaxExemptFlag)) ? "true"
          : "false";
      String sOrderDate = eleOrder.getAttribute(GCConstants.ORDER_DATE);
      if (YFCCommon.isVoid(sOrderDate)) {
        Calendar calRemorseDate = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat(
            GCConstants.DATE_TIME_FORMAT);
        sOrderDate = dateFormat.format(calRemorseDate.getTime());
        LOGGER.debug(" sOrderDate " + sOrderDate);
      }
      //OMS-4727 Starts
      Element eTrustedId=GCXMLUtil.getElementByXPath(docVertexCallIP,"Envelope/Body/VertexEnvelope/Login/TrustedId");
      String sTrustedId=YFSSystem.getProperty(GCConstants.VERTEX_CALL_REQUEST_TRUSTED_ID);
      eTrustedId.setTextContent(sTrustedId);
      //OMS-4727 Ends
      Element eQuotationRequest = GCXMLUtil.getElementByXPath(
          docVertexCallIP,
          "Envelope/Body/VertexEnvelope/QuotationRequest");
      if (!YFCCommon.isVoid(eQuotationRequest)) {

        eQuotationRequest.setAttribute("documentDate", sOrderDate);
        eQuotationRequest.setAttribute("transactionId", sOrderNo);
        eQuotationRequest.setAttribute("transactionType", "SALE");

        XPathAPI.selectSingleNode(eQuotationRequest, "Seller/Company")
        .setTextContent("99");
        XPathAPI.selectSingleNode(eQuotationRequest, "Seller/Division")
        .setTextContent("02");

        Element eleHeaderCustomer = (Element) XPathAPI
            .selectSingleNode(eQuotationRequest, "Customer");
        eleHeaderCustomer.setAttribute("isTaxExempt", sTaxExemptFlag);
        Element eleHeaderCustomerCode = (Element) XPathAPI
            .selectSingleNode(eleHeaderCustomer, "CustomerCode");
        eleHeaderCustomerCode.setAttribute("classCode", "MAIN");
        Element eleHeaderDestination = (Element) XPathAPI
            .selectSingleNode(eleHeaderCustomer, "Destination");
        appendPersonInfoShipTo(docVertexCallIP, eleHeaderDestination,
            elePersonInfoShipTo);

        NodeList nlHeaderCharge = XPathAPI.selectNodeList(
            docGetOrderListOP,
            "OrderList/Order/HeaderCharges/HeaderCharge");
        for (int iHeaderChargeCounter = 0; iHeaderChargeCounter < nlHeaderCharge
            .getLength(); iHeaderChargeCounter++) {
          Element eleHeaderCharge = (Element) nlHeaderCharge
              .item(iHeaderChargeCounter);
          String sChargeCategory = eleHeaderCharge
              .getAttribute(GCConstants.CHARGE_CATEGORY);
          String sChargeName = eleHeaderCharge
              .getAttribute(GCConstants.CHARGE_NAME);
          String sLineItemId = fetchLineItemId(sChargeCategory,
              sChargeName);
          String sChargeAmount = eleHeaderCharge
              .getAttribute(GCConstants.CHARGE_AMOUNT);
          Element eleLineItem = GCXMLUtil.createElement(
              docVertexCallIP, "LineItem", null);
          addLineItemAttributes(eleLineItem, sLineItemId, sOrderDate);
          eQuotationRequest.appendChild(eleLineItem);

          Element eleProduct = GCXMLUtil.createElement(
              docVertexCallIP, "Product", null);
          if (sChargeCategory.contains(GCConstants.SHIPPING)
              || sChargeCategory.contains("MiscellaneousCharges")) {
            eleProduct.setAttribute("productClass", "9999");
          }
          eleLineItem.appendChild(eleProduct);

          Element eleFreight = GCXMLUtil.createElement(
              docVertexCallIP, "Freight", null);
          eleFreight.setTextContent("0");
          eleLineItem.appendChild(eleFreight);

          Element eleExtendedPrice = GCXMLUtil.createElement(
              docVertexCallIP, "ExtendedPrice", null);
          eleExtendedPrice.setTextContent(sChargeAmount);
          eleLineItem.appendChild(eleExtendedPrice);

          Element eleCustomer = GCXMLUtil.createElement(
              docVertexCallIP, "Customer", null);
          eleCustomer.setAttribute("isTaxExempt", sTaxExemptFlag);
          eleLineItem.appendChild(eleCustomer);

          Element eleCustomerCode = GCXMLUtil.createElement(
              docVertexCallIP, "CustomerCode", null);
          eleCustomerCode.setAttribute("classCode", "MAIN");
          eleCustomer.appendChild(eleCustomerCode);

          Element eleDestination = GCXMLUtil.createElement(
              docVertexCallIP, "Destination", null);
          eleCustomer.appendChild(eleDestination);
          appendPersonInfoShipTo(docVertexCallIP, eleDestination,
              elePersonInfoShipTo);
        }

      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateHeaderTaxUE.prepareVertexWebServiceIP() ",
          e);
      throw new GCException(e);
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" GCRecalculateHeaderTaxUE.prepareVertexWebServiceIP() method, updated docVertexCallIP is"
          + GCXMLUtil.getXMLString(docVertexCallIP));
    }
    LOGGER.debug(" Exiting GCRecalculateHeaderTaxUE.prepareVertexWebServiceIP() method ");
    return docVertexCallIP;
  }

  private void appendPersonInfoShipTo(Document docVertexCallIP,
      Element eleDestination, Element elePersonInfoShipTo)
          throws GCException {
    LOGGER.debug(" Entering GCRecalculateHeaderTaxUE.appendPersonInfoShipTo() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" appendPersonInfoShipTo() method, docVertexCallIP is"
          + GCXMLUtil.getXMLString(docVertexCallIP));
      LOGGER.debug(" appendPersonInfoShipTo() method, eleDestination is"
          + GCXMLUtil.getElementXMLString(eleDestination));
      LOGGER.debug(" appendPersonInfoShipTo() method, elePersonInfoShipTo is"
          + GCXMLUtil.getElementXMLString(elePersonInfoShipTo));
    }
    try {
      Element eleStreetAddress1 = GCXMLUtil.createElement(
          docVertexCallIP, "StreetAddress1", null);
      eleStreetAddress1.setTextContent(elePersonInfoShipTo
          .getAttribute(GCConstants.ADDRESS_LINE1));
      eleDestination.appendChild(eleStreetAddress1);

      Element eleCity = GCXMLUtil.createElement(docVertexCallIP, "City",
          null);
      eleCity.setTextContent(elePersonInfoShipTo
          .getAttribute(GCConstants.CITY));
      eleDestination.appendChild(eleCity);

      Element eleMainDivision = GCXMLUtil.createElement(docVertexCallIP,
          "MainDivision", null);
      eleMainDivision.setTextContent(elePersonInfoShipTo
          .getAttribute(GCConstants.STATE));
      eleDestination.appendChild(eleMainDivision);

      Element elePostalCode = GCXMLUtil.createElement(docVertexCallIP,
          "PostalCode", null);
      elePostalCode.setTextContent(elePersonInfoShipTo
          .getAttribute(GCConstants.ZIP_CODE));
      eleDestination.appendChild(elePostalCode);

      Element eleCountry = GCXMLUtil.createElement(docVertexCallIP,
          "Country", null);
      eleCountry.setTextContent(elePersonInfoShipTo
          .getAttribute(GCConstants.COUNTRY));
      eleDestination.appendChild(eleCountry);
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateHeaderTaxUE.fetchLineItemId() ",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCRecalculateHeaderTaxUE.fetchLineItemId() method ");
  }

  public String fetchLineItemId(String sChargeCategory, String sChargeName)
      throws GCException {
    LOGGER.debug(" Entering GCRecalculateHeaderTaxUE.fetchLineItemId() method ");
    LOGGER.debug(" sChargeCategory " + sChargeCategory + " sChargeName "
        + sChargeName);
    String sLineItemId = "";
    try {
      if (sChargeCategory.contains(GCConstants.SHIPPING)
          || sChargeCategory.contains(GCConstants.OVERSIZE)
          || sChargeCategory.contains("MiscellaneousCharges")) {
        sLineItemId = "M-";
      } else {
        sLineItemId = "L-";
      }
      sLineItemId = sLineItemId + sChargeCategory + "_" + sChargeName;
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateHeaderTaxUE.fetchLineItemId() ",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCRecalculateHeaderTaxUE.fetchLineItemId() method ");
    return sLineItemId;
  }

  private void addLineItemAttributes(Element eleLineItem, String sLineItemId,
      String sDate) throws GCException {
    LOGGER.debug(" Entering GCRecalculateHeaderTaxUE.addLineItemAttributes() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" addLineItemAttributes() method, eleLineItem is"
          + GCXMLUtil.getElementXMLString(eleLineItem));
    }
    LOGGER.debug("sLineItemId" + sLineItemId + " sDate" + sDate);
    try {
      eleLineItem.setAttribute("lineItemId", sLineItemId);
      eleLineItem.setAttribute("isMulticomponent", "false");
      eleLineItem.setAttribute("lineItemNumber",
          String.valueOf(iLineItemNumber));
      eleLineItem.setAttribute("taxdate", sDate);
      iLineItemNumber = iLineItemNumber + 1;
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateHeaderTaxUE.addLineItemAttributes() ",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCRecalculateHeaderTaxUE.addLineItemAttributes() method ");
  }

  private void updateColTax(YFSExtnTaxCalculationOutStruct outStruct) {
    List<YFSExtnTaxBreakup> proratedColTax = new ArrayList<YFSExtnTaxBreakup>();
    for (int iCounter = 0; iCounter < colCharge.size(); iCounter++) {
      LOGGER.debug(" Looping on colCharge ");
      YFSExtnHeaderChargeStruct inputStructlineCharge = colCharge
          .get(iCounter);
      String strChargeCategory = inputStructlineCharge.chargeCategory;
      String strChargeName = "";
      if (!YFCCommon.isVoid(strChargeCategory)) {
        strChargeCategory = strChargeCategory.trim();
      }
      strChargeName = inputStructlineCharge.chargeName;
      if (!YFCCommon.isVoid(strChargeName)) {
        strChargeName = strChargeName.trim();
      }
      if (!(GCConstants.SHIPPING_PROMOTION
          .equalsIgnoreCase(strChargeCategory) || GCConstants.PROMOTION
          .equalsIgnoreCase(strChargeCategory))) {
        double dResponseTax = 0.00;
        YFSExtnTaxBreakup extnTaxBreakup = new YFSExtnTaxBreakup();
        extnTaxBreakup.chargeCategory = strChargeCategory;
        extnTaxBreakup.chargeName = strChargeName;
        extnTaxBreakup.taxName = strChargeName + "Tax";
        extnTaxBreakup.taxPercentage = 0.00;
        extnTaxBreakup.reference3 = "ERROR";
        extnTaxBreakup.tax = dResponseTax;
        proratedColTax.add(extnTaxBreakup);
      }
      LOGGER.debug(" proratedColTax" + proratedColTax);
      // GCSTORE-3104::Begin
      outStruct.colTax = proratedColTax;
      // GCSTORE-3104::End
    }
  }

  private void createEmptyOutStruct(YFSExtnTaxCalculationOutStruct outStruct)
      throws GCException {
    LOGGER.debug(" Entering GCRecalculateLineTaxUE.createEmptyOutStruct() method ");
    try {
      outStruct.colTax = colTax;
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateLineTaxUE.createEmptyOutStruct() ",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCRecalculateLineTaxUE.createEmptyOutStruct() method ");
  }
}
