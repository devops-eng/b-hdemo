/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *  *#################################################################################################################################################################
 *	        OBJECTIVE: The class invokes Vertex and recalculate taxes on the order line.
 *#################################################################################################################################################################
 *	        Version			   Date              Modified By             	Description
 *#################################################################################################################################################################
             1.0               20/05/2014        Singh, Gurpreet            Tax Calculations

             1.1              02/02/2015          Kumar, Rakesh              OMS:4727 Production OMS: No sales tax was applied to a production order in OMS

             1.2               03/09/2015        Pande, Anuj                POS Invoicing
             2.0			   08/06/2017		 Expicient			 		GCSTORE-4380: Rounding for sets can cause tax and payment discrepancy
 *#################################################################################################################################################################
 */
package com.gc.userexit;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.gc.api.GCProcessTaxCalculationsAPI;
import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCWebServiceUtil;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.core.YFSSystem;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSExtnLineChargeStruct;
import com.yantra.yfs.japi.YFSExtnLineTaxCalculationInputStruct;
import com.yantra.yfs.japi.YFSExtnTaxBreakup;
import com.yantra.yfs.japi.YFSExtnTaxCalculationOutStruct;
import com.yantra.yfs.japi.ue.YFSRecalculateLineTaxUE;

/**
 * @author <a href="mailto:gurpreet.singh@expicient.com">Gurpreet</a>
 */
public class GCRecalculateLineTaxUE implements YFSRecalculateLineTaxUE {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCRecalculateLineTaxUE.class);

  private String orderHeaderKey;
  private String documentType;
  private String orderLineKey;
  private Boolean bForInvoice;
  private Boolean bForPacklistPrice;
  private Double currentQty;
  private Double invoicedPricingQty;
  private Double lineQty;
  private Double unitPrice;
  private String itemId;
  private String shipToCity;
  private String shipToCountry;
  private String shipToState;
  private String shipToZipCode;
  String sDraftOrderFlag = "";
  private List<YFSExtnTaxBreakup> colTax;
  private List<YFSExtnLineChargeStruct> colCharge;

  int iLineItemNumber = 0;

  @Override
  public YFSExtnTaxCalculationOutStruct recalculateLineTax(
      YFSEnvironment env, YFSExtnLineTaxCalculationInputStruct inStruct) {
    LOGGER.debug(" Entering GCRecalculateLineTaxUE.recalculateLineTax() method ");
    YFSExtnTaxCalculationOutStruct outStruct = new YFSExtnTaxCalculationOutStruct();
    try {
      Document docVertexWebServiceOP = null;
      // Parse input struct
      parseInputStruct(inStruct);
      LOGGER.debug(" env.getTxnObject(orderHeaderKey) "
          + env.getTxnObject("OL_" + orderHeaderKey));
      LOGGER.debug(" env.getTxnObject(isVertexDown)) "
          + env.getTxnObject("isVertexDown"));
      LOGGER.debug(" env.getTxnObject(isProration)) "
          + env.getTxnObject("isProration"));
      LOGGER.debug(" env.getTxnObject(isBundleLineCreateOrder)) "
          + env.getTxnObject("isBundleLineCreateOrder"));
      LOGGER.debug(" env.getTxnObject(ConfirmDraftOrderInvoked)) "
              + env.getTxnObject(GCConstants.ENV_CONFIRM_DRAFT_ORDER_INVOKED));
      // Checking DoNotSendCancellationEMail flag, this flag is set in split order functionality ,
      // and changeOrder API is fired to spit the order, we are skipping vertex call for this
      // changeOrder
      LOGGER.debug(" env.getTxnObject(DoNotSendCancellationEMail)) "
          + env.getTxnObject("DoNotSendCancellationEMail"));
      
      if (GCConstants.YES.equals(env.getTxnObject("isVertexDown"))
          || (bForPacklistPrice)
          || GCConstants.YES.equals(env.getTxnObject("isProration"))
          || GCConstants.YES.equals(env
              .getTxnObject("isBundleLineCreateOrder"))
              || (GCConstants.RETURN_ORDER_DOCUMENT_TYPE
                  .equals(documentType) && colCharge.isEmpty())
                  || GCConstants.PO_DOCUMENT_TYPE.equals(documentType)
                  || (GCConstants.RETURN_ORDER_DOCUMENT_TYPE
                      .equals(documentType) && bForInvoice)
                      || GCConstants.YES
                      .equals(env.getTxnObject("IsNewExchangeOrder_"
                          + documentType)) ||
                          GCConstants.YES.equals(env.getTxnObject("DoNotSendCancellationEMail") )
                        		 || GCConstants.YES.equals(env.getTxnObject(GCConstants.ENV_CONFIRM_DRAFT_ORDER_INVOKED))) {
    	  //GCSTORE-4380: to suppress call if GCConstants.ENV_CONFIRM_DRAFT_ORDER_INVOKED =Y
        LOGGER.debug(" Inside if create Empty OutStruct ");
        createEmptyOutStruct(outStruct);
      } else if (env.getTxnObject("OL_" + orderHeaderKey) == null
          && !(GCConstants.YES
              .equals(env.getTxnObject("isProration")))
              && !(GCConstants.YES.equals(env
                  .getTxnObject("isVertexDown")))
                  && !(GCConstants.YES.equals(env
                      .getTxnObject("isBundleLineCreateOrder")))
                      && !(bForPacklistPrice)) {
        LOGGER.debug(" Inside if sales order document and orderHeaderKey is not in env ");
        Object flag=env.getTxnObject("BundleTaxesProratedForReturn");
        if (GCConstants.RETURN_ORDER_DOCUMENT_TYPE.equals(documentType)
            && !YFCCommon.isVoid(flag) && GCConstants.YES.equals(flag.toString())) {
          createEmptyOutStruct(outStruct);
          return outStruct;
        }
        Document docGetOrderListIP = GCXMLUtil
            .createDocument(GCConstants.ORDER);
        Element eleGetOrderListIP = docGetOrderListIP
            .getDocumentElement();
        eleGetOrderListIP.setAttribute(GCConstants.ORDER_HEADER_KEY,
            orderHeaderKey);
        Document docGetOrderListTmp = GCCommonUtil
            .getDocumentFromPath("/global/template/api/getOrderList.xml");
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug(" recalculateLineTax() method, docGetOrderListTmp is"
              + GCXMLUtil.getXMLString(docGetOrderListTmp));
        }

        Document docGetOrderListOP = GCCommonUtil.invokeAPI(env,
            GCConstants.API_GET_ORDER_LIST, docGetOrderListIP,
            docGetOrderListTmp);
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug(" recalculateLineTax() method, docGetOrderListOP is"
              + GCXMLUtil.getXMLString(docGetOrderListOP));
        }

        String sEntryType = GCXMLUtil.getAttributeFromXPath(docGetOrderListOP, "OrderList/Order/@EntryType");
        String sDraftOrderFlag = GCXMLUtil.getAttributeFromXPath(docGetOrderListOP, "OrderList/Order/@DraftOrderFlag");
        if (YFCCommon.equalsIgnoreCase(sEntryType, "mPOS") && YFCCommon.equals(sDraftOrderFlag, "Y")) {
          createEmptyOutStruct(outStruct);
          return outStruct;
        }

        sDraftOrderFlag = GCXMLUtil.getAttributeFromXPath(
            docGetOrderListOP, "OrderList/Order/@DraftOrderFlag");
        /* POS Invoicing - Start*/
        Element eleOrder = (Element)docGetOrderListOP.getDocumentElement().getElementsByTagName(GCConstants.ORDER).item(0);
        String sSellerOrganizationCode = eleOrder.getAttribute(GCConstants.SELLER_ORGANIZATION_CODE);
        if(!YFCCommon.isStringVoid(sSellerOrganizationCode) &&
            GCConstants.INTL_WEB.equalsIgnoreCase(sSellerOrganizationCode)){
          createEmptyOutStruct(outStruct);
          return outStruct;
        }
        /* POS Invoicing - End*/
        Document docGetOrderInvoiceDetailListOP = fecthOrderInvoiceDetailList(env);

        Document docVertexCallIP = prepareVertexWebServiceIP(
            docGetOrderListOP, docGetOrderInvoiceDetailListOP);
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug(" recalculateLineTax() method, docVertexCallIP is"
              + GCXMLUtil.getXMLString(docVertexCallIP));
        }

        GCWebServiceUtil obj = new GCWebServiceUtil();

        docVertexWebServiceOP = obj
            .invokeSOAPWebService(
                docVertexCallIP,
                YFSSystem
                .getProperty(GCConstants.VERTEX_WEB_SERVICE_URL),
                null);
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug(" recalculateLineTax() method, docVertexWebServiceOP is"
              + GCXMLUtil.getXMLString(docVertexWebServiceOP));
        }

        Element eleTaxVertexOP = GCXMLUtil.getElementByXPath(
            docVertexWebServiceOP, "Envelope/Body/VertexEnvelope"
                + "/QuotationResponse/TotalTax");
        if (YFCCommon.isVoid(eleTaxVertexOP)) {
          env.setTxnObject("isVertexDown", GCConstants.YES);
          createEmptyOutStruct(outStruct);
          return outStruct;
        } else {
          LOGGER.debug(" Inside else set orderHeaderKey in ENV ");
          env.setTxnObject("OL_" + orderHeaderKey,
              docVertexWebServiceOP);
        }

        Document docSalesOrderList = null;
        String sOrderHeaderKeyEnv = orderHeaderKey + "_" + documentType;
        if (GCConstants.RETURN_ORDER_DOCUMENT_TYPE.equals(documentType)
            && env.getTxnObject(sOrderHeaderKeyEnv) != null) {
          LOGGER.debug(" Inside if Document Type is Return Order ");
          docSalesOrderList = (Document) env
              .getTxnObject(sOrderHeaderKeyEnv);
          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(" docSalesOrderList is"
                + GCXMLUtil.getXMLString(docSalesOrderList));
          }
        }

        prepareOutStruct(env, inStruct, docVertexWebServiceOP,
            outStruct, docSalesOrderList);

      } else if (!bForPacklistPrice) {
        LOGGER.debug(" Inside else sales order document and orderHeaderKey is not in env ");
        docVertexWebServiceOP = (Document) env.getTxnObject("OL_"
            + orderHeaderKey);
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug(" recalculateLineTax() method, docVertexWebServiceOP is"
              + GCXMLUtil.getXMLString(docVertexWebServiceOP));
        }

        prepareOutStruct(env, inStruct, docVertexWebServiceOP,
            outStruct, null);
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateLineTaxUE.recalculateLineTax()",
          e);
      updateColTax(outStruct);
    }
    LOGGER.debug(" Exiting GCRecalculateLineTaxUE.recalculateLineTax() method ");
    return outStruct;
  }

  private Document fecthOrderInvoiceDetailList(YFSEnvironment env)
      throws GCException {
    LOGGER.debug(" Entering GCRecalculateLineTaxUE.fecthOrderInvoiceDetailList() method ");
    Document docGetOrderInvoiceDetailListOP = null;
    try {
      Document docGetOrderInvoiceDetailListIP = GCXMLUtil
          .createDocument("OrderInvoiceDetail");
      Element eleGetOrderInvoiceDetailListIP = docGetOrderInvoiceDetailListIP
          .getDocumentElement();
      Element eleInvoiceHeader = GCXMLUtil.createElement(
          docGetOrderInvoiceDetailListIP, "InvoiceHeader", null);
      eleInvoiceHeader.setAttribute(GCConstants.ORDER_HEADER_KEY,
          orderHeaderKey);
      eleGetOrderInvoiceDetailListIP.appendChild(eleInvoiceHeader);

      docGetOrderInvoiceDetailListOP = GCCommonUtil.invokeAPI(env,
          GCConstants.API_GET_ORDER_INVOICE_DETAIL_LIST,
          docGetOrderInvoiceDetailListIP, null);
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateLineTaxUE.fecthOrderInvoiceDetailList() ",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCRecalculateLineTaxUE.fecthOrderInvoiceDetailList() method ");
    return docGetOrderInvoiceDetailListOP;
  }

  private Document prepareVertexWebServiceIP(Document docGetOrderListOP,
      Document docGetOrderInvoiceDetailListOP) throws GCException {
    LOGGER.debug(" Entering GCRecalculateLineTaxUE.prepareVertexWebServiceIP() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" prepareVertexWebServiceIP() method, docGetOrderListOP is"
          + GCXMLUtil.getXMLString(docGetOrderListOP));
      LOGGER.debug(" prepareVertexWebServiceIP() method, docGetOrderInvoiceDetailListOP is"
          + GCXMLUtil.getXMLString(docGetOrderInvoiceDetailListOP));
    }
    Document docVertexCallIP = null;
    try {

      docVertexCallIP = GCCommonUtil
          .getDocumentFromPath("/global/template/api/extn_VertexCallRequest.xml");
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(" prepareVertexWebServiceIP() method, docVertexCallIP is"
            + GCXMLUtil.getXMLString(docVertexCallIP));
      }

      Element eleOrder = (Element) XPathAPI.selectSingleNode(
          docGetOrderListOP, "OrderList/Order");
      String sOrderNo = eleOrder.getAttribute(GCConstants.ORDER_NO);
      String sTaxExemptFlag = eleOrder
          .getAttribute(GCConstants.TAX_EXEMPT_FLAG);
      sTaxExemptFlag = (GCConstants.YES.equalsIgnoreCase(sTaxExemptFlag)) ? "true"
          : "false";
      String sOrderDate = eleOrder.getAttribute(GCConstants.ORDER_DATE);
      if (YFCCommon.isVoid(sOrderDate)) {
        Calendar calRemorseDate = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat(
            GCConstants.DATE_TIME_FORMAT);
        sOrderDate = dateFormat.format(calRemorseDate.getTime());
        LOGGER.debug(" sOrderDate " + sOrderDate);
      }
      sDraftOrderFlag = eleOrder.getAttribute("DraftOrderFlag");
      //OMS-4727 Starts
      Element eTrustedId=GCXMLUtil.getElementByXPath(docVertexCallIP,"Envelope/Body/VertexEnvelope/Login/TrustedId");
      String sTrustedId=YFSSystem.getProperty(GCConstants.VERTEX_CALL_REQUEST_TRUSTED_ID);
      eTrustedId.setTextContent(sTrustedId);
      //OMS-4727 Ends
      Element eQuotationRequest = GCXMLUtil.getElementByXPath(
          docVertexCallIP,
          "Envelope/Body/VertexEnvelope/QuotationRequest");
      if (!YFCCommon.isVoid(eQuotationRequest)) {
        LOGGER.debug(" Inside if(!YFCCommon.isVoid(eQuotationRequest) ");

        eQuotationRequest.setAttribute("documentDate", sOrderDate);
        eQuotationRequest.setAttribute("transactionId", sOrderNo);
        eQuotationRequest.setAttribute("transactionType", "SALE");

        XPathAPI.selectSingleNode(eQuotationRequest, "Seller/Company")
        .setTextContent("99");
        XPathAPI.selectSingleNode(eQuotationRequest, "Seller/Division")
        .setTextContent("02");

        Element eleHeaderCustomer = (Element) XPathAPI
            .selectSingleNode(eQuotationRequest, "Customer");
        eleHeaderCustomer.setAttribute("isTaxExempt", sTaxExemptFlag);
        Element eleHeaderCustomerCode = (Element) XPathAPI
            .selectSingleNode(eleHeaderCustomer, "CustomerCode");
        eleHeaderCustomerCode.setAttribute("classCode", "MAIN");
        Element eleHeaderDestination = (Element) XPathAPI
            .selectSingleNode(eleHeaderCustomer, "Destination");

        // Append Header Ship To element into the Vertex I/P XML
        Element eleHeaderPersonInfoShipTo = (Element) XPathAPI
            .selectSingleNode(eleOrder, "PersonInfoShipTo");
        if (!YFCCommon.isVoid(eleHeaderPersonInfoShipTo)) {
          LOGGER.debug("Inside if append header person info ship to");
          appendPersonInfoShipTo(docVertexCallIP,
              eleHeaderPersonInfoShipTo, eleHeaderDestination);
        }

        NodeList nlOrderLine = XPathAPI.selectNodeList(
            docGetOrderListOP,
            "OrderList/Order/OrderLines/OrderLine");
        for (int iLineCounter = 0; iLineCounter < nlOrderLine
            .getLength(); iLineCounter++) {
          LOGGER.debug(" looping on the orderlines");
          Element eleOrderLine = (Element) nlOrderLine
              .item(iLineCounter);
          String sOrderLineKey = eleOrderLine
              .getAttribute(GCConstants.ORDER_LINE_KEY);

          String sQuantity = "";
          double dQuantity = 0.00;
          String sOriginalQty = eleOrderLine
              .getAttribute(GCConstants.ORDERED_QTY);
          if (bForInvoice) {
            LOGGER.debug(" if(bForInvoice) is true");
            // OMS-5478 Start
            dQuantity = currentQty;
            // OMS-5478 End
          } else {
            LOGGER.debug(" else (bForInvoice) is false");
            sQuantity = eleOrderLine
                .getAttribute(GCConstants.ORDERED_QTY);
            dQuantity = Double.valueOf(sQuantity);
          }
          double dOriginalQty = Double.valueOf(sOriginalQty);
          Element eleItem = (Element) XPathAPI.selectSingleNode(
              eleOrderLine, GCConstants.ITEM);
          String sItemID = eleItem.getAttribute(GCConstants.ITEM_ID);
          String sTaxProductCode = GCXMLUtil.getAttributeFromXPath(
              eleOrderLine,
              "ItemDetails/ClassificationCodes/@TaxProductCode");

          boolean bIsPriceCharge = false;

          if (!bIsPriceCharge
              && GCConstants.SALES_ORDER_DOCUMENT_TYPE
              .equals(documentType)) {
            LOGGER.debug(" Inside if charge category is Price ");
            appendPriceToVertexIP(docVertexCallIP,
                eQuotationRequest, sOrderDate, sTaxExemptFlag,
                eleOrderLine);
          }

          NodeList nlLineCharge = XPathAPI.selectNodeList(
              eleOrderLine, "LineCharges/LineCharge");
          for (int iLineChargeCounter = 0; iLineChargeCounter < nlLineCharge
              .getLength(); iLineChargeCounter++) {
            Element eleLineCharge = (Element) nlLineCharge
                .item(iLineChargeCounter);
            String sChargeCategory = eleLineCharge
                .getAttribute(GCConstants.CHARGE_CATEGORY);
            String sChargeName = eleLineCharge
                .getAttribute(GCConstants.CHARGE_NAME);
            String sLineItemId = fetchLineItemId(sChargeCategory,
                sChargeName, sOrderLineKey);

            if (!(GCConstants.SHIPPING_PROMOTION
                .equalsIgnoreCase(sChargeCategory) || GCConstants.PROMOTION
                .equalsIgnoreCase(sChargeCategory) || GCConstants.CUSTOMER_APPEASEMENT.equalsIgnoreCase(sChargeCategory))) {
              LOGGER.debug(" Inside if charge category is not Shipping Promotion or Promotion ");
              String sChargePerUnit = eleLineCharge
                  .getAttribute(GCConstants.CHARGE_PER_UNIT);
              double dChargePerUnit = (!YFCCommon
                  .isVoid(sChargePerUnit)) ? Double
                      .valueOf(sChargePerUnit) : 0.00;
                      String sChargePerLine = eleLineCharge
                          .getAttribute(GCConstants.CHARGE_PER_LINE);
                      double dChargePerLine = (!YFCCommon
                          .isVoid(sChargePerLine)) ? Double
                              .valueOf(sChargePerLine) : 0.00;
                              double dChargeAmount = 0.00;
                              String sPrice = "0.00";

                              LOGGER.debug("sChargePerUnit" + sChargePerUnit
                                  + "sChargePerLine" + sChargePerLine);
                              Element eleLineItem = GCXMLUtil.createElement(
                                  docVertexCallIP, "LineItem", null);
                              LOGGER.debug(" Inside else, charge category is not Price ");
                              addLineItemAttributes(eleLineItem, sLineItemId,
                                  sOrderDate);

                              if (GCConstants.SHIPPING
                                  .equalsIgnoreCase(sChargeCategory)
                                  && sChargeName.contains("ShippingCharge")) {
                                LOGGER.debug(" Inside if Charge Category is Shipping ");
                                NodeList nlShippingPromotion = XPathAPI
                                    .selectNodeList(eleOrderLine,
                                        "LineCharges/LineCharge[@ChargeCategory='ShippingPromotion']");
                                for (int iCounter = 0; iCounter < nlShippingPromotion
                                    .getLength(); iCounter++) {
                                  LOGGER.debug(" Looping on ShippingPromotion charges ");
                                  Element eleShippingPromotion = (Element) nlShippingPromotion
                                      .item(iCounter);
                                  String sShippingPromotion = eleShippingPromotion
                                      .getAttribute(GCConstants.CHARGE_PER_LINE);
                                  double dShippingPromotion = (!YFCCommon
                                      .isVoid(sShippingPromotion)) ? Double
                                          .valueOf(sShippingPromotion) : 0.00;
                                          dChargePerLine = dChargePerLine
                                              - dShippingPromotion;
                                }
                              }

                              if (dChargePerUnit > 0.00) {
                                sPrice = sChargePerUnit;
                                dChargeAmount = dChargePerUnit * dQuantity;
                              } else if (dChargePerLine > 0.00) {
                                double dPrice = (dChargePerLine * dQuantity)
                                    / dOriginalQty;
                                sPrice = String.valueOf(dPrice);
                                dChargeAmount = dPrice;
                              }
                              eQuotationRequest.appendChild(eleLineItem);

                              addProductInfo(docVertexCallIP, eleLineItem,
                                  sChargeCategory, sItemID, sTaxProductCode);

                              Element eleQuantity = GCXMLUtil.createElement(
                                  docVertexCallIP, "Quantity", null);
                              eleQuantity.setTextContent(String
                                  .valueOf(dQuantity));
                              eleLineItem.appendChild(eleQuantity);

                              Element eleFreight = GCXMLUtil.createElement(
                                  docVertexCallIP, "Freight", null);
                              eleFreight.setTextContent("0");
                              eleLineItem.appendChild(eleFreight);

                              Element eleUnitPrice = GCXMLUtil.createElement(
                                  docVertexCallIP, "UnitPrice", null);
                              eleUnitPrice.setTextContent(sPrice);
                              eleLineItem.appendChild(eleUnitPrice);

                              Element eleExtendedPrice = GCXMLUtil.createElement(
                                  docVertexCallIP, "ExtendedPrice", null);
                              eleExtendedPrice.setTextContent(String
                                  .valueOf(dChargeAmount));
                              eleLineItem.appendChild(eleExtendedPrice);

                              Element eleCustomer = GCXMLUtil.createElement(
                                  docVertexCallIP, "Customer", null);
                              eleCustomer.setAttribute("isTaxExempt",
                                  sTaxExemptFlag);
                              eleLineItem.appendChild(eleCustomer);

                              Element eleCustomerCode = GCXMLUtil.createElement(
                                  docVertexCallIP, "CustomerCode", null);
                              eleCustomerCode.setAttribute("classCode", "MAIN");
                              eleCustomer.appendChild(eleCustomerCode);

                              Element elePersonInfoShipTo = (Element) XPathAPI
                                  .selectSingleNode(eleOrderLine,
                                      "PersonInfoShipTo");
                              if (!YFCCommon.isVoid(elePersonInfoShipTo)
                                  && !YFCUtils.equals(documentType, GCConstants.RETURN_ORDER_DOCUMENT_TYPE)) {
                                Element eleDestination = GCXMLUtil
                                    .createElement(docVertexCallIP,
                                        "Destination", null);
                                eleCustomer.appendChild(eleDestination);
                                appendPersonInfoShipTo(docVertexCallIP,
                                    elePersonInfoShipTo, eleDestination);
                              }
            }
          }
        }

      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateLineTaxUE.prepareVertexWebServiceIP() ",
          e);
      throw new GCException(e);
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" GCRecalculateLineTaxUE.prepareVertexWebServiceIP() method, updated docVertexCallIP is"
          + GCXMLUtil.getXMLString(docVertexCallIP));
    }
    LOGGER.debug(" Exiting GCRecalculateLineTaxUE.prepareVertexWebServiceIP() method ");
    return docVertexCallIP;
  }

  private void appendPersonInfoShipTo(Document docVertexCallIP,
      Element elePersonInfoShipTo, Element eleDestination)
          throws GCException {
    LOGGER.debug(" Entering GCRecalculateLineTaxUE.appendPersonInfoShipTo() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" appendPersonInfoShipTo() method, docVertexCallIP is"
          + GCXMLUtil.getXMLString(docVertexCallIP));
      LOGGER.debug(" appendPersonInfoShipTo() method, elePersonInfoShipTo is"
          + GCXMLUtil.getElementXMLString(elePersonInfoShipTo));
      LOGGER.debug(" appendPersonInfoShipTo() method, eleDestination is"
          + GCXMLUtil.getElementXMLString(eleDestination));
    }
    try {
      Element eleStreetAddress1 = GCXMLUtil.createElement(
          docVertexCallIP, "StreetAddress1", null);
      eleStreetAddress1.setTextContent(elePersonInfoShipTo
          .getAttribute(GCConstants.ADDRESS_LINE1));
      eleDestination.appendChild(eleStreetAddress1);

      Element eleCity = GCXMLUtil.createElement(docVertexCallIP, "City",
          null);
      eleCity.setTextContent(elePersonInfoShipTo
          .getAttribute(GCConstants.CITY));
      eleDestination.appendChild(eleCity);

      Element eleMainDivision = GCXMLUtil.createElement(docVertexCallIP,
          "MainDivision", null);
      eleMainDivision.setTextContent(elePersonInfoShipTo
          .getAttribute(GCConstants.STATE));
      eleDestination.appendChild(eleMainDivision);

      Element elePostalCode = GCXMLUtil.createElement(docVertexCallIP,
          "PostalCode", null);
      elePostalCode.setTextContent(elePersonInfoShipTo
          .getAttribute(GCConstants.ZIP_CODE));
      eleDestination.appendChild(elePostalCode);

      Element eleCountry = GCXMLUtil.createElement(docVertexCallIP,
          "Country", null);
      eleCountry.setTextContent(elePersonInfoShipTo
          .getAttribute(GCConstants.COUNTRY));
      eleDestination.appendChild(eleCountry);

    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateLineTaxUE.appendPersonInfoShipTo() ",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCRecalculateLineTaxUE.appendPersonInfoShipTo() ");
  }

  private void addLineItemAttributes(Element eleLineItem, String sLineItemId,
      String sDate) throws GCException {
    LOGGER.debug(" Entering GCRecalculateLineTaxUE.addLineItemAttributes() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" appendPersonInfoShipTo() method, eleLineItem is"
          + GCXMLUtil.getElementXMLString(eleLineItem));
    }
    LOGGER.debug("sLineItemId" + sLineItemId + " sDate" + sDate);
    try {
      eleLineItem.setAttribute("lineItemId", sLineItemId);
      eleLineItem.setAttribute("isMulticomponent", "false");
      eleLineItem.setAttribute("lineItemNumber",
          String.valueOf(iLineItemNumber));
      eleLineItem.setAttribute("taxdate", sDate);
      iLineItemNumber = iLineItemNumber + 1;
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateLineTaxUE.addLineItemAttributes() ",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCRecalculateLineTaxUE.addLineItemAttributes() method ");
  }

  public static void addProductInfo(Document docVertexCallIP,
      Element eleLineItem, String sChargeCategory, String sItemID,
      String sTaxProductCode) throws GCException {
    LOGGER.debug(" Exiting GCRecalculateLineTaxUE.addProductInfo() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" addProductInfo() method, docVertexCallIP is"
          + GCXMLUtil.getXMLString(docVertexCallIP));
      LOGGER.debug(" addProductInfo() method, eleLineItem is"
          + GCXMLUtil.getElementXMLString(eleLineItem));
    }
    LOGGER.debug(" sChargeCategory" + sChargeCategory + " sItemID"
        + sItemID);
    try {
      Element eleProduct = GCXMLUtil.createElement(docVertexCallIP,
          "Product", null);
      if (sChargeCategory.contains(GCConstants.SHIPPING)) {
        eleProduct.setAttribute("productClass", "9999");
      } else {
        eleProduct.setAttribute("productClass", sTaxProductCode);
      }
      eleProduct.setTextContent(sItemID);
      eleLineItem.appendChild(eleProduct);
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateLineTaxUE.addProductInfo() ",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCRecalculateLineTaxUE.addProductInfo() method ");
  }

  private void parseInputStruct(
      YFSExtnLineTaxCalculationInputStruct inputStruct)
          throws GCException {
    LOGGER.debug(" Entering GCRecalculateLineTaxUE.parseInputStruct() method ");
    orderHeaderKey = inputStruct.orderHeaderKey;
    orderLineKey = inputStruct.orderLineKey;
    documentType = inputStruct.documentType;
    bForInvoice = inputStruct.bForInvoice;
    bForPacklistPrice = inputStruct.bForPacklistPrice;
    currentQty = inputStruct.currentQty;
    invoicedPricingQty = inputStruct.invoicedPricingQty;
    lineQty = inputStruct.lineQty;
    colTax = inputStruct.colTax;
    colCharge = inputStruct.colCharge;
    unitPrice = inputStruct.unitPrice;
    itemId = inputStruct.itemId;
    shipToCity = inputStruct.shipToCity;
    shipToCountry = inputStruct.shipToCountry;
    shipToState = inputStruct.shipToState;
    shipToZipCode = inputStruct.shipToZipCode;
    LOGGER.debug(" Exiting GCRecalculateLineTaxUE.parseInputStruct() method ");
  }

  public static String fetchLineItemId(String sChargeCategory,
      String sChargeName, String sOrderLineKey) throws GCException {
    LOGGER.debug(" Entering GCRecalculateLineTaxUE.fetchLineItemId() method ");
    String sLineItemId = "";
    try {
      if (sChargeCategory.contains(GCConstants.SHIPPING)
          || sChargeCategory.contains(GCConstants.OVERSIZE)
          || sChargeCategory.contains("MiscellaneousCharges")) {
        sLineItemId = "M-";
      } else {
        sLineItemId = "L-";
      }
      sLineItemId = sLineItemId + sOrderLineKey + "_" + sChargeCategory
          + "_" + sChargeName;
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateLineTaxUE.fetchLineItemId() ",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCRecalculateLineTaxUE.fetchLineItemId() method ");
    return sLineItemId;
  }

  public YFSExtnTaxCalculationOutStruct prepareOutStruct(YFSEnvironment env,
      YFSExtnLineTaxCalculationInputStruct inStruct,
      Document docVertexWebServiceOP,
      YFSExtnTaxCalculationOutStruct outStruct, Document docSalesOrderList)
          throws GCException {
    LOGGER.debug(" Entering GCRecalculateLineTaxUE.prepareOutStruct() method ");
    try {
      List<YFSExtnTaxBreakup> proratedColTax = new ArrayList<YFSExtnTaxBreakup>();
      boolean bIsPriceCharge = false;
      if (!bIsPriceCharge) {
        LOGGER.debug(" Inside if(!bIsPriceCharge) ");
        String strChargeCategory = "LinePrice";
        String strChargeName = "LinePriceTax";
        String sLineItemIdPrefix = "L-" + orderLineKey;
        Element eleLineItem = (Element) XPathAPI.selectSingleNode(
            docVertexWebServiceOP, "Envelope/Body/VertexEnvelope/"
                + "QuotationResponse/LineItem[@lineItemId='"
                + sLineItemIdPrefix + "']");
        double dResponseTax = 0.00;
        double dTaxPercentage = 0.00;
        if (!YFCCommon.isVoid(eleLineItem)) {
          String sResponseTax = XPathAPI.selectSingleNode(
              eleLineItem, "TotalTax").getTextContent();
          dResponseTax = Double.parseDouble(sResponseTax);
          /* POS Invoicing - Start */
          GCProcessTaxCalculationsAPI effectiverateTax = new GCProcessTaxCalculationsAPI();
          dTaxPercentage = effectiverateTax.sumEffectiveTaxRate(eleLineItem, dTaxPercentage);
        } else if (GCConstants.RETURN_ORDER_DOCUMENT_TYPE
            .equals(documentType)) {
          LOGGER.debug(" Inside Price and document type is return order ");
          List<Double> listTax = calculateReturnProratedTax(env, strChargeCategory, strChargeName, 0.0);
          dResponseTax = listTax.get(0);
          dTaxPercentage = listTax.get(0);
        }
        double dOriginaltax = 0.00;
        LOGGER.debug(" dOriginaltax" + strChargeCategory
            + " strChargeName" + strChargeName
            + " sLineItemIdPrefix" + sLineItemIdPrefix
            + " dResponseTax" + dResponseTax + " dOriginaltax"
            + dOriginaltax);
        updateProratedColTax(proratedColTax, strChargeCategory,
            strChargeName, dResponseTax, dOriginaltax, dTaxPercentage);
        /* POS Invoicing - End */
        bIsPriceCharge = true;
      }

      for (int iCounter = 0; iCounter < colCharge.size(); iCounter++) {
        LOGGER.debug(" Looping on colCharge ");
        YFSExtnLineChargeStruct inputStructlineCharge = colCharge
            .get(iCounter);
        String strChargeCategory = inputStructlineCharge.chargeCategory;
        double dChargeAmount = inputStructlineCharge.chargeAmount;
        String strChargeName = "";
        String sLineItemIdPrefix = "";
        Element eleLineItem = null;

        if (!YFCCommon.isVoid(strChargeCategory)) {
          strChargeCategory = strChargeCategory.trim();
        } else {
          strChargeCategory = GCConstants.BLANK;
        }
        if (GCConstants.SALES_ORDER_DOCUMENT_TYPE.equals(documentType)
            && !(GCConstants.SHIPPING_PROMOTION
                .equalsIgnoreCase(strChargeCategory) || GCConstants.PROMOTION
                .equalsIgnoreCase(strChargeCategory))) {
          strChargeName = inputStructlineCharge.chargeName;
          if (!YFCCommon.isVoid(strChargeName)) {
            strChargeName = strChargeName.trim();
          } else {
            strChargeName = GCConstants.BLANK;
          }

          if (strChargeCategory.contains(GCConstants.SHIPPING)) {
            sLineItemIdPrefix = "M-";
          } else {
            sLineItemIdPrefix = "L-";
          }
          eleLineItem = (Element) XPathAPI
              .selectSingleNode(
                  docVertexWebServiceOP,
                  "Envelope/Body/VertexEnvelope/"
                      + "QuotationResponse/LineItem[@lineItemId='"
                      + sLineItemIdPrefix + ""
                      + orderLineKey + "_"
                      + strChargeCategory + "_"
                      + strChargeName + "']");
          double dResponseTax = 0.00;
          double dTaxPercentage = 0.00;
          if (!YFCCommon.isVoid(eleLineItem)) {
            String sResponseTax = XPathAPI.selectSingleNode(
                eleLineItem, "TotalTax").getTextContent();
            dResponseTax = Double.parseDouble(sResponseTax);
            /* POS Invoicing - Start */
            GCProcessTaxCalculationsAPI effectiverateTax = new GCProcessTaxCalculationsAPI();
            dTaxPercentage = effectiverateTax.sumEffectiveTaxRate(eleLineItem, dTaxPercentage);
          }
          double dOriginaltax = 0.00;
          LOGGER.debug(" strChargeCategory" + strChargeCategory
              + " strChargeName" + strChargeName
              + " sLineItemIdPrefix" + sLineItemIdPrefix
              + " dResponseTax" + dResponseTax + " dOriginaltax"
              + dOriginaltax);

          updateProratedColTax(proratedColTax, strChargeCategory,
              strChargeName, dResponseTax, dOriginaltax, dTaxPercentage);
          /* POS Invoicing - End */
        } else if (GCConstants.RETURN_ORDER_DOCUMENT_TYPE
            .equals(documentType)) {

          strChargeName = inputStructlineCharge.chargeName;
          if (!YFCCommon.isVoid(strChargeName)) {
            strChargeName = strChargeName.trim();
          } else {
            strChargeName = GCConstants.BLANK;
          }

          if (strChargeCategory.contains("MiscellaneousCharges")) {
            sLineItemIdPrefix = "M-";
          }
          eleLineItem = (Element) XPathAPI
              .selectSingleNode(
                  docVertexWebServiceOP,
                  "Envelope/Body/VertexEnvelope/"
                      + "QuotationResponse/LineItem[@lineItemId='"
                      + sLineItemIdPrefix + ""
                      + orderLineKey + "_"
                      + strChargeCategory + "_"
                      + strChargeName + "']");
          double dResponseTax = 0.00;
          double dOriginaltax = 0.00;
          double dTaxPercentage = 0.00;
          if (!YFCCommon.isVoid(eleLineItem)) {
            String sResponseTax = XPathAPI.selectSingleNode(
                eleLineItem, "TotalTax").getTextContent();
            dResponseTax = Double.parseDouble(sResponseTax);
            /* POS Invoicing - Start */
            GCProcessTaxCalculationsAPI effectiverateTax = new GCProcessTaxCalculationsAPI();
            dTaxPercentage = effectiverateTax.sumEffectiveTaxRate(eleLineItem, dTaxPercentage);
          } else if (GCConstants.CUSTOMER_APPEASEMENT.equalsIgnoreCase(strChargeCategory)) {
            // Fetch customer appeasement tax from input struct
            for (int iCount = 0; iCount < colTax.size(); iCount++) {
              LOGGER.debug(" Looping on colTax to find customer appeasement charge category ");
              YFSExtnTaxBreakup inputStructlineTax = colTax.get(iCount);
              String sChargeCategory = inputStructlineTax.chargeCategory;
              if (GCConstants.CUSTOMER_APPEASEMENT.equalsIgnoreCase(sChargeCategory)) {
                dResponseTax = inputStructlineTax.tax;
                dTaxPercentage = inputStructlineTax.taxPercentage;
                break;
              }
            }
          } else {
            LOGGER.debug(" Inside calculate return prorated tax");
            List<Double> listTax;
            if(YFCCommon.equals(strChargeCategory, "ShippingRefund")){
              String newChargeCategory = "Shipping";
              String newChargeName = "ShippingCharge";
              listTax = calculateReturnProratedTax(env,
                  newChargeCategory, newChargeName, dChargeAmount);
            }
            else{
              listTax = calculateReturnProratedTax(env,
                  strChargeCategory, strChargeName, dChargeAmount);
            }
            dResponseTax = listTax.get(0);
            dTaxPercentage = listTax.get(0);
          }
          LOGGER.debug(" strChargeCategory" + strChargeCategory
              + " strChargeName" + strChargeName
              + " sLineItemIdPrefix" + sLineItemIdPrefix
              + " dResponseTax" + dResponseTax + " dOriginaltax"
              + dOriginaltax);
          updateProratedColTax(proratedColTax, strChargeCategory,
              strChargeName, dResponseTax, dOriginaltax, dTaxPercentage);
          /* POS Invoicing - End */
        }
        LOGGER.debug(" proratedColTax" + proratedColTax);
      }
      outStruct.colTax = proratedColTax;

    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateLineTaxUE.prepareOutStruct() ",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCRecalculateLineTaxUE.prepareOutStruct() method ");
    return outStruct;
  }

  private List<Double> calculateReturnProratedTax(YFSEnvironment env,
      String strChargeCategory, String strChargeName,
      double dChargeAmount) throws GCException {
    LOGGER.debug(" Entering GCRecalculateLineTaxUE.calculateReturnProratedTax() method ");
    double dProratedTax = 0.00;
    double dTaxPercentage = 0.00;
    List<Double> listLineTax = new ArrayList<Double>();
    try {
      LOGGER.debug(" strChargeCategory " + strChargeCategory + " strChargeName " + strChargeName);
      String sSalesOrderLineKey = fetchSalesOrderLineKey(env);
      Document docSalesOrderLineList = fetchOrderLineList(env,
          sSalesOrderLineKey);
      String sSalesOrderQty = GCXMLUtil.getAttributeFromXPath(
          docSalesOrderLineList,
          "OrderLineList/OrderLine/@OrderedQty");
      Element eleLineTax = GCXMLUtil.getElementByXPath(docSalesOrderLineList, "OrderLineList/OrderLine/LineTaxes/LineTax[@ChargeCategory='"
          + strChargeCategory + "' and @ChargeName='"
          + strChargeName + "']");
      String sParentLineKey =
          GCXMLUtil.getAttributeFromXPath(docSalesOrderLineList, "OrderLineList/OrderLine/@BundleParentOrderLineKey");

      if (YFCCommon.isVoid(eleLineTax) && "Shipping".equals(strChargeCategory) && !YFCCommon.isVoid(sParentLineKey)) {

        eleLineTax = fetchParentLineTax(env, docSalesOrderLineList, strChargeCategory, strChargeName);

      } else if (!("Shipping".equals(strChargeCategory)) && !YFCCommon.isVoid(sParentLineKey)) {
        // Fetching returnOrderLine Details as LinePriceTax is already calculated in ReturnOrder

        LOGGER.debug(" Inside if chargeCategory != shipping and it is a bundle component");
        LOGGER.verbose("orderLineKey : " + orderLineKey);
        Document docReturnOrderLineList = fetchOrderLineList(env, orderLineKey);
        eleLineTax =
            GCXMLUtil.getElementByXPath(docReturnOrderLineList,
                "OrderLineList/OrderLine/LineTaxes/LineTax[@ChargeCategory='" + strChargeCategory
                + "' and @ChargeName='" + strChargeName + "']");
        String sReturnOrderTax = eleLineTax.getAttribute(GCConstants.TAX);
        String sReturnTaxPercentage = eleLineTax.getAttribute(GCConstants.TAX_PERCENTAGE);
        dTaxPercentage = Double.parseDouble(sReturnTaxPercentage);
        dProratedTax = (!YFCCommon.isVoid(sReturnOrderTax)) ? Double.valueOf(sReturnOrderTax) : 0.00;

        listLineTax.add(0, dProratedTax);
        listLineTax.add(1, dTaxPercentage);
        LOGGER.debug(" Exiting GCRecalculateLineTaxUE.calculateReturnProratedTax() method ");
        return listLineTax;
      }

      String sSalesOrderTax = eleLineTax.getAttribute(GCConstants.TAX);
      String sTaxPercentage = eleLineTax.getAttribute(GCConstants.TAX_PERCENTAGE);
      dTaxPercentage = Double.parseDouble(sTaxPercentage);

      double dSalesOrderTax = (!YFCCommon.isVoid(sSalesOrderTax)) ? Double
          .valueOf(sSalesOrderTax) : 0.00;
          double dSalesOrderQty = (!YFCCommon.isVoid(sSalesOrderQty)) ? Double
              .valueOf(sSalesOrderQty) : 0.00;
              LOGGER.debug(" dSalesOrderTax " + dSalesOrderTax
                  + " dSalesOrderQty " + dSalesOrderQty);
              if (dSalesOrderQty > 0.00) {
                dProratedTax = dSalesOrderTax * currentQty / dSalesOrderQty;
                
                LOGGER.debug(" dProratedTax - before rounding: " + dProratedTax);
                /*GCSTORE-6649 - Begin*/
                String sROLineTax = String.valueOf(dProratedTax);
        		BigDecimal a = new BigDecimal(sROLineTax);
        	    BigDecimal bROLineTax = a.setScale(2, BigDecimal.ROUND_DOWN);
        	    dProratedTax = bROLineTax.doubleValue();
        	    LOGGER.debug(" dProratedTax - after round down: " + dProratedTax);
        	     /*GCSTORE-6649 - End*/
              }
              if ("Shipping".equals(strChargeCategory)) {
                Element eleLineCharge =
                    GCXMLUtil.getElementByXPath(docSalesOrderLineList,
                        "OrderLineList/OrderLine/LineCharges/LineCharge[@ChargeCategory='" + strChargeCategory
                        + "' and @ChargeName='" + strChargeName + "']");
                if (!YFCCommon.isVoid(eleLineCharge)) {
                  String sSalesChargeAmt = eleLineCharge.getAttribute(GCConstants.CHARGE_AMOUNT);
                  BigDecimal bdSalesChargeAmt = new BigDecimal(sSalesChargeAmt);
                  BigDecimal bdCurrentChgAmt = new BigDecimal(String.valueOf(dChargeAmount));
                  BigDecimal bdSalesOrderTax = new BigDecimal(String.valueOf(dSalesOrderTax));
//                  dProratedTax =
//                      Double.parseDouble(bdSalesOrderTax.multiply(bdCurrentChgAmt).divide(bdSalesChargeAmt,RoundingMode.HALF_UP).setScale(2, 2)
//                          .toString());
                
                  /*GCSTORE-6649 - Begin*/
                  LOGGER.debug(" dProratedTax - before rounding" + dProratedTax);
                  BigDecimal bbROLineTax = bdSalesOrderTax.multiply(bdCurrentChgAmt).divide(bdSalesChargeAmt,RoundingMode.HALF_UP);
                  BigDecimal bROLineTaxUpdated = bbROLineTax.setScale(2, BigDecimal.ROUND_DOWN);
                  dProratedTax=bROLineTaxUpdated.doubleValue();
                  LOGGER.debug(" dProratedTax - after round down" + dProratedTax);
                  /*GCSTORE-6649 - End*/
                
                }
              }
              LOGGER.debug(" dProratedTax " + dProratedTax);
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateLineTaxUE.calculateReturnProratedTax() ",
          e);
      throw new GCException(e);
    }
    listLineTax.add(0, dProratedTax);
    listLineTax.add(1, dTaxPercentage);
    LOGGER.debug(" Exiting GCRecalculateLineTaxUE.calculateReturnProratedTax() method ");
    return listLineTax;
  }

  /**
   * This method is used to fetch parent's LineTaxes.
   *
   * @param env
   * @param docSalesOrderLineList
   * @param strChargeName
   * @param strChargeCategory
   * @return
   * @throws GCException
   */
  private Element fetchParentLineTax(YFSEnvironment env, Document docSalesOrderLineList, String strChargeCategory,
      String strChargeName) throws GCException {
    YFCDocument salesOrderDoc = YFCDocument.getDocumentFor(docSalesOrderLineList);
    LOGGER.verbose("salesOrderDoc : " + salesOrderDoc.toString());
    LOGGER.debug(" strChargeCategory " + strChargeCategory + " strChargeName " + strChargeName);
    String sParentLineKey =
        GCXMLUtil.getAttributeFromXPath(docSalesOrderLineList, "OrderLineList/OrderLine/@BundleParentOrderLineKey");

    LOGGER.verbose("sParentLineKey : " + sParentLineKey);
    Document docParentSalesOrderLineList = fetchOrderLineList(env, sParentLineKey);
    Element eleLineTax =
        GCXMLUtil.getElementByXPath(docParentSalesOrderLineList,
            "OrderLineList/OrderLine/LineTaxes/LineTax[@ChargeCategory='" + strChargeCategory + "' and @ChargeName='"
                + strChargeName + "']");

    return eleLineTax;
  }

  private void updateProratedColTax(List<YFSExtnTaxBreakup> proratedColTax,
      String strChargeCategory, String strChargeName,
      double dResponseTax, double dOriginaltax, double dTaxPercentage) throws GCException {
    LOGGER.debug(" Entering GCRecalculateLineTaxUE.updateProratedColTax() method ");
    try {
      if (bForInvoice && colTax != null) {
        LOGGER.debug(" Inside  if(bForInvoice && colTax != null) ");
        for (int i = 0; i < colTax.size(); i++) {
          YFSExtnTaxBreakup taxBreakup = colTax.get(i);
          if (StringUtils.equalsIgnoreCase(strChargeCategory,
              taxBreakup.chargeCategory)
              && StringUtils.equalsIgnoreCase(strChargeName,
                  taxBreakup.chargeName)) {
            LOGGER.debug(" Inside if ChargeCategory and ChargeName matches ");
            dOriginaltax = taxBreakup.tax;
            // OMS - 5478 Start
            // dOriginaltax = dOriginaltax * currentQty / lineQty;
            // OMS-5478 End
            double dInvoicedTax = taxBreakup.invoicedTax;
            if ((dResponseTax + dInvoicedTax) > dOriginaltax) {
              LOGGER.debug(" Inside if (dResponseTax+dInvoicedTax) > dOriginaltax ");
              String sReference2 = taxBreakup.reference2;
              double dReference2 = (!YFCCommon
                  .isVoid(sReference2)) ? Double
                      .valueOf(sReference2) : 0.00;
                      dReference2 = dReference2
                          + (dResponseTax + dInvoicedTax)
                          - dOriginaltax;
                      taxBreakup.reference2 = String.valueOf(dReference2);
                      taxBreakup.tax = dOriginaltax - dInvoicedTax;
                      taxBreakup.taxPercentage = dTaxPercentage;
                      proratedColTax.add(taxBreakup);
            } else {
              LOGGER.debug(" Inside else (dResponseTax+dInvoicedTax) > dOriginaltax ");
              YFSExtnTaxBreakup extnTaxBreakup = new YFSExtnTaxBreakup();
              extnTaxBreakup.chargeCategory = strChargeCategory;
              extnTaxBreakup.chargeName = strChargeName;
              extnTaxBreakup.taxName = ("LinePriceTax"
                  .equalsIgnoreCase(strChargeName)) ? strChargeName
                      : strChargeName + "Tax";
              extnTaxBreakup.taxPercentage = dTaxPercentage;
              extnTaxBreakup.tax = dResponseTax;
              proratedColTax.add(extnTaxBreakup);
            }
            break;
            // END IF - ChargeCat and ChargeName
          }
          // END LOOP - colTax
        }
      } else if (colTax != null) {
        LOGGER.debug(" Inside else if (colTax != null) ");
        YFSExtnTaxBreakup extnTaxBreakup = new YFSExtnTaxBreakup();
        extnTaxBreakup.chargeCategory = strChargeCategory;
        extnTaxBreakup.chargeName = strChargeName;
        extnTaxBreakup.taxName = ("LinePriceTax"
            .equalsIgnoreCase(strChargeName)) ? strChargeName
                : strChargeName + "Tax";
        extnTaxBreakup.taxPercentage = dTaxPercentage;
        extnTaxBreakup.tax = dResponseTax;
        proratedColTax.add(extnTaxBreakup);
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateLineTaxUE.updateProratedColTax() ",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCRecalculateLineTaxUE.updateProratedColTax() method ");
  }

  private void createEmptyOutStruct(YFSExtnTaxCalculationOutStruct outStruct)
      throws GCException {
    LOGGER.debug(" Entering GCRecalculateLineTaxUE.createEmptyOutStruct() method ");
    try {
      if (!colTax.isEmpty()) {
        for (int i = 0; i < colTax.size(); i++) {
          YFSExtnTaxBreakup extnTaxBreakup = colTax.get(i);
          Double categoryTaxAmt = extnTaxBreakup.tax;
          Double invoicedTax = extnTaxBreakup.invoicedTax;
          String chargeCategory = extnTaxBreakup.chargeCategory;
          String chargeName = extnTaxBreakup.chargeName;
          extnTaxBreakup.tax = calculateProratedTax(chargeCategory,
              chargeName, categoryTaxAmt, invoicedTax, false)
              .doubleValue();
        }
      }
      outStruct.colTax = colTax;
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateLineTaxUE.createEmptyOutStruct() ",
          e);
      throw new GCException(e);
    }
    LOGGER.debug(" Exiting GCRecalculateLineTaxUE.createEmptyOutStruct() method ");
  }

  private Double calculateProratedTax(String strChargeCategory,
      String strChargeName, Double categoryTaxAmt, Double invoicedTax,
      boolean bIsUnitCharge) throws GCException {
    LOGGER.debug(" Entering GCRecalculateLineTaxUE.calculateProratedTax() method ");
    LOGGER.debug(" strChargeCategory " + strChargeCategory
        + " strChargeName" + strChargeName + " categoryTaxAmt"
        + categoryTaxAmt + "invoicedTax" + invoicedTax);
    Double dChargeAmount = 0.0;
    Double proratedTax = 0.0;
    Double betaTax;
    boolean bIsPrice = false;
    LOGGER.debug(" Charge Category: " + strChargeCategory
        + " Charge Name: " + strChargeName + " IsUnit Charge: "
        + bIsUnitCharge);
    try {
      if (StringUtils.equalsIgnoreCase(strChargeCategory, "LinePrice")) {
        bIsUnitCharge = true;
        bIsPrice = true;
      } else {
        for (int iCounter = 0; iCounter < colCharge.size(); iCounter++) {

          YFSExtnLineChargeStruct inputStructlineCharge = colCharge
              .get(iCounter);

          String strTmpChargeCategory = inputStructlineCharge.chargeCategory;
          if (!YFCCommon.isVoid(strTmpChargeCategory)) {
            strTmpChargeCategory = strTmpChargeCategory.trim();
          } else {
            strTmpChargeCategory = GCConstants.BLANK;
          }

          String strTmpChargeName = inputStructlineCharge.chargeName;
          if (!YFCCommon.isVoid(strTmpChargeName)) {
            strTmpChargeName = strTmpChargeName.trim();
          } else {
            strTmpChargeName = GCConstants.BLANK;
          }

          if (!YFCCommon.isVoid(strChargeCategory)) {
            strChargeCategory = strChargeCategory.trim();
          } else {
            strChargeCategory = GCConstants.BLANK;
          }

          if (!YFCCommon.isVoid(strChargeName)) {
            strChargeName = strChargeName.trim();
          } else {
            strChargeName = GCConstants.BLANK;
          }

          if (StringUtils.equalsIgnoreCase(strChargeCategory,
              strTmpChargeCategory)
              && StringUtils.equalsIgnoreCase(strChargeName,
                  strTmpChargeName)) {

            if (inputStructlineCharge.chargePerUnit > 0.0) {
              bIsUnitCharge = true;
            }

            dChargeAmount = inputStructlineCharge.chargeAmount;
            break;
            // END IF - ChargeCategory & ChargeName
          }
          // END LOOP - Col Charge
        }
        // END IF - PRICE
      }

      LOGGER.debug("bIsUnitCharge: " + bIsUnitCharge
          + " & dChargeAmount: " + dChargeAmount);

      if (bIsUnitCharge) {
        betaTax = (categoryTaxAmt / lineQty) * currentQty;
        LOGGER.debug("Is Unit Charge Set BetaTax = " + betaTax);
      } else if (dChargeAmount == 0 && !bIsPrice) {
        betaTax = 0.0;
        LOGGER.debug("ChargeAmount is Zero. Set BetaTax = " + betaTax);
      } else if (lineQty.doubleValue() == 0.0) {
        betaTax = 0.0;
        LOGGER.debug("LineQty is Zero. Set BetaTax = " + betaTax);
      } else {
        betaTax = categoryTaxAmt;
        LOGGER.debug("Default Else. Set BetaTax = " + betaTax);
      }

      if (betaTax.isNaN()) {
        betaTax = 0.0;
      }
      // Set prorated tax value
      proratedTax = betaTax;
      LOGGER.debug("pack list: " + bForPacklistPrice + " & bForInvoice :"
          + bForInvoice);

      if (bForPacklistPrice) {
        proratedTax = (categoryTaxAmt * currentQty) / lineQty;
      }
      if (bForInvoice) {
        double dTaxToBeProrated = categoryTaxAmt - invoicedTax;
        double dRemainingQty = lineQty - invoicedPricingQty;
        proratedTax = (dTaxToBeProrated * currentQty) / dRemainingQty;
      }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateLineTaxUE.updateProratedColTax() ",
          e);
      throw new GCException(e);
    }
    LOGGER.debug("invoice====>" + bForInvoice);
    LOGGER.debug("lineQty====>" + lineQty);
    LOGGER.debug("currentQty====>" + currentQty);
    LOGGER.debug("invoicedQty====>" + invoicedPricingQty);
    LOGGER.debug("proratedTax====>" + proratedTax);
    LOGGER.debug("invoicedTax====>" + invoicedTax);
    LOGGER.debug(" Exiting GCRecalculateLineTaxUE.calculateProratedTax() method ");
    proratedTax = GCCommonUtil.splitNumberForDecimalPrecision(proratedTax,
        2)[0];
    return proratedTax;
  }

  /**
   *
   * Description of appendPriceToVertexIP Append Price to the Vertex IP XML
   *
   * @param docVertexCallIP
   * @param eQuotationRequest
   * @param sOrderDate
   * @param sTaxExemptFlag
   * @param bIsPriceCharge
   * @param eleOrderLine
   * @throws Exception
   *
   */
  private void appendPriceToVertexIP(Document docVertexCallIP,
      Element eQuotationRequest, String sOrderDate,
      String sTaxExemptFlag, Element eleOrderLine) throws GCException {
    LOGGER.debug(" Entering GCRecalculateLineTaxUE.appendPriceToVertexIP() method ");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" appendPriceToVertexIP() method, docVertexCallIP is"
          + GCXMLUtil.getXMLString(docVertexCallIP));
    }
    try {
      Element eleLineItem = GCXMLUtil.createElement(docVertexCallIP,
          "LineItem", null);
      String sOrderLineKey = eleOrderLine
          .getAttribute(GCConstants.ORDER_LINE_KEY);
      String sOrderedQty = eleOrderLine
          .getAttribute(GCConstants.ORDERED_QTY);
      String sItemID = GCXMLUtil.getAttributeFromXPath(eleOrderLine,
          "Item/@ItemID");
      String sTaxProductCode = GCXMLUtil.getAttributeFromXPath(
          eleOrderLine,
          "ItemDetails/ClassificationCodes/@TaxProductCode");
      double dOrderedQty = (!YFCCommon.isVoid(sOrderedQty)) ? Double
          .valueOf(sOrderedQty) : 0.00;
          String sLineItemId = "L-" + sOrderLineKey;
          addLineItemAttributes(eleLineItem, sLineItemId, sOrderDate);

          String strChargeCategory = "LinePrice";

          String sPrice = GCXMLUtil.getAttributeFromXPath(eleOrderLine,
              "LinePriceInfo/@UnitPrice");

          double dPrice = (!YFCCommon.isVoid(sPrice)) ? Double
              .valueOf(sPrice) : 0.00;
              double dChargeAmount = dPrice * dOrderedQty;

              LOGGER.debug(" Inside appendPriceToVertexIP(), sPrice" + sPrice
                  + " dChargeAmount" + dChargeAmount + " dPrice " + dPrice
                  + " sOrderedQty " + sOrderedQty);

              NodeList nlPromotion = XPathAPI.selectNodeList(eleOrderLine,
                  "LineCharges/LineCharge[@ChargeCategory='Promotion']");
              for (int iCounter = 0; iCounter < nlPromotion.getLength(); iCounter++) {
                LOGGER.debug(" Looping on Promotion Charges ");
                Element elePromotion = (Element) nlPromotion.item(iCounter);
                String sPromotion = elePromotion
                    .getAttribute(GCConstants.CHARGE_PER_LINE);
                double dPromotion = (!YFCCommon.isVoid(sPromotion)) ? Double
                    .valueOf(sPromotion) : 0.00;
                    dChargeAmount = dChargeAmount - dPromotion;
                    dPrice = dChargeAmount / currentQty;
                    LOGGER.debug(" Inside loop, dPrice" + dPrice + " dChargeAmount"
                        + dChargeAmount);
              }

              LOGGER.debug(" sOrderLineKey" + sOrderLineKey + " dChargeAmount"
                  + dChargeAmount);
              eQuotationRequest.appendChild(eleLineItem);
              GCRecalculateLineTaxUE.addProductInfo(docVertexCallIP, eleLineItem,
                  strChargeCategory, sItemID, sTaxProductCode);
              Element eleQuantity = GCXMLUtil.createElement(docVertexCallIP,
                  "Quantity", null);
              eleQuantity.setTextContent(sOrderedQty);
              eleLineItem.appendChild(eleQuantity);

              Element eleFreight = GCXMLUtil.createElement(docVertexCallIP,
                  "Freight", null);
              eleFreight.setTextContent("0");
              eleLineItem.appendChild(eleFreight);

              Element eleUnitPrice = GCXMLUtil.createElement(docVertexCallIP,
                  "UnitPrice", null);
              eleUnitPrice.setTextContent(String.valueOf(dPrice));
              eleLineItem.appendChild(eleUnitPrice);

              Element eleExtendedPrice = GCXMLUtil.createElement(docVertexCallIP,
                  "ExtendedPrice", null);
              eleExtendedPrice.setTextContent(String.valueOf(dChargeAmount));
              eleLineItem.appendChild(eleExtendedPrice);

              Element eleCustomer = GCXMLUtil.createElement(docVertexCallIP,
                  "Customer", null);
              eleCustomer.setAttribute("isTaxExempt", sTaxExemptFlag);
              eleLineItem.appendChild(eleCustomer);

              Element eleCustomerCode = GCXMLUtil.createElement(docVertexCallIP,
                  "CustomerCode", null);
              eleCustomerCode.setAttribute("classCode", "MAIN");
              eleCustomer.appendChild(eleCustomerCode);

              Element elePersonInfoShipTo = (Element) XPathAPI.selectSingleNode(
                  eleOrderLine, "PersonInfoShipTo");
              if (!YFCCommon.isVoid(elePersonInfoShipTo)) {
                Element eleDestination = GCXMLUtil.createElement(
                    docVertexCallIP, "Destination", null);
                eleCustomer.appendChild(eleDestination);
                appendPersonInfoShipTo(docVertexCallIP, elePersonInfoShipTo,
                    eleDestination);
              }
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateLineTaxUE.appendPriceToVertexIP() ",
          e);
      throw new GCException(e);
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" GCRecalculateLineTaxUE.appendPriceToVertexIP() method, updated docVertexCallIP is"
          + GCXMLUtil.getXMLString(docVertexCallIP));
    }
    LOGGER.debug(" Exiting GCRecalculateLineTaxUE.appendPriceToVertexIP() method ");
  }

  private String fetchSalesOrderLineKey(YFSEnvironment env)
      throws GCException {
    LOGGER.debug(" Entering GCRecalculateLineTaxUE.fetchSalesOrderLineKey() method ");
    String sSalesOrderKey = "";
    try {
      Document docGetOrderList = GCXMLUtil
          .createDocument(GCConstants.ORDER_LINE);
      docGetOrderList.getDocumentElement().setAttribute(
          GCConstants.ORDER_LINE_KEY, orderLineKey);
      Document docGetOrderListTmp = GCXMLUtil
          .getDocument("<OrderLineList><OrderLine DerivedFromOrderLineKey=''/></OrderLineList>");
      Document docGetOrderListOP = GCCommonUtil.invokeAPI(env,
          GCConstants.GET_ORDER_LINE_LIST, docGetOrderList,
          docGetOrderListTmp);
      sSalesOrderKey = GCXMLUtil.getAttributeFromXPath(docGetOrderListOP,
          "OrderLineList/OrderLine/@DerivedFromOrderLineKey");
    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateLineTaxUE.fetchSalesOrderLineKey() ",
          e);
      throw new GCException(e);
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" GCRecalculateLineTaxUE.fetchSalesOrderLineKey() method, sSalesOrderKey"
          + sSalesOrderKey);
    }
    LOGGER.debug(" Exiting GCRecalculateLineTaxUE.fetchSalesOrderLineKey() method ");
    return sSalesOrderKey;
  }

  private Document fetchOrderLineList(YFSEnvironment env,
      String sSalesOrderLineKey) throws GCException {
    LOGGER.debug(" Entering GCRecalculateLineTaxUE.fetchOrderLineList() method ");
    Document docOrderLineListOP = null;
    try {
      Document docGetOrderLineListIP = GCXMLUtil
          .createDocument(GCConstants.ORDER_LINE);
      docGetOrderLineListIP.getDocumentElement().setAttribute(
          GCConstants.ORDER_LINE_KEY, sSalesOrderLineKey);

      Document docGetOrderLineListTmp = GCXMLUtil
          .getDocument(GCConstants.GET_ORDER_LINE_LIST_TEMPLATE);
      docOrderLineListOP =
          GCCommonUtil.invokeAPI(env, GCConstants.API_GET_ORDER_LINE_LIST, docGetOrderLineListIP,
              docGetOrderLineListTmp);

    } catch (Exception e) {
      LOGGER.error(
          " Inside catch of GCRecalculateLineTaxUE.fetchOrderLineList() ",
          e);
      throw new GCException(e);
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(" GCRecalculateLineTaxUE.fetchOrderLineList() method, docOrderLineListOP is"
          + GCXMLUtil.getXMLString(docOrderLineListOP));
    }
    LOGGER.debug(" Exiting GCRecalculateLineTaxUE.fetchOrderLineList() method ");
    return docOrderLineListOP;
  }

  private void updateColTax(YFSExtnTaxCalculationOutStruct outStruct) {
    List<YFSExtnTaxBreakup> proratedColTax = new ArrayList<YFSExtnTaxBreakup>();
    boolean bIsPriceCharge = false;
    if (!bIsPriceCharge) {
      LOGGER.debug(" Inside if(!bIsPriceCharge) ");
      String strChargeCategory = "LinePrice";
      String strChargeName = "LinePriceTax";
      double dResponseTax = 0.00;
      YFSExtnTaxBreakup extnTaxBreakup = new YFSExtnTaxBreakup();
      extnTaxBreakup.chargeCategory = strChargeCategory;
      extnTaxBreakup.chargeName = strChargeName;
      extnTaxBreakup.taxName = strChargeName;
      extnTaxBreakup.reference3 = "ERROR";
      extnTaxBreakup.taxPercentage = 0.00;
      extnTaxBreakup.tax = dResponseTax;
      proratedColTax.add(extnTaxBreakup);
    }
    for (int iCounter = 0; iCounter < colCharge.size(); iCounter++) {
      LOGGER.debug(" Looping on colCharge ");
      YFSExtnLineChargeStruct inputStructlineCharge = colCharge
          .get(iCounter);
      String strChargeCategory = inputStructlineCharge.chargeCategory;
      String strChargeName = "";
      if (!YFCCommon.isVoid(strChargeCategory)) {
        strChargeCategory = strChargeCategory.trim();
      }
      strChargeName = inputStructlineCharge.chargeName;
      if (!YFCCommon.isVoid(strChargeName)) {
        strChargeName = strChargeName.trim();
      }
      if (!(GCConstants.SHIPPING_PROMOTION
          .equalsIgnoreCase(strChargeCategory) || GCConstants.PROMOTION
          .equalsIgnoreCase(strChargeCategory))) {
        double dResponseTax = 0.00;
        YFSExtnTaxBreakup extnTaxBreakup = new YFSExtnTaxBreakup();
        extnTaxBreakup.chargeCategory = strChargeCategory;
        extnTaxBreakup.chargeName = strChargeName;
        extnTaxBreakup.taxName = strChargeName + "Tax";
        extnTaxBreakup.taxPercentage = 0.00;
        extnTaxBreakup.reference3 = "ERROR";
        extnTaxBreakup.tax = dResponseTax;
        proratedColTax.add(extnTaxBreakup);
      }
      LOGGER.debug(" proratedColTax" + proratedColTax);
    }
    outStruct.colTax = proratedColTax;
  }
}
