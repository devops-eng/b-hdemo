/**
 * Copyright 2014 Guitar Center. All rights reserved.  Guitar Center PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  ######################################################################################################################################
 *   OBJECTIVE: This class defines the Stored Value Card payment group processing.
 *  ######################################################################################################################################
 *        Version    Date          Modified By           CR                Description
 *  ######################################################################################################################################
 *        1.0       02/04/2014     Soni, Karan                             Initial Version Stored Value Card UE.
 *        1.1       10/08/2014     Chopra, Inderpreet    OMS-3826          Handle emails processing during offline of payment gateway
 *        1.2       04/12/2017     Gosai, Pinky        GCSTORE-7151        GCPP vouchers are duplicating credits in DAX AR 
 *        1.3       05/17/2017     Gosai, Pinky        GCSTORE-6851        Prod - Declined WEB orders not sending Decline Letters 
 *  ######################################################################################################################################
 */
package com.gc.userexit;

import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCCommonUtil;
import com.gc.common.utils.GCDBUtils;
import com.gc.common.utils.GCException;
import com.gc.common.utils.GCPaymentUtils;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSExtnPaymentCollectionInputStruct;
import com.yantra.yfs.japi.YFSExtnPaymentCollectionOutputStruct;
import com.yantra.yfs.japi.YFSUserExitException;

public class GCStoredValueCardUE implements
com.yantra.yfs.japi.ue.YFSCollectionStoredValueCardUE {

  // initialize logger
  private static final YFCLogCategory LOGGER = YFCLogCategory
      .instance(GCStoredValueCardUE.class);

  @Override
  public YFSExtnPaymentCollectionOutputStruct collectionStoredValueCard(
      YFSEnvironment env, YFSExtnPaymentCollectionInputStruct inStruct)
          throws YFSUserExitException {
    YFSExtnPaymentCollectionOutputStruct outStruct = new YFSExtnPaymentCollectionOutputStruct();
    try {
      LOGGER.verbose("Entering the collectionCreditCard method");
      String strChargeType = inStruct.chargeType;
      if (GCConstants.PAYMENT_STATUS_CHARGE.equals(strChargeType)) {
        LOGGER.debug("Entering If in case of Charge Type as CHARGE");
        Document getOrderListOpDoc = GCCreditCardUE.getOrderListOp(env,
            inStruct);
        // Manual Authorization Logic
        String sPaymentReference4 = inStruct.paymentReference4;
        if (!YFCCommon.isVoid(sPaymentReference4)
            && "GIFT_CARD".equals(inStruct.paymentType)) {
          outStruct = processManualAuth(env, outStruct, inStruct);
          
          // Update for GCSTORE#6526 --- Starts
          callGetOrderDetailsAndPost(env, outStruct.tranReturnCode, inStruct.orderHeaderKey,
              inStruct.chargeTransactionKey);
          // Update for GCSTORE#6526 --- Ends
           
           
        } else {
          Document paymentResponseDoc = GCPaymentUtils
              .processAJBAuthInput(env, inStruct);

          if (YFCCommon.isVoid(paymentResponseDoc)) {
            LOGGER.debug("Entering the paymnt response doc null if");
            String strProgId = env.getProgId();
            if (GCConstants.PAYMENT_CALL_CALL_CENTER
                .equalsIgnoreCase(strProgId)) {
              outStruct.PaymentTransactionError = setPayTrnxError(
                  "PAYMENT_FAILURE",
                  GCConstants.PAYMENT_COMMUNICATION_FAILURE_MSG);
            }
            //OMS-3826 START
            outStruct.OfflineStatus=true;
            //OMS-3826 END
            GCCommonUtil.invokeService(env,
                GCConstants.PAYMENT_SERVICE_UNAVAILABLE,
                getOrderListOpDoc);
            LOGGER.debug("Exiting the payment response doc null if");
          } else {
            LOGGER.debug("Entering the Payment response received IF");
            if (LOGGER.isDebugEnabled()) {
              LOGGER.debug("The response doc -->"
                  + GCXMLUtil.getXMLString(paymentResponseDoc));
            }
            //OMS-3826 START
            outStruct.OfflineStatus=false;
            //OMS-3826 END
            outStruct = GCCreditCardUE.processCCAuthOutput(env,
                paymentResponseDoc, outStruct, inStruct,
                getOrderListOpDoc);

            // GCSTORE-6851 start
            String strProgId = env.getProgId();
            LOGGER.debug("strProgId " + strProgId);
            // GCSTORE-6851 End
            
            /**
             * GCSTORE-7151 : Starts
             * GiftCard redemption message must be sent to DAX, only when AJB charge is successful
             * Appended to the if condition GCConstants.PAYMENT_AUTHORIZED.equalsIgnoreCase(strAuthReturnCode)
             */
            String strAuthReturnCode = outStruct.authReturnCode;
            LOGGER.debug("strAuthReturnCode " + strAuthReturnCode);
            // Update for GCSTORE#6526 --- Starts
            if ("GIFT_CARD".equals(inStruct.paymentType) &&
            	GCConstants.PAYMENT_AUTHORIZED.equalsIgnoreCase(strAuthReturnCode)) 
            {
            	LOGGER.debug("Gift Card successfully charged");
            	/**
            	 * GCSTORE-7151 : End
            	 */
            	
              String sequenceNo = GCDBUtils.generateUniqueSequence(env, "select GC_SEQ_GIFT_CARD.nextval from dual");
              outStruct.tranReturnCode = "GCPP" + sequenceNo;
              LOGGER.debug("outStruct.tranReturnCode : " + outStruct.tranReturnCode);
              callGetOrderDetailsAndPost(env, outStruct.tranReturnCode, inStruct.orderHeaderKey,
                  inStruct.chargeTransactionKey);
              // Update for GCSTORE#6526 --- Ends

            }
            /**
             *  GCSTORE-6851 : Start
             *  OMS will send message for Payment Decline
             */
            else if("GIFT_CARD".equals(inStruct.paymentType) &&
                	GCConstants.PAYMENT_DECLINED.equalsIgnoreCase(strAuthReturnCode) &&
                	!GCConstants.PAYMENT_CALL_CALL_CENTER.equalsIgnoreCase(strProgId)) 
            {
            	LOGGER.debug("Gift Card declined charge");
            	Element eleOrder=getOrderListOpDoc.getDocumentElement();
            	GCPaymentUtils.prepareAndSendCardDeclineEmail(env, eleOrder, outStruct.authorizationId, outStruct.suspendPayment);
            }
            /**
             * GCSTORE-6851 : End
             */
            
            LOGGER.debug("Exiting the Payment response received IF");
          }
          LOGGER.debug("Exiting If in case of Charge Type as CHARGE");
        }
        LOGGER.verbose("Exiting the collectionCreditCard method");
      }
    } catch (Exception e) {
      LOGGER.error(
          "Inside GCStoredValueCardUE.collectionStoredValueCard():",
          e);
      YFSUserExitException ex = new YFSUserExitException(e.getMessage());
      ex.setErrorDescription(e.getMessage());
      throw ex;
    }
    return outStruct;
  }

  /**
   * This method is used to call getOrderDetails api and post message to DAX in case of GIFT_CARD.
   *
   * @param env
   * @param sequenceNo
   * @param orderHeaderKey
   * @param chargeTransactionKey
   */
  private void callGetOrderDetailsAndPost(YFSEnvironment env, String sequenceNo, String orderHeaderKey,
      String chargeTransactionKey) {

    YFCDocument getOdrListIndoc = YFCDocument.createDocument("Order");
    YFCElement eleOrder = getOdrListIndoc.getDocumentElement();
    eleOrder.setAttribute(GCConstants.ORDER_HEADER_KEY, orderHeaderKey);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("InDoc for getOrderDetails: " + getOdrListIndoc);
    }
    Document getOrderDetailsTemp =
        GCCommonUtil.getDocumentFromPath("/global/template/api/getOrderDetails_For_GiftCard.xml");
    YFCDocument tempdoc = YFCDocument.getDocumentFor(getOrderDetailsTemp);
    YFCDocument getOrderListOutDoc =
        GCCommonUtil.invokeAPI(env, GCConstants.GET_ORDER_DETAILS, getOdrListIndoc, tempdoc);
    if (LOGGER.isVerboseEnabled()) {
      LOGGER.verbose("OutDoc for getOrderDetails: " + getOrderListOutDoc);
    }
    // Retaining only one chargeTransactionDetail having GiftCard as PaymentType and removing extra
    // ChargeTransactionDetail elements.

    Element eleRoot = getOrderListOutDoc.getDocument().getDocumentElement();
    Element eleChrgTrnsctnDtls = (Element) eleRoot.getElementsByTagName("ChargeTransactionDetails").item(0);
    List<Element> yfcNLTransactionDetail =
        GCXMLUtil.getElementListByXpath(getOrderListOutDoc.getDocument(),
            "Order/ChargeTransactionDetails/ChargeTransactionDetail");
    Iterator<Element> itr = yfcNLTransactionDetail.iterator();
    while (itr.hasNext()) {
      LOGGER.debug("inside while");
      Element eleChgTransactionDtl = itr.next();
      if (YFCUtils.equals(eleChgTransactionDtl.getAttribute(GCConstants.CHARGE_TRANSACTION_KEY), chargeTransactionKey)) {
        LOGGER.debug("inside if");
        Element eleCreditCardTrnsctns =
            (Element) eleChgTransactionDtl.getElementsByTagName("CreditCardTransactions").item(0);
        Element eleCreditCardTrnsctn =
            (Element) eleCreditCardTrnsctns.getElementsByTagName("CreditCardTransaction").item(0);
        if (YFCCommon.isVoid(eleCreditCardTrnsctn)) {
          eleCreditCardTrnsctn = getOrderListOutDoc.getDocument().createElement("CreditCardTransaction");
          eleCreditCardTrnsctns.appendChild(eleCreditCardTrnsctn);
        }
        eleCreditCardTrnsctn.setAttribute("TranReturnCode", sequenceNo);

      } else {
        LOGGER.debug("inside else");
        eleChrgTrnsctnDtls.removeChild(eleChgTransactionDtl);
      }
    }
    // Posting the outputDoc to DAX
    GCCommonUtil.invokeService(env, "GCPostGiftCardMsgToDAX", getOrderListOutDoc);

  }

  /**
   *
   * @param env
   * @param outStruct
   * @param inStruct
   * @return
   * @throws SQLException
   * @throws Exception
   */
  private YFSExtnPaymentCollectionOutputStruct processManualAuth(
      YFSEnvironment env, YFSExtnPaymentCollectionOutputStruct outStruct,
      YFSExtnPaymentCollectionInputStruct inStruct) throws GCException,
      SQLException {
    Date date = new Date();
    outStruct.authorizationAmount = inStruct.requestAmount;
    outStruct.tranAmount = inStruct.requestAmount;
    outStruct.executionDate = date;
    outStruct.collectionDate = date;
    outStruct.authReturnMessage = "MANUAL_CHARGE_SUCCESS";
    outStruct.authorizationId = inStruct.paymentReference7;
    outStruct.authorizationExpirationDate = GCPaymentUtils
        .getAuthExpiryDate(env, inStruct);
    
    // Update for GCSTORE#6526 --- Starts
    outStruct.tranReturnCode =
        "GCPP" + GCDBUtils.generateUniqueSequence(env, "select GC_SEQ_GIFT_CARD.nextval from dual");
    LOGGER.debug("outStruct.tranReturnCode : " + outStruct.tranReturnCode);
    // Update for GCSTORE#6526 --- Ends
     
    return outStruct;
  }
  /**
   *
   * Description of setPayTrnxError
   *
   * @param strMsgType
   * @param strMsg
   * @return
   * @throws GCException
   *
   */
  private static Document setPayTrnxError(final String strMsgType,
      final String strMsg) throws GCException {
    Document docPayTrnErrorLst = null;
    try {
      docPayTrnErrorLst = GCXMLUtil
          .createDocument("PaymentTransactionErrorList");
      final Element elePayTrnxErrLst = docPayTrnErrorLst
          .getDocumentElement();
      final Element elePayTrnxErr = GCXMLUtil.createElement(
          docPayTrnErrorLst, "PaymentTransactionError", "");
      elePayTrnxErrLst.appendChild(elePayTrnxErr);
      elePayTrnxErr.setAttribute("MessageType", strMsgType);
      elePayTrnxErr.setAttribute("Message", strMsg);
    } catch (Exception ex) {
      LOGGER.error("Inside GCStoredValueCardUE.setPayTrnxError():", ex);
      throw new GCException(ex);
    }
    return docPayTrnErrorLst;
  }

}