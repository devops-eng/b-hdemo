/**
 * Copyright � 2014, Guitar Center,  All rights reserved.

 *  *#################################################################################################################################################################
 *          OBJECTIVE: This class will make bPreviousInvocationSuccessful as false;
 *#################################################################################################################################################################
 *          Version            Date              Modified By                Description
 *#################################################################################################################################################################
             1.0              10/28/2015         Yashu                POS Invoicing
 *#################################################################################################################################################################
 */
package com.gc.userexit;

import com.gc.api.invoicing.GCPOSInvoicing;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSExtnPaymentCollectionInputStruct;
import com.yantra.yfs.japi.YFSExtnPaymentCollectionOutputStruct;
import com.yantra.yfs.japi.YFSUserExitException;

public class GCValidateInvokedCollectionUE implements com.yantra.yfs.japi.ue.YFSValidateInvokedCollectionUE {
	
	private static final YFCLogCategory LOGGER = YFCLogCategory.instance(GCPOSInvoicing.class.getName());
	
	 /**
	   * This method will make bPreviousInvocationSuccessful as false, This would atleast make OMS clear the INVOKED status enabling Payments 
	   * Agent to process these the next time.
	   *
	   */
	public YFSExtnPaymentCollectionOutputStruct validateInvokedCollection(YFSEnvironment env, YFSExtnPaymentCollectionInputStruct oInStruct) 
			throws YFSUserExitException {
	 	YFSExtnPaymentCollectionOutputStruct oOutStruct = new YFSExtnPaymentCollectionOutputStruct();
	 	LOGGER.debug("Returning last Authorization successful as true for Charge Transaction Key: " + oInStruct.chargeTransactionKey);
		oOutStruct.bPreviousInvocationSuccessful = false;
		return oOutStruct;
	}
}