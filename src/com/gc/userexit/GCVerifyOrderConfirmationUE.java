package com.gc.userexit;

import com.yantra.yfc.log.YFCLogCategory;
import org.w3c.dom.Document;

import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;
import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.yfs.japi.ue.YFSVerifyOrderConfirmationUE;

public class GCVerifyOrderConfirmationUE implements
	YFSVerifyOrderConfirmationUE {
    private static final YFCLogCategory LOGGER= YFCLogCategory
			.instance(GCVerifyOrderConfirmationUE.class);
    @Override
    public void verifyOrderConfirmation(YFSEnvironment env, Document inDoc)
	    throws YFSUserExitException {

	LOGGER.verbose("Inside GCVerifyOrderConfirmationUE.verifyOrderConfirmation:  GCVerifyOrderConfirmationUE");
	YFSException ex = new YFSException();
	ex.setErrorCode("PAYMENT_AUTH_FAILED");
	ex.setErrorDescription("Payment auth failed");
	throw ex;

    }

}
