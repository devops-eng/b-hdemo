/**
 * Copyright � 2014, Guitar Center,  All rights reserved.
 *#################################################################################################################################################################
 *           OBJECTIVE: This file contains the java class used to implement the YFSCheckOrderBeforeProcessingUE
 *#################################################################################################################################################################
 *           Version            Date              Modified By                Description
 *#################################################################################################################################################################
 *             1.0               28-May-2015       Infosys Limited            Initial Version
 *#################################################################################################################################################################
 */
package com.gc.userexit;

// imports
import org.w3c.dom.Document;

import com.gc.common.constants.GCConstants;
import com.gc.common.utils.GCXMLUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.ue.YFSCheckOrderBeforeProcessingUE;

/**
 * <h3>Description:</h3> This class contains the implementation of the userexit
 * YFSCheckOrderBeforeProcessingUE. This is used to store the data required for the
 * INVGetDemandCorrectionsUE implementation.
 *
 * @author Infosys Limited
 *
 */
public class GCYFSCheckOrderBeforeProcessingUE implements YFSCheckOrderBeforeProcessingUE {

  // the class name
  private static final String CLASS_NAME = GCYFSCheckOrderBeforeProcessingUE.class.getName();

  // the logger reference
  private static final YFCLogCategory LOGGER = YFCLogCategory.instance(CLASS_NAME);


  /* (non-Javadoc)
   * @see com.yantra.yfs.japi.ue.YFSCheckOrderBeforeProcessingUE#checkOrderBeforeProcessing(com.yantra.yfs.japi.YFSEnvironment, org.w3c.dom.Document)
   */
  @Override
  public boolean checkOrderBeforeProcessing(final YFSEnvironment yfsEnv, final Document docInput) {

    // log the method entry
    LOGGER.info(CLASS_NAME + ":checkOrderBeforeProcessing:entry");

    //log the input
    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug(CLASS_NAME + ":checkOrderBeforeProcessing:Input=" + GCXMLUtil.getXMLString(docInput));
    }

    //Defect-3810
    YFCDocument inputDoc = YFCDocument.getDocumentFor(docInput);
    YFCElement eleRoot = inputDoc.getDocumentElement();
    String sPaymentStatus = eleRoot.getAttribute(GCConstants.PAYMENT_STATUS);
    if(YFCCommon.equals(sPaymentStatus, "AWAIT_AUTH") || YFCCommon.equals(sPaymentStatus, "AWAIT_PAY_INFO")){
    	return false;
    }
   
    //Ends Here Defect-3810
    // set the input xml in the environment
    yfsEnv.setTxnObject(GCConstants.ORDER_DTLS_ENV, docInput);

    // log the method exit
    LOGGER.info(CLASS_NAME + ":checkOrderBeforeProcessing:exit");

    // return true to indicate that the order is eligible for processing further
    return true;
  }
}