package com.gc.validations;

import com.sterlingcommerce.security.dv.SCByPassValidationRule;
import com.sterlingcommerce.security.dv.SCValidationException;

/**
 * This is the validation rule for the open form fields which are to be canonicalized 
 * and sent back to check for any intrusions.
 * We might need to change this a little bit to stop sql injection etc.
 */
public class GCByPassNoEncodingValidationRule extends SCByPassValidationRule {
	
	@Override
	public String getValidInput(String context, String input)
			throws SCValidationException {

		return input;
	}
}
