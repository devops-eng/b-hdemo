--- Installation Instructions -----
1) Keep entire file sets as it is target machine
2) modify runUserSync.sh for environment related properties
3) modify lib/config.cfg for input and output file properties
4) update locale_code_mapping.cfg to add / modify / delete locale selecation based on Enterprise Code and Language
5) update usr_grp.cfg and usrgrp_menu_mapping.cfg to select Menu based on user groups
6) update template_usr_mapping.cfg to modify template users based on EnterpriseCode and Menu
7) modify ftpjobs/fetchADFile and ftpjobs/fetchADFile.properties for incoming file ftp settingd
8) modify ftpjobs/sendADFile and ftpjobs/sendADFile.properties for outgoing file ftp settings.

--- Execution Instruction ----
1) run ./runUserSync.sh to execute the programme
	This will
	- fetch the incoming file
	- process the file and generate user.dat file in output dir
	- ftp the user.dat file to broker dir.

2) After every execution lib/control.dat file gets updated with sysdate. 
	- if current date = date in control.dat, program will quit
	- in order to re-run the program reset (differential and compelete reset) the date in control.dat
	- if the date is changed to y'day's or previous days, program will run in differential mode
	- in differential mode, it will compare (only) y'days records with today's records and processes
	  only new records. If no new records then user.dat will be blank
	- if date in control.dat file is blank, then it is complete reset and program will process all 
	  the records from incoming file
	  
3) Logs are available under log dir.

4) Working dir is used to store most recent file process (used in differential mode)
	