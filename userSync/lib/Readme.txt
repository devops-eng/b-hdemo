#############################################################################
# Company : IBM Canada ltd
# for HBC OMS project Active Directory synchronization with OMS.
# Please make a back up of the config.cfg file before using , for reference.
#############################################################################
1. Data in the XXMCF_LOCATION_TYPE table is saved to XXMCF_LOCATION_TYPE.txt file, it is preffered that the updates are done to this file/ or file get generated from any automated process. 
2. The LASTUPDATE.dat file will hold data for the last update occured.
3. The OUTBOUND.dat file is the processed out from the daily run. (If these file names need to be changed please do so using the config.cfg), also the  "delimiter" parameter in the config.cfg file will determine the field delimiter for the OUTBOUND.dat file.
4. TEMP_REC.dat is a temporary file to hold data till the data is written to LASTUPDATE.dat file.
5. APP_MCC_OMS_ and APP_MCF_PRD_G_  are values that determine the start character sequence for Template and Group values.
6. REJECTS.log will contain the rejected records for the current run.
7. If the process needed to be run more than once, control.dat need to be updated by deleting the value of the 'LAST_UPDATE=' property.
8. The program contain three file usersync.jar, config.cfg , control.dat and these files need to be in the same location (same root), and Readme.txt. In addition XXMCF_LOCATION_TYPE.txt is a a mandatory file althogh the file location is configurable.

9.To execute java -jar usersync.jar , make sure java is installed and file locations at config.cfg is configured.
10. It is recommended that the archive folder is a seperate folder. The zip archive file will be created in the format arch2012-09-06.zip, the date format (YYYY-MM-DD) should NOT be changed, and the archive filename should not have '.' in the file name except for the extension.
11. The default value is files older than 30 days , for deleting the old archive files, this value could be overwritten by using archive_delete_seq
 