export JAVA_HOME=/usr/OMS/fit1/9.1/Foundation/jdk
export USER_SYNC_HOME=/usr/OMS/fit1/userSync
export PWD=$USER_SYNC_HOME
export FTP_JOB_HOME=$USER_SYNC_HOME/ftpjobs
export USER_SYNC_LIB=$USER_SYNC_HOME/lib
export REMOTE_SOURCE_SERVER="ccftp01.hbc.com"
export REMOTE_SOURCE_DIR="isinfosecshared\\\\oms"
#****** For FIT ******
export REMOTE_DESTINATION_SERVER="CCDEV70A.hbc.com"
#export REMOTE_DESTINATION_DIR="/mcc/test/user/ldap_broker/input"
# posting file to UAT
export REMOTE_DESTINATION_DIR="/mcc/uat/user/ldap_broker/input"
#*********************
#******* For Prod ******
#export REMOTE_DESTINATION_SERVER="CCIB02A.HBC.COM"
#export REMOTE_DESTINATION_DIR="/mcc/user/ldap_broker/input"
#***********************
export LOCAL_INPUT_DIR="$USER_SYNC_HOME/input/"
export LOCAL_OUTPUT_DIR="$USER_SYNC_HOME/output/"
currentTime=`date +"%Y-%m-%d %H:%M:%S"`
echo ""
echo "****************************************************************************************************"
echo ""
echo "Log for $currentTime"
echo ""
echo "User Sync Home Dir is: $USER_SYNC_HOME "
cd $USER_SYNC_HOME
echo "----------------------------- Getting the file from LDAP -------------------------------------"
$FTP_JOB_HOME/fetchADFile
echo "----------------------------- Generating file for Broker -------------------------------------"
cd $USER_SYNC_LIB
#touch control.dat
#echo "LAST_UPDATE=" > control.dat
#java -jar usersync.jar
# updated to keep consistent with production - running with extracted jar files
$JAVA_HOME/bin/java -cp usersync.jar:lucene-core-3.6.1.jar:lucene-test-framework-3.6.1.jar com.ibm.hbc.oms.datatransfer.Indexer
cd -
echo "----------------------------- Sending the file to Broker -------------------------------------"
$FTP_JOB_HOME/sendADFile
echo "----------------------------- Sent the file to Broker -------------------------------------"
echo ""
echo "****************************************************************************************************"
echo ""

