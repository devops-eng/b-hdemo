package com.ibm.hbc.oms.datatransfer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;

public class Indexer {
	private int hitsPerPage = 50;
	private final Properties configFile = new java.util.Properties(); 
	private final Properties controlFile = new java.util.Properties(); 
	private final Properties usrgrp = new java.util.Properties();
	private final Properties usrgrp_menu_mapping = new java.util.Properties();
	private final Properties template_usr_mapping = new java.util.Properties();
	private final Properties locale_code_mapping = new java.util.Properties();
	private final Properties enterprise_code_mapping = new java.util.Properties();
	private File rejectsfile;
	private File current_accepted_recs;
	private File cntrlfile;
	private BufferedWriter rejectsout;
	private BufferedWriter accepted_out;
	private Calendar cal;
	private DateFormat dateFormat;
	private BufferedWriter errorlg;
	private BufferedWriter infolog;
	private String delimiter;
	 
	private StandardAnalyzer locaanalyzer; 
	private Directory locaindex;
	
	private IndexReader locareader;
	private IndexSearcher locasearcher;
	
	public Indexer() {
		try {
			configFile.load(new FileInputStream("config.cfg"));
			usrgrp.load(new FileInputStream("usr_grp.cfg"));
			usrgrp_menu_mapping.load(new FileInputStream("usrgrp_menu_mapping.cfg"));
			template_usr_mapping.load(new FileInputStream("template_usr_mapping.cfg"));
			locale_code_mapping.load(new FileInputStream("locale_code_mapping.cfg"));
			enterprise_code_mapping.load(new FileInputStream("enterprise_code_mapping.cfg"));
			//--------------------------------------------------
			//	control.dat is used to keep track of the 
			//	last update date , and is updated when the 
			//  process is run
			//--------------------------------------------------
			cntrlfile = new File("control.dat"); 
			if(!cntrlfile.exists())cntrlfile.createNewFile();  
			controlFile.load(new FileInputStream(cntrlfile)); 
			errorlg = new BufferedWriter(
					new FileWriter(getProperty("errorlog_path")+getProperty("errorlog_name")));
			infolog = new BufferedWriter(
					new FileWriter(getProperty("infolog_path")+getProperty("infolog_name"))); 
			delimiter = getProperty("delimiter");
			cal = Calendar.getInstance();
			dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			rejectsfile = new File(getProperty("current_rejects_directory_path")
								  +getProperty("current_rejects_file")); 
			rejectsout = new BufferedWriter(new FileWriter(rejectsfile));
 
			String currdatestr = dateFormat.format(cal.getTime()); 
			Date currdate = dateFormat.parse(currdatestr.trim());
			String lastupdatestr = controlFile.getProperty("LAST_UPDATE"); 
			Date lastdate = dateFormat.parse(currdatestr.trim());
			if(lastupdatestr!=null && !lastupdatestr.equals("")){
				lastdate = dateFormat.parse(lastupdatestr.trim());
			}
 
			writeToInfoLog("STRTING TO WRITE INFO LOG DATE "+cal.getTime());
			
			int datecompare = lastdate.compareTo(currdate); 
			createLocationIndex();

			if(lastupdatestr==null || lastupdatestr.equals("")){ 
				createValidUserListForCurrentInput(); 
				updateControlFile();
			}else  if(datecompare == 0){ 
				infolog.write("This process has been executed for "+cal.getTime()+"\n");
				infolog.write("Please delete value of the LAST_UPDATE from control.dat file \n" +
						"if you need to run the process again for the current date.\n");
				infolog.flush();
			}
			
			if(datecompare==-1){
				findUpdates();
				updateControlFile(); 
				
				//commenting temp file creation and last_update.dat file deletion
				//as records will be compared against last_update.dat file.
				/*String tempfilepath = getProperty("temp_file_path")+getProperty("temp_file_name");  
				String lastupdatedfile = getProperty("current_accepted_recs_path")+getProperty("current_accepted_recs_prefix");
				File file_tempfilepath = new File(tempfilepath);
				File file_lastupdatedfile = new File(lastupdatedfile);
				file_lastupdatedfile.delete(); 
				file_tempfilepath.renameTo(file_lastupdatedfile);*/				
			} 
			rejectsout.flush();
			rejectsout.close();
			errorlg.close();
			writeToInfoLog("ENDING WRITING TO INFO LOG DATE "+cal.getTime());
			infolog.close();
			
			createArchive();
			deleteOldArchives();
		} catch (Exception e) {
			writeToErrorLog("Error : "+e.getMessage());
			writeToErrorLog(stackTrace(e)); 
		}

	}
	
	  

	/**
	 * write to Error log.
	 * 
	 * @param str
	 */
	private void writeToErrorLog(String str) {
		try {
			errorlg.write("Error : " + str + "\n");
			errorlg.flush();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * write to Error log.
	 * 
	 * @param str
	 */
	private void writeToInfoLog(String str) {
		try {
			infolog.write("Info : " + str + "\n");
			infolog.flush();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * The config.cfg file is read as key,value pair in a property file, this
	 * method retrieves the property value given the key.
	 * 
	 * @param key
	 * @return
	 */
	private String getProperty(String key) {
		String value = this.configFile.getProperty(key);
		return value;
	}
	
	
	private void deleteOldArchives(){
		String archivefile_name_prefix = getProperty("archivefile_name_prefix");
		String archive_delete_seq = getProperty("archive_delete_seq");
		long del_deq=30;
		if(archive_delete_seq!=null && !archive_delete_seq.equals("")){
			del_deq = (new Long(archive_delete_seq)).longValue();
		}
		
		Calendar cal2 =  Calendar.getInstance();
		 
		try { 
			File file = new File(getProperty("archivefile_path")); 
			File[] files = file.listFiles();  
			for(int i=0;i<files.length;i++){ 
				String fname = files[i].getName();
				String fcreatedate = fname.substring(fname.indexOf(archivefile_name_prefix)+archivefile_name_prefix.length(),fname.indexOf("."));
				Date lastdate = dateFormat.parse(fcreatedate.trim());
				cal2.setTime(lastdate);
				long diffInDays = (cal.getTimeInMillis() - cal2.getTimeInMillis())  / (1000 * 60 * 60 * 24); 
				if(diffInDays>del_deq){
					files[i].delete();
				}
			} 
		} catch (Exception e) {
			writeToErrorLog("Error : "+e.getMessage());
			writeToErrorLog(stackTrace(e)); 
		}
	}
	
	/**
	 * This will generate a dated .zip file containing information and data from the current run.
	 * @throws IOException
	 */
	private void createArchive() throws IOException {
		try { 
			
            String rejectf = getProperty("current_rejects_directory_path")+getProperty("current_rejects_file");
			String lastupdatef = getProperty("current_accepted_recs_path")+getProperty("current_accepted_recs_prefix");  
            String errorf = getProperty("errorlog_path")+getProperty("errorlog_name");  
            String infof = getProperty("infolog_path")+getProperty("archivefile_name_prefix");  
            String inboundf = getProperty("current_inbound_file_path")+getProperty("current_inbound_file_name");  
            String outboundf = getProperty("outbound_file_path")+getProperty("outbound_file_name");  
            
            String[] sourceFiles = {rejectf,lastupdatef,errorf,infof,inboundf,outboundf};
            
			String zipFileName = getProperty("archivefile_path")+getProperty("archivefile_name_prefix")+dateFormat.format(cal.getTime())+".zip";
			File zipFile = new File(zipFileName);
			zipFile.createNewFile();
			
			byte[] buffer = new byte[1024];
			FileOutputStream fout = new FileOutputStream(zipFile);

			ZipOutputStream zout = new ZipOutputStream(fout);

			for (int i = 0; i < sourceFiles.length; i++) {
				File fl = new File(sourceFiles[i]);
				if(fl.exists()){
					FileInputStream fin = new FileInputStream(sourceFiles[i]);
					zout.putNextEntry(new ZipEntry(sourceFiles[i]));
					int length;
					while ((length = fin.read(buffer)) > 0) {
						zout.write(buffer, 0, length);
					}
					zout.flush();
					zout.closeEntry();
					fin.close();
				}
			}
			zout.close();
		} catch (Exception ioe) {
			//System.out.println("IOException :" + ioe.getMessage());
			writeToErrorLog("Error : "+ioe.getMessage());
			writeToErrorLog(stackTrace(ioe)); 
		} 
	}

	/**
	 * This will search all records from the current in-bound file against
	 * previous days list
	 */
	private void findUpdates() {
		try {
			//cal.add(Calendar.DATE, -1); // YESTER DAY
			File yesterdayfile = new File(
					getProperty("current_accepted_recs_path")
					+getProperty("current_accepted_recs_prefix") 
					); 
		 
			boolean exists = yesterdayfile.exists();
			if (exists) {
				StandardAnalyzer analyzer = new StandardAnalyzer(
						Version.LUCENE_35);
				Directory userindex = new RAMDirectory();
				IndexWriterConfig indexconfig = new IndexWriterConfig(
						Version.LUCENE_35, analyzer);

				createIndexForYesterdayList(yesterdayfile, userindex,
						indexconfig);
				searchRecordandUpdate(analyzer, userindex);
			}/* else {
				// nothing
				System.out.println("This is the first time the process is run "
						+ dateFormat.format(cal.getTime()));
			}*/

		} catch (Exception e) {
			writeToErrorLog("Error : "+e.getMessage());
			writeToErrorLog(stackTrace(e)); 
		}	
	}

	/**
	 * will search an individual record from the current in-bound file against
	 * previous days list and output to out-bound file.
	 * 
	 * @param analyzer
	 * @param userindex
	 */

	private void searchRecordandUpdate(StandardAnalyzer analyzer,
			Directory userindex) {
		try {
			BufferedReader in = new BufferedReader(new FileReader(
					getProperty("current_inbound_file_path")
					+getProperty("current_inbound_file_name")));
			
			String outboundfile = getProperty("outbound_file_path")+getProperty("outbound_file_name");
			BufferedWriter buffout = new BufferedWriter(new FileWriter(outboundfile));
			
			//Instead of creating new temp file, the records will be compared against the 
			//last_update.dat
			//String tempfilepath = getProperty("temp_file_path")+getProperty("temp_file_name");
			String tempfilepath = getProperty("current_accepted_recs_path")+getProperty("current_accepted_recs_prefix");
			BufferedWriter tempfile = new BufferedWriter(new FileWriter(tempfilepath,true)); 
			
			IndexReader reader = IndexReader.open(userindex);
			IndexSearcher searcher = new IndexSearcher(reader); 

			String str;
			while ((str = in.readLine()) != null) {
				str = str.trim();
				String l_user_id = str.substring(7, 15);
				searchDoc(l_user_id, str, analyzer, searcher, buffout,tempfile); 
			}
			buffout.flush(); 
			buffout.close();
			tempfile.flush(); 
			tempfile.close();
			searcher.close();
			in.close(); 
			
		} catch (Exception e) {
			writeToErrorLog("Error : "+e.getMessage());
			writeToErrorLog(stackTrace(e)); 
		}
	}

	/**
	 * This will validate individual record of INBOUND data file and create a
	 * file containing a list of valid records , and also a file containing
	 * rejected records for the specific date.
	 * 
	 */

	public void createValidUserListForCurrentInput() {
		try {  
			current_accepted_recs = new File(
					getProperty("current_accepted_recs_path")
					+getProperty("current_accepted_recs_prefix") 
					); 
			
			accepted_out = new BufferedWriter(new FileWriter(
					current_accepted_recs));
			BufferedReader in = new BufferedReader(new FileReader(
					getProperty("current_inbound_file_path")
					+getProperty("current_inbound_file_name")));
			String outboundfile = getProperty("outbound_file_path")+getProperty("outbound_file_name");
			BufferedWriter buffout = new BufferedWriter(new FileWriter(outboundfile));
			 
			
			int totrecs=0;
			int totvalid=0;
			String str;
			while ((str = in.readLine()) != null) {
				totrecs++;
				str = str.trim();
				String recvalue = validateRecord(str);
				if (recvalue!=null) { 
					accepted_out.write(str.trim() + "\n");
					buffout.write(recvalue + "\n");
					buffout.flush();
					totvalid++;
				}
			} 
			
			if((totrecs==0) && (in.readLine()== null)){
				writeToErrorLog("***The Specified INBOUND FILE is EMPTY..."); 
			}else{
				writeToInfoLog("TOTAL NUMBER of RECORDS read from INBOUND FILE	: "+totrecs);
				writeToInfoLog("NUMBER of VALID RECORDS  					  	: "+totvalid); 
			}
			 
			buffout.flush();
			accepted_out.flush();
			buffout.close();
			accepted_out.close(); 
			in.close(); 

		} catch (Exception e) {
			writeToErrorLog("Error : "+e.getMessage());
			writeToErrorLog(stackTrace(e)); 
		}
	}
	
	private void updateControlFile(){
		BufferedWriter controlfileout;
		try {
			controlfileout = new BufferedWriter(new FileWriter(cntrlfile));
		
		controlfileout.write("LAST_UPDATE="+dateFormat.format(cal.getTime()));
		controlfileout.flush();
		controlfileout.close();
		} catch (Exception e) {
			writeToErrorLog("Error : "+e.getMessage());
			writeToErrorLog(stackTrace(e)); 
		}
	}

	/**
	 * This will create a index of records from the list of valid records from
	 * 'yesterday' , index is used here as a way to search if a user exits in
	 * the previous load, without extensive file handling or keeping data in a
	 * DATABASE table for the purpose of search.
	 * 
	 * @param file
	 * @param _userindex
	 * @param _indexconfig
	 */
	public void createIndexForYesterdayList(File file, Directory _userindex,
			IndexWriterConfig _indexconfig) {
		try {
			IndexWriter indexWriter = new IndexWriter(_userindex, _indexconfig);
			BufferedReader in = new BufferedReader(new FileReader(file));

			String str;
			while ((str = in.readLine()) != null) {
				str = str.trim();
				String l_user_id = str.substring(7, 15);
				addDoc(indexWriter, l_user_id, str);
			}
			in.close();
			indexWriter.close();

		} catch (Exception e) {
			writeToErrorLog("Error : "+e.getMessage());
			writeToErrorLog(stackTrace(e)); 
		}
	}

	/**
	 * This will validate the list of records for the current load , by
	 * identifying missing information for a record.
	 * 
	 * @param rec
	 * @return
	 */
	private String validateRecord(String rec) {
		try {
			
			String formattedrec="";
			String group_val = "";
			String template_val = "";

			String l_employee_num = readField(rec,0, 7);
			String l_user_id = readField(rec,7, 15);
			String l_lang_indicator = readField(rec,15, 16);
			String l_store_num = readField(rec,16, 20); 
			
			String l_first_name = readField(rec,20, 50);
			String l_last_name = readField(rec,50, 80);
			String l_badge_num = readField(rec,80, 89);
			String l_enterpise_name=readField(rec,89, 109);
			String l_error_reason = "";
		
			String l_reason = "";
			

			// System.out.println(l_employee_num
			// +"|"+l_user_id+"|"+l_lang_indicator+"|"+l_store_num+"|"+l_first_name+"|"+l_last_name+"|"+l_badge_num);
			if (l_employee_num.trim().isEmpty())
				l_error_reason = l_error_reason + "||EMPLOYEE NUM IS NULL";
			if (l_user_id.trim().isEmpty())
				l_error_reason = l_error_reason + "||USER ID IS NULL";
			if (l_lang_indicator.trim().isEmpty())
				l_error_reason = l_error_reason
						+ "||LANGUAGE INDICATOR IS NULL";
			if (!(l_lang_indicator.trim().equalsIgnoreCase("E") || l_lang_indicator
					.trim().equalsIgnoreCase("F")))
				l_error_reason = l_error_reason
						+ "LANG IDICATOR IS NOT ENG OR FRENCH";
			if (l_first_name.trim().isEmpty())
				l_error_reason = l_error_reason + "||FIRST NAME IS NULL";
			if (l_last_name.trim().isEmpty())
				l_error_reason = l_error_reason + "||LAST NAME IS NULL";
			
			int templatecount = 0;
			int l_groupcount = 0;
			//int loop_add = 89;
			int loop_add = 129;
			int max_record_length = rec.length(); 
			boolean checkgroups = true;
			String allgroups = "";
			int allgroupslength = 0;
			String menu_code= "";

			if ((max_record_length <= loop_add)
					|| rec.substring(loop_add, max_record_length).isEmpty()) {
				//l_reason = "||GROUP IS NULL FOR RECORD ||>>" + rec;
				writeToInfoLog("||GROUP IS NULL FOR RECORD ||>> "+rec+"\n");
				checkgroups = false;
			} else {
				allgroups = rec.substring(loop_add, max_record_length);
				allgroupslength = allgroups.length();
			}
			
			Vector vec = new Vector(); 
			Object[] allgroups_arry = new Object[]{}; 
			
			
			if(checkgroups) findGroups(vec,rec,loop_add,max_record_length);
			allgroups_arry = vec.toArray();   
			allgroupslength = allgroups_arry.length;
			if (checkgroups && (allgroups_arry.length == 0)) {
				//l_reason = "||GROUP IS NULL FOR RECORD ||>>" + rec;
				writeToInfoLog("||GROUP IS NULL FOR RECORD ||>> "+rec+"\n");
			} else {

				for (int i = 0; i < allgroupslength; i++) { 
					if ((allgroups_arry[i]!=null) 
							&& allgroups_arry[i].toString()
							.startsWith(getProperty("template_string_start_format"))) {
						// check for validity of these two checking
						templatecount = templatecount + 1;
						template_val = template_val+delimiter+allgroups_arry[i];
					} 					
					if ((allgroups_arry[i]!=null) &&  allgroups_arry[i].toString()
							.startsWith(getProperty("group_string_start_format"))) { 
						// check for validity of these two checking
						l_groupcount = l_groupcount + 1;
						menu_code += usrgrp.getProperty(((String)allgroups_arry[i]).trim());
						group_val = group_val+delimiter+padString((String)allgroups_arry[i],40);						
					} 
					if ((allgroups_arry[i]!=null) 
							&&  !allgroups_arry[i].toString().startsWith(getProperty("group_string_start_format")) 							
							&& !allgroups_arry[i].toString().startsWith(getProperty("template_string_start_format"))) {
						
						l_reason = "!!!GROUP/TEMPLATE :  ||"
								+ allgroups_arry[i]
								+ "||  format is not correct.. Ignored";
					}
				}
				writeToInfoLog("USERID: "+l_user_id+" GROUP COUNT : "+l_groupcount+"\n");
				writeToInfoLog("USERID: "+l_user_id+" TEMPLATE COUNT : "+templatecount+"\n");				
			}

			if (getProperty("istemplate").equalsIgnoreCase("YES") && templatecount == 0) {
				l_error_reason = " || TEMPLATE NAME IS MANDATORY.";
			}
			if (!getProperty("istemplate").equalsIgnoreCase("YES") && templatecount == 0) {
				writeToInfoLog("TEMPLATE VALUE MISSING FOR RECORD (NOT MANDATORY) : "+rec );
			}
 
			if (!l_error_reason.isEmpty()) {
				rejectsout.write("*** Error " + l_error_reason + "\n");
			}
			if (!l_reason.isEmpty()) {
				rejectsout.write("REASON : "+l_reason+"  :  "+rec + "\n");
			}
			rejectsout.flush();
			
			if(l_reason.isEmpty() && l_error_reason.isEmpty()){
				// get Enterprise code from enterprise name
				String l_enterprise_code = getEneterpriseCode(l_enterpise_name);
				
				// get menu name from menu code
				char[] menu_code_chars = menu_code.toCharArray();
				java.util.Arrays.sort(menu_code_chars);
				String sorted_menu_code = new String(menu_code_chars);
				String uniq_chars_menu_code = getUniqueueChars(sorted_menu_code);
				//Started PVA changes
				if(uniq_chars_menu_code != null && uniq_chars_menu_code.equalsIgnoreCase("ab")){
					
				}
				//Ended PVA changes
				String menu_name = usrgrp_menu_mapping.getProperty(uniq_chars_menu_code);
				if(menu_name == null || menu_name.equals("")){
					menu_name = usrgrp_menu_mapping.getProperty("DEFAULT_MENU_CODE");
				}
				String locale_code = locale_code_mapping.getProperty(l_enterprise_code+"-"+l_lang_indicator);
				if(locale_code == null || locale_code.equals("")){
					locale_code = locale_code_mapping.getProperty("DEFAULT_LOCALE_CODE");
				}
				//get Template user from enterpriseCode and menu combination
				String l_template_user = template_usr_mapping.getProperty(l_enterprise_code+"-"+menu_name);
				if (l_template_user==null || l_template_user.equals("")){
					l_template_user = template_usr_mapping.getProperty("DEFAULT_TEMPLATE_USER");
				}
				// EMPLOYEE_NUM	USER_ID	LANG_INDICATOR	STORE_NUM	FIRST_NAME	LAST_NAME	BADGE_NUM	TEMPLATE_NAME	GROUP1	GROUP2
				// Function definition changed for Defect 410- added group
				l_store_num = searchAndReplaceStoreIdForLocation(l_store_num,group_val);
				formattedrec = padString(l_employee_num,7)+delimiter
						+padString(l_user_id,8)+delimiter
						+padString(l_lang_indicator,1)+delimiter
						+padString(l_store_num,7)+delimiter
						+padString(l_enterprise_code,7)+delimiter
						+padString(l_first_name,30)+delimiter
						+padString(l_last_name,30)+delimiter
						+padString(l_badge_num,9)+delimiter
						+padString(menu_name,40)+delimiter
						+padString(locale_code,7)+delimiter
						+padString(l_template_user,40)+delimiter;
				
				formattedrec = formattedrec+padString(Integer.toString(l_groupcount),2)+delimiter;
				if(template_val.length()>0){
					formattedrec = formattedrec+padString(template_val,40)+delimiter;
				}
				if(l_groupcount>0){	 
					formattedrec = formattedrec+group_val;//padString(group_val,40);  
				}						
									
				return formattedrec;
			}else{
				rejectsout.write("RECORD EXISTS  : "+rec + "\n");
				return null;
			}
			// return l_reason.isEmpty() && l_error_reason.isEmpty();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			writeToErrorLog("Error : "+e.getMessage());
			writeToErrorLog(stackTrace(e)); 
			return null;
		}

	}
	
	private String readField(String rec,int start,int end){		
		try{
			if(rec.length()<end){
				return rec.substring(start,rec.length());
			}else{
				return rec.substring(start,end);
			}
		}catch(Exception e){ 
			String gg = rec.format("%1$-" + (end-start) + "s", "");	 
			return gg;
		}  
	}
	
	
	/**
	 * 
	 * @param strng
	 * @param len
	 * @return
	 */
	private String padString(String strng, int len){
		  String str = strng;
		  if(str == null){
			  str=" ";
		  }
		  StringBuffer padded = new StringBuffer(str);
		  while (padded.length() < len)
		  {
		    padded.append(" ");
		  } 
		  return padded.toString();

	}
	
	
	/**
	 * 
	 * @param arry
	 * @param rec
	 * @param loop_add
	 * @param max_record_length
	 */
	private void findGroups(Vector arry,String rec, int loop_add,int max_record_length){ 
		if(max_record_length<loop_add+40){
			arry.add((String)rec.substring(loop_add, max_record_length)); 
		} else {
			arry.add((String)rec.substring(loop_add, loop_add+40)); 
			loop_add =loop_add+40; 
			findGroups(arry,rec,loop_add,max_record_length);
		}
	}
	
	/**
	 * This will search a user record from the index based on the users id, if
	 * not found ie: its a new user, and will be added to the valid list to be
	 * sent to OUTBOUND data file. if found check if the user's record match
	 * with yesterdays feed, if there are changes then the user is added to the
	 * valid list to be sent to OUTBOUND data file.
	 * 
	 * @param query
	 */

	public void searchDoc(String query, String userrec,
			StandardAnalyzer _analyzer, IndexSearcher searcher,
			BufferedWriter buffout,BufferedWriter tempfile) {
		try { 
			TopScoreDocCollector collector = TopScoreDocCollector.create(
					hitsPerPage, true);
			/*Query q = new QueryParser(Version.LUCENE_35, "user", _analyzer)
					.parse(querystr);*/
			Query q = new QueryParser(Version.LUCENE_35, "user", _analyzer).parse(query);
			searcher.search(q, collector);
			ScoreDoc[] hits = collector.topDocs().scoreDocs; 
			if (hits.length == 0) { 
				String recvalue = validateRecord(userrec);
				if (recvalue!=null) {  
					buffout.write(recvalue + "\n"); 
					tempfile.write(userrec + "\n"); 
				}
			}
			//As part of R2,Modified the logic to check if multiple hits are found: check 
			//for matching records. If no matc found then write the record to output.dat
			//otherwise write to Rejects.log
			if (hits.length >= 1) {
				boolean bIsMatchingRecord = true;
				for (int i=0; i<hits.length; i++){
					int docId = hits[i].doc;
					Document d = searcher.doc(docId); 
					if (!d.get("data").equalsIgnoreCase(userrec)) { 
						/*String recvalue = validateRecord(userrec);
						if (recvalue!=null) {  
							buffout.write(recvalue + "\n"); 
							tempfile.write(userrec + "\n"); */		
						bIsMatchingRecord = false;						
					}else{ 
						//rejectsout.write("RECORD EXISTS  : "+userrec + "\n");
						bIsMatchingRecord = true;
						break;
					}
				}
				if(!bIsMatchingRecord){
					String recvalue = validateRecord(userrec);
					if (recvalue!=null) {  
						buffout.write(recvalue + "\n"); 
						tempfile.write(userrec + "\n");
					}			
				} else{
					rejectsout.write("RECORD EXISTS  : "+userrec + "\n");
				}	
			}
			//accepted_out.flush(); 
			// possibility of having hits.length > 1 , is zero.

		} catch (Exception e) {
			writeToErrorLog("Error : "+e.getMessage());
			writeToErrorLog(stackTrace(e)); 
		}

	}
	
	/**
	 * Used to add a user record to the index.
	 * 
	 * @param w
	 * @param user
	 * @param data
	 */

	private void addDoc(IndexWriter w, String user, String data) {
		try {
			Document doc = new Document();
			doc.add(new Field("user", user, Field.Store.YES,
					Field.Index.ANALYZED));
			doc.add(new Field("data", data, Field.Store.YES,
					Field.Index.ANALYZED));
			w.addDocument(doc);
		} catch (Exception e) {
			writeToErrorLog("Error : "+e.getMessage());
			writeToErrorLog(stackTrace(e)); 

		}
	}

	private void addLocationsDoc(IndexWriter w, String store_id, String data) {
		try {
			Document doc = new Document();
			doc.add(new Field("storeid", store_id, Field.Store.YES,
					Field.Index.ANALYZED));
			doc.add(new Field("locationtype", data, Field.Store.YES,
					Field.Index.ANALYZED));
			w.addDocument(doc);
		} catch (Exception e) {
			writeToErrorLog("Error : "+e.getMessage());
			writeToErrorLog(stackTrace(e)); 

		}
	}
	
	
	private String stackTrace(Exception e) { 
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		e.printStackTrace(printWriter);
	return stringWriter.toString();
	}

	/**
	 * add all the locations to a separate index
	 */
	private void createLocationIndex(){
		
		locaanalyzer = new StandardAnalyzer(
				Version.LUCENE_35);
		locaindex = new RAMDirectory();
		IndexWriterConfig indexconfig = new IndexWriterConfig(
				Version.LUCENE_35, locaanalyzer); 
		
		try {
			IndexWriter indexWriter = new IndexWriter(locaindex, indexconfig);
			String locationfile= getProperty("locations_file_path") +getProperty("locations_file_name");
			BufferedReader in = new BufferedReader(new FileReader(locationfile));

			String str;
			while ((str = in.readLine()) != null) {
				str = str.trim();
				String[] keyval = str.split(","); 
				if(keyval.length>1){
					//String store_id = str.substring(0, 4);
					//String orgid = str.substring(4, str.length());
					String store_id = keyval[0];
					String orgid = keyval[1];	 
					addLocationsDoc(indexWriter, store_id, orgid);
				}else{
					writeToErrorLog("Error : in "+locationfile+ " : "+str+" is not a valid location.... must contain two values.");
				} 
			}
			in.close();
			indexWriter.close();  
			
		    locareader = IndexReader.open(locaindex);
			locasearcher = new IndexSearcher(locareader);

		} catch (IOException e) {
			writeToErrorLog("Error : "+e.getMessage());
			writeToErrorLog(stackTrace(e)); 
		} 	
	}
	
	/**
	 * 
	 * @param storeid
	 * @return
	 */
	// Function definition changed for Defect 410- added group
	private String searchAndReplaceStoreIdForLocation(String storeid, String group_val){
		
		String recvalue = "       ";
		
		try { 
			String querystr = storeid;
			TopScoreDocCollector collector = TopScoreDocCollector.create(
					hitsPerPage, true);
			Query q = new QueryParser(Version.LUCENE_35, "storeid", locaanalyzer)
					.parse(querystr);

			locasearcher.search(q, collector);
			ScoreDoc[] hits = collector.topDocs().scoreDocs;  
			if (hits.length == 0) { 
				 //return "DEFAULT";
				return recvalue;
			}
			if (hits.length == 1) { 
				int docId = hits[0].doc;
				Document d = locasearcher.doc(docId);
				// changed for Defect 410- Store id should be empty if user is a call center user
				if (d.get("storeid").equalsIgnoreCase(querystr) &&
						(d.get("locationtype").equalsIgnoreCase("TS") || d.get("locationtype").equalsIgnoreCase("BS")) && !group_val.contains("CallCtr") ) { 
					//String recvalue = d.get("locationtype");
					recvalue = storeid + "   ";
					return recvalue;
				}
			} 
			 
			//return "DEFAULT";
			return recvalue;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			writeToErrorLog("Error : "+e.getMessage());
			writeToErrorLog(stackTrace(e)); 
			//return "DEFAULT";
			return recvalue;
		}

	}
	
	/**
	 * 
	 * @param l_enterprise_name
	 * @return
	 */
	private String getEneterpriseCode(String enterprise_name){
		/*if(enterprise_name.trim().equalsIgnoreCase("Lord & Taylor")||enterprise_name.trim().equalsIgnoreCase("Lord and Taylor")){
			return "LT";
		} else
		if(enterprise_name.trim().equalsIgnoreCase("Hudson's Bay Company")){
			return "HBC";
		} else
		if(enterprise_name.trim().equalsIgnoreCase("The Bay")){
			return "BAY";
		} else {
			return "DEFAULT";
		}	*/
		String sEnterpriseCode = enterprise_code_mapping.getProperty(enterprise_name.trim());
		if (sEnterpriseCode == null || sEnterpriseCode.equals("")){
			sEnterpriseCode = enterprise_code_mapping.getProperty("DEFAULT");
		}
		return sEnterpriseCode;
	}
	
	/**
	 * 
	 * @param str
	 * @return
	 */
	private String getUniqueueChars(String str){
		String uniq_char_string = new String("");
	    for (int i = 0; i < str.length(); i++) {
	        if (!uniq_char_string.contains("" + str.charAt(i))) {
	        	uniq_char_string += "" + str.charAt(i);
	        }
	    }
	    return uniq_char_string;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Started");
		Indexer indexer = new Indexer();
		// indexer.searchDoc("C997944");
		System.out.println("Done");
	}
}
