<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output omit-xml-declaration="yes" />  
    <xsl:template match="/">
        <xsl:call-template name="generate"/>
    </xsl:template>

    <xsl:template name="generate">
package com.yantra.interop.services.webservices.rpc.ejb;
import  com.yantra.interop.japi.YIFApi;
import  com.yantra.interop.japi.YIFClientFactory;
import  com.yantra.yfs.japi.YFSException;
import  com.yantra.yfc.util.YFCException;
import  com.yantra.interop.client.InteropEnvStub;
import  com.yantra.interop.client.ContainerUserIdHolder;
import  com.yantra.interop.japi.YIFClientCreationException;
import  com.yantra.yfc.dom.YFCDocument;
import  com.yantra.yfc.log.YFCLogCategory;
import  org.w3c.dom.Document;
import  java.rmi.RemoteException;
// Import standard JWS annotations
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.soap.SOAPBinding;
// Import WebLogic JWS anntoation
import weblogic.jws.WLHttpTransport;
import java.io.IOException;
import java.io.StringWriter;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

@WebService(name="yantrawebservice",serviceName="YIFWebService",targetNamespace="http://yantra.com/yantrawebservices")
@SOAPBinding(style=SOAPBinding.Style.DOCUMENT,use=SOAPBinding.Use.LITERAL,parameterStyle=SOAPBinding.ParameterStyle.WRAPPED)
@WLHttpTransport(contextPath="yantrawebservices", serviceUri="/yantrawebservice",portName="yantrawebservice")

public class yantrawebservices implements YIFWebServiceApi{
  private  YIFApi _api ;
  public yantrawebservices () { }

    protected String getContainerUserId() {
        return null;
    }

          <xsl:for-each select="//Api">
  <xsl:if test="not(@value='createEnvironment' or @value='releaseEnvironment' or @name='invoke' or @name='executeFlow')">
    @WebMethod()
    public <xsl:value-of select="@safeReturn"/><xsl:text xml:space="preserve"> </xsl:text> <xsl:value-of select="@value"/> (<xsl:for-each select="Arg"><xsl:value-of select="@safeClassName"/><xsl:text xml:space="preserve"> </xsl:text><xsl:value-of select="@name"/><xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>) throws RemoteException, YIFWebServiceException {
  
        try{
            if(_api == null) {
                _api = YIFClientFactory.getInstance().getLocalApi();
                ((ContainerUserIdHolder)_api).setContainerUserId(getContainerUserId());
            }
        }catch (YIFClientCreationException e) {    
            throw new YIFWebServiceException(new YFSException(new YFCException(e).getXMLErrorBuf()));
        }
        
        InteropEnvStub envStub = getEnvironmentFromString(<xsl:for-each select="Arg"><xsl:if test="(@className='com.yantra.yfs.japi.YFSEnvironment')"><xsl:value-of select="@name"/></xsl:if></xsl:for-each>);
        try {
            <xsl:value-of select="@return"/> ret = _api.<xsl:value-of select="@value"/>(envStub<xsl:for-each select="Arg"><xsl:if test="@name != 'env'">, <xsl:if test="@isDoc = 'Y'">createInputDoc(<xsl:value-of select="@name"/>)</xsl:if><xsl:if test="@isDoc = 'N'"><xsl:value-of select="@name"/></xsl:if></xsl:if></xsl:for-each>);
            <xsl:if test="@isDoc = 'Y'" >
            if (null != ret) {
               return serialize(YFCDocument.getDocumentFor(ret).getDocument(), "<xsl:text>iso-8859-1</xsl:text>", true);
            } else {
                return "";
            }
            </xsl:if>
            <xsl:if test="@isDoc = 'N'" >
            return ret;
            </xsl:if>
        }
        catch (Exception e) {
            if(e instanceof YFSException)
            	throw new YIFWebServiceException((YFSException)e);
            else
            	throw new YIFWebServiceException(new YFSException(new YFCException(e).getXMLErrorBuf()));
            	
        }
    }
   </xsl:if>
</xsl:for-each>

<xsl:for-each select="//Service">
    @WebMethod()
    public String <xsl:value-of select="@ExposedName"/> (String envString, String apiString) throws RemoteException, YIFWebServiceException {
        try{
            if(_api == null) {
                _api = YIFClientFactory.getInstance().getLocalApi();
                ((ContainerUserIdHolder)_api).setContainerUserId(getContainerUserId());
            }
        }catch (YIFClientCreationException e) {    
            throw new YIFWebServiceException(new YFSException(new YFCException(e).getXMLErrorBuf()));
        }
        
        InteropEnvStub env = getEnvironmentFromString(envString);
        try {
            Document retDoc = _api.executeFlow(env,"<xsl:value-of select="@ServiceName"/>", createInputDoc(apiString));
            if (null != retDoc) {
                return serialize(YFCDocument.getDocumentFor(retDoc).getDocument(), "<xsl:text>iso-8859-1</xsl:text>", true);
            } else {
                return "";
            }
        }
        catch (Exception e) {
            if(e instanceof YFSException)
            	throw new YIFWebServiceException((YFSException)e);
            else
            	throw new YIFWebServiceException(new YFSException(new YFCException(e).getXMLErrorBuf()));
            	
        }
    }
  </xsl:for-each>

    private InteropEnvStub getEnvironmentFromString (String inString) throws RemoteException, YIFWebServiceException{
        // remember to stuff in the correct userid and progid
        // also remember to take care of authentication stuff
        // Actually authentication would have been done prior to 
        // the execution of this method itself
        Document inDoc = null;
        try {
            inDoc = YFCDocument.parse(inString).getDocument();
        } catch(Exception e) {
            throw new YIFWebServiceException(new YFSException(new YFCException(e).getXMLErrorBuf()));
        }

        // Remember to set the password for authentication purposes.
        return (InteropEnvStub)_api.createEnvironment(inDoc);
    }

    private Document createInputDoc (String inString) throws YIFWebServiceException{

        // create a fresh document with the input element as the root element
        Document retDoc = null;
        try{
            retDoc = YFCDocument.parse(inString).getDocument();
        }catch(Exception e){
            throw new YIFWebServiceException(new YFSException(new YFCException(e).getXMLErrorBuf()));
        }
        return retDoc;
    }

    //This method converts the xml into string and also chops-off the document fragment node from the original xml
    private static String serialize(Node node, String encoding, boolean indenting) {
        OutputFormat outFmt = null;
        StringWriter strWriter = null;
        XMLSerializer xmlSerializer = null;
        String retVal = null;
        
        try{
            outFmt = new OutputFormat("xml", encoding, indenting);
            outFmt.setOmitXMLDeclaration(true);
            
            strWriter = new StringWriter();
            
            xmlSerializer = new XMLSerializer(strWriter, outFmt);
            
            short ntype = node.getNodeType();
            
            switch(ntype) {
                case Node.DOCUMENT_FRAGMENT_NODE: xmlSerializer.serialize((DocumentFragment)node); break;
                case Node.DOCUMENT_NODE: xmlSerializer.serialize((Document)node); break;
                case Node.ELEMENT_NODE: xmlSerializer.serialize((Element)node); break;
                default: throw new IOException("Can serialize only Document, DocumentFragment and Element type nodes");
            }
            
            retVal = strWriter.toString();
        } catch (IOException e) {
            retVal = e.getMessage();
        } finally{
            try {
                strWriter.close();
            } catch (IOException ie) {}
        }
        
        return retVal;
    }    
}
</xsl:template>
</xsl:stylesheet>
